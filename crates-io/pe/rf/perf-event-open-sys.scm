(define-module (crates-io pe rf perf-event-open-sys) #:use-module (crates-io))

(define-public crate-perf-event-open-sys-0.1.0 (c (n "perf-event-open-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jplj4nmv2k1hws8lzjq9lg1ji8f1lyajw1jnyzfdh6r376y85kv")))

(define-public crate-perf-event-open-sys-0.2.0 (c (n "perf-event-open-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0djh60sg7if74ggimqxfkpbmwp37pgn5h2bb2h81c1i4nz3y969i")))

(define-public crate-perf-event-open-sys-0.2.1 (c (n "perf-event-open-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fv6is4r6085zfyv3vij99f32j7m4y9c3vhxm812cr0i2211y0dh")))

(define-public crate-perf-event-open-sys-0.3.0 (c (n "perf-event-open-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1p62vdjvjk6s2xv3xlfcljk4iix2x1cchnbnx3qjjhgm1bn7jdkh")))

(define-public crate-perf-event-open-sys-0.3.1 (c (n "perf-event-open-sys") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15kzp7grvkz78lppbik0qqn253s1z5727lim4621pg32g3in7nwm")))

(define-public crate-perf-event-open-sys-0.3.2 (c (n "perf-event-open-sys") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ww2qxap7n8vzmm05gs8x6nk9xzya3pwl70a7lk10vgkc8w1irw3")))

(define-public crate-perf-event-open-sys-0.3.3 (c (n "perf-event-open-sys") (v "0.3.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ml8j2n9062fsbrngkwzka1yjj738hqm6nlcfzpq9f0cxywy5syr")))

(define-public crate-perf-event-open-sys-1.0.0 (c (n "perf-event-open-sys") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01xj0k42ba27w6r6r2fbbrll4y3ilqdjgh53f9cd14lxw917iwv6")))

(define-public crate-perf-event-open-sys-1.0.1 (c (n "perf-event-open-sys") (v "1.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06jnb2jkwjinpj4hc41kbwsqbxdb1ywy5vci4ggly8rcvbsyv6yf")))

(define-public crate-perf-event-open-sys-2.0.0 (c (n "perf-event-open-sys") (v "2.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1322ch9ammbnjkml39vvbxg36y4vws77bn7x1sd93v98j8jpm7rg")))

(define-public crate-perf-event-open-sys-2.0.1 (c (n "perf-event-open-sys") (v "2.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0slx7pvsk4x080r1wpz1pnwpxswpy2n0vrpn7rb3k29b6rjirl72")))

(define-public crate-perf-event-open-sys-3.0.0 (c (n "perf-event-open-sys") (v "3.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0n0z58l3k9qfm5gxrv32q31q5flbf9zihwxwyqwnjb616nxf56xj")))

(define-public crate-perf-event-open-sys-4.0.0 (c (n "perf-event-open-sys") (v "4.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1a626d7zq2r35dz4iwancmp3s2z4akkwdby4a8v5m92ifqfgni3w")))

