(define-module (crates-io pe rf perf) #:use-module (crates-io))

(define-public crate-perf-0.0.0 (c (n "perf") (v "0.0.0") (d (list (d (n "errno") (r "^0.2.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)))) (h "0vrpllgarm0004r7i2hfc5m7c0cg4nlby45rpcdg95mh16fi5wi3")))

(define-public crate-perf-0.0.1 (c (n "perf") (v "0.0.1") (d (list (d (n "errno") (r "^0.2.3") (d #t) (k 0)) (d (n "ioctl-sys") (r "^0.5.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.35") (d #t) (k 0)))) (h "1z3mqazh895kmxcggz88w60hbhvgn4f2p5maf1bp6id3kana816w")))

(define-public crate-perf-0.0.2 (c (n "perf") (v "0.0.2") (d (list (d (n "errno") (r "^0.2.4") (d #t) (k 0)) (d (n "ioctl-sys") (r "^0.5.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "0ff70fdjpgqbd6vxswfjmk8rab7x12sfj3anxg9cwf5ss1h86mk8")))

