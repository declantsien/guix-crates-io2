(define-module (crates-io pe rf perf-count) #:use-module (crates-io))

(define-public crate-perf-count-0.1.0 (c (n "perf-count") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "kperf-rs") (r "^0.1.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmap") (r "0.1.*") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "nom") (r "^4.2.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "148g8r4rb08xvs1ry0m7q3jd6hsq6sfi6lc7vfygxdbxjb9vn93z")))

