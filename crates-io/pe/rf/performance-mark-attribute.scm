(define-module (crates-io pe rf performance-mark-attribute) #:use-module (crates-io))

(define-public crate-performance-mark-attribute-0.1.0 (c (n "performance-mark-attribute") (v "0.1.0") (d (list (d (n "performance-mark-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1jm924ar21mpdndhbzq72894d2s648c0ljs6q7nmlgnzkqlif826")))

(define-public crate-performance-mark-attribute-0.2.1 (c (n "performance-mark-attribute") (v "0.2.1") (d (list (d (n "performance-mark-macro") (r "^0.2.1") (d #t) (k 0)))) (h "11pgcmsv6w1hkvq4iyjx1kmnpgbsk7inyw6fxh11m2kkbdnmwwm6")))

(define-public crate-performance-mark-attribute-0.2.2 (c (n "performance-mark-attribute") (v "0.2.2") (d (list (d (n "performance-mark-macro") (r "^0.2.2") (d #t) (k 0)))) (h "0fqig4wmjz3cgfxarn2rs5s4pjgrsm80j4b745k3i8c4jbc78bw2")))

