(define-module (crates-io pe rf perftree-cli) #:use-module (crates-io))

(define-public crate-perftree-cli-0.1.0 (c (n "perftree-cli") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "perftree") (r "^0.3.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1a06lv3sc01gpaf5h84ilw3byxbgpcxc00vkj4z0fkfwxhh3jrv6")))

