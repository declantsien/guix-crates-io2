(define-module (crates-io pe rf perf-event) #:use-module (crates-io))

(define-public crate-perf-event-0.1.0 (c (n "perf-event") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "13w3hglpnwaf33x397g7valkh6jxirmcdki9jixvp5bbqipj46m2")))

(define-public crate-perf-event-0.1.1 (c (n "perf-event") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ahca5kjlwc5ckw1j1jxd142qq0xf4lhaycjxss450pbs4ynh7yh")))

(define-public crate-perf-event-0.2.0 (c (n "perf-event") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1d133l064n4qnkw8zvxw88y81ydpaqlq91xi69vx06w2vk9gsccn")))

(define-public crate-perf-event-0.3.0 (c (n "perf-event") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^0.1") (d #t) (k 0)))) (h "07zlygkn563hskrzyrbppzihhpgr81mrvpypgglnxxbpbi92rwlj")))

(define-public crate-perf-event-0.4.0 (c (n "perf-event") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^0.3") (d #t) (k 0)))) (h "0xb2bz76h45dbiv5qligx898wwhxhmr7qqkshhgzghszj9s4sd3d")))

(define-public crate-perf-event-0.4.1 (c (n "perf-event") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^0.3") (d #t) (k 0)))) (h "1prm97hbvlhirj6yvvz7z516z1likxfjv7dbj3wywdwz3w0jqbsj")))

(define-public crate-perf-event-0.4.2 (c (n "perf-event") (v "0.4.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^0.3") (d #t) (k 0)))) (h "1hj4kq2qp86j001n1af3is79xwvqkv4lkfz9v1xp294c65ai0iq1")))

(define-public crate-perf-event-0.4.3 (c (n "perf-event") (v "0.4.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^0.3") (d #t) (k 0)))) (h "00v8pyv15dqq14chngnnw33l2ni4if22hzxlzhygnq1m6qpqmcqc")))

(define-public crate-perf-event-0.4.4 (c (n "perf-event") (v "0.4.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^0.3") (d #t) (k 0)))) (h "12cgghy2xvqg62xsh7jvr2sdjdpk1jv5yiwzp5r48bssv2jjpi3n")))

(define-public crate-perf-event-0.4.5 (c (n "perf-event") (v "0.4.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^1.0") (d #t) (k 0)))) (h "02i4wa3lsv76ddn31cv2wb10szwvicr66mlbbskrp4snp786jc17")))

(define-public crate-perf-event-0.4.6 (c (n "perf-event") (v "0.4.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^1.0") (d #t) (k 0)))) (h "0rkc5pjfz966l919i5116jn482bzp3sh1n9vfzgmxmkpi9kw58dp")))

(define-public crate-perf-event-0.4.7 (c (n "perf-event") (v "0.4.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^1.0") (d #t) (k 0)))) (h "0rlxmyscr38wynknqqpy82fypq8ywa52ad3dbm22ia7as8n5d5jk")))

(define-public crate-perf-event-0.4.8 (c (n "perf-event") (v "0.4.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^4.0") (d #t) (k 0)))) (h "08z8will2qdx23aabkvyisd3jrwwnn6bfy8d11cj2d1qj8ykkmml") (f (quote (("hooks") ("default" "hooks"))))))

