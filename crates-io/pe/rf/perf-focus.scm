(define-module (crates-io pe rf perf-focus) #:use-module (crates-io))

(define-public crate-perf-focus-0.1.0 (c (n "perf-focus") (v "0.1.0") (d (list (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rusty-peg") (r "^0") (d #t) (k 0)))) (h "0ji0y0ldq3gmxqy2qxlkghmf8vlhgpjpy5vlargbxk193qcm62r8")))

(define-public crate-perf-focus-0.2.0 (c (n "perf-focus") (v "0.2.0") (d (list (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rusty-peg") (r "^0") (d #t) (k 0)))) (h "04i7yj5q9yy103l7z1h4l9ckmn3zl4iki44xs3m9aq5ggpmf97sa")))

(define-public crate-perf-focus-1.0.0 (c (n "perf-focus") (v "1.0.0") (d (list (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rusty-peg") (r "^0") (d #t) (k 0)))) (h "0hw12nam6l9a3a2jdsjakl9bf6rqwii5nb5rlarm7xkj2g7kw4n9")))

(define-public crate-perf-focus-1.1.0 (c (n "perf-focus") (v "1.1.0") (d (list (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rusty-peg") (r "^0") (d #t) (k 0)))) (h "1p0c7fj16s72031vwddgsyhdskwvqrysa56r2za1vchkfrxnfhl6")))

