(define-module (crates-io pe rf performance_measure) #:use-module (crates-io))

(define-public crate-performance_measure-0.1.0 (c (n "performance_measure") (v "0.1.0") (h "1gx8jd1rnxlj36npaif0hkg33zhvq4lqvdhd00fcz3bxawn7hgsq")))

(define-public crate-performance_measure-1.0.0 (c (n "performance_measure") (v "1.0.0") (h "0pf8saxxripnxrmpkc0lay35g51vmdwj3hhk7l7vf8az5scc68jp")))

(define-public crate-performance_measure-1.1.0 (c (n "performance_measure") (v "1.1.0") (d (list (d (n "graplot") (r "^0.1.22") (o #t) (d #t) (k 0)))) (h "1ksgmdqr9ali0flrzl9l8zakqkpk3br20isdyxnzwvf1bkh2py5q") (f (quote (("default" "plot")))) (s 2) (e (quote (("plot" "dep:graplot"))))))

(define-public crate-performance_measure-1.1.1 (c (n "performance_measure") (v "1.1.1") (d (list (d (n "graplot") (r "^0.1.22") (o #t) (d #t) (k 0)))) (h "17465lgz3agx8q6fgnp8h4wvlzwraas3ydbrabx3rjy0yqajdw85") (f (quote (("default" "plot")))) (s 2) (e (quote (("plot" "dep:graplot"))))))

(define-public crate-performance_measure-2.0.0 (c (n "performance_measure") (v "2.0.0") (d (list (d (n "graplot") (r "^0.1.22") (o #t) (d #t) (k 0)))) (h "1g06q2fgpqwp9zivsrmw84hvbmqa4zx2hjrrryknv66ri4kyapig") (f (quote (("default" "plot")))) (s 2) (e (quote (("plot" "dep:graplot"))))))

