(define-module (crates-io pe rf perfcnt) #:use-module (crates-io))

(define-public crate-perfcnt-0.0.1 (c (n "perfcnt") (v "0.0.1") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "x86") (r "*") (d #t) (k 0)))) (h "1ir95hvi67vvyqqnk2n0fl61pv0yh6zmd30r2p357x0pqdnvxx5n")))

(define-public crate-perfcnt-0.0.2 (c (n "perfcnt") (v "0.0.2") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "x86") (r "*") (d #t) (k 0)))) (h "1if923m496ndv04hbffc9a6pq8b057g9gxh3yvqpgwwlgjp9dabq")))

(define-public crate-perfcnt-0.0.3 (c (n "perfcnt") (v "0.0.3") (d (list (d (n "bitflags") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "x86") (r "^0.6.0") (d #t) (k 0)))) (h "1dvp9idapvfp2hqam4ia46513nhgwx5r5lzp3hbnpbgd4m0km771")))

(define-public crate-perfcnt-0.1.0 (c (n "perfcnt") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "x86") (r "^0.6.0") (d #t) (k 0)))) (h "0w9qmhp2149vi2c1szwjhf1z3299xkwxgr1d7yxvph82a6mch2hw")))

(define-public crate-perfcnt-0.2.0 (c (n "perfcnt") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.1.1") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "mmap") (r "0.1.*") (d #t) (k 0)) (d (n "nom") (r "^1.2.4") (d #t) (k 0)) (d (n "x86") (r "^0.6.0") (d #t) (k 0)))) (h "0c5w1zf2802nflnqcqy611y7i25ygg27czvx2w1n8ccfnxq2vn55")))

(define-public crate-perfcnt-0.3.0 (c (n "perfcnt") (v "0.3.0") (d (list (d (n "bitflags") (r "0.7.*") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "mmap") (r "0.1.*") (d #t) (k 0)) (d (n "nom") (r "^1.2.4") (d #t) (k 0)) (d (n "x86") (r "^0.6.0") (d #t) (k 0)))) (h "18s7qwsqazd2lbd5fcw7ifs4bz0184yca4bj14brprmz64j288jy")))

(define-public crate-perfcnt-0.4.0 (c (n "perfcnt") (v "0.4.0") (d (list (d (n "bitflags") (r "0.7.*") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmap") (r "0.1.*") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "phf") (r "^0.7.23") (d #t) (k 0)) (d (n "x86") (r "^0.10.1") (f (quote ("performance-counter"))) (d #t) (k 0)))) (h "0rdfjhqvki7pbrdav0bwx6mxn4j4mh3v8cia7jwvil0yywf1rkhw")))

(define-public crate-perfcnt-0.3.1 (c (n "perfcnt") (v "0.3.1") (d (list (d (n "bitflags") (r "0.7.*") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmap") (r "0.1.*") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "phf") (r "^0.7.23") (d #t) (k 0)) (d (n "x86") (r "^0.15.1") (f (quote ("performance-counter"))) (d #t) (k 0)))) (h "0v31xg6h8jxky3m0k1szrnwq3ws9llh93gmc4214hw0wqwlp8qsx") (y #t)))

(define-public crate-perfcnt-0.4.1 (c (n "perfcnt") (v "0.4.1") (d (list (d (n "bitflags") (r "0.7.*") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmap") (r "0.1.*") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "phf") (r "^0.7.23") (d #t) (k 0)) (d (n "x86") (r "^0.15.1") (f (quote ("performance-counter"))) (d #t) (k 0)))) (h "09l74ccm800b6clhi94cfjnxx5jm5hbmzy01ck08mwfjas2pwif5")))

(define-public crate-perfcnt-0.4.2 (c (n "perfcnt") (v "0.4.2") (d (list (d (n "bitflags") (r "0.7.*") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmap") (r "0.1.*") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "phf") (r "^0.7.23") (d #t) (k 0)) (d (n "x86") (r "^0.15.1") (f (quote ("performance-counter"))) (d #t) (k 0)))) (h "1g3vprj2c9n8ns26yb99s6gsr9acvmavw9flgi87vicgq95x3g5s")))

(define-public crate-perfcnt-0.4.3 (c (n "perfcnt") (v "0.4.3") (d (list (d (n "bitflags") (r "0.7.*") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmap") (r "0.1.*") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "phf") (r "^0.7.23") (d #t) (k 0)) (d (n "x86") (r "^0.16.3") (f (quote ("performance-counter"))) (d #t) (k 0)))) (h "00w61p1y8b75bpjxs34wrh4bwxcgcby39fx78baabq8zpyijg9bd")))

(define-public crate-perfcnt-0.4.4 (c (n "perfcnt") (v "0.4.4") (d (list (d (n "bitflags") (r "0.7.*") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmap") (r "0.1.*") (d #t) (k 0)) (d (n "nom") (r "^2.0") (d #t) (k 0)) (d (n "phf") (r "^0.7.23") (d #t) (k 0)) (d (n "x86") (r "^0.16.3") (f (quote ("performance-counter"))) (d #t) (k 0)))) (h "1kcajd4i7hjb3k2q7gdw507xmw9l8wl9bds52yig8b1f07wdixzq")))

(define-public crate-perfcnt-0.5.0 (c (n "perfcnt") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmap") (r "0.1.*") (d #t) (k 0)) (d (n "nom") (r "^4.2.3") (d #t) (k 0)) (d (n "phf") (r "^0.7.23") (d #t) (k 0)) (d (n "x86") (r "^0.19.0") (f (quote ("performance-counter"))) (d #t) (k 0)))) (h "0i2lvvgz52bzzsbiz8znz8ldf64cblbwk7pvdksbsgwfmx0v62av")))

(define-public crate-perfcnt-0.5.1 (c (n "perfcnt") (v "0.5.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmap") (r "0.1.*") (d #t) (k 0)) (d (n "nom") (r "^4.2.3") (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (d #t) (k 0)) (d (n "x86") (r "^0.33.0") (f (quote ("performance-counter"))) (d #t) (k 0)))) (h "1zyh1sh07m1qhp0x2s863759g90976vkjkjfmmqffqjrvvaswivy")))

(define-public crate-perfcnt-0.6.0 (c (n "perfcnt") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmap") (r "0.1.*") (d #t) (k 0)) (d (n "nom") (r "^4.2.3") (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (d #t) (k 0)) (d (n "x86") (r "^0.34.0") (f (quote ("performance-counter"))) (d #t) (k 0)))) (h "1b67k82w46qp57l6ldw85cvyq3bga8cci5nvvl52ywbfn329ry75")))

(define-public crate-perfcnt-0.7.0 (c (n "perfcnt") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmap") (r "0.1.*") (d #t) (k 0)) (d (n "nom") (r "^4.2.3") (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (d #t) (k 0)) (d (n "x86") (r "^0.34.0") (f (quote ("performance-counter"))) (d #t) (k 0)))) (h "09nv9mjz4r7na4nc3ndyj7kip15q06yrxalh1bzia3n95f713ivv")))

(define-public crate-perfcnt-0.7.1 (c (n "perfcnt") (v "0.7.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmap") (r "0.1.*") (d #t) (k 0)) (d (n "nom") (r "^4.2.3") (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (d #t) (k 0)) (d (n "x86") (r "^0.37.0") (f (quote ("performance-counter"))) (d #t) (k 0)))) (h "1d686jzy0x74wyc82d3x9cqka3fm1icpv8qwfhnj670p5p7xx948")))

(define-public crate-perfcnt-0.7.2 (c (n "perfcnt") (v "0.7.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmap") (r "0.1.*") (d #t) (k 0)) (d (n "nom") (r "^4.2.3") (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (d #t) (k 0)) (d (n "x86") (r "^0.45.0") (f (quote ("performance-counter"))) (d #t) (k 0)))) (h "1qg8q06sd22jqmixgfpgwbqrigjw4n1h5pbprvi2jiyqfg0gbjsm")))

(define-public crate-perfcnt-0.7.3 (c (n "perfcnt") (v "0.7.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmap") (r "0.1.*") (d #t) (k 0)) (d (n "nom") (r "^4.2.3") (d #t) (k 0)) (d (n "phf") (r "^0.9.0") (d #t) (k 0)) (d (n "x86") (r "^0.45.0") (f (quote ("performance-counter"))) (d #t) (k 0)))) (h "03w34j5nq72xj9s1l9sfx3wwmwvxpypx37xac7s2w9hf622liydq")))

(define-public crate-perfcnt-0.8.0 (c (n "perfcnt") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmap") (r "0.1.*") (d #t) (k 0)) (d (n "nom") (r "^4.2.3") (d #t) (k 0)) (d (n "x86") (r "^0.47.0") (f (quote ("performance-counter"))) (d #t) (k 0)))) (h "008mrdd8zjk54qg8xh8crk9is98sxv2c0kk2v25nzjkhaaazv8ab")))

