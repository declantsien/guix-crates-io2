(define-module (crates-io pe ga pegasus_astro) #:use-module (crates-io))

(define-public crate-pegasus_astro-0.1.0 (c (n "pegasus_astro") (v "0.1.0") (d (list (d (n "astrotools") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lightspeed-astro") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serialport") (r "^4.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio-serial") (r "^5.4") (d #t) (k 0)) (d (n "tonic") (r "^0.7") (d #t) (k 0)) (d (n "tonic-reflection") (r "^0.4") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "153hj9k89s94icy17gdraxihcbrcwx227y39dla9zrhvgyx5wl4s")))

