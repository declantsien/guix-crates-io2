(define-module (crates-io pe ar pearlite-syn) #:use-module (crates-io))

(define-public crate-pearlite-syn-0.1.0 (c (n "pearlite-syn") (v "0.1.0") (d (list (d (n "insta") (r "^1.0.29") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0l94dvbnc8ilblns0y8i8c6bfh0ravsizrf5sfmp19bpp3732cyb") (f (quote (("printing") ("parsing") ("full" "parsing" "printing") ("default" "full"))))))

