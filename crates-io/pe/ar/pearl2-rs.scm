(define-module (crates-io pe ar pearl2-rs) #:use-module (crates-io))

(define-public crate-pearl2-rs-0.1.0 (c (n "pearl2-rs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0b0pv9lkk9hxbnv9qdj5dw4yp516pgkm7cdh5vjxb9d3mi3hwx60")))

(define-public crate-pearl2-rs-0.1.1 (c (n "pearl2-rs") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1845bqi6xm9qp2d2bl96bi7jx5iszrgm2zm0qlsbdjifwgbkh6f8")))

