(define-module (crates-io pe ar pear_codegen) #:use-module (crates-io))

(define-public crate-pear_codegen-0.0.1 (c (n "pear_codegen") (v "0.0.1") (h "1d2z1z8bdfgcjz9b414hwr5lby3v1n2c8xmdv1w86bwf9grz3yfi")))

(define-public crate-pear_codegen-0.0.2 (c (n "pear_codegen") (v "0.0.2") (h "1x6z2bn2n40nzp1l747pqn91rabs9jgqi6zkws6w177a9d1hzm50")))

(define-public crate-pear_codegen-0.0.3 (c (n "pear_codegen") (v "0.0.3") (h "17yrjs0b4klcnpby4c6pswm1c47dc410qn72y6rv987ljrz6rnyb")))

(define-public crate-pear_codegen-0.0.4 (c (n "pear_codegen") (v "0.0.4") (h "1pjyilkggcxc8d79lpvl995az4l3y88fjyk5xgq6xk09yimmh9vr")))

(define-public crate-pear_codegen-0.0.5 (c (n "pear_codegen") (v "0.0.5") (h "1b56v6sdxdyfk39c78kwjxpn9rjbg8n08q5a7m04p6q4g7xsl8z7")))

(define-public crate-pear_codegen-0.0.6 (c (n "pear_codegen") (v "0.0.6") (h "1k3zd37nqhlkq5bnqi8fk5d13xjhmxhxaidb1x59b9q9b00qr2gn")))

(define-public crate-pear_codegen-0.0.7 (c (n "pear_codegen") (v "0.0.7") (h "113896wr4apmvkgpkc1qf3zsnqn1w2fxdllm3b8mnpb5vkksxvzq")))

(define-public crate-pear_codegen-0.0.8 (c (n "pear_codegen") (v "0.0.8") (h "16b2g2c4y1myhdnbghp6zks7kjfc3cgqk0dik83w54gfaxdsw0nb")))

(define-public crate-pear_codegen-0.0.9 (c (n "pear_codegen") (v "0.0.9") (h "0zv5j3s6b9aw6dq5nwzc15zsqmh7pdi8ggj4ic86mia3n7gx2sdw")))

(define-public crate-pear_codegen-0.0.10 (c (n "pear_codegen") (v "0.0.10") (h "092fwzk6dcrw7sp1pj5yij23mnxq3vqmjq15aajl1amk0xyvcm84")))

(define-public crate-pear_codegen-0.0.11 (c (n "pear_codegen") (v "0.0.11") (d (list (d (n "version_check") (r "^0.1.3") (d #t) (k 1)) (d (n "yansi") (r "^0.3") (d #t) (k 1)))) (h "1nf4nhxmkifif4r310fbmag56v239d15j148fazq4m7dp2b5br8n")))

(define-public crate-pear_codegen-0.0.12 (c (n "pear_codegen") (v "0.0.12") (d (list (d (n "version_check") (r "^0.1.3") (d #t) (k 1)) (d (n "yansi") (r "^0.3") (d #t) (k 1)))) (h "136ir1xw3z1cwg008dcsvfl569bbvylfw9434x4v1riyifvkp1nz")))

(define-public crate-pear_codegen-0.0.13 (c (n "pear_codegen") (v "0.0.13") (d (list (d (n "version_check") (r "^0.1.3") (d #t) (k 1)) (d (n "yansi") (r "^0.4") (d #t) (k 1)))) (h "1rhv3sn5csxmwkmamgfg5m2ljdhai4nw4b39b4ihydmnfilwma62")))

(define-public crate-pear_codegen-0.0.14 (c (n "pear_codegen") (v "0.0.14") (d (list (d (n "version_check") (r "^0.1.3") (d #t) (k 1)) (d (n "yansi") (r "^0.4") (d #t) (k 1)))) (h "0cizr0z1gnwihfhg47dfm803pr5mp015wx3qb6x7sd0cnnf6m4rh")))

(define-public crate-pear_codegen-0.0.15 (c (n "pear_codegen") (v "0.0.15") (d (list (d (n "version_check") (r "^0.1.3") (d #t) (k 1)) (d (n "yansi") (r "^0.4") (d #t) (k 1)))) (h "1rpx4wk1xsvn0dfsyggcc175x76k981h4hsib9g9qiyq7rss2hkk")))

(define-public crate-pear_codegen-0.0.16 (c (n "pear_codegen") (v "0.0.16") (d (list (d (n "version_check") (r "^0.1.3") (d #t) (k 1)) (d (n "yansi") (r "^0.4") (d #t) (k 1)))) (h "0r926h4wi7r0j5fjr9v31v6zbcq4akd1cabp4bzfx6il56c10d6a")))

(define-public crate-pear_codegen-0.0.17 (c (n "pear_codegen") (v "0.0.17") (d (list (d (n "version_check") (r "^0.1.3") (d #t) (k 1)) (d (n "yansi") (r "^0.4") (d #t) (k 1)))) (h "0p3rzhmk23fn2gq7i1mvfifpjrs1nac7mx5v6rmyy08qyx2ck5rs")))

(define-public crate-pear_codegen-0.0.18 (c (n "pear_codegen") (v "0.0.18") (d (list (d (n "version_check") (r "^0.1.3") (d #t) (k 1)) (d (n "yansi") (r "^0.4") (d #t) (k 1)))) (h "125f6g07qmaz82c5asy3bdhmqwrr7kwm9947v1jz8lwhj5mb52km")))

(define-public crate-pear_codegen-0.0.19 (c (n "pear_codegen") (v "0.0.19") (d (list (d (n "version_check") (r "^0.1.3") (d #t) (k 1)) (d (n "yansi") (r "^0.4") (d #t) (k 1)))) (h "16gx21m0q1lpm82z2qk81y7061mvy7ijm80mhifqhx0j1d9cwy5c")))

(define-public crate-pear_codegen-0.0.20 (c (n "pear_codegen") (v "0.0.20") (d (list (d (n "version_check") (r "^0.1.3") (d #t) (k 1)) (d (n "yansi") (r "^0.4") (d #t) (k 1)))) (h "1ds2a5km1kzir5x7lzqwkinxyw3a46lfbn7s0065x19dvgqkw3zx")))

(define-public crate-pear_codegen-0.1.0 (c (n "pear_codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "version_check") (r "^0.1.3") (d #t) (k 1)) (d (n "yansi") (r "^0.4") (d #t) (k 1)))) (h "0yfxz8fzpbkms6ms5z2j3maxg6lfxqi02h8kf70sv8r1bdl5qrvr")))

(define-public crate-pear_codegen-0.1.1 (c (n "pear_codegen") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "version_check") (r "^0.1.3") (d #t) (k 1)) (d (n "yansi") (r "^0.4") (d #t) (k 1)))) (h "07vnrzavdn83xwjkw9cghfpqw422m7rsvcfnipzs5qwfd5c4ii9m")))

(define-public crate-pear_codegen-0.1.2 (c (n "pear_codegen") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "version_check") (r "^0.1.3") (d #t) (k 1)) (d (n "yansi") (r "^0.4") (d #t) (k 1)))) (h "0vksbp1l04pm7yx3ggdwqd68552k4jli2khcxgpm8zycjahv8v9k")))

(define-public crate-pear_codegen-0.1.3 (c (n "pear_codegen") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "version_check") (r "^0.1.3") (d #t) (k 1)) (d (n "yansi") (r "^0.4") (d #t) (k 1)))) (h "0f78qmb2w21fb3bdn8x9i086ygf8vw7hid4k101mxvn7jj2nny5k")))

(define-public crate-pear_codegen-0.1.4 (c (n "pear_codegen") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)) (d (n "yansi") (r "^0.5") (d #t) (k 1)))) (h "1jk8jxkgjd3pymldkckg2ligkpy4nnskgcj8qf3yzlf3zlvcihdz")))

(define-public crate-pear_codegen-0.2.0 (c (n "pear_codegen") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0p57824yv8l5ygsck2kwgpl6zqa3avh6jmx77rkcmfz91rwg2b30")))

(define-public crate-pear_codegen-0.2.1 (c (n "pear_codegen") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "110bngan7s13jrfv4c62vkvz933qs8lcchq1fyihg4n8299zjpxk")))

(define-public crate-pear_codegen-0.2.2 (c (n "pear_codegen") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1jwlb51ff2bimsgy7rnj3k4cih0r2jdhqdh1n6dbahdsfr3rq332")))

(define-public crate-pear_codegen-0.2.3 (c (n "pear_codegen") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1l4209fi1n0wj110l12l4xpy32d1xffm61nm82vyq0r37ijcm9c2")))

(define-public crate-pear_codegen-0.1.5 (c (n "pear_codegen") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)) (d (n "yansi") (r "^0.5") (d #t) (k 1)))) (h "19lcpkfspizd4ywwvca6rxgc311m070k3ndvwa9vrbw1snjqna60")))

(define-public crate-pear_codegen-0.2.4 (c (n "pear_codegen") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0g4xhy6gsxh40rva42fqaqlzzxl8gi6qw0c2m0p9zw4k7yjs6qcn")))

(define-public crate-pear_codegen-0.2.5 (c (n "pear_codegen") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1bwrr640bibq9iivxwcs9l7mzv4zq11fqn53r20vjv270mlkrmqs")))

(define-public crate-pear_codegen-0.2.6 (c (n "pear_codegen") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "1w79n08r1vx5d3jwssa5xq53r7vzyv3s9cqk7jzylmjb11wpf777")))

(define-public crate-pear_codegen-0.2.7 (c (n "pear_codegen") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0m0dras73cm92sqn1715ypn46h9z1r8sc043kq9rq1n8v89hz7ys")))

(define-public crate-pear_codegen-0.2.8 (c (n "pear_codegen") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.30") (d #t) (k 0)) (d (n "syn") (r "^2.0.30") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0xrwnlncg7l64gfy82vf6kq55ww7p6krq6bc3pqwymxpiq76f8if")))

(define-public crate-pear_codegen-0.2.9 (c (n "pear_codegen") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "proc-macro2-diagnostics") (r "^0.10.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.30") (d #t) (k 0)) (d (n "syn") (r "^2.0.30") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0izijffdd2xs762497mk0xr7xwmyw62dzdqjz12v70n0bnc5pasb")))

