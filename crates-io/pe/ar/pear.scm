(define-module (crates-io pe ar pear) #:use-module (crates-io))

(define-public crate-pear-0.0.1 (c (n "pear") (v "0.0.1") (h "1q4w061bmsm30xbxzjm3impyhksv38r9sjmlqzk6j0y0bg6l7vj9")))

(define-public crate-pear-0.0.2 (c (n "pear") (v "0.0.2") (h "0sw9gp1bki7268gi4vb8qfxnn8p0vm7j75nakqbkc2qmrq7whw3x")))

(define-public crate-pear-0.0.3 (c (n "pear") (v "0.0.3") (h "0h3h8x8lccdap8hmcpc17rz769phhjnck9wcvfvxzpbydbmk4b7a")))

(define-public crate-pear-0.0.4 (c (n "pear") (v "0.0.4") (h "108fp197a7nyps1xyxcyvn6xyjm5h1557pqv8nkq9ni0cyrxwnh3")))

(define-public crate-pear-0.0.5 (c (n "pear") (v "0.0.5") (h "052vdwaz2hgg9m5ckig9diaay4ijmnha5fwmxhy0qhrcvcja3sb8")))

(define-public crate-pear-0.0.6 (c (n "pear") (v "0.0.6") (h "0724cwd3izlqhq59lkid2lij18fhi3w9qig5c1lyx12lbyq17f48")))

(define-public crate-pear-0.0.7 (c (n "pear") (v "0.0.7") (h "05pxcq9w9fpjfi990visdihbi8wjx7gcykcvkyr6cfahdhnfz11r")))

(define-public crate-pear-0.0.8 (c (n "pear") (v "0.0.8") (h "0d0wgaadc75yw5j03mq2b940mndwi069wg1clnxpmyvad4qfzg83")))

(define-public crate-pear-0.0.9 (c (n "pear") (v "0.0.9") (h "1l6sa43acmwa279q1nbmn4s1n1d7n0bmabx86riiivvfx70nd445")))

(define-public crate-pear-0.0.10 (c (n "pear") (v "0.0.10") (h "0nzinw1kv1wy6vwl7z8i80827z3dhndzhh4fcm3v061c9q40xpc7")))

(define-public crate-pear-0.0.11 (c (n "pear") (v "0.0.11") (h "1nls7vk0qbmrxnlmb3inq2p44xbyjshc28wd1azif6rq8n5ai9bf")))

(define-public crate-pear-0.0.12 (c (n "pear") (v "0.0.12") (h "1gf52s396c53mkhjshdlba4c6ccba5xvwilfmjzxjl0ndjyxmhmm")))

(define-public crate-pear-0.0.13 (c (n "pear") (v "0.0.13") (h "0fdfaa0ax29ipy4bjchg3ysp6cgb98kiv6qc68654fm9w7qn7h9x")))

(define-public crate-pear-0.0.14 (c (n "pear") (v "0.0.14") (h "1mc06vihjcm2qb17ij5whm45kww2kh33h8n3fwxl37mk2dig9g3q")))

(define-public crate-pear-0.0.15 (c (n "pear") (v "0.0.15") (h "118zx6h93mx38a2vwy5if89hi6hq599l1kxbfj6y2slw3k95lg5f")))

(define-public crate-pear-0.0.16 (c (n "pear") (v "0.0.16") (h "0zxpkj7bashx0s56gvjy66wmyv3dp6wv8rwzgsk1046g0ym4bdmr")))

(define-public crate-pear-0.0.17 (c (n "pear") (v "0.0.17") (h "12d25pka7spibl0ph20q1i1h4vkvncflfiimvkz3awrqkdprc2rd")))

(define-public crate-pear-0.0.18 (c (n "pear") (v "0.0.18") (h "0npqhimc029c10mw3p8dqly542ajhx627bffify1r8kfmjhgcs78")))

(define-public crate-pear-0.0.19 (c (n "pear") (v "0.0.19") (h "0gpa88smrnhw10yfdxy7i5rmskm965v4hs8hridmylihqxxqxhbz")))

(define-public crate-pear-0.0.20 (c (n "pear") (v "0.0.20") (h "009klqwh3i0a13rqs3ipjvv6407sz3hikv54kkrw0c54yy7yhgrm")))

(define-public crate-pear-0.1.0 (c (n "pear") (v "0.1.0") (d (list (d (n "pear_codegen") (r "^0.1") (d #t) (k 0)))) (h "14kkxnj2j6l8yxghaqkk1qricy3qp4iyhhq55bk99ni6cbzw3bvg")))

(define-public crate-pear-0.1.1 (c (n "pear") (v "0.1.1") (d (list (d (n "pear_codegen") (r "^0.1") (d #t) (k 0)))) (h "1ik07d30g0zrdnv5m8ql8h6bacdya60wbpinlbpqi0678601h2sp")))

(define-public crate-pear-0.1.2 (c (n "pear") (v "0.1.2") (d (list (d (n "pear_codegen") (r "^0.1") (d #t) (k 0)))) (h "09gzhp6g076clqwcl1yvw2lj3wbs16yv3qykf37gyqvhwj92nvf2")))

(define-public crate-pear-0.1.3 (c (n "pear") (v "0.1.3") (d (list (d (n "pear_codegen") (r "^0.1") (d #t) (k 0)))) (h "18yi8hh2kjhi5p7rrig7b9g8533gckxvmnzg5f3ahzm06gl7pyng")))

(define-public crate-pear-0.1.4 (c (n "pear") (v "0.1.4") (d (list (d (n "pear_codegen") (r "^0.1") (d #t) (k 0)))) (h "12mb00ick6y1slbxk6r4i8qxdbq8km3bs4pygjv94xwnvc9g482k")))

(define-public crate-pear-0.2.0 (c (n "pear") (v "0.2.0") (d (list (d (n "inlinable_string") (r "^0.1") (d #t) (k 0)) (d (n "pear_codegen") (r "^0.2.0") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0hf9qrrh64zf5srhf4p44yqc6j21iv293898vpsh7pgrs35i5xh9") (f (quote (("default" "color") ("color" "yansi"))))))

(define-public crate-pear-0.2.1 (c (n "pear") (v "0.2.1") (d (list (d (n "inlinable_string") (r "^0.1") (d #t) (k 0)) (d (n "pear_codegen") (r "^0.2.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0l2i6bk5l7zcgwfaad8zfa14dji08qxr9lhp1fyz8zky7dmw4q9y") (f (quote (("default" "color") ("color" "yansi"))))))

(define-public crate-pear-0.2.2 (c (n "pear") (v "0.2.2") (d (list (d (n "inlinable_string") (r "^0.1") (d #t) (k 0)) (d (n "pear_codegen") (r "^0.2.2") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (o #t) (d #t) (k 0)))) (h "04xibhjabphsq9da9cljjcqx53cgi79cpgfvx9kysi99g4mkmaw6") (f (quote (("default" "color") ("color" "yansi"))))))

(define-public crate-pear-0.2.3 (c (n "pear") (v "0.2.3") (d (list (d (n "inlinable_string") (r "^0.1") (d #t) (k 0)) (d (n "pear_codegen") (r "^0.2.3") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (o #t) (d #t) (k 0)))) (h "00l7llav8cidhclx0m2gxm267pfa90c7r2x7xbinij74qm0l5r0m") (f (quote (("default" "color") ("color" "yansi"))))))

(define-public crate-pear-0.1.5 (c (n "pear") (v "0.1.5") (d (list (d (n "pear_codegen") (r "^0.1") (d #t) (k 0)))) (h "02lnp8c414z5ds0lskd4zxlalrjljzpkg8g6kizszij4h52sgprj")))

(define-public crate-pear-0.2.4 (c (n "pear") (v "0.2.4") (d (list (d (n "inlinable_string") (r "^0.1") (d #t) (k 0)) (d (n "pear_codegen") (r "^0.2.4") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1s17f54y0qf6xvffardhq2lvg55n2hz0drc4a9bh6x88ly05dj8f") (f (quote (("default" "color") ("color" "yansi"))))))

(define-public crate-pear-0.2.5 (c (n "pear") (v "0.2.5") (d (list (d (n "inlinable_string") (r "^0.1") (d #t) (k 0)) (d (n "pear_codegen") (r "^0.2.5") (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc") (o #t) (d #t) (k 0)))) (h "0z9zx55lciqqll15gq22b9w1yskqjgf8m91xmf3l4fsygw21pf8p") (f (quote (("default" "color") ("color" "yansi"))))))

(define-public crate-pear-0.2.6 (c (n "pear") (v "0.2.6") (d (list (d (n "inlinable_string") (r "^0.1.9") (d #t) (k 0)) (d (n "pear_codegen") (r "^0.2.6") (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc") (o #t) (d #t) (k 0)))) (h "1k78xr9xqxrx7qv5zyw9n897vdnvxmn3wgqn1ib9w7i8abmkmlsw") (f (quote (("default" "color") ("color" "yansi"))))))

(define-public crate-pear-0.2.7 (c (n "pear") (v "0.2.7") (d (list (d (n "inlinable_string") (r "^0.1.9") (d #t) (k 0)) (d (n "pear_codegen") (r "^0.2.7") (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc") (o #t) (d #t) (k 0)))) (h "077pd1lbr5g99gsmcbglcrq6izl32qvd2l2bc2cx6aajf76qd8v1") (f (quote (("default" "color") ("color" "yansi"))))))

(define-public crate-pear-0.2.8 (c (n "pear") (v "0.2.8") (d (list (d (n "inlinable_string") (r "^0.1.12") (d #t) (k 0)) (d (n "pear_codegen") (r "^0.2.8") (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc.1") (o #t) (d #t) (k 0)))) (h "1j03s6m80iqldnm6jzh3k1fbyk0lxirx8bi4ivgq3k3sq7va1k2c") (f (quote (("default" "color") ("color" "yansi"))))))

(define-public crate-pear-0.2.9 (c (n "pear") (v "0.2.9") (d (list (d (n "inlinable_string") (r "^0.1.12") (d #t) (k 0)) (d (n "pear_codegen") (r "^0.2.9") (d #t) (k 0)) (d (n "yansi") (r "^1.0.0-rc.1") (o #t) (d #t) (k 0)))) (h "0rxlyizzaqq6lswgyfdxjxd3dyb1jfml9gwfpbx5g1j8rq0amvmx") (f (quote (("default" "color") ("color" "yansi"))))))

