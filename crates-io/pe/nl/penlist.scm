(define-module (crates-io pe nl penlist) #:use-module (crates-io))

(define-public crate-penlist-0.0.2 (c (n "penlist") (v "0.0.2") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "17in8sh2p3wgfl91xv0al2kg6rc0zr3ph83cbd8szy5y0mbzm0x2")))

(define-public crate-penlist-0.0.3 (c (n "penlist") (v "0.0.3") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "02llfpnq3szjk6a0vkdzmp55wbjpz3z7nssmdrs0ricd5jncmlvc")))

(define-public crate-penlist-0.0.5 (c (n "penlist") (v "0.0.5") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1clmmsyrgxz3h7cnjkqj73kkfhh3vajv64s2i88nyqc96f8655yk")))

(define-public crate-penlist-0.0.6 (c (n "penlist") (v "0.0.6") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vfflvs7wfw5padl0qasggdmc0ny9a20m5fdzw454631zw7yv752")))

(define-public crate-penlist-0.0.7 (c (n "penlist") (v "0.0.7") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rcn26pxijl1rn8cy9rlhzjgfd3vx7kqk7i8q0iyv960bs09fp9g")))

(define-public crate-penlist-0.0.9 (c (n "penlist") (v "0.0.9") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11l7mk8vfri0jm9dcvy71b32z0z8phwv33hxgid3x15llgh2w7cb")))

(define-public crate-penlist-0.1.1 (c (n "penlist") (v "0.1.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "1xj14mddndsiimarqzkm8dy42sdrxa81g07sf09kdki1gb627iif")))

