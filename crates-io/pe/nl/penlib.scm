(define-module (crates-io pe nl penlib) #:use-module (crates-io))

(define-public crate-penlib-0.0.1 (c (n "penlib") (v "0.0.1") (d (list (d (n "palette") (r "^0.4") (d #t) (k 0)))) (h "1q4v9cpcn06s9v470m3dsx0xp3252dr89q8mvsfh4bhsbfqs2917")))

(define-public crate-penlib-0.0.2 (c (n "penlib") (v "0.0.2") (d (list (d (n "palette") (r "^0.4") (d #t) (k 0)))) (h "09wbzzhqn1sp2v9n0bhc9iglfn2kf571kg1nb1xjmf8mb9j4brl7")))

(define-public crate-penlib-0.0.3 (c (n "penlib") (v "0.0.3") (d (list (d (n "palette") (r "^0.4") (d #t) (k 0)))) (h "0fbgrrbl4z1fcn5kpagrvfwnkzyw5r7g1qhd2qyfwbf4hvjxylar")))

(define-public crate-penlib-0.0.4 (c (n "penlib") (v "0.0.4") (d (list (d (n "palette") (r "^0.4") (d #t) (k 0)))) (h "1g47p3r7s7ckh9f56ka9simampc2pkaxmb15z3zb6k7qqb60sxl8")))

