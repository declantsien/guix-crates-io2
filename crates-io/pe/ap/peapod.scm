(define-module (crates-io pe ap peapod) #:use-module (crates-io))

(define-public crate-peapod-0.1.0 (c (n "peapod") (v "0.1.0") (d (list (d (n "bitvec") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "phenotype-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "phenotype-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1043k8x8i7i3pbhvn6pia7p5pyp9xxdxxbpdxa4app8qm204cdal") (y #t)))

(define-public crate-peapod-0.1.1 (c (n "peapod") (v "0.1.1") (d (list (d (n "bitvec") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "phenotype-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "phenotype-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0q4v2v06jzd6nwq8jaz592ysjm417i9m5mxljlqlr8qa2z55wiyv") (y #t)))

(define-public crate-peapod-0.1.2 (c (n "peapod") (v "0.1.2") (d (list (d (n "bitvec") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "phenotype-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "phenotype-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1pb2cjw0xz6lpi6fzssnmwvqkl2za8xba9qc7nm9pkid4z8ayjzc") (y #t)))

(define-public crate-peapod-0.1.3 (c (n "peapod") (v "0.1.3") (d (list (d (n "bitvec") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "phenotype-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "phenotype-macro") (r "^0.1.0") (d #t) (k 0)))) (h "11va1jyiiwyaj1rki3xccfz7b42zmngqdhb12bkss02562qs8ppy") (y #t)))

(define-public crate-peapod-0.1.4 (c (n "peapod") (v "0.1.4") (d (list (d (n "bitvec") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "phenotype-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "phenotype-macro") (r "^0.1.0") (d #t) (k 0)))) (h "035yg92ncwx785gpyrw9svj6jz5rwbqgvqmlf5l9jd7ik8cjnlf2") (y #t)))

(define-public crate-peapod-0.1.5 (c (n "peapod") (v "0.1.5") (d (list (d (n "bitvec") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "phenotype-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "phenotype-macro") (r "^0.1.1") (d #t) (k 0)))) (h "11bwpyf9v0bi71ivjcg974z9cqma6icxb3r5089vqkcaq5khfhca") (y #t)))

(define-public crate-peapod-0.1.6 (c (n "peapod") (v "0.1.6") (d (list (d (n "bitvec") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "phenotype-internal") (r "^0.1.0") (d #t) (k 0)) (d (n "phenotype-macro") (r "^0.1.2") (d #t) (k 0)))) (h "03gi7lji5m5h2k6rb9g9pf5831rgw1dn1yvyrf5dcz6lqkc5f1jf") (y #t)))

(define-public crate-peapod-0.1.7 (c (n "peapod") (v "0.1.7") (d (list (d (n "bitvec") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "phenotype-internal") (r "^0.2.0") (d #t) (k 0)) (d (n "phenotype-macro") (r "^0.1.3") (d #t) (k 0)))) (h "1l4jbbabg81dm5v370qpfnyniivc1mrgk4ixgk32hfnhl59fs9md") (y #t)))

(define-public crate-peapod-0.1.8 (c (n "peapod") (v "0.1.8") (d (list (d (n "bitvec") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "phenotype-internal") (r "^0.2.0") (d #t) (k 0)) (d (n "phenotype-macro") (r "^0.1.4") (d #t) (k 0)))) (h "1vg3smmbksw3dv6gzhg21br9vzayzj01hinkcaz2i14fkzkj9vlr")))

(define-public crate-peapod-0.1.9 (c (n "peapod") (v "0.1.9") (d (list (d (n "bitvec") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "phenotype-internal") (r "^0.2.0") (d #t) (k 0)) (d (n "phenotype-macro") (r "^0.1.4") (d #t) (k 0)))) (h "00h77g1m4njfarbd3j84r0a2nfri12pwsj4a1ykv9ncbbd1ii3v5")))

