(define-module (crates-io pe ti petitset) #:use-module (crates-io))

(define-public crate-petitset-0.1.0 (c (n "petitset") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1wbnw84y1nlx2svnscmqggqzyfn9fh1v8rd2cya375sb009xm24i") (f (quote (("thiserror_trait" "thiserror") ("set_algebra"))))))

(define-public crate-petitset-0.1.1 (c (n "petitset") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0ca3jzfj9n3bzaqzpi9fmf0jfp0pc5mwbg5r65bgamb144zc9xlz") (f (quote (("thiserror_trait" "thiserror") ("set_algebra"))))))

(define-public crate-petitset-0.1.2 (c (n "petitset") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1mrnpl0lhckvaypman2nryd101li1lvm8dql88pf98x85f3isbxa") (f (quote (("thiserror_trait" "thiserror") ("set_algebra"))))))

(define-public crate-petitset-0.2.0 (c (n "petitset") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "18aj1lfn23r832fybd34wvjkchpg890dkyg6ncac6wd84f095kx3") (f (quote (("std") ("set_algebra")))) (s 2) (e (quote (("thiserror" "dep:thiserror" "std") ("serde" "dep:serde" "std"))))))

(define-public crate-petitset-0.2.1 (c (n "petitset") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1b32v1hzxiww5khs6qg3hslk4da5iv326v3mspv6llm247c506kb") (f (quote (("thiserror_compat" "thiserror" "std") ("std") ("set_algebra") ("serde_compat" "serde" "std"))))))

