(define-module (crates-io gu rl gurl) #:use-module (crates-io))

(define-public crate-gurl-0.1.0 (c (n "gurl") (v "0.1.0") (d (list (d (n "mio") (r "^0") (d #t) (k 0)) (d (n "rustls") (r "^0") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "18yab8h582cyparbh5rv5f283xp7a5nqmb4v5nwlkspi1w7krc9g")))

(define-public crate-gurl-0.1.1 (c (n "gurl") (v "0.1.1") (d (list (d (n "mio") (r "^0") (d #t) (k 0)) (d (n "rustls") (r "^0") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0g2aidk9wv4fdnz4n1g1w45sqlfanqnhg6k19rq1saa55fp47r2g")))

(define-public crate-gurl-0.1.2 (c (n "gurl") (v "0.1.2") (d (list (d (n "argh") (r "^0") (d #t) (k 0)) (d (n "mio") (r "^0") (d #t) (k 0)) (d (n "rustls") (r "^0") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "152ra8v521qd79vym8z31g46llvlgjdrs8di5q3sd3qqgb4anbpa")))

