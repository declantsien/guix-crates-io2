(define-module (crates-io gu rl gurls) #:use-module (crates-io))

(define-public crate-gurls-0.0.0 (c (n "gurls") (v "0.0.0") (d (list (d (n "gasm") (r "^0.0.1") (d #t) (k 1)))) (h "151hl7a311a068sarv3sdqq485j7z3f2rjvpchx33ap0c15fvj60")))

(define-public crate-gurls-0.0.1 (c (n "gurls") (v "0.0.1") (d (list (d (n "gear-core") (r "^1.0.2-pre.15") (d #t) (k 0)) (d (n "gear-wasm-builder") (r "^1.0.2-354d660.10") (d #t) (k 1)) (d (n "gmeta") (r "^1.0.2-pre.15") (d #t) (k 0)) (d (n "gstd") (r "^1.0.2-354d660.10") (d #t) (k 0)) (d (n "io") (r "^0") (d #t) (k 0) (p "gurls-io")) (d (n "io") (r "^0") (d #t) (k 1) (p "gurls-io")))) (h "1bz3v36ygizmp1bgdzwg25j0qkicgz2a0m06hvrsdq882plf0j8i")))

(define-public crate-gurls-0.0.2 (c (n "gurls") (v "0.0.2") (d (list (d (n "gear-wasm-builder") (r "^1") (d #t) (k 1)) (d (n "gmeta") (r "^1") (d #t) (k 0)) (d (n "gstd") (r "^1") (d #t) (k 0)) (d (n "io") (r "^0.0.2") (d #t) (k 0)) (d (n "io") (r "^0.0.2") (d #t) (k 1)))) (h "1mvnkmkrj4layv6sk7z2l1q5cp1176a0w2mfxpy7lvziaryryd2f")))

