(define-module (crates-io gu et guetzli-sys) #:use-module (crates-io))

(define-public crate-guetzli-sys-0.0.1 (c (n "guetzli-sys") (v "0.0.1") (d (list (d (n "cc") (r "^1.0.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cpp") (r "^0.3") (d #t) (k 0)) (d (n "cpp_build") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "0fqbg3qsmhwxjxnxmmq9z9nfpdckpdzb7421c44psh5w4nbi5hd5")))

(define-public crate-guetzli-sys-0.0.2 (c (n "guetzli-sys") (v "0.0.2") (d (list (d (n "cc") (r "^1.0.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cpp") (r "^0.3") (d #t) (k 0)) (d (n "cpp_build") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "00nnr75c2dkbfri138pkvd34s66ax1aspz3viqjzldn181bl7f89")))

(define-public crate-guetzli-sys-0.0.3 (c (n "guetzli-sys") (v "0.0.3") (d (list (d (n "cc") (r "^1.0.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cpp") (r "^0.3") (d #t) (k 0)) (d (n "cpp_build") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "1l5s5pand47as98wc8wa5g6ppd77naj27nx3sin2dm0zyw6nq7d3")))

