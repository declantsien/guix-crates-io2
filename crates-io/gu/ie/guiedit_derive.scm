(define-module (crates-io gu ie guiedit_derive) #:use-module (crates-io))

(define-public crate-guiedit_derive-0.1.0 (c (n "guiedit_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "11cxkh9w3sra80wc7warr75lq0f7ah142ydzrwhbryrd5q66xbqc")))

