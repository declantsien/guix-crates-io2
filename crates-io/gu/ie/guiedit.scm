(define-module (crates-io gu ie guiedit) #:use-module (crates-io))

(define-public crate-guiedit-0.1.0 (c (n "guiedit") (v "0.1.0") (d (list (d (n "egui") (r "^0.19.0") (d #t) (k 0)) (d (n "egui-sfml") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "guiedit_derive") (r "=0.1.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "sfml") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "10ykv1y5zcaymqv5mvx3annnfgb0a5gpf9cx3c7fsf5cs25mj307") (f (quote (("default" "derive")))) (s 2) (e (quote (("sfml" "dep:sfml" "egui-sfml") ("derive" "dep:guiedit_derive"))))))

