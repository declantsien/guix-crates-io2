(define-module (crates-io gu io guion_sdl2) #:use-module (crates-io))

(define-public crate-guion_sdl2-0.1.0 (c (n "guion_sdl2") (v "0.1.0") (d (list (d (n "guion") (r "^0.1.0") (d #t) (k 0)) (d (n "qwutils") (r "^0.1.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.33") (f (quote ("ttf"))) (k 0)))) (h "196knjpc69i7zifbr1yhcwrvj88k6y79xbq2gwbqyq051g3gwsqd")))

(define-public crate-guion_sdl2-0.1.1 (c (n "guion_sdl2") (v "0.1.1") (d (list (d (n "guion") (r "^0.1.1") (d #t) (k 0)) (d (n "qwutils") (r "^0.1.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.33") (f (quote ("ttf"))) (k 0)))) (h "08j6cygshg2l1dxqn61zn2i2h378jv4736ipimhryc1b4jivrxxn")))

(define-public crate-guion_sdl2-0.2.0-dev1 (c (n "guion_sdl2") (v "0.2.0-dev1") (d (list (d (n "guion") (r "^0.2.0-dev1") (d #t) (k 0)) (d (n "qwutils") (r "^0.1.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.33") (f (quote ("ttf"))) (k 0)))) (h "1myzypy1631473i3pxzgnmh3mhghj73f7ahbl1mk20wc76kbsc8v")))

(define-public crate-guion_sdl2-0.2.0-dev2 (c (n "guion_sdl2") (v "0.2.0-dev2") (d (list (d (n "guion") (r "^0.2.0-dev2") (d #t) (k 0)) (d (n "qwutils") (r "^0.2.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "sdl2") (r "^0.34") (k 0)))) (h "06qbnzfwz69n0kagbykhajhwfmdvvadm8qng44md810pa04vj2cc")))

