(define-module (crates-io gu in guinistrs) #:use-module (crates-io))

(define-public crate-GuiNistRs-0.1.0 (c (n "GuiNistRs") (v "0.1.0") (d (list (d (n "eframe") (r "^0.19.0") (d #t) (k 0)) (d (n "egui") (r "^0.19.0") (d #t) (k 0)) (d (n "egui_extras") (r "^0.19.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "native-dialog") (r "^0.6.3") (d #t) (k 0)) (d (n "nistrs") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.6.0") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.14.0") (d #t) (k 0)) (d (n "winres") (r "^0.1.12") (d #t) (t "cfg(windows)") (k 1)))) (h "0lp3934vx4xkx8wscv99vafjjfy2s8i2rvvqsirhfd4nb9fmd34s") (f (quote (("wgpu" "eframe/wgpu"))))))

