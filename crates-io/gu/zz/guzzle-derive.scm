(define-module (crates-io gu zz guzzle-derive) #:use-module (crates-io))

(define-public crate-guzzle-derive-0.1.0 (c (n "guzzle-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.36") (d #t) (k 0)))) (h "198irjnrviynlqiiyfmnwdkscjs1dly89y11lfih8jw97knfhx7p")))

(define-public crate-guzzle-derive-1.0.0 (c (n "guzzle-derive") (v "1.0.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.6") (d #t) (k 0)))) (h "0dqywbmrh02gzjvf5hhnhr8ir9n8jvh4556bgs9fkxfrjpn764s5")))

