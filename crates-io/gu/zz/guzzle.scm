(define-module (crates-io gu zz guzzle) #:use-module (crates-io))

(define-public crate-guzzle-0.1.0 (c (n "guzzle") (v "0.1.0") (d (list (d (n "guzzle-derive") (r "^0.1.0") (d #t) (k 0)))) (h "126mnk7wgwyyrmp20qh2c9im0h5dq3ak9gvj963yrxxlfhfws1qp")))

(define-public crate-guzzle-1.0.0 (c (n "guzzle") (v "1.0.0") (d (list (d (n "guzzle-derive") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 1)) (d (n "trybuild") (r "^1.0.17") (d #t) (k 2)))) (h "09yp6mbjwmrml0q6w6cgrqzndhikiqprnwg1nv2jrxxz6668lxiy")))

