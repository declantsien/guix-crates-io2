(define-module (crates-io gu nz gunzip-split) #:use-module (crates-io))

(define-public crate-gunzip-split-0.1.0 (c (n "gunzip-split") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("std" "derive"))) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)))) (h "19yd92h9z83iwzlnc6py0gws3x5n93jsmr77347yradqp0yy09hg")))

(define-public crate-gunzip-split-0.1.1 (c (n "gunzip-split") (v "0.1.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("std" "derive"))) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)))) (h "0i72dipbf757vghi12fcy280z9lvfi3dd49v72b6h3gf3v08vyi6")))

