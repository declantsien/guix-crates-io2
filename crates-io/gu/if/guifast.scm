(define-module (crates-io gu if guifast) #:use-module (crates-io))

(define-public crate-guifast-0.1.0 (c (n "guifast") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libflo") (r "^0.1") (d #t) (k 0)) (d (n "libflo_error") (r "^0.1") (d #t) (k 0)) (d (n "libflo_module") (r "^0.1") (d #t) (k 0)) (d (n "mut_static") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0zl9pmb0mvk1fy0623qpajxwrxkxx9qv1drin3yxiqzyk7s93c6z") (f (quote (("unstable" "serde_macros") ("default" "serde_codegen"))))))

