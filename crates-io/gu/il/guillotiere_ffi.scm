(define-module (crates-io gu il guillotiere_ffi) #:use-module (crates-io))

(define-public crate-guillotiere_ffi-0.4.2 (c (n "guillotiere_ffi") (v "0.4.2") (d (list (d (n "guillotiere") (r "^0.4.2") (d #t) (k 0)))) (h "0bq69ay6kv63m157xv1mikv0fkl5a439048kihba0a2vsf9plfwv")))

(define-public crate-guillotiere_ffi-0.4.3 (c (n "guillotiere_ffi") (v "0.4.3") (d (list (d (n "guillotiere") (r "^0.5.0") (d #t) (k 0)))) (h "0lvzhxg1ms687330rcxbd46l8iam2n2k7wb2w5b6r3g04c28ga6j")))

(define-public crate-guillotiere_ffi-0.6.0 (c (n "guillotiere_ffi") (v "0.6.0") (d (list (d (n "guillotiere") (r "^0.6.0") (d #t) (k 0)))) (h "0bdm7d7bm3cqfs1i5p5p1hcjs3cg4534zm0pinwf5kzqpwaq5hv3")))

