(define-module (crates-io gu il guile-3-sys) #:use-module (crates-io))

(define-public crate-guile-3-sys-0.1.0 (c (n "guile-3-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "19qh1acaq4k0q860lkmlyj47vvlk815b6kz7yd83klsid1sd47bm")))

