(define-module (crates-io gu il guilded_validation) #:use-module (crates-io))

(define-public crate-guilded_validation-0.1.0-dev.1 (c (n "guilded_validation") (v "0.1.0-dev.1") (h "07695iqf6lip7458a3wxml0zzwsd19kg6zq465kqd9q3h5pqgysb") (r "1.57.0")))

(define-public crate-guilded_validation-0.1.0-dev.2 (c (n "guilded_validation") (v "0.1.0-dev.2") (h "082gcj1hm9q0pvls7gsg849rlpgb44lzkg64najw7zn8a1sgcpzk") (r "1.57.0")))

(define-public crate-guilded_validation-0.1.0-dev.3 (c (n "guilded_validation") (v "0.1.0-dev.3") (h "0rnbb5s30wdwlj14b38mqlqcls2gnxbyf2gwvrjwg25d8qd0c6sb") (r "1.57.0")))

(define-public crate-guilded_validation-0.1.0-dev.4 (c (n "guilded_validation") (v "0.1.0-dev.4") (h "1ra738cvp5391g8qh2kip4wh58xwn95x1cb2qkk3m43clkqbwcpq") (r "1.57.0")))

(define-public crate-guilded_validation-0.1.0-dev.5 (c (n "guilded_validation") (v "0.1.0-dev.5") (h "11b5fxdzs9x8zm96kvanc2h25klcc5r89xakx0dccr91v7rw1vxp") (r "1.57.0")))

(define-public crate-guilded_validation-0.1.0-dev.6 (c (n "guilded_validation") (v "0.1.0-dev.6") (h "12pds5h4k3dsvagk1cwfy9421b0cbbfnn2kvz5q6hkdzzid42ckx") (r "1.57.0")))

(define-public crate-guilded_validation-0.1.0-dev.7 (c (n "guilded_validation") (v "0.1.0-dev.7") (h "1cvrg2q58g13w7mdibhh1xhbj3nxkxfygycyab894k1scqvl3a14") (r "1.57.0")))

(define-public crate-guilded_validation-0.1.0-dev.8 (c (n "guilded_validation") (v "0.1.0-dev.8") (h "09mvmcqx7cvxlxsm1qrsilbjvfsks27af8ayb5g6lkbyywy33194") (r "1.57.0")))

(define-public crate-guilded_validation-0.1.0-dev.9 (c (n "guilded_validation") (v "0.1.0-dev.9") (h "0ys3chza8pvgp87bvpib6m6zfbvg4xal7afl1qm7cgcd0chp6iw5") (r "1.57.0")))

(define-public crate-guilded_validation-0.1.0-dev.10 (c (n "guilded_validation") (v "0.1.0-dev.10") (h "0wzfhzd9sf1rnz8myv9ccyndzmizdh6j17lnm41xsi0gvrs5jph1") (r "1.57.0")))

(define-public crate-guilded_validation-0.1.0-dev.12 (c (n "guilded_validation") (v "0.1.0-dev.12") (h "08330zvrhp8v6pzarpa612f33blzfgcmb0x6w09jkgh0r89cvl46") (r "1.57.0")))

(define-public crate-guilded_validation-0.1.0-dev.13 (c (n "guilded_validation") (v "0.1.0-dev.13") (h "1wl26skblj8qjvgxg2q7rp5xa6h3vknssnng601idnqnbgbxh503") (r "1.57.0")))

(define-public crate-guilded_validation-0.1.0-dev.14 (c (n "guilded_validation") (v "0.1.0-dev.14") (h "0fqx7096a6n5liz0wq13qhvmh148d8abk186ggk9kkgvn6z5zp89") (r "1.57.0")))

(define-public crate-guilded_validation-0.1.0-dev.15 (c (n "guilded_validation") (v "0.1.0-dev.15") (h "194dvk4s2bagm49bivym5f419n5hhg9594l8yl2b1wacg22nkyd0") (r "1.57.0")))

(define-public crate-guilded_validation-0.1.0-dev.16 (c (n "guilded_validation") (v "0.1.0-dev.16") (h "1pffwmh05qa4ik8zsgiz7b3m0nw370vs83zkllbdx503krxs5b3w") (r "1.57.0")))

(define-public crate-guilded_validation-0.1.0-dev.17 (c (n "guilded_validation") (v "0.1.0-dev.17") (h "1dpxz9pllmpqdlmh8b3q6j5v70vnj0clgfmbd4pbfmxlqcy4j48f") (r "1.57.0")))

(define-public crate-guilded_validation-0.1.0-dev.18 (c (n "guilded_validation") (v "0.1.0-dev.18") (h "0k8pbdnvp1kgb4dnnkqkga0x4mc44rc2v88bcb7q6kizm6q15i4a") (r "1.57.0")))

(define-public crate-guilded_validation-0.1.0-dev.19 (c (n "guilded_validation") (v "0.1.0-dev.19") (h "1k07kj80l7y50gj06jxafvq72lgh8xal72rqyvdbl2cp7d92n39a") (r "1.57.0")))

