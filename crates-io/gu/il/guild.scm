(define-module (crates-io gu il guild) #:use-module (crates-io))

(define-public crate-guild-0.1.0 (c (n "guild") (v "0.1.0") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serenity") (r "^0.9.3") (d #t) (k 0)) (d (n "tokio") (r "~0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0p3pq3yh9v8v2848652xw8ii1zgjwa6w2p57cgv2c2nhp61zy62k")))

(define-public crate-guild-0.1.1 (c (n "guild") (v "0.1.1") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serenity") (r "^0.9.3") (d #t) (k 0)) (d (n "tokio") (r "~0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "09v325d8rpyic4h94fa6345xygnnai2qy100gi8d8a3ksv2pi6g4")))

(define-public crate-guild-0.1.2 (c (n "guild") (v "0.1.2") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serenity") (r "^0.9.3") (f (quote ("rustls_backend" "http" "model"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "10hm4if3a75lqbhg2gl42sklz72jw9jn8w7gjvi6mmz5nfzcd88h") (y #t)))

(define-public crate-guild-0.1.3 (c (n "guild") (v "0.1.3") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serenity") (r "^0.9.3") (d #t) (k 0)) (d (n "tokio") (r "~0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "19m4lf6qa6p24b1bvyz6xbiw2cn7r3y6as8q1321ddgyl15ihdz4")))

(define-public crate-guild-0.2.0 (c (n "guild") (v "0.2.0") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rusqlite") (r "^0.24.2") (d #t) (k 0)) (d (n "serenity") (r "^0.9.3") (d #t) (k 0)) (d (n "tokio") (r "~0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1rgcrr0922am8nv10yfqansl60shl3iifxvmzxzh7s7ql83qhng1")))

(define-public crate-guild-0.2.1 (c (n "guild") (v "0.2.1") (d (list (d (n "clap") (r "~2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serenity") (r "^0.9.3") (d #t) (k 0)) (d (n "tokio") (r "~0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0db4l7ivzghyj7r9fh6vfh24vbfk9yvqx4vir3b5gv9v5jxk6pmv")))

