(define-module (crates-io gu il guilt-by-association) #:use-module (crates-io))

(define-public crate-guilt-by-association-0.1.0 (c (n "guilt-by-association") (v "0.1.0") (h "1ds7fmj3zfp06dr3gaxp4i3i207k76acm1apzykapvfg7x011sbj")))

(define-public crate-guilt-by-association-0.2.0 (c (n "guilt-by-association") (v "0.2.0") (h "04q26cngq76nh1mdwycapbnm5n2wkagy21wbqmkxfcpviz6xn9wq")))

(define-public crate-guilt-by-association-0.2.1 (c (n "guilt-by-association") (v "0.2.1") (h "1las8g3m20qzmpnkxfxc46g623zyff5l2c3hjid9hjl2fqlx28b6")))

(define-public crate-guilt-by-association-0.2.1-test (c (n "guilt-by-association") (v "0.2.1-test") (h "1x5ak7nv4aivk788g8knr8wyflakbkirl9dlqffd71ijyms5b7yn") (y #t)))

(define-public crate-guilt-by-association-0.3.0 (c (n "guilt-by-association") (v "0.3.0") (h "1blg508vk7hz9qavpqp29gkdjd460zj5r7pl89mrp10lx7hnhj8g")))

(define-public crate-guilt-by-association-0.3.1 (c (n "guilt-by-association") (v "0.3.1") (h "0kyg9f1816hrzkrgiirhkxjjfwx0yjimmh6358p94xzpbs1g6077")))

(define-public crate-guilt-by-association-0.4.0 (c (n "guilt-by-association") (v "0.4.0") (h "1fvmdw4hlsiyd192axsxi56rbz6xprwxmh1dyg6skaslhxzaclzm")))

(define-public crate-guilt-by-association-0.4.1 (c (n "guilt-by-association") (v "0.4.1") (h "1wv323x0xr2gahw4dp02c4vja38gcvhahakyhvdc6lw3yxga732x")))

(define-public crate-guilt-by-association-0.5.0 (c (n "guilt-by-association") (v "0.5.0") (h "028nqnbncx7xgcsfrfcckn2zjww6v9xdsg4x3q6zx87kjlbny7c5")))

