(define-module (crates-io gu il guilded_util) #:use-module (crates-io))

(define-public crate-guilded_util-0.1.0-dev.1 (c (n "guilded_util") (v "0.1.0-dev.1") (d (list (d (n "guilded_model") (r "^0.1.0-dev.18") (d #t) (k 0)) (d (n "guilded_validation") (r "^0.1.0-dev.8") (d #t) (k 0)))) (h "011vhjp7fl8l6nk129hn6r54j49b5n437fzyg07g14sk3yv0x2rb") (f (quote (("builder")))) (r "1.57.0")))

(define-public crate-guilded_util-0.1.0-dev.2 (c (n "guilded_util") (v "0.1.0-dev.2") (d (list (d (n "guilded_model") (r "^0.1.0-dev.18") (d #t) (k 0)) (d (n "guilded_validation") (r "^0.1.0-dev.10") (d #t) (k 0)))) (h "1dbfprdxzrl7xbrk8ffkp7a2im87snl23jip5flxnyk3x16vh496") (f (quote (("builder")))) (r "1.57.0")))

(define-public crate-guilded_util-0.1.0-dev.3 (c (n "guilded_util") (v "0.1.0-dev.3") (d (list (d (n "guilded_model") (r "^0.1.0-dev.20") (d #t) (k 0)) (d (n "guilded_validation") (r "^0.1.0-dev.12") (d #t) (k 0)))) (h "0z2ps51rdmfzjzqlsbhskvsf6q4sb27gsskpqyxv9m38ibdrrnlx") (f (quote (("builder")))) (r "1.57.0")))

(define-public crate-guilded_util-0.1.0-dev.4 (c (n "guilded_util") (v "0.1.0-dev.4") (d (list (d (n "guilded_model") (r "^0.1.0-dev.20") (d #t) (k 0)) (d (n "guilded_validation") (r "^0.1.0-dev.15") (d #t) (k 0)))) (h "154j8ad888g4mn362scgx6a2w2naqqcpl11h2kihbmpg9c5l5hmj") (f (quote (("builder")))) (r "1.57.0")))

(define-public crate-guilded_util-0.1.0-dev.5 (c (n "guilded_util") (v "0.1.0-dev.5") (d (list (d (n "guilded_model") (r "^0.1.0-dev.23") (d #t) (k 0)) (d (n "guilded_validation") (r "^0.1.0-dev.17") (d #t) (k 0)))) (h "0qms17j8xab9mkfrid8fvzybn2v0l3c4al6sa7vjm04pvdl9zd7f") (f (quote (("builder")))) (r "1.57.0")))

(define-public crate-guilded_util-0.1.0-dev.6 (c (n "guilded_util") (v "0.1.0-dev.6") (d (list (d (n "guilded_model") (r "^0.1.0-dev.23") (d #t) (k 0)) (d (n "guilded_validation") (r "^0.1.0-dev.18") (d #t) (k 0)))) (h "03q3v13jszr0gdn4xdkgi45554vpapavzv2wkkm0vwfdppp7m3mn") (f (quote (("builder")))) (r "1.57.0")))

