(define-module (crates-io gu il guile) #:use-module (crates-io))

(define-public crate-guile-0.0.1 (c (n "guile") (v "0.0.1") (d (list (d (n "guile-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1hya8f4g1gsgqgp8rgc3m8m379m5nlphdgq9xlnm7wsg6pzccmhj")))

(define-public crate-guile-0.0.2 (c (n "guile") (v "0.0.2") (d (list (d (n "guile-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1gx08xfhj8ydr3r6mb6ywfszqi1dlhnzfliy67vs611iw32axygq")))

