(define-module (crates-io gu il guile-rs) #:use-module (crates-io))

(define-public crate-guile-rs-0.1.0 (c (n "guile-rs") (v "0.1.0") (d (list (d (n "guile-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0i9c7ggv19n4hbv5am1zm0yw9adacq8r4xprpwhgfvza1s0sxm94")))

(define-public crate-guile-rs-0.1.1 (c (n "guile-rs") (v "0.1.1") (d (list (d (n "guile-rs-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0q2s4qal5hb4i9dmc96s13s606ynhhpfscz1qy74zbb0wr1hb61b")))

