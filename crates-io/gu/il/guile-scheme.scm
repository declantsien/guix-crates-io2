(define-module (crates-io gu il guile-scheme) #:use-module (crates-io))

(define-public crate-guile-scheme-0.1.0 (c (n "guile-scheme") (v "0.1.0") (d (list (d (n "libguile-src") (r "^0.1.0") (d #t) (k 0)))) (h "0z5g7gn847l1nvp35cjnzrkd00pjjgk5vhql2wsddlq1d9w5hq6c")))

(define-public crate-guile-scheme-0.1.1 (c (n "guile-scheme") (v "0.1.1") (d (list (d (n "libguile-src") (r "^0.1.0") (d #t) (k 0)))) (h "04ad4g9ssmhi0vknqdba2j9sp1q3cxm2hmfxa6qlyr46s59i8wqc")))

(define-public crate-guile-scheme-0.1.2 (c (n "guile-scheme") (v "0.1.2") (d (list (d (n "libguile-src") (r "^0.1.0") (d #t) (k 0)))) (h "0x8a69y1jj6s4qygfx1a882qndgrwp0rs39n69ql47m5f18wx6zs")))

(define-public crate-guile-scheme-0.1.3 (c (n "guile-scheme") (v "0.1.3") (d (list (d (n "libguile-src") (r "^0.1.0") (d #t) (k 0)))) (h "14zvg988g2ksxilya0g24hwf6hvyd2zvgh4jwrqch9sjb9zgqw55")))

