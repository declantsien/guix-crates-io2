(define-module (crates-io gu il guilded_model) #:use-module (crates-io))

(define-public crate-guilded_model-0.1.0-dev.13 (c (n "guilded_model") (v "0.1.0-dev.13") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive" "std"))) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("parsing" "std"))) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v5"))) (k 0)))) (h "0hzwli8fsljq84jbrp6immgi80z2v5gyfmlw3scf809ar9rjlz2c") (r "1.57.0")))

(define-public crate-guilded_model-0.1.0-dev.14 (c (n "guilded_model") (v "0.1.0-dev.14") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive" "std"))) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("parsing" "std"))) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v5"))) (k 0)))) (h "14mzvf8c2sa793qqfhvhc2aas6i9pbc11avp6yk7cbbs9n9y2c0g") (r "1.57.0")))

(define-public crate-guilded_model-0.1.0-dev.15 (c (n "guilded_model") (v "0.1.0-dev.15") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive" "std"))) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("parsing" "std"))) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v5"))) (k 0)))) (h "125r89r0mzkijzi1z6jchbwhyarpnsyxd0mrhln5g2v2z14pdi1k") (r "1.57.0")))

(define-public crate-guilded_model-0.1.0-dev.16 (c (n "guilded_model") (v "0.1.0-dev.16") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive" "std"))) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("parsing" "std"))) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v5"))) (k 0)))) (h "1zx7143pzjsczj71bsvak218x5xghv5kjij5zfd11nq3l1sff9l2") (r "1.57.0")))

(define-public crate-guilded_model-0.1.0-dev.17 (c (n "guilded_model") (v "0.1.0-dev.17") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive" "std"))) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("parsing" "std"))) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v5"))) (k 0)))) (h "1z6q3563v8nyax0q4y1bl8xnbgimcvi57zk3piv8hlz652ahd0sc") (r "1.57.0")))

(define-public crate-guilded_model-0.1.0-dev.18 (c (n "guilded_model") (v "0.1.0-dev.18") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive" "std"))) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("parsing" "std"))) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v5"))) (k 0)))) (h "1ss2zrk20asmak8b14dknzwd3qyq144x4cywyjh8pa5cmwxi3hiq") (r "1.57.0")))

(define-public crate-guilded_model-0.1.0-dev.19 (c (n "guilded_model") (v "0.1.0-dev.19") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive" "std"))) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("parsing" "std"))) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v5"))) (k 0)))) (h "1cf2inlav7zw7rnpgs7bn226ji3g6ip3baiz0fffk0gcnyn38k1w") (r "1.57.0")))

(define-public crate-guilded_model-0.1.0-dev.20 (c (n "guilded_model") (v "0.1.0-dev.20") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive" "std"))) (k 0)) (d (n "time") (r "^0.3.10") (f (quote ("parsing" "std"))) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v5"))) (k 0)))) (h "0mxyi7ywj5jrygy6ppj9550q33yx5rdxs2524c3wx83a2lmfkypf") (r "1.57.0")))

(define-public crate-guilded_model-0.1.0-dev.21 (c (n "guilded_model") (v "0.1.0-dev.21") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive" "std"))) (k 0)) (d (n "time") (r "^0.3.14") (f (quote ("parsing" "std"))) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v5"))) (k 0)))) (h "083ijyfgrd8mjnz3h7fi5f1lwyin2zmbbii7dg293865daqrybw9") (r "1.57.0")))

(define-public crate-guilded_model-0.1.0-dev.22 (c (n "guilded_model") (v "0.1.0-dev.22") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive" "std"))) (k 0)) (d (n "time") (r "^0.3.14") (f (quote ("parsing" "std"))) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v5"))) (k 0)))) (h "0d2yd4wpzin8bzj0779hqg2fidmgwkf4r03pk7sv99g7qivvj0q0") (r "1.57.0")))

(define-public crate-guilded_model-0.1.0-dev.23 (c (n "guilded_model") (v "0.1.0-dev.23") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive" "std"))) (k 0)) (d (n "time") (r "^0.3.14") (f (quote ("parsing" "std"))) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v5"))) (k 0)))) (h "0k93xbslj23z88bk7a93i8avrnixj8k9wdpwbn9i0xzsfh0fw1an") (r "1.57.0")))

(define-public crate-guilded_model-0.1.0-dev.24 (c (n "guilded_model") (v "0.1.0-dev.24") (d (list (d (n "serde") (r "^1.0.147") (f (quote ("derive" "std"))) (k 0)) (d (n "time") (r "^0.3.16") (f (quote ("parsing" "std"))) (k 0)) (d (n "uuid") (r "^1.2.1") (f (quote ("v5"))) (k 0)))) (h "0r7yvkh1fv649di6k7jijzbblvlcygzpv3dcf8465dr2xscm5lir") (r "1.57.0")))

