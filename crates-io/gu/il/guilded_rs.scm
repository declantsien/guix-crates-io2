(define-module (crates-io gu il guilded_rs) #:use-module (crates-io))

(define-public crate-guilded_rs-0.0.1 (c (n "guilded_rs") (v "0.0.1") (h "0zhv3ph64gzp2gnvcd2z1w4frd0yy6nr1yfm8xw135jwiz3fk60c") (y #t)))

(define-public crate-guilded_rs-0.1.0 (c (n "guilded_rs") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "csbindgen") (r "^1.7.0") (d #t) (k 1)) (d (n "iso8601-timestamp") (r "^0.2.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_with") (r "^3.0.0") (d #t) (k 0)) (d (n "websocket") (r "^0.26.5") (d #t) (k 0)))) (h "1wrhs1vhagwq4910mbp13dmpzb6c2v3n7fl2jl23z5db2sjm7lsq")))

