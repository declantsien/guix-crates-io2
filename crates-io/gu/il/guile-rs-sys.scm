(define-module (crates-io gu il guile-rs-sys) #:use-module (crates-io))

(define-public crate-guile-rs-sys-0.1.0 (c (n "guile-rs-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.31.3") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wmhp11qalgrpy7g6nhbg2z2nqgxliz7zvxaqdp1xxzmw3y00s4p")))

