(define-module (crates-io gu il guillotiere) #:use-module (crates-io))

(define-public crate-guillotiere-0.1.0 (c (n "guillotiere") (v "0.1.0") (d (list (d (n "euclid") (r "^0.19.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1d8rxv8wai04h99m10w2sw7z6ykji0zmlwi3c2sj5zpxncgjbmzn") (f (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-guillotiere-0.2.0 (c (n "guillotiere") (v "0.2.0") (d (list (d (n "euclid") (r "^0.19.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1i37mycwpilbgpmpbqlwcv40n9zlac3ybf7qa7k3nf391kqg29mr") (f (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-guillotiere-0.3.0 (c (n "guillotiere") (v "0.3.0") (d (list (d (n "euclid") (r "^0.19.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "11lz5wc9haxswp9wl8jycpcy0hvhsq0ah2rynfqq1w332bz8r18p") (f (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-guillotiere-0.4.0 (c (n "guillotiere") (v "0.4.0") (d (list (d (n "euclid") (r "^0.19.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1yksj62i0948a9vbh2x9993ra09kcjv5n8n72pk8xpm220y5n0xq") (f (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-guillotiere-0.4.1 (c (n "guillotiere") (v "0.4.1") (d (list (d (n "euclid") (r "^0.19.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "01pf92xmfasa3kmhkhpcm9clynpi8pchdyzakwva1crhcv36bsa6") (f (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-guillotiere-0.4.2 (c (n "guillotiere") (v "0.4.2") (d (list (d (n "euclid") (r "^0.19.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "svg_fmt") (r "^0.2.0") (d #t) (k 0)))) (h "0daygx3p6r8f16f7d4w40ss5mafwxj35alqhz6z8ypa3nhlgjahq") (f (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-guillotiere-0.4.3 (c (n "guillotiere") (v "0.4.3") (d (list (d (n "euclid") (r "^0.19.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "svg_fmt") (r "^0.2.0") (d #t) (k 0)))) (h "1jdw1w02a9vzsmcq89mx2b0pfddlvhalmqwha17iakpmbjg9ls0p") (f (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-guillotiere-0.4.4 (c (n "guillotiere") (v "0.4.4") (d (list (d (n "euclid") (r "^0.19.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "svg_fmt") (r "^0.2.0") (d #t) (k 0)))) (h "197g5bh89a9izs857habhlyf9316710w8lbcwgg6qiwd2a0ncyj8") (f (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-guillotiere-0.5.0 (c (n "guillotiere") (v "0.5.0") (d (list (d (n "euclid") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "svg_fmt") (r "^0.4.0") (d #t) (k 0)))) (h "1q3yv1m4sc1nd3qd0i7jicdhlf7qi736absx2705s6racwc7h3zn") (f (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-guillotiere-0.5.1 (c (n "guillotiere") (v "0.5.1") (d (list (d (n "euclid") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "svg_fmt") (r "^0.4.0") (d #t) (k 0)))) (h "1daj261b75nhsnd12ml04vkswim6kyb1y3v57n3ykcngy6ils2iq") (f (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-guillotiere-0.5.2 (c (n "guillotiere") (v "0.5.2") (d (list (d (n "euclid") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "svg_fmt") (r "^0.4.0") (d #t) (k 0)))) (h "1hfcfhj2njb9wb0hyh1cag0zbgsm3q2sggpzqik0001g5q2ms1j7") (f (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-guillotiere-0.6.0 (c (n "guillotiere") (v "0.6.0") (d (list (d (n "euclid") (r "^0.22.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "svg_fmt") (r "^0.4.1") (d #t) (k 0)))) (h "07skjvk9bglbvjz0n8cqqlyjfi3ayhk06l7924z6d3s1pzpwqz5w") (f (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-guillotiere-0.6.1 (c (n "guillotiere") (v "0.6.1") (d (list (d (n "euclid") (r "^0.22.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "svg_fmt") (r "^0.4.1") (d #t) (k 0)))) (h "04k2d1dn42k0xnwiksymbsarj4m3wff1r0zyj6yb4817m3gfwlwj") (f (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

(define-public crate-guillotiere-0.6.2 (c (n "guillotiere") (v "0.6.2") (d (list (d (n "euclid") (r "^0.22.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "svg_fmt") (r "^0.4.1") (d #t) (k 0)))) (h "10m7fhp5kzf09kz08k6apkbzblriyqynjl1wwa9i7jrnq1jmhbdn") (f (quote (("serialization" "serde" "euclid/serde") ("checks"))))))

