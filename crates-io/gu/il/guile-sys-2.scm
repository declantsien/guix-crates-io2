(define-module (crates-io gu il guile-sys-2) #:use-module (crates-io))

(define-public crate-guile-sys-2-0.1.0 (c (n "guile-sys-2") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.42.2") (d #t) (k 1)))) (h "1hiia99kpwvl4q5wc8h57ary9b4n5qvmh1j9khfysq9myj0vcxf3")))

(define-public crate-guile-sys-2-0.1.1 (c (n "guile-sys-2") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)))) (h "15p51xvs5wf8va41vip6qar3jlcg1cx3kbai2y5g6glsn78n0iba")))

