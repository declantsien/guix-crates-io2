(define-module (crates-io gu il guile-sys) #:use-module (crates-io))

(define-public crate-guile-sys-0.1.0 (c (n "guile-sys") (v "0.1.0") (h "0lv48y5lqqyr4613yzl82scydj3qr2d6zfmkcizmvqhgw96mnkip")))

(define-public crate-guile-sys-0.1.1 (c (n "guile-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ryw06gq4w4cqcvb9xp90mmbs71krfg2qwg0y0nn3n2q5cw0jgxv")))

