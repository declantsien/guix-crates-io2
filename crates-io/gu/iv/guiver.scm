(define-module (crates-io gu iv guiver) #:use-module (crates-io))

(define-public crate-guiver-0.0.1 (c (n "guiver") (v "0.0.1") (d (list (d (n "druid-shell") (r "^0") (d #t) (k 0)))) (h "1k5hcvr3g58jzm69gzfcnb2qf0grr9ah1zqmy9fd0qvdwx3aw3vq")))

(define-public crate-guiver-0.1.0 (c (n "guiver") (v "0.1.0") (d (list (d (n "druid-shell") (r "^0") (d #t) (k 0)))) (h "10idd2fd6w4z3wyffjx14mdic6zs6czx4b0vx0dhd1q5alvfs95s")))

