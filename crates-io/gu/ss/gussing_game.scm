(define-module (crates-io gu ss gussing_game) #:use-module (crates-io))

(define-public crate-gussing_game-0.1.0 (c (n "gussing_game") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0dn7w01k1xsa2dkjkx2yibhh3pq98c9k9m6qw6wzg4y28629sr0h")))

(define-public crate-gussing_game-0.1.1 (c (n "gussing_game") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0xlgp65zzbg9mgkgg210zwyavkjcaryhmdbb0jh7qff9gw3i82y2")))

