(define-module (crates-io gu t- gut-lib) #:use-module (crates-io))

(define-public crate-gut-lib-0.1.0 (c (n "gut-lib") (v "0.1.0") (d (list (d (n "dirs") (r "^3") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7") (d #t) (k 0)))) (h "069k4xpp6zx6s2dvsnj9hb4im9dx8w42wa6j52qfm1ypwj541scb")))

(define-public crate-gut-lib-0.1.1 (c (n "gut-lib") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.20") (f (quote ("event-stream"))) (d #t) (k 0)) (d (n "dirs") (r "^3") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7") (d #t) (k 0)))) (h "1a3xci8gmn1i5d6a9py8nlji97pw3a815rfi502sxg0b7i1dnc6g")))

