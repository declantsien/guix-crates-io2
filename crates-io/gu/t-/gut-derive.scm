(define-module (crates-io gu t- gut-derive) #:use-module (crates-io))

(define-public crate-gut-derive-0.1.0 (c (n "gut-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0532nmb5vc5bzfpfyq177dia5s5fip3pa2fnnlvr624s7fb5zl84")))

(define-public crate-gut-derive-0.5.0 (c (n "gut-derive") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00fy5rzdanpmrib47mzr3qjiscd82kl0fhlzmfxr89vy0dymb92g")))

