(define-module (crates-io gu sk gusket-codegen) #:use-module (crates-io))

(define-public crate-gusket-codegen-0.1.0 (c (n "gusket-codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0zi96fayngp3662ddrf8hmkmcyi77lb5ngq9h87f6jlx7i535gww")))

