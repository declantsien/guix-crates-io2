(define-module (crates-io gu er guerrilla) #:use-module (crates-io))

(define-public crate-guerrilla-0.0.0 (c (n "guerrilla") (v "0.0.0") (h "0gk5qiagfddh6c9ylbwm01h5c74rdhda32x1ivdiqjrs9g414ak3")))

(define-public crate-guerrilla-0.1.0 (c (n "guerrilla") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (t "cfg(any(unix, macos))") (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("memoryapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1bqik94ys8xz3c2d77nrhbq3qr3fq7izqyvklq849v3w7agv272a")))

(define-public crate-guerrilla-0.1.1 (c (n "guerrilla") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (t "cfg(any(unix, macos))") (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("memoryapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1x4sqycfwlcpslyw6rgbhay8ydm0i815x5vr1j4iy03p49hycm9a")))

(define-public crate-guerrilla-0.1.2 (c (n "guerrilla") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (t "cfg(any(unix, macos))") (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("memoryapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0dwck4yhgbz1b48pngcz2wdy2jab614j82lp972v22cb90iwii7x")))

(define-public crate-guerrilla-0.1.3 (c (n "guerrilla") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (t "cfg(any(unix, macos))") (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("memoryapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "104a8pbfh115f3hnjl49xnjz8ygv7bh7qk92qzl7xl0alpdm04vf")))

(define-public crate-guerrilla-0.1.4 (c (n "guerrilla") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 2)) (d (n "libc") (r "^0.2.43") (d #t) (t "cfg(any(unix, macos))") (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("memoryapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "00m8ri2mq4z6qhg9238kqjy9ygsjwqr74hhixppfz475772w0yis")))

