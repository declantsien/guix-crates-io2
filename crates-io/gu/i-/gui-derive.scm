(define-module (crates-io gu i- gui-derive) #:use-module (crates-io))

(define-public crate-gui-derive-0.0.0 (c (n "gui-derive") (v "0.0.0") (h "1d2ajvfr52xfm195rznnljd5cjvz4a1fkr16bl215szfidl4bdzz")))

(define-public crate-gui-derive-0.1.0 (c (n "gui-derive") (v "0.1.0") (d (list (d (n "gui") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("derive" "parsing" "printing"))) (k 0)))) (h "1gk9slnsc9268jidwpybhjzscv58w4j3snzcyxx2ncyvn7iqcl79")))

(define-public crate-gui-derive-0.1.1 (c (n "gui-derive") (v "0.1.1") (d (list (d (n "gui") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("derive" "parsing" "printing"))) (k 0)))) (h "10gny5vb7x2zxkrqm10m4jq3zn4vhl4zzwz0cmp7v61571x4ppj5")))

(define-public crate-gui-derive-0.2.0 (c (n "gui-derive") (v "0.2.0") (d (list (d (n "gui") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("derive" "parsing" "printing"))) (k 0)))) (h "0ndlqzaz5n2dczn9cnsw0wf75ivlbdr3yaiq4kmak3dbml3vl2i8")))

(define-public crate-gui-derive-0.2.1 (c (n "gui-derive") (v "0.2.1") (d (list (d (n "gui") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("derive" "parsing" "printing"))) (k 0)))) (h "1jqmq73g25qzfxmxdrv1ch0zff06vpgjmhf646gqxf9mgalaylm5")))

(define-public crate-gui-derive-0.2.2 (c (n "gui-derive") (v "0.2.2") (d (list (d (n "gui") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive" "parsing" "printing"))) (k 0)))) (h "1vfdrfpsg79a1zmi8gby18fj3d52dm68hb66pvbk2h705kgbkhgz")))

(define-public crate-gui-derive-0.1.0-alpha.0 (c (n "gui-derive") (v "0.1.0-alpha.0") (d (list (d (n "gui") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("derive" "parsing" "printing"))) (k 0)))) (h "0klsna9sil42si6rgw2dik47m9p6gzcn6wc9iw0632nsvfrl2pw7")))

(define-public crate-gui-derive-0.1.0-alpha.1 (c (n "gui-derive") (v "0.1.0-alpha.1") (d (list (d (n "gui") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("derive" "parsing" "printing"))) (k 0)))) (h "0m1rv0kxid9ll2ym3w73flvwk47y4sc2n73b89fkhhya00z941dj")))

(define-public crate-gui-derive-0.1.0-alpha.2 (c (n "gui-derive") (v "0.1.0-alpha.2") (d (list (d (n "gui") (r "^0.1.0-alpha.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("derive" "parsing" "printing"))) (k 0)))) (h "08drj5qcg1gqi66sai8d6jvf56pkl6kbkbmyg78q9c4ky5nma3vy")))

(define-public crate-gui-derive-0.1.0-alpha.3 (c (n "gui-derive") (v "0.1.0-alpha.3") (d (list (d (n "gui") (r "^0.1.0-alpha.3") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("derive" "parsing" "printing"))) (k 0)))) (h "0zk30afknyma1c0b6wgba2m7ci1b4knwrilarcb1afj4k76vm9by")))

(define-public crate-gui-derive-0.3.0 (c (n "gui-derive") (v "0.3.0") (d (list (d (n "gui") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("clone-impls" "derive" "parsing" "printing"))) (k 0)))) (h "0abgyiaj5af4cz8fijnwhndgrc5zphx2qmpa2hb9ccpy9bm439f8")))

(define-public crate-gui-derive-0.4.0 (c (n "gui-derive") (v "0.4.0") (d (list (d (n "gui") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("clone-impls" "derive" "extra-traits" "parsing" "printing"))) (k 0)))) (h "02ij7rmxcl9rg191bj69vm7401hv9w3zlir9wnpr030fxhz6yp27")))

(define-public crate-gui-derive-0.5.0 (c (n "gui-derive") (v "0.5.0") (d (list (d (n "gui") (r "^0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("clone-impls" "derive" "extra-traits" "parsing" "printing"))) (k 0)))) (h "0l226s6p511qbmffmx77lzj6qgj44hd9rm3w8mvprpf8b6sfvhs7")))

(define-public crate-gui-derive-0.6.0-alpha.0 (c (n "gui-derive") (v "0.6.0-alpha.0") (d (list (d (n "gui") (r "=0.6.0-alpha.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("clone-impls" "derive" "extra-traits" "parsing" "printing"))) (k 0)))) (h "1nsfgfbny1ya9i8rzsrmwf87falxn0kr279pfhyi99969v8cs6zq")))

(define-public crate-gui-derive-0.6.0-alpha.1 (c (n "gui-derive") (v "0.6.0-alpha.1") (d (list (d (n "async-trait") (r "^0.1.41") (d #t) (k 2)) (d (n "gui") (r "=0.6.0-alpha.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("clone-impls" "derive" "extra-traits" "parsing" "printing"))) (k 0)) (d (n "tokio") (r "^0.3.1") (f (quote ("macros" "rt"))) (k 2)))) (h "1gfdlp13b4pac07mjk8hcnd8j7fpw9jq9fwiival6p55iwb4xsdq")))

(define-public crate-gui-derive-0.6.0-alpha.2 (c (n "gui-derive") (v "0.6.0-alpha.2") (d (list (d (n "async-trait") (r "^0.1.41") (d #t) (k 2)) (d (n "gui") (r "=0.6.0-alpha.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("clone-impls" "derive" "extra-traits" "parsing" "printing"))) (k 0)) (d (n "tokio") (r "^0.3.1") (f (quote ("macros" "rt"))) (k 2)))) (h "10mqhif0xj2pgmv8x4pqnwmwly3mk9dz5yhfzh259mjang2ihy3s")))

(define-public crate-gui-derive-0.6.0 (c (n "gui-derive") (v "0.6.0") (d (list (d (n "async-trait") (r "^0.1.41") (d #t) (k 2)) (d (n "gui") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("clone-impls" "derive" "extra-traits" "parsing" "printing"))) (k 0)) (d (n "tokio") (r "^0.3.1") (f (quote ("macros" "rt"))) (k 2)))) (h "0qfgjia177z7i4n78a33xl4icrpbqasr8qr0bkm2lqc7npa46hv9")))

(define-public crate-gui-derive-0.6.1 (c (n "gui-derive") (v "0.6.1") (d (list (d (n "async-trait") (r "^0.1.41") (d #t) (k 2)) (d (n "gui") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("clone-impls" "derive" "extra-traits" "parsing" "printing"))) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt"))) (k 2)))) (h "11gib4w4smcx4id7s6rv7b8h55dpndrpsxz3bcwxpjfih5fmpc3m")))

(define-public crate-gui-derive-0.6.2 (c (n "gui-derive") (v "0.6.2") (d (list (d (n "async-trait") (r "^0.1.41") (d #t) (k 2)) (d (n "gui") (r "^0.6.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("clone-impls" "derive" "extra-traits" "parsing" "printing"))) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt"))) (k 2)))) (h "15s4lmy9pjmz1yi4kxmf34i279a508nf3l5bak87szwg76c1r1lg")))

