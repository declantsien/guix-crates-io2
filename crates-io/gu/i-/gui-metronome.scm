(define-module (crates-io gu i- gui-metronome) #:use-module (crates-io))

(define-public crate-gui-metronome-1.1.1 (c (n "gui-metronome") (v "1.1.1") (d (list (d (n "fruitbasket") (r "^0.10") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "iced") (r "^0.9") (f (quote ("smol"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rodio") (r "^0.17") (d #t) (k 0)))) (h "0kqjvrc1sgpwbfn9d4nm3widl9b2xkmr3fjzpcknmz38apxw36qf") (y #t)))

(define-public crate-gui-metronome-1.1.2 (c (n "gui-metronome") (v "1.1.2") (d (list (d (n "fruitbasket") (r "^0.10") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "iced") (r "^0.9") (f (quote ("smol"))) (d #t) (k 0)) (d (n "iced_native") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rodio") (r "^0.17") (d #t) (k 0)))) (h "0fncn7f1y1yzx1pfs45xsgysj7h73y57bn215ya21zba4k8lnxav") (y #t)))

