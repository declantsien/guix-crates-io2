(define-module (crates-io gu ac guacamole) #:use-module (crates-io))

(define-public crate-guacamole-0.1.0 (c (n "guacamole") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0vwr8qcjzn8jp3zkq41j6n82zzmd4vkpxwq91j6ykbv7dizp1jar")))

(define-public crate-guacamole-0.2.0 (c (n "guacamole") (v "0.2.0") (d (list (d (n "arrrg") (r "^0.1.0") (d #t) (k 0)) (d (n "arrrg_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1x9jhp9whd2wxalyf6psvpg5hnx4mg766wqzf9xzin0c1zlmywdy")))

(define-public crate-guacamole-0.2.1 (c (n "guacamole") (v "0.2.1") (d (list (d (n "arrrg") (r "^0.1") (d #t) (k 0)) (d (n "arrrg_derive") (r "^0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "00i4mp68g748bhmhb01kyydq1bqcdlchcbq2r7r15l1v69hzmnm9")))

(define-public crate-guacamole-0.3.0 (c (n "guacamole") (v "0.3.0") (d (list (d (n "arrrg") (r "^0.2") (d #t) (k 0)) (d (n "arrrg_derive") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1bhgcpy8pdgmvj7svyd0mhz8rq8hxhdczl5k75wj5sy9f5ydc99n")))

(define-public crate-guacamole-0.4.0 (c (n "guacamole") (v "0.4.0") (d (list (d (n "arrrg") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "arrrg_derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "getopts") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1n198zdsgbj1xq4kdivy7bzrhc05hh0nml9yhcjyib0g5rbp7bm9") (f (quote (("default" "binaries") ("binaries" "arrrg" "arrrg_derive" "getopts"))))))

(define-public crate-guacamole-0.5.0 (c (n "guacamole") (v "0.5.0") (d (list (d (n "arrrg") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "arrrg_derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "getopts") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1fzh8h5ic3blp9f9chd65qydmiffi4ah29lr42gavhlvdrrvbaj7") (f (quote (("default" "binaries")))) (s 2) (e (quote (("binaries" "dep:arrrg" "dep:arrrg_derive" "dep:getopts"))))))

(define-public crate-guacamole-0.6.0 (c (n "guacamole") (v "0.6.0") (d (list (d (n "arrrg") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "arrrg_derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "getopts") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0xx5km7k2a7f5qasbkywc49ab5vam0ybv5janxiac9mr38qa6lch") (f (quote (("default" "binaries")))) (s 2) (e (quote (("binaries" "dep:arrrg" "dep:arrrg_derive" "dep:getopts"))))))

(define-public crate-guacamole-0.6.1 (c (n "guacamole") (v "0.6.1") (d (list (d (n "arrrg") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "arrrg_derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "getopts") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0f4hr81v5s9a5mr4wwq9iasv6q9541w15pa7fv20s8s9anj6cm1c") (f (quote (("default" "binaries")))) (s 2) (e (quote (("binaries" "dep:arrrg" "dep:arrrg_derive" "dep:getopts"))))))

