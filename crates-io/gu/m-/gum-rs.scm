(define-module (crates-io gu m- gum-rs) #:use-module (crates-io))

(define-public crate-gum-rs-0.2.0 (c (n "gum-rs") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "configparser") (r "^3.0.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "term-table") (r "^1.3.2") (d #t) (k 0)))) (h "0259s8j8mdi1kyh4vkq6y0085rhncvhsdn89zrm43s9x6p790pkh")))

