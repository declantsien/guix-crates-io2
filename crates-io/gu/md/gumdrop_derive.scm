(define-module (crates-io gu md gumdrop_derive) #:use-module (crates-io))

(define-public crate-gumdrop_derive-0.1.0 (c (n "gumdrop_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0fs225bapmf793h8ghppyxvdww4zrdwsnxbnri9ld8mry1d8rwnz")))

(define-public crate-gumdrop_derive-0.2.0 (c (n "gumdrop_derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1kfi32hz90ddfcjqzc9x65r5flz6gr33wixg9pipcj2wnxligx43")))

(define-public crate-gumdrop_derive-0.3.0 (c (n "gumdrop_derive") (v "0.3.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "086hm84il45ijph8kbfhcbyvvm7jxnsxh28w11h56sn1cjxkff2z")))

(define-public crate-gumdrop_derive-0.4.0 (c (n "gumdrop_derive") (v "0.4.0") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0ysl2icw177qbd72a0vny5pmihinjba8wf1862apbrwa2ajf6kcj")))

(define-public crate-gumdrop_derive-0.4.1 (c (n "gumdrop_derive") (v "0.4.1") (d (list (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (d #t) (k 0)))) (h "0c2y31qsp9n3s51s6p1cywkgigadhmy5adaqaidsv4nl7l3na66m")))

(define-public crate-gumdrop_derive-0.5.0 (c (n "gumdrop_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1gfxl7innnk60459plhsjkyr251hl3knmljwbw3dj136zmikdi04")))

(define-public crate-gumdrop_derive-0.6.0 (c (n "gumdrop_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1xzi26w0yfm3jcwn4kgn5qzdijgqzjk4wzh2wa37a3q95j74irfs")))

(define-public crate-gumdrop_derive-0.7.0 (c (n "gumdrop_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1mrm1h4n13b3n79d1215jq9q9d6sgcvfzdb8i5mcmds0vvj4qich") (f (quote (("default_expr" "syn/full") ("default"))))))

(define-public crate-gumdrop_derive-0.8.0 (c (n "gumdrop_derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "01cdc7w5wf1g9i04ykcssczjmmnl6jky47a648sp710df5yg0pli") (f (quote (("default_expr" "syn/full") ("default"))))))

(define-public crate-gumdrop_derive-0.8.1 (c (n "gumdrop_derive") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "17d91ai4p9f9cwhqqnyivw9yi7prl9xzpaqq3a1yfxwx8k9rp7vj") (f (quote (("default_expr" "syn/full") ("default"))))))

