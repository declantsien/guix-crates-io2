(define-module (crates-io gu rg gurgle) #:use-module (crates-io))

(define-public crate-gurgle-0.1.0 (c (n "gurgle") (v "0.1.0") (d (list (d (n "nanorand") (r "^0.6") (f (quote ("std" "tls" "wyrand"))) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0wkpkdvxjn9g8fa1f29zj0dp6l0w7lmwq5wijf72hwp4v73jfdrb") (f (quote (("detail") ("default" "detail"))))))

(define-public crate-gurgle-0.2.0 (c (n "gurgle") (v "0.2.0") (d (list (d (n "nanorand") (r "^0.6") (f (quote ("std" "tls" "wyrand"))) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1hxr8h4q6957y1ksgrc4sigk8f7mp9x2slfp4f1q98wxghllvnn1") (f (quote (("detail") ("default" "detail"))))))

(define-public crate-gurgle-0.3.0 (c (n "gurgle") (v "0.3.0") (d (list (d (n "nanorand") (r "^0.6") (f (quote ("std" "tls" "wyrand"))) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1c91ax9lqlxvsa22s2gpl6bzfpgvphm7yf0zhz2hgywrr0j448w4") (f (quote (("detail") ("default" "detail"))))))

(define-public crate-gurgle-0.4.0 (c (n "gurgle") (v "0.4.0") (d (list (d (n "nanorand") (r "^0.6") (f (quote ("std" "tls" "wyrand"))) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "18sdr07vi7vmi90yl6jdhr7wsx2wazp51jk6mlx1znm63axcf1zw") (f (quote (("detail") ("default" "detail"))))))

(define-public crate-gurgle-0.5.0 (c (n "gurgle") (v "0.5.0") (d (list (d (n "nanorand") (r "^0.6") (f (quote ("std" "tls" "wyrand"))) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1hvdrsmabbfpxnymysc956wf44pdla1kkcv355009pzfawxm8kri") (f (quote (("detail") ("default" "detail"))))))

