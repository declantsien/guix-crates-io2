(define-module (crates-io gu id guidgen) #:use-module (crates-io))

(define-public crate-guidgen-1.0.0 (c (n "guidgen") (v "1.0.0") (d (list (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0x4v7kp92b6pghf2g97j4phr4a4is85q4b1f0rbkal86bnka3syz")))

(define-public crate-guidgen-1.1.0 (c (n "guidgen") (v "1.1.0") (d (list (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "02b7as5m58wv63yblmpcysv8iaqqsqvw4fd4f7gxvm2qqzdix9xl")))

(define-public crate-guidgen-1.1.1 (c (n "guidgen") (v "1.1.1") (d (list (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ca98vw4vgr7adh08wcwc7dqsxc9gaf65pzry9155j0bybw6wn8m")))

