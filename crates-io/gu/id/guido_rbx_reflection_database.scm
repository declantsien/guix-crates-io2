(define-module (crates-io gu id guido_rbx_reflection_database) #:use-module (crates-io))

(define-public crate-guido_rbx_reflection_database-0.2.6+roblox-557 (c (n "guido_rbx_reflection_database") (v "0.2.6+roblox-557") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rbx_reflection") (r "^4.2.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)))) (h "1d8g31ivp5hmhx2an7h0dvn5s04g87nkxa9510pn47r1l652i9vf") (y #t)))

