(define-module (crates-io gu id guid-parser) #:use-module (crates-io))

(define-public crate-guid-parser-0.1.0 (c (n "guid-parser") (v "0.1.0") (d (list (d (n "chomp") (r "^0.3.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "1ba825ngrs0501lvzdbsqdz6y5b3xaqrxzvckscj704286savixb")))

