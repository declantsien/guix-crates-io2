(define-module (crates-io gu id guid_win) #:use-module (crates-io))

(define-public crate-guid_win-0.1.0-alpha1 (c (n "guid_win") (v "0.1.0-alpha1") (d (list (d (n "comedy") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "lpwstr") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("combaseapi" "guiddef"))) (d #t) (k 0)))) (h "0lk856sqfagdcji1x7jfc2g0sdgwh574amm4m4g4jl7w5iklzsh5") (f (quote (("guid_serde" "serde" "serde_derive")))) (y #t)))

(define-public crate-guid_win-0.1.0-alpha2 (c (n "guid_win") (v "0.1.0-alpha2") (d (list (d (n "comedy") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("combaseapi" "guiddef"))) (d #t) (k 0)))) (h "028s3rpcrlz60p6n5xqpfgjmja1ypj1wi87y8swhx0f8y922pgws") (f (quote (("guid_serde" "serde" "serde_derive"))))))

(define-public crate-guid_win-0.1.0-alpha3 (c (n "guid_win") (v "0.1.0-alpha3") (d (list (d (n "comedy") (r "^0.1.0-alpha2") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("combaseapi" "guiddef"))) (d #t) (k 0)))) (h "1h9z8hp858b5a2nvbp2gfqp639m1sn2nhbng05011zxsyignmd8h") (f (quote (("guid_serde" "serde" "serde_derive"))))))

(define-public crate-guid_win-0.1.0 (c (n "guid_win") (v "0.1.0") (d (list (d (n "comedy") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("combaseapi" "guiddef"))) (d #t) (k 0)))) (h "12zmypw1sk73dg0j1y0ymf98lg8m5c646ajc9xcbcdayrj31c9l7") (f (quote (("guid_serde" "serde" "serde_derive"))))))

(define-public crate-guid_win-0.2.0 (c (n "guid_win") (v "0.2.0") (d (list (d (n "comedy") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("combaseapi" "guiddef"))) (d #t) (k 0)))) (h "0fqqdxgy4fmkx30m1a9q55jlhi2ghc4j0vriwjs9hysmgbl4nzyq") (f (quote (("guid_serde" "serde" "serde_derive"))))))

