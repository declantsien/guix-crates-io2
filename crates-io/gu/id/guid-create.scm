(define-module (crates-io gu id guid-create) #:use-module (crates-io))

(define-public crate-guid-create-0.0.1 (c (n "guid-create") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "guid") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "0j1wqlgmvcrvw4qiva5fvy6xa76yf8i4q1i4kch01ifxyl6hlz5v")))

(define-public crate-guid-create-0.1.0 (c (n "guid-create") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "chomp") (r "^0.3") (d #t) (k 0)) (d (n "guid") (r "^0.1") (d #t) (k 0)) (d (n "guid-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "0nm4lhjih7hq7i8j52vvvl5gczgq72az8l54ixnrljfljs70gz8s")))

(define-public crate-guid-create-0.1.1 (c (n "guid-create") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "chomp") (r "^0.3") (d #t) (k 0)) (d (n "guid") (r "^0.1") (d #t) (k 0)) (d (n "guid-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)))) (h "03f42h4ghcdcvjc0v4pis7p7snnybpz9h3srmdk222d6yxxj1spw")))

(define-public crate-guid-create-0.2.0 (c (n "guid-create") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chomp") (r "^0.3.1") (d #t) (k 0)) (d (n "guid") (r "^0.1.0") (d #t) (k 0)) (d (n "guid-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "139h5r2mj0iqg9mcqi2qd6lm1xxark46aiykz5m3w6crqqspwiri")))

(define-public crate-guid-create-0.3.0 (c (n "guid-create") (v "0.3.0") (d (list (d (n "chomp") (r "^0.3.1") (d #t) (k 0)) (d (n "guid-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "0czd2mawqm0hkkbfw7aks8dkl900xb6j365jhynj40fwypwx9bpz") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-guid-create-0.3.1 (c (n "guid-create") (v "0.3.1") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "102r6kq7x5q5qjh6by26qha292sp9jwdcdfjk1c6iqrgglb36faj") (s 2) (e (quote (("serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-guid-create-0.4.0 (c (n "guid-create") (v "0.4.0") (d (list (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (d #t) (t "cfg(windows)") (k 0)))) (h "1fljx4dsfmbjak03s978pjmayyyk4qjy6yyy83v1nlgakjfikfhq") (s 2) (e (quote (("serde" "dep:serde") ("bytemuck" "dep:bytemuck")))) (r "1.60.0")))

