(define-module (crates-io gu id guidon-cli) #:use-module (crates-io))

(define-public crate-guidon-cli-0.2.3 (c (n "guidon-cli") (v "0.2.3") (d (list (d (n "guidon") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "log4rs") (r "^0.11.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "0zm9isbslxq4v7s1z870vf2ikz1y50v8mpyc2if4hzx22ra0ji0w")))

(define-public crate-guidon-cli-0.2.4 (c (n "guidon-cli") (v "0.2.4") (d (list (d (n "guidon") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "log4rs") (r "^0.11.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1wv8bpd9d8y7046dbhf93r36j55fp4vxkk1z5zyixx5z7873f327")))

(define-public crate-guidon-cli-0.2.6 (c (n "guidon-cli") (v "0.2.6") (d (list (d (n "guidon") (r "^0.2.6") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "log4rs") (r "^0.11.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1nvc6w83k43zi65xwhl3kdbv3d2990yvjij94bdwikfb41d15lf4")))

(define-public crate-guidon-cli-0.2.7 (c (n "guidon-cli") (v "0.2.7") (d (list (d (n "guidon") (r "^0.2.7") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "gumdrop_derive") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "0r2rr4ibksnwkk12k3ap4kq4xjrfxb9x0w4lp9w07sgx3sjl6pzn")))

(define-public crate-guidon-cli-0.2.8 (c (n "guidon-cli") (v "0.2.8") (d (list (d (n "guidon") (r "^0.2.8") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "gumdrop_derive") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6.0") (d #t) (k 0)))) (h "07jwyr6ryzjvdw7kzgl891wi8kp9y080a5k3a9w8vg5d0jwpavpx")))

(define-public crate-guidon-cli-0.2.9 (c (n "guidon-cli") (v "0.2.9") (d (list (d (n "guidon") (r "^0.2.9") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "gumdrop_derive") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 0)))) (h "0zjr0m2q16a4v5sc2fqyk125l7qq54bz8q7aznw8mavq5clwnivc")))

(define-public crate-guidon-cli-0.2.10 (c (n "guidon-cli") (v "0.2.10") (d (list (d (n "guidon") (r "^0.2.10") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "gumdrop_derive") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 0)))) (h "075ziipl5zl5583wia4l7wl08q6gc86mlq42vxma02zn7b5yg2ms")))

(define-public crate-guidon-cli-0.3.0 (c (n "guidon-cli") (v "0.3.0") (d (list (d (n "guidon") (r "^0.3.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "gumdrop_derive") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 0)))) (h "1f6hmm5mf9581fy0k7h8w1y52a4243g63y3ddv5vicpqh1n6rfqy")))

(define-public crate-guidon-cli-0.4.0 (c (n "guidon-cli") (v "0.4.0") (d (list (d (n "guidon") (r "^0.4.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "gumdrop_derive") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 0)))) (h "1fyjznms1xfk4hvr552xfvmgp2b221i02bldj07kg9xmfvqyn07l")))

(define-public crate-guidon-cli-0.4.1 (c (n "guidon-cli") (v "0.4.1") (d (list (d (n "guidon") (r "^0.4.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "gumdrop_derive") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11.0") (d #t) (k 0)))) (h "0snkpnhvzc40hbs91424wgq32lbnzkvlr485ig3qgjxd051ypp2q")))

(define-public crate-guidon-cli-0.4.2 (c (n "guidon-cli") (v "0.4.2") (d (list (d (n "guidon") (r "^0.4.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "gumdrop_derive") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.1") (d #t) (k 0)))) (h "1nn5izcry4ka5jqr3a9sl7igwc0rf6bcas41f4x0y57ch5l3da53")))

(define-public crate-guidon-cli-0.4.3 (c (n "guidon-cli") (v "0.4.3") (d (list (d (n "guidon") (r "^0.4.0") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "gumdrop_derive") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.1") (d #t) (k 0)))) (h "0l2dgnlqnh38i3m7znda1pp29s7iyvcvgx9zb45zlwsvy9j5bafn")))

(define-public crate-guidon-cli-0.4.4 (c (n "guidon-cli") (v "0.4.4") (d (list (d (n "guidon") (r "^0.4.1") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "gumdrop_derive") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5.1") (d #t) (k 0)))) (h "05fgs222k1bm5sql3d8z9z7k65icdxb63nrjw8jjwpm815jsn7m6")))

