(define-module (crates-io gu id guido_rbx_xml) #:use-module (crates-io))

(define-public crate-guido_rbx_xml-0.12.5 (c (n "guido_rbx_xml") (v "0.12.5") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "insta") (r "^1.14.1") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rbx_dom_weak") (r "^2.2.0") (d #t) (k 0)) (d (n "rbx_reflection") (r "^4.2.0") (d #t) (k 0)) (d (n "rbx_reflection_database") (r "^0.2.6") (d #t) (k 0) (p "guido_rbx_reflection_database")) (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "0s314wn199pljdp9dssvwgf30ixghpnx9rqn79h0ladhc8blrq1a") (y #t)))

