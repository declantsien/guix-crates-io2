(define-module (crates-io gu id guided_digraph) #:use-module (crates-io))

(define-public crate-guided_digraph-0.1.1 (c (n "guided_digraph") (v "0.1.1") (d (list (d (n "disjoint-sets") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "time") (r "^0.3.14") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "01cb15wirrh11xx22fm6zh49kg1pdvbrjynqkasm764hyi2z195i")))

