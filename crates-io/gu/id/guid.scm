(define-module (crates-io gu id guid) #:use-module (crates-io))

(define-public crate-guid-0.1.0 (c (n "guid") (v "0.1.0") (d (list (d (n "chomp") (r "^0.3.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "guid-macro-impl") (r "^0.1.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "guid-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "1niqi8rhdlylvxw8qvgrxsg8rsrvfnz4dswswabpav12kd6wd4g6")))

