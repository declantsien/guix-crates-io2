(define-module (crates-io gu id guid-macro-impl) #:use-module (crates-io))

(define-public crate-guid-macro-impl-0.1.0 (c (n "guid-macro-impl") (v "0.1.0") (d (list (d (n "chomp") (r "^0.3.1") (d #t) (k 0)) (d (n "guid-parser") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.12") (d #t) (k 0)))) (h "05h835qmlylqb6jx4pg21jb50fhi970z3xn0vsjvawv095y0zm88")))

