(define-module (crates-io gu id guido_rbx_types) #:use-module (crates-io))

(define-public crate-guido_rbx_types-1.4.2 (c (n "guido_rbx_types") (v "1.4.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "insta") (r "^1.14.1") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0dlydijmsis64d1zvv8lqx02gbv9ijfjln306h6y58nj8ksmw0r7") (y #t)))

