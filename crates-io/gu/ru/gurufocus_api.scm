(define-module (crates-io gu ru gurufocus_api) #:use-module (crates-io))

(define-public crate-gurufocus_api-0.1.0 (c (n "gurufocus_api") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1l64vp6j6ik563s3cyz0lv6d8fgg7hrqqi4sy9vi3hnpzw0hiy4p")))

(define-public crate-gurufocus_api-0.1.1 (c (n "gurufocus_api") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "002spamjisb3wqyqwag3az2da3j2shd48d8l4bcvkwymf8x2h9f2")))

(define-public crate-gurufocus_api-0.1.2 (c (n "gurufocus_api") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "018h16d41j726kkad1y3fp4vlvnmjxnaq1ziklqzgzz4f466pr4q")))

(define-public crate-gurufocus_api-0.1.3 (c (n "gurufocus_api") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "078smy2pql4f9jdh1mxvp08d2fzwcfx7cpmwn1hsw6avdf17q6iq")))

(define-public crate-gurufocus_api-0.1.4 (c (n "gurufocus_api") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05icvyi2ycwb7cgi5bklk30rb2703ggw7pi5w86s8x6p5nhdqsqr")))

(define-public crate-gurufocus_api-0.1.5 (c (n "gurufocus_api") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0p95bzjg33dhp03n7hh1jrnllph6zrqk7hpp8cpxrjkqzbmkvbxd")))

(define-public crate-gurufocus_api-0.2.0 (c (n "gurufocus_api") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13gd7ann595y6dxsq1xfciljns1lzz9spz5j48naxqznrwwdwn8w")))

(define-public crate-gurufocus_api-0.2.1 (c (n "gurufocus_api") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fvnc5x7dgnbq68vanpy3blk37xsvqs2b52zf54yfy8b4p4h1nx0")))

(define-public crate-gurufocus_api-0.2.2 (c (n "gurufocus_api") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03ii5mh5g0xww0cx55plaf53z6k42dl7a8npfvlhrpz1pajiyw2l")))

(define-public crate-gurufocus_api-0.2.3 (c (n "gurufocus_api") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fd91vsns0xnfzz5bqxl8z1qy27cfxn7iqfaqvbknlxw1kr7djmh")))

(define-public crate-gurufocus_api-0.2.4 (c (n "gurufocus_api") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06ljxidrr4iyq2b90js8hzbwmjg3kdkvdiyldhl3hxvc0zac04x7")))

(define-public crate-gurufocus_api-0.3.0 (c (n "gurufocus_api") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "061y7azl4raamxicy9awdzkhbj1a0i5bp20bhncgbby7qz9y9y9p")))

(define-public crate-gurufocus_api-0.3.1 (c (n "gurufocus_api") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15b1ahf6ydivvz23rqkf4clfmmkrgyffy37pmz6nsgk4hs9ayi68")))

(define-public crate-gurufocus_api-0.3.2 (c (n "gurufocus_api") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rdymh9xn57hqirwf2f042x59gy4c8ax4n8gavffrgxn5q2hzssc")))

(define-public crate-gurufocus_api-0.4.0 (c (n "gurufocus_api") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros"))) (d #t) (k 0)))) (h "021w5d7by1iarf9kxkmwsa90iy8d0bshx64zf6s37nvs7q2jzpv4")))

(define-public crate-gurufocus_api-0.4.1 (c (n "gurufocus_api") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "tokio-compat-02") (r "^0.1") (d #t) (k 0)))) (h "0i86rk8b1bfl8ik6hfba9cl63dks39lm7q6iab6hx3959w71hgpn")))

(define-public crate-gurufocus_api-0.4.2 (c (n "gurufocus_api") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "tokio-compat-02") (r "^0.1") (d #t) (k 0)))) (h "10vrpmx5c8x7ss1kvf9bxpbc3yyn6p6jj6fcmmiwfzfqp3sfjkka")))

(define-public crate-gurufocus_api-0.5.0 (c (n "gurufocus_api") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "tokio-compat-02") (r "^0.2.0") (d #t) (k 0)))) (h "1vb0zgfk43g52x0lg32dcbcvr0adyclkwwj0vhcibw08in8vq7d6")))

(define-public crate-gurufocus_api-0.6.0 (c (n "gurufocus_api") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.14") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "tokio-compat-02") (r "^0.2.0") (d #t) (k 0)))) (h "0wnzhhms739vhpamd74gi2z5f7r0sn3i6pq89fa1pc28xf75zpqc")))

(define-public crate-gurufocus_api-0.6.1 (c (n "gurufocus_api") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "tokio-compat-02") (r "^0.2.0") (d #t) (k 0)))) (h "18hsm3r57mkyx3if6i5j3iasvfh7sx2yhz9jcjmws62sjl3vbx15")))

