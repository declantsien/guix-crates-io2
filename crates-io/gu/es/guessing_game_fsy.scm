(define-module (crates-io gu es guessing_game_fsy) #:use-module (crates-io))

(define-public crate-guessing_game_fsy-0.1.0 (c (n "guessing_game_fsy") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0wa3bwr6w3nc3jzadw8218q3qq36rvnppqpyli0bbs7m5pp1fpq5")))

(define-public crate-guessing_game_fsy-0.1.1 (c (n "guessing_game_fsy") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1ij4vz1djjkz2zw2w0gyg7ryc8cqmdwih4v8ckfn747abwabdrpj")))

