(define-module (crates-io gu es guessing_game_2022_02_28) #:use-module (crates-io))

(define-public crate-guessing_game_2022_02_28-0.1.1 (c (n "guessing_game_2022_02_28") (v "0.1.1") (d (list (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "0lvsif3sqnn5wnxj9jd45hxypqwd0bx22106ml24h0nd6ifgpfc5")))

(define-public crate-guessing_game_2022_02_28-0.1.2 (c (n "guessing_game_2022_02_28") (v "0.1.2") (d (list (d (n "rand") (r "^0.4.0") (d #t) (k 0)))) (h "0qzcvp0m98k2fa1wc837qxg1bk8lwxikdiigxcva43fc0v4wjmxc")))

