(define-module (crates-io gu es guessing_game_game) #:use-module (crates-io))

(define-public crate-guessing_game_game-0.1.0 (c (n "guessing_game_game") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "00mqvx864spgg1ybssk3zwyxa24f29izrqvglzmxl7a8128yl2jx")))

(define-public crate-guessing_game_game-0.1.1 (c (n "guessing_game_game") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "15jvy695j9v0xlcdawasbqz4rsr383pcbmajmn9kmjdlxik6bldn")))

