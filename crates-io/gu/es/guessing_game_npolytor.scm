(define-module (crates-io gu es guessing_game_npolytor) #:use-module (crates-io))

(define-public crate-guessing_game_npolytor-1.0.0 (c (n "guessing_game_npolytor") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "124zb1jzh9ws5421glkmw23k4zhy18jl0d1wyrk35nzvl2knfyp3")))

(define-public crate-guessing_game_npolytor-2.0.0 (c (n "guessing_game_npolytor") (v "2.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1cifxav8mj4k8wqi0vwmr7drd2pl5nf4gmx85yym4sdygg93h54n") (y #t)))

