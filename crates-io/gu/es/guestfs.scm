(define-module (crates-io gu es guestfs) #:use-module (crates-io))

(define-public crate-guestfs-0.1.0 (c (n "guestfs") (v "0.1.0") (h "1zx4vkfrxzmq18n84j3cip1ydlb7ya1blfi4qwslyvpqfn0ljwg2")))

(define-public crate-guestfs-0.1.0-pre1 (c (n "guestfs") (v "0.1.0-pre1") (h "12yln76ar824gbglkk2n2ypwj35cd5q0jyd82pjlrl5jz2mflk3q")))

(define-public crate-guestfs-0.1.0-compat1.40.0 (c (n "guestfs") (v "0.1.0-compat1.40.0") (h "01irlv73vws94s6qij789jg4yb8cvb4glprl9sqgs3yz43w3cabw")))

