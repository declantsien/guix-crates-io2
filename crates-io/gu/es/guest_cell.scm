(define-module (crates-io gu es guest_cell) #:use-module (crates-io))

(define-public crate-guest_cell-0.1.0 (c (n "guest_cell") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0cvg2w7r0g04m5h9jdsmmcpcqfp2w0rdcpvb8ki07d12ncbvsam5")))

(define-public crate-guest_cell-0.1.1 (c (n "guest_cell") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "15z7dab5kg9f2zl2l2ghv3bc64yvy1v1sfaz6c6kjr92kqgq5ss0")))

(define-public crate-guest_cell-0.1.2 (c (n "guest_cell") (v "0.1.2") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "092jwlsyrkga6lcpbffdny7nb2lh80c79qgadakz99asfy2qic6m")))

(define-public crate-guest_cell-0.1.3 (c (n "guest_cell") (v "0.1.3") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0y98y7zs180qpn6az1rh31v3i4k770lh3bpg650fic19qjsksl5a")))

(define-public crate-guest_cell-0.1.4 (c (n "guest_cell") (v "0.1.4") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "0rz5qa2fyavpkfkv3mxjnnc5rh0jmk8n4nihp5c5w2h42cczyznq")))

(define-public crate-guest_cell-0.1.5 (c (n "guest_cell") (v "0.1.5") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "11cv3d7dmrd67j77fiphmg5l39jmyr5lr2ldsvm5h63j36rmpxgw")))

