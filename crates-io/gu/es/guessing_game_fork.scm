(define-module (crates-io gu es guessing_game_fork) #:use-module (crates-io))

(define-public crate-guessing_game_fork-0.1.2 (c (n "guessing_game_fork") (v "0.1.2") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "077zql7s89pmmzbwhmyh5hv1nz8cgv9jij0xjprnsx63i9wk9g3r") (y #t)))

(define-public crate-guessing_game_fork-0.1.3 (c (n "guessing_game_fork") (v "0.1.3") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "0lk9172sklihxdrnrgd0id8fwasv46fal2gh4x2sv14zb1bv9ap8")))

