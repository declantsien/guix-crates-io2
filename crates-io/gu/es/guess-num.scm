(define-module (crates-io gu es guess-num) #:use-module (crates-io))

(define-public crate-guess-num-0.1.0 (c (n "guess-num") (v "0.1.0") (d (list (d (n "aoko") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1wfxyxad41fz58z116dnw20gwghylqb2cx3qd07vcs2vc1z4f2sz")))

(define-public crate-guess-num-0.1.1 (c (n "guess-num") (v "0.1.1") (d (list (d (n "aoko") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1s7csc97qca6irmbd04v6qymhfnnhyr5sp8vh896mxwap31xx6jq")))

(define-public crate-guess-num-0.1.2 (c (n "guess-num") (v "0.1.2") (d (list (d (n "aoko") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "14cq93wlc33xb7fxyz9sad1365k3zi02grrdq051q8yvyzl7xi9k")))

(define-public crate-guess-num-0.1.3 (c (n "guess-num") (v "0.1.3") (d (list (d (n "aoko") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1c7as1r876ybmwxjb8q6h2x2r9vzdnwnada655csq6d96p91l7k8")))

