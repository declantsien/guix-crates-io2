(define-module (crates-io gu es guessing_game_crate) #:use-module (crates-io))

(define-public crate-guessing_game_crate-0.1.0 (c (n "guessing_game_crate") (v "0.1.0") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "1kh4ljlpv65r0k74dbj3zp4qsdz7knvsqfksc7xy6hyvp0g1kw2q") (y #t)))

(define-public crate-guessing_game_crate-0.1.1 (c (n "guessing_game_crate") (v "0.1.1") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "0qs686r20l6wzhmwjvjx7d974y01ym347qclx5ca4azx28fzf89h")))

