(define-module (crates-io gu es guessing_game_by_bo) #:use-module (crates-io))

(define-public crate-guessing_game_by_bo-0.1.0 (c (n "guessing_game_by_bo") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1pml0mixfbfi1sg0zpr3drqk4ah27gqbxbjky5zpin01iw8k02fm") (y #t)))

(define-public crate-guessing_game_by_bo-0.1.1 (c (n "guessing_game_by_bo") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1w58q2akb8kabdgg8j2nd1w0yks1fghyq3q0796yn9nrq2v5a737")))

