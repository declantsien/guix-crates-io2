(define-module (crates-io gu es guessing_number) #:use-module (crates-io))

(define-public crate-guessing_number-0.1.0 (c (n "guessing_number") (v "0.1.0") (d (list (d (n "rand") (r "^0.4.6") (d #t) (k 0)))) (h "1c7hcf0rs33mdk9xdhdqfbp9d2i4zj9yp27vs7ybw033h6brn368")))

(define-public crate-guessing_number-0.1.1 (c (n "guessing_number") (v "0.1.1") (d (list (d (n "actix-web") (r "^1.0.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "1l2q647ndw1ayqrqy4nqpv3dccf658qi8kpa90dxsd53g7jwa80s")))

