(define-module (crates-io gu es guessing_game_cloud698) #:use-module (crates-io))

(define-public crate-guessing_game_cloud698-0.1.0 (c (n "guessing_game_cloud698") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "178nw9ybwrvx5bc715j7j6bhnjyp1w9psrgd7qajand4z2dc6lgw") (y #t)))

(define-public crate-guessing_game_cloud698-0.1.1 (c (n "guessing_game_cloud698") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1f3197nlpzy8qdl382py2m2ca0zbjd6wk6x349ynqdxbkl1ay4wc") (y #t)))

