(define-module (crates-io gu es guessing_game) #:use-module (crates-io))

(define-public crate-guessing_game-0.1.0 (c (n "guessing_game") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "0c21kxd0xhq6bdh1nl1py8rrpshnaa2xnmb7hwxzli1011llywvz")))

(define-public crate-guessing_game-0.1.1 (c (n "guessing_game") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "1knagshs87afixlz8h0gzcif7s2pr8vyv2vz5pnfrxicppvzj1zd")))

(define-public crate-guessing_game-0.1.2 (c (n "guessing_game") (v "0.1.2") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "1xrrkzz7adrsy9l81j38xrh11k51zrv673dd5p11pcx1p9c375xv")))

