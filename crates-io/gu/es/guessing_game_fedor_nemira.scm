(define-module (crates-io gu es guessing_game_fedor_nemira) #:use-module (crates-io))

(define-public crate-guessing_game_fedor_nemira-0.1.0 (c (n "guessing_game_fedor_nemira") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0qyx11mr1qr5jpjgbci5p1l0rhd6zqwmw7hk5lj3xpdzqjn4sf05")))

(define-public crate-guessing_game_fedor_nemira-0.1.1 (c (n "guessing_game_fedor_nemira") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0v956zps7g610b0s5kjkljfngr7yp8pdfypgljywji09nc84q7br")))

