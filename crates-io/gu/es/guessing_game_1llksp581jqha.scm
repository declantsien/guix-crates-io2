(define-module (crates-io gu es guessing_game_1llksp581jqha) #:use-module (crates-io))

(define-public crate-guessing_game_1llksp581jqha-0.1.0 (c (n "guessing_game_1llksp581jqha") (v "0.1.0") (d (list (d (n "rand") (r "^0.6.0") (d #t) (k 0)))) (h "0jxrfydk8s6gsc6vhdga155y3zg7y307cps2fjl7him9cp9yh13g") (y #t)))

(define-public crate-guessing_game_1llksp581jqha-0.1.1 (c (n "guessing_game_1llksp581jqha") (v "0.1.1") (d (list (d (n "rand") (r "^0.6.0") (d #t) (k 0)))) (h "0lgk0gl1l6c697rbjg49jsicd4pmafz8b1k87ghz7mmgxz496k4a") (y #t)))

