(define-module (crates-io gu es guessing_game_zix) #:use-module (crates-io))

(define-public crate-guessing_game_zix-0.1.0 (c (n "guessing_game_zix") (v "0.1.0") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "18sj7c9mxcjaww2m7a2j22dxph100c9d7jvd10p6i4gf08y54223")))

(define-public crate-guessing_game_zix-0.1.1 (c (n "guessing_game_zix") (v "0.1.1") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "1bkjjmpcycf3ywcdfp9in95gwymda0b1lwpmin31gw9r3b9niqmz")))

