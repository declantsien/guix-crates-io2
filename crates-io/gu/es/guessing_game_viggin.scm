(define-module (crates-io gu es guessing_game_viggin) #:use-module (crates-io))

(define-public crate-guessing_game_viggin-0.1.0 (c (n "guessing_game_viggin") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "16dam8sb9a9564aj57dxvp00hdgsdhkh72fb3b76ml2mh26f40l3")))

