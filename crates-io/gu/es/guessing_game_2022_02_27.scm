(define-module (crates-io gu es guessing_game_2022_02_27) #:use-module (crates-io))

(define-public crate-guessing_game_2022_02_27-0.1.0 (c (n "guessing_game_2022_02_27") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1vlnixrc7lfbpgvxz7cqqh19dv7nl1lbhj6vd8xsid9axav5zv9m") (y #t)))

(define-public crate-guessing_game_2022_02_27-0.1.1 (c (n "guessing_game_2022_02_27") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "17r6bfgv9ybwc738m9ia3y9z8kvdk2bvjk0aap9arbql04kprrh1") (y #t)))

