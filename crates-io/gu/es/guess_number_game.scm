(define-module (crates-io gu es guess_number_game) #:use-module (crates-io))

(define-public crate-guess_number_game-0.1.0 (c (n "guess_number_game") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "0yplgskdkbpbbyf3s9kqv6zjcjqc3scrrr6s18axxrsiygp5bn6z")))

(define-public crate-guess_number_game-0.1.1 (c (n "guess_number_game") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "1cag6f364a57zc5bdbl29xgdmrs5wlb3kayi8p7k1gli0apq4hd5")))

