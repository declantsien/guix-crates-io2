(define-module (crates-io gu es guessing_game_v2) #:use-module (crates-io))

(define-public crate-guessing_game_v2-0.1.0 (c (n "guessing_game_v2") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0p3qflqk6cl0j3kx3yd3xf4bcyvnci6bzvpdk7yplpf9l7s803jz")))

(define-public crate-guessing_game_v2-1.0.1 (c (n "guessing_game_v2") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1vrb9mibc76fcz8v2xjg7w7gbbyv8479y41makdjwnrz7gaj6v6l")))

