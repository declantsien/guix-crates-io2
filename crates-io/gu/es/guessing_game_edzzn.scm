(define-module (crates-io gu es guessing_game_edzzn) #:use-module (crates-io))

(define-public crate-guessing_game_edzzn-0.1.0 (c (n "guessing_game_edzzn") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1m21b89vgpbgym0irddgrshsjby411gm74n2v47nh4fi765sli4n") (y #t)))

(define-public crate-guessing_game_edzzn-0.1.1 (c (n "guessing_game_edzzn") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1xpfh4033k4279cqzm6f87aq16bjakkyxijrd1ffq3pfbpg035xh")))

