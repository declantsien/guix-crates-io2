(define-module (crates-io gu es guessg) #:use-module (crates-io))

(define-public crate-guessg-0.1.0 (c (n "guessg") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0b0jja2lkimpsh9s100i60h6bjdy2yx23fgz1min3ajmmn7hp4zp")))

(define-public crate-guessg-0.1.1 (c (n "guessg") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1ddgj10vghqgp4x6sk7d3s9z6i0p7pzc700pd5y7ni5y2654dx6n")))

