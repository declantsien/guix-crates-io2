(define-module (crates-io gu es guessing_game-xfossdotcom) #:use-module (crates-io))

(define-public crate-guessing_game-xfossdotcom-0.1.0 (c (n "guessing_game-xfossdotcom") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1n44y8wwm2m4iyp0rzs5fljsfcpajlcrgvivz9gd46dv7xhxfi4f") (y #t)))

(define-public crate-guessing_game-xfossdotcom-0.1.1 (c (n "guessing_game-xfossdotcom") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1izkcwkmr8ci3kmbl3wgdiqwrs60bz404gq42p9wz193d5ph8i4b")))

