(define-module (crates-io gu es guessing_utils) #:use-module (crates-io))

(define-public crate-guessing_utils-1.0.0 (c (n "guessing_utils") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0v8k0pyscgm6c2b4cafy83ww8grn312fwgcpgifn8jd8h93gghdr")))

(define-public crate-guessing_utils-1.0.1 (c (n "guessing_utils") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1fgcc4rc4ilz76x97s8hxcrcj5jgz3a0zkavhxk3w8gk06a20m8c") (y #t)))

(define-public crate-guessing_utils-1.0.2 (c (n "guessing_utils") (v "1.0.2") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0la0b8kk9wxhlhxd0wdq84d23y9rxyc67s196p88aj5bybn044r5")))

(define-public crate-guessing_utils-1.0.3 (c (n "guessing_utils") (v "1.0.3") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1q48ib2wkpfa6xgl4c6kjazl40incbsk6k3k1wfanr510fxds1nz") (y #t)))

(define-public crate-guessing_utils-1.0.4 (c (n "guessing_utils") (v "1.0.4") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1zq8nb5gpz4cm8nm0g5pdhic9h568yhdp2f7rx5k69vbzag193sy")))

