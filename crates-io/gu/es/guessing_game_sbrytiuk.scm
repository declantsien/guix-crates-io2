(define-module (crates-io gu es guessing_game_sbrytiuk) #:use-module (crates-io))

(define-public crate-guessing_game_sbrytiuk-0.1.0 (c (n "guessing_game_sbrytiuk") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1vlszk50faaja25rbcgx3lwbrqyhr04yixdk48s9icwlh7yadn4b")))

(define-public crate-guessing_game_sbrytiuk-0.1.1 (c (n "guessing_game_sbrytiuk") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1xwlw53bx2z92s91fkhnhvigp1mhzrhzsg19frgwvlwsz3glyqmx")))

