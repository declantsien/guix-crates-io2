(define-module (crates-io gu es guessing_game_poen) #:use-module (crates-io))

(define-public crate-guessing_game_poen-0.1.0 (c (n "guessing_game_poen") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "107sprkc0xqfgsm09mga4c0xwh4d2hl99npiyr3c643m8f2f6ci0")))

(define-public crate-guessing_game_poen-0.1.1 (c (n "guessing_game_poen") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0p1i319h6acdyxg97p5k33cgx854z3hlpak0bmkg403j22vkm1s2")))

