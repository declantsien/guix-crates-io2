(define-module (crates-io gu es guessing-game-101) #:use-module (crates-io))

(define-public crate-guessing-game-101-0.1.0 (c (n "guessing-game-101") (v "0.1.0") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "1pqn73p60jxg0pbcqcgi3frjwjgrh5x7wpia35wdylmz5hmzmnj0") (y #t)))

(define-public crate-guessing-game-101-0.1.1 (c (n "guessing-game-101") (v "0.1.1") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "1sdljd69qbhrrycgqng8slpbm4r60sg4cs6fspgndvavza1741sl")))

