(define-module (crates-io gu rk gurkle-parser) #:use-module (crates-io))

(define-public crate-gurkle-parser-0.2.0 (c (n "gurkle-parser") (v "0.2.0") (d (list (d (n "combine") (r "^3.2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0wnw9l84mpjma9qxi3qkkfssm09p9d9gnwprv59kc1456dr974hr")))

(define-public crate-gurkle-parser-0.2.1 (c (n "gurkle-parser") (v "0.2.1") (d (list (d (n "combine") (r "^3.2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "1ixy339l53gk4c8c66mkfhn1791lvpcz2gj3k7qkr7nrjin3cbma")))

(define-public crate-gurkle-parser-0.3.0 (c (n "gurkle-parser") (v "0.3.0") (d (list (d (n "combine") (r "^3.2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "16nprnp8jwr84685pdnkcmcizc3lgd2k4kzpx6p1g3h03bwv8kwk")))

