(define-module (crates-io gu it guit) #:use-module (crates-io))

(define-public crate-guit-0.1.0 (c (n "guit") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)))) (h "03vkvmg2xia4w7d2xpcq67hkn148yyklbb2bvp4z263qip0h6ybr") (y #t)))

(define-public crate-guit-0.1.1 (c (n "guit") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)))) (h "0irqlyyshvi856ikyf9aj6cxklykyln6n2382pymhk3bpgnmaa48")))

