(define-module (crates-io gu it guitar-riff) #:use-module (crates-io))

(define-public crate-guitar-riff-0.1.0 (c (n "guitar-riff") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "03g3dvq0bgn3qkcsq4fk09nvcppizh62w24m2mcma445z2yq803c")))

(define-public crate-guitar-riff-0.1.1 (c (n "guitar-riff") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "19dd6ja57yhskrpps9nsyahgwrcdzr56h80g5p28xjxns6c9dnp9")))

