(define-module (crates-io gu it guitarpro) #:use-module (crates-io))

(define-public crate-guitarpro-0.1.0 (c (n "guitarpro") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "fraction") (r "^0.10") (d #t) (k 0)))) (h "020b1cl50ka2b41gwfzwsbm9lxmpk3kjx04im6yri896pgwk6pwc") (f (quote (("build-binary" "clap"))))))

