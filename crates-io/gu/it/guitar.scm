(define-module (crates-io gu it guitar) #:use-module (crates-io))

(define-public crate-guitar-0.1.0 (c (n "guitar") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "1p3kindvb5lqs66mhdynm9zx52lm8fjzzkq4d6s2r0cl27q0canp") (y #t)))

(define-public crate-guitar-0.1.1 (c (n "guitar") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "1jaam55n752xpw1xspy3aqyysxd2f9a9aifmc1g0xisb2v84wm0y")))

