(define-module (crates-io gu ro gurobi) #:use-module (crates-io))

(define-public crate-gurobi-0.1.0 (c (n "gurobi") (v "0.1.0") (d (list (d (n "gurobi-sys") (r "^0.1.0") (d #t) (k 0)))) (h "160w25hrfpb0y4ps1pfq659550gdbkc4j3wzi5r46rdrg10l2zqh") (y #t)))

(define-public crate-gurobi-0.1.1 (c (n "gurobi") (v "0.1.1") (d (list (d (n "gurobi-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1wc5414s577x4kv35778warpb7bd94yrrsz5wj6gc46g99raaz3p") (y #t)))

(define-public crate-gurobi-0.1.2 (c (n "gurobi") (v "0.1.2") (d (list (d (n "gurobi-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0v35651v1h06ms1g4lykxrdyhj8jbrlcbcn0c38yblx6nhc4h51c") (y #t)))

(define-public crate-gurobi-0.1.3 (c (n "gurobi") (v "0.1.3") (d (list (d (n "gurobi-sys") (r "^0.1.3") (d #t) (k 0)))) (h "0l04mbzim93xgxqkxap5jgd36asrvyxvzzsyhdxhxwqppdiw6i93") (y #t)))

(define-public crate-gurobi-0.1.4 (c (n "gurobi") (v "0.1.4") (d (list (d (n "gurobi-sys") (r "^0.1.3") (d #t) (k 0)))) (h "1adzmimw6ydj3r0s7xjg0cdjcfkhjbclm594xgixl0r2i5z7n7q6") (y #t)))

(define-public crate-gurobi-0.1.5 (c (n "gurobi") (v "0.1.5") (d (list (d (n "gurobi-sys") (r "^0.1.3") (d #t) (k 0)))) (h "1jq4gz2y4whngqyj1lh015zg1883mri9y3rc43wh46hwvixlfvcx") (y #t)))

(define-public crate-gurobi-0.1.6 (c (n "gurobi") (v "0.1.6") (d (list (d (n "gurobi-sys") (r "^0.1.3") (d #t) (k 0)))) (h "1fdwk8s210mhb8b03vppb1pi868ijijvz61lqqkjpq3shwl1kg7g") (y #t)))

(define-public crate-gurobi-0.1.7 (c (n "gurobi") (v "0.1.7") (d (list (d (n "gurobi-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "itertools") (r "^0.4.18") (d #t) (k 0)))) (h "1mv2nb5s8bhglwqlih90nxgvi72pckr3z61gmfd80ch3k2r21ng1") (y #t)))

(define-public crate-gurobi-0.1.8 (c (n "gurobi") (v "0.1.8") (d (list (d (n "gurobi-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "itertools") (r "^0.4.18") (d #t) (k 0)))) (h "03akk9izikshkj4y2fwhadpc7gz37wcz8zlp356dh9hkjw6s5lqp") (y #t)))

(define-public crate-gurobi-0.2.0 (c (n "gurobi") (v "0.2.0") (d (list (d (n "gurobi-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "itertools") (r "^0.4.18") (d #t) (k 0)))) (h "0x1zmmjiwj57s52kf160alimvickrch0x23007nwziiav4sb0mh2")))

(define-public crate-gurobi-0.3.0 (c (n "gurobi") (v "0.3.0") (d (list (d (n "gurobi-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.4.18") (d #t) (k 0)))) (h "14w9wgqh09bwnahpbd0gncrwhng7avlzc0gdh3dnw1vp2297cq1p")))

(define-public crate-gurobi-0.3.1 (c (n "gurobi") (v "0.3.1") (d (list (d (n "gurobi-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.4.18") (d #t) (k 0)))) (h "1vpwla0iybr7c1g43kgr0xg151k7wg0b6m2jf8qmmaaxpbf0pv4v")))

(define-public crate-gurobi-0.3.2 (c (n "gurobi") (v "0.3.2") (d (list (d (n "gurobi-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.4.18") (d #t) (k 0)))) (h "0sdz0kb0jcfb4161808gdalnj6iv31sfkg87lvkzhn5sw47mj2ra")))

(define-public crate-gurobi-0.3.3 (c (n "gurobi") (v "0.3.3") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "gurobi-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.4.18") (d #t) (k 0)))) (h "1mqmgv5h50kqd93hpi4f76m8ljpr5zx65knpyp7acfa65xss9ivc")))

(define-public crate-gurobi-0.3.4 (c (n "gurobi") (v "0.3.4") (d (list (d (n "gurobi-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.4.18") (d #t) (k 0)))) (h "1fsda8rb5q91rgx6maj0gdxygy0xjyyqq5qsrnv1js4k4i0jyqbg")))

