(define-module (crates-io gu ro gurobi-sys) #:use-module (crates-io))

(define-public crate-gurobi-sys-0.1.0 (c (n "gurobi-sys") (v "0.1.0") (h "0rkwd1r1hfr5lifsl8gyfj02172ikrwqg3y89jmcs35zmkwprff0")))

(define-public crate-gurobi-sys-0.1.2 (c (n "gurobi-sys") (v "0.1.2") (h "1gfww4v0r1zwihmrsc4zg5islq0c85gxd800p4a8rjlv7njjhc10")))

(define-public crate-gurobi-sys-0.1.3 (c (n "gurobi-sys") (v "0.1.3") (h "0kj4hd4b1vkqvq4x8zrsf58n81ck44f2br6wzryx2csnhjbpkxq6")))

(define-public crate-gurobi-sys-0.1.4 (c (n "gurobi-sys") (v "0.1.4") (h "1rl9mm9zpxn4jqbyg4pv8x9q453802csq4vpmh0rg733jr7096jx")))

(define-public crate-gurobi-sys-0.3.0 (c (n "gurobi-sys") (v "0.3.0") (h "1jdvxbpbc849prs2v24asx9dl6jh2zrbayksyqfr0s21a3mxzar7")))

