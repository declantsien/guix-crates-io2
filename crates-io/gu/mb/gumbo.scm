(define-module (crates-io gu mb gumbo) #:use-module (crates-io))

(define-public crate-gumbo-0.1.1 (c (n "gumbo") (v "0.1.1") (d (list (d (n "base64") (r "^0.22.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "codegen") (r "^0.2") (d #t) (k 0)) (d (n "cruet") (r "^0.14") (d #t) (k 0)) (d (n "pluralizer") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "120d56yg48gwqr3iy9jr54mpzh9lx5c2vp5i848c2wjzzczzhxr2")))

