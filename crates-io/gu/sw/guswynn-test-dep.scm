(define-module (crates-io gu sw guswynn-test-dep) #:use-module (crates-io))

(define-public crate-guswynn-test-dep-0.1.0 (c (n "guswynn-test-dep") (v "0.1.0") (h "0z1y3kyi2rc6whq712ss8vc1ic814qzdlqak0di2mwr8mmv33z5b")))

(define-public crate-guswynn-test-dep-0.2.0 (c (n "guswynn-test-dep") (v "0.2.0") (h "13qzpmmzl21lwvzy3r04g3vcxkjhlkg56ryyrk47p22myd9506v3")))

(define-public crate-guswynn-test-dep-0.2.1 (c (n "guswynn-test-dep") (v "0.2.1") (h "0apqc1da41j1105zmqqvmz9406vgjscxj7rxfr25pjkdk131d2cv")))

(define-public crate-guswynn-test-dep-0.3.0 (c (n "guswynn-test-dep") (v "0.3.0") (h "0ns9a6wwcsi2fk87wsdxbps9pbs4pb2g62nync3fv9fp3znjq683")))

(define-public crate-guswynn-test-dep-0.3.1 (c (n "guswynn-test-dep") (v "0.3.1") (h "18lz85cil8q82ir5s7jc4ryh1krvw69mkw7dzapzcysa7qfsc3zk")))

