(define-module (crates-io gu sw guswynn-test-crate) #:use-module (crates-io))

(define-public crate-guswynn-test-crate-0.1.0 (c (n "guswynn-test-crate") (v "0.1.0") (h "06kh3cgkfh66gayw4mz8j7kaz1d5m9i3ff9hbdpvpfdn3c4p8d8h")))

(define-public crate-guswynn-test-crate-0.2.0 (c (n "guswynn-test-crate") (v "0.2.0") (d (list (d (n "guswynn-test-dep") (r "~0.1") (d #t) (k 0)))) (h "1lp3563m23ygq3i6akzsfv9ks2m0q5mnfqlysf2l729xnhdh723s")))

(define-public crate-guswynn-test-crate-0.3.0 (c (n "guswynn-test-crate") (v "0.3.0") (d (list (d (n "guswynn-test-dep") (r "~0.2") (d #t) (k 0)))) (h "19ykkvc96s0qd5icjshdzykndq9fq0qpa15g2zwbaz56lrbrg2sp")))

