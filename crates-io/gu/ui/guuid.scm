(define-module (crates-io gu ui guuid) #:use-module (crates-io))

(define-public crate-guuid-1.0.0 (c (n "guuid") (v "1.0.0") (d (list (d (n "base32") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "getrandom" "std_rng"))) (o #t) (d #t) (k 0)))) (h "1x98j6y9y119rklz0701igp661qafzqx3j3x7sz2a55jwi2111qq") (f (quote (("std" "rand" "base32") ("no_std") ("default" "std") ("alloc" "base32"))))))

