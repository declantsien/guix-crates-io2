(define-module (crates-io gu ib guibuilder) #:use-module (crates-io))

(define-public crate-guibuilder-0.1.0 (c (n "guibuilder") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_derive") (r "^4.1.0") (d #t) (k 0)) (d (n "iced") (r "^0.7.0") (f (quote ("async-std" "image_rs"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1218ijlm0m96g7jk3z0h31ljh2m9bc0jwmxh088znrdrc5y7cx87")))

(define-public crate-guibuilder-0.1.1 (c (n "guibuilder") (v "0.1.1") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_derive") (r "^4.1.0") (d #t) (k 0)) (d (n "iced") (r "^0.7.0") (f (quote ("async-std" "image_rs"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "0rhn17v6sbr4md06yjadjz7g69psrk6vibhixn45y9f13l3f27hl")))

