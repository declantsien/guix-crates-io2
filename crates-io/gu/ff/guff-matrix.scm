(define-module (crates-io gu ff guff-matrix) #:use-module (crates-io))

(define-public crate-guff-matrix-0.1.0 (c (n "guff-matrix") (v "0.1.0") (h "14h4g9m73r0gg3n7ws62hj30dqpdd2x2dcgkmddsa2hha68fdqm8") (f (quote (("fake-simd"))))))

(define-public crate-guff-matrix-0.1.1 (c (n "guff-matrix") (v "0.1.1") (d (list (d (n "guff") (r "^0.1.7") (d #t) (k 0)))) (h "15xcnm1xb2ragivxjblmdad9pj9d4gkxbzv92fvc1nplc89l1x96") (f (quote (("fake-simd"))))))

(define-public crate-guff-matrix-0.1.2 (c (n "guff-matrix") (v "0.1.2") (d (list (d (n "guff") (r "^0.1.7") (d #t) (k 0)))) (h "06xkw3rbxwd8cydd9g6xhpsgin9g4gwq6hzxxmzq1vjg4bpkhjh5") (f (quote (("fake-simd"))))))

(define-public crate-guff-matrix-0.1.3 (c (n "guff-matrix") (v "0.1.3") (d (list (d (n "guff") (r "^0.1.7") (d #t) (k 0)))) (h "01w607acz62bv8miygzg0ix3jl0c9wvzf9a80fgcwvv72cbswn9l") (f (quote (("simulator") ("arm_vmull") ("arm_long") ("arm_dsp"))))))

(define-public crate-guff-matrix-0.1.4 (c (n "guff-matrix") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "guff") (r "^0.1.7") (d #t) (k 0)))) (h "0f0v1l3m8n1x88n2bh72admvkzwd8sxq1mbrw1a73i3cp5ji4rx6") (f (quote (("simulator") ("arm_vmull") ("arm_long") ("arm_dsp"))))))

(define-public crate-guff-matrix-0.1.5 (c (n "guff-matrix") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "guff") (r "^0.1.7") (d #t) (k 0)))) (h "1dlq19kzn3g2px5r8pwl3967r7bj30cf19anfbhzdlva58hjgw47") (f (quote (("simulator") ("arm_vmull") ("arm_long") ("arm_dsp"))))))

(define-public crate-guff-matrix-0.1.6 (c (n "guff-matrix") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "guff") (r "^0.1.7") (d #t) (k 0)))) (h "08d64i1g9p5q6scnh6dssn8w35sj9s6ig0j08lcv7hzq86w1m25q") (f (quote (("simulator") ("arm_vmull") ("arm_long") ("arm_dsp"))))))

(define-public crate-guff-matrix-0.1.7 (c (n "guff-matrix") (v "0.1.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "guff") (r "^0.1.7") (d #t) (k 0)))) (h "1p3ssch9pbd5ivmg5vj2ipyj0g46lbvg36gr6qr2f0dkanambvgw") (f (quote (("simulator") ("arm_vmull") ("arm_long") ("arm_dsp"))))))

(define-public crate-guff-matrix-0.1.8 (c (n "guff-matrix") (v "0.1.8") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "guff") (r "^0.1.7") (d #t) (k 0)))) (h "0ghbsz7rmkrn0xcnz837b5rcl38f92d9n5flsxpkzw06fmq2ws3v") (f (quote (("simulator") ("arm_vmull") ("arm_long") ("arm_dsp"))))))

(define-public crate-guff-matrix-0.1.9 (c (n "guff-matrix") (v "0.1.9") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "guff") (r "^0.1.7") (d #t) (k 0)))) (h "13qcm2kakiz9pf06wh4c5bp7fwbi65366pb1mrm9kjh2sbn2ziv8") (f (quote (("simulator") ("arm_vmull") ("arm_long") ("arm_dsp"))))))

(define-public crate-guff-matrix-0.1.10 (c (n "guff-matrix") (v "0.1.10") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "guff") (r "^0.1.7") (d #t) (k 0)))) (h "0qs0gyany0w4ch9scv8qrp92pz3swljf70q78w01mqbkigdr8zvq") (f (quote (("simulator") ("arm_vmull") ("arm_long") ("arm_dsp"))))))

(define-public crate-guff-matrix-0.1.11 (c (n "guff-matrix") (v "0.1.11") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "guff") (r "^0.1.7") (d #t) (k 0)))) (h "0xvaaq3f7i227xcfdh6sip6qn4p56bl7fmb49x2bgdjj0bwxki3w") (f (quote (("simulator") ("arm_vmull") ("arm_long") ("arm_dsp"))))))

(define-public crate-guff-matrix-0.1.12 (c (n "guff-matrix") (v "0.1.12") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "guff") (r "~0.1") (d #t) (k 0)))) (h "0z7cajca37ayxbhy67by99061z5mxxyvqq74vc8iyjz9qjmxd5l5") (f (quote (("simulator") ("arm_vmull") ("arm_long") ("arm_dsp"))))))

