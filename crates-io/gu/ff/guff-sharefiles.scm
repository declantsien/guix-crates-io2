(define-module (crates-io gu ff guff-sharefiles) #:use-module (crates-io))

(define-public crate-guff-sharefiles-0.1.8 (c (n "guff-sharefiles") (v "0.1.8") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "guff") (r "^0.1.7") (d #t) (k 0)) (d (n "guff-matrix") (r "^0.1.8") (d #t) (k 0)))) (h "13s8ikhc59llw9ll75wdgal5jljb7sxd8950a3bvyin98wny628y")))

(define-public crate-guff-sharefiles-0.1.2 (c (n "guff-sharefiles") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "guff") (r "^0.1.7") (d #t) (k 0)) (d (n "guff-ida") (r "^0.1.2") (d #t) (k 0)) (d (n "guff-matrix") (r "^0.1.8") (d #t) (k 0)))) (h "0jsbyihi37xr9nyvnimkrwbpbmjc166yb513snncvzfj0wrsxfjl") (y #t)))

(define-public crate-guff-sharefiles-0.1.9 (c (n "guff-sharefiles") (v "0.1.9") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "guff") (r "^0.1.7") (d #t) (k 0)) (d (n "guff-ida") (r "^0.1.2") (d #t) (k 0)) (d (n "guff-matrix") (r "^0.1.8") (d #t) (k 0)))) (h "06f84i0f93wir4xjhwn5j1l1wxdyng2b5vrw8fkpd4x73nxsswwx")))

(define-public crate-guff-sharefiles-0.1.10 (c (n "guff-sharefiles") (v "0.1.10") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "guff") (r "^0.1.7") (d #t) (k 0)) (d (n "guff-ida") (r "^0.1.2") (d #t) (k 0)) (d (n "guff-matrix") (r "^0.1.11") (d #t) (k 0)))) (h "0m2babxsw16shn17rhhc4dz7p73fmq9423v1sp4l6s7fmr2md8ad")))

(define-public crate-guff-sharefiles-0.1.11 (c (n "guff-sharefiles") (v "0.1.11") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "guff") (r "~0.1") (d #t) (k 0)) (d (n "guff-ida") (r "~0.1") (d #t) (k 0)) (d (n "guff-matrix") (r "~0.1") (f (quote ("arm_vmull"))) (d #t) (k 0)))) (h "07fvdnviaq9d6yh1p7qw8yjrwxm9gwbjgf2rsz6mgrd05xcmra2y")))

