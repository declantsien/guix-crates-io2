(define-module (crates-io gu ff guff) #:use-module (crates-io))

(define-public crate-guff-0.1.0 (c (n "guff") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1cjyiy7sd98sxk3fz9ywf71pbwcw11kicy6z44dpff6dzhzpqq5c")))

(define-public crate-guff-0.1.1 (c (n "guff") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0xh72x0sjbsc4jplylvbly2x5xp8a9309mmhf3lav7by3q0cfj8v")))

(define-public crate-guff-0.1.2 (c (n "guff") (v "0.1.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1fzmfm0w603n7plx41wyfd2f7in72cxkdr6br30ibjhw8knip4iq")))

(define-public crate-guff-0.1.3 (c (n "guff") (v "0.1.3") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "04xkgsasm9mb97y8icjkzxqam8vxlfrhsgaa1c88lwr0ag49chjs")))

(define-public crate-guff-0.1.4 (c (n "guff") (v "0.1.4") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1jhg8y9wsajlkxwamq1zcfyz9fag8i3w9kk07d6qghv84c1ygbfc")))

(define-public crate-guff-0.1.5 (c (n "guff") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0bcpfbq8wc6ipg86b3c50r9nqhgkiz0fzfcyb0b23ifflgrlhwxc")))

(define-public crate-guff-0.1.6 (c (n "guff") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0znwdgn0cqza5zf5waldg1cy3wck8ikgw65dsrbcayqwnr78c2y4")))

(define-public crate-guff-0.1.7 (c (n "guff") (v "0.1.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "14ly9awiqw8669bbp2sw8jd36v1kwdzx9r7kjkwh3r6i9b10i43g")))

