(define-module (crates-io gu ff guff-ida) #:use-module (crates-io))

(define-public crate-guff-ida-0.1.0 (c (n "guff-ida") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "guff") (r "^0.1.7") (d #t) (k 0)) (d (n "guff-matrix") (r "^0.1.7") (d #t) (k 0)))) (h "0j14viy2cqp2w8fyyr0l464z6lxxyd86dkw6ya97z3cqbjd0avfl")))

(define-public crate-guff-ida-0.1.2 (c (n "guff-ida") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "guff") (r "^0.1.7") (d #t) (k 0)) (d (n "guff-matrix") (r "^0.1.8") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "05x7kpn6ck56spaqkmbsd2yi33i36nv5qiadvfli3mr82rf8mwbh")))

(define-public crate-guff-ida-0.1.3 (c (n "guff-ida") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "guff") (r "~0.1") (d #t) (k 0)) (d (n "guff-matrix") (r "~0.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0p0ps5dkhdi4x42iibib1dwcnf5ar20kar5b7mha8ivrlcdki11g")))

