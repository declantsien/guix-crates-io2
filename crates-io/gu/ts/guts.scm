(define-module (crates-io gu ts guts) #:use-module (crates-io))

(define-public crate-guts-0.1.0 (c (n "guts") (v "0.1.0") (h "1z05y5z29l0j8f6pxsriz4rvjy9a1m7s3y69f3lw9b3g9h6407hk") (f (quote (("never_type"))))))

(define-public crate-guts-0.1.1 (c (n "guts") (v "0.1.1") (h "1mgw0fzcjq7p6dicam69igd7cgay1fvxp8gwys6nwv4wq54xpv0w") (f (quote (("never_type"))))))

