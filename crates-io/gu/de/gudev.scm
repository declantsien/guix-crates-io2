(define-module (crates-io gu de gudev) #:use-module (crates-io))

(define-public crate-gudev-0.1.0 (c (n "gudev") (v "0.1.0") (d (list (d (n "glib") (r "^0.1.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "gudev-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0f3adr3vqih3pgbmn5d9l0ld5v8zrgfk20av5zhzivrpg24jazf4")))

(define-public crate-gudev-0.1.1 (c (n "gudev") (v "0.1.1") (d (list (d (n "glib") (r "^0.1.1") (d #t) (k 0)) (d (n "glib-sys") (r "^0.3.2") (d #t) (k 0)) (d (n "gudev-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fdv973d49hvrg36kms17zvmapfiz1kjn6x4smf6ajh411rk55li")))

(define-public crate-gudev-0.2.0 (c (n "gudev") (v "0.2.0") (d (list (d (n "glib") (r "^0.1.1") (d #t) (k 0)) (d (n "glib-sys") (r "^0.3.2") (d #t) (k 0)) (d (n "gudev-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "137rybg4jymfnwcxgrq7ydmp20ikjw9vzy10agxkwz4y1gl3cakf")))

(define-public crate-gudev-0.2.1 (c (n "gudev") (v "0.2.1") (d (list (d (n "glib") (r "^0.1.2") (d #t) (k 0)) (d (n "glib-sys") (r "^0.3.3") (d #t) (k 0)) (d (n "gudev-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1a68zpwbrn2v1rzy3c4gxqff018d2bjh9b8gzl9ir52vl9p17fbx")))

(define-public crate-gudev-0.2.2 (c (n "gudev") (v "0.2.2") (d (list (d (n "glib") (r "^0.1.3") (d #t) (k 0)) (d (n "glib-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "gudev-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06pgnf7rmww1y7rwa7r8l4dkcqjl8lcsb9qcyfhhjj4wpgr51qh6")))

(define-public crate-gudev-0.3.0 (c (n "gudev") (v "0.3.0") (d (list (d (n "glib") (r "^0.3.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "gudev-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1n5cc9mkanjxbb82dwdp4c46bbv307z8klfsis06a8g39hksgg8g")))

(define-public crate-gudev-0.4.0 (c (n "gudev") (v "0.4.0") (d (list (d (n "glib") (r "^0.5.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "gudev-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1agn5nhzhv9nr9zxivf2yhs2hll9k0nd2vq0mksz6324blq5aajc")))

(define-public crate-gudev-0.5.0 (c (n "gudev") (v "0.5.0") (d (list (d (n "glib") (r "^0.6.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "gudev-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0q5x7vz709372x4q32fyf9kfdds8y5pm0awzq85w8zggp10w8jwf")))

(define-public crate-gudev-0.6.0 (c (n "gudev") (v "0.6.0") (d (list (d (n "glib") (r "^0.7.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "gudev-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0m3hx530ir7c9vgcfk0wdb45ngf5j76cij1cjgayb81gc7k96wjh")))

(define-public crate-gudev-0.7.0 (c (n "gudev") (v "0.7.0") (d (list (d (n "glib") (r "^0.8.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.9.0") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.9.0") (d #t) (k 0)) (d (n "gudev-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rj2lbim9kvf01269df1c5h6vsillc484d1sm4mh082lxriwnxkd")))

(define-public crate-gudev-0.8.0 (c (n "gudev") (v "0.8.0") (d (list (d (n "glib") (r "^0.9.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.9.1") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.9.1") (d #t) (k 0)) (d (n "gudev-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "04xhmxkp2m7kj2hl7ik2hh2axihf99y5wl5bav02ykfcmjic5q8r")))

(define-public crate-gudev-0.9.0 (c (n "gudev") (v "0.9.0") (d (list (d (n "glib") (r "^0.10.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.10.0") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.10.0") (d #t) (k 0)) (d (n "gudev-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0i6wwwmagxzdkqpdlvng4yw6ykyfbfgfb9672kpxk3ljl995da8y")))

(define-public crate-gudev-0.10.0 (c (n "gudev") (v "0.10.0") (d (list (d (n "glib") (r "^0.14.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.14.0") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.14.0") (d #t) (k 0)) (d (n "gudev-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0r20yzrpg0qqyndnp0dfi6alk2bmrh010iagv5sm16w3jcaa09mx")))

(define-public crate-gudev-0.11.0 (c (n "gudev") (v "0.11.0") (d (list (d (n "ffi") (r "^0.9.0") (d #t) (k 0) (p "gudev-sys")) (d (n "glib") (r "^0.15.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cl98v0sygkf17vm3973q1slr28y5apdl6b8sivq8q719lvh6dcb")))

(define-public crate-gudev-0.12.0 (c (n "gudev") (v "0.12.0") (d (list (d (n "ffi") (r "^0.9.0") (d #t) (k 0) (p "gudev-sys")) (d (n "glib") (r "^0.15.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1kql2s1k0i28qsvfali0ifrczyh1r6688s0yflxvs21k0jy3sw3k")))

(define-public crate-gudev-0.13.0 (c (n "gudev") (v "0.13.0") (d (list (d (n "ffi") (r "^0.10.0") (d #t) (k 0) (p "gudev-sys")) (d (n "glib") (r "^0.16.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zhalw3jphq57ffi43h6yrmzzpziwdhczrfkvfi6szmnjwdjnxqn")))

(define-public crate-gudev-0.14.0 (c (n "gudev") (v "0.14.0") (d (list (d (n "ffi") (r "^0.11.0") (d #t) (k 0) (p "gudev-sys")) (d (n "glib") (r "^0.17.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0i7a7ff77amz36ywcx6khpns6fg9k9ydzkfi0f5ir4n0mn23axb7")))

(define-public crate-gudev-0.15.0 (c (n "gudev") (v "0.15.0") (d (list (d (n "ffi") (r "^0.12.0") (d #t) (k 0) (p "gudev-sys")) (d (n "glib") (r "^0.18.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "177yh807c92anx857dj6kz40h0y6dgcjvhgglgldi4cyb8qgsi8c")))

(define-public crate-gudev-0.16.0 (c (n "gudev") (v "0.16.0") (d (list (d (n "ffi") (r "^0.13.0") (d #t) (k 0) (p "gudev-sys")) (d (n "glib") (r "^0.19.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fk34i1sgi27f7vnxxpgb9zlsymqpic405cgqcip8cq3flkmsf2h")))

