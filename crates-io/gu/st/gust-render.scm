(define-module (crates-io gu st gust-render) #:use-module (crates-io))

(define-public crate-gust-render-0.2.0 (c (n "gust-render") (v "0.2.0") (d (list (d (n "alga") (r "^0.7.2") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.19.0") (d #t) (k 0)) (d (n "gl") (r "^0.11.0") (d #t) (k 0)) (d (n "glfw") (r "^0.25.0") (d #t) (k 0)) (d (n "image") (r "^0.20.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.11") (d #t) (k 0)))) (h "02bvarrh32vxagmvqpfka4jfh8l1xl1p3r674qg6f026fm4vx93g")))

