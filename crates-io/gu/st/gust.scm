(define-module (crates-io gu st gust) #:use-module (crates-io))

(define-public crate-gust-0.1.0 (c (n "gust") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sz56zsvysz8zlbyclsry4smm4mxyncs38nwxxwrskdmfnqwxxsm")))

(define-public crate-gust-0.1.1 (c (n "gust") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1saahc9rabczvig5mmqlmdg7gblhwiyap8wff91v21yqdxm0kh8z")))

(define-public crate-gust-0.1.2 (c (n "gust") (v "0.1.2") (d (list (d (n "liquid") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16prwr3jk0vhld0csh6zfn3qs1xa45j2c1adl4v8vzf675xwgngv")))

(define-public crate-gust-0.1.3 (c (n "gust") (v "0.1.3") (d (list (d (n "liquid") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hqfkpx8m9j3l6jn5zsayi6lyvd79xiyxnd6yjyicrwpdxcfw8bc")))

(define-public crate-gust-0.1.4 (c (n "gust") (v "0.1.4") (d (list (d (n "liquid") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00h237vy7b881hmwlb86ryaq79ymylrvw9s1m3qncslfb73chkp0")))

