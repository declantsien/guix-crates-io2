(define-module (crates-io gu la gulali) #:use-module (crates-io))

(define-public crate-gulali-2019.3.4 (c (n "gulali") (v "2019.3.4") (h "0ba29xsxiknbl5h4xhyq23dfi926wj5g7bwq9600ab19dz4b8wp5")))

(define-public crate-gulali-2019.3.5 (c (n "gulali") (v "2019.3.5") (h "1r389yxbysp0dc8kmrwqq04fh5fgk84cbwyzkl0h84jakbf6dk57")))

(define-public crate-gulali-2019.3.6 (c (n "gulali") (v "2019.3.6") (h "1hw7pyax2cqkr69iqlwsjgzf7dcdarsm9qx576mrvdhn4rqlibh9")))

(define-public crate-gulali-2019.3.7 (c (n "gulali") (v "2019.3.7") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "0d203saf1xiwvl3266c4jq2lv12f0y0rvwqn6dklc7avav6w5072")))

(define-public crate-gulali-2019.3.8 (c (n "gulali") (v "2019.3.8") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "0zl518mm80sg2glw8w4yj8plkvxjc2wd65pvdvdxsd5am2kggbzq")))

(define-public crate-gulali-2019.3.9 (c (n "gulali") (v "2019.3.9") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "09rqrml9jzfc9xamahwgskzqg945hk5x5fbmwxbbbsjyr1g2lwmx")))

