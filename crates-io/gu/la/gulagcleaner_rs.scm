(define-module (crates-io gu la gulagcleaner_rs) #:use-module (crates-io))

(define-public crate-gulagcleaner_rs-0.10.1 (c (n "gulagcleaner_rs") (v "0.10.1") (d (list (d (n "flate2") (r "^1.0.27") (d #t) (k 0)) (d (n "lopdf") (r "^0.31.0") (d #t) (k 0)))) (h "0j2gk9jsyclr8v5g55hwikx4hm40v5yplxfi4hxrh4256qp4bfwk")))

(define-public crate-gulagcleaner_rs-0.10.2 (c (n "gulagcleaner_rs") (v "0.10.2") (d (list (d (n "flate2") (r "^1.0.27") (d #t) (k 0)) (d (n "lopdf") (r "^0.31.0") (d #t) (k 0)))) (h "0nii355kas8ihaf5lvmipjkj1mwbbjbh01jd2jp1jbbcfz4xvflg")))

(define-public crate-gulagcleaner_rs-0.10.3 (c (n "gulagcleaner_rs") (v "0.10.3") (d (list (d (n "flate2") (r "^1.0.27") (d #t) (k 0)) (d (n "lopdf") (r "^0.31.0") (d #t) (k 0)))) (h "0wi2w6rd0lq23cav4kmdyy78g4xzsglg97lkdghi0jlhhmgcblxc")))

(define-public crate-gulagcleaner_rs-0.11.0 (c (n "gulagcleaner_rs") (v "0.11.0") (d (list (d (n "flate2") (r "^1.0.27") (d #t) (k 0)) (d (n "lopdf") (r "^0.31.0") (d #t) (k 0)))) (h "0j4rhbgs6s46xcdb178k4hwziiq6jw9c398w1l7w1fdqfk09zbh8")))

(define-public crate-gulagcleaner_rs-0.11.1 (c (n "gulagcleaner_rs") (v "0.11.1") (d (list (d (n "flate2") (r "^1.0.27") (d #t) (k 0)) (d (n "lopdf") (r "^0.31.0") (d #t) (k 0)))) (h "0ss4ckjm1mayps9fn0j13fm31kl2vhzvbnllbmicakshvq3y9lnm")))

(define-public crate-gulagcleaner_rs-0.12.1 (c (n "gulagcleaner_rs") (v "0.12.1") (d (list (d (n "flate2") (r "^1.0.27") (d #t) (k 0)) (d (n "lopdf") (r "^0.32.0") (d #t) (k 0)))) (h "1a2mkm5dhzblh95dcdki6d5lx8pqqhwk1pqd936zmhi43nvpsyk4")))

(define-public crate-gulagcleaner_rs-0.12.2 (c (n "gulagcleaner_rs") (v "0.12.2") (d (list (d (n "flate2") (r "^1.0.27") (d #t) (k 0)) (d (n "lopdf") (r "^0.32.0") (d #t) (k 0)))) (h "0i7lbkbksp56i9g6r8c3rd17aa5ki0wz0k3z8dsh5lg2p7c5nrx6")))

