(define-module (crates-io gu ar guard_let) #:use-module (crates-io))

(define-public crate-guard_let-0.1.0 (c (n "guard_let") (v "0.1.0") (d (list (d (n "pmutil") (r "^0.5.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("fold" "full" "derive"))) (d #t) (k 0)))) (h "1jyi5flqc1zmbqgbc417h1jayan56rcjbqrwgwr9gza5h7j7l35a")))

(define-public crate-guard_let-0.1.1 (c (n "guard_let") (v "0.1.1") (d (list (d (n "pmutil") (r "^0.5.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("fold" "full" "derive"))) (d #t) (k 0)))) (h "0k1cgbx7xirdlgxxbq95pk6lh2wgyxy79sgzhcl1hm6yxzpp6dy5")))

(define-public crate-guard_let-0.1.2 (c (n "guard_let") (v "0.1.2") (d (list (d (n "pmutil") (r "^0.5.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("fold" "full" "derive"))) (d #t) (k 0)))) (h "1hxki3bqiii7b2rbdg14in3j83dd49achlr0ngzi41jypzk88nzm")))

