(define-module (crates-io gu ar guarding_adapter) #:use-module (crates-io))

(define-public crate-guarding_adapter-0.1.0 (c (n "guarding_adapter") (v "0.1.0") (d (list (d (n "guarding_core") (r "^0.2.5") (d #t) (k 0)) (d (n "guarding_parser") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1rvwhsd3cqmx32jljw8b64cgmx44xfqzbhy54aflm30iby0s3dbd")))

