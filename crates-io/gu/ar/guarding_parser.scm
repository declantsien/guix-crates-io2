(define-module (crates-io gu ar guarding_parser) #:use-module (crates-io))

(define-public crate-guarding_parser-0.2.3 (c (n "guarding_parser") (v "0.2.3") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0w10vwvwj34qwp6rakmlgrh35vlszcsi3p0kqr1khfq539agil4a")))

(define-public crate-guarding_parser-0.2.4 (c (n "guarding_parser") (v "0.2.4") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "01y7jv9dcchn86d5b9kjdwyvjlg11nc5d1864nn2ifljx9r6ggx7")))

(define-public crate-guarding_parser-0.2.5 (c (n "guarding_parser") (v "0.2.5") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0kbj9inbmkh1kv13myf6xc3sqlwgpavmnmgip3xvvndgvlb3h3ym")))

(define-public crate-guarding_parser-0.2.6 (c (n "guarding_parser") (v "0.2.6") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1177v9cip9mq7jxx5fr8l89rwfq7dqlgg9xdjgdmhxgm8nczg658")))

