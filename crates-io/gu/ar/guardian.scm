(define-module (crates-io gu ar guardian) #:use-module (crates-io))

(define-public crate-guardian-0.3.0 (c (n "guardian") (v "0.3.0") (h "13rnwh4yx5hvfggqsvx4r5bajfpqp1j1jralplaby9svb6qgr71z")))

(define-public crate-guardian-0.3.1 (c (n "guardian") (v "0.3.1") (h "0vn5xhiwvgc7ziv7npksczkmpsd4fwfiw2p71sdipp810lr5lv3r")))

(define-public crate-guardian-1.0.0 (c (n "guardian") (v "1.0.0") (h "1clcbn14rpb9dzrrkh0gf10yhqfdh1svcz9nnm3hnmnqlanjarcd")))

(define-public crate-guardian-1.0.1 (c (n "guardian") (v "1.0.1") (h "0m6avspl7gx4wsmzz9fi95znfi4f9cnini0dgjjhw3cfprk1fpvn")))

(define-public crate-guardian-1.0.2 (c (n "guardian") (v "1.0.2") (h "0p487li66bn012hpivyns1rkf2gsk6c8mdwib3kblgdd7ic6jshy")))

(define-public crate-guardian-1.1.0 (c (n "guardian") (v "1.1.0") (h "1da82ha3p03dy127091rcl79smxgfkkh0ra8y0mbpsfji53ia5v8") (r "1.56.0")))

