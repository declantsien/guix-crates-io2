(define-module (crates-io gu ar guard-trait) #:use-module (crates-io))

(define-public crate-guard-trait-0.1.0 (c (n "guard-trait") (v "0.1.0") (d (list (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "0v02ppic9nm0aaij9li3l0d91v0kb7yjw0vx60g4jwbw056p3qbr") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-guard-trait-0.1.1 (c (n "guard-trait") (v "0.1.1") (d (list (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "0n0i4pzmv5ira938iiz85nfanxsnpylrwmvx5xvsjk2906lmb1wc") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-guard-trait-0.2.0 (c (n "guard-trait") (v "0.2.0") (d (list (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "01m9ppw5rxxdp7vmkafc6kni60bckc7srkivnj9bi1x3sjywfkfy") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-guard-trait-0.2.1 (c (n "guard-trait") (v "0.2.1") (d (list (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "1c9x04vrsl7qgd8zqk85wijjjdsww13sppcq8rnbx15j0kwza9fb") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-guard-trait-0.3.0 (c (n "guard-trait") (v "0.3.0") (d (list (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "05v87ypwwm2jmr6bnay11a5wz40pfpfar139g5s1fa165r8jj4rf") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-guard-trait-0.4.0 (c (n "guard-trait") (v "0.4.0") (d (list (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "0ap2hc4r1dcb1b4xyr4knily6c8hmx68gixm3zn2fh2hrhdhc5w0") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-guard-trait-0.4.1 (c (n "guard-trait") (v "0.4.1") (d (list (d (n "stable_deref_trait") (r "^1.2") (d #t) (k 0)))) (h "18gi23djz4wiyrpjidwpv3z94irh90dv8r7igzk6zhdr8i4wf51z") (f (quote (("std") ("nightly") ("default" "std"))))))

