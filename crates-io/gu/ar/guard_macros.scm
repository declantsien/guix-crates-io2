(define-module (crates-io gu ar guard_macros) #:use-module (crates-io))

(define-public crate-guard_macros-1.0.0 (c (n "guard_macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("printing" "parsing" "proc-macro" "full"))) (k 0)))) (h "0zgmz51lypsdiwds57c0qnyaps4wl50wvplf3pjyzfcnqadqldly") (f (quote (("debug-print" "syn/extra-traits"))))))

(define-public crate-guard_macros-1.0.1 (c (n "guard_macros") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("printing" "parsing" "proc-macro" "full"))) (k 0)))) (h "16ap4wa3szl929khc2dvhhlwhdc0qc2a1jydqp67bz2qa8pvaw9x") (f (quote (("debug-print" "syn/extra-traits"))))))

(define-public crate-guard_macros-1.0.2 (c (n "guard_macros") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (k 0)) (d (n "syn") (r "^2") (f (quote ("printing" "parsing" "proc-macro" "full"))) (k 0)))) (h "17wd2flhns2bqpm4qcplnwd5xphisqz2v8dixr4ivznri5122mdi") (f (quote (("debug-print" "syn/extra-traits"))))))

