(define-module (crates-io gu ar guarding_core) #:use-module (crates-io))

(define-public crate-guarding_core-0.2.1 (c (n "guarding_core") (v "0.2.1") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "156w25srcf4a5li3z3pfjh5sndp1g2hj34q851qghhs3d6p2hf81")))

(define-public crate-guarding_core-0.2.2 (c (n "guarding_core") (v "0.2.2") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1160k5cpphydprqm00n4rlz7cjcpf2x3mi04dk3c3wv24sf5mvwq")))

(define-public crate-guarding_core-0.2.3 (c (n "guarding_core") (v "0.2.3") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "15y4kh9pf1rfms0kfxdrxbql5dbz163bx54lncmc77mfsgxqpqds")))

(define-public crate-guarding_core-0.2.4 (c (n "guarding_core") (v "0.2.4") (d (list (d (n "guarding_parser") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "030p1wycszm0jg0fmb23nqp0pjkvrz4cn40ynj319a4j8xa5ll3h")))

(define-public crate-guarding_core-0.2.5 (c (n "guarding_core") (v "0.2.5") (d (list (d (n "guarding_parser") (r "^0.2.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ks3c93jzjnh1m804bzks862f5cj22gxq79vqid8bl6baagscky3")))

(define-public crate-guarding_core-0.2.6 (c (n "guarding_core") (v "0.2.6") (d (list (d (n "guarding_parser") (r "^0.2.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "09ib3yypybs1d8nxnf2wbdk85r4s2i3pc03y0vwqjk3r4m86mjbx")))

(define-public crate-guarding_core-0.2.7 (c (n "guarding_core") (v "0.2.7") (d (list (d (n "guarding_parser") (r "^0.2.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "13wgdih9vqzdl8hghq9rvg6ps90mhbrvdry124rcw7r2km4snq5c")))

