(define-module (crates-io gu ar guard) #:use-module (crates-io))

(define-public crate-guard-0.1.0 (c (n "guard") (v "0.1.0") (h "18dsgmiqhbdwgfz32j1bkvd16bh0vc2wsds9bphm67cr3bbklkp1") (f (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.1.1 (c (n "guard") (v "0.1.1") (h "11zk8rjwg17k4ayw40x0p2iczrdfclyxck5yyh3m2pv66zy19ldv") (f (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.2.0 (c (n "guard") (v "0.2.0") (h "15zrhpiwxwrhakp1c4imwpcyjyyyl7m1qp0s9aafyg7h0874m7jl") (f (quote (("nightly") ("debug")))) (y #t)))

(define-public crate-guard-0.2.1 (c (n "guard") (v "0.2.1") (h "0x78mkk1c0mmch5crcjm7vaw0q7srpz0q4jhj6cgmh00ka949z1q") (f (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.2.2 (c (n "guard") (v "0.2.2") (h "153fhv8nlv9005lk0yyfisrsvmaxjsm7ybypzd1a5ryfmbykwm1v") (f (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.2.3 (c (n "guard") (v "0.2.3") (h "0rdcwvaim9sjzvpra97zh8nrjj3i6dam1aqn8gzq9ziy0idgyfna") (f (quote (("nightly") ("debug")))) (y #t)))

(define-public crate-guard-0.2.4 (c (n "guard") (v "0.2.4") (h "15yvqwnvblhsjqih3dgkdaprvpw7jwa5vcr3icp3ys581800lqzh") (f (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.2.5 (c (n "guard") (v "0.2.5") (h "0r9d3splmi4cfw4ngdf2sgm35y4ry0ndkzgkhnw71l08zcdh83mc") (f (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.2.6 (c (n "guard") (v "0.2.6") (h "0am6rjh95h91q40fs3hcj3g765lljz9wg9vv2irf6wbdix65nd73") (f (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.3.0 (c (n "guard") (v "0.3.0") (h "0cb2idw4nb5gzk05x9illhmskm50a4d8p74kgpivxjlqan7hpymn") (f (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.3.1 (c (n "guard") (v "0.3.1") (h "0n4lksmhjcqihla0sq2ga09bgm1fnprlj4qknc7kq2q963smajkp") (f (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.3.2 (c (n "guard") (v "0.3.2") (h "0n2gbfjq03sfmbara36d3w80c4vqd2bdglxqydz9nlqhf1j8z346") (f (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.3.3 (c (n "guard") (v "0.3.3") (h "18paa29s5jfvjxpqr0fyh35agkjfrmjbfwczr2fd0czxsh82bq0y") (f (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.3.4 (c (n "guard") (v "0.3.4") (h "0k6i5jbcjdw1rw6h8ywkzp6kw0q9mc2afl540i5dqycq3izll6w2") (f (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.4.0 (c (n "guard") (v "0.4.0") (h "0zm9gbavrcw3250nb81il3g0z8yg4544b6jl8990fa4l8yg2hr3d") (f (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.5.0 (c (n "guard") (v "0.5.0") (h "1cdvdylsp0ywq3fkglsxnljnjq9kfxnnq9zfvwnklaavqm7ddwz1") (f (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.5.1 (c (n "guard") (v "0.5.1") (h "1slkgi0xj1gqjq44cvg8rc65mg3fq1v47bzvfcxqlkx03v2kr2gz") (f (quote (("nightly") ("debug"))))))

(define-public crate-guard-0.5.2 (c (n "guard") (v "0.5.2") (h "112d0sidfjm28fcpzx79k891pl1smwpvnrr4kb6g7fai8jy04x3x") (f (quote (("nightly") ("debug"))))))

