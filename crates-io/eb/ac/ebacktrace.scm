(define-module (crates-io eb ac ebacktrace) #:use-module (crates-io))

(define-public crate-ebacktrace-0.2.0 (c (n "ebacktrace") (v "0.2.0") (h "1g6fhrb5c64yxs1rs1q1bvnil5kj76skpcg3bj0gzsl66dyk4nj1") (f (quote (("force_backtrace") ("derive_display") ("default" "force_backtrace" "derive_display"))))))

(define-public crate-ebacktrace-0.3.0 (c (n "ebacktrace") (v "0.3.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)))) (h "04h1k0cpgk5lh14i3vwwhbf072fjjmlwi46vfsraj84j8hxmclws") (f (quote (("derive_display") ("default" "derive_display"))))))

(define-public crate-ebacktrace-0.3.1 (c (n "ebacktrace") (v "0.3.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)))) (h "0v2akgnggf9addzkf2qlnrydm6j0n665dmhbpij4dfdj6y90m9ai") (f (quote (("force_backtrace") ("derive_display") ("default" "derive_display"))))))

(define-public crate-ebacktrace-0.3.2 (c (n "ebacktrace") (v "0.3.2") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)))) (h "12sf395g08fn78fp8jrjfnvg9hqhk0dd7p6c2adnah0cs80qhcjj") (f (quote (("force_backtrace") ("derive_display") ("default" "derive_display"))))))

(define-public crate-ebacktrace-0.4.0 (c (n "ebacktrace") (v "0.4.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)))) (h "1pkbj1cvpr8a246qpbr1h4r4ga57m9qhr81yzili8v7pqmwmvd44") (f (quote (("force_backtrace") ("derive_display") ("default" "derive_display"))))))

(define-public crate-ebacktrace-0.5.0 (c (n "ebacktrace") (v "0.5.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)))) (h "1czgm0mnakmwsmvxi2salqhq76ihyj5f0njlyqzg55hixg8b6c96") (f (quote (("force_backtrace") ("default")))) (y #t)))

(define-public crate-ebacktrace-0.5.1 (c (n "ebacktrace") (v "0.5.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)))) (h "1w4qhysklbhdmx5y8bfrrl7ivs3g0xx5kk1i0rfy4314780xw64a") (f (quote (("force_backtrace") ("default"))))))

