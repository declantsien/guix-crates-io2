(define-module (crates-io eb ay ebay_derive) #:use-module (crates-io))

(define-public crate-ebay_derive-0.2.0 (c (n "ebay_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (d #t) (k 0)))) (h "0bdrgzprq9qq6pi035fbg9zzmqspq9yiyqbg6z9n23s9w0vrvpcz")))

