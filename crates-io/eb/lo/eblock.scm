(define-module (crates-io eb lo eblock) #:use-module (crates-io))

(define-public crate-eblock-0.0.0 (c (n "eblock") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colorize") (r "^0.1.0") (d #t) (k 0)) (d (n "futures-channel") (r "^0.3.15") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1.8.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.15.0") (f (quote ("native-tls"))) (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1llzlmw5n0amx9dwaamhwqribzbzk6w1i4rbz2hw9d39k7zalab1")))

