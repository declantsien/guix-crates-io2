(define-module (crates-io eb pf ebpf-user) #:use-module (crates-io))

(define-public crate-ebpf-user-0.1.0 (c (n "ebpf-user") (v "0.1.0") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "ebpf-user-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "libbpf-sys") (r "^0.3.0-1") (d #t) (k 0)))) (h "1ihj7r6c51mviapz2ggp02iyd6lz6h2wr65sk72w08chcnkld6fi") (f (quote (("macros" "ebpf-user-macros"))))))

(define-public crate-ebpf-user-0.1.2 (c (n "ebpf-user") (v "0.1.2") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "ebpf-user-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "libbpf-sys") (r "^0.3.0-1") (d #t) (k 0)))) (h "0mcdd7z6c33p2v1b82lvlp5rr8wiyk74xpsw0xhq7nyzxml4d0r3") (f (quote (("macros" "ebpf-user-macros"))))))

(define-public crate-ebpf-user-0.1.3 (c (n "ebpf-user") (v "0.1.3") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "ebpf-user-macros") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "libbpf-sys") (r "^0.3.0-1") (d #t) (k 0)))) (h "1l4lphgxk11121g000gkaqd85l4fi1dzvf9q2rjag9sc48jlia5a") (f (quote (("macros" "ebpf-user-macros"))))))

(define-public crate-ebpf-user-0.1.4 (c (n "ebpf-user") (v "0.1.4") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "ebpf-user-macros") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "libbpf-sys") (r "^0.3.0-1") (d #t) (k 0)))) (h "0rkzd3ihwd8zaijvskq4sgc7y4s75m13mkp9jb92sfb1y7p3ziys") (f (quote (("macros" "ebpf-user-macros"))))))

(define-public crate-ebpf-user-0.1.5 (c (n "ebpf-user") (v "0.1.5") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "ebpf-user-macros") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "libbpf-sys") (r "^0.3.0-1") (d #t) (k 0)))) (h "1q5cfdy4g8c5iacxh149xahnii4v6xrxccsplnppi8i2rpd4m8rd") (f (quote (("macros" "ebpf-user-macros"))))))

(define-public crate-ebpf-user-0.1.6 (c (n "ebpf-user") (v "0.1.6") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "ebpf-user-macros") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "libbpf-sys") (r "^0.3.0-1") (d #t) (k 0)))) (h "00m6bwv58gzhh3chsljrj94qgaidqjb8p3lkja5asp99plsydbg0") (f (quote (("macros" "ebpf-user-macros"))))))

(define-public crate-ebpf-user-0.2.0 (c (n "ebpf-user") (v "0.2.0") (d (list (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "ebpf-user-macros") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "libbpf-sys") (r "^0.8.1") (d #t) (k 0)))) (h "0xngr07mp2lcj7zx189jf5r2mqkxvr3lfrmpriarqp0vg7b87mw6") (f (quote (("macros" "ebpf-user-macros"))))))

