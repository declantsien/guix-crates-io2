(define-module (crates-io eb pf ebpf-histogram) #:use-module (crates-io))

(define-public crate-ebpf-histogram-0.1.0 (c (n "ebpf-histogram") (v "0.1.0") (d (list (d (n "aya") (r "^0.12.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.13.3") (d #t) (k 0)))) (h "1qhls1hw464vq3knkfc70c0qzdha6c7vfwchwayv3h1pggnips3v")))

