(define-module (crates-io eb pf ebpf) #:use-module (crates-io))

(define-public crate-ebpf-0.0.0 (c (n "ebpf") (v "0.0.0") (h "1pa8g56acj6w2cpkp5b05362jfw8r9v0lp597cfi6659cvxb2m4n")))

(define-public crate-ebpf-0.0.1 (c (n "ebpf") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "errno") (r "^0.2.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.35") (d #t) (k 0)) (d (n "xmas-elf") (r "^0.6.1") (d #t) (k 0)) (d (n "zero") (r "^0.1.2") (d #t) (k 0)))) (h "0f0fv38zig5w24mriq2glg1pskdh0sv25f78r16pmxxsnkk7j2by")))

(define-public crate-ebpf-0.0.2 (c (n "ebpf") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "errno") (r "^0.2.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.41") (d #t) (k 0)) (d (n "perf") (r "^0.0.1") (d #t) (k 2)) (d (n "xmas-elf") (r "^0.6.2") (d #t) (k 0)) (d (n "zero") (r "^0.1.2") (d #t) (k 0)))) (h "0k5hcbdlaszzz5ijy4by5r54wg9zx78q8ifvw4mfcdqf3ssyiayb")))

(define-public crate-ebpf-0.0.3 (c (n "ebpf") (v "0.0.3") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "errno") (r "^0.2.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.41") (d #t) (k 0)) (d (n "perf") (r "^0.0.1") (d #t) (k 2)) (d (n "xmas-elf") (r "^0.6.2") (d #t) (k 0)) (d (n "zero") (r "^0.1.2") (d #t) (k 0)))) (h "0xw0a1k277vj3p38n09fqxchkfw3risx47a70kckprbidc1yl95n")))

(define-public crate-ebpf-0.0.4 (c (n "ebpf") (v "0.0.4") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "errno") (r "^0.2.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.41") (d #t) (k 0)) (d (n "perf") (r "^0.0.1") (d #t) (k 2)) (d (n "xmas-elf") (r "^0.6.2") (d #t) (k 0)) (d (n "zero") (r "^0.1.2") (d #t) (k 0)))) (h "0lj38szlck552lbd5avzj3jh451hlfxw0q1wi130w38if9pxlkmh")))

