(define-module (crates-io eb pf ebpf-kern-macros) #:use-module (crates-io))

(define-public crate-ebpf-kern-macros-0.1.0 (c (n "ebpf-kern-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "1c4q0w0dzfgy1p16hll85f2qnyphkd1q6mbnrlv60bjmi547jc2x")))

(define-public crate-ebpf-kern-macros-0.1.2 (c (n "ebpf-kern-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "0bcl4w9m8n6xs737s9d07biaf2sm2wxcg2nf001w3vbvq5b9lp36")))

(define-public crate-ebpf-kern-macros-0.1.3 (c (n "ebpf-kern-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "10lj19yn5bn54qdqma49znkmmwk57762sirl9si9y11z6p96s2yr")))

(define-public crate-ebpf-kern-macros-0.1.4 (c (n "ebpf-kern-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "0y8snlslrpkc3rqj7yls0v4pkkxrb63hwd2dri94yc9hjq41yldx")))

(define-public crate-ebpf-kern-macros-0.1.7 (c (n "ebpf-kern-macros") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "0qky2fjyni4y0nyphblvvdaaa01n2h6kzrisy0ikvmvbqxzm09yg")))

(define-public crate-ebpf-kern-macros-0.2.0 (c (n "ebpf-kern-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "10pal2rfqm83vrgz7728ma3ac21svdn4s4713wy67w16ksiqb9h6")))

