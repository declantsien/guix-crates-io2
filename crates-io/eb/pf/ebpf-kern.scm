(define-module (crates-io eb pf ebpf-kern) #:use-module (crates-io))

(define-public crate-ebpf-kern-0.1.0 (c (n "ebpf-kern") (v "0.1.0") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "ebpf-kern-macros") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1brj6z7i64bw035b6zi5q3s2agsar7gvqzls6j0sgxk98wrmad6d") (f (quote (("macros" "ebpf-kern-macros"))))))

(define-public crate-ebpf-kern-0.1.1 (c (n "ebpf-kern") (v "0.1.1") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "ebpf-kern-macros") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0bdsw4dwqd793hy9nalspfdm67qbf2gbqnvlw5nx9gwi7525pjn4") (f (quote (("macros" "ebpf-kern-macros"))))))

(define-public crate-ebpf-kern-0.1.2 (c (n "ebpf-kern") (v "0.1.2") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "ebpf-kern-macros") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1vliifz6i3c663a39wqkm9j7b7sc2hnwm9bj6cxslm7drszn3sn6") (f (quote (("macros" "ebpf-kern-macros")))) (y #t)))

(define-public crate-ebpf-kern-0.1.3 (c (n "ebpf-kern") (v "0.1.3") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "ebpf-kern-macros") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "0fbj8zl8rdsic69d0inix5df9d5fswy8r5aqad91mxmbaj5glvfj") (f (quote (("macros" "ebpf-kern-macros"))))))

(define-public crate-ebpf-kern-0.1.4 (c (n "ebpf-kern") (v "0.1.4") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "ebpf-kern-macros") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "1acbj63vj5havhr8c74zzzmkp3xfj6n0dp1m59czyp0p6qrvpzn6") (f (quote (("macros" "ebpf-kern-macros"))))))

(define-public crate-ebpf-kern-0.1.5 (c (n "ebpf-kern") (v "0.1.5") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "ebpf-kern-macros") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "1x9v2fa66i92034xa39zpqky8lywp35g6w705iyyz8rdhfiw51nh") (f (quote (("macros" "ebpf-kern-macros"))))))

(define-public crate-ebpf-kern-0.1.6 (c (n "ebpf-kern") (v "0.1.6") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "ebpf-kern-macros") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "109prjwsdhcq659j3m7vlcjygzq17b4w8cldzxdy4sav5ysm4rad") (f (quote (("macros" "ebpf-kern-macros"))))))

(define-public crate-ebpf-kern-0.1.7 (c (n "ebpf-kern") (v "0.1.7") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "ebpf-kern-macros") (r "^0.1.7") (o #t) (d #t) (k 0)))) (h "1wa4k6w6pw7cqayy5bd19gh4hc0pcg3kgp7jm1p7lcy2x8gry068") (f (quote (("macros" "ebpf-kern-macros"))))))

(define-public crate-ebpf-kern-0.2.0 (c (n "ebpf-kern") (v "0.2.0") (d (list (d (n "ebpf-kern-macros") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "08qhx3r4jrx2x9vx05bwjr718gaafkw9mz3y166i4w788rv5gpn7") (f (quote (("macros" "ebpf-kern-macros"))))))

