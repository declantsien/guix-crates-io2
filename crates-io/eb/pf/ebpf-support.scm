(define-module (crates-io eb pf ebpf-support) #:use-module (crates-io))

(define-public crate-ebpf-support-0.1.0 (c (n "ebpf-support") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "081qkppvbqpkqdz9bn488v2qxv8gj8f7gxpdsmjhyfrx2pagsbaa")))

