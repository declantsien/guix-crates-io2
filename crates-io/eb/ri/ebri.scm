(define-module (crates-io eb ri ebri) #:use-module (crates-io))

(define-public crate-ebri-0.0.2 (c (n "ebri") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.80") (k 0)) (d (n "log") (r "^0.4.20") (k 0)) (d (n "mcslock") (r "^0.1.1") (f (quote ("lock_api"))) (k 0)) (d (n "once_cell") (r "^1.19.0") (f (quote ("portable-atomic" "race"))) (k 0)))) (h "115f565chii7s3a0klhlswlckwa2gyaav6kk13wzpxa1w507bj3q") (f (quote (("std") ("default")))) (r "1.65.0")))

(define-public crate-ebri-0.0.3 (c (n "ebri") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.80") (k 0)) (d (n "log") (r "^0.4.20") (k 0)) (d (n "mcslock") (r "^0.1.1") (f (quote ("lock_api"))) (k 0)))) (h "1a4rvh8bdd4wmf4wyr7v6cnwdh0qvxiqrr7mj6ifybdbscd1mnfh") (f (quote (("std") ("default")))) (r "1.65.0")))

(define-public crate-ebri-0.0.4 (c (n "ebri") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.80") (k 0)) (d (n "log") (r "^0.4.20") (k 0)) (d (n "mcslock") (r "^0.1.1") (f (quote ("lock_api"))) (k 0)) (d (n "portable-atomic") (r "^1.6.0") (f (quote ("fallback"))) (k 0)))) (h "0saigfgqsc015pbdfqhjrdixib0f14b17hd3vibrq9ca1qn08zyb") (f (quote (("std") ("dropguard-manually") ("dropguard-deref") ("default")))) (r "1.65.0")))

(define-public crate-ebri-0.0.5 (c (n "ebri") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.80") (k 0)) (d (n "log") (r "^0.4.20") (k 0)) (d (n "mcslock") (r "^0.1.1") (f (quote ("lock_api"))) (k 0)) (d (n "portable-atomic") (r "^1.6.0") (f (quote ("fallback"))) (k 0)))) (h "0n2pj1kfkgkmjmir9sqr72jjsdkdi770x6wc6d9jnwh2m4xix25v") (f (quote (("std") ("dropguard-manually") ("dropguard-deref") ("default")))) (r "1.65.0")))

