(define-module (crates-io eb us ebus) #:use-module (crates-io))

(define-public crate-ebus-0.1.0 (c (n "ebus") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.58") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "1hf13vjkqb8qzs3493m8zbvzjwijlwcdzfd9h7klvp2qxaiwjrcr")))

(define-public crate-ebus-1.0.0 (c (n "ebus") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1.58") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "0p8w7w0mhgyy8qx45kvdc0l2b2yssc1hn38sdi5fpz08cv658my2") (f (quote (("uuid") ("full" "uuid"))))))

(define-public crate-ebus-1.1.0 (c (n "ebus") (v "1.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "087igpjw6iplgap9axxf7r0igmqz8q9wkxh1lqbxlgk76m7nc9bz")))

(define-public crate-ebus-1.1.1 (c (n "ebus") (v "1.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0pa6b6klbmb858xqgsss6d2imgwcp5kh9l8f636347siap14jw8x")))

(define-public crate-ebus-1.2.0 (c (n "ebus") (v "1.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1fjqhs6xv6rkf7xfsq4vj7kvmh6n0axkc98kkqx08r6pr26qfzin")))

(define-public crate-ebus-1.3.0 (c (n "ebus") (v "1.3.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0b9k7pks8i2p6xm66s5a0pwdynr30kshmsd9c16dv9f6x4dkcpag") (f (quote (("default" "async_subscriber") ("async_subscriber"))))))

