(define-module (crates-io eb us ebustl) #:use-module (crates-io))

(define-public crate-ebustl-0.1.0 (c (n "ebustl") (v "0.1.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "iso6937") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^2.1") (d #t) (k 0)))) (h "1nvqbz7jklbbza9acwir8an962bpf1ali94n1xvvb0xfnf323jsx")))

(define-public crate-ebustl-0.2.0 (c (n "ebustl") (v "0.2.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "iso6937") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1j3sv4ffk3qz0x708y4ijw7cyflkvfida1324mf25ncv6dly6cq0")))

(define-public crate-ebustl-0.3.0 (c (n "ebustl") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "iso6937") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "044hw4h5wq5bp38wpdjw0j8zhnir2v1zi7n9dhp062v0asa2h14j")))

(define-public crate-ebustl-0.4.0 (c (n "ebustl") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "codepage-strings") (r "^1.0.2") (d #t) (k 0)) (d (n "iso6937") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02yl9qxnib8211ycksz042hj8bj32x356pwcz43qgmh2dk7syp3w")))

