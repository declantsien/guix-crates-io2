(define-module (crates-io eb #{90}# eb90) #:use-module (crates-io))

(define-public crate-eb90-0.1.0 (c (n "eb90") (v "0.1.0") (d (list (d (n "bytes") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "crc") (r "^2") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (o #t) (d #t) (k 0)))) (h "1xjda3671qic8rv1d0f3sn3xdcs71mjmfyq3vsb5i5xj93hy05mr") (f (quote (("default" "alloc" "codec") ("alloc")))) (y #t) (s 2) (e (quote (("codec" "alloc" "dep:tokio-util" "dep:bytes"))))))

(define-public crate-eb90-0.1.1 (c (n "eb90") (v "0.1.1") (d (list (d (n "bytes") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "crc") (r "^2") (d #t) (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "tokio-util") (r "^0.7") (f (quote ("codec"))) (o #t) (d #t) (k 0)))) (h "1z52glmyh4qri70wfncap8m055ci5gwyw6spq1bbv22jvl57ii0c") (f (quote (("default" "alloc" "codec") ("alloc")))) (s 2) (e (quote (("codec" "alloc" "dep:tokio-util" "dep:bytes"))))))

