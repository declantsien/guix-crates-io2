(define-module (crates-io eb ir ebirsu) #:use-module (crates-io))

(define-public crate-ebirsu-0.1.0 (c (n "ebirsu") (v "0.1.0") (d (list (d (n "GSL") (r "^6.0.0") (d #t) (k 0)))) (h "03y0k049zd7v1agddkbgz8fb3460mcd0cvmfmfvcmiaxgwqx07kw")))

(define-public crate-ebirsu-0.1.1 (c (n "ebirsu") (v "0.1.1") (d (list (d (n "GSL") (r "^6.0.0") (d #t) (k 0)))) (h "09wni4nc1vv4pdg8r5mdvpl8hmvk7hd60w712ym453gyk0mlan2j")))

(define-public crate-ebirsu-0.2.0 (c (n "ebirsu") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (f (quote ("openblas-static"))) (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.5.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.3") (d #t) (k 0)) (d (n "scilib") (r "^0.7.1") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "1qf5mpq30ww1f18qja79mm9sghda9dwkvc77yj6x85jv42hqzxfk")))

