(define-module (crates-io eb ur ebur128rs) #:use-module (crates-io))

(define-public crate-ebur128rs-0.2.1 (c (n "ebur128rs") (v "0.2.1") (d (list (d (n "audrey") (r "^0.2.0") (d #t) (k 2)) (d (n "biquad") (r "^0.3.1") (d #t) (k 0)) (d (n "dasp_signal") (r "^0.11.0") (d #t) (k 2)) (d (n "enum-display-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 2)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "16c1n4j0gyhri2aqvq3qjmfkgsy486ikygh2gfhham5j7f3ja0v3") (y #t)))

