(define-module (crates-io eb ml ebml-iterable-specification-derive) #:use-module (crates-io))

(define-public crate-ebml-iterable-specification-derive-0.1.0 (c (n "ebml-iterable-specification-derive") (v "0.1.0") (d (list (d (n "ebml-iterable-specification") (r "=0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ahmrg335yiajbq925mf1s5vjhmaj07lixi6kg8zjj66gix3qirb")))

(define-public crate-ebml-iterable-specification-derive-0.2.0 (c (n "ebml-iterable-specification-derive") (v "0.2.0") (d (list (d (n "ebml-iterable-specification") (r "=0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kfc991x6f5z3sn2gp135hnwg3iav1c11vqyfk7w1h162rqgjkb1")))

(define-public crate-ebml-iterable-specification-derive-0.3.0 (c (n "ebml-iterable-specification-derive") (v "0.3.0") (d (list (d (n "ebml-iterable-specification") (r "=0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v8naf2mjb0dx6c2z40akmg9nvlshclnnvlm0shricr29ip1xivb")))

(define-public crate-ebml-iterable-specification-derive-0.4.0 (c (n "ebml-iterable-specification-derive") (v "0.4.0") (d (list (d (n "ebml-iterable-specification") (r "=0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rp0gpwbbcasy8lasvzr98r9j9hhb8sxpi3i1zjdq3rh308bhrmh")))

