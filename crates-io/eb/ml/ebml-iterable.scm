(define-module (crates-io eb ml ebml-iterable) #:use-module (crates-io))

(define-public crate-ebml-iterable-0.1.0 (c (n "ebml-iterable") (v "0.1.0") (h "014543ylia7pp1vxwcq385nhqxw3h4sf3d5plhcyzb418sjiiyf7")))

(define-public crate-ebml-iterable-0.2.0 (c (n "ebml-iterable") (v "0.2.0") (d (list (d (n "ebml-iterable-specification") (r "=0.1.0") (d #t) (k 0)) (d (n "ebml-iterable-specification-derive") (r "=0.1.0") (o #t) (d #t) (k 0)))) (h "1pl1vh31b8m0sn98i3iv121lbfy17r2gfvwga5dcm6id20zfdj61") (f (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

(define-public crate-ebml-iterable-0.3.0 (c (n "ebml-iterable") (v "0.3.0") (d (list (d (n "ebml-iterable-specification") (r "=0.2.0") (d #t) (k 0)) (d (n "ebml-iterable-specification-derive") (r "=0.2.0") (o #t) (d #t) (k 0)))) (h "1s4rp3slsd6vlzkm51rq17rq18xzzrm5qf6r8hqbayc9y6pc2l1g") (f (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

(define-public crate-ebml-iterable-0.3.1 (c (n "ebml-iterable") (v "0.3.1") (d (list (d (n "ebml-iterable-specification") (r "=0.2.0") (d #t) (k 0)) (d (n "ebml-iterable-specification-derive") (r "=0.2.0") (o #t) (d #t) (k 0)))) (h "0qbals26xc3ygbb7p5c8vj81mb85ibr2rc1rywi0nlhrqm192l1v") (f (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

(define-public crate-ebml-iterable-0.3.2 (c (n "ebml-iterable") (v "0.3.2") (d (list (d (n "ebml-iterable-specification") (r "=0.2.0") (d #t) (k 0)) (d (n "ebml-iterable-specification-derive") (r "=0.2.0") (o #t) (d #t) (k 0)))) (h "0hxgnpag55ajpv85ypqbbsdgiy2blj4skf7dqj8qpd4wns7yimp6") (f (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

(define-public crate-ebml-iterable-0.4.0 (c (n "ebml-iterable") (v "0.4.0") (d (list (d (n "ebml-iterable-specification") (r "=0.3.0") (d #t) (k 0)) (d (n "ebml-iterable-specification-derive") (r "=0.3.0") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)))) (h "0a9aw1wys9a1lig4vxss7hiz83v458480y03lgaljf8vz2973rf5") (f (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

(define-public crate-ebml-iterable-0.4.1 (c (n "ebml-iterable") (v "0.4.1") (d (list (d (n "ebml-iterable-specification") (r "=0.3.0") (d #t) (k 0)) (d (n "ebml-iterable-specification-derive") (r "=0.3.0") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)))) (h "1hw8h97hgd8485mcq0cfsypipw2yzhdwq6yd56j0vpmzm82czwqn") (f (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

(define-public crate-ebml-iterable-0.4.2 (c (n "ebml-iterable") (v "0.4.2") (d (list (d (n "ebml-iterable-specification") (r "=0.3.0") (d #t) (k 0)) (d (n "ebml-iterable-specification-derive") (r "=0.3.0") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)))) (h "0y60fdca0l695x2z1wakba5ndl16gmad58lqxyp8iyrspali47dk") (f (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

(define-public crate-ebml-iterable-0.4.3 (c (n "ebml-iterable") (v "0.4.3") (d (list (d (n "ebml-iterable-specification") (r "=0.3.0") (d #t) (k 0)) (d (n "ebml-iterable-specification-derive") (r "=0.3.0") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)))) (h "1va640375d985w60x2ms1vrlnvf030l1g9pa7fhirciyxmkc4g8v") (f (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

(define-public crate-ebml-iterable-0.4.4 (c (n "ebml-iterable") (v "0.4.4") (d (list (d (n "ebml-iterable-specification") (r "=0.3.0") (d #t) (k 0)) (d (n "ebml-iterable-specification-derive") (r "=0.3.0") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)))) (h "1jdaklv9f8nj5zmpr935ii97l74ml93xwn7n29nk6y63hc33z9mg") (f (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

(define-public crate-ebml-iterable-0.5.0 (c (n "ebml-iterable") (v "0.5.0") (d (list (d (n "ebml-iterable-specification") (r "=0.4.0") (d #t) (k 0)) (d (n "ebml-iterable-specification-derive") (r "=0.4.0") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)))) (h "0g2yz9nfp1dgw67f7shby5wcrwxrvgqh5n381hl45pfad2wm7a65") (f (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

(define-public crate-ebml-iterable-0.6.0 (c (n "ebml-iterable") (v "0.6.0") (d (list (d (n "ebml-iterable-specification") (r "=0.4.0") (d #t) (k 0)) (d (n "ebml-iterable-specification-derive") (r "=0.4.0") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)))) (h "0h76gaqn1z9bwh6cy6yl3fpds19lww63m9cdc9q1mk2gza714892") (f (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

(define-public crate-ebml-iterable-0.6.1 (c (n "ebml-iterable") (v "0.6.1") (d (list (d (n "ebml-iterable-specification") (r "=0.4.0") (d #t) (k 0)) (d (n "ebml-iterable-specification-derive") (r "=0.4.0") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)))) (h "0jyxjw8hin075sl536b1sf3c6j0v70g804x3253gyn6yapj1y1i2") (f (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

(define-public crate-ebml-iterable-0.6.2 (c (n "ebml-iterable") (v "0.6.2") (d (list (d (n "ebml-iterable-specification") (r "=0.4.0") (d #t) (k 0)) (d (n "ebml-iterable-specification-derive") (r "=0.4.0") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)))) (h "1c517b4b0ahf6k4mkfsfh0hhka01qhdxfpy8cpcf3jmxdr4jam80") (f (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

