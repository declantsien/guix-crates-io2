(define-module (crates-io eb ml ebml-iterable-specification) #:use-module (crates-io))

(define-public crate-ebml-iterable-specification-0.1.0 (c (n "ebml-iterable-specification") (v "0.1.0") (h "021p181j904ymgk4v945y0bckpyrakljldxzv0v56vr0rqr0wz2j")))

(define-public crate-ebml-iterable-specification-0.2.0 (c (n "ebml-iterable-specification") (v "0.2.0") (h "074pc67ajf7m85y7dggd8sdy0fxwa8m813gb3an6pibjblm03ibn")))

(define-public crate-ebml-iterable-specification-0.3.0 (c (n "ebml-iterable-specification") (v "0.3.0") (h "1i0zrqj6j2n6vlgcdx43kxdfcj419pd6gx4hibhkpz3jf84wk4d9")))

(define-public crate-ebml-iterable-specification-0.4.0 (c (n "ebml-iterable-specification") (v "0.4.0") (h "113720zsjjcnp41z2rij03k1ks85lpm567r38ifp764s2npnfr7m")))

