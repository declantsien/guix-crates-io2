(define-module (crates-io eb nf ebnf) #:use-module (crates-io))

(define-public crate-ebnf-0.1.0 (c (n "ebnf") (v "0.1.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "parse-hyperlinks") (r "^0.23.3") (d #t) (k 0)))) (h "1ydgk4ynaa40nba52l2z6blrs1jx11di5c61xx3r46bnajqqmain")))

(define-public crate-ebnf-0.1.1 (c (n "ebnf") (v "0.1.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "parse-hyperlinks") (r "^0.23.3") (d #t) (k 0)))) (h "0lh5dfp0d1jvblj8yx7cq5vxzfw1qww184y6zcg7fpyxsipvmzzm")))

(define-public crate-ebnf-0.1.2 (c (n "ebnf") (v "0.1.2") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "parse-hyperlinks") (r "^0.23.3") (d #t) (k 0)))) (h "058fhyzwam4dvws9c6f92syp67ld12s9v4g2j6qx687sh4x9d8bj")))

(define-public crate-ebnf-0.1.3 (c (n "ebnf") (v "0.1.3") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "parse-hyperlinks") (r "^0.23.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "insta") (r "^1.26.0") (f (quote ("yaml"))) (d #t) (k 2)))) (h "167y8fv7zc9p6f8xzv8dw7fzgb4cpfkfa2x0qxyi5yv0qdbhmqhq") (f (quote (("strict"))))))

(define-public crate-ebnf-0.1.4 (c (n "ebnf") (v "0.1.4") (d (list (d (n "insta") (r "^1.26.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "parse-hyperlinks") (r "^0.23.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13szmv96fkjmlhwj2c33zmzxv7axrcas4bwpdy7cv50qrhswwalv") (f (quote (("strict"))))))

