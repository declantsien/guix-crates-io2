(define-module (crates-io eb nf ebnf-fmt) #:use-module (crates-io))

(define-public crate-ebnf-fmt-0.1.0 (c (n "ebnf-fmt") (v "0.1.0") (d (list (d (n "ebnf-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "08nvj0d5ak9624jl55y7dkavfxdkn5mqr2828wp0b13mr5hlz76b") (f (quote (("fromstr" "strum") ("default"))))))

