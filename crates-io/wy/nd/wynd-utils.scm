(define-module (crates-io wy nd wynd-utils) #:use-module (crates-io))

(define-public crate-wynd-utils-0.4.1 (c (n "wynd-utils") (v "0.4.1") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta7") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.13.1") (d #t) (k 2)) (d (n "prost") (r "^0.9") (d #t) (k 2)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1akj7ysmwqziqikihdmhl13syns67d9dgw66mjczb3c0p8zpp8ys")))

