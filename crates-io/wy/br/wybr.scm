(define-module (crates-io wy br wybr) #:use-module (crates-io))

(define-public crate-wybr-0.0.1 (c (n "wybr") (v "0.0.1") (h "111hqnmqlyyf3qvl7gwz73w7d35ssqasilrrq8yy83jz8r3z82v6")))

(define-public crate-wybr-0.0.2 (c (n "wybr") (v "0.0.2") (h "0cf1mbswymdr9h6i971jacgxxdhis4pkxg6x9258riv79nrwwqvv")))

(define-public crate-wybr-0.0.5 (c (n "wybr") (v "0.0.5") (h "1wg7wpz4zvqfhgxaf73k9vdjlcxwr62rh557ma5bn4jbnsgiphp6")))

