(define-module (crates-io wy ha wyhash) #:use-module (crates-io))

(define-public crate-wyhash-0.1.0 (c (n "wyhash") (v "0.1.0") (h "0a9h862p3agj7qpdpn0xypgaiqrd84d9r7snrkxrhd44yz8ha35z") (f (quote (("std") ("default" "std"))))))

(define-public crate-wyhash-0.2.0 (c (n "wyhash") (v "0.2.0") (h "0xza1w48yxmkq5j7p3b5kpvx5jj9j4ssczs11cci3z8k42dwk8h4")))

(define-public crate-wyhash-0.2.1 (c (n "wyhash") (v "0.2.1") (d (list (d (n "rand_core") (r "^0.4") (d #t) (k 0)))) (h "0rd39k5lw1h77j1bi42dwd3v6hpis75k3yyk5ypfivqn094jfv8a")))

(define-public crate-wyhash-0.3.0 (c (n "wyhash") (v "0.3.0") (d (list (d (n "rand_core") (r "^0.4") (d #t) (k 0)))) (h "005fad6cf4pb94dl41fp1a6z6hkv3k39klbw48b6jcy4ibs50akq")))

(define-public crate-wyhash-0.4.0 (c (n "wyhash") (v "0.4.0") (d (list (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "1zhg5qm083g2ry23099agi9q8apd41nkz6zgj6dn084ilzj2svwc")))

(define-public crate-wyhash-0.4.1 (c (n "wyhash") (v "0.4.1") (d (list (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "0754mjqx16kda0bhl5wz93dhxzpv0scfvv8cnd7mwmr7vchn3qhg")))

(define-public crate-wyhash-0.4.2 (c (n "wyhash") (v "0.4.2") (d (list (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "0m7hjqpgywxrkz1nmmywpxni8sxb6xirbzmr3w6rlsds2kjfr1ln")))

(define-public crate-wyhash-0.5.0 (c (n "wyhash") (v "0.5.0") (d (list (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "15f26hvx6nyp4d6iswha7rm3psidxa2k2iab1f1aqgsyq9iy3xms")))

