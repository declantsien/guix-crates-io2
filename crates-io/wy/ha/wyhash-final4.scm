(define-module (crates-io wy ha wyhash-final4) #:use-module (crates-io))

(define-public crate-wyhash-final4-0.1.0 (c (n "wyhash-final4") (v "0.1.0") (h "0nqm802s5rcq2xll3a4ds2zgqk7x8kvzds358iwg4v64abwnkx1v") (f (quote (("wyhash64condom") ("wyhash64") ("wyhash32condom") ("wyhash32") ("std") ("default" "wyhash32" "wyhash32condom" "wyhash64" "wyhash64condom" "std")))) (y #t)))

(define-public crate-wyhash-final4-0.1.1 (c (n "wyhash-final4") (v "0.1.1") (h "19divz6xjsgb4yilyfrc6mz1fdysbwwwl0icnwg693mbaypnhxyn") (f (quote (("wyhash64condom") ("wyhash64") ("wyhash32condom") ("wyhash32") ("std") ("default" "wyhash32" "wyhash32condom" "wyhash64" "wyhash64condom" "std")))) (y #t)))

(define-public crate-wyhash-final4-0.1.2 (c (n "wyhash-final4") (v "0.1.2") (h "0cwy75g67b6h63bhb3q7ps279la90wvrj0fznns1in0wq7a11px2") (f (quote (("wyhash64condom") ("wyhash64") ("wyhash32condom") ("wyhash32") ("std") ("default" "wyhash32" "wyhash32condom" "wyhash64" "wyhash64condom" "std")))) (y #t)))

(define-public crate-wyhash-final4-0.1.3 (c (n "wyhash-final4") (v "0.1.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1dqvz48cvch6isf9ir46wfzzna8nwdq08qjgj3dg5knkg08h3di5") (f (quote (("wyhash64condom") ("wyhash64") ("wyhash32condom") ("wyhash32") ("std") ("default" "wyhash32" "wyhash32condom" "wyhash64" "wyhash64condom" "std"))))))

