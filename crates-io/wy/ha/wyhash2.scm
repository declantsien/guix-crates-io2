(define-module (crates-io wy ha wyhash2) #:use-module (crates-io))

(define-public crate-wyhash2-0.1.0 (c (n "wyhash2") (v "0.1.0") (h "0vqjfzc8hlh6sha074bv7bzbyz23p2f93ps3infb40zx5h3pc27s") (f (quote (("nightly") ("default"))))))

(define-public crate-wyhash2-0.1.1 (c (n "wyhash2") (v "0.1.1") (h "1fd2pw9z4adrz2zrkidqf9kjwqvl9s0ia8fxhfbpnklwmw44cx4x") (f (quote (("nightly") ("default"))))))

(define-public crate-wyhash2-0.2.0 (c (n "wyhash2") (v "0.2.0") (d (list (d (n "no-std-compat") (r "^0.4") (d #t) (k 0)))) (h "1xyhmmjxrqg50kpphwhzwmxisqbmpd0d72mvq4vrwpzcj22rkw08") (f (quote (("std" "no-std-compat/std") ("nightly") ("default" "std")))) (y #t)))

(define-public crate-wyhash2-0.2.1 (c (n "wyhash2") (v "0.2.1") (d (list (d (n "no-std-compat") (r "^0.4") (d #t) (k 0)))) (h "06xgy5m270364rpdi1y0pp3zk0awdqgw77wc0fbq32ijdv4cfcwl") (f (quote (("std" "no-std-compat/std") ("nightly") ("default" "std"))))))

