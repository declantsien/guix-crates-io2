(define-module (crates-io wy sg wysgy) #:use-module (crates-io))

(define-public crate-wysgy-0.1.0 (c (n "wysgy") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wysgy_core") (r "^0.1.2") (d #t) (k 0)))) (h "084pp088ii2cxa2l96iavs5hwmpnniig99pxv13c9911jc98cpd1")))

(define-public crate-wysgy-0.1.1 (c (n "wysgy") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wysgy_core") (r "^0.1.2") (d #t) (k 0)))) (h "169rgf4lb9hpbd6pgdq00yw57vfzcpyb9wbjbnkwz3jirhasyp8p")))

(define-public crate-wysgy-0.1.2 (c (n "wysgy") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wysgy_core") (r "^0.1.2") (d #t) (k 0)))) (h "0nlb9iy6k1bw2wjxsfqp5d10kwricyl7mkiy24j6l7xsjmslddlj")))

(define-public crate-wysgy-0.1.3 (c (n "wysgy") (v "0.1.3") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wysgy_core") (r "^0.1.4") (d #t) (k 0)))) (h "1vgqfsgd5xb8pxjgzdfmwv11yfw3iiliway9lgc8kf2hwj5sp79k")))

(define-public crate-wysgy-0.1.4 (c (n "wysgy") (v "0.1.4") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "cursive") (r "^0.12.0") (d #t) (k 0)) (d (n "gag") (r "^0.1.10") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.7") (d #t) (k 0)) (d (n "log4rs") (r "^0.8.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sublime_fuzzy") (r "^0.6.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.11") (d #t) (k 0)) (d (n "wysgy_core") (r "^0.1.4") (d #t) (k 0)))) (h "0ic8bz8avzd9jlgy79jyy5bw5nk2b2x2v5kq8vcaq8g8qiy19xbz")))

