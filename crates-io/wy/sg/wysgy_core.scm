(define-module (crates-io wy sg wysgy_core) #:use-module (crates-io))

(define-public crate-wysgy_core-0.1.0 (c (n "wysgy_core") (v "0.1.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mpchbm81msimrwzxvcn9msrl3l0fxns12h6rxlwq6li064c0bh4")))

(define-public crate-wysgy_core-0.1.1 (c (n "wysgy_core") (v "0.1.1") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1iszkjv3dwdmvikqqw08i04rnx7yqhqz78wa77snjmi331m15fjy")))

(define-public crate-wysgy_core-0.1.2 (c (n "wysgy_core") (v "0.1.2") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hdd7w4k41wrcrw7cnsh5mv9xfn5vrf2lrkg7205z3k6wzj117s8")))

(define-public crate-wysgy_core-0.1.4 (c (n "wysgy_core") (v "0.1.4") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.11") (d #t) (k 0)))) (h "1nd9k34rgd83i6f5pxp4xm67n7zml4msfr08nvvb1ypasp57waak")))

(define-public crate-wysgy_core-0.1.5 (c (n "wysgy_core") (v "0.1.5") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.7") (d #t) (k 0)) (d (n "log4rs") (r "^0.8.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.11") (d #t) (k 0)))) (h "1w3zirbbfj36j23i8s31w117sh2wypdiw0q0fyig0i7s3cvcm5zc")))

