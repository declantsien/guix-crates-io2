(define-module (crates-io wy ch wycheproof) #:use-module (crates-io))

(define-public crate-wycheproof-0.1.0 (c (n "wycheproof") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0cvkxl9bv5rs6l3kxsjh749z9hsg8qwrqihmpycyzb0n9bxb51zg")))

(define-public crate-wycheproof-0.2.0 (c (n "wycheproof") (v "0.2.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "009bgqhzdzv9csld0544nxm073jhhhccm0iprgddvg3bk0d40lxc")))

(define-public crate-wycheproof-0.3.0 (c (n "wycheproof") (v "0.3.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1kkpqdg2pqarfxk6ga63dv3j94c82gmml94hf45q0msj059b0i07")))

(define-public crate-wycheproof-0.4.0 (c (n "wycheproof") (v "0.4.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1pz38g3ypaim490819m6rdk6n068nsnkmk9kmjfvfx6642b7hg0q")))

(define-public crate-wycheproof-0.5.0 (c (n "wycheproof") (v "0.5.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "072rq77c9jp4ida3ag55hd4fn30q8zvi1k5adzvgf6cwyqcggbnc") (r "1.57")))

(define-public crate-wycheproof-0.5.1 (c (n "wycheproof") (v "0.5.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "04kxlxfg7snpv729fjvs3p1c8qgd1zn1l0bqnf26a35qadrgafg6") (r "1.57")))

