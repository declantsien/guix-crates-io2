(define-module (crates-io wy nn wynncraft) #:use-module (crates-io))

(define-public crate-wynncraft-0.1.0 (c (n "wynncraft") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "1y0yvcc7cxpg1vssxmhn94imglx8p5aji3kzc4p7vwbb1m66aaz5")))

(define-public crate-wynncraft-0.2.0 (c (n "wynncraft") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "0pni1406a2c66yln1l0npldgqh5vng1lhj7ii07yydaiki9bf314")))

