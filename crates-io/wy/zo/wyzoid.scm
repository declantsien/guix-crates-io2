(define-module (crates-io wy zo wyzoid) #:use-module (crates-io))

(define-public crate-wyzoid-0.1.0 (c (n "wyzoid") (v "0.1.0") (d (list (d (n "ash") (r "^0.29") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0k06blmmfb7j7f46brjylh9myw8m00nf0d9vlk76nhnjcdciffh4")))

(define-public crate-wyzoid-0.1.1 (c (n "wyzoid") (v "0.1.1") (d (list (d (n "ash") (r "^0.29") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1nbzvgrycxrgxs7z9w3s2k655hvp1wkpyh81lfz4jrfssgvp88pz")))

(define-public crate-wyzoid-0.1.2 (c (n "wyzoid") (v "0.1.2") (d (list (d (n "ash") (r "^0.29") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0n1pirjmx39p1pn0ax420yxp9m9780525il2niww2r2q4ydx53pg")))

