(define-module (crates-io ho st hostname1-zbus) #:use-module (crates-io))

(define-public crate-hostname1-zbus-0.1.0 (c (n "hostname1-zbus") (v "0.1.0") (d (list (d (n "pico-args") (r "^0.5.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "zbus") (r "^3.0.0") (d #t) (k 0)))) (h "0m0g4r2ikqvj69a44wiyw972s9xqfcz7b0445x7mv36jmvnajyga")))

