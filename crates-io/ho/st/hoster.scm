(define-module (crates-io ho st hoster) #:use-module (crates-io))

(define-public crate-hoster-0.1.0 (c (n "hoster") (v "0.1.0") (d (list (d (n "smallvec") (r "^1.9.0") (f (quote ("const_generics" "const_new" "union"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0vb00k6s7yc5idfmds9r5vh9rp6dy51kxdq7gsafqn7q007x96ki")))

(define-public crate-hoster-0.1.1 (c (n "hoster") (v "0.1.1") (d (list (d (n "smallvec") (r "^1.9.0") (f (quote ("const_generics" "const_new" "union"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1l2cqdc26gsnparfmc7rv402q1g6akfih679lmy251pjx43s3crx")))

