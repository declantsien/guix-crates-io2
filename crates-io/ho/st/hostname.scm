(define-module (crates-io ho st hostname) #:use-module (crates-io))

(define-public crate-hostname-0.1.0 (c (n "hostname") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "winutil") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)))) (h "1jy4lfis3179l7kmwrr8x2rzrc1b5jsrqyq7qsiswjx2afc7dh2f") (f (quote (("unstable"))))))

(define-public crate-hostname-0.1.1 (c (n "hostname") (v "0.1.1") (d (list (d (n "libc") (r "^0.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "winutil") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)))) (h "1gfhad6gakmq3vxivqh91sfaglcd52fgsc7wv4dnh3i6h8w406l3") (f (quote (("unstable"))))))

(define-public crate-hostname-0.1.2 (c (n "hostname") (v "0.1.2") (d (list (d (n "libc") (r "^0.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "winutil") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)))) (h "0k5a5c628165zm3qnn6lrjlvgpll8wpz2wh2x1c0n1yrry85hhif") (f (quote (("unstable"))))))

(define-public crate-hostname-0.1.3 (c (n "hostname") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winutil") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)))) (h "0hvr2hq28w5177pgydk8rx7fzjlkgmh43hxq64f0177614az0rfq") (f (quote (("unstable"))))))

(define-public crate-hostname-0.1.4 (c (n "hostname") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winutil") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)))) (h "1jyij48azryaxdd8iccjsyldgmjvmamlsjilrns0njs3fzhvdyjq") (f (quote (("unstable"))))))

(define-public crate-hostname-0.1.5 (c (n "hostname") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(unix, target_os = \"redox\"))") (k 0)) (d (n "winutil") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)))) (h "0kprf862qaa7lwdms6aw7f3275h0j2rwhs9nz5784pm8hdmb9ki1") (f (quote (("unstable"))))))

(define-public crate-hostname-0.2.0 (c (n "hostname") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(unix, target_os = \"redox\"))") (k 0)) (d (n "match_cfg") (r "^0.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("sysinfoapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "185rylvalaqr2rbl3lss0hs12xgzxas7ynnadxmijxrqqvk60lnw")))

(define-public crate-hostname-0.3.0 (c (n "hostname") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(unix, target_os = \"redox\"))") (k 0)) (d (n "match_cfg") (r "^0.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("sysinfoapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "11kqn05h0da5ka9d7m3k93vj1vcshw6zziir3kgak2q6dn6szc81") (f (quote (("set") ("default"))))))

(define-public crate-hostname-0.3.1 (c (n "hostname") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(unix, target_os = \"redox\"))") (k 0)) (d (n "match_cfg") (r "^0.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("sysinfoapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0rz8yf70cvzl3nry71m4bz9w6x4j9kdz3qng6pnwhk2h20z1qwrw") (f (quote (("set") ("default"))))))

(define-public crate-hostname-0.4.0 (c (n "hostname") (v "0.4.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(unix, target_os = \"redox\"))") (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "windows") (r "^0.52") (f (quote ("Win32_Foundation" "Win32_System_SystemInformation"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1fpjr3vgi64ly1ci8phdqjbha4k22c65c94a9drriiqnmk4cgizr") (f (quote (("set") ("default")))) (r "1.67")))

