(define-module (crates-io ho st hostfile) #:use-module (crates-io))

(define-public crate-hostfile-0.1.0 (c (n "hostfile") (v "0.1.0") (d (list (d (n "mktemp") (r "^0.4.0") (d #t) (k 2)))) (h "0ksyyizgfar7wp5x8g0vn7ir17rpsa7k6xxh9qp2j5959ny14wk9")))

(define-public crate-hostfile-0.2.0 (c (n "hostfile") (v "0.2.0") (d (list (d (n "mktemp") (r "^0.4.0") (d #t) (k 2)))) (h "0n6nc0ckcjwhxwv0qk0w53bqdx2rm254vijhnhahlmf0ph334wy2")))

(define-public crate-hostfile-0.3.0 (c (n "hostfile") (v "0.3.0") (d (list (d (n "mktemp") (r "^0.4.0") (d #t) (k 2)))) (h "034y75axgbdkvrqzf523n6vq34q9r86nd1dhk2qbs1kgr53xhbyp")))

