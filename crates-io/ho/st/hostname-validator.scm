(define-module (crates-io ho st hostname-validator) #:use-module (crates-io))

(define-public crate-hostname-validator-1.0.0 (c (n "hostname-validator") (v "1.0.0") (h "0af10mlyz4iwgbafpsjifj0lwyglliydwz2jy0skmxnr92wvrf3h")))

(define-public crate-hostname-validator-1.1.0 (c (n "hostname-validator") (v "1.1.0") (h "1m49mnav5pipz95xq6j6bpl6masb277miq0xj3lhr7xwr2aa255y")))

(define-public crate-hostname-validator-1.1.1 (c (n "hostname-validator") (v "1.1.1") (h "1qh5sxkckalibc28029ndnfd7w0s8mwvb68d82xbb25gr55acn7m")))

