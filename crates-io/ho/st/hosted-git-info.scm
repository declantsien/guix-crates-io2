(define-module (crates-io ho st hosted-git-info) #:use-module (crates-io))

(define-public crate-hosted-git-info-0.1.0 (c (n "hosted-git-info") (v "0.1.0") (d (list (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "derive_builder") (r "^0.10.2") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "095gv8mn6hf9pmg4cwa8hrai9ddb3zrq0z56x0ndr9ygf023682d")))

(define-public crate-hosted-git-info-0.1.1 (c (n "hosted-git-info") (v "0.1.1") (d (list (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "derive_builder") (r "^0.10.2") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1qrrqqw1ni38xpv0m2m3bjhb3nfrhjdsnb6k89hizcnmrw9dhyy7")))

(define-public crate-hosted-git-info-0.1.2 (c (n "hosted-git-info") (v "0.1.2") (d (list (d (n "claim") (r "^0.5.0") (d #t) (k 2)) (d (n "derive_builder") (r "^0.10.2") (o #t) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0l49p4zcb7hmr0rb4kvpca8dsna3byd0bv03dq6kmlnd2n78zl6l")))

