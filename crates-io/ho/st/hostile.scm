(define-module (crates-io ho st hostile) #:use-module (crates-io))

(define-public crate-hostile-0.1.0 (c (n "hostile") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.4") (d #t) (k 0)))) (h "04l03pyzhpkq0l4zyraaqncssvqc81yn59avvv568mdf4ggnci7y") (y #t)))

(define-public crate-hostile-0.1.1 (c (n "hostile") (v "0.1.1") (d (list (d (n "argh") (r "^0.1.4") (d #t) (k 0)))) (h "0bwpl97b344k2hax6ayafi7a74rkg3x96dsrdhd2b0czshqnsg17") (y #t)))

(define-public crate-hostile-0.1.2 (c (n "hostile") (v "0.1.2") (d (list (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0fsb8069qa7hrcnwrly6nsypqfrj60y2ryprcn24k62s2a95v8vq") (y #t)))

(define-public crate-hostile-0.1.3 (c (n "hostile") (v "0.1.3") (d (list (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1b86k1ahfivi950c0k39zqfalvqydb3q432828s6m440krpzg34q") (y #t)))

(define-public crate-hostile-0.1.4 (c (n "hostile") (v "0.1.4") (d (list (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (d #t) (k 0)))) (h "0ni8zagc5mb14gqwnpq62ds6n9rlcd8vyjfwc10wksq7wy3dhsw3") (y #t)))

(define-public crate-hostile-0.1.5 (c (n "hostile") (v "0.1.5") (d (list (d (n "argh") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (d #t) (k 0)))) (h "01bvy41bg51g0hykq3a90m30ccv0lpifd6hn18s2blh28bhf6a3g") (y #t)))

