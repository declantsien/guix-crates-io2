(define-module (crates-io ho st hostcat) #:use-module (crates-io))

(define-public crate-hostcat-0.1.0 (c (n "hostcat") (v "0.1.0") (d (list (d (n "better-panic") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "users") (r "^0.10.0") (d #t) (k 0)))) (h "13g3mxwmwbpdc9z1rq3lr14h283d3b0fcgf7yanw780yzd03pdg6")))

(define-public crate-hostcat-0.2.0 (c (n "hostcat") (v "0.2.0") (d (list (d (n "better-panic") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "users") (r "^0.10.0") (d #t) (k 0)))) (h "1xdhcw3ijx9hd9qhgmkcm9ipwllymw7yh2k8wccnpdjyih48ba7b")))

(define-public crate-hostcat-0.3.0 (c (n "hostcat") (v "0.3.0") (d (list (d (n "better-panic") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "users") (r "^0.10.0") (d #t) (k 0)))) (h "1hn0qpngcyx9ci5a1igmybasgy66rj13yldcl9m38ffg32p2n8z4")))

(define-public crate-hostcat-0.4.0 (c (n "hostcat") (v "0.4.0") (d (list (d (n "better-panic") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "users") (r "^0.10.0") (d #t) (k 0)))) (h "1ijg5bqkpzr2p521v6mpy7x2bv8qxlxqwp9f1vsb5bcdgjd0w6k1")))

(define-public crate-hostcat-0.5.0 (c (n "hostcat") (v "0.5.0") (d (list (d (n "better-panic") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)))) (h "1sq988nb987zm92rq5s71q7kw5i4w38ga030zk70smzbp91q7vy0")))

(define-public crate-hostcat-0.5.1 (c (n "hostcat") (v "0.5.1") (d (list (d (n "better-panic") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)))) (h "0rr09ry93ylkpx3f198rx83j70lq291fg8cpvxq7ixyazv0wfvbw")))

(define-public crate-hostcat-0.5.2 (c (n "hostcat") (v "0.5.2") (d (list (d (n "better-panic") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)))) (h "1ypg88mm5lj6d7i7fp64y7nsw883ahchzms96m0wlcsj5jvgcjza")))

