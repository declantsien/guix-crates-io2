(define-module (crates-io ho st hostess) #:use-module (crates-io))

(define-public crate-hostess-0.1.0 (c (n "hostess") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "pad") (r "^0.1.5") (d #t) (k 0)))) (h "13qrl1daw7v7dpa2bqrvkc95q3w1qwrfzdxafxv1ax4i0sbvjy30")))

(define-public crate-hostess-0.1.0-beta (c (n "hostess") (v "0.1.0-beta") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "pad") (r "^0.1.5") (d #t) (k 0)))) (h "12nna8z7hkkwxmfdfjrlvp5w2yjswad658xca613wbadhspy55q8")))

(define-public crate-hostess-0.0.1 (c (n "hostess") (v "0.0.1") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "pad") (r "^0.1.5") (d #t) (k 0)))) (h "0p1q84lql9k7fw6bmazpp69mwxx52h186ipzgkq00isf0spg9kbs")))

(define-public crate-hostess-0.2.0 (c (n "hostess") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "pad") (r "^0.1.5") (d #t) (k 0)))) (h "1x62s4arslgkql7jafrawccbb1g3vvfi80ps2nvam8pr1n02pjar")))

(define-public crate-hostess-0.2.2 (c (n "hostess") (v "0.2.2") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "pad") (r "^0.1.5") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0a8610hkav8k5q545h4bc07jk0lyxj2w4hfhf5yspshdsvs2ghss")))

(define-public crate-hostess-0.2.3 (c (n "hostess") (v "0.2.3") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "pad") (r "^0.1.5") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "050dqrz0rsklxr78h15bf96h0v658xq2gsz690bvgs31yrnjj74c")))

(define-public crate-hostess-0.2.4 (c (n "hostess") (v "0.2.4") (d (list (d (n "pad") (r "^0.1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0xhrf89niksihmvncmnp0687s3hd0vkqfhvls15z6mr5kw7wjirg")))

(define-public crate-hostess-0.2.5 (c (n "hostess") (v "0.2.5") (d (list (d (n "pad") (r "^0.1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "0a0q0sq42lc9nc6li7xj6m0qcjvjy0bl9v8gxqq37kak3pbvamfm")))

(define-public crate-hostess-0.2.6 (c (n "hostess") (v "0.2.6") (d (list (d (n "pad") (r "^0.1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "11jkb59hjdqqj473yvs9jdzglicgjy505rnj63nyb333xsayidxz")))

