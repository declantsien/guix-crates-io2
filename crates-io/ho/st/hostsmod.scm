(define-module (crates-io ho st hostsmod) #:use-module (crates-io))

(define-public crate-hostsmod-0.2.2 (c (n "hostsmod") (v "0.2.2") (d (list (d (n "hostname") (r "^0.3.1") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "09s2iyzcfcd2im0npkkss3vgmpz21rasvwhsyvaic8ykifmjwmhg")))

