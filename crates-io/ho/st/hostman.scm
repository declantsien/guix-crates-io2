(define-module (crates-io ho st hostman) #:use-module (crates-io))

(define-public crate-hostman-0.1.0 (c (n "hostman") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "parse-hosts") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0q4nwvvgvf1fi4bmn4nqn78w56g6ikij0qvrshskxan81rwh6lkd")))

(define-public crate-hostman-0.1.1 (c (n "hostman") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "parse-hosts") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1kig2bssm77fjl29y0q5rdbdv21hlnd7wbl40scfnlzrm5hx8amd")))

(define-public crate-hostman-0.1.2 (c (n "hostman") (v "0.1.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "parse-hosts") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1mf4vv115qz7w58n6gcs3bj7m9s3ikp1dym43zf58707kq07csrp")))

(define-public crate-hostman-0.1.3 (c (n "hostman") (v "0.1.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "parse-hosts") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1wyr1x6rp51xl21knzvmjpg951xcgmhp7v13rb3cbckbjb4m0pkd")))

(define-public crate-hostman-0.1.4 (c (n "hostman") (v "0.1.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "parse-hosts") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1z7aq22ldyabw5lp21bjx9j93976ylnw37zg4mwxbpz8lg0fbxrq")))

(define-public crate-hostman-0.1.5 (c (n "hostman") (v "0.1.5") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "parse-hosts") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "15fskcg11hwsacf539c791qg2pvd905yvfmzjpkim4n2a1njqxy9")))

(define-public crate-hostman-0.1.6 (c (n "hostman") (v "0.1.6") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "parse-hosts") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0051r3i5di85qvjpkm2kh280l3y3li06jqdsrvv8rh4nwxik5hdj")))

(define-public crate-hostman-0.1.7 (c (n "hostman") (v "0.1.7") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "parse-hosts") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "147m5jjnwvfs8q57rw79m8n8xdv8jmzzhg1yxz327hz81r6ydah1")))

(define-public crate-hostman-0.1.8 (c (n "hostman") (v "0.1.8") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "parse-hosts") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0by1f6lyx209r1nswsdbd2wrpwrl4p2wnf719mb1qj2wf2hw3mvk")))

(define-public crate-hostman-0.2.0 (c (n "hostman") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "parse-hosts") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "08g40nq3if3x598b3dp00ws0b4zigwk4pb3sqmklikn2ikx50jx0")))

(define-public crate-hostman-0.3.0 (c (n "hostman") (v "0.3.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "parse-hosts") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "self_update") (r "^0.5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "17vap9sdwjd4ggfsp8rvbx77vxhkjngl1x9wyr6pkgmnk3hs3ddl")))

(define-public crate-hostman-0.3.1 (c (n "hostman") (v "0.3.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "parse-hosts") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "self_update") (r "^0.5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "05rzc48q0rq066pbdp7491sq1adw9cpcqggxrzr9y53czgikcjv1")))

(define-public crate-hostman-0.3.2 (c (n "hostman") (v "0.3.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "parse-hosts") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "self_update") (r "^0.5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0qv48vjrp7xvx3dpjcaxf66bcpnh1s8m4fw5ka2xlx5750i1c2f9")))

(define-public crate-hostman-0.4.0 (c (n "hostman") (v "0.4.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "parse-hosts") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "self_update") (r "^0.5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "05xidssdi7qf134n8ipgv57ma9s2h2q9qjcdb96q5iqx2zmsmlqb")))

(define-public crate-hostman-0.5.0 (c (n "hostman") (v "0.5.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "hosts-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^5.0.0-alpha2") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "self_update") (r "^0.5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0l30wd24amrnnniwrrdl1bvc9i8mgwnm1mm72sg545qhm6v78dpp")))

(define-public crate-hostman-0.5.1 (c (n "hostman") (v "0.5.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "hosts-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "self_update") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)))) (h "1r8vb55zxhknk2zpi6m86gq8bvwp78iggn3kfrsl5km8g3kn3z5g")))

(define-public crate-hostman-0.5.2 (c (n "hostman") (v "0.5.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.9.0") (d #t) (k 0)) (d (n "hosts-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "self_update") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)))) (h "0mgnwm07fyzjrpb8y49bq90m47am3k7q8hfqsxgbplhnag7ndhib")))

