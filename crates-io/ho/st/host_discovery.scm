(define-module (crates-io ho st host_discovery) #:use-module (crates-io))

(define-public crate-host_discovery-0.1.0 (c (n "host_discovery") (v "0.1.0") (h "0q1mnk795ginl2ppk6z3shj1nm4155q83v3j4dk5my0cba69rd7q") (y #t)))

(define-public crate-host_discovery-0.1.1 (c (n "host_discovery") (v "0.1.1") (h "1lhsnis01i82gqz55ham3kg8in5i8rx00llhz2sk5vdwlhx2ydqf") (y #t)))

(define-public crate-host_discovery-0.1.2 (c (n "host_discovery") (v "0.1.2") (h "073b3z0lrmxz8llq7ybikbknsf2bmmn526v0cjwhps8079lllq19") (y #t)))

(define-public crate-host_discovery-0.1.3 (c (n "host_discovery") (v "0.1.3") (h "0dm9ddklsrrmb4lqbrmkd9qwh1bgb5l9x9pgg995sb818x4244cc") (y #t)))

(define-public crate-host_discovery-0.1.4 (c (n "host_discovery") (v "0.1.4") (h "0z04bf4w58g439mrgv0wynrj7wxvqk9vm4qwg9qcd3jzbnk5z7p0") (y #t)))

(define-public crate-host_discovery-0.1.5 (c (n "host_discovery") (v "0.1.5") (h "0ap0113idwjcglqlpfb73lpxym4q3q1p41pjwzf5mx6bd2whxivq") (y #t)))

(define-public crate-host_discovery-0.1.6 (c (n "host_discovery") (v "0.1.6") (h "0vrzp5l9x1dsil6240hqrh6sxm7svxnqdjiw9y6g2y1k7pqjqahq") (y #t)))

(define-public crate-host_discovery-0.5.0 (c (n "host_discovery") (v "0.5.0") (h "1a3k370qmhp2yn6qrarnbwdqgbc718r8sr79m41sgb520c55rsw6") (y #t)))

(define-public crate-host_discovery-1.0.0 (c (n "host_discovery") (v "1.0.0") (h "1zqasdzsycrk3dm646zapd3rqc2hjndqakbnhpkhgmmncw8hpfrg")))

(define-public crate-host_discovery-1.0.1 (c (n "host_discovery") (v "1.0.1") (h "1acyksgyjb667gwh8qk05wishz6bg78jvylqfcfhb5z7c1jk25m3")))

(define-public crate-host_discovery-1.0.2 (c (n "host_discovery") (v "1.0.2") (h "0il4p8pxd6imk5i3w3igkwlpw4vf78w3825m3lf5pinaikra5z50")))

(define-public crate-host_discovery-1.0.3 (c (n "host_discovery") (v "1.0.3") (h "0yimnclmrxw6bizk2bn560bh1mjw03c8vz49kiln18ghyxylmgf1")))

(define-public crate-host_discovery-1.0.4 (c (n "host_discovery") (v "1.0.4") (h "0p3jfr1bwfnhixd5hvzq25bp2vwr17wmfpxqxrfp0xlihzn01c9w")))

(define-public crate-host_discovery-1.0.5 (c (n "host_discovery") (v "1.0.5") (h "0y85ak5yfnqrvxdlk6v5fz0919b334l7hbgcyzpjvl1n9w358gqz")))

(define-public crate-host_discovery-1.0.6 (c (n "host_discovery") (v "1.0.6") (h "1q4ap8yxn2frv826y85lgckxc0y500j0x8kav5azb1c8hbadl502")))

(define-public crate-host_discovery-1.0.7 (c (n "host_discovery") (v "1.0.7") (h "0v7457wvmw19xcsac3py37imy70797095ll2zhijlp7kdajspyqq")))

(define-public crate-host_discovery-1.0.8 (c (n "host_discovery") (v "1.0.8") (h "0vmbnzy6h7dbw7813aihlahzjpy1gj4glv4ic46961r01bd7brxb")))

(define-public crate-host_discovery-1.0.9 (c (n "host_discovery") (v "1.0.9") (h "1qbi9kyiwy2xzzwl96wq33zl257qvyzyrpvsf17qvjm0j5rc77wq")))

(define-public crate-host_discovery-1.2.0 (c (n "host_discovery") (v "1.2.0") (h "0n2bmbzh2a26k569pi2gbn6wnv9dh6wc9zpf7hb5vyd4rp8nqchy")))

(define-public crate-host_discovery-1.2.1 (c (n "host_discovery") (v "1.2.1") (h "1rl3krqjf9hgaw5ga6lyb9pmk2safm35walzwwvfcymv8l2ascn8")))

(define-public crate-host_discovery-1.2.2 (c (n "host_discovery") (v "1.2.2") (h "1jkrxin1c2m2sw1pkgv2sgh9gygrgbnmj35skyqrv215wr8g3pi3")))

(define-public crate-host_discovery-1.2.3 (c (n "host_discovery") (v "1.2.3") (h "1b40lml9d4cbzd44rf9vfxqvj14lf1k8xngfys55f7g26a285izh")))

(define-public crate-host_discovery-1.2.5 (c (n "host_discovery") (v "1.2.5") (h "1mp05n95n7ngy65vfj2q11i8v1690gikv82lvwnhlk4jbl1mlgpq")))

(define-public crate-host_discovery-1.3.0 (c (n "host_discovery") (v "1.3.0") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (k 0)))) (h "0ikdr4455dkffayzykvzjc1vr93k71m36jg4wxzcn0fny00rkyjh")))

(define-public crate-host_discovery-1.3.1 (c (n "host_discovery") (v "1.3.1") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0yvysdx7fac35zvh2hf2pr2n896gb4x1k4akx758wy16lq7lcfir")))

(define-public crate-host_discovery-1.3.2 (c (n "host_discovery") (v "1.3.2") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0vhnrq8xv64abab0vw65lfwjz50nv9s0s41gf2yj05ixjz91g9cw")))

(define-public crate-host_discovery-1.3.3 (c (n "host_discovery") (v "1.3.3") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "133cx12zisw6ciksrcwza19j9q1499nm3k5knzlwdrjvh5km2mkh")))

(define-public crate-host_discovery-1.3.4 (c (n "host_discovery") (v "1.3.4") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "152dr5wwn0xnfzay7f5j3s6hll84w2pvawa317b3lydi2sm9swbm")))

(define-public crate-host_discovery-1.4.0 (c (n "host_discovery") (v "1.4.0") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "01xxqcsw1k2jgradpxrw7m5qqqskpc087h9rphw54g12nn0jqzn5")))

(define-public crate-host_discovery-1.5.0 (c (n "host_discovery") (v "1.5.0") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0xw8rlcj1xrjjb204fs3bsb7d3wh4wdl1vcq9cw90rvavcqzwx2f")))

(define-public crate-host_discovery-1.5.1 (c (n "host_discovery") (v "1.5.1") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "186xrzx8dm1pkdrdw4ryx0dfvnfsqly9b5pvw7gh7j7fj6a2b6da")))

(define-public crate-host_discovery-1.5.2 (c (n "host_discovery") (v "1.5.2") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0s6gv1l0xarbasfgd2938b3pz3sdir6qmk4jfm6gflz8q3745pp4")))

(define-public crate-host_discovery-1.5.3 (c (n "host_discovery") (v "1.5.3") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "124lx2qlnjvqxsqx3pip4zwv9wy7i3254av4d85hlf4wgg1avk6s")))

(define-public crate-host_discovery-1.5.4 (c (n "host_discovery") (v "1.5.4") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0bb25b1palks45smkagnv24cr7i8cyg2dqncicfbna0vlmbjy9w3")))

(define-public crate-host_discovery-1.5.5 (c (n "host_discovery") (v "1.5.5") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0dkgrwsqfyhv1yw5nnbc0zbbarqmrpms1yhr1221hy0d6br7daf6")))

(define-public crate-host_discovery-1.5.6 (c (n "host_discovery") (v "1.5.6") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "1zixzqdxbalg382h23jlxmgjz6smhzcij6fkdinqv2d36bq72j14")))

(define-public crate-host_discovery-1.5.7 (c (n "host_discovery") (v "1.5.7") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "1aln62mhl22h2hzbbzwxb158ph8a6cn37wggbiigiflxkslwxljp")))

(define-public crate-host_discovery-1.5.8 (c (n "host_discovery") (v "1.5.8") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "11rbkla74ls6lzj56vyczsigmvrjybv3ng51pibvcm8cb0ygxcxm")))

(define-public crate-host_discovery-1.5.9 (c (n "host_discovery") (v "1.5.9") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "1y5rc9xir1lbn3qmlrfpjxra8gx2qyk67hgaf6xlp5aqshizk3d5")))

(define-public crate-host_discovery-1.6.0 (c (n "host_discovery") (v "1.6.0") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "1kz74jgr2iqj17vmv9akcwwz90l1c21pp8fkz32xxl3rlmmzp477")))

(define-public crate-host_discovery-1.6.1 (c (n "host_discovery") (v "1.6.1") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "1wd2hljbnjhixydlhnnnhkljhz8zgajcgnl0ynh3kvh2ssh7jccj")))

(define-public crate-host_discovery-1.6.2 (c (n "host_discovery") (v "1.6.2") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "1s370rl8wa6ggjcx06zsz90bnzrk122c07d5r4wj2iap6aq1ng0g")))

(define-public crate-host_discovery-1.6.3 (c (n "host_discovery") (v "1.6.3") (d (list (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0jsp6xhxwklx4i4jmgadksw1hpxmqmmj35d3dbqwrcdvq5fxa54z")))

(define-public crate-host_discovery-1.6.4 (c (n "host_discovery") (v "1.6.4") (d (list (d (n "winreg") (r "^0.52.0") (o #t) (d #t) (t "cfg(windows)") (k 0)))) (h "17vpi89v4cf5zmm5isrccaxbrpvxv7i9m7dqf9nj0afj0bhx2m6p") (s 2) (e (quote (("windows" "dep:winreg"))))))

(define-public crate-host_discovery-1.6.5 (c (n "host_discovery") (v "1.6.5") (d (list (d (n "winreg") (r "^0.52.0") (o #t) (d #t) (t "cfg(windows)") (k 0)))) (h "03mj2lz959h1yhxk942y2brdzkw9hvknkmdqq68d3w2mw0kcm5db") (s 2) (e (quote (("windows" "dep:winreg"))))))

(define-public crate-host_discovery-1.6.6 (c (n "host_discovery") (v "1.6.6") (d (list (d (n "winreg") (r "^0.52.0") (o #t) (d #t) (t "cfg(windows)") (k 0)))) (h "0bipar0a6vq6kclzwf1m6h9k76p81hdv99cha9nj876m7xb48phh") (s 2) (e (quote (("windows" "dep:winreg"))))))

(define-public crate-host_discovery-1.6.7 (c (n "host_discovery") (v "1.6.7") (d (list (d (n "winreg") (r "^0.52.0") (o #t) (d #t) (t "cfg(windows)") (k 0)))) (h "0ydzrh8jbd06l3m4kyv8y3rzv1whlym96d8w3wym8vjlf3wjhpb9") (s 2) (e (quote (("windows" "dep:winreg"))))))

(define-public crate-host_discovery-1.6.8 (c (n "host_discovery") (v "1.6.8") (d (list (d (n "winreg") (r "^0.52.0") (o #t) (d #t) (t "cfg(windows)") (k 0)))) (h "0fh0l5za0vma8qaxnsa02y3v6yg6l6dhrbs2wp68rgr4v24d7i5z") (s 2) (e (quote (("windows" "dep:winreg"))))))

(define-public crate-host_discovery-1.7.0 (c (n "host_discovery") (v "1.7.0") (d (list (d (n "winreg") (r "^0.52.0") (o #t) (d #t) (t "cfg(windows)") (k 0)))) (h "0ghnchmm1h5f5r9y3a1ci83hxcwbaa85ybj080i5hb400xlkzkh0") (s 2) (e (quote (("windows" "dep:winreg"))))))

(define-public crate-host_discovery-1.7.1 (c (n "host_discovery") (v "1.7.1") (d (list (d (n "winreg") (r "^0.52.0") (o #t) (d #t) (t "cfg(windows)") (k 0)))) (h "187nfsjxrml78naki13xlj5p6z6wnyvvxbpbrnlza82zdzi8fcpr") (s 2) (e (quote (("windows" "dep:winreg"))))))

(define-public crate-host_discovery-1.8.0 (c (n "host_discovery") (v "1.8.0") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "winreg") (r "^0.52.0") (o #t) (d #t) (t "cfg(windows)") (k 0)))) (h "10sk6ikg5r7rdii3llvjr0bl7j2w5xjv2rip726by6j9dmv0hj36") (s 2) (e (quote (("windows" "dep:winreg"))))))

(define-public crate-host_discovery-1.8.5 (c (n "host_discovery") (v "1.8.5") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.19.1") (d #t) (k 0)) (d (n "winreg") (r "^0.52.0") (o #t) (d #t) (t "cfg(windows)") (k 0)))) (h "18glqvgjngk84p0gcy32cpwsav0nidm7nwhpa1fvsp7dipr5394r") (s 2) (e (quote (("windows" "dep:winreg"))))))

(define-public crate-host_discovery-1.9.0 (c (n "host_discovery") (v "1.9.0") (d (list (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.19.1") (d #t) (k 0)) (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0vsq6k46pb8za0qg2m92qg6yqwsx6xvlgkdkx979jg89n909441r")))

(define-public crate-host_discovery-1.9.1 (c (n "host_discovery") (v "1.9.1") (d (list (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.19.1") (d #t) (k 0)) (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "1cyvkai1n9vqa4l1nb3as2d1n1g4nk5svxiwbz1fzvvk91474zfz")))

(define-public crate-host_discovery-1.9.2 (c (n "host_discovery") (v "1.9.2") (d (list (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "wgpu") (r "^0.19.1") (d #t) (k 0)) (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "1l8naa1z4s6ygvq953wyk2r3q2b81vsgsm2mj3j61gl7icgwxkwz")))

