(define-module (crates-io ho st hosts-parser) #:use-module (crates-io))

(define-public crate-hosts-parser-0.1.0 (c (n "hosts-parser") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1vflvmh6kpzwp9r4zivqfna0m5rkdm3cv0qv6wl3izhk52aj932l")))

