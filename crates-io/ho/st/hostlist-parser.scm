(define-module (crates-io ho st hostlist-parser) #:use-module (crates-io))

(define-public crate-hostlist-parser-0.1.0 (c (n "hostlist-parser") (v "0.1.0") (d (list (d (n "combine") (r "^4.0.0-beta.1") (d #t) (k 0)) (d (n "insta") (r "^0.11") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "03z9i1vm757ics5svvlyngxylkb17sslyy42vm3vw047a6vpcn5s")))

(define-public crate-hostlist-parser-0.1.2 (c (n "hostlist-parser") (v "0.1.2") (d (list (d (n "combine") (r "^4.0.1") (d #t) (k 0)) (d (n "insta") (r "^0.13") (d #t) (k 2)) (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "042il2zrlxmcibp79ns5rdvllbi8rz2ygzzy85d6p5arwjvazvnr")))

(define-public crate-hostlist-parser-0.1.3 (c (n "hostlist-parser") (v "0.1.3") (d (list (d (n "combine") (r "^4.1.0") (d #t) (k 0)) (d (n "insta") (r "^0.16") (d #t) (k 2)) (d (n "itertools") (r "^0.9") (d #t) (k 0)))) (h "02mkn7a10727c8fygr2hsldfhvlrxxil9qlpbs2bx5gc6mlyhx5y")))

(define-public crate-hostlist-parser-0.1.4 (c (n "hostlist-parser") (v "0.1.4") (d (list (d (n "combine") (r "^4.6") (d #t) (k 0)) (d (n "insta") (r "^1.12") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "1amnv00sdfc90qnvgqjqxzzmc9rf4h6n6wdyd3q50b9j3s2qki5m")))

(define-public crate-hostlist-parser-0.1.5 (c (n "hostlist-parser") (v "0.1.5") (d (list (d (n "combine") (r "^4.6") (d #t) (k 0)) (d (n "insta") (r "^1.12") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "1d6n7maz19rfnxni0kfi7lh8jlmvrm6wskn6hl399sycsqnnfpv6")))

(define-public crate-hostlist-parser-0.1.6 (c (n "hostlist-parser") (v "0.1.6") (d (list (d (n "combine") (r "^4.6") (d #t) (k 0)) (d (n "insta") (r "^1") (d #t) (k 2)) (d (n "itertools") (r "^0.12") (d #t) (k 0)))) (h "14c7l4pdj7p0g64iw7jbh9qlph7r0x19f06pipfksnyi8lz6a49s")))

