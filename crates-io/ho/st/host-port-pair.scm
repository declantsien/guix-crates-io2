(define-module (crates-io ho st host-port-pair) #:use-module (crates-io))

(define-public crate-host-port-pair-0.1.0 (c (n "host-port-pair") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1722wgg9b17qgvlm3549v5j6bm4mksg78mlabg523lipcn1xqxkb")))

(define-public crate-host-port-pair-0.1.1 (c (n "host-port-pair") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0369y5mz1550db3g0mh7v89vzbqq91qys00wszra0r2b37g8b0j2")))

