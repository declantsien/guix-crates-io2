(define-module (crates-io ho st hostlist) #:use-module (crates-io))

(define-public crate-hostlist-0.1.0 (c (n "hostlist") (v "0.1.0") (d (list (d (n "nom") (r "^4.1") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "13cpnnzpwlsqgi9kkjw0q3n38s238450cbp6prxyhfrslk55k11v")))

(define-public crate-hostlist-0.1.1 (c (n "hostlist") (v "0.1.1") (d (list (d (n "nom") (r "^4.1") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "0kqiqvrgfnpq8w83kg224hj4c82fhdhw58vd41225kjlrcg3vww1")))

(define-public crate-hostlist-0.1.2 (c (n "hostlist") (v "0.1.2") (d (list (d (n "nom") (r "^4.1") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "189hwd7p6969dzlhdmdl1syiwa2j3hcrwxcxjbmsxj806jdjnb0h")))

(define-public crate-hostlist-0.2.0 (c (n "hostlist") (v "0.2.0") (d (list (d (n "nom") (r "^4.1") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "09didbcymppwx2pc7krm1si3q10yiajdzsibgmx94q6wxpa9fz7a")))

(define-public crate-hostlist-0.2.1 (c (n "hostlist") (v "0.2.1") (d (list (d (n "nom") (r "^4.1") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "03vnrg9hlmsa9y8fagxgaf8n77yb51ymjcwqq9dk1j2vcckhaysz")))

(define-public crate-hostlist-0.3.0 (c (n "hostlist") (v "0.3.0") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)))) (h "021qbn5xg0m0dndp3967mqgrc0671c4xhvv2fhlndvp7fcjp3z6g") (f (quote (("default" "clap"))))))

(define-public crate-hostlist-0.3.1 (c (n "hostlist") (v "0.3.1") (d (list (d (n "clap") (r "^2") (o #t) (d #t) (k 0)) (d (n "nom") (r "^6.0") (d #t) (k 0)))) (h "0vixwyw5l4mngkafa8d36z6g4jm1ay9mjhsn61z7gm7df32ay6w1") (f (quote (("default" "clap"))))))

