(define-module (crates-io ho us householder) #:use-module (crates-io))

(define-public crate-householder-0.0.1 (c (n "householder") (v "0.0.1") (d (list (d (n "approx") (r "^0.5") (f (quote ("num-complex"))) (d #t) (k 0)) (d (n "cauchy") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "matrixmultiply") (r "^0.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0f1x2n9pxnyrhnzszm534hk88657jcmkkif15p2nn7bf4av31i2p") (y #t)))

