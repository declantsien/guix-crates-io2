(define-module (crates-io ho us houseflow) #:use-module (crates-io))

(define-public crate-houseflow-0.5.0 (c (n "houseflow") (v "0.5.0") (d (list (d (n "actix-rt") (r "^2.2.0") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.8") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "clap_generate") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "houseflow-api") (r "^0.1.0") (f (quote ("auth" "fulfillment"))) (o #t) (d #t) (k 0)) (d (n "houseflow-config") (r "^0.1.0") (f (quote ("server" "device" "client" "postgres" "fs"))) (d #t) (k 0)) (d (n "houseflow-db") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "houseflow-device") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "houseflow-server") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "houseflow-types") (r "^0.1.0") (f (quote ("token"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "szafka") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("sync" "rt-multi-thread" "macros" "fs"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "xdg") (r "^2.2.0") (d #t) (k 0)))) (h "0c774x5jyf6nzm5222fn1n0pby75k7p3zfdr6mg5l1pb5fzlifhg") (f (quote (("server" "houseflow-server" "houseflow-db" "actix-web") ("device" "houseflow-device") ("default" "server" "client" "device") ("client" "houseflow-api" "szafka" "dialoguer")))) (y #t)))

(define-public crate-houseflow-0.5.1 (c (n "houseflow") (v "0.5.1") (d (list (d (n "actix-rt") (r "^2.2.0") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.8") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "clap_generate") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "houseflow-api") (r "^0.1.1") (f (quote ("auth" "fulfillment" "admin"))) (o #t) (d #t) (k 0)) (d (n "houseflow-config") (r "^0.1.1") (f (quote ("server" "device" "client" "postgres" "fs"))) (d #t) (k 0)) (d (n "houseflow-db") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "houseflow-device") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "houseflow-server") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "houseflow-types") (r "^0.1.1") (f (quote ("token"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "szafka") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("sync" "rt-multi-thread" "macros" "fs"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "xdg") (r "^2.2.0") (d #t) (k 0)))) (h "1wiavi2hadpj2m07rvdxcx4hcnfi3fmhncq03hh3haharvr63wy8") (f (quote (("server" "houseflow-server" "houseflow-db" "actix-web") ("device" "houseflow-device") ("default" "server" "client" "device") ("client" "houseflow-api" "szafka" "dialoguer" "semver")))) (y #t)))

(define-public crate-houseflow-0.5.2 (c (n "houseflow") (v "0.5.2") (d (list (d (n "actix-rt") (r "^2.2.0") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.8") (o #t) (d #t) (k 0)) (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "clap_generate") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "houseflow-api") (r "^0.1.1") (f (quote ("auth" "fulfillment" "admin"))) (o #t) (d #t) (k 0)) (d (n "houseflow-config") (r "^0.1.1") (f (quote ("server" "device" "client" "postgres" "fs"))) (d #t) (k 0)) (d (n "houseflow-db") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "houseflow-device") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "houseflow-server") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "houseflow-types") (r "^0.1.1") (f (quote ("token"))) (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "semver") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "szafka") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("sync" "rt-multi-thread" "macros" "fs"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "xdg") (r "^2.2.0") (d #t) (k 0)))) (h "0v4y6ppqkc7bcp0h7a4v626kf0iybnfvf2rdxaj23wm9lxr71k97") (f (quote (("server" "houseflow-server" "houseflow-db" "actix-web") ("device" "houseflow-device") ("default" "server" "client" "device") ("client" "houseflow-api" "szafka" "dialoguer" "semver")))) (y #t)))

(define-public crate-houseflow-0.1.0 (c (n "houseflow") (v "0.1.0") (h "0mld96vy0pkgw5bkqjwvw6fc0pk3l6kwir5vbhfcb73zl8lcp71p") (y #t)))

