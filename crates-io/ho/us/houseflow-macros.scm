(define-module (crates-io ho us houseflow-macros) #:use-module (crates-io))

(define-public crate-houseflow-macros-0.1.0 (c (n "houseflow-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0q372bvl7c3vaakrffg3zy1grbgfp5yqnbxvymczwzhp9gd56d9w")))

(define-public crate-houseflow-macros-0.1.1 (c (n "houseflow-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bkr79f4gbb1g7drajsslqb193sli6z4qd3cl6v641wvyjr7490c")))

