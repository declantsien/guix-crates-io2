(define-module (crates-io ho us housekeeper) #:use-module (crates-io))

(define-public crate-housekeeper-0.1.0 (c (n "housekeeper") (v "0.1.0") (d (list (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "paw") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (f (quote ("paw"))) (d #t) (k 0)))) (h "043qyp43n9sc9shnbql1rip9wa8zw48mzhcg4qgwdqxbkmfliv73")))

