(define-module (crates-io ho ro horokai-network) #:use-module (crates-io))

(define-public crate-horokai-network-0.1.0 (c (n "horokai-network") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "0gkg5v5ymzfijd1rcjm4z879ial6dyrwg28d4y38dfswlhbqh04y")))

(define-public crate-horokai-network-0.1.1 (c (n "horokai-network") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "08nsdg73hrj5l14cpg1x2qn4w71hidjfar9nfsxz9ylz4qx8gj1s")))

