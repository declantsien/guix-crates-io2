(define-module (crates-io ho is hoist_temporaries) #:use-module (crates-io))

(define-public crate-hoist_temporaries-0.1.0 (c (n "hoist_temporaries") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "05g5m1vqh08vwyma7yh6bcq3yvbigbp2h0adnbhvdl7jgdhrg061")))

(define-public crate-hoist_temporaries-0.2.0 (c (n "hoist_temporaries") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1zgf1w7am7634v0db9xnxlxjqg3kg7c6b93xafhkyh53cdfcn3hl")))

(define-public crate-hoist_temporaries-0.2.1 (c (n "hoist_temporaries") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0jfa9malc33g5f9x065pxn2alplrgdj0nv63hgy3wh88c6mgpr1p")))

