(define-module (crates-io ho od hoodlum-parser) #:use-module (crates-io))

(define-public crate-hoodlum-parser-0.1.0 (c (n "hoodlum-parser") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.11.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.11.0") (d #t) (k 0)))) (h "1fiq8h26py4yfxic6rgfcdjwihhf94i9iz0pv1v8zg1m8ncb9sjx")))

(define-public crate-hoodlum-parser-0.2.0 (c (n "hoodlum-parser") (v "0.2.0") (d (list (d (n "lalrpop") (r "^0.11.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.11.0") (d #t) (k 0)))) (h "06dhszmkw56nn82zmcwvg18df0fn3x3qgm2535qil4avjdj5zzyw")))

(define-public crate-hoodlum-parser-0.4.0 (c (n "hoodlum-parser") (v "0.4.0") (d (list (d (n "lalrpop") (r "^0.11.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.11.0") (d #t) (k 0)))) (h "1pk8cn50v5qnnlxf1vlp9vzbpyhmhbmk2qg2p4vg68bc9dsbrjr4")))

(define-public crate-hoodlum-parser-0.4.1 (c (n "hoodlum-parser") (v "0.4.1") (d (list (d (n "lalrpop-util") (r "^0.12") (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 1)))) (h "0673vvvsfwldq06bpkr0kjq9b7wp48iwj5wygrbshvibsf12gwdw")))

(define-public crate-hoodlum-parser-0.4.2 (c (n "hoodlum-parser") (v "0.4.2") (d (list (d (n "lalrpop-util") (r "^0.12") (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 1)))) (h "19m46266nqyldjh9n9s6vm2q767a8c54l42x7zdqfbxl4v4n6zcw")))

(define-public crate-hoodlum-parser-0.5.0 (c (n "hoodlum-parser") (v "0.5.0") (d (list (d (n "lalrpop-util") (r "^0.12") (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 1)))) (h "17350hxrz4qm39q4irfy5xpg8wxmz73azkpwhglkksfagv2ywjd9")))

