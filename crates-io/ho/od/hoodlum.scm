(define-module (crates-io ho od hoodlum) #:use-module (crates-io))

(define-public crate-hoodlum-0.1.0 (c (n "hoodlum") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.11.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.11.0") (d #t) (k 0)))) (h "0gw5016c46hi6drbzmhy27dqyk8did8bpm968xi9sch3srdj2pmn")))

(define-public crate-hoodlum-0.2.0 (c (n "hoodlum") (v "0.2.0") (d (list (d (n "hoodlum-parser") (r "^0.2.0") (d #t) (k 0)) (d (n "lalrpop-util") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "14n3kb0mrn6paxzrlbvl5f9a7fhg1bfx06vj2w2s81a5xx219ga4")))

(define-public crate-hoodlum-0.4.0-pre (c (n "hoodlum") (v "0.4.0-pre") (d (list (d (n "clap") (r "^2.19") (f (quote ("wrap_help" "suggestions"))) (k 0)) (d (n "hoodlum-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.5.4") (d #t) (k 0)) (d (n "lalrpop-util") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "maplit") (r "^0.1.4") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0a848592z33y97rxk2wmp6av51gkqxhif3b17bb18xljag5jlxm5")))

(define-public crate-hoodlum-0.4.0 (c (n "hoodlum") (v "0.4.0") (d (list (d (n "clap") (r "^2.19") (f (quote ("wrap_help" "suggestions"))) (k 0)) (d (n "hoodlum-parser") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.5.4") (d #t) (k 0)) (d (n "lalrpop-util") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "maplit") (r "^0.1.4") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1qcgsm0lnbw7hwf0mplzwbrym6wfkywal9wzgdfaqv20whk519w5")))

(define-public crate-hoodlum-0.5.0 (c (n "hoodlum") (v "0.5.0") (d (list (d (n "clap") (r "^2.19") (f (quote ("wrap_help" "suggestions"))) (k 0)) (d (n "hoodlum-parser") (r "^0.5.0") (d #t) (k 0)) (d (n "itertools") (r "^0.5.4") (d #t) (k 0)) (d (n "lalrpop-util") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "maplit") (r "^0.1.4") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1nvldxsr87pl4lvgcnlc39y91rh87pk0zq51y4qwfgyqj4nqqk48")))

