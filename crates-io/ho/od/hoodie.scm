(define-module (crates-io ho od hoodie) #:use-module (crates-io))

(define-public crate-hoodie-0.2.0 (c (n "hoodie") (v "0.2.0") (d (list (d (n "dioxus") (r "^0.3.2") (d #t) (k 0)) (d (n "dioxus") (r "^0.3.2") (d #t) (k 2)) (d (n "dioxus-desktop") (r "^0.3.0") (d #t) (k 2)) (d (n "dioxus-ssr") (r "^0.3.0") (d #t) (k 2)) (d (n "generational-arena") (r "^0.2.8") (d #t) (k 0)))) (h "1qbl3rlb1hk62g9hymfizfxjz4v79rfdsr781il041bicfgw9vqw")))

