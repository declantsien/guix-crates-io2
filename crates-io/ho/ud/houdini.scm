(define-module (crates-io ho ud houdini) #:use-module (crates-io))

(define-public crate-houdini-1.0.0 (c (n "houdini") (v "1.0.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("fileapi" "handleapi" "libloaderapi" "ntdef" "minwindef" "winnt" "minwinbase"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "09kdgaf0g7v07wipywyi4v9j3h6bqc4rhdwi6bh5p6qwzwsar5iy") (f (quote (("debug"))))))

(define-public crate-houdini-1.0.1 (c (n "houdini") (v "1.0.1") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("fileapi" "handleapi" "libloaderapi" "ntdef" "minwindef" "winnt" "minwinbase"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "19p0a1339phz4kiwsqbnf1pn396k8gyh9jpvrdyva1pd49n7j8my") (f (quote (("debug"))))))

(define-public crate-houdini-1.0.2 (c (n "houdini") (v "1.0.2") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("fileapi" "handleapi" "libloaderapi" "ntdef" "minwindef" "winnt" "minwinbase"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1vvyp5ri4r8v91f642crm3y36282s4gqm3qsds9ybsrivx3w08cs") (f (quote (("debug"))))))

(define-public crate-houdini-2.0.0 (c (n "houdini") (v "2.0.0") (d (list (d (n "windows") (r "^0.46.0") (f (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_LibraryLoader" "Win32_Security"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1m9sm5zq7ixxiyfzsyph4fz0rpyaac2mmrzd2app4znzi8jl1zk5") (f (quote (("debug"))))))

