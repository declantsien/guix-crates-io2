(define-module (crates-io ho lm holmes) #:use-module (crates-io))

(define-public crate-holmes-0.1.0 (c (n "holmes") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "postgres") (r "^0.13") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "0b1lalix7zvaxr58hg8a8w11i8s06qkxi7dikaj0ihi334hv2aya")))

