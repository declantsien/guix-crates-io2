(define-module (crates-io ho wm howmuch) #:use-module (crates-io))

(define-public crate-howmuch-0.1.0 (c (n "howmuch") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1b76h08hngwgxiy1qsgkb79dab4i0fzbys7pc88ca2kmyh76iwz3")))

(define-public crate-howmuch-0.1.1 (c (n "howmuch") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "05lspyhmm7lvhqi3571vixkqncvynryxqc5k1hwv5fivac6crvgf")))

(define-public crate-howmuch-0.1.2 (c (n "howmuch") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1hi4kndnwhh0jk0n2kgi5xnsrsac4iwnprjh0w52dfzdqw66hyy2")))

