(define-module (crates-io ho nk honk-rpc) #:use-module (crates-io))

(define-public crate-honk-rpc-0.1.0 (c (n "honk-rpc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bson") (r "^2.2.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0mrcsn0qv0y2wf4fiq22v06n1p73ix8l581nzkhi0v25q2ib9nbr") (r "1.66")))

(define-public crate-honk-rpc-0.1.1 (c (n "honk-rpc") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bson") (r ">=2.0, <=2.4.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2") (d #t) (k 2)) (d (n "sha3") (r "^0.10") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ahfwp54v43v8dc9l0s1pgxd30sd27alfrgmwkqfq7vbjlvfpc4c") (r "1.63")))

