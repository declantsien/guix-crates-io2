(define-module (crates-io ho rt horticulteur) #:use-module (crates-io))

(define-public crate-horticulteur-1.0.0 (c (n "horticulteur") (v "1.0.0") (h "1d6psfwskyip7dp51299m2qjlzcwvrnpwbk7xdwdqfn9pqxyz83j")))

(define-public crate-horticulteur-1.1.0 (c (n "horticulteur") (v "1.1.0") (h "000f6i1aybhv3ajka1sbqdmql7fwm8vvlhpcv60czd11a4p2krgb")))

