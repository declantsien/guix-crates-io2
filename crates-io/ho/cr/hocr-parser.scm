(define-module (crates-io ho cr hocr-parser) #:use-module (crates-io))

(define-public crate-hocr-parser-0.1.0 (c (n "hocr-parser") (v "0.1.0") (d (list (d (n "roxmltree") (r "^0.19.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "11h0vbk7g9xhavdql2g05dln6rmf3ds1wikzshcp0w185qvibmyj") (s 2) (e (quote (("serde" "dep:serde"))))))

