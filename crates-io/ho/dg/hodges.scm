(define-module (crates-io ho dg hodges) #:use-module (crates-io))

(define-public crate-hodges-0.1.0 (c (n "hodges") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.47.1") (d #t) (k 1)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cmake") (r "^0.1.35") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0y1ps66yh2dbl2bk7jv64vw3vch2zpylx31kak868j5g3xwwqw7k")))

(define-public crate-hodges-1.0.0 (c (n "hodges") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.47.1") (d #t) (k 1)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cmake") (r "^0.1.35") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ri366p32p5dh6943mrwa8a5pjbk9zj8caybjir4rnf9y3pxx6xa")))

