(define-module (crates-io ho dg hodgepodge) #:use-module (crates-io))

(define-public crate-hodgepodge-0.1.0 (c (n "hodgepodge") (v "0.1.0") (d (list (d (n "strum") (r "^0.19.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.19.1") (d #t) (k 0)))) (h "0d6x7xc71k70dz3xa1chfnv8xvfkz2z9zn00csqq3l65n9spyfxf")))

(define-public crate-hodgepodge-0.1.2 (c (n "hodgepodge") (v "0.1.2") (d (list (d (n "strum") (r "^0.19.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.19.1") (d #t) (k 0)))) (h "1y4lrli0ac4lhfacwrsaj8pazim5kb04sbjd74ipnnl92vbq1vvx")))

(define-public crate-hodgepodge-0.1.3 (c (n "hodgepodge") (v "0.1.3") (d (list (d (n "strum") (r "^0.19.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.19.1") (d #t) (k 0)))) (h "1ghyc3d962vjrf8n9pb5s79r6n6b7d3qv904zqcjag8nh5ajv7j1")))

(define-public crate-hodgepodge-0.1.4 (c (n "hodgepodge") (v "0.1.4") (d (list (d (n "strum") (r "^0.19.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.19.1") (d #t) (k 0)))) (h "1d86kg0h2z6p5pih18k3wvfb3nbng34r45vg4lzkkydl3hxcm8hd")))

(define-public crate-hodgepodge-0.1.5 (c (n "hodgepodge") (v "0.1.5") (d (list (d (n "strum") (r "^0.19.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.19.1") (d #t) (k 0)))) (h "1lbpz9vmljc3kwfdmqc9jxn5yk97yg8api8v8a63fp91kjbqwlkv")))

