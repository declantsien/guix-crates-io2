(define-module (crates-io ho t- hot-reload-server) #:use-module (crates-io))

(define-public crate-hot-reload-server-0.0.0 (c (n "hot-reload-server") (v "0.0.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (k 0)) (d (n "hyper") (r "^0.13.3") (d #t) (k 0)) (d (n "notify") (r "^4.0.15") (d #t) (k 0)) (d (n "tokio") (r "^0.2.13") (f (quote ("macros" "rt-core" "fs" "io-std"))) (d #t) (k 0)))) (h "0b3mmsr5k0vfi9hr7xyl7dck9snkmjv5yydbcnkarzgx25wj26xd")))

