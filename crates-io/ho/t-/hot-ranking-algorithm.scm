(define-module (crates-io ho t- hot-ranking-algorithm) #:use-module (crates-io))

(define-public crate-hot-ranking-algorithm-1.0.1 (c (n "hot-ranking-algorithm") (v "1.0.1") (d (list (d (n "clippy") (r "^0.0.124") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 0)))) (h "0f78i17wcln541m9n4mdivb8rbdj82ffgpr75rzhjvf7bplc57rk")))

(define-public crate-hot-ranking-algorithm-2.0.0 (c (n "hot-ranking-algorithm") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)))) (h "1qbwswcr01cczlg03fkfcaxva35q52saih4ppli62dkv2xqazw0c")))

