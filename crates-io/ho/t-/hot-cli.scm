(define-module (crates-io ho t- hot-cli) #:use-module (crates-io))

(define-public crate-hot-cli-0.1.0 (c (n "hot-cli") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (k 0)) (d (n "mio") (r "^0.8.4") (f (quote ("os-ext" "os-poll"))) (k 0)))) (h "16bdqxw1gpy5136xpwjxhs1apbld5g3mknida9wc5s7il0yrb9bv")))

