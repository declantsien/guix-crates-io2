(define-module (crates-io ho t- hot-lib-reloader-macro) #:use-module (crates-io))

(define-public crate-hot-lib-reloader-macro-0.4.0 (c (n "hot-lib-reloader-macro") (v "0.4.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05y8pp01blspqx7bxrhppr7v58jdrnvfxgdw7yi5ssjdh5rlr831")))

(define-public crate-hot-lib-reloader-macro-0.4.1 (c (n "hot-lib-reloader-macro") (v "0.4.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hq45mpcyvxyvl4zigazl87cwpbfvpdlfzqqpan79fpriw4sa0xb")))

(define-public crate-hot-lib-reloader-macro-0.4.2 (c (n "hot-lib-reloader-macro") (v "0.4.2") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18ba3hwf9y3sn87i1sky7mr9n0k1pgwwchb00jc5yfnb0j6a8qd6")))

(define-public crate-hot-lib-reloader-macro-0.4.3 (c (n "hot-lib-reloader-macro") (v "0.4.3") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13wxmn0lar0xnsd8mpjmyy3zcqwbw1yxywvfy3rlzxbsabigbwnr")))

(define-public crate-hot-lib-reloader-macro-0.4.4 (c (n "hot-lib-reloader-macro") (v "0.4.4") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0j6xxpr0syjb9v0wd22b2br3b4nqxhc9l4n20rcbi45ljhaf1mgp")))

(define-public crate-hot-lib-reloader-macro-0.5.0 (c (n "hot-lib-reloader-macro") (v "0.5.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0211v33zsrz32xfa7a1c2aym0fkf7ljzc07lagmgppzbvy9v5s43")))

(define-public crate-hot-lib-reloader-macro-0.5.1 (c (n "hot-lib-reloader-macro") (v "0.5.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1idqlym9m4456nadf4b3a4lldsg34pjxwankc41wlf0h4sngzn0j")))

(define-public crate-hot-lib-reloader-macro-0.5.2 (c (n "hot-lib-reloader-macro") (v "0.5.2") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16w37zivcdslmzncj31v8y99lwmz27q4yh93yn9kfc3x8d6vzfky")))

(define-public crate-hot-lib-reloader-macro-0.5.3 (c (n "hot-lib-reloader-macro") (v "0.5.3") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ali2hp5hrhml31jwg84ym9x2k84d84l0fl7rrmja3kfvj9xl7rr")))

(define-public crate-hot-lib-reloader-macro-0.5.4 (c (n "hot-lib-reloader-macro") (v "0.5.4") (d (list (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1inwrkr102cnc9h2wzm1jra3flxndmhrhdnyrpbp31llnyh139f2")))

(define-public crate-hot-lib-reloader-macro-0.5.5 (c (n "hot-lib-reloader-macro") (v "0.5.5") (d (list (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1052rmmlh0ss8ywqws2k6d7kr24v2lrarg3f9lss8qphgwcajh80")))

(define-public crate-hot-lib-reloader-macro-0.5.6 (c (n "hot-lib-reloader-macro") (v "0.5.6") (d (list (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03xp679v65n1smrx7fdp2kymhx24k4xq5qw2c18qbzynp50rwdxl")))

(define-public crate-hot-lib-reloader-macro-0.6.0 (c (n "hot-lib-reloader-macro") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0y7j022k8wydqhr1mkgq9q7d58gbl5w2vky5kwyl7dr0v8h574jh")))

(define-public crate-hot-lib-reloader-macro-0.6.1 (c (n "hot-lib-reloader-macro") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02zkipr2k2f1czqy5b8gcaxfjlnrapvdzipnhbhih1prfr2bmwry")))

(define-public crate-hot-lib-reloader-macro-0.6.2 (c (n "hot-lib-reloader-macro") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16rm2828xr3bhpy2m0imv3r6lbsywn03zwkqvwymdfzid4drpq6h")))

(define-public crate-hot-lib-reloader-macro-0.6.3 (c (n "hot-lib-reloader-macro") (v "0.6.3") (d (list (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "024vv2yp332g2qjmpgjjsvdh7k34l2p38nr8470mab9nzpnx8kxr")))

(define-public crate-hot-lib-reloader-macro-0.6.4 (c (n "hot-lib-reloader-macro") (v "0.6.4") (d (list (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "066d5qr1kmsf5c1aihbcrrr7dyfjapn73lshkvmjybr4hjssf59j")))

(define-public crate-hot-lib-reloader-macro-0.6.5 (c (n "hot-lib-reloader-macro") (v "0.6.5") (d (list (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qkpww2i8kmn7qaxac97q4nddx4drxy8l2ncs2vad5gs7651ymf5")))

(define-public crate-hot-lib-reloader-macro-0.7.0 (c (n "hot-lib-reloader-macro") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "000830a588xfjrgvvn7bd47sr8kb309pxnyca3gf5n5xx8jnq77h")))

