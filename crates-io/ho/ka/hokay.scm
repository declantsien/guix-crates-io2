(define-module (crates-io ho ka hokay) #:use-module (crates-io))

(define-public crate-hokay-0.1.0 (c (n "hokay") (v "0.1.0") (d (list (d (n "hyper") (r "^0.14") (f (quote ("http1" "runtime" "server"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "net" "rt" "signal" "sync"))) (d #t) (k 0)))) (h "0z6jldmjlacm3lahdzclh2dya1dl97k22w9mlqm74cjmq2ga8bzm")))

(define-public crate-hokay-0.2.0 (c (n "hokay") (v "0.2.0") (d (list (d (n "clap") (r "^3") (f (quote ("cargo" "derive" "std"))) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("http1" "runtime" "server"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "net" "rt" "signal" "sync"))) (k 0)))) (h "1k9bnfg9xj8praq4zqzmhdbjx5hc6xmfhvb17d5b05dncah8bxng")))

(define-public crate-hokay-0.2.1 (c (n "hokay") (v "0.2.1") (d (list (d (n "clap") (r "^3") (f (quote ("cargo" "derive" "std"))) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("http1" "runtime" "server"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "net" "rt" "signal" "sync"))) (k 0)))) (h "17z9swzvp88r6x8zq3sd91ylj0gh0p6y7fqxkkn5pv5nq3lrh3bz")))

(define-public crate-hokay-0.2.2 (c (n "hokay") (v "0.2.2") (d (list (d (n "clap") (r "^3") (f (quote ("cargo" "derive" "std"))) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("http1" "runtime" "server"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "net" "rt" "signal" "sync"))) (k 0)))) (h "00cazfwvacmx37x2817d6ka3y0pri5x84jnczv2085jsxhr9p695")))

