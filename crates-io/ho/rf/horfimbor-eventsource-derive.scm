(define-module (crates-io ho rf horfimbor-eventsource-derive) #:use-module (crates-io))

(define-public crate-horfimbor-eventsource-derive-0.1.0 (c (n "horfimbor-eventsource-derive") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bdp1bh80lf50wkpx4cld7wipg2im3snviq4310vxv3mrg0wm73c")))

(define-public crate-horfimbor-eventsource-derive-0.1.1 (c (n "horfimbor-eventsource-derive") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vkqspgn5fwi863id8l169rx8kzhrp4pc8gwlr56sazp2bf0ci2w")))

(define-public crate-horfimbor-eventsource-derive-0.1.2 (c (n "horfimbor-eventsource-derive") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "098hiz0s9zdlswxxbra93z6cml4gj9l4z1v2axmjl44c01k83gn5")))

(define-public crate-horfimbor-eventsource-derive-0.1.3 (c (n "horfimbor-eventsource-derive") (v "0.1.3") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dzn3xg6f824n8i490d6z4zjcf7cs9fldma4pispnkxvx2lwxlac")))

(define-public crate-horfimbor-eventsource-derive-0.1.4 (c (n "horfimbor-eventsource-derive") (v "0.1.4") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06iazh8nzq68g8i6zindgprkml0lx0lbgfz5sy7xjp2sp54sw4s2")))

