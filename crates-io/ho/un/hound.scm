(define-module (crates-io ho un hound) #:use-module (crates-io))

(define-public crate-hound-0.1.0 (c (n "hound") (v "0.1.0") (h "01bx5zh7sdm8bp29hi6nf1mw1j07krn67zlbcjz596nmjy1wax17")))

(define-public crate-hound-0.2.0 (c (n "hound") (v "0.2.0") (h "0ygpfajhinhswb45b5lfkm0k43j0bpblazq0sipwlqx0346r2xpq")))

(define-public crate-hound-0.3.0 (c (n "hound") (v "0.3.0") (d (list (d (n "cpal") (r "^0.0.20") (d #t) (k 2)))) (h "1152prsw04wbdb91nnyxl39amv3b9cskpn1pvzyf200c60ry0qmf")))

(define-public crate-hound-0.4.0 (c (n "hound") (v "0.4.0") (d (list (d (n "cpal") (r "^0.0.20") (d #t) (k 2)))) (h "0900i14g0fdnp29fgsb225kn6b6y7zafznnba50gw3x0mvqgvyq8")))

(define-public crate-hound-1.0.0 (c (n "hound") (v "1.0.0") (d (list (d (n "cpal") (r "^0.0.20") (d #t) (k 2)))) (h "0fpy6xw8zlyb960y820z0mz90k34avg1mnvf7vlpnzv0277d8xw6")))

(define-public crate-hound-1.1.0 (c (n "hound") (v "1.1.0") (d (list (d (n "cpal") (r "^0.1.2") (d #t) (k 2)))) (h "1a2nfs282d59rf1fwjmyb0ja6jdq8kkimbn6vdlih6i5mn6azckw")))

(define-public crate-hound-2.0.0 (c (n "hound") (v "2.0.0") (d (list (d (n "cpal") (r "^0.1.2") (d #t) (k 2)))) (h "03xxag5ypqwm4qazwlybzh0q3ch88laxjvw0cvfcrlq7cbi1z5qy")))

(define-public crate-hound-3.0.0 (c (n "hound") (v "3.0.0") (d (list (d (n "cpal") (r "^0.1.2") (d #t) (k 2)))) (h "0c25k7rwgwr0bp9i7ffkqsigyb4kbnwa6x8n418pyk0v8y4y1k6b")))

(define-public crate-hound-3.0.1 (c (n "hound") (v "3.0.1") (d (list (d (n "cpal") (r "^0.1.2") (d #t) (k 2)))) (h "1gpkb4vga89n0qa6sqms963vcarqrczq5pnkw7fh576q50w4cr04")))

(define-public crate-hound-3.1.0 (c (n "hound") (v "3.1.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)) (d (n "lazy_static") (r "= 0.2.0") (d #t) (k 2)))) (h "1v32wl159flpr87wg1ihgkq5xl8qcaibx5v28mwcagsrmhvbhlfp")))

(define-public crate-hound-3.2.0 (c (n "hound") (v "3.2.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "1f9bg04xdsy2wpkvfnbzzf53dki3szhis4gasby1r44j8spfx0c7")))

(define-public crate-hound-3.3.0 (c (n "hound") (v "3.3.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "1s8vxy7g7ccvs2q2aavfrm1xzldsgk9x828nz7hcggszc4drpkig")))

(define-public crate-hound-3.3.1 (c (n "hound") (v "3.3.1") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "1q50z1i47bqdwl9bxiaqd6c8bn74rip0k583av5hbsj10k1r7p73")))

(define-public crate-hound-3.4.0 (c (n "hound") (v "3.4.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "0jbm25p2nc8758dnfjan1yk7hz2i85y89nrbai14zzxfrsr4n5la")))

(define-public crate-hound-3.5.0 (c (n "hound") (v "3.5.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "1cadkxzdsb3bxwzri6r6l78a1jy9j0jxrfwmh34gjadvbnyws4sd")))

(define-public crate-hound-3.5.1 (c (n "hound") (v "3.5.1") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "0kw5yybfc7hdwxwm6d3m3h4ms52fkw0n0zch35drb52ci2xsmbb2")))

