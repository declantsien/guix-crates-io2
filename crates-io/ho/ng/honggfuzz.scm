(define-module (crates-io ho ng honggfuzz) #:use-module (crates-io))

(define-public crate-honggfuzz-0.1.0 (c (n "honggfuzz") (v "0.1.0") (h "0nf3p3rl6hykkwfmcfqw3n1c6k1y5kdjm5ky0p08pyvf3xqyk4yf") (y #t)))

(define-public crate-honggfuzz-0.1.1 (c (n "honggfuzz") (v "0.1.1") (h "00jy4dlj032wcaxqjqyrr6bmlz2hvdnwqkwgg02wd0rldbqwia0l")))

(define-public crate-honggfuzz-0.2.0 (c (n "honggfuzz") (v "0.2.0") (h "031607kj1b198k028fc4pcajmq9s2hcfprxla3rms4vpz8gpzdkh")))

(define-public crate-honggfuzz-0.2.1 (c (n "honggfuzz") (v "0.2.1") (h "0fvhazgrig9yxq7b04nli2g47krxx32fsf5kzridhy4zkplrw1lj")))

(define-public crate-honggfuzz-0.3.0 (c (n "honggfuzz") (v "0.3.0") (h "110vm72jxc9crdd4xrmxxpzm2zm1wyfcikyjnjfnqjhfk42r35jx")))

(define-public crate-honggfuzz-0.4.0 (c (n "honggfuzz") (v "0.4.0") (h "0p7h4r6f5a2yjrpc6w01dlynlws5hnyb4b255kw4kwmkj45jiib0")))

(define-public crate-honggfuzz-0.5.0 (c (n "honggfuzz") (v "0.5.0") (d (list (d (n "memmap") (r "^0.6") (d #t) (t "cfg(fuzzing_debug)") (k 0)))) (h "1294ip9xmhnc241p4rr0lb67nnyasbg9mhz1rpqbngl7k9xw15zc")))

(define-public crate-honggfuzz-0.5.1 (c (n "honggfuzz") (v "0.5.1") (d (list (d (n "memmap") (r "^0.6") (d #t) (t "cfg(fuzzing_debug)") (k 0)))) (h "0shwr0pagph054n8knqy6l35jyh308y9ssk9m2jafl20shc13sxg")))

(define-public crate-honggfuzz-0.5.2 (c (n "honggfuzz") (v "0.5.2") (d (list (d (n "memmap") (r "^0.6") (d #t) (t "cfg(fuzzing_debug)") (k 0)))) (h "13bc02pzmx5r965iz0rkabznzzfwsr13806wfbmc8ywcgr2zzwaa")))

(define-public crate-honggfuzz-0.5.3 (c (n "honggfuzz") (v "0.5.3") (d (list (d (n "memmap") (r "^0.6") (d #t) (t "cfg(fuzzing_debug)") (k 0)))) (h "174xc0969kxywvixcjgv6a9d3yp3awkf3pbq669119l8bb6cayqf")))

(define-public crate-honggfuzz-0.5.4 (c (n "honggfuzz") (v "0.5.4") (d (list (d (n "memmap") (r "^0.6") (d #t) (t "cfg(fuzzing_debug)") (k 0)))) (h "1k00wfjn5d3bkwafrmiig44zhi2y2bcwlds1sx36irbrglmbwiby") (y #t)))

(define-public crate-honggfuzz-0.5.5 (c (n "honggfuzz") (v "0.5.5") (d (list (d (n "memmap") (r "^0.6") (d #t) (t "cfg(fuzzing_debug)") (k 0)))) (h "19m294iizspw5lfc7gr2b3d3jhq75qn8fky467d7cg3mrslvryag")))

(define-public crate-honggfuzz-0.5.6 (c (n "honggfuzz") (v "0.5.6") (d (list (d (n "memmap") (r "^0.6") (d #t) (t "cfg(fuzzing_debug)") (k 0)))) (h "0zasvzyyxq1k3dy2qjrvnz96ni03719ryikz2b3bv3r661jgharn")))

(define-public crate-honggfuzz-0.5.7 (c (n "honggfuzz") (v "0.5.7") (d (list (d (n "memmap") (r "^0.6") (d #t) (t "cfg(fuzzing_debug)") (k 0)))) (h "12b4m1wgir05yjr7mhfpfs640k97ncw2d5zalrgq9l9q7ykrhjq0")))

(define-public crate-honggfuzz-0.5.8 (c (n "honggfuzz") (v "0.5.8") (d (list (d (n "memmap") (r "^0.6") (d #t) (t "cfg(fuzzing_debug)") (k 0)))) (h "1j7kx31qq8zj86ir2knd7bs13256qbfap4q6yc7nrdl4sla7sf2v") (y #t)))

(define-public crate-honggfuzz-0.5.9 (c (n "honggfuzz") (v "0.5.9") (d (list (d (n "memmap") (r "^0.6") (d #t) (t "cfg(fuzzing_debug)") (k 0)))) (h "0f81mj0nkw7n7h35zzilphk482ri5v98385pbpvfx54x6jkgv2ka")))

(define-public crate-honggfuzz-0.5.10 (c (n "honggfuzz") (v "0.5.10") (d (list (d (n "memmap") (r "^0.6") (d #t) (t "cfg(fuzzing_debug)") (k 0)))) (h "02kfv8gpjs0c9ixp0bs6d3wac3i7fx7l84gs990gcaj1hs9f480n")))

(define-public crate-honggfuzz-0.5.11 (c (n "honggfuzz") (v "0.5.11") (d (list (d (n "memmap") (r "^0.6") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0q8c07gyhaa2zx5yh9xh5yiswq6gw1mpr3n4jiip2k4qs7imslkr")))

(define-public crate-honggfuzz-0.5.12 (c (n "honggfuzz") (v "0.5.12") (d (list (d (n "memmap") (r "^0.6") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0vjcm2333b6fz6md77idyiszjr5vpn4h6pz8fa2kk1jnx4bbda03")))

(define-public crate-honggfuzz-0.5.13 (c (n "honggfuzz") (v "0.5.13") (d (list (d (n "memmap") (r "^0.6") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0byhx08kg0gq6hzaqsbsbmzy8kqnzhzb2ww3w6ndjzr2lqn9ji6r")))

(define-public crate-honggfuzz-0.5.14 (c (n "honggfuzz") (v "0.5.14") (d (list (d (n "memmap") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0bcg61n0yqp1ggc8ky2nwf03ggpbd2mrn6mrkr6azqkv75ql5z54")))

(define-public crate-honggfuzz-0.5.15 (c (n "honggfuzz") (v "0.5.15") (d (list (d (n "memmap") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1zql0l4259m8ay6c10g051fl7d2smwikhl17gf709gklrbdf5p85")))

(define-public crate-honggfuzz-0.5.16 (c (n "honggfuzz") (v "0.5.16") (d (list (d (n "memmap") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0xshc9q2f7243p5iyfw224lfihfy8n8an8yypga838p9r8s18gmr")))

(define-public crate-honggfuzz-0.5.17 (c (n "honggfuzz") (v "0.5.17") (d (list (d (n "memmap") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0apx7mgksih02myff5qs7i2yxbssi8nzgxy35ksx59vsacq8civa")))

(define-public crate-honggfuzz-0.5.18 (c (n "honggfuzz") (v "0.5.18") (d (list (d (n "memmap") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0l28a9ar8lhkigrcrg8cmmjkmgbcpnjkdqz2g8h6y0i2pxy472ma")))

(define-public crate-honggfuzz-0.5.19 (c (n "honggfuzz") (v "0.5.19") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1znhj7zzrz945iavy6gn6wcihbyzz7a07l948klfz8pgp2iykcxg")))

(define-public crate-honggfuzz-0.5.20 (c (n "honggfuzz") (v "0.5.20") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.6") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0l4ckzagyc7nhw14pybwjnfgbvxvr5rh24mf00iplql2v928hr7b")))

(define-public crate-honggfuzz-0.5.21 (c (n "honggfuzz") (v "0.5.21") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.6") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0321p8llldnpbysd7v2i4caa5f81lf2f8sxw43a317iqcry66hf9")))

(define-public crate-honggfuzz-0.5.22 (c (n "honggfuzz") (v "0.5.22") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.6") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0h7gxpr4ixcv31rmvld2llfffmp3jg2zhp51hybiby78xlv8r7qx")))

(define-public crate-honggfuzz-0.5.23 (c (n "honggfuzz") (v "0.5.23") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.6") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "086ya6r0d6vbhmz6dbwjpnfgj8qz4pzs0fnw03mfkrzzzs6yvkj1")))

(define-public crate-honggfuzz-0.5.24 (c (n "honggfuzz") (v "0.5.24") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.6") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1sjrhw4xzg161wzmfd0rjlsn3qy4nislqrd4y50lgwc1055zflqi")))

(define-public crate-honggfuzz-0.5.25 (c (n "honggfuzz") (v "0.5.25") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.6") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0ky5k04ypjfifllk5dvjg9xxwdgr3d2dn67hkagk8fw7ijcj0x0n")))

(define-public crate-honggfuzz-0.5.26 (c (n "honggfuzz") (v "0.5.26") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.6") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0hzxb89xdkrd4zmbd14kswamk1ixghfvpqvsf85cn76xq2svvfbr")))

(define-public crate-honggfuzz-0.5.27 (c (n "honggfuzz") (v "0.5.27") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.6") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1gjvz6mf0j7y93rgsjw38canabpbny2sn8mv66arwkvq0dz3piyx")))

(define-public crate-honggfuzz-0.5.28 (c (n "honggfuzz") (v "0.5.28") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.6") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "05bw97gxcsddvplr0rh37fpq8ipdc05vbf4a0i8kqvybajip5dlb")))

(define-public crate-honggfuzz-0.5.29 (c (n "honggfuzz") (v "0.5.29") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.6") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "16varddpn94qqris27v52zx0m26cj8gyzvfvmg5lpc34l4lyd3mv")))

(define-public crate-honggfuzz-0.5.30 (c (n "honggfuzz") (v "0.5.30") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0lcbnh6my1agjvfwghg5d9n3sdf4g156ahn6p8bnvysjb7r78q38")))

(define-public crate-honggfuzz-0.5.31 (c (n "honggfuzz") (v "0.5.31") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1a3ag079z74pl8q4pjyngqcfhkfv0ma5p84v43xvf6jv1zllnd9y")))

(define-public crate-honggfuzz-0.5.32 (c (n "honggfuzz") (v "0.5.32") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0v3x8gf52wc4v0wmsb7wdczys8069x23ncp3nqkjj4yfxlrwn97v")))

(define-public crate-honggfuzz-0.5.33 (c (n "honggfuzz") (v "0.5.33") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0qh3f7xpk7rd5nx57ajj8kgp3m25vdnqrj111bgbpswdff5y656p")))

(define-public crate-honggfuzz-0.5.34 (c (n "honggfuzz") (v "0.5.34") (d (list (d (n "arbitrary") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1cg822wcfh4mi20qm4av0984b4f6pdyxcay0afvpmbyj04rz76ni")))

(define-public crate-honggfuzz-0.5.35 (c (n "honggfuzz") (v "0.5.35") (d (list (d (n "arbitrary") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0ifhhhyr5cyamjd1qf2w3266kx0mdyvv876bzrbhnmijkcrgp2c9")))

(define-public crate-honggfuzz-0.5.36 (c (n "honggfuzz") (v "0.5.36") (d (list (d (n "arbitrary") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1y5rngi9k0889cay8xy2x308qxipmcsj5vbzniksjfsk9l8w2357")))

(define-public crate-honggfuzz-0.5.37 (c (n "honggfuzz") (v "0.5.37") (d (list (d (n "arbitrary") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1ijaskxiw8sysiwp9nsq0bg0pig5fv4rww4k8gw77ccg106frjz4")))

(define-public crate-honggfuzz-0.5.38 (c (n "honggfuzz") (v "0.5.38") (d (list (d (n "arbitrary") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1m60vk8y9hj0w4s0ynkkx2n46hcn9h64izs0wp29w87wx8n54jx0")))

(define-public crate-honggfuzz-0.5.39 (c (n "honggfuzz") (v "0.5.39") (d (list (d (n "arbitrary") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "17g21dwhc5766cni0iai8qcrq07s62qxc80mgvkn2qws4ysfy702")))

(define-public crate-honggfuzz-0.5.40 (c (n "honggfuzz") (v "0.5.40") (d (list (d (n "arbitrary") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "19nkl8ij4vivkb36v2mgn9hyv7hj2yzjq16jf3a4jcslxa0wis3f")))

(define-public crate-honggfuzz-0.5.41 (c (n "honggfuzz") (v "0.5.41") (d (list (d (n "arbitrary") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0yvi3bypcznqy5q6ffynj6yxnn6y1frh9zvhsv24j0zgx946h7iw")))

(define-public crate-honggfuzz-0.5.42 (c (n "honggfuzz") (v "0.5.42") (d (list (d (n "arbitrary") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0lbjwbsb695r35igqlvaljfydzpvd5x0qxf7m4jcs01p7zcf4pib")))

(define-public crate-honggfuzz-0.5.43 (c (n "honggfuzz") (v "0.5.43") (d (list (d (n "arbitrary") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "01jj6x44b2dhvby7hrrhfz7swn848ifykgy75n69l356w6rk9415")))

(define-public crate-honggfuzz-0.5.44 (c (n "honggfuzz") (v "0.5.44") (d (list (d (n "arbitrary") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "16a2r36g6hivzdg0jgcyfjx4q4d615pr0ay5bx12q3gypjj2gizk")))

(define-public crate-honggfuzz-0.5.45 (c (n "honggfuzz") (v "0.5.45") (d (list (d (n "arbitrary") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0d7v2065njlcdwj9ni4aiz0sxa9ws34m4gg3v086v784ld57phi4")))

(define-public crate-honggfuzz-0.5.46 (c (n "honggfuzz") (v "0.5.46") (d (list (d (n "arbitrary") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.2") (d #t) (k 2)))) (h "1lr8wlwwcwd7nzk34hmvqx201qhdgbqlfcl7yz0ps5mvqpn2qy93")))

(define-public crate-honggfuzz-0.5.47 (c (n "honggfuzz") (v "0.5.47") (d (list (d (n "arbitrary") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.2") (d #t) (k 2)))) (h "0l0i660yb9npvwy69b4sbc8207avm2614wjs3kgkaxzgfcr2rpn3")))

(define-public crate-honggfuzz-0.5.48 (c (n "honggfuzz") (v "0.5.48") (d (list (d (n "arbitrary") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.2") (d #t) (k 2)))) (h "0wxh6r7wj1hq82gppnss8rpaynld6gl6ixcbsbvldyr7q7py4jia")))

(define-public crate-honggfuzz-0.5.49 (c (n "honggfuzz") (v "0.5.49") (d (list (d (n "arbitrary") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.2") (d #t) (k 2)))) (h "0zq9ksmcswlk9c6jsxjybl6yk3r3ddhsinl7331ddirfm0caqaw3")))

(define-public crate-honggfuzz-0.5.50 (c (n "honggfuzz") (v "0.5.50") (d (list (d (n "arbitrary") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.2") (d #t) (k 2)))) (h "1qgaajxjynm6aaynnp57r2vxqli4a23sxlmvczvw2f3nv6gnnk4s") (y #t)))

(define-public crate-honggfuzz-0.5.51 (c (n "honggfuzz") (v "0.5.51") (d (list (d (n "arbitrary") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.2") (d #t) (k 2)))) (h "1g4l0p1j6s8hxw8f66y9mhhgwk89fd3n4kq1kyapx3c2lljmf23g")))

(define-public crate-honggfuzz-0.5.52 (c (n "honggfuzz") (v "0.5.52") (d (list (d (n "arbitrary") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "18l3j2b8k65b36g2wiwpm4fy2qq318kbmk6n0rl3j75dpjbqin7a")))

(define-public crate-honggfuzz-0.5.53 (c (n "honggfuzz") (v "0.5.53") (d (list (d (n "arbitrary") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "07pw8azdwg1x932kk5b2bmn00jiny3847wwc33a5h6ml2472y302")))

(define-public crate-honggfuzz-0.5.54 (c (n "honggfuzz") (v "0.5.54") (d (list (d (n "arbitrary") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)))) (h "1w81ag0bjlybs2rq4xznaankbyy4fki93j5pb5gqmaa8v5vrb85y")))

(define-public crate-honggfuzz-0.5.55 (c (n "honggfuzz") (v "0.5.55") (d (list (d (n "arbitrary") (r "^1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)))) (h "13pi8aqnk382yswlh53lf3wfi8vmwkkfhqsslfhdmq4j218rr3l4") (f (quote (("default" "arbitrary"))))))

(define-public crate-honggfuzz-0.5.56 (c (n "honggfuzz") (v "0.5.56") (d (list (d (n "arbitrary") (r "^1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (t "cfg(fuzzing)") (k 0)) (d (n "memmap2") (r "^0.9") (d #t) (t "cfg(fuzzing_debug)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4") (d #t) (k 0)))) (h "019m08ylzd34p431a8ysw14125c6679pj4vdjirymj8k9hivcxkw") (f (quote (("default" "arbitrary"))))))

