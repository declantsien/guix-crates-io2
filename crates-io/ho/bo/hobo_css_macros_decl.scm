(define-module (crates-io ho bo hobo_css_macros_decl) #:use-module (crates-io))

(define-public crate-hobo_css_macros_decl-0.1.0 (c (n "hobo_css_macros_decl") (v "0.1.0") (d (list (d (n "css_macros") (r "^0.1") (d #t) (k 0) (p "hobo_css_macros")) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1d2kdfybnmkb84dgik79wc6ls3n0rissn8xji6cgkw411vbv530c")))

(define-public crate-hobo_css_macros_decl-0.1.1 (c (n "hobo_css_macros_decl") (v "0.1.1") (d (list (d (n "css_macros") (r "^0.1") (d #t) (k 0) (p "hobo_css_macros")) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "14v3z7ck1x3wkhdxfq2i3pyajl86bzrnmrg9p4qd6lkx6fv3madp")))

(define-public crate-hobo_css_macros_decl-0.1.2 (c (n "hobo_css_macros_decl") (v "0.1.2") (d (list (d (n "css_macros") (r "=0.1.2") (d #t) (k 0) (p "hobo_css_macros")) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "16kla2rdsq35323nrll28zwwajhqsm5p12p6d8c4g0bshb65v5k3")))

(define-public crate-hobo_css_macros_decl-0.1.3 (c (n "hobo_css_macros_decl") (v "0.1.3") (d (list (d (n "hobo_css_macros") (r "=0.1.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1fab4pcr7xcxbk4lpqvwwvsfd98pfks72g7acscljxb3b52a3gaa")))

(define-public crate-hobo_css_macros_decl-0.1.4 (c (n "hobo_css_macros_decl") (v "0.1.4") (d (list (d (n "hobo_css_macros") (r "=0.1.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "180h5qd2kfsk32qs78xbjy6s63cpb446p5xjz5wix0imbq41d3d6")))

(define-public crate-hobo_css_macros_decl-0.1.5 (c (n "hobo_css_macros_decl") (v "0.1.5") (d (list (d (n "hobo_css_macros") (r "=0.1.5") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0mrl06y3067vg56n8dy16g1v95hia0z99l93vnzdi77avhfkjql3")))

(define-public crate-hobo_css_macros_decl-0.1.6 (c (n "hobo_css_macros_decl") (v "0.1.6") (d (list (d (n "hobo_css_macros") (r "=0.1.6") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1wdssjl0znjhs21i2zwy7gidas337hms14fmvbw52k190vwj8k5j")))

(define-public crate-hobo_css_macros_decl-0.1.7 (c (n "hobo_css_macros_decl") (v "0.1.7") (d (list (d (n "hobo_css_macros") (r "=0.1.7") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0phmnglzi1zipgbmdpf66l899vkwzwi5pg6nlz79w2s2xx1m4vrs")))

(define-public crate-hobo_css_macros_decl-0.1.8 (c (n "hobo_css_macros_decl") (v "0.1.8") (d (list (d (n "hobo_css_macros") (r "=0.1.8") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0j5wy9qgbaydf7zzzl3ddjr0ikvd3zcby41fzfc4w5qx4b5y1wvj")))

(define-public crate-hobo_css_macros_decl-0.1.9 (c (n "hobo_css_macros_decl") (v "0.1.9") (d (list (d (n "hobo_css_macros") (r "=0.1.9") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1a78p9h9z7a3f0wl4q0qsv0cybg6ddj8q6vgbydbb8236s63flxf")))

