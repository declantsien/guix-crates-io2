(define-module (crates-io ho bo hobofan_stdweb_logger) #:use-module (crates-io))

(define-public crate-hobofan_stdweb_logger-0.1.0 (c (n "hobofan_stdweb_logger") (v "0.1.0") (d (list (d (n "log") (r "^0.4.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "stdweb") (r "^0.3.0") (d #t) (k 0)))) (h "01arjbfd8csyawmi5n6yl8xwr1ifb6mk3g7bdcc7n0766l8b2kzx")))

(define-public crate-hobofan_stdweb_logger-0.1.1 (c (n "hobofan_stdweb_logger") (v "0.1.1") (d (list (d (n "log") (r "^0.4.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "stdweb") (r "^0.3.0") (d #t) (k 0)))) (h "0q5qdqij65mj5g3525nl173kr66h72lnc9bvw2gw51iihcsi15zj")))

