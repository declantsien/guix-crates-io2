(define-module (crates-io ho s_ hos_kernel) #:use-module (crates-io))

(define-public crate-hos_kernel-0.1.0 (c (n "hos_kernel") (v "0.1.0") (d (list (d (n "bootloader_api") (r "^0.11.2") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.8.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.9.0") (d #t) (k 0)) (d (n "pc-keyboard") (r "^0.5.0") (d #t) (k 0)) (d (n "pic8259") (r "^0.10.1") (d #t) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "volatile") (r "^0.2.6") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.2") (d #t) (k 0)))) (h "0gyy3f9imv149ar4rh50jkhf5cpbva99n69r1yklz5vgv5kramaf")))

