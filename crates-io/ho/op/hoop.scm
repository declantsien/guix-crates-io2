(define-module (crates-io ho op hoop) #:use-module (crates-io))

(define-public crate-hoop-0.2.7 (c (n "hoop") (v "0.2.7") (h "01jj6vbckpbk9qkq1hsb85f9n6ikr0l8bp6b3qx6f012ly9i6lha")))

(define-public crate-hoop-0.2.8 (c (n "hoop") (v "0.2.8") (h "16wwg1qzgs7xk8r2wq5h2lm6101mamq5f0cji9nsh0wkbn23vyj1")))

