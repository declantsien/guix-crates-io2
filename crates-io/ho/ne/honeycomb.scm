(define-module (crates-io ho ne honeycomb) #:use-module (crates-io))

(define-public crate-honeycomb-0.1.0 (c (n "honeycomb") (v "0.1.0") (h "1236xw9brh1mhkfl5py5lpvma7d811jn0hmdxgazx240j3rmjd20")))

(define-public crate-honeycomb-0.1.1 (c (n "honeycomb") (v "0.1.1") (h "1928cr2vqncki53mc6kixxagd38blham40yfmg3k3b8nhvdcw9lx")))

(define-public crate-honeycomb-0.1.2 (c (n "honeycomb") (v "0.1.2") (h "06rgz4vzgpwvzjwhjr9xa8f6hwgcld40cgmbngh6yxwbmbhppk80")))

(define-public crate-honeycomb-0.1.3 (c (n "honeycomb") (v "0.1.3") (h "17y16c7q0rfd8l9b20ylfkgdhg86mfmb9s26164mkjdqpi75jdr0")))

(define-public crate-honeycomb-0.1.4 (c (n "honeycomb") (v "0.1.4") (h "1idzpyhm8z3pav0bb7fw2nnn5pjdy2bayz8dda5hk8l4j667falf")))

