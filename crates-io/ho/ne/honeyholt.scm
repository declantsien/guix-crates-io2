(define-module (crates-io ho ne honeyholt) #:use-module (crates-io))

(define-public crate-honeyholt-0.1.0 (c (n "honeyholt") (v "0.1.0") (d (list (d (n "function_name") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("max_level_trace" "release_max_level_warn"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "volmark") (r "^0.1.0") (d #t) (k 0)))) (h "00r8wdbnr7hg7gb0m54qn9dq0h6jnyic766mxbyhsm2i9b2mzhvx")))

