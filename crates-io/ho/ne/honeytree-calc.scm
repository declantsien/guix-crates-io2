(define-module (crates-io ho ne honeytree-calc) #:use-module (crates-io))

(define-public crate-honeytree-calc-1.0.0 (c (n "honeytree-calc") (v "1.0.0") (h "0syj3vz90b8pa6xkz3bw38kyzcjdbh588hvplzw122b257pgqb74")))

(define-public crate-honeytree-calc-1.0.1 (c (n "honeytree-calc") (v "1.0.1") (h "0y7lkw1wb77z4cd6lrhs3mfx5m8cszx7gnxwfhv5wf5qx8wnwqyw")))

