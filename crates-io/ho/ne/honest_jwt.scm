(define-module (crates-io ho ne honest_jwt) #:use-module (crates-io))

(define-public crate-honest_jwt-0.1.0 (c (n "honest_jwt") (v "0.1.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1pz1w1a9fslbscy8lcgfd933pvc2fcks2fdkr5aglqpmcb6ky8q5")))

