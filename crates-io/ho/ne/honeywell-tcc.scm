(define-module (crates-io ho ne honeywell-tcc) #:use-module (crates-io))

(define-public crate-honeywell-tcc-0.1.0 (c (n "honeywell-tcc") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "rustls-tls" "cookies"))) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1gyflw9clab7xn0gh97cgpkbrf9dmclbnrxiz19nqv05gsnz5r9p")))

