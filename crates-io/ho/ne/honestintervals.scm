(define-module (crates-io ho ne honestintervals) #:use-module (crates-io))

(define-public crate-honestintervals-0.1.0 (c (n "honestintervals") (v "0.1.0") (d (list (d (n "libc") (r "0.*") (d #t) (k 0)))) (h "12qvbr0ldfncqnry80fxx121ph94nksc9lhlgmqa6yvaydpfas8v")))

(define-public crate-honestintervals-0.1.1 (c (n "honestintervals") (v "0.1.1") (d (list (d (n "libc") (r "0.*") (d #t) (k 0)))) (h "1mi1kfb9a3bp1994vs6dd56h2kqw081xmm105d0fmb680j7q1r40")))

(define-public crate-honestintervals-0.2.0 (c (n "honestintervals") (v "0.2.0") (d (list (d (n "libc") (r "0.*") (d #t) (k 0)))) (h "02jfzb74kfpbp4bm1kyh6xzr0w1819bfvgx9hkvz62phvzykl8j4")))

