(define-module (crates-io ho ne hone) #:use-module (crates-io))

(define-public crate-hone-0.0.1 (c (n "hone") (v "0.0.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0g6mn4bkai2xhx0r9ri9hpyxcanwcsidkrqs3b6k5fp87j32rlsc")))

