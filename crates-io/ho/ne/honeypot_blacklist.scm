(define-module (crates-io ho ne honeypot_blacklist) #:use-module (crates-io))

(define-public crate-honeypot_blacklist-0.1.0 (c (n "honeypot_blacklist") (v "0.1.0") (d (list (d (n "resolve") (r "^0.1.2") (d #t) (k 0)))) (h "0nd9mcgvcz26k8ld7b9jwn5h2c2cl2iinjcqrmjkl06g0d4zidi9")))

(define-public crate-honeypot_blacklist-0.1.2 (c (n "honeypot_blacklist") (v "0.1.2") (d (list (d (n "domain") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.11") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.6") (d #t) (k 0)))) (h "08f32f0rlyskjqkc63if85dqzy4yci37rb7wzl2706c45zi6vhj6")))

