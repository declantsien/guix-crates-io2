(define-module (crates-io ho oo hooo) #:use-module (crates-io))

(define-public crate-hooo-0.1.0 (c (n "hooo") (v "0.1.0") (d (list (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)))) (h "0dzz6vfhzf4arf9ylfpr6pfszc95ja4m55wai5r2jj92xj7ilfwy")))

(define-public crate-hooo-0.2.0 (c (n "hooo") (v "0.2.0") (d (list (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)))) (h "1jrkwkkpsr5lfcz27dplfpdmg6mljr19sn2h13gq0wi81fzhrdfv")))

(define-public crate-hooo-0.2.1 (c (n "hooo") (v "0.2.1") (d (list (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)))) (h "1c3ayh23qww0s3bbjfx5a54cg4i2g9vf97v9yiczk4ash37gfnib")))

(define-public crate-hooo-0.2.2 (c (n "hooo") (v "0.2.2") (d (list (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)))) (h "1jqg7bbn1f1b7sz6j1qz2nqi3s7wsaqk6xhvd20s70sc07jz1qvk")))

(define-public crate-hooo-0.2.3 (c (n "hooo") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1gckdzjl5qyxzgck5qsm434mphczdgd1i314n75xap38xxhj58zx")))

(define-public crate-hooo-0.2.4 (c (n "hooo") (v "0.2.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "0b4k4kpb2lgcxkqc3g7sl3fc4hdlcwwdbkycblbnhrr3kr5lj0pv")))

(define-public crate-hooo-0.2.5 (c (n "hooo") (v "0.2.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)))) (h "1jjd2g1kyhyy8625r8lpdhdaddzgap1xx9gj433njackwf41lmm9")))

(define-public crate-hooo-0.2.6 (c (n "hooo") (v "0.2.6") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l5l7j429bqf9931jsbq9019ambqx1cf8bjrgpxs6qbq283aqp8s")))

(define-public crate-hooo-0.3.0 (c (n "hooo") (v "0.3.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "132ijdzd4r7i0alkcvs5iwh7gzb95njm5ya0077d1glalw5xh00y")))

(define-public crate-hooo-0.3.1 (c (n "hooo") (v "0.3.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rdnpdydw63iwh2rs1aaha8l1cscp17ggizbfs3xdpkk5l3idf8n")))

(define-public crate-hooo-0.3.2 (c (n "hooo") (v "0.3.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "deflate") (r "^1.0.0") (d #t) (k 0)) (d (n "inflate") (r "^0.4.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0acf62z9s05mn886kgy52yzrcds5x66vf0v52ldk24dmsxnlx4dz")))

(define-public crate-hooo-0.3.3 (c (n "hooo") (v "0.3.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "deflate") (r "^1.0.0") (d #t) (k 0)) (d (n "inflate") (r "^0.4.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qxsb5vk5q3p8f9qjflsgph0m4gaqcycl940nxa2ravfjq41972v")))

(define-public crate-hooo-0.3.4 (c (n "hooo") (v "0.3.4") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "deflate") (r "^1.0.0") (d #t) (k 0)) (d (n "inflate") (r "^0.4.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16cxkzsz74r239dnsybfzw1ghdwxa0hpp1208aq4x3v6frl0f22j")))

(define-public crate-hooo-0.3.5 (c (n "hooo") (v "0.3.5") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "deflate") (r "^1.0.0") (d #t) (k 0)) (d (n "inflate") (r "^0.4.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l7201qin1csicxhs8s2y9b587wkq1ln695m7l83mvpbsmlm60q2")))

(define-public crate-hooo-0.3.6 (c (n "hooo") (v "0.3.6") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "deflate") (r "^1.0.0") (d #t) (k 0)) (d (n "inflate") (r "^0.4.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kc81hnw88jzkwyg6rdc1821zwzp58qpzlbj6ss4xf2wl8cjd72f")))

(define-public crate-hooo-0.3.7 (c (n "hooo") (v "0.3.7") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "deflate") (r "^1.0.0") (d #t) (k 0)) (d (n "inflate") (r "^0.4.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16qjhrgssran47zd6zf29gjavyc44bz442cjjghmqag933jsapp6")))

(define-public crate-hooo-0.3.8 (c (n "hooo") (v "0.3.8") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "deflate") (r "^1.0.0") (d #t) (k 0)) (d (n "inflate") (r "^0.4.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x4k6hzsdyhhqwf80v1gj1j2nkay5q9w4krsjn5qlapw4hknhycj")))

(define-public crate-hooo-0.4.0 (c (n "hooo") (v "0.4.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "deflate") (r "^1.0.0") (d #t) (k 0)) (d (n "inflate") (r "^0.4.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1afargf3wa3pjdlrc6dm29bx5ag8vkfxrihgp7w3z865yjkzmb70")))

(define-public crate-hooo-0.5.0 (c (n "hooo") (v "0.5.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "deflate") (r "^1.0.0") (d #t) (k 0)) (d (n "inflate") (r "^0.4.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f18nm167c66ff6xhcas6sd2y9b9329ivwvz551p1rbb658ly6i3")))

