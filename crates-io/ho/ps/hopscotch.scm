(define-module (crates-io ho ps hopscotch) #:use-module (crates-io))

(define-public crate-hopscotch-0.0.1 (c (n "hopscotch") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "im") (r "^14.3") (d #t) (k 0)) (d (n "nohash-hasher") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1dmzvvf1acl301vvvqabqfdys4jyql4i555y0fhmz362nf7c8zqh")))

(define-public crate-hopscotch-0.0.2 (c (n "hopscotch") (v "0.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "im") (r "^14.3") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0sql9kaz846yh9h0zgjhwkd5k6zsk5wzpdk8x1fwaikxnvkj6by9")))

(define-public crate-hopscotch-0.0.3 (c (n "hopscotch") (v "0.0.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rpds") (r "^0.7") (d #t) (k 0)))) (h "1mbkw773bz3b4d1plh77a94w79g9k9ymdrwx3bq0q1c75yax66r9")))

(define-public crate-hopscotch-0.1.0 (c (n "hopscotch") (v "0.1.0") (d (list (d (n "archery") (r "^0.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rpds") (r "^0.7") (d #t) (k 0)) (d (n "rust-embed") (r "^5.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 2)))) (h "0xkvvjrlsjbp6k08d3dy8mfi2ckbbazkb54wg81flfxxcg503n9i")))

(define-public crate-hopscotch-0.1.1 (c (n "hopscotch") (v "0.1.1") (d (list (d (n "archery") (r "^0.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rpds") (r "^0.7") (d #t) (k 0)) (d (n "rust-embed") (r "^5.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 2)))) (h "076f2jpv2yrn54sf6q8d95wc3qvihd00q5h89p75hc6dwm435gaq")))

