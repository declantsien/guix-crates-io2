(define-module (crates-io ho lo holodeque) #:use-module (crates-io))

(define-public crate-holodeque-0.1.0 (c (n "holodeque") (v "0.1.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "15081fsj2w1vvb0dm6xjw9w21pgdmf14jcx3nlm77msqm4ikv3mm") (f (quote (("std") ("default" "std"))))))

(define-public crate-holodeque-0.2.0 (c (n "holodeque") (v "0.2.0") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "11n2krw3f1zw7axs86am8yay5rd0x7qx95xjria054hjp0c46kcn") (f (quote (("std") ("default" "std"))))))

