(define-module (crates-io ho lo holochain_common) #:use-module (crates-io))

(define-public crate-holochain_common-0.0.33-alpha5 (c (n "holochain_common") (v "0.0.33-alpha5") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "1z1q6la91qsr8dvc3ph4fjl2547xv8zg4i257a9za54smfppz975")))

(define-public crate-holochain_common-0.0.33-alpha6 (c (n "holochain_common") (v "0.0.33-alpha6") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "1lnl9iiav4ai2rr0i6brl9zzkhkmpckj4vj10dfk3rrgh853cvd1")))

(define-public crate-holochain_common-0.0.34-alpha1 (c (n "holochain_common") (v "0.0.34-alpha1") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "0hxrbsb9dbmj9qii7mzp64brvkzlh3zawiwra3a3k3w64d6l1wv4")))

(define-public crate-holochain_common-0.0.35-alpha7 (c (n "holochain_common") (v "0.0.35-alpha7") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "0vdhq08l7mgsvklqnyg56a76akjzgwbblb2sjwvivz2l86jqfw42")))

(define-public crate-holochain_common-0.0.37-alpha1 (c (n "holochain_common") (v "0.0.37-alpha1") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "0fhd87zi5mf12vsi0vllgxndsyzp66dqrns3wwkd5p5zib82sbl6")))

(define-public crate-holochain_common-0.0.37-alpha3 (c (n "holochain_common") (v "0.0.37-alpha3") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "0zg5ni22qfwzpp4vri94zzrk6dkizy9f7pjrffzj7m39b9xpkq3h")))

(define-public crate-holochain_common-0.0.37-alpha6 (c (n "holochain_common") (v "0.0.37-alpha6") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "14qwlm3g3jqwjm0cxvxjcbma2hab7fgy1xy192j3xhg9as32pycp")))

(define-public crate-holochain_common-0.0.37-alpha7 (c (n "holochain_common") (v "0.0.37-alpha7") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "0a3y5a4rnkxghz3hc9448hkv409arw17llr3z7gswsb98p8acnrw")))

(define-public crate-holochain_common-0.0.37-alpha8 (c (n "holochain_common") (v "0.0.37-alpha8") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "19w1g3iy01ifjyw8qilvsp3dbkpy5njqzrm8dl43mpcmdrbi91y8")))

(define-public crate-holochain_common-0.0.37-alpha9 (c (n "holochain_common") (v "0.0.37-alpha9") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "0hcmwq8n461w3xywcxb47w55i6rk22akh044n07l09h5215hzj9w")))

(define-public crate-holochain_common-0.0.37-alpha10 (c (n "holochain_common") (v "0.0.37-alpha10") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "0ciiwycciv54mg4a81vzd81a2zvvjkk3xydph49nszwas3z38k24")))

(define-public crate-holochain_common-0.0.37-alpha11 (c (n "holochain_common") (v "0.0.37-alpha11") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "12k3nh6kpsfnnqzrlxi484xp27pvv114yjsfbq86pld9my1fm1lz")))

(define-public crate-holochain_common-0.0.37-alpha12 (c (n "holochain_common") (v "0.0.37-alpha12") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "1mfmsqkdjxnghbdfl4i5gslxq39byvi92xpifiva7xs7n4jr0c3n")))

(define-public crate-holochain_common-0.0.38-alpha2 (c (n "holochain_common") (v "0.0.38-alpha2") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "18cjhzpy989vfj01qvbd7nyz37ng7psxl31f7xkfvd3fahn3ninn")))

(define-public crate-holochain_common-0.0.38-alpha4 (c (n "holochain_common") (v "0.0.38-alpha4") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "1rw46s84js8fl13784v8msihnnh9ai0lbs49z790bk4fx2s1lyq9")))

(define-public crate-holochain_common-0.0.38-alpha5 (c (n "holochain_common") (v "0.0.38-alpha5") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "1rjnfi8qz0czs0d2awlrn61hdq6pir47izg7yw6f3dfbc54n0xvq")))

(define-public crate-holochain_common-0.0.38-alpha6 (c (n "holochain_common") (v "0.0.38-alpha6") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "1bqgz65qas262hz45q444sqw5xiqkhh3gpj2v55djisy03s2plvw")))

(define-public crate-holochain_common-0.0.38-alpha7 (c (n "holochain_common") (v "0.0.38-alpha7") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "0z0fqxmnmg5ar3hdig6hyyvlgy4bcbhzsgklz20vvp7y57myd39i")))

(define-public crate-holochain_common-0.0.38-alpha8 (c (n "holochain_common") (v "0.0.38-alpha8") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "027544pw2lq0y9x0mng53l68gpg5lf9jal5plx2p0l5v3ij3ng9m")))

(define-public crate-holochain_common-0.0.38-alpha9 (c (n "holochain_common") (v "0.0.38-alpha9") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "1nphlw8vg7zi3z4pcf68hzpccvcsv6y7y1smay6cxcijik9gpi3g")))

(define-public crate-holochain_common-0.0.38-alpha12 (c (n "holochain_common") (v "0.0.38-alpha12") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "18yck85vm5nbpvj6b243zikvd66crddh9j08rmx5hni1032c7q5f")))

(define-public crate-holochain_common-0.0.38-alpha13 (c (n "holochain_common") (v "0.0.38-alpha13") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "0jw39058flq4ryk34qbghyphwrw7z58zl26j3s36767xg30igafz")))

(define-public crate-holochain_common-0.0.38-alpha14 (c (n "holochain_common") (v "0.0.38-alpha14") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "0fh8yflv5jja26hvhiixdixaaw9c5bwyvl2zwpmh1casylxrkbs2")))

(define-public crate-holochain_common-0.0.39-alpha1 (c (n "holochain_common") (v "0.0.39-alpha1") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "0w2s5h3m0j62i9z84clfz2vfkxldlna3pvrrjh5yfkaghb1lfizx")))

(define-public crate-holochain_common-0.0.39-alpha2 (c (n "holochain_common") (v "0.0.39-alpha2") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "0b0pnkr0bl96b1lfgx7pjacfmd9n4qfnh97q9dhvd69k8f33v2kv")))

(define-public crate-holochain_common-0.0.39-alpha3 (c (n "holochain_common") (v "0.0.39-alpha3") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "1wyrgrp7aziy0is2xbh1ifah4mqbd49g9f002nvafg96gdd3rcgs")))

(define-public crate-holochain_common-0.0.39-alpha4 (c (n "holochain_common") (v "0.0.39-alpha4") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "15c3vjlgr3a7ylc72kvpdyhbzjn8jfnzz0vjp5n9wjkvi3i3ljmi")))

(define-public crate-holochain_common-0.0.40-alpha1 (c (n "holochain_common") (v "0.0.40-alpha1") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "1r72grcgzcs8p3jhq7xi7ca58fzah9p0775nc78qcvraw1zbn4dd")))

(define-public crate-holochain_common-0.0.41-alpha2 (c (n "holochain_common") (v "0.0.41-alpha2") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "1izd72f3p6dvvg3bzpv8b9834rcvbl9hpa0xzdbxdmj8c8r9dspl")))

(define-public crate-holochain_common-0.0.41-alpha3 (c (n "holochain_common") (v "0.0.41-alpha3") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "1x3y0l1ls0fqivr9fnrd34hz3y3jw1p4f4xd23bxmz9knm71ikzv")))

(define-public crate-holochain_common-0.0.41-alpha4 (c (n "holochain_common") (v "0.0.41-alpha4") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "1y5ky36wzwn0qmfg1x0n56i8h2cl3b7qx37s0bkq5cwvk2ij9z5w")))

(define-public crate-holochain_common-0.0.42-alpha1 (c (n "holochain_common") (v "0.0.42-alpha1") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "0vny7isf3p66bc6mng4pa2ibi0yzq655h6nri79jvp4sg7a6x6i1")))

(define-public crate-holochain_common-0.0.42-alpha2 (c (n "holochain_common") (v "0.0.42-alpha2") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "0daf8lxfvamyi2a9kjm805xaqzxd45ypjkxr97fjq8ivlpizf9np")))

(define-public crate-holochain_common-0.0.42-alpha3 (c (n "holochain_common") (v "0.0.42-alpha3") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "0112gywr8829lv4zqacqpx61mzra10iiyy6hr9yn2gn0pzwbrjii")))

(define-public crate-holochain_common-0.0.42-alpha4 (c (n "holochain_common") (v "0.0.42-alpha4") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "0k54b4340i67k7hcwiirbjlpr7kawrwpmjyxya0km91l3n0x14a2")))

(define-public crate-holochain_common-0.0.42-alpha5 (c (n "holochain_common") (v "0.0.42-alpha5") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)))) (h "0nmmzi0c8lqyk6p8wpqdnjn5hd783yd000ym95dlc05f8azpsjy9")))

(define-public crate-holochain_common-0.0.43-alpha1 (c (n "holochain_common") (v "0.0.43-alpha1") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "= 1.4.0") (d #t) (k 0)))) (h "00s7nm2x9swimckpz6ai0lz5qz5czh3b01lpnhwzlkj5pb78lxrl")))

(define-public crate-holochain_common-0.0.43-alpha2 (c (n "holochain_common") (v "0.0.43-alpha2") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "= 1.4.0") (d #t) (k 0)))) (h "0737zypzc4zb6jn6w1yk7ya8wjs6cbg7yyzn9h0956yiiql4hh1d")))

(define-public crate-holochain_common-0.0.43-alpha3 (c (n "holochain_common") (v "0.0.43-alpha3") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "= 1.4.0") (d #t) (k 0)))) (h "0b8bjhanxpssjs1p05nrh48m8vlqdmfb6qkzzaq50rfb70ikgsmi")))

(define-public crate-holochain_common-0.0.44-alpha1 (c (n "holochain_common") (v "0.0.44-alpha1") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "= 1.4.0") (d #t) (k 0)))) (h "1d690jnz92s84xqshsfbmfdj2z4qw8yk5hs70wk21p5vhg6lkx1v")))

(define-public crate-holochain_common-0.0.44-alpha2 (c (n "holochain_common") (v "0.0.44-alpha2") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "= 1.4.0") (d #t) (k 0)))) (h "07zsvizqk4xgnmxsb1gf7cjgrjasd5pp38j0wiyma2fl24x9dnq4")))

(define-public crate-holochain_common-0.0.44-alpha3 (c (n "holochain_common") (v "0.0.44-alpha3") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "= 1.4.0") (d #t) (k 0)))) (h "1kvzlmp6h79b99a89zpjigs515z9vh3bmpank0ycvmwx4sqvysfb")))

(define-public crate-holochain_common-0.0.45-alpha1 (c (n "holochain_common") (v "0.0.45-alpha1") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "= 1.4.0") (d #t) (k 0)))) (h "0687w8qzkyz1j3501jrwcz0y06z0w14s181q9daq9apgnng199l1")))

(define-public crate-holochain_common-0.0.46-alpha1 (c (n "holochain_common") (v "0.0.46-alpha1") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "= 1.4.0") (d #t) (k 0)))) (h "13s7k15zbisx23ykg5i3ldmmlg7ajcr0hv5mn1p66bd35wpj9afz")))

(define-public crate-holochain_common-0.0.47-alpha1 (c (n "holochain_common") (v "0.0.47-alpha1") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "= 1.4.0") (d #t) (k 0)))) (h "022pjk3rmixkw696vp87sjmf851rmk8bd4c7bf9iimmxck5872z0")))

(define-public crate-holochain_common-0.0.48-alpha1 (c (n "holochain_common") (v "0.0.48-alpha1") (d (list (d (n "directories") (r "= 1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "= 1.4.0") (d #t) (k 0)))) (h "007bxcxq2m3kjw5rqknivbrdg3k3dv8zrdjcx0b9xdn3gab6gzbc")))

(define-public crate-holochain_common-0.0.49-alpha1 (c (n "holochain_common") (v "0.0.49-alpha1") (d (list (d (n "directories") (r "=1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "=1.4.0") (d #t) (k 0)))) (h "1d993xdpfif6paa5j0bi49sn643vkg2g1zwc0kmqclwcdgylh67a")))

(define-public crate-holochain_common-0.0.50-alpha1 (c (n "holochain_common") (v "0.0.50-alpha1") (d (list (d (n "directories") (r "=1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "=1.4.0") (d #t) (k 0)))) (h "043c2snhkxgk7baimng55kbhw3jwc3p2pxcddwsla1082l7z52fb")))

(define-public crate-holochain_common-0.0.50-alpha2 (c (n "holochain_common") (v "0.0.50-alpha2") (d (list (d (n "directories") (r "=1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "=1.4.0") (d #t) (k 0)))) (h "10n3n9l2i3lv3yhv2s4rxz4wjd7pagqzszfax8207x7ni6mb17am")))

(define-public crate-holochain_common-0.0.50-alpha3 (c (n "holochain_common") (v "0.0.50-alpha3") (d (list (d (n "directories") (r "=1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "=1.4.0") (d #t) (k 0)))) (h "0vas5nh0xwkzzshs2j7wfd33m2gkz4s52gnxph4vmlbx0rabwixi")))

(define-public crate-holochain_common-0.0.50-alpha4 (c (n "holochain_common") (v "0.0.50-alpha4") (d (list (d (n "directories") (r "=1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "=1.4.0") (d #t) (k 0)))) (h "1x9grxv9rgsnw3cdjd0cgf1nwxyralfs3wvrkbp47yprf6p7sw7z")))

(define-public crate-holochain_common-0.0.51-alpha1 (c (n "holochain_common") (v "0.0.51-alpha1") (d (list (d (n "directories") (r "=1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "=1.4.0") (d #t) (k 0)))) (h "1chnbhrlcjz5pj5bzw7l6fd3kzsrwax7vmj67wfqy75mn1ypj6x5")))

(define-public crate-holochain_common-0.0.52-alpha1 (c (n "holochain_common") (v "0.0.52-alpha1") (d (list (d (n "directories") (r "=1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "=1.4.0") (d #t) (k 0)))) (h "00asvp5jhngvwxb0snqhp1fff3cq63jjcfixgg6x9j2pi8prxg6c")))

(define-public crate-holochain_common-0.0.52-alpha2 (c (n "holochain_common") (v "0.0.52-alpha2") (d (list (d (n "directories") (r "=1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "=1.4.0") (d #t) (k 0)))) (h "1cv782sd1mhn936wyjpjdfkiw8spmp7ycbwjsj1l0iv3y5w1szg4")))

