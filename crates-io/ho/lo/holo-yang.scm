(define-module (crates-io ho lo holo-yang) #:use-module (crates-io))

(define-public crate-holo-yang-0.4.0 (c (n "holo-yang") (v "0.4.0") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "yang2") (r "^0.9") (f (quote ("bundled"))) (d #t) (k 0)))) (h "18nvv1h0cx0g75yx51sspk2qknpr39w3svzlyxldb4hi3jz12vbh")))

(define-public crate-holo-yang-0.4.1 (c (n "holo-yang") (v "0.4.1") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "yang2") (r "^0.9") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1hc768kgiy86kcp9x4i812filjcq4qcfgmga8ag61v30671kvic2")))

(define-public crate-holo-yang-0.4.2 (c (n "holo-yang") (v "0.4.2") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "yang2") (r "^0.13") (f (quote ("bundled"))) (d #t) (k 0)))) (h "101lmrxh4l8xdaj74q4znk0h5wraa7imcrhsx426gc50d2igrbf5")))

