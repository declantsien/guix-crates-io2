(define-module (crates-io ho lo holochain_serialized_bytes_derive) #:use-module (crates-io))

(define-public crate-holochain_serialized_bytes_derive-0.0.34 (c (n "holochain_serialized_bytes_derive") (v "0.0.34") (d (list (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (d #t) (k 0)))) (h "0scnzrima568adxymxpixkrcfxbdjkr71k7axrbk0df1gx4h2z8y")))

(define-public crate-holochain_serialized_bytes_derive-0.0.35 (c (n "holochain_serialized_bytes_derive") (v "0.0.35") (d (list (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (d #t) (k 0)))) (h "0qx3g0qrp7yyb1rj6lwamz2msl7iramzyravpp1gr7q35n06n40y")))

(define-public crate-holochain_serialized_bytes_derive-0.0.36 (c (n "holochain_serialized_bytes_derive") (v "0.0.36") (d (list (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (d #t) (k 0)))) (h "083bn9nkjv7k7xqn0dwphpydc4yh9nnvp5xhgr58n8p8x1f0p81c")))

(define-public crate-holochain_serialized_bytes_derive-0.0.37 (c (n "holochain_serialized_bytes_derive") (v "0.0.37") (d (list (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (d #t) (k 0)))) (h "1r3xrqjfcnvr3ijasnrgf3zmdwrn5w96c2jw8p4hb36m45ahg6di")))

(define-public crate-holochain_serialized_bytes_derive-0.0.38 (c (n "holochain_serialized_bytes_derive") (v "0.0.38") (d (list (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.31") (d #t) (k 0)))) (h "1d8f8iaj47gcp12x3ja8pjca90fnp40pjh2lmdlrn6brxmccp7ag")))

(define-public crate-holochain_serialized_bytes_derive-0.0.39 (c (n "holochain_serialized_bytes_derive") (v "0.0.39") (d (list (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.31") (d #t) (k 0)))) (h "0rxf4axxcmcn2b17c5gxhl8zfz3fvs2xasji8xz9mw0wwkaflxm1")))

(define-public crate-holochain_serialized_bytes_derive-0.0.40 (c (n "holochain_serialized_bytes_derive") (v "0.0.40") (d (list (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.31") (d #t) (k 0)))) (h "18brr17h0pkjg2ic3wqp20r9c0f47n9varc4cp7h2g8xm53i4clh")))

(define-public crate-holochain_serialized_bytes_derive-0.0.41 (c (n "holochain_serialized_bytes_derive") (v "0.0.41") (d (list (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.31") (d #t) (k 0)))) (h "0ffgpcgpk26041zn6lzc2lvfy328cvrqghaaxkyvaln9vmi5rawn")))

(define-public crate-holochain_serialized_bytes_derive-0.0.42 (c (n "holochain_serialized_bytes_derive") (v "0.0.42") (d (list (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.31") (d #t) (k 0)))) (h "0zb9g1ljn444grzhpsqv534liz4010fbpl1m96wbypg9dndh0gs8")))

(define-public crate-holochain_serialized_bytes_derive-0.0.43 (c (n "holochain_serialized_bytes_derive") (v "0.0.43") (d (list (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.31") (d #t) (k 0)))) (h "1mk0kzvrhr1dvy95mwrnplshflvcq19byfk0frz9yaisfi60a8zc")))

(define-public crate-holochain_serialized_bytes_derive-0.0.44 (c (n "holochain_serialized_bytes_derive") (v "0.0.44") (d (list (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.31") (d #t) (k 0)))) (h "050vn0fk6nbicsi33r03z8rc3ss9vaicv2320869sfn1k3prd7pf")))

(define-public crate-holochain_serialized_bytes_derive-0.0.45 (c (n "holochain_serialized_bytes_derive") (v "0.0.45") (d (list (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.31") (d #t) (k 0)))) (h "0272640x9i7dxvc69ad10fsgyhhx1gad8bskwl0cp87har6zifrd")))

(define-public crate-holochain_serialized_bytes_derive-0.0.46 (c (n "holochain_serialized_bytes_derive") (v "0.0.46") (d (list (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.31") (d #t) (k 0)))) (h "0d3pbcji4464xc6bpwjn1asx71b2xg0yjalir1isqxr1i3pw1951")))

(define-public crate-holochain_serialized_bytes_derive-0.0.47 (c (n "holochain_serialized_bytes_derive") (v "0.0.47") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1s990xwbmvhd5dxhd4lkw5d2kfmwf3sjy8cs2b3lnkm15b6vf681")))

(define-public crate-holochain_serialized_bytes_derive-0.0.48 (c (n "holochain_serialized_bytes_derive") (v "0.0.48") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07pc6jf194ap99rarpjp6wppbw5n961qssvjxir4gav9gprk5gad")))

(define-public crate-holochain_serialized_bytes_derive-0.0.49 (c (n "holochain_serialized_bytes_derive") (v "0.0.49") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cggba8sayvhkb5y08x8k3gkk8c9y0gf8f2iba1zmsgssj6xfba9")))

(define-public crate-holochain_serialized_bytes_derive-0.0.50 (c (n "holochain_serialized_bytes_derive") (v "0.0.50") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01r2spj6r470ial0riivyfx9khvjcfcw4dryzvpvx4amv5rf4pq8")))

(define-public crate-holochain_serialized_bytes_derive-0.0.51 (c (n "holochain_serialized_bytes_derive") (v "0.0.51") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0b5k7hrrbsrvq7b841l1n6ppwi0fh0iglf71p7z68za21hnj6xqh")))

(define-public crate-holochain_serialized_bytes_derive-0.0.52 (c (n "holochain_serialized_bytes_derive") (v "0.0.52") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0af1669cp28yjfb39gwkxxm2giw8brdqn2idfy3wnzriqb8c1khp")))

(define-public crate-holochain_serialized_bytes_derive-0.0.53 (c (n "holochain_serialized_bytes_derive") (v "0.0.53") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wcxaf4cakx890n7lbcjskv6vci547h40va72ksz1jq543q0qgpc")))

(define-public crate-holochain_serialized_bytes_derive-0.0.54 (c (n "holochain_serialized_bytes_derive") (v "0.0.54") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0r4lp1407q0vvf9py1gabb0gy3i236z3r8n4md2dccvj04cpzk3i")))

