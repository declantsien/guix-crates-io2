(define-module (crates-io ho lo holochain_nonce) #:use-module (crates-io))

(define-public crate-holochain_nonce-0.3.0-beta-dev.20 (c (n "holochain_nonce") (v "0.3.0-beta-dev.20") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("std"))) (k 0)) (d (n "holochain_secure_primitive") (r "^0.3.0-beta-dev.20") (k 0)) (d (n "kitsune_p2p_timestamp") (r "^0.3.0-beta-dev.4") (d #t) (k 0)))) (h "0wyy1zp03drwlg8ma2b7mji8gxz2xmp6wvcyhyfmf6m7xp5mkn0p")))

(define-public crate-holochain_nonce-0.3.0-beta-dev.21 (c (n "holochain_nonce") (v "0.3.0-beta-dev.21") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("std"))) (k 0)) (d (n "holochain_secure_primitive") (r "^0.3.0-beta-dev.20") (k 0)) (d (n "kitsune_p2p_timestamp") (r "^0.3.0-beta-dev.5") (d #t) (k 0)))) (h "0hdszg8nkl252xjmrgxkznikfkhk1wgvp06wfqixpwxfkp0122ab")))

(define-public crate-holochain_nonce-0.3.0-beta-dev.22 (c (n "holochain_nonce") (v "0.3.0-beta-dev.22") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("std"))) (k 0)) (d (n "holochain_secure_primitive") (r "^0.3.0-beta-dev.21") (k 0)) (d (n "kitsune_p2p_timestamp") (r "^0.3.0-beta-dev.5") (d #t) (k 0)))) (h "0v0ya437wa1z0jbj2igiy0inyjs8izy8pycdh3h3n0jgjhs2dadc")))

(define-public crate-holochain_nonce-0.3.0-beta-dev.23 (c (n "holochain_nonce") (v "0.3.0-beta-dev.23") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("std"))) (k 0)) (d (n "holochain_secure_primitive") (r "^0.3.0-beta-dev.21") (k 0)) (d (n "kitsune_p2p_timestamp") (r "^0.3.0-beta-dev.6") (d #t) (k 0)))) (h "1q6ccqv877z4z371klxccmzh9fk98sgrnv8mdgkp3yazrx20dsyn")))

(define-public crate-holochain_nonce-0.3.0-beta-dev.24 (c (n "holochain_nonce") (v "0.3.0-beta-dev.24") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("std"))) (k 0)) (d (n "holochain_secure_primitive") (r "^0.3.0-beta-dev.21") (k 0)) (d (n "kitsune_p2p_timestamp") (r "^0.3.0-beta-dev.7") (d #t) (k 0)))) (h "1aa54fgfxknk4ccbqxi3wnh18fvcn015z9yy59axla6smgi1gjsl")))

(define-public crate-holochain_nonce-0.3.0-beta-dev.25 (c (n "holochain_nonce") (v "0.3.0-beta-dev.25") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("std"))) (k 0)) (d (n "holochain_secure_primitive") (r "^0.3.0-beta-dev.22") (k 0)) (d (n "kitsune_p2p_timestamp") (r "^0.3.0-beta-dev.8") (d #t) (k 0)))) (h "04a72cj5p7c0m956q53259gl055d4chlwa6h7m3gxf4bsg2rkyn1")))

(define-public crate-holochain_nonce-0.3.0-beta-dev.26 (c (n "holochain_nonce") (v "0.3.0-beta-dev.26") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("std"))) (k 0)) (d (n "holochain_secure_primitive") (r "^0.3.0-beta-dev.23") (k 0)) (d (n "kitsune_p2p_timestamp") (r "^0.3.0-beta-dev.9") (d #t) (k 0)))) (h "0ly7wa19hr4ad3rfnbxigrb8i8kbv0p4idpzaf0hfhswrgry5ppz")))

(define-public crate-holochain_nonce-0.3.0-beta-dev.27 (c (n "holochain_nonce") (v "0.3.0-beta-dev.27") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("std"))) (k 0)) (d (n "holochain_secure_primitive") (r "^0.3.0-beta-dev.23") (k 0)) (d (n "kitsune_p2p_timestamp") (r "^0.3.0-beta-dev.10") (d #t) (k 0)))) (h "03vvc3zarcq84vifhmbyabkpmww851a3qsh07fnfbdlimd04qvya")))

(define-public crate-holochain_nonce-0.4.0-dev.0 (c (n "holochain_nonce") (v "0.4.0-dev.0") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("std"))) (k 0)) (d (n "holochain_secure_primitive") (r "^0.3.0-dev.0") (k 0)) (d (n "kitsune_p2p_timestamp") (r "^0.4.0-dev.0") (d #t) (k 0)))) (h "0s9sbc2qzkbcb8bsmmgxrviwq0qpv4xgw8mzyzhpdfwa8kgill40")))

(define-public crate-holochain_nonce-0.4.0-dev.1 (c (n "holochain_nonce") (v "0.4.0-dev.1") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("std"))) (k 0)) (d (n "holochain_secure_primitive") (r "^0.4.0-dev.1") (k 0)) (d (n "kitsune_p2p_timestamp") (r "^0.4.0-dev.0") (d #t) (k 0)))) (h "1j3h9gj1l1c2z98a5ra8acws84qfly6jyf14iqwf94fkcpc2bw5b")))

(define-public crate-holochain_nonce-0.4.0-dev.2 (c (n "holochain_nonce") (v "0.4.0-dev.2") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("std"))) (k 0)) (d (n "holochain_secure_primitive") (r "^0.4.0-dev.1") (k 0)) (d (n "kitsune_p2p_timestamp") (r "^0.4.0-dev.1") (d #t) (k 0)))) (h "192jmrjc06ahcky8jsw3pzih5lfqampjv1743vxn620hfrygsdmr")))

(define-public crate-holochain_nonce-0.3.1-rc.0 (c (n "holochain_nonce") (v "0.3.1-rc.0") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("std"))) (k 0)) (d (n "holochain_secure_primitive") (r "^0.3.1-rc.0") (k 0)) (d (n "kitsune_p2p_timestamp") (r "^0.3.1-rc.0") (d #t) (k 0)))) (h "0b4b3fib7nac2v2gz50yfv0ic08vfrx8wng1x5kwxsmwnk3g66wq")))

