(define-module (crates-io ho lo holochain_cli_signal_srv) #:use-module (crates-io))

(define-public crate-holochain_cli_signal_srv-0.2.0-beta-rc.2 (c (n "holochain_cli_signal_srv") (v "0.2.0-beta-rc.2") (d (list (d (n "holochain_trace") (r "^0.2.0-beta-rc.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tx5-signal-srv") (r "=0.0.1-alpha.7") (d #t) (k 0)))) (h "0aj5lxss0bwy8ws0rwaw6szddmb85xcz9frra6wp8h9kwgc96hi7")))

(define-public crate-holochain_cli_signal_srv-0.2.0 (c (n "holochain_cli_signal_srv") (v "0.2.0") (d (list (d (n "holochain_trace") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tx5-signal-srv") (r "=0.0.1-alpha.7") (d #t) (k 0)))) (h "0wc7ny47g6v1585gbxwmyj3scs0fi24rkczgg7kdh6lv9w70lx5r")))

