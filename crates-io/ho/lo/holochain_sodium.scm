(define-module (crates-io ho lo holochain_sodium) #:use-module (crates-io))

(define-public crate-holochain_sodium-0.0.1-alpha1 (c (n "holochain_sodium") (v "0.0.1-alpha1") (d (list (d (n "lazy_static") (r "= 1.2.0") (d #t) (k 0)) (d (n "libc") (r "= 0.2.50") (d #t) (k 0)) (d (n "rust_sodium") (r "= 0.10.2") (d #t) (k 0)) (d (n "rust_sodium-sys") (r "= 0.10.4") (d #t) (k 0)))) (h "0857hinbabk5amjx0lwn2zp75bj168j9pdi6ylc1pmka928z4vr2")))

