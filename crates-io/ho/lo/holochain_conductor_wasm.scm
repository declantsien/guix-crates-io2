(define-module (crates-io ho lo holochain_conductor_wasm) #:use-module (crates-io))

(define-public crate-holochain_conductor_wasm-0.0.33-alpha5 (c (n "holochain_conductor_wasm") (v "0.0.33-alpha5") (d (list (d (n "holochain_core_types") (r "= 0.0.33-alpha5") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "0vb7p9hk161z0v8ck0fy1y2c5l7m2aszq614hdayrfcx7rgj2sx0")))

(define-public crate-holochain_conductor_wasm-0.0.33-alpha6 (c (n "holochain_conductor_wasm") (v "0.0.33-alpha6") (d (list (d (n "holochain_core_types") (r "= 0.0.33-alpha6") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "14nvy3dpzjvgpf826z1xjmfpp8qb10xmw09gclql4fg8il3d7v9g")))

(define-public crate-holochain_conductor_wasm-0.0.34-alpha1 (c (n "holochain_conductor_wasm") (v "0.0.34-alpha1") (d (list (d (n "holochain_core_types") (r "= 0.0.34-alpha1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "05fpjclclbgfrra3hs1lsxalxizfplwd12jb9xn9sbfiaj542d2j")))

(define-public crate-holochain_conductor_wasm-0.0.35-alpha7 (c (n "holochain_conductor_wasm") (v "0.0.35-alpha7") (d (list (d (n "holochain_core_types") (r "= 0.0.35-alpha7") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "1avqgkjnwb87m6cs56ib93y14pi8hgirvzxv5spvr25giirlihy7")))

(define-public crate-holochain_conductor_wasm-0.0.37-alpha6 (c (n "holochain_conductor_wasm") (v "0.0.37-alpha6") (d (list (d (n "holochain_core_types") (r "= 0.0.37-alpha6") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "1vscbgr682a041d1y0i7ijbch60cbils532xl93jwrgpzg0al405")))

(define-public crate-holochain_conductor_wasm-0.0.38-alpha2 (c (n "holochain_conductor_wasm") (v "0.0.38-alpha2") (d (list (d (n "holochain_core_types") (r "= 0.0.38-alpha2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "0dafc1gz2i8a4lz37fwpypxr60qmkjgsm67sh89yfy9q2sh33pwy")))

(define-public crate-holochain_conductor_wasm-0.0.38-alpha9 (c (n "holochain_conductor_wasm") (v "0.0.38-alpha9") (d (list (d (n "holochain_core_types") (r "= 0.0.38-alpha9") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "01cbpf6cs1q5yz9n1jzxbiynil826qvjx9nrjgvqn0ami8igg8y8")))

(define-public crate-holochain_conductor_wasm-0.0.38-alpha12 (c (n "holochain_conductor_wasm") (v "0.0.38-alpha12") (d (list (d (n "holochain_core_types") (r "= 0.0.38-alpha12") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "1yxhm5pxxbp6namwqrsasji1b8cnvdgbry06img2kj0m7smkfn47")))

(define-public crate-holochain_conductor_wasm-0.0.38-alpha13 (c (n "holochain_conductor_wasm") (v "0.0.38-alpha13") (d (list (d (n "holochain_core_types") (r "= 0.0.38-alpha13") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "0n0mlzq7rxs9rhccmwlpn57135kri0x7nlcvyaaxdhdsl8v1h4mh")))

(define-public crate-holochain_conductor_wasm-0.0.38-alpha14 (c (n "holochain_conductor_wasm") (v "0.0.38-alpha14") (d (list (d (n "holochain_core_types") (r "= 0.0.38-alpha14") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "09cmadv3l2y3fg09iagbn4xrdxqksdrrphsi9kxm40bkxnpq0cj3")))

(define-public crate-holochain_conductor_wasm-0.0.39-alpha2 (c (n "holochain_conductor_wasm") (v "0.0.39-alpha2") (d (list (d (n "holochain_core_types") (r "= 0.0.39-alpha2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "0lnwm0f0y4zm0rk74wc1cpzjy1hcc4m7dbkvac85hmjcyihm37kl")))

(define-public crate-holochain_conductor_wasm-0.0.39-alpha4 (c (n "holochain_conductor_wasm") (v "0.0.39-alpha4") (d (list (d (n "holochain_core_types") (r "= 0.0.39-alpha4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "1cf1m9a7xyf3girnmr9n3v3zdkxydz1kk3kp57hrvvsj3vj4pr92")))

(define-public crate-holochain_conductor_wasm-0.0.40-alpha1 (c (n "holochain_conductor_wasm") (v "0.0.40-alpha1") (d (list (d (n "holochain_core_types") (r "= 0.0.40-alpha1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "1rm911bjyscqq7agl9xj4p24sqlrc6mp29s7xsi49n7426qk790n")))

(define-public crate-holochain_conductor_wasm-0.0.41-alpha4 (c (n "holochain_conductor_wasm") (v "0.0.41-alpha4") (d (list (d (n "holochain_core_types") (r "= 0.0.41-alpha4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "1x8xkrfpl6dhm18yfar791fisrc42zlb6ndwk0xixpk87ylqy08f")))

(define-public crate-holochain_conductor_wasm-0.0.42-alpha1 (c (n "holochain_conductor_wasm") (v "0.0.42-alpha1") (d (list (d (n "holochain_core_types") (r "= 0.0.42-alpha1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "0zqgal34al9wb991w2v1h19flcrf1s664wrh8vih83pqj2as9r00")))

(define-public crate-holochain_conductor_wasm-0.0.42-alpha2 (c (n "holochain_conductor_wasm") (v "0.0.42-alpha2") (d (list (d (n "holochain_core_types") (r "= 0.0.42-alpha2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "0051ym9y2yq5wzk4wah4r2nnffd7dyr4xa4zm74dp4bp9byic14n")))

(define-public crate-holochain_conductor_wasm-0.0.42-alpha3 (c (n "holochain_conductor_wasm") (v "0.0.42-alpha3") (d (list (d (n "holochain_core_types") (r "= 0.0.42-alpha3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "0mg0yzzqc52d142v0nzchb2r0a5xl2r8axdhnqj9nhrnczxhw2ws")))

(define-public crate-holochain_conductor_wasm-0.0.42-alpha4 (c (n "holochain_conductor_wasm") (v "0.0.42-alpha4") (d (list (d (n "holochain_core_types") (r "= 0.0.42-alpha4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "1x8a238iw89lwkrk9axsgy3hpfrrzymqxn3y7yryv3199q0lvxgp")))

(define-public crate-holochain_conductor_wasm-0.0.42-alpha5 (c (n "holochain_conductor_wasm") (v "0.0.42-alpha5") (d (list (d (n "holochain_core_types") (r "= 0.0.42-alpha5") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "0yr22xszlnc0jnn1h0xqs533jwhhdizp78dd5y2hs1g50lhzpd2b")))

(define-public crate-holochain_conductor_wasm-0.0.43-alpha3 (c (n "holochain_conductor_wasm") (v "0.0.43-alpha3") (d (list (d (n "holochain_core_types") (r "= 0.0.43-alpha3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "06z2cd8qjpjv5r71h2pmbaxv12lgslc6f2jg5lpd9hgzh9bqqky2")))

(define-public crate-holochain_conductor_wasm-0.0.44-alpha1 (c (n "holochain_conductor_wasm") (v "0.0.44-alpha1") (d (list (d (n "holochain_core_types") (r "= 0.0.44-alpha1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "1mbqf7xisv66cjlqx0jvabsqpkab1gbikrpc5r6l2ksbgha83sha")))

(define-public crate-holochain_conductor_wasm-0.0.44-alpha2 (c (n "holochain_conductor_wasm") (v "0.0.44-alpha2") (d (list (d (n "holochain_core_types") (r "= 0.0.44-alpha2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "03gxr1nyz0yli86k3fg3x74gsngyzs5hw6y26h72qzf0p0md7b1j")))

(define-public crate-holochain_conductor_wasm-0.0.44-alpha3 (c (n "holochain_conductor_wasm") (v "0.0.44-alpha3") (d (list (d (n "holochain_core_types") (r "= 0.0.44-alpha3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "05w6wd9zkfnf3qcp2kcgzd5h6qxa7p4sl6c6yrgssxmz5cm0ghcx")))

(define-public crate-holochain_conductor_wasm-0.0.45-alpha1 (c (n "holochain_conductor_wasm") (v "0.0.45-alpha1") (d (list (d (n "holochain_core_types") (r "= 0.0.45-alpha1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "0jvq3xv1jiz6hkiyhxiy4gb0l6wbbvf9f24ymr96zgd7rby5vaah")))

(define-public crate-holochain_conductor_wasm-0.0.46-alpha1 (c (n "holochain_conductor_wasm") (v "0.0.46-alpha1") (d (list (d (n "holochain_core_types") (r "= 0.0.46-alpha1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "1z3mm9ay3rb89c78sgsapq21yj9rp3s78d70q8701aywjq0s2ibx")))

(define-public crate-holochain_conductor_wasm-0.0.47-alpha1 (c (n "holochain_conductor_wasm") (v "0.0.47-alpha1") (d (list (d (n "holochain_core_types") (r "= 0.0.47-alpha1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "1q2xx52v1vr4hr4485vhqhlszn9ccy051kba3i6c1akjaa0k0233")))

(define-public crate-holochain_conductor_wasm-0.0.48-alpha1 (c (n "holochain_conductor_wasm") (v "0.0.48-alpha1") (d (list (d (n "holochain_core_types") (r "= 0.0.48-alpha1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.32") (d #t) (k 0)))) (h "078pc07akajyh8a4mfhy6nkhy14jg1n03cb463dzck3ckzgs0ar3")))

(define-public crate-holochain_conductor_wasm-0.0.49-alpha1 (c (n "holochain_conductor_wasm") (v "0.0.49-alpha1") (d (list (d (n "holochain_core_types") (r "=0.0.49-alpha1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "=0.2.32") (d #t) (k 0)))) (h "1xh1qav6qzsli5h6bjr5alqvwja96cj6ih50y8azb1ysqbc4jka5")))

(define-public crate-holochain_conductor_wasm-0.0.50-alpha4 (c (n "holochain_conductor_wasm") (v "0.0.50-alpha4") (d (list (d (n "holochain_core_types") (r "=0.0.50-alpha4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "=0.2.32") (d #t) (k 0)))) (h "1hqvaymr3prqjkw9hz3fp01a6jb4izl2js8wyf1dkxfsiy9brn3z")))

(define-public crate-holochain_conductor_wasm-0.0.51-alpha1 (c (n "holochain_conductor_wasm") (v "0.0.51-alpha1") (d (list (d (n "holochain_core_types") (r "=0.0.51-alpha1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "=0.2.32") (d #t) (k 0)))) (h "0q3fvj99738bgs5nlx1h7217yqjwn8xanlbjiiyv36jvd5s85l7w")))

(define-public crate-holochain_conductor_wasm-0.0.52-alpha1 (c (n "holochain_conductor_wasm") (v "0.0.52-alpha1") (d (list (d (n "holochain_core_types") (r "=0.0.52-alpha1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "=0.2.48") (d #t) (k 0)))) (h "0m6fr4181ym1hsxvqiylx3pqrrm7jwwc8yv1adsnkdmrhmcr98sy")))

(define-public crate-holochain_conductor_wasm-0.0.52-alpha2 (c (n "holochain_conductor_wasm") (v "0.0.52-alpha2") (d (list (d (n "holochain_core_types") (r "=0.0.52-alpha2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "=0.2.48") (d #t) (k 0)))) (h "058w3xb0cg6qxrr4sdbd18chjy9b0v27jqhjvsng96g0r0yr2wiv")))

