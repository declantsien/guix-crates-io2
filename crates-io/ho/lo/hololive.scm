(define-module (crates-io ho lo hololive) #:use-module (crates-io))

(define-public crate-hololive-0.1.0 (c (n "hololive") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1zr0y9xzlwvr7gxipmnhbz999nndcjsvn2xsbp7wdjrr6g2f0kfl")))

(define-public crate-hololive-0.1.1 (c (n "hololive") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1zagd17w2089fc54rjapk5wqpnafrkwam0h2lw372s62ww4scgmk")))

(define-public crate-hololive-0.1.2 (c (n "hololive") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "10pg9ghxbkzp3acyqbpw7mizz445mhs1jss0fpb1ry49615nqbd5")))

