(define-module (crates-io ho lo holochain-app-ws-tester) #:use-module (crates-io))

(define-public crate-holochain-app-ws-tester-0.1.0 (c (n "holochain-app-ws-tester") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "holochain_client") (r "=0.4.2") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (d #t) (k 0)))) (h "1ijxc5hpq35q2d105gwaivqw35kqgf8gaw53ddacn5qr65w37xv8")))

(define-public crate-holochain-app-ws-tester-0.1.1 (c (n "holochain-app-ws-tester") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "holochain_client") (r "=0.4.3") (d #t) (k 0)) (d (n "serde") (r "=1.0.166") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (d #t) (k 0)))) (h "0jipyvhz3a0a8iazixvy856k33jqiv3hpk9m93ydbvasrlbgf2qb")))

