(define-module (crates-io ho lo holochain-services) #:use-module (crates-io))

(define-public crate-holochain-services-0.2.0-beta-dev.0 (c (n "holochain-services") (v "0.2.0-beta-dev.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "holochain_keystore") (r "^0.3.0-beta-dev.18") (k 0)) (d (n "holochain_types") (r "^0.3.0-beta-dev.17") (d #t) (k 0)) (d (n "mockall") (r "^0.11") (d #t) (k 0)) (d (n "must_future") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1b792khf0z4422hnwdl31fv12qs93x5xhjfasgaqrxz42b4lkx80")))

