(define-module (crates-io ho lo holochain_test_wasm_common) #:use-module (crates-io))

(define-public crate-holochain_test_wasm_common-0.0.1 (c (n "holochain_test_wasm_common") (v "0.0.1") (d (list (d (n "hdk") (r "^0.0.101") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1l81xzqiinhc79k7ikba6n17524z8nba332g6049zdd53n3j8lac")))

(define-public crate-holochain_test_wasm_common-0.0.2 (c (n "holochain_test_wasm_common") (v "0.0.2") (d (list (d (n "hdk") (r "^0.0.102") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0j0j60w4bl1n6g2lxk39wad5k3610y8424phvz9mb9w78x7zx0y9")))

(define-public crate-holochain_test_wasm_common-0.0.3 (c (n "holochain_test_wasm_common") (v "0.0.3") (d (list (d (n "hdk") (r "^0.0.103") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1532fk369kf3808drq6il7a3c8y8p8zck02sx1cpw4vpzdj1w3mk")))

(define-public crate-holochain_test_wasm_common-0.0.4 (c (n "holochain_test_wasm_common") (v "0.0.4") (d (list (d (n "hdk") (r "^0.0.104") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "02az6dpl28kk23nngkjl1j8a22nr4icixvvrbp6ls2cq2p21rld3")))

(define-public crate-holochain_test_wasm_common-0.0.6 (c (n "holochain_test_wasm_common") (v "0.0.6") (d (list (d (n "hdk") (r "^0.0.106") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1na9ldqf1j5b9sh73za3vpdg9haipyhr2kg78s8aswjwiy897hpk")))

(define-public crate-holochain_test_wasm_common-0.0.7 (c (n "holochain_test_wasm_common") (v "0.0.7") (d (list (d (n "hdk") (r "^0.0.107") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "04cw4wfjk9ccvrfag3ryjn70higgl5f4vl6wb9p6k5kkz4dhbz69")))

(define-public crate-holochain_test_wasm_common-0.0.8 (c (n "holochain_test_wasm_common") (v "0.0.8") (d (list (d (n "hdk") (r "^0.0.108") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "12mmw9sc76hqaf0j7xnk4xk8mwzzwyariyn5xfgpx3yp66yf57v5")))

(define-public crate-holochain_test_wasm_common-0.0.9 (c (n "holochain_test_wasm_common") (v "0.0.9") (d (list (d (n "hdk") (r "^0.0.109") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0wazjdln0fcism33sjdp03vj03r474dricj3cnp5hh5z7rj3ca3z")))

(define-public crate-holochain_test_wasm_common-0.0.10 (c (n "holochain_test_wasm_common") (v "0.0.10") (d (list (d (n "hdk") (r "^0.0.110") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1hf2fcksgd36s4p0iny08m5wl279dg59h5ri8x76j4817fjaid3i")))

(define-public crate-holochain_test_wasm_common-0.0.11 (c (n "holochain_test_wasm_common") (v "0.0.11") (d (list (d (n "hdk") (r "^0.0.111") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "07p0hp15gldnql6yqf87kh6ar96g9l11k6ml6ymxcs6nfvjzxax3")))

(define-public crate-holochain_test_wasm_common-0.0.12 (c (n "holochain_test_wasm_common") (v "0.0.12") (d (list (d (n "hdk") (r "^0.0.112") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0d1c6m26zngvrzbcvwkcgqyl8l882mrnwr6il20885xnmlnvdshd")))

(define-public crate-holochain_test_wasm_common-0.0.13 (c (n "holochain_test_wasm_common") (v "0.0.13") (d (list (d (n "hdk") (r "^0.0.113") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1l940sl8mibz4qbhgf276pwcwg2m85nw213n3vdn910vp5866kvf")))

(define-public crate-holochain_test_wasm_common-0.0.14 (c (n "holochain_test_wasm_common") (v "0.0.14") (d (list (d (n "hdk") (r "^0.0.114") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0gmqdi7372wqjp15s2jhcxw39rcg89qac5ldaz72k9g8b8fbl1mv")))

(define-public crate-holochain_test_wasm_common-0.0.15 (c (n "holochain_test_wasm_common") (v "0.0.15") (d (list (d (n "hdk") (r "^0.0.115") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1nwijpq84vly1pr8zzagzf7xzdjz6dcp11r5yk7l119dp3qp873k")))

(define-public crate-holochain_test_wasm_common-0.0.16 (c (n "holochain_test_wasm_common") (v "0.0.16") (d (list (d (n "hdk") (r "^0.0.116") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "037zczwvr88w07l7l9mccxwv0qnqk0c963253bz4lsx4s0z5a834")))

(define-public crate-holochain_test_wasm_common-0.0.17 (c (n "holochain_test_wasm_common") (v "0.0.17") (d (list (d (n "hdk") (r "^0.0.117") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1dzbka3hfsvv80c0g1v4jw7z3hq9n06qcrrmq4915s6jmsypmyr8")))

(define-public crate-holochain_test_wasm_common-0.0.18 (c (n "holochain_test_wasm_common") (v "0.0.18") (d (list (d (n "hdk") (r "^0.0.118") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1qizjrwhsy8mlic04j82hvynb596wsc2jngipbsk7f1nkr0i79qd")))

(define-public crate-holochain_test_wasm_common-0.0.19 (c (n "holochain_test_wasm_common") (v "0.0.19") (d (list (d (n "hdk") (r "^0.0.119") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1qs19m7s005w97hbq14rp75zar8596yf1gy2fvvxbbnkca6cyvhi")))

(define-public crate-holochain_test_wasm_common-0.0.20 (c (n "holochain_test_wasm_common") (v "0.0.20") (d (list (d (n "hdk") (r "^0.0.120") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1j2958jy0f9l5g8insri2ahhwfn5lbr9j3dx0nvd3p5v49x8ccaa")))

(define-public crate-holochain_test_wasm_common-0.0.21 (c (n "holochain_test_wasm_common") (v "0.0.21") (d (list (d (n "hdk") (r "^0.0.121") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1rsj4vi6qi1g256d0sc0rsqf4z5f57i1qlksbp2inl8z2yplqas0")))

(define-public crate-holochain_test_wasm_common-0.0.22 (c (n "holochain_test_wasm_common") (v "0.0.22") (d (list (d (n "hdk") (r "^0.0.122") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1nqpzixrma5gskhj4bqchyhc7h877w6sh2abwagkcxrnr03yxr31")))

(define-public crate-holochain_test_wasm_common-0.0.23 (c (n "holochain_test_wasm_common") (v "0.0.23") (d (list (d (n "hdk") (r "^0.0.123") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0b0j0hvrsn0h8lyc577wgkygrmwcxziaf6x2y5kqyrqcfj4cmgxq")))

(define-public crate-holochain_test_wasm_common-0.0.24 (c (n "holochain_test_wasm_common") (v "0.0.24") (d (list (d (n "hdk") (r "^0.0.124") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0jvx21y1dfjj82a5nqz0qim8qszvqf64pmsxabi2dwpbnj5544m7")))

(define-public crate-holochain_test_wasm_common-0.0.25 (c (n "holochain_test_wasm_common") (v "0.0.25") (d (list (d (n "hdk") (r "^0.0.125") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1qvap7bbg95s3hswzgqk5646k7svzpgc5z5m9q2p21ycswp9z2y4")))

(define-public crate-holochain_test_wasm_common-0.0.26 (c (n "holochain_test_wasm_common") (v "0.0.26") (d (list (d (n "hdk") (r "^0.0.126") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0559cjb3c6zwmj4nwlxlq9cz67pgxdw38izwph4fhlc0vcvzanaj")))

(define-public crate-holochain_test_wasm_common-0.0.27 (c (n "holochain_test_wasm_common") (v "0.0.27") (d (list (d (n "hdk") (r "^0.0.127") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0ws98fmjnf04g7ivqnm67bzlxch25xd72qq37213hj30ax747sq2")))

(define-public crate-holochain_test_wasm_common-0.0.28 (c (n "holochain_test_wasm_common") (v "0.0.28") (d (list (d (n "hdk") (r "^0.0.128-dev.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0prh7fb131d84mxk3ickkh37m5c4ab2gcm26m4gvldksfzyjnpmm")))

(define-public crate-holochain_test_wasm_common-0.0.29 (c (n "holochain_test_wasm_common") (v "0.0.29") (d (list (d (n "hdk") (r "^0.0.128") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "18sc1zfx3sw7j6ynnjhvq5j1b7k22mf4hxj85a972jwzk50lggv4")))

(define-public crate-holochain_test_wasm_common-0.0.30 (c (n "holochain_test_wasm_common") (v "0.0.30") (d (list (d (n "hdk") (r "^0.0.129") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0lad3z4j8cn5cca1s27hjc3pqlpij6pyjk1471j711qd2fxi21v0")))

(define-public crate-holochain_test_wasm_common-0.0.31 (c (n "holochain_test_wasm_common") (v "0.0.31") (d (list (d (n "hdk") (r "^0.0.130") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1fy4di355n8xrx7dy2ij2z39lrpv8pcpyq2lzzmmvhbgab3pb7zb")))

(define-public crate-holochain_test_wasm_common-0.0.32 (c (n "holochain_test_wasm_common") (v "0.0.32") (d (list (d (n "hdk") (r "^0.0.131") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "07m6k1ydx81imx6n0r1wsbi2b5gns9hmak0akz5f3ysx2xdk4vpf")))

(define-public crate-holochain_test_wasm_common-0.0.33 (c (n "holochain_test_wasm_common") (v "0.0.33") (d (list (d (n "hdk") (r "^0.0.132") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "16a0kq0py11076qlv9np15q0krp9i9lvpy1f6a0czn1gnvii5m20")))

(define-public crate-holochain_test_wasm_common-0.0.34 (c (n "holochain_test_wasm_common") (v "0.0.34") (d (list (d (n "hdk") (r "^0.0.133") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1ynhpj5mdyxrd8xzx1py31dd9m8vng82vifi43bqvxqs42x5sg9g")))

(define-public crate-holochain_test_wasm_common-0.0.35 (c (n "holochain_test_wasm_common") (v "0.0.35") (d (list (d (n "hdk") (r "^0.0.134") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1ph2806kr6crj3awmjmz7pi1j4dd70w5wck0df6s30zljgxn46cq")))

(define-public crate-holochain_test_wasm_common-0.0.36 (c (n "holochain_test_wasm_common") (v "0.0.36") (d (list (d (n "hdk") (r "^0.0.135") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0362vwc091kr1ki2z28il5vnqajn12a6fcpppabih3vm22a5yi1p")))

(define-public crate-holochain_test_wasm_common-0.0.37 (c (n "holochain_test_wasm_common") (v "0.0.37") (d (list (d (n "hdk") (r "^0.0.136") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1cbhzzlaydsfpk6y0pp346dx5y9issh17qx1mqkzasyjgbqw4m81")))

(define-public crate-holochain_test_wasm_common-0.0.38 (c (n "holochain_test_wasm_common") (v "0.0.38") (d (list (d (n "hdk") (r "^0.0.137") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1ri41mixkw9na3p25nh7c0j2ww65pwsrwdjgv6al48a8v0zcs3kq")))

(define-public crate-holochain_test_wasm_common-0.0.39 (c (n "holochain_test_wasm_common") (v "0.0.39") (d (list (d (n "hdk") (r "^0.0.138") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1x8028y4x1s136w2817chh1djizh7yq1hilaamy92nic4fa9aiiy")))

(define-public crate-holochain_test_wasm_common-0.0.40 (c (n "holochain_test_wasm_common") (v "0.0.40") (d (list (d (n "hdk") (r "^0.0.139") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0387ia6zy133mx8ijh4855m33az8920279raz8x0zmhahgl057rk")))

(define-public crate-holochain_test_wasm_common-0.0.41 (c (n "holochain_test_wasm_common") (v "0.0.41") (d (list (d (n "hdk") (r "^0.0.140") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0f13fx4lnn125aicpbxgzgb2nl7qp9lc4wz3c068pzm3la0jz2vv")))

(define-public crate-holochain_test_wasm_common-0.0.42 (c (n "holochain_test_wasm_common") (v "0.0.42") (d (list (d (n "hdk") (r "^0.0.141") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1bdwwspm3fk3vakrhvb7j2hi80iy7n8gl2bzb94f69f4mxirymdb")))

(define-public crate-holochain_test_wasm_common-0.0.43 (c (n "holochain_test_wasm_common") (v "0.0.43") (d (list (d (n "hdk") (r "^0.0.142") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "15fvgszg7jy9f07yk9mqk4ydf16zb5ql8va259lakb4av5dzdnam")))

(define-public crate-holochain_test_wasm_common-0.0.44 (c (n "holochain_test_wasm_common") (v "0.0.44") (d (list (d (n "hdk") (r "^0.0.143") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "173x1bd63fniklw1f1vxmsmp40pslp9spdz94qv1gps7rzlr59f0")))

(define-public crate-holochain_test_wasm_common-0.0.45 (c (n "holochain_test_wasm_common") (v "0.0.45") (d (list (d (n "hdk") (r "^0.0.144") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0z5f448nkhs7zy0as8xwa52p01hjfjm8krfipvmg30vwdvfwpyig")))

(define-public crate-holochain_test_wasm_common-0.0.46 (c (n "holochain_test_wasm_common") (v "0.0.46") (d (list (d (n "hdk") (r "^0.0.145") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "191x97vayxp67rhyyczmr0qad3dyp1a7vp2mjgb0z8vv9954brns")))

(define-public crate-holochain_test_wasm_common-0.0.47 (c (n "holochain_test_wasm_common") (v "0.0.47") (d (list (d (n "hdk") (r "^0.0.146") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "188s64apaagdah15znazw5g4w71rc0y01j0d58fcvzb7pvm6wkiy")))

(define-public crate-holochain_test_wasm_common-0.0.48 (c (n "holochain_test_wasm_common") (v "0.0.48") (d (list (d (n "hdk") (r "^0.0.147") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1yr70k9brk6k84cm7k8ddyd5ghp9gygbxq4aix50057maj8q04ng")))

(define-public crate-holochain_test_wasm_common-0.0.49 (c (n "holochain_test_wasm_common") (v "0.0.49") (d (list (d (n "hdk") (r "^0.0.148") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1xid6fkyqmva3xk2hzsdyyfm688jryxs3a25w0zmrj6xxzzjm9y6")))

(define-public crate-holochain_test_wasm_common-0.0.50 (c (n "holochain_test_wasm_common") (v "0.0.50") (d (list (d (n "hdk") (r "^0.0.149") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0r382qry3vidhibx307ff3d0i304diisf9y25kcq79wijf23ahha")))

(define-public crate-holochain_test_wasm_common-0.0.51 (c (n "holochain_test_wasm_common") (v "0.0.51") (d (list (d (n "hdk") (r "^0.0.150") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1p1h1b88iyv6hnj8m2ppi7cbdcxf5xily25bpxy7xwz3ffc10kyf")))

(define-public crate-holochain_test_wasm_common-0.0.52 (c (n "holochain_test_wasm_common") (v "0.0.52") (d (list (d (n "hdk") (r "^0.0.151") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0lszn7ff1gl1hxlfy9j3fgnc9hdfbjq66ac7sq58gc2axjii91as")))

(define-public crate-holochain_test_wasm_common-0.0.53 (c (n "holochain_test_wasm_common") (v "0.0.53") (d (list (d (n "hdk") (r "^0.0.152") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "05i1w50jqshddfmc3sh841q1mwnn4hp1p6fblxm0x526mg374r3i")))

(define-public crate-holochain_test_wasm_common-0.0.54 (c (n "holochain_test_wasm_common") (v "0.0.54") (d (list (d (n "hdk") (r "^0.0.153") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0wina8xyhlwhz4j50clbdxcvc6frdwyzgbk5vzdrfkrchc6rg0iz")))

(define-public crate-holochain_test_wasm_common-0.0.55 (c (n "holochain_test_wasm_common") (v "0.0.55") (d (list (d (n "hdk") (r "^0.0.154") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0jkccb0hvr2vscc6b9sdhla9z5jjh190smbfmic4dhwnp8zbnrrn")))

(define-public crate-holochain_test_wasm_common-0.0.56 (c (n "holochain_test_wasm_common") (v "0.0.56") (d (list (d (n "hdk") (r "^0.0.155") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0h745f1jnlylmzyzxya1a185lzirfxk2j8cywbk90wsa87rgcmwl")))

(define-public crate-holochain_test_wasm_common-0.0.57 (c (n "holochain_test_wasm_common") (v "0.0.57") (d (list (d (n "hdk") (r "^0.0.156") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0yzk5c90yhgikjfq4l8iaibppmxzzad89cp6a1s9yzjggfnifnii")))

(define-public crate-holochain_test_wasm_common-0.0.58 (c (n "holochain_test_wasm_common") (v "0.0.58") (d (list (d (n "hdk") (r "^0.0.157") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1bivbn9jcz3l2maf9g3fac167ngdw74r5i8aqnzdfavmxm66sz1b")))

(define-public crate-holochain_test_wasm_common-0.0.59 (c (n "holochain_test_wasm_common") (v "0.0.59") (d (list (d (n "hdk") (r "^0.0.158") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0s5xy9zckzdfijfkg2f3dxipy1sd0v00ls3zqmpvjs3p64f0mpl0")))

(define-public crate-holochain_test_wasm_common-0.0.60 (c (n "holochain_test_wasm_common") (v "0.0.60") (d (list (d (n "hdk") (r "^0.0.159") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0s2n9i7hnlwkphc67qc6z925g63w2l879c97da7h4mrs9kyw066i")))

(define-public crate-holochain_test_wasm_common-0.0.61 (c (n "holochain_test_wasm_common") (v "0.0.61") (d (list (d (n "hdk") (r "^0.0.160") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1cn7nanqzpc6hbn2qcqwasmdgd1rf4lrryzv8pjiwbps5dy223pa")))

(define-public crate-holochain_test_wasm_common-0.0.62 (c (n "holochain_test_wasm_common") (v "0.0.62") (d (list (d (n "hdk") (r "^0.0.161") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0pj5r3j3k4m8ll4nq6n52fwh1cplksbrabw0a8bjhb6mgk24il97")))

(define-public crate-holochain_test_wasm_common-0.0.63 (c (n "holochain_test_wasm_common") (v "0.0.63") (d (list (d (n "hdk") (r "^0.0.162") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1clbx96hhxbk6033yd8i0x8rlvgrxr9pnsm4k81vvrbw6ng2xahg")))

(define-public crate-holochain_test_wasm_common-0.0.64 (c (n "holochain_test_wasm_common") (v "0.0.64") (d (list (d (n "hdk") (r "^0.0.163") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0hsha2bi8hiy1mz6ikrlc3jcjv68sy93w8w5dcgk807xfnql04r2")))

(define-public crate-holochain_test_wasm_common-0.1.0-beta-rc.0 (c (n "holochain_test_wasm_common") (v "0.1.0-beta-rc.0") (d (list (d (n "hdk") (r "^0.1.0-beta-rc.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1bwx32db13l3jnjiz8q787kkj0f07yy2n3snjcqmp390dk1anabs")))

(define-public crate-holochain_test_wasm_common-0.1.0-beta-rc.1 (c (n "holochain_test_wasm_common") (v "0.1.0-beta-rc.1") (d (list (d (n "hdk") (r "^0.1.0-beta-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "18vh81x00rpkl1p6f48m0mzsrrldyxk98ng5568zfzq413ck5gnk")))

(define-public crate-holochain_test_wasm_common-0.1.0-beta-rc.2 (c (n "holochain_test_wasm_common") (v "0.1.0-beta-rc.2") (d (list (d (n "hdk") (r "^0.1.0-beta-rc.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1bxjbkz97lhfkxlbaqnvh6r46z06a4pfsqrh3yy58v4msjpr729v")))

(define-public crate-holochain_test_wasm_common-0.1.0-beta-rc.3 (c (n "holochain_test_wasm_common") (v "0.1.0-beta-rc.3") (d (list (d (n "hdk") (r "^0.1.0-beta-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1p049q20afn6sps4v0cdgg0mkjy6ka5w2davfajxga4mccqx841n")))

(define-public crate-holochain_test_wasm_common-0.1.0 (c (n "holochain_test_wasm_common") (v "0.1.0") (d (list (d (n "hdk") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "175qnpw34pzdx0gvf9l34bfpv0lbdsqjdi6c5nclb4cs5nkij2m9")))

(define-public crate-holochain_test_wasm_common-0.1.1 (c (n "holochain_test_wasm_common") (v "0.1.1") (d (list (d (n "hdk") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "13rnynapjapnh5sfpn6ib17scd6xl1ys20k8am8jrld8542w53xc")))

(define-public crate-holochain_test_wasm_common-0.2.0-beta-rc.0 (c (n "holochain_test_wasm_common") (v "0.2.0-beta-rc.0") (d (list (d (n "hdk") (r "^0.2.0-beta-rc.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0z198w806x2xdkci6avyvzhqfc2ai82vmwdhcr5417i2izq48f8q")))

(define-public crate-holochain_test_wasm_common-0.2.0-beta-rc.1 (c (n "holochain_test_wasm_common") (v "0.2.0-beta-rc.1") (d (list (d (n "hdk") (r "^0.2.0-beta-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0a0bl7scf867fzpfgpa1vb9p7giad0kidrh7qm3lnwgvmd5pjzd3")))

(define-public crate-holochain_test_wasm_common-0.2.0-beta-rc.2 (c (n "holochain_test_wasm_common") (v "0.2.0-beta-rc.2") (d (list (d (n "hdk") (r "^0.2.0-beta-rc.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0sk27a8wh0i4yrxaqnlj7sakf4w9r8v2mvjdypm4k1842hfb2b0z")))

(define-public crate-holochain_test_wasm_common-0.2.0-beta-rc.3 (c (n "holochain_test_wasm_common") (v "0.2.0-beta-rc.3") (d (list (d (n "hdk") (r "^0.2.0-beta-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0bycnjfnx7bg46vy2z7bhzln7pijz325azawk62ayyk05njb0yz0")))

(define-public crate-holochain_test_wasm_common-0.2.0-beta-rc.4 (c (n "holochain_test_wasm_common") (v "0.2.0-beta-rc.4") (d (list (d (n "hdk") (r "^0.2.0-beta-rc.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1ln4l0gs43w6wm9b8h20m4vqysxhq5a3z8rgc049kwmww8k1b1ki")))

(define-public crate-holochain_test_wasm_common-0.1.2 (c (n "holochain_test_wasm_common") (v "0.1.2") (d (list (d (n "hdk") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "19d5d8y80wa2301sv2apb5b7d84fj70wpakfx4gnhh1wq6c91gwp")))

(define-public crate-holochain_test_wasm_common-0.2.0-beta-rc.5 (c (n "holochain_test_wasm_common") (v "0.2.0-beta-rc.5") (d (list (d (n "hdk") (r "^0.2.0-beta-rc.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0ifgnzwg7pl5d2w6a8q1iwqw3klvbyzlwj6li8sgr4mjvlyx3pmg")))

(define-public crate-holochain_test_wasm_common-0.1.3-beta-rc.0 (c (n "holochain_test_wasm_common") (v "0.1.3-beta-rc.0") (d (list (d (n "hdk") (r "^0.1.3-beta-rc.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1gv3irai4wdpjxaw2pnqi21nha4zax7z6bx46pza6j1hxf1n0ivq")))

(define-public crate-holochain_test_wasm_common-0.2.0-beta-rc.6 (c (n "holochain_test_wasm_common") (v "0.2.0-beta-rc.6") (d (list (d (n "hdk") (r "^0.2.0-beta-rc.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "11nja7vcgjn38ygv25z8byvqy9wjdhsi5ckhi21sav245bf1l4af")))

(define-public crate-holochain_test_wasm_common-0.2.0 (c (n "holochain_test_wasm_common") (v "0.2.0") (d (list (d (n "hdk") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1p0bfsmywpr44vxmmikg1qy2w8b4cki9qnm5ivhyn8brczfxamfi")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.0 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.0") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1man6gykglry9g5wxm01yxz7f1a222bwzrdnrnmy2lhgfdk3nk1y")))

(define-public crate-holochain_test_wasm_common-0.1.3-beta-rc.1 (c (n "holochain_test_wasm_common") (v "0.1.3-beta-rc.1") (d (list (d (n "hdk") (r "^0.1.3-beta-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0h0gb6wijn9d1nx7n8163cr105xfk3jg5ldjznzz343vmjsz80s8")))

(define-public crate-holochain_test_wasm_common-0.2.1-beta-dev.0 (c (n "holochain_test_wasm_common") (v "0.2.1-beta-dev.0") (d (list (d (n "hdk") (r "^0.2.1-beta-dev.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0b3la41wg12zqb0d2wzywzja3ri5bhw5f92ffsmdnh3sr7wpqazv")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.1 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.1") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0cp1saz39nzjz89paiwli3cnr56b0b9hhiqyg12adfbq8ib5bpni")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.2 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.2") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1414mzdm1agswic4acb3w0d4i7clq6y396f9sdnr80zn9cgcjz13")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.3 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.3") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0s4xlrlkvyl0wpsz52s9nblvm2asrnxxqkh56s7q38pnrrr10sa7")))

(define-public crate-holochain_test_wasm_common-0.1.3 (c (n "holochain_test_wasm_common") (v "0.1.3") (d (list (d (n "hdk") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "179w58sy9f6al3cihbk1qmkj0wjbyviwm0d0zyyz0yag5fxpmhi0")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.4 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.4") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "150yihcaj121572h1w06kgj952js1d5ii15lc2i4alk1sl9lqhsw")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.5 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.5") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0584w2vprirsxq7fsfz266l3q7idn9vzcn75vj0mxgi20whqm8x5")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.6 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.6") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "10i4g9spjd8j50z461i0hrsx0m8r0aq93kavm35860d4kaf29ibk")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.7 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.7") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "08dc7bzd53h7j4dm7vmfg7gns085x82xszhv8fnn84yzd2mdbcls")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.8 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.8") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "073295aqhj82xcm97mjyivaj2n60w9l3pxad8bmak7bf4imkjcnl")))

(define-public crate-holochain_test_wasm_common-0.2.1-beta-rc.0 (c (n "holochain_test_wasm_common") (v "0.2.1-beta-rc.0") (d (list (d (n "hdk") (r "^0.2.1-beta-rc.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1jx7mrjqbkxl6sjahba38vmzrb2z43d72739ngkmy2vi8zbkcmfy")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.9 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.9") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "174221yx91grdsf53sq92vill5f9rzx2s04n0drykksx88ghjrh9")))

(define-public crate-holochain_test_wasm_common-0.2.1 (c (n "holochain_test_wasm_common") (v "0.2.1") (d (list (d (n "hdk") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0rlfyr3jwad7929l6gai232qa6gi1vvfy099gglyjlhg0cdy57gc")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.10 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.10") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0fw40wp7n825m36n8cnldvjgr3y7kc7ai0gxfs13y63b0ma8wd4x")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.11 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.11") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0vfzkng1w182ca5ji0iwjs09kkx7a9lra861fzl4h2lfxf9105sv")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.12 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.12") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0ga0iyfyqdnr2bkqy3zvp8v0kbgiy7cx3ikqkmn58mpdzc46c1yr")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.13 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.13") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1dwr4c05hadl9cpmm668kdmhq5gp7bvnbb7cjvhb5pkq5rdlv4r3")))

(define-public crate-holochain_test_wasm_common-0.1.4-beta-rc.0 (c (n "holochain_test_wasm_common") (v "0.1.4-beta-rc.0") (d (list (d (n "hdk") (r "^0.1.4-beta-rc.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0yf99r152fkfvlcv8gbpp2h4mpr1b26z3vrrrdp3l01ayv7xz76g")))

(define-public crate-holochain_test_wasm_common-0.1.4 (c (n "holochain_test_wasm_common") (v "0.1.4") (d (list (d (n "hdk") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1dzwcviw7z9pcrn9az4yklycsiimjdrwkifljnrm3gixxvfzgml5")))

(define-public crate-holochain_test_wasm_common-0.2.2-beta-rc.0 (c (n "holochain_test_wasm_common") (v "0.2.2-beta-rc.0") (d (list (d (n "hdk") (r "^0.2.2-beta-rc.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1y7xn7s8fblzjiyxzk14rmsh1xravz0s8i5lc1ziv8x67iyh1f5s")))

(define-public crate-holochain_test_wasm_common-0.2.2-beta-rc.1 (c (n "holochain_test_wasm_common") (v "0.2.2-beta-rc.1") (d (list (d (n "hdk") (r "^0.2.2-beta-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0950lrb6mpd1dzrjv9qp4ik56swps6ax5n2s6sm1a4q48k692fb0")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.14 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.14") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "14v63wl9pwwqjnp07pdkmd04dll1n1520ili4qg283hxnfsnplx0")))

(define-public crate-holochain_test_wasm_common-0.2.2 (c (n "holochain_test_wasm_common") (v "0.2.2") (d (list (d (n "hdk") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "091118bc62qyda8ffxsn2n3jhzw8ayxl7bp4ap5qz4x8vkc1j6js")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.15 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.15") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "04q9hfpcpa5kps7wjcmdfn2lynxzsd27j49jrh8wl2vfz0w4d2hf")))

(define-public crate-holochain_test_wasm_common-0.2.3-beta-rc.0 (c (n "holochain_test_wasm_common") (v "0.2.3-beta-rc.0") (d (list (d (n "hdk") (r "^0.2.3-beta-rc.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0464yhsyx4aa6k10m1nd4mnyxqwiaygfqxfqb7ryah81hvpsqwgn")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.16 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.16") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "05als4iplxn0wvckryvfm51dkhyins3ycv9j9b2zswnrqlx6glb7")))

(define-public crate-holochain_test_wasm_common-0.1.5-rc.0 (c (n "holochain_test_wasm_common") (v "0.1.5-rc.0") (d (list (d (n "hdk") (r "^0.1.5-rc.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1bgb82jkv06n14yvjr6kj18lk2prnpj55akx4cy17p3yyzc300rk")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.17 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.17") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1yd1czm0m4a8bkrqfgr33wfgjf7rxly5rq1c7cix7lral67jjghx")))

(define-public crate-holochain_test_wasm_common-0.1.5 (c (n "holochain_test_wasm_common") (v "0.1.5") (d (list (d (n "hdk") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "02lkmfbfjv9rmv3yh8p8vibh3w58mn91y3a9w4p2jyaapmdgd20c")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.18 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.18") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1vbm6j52pdbd4ai1qrw8m5kmdxdzdgqrm3s3w75b87b2jdl0bgnv")))

(define-public crate-holochain_test_wasm_common-0.2.3-rc.0 (c (n "holochain_test_wasm_common") (v "0.2.3-rc.0") (d (list (d (n "hdk") (r "^0.2.3-rc.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0, <=1.0.166") (d #t) (k 0)))) (h "0aic3gqhx6wyqdz052ccx4amwj4z52a7360x71kch698vh71cich")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.19 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.19") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "097dj039a5wpf6560glq8z21ib5qffz3vap7znvncafi4daw4z33")))

(define-public crate-holochain_test_wasm_common-0.2.3-rc.1 (c (n "holochain_test_wasm_common") (v "0.2.3-rc.1") (d (list (d (n "hdk") (r "^0.2.3-rc.1") (d #t) (k 0)) (d (n "serde") (r ">=1.0, <=1.0.166") (d #t) (k 0)))) (h "0r97siz0k6n7aby19b2qc2s7299w5lilmj9snqr0qxmkw0mkdjr9")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.20 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.20") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1z8gxsqrasdg82jwm4qii6immbw3i85whnh292883g7gc3p8s00h")))

(define-public crate-holochain_test_wasm_common-0.2.3 (c (n "holochain_test_wasm_common") (v "0.2.3") (d (list (d (n "hdk") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r ">=1.0, <=1.0.166") (d #t) (k 0)))) (h "1sdm0w1myhqrxqhccwqgdj29qgf588wd58s88pkc497z10r77pdy")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.21 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.21") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.21") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0bsyyzg6yr6w3iqlbsgcb9nq6wm40mqj0y53h550jz4slbynndzi")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.22 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.22") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "03x5zbvbk7j6rj19j77dyh2pbjb4lrvs89h1hn17l712grl88fwc")))

(define-public crate-holochain_test_wasm_common-0.2.4-rc.0 (c (n "holochain_test_wasm_common") (v "0.2.4-rc.0") (d (list (d (n "hdk") (r "^0.2.4-rc.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0, <=1.0.166") (d #t) (k 0)))) (h "1ibs9whq1gf9zjyf8z6hqf6wqpldnda6vkyqdwgxh6p0kzf9vaf1")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.23 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.23") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.23") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1ry84y1z8945c0z0wq8p32ia6kz2dpc48qcyc4ia1yfmqhkxrn3s")))

(define-public crate-holochain_test_wasm_common-0.2.4 (c (n "holochain_test_wasm_common") (v "0.2.4") (d (list (d (n "hdk") (r "^0.2.4") (d #t) (k 0)) (d (n "serde") (r ">=1.0, <=1.0.166") (d #t) (k 0)))) (h "1r4bjn8dhrsca10ig77v1j8lscvbl6xyjmifgp2piai1qpwds56w")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.24 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.24") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "07x8nk8lrjm7m8xbfq65yhjikhv154jbslc22x1qn7dzqmlqjw4f")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.25 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.25") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.25") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1s5kipvc4ls2f28p7l6ywfl9ypisnz4nx1xy7czfgcagywa4ywax")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.26 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.26") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0hmyl5cz55dqkghwpzya6djkrixw5wy2kc2i70dg90dcf71i03ci")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.27 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.27") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.27") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1984b5f1iza0sa8iqpgvlnspac4smqj7vvbr0bwc15nlh5216if9")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.28 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.28") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.28") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1ysbkqbvhp0y3rmbszpi9sxwmd0hdk11h6flkfz0123w0p3bj326")))

(define-public crate-holochain_test_wasm_common-0.2.5-rc.0 (c (n "holochain_test_wasm_common") (v "0.2.5-rc.0") (d (list (d (n "hdk") (r "^0.2.5-rc.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0, <=1.0.166") (d #t) (k 0)))) (h "18ld4r0wd5qr89ybnwk3ik6axwgpg7wjzxf1x8w2mk3zahmr243w")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.29 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.29") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.29") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "184fjimj2cwlkkrprgx6b6h7mg6ml9ijwr4b6wv6x5w8j98kvl3c")))

(define-public crate-holochain_test_wasm_common-0.1.6 (c (n "holochain_test_wasm_common") (v "0.1.6") (d (list (d (n "hdk") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1mdcg8g3kp40cx06wq7f9z6vqz9wcihmab4x72ym9wl42pqjq16q")))

(define-public crate-holochain_test_wasm_common-0.2.5 (c (n "holochain_test_wasm_common") (v "0.2.5") (d (list (d (n "hdk") (r "^0.2.5") (d #t) (k 0)) (d (n "serde") (r ">=1.0, <=1.0.166") (d #t) (k 0)))) (h "1hbacwg4sl256pbcx9yr9az2bk7k18xk62m962cxa6a158yz257x")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.30 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.30") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.30") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1rmgn3ncc2j8fjf3fhxp4b7ii5fmg3pn54x1ca1g601jd048bh9m")))

(define-public crate-holochain_test_wasm_common-0.2.6-rc.0 (c (n "holochain_test_wasm_common") (v "0.2.6-rc.0") (d (list (d (n "hdk") (r "^0.2.6-rc.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0, <=1.0.166") (d #t) (k 0)))) (h "1bxl6hl1qi4grs3jf5blbbcq2z7nx0d48h3q6gfz2krkgx9q883l")))

(define-public crate-holochain_test_wasm_common-0.2.6 (c (n "holochain_test_wasm_common") (v "0.2.6") (d (list (d (n "hdk") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r ">=1.0, <=1.0.166") (d #t) (k 0)))) (h "0iacs33v6q50vvn849scl9niipviql4dfakqhd8zzlz9nm560kkw")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.31 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.31") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.31") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1swrry0644wanr21cj4k2nn3jvyx8vx0vik5iancbpxb85zqcfd8")))

(define-public crate-holochain_test_wasm_common-0.2.7-rc.0 (c (n "holochain_test_wasm_common") (v "0.2.7-rc.0") (d (list (d (n "hdk") (r "^0.2.7-rc.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0, <=1.0.166") (d #t) (k 0)))) (h "0jbnlqfmip173x3n46hmjnqwvlwjsqq6gcd2l5m1qr43mkz2xrp9")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.32 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.32") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.32") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0p2avi426nsdhm4wryclbrns74sa8ijpbglsfs888zgkm1spcasm")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.33 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.33") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.33") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1xak5nbwxijfj5s200jbp8p8q5bcckjlwfzx1szs22kvrq77p3c3")))

(define-public crate-holochain_test_wasm_common-0.2.7-rc.1 (c (n "holochain_test_wasm_common") (v "0.2.7-rc.1") (d (list (d (n "hdk") (r "^0.2.7-rc.1") (d #t) (k 0)) (d (n "serde") (r ">=1.0, <=1.0.166") (d #t) (k 0)))) (h "0qi57fp1vmim2nm7ids8qvbr9i8kb4y9yblixrakn7rf71b88dvc")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.34 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.34") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.34") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0d2sw7xf4ff0sc6scmahbhaxhqvdni6y3ciyk4jxbjqvhc52p32x")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.35 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.35") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.35") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1vjxsiadsirn230y6h90yx654ny3ijg45nj4mh1s5665p3abxvdj")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.36 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.36") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.36") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0313qk534n5ms68ag31nfvzy87z0f1n9kybdb00c1ynz1jg49mj2")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.37 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.37") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.37") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0idfpvbf1b9fvk1s89m1gz2b2ld68j1g5gj9qjc4irp5bk5gi7q5")))

(define-public crate-holochain_test_wasm_common-0.2.7 (c (n "holochain_test_wasm_common") (v "0.2.7") (d (list (d (n "hdk") (r "^0.2.7") (d #t) (k 0)) (d (n "serde") (r ">=1.0, <=1.0.166") (d #t) (k 0)))) (h "1mv81a2jizpkgpwgkc3zhp70ylykk4ha9z1yii19pg307wif7gpr")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.38 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.38") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.38") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1q816cx9ah2hzgdxg7g0w3hnyxff1xkmak612nmshh0ffn42psaz")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.39 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.39") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.39") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0wpgzrm9wgsqmzsfx800fvbigkz2alr1dcvc352h3g0dc56hmrfr")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.40 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.40") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.40") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1rffain13z43phz7s3w7xi4wf6q987vnv9j4f87cca5cn9a4cxdg")))

(define-public crate-holochain_test_wasm_common-0.3.0-beta-dev.41 (c (n "holochain_test_wasm_common") (v "0.3.0-beta-dev.41") (d (list (d (n "hdk") (r "^0.3.0-beta-dev.41") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1sbgxwlp6173ccfpkcdgw2xajjk87j21f7bhpqj977r2ig42crzb")))

(define-public crate-holochain_test_wasm_common-0.2.8 (c (n "holochain_test_wasm_common") (v "0.2.8") (d (list (d (n "hdk") (r "^0.2.8") (d #t) (k 0)) (d (n "serde") (r ">=1.0, <=1.0.166") (d #t) (k 0)))) (h "0497q9dvb8n6nvn0p9pfjbbj1srf6jgcm9419v1bm3j16zvcn6sm")))

(define-public crate-holochain_test_wasm_common-0.4.0-dev.0 (c (n "holochain_test_wasm_common") (v "0.4.0-dev.0") (d (list (d (n "hdk") (r "^0.4.0-dev.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0fq6k5fbi90dc9kjsnhl89cvimhr1fjih76h6dk7lyqc21zm3k72")))

(define-public crate-holochain_test_wasm_common-0.4.0-dev.1 (c (n "holochain_test_wasm_common") (v "0.4.0-dev.1") (d (list (d (n "hdk") (r "^0.4.0-dev.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0nkc4q6y73j695azik68zvwfzmi8g0x90jlyivl5nan8cn8mkyhz")))

(define-public crate-holochain_test_wasm_common-0.4.0-dev.2 (c (n "holochain_test_wasm_common") (v "0.4.0-dev.2") (d (list (d (n "hdk") (r "^0.4.0-dev.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1zv9x7853nncwlg1nmmi77xqm3mq1j8rpr91glfkj687kp15g4c2")))

(define-public crate-holochain_test_wasm_common-0.4.0-dev.3 (c (n "holochain_test_wasm_common") (v "0.4.0-dev.3") (d (list (d (n "hdk") (r "^0.4.0-dev.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0b35rxwgw2ah90iicrqc514di7k8gknwxm5id30inw36qx2sd0kj")))

(define-public crate-holochain_test_wasm_common-0.3.1-rc.0 (c (n "holochain_test_wasm_common") (v "0.3.1-rc.0") (d (list (d (n "hdk") (r "^0.3.1-rc.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0qdnn1qw2ngd56bybfbimvwg8hzw9wr78v3ybii5wmlk6l9i47mj")))

(define-public crate-holochain_test_wasm_common-0.4.0-dev.4 (c (n "holochain_test_wasm_common") (v "0.4.0-dev.4") (d (list (d (n "hdk") (r "^0.4.0-dev.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0cdl7cia5xfr065kqzp44s46s5pm8440x4hcgbyqyxb2d553cz03")))

(define-public crate-holochain_test_wasm_common-0.4.0-dev.5 (c (n "holochain_test_wasm_common") (v "0.4.0-dev.5") (d (list (d (n "hdk") (r "^0.4.0-dev.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1kk6fbigvzllqlf5nbhi4hmjp8nj3krblx0x4l0zpimsddmz2idr")))

