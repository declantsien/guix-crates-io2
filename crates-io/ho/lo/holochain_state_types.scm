(define-module (crates-io ho lo holochain_state_types) #:use-module (crates-io))

(define-public crate-holochain_state_types-0.3.0-beta-dev.20 (c (n "holochain_state_types") (v "0.3.0-beta-dev.20") (d (list (d (n "holo_hash") (r "^0.3.0-beta-dev.10") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.3.0-beta-dev.13") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xr2i1w87fd97cpnsbxbfzcy35hg2745xc8wj1jdk4py96364d80")))

(define-public crate-holochain_state_types-0.3.0-beta-dev.21 (c (n "holochain_state_types") (v "0.3.0-beta-dev.21") (d (list (d (n "holo_hash") (r "^0.3.0-beta-dev.11") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.3.0-beta-dev.14") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ms5w23n48bj0647prqmaxh5kqmk133p8z24kmyj31j8ipcpzmix")))

(define-public crate-holochain_state_types-0.3.0-beta-dev.22 (c (n "holochain_state_types") (v "0.3.0-beta-dev.22") (d (list (d (n "holo_hash") (r "^0.3.0-beta-dev.12") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.3.0-beta-dev.15") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vjx972hrafi4nfvkz5ywwlhkwyw8d0yy5r2dzaf54dna7frr5wh")))

(define-public crate-holochain_state_types-0.3.0-beta-dev.23 (c (n "holochain_state_types") (v "0.3.0-beta-dev.23") (d (list (d (n "holo_hash") (r "^0.3.0-beta-dev.13") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.3.0-beta-dev.16") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bda4aapr35dcsd6qx1sil23xih89qqcjnzsg9733xgj4rf22xm8")))

(define-public crate-holochain_state_types-0.3.0-beta-dev.24 (c (n "holochain_state_types") (v "0.3.0-beta-dev.24") (d (list (d (n "holo_hash") (r "^0.3.0-beta-dev.14") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.3.0-beta-dev.17") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06vnk8hqk5f57qdkj87ppp89cqc4sg7jqmm3zsg65c4x55kgwxrs")))

(define-public crate-holochain_state_types-0.3.0-beta-dev.25 (c (n "holochain_state_types") (v "0.3.0-beta-dev.25") (d (list (d (n "holo_hash") (r "^0.3.0-beta-dev.15") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.3.0-beta-dev.18") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "168x14d3wcg4g4h15qyxlpwb889a2znnpjs44b01fx8r63645vwb")))

(define-public crate-holochain_state_types-0.3.0-beta-dev.26 (c (n "holochain_state_types") (v "0.3.0-beta-dev.26") (d (list (d (n "holo_hash") (r "^0.3.0-beta-dev.16") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.3.0-beta-dev.19") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "149kjni77269g9fqp31akdvkrs8adjmp62065ylb48662536p545")))

(define-public crate-holochain_state_types-0.3.0-beta-dev.27 (c (n "holochain_state_types") (v "0.3.0-beta-dev.27") (d (list (d (n "holo_hash") (r "^0.3.0-beta-dev.17") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.3.0-beta-dev.20") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wdv1zz8l7m1jc9f6shdncpf3xwrx05a4307r46z5ahcgda6cf20")))

(define-public crate-holochain_state_types-0.3.0-beta-dev.28 (c (n "holochain_state_types") (v "0.3.0-beta-dev.28") (d (list (d (n "holo_hash") (r "^0.3.0-beta-dev.18") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.3.0-beta-dev.21") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "17pcpyf7zbb53gp9j0syig1vy4fmyxsy2mzk60phldcn18abbp38")))

(define-public crate-holochain_state_types-0.3.0-beta-dev.29 (c (n "holochain_state_types") (v "0.3.0-beta-dev.29") (d (list (d (n "holo_hash") (r "^0.3.0-beta-dev.19") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.3.0-beta-dev.22") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k7ps3ypbhdw5w13ag1zx3zq77gcpkdzdqsxyv4x2bxv55rwy9va")))

(define-public crate-holochain_state_types-0.3.0-beta-dev.30 (c (n "holochain_state_types") (v "0.3.0-beta-dev.30") (d (list (d (n "holo_hash") (r "^0.3.0-beta-dev.19") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.3.0-beta-dev.23") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m31974zaxxr4vv0qsz33xav6gp2yr0ggc3qm38z60c4da76hzlv")))

(define-public crate-holochain_state_types-0.3.0-beta-dev.31 (c (n "holochain_state_types") (v "0.3.0-beta-dev.31") (d (list (d (n "holo_hash") (r "^0.3.0-beta-dev.20") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.3.0-beta-dev.24") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h13xkx5g5yvqrmmr5xax92b6nvy9klr3d9idsk2s0b8rfh5x4v5")))

(define-public crate-holochain_state_types-0.3.0-beta-dev.32 (c (n "holochain_state_types") (v "0.3.0-beta-dev.32") (d (list (d (n "holo_hash") (r "^0.3.0-beta-dev.21") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.3.0-beta-dev.25") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1b97snymil9fz4wzwcf1mi2x1x3l72q3lhv08q51qxhrbm9rsqrb")))

(define-public crate-holochain_state_types-0.3.0-beta-dev.33 (c (n "holochain_state_types") (v "0.3.0-beta-dev.33") (d (list (d (n "holo_hash") (r "^0.3.0-beta-dev.22") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.3.0-beta-dev.26") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y3b9jfhbqdy0b1b9nkfjy519l53zd0vn61szxqi5sk5jrk56dry")))

(define-public crate-holochain_state_types-0.3.0-beta-dev.34 (c (n "holochain_state_types") (v "0.3.0-beta-dev.34") (d (list (d (n "holo_hash") (r "^0.3.0-beta-dev.23") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.3.0-beta-dev.27") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kn0mhkfd9356xwg55x1yx9mv20a87vpf7bi9yixxwhzddjn4p02")))

(define-public crate-holochain_state_types-0.3.0-beta-dev.35 (c (n "holochain_state_types") (v "0.3.0-beta-dev.35") (d (list (d (n "holo_hash") (r "^0.3.0-beta-dev.24") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.3.0-beta-dev.28") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n3ss9l18qip7k46665dyai1ds8wph5a59fpjrrrmfwf0f3qmhcn")))

(define-public crate-holochain_state_types-0.3.0-beta-dev.36 (c (n "holochain_state_types") (v "0.3.0-beta-dev.36") (d (list (d (n "holo_hash") (r "^0.3.0-beta-dev.25") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.3.0-beta-dev.29") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1djq74bg1a98yncpjkd83s5x3z5mxm8xkr283k67mvjvknpcw8n9")))

(define-public crate-holochain_state_types-0.3.0-beta-dev.37 (c (n "holochain_state_types") (v "0.3.0-beta-dev.37") (d (list (d (n "holo_hash") (r "^0.3.0-beta-dev.26") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.3.0-beta-dev.30") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kc97098gcb2d4x2dxx6plpz5rlqijd0f2bai5pc32a6623bk6mf")))

(define-public crate-holochain_state_types-0.3.0-beta-dev.38 (c (n "holochain_state_types") (v "0.3.0-beta-dev.38") (d (list (d (n "holo_hash") (r "^0.3.0-beta-dev.26") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.3.0-beta-dev.31") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11krz37i4aqf9rsxrja0svj1bpp2h5yk3k5c61vqnal8kndiyj9g")))

(define-public crate-holochain_state_types-0.3.0-beta-dev.39 (c (n "holochain_state_types") (v "0.3.0-beta-dev.39") (d (list (d (n "holo_hash") (r "^0.3.0-beta-dev.27") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.3.0-beta-dev.32") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hxcgbcdpv4yj840rjp74qqd678b0p3njws2bxn93lh7zqw4jjz2")))

(define-public crate-holochain_state_types-0.3.0-beta-dev.40 (c (n "holochain_state_types") (v "0.3.0-beta-dev.40") (d (list (d (n "holo_hash") (r "^0.3.0-beta-dev.28") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.3.0-beta-dev.33") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04ljhzhp979ziiwbc37nzjblslgj645psxcqh72glw6fbqgd8pym")))

(define-public crate-holochain_state_types-0.4.0-dev.0 (c (n "holochain_state_types") (v "0.4.0-dev.0") (d (list (d (n "holo_hash") (r "^0.4.0-dev.0") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.4.0-dev.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gqijnbzpzmgnlnajwn36mdsg176ysyf3sk181kbbgri3llz2kap")))

(define-public crate-holochain_state_types-0.4.0-dev.1 (c (n "holochain_state_types") (v "0.4.0-dev.1") (d (list (d (n "holo_hash") (r "^0.4.0-dev.1") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.4.0-dev.1") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1i912l6a3c3qyrp9vzmj34q87isl7kzii4b7fmj99fm5da2i8r46")))

(define-public crate-holochain_state_types-0.4.0-dev.2 (c (n "holochain_state_types") (v "0.4.0-dev.2") (d (list (d (n "holo_hash") (r "^0.4.0-dev.2") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.4.0-dev.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fzzf7vaf5w4pplmygysb5y5jwhvlb6f6al497crjwbjpci2gcn3")))

(define-public crate-holochain_state_types-0.4.0-dev.3 (c (n "holochain_state_types") (v "0.4.0-dev.3") (d (list (d (n "holo_hash") (r "^0.4.0-dev.3") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.4.0-dev.3") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hc67aj3arf6mj73k87zh5sh9p5dc14dp0z18831pkrp90xxrqv2")))

(define-public crate-holochain_state_types-0.3.1-rc.0 (c (n "holochain_state_types") (v "0.3.1-rc.0") (d (list (d (n "holo_hash") (r "^0.3.1-rc.0") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.3.1-rc.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yc7iy2k8r2g4axk28pyzabw2fn8fvhqn6rxshzbj2zs0fljqfs7")))

(define-public crate-holochain_state_types-0.4.0-dev.4 (c (n "holochain_state_types") (v "0.4.0-dev.4") (d (list (d (n "holo_hash") (r "^0.4.0-dev.4") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.4.0-dev.4") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "061x939k3jd4fzgb3scjgcq6jrbqbkgmxcca7914wyxr1n52jyni")))

(define-public crate-holochain_state_types-0.4.0-dev.5 (c (n "holochain_state_types") (v "0.4.0-dev.5") (d (list (d (n "holo_hash") (r "^0.4.0-dev.5") (d #t) (k 0)) (d (n "holochain_integrity_types") (r "^0.4.0-dev.5") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lgv4y3d9fyvyzglj149af6v6xbq92qpknyp91vhr5qcx246dglw")))

