(define-module (crates-io ho lo holochain_entry_utils) #:use-module (crates-io))

(define-public crate-holochain_entry_utils-0.1.0 (c (n "holochain_entry_utils") (v "0.1.0") (d (list (d (n "hdk") (r "= 0.0.45-alpha1") (d #t) (k 0)) (d (n "holochain_json_derive") (r "= 0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "18f3y5alxjh0ksw36lpxxmdw7ll9s2bflrkmiinljvc0y74a2hc1")))

(define-public crate-holochain_entry_utils-0.1.1 (c (n "holochain_entry_utils") (v "0.1.1") (d (list (d (n "hdk") (r "= 0.0.46-alpha1") (d #t) (k 0)) (d (n "holochain_json_derive") (r "= 0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1jqi71ds7r7n1r7xpxn6d6q2c2gsyhzjgw1sxxil5gpvliz0vjbh")))

(define-public crate-holochain_entry_utils-0.1.2 (c (n "holochain_entry_utils") (v "0.1.2") (d (list (d (n "hdk") (r "= 0.0.47-alpha1") (d #t) (k 0)) (d (n "holochain_json_derive") (r "= 0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1smvx8hnlcvcgk5sacndj9iwww1za1jmb52hzxvi87pzxzqaafra")))

(define-public crate-holochain_entry_utils-0.1.3 (c (n "holochain_entry_utils") (v "0.1.3") (d (list (d (n "hdk") (r "= 0.0.48-alpha1") (d #t) (k 0)) (d (n "holochain_json_derive") (r "= 0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0pv8q9x09lykpwfak7wrq51xwqx6kbid16zkj1ygyjih9ddkybgx")))

(define-public crate-holochain_entry_utils-0.1.4 (c (n "holochain_entry_utils") (v "0.1.4") (d (list (d (n "hdk") (r "=0.0.49-alpha1") (d #t) (k 0)) (d (n "holochain_json_derive") (r "=0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "16vhrj278ydljq0rc0nwix9r8wlamdfxcsg4s8akq0bwwpdc4dwg")))

(define-public crate-holochain_entry_utils-0.1.5 (c (n "holochain_entry_utils") (v "0.1.5") (d (list (d (n "hdk") (r "=0.0.50-alpha4") (d #t) (k 0)) (d (n "holochain_json_derive") (r "=0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1bpb352cp0mq14sbzpni3dbvap8j4drd5kqzghyh5w7cipgjkzk8")))

(define-public crate-holochain_entry_utils-0.1.6 (c (n "holochain_entry_utils") (v "0.1.6") (d (list (d (n "hdk") (r "=0.0.51-alpha1") (d #t) (k 0)) (d (n "holochain_json_derive") (r "=0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0khfndmqpplwdd3azanfh1ai2r6n043hmahln41a4ny6cl186wms")))

