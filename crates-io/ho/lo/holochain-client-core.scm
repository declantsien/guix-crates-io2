(define-module (crates-io ho lo holochain-client-core) #:use-module (crates-io))

(define-public crate-holochain-client-core-0.1.0 (c (n "holochain-client-core") (v "0.1.0") (d (list (d (n "jsonrpc-core") (r "^14.0.5") (d #t) (k 0)) (d (n "jsonrpc-core-client") (r "^14.0.5") (f (quote ("ws"))) (d #t) (k 0)) (d (n "jsonrpc-derive") (r "^14.0.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (d #t) (k 0)) (d (n "url") (r "^1.7.0") (d #t) (k 0)))) (h "1kqqwgy06i402a97dqk8fyxadr7hj4rrmvca6mjmrx4rv9l7y148")))

