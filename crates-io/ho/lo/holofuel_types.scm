(define-module (crates-io ho lo holofuel_types) #:use-module (crates-io))

(define-public crate-holofuel_types-0.1.0 (c (n "holofuel_types") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "hdk") (r "=0.0.107") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wglnqnn35k9968cwdqmb8d3fhh7ls7inapdx5x08sjkbfz80pal")))

(define-public crate-holofuel_types-0.1.1 (c (n "holofuel_types") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "hdk") (r "=0.0.122") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zf7as3dr4grnpmwq0yf78jgml1l05rqaxdnncrw315234wnsbv2")))

(define-public crate-holofuel_types-0.2.0 (c (n "holofuel_types") (v "0.2.0") (d (list (d (n "holochain_deterministic_integrity") (r "=0.0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01w26b20xk3vw9pryzjgad1zxrx9jai3dzb7yik7illj5xljqjps")))

(define-public crate-holofuel_types-0.3.0 (c (n "holofuel_types") (v "0.3.0") (d (list (d (n "hdi") (r "=0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02n11mvzaw2g1gdipf5lq5vf6miy2rm7nlw4x16v04z77zbi053v")))

(define-public crate-holofuel_types-0.3.1 (c (n "holofuel_types") (v "0.3.1") (d (list (d (n "hdi") (r "=0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ffxkjga5qkkwmqxrkxglrbgfrm1pwdvqhs0fqmkm429p759xcsn")))

(define-public crate-holofuel_types-0.3.2 (c (n "holofuel_types") (v "0.3.2") (d (list (d (n "hdi") (r "=0.1.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02gw6mn2kwgjhxzhfsfg2qr16n4j43m500jv3428m69p8qb0mpn8")))

(define-public crate-holofuel_types-0.4.0 (c (n "holofuel_types") (v "0.4.0") (d (list (d (n "hdi") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bbwqf0wdqzl4lwpy08ny2lykv9dwphhw6ak0bk613nvqvpxfl8b")))

(define-public crate-holofuel_types-0.5.0 (c (n "holofuel_types") (v "0.5.0") (d (list (d (n "hdi") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bizszzn4fh5ly25fy6li4sj9ssr30d6cp4pqmzhivsihki1f44g")))

(define-public crate-holofuel_types-0.5.1 (c (n "holofuel_types") (v "0.5.1") (d (list (d (n "hdi") (r "=0.3.1-beta-dev.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bxm1ilihlssa3qzlfkm8ihijyznh2jbw2vhqsxc2sycz9rq05rm")))

(define-public crate-holofuel_types-0.5.2 (c (n "holofuel_types") (v "0.5.2") (d (list (d (n "hdi") (r "=0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05ka008yvqlsbmg4a0mj274ycjfgxmh08ccrq6awcsx1spvxkzkh")))

(define-public crate-holofuel_types-0.5.3 (c (n "holofuel_types") (v "0.5.3") (d (list (d (n "hdi") (r "^0.3.2-beta-rc.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wxiw7hfamskjmfkr63067cwfflsqcmwgqsn0nh6cvssh84gwvhp")))

(define-public crate-holofuel_types-0.5.4 (c (n "holofuel_types") (v "0.5.4") (d (list (d (n "hdi") (r "=0.3.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bcdz8clz2qhz4vv9vjjas21v566qxfrab31b4jbi6hrl01kdx2r")))

(define-public crate-holofuel_types-0.5.5 (c (n "holofuel_types") (v "0.5.5") (d (list (d (n "hdi") (r "=0.3.4-rc.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lidrbf3ws5j96bj6arz3s852p9g83ddpani6s23k8f4ryxg4cid")))

(define-public crate-holofuel_types-0.5.6 (c (n "holofuel_types") (v "0.5.6") (d (list (d (n "hdi") (r "=0.3.5-rc.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vx289xb15cnca27mqwq5c74mhg2g1jfgi4b6p4nyi9c4rjxfv74")))

(define-public crate-holofuel_types-0.5.7 (c (n "holofuel_types") (v "0.5.7") (d (list (d (n "hdi") (r "=0.4.0-beta-dev.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qla4nx32k0df1q7isjylnf1y2hcdhd2v04bw00ambnpgsx3l82n")))

(define-public crate-holofuel_types-0.5.8 (c (n "holofuel_types") (v "0.5.8") (d (list (d (n "hdi") (r "=0.4.0-beta-dev.36") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cph9cw9a6vsd2zqharn72dwxgl791drnjd1fr8bn4d2mfv4zfkx")))

(define-public crate-holofuel_types-0.5.9 (c (n "holofuel_types") (v "0.5.9") (d (list (d (n "hdi") (r "=0.5.0-dev.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08mpiqz73qam7xyvqfggw76583f0h9apj65790ayza31pxxqg4yi")))

