(define-module (crates-io ho lo holochain_anchors) #:use-module (crates-io))

(define-public crate-holochain_anchors-0.1.0 (c (n "holochain_anchors") (v "0.1.0") (d (list (d (n "hdk") (r "^0.0.39-alpha4") (d #t) (k 0)) (d (n "holochain_json_derive") (r "= 0.0.15") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0kg0c6pnygsb33ixnyhgg94xwl4bixilgx1vzq4zysj0l06l6hav")))

(define-public crate-holochain_anchors-0.1.1 (c (n "holochain_anchors") (v "0.1.1") (d (list (d (n "hdk") (r "^0.0.40-alpha1") (d #t) (k 0)) (d (n "holochain_json_derive") (r "= 0.0.15") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "014nns1pbpm63w1r2b2hlqhxcl1rvalf0808wlxw762p8v22vjb6")))

(define-public crate-holochain_anchors-0.2.0 (c (n "holochain_anchors") (v "0.2.0") (d (list (d (n "hdk") (r "^0.0.40-alpha1") (d #t) (k 0)) (d (n "holochain_json_derive") (r "= 0.0.15") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0iy8ifi4f368qpb4mhmlr3rsg7nikhcja6m8gv4iqf447h1xfl22")))

(define-public crate-holochain_anchors-0.2.1 (c (n "holochain_anchors") (v "0.2.1") (d (list (d (n "hdk") (r "^0.0.42-alpha3") (d #t) (k 0)) (d (n "holochain_json_derive") (r "= 0.0.15") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "00nywp3bgsgjp7m3qx4cx6dfzwx17s66yzi9d8izripb4680ixhv")))

