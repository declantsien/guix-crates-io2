(define-module (crates-io ho lo holochain_secure_primitive) #:use-module (crates-io))

(define-public crate-holochain_secure_primitive-0.3.0-beta-dev.20 (c (n "holochain_secure_primitive") (v "0.3.0-beta-dev.20") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "subtle") (r "^2") (d #t) (k 0)))) (h "1h3x810arkiglwa1cclfxq9wzbb4khdh59ik833ik0sz15l14a6l")))

(define-public crate-holochain_secure_primitive-0.3.0-beta-dev.21 (c (n "holochain_secure_primitive") (v "0.3.0-beta-dev.21") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "subtle") (r "^2") (d #t) (k 0)))) (h "0ln07asdnm1s9d7crxr3lvnhfdnicca4cmk4afg61i95lrmqbbn0")))

(define-public crate-holochain_secure_primitive-0.3.0-beta-dev.22 (c (n "holochain_secure_primitive") (v "0.3.0-beta-dev.22") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "subtle") (r "^2") (d #t) (k 0)))) (h "06phd98dgcdk3c8hjizj3pwh8rdjsy9qclyv8n16i63q3ylmphnw")))

(define-public crate-holochain_secure_primitive-0.3.0-beta-dev.23 (c (n "holochain_secure_primitive") (v "0.3.0-beta-dev.23") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "subtle") (r "^2") (d #t) (k 0)))) (h "0sjnxf1ia0bp2aplis8r8v921l8s17jh0bph1v6prk195kviqw57")))

(define-public crate-holochain_secure_primitive-0.3.0-dev.0 (c (n "holochain_secure_primitive") (v "0.3.0-dev.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "subtle") (r "^2") (d #t) (k 0)))) (h "0383vffin5373qfwsg7p2r2nxk65lzwq1smil1n1rygm94d6ah60")))

(define-public crate-holochain_secure_primitive-0.4.0-dev.1 (c (n "holochain_secure_primitive") (v "0.4.0-dev.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "subtle") (r "^2") (d #t) (k 0)))) (h "1ir6rxc6jh176lp6z95ccadkb3s5qkj6b6nn7x16gmzzisxcpjf9")))

(define-public crate-holochain_secure_primitive-0.3.1-rc.0 (c (n "holochain_secure_primitive") (v "0.3.1-rc.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "subtle") (r "^2") (d #t) (k 0)))) (h "0rk081lj5flgk0mld4jj7pwrq6ws53kk9bajkv7c9w6pj92h6xvq")))

