(define-module (crates-io ho lo holochain_wasmer_guest) #:use-module (crates-io))

(define-public crate-holochain_wasmer_guest-0.0.9 (c (n "holochain_wasmer_guest") (v "0.0.9") (d (list (d (n "holochain_wasmer_common") (r "= 0.0.9") (d #t) (k 0)))) (h "082si5vivgbcdcr2ncwdppirnn3lq6frnm9bbx227d3hk6h26n3j")))

(define-public crate-holochain_wasmer_guest-0.0.10 (c (n "holochain_wasmer_guest") (v "0.0.10") (d (list (d (n "holochain_wasmer_common") (r "= 0.0.10") (d #t) (k 0)))) (h "15j58xji2hi114qcdngsw5nyb04wz7pxgclmg5paajmifllm0k48")))

(define-public crate-holochain_wasmer_guest-0.0.11 (c (n "holochain_wasmer_guest") (v "0.0.11") (d (list (d (n "holochain_wasmer_common") (r "= 0.0.11") (d #t) (k 0)))) (h "1r3d7cr43n5ibljba6189jygmb96ma8p8vhrh5r562kbvszpms0k")))

(define-public crate-holochain_wasmer_guest-0.0.12 (c (n "holochain_wasmer_guest") (v "0.0.12") (d (list (d (n "holochain_wasmer_common") (r "= 0.0.12") (d #t) (k 0)))) (h "15xx7pv0bs812rybv2k5ndrkp2b0fhv7h1arr8hplrsgv4595y27")))

(define-public crate-holochain_wasmer_guest-0.0.13 (c (n "holochain_wasmer_guest") (v "0.0.13") (d (list (d (n "holochain_wasmer_common") (r "= 0.0.13") (d #t) (k 0)))) (h "0dl6hi49dbnq08fdaj4h5sxbwz8jn1dq2wpb1ak61bxdyxqc4gpk")))

(define-public crate-holochain_wasmer_guest-0.0.14 (c (n "holochain_wasmer_guest") (v "0.0.14") (d (list (d (n "holochain_wasmer_common") (r "= 0.0.14") (d #t) (k 0)))) (h "1mq2fv19ksi3pxh22kpzrr9dhqiizfq7hkgmc6c6slf2vc9jd1np")))

(define-public crate-holochain_wasmer_guest-0.0.15 (c (n "holochain_wasmer_guest") (v "0.0.15") (d (list (d (n "holochain_wasmer_common") (r "= 0.0.15") (d #t) (k 0)))) (h "1m8phlic01fxs07snpd41p598ngh4ck73jalz741di5qy6p9rznq")))

(define-public crate-holochain_wasmer_guest-0.0.17 (c (n "holochain_wasmer_guest") (v "0.0.17") (d (list (d (n "holochain_wasmer_common") (r "= 0.0.17") (d #t) (k 0)))) (h "1hj2bwvgb0zn3ix3z6s73h6d7z0ml5c3g9jzv4mlgs4ssvvjln6n")))

(define-public crate-holochain_wasmer_guest-0.0.18 (c (n "holochain_wasmer_guest") (v "0.0.18") (d (list (d (n "holochain_wasmer_common") (r "= 0.0.18") (d #t) (k 0)))) (h "003psb5hwdr2y209r07iljpqk79abzb7a5chgd53lqxpnwhjdrcd")))

(define-public crate-holochain_wasmer_guest-0.0.19 (c (n "holochain_wasmer_guest") (v "0.0.19") (d (list (d (n "holochain_serialized_bytes") (r "= 0.0.30") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "= 0.0.19") (d #t) (k 0)) (d (n "serde") (r "= 1.0.104") (d #t) (k 0)))) (h "0w1irjs792jnqmlb7j5w45ic6s4ycfn335qk98shwp18ii4f661p")))

(define-public crate-holochain_wasmer_guest-0.0.20 (c (n "holochain_wasmer_guest") (v "0.0.20") (d (list (d (n "holochain_serialized_bytes") (r "= 0.0.32") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "= 0.0.20") (d #t) (k 0)) (d (n "serde") (r "= 1.0.104") (d #t) (k 0)))) (h "1aq18xvh0y40rkl9x9bpg0gwwcgjkmvmal943nbbbx883dg0wxj1")))

(define-public crate-holochain_wasmer_guest-0.0.21 (c (n "holochain_wasmer_guest") (v "0.0.21") (d (list (d (n "holochain_serialized_bytes") (r "= 0.0.32") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "= 0.0.21") (d #t) (k 0)) (d (n "serde") (r "= 1.0.104") (d #t) (k 0)))) (h "0kw3g42zv0m0kja9mhyrb7fyzivg4psn7xj97jcj9xhkyf3ivcva")))

(define-public crate-holochain_wasmer_guest-0.0.22 (c (n "holochain_wasmer_guest") (v "0.0.22") (d (list (d (n "holochain_serialized_bytes") (r "= 0.0.36") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "= 0.0.22") (d #t) (k 0)) (d (n "serde") (r "= 1.0.104") (d #t) (k 0)))) (h "01l9zfyxjysfx44xi8pifxmb9qijxarlbagskpm4r5xbh46q7jiq")))

(define-public crate-holochain_wasmer_guest-0.0.24 (c (n "holochain_wasmer_guest") (v "0.0.24") (d (list (d (n "holochain_serialized_bytes") (r "= 0.0.36") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "= 0.0.24") (d #t) (k 0)) (d (n "serde") (r "= 1.0.104") (d #t) (k 0)))) (h "0z96qdgsc2p0c14nn3f30f52iza9wcqnf49i7gn667sfmdlab99y")))

(define-public crate-holochain_wasmer_guest-0.0.25 (c (n "holochain_wasmer_guest") (v "0.0.25") (d (list (d (n "holochain_serialized_bytes") (r "= 0.0.37") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "= 0.0.25") (d #t) (k 0)) (d (n "serde") (r "= 1.0.104") (d #t) (k 0)))) (h "19i62wgmgi73i1wp439zwf7679xm9cl09adwriak4vldhjzlv0pi")))

(define-public crate-holochain_wasmer_guest-0.0.26 (c (n "holochain_wasmer_guest") (v "0.0.26") (d (list (d (n "holochain_serialized_bytes") (r "^0.0.39") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "= 0.0.26") (d #t) (k 0)) (d (n "serde") (r "= 1.0.104") (d #t) (k 0)))) (h "0yplfd6ib8zrnh9zf929jzxv2cwi67dzsi0p4c0f8w4yxwwvd9gr")))

(define-public crate-holochain_wasmer_guest-0.0.27 (c (n "holochain_wasmer_guest") (v "0.0.27") (d (list (d (n "holochain_serialized_bytes") (r "^0.0.39") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "= 0.0.27") (d #t) (k 0)) (d (n "serde") (r "= 1.0.104") (d #t) (k 0)))) (h "0r0rnfq2ycnz9flp51ydh048ahl3b45x3wgawlkwp8950xhr8v15")))

(define-public crate-holochain_wasmer_guest-0.0.28 (c (n "holochain_wasmer_guest") (v "0.0.28") (d (list (d (n "holochain_serialized_bytes") (r "^0.0.39") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "= 0.0.28") (d #t) (k 0)) (d (n "serde") (r "= 1.0.104") (d #t) (k 0)))) (h "05flvfx7h4z0l0pn8xnaznvazf638mni0dyj1br132137shdi0wh")))

(define-public crate-holochain_wasmer_guest-0.0.29 (c (n "holochain_wasmer_guest") (v "0.0.29") (d (list (d (n "holochain_serialized_bytes") (r "^0.0.39") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "= 0.0.29") (d #t) (k 0)) (d (n "serde") (r "= 1.0.104") (d #t) (k 0)))) (h "0wm7lgldwdv5d632pgr60mlarfajx8f20xmfmawcg32q84ihig86")))

(define-public crate-holochain_wasmer_guest-0.0.30 (c (n "holochain_wasmer_guest") (v "0.0.30") (d (list (d (n "holochain_serialized_bytes") (r "^0.0.39") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "= 0.0.30") (d #t) (k 0)) (d (n "serde") (r "= 1.0.104") (d #t) (k 0)))) (h "1vw8facfvbnrfxr8ibxja4fxkyj6gz4mz1wk1sxykf7sbl468mis")))

(define-public crate-holochain_wasmer_guest-0.0.31 (c (n "holochain_wasmer_guest") (v "0.0.31") (d (list (d (n "holochain_serialized_bytes") (r "^0.0.39") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.31") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "12kafy07bvrlqdvkmar0v9a1njj76yh25kxsj8fgkhccz2sqvk22")))

(define-public crate-holochain_wasmer_guest-0.0.32 (c (n "holochain_wasmer_guest") (v "0.0.32") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.40") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.32") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "0fqzbyp0yf034x74rssm94bs25c5jqapy03b2dafq6aiqq4g1w6g")))

(define-public crate-holochain_wasmer_guest-0.0.33 (c (n "holochain_wasmer_guest") (v "0.0.33") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.40") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.33") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "08r07afwk9wl6s5337vqli3kyljmz6qvpvyl3dcvpv61s5p0hiyk")))

(define-public crate-holochain_wasmer_guest-0.0.34 (c (n "holochain_wasmer_guest") (v "0.0.34") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.40") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.34") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "0f3dj9fvhxppp0b0qgqq7pcdllxgz2h6jb13rq4r5xsbj1874zz1")))

(define-public crate-holochain_wasmer_guest-0.0.35 (c (n "holochain_wasmer_guest") (v "0.0.35") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.40") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.35") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "07n534s4441yv0adik3zrx2wkc4v08bq2yrbbm5wzdav3hwjh0rg")))

(define-public crate-holochain_wasmer_guest-0.0.36 (c (n "holochain_wasmer_guest") (v "0.0.36") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.40") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.36") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "094dk1fhdn1b4srz3n44qkijhnv8xnsldybxryirz47iifhnx9km")))

(define-public crate-holochain_wasmer_guest-0.0.37 (c (n "holochain_wasmer_guest") (v "0.0.37") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.40") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.37") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "10l6ps639iiprkkngm7sppnwpnais2ivw0n6ms3gi1w5vr2qglz1")))

(define-public crate-holochain_wasmer_guest-0.0.38 (c (n "holochain_wasmer_guest") (v "0.0.38") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.40") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.38") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "1y4pqkqzgplgvnn0z9n4zs406mmc2svk1bp9dkdlvy41vwxmyb9y")))

(define-public crate-holochain_wasmer_guest-0.0.39 (c (n "holochain_wasmer_guest") (v "0.0.39") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.40") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.39") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "1q7vm39yn4h5y2l9174cp09vwyqfz0i6rjl2a9lhhd4j65x0x4xq")))

(define-public crate-holochain_wasmer_guest-0.0.40 (c (n "holochain_wasmer_guest") (v "0.0.40") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.41") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.40") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "03q5s1klr3pj4axcl7lfs1cbix56g2y5lz0m5fjpjjx0n9aqm7kf")))

(define-public crate-holochain_wasmer_guest-0.0.41 (c (n "holochain_wasmer_guest") (v "0.0.41") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.42") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.41") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "0yfsihjl96fw6grbvma26145nhjk85mbs7cr3hs0dx1jm0gd3d13")))

(define-public crate-holochain_wasmer_guest-0.0.42 (c (n "holochain_wasmer_guest") (v "0.0.42") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.42") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.42") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "18n3g35la9fj0rdavq6v4ssh36i17kihrbzi5k2q6lzzlw7cfj7m")))

(define-public crate-holochain_wasmer_guest-0.0.43 (c (n "holochain_wasmer_guest") (v "0.0.43") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.42") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.43") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "0sk3bn3q49dzfik81704bc8w77b9w7pc0zilassbkl43ffa5i5v6")))

(define-public crate-holochain_wasmer_guest-0.0.44 (c (n "holochain_wasmer_guest") (v "0.0.44") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.42") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.44") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "1pld52rkkxkk06n8zpfzv9k8qpikny34sjzl44rhav8zmdg8x9jy")))

(define-public crate-holochain_wasmer_guest-0.0.45 (c (n "holochain_wasmer_guest") (v "0.0.45") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.43") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.45") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "1m6w8wac2k98c8z25if0zpk8fhmva9ij2xapfp3fkkh48w96ijy1")))

(define-public crate-holochain_wasmer_guest-0.0.46 (c (n "holochain_wasmer_guest") (v "0.0.46") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.45") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.46") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "1x6z7hna8brwdlg088xa4j814p6sr7w3dh7l8k233nkphkbalnyj")))

(define-public crate-holochain_wasmer_guest-0.0.47 (c (n "holochain_wasmer_guest") (v "0.0.47") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.45") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.47") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "0vcdj5yki6j3avb8gmbiw7lxcsx0wic9mcgvjkysyj4i6r4289q8")))

(define-public crate-holochain_wasmer_guest-0.0.48 (c (n "holochain_wasmer_guest") (v "0.0.48") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.45") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.48") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "17bpp93dk2q84vk5gz3cd6lx679js0km9jj8igrmj8zjis13i4l8")))

(define-public crate-holochain_wasmer_guest-0.0.49 (c (n "holochain_wasmer_guest") (v "0.0.49") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.45") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.49") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "1yrgqms6hw9f5pprd879k4qfxzsb1ma5vmmvb8bx0vaq75skh6j7")))

(define-public crate-holochain_wasmer_guest-0.0.50 (c (n "holochain_wasmer_guest") (v "0.0.50") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.45") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.50") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "06v4hppwkmsq3bwj0s9r3kaync68liw1knwlan1xhwrwlwj50jlj")))

(define-public crate-holochain_wasmer_guest-0.0.51 (c (n "holochain_wasmer_guest") (v "0.0.51") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.45") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.51") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "11symi2ah5y6mh18rq6llscdwnvx79qkfwl7g5snqbyl7r64sxzl")))

(define-public crate-holochain_wasmer_guest-0.0.52 (c (n "holochain_wasmer_guest") (v "0.0.52") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.46") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.52") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "00kzyjk34jwf5qq3mf4jl58nxrk0clqyyxv1m5pgiy023r9bz1kc")))

(define-public crate-holochain_wasmer_guest-0.0.53 (c (n "holochain_wasmer_guest") (v "0.0.53") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.47") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.53") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "006m3xsj8ciq09ij2wq62icbiijj82wn20clgss6v9dabpvh93zw")))

(define-public crate-holochain_wasmer_guest-0.0.54 (c (n "holochain_wasmer_guest") (v "0.0.54") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.47") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.54") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "1m1yjbzfkcz7k7xsz4qxhfrzghz0z6clg7j02bk17yxhpy6383fg")))

(define-public crate-holochain_wasmer_guest-0.0.64 (c (n "holochain_wasmer_guest") (v "0.0.64") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.47") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.64") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "01w1s3ygfs4wp6pq9s6xn3h6j9m93nrhjl4327kbc31vgcni4dyw")))

(define-public crate-holochain_wasmer_guest-0.0.65 (c (n "holochain_wasmer_guest") (v "0.0.65") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.48") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.65") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)))) (h "0m5pq9lyiqzx58ad30md5h72c90qlgz5lrb6c89p0pikdjdwcwgk")))

(define-public crate-holochain_wasmer_guest-0.0.66 (c (n "holochain_wasmer_guest") (v "0.0.66") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.48") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.66") (d #t) (k 0)) (d (n "serde") (r "=1.0.104") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1kwvdqk79lhvss3ww6pav9a7jh2shmdb51y4hz7kh35cz8jin1zz")))

(define-public crate-holochain_wasmer_guest-0.0.67 (c (n "holochain_wasmer_guest") (v "0.0.67") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.50") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.67") (d #t) (k 0)) (d (n "serde") (r "=1.0.123") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1cn0yn31pmnv1n1ysgw3904xkz78m98xw80wgc827vl07xmz3hm9")))

(define-public crate-holochain_wasmer_guest-0.0.71 (c (n "holochain_wasmer_guest") (v "0.0.71") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.50") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.71") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "serde") (r "=1.0.123") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0h3m78qjxhdxk2zr0ynbcjnrf05k184gf4dwjvv6rgpq5qcvgrk1")))

(define-public crate-holochain_wasmer_guest-0.0.72 (c (n "holochain_wasmer_guest") (v "0.0.72") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.50") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.72") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "serde") (r "=1.0.123") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1i44nhn0zmaxdjmmrwk28pil0wwizzn4mwjyqqhpknvgigpz52jy")))

(define-public crate-holochain_wasmer_guest-0.0.73 (c (n "holochain_wasmer_guest") (v "0.0.73") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.51") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.73") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "serde") (r "=1.0.123") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0s0qvkqm6nk2ysvn9mvqgns186l478zidn3k2jq107f9hpn2sp16")))

(define-public crate-holochain_wasmer_guest-0.0.76 (c (n "holochain_wasmer_guest") (v "0.0.76") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.51") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.76") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "serde") (r "=1.0.123") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0yi1p2qjj280kb500vpr08d4b6vyc2vm0m9isg5bgv99c4zl6pjg")))

(define-public crate-holochain_wasmer_guest-0.0.77 (c (n "holochain_wasmer_guest") (v "0.0.77") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.51") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.77") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "03m90sqrnq7zdygzq5clsy3xkim877mh9bmfq6hq3b32qskd5kdy")))

(define-public crate-holochain_wasmer_guest-0.0.78 (c (n "holochain_wasmer_guest") (v "0.0.78") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.51") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.78") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1zbjj623h6x0l79vb99x163bhg7a858ajghnf3w7bayh3gpb8zap")))

(define-public crate-holochain_wasmer_guest-0.0.79 (c (n "holochain_wasmer_guest") (v "0.0.79") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.51") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.79") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1ql8864cjhcinn74ppshqy2x0h8gjim1da3wd7yvf50vbh7s1x23")))

(define-public crate-holochain_wasmer_guest-0.0.80 (c (n "holochain_wasmer_guest") (v "0.0.80") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.51") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.80") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "19i59jpr0fiklgrh3lk7kczvpkj8m5ff5iwfi44a1mpm6vmv7q5f")))

(define-public crate-holochain_wasmer_guest-0.0.81 (c (n "holochain_wasmer_guest") (v "0.0.81") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.51") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.81") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0i29c4nnbk4bqg0mynfxbdbw9309nayk3wpbgq2c1af5gcjxn62q")))

(define-public crate-holochain_wasmer_guest-0.0.82 (c (n "holochain_wasmer_guest") (v "0.0.82") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.51") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.82") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1dp4p8341a0xhpdqfvciq45ijgn24infdjxyazqkb0ifal2h46b2")))

(define-public crate-holochain_wasmer_guest-0.0.83 (c (n "holochain_wasmer_guest") (v "0.0.83") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.51") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.83") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "test-fuzz") (r "=3.0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1nyp29npg2liqjpnxy2mizhk9vwqlcmq5lmvzv8gk8wi2yvrllzm")))

(define-public crate-holochain_wasmer_guest-0.0.84 (c (n "holochain_wasmer_guest") (v "0.0.84") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.51") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.84") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "test-fuzz") (r "=3.0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0hnf6ank0l9sjcr2k8iab02nd4vp4mi76ja611hv2p2r8ip05clj")))

(define-public crate-holochain_wasmer_guest-0.0.85 (c (n "holochain_wasmer_guest") (v "0.0.85") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.53") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.85") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "test-fuzz") (r "=3.0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1g5m2mph2y92aa8cvcsq2nlyq1l9q6kcrjgy7wwn2l18sfw61dcz")))

(define-public crate-holochain_wasmer_guest-0.0.86 (c (n "holochain_wasmer_guest") (v "0.0.86") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.53") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.86") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "test-fuzz") (r "=3.0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "08mapx4d0j67zcgl2bj1908zgpdxf4iqjg88klskc7br2f1c4a7s")))

(define-public crate-holochain_wasmer_guest-0.0.87 (c (n "holochain_wasmer_guest") (v "0.0.87") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.53") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.87") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "test-fuzz") (r "=3.0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "10xnih9rnm1nm1iry58f7ih0pwfh8llgkmjlm8fpbj7qzaa71g55")))

(define-public crate-holochain_wasmer_guest-0.0.88 (c (n "holochain_wasmer_guest") (v "0.0.88") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.53") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.88") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "test-fuzz") (r "=3.0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "16s36q84acn84sgj6x83b07n055pbcr238bpy8zvd6bya53z7q22")))

(define-public crate-holochain_wasmer_guest-0.0.89 (c (n "holochain_wasmer_guest") (v "0.0.89") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.53") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.89") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "test-fuzz") (r "=3.0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1l82wvzr9xdawzvcbn8c01db9pcsrf9qfxjh65dap1w65yim60nx")))

(define-public crate-holochain_wasmer_guest-0.0.90 (c (n "holochain_wasmer_guest") (v "0.0.90") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.53") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.90") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "test-fuzz") (r "=3.0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "00nax1qpi481sq38rln3ymddh479m1wm9hcbydrqlqrqm2pz5pn9")))

(define-public crate-holochain_wasmer_guest-0.0.91 (c (n "holochain_wasmer_guest") (v "0.0.91") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.53") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.91") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "test-fuzz") (r "=3.0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "06dp359lx6qf75rml3yh9rv1ixycsqryv9581rzq8hkzz7ff791h")))

(define-public crate-holochain_wasmer_guest-0.0.92 (c (n "holochain_wasmer_guest") (v "0.0.92") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.53") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.92") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "test-fuzz") (r "=3.0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0py8nixllznhyyzksvdsm2l7chqs1v8zx9j1fps4dr4yl629whlc")))

(define-public crate-holochain_wasmer_guest-0.0.93 (c (n "holochain_wasmer_guest") (v "0.0.93") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.54") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.93") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "test-fuzz") (r "=3.0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1l3dc7v064p0r4b9zxvyz7rvpg1qlki468rvl4i54p25gl6v3z0h")))

(define-public crate-holochain_wasmer_guest-0.0.94 (c (n "holochain_wasmer_guest") (v "0.0.94") (d (list (d (n "holochain_serialized_bytes") (r "=0.0.54") (d #t) (k 0)) (d (n "holochain_wasmer_common") (r "=0.0.94") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "test-fuzz") (r "=3.0.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1l4jwy41ka1vb8nf8l39qp3zshw4mlywziv6nlvcw5h7v8py34p2")))

