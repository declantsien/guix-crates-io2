(define-module (crates-io ho lo holochain_deepkey_dna) #:use-module (crates-io))

(define-public crate-holochain_deepkey_dna-0.0.1 (c (n "holochain_deepkey_dna") (v "0.0.1") (h "0qaymdb8bljvnxkn5p96ap017g2qjsqnksy2gdr7r1hzc2ksmfbp")))

(define-public crate-holochain_deepkey_dna-0.0.2 (c (n "holochain_deepkey_dna") (v "0.0.2") (h "01sdz5wmq3d7bzs8jxj3dg0rghgypki4hnf4n70z1bxjjmr4z9jk") (y #t)))

(define-public crate-holochain_deepkey_dna-0.0.3 (c (n "holochain_deepkey_dna") (v "0.0.3") (d (list (d (n "hc_deepkey_types") (r "^0.1.0") (d #t) (k 0)))) (h "0959nzcn3n0x773bqnxc3bvnr5ixwhxwpzisk3i85p63hmj1gym8") (y #t)))

(define-public crate-holochain_deepkey_dna-0.0.4 (c (n "holochain_deepkey_dna") (v "0.0.4") (d (list (d (n "hc_deepkey_sdk") (r "^0.2.0") (d #t) (k 0)))) (h "13gxnsh5vwhmsv4mkhkr6xw1vmc6ww9fi0drp8014inzp1r3rdj0") (f (quote (("fuzzing" "hc_deepkey_sdk/fuzzing"))))))

(define-public crate-holochain_deepkey_dna-0.0.5 (c (n "holochain_deepkey_dna") (v "0.0.5") (d (list (d (n "hc_deepkey_sdk") (r "^0.3.0") (d #t) (k 0)))) (h "1ccdibhlhsq3l2acip7z56yizq6yhmijwhp6z09jihi2710prn04") (f (quote (("fuzzing" "hc_deepkey_sdk/fuzzing"))))))

(define-public crate-holochain_deepkey_dna-0.0.6-dev.0 (c (n "holochain_deepkey_dna") (v "0.0.6-dev.0") (d (list (d (n "hc_deepkey_sdk") (r "^0.4.0-dev.0") (d #t) (k 0)))) (h "1iwn1ikaskcybfgy6w61ss021byh94ps4l9nd72jmrdbi2m7gvhr") (f (quote (("fuzzing" "hc_deepkey_sdk/fuzzing"))))))

(define-public crate-holochain_deepkey_dna-0.0.6-dev.1 (c (n "holochain_deepkey_dna") (v "0.0.6-dev.1") (d (list (d (n "hc_deepkey_sdk") (r "^0.5.0") (d #t) (k 0)))) (h "1a270s7f747b2ax56xckxha3dgrmaaq1a0fminaqd1210gzdfdav") (f (quote (("fuzzing" "hc_deepkey_sdk/fuzzing"))))))

(define-public crate-holochain_deepkey_dna-0.0.6-dev.2 (c (n "holochain_deepkey_dna") (v "0.0.6-dev.2") (d (list (d (n "hc_deepkey_sdk") (r "^0.6.0") (d #t) (k 0)))) (h "1b2zwp2gszzyp20d3njfbras6bsb3k7cr357dhw6w5hag6rd51cv") (f (quote (("fuzzing" "hc_deepkey_sdk/fuzzing"))))))

(define-public crate-holochain_deepkey_dna-0.0.6-dev.3 (c (n "holochain_deepkey_dna") (v "0.0.6-dev.3") (d (list (d (n "hc_deepkey_sdk") (r "^0.6.0") (d #t) (k 0)))) (h "12cbyqp08g7v8i00d6wfnz6qc4vx4v0cdlm0h11vc0fxzgglmpzd") (f (quote (("fuzzing" "hc_deepkey_sdk/fuzzing"))))))

(define-public crate-holochain_deepkey_dna-0.0.6-dev.4 (c (n "holochain_deepkey_dna") (v "0.0.6-dev.4") (d (list (d (n "hc_deepkey_sdk") (r "^0.6.0") (d #t) (k 0)))) (h "0wka1dj5gh951xjmq2fgg4ax5n5grbz7xnw9z8r4x3v9jpx35jkl") (f (quote (("fuzzing" "hc_deepkey_sdk/fuzzing"))))))

(define-public crate-holochain_deepkey_dna-0.0.6-dev.5 (c (n "holochain_deepkey_dna") (v "0.0.6-dev.5") (d (list (d (n "hc_deepkey_sdk") (r "^0.6.0") (d #t) (k 0)))) (h "0yqpq6y8k0jjml9jignd9m79xd7v28i8y6q9r8ngl42gmmiy990w") (f (quote (("fuzzing" "hc_deepkey_sdk/fuzzing"))))))

