(define-module (crates-io ho rs horse) #:use-module (crates-io))

(define-public crate-horse-0.1.0 (c (n "horse") (v "0.1.0") (h "14j00mqw8sikisvlpgrj41fayhk03vq313rcqw1i6lvljzr81ckq")))

(define-public crate-horse-0.1.1 (c (n "horse") (v "0.1.1") (h "0jjslsvra508n4b29sq5kym9zjq4zcsb3y02s68iyqqsgxzlmkcv")))

(define-public crate-horse-0.1.2 (c (n "horse") (v "0.1.2") (h "066z0ng95d1vnpp518q39cymsgb04dwdkyqjzgbdz6v1jbh83zpq")))

