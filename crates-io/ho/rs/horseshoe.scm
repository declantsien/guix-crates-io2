(define-module (crates-io ho rs horseshoe) #:use-module (crates-io))

(define-public crate-HorseShoe-0.1.0 (c (n "HorseShoe") (v "0.1.0") (d (list (d (n "num-complex") (r "^0.2.1") (k 0)) (d (n "num-traits") (r "^0.2.6") (k 0)))) (h "1pl1f3bnf31mkbishsp0k7ls8frdp17v5bkwbms84y63c8z5qk6x") (y #t)))

(define-public crate-HorseShoe-0.1.1 (c (n "HorseShoe") (v "0.1.1") (d (list (d (n "num-complex") (r "^0.2.1") (k 0)) (d (n "num-traits") (r "^0.2.6") (k 0)))) (h "0ml2p911jlw73lvxz5r3azvbcggvdx0nbavan2b6mbpa48ylr5wk") (y #t)))

(define-public crate-HorseShoe-0.1.2 (c (n "HorseShoe") (v "0.1.2") (d (list (d (n "num-complex") (r "^0.2.1") (k 0)) (d (n "num-traits") (r "^0.2.6") (k 0)))) (h "18mz9gm9j8nl1qhglby13wym4a31bp9m11zbw6xr6l8zfcsjjk8h") (y #t)))

(define-public crate-HorseShoe-0.0.1 (c (n "HorseShoe") (v "0.0.1") (d (list (d (n "num-complex") (r "^0.2.1") (k 0)) (d (n "num-traits") (r "^0.2.6") (k 0)))) (h "0xlhyri1svf4q3my7j3i7xgyys7a32yqnhmgh08011pwwv213v93")))

