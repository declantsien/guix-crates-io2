(define-module (crates-io ho ok hooks-macro) #:use-module (crates-io))

(define-public crate-hooks-macro-0.1.0 (c (n "hooks-macro") (v "0.1.0") (d (list (d (n "hooks-macro-core") (r "^0.1.0") (d #t) (k 0)))) (h "0hmgs8mk1d8zz285gg8qriz85877v13qzhfb3pn2l9azmr4hpjws")))

(define-public crate-hooks-macro-0.1.1 (c (n "hooks-macro") (v "0.1.1") (d (list (d (n "hooks-macro-core") (r "^0.2.0") (d #t) (k 0)))) (h "1w0h5fsa66bihlv8v8xbvfasrf79np9dh7vfqr6cc7pd3771cdbh")))

(define-public crate-hooks-macro-0.1.2 (c (n "hooks-macro") (v "0.1.2") (d (list (d (n "hooks-macro-core") (r "^0.3.0") (d #t) (k 0)))) (h "03mh38hc6kdsp5xl1rh0hbf5hx0a0972z0y8li4h2l4xi20mjy61")))

(define-public crate-hooks-macro-0.2.0 (c (n "hooks-macro") (v "0.2.0") (d (list (d (n "hooks-macro-core") (r "^0.4.0") (d #t) (k 0)))) (h "0gvr7g41vyag2qxb8af80667jnz71rzs0w6649cjvdhglicjgvxi")))

