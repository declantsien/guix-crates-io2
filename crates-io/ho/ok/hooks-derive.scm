(define-module (crates-io ho ok hooks-derive) #:use-module (crates-io))

(define-public crate-hooks-derive-1.0.0-alpha.1 (c (n "hooks-derive") (v "1.0.0-alpha.1") (h "1xnjmq2cmzx5nfir26zns3b08a1paxx6xvz4vv6arkgsgh87kj50")))

(define-public crate-hooks-derive-1.0.0-alpha.2 (c (n "hooks-derive") (v "1.0.0-alpha.2") (d (list (d (n "hooks-derive-core") (r "^1.0.0-alpha.2") (d #t) (k 0)))) (h "1dx3m0yx6y7k9rdrnv7r0v8m4fp1l7lx26ri854qbjgyxw7s60vc")))

(define-public crate-hooks-derive-1.0.0-alpha.3 (c (n "hooks-derive") (v "1.0.0-alpha.3") (d (list (d (n "hooks-derive-core") (r "^1.0.0-alpha.3") (d #t) (k 0)))) (h "1fjb86q2hkx8cq96bsq9iz5ngb85s5wxqb5w6m7x44hjppz5drqi")))

(define-public crate-hooks-derive-1.0.0-alpha.4 (c (n "hooks-derive") (v "1.0.0-alpha.4") (d (list (d (n "hooks-derive-core") (r "^1.0.0-alpha.4") (d #t) (k 0)))) (h "1ylvlhcwhcwrdxzg0p9pdaf1rlw1irk233kcqa2qw1inzfslklz1")))

(define-public crate-hooks-derive-1.0.0-alpha.5 (c (n "hooks-derive") (v "1.0.0-alpha.5") (d (list (d (n "hooks-derive-core") (r "^1.0.0-alpha.5") (d #t) (k 0)))) (h "1jzmkmi6wlvf97finbgsiywkzid4c2ivg3hp321zimlm248b1n5l")))

(define-public crate-hooks-derive-1.0.0-alpha.6 (c (n "hooks-derive") (v "1.0.0-alpha.6") (d (list (d (n "hooks-derive-core") (r "^1.0.0-alpha.6") (d #t) (k 0)))) (h "0wa8j52l7ciy025rsbi1ys6s9l44b52wkbj9g4q2jzss50xdkii4")))

(define-public crate-hooks-derive-1.0.0-alpha.7 (c (n "hooks-derive") (v "1.0.0-alpha.7") (d (list (d (n "hooks-derive-core") (r "^1.0.0-alpha.7") (d #t) (k 0)))) (h "1vyvg48ps0m4n6pyli5drmr4ivjzjj0xfmir6wk1k7ngrq66z0v7")))

(define-public crate-hooks-derive-1.0.0-alpha.8 (c (n "hooks-derive") (v "1.0.0-alpha.8") (d (list (d (n "hooks-derive-core") (r "^1.0.0-alpha.8") (d #t) (k 0)))) (h "07qkgph7a8aan1jl0ai1wd6wixnw5kzl1zpdkywbly0bqmgpibvc")))

(define-public crate-hooks-derive-1.0.1-alpha.8 (c (n "hooks-derive") (v "1.0.1-alpha.8") (d (list (d (n "hooks-derive-core") (r "^1.1.0-alpha.8") (d #t) (k 0)))) (h "1ivhk6icdsfh17dp7c6y2kambxzwr7n4zark16nym1wqkgkba437")))

