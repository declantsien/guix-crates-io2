(define-module (crates-io ho ok hooks-macro-core) #:use-module (crates-io))

(define-public crate-hooks-macro-core-0.1.0 (c (n "hooks-macro-core") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "1pqnh2790nvh4x110nk3myxiqjagn07lb2ak0wb2xwjmxpivbxxi") (f (quote (("extra-traits"))))))

(define-public crate-hooks-macro-core-0.2.0 (c (n "hooks-macro-core") (v "0.2.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "0zvh6g57dpj8rfh4kwgzl71xi6g8pgd222wpxrmdaa4xnl36kc9y") (f (quote (("extra-traits"))))))

(define-public crate-hooks-macro-core-0.3.0 (c (n "hooks-macro-core") (v "0.3.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "1c5vqkscwgvlfjr0gr0dwvkh6dhzxiwa5c80kw003l34gjbqq9gj") (f (quote (("extra-traits"))))))

(define-public crate-hooks-macro-core-0.4.0 (c (n "hooks-macro-core") (v "0.4.0") (d (list (d (n "darling") (r "^0.20.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "0zs6gis15l3xnsa91jb06bd5nj2avqr6zcqbzj5rcn8189igzz24") (f (quote (("extra-traits"))))))

