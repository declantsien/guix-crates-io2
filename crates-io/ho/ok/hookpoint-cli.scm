(define-module (crates-io ho ok hookpoint-cli) #:use-module (crates-io))

(define-public crate-hookpoint-cli-0.1.0 (c (n "hookpoint-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "11dddyh8gzqk61j03mq15ijn6mwi07c58kag77ijfhr1ikdfd0rq")))

