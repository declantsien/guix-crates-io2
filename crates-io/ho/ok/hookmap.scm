(define-module (crates-io ho ok hookmap) #:use-module (crates-io))

(define-public crate-hookmap-0.1.0 (c (n "hookmap") (v "0.1.0") (d (list (d (n "hookmap-core") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "1gg64m91r2qqprxy5a6aqk1wr849yxyxr4azwsrybk8d7y8flf5j") (f (quote (("block-input-event"))))))

(define-public crate-hookmap-0.1.1 (c (n "hookmap") (v "0.1.1") (d (list (d (n "hookmap-core") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "1b2h4lbijmxqcfppyff2c5fx7fbpvlrkjf2mwik0syyc4xnbqiq7") (f (quote (("block-input-event"))))))

(define-public crate-hookmap-0.1.2 (c (n "hookmap") (v "0.1.2") (d (list (d (n "hookmap-core") (r "^0.1.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "0qbj5v7fy8d5anzx9q8jp1criika7ya5iy6vlggkj4b9cpddyq9w") (f (quote (("block-input-event" "hookmap-core/block-input-event"))))))

(define-public crate-hookmap-0.2.0 (c (n "hookmap") (v "0.2.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "hookmap-core") (r "^0.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "1zxd8rnwkbfwb7wwiwqsgkynhw8s7h12kpyb38yrpnwnibx32hf8") (f (quote (("block-input-event" "hookmap-core/block-input-event"))))))

(define-public crate-hookmap-0.3.0 (c (n "hookmap") (v "0.3.0") (d (list (d (n "hookmap-core") (r "^0.1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "1d63f27w7r3yjlra05ig3k45pf5w27fz6ypq7bwa4psrb7d0s3c7") (f (quote (("block-input-event" "hookmap-core/block-input-event"))))))

(define-public crate-hookmap-0.3.1 (c (n "hookmap") (v "0.3.1") (d (list (d (n "hookmap-core") (r "^0.1.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "1zzy93z4i0ggdlxdi668b92kcwlw9y0jrbklbfc9v4pq07f7wzcb") (f (quote (("block-input-event" "hookmap-core/block-input-event"))))))

(define-public crate-hookmap-0.4.0 (c (n "hookmap") (v "0.4.0") (d (list (d (n "hookmap-core") (r "^0.1.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "0qh114k4jzcd3ww5aipz9s8b2rld2kis2ik9lxm34kqnhyw697br") (f (quote (("block-input-event" "hookmap-core/block-input-event"))))))

(define-public crate-hookmap-0.4.1 (c (n "hookmap") (v "0.4.1") (d (list (d (n "hookmap-core") (r "^0.1.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "smart-default") (r "^0.6.0") (d #t) (k 0)))) (h "0aiwvf5ljv4sspkwx58i06fjgp50jlnklkfqipwyyy06cldz98l4") (f (quote (("block-input-event" "hookmap-core/block-input-event"))))))

(define-public crate-hookmap-0.4.2 (c (n "hookmap") (v "0.4.2") (d (list (d (n "hookmap-core") (r "^0.1.8") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "smart-default") (r "^0.6.0") (d #t) (k 0)))) (h "1jsd5yj9fw9nqx2b0nmrvqnaxmpz07h7nlg4j4x8d9qayn8bm242") (f (quote (("block-input-event" "hookmap-core/block-input-event"))))))

(define-public crate-hookmap-0.4.3 (c (n "hookmap") (v "0.4.3") (d (list (d (n "hookmap-core") (r "^0.1.9") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "smart-default") (r "^0.6.0") (d #t) (k 0)))) (h "1ji5m3038s1lsjqnrb83kpxz0d2zpikcqdsknbh3d4wmswzs72fj") (f (quote (("us-keyboard-layout" "hookmap-core/us-keyboard-layout") ("japanese-keyboard-layout" "hookmap-core/japanese-keyboard-layout") ("default" "us-keyboard-layout") ("block-input-event" "hookmap-core/block-input-event"))))))

(define-public crate-hookmap-0.4.4 (c (n "hookmap") (v "0.4.4") (d (list (d (n "hookmap-core") (r "^0.1.10") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "smart-default") (r "^0.6.0") (d #t) (k 0)))) (h "03naicviz6nc4x7jfnvln2hc62k9298d0fi9hdsy57law3gq8afp") (f (quote (("us-keyboard-layout" "hookmap-core/us-keyboard-layout") ("japanese-keyboard-layout" "hookmap-core/japanese-keyboard-layout") ("default" "us-keyboard-layout") ("block-input-event" "hookmap-core/block-input-event"))))))

(define-public crate-hookmap-0.4.5 (c (n "hookmap") (v "0.4.5") (d (list (d (n "hookmap-core") (r "^0.1.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "smart-default") (r "^0.6.0") (d #t) (k 0)))) (h "034q38a9mw6jf8gnm2xil8qmjcby8idfrrfzb77dqijsxpc8ja2r") (f (quote (("us-keyboard-layout" "hookmap-core/us-keyboard-layout") ("japanese-keyboard-layout" "hookmap-core/japanese-keyboard-layout") ("default" "us-keyboard-layout") ("block-input-event" "hookmap-core/block-input-event"))))))

(define-public crate-hookmap-0.4.6 (c (n "hookmap") (v "0.4.6") (d (list (d (n "hookmap-core") (r "^0.1.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "typed-builder") (r "^0.9.1") (d #t) (k 0)))) (h "07j0fssfvcyasf4fnvmgs45s9sj02ps880xnp7j69aig1c8pfkv2") (f (quote (("us-keyboard-layout" "hookmap-core/us-keyboard-layout") ("japanese-keyboard-layout" "hookmap-core/japanese-keyboard-layout") ("default" "us-keyboard-layout") ("block-input-event" "hookmap-core/block-input-event"))))))

(define-public crate-hookmap-0.4.7 (c (n "hookmap") (v "0.4.7") (d (list (d (n "hookmap-core") (r "^0.1.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "typed-builder") (r "^0.9.1") (d #t) (k 0)))) (h "1ajhxianfqfwhanzkgpfvmw27rvxdj3m20lb9g8apbmpj88cfq0y") (f (quote (("us-keyboard-layout" "hookmap-core/us-keyboard-layout") ("japanese-keyboard-layout" "hookmap-core/japanese-keyboard-layout") ("default" "us-keyboard-layout") ("block-input-event" "hookmap-core/block-input-event"))))))

(define-public crate-hookmap-0.4.8 (c (n "hookmap") (v "0.4.8") (d (list (d (n "hookmap-core") (r "^0.1.12") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "0nvzh856835mi2y7mpzsh2zhg26y02px2qnnwxq1mjz13a4hpf7l") (f (quote (("us-keyboard-layout" "hookmap-core/us-keyboard-layout") ("japanese-keyboard-layout" "hookmap-core/japanese-keyboard-layout") ("default" "us-keyboard-layout") ("block-input-event" "hookmap-core/block-input-event"))))))

(define-public crate-hookmap-0.5.0 (c (n "hookmap") (v "0.5.0") (d (list (d (n "hookmap-core") (r "^0.2.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "090d9ax6lzh6zb1xn3khmf5s22s01s8g2aw1h8ifxwzb15k715x2") (f (quote (("us-keyboard-layout" "hookmap-core/us-keyboard-layout") ("japanese-keyboard-layout" "hookmap-core/japanese-keyboard-layout") ("default" "us-keyboard-layout"))))))

(define-public crate-hookmap-0.5.1 (c (n "hookmap") (v "0.5.1") (d (list (d (n "hookmap-core") (r "^0.2.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)))) (h "1p2vnsp72s6pz3ngdfrlb780iw5xz53lcsdsfkig7xhzsg6bi24s") (f (quote (("us-keyboard-layout" "hookmap-core/us-keyboard-layout") ("japanese-keyboard-layout" "hookmap-core/japanese-keyboard-layout") ("default" "us-keyboard-layout"))))))

