(define-module (crates-io ho ok hooklet) #:use-module (crates-io))

(define-public crate-hooklet-0.1.0 (c (n "hooklet") (v "0.1.0") (h "0pjjxx3dk77wvgy4zwzpyxbkfd1l75x9di26s3p0napg8qch0ypr")))

(define-public crate-hooklet-0.1.1 (c (n "hooklet") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "windows") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_System_Memory" "Win32_System_LibraryLoader"))) (d #t) (k 0)))) (h "0617v21v8fzygbm6w97w05g6hcv9a7y40plb5163ghldchlamfc3")))

(define-public crate-hooklet-0.1.2 (c (n "hooklet") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "windows") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_System_Memory" "Win32_System_LibraryLoader"))) (d #t) (k 0)))) (h "0gi67v0ijza0yki11im5awi469iiiw993syarklcbg2kjcvbwp0f")))

