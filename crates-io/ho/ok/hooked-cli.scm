(define-module (crates-io ho ok hooked-cli) #:use-module (crates-io))

(define-public crate-hooked-cli-0.1.0 (c (n "hooked-cli") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.5") (d #t) (k 2)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "hooked-config") (r "^0.1.0") (d #t) (k 0)) (d (n "insta") (r "^1.21.0") (d #t) (k 2)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.9") (d #t) (k 0)) (d (n "supports-color") (r "^1.3.0") (d #t) (k 0)) (d (n "tera") (r "^1.17.1") (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)))) (h "0mw201h3z7f2xbfwwqk0xvfibjgpj5h3c3s7md1ip6ih6ad783c2")))

