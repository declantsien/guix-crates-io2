(define-module (crates-io ho ok hooky-cli) #:use-module (crates-io))

(define-public crate-hooky-cli-0.1.0 (c (n "hooky-cli") (v "0.1.0") (h "05xsz620gj2539mvbkfsnbm4mdsmqldy4l5jagan7v8737rmnfpr")))

(define-public crate-hooky-cli-1.0.0 (c (n "hooky-cli") (v "1.0.0") (h "0wrdlmb1ra8lkvq96cfcgpwbzyk6min05vvh9v207iq77lh3xs96")))

(define-public crate-hooky-cli-1.0.0-1 (c (n "hooky-cli") (v "1.0.0-1") (h "0zzrlml1di12jxviw488q054ax09myfymq5yhk9ipvplpjh33nix")))

