(define-module (crates-io ho ok hooks-derive-core) #:use-module (crates-io))

(define-public crate-hooks-derive-core-1.0.0-alpha.2 (c (n "hooks-derive-core") (v "1.0.0-alpha.2") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "0jdc56lmfnq89c4yaxvckmpyqnhcbr98xzrzwr3ksjvcb7bf0i0r") (f (quote (("extra-traits"))))))

(define-public crate-hooks-derive-core-1.0.0-alpha.3 (c (n "hooks-derive-core") (v "1.0.0-alpha.3") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "12w0j2fkjwi39ngddv0clnkq8g8v9zmq7x2nkw6wfh2lmgih83cj") (f (quote (("extra-traits"))))))

(define-public crate-hooks-derive-core-1.0.0-alpha.4 (c (n "hooks-derive-core") (v "1.0.0-alpha.4") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "1sqdwymdp80qy0yymvcn4adcsallkhycbp2r41ac2nx6c9p5ffw8") (f (quote (("extra-traits"))))))

(define-public crate-hooks-derive-core-1.0.0-alpha.5 (c (n "hooks-derive-core") (v "1.0.0-alpha.5") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "1gaishk2fdr4mcxqbqzjbgz6df24b5kz0vl0vn67rxff0knjxniz") (f (quote (("extra-traits"))))))

(define-public crate-hooks-derive-core-1.0.0-alpha.6 (c (n "hooks-derive-core") (v "1.0.0-alpha.6") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "12cdnsgbhd3frc87jz6c0vmf8iq4q6p02b1ncy87gq58l0k5mgja") (f (quote (("extra-traits"))))))

(define-public crate-hooks-derive-core-1.0.0-alpha.7 (c (n "hooks-derive-core") (v "1.0.0-alpha.7") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "1xncbg0q099qw24wi2n6ihikqlldmkmi0a1jr201zyhxwc6bmzjk") (f (quote (("extra-traits"))))))

(define-public crate-hooks-derive-core-1.0.0-alpha.8 (c (n "hooks-derive-core") (v "1.0.0-alpha.8") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "0066gh2sn88czzq9fhac5y64ss563mpnsf5m6b3whfgrbhqmzcac") (f (quote (("extra-traits"))))))

(define-public crate-hooks-derive-core-1.1.0-alpha.8 (c (n "hooks-derive-core") (v "1.1.0-alpha.8") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "1wnra2f87k5iawrvlvnchsm040axsn1jqcb0ljzvnj3nzc40rxia") (f (quote (("extra-traits"))))))

