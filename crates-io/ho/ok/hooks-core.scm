(define-module (crates-io ho ok hooks-core) #:use-module (crates-io))

(define-public crate-hooks-core-1.0.0-alpha.1 (c (n "hooks-core") (v "1.0.0-alpha.1") (d (list (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)))) (h "00x380n60m2xgqnql71dcav1b2vg9p0g92z76zn8aax1p0d9c2mq")))

(define-public crate-hooks-core-1.0.0-alpha.2 (c (n "hooks-core") (v "1.0.0-alpha.2") (d (list (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)))) (h "098q4l1rsrq81vgbd23rqk0sh64qlr0ia30hgmbb9xn8rw2r9pvg")))

(define-public crate-hooks-core-1.0.0-alpha.3 (c (n "hooks-core") (v "1.0.0-alpha.3") (d (list (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)))) (h "1gfn3cfhnyd0f6sviivl2ryfyj757v8ry18f3fnsmxfap8k5h49f")))

(define-public crate-hooks-core-1.0.0-alpha.4 (c (n "hooks-core") (v "1.0.0-alpha.4") (d (list (d (n "futures-core") (r "^0.3.25") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)))) (h "04gpdl9sm3jgwmpaxfqlcfzp1mgaqrm3hnd82hzl98rhri6hwdly") (f (quote (("default" "futures-core"))))))

(define-public crate-hooks-core-1.0.0-alpha.5 (c (n "hooks-core") (v "1.0.0-alpha.5") (d (list (d (n "futures-core") (r "^0.3.25") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)))) (h "1rqykwy0793wzzzjxzk6jcaj47ga8f9q07sby3ys6w6qhjqga156") (f (quote (("default" "futures-core"))))))

(define-public crate-hooks-core-1.0.0-alpha.6 (c (n "hooks-core") (v "1.0.0-alpha.6") (d (list (d (n "futures-core") (r "^0.3.25") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)))) (h "1kycqbjxlrz4sl7q3dqhh5f3kkk9r7p7hnwq534xagn98953i9ik") (f (quote (("default" "futures-core"))))))

(define-public crate-hooks-core-1.0.0-alpha.7 (c (n "hooks-core") (v "1.0.0-alpha.7") (d (list (d (n "futures-core") (r "^0.3.25") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)))) (h "026yynrlk16l94bk0i573j8ykf1ap26msl8z0g7rds28nklmr059") (f (quote (("default" "futures-core"))))))

(define-public crate-hooks-core-1.0.0-alpha.8 (c (n "hooks-core") (v "1.0.0-alpha.8") (d (list (d (n "futures-core") (r "^0.3.25") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)))) (h "1sj5s76pb5vqcwy31r03wbr57mfq6ypzj31g376r0isj4pcmj4b5") (f (quote (("default" "futures-core"))))))

(define-public crate-hooks-core-1.0.0-alpha.9 (c (n "hooks-core") (v "1.0.0-alpha.9") (d (list (d (n "futures-core") (r "^0.3.25") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)))) (h "0pmgzhnx5d275473jc44s7vgf935dkgh80ri8rhiz8virc7kzkiq") (f (quote (("default" "futures-core"))))))

(define-public crate-hooks-core-1.0.0-alpha.10 (c (n "hooks-core") (v "1.0.0-alpha.10") (d (list (d (n "futures-core") (r "^0.3.25") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)))) (h "1xmmw79lkhjdi3rgm5r5xxc833m6skq0v0qpas0ljy09kj1x7780") (f (quote (("default" "futures-core"))))))

(define-public crate-hooks-core-2.0.0-alpha (c (n "hooks-core") (v "2.0.0-alpha") (d (list (d (n "futures-core") (r "^0.3.25") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)))) (h "0mi937bssw407gh4ndm7zqkvv3gndcsqyg8rh4afc27z22pxidld") (f (quote (("default" "futures-core"))))))

(define-public crate-hooks-core-2.0.1-alpha (c (n "hooks-core") (v "2.0.1-alpha") (d (list (d (n "futures-core") (r "^0.3.25") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)))) (h "1jskma9v9sq0zxvn89h7mrz1vqvsgly29iqlclg34wqnla5jdkhy") (f (quote (("default" "futures-core"))))))

(define-public crate-hooks-core-2.1.0-alpha (c (n "hooks-core") (v "2.1.0-alpha") (d (list (d (n "futures-core") (r "^0.3.25") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "syn-lite") (r "^0.1.0") (d #t) (k 0)))) (h "0l19cvawwh38a12q2qnhjahgx1ifha4cv872pfzkzk9rbnfc6q2l") (f (quote (("default" "futures-core"))))))

(define-public crate-hooks-core-2.2.0-alpha (c (n "hooks-core") (v "2.2.0-alpha") (d (list (d (n "futures-core") (r "^0.3.25") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "syn-lite") (r "^0.1.0") (d #t) (k 0)))) (h "1qkwgr4nbfl7h7nx065gf87llcxjavys42vdk9f6ylmq1fkqlg93") (f (quote (("default" "futures-core"))))))

(define-public crate-hooks-core-3.0.0-alpha.1 (c (n "hooks-core") (v "3.0.0-alpha.1") (d (list (d (n "futures-core") (r "^0.3.25") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "syn-lite") (r "^0.1.0") (d #t) (k 0)))) (h "127n0kll2gn46zchfppz6d2pdgp32d9lgrg8k1w8hc1m7zsbwn2h") (f (quote (("default" "futures-core"))))))

