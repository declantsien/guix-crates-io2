(define-module (crates-io ho ok hooky) #:use-module (crates-io))

(define-public crate-hooky-0.1.0 (c (n "hooky") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0fxq7r0xk5pga6fljc73c7q093w66k0l1frvxql8dgj3ivj3v633") (f (quote (("use_parking_lot" "parking_lot") ("default"))))))

(define-public crate-hooky-0.2.0 (c (n "hooky") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0930gc0zbfc3xy10hamy60f13p2m5rhkm71f2jibldlyll5hqhcm") (f (quote (("default"))))))

