(define-module (crates-io ho ok hookmap-core) #:use-module (crates-io))

(define-public crate-hookmap-core-0.1.0 (c (n "hookmap-core") (v "0.1.0") (d (list (d (n "bimap") (r "^0.6.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1p90ci0pk6ns8qvi6a6rqvs3fpaisbcc0sz69vc74h9ijk1xv09h")))

(define-public crate-hookmap-core-0.1.1 (c (n "hookmap-core") (v "0.1.1") (d (list (d (n "bimap") (r "^0.6.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1v06zriq01cm39y3bq9hwa4ylfzarn8ic941j5rfgcsfws20np81")))

(define-public crate-hookmap-core-0.1.2 (c (n "hookmap-core") (v "0.1.2") (d (list (d (n "bimap") (r "^0.6.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1hljrixn7jzjskzgwyhxh66jgfalh1knj76lbjdhsycb11hxjr46") (f (quote (("block-input-event"))))))

(define-public crate-hookmap-core-0.1.3 (c (n "hookmap-core") (v "0.1.3") (d (list (d (n "bimap") (r "^0.6.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0d3gm1w0nk9wwr4yd50mqaav960x3d3vf91wliymj8x493ab83ac") (f (quote (("block-input-event"))))))

(define-public crate-hookmap-core-0.1.4 (c (n "hookmap-core") (v "0.1.4") (d (list (d (n "bimap") (r "^0.6.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1dh9gbrbq9wxx4381x9vjsm1kvjj7ic370vfyhjq3hwzcsg1apli") (f (quote (("block-input-event"))))))

(define-public crate-hookmap-core-0.1.5 (c (n "hookmap-core") (v "0.1.5") (d (list (d (n "bimap") (r "^0.6.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1pkmi55v3q1zr9wmnz3gyrak22faha78ljlampmgxipiy89aw048") (f (quote (("block-input-event"))))))

(define-public crate-hookmap-core-0.1.6 (c (n "hookmap-core") (v "0.1.6") (d (list (d (n "bimap") (r "^0.6.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0bx99rw0zzg49f74xvbcv2jcswcid3ssh976yk9ndzmlp882y0hf") (f (quote (("block-input-event"))))))

(define-public crate-hookmap-core-0.1.7 (c (n "hookmap-core") (v "0.1.7") (d (list (d (n "bimap") (r "^0.6.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "01cq7a6h3iph58nih3yr100w35faqdc1cx4waylfz3bxb11mmibf") (f (quote (("block-input-event"))))))

(define-public crate-hookmap-core-0.1.8 (c (n "hookmap-core") (v "0.1.8") (d (list (d (n "bimap") (r "^0.6.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1pam1ra821yic3xcm77dyd6b5k1csfibgqsvqmy5wjqx23imcdwj") (f (quote (("block-input-event"))))))

(define-public crate-hookmap-core-0.1.9 (c (n "hookmap-core") (v "0.1.9") (d (list (d (n "bimap") (r "^0.6.1") (d #t) (k 0)) (d (n "bitmaps") (r "^3.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "variant_count") (r "^1.1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0n3clrq6kcil5hk46dbbm0n42qa6wlmnaizfgm7d16mm83mn3zjs") (f (quote (("us-keyboard-layout") ("japanese-keyboard-layout") ("block-input-event"))))))

(define-public crate-hookmap-core-0.1.10 (c (n "hookmap-core") (v "0.1.10") (d (list (d (n "bimap") (r "^0.6.1") (d #t) (k 0)) (d (n "bitmaps") (r "^3.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "variant_count") (r "^1.1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0q393rv7hwv2qr9pssxw920krs6wrywicaw2r4pcy4rcnpap37d2") (f (quote (("us-keyboard-layout") ("japanese-keyboard-layout") ("block-input-event"))))))

(define-public crate-hookmap-core-0.1.11 (c (n "hookmap-core") (v "0.1.11") (d (list (d (n "bimap") (r "^0.6.1") (d #t) (k 0)) (d (n "bitmaps") (r "^3.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "variant_count") (r "^1.1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0hazwp4xz6bk18grccn5p68lva6ppy0rnknw0n50m109ir2iif0c") (f (quote (("us-keyboard-layout") ("japanese-keyboard-layout") ("block-input-event"))))))

(define-public crate-hookmap-core-0.1.12 (c (n "hookmap-core") (v "0.1.12") (d (list (d (n "bimap") (r "^0.6.1") (d #t) (k 0)) (d (n "bitmaps") (r "^3.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "variant_count") (r "^1.1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0vfl4j2n5a6033i46w0zcpbgapx0yhm833bcccwq729a2yaawi45") (f (quote (("us-keyboard-layout") ("japanese-keyboard-layout") ("block-input-event"))))))

(define-public crate-hookmap-core-0.2.0 (c (n "hookmap-core") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "variant_count") (r "^1.1.0") (d #t) (k 0)) (d (n "windows") (r "^0.36.1") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging" "Win32_UI_HiDpi" "Win32_UI_Input_KeyboardAndMouse"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0ppq66kvbgzw0v5k2nmwqdihcyh0lyj6mhdzkwcm8q6g1f6vpwkg") (f (quote (("us-keyboard-layout") ("japanese-keyboard-layout"))))))

(define-public crate-hookmap-core-0.2.1 (c (n "hookmap-core") (v "0.2.1") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "variant_count") (r "^1.1.0") (d #t) (k 0)) (d (n "windows") (r "^0.36.1") (f (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging" "Win32_UI_HiDpi" "Win32_UI_Input_KeyboardAndMouse"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0gn86m2wzfnl66vj45k24xkc4rs0xiw1rb9a9vflhkpj42q6p4m3") (f (quote (("us-keyboard-layout") ("japanese-keyboard-layout"))))))

