(define-module (crates-io ho ok hooks-yew) #:use-module (crates-io))

(define-public crate-hooks-yew-1.0.0-alpha.1 (c (n "hooks-yew") (v "1.0.0-alpha.1") (d (list (d (n "hooks") (r "^1.0.0-alpha.15") (f (quote ("derive"))) (k 0)) (d (n "hooks-yew-derive") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "1wywfs9g8ny5lmfqyiyjqk87hzc8vndccwryy8hqdgncrq62gjq8")))

(define-public crate-hooks-yew-1.0.0-alpha.2 (c (n "hooks-yew") (v "1.0.0-alpha.2") (d (list (d (n "hooks") (r "^1.0.0-alpha.16") (f (quote ("derive"))) (k 0)) (d (n "hooks-yew-derive") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "yew") (r "^0.19") (d #t) (k 0)))) (h "04b65mysbvc08licf7sq31bkp3hdf1zcimp7wqx0w3rlygwjdyry")))

