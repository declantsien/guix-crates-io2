(define-module (crates-io ho ok hooked-config) #:use-module (crates-io))

(define-public crate-hooked-config-0.1.0 (c (n "hooked-config") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "insta") (r "^1.21.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1j0k5wc8lfp561xbi2bydlxhi1afhfcb3bfmhwx88clxslxg8f5n")))

