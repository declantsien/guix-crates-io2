(define-module (crates-io ho ok hooker) #:use-module (crates-io))

(define-public crate-hooker-0.1.0 (c (n "hooker") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.4") (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)) (d (n "zydis-sys") (r "^0.1.1") (d #t) (k 0)))) (h "18z49z95mwkcd8z57fww4aba156lxnf6lcbkffx14bp2bbc6ki6q") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-hooker-0.1.1 (c (n "hooker") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.7.4") (k 0)) (d (n "region") (r "^3.0.1") (d #t) (k 2)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)) (d (n "zydis-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1fkv1fqd73ypcdzwxh0np7qb2yyxhbqv4ixfybzfqb4fn6wzalp4") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-hooker-0.1.2 (c (n "hooker") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.7.4") (k 0)) (d (n "region") (r "^3.0.1") (d #t) (k 2)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)) (d (n "zydis-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1vjha8gd4qnhrcvqpl1vcpfzhrhcjchqaiq0qfym98ca1s2bmkf2") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-hooker-0.1.3 (c (n "hooker") (v "0.1.3") (d (list (d (n "arrayvec") (r "^0.7.4") (k 0)) (d (n "region") (r "^3.0.1") (d #t) (k 2)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)) (d (n "zydis-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0prnvhyn3kj9x2aksjqbdl2rnb95aslffmc0yiimdga9883cy985") (f (quote (("std" "thiserror-no-std/std" "arrayvec/std"))))))

(define-public crate-hooker-0.1.4 (c (n "hooker") (v "0.1.4") (d (list (d (n "arrayvec") (r "^0.7.4") (k 0)) (d (n "region") (r "^3.0.1") (d #t) (k 2)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)) (d (n "zydis-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1dc3bf16vg2gfiy2f470wk1p22p8hykhxzi68i6sjv01h3v5s2ap") (f (quote (("std" "thiserror-no-std/std" "arrayvec/std"))))))

(define-public crate-hooker-0.1.5 (c (n "hooker") (v "0.1.5") (d (list (d (n "arrayvec") (r "^0.7.4") (k 0)) (d (n "region") (r "^3.0.1") (d #t) (k 2)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)) (d (n "zydis-sys") (r "^0.1.1") (d #t) (k 0)))) (h "06l9g4sghdkspk0bcasp6ch1ydn0fcxgljcmlrsh5n167i1z037r") (f (quote (("std" "thiserror-no-std/std" "arrayvec/std"))))))

(define-public crate-hooker-0.1.6 (c (n "hooker") (v "0.1.6") (d (list (d (n "arrayvec") (r "^0.7.4") (k 0)) (d (n "region") (r "^3.0.1") (d #t) (k 2)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)) (d (n "zydis-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1qnhmpfbwx0mn482i6z3r7dcv456zv7y1z7m6jq8j90ym67r8ngp") (f (quote (("std" "thiserror-no-std/std" "arrayvec/std"))))))

(define-public crate-hooker-0.1.7 (c (n "hooker") (v "0.1.7") (d (list (d (n "arrayvec") (r "^0.7.4") (k 0)) (d (n "region") (r "^3.0.1") (d #t) (k 2)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)) (d (n "zydis-sys") (r "^0.1.2") (d #t) (k 0)))) (h "04z87i211p9vvm7yp8xfw2cak1q6bkfilpdxhwdgpm1s4bq4k0cg") (f (quote (("std" "thiserror-no-std/std" "arrayvec/std"))))))

(define-public crate-hooker-0.1.8 (c (n "hooker") (v "0.1.8") (d (list (d (n "arrayvec") (r "^0.7.4") (k 0)) (d (n "region") (r "^3.0.1") (d #t) (k 2)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)) (d (n "zydis-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1d23widzplv96zzd5wlgr1mx635bakn938hwfcm2z4kqdm4rk9h3") (f (quote (("std" "thiserror-no-std/std" "arrayvec/std"))))))

(define-public crate-hooker-0.1.9 (c (n "hooker") (v "0.1.9") (d (list (d (n "arrayvec") (r "^0.7.4") (k 0)) (d (n "region") (r "^3.0.1") (d #t) (k 2)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)) (d (n "zydis-sys") (r "^0.1.2") (d #t) (k 0)))) (h "159lpi80gkxy81cl5yfvibii4avhzcqshj54lbccjzqyfzcgh933") (f (quote (("std" "thiserror-no-std/std" "arrayvec/std"))))))

