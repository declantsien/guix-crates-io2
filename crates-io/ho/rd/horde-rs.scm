(define-module (crates-io ho rd horde-rs) #:use-module (crates-io))

(define-public crate-horde-rs-0.1.0 (c (n "horde-rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (d #t) (k 0)) (d (n "rocket") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "serde_with") (r "^2.2.0") (d #t) (k 0)))) (h "1yill9ca58iipxxyv9zna07xhh6xygw2hjplbwdyykdsw7hk6zlq")))

