(define-module (crates-io ho rd horde) #:use-module (crates-io))

(define-public crate-horde-0.0.1 (c (n "horde") (v "0.0.1") (h "0f7l4p9aa38spjmgmam53q7xjdfsligrmw9732w5x7x05ii7q10r")))

(define-public crate-horde-0.1.1 (c (n "horde") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "0qk0vwpd9k82hhz5r6gamrgzbrnr1hcs7789qp5kzjmahacdjgma")))

