(define-module (crates-io ho rd horde3d-sys) #:use-module (crates-io))

(define-public crate-horde3d-sys-0.1.0 (c (n "horde3d-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "15jqixzs3bh13z7zbcxy227yxghci4245yf67k8da5m4x5bh340l")))

(define-public crate-horde3d-sys-0.1.1 (c (n "horde3d-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1bybw7dhk81v5v9byykqsbrh8x651c27byr5agjjj5p06ggp4py8")))

