(define-module (crates-io ho do hodoku) #:use-module (crates-io))

(define-public crate-hodoku-0.1.0 (c (n "hodoku") (v "0.1.0") (h "0fq8vv0n8yyvy4lyq7a7cw945kyjcxqigjm8k7973z66hhiafqwn") (y #t)))

(define-public crate-hodoku-0.1.1 (c (n "hodoku") (v "0.1.1") (h "028ps7sr70nmlq5m4gfmmayzcbirv2gww8yi991bwfq7glzaqcx3") (y #t)))

(define-public crate-hodoku-0.1.2 (c (n "hodoku") (v "0.1.2") (h "047qwr7vwjl81pcvih48agm8c7h81xd8k74kayqsxwvc8rb30xf9")))

(define-public crate-hodoku-0.1.3 (c (n "hodoku") (v "0.1.3") (h "1b1hdvjjipylpa5wphrwpnrvjdzxjxpqh4m0qrq5cznxg6jw2sjz")))

(define-public crate-hodoku-0.1.4 (c (n "hodoku") (v "0.1.4") (h "123k11qnqf9r8jb97gnnd4s61zha3isy00ip3zpzfrdasr79jbfc")))

(define-public crate-hodoku-0.1.5 (c (n "hodoku") (v "0.1.5") (h "186g9vx1rbz5synd4vvv6kk6vl6qyqgn49zycrphmpc7vcr57w3d") (r "1.56")))

