(define-module (crates-io ho do hodor-program) #:use-module (crates-io))

(define-public crate-hodor-program-0.1.0 (c (n "hodor-program") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "solana-program") (r "^1.10.26") (d #t) (k 0)) (d (n "spl-token") (r "^3.3.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0nz8spdd8y08rbpj0mqzihics34lgqzpnvws9i73l75vww2pqnz6") (f (quote (("no-entrypoint"))))))

