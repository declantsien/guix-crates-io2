(define-module (crates-io ho ra horaedbproto) #:use-module (crates-io))

(define-public crate-horaedbproto-1.0.23 (c (n "horaedbproto") (v "1.0.23") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0shc8ds9b6faq2wbc5wlr9gg5hy4am7rxkb8i37gc41zdwvw423v")))

(define-public crate-horaedbproto-1.0.24 (c (n "horaedbproto") (v "1.0.24") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "1mzs1ljxlbxv1h4wng22i1wgpiy4l8f38l10rxw8k090xrqcf1sr")))

(define-public crate-horaedbproto-2.0.0 (c (n "horaedbproto") (v "2.0.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0x9j2k7rnmz1vnr58f32m7hm3l64m41h3dkl2kmrwmmw6ya2cn1c")))

