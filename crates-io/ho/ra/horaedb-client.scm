(define-module (crates-io ho ra horaedb-client) #:use-module (crates-io))

(define-public crate-horaedb-client-1.0.2 (c (n "horaedb-client") (v "1.0.2") (d (list (d (n "arrow") (r "^38.0.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.72") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dashmap") (r "^5.3.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "horaedbproto") (r "^1.0.23") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.29") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("full"))) (d #t) (k 2)) (d (n "tonic") (r "^0.8.1") (d #t) (k 0)) (d (n "zstd") (r "^0.12") (k 0)))) (h "1cxk8sp7cq4cwx3fim80gyg1bw2jcs1l5a3q2arrapgww7qm1f3v")))

