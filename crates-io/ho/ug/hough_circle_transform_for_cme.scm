(define-module (crates-io ho ug hough_circle_transform_for_cme) #:use-module (crates-io))

(define-public crate-hough_circle_transform_for_cme-0.1.0 (c (n "hough_circle_transform_for_cme") (v "0.1.0") (d (list (d (n "image") (r "^0.23.10") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.6.0") (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "06ffn7p4fz4ygrx9fq7xwxhw8awzw82nacng58hhbnncivf0blcv")))

(define-public crate-hough_circle_transform_for_cme-0.1.1 (c (n "hough_circle_transform_for_cme") (v "0.1.1") (d (list (d (n "image") (r "^0.23.10") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.6.0") (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "02z7lcg7pyi6ylzkhmpcxj5fkn28s9ysnqflwzziqhkq31hx0c1s")))

(define-public crate-hough_circle_transform_for_cme-0.1.2 (c (n "hough_circle_transform_for_cme") (v "0.1.2") (d (list (d (n "image") (r "^0.23.10") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.6.0") (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "1pwyvf4728hf8adzjvks8d0iffdi335kz433fqb7i3vwsr60j2wh")))

(define-public crate-hough_circle_transform_for_cme-0.1.3 (c (n "hough_circle_transform_for_cme") (v "0.1.3") (d (list (d (n "image") (r "^0.23.10") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "ndarray-npy") (r "^0.6.0") (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "11wqw09d04n520dn5xdn3vkaq3hd306h1qlhimyvdbsydwwczxcb")))

