(define-module (crates-io ho gw hogwild) #:use-module (crates-io))

(define-public crate-hogwild-0.1.0 (c (n "hogwild") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.11") (d #t) (k 0)))) (h "1a8s7qxflfixscg1c6sy63b1235h04xcwlj6yp3h46j5rkcllya2")))

(define-public crate-hogwild-0.2.0 (c (n "hogwild") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.11") (d #t) (k 0)))) (h "0yh63lnjj1ch41f99xrnrc944k8hy15n756bmsv2khwzsk0p5x2f")))

(define-public crate-hogwild-0.3.0 (c (n "hogwild") (v "0.3.0") (d (list (d (n "ndarray") (r "^0.12") (d #t) (k 0)))) (h "1dwvrhnl6k369jk50infv9qf501427y3s8lyb3nakwnv4xbxjx8c")))

(define-public crate-hogwild-0.4.0 (c (n "hogwild") (v "0.4.0") (d (list (d (n "ndarray") (r "^0.12") (d #t) (k 0)))) (h "102rmg1r9gc0ifcf9zkn4xrb1f7px4ca16lajhkk3ra590v1xss1")))

(define-public crate-hogwild-0.4.1 (c (n "hogwild") (v "0.4.1") (d (list (d (n "ndarray") (r "^0.12") (d #t) (k 0)))) (h "0z9bvk3g9h1rj9xryw63zh04l7yy0wyl5y42gg4mn1k5bsasippd")))

(define-public crate-hogwild-0.5.0 (c (n "hogwild") (v "0.5.0") (d (list (d (n "ndarray") (r "^0.12") (d #t) (k 0)))) (h "15kj8alkxm59akicl5ikkq1m7s1zb77m59gah5b9f3vgds03idpd")))

(define-public crate-hogwild-0.6.0 (c (n "hogwild") (v "0.6.0") (d (list (d (n "ndarray") (r "^0.12") (d #t) (k 0)))) (h "09wbh7akn0am2lg2lggfr2bwbd3pv0im9k2ym698jmnmyaggfgv0")))

(define-public crate-hogwild-0.6.1 (c (n "hogwild") (v "0.6.1") (d (list (d (n "ndarray") (r "^0.12") (d #t) (k 0)))) (h "1vfr95cz8iidzsbgpx3k1ix1kpla4bgjk6dpc3vgbzsxwd3i0rha")))

