(define-module (crates-io ho tc hotcorners) #:use-module (crates-io))

(define-public crate-hotcorners-0.1.0 (c (n "hotcorners") (v "0.1.0") (d (list (d (n "windows") (r "^0.21") (d #t) (k 0)) (d (n "windows") (r "^0.21") (d #t) (k 1)))) (h "0scaxl2ganv8gkgn2sfj378fyvknb7nqsnk76wh4gdcn1i7lm0c5")))

(define-public crate-hotcorners-0.1.1 (c (n "hotcorners") (v "0.1.1") (d (list (d (n "windows") (r "^0.21") (d #t) (k 0)) (d (n "windows") (r "^0.21") (d #t) (k 1)))) (h "0806p43m0nvcfdj2lmgmw8w2z15nm7arcyhvigrwp8p9n1bbf1nr")))

(define-public crate-hotcorners-0.1.2 (c (n "hotcorners") (v "0.1.2") (d (list (d (n "windows") (r "^0.21") (d #t) (k 0)) (d (n "windows") (r "^0.21") (d #t) (k 1)))) (h "04ndp7v6rxv71c1v65y9bingqxnb5wxcdxvl8z3b9zbxjclcsfv5")))

(define-public crate-hotcorners-0.2.0 (c (n "hotcorners") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "windows") (r "^0.22") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi" "Win32_UI_KeyboardAndMouseInput" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "0wasdfd389bjdx70115cjfskkrmjfznjl83sv9gn8pncv9j5rrvd")))

(define-public crate-hotcorners-0.2.1 (c (n "hotcorners") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "windows") (r "^0.22") (f (quote ("Win32_Foundation" "Win32_Graphics_Gdi" "Win32_UI_KeyboardAndMouseInput" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "0qrbfhr2jl3jf2mp0mf2g7pvkd8sj6vslk2lqdld91kkn7p3cf2r")))

