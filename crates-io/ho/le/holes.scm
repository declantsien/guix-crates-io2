(define-module (crates-io ho le holes) #:use-module (crates-io))

(define-public crate-holes-0.0.1 (c (n "holes") (v "0.0.1") (d (list (d (n "futures") (r "^0.3") (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "time"))) (d #t) (k 2)))) (h "0kf8v1p1hs4lpdnji7ak6d8q2jhh19yfm9irij8d8ljlid4n2ffd") (y #t)))

(define-public crate-holes-0.0.2 (c (n "holes") (v "0.0.2") (d (list (d (n "futures") (r "^0.3") (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "time" "sync"))) (d #t) (k 2)))) (h "09bymwnqjy8zdbn32aspawcnkbxlnqzd1kcvhb3g8p93im4x7y26") (y #t)))

