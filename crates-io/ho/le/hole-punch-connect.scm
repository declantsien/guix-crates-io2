(define-module (crates-io ho le hole-punch-connect) #:use-module (crates-io))

(define-public crate-hole-punch-connect-0.1.0 (c (n "hole-punch-connect") (v "0.1.0") (h "1xjd9avgqaqzr3snn298a91q5fvqkx9m46npblblzg1msz80x91g")))

(define-public crate-hole-punch-connect-0.1.1 (c (n "hole-punch-connect") (v "0.1.1") (h "0dmqkck0hyivzgdvkipl0qnxi7wsa5wsl4549428wxfhnhvmk3xa")))

(define-public crate-hole-punch-connect-0.1.2 (c (n "hole-punch-connect") (v "0.1.2") (h "1932gkr3jk658j6b59ngz49v7pfyz9mvlal2bwhaa5f218d27910")))

