(define-module (crates-io ho le hole-punch) #:use-module (crates-io))

(define-public crate-hole-punch-0.0.0 (c (n "hole-punch") (v "0.0.0") (h "1ccmh11m330xf63yk600ipz4595r335id37wbsvmf04p9brnx8a2")))

(define-public crate-hole-punch-0.0.1 (c (n "hole-punch") (v "0.0.1") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.67") (d #t) (t "cfg(unix)") (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "1gixhmjzpn34xp1v99wdby760w6qn8d6dm2m8iv1blb9cn4nh8am")))

(define-public crate-hole-punch-0.0.2 (c (n "hole-punch") (v "0.0.2") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.67") (d #t) (t "cfg(unix)") (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("ioapiset" "winioctl"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1hdf6ysjzbiq36pnn11ll2n6hdag1h87hgafsm0d1icss9pr5b40")))

(define-public crate-hole-punch-0.0.3 (c (n "hole-punch") (v "0.0.3") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)) (d (n "errno") (r "^0.2.4") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2.67") (d #t) (t "cfg(unix)") (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("ioapiset" "winioctl"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1sysy5ww7syg92jqq90liggfphlklcxraw0qr34fddyz72drvgns")))

