(define-module (crates-io ho le holepunch) #:use-module (crates-io))

(define-public crate-holepunch-0.0.0 (c (n "holepunch") (v "0.0.0") (h "1hvaw6zcrq8hjqrrp062vl0agjihb4nx98hazqz7bvfzri64qjjm") (y #t)))

(define-public crate-holepunch-0.0.1 (c (n "holepunch") (v "0.0.1") (h "162nsymv2pnzdyry66qxxyq0qqyqyjc7pl6xl397qza6h7idwsj4") (y #t)))

(define-public crate-holepunch-0.0.2 (c (n "holepunch") (v "0.0.2") (d (list (d (n "icmp-socket") (r "^0.2.0") (d #t) (k 0)))) (h "02ss5x2mf25r265j0dljp3937cxaqw23bm137w463sa25if9b70s") (y #t)))

