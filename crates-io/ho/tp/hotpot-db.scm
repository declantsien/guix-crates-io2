(define-module (crates-io ho tp hotpot-db) #:use-module (crates-io))

(define-public crate-hotpot-db-0.0.0 (c (n "hotpot-db") (v "0.0.0") (d (list (d (n "rusqlite") (r "^0.21.0") (f (quote ("bundled" "serde_json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x35dqdall0cvh6nhsd2vwb7ds1zpi4jvw0hq8mvs9dp0icg6q8g")))

(define-public crate-hotpot-db-0.0.1 (c (n "hotpot-db") (v "0.0.1") (d (list (d (n "rusqlite") (r "^0.21.0") (f (quote ("bundled" "serde_json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ij6gz7910jgrlrjykrp0wq96vl8bq7vddlvwq5a9ly3crn98l2q")))

(define-public crate-hotpot-db-0.0.2 (c (n "hotpot-db") (v "0.0.2") (d (list (d (n "rusqlite") (r "^0.21.0") (f (quote ("bundled" "serde_json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sxqic67mhph31k43ig19i707i6ggcz31ssvd4h334j0mhhgyi4j")))

