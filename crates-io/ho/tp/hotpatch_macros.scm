(define-module (crates-io ho tp hotpatch_macros) #:use-module (crates-io))

(define-public crate-hotpatch_macros-0.1.0 (c (n "hotpatch_macros") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01xp35y1vzgb7p0gm53va8wq1h04a0c4n6inlmif7829qbspag4m") (f (quote (("redirect-main") ("default") ("allow-main"))))))

(define-public crate-hotpatch_macros-0.2.0 (c (n "hotpatch_macros") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zas1az2nb5b073715z7y91yfyrajvrzdqcxqvph3pp2w970jznf") (f (quote (("redirect-main") ("default") ("allow-main"))))))

(define-public crate-hotpatch_macros-0.3.0 (c (n "hotpatch_macros") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lpsa9bls374k4k79hg33hp9r6fijp1kxiaywvapyiabp4rz72dy") (f (quote (("redirect-main") ("default") ("allow-main"))))))

