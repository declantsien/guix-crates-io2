(define-module (crates-io ho wl howl) #:use-module (crates-io))

(define-public crate-howl-0.1.0 (c (n "howl") (v "0.1.0") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "keyring") (r "^0.10.1") (d #t) (k 0)) (d (n "read_input") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "tree_magic") (r "^0.2.3") (d #t) (k 0)) (d (n "ureq") (r "^1.5.2") (d #t) (k 0)))) (h "1h272pnn4vidacqf2hi89dr7m95234rba2iacxsawpnaibi2pl74")))

