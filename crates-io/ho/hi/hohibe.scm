(define-module (crates-io ho hi hohibe) #:use-module (crates-io))

(define-public crate-hohibe-0.1.0 (c (n "hohibe") (v "0.1.0") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bls12_381_plus") (r "^0.8.9") (d #t) (k 0)) (d (n "ctr") (r "^0.9.2") (f (quote ("alloc" "std"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1573ixqfqm2kdc1kkw3v1pr01qj1s8ipjmbzabqyd908wx2mnhk5")))

