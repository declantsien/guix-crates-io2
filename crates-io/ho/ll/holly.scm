(define-module (crates-io ho ll holly) #:use-module (crates-io))

(define-public crate-holly-0.1.0 (c (n "holly") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tokio-threadpool") (r "^0.1.11") (d #t) (k 2)))) (h "0hwrm3lnnrbgv34cbazip7js4jflv04785j7s5nf8fdhlbk407xl")))

(define-public crate-holly-0.2.0 (c (n "holly") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4.11") (d #t) (k 2)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tokio") (r "^0.1.15") (d #t) (k 2)) (d (n "tokio-threadpool") (r "^0.1.11") (d #t) (k 2)))) (h "1jid627gxq1sd5cif8dcyagp6f77snxgharzjz22kgd8h21bkw57")))

(define-public crate-holly-0.3.0 (c (n "holly") (v "0.3.0") (d (list (d (n "bytes") (r "^0.4.11") (d #t) (k 2)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tokio") (r "^0.1.15") (d #t) (k 2)) (d (n "tokio-threadpool") (r "^0.1.11") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "0z0l15qiyvcq1aa9n33xpr0g6sxarq3a8z8344fcczb9q5603s2a")))

(define-public crate-holly-0.4.0 (c (n "holly") (v "0.4.0") (d (list (d (n "bytes") (r "^0.4.11") (d #t) (k 2)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tokio") (r "^0.1.15") (d #t) (k 2)) (d (n "tokio-threadpool") (r "^0.1.11") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "115jgkb58h8mxmz8yqc5jag3r5dycah546kb00f01hra2426y4fq")))

(define-public crate-holly-0.5.0 (c (n "holly") (v "0.5.0") (d (list (d (n "bytes") (r "^0.4.11") (d #t) (k 2)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tokio") (r "^0.1.15") (d #t) (k 2)) (d (n "tokio-threadpool") (r "^0.1.11") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "1pg48vyxv9ww9g81li38qm1s79hvgwdl2g26pp1z7ihyds028pgk")))

(define-public crate-holly-0.5.1 (c (n "holly") (v "0.5.1") (d (list (d (n "bytes") (r "^0.4.11") (d #t) (k 2)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tokio") (r "^0.1.15") (d #t) (k 2)) (d (n "tokio-threadpool") (r "^0.1.11") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "031kwsi8vw5yqyy71pbzwv85lzfhxx8463q9al8i5b26cq5vf67p")))

(define-public crate-holly-0.5.2 (c (n "holly") (v "0.5.2") (d (list (d (n "bytes") (r "^0.4.11") (d #t) (k 2)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tokio") (r "^0.1.15") (d #t) (k 2)) (d (n "tokio-threadpool") (r "^0.1.11") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "1zi3wh06i2dr1nwkpcw0v5wv0p6bwyvbi43lma3bjifkfz7i6a86")))

(define-public crate-holly-0.5.3 (c (n "holly") (v "0.5.3") (d (list (d (n "bytes") (r "^0.4.11") (d #t) (k 2)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "parking_lot") (r "^0.8.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.15") (d #t) (k 2)) (d (n "tokio-threadpool") (r "^0.1.11") (d #t) (k 2)) (d (n "void") (r "^1.0.2") (d #t) (k 0)))) (h "0c36vfh8jfxgg09sc7j6q4awmgd2ayv0jw7k7m82ap1h7hk7dl1w")))

