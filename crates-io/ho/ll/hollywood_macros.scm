(define-module (crates-io ho ll hollywood_macros) #:use-module (crates-io))

(define-public crate-hollywood_macros-0.1.0 (c (n "hollywood_macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "0sdc2x2bgwmgv2r4f90jy7zw4ml7sf5pr18d4jkgfsr5rs162xrk")))

(define-public crate-hollywood_macros-0.2.0 (c (n "hollywood_macros") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1mnip7vd29xlc4whm9ds3x0bdl188p49rxkzfdmndc68111izb5q")))

(define-public crate-hollywood_macros-0.2.1 (c (n "hollywood_macros") (v "0.2.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "0zxdqbcd57kv1aihk9bz3m16136hxb71r0y3x7pb6qas8xpgjarr")))

(define-public crate-hollywood_macros-0.2.2 (c (n "hollywood_macros") (v "0.2.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1f497nv9awi5lp7wyv09qy4sk2hqgrwyn859az17y9vkrr8ninfc")))

(define-public crate-hollywood_macros-0.3.0 (c (n "hollywood_macros") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "19nrlzlvr2xn98y3w4cq68mz65mzc36bss8iivrl9qrpi71jm4fn")))

(define-public crate-hollywood_macros-0.4.0 (c (n "hollywood_macros") (v "0.4.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "0kxx4hmxy6y6q5mnfdcv8w8gf63f9wnqryahaalvm4s8qk2hg8fz")))

(define-public crate-hollywood_macros-0.5.0 (c (n "hollywood_macros") (v "0.5.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1rkbf1gwvs37w22r0ka5v0mr4ki272qkib57c43n6s4nzh2x2lxr")))

(define-public crate-hollywood_macros-0.5.1 (c (n "hollywood_macros") (v "0.5.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0clvgbj847nsdj4biw5z5slca7j6p650i4kgi7kmgya0crkhq96m")))

(define-public crate-hollywood_macros-0.5.2 (c (n "hollywood_macros") (v "0.5.2") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0anb8jsinn1xmj5970savgb788l9kajla7j1y7yi722891kckqdb")))

(define-public crate-hollywood_macros-0.6.0 (c (n "hollywood_macros") (v "0.6.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06z4dffkhra8lfc6s30ncmwcvbczhs2dbwy75axnnvnvp20b8lzc")))

(define-public crate-hollywood_macros-0.7.0 (c (n "hollywood_macros") (v "0.7.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0m3y8y7ai2w543k5bclih8ynj5bzwfyysyadfbiv7dkw33vjhpb6")))

