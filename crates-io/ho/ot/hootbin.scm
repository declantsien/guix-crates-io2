(define-module (crates-io ho ot hootbin) #:use-module (crates-io))

(define-public crate-hootbin-0.1.0 (c (n "hootbin") (v "0.1.0") (d (list (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "hoot") (r "^0.1.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0gv26ca9g8k1mi906bsvhv9sf0x7xpgwsrfv7202hr0d8lsdfh7p")))

(define-public crate-hootbin-0.1.1 (c (n "hootbin") (v "0.1.1") (d (list (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "hoot") (r "^0.1.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1cpxbk2miw5hycxqdydbdvf5jhkw42nn55abqhwimsj9is360kim") (r "1.61")))

(define-public crate-hootbin-0.1.4 (c (n "hootbin") (v "0.1.4") (d (list (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "hoot") (r "^0.1.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0pj626jiwi7sahzr98a1ydj0f91scd6cpswg91q53g69ycywpp9h") (f (quote (("default" "json")))) (s 2) (e (quote (("json" "dep:serde" "dep:serde_json")))) (r "1.61")))

(define-public crate-hootbin-0.1.5 (c (n "hootbin") (v "0.1.5") (d (list (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "hoot") (r "^0.1.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1f616q6z7z97p1ylns8hdbikcpbazyad0370mfihkq8sj4brxkzb") (r "1.61")))

