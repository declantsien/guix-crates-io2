(define-module (crates-io ho ot hootie) #:use-module (crates-io))

(define-public crate-hootie-0.5.0 (c (n "hootie") (v "0.5.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.26") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.26") (d #t) (k 0)) (d (n "structopt") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "05yfygypxibj9rdjbf2lvsammsbwfy85z1a0i8jpvq64pssjwmiv")))

