(define-module (crates-io ho lu holup) #:use-module (crates-io))

(define-public crate-holup-0.1.1 (c (n "holup") (v "0.1.1") (h "0bp1wg209vd7yrfjw1sxd743bxnpgrws2ws1ljanbpg4ix64m192")))

(define-public crate-holup-0.2.0 (c (n "holup") (v "0.2.0") (h "0x1bjhsm1pmglfjdph2hjydh92vv3ri7fij2np0pvbzpf8pmwryx")))

