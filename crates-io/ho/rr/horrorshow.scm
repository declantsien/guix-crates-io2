(define-module (crates-io ho rr horrorshow) #:use-module (crates-io))

(define-public crate-horrorshow-0.3.0 (c (n "horrorshow") (v "0.3.0") (d (list (d (n "maud") (r "*") (d #t) (k 2)) (d (n "maud_macros") (r "*") (d #t) (k 2)))) (h "1hg1msis30y9f49riiwwp9if04z3h24qj8a3iw1dcqgvl2rn260m")))

(define-public crate-horrorshow-0.3.1 (c (n "horrorshow") (v "0.3.1") (d (list (d (n "maud") (r "*") (d #t) (k 2)) (d (n "maud_macros") (r "*") (d #t) (k 2)))) (h "0phka2l0fl0nxb3p98rivhpich5l0k424ii46qih6hxgc7by5l2p")))

(define-public crate-horrorshow-0.3.2 (c (n "horrorshow") (v "0.3.2") (d (list (d (n "maud") (r "*") (d #t) (k 2)) (d (n "maud_macros") (r "*") (d #t) (k 2)))) (h "0r4m1lv4cih8ivlivz92bb589lnqzzl1ni8phv97jzmvrn34kbv1")))

(define-public crate-horrorshow-0.3.3 (c (n "horrorshow") (v "0.3.3") (d (list (d (n "maud") (r "*") (d #t) (k 2)) (d (n "maud_macros") (r "*") (d #t) (k 2)))) (h "1z2mr17i9b08bja901xz5v12azggg69vz4dwibrk7kxg5ljp7ckb") (f (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.3.4 (c (n "horrorshow") (v "0.3.4") (d (list (d (n "maud") (r "*") (d #t) (k 2)) (d (n "maud_macros") (r "*") (d #t) (k 2)))) (h "10qjgdc8izn736ygw6iylyfv08dpm8x5znm82dha4hyy7hi9ps2q") (f (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.3.5 (c (n "horrorshow") (v "0.3.5") (d (list (d (n "maud") (r "*") (d #t) (k 2)) (d (n "maud_macros") (r "*") (d #t) (k 2)))) (h "15dcp98rwgx7ppragnm0aki89kwhhpa07bszx70d46fz20laahmn") (f (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.4.0 (c (n "horrorshow") (v "0.4.0") (d (list (d (n "maud") (r "*") (d #t) (k 2)) (d (n "maud_macros") (r "*") (d #t) (k 2)))) (h "1a9mn4vfznxkys66bj9pcacylfp3mawbbkw429pkqxhjqdra9nwx") (f (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.5.0 (c (n "horrorshow") (v "0.5.0") (d (list (d (n "maud") (r "*") (d #t) (k 2)) (d (n "maud_macros") (r "*") (d #t) (k 2)))) (h "0vg7y9mi493j1fp25rbgsd7aq984rpkmvxwbc9dazn8lp2dc52yl") (f (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.5.1 (c (n "horrorshow") (v "0.5.1") (d (list (d (n "maud") (r "*") (d #t) (k 2)) (d (n "maud_macros") (r "*") (d #t) (k 2)))) (h "1srllv4grbzkqg32wyy3zjlzmhiggs6yaf911i4s1z9nr0b9f6ac") (f (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.5.2 (c (n "horrorshow") (v "0.5.2") (d (list (d (n "maud") (r "*") (d #t) (k 2)) (d (n "maud_macros") (r "*") (d #t) (k 2)))) (h "1p76952w1q9sckr3chqxi6i42hw1rjk0av0yh3l9xznqf1wrh09p") (f (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.5.3 (c (n "horrorshow") (v "0.5.3") (d (list (d (n "maud") (r "*") (d #t) (k 2)) (d (n "maud_macros") (r "*") (d #t) (k 2)))) (h "03146ijkw6ighrcs2h8ljdmym1axzm136gig2p45hhak7wwhi1ah") (f (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.5.4 (c (n "horrorshow") (v "0.5.4") (d (list (d (n "maud") (r "^0.8") (d #t) (k 2)) (d (n "maud_macros") (r "^0.8") (d #t) (k 2)))) (h "1wrpj2mdrww4sbqv88kmf45w9zsk38wvnhjam9hn0vj8a7r06gz3") (f (quote (("ops") ("default" "ops")))) (y #t)))

(define-public crate-horrorshow-0.5.5 (c (n "horrorshow") (v "0.5.5") (d (list (d (n "maud") (r "^0.8") (d #t) (k 2)) (d (n "maud_macros") (r "^0.8") (d #t) (k 2)))) (h "0fqycg93mfv74zr90mssizi5hl2qfrd58zix4iqi9793kqmi5sfv") (f (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.5.6 (c (n "horrorshow") (v "0.5.6") (d (list (d (n "maud") (r "^0.8") (d #t) (k 2)) (d (n "maud_macros") (r "^0.8") (d #t) (k 2)))) (h "1if3r8l2xrsb44bd1mgm80vkq7v5hdlabfjwg30kmpfyiwan47ak") (f (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.5.7 (c (n "horrorshow") (v "0.5.7") (d (list (d (n "maud") (r "^0.9") (d #t) (k 2)) (d (n "maud_macros") (r "^0.9") (d #t) (k 2)))) (h "0gr1hya2l6c9fv2k8ilddah922fgvgxkarzcwhjf6wcjjsq2dfl2") (f (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.5.8 (c (n "horrorshow") (v "0.5.8") (d (list (d (n "maud") (r "^0.11") (d #t) (k 2)) (d (n "maud_macros") (r "^0.11") (d #t) (k 2)))) (h "0hwmaqbz6ckdz9dq59vi3gahx78hrw4giqv9f3chlasnkpw2hii3") (f (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.6.0 (c (n "horrorshow") (v "0.6.0") (d (list (d (n "maud") (r "^0.13") (d #t) (k 2)) (d (n "maud_macros") (r "^0.13") (d #t) (k 2)))) (h "01pzz5lq25cas15hc9nb4v0zhqdwbsd1f3maqizhjq9y37jgf31a") (f (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.6.1 (c (n "horrorshow") (v "0.6.1") (d (list (d (n "maud") (r "^0.13") (d #t) (k 2)) (d (n "maud_macros") (r "^0.13") (d #t) (k 2)))) (h "0nr04dar7vqghwfpfcprmpdpi8ms9rmisijl84asj8bkchasav13") (f (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.6.2 (c (n "horrorshow") (v "0.6.2") (d (list (d (n "maud") (r "^0.16") (d #t) (k 2)) (d (n "maud_macros") (r "^0.16") (d #t) (k 2)))) (h "1klbjd4i1wkmd8pr32fy76iv3jbx0ynzrqw81hzn2kmz2m8pd1ym") (f (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.6.3 (c (n "horrorshow") (v "0.6.3") (d (list (d (n "maud") (r "^0.17") (d #t) (k 2)) (d (n "maud_macros") (r "^0.17") (d #t) (k 2)))) (h "174gns3kmpwx4ycnqzda6x4dsj9zhswish5h67sfsz0gbkfn0nf3") (f (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.6.4 (c (n "horrorshow") (v "0.6.4") (h "1bvgbym6h6gsa6qx6drf1k57ac56zs6r9hc5q6h8j87s785jdxcr") (f (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.6.5 (c (n "horrorshow") (v "0.6.5") (h "0hr1j0bkzmlmqdyfdf5dk5wrrprbxf0gvc6iw5pndzlc4dhyx5r5") (f (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.6.6 (c (n "horrorshow") (v "0.6.6") (h "095njs49v9ma3yb9dpawjdggim09d2nklf550nnv31fcq3v78xjr") (f (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.7.0 (c (n "horrorshow") (v "0.7.0") (h "0yk9xsdigmxh3r5lvssjldfp5s502vg9jl8sjk4kb72b6rplzf3k") (f (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.8.0 (c (n "horrorshow") (v "0.8.0") (h "07lyckgbd8cx3qh3dnv5ds0a2rnqwsrh7x2h5xiak1xx1wg5vn5x") (f (quote (("std" "alloc") ("ops") ("default" "ops" "std" "alloc") ("alloc"))))))

(define-public crate-horrorshow-0.8.1 (c (n "horrorshow") (v "0.8.1") (h "0kahf2dxv6xx1v4yzdgsqxgj4wnzl39k3260isa9djdy7pqf34si") (f (quote (("std" "alloc") ("ops") ("default" "ops" "std" "alloc") ("alloc"))))))

(define-public crate-horrorshow-0.8.2 (c (n "horrorshow") (v "0.8.2") (h "1pzh0j45xvkdp77mnsb93smvbpq43c1h4jxh6mkqb71x28r7cldd") (f (quote (("std" "alloc") ("ops") ("default" "ops" "std" "alloc") ("alloc"))))))

(define-public crate-horrorshow-0.8.3 (c (n "horrorshow") (v "0.8.3") (h "1yykh6hly8dk2ghvzgjil41aa4g8cib4xjdbjsc4ir683c57xkl7") (f (quote (("std" "alloc") ("ops") ("default" "ops" "std" "alloc") ("alloc"))))))

(define-public crate-horrorshow-0.8.4 (c (n "horrorshow") (v "0.8.4") (h "185yb8mzxiivqfm1v1nll5k797v9yrxi3jzpahd0n5a032cgnwc3") (f (quote (("std" "alloc") ("ops") ("default" "ops" "std" "alloc") ("alloc"))))))

