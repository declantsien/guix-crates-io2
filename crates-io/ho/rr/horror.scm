(define-module (crates-io ho rr horror) #:use-module (crates-io))

(define-public crate-horror-0.1.0 (c (n "horror") (v "0.1.0") (h "0s8vhm95v0gdi1h4g0d35cvifwwcavhjjfbkkmnw64vs29n1l73y") (y #t)))

(define-public crate-horror-0.1.1 (c (n "horror") (v "0.1.1") (h "0a16j1sqrc3k9j5c8k43zf2nac6v65w5v2zk7k7zvzif6wprpfr8")))

(define-public crate-horror-0.1.2 (c (n "horror") (v "0.1.2") (h "0wnwzinryii2sh2j3wf4qj6ry40cd7ks4pvnaqfav1anhfvs2qyn")))

