(define-module (crates-io ho ho hohoho) #:use-module (crates-io))

(define-public crate-hohoho-0.1.0 (c (n "hohoho") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "brainfuck") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01m01jbh54ln9bkc28kfqgj3jvqhq3z4j8jhvqz70rna5mvvz1hy")))

(define-public crate-hohoho-0.1.1 (c (n "hohoho") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "brainfuck") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0va0l51iwj3ilyxhrnlqsbm2j59x7glna1g4bjdmawgqqvcy7jbz")))

