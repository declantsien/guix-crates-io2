(define-module (crates-io ho tg hotglsl) #:use-module (crates-io))

(define-public crate-hotglsl-0.1.0 (c (n "hotglsl") (v "0.1.0") (d (list (d (n "glsl-to-spirv") (r "^0.1.7") (d #t) (k 0)) (d (n "notify") (r "^5.0.0-pre.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0pnbvaj5mv2lnbgszlqvmljs8nsd4v15i9p132j981yjfqnx4f08")))

(define-public crate-hotglsl-0.2.0 (c (n "hotglsl") (v "0.2.0") (d (list (d (n "naga") (r "^0.14") (f (quote ("glsl-in" "spv-out"))) (d #t) (k 0)) (d (n "notify") (r "^6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1xriq13dk9xyafvmgdzb2j05xngd82smgyq1i3rw5xi6aqnv6ma2")))

