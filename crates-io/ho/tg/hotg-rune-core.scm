(define-module (crates-io ho tg hotg-rune-core) #:use-module (crates-io))

(define-public crate-hotg-rune-core-0.4.0 (c (n "hotg-rune-core") (v "0.4.0") (d (list (d (n "log") (r "^0.4.14") (f (quote ("serde" "max_level_trace"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)))) (h "1phxrwsxpj7jifcl0ijlga56mkhj57fk55d4mgrikcdhd4sv750i") (f (quote (("std") ("default"))))))

(define-public crate-hotg-rune-core-0.4.1 (c (n "hotg-rune-core") (v "0.4.1") (d (list (d (n "log") (r "^0.4.14") (f (quote ("serde" "max_level_trace"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)))) (h "1p5byq8x6w46v11x634j3grkji4xpj2jjlnpqk6b67bsn2xpfgf8") (f (quote (("std") ("default"))))))

(define-public crate-hotg-rune-core-0.5.0 (c (n "hotg-rune-core") (v "0.5.0") (d (list (d (n "log") (r "^0.4.14") (f (quote ("serde" "max_level_trace"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)))) (h "030z50zjjb8z3gmpkpfpdfw0pwj3xzij9zspz5mhl7xx3s05jvl5") (f (quote (("std") ("default"))))))

(define-public crate-hotg-rune-core-0.5.1 (c (n "hotg-rune-core") (v "0.5.1") (d (list (d (n "log") (r "^0.4.14") (f (quote ("serde" "max_level_trace"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)))) (h "1ff95wqwmd3fy7g867ihfh0yr7jgz09kpjq5kzp4wa5l2fkqvwml") (f (quote (("std") ("default"))))))

(define-public crate-hotg-rune-core-0.5.2 (c (n "hotg-rune-core") (v "0.5.2") (d (list (d (n "log") (r "^0.4.14") (f (quote ("serde" "max_level_trace"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)))) (h "1w2x09g7clkpzd4naww68n2phq0hmsicxv3hkimqcabrmbizbiv5") (f (quote (("std") ("default"))))))

(define-public crate-hotg-rune-core-0.5.3 (c (n "hotg-rune-core") (v "0.5.3") (d (list (d (n "log") (r "^0.4.14") (f (quote ("serde" "max_level_trace"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)))) (h "0c6vgn15b3k4lhwvbcc8915lh1xgvqkw7iglrc74ydiaag00422l") (f (quote (("std") ("default"))))))

(define-public crate-hotg-rune-core-0.6.0 (c (n "hotg-rune-core") (v "0.6.0") (d (list (d (n "log") (r "^0.4.14") (f (quote ("serde" "max_level_trace"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)))) (h "0sm1wjblgi5ypc0l37xbh09y25ab0n1xhyy8id0mvzh1w2qh5cg2") (f (quote (("std") ("default"))))))

(define-public crate-hotg-rune-core-0.7.0 (c (n "hotg-rune-core") (v "0.7.0") (d (list (d (n "log") (r "^0.4.14") (f (quote ("serde" "max_level_trace"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)))) (h "025dqh1jb1ca96crinipmk79qc8m5mry41rfzg7h9w0vl93ylwcw") (f (quote (("std") ("default"))))))

(define-public crate-hotg-rune-core-0.8.0 (c (n "hotg-rune-core") (v "0.8.0") (d (list (d (n "log") (r "^0.4.14") (f (quote ("serde" "max_level_trace"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)))) (h "1d1c0bma24kb5fmx9p0r8l5vg1y2wvfcx7frzby6rcnv70wrvx12") (f (quote (("std") ("default"))))))

(define-public crate-hotg-rune-core-0.9.0 (c (n "hotg-rune-core") (v "0.9.0") (d (list (d (n "log") (r "^0.4.14") (f (quote ("serde" "max_level_trace"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)))) (h "07v2b8d8z4bm5dagzlp9ysglk2m6kj7y09x97jjw0paxl1wz8r72") (f (quote (("std") ("default"))))))

(define-public crate-hotg-rune-core-0.8.1 (c (n "hotg-rune-core") (v "0.8.1") (d (list (d (n "log") (r "^0.4.14") (f (quote ("serde" "max_level_trace"))) (k 0)) (d (n "new-hotg-rune-core") (r "^0.9.0") (d #t) (k 0) (p "hotg-rune-core")) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)))) (h "12amrrgy49da8fx83h1b4bmyr27444rf6766j4mcy938jh040zkv") (f (quote (("std") ("default"))))))

(define-public crate-hotg-rune-core-0.9.1 (c (n "hotg-rune-core") (v "0.9.1") (d (list (d (n "log") (r "^0.4.14") (f (quote ("serde" "max_level_trace"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)))) (h "1zxa101zqz6inyz8dkp3bg8w29k4sni763yg8k55nkh289jn1qpy") (f (quote (("std") ("default"))))))

(define-public crate-hotg-rune-core-0.9.2 (c (n "hotg-rune-core") (v "0.9.2") (d (list (d (n "log") (r "^0.4.14") (f (quote ("serde" "max_level_trace"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)))) (h "09zj4zgl22sdmfmijlw0xw0jqmwp37cciyk3n8ajjf4ahkydmhk2") (f (quote (("std") ("default"))))))

(define-public crate-hotg-rune-core-0.9.3 (c (n "hotg-rune-core") (v "0.9.3") (d (list (d (n "log") (r "^0.4.14") (f (quote ("serde" "max_level_trace"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)))) (h "1l6h1nrdnd1vyzdpa86dbc8rhf9n1gdg1cfs47d3cls1xhna493z") (f (quote (("std") ("default"))))))

(define-public crate-hotg-rune-core-0.10.0 (c (n "hotg-rune-core") (v "0.10.0") (d (list (d (n "log") (r "^0.4.14") (f (quote ("serde" "max_level_trace"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)))) (h "0l88wydiglmhdbykyq3r283c7vz51p99xb9jisnz737w8i0g7fx0") (f (quote (("std") ("default"))))))

(define-public crate-hotg-rune-core-0.9.4 (c (n "hotg-rune-core") (v "0.9.4") (d (list (d (n "new-hotg-rune-core") (r "^0.10.0") (d #t) (k 0) (p "hotg-rune-core")))) (h "1b77ff5pyhl82p3h25d7vxhfbmimrmx4b3z1fd9pwk0lnh3b40xs") (f (quote (("std" "new-hotg-rune-core/std") ("default"))))))

(define-public crate-hotg-rune-core-0.11.0 (c (n "hotg-rune-core") (v "0.11.0") (d (list (d (n "log") (r "^0.4.14") (f (quote ("serde" "max_level_trace"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)))) (h "0rh2pk8isjk0ihyjcin85gmrdh5s5fpdpv9r4786xhpg9p1ca828") (f (quote (("unstable_doc_cfg") ("std") ("default"))))))

(define-public crate-hotg-rune-core-0.11.1 (c (n "hotg-rune-core") (v "0.11.1") (d (list (d (n "log") (r "^0.4.14") (f (quote ("serde" "max_level_trace"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)))) (h "15x39172h947b86nswnvmg75kljvxgzl73nl0k52n5c9q1mh7rj8") (f (quote (("unstable_doc_cfg") ("std") ("default"))))))

(define-public crate-hotg-rune-core-0.11.2 (c (n "hotg-rune-core") (v "0.11.2") (d (list (d (n "log") (r "^0.4.14") (f (quote ("serde" "max_level_trace"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)))) (h "1xgrh6q1a9z2nr8cnarj5n3q338q6d3msy50k4ddrwz5gysi5ix5") (f (quote (("unstable_doc_cfg") ("std") ("default"))))))

(define-public crate-hotg-rune-core-0.11.3 (c (n "hotg-rune-core") (v "0.11.3") (d (list (d (n "log") (r "^0.4.14") (f (quote ("serde" "max_level_trace"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)))) (h "0xb0pwwbppn6lazvhnhhfkr6f2zh0yrv5jmxp5h6jpfjxqclspap") (f (quote (("unstable_doc_cfg") ("std") ("default"))))))

