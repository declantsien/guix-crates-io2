(define-module (crates-io ho ri horizontal_mixer) #:use-module (crates-io))

(define-public crate-horizontal_mixer-0.1.0 (c (n "horizontal_mixer") (v "0.1.0") (d (list (d (n "kira") (r "^0.8.5") (d #t) (k 0)))) (h "1541i01qmsy1dn8vm0qwv7dczb7jiv79j7f451s6789c4bbdn2ha")))

(define-public crate-horizontal_mixer-0.1.1 (c (n "horizontal_mixer") (v "0.1.1") (d (list (d (n "kira") (r "^0.8.5") (d #t) (k 0)))) (h "0npqbkx1wj1gdw76s67h6nkl8lx1knpn8la796fwhk14zk31dwgm")))

(define-public crate-horizontal_mixer-0.1.2 (c (n "horizontal_mixer") (v "0.1.2") (d (list (d (n "kira") (r "^0.8.5") (d #t) (k 0)))) (h "07y230arvs6zifknq7f4zrf3wr9w6426dlxkk7kbzngd0kgiv4yr")))

(define-public crate-horizontal_mixer-0.1.3 (c (n "horizontal_mixer") (v "0.1.3") (d (list (d (n "kira") (r "^0.8.5") (d #t) (k 0)))) (h "1k42wivz6xbcz73qifsy80ly8spf68hzqbp8n0jdixg8byzcn3fc")))

