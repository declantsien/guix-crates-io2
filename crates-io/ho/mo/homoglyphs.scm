(define-module (crates-io ho mo homoglyphs) #:use-module (crates-io))

(define-public crate-homoglyphs-0.1.0 (c (n "homoglyphs") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.17") (d #t) (k 0)) (d (n "homoglyph-core") (r "^0.1.0") (d #t) (k 0)) (d (n "homoglyph-driver") (r "^0.1.0") (d #t) (k 0)) (d (n "homoglyph-service") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (d #t) (k 0)))) (h "1y1s7v203rwfwji9qf3rv1bxix24yfaz69xjlrgi1a7l94bqchj4")))

