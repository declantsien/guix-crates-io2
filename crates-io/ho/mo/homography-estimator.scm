(define-module (crates-io ho mo homography-estimator) #:use-module (crates-io))

(define-public crate-homography-estimator-0.1.0 (c (n "homography-estimator") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "noisy_float") (r "^0.2") (d #t) (k 0)) (d (n "ofps") (r "^0.1") (d #t) (k 0)) (d (n "opencv") (r "^0.62") (f (quote ("clang-runtime"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1ygl3l9g9xnk8j2m8clriphwxnglgxrqw3y0ab1nlxjfhv3vcprg")))

