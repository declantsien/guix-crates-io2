(define-module (crates-io ho mo homoglyph-driver) #:use-module (crates-io))

(define-public crate-homoglyph-driver-0.1.0 (c (n "homoglyph-driver") (v "0.1.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "homoglyph-core") (r "^0.1.0") (d #t) (k 0)) (d (n "tantivy") (r "^0.18.0") (d #t) (k 0)))) (h "16673xrmjfb6x886b81ij3g5lb4q6qmnxbj3s3nfxw4pjg3hw0ys")))

