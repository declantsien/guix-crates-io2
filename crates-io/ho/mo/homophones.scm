(define-module (crates-io ho mo homophones) #:use-module (crates-io))

(define-public crate-homophones-1.0.0 (c (n "homophones") (v "1.0.0") (d (list (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nrn2ka49y9x4pcw5y480cngf0hc4qrb4169gh2az0qbdsswjzpq")))

(define-public crate-homophones-1.0.1 (c (n "homophones") (v "1.0.1") (d (list (d (n "docopt") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "12ch6s5ppmdxlxf20m73ccs8189nj9gbdi4b0iwm71pkjl9yz97f")))

