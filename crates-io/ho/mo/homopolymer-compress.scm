(define-module (crates-io ho mo homopolymer-compress) #:use-module (crates-io))

(define-public crate-homopolymer-compress-1.0.0 (c (n "homopolymer-compress") (v "1.0.0") (d (list (d (n "bio") (r "^0.40.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "simplelog") (r "^0.11.2") (d #t) (k 0)))) (h "0vdkfls2b81fhvby7vx0s1pxrrav363lhwihn0dsqzjyn5348nqc") (r "1.58.1")))

