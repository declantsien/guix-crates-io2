(define-module (crates-io ho mo homo-rs) #:use-module (crates-io))

(define-public crate-homo-rs-0.1.0 (c (n "homo-rs") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.13.1") (d #t) (k 0)) (d (n "pomsky-macro") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "uint") (r "^0.9.3") (d #t) (k 0)))) (h "09akl0lli91ai9b0mpsbilf3a31i94s26nlxdrp6nl8siipr2rn1")))

(define-public crate-homo-rs-0.1.1 (c (n "homo-rs") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.13.1") (d #t) (k 0)) (d (n "pomsky-macro") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "uint") (r "^0.9.3") (d #t) (k 0)))) (h "18swn3hb3nzcqckycyrm31ymcihlpydqx5fmm7z897ps482gin6n")))

(define-public crate-homo-rs-0.1.2 (c (n "homo-rs") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.13.1") (d #t) (k 0)) (d (n "pomsky-macro") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "uint") (r "^0.9.3") (d #t) (k 0)))) (h "05wb7rbsq973w9w3lxwf9rxnx1mvkvy3m0r0s2kca8pdv4pvg844")))

(define-public crate-homo-rs-0.1.3 (c (n "homo-rs") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.13.1") (d #t) (k 0)) (d (n "pomsky-macro") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "uint") (r "^0.9.3") (d #t) (k 0)))) (h "0ss0sciays004yd7v7cigj1bcx6h8pw69c8fjmam1yi8z556k8x2")))

