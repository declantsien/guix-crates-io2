(define-module (crates-io ho mo homoglyph-service) #:use-module (crates-io))

(define-public crate-homoglyph-service-0.1.0 (c (n "homoglyph-service") (v "0.1.0") (d (list (d (n "homoglyph-core") (r "^0.1.0") (d #t) (k 0)) (d (n "homoglyph-driver") (r "^0.1.0") (d #t) (k 0)))) (h "1f3cy17md4haj632r1zjhvmfs05g44rragmyjg95bjsxa4rfjd54")))

