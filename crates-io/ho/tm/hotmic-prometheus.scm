(define-module (crates-io ho tm hotmic-prometheus) #:use-module (crates-io))

(define-public crate-hotmic-prometheus-0.1.0 (c (n "hotmic-prometheus") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hotmic") (r "^0.8") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "tokio-sync") (r "^0.1") (d #t) (k 0)))) (h "1n3zdz23s89c0jkwzlxpsl2c3mm2apb6czzlxav75cgaf28lxsf5")))

