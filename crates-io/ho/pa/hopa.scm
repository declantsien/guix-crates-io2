(define-module (crates-io ho pa hopa) #:use-module (crates-io))

(define-public crate-HOPA-0.1.0 (c (n "HOPA") (v "0.1.0") (d (list (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)))) (h "121g287hfikgzfv3v4pxj0ci93f72c0idnkd7xkxjn8b8lhzbzhp")))

(define-public crate-HOPA-0.1.1 (c (n "HOPA") (v "0.1.1") (d (list (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)))) (h "17gpzp10ijm5qi4swvdd2pmqdg9xp3haihddgn2q115ldmyxx7n0")))

(define-public crate-HOPA-0.1.2 (c (n "HOPA") (v "0.1.2") (d (list (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)))) (h "1m9r7w3a3f8wvn4gylwbai5zz4aa5vya49zhnmpb7lkvqx4abji0")))

