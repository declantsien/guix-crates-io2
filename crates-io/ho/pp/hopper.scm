(define-module (crates-io ho pp hopper) #:use-module (crates-io))

(define-public crate-hopper-0.1.0 (c (n "hopper") (v "0.1.0") (d (list (d (n "bincode") (r "^0.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0vy3cjf1dwn578hjsaznk2l60snldvks01nk4gly551a3xml5c8g")))

(define-public crate-hopper-0.1.1 (c (n "hopper") (v "0.1.1") (d (list (d (n "bincode") (r "^0.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0qk2pz079mx4a5q36j3basxnmnjxdgdaa6jakpv74z6crhq6h5wm") (y #t)))

(define-public crate-hopper-0.1.2 (c (n "hopper") (v "0.1.2") (d (list (d (n "bincode") (r "^0.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1hjy7gwbl0474x1fa4wwp9010bk81h88i38jcgp115bh9masxplb")))

(define-public crate-hopper-0.2.0 (c (n "hopper") (v "0.2.0") (d (list (d (n "bincode") (r "^1.0.0-alpha1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1xxhljpr1mlvb70v0cziprimcb4m2klxal9nr8522pd9pa7lv35q")))

(define-public crate-hopper-0.2.1 (c (n "hopper") (v "0.2.1") (d (list (d (n "bincode") (r "^1.0.0-alpha2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0kp9y3rk2zb9n111vsvnzx30jjyqfm3kmyiar8v3hqjwkpmsvqdy")))

(define-public crate-hopper-0.2.2 (c (n "hopper") (v "0.2.2") (d (list (d (n "bincode") (r "^1.0.0-alpha6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1s8k1dy3kj11752x34swxk9vxd19r24jg5mhl16m9izc33djaq6a")))

(define-public crate-hopper-0.3.0 (c (n "hopper") (v "0.3.0") (d (list (d (n "bincode") (r "^0.8.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0sw3x6py3y4jz1rlaq2pzjsv066swhr5bwznwsdf49hhfhxzl45h")))

(define-public crate-hopper-0.3.1 (c (n "hopper") (v "0.3.1") (d (list (d (n "bincode") (r "^0.8.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0wl5r182kmhv6x0x6f9lnyiphr49ji9fbp4lmcwczlwiah3mkzzr")))

(define-public crate-hopper-0.3.2 (c (n "hopper") (v "0.3.2") (d (list (d (n "bincode") (r "^0.8.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1j9sd72hn35dqvyz5npq29w1ikyavh0lcda57k996hkjlsrhhp9n")))

(define-public crate-hopper-0.3.3 (c (n "hopper") (v "0.3.3") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "zlo") (r "^0.1") (d #t) (k 0)))) (h "088pxr5cbwgzwnvh2xkmnbxy648qkqw8cccbzmp6r6n17mg335rw")))

(define-public crate-hopper-0.3.4 (c (n "hopper") (v "0.3.4") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "072phz86m7qb4z3hzg7d8mv4xpmfzypi5ja2kkfv90cg3niw0vpc")))

(define-public crate-hopper-0.4.0 (c (n "hopper") (v "0.4.0") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2.0") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0j93cv7vmcgaf1pzzrf99pjbhrbriqyhy2cxgrasrpgj5sy0ahvh")))

(define-public crate-hopper-0.4.1 (c (n "hopper") (v "0.4.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2.0") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0vvc7afdq6kd5bg1makklm1ka6z4vf2rkifjmxj1np51d2rsahjs")))

(define-public crate-hopper-0.4.2 (c (n "hopper") (v "0.4.2") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.2.0") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "19ck4jf6yvzz9yhjyfjg5q6c7z7jrna37ma5zlmbni2ywpxkj76s")))

