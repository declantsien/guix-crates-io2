(define-module (crates-io ho me homer) #:use-module (crates-io))

(define-public crate-homer-0.2.0 (c (n "homer") (v "0.2.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "08z5xnvh8y1vqafq7i1d46vdszg24c50jk3dyw4rp9g50a718fc0")))

