(define-module (crates-io ho me homeassistent) #:use-module (crates-io))

(define-public crate-homeassistent-1.0.0-kylep-add-as-mobile-app-SNAPSHOT (c (n "homeassistent") (v "1.0.0-kylep-add-as-mobile-app-SNAPSHOT") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05i7zfvnkcvk9yj1i818jc7b3h79wi03mv6yznfszd976z48z250") (y #t)))

