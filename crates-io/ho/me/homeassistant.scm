(define-module (crates-io ho me homeassistant) #:use-module (crates-io))

(define-public crate-homeassistant-0.1.0 (c (n "homeassistant") (v "0.1.0") (d (list (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "roadrunner") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "1cmkfwayf3ppqvmkvz11cz1ck76wr17snkffcrsi3gbpfmni2j5w")))

(define-public crate-homeassistant-0.1.1 (c (n "homeassistant") (v "0.1.1") (d (list (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "roadrunner") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 0)))) (h "0axkw4lgwyxa4xpfls02jvd6bip3dcv7bpmqbvf56185mvg3aqyq")))

(define-public crate-homeassistant-0.2.0 (c (n "homeassistant") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wkgvyxcm51r9inw7vahl5qf7qw3z8i6c6hr2rl6j4avcf9g7ckl")))

(define-public crate-homeassistant-0.3.0 (c (n "homeassistant") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z2yksswvjy90y6fcpv4f6blj2crb6l7gd172ibvva52pahlk953")))

