(define-module (crates-io ho me home-easy) #:use-module (crates-io))

(define-public crate-home-easy-0.1.0 (c (n "home-easy") (v "0.1.0") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "log4rs") (r "^0.7.0") (d #t) (k 0)) (d (n "rppal") (r "^0.1.3") (d #t) (k 0)) (d (n "wiringpi") (r "^0.2.1") (d #t) (k 0)))) (h "18dd26nn4x1ziyddsw7ka5j0zjwzwrwp72bv05ck5ww6rnlf2x63")))

(define-public crate-home-easy-0.1.1 (c (n "home-easy") (v "0.1.1") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "log4rs") (r "^0.7.0") (d #t) (k 0)) (d (n "rppal") (r "^0.1.3") (d #t) (k 0)) (d (n "wiringpi") (r "^0.2.1") (d #t) (k 0)))) (h "00kwb669q39lhp1s89y51sy9dwva8jk97sl6vhpb1iqr7sjgkd18")))

(define-public crate-home-easy-0.2.0 (c (n "home-easy") (v "0.2.0") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "log4rs") (r "^0.7.0") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "1pc3s8d19c1mqyr7w6hvkxw3ibsalxwhvp5316w8gaq1z3c0wkp9")))

(define-public crate-home-easy-0.2.1 (c (n "home-easy") (v "0.2.1") (d (list (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "log4rs") (r "^0.7.0") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "0n85pjk3zxgjdpxf8gj3c6v3vlray7wnh2nxjmwpdnzrrspfnplr")))

