(define-module (crates-io ho me homegate) #:use-module (crates-io))

(define-public crate-homegate-0.1.0 (c (n "homegate") (v "0.1.0") (d (list (d (n "base64") (r "^0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gyxa2y80dbrvixywn1h6plnjnnbgk4gxkpsfymcksvn8crs9rnf")))

