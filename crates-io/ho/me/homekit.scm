(define-module (crates-io ho me homekit) #:use-module (crates-io))

(define-public crate-homekit-0.0.0 (c (n "homekit") (v "0.0.0") (d (list (d (n "byteorder") (r "^1.3.1") (k 0)) (d (n "insta") (r "^0.7.4") (d #t) (k 2)) (d (n "rubble") (r "^0.0.2") (f (quote ("log"))) (d #t) (k 0)))) (h "0wj7a0hws5a30mdg00h8d0xx5bfpn78kz5bc6y53dd7mvcds21sj")))

