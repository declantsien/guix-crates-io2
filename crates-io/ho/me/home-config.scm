(define-module (crates-io ho me home-config) #:use-module (crates-io))

(define-public crate-home-config-0.1.0 (c (n "home-config") (v "0.1.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "18nqc3wrvrphjsyv63brqhynvplg61s8qd7k1m7w6n09w0zpi5fy") (y #t)))

(define-public crate-home-config-0.2.0 (c (n "home-config") (v "0.2.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "0yjfby5nwbjwwpk3avkg5779dj481ghzzz34lnnh1wqsnmrlkqwn") (y #t)))

(define-public crate-home-config-0.2.1 (c (n "home-config") (v "0.2.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "122i6sdp5dvkms9dmiiapa1rrfafv69x3fpmkv015hgk3a6jng3y") (y #t)))

(define-public crate-home-config-0.2.2 (c (n "home-config") (v "0.2.2") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "0z224hi8psjsvaflvqg3ssw7qv9fhlb2z0pjngm4zj2x3ar4d7pk") (y #t)))

(define-public crate-home-config-0.3.0 (c (n "home-config") (v "0.3.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (o #t) (d #t) (k 0)))) (h "0j6j5sg13k2qsijzfap7xzc9gfhx28dm3alnavcmks4xkckynicn") (f (quote (("yaml" "serde" "serde_yaml") ("json" "serde" "serde_json") ("default")))) (y #t)))

(define-public crate-home-config-0.4.0 (c (n "home-config") (v "0.4.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (o #t) (d #t) (k 0)))) (h "07cqjlc2w3qm784mph35amy8b8jsnf94r6pr6snw5f3crjqdiy97") (f (quote (("default")))) (y #t) (s 2) (e (quote (("yaml" "dep:serde" "dep:serde_yaml") ("toml" "dep:serde" "dep:toml") ("json" "dep:serde" "dep:serde_json"))))))

(define-public crate-home-config-0.5.0 (c (n "home-config") (v "0.5.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.11") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (o #t) (d #t) (k 0)))) (h "1mapcxd3cfzbvi00y6xzhzfakkzixvv1lq0lpcy6l1vasw02c8cc") (f (quote (("default")))) (y #t) (s 2) (e (quote (("yaml" "dep:serde" "dep:serde_yaml") ("toml" "dep:serde" "dep:toml") ("json" "dep:serde" "dep:serde_json"))))))

(define-public crate-home-config-0.6.0 (c (n "home-config") (v "0.6.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "hcl-rs") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.14") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (o #t) (d #t) (k 0)))) (h "1w2cry9rdzabfs42q84k2najfyafilcnaq5ps51zspkbcdf5m0i7") (f (quote (("default")))) (s 2) (e (quote (("yaml" "dep:serde" "dep:serde_yaml") ("toml" "dep:serde" "dep:toml") ("json" "dep:serde" "dep:serde_json") ("hcl" "dep:serde" "dep:hcl-rs"))))))

