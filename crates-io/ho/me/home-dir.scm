(define-module (crates-io ho me home-dir) #:use-module (crates-io))

(define-public crate-home-dir-0.1.0 (c (n "home-dir") (v "0.1.0") (d (list (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04l2b780mhvsq8si89ikcvyj1jh8np8jds2vfxc71b63j2b4yb2p")))

