(define-module (crates-io ho me homelander) #:use-module (crates-io))

(define-public crate-homelander-0.1.0 (c (n "homelander") (v "0.1.0") (h "0dyfyr52rc6wa1xxywf7dm5yp22i9wvb6kvc0swb772fv0hrmws6") (y #t)))

(define-public crate-homelander-0.1.1 (c (n "homelander") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "strum_macros") (r "^0.24.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tracing") (r "^0.1.35") (d #t) (k 0)))) (h "1r9c34j0cll3ygymx6m795cpnsxjsyyz7pi2q7kcj2s3pig796s5")))

