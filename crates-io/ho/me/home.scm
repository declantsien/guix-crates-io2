(define-module (crates-io ho me home) #:use-module (crates-io))

(define-public crate-home-0.1.0 (c (n "home") (v "0.1.0") (d (list (d (n "advapi32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "scopeguard") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "userenv-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "1v1k03f9b9z6j1p62ikfd83bw7dx6wqdgmas55a27qhijjvx20lb")))

(define-public crate-home-0.2.0 (c (n "home") (v "0.2.0") (d (list (d (n "advapi32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "scopeguard") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "userenv-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "02bldrcnm49hvlkfx298j2fyp9l3q2xqlz8bghrzagrkbi84h8na")))

(define-public crate-home-0.3.0 (c (n "home") (v "0.3.0") (d (list (d (n "advapi32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "scopeguard") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "userenv-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)))) (h "1nz63xj1pjhfsm0asyl964klpkyc9syz0ga8igp3z3wx15hsw9cz")))

(define-public crate-home-0.3.2 (c (n "home") (v "0.3.2") (d (list (d (n "scopeguard") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "userenv-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "handleapi" "processthreadsapi" "std" "winerror" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1p74wmjn6vd9l8hw6snk868va6q9n8r8dw4c1jx6w2h0vkxzd54g")))

(define-public crate-home-0.3.3 (c (n "home") (v "0.3.3") (d (list (d (n "scopeguard") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "handleapi" "processthreadsapi" "std" "winerror" "winnt" "userenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1zrs6g12ndq4mm8dlg2m06955q0b055iiadrgxhvrywcnlpzipw0")))

(define-public crate-home-0.3.4 (c (n "home") (v "0.3.4") (d (list (d (n "scopeguard") (r "^0.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "handleapi" "processthreadsapi" "std" "winerror" "winnt" "userenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0207wz5k8qrnhrgwgdl0gwyacc9k2pix31x8aykk2qm7ry82nc19")))

(define-public crate-home-0.4.0 (c (n "home") (v "0.4.0") (d (list (d (n "scopeguard") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "handleapi" "processthreadsapi" "std" "winerror" "winnt" "userenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "180cm5lp043d6j0xf1y3bzjvn7psm87103mxhlladp5fd7776mja") (y #t)))

(define-public crate-home-0.4.1 (c (n "home") (v "0.4.1") (d (list (d (n "scopeguard") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "handleapi" "processthreadsapi" "std" "winerror" "winnt" "userenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0073j0lkp3y0miwfz2y0n4c244c0lq4px2qbxzjnfd567ins5m3j") (y #t)))

(define-public crate-home-0.4.2 (c (n "home") (v "0.4.2") (d (list (d (n "scopeguard") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "handleapi" "processthreadsapi" "std" "winerror" "winnt" "userenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1f70zg31hxw2q2wi8dn3biqy5nbmzf6dslxzsss1n89lj5p4wgh1")))

(define-public crate-home-0.5.0 (c (n "home") (v "0.5.0") (d (list (d (n "scopeguard") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "handleapi" "processthreadsapi" "std" "winerror" "winnt" "userenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1cwmx2h3ibfa6zfxrc8d94p7h1i7xypds83a08zzimkb21g32z60")))

(define-public crate-home-0.5.1 (c (n "home") (v "0.5.1") (d (list (d (n "scopeguard") (r "^1") (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "handleapi" "processthreadsapi" "std" "winerror" "winnt" "userenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1a4wcnadw2sarmisb5bz7gs4qwnijalvbf5gf7kg0wdxyxa3jxd3")))

(define-public crate-home-0.5.2 (c (n "home") (v "0.5.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("shlobj" "std" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0psxn1avxac62p24x8yanap0hniba4ksfaj0kw9l3ksq43mnp8zc") (y #t)))

(define-public crate-home-0.5.3 (c (n "home") (v "0.5.3") (d (list (d (n "winapi") (r "^0.3") (f (quote ("shlobj" "std" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0m3nfcksrj784liji1c5j47dymxw2l7hqy5fj90piadnwvrawmi4")))

(define-public crate-home-0.5.4 (c (n "home") (v "0.5.4") (d (list (d (n "winapi") (r "^0.3") (f (quote ("shlobj" "std" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "023liawrnw7w447cd26jjx9mx6zf0gp2lpxjn1bnvh20njs0jwvl")))

(define-public crate-home-0.5.5 (c (n "home") (v "0.5.5") (d (list (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_UI_Shell"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1nqx1krijvpd03d96avsdyknd12h8hs3xhxwgqghf8v9xxzc4i2l")))

(define-public crate-home-0.5.9 (c (n "home") (v "0.5.9") (d (list (d (n "windows-sys") (r "^0.52") (f (quote ("Win32_Foundation" "Win32_UI_Shell" "Win32_System_Com"))) (d #t) (t "cfg(windows)") (k 0)))) (h "19grxyg35rqfd802pcc9ys1q3lafzlcjcv2pl2s5q8xpyr5kblg3") (r "1.70.0")))

