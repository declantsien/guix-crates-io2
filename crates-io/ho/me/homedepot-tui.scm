(define-module (crates-io ho me homedepot-tui) #:use-module (crates-io))

(define-public crate-homedepot-tui-0.1.0 (c (n "homedepot-tui") (v "0.1.0") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.4") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "15arkkw5k8c30dp77l5lvaym15iia7nbkqs26f6mxnqwsl7nsjs9")))

