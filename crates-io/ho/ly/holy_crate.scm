(define-module (crates-io ho ly holy_crate) #:use-module (crates-io))

(define-public crate-holy_crate-0.1.0 (c (n "holy_crate") (v "0.1.0") (h "080ykh5f853a492bkdih5pcq6c56y4lv5snrxg2m77xvmaqcfc5r") (y #t)))

(define-public crate-holy_crate-0.1.1 (c (n "holy_crate") (v "0.1.1") (h "1d0yx2ghsh3plrv55dqscrrz9qkhgnjdxpyyqf2nxi1yd4k65v9r") (y #t)))

(define-public crate-holy_crate-0.1.2 (c (n "holy_crate") (v "0.1.2") (h "07l6gzahnysnlxqamghkjh4j2pabdi02wjgfinjf2x2ynyx8n5z1") (y #t)))

