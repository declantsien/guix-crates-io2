(define-module (crates-io ho ly holy) #:use-module (crates-io))

(define-public crate-holy-0.1.0 (c (n "holy") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03y36aimhb6p76k2349p0apg9583dv0gnsb4qrnmcj0i7ccw66cw") (r "1.75")))

(define-public crate-holy-0.1.1 (c (n "holy") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.8.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1r85vwy3msn4bnlv81ff2jx24xzb5zsnj4jgr0chn8bvbf9pfbqj") (r "1.75")))

(define-public crate-holy-0.1.2 (c (n "holy") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.8.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11k04a3i56yzds45h270wk1r64vq5m4lnamqpy87pqhp36y871c7") (r "1.75")))

