(define-module (crates-io ho ly holyhashmap) #:use-module (crates-io))

(define-public crate-holyhashmap-0.1.0 (c (n "holyhashmap") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (o #t) (d #t) (k 0)))) (h "0n5zsfknhzmknjc32d6375bxp35g5yvxkprqvzvrwb31wskm1jrf")))

(define-public crate-holyhashmap-0.1.1 (c (n "holyhashmap") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (o #t) (d #t) (k 0)))) (h "0fdzwn1798byd7pl5y94rhhbra3q5970x1mklzfscdmp4cmfi3nj")))

(define-public crate-holyhashmap-0.1.2 (c (n "holyhashmap") (v "0.1.2") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (o #t) (d #t) (k 0)))) (h "1nf9ppzds2l3d610h7frhmasjpxbz97xkzdgp5wr2as1c949ka0y")))

