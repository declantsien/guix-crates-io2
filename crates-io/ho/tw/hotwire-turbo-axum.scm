(define-module (crates-io ho tw hotwire-turbo-axum) #:use-module (crates-io))

(define-public crate-hotwire-turbo-axum-0.1.0 (c (n "hotwire-turbo-axum") (v "0.1.0") (d (list (d (n "axum") (r "^0.6") (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "test-util"))) (d #t) (k 2)) (d (n "tower") (r "^0.4") (f (quote ("util"))) (d #t) (k 2)))) (h "095gk5zxsz3b373jj7j1ahblcd0y1shv2i8j25qrq86gsdh3pxkk")))

(define-public crate-hotwire-turbo-axum-0.1.1 (c (n "hotwire-turbo-axum") (v "0.1.1") (d (list (d (n "axum") (r "^0.7") (k 0)) (d (n "http-body-util") (r "^0.1") (d #t) (k 2)) (d (n "hyper") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "test-util"))) (d #t) (k 2)) (d (n "tower") (r "^0.4") (f (quote ("util"))) (d #t) (k 2)))) (h "1qg4innbjfjlhgd3sggzz447bsibwf52gslhlxn1x39188ninnnp")))

