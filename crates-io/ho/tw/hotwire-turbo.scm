(define-module (crates-io ho tw hotwire-turbo) #:use-module (crates-io))

(define-public crate-hotwire-turbo-0.1.0 (c (n "hotwire-turbo") (v "0.1.0") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)))) (h "0j3zzgp46jg06pr8xvh2n11l4y19hgbyqgdkmqfbdbkqy5mbs6fl")))

(define-public crate-hotwire-turbo-0.1.1 (c (n "hotwire-turbo") (v "0.1.1") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)))) (h "04qil4p596kzwh47rwicg0h5z6qfrv68d0wl1s06gab970a709wp")))

