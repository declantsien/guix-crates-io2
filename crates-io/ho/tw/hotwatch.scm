(define-module (crates-io ho tw hotwatch) #:use-module (crates-io))

(define-public crate-hotwatch-0.1.0 (c (n "hotwatch") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "notify") (r "^2.6") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)))) (h "1hqwjddd85pbddhfq7hgc2ksvnigh5xjbq0jn96licx0zc1i5jxk") (f (quote (("default"))))))

(define-public crate-hotwatch-0.2.0 (c (n "hotwatch") (v "0.2.0") (d (list (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "0793vqxkciq6yh97h5nak4293gk37fz0d21c0nl52b3rxng8116w")))

(define-public crate-hotwatch-0.2.1 (c (n "hotwatch") (v "0.2.1") (d (list (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "03z90z8kszf2721hvxiscc1h054kdknwfs5rri2lj7lr9jd7zqc7")))

(define-public crate-hotwatch-0.3.0 (c (n "hotwatch") (v "0.3.0") (d (list (d (n "derive_more") (r "^0.6") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.4") (d #t) (k 0)))) (h "03swdq1hprdjp3a2v89hld5d2w7frqqkvmrl1nwjcympacfjnc51")))

(define-public crate-hotwatch-0.3.1 (c (n "hotwatch") (v "0.3.1") (d (list (d (n "derive_more") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6") (d #t) (k 0)))) (h "199lrngn71xax45xndszcwxm87l7vh37hdnzifpyd411wvbfm0gv")))

(define-public crate-hotwatch-0.4.0 (c (n "hotwatch") (v "0.4.0") (d (list (d (n "derive_more") (r "^0.14") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)))) (h "1vwqz0k0xjm6r0nj3r334zimvizl4rq49ay0nqp9rdxjrihhsrcj")))

(define-public crate-hotwatch-0.4.1 (c (n "hotwatch") (v "0.4.1") (d (list (d (n "derive_more") (r "^0.14") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)))) (h "04ylsjz725k6294k6hpwdbj4qw2znzm8524wfsiyjal3hp1wrdvg")))

(define-public crate-hotwatch-0.4.2 (c (n "hotwatch") (v "0.4.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1jg07mfkbdpsyds00rxq70r4zysikq28izfv6yhfpy503r7lzl9j")))

(define-public crate-hotwatch-0.4.3 (c (n "hotwatch") (v "0.4.3") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0h67brckc5jc4brygg03n1x8d77p9nj1fih256fd3mww3qwxs2ms")))

(define-public crate-hotwatch-0.4.4 (c (n "hotwatch") (v "0.4.4") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0k929sgnir8cc08814rd2cf9xg3d9bqcl6f9c65vk58j0h6bifbm")))

(define-public crate-hotwatch-0.4.5 (c (n "hotwatch") (v "0.4.5") (d (list (d (n "failure") (r ">=0.1.0, <0.2.0") (d #t) (k 2)) (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "notify") (r ">=4.0.0, <5.0.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 2)))) (h "0c9br2f29js5syvvdj2bpdyjgxjbbhpahq8kfr0pn8vzww1ff7nn")))

(define-public crate-hotwatch-0.4.6 (c (n "hotwatch") (v "0.4.6") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1f2w8jmnxv219c7ya6y7lxddyl4s6yd186vaydsqnygmlrq1cc1r")))

(define-public crate-hotwatch-0.5.0 (c (n "hotwatch") (v "0.5.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^6.0") (d #t) (k 0)) (d (n "notify-debouncer-full") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0ml7cr60cxp3m9aijlj47i683iaprjh5ayf3b3mr4g0nb68j2h4i")))

