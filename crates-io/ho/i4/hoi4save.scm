(define-module (crates-io ho i4 hoi4save) #:use-module (crates-io))

(define-public crate-hoi4save-0.1.0 (c (n "hoi4save") (v "0.1.0") (d (list (d (n "attohttpc") (r "^0.16") (d #t) (k 2)) (d (n "jomini") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 2)))) (h "1rrk6n59sk04nphh3b8n9q5pdlf4y0hbbwzy7rm0c63c8mipw22k")))

(define-public crate-hoi4save-0.1.1 (c (n "hoi4save") (v "0.1.1") (d (list (d (n "attohttpc") (r "^0.16") (d #t) (k 2)) (d (n "jomini") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 2)))) (h "180si0ygipgskasyj9wk3syp6il5xhy6jwaar06yyxyp2y5x2840")))

(define-public crate-hoi4save-0.1.2 (c (n "hoi4save") (v "0.1.2") (d (list (d (n "attohttpc") (r "^0.16") (d #t) (k 2)) (d (n "jomini") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 2)))) (h "15bwb4nxhkz7v770500rrax4l7330jldsvs9znbis9xjy5vk8nm2")))

(define-public crate-hoi4save-0.1.3 (c (n "hoi4save") (v "0.1.3") (d (list (d (n "attohttpc") (r "^0.16") (d #t) (k 2)) (d (n "jomini") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 2)))) (h "1zf1hvi0kmvk0cwqwwgjfshnc40xqzmzv2h5n1ihx3792bwk5psf")))

(define-public crate-hoi4save-0.1.4 (c (n "hoi4save") (v "0.1.4") (d (list (d (n "attohttpc") (r "^0.17") (d #t) (k 2)) (d (n "jomini") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 2)))) (h "0whlsqz2vp9a96klahm4mh2ag6ssfmp17sdwams3p6alrrl6ia46")))

(define-public crate-hoi4save-0.1.5 (c (n "hoi4save") (v "0.1.5") (d (list (d (n "attohttpc") (r "^0.17") (d #t) (k 2)) (d (n "jomini") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 2)))) (h "0zvdmm8vj65pk19f6j5sraljijclzfvna7n14mq633l7l6bgfahy")))

(define-public crate-hoi4save-0.1.6 (c (n "hoi4save") (v "0.1.6") (d (list (d (n "attohttpc") (r "^0.17") (d #t) (k 2)) (d (n "jomini") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 2)))) (h "0znmzx43isgmx5kdggjjcvp60kkk55j4kv6ac62hrwr759a8ix41")))

(define-public crate-hoi4save-0.1.7 (c (n "hoi4save") (v "0.1.7") (d (list (d (n "attohttpc") (r "^0.18") (d #t) (k 2)) (d (n "jomini") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 2)))) (h "1ws77a2cn8v20xf6b7b9cviphzbl5xvxq6w6z42wq4qhnxywsgxq")))

(define-public crate-hoi4save-0.1.8 (c (n "hoi4save") (v "0.1.8") (d (list (d (n "attohttpc") (r "^0.18") (d #t) (k 2)) (d (n "jomini") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 2)))) (h "0a22cyq2ywkz0bsdh1d96sqpzr403m12189khzk88w2lm8gqczr6")))

(define-public crate-hoi4save-0.1.9 (c (n "hoi4save") (v "0.1.9") (d (list (d (n "attohttpc") (r "^0.18") (d #t) (k 2)) (d (n "jomini") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "^0.5") (f (quote ("deflate"))) (k 2)))) (h "07zszka8xjkdmwaf0xdprc2zx5vlg56a6kavsphqs9k8nqd9q3g1")))

(define-public crate-hoi4save-0.2.0 (c (n "hoi4save") (v "0.2.0") (d (list (d (n "attohttpc") (r "^0.19") (d #t) (k 2)) (d (n "jomini") (r "^0.19") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate"))) (k 2)))) (h "15b022f790jkmfhvljl9vqqd2xnxvdqxg4mbpx8y6na5s7v01235")))

(define-public crate-hoi4save-0.2.1 (c (n "hoi4save") (v "0.2.1") (d (list (d (n "attohttpc") (r "^0.19") (d #t) (k 2)) (d (n "jomini") (r "^0.19") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate"))) (k 2)))) (h "1v2f0kp72balzfhphykrxksm9af02bgql223zrf46kbz3bqfaasw")))

(define-public crate-hoi4save-0.3.0 (c (n "hoi4save") (v "0.3.0") (d (list (d (n "attohttpc") (r "^0.19") (d #t) (k 2)) (d (n "jomini") (r "^0.19") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate"))) (k 2)))) (h "0k0w998dfsn8gbb2ipgdg2546m5xsp5za41qq1656kylkg80z7v1")))

(define-public crate-hoi4save-0.3.1 (c (n "hoi4save") (v "0.3.1") (d (list (d (n "attohttpc") (r "^0.19") (d #t) (k 2)) (d (n "jomini") (r "^0.19") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate"))) (k 2)))) (h "0cvl57kvh5y4j05hf51yxrnl2isnchvizpkcxdpcacp8xfqjgjdh")))

(define-public crate-hoi4save-0.3.2 (c (n "hoi4save") (v "0.3.2") (d (list (d (n "attohttpc") (r "^0.19") (d #t) (k 2)) (d (n "jomini") (r "^0.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate"))) (k 2)))) (h "0n6sbvm7a260k515ayl63gjqbhmyxwx67q9ibhwlvpri6iqm20qi")))

(define-public crate-hoi4save-0.3.3 (c (n "hoi4save") (v "0.3.3") (d (list (d (n "attohttpc") (r "^0.24") (d #t) (k 2)) (d (n "jomini") (r "^0.21") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate"))) (k 2)))) (h "1p4xlylfcp6lrgxa8hk5qij21nj72xmp3axhd6m31cch72x0c35j")))

(define-public crate-hoi4save-0.3.4 (c (n "hoi4save") (v "0.3.4") (d (list (d (n "attohttpc") (r "^0.24") (d #t) (k 2)) (d (n "jomini") (r "^0.21") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zip") (r "^0.6") (f (quote ("deflate"))) (k 2)))) (h "19hrya4cgcprhqyhjsm06qfbbjwc2pr8jkh1598j7gp7bh97hq5z")))

