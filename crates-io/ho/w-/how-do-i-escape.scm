(define-module (crates-io ho w- how-do-i-escape) #:use-module (crates-io))

(define-public crate-how-do-i-escape-0.1.0 (c (n "how-do-i-escape") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6.80") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0f7mknh0ysadvbj3mwmd8088dm3q9fnjd84swgc82x269fc3whi6")))

(define-public crate-how-do-i-escape-0.2.0 (c (n "how-do-i-escape") (v "0.2.0") (d (list (d (n "docopt") (r "^0.6.80") (d #t) (k 0)) (d (n "entities") (r "^0.3.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0z9wrq7rnhbfagnc023bg93l8vbq2zicxbi1001gahk791iy0xrs")))

(define-public crate-how-do-i-escape-0.3.0 (c (n "how-do-i-escape") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "docopt") (r "^0.6.80") (d #t) (k 0)) (d (n "entities") (r "^1.0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "1v805xs2lwklyxn5ammzwp4s8gvym1s051ypn4iml0qxx6y791cy")))

