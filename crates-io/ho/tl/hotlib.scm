(define-module (crates-io ho tl hotlib) #:use-module (crates-io))

(define-public crate-hotlib-0.1.0 (c (n "hotlib") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "humantime") (r "^1.3") (d #t) (k 0)) (d (n "libloading") (r "^0.6.2") (d #t) (k 0)) (d (n "notify") (r "=5.0.0-pre.1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "slug") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0pbrwb1abmdg8fkznn499kmp4yblmwys9f40i89rsd357f1p2i76")))

