(define-module (crates-io ho li holidays_se) #:use-module (crates-io))

(define-public crate-holidays_se-0.1.0 (c (n "holidays_se") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)))) (h "0sfx4alw9vbglfljqzsi4bs98bh1kn11ms0jgyxvpbq7pvpjxx3w")))

(define-public crate-holidays_se-0.2.2 (c (n "holidays_se") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)))) (h "0swgywfqcggkjh21cigijm5g81ga8gpc53rw796big5f4mimqlcv")))

(define-public crate-holidays_se-0.3.0 (c (n "holidays_se") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)))) (h "1whqawjs3hav7b1dx3h2d7y7kaxyydgh9r67mcrg18pay8a9pls9")))

(define-public crate-holidays_se-0.3.1 (c (n "holidays_se") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)))) (h "1z019vv278nr7fmn4075a169ff85cxpcjfdjwllk87bl759h8kmc")))

(define-public crate-holidays_se-1.0.0 (c (n "holidays_se") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1w3zd64zh215dk8if3hsa64wsfvbx4vl9grkljm57xf87mxhl800") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

