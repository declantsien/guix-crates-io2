(define-module (crates-io ho li holiday_jp) #:use-module (crates-io))

(define-public crate-holiday_jp-0.1.0 (c (n "holiday_jp") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1wqmnw3cwll36zbvwmhblv4rbs8s93k70mr1idd8srm6p1gyphc0")))

(define-public crate-holiday_jp-0.1.1 (c (n "holiday_jp") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "12097azxqfal1fcyljic1mp1c4vbp0x3nc5wl90fkgj7y56kz92j")))

(define-public crate-holiday_jp-0.1.2 (c (n "holiday_jp") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1jprblas550czcskkcamr3kr9vayap1frgd29xlw3cbakdl1ykwh")))

