(define-module (crates-io ho li holium-macro) #:use-module (crates-io))

(define-public crate-holium-macro-1.0.0-alpha (c (n "holium-macro") (v "1.0.0-alpha") (d (list (d (n "holium-macro-support") (r "^1.0.0-alpha") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "=1.0.49") (d #t) (k 2)))) (h "041cbnk8vqir1kwg9sccfjwl0i3ahw52jmyi4hgj2rf3xckcyxyd")))

(define-public crate-holium-macro-1.0.0-alpha.1 (c (n "holium-macro") (v "1.0.0-alpha.1") (d (list (d (n "holium-macro-support") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (d #t) (k 2)))) (h "0la8i0hg8pbd2mgjqag2i2k24hv2h4bj8s5kldjg0m27rslwyza8")))

(define-public crate-holium-macro-1.0.0-alpha.3 (c (n "holium-macro") (v "1.0.0-alpha.3") (d (list (d (n "holium-macro-support") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (d #t) (k 2)))) (h "1pp4zrxmh57vm3wvvrhsjclbykddjdy0h0y8hynvpf6c2y4ip3v7")))

(define-public crate-holium-macro-1.0.0 (c (n "holium-macro") (v "1.0.0") (d (list (d (n "holium-macro-support") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (d #t) (k 2)))) (h "1xy8fick7hm21ivz303xwgijz4l0fn88lgnpc7xl40by0ibw3gbx")))

