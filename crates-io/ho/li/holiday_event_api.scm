(define-module (crates-io ho li holiday_event_api) #:use-module (crates-io))

(define-public crate-holiday_event_api-0.1.0 (c (n "holiday_event_api") (v "0.1.0") (d (list (d (n "mockito") (r "^1") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "rustc_version_runtime") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1d9gl13r0bnd5y3ngh9pjllvydvab20ii1dimp2s5pawqwn4wyba")))

(define-public crate-holiday_event_api-1.0.0 (c (n "holiday_event_api") (v "1.0.0") (d (list (d (n "mockito") (r "^1") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "rustc_version_runtime") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1259736ybh71xw0v256wavkyd6774ik8xmwpc5r3r848h7p0dklk")))

(define-public crate-holiday_event_api-1.2.0 (c (n "holiday_event_api") (v "1.2.0") (d (list (d (n "mockito") (r "^1") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "rustc_version_runtime") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0hp0zpb92pr70kr805r6z65037dnvl179z1z00q9asxi552kgxww")))

