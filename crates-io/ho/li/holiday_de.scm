(define-module (crates-io ho li holiday_de) #:use-module (crates-io))

(define-public crate-holiday_de-0.1.0 (c (n "holiday_de") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "computus") (r "^1.0.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)))) (h "0gi9yyrs7xldghypjv5xyd22j4saggscq0vx64ipi3w1qms4d4xl")))

(define-public crate-holiday_de-0.1.1 (c (n "holiday_de") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "computus") (r "^1.0.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)))) (h "0p8jqp3rnz6h0k5hp0783ywixzjcwkimp7lm9xa8im8r57xgn41x")))

