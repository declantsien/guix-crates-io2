(define-module (crates-io ho li holib) #:use-module (crates-io))

(define-public crate-HoLib-0.1.0 (c (n "HoLib") (v "0.1.0") (h "01b3vswbmkvpw0yriblx00irkkwavvw9frx4wn7kz8rb458wijbs")))

(define-public crate-HoLib-0.1.1 (c (n "HoLib") (v "0.1.1") (h "0ssjbfkwgfj1w8kiff3fkwxbn00qwnqi7z1gb1h3349swclj956y")))

(define-public crate-HoLib-0.1.2 (c (n "HoLib") (v "0.1.2") (h "0bhgyb9gngx4cvw37fr0nfn5xblaprshwrv80wm0xhi09sscp3ny")))

