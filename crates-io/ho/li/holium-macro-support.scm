(define-module (crates-io ho li holium-macro-support) #:use-module (crates-io))

(define-public crate-holium-macro-support-1.0.0-alpha (c (n "holium-macro-support") (v "1.0.0-alpha") (d (list (d (n "holium-backend") (r "^1.0.0-alpha") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("visit" "full"))) (d #t) (k 0)))) (h "0wfq1mcql0m5822wkk1fabc14219v6a3bdr20p7nd06hddrmkbiv") (f (quote (("strict-macro") ("extra-traits" "syn/extra-traits"))))))

(define-public crate-holium-macro-support-1.0.0-alpha.1 (c (n "holium-macro-support") (v "1.0.0-alpha.1") (d (list (d (n "holium-backend") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("visit" "full"))) (d #t) (k 0)))) (h "02mfcj2g35zxf3g8mky7m2jzd10k8620pz0vzhix0vf2pbwdjbc7") (f (quote (("strict-macro") ("extra-traits" "syn/extra-traits"))))))

(define-public crate-holium-macro-support-1.0.0-alpha.3 (c (n "holium-macro-support") (v "1.0.0-alpha.3") (d (list (d (n "holium-backend") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("visit" "full"))) (d #t) (k 0)))) (h "1qjiws1vmkckiv3cziqp0dxv3ip7abh92pq449qdfl3jmx3qrlfc") (f (quote (("strict-macro") ("extra-traits" "syn/extra-traits"))))))

(define-public crate-holium-macro-support-1.0.0 (c (n "holium-macro-support") (v "1.0.0") (d (list (d (n "holium-backend") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("visit" "full"))) (d #t) (k 0)))) (h "1l7ir1zv1asfvbk74dw31slfwglfzwlax1brxjhzr3zhh142dg80") (f (quote (("strict-macro") ("extra-traits" "syn/extra-traits"))))))

