(define-module (crates-io ho li holium-rs-sdk) #:use-module (crates-io))

(define-public crate-holium-rs-sdk-1.0.0-alpha (c (n "holium-rs-sdk") (v "1.0.0-alpha") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "holium-macro") (r "^1.0.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "10vx2m41wfq5sgypns1hxrlz6km5spb654drm7fkjcbasrj4zhkp")))

(define-public crate-holium-rs-sdk-1.0.0-alpha.1 (c (n "holium-rs-sdk") (v "1.0.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "holium-macro") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0lvjwlhpdvv4f8fs6zdif1l4z9zc7ww4c22nnzzmplvzyciw86nr")))

(define-public crate-holium-rs-sdk-1.0.0-alpha.3 (c (n "holium-rs-sdk") (v "1.0.0-alpha.3") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "holium-macro") (r "^1.0.0-alpha.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0b6r8gmx5kbh9j3bkvjbb3axqjm19rv1sjf9kiv8pgnm83y5qfhb")))

(define-public crate-holium-rs-sdk-1.0.0 (c (n "holium-rs-sdk") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "holium-macro") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1fblavc2ixb2wylrzmzxnydsb44fhrd6g440kq63ig45qg3knk8r")))

