(define-module (crates-io ho ld holdem) #:use-module (crates-io))

(define-public crate-holdem-0.1.0 (c (n "holdem") (v "0.1.0") (d (list (d (n "cards") (r "^1.1.0") (d #t) (k 0)))) (h "0bsplj0q69wc1k65bbgcjavvr4m2w8bh6x4rlcrzc31q9qz5zc03")))

(define-public crate-holdem-0.1.1 (c (n "holdem") (v "0.1.1") (d (list (d (n "cards") (r "^1.1.1") (d #t) (k 0)))) (h "13sxy6s2b76jvpq61pb7pwyklzzab1i36zk4kcggjffch2q8mwc6")))

(define-public crate-holdem-0.1.2 (c (n "holdem") (v "0.1.2") (d (list (d (n "cards") (r "^1.1.2") (d #t) (k 0)))) (h "1s6cx1q0h5ghiy17cv6r2i0f48bsid5zrf85868il5qasqm88qqq")))

