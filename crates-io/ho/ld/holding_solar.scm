(define-module (crates-io ho ld holding_solar) #:use-module (crates-io))

(define-public crate-holding_solar-0.1.0 (c (n "holding_solar") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "holding_color") (r "^0.1") (d #t) (k 0)) (d (n "holding_kronos") (r "^0.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.19.2") (d #t) (k 0)) (d (n "strum_macros") (r "^0.19.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0wp1j312qszbmqbdx4q6kn1ahgfhx31881gxfchfpx9rprydwjl6")))

