(define-module (crates-io ho ld hold_my_beer) #:use-module (crates-io))

(define-public crate-hold_my_beer-0.1.0 (c (n "hold_my_beer") (v "0.1.0") (h "0f1c7j4prm1xi9pfx292kg0jvczv5fz80hr4ixyx0h6kp2sgw0sc")))

(define-public crate-hold_my_beer-0.1.1 (c (n "hold_my_beer") (v "0.1.1") (h "1ard4r5wn6dzmpq755iz45rnd5mk4d4fv808aa8kyq8dd8n30n7k")))

(define-public crate-hold_my_beer-0.2.0 (c (n "hold_my_beer") (v "0.2.0") (h "0q8mb6vgdhpdv81x1gi5v5f5xmm5cx0514x5bbv5sxi65xjhq0av")))

