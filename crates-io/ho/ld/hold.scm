(define-module (crates-io ho ld hold) #:use-module (crates-io))

(define-public crate-hold-0.1.0-placeholder (c (n "hold") (v "0.1.0-placeholder") (h "122qc8s034vy7n5zpf1kqg8yfkj34xdbx0jplxvfgclsiy7qs3ni") (y #t)))

(define-public crate-hold-0.1.0-alpha.1 (c (n "hold") (v "0.1.0-alpha.1") (d (list (d (n "async-trait") (r "^0.1.30") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "1a6irh66hi8asj8nin5mk4jrs57b0xkmv3svjhv8x0i5cgcyzx17")))

(define-public crate-hold-0.1.0-alpha.2 (c (n "hold") (v "0.1.0-alpha.2") (d (list (d (n "async-trait") (r "^0.1.30") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "1g5s5fvzhd3wgnna0lbfs0fvaf38q18diap49q0ybhp6vn8x53q3")))

(define-public crate-hold-0.1.0-alpha.3 (c (n "hold") (v "0.1.0-alpha.3") (d (list (d (n "async-trait") (r "^0.1.30") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "13hdvcd8jhr76hi02p50p5ra9irlspncf88n4jjfdnw6a2qnsp7y")))

(define-public crate-hold-0.1.0-alpha.4 (c (n "hold") (v "0.1.0-alpha.4") (d (list (d (n "async-trait") (r "^0.1.30") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "snafu") (r "^0.6.8") (d #t) (k 0)))) (h "09plil9jxhwcs64yxmch98lh9ndvqkb8x1k38yyzbpx1nzv7kzmc")))

(define-public crate-hold-0.1.0-alpha.5 (c (n "hold") (v "0.1.0-alpha.5") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "19pxaid0xjprav164n2h91n9g1gjc1i0hygpaif6v5ilqbplrlw6")))

(define-public crate-hold-0.1.0-alpha.6 (c (n "hold") (v "0.1.0-alpha.6") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "03yys6m8l3hj3n7a2q6g8w4ip3qvnql9sv6cm2lpzqibm6fc1ml0") (y #t)))

