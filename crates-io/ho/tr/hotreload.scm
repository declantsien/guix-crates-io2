(define-module (crates-io ho tr hotreload) #:use-module (crates-io))

(define-public crate-hotreload-0.1.0 (c (n "hotreload") (v "0.1.0") (d (list (d (n "notify") (r "^6.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.0") (d #t) (k 0)))) (h "1iggydqmh625cf5xx5qhq8ldjacq9xs7s9hpcmsmr6dbkp7h0gpc")))

