(define-module (crates-io ho km hokmah-uci) #:use-module (crates-io))

(define-public crate-hokmah-uci-0.1.0 (c (n "hokmah-uci") (v "0.1.0") (h "1bbd0c11jl3gzr4vqgcbbhx7vak7rlss1ggg9vdh9s1vqn87bw96")))

(define-public crate-hokmah-uci-0.1.1 (c (n "hokmah-uci") (v "0.1.1") (h "1kp4czx901ilqq5xn2m71ws745lxzlfxnbscimh0jqqiv6bhan2n")))

