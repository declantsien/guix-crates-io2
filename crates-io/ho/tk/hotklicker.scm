(define-module (crates-io ho tk hotklicker) #:use-module (crates-io))

(define-public crate-hotklicker-0.1.0 (c (n "hotklicker") (v "0.1.0") (d (list (d (n "enigo") (r "^0.1.2") (d #t) (k 0)) (d (n "inputbot") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "162gbrja5n9wsyd74f9w01b46w8cbqgzwffmgfzkiwv4kyy25lzj")))

