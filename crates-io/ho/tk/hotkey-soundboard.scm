(define-module (crates-io ho tk hotkey-soundboard) #:use-module (crates-io))

(define-public crate-hotkey-soundboard-0.0.1 (c (n "hotkey-soundboard") (v "0.0.1") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(target_os = \"macos\")") (k 1)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "x11-dl") (r "^2.18") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "03nnsj5l8xsxdqfx3k96gkyf0a939scf4j6dvw89n9swyqix9x3h")))

(define-public crate-hotkey-soundboard-0.0.3 (c (n "hotkey-soundboard") (v "0.0.3") (d (list (d (n "cc") (r "^1") (d #t) (t "cfg(target_os = \"macos\")") (k 1)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "x11-dl") (r "^2.18") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "16lmi0pasp73rnqirxb6ylraa2dvlxlp44r5v3x6ya689ijh18ja")))

