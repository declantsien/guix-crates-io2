(define-module (crates-io ho tk hotkey) #:use-module (crates-io))

(define-public crate-hotkey-0.1.0 (c (n "hotkey") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0fc4696px2pi3ijn6afawzg9db6gqcn3w4c0cl0y9gn6kskawbfj")))

(define-public crate-hotkey-0.2.0 (c (n "hotkey") (v "0.2.0") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "x11-dl") (r "^2.18.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "04qzrjmn7h2x31j7m09vv4w67c51mg9ns57yxga0jp3dgh35wzvm")))

(define-public crate-hotkey-0.3.0 (c (n "hotkey") (v "0.3.0") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "x11-dl") (r "^2.18.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "04n96xsz6kfmznpig0jpvaqcs33xb3iwmy1mi79gdnw6c9wcfjx1")))

(define-public crate-hotkey-0.3.1 (c (n "hotkey") (v "0.3.1") (d (list (d (n "winapi") (r "^0.3.8") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "x11-dl") (r "^2.18.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1j6mpq9dvv38n67dv4zmanqrzhs3ahccgdv7qskcx0rynfkqrvb8")))

