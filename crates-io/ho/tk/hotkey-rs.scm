(define-module (crates-io ho tk hotkey-rs) #:use-module (crates-io))

(define-public crate-hotkey-rs-0.1.0 (c (n "hotkey-rs") (v "0.1.0") (h "0n7r0nwn22jz62diq55mbshmbsiss1kkf27366r3344x9f5m0b4n") (y #t)))

(define-public crate-hotkey-rs-0.1.1 (c (n "hotkey-rs") (v "0.1.1") (h "16h65vyvjiwjddgrlvdy2pqj9m47p54hafrm44ypm3n6ws3l7q6b") (y #t)))

(define-public crate-hotkey-rs-0.1.2 (c (n "hotkey-rs") (v "0.1.2") (h "0x2yq4fpgfyrx12fxgah7ks8alvnblfjb5msa8i3jfp6dc5cj3rm")))

