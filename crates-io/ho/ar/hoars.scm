(define-module (crates-io ho ar hoars) #:use-module (crates-io))

(define-public crate-hoars-0.1.0 (c (n "hoars") (v "0.1.0") (d (list (d (n "ariadne") (r "^0.1.5") (d #t) (k 0)) (d (n "chumsky") (r "^0.9.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "1kgal37wl62fajim7rgjmmpxq36cs90j4bg1hdxx1qm08rn53mis")))

(define-public crate-hoars-0.1.1 (c (n "hoars") (v "0.1.1") (d (list (d (n "ariadne") (r "^0.2") (d #t) (k 0)) (d (n "chumsky") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "1c1rgw98iz1b970wi10cyqmv894icyyfihx2b10lqj57gvfaicz1")))

(define-public crate-hoars-0.2.0 (c (n "hoars") (v "0.2.0") (d (list (d (n "ariadne") (r "^0.4") (d #t) (k 0)) (d (n "biodivine-lib-bdd") (r "^0.5") (d #t) (k 0)) (d (n "chumsky") (r "^0.9") (f (quote ("ahash" "std"))) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "0kx32aslhm4bxd6kgafi8wnaksmgvppd3brpfx1d4z2ljlnclycb")))

