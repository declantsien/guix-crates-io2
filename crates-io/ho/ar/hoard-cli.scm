(define-module (crates-io ho ar hoard-cli) #:use-module (crates-io))

(define-public crate-hoard-cli-0.0.0 (c (n "hoard-cli") (v "0.0.0") (h "1g3gpdi12rgw049qwm20z969awbrwmzwzicp3wy6qq2vc4ix0x23") (f (quote (("default")))) (r "1.57")))

(define-public crate-hoard-cli-0.0.1 (c (n "hoard-cli") (v "0.0.1") (h "08x4sqjy2s1z8arfn2lqcxbbgyd24kk6f8fciibicl8shyz11mbd")))

