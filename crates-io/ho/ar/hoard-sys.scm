(define-module (crates-io ho ar hoard-sys) #:use-module (crates-io))

(define-public crate-hoard-sys-0.1.0 (c (n "hoard-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.82") (d #t) (k 0)))) (h "11clswzd4fbinliwcc22m4lkg5pdvp0586a779i5kn480pnw81fj")))

(define-public crate-hoard-sys-0.1.1 (c (n "hoard-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "uname") (r "^0.1.1") (d #t) (k 1)))) (h "1nc0v7ng81z5yc0p1kl2mg9ssy1shpc1lbypa09ni4nr2bnxsphy")))

(define-public crate-hoard-sys-0.1.2 (c (n "hoard-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "uname") (r "^0.1.1") (d #t) (k 1)))) (h "0jv551vsnlw7ism0lyskpphi78x7ag8kyjvm0n1h3skjsws53z32")))

