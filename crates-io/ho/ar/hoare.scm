(define-module (crates-io ho ar hoare) #:use-module (crates-io))

(define-public crate-hoare-0.1.0 (c (n "hoare") (v "0.1.0") (h "0s9gx0zmfmiy9l3akgkgbasq68g3gd79hwjmc3l65sbpac1xqmc9")))

(define-public crate-hoare-0.1.1 (c (n "hoare") (v "0.1.1") (h "0hlcs66fja3nbll887ryk5iificzpab6pcc9jbn9vn2n7324fddl")))

