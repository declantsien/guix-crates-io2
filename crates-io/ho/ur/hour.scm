(define-module (crates-io ho ur hour) #:use-module (crates-io))

(define-public crate-hour-0.0.0 (c (n "hour") (v "0.0.0") (h "1wi3yv8bsnd435i9hqr0x1yms5sg0jlf824y4g2jy8cjpcnkh5cx")))

(define-public crate-hour-0.1.0 (c (n "hour") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "0gcinn2nrxcgk18pvczzvdi2zcj9xf22jpc5cssmvd51sc5b15zx") (f (quote (("with-num-traits" "num-traits") ("with-chrono" "chrono") ("default" "with-chrono"))))))

