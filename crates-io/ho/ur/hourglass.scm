(define-module (crates-io ho ur hourglass) #:use-module (crates-io))

(define-public crate-hourglass-0.4.2 (c (n "hourglass") (v "0.4.2") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)))) (h "1agpgkkrxg28kn5gyap7gaffz49dm4ykr7nqigk21rm7jindjcgg")))

(define-public crate-hourglass-0.4.3 (c (n "hourglass") (v "0.4.3") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)))) (h "0q5340yg4i3vnhysn16yfivama4fazc8fcdpns4n5cr28niisijs")))

(define-public crate-hourglass-0.4.4 (c (n "hourglass") (v "0.4.4") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)))) (h "00dhxhaxl4kwp8wzw7cy6cb3fhwk4q16ynjjxj71gpds17lm1qyb")))

(define-public crate-hourglass-0.5.0 (c (n "hourglass") (v "0.5.0") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1gph4p4y72rcm8silbymbfjfkrqgx9cavd829ff2zc17vqn6ha4l")))

(define-public crate-hourglass-0.6.0 (c (n "hourglass") (v "0.6.0") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)) (d (n "nom") (r "^1.1.0") (d #t) (k 0)))) (h "1q29n3jljg8869giblqyb3m0snm6l3s9p8h5i35a0995lzs337qm")))

(define-public crate-hourglass-0.6.1 (c (n "hourglass") (v "0.6.1") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)) (d (n "nom") (r "^1.1.0") (d #t) (k 0)))) (h "15wnpfra9drgajc8dlh8sr8qjn9xbz8jdfx693qv4i160b6kb3z8")))

(define-public crate-hourglass-0.6.2 (c (n "hourglass") (v "0.6.2") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)) (d (n "nom") (r "^1.1.0") (d #t) (k 0)))) (h "09l0lm95gi80239b0j8my22sh45lqjx88mdci1d68hjkp9p78a6f")))

(define-public crate-hourglass-0.6.3 (c (n "hourglass") (v "0.6.3") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)) (d (n "nom") (r "^1.1.0") (d #t) (k 0)))) (h "0avgb7ljgdxj69dvxsy6y9x5fxv2r4xrfakkvlygr6hlhsxbb0na")))

(define-public crate-hourglass-0.6.4 (c (n "hourglass") (v "0.6.4") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)) (d (n "nom") (r "^1.1.0") (d #t) (k 0)))) (h "1rw44df0gx0avgp1bw6f1gs8y2yj4mdzxw6sgf36xhpind48d626")))

(define-public crate-hourglass-0.6.5 (c (n "hourglass") (v "0.6.5") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)) (d (n "nom") (r "^1.1.0") (d #t) (k 0)))) (h "0y3mqajbaf43lj3r6iwlbdkxgrasja5l3s393cixkf5md4wixdw8")))

(define-public crate-hourglass-0.7.0 (c (n "hourglass") (v "0.7.0") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)) (d (n "nom") (r "^1.1.0") (d #t) (k 0)))) (h "0jxb4pxqb8dx7prra9i4r5v5hdhiaw4f02hxmfhs55kr94z0fizr")))

(define-public crate-hourglass-0.7.1 (c (n "hourglass") (v "0.7.1") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)) (d (n "nom") (r "^1.1.0") (d #t) (k 0)))) (h "05k6ps7x15g8637znvyc4ki4600vgy5x869q8a5z45wbbny0i6kx")))

(define-public crate-hourglass-0.8.0 (c (n "hourglass") (v "0.8.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)) (d (n "nom") (r "^1.1.0") (d #t) (k 0)))) (h "01vgm1w4zl7ia4v1v65hyhayv3vwzvsr7mhzdxhpxldigrqv76iw")))

