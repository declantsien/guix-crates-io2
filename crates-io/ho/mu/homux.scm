(define-module (crates-io ho mu homux) #:use-module (crates-io))

(define-public crate-homux-0.1.1 (c (n "homux") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "matchpick") (r "^0.1.1") (d #t) (k 0)))) (h "03jv5l794302ayq02y266qj8kv2wjanlqaqaz2mlvs26yny8zar5")))

(define-public crate-homux-0.1.2 (c (n "homux") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "matchpick") (r "^0.1.1") (d #t) (k 0)))) (h "0vy2vhfsa2hcndw9qfdscv7mq807dq48xjhq4s7qdg79iwzgg8v1")))

(define-public crate-homux-0.2.0 (c (n "homux") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "matchpick") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "105y70aaklhjk10di7ihriqmdar7p83g9jpih0acibdzv24k791y")))

