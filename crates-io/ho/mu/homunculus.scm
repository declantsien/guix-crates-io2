(define-module (crates-io ho mu homunculus) #:use-module (crates-io))

(define-public crate-homunculus-0.1.0 (c (n "homunculus") (v "0.1.0") (d (list (d (n "glam") (r "^0.20") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "1xcgakjkzgw6byc0v6ywvq0f0i04ljyyg8gjd6ip4maybl1x6w6y")))

(define-public crate-homunculus-0.2.0 (c (n "homunculus") (v "0.2.0") (d (list (d (n "glam") (r "^0.20") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ji1jbhvwj2xswi5fwckxzpyhy9hkv21ah08120gz0sdi6sj6xxv")))

(define-public crate-homunculus-0.3.0 (c (n "homunculus") (v "0.3.0") (d (list (d (n "glam") (r "^0.20") (f (quote ("serde"))) (d #t) (k 0)) (d (n "muon-rs") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0k14y2nlgf8fr9078cam31hfvl2j6vfgqz6rmwfrcdpwngqi6hla")))

(define-public crate-homunculus-0.4.0 (c (n "homunculus") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "fastrand") (r "^2") (d #t) (k 2)) (d (n "glam") (r "^0.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1xp3li4vy4b43ar4b088mqiibmcbz6vf4lw6fxzb0fx2dik2pp3n")))

(define-public crate-homunculus-0.5.0 (c (n "homunculus") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "argh") (r "^0.1") (d #t) (k 2)) (d (n "fastrand") (r "^2") (d #t) (k 2)) (d (n "glam") (r "^0.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0d0wb5k70h782942sdp4nhg459w8vjcyss89zkfccqv0a4qc91cy")))

