(define-module (crates-io ho pc hopcroft-karp) #:use-module (crates-io))

(define-public crate-hopcroft-karp-0.1.0 (c (n "hopcroft-karp") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "0qnfa5zn7g2r7cxa03b17j52x12j1hj47afwg57vb65gjba8krw3")))

(define-public crate-hopcroft-karp-0.1.1 (c (n "hopcroft-karp") (v "0.1.1") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "1pvnsmvgk721mr8i0b7b3zm2xd32flgwhnzxnp3qmmbssdcpq4q5")))

(define-public crate-hopcroft-karp-0.2.0 (c (n "hopcroft-karp") (v "0.2.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "04jh86jh9fhkhv177xi2vm4c8v5806fa9nx20zvsgz81g0m0v6xb")))

(define-public crate-hopcroft-karp-0.2.1 (c (n "hopcroft-karp") (v "0.2.1") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "0bknn9622mx2aw73jvz3haw4wkkmvav08df2s7snyxinm6dksbx7")))

