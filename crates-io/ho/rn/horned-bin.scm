(define-module (crates-io ho rn horned-bin) #:use-module (crates-io))

(define-public crate-horned-bin-0.14.0 (c (n "horned-bin") (v "0.14.0") (d (list (d (n "clap") (r "^3.2.2") (d #t) (k 0)) (d (n "horned-owl") (r "^0.14.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "pretty_rdf") (r "^0.2.0") (d #t) (k 0)) (d (n "rio_api") (r "^0.7.1") (d #t) (k 0)) (d (n "rio_xml") (r "^0.7.3") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.2") (d #t) (k 2)) (d (n "predicates") (r "^2.1.0") (d #t) (k 2)))) (h "06vs1pf0r4rc1ar0y0g9fi1m6s93lx9gpmcn29fiwhbps67nx0v5")))

