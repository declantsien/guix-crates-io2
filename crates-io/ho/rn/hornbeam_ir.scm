(define-module (crates-io ho rn hornbeam_ir) #:use-module (crates-io))

(define-public crate-hornbeam_ir-0.0.1 (c (n "hornbeam_ir") (v "0.0.1") (d (list (d (n "hornbeam_grammar") (r "^0.0.1") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "insta") (r "^1.28.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1b74glcf7cvmgbcarclcla9rp6gci1cxfhi2wc3834w16vimx0xs")))

(define-public crate-hornbeam_ir-0.0.2 (c (n "hornbeam_ir") (v "0.0.2") (d (list (d (n "hornbeam_grammar") (r "^0.0.2") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "insta") (r "^1.28.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1rmmblqd3ymm7v4xrbm0sbgyli8494sxabb8iq4df9rd8bdvpjnr")))

(define-public crate-hornbeam_ir-0.0.3 (c (n "hornbeam_ir") (v "0.0.3") (d (list (d (n "hornbeam_grammar") (r "^0.0.3") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "insta") (r "^1.28.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "18xw0dnzf35i5d3k0w6msrzskybpakr2z36qbb4m6kw97jqb0rs4")))

