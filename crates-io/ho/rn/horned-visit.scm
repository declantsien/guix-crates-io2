(define-module (crates-io ho rn horned-visit) #:use-module (crates-io))

(define-public crate-horned-visit-0.1.0 (c (n "horned-visit") (v "0.1.0") (d (list (d (n "blanket") (r "^0.2.0") (d #t) (k 0)) (d (n "horned-owl") (r "^0.11.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)))) (h "0svj12v9fkvqpyaf08gspsn2ngvsn9kgya019i29r2rf0911g40h")))

