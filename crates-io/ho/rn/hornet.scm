(define-module (crates-io ho rn hornet) #:use-module (crates-io))

(define-public crate-hornet-0.1.0 (c (n "hornet") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "hdrsample") (r "^4.0.0") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "memmap") (r "^0.5.2") (d #t) (k 0)) (d (n "nix") (r "^0.8.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1yk6bx0p95g8w464z4jgv0qg07pn0g382mbyjgpx1dl63175sagh")))

