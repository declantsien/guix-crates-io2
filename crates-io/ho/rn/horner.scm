(define-module (crates-io ho rn horner) #:use-module (crates-io))

(define-public crate-horner-0.1.0 (c (n "horner") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1djv14hgngpy2rq1b89f6z232dh9vi377i4z6nb5zjh4d87qkin5")))

(define-public crate-horner-0.1.1 (c (n "horner") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "144mf3a916vs1fmy508jz2ximhx6wp4c26q33ds4h5wgjy15k842")))

(define-public crate-horner-0.1.2 (c (n "horner") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "16k2hk9y6sqc6qmql2zp8pqwvz37hdj6d964gbi7r6bq36f5q89p")))

(define-public crate-horner-0.2.0 (c (n "horner") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "179v6v0zjnak2qraqsfkcygc1g6jss4dszcdxddqc45zxmrd8kfm")))

(define-public crate-horner-0.2.1 (c (n "horner") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0fdi4cfgmv58jcvrp3xqv9inhahkhhbgaz00rs7m9whxfi8is0la")))

(define-public crate-horner-0.2.2 (c (n "horner") (v "0.2.2") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1r4dnx96a67vq8qrykw5zdn92n763ki31pp7xxprqwkifzajiq3h")))

