(define-module (crates-io ho la hola) #:use-module (crates-io))

(define-public crate-hola-0.1.0 (c (n "hola") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "0prk8l800cmqcgig3srfxnrxflsgk4i336p4ccdjisspgmrzz09k") (y #t)))

(define-public crate-hola-0.1.1 (c (n "hola") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "03kavxh22n1912q43slriwdvcwg91cz9gjg544y5mwyfc8gchpdv")))

