(define-module (crates-io ho wa howami) #:use-module (crates-io))

(define-public crate-howami-0.1.0 (c (n "howami") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "systemstat") (r "^0.1.6") (d #t) (k 0)))) (h "1valvvzlwvqqrxdhvyvl41bx21xi7jrqqibs4zfz70grn3y0jbgb")))

(define-public crate-howami-0.1.1 (c (n "howami") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "systemstat") (r "^0.1.6") (d #t) (k 0)))) (h "1myzy8lcy200yfcw2b00wj4dg6khmqia69akyd0nrrqbs598w98a")))

(define-public crate-howami-0.1.2 (c (n "howami") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "systemstat") (r "^0.1.6") (d #t) (k 0)))) (h "1901487xgzl4hqp4pbpgjb6sl21bg6r9waibyrh6wn6rddvd0wkx")))

(define-public crate-howami-0.1.3 (c (n "howami") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "systemstat") (r "^0.1.6") (d #t) (k 0)))) (h "0qhw1r3n0sljm2x2p51xkjxmlgaicfvylsih52jcnw76ap6qkjpj")))

