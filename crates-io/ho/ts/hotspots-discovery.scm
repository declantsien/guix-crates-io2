(define-module (crates-io ho ts hotspots-discovery) #:use-module (crates-io))

(define-public crate-hotspots-discovery-0.0.4 (c (n "hotspots-discovery") (v "0.0.4") (d (list (d (n "detect-lang") (r "^0") (d #t) (k 0)) (d (n "hotspots-utilities") (r "^0.0.4") (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0dbg8giwii63k9rq0rljsq5vs7bb1albdzf5sc6zygf0xjgwdpgx")))

(define-public crate-hotspots-discovery-0.0.5 (c (n "hotspots-discovery") (v "0.0.5") (d (list (d (n "detect-lang") (r "^0") (d #t) (k 0)) (d (n "hotspots-utilities") (r "^0.0.5") (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0vj56w6vb46g9q5jmsvbpprgy72hh0zbhdm5w3lmyi80zvjl9k0d")))

(define-public crate-hotspots-discovery-0.0.6 (c (n "hotspots-discovery") (v "0.0.6") (d (list (d (n "detect-lang") (r "^0") (d #t) (k 0)) (d (n "hotspots-utilities") (r "^0.0.6") (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "09lq727rr44wmcqzaq65rwqzhkn1hg9y90lw7sf1df8p6dffmi05")))

(define-public crate-hotspots-discovery-0.0.7 (c (n "hotspots-discovery") (v "0.0.7") (d (list (d (n "detect-lang") (r "^0") (d #t) (k 0)) (d (n "hotspots-utilities") (r "^0.0.7") (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1pzbmcpygs460zpg0dp3sjhmjwh73sivs83x365wml52k5g6bswh")))

(define-public crate-hotspots-discovery-0.0.8 (c (n "hotspots-discovery") (v "0.0.8") (d (list (d (n "detect-lang") (r "^0") (d #t) (k 0)) (d (n "hotspots-utilities") (r "^0.0.8") (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "speculoos") (r "^0") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0yxbwr8isiva019kcnnl4268sl7r096bvh1dacswwswgb0v12w85") (r "1.69")))

