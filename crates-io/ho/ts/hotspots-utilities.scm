(define-module (crates-io ho ts hotspots-utilities) #:use-module (crates-io))

(define-public crate-hotspots-utilities-0.0.4 (c (n "hotspots-utilities") (v "0.0.4") (d (list (d (n "git2") (r "^0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0f5scks55kxm5dg6a26m32z2n9rlsnyfyn2abqzsqmiq8qjf90di")))

(define-public crate-hotspots-utilities-0.0.5 (c (n "hotspots-utilities") (v "0.0.5") (d (list (d (n "git2") (r "^0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0yxhhiyi8b5vjqxsa75d07bl01g71vlrlarhyrgv7yjjjjs442xh")))

(define-public crate-hotspots-utilities-0.0.6 (c (n "hotspots-utilities") (v "0.0.6") (d (list (d (n "git2") (r "^0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "17m58vqi5sfaaa8h824dzdzajpvac9gji6a4x65129x7isr8y72h")))

(define-public crate-hotspots-utilities-0.0.7 (c (n "hotspots-utilities") (v "0.0.7") (d (list (d (n "git2") (r "^0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1vlr37fivh97q5kf2qbipnsj7kqlbaacf2qkrmm0sqnxs9a2wwcq")))

(define-public crate-hotspots-utilities-0.0.8 (c (n "hotspots-utilities") (v "0.0.8") (d (list (d (n "git2") (r "^0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "09bjrw5bqy9pmlimf9y2zv6qmhxc0g0swf370gn0b7iq0fx8k4gx") (r "1.69")))

