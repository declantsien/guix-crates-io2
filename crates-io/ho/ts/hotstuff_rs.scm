(define-module (crates-io ho ts hotstuff_rs) #:use-module (crates-io))

(define-public crate-hotstuff_rs-0.0.0 (c (n "hotstuff_rs") (v "0.0.0") (h "1w0iphi2ijxaqa4icfpca1jm67pqp1x0hszwpnqp8jgspb2fvk92")))

(define-public crate-hotstuff_rs-0.2.0 (c (n "hotstuff_rs") (v "0.2.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "borsh") (r "^0.10") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 0)) (d (n "fern") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "1rij12q4gslfx34a7nd8l2p0fjdy1n6pxjry3axsb9ia5azz9fs0")))

(define-public crate-hotstuff_rs-0.2.1 (c (n "hotstuff_rs") (v "0.2.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "borsh") (r "^0.10") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 0)) (d (n "fern") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0fdcnw6fjbf8l33b5hg3cvgkkxz0agil0lxdm4s9ci1zljrp8zzy")))

(define-public crate-hotstuff_rs-0.2.2 (c (n "hotstuff_rs") (v "0.2.2") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "borsh") (r "^0.10") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 0)) (d (n "fern") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0iknvwan4d6zfz4fvy1a2vzzzpbvz55k483wzfpid4nk1h7miwfi")))

(define-public crate-hotstuff_rs-0.3.0 (c (n "hotstuff_rs") (v "0.3.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "borsh") (r "^0.10") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^2.0.0") (f (quote ("rand_core"))) (d #t) (k 0)) (d (n "fern") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (f (quote ("getrandom"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "typed-builder") (r "^0.16.2") (d #t) (k 0)))) (h "1yyb7i0kq5mnfsmmzvc11r1pfgxr0il376mmi5j5r8m8biy8wkvq")))

