(define-module (crates-io ho ts hotspot) #:use-module (crates-io))

(define-public crate-hotspot-0.0.1 (c (n "hotspot") (v "0.0.1") (d (list (d (n "assert") (r "^0.0.1") (d #t) (k 0)))) (h "0c9nx1xkgm0p8q43sknpgzldbg3s2lkir9lxqjmmcab8wbhnc984")))

(define-public crate-hotspot-0.0.2 (c (n "hotspot") (v "0.0.2") (d (list (d (n "assert") (r "^0.0.1") (d #t) (k 0)))) (h "1ibr2paml5drxz14z9nrq3c9n72hbi91nhw6gpr3wjn32ksmvmq6")))

(define-public crate-hotspot-0.0.3 (c (n "hotspot") (v "0.0.3") (d (list (d (n "assert") (r "^0.0.2") (d #t) (k 2)))) (h "09wgnkynyzgin39rk8xaxfgshzcp8kn5ry0v35b4cn7pvy5jbjsj")))

(define-public crate-hotspot-0.0.4 (c (n "hotspot") (v "0.0.4") (d (list (d (n "assert") (r "^0.0.2") (d #t) (k 2)))) (h "1va6dajd9c3cpacsnn1255zj154dy5wr4qzwysahwh8wm6h9yhw8")))

(define-public crate-hotspot-0.0.5 (c (n "hotspot") (v "0.0.5") (d (list (d (n "assert") (r "^0.0.2") (d #t) (k 2)))) (h "0xysw1j5a8ra6hzybqdzffwvl77wqwdhr6vkal0by8k9sibvd82b")))

(define-public crate-hotspot-0.0.6 (c (n "hotspot") (v "0.0.6") (d (list (d (n "assert") (r "^0.0.3") (d #t) (k 2)))) (h "0mis6z1a80nlvw9piw4vd5qv0qxf5j65sd306yyssfjx9nfg4zyl")))

(define-public crate-hotspot-0.0.7 (c (n "hotspot") (v "0.0.7") (d (list (d (n "assert") (r "^0.0.4") (d #t) (k 2)))) (h "0f35fb1i5bh8jpyr8xx8yc1k08j347j48hc7yhx6m3ykhsr33kha")))

(define-public crate-hotspot-0.0.8 (c (n "hotspot") (v "0.0.8") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)))) (h "0b4x7kng746z16jvywfdgam60zc646dizks2vdjj4byz9c386y8k")))

(define-public crate-hotspot-0.0.9 (c (n "hotspot") (v "0.0.9") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)))) (h "1n3kzcb730ryb1k4j84gvnr4hhwqjaf9f68hsmbnxczs6b00mwap")))

(define-public crate-hotspot-0.0.10 (c (n "hotspot") (v "0.0.10") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "01fy2id6agxkjwc258lkavrj8h1cffcy66chad3d9yzs7prjidqj")))

(define-public crate-hotspot-0.0.11 (c (n "hotspot") (v "0.0.11") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "12rmmqsw9v6x4xf816jfkbkm2gyrm8ifi6kgw672hj3cmvwnd555")))

(define-public crate-hotspot-0.0.12 (c (n "hotspot") (v "0.0.12") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "01nbdddzsz0fj6i5flcndf3fh7yirpdas6yvv3kzz3i5sdxxzpjh")))

(define-public crate-hotspot-0.0.13 (c (n "hotspot") (v "0.0.13") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "16wcb16ycac9a9vhj65m3nclg07p1j8slmy84nf2v71vd2q135i9")))

(define-public crate-hotspot-0.0.14 (c (n "hotspot") (v "0.0.14") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0lfqli1jzr6g2vj6p6w8x79ki9pb1hx07n6852l7brm5c29h46f3")))

(define-public crate-hotspot-0.0.15 (c (n "hotspot") (v "0.0.15") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0qx4ikzvl98qwd4nlqpicfb1qad19m399vzxj18nnad0v82rcax1")))

(define-public crate-hotspot-0.0.16 (c (n "hotspot") (v "0.0.16") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "185bk3svvxnyr7ha9g4dkb6a9bjz4ijjf5fw8cckx4c3jqglzqws")))

(define-public crate-hotspot-0.0.17 (c (n "hotspot") (v "0.0.17") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "026lz7531yyp54kc50h0ljdf1g46f67s6wwyhr8lri5z82jxs1xx")))

(define-public crate-hotspot-0.0.18 (c (n "hotspot") (v "0.0.18") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1xv6b0s7h3p1cwp5kx0x5qzy6amdd2q7vac3mj0n6skgda7p6svw")))

(define-public crate-hotspot-0.0.19 (c (n "hotspot") (v "0.0.19") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0bd6r61dnd50vqgszzxa49654qqw6ahl556cyr4mcz1gvpvb7ag9")))

(define-public crate-hotspot-0.1.0 (c (n "hotspot") (v "0.1.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1v896way3q6k5iw1q9ff6283sw2nx25d34bxl4zv7h2ngi89vb8v")))

(define-public crate-hotspot-0.1.1 (c (n "hotspot") (v "0.1.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1cpqdpcsgqp4h09icrbc9f8n41pvf10xq45ha82mlfzylh916clg")))

(define-public crate-hotspot-0.2.0 (c (n "hotspot") (v "0.2.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "024wlwr9as7jdi6m524a09hxvir8ajhj61m29rqbipwjy162wd1h")))

(define-public crate-hotspot-0.3.0 (c (n "hotspot") (v "0.3.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1018wc09ys4jdyjz34a9a9k2pq168myhgfd2spjsf8cqdrpkpxrk")))

(define-public crate-hotspot-0.3.1 (c (n "hotspot") (v "0.3.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1x09qwyk1w05ihvav4g9swf35v9pqdziqm9y0fqznnjvr3c4d3gh")))

(define-public crate-hotspot-0.4.0 (c (n "hotspot") (v "0.4.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "matrix") (r "^0.11") (d #t) (k 0)))) (h "1rvpsjrvc14igahxwr56qzwwvzh78i4j87dwdcmdim3b8jsgb0ng")))

(define-public crate-hotspot-0.4.1 (c (n "hotspot") (v "0.4.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "matrix") (r "^0.12") (d #t) (k 0)))) (h "1bcglqjw4lb0l3isnvya0wkrr645sfb04xkq7vrkd622rb5zz3rr")))

(define-public crate-hotspot-0.4.2 (c (n "hotspot") (v "0.4.2") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "matrix") (r "^0.13") (d #t) (k 0)))) (h "07zjn2qiamrba8zn69z83g06dg3xh28lwyh8f6p4blral54bb1h9")))

(define-public crate-hotspot-0.4.3 (c (n "hotspot") (v "0.4.3") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "matrix") (r "^0.14") (d #t) (k 0)))) (h "1abkpxqqbm8zk3smhlxd4b7m2k7lnkgjhksplybishpgmqv3p45q")))

(define-public crate-hotspot-0.4.4 (c (n "hotspot") (v "0.4.4") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "matrix") (r "^0.15") (d #t) (k 0)))) (h "0xc7sfxyj17l5ipickbnjvh4sdrdayxggrcdgl4jzygd1053h9yv")))

(define-public crate-hotspot-0.4.5 (c (n "hotspot") (v "0.4.5") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "matrix") (r "^0.17") (k 0)))) (h "0cjvrcax5id25gnz7csr3baszsb0vvh0md0hafkv4x4jf9w5lp5w")))

(define-public crate-hotspot-0.5.0 (c (n "hotspot") (v "0.5.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "matrix") (r "^0.17") (k 0)))) (h "0hhh0921rr0fw2pmdap3ffy0gm1n2jzzxmk1wn1rv5a1iq8igaps")))

(define-public crate-hotspot-0.5.1 (c (n "hotspot") (v "0.5.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "matrix") (r "^0.18") (k 0)))) (h "1qlr43xkh6mhmgzfyvxld81aysalq5rlkxnh5rk0n7kvyn5rk1l0")))

(define-public crate-hotspot-0.5.2 (c (n "hotspot") (v "0.5.2") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "matrix") (r "^0.19") (k 0)))) (h "0chxdn9yqiqdxw3jlf9ygz94ya9prn6ziza96687aj4lxc5hvqgc")))

(define-public crate-hotspot-0.5.3 (c (n "hotspot") (v "0.5.3") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "matrix") (r "^0.20") (k 0)))) (h "10jjpk4kwfd8zipcyfd18s51a35za0ykb81fqdaj0dkdhfg7w4ac")))

(define-public crate-hotspot-0.5.4 (c (n "hotspot") (v "0.5.4") (d (list (d (n "assert") (r "^0.6") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "matrix") (r "^0.20") (k 0)))) (h "13iqazp1njj2swcavh32lh3zfnh5jzn77hzy37lh4jchw4g2jjx8")))

(define-public crate-hotspot-0.5.5 (c (n "hotspot") (v "0.5.5") (d (list (d (n "assert") (r "^0.6") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "matrix") (r "^0.21") (k 0)))) (h "1yiwy0gdmc08y6fgh0wzh6hqh1wlh24s13gjnz7dk2c01dqrrj77")))

(define-public crate-hotspot-0.6.0 (c (n "hotspot") (v "0.6.0") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "matrix") (r "^0.21") (k 0)))) (h "0sgjj3i695hm22a46kbpdqy1ar3f2hylr9by8h45ra066r8s9hr4")))

