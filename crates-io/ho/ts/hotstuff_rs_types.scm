(define-module (crates-io ho ts hotstuff_rs_types) #:use-module (crates-io))

(define-public crate-hotstuff_rs_types-0.1.0 (c (n "hotstuff_rs_types") (v "0.1.0") (d (list (d (n "borsh") (r "^0.9") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "1j9sn07ccaryxphbgk6x0mqx61jsazy8l9iwi7hylk8jrkanrn1k")))

