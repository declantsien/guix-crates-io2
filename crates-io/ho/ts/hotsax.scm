(define-module (crates-io ho ts hotsax) #:use-module (crates-io))

(define-public crate-hotsax-0.1.0 (c (n "hotsax") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1cbnydr9s8ahlw6mqks19vfv8msjkxlsjyi48af8v01fv0dq8kaj")))

(define-public crate-hotsax-0.1.1 (c (n "hotsax") (v "0.1.1") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "09k31kkv1yg64jdg0h9y8dkliqpb57azjlwjbwybxgggm28g98pp")))

(define-public crate-hotsax-0.1.2 (c (n "hotsax") (v "0.1.2") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0drib8kz31x2hfq92az20sz5blhkirqvwqmx0521bm26gjgrafi3")))

(define-public crate-hotsax-0.1.3 (c (n "hotsax") (v "0.1.3") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1496cxsbzy6r0v1p25lwigxmr2ki3ba39n5p243y0vn4zmgsyf6l")))

(define-public crate-hotsax-0.1.4 (c (n "hotsax") (v "0.1.4") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "10187mw4gycpgaaqjdl8c6q4h2ix51xqq3cb8y1nnyss9ba9pv4d")))

(define-public crate-hotsax-0.1.5 (c (n "hotsax") (v "0.1.5") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0l6dfzbcvjmw2j0xqsqcdzn8jbiclp0i2aq76cnw73zzmk31gkbi")))

(define-public crate-hotsax-0.2.0 (c (n "hotsax") (v "0.2.0") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0758ilsgg0madf31kw7sqg2czid8bnhqamx9pplk13ml7y7dnsjf")))

(define-public crate-hotsax-0.3.0 (c (n "hotsax") (v "0.3.0") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0d0y2y0h4grcbnd4nx7ly093mpz65mm9dc8s9481a5l57jdb8i6r")))

(define-public crate-hotsax-0.4.0 (c (n "hotsax") (v "0.4.0") (d (list (d (n "csv") (r "^1.1.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "plotly") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0da936dspinl1ib40sjmranr4rdqn4c9fwbicjydyja47w01k7mh")))

