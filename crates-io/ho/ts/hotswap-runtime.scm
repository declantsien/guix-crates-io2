(define-module (crates-io ho ts hotswap-runtime) #:use-module (crates-io))

(define-public crate-hotswap-runtime-0.1.0 (c (n "hotswap-runtime") (v "0.1.0") (d (list (d (n "libloading") (r "^0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.3") (f (quote ("nightly"))) (d #t) (k 0)))) (h "1aa1zppvzdy6dgjp12dlh53yyymsqfp2k3dyazr7j973riv2v9y1")))

(define-public crate-hotswap-runtime-0.2.0 (c (n "hotswap-runtime") (v "0.2.0") (d (list (d (n "libloading") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5.3") (f (quote ("nightly"))) (d #t) (k 0)))) (h "1kpvlzmwi2d1dgjvdflvmibm9fn9mmmi5h4w90dzma4paiij6pwc")))

