(define-module (crates-io ho ts hotsauce) #:use-module (crates-io))

(define-public crate-hotsauce-0.0.0 (c (n "hotsauce") (v "0.0.0") (h "0zj48lsmnrl2pzh94k9ljiqyywx0z0aq8pzfr6c19spqgdg3ldfl")))

(define-public crate-hotsauce-0.1.0 (c (n "hotsauce") (v "0.1.0") (d (list (d (n "expect-test") (r "^1.4.0") (k 2)) (d (n "regex-automata") (r "^0.1.10") (f (quote ("std"))) (k 0)))) (h "14i56bq2758gq3zyy27hgxk9prhwsvqsz2ldvdr3vy7zl67mdank")))

(define-public crate-hotsauce-0.1.1 (c (n "hotsauce") (v "0.1.1") (d (list (d (n "expect-test") (r "^1.4.0") (k 2)) (d (n "regex-automata") (r "^0.1.10") (f (quote ("std"))) (k 0)))) (h "1w3zsy78wrb6p1r7ik5psyfiyriqa0mcm12zfmvrvn3vzj8pn29v")))

