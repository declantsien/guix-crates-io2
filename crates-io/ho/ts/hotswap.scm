(define-module (crates-io ho ts hotswap) #:use-module (crates-io))

(define-public crate-hotswap-0.1.0 (c (n "hotswap") (v "0.1.0") (d (list (d (n "aster") (r "0.18.*") (d #t) (k 0)) (d (n "lazy_static") (r "0.2.*") (d #t) (k 0)))) (h "1z2x6lk2nfwpm3z3yll5avwgni2an2qmh2jswia8ydxsa00ax5pg")))

(define-public crate-hotswap-0.1.1 (c (n "hotswap") (v "0.1.1") (d (list (d (n "aster") (r "0.18.*") (d #t) (k 0)) (d (n "lazy_static") (r "0.2.*") (d #t) (k 0)))) (h "0c4p2avyjfhacvjcck00bhdv3xmsjrb50prq3qxhha0h0cmacmn6")))

(define-public crate-hotswap-0.1.2 (c (n "hotswap") (v "0.1.2") (d (list (d (n "aster") (r "0.18.*") (d #t) (k 0)) (d (n "lazy_static") (r "0.2.*") (d #t) (k 0)))) (h "0kx3cgwafz2i8hrkpdnq8pdx2qdc0wndiamgz90k122jim4w9052")))

(define-public crate-hotswap-0.1.3 (c (n "hotswap") (v "0.1.3") (d (list (d (n "lazy_static") (r "0.2.*") (d #t) (k 0)))) (h "12kahbgwrgls6k24m573r1f7hp865m1ghkn7wqp74vwhiglx5wc7")))

(define-public crate-hotswap-0.1.4 (c (n "hotswap") (v "0.1.4") (h "1a8s5bvfz17gda4yw4zkry677fvxr7fr08xlxad99hay33k1fkpj")))

(define-public crate-hotswap-0.1.5 (c (n "hotswap") (v "0.1.5") (h "1i6xmp82xr39gv7k5d4m14bvlyr2b9vf1gswq27fbc28rjkk7mp9")))

(define-public crate-hotswap-0.1.7 (c (n "hotswap") (v "0.1.7") (d (list (d (n "libloading") (r "0.2.*") (d #t) (k 0)) (d (n "parking_lot") (r "0.2.*") (f (quote ("nightly"))) (d #t) (k 0)))) (h "00k35ijgxs3i82p377l13g4r21bcrcb7vlq34047pm8r889zh548")))

(define-public crate-hotswap-0.1.8 (c (n "hotswap") (v "0.1.8") (d (list (d (n "libloading") (r "0.2.*") (d #t) (k 0)) (d (n "parking_lot") (r "0.2.*") (f (quote ("nightly"))) (d #t) (k 0)))) (h "00ra7508dqp261kkckz2nsfmamzq4fdgw0c8fma4v0i58gmwvcld")))

(define-public crate-hotswap-0.2.0 (c (n "hotswap") (v "0.2.0") (h "09g3m2mzm79dvhc59wzvjlmi5j0iqi44s3snwn0jbrgnc758xifm")))

(define-public crate-hotswap-0.3.0 (c (n "hotswap") (v "0.3.0") (h "1d2q4lnbpifhb34bwwg11is4zyfpi42lmcdyijp6xdhqy671cvh9")))

(define-public crate-hotswap-0.3.1 (c (n "hotswap") (v "0.3.1") (h "1gx5zq5nn4mp2sm01hi1fngn3w9i3zzvmm6kmkrz07h4n5kqjh4c")))

(define-public crate-hotswap-0.3.2 (c (n "hotswap") (v "0.3.2") (h "0hcpdmqqj7kgv0k3alz68686isyvd53k64vpylhsnw9kmn5qalgy")))

(define-public crate-hotswap-0.4.0 (c (n "hotswap") (v "0.4.0") (h "0r43rdss4x5qyz8f3yvld0vf7c684qn12gjghzjlkv1pbhibiccx")))

(define-public crate-hotswap-0.4.1 (c (n "hotswap") (v "0.4.1") (h "0vbflzq5pmb4krrpgp62smjlpqlg85bl13zjacihlfpgqv8c5fhx")))

(define-public crate-hotswap-0.4.2 (c (n "hotswap") (v "0.4.2") (h "01wh7nhn0b3gw7hr0cslybv968y3cixyhyvbsm4dckrhr7kjv89p")))

(define-public crate-hotswap-0.5.0 (c (n "hotswap") (v "0.5.0") (h "1mz69aw7w91avbhmgaisc9ifr1mw16vsdad3amgp72wv4al9l031")))

(define-public crate-hotswap-0.6.0 (c (n "hotswap") (v "0.6.0") (h "0iws8f0hskvp963mhzwzd95cksyyryjhrn1f7j2bkzlhp56m12g9")))

