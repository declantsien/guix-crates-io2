(define-module (crates-io ho p- hop-kak) #:use-module (crates-io))

(define-public crate-hop-kak-0.1.0 (c (n "hop-kak") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0cbphdd8fj5ih7j5y18krrknm3xfkbf53g611a9drvmg0gr82fmp") (r "1.71")))

(define-public crate-hop-kak-0.2.0 (c (n "hop-kak") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0jx8a2ljmmr70l7cf70w7l5hdbivbwqfb23q4yxasbmim71gpz1p") (f (quote (("init") ("default" "init")))) (r "1.71")))

