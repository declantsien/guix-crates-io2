(define-module (crates-io ho wt howtoshell) #:use-module (crates-io))

(define-public crate-howtoshell-1.0.1 (c (n "howtoshell") (v "1.0.1") (d (list (d (n "clap") (r "^4.2.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0s9n1ykvj5aqc8sj3w07mf11x07c5c0pibdsajz9chpl556brn7v")))

