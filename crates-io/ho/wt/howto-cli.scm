(define-module (crates-io ho wt howto-cli) #:use-module (crates-io))

(define-public crate-howto-cli-0.1.1 (c (n "howto-cli") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "howto") (r "^0.1") (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1.2") (d #t) (k 0)))) (h "01ny7s79ch7lxay2jrljjag94pv0piqibx9f9drbw8z93k8ya1gx")))

(define-public crate-howto-cli-0.1.2 (c (n "howto-cli") (v "0.1.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "howto") (r "^0.1") (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1.2") (d #t) (k 0)))) (h "17alh9zkb1gazshm86axznxhas1xz80qnkf7lh8sbwrh4h52x2dd")))

(define-public crate-howto-cli-0.1.3 (c (n "howto-cli") (v "0.1.3") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "howto") (r "^0.1") (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1.2") (d #t) (k 0)))) (h "15124b7dm5mmls7gw9ssigxyfs1jijkdpipqjyxnvlagd1j515li")))

(define-public crate-howto-cli-0.1.4 (c (n "howto-cli") (v "0.1.4") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "howto") (r "^0.1") (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1.2") (d #t) (k 0)))) (h "19r70v80ja3pm11ia6b4dmr40n2g3gma3p9pavnf2ax29m6v0msm")))

(define-public crate-howto-cli-0.2.0 (c (n "howto-cli") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "howto") (r "^0.2.0") (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1.13") (d #t) (k 0)))) (h "05dqmywl6y7ssa2wr6vhv329ggvzap6jgwhz4yb8m3301ajmi4q3")))

(define-public crate-howto-cli-0.4.0 (c (n "howto-cli") (v "0.4.0") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "howto") (r "^0.4") (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded"))) (d #t) (k 0)))) (h "1s2bfp7lp13aajj75qwhkmidgdd2g5aqnd2zvlhw9n8lxxdxmsdz")))

