(define-module (crates-io ho wt howto) #:use-module (crates-io))

(define-public crate-howto-0.1.0 (c (n "howto") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "scraper") (r "^0.7") (d #t) (k 0)) (d (n "slugify") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "0ac91hq9ra4wk6hcq0zdmk454ik0q4kxv2na9b0wbmfck06cqr2p") (y #t)))

(define-public crate-howto-0.1.1 (c (n "howto") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "scraper") (r "^0.7") (d #t) (k 0)) (d (n "slugify") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "0nhp05jr4pxx5f6rfkrf791pjpbca0yiwfvpw4q1j6lakgl66q7g")))

(define-public crate-howto-0.1.2 (c (n "howto") (v "0.1.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "scraper") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "03dkklvkvwd0ag0vfwad0mscj24ayhybmqf7wa6v8in2kl2aj5kh")))

(define-public crate-howto-0.1.3 (c (n "howto") (v "0.1.3") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "scraper") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0b96j0bm145819x2f8s74kbyprd47qlr2209m64abr639g2wfmx9")))

(define-public crate-howto-0.2.0 (c (n "howto") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "scraper") (r "^0.9.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.13") (d #t) (k 0)))) (h "0nlkvdk5i9zyxpnjc1ja96v83qfwindgikv0kvhim0n9v2ddji0b")))

(define-public crate-howto-0.2.1 (c (n "howto") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.7") (d #t) (k 0)) (d (n "scraper") (r "^0.9.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.14") (d #t) (k 0)))) (h "1v7fihqzwd2cxwfbj3p6m2zz109yp6wrgvz0vmchsb7w94irqx3k")))

(define-public crate-howto-0.3.0 (c (n "howto") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "scraper") (r "^0.10.1") (d #t) (k 0)) (d (n "surf") (r "^1.0.2") (d #t) (k 0)))) (h "1y62rkr377nrx9w7jf7vzl3qipy9giinfnrjgwn1vj75j8wy03i0")))

(define-public crate-howto-0.3.1 (c (n "howto") (v "0.3.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "scraper") (r "^0.10.1") (d #t) (k 0)) (d (n "surf") (r "^1.0.2") (d #t) (k 0)))) (h "13ksl13m1sf7zncjc7n6ki0f093ciykis4abrq6n5w3r99ah9sn6")))

(define-public crate-howto-0.4.0 (c (n "howto") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "scraper") (r "^0.10.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "09hj7hh7bygax8x7kid249d35vaaxajrszdai1lxnkvz66q8libk")))

