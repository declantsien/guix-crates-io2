(define-module (crates-io ho ed hoedown) #:use-module (crates-io))

(define-public crate-hoedown-0.0.1 (c (n "hoedown") (v "0.0.1") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 1)) (d (n "regex_macros") (r "*") (d #t) (k 1)))) (h "1n5m0rxvba768gyvrfwrv8fnz4w5mpqzqxvb815gsfb9kv0gybq4")))

(define-public crate-hoedown-1.0.0 (c (n "hoedown") (v "1.0.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "glob") (r "*") (d #t) (k 2)) (d (n "regex") (r "*") (d #t) (k 1)) (d (n "regex_macros") (r "*") (d #t) (k 1)))) (h "1l45m6510qm3a97az9qidgkb9mmcy7axbx6zd6hiv19k6ld2ngif")))

(define-public crate-hoedown-1.1.0 (c (n "hoedown") (v "1.1.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "glob") (r "*") (d #t) (k 2)) (d (n "regex") (r "*") (d #t) (k 1)) (d (n "regex_macros") (r "*") (d #t) (k 1)))) (h "02623ak565d306w3782bh338mlsrf3c71zc4i0ra64bsgw66h769")))

(define-public crate-hoedown-1.1.1 (c (n "hoedown") (v "1.1.1") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "glob") (r "*") (d #t) (k 2)) (d (n "regex") (r "*") (d #t) (k 1)) (d (n "regex_macros") (r "*") (d #t) (k 1)))) (h "04lk2j1py1073c1nbxg5lbz13q4wgvypvvaqxw2n4si3igdhqy4b")))

(define-public crate-hoedown-1.1.2 (c (n "hoedown") (v "1.1.2") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "glob") (r "*") (d #t) (k 2)) (d (n "regex") (r "*") (d #t) (k 1)) (d (n "regex_macros") (r "*") (d #t) (k 1)))) (h "1igab11df0arjj3ykl82cig59v784f6pv29lilv26vli43n2lc6m")))

(define-public crate-hoedown-1.1.3 (c (n "hoedown") (v "1.1.3") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "glob") (r "*") (d #t) (k 2)) (d (n "regex") (r "*") (d #t) (k 1)) (d (n "regex_macros") (r "*") (d #t) (k 1)))) (h "0vjs3crj0xrpbnlgxycw9kld4lc7ys0v0gyk2msy1jfn3rwcxkzl")))

(define-public crate-hoedown-1.1.4 (c (n "hoedown") (v "1.1.4") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "glob") (r "*") (d #t) (k 2)) (d (n "regex") (r "*") (d #t) (k 1)) (d (n "regex_macros") (r "*") (d #t) (k 1)))) (h "000c2jh70srfys98hhhh8dyyc15vbxlv4axslx8phvnm6lx97yvv")))

(define-public crate-hoedown-1.1.5 (c (n "hoedown") (v "1.1.5") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "glob") (r "*") (d #t) (k 2)) (d (n "regex") (r "*") (d #t) (k 1)) (d (n "regex_macros") (r "*") (d #t) (k 1)))) (h "1f8d4rp583w2wl2kci1xqdr0z26mg7l3r00510qsm1pwi3hjnh9n")))

(define-public crate-hoedown-1.1.6 (c (n "hoedown") (v "1.1.6") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "glob") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 1)) (d (n "regex_macros") (r "*") (d #t) (k 1)))) (h "0drbxby06gnv9qwgp0cva5p820f8iim4fc20ba72kpgy680l5f88")))

(define-public crate-hoedown-1.1.7 (c (n "hoedown") (v "1.1.7") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "glob") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 1)) (d (n "regex_macros") (r "*") (d #t) (k 1)))) (h "1s2nqpk87m4ymlfcksml95yn8zfrhk62rbs79g85678ylldzr498")))

(define-public crate-hoedown-1.1.8 (c (n "hoedown") (v "1.1.8") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "glob") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 1)) (d (n "regex_macros") (r "*") (d #t) (k 1)))) (h "1gsp46d8hy0p4va9x0bj23zfhlk0rl3qrkcjnzwkszk8yvk3icci")))

(define-public crate-hoedown-1.1.9 (c (n "hoedown") (v "1.1.9") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "glob") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 1)) (d (n "regex_macros") (r "*") (d #t) (k 1)))) (h "08pc569cmwhjkrlg9ycfk6fcblnvzsbnwbpdlfbygiyb9mzgvhl5")))

(define-public crate-hoedown-1.1.10 (c (n "hoedown") (v "1.1.10") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "glob") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 1)) (d (n "regex_macros") (r "*") (d #t) (k 1)))) (h "15hispq8gb2040y83ad3nb6y7s9m82kp9nm1x46jhjz2g9yhwjaj")))

(define-public crate-hoedown-1.1.11 (c (n "hoedown") (v "1.1.11") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "glob") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 1)) (d (n "regex_macros") (r "*") (d #t) (k 1)))) (h "1wiknydan39rz5lzxfa8af6cmlh8m4196d753z6m5dzy71pip7gp")))

(define-public crate-hoedown-2.0.0 (c (n "hoedown") (v "2.0.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "glob") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 1)) (d (n "regex_macros") (r "*") (d #t) (k 1)))) (h "0ma1wypy5fdp7lmc4956rhdd77cc4d4rhv942q334d1wq8y1582v")))

(define-public crate-hoedown-3.0.0 (c (n "hoedown") (v "3.0.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "glob") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 1)) (d (n "regex_macros") (r "*") (d #t) (k 1)))) (h "14ckkkvwnic8bkdzp5r3diqclhmpw2fphhvqf42d7cydhiimgrja")))

(define-public crate-hoedown-3.0.1 (c (n "hoedown") (v "3.0.1") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "glob") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 1)) (d (n "regex_macros") (r "*") (d #t) (k 1)))) (h "0b0yvlxjmh7ca001a2030g9nxymvcavg5jqy6f9vs8mbwi3j50c6")))

(define-public crate-hoedown-3.0.2 (c (n "hoedown") (v "3.0.2") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "glob") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "16fpjcg0hxldmajv3nd9zsdig6gl1bkjpl5mzna7fz7fb2vcsw5r")))

(define-public crate-hoedown-3.0.3 (c (n "hoedown") (v "3.0.3") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glob") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0y2k1wpmkpmhiwrr22g4xv7v5wff8387das29l18wkzqr46122x9")))

(define-public crate-hoedown-3.0.5 (c (n "hoedown") (v "3.0.5") (d (list (d (n "bitflags") (r "^0.5") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glob") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1975qyd46rvwsr5vwdxikaiyxs6p5zspi2i0ldpd49id4i5c8x5y")))

(define-public crate-hoedown-4.0.0 (c (n "hoedown") (v "4.0.0") (d (list (d (n "bitflags") (r "^0.5") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glob") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "028r1zaigwpbvfpm18n429p7qivyr5d3dnrssqfxvkn0syr478zv")))

(define-public crate-hoedown-5.0.0 (c (n "hoedown") (v "5.0.0") (d (list (d (n "bitflags") (r "^0.5") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glob") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qmjfgvj3lrzm5y8c5j1dyxkpmin3p3kzzwnakchj0a9381qw10d")))

(define-public crate-hoedown-6.0.0 (c (n "hoedown") (v "6.0.0") (d (list (d (n "bitflags") (r "^0.5") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "glob") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "timebomb") (r "^0.1.2") (d #t) (k 2)))) (h "1mamr5ncy5l2wrdhhdgdpg0myvlgnsvk57ygjmy1wi47y9ry3gg5")))

