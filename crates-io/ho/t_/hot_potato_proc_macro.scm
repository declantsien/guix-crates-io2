(define-module (crates-io ho t_ hot_potato_proc_macro) #:use-module (crates-io))

(define-public crate-hot_potato_proc_macro-0.1.0 (c (n "hot_potato_proc_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (d #t) (k 0)) (d (n "venial") (r "^0.5.0") (d #t) (k 0)))) (h "063bwg2khs5ggnc1hzvxa8ggcx3b3arbxvqn5m71x7b53hdh7n3y")))

(define-public crate-hot_potato_proc_macro-0.1.1 (c (n "hot_potato_proc_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (d #t) (k 0)) (d (n "venial") (r "^0.5.0") (d #t) (k 0)))) (h "0pijgmd776638m6qg64ww89gwgb6r2diqc2qkn2njkkgsfharfmv")))

(define-public crate-hot_potato_proc_macro-0.1.2 (c (n "hot_potato_proc_macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (d #t) (k 0)) (d (n "venial") (r "^0.5.0") (d #t) (k 0)))) (h "0bz9a71aqpamip4yhwgjgfv4l3v42cr825cbpg95cil4pf98zmjx")))

