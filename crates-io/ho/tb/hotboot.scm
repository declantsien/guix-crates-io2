(define-module (crates-io ho tb hotboot) #:use-module (crates-io))

(define-public crate-hotboot-0.1.0 (c (n "hotboot") (v "0.1.0") (d (list (d (n "openssl") (r "^0.9.7") (d #t) (k 0)))) (h "1nxyj54wzfry33d8w540ynfpfpvpmn4my1y4d9y9cafr41qcw1rg")))

(define-public crate-hotboot-0.1.1 (c (n "hotboot") (v "0.1.1") (d (list (d (n "openssl") (r "^0.10.29") (d #t) (k 0)))) (h "1vd19zkz25x4sbw9ll4kbq8kgq7a1c5g3k9pldfvky2namaklanr")))

