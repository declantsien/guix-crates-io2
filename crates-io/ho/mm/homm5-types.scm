(define-module (crates-io ho mm homm5-types) #:use-module (crates-io))

(define-public crate-homm5-types-0.1.0 (c (n "homm5-types") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.30.0") (f (quote ("serialize" "serde-types"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.2") (d #t) (k 0)))) (h "03q852qrqicdnclg76cxd0s2v88cn6nvjzmm0hvph3lwhpmkks18")))

(define-public crate-homm5-types-0.1.1 (c (n "homm5-types") (v "0.1.1") (d (list (d (n "quick-xml") (r "^0.30.0") (f (quote ("serialize" "serde-types"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.2") (d #t) (k 0)))) (h "1yq5jl4y1xmj5690ycaz7qmrjjam7d91s8ikqp5q4mx341s1gi2f")))

(define-public crate-homm5-types-0.1.11 (c (n "homm5-types") (v "0.1.11") (d (list (d (n "quick-xml") (r "^0.30.0") (f (quote ("serialize" "serde-types"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.2") (d #t) (k 0)))) (h "024nmjl2aw7rq7lygwm1x4hic5mzrnakjll55h4vfhhyzmxv8363")))

(define-public crate-homm5-types-0.1.12 (c (n "homm5-types") (v "0.1.12") (d (list (d (n "quick-xml") (r "^0.30.0") (f (quote ("serialize" "serde-types"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.2") (d #t) (k 0)))) (h "0jcn6g3v0s8l220943i7qvyxb0jkqkc0jffpiv35gdxx1ac6v5zs")))

(define-public crate-homm5-types-0.1.13 (c (n "homm5-types") (v "0.1.13") (d (list (d (n "quick-xml") (r "^0.30.0") (f (quote ("serialize" "serde-types"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.2") (d #t) (k 0)))) (h "1kim3dykcqdqj39y89sv3sayf0idk731j6hkg1fklmafw82mn927")))

(define-public crate-homm5-types-0.1.14 (c (n "homm5-types") (v "0.1.14") (d (list (d (n "quick-xml") (r "^0.30.0") (f (quote ("serialize" "serde-types"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.2") (d #t) (k 0)))) (h "19awb1bbsx7kh00sy7wdi4h2k9l7mqcxnnzzdp9wcdzl4ncc2jcz")))

(define-public crate-homm5-types-0.1.15 (c (n "homm5-types") (v "0.1.15") (d (list (d (n "quick-xml") (r "^0.30.0") (f (quote ("serialize" "serde-types"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.2") (d #t) (k 0)))) (h "0g214fjlvdc8wvnrlwqbg44lsgz1v6f8pig3xg9132py7ag6ixwd")))

(define-public crate-homm5-types-0.1.16 (c (n "homm5-types") (v "0.1.16") (d (list (d (n "quick-xml") (r "^0.30.0") (f (quote ("serialize" "serde-types"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.2") (d #t) (k 0)))) (h "1h4ln0sf0bl5rvj9hx9xlq465gvp69m02mlcvf435axgk3wgv0nq")))

(define-public crate-homm5-types-0.1.17 (c (n "homm5-types") (v "0.1.17") (d (list (d (n "quick-xml") (r "^0.30.0") (f (quote ("serialize" "serde-types"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.2") (d #t) (k 0)))) (h "1kbki0s8b00p4z1xnrk14frff2x9gnj5k98kyv19bk45qndy0l55")))

(define-public crate-homm5-types-0.1.18 (c (n "homm5-types") (v "0.1.18") (d (list (d (n "quick-xml") (r "^0.30.0") (f (quote ("serialize" "serde-types"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.2") (d #t) (k 0)))) (h "0y4r8klhlvfh2rxvg41vhskzkq6vbqw8a7zsyymhc0nmk4d903jn")))

(define-public crate-homm5-types-0.1.19 (c (n "homm5-types") (v "0.1.19") (d (list (d (n "quick-xml") (r "^0.30.0") (f (quote ("serialize" "serde-types"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.2") (d #t) (k 0)))) (h "18gkk3yyq3k2mgf7hjdlc03ymnnliwhq2nz7a1md957j0vlb94am")))

(define-public crate-homm5-types-0.1.2 (c (n "homm5-types") (v "0.1.2") (d (list (d (n "quick-xml") (r "^0.30.0") (f (quote ("serialize" "serde-types"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.2") (d #t) (k 0)))) (h "0hhs72bb1paxq5w9vh3qvwk8zqcbiwc90xsvqd199d3ky1bzfz7p")))

