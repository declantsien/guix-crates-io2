(define-module (crates-io od ra odra-casper-livenet-env) #:use-module (crates-io))

(define-public crate-odra-casper-livenet-env-0.8.0 (c (n "odra-casper-livenet-env") (v "0.8.0") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "odra-casper-rpc-client") (r "^0.8.0") (d #t) (k 0)) (d (n "odra-core") (r "^0.8.0") (d #t) (k 0)))) (h "0lpcqcchbkd9j6dgay7dmz7dqr89aymp89s0qgd9qyhc8m4zzxbx")))

(define-public crate-odra-casper-livenet-env-0.8.1 (c (n "odra-casper-livenet-env") (v "0.8.1") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "odra-casper-rpc-client") (r "^0.8.1") (d #t) (k 0)) (d (n "odra-core") (r "^0.8.1") (d #t) (k 0)))) (h "011m38vd5k9mhcixlns2c07p8mhvvnsrzv1psfc1anfc30jybnv3")))

(define-public crate-odra-casper-livenet-env-0.9.0 (c (n "odra-casper-livenet-env") (v "0.9.0") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "odra-casper-rpc-client") (r "^0.9.0") (d #t) (k 0)) (d (n "odra-core") (r "^0.9.0") (d #t) (k 0)))) (h "12d7ll58d940l0dwvcpddrxj03nz34zchj35a8vh6xjwy1vamaf3")))

(define-public crate-odra-casper-livenet-env-0.9.1 (c (n "odra-casper-livenet-env") (v "0.9.1") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "odra-casper-rpc-client") (r "^0.9.1") (d #t) (k 0)) (d (n "odra-core") (r "^0.9.1") (d #t) (k 0)))) (h "0x8mzyfr2yd5dcpvrxwb51mjgws645i6lwlwgj8nw4js70m5wfvy")))

(define-public crate-odra-casper-livenet-env-1.0.0-rc.1 (c (n "odra-casper-livenet-env") (v "1.0.0-rc.1") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "odra-casper-rpc-client") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "odra-core") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "0pyiq6k9d26ajgv7hdfsc0dzsn6c0b39v2rn7hrjx0m1fs5anzrh")))

(define-public crate-odra-casper-livenet-env-1.0.0 (c (n "odra-casper-livenet-env") (v "1.0.0") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "odra-casper-rpc-client") (r "^1.0.0") (d #t) (k 0)) (d (n "odra-core") (r "^1.0.0") (d #t) (k 0)))) (h "0lybw44k8dz3zw2361r7nfhl8jrndx9aqj63sp1485yv73fb09y1")))

