(define-module (crates-io od ra odra-utils) #:use-module (crates-io))

(define-public crate-odra-utils-0.0.1 (c (n "odra-utils") (v "0.0.1") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "odra-types") (r "^0.0.1") (d #t) (k 0)))) (h "079li8xzjdpm95simnvxv8bbzf6l9ha30nnpg0jj98vgh884adzl")))

(define-public crate-odra-utils-0.1.0 (c (n "odra-utils") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "odra-types") (r "^0.1.0") (d #t) (k 0)))) (h "17x6cva2gckzbwrkqiljkhf35x3dhbd1bq4p8w6sfgncgda5wlf2")))

(define-public crate-odra-utils-0.2.0 (c (n "odra-utils") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)))) (h "15f736j3z08fsqv510aa00n3hqd269p63biqnphmi5mrbwg6kd6n")))

(define-public crate-odra-utils-0.3.0 (c (n "odra-utils") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)))) (h "1cq26xiwvmwzsqrjhqv5ycwxb5q8x8jhy6671zhfyr4yrqg08mq6")))

(define-public crate-odra-utils-0.3.1 (c (n "odra-utils") (v "0.3.1") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)))) (h "0iascrpfch81idl8wc65b8827n0l4h11v74dicvzpdwwy424gxpz")))

(define-public crate-odra-utils-0.4.0 (c (n "odra-utils") (v "0.4.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)))) (h "02xkzrfc787dfb4i3gka86p2bhh7mssizszwr61vhwh4ybf5hl8s")))

(define-public crate-odra-utils-0.5.0 (c (n "odra-utils") (v "0.5.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)))) (h "02gifrcs3ip4qchg449c8n92pyygrcpdf4wyji0zvbpsv3h4iajy")))

(define-public crate-odra-utils-0.6.0 (c (n "odra-utils") (v "0.6.0") (d (list (d (n "convert_case") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "09p860cylbpbv49m8dqsbhx225didj80f1l060dxw0zj7v437i01") (f (quote (("std" "convert_case"))))))

(define-public crate-odra-utils-0.7.0 (c (n "odra-utils") (v "0.7.0") (d (list (d (n "convert_case") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "0agd6qscya2n1nr71y5hmj72bfnrrn15pb0gsapa925v3rnj8ldd") (f (quote (("std" "convert_case"))))))

(define-public crate-odra-utils-0.7.1 (c (n "odra-utils") (v "0.7.1") (d (list (d (n "convert_case") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "0ghlvcwajngpk74bqnbjmq17xni57sj4jsxaxrlgpp3vyv5nsqq8") (f (quote (("std" "convert_case"))))))

(define-public crate-odra-utils-0.6.1 (c (n "odra-utils") (v "0.6.1") (d (list (d (n "convert_case") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "04aiygvcnhrpbs57a71ql5vrraabj0ah0ws0zai579frvbf6vmkf") (f (quote (("std" "convert_case"))))))

(define-public crate-odra-utils-0.6.2 (c (n "odra-utils") (v "0.6.2") (d (list (d (n "convert_case") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "0b1iws67sml5lggxkla1r22si686qdyjq316pjn3cnfgd547sgi8") (f (quote (("std" "convert_case"))))))

