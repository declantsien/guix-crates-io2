(define-module (crates-io od ra odra-proc-macros) #:use-module (crates-io))

(define-public crate-odra-proc-macros-0.0.1 (c (n "odra-proc-macros") (v "0.0.1") (d (list (d (n "odra-codegen") (r "^0.0.1") (d #t) (k 0)) (d (n "odra-ir") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.19") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qp3gxlxr2p6f8acs57id89rh5kwjmk9hwz3vf6a9f03i661nb82")))

(define-public crate-odra-proc-macros-0.1.0 (c (n "odra-proc-macros") (v "0.1.0") (d (list (d (n "odra-codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "odra-ir") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.19") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mn31dyqajnsvfwd6pliym6y9qkcvdz1f3jj843w4208g485b6az")))

(define-public crate-odra-proc-macros-0.2.0 (c (n "odra-proc-macros") (v "0.2.0") (d (list (d (n "odra-codegen") (r "^0.2.0") (d #t) (k 0)) (d (n "odra-ir") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.19") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "175473s7shk4iw9ph604jciymysc3gz7v1njkqcpwjqzxn58fxg4")))

(define-public crate-odra-proc-macros-0.3.0 (c (n "odra-proc-macros") (v "0.3.0") (d (list (d (n "odra-codegen") (r "^0.3.0") (d #t) (k 0)) (d (n "odra-ir") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.19") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1nd9larlbiajj199bhj331f5fvr73s14rpjvc9wl3xn2a096lr6l")))

(define-public crate-odra-proc-macros-0.3.1 (c (n "odra-proc-macros") (v "0.3.1") (d (list (d (n "odra-codegen") (r "^0.3.1") (d #t) (k 0)) (d (n "odra-ir") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.19") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mzldv7yqmhzcp4lyn22knbw6ibqf1f5658p1sr6p2a461mr8lm6")))

(define-public crate-odra-proc-macros-0.4.0 (c (n "odra-proc-macros") (v "0.4.0") (d (list (d (n "odra-codegen") (r "^0.4.0") (d #t) (k 0)) (d (n "odra-ir") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.19") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1b40rchkd3kw1mkbv1f5x1km5wnjr54knqnzhyk5p8fnkcj0w5y4")))

(define-public crate-odra-proc-macros-0.5.0 (c (n "odra-proc-macros") (v "0.5.0") (d (list (d (n "odra-codegen") (r "^0.5.0") (d #t) (k 0)) (d (n "odra-ir") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.19") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1a6wwfz3dxhh0fs6x885il7nyv79kdz8j4sjrmjyzl4jsa8g9jh2")))

(define-public crate-odra-proc-macros-0.6.0 (c (n "odra-proc-macros") (v "0.6.0") (d (list (d (n "odra-codegen") (r "^0.6.0") (d #t) (k 0)) (d (n "odra-ir") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.19") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lcj7jfrpajka270p14yiy2dlifxl9h4jqjss0qr2x27gbki3wpp")))

(define-public crate-odra-proc-macros-0.7.0 (c (n "odra-proc-macros") (v "0.7.0") (d (list (d (n "odra-codegen") (r "^0.7.0") (d #t) (k 0)) (d (n "odra-ir") (r "^0.7.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.19") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1kc8b4y9ilk4rczis0n4dcdvqq801v04pnvj6jxnn63hm1rbi9fh")))

(define-public crate-odra-proc-macros-0.7.1 (c (n "odra-proc-macros") (v "0.7.1") (d (list (d (n "odra-codegen") (r "^0.7.1") (d #t) (k 0)) (d (n "odra-ir") (r "^0.7.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.19") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19psvvwcbmq943zcxl3ycia9rwal3bbw8jz065dmncdr443l2zjy")))

(define-public crate-odra-proc-macros-0.6.1 (c (n "odra-proc-macros") (v "0.6.1") (d (list (d (n "odra-codegen") (r "^0.6.1") (d #t) (k 0)) (d (n "odra-ir") (r "^0.6.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.19") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0d4ny3ip4cgma495c3bbawqxwlcv6c800vh0ldsqsz910yx6yg51")))

(define-public crate-odra-proc-macros-0.6.2 (c (n "odra-proc-macros") (v "0.6.2") (d (list (d (n "odra") (r "^0.6.2") (d #t) (k 2)) (d (n "odra-codegen") (r "^0.6.2") (d #t) (k 0)) (d (n "odra-ir") (r "^0.6.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.19") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rmy6kcmyvvn35a7pzrdavgb4bl7blzq8338s88g393j05w4nyy2")))

