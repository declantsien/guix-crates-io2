(define-module (crates-io od ra odra-build) #:use-module (crates-io))

(define-public crate-odra-build-0.8.1 (c (n "odra-build") (v "0.8.1") (h "074gp9n3714j2v8h44aqw1ymra04iw5pq7ysvifs21kzdbaxwa6i")))

(define-public crate-odra-build-0.9.0 (c (n "odra-build") (v "0.9.0") (h "1sqi1mhwkrjbmfnd9l4vy8l6jhy1ll20i5pjanv56q1yind1n78m")))

(define-public crate-odra-build-0.9.1 (c (n "odra-build") (v "0.9.1") (h "0k0jdh7ni8am73315cq7kcbxp89dkixx346lk2xmgabxs3avq2zl")))

(define-public crate-odra-build-1.0.0-rc.1 (c (n "odra-build") (v "1.0.0-rc.1") (h "15zzh788xssmpn700ah0ns1xlmz8nwh9y7p8iskc2cp8ikzpgbd1")))

(define-public crate-odra-build-1.0.0 (c (n "odra-build") (v "1.0.0") (h "1142wg3zc3zszghgfll8fzyh2jpig9pbc1xfhxq9apjg0626khkw")))

