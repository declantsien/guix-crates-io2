(define-module (crates-io od ra odra-test-env-wrapper) #:use-module (crates-io))

(define-public crate-odra-test-env-wrapper-0.0.1 (c (n "odra-test-env-wrapper") (v "0.0.1") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "dlopen_derive") (r "^0.1.4") (d #t) (k 0)) (d (n "odra-types") (r "^0.0.1") (d #t) (k 0)))) (h "0fs9r3w8r1g5rx70pcyhgnqj5nnvm8msv77vkq226i3qsfrlwmvb")))

(define-public crate-odra-test-env-wrapper-0.1.0 (c (n "odra-test-env-wrapper") (v "0.1.0") (d (list (d (n "dlopen") (r "^0.1.8") (d #t) (k 0)) (d (n "dlopen_derive") (r "^0.1.4") (d #t) (k 0)) (d (n "odra-types") (r "^0.1.0") (d #t) (k 0)))) (h "1nibj767r7vr7qxqzrqgzfcph2k905bhcs50q5jrl8cw7hjcqmp4")))

