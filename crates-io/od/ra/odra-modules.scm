(define-module (crates-io od ra odra-modules) #:use-module (crates-io))

(define-public crate-odra-modules-0.2.0 (c (n "odra-modules") (v "0.2.0") (d (list (d (n "odra") (r "^0.2.0") (k 0)))) (h "0lk834frprxy40h9gdijjfybyaj2g2vzfmgc6cnzsgs3ixk4f1fp") (f (quote (("mock-vm" "odra/mock-vm") ("default" "mock-vm") ("casper" "odra/casper"))))))

(define-public crate-odra-modules-0.3.0 (c (n "odra-modules") (v "0.3.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "odra") (r "^0.3.0") (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)))) (h "0g8qjqkx8kx82l9sqa7dwpigwlsfx9vxpbnnf5zlh08dz5p0c3ly") (f (quote (("mock-vm" "odra/mock-vm") ("default" "mock-vm") ("casper-livenet" "odra/casper-livenet") ("casper" "odra/casper"))))))

(define-public crate-odra-modules-0.3.1 (c (n "odra-modules") (v "0.3.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "odra") (r "^0.3.1") (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)))) (h "1347wwp48cr8n144vg8fab60jqqkn6yvy96hzfmkn3hm9a8zf1af") (f (quote (("mock-vm" "odra/mock-vm") ("default" "mock-vm") ("casper-livenet" "odra/casper-livenet") ("casper" "odra/casper"))))))

(define-public crate-odra-modules-0.4.0 (c (n "odra-modules") (v "0.4.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "odra") (r "^0.4.0") (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)))) (h "1rsrrgsfch81qiwb23z3czz4fkc7hpxfq2lcszsw3cm2ykkbiv8y") (f (quote (("mock-vm" "odra/mock-vm") ("default" "mock-vm") ("casper-livenet" "odra/casper-livenet") ("casper" "odra/casper"))))))

(define-public crate-odra-modules-0.5.0 (c (n "odra-modules") (v "0.5.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "odra") (r "^0.5.0") (k 0)) (d (n "sha3") (r "^0.10.6") (d #t) (k 0)))) (h "1srvmp6db1fahwc4hlic8s20l4yr1zqqz117n0lbv0bx6cd9im2n") (f (quote (("mock-vm" "odra/mock-vm") ("default" "mock-vm") ("casper-livenet" "odra/casper-livenet") ("casper" "odra/casper"))))))

(define-public crate-odra-modules-0.6.0 (c (n "odra-modules") (v "0.6.0") (d (list (d (n "hex") (r "^0.4.3") (k 0)) (d (n "odra") (r "^0.6.0") (k 0)) (d (n "sha3") (r "^0.10.6") (k 0)))) (h "1wvig40bsf5ypbana75vrk4fz6b2j386nzkzica4gsnn9yf4jja3") (f (quote (("std") ("mock-vm" "odra/mock-vm") ("default" "mock-vm") ("casper-livenet" "odra/casper-livenet") ("casper" "odra/casper"))))))

(define-public crate-odra-modules-0.7.0 (c (n "odra-modules") (v "0.7.0") (d (list (d (n "hex") (r "^0.4.3") (k 0)) (d (n "odra") (r "^0.7.0") (k 0)))) (h "1cc1xjkmc1fixbwsa6d83nmnkzkgqihfmc9rgk3nnvs890syiczi") (f (quote (("std") ("mock-vm" "odra/mock-vm") ("default" "mock-vm") ("casper-livenet" "odra/casper-livenet") ("casper" "odra/casper"))))))

(define-public crate-odra-modules-0.7.1 (c (n "odra-modules") (v "0.7.1") (d (list (d (n "hex") (r "^0.4.3") (k 0)) (d (n "odra") (r "^0.7.1") (k 0)))) (h "0gjwg44cb4fqmsapc43gd169gs61shxaig095a8y7qa8q22rsbxf") (f (quote (("std") ("mock-vm" "odra/mock-vm") ("default" "mock-vm") ("casper-livenet" "odra/casper-livenet") ("casper" "odra/casper"))))))

(define-public crate-odra-modules-0.8.0 (c (n "odra-modules") (v "0.8.0") (d (list (d (n "hex") (r "^0.4.3") (k 0)) (d (n "odra") (r "^0.8.0") (k 0)) (d (n "odra-test") (r "^0.8.0") (d #t) (k 2)))) (h "1x25wijy92jmrdk82zr4x5xraq5sbqj53mj1zz7vy50adlq1y3pl")))

(define-public crate-odra-modules-0.8.1 (c (n "odra-modules") (v "0.8.1") (d (list (d (n "hex") (r "^0.4.3") (k 0)) (d (n "odra") (r "^0.8.1") (k 0)) (d (n "odra-build") (r "^0.8.1") (d #t) (k 1)) (d (n "odra-test") (r "^0.8.1") (d #t) (k 2)))) (h "0wb8gdg9dhjnwwggks202gzjwwk8nnqi7m6w1iyzbanddr10wkr4") (f (quote (("disable-allocator" "odra/disable-allocator") ("default"))))))

(define-public crate-odra-modules-0.6.1 (c (n "odra-modules") (v "0.6.1") (d (list (d (n "hex") (r "^0.4.3") (k 0)) (d (n "odra") (r "^0.6.1") (k 0)) (d (n "sha3") (r "^0.10.6") (k 0)))) (h "1qnszzmpq2yqb927jc1dw3bin9dcgix46aklh4yqdvgvahm03f2w") (f (quote (("std") ("mock-vm" "odra/mock-vm") ("default" "mock-vm") ("casper-livenet" "odra/casper-livenet") ("casper" "odra/casper"))))))

(define-public crate-odra-modules-0.9.0 (c (n "odra-modules") (v "0.9.0") (d (list (d (n "hex") (r "^0.4.3") (k 0)) (d (n "odra") (r "^0.9.0") (k 0)) (d (n "odra-build") (r "^0.9.0") (d #t) (k 1)) (d (n "odra-test") (r "^0.9.0") (d #t) (k 2)))) (h "006l1r8kq7ijrq8b098qak4h34l7byl8by0qdgwqjb5f8f7cfrn6") (f (quote (("disable-allocator" "odra/disable-allocator") ("default"))))))

(define-public crate-odra-modules-0.9.1 (c (n "odra-modules") (v "0.9.1") (d (list (d (n "hex") (r "^0.4.3") (k 0)) (d (n "odra") (r "^0.9.1") (k 0)) (d (n "odra-build") (r "^0.9.1") (d #t) (k 1)) (d (n "odra-test") (r "^0.9.1") (d #t) (k 2)))) (h "023747w3qjdavyqzfk4hr0kfw47w8z9wpazrafnlksx9pap0l5b9") (f (quote (("disable-allocator" "odra/disable-allocator") ("default"))))))

(define-public crate-odra-modules-0.6.2 (c (n "odra-modules") (v "0.6.2") (d (list (d (n "hex") (r "^0.4.3") (k 0)) (d (n "odra") (r "^0.6.2") (k 0)) (d (n "sha3") (r "^0.10.6") (k 0)))) (h "1jzxxdwp39j3hc6ym7bz7ad2dmsslc5pg1gh29rc7d4h8z4c9jkq") (f (quote (("std") ("mock-vm" "odra/mock-vm") ("default" "mock-vm") ("casper-livenet" "odra/casper-livenet") ("casper" "odra/casper"))))))

(define-public crate-odra-modules-1.0.0-rc.1 (c (n "odra-modules") (v "1.0.0-rc.1") (d (list (d (n "base16") (r "^0.2.1") (k 0)) (d (n "base64") (r "^0.22.0") (f (quote ("alloc"))) (k 0)) (d (n "blake2") (r "^0.10.6") (d #t) (k 2)) (d (n "odra") (r "^1.0.0-rc.1") (k 0)) (d (n "odra-build") (r "^1.0.0-rc.1") (d #t) (k 1)) (d (n "odra-test") (r "^1.0.0-rc.1") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.80") (k 0)) (d (n "serde-json-wasm") (r "^1.0.1") (k 0)) (d (n "serde_json") (r "^1.0.59") (k 0)))) (h "0wab5j91pjz9a44c4pr9zirgg0ll62y7gy426r3fcdmjy7alaf1f") (f (quote (("disable-allocator" "odra/disable-allocator") ("default"))))))

(define-public crate-odra-modules-1.0.0 (c (n "odra-modules") (v "1.0.0") (d (list (d (n "base16") (r "^0.2.1") (k 0)) (d (n "base64") (r "^0.22.0") (f (quote ("alloc"))) (k 0)) (d (n "blake2") (r "^0.10.6") (d #t) (k 2)) (d (n "odra") (r "^1.0.0") (k 0)) (d (n "odra-build") (r "^1.0.0") (d #t) (k 1)) (d (n "odra-test") (r "^1.0.0") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.80") (k 0)) (d (n "serde-json-wasm") (r "^1.0.1") (k 0)) (d (n "serde_json") (r "^1.0.59") (k 0)))) (h "1ck6v306s9s7j5kjgad942l214vjyg0la4295f98hb50va9qgl74") (f (quote (("disable-allocator" "odra/disable-allocator") ("default"))))))

