(define-module (crates-io od ra odra-casper-types) #:use-module (crates-io))

(define-public crate-odra-casper-types-0.2.0 (c (n "odra-casper-types") (v "0.2.0") (d (list (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "odra-types") (r "^0.2.0") (d #t) (k 0)))) (h "0v6rxpxjqnly7rp4jgm87qx1fhrid11z7a1j7nh3w8sih61rxmh1")))

(define-public crate-odra-casper-types-0.3.0 (c (n "odra-casper-types") (v "0.3.0") (d (list (d (n "casper-event-standard") (r "^0.3.0") (d #t) (k 0)) (d (n "casper-types") (r "^2.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "odra-types") (r "^0.3.0") (d #t) (k 0)))) (h "180l351yw5gprqbnfg90k8afynszmlhd1bsn4p8b4p8vb3i0kpfi")))

(define-public crate-odra-casper-types-0.3.1 (c (n "odra-casper-types") (v "0.3.1") (d (list (d (n "casper-event-standard") (r "^0.3.0") (d #t) (k 0)) (d (n "casper-types") (r "^2.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "odra-types") (r "^0.3.1") (d #t) (k 0)))) (h "1sl5p500d2z2dqi5mv1vlyv53fnhwm0z8rncvk43jmxlx552gi2b")))

(define-public crate-odra-casper-types-0.4.0 (c (n "odra-casper-types") (v "0.4.0") (d (list (d (n "casper-types") (r "^3.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "odra-types") (r "^0.4.0") (d #t) (k 0)))) (h "01h8774kgh5na854rhy228ig2q3979kiymjvkxq93cs2dxnk7krx")))

(define-public crate-odra-casper-types-0.5.0 (c (n "odra-casper-types") (v "0.5.0") (d (list (d (n "casper-types") (r "^3.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "odra-types") (r "^0.5.0") (d #t) (k 0)))) (h "0qs3iw19mklyh5pj510m4j5090ca1ba8f427shlq0mzy7r0dn59y")))

(define-public crate-odra-casper-types-0.6.0 (c (n "odra-casper-types") (v "0.6.0") (d (list (d (n "casper-types") (r "^3.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "odra-types") (r "^0.6.0") (d #t) (k 0)))) (h "1bhp6clkp4fn2rj3fydya565ah8f6mhvll1sp0nvxby40w6qz54z")))

(define-public crate-odra-casper-types-0.6.1 (c (n "odra-casper-types") (v "0.6.1") (d (list (d (n "casper-types") (r "^3.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "odra-types") (r "^0.6.1") (d #t) (k 0)))) (h "12j991d525vp7q821flw83s95a4h468yicgb3n95jp9wwi64j6y0")))

(define-public crate-odra-casper-types-0.6.2 (c (n "odra-casper-types") (v "0.6.2") (d (list (d (n "casper-types") (r "^4.0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "odra-types") (r "^0.6.2") (d #t) (k 0)))) (h "1s6sf7256235sqyfq31mdgyzhilr4vh68gl6pypcx6x1w605s9dx")))

