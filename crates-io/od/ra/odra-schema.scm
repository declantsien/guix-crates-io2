(define-module (crates-io od ra odra-schema) #:use-module (crates-io))

(define-public crate-odra-schema-0.9.0 (c (n "odra-schema") (v "0.9.0") (d (list (d (n "casper-contract-schema") (r "^0.1.0") (d #t) (k 0)) (d (n "casper-types") (r "^3.0.0") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "odra-core") (r "^0.9.0") (d #t) (k 0)))) (h "1nlabxcjcnxm3db1q7lzw7yfwwd20wpv47b2p8kgi142irpryaby")))

(define-public crate-odra-schema-0.9.1 (c (n "odra-schema") (v "0.9.1") (d (list (d (n "casper-contract-schema") (r "^0.1.0") (d #t) (k 0)) (d (n "casper-types") (r "^3.0.0") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "odra-core") (r "^0.9.1") (d #t) (k 0)))) (h "10vyvrzma72d9w5fh2pqnizi9ck4c0inrj5xl81w8vidasg218kh")))

(define-public crate-odra-schema-1.0.0-rc.1 (c (n "odra-schema") (v "1.0.0-rc.1") (d (list (d (n "casper-contract-schema") (r "^0.1.0") (d #t) (k 0)) (d (n "casper-types") (r "^4.0.1") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "odra-core") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "11mm8mbk1wk2l4wc9c30apmzd88mvbm5jjj0mkjd229lj9vrvk7j")))

(define-public crate-odra-schema-1.0.0 (c (n "odra-schema") (v "1.0.0") (d (list (d (n "casper-contract-schema") (r "^0.1.0") (d #t) (k 0)) (d (n "casper-types") (r "^4.0.1") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "odra-core") (r "^1.0.0") (d #t) (k 0)))) (h "179smcba8pk5qq1c1krgvw2z9s9k70zp2sg481qpjhp2p9qf9al9")))

