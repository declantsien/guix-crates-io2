(define-module (crates-io od ra odra-vm) #:use-module (crates-io))

(define-public crate-odra-vm-0.8.0 (c (n "odra-vm") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "odra-core") (r "^0.8.0") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1bbyl4cp80hijmb11dqnk5q6w2czs0xfdpw5hiiamyy57m4fpqh0")))

(define-public crate-odra-vm-0.8.1 (c (n "odra-vm") (v "0.8.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "odra-core") (r "^0.8.1") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0yld4ixpzjdis7yf6y9dzcnhsw98gwsir637nfnh5bl7grp1988c")))

(define-public crate-odra-vm-0.9.0 (c (n "odra-vm") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "odra-core") (r "^0.9.0") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0h0fr2zgn0hnyg915b8rkmrccq145brs73nq7gm096mdcg0yvm78")))

(define-public crate-odra-vm-0.9.1 (c (n "odra-vm") (v "0.9.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "odra-core") (r "^0.9.1") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0r9blvcq228xqwn6frlj1crplgmhwx3riml0s7b9whbv3wvvw2k8")))

(define-public crate-odra-vm-1.0.0-rc.1 (c (n "odra-vm") (v "1.0.0-rc.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "odra-core") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1na1cpzwdd6py2jm11c8mmhfs5wb8wmdw58v3s4gxr5a6500b46j")))

(define-public crate-odra-vm-1.0.0 (c (n "odra-vm") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "odra-core") (r "^1.0.0") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0r47ayrvyfqrn2sj9zvqlva0rqp2mb4bknhvwk31zj8g9k4fgb0s")))

