(define-module (crates-io od ra odra-mock-vm-types) #:use-module (crates-io))

(define-public crate-odra-mock-vm-types-0.2.0 (c (n "odra-mock-vm-types") (v "0.2.0") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "odra-types") (r "^0.2.0") (d #t) (k 0)) (d (n "odra-utils") (r "^0.2.0") (d #t) (k 0)) (d (n "uint") (r "^0.9.4") (d #t) (k 0)))) (h "0ldjs5ld3j9f1xda0qzd5i09bnb1jwyb1yqcpy68nnhg8ph2aayp")))

(define-public crate-odra-mock-vm-types-0.3.0 (c (n "odra-mock-vm-types") (v "0.3.0") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "odra-types") (r "^0.3.0") (d #t) (k 0)) (d (n "odra-utils") (r "^0.3.0") (d #t) (k 0)) (d (n "uint") (r "^0.9.4") (d #t) (k 0)))) (h "1gxfwcxwwyf75n03rk49yp3m80vs2d4vc0plywvvmyglxdl13rjf")))

(define-public crate-odra-mock-vm-types-0.3.1 (c (n "odra-mock-vm-types") (v "0.3.1") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "odra-types") (r "^0.3.1") (d #t) (k 0)) (d (n "odra-utils") (r "^0.3.1") (d #t) (k 0)) (d (n "uint") (r "^0.9.4") (d #t) (k 0)))) (h "19p2w9hpq454gbk5rns1rbvmv1q2jpgqklm2ny7lrm4lx44yn6p2")))

(define-public crate-odra-mock-vm-types-0.4.0 (c (n "odra-mock-vm-types") (v "0.4.0") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "odra-types") (r "^0.4.0") (d #t) (k 0)) (d (n "odra-utils") (r "^0.4.0") (d #t) (k 0)) (d (n "uint") (r "^0.9.4") (d #t) (k 0)))) (h "07pm2a7ffy6h8i8hs75fr7ajm7v4m65n0s9q7v598yjz8hjrk2vk")))

(define-public crate-odra-mock-vm-types-0.5.0 (c (n "odra-mock-vm-types") (v "0.5.0") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "odra-types") (r "^0.5.0") (d #t) (k 0)) (d (n "odra-utils") (r "^0.5.0") (d #t) (k 0)) (d (n "uint") (r "^0.9.4") (d #t) (k 0)))) (h "0civraqij0zq82zba5fvhbydr6knwcb896irm7xsmz8nfyf02vls")))

(define-public crate-odra-mock-vm-types-0.6.0 (c (n "odra-mock-vm-types") (v "0.6.0") (d (list (d (n "borsh") (r "^0.9.3") (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "odra-types") (r "^0.6.0") (d #t) (k 0)) (d (n "odra-utils") (r "^0.6.0") (d #t) (k 0)) (d (n "uint") (r "^0.9.4") (d #t) (k 0)))) (h "0697i5mvhv1wr7qfw075iv1ybc7ybmzbyv6sdd3yqkq34jz56mid")))

(define-public crate-odra-mock-vm-types-0.6.1 (c (n "odra-mock-vm-types") (v "0.6.1") (d (list (d (n "borsh") (r "^0.9.3") (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "odra-types") (r "^0.6.1") (d #t) (k 0)) (d (n "odra-utils") (r "^0.6.1") (d #t) (k 0)) (d (n "uint") (r "^0.9.4") (d #t) (k 0)))) (h "0d6328xc65zlrigibz5ib0az4mim4rvy3a67dlbxqg2si61vvhcc")))

(define-public crate-odra-mock-vm-types-0.6.2 (c (n "odra-mock-vm-types") (v "0.6.2") (d (list (d (n "borsh") (r "^0.9.3") (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "odra-types") (r "^0.6.2") (d #t) (k 0)) (d (n "odra-utils") (r "^0.6.2") (d #t) (k 0)) (d (n "uint") (r "^0.9.4") (d #t) (k 0)))) (h "12pzwncm4jqhsg7xdl8a7q71xzddfg0kqq1ds53zz8ggkl40n6yd")))

