(define-module (crates-io od ra odra-types) #:use-module (crates-io))

(define-public crate-odra-types-0.0.1 (c (n "odra-types") (v "0.0.1") (d (list (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0w772pynzplkk0absqhlsl3002p857731x14a3dizr8a8pp91ai8")))

(define-public crate-odra-types-0.1.0 (c (n "odra-types") (v "0.1.0") (d (list (d (n "casper-types") (r "^1.5.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0mshxinx41as9v2x9q24irj2sk6k1hgwa6w58937fz4m392jp273")))

(define-public crate-odra-types-0.2.0 (c (n "odra-types") (v "0.2.0") (h "1l5jqk181gy77yl2ss5hsrr46097mysn9wzfis2kh4ly4x5pkkaa")))

(define-public crate-odra-types-0.3.0 (c (n "odra-types") (v "0.3.0") (h "05dn0w9dmd6mjh6kg46i158i32fw1f9bv61qfcf4xg2zy9mxgfm3")))

(define-public crate-odra-types-0.3.1 (c (n "odra-types") (v "0.3.1") (h "1iwhb3gkj745ar8w24q5jlc53p4v6p79jb6iza6vkynd53q4khjm")))

(define-public crate-odra-types-0.4.0 (c (n "odra-types") (v "0.4.0") (h "0d3160lz74vs851xm21sbcn8wvi50rw4y1c8ib44zn0wl24b2hqj")))

(define-public crate-odra-types-0.5.0 (c (n "odra-types") (v "0.5.0") (h "1wcndddsaawbdh9nb8fc9q3kjqzgv7c6bsivkii6ngi36p837kah")))

(define-public crate-odra-types-0.6.0 (c (n "odra-types") (v "0.6.0") (d (list (d (n "odra-utils") (r "^0.6.0") (d #t) (k 2)))) (h "04dl9dld5sq5yyix8wmmmgbz134fjd16n3g7l634xxm1hi245fkc")))

(define-public crate-odra-types-0.7.0 (c (n "odra-types") (v "0.7.0") (d (list (d (n "casper-types") (r "^3.0.0") (d #t) (k 0)) (d (n "odra-utils") (r "^0.7.0") (d #t) (k 2)))) (h "0qnlnmyl7g5vr44bcjh5wdpm6paskmif969irn0c1yv4l4y77ddi") (f (quote (("test-support") ("default"))))))

(define-public crate-odra-types-0.7.1 (c (n "odra-types") (v "0.7.1") (d (list (d (n "casper-types") (r "^3.0.0") (d #t) (k 0)) (d (n "odra-utils") (r "^0.7.1") (d #t) (k 2)))) (h "1vmkdvxmp1fqdzkdswz06x3npz54aswnfk34hqzmy1gkq2ckwj7n") (f (quote (("test-support") ("default"))))))

(define-public crate-odra-types-0.6.1 (c (n "odra-types") (v "0.6.1") (d (list (d (n "odra-utils") (r "^0.6.1") (d #t) (k 2)))) (h "1i1pkjnj87zdijfwx3jc5mx53rl80nlfv2q12h08yzh9n44ww6fq")))

(define-public crate-odra-types-0.6.2 (c (n "odra-types") (v "0.6.2") (d (list (d (n "odra-utils") (r "^0.6.2") (d #t) (k 2)))) (h "16a9nbf65pl9hj86660b38yn2rghq5rwrmzgs5hh31awga03f82r")))

