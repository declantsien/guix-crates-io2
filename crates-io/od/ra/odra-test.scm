(define-module (crates-io od ra odra-test) #:use-module (crates-io))

(define-public crate-odra-test-0.8.0 (c (n "odra-test") (v "0.8.0") (d (list (d (n "odra-casper-test-vm") (r "^0.8.0") (d #t) (k 0)) (d (n "odra-core") (r "^0.8.0") (d #t) (k 0)) (d (n "odra-vm") (r "^0.8.0") (d #t) (k 0)))) (h "10p4w80qd0k3sgz6jxwmhrwrqdvjsnjv1x2fvn5mly4wddnifm80")))

(define-public crate-odra-test-0.8.1 (c (n "odra-test") (v "0.8.1") (d (list (d (n "odra-casper-test-vm") (r "^0.8.1") (d #t) (k 0)) (d (n "odra-core") (r "^0.8.1") (d #t) (k 0)) (d (n "odra-vm") (r "^0.8.1") (d #t) (k 0)))) (h "1nj11558y21lx9sb9b60ji7r9y07qzmmkabspr5mg8dnniv4kr6b")))

(define-public crate-odra-test-0.9.0 (c (n "odra-test") (v "0.9.0") (d (list (d (n "odra-casper-test-vm") (r "^0.9.0") (d #t) (k 0)) (d (n "odra-core") (r "^0.9.0") (d #t) (k 0)) (d (n "odra-vm") (r "^0.9.0") (d #t) (k 0)))) (h "0cw60d2npvm9vf85rdxg719va32zqp1m29dpjxb9gcyv5sisib7s")))

(define-public crate-odra-test-0.9.1 (c (n "odra-test") (v "0.9.1") (d (list (d (n "odra-casper-test-vm") (r "^0.9.1") (d #t) (k 0)) (d (n "odra-core") (r "^0.9.1") (d #t) (k 0)) (d (n "odra-vm") (r "^0.9.1") (d #t) (k 0)))) (h "0amdna91n05gamiv9gjwfbk1yd5w0b0j4d68inigv63jiib4fr5x")))

(define-public crate-odra-test-1.0.0-rc.1 (c (n "odra-test") (v "1.0.0-rc.1") (d (list (d (n "odra-casper-test-vm") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "odra-core") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "odra-vm") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "13361ykc2451grjmyp35igfdjxjgjwb342cf58gri68afi7a4b41")))

(define-public crate-odra-test-1.0.0 (c (n "odra-test") (v "1.0.0") (d (list (d (n "odra-casper-test-vm") (r "^1.0.0") (d #t) (k 0)) (d (n "odra-core") (r "^1.0.0") (d #t) (k 0)) (d (n "odra-vm") (r "^1.0.0") (d #t) (k 0)))) (h "08bp7gpdgkwjbg9pnh13wlw3z0lqpzbyd1mh3z29hkyf3ask28q9")))

