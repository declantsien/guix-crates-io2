(define-module (crates-io od es odesli-rs) #:use-module (crates-io))

(define-public crate-odesli-rs-1.0.0 (c (n "odesli-rs") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("gzip" "json" "deflate"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)))) (h "00yfvlzmmz3qvx7jlkq9bvpmah36bzx6kcv68jrsydrcydww6k4n")))

(define-public crate-odesli-rs-2.0.0 (c (n "odesli-rs") (v "2.0.0") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("gzip" "json" "deflate"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jw8fbwajb51xfdwadxz1ih272f596q0vq1y2jslypmbs3r1f8x0")))

(define-public crate-odesli-rs-3.0.0 (c (n "odesli-rs") (v "3.0.0") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("gzip" "json" "deflate"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hi67pwfnhskqbzmm9w9296bwdy3vx6f76xsnxd8091bbwzl1rx2")))

(define-public crate-odesli-rs-4.0.0 (c (n "odesli-rs") (v "4.0.0") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("gzip" "json" "deflate"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "13g65xxhrk2y28ihdn47l2k616fxgk87cd4d9s5sncigqs90kxqw")))

(define-public crate-odesli-rs-4.0.1 (c (n "odesli-rs") (v "4.0.1") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("gzip" "json" "deflate"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "09bw0r1zva0vjq7nkz19p1bpcgs8sqliy3rxppw6lb9ykgl0zyyd")))

(define-public crate-odesli-rs-4.1.0 (c (n "odesli-rs") (v "4.1.0") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("gzip" "json" "deflate"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zaiac3vivla17030vxcqqprlswby6ldb2933h31ig2gy4lqhj65")))

(define-public crate-odesli-rs-5.0.0 (c (n "odesli-rs") (v "5.0.0") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("gzip" "json" "deflate"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16yi5m1a7p0sd72ykgdjr39p1srbdacnras1d3cwy13yii218ipc")))

