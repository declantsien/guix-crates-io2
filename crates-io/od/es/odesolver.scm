(define-module (crates-io od es odesolver) #:use-module (crates-io))

(define-public crate-odesolver-0.1.0 (c (n "odesolver") (v "0.1.0") (h "0n8vsisp382nrzpp13496zgamvlagzsk14imnc2jirz6fff52nf6")))

(define-public crate-odesolver-0.1.1 (c (n "odesolver") (v "0.1.1") (h "0qlkrk3nl8ddychpmha9avak0h6yp6257m1ggv5drh4mnamasgzb")))

(define-public crate-odesolver-0.1.2 (c (n "odesolver") (v "0.1.2") (h "16kankyl5ra4b3lbsafm17nch3r3galnfwm5z792d61dc7mqcsjm")))

(define-public crate-odesolver-0.1.3 (c (n "odesolver") (v "0.1.3") (d (list (d (n "dyn-clone") (r "^1.0.10") (d #t) (k 0)))) (h "0ndgn640v4gfpq99p6d73lbrsvi8737nhyvki07r48lp2q4pm94m")))

(define-public crate-odesolver-0.1.4 (c (n "odesolver") (v "0.1.4") (d (list (d (n "dyn-clone") (r "^1.0.10") (d #t) (k 0)))) (h "11cc9ggfzjhwbhjwp5w8xyiix6bi8qw3cv2v991x5djp70v2jnd9")))

(define-public crate-odesolver-0.1.5 (c (n "odesolver") (v "0.1.5") (d (list (d (n "dyn-clone") (r "^1.0.10") (d #t) (k 0)))) (h "1b88kyylrzc8sh2yy93wpg6md1ikc6ypmnw7dpkjqdca1yd0z6dw")))

