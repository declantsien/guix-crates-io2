(define-module (crates-io od oo odoors) #:use-module (crates-io))

(define-public crate-odoors-0.1.0 (c (n "odoors") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "02s1q3qlc9dvd6hynflmnbdrdhrgzqzmz87qpcdly29vpny0y9h2")))

(define-public crate-odoors-0.2.0 (c (n "odoors") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vscgsl8k0qzj97z3r2g5l3r019vhp3xrxvnxgfyqgv61sifnxlk")))

(define-public crate-odoors-0.3.0 (c (n "odoors") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "serde_with") (r "^1.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0dcqgl649qi5cq7jykcqpwdmnpwmikx9520m2493hxl159f1lm4i")))

(define-public crate-odoors-0.4.0 (c (n "odoors") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "serde_with") (r "^1.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1yfsqsck2dcmv07aa86ifq95bhnzgn575srq397kq6n7i6j5y108")))

(define-public crate-odoors-0.5.0 (c (n "odoors") (v "0.5.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "serde_with") (r "^1.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "13764k9l3gf8kfjlgjrfxj8w07gqmbqxqfmiyx8029lrq27n1iik")))

