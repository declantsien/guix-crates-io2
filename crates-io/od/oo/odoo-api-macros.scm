(define-module (crates-io od oo odoo-api-macros) #:use-module (crates-io))

(define-public crate-odoo-api-macros-0.1.0 (c (n "odoo-api-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fibdz61zvmn6vr9z407zh1nmr5dlrxaxix5can0gmxqya8j4dsa")))

(define-public crate-odoo-api-macros-0.1.1 (c (n "odoo-api-macros") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zrm0ny4lllav6ysr4vj0an74zvb4sg66vlakpf8i63x6w5ryjwf")))

(define-public crate-odoo-api-macros-0.1.2 (c (n "odoo-api-macros") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dq5jimik2isxbx0v2lc55f4ibm401z7dxi08ck68pifnhr53yha")))

(define-public crate-odoo-api-macros-0.2.0 (c (n "odoo-api-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1qlqzbmih7c4cr2a0m3z8aqisldls3lg4kld8ccvkw2lc4bsjfx8")))

(define-public crate-odoo-api-macros-0.2.1 (c (n "odoo-api-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "18yvh5hmg9d37957hv8g8v61ygk3hsr0iqzbw0473z785frjxvkv")))

