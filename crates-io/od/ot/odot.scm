(define-module (crates-io od ot odot) #:use-module (crates-io))

(define-public crate-odot-0.1.0 (c (n "odot") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "~2.32") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0bnr70pk50s7djb3c7g6hi1z8lxc6chxs9sxq0a10d1kdihhx4rg")))

(define-public crate-odot-0.1.1 (c (n "odot") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.37") (d #t) (k 0)) (d (n "uuid") (r "^0.7.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1w4vpyarpxim5gmq085n2sbjbfi5v5zwfjjgbd5hjhi3ysq1ir90")))

