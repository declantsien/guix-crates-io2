(define-module (crates-io od ys odysseus_codegen) #:use-module (crates-io))

(define-public crate-odysseus_codegen-0.0.0 (c (n "odysseus_codegen") (v "0.0.0") (h "0mm1zvns9ldfk205s263s3mgwfprqp9hi9alqckpsr3jczwgcm84") (y #t)))

(define-public crate-odysseus_codegen-0.0.1 (c (n "odysseus_codegen") (v "0.0.1") (h "14h6lalaa5jh014vc5vy2g60pmpyfgp60zr8qks4qy2cx2l183c1") (y #t)))

