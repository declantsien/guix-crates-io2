(define-module (crates-io od ys odysseus_codegen_impl) #:use-module (crates-io))

(define-public crate-odysseus_codegen_impl-0.0.0 (c (n "odysseus_codegen_impl") (v "0.0.0") (h "0sbl525gf8avh8j0pilfajnas77s3ym5x0b42ypcw9q52bcwppqc") (y #t)))

(define-public crate-odysseus_codegen_impl-0.0.1 (c (n "odysseus_codegen_impl") (v "0.0.1") (h "1bawdx4c9dzcmjlygcm6dp4xmqp39sd47a81ldg1fbrmlghylrzk") (y #t)))

