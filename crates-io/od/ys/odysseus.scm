(define-module (crates-io od ys odysseus) #:use-module (crates-io))

(define-public crate-odysseus-0.0.0 (c (n "odysseus") (v "0.0.0") (h "0spgzfnp5qdlmsb8wgw2m8ljqvf8xfr1yr25qfg9i6cvllp3kady") (y #t)))

(define-public crate-odysseus-0.0.1 (c (n "odysseus") (v "0.0.1") (h "10wz74ifgrpgl9v4smgdxlir8vssi0izl5clx620wl65739pyjw4") (y #t)))

