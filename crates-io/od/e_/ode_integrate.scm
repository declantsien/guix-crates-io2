(define-module (crates-io od e_ ode_integrate) #:use-module (crates-io))

(define-public crate-ode_integrate-0.0.1 (c (n "ode_integrate") (v "0.0.1") (d (list (d (n "nalgebra") (r "^0.31") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 2)))) (h "1ix72ycwxcs6h12k38xighljyn3nflcxk086z9iav889611qnzr3")))

(define-public crate-ode_integrate-0.0.2 (c (n "ode_integrate") (v "0.0.2") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "arrayfire") (r "^3.8") (d #t) (k 2)) (d (n "f128") (r "^0.2") (d #t) (k 2)) (d (n "half") (r "^2.1") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 2)))) (h "1ad5cf5422zcgwssgsfx875lvjkjq5h9ma6z1vrkya4v9b6sdw58")))

