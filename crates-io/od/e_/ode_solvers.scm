(define-module (crates-io od e_ ode_solvers) #:use-module (crates-io))

(define-public crate-ode_solvers-0.1.0 (c (n "ode_solvers") (v "0.1.0") (d (list (d (n "alga") (r "^0.7.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.0") (d #t) (k 0)))) (h "1aq9zb969294svncmjjg5cbz0df1z3l082xr8hyqrk2sw9mfiyl9")))

(define-public crate-ode_solvers-0.1.1 (c (n "ode_solvers") (v "0.1.1") (d (list (d (n "alga") (r "^0.7.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.0") (d #t) (k 0)))) (h "1713b7jz5617rdcdskgynlf1yhjbk356s3bdf3k6vv8idfzkziqb")))

(define-public crate-ode_solvers-0.1.2 (c (n "ode_solvers") (v "0.1.2") (d (list (d (n "alga") (r "^0.7.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16.5") (d #t) (k 0)))) (h "1bdigfq6gk8nkgs49nx5df0lzhnfrfahvsrjqfj5i9ippfiy8qdz")))

(define-public crate-ode_solvers-0.2.0 (c (n "ode_solvers") (v "0.2.0") (d (list (d (n "alga") (r "^0.8.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.17.2") (d #t) (k 0)))) (h "0yya5xdrw1frhns6y11f55frhkwfy3kclxmwkf572q8m5r139j18")))

(define-public crate-ode_solvers-0.3.0 (c (n "ode_solvers") (v "0.3.0") (d (list (d (n "alga") (r "^0.8.2") (d #t) (k 0)) (d (n "nalgebra") (r "^0.17.3") (d #t) (k 0)))) (h "0277xdvm8yhzqjnimi7q5j03jbgihznjwg011j34zxjkahsw70hx")))

(define-public crate-ode_solvers-0.3.1 (c (n "ode_solvers") (v "0.3.1") (d (list (d (n "nalgebra") (r "^0.27.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "simba") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "01axszgfrs84n6c1msk59c3cmxml52pkxpbp96a0pwsxqj3arziv")))

(define-public crate-ode_solvers-0.3.2 (c (n "ode_solvers") (v "0.3.2") (d (list (d (n "nalgebra") (r "^0.27.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "simba") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0c30f43nyfgm5dwy7vmsal8wx215cr6kc4jlbhl0vby3x25qvzgl")))

(define-public crate-ode_solvers-0.3.3 (c (n "ode_solvers") (v "0.3.3") (d (list (d (n "nalgebra") (r "^0.27.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "simba") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0msidb4ynck8j1a7pxwzrgflvgwgk44iav0d10ahw3vqpa2sl4in")))

(define-public crate-ode_solvers-0.3.4 (c (n "ode_solvers") (v "0.3.4") (d (list (d (n "nalgebra") (r "^0.29.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "simba") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1kb2lidcwy932p1jv4kyk6p3zkgc0qxys8zz85xwwdxwmp1472xj")))

(define-public crate-ode_solvers-0.3.5 (c (n "ode_solvers") (v "0.3.5") (d (list (d (n "nalgebra") (r "^0.29.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "simba") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "11b3msb4jkwl3gh86h7jjvzirj82kyzqbydn2fafg4pah2nxn59a")))

(define-public crate-ode_solvers-0.3.6 (c (n "ode_solvers") (v "0.3.6") (d (list (d (n "nalgebra") (r "^0.30.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "simba") (r "^0.7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0zj5s8wl9ffkrnqrpxd25wh6jayisfyzmnsvpw02ldznbhwz9xpg")))

(define-public crate-ode_solvers-0.3.7 (c (n "ode_solvers") (v "0.3.7") (d (list (d (n "nalgebra") (r "^0.31.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "simba") (r "^0.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0gyrvgjfphwjg94qs6qs88whxwj45brksdl05wya86b185z796w3")))

(define-public crate-ode_solvers-0.4.0 (c (n "ode_solvers") (v "0.4.0") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "simba") (r "^0.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0bmgd3wly6d10d45ai5azqfqnpqxdf49nbgzwi23v2ypk0w1kad4")))

