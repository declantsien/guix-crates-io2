(define-module (crates-io od s2 ods2sql) #:use-module (crates-io))

(define-public crate-ods2sql-0.1.0 (c (n "ods2sql") (v "0.1.0") (d (list (d (n "calamine") (r "^0.14") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13") (d #t) (k 0)))) (h "01n4vga04vmmy59vdfjdny65ic6kf8zzypnq6r679fcrk8l0di0c")))

(define-public crate-ods2sql-0.1.1 (c (n "ods2sql") (v "0.1.1") (d (list (d (n "calamine") (r "^0.14") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.14.0") (d #t) (k 0)))) (h "0djny78d2vwr77y643rw5791ggdp2vxahxkvcv05hzfgdqgv3qsr")))

(define-public crate-ods2sql-0.2.0 (c (n "ods2sql") (v "0.2.0") (d (list (d (n "calamine") (r "^0.15") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.16") (d #t) (k 0)))) (h "17dd1iyfvqfdjw5x9sc7rbpfz85s6ngkcrz0l7gyimzin35q703m")))

(define-public crate-ods2sql-0.3.0 (c (n "ods2sql") (v "0.3.0") (d (list (d (n "calamine") (r "^0.15") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.16") (d #t) (k 0)))) (h "0h0pf0yq3xaa4wh9vwkdhgsqpnwbm0cpcf53q5fz5gn0z4ydf1gx")))

(define-public crate-ods2sql-0.3.1 (c (n "ods2sql") (v "0.3.1") (d (list (d (n "calamine") (r "^0.16") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.24") (d #t) (k 0)))) (h "0kkp935fdrsgcr5r0hg1dq3k16kv593akb8jpc3hj6ngwr4nqwrj")))

(define-public crate-ods2sql-0.4.0 (c (n "ods2sql") (v "0.4.0") (d (list (d (n "calamine") (r "^0.16") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.24") (d #t) (k 0)))) (h "0qiw7kk0bi1rnw67skbjj6wq9pdhcv21b019fjw8b3w91rlyizmm")))

