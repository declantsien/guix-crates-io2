(define-module (crates-io od at odata_client_util) #:use-module (crates-io))

(define-public crate-odata_client_util-0.1.0 (c (n "odata_client_util") (v "0.1.0") (d (list (d (n "iso8601") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.6") (d #t) (k 0)))) (h "0pbpq8z9vgsw97k47wgni30rb239pa67jwjxs5avv0lh97x59yiv")))

