(define-module (crates-io od at odata-rust-generator) #:use-module (crates-io))

(define-public crate-odata-rust-generator-0.1.0 (c (n "odata-rust-generator") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "codegen") (r "^0.1.3") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "odata-parser-rs") (r "^0.1.1") (d #t) (k 0)))) (h "13hycwhlxfjnj8yx0a1qmkdkwz3cj98kfdbgjdv35zaai80lqa65")))

(define-public crate-odata-rust-generator-0.1.1 (c (n "odata-rust-generator") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "codegen") (r "^0.1.3") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "odata-parser-rs") (r "^0.1.1") (d #t) (k 0)))) (h "12qg5v39k5qks79wpc97djy1yb64fslirb540dg59iybs452bxlg")))

(define-public crate-odata-rust-generator-0.2.0 (c (n "odata-rust-generator") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "codegen") (r "^0.1.3") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "odata-parser-rs") (r "^0.1.1") (d #t) (k 0)))) (h "1d6jrhr6zzhcj30b7yin2fd6as9kl5zpdf382z5x4mccpqjkzr73")))

(define-public crate-odata-rust-generator-0.3.0 (c (n "odata-rust-generator") (v "0.3.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "codegen") (r "^0.1.3") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "odata-parser-rs") (r "^0.1.1") (d #t) (k 0)))) (h "0ma6mxb02fg5sh9615g042xm201h2hw2m6dsdnzan0yqlb3m7w7s")))

(define-public crate-odata-rust-generator-0.3.1 (c (n "odata-rust-generator") (v "0.3.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "codegen") (r "^0.1.3") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "odata-parser-rs") (r "^0.1.1") (d #t) (k 0)))) (h "1jygzcx50qngv9a3ydp135zxgbh73k9m0dyzsqcp8rncp86a39bx")))

(define-public crate-odata-rust-generator-0.3.2 (c (n "odata-rust-generator") (v "0.3.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "codegen") (r "^0.1.3") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "odata-parser-rs") (r "^0.1.1") (d #t) (k 0)))) (h "0l88rys005l7pc63ywi4ach75phn55h15g2a820gky00v9m6498n")))

(define-public crate-odata-rust-generator-0.3.3 (c (n "odata-rust-generator") (v "0.3.3") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "codegen") (r "^0.1.3") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "odata-parser-rs") (r "^0.1.1") (d #t) (k 0)))) (h "069pras12qgfk1hn1vl4acfs9x8ij1npwywyvzglk5vyra8718ws")))

(define-public crate-odata-rust-generator-0.4.0 (c (n "odata-rust-generator") (v "0.4.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "codegen") (r "^0.1.3") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "odata-parser-rs") (r "^0.1.1") (d #t) (k 0)))) (h "0f5rpzmsnb9zpc5ncq9d111lxi4gz5jck1ja7x2zcwy5krdwqj1q")))

(define-public crate-odata-rust-generator-0.6.0 (c (n "odata-rust-generator") (v "0.6.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "codegen") (r "^0.1.4") (d #t) (k 0) (p "codegen2")) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "odata-parser-rs") (r "^0.1.2") (d #t) (k 0)))) (h "1fym81n1kw2qfaijfdylk21yiyadfgwn4mrfnnhs09fq54yb41i8")))

