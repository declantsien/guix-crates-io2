(define-module (crates-io od at odata_client_derive) #:use-module (crates-io))

(define-public crate-odata_client_derive-0.1.0 (c (n "odata_client_derive") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "odata_client_codegen") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "default-tls"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "10c5n6nraa1yg31xmg5f4n23jv4q0waav8ffiha2xf6acbjcpj23")))

