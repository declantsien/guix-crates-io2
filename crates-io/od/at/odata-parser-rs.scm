(define-module (crates-io od at odata-parser-rs) #:use-module (crates-io))

(define-public crate-odata-parser-rs-0.1.0 (c (n "odata-parser-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y86idjfww64vgjk8prp6rscyq3mnkw50myic09xlp8x3pw6gsgc")))

(define-public crate-odata-parser-rs-0.1.1 (c (n "odata-parser-rs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "11lgi4ammscp4y9zfbyjk1rqdr9r1cyyykh5fzx7700il9jxl1wk")))

(define-public crate-odata-parser-rs-0.1.2 (c (n "odata-parser-rs") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z40ngq4l13kki0yi49hzfhqhcj9sxldd4irkg3wzlrlc1fkl38d")))

