(define-module (crates-io od _o od_opencv) #:use-module (crates-io))

(define-public crate-od_opencv-0.1.1 (c (n "od_opencv") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "opencv") (r "^0.66") (f (quote ("dnn" "imgcodecs" "imgproc"))) (k 0)))) (h "11n401qjvf1rz9mabp4nj1j3h65jj2dqlr92g0v6i1b9d5wf9l4k")))

(define-public crate-od_opencv-0.1.2 (c (n "od_opencv") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "opencv") (r "^0.66") (f (quote ("dnn" "imgcodecs" "imgproc"))) (k 0)))) (h "13k62r0dz61l0scpxi5ri7i8c9v7jz1yqs49g0y30q7l5627fynn") (r "1.73")))

(define-public crate-od_opencv-0.1.3 (c (n "od_opencv") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "opencv") (r "^0.66") (f (quote ("dnn" "imgcodecs" "imgproc"))) (k 0)))) (h "10zv1gq1lc9ssbc3f6sp5y9s1mxjlxd4qpffvky3ln3ki2a1hhkq") (r "1.73")))

(define-public crate-od_opencv-0.1.4 (c (n "od_opencv") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "opencv") (r "^0.66") (f (quote ("dnn" "imgcodecs" "imgproc"))) (k 0)))) (h "1lwiz3fkj3jczfvk26k82ycpkhldvkhyibsyal0kdzmahdqz4n67") (r "1.73")))

(define-public crate-od_opencv-0.1.5 (c (n "od_opencv") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "opencv") (r "^0.88.5") (f (quote ("dnn" "imgcodecs" "imgproc"))) (k 0)))) (h "1c8vw0p2zykjpli578cmpjzg53dcgwg29v6c84g9qdjfmn6b3wgy") (r "1.73")))

(define-public crate-od_opencv-0.1.6 (c (n "od_opencv") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "opencv") (r "^0.91.3") (f (quote ("dnn" "imgcodecs" "imgproc"))) (k 0)))) (h "1y6slvjgpymxxmnx0kianz25k1vgsj30p8489i7krhd0bc0jgsac") (r "1.73")))

