(define-module (crates-io od di oddio) #:use-module (crates-io))

(define-public crate-oddio-0.1.0 (c (n "oddio") (v "0.1.0") (h "12sy229kv5lqhxl7gkm8l7xicqyn7i9m95rv61bjb83mx5m2szy0")))

(define-public crate-oddio-0.1.1 (c (n "oddio") (v "0.1.1") (d (list (d (n "cpal") (r "^0.13.1") (d #t) (k 2)) (d (n "mint") (r "^0.5.5") (d #t) (k 0)) (d (n "wav") (r "^0.4") (d #t) (k 2)))) (h "0jmr7drxavhnh3abzgaxpk4knyn1i24ps4fybidcj4jzg7yzjqx1")))

(define-public crate-oddio-0.2.0 (c (n "oddio") (v "0.2.0") (d (list (d (n "cpal") (r "^0.13.1") (d #t) (k 2)) (d (n "mint") (r "^0.5.5") (d #t) (k 0)) (d (n "wav") (r "^0.4") (d #t) (k 2)))) (h "1vk5a6b2n0mm78lmkh6qxm7gsg3h3wg6a4rhgsdy92armnaq174b")))

(define-public crate-oddio-0.3.0 (c (n "oddio") (v "0.3.0") (d (list (d (n "cpal") (r "^0.13.1") (d #t) (k 2)) (d (n "mint") (r "^0.5.5") (d #t) (k 0)) (d (n "wav") (r "^0.4") (d #t) (k 2)))) (h "1i2ijqr5mm7w2mi28qmf04w4vsy8cjv5fggnypd6l0a0msd7s7pp")))

(define-public crate-oddio-0.4.0 (c (n "oddio") (v "0.4.0") (d (list (d (n "cpal") (r "^0.13.1") (d #t) (k 2)) (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "mint") (r "^0.5.5") (d #t) (k 0)))) (h "11drb1lyh8rxlv8mwwkd8vsm9yqrdiak33aa7kh7fsj2kxzzwn2k") (y #t)))

(define-public crate-oddio-0.4.1 (c (n "oddio") (v "0.4.1") (d (list (d (n "cpal") (r "^0.13.1") (d #t) (k 2)) (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "mint") (r "^0.5.5") (d #t) (k 0)))) (h "0yiq934alww5vpg830d2fvbfhwl7083sgb4rp3f56bssp2314r7f")))

(define-public crate-oddio-0.5.0 (c (n "oddio") (v "0.5.0") (d (list (d (n "cpal") (r "^0.13.1") (d #t) (k 2)) (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.5") (d #t) (k 0)))) (h "081ydx0mqgc9zlakpnspwwqi8kzir6yiqx525fq24qzj52hxbxik") (f (quote (("no_std" "libm"))))))

(define-public crate-oddio-0.6.0 (c (n "oddio") (v "0.6.0") (d (list (d (n "cpal") (r "^0.13.1") (d #t) (k 2)) (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.5") (d #t) (k 0)))) (h "0k07px8dl025swd8qs1vkdlk2jnqary3z03d5x9xgmkjc2irazzj") (f (quote (("no_std" "libm"))))))

(define-public crate-oddio-0.6.1 (c (n "oddio") (v "0.6.1") (d (list (d (n "cpal") (r "^0.13.1") (d #t) (k 2)) (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.5") (d #t) (k 0)))) (h "1wlg08mvqa1sqsv23gsn59sa3g1h0wx3039ji9yr155sdsprcbcl") (f (quote (("no_std" "libm"))))))

(define-public crate-oddio-0.6.2 (c (n "oddio") (v "0.6.2") (d (list (d (n "cpal") (r "^0.13.1") (d #t) (k 2)) (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.5") (d #t) (k 0)))) (h "0d67n2hzlij6zq774fw4jrlc9bzsfvr11qx0sjqd0m78qyf3gwzh") (f (quote (("no_std" "libm"))))))

(define-public crate-oddio-0.7.0 (c (n "oddio") (v "0.7.0") (d (list (d (n "cpal") (r "^0.13.1") (d #t) (k 2)) (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.5") (d #t) (k 0)))) (h "1aiq9qfyfffyh5a7qlk8psv4cb6nmd4w5fnlxi7k73vm53lhv6x8") (f (quote (("no_std" "libm"))))))

(define-public crate-oddio-0.7.1 (c (n "oddio") (v "0.7.1") (d (list (d (n "cpal") (r "^0.13.1") (d #t) (k 2)) (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.5") (d #t) (k 0)))) (h "1ymbdzk5vlxkvmwr2wy1pd5ky0kfick432mqaq0c11y8mw4zvdpm") (f (quote (("no_std" "libm"))))))

(define-public crate-oddio-0.7.2 (c (n "oddio") (v "0.7.2") (d (list (d (n "cpal") (r "^0.13.1") (d #t) (k 2)) (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.5") (d #t) (k 0)))) (h "1zn83kq0dzzcq5d6nb4zzk8lr8gi074rwiqs4xhpxms0xyk865d6") (f (quote (("no_std" "libm"))))))

(define-public crate-oddio-0.7.3 (c (n "oddio") (v "0.7.3") (d (list (d (n "cpal") (r "^0.13.1") (d #t) (k 2)) (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.5") (d #t) (k 0)))) (h "1b4ancjab4rqnbkyr56i1xgqy4fhldgf42cslskbihjm5xw0i22s") (f (quote (("no_std" "libm"))))))

(define-public crate-oddio-0.7.4 (c (n "oddio") (v "0.7.4") (d (list (d (n "cpal") (r "^0.13.1") (d #t) (k 2)) (d (n "hound") (r "^3.4") (d #t) (k 2)) (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "mint") (r "^0.5.5") (d #t) (k 0)))) (h "1wjc6k5gy6ski7x5rb8ndzgbvm26gaccmbargvc3px8i0nvb51y2") (f (quote (("no_std" "libm"))))))

