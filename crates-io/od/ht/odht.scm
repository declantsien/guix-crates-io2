(define-module (crates-io od ht odht) #:use-module (crates-io))

(define-public crate-odht-0.1.0 (c (n "odht") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.2") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)))) (h "0pr9pqr32d65g1ywqdqgdrv0zarr5vrby79550a1ada13fclgrci") (f (quote (("no_simd") ("nightly"))))))

(define-public crate-odht-0.2.0 (c (n "odht") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.2") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)))) (h "0l4y9km8iqp6yacsqf7y7vmp1cjk1sbb3ln8nkn7jqmzhcklpc75") (f (quote (("no_simd") ("nightly"))))))

(define-public crate-odht-0.2.1 (c (n "odht") (v "0.2.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.2") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)))) (h "042hc8zwlm3w4m284fkw18aw42ynyy7d024pqjsyglqrr78sh60b") (f (quote (("no_simd") ("nightly"))))))

(define-public crate-odht-0.3.0 (c (n "odht") (v "0.3.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.2") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)))) (h "0hh1ray6w4abcv5rd08qgi00wrmb6lj3j9gmkqpky2x4zlllsl6j") (f (quote (("no_simd") ("nightly"))))))

(define-public crate-odht-0.3.1 (c (n "odht") (v "0.3.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.2") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)))) (h "1fqscbbvx0krlpf96n31f67lk20ypa72dl14jrb5pchlmh4qhlas") (f (quote (("no_simd") ("nightly"))))))

