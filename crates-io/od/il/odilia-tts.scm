(define-module (crates-io od il odilia-tts) #:use-module (crates-io))

(define-public crate-odilia-tts-0.1.0 (c (n "odilia-tts") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "ssip-client-async") (r "^0.9.0") (f (quote ("tokio"))) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("sync" "macros" "rt" "signal" "tracing"))) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "01zng8yhr2c03wxq9gwfn9aii4m4a22y4s5smmpgvfn704rb017m")))

(define-public crate-odilia-tts-0.1.1 (c (n "odilia-tts") (v "0.1.1") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "ssip-client-async") (r "^0.10.0") (f (quote ("tokio"))) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("sync" "macros" "rt" "signal" "tracing"))) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "170mb1n91p02vvyj3iw8yx8jz4qf5zqlvqpawygm5hdyic7r5fs7")))

(define-public crate-odilia-tts-0.1.2 (c (n "odilia-tts") (v "0.1.2") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "ssip-client-async") (r "^0.10.0") (f (quote ("tokio"))) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("sync" "macros" "rt" "signal" "tracing"))) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "154giyk75ajr1j0cxyr5bpd5gx54ipmmf7c489w3wdw074xm0qag")))

(define-public crate-odilia-tts-0.1.3 (c (n "odilia-tts") (v "0.1.3") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "ssip-client-async") (r "^0.10.0") (f (quote ("tokio"))) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("sync" "macros" "rt" "signal" "tracing"))) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0qv6dvij2g3l7ldhb13lcz7k4grkzkfx27xhhaa3r24cp6mpdyi3")))

(define-public crate-odilia-tts-0.1.4 (c (n "odilia-tts") (v "0.1.4") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "ssip-client-async") (r "^0.10.0") (f (quote ("tokio"))) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("sync" "macros" "rt" "signal" "tracing"))) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0yq16b91r064lgl3r8hswlddyvcb45w341z45w2hxf4r21xdzdi0")))

