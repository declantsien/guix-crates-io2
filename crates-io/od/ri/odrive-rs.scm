(define-module (crates-io od ri odrive-rs) #:use-module (crates-io))

(define-public crate-odrive-rs-0.1.0 (c (n "odrive-rs") (v "0.1.0") (d (list (d (n "serialport") (r "^3.3.0") (d #t) (k 2)))) (h "1j1yiwkik9x61ibi1sgxfw9b9abi8np30xhz3ayc85m636i3fias")))

(define-public crate-odrive-rs-0.1.1 (c (n "odrive-rs") (v "0.1.1") (d (list (d (n "serialport") (r "^3.3.0") (d #t) (k 2)))) (h "08rkpcsfwa1wm6kx1vj2hdhsgs7pg74v7h6iy88v88wlwj72dnfw")))

(define-public crate-odrive-rs-1.0.0 (c (n "odrive-rs") (v "1.0.0") (d (list (d (n "serialport") (r "^3.3.0") (d #t) (k 2)))) (h "11vyajhzq3m81sg940knd4lb0xg7xjkiwh5qmzfcxwqmnbsj9lz7")))

(define-public crate-odrive-rs-1.1.0 (c (n "odrive-rs") (v "1.1.0") (d (list (d (n "serialport") (r "^3.3.0") (d #t) (k 2)))) (h "0z9mr3mzn5bnxa33my25sqb9bwcz83rxg1irbx920vl18sjfwy78")))

(define-public crate-odrive-rs-2.0.0-alpha.1 (c (n "odrive-rs") (v "2.0.0-alpha.1") (d (list (d (n "serialport") (r "^3.3") (d #t) (k 0)))) (h "17jmpvc6nwzy7xv2lh9k2yqrwz6fzaivrrl1v3ws37y84n88zn66")))

