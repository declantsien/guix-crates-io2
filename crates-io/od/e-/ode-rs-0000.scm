(define-module (crates-io od e- ode-rs-0000) #:use-module (crates-io))

(define-public crate-ode-rs-0000-0.9.1 (c (n "ode-rs-0000") (v "0.9.1") (d (list (d (n "asciiz") (r "^0.1") (d #t) (k 0)) (d (n "bindgen") (r "=0.65.1") (d #t) (k 1)) (d (n "cc") (r "=1.0.79") (d #t) (k 1)) (d (n "home") (r "=0.5.5") (d #t) (k 0)) (d (n "oyk") (r "^0.9.1") (d #t) (k 0)))) (h "1q8qb5idfm6kfrydcs7alb3nmz738g8z24cr43ygi8bp1kkkpbqz") (y #t)))

(define-public crate-ode-rs-0000-0.10.1 (c (n "ode-rs-0000") (v "0.10.1") (d (list (d (n "asciiz") (r "^0.1") (d #t) (k 0)) (d (n "bindgen") (r "=0.65.1") (d #t) (k 1)) (d (n "cc") (r "=1.0.79") (d #t) (k 1)) (d (n "home") (r "=0.5.5") (d #t) (k 0)) (d (n "ode-rs") (r "^0.0.1") (d #t) (k 0)))) (h "1q52hkhmhcf2jkd6gklf4ai3ik556qvxhnvfh54fn0n8y7v0g05a") (y #t)))

(define-public crate-ode-rs-0000-1.0.1 (c (n "ode-rs-0000") (v "1.0.1") (d (list (d (n "asciiz") (r "^0.1") (d #t) (k 0)) (d (n "bindgen") (r "=0.65.1") (d #t) (k 1)) (d (n "cc") (r "=1.0.79") (d #t) (k 1)) (d (n "home") (r "=0.5.5") (d #t) (k 0)) (d (n "ode-rs") (r "^1.0.1") (d #t) (k 0)))) (h "1jsjypd0gzz6ixlbpjn9l4w32qy08rmxgfp39bczzyss872qf2qz") (y #t)))

(define-public crate-ode-rs-0000-1.0.3 (c (n "ode-rs-0000") (v "1.0.3") (d (list (d (n "asciiz") (r "^0.1") (d #t) (k 0)) (d (n "bindgen") (r "=0.65.1") (d #t) (k 1)) (d (n "cc") (r "=1.0.79") (d #t) (k 1)) (d (n "home") (r "=0.5.5") (d #t) (k 0)) (d (n "ode-rs") (r "^1.0") (d #t) (k 0)))) (h "1qj4hg43wlzampdphwh1m36kb5f4wari73a2m52c1zd41md292ln") (y #t)))

(define-public crate-ode-rs-0000-1.0.4 (c (n "ode-rs-0000") (v "1.0.4") (d (list (d (n "asciiz") (r "^0.1") (d #t) (k 0)) (d (n "bindgen") (r "=0.65.1") (d #t) (k 1)) (d (n "cc") (r "=1.0.79") (d #t) (k 1)) (d (n "home") (r "=0.5.5") (d #t) (k 0)) (d (n "ode-rs") (r "^1.0") (d #t) (k 0)))) (h "0zqahzd2s8hm2sjkfsvc73frjy4pyv6gv6sfmssi3gclcn64cjz8") (y #t)))

(define-public crate-ode-rs-0000-1.2.1 (c (n "ode-rs-0000") (v "1.2.1") (d (list (d (n "asciiz") (r "^0.1") (d #t) (k 0)) (d (n "bindgen") (r "=0.65.1") (d #t) (k 1)) (d (n "cc") (r "=1.0.79") (d #t) (k 1)) (d (n "home") (r "=0.5.5") (d #t) (k 0)) (d (n "ode-rs") (r "^1.2") (d #t) (k 0)))) (h "0w6nwfbgmx90r2ar1dwdrfbgnsv0bdwzqrhfwak771iik7ggsygn")))

