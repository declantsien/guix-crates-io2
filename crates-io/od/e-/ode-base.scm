(define-module (crates-io od e- ode-base) #:use-module (crates-io))

(define-public crate-ode-base-0.0.1 (c (n "ode-base") (v "0.0.1") (d (list (d (n "bindgen") (r "=0.65.1") (d #t) (k 1)) (d (n "cc") (r "=1.0.79") (d #t) (k 1)) (d (n "home") (r "=0.5.5") (d #t) (k 0)))) (h "07r4azq100ka8vc2kxjag2jgfqagzigy95qjldb93p3ly5ri6l4y") (y #t)))

(define-public crate-ode-base-0.1.1 (c (n "ode-base") (v "0.1.1") (d (list (d (n "bindgen") (r "=0.65.1") (d #t) (k 1)) (d (n "cc") (r "=1.0.79") (d #t) (k 1)) (d (n "home") (r "=0.5.5") (d #t) (k 0)))) (h "0qzxkkidk7j5i75akqqcbxmgia16qw0fyp4bsyzsnas9fzr79fcr") (y #t)))

(define-public crate-ode-base-0.1.2 (c (n "ode-base") (v "0.1.2") (d (list (d (n "bindgen") (r "=0.65.1") (d #t) (k 1)) (d (n "cc") (r "=1.0.79") (d #t) (k 1)) (d (n "home") (r "=0.5.5") (d #t) (k 0)))) (h "0ws4qld6flnw83v7xy0brq5rppj2x19sjy608xhmyfrq780dkqdm") (y #t)))

(define-public crate-ode-base-0.2.1 (c (n "ode-base") (v "0.2.1") (d (list (d (n "bindgen") (r "=0.65.1") (d #t) (k 1)) (d (n "cc") (r "=1.0.79") (d #t) (k 1)) (d (n "home") (r "=0.5.5") (d #t) (k 0)))) (h "1g1k8cfmi3172dysv6c82dn7rgvkfmzbdqlm7arj7rwgshbprb0a")))

