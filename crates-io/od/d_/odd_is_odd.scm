(define-module (crates-io od d_ odd_is_odd) #:use-module (crates-io))

(define-public crate-odd_is_odd-0.1.0 (c (n "odd_is_odd") (v "0.1.0") (h "0aqphxr3kzf7vkz7sz83x0m2rj5zf4lzl5hksadlpgg78a8bq34f")))

(define-public crate-odd_is_odd-0.1.1 (c (n "odd_is_odd") (v "0.1.1") (h "0bbdqg3rr8ma2w4j8062yif630sm35w675pyd9103jh08v2fwz4h")))

(define-public crate-odd_is_odd-0.1.2 (c (n "odd_is_odd") (v "0.1.2") (h "0kdj2v4nylcghbbzmsk3sz36ncbg0mzdac1miyl649dwkhza82fn")))

(define-public crate-odd_is_odd-0.2.0 (c (n "odd_is_odd") (v "0.2.0") (h "1mwyk0h93yqyv2n1dxqdlbcpacc2kiyh58i6p7x1ywkl1w9vym10")))

(define-public crate-odd_is_odd-0.2.1 (c (n "odd_is_odd") (v "0.2.1") (h "15sqrcixnbfqfj5607hx21410a6i6p9ngbbf0zmvd3779hcnbksh")))

