(define-module (crates-io od ds odds) #:use-module (crates-io))

(define-public crate-odds-0.1.0 (c (n "odds") (v "0.1.0") (h "1k20rzn0a8fqhbfr454l51ykaifjd6ivbx341ix3v9anq1m2h6a5")))

(define-public crate-odds-0.1.1 (c (n "odds") (v "0.1.1") (h "1qa5qgswfd323dvx675h3g1rydfwp3vcw0hyyh0rdmznrfddgv5j") (f (quote (("unstable"))))))

(define-public crate-odds-0.1.2 (c (n "odds") (v "0.1.2") (h "1whcqzhi9bbnck58nqrfwnqqhpldhvcql4jc4zn8wxxgj4rcbypz") (f (quote (("unstable"))))))

(define-public crate-odds-0.2.0 (c (n "odds") (v "0.2.0") (d (list (d (n "unreachable") (r "^0.0.2") (d #t) (k 0)))) (h "1flj6vx197dmiwyqdjhjnzwi08lxhyc5pfy6iw06h4352h34l5zq") (f (quote (("unstable"))))))

(define-public crate-odds-0.2.1 (c (n "odds") (v "0.2.1") (d (list (d (n "unreachable") (r "^0.0.2") (d #t) (k 0)))) (h "0a905180c948n9kbs2sjrdaj6in21idbdxja80wm38i7baw41lbk") (f (quote (("unstable"))))))

(define-public crate-odds-0.2.2 (c (n "odds") (v "0.2.2") (d (list (d (n "unreachable") (r "^0.0.2") (d #t) (k 0)))) (h "0zadhkrs2rr02rkczvb3c9fvdg3dqnv3p0sxsgklmpkhrwmi0cc3") (f (quote (("unstable"))))))

(define-public crate-odds-0.2.3 (c (n "odds") (v "0.2.3") (d (list (d (n "unreachable") (r "^0.0.2") (d #t) (k 0)))) (h "1pdff5l12yj74g8cm76nk6dcw4pl6x032hn9pvkz7nwvgkcsabff") (f (quote (("unstable"))))))

(define-public crate-odds-0.2.4 (c (n "odds") (v "0.2.4") (d (list (d (n "itertools") (r "^0.3") (d #t) (k 2)) (d (n "unreachable") (r "^0.0.2") (d #t) (k 0)))) (h "08i1z99y2526p82jsnc03lg62wjqydjd6akf8is32f7i5qzz9ija") (f (quote (("unstable"))))))

(define-public crate-odds-0.2.5 (c (n "odds") (v "0.2.5") (d (list (d (n "itertools") (r "^0.3") (d #t) (k 2)) (d (n "unreachable") (r "^0.0.2") (d #t) (k 0)))) (h "0das06adsylhzmc2k9kr2s42zih799c88mr1m22rx5fliy6vf0ca") (f (quote (("unstable"))))))

(define-public crate-odds-0.2.6 (c (n "odds") (v "0.2.6") (d (list (d (n "itertools") (r "^0.3") (d #t) (k 2)) (d (n "unreachable") (r "^0.0.2") (d #t) (k 0)))) (h "18wca2kw661cqrr268kwcndynzczg55llga1qxzflqbbzanqh251") (f (quote (("unstable"))))))

(define-public crate-odds-0.2.7 (c (n "odds") (v "0.2.7") (d (list (d (n "itertools") (r "^0.3") (d #t) (k 2)) (d (n "unreachable") (r "^0.0.2") (d #t) (k 0)))) (h "1cs3s9h6yycgsikjw9rxhvk966mkb0bac8nsgw4xhs0hb1chpkfl") (f (quote (("unstable"))))))

(define-public crate-odds-0.2.8 (c (n "odds") (v "0.2.8") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 2)) (d (n "unreachable") (r "^0.0.2") (d #t) (k 0)))) (h "19vhclxkjzr68z46x4ndi4xg07v0r9a0hpa4abln98j6hn425l7n") (f (quote (("unstable"))))))

(define-public crate-odds-0.2.9 (c (n "odds") (v "0.2.9") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 2)) (d (n "unreachable") (r "^0.0.2") (d #t) (k 0)))) (h "1k48x0gr4kxc7c5cn769g4d31mc3g04b8sp9xg410qa6iqx1qhl9") (f (quote (("unstable"))))))

(define-public crate-odds-0.2.10 (c (n "odds") (v "0.2.10") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 2)) (d (n "unreachable") (r "^0.0.2") (d #t) (k 0)))) (h "1gh7l1x5hyq8ws9jkbycmcb3z1afr1lwdxa4pcxs6n1gqfd2hjsf") (f (quote (("unstable"))))))

(define-public crate-odds-0.2.11 (c (n "odds") (v "0.2.11") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 2)))) (h "0zsdninapmx1m4nslklf39n2dh6m71bpvnv8v66zbwgk1spfhs0g") (f (quote (("unstable"))))))

(define-public crate-odds-0.2.12 (c (n "odds") (v "0.2.12") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 2)))) (h "1hcfqw8dqg0dk31g8sbl0vlbsjg8bx5kkmhmshi92y0g3gl0d35j") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2.13 (c (n "odds") (v "0.2.13") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 2)))) (h "1rpgznqjw8hy79az061k5x22kw4wimqzw4q8wfvgk4rspmyah5cm") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2.14 (c (n "odds") (v "0.2.14") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 2)))) (h "0j7ad14i6cbxz97yj3jrr06a3m8vmsvnvyc9kwgllqq44rks47xy") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2.15 (c (n "odds") (v "0.2.15") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 2)))) (h "0sgjn6kvh6bv46a4k94w72iwfpw8zp23x9sghyccks1s9r6b9bg2") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2.16 (c (n "odds") (v "0.2.16") (d (list (d (n "itertools") (r "^0.4") (d #t) (k 2)))) (h "0h5zjckgarik2mv2j0q6434zi7g33p1rdzrpmlcy6xhnxkyiqw7k") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2.17 (c (n "odds") (v "0.2.17") (d (list (d (n "itertools") (r "^0.5") (d #t) (k 2)) (d (n "memchr") (r "^0.1.11") (d #t) (k 2)))) (h "03kvsd7iykjclwwk252rxsrxj6cd7lz6d5nig94wih0d5m192c5c") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2.18 (c (n "odds") (v "0.2.18") (d (list (d (n "itertools") (r "^0.5") (d #t) (k 2)) (d (n "memchr") (r "^0.1.11") (d #t) (k 2)))) (h "0qjwdpywi9ak6487drgz4l30pbpjb4pc2k59jjn6k9klwa5rnl4m") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2.19 (c (n "odds") (v "0.2.19") (d (list (d (n "itertools") (r "^0.5.1") (d #t) (k 2)) (d (n "memchr") (r "^0.1.11") (d #t) (k 2)))) (h "1rz3f9z3j491w7h1468y9il7xqin85kbi3vac8xszfi7biqjxc98") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2.20 (c (n "odds") (v "0.2.20") (d (list (d (n "itertools") (r "^0.5.1") (d #t) (k 2)) (d (n "memchr") (r "^0.1.11") (d #t) (k 2)))) (h "1ilj97jyv3s2hf1262p82gpgn88fizk3sl255j3jz8l36bbmxdfw") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2.21 (c (n "odds") (v "0.2.21") (d (list (d (n "itertools") (r "^0.5.1") (d #t) (k 2)) (d (n "memchr") (r "^0.1.11") (d #t) (k 2)))) (h "1vaa6zfkcn8ds23swsjgslb8aks8s0n4kdhaggpfryp1xy97h3dj") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2.22 (c (n "odds") (v "0.2.22") (d (list (d (n "itertools") (r "^0.5.1") (d #t) (k 2)) (d (n "memchr") (r "^0.1.11") (d #t) (k 2)))) (h "1c7g0lcnn8vjq47zffdk9924pz12g3wan140pv2a4rd1r8ibk3ih") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2.23 (c (n "odds") (v "0.2.23") (d (list (d (n "itertools") (r "^0.5.1") (d #t) (k 2)) (d (n "memchr") (r "^0.1.11") (d #t) (k 2)))) (h "181k5nkqdwkfjd18458f88mz0dh1liajb3sdig2wh71z5fk30ip0") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2.24 (c (n "odds") (v "0.2.24") (d (list (d (n "itertools") (r "^0.5.1") (d #t) (k 2)) (d (n "memchr") (r "^0.1.11") (d #t) (k 2)))) (h "03gw4hqsy6kar8wanw37y4ydb3nxv0infyjgs505g59l4z0xgclp") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2.25 (c (n "odds") (v "0.2.25") (d (list (d (n "itertools") (r "^0.5.1") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 2)) (d (n "memchr") (r "^0.1.11") (d #t) (k 2)))) (h "1fpwxqs5f5p62mh6v4yh032akwwx5xz0pagsqfhs7klq09rrppy3") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2.26 (c (n "odds") (v "0.2.26") (d (list (d (n "itertools") (r "^0.5.1") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 2)) (d (n "memchr") (r "^0.1.11") (d #t) (k 2)))) (h "08pvngx0nf7yl9cgk4bahn1a0s8na5g9knbhq7y29kysp58h3bjf") (f (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.3.0 (c (n "odds") (v "0.3.0") (d (list (d (n "itertools") (r "^0.7.2") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 2)) (d (n "memchr") (r "^2.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.1") (k 2)) (d (n "rawpointer") (r "^0.1.0") (d #t) (k 0)) (d (n "rawslice") (r "^0.1.0") (d #t) (k 0)) (d (n "unchecked-index") (r "^0.2.2") (d #t) (k 0)))) (h "1560gcb7bg95g53naha7c9xal72jicl1i4sx9p5h3xzi8c31lr7p") (f (quote (("unstable") ("std-vec" "std") ("std-string" "std") ("std") ("docs" "std-string" "std-vec") ("default"))))))

(define-public crate-odds-0.3.1 (c (n "odds") (v "0.3.1") (d (list (d (n "itertools") (r "^0.7.2") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 2)) (d (n "memchr") (r "^2.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.1") (k 2)) (d (n "rawpointer") (r "^0.1.0") (d #t) (k 0)) (d (n "rawslice") (r "^0.1.0") (d #t) (k 0)) (d (n "unchecked-index") (r "^0.2.2") (d #t) (k 0)))) (h "0rdnxa0na4897yb0svb3figz35g4imxjv61yfm2j21gbh5q8v8d9") (f (quote (("unstable") ("std-vec" "std") ("std-string" "std") ("std") ("docs" "std-string" "std-vec") ("default"))))))

(define-public crate-odds-0.4.0 (c (n "odds") (v "0.4.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "memchr") (r "^2.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (k 2)) (d (n "rawpointer") (r "^0.2") (d #t) (k 0)) (d (n "rawslice") (r "^0.1.0") (d #t) (k 0)) (d (n "unchecked-index") (r "^0.2.2") (d #t) (k 0)))) (h "17jd5fjcrlya7dbnnj0v8s83l3jhlajyljmkcy49pxsvxj9zdsdz") (f (quote (("unstable") ("std-vec" "std") ("std-string" "std") ("std") ("docs" "std-string" "std-vec") ("default"))))))

