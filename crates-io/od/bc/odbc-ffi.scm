(define-module (crates-io od bc odbc-ffi) #:use-module (crates-io))

(define-public crate-odbc-ffi-0.1.0 (c (n "odbc-ffi") (v "0.1.0") (h "1rjn9l8ln7w8gkswfzcdscj56n50p0s1fn6ydiidhd0bhsiqn65h") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-ffi-0.2.0 (c (n "odbc-ffi") (v "0.2.0") (h "1xfzgvhvh2g0s3gg1kd85gqxy8wsnlvxh0l9amfggnby25h62fjs") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-ffi-0.2.1 (c (n "odbc-ffi") (v "0.2.1") (h "0kn7wxpyf8dnvajgnssgk60crxlw9vgxr09na2lxrz94bj69k9bb") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-ffi-0.2.2 (c (n "odbc-ffi") (v "0.2.2") (h "1wij14aic8xir5xnl04zlan4zfbz951r049ba4qcb9wdm5bkyjnm") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-ffi-0.2.3 (c (n "odbc-ffi") (v "0.2.3") (h "1hra01dpaf61wxp7nq2r7r5rmgxq1xrl8sqn44bg43lmlyhy81hp") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-ffi-0.2.4 (c (n "odbc-ffi") (v "0.2.4") (h "1bl73ciajb4py8k6igvgpq7y2rddzaidc62xrl89clygpnx2iqiz") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

