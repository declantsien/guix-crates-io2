(define-module (crates-io od bc odbc-sys) #:use-module (crates-io))

(define-public crate-odbc-sys-0.2.4 (c (n "odbc-sys") (v "0.2.4") (h "0i1qgp9s4jlgiazr836x3w02qn2irwncdg8ibdc7gjnpmxfn00g4") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.2.5 (c (n "odbc-sys") (v "0.2.5") (h "0zmfabbzm93kfliqrfgq5cfnfmr8380i868y85ws564jf7ilmmsc") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.2.6 (c (n "odbc-sys") (v "0.2.6") (h "03rz9zks4khi6f70ygpz1b5kw832dmd5pccwclgs6smrxm9bgz99") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.2.7 (c (n "odbc-sys") (v "0.2.7") (h "0ap269pjxcz26nncsi15jdg17x89kif39kys3vgjgw1nz3bgjsjh") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.2.8 (c (n "odbc-sys") (v "0.2.8") (h "0by6nrx1r4sgnwli00449cfyq3p4xsf0rkskd72aabi3il9m6cvk") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.2.9 (c (n "odbc-sys") (v "0.2.9") (h "1jbsvhdhwapfi4yg61w6xk2950xznbywj06sk3x17lka088x3qaf") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.3.0 (c (n "odbc-sys") (v "0.3.0") (h "0h2dnsb0vr26g2jlqwr6iw8wrki4610yrh2l4dzf1bbdvsrsbgcj") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.4.0 (c (n "odbc-sys") (v "0.4.0") (h "1yyj96y97f606q5n3hw2dnj32zamjz5xqyzlq4f6rw59rxn4i49b") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.5.0 (c (n "odbc-sys") (v "0.5.0") (h "1awbkswcvwv0vv03bm5dw3ixb42nzsixjrab3fnglbk207kx449i") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.5.1 (c (n "odbc-sys") (v "0.5.1") (h "11k2iiqkmqi6qlh2j68f4zrcin0qc7gdblkcr72xjmpk6hwpj7qa") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.5.2 (c (n "odbc-sys") (v "0.5.2") (h "14ix0r1kjwldb8ppgbimy25plpjrnabc63vhj92pdiw788ni7can") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.5.3 (c (n "odbc-sys") (v "0.5.3") (h "0pc8w676kbjsvdk9aadxbd0847clri9ivm8v7kyhpfq7ny00zpha") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80")))) (y #t)))

(define-public crate-odbc-sys-0.5.4 (c (n "odbc-sys") (v "0.5.4") (h "13bigvb26fb48iawbq8kllyfh78a4ayw30pxfn8vg0m3y9wcw3a1") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.6.0 (c (n "odbc-sys") (v "0.6.0") (h "1f9ixg9f0psz74219anvka187j7s9a6jw2ag2ipgwanbbknjf7ld") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.6.1 (c (n "odbc-sys") (v "0.6.1") (h "0van92cmfg7kzjc3x3iiizcb67dvjjhyzj8mr0wwchsklxakdnrr") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.6.2 (c (n "odbc-sys") (v "0.6.2") (h "00pf6crr63lk2rqw2jjf5wkzik4zg5lxv4zlc89hidigy552b7vp") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80")))) (y #t)))

(define-public crate-odbc-sys-0.6.3 (c (n "odbc-sys") (v "0.6.3") (h "0wlbk3amvy1y6wayw3id9mv3ajdk8gayghz8pyvwmab75pxlajw2") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.7.0 (c (n "odbc-sys") (v "0.7.0") (h "0wwrc1l2givnxhq51snnhdz2wdjvzwq4scfj0rvwmky443msa7jl") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.8.0 (c (n "odbc-sys") (v "0.8.0") (h "02ncwdm1h3mrlq39aq9i3rkp6311z4i3xixhwmkz8ajny3qcxwgl") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.8.1 (c (n "odbc-sys") (v "0.8.1") (h "13k1n9f75x68378plkzpw0igsyaa4m3j676inr5xa2srqk7mx023") (f (quote (("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.8.2 (c (n "odbc-sys") (v "0.8.2") (h "0dsg5xj5fn57c5mv9may7w290923rk93rc88ap927i0k1r4wbify") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.9.0 (c (n "odbc-sys") (v "0.9.0") (h "0k56j6c9iqrywf0izzm734z85xxnlci9dzgifc832bq5a21ayh3w") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.10.0 (c (n "odbc-sys") (v "0.10.0") (h "15gjx1alpkqi32yag8ps8yjz7czham0lp0cwsqzg1w9im6glrz9g") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.11.0 (c (n "odbc-sys") (v "0.11.0") (h "006k7d66g7w4mggr1kyxissb7wbjshr85qqchjrqy6r6mypbp2n1") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.12.0 (c (n "odbc-sys") (v "0.12.0") (h "0wadnc9nd62sr2ygsaf0dv18na9v30xvzdljyka8nfqghdbpgxdg") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.12.1 (c (n "odbc-sys") (v "0.12.1") (h "03a4j3qfysiqfjn4q93r9zm97z7fnqm639dbi8dqapi5n73xc1y5") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.12.2 (c (n "odbc-sys") (v "0.12.2") (h "0agz40255k8xfp8r28ah2hr14c427mgf9ig7vgsq2ix0vflh8kvn") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.12.3 (c (n "odbc-sys") (v "0.12.3") (h "0b7a10k704y9r7awk5h4ag3lhsr0r85ndjlck5dg33hmvbnnbpb6") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.12.4 (c (n "odbc-sys") (v "0.12.4") (h "01r450q38zm179d3a7gjrcg5kzsr9vfafwqi3jl6zqbfphi7nzp3") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.12.5 (c (n "odbc-sys") (v "0.12.5") (h "1wbrkady69h4648sbj05gkgwg6vag74ldxh93b6xpr0l0a6qq74p") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.13.0 (c (n "odbc-sys") (v "0.13.0") (h "1hlkf249414sfamjcgvcz72bx356h29x2ys9jy214az99dipyr6g") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.13.1 (c (n "odbc-sys") (v "0.13.1") (h "1r651kh7jfs5dlkk0n3ri29wp58am2iay65mnnwgsnzzb1a45mgr") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.14.0 (c (n "odbc-sys") (v "0.14.0") (h "0q5285z6w677jzack5vs6bpn94bzw8abf5c4669qfk3a7rkfb8fb") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.15.0 (c (n "odbc-sys") (v "0.15.0") (h "0llalpg99idz3j3wyk8phzqg5gzfa7r0pkzf036fya8ipar9a5y0") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.16.0 (c (n "odbc-sys") (v "0.16.0") (h "0kcjxda3a20wj6r3b0n07ydg2l1x3cbrv0qdgr09flycsyrrdxgx") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.17.0 (c (n "odbc-sys") (v "0.17.0") (h "04p95b06qfw6r4fmi7bgdy3ljhxgdwrmfb5pmq1w98i795mqfawk") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.17.1 (c (n "odbc-sys") (v "0.17.1") (h "1a3hn5i3bjlinqgd6j5jx7h25v2mydcw46m070vmp3xhrrlcgj60") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.17.2 (c (n "odbc-sys") (v "0.17.2") (h "0pl7x5wk1a2w6s0ddvl5a0pimdnag92mkm2z0qq311zk2vw7sn45") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("iodbc") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.18.0 (c (n "odbc-sys") (v "0.18.0") (h "065h3p0ls4675w7vp1g1fq1dzvr9q9b6hxpabphqd8bpxr4456lf") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("iodbc") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.18.1 (c (n "odbc-sys") (v "0.18.1") (h "0srfwmavp81vcb5v3nkx4f4bm4xayfqkwflihjag11nn477xwhdx") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("iodbc") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.18.2 (c (n "odbc-sys") (v "0.18.2") (h "0vq2zvvzy3c0rb6dvglvwp1v7bbp4scw2b60bmjpqljzmrarlmvl") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("iodbc") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.18.3 (c (n "odbc-sys") (v "0.18.3") (h "1vgqicpfbbwjk0h4sj48fwchkhy34mfp9v1j750dlg20pyz5vxsb") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("iodbc") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.18.4 (c (n "odbc-sys") (v "0.18.4") (h "0sci1680bq4qwbrhfjm3mrqw2kw5vig9vhcjl74qr2p3q8yqpax1") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("iodbc") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.19.0 (c (n "odbc-sys") (v "0.19.0") (h "087njds84g5l8ksd4424i0xdf78b3kmz2yq73i090532x12g7y3j") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("iodbc") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.20.0 (c (n "odbc-sys") (v "0.20.0") (h "0b4lxfpipfymi4qk31rzz1dxp92q5p5sk1ycwcgc2jp4fngzrv2b") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("iodbc") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.21.0 (c (n "odbc-sys") (v "0.21.0") (h "0mhi6ijj9113r74za4yh9ylv768cz4s519743nh5zyclj7qj4bkw") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("iodbc") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.21.1 (c (n "odbc-sys") (v "0.21.1") (h "035qkksfa3xf83ll41sfpzp82dywsk30hinp3c7k1ydvwfspb568") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("iodbc") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.21.2 (c (n "odbc-sys") (v "0.21.2") (h "1cn7mrv2f3i1cz6gsjpln6f4xz3a8qbnb7dn7kiiz7zfbh96maaz") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("iodbc") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.21.3 (c (n "odbc-sys") (v "0.21.3") (h "0sdb6hfhxf89jvkdbxsz5lysknv7rfspb0lmi0wa3zx0pdmnsv2p") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("iodbc") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.21.4 (c (n "odbc-sys") (v "0.21.4") (h "024gylzq3q7s6b10qhbb8cjis6aj8a83jay553jdsiwgwm65qb2r") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("iodbc") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.22.0 (c (n "odbc-sys") (v "0.22.0") (h "04c64ws9823x41bvf9m5gg9bn9kh728ciikcw2y9xkg1ixwqrdrb") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("iodbc") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.23.0 (c (n "odbc-sys") (v "0.23.0") (h "1cy0xxrg94asdcqi9hdmi8jgfxxpl1d2rj3pp4xsc9bwbxl333hw") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("iodbc") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.23.1 (c (n "odbc-sys") (v "0.23.1") (h "14imqnk67avwbffnm3dm3k7h86iyzy830gaz67ic9sykjfhx5x3v") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("iodbc") ("default" "odbc_version_3_80"))))))

(define-public crate-odbc-sys-0.24.0 (c (n "odbc-sys") (v "0.24.0") (h "1dini6dyrxz7jh8bzaq5r8pgzk9jh4sdf8fr75r1dsyr9im9n8bc") (f (quote (("static") ("odbc_version_4" "odbc_version_3_80") ("odbc_version_3_80" "odbc_version_3_50") ("odbc_version_3_50") ("iodbc") ("default" "odbc_version_3_80"))))))

