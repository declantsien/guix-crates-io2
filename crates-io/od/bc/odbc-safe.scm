(define-module (crates-io od bc odbc-safe) #:use-module (crates-io))

(define-public crate-odbc-safe-0.1.0 (c (n "odbc-safe") (v "0.1.0") (d (list (d (n "odbc-sys") (r "^0.5.0") (d #t) (k 0)))) (h "1j7ch3ql5s033m0kqilvqj4yf8zw0jkp1hgnpz1b767q9rjxvk2q") (f (quote (("travis") ("default"))))))

(define-public crate-odbc-safe-0.2.0 (c (n "odbc-safe") (v "0.2.0") (d (list (d (n "odbc-sys") (r "^0.5.0") (d #t) (k 0)))) (h "0d2ncxm5r898p57l0cinxy0qvz91i2xnixfn8whlzwih6imxskw0") (f (quote (("travis") ("default"))))))

(define-public crate-odbc-safe-0.2.1 (c (n "odbc-safe") (v "0.2.1") (d (list (d (n "odbc-sys") (r "^0.5.0") (d #t) (k 0)))) (h "0p13n4i1ga23lr4q2p5xqhk71nhl5105jjjd50v6cwaiph77swm3") (f (quote (("travis") ("default"))))))

(define-public crate-odbc-safe-0.2.2 (c (n "odbc-safe") (v "0.2.2") (d (list (d (n "odbc-sys") (r "^0.5.0") (d #t) (k 0)))) (h "14ch82pwhc7si3095ycdj3waaskaqkky44gdjyi234m5hxnih3h6") (f (quote (("travis") ("default"))))))

(define-public crate-odbc-safe-0.2.3 (c (n "odbc-safe") (v "0.2.3") (d (list (d (n "odbc-sys") (r "^0.5.0") (d #t) (k 0)))) (h "09j50w2ff7rmj8mjzwppz6gf6bmnxqi0wmv31axdv36xjxzf1b7k") (f (quote (("travis") ("default"))))))

(define-public crate-odbc-safe-0.3.0 (c (n "odbc-safe") (v "0.3.0") (d (list (d (n "odbc-sys") (r "^0.5.1") (d #t) (k 0)))) (h "1l622iacm61nvr6mnazd18cvi8i0wfvclm8kqnwn710agkp9scam") (f (quote (("travis") ("default"))))))

(define-public crate-odbc-safe-0.3.1 (c (n "odbc-safe") (v "0.3.1") (d (list (d (n "odbc-sys") (r "^0.5.1") (d #t) (k 0)))) (h "1x0lx7qnglg04i61k21a0c9y30y295m5x57jqxvfrag3z6m65hsk") (f (quote (("travis") ("default"))))))

(define-public crate-odbc-safe-0.3.2 (c (n "odbc-safe") (v "0.3.2") (d (list (d (n "odbc-sys") (r "^0.5.1") (d #t) (k 0)))) (h "1sk3jyrx52cmqlrrc5c9vh85w9ncjwhjayxifq35iyj5j186mapq") (f (quote (("travis") ("default"))))))

(define-public crate-odbc-safe-0.4.0 (c (n "odbc-safe") (v "0.4.0") (d (list (d (n "odbc-sys") (r "^0.5.1") (d #t) (k 0)))) (h "02xydndzyy1r701b2bgql7v1klrcwmimx7wh8rvczd1vssg7p2xr") (f (quote (("travis") ("default"))))))

(define-public crate-odbc-safe-0.4.1 (c (n "odbc-safe") (v "0.4.1") (d (list (d (n "odbc-sys") (r "^0.6.0") (d #t) (k 0)))) (h "00jmc7hkrlmimv453aa0wln7yr8sph86gnikbc0437cck05l4zpm") (f (quote (("travis") ("default"))))))

(define-public crate-odbc-safe-0.4.2 (c (n "odbc-safe") (v "0.4.2") (d (list (d (n "odbc-sys") (r "^0.8.2") (d #t) (k 0)))) (h "00q2sf4mgapaiv4c3rcli8xprkpz7574cha9z5wj8gh81jp7hp19") (f (quote (("travis") ("default")))) (y #t)))

(define-public crate-odbc-safe-0.5.0 (c (n "odbc-safe") (v "0.5.0") (d (list (d (n "odbc-sys") (r "^0.8.2") (d #t) (k 0)))) (h "0nkyirklksyh6a8hiv626kc9058vjyfi6r0fxykkhyyhw8mf0pgl") (f (quote (("travis") ("default"))))))

(define-public crate-odbc-safe-0.5.1 (c (n "odbc-safe") (v "0.5.1") (d (list (d (n "odbc-sys") (r "^0.8.2") (d #t) (k 0)))) (h "056xsbkpmkqac0xh816hk839ammb64znkc9kkrcrsss7rvpgra55") (f (quote (("travis") ("default")))) (y #t)))

(define-public crate-odbc-safe-0.6.0 (c (n "odbc-safe") (v "0.6.0") (d (list (d (n "odbc-sys") (r "^0.8.2") (d #t) (k 0)))) (h "1mmw662pgg6452x4rhq9qka40xi3815lwswjawfkssr9x1cxmrjx") (f (quote (("travis") ("default"))))))

