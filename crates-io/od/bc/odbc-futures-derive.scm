(define-module (crates-io od bc odbc-futures-derive) #:use-module (crates-io))

(define-public crate-odbc-futures-derive-0.1.0 (c (n "odbc-futures-derive") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "0ns9x5f41lbv3rrdpvqs7mr7ip00vh0m4hk39rkwyi2yhmncn1jq") (y #t)))

(define-public crate-odbc-futures-derive-0.1.1 (c (n "odbc-futures-derive") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "04i50ydcj401a5c1sdjfdz95gl2q1rh51433vlgwi3516bhgml68")))

