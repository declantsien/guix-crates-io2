(define-module (crates-io od bc odbc-iter-schema) #:use-module (crates-io))

(define-public crate-odbc-iter-schema-0.1.0 (c (n "odbc-iter-schema") (v "0.1.0") (d (list (d (n "ensure") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "odbc-iter") (r "^0.2.2") (d #t) (k 0)) (d (n "problem") (r "^5.3.0") (d #t) (k 0)))) (h "0vp43vfd9vcnl971mwrb5k74q0f4mxflghp2i7aapgza9dmqv8pn")))

