(define-module (crates-io od bc odbc-bridge) #:use-module (crates-io))

(define-public crate-odbc-bridge-0.1.0 (c (n "odbc-bridge") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "odbc-api-helper") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "simple-log") (r "^1.6.0") (d #t) (k 0)))) (h "11zhmviimf7xsdcaj5bc89kzc1wf4m552zk4vhfwh36pknxfy2a1")))

