(define-module (crates-io od bc odbc-common) #:use-module (crates-io))

(define-public crate-odbc-common-0.2.3 (c (n "odbc-common") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nu-protocol") (r "=0.69.1") (d #t) (k 0)) (d (n "nu-table") (r "=0.69.1") (d #t) (k 0)) (d (n "odbc-api") (r "^0.50.2") (d #t) (k 0)))) (h "1pj4g6kk52n2b2hxbj1lnjk84s51l8d6mkv65qymzmmhf7rq217g")))

