(define-module (crates-io od bc odbc-tools) #:use-module (crates-io))

(define-public crate-odbc-tools-0.1.0 (c (n "odbc-tools") (v "0.1.0") (d (list (d (n "cotton") (r "^0.0.1") (d #t) (k 0)) (d (n "odbc-iter") (r "^0.1.0") (f (quote ("serde_json" "serde"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1rpn6h5ykchl4axn8bwkizdiimswwc7f02f89p1s8x08qzxy6ydf")))

(define-public crate-odbc-tools-0.2.0 (c (n "odbc-tools") (v "0.2.0") (d (list (d (n "cotton") (r "^0.0.1") (d #t) (k 0)) (d (n "odbc-avro") (r "^0.2.0") (d #t) (k 0)) (d (n "odbc-iter") (r "^0.2.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1ni4pbzv5bdjbx3zqdrzfjikx9z2scjbq4qc7492rlcidr8vd6nc")))

(define-public crate-odbc-tools-1.0.0 (c (n "odbc-tools") (v "1.0.0") (d (list (d (n "cotton") (r "^0.0.3") (d #t) (k 0)) (d (n "odbc-avro") (r "^0.2.0") (d #t) (k 0)) (d (n "odbc-iter") (r "^0.2.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "0p745dki6vgcxxrqdlga3cajmpijyshslip8jz4c20crx59k487s")))

(define-public crate-odbc-tools-1.1.0 (c (n "odbc-tools") (v "1.1.0") (d (list (d (n "cotton") (r "^0.0.3") (d #t) (k 0)) (d (n "odbc-avro") (r "^0.2.1") (f (quote ("decimal"))) (d #t) (k 0)) (d (n "odbc-iter") (r "^0.2.4") (f (quote ("serde" "rust_decimal"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "0frcz2x5s87hlwc454nh1zlvxric2s9r0hij5b9cnyzwdkzrgm1n")))

(define-public crate-odbc-tools-1.1.1 (c (n "odbc-tools") (v "1.1.1") (d (list (d (n "cotton") (r "^0.0.3") (d #t) (k 0)) (d (n "odbc-avro") (r "^0.2.1") (f (quote ("decimal"))) (d #t) (k 0)) (d (n "odbc-iter") (r "^0.2.5") (f (quote ("serde" "rust_decimal"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "0iypqaz26jr09pwcp908v98s45sbbpfrnvqsk4cpwn26gc3hd6x7")))

