(define-module (crates-io kt ut ktutils) #:use-module (crates-io))

(define-public crate-ktutils-0.1.0 (c (n "ktutils") (v "0.1.0") (h "1636lwqy938b6b62ihmjx2y7m2hp6nhw2jifmm56z1b8hv8znppm") (y #t)))

(define-public crate-ktutils-0.1.1 (c (n "ktutils") (v "0.1.1") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 2)))) (h "1xvsxavnvqm57j93sv02p6nwqsxjm92057w6svy8zqr7cn0rjm5s") (y #t)))

