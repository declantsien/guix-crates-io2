(define-module (crates-io kt or ktorrent) #:use-module (crates-io))

(define-public crate-ktorrent-0.1.0 (c (n "ktorrent") (v "0.1.0") (d (list (d (n "select") (r "^0.5.0") (d #t) (k 0)))) (h "1k6azy01iwy9ih9fx5b1vznfd5sp4gjlsr1rcwxzif018lxm7dxm") (y #t)))

(define-public crate-ktorrent-0.1.1 (c (n "ktorrent") (v "0.1.1") (d (list (d (n "select") (r "^0.5.0") (d #t) (k 0)))) (h "0brf38n8lvb1bd7ymyn13jlwfs3gib6ifqgik7hfbr140jcx0cp3")))

(define-public crate-ktorrent-0.1.2 (c (n "ktorrent") (v "0.1.2") (d (list (d (n "select") (r "^0.5.0") (d #t) (k 0)))) (h "1m52gvv7lwp80plg02psmgs64p52kllqg6yy76b2pzw5wjam106z")))

(define-public crate-ktorrent-0.2.0 (c (n "ktorrent") (v "0.2.0") (d (list (d (n "select") (r "^0.5.0") (d #t) (k 0)))) (h "0270b2nrjp6y81ysdxd5paf36lg9l24rviqazf17p486dnixsmly")))

