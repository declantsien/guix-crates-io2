(define-module (crates-io kt sc ktsctl) #:use-module (crates-io))

(define-public crate-ktsctl-0.1.0 (c (n "ktsctl") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "kak-tree-sitter-config") (r "^0.1.0") (d #t) (k 0)))) (h "1af1wzh0s1x4q38fcsk0g9ryyd3mb49kz5hrg52wyly52ywnqp5f") (r "1.65.0")))

(define-public crate-ktsctl-0.2.0 (c (n "ktsctl") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "kak-tree-sitter-config") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1n24ya71ab6lpy5q4am3hzy6ls3xv1r5fkzkj3a9h27xmgl7h696") (r "1.65.0")))

(define-public crate-ktsctl-0.3.0 (c (n "ktsctl") (v "0.3.0") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "kak-tree-sitter-config") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0b47qykgjxpnzhy69axgfbvaxcs1y6nvm0v42v0m7p8bmynkcfd4") (r "1.65.0")))

(define-public crate-ktsctl-0.3.1 (c (n "ktsctl") (v "0.3.1") (d (list (d (n "clap") (r "^4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "kak-tree-sitter-config") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0q81qlv7fybllwdfrbyipbvgwcd8gd76wa6azpxa1x2svd089ik7") (r "1.65.0")))

(define-public crate-ktsctl-0.3.2 (c (n "ktsctl") (v "0.3.2") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "kak-tree-sitter-config") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "0pc7lpwrrs1avq0grw2m5agnj56rq2nmzjzjxiifwz6x507dpdfc") (r "1.65.0")))

(define-public crate-ktsctl-0.3.3 (c (n "ktsctl") (v "0.3.3") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "kak-tree-sitter-config") (r ">=0.3.0, <0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "194n3f01d8365mwkmkr6cpykjwc4acl261f02wxwwi05z4iwxk68") (r "1.65.0")))

(define-public crate-ktsctl-0.4.0 (c (n "ktsctl") (v "0.4.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "kak-tree-sitter-config") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "simple_logger") (r "^4.3.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)))) (h "04cnxrmnl9p41w472zj59xd8jx2a5a4v3mw5y88fhkmwcjh9mri4") (r "1.65.0")))

