(define-module (crates-io kt ra ktrace) #:use-module (crates-io))

(define-public crate-ktrace-0.0.1 (c (n "ktrace") (v "0.0.1") (d (list (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.18") (d #t) (k 0)) (d (n "nix") (r "^0.7.0") (d #t) (k 0)))) (h "1hw3fkhpgc5b02iwb8hbz9cag9jk3bvg7d93r8hlrwy4jgcpi9v0")))

(define-public crate-ktrace-0.1.0 (c (n "ktrace") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "clap") (r "^2.19.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.18") (d #t) (k 0)) (d (n "nix") (r "^0.7.0") (d #t) (k 0)))) (h "0yvabhr8h9ripzhg1vn0d3n9w6sb38jb75kyvbb992y4hpmxhfpk")))

