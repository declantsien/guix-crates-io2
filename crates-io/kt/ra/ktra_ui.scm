(define-module (crates-io kt ra ktra_ui) #:use-module (crates-io))

(define-public crate-ktra_ui-0.1.0 (c (n "ktra_ui") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "curl") (r "^0.4.34") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "url") (r "^2.2.1") (d #t) (k 0)) (d (n "vergen") (r "^3.1.0") (d #t) (k 1)))) (h "055di7grp3vzmgkf4a6jpy5w9kp2q4nk6b43ag3h2c87xpmvpby8")))

