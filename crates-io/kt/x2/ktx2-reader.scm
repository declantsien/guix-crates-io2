(define-module (crates-io kt x2 ktx2-reader) #:use-module (crates-io))

(define-public crate-ktx2-reader-0.1.0 (c (n "ktx2-reader") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-core" "fs"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-core" "fs" "macros"))) (d #t) (k 2)))) (h "18s18g5sbcbfvip6qcd4ivmksbsy0yqm3r9xwnrrdfyx1c8wxzrb")))

(define-public crate-ktx2-reader-0.1.1 (c (n "ktx2-reader") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-core"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-core" "fs" "macros"))) (d #t) (k 2)))) (h "1vznwpiglchkbhv3nh08c1cjfgczyn4c4wb8jz5f3akvw907hkxg")))

(define-public crate-ktx2-reader-0.1.2 (c (n "ktx2-reader") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-core" "fs"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-core" "fs" "macros"))) (d #t) (k 2)))) (h "0wiwcbz29d4afp54ljlcl74xc6fqcwiay0kmyf72r01wnq1rqfnr")))

