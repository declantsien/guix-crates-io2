(define-module (crates-io kt x2 ktx2) #:use-module (crates-io))

(define-public crate-ktx2-0.2.0 (c (n "ktx2") (v "0.2.0") (h "0gbm5ywzjiyd84jjs9mgiv6qyqrhhzfcjvj8acziw587h1m2dd1i") (f (quote (("std") ("default" "std"))))))

(define-public crate-ktx2-0.3.0 (c (n "ktx2") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "0f781irq66llyrqxwz7yh56nnxdrlvm3j0bss84y80pcm445xml7") (f (quote (("std") ("default" "std"))))))

