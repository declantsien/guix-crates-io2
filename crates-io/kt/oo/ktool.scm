(define-module (crates-io kt oo ktool) #:use-module (crates-io))

(define-public crate-ktool-0.1.0 (c (n "ktool") (v "0.1.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "block-modes") (r "^0.3.3") (d #t) (k 0)) (d (n "curl") (r "^0.4.26") (d #t) (k 0)) (d (n "des") (r "^0.3.0") (d #t) (k 0)) (d (n "json") (r "^0.12.1") (d #t) (k 0)) (d (n "shell_command") (r "^0.1.0") (d #t) (k 0)))) (h "10yzl4ng4bwxfnd2j9dhp7sk3cydgxlrq6xh3g0h0gkhw8a5yqvg")))

