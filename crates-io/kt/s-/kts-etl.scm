(define-module (crates-io kt s- kts-etl) #:use-module (crates-io))

(define-public crate-kts-etl-0.1.0 (c (n "kts-etl") (v "0.1.0") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.15.0") (f (quote ("bundled" "functions" "unlock_notify"))) (d #t) (k 0)))) (h "13j5xnm2ck135vgr333hbh8lxipf2y419a9fmyag9cghga555i38")))

(define-public crate-kts-etl-0.1.1 (c (n "kts-etl") (v "0.1.1") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.15.0") (f (quote ("bundled" "functions" "unlock_notify"))) (d #t) (k 0)))) (h "1y1lcp94mbkxlibmfz42gsr9w4637nwh9dxqvlpl5p4mpn7n1a6b")))

(define-public crate-kts-etl-0.1.2 (c (n "kts-etl") (v "0.1.2") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.15.0") (f (quote ("bundled" "functions" "unlock_notify"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "12ypd20mg59wax73y1ai1mfzxffvgzgznbfs2lb0dm8h7fp3ghw9")))

(define-public crate-kts-etl-0.1.3 (c (n "kts-etl") (v "0.1.3") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.15.0") (f (quote ("bundled" "functions" "unlock_notify"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "10ih492vd7pcn646pww6hdjvgwlnsybczv5mbwzdalzwxsf2k7sq")))

(define-public crate-kts-etl-0.1.4 (c (n "kts-etl") (v "0.1.4") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.15.0") (f (quote ("bundled" "functions" "unlock_notify"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "06nd637whm7dlh4x1fg95cfdc7yf6ldgzim0jq6y76s9nzx91k6i")))

(define-public crate-kts-etl-0.1.5 (c (n "kts-etl") (v "0.1.5") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.15.0") (f (quote ("bundled" "functions" "unlock_notify"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "00is9952n1m1bbxywg3hll8dqlcmzhhw5qs3r99wdf59w7pv5l97")))

(define-public crate-kts-etl-0.1.6 (c (n "kts-etl") (v "0.1.6") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.15.0") (f (quote ("bundled" "functions" "unlock_notify"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "0vq0sg8rlydw3y8nvd0n6dkppvlq1vj28cbyi89swgh9g1akbwr2")))

(define-public crate-kts-etl-0.1.8 (c (n "kts-etl") (v "0.1.8") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.15.0") (f (quote ("bundled" "functions" "unlock_notify"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "18p5lqkb19c7cj4v3wzj4ny8lnyaghzppc4fha3src1pzs10z3mg")))

(define-public crate-kts-etl-0.1.9 (c (n "kts-etl") (v "0.1.9") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.15.0") (f (quote ("bundled" "functions" "unlock_notify"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "1377bjg811wr904l1f6ijdidh2wn14ppibp8wsxiy6zmirqvdgiy")))

(define-public crate-kts-etl-0.1.10 (c (n "kts-etl") (v "0.1.10") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.15.0") (f (quote ("bundled" "functions" "unlock_notify"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "1n6cnpi22f8xf835lrzn8f8sym6fagpvdxlqa50g12j4zhvwcpng")))

(define-public crate-kts-etl-0.1.12 (c (n "kts-etl") (v "0.1.12") (d (list (d (n "arrow") (r "^0.14") (d #t) (k 0)) (d (n "datafusion") (r "^0.14") (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "kts-analyze") (r "^0.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.15.0") (f (quote ("bundled" "functions" "unlock_notify"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "148qgyljr9wylf8a3yc1d89k9kyjdqzlzss1h7y79z0ckh8dny90")))

