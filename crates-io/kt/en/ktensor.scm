(define-module (crates-io kt en ktensor) #:use-module (crates-io))

(define-public crate-ktensor-0.0.1 (c (n "ktensor") (v "0.0.1") (h "1inpkmi2w9cnhpf4943qjjjxbmcjrb05m6p3h8rbv7dryvym5x2l")))

(define-public crate-ktensor-0.1.0 (c (n "ktensor") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "05d47ivydr1h7dhv0y4nwcxk7kc5fm6f0hl989x3000s00hvaxvd")))

