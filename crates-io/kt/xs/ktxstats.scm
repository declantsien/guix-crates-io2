(define-module (crates-io kt xs ktxstats) #:use-module (crates-io))

(define-public crate-ktxstats-0.1.0 (c (n "ktxstats") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "0mfwr05imbws59gsf5ld1h7wb9jzg0isqazmb50mhz392s8pz4dc")))

(define-public crate-ktxstats-0.2.0 (c (n "ktxstats") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "1lmq7rz6vmx2h4pd4lynkmsj35k7c0laj9hh9qn4acj4siv6hav2")))

(define-public crate-ktxstats-0.3.0 (c (n "ktxstats") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "0v5j5s6spxm7zfy04vdaiwj5gc76iddj3nc2flyf6adjxmhnrban")))

