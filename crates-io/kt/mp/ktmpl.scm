(define-module (crates-io kt mp ktmpl) #:use-module (crates-io))

(define-public crate-ktmpl-0.1.0 (c (n "ktmpl") (v "0.1.0") (d (list (d (n "clap") (r "^2.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "regex") (r "^0.1.54") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.0") (d #t) (k 0)))) (h "1aw567csl72q6vw2gxxv3hc1r2qzyj29qrnqklh2r7h79y98sqcn")))

(define-public crate-ktmpl-0.2.0 (c (n "ktmpl") (v "0.2.0") (d (list (d (n "clap") (r "^2.5.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.69") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.2") (d #t) (k 0)))) (h "0wh3whl21nr9wcr3i0m1ff1ya2wv45wl1vg9mzz0pdbfk333mrc2")))

(define-public crate-ktmpl-0.2.1 (c (n "ktmpl") (v "0.2.1") (d (list (d (n "clap") (r "^2.5.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.69") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.2") (d #t) (k 0)))) (h "08kji21lzqxb097bipxj90lcacpg23k3varsqgw6khds993kn1zh")))

(define-public crate-ktmpl-0.4.0 (c (n "ktmpl") (v "0.4.0") (d (list (d (n "base64") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^2.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.71") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.2") (d #t) (k 0)))) (h "1qp5csxcwkln6wv91kl6aai8w4javwf7yvn23j7di08ka6pif7jq")))

(define-public crate-ktmpl-0.4.1 (c (n "ktmpl") (v "0.4.1") (d (list (d (n "base64") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^2.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.71") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.2") (d #t) (k 0)))) (h "0md4kdzpjjshzav892zjzcz0ag26gfirqkr7yfrq5jijjy3jvn7k")))

(define-public crate-ktmpl-0.5.0 (c (n "ktmpl") (v "0.5.0") (d (list (d (n "base64") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.11.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.75") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.3") (d #t) (k 0)))) (h "19mrw8ra118fsrsd88iijvl8rjg1yag4vdvllmgm10az9j9piy48")))

(define-public crate-ktmpl-0.6.0 (c (n "ktmpl") (v "0.6.0") (d (list (d (n "base64") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.20.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.5") (d #t) (k 0)))) (h "0wqzx42q7340xmmap3n12fg81ibmxakiarg3cjm1530ylg7w28d4")))

(define-public crate-ktmpl-0.7.0 (c (n "ktmpl") (v "0.7.0") (d (list (d (n "base64") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.20.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3.5") (d #t) (k 0)))) (h "0pmssn9ks7nj9pxrq8zdlh4hjswrs9g4v7idimf0drzxbz5b351j")))

(define-public crate-ktmpl-0.8.0 (c (n "ktmpl") (v "0.8.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.6") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.0") (d #t) (k 0)))) (h "0x9v1qmgiwc15vjrzkdbsilmpdqfymf5cdnmc06amwn74vrb383c")))

(define-public crate-ktmpl-0.9.0 (c (n "ktmpl") (v "0.9.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.2") (d #t) (k 0)))) (h "1w33ghcwi0wgp5las61fy92hd61wag0sf03lxkgs12sgihgh2brs")))

(define-public crate-ktmpl-0.9.1 (c (n "ktmpl") (v "0.9.1") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1ph5ym15w3677yf8r1xqai0s2l2ps1igxghnm741lskydqwvnp8n")))

