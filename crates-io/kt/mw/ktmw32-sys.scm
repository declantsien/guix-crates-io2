(define-module (crates-io kt mw ktmw32-sys) #:use-module (crates-io))

(define-public crate-ktmw32-sys-0.0.1 (c (n "ktmw32-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0pzzvgivmdgwcmydvj42r6ykcr1anisjf20x7shndys59wzs8rlj")))

(define-public crate-ktmw32-sys-0.1.0 (c (n "ktmw32-sys") (v "0.1.0") (d (list (d (n "winapi") (r "*") (d #t) (k 0)) (d (n "winapi-build") (r "*") (d #t) (k 1)))) (h "1vm0776ljri8yw2i7x3iswbal1nwj9dbg46riph9lxzzd6l174vz")))

(define-public crate-ktmw32-sys-0.1.1 (c (n "ktmw32-sys") (v "0.1.1") (d (list (d (n "winapi") (r "^0.2") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "01dv7g8fml3q5qp1y6qsixia1jzvb6nyaxz1l8063dlwj14x6yry")))

