(define-module (crates-io kt i_ kti_cqrs_provider_rs) #:use-module (crates-io))

(define-public crate-kti_cqrs_provider_rs-0.0.1 (c (n "kti_cqrs_provider_rs") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "kti_cqrs_rs") (r "^0.0.1") (d #t) (k 0)))) (h "0b49g7x80ybdwm91ym4130ybqbq6izzh7h4ni7c64y89b2gqy7m4")))

(define-public crate-kti_cqrs_provider_rs-0.1.0 (c (n "kti_cqrs_provider_rs") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "kti_cqrs_rs") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0gsvrkjjfy7hl7gpnydqsvhrsgaw68fxggf16ry8jn06ykffvjc6")))

(define-public crate-kti_cqrs_provider_rs-0.2.0 (c (n "kti_cqrs_provider_rs") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "ioc_container_rs") (r "^0.1.0") (d #t) (k 0)) (d (n "kti_cqrs_rs") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1y6ypxxpdb8jf4iw9mrc9p6jsh2p3zwnw122yvasw2r5cvs52dad")))

