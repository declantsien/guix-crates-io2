(define-module (crates-io kt i_ kti_cqrs_rs) #:use-module (crates-io))

(define-public crate-kti_cqrs_rs-0.0.1 (c (n "kti_cqrs_rs") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ghdirvplg94diays37fmm2i48ghbmj3avgz0yizi7q7zb6aib5m")))

(define-public crate-kti_cqrs_rs-0.1.0 (c (n "kti_cqrs_rs") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1dndrgpq6mqzpsf6wgrbw0wg2635xxqm9c0d3gfc21ad04vklm1z")))

(define-public crate-kti_cqrs_rs-0.2.0 (c (n "kti_cqrs_rs") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15n9zpf30almqr1dfmidj2v32xzpxcjxdpm85nld14j9y8r0pv0g")))

