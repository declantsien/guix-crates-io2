(define-module (crates-io kt ls ktls-recvmsg) #:use-module (crates-io))

(define-public crate-ktls-recvmsg-0.1.0 (c (n "ktls-recvmsg") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.148") (f (quote ("const-extern-fn" "extra_traits"))) (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("socket" "uio" "net"))) (k 0)))) (h "08gk2yf6cy9h477ancn18mn3zlzqv1jw37aqzff1xsdzywsdfba4")))

(define-public crate-ktls-recvmsg-0.1.1 (c (n "ktls-recvmsg") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.148") (f (quote ("const-extern-fn" "extra_traits"))) (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("socket" "uio" "net"))) (k 0)))) (h "0n9yafbld3yciaidcxgxgil8kls4fl88jaarniwif247gn9bn5mj")))

(define-public crate-ktls-recvmsg-0.1.2 (c (n "ktls-recvmsg") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.148") (f (quote ("const-extern-fn" "extra_traits"))) (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("socket" "uio" "net"))) (k 0)))) (h "04r6wadmlkkhc6m74wlc6288kwpcrppd81bif5a8f9ifj7n52zkw")))

(define-public crate-ktls-recvmsg-0.1.3 (c (n "ktls-recvmsg") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.148") (f (quote ("const-extern-fn" "extra_traits"))) (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("socket" "uio" "net"))) (k 0)))) (h "1nakn1kg09mlv9757mijm3aiz9f139vhripq08abyw9g7yk98bnk")))

(define-public crate-ktls-recvmsg-0.2.0 (c (n "ktls-recvmsg") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.153") (f (quote ("const-extern-fn" "extra_traits"))) (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("socket" "uio" "net"))) (k 0)))) (h "0gqxnvbj9zkhvfkn8azkxj3ljkk56c84zz02ajjbpaavqsxlbfdl")))

