(define-module (crates-io kt ls ktls-sys) #:use-module (crates-io))

(define-public crate-ktls-sys-1.0.0 (c (n "ktls-sys") (v "1.0.0") (h "1in3gz4zckv17yqc8c77ridv5r5lhmjhbp4nzjhdx09mvpnlm90k")))

(define-public crate-ktls-sys-1.0.1 (c (n "ktls-sys") (v "1.0.1") (h "113f4nfvk058i6sjqa4aaqjc8bnb4ms8pdrd7y6dzhs1v341ynq9")))

