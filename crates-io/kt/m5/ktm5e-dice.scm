(define-module (crates-io kt m5 ktm5e-dice) #:use-module (crates-io))

(define-public crate-ktm5e-dice-0.1.0 (c (n "ktm5e-dice") (v "0.1.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "microbench") (r "^0.3.1") (f (quote ("nightly"))) (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "165gy968ydvjifcdpcl2rbn8fj4z9r295n142c78smai9y3gh08a") (f (quote (("benchmark"))))))

