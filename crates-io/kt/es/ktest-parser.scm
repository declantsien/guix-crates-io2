(define-module (crates-io kt es ktest-parser) #:use-module (crates-io))

(define-public crate-ktest-parser-0.1.0 (c (n "ktest-parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rbpxvxqfl3rf6fx5apcl06wgihqssrfzjr27lzqbfwa02w7a44v")))

(define-public crate-ktest-parser-0.1.1 (c (n "ktest-parser") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cv41xy1jcvx84qgdzm10y0zznagw4rqsvy07abfck2c2j35mhjm")))

