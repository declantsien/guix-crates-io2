(define-module (crates-io i3 ga i3gamma) #:use-module (crates-io))

(define-public crate-i3gamma-0.1.0 (c (n "i3gamma") (v "0.1.0") (d (list (d (n "i3ipc") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0plc4nqvnmhchyh44s4asanj4nryvp98kq4j48p4dy1hm5xz9yij")))

