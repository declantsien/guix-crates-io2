(define-module (crates-io i3 re i3ref) #:use-module (crates-io))

(define-public crate-i3ref-0.1.0 (c (n "i3ref") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("env" "cargo"))) (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)))) (h "1d2km6n3c5aw2slymqy2631lq0565qfrvpy2xgvpwqj8jdhmw9g2")))

