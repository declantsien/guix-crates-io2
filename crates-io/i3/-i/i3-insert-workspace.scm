(define-module (crates-io i3 #{-i}# i3-insert-workspace) #:use-module (crates-io))

(define-public crate-i3-insert-workspace-1.0.0 (c (n "i3-insert-workspace") (v "1.0.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "i3ipc") (r "^0.10.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0cg7zqsa480q6ijqpnd7dxhzn7j6wdf9wzs9raq8icz9rp2n93pb")))

(define-public crate-i3-insert-workspace-1.0.1 (c (n "i3-insert-workspace") (v "1.0.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "i3ipc") (r "^0.10.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "19n65mgis4vmgad5bmrjinh78zisdafdzdc2giyd547y1w8s6fs2")))

(define-public crate-i3-insert-workspace-1.3.1 (c (n "i3-insert-workspace") (v "1.3.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "i3ipc") (r "^0.10.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "04sqgd46q5ri5j7arz303nn3v8kar9f5v0064xyn5nwkyal26hi7")))

(define-public crate-i3-insert-workspace-1.3.2 (c (n "i3-insert-workspace") (v "1.3.2") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "i3ipc") (r "^0.10.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "179kxbdnkvzpr9v3b3y5gjsznzpp4d3mb2x3kc3fn434slyq9syx")))

