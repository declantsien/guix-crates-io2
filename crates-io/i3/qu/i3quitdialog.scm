(define-module (crates-io i3 qu i3quitdialog) #:use-module (crates-io))

(define-public crate-i3quitdialog-0.1.0 (c (n "i3quitdialog") (v "0.1.0") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "i3_ipc") (r "^0.16") (d #t) (k 0)))) (h "1q2rd9ccgxg31py9q8wsw3ggh4xnzpn9x8nwg807r93d7scji6hs") (y #t)))

(define-public crate-i3quitdialog-0.1.1 (c (n "i3quitdialog") (v "0.1.1") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "i3_ipc") (r "^0.16") (d #t) (k 0)))) (h "14391sz4zh4bgz9ya0az4kbrxkbg43ki7k19rlb6dzypw0xs3lv5") (y #t)))

(define-public crate-i3quitdialog-0.1.2 (c (n "i3quitdialog") (v "0.1.2") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "i3_ipc") (r "^0.16") (d #t) (k 0)))) (h "0chvn6ad69xb42fbdrx8jvlagy7gsb9h17ijp25qry2sdgh4d7h9") (y #t)))

(define-public crate-i3quitdialog-0.1.3 (c (n "i3quitdialog") (v "0.1.3") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "i3_ipc") (r "^0.16") (d #t) (k 0)))) (h "1ncgnjm7hx5na4pflqph996xm3d0dhwad8maqqcawqkdl2rac16g") (y #t)))

(define-public crate-i3quitdialog-0.1.4 (c (n "i3quitdialog") (v "0.1.4") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "i3_ipc") (r "^0.16") (d #t) (k 0)))) (h "099a4r3xmlvvp59fv52j12x0ryn22jj6d1s39nrz3n8qbvip65dc") (y #t)))

(define-public crate-i3quitdialog-0.1.5 (c (n "i3quitdialog") (v "0.1.5") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "i3_ipc") (r "^0.16") (d #t) (k 0)))) (h "05a7i7h9s2lg9ddfda0vi6zxbsbnsyn9g55140lyrq7hlfwj7gnx") (y #t)))

(define-public crate-i3quitdialog-0.1.6 (c (n "i3quitdialog") (v "0.1.6") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "i3_ipc") (r "^0.16") (d #t) (k 0)) (d (n "users") (r "^0.11") (d #t) (k 0)))) (h "1a5a7vqyfd1w28wpyyxxmmnpzhdszmwq8i2kss9shmlr3rgp12zv") (y #t)))

(define-public crate-i3quitdialog-0.1.7 (c (n "i3quitdialog") (v "0.1.7") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "i3_ipc") (r "^0.16") (d #t) (k 0)) (d (n "users") (r "^0.11") (d #t) (k 0)))) (h "1gqz6a45rjh0d7idbszbcysfqvwis16fi8l3rd9yfgwcpi9jqmhb") (y #t)))

(define-public crate-i3quitdialog-0.1.8 (c (n "i3quitdialog") (v "0.1.8") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "i3_ipc") (r "^0.16") (d #t) (k 0)) (d (n "users") (r "^0.11") (d #t) (k 0)))) (h "01zs7mrawp0p9zhmpln1fllx32n2zrzv9zhf9lkqvdrqzd2wr9va") (y #t)))

(define-public crate-i3quitdialog-0.1.9 (c (n "i3quitdialog") (v "0.1.9") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "i3_ipc") (r "^0.16") (d #t) (k 0)) (d (n "users") (r "^0.11") (d #t) (k 0)))) (h "0sd8r5l6hkw2r1b396dak3qh0knx2rh8f3x763awqxn3kggyszfp")))

(define-public crate-i3quitdialog-0.1.10 (c (n "i3quitdialog") (v "0.1.10") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "i3_ipc") (r "^0.16") (d #t) (k 0)) (d (n "users") (r "^0.11") (d #t) (k 0)))) (h "0f73c5dcqxbf1k95vm74lj4skqxsw9mf7sb4dp8zrdja6mhycqja")))

(define-public crate-i3quitdialog-0.1.11 (c (n "i3quitdialog") (v "0.1.11") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "i3_ipc") (r "^0.16") (d #t) (k 0)) (d (n "users") (r "^0.11") (d #t) (k 0)))) (h "0nh3r7g3s9i5lmv5wdjzsvyr1rmk8vyh2yq9qjlmyayw9p4w0hcg")))

(define-public crate-i3quitdialog-0.1.12 (c (n "i3quitdialog") (v "0.1.12") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "i3_ipc") (r "^0.16") (d #t) (k 0)) (d (n "users") (r "^0.11") (d #t) (k 0)))) (h "1v38jdgmv1achlqrpk94195hxmliv7nmjsnq1i8f0lg3q0cxppdw")))

(define-public crate-i3quitdialog-0.1.13 (c (n "i3quitdialog") (v "0.1.13") (d (list (d (n "fltk") (r "^1.3") (d #t) (k 0)) (d (n "i3_ipc") (r "^0.16") (d #t) (k 0)) (d (n "users") (r "^0.11") (d #t) (k 0)))) (h "1fxrgdw1w8c4hghr0dn6gjd4sh3ps129nqnp7swdsf89mdiq5xbf")))

(define-public crate-i3quitdialog-0.1.14 (c (n "i3quitdialog") (v "0.1.14") (d (list (d (n "eframe") (r "^0.21") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "i3_ipc") (r "^0.16") (d #t) (k 0)) (d (n "static_init") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.11") (d #t) (k 0)))) (h "1iclfy588136s46qkkr370xhf0scsg2v0dqca5p796ljhwfv8754")))

(define-public crate-i3quitdialog-0.1.15 (c (n "i3quitdialog") (v "0.1.15") (d (list (d (n "eframe") (r "^0.22") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "i3_ipc") (r "^0.16") (d #t) (k 0)) (d (n "static_init") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.11") (d #t) (k 0)))) (h "06fqs3d1j7r364xfwlc1400l2854yflascxdyq742jd8m6vg81jb")))

(define-public crate-i3quitdialog-1.0.0 (c (n "i3quitdialog") (v "1.0.0") (d (list (d (n "eframe") (r "^0.22") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "i3_ipc") (r "^0.16") (d #t) (k 0)) (d (n "static_init") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.11") (d #t) (k 0)))) (h "1792aa9qdm13qm4qsxp5fgx9i98pm8rlgk0h5ifvlph42a3jzmfw")))

