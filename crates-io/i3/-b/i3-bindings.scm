(define-module (crates-io i3 -b i3-bindings) #:use-module (crates-io))

(define-public crate-i3-bindings-1.0.0 (c (n "i3-bindings") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xy2pa54ga4pmvdr16gnq4dhx8a3686n8wysrynz21fclzbxknhh")))

(define-public crate-i3-bindings-1.0.1 (c (n "i3-bindings") (v "1.0.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)))) (h "0351s3crdzjy73sly9mvjvlk1qgvysraavlfpqgw1lfdrcfw4n2p")))

