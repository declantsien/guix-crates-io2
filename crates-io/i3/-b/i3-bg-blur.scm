(define-module (crates-io i3 -b i3-bg-blur) #:use-module (crates-io))

(define-public crate-i3-bg-blur-0.1.0 (c (n "i3-bg-blur") (v "0.1.0") (d (list (d (n "i3ipc") (r "~0.8") (d #t) (k 0)) (d (n "image") (r "~0.19") (d #t) (k 0)))) (h "177kxv24chw4h7lm3m7lgcaysq626pd9i9rcwbi7f1ndnclx905a")))

(define-public crate-i3-bg-blur-0.2.0 (c (n "i3-bg-blur") (v "0.2.0") (d (list (d (n "clap") (r "^2.32") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "dirs") (r "^1.0.3") (d #t) (k 0)) (d (n "i3ipc") (r "^0.8") (d #t) (k 0)) (d (n "image") (r "^0.19") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)))) (h "11dhbj14xfmxs93p9njn5yzg0wgwdn8ihb1kw41hm7fa4brqprys")))

