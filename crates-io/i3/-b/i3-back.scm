(define-module (crates-io i3 -b i3-back) #:use-module (crates-io))

(define-public crate-i3-back-0.1.0 (c (n "i3-back") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "i3ipc") (r "^0.10.1") (f (quote ("i3-4-14"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)))) (h "0wm16aylhpq9936wy3mbiyr5lgbwzbgw3wd3nw1k7hmmwhh37dcl")))

(define-public crate-i3-back-0.1.1 (c (n "i3-back") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "i3ipc") (r "^0.10.1") (f (quote ("i3-4-14"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)))) (h "193jr8kk9z68s39fqlnfphsly3y3yvr15c26g90hak0jl0rqq5sw")))

(define-public crate-i3-back-0.1.2 (c (n "i3-back") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.5") (f (quote ("termination"))) (d #t) (k 0)) (d (n "i3ipc") (r "^0.10.1") (f (quote ("i3-4-14"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)))) (h "05h2kmwmrgwc316igfda3jskrxs2mvl4f8fllqa0khdkc85qviss")))

(define-public crate-i3-back-0.1.3 (c (n "i3-back") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.5") (f (quote ("termination"))) (d #t) (k 0)) (d (n "i3ipc") (r "^0.10.1") (f (quote ("i3-4-14"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)))) (h "0ww5aiff6pn04zd95p5g8j6vl2yadi0j738isdczc09sgka7hg87")))

(define-public crate-i3-back-0.2.0 (c (n "i3-back") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "dbus-crossroads") (r "^0.5.2") (d #t) (k 0)) (d (n "i3ipc") (r "^0.10.1") (f (quote ("i3-4-14"))) (d #t) (k 0)))) (h "0c185s34rdn7f470k6yka4vdsfn94qazw0bvl6mqv7dw3rnl56ir")))

(define-public crate-i3-back-0.3.0 (c (n "i3-back") (v "0.3.0") (d (list (d (n "i3ipc") (r "^0.10.1") (f (quote ("i3-4-14"))) (d #t) (k 0)))) (h "1ib2x906jzs1lfna2cm8vwvk4sx7vy27871bjlv49v5wrvbnw73i")))

(define-public crate-i3-back-0.3.2 (c (n "i3-back") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.5") (f (quote ("termination"))) (d #t) (k 0)) (d (n "i3ipc") (r "^0.10.1") (f (quote ("i3-4-14"))) (d #t) (k 0)))) (h "1dd0vqzpk63qy6rpywj8sbxmnwczx273inf8i327ywi3xvbklyqb")))

