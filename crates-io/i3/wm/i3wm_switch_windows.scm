(define-module (crates-io i3 wm i3wm_switch_windows) #:use-module (crates-io))

(define-public crate-i3wm_switch_windows-0.1.0 (c (n "i3wm_switch_windows") (v "0.1.0") (d (list (d (n "tokio") (r "^1.37.0") (d #t) (k 0)) (d (n "tokio-i3ipc") (r "^0.16.0") (d #t) (k 0)))) (h "1qwl8ywnarsihjnnr2r1yxfpbrj5gardkv6vdxs6rs7cr9xldnqm")))

(define-public crate-i3wm_switch_windows-0.1.1 (c (n "i3wm_switch_windows") (v "0.1.1") (d (list (d (n "tokio") (r "^1.37.0") (d #t) (k 0)) (d (n "tokio-i3ipc") (r "^0.16.0") (d #t) (k 0)))) (h "0050w28b13s4p9nr7j9wd9ik9kz7abklcmdsdnxziqkvfx717v59")))

(define-public crate-i3wm_switch_windows-0.2.0 (c (n "i3wm_switch_windows") (v "0.2.0") (d (list (d (n "tokio") (r "^1.37.0") (d #t) (k 0)) (d (n "tokio-i3ipc") (r "^0.16.0") (d #t) (k 0)))) (h "0ji4c75zpkgh2zwszwnmzqai6fhg6p64ksmg1hxa3vxysr7q3rq8")))

(define-public crate-i3wm_switch_windows-0.3.0 (c (n "i3wm_switch_windows") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (d #t) (k 0)) (d (n "tokio-i3ipc") (r "^0.16.0") (d #t) (k 0)))) (h "04igjs2zxhijk6x4mjb3fn5fki277yj94lwmkww6i26lskqjznp2")))

