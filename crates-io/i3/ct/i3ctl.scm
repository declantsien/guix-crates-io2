(define-module (crates-io i3 ct i3ctl) #:use-module (crates-io))

(define-public crate-i3ctl-0.1.0 (c (n "i3ctl") (v "0.1.0") (d (list (d (n "clap-verbosity-flag") (r "^0.2.0") (d #t) (k 0)) (d (n "config") (r "^0.9.2") (d #t) (k 0)) (d (n "directories") (r "^1.0.2") (d #t) (k 0)) (d (n "i3ipc") (r "^0.10") (f (quote ("i3-next"))) (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "17i60hndppws6l0xdg4gv9nsm3rg7d389dblb4w0a4ymmn5c4y4f")))

