(define-module (crates-io i3 -w i3-workspace-brightness) #:use-module (crates-io))

(define-public crate-i3-workspace-brightness-1.0.0 (c (n "i3-workspace-brightness") (v "1.0.0") (d (list (d (n "hashbrown") (r "^0.8.2") (d #t) (k 0)) (d (n "i3ipc") (r "^0.10.1") (d #t) (k 0)))) (h "0phb1pkgjndfy1cxz377hnryz4j9v5fx0vc21zx94l8lw3yk8whb")))

(define-public crate-i3-workspace-brightness-1.0.1 (c (n "i3-workspace-brightness") (v "1.0.1") (d (list (d (n "hashbrown") (r "^0.8.2") (d #t) (k 0)) (d (n "i3ipc") (r "^0.10.1") (d #t) (k 0)))) (h "007q2mh5xn4b041s8cq1q5fmw1xz6xqpmip966h7gmjrc6bbra20")))

(define-public crate-i3-workspace-brightness-1.0.2 (c (n "i3-workspace-brightness") (v "1.0.2") (d (list (d (n "hashbrown") (r "^0.8.2") (d #t) (k 0)) (d (n "i3ipc") (r "^0.10.1") (d #t) (k 0)))) (h "1zgk97r51b87994xsy73bpbxzdg20m995fynkw6h0gasgbl18l97")))

(define-public crate-i3-workspace-brightness-1.0.3 (c (n "i3-workspace-brightness") (v "1.0.3") (d (list (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "i3ipc") (r "^0.10.1") (d #t) (k 0)))) (h "18ifmzr3l2ak76a7w47lv1dly7pbsqmk1x419n3507ld583g5rbb")))

