(define-module (crates-io i3 -w i3-workspace-scroll) #:use-module (crates-io))

(define-public crate-i3-workspace-scroll-0.1.0 (c (n "i3-workspace-scroll") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "i3ipc") (r "^0.10.1") (d #t) (k 0)))) (h "1m7zh1yca9z4r7m377ysyr96g35610ib021i0q3v47npd0va6nha")))

