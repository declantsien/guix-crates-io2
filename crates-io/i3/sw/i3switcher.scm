(define-module (crates-io i3 sw i3switcher) #:use-module (crates-io))

(define-public crate-i3switcher-0.1.0 (c (n "i3switcher") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "i3ipc") (r "^0.5.0") (d #t) (k 0)))) (h "045k4nw92pjd71s7jmgp8mjl47sbhiy2cmcba04nh4i7d99ymnn6")))

(define-public crate-i3switcher-0.2.0 (c (n "i3switcher") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "i3ipc") (r "^0.5.0") (d #t) (k 0)))) (h "0dg2qk4jmbhf9z3ncqrrrm8yzh3k04s7vhk4gcvxd6iw1rdcjcbf")))

(define-public crate-i3switcher-0.3.0 (c (n "i3switcher") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "i3ipc") (r "^0.8") (d #t) (k 0)))) (h "0hwzqaj8w3pg8dg4ad2wc2mfa8lrnhccm5bn9gb6ks983r7pr4sb")))

(define-public crate-i3switcher-0.3.1 (c (n "i3switcher") (v "0.3.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "i3ipc") (r "^0.8") (d #t) (k 0)))) (h "09n2p3hjs75gjkdkgc14ajif59341yss0bhrns824qacxw3b4ncr")))

