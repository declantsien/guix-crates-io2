(define-module (crates-io i3 -f i3-focus-last) #:use-module (crates-io))

(define-public crate-i3-focus-last-0.2.2 (c (n "i3-focus-last") (v "0.2.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "i3ipc") (r "^0.9.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.13.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)))) (h "1i4wydvidxy6fnf8fz6yqabq6m2k6r9h9n2abj5j4xfa1x8xclf2")))

(define-public crate-i3-focus-last-0.3.0 (c (n "i3-focus-last") (v "0.3.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "i3ipc") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1npr4sz3323mxf23ksddpj14g7z5djn36j7sd63mir2qnn6hgyff")))

(define-public crate-i3-focus-last-0.3.1 (c (n "i3-focus-last") (v "0.3.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "i3ipc") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gd5d7yj1ixla9f7bgpvixvcm2ypwsc642i7d6cn47vqngvasxri")))

(define-public crate-i3-focus-last-0.4.0 (c (n "i3-focus-last") (v "0.4.0") (d (list (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "i3ipc") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k3jdz2wc3hz8l0izzf3f4jk745v9lcx4d3jz82dm7ahk9rnnnyg")))

(define-public crate-i3-focus-last-0.5.0 (c (n "i3-focus-last") (v "0.5.0") (d (list (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "swayipc") (r "^2.7.2") (d #t) (k 0)))) (h "09b75lcdva6ckx5vcsblxn0sihr1amlrrldkpwq9a9vir4h02zpp")))

(define-public crate-i3-focus-last-0.5.1 (c (n "i3-focus-last") (v "0.5.1") (d (list (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "swayipc") (r "^3.0.0-alpha.1") (d #t) (k 0)))) (h "19pz0ssgn9yy2bnbsj60xjrgb2fc35vvgcwsx74r1c2m8w476sfc")))

(define-public crate-i3-focus-last-0.5.2 (c (n "i3-focus-last") (v "0.5.2") (d (list (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "swayipc") (r "^3.0.0-alpha.1") (d #t) (k 0)))) (h "0vb9rrq8mqw27yhfwp1bj45xal448cm7ai35zzwnq69a1yrsqmbs")))

(define-public crate-i3-focus-last-0.5.3 (c (n "i3-focus-last") (v "0.5.3") (d (list (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "swayipc") (r "^3.0.0") (d #t) (k 0)))) (h "0sw1pmca3d7bgwss9ryzmgsn6xaalajgp5xi1awr90420fxawq9n")))

(define-public crate-i3-focus-last-0.5.4 (c (n "i3-focus-last") (v "0.5.4") (d (list (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "swayipc") (r "^3.0.0") (d #t) (k 0)))) (h "1nxgg9sbx4lmy1j56jgc05bvr5hvaq13yiv1irn9w5ygnhri921p")))

