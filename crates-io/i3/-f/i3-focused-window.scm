(define-module (crates-io i3 -f i3-focused-window) #:use-module (crates-io))

(define-public crate-i3-focused-window-0.1.0 (c (n "i3-focused-window") (v "0.1.0") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "i3ipc") (r "^0.9.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1qq7i2jcqhv1jvmwybzzhxswi72x1x1rnca0g318zx44hsmalkk3")))

