(define-module (crates-io i3 -o i3-open-next-ws) #:use-module (crates-io))

(define-public crate-i3-open-next-ws-0.1.2 (c (n "i3-open-next-ws") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "i3_ipc") (r "^0.12.0") (d #t) (k 0)) (d (n "swayipc") (r "^2.7.2") (d #t) (k 0)))) (h "0qnvlbyc45n0zw2n1q7119zg75syfr5m6wmzfhrfag36jqnr1i3g")))

(define-public crate-i3-open-next-ws-0.1.3 (c (n "i3-open-next-ws") (v "0.1.3") (d (list (d (n "clap") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "i3_ipc") (r "^0.12.0") (d #t) (k 0)) (d (n "swayipc") (r "^2.7.2") (d #t) (k 0)))) (h "1lwm5q5rn12kmf1migzmppidx8v0zxadizr4p4cqjpnk21jrm8lb")))

(define-public crate-i3-open-next-ws-0.1.4 (c (n "i3-open-next-ws") (v "0.1.4") (d (list (d (n "clap") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "i3_ipc") (r "^0.12.0") (d #t) (k 0)) (d (n "swayipc") (r "^2.7.2") (d #t) (k 0)))) (h "0rj5sdcarsjh0zf5rxpya36gsnvdpvw7h0pg6qsilhi5c6vi85zm")))

(define-public crate-i3-open-next-ws-0.1.5 (c (n "i3-open-next-ws") (v "0.1.5") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "i3_ipc") (r "^0.14") (d #t) (k 0)) (d (n "swayipc") (r "^3.0") (d #t) (k 0)))) (h "07y77qvwcx9cavl5n957qbrw3h3b0nb5z7wp7w6jw68x3i4nagc6")))

