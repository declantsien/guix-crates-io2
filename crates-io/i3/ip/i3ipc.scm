(define-module (crates-io i3 ip i3ipc) #:use-module (crates-io))

(define-public crate-i3ipc-0.1.0 (c (n "i3ipc") (v "0.1.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "unix_socket") (r "*") (d #t) (k 0)))) (h "03gbf0bl1ixbqgh2ggj0hzlc5llz0z19l1hjdy3ppvy9d2zr3wv9")))

(define-public crate-i3ipc-0.2.0 (c (n "i3ipc") (v "0.2.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "unix_socket") (r "*") (d #t) (k 0)))) (h "0zp2gpavclf8crhwlyfkbzy7591m5gz5r2kyds791icaphlf4ikd")))

(define-public crate-i3ipc-0.3.0 (c (n "i3ipc") (v "0.3.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "unix_socket") (r "*") (d #t) (k 0)))) (h "1l4xx4ycd5f7mnsc0f1906yg7f84fwi7f9jnkd6y3hgnr4p083m1")))

(define-public crate-i3ipc-0.4.0 (c (n "i3ipc") (v "0.4.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "unix_socket") (r "^0.4.5") (d #t) (k 0)))) (h "1d68m7z4xvs83y0bwnkkcicd3gf7pk8h5w2726n71b0r5lwp1pj1")))

(define-public crate-i3ipc-0.4.1 (c (n "i3ipc") (v "0.4.1") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "unix_socket") (r "^0.4.5") (d #t) (k 0)))) (h "1s2z489rpcxnmynmyj01fhwb6vjly8lz33v56fnqr5ljy0qda8r6")))

(define-public crate-i3ipc-0.4.2 (c (n "i3ipc") (v "0.4.2") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "unix_socket") (r "^0.4.5") (d #t) (k 0)))) (h "0whh5gyl79kwsn5hqdgnf6223ipg3fsmg9xp0q49bn5rvkqpywdv")))

(define-public crate-i3ipc-0.5.0 (c (n "i3ipc") (v "0.5.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "serde") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "unix_socket") (r "^0.4.5") (d #t) (k 0)))) (h "1189bdaq6rjsrvkxbasdq8rw5sq2gf9zpqjc69m3q2898zwzzvsr")))

(define-public crate-i3ipc-0.6.0 (c (n "i3ipc") (v "0.6.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "serde") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "unix_socket") (r "^0.4.5") (d #t) (k 0)))) (h "0bkwaffdcv8a5h79xfjfqh8vmpagcfc5fwhlzkwdmd3jdb52dx2s")))

(define-public crate-i3ipc-0.7.0 (c (n "i3ipc") (v "0.7.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "unix_socket") (r "^0.4.5") (d #t) (k 0)))) (h "149zwykswq1gfnh44pisclhf2n36sgsc410x82v487rssly7k7hm")))

(define-public crate-i3ipc-0.8.0 (c (n "i3ipc") (v "0.8.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "serde") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "unix_socket") (r "^0.4.5") (d #t) (k 0)))) (h "1bqi58y80pa9rxxwid0rzadhx57hsrxqkwhld3hrvg1fgv99n0l3") (f (quote (("i3-next" "i3-4-14") ("i3-4-14" "i3-4-13") ("i3-4-13" "i3-4-12") ("i3-4-12") ("dox" "i3-next"))))))

(define-public crate-i3ipc-0.8.1 (c (n "i3ipc") (v "0.8.1") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "serde") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "unix_socket") (r "^0.4.5") (d #t) (k 0)))) (h "0iynj6b4cfvpdvs64ldxrh3clc4ig5mcbzgg629g92286i3cb6k7") (f (quote (("i3-next" "i3-4-14") ("i3-4-14" "i3-4-13") ("i3-4-13" "i3-4-12") ("i3-4-12") ("dox" "i3-next"))))))

(define-public crate-i3ipc-0.8.2 (c (n "i3ipc") (v "0.8.2") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 0)))) (h "00pk6x78fb0rn4nj296vrirwbb58k08xwa57z4mz6x84lml0qkla") (f (quote (("i3-next" "i3-4-14") ("i3-4-14" "i3-4-13") ("i3-4-13" "i3-4-12") ("i3-4-12") ("dox" "i3-next"))))))

(define-public crate-i3ipc-0.8.3 (c (n "i3ipc") (v "0.8.3") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 0)))) (h "0f736y9in88mvl85mfd3rmvns725pfkvdkf1drd56xvgx55d50vy") (f (quote (("i3-next" "i3-4-14") ("i3-4-14" "i3-4-13") ("i3-4-13" "i3-4-12") ("i3-4-12") ("dox" "i3-next"))))))

(define-public crate-i3ipc-0.8.4 (c (n "i3ipc") (v "0.8.4") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 0)))) (h "1xl2m46kgy053b7793gjc2rxrgn40bpd4gzw94s5wxa6y7hdjqh0") (f (quote (("i3-next" "i3-4-14") ("i3-4-14" "i3-4-13") ("i3-4-13" "i3-4-12") ("i3-4-12") ("dox" "i3-next"))))))

(define-public crate-i3ipc-0.9.0 (c (n "i3ipc") (v "0.9.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "0gkma8krw3x7f9lwcc9z6m81j1z2q419a3c4lfsa5m206rkq0y26") (f (quote (("i3-next" "i3-4-14") ("i3-4-14" "i3-4-13") ("i3-4-13" "i3-4-12") ("i3-4-12") ("dox" "i3-next"))))))

(define-public crate-i3ipc-0.10.0 (c (n "i3ipc") (v "0.10.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "097nsr8m8gv2b1lb4v1ycl3mcspwcw46x983n8ln59mmhj3s7lsn") (f (quote (("i3-next" "i3-4-14") ("i3-4-14" "i3-4-13") ("i3-4-13" "i3-4-12") ("i3-4-12") ("dox" "i3-next"))))))

(define-public crate-i3ipc-0.10.1 (c (n "i3ipc") (v "0.10.1") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.32") (d #t) (k 0)))) (h "0zljiin4s8d5ci18f87v5yinji10299z651irf4awgs71k0dmwv3") (f (quote (("i3-next" "i3-4-14") ("i3-4-14" "i3-4-13") ("i3-4-13" "i3-4-12") ("i3-4-12") ("dox" "i3-next"))))))

