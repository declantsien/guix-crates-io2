(define-module (crates-io i3 -a i3-auto-layout) #:use-module (crates-io))

(define-public crate-i3-auto-layout-0.2.0 (c (n "i3-auto-layout") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.30") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.15.2") (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("rt-threaded" "macros" "sync" "stream"))) (k 0)) (d (n "tokio-i3ipc") (r "^0.8.0") (d #t) (k 0)))) (h "1928xs7s4f11995wgbwj9z97ygga69687lqdih29071p9riax7js")))

