(define-module (crates-io i3 -a i3-autolayout) #:use-module (crates-io))

(define-public crate-i3-autolayout-0.1.0 (c (n "i3-autolayout") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "i3_ipc") (r "^0.15") (d #t) (k 0)))) (h "1fq5x6plapncn97b57pgn7gz2bscx51hzww1ap1dxxyijkwpjp34")))

(define-public crate-i3-autolayout-0.2.0 (c (n "i3-autolayout") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "i3_ipc") (r "^0.15") (d #t) (k 0)))) (h "0v0h5jxjywlz4bb462j9lkbwwsxnziqjgr2j2lxm04yvrxc3wzvk") (y #t)))

(define-public crate-i3-autolayout-0.2.1 (c (n "i3-autolayout") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "i3_ipc") (r "^0.15.0") (d #t) (k 0)))) (h "029m1ck0yvz923in6dawkwn7zjw890qbclx94snvcvls9nwgip83")))

(define-public crate-i3-autolayout-0.2.2 (c (n "i3-autolayout") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "i3_ipc") (r "^0.15.0") (d #t) (k 0)))) (h "0l5fp41v2mng4xza0ga6xdj2pw52770j3x2ry5hgdq6ih50xd8c9")))

(define-public crate-i3-autolayout-0.2.3-beta (c (n "i3-autolayout") (v "0.2.3-beta") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "i3_ipc") (r "^0.15.0") (d #t) (k 0)) (d (n "ptree") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)))) (h "1lsss5i1yzcfnlz05jsg4ni8kd03kbjrdn0v1g45dyf313mvnl6s")))

(define-public crate-i3-autolayout-0.2.3 (c (n "i3-autolayout") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "i3_ipc") (r "^0.15.0") (d #t) (k 0)) (d (n "ptree") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)))) (h "0r0bi0aby85yvzyzn722c00lvpxqdvd7mzwdrn32ki4ps3m28h40")))

