(define-module (crates-io i3 st i3status_ext) #:use-module (crates-io))

(define-public crate-i3status_ext-0.0.1 (c (n "i3status_ext") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "http") (r "^0.2.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "0ppjgjfw2icdxz1i6pa9zpld9c8l370ayamdg8y8qxb2jhqjb2ql") (y #t)))

(define-public crate-i3status_ext-0.0.2 (c (n "i3status_ext") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "http") (r "^0.2.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "1jg8bq6jmxybpjh5xna1m2rr2p8f49lnmbasxz8vnxbwvdqzzv0v") (y #t)))

(define-public crate-i3status_ext-0.0.3 (c (n "i3status_ext") (v "0.0.3") (d (list (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "0wpl394xzr1kf0hpg0d80dmxd2aawlf1akgqfpalp7x6xxrw470v") (y #t)))

(define-public crate-i3status_ext-0.0.4 (c (n "i3status_ext") (v "0.0.4") (d (list (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "0qr3z4z2jd64iaqapsp0awi53szp0s9fiq83g9b49j677vgxi2yj") (y #t)))

(define-public crate-i3status_ext-0.0.5 (c (n "i3status_ext") (v "0.0.5") (d (list (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "1i58l2q9m7aifixgh5g95hcmblc6bnws4w50lkv7r42lji3jgnhc") (y #t)))

(define-public crate-i3status_ext-0.0.6 (c (n "i3status_ext") (v "0.0.6") (d (list (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "18siizs91zw4zb3flz303qdaiblp51w69j1cmzgwn7q0ihqs22nn") (y #t)))

(define-public crate-i3status_ext-0.0.7 (c (n "i3status_ext") (v "0.0.7") (d (list (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "1z9a4psc2saq5r2xhrlsm15y54b59yjws4nzk7pgqdqjdh8z0j2p") (y #t)))

(define-public crate-i3status_ext-0.0.8 (c (n "i3status_ext") (v "0.0.8") (d (list (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "12lvgy93dd8cpnfgnnpqal81cb80wqxrpd63cjyi2pa7vjr64hs5") (y #t)))

(define-public crate-i3status_ext-0.0.9 (c (n "i3status_ext") (v "0.0.9") (d (list (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "00qsvx2hldga4wmgkqdbslkvdqyqyi2hw9k0nr8hrizvhwqsshgb") (y #t)))

(define-public crate-i3status_ext-0.1.0-beta.1 (c (n "i3status_ext") (v "0.1.0-beta.1") (d (list (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "0d43705z3vwmzly07nbgc063v469mj5q33k0j7innx9j2x3cx0fw")))

(define-public crate-i3status_ext-0.1.0-beta.2 (c (n "i3status_ext") (v "0.1.0-beta.2") (d (list (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "043xwklhpvpj93gvgp184hgd1hsjfrbgd9r6w74zxji6ys24irqj")))

(define-public crate-i3status_ext-0.1.0-beta.3 (c (n "i3status_ext") (v "0.1.0-beta.3") (d (list (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "1pxgj15pzm686dl5rarnrbsv6zb3al2cjp54amsqdkc6v23zarvq")))

(define-public crate-i3status_ext-0.1.1 (c (n "i3status_ext") (v "0.1.1") (d (list (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "15yqiqly9jpdzjh0wn61li78gcgpwrz3w9lgijfl5qkzxcqzr2sx")))

(define-public crate-i3status_ext-0.1.2 (c (n "i3status_ext") (v "0.1.2") (d (list (d (n "regex") (r "^1.4.6") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "text_io") (r "^0.1.8") (k 0)))) (h "00p1f6j71jzndwdw77panxaz0wh0b8wyv7iwp6j0rd25iddckbk9")))

