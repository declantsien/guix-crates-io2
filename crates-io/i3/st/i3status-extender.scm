(define-module (crates-io i3 st i3status-extender) #:use-module (crates-io))

(define-public crate-i3status-extender-0.0.1 (c (n "i3status-extender") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "http") (r "^0.2.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "1p0lsyp9divm15km3q4cr5j5gcfwv857ack6a2gzsa32cfy8nm93") (y #t)))

