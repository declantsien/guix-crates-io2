(define-module (crates-io i3 mo i3monkit) #:use-module (crates-io))

(define-public crate-i3monkit-0.1.0 (c (n "i3monkit") (v "0.1.0") (d (list (d (n "alsa") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mhwyfc0759rag92wvvylmpkbkfvddw6pl64xfvgg5y2l93ci7gb")))

(define-public crate-i3monkit-0.1.1 (c (n "i3monkit") (v "0.1.1") (d (list (d (n "alsa") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12g45pzysmin0rg231z59xbl4i7rvnqld90f09x0hgwg3k0h7q6d")))

(define-public crate-i3monkit-0.1.2 (c (n "i3monkit") (v "0.1.2") (d (list (d (n "alsa") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hg1g0c8jv8vqmg76k2m2m3q70rzwzwdzsn3wgbsfr0im0bas724")))

