(define-module (crates-io i3 -s i3-scratchmenu) #:use-module (crates-io))

(define-public crate-i3-scratchmenu-0.1.0 (c (n "i3-scratchmenu") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0i1l76ary60vlfjhcxw2vfxdcpic4aki62nl15153zjzs7vq7mqg")))

(define-public crate-i3-scratchmenu-0.1.1 (c (n "i3-scratchmenu") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "115ndigw6sjc2366kgcq046diy1zy5k17ip6qa5qx3zrd07pgm9q")))

(define-public crate-i3-scratchmenu-0.1.2 (c (n "i3-scratchmenu") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01rm1kila5wsygy6aifadi1p7idyqfn6y08d3kl39ky24aiac4c5")))

