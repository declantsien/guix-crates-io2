(define-module (crates-io i3 sp i3spotify) #:use-module (crates-io))

(define-public crate-i3spotify-0.1.0 (c (n "i3spotify") (v "0.1.0") (d (list (d (n "dbus") (r "^0.9.3") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "16w83609dgm9i2x2mdyh9nsx3anfs5bpvisz6i2lk8scyrqs7jia")))

(define-public crate-i3spotify-0.1.1 (c (n "i3spotify") (v "0.1.1") (d (list (d (n "dbus") (r "^0.9.3") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0x1zvwvnp5pmblggyahp9lln9dq2jzyvznpp6acfbd6bb20dl77q")))

