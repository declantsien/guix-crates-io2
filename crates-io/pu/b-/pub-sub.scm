(define-module (crates-io pu b- pub-sub) #:use-module (crates-io))

(define-public crate-pub-sub-1.0.0 (c (n "pub-sub") (v "1.0.0") (d (list (d (n "env_logger") (r "^0") (d #t) (k 2)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "uuid") (r "^0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1klsgp2z8xx6scjsxqqagkcsf48rwa3f4mp3kysy95qdcpsrdsrq")))

(define-public crate-pub-sub-2.0.0 (c (n "pub-sub") (v "2.0.0") (d (list (d (n "uuid") (r "^0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1h4xsxsxgfz25n5nc9jn88sbiz9qvknczhasrg7amcfiikzddchg")))

