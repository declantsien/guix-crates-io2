(define-module (crates-io pu b- pub-iterator-type) #:use-module (crates-io))

(define-public crate-pub-iterator-type-0.1.0 (c (n "pub-iterator-type") (v "0.1.0") (h "1q4nw0wzsnpbwm7vijycvwbwpgrmpqhdbvpi7v60ckhjd3c53vag")))

(define-public crate-pub-iterator-type-0.1.1 (c (n "pub-iterator-type") (v "0.1.1") (h "063h5ybna5fk6shnihv9r007gni6fg7lh4q369p7qrffrnzgv2l5")))

