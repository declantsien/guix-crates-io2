(define-module (crates-io pu b- pub-sub-client-derive) #:use-module (crates-io))

(define-public crate-pub-sub-client-derive-0.9.0 (c (n "pub-sub-client-derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rvh50dh5kp6saqfi1kxzichv43hd34y4rv76v75hj7anzb0jfjm") (r "1.58")))

(define-public crate-pub-sub-client-derive-0.9.1 (c (n "pub-sub-client-derive") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "09fckzsb4nv4flf852i1yc2xddp3qixbaax06c6nzqb0c72jfmgl") (r "1.58")))

(define-public crate-pub-sub-client-derive-0.9.2 (c (n "pub-sub-client-derive") (v "0.9.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0zbk119lydybcvp76a6y2v4xi7vbka87wyvk27ip9rqbz3dwj1im") (r "1.58")))

(define-public crate-pub-sub-client-derive-0.10.0 (c (n "pub-sub-client-derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "10310sr0p61xncv5nqlhs69cjjb95niav68wi171nmvca69qnk7p") (r "1.58")))

(define-public crate-pub-sub-client-derive-0.11.0 (c (n "pub-sub-client-derive") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nycfi0xzc9807kxgjq4yx551v6l7pbsg8g0q6s597gddf7gi41y")))

