(define-module (crates-io pu b- pub-ip) #:use-module (crates-io))

(define-public crate-pub-ip-0.1.0 (c (n "pub-ip") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "014mq4c1m39j2cjap45bnz821gg3avpl04k8lq87l0zf4yzlgbhp")))

