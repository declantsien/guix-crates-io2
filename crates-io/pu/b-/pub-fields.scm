(define-module (crates-io pu b- pub-fields) #:use-module (crates-io))

(define-public crate-pub-fields-0.1.0 (c (n "pub-fields") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "11nx6sr70sqh7qfa4nni4si4d5jbhzlx4hlb806fnxsdwspz8v1s")))

(define-public crate-pub-fields-0.1.1 (c (n "pub-fields") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "049cq5rb0nilc5v45rv6mxmvkr6nllg53bbil6ykczxnw7xrswvh")))

