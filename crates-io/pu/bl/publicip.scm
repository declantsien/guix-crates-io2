(define-module (crates-io pu bl publicip) #:use-module (crates-io))

(define-public crate-publicip-0.1.0 (c (n "publicip") (v "0.1.0") (d (list (d (n "anyhow") (r ">=1.0.34, <2.0.0") (d #t) (k 0)) (d (n "async-std") (r ">=1.7.0, <2.0.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "surf") (r ">=2.1.0, <3.0.0") (d #t) (k 0)))) (h "14lgpiq3f8wxzp9pl4a0257xschhfl2460b2gg74nxqgn0py4svk") (f (quote (("exe") ("default" "exe"))))))

(define-public crate-publicip-0.2.0 (c (n "publicip") (v "0.2.0") (d (list (d (n "async-std") (r ">=1.7.0, <2.0.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "surf") (r ">=2.1.0, <3.0.0") (d #t) (k 0)))) (h "0vqf5mm32bjacgb6zhsbz63q37rzgpj5vfb1na780y3lscxjz9c1") (f (quote (("exe") ("default" "exe"))))))

(define-public crate-publicip-0.3.0 (c (n "publicip") (v "0.3.0") (d (list (d (n "async-std") (r ">=1.7.0, <2.0.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "structopt") (r ">=0.3.20, <0.4.0") (d #t) (k 0)) (d (n "surf") (r ">=2.1.0, <3.0.0") (d #t) (k 0)))) (h "0j789j15zzk4dia20mqysr4nmian55pv3s9f2ly4kbqy74gxbqqq") (f (quote (("exe") ("default" "exe"))))))

