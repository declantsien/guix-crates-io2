(define-module (crates-io pu bl published_value) #:use-module (crates-io))

(define-public crate-published_value-0.1.0 (c (n "published_value") (v "0.1.0") (h "0600mcc3k9ay9ymmhy80fb6jbyrs6spj2l4zsgv38aryq6dz0v15")))

(define-public crate-published_value-0.1.1 (c (n "published_value") (v "0.1.1") (h "03li44wm8wbvj5jr2nkq3ijxvjbm2sdqdlz4nhyidm1mk8v8in98")))

(define-public crate-published_value-0.1.2 (c (n "published_value") (v "0.1.2") (h "0pxcfzqqxf2sk74lnjqw1rqlwacp2zvp7qaljm32623yp0pdl7lc")))

