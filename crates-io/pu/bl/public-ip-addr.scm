(define-module (crates-io pu bl public-ip-addr) #:use-module (crates-io))

(define-public crate-public-ip-addr-0.1.0 (c (n "public-ip-addr") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "surf") (r "^2.3.2") (f (quote ("h1-client-rustls"))) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("tokio-macros" "macros" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "04rb0yha7cznqpqhm634xwz8c8rbxjhkbqcc6k63vbk0i1mvvmpg")))

