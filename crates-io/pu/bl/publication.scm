(define-module (crates-io pu bl publication) #:use-module (crates-io))

(define-public crate-publication-0.1.0 (c (n "publication") (v "0.1.0") (h "01dh4gcjx4ilhs8vjv9r18nnyg86xj7j6xl3h0qalshd12sdh9kw")))

(define-public crate-publication-0.1.1 (c (n "publication") (v "0.1.1") (h "1anlfmdid4ay7nmdpl9y6i397bvj4yaih80rb7yj1l8y9hpjd0an")))

(define-public crate-publication-0.1.2 (c (n "publication") (v "0.1.2") (h "07dmw8nzbw8ayb18vj21vchq03vzbflv0r5kd7g59qzlk3gjxg6a")))

(define-public crate-publication-0.1.3 (c (n "publication") (v "0.1.3") (h "0l5ymnmf2na3sr9882k84a935q7d28w2i54gs7wjsp3z63v140f9")))

(define-public crate-publication-0.1.4 (c (n "publication") (v "0.1.4") (h "1qilb29m8c7d0hb75k5n215dlb3a5v0vsdcx4dx6a31yn9z0dp0j")))

