(define-module (crates-io pu bl publishtest) #:use-module (crates-io))

(define-public crate-publishtest-0.0.1 (c (n "publishtest") (v "0.0.1") (h "1wafkh0f2361vzyf9hkwvkwydb266kga0cvkipdzv4pldbhm6jkx") (y #t)))

(define-public crate-publishtest-0.0.2 (c (n "publishtest") (v "0.0.2") (h "0862wxqrq9y91z0dkkd1ymqaxrnd255ncb0l473dqga6p2gajvfa") (y #t)))

