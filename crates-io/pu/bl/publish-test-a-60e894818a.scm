(define-module (crates-io pu bl publish-test-a-60e894818a) #:use-module (crates-io))

(define-public crate-publish-test-a-60e894818a-0.1.0 (c (n "publish-test-a-60e894818a") (v "0.1.0") (h "1dk4b621g4hwz1da6a7v4irk8gjah9d78gbkv7gbq1j60d18261i")))

(define-public crate-publish-test-a-60e894818a-0.1.1 (c (n "publish-test-a-60e894818a") (v "0.1.1") (h "0ppiqkc47bl9fq8ifzmf79ryfc92jjb4wyib0gq6qd9bp905jwal")))

(define-public crate-publish-test-a-60e894818a-0.2.0 (c (n "publish-test-a-60e894818a") (v "0.2.0") (h "0n3ya84djdiaycm5l4kcpirx5asz692cxsipamin4hk07dc655lq")))

(define-public crate-publish-test-a-60e894818a-0.3.0 (c (n "publish-test-a-60e894818a") (v "0.3.0") (h "0d2npfvh9dnw4r6yzpi6a34alyy06qa99hs7yfglzxiracfab3yv")))

(define-public crate-publish-test-a-60e894818a-0.3.1 (c (n "publish-test-a-60e894818a") (v "0.3.1") (h "0zgsga77wcliyr7c6cl4jkxs7qjqai9fczvc20k6zycysf0hapqn")))

