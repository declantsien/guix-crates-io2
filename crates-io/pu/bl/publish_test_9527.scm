(define-module (crates-io pu bl publish_test_9527) #:use-module (crates-io))

(define-public crate-publish_test_9527-0.1.0 (c (n "publish_test_9527") (v "0.1.0") (h "0nvi99w48250lcyp3b67r98bpwc2gryny93lhyfdkavhkjwb4yxy")))

(define-public crate-publish_test_9527-0.1.1 (c (n "publish_test_9527") (v "0.1.1") (h "0209z93ym5fg8zq27c6g0wih8fkpxbccxfk1xl711sh3m5lyrfp0")))

(define-public crate-publish_test_9527-0.1.2 (c (n "publish_test_9527") (v "0.1.2") (h "0pc8r811bi7dc79j5nycpvm1aqp2j62zc4w7k8ir39s0yiziwz3y")))

