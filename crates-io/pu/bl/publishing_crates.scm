(define-module (crates-io pu bl publishing_crates) #:use-module (crates-io))

(define-public crate-publishing_crates-0.1.0 (c (n "publishing_crates") (v "0.1.0") (h "1vrs9lkavh3m01p7mqkrcqci3dyjp1x6jfh5hdh5r08ycfm0nrff")))

(define-public crate-publishing_crates-0.2.0 (c (n "publishing_crates") (v "0.2.0") (h "1cm78yy5fqxab681ysdj0yxmf3h7xjbpiizvmknz85g0vwlwal0b")))

(define-public crate-publishing_crates-0.2.1 (c (n "publishing_crates") (v "0.2.1") (h "12jk62shv8k34d6mhsg1wxk1n2x3j4lm04hf0dy6z3hvxqavplbj")))

(define-public crate-publishing_crates-0.2.2 (c (n "publishing_crates") (v "0.2.2") (h "116rpxv3v7hznjkyl8ig21xndramj809jf4mqm71rfnws4d6gn7n")))

