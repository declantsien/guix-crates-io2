(define-module (crates-io pu bl publish-rust) #:use-module (crates-io))

(define-public crate-publish-rust-0.1.0 (c (n "publish-rust") (v "0.1.0") (h "0nb5s413vcmp7lhqq11jjmfblbjijifdqifxsps5z6l13748q80l")))

(define-public crate-publish-rust-0.1.1 (c (n "publish-rust") (v "0.1.1") (h "14jf4bzny6w2808xf3f3sszf8a40jcw1241q8g987gi0vzs0dqfh")))

(define-public crate-publish-rust-0.1.2 (c (n "publish-rust") (v "0.1.2") (h "1z5p394jdxx21zxlf2p0a8r1npzfk9yldv1kab8s8nbw1djf070a")))

(define-public crate-publish-rust-0.1.3 (c (n "publish-rust") (v "0.1.3") (h "1civrf0hlwj29hzabzr53hrz9wc85qj6i7awcarsahy2bqx7ka5a")))

