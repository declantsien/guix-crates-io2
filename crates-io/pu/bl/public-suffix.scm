(define-module (crates-io pu bl public-suffix) #:use-module (crates-io))

(define-public crate-public-suffix-0.1.0 (c (n "public-suffix") (v "0.1.0") (h "0hskjjiwcy8b52vqb14pa7m8fs89mlp04022s12m8q2sxf0v3nbq") (f (quote (("default_provider") ("default" "default_provider"))))))

(define-public crate-public-suffix-0.1.1 (c (n "public-suffix") (v "0.1.1") (h "1zjacy633194x46bn03mv169pkg5kmpnmnf8fq46cf87zyw4d8wc") (f (quote (("default_provider") ("default" "default_provider"))))))

