(define-module (crates-io pu bl publish-ws) #:use-module (crates-io))

(define-public crate-publish-ws-0.1.0 (c (n "publish-ws") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1jmkb0v2g2qqad1nljzi2pyzh12ssjka6cw18w9h1f7l7qp6i0ga") (y #t)))

(define-public crate-publish-ws-0.1.1 (c (n "publish-ws") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1s14rzvr9y7649wqj5vkkpdmgh0r3qjb4ddfvw3kr5jdnrqsnvcf") (y #t)))

(define-public crate-publish-ws-0.1.2 (c (n "publish-ws") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "040hdr9sw5hakncn6d1zqsm598hry698xgpnb47lb8dq93p7wa1d")))

(define-public crate-publish-ws-0.1.5 (c (n "publish-ws") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0g574nbia1z5jn6dm3hlqaginbr7qz0pswmbggaimv6jkmcywdc8")))

(define-public crate-publish-ws-0.1.6 (c (n "publish-ws") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0d8a3q8pqxjzc5xiyq4gnhndj60shs82b28sy00yjb6xfwyk4vaa")))

(define-public crate-publish-ws-0.1.7 (c (n "publish-ws") (v "0.1.7") (d (list (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.126") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0j0zrw13ly0jda1sl8p5dnk6pv2n88q4j381as3vbi9wn5pgnz2a")))

(define-public crate-publish-ws-0.1.8 (c (n "publish-ws") (v "0.1.8") (d (list (d (n "serde") (r "^1.0.135") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.135") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1zr190xfymlx0l3pd8w5fh90r1m78fzmrxipqjd3m4gx17b55f31")))

(define-public crate-publish-ws-0.1.9 (c (n "publish-ws") (v "0.1.9") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1wq7p5dqm033sv3pbyicgl4dcx5px3p0pi5gwn0yz08q1nj4a5a2")))

(define-public crate-publish-ws-0.1.10 (c (n "publish-ws") (v "0.1.10") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "toml") (r "^0.5.10") (d #t) (k 0)))) (h "0b9mz4azp99smz50w4hkqbn78dm2j84dcipzvkxziyxjqyc9whsm")))

(define-public crate-publish-ws-0.1.11 (c (n "publish-ws") (v "0.1.11") (d (list (d (n "serde") (r "^1.0.190") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.190") (d #t) (k 0)) (d (n "toml") (r "^0.8.6") (d #t) (k 0)))) (h "1x9lryf8m5x1sc0hrxkzwgk3dqi08r8jsbpa4cs07qyv63lw1c1v")))

