(define-module (crates-io pu bl publish-test-b-4347e0f632) #:use-module (crates-io))

(define-public crate-publish-test-b-4347e0f632-0.1.0 (c (n "publish-test-b-4347e0f632") (v "0.1.0") (d (list (d (n "publish-test-a-60e894818a") (r "^0.1.0") (d #t) (k 0)))) (h "0cwv2c35qcy8anahixjwmm3gcc68k59mrhqh1mz1il32kij2mzdm")))

(define-public crate-publish-test-b-4347e0f632-0.1.1 (c (n "publish-test-b-4347e0f632") (v "0.1.1") (d (list (d (n "publish-test-a-60e894818a") (r "^0.1.0") (d #t) (k 0)))) (h "1w7n00lxgq2g3b19b5lbkb8z4jx139r3cwh6613ifc1m3gwjdi4d")))

(define-public crate-publish-test-b-4347e0f632-0.2.0 (c (n "publish-test-b-4347e0f632") (v "0.2.0") (d (list (d (n "publish-test-a-60e894818a") (r "^0.3.0") (d #t) (k 0)))) (h "1gw9k9l0mzr760ni1a2hcqwg0jxz3g8cm95nwqzvgm415ldr7vdr")))

(define-public crate-publish-test-b-4347e0f632-0.3.0 (c (n "publish-test-b-4347e0f632") (v "0.3.0") (d (list (d (n "publish-test-a-60e894818a") (r "^0.3.1") (d #t) (k 0)))) (h "1xq2jdm7i9bqarm6v6lb0mdb6xlqvi5ggq8a4cdk44kvxpza06m5")))

(define-public crate-publish-test-b-4347e0f632-0.4.0 (c (n "publish-test-b-4347e0f632") (v "0.4.0") (d (list (d (n "publish-test-a-60e894818a") (r "^0.3.1") (d #t) (k 0)))) (h "1bi934l84j642m905rlxmy3fx0lswmbkk0hjzdm4vm7hy8v0bwnh")))

