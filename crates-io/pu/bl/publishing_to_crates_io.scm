(define-module (crates-io pu bl publishing_to_crates_io) #:use-module (crates-io))

(define-public crate-publishing_to_crates_io-0.1.0 (c (n "publishing_to_crates_io") (v "0.1.0") (h "1fwip00jclnpz49y1r59mhv9znimi5i71y23vsm9qn59pwi6jfqy")))

(define-public crate-publishing_to_crates_io-0.2.0 (c (n "publishing_to_crates_io") (v "0.2.0") (h "1qs7jz4c0jqki483yy5m9l6ib5k7h6aljkcvlj2vpbkpjzdwfis9") (y #t)))

