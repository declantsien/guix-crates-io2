(define-module (crates-io pu bl publish-testing5) #:use-module (crates-io))

(define-public crate-publish-testing5-0.0.0 (c (n "publish-testing5") (v "0.0.0") (h "0b57b70l3bhzwva7s9fs035xjhbbnnk9znjvzwy4ix4pkp7i07mc")))

(define-public crate-publish-testing5-0.1.9 (c (n "publish-testing5") (v "0.1.9") (h "0jgpz2ckw046b4rd7qj6m50lz6680nl619ii00ws0gzb6vq8yfpz")))

(define-public crate-publish-testing5-0.2.0 (c (n "publish-testing5") (v "0.2.0") (h "1bvllw93kh48i86fhvn5k081m29j50qws2vlvr5r48p9pbcm3cq3")))

