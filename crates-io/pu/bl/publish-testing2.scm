(define-module (crates-io pu bl publish-testing2) #:use-module (crates-io))

(define-public crate-publish-testing2-0.1.8 (c (n "publish-testing2") (v "0.1.8") (h "1gw2ww0y3f2nhwv8dax6rd1pg5n6ya6fjzz1s02r0ckygzp1nzxi")))

(define-public crate-publish-testing2-0.1.9 (c (n "publish-testing2") (v "0.1.9") (h "1ynkhcw3csxg24649ssz76fanhv5mgd1c00j1wpl5xilcz9p1vjz")))

