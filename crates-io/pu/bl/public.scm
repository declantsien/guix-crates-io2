(define-module (crates-io pu bl public) #:use-module (crates-io))

(define-public crate-public-0.1.0 (c (n "public") (v "0.1.0") (h "150s4lkzis3qi7iyayv6713hhwnp9mkixrjvbw1mkjiryllrf076")))

(define-public crate-public-0.1.1 (c (n "public") (v "0.1.1") (h "11z7fvkldcwjby4wm52jpb3nyb0x8s7lhgj85dc26v31lax29ji5")))

(define-public crate-public-0.1.2 (c (n "public") (v "0.1.2") (h "1s6klsbnkdccccm8zd4ah9yy8l0nxpsiz3cnjfsxdz0rzf7gjv2a")))

(define-public crate-public-0.1.3 (c (n "public") (v "0.1.3") (h "17d696cj5hi2i0m99wvvyn4nzyzrydwawq1vz3wn26s0dnmnnjrp")))

(define-public crate-public-0.1.4 (c (n "public") (v "0.1.4") (h "1kvj7vpwk064bn22fq96v3s0cqi5vc6dc6rl9vghkh1k6kmyk8nl")))

(define-public crate-public-0.1.5 (c (n "public") (v "0.1.5") (h "0iq0jvh7yqwir54q0lxf6l2x6j99mrc650qc4jx1yvjm9q45inkm")))

(define-public crate-public-0.1.6 (c (n "public") (v "0.1.6") (h "0asrm9aqj1l07jc1rnkmid37vi1cxwa7q5j4ahwk40vgb0qa0rni")))

(define-public crate-public-0.1.7 (c (n "public") (v "0.1.7") (h "17zh3al75fdhxwh2dv1i7my177lzbn0ra6bfhiad6q6vrdmfwag5")))

(define-public crate-public-0.1.8 (c (n "public") (v "0.1.8") (h "1g66x9wb1hg9grzby887gxw1silk9kjg3vqhmxh04cq2qkamszf2")))

(define-public crate-public-0.1.9 (c (n "public") (v "0.1.9") (h "0250p430585pabzph5b32wxw4c1krkz7kizy79dp0nlhqpwml4ha")))

(define-public crate-public-0.2.0 (c (n "public") (v "0.2.0") (h "1ym42qvlnd9j0vfk26qywfg65j52k8x01s94hzb18vbfd0afqdfq")))

(define-public crate-public-0.2.1 (c (n "public") (v "0.2.1") (h "0vkf0z2qpzfc5d1wzn9inzskvqk9qbdmfdhcm5q6q74i9z37xgjn")))

(define-public crate-public-0.2.2 (c (n "public") (v "0.2.2") (h "1vsw7qd6nrq9rgcpzwp5kd8lh98qwswgc1jr14jcbp6sf7klcxb6")))

