(define-module (crates-io pu ff pufferfish) #:use-module (crates-io))

(define-public crate-pufferfish-0.1.0 (c (n "pufferfish") (v "0.1.0") (h "06bg096q87px59b5dykqmgf3a3l23jjvp7gckldkcm01r9mrflml") (y #t)))

(define-public crate-pufferfish-0.1.1 (c (n "pufferfish") (v "0.1.1") (d (list (d (n "etagere") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "fontdue") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "fugu") (r "^0.1.0") (d #t) (k 0)) (d (n "png-decoder") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (f (quote ("bundled"))) (d #t) (k 0)))) (h "00w0975l6ma0j8jkav2midcswmg2fz77ghy76x2zhqc1f45kvz0l") (f (quote (("default" "png-decoder" "text")))) (s 2) (e (quote (("text" "dep:etagere" "dep:fontdue")))) (r "1.60")))

