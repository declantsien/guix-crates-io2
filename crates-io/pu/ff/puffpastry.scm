(define-module (crates-io pu ff puffpastry) #:use-module (crates-io))

(define-public crate-puffpastry-0.1.0 (c (n "puffpastry") (v "0.1.0") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "11hh9fkd3nqz0gi1lxhnsbv6mbpjm4hbas5b5isb8szzvsnyvzqf")))

