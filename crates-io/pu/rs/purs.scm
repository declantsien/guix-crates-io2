(define-module (crates-io pu rs purs) #:use-module (crates-io))

(define-public crate-purs-0.1.0 (c (n "purs") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "clap") (r "^2.26.1") (d #t) (k 0)) (d (n "git2") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "tico") (r "^1.0.0") (d #t) (k 0)) (d (n "try_opt") (r "^0.1.1") (d #t) (k 0)))) (h "0czwkxlyqn0cdl7l8jl65g1rccdzdb6a9jmwj6hz22gvywp14zp0")))

