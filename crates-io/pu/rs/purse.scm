(define-module (crates-io pu rs purse) #:use-module (crates-io))

(define-public crate-purse-0.1.2 (c (n "purse") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "dashmap") (r "^5.5.3") (o #t) (d #t) (k 0)))) (h "0jaxckdqdprs31yk1yiihfqsignvzx2gfpxlrc60n64hy13r8ack") (f (quote (("atomic" "dashmap")))) (r "1.71.0")))

(define-public crate-purse-0.1.3 (c (n "purse") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "dashmap") (r "^5.5.3") (o #t) (d #t) (k 0)))) (h "1139cax4mrm1dn7aa611zdjkjp5zl0jxc9kav83s3wyx4jyxfyd7") (f (quote (("atomic" "dashmap")))) (r "1.71.0")))

(define-public crate-purse-0.1.4 (c (n "purse") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1mkh7kfn9dy092sikmsbgdmf4nwh1vf3fi3qib55r69cq2yqagpp") (r "1.71.0")))

