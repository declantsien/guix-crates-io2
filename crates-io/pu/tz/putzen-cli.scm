(define-module (crates-io pu tz putzen-cli) #:use-module (crates-io))

(define-public crate-putzen-cli-1.0.0 (c (n "putzen-cli") (v "1.0.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9") (d #t) (k 0)) (d (n "jwalk") (r "^0.6") (d #t) (k 0)))) (h "1mvp1slmcfllzqlwc2wiczv1sawkan6dh0spqywf5qkz75d5s6r5")))

(define-public crate-putzen-cli-1.0.1 (c (n "putzen-cli") (v "1.0.1") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9") (d #t) (k 0)) (d (n "jwalk") (r "^0.6") (d #t) (k 0)))) (h "003d7bsifdr4i3wxqa3h0ss8wdkmgjppw4slf4gdww46jyx4rd5d")))

(define-public crate-putzen-cli-1.0.2 (c (n "putzen-cli") (v "1.0.2") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9") (d #t) (k 0)) (d (n "jwalk") (r "^0.6") (d #t) (k 0)) (d (n "remove_dir_all") (r "^0.7.0") (d #t) (t "cfg(target_family = \"windows\")") (k 0)))) (h "073pj42qfcsg38h678j961db76k5v9l98hpr5cccl8bcnvimj5cw")))

(define-public crate-putzen-cli-1.0.3 (c (n "putzen-cli") (v "1.0.3") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9") (d #t) (k 0)) (d (n "jwalk") (r "^0.6") (d #t) (k 0)) (d (n "remove_dir_all") (r "^0.7.0") (d #t) (t "cfg(target_family = \"windows\")") (k 0)))) (h "00izycdilgkj4zvhzq144bbyrhd3c8r1k15f6ckqx01j61ijyrnx")))

(define-public crate-putzen-cli-1.0.4 (c (n "putzen-cli") (v "1.0.4") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10") (d #t) (k 0)) (d (n "jwalk") (r "^0.6") (d #t) (k 0)) (d (n "remove_dir_all") (r "^0.7.0") (d #t) (t "cfg(target_family = \"windows\")") (k 0)))) (h "1i70lxfrd9w0lwjn9mkkxiqh7b506is1n1m05s7a0ix69n077mqy")))

(define-public crate-putzen-cli-1.0.6 (c (n "putzen-cli") (v "1.0.6") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10") (d #t) (k 0)) (d (n "jwalk") (r "^0.8") (d #t) (k 0)) (d (n "remove_dir_all") (r "^0.7.0") (d #t) (t "cfg(target_family = \"windows\")") (k 0)))) (h "0y455h3grb2w1mfx820c2cnqyi25vbk6vw8mf4gq79988gx0v825")))

(define-public crate-putzen-cli-1.0.7 (c (n "putzen-cli") (v "1.0.7") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10") (d #t) (k 0)) (d (n "jwalk") (r "^0.8") (d #t) (k 0)) (d (n "remove_dir_all") (r "^0.7.0") (d #t) (t "cfg(target_family = \"windows\")") (k 0)))) (h "0vjrmmbwgx76fmp0b5wksd568j2l7x0kv49mbsrh6n6hnws8kmzz")))

