(define-module (crates-io pu cc puccinier) #:use-module (crates-io))

(define-public crate-puccinier-0.1.2 (c (n "puccinier") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1xj087ykr74z1jhw3rjx12zn102vgcjpcklzzr37qakh9rgwp1im")))

(define-public crate-puccinier-1.0.2 (c (n "puccinier") (v "1.0.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.6") (f (quote ("std"))) (k 0)) (d (n "xflags") (r "^0.3") (d #t) (k 0)))) (h "0zy95klia5f5y4smhak16crm6v3axzzibzdny4xmdqzsm8vwk52w")))

(define-public crate-puccinier-1.0.3 (c (n "puccinier") (v "1.0.3") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.6") (f (quote ("std"))) (k 0)) (d (n "xflags") (r "^0.3") (d #t) (k 0)))) (h "0b226h71c0ms8rq50rmnzs06bp8gmpcx7vs0qg720jza0aadwqv7")))

(define-public crate-puccinier-1.0.4 (c (n "puccinier") (v "1.0.4") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.10") (f (quote ("std"))) (k 0)) (d (n "xflags") (r "^0.3") (d #t) (k 0)))) (h "16phacl6p106rwnbbdhh7j0mx26my08fx39hyal08zb75xc21npv")))

(define-public crate-puccinier-1.0.5+deprecated (c (n "puccinier") (v "1.0.5+deprecated") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.10") (f (quote ("std"))) (k 0)) (d (n "xflags") (r "^0.3") (d #t) (k 0)))) (h "05jr8a6wvcq0vd6hmfdwamqrvcrz04y3z02vs11ds3wx2jvi231w")))

