(define-module (crates-io pu nf punfetch-derive) #:use-module (crates-io))

(define-public crate-punfetch-derive-0.1.0 (c (n "punfetch-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 2)))) (h "0kzm3y8g1zl0zzmb9l55b1bda0cx0x0y9khzdg58706p9lhaw78c")))

(define-public crate-punfetch-derive-0.1.1 (c (n "punfetch-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 2)))) (h "1pmi63ig1c1sjc2z7x7vgb465jsmfks6nhxs4vy83m7qns4329x8")))

