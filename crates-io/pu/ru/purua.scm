(define-module (crates-io pu ru purua) #:use-module (crates-io))

(define-public crate-purua-0.0.1 (c (n "purua") (v "0.0.1") (d (list (d (n "combine") (r "^4.5.2") (d #t) (k 0)) (d (n "combine-language") (r "^4.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "15gn26vzkxnw1rys4xfkwa5rx6vs7r3nm26awqf0w905s5hkc18h")))

(define-public crate-purua-0.0.2 (c (n "purua") (v "0.0.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "combine") (r "^4.5.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0pjms030n7ig2b9jjq12s9wvkc2j6xi66m0mrmwhkjsvy1vicgbw")))

