(define-module (crates-io pu ru puruspe) #:use-module (crates-io))

(define-public crate-puruspe-0.1.0 (c (n "puruspe") (v "0.1.0") (h "1hn15lzfkjrb5b7mp2qdb7xww73rwhjp260z9px18aiqy08b21kv")))

(define-public crate-puruspe-0.1.1 (c (n "puruspe") (v "0.1.1") (h "1qb56s1lr9b01wlbam2cf18fy9sy1sf8ayr6v78yf886xnpj3jy2")))

(define-public crate-puruspe-0.1.3 (c (n "puruspe") (v "0.1.3") (h "1rfg0xq52g5ccv0a37zpw8rdyrxff15hh37n6cni6427irj7418i")))

(define-public crate-puruspe-0.1.4 (c (n "puruspe") (v "0.1.4") (h "1hx03s2yhsxqpzsl2vn60qb2l2mdbwxhmnj620fzxfd4hp3mmrln")))

(define-public crate-puruspe-0.1.5 (c (n "puruspe") (v "0.1.5") (h "0xdkbg07h5jgbjvskb6m7p5nv3s69df5iwnmsq4x48sh7251aziv")))

(define-public crate-puruspe-0.2.0 (c (n "puruspe") (v "0.2.0") (h "1f43khpag8xsdf65j6y1lwz6h8akkwwr139v6za6zfmjkzhnaxzy")))

(define-public crate-puruspe-0.2.1 (c (n "puruspe") (v "0.2.1") (h "1p8qpj7mkwxwnshsnf72i70fqdzlx8wm7d7snd5hjaryfk1cghyb")))

(define-public crate-puruspe-0.2.2 (c (n "puruspe") (v "0.2.2") (h "01xrc6fjxpj2gva2pq4w2lr5imx1w4lay91w3cdjx6yfvgnygda9")))

(define-public crate-puruspe-0.2.3 (c (n "puruspe") (v "0.2.3") (h "12kw4h111i20576h6vf7krqjs8fw3w5mb7avzh3izcklmskphrrc")))

(define-public crate-puruspe-0.2.4 (c (n "puruspe") (v "0.2.4") (d (list (d (n "peroxide") (r "^0.35") (f (quote ("plot"))) (d #t) (k 2)))) (h "0gzx9lilwxmk2m8fblnnjsinzh3vryfh8pmsbylyl9gn2pbyx886")))

