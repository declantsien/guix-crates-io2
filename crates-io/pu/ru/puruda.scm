(define-module (crates-io pu ru puruda) #:use-module (crates-io))

(define-public crate-puruda-0.1.0 (c (n "puruda") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "peroxide") (r "^0.21") (f (quote ("dataframe"))) (d #t) (k 2)) (d (n "puruda-macro") (r "^0.1") (d #t) (k 0)))) (h "0n1i4qnpqi97sdnbgqsj97vj846cwmdi8bhvg4gp6z635i0fd37g") (f (quote (("default" "csv"))))))

