(define-module (crates-io pu zu puzuzu) #:use-module (crates-io))

(define-public crate-puzuzu-0.1.0 (c (n "puzuzu") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "structure") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.78") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1vpss7dm3h3vqq9whb48q2mnv106ldvhxcxig5whh392d902qw4s")))

(define-public crate-puzuzu-0.1.1 (c (n "puzuzu") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.78") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0990vwz8wdrxac6a9xqavq6hrds7bsh19qwajsh13i1vdiwzwnm5")))

