(define-module (crates-io pu gl pugl-ui) #:use-module (crates-io))

(define-public crate-pugl-ui-0.1.0 (c (n "pugl-ui") (v "0.1.0") (d (list (d (n "cairo-rs") (r "^0.8.1") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.1.1") (d #t) (k 0)) (d (n "pango") (r "^0.8.0") (d #t) (k 0)) (d (n "pangocairo") (r "^0.9.0") (d #t) (k 0)) (d (n "pugl-sys") (r "^0.1.0") (d #t) (k 0)))) (h "16pkjdh8x3wpgf2yii5ccp4sy65lcmcpjiwzvm95i93s6gzki9gv")))

(define-public crate-pugl-ui-0.1.1 (c (n "pugl-ui") (v "0.1.1") (d (list (d (n "cairo-rs") (r "^0.8.1") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.1.1") (d #t) (k 0)) (d (n "pango") (r "^0.8.0") (d #t) (k 0)) (d (n "pangocairo") (r "^0.9.0") (d #t) (k 0)) (d (n "pugl-sys") (r "^0.1.0") (d #t) (k 0)))) (h "00k3bnrxki0mya6m1hjh3cinkjc3wzdv3f5i8cdq2lcd5dw58m8q")))

(define-public crate-pugl-ui-0.1.2 (c (n "pugl-ui") (v "0.1.2") (d (list (d (n "cairo-rs") (r "^0.8.1") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.1.1") (d #t) (k 0)) (d (n "pango") (r "^0.8.0") (d #t) (k 0)) (d (n "pangocairo") (r "^0.9.0") (d #t) (k 0)) (d (n "pugl-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0ndypmi2z4mccbpd6zfvp7n1qvd5z1a8gpc7882b2vzbzqa6zlx1")))

(define-public crate-pugl-ui-0.2.0 (c (n "pugl-ui") (v "0.2.0") (d (list (d (n "cairo-rs") (r "^0.9.0") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "pango") (r "^0.9.0") (d #t) (k 0)) (d (n "pangocairo") (r "^0.10.0") (d #t) (k 0)) (d (n "pugl-sys") (r "^0.3.3") (d #t) (k 0)))) (h "0irpq60ppyd8clkn5wmy9n3c8p4zdi9vwvpabgklb2x9jvgzajrv")))

(define-public crate-pugl-ui-0.3.0 (c (n "pugl-ui") (v "0.3.0") (d (list (d (n "cairo-rs") (r "^0.9.1") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "pango") (r "^0.9.1") (d #t) (k 0)) (d (n "pangocairo") (r "^0.10.0") (d #t) (k 0)) (d (n "pugl-sys") (r "^0.4.0") (d #t) (k 0)))) (h "07p7k5qngl78xr310yjilwpjghc1x4pnm9gv36wnbixrn11hlfpf") (f (quote (("testing" "pugl-sys/testing"))))))

