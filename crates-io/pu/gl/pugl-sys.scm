(define-module (crates-io pu gl pugl-sys) #:use-module (crates-io))

(define-public crate-pugl-sys-0.1.0 (c (n "pugl-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cairo-rs") (r "^0.8.1") (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.9.2") (d #t) (k 0)))) (h "1cj626hddh214gzi1875c9n8c46v5kjh43srpgh3v7ll93hlgsl8")))

(define-public crate-pugl-sys-0.2.0 (c (n "pugl-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cairo-rs") (r "^0.8.1") (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.9.2") (d #t) (k 0)))) (h "0kqz4bni3h9shza71divfzf3q28l2baws56z42pxg2mc310vmcws")))

(define-public crate-pugl-sys-0.3.0 (c (n "pugl-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cairo-rs") (r "^0.8.1") (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.9.2") (d #t) (k 0)))) (h "0c8cgfry0213kabhnp583w5lm0g41pzikypv7llm2jzwx0wnhhp5")))

(define-public crate-pugl-sys-0.3.1 (c (n "pugl-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cairo-rs") (r "^0.8.1") (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.9.2") (d #t) (k 0)))) (h "1qijr302fbcgj2zchqm2p43y3qvmjzk2gia795w8wm8bzx8xdi2f")))

(define-public crate-pugl-sys-0.3.2 (c (n "pugl-sys") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cairo-rs") (r "^0.8.1") (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.9.2") (d #t) (k 0)))) (h "13ay1ygw1512yydy901qqhl6b7sdwlmz8lh4xxid9yh3cmgglamr")))

(define-public crate-pugl-sys-0.3.3 (c (n "pugl-sys") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cairo-rs") (r "^0.9.0") (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.10.0") (d #t) (k 0)))) (h "0n2hwh8pq46y3aln38fakhl1r015q6yylrlixifvl0ss47x6k97x")))

(define-public crate-pugl-sys-0.4.0 (c (n "pugl-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cairo-rs") (r "^0.9.1") (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.0") (d #t) (k 0)))) (h "0akxlpxi1mwsf0dj045fh9015ij9s1m2776dwgg54ar1jvkx12j3") (f (quote (("testing"))))))

(define-public crate-pugl-sys-0.4.1 (c (n "pugl-sys") (v "0.4.1") (d (list (d (n "bindgen") (r ">=0.55.1, <0.56.0") (d #t) (k 1)) (d (n "bitflags") (r ">=1.2.1, <2.0.0") (d #t) (k 0)) (d (n "cairo-rs") (r ">=0.9.1, <0.10.0") (d #t) (k 0)) (d (n "cairo-sys-rs") (r ">=0.10.0, <0.11.0") (d #t) (k 0)) (d (n "serial_test") (r ">=0.5.0, <0.6.0") (d #t) (k 0)))) (h "0jvdyjk7ldbni7r6y49x7aggnr6br250ibqklf13fngmhib3ck88") (f (quote (("testing"))))))

(define-public crate-pugl-sys-0.4.2 (c (n "pugl-sys") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cairo-rs") (r "^0.9.1") (d #t) (k 0)) (d (n "cairo-sys-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.0") (d #t) (k 0)))) (h "17rs6019p47kx555760ca0vd3gb6n9cj0684icv09m5x9sxsg2k5") (f (quote (("testing"))))))

