(define-module (crates-io pu bs pubsub) #:use-module (crates-io))

(define-public crate-pubsub-0.2.0 (c (n "pubsub") (v "0.2.0") (d (list (d (n "threadpool") (r "^0.1.4") (d #t) (k 0)))) (h "0pfqcydjsl403hj0z81f2qj010nb0bv4n1bn27mkc8g32hpkm32b")))

(define-public crate-pubsub-0.2.1 (c (n "pubsub") (v "0.2.1") (d (list (d (n "threadpool") (r "^0.1.4") (d #t) (k 0)))) (h "01sk54xzw8mj2n1b918dl65v5ckc69kayvxzkxnnnzz2lynkxalk")))

(define-public crate-pubsub-0.2.2 (c (n "pubsub") (v "0.2.2") (d (list (d (n "threadpool") (r "^0.1.4") (d #t) (k 0)))) (h "1kbv1p0491bisl6k4khkwmazbiw989dxqq9crni4xcv6l1xqyih9")))

(define-public crate-pubsub-0.2.3 (c (n "pubsub") (v "0.2.3") (d (list (d (n "threadpool") (r "^0.1.4") (d #t) (k 0)))) (h "132y4x8gr3dfn2lmca09drp56mgyi6h632fjl2s6kbnmx8ksnhvr")))

