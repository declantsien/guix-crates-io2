(define-module (crates-io pu bs pubsub_grpc) #:use-module (crates-io))

(define-public crate-pubsub_grpc-0.1.0 (c (n "pubsub_grpc") (v "0.1.0") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)) (d (n "tonic") (r "^0.1") (f (quote ("tls" "tls-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.1") (d #t) (k 1)))) (h "1j2g3zv0qxyzw2py49xkgsjlnni2wmqq6hrp1ip3w7cgcqcbw3ni")))

(define-public crate-pubsub_grpc-0.4.0 (c (n "pubsub_grpc") (v "0.4.0") (d (list (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-types") (r "^0.7") (d #t) (k 0)) (d (n "tonic") (r "^0.4.0") (f (quote ("tls" "tls-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.4.0") (d #t) (k 1)))) (h "1lhnl6bnva3wm3rkzgrn6dqxkrpyg97jwwp25nn10nkys4ccjplp")))

(define-public crate-pubsub_grpc-0.5.2 (c (n "pubsub_grpc") (v "0.5.2") (d (list (d (n "prost") (r "^0.8") (d #t) (k 0)) (d (n "prost-types") (r "^0.8") (d #t) (k 0)) (d (n "tonic") (r "^0.5.2") (f (quote ("tls" "tls-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.5.2") (d #t) (k 1)))) (h "066jhmfqmf5n05cxnpdigycimajlrq8whban56r065h07nlpp78v")))

