(define-module (crates-io pu ro puroro) #:use-module (crates-io))

(define-public crate-puroro-0.1.0 (c (n "puroro") (v "0.1.0") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1snj68dbkxh2npmadchpnd46s9jbyfn7q6safldi5zylxhy0fd6i") (f (quote (("default"))))))

(define-public crate-puroro-0.1.1 (c (n "puroro") (v "0.1.1") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "14y9v0yn02xijvxkd3p89iphn1kxs5m98a1grniiafysx0cxl534") (f (quote (("default"))))))

(define-public crate-puroro-0.1.2 (c (n "puroro") (v "0.1.2") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0xdhd0vi147w0va4lqvcz952y924ipn8cbx7s00cf419c5sw8vh8") (f (quote (("default"))))))

(define-public crate-puroro-0.1.3 (c (n "puroro") (v "0.1.3") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0mgsk5z27xc62p56l9ry5afgphaya5cz3cyb5n3vi1wg50dn9grg") (f (quote (("default"))))))

(define-public crate-puroro-0.2.0 (c (n "puroro") (v "0.2.0") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1nk2ihkmy10np4r921n45napzljasxj6r81khkkipj2sw0npc0hq") (f (quote (("default")))) (y #t)))

(define-public crate-puroro-0.2.1 (c (n "puroro") (v "0.2.1") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0frdsg8v3043i9s150pcc0fk6l2cyp64jcz805xpz9z4x3b38b4w") (f (quote (("default"))))))

(define-public crate-puroro-0.3.0 (c (n "puroro") (v "0.3.0") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "18gvmq508484258fdm3hw8hm8i8i6cp05g1n8v52kxgvb38wcbxm") (f (quote (("default"))))))

(define-public crate-puroro-0.3.1 (c (n "puroro") (v "0.3.1") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0dakjgnmdigah5bsqbk3cl1g8rylj7lqhr4rzimj8n924d7p9r3j") (f (quote (("default"))))))

(define-public crate-puroro-0.4.0 (c (n "puroro") (v "0.4.0") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "bumpalo") (r "^3.8.0") (f (quote ("collections" "boxed"))) (o #t) (d #t) (k 0)) (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0qc6qsbs7kysmq9npw0vnni7xryzg8i486c8r455cql1n44w1f87") (f (quote (("puroro-bumpalo" "bumpalo") ("default" "puroro-bumpalo"))))))

(define-public crate-puroro-0.4.1 (c (n "puroro") (v "0.4.1") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "bumpalo") (r "^3.8.0") (f (quote ("collections" "boxed"))) (o #t) (d #t) (k 0)) (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0x331spgsjhn5jghkmffmmc7fnzdwmnckrikdr9q1d01w2gj8l6p") (f (quote (("puroro-bumpalo" "bumpalo") ("default" "puroro-bumpalo"))))))

(define-public crate-puroro-0.4.2 (c (n "puroro") (v "0.4.2") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "bumpalo") (r "^3.8.0") (f (quote ("collections" "boxed"))) (o #t) (d #t) (k 0)) (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0h0xppbgqhbswqxi0014xf7yyra87iprmx196n03vvrk7rfqvaiz") (f (quote (("puroro-bumpalo" "bumpalo") ("default" "puroro-bumpalo"))))))

(define-public crate-puroro-0.4.3 (c (n "puroro") (v "0.4.3") (d (list (d (n "bitvec") (r "^0.22.3") (d #t) (k 0)) (d (n "bumpalo") (r "^3.8.0") (f (quote ("collections" "boxed"))) (o #t) (d #t) (k 0)) (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "0vga8p5j8p7ymjzdypw0b0d5jg6bpjwzc28id5pfwh75kqhg8wxy") (f (quote (("puroro-bumpalo" "bumpalo") ("default" "puroro-bumpalo"))))))

(define-public crate-puroro-0.5.0 (c (n "puroro") (v "0.5.0") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0x9kisbh7078rgk53icz2di08y57172jvnzl2ff4bryi2qkf4cqc")))

(define-public crate-puroro-0.5.1 (c (n "puroro") (v "0.5.1") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ml51csxg1id1xyvdzcsrmanl65y4binn50lzp2cbf666ivhga18")))

(define-public crate-puroro-0.5.2 (c (n "puroro") (v "0.5.2") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jlhwgflrdwrfg8vbxw0dfjwslb343rvcdfhvkrq7lf3jj43jvpv")))

(define-public crate-puroro-0.6.0 (c (n "puroro") (v "0.6.0") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0haf3j9skxdbg6x1zfdx89m4la533xix5dqq5p4nlfzw8av03133")))

(define-public crate-puroro-0.7.0 (c (n "puroro") (v "0.7.0") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0q1qm37czprpg22pf5bmwn5d9hj0b7c4bjmakdic77qxwp358sr8")))

(define-public crate-puroro-0.8.0 (c (n "puroro") (v "0.8.0") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04h85xqjxv8vyzdkmqycdhxnii92g82vka5n71kn9ysi0j9sn9lb")))

(define-public crate-puroro-0.9.0 (c (n "puroro") (v "0.9.0") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0shr669qfyghd7m4vqak4fc24bh556cbdhwapzhh06fpwz6vbfrp")))

(define-public crate-puroro-0.9.1 (c (n "puroro") (v "0.9.1") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0rzy3vgrd00a2cqsrazaslfq88y8dwivd6z8v75vrc0afbwfnfwa")))

(define-public crate-puroro-0.10.0 (c (n "puroro") (v "0.10.0") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1yjh0c606zb8wp6cy8hym5ihvn07anfc924pxjmpw5q4bsyy7bzc")))

(define-public crate-puroro-0.11.0 (c (n "puroro") (v "0.11.0") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "stable_puroro") (r "^0") (o #t) (d #t) (k 0) (p "puroro")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1x0l1mbayhk0lq086f054k3nm8g2778kzg2j5fn98sj0sbzcmvn2") (f (quote (("dev-for-protobuf-use-stable-puroro" "stable_puroro"))))))

(define-public crate-puroro-0.11.1 (c (n "puroro") (v "0.11.1") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "stable_puroro") (r "^0.10.0") (o #t) (d #t) (k 0) (p "puroro")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "160xvsp6vl3mcc69n4yi32yv48yqqhglvs17jrjq2ms3dn91hl93") (f (quote (("dev-for-protobuf-use-stable-puroro" "stable_puroro"))))))

(define-public crate-puroro-0.12.0 (c (n "puroro") (v "0.12.0") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "stable_puroro") (r "^0.11.1") (o #t) (d #t) (k 0) (p "puroro")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0clafxskhvdnsns07y77fxwbbj8aakwxfcv48gi39pl9b74a84dz") (f (quote (("dev-for-protobuf-use-stable-puroro" "stable_puroro") ("default") ("allocator_api"))))))

(define-public crate-puroro-0.13.0 (c (n "puroro") (v "0.13.0") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1lcq7ygsnja8jwwjc36j9vivzribsglgzjd3azhdygw1svw6dpsq") (f (quote (("default") ("allocator_api"))))))

(define-public crate-puroro-0.14.0 (c (n "puroro") (v "0.14.0") (d (list (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0v3y1gxkzx6hbi9mwmrj8s0sajaq0jka8q4sxrm0iynqgk0i0i7g") (f (quote (("default") ("allocator_api"))))))

