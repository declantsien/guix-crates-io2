(define-module (crates-io pu ro puroro-protobuf-compiled) #:use-module (crates-io))

(define-public crate-puroro-protobuf-compiled-0.1.0 (c (n "puroro-protobuf-compiled") (v "0.1.0") (d (list (d (n "puroro") (r "^0.1.0") (d #t) (k 0)))) (h "1zg30i0ayd5l4rgca784k34kqrq8jy9r6hjrwhg6iwv6rg0mx2qb")))

(define-public crate-puroro-protobuf-compiled-0.1.1 (c (n "puroro-protobuf-compiled") (v "0.1.1") (d (list (d (n "puroro") (r "^0.1.0") (d #t) (k 0)))) (h "0wp85s25qfy19w3cqy0qkbbkb168pdh460v4fyhcxkrpqr9yyarc")))

(define-public crate-puroro-protobuf-compiled-0.2.1 (c (n "puroro-protobuf-compiled") (v "0.2.1") (d (list (d (n "puroro") (r "^0.2.1") (d #t) (k 0)))) (h "1x06fmd64klnirxm745q9ncxhhcxpmi64wpg8wdrs8wq557vmp9l")))

(define-public crate-puroro-protobuf-compiled-0.3.0 (c (n "puroro-protobuf-compiled") (v "0.3.0") (d (list (d (n "puroro") (r "^0.3.0") (d #t) (k 0)))) (h "0jhsk9gsilv2k9b8g35h4apgxnhslh6j4icj233nqv2xyah7jn7y")))

(define-public crate-puroro-protobuf-compiled-0.3.1 (c (n "puroro-protobuf-compiled") (v "0.3.1") (d (list (d (n "puroro") (r "^0.3.1") (d #t) (k 0)))) (h "1w2wprxp190s9rqqhdzdqh8aspxpansd0vdbm4wrmwi101lc3n0n")))

(define-public crate-puroro-protobuf-compiled-0.4.0 (c (n "puroro-protobuf-compiled") (v "0.4.0") (d (list (d (n "puroro") (r "^0.4.0") (d #t) (k 0)))) (h "1y224amzqmmx5vfzla2kzpp5jpzjwxl7mm0dr5h5cp635xk0a39s")))

(define-public crate-puroro-protobuf-compiled-0.4.1 (c (n "puroro-protobuf-compiled") (v "0.4.1") (d (list (d (n "puroro") (r "^0.4.1") (d #t) (k 0)))) (h "1d7faxpz956dclri4zc6n6hansi53jgxdjijrzphnp85xf3a7w6y")))

(define-public crate-puroro-protobuf-compiled-0.4.2 (c (n "puroro-protobuf-compiled") (v "0.4.2") (d (list (d (n "puroro") (r "^0.4.2") (d #t) (k 0)))) (h "1qj1n6g0q566g3qhlxrq7gqr0maj2vq3iw9ryysz8986zbpxzy3m")))

(define-public crate-puroro-protobuf-compiled-0.4.3 (c (n "puroro-protobuf-compiled") (v "0.4.3") (d (list (d (n "puroro") (r "^0.4.3") (d #t) (k 0)))) (h "0d4izb4a23151xms1n9xpj16k91p1v7h07ymr7l12a1s0ysmk7ig")))

(define-public crate-puroro-protobuf-compiled-0.5.0 (c (n "puroro-protobuf-compiled") (v "0.5.0") (d (list (d (n "puroro") (r "^0.5") (d #t) (k 0)))) (h "0zlhjqdcdk7cv1z3hamiq6670r8ip9ccay74am0gdvk8ykbj8h54")))

(define-public crate-puroro-protobuf-compiled-0.5.1 (c (n "puroro-protobuf-compiled") (v "0.5.1") (d (list (d (n "puroro") (r "^0.5.1") (d #t) (k 0)))) (h "0fh3mmx7gb213kqscfdsbd05rxi3zp664anmx06vs7sn3zrhhskq")))

(define-public crate-puroro-protobuf-compiled-0.5.3 (c (n "puroro-protobuf-compiled") (v "0.5.3") (d (list (d (n "puroro") (r "^0.5.1") (d #t) (k 0)) (d (n "puroro") (r "^0.5.1") (d #t) (k 1)) (d (n "puroro-plugin") (r "^0.5.1") (d #t) (k 1)))) (h "0ysb97pcjvy24d4njc8k6rs6cx5s8yys6dfrd7mwc9dkkdrzwfhj") (f (quote (("default") ("compile_proto"))))))

(define-public crate-puroro-protobuf-compiled-0.5.4 (c (n "puroro-protobuf-compiled") (v "0.5.4") (d (list (d (n "puroro") (r "^0.5.1") (d #t) (k 0)))) (h "1dfhw2nwsdgb5lw1fiz90300p5smddvi2g4q0h6gnbjn6llv4s9p")))

(define-public crate-puroro-protobuf-compiled-0.5.5 (c (n "puroro-protobuf-compiled") (v "0.5.5") (d (list (d (n "puroro") (r "^0.5.1") (d #t) (k 0)))) (h "17z190jdifjdgv79dxxj0dn19bja9a3gyzrdfhgmrig97rdaxwiy")))

(define-public crate-puroro-protobuf-compiled-0.6.0 (c (n "puroro-protobuf-compiled") (v "0.6.0") (d (list (d (n "puroro") (r "^0.6.0") (d #t) (k 0)))) (h "0dlkdwlc5kwqvi91b5mail1pnzj2jxfl0733pvkx170xymdkkmlb")))

(define-public crate-puroro-protobuf-compiled-0.7.0 (c (n "puroro-protobuf-compiled") (v "0.7.0") (d (list (d (n "puroro") (r "^0.7.0") (d #t) (k 0)))) (h "0g5mal692kiazqdk3ahpgwi09agrc6dm2yq86llcjnzb4v97n5y3")))

(define-public crate-puroro-protobuf-compiled-0.8.0 (c (n "puroro-protobuf-compiled") (v "0.8.0") (d (list (d (n "puroro") (r "^0.8.0") (d #t) (k 0)))) (h "0kigv3m6jfahpccdnxqvvkpbwjmzgq3n104gvdz1pfrawycs5s25")))

(define-public crate-puroro-protobuf-compiled-0.9.0 (c (n "puroro-protobuf-compiled") (v "0.9.0") (d (list (d (n "puroro") (r "^0.9.0") (d #t) (k 0)))) (h "0xdx8nq84mdxjbrgn7zjmy4n3lzjm8kivqv48wz26j8c8qmjj76x")))

(define-public crate-puroro-protobuf-compiled-0.10.0 (c (n "puroro-protobuf-compiled") (v "0.10.0") (d (list (d (n "puroro") (r "^0.10.0") (d #t) (k 0)))) (h "0by1w2r2r2sdk156388044863al365zfdvmhrcjv5087cjkny9sr")))

(define-public crate-puroro-protobuf-compiled-0.11.0 (c (n "puroro-protobuf-compiled") (v "0.11.0") (d (list (d (n "puroro") (r "^0.10.0") (d #t) (k 0)))) (h "19i28x7r4mk8p2wi1g2y89bb4ngspc97m93q6jdja0k3bynri8p1")))

