(define-module (crates-io pu i- pui-cell) #:use-module (crates-io))

(define-public crate-pui-cell-0.5.0 (c (n "pui-cell") (v "0.5.0") (d (list (d (n "pui-core") (r "^0.5.1") (k 0)) (d (n "typsy") (r "^0.1") (k 0)))) (h "08gssxsln64vx0jbr8xgfy90gz7vkqrk92d3wb5myzapm9p4ix5n")))

(define-public crate-pui-cell-0.5.1 (c (n "pui-cell") (v "0.5.1") (d (list (d (n "pui-core") (r "^0.5.2") (k 0)) (d (n "typsy") (r "^0.1") (k 0)))) (h "0qhc0sp7w0wzjylyi7kxfla1ji8ssrf8jphfzxj711riiz9zlph2")))

