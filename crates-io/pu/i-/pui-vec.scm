(define-module (crates-io pu i- pui-vec) #:use-module (crates-io))

(define-public crate-pui-vec-0.5.0 (c (n "pui-vec") (v "0.5.0") (d (list (d (n "pui-core") (r "^0.5.1") (o #t) (k 0)))) (h "0wfh9qvaik33c1dk8k34i5ckz6rszbj80brr92al6fqlb6ncimaz") (f (quote (("pui" "pui-core") ("default" "pui"))))))

(define-public crate-pui-vec-0.5.1 (c (n "pui-vec") (v "0.5.1") (d (list (d (n "pui-core") (r "^0.5.2") (o #t) (k 0)))) (h "1fkaankf8xx2zwvywghfhf0yky6579lrmfppcb4lxrfbfa0yim2y") (f (quote (("pui" "pui-core") ("default" "pui"))))))

