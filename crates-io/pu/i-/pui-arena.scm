(define-module (crates-io pu i- pui-arena) #:use-module (crates-io))

(define-public crate-pui-arena-0.5.0 (c (n "pui-arena") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pui-core") (r "^0.5.1") (o #t) (k 0)) (d (n "pui-vec") (r "^0.5") (d #t) (k 0)))) (h "0d0z163b2p1qn8j02749kv9z64dgl48avszl2ww7fa8h9a3hdxyx") (f (quote (("slotmap") ("slab") ("scoped" "pui") ("pui" "pui-vec/pui" "pui-core"))))))

(define-public crate-pui-arena-0.5.1 (c (n "pui-arena") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pui-core") (r "^0.5.2") (o #t) (k 0)) (d (n "pui-vec") (r "^0.5.1") (k 0)))) (h "1czzzlccn12dk4vh1ywsjlqcvcscdzw9kalk3fdqznj93cvrb1zn") (f (quote (("slotmap") ("slab") ("scoped" "pui") ("pui" "pui-vec/pui" "pui-core") ("default" "pui"))))))

