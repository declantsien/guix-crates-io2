(define-module (crates-io pu mp pumpkindb_client) #:use-module (crates-io))

(define-public crate-pumpkindb_client-0.2.0 (c (n "pumpkindb_client") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "pumpkinscript") (r "^0.2") (d #t) (k 0)))) (h "06pslg0yl56lnbfs93l4j5rmd50jm7b871pp79652s0pgz8r94wh")))

