(define-module (crates-io pu mp pump) #:use-module (crates-io))

(define-public crate-pump-0.0.0 (c (n "pump") (v "0.0.0") (h "1cax8jkz2lr5jzmjbdbj9x0h1l84h8j6qnq34wqfrk0i8msm0kcc")))

(define-public crate-pump-0.0.1 (c (n "pump") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serialport") (r "^4.0.0") (k 0)))) (h "00aq2bqisa3f4hr1adkacw6j6jdg8liv90vryc2b3ma0y2z0xc5g") (f (quote (("libudev" "serialport/libudev"))))))

(define-public crate-pump-0.0.2 (c (n "pump") (v "0.0.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serialport") (r "^4.0.0") (k 0)))) (h "1ib9zkrmqw9gvcw2n36jfk9chv5pps0l86dnc47ra0zlv4wy4sn9") (f (quote (("libudev" "serialport/libudev"))))))

(define-public crate-pump-0.0.3 (c (n "pump") (v "0.0.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serialport") (r "^4.0.0") (k 0)))) (h "0lng9lpg8ki8qqgvkbhyl7m7q6l4vjfbjcb1mjqcmqgy8gpafbkj") (f (quote (("libudev" "serialport/libudev"))))))

(define-public crate-pump-0.0.4 (c (n "pump") (v "0.0.4") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serialport") (r "^4.0.0") (k 0)))) (h "1p7ws39gawap52v9mbara1mjxj2vjp6qqnc4lkan8rc56zn2gw5y") (f (quote (("libudev" "serialport/libudev"))))))

(define-public crate-pump-0.0.5 (c (n "pump") (v "0.0.5") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serialport") (r "^4.0.2") (k 0)))) (h "08zlswswfi4m1yhjkysbjqz1fzq61267p8l382g7zd904l217hbs") (f (quote (("libudev" "serialport/libudev"))))))

