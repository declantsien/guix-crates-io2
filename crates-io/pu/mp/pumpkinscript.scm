(define-module (crates-io pu mp pumpkinscript) #:use-module (crates-io))

(define-public crate-pumpkinscript-0.2.0 (c (n "pumpkinscript") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^3.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1.35") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "0xizv2z4smdwhd51h7mwndzc1y3qi5f2m1iyfq9j9z3sqij4laby")))

