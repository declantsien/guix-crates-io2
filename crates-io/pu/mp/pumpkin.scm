(define-module (crates-io pu mp pumpkin) #:use-module (crates-io))

(define-public crate-pumpkin-0.1.0 (c (n "pumpkin") (v "0.1.0") (d (list (d (n "custom_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "ramp") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "^0.3.11") (d #t) (k 0)))) (h "1mx85l5wf8lllisg7cqcrcl3irhzm435jb1h9zh9n1pp0ysymrj3")))

(define-public crate-pumpkin-0.2.0 (c (n "pumpkin") (v "0.2.0") (d (list (d (n "custom_derive") (r "0.1.*") (d #t) (k 0)) (d (n "newtype_derive") (r "0.1.*") (d #t) (k 0)) (d (n "ramp") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "1j860hv3vcn6ld5vi7zyqkwbd6a2sk1ai6ymy8996al4jaahdca5") (f (quote (("unstable"))))))

(define-public crate-pumpkin-0.2.1 (c (n "pumpkin") (v "0.2.1") (d (list (d (n "custom_derive") (r "0.1.*") (d #t) (k 0)) (d (n "newtype_derive") (r "0.1.*") (d #t) (k 0)) (d (n "ramp") (r "0.2.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "1v2js3zn0jzcia4jigikn6fy5q60d670qzd8lqjvnnysf0w94ipa") (f (quote (("unstable"))))))

(define-public crate-pumpkin-0.3.0 (c (n "pumpkin") (v "0.3.0") (d (list (d (n "custom_derive") (r "0.1.*") (d #t) (k 0)) (d (n "newtype_derive") (r "0.1.*") (d #t) (k 0)) (d (n "ramp") (r "0.2.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "0dwx6dralxy0yaw51ya696s5r7cdfr08zg66nl4yp9pmyxxkz2g4") (f (quote (("unstable"))))))

(define-public crate-pumpkin-1.0.0 (c (n "pumpkin") (v "1.0.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "custom_derive") (r "0.1.*") (d #t) (k 0)) (d (n "newtype_derive") (r "0.1.*") (d #t) (k 0)) (d (n "ramp") (r "0.2.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "1w7c3nqfwznfqfjhcmq9bv4wjrl9vk3dx5vrv8rxghawd00pf7xh") (f (quote (("unstable") ("dev" "clippy"))))))

(define-public crate-pumpkin-1.0.1 (c (n "pumpkin") (v "1.0.1") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "custom_derive") (r "0.1.*") (d #t) (k 0)) (d (n "newtype_derive") (r "0.1.*") (d #t) (k 0)) (d (n "ramp") (r "0.2.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "1d4vgm0s6w08ihi75mfzzwv2pa2ivx4azsyyj7daxv5vf86jsdyw") (f (quote (("unstable") ("dev" "clippy"))))))

(define-public crate-pumpkin-2.0.1 (c (n "pumpkin") (v "2.0.1") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "custom_derive") (r "^0.1") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1") (d #t) (k 0)) (d (n "ramp") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1qq11iwf1krchmf62fb7z31s55l1cwpd7qmvc9cdgd524ycqzyzl") (f (quote (("unstable") ("dev" "clippy"))))))

