(define-module (crates-io pu ba puball) #:use-module (crates-io))

(define-public crate-puball-0.1.0 (c (n "puball") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ixkrsp92ayr3chh94di5v3l64a8ch6l32i1shk79bgs3zwp3d2m")))

(define-public crate-puball-0.1.1 (c (n "puball") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03my8zybfafzqvy8iy3q7nfg7b4h20z80pgq59n26f09k22z2xrd")))

