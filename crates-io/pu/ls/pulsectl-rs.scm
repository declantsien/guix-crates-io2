(define-module (crates-io pu ls pulsectl-rs) #:use-module (crates-io))

(define-public crate-pulsectl-rs-0.2.13 (c (n "pulsectl-rs") (v "0.2.13") (d (list (d (n "libpulse-binding") (r "^2.24.0") (d #t) (k 0)))) (h "06ic7vics3m8115kml95l5kp9w3s04hlswkza6cj9yfgda5r14zj")))

(define-public crate-pulsectl-rs-0.2.14 (c (n "pulsectl-rs") (v "0.2.14") (d (list (d (n "libpulse-binding") (r "^2.24.0") (d #t) (k 0)))) (h "19d8dfv4awmf7pllljgj8m134w7z2h558ac7pwzsg8jb2085lw5l")))

(define-public crate-pulsectl-rs-0.3.0 (c (n "pulsectl-rs") (v "0.3.0") (d (list (d (n "pulse") (r "^2.24.0") (d #t) (k 0) (p "libpulse-binding")))) (h "00sj5v31jhr6ifdgcshxchhqr18asaaj26i3y0r13pxrlyvv6shi")))

(define-public crate-pulsectl-rs-0.3.1 (c (n "pulsectl-rs") (v "0.3.1") (d (list (d (n "pulse") (r "^2.24.0") (d #t) (k 0) (p "libpulse-binding")))) (h "1b9lyrzrf19ykn4wsn677lwglgfcnkk1rjv7v8863rapigii71jw")))

(define-public crate-pulsectl-rs-0.3.2 (c (n "pulsectl-rs") (v "0.3.2") (d (list (d (n "pulse") (r "^2.24.0") (d #t) (k 0) (p "libpulse-binding")))) (h "1z1wvirwajzmi6q25l3bivzxlgq79q7dm8y4bwn1p66ixsy8ia86")))

