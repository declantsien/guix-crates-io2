(define-module (crates-io pu ls pulsar-admin-rust) #:use-module (crates-io))

(define-public crate-pulsar-admin-rust-0.0.1 (c (n "pulsar-admin-rust") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("json" "__rustls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p9rkn2nrxiavfv58vk4d09j5h99yd98l9h4jiizcyjzdhzdfllg")))

