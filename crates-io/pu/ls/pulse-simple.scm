(define-module (crates-io pu ls pulse-simple) #:use-module (crates-io))

(define-public crate-pulse-simple-0.0.0 (c (n "pulse-simple") (v "0.0.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libpulse-sys") (r "^0.0.0") (d #t) (k 0)))) (h "1bscpl781ih18qlw3d2bfy4sznivrf9b2dmjw869kbsrr839bj4m")))

(define-public crate-pulse-simple-1.0.0 (c (n "pulse-simple") (v "1.0.0") (d (list (d (n "libc") (r "~0.2.14") (d #t) (k 0)) (d (n "libpulse-sys") (r "~0.0.0") (d #t) (k 0)))) (h "1849bxhmr0zg54r3ydfm339lhxx6i1ff47i4m5r8yllgjh7zjsjq")))

(define-public crate-pulse-simple-1.0.1 (c (n "pulse-simple") (v "1.0.1") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "libpulse-simple-sys") (r "^1.4") (d #t) (k 0)) (d (n "libpulse-sys") (r "^1.4") (d #t) (k 0)))) (h "019dw0rx50ar293bxczjnvf8sblmwfqw7r7l3xw3q304rfpk4qw4")))

