(define-module (crates-io pu ls pulse) #:use-module (crates-io))

(define-public crate-pulse-0.1.0 (c (n "pulse") (v "0.1.0") (d (list (d (n "atom") (r "*") (d #t) (k 0)))) (h "1gxk0nhqxs8ps9mfrlfl8nr1crvv94phmkaq8nbaj1dq4jc7r6vh")))

(define-public crate-pulse-0.2.0 (c (n "pulse") (v "0.2.0") (d (list (d (n "atom") (r "*") (d #t) (k 0)))) (h "18n14b4v8ih2qhhp0l79l1xd5ibzr8vqa8qdsq172w021d0gwxcf")))

(define-public crate-pulse-0.2.1 (c (n "pulse") (v "0.2.1") (d (list (d (n "atom") (r "*") (d #t) (k 0)))) (h "0wavhlc1ssgrk1rgv7xq7hvh44m2fn9d5xljcrayc9z21yjj89f3") (y #t)))

(define-public crate-pulse-0.2.2 (c (n "pulse") (v "0.2.2") (d (list (d (n "atom") (r "*") (d #t) (k 0)))) (h "0jihlrfm2555k076yfx032mkj50fnq42nb6c9qrydw462grp6pf9")))

(define-public crate-pulse-0.2.3 (c (n "pulse") (v "0.2.3") (d (list (d (n "atom") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "04rqq2racizpcd515rnrimqx5949dhasd64jxmab40ff2a7dgpnp")))

(define-public crate-pulse-0.3.0 (c (n "pulse") (v "0.3.0") (d (list (d (n "atom") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0vdbc14g3hyvffhw8kxvdpxa6x32vk64i7p5mzfzvacva73yzdw8")))

(define-public crate-pulse-0.3.1 (c (n "pulse") (v "0.3.1") (d (list (d (n "atom") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1ydrwwq7314mfns545jk04dsnsip3x1r2b9vr76qagvx91pk8d8x")))

(define-public crate-pulse-0.3.2 (c (n "pulse") (v "0.3.2") (d (list (d (n "atom") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0czjrb42dpk0v75rnppgmcfchhjjfdk02sk2c146900255kknldl") (f (quote (("default") ("callback"))))))

(define-public crate-pulse-0.3.3 (c (n "pulse") (v "0.3.3") (d (list (d (n "atom") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "14mjjkhn03q97a395kvg1kqmjji1d5rr8yhfcrr7ccfaky6rl2j1") (f (quote (("default") ("callback"))))))

(define-public crate-pulse-0.3.4 (c (n "pulse") (v "0.3.4") (d (list (d (n "atom") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1s2w5s63xd1i2rdbn0dpvjz86nl7q81vyfrgnz0r79fxp14qsk34") (f (quote (("default") ("callback"))))))

(define-public crate-pulse-0.3.5 (c (n "pulse") (v "0.3.5") (d (list (d (n "atom") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1ykfxi7pq40zp8y589z6gmvyr6dd5dxar5jyd9pwcqjpbr1yxah2") (f (quote (("default") ("callback"))))))

(define-public crate-pulse-0.3.6 (c (n "pulse") (v "0.3.6") (d (list (d (n "atom") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0x9afifmlr9blimhc1gdfrd33ni6sfva2gvvlyk3vqkdx9jm729p") (f (quote (("default") ("callback"))))))

(define-public crate-pulse-0.4.0 (c (n "pulse") (v "0.4.0") (d (list (d (n "atom") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "04jyprfps3bmcbdzfbjhrvdaf81zaqabm4nrh1mjxf0r18czhyhb") (f (quote (("default") ("callback"))))))

(define-public crate-pulse-0.5.0 (c (n "pulse") (v "0.5.0") (d (list (d (n "atom") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1wc1z6d7pmh915zaj9qpx96h7b9q2hl9cl3vb12yafvb1p2q16q2") (f (quote (("default") ("callback"))))))

(define-public crate-pulse-0.5.1 (c (n "pulse") (v "0.5.1") (d (list (d (n "atom") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0phdcdrygdbgrxm7h9n17rvxbr4w1fk9v84pbbdza86r5q0a9p3v") (f (quote (("default") ("callback"))))))

(define-public crate-pulse-0.5.2 (c (n "pulse") (v "0.5.2") (d (list (d (n "atom") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0pms6z5pd6214swc851118jbhwk4ah9zq420dp232272db37iy9i") (f (quote (("default") ("callback"))))))

(define-public crate-pulse-0.5.3 (c (n "pulse") (v "0.5.3") (d (list (d (n "atom") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1w4skcnwmavm8ra9blf1hy7bc9grnin2kziiyc18lsnrr2v14mk5") (f (quote (("default") ("callback"))))))

