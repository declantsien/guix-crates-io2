(define-module (crates-io pu ls pulsar-broker) #:use-module (crates-io))

(define-public crate-pulsar-broker-0.1.0 (c (n "pulsar-broker") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "0nk5by4jk8j0w082mb10gcc6z1y1hqihc4mvfm3963mad3x17m39")))

(define-public crate-pulsar-broker-0.1.1 (c (n "pulsar-broker") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "0yf383mld1iia877cg3d4z4gxras4rr885hdha3xxqgwx6x7pm80")))

