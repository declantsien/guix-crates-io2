(define-module (crates-io pu ls pulsar-network) #:use-module (crates-io))

(define-public crate-pulsar-network-0.1.2 (c (n "pulsar-network") (v "0.1.2") (d (list (d (n "neutrondb") (r "^0.9.19") (d #t) (k 0)) (d (n "opis") (r "^1.0.0") (d #t) (k 0)) (d (n "stellar-notation") (r "^0.9.18") (d #t) (k 0)))) (h "17i44akyg74qhifvvijq55fd1z0p8i6n2n5aq9wbkhlsshir03pd")))

(define-public crate-pulsar-network-0.4.1 (c (n "pulsar-network") (v "0.4.1") (d (list (d (n "astro-notation") (r "^3.1.0") (d #t) (k 0)) (d (n "fides") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02bkkl1hlah7z7rkmh2j86ij2ma2y95mdi8z98c6ax18a63ssvg3")))

(define-public crate-pulsar-network-0.4.2 (c (n "pulsar-network") (v "0.4.2") (d (list (d (n "astro-notation") (r "^3.1.0") (d #t) (k 0)) (d (n "fides") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "16cr7arjfpbiwdgkmsirmyk0ib7azzp5f1prjy8zhbf4jf38dv7y")))

(define-public crate-pulsar-network-0.4.3 (c (n "pulsar-network") (v "0.4.3") (d (list (d (n "astro-notation") (r "^3.1.0") (d #t) (k 0)) (d (n "fides") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rlyxn27h9vvzwni47lnpngghkqwjr6fc1dizq7mzjw2d1sinz2k")))

(define-public crate-pulsar-network-0.4.4 (c (n "pulsar-network") (v "0.4.4") (d (list (d (n "astro-notation") (r "^3.1.0") (d #t) (k 0)) (d (n "fides") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0kgbm27fbwrpx8w7zzjnzijh1h8hkax1h82n0vf8d93cxblxy0il")))

(define-public crate-pulsar-network-0.4.5 (c (n "pulsar-network") (v "0.4.5") (d (list (d (n "astro-notation") (r "^3.1.0") (d #t) (k 0)) (d (n "fides") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1y3h8hvmvjccv948j0zb9yg04m975n59wbxrav12jq1ir9k1clbd")))

(define-public crate-pulsar-network-0.4.6 (c (n "pulsar-network") (v "0.4.6") (d (list (d (n "astro-notation") (r "^3.1.0") (d #t) (k 0)) (d (n "fides") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1pqbr3pwbhcyxg3dslh34373ib06z6rlacf8n0zgibz0a3vx9cb9")))

(define-public crate-pulsar-network-0.4.7 (c (n "pulsar-network") (v "0.4.7") (d (list (d (n "astro-notation") (r "^3.1.0") (d #t) (k 0)) (d (n "fides") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0m86dk1q8miwxxcvzhd54m23wv79qm3qfxdlakizg391p7lzxpp6")))

(define-public crate-pulsar-network-0.5.0 (c (n "pulsar-network") (v "0.5.0") (d (list (d (n "astro-notation") (r "^3.1.0") (d #t) (k 0)) (d (n "fides") (r "^2.0.0") (d #t) (k 0)) (d (n "opis") (r "^3.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0qv26wvpcb2j02b646hvviqy13wh6idn2s3yqlb2lzxfnjlvzzws")))

(define-public crate-pulsar-network-0.5.1 (c (n "pulsar-network") (v "0.5.1") (d (list (d (n "astro-notation") (r "^3.1.0") (d #t) (k 0)) (d (n "fides") (r "^2.0.0") (d #t) (k 0)) (d (n "opis") (r "^3.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1nc3c057vyn3hv6nalmqaa0lrbsn289rkcd4m3jm638ijgxpzylv")))

(define-public crate-pulsar-network-0.5.2 (c (n "pulsar-network") (v "0.5.2") (d (list (d (n "astro-notation") (r "^3.1.0") (d #t) (k 0)) (d (n "fides") (r "^2.0.0") (d #t) (k 0)) (d (n "opis") (r "^3.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "17206q5q45swrnf3ffvry7x2y6yya9vsqj1cj4ksqd7j761hs4yb")))

(define-public crate-pulsar-network-0.5.3 (c (n "pulsar-network") (v "0.5.3") (d (list (d (n "astro-notation") (r "^3.1.0") (d #t) (k 0)) (d (n "fides") (r "^2.0.0") (d #t) (k 0)) (d (n "opis") (r "^3.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1s4brij28gkaha3jm1ws2xmmixwirbljqliahksp56d61w0q67a4")))

(define-public crate-pulsar-network-0.6.0 (c (n "pulsar-network") (v "0.6.0") (d (list (d (n "astro-notation") (r "^3.1.0") (d #t) (k 0)) (d (n "fides") (r "^2.0.0") (d #t) (k 0)) (d (n "opis") (r "^3.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "10f47xvv0hcjxldg2jsw0lg535vznhfdhnf7wglwp27092jsc21b")))

(define-public crate-pulsar-network-0.6.1 (c (n "pulsar-network") (v "0.6.1") (d (list (d (n "astro-notation") (r "^3.1.0") (d #t) (k 0)) (d (n "fides") (r "^2.0.0") (d #t) (k 0)) (d (n "opis") (r "^3.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "15kp7g9kkp37mk5jgi94kfz90zmmzqqd2vczs8i8xi9hr6x4f0xl")))

(define-public crate-pulsar-network-0.7.0 (c (n "pulsar-network") (v "0.7.0") (d (list (d (n "astro-format") (r "^0.5.0") (d #t) (k 0)) (d (n "fides") (r "^2.2.1") (d #t) (k 0)) (d (n "opis") (r "^3.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0859gzl18pnd4p12q0093rm6lgn8wm170rj8rqirxims23ng6769")))

(define-public crate-pulsar-network-0.8.0 (c (n "pulsar-network") (v "0.8.0") (d (list (d (n "astro-format") (r "^0.8.0") (d #t) (k 0)) (d (n "fides") (r "^2.3.0") (d #t) (k 0)) (d (n "opis") (r "^3.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1h2pqzmzhqr8nrmj11szhw9qixgrif0yapkd6440z5s1lg2d77f2")))

