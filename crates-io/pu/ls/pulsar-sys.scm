(define-module (crates-io pu ls pulsar-sys) #:use-module (crates-io))

(define-public crate-pulsar-sys-2.6.0 (c (n "pulsar-sys") (v "2.6.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "1j7vvnlrvb17dwcwl87qnp1cmjb16jw85i4xj1p7pgb3bkj4az2z") (l "pulsar")))

(define-public crate-pulsar-sys-2.6.1 (c (n "pulsar-sys") (v "2.6.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "0yjzd8dms0acmnalwwgahj80gpsnhvvnryqnkqzjdni1rapabcj4") (l "pulsar")))

(define-public crate-pulsar-sys-2.7.0 (c (n "pulsar-sys") (v "2.7.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "195cwni1ah94hspqx5r1k8xp5ph7pc63191lbdyn8wz35pw39ra5") (l "pulsar")))

