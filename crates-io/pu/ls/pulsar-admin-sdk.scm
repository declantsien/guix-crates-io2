(define-module (crates-io pu ls pulsar-admin-sdk) #:use-module (crates-io))

(define-public crate-pulsar-admin-sdk-2.0.0 (c (n "pulsar-admin-sdk") (v "2.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1lsyl8qi9jfz5b85dmmvjyfvdzf0l8jwfhk03q8mmw8jnqf78hk2")))

(define-public crate-pulsar-admin-sdk-2.1.0 (c (n "pulsar-admin-sdk") (v "2.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0m5yvaxns72357hpbvd4yr43qflm5vy7fqs23550xbpg2jqrqrwb")))

