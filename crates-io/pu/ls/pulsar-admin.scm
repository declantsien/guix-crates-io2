(define-module (crates-io pu ls pulsar-admin) #:use-module (crates-io))

(define-public crate-pulsar-admin-0.0.1 (c (n "pulsar-admin") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("json" "__rustls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "tokio") (r "^1.13.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01bmnmfzrq0sgw9rlc5xz09lq6gyp1znchwmbjc9ddlamhpcrhb7")))

