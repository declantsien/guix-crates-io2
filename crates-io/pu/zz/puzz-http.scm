(define-module (crates-io pu zz puzz-http) #:use-module (crates-io))

(define-public crate-puzz-http-0.1.0 (c (n "puzz-http") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "0rxiz7vhhhjq9q9v5g2bz1z3w6pf4zpf6prg8i3alsfspw647564")))

