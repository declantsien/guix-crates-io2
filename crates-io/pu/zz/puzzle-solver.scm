(define-module (crates-io pu zz puzzle-solver) #:use-module (crates-io))

(define-public crate-puzzle-solver-0.1.0 (c (n "puzzle-solver") (v "0.1.0") (d (list (d (n "bit-set") (r "^0.4") (d #t) (k 0)))) (h "0v05cyi5rlczn0f57w2yazfwva6k59c5mwxk059gpaa84yhz7ksg")))

(define-public crate-puzzle-solver-0.2.0 (c (n "puzzle-solver") (v "0.2.0") (d (list (d (n "bit-set") (r "^0.4") (d #t) (k 0)))) (h "166iidjp5iv2y5lkhad5vsibn7541r3crhcsih49q4j0d0c84jjx")))

(define-public crate-puzzle-solver-0.3.0 (c (n "puzzle-solver") (v "0.3.0") (d (list (d (n "bit-set") (r "^0.4") (d #t) (k 0)))) (h "0vqcjycj6ilfi47sh172mga201463ahs38ljpz13m3l3jl7fdh08")))

(define-public crate-puzzle-solver-0.4.0 (c (n "puzzle-solver") (v "0.4.0") (d (list (d (n "bit-set") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0mfzz7454w1j2jrqh64v08ac7r5gf1m6pnlnd655qlqinp18w6gp")))

(define-public crate-puzzle-solver-0.4.1 (c (n "puzzle-solver") (v "0.4.1") (d (list (d (n "bit-set") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "17h3rdcbdnsb876v8kjnz7ljr2ri1gxi42cp7r47mi0cs4fis54n")))

