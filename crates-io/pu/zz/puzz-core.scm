(define-module (crates-io pu zz puzz-core) #:use-module (crates-io))

(define-public crate-puzz-core-0.1.0 (c (n "puzz-core") (v "0.1.0") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "puzz-http") (r "^0.1.0") (d #t) (k 0)) (d (n "puzz-service") (r "^0.1.0") (f (quote ("util"))) (d #t) (k 0)))) (h "1n74lbnrwvzw9mn6k5awanjnik9dihbrsjyd9l09fq6xbldxhw3j")))

