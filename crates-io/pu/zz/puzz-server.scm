(define-module (crates-io pu zz puzz-server) #:use-module (crates-io))

(define-public crate-puzz-server-0.1.0 (c (n "puzz-server") (v "0.1.0") (d (list (d (n "actix-http") (r "^3") (f (quote ("http2"))) (o #t) (d #t) (k 0)) (d (n "actix-server") (r "^2") (o #t) (d #t) (k 0)) (d (n "actix-service") (r "^2") (o #t) (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "puzz-core") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util"))) (k 2)))) (h "0qf1n5v8358mb929xd2mhfjajxzr65qnjnq21l8wkgb2603l80jm") (f (quote (("default" "actix") ("actix" "actix-http" "actix-server" "actix-service"))))))

