(define-module (crates-io pu zz puzz-route) #:use-module (crates-io))

(define-public crate-puzz-route-0.1.0 (c (n "puzz-route") (v "0.1.0") (d (list (d (n "matchit") (r "^0.5") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "puzz-core") (r "^0.1.0") (d #t) (k 0)))) (h "0n8ghi523hkcp04a0c6w0mkiss1xrc5zz2lcmigc53ax68klgz24")))

