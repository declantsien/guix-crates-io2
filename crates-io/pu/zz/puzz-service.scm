(define-module (crates-io pu zz puzz-service) #:use-module (crates-io))

(define-public crate-puzz-service-0.1.0 (c (n "puzz-service") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (o #t) (d #t) (k 0)))) (h "13fk4vsfckvgbrjcfkmjrqmnhg2sq94s3ig60j4znriidh22r403") (f (quote (("util" "pin-project-lite" "futures-util" "alloc") ("default" "alloc") ("alloc"))))))

