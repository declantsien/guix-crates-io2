(define-module (crates-io pu zz puzz-middleware) #:use-module (crates-io))

(define-public crate-puzz-middleware-0.1.0 (c (n "puzz-middleware") (v "0.1.0") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "puzz-core") (r "^0.1.0") (d #t) (k 0)))) (h "0gyky0jpggrmp2dr4ry5nq8g5094m8shs58k1n7i1nvig6q5fdmq") (f (quote (("default") ("core"))))))

