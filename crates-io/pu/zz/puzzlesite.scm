(define-module (crates-io pu zz puzzlesite) #:use-module (crates-io))

(define-public crate-puzzlesite-0.1.0 (c (n "puzzlesite") (v "0.1.0") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "02kb8gcg33jr89b1vdz3pm7g9hhrlq31191wp6bvh2q31bvcxy7p")))

(define-public crate-puzzlesite-0.1.1 (c (n "puzzlesite") (v "0.1.1") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "1ap5lvmcwb64cl4igar84ng8yscz7xgp0rxvfhjnwbxa1m9yz6ai")))

(define-public crate-puzzlesite-0.2.0 (c (n "puzzlesite") (v "0.2.0") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0w2q0alv2gzrx1w1fns0fymn9nqvaad26694ddgysvwqdw9a90dy")))

(define-public crate-puzzlesite-0.3.0 (c (n "puzzlesite") (v "0.3.0") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "1pb0nv2089wld3f1rvqs94bzr6w9f8l2shxv372jq28ihg890bln")))

(define-public crate-puzzlesite-0.3.1 (c (n "puzzlesite") (v "0.3.1") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "100a1hnj5kxkrgm24z5fx4vpkn93cl6il8kfzl0j12d7sq830l69")))

(define-public crate-puzzlesite-0.4.0 (c (n "puzzlesite") (v "0.4.0") (d (list (d (n "axum") (r "^0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "1sy507fjl4xqlra6jsxd3y2bj5wgnz5jf071lwwq14xdwaclbj0a")))

