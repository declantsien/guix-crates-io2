(define-module (crates-io pu zz puzz-sse) #:use-module (crates-io))

(define-public crate-puzz-sse-0.1.0 (c (n "puzz-sse") (v "0.1.0") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "memchr") (r "^2.4") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "puzz-core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (k 0)))) (h "052cppf5dy2kn3s0y67ag5s2q0gz1wl37vzlg4w7l3mx7ccl2mlv")))

