(define-module (crates-io pu zz puzzle_cube) #:use-module (crates-io))

(define-public crate-puzzle_cube-0.1.0 (c (n "puzzle_cube") (v "0.1.0") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 1)) (d (n "getch") (r "^0.2.1") (d #t) (k 0)))) (h "1h05dwbd1m0agyqgll23qaf3v559knvvns8j03a86cbmw0f986k0")))

(define-public crate-puzzle_cube-0.1.1 (c (n "puzzle_cube") (v "0.1.1") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 1)) (d (n "getch") (r "^0.2.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "1p0isdg7ivb1xrq8r7q5dvznm7vgmq6b5i32wfpc5c0n49xs78nx")))

(define-public crate-puzzle_cube-0.1.2 (c (n "puzzle_cube") (v "0.1.2") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 1)) (d (n "getch") (r "^0.2.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "0yr8jk20ywskdlla1ld19a69b05n62bablbz996324kszh87x2fz")))

(define-public crate-puzzle_cube-0.1.3 (c (n "puzzle_cube") (v "0.1.3") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 1)) (d (n "getch") (r "^0.2.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "0fbqbxafxdswv979510pq3qkcm8raw8kw5h5hzgwr2dqs44sfbfy")))

