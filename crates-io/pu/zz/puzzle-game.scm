(define-module (crates-io pu zz puzzle-game) #:use-module (crates-io))

(define-public crate-puzzle-game-0.1.0 (c (n "puzzle-game") (v "0.1.0") (d (list (d (n "device_query") (r "^1.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.9.0-alpha.1") (d #t) (k 0)))) (h "0r6by97123xq4alpzjdy0ifghmkan2m3ra3ib3pq1zd9n25h4asb")))

