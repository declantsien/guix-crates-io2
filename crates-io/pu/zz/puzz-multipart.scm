(define-module (crates-io pu zz puzz-multipart) #:use-module (crates-io))

(define-public crate-puzz-multipart-0.1.0 (c (n "puzz-multipart") (v "0.1.0") (d (list (d (n "actix-http") (r "^3") (d #t) (k 0)) (d (n "actix-multipart") (r "^0.4.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "0b81x7j1pbzr4gmin1zf3zw13lsf8frzvcry471b9yv1i8q72pdi")))

