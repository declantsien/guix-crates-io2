(define-module (crates-io pu zz puzzles) #:use-module (crates-io))

(define-public crate-puzzles-0.1.0 (c (n "puzzles") (v "0.1.0") (d (list (d (n "arrayvec") (r "0.7.*") (d #t) (k 0)) (d (n "bitm") (r "^0.2") (d #t) (k 0)) (d (n "csf") (r "^0.1") (d #t) (k 0)) (d (n "ph") (r "^0.8") (d #t) (k 0)))) (h "0wvbi560xqz0igv160m72lb67lkj0f9ly4rpb5hr7hyz4ngbyyhs")))

