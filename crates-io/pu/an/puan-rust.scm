(define-module (crates-io pu an puan-rust) #:use-module (crates-io))

(define-public crate-puan-rust-0.1.0 (c (n "puan-rust") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "025c4wg7lbmr3dv8ynjgaq5ip7yq16wj5x5rg58nci8fgfqf4jcg") (y #t)))

(define-public crate-puan-rust-0.1.1 (c (n "puan-rust") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.4") (d #t) (k 0)))) (h "09dkq89k7r3gj1035l997f2lnidxz19lg019hvz8ffgark8myg9z")))

(define-public crate-puan-rust-0.1.2 (c (n "puan-rust") (v "0.1.2") (d (list (d (n "itertools") (r "^0.10.4") (d #t) (k 0)))) (h "0gwnwfss596hxrpj00amifqc2vxzq5z5q9py7m7h437nwj2xv4zv")))

(define-public crate-puan-rust-0.1.3 (c (n "puan-rust") (v "0.1.3") (d (list (d (n "itertools") (r "^0.10.4") (d #t) (k 0)))) (h "10x9fwkbgygygapdpyf70ck5lf4wr3bc8z5fhv2sfdfwci3nahfw")))

(define-public crate-puan-rust-0.1.4 (c (n "puan-rust") (v "0.1.4") (d (list (d (n "itertools") (r "^0.10.4") (d #t) (k 0)))) (h "1qamacz9mvcm0x9qh5xnphych4ialm1sq71f841mis6k55plj2ii")))

(define-public crate-puan-rust-0.1.5 (c (n "puan-rust") (v "0.1.5") (d (list (d (n "itertools") (r "^0.10.4") (d #t) (k 0)))) (h "077qy2mwsx8z1mqn0jas8xca3yfm9drhb9flfy0y6a7b8nqgdgbz")))

(define-public crate-puan-rust-0.1.6 (c (n "puan-rust") (v "0.1.6") (d (list (d (n "itertools") (r "^0.10.4") (d #t) (k 0)))) (h "0j5548kqdqfpfpwh2r861gvwhk0yc66k5fx8hfyh8xzjxnkbjyia")))

(define-public crate-puan-rust-0.1.7 (c (n "puan-rust") (v "0.1.7") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.10.4") (d #t) (k 0)))) (h "1cyppx6sqnj8kb782k0sdjyhjkzhbvp4dhhd5zr0akl92ncdvljn")))

(define-public crate-puan-rust-0.1.8 (c (n "puan-rust") (v "0.1.8") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.10.4") (d #t) (k 0)))) (h "07znpcl2n2cnpyfd9yv3rnfam2nsksb4cq7fr96pq0p7gj5bl3z9")))

(define-public crate-puan-rust-0.1.9 (c (n "puan-rust") (v "0.1.9") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.10.4") (d #t) (k 0)))) (h "05zj80v1q0g20dzjmfrmfl7lzqdsl0pyb8ph885kzpzqrib4vqxv")))

