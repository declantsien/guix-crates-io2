(define-module (crates-io pu an puan-pv) #:use-module (crates-io))

(define-public crate-puan-pv-0.1.0 (c (n "puan-pv") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "05ica96hl88v7227xgz34j23f2x8w78xy2yjpkyzbi5qwnjmw6kf")))

(define-public crate-puan-pv-0.1.1 (c (n "puan-pv") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0n3v96cvvjqfm5c02ngdk34fll5vi3v18s6ngjpv83hzxzkk12jn")))

(define-public crate-puan-pv-0.1.2 (c (n "puan-pv") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1f78kgailng2cn8099hsrlsgvicjqa69xm89qsknk0a22prsjxl3")))

(define-public crate-puan-pv-0.1.3 (c (n "puan-pv") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "02vag7ywgdq79v0pl8grccpz99y3bbc8bwqn0zzyqj72sr4djzb0")))

(define-public crate-puan-pv-0.1.4 (c (n "puan-pv") (v "0.1.4") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.188") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1n7rw8ml5lyxkchw72cgl7lc042krz5y2jgl804avlgianydkrpj")))

