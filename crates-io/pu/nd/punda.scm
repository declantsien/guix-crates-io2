(define-module (crates-io pu nd punda) #:use-module (crates-io))

(define-public crate-punda-0.1.0 (c (n "punda") (v "0.1.0") (d (list (d (n "alloc-cortex-m") (r "^0.3.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.2") (f (quote ("inline-asm"))) (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.1") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "nrf51") (r "^0.5.0") (d #t) (k 0)) (d (n "nrf51-hal") (r "^0.5.5") (f (quote ("rt"))) (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.5.1") (d #t) (k 0)))) (h "09xbnj4nxmd9l6hkfmmv9mhwi1zjyr99zsd3m70qkpjhznz349mh")))

