(define-module (crates-io pu b_ pub_this) #:use-module (crates-io))

(define-public crate-pub_this-0.1.0 (c (n "pub_this") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zns7argy1bf6hpss2z3k7iil92cbc3nyyww964g9z6w7jsyxp16")))

