(define-module (crates-io pu so pusoy_dos) #:use-module (crates-io))

(define-public crate-pusoy_dos-0.1.0 (c (n "pusoy_dos") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dsdqgflxfi6025fkd8qlvkmzw13a9ar9xcqj9q0vngvgssz2w4q")))

(define-public crate-pusoy_dos-0.1.1 (c (n "pusoy_dos") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vpvr0dnnciriyb61pgirpmbfkkd04z9bz9d19l2ddh3gnjr5lz9")))

