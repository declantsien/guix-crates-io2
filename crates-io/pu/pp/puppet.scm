(define-module (crates-io pu pp puppet) #:use-module (crates-io))

(define-public crate-puppet-0.1.0 (c (n "puppet") (v "0.1.0") (d (list (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "puppet-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "13y3g1b9zcnxsxsz02ni6n82hns69fc4hh5arn46zxfmwf70nwss")))

(define-public crate-puppet-0.1.1 (c (n "puppet") (v "0.1.1") (d (list (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "puppet-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1j82jsxpb36v6chm0kkv1ayx8xyklr17hhxanf9z1bdj0qyk3v3k")))

(define-public crate-puppet-0.1.2 (c (n "puppet") (v "0.1.2") (d (list (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "puppet-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "15qqdqqrhxg8q22vs4cvv6xqbvnl7yzb0sqw7g6dm3vqcn807kjh")))

(define-public crate-puppet-0.1.3 (c (n "puppet") (v "0.1.3") (d (list (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "puppet-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0i74lmk812baaks811xiy1fyqqnpk5fl6xk838vxwk1aq8aln7f8")))

(define-public crate-puppet-0.1.4 (c (n "puppet") (v "0.1.4") (d (list (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "puppet-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1b9m83qx5g5ngcvjj059fpv0495i3mvwkhff3sx9fs2jc86sc8ih")))

(define-public crate-puppet-0.2.0 (c (n "puppet") (v "0.2.0") (d (list (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "puppet-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1p838i7yj036g71vp1xjm1g827y95s61wlc3x54vnmc5v60qbv7m")))

(define-public crate-puppet-0.3.0 (c (n "puppet") (v "0.3.0") (d (list (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "puppet-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1zaw5mvmzp32x62pkvwg9xwq6sq9czw13a823kwpp6vkpchii243") (f (quote (("helper-methods" "tokio" "puppet-derive/helper-methods" "puppet-derive/custom-executor") ("default" "helper-methods") ("custom-executor" "puppet-derive/custom-executor"))))))

(define-public crate-puppet-0.4.0 (c (n "puppet") (v "0.4.0") (d (list (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "puppet-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0m5b18jmc22b4bifi47hpx1ryfy1xahmsipfqvvnhj0xa04j61k9") (f (quote (("helper-methods" "tokio" "puppet-derive/helper-methods" "puppet-derive/custom-executor") ("default" "helper-methods") ("custom-executor" "puppet-derive/custom-executor"))))))

