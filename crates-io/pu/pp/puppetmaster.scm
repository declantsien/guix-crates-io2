(define-module (crates-io pu pp puppetmaster) #:use-module (crates-io))

(define-public crate-puppetmaster-0.1.0 (c (n "puppetmaster") (v "0.1.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "ggez") (r "^0.7.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)))) (h "0lrzq19bglviflb02lx93hnlcdl3mx4h2hp5p3666x10g79nv0ap")))

