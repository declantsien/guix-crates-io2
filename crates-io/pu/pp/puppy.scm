(define-module (crates-io pu pp puppy) #:use-module (crates-io))

(define-public crate-puppy-0.0.1 (c (n "puppy") (v "0.0.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hjkdyc3fidyyyqkhnpci5rzps21vq3jq7ldwwcn0iz5s57567x7")))

(define-public crate-puppy-0.0.2 (c (n "puppy") (v "0.0.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1d89r7lk6f9bakbwzmggs113y7j1va54myfk24by3y9p3w6f8d85")))

(define-public crate-puppy-0.0.3 (c (n "puppy") (v "0.0.3") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "0k0c8wwby8a7lf099czfvhvakwbrfh5y5yawgyw9ykqqc18gfzvi")))

(define-public crate-puppy-0.0.4 (c (n "puppy") (v "0.0.4") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "03zhfbcqlaiixybn0r44g17hv3f99bqcljfgcak8q8s4sdnpskdz")))

(define-public crate-puppy-0.0.5 (c (n "puppy") (v "0.0.5") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "1ybkbqza7qgh756lz7qq71xqy01g5qkynj3zwk4qa6ma091g94g1")))

(define-public crate-puppy-0.0.7 (c (n "puppy") (v "0.0.7") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "10mf6nz26y8zpa6ds5hssmddxm3gzc34g8v5n3fwsnypn60hspdd")))

(define-public crate-puppy-0.0.8 (c (n "puppy") (v "0.0.8") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "18npy910xb5hjmq6c33bk5dm8syvsb91dpr44x3vx1a8zxsf0b79")))

