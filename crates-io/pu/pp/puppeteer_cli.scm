(define-module (crates-io pu pp puppeteer_cli) #:use-module (crates-io))

(define-public crate-puppeteer_cli-0.1.0 (c (n "puppeteer_cli") (v "0.1.0") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "chromiumoxide") (r "^0.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)))) (h "1jq3hvz7cz0cqzps8ml4fhd8j12f7s6qr94hcxwqc7ragdsz57hw")))

(define-public crate-puppeteer_cli-0.1.1 (c (n "puppeteer_cli") (v "0.1.1") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "chromiumoxide") (r "^0.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)))) (h "1jhzz0739rkmrj4h93d8vq16d06h8x1yqhg6az5aylars27baigz")))

(define-public crate-puppeteer_cli-0.1.2 (c (n "puppeteer_cli") (v "0.1.2") (d (list (d (n "async-channel") (r "^1.8.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "chromiumoxide") (r "^0.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)))) (h "1cil6jpbv2gfbavkqbdnn2j2gg8m9a22pprmxjva8fgsa6zsl12n")))

