(define-module (crates-io pu pp puppet-derive) #:use-module (crates-io))

(define-public crate-puppet-derive-0.1.0 (c (n "puppet-derive") (v "0.1.0") (d (list (d (n "crossbeam-queue") (r "^0.3.8") (d #t) (k 0)) (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wybll0g0nyi6nlhrc1nvh6qc18nin90hmryhh7967r1z3ygwwly")))

(define-public crate-puppet-derive-0.1.1 (c (n "puppet-derive") (v "0.1.1") (d (list (d (n "crossbeam-queue") (r "^0.3.8") (d #t) (k 0)) (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f6vq0kyjkzxf9z4ai8g23kywn8yizc8sbwsqdzpx9y2wdanzfg4")))

(define-public crate-puppet-derive-0.1.2 (c (n "puppet-derive") (v "0.1.2") (d (list (d (n "crossbeam-queue") (r "^0.3.8") (d #t) (k 0)) (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1iyc8frx3h03zv6xjiir07q1f5r1hri0bzv0av4amyz4nhkjqrpy")))

(define-public crate-puppet-derive-0.2.0 (c (n "puppet-derive") (v "0.2.0") (d (list (d (n "crossbeam-queue") (r "^0.3.8") (d #t) (k 0)) (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wyl54bghf5xg6xccq4f2agq2466vkinc1741z9xlx6bbp0i1hkz") (f (quote (("helper-methods") ("custom-executor"))))))

(define-public crate-puppet-derive-0.3.0 (c (n "puppet-derive") (v "0.3.0") (d (list (d (n "crossbeam-queue") (r "^0.3.8") (d #t) (k 0)) (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0py2rvfssxkqkja3n1vxnm7x4a3s60gyc06jz8lqbslzw25gz8sd") (f (quote (("helper-methods") ("custom-executor"))))))

