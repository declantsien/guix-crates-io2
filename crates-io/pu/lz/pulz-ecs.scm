(define-module (crates-io pu lz pulz-ecs) #:use-module (crates-io))

(define-public crate-pulz-ecs-0.0.1 (c (n "pulz-ecs") (v "0.0.1") (d (list (d (n "pulz-arena") (r "^0.0.1") (d #t) (k 0)))) (h "002adxfbvgq6xfy0drck1ysv9c4kv7n4qf5cpkw347896fdcxyjk")))

(define-public crate-pulz-ecs-0.1.0-alpha (c (n "pulz-ecs") (v "0.1.0-alpha") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "pulz-bitset") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "pulz-ecs-macros") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "pulz-schedule") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "slotmap") (r "^1.0") (d #t) (k 0)))) (h "1sqww6hz6f78vk3z28ysps82f5x8l8rxy695144zipcg9ng0l654")))

