(define-module (crates-io pu lz pulz-arena) #:use-module (crates-io))

(define-public crate-pulz-arena-0.0.1 (c (n "pulz-arena") (v "0.0.1") (h "04ifabqwkji1hq3w5510f2ykblnha675zg8xfpdh5mv6jmywh62y")))

(define-public crate-pulz-arena-0.1.0 (c (n "pulz-arena") (v "0.1.0") (h "1j7l8xvxl3lw2w8hsx6prp52b0r6hlpxay0npl0sfab18j6xsh88")))

(define-public crate-pulz-arena-0.1.1 (c (n "pulz-arena") (v "0.1.1") (h "18izhk1syv2vhj9wis4h48vc7x2lij8vmhv0ji4bxbcfv9zdpwwf")))

(define-public crate-pulz-arena-0.2.0 (c (n "pulz-arena") (v "0.2.0") (h "0vknwpsi5248idi61n0bz41dg5dz5qbcw1gr2zkqg1ag5av1h3p5")))

(define-public crate-pulz-arena-0.3.0 (c (n "pulz-arena") (v "0.3.0") (h "1bi9wjyp89ckff1mjwic36z8rfwmbs7r3wh0khix2m58jk9jwhv0")))

(define-public crate-pulz-arena-0.4.0 (c (n "pulz-arena") (v "0.4.0") (h "0ibfkxvkbhmkxlll2kp0ispcy8038a8rvr755dgz1bx04aicmrk7")))

