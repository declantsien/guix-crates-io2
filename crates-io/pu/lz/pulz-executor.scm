(define-module (crates-io pu lz pulz-executor) #:use-module (crates-io))

(define-public crate-pulz-executor-0.1.0-alpha (c (n "pulz-executor") (v "0.1.0-alpha") (d (list (d (n "async-std") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "async-std") (r "^1.10") (f (quote ("attributes"))) (d #t) (t "cfg(not(target_os = \"unknown\"))") (k 2)) (d (n "tokio") (r "^1.12") (f (quote ("rt"))) (o #t) (k 0)))) (h "0wbgd7pkj5g1nqryf59i3lgp0cf26xi1ibbw79q6bish7nxhjfhz")))

