(define-module (crates-io pu lz pulz-schedule) #:use-module (crates-io))

(define-public crate-pulz-schedule-0.1.0-alpha (c (n "pulz-schedule") (v "0.1.0-alpha") (d (list (d (n "async-std") (r "^1.10") (f (quote ("attributes"))) (d #t) (t "cfg(not(target_os = \"unknown\"))") (k 2)) (d (n "atomic_refcell") (r "^0.1") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.2") (d #t) (k 0)) (d (n "pulz-executor") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "pulz-executor") (r "^0.1.0-alpha") (f (quote ("async-std"))) (d #t) (t "cfg(not(target_os = \"unknown\"))") (k 2)) (d (n "tinybox") (r "^0.2") (d #t) (k 0)))) (h "00z9szzy5h3a30v483wycmf2yaaqvb5xyknb157lzbqviwnc7qic")))

