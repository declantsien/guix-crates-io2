(define-module (crates-io pu lz pulz-ecs-macros) #:use-module (crates-io))

(define-public crate-pulz-ecs-macros-0.1.0-alpha (c (n "pulz-ecs-macros") (v "0.1.0-alpha") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19rmlzr2bs75mm7lbfcz0q2lhpzp0an8hagx6dsvcrmbm6wbki04")))

