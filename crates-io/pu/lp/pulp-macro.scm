(define-module (crates-io pu lp pulp-macro) #:use-module (crates-io))

(define-public crate-pulp-macro-0.1.0 (c (n "pulp-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1ndxk9i90h963aa1xdxnqxwnh83x0b73mv0pwpsjz5rc56jm569z")))

(define-public crate-pulp-macro-0.1.1 (c (n "pulp-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0ripa5aw418lindx78rjc1dgccvanlf2a48fpirlh3kqgccv65fk")))

