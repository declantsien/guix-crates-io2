(define-module (crates-io pu rp purp-value) #:use-module (crates-io))

(define-public crate-purp-value-0.1.0 (c (n "purp-value") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "pest") (r "^2.5.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.7") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1p1f3zjhiqxlp9ahgsks5l28l7lbvgsiiikffq8pzgm3nzfxx6lc") (f (quote (("parser" "json") ("json") ("default" "parser")))) (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-purp-value-0.1.1 (c (n "purp-value") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "pest") (r "^2.5.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.7") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0cmwz2hyh0s487xnpck2jj4g6s4f11hfa2cpxmk8ba47f8cf88zy") (f (quote (("parser" "json") ("json") ("default" "parser")))) (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-purp-value-0.1.2 (c (n "purp-value") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "pest") (r "^2.5.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.7") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1wmq29l3nvypymg3gkdd5r5zfi27yaqhmw4scn7i6i03bnmghn6g") (f (quote (("parser" "json") ("json") ("default" "parser")))) (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

