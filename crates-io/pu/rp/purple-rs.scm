(define-module (crates-io pu rp purple-rs) #:use-module (crates-io))

(define-public crate-purple-rs-0.1.0 (c (n "purple-rs") (v "0.1.0") (d (list (d (n "glib") (r "^0") (d #t) (k 0)) (d (n "glib-sys") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "purple-sys") (r "^0") (d #t) (k 0) (p "purple-sys-flared")) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bwd5v799sax71apng7qsnr2ani6qryh7g4hmk6l9naqgvyrgm35")))

(define-public crate-purple-rs-0.2.0 (c (n "purple-rs") (v "0.2.0") (d (list (d (n "glib") (r "^0") (d #t) (k 0)) (d (n "glib-sys") (r "^0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "purple-sys") (r "^0") (d #t) (k 0) (p "purple-sys-flared")) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "169knlar902y764gqyd4y9ag7ad58slc5i32rxgvrg4rsgb6ll0f")))

