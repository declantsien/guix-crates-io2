(define-module (crates-io pu rp purple-sys) #:use-module (crates-io))

(define-public crate-purple-sys-0.0.2 (c (n "purple-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "~0.21") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 1)) (d (n "pkg-config") (r "~0.3") (d #t) (k 1)))) (h "1g7k1iichmgd8xh0zgnkssg6fxd1hbhsxl7y4a1capasvp5kj2jl")))

(define-public crate-purple-sys-0.0.3 (c (n "purple-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "~0.21") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 1)) (d (n "pkg-config") (r "~0.3") (d #t) (k 1)))) (h "1fqy82kslyia4r0jf83ay2ipy1w5zywsdg13z62gsbypk128dx3j")))

(define-public crate-purple-sys-0.0.4 (c (n "purple-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "~0.21") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 1)) (d (n "pkg-config") (r "~0.3") (d #t) (k 1)))) (h "191fzcs1n74ij8jj96vjkvwha2crfl7i518jcda3lvyi054ndbqr")))

(define-public crate-purple-sys-0.1.0 (c (n "purple-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "~0.29") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 1)) (d (n "pkg-config") (r "~0.3") (d #t) (k 1)))) (h "0k3r0iws76ikhvywbr0wv1xn0kbhmmxzq565whhif5k6mxacgcab")))

