(define-module (crates-io pu rp purpur) #:use-module (crates-io))

(define-public crate-purpur-0.1.0 (c (n "purpur") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "image") (r "^0.24") (f (quote ("jpeg" "png"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1ncl6fnyc5937z36dlhqkb9py4rr02zk681in6dyl9yif8zymz8i")))

