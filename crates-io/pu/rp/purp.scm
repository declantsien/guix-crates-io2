(define-module (crates-io pu rp purp) #:use-module (crates-io))

(define-public crate-purp-0.1.0 (c (n "purp") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)))) (h "1fh0byarisk6kdfjagi5jrxds0lmn68mnp99pa1c1zw36mhi7i23") (y #t)))

(define-public crate-purp-0.2.0 (c (n "purp") (v "0.2.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)))) (h "0vzk832x9j3vr1srnj88k0m2v14nc5v43bj8wcvics7f78fc9vil")))

