(define-module (crates-io pu rp purpurea) #:use-module (crates-io))

(define-public crate-purpurea-0.1.0 (c (n "purpurea") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.68") (f (quote ("full"))) (d #t) (k 0)))) (h "0kxgyn38c7w514l37a1avqk0111j6dxsn80kz00jvnm0gm3mhdym")))

