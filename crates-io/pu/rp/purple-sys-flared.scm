(define-module (crates-io pu rp purple-sys-flared) #:use-module (crates-io))

(define-public crate-purple-sys-flared-0.0.6 (c (n "purple-sys-flared") (v "0.0.6") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "glib-sys") (r "^0") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 1)) (d (n "pkg-config") (r "^0") (d #t) (k 1)))) (h "0l6a0iwy2vfdmx9kjfm3f9bwz4l5z6ig5xwipmgv805vhyr95p90") (l "purple")))

(define-public crate-purple-sys-flared-0.0.7 (c (n "purple-sys-flared") (v "0.0.7") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "glib-sys") (r "^0") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 1)) (d (n "pkg-config") (r "^0") (d #t) (k 1)))) (h "148b449ras10vvvrn2jh4x0kq3kvnlx8ck4i4crw21hndlb07wq8") (l "purple")))

