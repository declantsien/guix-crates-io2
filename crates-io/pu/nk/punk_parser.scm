(define-module (crates-io pu nk punk_parser) #:use-module (crates-io))

(define-public crate-punk_parser-0.0.0 (c (n "punk_parser") (v "0.0.0") (d (list (d (n "lalrpop") (r "^0.17.2") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "01jpsx76rjhs4qsb3jv3ph3isbncmj449dxmajiyyr1j4wmd9nrm")))

