(define-module (crates-io pu nk punk-pallet) #:use-module (crates-io))

(define-public crate-punk-pallet-2.0.0 (c (n "punk-pallet") (v "2.0.0") (d (list (d (n "codec") (r "^1.3.4") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "frame-support") (r "^2.0.0") (k 0)) (d (n "frame-system") (r "^2.0.0") (k 0)) (d (n "sp-core") (r "^2.0.0") (k 2)) (d (n "sp-io") (r "^2.0.0") (k 2)) (d (n "sp-runtime") (r "^2.0.0") (k 2)))) (h "0jnl6a5aafm3lrmmdd2vp934i94ac3lgv8lh0q5llmkvd5zmbrz4") (f (quote (("std" "codec/std" "frame-support/std" "frame-system/std") ("default" "std"))))))

