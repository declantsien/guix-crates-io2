(define-module (crates-io pu nk punkt) #:use-module (crates-io))

(define-public crate-punkt-0.0.3 (c (n "punkt") (v "0.0.3") (d (list (d (n "log") (r "^0.1.10") (d #t) (k 0)) (d (n "phf") (r "^0.5.0") (d #t) (k 0)) (d (n "phf_mac") (r "^0.5.0") (d #t) (k 0)) (d (n "rust-freqdist") (r "^0.0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2.9") (d #t) (k 0)) (d (n "xxhash") (r "^0.0.8") (d #t) (k 0)))) (h "0g41xmi19a180zbsv3z50wzlzpl3lrkn09hm8l6zbfnjyp7jadx7")))

(define-public crate-punkt-0.1.1 (c (n "punkt") (v "0.1.1") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7") (d #t) (k 0)) (d (n "phf_shared") (r "^0.7") (d #t) (k 0)) (d (n "rust-freqdist") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.12") (d #t) (k 0)))) (h "0a3algvpgdygsmvp16c2rbkjbfcsafjh4mwpz5a78s18fnpb1107")))

(define-public crate-punkt-0.1.2 (c (n "punkt") (v "0.1.2") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7") (d #t) (k 0)) (d (n "phf_shared") (r "^0.7") (d #t) (k 0)) (d (n "rust-freqdist") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.12") (d #t) (k 0)))) (h "17f684bjy7d9rdyh93fxb5gmmq4p9x9nid177c6504vgsw3qbzd2")))

(define-public crate-punkt-1.0.0 (c (n "punkt") (v "1.0.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7") (d #t) (k 0)) (d (n "rust-freqdist") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "026hmhgmzsivn6a4gxf41yqgx8nzvjn0ddq5b958dckpfyz3ixw4")))

(define-public crate-punkt-1.0.2 (c (n "punkt") (v "1.0.2") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7") (d #t) (k 0)) (d (n "rust-freqdist") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 2)))) (h "0a5r0mcdgyskcpsl6kyy0ikhh3mjh4q0fz3g9mfgzxzhm9v0xwli")))

(define-public crate-punkt-1.0.3 (c (n "punkt") (v "1.0.3") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7") (d #t) (k 0)) (d (n "rust-freqdist") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 2)))) (h "0gx1qd8pbk0ahb7smfnlw4h3xy9f5dwki9wfs24wrcl4rh604b8x")))

(define-public crate-punkt-1.0.4 (c (n "punkt") (v "1.0.4") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7") (d #t) (k 0)) (d (n "rust-freqdist") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 2)))) (h "1zb89z09i8z83gj9hx8bbw5fp3gngxaw0x22wh1jlhv3ppvbyh67")))

(define-public crate-punkt-1.0.5 (c (n "punkt") (v "1.0.5") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "phf") (r "^0.7") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rust-freqdist") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 2)))) (h "1sipp43vn51ch5cmsfk05paa7c9jmjw26wcsr22fpnwhc8ildxsl")))

