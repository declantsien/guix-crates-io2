(define-module (crates-io pu bm pubmed) #:use-module (crates-io))

(define-public crate-pubmed-0.1.0 (c (n "pubmed") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.11") (d #t) (k 0)) (d (n "roxmltree") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1a3k0a5in5ivzj2q8f4377rsd4g6f615862w9hjanbirjs50v90m")))

(define-public crate-pubmed-0.1.1 (c (n "pubmed") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.11") (d #t) (k 0)) (d (n "roxmltree") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "01gqp0rh88yjr85q5ra4f7kwrigyjfjjf1fwnfsm3508vmvdas1a")))

(define-public crate-pubmed-0.1.2 (c (n "pubmed") (v "0.1.2") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.11") (d #t) (k 0)) (d (n "roxmltree") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0y2injpaf947s4is2c9qxpfip0kqlskfn9p2xaki7q6jrrdfmwfy")))

