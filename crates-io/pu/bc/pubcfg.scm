(define-module (crates-io pu bc pubcfg) #:use-module (crates-io))

(define-public crate-pubcfg-0.0.0 (c (n "pubcfg") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.35") (f (quote ("full"))) (d #t) (k 0)))) (h "01bfywx3fqk5mi7sxfql9vhqvw3dzgynnk10l7p25ki9isyj2qdy")))

