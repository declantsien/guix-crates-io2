(define-module (crates-io pu bc pubchem) #:use-module (crates-io))

(define-public crate-pubchem-0.1.0 (c (n "pubchem") (v "0.1.0") (d (list (d (n "form_urlencoded") (r "^1.0.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("tls"))) (k 0)))) (h "0gvzydl62197glrs3l5dypdxwyh4jq812q2l2iq5vz0ihfl47vjp")))

(define-public crate-pubchem-0.1.1 (c (n "pubchem") (v "0.1.1") (d (list (d (n "form_urlencoded") (r "^1.0.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("tls"))) (k 0)))) (h "0rljvsp2nd9spc3xl7gzn3cxjkx47379gl4f782gvhjcp4sg9f66")))

