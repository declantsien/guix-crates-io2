(define-module (crates-io pu rg purge) #:use-module (crates-io))

(define-public crate-purge-0.0.0 (c (n "purge") (v "0.0.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0gsjdrxyjrxqw6za8qagifnsfhpg8lwnq3zm9ssk5aqi5xz5cpdf")))

