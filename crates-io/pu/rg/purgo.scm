(define-module (crates-io pu rg purgo) #:use-module (crates-io))

(define-public crate-purgo-0.1.0 (c (n "purgo") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "inquire") (r "^0.7.5") (d #t) (k 0)) (d (n "size") (r "^0.4.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "00h82lp29rwqr12rw7vg504qvi9gfaalch5rk718wkn18njkzgqp")))

