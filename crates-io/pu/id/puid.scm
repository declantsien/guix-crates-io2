(define-module (crates-io pu id puid) #:use-module (crates-io))

(define-public crate-puid-0.1.0 (c (n "puid") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1swl7v7qha1ivphmx449xf3rmqw1bnrq9qj2jsrzqw0fkc0pgmf2")))

(define-public crate-puid-0.2.0 (c (n "puid") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1rwvnnqxwkfvfryankijmbs5wy1wfrkdxxgbii4yd7077mid61cf")))

