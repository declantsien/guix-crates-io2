(define-module (crates-io pu bn pubnub-util) #:use-module (crates-io))

(define-public crate-pubnub-util-0.1.0 (c (n "pubnub-util") (v "0.1.0") (d (list (d (n "percent-encoding") (r "^2.1") (o #t) (d #t) (k 0)))) (h "1j0zpjq0s51ydfqsjg2r0phj98faqsqz3wwyib64vz3jp405mrav") (f (quote (("encoded-channels-list" "percent-encoding") ("default" "encoded-channels-list"))))))

