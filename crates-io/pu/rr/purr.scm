(define-module (crates-io pu rr purr) #:use-module (crates-io))

(define-public crate-purr-0.1.0 (c (n "purr") (v "0.1.0") (h "0z8285ld8dzpdmfyp970s0546i7zzqrjpk2jdqgxjmwrmq85pydb")))

(define-public crate-purr-0.1.1 (c (n "purr") (v "0.1.1") (h "0si0lzv5pwxpymgxmylsgmgc6b2g4q83753d1v0izj84wk12wh9f")))

(define-public crate-purr-0.2.0 (c (n "purr") (v "0.2.0") (h "1x4xjgrh8c549d633mxb3dmb3n0zi44s7ag7z79ggaghr0lmfpv1")))

(define-public crate-purr-0.3.0 (c (n "purr") (v "0.3.0") (h "1732g5x7vwx3h6l1fdp9xwm4lg74bl27k7wn1z1y0k4phxrk4v39")))

(define-public crate-purr-0.4.0 (c (n "purr") (v "0.4.0") (h "01pxbkrzli9fcw36zycpinq9ib6hxk7idk2ic7sjzbnnj3605c9p")))

(define-public crate-purr-0.5.0 (c (n "purr") (v "0.5.0") (h "06nahi7jaj1s2gjarl73diffpajqigdc6svv5vx8c0d7772x3p85")))

(define-public crate-purr-0.6.0 (c (n "purr") (v "0.6.0") (h "0f8z3hhb15c9z66g5cfxd77pbsfsnwx06xpb9aadfysrc93as0m3")))

(define-public crate-purr-0.6.1 (c (n "purr") (v "0.6.1") (h "19bh27z6nwcwnxdc5iscjmqyik7sqcprrfw3mjja0gnf58p55yi8")))

(define-public crate-purr-0.6.2 (c (n "purr") (v "0.6.2") (h "0ii883fvc8xxl9qmzl2cm1xf9hrzzdqcpbqgcnsc4237c5aal2ia")))

(define-public crate-purr-0.6.3 (c (n "purr") (v "0.6.3") (h "0lg44nzjaklpvvdqayfrcnlhbsgmp11cw9xg0h1f1nv5q91y6nsz")))

(define-public crate-purr-0.7.0 (c (n "purr") (v "0.7.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "1ka4n29kk697p21snc3cc6blqywhd4gslsla9m3xymrqc8krjfdz")))

(define-public crate-purr-0.8.0 (c (n "purr") (v "0.8.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "1m89akx3kqcsa89y6364j1yiggwv72v2cp1jng8k9ggbgipqhznl")))

(define-public crate-purr-0.9.0 (c (n "purr") (v "0.9.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "12hh4264rfic7mm9zlsaqvglj79y6xjx1g0mly8m3js1cn7wyppn")))

