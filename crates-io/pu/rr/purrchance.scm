(define-module (crates-io pu rr purrchance) #:use-module (crates-io))

(define-public crate-purrchance-0.1.0 (c (n "purrchance") (v "0.1.0") (d (list (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "05xp3zwxq1ljpacn986k0xx6fv88g7pc6cdbizcwmgn8yvcx8w2q")))

(define-public crate-purrchance-0.2.0 (c (n "purrchance") (v "0.2.0") (d (list (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "089kgclcivd9x9m3ymj7vl9f36wkjispwb26ml3k8kwgyl2qzs69")))

(define-public crate-purrchance-0.3.0 (c (n "purrchance") (v "0.3.0") (d (list (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0q8f1629ycq7k46bxhv412l7mfi8l8xz5vpsx7ggk14gv1lvbxc0")))

(define-public crate-purrchance-0.4.0 (c (n "purrchance") (v "0.4.0") (d (list (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "06z37mx4sddjhjqhff97figajzi4v37vbdl4ddc77h71rxiajp46")))

(define-public crate-purrchance-0.4.1 (c (n "purrchance") (v "0.4.1") (d (list (d (n "nom") (r "^5.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "12w8936jgdc82kcmr3l5yxb1fg7zlxmg61pinr3ja9s6w5g4wc8v")))

