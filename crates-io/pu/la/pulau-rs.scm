(define-module (crates-io pu la pulau-rs) #:use-module (crates-io))

(define-public crate-pulau-rs-0.1.0 (c (n "pulau-rs") (v "0.1.0") (h "1y5j5mxg53fvbddnr6lab7ic4f6k97rn2xqb3vk3xy2rvjh406k8")))

(define-public crate-pulau-rs-0.2.0 (c (n "pulau-rs") (v "0.2.0") (d (list (d (n "heapless") (r "^0.7.16") (d #t) (k 2)))) (h "0rzd3ybiv422d81py8xa53x7xbbxnl5q86kz3w0fayjicmlazw2f") (f (quote (("traits") ("default" "traits"))))))

