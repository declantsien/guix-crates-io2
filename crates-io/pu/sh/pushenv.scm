(define-module (crates-io pu sh pushenv) #:use-module (crates-io))

(define-public crate-pushenv-0.1.0 (c (n "pushenv") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "0bxxq5h0cx6dvr2m1hz7mlajmjw73hxr1m0yw3ka54m3s42zzbcb") (y #t)))

(define-public crate-pushenv-0.1.1 (c (n "pushenv") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "0xjf8g2plhw0ncbha2crpf3wh0phqk07pl5j6zsfpkr1i4g9bhf9") (y #t)))

(define-public crate-pushenv-1.0.0 (c (n "pushenv") (v "1.0.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "18j0506sdjncigmwwj6px1cysg1vssg9xnzwmmmmv4awgb3rb9ic") (y #t)))

(define-public crate-pushenv-1.1.0 (c (n "pushenv") (v "1.1.0") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)) (d (n "rstest") (r "^0.19.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "0hpvwdc11y9zdyx1cmpdz807r85bsrsw8c5yzy6c9knb672lnxc9") (y #t)))

(define-public crate-pushenv-1.1.1 (c (n "pushenv") (v "1.1.1") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)) (d (n "rstest") (r "^0.19.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "1g6n8lzhj2f27m8pcqfcpxank2pn8j0mzl1538xlr9q7gjlbkipq")))

(define-public crate-pushenv-1.1.2 (c (n "pushenv") (v "1.1.2") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)) (d (n "rstest") (r "^0.19.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "18hzxxa0a5pgq6lgr6zw9fcprmgpd8vdj6nad9zr431s9i44x4hr")))

