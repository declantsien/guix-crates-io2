(define-module (crates-io pu sh pushrod) #:use-module (crates-io))

(define-public crate-pushrod-0.1.0 (c (n "pushrod") (v "0.1.0") (d (list (d (n "pushrod-widgets") (r "^0") (d #t) (k 0)) (d (n "sdl2") (r "^0.34") (f (quote ("ttf" "image" "unsafe_textures"))) (d #t) (k 0)))) (h "19dzghqwk1d9q91fqv64jw2ygs31adap5m30mssdnhj49gdawajb") (y #t)))

(define-public crate-pushrod-0.1.1 (c (n "pushrod") (v "0.1.1") (d (list (d (n "pushrod-widgets") (r "^0") (d #t) (k 0)) (d (n "sdl2") (r "^0.34") (f (quote ("ttf" "image" "unsafe_textures"))) (d #t) (k 0)))) (h "1r68plmn6iyhadfvrvni7xw3d6p2n2hk0zpi11syk0f692q3mmzw") (y #t)))

(define-public crate-pushrod-0.1.2 (c (n "pushrod") (v "0.1.2") (d (list (d (n "pushrod-widgets") (r "^0") (d #t) (k 0)) (d (n "sdl2") (r "^0.34") (f (quote ("ttf" "image" "unsafe_textures"))) (d #t) (k 0)))) (h "17la7372k2vdrd6rdr5jkwg6sh0v86x7sccan300jgz8m9yw5qkc") (y #t)))

(define-public crate-pushrod-0.1.3 (c (n "pushrod") (v "0.1.3") (d (list (d (n "pushrod-widgets") (r "^0") (d #t) (k 0)) (d (n "sdl2") (r "^0.34") (f (quote ("ttf" "image" "unsafe_textures"))) (d #t) (k 0)))) (h "0d49abdhcicp1n99b8h8rf1ww14yg564ii4nprw11a5f4cc6ipha") (y #t)))

