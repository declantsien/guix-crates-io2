(define-module (crates-io pu sh pushb) #:use-module (crates-io))

(define-public crate-pushb-0.1.0 (c (n "pushb") (v "0.1.0") (d (list (d (n "dirs") (r "^1.0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "1hw0g7i022d71xlsc16kbpgdh8wfdbd3nkzbpqqdd38a0dkl4mm8")))

(define-public crate-pushb-0.1.1 (c (n "pushb") (v "0.1.1") (d (list (d (n "dirs") (r "^1.0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "0366wx0r3bxhhbnwszwp0dlnxscswcfqm9gglfx1pkj84nmnw64x")))

(define-public crate-pushb-0.1.2 (c (n "pushb") (v "0.1.2") (d (list (d (n "dirs") (r "^1.0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "0vbcfdh6nixcr6bl4ff3iybbpxkjfc07f2xmh6zdczswpcl44yaj")))

(define-public crate-pushb-0.1.3 (c (n "pushb") (v "0.1.3") (d (list (d (n "dirs") (r "^1.0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "0sg846fsy9hij5lqrzzbpdqggzs09n4ghhga6z5ab9jzimg0ymli")))

(define-public crate-pushb-0.2.0 (c (n "pushb") (v "0.2.0") (d (list (d (n "dirs") (r "^1.0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "1k31q677hs8kgd5z46f8xx1yqpa3l9nf1kj6dx82j1z3j3ani0vk")))

(define-public crate-pushb-0.2.1 (c (n "pushb") (v "0.2.1") (d (list (d (n "dirs") (r "^1.0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0yhv3336fxgh0qak9wrfxciiadb434ralckgz307llj0mqnq8851")))

(define-public crate-pushb-0.2.2 (c (n "pushb") (v "0.2.2") (d (list (d (n "dirs") (r "^1.0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "0k5qqy8v7s51wap0l4b1v3bkqdifhz2qrdb7axaahjmb82sri9g3")))

(define-public crate-pushb-0.2.3 (c (n "pushb") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.61") (d #t) (k 0)) (d (n "dirs") (r "^1.0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)))) (h "1nd9bsjmf3p5wxsv9wlm66ry6zh5j8cd3r084swfnj3h6l1przg2")))

