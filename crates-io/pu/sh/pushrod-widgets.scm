(define-module (crates-io pu sh pushrod-widgets) #:use-module (crates-io))

(define-public crate-pushrod-widgets-0.1.0 (c (n "pushrod-widgets") (v "0.1.0") (d (list (d (n "sdl2") (r "^0.34") (f (quote ("ttf" "image" "unsafe_textures"))) (d #t) (k 0)))) (h "1m4z5jfc4f89ys48cmnya0kywv78jf9v40rvz44627aql6b7b01g") (y #t)))

(define-public crate-pushrod-widgets-0.1.1 (c (n "pushrod-widgets") (v "0.1.1") (d (list (d (n "sdl2") (r "^0.34") (f (quote ("ttf" "image" "unsafe_textures"))) (d #t) (k 0)))) (h "1i2x4303pzz52x51lkh52h3j7hfp9v54zzgfgflnr2bxzrl533ka") (y #t)))

(define-public crate-pushrod-widgets-0.1.2 (c (n "pushrod-widgets") (v "0.1.2") (d (list (d (n "sdl2") (r "^0.34") (f (quote ("ttf" "image" "unsafe_textures"))) (d #t) (k 0)))) (h "1w92iv8rbsy71kykcbwsxz45a8ykqa7r0xhpcnpjydryc9gdfx16") (y #t)))

(define-public crate-pushrod-widgets-0.1.3 (c (n "pushrod-widgets") (v "0.1.3") (d (list (d (n "sdl2") (r "^0.34") (f (quote ("ttf" "image" "unsafe_textures"))) (d #t) (k 0)))) (h "0cfyl1df1lsj1y974znmx2av8x29744hndvr45w41v0s82a9925b") (y #t)))

