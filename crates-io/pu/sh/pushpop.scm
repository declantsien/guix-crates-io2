(define-module (crates-io pu sh pushpop) #:use-module (crates-io))

(define-public crate-pushpop-0.1.0 (c (n "pushpop") (v "0.1.0") (d (list (d (n "mail-test-account") (r "^0.1.1") (d #t) (k 2)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)) (d (n "rustls") (r "^0.15") (d #t) (k 0)) (d (n "untrusted") (r "^0.6.2") (d #t) (k 2)) (d (n "webpki") (r "^0.19") (d #t) (k 2)) (d (n "webpki-roots") (r "^0.16") (d #t) (k 2)))) (h "14yx0lgcyh7k424dr6ahnvk20g3nbsa32bxy87d5b20h29pz7qsg")))

