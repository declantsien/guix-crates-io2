(define-module (crates-io pu sh push2_display) #:use-module (crates-io))

(define-public crate-push2_display-0.1.0 (c (n "push2_display") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.6.2") (d #t) (k 0)) (d (n "rusb") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1xvi0ds55qb610zc6fjfljrqg7ghmavxx83pkxzbyw1g98d0wl6f")))

(define-public crate-push2_display-0.1.1 (c (n "push2_display") (v "0.1.1") (d (list (d (n "embedded-graphics") (r "^0.6.2") (d #t) (k 0)) (d (n "rusb") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0grjk92wld7ap03wq808yc08j52745x3hj05xblspb3hhhsw9ark")))

(define-public crate-push2_display-1.0.0-alpha.1 (c (n "push2_display") (v "1.0.0-alpha.1") (d (list (d (n "embedded-graphics") (r "^0.7.0-alpha.2") (d #t) (k 2)) (d (n "embedded-graphics-core") (r "^0.1.1") (d #t) (k 0)) (d (n "rusb") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "14d7pxlf44gvnmdnp8kzq5w2gslnn1v06cbr3fq3xjpdz4rim57k") (y #t)))

(define-public crate-push2_display-1.0.0-alpha.2 (c (n "push2_display") (v "1.0.0-alpha.2") (d (list (d (n "embedded-graphics") (r "^0.7.0-alpha.3") (d #t) (k 2)) (d (n "embedded-graphics-core") (r "^0.2.0") (d #t) (k 0)) (d (n "rusb") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "17qkg8jx7jmqfjyis8kgd6cj4l1xg2inhqmmby9lpcm7bxs547vx") (y #t)))

(define-public crate-push2_display-1.0.0-alpha.3 (c (n "push2_display") (v "1.0.0-alpha.3") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 2)) (d (n "embedded-graphics-core") (r "^0.3.3") (d #t) (k 0)) (d (n "rusb") (r "^0.9.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0ggynxd6mbmwj1bflpyq2izbbzmf6nbxz5j7b4wfjls06vlg3mj9") (y #t)))

(define-public crate-push2_display-0.2.0 (c (n "push2_display") (v "0.2.0") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 2)) (d (n "embedded-graphics-core") (r "^0.3.3") (d #t) (k 0)) (d (n "rusb") (r "^0.9.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0z1kjpggdq8xrq0k7vwz5s2zwi43fbvk8b9p4fw05mcrsd3a1mii")))

