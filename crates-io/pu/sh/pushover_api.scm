(define-module (crates-io pu sh pushover_api) #:use-module (crates-io))

(define-public crate-pushover_api-0.1.0 (c (n "pushover_api") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "0qq2s0k43zpkinbsi0xbcmqc24ap7a2313rwj76310prrngyrq89") (y #t)))

