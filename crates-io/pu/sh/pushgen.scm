(define-module (crates-io pu sh pushgen) #:use-module (crates-io))

(define-public crate-pushgen-0.0.1 (c (n "pushgen") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0xill4xyn3yh81m0i35vz36brcwck8h40bsismgy19dlhjybn1ad")))

(define-public crate-pushgen-0.0.2 (c (n "pushgen") (v "0.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "either") (r "^1.0") (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 2)))) (h "1xrjbmr2xi21x888girlj10xzdhhhnm8ksk3f0rl2mzm140i19pl") (f (quote (("test" "std") ("std") ("default" "std"))))))

