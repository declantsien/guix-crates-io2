(define-module (crates-io pu sh pushd) #:use-module (crates-io))

(define-public crate-pushd-0.0.1 (c (n "pushd") (v "0.0.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serial_test") (r "^2.0.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "143km7r5xy0p27al7fpdh9ncq9yz6k6q0zbcj61nkwc6xn4jwral")))

