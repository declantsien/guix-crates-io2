(define-module (crates-io pu sh push2_pong) #:use-module (crates-io))

(define-public crate-push2_pong-0.1.0 (c (n "push2_pong") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.7.0-alpha.2") (d #t) (k 0)) (d (n "gameloop") (r "^0.2.0") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 0)) (d (n "push2_display") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0mxwiazqix5h4bklf2cpbiji8z7gc0syxmppjmhywsdvjrjaccq9")))

(define-public crate-push2_pong-0.1.1 (c (n "push2_pong") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.7.0-alpha.2") (d #t) (k 0)) (d (n "gameloop") (r "^0.2.0") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 0)) (d (n "push2_display") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "126jfjj8s1f6g6w5l9j3ic4z02razfnb7s0isj1jlr2lw6swds9s")))

(define-public crate-push2_pong-0.2.0 (c (n "push2_pong") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.7.0-alpha.3") (d #t) (k 0)) (d (n "gameloop") (r "^0.2.0") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 0)) (d (n "push2_display") (r "^1.0.0-alpha.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "02vd23smrv2qz7gvqjw4hxhnipki5yh6aql5c5wmrr6xwng1d7if")))

(define-public crate-push2_pong-0.2.1 (c (n "push2_pong") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "gameloop") (r "^0.2.0") (d #t) (k 0)) (d (n "midir") (r "^0.7.0") (d #t) (k 0)) (d (n "push2_display") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1rf208pc3sfi7g8ia7k57d460ncdqv00mcpip80zk0r5gbw8wymm")))

