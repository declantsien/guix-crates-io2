(define-module (crates-io pu sh pushr) #:use-module (crates-io))

(define-public crate-pushr-0.1.0 (c (n "pushr") (v "0.1.0") (d (list (d (n "names") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "1ajlr3r05zk9vziwr8zs4fnza9ixixh75vmpq4w0m5pbd5ahp3kk")))

(define-public crate-pushr-0.1.1 (c (n "pushr") (v "0.1.1") (d (list (d (n "names") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "1br19fv7dikd26gi4m8lxbb33689hsyad3flmj47x4msgys276jm")))

(define-public crate-pushr-0.1.2 (c (n "pushr") (v "0.1.2") (d (list (d (n "names") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "12n0s01sn448h1xzwb7lj9f1cn86xm5npy4yxjpgp81w2nc0ni3h")))

(define-public crate-pushr-0.1.3 (c (n "pushr") (v "0.1.3") (d (list (d (n "names") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "10rcwpyqkn0dczbhmxamrxh152m0y7xmsxp80lf5hwgnsm8jyb40")))

(define-public crate-pushr-0.1.4 (c (n "pushr") (v "0.1.4") (d (list (d (n "names") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "0s0972h7pc6zlghvpqasq1dy2s3d0yzir5p4l847wzwzfqnir6cg")))

(define-public crate-pushr-0.1.5 (c (n "pushr") (v "0.1.5") (d (list (d (n "names") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "0aggbfz8sdyqfphh2wrn44shwv5pwsx8nshcxcr3pixnw4d56fg0")))

(define-public crate-pushr-0.1.6 (c (n "pushr") (v "0.1.6") (d (list (d (n "names") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "0li2n8mng4xiff86ynk7zkn2pmmn06kb79b8g4gfsjf3ikxrdgh4")))

(define-public crate-pushr-0.1.7 (c (n "pushr") (v "0.1.7") (d (list (d (n "names") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "0r619p1ij3mdqsmf1b44vm1wf3rjyns7asqnx89v7r4gkfv9wgbq")))

(define-public crate-pushr-0.1.8 (c (n "pushr") (v "0.1.8") (d (list (d (n "names") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "1vd234jzqj7c9layd5ajg12ka4rvclg5xbispw7rbl9z8rxcav53")))

(define-public crate-pushr-0.1.9 (c (n "pushr") (v "0.1.9") (d (list (d (n "names") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "1y3ira502gdk1lcwnjxmpsy0i9h88k45ah1jqzbcaslni7mfnjdn")))

(define-public crate-pushr-0.1.10 (c (n "pushr") (v "0.1.10") (d (list (d (n "names") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "0wwgzi0kcd6dhvxa9hibz8l5f8fjaj4jlv2ib09m2ya5r7xrfnfq")))

(define-public crate-pushr-0.1.11 (c (n "pushr") (v "0.1.11") (d (list (d (n "names") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "0c04dxbxhfkmx3za70qw1nx5gysz65psh8p0v8ag7xnlcc00p348")))

(define-public crate-pushr-0.2.0 (c (n "pushr") (v "0.2.0") (d (list (d (n "names") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "03pdk4vz4wfmvmxp9wjbpxzp1243psslshrfnbc4zha6yvazgrfi")))

(define-public crate-pushr-0.2.1 (c (n "pushr") (v "0.2.1") (d (list (d (n "names") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "0jf34r6gzbsgxnlsmf8wf75v1lrkm4dgl64ni3vi8jv7q8mkysdp")))

(define-public crate-pushr-0.2.2 (c (n "pushr") (v "0.2.2") (d (list (d (n "names") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "0pz8diqily7irh24482jk7ni29zy6l7rrlkp2fvpkkd0lhk0x6v8")))

(define-public crate-pushr-0.3.0 (c (n "pushr") (v "0.3.0") (d (list (d (n "names") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "1i8cmapkqqq5pgshqwg7v3k43c01r0rlpm0j8yqhmgx0vvvc76ii")))

(define-public crate-pushr-0.3.1 (c (n "pushr") (v "0.3.1") (d (list (d (n "names") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "186bb58fhiwz76b0113d68ni7iy1rzi72s5i9ajhwr63w1vdhv8p")))

(define-public crate-pushr-0.3.2 (c (n "pushr") (v "0.3.2") (d (list (d (n "names") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "0llimdsss58vyjfqs3daygkgskxljsxqsnyzl6fgh5hwqxyi3jqi")))

(define-public crate-pushr-0.3.3 (c (n "pushr") (v "0.3.3") (d (list (d (n "names") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "0wz7h3nh2r4j1q9c1sfh7ypaj5j3bjdyz0g1v4gj7msgg8dyx9mc")))

(define-public crate-pushr-0.3.4 (c (n "pushr") (v "0.3.4") (d (list (d (n "names") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "0jazjkzdxygpl320c1vw1lcq32aavmi0q4375q99x98skl8h1lrr")))

(define-public crate-pushr-0.3.5 (c (n "pushr") (v "0.3.5") (d (list (d (n "names") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "00ma4zblzjil4rqmqaw899vpk0cxrknkdds6j2p15b6gp4fq08n4")))

(define-public crate-pushr-0.3.6 (c (n "pushr") (v "0.3.6") (d (list (d (n "names") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "0iga9n01h6apk4jfh7gyxx9byq5q8g8g1lhh2h33gqn6b24wrzb7")))

(define-public crate-pushr-0.3.7 (c (n "pushr") (v "0.3.7") (d (list (d (n "names") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "1l94z5abz3bmy7qcahib9z7a8v7992l56agc32b8rm077z6scf73")))

(define-public crate-pushr-0.4.0 (c (n "pushr") (v "0.4.0") (d (list (d (n "names") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "1z4ipbw5dlg58c423caj4dvvp4cyq2z93pk3i44317gqy7v35ds9")))

(define-public crate-pushr-0.4.1 (c (n "pushr") (v "0.4.1") (d (list (d (n "names") (r "^0.10.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.1") (d #t) (k 0)))) (h "0abvgd7fm5l4rjksai97a9hfzw07plscrmh3544ydiwgac37p42x")))

