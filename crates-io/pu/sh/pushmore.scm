(define-module (crates-io pu sh pushmore) #:use-module (crates-io))

(define-public crate-pushmore-0.1.0 (c (n "pushmore") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "0h299ps06q5xrbpamm2198clsh4sjbqhvk764awiyinf988y9gbn")))

(define-public crate-pushmore-0.1.1 (c (n "pushmore") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "0jziwzcfzzdrh7s1fgkysjjlah6mh82qfhq836pgd64iwp7hj92r")))

(define-public crate-pushmore-0.1.2 (c (n "pushmore") (v "0.1.2") (d (list (d (n "ureq") (r "^2.0") (d #t) (k 0)))) (h "0hj82r5s7a3yqykswsrvx40lrb80315gx22g3c7kfk0lkxmgdz1m")))

