(define-module (crates-io pu sh pusherator) #:use-module (crates-io))

(define-public crate-pusherator-0.0.0 (c (n "pusherator") (v "0.0.0") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "variadics") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "06232v21bli7qid8x474ic5fyfkp926k5wv2pdfym1g0kcsy342d") (f (quote (("default" "demux")))) (s 2) (e (quote (("demux" "dep:variadics"))))))

(define-public crate-pusherator-0.0.1 (c (n "pusherator") (v "0.0.1") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "variadics") (r "^0.0.2") (o #t) (d #t) (k 0)))) (h "10b2x282fawyb2dzpgwnrxv9v21wyqv6fr0mwn8w35dmyywxiyh6") (f (quote (("default" "demux")))) (s 2) (e (quote (("demux" "dep:variadics"))))))

(define-public crate-pusherator-0.0.2 (c (n "pusherator") (v "0.0.2") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "variadics") (r "^0.0.2") (o #t) (d #t) (k 0)))) (h "01ydmbbfgs36xrf64fcwjzz7js4q6hrkgw8rwmikvm9p5484hrv2") (f (quote (("default" "demux")))) (s 2) (e (quote (("demux" "dep:variadics"))))))

(define-public crate-pusherator-0.0.3 (c (n "pusherator") (v "0.0.3") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "variadics") (r "^0.0.2") (o #t) (d #t) (k 0)))) (h "1vqjqcyr5wr23687kk394ndjkbhpi7jbmbix3nlqy39y2nsnqj5x") (f (quote (("default" "demux")))) (s 2) (e (quote (("demux" "dep:variadics"))))))

(define-public crate-pusherator-0.0.4 (c (n "pusherator") (v "0.0.4") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "variadics") (r "^0.0.3") (o #t) (d #t) (k 0)))) (h "1r94lmqn1kg0f62iv138q8d0vd8yld35jj2d9hzwpgxvhsy0h0ix") (f (quote (("default" "demux")))) (s 2) (e (quote (("demux" "dep:variadics"))))))

(define-public crate-pusherator-0.0.5 (c (n "pusherator") (v "0.0.5") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "variadics") (r "^0.0.4") (o #t) (d #t) (k 0)))) (h "10nq1v6miqzqm5q473ckps50jmilwfcrxs46cga10i3s3bmg471c") (f (quote (("default" "demux")))) (s 2) (e (quote (("demux" "dep:variadics"))))))

(define-public crate-pusherator-0.0.6 (c (n "pusherator") (v "0.0.6") (d (list (d (n "either") (r "^1.8.0") (d #t) (k 0)) (d (n "variadics") (r "^0.0.4") (o #t) (d #t) (k 0)))) (h "1a44f31i6y734vv0m91h9hamgc0iaph0raczslw166dsldm78bbc") (f (quote (("default" "demux")))) (s 2) (e (quote (("demux" "dep:variadics"))))))

