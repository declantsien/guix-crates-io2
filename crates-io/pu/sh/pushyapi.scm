(define-module (crates-io pu sh pushyapi) #:use-module (crates-io))

(define-public crate-pushyapi-0.1.0 (c (n "pushyapi") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)))) (h "02q530mi3naans3ckxr1c9lx000b6gyn3fzjb7c2gfh9mpy3psmy") (y #t)))

(define-public crate-pushyapi-0.1.1 (c (n "pushyapi") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)))) (h "0h24d1gykhx1am3n9dai9ksgcnw286l9kk682r1fqnmqj0ag00vh") (y #t)))

(define-public crate-pushyapi-0.1.2 (c (n "pushyapi") (v "0.1.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json" "rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)))) (h "063a5kbabky87dr6zjdh2njchx136sj1z3l5cmpxidf5jbl7xbgc") (y #t)))

(define-public crate-pushyapi-0.1.3 (c (n "pushyapi") (v "0.1.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)))) (h "14gx03zqjkrnqg5cmwd0v0k8pkwxm7394n9rfjg8c56v5zyw8c7l")))

