(define-module (crates-io pu sh push_the_elephant) #:use-module (crates-io))

(define-public crate-push_the_elephant-0.0.1 (c (n "push_the_elephant") (v "0.0.1") (d (list (d (n "derive_builder") (r "^0.7.2") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.1.3") (d #t) (k 0)) (d (n "kafka") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4.7") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qy9ayqkqrvm6g91aqzbximhjgynhw2b6kzpn872b86j776r0h7v")))

(define-public crate-push_the_elephant-0.0.2 (c (n "push_the_elephant") (v "0.0.2") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "derive_builder") (r "^0.7.2") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.1.3") (d #t) (k 0)) (d (n "kafka") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4.7") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "retry") (r "^0.5.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "0z2zjjg3lll7hj2gsg2wgkj9hzjjgdpb16v975lhlmq343in8scm")))

