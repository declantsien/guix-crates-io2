(define-module (crates-io pu sh pushback-iter) #:use-module (crates-io))

(define-public crate-pushback-iter-0.1.0 (c (n "pushback-iter") (v "0.1.0") (h "0iyv4fq2ivv0f9xxys2dyc9dz21jrq5rmbnin5mb84nnskfsz67j")))

(define-public crate-pushback-iter-0.2.0 (c (n "pushback-iter") (v "0.2.0") (h "0rwli3lnjg3kvb25gjravblr8qkm1bvmabz20af5x9f606laqacc")))

