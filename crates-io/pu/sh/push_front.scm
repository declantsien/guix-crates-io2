(define-module (crates-io pu sh push_front) #:use-module (crates-io))

(define-public crate-push_front-0.1.0 (c (n "push_front") (v "0.1.0") (h "1jkp2aq912f03igwgcb2jb90a2m92nf3ac48aw5j1apcbydi6dhx") (y #t)))

(define-public crate-push_front-0.1.1 (c (n "push_front") (v "0.1.1") (h "1iyl2aq796yr18rk17grn46c1fihshkg5aabq87qdg7qv4022385") (y #t)))

