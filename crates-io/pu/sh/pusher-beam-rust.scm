(define-module (crates-io pu sh pusher-beam-rust) #:use-module (crates-io))

(define-public crate-pusher-beam-rust-0.1.0 (c (n "pusher-beam-rust") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "08c4xqfyqhlg05rdr0rha70gzmaadmkrl0xf9b9634rycidk3bmc")))

(define-public crate-pusher-beam-rust-0.1.1 (c (n "pusher-beam-rust") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "1cc4fvrz1ahsxl20glygn4h3qmac5hlrg7by2b82d84skzzxzfbl") (y #t)))

(define-public crate-pusher-beam-rust-0.1.2 (c (n "pusher-beam-rust") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "1090r6vipki01b306g7vsf5lwrd8rxyhy4is6by9bv1lwn304vgs")))

(define-public crate-pusher-beam-rust-0.1.3 (c (n "pusher-beam-rust") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "18wfrcrfhf8ag2861rgbsb5zg0f9k3r61n75pvxrgdvdnz6zvsrb")))

