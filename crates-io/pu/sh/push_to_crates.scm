(define-module (crates-io pu sh push_to_crates) #:use-module (crates-io))

(define-public crate-PUSH_TO_CRATES-0.1.0 (c (n "PUSH_TO_CRATES") (v "0.1.0") (h "0sh89bzh672qyqx2hp9z3v6nsawx1pvazpc9xkdqzwcxhi2g2nic")))

(define-public crate-PUSH_TO_CRATES-0.2.0 (c (n "PUSH_TO_CRATES") (v "0.2.0") (h "1xsgj1qckv09hbgjzzj9b486sizw7j9xjnrpvpp6h3ll5mxrncgp")))

