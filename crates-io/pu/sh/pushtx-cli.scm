(define-module (crates-io pu sh pushtx-cli) #:use-module (crates-io))

(define-public crate-pushtx-cli-0.2.0 (c (n "pushtx-cli") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pushtx") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "0h0pkix53wh7hby3by66vnk58jm18k7cjlssffygdp70fpn31bgf")))

(define-public crate-pushtx-cli-0.2.1 (c (n "pushtx-cli") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pushtx") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "0720rmpwr9my91pmx7qipn2c9asknwql8gwb6wq7bmjxmvxl4636")))

(define-public crate-pushtx-cli-0.2.2 (c (n "pushtx-cli") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pushtx") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "09lckajy6yzmv5gscg82hkllryq2k7g36nji4hzf3ai814l0p7di")))

(define-public crate-pushtx-cli-0.2.3 (c (n "pushtx-cli") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pushtx") (r "^0.2.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "1ml7bjmxjcb74d5bflnd1jd89840lq7nvq25fnym85ldn22ygcsv") (y #t)))

(define-public crate-pushtx-cli-0.2.4 (c (n "pushtx-cli") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pushtx") (r "^0.2.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "0frpqknkh6ms2n9q0c442rh0w71s5q0axyhh2agxddka59q0nrnk")))

(define-public crate-pushtx-cli-0.3.0 (c (n "pushtx-cli") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pushtx") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "0kbwc644i34wj0890sjvrnwvvq8ycpcyxamd5a7g3p1v3q9a0d6p")))

