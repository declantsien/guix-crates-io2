(define-module (crates-io pu sh pushevent) #:use-module (crates-io))

(define-public crate-pushevent-0.1.0 (c (n "pushevent") (v "0.1.0") (d (list (d (n "ws") (r "^0.9.1") (d #t) (k 0)))) (h "1f13zs7cry9crxi1gkishv0v7bqi2n89wwcxh5hmidfpylndbxlx")))

(define-public crate-pushevent-0.1.1 (c (n "pushevent") (v "0.1.1") (d (list (d (n "ws") (r "^0.9.1") (d #t) (k 0)))) (h "0asfia5p1z777hivk6aslnzgfzpsyafhz48w68ixsfb1sxva78md")))

(define-public crate-pushevent-0.1.2 (c (n "pushevent") (v "0.1.2") (d (list (d (n "ws") (r "^0.9.1") (d #t) (k 0)))) (h "14phmvq0nnh7l9358m8xx11ydsbc1l50by8dg60mb5qa4wgh4bff")))

(define-public crate-pushevent-0.1.3 (c (n "pushevent") (v "0.1.3") (d (list (d (n "ws") (r "^0.9.1") (d #t) (k 0)))) (h "0szhz4vr9xlg8vw83z1ab4m15h03wl2nhmdz951cfyvbhl35qgi4")))

