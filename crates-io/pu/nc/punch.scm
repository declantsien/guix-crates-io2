(define-module (crates-io pu nc punch) #:use-module (crates-io))

(define-public crate-punch-0.1.0 (c (n "punch") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.15.0") (d #t) (k 0)))) (h "1azizr03gcibirvwayrid153mh1axh41y1kibfl8wji7lshsyd5h")))

(define-public crate-punch-0.1.1 (c (n "punch") (v "0.1.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.15.0") (d #t) (k 0)))) (h "13a6mpj47nmx8ij1bn77k1gscxnl1x33ywrrqbx45krpf0wzd6nv")))

(define-public crate-punch-0.1.2 (c (n "punch") (v "0.1.2") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.15.0") (d #t) (k 0)))) (h "0y0wvrf60dibrdqi12khgzdi176q5z8wgp43ib0rs1izlm1gqib5")))

