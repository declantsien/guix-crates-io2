(define-module (crates-io pu nc punc) #:use-module (crates-io))

(define-public crate-punc-0.0.1 (c (n "punc") (v "0.0.1") (h "1wh383ymgza3289vh49lbsl06v1lnk337xddlr5wmwdms878zrkr")))

(define-public crate-punc-0.0.2 (c (n "punc") (v "0.0.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "13lzl7c3x15nb0p4c9z4in2khzf6vd9h7823v6cakh2dwr8x4598")))

(define-public crate-punc-0.0.3 (c (n "punc") (v "0.0.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0x6s7yrlz9zdn2287yfsdal7hd69xv4rrpzlx9qhr8jjhj0j7byv")))

(define-public crate-punc-0.0.4 (c (n "punc") (v "0.0.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0l54z3qblxp7b0s35g5d91raxvr6xqlp8faj5l7l2ayiv3qwnfd4")))

(define-public crate-punc-1.0.0 (c (n "punc") (v "1.0.0") (d (list (d (n "im-lists") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0dg7vj4p4p2hii4kzcdvznjkjn9jr6qfmv1g5acs2aiyz5qh932x")))

(define-public crate-punc-1.0.1 (c (n "punc") (v "1.0.1") (d (list (d (n "im-lists") (r "^0.4") (d #t) (k 0)))) (h "0dp6mg4xjm13hkg94g79dh85w27rzd8c721rxh0v9l09qqq450hx")))

(define-public crate-punc-1.0.3 (c (n "punc") (v "1.0.3") (d (list (d (n "im-lists") (r "^0.4") (d #t) (k 0)))) (h "1s7chswilpinm9hqyr4fg9bv6dcirylc32l4q2412zcc52k71rz6")))

(define-public crate-punc-1.0.4 (c (n "punc") (v "1.0.4") (d (list (d (n "im-lists") (r "^0.4") (d #t) (k 0)))) (h "0jjl3hz8jjgpvm3p0a3jh717qqrmxkzgvs11riagq9czkr4bg66c")))

(define-public crate-punc-1.0.5 (c (n "punc") (v "1.0.5") (d (list (d (n "im-lists") (r "^0.4") (d #t) (k 0)))) (h "10y5npyi2pnrjf63w3lid0q68ls67jf6p9wdsqbaq2qa6l6hnssp")))

(define-public crate-punc-1.0.6 (c (n "punc") (v "1.0.6") (d (list (d (n "im-lists") (r "^0.4") (d #t) (k 0)))) (h "0hzn73isw66z2z864x5b9pbz1bsb53bii4q3a5gvs1yzgyf0ha6x")))

(define-public crate-punc-1.0.7 (c (n "punc") (v "1.0.7") (d (list (d (n "im-lists") (r "^0.4") (d #t) (k 0)))) (h "1hf0rjyj1nz6268nwr8ara847yq8p1zsvi3vv3icl2mv6acmq9rh")))

(define-public crate-punc-1.0.8 (c (n "punc") (v "1.0.8") (d (list (d (n "im-lists") (r "^0.4") (d #t) (k 0)))) (h "0j1g47d6jnp0a7skkgv4bwm5d0mg9s75zghfpf0q1z0dxm2k2ag4")))

(define-public crate-punc-1.0.9 (c (n "punc") (v "1.0.9") (d (list (d (n "im-lists") (r "^0.4") (d #t) (k 0)))) (h "1p540bf2nx5brqlsrg8q5k26vr1lphpvqhdi084lvl2r5ajyjdxj")))

(define-public crate-punc-1.0.10 (c (n "punc") (v "1.0.10") (d (list (d (n "im-lists") (r "^0.4") (d #t) (k 0)))) (h "16vzcgmv89ip4balsfwjznmwxzm4my61hpjjfl7ibbf4p01ivz24")))

(define-public crate-punc-1.0.11 (c (n "punc") (v "1.0.11") (d (list (d (n "im-lists") (r "^0.4") (d #t) (k 0)))) (h "1jri271yhfx9k9qn9lkk481c267byfbclpcl41y3gy3b3z7kklg9")))

(define-public crate-punc-1.0.12 (c (n "punc") (v "1.0.12") (d (list (d (n "im-lists") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0a4qw1blqxz8k591b9qvmhrww4lraf1wvgq99lf7smildaz699s2")))

(define-public crate-punc-1.0.15 (c (n "punc") (v "1.0.15") (d (list (d (n "im-lists") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "08q5sdjcvcb0d2xr70cx3pfj7qp4mk0vl4fbf5mkmmclv4jx48zi")))

(define-public crate-punc-1.0.16 (c (n "punc") (v "1.0.16") (d (list (d (n "im-lists") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1p79mbk3a70qn8p7d4c4kx5d2zlmrn58i6d80d02ql128paqalm8")))

(define-public crate-punc-1.0.17 (c (n "punc") (v "1.0.17") (d (list (d (n "im-lists") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "06s2rh6hxlwrzb54zlx05qxqz0cwhpazl88k65dcikpj6p0bmw68")))

(define-public crate-punc-1.0.18 (c (n "punc") (v "1.0.18") (d (list (d (n "im-lists") (r "^0.4") (d #t) (k 0)))) (h "0v91j6957w5ljkp8di7fvpla5fv98bs1icdqgmm6dz7gbfsmhyh2")))

