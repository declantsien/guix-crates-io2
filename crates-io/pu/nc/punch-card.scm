(define-module (crates-io pu nc punch-card) #:use-module (crates-io))

(define-public crate-punch-card-1.0.0 (c (n "punch-card") (v "1.0.0") (h "1i47krw89msi8n3i07yz1dpk31i34hbf4lc832cxw2pv91wf1am7")))

(define-public crate-punch-card-1.0.1 (c (n "punch-card") (v "1.0.1") (h "0la4bghdz9z1jlb7w9ppsn0sk1y18z3lcx72vgdxzl13gj2cyybk")))

(define-public crate-punch-card-1.0.2 (c (n "punch-card") (v "1.0.2") (h "1a3m18b5812i1cjs57c8h4zmwvlw0h505l2d67bbjl73hzqijq5f")))

(define-public crate-punch-card-1.0.3 (c (n "punch-card") (v "1.0.3") (h "1l9k40yg7az56xw4zmp852ylwf17awcpb8hapvpzimldim2apyg2") (y #t)))

(define-public crate-punch-card-1.1.0 (c (n "punch-card") (v "1.1.0") (h "1h17zf1khzrds3iqnda8qzm8hs9iywjxcx2zqyhwhqf4hcrzfmf4")))

