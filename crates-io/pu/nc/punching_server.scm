(define-module (crates-io pu nc punching_server) #:use-module (crates-io))

(define-public crate-punching_server-0.1.0 (c (n "punching_server") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "async-std") (r "^1.6.2") (d #t) (k 0)))) (h "0caazgqbd7yqw6f3awnx9g4pfwfmbqhgqdq2lkyh9a4bqnnnlh8x")))

(define-public crate-punching_server-0.1.1 (c (n "punching_server") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "async-std") (r "^1.6.2") (d #t) (k 0)) (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "04wdnm4wn7971wf9kv182sx766zyl7dcxmkg8sflxh9dw6x2l1ay")))

