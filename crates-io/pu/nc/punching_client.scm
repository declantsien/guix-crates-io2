(define-module (crates-io pu nc punching_client) #:use-module (crates-io))

(define-public crate-punching_client-0.1.0 (c (n "punching_client") (v "0.1.0") (h "0dddw3vq61sij3z4l4f4icsd4v24bpm2b27lpb40m1xq5nbpamii")))

(define-public crate-punching_client-0.1.1 (c (n "punching_client") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "async-std") (r "^1.6.2") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.36") (d #t) (k 0)) (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "punching_server") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fxn1yqf4s8qm3h74w9g857aqhsg45m6ywra2na26sap1pkinda3")))

