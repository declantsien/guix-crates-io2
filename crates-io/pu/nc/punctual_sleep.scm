(define-module (crates-io pu nc punctual_sleep) #:use-module (crates-io))

(define-public crate-punctual_sleep-0.0.1 (c (n "punctual_sleep") (v "0.0.1") (d (list (d (n "windows") (r "^0.9") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "windows") (r "^0.9") (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "1k1ps81krrqy2j7nnr3rbmxwwi6andkkzrw5wgw6slwzf97qcnxm")))

(define-public crate-punctual_sleep-0.1.0 (c (n "punctual_sleep") (v "0.1.0") (d (list (d (n "windows") (r "^0.48") (f (quote ("Win32_System_Threading" "Win32_Foundation" "Win32_Security"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0k1c29k7azszs9i4cxlw0pry9hr6k5vk4qswipdrf099vcsynlr3") (r "1.73")))

