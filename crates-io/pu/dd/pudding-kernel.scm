(define-module (crates-io pu dd pudding-kernel) #:use-module (crates-io))

(define-public crate-pudding-kernel-0.1.0 (c (n "pudding-kernel") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "cc") (r "^1.0.36") (d #t) (k 1)) (d (n "num") (r "^0.4") (k 0)) (d (n "pudding-pac") (r "^0.1.0") (k 0)))) (h "01lvqjlrcbn7yl35cwrdh2d64rljkad93iiyjlbax4ysaibx8pbp") (f (quote (("std") ("pl390") ("default" "std"))))))

(define-public crate-pudding-kernel-0.1.1 (c (n "pudding-kernel") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "cc") (r "^1.0.36") (d #t) (k 1)) (d (n "num") (r "^0.4") (k 0)) (d (n "pudding-pac") (r "^0.1.2") (k 0)))) (h "1aavcg53qyw7h03mzxdy0jfbp1qrx16wrn7ifafd6fzl9035sl6d") (f (quote (("std") ("pl390") ("default" "std"))))))

(define-public crate-pudding-kernel-0.1.2 (c (n "pudding-kernel") (v "0.1.2") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.36") (d #t) (k 1)) (d (n "num") (r "^0.4") (k 0)) (d (n "pudding-pac") (r "^0.1.3") (k 0)))) (h "1n60gin0amknpj7cj2mwdx9nmha8wq2asm2asd2vy92mj5ymd93m") (f (quote (("std") ("pl390") ("default" "std"))))))

