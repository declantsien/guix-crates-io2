(define-module (crates-io pu dd pudding-pac) #:use-module (crates-io))

(define-public crate-pudding-pac-0.1.0 (c (n "pudding-pac") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "1qr2823z2mgwv6n5vs6d6z8as8r99bmvpnn8jwgjgrx9b3wmcj7z")))

(define-public crate-pudding-pac-0.1.1 (c (n "pudding-pac") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "08az9d65qy533qqlxkzvlby49z13pmqm5p8f2sjpfnglphpcqwwh")))

(define-public crate-pudding-pac-0.1.2 (c (n "pudding-pac") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "0snxp6jm9m2aiv5pqh74mzrjh0jbnj2f2kkrlibfv20k4iih01j0")))

(define-public crate-pudding-pac-0.1.3 (c (n "pudding-pac") (v "0.1.3") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)))) (h "1808dx7qj5khgq47zdql82pm3a3bj04xaiqrnwhw6kzjf2f59b8c")))

