(define-module (crates-io pu ll pulldown-cmark-frontmatter) #:use-module (crates-io))

(define-public crate-pulldown-cmark-frontmatter-0.1.0 (c (n "pulldown-cmark-frontmatter") (v "0.1.0") (d (list (d (n "pulldown-cmark") (r "^0.9.2") (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.5.10") (d #t) (k 2)))) (h "1lqzwmzs2iv6q7y9chpchhlikika78nxbz4ia1hwvvy4fbv2xvk5") (r "1.58")))

(define-public crate-pulldown-cmark-frontmatter-0.1.1 (c (n "pulldown-cmark-frontmatter") (v "0.1.1") (d (list (d (n "pulldown-cmark") (r "^0.9.2") (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.7.2") (d #t) (k 2)))) (h "1c3sfzn3bnai0y74p71jxd2sa66dxkk5hk83cvb3nrib9368bxjl") (r "1.58")))

(define-public crate-pulldown-cmark-frontmatter-0.2.0 (c (n "pulldown-cmark-frontmatter") (v "0.2.0") (d (list (d (n "pulldown-cmark") (r "^0.9.2") (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.8.0") (d #t) (k 2)))) (h "0q5bzvhzn3lrbb90gbpgby5lh1zilxcd60m1cwi9xs4z7yy6wy8d") (r "1.58")))

