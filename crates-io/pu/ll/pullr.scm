(define-module (crates-io pu ll pullr) #:use-module (crates-io))

(define-public crate-pullr-0.1.0 (c (n "pullr") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.10") (d #t) (k 2)))) (h "0v8yg9yqxn70vfx7wiav78pcxcgny4qrnxjxwshjabvs9ny6gacf")))

