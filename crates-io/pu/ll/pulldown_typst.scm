(define-module (crates-io pu ll pulldown_typst) #:use-module (crates-io))

(define-public crate-pulldown_typst-0.1.0 (c (n "pulldown_typst") (v "0.1.0") (d (list (d (n "pulldown-cmark") (r "^0.9.3") (k 0)))) (h "0ihyg5dznm2chblkr9iywk5gf1l1nj5rh921xbjq5w8iqg6sz038")))

(define-public crate-pulldown_typst-0.2.0 (c (n "pulldown_typst") (v "0.2.0") (d (list (d (n "pulldown-cmark") (r "^0.9.3") (k 0)))) (h "18rnzpk01lmjc3a02g141cdz9zr7cv1cmpbygijzx3kj7ykf8qsg")))

(define-public crate-pulldown_typst-0.3.0 (c (n "pulldown_typst") (v "0.3.0") (d (list (d (n "pulldown-cmark") (r "^0.9.3") (k 0)) (d (n "tracing") (r "^0.1.40") (o #t) (d #t) (k 0)))) (h "19a6vhixl2x0g0ijq2km2dw7fs9g0gd76937bwzlip61mzx19s9s") (s 2) (e (quote (("tracing" "dep:tracing"))))))

(define-public crate-pulldown_typst-0.3.1 (c (n "pulldown_typst") (v "0.3.1") (d (list (d (n "pulldown-cmark") (r "^0.9.3") (k 0)) (d (n "tracing") (r "^0.1.40") (o #t) (d #t) (k 0)))) (h "0n6yx1a322p1bfli7sgg1vhr93i8820bs2b0vxp7f2gx6rhhlrzj") (s 2) (e (quote (("tracing" "dep:tracing"))))))

(define-public crate-pulldown_typst-0.3.2 (c (n "pulldown_typst") (v "0.3.2") (d (list (d (n "pulldown-cmark") (r "^0.9.3") (k 0)) (d (n "tracing") (r "^0.1.40") (o #t) (d #t) (k 0)))) (h "0mfpqh0zb5kjn820yyhza7liwiyp54l0qlsqgsipk7r3jsfg3wlm") (s 2) (e (quote (("tracing" "dep:tracing"))))))

(define-public crate-pulldown_typst-0.3.3 (c (n "pulldown_typst") (v "0.3.3") (d (list (d (n "pulldown-cmark") (r "^0.9.3") (k 0)) (d (n "tracing") (r "^0.1.40") (o #t) (d #t) (k 0)))) (h "07054wkr6siirvv0k2ny9z6wvv1ask4g51yc0myx6d9444jfsnbc") (s 2) (e (quote (("tracing" "dep:tracing"))))))

