(define-module (crates-io pu ll pulldown-cmark-escape) #:use-module (crates-io))

(define-public crate-pulldown-cmark-escape-0.10.0 (c (n "pulldown-cmark-escape") (v "0.10.0") (h "0sr72gv731wjys9srjzrf13zfwivxq200c00pzwgmg1w1smgkn6m") (r "1.70")))

(define-public crate-pulldown-cmark-escape-0.10.1 (c (n "pulldown-cmark-escape") (v "0.10.1") (h "1lqx7c2f0bx0qq9kkyn18gsa2dl2sk8x5jp8gvdax75w73sqyd5x") (f (quote (("simd")))) (r "1.70")))

(define-public crate-pulldown-cmark-escape-0.11.0 (c (n "pulldown-cmark-escape") (v "0.11.0") (h "1bp13akkz52p43vh2ffpgv604l3xd9b67b4iykizidnsbpdqlz80") (f (quote (("simd")))) (r "1.70")))

