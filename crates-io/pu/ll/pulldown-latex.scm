(define-module (crates-io pu ll pulldown-latex) #:use-module (crates-io))

(define-public crate-pulldown-latex-0.1.0 (c (n "pulldown-latex") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "fantoccini") (r "^0.19") (d #t) (k 2)) (d (n "heck") (r "^0.5") (d #t) (k 2)) (d (n "inventory") (r "^0.3") (d #t) (k 2)) (d (n "libtest-mimic") (r "^0.7") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("test-util" "rt-multi-thread" "process" "fs"))) (d #t) (k 2)))) (h "0j6q4y082hp71isb7zdzigfbgrm4wvc6rp2iy0jbvg8r86g45whp")))

