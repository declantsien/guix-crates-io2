(define-module (crates-io pu ll pulldown-cmark-to-cmark) #:use-module (crates-io))

(define-public crate-pulldown-cmark-to-cmark-1.0.0 (c (n "pulldown-cmark-to-cmark") (v "1.0.0") (d (list (d (n "indoc") (r "^0.2.3") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.1.0") (k 0)))) (h "003ypqs5jd9kacyv6d70jjgkfmlsk0fxjhpp8n7rk8007hzynsbw")))

(define-public crate-pulldown-cmark-to-cmark-1.1.0 (c (n "pulldown-cmark-to-cmark") (v "1.1.0") (d (list (d (n "indoc") (r "^0.2.3") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.1.0") (k 0)))) (h "1r77pd4ini2937ksfnn707pii7qnl7hyxg297bp3cczna9gwmvsp")))

(define-public crate-pulldown-cmark-to-cmark-1.2.0 (c (n "pulldown-cmark-to-cmark") (v "1.2.0") (d (list (d (n "indoc") (r "^0.3.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.2.0") (k 0)))) (h "16796k1bkl7v5pn6mq03zahab0v3nbgvp5bxwz567xs0dh625grw")))

(define-public crate-pulldown-cmark-to-cmark-1.2.1 (c (n "pulldown-cmark-to-cmark") (v "1.2.1") (d (list (d (n "indoc") (r "^0.3.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.4.1") (k 0)))) (h "1isb2skj0wpcalvl42wa9n1ms6pal2bgyl2agbh1nz3ng2pyvfar")))

(define-public crate-pulldown-cmark-to-cmark-1.2.2 (c (n "pulldown-cmark-to-cmark") (v "1.2.2") (d (list (d (n "indoc") (r "^0.3.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.5.2") (k 0)))) (h "1v1hf25kg44m05n6qwnlvjqbxp5gg28ihdribi8x5f5d54zyrbdg")))

(define-public crate-pulldown-cmark-to-cmark-1.2.3 (c (n "pulldown-cmark-to-cmark") (v "1.2.3") (d (list (d (n "indoc") (r "^0.3.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.5.2") (k 0)))) (h "175fkin1gp7va1mr8qlrnfaingg2acwkj09bnd6vchg549akxz4g")))

(define-public crate-pulldown-cmark-to-cmark-1.2.4 (c (n "pulldown-cmark-to-cmark") (v "1.2.4") (d (list (d (n "indoc") (r "^0.3.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.5.2") (k 0)))) (h "0px2f3c5ilmwqyy5j2016y7yls58xn6k74pgxw08baq2ik5snxzj")))

(define-public crate-pulldown-cmark-to-cmark-2.0.0 (c (n "pulldown-cmark-to-cmark") (v "2.0.0") (d (list (d (n "indoc") (r "^0.3.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.6.1") (k 0)))) (h "1cdgrynx67520c4bkz451m2dlcn4h81jd4jnbi9fzq0jnm5dhsp6")))

(define-public crate-pulldown-cmark-to-cmark-2.0.1 (c (n "pulldown-cmark-to-cmark") (v "2.0.1") (d (list (d (n "indoc") (r "^0.3.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.6.1") (k 0)))) (h "12gyc03v5946vgzqik11ywx3k5nq0n9nzg32644hx0503ndnb2dz")))

(define-public crate-pulldown-cmark-to-cmark-3.0.0 (c (n "pulldown-cmark-to-cmark") (v "3.0.0") (d (list (d (n "indoc") (r "^0.3.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.7.0") (k 0)))) (h "01xr3gxsc9szw3srfi9zwl7z4bd0pyqg68429fvf3sv7nx6hvan6")))

(define-public crate-pulldown-cmark-to-cmark-3.0.1 (c (n "pulldown-cmark-to-cmark") (v "3.0.1") (d (list (d (n "indoc") (r "^0.3.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.7.0") (k 0)))) (h "0ykvmlm5c1q7w8id21g9b00aa5gy3mziays0mkarlb6b6yy50yiv")))

(define-public crate-pulldown-cmark-to-cmark-4.0.0 (c (n "pulldown-cmark-to-cmark") (v "4.0.0") (d (list (d (n "indoc") (r "^0.3.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.7.0") (k 0)))) (h "0aiqba65z47n3g8c90jxrs2mj1al2h75ml76k0974527mqdqb04z")))

(define-public crate-pulldown-cmark-to-cmark-4.0.1 (c (n "pulldown-cmark-to-cmark") (v "4.0.1") (d (list (d (n "indoc") (r "^0.3.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.7.0") (k 0)))) (h "0vs3phh8zx3wf34j6n5f5c4bydkxsz2jy482y91na6d6id58sk9v")))

(define-public crate-pulldown-cmark-to-cmark-4.0.2 (c (n "pulldown-cmark-to-cmark") (v "4.0.2") (d (list (d (n "indoc") (r "^0.3.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.7.0") (k 0)))) (h "0n39qld14qq8cz8sh5v7740frnlilaj640qrmhwf4a9x8m75kyyg")))

(define-public crate-pulldown-cmark-to-cmark-5.0.0 (c (n "pulldown-cmark-to-cmark") (v "5.0.0") (d (list (d (n "indoc") (r "^0.3.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.7.0") (k 0)))) (h "0dpp8qrnb3zl3npjbksrrkb64dvhccrng9cca05qq78jfd2czb1j")))

(define-public crate-pulldown-cmark-to-cmark-6.0.0 (c (n "pulldown-cmark-to-cmark") (v "6.0.0") (d (list (d (n "indoc") (r "^1.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.8.0") (k 0)))) (h "08yyjr60jk8f4v4sqcgmna5n9y31l6lx3z9lak38wd82h63vkwp8")))

(define-public crate-pulldown-cmark-to-cmark-6.0.1 (c (n "pulldown-cmark-to-cmark") (v "6.0.1") (d (list (d (n "indoc") (r "^1.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.8.0") (k 0)))) (h "106fjqc7d924c7jb1xw28cwvg6n469as7k20zhrxhs2w2abg75ba")))

(define-public crate-pulldown-cmark-to-cmark-6.0.2 (c (n "pulldown-cmark-to-cmark") (v "6.0.2") (d (list (d (n "indoc") (r "^1.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.8.0") (k 0)))) (h "01rp9hy8bn7fynkwlba72zdbg1fmci0chlddjazag7as2618614m")))

(define-public crate-pulldown-cmark-to-cmark-6.0.3 (c (n "pulldown-cmark-to-cmark") (v "6.0.3") (d (list (d (n "indoc") (r "^1.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.8.0") (k 0)))) (h "13p0f8ck0ffmmfvr90zp4ykhv5pdngb9jjljbs7crd5qi5cpfbd7")))

(define-public crate-pulldown-cmark-to-cmark-6.0.4 (c (n "pulldown-cmark-to-cmark") (v "6.0.4") (d (list (d (n "indoc") (r "^1.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.8.0") (k 0)))) (h "1s6h5lqfhfg201vazz6lf2h04r71y6y6n3ydi70w7c4gqvi2an8z")))

(define-public crate-pulldown-cmark-to-cmark-7.0.0 (c (n "pulldown-cmark-to-cmark") (v "7.0.0") (d (list (d (n "indoc") (r "^1.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.8.0") (k 0)))) (h "08p5svfdn727rvvyibcgzdhxs2jx9d41s8zk8ihib5sv44sw6giv")))

(define-public crate-pulldown-cmark-to-cmark-7.1.0 (c (n "pulldown-cmark-to-cmark") (v "7.1.0") (d (list (d (n "indoc") (r "^1.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.8.0") (k 0)))) (h "0n6nm4qk05awv3y0prny9sk642xhhj3dwf0y0jsw4j82vq3yd19d")))

(define-public crate-pulldown-cmark-to-cmark-7.1.1 (c (n "pulldown-cmark-to-cmark") (v "7.1.1") (d (list (d (n "indoc") (r "^1.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.8.0") (k 0)))) (h "03f6bnqjzchmzs4qaf63pirgj95b0x3l2rlp1wj1z27mp734phcb")))

(define-public crate-pulldown-cmark-to-cmark-8.0.0 (c (n "pulldown-cmark-to-cmark") (v "8.0.0") (d (list (d (n "indoc") (r "^1.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.9.0") (k 0)))) (h "0b0a1a508a4sv660wpypkgvpza8jf94cifk4hqbnjf20g6si0fhp")))

(define-public crate-pulldown-cmark-to-cmark-9.0.0 (c (n "pulldown-cmark-to-cmark") (v "9.0.0") (d (list (d (n "indoc") (r "^1.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.9.0") (k 0)))) (h "05qdlwjv3q45dfi1crgz3z54v7yyln5l5a6fik90migmdrsxr51z")))

(define-public crate-pulldown-cmark-to-cmark-10.0.0 (c (n "pulldown-cmark-to-cmark") (v "10.0.0") (d (list (d (n "indoc") (r "^1.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.9.0") (k 0)))) (h "121g0vbrns4syapb1j81qwgf85k0mandarcg977fdkmv5hw1kpfi")))

(define-public crate-pulldown-cmark-to-cmark-10.0.1 (c (n "pulldown-cmark-to-cmark") (v "10.0.1") (d (list (d (n "indoc") (r "^1.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.9.0") (k 0)))) (h "0prqs220j1mlgqjz957gd25gqi7200m4zll3bmdqkp7p21ivgbry")))

(define-public crate-pulldown-cmark-to-cmark-10.0.2 (c (n "pulldown-cmark-to-cmark") (v "10.0.2") (d (list (d (n "indoc") (r "^1.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.9.0") (k 0)))) (h "04yr4n1f0yggg1fq6m3y9wzx0rv1fkzn0gld48jsjbqr1323ldf1")))

(define-public crate-pulldown-cmark-to-cmark-10.0.3 (c (n "pulldown-cmark-to-cmark") (v "10.0.3") (d (list (d (n "indoc") (r "^1.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.9.0") (k 0)))) (h "1y6fv24ckd4vaqxhrad44j591g8c39ccn19v0481d103vq4w8rw7")))

(define-public crate-pulldown-cmark-to-cmark-10.0.4 (c (n "pulldown-cmark-to-cmark") (v "10.0.4") (d (list (d (n "indoc") (r "^1.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.9.0") (k 0)))) (h "0gc366cmd5jxal9m95l17rvqsm4dn62lywc8v5gwq8vcjvhyd501")))

(define-public crate-pulldown-cmark-to-cmark-11.0.0 (c (n "pulldown-cmark-to-cmark") (v "11.0.0") (d (list (d (n "indoc") (r "^1.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.9.0") (k 0)))) (h "169012asknhgklfkmgdk4dp0sz6rxfgrssb92sjxikkjb8iql4gk")))

(define-public crate-pulldown-cmark-to-cmark-11.0.1 (c (n "pulldown-cmark-to-cmark") (v "11.0.1") (d (list (d (n "indoc") (r "^1.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.9.0") (k 0)))) (h "0szv02bb1d9a0dji22ahfyg29ybhpy1slch8zy5yw9xfiq8r8mpg")))

(define-public crate-pulldown-cmark-to-cmark-11.0.2 (c (n "pulldown-cmark-to-cmark") (v "11.0.2") (d (list (d (n "indoc") (r "^1.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.9.0") (k 0)))) (h "0417fa5qwl427ilqabprgkni3wpnwvfqjfmh46aj0kzwz2ay90x8")))

(define-public crate-pulldown-cmark-to-cmark-11.1.0 (c (n "pulldown-cmark-to-cmark") (v "11.1.0") (d (list (d (n "indoc") (r "^1.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.9.0") (k 0)))) (h "0cfhpzpg7s361xbh1ir1km2wl4rg6s78s9n05ryzrr88m1rimi66") (y #t)))

(define-public crate-pulldown-cmark-to-cmark-11.2.0 (c (n "pulldown-cmark-to-cmark") (v "11.2.0") (d (list (d (n "indoc") (r "^1.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.9.0") (k 0)))) (h "0hjfyd3q2wm7a9rlds7bx9yb36ryc2d9dkczhig06cbn5prn9m3d")))

(define-public crate-pulldown-cmark-to-cmark-12.0.0 (c (n "pulldown-cmark-to-cmark") (v "12.0.0") (d (list (d (n "indoc") (r "^1.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.10.0") (k 0)))) (h "0hb1z34czm4z36k00ifqqbsd7crwi5pn5mgwry2p3bz6pv2vn10a")))

(define-public crate-pulldown-cmark-to-cmark-13.0.0 (c (n "pulldown-cmark-to-cmark") (v "13.0.0") (d (list (d (n "indoc") (r "^1.0.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "pulldown-cmark") (r "^0.10.0") (k 0)))) (h "0fqnayiafhrzwy39zsci92vz2zmmkxdl2s7pzkf7jpw3imf7j2gn")))

