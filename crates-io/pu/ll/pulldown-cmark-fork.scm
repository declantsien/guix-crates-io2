(define-module (crates-io pu ll pulldown-cmark-fork) #:use-module (crates-io))

(define-public crate-pulldown-cmark-fork-0.5.2 (c (n "pulldown-cmark-fork") (v "0.5.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "html5ever") (r "^0.23") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3") (d #t) (k 2)) (d (n "memchr") (r "^2.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 2)) (d (n "tendril") (r "^0.4") (d #t) (k 2)) (d (n "unicase") (r "^2.4") (d #t) (k 0)))) (h "1mvcxnjzhvwschhx1jhpkicgwslqcsi4s63b7cfc5v9f9cda1aqm") (f (quote (("simd") ("gen-tests") ("default" "getopts"))))))

