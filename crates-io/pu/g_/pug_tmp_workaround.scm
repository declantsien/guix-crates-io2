(define-module (crates-io pu g_ pug_tmp_workaround) #:use-module (crates-io))

(define-public crate-pug_tmp_workaround-0.1.9 (c (n "pug_tmp_workaround") (v "0.1.9") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1a038b04z6a2jival3hxvgf9l7dzh7m4226l8mbqqhdvxfpc20q9")))

(define-public crate-pug_tmp_workaround-0.0.1 (c (n "pug_tmp_workaround") (v "0.0.1") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "11jyv6m6sj5rsmw5rglhhvbhjxhdq7b821pxb3m0sv53mm33fl9x")))

(define-public crate-pug_tmp_workaround-0.0.2 (c (n "pug_tmp_workaround") (v "0.0.2") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1phsypghahnn6544c1w8krlhy4n37xf5yk92gfy67cqmvg7s3cgy")))

(define-public crate-pug_tmp_workaround-0.0.3 (c (n "pug_tmp_workaround") (v "0.0.3") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1jzvzgqy8bs7zbpzj72m493hzadv79m73xcb70ygmcb4q1i0pmzy")))

