(define-module (crates-io pu bg pubgrub) #:use-module (crates-io))

(define-public crate-pubgrub-0.1.0 (c (n "pubgrub") (v "0.1.0") (d (list (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1wvx3iqja9l8m637caxw8c9kykqx2vfgxl4m0wkcirmwzi744wgr")))

(define-public crate-pubgrub-0.2.0 (c (n "pubgrub") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "ron") (r "^0.6") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "varisat") (r "^0.2.2") (d #t) (k 2)))) (h "044qnsmcxnc006k8z354sp8vxkm760g50ijd6n6iqga54lcygxyi")))

(define-public crate-pubgrub-0.2.1 (c (n "pubgrub") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "ron") (r "^0.6") (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "varisat") (r "^0.2.2") (d #t) (k 2)))) (h "1z9qdzjfq4yqfzrh2rjx6m2a08k8y9vda41w68x78pazmm94blfd")))

