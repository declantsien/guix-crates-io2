(define-module (crates-io pu ny punycode) #:use-module (crates-io))

(define-public crate-punycode-0.1.0 (c (n "punycode") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "0mjndy4ffb45wl6rghym2kx6gjlnys9mdsvw1k9sl7l617zcwas2") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-punycode-0.2.0 (c (n "punycode") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "0ya78whqfq2896wj68kpnhia43xm5asmh8h1jrw0xrahqzmnpzmw") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-punycode-0.3.0 (c (n "punycode") (v "0.3.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "0q9p6n1k1q2hlhr48kr3wv8vm7y4azp1lpfjp51hf42lfxw9i2y8") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-punycode-0.4.0 (c (n "punycode") (v "0.4.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "1lwa292nhg4p9zv4k1kmwj5zyxnkl5l1blmjhc4d793hr8n13pbd") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-punycode-0.4.1 (c (n "punycode") (v "0.4.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "1zm5722qaz1zhxb5nxnisj009crrknjs9xv4vdp9z0yn42rxrqg9") (f (quote (("dev" "clippy") ("default"))))))

