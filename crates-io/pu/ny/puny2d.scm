(define-module (crates-io pu ny puny2d) #:use-module (crates-io))

(define-public crate-puny2d-0.0.1 (c (n "puny2d") (v "0.0.1") (d (list (d (n "font-loader") (r "~0.11.0") (d #t) (k 0)) (d (n "glyph_brush_layout") (r "~0.2.1") (d #t) (k 0)) (d (n "image") (r "~0.23.13") (d #t) (k 2)) (d (n "lru") (r "~0.6.4") (d #t) (k 0)) (d (n "microbench") (r "~0.5.0") (d #t) (k 2)) (d (n "ocl") (r "~0.19.3") (o #t) (d #t) (k 0)) (d (n "rayon") (r "~1.5.0") (d #t) (k 0)) (d (n "rusttype") (r "~0.9.2") (d #t) (k 0)))) (h "0l40rfw73k1d21qxd17l5y7jzwibl5yjhjw3azw58894pwfvk96n") (f (quote (("gpgpu" "ocl"))))))

(define-public crate-puny2d-0.0.2 (c (n "puny2d") (v "0.0.2") (d (list (d (n "font-loader") (r "~0.11.0") (d #t) (k 0)) (d (n "glyph_brush_layout") (r "~0.2.1") (d #t) (k 0)) (d (n "image") (r "~0.23.13") (d #t) (k 2)) (d (n "lru") (r "~0.6.4") (d #t) (k 0)) (d (n "microbench") (r "~0.5.0") (d #t) (k 2)) (d (n "rayon") (r "~1.5.0") (d #t) (k 0)) (d (n "rusttype") (r "~0.9.2") (d #t) (k 0)))) (h "09vmq4g46dx8axm0pj5i2k39zpgpmx34vsm5f94azh7n6r4x01w3")))

