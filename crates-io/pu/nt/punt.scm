(define-module (crates-io pu nt punt) #:use-module (crates-io))

(define-public crate-punt-0.1.0 (c (n "punt") (v "0.1.0") (d (list (d (n "crc-any") (r "^2.2.2") (d #t) (k 0)) (d (n "rusb") (r "^0.5.1") (d #t) (k 0)))) (h "05aqv58c2hrd8y4svddc0gdfj96bn0ksqrrn6wjkz22d3qskq5pb")))

(define-public crate-punt-0.2.0 (c (n "punt") (v "0.2.0") (d (list (d (n "crc-any") (r "^2.2.2") (d #t) (k 0)) (d (n "rusb") (r "^0.5.1") (d #t) (k 0)))) (h "16d7qrmbsc3jrhwnmj50kc5wfw3q7j9j6y24xkkk498cwsjzyr9v")))

(define-public crate-punt-0.2.1 (c (n "punt") (v "0.2.1") (d (list (d (n "crc-any") (r "^2.4.3") (d #t) (k 0)) (d (n "rusb") (r "^0.9.2") (d #t) (k 0)))) (h "18fcg2azca5yrdr2bd6gr6ag1fagrsmbdq9vdcrlymi3as8lpchv")))

(define-public crate-punt-0.3.0 (c (n "punt") (v "0.3.0") (d (list (d (n "crc-any") (r "^2.4.3") (d #t) (k 0)) (d (n "rusb") (r "^0.9.2") (d #t) (k 0)))) (h "19fzx745gzgxd7n1h0cnwwrp6wakvv1g5dqpy3s6dd9vjg04djhh")))

