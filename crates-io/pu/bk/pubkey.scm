(define-module (crates-io pu bk pubkey) #:use-module (crates-io))

(define-public crate-pubkey-0.1.0 (c (n "pubkey") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.9") (d #t) (k 0)))) (h "1c2akj4jaf9rx1c99f9q863swah319by19hmf6ihyx652r9byzzn") (y #t)))

(define-public crate-pubkey-0.1.1 (c (n "pubkey") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7") (d #t) (k 0)))) (h "015kj8pnzrnind6p5x454w6a6h9js3js97ywlhxmfnw89lfcz1mm") (y #t)))

