(define-module (crates-io pu pi pupil) #:use-module (crates-io))

(define-public crate-pupil-0.1.0 (c (n "pupil") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0sydr1g7c1jswpz1s6w80as58aifk92wqih3iqpdr4qz054nw7qw")))

(define-public crate-pupil-0.1.1 (c (n "pupil") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zcrhr8g4171d0ynl7whs6qlas1m3gkmkpz2n9k1wg5yrdimgili")))

(define-public crate-pupil-0.1.2 (c (n "pupil") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0bz0w5cfihhylw8qyfd8cr05vpsnfcqj588ab7kn7hc7k0ql5k3n")))

(define-public crate-pupil-0.1.3 (c (n "pupil") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1d0ljkl6p5izm0nrrkkqwdnmnvxndyzvpvfyc098driv5hn2irm2")))

