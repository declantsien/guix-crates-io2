(define-module (crates-io pu re pure_vorbis) #:use-module (crates-io))

(define-public crate-pure_vorbis-0.0.1 (c (n "pure_vorbis") (v "0.0.1") (d (list (d (n "ao") (r "^0.6.1") (d #t) (k 2)) (d (n "clap") (r "^2.11.0") (d #t) (k 2)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.1.34") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0.0") (d #t) (k 2)) (d (n "ogg_vorbis_ref") (r "^0.0.1") (d #t) (k 2)) (d (n "scoped-pool") (r "^1.0.0") (d #t) (k 2)))) (h "1lpvdycqvgq1jvy57k7xi1ffv2p9x2ajd402y40g1n67wq2bmviy")))

