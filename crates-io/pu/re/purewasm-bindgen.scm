(define-module (crates-io pu re purewasm-bindgen) #:use-module (crates-io))

(define-public crate-purewasm-bindgen-0.1.0 (c (n "purewasm-bindgen") (v "0.1.0") (d (list (d (n "lol_alloc") (r "^0.4.0") (d #t) (k 0)) (d (n "purewasm-core") (r "^0.1.0") (d #t) (k 0)) (d (n "purewasm-proc-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive" "alloc" "derive" "alloc"))) (k 0)))) (h "1m9snz5virxaxhcqf98hmkahzlsl7rcwp207pw5nxlbs3knmdz4m")))

