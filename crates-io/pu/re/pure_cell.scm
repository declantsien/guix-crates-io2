(define-module (crates-io pu re pure_cell) #:use-module (crates-io))

(define-public crate-pure_cell-0.1.0 (c (n "pure_cell") (v "0.1.0") (h "1j1201ia54fivvgcw3k5fh98pkc86w26l76p018f81j2c5cxbkd9")))

(define-public crate-pure_cell-0.2.0 (c (n "pure_cell") (v "0.2.0") (h "0kckwd357kfw1zn5qg4zv1f9pym5vfxmjdd8484ypf7z5hx28kmz")))

