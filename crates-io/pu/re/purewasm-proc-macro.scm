(define-module (crates-io pu re purewasm-proc-macro) #:use-module (crates-io))

(define-public crate-purewasm-proc-macro-0.1.0 (c (n "purewasm-proc-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "1r6l9ywnvxzhn4gchcl1q25cf7xfq3q14rzfa8s5j82jrcxjj9jw")))

