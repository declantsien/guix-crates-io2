(define-module (crates-io pu re purescript_waterslide) #:use-module (crates-io))

(define-public crate-purescript_waterslide-0.1.0 (c (n "purescript_waterslide") (v "0.1.0") (h "003mbxmpd8414gjaanybpbicwsv9qlaj30yl45nnsppr5n35bqid")))

(define-public crate-purescript_waterslide-0.2.0 (c (n "purescript_waterslide") (v "0.2.0") (h "1pvyazs23wvxm9i74pckzq53x19gqiysfrcrll3yzp1yaamsm0jf")))

(define-public crate-purescript_waterslide-0.2.1 (c (n "purescript_waterslide") (v "0.2.1") (h "11yvifz9fchbdqs61gmqglx462r6ksc8gg1nan2kk3i05c1gk7x1")))

(define-public crate-purescript_waterslide-0.3.0 (c (n "purescript_waterslide") (v "0.3.0") (h "0k2pmrk5lar5c33byz91xsvnx1l81lq6lz9j53r1zivjvg4s6jbl")))

(define-public crate-purescript_waterslide-0.3.1 (c (n "purescript_waterslide") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "0j3chgjlz4qrq7d8w1z7d9y35fddavcj1kbxwm8mx4l3rzmdacwr") (f (quote (("uuid_support" "uuid") ("chrono_support" "chrono"))))))

