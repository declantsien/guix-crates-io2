(define-module (crates-io pu re pure_decimal) #:use-module (crates-io))

(define-public crate-pure_decimal-0.0.1 (c (n "pure_decimal") (v "0.0.1") (d (list (d (n "decimal") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "18arg00i0cfd4qymhi02y2b9m8zbp6y9q02b7d4s7hz0n0hpszqk") (f (quote (("default" "serde"))))))

(define-public crate-pure_decimal-0.0.2 (c (n "pure_decimal") (v "0.0.2") (d (list (d (n "decimal") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "19fkg5237z8s311paqv9sn83jzx2cwl24dnm9mlvjh52w64pqmw6") (f (quote (("default" "serde"))))))

(define-public crate-pure_decimal-0.0.3 (c (n "pure_decimal") (v "0.0.3") (d (list (d (n "decimal") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1pl1bwir2rv3hd4q035sngd88hbcf1kngg0cd0zz0xnnbljs3jf0") (f (quote (("default" "serde"))))))

(define-public crate-pure_decimal-0.0.4 (c (n "pure_decimal") (v "0.0.4") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "decimal") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "179dyj185v2sfi96igbrkm8w8750nrjmp3wn716c50mkm0sw3nc2") (f (quote (("default" "serde"))))))

(define-public crate-pure_decimal-0.0.5 (c (n "pure_decimal") (v "0.0.5") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "decimal") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1gpyw1y68pjk99q52ahsq984vwmli7dk8nzkp1j63fm0fkj8b0mf") (f (quote (("default" "serde"))))))

(define-public crate-pure_decimal-0.0.6 (c (n "pure_decimal") (v "0.0.6") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "decimal") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1ccaz4z41knd1j1nli6nw2yy0npsq3dxpkk4cjgasr02zzlcnrp1") (f (quote (("default" "serde"))))))

(define-public crate-pure_decimal-0.0.7 (c (n "pure_decimal") (v "0.0.7") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "decimal") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "01vpqvlcpsmaxsmaxr2msk5hykbrpx0ckr1zxycnh50fd39fin92") (f (quote (("default" "serde"))))))

