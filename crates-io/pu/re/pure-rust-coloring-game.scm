(define-module (crates-io pu re pure-rust-coloring-game) #:use-module (crates-io))

(define-public crate-pure-rust-coloring-game-0.1.0 (c (n "pure-rust-coloring-game") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1w5b2xizcig6q61y9gjikh85wrdhgwyaqsi4dfpzr05v8583q1w7") (y #t)))

