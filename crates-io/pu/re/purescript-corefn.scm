(define-module (crates-io pu re purescript-corefn) #:use-module (crates-io))

(define-public crate-purescript-corefn-0.1.0 (c (n "purescript-corefn") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)))) (h "1sphkh6wsx5nzblyami6cbbckv06zdzk6rx6i42swr2g4fxcrmx9")))

(define-public crate-purescript-corefn-0.1.1 (c (n "purescript-corefn") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)))) (h "0ss28snpk1na3mncag0h2qwa3a72li335rhyc7124cr85p3p1aag")))

(define-public crate-purescript-corefn-0.1.2 (c (n "purescript-corefn") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)))) (h "0fv1xgimsdch3m3c2pxzz1xmb3lgjkn6qh0ybjb7n1l4qxrffb9w")))

(define-public crate-purescript-corefn-0.1.3 (c (n "purescript-corefn") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)))) (h "1id0jd9a835ikl0mxvkssbnfq5ir3y6rzv427ysd9w8nqlw2qsh6")))

(define-public crate-purescript-corefn-0.1.4 (c (n "purescript-corefn") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)))) (h "1q02g6wlgg5l4yxg6nhxlaz67mkksn7qd3iv8v42ipd1zgxm4d0z")))

(define-public crate-purescript-corefn-0.2.0 (c (n "purescript-corefn") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.36") (d #t) (k 0)))) (h "0lrbk1z9mqnjpnhpb06nkqv7ap0azrk7lczi9s933l1ly7pa7wss")))

(define-public crate-purescript-corefn-0.2.1 (c (n "purescript-corefn") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.36") (d #t) (k 0)))) (h "0byrs3sgxqlmdv7y7rzwp8sb1b3693gb4br00bgryvl7rqmi9v18")))

(define-public crate-purescript-corefn-0.2.2 (c (n "purescript-corefn") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.36") (d #t) (k 0)))) (h "05x4hdjs9vsamnl54mw0fcibpniypcr0b1b0zd2kvjzyp7a7vc85")))

(define-public crate-purescript-corefn-0.3.0 (c (n "purescript-corefn") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "0n76r7qx4gxmk9d05wkp0fn7946c9sq3kc2dx47ggb0xwi2xc1g8")))

