(define-module (crates-io pu re purewasm) #:use-module (crates-io))

(define-public crate-purewasm-0.1.0 (c (n "purewasm") (v "0.1.0") (d (list (d (n "purewasm-bindgen") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "purewasm-cbor") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "purewasm-core") (r "^0.1.0") (d #t) (k 0)) (d (n "purewasm-json") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "purewasm-wasmtime") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0j7hp2ib3i7alr09fhp78f58jz3dd3mcik4ahpcrdm23f1wr4d6p") (f (quote (("wasmtime" "purewasm-wasmtime")))) (s 2) (e (quote (("bindgen-json" "purewasm-bindgen" "dep:purewasm-json") ("bindgen" "purewasm-bindgen" "dep:purewasm-cbor"))))))

