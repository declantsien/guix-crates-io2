(define-module (crates-io pu re pure-hfsm) #:use-module (crates-io))

(define-public crate-pure-hfsm-0.1.0 (c (n "pure-hfsm") (v "0.1.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "smallvec") (r "^1.7") (f (quote ("union" "const_generics"))) (d #t) (k 0)))) (h "13f8701rbhy0yiicvc5wykgq0yybmpmhcfjwdz5mh63k0i86w11m")))

