(define-module (crates-io pu re purewasm-json) #:use-module (crates-io))

(define-public crate-purewasm-json-0.1.0 (c (n "purewasm-json") (v "0.1.0") (d (list (d (n "purewasm-core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_json") (r "^1.0.105") (f (quote ("alloc"))) (k 0)))) (h "14lwlgjhbsv1m0rjls9azv0c8bzkwgzncrl3xgpqp82qyfwmwbk7")))

