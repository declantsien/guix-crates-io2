(define-module (crates-io pu re purezen) #:use-module (crates-io))

(define-public crate-purezen-0.0.0 (c (n "purezen") (v "0.0.0") (h "0lfhmn42ikkr42n3cs7z3gcmslrw2qjgvfv6cy4n7hf4zzd138wr")))

(define-public crate-purezen-0.0.1 (c (n "purezen") (v "0.0.1") (d (list (d (n "failure") (r "^0.1") (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.11") (d #t) (k 0)) (d (n "heapless") (r "^0.4") (d #t) (k 0)) (d (n "typenum") (r "^1") (k 0)))) (h "01cgk5b8hk3ga0y3fch2k8vad3wnfy3pvrg275wbbvc7gi446v61") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-purezen-0.0.2 (c (n "purezen") (v "0.0.2") (d (list (d (n "failure") (r "^0.1") (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.11") (d #t) (k 0)) (d (n "heapless") (r "^0.4") (d #t) (k 0)) (d (n "typenum") (r "^1") (k 0)))) (h "14ljk9h4hjwdla1z4dyv0ns1nzh4mnpi5yb1cc6vcqa3nl667ygz") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

