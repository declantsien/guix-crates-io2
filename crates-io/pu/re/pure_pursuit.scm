(define-module (crates-io pu re pure_pursuit) #:use-module (crates-io))

(define-public crate-pure_pursuit-0.1.0 (c (n "pure_pursuit") (v "0.1.0") (d (list (d (n "array-concat") (r "^0.5.1") (d #t) (k 0)) (d (n "libc-print") (r "^0.1.17") (d #t) (k 2)) (d (n "libm") (r "^0.2.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1wmjviw6jyaghp80ms70cqgg7n1hbd9v2bsckpgqjgi0pkdfjw1a")))

(define-public crate-pure_pursuit-0.1.1 (c (n "pure_pursuit") (v "0.1.1") (d (list (d (n "libc-print") (r "^0.1.17") (d #t) (k 2)) (d (n "libm") (r "^0.2.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1xg0479jnjlq2hddvhlvnv3cvw5nnfjmxwx0jlfhhnfz5gkv9dnv")))

(define-public crate-pure_pursuit-0.1.2 (c (n "pure_pursuit") (v "0.1.2") (d (list (d (n "libc-print") (r "^0.1.17") (d #t) (k 2)) (d (n "libm") (r "^0.2.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (f (quote ("libm"))) (k 0)))) (h "00sn48v4bhk4s30dqwa2v8mmg3yi89d137z0bgjf7xrqkra0grkj")))

(define-public crate-pure_pursuit-0.1.3 (c (n "pure_pursuit") (v "0.1.3") (d (list (d (n "libc-print") (r "^0.1.17") (d #t) (k 2)) (d (n "libm") (r "^0.2.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (f (quote ("libm"))) (k 0)))) (h "1hg1saqj0lgga1262yxbrsdl6fp9fjgq7xv3cd162pbkdfs0nin9")))

(define-public crate-pure_pursuit-0.1.4 (c (n "pure_pursuit") (v "0.1.4") (d (list (d (n "libc-print") (r "^0.1.17") (d #t) (k 2)) (d (n "libm") (r "^0.2.2") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (f (quote ("libm"))) (k 0)))) (h "1y4js442acgy3c2zvdnxl6idjf26dayczbn2ydywbj0kaxi8548d")))

