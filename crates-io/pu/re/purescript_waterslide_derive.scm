(define-module (crates-io pu re purescript_waterslide_derive) #:use-module (crates-io))

(define-public crate-purescript_waterslide_derive-0.1.0 (c (n "purescript_waterslide_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.1.2") (d #t) (k 0)) (d (n "purescript_waterslide") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0q3fr758pq2kvmjglwkgg5gip1sgsbgmpm0xlj9qb7i1djd72l9d")))

(define-public crate-purescript_waterslide_derive-0.2.0 (c (n "purescript_waterslide_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.1.2") (d #t) (k 0)) (d (n "purescript_waterslide") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0fyjv8fp25a6piszgj9z8ny7y5kvqmxg0lgprxwkw5nzxm61mm1f")))

(define-public crate-purescript_waterslide_derive-0.2.1 (c (n "purescript_waterslide_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.1.2") (d #t) (k 0)) (d (n "purescript_waterslide") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1j823ihf38qj20qakyqh6s5qzaznlajip43l7133smqqmv89s7w4")))

(define-public crate-purescript_waterslide_derive-0.3.0 (c (n "purescript_waterslide_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.1.2") (d #t) (k 0)) (d (n "purescript_waterslide") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0m3z4xm44qd9vpqcb5d36jpjy4wrx0f5i8zy940mmbgxmj9hk793")))

(define-public crate-purescript_waterslide_derive-0.3.1 (c (n "purescript_waterslide_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.1.2") (d #t) (k 0)) (d (n "purescript_waterslide") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0n1ji8b30kcx8lpr2vaxxz1fqfdmdhrgi3vq5rxg25idcqkm2n6b")))

