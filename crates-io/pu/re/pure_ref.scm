(define-module (crates-io pu re pure_ref) #:use-module (crates-io))

(define-public crate-pure_ref-0.1.0 (c (n "pure_ref") (v "0.1.0") (h "1ipg2rhvnxr097z2inhrc74laa8v22qbgckw4cxmnmkyhf7y6hxc")))

(define-public crate-pure_ref-0.1.1 (c (n "pure_ref") (v "0.1.1") (h "1ryzw6gad35indld7294wpl61hy7p4rgkdwfbch81n55yl350mr9")))

(define-public crate-pure_ref-0.1.2 (c (n "pure_ref") (v "0.1.2") (h "0q7flrmzs4l6i055ckasj51gjakagaaic97i6ni3waibg6fhb08r")))

(define-public crate-pure_ref-0.1.3 (c (n "pure_ref") (v "0.1.3") (h "1ls9ncddqbjaqxcqp72pkb0451r083djj9544621wrv5qzkkphnz")))

