(define-module (crates-io pu re puremp3) #:use-module (crates-io))

(define-public crate-puremp3-0.1.0 (c (n "puremp3") (v "0.1.0") (d (list (d (n "bitstream-io") (r "^0.8.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "cpal") (r "^0.8") (d #t) (k 2)) (d (n "sample") (r "^0.10") (d #t) (k 2)))) (h "10yq20kk0m8bc6554gdr3xsssv7sfl9n3q1rq5qaywz376xyzdzj")))

