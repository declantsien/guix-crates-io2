(define-module (crates-io pu re purewasm-cbor) #:use-module (crates-io))

(define-public crate-purewasm-cbor-0.1.0 (c (n "purewasm-cbor") (v "0.1.0") (d (list (d (n "ciborium") (r "^0.2.1") (k 0)) (d (n "purewasm-core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive" "alloc"))) (k 0)))) (h "12fmg6v3k79qa4p1ymg28b7gdm3n089lwpmjsm96q8gjm6bgpxrc")))

