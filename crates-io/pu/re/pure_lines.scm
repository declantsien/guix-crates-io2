(define-module (crates-io pu re pure_lines) #:use-module (crates-io))

(define-public crate-pure_lines-0.1.0 (c (n "pure_lines") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "09850d6mr794vl9aldf6knf0ry2ngs3zyhcdr1vws53lhrsrq3cy")))

(define-public crate-pure_lines-0.2.0 (c (n "pure_lines") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1zfhady2ibssk1mvgwx113p0ip1r2y06sgx6pnsqrc4nb1f3znan") (f (quote (("quick"))))))

