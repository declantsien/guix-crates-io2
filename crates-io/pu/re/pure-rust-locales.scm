(define-module (crates-io pu re pure-rust-locales) #:use-module (crates-io))

(define-public crate-pure-rust-locales-0.1.0 (c (n "pure-rust-locales") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 1)) (d (n "nom") (r "^5.0") (d #t) (k 1)))) (h "08j79jkwlqmfz9brgmicpx7zwyj2hk1fwsk279p62wn3fx6zzmxc")))

(define-public crate-pure-rust-locales-0.2.0 (c (n "pure-rust-locales") (v "0.2.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 1)) (d (n "nom") (r "^5.0") (d #t) (k 1)))) (h "10qa6786142007csrlnzxf1jrw0qbhws4i1mmi5nz6r716gx6hg5")))

(define-public crate-pure-rust-locales-0.2.1 (c (n "pure-rust-locales") (v "0.2.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 1)) (d (n "nom") (r "^5.0") (d #t) (k 1)))) (h "0lhylri9gw5zjma5ycwa8sb9li5j45nc43a5k182pl42xcc9f96c")))

(define-public crate-pure-rust-locales-0.2.2 (c (n "pure-rust-locales") (v "0.2.2") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 1)) (d (n "nom") (r "^5.0") (d #t) (k 1)))) (h "0krkbxgghn3s1m82vb2yvjgp0171wpgpj0bgvkwdgfnv9vbq6gj4")))

(define-public crate-pure-rust-locales-0.3.0 (c (n "pure-rust-locales") (v "0.3.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 1)) (d (n "nom") (r "^5.0") (d #t) (k 1)))) (h "1qimzrh8a6cni5vdi9cdrr6r7rgs3ibwycyzw74habfk4kcgc5pf")))

(define-public crate-pure-rust-locales-0.4.0 (c (n "pure-rust-locales") (v "0.4.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 1)) (d (n "nom") (r "^5.0") (d #t) (k 1)))) (h "0lwzmzm6ry197llb0zfrnmiqnd70ixjxw6zl4mis2ahhy9q3s6wi")))

(define-public crate-pure-rust-locales-0.5.0 (c (n "pure-rust-locales") (v "0.5.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 1)) (d (n "nom") (r "^5.0") (d #t) (k 1)))) (h "08q6w1y5af8kp52w3rik5d6b5j66222xgbhmzb5f589wwf79vhgs")))

(define-public crate-pure-rust-locales-0.5.1 (c (n "pure-rust-locales") (v "0.5.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 1)) (d (n "nom") (r "^5.0") (d #t) (k 1)))) (h "02rb2dff0w31z0b356mxzbr3lmwx2nwk28pjhwqsyq40szqrkwby")))

(define-public crate-pure-rust-locales-0.5.2 (c (n "pure-rust-locales") (v "0.5.2") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 1)) (d (n "nom") (r "^5.0") (d #t) (k 1)))) (h "0vhyqi44xc3za938h3mazvarjqardjqmxkzimin999j7kr7x6msd")))

(define-public crate-pure-rust-locales-0.5.3 (c (n "pure-rust-locales") (v "0.5.3") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 1)) (d (n "nom") (r "^5.0") (d #t) (k 1)))) (h "0ryjj0gs4hfadqx9vl4sgi32zyb2dlvwpxca1m1kmrw9hk1g7gv5")))

(define-public crate-pure-rust-locales-0.5.4 (c (n "pure-rust-locales") (v "0.5.4") (h "17g0gjy6nxz71yn0j1l2yyvzjng7fxnqs3070kkx8w3dkz8gcwjg")))

(define-public crate-pure-rust-locales-0.5.5 (c (n "pure-rust-locales") (v "0.5.5") (h "0gmfmckk0lcvma3hm3rnysjc89w5z77x8bgf5vrm8yr3gd91xf1y")))

(define-public crate-pure-rust-locales-0.5.6 (c (n "pure-rust-locales") (v "0.5.6") (h "1n1jqy8g7ph9lvzncc8vy5jaqq2dljryp1agcnp5pwwi9zy4jp5l")))

(define-public crate-pure-rust-locales-0.6.0 (c (n "pure-rust-locales") (v "0.6.0") (h "0ssj0ysdc8pj61wwc4kcyc9294w5j6ydbplfsw8h1fwf09ynv1ab")))

(define-public crate-pure-rust-locales-0.7.0 (c (n "pure-rust-locales") (v "0.7.0") (h "0cl46srhxzj0jlvfp73l8l9qw54qwa04zywaxdf73hidwqlsh0pd")))

(define-public crate-pure-rust-locales-0.8.0 (c (n "pure-rust-locales") (v "0.8.0") (h "1plq4hl7291dvg99iklw7bi5phpbyykp91hzfb50iy5v9snav21m")))

(define-public crate-pure-rust-locales-0.8.1 (c (n "pure-rust-locales") (v "0.8.1") (h "0fkkwggiq2053rmiah2h06dz6w3yhy9pa82g30vy3sbcmqcgv40i") (r "1.56.0")))

