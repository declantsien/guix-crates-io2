(define-module (crates-io pu re purewasm-core) #:use-module (crates-io))

(define-public crate-purewasm-core-0.1.0 (c (n "purewasm-core") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive" "alloc"))) (k 0)))) (h "0vflhd2xqr1dzd81yh5qc2w1w83mx47g90wkyzryn3bmjl3zw3pw")))

