(define-module (crates-io in -o in-out) #:use-module (crates-io))

(define-public crate-in-out-0.1.0 (c (n "in-out") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hflxvxm5m66ygqpb48iwhqbfzr7iz4yjp3qmwz5ym5mlsfya9kk") (f (quote (("default") ("ctx"))))))

