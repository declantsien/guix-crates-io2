(define-module (crates-io in _s in_struct) #:use-module (crates-io))

(define-public crate-in_struct-1.0.0 (c (n "in_struct") (v "1.0.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "13if4rslf2kyn6zrhwk6jjr8b3y7lxcpdxmlyzwhrdx7h5wzb45i")))

(define-public crate-in_struct-1.0.1 (c (n "in_struct") (v "1.0.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "06w45axyzyjlq7dxdxlf4y438ix2ixc5qvnr147lvk9fkrsd92is")))

