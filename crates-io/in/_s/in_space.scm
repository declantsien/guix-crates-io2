(define-module (crates-io in _s in_space) #:use-module (crates-io))

(define-public crate-in_space-0.0.1 (c (n "in_space") (v "0.0.1") (d (list (d (n "in_space_routes") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "1fy2q4y7vd2f58wn11xd6bilyk2wwyrw19in8r6jvavsklygf6ys")))

(define-public crate-in_space-0.0.2 (c (n "in_space") (v "0.0.2") (d (list (d (n "in_space_routes") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "0y7n3g26bji2vr1jjzhg79k084iq4y3yfdmwzyzzn2jy4p7p1sg4")))

(define-public crate-in_space-0.0.4 (c (n "in_space") (v "0.0.4") (d (list (d (n "in_space_core") (r ">=0.0.4, <0.0.5") (d #t) (k 0)) (d (n "in_space_routes") (r ">=0.0.4, <0.0.5") (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 2)) (d (n "tokio") (r ">=0.2.0, <0.3.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "warp") (r ">=0.2.0, <0.3.0") (d #t) (k 0)))) (h "0gifc0qp243hhn4mbh0w2xs2q1qny1kmw2qjjyflxhk180nwvy5w")))

(define-public crate-in_space-0.0.5 (c (n "in_space") (v "0.0.5") (d (list (d (n "in_space_core") (r ">=0.0.5, <0.0.6") (d #t) (k 0)) (d (n "in_space_routes") (r ">=0.0.5, <0.0.6") (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 2)) (d (n "tokio") (r ">=0.2.0, <0.3.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "warp") (r ">=0.2.0, <0.3.0") (d #t) (k 0)))) (h "0ka7gmklrnasc4i3xfjakhawxdm7mkdqaj9ll9ycaqbq2kf60zkr")))

