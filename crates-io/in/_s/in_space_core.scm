(define-module (crates-io in _s in_space_core) #:use-module (crates-io))

(define-public crate-in_space_core-0.0.4 (c (n "in_space_core") (v "0.0.4") (d (list (d (n "warp") (r ">=0.2.0, <0.3.0") (d #t) (k 0)))) (h "16pdszyp96avk29zzkfvkhy3ck3mf5z4bfqy598am8rvp746g72l")))

(define-public crate-in_space_core-0.0.5 (c (n "in_space_core") (v "0.0.5") (d (list (d (n "warp") (r ">=0.2.0, <0.3.0") (d #t) (k 0)))) (h "0iv6y9rgn427acycacf6f3dpf0q6l60a7bj5g1fhlg3pxbggqms4")))

