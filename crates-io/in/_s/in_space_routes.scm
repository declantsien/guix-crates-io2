(define-module (crates-io in _s in_space_routes) #:use-module (crates-io))

(define-public crate-in_space_routes-0.0.1 (c (n "in_space_routes") (v "0.0.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "0c4cazc86kk60m2x5yiiwrsz0zj4iqnljm7l5dzsvzwl9k58pyfc")))

(define-public crate-in_space_routes-0.0.4 (c (n "in_space_routes") (v "0.0.4") (d (list (d (n "in_space_core") (r ">=0.0.4, <0.0.5") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "warp") (r ">=0.2.0, <0.3.0") (d #t) (k 0)))) (h "0mm02l4wwixmss2jkca2l2vnzhrn8g04xjrk61hzypsjdrmzrlz9")))

(define-public crate-in_space_routes-0.0.5 (c (n "in_space_routes") (v "0.0.5") (d (list (d (n "in_space_core") (r ">=0.0.5, <0.0.6") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "warp") (r ">=0.2.0, <0.3.0") (d #t) (k 0)))) (h "02xl2bs3s05gh7p6hb5p0sxwi77vgmrk20740bzy7d08irc38gk5")))

