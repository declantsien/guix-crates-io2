(define-module (crates-io in sy insync) #:use-module (crates-io))

(define-public crate-insync-0.1.0 (c (n "insync") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "196happp2fifg8agnd9psynx85qqp0gjl9vwcawwcv3z91kwsb58")))

