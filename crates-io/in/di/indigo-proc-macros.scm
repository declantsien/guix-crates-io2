(define-module (crates-io in di indigo-proc-macros) #:use-module (crates-io))

(define-public crate-indigo-proc-macros-0.1.0 (c (n "indigo-proc-macros") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pvfp7n7896hcy3hd23pgp972g18184gcqgrd7ml92wr4m7kdkjx") (f (quote (("logger") ("dotenv") ("default"))))))

(define-public crate-indigo-proc-macros-0.1.1 (c (n "indigo-proc-macros") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0017vr74jadf4npfmxs71cqikrhgi6rsz2fhcd3ck70yg1af7cxw") (f (quote (("logger") ("dotenv") ("default"))))))

