(define-module (crates-io in di indicato_rs) #:use-module (crates-io))

(define-public crate-indicato_rs-0.0.0 (c (n "indicato_rs") (v "0.0.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "indicato_rs_proc") (r "^0.1.0") (d #t) (k 0)))) (h "0dwz33wxspwfr99k2vp8dkwpb6di8ycxhqq8hjn8vyg5fry8cing") (y #t)))

(define-public crate-indicato_rs-0.1.0 (c (n "indicato_rs") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "indicato_rs_proc") (r "^0.1.0") (d #t) (k 0)))) (h "0fj6lb6i6jskd5ha19bj9jddnfgx8bf6ycqd85qx9khwz501lzw1") (y #t)))

(define-public crate-indicato_rs-0.1.1 (c (n "indicato_rs") (v "0.1.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "indicato_rs_proc") (r "^0.1.0") (d #t) (k 0)))) (h "05d043r9yp3c53ppnc414vaw2h49r8njfgilxack9r77d0plh4zx")))

(define-public crate-indicato_rs-0.1.2 (c (n "indicato_rs") (v "0.1.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "indicato_rs_proc") (r "^0.1.0") (d #t) (k 0)))) (h "06wbzmc84dvk0n7wba3s09f3npq5w0jam2jk4hpgf5x1nw54m5d0")))

(define-public crate-indicato_rs-0.1.3 (c (n "indicato_rs") (v "0.1.3") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "indicato_rs_proc") (r "^0.1.0") (d #t) (k 0)))) (h "1bzl2by2bmyllbl6pxawfhjpwk5ihqc0mvqz6bqmg1p025svv1mv")))

