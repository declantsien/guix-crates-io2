(define-module (crates-io in di indigo-structopt-derive) #:use-module (crates-io))

(define-public crate-indigo-structopt-derive-0.4.14 (c (n "indigo-structopt-derive") (v "0.4.14") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1f80qd072rp92phb8r3963aqjpgrbphmvxg89s57qdshcvh1br6s") (f (quote (("paw"))))))

