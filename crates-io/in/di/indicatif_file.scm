(define-module (crates-io in di indicatif_file) #:use-module (crates-io))

(define-public crate-indicatif_file-0.1.0 (c (n "indicatif_file") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)))) (h "081l7czzn6lffpq6d3wslz57rqwsydp18jy464n88kf7r2nbwmdb")))

(define-public crate-indicatif_file-0.1.1 (c (n "indicatif_file") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)))) (h "02r9yfqlqv83c96r7yr1xl03bfkdqdp25d1fp2q8wfjqhhbfwz93")))

