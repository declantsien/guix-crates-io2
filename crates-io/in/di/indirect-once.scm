(define-module (crates-io in di indirect-once) #:use-module (crates-io))

(define-public crate-indirect-once-0.1.0 (c (n "indirect-once") (v "0.1.0") (d (list (d (n "indirect-once-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0") (o #t) (d #t) (k 0)))) (h "00fl2dccbvgpba9dzmsvvhfjxswacfxwd41awkf3hin0kcqxx1kk") (f (quote (("parking-lot" "parking_lot" "indirect-once-derive/parking_lot") ("default"))))))

(define-public crate-indirect-once-0.1.1 (c (n "indirect-once") (v "0.1.1") (d (list (d (n "indirect-once-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0") (o #t) (d #t) (k 0)))) (h "0dl73gafgd9wf6ws8bcfsxmpbaiag4fyn6mxn37snpsdg0z48411") (f (quote (("parking-lot" "parking_lot" "indirect-once-derive/parking_lot") ("default"))))))

(define-public crate-indirect-once-0.2.0 (c (n "indirect-once") (v "0.2.0") (d (list (d (n "indirect-once-derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0") (o #t) (d #t) (k 0)))) (h "0p8ghzif5dc9d2xjarq8v72xmqmlj956ala0gm3y5fnplb4lhkh7") (f (quote (("proc-macro" "indirect-once-derive") ("parking-lot" "parking_lot" "indirect-once-derive/parking_lot") ("default" "parking-lot" "proc-macro"))))))

