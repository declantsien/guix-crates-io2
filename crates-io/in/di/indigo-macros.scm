(define-module (crates-io in di indigo-macros) #:use-module (crates-io))

(define-public crate-indigo-macros-0.1.0 (c (n "indigo-macros") (v "0.1.0") (h "18a53dzinqjpgrvgi1jibg4799arpzx6sj60kmxwnbjc7lqi028g")))

(define-public crate-indigo-macros-0.1.1 (c (n "indigo-macros") (v "0.1.1") (h "0xg47kpyd645hs2w0h4gvf3nv8p85mmjyf1li4s69jv3x8fvy6np")))

