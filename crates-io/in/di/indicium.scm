(define-module (crates-io in di indicium) #:use-module (crates-io))

(define-public crate-indicium-0.1.0 (c (n "indicium") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0y9fvi54lwyiahah43360f5iw4jbv1hdslmw8lfksgiv7h0v6jxm")))

(define-public crate-indicium-0.1.1 (c (n "indicium") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1pvjmiby4csfp0pnjs95n8gjzc47dvmhwxy4w128a0cir6hkn1wg")))

(define-public crate-indicium-0.1.2 (c (n "indicium") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "17hg5drmd7jlgj8d0wy4h7bawf2dvaaxapvs1c36aaj6ca2izqh8")))

(define-public crate-indicium-0.1.3 (c (n "indicium") (v "0.1.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0q7r9mkihrbiwfcc2qa90bm1dis09dznli0fmc999304cxilhldn")))

(define-public crate-indicium-0.1.4 (c (n "indicium") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1vgchjryzwr4w5cdsklykpfn45sfbf10nlxa86sgi68x1h95b3yh") (f (quote (("simple") ("select2" "serde") ("default" "simple" "select2"))))))

(define-public crate-indicium-0.1.5 (c (n "indicium") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0zhj73dqs6rizki1yii9l32kl0k8lk91l8cwyiqgs4mc0f5c658y") (f (quote (("simple") ("select2" "serde") ("default" "simple" "select2"))))))

(define-public crate-indicium-0.2.0 (c (n "indicium") (v "0.2.0") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0nlwd08mhadnsg8gyrwharvz5950dqjaqz6yq81pxgra82vm3cnc") (f (quote (("simple") ("select2" "serde") ("default" "rayon" "simple" "select2"))))))

(define-public crate-indicium-0.2.1 (c (n "indicium") (v "0.2.1") (h "1rk3hpak1rnak24v617b93ii66k9q3pnh6cc3w2znjpbrn619kvy")))

(define-public crate-indicium-0.2.2 (c (n "indicium") (v "0.2.2") (h "1xa7bgqng1pz0a1yfpr6zlzzaisdgpx47jfbx0bncr8mq7c0iw1j")))

(define-public crate-indicium-0.2.3 (c (n "indicium") (v "0.2.3") (h "1lh23ajnfkh20qkpavc01s90rb6sll4qma2wgndrm6pq9d2v9pbm")))

(define-public crate-indicium-0.2.4 (c (n "indicium") (v "0.2.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0kfhxqvx9j09z94yympvsr2lgfzmvbvf7pyhbxp7cfxh461gmlqi") (f (quote (("simple") ("default" "simple"))))))

(define-public crate-indicium-0.3.0 (c (n "indicium") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1z6i9hszlil1sissslxnhrz2nka5sc987wmzknrqnf7ylxwjn9jf") (f (quote (("simple") ("default" "simple"))))))

(define-public crate-indicium-0.3.1 (c (n "indicium") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0wfijhx3nkv3zpq8zjlaq1kfg4rfiq4d39m9jlfzair3brdbcwy2") (f (quote (("simple") ("select2" "simple" "serde") ("default" "simple"))))))

(define-public crate-indicium-0.3.2 (c (n "indicium") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1p9ncmyhw5flh6smvdmb6xvngka1dx3qj4r4vksp8bwidn9cnf8b") (f (quote (("simple") ("select2" "simple" "serde") ("default" "simple"))))))

(define-public crate-indicium-0.3.3 (c (n "indicium") (v "0.3.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1xbrhhfmps0narm3p02xlvbzz7d5bjax0w9q4fhpyx36c3xm7gvn") (f (quote (("simple") ("select2" "simple" "serde") ("default" "simple"))))))

(define-public crate-indicium-0.3.4 (c (n "indicium") (v "0.3.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1zaphdg83hbd4bsssiznba206i25igl4jskss2izg8i8xd7020z4") (f (quote (("simple") ("select2" "simple" "serde") ("default" "simple"))))))

(define-public crate-indicium-0.3.5 (c (n "indicium") (v "0.3.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "07w8n68k15zxbzyr5khdj7g1ziyyigba0x8r4cqj2la83694f78k") (f (quote (("simple") ("select2" "simple" "serde") ("default" "simple" "select2"))))))

(define-public crate-indicium-0.3.6 (c (n "indicium") (v "0.3.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "03jx0jjrsj1igh1k0ls947win8l08ljivlx6q1l4pzvv2a1nwckv") (f (quote (("simple") ("select2" "simple" "serde") ("default" "simple" "select2"))))))

(define-public crate-indicium-0.3.7 (c (n "indicium") (v "0.3.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1a67pmjgyim59vhzgw8yhzmqz3jm58bajpwfzhz836n0wp8qihm8") (f (quote (("simple") ("select2" "simple" "serde") ("default" "simple"))))))

(define-public crate-indicium-0.4.0 (c (n "indicium") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "strsim") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0fi3aimw8l2vq6vq6c6jk9dpn12bd976n1fggjfp3vahl6wpqw64") (f (quote (("simple") ("select2" "simple" "serde") ("fuzzy" "strsim") ("default" "simple" "fuzzy"))))))

(define-public crate-indicium-0.4.1 (c (n "indicium") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "strsim") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1r052k6mcgrh90207x9rbjs0k4i6v78mazkhkyh6i2f3vghf8and") (f (quote (("simple") ("select2" "simple" "serde") ("fuzzy" "strsim") ("default" "simple" "fuzzy"))))))

(define-public crate-indicium-0.4.2 (c (n "indicium") (v "0.4.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "strsim") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1zkrihpl7cwkp8144kgzfz8jl5f7sqg340scyp5qxjsjn6hhh2zf") (f (quote (("simple") ("select2" "simple" "serde") ("fuzzy" "strsim") ("default" "simple" "fuzzy"))))))

(define-public crate-indicium-0.4.3 (c (n "indicium") (v "0.4.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "strsim") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1qhfg7di32q7a570d96wi9i1l5sa92a81ma525q817d32xv4vscs") (f (quote (("simple") ("select2" "simple" "serde") ("fuzzy" "strsim") ("default" "simple" "fuzzy"))))))

(define-public crate-indicium-0.5.0 (c (n "indicium") (v "0.5.0") (d (list (d (n "kstring") (r "^2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "strsim") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "05cbgki3jwpfacmiz3pnri8a050xp8q4nny78yzn8vsbbf2hgfdy") (f (quote (("simple") ("select2" "simple" "serde") ("fuzzy" "strsim") ("default" "simple" "fuzzy")))) (y #t)))

(define-public crate-indicium-0.5.1 (c (n "indicium") (v "0.5.1") (d (list (d (n "kstring") (r "^2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "strsim") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1ssb4fp5q5l5wyz24arc195s65vf5hnakxfnhcn2parl8sqplk3k") (f (quote (("simple") ("select2" "simple" "serde") ("fuzzy" "strsim") ("default" "simple" "fuzzy"))))))

(define-public crate-indicium-0.5.2 (c (n "indicium") (v "0.5.2") (d (list (d (n "ahash") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "kstring") (r "^2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "strsim") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1bj4apbv6ld1cnfnbfqyc930l5n93bjbm721fnw0racxilsyx89s") (f (quote (("simple") ("select2" "simple" "serde") ("fuzzy" "strsim") ("default" "simple" "fuzzy" "ahash")))) (s 2) (e (quote (("ahash" "dep:ahash"))))))

(define-public crate-indicium-0.6.0 (c (n "indicium") (v "0.6.0") (d (list (d (n "ahash") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "eddie") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "gxhash") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "kstring") (r "^2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "strsim") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1idgqb40skfxw1vi0s9d09hr6l8rlmsm0lqq0hg3dfy7kf1f1j6h") (f (quote (("simple") ("select2" "simple" "serde") ("fuzzy" "eddie") ("default" "simple" "eddie" "ahash")))) (s 2) (e (quote (("strsim" "dep:strsim") ("gxhash" "dep:gxhash") ("eddie" "dep:eddie") ("ahash" "dep:ahash"))))))

(define-public crate-indicium-0.6.1 (c (n "indicium") (v "0.6.1") (d (list (d (n "ahash") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "eddie") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "gxhash") (r "^3.1") (o #t) (d #t) (k 0)) (d (n "kstring") (r "^2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "strsim") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "09vi3cgcdcmyva0amlakvhyi7x13b448d4wrn6ch61ms7qyjl3rw") (f (quote (("simple") ("select2" "simple" "serde") ("fuzzy" "eddie") ("default" "simple" "strsim" "ahash")))) (s 2) (e (quote (("strsim" "dep:strsim") ("gxhash" "dep:gxhash") ("eddie" "dep:eddie") ("ahash" "dep:ahash")))) (r "1.62.1")))

(define-public crate-indicium-0.6.2 (c (n "indicium") (v "0.6.2") (d (list (d (n "ahash") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "eddie") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "gxhash") (r "^3.1") (o #t) (d #t) (k 0)) (d (n "kstring") (r "^2.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "strsim") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1f8qmbi9id78s18p2nqv4ssn6d56jiazcdb45cbnmfl8zkzqbm6s") (f (quote (("simple") ("select2" "simple" "serde") ("fuzzy" "strsim") ("default" "simple" "strsim" "ahash")))) (s 2) (e (quote (("strsim" "dep:strsim") ("gxhash" "dep:gxhash") ("eddie" "dep:eddie") ("ahash" "dep:ahash")))) (r "1.62.1")))

