(define-module (crates-io in di indirect-once-derive) #:use-module (crates-io))

(define-public crate-indirect-once-derive-0.1.0 (c (n "indirect-once-derive") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0") (o #t) (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0bvr7xqawjrfi4vjv6v4c8i47ah4anmwhg46hixyl7429ipfz37f")))

(define-public crate-indirect-once-derive-0.1.1 (c (n "indirect-once-derive") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0") (o #t) (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19sxyyf7w9rzcmnxhf06v0mbhdv7v4rwgbw5nxqasnngh461v94h")))

(define-public crate-indirect-once-derive-0.2.0 (c (n "indirect-once-derive") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0") (o #t) (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ix7m727wzhp38xlrfg5ajvas4cpzyyfcz1p5j1ksqp8pciv87fz")))

