(define-module (crates-io in di indiscriminant_impl_byte_str) #:use-module (crates-io))

(define-public crate-indiscriminant_impl_byte_str-0.1.0 (c (n "indiscriminant_impl_byte_str") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (d #t) (k 0)))) (h "0jv5pnfimmfg845hnncqxvlhxwsd81amijc2pbg3fa4d0hs7zxvp")))

(define-public crate-indiscriminant_impl_byte_str-0.2.0 (c (n "indiscriminant_impl_byte_str") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (d #t) (k 0)))) (h "1m6508456xgyhqfpcrc7gr8f3g47dq2xh24rmqrx42jhprm40c85")))

