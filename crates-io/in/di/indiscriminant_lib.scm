(define-module (crates-io in di indiscriminant_lib) #:use-module (crates-io))

(define-public crate-indiscriminant_lib-0.1.0 (c (n "indiscriminant_lib") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (d #t) (k 0)))) (h "0m8sr4jzx73ghii5rql2g13r3f1hyf8x6arphkdps12liy6ckx8c")))

