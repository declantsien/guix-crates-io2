(define-module (crates-io in di indices) #:use-module (crates-io))

(define-public crate-indices-0.1.0 (c (n "indices") (v "0.1.0") (h "09f779aaa20lfb82whpmpzwbmsgz2yshyyaainka78q2pxhwxw67")))

(define-public crate-indices-0.2.0 (c (n "indices") (v "0.2.0") (d (list (d (n "trybuild") (r "^1.0.91") (d #t) (k 2)))) (h "1vxz2ng5gfcjg8hqmzikbijvhv1fj0gr1ipzvd71md0w4r1j40y2")))

(define-public crate-indices-0.2.1 (c (n "indices") (v "0.2.1") (d (list (d (n "trybuild") (r "^1.0.91") (d #t) (k 2)))) (h "0i7jyxjaq8ikyiahzs47bqymli2fvd6rd80az9byrsyyz7wl10iw")))

(define-public crate-indices-0.3.0 (c (n "indices") (v "0.3.0") (d (list (d (n "trybuild") (r "^1.0.91") (d #t) (k 2)))) (h "183sjah3jnxlnc46rvf8lv6cwad63qvkz4ch7ddry1vywkync6lw")))

(define-public crate-indices-0.3.1 (c (n "indices") (v "0.3.1") (d (list (d (n "trybuild") (r "^1.0.91") (d #t) (k 2)))) (h "0yqdzgr8ilfa5mbd8ajc0ikvnks2w40f9591158qngyffhdm4x3v")))

