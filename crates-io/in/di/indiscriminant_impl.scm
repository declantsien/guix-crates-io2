(define-module (crates-io in di indiscriminant_impl) #:use-module (crates-io))

(define-public crate-indiscriminant_impl-0.1.0 (c (n "indiscriminant_impl") (v "0.1.0") (d (list (d (n "indiscriminant_lib") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (d #t) (k 0)))) (h "0chl9ll5j30x11xjbp4xy3rhv3nnfaj7p3argv0a7kqrjwjvl8p7")))

