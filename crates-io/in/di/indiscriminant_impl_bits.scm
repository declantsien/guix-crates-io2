(define-module (crates-io in di indiscriminant_impl_bits) #:use-module (crates-io))

(define-public crate-indiscriminant_impl_bits-0.1.0 (c (n "indiscriminant_impl_bits") (v "0.1.0") (d (list (d (n "indiscriminant_impl_str") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (d #t) (k 0)))) (h "0980v3wsj42hxn8vgpf95w45qv400j3l52rx4r1l7gcazfb9dj50")))

(define-public crate-indiscriminant_impl_bits-0.2.0 (c (n "indiscriminant_impl_bits") (v "0.2.0") (d (list (d (n "indiscriminant_impl_str") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (d #t) (k 0)))) (h "1648a9s0ngz4q3ilx56ikzz31rlw9k9m5q4ma1y4xm0x1632cmbk")))

