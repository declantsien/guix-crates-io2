(define-module (crates-io in di indicator_derive) #:use-module (crates-io))

(define-public crate-indicator_derive-0.4.1-alpha.0 (c (n "indicator_derive") (v "0.4.1-alpha.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0wpva0pwgsjrkmv7rjfcv8vfh7bz1dsnxgvza4sw60z4awbvdis5") (r "1.67.0")))

