(define-module (crates-io in di inditech) #:use-module (crates-io))

(define-public crate-inditech-0.1.0 (c (n "inditech") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "csv") (r "^1.1.6") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "ringvec") (r "^0.1.0") (d #t) (k 0)))) (h "0dxylqw2vy9fmc1hqfj6h67zvvjpwl86myb63gdpb1c9r9wy36wg")))

