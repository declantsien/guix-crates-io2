(define-module (crates-io in di indie) #:use-module (crates-io))

(define-public crate-indie-0.1.0 (c (n "indie") (v "0.1.0") (h "1c5rrvwa24n31sww2slcxh4k6hwdcfka70njf3d3anln3m8n84j3") (y #t)))

(define-public crate-indie-0.1.1 (c (n "indie") (v "0.1.1") (h "0vn51ygd7npmlrrjpy5si7x8lihp4mckvcwvfgcj4xfpnzf2bmkw")))

(define-public crate-indie-0.1.2 (c (n "indie") (v "0.1.2") (h "054r1gac4hn9zg40p92gsja7ys18yvz59jvca0r4vzyj3686kic3")))

