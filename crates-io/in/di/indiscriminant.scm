(define-module (crates-io in di indiscriminant) #:use-module (crates-io))

(define-public crate-indiscriminant-0.1.0 (c (n "indiscriminant") (v "0.1.0") (d (list (d (n "indiscriminant_impl_bits") (r "^0.1.0") (d #t) (k 0)) (d (n "indiscriminant_impl_byte_str") (r "^0.1.0") (d #t) (k 0)) (d (n "indiscriminant_impl_str") (r "^0.1.0") (d #t) (k 0)))) (h "0ybpa2mq5jxacnlnh9qgy4xvg3j182nffv9gjpagg26yck9a4zh8")))

(define-public crate-indiscriminant-0.2.0 (c (n "indiscriminant") (v "0.2.0") (d (list (d (n "indiscriminant_impl") (r "^0.1.0") (d #t) (k 0)))) (h "1ki5jg1saj5amq5cxnl1lzc4r0g0npb2k4d0dwfgh7h8h7467ddx")))

