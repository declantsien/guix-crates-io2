(define-module (crates-io in di indicatif-log-bridge) #:use-module (crates-io))

(define-public crate-indicatif-log-bridge-0.1.0 (c (n "indicatif-log-bridge") (v "0.1.0") (d (list (d (n "indicatif") (r ">=0.17.5") (d #t) (k 0)) (d (n "log") (r ">=0.4.19") (f (quote ("std"))) (d #t) (k 0)))) (h "017rhck2mbv16qhd6wjqw2nmv4hs7c8pj2ayxxrh8ira0732s8l5")))

(define-public crate-indicatif-log-bridge-0.2.0 (c (n "indicatif-log-bridge") (v "0.2.0") (d (list (d (n "env_logger") (r ">=0.10.0") (d #t) (k 2)) (d (n "indicatif") (r ">=0.17.5") (d #t) (k 0)) (d (n "log") (r ">=0.4.19") (f (quote ("std"))) (d #t) (k 0)))) (h "1lqiwajd3a5a7ia1x4pwygm5jd8qm123canmv4bsh9xslnd4pr75")))

(define-public crate-indicatif-log-bridge-0.2.1 (c (n "indicatif-log-bridge") (v "0.2.1") (d (list (d (n "env_logger") (r ">=0.10.0") (d #t) (k 2)) (d (n "indicatif") (r ">=0.17.5") (d #t) (k 0)) (d (n "log") (r ">=0.4.19") (f (quote ("std"))) (d #t) (k 0)))) (h "0m5rcvsxc4yxqj07sw8s286p3hap5l9b34c69nyy3j0p7gp3vd10")))

(define-public crate-indicatif-log-bridge-0.2.2 (c (n "indicatif-log-bridge") (v "0.2.2") (d (list (d (n "env_logger") (r ">=0.10.0") (d #t) (k 2)) (d (n "indicatif") (r ">=0.17.5") (d #t) (k 0)) (d (n "log") (r ">=0.4.19") (f (quote ("std"))) (d #t) (k 0)))) (h "1qswf89rbv7hgxq293rpnl2xldb21bclyxbyzpiy615251ph8qr9")))

