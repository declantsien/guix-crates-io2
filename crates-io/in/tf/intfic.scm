(define-module (crates-io in tf intfic) #:use-module (crates-io))

(define-public crate-intfic-0.3.0 (c (n "intfic") (v "0.3.0") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "ron") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "19bs9a8565argmg7x95gbh8h7h129pp302v11vqknk6v4jjm5nsj")))

(define-public crate-intfic-0.3.1 (c (n "intfic") (v "0.3.1") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "ron") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "1zxi6nd5x8a2q4qyvwzlyf4bbjlai5bqpwxwp992azrndl8spakl")))

(define-public crate-intfic-0.3.2 (c (n "intfic") (v "0.3.2") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "ron") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "09qlybf2sdcj6ld17pplb1zqfhnnfwjpmic4ifhg4dz9ph8zrwrq")))

(define-public crate-intfic-0.3.3 (c (n "intfic") (v "0.3.3") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "ron") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "01290jfrnyj5yryhq8pq6rkk3d0zmg71aw9zxpp2qkqijl3hvbmh")))

(define-public crate-intfic-0.3.4 (c (n "intfic") (v "0.3.4") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "ron") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "01bkxi3lyaqjjgjvchcllm69yd9sh7cgydm034shand65s4a3lpd")))

(define-public crate-intfic-0.3.5 (c (n "intfic") (v "0.3.5") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "ron") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "0rrj90g9z1xgv9b6ccjwpwrhp2bxj58m0xf0zjnk4r3f7lq5bfcv")))

(define-public crate-intfic-0.3.6 (c (n "intfic") (v "0.3.6") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "ron") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "0bmfcwgycb52nnrv66291ank6l4k0cls5grva9d5xf4zm1d1ag3q")))

(define-public crate-intfic-0.3.7 (c (n "intfic") (v "0.3.7") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "ron") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "1p7gxbxzlkk4rc3rzd4qq5vn765fbwc4x4sfqvd47qzp0rgh10pm")))

(define-public crate-intfic-0.3.8 (c (n "intfic") (v "0.3.8") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "ron") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (d #t) (k 0)) (d (n "text_io") (r "^0.1.8") (d #t) (k 0)))) (h "0aj7px9195hhlb8jn64y57bipqzkh060hpz89kah48s3ml4njmd2")))

