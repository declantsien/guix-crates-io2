(define-module (crates-io in ve invelion) #:use-module (crates-io))

(define-public crate-invelion-0.1.0 (c (n "invelion") (v "0.1.0") (d (list (d (n "bitreader") (r "^0.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.1") (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 0)))) (h "0xrqph9d2yiyd4lx3nngz1vznqdhrv73p31w8fzf92b5n5fhjh1l")))

(define-public crate-invelion-0.1.1 (c (n "invelion") (v "0.1.1") (d (list (d (n "bitreader") (r "^0.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.1") (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 0)))) (h "14wq6wc191gv6wd3j2l0hhivzghaws8bdj3fkbz85rsa6jydmb1l")))

(define-public crate-invelion-0.1.2 (c (n "invelion") (v "0.1.2") (d (list (d (n "bitreader") (r "^0.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.1") (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 0)))) (h "14zj9r0lgqqfaxqlvkmyhkgksz3n4jb4jvhwa2hn37hv647vzcbn")))

(define-public crate-invelion-0.1.3 (c (n "invelion") (v "0.1.3") (d (list (d (n "bitreader") (r "^0.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.1") (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 0)))) (h "1msb3fbvrb6qrz51jwqs5wd6gri2mcvm4sigj6nypips5yp2zzm5")))

