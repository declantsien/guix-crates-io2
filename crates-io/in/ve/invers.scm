(define-module (crates-io in ve invers) #:use-module (crates-io))

(define-public crate-invers-0.1.0-alpha.1 (c (n "invers") (v "0.1.0-alpha.1") (h "17kqwfhbqyhswwdb8wl8qagdqr1qnlggx01bnbk6fzy4jdrs3xvn") (y #t)))

(define-public crate-invers-0.0.1-alpha.1 (c (n "invers") (v "0.0.1-alpha.1") (h "1ig0ni9v3iwxm1lizarkx7nv66lb4x2yxvpi31xgpqwdipvxpalv")))

