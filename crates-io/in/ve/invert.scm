(define-module (crates-io in ve invert) #:use-module (crates-io))

(define-public crate-invert-0.1.0 (c (n "invert") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1rpzjpdki6wn3d181mafg86a40i75im7km8xddzh0mh1b0qdf2wx")))

(define-public crate-invert-0.2.0 (c (n "invert") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0ch7nqxvnac3x49xb1j5x941s48vg0vbnazyaal6wnbv1zkdkarm")))

(define-public crate-invert-0.2.1 (c (n "invert") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "052c9gdnwy12hcwk71yj8k06i1v5jm4z1w6r2q3aj1y06qk8pxzd")))

