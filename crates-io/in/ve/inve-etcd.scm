(define-module (crates-io in ve inve-etcd) #:use-module (crates-io))

(define-public crate-inve-etcd-0.0.1 (c (n "inve-etcd") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)) (d (n "tonic") (r "^0.6") (d #t) (k 0)) (d (n "tonic-build") (r "^0.6") (d #t) (k 1)))) (h "1vdsz819mgbcixiiirm0gp8ix8a2dkyy7qq3dx1gj55gns9acfxw") (f (quote (("tls" "tonic/tls" "tokio/fs") ("default" "tls"))))))

