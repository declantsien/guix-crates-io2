(define-module (crates-io in ve inverted-index-util) #:use-module (crates-io))

(define-public crate-inverted-index-util-0.0.1 (c (n "inverted-index-util") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rusty_ulid") (r "^0.9.3") (d #t) (k 2)))) (h "0x5brkdcryhbr1a5dz43yn2cz8x85srygclh3412cvxxvqcq2j20") (y #t)))

(define-public crate-inverted-index-util-0.0.2 (c (n "inverted-index-util") (v "0.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rusty_ulid") (r "^0.9.3") (d #t) (k 2)))) (h "0qhydif3a0ziy03mlszpv0m1nvynpiqm50r9r10i0pnppq7xz6aj")))

(define-public crate-inverted-index-util-0.0.3 (c (n "inverted-index-util") (v "0.0.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rusty_ulid") (r "^0.9.3") (d #t) (k 2)))) (h "1hyvk2i5j1jdy3bxxcqf8s4amxjln3wla8s67h0di3fvn33d5bjg")))

(define-public crate-inverted-index-util-0.0.4 (c (n "inverted-index-util") (v "0.0.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rusty_ulid") (r "^0.9.3") (d #t) (k 2)) (d (n "typenum") (r "^1.11.2") (d #t) (k 0)))) (h "0b7kakznpvxq2pr13wgzhmq09iqm6pcqlyhchx8a0gxidc3skw8r") (y #t)))

(define-public crate-inverted-index-util-0.0.5 (c (n "inverted-index-util") (v "0.0.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rusty_ulid") (r "^0.9.3") (d #t) (k 2)) (d (n "typenum") (r "^1.11.2") (d #t) (k 0)))) (h "0fz93xf2ycdlg00njqash65a2hn0k2s9026h1pqbn5s2qzbq8g5x")))

