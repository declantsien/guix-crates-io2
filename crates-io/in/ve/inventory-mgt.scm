(define-module (crates-io in ve inventory-mgt) #:use-module (crates-io))

(define-public crate-inventory-mgt-0.1.0 (c (n "inventory-mgt") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "csv") (r "^1.0.5") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1.8") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.87") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1mc6xz1gzfnhrj5kcj8jc2xr1999nz9njb97wcf2nw609m5rkdsa")))

(define-public crate-inventory-mgt-0.1.1 (c (n "inventory-mgt") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "csv") (r "^1.0.5") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1.8") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.87") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1arfj0vz36bkqzvmxq61c6pj59wqk0f95dhznqcr5yg8qb6gl848")))

