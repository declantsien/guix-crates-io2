(define-module (crates-io in ve inventory-rs) #:use-module (crates-io))

(define-public crate-inventory-rs-0.1.0 (c (n "inventory-rs") (v "0.1.0") (h "06dv0q8gvfsc2kzxw8k081gqnsavqrnfas8ybv3sawbfbm9afkgd") (y #t)))

(define-public crate-inventory-rs-0.1.1 (c (n "inventory-rs") (v "0.1.1") (h "0zgqlpn5717knjy5d79z5xiqvj1zy36kpj7p61ibfhpmlq66ifbp") (y #t)))

