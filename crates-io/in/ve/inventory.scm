(define-module (crates-io in ve inventory) #:use-module (crates-io))

(define-public crate-inventory-0.0.1 (c (n "inventory") (v "0.0.1") (h "0v6z6pn5h3vxwwd1qp0gx1cb1ky5xmjhbidx7wpmwarlssdfg8rj")))

(define-public crate-inventory-0.1.0 (c (n "inventory") (v "0.1.0") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "ghost") (r "^0.1") (d #t) (k 0)) (d (n "inventory-impl") (r "^0.1") (d #t) (k 0)))) (h "0yi8jw5gi2li7dpg0d66dkr78csqb4mxghl57frf1l0b38vldxj4")))

(define-public crate-inventory-0.1.1 (c (n "inventory") (v "0.1.1") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "ghost") (r "^0.1") (d #t) (k 0)) (d (n "inventory-impl") (r "^0.1.1") (d #t) (k 0)))) (h "1qc78m9n00qd43i76a2z2bzl226kkqn3c6f7nvxfgxxry7zqmvwq")))

(define-public crate-inventory-0.1.2 (c (n "inventory") (v "0.1.2") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "ghost") (r "^0.1") (d #t) (k 0)) (d (n "inventory-impl") (r "^0.1.2") (d #t) (k 0)))) (h "1livg7vhlp787wimb563c6lk4wrdqrira41wikcwx9hsbxh99bbf")))

(define-public crate-inventory-0.1.3 (c (n "inventory") (v "0.1.3") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "ghost") (r "^0.1") (d #t) (k 0)) (d (n "inventory-impl") (r "= 0.1.3") (d #t) (k 0)))) (h "08nr2pk8nfdpb73gyshk3w6sxl8gvk9j6xr6q85li5703yc8bpr1")))

(define-public crate-inventory-0.1.4 (c (n "inventory") (v "0.1.4") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "ghost") (r "^0.1") (d #t) (k 0)) (d (n "inventory-impl") (r "= 0.1.4") (d #t) (k 0)))) (h "168jlxj6j024z1sljh7yqn8z0ra7vydbwysy8grxjwgap8hcxkpl")))

(define-public crate-inventory-0.1.5 (c (n "inventory") (v "0.1.5") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "ghost") (r "^0.1.1") (d #t) (k 0)) (d (n "inventory-impl") (r "= 0.1.5") (d #t) (k 0)))) (h "09vs5hdxn19n55806lj1g147wazjjyffy2fc19acplhv12b85y9b")))

(define-public crate-inventory-0.1.6 (c (n "inventory") (v "0.1.6") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "ghost") (r "^0.1.1") (d #t) (k 0)) (d (n "inventory-impl") (r "= 0.1.6") (d #t) (k 0)))) (h "0miry7yx0ycizdrfvqafr3kj8qqcv86wcy3ln6bmqwl70awz9lw2")))

(define-public crate-inventory-0.1.7 (c (n "inventory") (v "0.1.7") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "ghost") (r "^0.1.1") (d #t) (k 0)) (d (n "inventory-impl") (r "=0.1.7") (d #t) (k 0)))) (h "0a0365yipgdqzfr6dadfnnr8va8fiai23y3ipnqd73wnfv0m06v2")))

(define-public crate-inventory-0.1.8 (c (n "inventory") (v "0.1.8") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "ghost") (r "^0.1.1") (d #t) (k 0)) (d (n "inventory-impl") (r "=0.1.8") (d #t) (k 0)))) (h "1jq0wavrzns0mf7306ia5fxabv249ydjlnzmqqys7gdir2lqvij9")))

(define-public crate-inventory-0.1.9 (c (n "inventory") (v "0.1.9") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "ghost") (r "^0.1.1") (d #t) (k 0)) (d (n "inventory-impl") (r "=0.1.9") (d #t) (k 0)))) (h "1pd0agvm1cjh4r2xcz63x25191q6848nch016xhn7hnq4kg4kpgy")))

(define-public crate-inventory-0.1.10 (c (n "inventory") (v "0.1.10") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "ghost") (r "^0.1.1") (d #t) (k 0)) (d (n "inventory-impl") (r "=0.1.10") (d #t) (k 0)))) (h "0zzz5sgrkxv1rpim4ihaidzf6jgha919xm4svcrmxjafh3xpw3qg")))

(define-public crate-inventory-0.1.11 (c (n "inventory") (v "0.1.11") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "ghost") (r "^0.1.1") (d #t) (k 0)) (d (n "inventory-impl") (r "=0.1.11") (d #t) (k 0)))) (h "1m1zdjgs3nzfdxf86mc1i0id56fvk8rfw63rf04yi88bqrh53szh") (r "1.31")))

(define-public crate-inventory-0.2.0 (c (n "inventory") (v "0.2.0") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "ghost") (r "^0.1.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0y3nxdbjsxpp3gmpayhn3xiqbfq3hd9339k7p6yabwhgfpbgwrqk") (r "1.37")))

(define-public crate-inventory-0.2.1 (c (n "inventory") (v "0.2.1") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "ghost") (r "^0.1.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0732zzlbmkcnf1bqrzmwbdjhkdawxfi3ncg0g2p5fr7xqpdnybxa") (r "1.37")))

(define-public crate-inventory-0.2.2 (c (n "inventory") (v "0.2.2") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "ghost") (r "^0.1.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "183j9zsqzypknql9krbp4yzzv76gqshxgpcm3n0svywvcs65ssyf") (r "1.37")))

(define-public crate-inventory-0.2.3 (c (n "inventory") (v "0.2.3") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "ghost") (r "^0.1.4") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "06h9xw67wx18rank4yyz93iq89j0fk6fbazryfvf5ach1dp4qd44") (r "1.37")))

(define-public crate-inventory-0.3.0 (c (n "inventory") (v "0.3.0") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "ghost") (r "^0.1.4") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "16nwr47f5wxm5623vv3bjdsqh1asvnsns84rchlvmnliihk7w9g0") (r "1.62")))

(define-public crate-inventory-0.3.1 (c (n "inventory") (v "0.3.1") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "ghost") (r "^0.1.4") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0cwikidqa2x32csw30glfby7lywqn40sfnvj5pw9cyfq060ip9ih") (r "1.62")))

(define-public crate-inventory-0.3.2 (c (n "inventory") (v "0.3.2") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "ghost") (r "^0.1.4") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0mbgihkm8yb32dm6srbmfrillgrix0azf5zf4aa4cjywlhv0l7p2") (r "1.62")))

(define-public crate-inventory-0.3.3 (c (n "inventory") (v "0.3.3") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "ghost") (r "^0.1.4") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1dd2wgfjcs7aadpgbj229zvn6brsazkjar7h2wlzglabsqskpzhn") (r "1.62")))

(define-public crate-inventory-0.3.4 (c (n "inventory") (v "0.3.4") (d (list (d (n "ctor") (r "^0.1") (d #t) (k 0)) (d (n "ghost") (r "^0.1.4") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "16fvcmdk8kq3rbq1qbqr75l3hq3sandj61kmj4mrgir9qg4y32j9") (r "1.62")))

(define-public crate-inventory-0.3.5 (c (n "inventory") (v "0.3.5") (d (list (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "ghost") (r "^0.1.4") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1lsp86f0agblywirkw5yyg46nsqixfj7gyy0fz72i6vadld30hbp") (r "1.62")))

(define-public crate-inventory-0.3.6 (c (n "inventory") (v "0.3.6") (d (list (d (n "ghost") (r "^0.1.4") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0i76vhdinwabnzwamjaq3qan0d9ik5rvmc6ndg784594x5frnlz0") (r "1.62")))

(define-public crate-inventory-0.3.7 (c (n "inventory") (v "0.3.7") (d (list (d (n "ghost") (r "^0.1.4") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0zb43yy5cwglsgrjnnrksshdjcsprfa59dg4kkg99c0c37cfb1nf") (r "1.62")))

(define-public crate-inventory-0.3.8 (c (n "inventory") (v "0.3.8") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1yd0qljqk29vkgpgac1vnigs44li8sd029jbrlrj8xg2w2hqg2n3") (r "1.62")))

(define-public crate-inventory-0.3.9 (c (n "inventory") (v "0.3.9") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1sm9armh80k2p2ak7jri56iq0l6zi6v9iyfykd0zqxgvp6sddc95") (r "1.62")))

(define-public crate-inventory-0.3.10 (c (n "inventory") (v "0.3.10") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0dbz8nbfmmrbkx28cd3ryqw36g5ssv701l4sf6s6idingfr1mh2r") (r "1.62")))

(define-public crate-inventory-0.3.11 (c (n "inventory") (v "0.3.11") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0fz60yxnvdz57z9ggdzq3c5qlyqyxagcp8kj6d7rs77pgk48hc55") (r "1.62")))

(define-public crate-inventory-0.3.12 (c (n "inventory") (v "0.3.12") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "13mcyn6adlkg42pm9fjl6czxvd4xm2768alrjig5kw0b8463igp1") (r "1.62")))

(define-public crate-inventory-0.3.13 (c (n "inventory") (v "0.3.13") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0g6qv0g3arj294p168m4vixz58pxmfkbjan2n3z5vzcvzrnca205") (r "1.62")))

(define-public crate-inventory-0.3.14 (c (n "inventory") (v "0.3.14") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1iyckc52afd5d1ni819kxzz7g1l8bzdg88rvqxra6hxn3wmknmy8") (r "1.62")))

(define-public crate-inventory-0.3.15 (c (n "inventory") (v "0.3.15") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.89") (f (quote ("diff"))) (d #t) (k 2)))) (h "0rspmi9qxz9hkajg4dx5hhwmcd3n3qw107hl3050hrs1izbd6n7r") (r "1.62")))

