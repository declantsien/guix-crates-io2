(define-module (crates-io in ve inventory-impl) #:use-module (crates-io))

(define-public crate-inventory-impl-0.1.0 (c (n "inventory-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1f4vzaidmgg75bpiqry5fmplrpw9l4x3dba27ggkk2mm3qdz8z4s")))

(define-public crate-inventory-impl-0.1.1 (c (n "inventory-impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "02m7cxjwbgsr022z6qljxz0gfv3qwgkgj4fa1dzdb4f2jdczsidw")))

(define-public crate-inventory-impl-0.1.2 (c (n "inventory-impl") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0d0ird5yw7ildd4i13d8l2gq1j4wa50nvsa5gq5hjdbz4pmhbdv5")))

(define-public crate-inventory-impl-0.1.3 (c (n "inventory-impl") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1akmdsfzln0r6qmr6akvhanpl2cyjcq0g1zdx7ah4x77pkl7m1wa")))

(define-public crate-inventory-impl-0.1.4 (c (n "inventory-impl") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1yfs9aj1xfgy6ksj626aipj4h79lf3gn0zmq3ixrg679fbwrp1n2")))

(define-public crate-inventory-impl-0.1.5 (c (n "inventory-impl") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0x02bhwri91h3yysa4m2s06npyxjf1mi6fcsdaiywa7yb9bk13ha")))

(define-public crate-inventory-impl-0.1.6 (c (n "inventory-impl") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1v40bry9857ff4qmc6f40hdpsvha301py4zghy93wl4xzkza94lh")))

(define-public crate-inventory-impl-0.1.7 (c (n "inventory-impl") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wyvc9bq2vh0ix71rpn4wi7nw8kw92rylnyhd1jfgbab608l36pr")))

(define-public crate-inventory-impl-0.1.8 (c (n "inventory-impl") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0k1mq35yvkh0hd82gadgdkhckq3sbb7rag1f51vibrcs71xh0hs1")))

(define-public crate-inventory-impl-0.1.9 (c (n "inventory-impl") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1njn48pjg0r45v170s4g9sxp09grdxzrm1mmsgy5f3y51f4disnx")))

(define-public crate-inventory-impl-0.1.10 (c (n "inventory-impl") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lgs8kia3284s34g7078j820cn2viyb6cij86swklwhn93lr9h3m")))

(define-public crate-inventory-impl-0.1.11 (c (n "inventory-impl") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0j45a7nq4vircnz5m23db34527icxqnvh2si96zc9w662lvvahby") (r "1.31")))

