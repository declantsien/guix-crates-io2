(define-module (crates-io in ve inverted-pin) #:use-module (crates-io))

(define-public crate-inverted-pin-0.1.0 (c (n "inverted-pin") (v "0.1.0") (d (list (d (n "dummy-pin") (r "^0.1.1") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)))) (h "0bd3ndxfbcwscvivqlkmhipw9qdgsvyi7n7m94nbyp6kagivvfiy")))

(define-public crate-inverted-pin-0.2.0 (c (n "inverted-pin") (v "0.2.0") (d (list (d (n "dummy-pin") (r "^0.1.1") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.7") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 2)))) (h "1xjghvgjs17xr06mbkbm4rmpvmwg4l6594bav4bz4c3ifkdacv39")))

(define-public crate-inverted-pin-1.0.0 (c (n "inverted-pin") (v "1.0.0") (d (list (d (n "dummy-pin") (r "^1.0.0") (d #t) (k 2)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (f (quote ("eh1"))) (k 2)) (d (n "linux-embedded-hal") (r "^0.4") (d #t) (k 2)))) (h "15zwri79s97inziw3ry6jrqkiaplzh0nbqp4ah4n3h35fsap9b88")))

