(define-module (crates-io in ve inverse_distance_weight) #:use-module (crates-io))

(define-public crate-inverse_distance_weight-0.1.0 (c (n "inverse_distance_weight") (v "0.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1d8m52k21ivz74qzxkpjjaym04zyz6qb1nlrsagwbsq7wrmzd2k2")))

(define-public crate-inverse_distance_weight-0.1.1 (c (n "inverse_distance_weight") (v "0.1.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1c69fchdiqsb3yc4kddx0sm6j2nf60jdqi6kxmx05l0ay6ys21av")))

