(define-module (crates-io in ve inventorize) #:use-module (crates-io))

(define-public crate-inventorize-0.1.0 (c (n "inventorize") (v "0.1.0") (d (list (d (n "clap") (r "^2") (k 0)) (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "md-5") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.2") (d #t) (k 0)))) (h "0rfvcyj7y1wvvdxa57j16pbfasl37dvg6dyb5g5zy7ppp86i4yrg")))

(define-public crate-inventorize-0.1.1 (c (n "inventorize") (v "0.1.1") (d (list (d (n "clap") (r "^2") (k 0)) (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 0)) (d (n "md-5") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha-1") (r "^0.9.2") (d #t) (k 0)))) (h "1zxazdikn8chqw9gvhrmg98ip4lq3sxjkb4cqlvqldf8jsq4ylqq")))

