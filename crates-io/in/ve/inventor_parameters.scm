(define-module (crates-io in ve inventor_parameters) #:use-module (crates-io))

(define-public crate-inventor_parameters-0.1.0 (c (n "inventor_parameters") (v "0.1.0") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "rust_xlsxwriter") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "174f526d4xydwclahsjl73ynfp75mdwiz3q0aa6nyhg2y1lnkgf5")))

(define-public crate-inventor_parameters-0.1.1 (c (n "inventor_parameters") (v "0.1.1") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "rust_xlsxwriter") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nc17398d3rzzcvr45hn1ppkbh699y2p7fs8qfkk7bkhx73bidry")))

