(define-module (crates-io in do indoc) #:use-module (crates-io))

(define-public crate-indoc-0.1.0 (c (n "indoc") (v "0.1.0") (h "15zaq94m5xm4bkqv63x0j75j5m7n0dclnc2gf5vcyq75p0kzv8mn")))

(define-public crate-indoc-0.1.1 (c (n "indoc") (v "0.1.1") (h "03c7i574b83777r6wjxg3b3lxs35wf9g87rqc0yll81j562914z8")))

(define-public crate-indoc-0.1.2 (c (n "indoc") (v "0.1.2") (d (list (d (n "syntex") (r "^0.30") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.30") (o #t) (d #t) (k 0)))) (h "13bphxxmgacmfmzspxj89ycph20rggcvmam8i64vdy8ani4r1szm") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("default"))))))

(define-public crate-indoc-0.1.3 (c (n "indoc") (v "0.1.3") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.1") (d #t) (k 2)) (d (n "syntex") (r "^0.30") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.30") (o #t) (d #t) (k 0)))) (h "0l7ysiwnhchjm7zaygq3qilsijvwl6h5z06qzzw5ll8wk2xffrsv") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("default"))))))

(define-public crate-indoc-0.1.4 (c (n "indoc") (v "0.1.4") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.1") (d #t) (k 2)) (d (n "syntex") (r "^0.30") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.30") (o #t) (d #t) (k 0)))) (h "0piv96grbayqjqf90q30zi3ix8xfivy1zb9c2655dyz4f5brwrqq") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("default"))))))

(define-public crate-indoc-0.1.5 (c (n "indoc") (v "0.1.5") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.1") (d #t) (k 2)) (d (n "syntex") (r "^0.30") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.30") (o #t) (d #t) (k 0)))) (h "1j847dgbraql712zy80x4m4k2nn1nbn6yachyypkapiz2jjli95k") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("default"))))))

(define-public crate-indoc-0.1.6 (c (n "indoc") (v "0.1.6") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.1") (d #t) (k 2)) (d (n "syntex") (r "^0.30") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.30") (o #t) (d #t) (k 0)))) (h "1isv5fykp4hzhy8l46mkhnn29cvg996hybijlbf7g0z3vmxm16n1") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("default"))))))

(define-public crate-indoc-0.1.7 (c (n "indoc") (v "0.1.7") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.1") (d #t) (k 2)) (d (n "syntex") (r "^0.31") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.31") (o #t) (d #t) (k 0)))) (h "0f7wvxpy27r83z7f1fzn7awa0rd24b6l6nxhgj3m30g9w0aq60sy") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("default"))))))

(define-public crate-indoc-0.1.8 (c (n "indoc") (v "0.1.8") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.1") (d #t) (k 2)) (d (n "syntex") (r "^0.32") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.32") (o #t) (d #t) (k 0)))) (h "0fdz2szg578fxih8iqzr59rz7ms9v0zv9mapjvy7xlfhqwbkr8g3") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("default"))))))

(define-public crate-indoc-0.1.9 (c (n "indoc") (v "0.1.9") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.1") (d #t) (k 2)) (d (n "syntex") (r "^0.33") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.33") (o #t) (d #t) (k 0)))) (h "0p4rhk2xk9knmqv9ah6cyinqci3f9abgrn8vkwzx6wyrknk1s6kl") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("default"))))))

(define-public crate-indoc-0.1.10 (c (n "indoc") (v "0.1.10") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.1") (d #t) (k 2)) (d (n "syntex") (r "^0.33") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.33") (o #t) (d #t) (k 0)))) (h "0hy6scqbps4w2f10jzmaaaq4fx21r9mk2arhphx7s1i9az9cn36a") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("default"))))))

(define-public crate-indoc-0.1.11 (c (n "indoc") (v "0.1.11") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.1") (d #t) (k 2)) (d (n "syntex") (r "^0.35") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.35") (o #t) (d #t) (k 0)))) (h "0r3rsigg9nps0za04gpq0fnapjkqr1h3axr2alfmb5803yr8nx6i") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("default"))))))

(define-public crate-indoc-0.1.12 (c (n "indoc") (v "0.1.12") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.1") (d #t) (k 2)) (d (n "syntex") (r "^0.37") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.37") (o #t) (d #t) (k 0)))) (h "1p5hhlkma3n2kkb447v642mx04jfvrs496bxdsfmbvy5s6v25n8a") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("default"))))))

(define-public crate-indoc-0.1.13 (c (n "indoc") (v "0.1.13") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.1") (d #t) (k 2)) (d (n "syntex") (r "^0.39.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.39.0") (o #t) (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 0)))) (h "1sjs7wzvpy16jawhfh9xabaz6a5hjfz5mzl6m1dh4552alba572k") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("default"))))))

(define-public crate-indoc-0.1.14 (c (n "indoc") (v "0.1.14") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.4") (d #t) (k 2)) (d (n "syntex") (r "^0.45.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.45.0") (o #t) (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 0)))) (h "0xp1w1zi9nqyn281p2gpyiyyaig1b1y1p0vb91ivxhzq1lrl5vrk") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("default"))))))

(define-public crate-indoc-0.1.15 (c (n "indoc") (v "0.1.15") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2.4") (d #t) (k 2)) (d (n "syntex") (r "^0.51.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.51.0") (o #t) (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 0)))) (h "1j87v0f7xcab7qigp1pcp3k2rvalkccwyzn6rfcn10hwj9svy9rz") (f (quote (("with-syntex" "syntex" "syntex_syntax") ("default"))))))

(define-public crate-indoc-0.2.0 (c (n "indoc") (v "0.2.0") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "indoc-impl") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)))) (h "0q5b2fq1xnlchmd2l0qr4byah6dc91068dv8ay07ga61ir2rfs73")))

(define-public crate-indoc-0.2.1 (c (n "indoc") (v "0.2.1") (d (list (d (n "indoc-impl") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)))) (h "0xk04mpwsxv2n6l08c5bw0sfkn6kx5vjswh1yak3s854ypyn2ahs") (f (quote (("unstable" "indoc-impl/unstable"))))))

(define-public crate-indoc-0.2.2 (c (n "indoc") (v "0.2.2") (d (list (d (n "indoc-impl") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.3") (k 0)))) (h "1wdnz5ajfv7v1w8jcawgnw4wiph902si7yn13n4i70m4jmr9pd3c") (f (quote (("unstable" "indoc-impl/unstable"))))))

(define-public crate-indoc-0.2.3 (c (n "indoc") (v "0.2.3") (d (list (d (n "indoc-impl") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "17ipnbw6ybcv9f2kzlprsfmm0jf1qz8n0laq8cldrxj55s5fwa0p") (f (quote (("unstable" "indoc-impl/unstable"))))))

(define-public crate-indoc-0.2.4 (c (n "indoc") (v "0.2.4") (d (list (d (n "indoc-impl") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "18fy5v4b6bc7cfsrdb9d8lyyk7fn9w9pfmqw1q3khl9vi2jahr33") (f (quote (("unstable" "indoc-impl/unstable"))))))

(define-public crate-indoc-0.2.5 (c (n "indoc") (v "0.2.5") (d (list (d (n "indoc-impl") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "1gvg7hwz4wzr2wx6hnmb0ik3nwc87bgzv74mfsy4z9543ki1z995") (f (quote (("unstable" "indoc-impl/unstable"))))))

(define-public crate-indoc-0.2.6 (c (n "indoc") (v "0.2.6") (d (list (d (n "indoc-impl") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "0w8lfnj2zfvzr2pavgly1fjrsmnzlk5p3khdy86f90ab7jkjd6ha") (f (quote (("unstable" "indoc-impl/unstable"))))))

(define-public crate-indoc-0.2.7 (c (n "indoc") (v "0.2.7") (d (list (d (n "indoc-impl") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "1qm05kpc94cl7v7325kfvbzf6spqkddcbq9ibqwryji59khsx9hj") (f (quote (("unstable" "indoc-impl/unstable"))))))

(define-public crate-indoc-0.2.8 (c (n "indoc") (v "0.2.8") (d (list (d (n "indoc-impl") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "1i68kc2l7vl4aql9ddq71l2jzhqqsx1lxy3r5nin4c9x4b2q52h9") (f (quote (("unstable" "indoc-impl/unstable"))))))

(define-public crate-indoc-0.3.0 (c (n "indoc") (v "0.3.0") (d (list (d (n "indoc-impl") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "10743520951vxlpfd65cpfkamg97y0id67zvynvyvdfngsfl7qy8") (f (quote (("unstable" "indoc-impl/unstable"))))))

(define-public crate-indoc-0.3.1 (c (n "indoc") (v "0.3.1") (d (list (d (n "indoc-impl") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.3") (d #t) (k 0)))) (h "06bwg2zzzjqllfjx57kp6gfa6p1rqjpil4hpg3i0w6pby21r42kk") (f (quote (("unstable" "indoc-impl/unstable"))))))

(define-public crate-indoc-0.3.2 (c (n "indoc") (v "0.3.2") (d (list (d (n "indoc-impl") (r "= 0.3.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.3") (d #t) (k 0)))) (h "12gsl9d8dwdh3cq93x0nkr7fqsk48d52np7v9zpgyq8db7dcvgsd") (f (quote (("unstable" "indoc-impl/unstable"))))))

(define-public crate-indoc-0.3.3 (c (n "indoc") (v "0.3.3") (d (list (d (n "indoc-impl") (r "= 0.3.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.3") (d #t) (k 0)))) (h "1mkvcirz950hj4m6hzsq933m8w4s0f1rddysv3nadzbnihi9zxd1") (f (quote (("unstable" "indoc-impl/unstable"))))))

(define-public crate-indoc-0.3.4 (c (n "indoc") (v "0.3.4") (d (list (d (n "indoc-impl") (r "= 0.3.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.3") (d #t) (k 0)) (d (n "rustversion") (r "^0.1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1y4qbzbg1gy6if61flcd5bbfwxi8byg35szbfy5ln4bcw70m759z") (f (quote (("unstable" "indoc-impl/unstable"))))))

(define-public crate-indoc-0.3.5 (c (n "indoc") (v "0.3.5") (d (list (d (n "indoc-impl") (r "= 0.3.5") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.10") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "0i65f9l3fh8y41n6b43cwqsfn7bv0ns63hlyvxfrj4apkzr5q9br") (f (quote (("unstable" "indoc-impl/unstable"))))))

(define-public crate-indoc-0.3.6 (c (n "indoc") (v "0.3.6") (d (list (d (n "indoc-impl") (r "=0.3.6") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.10") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "1n2fd2wm1h005hd7pjgx4gv5ymyq4sxqn8z0ssw6xchgqs5ilx27") (f (quote (("unstable" "indoc-impl/unstable"))))))

(define-public crate-indoc-1.0.0 (c (n "indoc") (v "1.0.0") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "0zy17nkjrj214qg41lw8xscb7yiqbp53d9dl2bzxxhdcm49772lx")))

(define-public crate-indoc-1.0.1 (c (n "indoc") (v "1.0.1") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "120v2yisy65905bfh1kxazq10vdni34w4wqbzjvmv7s463d9wx2j")))

(define-public crate-indoc-1.0.2 (c (n "indoc") (v "1.0.2") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "0vzaxr25h8cs0pkrpk633x5039g2lacjws1scl2pi3g6zv7fykb4")))

(define-public crate-indoc-1.0.3 (c (n "indoc") (v "1.0.3") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)) (d (n "unindent") (r "^0.1.7") (d #t) (k 0)))) (h "0diih20xsxjb159nr0dq6jxnyhq7gg10dlsnh2siikphmvm5m9z5")))

(define-public crate-indoc-1.0.4 (c (n "indoc") (v "1.0.4") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)) (d (n "unindent") (r "^0.1.8") (d #t) (k 0)))) (h "17iwlax2kkdjgdwk6bij2vlqvld1jzj1097pfjvsrbmbmfgnm477") (r "1.42")))

(define-public crate-indoc-1.0.5 (c (n "indoc") (v "1.0.5") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)) (d (n "unindent") (r "^0.1.8") (d #t) (k 0)))) (h "0z4kwrfdz4jfm1258i7x6lrgk1j6xc2p2v27g1551ca9gai9v32a") (r "1.42")))

(define-public crate-indoc-1.0.6 (c (n "indoc") (v "1.0.6") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)) (d (n "unindent") (r "^0.1.9") (d #t) (k 2)))) (h "0vkxja3mdjgq9hjbsvq4ql9dbfdphw01hmbqndldkr9rjc0vv805") (r "1.42")))

(define-public crate-indoc-1.0.7 (c (n "indoc") (v "1.0.7") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)) (d (n "unindent") (r "^0.1.9") (d #t) (k 2)))) (h "1qs42cn8rj7kdpmp1dlkzhv62rfmbx3ffwvsfw67zyq86jm1xaxd") (r "1.42")))

(define-public crate-indoc-1.0.8 (c (n "indoc") (v "1.0.8") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)) (d (n "unindent") (r "^0.1.9") (d #t) (k 2)))) (h "10670ygqqsahaf2v160igzg8zkbvnvgjbviyqmv7x7gazwinybfs") (r "1.42")))

(define-public crate-indoc-1.0.9 (c (n "indoc") (v "1.0.9") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)) (d (n "unindent") (r "^0.1.9") (d #t) (k 2)))) (h "01l3b4ami6sck57yrn8n2z44jifph2m3jiivkws7w2njbvfrk9xz") (r "1.42")))

(define-public crate-indoc-2.0.0 (c (n "indoc") (v "2.0.0") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)) (d (n "unindent") (r "^0.2") (d #t) (k 2)))) (h "19vn7g7hhpw3blkhw8gws13am3pj6xzm9cyxdwia1s3443cbkqkg") (r "1.56")))

(define-public crate-indoc-2.0.1 (c (n "indoc") (v "2.0.1") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)) (d (n "unindent") (r "^0.2") (d #t) (k 2)))) (h "142nzc474x1xgdzk057yv1yfrgxsk5a9zybbcwwrzp5ih65v8b4z") (r "1.56")))

(define-public crate-indoc-2.0.2 (c (n "indoc") (v "2.0.2") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)) (d (n "unindent") (r "^0.2") (d #t) (k 2)))) (h "19xv7xcwln384m5m3p93hva9vv3yicjgsa69z1vrhakyq90dw73n") (r "1.56")))

(define-public crate-indoc-2.0.3 (c (n "indoc") (v "2.0.3") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)) (d (n "unindent") (r "^0.2") (d #t) (k 2)))) (h "1i5mska11gv759ilafl4abkprwhbvbbbiz3d84rhxg9ynvpmwy1c") (r "1.56")))

(define-public crate-indoc-2.0.4 (c (n "indoc") (v "2.0.4") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)) (d (n "unindent") (r "^0.2.3") (d #t) (k 2)))) (h "1n2z66b0y59rr6v4znpcijc2yd3yg6s40hpzv89yb140mvxnq60y") (r "1.56")))

(define-public crate-indoc-2.0.5 (c (n "indoc") (v "2.0.5") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)) (d (n "unindent") (r "^0.2.3") (d #t) (k 2)))) (h "1dgjk49rkmx4kjy07k4b90qb5vl89smgb5rcw02n0q0x9ligaj5j") (r "1.56")))

