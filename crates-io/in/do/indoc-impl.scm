(define-module (crates-io in do indoc-impl) #:use-module (crates-io))

(define-public crate-indoc-impl-0.2.0 (c (n "indoc-impl") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 0)))) (h "1mdji4vwfcj24s2l21fhpdpzbw9jl7sk4akk67bd49yj19pvhka5")))

(define-public crate-indoc-impl-0.2.1 (c (n "indoc-impl") (v "0.2.1") (d (list (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 0)))) (h "1b3frivsndlq1lzx0zkciwjd15i1why746dd093pm82ybsfqg7ml") (f (quote (("unstable"))))))

(define-public crate-indoc-impl-0.2.2 (c (n "indoc-impl") (v "0.2.2") (d (list (d (n "proc-macro-hack") (r "^0.3") (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 0)))) (h "0nlhrb62bss9grfg58hsbch1vhpyc9yc4hvf98ajvqmf6wvf480y") (f (quote (("unstable"))))))

(define-public crate-indoc-impl-0.2.3 (c (n "indoc-impl") (v "0.2.3") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 0)))) (h "1bisfqivsqmhr48m4l3b5jzcf4hb78f7r69zyxjr68cqpq87vn54") (f (quote (("unstable"))))))

(define-public crate-indoc-impl-0.2.4 (c (n "indoc-impl") (v "0.2.4") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3") (k 0)) (d (n "quote") (r "^0.5") (k 0)) (d (n "syn") (r "^0.13") (f (quote ("derive" "parsing" "printing"))) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 0)))) (h "19024cxqaxgw84jsdxzyb5by9zcjbrksgyxz0a30nf2qpncmzryj") (f (quote (("unstable"))))))

(define-public crate-indoc-impl-0.2.5 (c (n "indoc-impl") (v "0.2.5") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3") (k 0)) (d (n "quote") (r "^0.5") (k 0)) (d (n "syn") (r "^0.13") (f (quote ("derive" "parsing" "printing"))) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 0)))) (h "1g5vnjnpwswk0vy5phxmsfmjw0p3w67sjdj0g0qakgyyk80m4rx4") (f (quote (("unstable"))))))

(define-public crate-indoc-impl-0.2.6 (c (n "indoc-impl") (v "0.2.6") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (k 0)) (d (n "quote") (r "^0.6") (k 0)) (d (n "syn") (r "^0.14") (f (quote ("derive" "parsing" "printing"))) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 0)))) (h "0v8p2yn192jpm7jw5yv3vx5gslcjgdz347rrqjrpdwbnsh5f39ik") (f (quote (("unstable"))))))

(define-public crate-indoc-impl-0.2.7 (c (n "indoc-impl") (v "0.2.7") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (k 0)) (d (n "quote") (r "^0.6") (k 0)) (d (n "syn") (r "^0.14") (f (quote ("derive" "parsing" "printing"))) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 0)))) (h "18xwm63dsmm7lya5v877fcn1pamh133qfjmkd796g3rld4sgq9ih") (f (quote (("unstable"))))))

(define-public crate-indoc-impl-0.2.8 (c (n "indoc-impl") (v "0.2.8") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (k 0)) (d (n "quote") (r "^0.6") (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive" "parsing" "printing"))) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 0)))) (h "04dpls8v6a6pkkknarm1b5ywkyhrrspavjawb43ipnvhr0ajmksm") (f (quote (("unstable"))))))

(define-public crate-indoc-impl-0.3.0 (c (n "indoc-impl") (v "0.3.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (k 0)) (d (n "quote") (r "^0.6") (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive" "parsing" "printing"))) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 0)))) (h "0hf4lr671yms3kcg2j5cs34510wzyfs7nk60dzlisl8k69xnl1fw") (f (quote (("unstable"))))))

(define-public crate-indoc-impl-0.3.1 (c (n "indoc-impl") (v "0.3.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (k 0)) (d (n "quote") (r "^0.6") (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive" "parsing" "printing"))) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 0)))) (h "0sazcd90avzxwl7yqwk3nw2wvs03n1l2cnmka596m4lkn64iqd65") (f (quote (("unstable"))))))

(define-public crate-indoc-impl-0.3.2 (c (n "indoc-impl") (v "0.3.2") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (k 0)) (d (n "quote") (r "^0.6") (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive" "parsing" "printing"))) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 0)))) (h "04k7jx3l6ah1vh4kfggsslfvp2rjn2rvl5lgvv3prmvgz9wgmlzg") (f (quote (("unstable"))))))

(define-public crate-indoc-impl-0.3.3 (c (n "indoc-impl") (v "0.3.3") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (k 0)) (d (n "quote") (r "^0.6") (k 0)) (d (n "syn") (r "^0.15") (f (quote ("derive" "parsing" "printing"))) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "0fwkv1hnmaajk424wz1drv04q41mpg3pbk0f38f61cqd13pp1w33") (f (quote (("unstable"))))))

(define-public crate-indoc-impl-0.3.4 (c (n "indoc-impl") (v "0.3.4") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "0xgv821yygcqb97f62dm6yn1ai136qazzkbp14wic5wns04gq55p") (f (quote (("unstable"))))))

(define-public crate-indoc-impl-0.3.5 (c (n "indoc-impl") (v "0.3.5") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "0k0k0rrb9mzkaln7qqmwm8wkijgi480fl1a09177a5rxm8840mal") (f (quote (("unstable"))))))

(define-public crate-indoc-impl-0.3.6 (c (n "indoc-impl") (v "0.3.6") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "1w58yg249kmzsn75kcj34qaxqh839l1hsaj3bzggy3q03wb6s16f") (f (quote (("unstable"))))))

(define-public crate-indoc-impl-0.3.7 (c (n "indoc-impl") (v "0.3.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "unindent") (r "^0.1.3") (d #t) (k 0)))) (h "0m0s0klnyjprfw6q6rnz6rl5hyfkjiv3md30s15v487wkcby05vi")))

