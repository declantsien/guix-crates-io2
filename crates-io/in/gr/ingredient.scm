(define-module (crates-io in gr ingredient) #:use-module (crates-io))

(define-public crate-ingredient-0.1.0 (c (n "ingredient") (v "0.1.0") (d (list (d (n "nom") (r "^6.1.0") (d #t) (k 0)))) (h "02ifzdv5njqvpn54606qx5daby17f44hqgamncicfr919ymx06vj")))

(define-public crate-ingredient-0.1.1 (c (n "ingredient") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0b4zip0flrcsyq3zxhg0d2c32w18p35lzsgc93zbjzskk46zl4xk") (f (quote (("serde-derive" "serde/derive") ("default" "serde-derive"))))))

(define-public crate-ingredient-0.1.2 (c (n "ingredient") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2") (o #t) (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (f (quote ("alloc"))) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1vhmnk7bhiiv9gksv5jf0vawmgc3bxsd74mpc9pyimb3954jjw2y") (f (quote (("serde-derive" "serde/derive") ("format-colors" "colored") ("default" "serde-derive"))))))

(define-public crate-ingredient-0.1.3 (c (n "ingredient") (v "0.1.3") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2") (o #t) (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (f (quote ("alloc"))) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ry9a8lpswplf1b8pcjyg34j7fn8bzbm7bjrymn86jmnsyp9m3nq") (f (quote (("serde-derive" "serde/derive") ("format-colors" "colored") ("default" "serde-derive"))))))

(define-public crate-ingredient-0.1.4 (c (n "ingredient") (v "0.1.4") (d (list (d (n "nom") (r "^7.1.0") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0vl7hjx78bfi3s6yjv23f267kbpphr21swi3gjv2gxd3spfqnpld") (f (quote (("serde-derive" "serde/derive") ("default" "serde-derive"))))))

(define-public crate-ingredient-0.2.0 (c (n "ingredient") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (f (quote ("alloc"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1hf6wn88a8x6l5ni12v0fqyqps90m2v6318jinq34123dlp9q8mv") (f (quote (("serde-derive" "serde/derive") ("default" "serde-derive"))))))

(define-public crate-ingredient-0.3.0 (c (n "ingredient") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (f (quote ("alloc"))) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0q0qg0d11mbxk3hj3s5viyf3d4082y4lfx2rqpvrcx74x71kydyf") (f (quote (("serde-derive" "serde/derive") ("default" "serde-derive"))))))

