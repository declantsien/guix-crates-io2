(define-module (crates-io in gr ingress-status-sync) #:use-module (crates-io))

(define-public crate-ingress-status-sync-0.0.2 (c (n "ingress-status-sync") (v "0.0.2") (d (list (d (n "gumdrop") (r "^0.8.0") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.11.0") (f (quote ("v1_19"))) (k 0)) (d (n "kube") (r "^0.52.0") (d #t) (k 0)) (d (n "kube-runtime") (r "^0.52.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.25") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.17") (d #t) (k 0)))) (h "03wyij73v2z7hdd950341njf20igh5ybh56s3c95xpqav8nhvqms") (f (quote (("strict"))))))

