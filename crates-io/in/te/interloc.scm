(define-module (crates-io in te interloc) #:use-module (crates-io))

(define-public crate-interloc-0.1.0 (c (n "interloc") (v "0.1.0") (d (list (d (n "lock_api") (r "^0.2.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.8.0") (d #t) (k 0)))) (h "0bhqhl4vqam43qz0j46l9g3ghri1nl3v3g97igg3xpcrvirkv6p1")))

(define-public crate-interloc-0.1.1 (c (n "interloc") (v "0.1.1") (d (list (d (n "lock_api") (r "^0.2.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.8.0") (d #t) (k 0)))) (h "1x082zsg3phslxpnvabv2z2giam2g4w42acl5zk16l8xnj73vfga")))

(define-public crate-interloc-0.1.2 (c (n "interloc") (v "0.1.2") (d (list (d (n "lock_api") (r "^0.2.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.8.0") (d #t) (k 0)))) (h "02dxfcyndr9vzz5qwz2xkw36nzhvzz3qpxl1393rwz7m69bgivc0")))

(define-public crate-interloc-0.1.3 (c (n "interloc") (v "0.1.3") (d (list (d (n "lock_api") (r "^0.2.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.8.0") (d #t) (k 0)))) (h "1s17vp6pa586224qcfdcjdcwd7gzchlnil27y6cf3pgxqqnd781k")))

