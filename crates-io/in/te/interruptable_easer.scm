(define-module (crates-io in te interruptable_easer) #:use-module (crates-io))

(define-public crate-interruptable_easer-1.0.0 (c (n "interruptable_easer") (v "1.0.0") (h "1wa2b3srbwpyqa6pxdasmg9jc2ag6xgsaw4x9qfmil2zhf7byfki")))

(define-public crate-interruptable_easer-1.1.0 (c (n "interruptable_easer") (v "1.1.0") (h "06hfkns9pidfgfcy27vnrx3clwya6hvmn5ialmdglz9cfblqcxkx")))

(define-public crate-interruptable_easer-1.1.1 (c (n "interruptable_easer") (v "1.1.1") (h "00g1bh1yswg4fdsqx3fzlcjhr6xrbl8jfg2rmar9smrdkdh1aa20")))

(define-public crate-interruptable_easer-1.1.2 (c (n "interruptable_easer") (v "1.1.2") (h "0g451q5zrjrh6vzdbblm3m7h6pbk6wv9lcr5lgg4g6b1f94dc98k")))

