(define-module (crates-io in te intel_8080_cpu) #:use-module (crates-io))

(define-public crate-intel_8080_cpu-0.1.0 (c (n "intel_8080_cpu") (v "0.1.0") (h "0alvmjg75jy8wfgy7pd5q2b9pplcwscl83jm6gjpi6ql8bwmsy0a") (y #t)))

(define-public crate-intel_8080_cpu-0.1.1 (c (n "intel_8080_cpu") (v "0.1.1") (h "1ha6pwqfcmw4zw5z79cc4qdlb4azzc3dadcizbwv6llm4dx8ly5a")))

