(define-module (crates-io in te intern) #:use-module (crates-io))

(define-public crate-intern-0.0.0 (c (n "intern") (v "0.0.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "140pihrygnxrzipgvj4zw9z22qazl83is7didi8y7vn959axhfxv")))

(define-public crate-intern-0.2.0 (c (n "intern") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "nodrop") (r "^0.1") (d #t) (k 0)))) (h "1am73njlhzqphd4c44nq136w8phnbzz09ffmdpx614dnhlsxc9bx")))

