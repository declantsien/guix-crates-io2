(define-module (crates-io in te integer-hasher) #:use-module (crates-io))

(define-public crate-integer-hasher-0.1.0 (c (n "integer-hasher") (v "0.1.0") (h "0iz7jmfsvq3wx6378als3n6r3rhc43y5gw3xwfckhpinw7f0a170") (f (quote (("std") ("default" "std"))))))

(define-public crate-integer-hasher-0.1.1 (c (n "integer-hasher") (v "0.1.1") (h "15l3qpb72b5va32k6g3h5j3944r203yrbijqb078pa6sfw4yzjfh") (f (quote (("std") ("default" "std"))))))

