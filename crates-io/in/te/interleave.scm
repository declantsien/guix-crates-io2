(define-module (crates-io in te interleave) #:use-module (crates-io))

(define-public crate-interleave-0.2.0 (c (n "interleave") (v "0.2.0") (h "0srdwnplk5ark5dwimmaj7p0mc1p227yg82xxgn4ry2y1ymyjjjq")))

(define-public crate-interleave-0.2.1 (c (n "interleave") (v "0.2.1") (h "09cpizpj1rkfpdipw99i9q4cyjcfqh64slihsf0a85zig5r28kbf")))

(define-public crate-interleave-0.2.2 (c (n "interleave") (v "0.2.2") (h "1635vqp7lcb9cbrryrjzms42pad5cimc57fiwd0ks6irmh92qbim")))

(define-public crate-interleave-0.2.3 (c (n "interleave") (v "0.2.3") (d (list (d (n "clippy") (r "^0.0.77") (o #t) (d #t) (k 0)))) (h "07sz64w2l18x9yp7g2jjxl61ngpm1zszhlkz3d3ra1apn9kh80iq") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-interleave-0.2.4 (c (n "interleave") (v "0.2.4") (d (list (d (n "clippy") (r "^0.0.77") (o #t) (d #t) (k 0)))) (h "0si8jaahc06q23xl9j231px3f21fmicjw3q2v0q77vfw3w8hxz4d") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-interleave-1.0.0 (c (n "interleave") (v "1.0.0") (d (list (d (n "clippy") (r "^0.0.77") (o #t) (d #t) (k 0)))) (h "1d4ggmd95jjlz7ba066s6fny60yzzvwz5905bmgm3x4mii6nrpaj") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-interleave-1.0.1 (c (n "interleave") (v "1.0.1") (d (list (d (n "clippy") (r "^0.0.77") (o #t) (d #t) (k 0)))) (h "177l2yr6clzkcv7ahvxjk2368r948hnvsqmkxz5b8m6gzqwgl8wg") (f (quote (("dev" "clippy") ("default"))))))

