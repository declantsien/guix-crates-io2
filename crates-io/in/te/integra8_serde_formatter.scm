(define-module (crates-io in te integra8_serde_formatter) #:use-module (crates-io))

(define-public crate-integra8_serde_formatter-0.0.5-rc1 (c (n "integra8_serde_formatter") (v "0.0.5-rc1") (d (list (d (n "integra8") (r "^0.0.5-rc1") (f (quote ("enable_serde" "formatters"))) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1401gxylcyky0536ln7vsmjk3d0ab7b1zz6r9g3b9wm4hkf7zi1d") (f (quote (("yaml" "serde_yaml") ("json" "serde_json") ("default" "yaml"))))))

