(define-module (crates-io in te integra8_components) #:use-module (crates-io))

(define-public crate-integra8_components-0.0.1-alpha (c (n "integra8_components") (v "0.0.1-alpha") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "0cbx26m5rhx32zpkz5pzjj19z2crr6g2g0jcwjf7f97zm0bcmbj1") (y #t)))

(define-public crate-integra8_components-0.0.2-alpha (c (n "integra8_components") (v "0.0.2-alpha") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)))) (h "1rcdrsamzz0yq3r2wp5s0bvlhw7z4mjhgvcr9cy65dxdhdqylssg")))

(define-public crate-integra8_components-0.0.3-alpha (c (n "integra8_components") (v "0.0.3-alpha") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mc5hw6db1xivg08mgq95qf0na51bc2qhnmk2m4b7glva7k056jh")))

(define-public crate-integra8_components-0.0.4-alpha (c (n "integra8_components") (v "0.0.4-alpha") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k7n8kb1szmjsijzkxpp4ncb2pjqnw4zhznx09337vcybzvdr9hr")))

