(define-module (crates-io in te intercom-attributes) #:use-module (crates-io))

(define-public crate-intercom-attributes-0.1.0 (c (n "intercom-attributes") (v "0.1.0") (d (list (d (n "intercom-common") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0xmk99rni4qp83i6q4s8qbpcch1b1q69qjzy5w6zhb3di0g7856b")))

(define-public crate-intercom-attributes-0.2.0 (c (n "intercom-attributes") (v "0.2.0") (d (list (d (n "difference") (r "^1.0.0") (d #t) (k 2)) (d (n "intercom-common") (r "^0.2.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 2)) (d (n "rustfmt-nightly") (r "^0.2") (d #t) (k 2)) (d (n "syn") (r "^0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0q0bvgmjj56fl1a9zb8w6k92anp9zmvskyy9xwwl8gmy5lq0w43c")))

(define-public crate-intercom-attributes-0.3.0 (c (n "intercom-attributes") (v "0.3.0") (d (list (d (n "intercom-common") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1dbz3c1886fzjykb7nw8clya08fz94r4j2j70iwbyi6fxnn6n01p")))

(define-public crate-intercom-attributes-0.4.0 (c (n "intercom-attributes") (v "0.4.0") (d (list (d (n "difference") (r "^2") (d #t) (k 2)) (d (n "intercom-common") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 2)) (d (n "simple_logger") (r "^1.6") (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 2)))) (h "1l4hgsmgnh56rpzn49sp7vzlz92bkdqnzzy66nlijganv34izahk")))

