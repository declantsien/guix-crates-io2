(define-module (crates-io in te interleaved-ordered) #:use-module (crates-io))

(define-public crate-interleaved-ordered-0.1.0 (c (n "interleaved-ordered") (v "0.1.0") (h "05jdk6zzwn60pk7n7li7pn2fh0fj6x74j0b3mkzj49v66l7msf3f")))

(define-public crate-interleaved-ordered-0.1.1 (c (n "interleaved-ordered") (v "0.1.1") (h "0xrx3sq647rqhqrv0ssb2wkbigyf42fysk1xpn8p9v8mbc4l04ql")))

