(define-module (crates-io in te interact_derive) #:use-module (crates-io))

(define-public crate-interact_derive-0.1.0 (c (n "interact_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.21") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1aa4nf05dzd2d8b4vlpkc0k46xvlphicfckrhkss5i4ccdpd23kd")))

(define-public crate-interact_derive-0.2.0 (c (n "interact_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.21") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "130bqw4jvk11g7jj09fzamc0zmbgn1cj7xm75l70j19cp3yfwymr")))

(define-public crate-interact_derive-0.3.0 (c (n "interact_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.21") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0r5mmyfqny6kpfsllxy929lnjmrhfc6sp0bs1k44mnq4zawpvmrm")))

(define-public crate-interact_derive-0.3.1 (c (n "interact_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "17vc1cbpqk44y9xashcmbi5i6zb6q7xi82vjgzqm9qjc19ql5wd0")))

(define-public crate-interact_derive-0.3.2 (c (n "interact_derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1nm4asc5zxns4jv36g40i2l2rn2bff0h5r10w4wv1ylani284k31")))

(define-public crate-interact_derive-0.3.3 (c (n "interact_derive") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "04zvry6ii285zkxa9wyzlwzh4yfcfnc612bc1fsh91227psby0y7")))

(define-public crate-interact_derive-0.3.4 (c (n "interact_derive") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0w0wdv9lr1w11la923dphsi5yvqkk4zbvxqqfkmnj08q97w7rk8p")))

(define-public crate-interact_derive-0.3.5 (c (n "interact_derive") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "19yhc5rm509vmnanzx42brf90qcik7a7rxv9hijjm054wrbhqnag")))

(define-public crate-interact_derive-0.3.6 (c (n "interact_derive") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1027na47cpxbk62z14yv4abxjmmql66aba29s8km6yva3qfd9z7c")))

