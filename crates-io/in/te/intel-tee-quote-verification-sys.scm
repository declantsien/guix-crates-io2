(define-module (crates-io in te intel-tee-quote-verification-sys) #:use-module (crates-io))

(define-public crate-intel-tee-quote-verification-sys-0.2.0 (c (n "intel-tee-quote-verification-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "0z70201i74vg22bq22nar21a0a015rj1s99l9vnvbfxn7784zh49") (l "sgx_dcap_quoteverify")))

(define-public crate-intel-tee-quote-verification-sys-0.2.1 (c (n "intel-tee-quote-verification-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1k3g861wv9icbina5rgryms2msqbdrq5mxj11qqliylqsm4brj4k") (l "sgx_dcap_quoteverify")))

