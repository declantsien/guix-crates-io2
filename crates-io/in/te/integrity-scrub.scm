(define-module (crates-io in te integrity-scrub) #:use-module (crates-io))

(define-public crate-integrity-scrub-0.0.0 (c (n "integrity-scrub") (v "0.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "0h4wvmhscjb86812fm4s9wj0qzh3j40qgh9hm7c19hwaz2zgsjr7")))

(define-public crate-integrity-scrub-0.0.1 (c (n "integrity-scrub") (v "0.0.1") (d (list (d (n "bytesize") (r "^1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "0hh13z7zw9v82xa3r4iwdimma68l0mx01ig8iia2j620k2z5k5k0")))

(define-public crate-integrity-scrub-0.1.0 (c (n "integrity-scrub") (v "0.1.0") (d (list (d (n "bytesize") (r "^1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "sensitive") (r "^0.9") (d #t) (k 0)))) (h "1jq94lh3wc4d8wi8fpmyppz7v4bqnff50x23rl76993dnl09xc6k")))

(define-public crate-integrity-scrub-0.2.0 (c (n "integrity-scrub") (v "0.2.0") (d (list (d (n "bytesize") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "sensitive") (r "^0.9") (k 0)))) (h "1l82i39z6by70mqb8bdx1192rhd7vkc4wq1z976z7axlg5fdij1z")))

(define-public crate-integrity-scrub-0.2.1 (c (n "integrity-scrub") (v "0.2.1") (d (list (d (n "bytesize") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "sensitive") (r "^0.9") (k 0)))) (h "0dryh5kwprlz57kfyj9irs2rgm419639ynnbnshw52a0mkfzafyz")))

(define-public crate-integrity-scrub-0.3.0 (c (n "integrity-scrub") (v "0.3.0") (d (list (d (n "bytesize") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "sensitive") (r "^0.9") (k 0)))) (h "0x55wqf4ik3z84qgigfklg1ci0aa9l7kz19ypvjw83ym325vm0gm")))

(define-public crate-integrity-scrub-0.3.1 (c (n "integrity-scrub") (v "0.3.1") (d (list (d (n "bytesize") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "sensitive") (r "^0.9") (k 0)))) (h "1k81lyg23dsxkjh3s5m2kdi3pkz6zdpk7h4nywmkqjiq9962vxfd")))

(define-public crate-integrity-scrub-0.3.2 (c (n "integrity-scrub") (v "0.3.2") (d (list (d (n "bytesize") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "sensitive") (r "^0.9") (k 0)))) (h "15na7r833xi1ib1jadn9fzg13v0d8rxs97nvkwcnn33az069ra6m")))

(define-public crate-integrity-scrub-0.4.0 (c (n "integrity-scrub") (v "0.4.0") (d (list (d (n "bytesize") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "sensitive") (r "^0.9") (k 0)))) (h "15snadc0hhfd2kj6liq75yaga926pdp26ca53g2w4qjbaxr5bbxy")))

(define-public crate-integrity-scrub-0.5.0 (c (n "integrity-scrub") (v "0.5.0") (d (list (d (n "bytesize") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "ioprio") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "sensitive") (r "^0.9") (k 0)))) (h "1965pdsx8cw2v0fkqvr3vx21q2ka3i1qjrrndckz5lb9fhx5zrfd")))

(define-public crate-integrity-scrub-0.5.1 (c (n "integrity-scrub") (v "0.5.1") (d (list (d (n "bytesize") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "ioprio") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "sensitive") (r "^0.9") (k 0)))) (h "0c715hqm43hz7qr5mbkp6axfd796ihv1jllh6k9vj2y6kvaji285")))

(define-public crate-integrity-scrub-0.5.2 (c (n "integrity-scrub") (v "0.5.2") (d (list (d (n "bytesize") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "ioprio") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "sensitive") (r "^0.9") (k 0)))) (h "0zpmac9gj5hndw7p29jswv8ixhxav94p6j4qr8rbyil56qlb58v4")))

(define-public crate-integrity-scrub-0.5.3 (c (n "integrity-scrub") (v "0.5.3") (d (list (d (n "bytesize") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "ioprio") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "sensitive") (r "^0.9") (k 0)))) (h "1wja1gpnr800kpyzh249xmzd6vdj6jf9j7l2h9b301jzb5z80a13") (y #t)))

(define-public crate-integrity-scrub-0.5.4 (c (n "integrity-scrub") (v "0.5.4") (d (list (d (n "bytesize") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "ioprio") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "sensitive") (r "^0.9") (k 0)))) (h "01ybwfhnv0j7lmdc5bz8gvnh797749n3sxnm6ryigi5wf0l717fd")))

(define-public crate-integrity-scrub-0.5.5 (c (n "integrity-scrub") (v "0.5.5") (d (list (d (n "bytesize") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "ioprio") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "sensitive") (r "^0.9") (k 0)))) (h "1w3gv4b84zaymprmyfhiw06czb91lipi3ysbc76lid5k8n7p5k0r")))

(define-public crate-integrity-scrub-0.5.6 (c (n "integrity-scrub") (v "0.5.6") (d (list (d (n "bytesize") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "ioprio") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "sensitive") (r "^0.9") (k 0)))) (h "03h4d8mjx0nilnbkjgbrn5s2pq75xz5f3z98fxm7k82vkpwkg9z4")))

(define-public crate-integrity-scrub-0.5.7 (c (n "integrity-scrub") (v "0.5.7") (d (list (d (n "bytesize") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "ioprio") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "sensitive") (r "^0.9") (k 0)))) (h "16qz83p7h5iggyrnnjiqj4w3qww1aylicsq72bc0rmqzw9k9v37s")))

(define-public crate-integrity-scrub-0.5.8 (c (n "integrity-scrub") (v "0.5.8") (d (list (d (n "bytesize") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "ioprio") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "sensitive") (r "^0.9") (k 0)))) (h "11scfvkp4yylp8h4yr268ky4qrvk4606x767qbyzmjbi6vsha3il")))

(define-public crate-integrity-scrub-0.5.9 (c (n "integrity-scrub") (v "0.5.9") (d (list (d (n "bytesize") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "ioprio") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.24") (d #t) (k 0)) (d (n "sensitive") (r "^0.9") (k 0)))) (h "1s6pmm2qxr9pmxsnkbvr3y500rri2skhhwc32kzxi6vfmn806bgf")))

(define-public crate-integrity-scrub-0.5.10 (c (n "integrity-scrub") (v "0.5.10") (d (list (d (n "bytesize") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "ioprio") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.25") (d #t) (k 0)) (d (n "sensitive") (r "^0.10") (k 0)))) (h "1blnf00k04in95yfx5dd0v3vbx3ljlj04qw29c27r81r8w2a6rfn")))

(define-public crate-integrity-scrub-0.6.0 (c (n "integrity-scrub") (v "0.6.0") (d (list (d (n "bytesize") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "ioprio") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "sensitive") (r "^0.10") (k 0)))) (h "1kxfdk78yfshi05c4k79yjngmx70wn8n8idlhg5ra9dwyvz0krd8")))

(define-public crate-integrity-scrub-0.6.1 (c (n "integrity-scrub") (v "0.6.1") (d (list (d (n "bytesize") (r "^1.2") (d #t) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "ioprio") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "sensitive") (r "^0.10") (k 0)))) (h "1nqajcs24np3s2zn8g38fp6ij85qcn488r48dqdwzxmhq2zi72fv")))

(define-public crate-integrity-scrub-0.6.2 (c (n "integrity-scrub") (v "0.6.2") (d (list (d (n "bytesize") (r "^1.2") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "ioprio") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "sensitive") (r "^0.10") (k 0)))) (h "1sxxil1s30l4gqg97p5wxvxcjkcl363g44vmm5pa9n9867ssacz6")))

(define-public crate-integrity-scrub-0.6.3 (c (n "integrity-scrub") (v "0.6.3") (d (list (d (n "bytesize") (r "^1.3") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "ioprio") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "sensitive") (r "^0.10") (k 0)))) (h "17bfv5s5wxh6lk8fs5n41qi5l02r90lyzv65r8rlpzmsl6sbxxfd")))

(define-public crate-integrity-scrub-0.6.4 (c (n "integrity-scrub") (v "0.6.4") (d (list (d (n "bytesize") (r "^1.3") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "ioprio") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.28") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "sensitive") (r "^0.10") (k 0)))) (h "0rdca3wnx8kcic574k9v13bpsg948ldmhd8hjzq57z8i9d9zndga")))

