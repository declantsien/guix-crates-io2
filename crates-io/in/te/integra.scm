(define-module (crates-io in te integra) #:use-module (crates-io))

(define-public crate-integra-0.1.0 (c (n "integra") (v "0.1.0") (h "0qfw2ymabvfknx2pk49bf4sbji8czaydz0z8zrn84dfp44psjcy7") (y #t)))

(define-public crate-integra-0.0.1 (c (n "integra") (v "0.0.1") (h "1bd25c61kajb3mf9wy3swfy2skv612cwpnzhpayb7q0r4hga2p8d") (y #t)))

(define-public crate-integra-0.0.2 (c (n "integra") (v "0.0.2") (h "0c5n7j9v8xsqrlsdl5mdqx4dz02vssaiw2zkn9626ymflx9pk6g4") (y #t)))

(define-public crate-integra-0.0.3 (c (n "integra") (v "0.0.3") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "15gxcgrkgy5vv223ryrzni2cscxf9gc6kbvw25rj56x30hp2fin5") (y #t)))

(define-public crate-integra-0.0.4 (c (n "integra") (v "0.0.4") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "13h6vpnvb3lvawqx0lqa4dl5s9xcbvhylxn8353ar7dh94l4pi0p") (y #t)))

(define-public crate-integra-0.0.5 (c (n "integra") (v "0.0.5") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "0f25fpcnviqwqxf524hng3msxzdc02m4961sy8ip3293fpkc7d2i") (y #t)))

(define-public crate-integra-0.0.6 (c (n "integra") (v "0.0.6") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "1a76zz8jr0zxx247nk4fj7xm6bqnwzhwlnhmhcnnygbjv0mgaf4i") (y #t)))

(define-public crate-integra-0.0.7 (c (n "integra") (v "0.0.7") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "0wvhm750pj2rrlkik5zqh1ipcfhzf95y8xxpafgyxl3p04sk79xm")))

(define-public crate-integra-0.0.8 (c (n "integra") (v "0.0.8") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "0yqxzla0k9wd06gbhrg93wnjgf9r5fhh8b72afc9brxhl0fajlv7")))

(define-public crate-integra-0.0.9 (c (n "integra") (v "0.0.9") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "0mmz18542gzj2jjyf30xiblxwdwa2sx5091p4sgcndvvp136g9hh")))

