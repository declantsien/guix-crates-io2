(define-module (crates-io in te internet-checksum) #:use-module (crates-io))

(define-public crate-internet-checksum-0.1.0 (c (n "internet-checksum") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.1.1") (d #t) (k 2)))) (h "1an68jkd8m8yi9xv0a86x3s6sdss6sy8h679fvpz41clg41fa32v") (f (quote (("benchmark"))))))

(define-public crate-internet-checksum-0.2.0 (c (n "internet-checksum") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.1.1") (d #t) (k 2)))) (h "0ywcyavc0dg3ydc7z39caf9a8wzaiqcchp5c6raf19ghfzr31czg") (f (quote (("benchmark"))))))

(define-public crate-internet-checksum-0.2.1 (c (n "internet-checksum") (v "0.2.1") (d (list (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.1.1") (d #t) (k 2)))) (h "05nxhharlwnkvd99s6gr3ha8ayzb179ybywp3xdi49cf00364vgw") (f (quote (("benchmark"))))))

