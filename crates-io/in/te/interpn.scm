(define-module (crates-io in te interpn) #:use-module (crates-io))

(define-public crate-interpn-0.1.0 (c (n "interpn") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.16") (f (quote ("libm"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1ah0lbfdyrqc08glp608j244zl6rdinyyajj79lkdyrlzqvzwwzr") (f (quote (("std" "itertools") ("default" "std"))))))

(define-public crate-interpn-0.2.0 (c (n "interpn") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.16") (f (quote ("libm"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0wjhqmn0i6wl0k3z3326mcafj5yq8pacbsmp54k13fw5id27k2wl") (f (quote (("std" "itertools") ("default" "std"))))))

(define-public crate-interpn-0.3.0 (c (n "interpn") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.16") (f (quote ("libm"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1yzpvibfr8m55g7c1g7gm9ynmk240nwd4a32nw2l0mfzjchqsnw3") (f (quote (("std" "itertools") ("default" "std"))))))

(define-public crate-interpn-0.4.0 (c (n "interpn") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.16") (f (quote ("libm"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "183jj5awlgshx5xa1f6srmb6d8jxbawyq8dmr3j4dbzcrzqn7icc") (f (quote (("std" "itertools") ("default" "std")))) (y #t)))

(define-public crate-interpn-0.4.1 (c (n "interpn") (v "0.4.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.16") (f (quote ("libm"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0x833vr4s7fy6c71wz3vp144dssfgryv8nsd66km8w6icafqbv42") (f (quote (("std" "itertools") ("default" "std"))))))

(define-public crate-interpn-0.4.2 (c (n "interpn") (v "0.4.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.16") (f (quote ("libm"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0ss2ialjg3x1hxfx0qxzwgrlh4mfz0x5h7mhbvnkn2h3c67w8z7k") (f (quote (("std" "itertools") ("default" "std"))))))

