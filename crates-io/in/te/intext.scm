(define-module (crates-io in te intext) #:use-module (crates-io))

(define-public crate-intext-0.0.0 (c (n "intext") (v "0.0.0") (d (list (d (n "chonk") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syntect") (r "^3") (o #t) (d #t) (k 0)))) (h "0f360jjv5ymhjb72rx7h2vc78iqqp372mrri7rfjyai5xb6ii558") (f (quote (("syntect-plugin" "syntect") ("default" "syntect-plugin"))))))

