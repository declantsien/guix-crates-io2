(define-module (crates-io in te interchange) #:use-module (crates-io))

(define-public crate-interchange-0.0.0 (c (n "interchange") (v "0.0.0") (h "0qk4kq3p60qkqm0zz10x7prjh62zh0y3zp53dfb1h2bsabclij70")))

(define-public crate-interchange-0.1.0 (c (n "interchange") (v "0.1.0") (h "008a9h0a7z6xcq2czxl3spnyjanl02cfhza6lqq6jp10n1garfwi")))

(define-public crate-interchange-0.1.1 (c (n "interchange") (v "0.1.1") (h "1izp0ws2xbpvssrz2swv6j1fi2xwrfhcdg32ndz7s6cipwysrvg8")))

(define-public crate-interchange-0.1.2 (c (n "interchange") (v "0.1.2") (h "054wrcfsjm6q1c1pv6i5law2rimg1wcspbias55q4yck4hb3dkn8")))

(define-public crate-interchange-0.2.0 (c (n "interchange") (v "0.2.0") (h "1lvv99qfh96asq39hv6xlk25z8b8dk9v8i7pi6cqf72ylxrcd67c") (y #t)))

(define-public crate-interchange-0.2.1 (c (n "interchange") (v "0.2.1") (h "133n5ms661qrizig57nq80yk6pk4n98ah1iafjp573kbbqaspnb5")))

(define-public crate-interchange-0.2.2 (c (n "interchange") (v "0.2.2") (h "1gycyiki3yx7xf5xgfgqv3ral1pz7vfpzxx2bc6z367p4cy7839i")))

(define-public crate-interchange-0.3.0 (c (n "interchange") (v "0.3.0") (d (list (d (n "loom") (r "^0.5") (d #t) (t "cfg(loom)") (k 0)))) (h "0kvv6yrlpv6dnk94pvgc0cw1prrp93p4vq3rn954bnlrwi5smkwj")))

