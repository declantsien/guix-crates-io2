(define-module (crates-io in te integra8_async_runtime) #:use-module (crates-io))

(define-public crate-integra8_async_runtime-0.0.1-alpha (c (n "integra8_async_runtime") (v "0.0.1-alpha") (d (list (d (n "async-std") (r "^1.10.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("rt" "sync" "time" "rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "00mgsy9pcfrql6nc9v726zfisir5ajxx7kxy527f3nzjvzmnnsiq") (f (quote (("tokio-runtime" "tokio") ("async-std-runtime" "async-std")))) (y #t)))

(define-public crate-integra8_async_runtime-0.0.2-alpha (c (n "integra8_async_runtime") (v "0.0.2-alpha") (d (list (d (n "async-std") (r "^1.10.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("rt" "sync" "time" "rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "0wrbj4b8r577c7ryn1xsjzz1azr1f4cxr2ijp5r6s3lngq47ii9r") (f (quote (("tokio-runtime" "tokio") ("async-std-runtime" "async-std"))))))

(define-public crate-integra8_async_runtime-0.0.3-alpha (c (n "integra8_async_runtime") (v "0.0.3-alpha") (d (list (d (n "async-std") (r "^1.10.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("rt" "sync" "time" "rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "03n6x80yfs12ny3y758r6q8nlxrsx3jjy318i87fqlqwcb72kdmy") (f (quote (("tokio-runtime" "tokio") ("async-std-runtime" "async-std"))))))

(define-public crate-integra8_async_runtime-0.0.4-alpha (c (n "integra8_async_runtime") (v "0.0.4-alpha") (d (list (d (n "async-std") (r "^1.10.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("rt" "sync" "time" "rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "0fsqg56j24ga461n2p0l8ilcynqmmn3y74ka63ryv4335r3za13f") (f (quote (("tokio-runtime" "tokio") ("async-std-runtime" "async-std"))))))

