(define-module (crates-io in te intervals-good) #:use-module (crates-io))

(define-public crate-intervals-good-0.1.0 (c (n "intervals-good") (v "0.1.0") (d (list (d (n "egg") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rug") (r "^1.16.0") (d #t) (k 0)))) (h "0nrcmwdlqfdwjxpm1865la4qp6hrbhjvprcqxi9nhc3na6rqzx0p")))

(define-public crate-intervals-good-0.1.1 (c (n "intervals-good") (v "0.1.1") (d (list (d (n "egg") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rug") (r "^1.18.0") (d #t) (k 0)))) (h "1pwm8vz995k5qciqw6841lsm020zlvm9g5d35k3vz69d15ngm7dp")))

