(define-module (crates-io in te interoptopus_reference_project) #:use-module (crates-io))

(define-public crate-interoptopus_reference_project-0.1.0 (c (n "interoptopus_reference_project") (v "0.1.0") (d (list (d (n "interoptopus") (r "^0.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "03x6f7rhz94drh25hnwybw8fmv6q6cd3c4ns9qkjdzzdy3grv857")))

(define-public crate-interoptopus_reference_project-0.1.1 (c (n "interoptopus_reference_project") (v "0.1.1") (d (list (d (n "interoptopus") (r "^0.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ipc6m2c7zijp13fy3b8bnh6akalpc748q854d33m92wg4m7n0kk")))

(define-public crate-interoptopus_reference_project-0.1.5 (c (n "interoptopus_reference_project") (v "0.1.5") (d (list (d (n "interoptopus") (r "^0.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "00h7m0z1fw4ni6vgqzwj1lga7j6m2k766fjpyfhgmc6yjg3s8jik")))

(define-public crate-interoptopus_reference_project-0.1.6 (c (n "interoptopus_reference_project") (v "0.1.6") (d (list (d (n "interoptopus") (r "^0.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r79h78m65lk9wsq7i17775bgsa65s5knpbh5ixz40mf08470bqq")))

(define-public crate-interoptopus_reference_project-0.1.7 (c (n "interoptopus_reference_project") (v "0.1.7") (d (list (d (n "interoptopus") (r "^0.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vvsziw847jfwb08p9vfglyr22b7sdsy1dg1basa5px8s10gwfhm")))

(define-public crate-interoptopus_reference_project-0.1.8 (c (n "interoptopus_reference_project") (v "0.1.8") (d (list (d (n "interoptopus") (r "^0.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ykgvb3sxvp94xp8b04pz4xkasy766b9d49g2nsmdbknw6hbqzvs")))

(define-public crate-interoptopus_reference_project-0.1.12 (c (n "interoptopus_reference_project") (v "0.1.12") (d (list (d (n "interoptopus") (r "^0.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dcprdbqixdspvi4wln599lj96qlcrzlmf6q8vfz2jhw82qylq7h")))

(define-public crate-interoptopus_reference_project-0.1.13 (c (n "interoptopus_reference_project") (v "0.1.13") (d (list (d (n "interoptopus") (r "^0.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "08r21psr0p3z3ng19bxzgghsfl8vh7cb9mqrnk0kq4l67gqjv3r7")))

(define-public crate-interoptopus_reference_project-0.1.14 (c (n "interoptopus_reference_project") (v "0.1.14") (d (list (d (n "interoptopus") (r "^0.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "10l6jrshhshriw244k3a3afnj09h1yx73vlzz6xxn3lk2jyjmjh5")))

(define-public crate-interoptopus_reference_project-0.1.15 (c (n "interoptopus_reference_project") (v "0.1.15") (d (list (d (n "interoptopus") (r "^0.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zxzcvw8i7r8bncv7mz2mxyxzirkcbj1pzi337pazvr4zrsjhlcl")))

(define-public crate-interoptopus_reference_project-0.1.16 (c (n "interoptopus_reference_project") (v "0.1.16") (d (list (d (n "interoptopus") (r "^0.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "186m1pn3y3q8qyp3qqmy3iwq6cb18b243334077gk2i2wg8p6hs6")))

(define-public crate-interoptopus_reference_project-0.1.17 (c (n "interoptopus_reference_project") (v "0.1.17") (d (list (d (n "interoptopus") (r "^0.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fqfmys2n4j0vzp1m6jchvw60cd0akqifbcvdj63ncqyjx1igyy9")))

(define-public crate-interoptopus_reference_project-0.2.2 (c (n "interoptopus_reference_project") (v "0.2.2") (d (list (d (n "interoptopus") (r "^0.2.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vqcijpcm1zazzjdk4wxlz52yg2y8h7g6x1bn4kh6h2dk30dwwsf")))

(define-public crate-interoptopus_reference_project-0.2.3 (c (n "interoptopus_reference_project") (v "0.2.3") (d (list (d (n "interoptopus") (r "^0.2.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r851mk6h5plp0n669m84x7y356wfr0708d3n1hzlnj4kwndl7rx")))

(define-public crate-interoptopus_reference_project-0.2.4 (c (n "interoptopus_reference_project") (v "0.2.4") (d (list (d (n "interoptopus") (r "^0.2.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k3n838myf0dzvr6mw47zyr2plk8xiyfck8xcnz0qdwrfahxqcf6")))

(define-public crate-interoptopus_reference_project-0.2.5 (c (n "interoptopus_reference_project") (v "0.2.5") (d (list (d (n "interoptopus") (r "^0.2.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02fhy4h9pasbzym1gxx9vyz68w1gv0im0kqqmjbbx89dyydh7skl")))

(define-public crate-interoptopus_reference_project-0.2.6 (c (n "interoptopus_reference_project") (v "0.2.6") (d (list (d (n "interoptopus") (r "^0.2.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rjyjyfqbsa4795xl6q296yrd03j8l7y2q18zl233avmsxd5s241")))

(define-public crate-interoptopus_reference_project-0.2.7 (c (n "interoptopus_reference_project") (v "0.2.7") (d (list (d (n "interoptopus") (r "^0.2.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c54y8j9pqdplc1i2inqs97ky9ksssrdn0qqkzjq3jzmskdqa558")))

(define-public crate-interoptopus_reference_project-0.3.0 (c (n "interoptopus_reference_project") (v "0.3.0") (d (list (d (n "interoptopus") (r "^0.3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "181q1cpkjvmpfpbq6jsgxxi6xq7vbzv78pb0h4z405lfpn130gfd")))

(define-public crate-interoptopus_reference_project-0.3.1 (c (n "interoptopus_reference_project") (v "0.3.1") (d (list (d (n "interoptopus") (r "^0.3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mig76085nidksfjvbib16s68vlqp6x1v7pxkaca63028s3wsv89")))

(define-public crate-interoptopus_reference_project-0.3.2 (c (n "interoptopus_reference_project") (v "0.3.2") (d (list (d (n "interoptopus") (r "^0.3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dhn786akqclgvjb02pb7n32jfqs1bqzjyc3vrzdfbj5fvxsqf4d")))

(define-public crate-interoptopus_reference_project-0.3.3 (c (n "interoptopus_reference_project") (v "0.3.3") (d (list (d (n "interoptopus") (r "^0.3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bykqd4f1jzngziq5mxnvw7hyp6qxi6xz95wj6acn7w5p0r49i6p")))

(define-public crate-interoptopus_reference_project-0.3.4 (c (n "interoptopus_reference_project") (v "0.3.4") (d (list (d (n "interoptopus") (r "^0.3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rpbgq0nq8mlxpn3yfmhcfnm059gz83xcglkashzvmg2ilf9xx8c")))

(define-public crate-interoptopus_reference_project-0.3.5 (c (n "interoptopus_reference_project") (v "0.3.5") (d (list (d (n "interoptopus") (r "^0.3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05lyn1lncily5gnsdmzblichcz5wh9mian834si2pb0gp7fw12fn")))

(define-public crate-interoptopus_reference_project-0.4.0 (c (n "interoptopus_reference_project") (v "0.4.0") (d (list (d (n "interoptopus") (r "^0.4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1visqpy8fjgfhdww9zdaxfllcbrr7q4zlzxdlmfia13pkxn2x8xx")))

(define-public crate-interoptopus_reference_project-0.5.0 (c (n "interoptopus_reference_project") (v "0.5.0") (d (list (d (n "interoptopus") (r "^0.5.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "014z6v5gmdwqxzfjnbbqmzka8j2wxl7jzl2z43ldpa7i5255rf4i")))

(define-public crate-interoptopus_reference_project-0.5.1 (c (n "interoptopus_reference_project") (v "0.5.1") (d (list (d (n "interoptopus") (r "^0.5.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1w99r3l4mpdpc2asjgp0pdbcb4d8bkx11fc8ck0fs9g13dyd4qff")))

(define-public crate-interoptopus_reference_project-0.6.0 (c (n "interoptopus_reference_project") (v "0.6.0") (d (list (d (n "interoptopus") (r "^0.6.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10c53d17p3zszvzfanajdn29g6idv0sb9v7nj9i96lh5xcnbjp9f")))

(define-public crate-interoptopus_reference_project-0.7.0 (c (n "interoptopus_reference_project") (v "0.7.0") (d (list (d (n "interoptopus") (r "^0.7.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jdgslg73kakcrirvzka8ckpbb01il8bavpl99vkz23b3xs5sh1d")))

(define-public crate-interoptopus_reference_project-0.7.2 (c (n "interoptopus_reference_project") (v "0.7.2") (d (list (d (n "interoptopus") (r "^0.7.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dlb8iy4i960380sw6kzwn2vm2qnslngnrbggjr1kqmgh9a8crgp")))

(define-public crate-interoptopus_reference_project-0.8.0 (c (n "interoptopus_reference_project") (v "0.8.0") (d (list (d (n "interoptopus") (r "^0.8.0") (d #t) (k 0)))) (h "10szcs412cy8hiyn931ybyf6362ik57w1ild0jpg4scvlhvb5v80")))

(define-public crate-interoptopus_reference_project-0.8.1 (c (n "interoptopus_reference_project") (v "0.8.1") (d (list (d (n "interoptopus") (r "^0.8.0") (d #t) (k 0)))) (h "1q66g5ckxnhsci62g8rcvyrcnfrsnlgiy3xjdp8pjjkr3cf2cd67")))

(define-public crate-interoptopus_reference_project-0.9.0 (c (n "interoptopus_reference_project") (v "0.9.0") (d (list (d (n "interoptopus") (r "^0.9.0") (d #t) (k 0)))) (h "031n87if94kj5y1x90m80fhrmhr7ba6fla4jk0wvrq996kfq1jky")))

(define-public crate-interoptopus_reference_project-0.10.0 (c (n "interoptopus_reference_project") (v "0.10.0") (d (list (d (n "interoptopus") (r "^0.10.0") (d #t) (k 0)))) (h "0agcn1vfq3gnvvgyq54lkdlh2aj3726ipwwil2h1xcn411r6yb37")))

(define-public crate-interoptopus_reference_project-0.10.1 (c (n "interoptopus_reference_project") (v "0.10.1") (d (list (d (n "interoptopus") (r "^0.10.0") (d #t) (k 0)))) (h "056l7xckq0q06fjg2pb9vzk5x30hndji4wndc1cgpv63rqn85gah")))

(define-public crate-interoptopus_reference_project-0.11.0 (c (n "interoptopus_reference_project") (v "0.11.0") (d (list (d (n "interoptopus") (r "^0.11.0") (d #t) (k 0)))) (h "0jqdi4xwvr39jjmn6b91f6wmim8fvc79ywi9w7h295044irimchz")))

(define-public crate-interoptopus_reference_project-0.12.0 (c (n "interoptopus_reference_project") (v "0.12.0") (d (list (d (n "interoptopus") (r "^0.12.0") (d #t) (k 0)))) (h "0549kh6dv64kl7f0i3w2k57iy2066f5jbhwam82gsa127n3rfzv4")))

(define-public crate-interoptopus_reference_project-0.13.0 (c (n "interoptopus_reference_project") (v "0.13.0") (d (list (d (n "interoptopus") (r "^0.13.0") (d #t) (k 0)))) (h "0m3pxkb1gf65551q640l94q48pi45f1hbff1mjmgfv1xna43mfv5")))

(define-public crate-interoptopus_reference_project-0.13.3 (c (n "interoptopus_reference_project") (v "0.13.3") (d (list (d (n "interoptopus") (r "^0.13.0") (d #t) (k 0)))) (h "1nal1szw82zjkvdg94ddsnhmzgamrbm08c1ml48lvyldfc7rsxil")))

(define-public crate-interoptopus_reference_project-0.13.5 (c (n "interoptopus_reference_project") (v "0.13.5") (d (list (d (n "interoptopus") (r "^0.13.0") (d #t) (k 0)))) (h "0yxw1s1fqw6mf5z0v1s9wmnjlsyj5jcwvnl265ad80qppvc7gdw8")))

(define-public crate-interoptopus_reference_project-0.13.6 (c (n "interoptopus_reference_project") (v "0.13.6") (d (list (d (n "interoptopus") (r "^0.13.0") (d #t) (k 0)))) (h "0dlngcgk8ymsafldh134ym3vi47480szylrwv14warhmnsra3wa1")))

(define-public crate-interoptopus_reference_project-0.13.8 (c (n "interoptopus_reference_project") (v "0.13.8") (d (list (d (n "interoptopus") (r "^0.13.0") (d #t) (k 0)))) (h "0f0w4ajqh6sd5f5d3dffqiipr7jsnhsp2iayh73sgs64ad3gg9lm")))

(define-public crate-interoptopus_reference_project-0.14.0 (c (n "interoptopus_reference_project") (v "0.14.0") (d (list (d (n "interoptopus") (r "^0.14.0") (d #t) (k 0)))) (h "00na1grnqbp2a3dw8xxdaq4xjn2zmpiz8glsp20bklzgsp8n6rjs")))

(define-public crate-interoptopus_reference_project-0.14.2 (c (n "interoptopus_reference_project") (v "0.14.2") (d (list (d (n "interoptopus") (r "^0.14.0") (d #t) (k 0)))) (h "002h9jkp64aj023bk7biw90hr44zcf5ivxjil43c58k3zj0hkb4n")))

(define-public crate-interoptopus_reference_project-0.14.6 (c (n "interoptopus_reference_project") (v "0.14.6") (d (list (d (n "interoptopus") (r "^0.14.0") (d #t) (k 0)))) (h "03w88yb4c427n1a7jihwcvdmaf3k1igb0v8266jcq3c6bsgwrvdg")))

(define-public crate-interoptopus_reference_project-0.14.7 (c (n "interoptopus_reference_project") (v "0.14.7") (d (list (d (n "interoptopus") (r "^0.14.0") (d #t) (k 0)))) (h "09y4a8wxpmnzp5hnqgfb2b90nhi0n04a37mdx256hrck8mmfgwiy")))

(define-public crate-interoptopus_reference_project-0.14.9 (c (n "interoptopus_reference_project") (v "0.14.9") (d (list (d (n "interoptopus") (r "^0.14.0") (d #t) (k 0)))) (h "0f25mlndilapnhwbqi3bqq9742wwssdnv87y37pir3vsk9i5vdrl")))

(define-public crate-interoptopus_reference_project-0.14.13 (c (n "interoptopus_reference_project") (v "0.14.13") (d (list (d (n "interoptopus") (r "^0.14.0") (d #t) (k 0)))) (h "135c2c2fq57wmv7kn3w7fwqr63y1d0s6pr5ww43mbk324fpwssm8")))

(define-public crate-interoptopus_reference_project-0.14.14 (c (n "interoptopus_reference_project") (v "0.14.14") (d (list (d (n "interoptopus") (r "^0.14.0") (d #t) (k 0)))) (h "0r6443kdq4g46njpp4kn6ds6vvlr8m54wn42vd4kja9xq7951n24")))

(define-public crate-interoptopus_reference_project-0.14.15 (c (n "interoptopus_reference_project") (v "0.14.15") (d (list (d (n "interoptopus") (r "^0.14.0") (d #t) (k 0)))) (h "0sbn4gsxiynq4z42hk75kq3nn8rsfh36prqqn38kfwbm6klv985r")))

(define-public crate-interoptopus_reference_project-0.14.18 (c (n "interoptopus_reference_project") (v "0.14.18") (d (list (d (n "interoptopus") (r "^0.14.0") (d #t) (k 0)))) (h "0d2wzy8igkskm6cdrmsjj729rr3953rgsh7a088l9kaqij6spxbs")))

(define-public crate-interoptopus_reference_project-0.14.19 (c (n "interoptopus_reference_project") (v "0.14.19") (d (list (d (n "interoptopus") (r "^0.14.0") (d #t) (k 0)))) (h "0q8r3if76ilplqdc4fxk6app73f814sf5zm9jbnshyiv5nqqlx9a")))

(define-public crate-interoptopus_reference_project-0.14.21 (c (n "interoptopus_reference_project") (v "0.14.21") (d (list (d (n "interoptopus") (r "^0.14.0") (d #t) (k 0)))) (h "1pcyxjm67s2j4xc00hl6l9470ggdc11k5jc15mlfkz4a2ignik76")))

(define-public crate-interoptopus_reference_project-0.14.22 (c (n "interoptopus_reference_project") (v "0.14.22") (d (list (d (n "interoptopus") (r "^0.14.0") (d #t) (k 0)))) (h "0nznc5w3d2y7i34vcrmzvkwf3kpzrwiaqvywmp9v9c7gkbblzwk6")))

(define-public crate-interoptopus_reference_project-0.14.24 (c (n "interoptopus_reference_project") (v "0.14.24") (d (list (d (n "interoptopus") (r "^0.14.0") (d #t) (k 0)))) (h "03kwfjks156mbb9h9m86vi7whv4pm3drqiwfpkk55vfg5ax75yp4")))

