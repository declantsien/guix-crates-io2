(define-module (crates-io in te interpreter-interface) #:use-module (crates-io))

(define-public crate-interpreter-interface-0.3.0 (c (n "interpreter-interface") (v "0.3.0") (d (list (d (n "fluence") (r "^0.2.18") (d #t) (k 0)) (d (n "fluence-it-types") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "=1.0.118") (d #t) (k 0)))) (h "00am6c9n3535w6vr6wgwn5mq9xmjjyzam8m2arxmxdgyxpsghf3c")))

