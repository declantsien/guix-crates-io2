(define-module (crates-io in te interview) #:use-module (crates-io))

(define-public crate-interview-0.1.0 (c (n "interview") (v "0.1.0") (h "0zvxxx7yznl5y71p7kdqnkqg95xb08k6vbh3hx1ggc3sw37skpvg")))

(define-public crate-interview-0.1.1 (c (n "interview") (v "0.1.1") (h "07cxv2zn9c84pc5icciirdz5w3sr3hnkh9pww66xbw6pzxr0y0s5")))

(define-public crate-interview-0.1.2 (c (n "interview") (v "0.1.2") (h "0d1fwc2q7880lbcpmpc3mq7kg2bjpdi3ggj6s14aklqrs2k128lw")))

