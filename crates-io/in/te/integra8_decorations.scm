(define-module (crates-io in te integra8_decorations) #:use-module (crates-io))

(define-public crate-integra8_decorations-0.0.1-alpha (c (n "integra8_decorations") (v "0.0.1-alpha") (d (list (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "integra8_components") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "integra8_decorations_impl") (r "^0.0.1-alpha") (d #t) (k 2)) (d (n "linkme") (r "^0.2") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0wz03bdhz31l6sfcplmisamfv9qrc1d9gpa1h8rj0rfnw8lqrbny") (y #t)))

(define-public crate-integra8_decorations-0.0.2-alpha (c (n "integra8_decorations") (v "0.0.2-alpha") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "integra8_components") (r "^0.0.2-alpha") (d #t) (k 0)) (d (n "integra8_decorations_impl") (r "^0.0.2-alpha") (d #t) (k 2)) (d (n "linkme") (r "^0.2") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "1gs0cvgylm6azzsji7gvcijjs2ha6i4awgg0rq3mvadxb4b5gyna")))

(define-public crate-integra8_decorations-0.0.3-alpha (c (n "integra8_decorations") (v "0.0.3-alpha") (d (list (d (n "integra8_components") (r "^0.0.3-alpha") (d #t) (k 0)) (d (n "integra8_decorations_impl") (r "^0.0.3-alpha") (d #t) (k 2)) (d (n "linkme") (r "^0.2") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0066f643wcpakqg1iw0zapim48vsxmjw6z8kbzxpsiabb1kkg30y")))

(define-public crate-integra8_decorations-0.0.4-alpha (c (n "integra8_decorations") (v "0.0.4-alpha") (d (list (d (n "integra8_components") (r "^0.0.4-alpha") (d #t) (k 0)) (d (n "integra8_decorations_impl") (r "^0.0.4-alpha") (d #t) (k 2)) (d (n "linkme") (r "^0.2") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0q5d4s211qx3vqj5zvsg851qlhyrnwygayq3y4x5ag7z29hgx0xq")))

