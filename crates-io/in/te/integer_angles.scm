(define-module (crates-io in te integer_angles) #:use-module (crates-io))

(define-public crate-integer_angles-0.1.0 (c (n "integer_angles") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.20.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "18y4my3bpsnnilpl5vkza4gixx33sg0k0z39gcw8skwfqrkib1p5")))

(define-public crate-integer_angles-0.2.0 (c (n "integer_angles") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.20.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.1") (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0dbka4cq7924x6cc5cwjmgjnr996xyl4arr8p4z81bk4l32jg6kb")))

