(define-module (crates-io in te interp) #:use-module (crates-io))

(define-public crate-interp-0.1.0 (c (n "interp") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10") (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1c31sydc8xb5gh0lhzzaibvwmq3hvfp509vvx6cmhklr04534a6l")))

(define-public crate-interp-0.1.1 (c (n "interp") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10") (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "00ic9g1r0is70l33kixs83n834p494ddwh3ab2fwy48p3m3wccs8")))

(define-public crate-interp-1.0.0 (c (n "interp") (v "1.0.0") (d (list (d (n "itertools") (r "^0.10") (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "14k0115w448gccllppnfp9c06hxmdvwcl4qxm1g3pg9z2mmk9czd") (f (quote (("interp_array"))))))

(define-public crate-interp-1.0.1 (c (n "interp") (v "1.0.1") (d (list (d (n "itertools") (r "^0.10") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "147477vpp43as7jracjm2flrya9jj035sd20qcpcvvwfl5kvnv59") (f (quote (("interp_array"))))))

(define-public crate-interp-1.0.2 (c (n "interp") (v "1.0.2") (d (list (d (n "itertools") (r "^0.11") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0zy5m0x5gfnvv84klprkfc0hj8xg6hz72x34sdhvzdi5zzf8i3kk") (f (quote (("interp_array"))))))

(define-public crate-interp-1.0.3 (c (n "interp") (v "1.0.3") (d (list (d (n "itertools") (r "^0.12") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "12zh24b6ng2k69avg508qhr5qxvws67d14z1smyzq0hmmdnil3sk") (f (quote (("interp_array"))))))

