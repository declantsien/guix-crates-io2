(define-module (crates-io in te interlink-derive) #:use-module (crates-io))

(define-public crate-interlink-derive-0.1.0 (c (n "interlink-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1sw07v19pih8cf81y3saf09lk7fznpwq79qm0f1yrii91fv41zi1")))

(define-public crate-interlink-derive-0.1.1 (c (n "interlink-derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03yh6kz2n7vagpkn3dys3yfbrg2ajzjvqvw0f9ba9rfyx22dg7dk")))

(define-public crate-interlink-derive-0.1.2 (c (n "interlink-derive") (v "0.1.2") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0wzfz3gimvfvdjvk5v4z9l5w6gmy34yl5r01xr3m7cbyis8yh01g")))

