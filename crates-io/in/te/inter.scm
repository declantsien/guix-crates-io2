(define-module (crates-io in te inter) #:use-module (crates-io))

(define-public crate-inter-0.1.0 (c (n "inter") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "default"))) (d #t) (k 0)))) (h "12qlwda0v22q896bb2cg25bsyz4jb0sfm4bjsx393x427yi3i5n1")))

