(define-module (crates-io in te interface-builder) #:use-module (crates-io))

(define-public crate-interface-builder-0.1.0 (c (n "interface-builder") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "1xrid4myp37pxdni8s8fj0zmx6j5imf3p416q7jpb0rfz3jqk8qy")))

(define-public crate-interface-builder-0.1.1 (c (n "interface-builder") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "1wj7dsq8pc4zyg6p3hrz8v3bgpcg8qwlqggrzk82w5irnpy48jz1")))

(define-public crate-interface-builder-0.1.2 (c (n "interface-builder") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "0cf7ir1jfqhlx73zcy0r4gmaad8lwc5flgij2s2vpa1pkg2ypafp")))

(define-public crate-interface-builder-0.1.3 (c (n "interface-builder") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "0kivxmczy6n7fa8g68b1r919qsqw324syvm2iysflmhf49b8wr96")))

