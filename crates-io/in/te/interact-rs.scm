(define-module (crates-io in te interact-rs) #:use-module (crates-io))

(define-public crate-interact-rs-0.1.0 (c (n "interact-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07h7nk61jp0hsxiwsb64kybfw1i56jrklbh27fwwzkq1jymr35b7") (y #t)))

(define-public crate-interact-rs-0.1.1 (c (n "interact-rs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04rxqfi0annby1rw1q56prc4lnps2fbjcvyv6sqf7fl3vks69zr6")))

