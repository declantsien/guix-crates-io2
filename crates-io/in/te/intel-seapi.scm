(define-module (crates-io in te intel-seapi) #:use-module (crates-io))

(define-public crate-intel-seapi-0.0.0 (c (n "intel-seapi") (v "0.0.0") (d (list (d (n "bindgen") (r "^0.43.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12ald1n07wszl255mp7gv29m8a12pykzga8znrpar7jr3z8is3zp")))

(define-public crate-intel-seapi-0.0.1 (c (n "intel-seapi") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.43.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ivs3sbz3ch4ympy5f2d8k4y0mj05vj4yv2ab5bp6wn29zbz8q6l")))

(define-public crate-intel-seapi-0.0.2 (c (n "intel-seapi") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.43.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18m7y6hdl2cwn16w17pjc7i3k3rh0mggadilrgjb7wd5hlnyvh5x")))

(define-public crate-intel-seapi-0.1.0 (c (n "intel-seapi") (v "0.1.0") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "intel-seapi-sys") (r "^0.1") (d #t) (k 0)))) (h "1v2ddh02f5fg8lh3bnpb17my35lbrd1b130rmgb3aph318lsvhc5")))

(define-public crate-intel-seapi-0.1.1 (c (n "intel-seapi") (v "0.1.1") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "intel-seapi-sys") (r "^0.1") (d #t) (k 0)))) (h "1kw4v8rcahgg722ml3glgvsb9a734gy3ix0cyh5r2wghxyv1n88w")))

(define-public crate-intel-seapi-0.1.2 (c (n "intel-seapi") (v "0.1.2") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "intel-seapi-sys") (r "^0.1") (d #t) (k 0)))) (h "113s0bnkfpcxfhp3bq8bf1sv25z7frx1qmzpm8117jk6bd3d1ddq")))

(define-public crate-intel-seapi-0.1.3 (c (n "intel-seapi") (v "0.1.3") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "intel-seapi-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0hmaqwxd1sng4gh4nc8skpznpmi1l73xsr9bjky02h594dnns5a6")))

(define-public crate-intel-seapi-0.2.0 (c (n "intel-seapi") (v "0.2.0") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "intel-seapi-sys") (r "^0.2") (d #t) (k 0)))) (h "0383iiwcky57klfyi2hv6a24bfnbznnjg5r11m70jyhriaq0hs6m")))

(define-public crate-intel-seapi-0.3.0 (c (n "intel-seapi") (v "0.3.0") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "intel-seapi-sys") (r "^0.2") (d #t) (k 0)))) (h "1346qwa7v6zjjw7rv16aznhp5sj11mi9353zrlfplb3qbav9v2kb")))

(define-public crate-intel-seapi-0.3.1 (c (n "intel-seapi") (v "0.3.1") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "intel-seapi-sys") (r "^0.2") (d #t) (k 0)))) (h "0npbmf7v5bd348sw4vd1596201spcr0yqbra09qw43h5bfyijlhj")))

