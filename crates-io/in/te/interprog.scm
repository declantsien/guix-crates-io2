(define-module (crates-io in te interprog) #:use-module (crates-io))

(define-public crate-interprog-0.1.0 (c (n "interprog") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "08drb0ppc91bffa1nl9wdlfz3axmysfgbfkl4hgp0wkqs50425sz")))

