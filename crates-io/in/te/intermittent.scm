(define-module (crates-io in te intermittent) #:use-module (crates-io))

(define-public crate-intermittent-0.0.1 (c (n "intermittent") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0il0mj4q3251xkb5il30qwy00gpf8c3v7hwf06q7af5l72b8z2hp")))

