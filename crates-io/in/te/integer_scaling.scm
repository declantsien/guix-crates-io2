(define-module (crates-io in te integer_scaling) #:use-module (crates-io))

(define-public crate-integer_scaling-1.0.0 (c (n "integer_scaling") (v "1.0.0") (d (list (d (n "IntegerScaling") (r "^1.0.0") (d #t) (k 0)))) (h "0zd4pg86nj4r87ww882qw0rmpmkj0c9hxfzmmxk14899wfjmdwgc") (y #t)))

(define-public crate-integer_scaling-1.0.1 (c (n "integer_scaling") (v "1.0.1") (d (list (d (n "IntegerScaling") (r "^1.0.1") (d #t) (k 0)))) (h "0q46a05y9q82lhb9kca48lw9hz6klqjhn41jvzrmc2ghjbm6lidh") (y #t)))

(define-public crate-integer_scaling-1.1.0 (c (n "integer_scaling") (v "1.1.0") (d (list (d (n "IntegerScaling") (r "^1.1.0") (d #t) (k 0)))) (h "1av7x6scc86mpdpbfy486casgmr87nfr9wsh7ffa3ygcl0rgjalq") (y #t)))

(define-public crate-integer_scaling-1.2.0 (c (n "integer_scaling") (v "1.2.0") (d (list (d (n "IntegerScaling") (r "^1.2.0") (d #t) (k 0)))) (h "1i3awq4acy1hnyx1qa8kgmk577phwfy5vp6yr8zhycmr2wg86dw8")))

(define-public crate-integer_scaling-1.2.1 (c (n "integer_scaling") (v "1.2.1") (d (list (d (n "IntegerScaling") (r "^1.2.0") (d #t) (k 0)))) (h "0nnhkvkc1ry533r5n0irbachikaivbjmm7na7v6gprbsb51zhv03")))

(define-public crate-integer_scaling-1.2.2 (c (n "integer_scaling") (v "1.2.2") (d (list (d (n "IntegerScaling") (r "^1.2.2") (d #t) (k 0)))) (h "1dpz1b85xqwynshl92l9nal48g9bv74vh5x371ww97gxgf9gzgi9") (y #t)))

(define-public crate-integer_scaling-1.2.3 (c (n "integer_scaling") (v "1.2.3") (d (list (d (n "IntegerScaling") (r "^1.2.3") (d #t) (k 0)))) (h "1lcax4jq2qjddqzdmq7r21bjcc7b7q12r4ih490ccs0dds51czjb")))

(define-public crate-integer_scaling-1.3.1 (c (n "integer_scaling") (v "1.3.1") (d (list (d (n "IntegerScaling") (r "^1.3.0") (d #t) (k 0)))) (h "1d6gipid4ryppl1mzcf86z0y980j6lrywlpdmdkwvgmc1fjk7v47") (y #t)))

(define-public crate-integer_scaling-1.3.2 (c (n "integer_scaling") (v "1.3.2") (d (list (d (n "IntegerScaling") (r "^1.3.2") (d #t) (k 0)))) (h "1cy2l7yvwmgsa9nc30vpbdqzhgsx3j1l9g7qp0iwnh3416n7v6a2")))

