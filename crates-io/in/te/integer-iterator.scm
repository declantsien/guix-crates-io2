(define-module (crates-io in te integer-iterator) #:use-module (crates-io))

(define-public crate-integer-iterator-0.1.0 (c (n "integer-iterator") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "0p8s3vv3i0hm5gi2grm22jrd8mgp1gxd9kqi1hpwvr9v5lr7psjg")))

(define-public crate-integer-iterator-0.2.0 (c (n "integer-iterator") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "1qnw2zzn7q818b9x8a0djfpbblaqfvkhh4sv3011kv2ji0mahydw")))

(define-public crate-integer-iterator-0.3.0 (c (n "integer-iterator") (v "0.3.0") (h "0k67py1qb45miaq12lw6f02zz1c61hd6m369f130sw9njirz9grf")))

