(define-module (crates-io in te intel_mcs51_cpu) #:use-module (crates-io))

(define-public crate-intel_mcs51_cpu-0.1.0 (c (n "intel_mcs51_cpu") (v "0.1.0") (h "1xwsyg0cdmhkl5fbhbwwfp6n3gfj0mx16c0gw13h0grz3x8adlk8")))

(define-public crate-intel_mcs51_cpu-0.1.1 (c (n "intel_mcs51_cpu") (v "0.1.1") (h "1xq2qw0d78cklqk1b4vvcjc45153mkvg4jkgqr7p02ghcvgqhpqz")))

