(define-module (crates-io in te interakt) #:use-module (crates-io))

(define-public crate-interakt-0.1.0 (c (n "interakt") (v "0.1.0") (h "18ii8mw1isjwnq115znslhrw5jx66kd64nazzb6bhvjjzlbp1za0")))

(define-public crate-interakt-0.1.1 (c (n "interakt") (v "0.1.1") (h "08x2dd8mgm7jaf52f1qz7sw5wx9ixwl8d0rb0ny9r6ypw5972wyh")))

