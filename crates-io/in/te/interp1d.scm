(define-module (crates-io in te interp1d) #:use-module (crates-io))

(define-public crate-interp1d-0.1.0 (c (n "interp1d") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.5.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1jliar6p893cx0cm6kgkny5nnzl4rhpclxaczql95l5wb16z7vda")))

(define-public crate-interp1d-0.1.1 (c (n "interp1d") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.5.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "06b391b0wn3yq41fip61y4vyszxbgs1kwyas5la143y905pqljj5")))

(define-public crate-interp1d-0.2.0 (c (n "interp1d") (v "0.2.0") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "num-complex") (r "^0.4.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.5.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1li9418iwbh58x9hkzck63nl9szd2mwdfxzrljidmfy64qyqz8yk")))

