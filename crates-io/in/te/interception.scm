(define-module (crates-io in te interception) #:use-module (crates-io))

(define-public crate-interception-0.1.0 (c (n "interception") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "interception-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.0") (d #t) (k 0)))) (h "1sq298k6zny6p66qgspskzwsg9s96sb5m36zimqi5pyh1xj07dwr")))

(define-public crate-interception-0.1.1 (c (n "interception") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "interception-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.0") (d #t) (k 0)))) (h "0dvqsazbgrmvvwarkppvkvzyw5cr1l67phy2fn0klg92f0nvwkr9")))

(define-public crate-interception-0.1.2 (c (n "interception") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "interception-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cqp5g83l7m76gjr3csnds9nsf8024m8jyw0x73g7p3gihhxrd3n")))

