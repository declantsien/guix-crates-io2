(define-module (crates-io in te integer-cbrt) #:use-module (crates-io))

(define-public crate-integer-cbrt-0.1.0 (c (n "integer-cbrt") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0s0b8l497d0c99g77y43mivp9k8p6nh9ap77k1y4x3ivn8yjxq6q")))

(define-public crate-integer-cbrt-0.1.1 (c (n "integer-cbrt") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0gglix9vmsj8jzlnbscj3c7b30z426wn38pcqxl9l2a27yhzqgyj")))

(define-public crate-integer-cbrt-0.1.2 (c (n "integer-cbrt") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "134lnaxlqvkqj8xk8bkkg6zp77677hsk4b6iqwqshzdsh52cw6qm")))

