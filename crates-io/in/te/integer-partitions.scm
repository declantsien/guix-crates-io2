(define-module (crates-io in te integer-partitions) #:use-module (crates-io))

(define-public crate-integer-partitions-0.1.0 (c (n "integer-partitions") (v "0.1.0") (h "19585abfn85zmkj146gsp1yy5cxyzlzmzxdr1xhmlxn2i6gqz6w6")))

(define-public crate-integer-partitions-0.1.1 (c (n "integer-partitions") (v "0.1.1") (h "060svq44ybzv2cw04by8f8sppbx81n809ldnvd7sjpdn46d649h4")))

