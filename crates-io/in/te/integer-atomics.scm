(define-module (crates-io in te integer-atomics) #:use-module (crates-io))

(define-public crate-integer-atomics-1.0.0 (c (n "integer-atomics") (v "1.0.0") (h "0l363yh8ka3q8f1csa53xcbld0p0mj3flrdlxvyhp4nbi7wajzvb") (f (quote (("nightly"))))))

(define-public crate-integer-atomics-1.0.1 (c (n "integer-atomics") (v "1.0.1") (h "17k9mzp8v532a9ppx85kib7kl2na6rraqjv5kzryhmvlfqzad7zj") (f (quote (("nightly"))))))

(define-public crate-integer-atomics-1.0.2 (c (n "integer-atomics") (v "1.0.2") (h "0fa184z8md5ljy1p74y18wg8qmp8chiykq5cr9ky3bdl316wscsw") (f (quote (("nightly"))))))

