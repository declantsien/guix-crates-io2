(define-module (crates-io in te intercomm) #:use-module (crates-io))

(define-public crate-intercomm-0.1.0 (c (n "intercomm") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "parking_lot"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "04fgb0bf37vr2b2axzg5vkc7y546hg9ashss8z84c5f4ivc0cqk5") (r "1.56")))

(define-public crate-intercomm-0.1.1 (c (n "intercomm") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "parking_lot"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1mnm5hjx595wvngxjnj8f9af96ajkj9arsg8v9k9llil0mrzipjr") (r "1.56")))

