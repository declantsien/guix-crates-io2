(define-module (crates-io in te interpolation) #:use-module (crates-io))

(define-public crate-interpolation-0.0.0 (c (n "interpolation") (v "0.0.0") (h "0qhr8vp6ngpnxp26mn0xn8kdihd7az3k9vgvlv04w9qxn7igkskq")))

(define-public crate-interpolation-0.0.1 (c (n "interpolation") (v "0.0.1") (h "01cgcc0n0d249k0ncis0kxdxaxyvhzlym2f3j8b7ys0jj617qxv9")))

(define-public crate-interpolation-0.0.2 (c (n "interpolation") (v "0.0.2") (h "01brfnnwk1xjplnxc8f2sx223j88pgzi48m7navsnrhkjn7cq9ri")))

(define-public crate-interpolation-0.0.3 (c (n "interpolation") (v "0.0.3") (h "0g57xlwgqhc6pq7s7wa6fp3d9m7sfc4a90yy1mrm4d03m70d1p0h")))

(define-public crate-interpolation-0.0.4 (c (n "interpolation") (v "0.0.4") (h "1b6sly1va04i602anyfln8sir2aqk523ny3amf2bc808nhrdjlxz")))

(define-public crate-interpolation-0.0.5 (c (n "interpolation") (v "0.0.5") (h "0y7p3x0r9x89qb8g9zndm20ml55jj8a40zpahc7mqkx58wkyq6pq")))

(define-public crate-interpolation-0.0.6 (c (n "interpolation") (v "0.0.6") (h "0n7yzkn67rmyk00k5zwv3z2zxrx6blww3cjnl8p7bqljxb6li2qq")))

(define-public crate-interpolation-0.1.0 (c (n "interpolation") (v "0.1.0") (h "0vp67zckvzxh3a0ix6jrgw813vh43nxbbnrw5m656dgpfwl3xrc4")))

(define-public crate-interpolation-0.2.0 (c (n "interpolation") (v "0.2.0") (h "00icvvgc72zdgyrwwg2p0wad4hry4d2vd6l9iqpyjpmw5dykbdyk")))

(define-public crate-interpolation-0.3.0 (c (n "interpolation") (v "0.3.0") (h "1kk9nbw7bwbha7v277pajm9s20kx5964qrdnmasgqj0iv7lkmhg9")))

