(define-module (crates-io in te integerscaling) #:use-module (crates-io))

(define-public crate-IntegerScaling-1.0.0 (c (n "IntegerScaling") (v "1.0.0") (h "12p5k1jq7gr5g29w1n16hmq2d70jp53mzsxym255xg3dglpk41mr") (y #t)))

(define-public crate-IntegerScaling-1.0.1 (c (n "IntegerScaling") (v "1.0.1") (h "03qicg6cp1zfcavd326jahsbh9izb0hwja6pzc3zmwfbdvvvb625") (y #t)))

(define-public crate-IntegerScaling-1.1.0 (c (n "IntegerScaling") (v "1.1.0") (h "0zlrvwh2r54qa0awdsg7106frjsyczdjxdhm72ml6zwwx6ydznv2") (y #t)))

(define-public crate-IntegerScaling-1.2.0 (c (n "IntegerScaling") (v "1.2.0") (h "180pcziala3yahm6qqjj1qbjxbldgs94hb8mn0aqpa9vsa4nm9q4")))

(define-public crate-IntegerScaling-1.2.1 (c (n "IntegerScaling") (v "1.2.1") (h "0cpqa983n3025kifddpsvrxiczmi1x9lwfdinkc74786s7jabbsk")))

(define-public crate-IntegerScaling-1.2.2 (c (n "IntegerScaling") (v "1.2.2") (h "115n7qcl760a945kh7xlh6g03b0rv37c2v7cvpxgy2gbm8d7wy1q") (y #t)))

(define-public crate-IntegerScaling-1.2.3 (c (n "IntegerScaling") (v "1.2.3") (h "0vc45gy3ysaan8vwijws7ddrwkh1lvzbnk434hn5qi3g3dx02is4")))

(define-public crate-IntegerScaling-1.3.0 (c (n "IntegerScaling") (v "1.3.0") (h "02svh0jrfvwnxibcxj7nd9pswjx6x08sjss3g935vbybma0f18lv") (y #t)))

(define-public crate-IntegerScaling-1.3.2 (c (n "IntegerScaling") (v "1.3.2") (h "0rarpqswg1n9xa0kk426vx5k2mvgb91phha3fw5sxyp7k9n8adpv")))

