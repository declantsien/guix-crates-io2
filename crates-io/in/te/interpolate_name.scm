(define-module (crates-io in te interpolate_name) #:use-module (crates-io))

(define-public crate-interpolate_name-0.1.0 (c (n "interpolate_name") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "192y5n8fqbx0gakaazcnlqfprk2dvr9sgv9kca9gc6gal3nj46js")))

(define-public crate-interpolate_name-0.1.1 (c (n "interpolate_name") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "15w3zsv1hf5nw0wic5rliq7225xs7619lzqdhhp6hswx74xvc8yg")))

(define-public crate-interpolate_name-0.1.2 (c (n "interpolate_name") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "09w5l6pzzbiavp2bk86r2py8a0mcc6iv6shkff5l3qdwzsg3qhmy")))

(define-public crate-interpolate_name-0.2.0 (c (n "interpolate_name") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0aqxv92znvh4f4l52f969pr9nwpkjp34x9l98cm01blwh0adrn0p")))

(define-public crate-interpolate_name-0.2.1 (c (n "interpolate_name") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "0vsnjbbsgp8rinkcnj3y9s2n584n3c9k6zd536hagwkd4dvl4id3")))

(define-public crate-interpolate_name-0.2.2 (c (n "interpolate_name") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "0gzrhn4nv6vcxagb1z5r1skbncbj6lfrkl2kds0pyd98ym5gn786")))

(define-public crate-interpolate_name-0.2.3 (c (n "interpolate_name") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "05vzsiqb69d1mbpaphcg4ifjsjs6g03b8pacskfcydqhh555zcxl")))

(define-public crate-interpolate_name-0.2.4 (c (n "interpolate_name") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "0q7s5mrfkx4p56dl8q9zq71y1ysdj4shh6f28qf9gly35l21jj63")))

