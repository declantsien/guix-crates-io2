(define-module (crates-io in te intertrait-macros) #:use-module (crates-io))

(define-public crate-intertrait-macros-0.1.0 (c (n "intertrait-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "19lfzizl69493ci2089hk9cbwy9zwy5jybclvxw1wp78vil1qpbd")))

(define-public crate-intertrait-macros-0.1.1 (c (n "intertrait-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "12lrfjvhr04mn063g9cqzmdzbx8mzlshwxvbabbh4jsyrwwsnfhq")))

(define-public crate-intertrait-macros-0.2.0 (c (n "intertrait-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "07jb6g1lnaf5v6i7bfjf4ac0bj7ci9rl45ang9sv9wdffa25kkgd")))

(define-public crate-intertrait-macros-0.2.1 (c (n "intertrait-macros") (v "0.2.1") (d (list (d (n "intertrait") (r "=0.2.1") (d #t) (k 2)) (d (n "linkme") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1knwkhc4p93jbhqdr51n0326bz0nvgn8h8p9w4rdb0353m7hkfbx")))

(define-public crate-intertrait-macros-0.2.2 (c (n "intertrait-macros") (v "0.2.2") (d (list (d (n "intertrait") (r "=0.2.2") (d #t) (k 2)) (d (n "linkme") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1f3vx58625wrzr3c4hgn7114ydd9clp8jqw4vvbxdjfll96rhmjd")))

