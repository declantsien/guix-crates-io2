(define-module (crates-io in te intel-8080-emu) #:use-module (crates-io))

(define-public crate-intel-8080-emu-0.1.0 (c (n "intel-8080-emu") (v "0.1.0") (h "0srx4xsmlpx09p93f09l6vk6cndkzcl9m5675qd33czfm38acyx6")))

(define-public crate-intel-8080-emu-0.1.1 (c (n "intel-8080-emu") (v "0.1.1") (h "1mmhmhggw73hxp7izcr26b7xf5h5acmdxhic7dyvk2cn1q5qhw6s")))

(define-public crate-intel-8080-emu-0.2.0 (c (n "intel-8080-emu") (v "0.2.0") (h "0z1r2m84537241cj49l4ahxl5qw2fs1rscq4ykg5k2pzndlj1h1a")))

