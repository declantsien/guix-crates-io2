(define-module (crates-io in te interfaces2) #:use-module (crates-io))

(define-public crate-interfaces2-0.0.5 (c (n "interfaces2") (v "0.0.5") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "handlebars") (r "^0.29") (d #t) (k 1)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0") (d #t) (k 1)))) (h "07vbk3wfa508xyjjnh342rbbam2xknjbz3gag39k52a0h7lh6jsa")))

