(define-module (crates-io in te intel-mkl-src) #:use-module (crates-io))

(define-public crate-intel-mkl-src-0.1.0 (c (n "intel-mkl-src") (v "0.1.0") (h "0h7971rrpxjf5byf4jnjsclsbzp658ckx3vszw9wn0nyjx9xgbkw")))

(define-public crate-intel-mkl-src-0.2.0 (c (n "intel-mkl-src") (v "0.2.0") (h "1dwy21pk99hnb9yw6hxjpbm0r79jk1cnvqlzw38w2avqh69bw12q")))

(define-public crate-intel-mkl-src-0.2.1 (c (n "intel-mkl-src") (v "0.2.1") (h "098b7si4aakbmppa2524xm0x7hycffwjbxhfzbmj92wxl7kqyghz")))

(define-public crate-intel-mkl-src-0.2.2 (c (n "intel-mkl-src") (v "0.2.2") (h "1viclplx0lj3g2p0xbh3sdpr4j9h6426mb9fs77gmhch5syxhq5i")))

(define-public crate-intel-mkl-src-0.2.3 (c (n "intel-mkl-src") (v "0.2.3") (d (list (d (n "curl") (r "^0.4") (d #t) (k 1)) (d (n "procedurals") (r "^0.2") (d #t) (k 1)))) (h "1qpvfaq0lm3zzfrwpqxm1cv1cspnqyi1gy8gqm9ykssnw59azb26")))

(define-public crate-intel-mkl-src-0.2.4 (c (n "intel-mkl-src") (v "0.2.4") (h "1wszsqlr4ghy7pyvbwf6b0q0lzswkj26vhm00h5az7hgaha0xqnw")))

(define-public crate-intel-mkl-src-0.2.5 (c (n "intel-mkl-src") (v "0.2.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 1)))) (h "1nsiwf5cn54njjyy1ng6afk5dvax9xy3krspb3fkk93yag6xr7gh")))

(define-public crate-intel-mkl-src-0.3.0 (c (n "intel-mkl-src") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 1)))) (h "0l9naj4lkgr86qhzrz1mfz44y29645iirqxzahcyn7b8ck3zkylp") (l "mkl_intel_lp64")))

(define-public crate-intel-mkl-src-0.4.0 (c (n "intel-mkl-src") (v "0.4.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "reqwest") (r "^0.9") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "xz2") (r "^0.1") (d #t) (k 1)))) (h "16kq725cl4rnvfpwq9x4rl83ylcqs7d0xryagx8ijm6bdblbfabc") (l "mkl_intel_lp64")))

(define-public crate-intel-mkl-src-0.4.1 (c (n "intel-mkl-src") (v "0.4.1") (d (list (d (n "curl") (r "^0.4.25") (d #t) (k 1)) (d (n "failure") (r "^0.1.6") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)) (d (n "xz2") (r "^0.1.6") (d #t) (k 1)))) (h "0w1adcdpnbqccjri4lq0ng8n9snmb0724xfq1arbypimdmwiwn8x") (l "mkl_intel_lp64")))

(define-public crate-intel-mkl-src-0.5.0 (c (n "intel-mkl-src") (v "0.5.0") (d (list (d (n "intel-mkl-tool") (r "^0.1.0") (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 2)))) (h "155q49a7nfbq1lllchsyx8jv2q2pijrjh1w08awvrbjyfcxb6q3j") (f (quote (("use-shared") ("default")))) (l "mkl_core")))

(define-public crate-intel-mkl-src-0.6.0-alpha.0 (c (n "intel-mkl-src") (v "0.6.0-alpha.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "intel-mkl-tool") (r "^0.2.0-alpha.0") (k 1)))) (h "0vr9wn21jn4mqr0hpl6jsmfd7q9nw8fc03kp9lgn47dybxamnxjp") (f (quote (("xdg-data-home") ("mkl-static-lp64-seq") ("mkl-static-lp64-iomp") ("mkl-static-ilp64-seq") ("mkl-static-ilp64-iomp") ("mkl-dynamic-lp64-seq") ("mkl-dynamic-lp64-iomp") ("mkl-dynamic-ilp64-seq") ("mkl-dynamic-ilp64-iomp") ("download") ("default" "download" "mkl-dynamic-lp64-iomp")))) (l "mkl_core")))

(define-public crate-intel-mkl-src-0.6.0-alpha.1 (c (n "intel-mkl-src") (v "0.6.0-alpha.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "intel-mkl-tool") (r "^0.2.0-alpha.0") (k 1)))) (h "056k6klbfzqkw7k1hy826kkkl3mwqxxwcvb1ca9pbv2l9whpsmk6") (f (quote (("xdg-data-home") ("mkl-static-lp64-seq") ("mkl-static-lp64-iomp") ("mkl-static-ilp64-seq") ("mkl-static-ilp64-iomp") ("mkl-dynamic-lp64-seq") ("mkl-dynamic-lp64-iomp") ("mkl-dynamic-ilp64-seq") ("mkl-dynamic-ilp64-iomp") ("download") ("default" "download" "mkl-dynamic-lp64-iomp")))) (l "mkl_core")))

(define-public crate-intel-mkl-src-0.6.0+mkl2020.1 (c (n "intel-mkl-src") (v "0.6.0+mkl2020.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "intel-mkl-tool") (r "^0.2.0") (k 1)))) (h "09sqx8wq0fzglzncln2x68lpdcjcw0c8c0zjnjdpw267dj3zi0jn") (f (quote (("xdg-data-home") ("mkl-static-lp64-seq") ("mkl-static-lp64-iomp") ("mkl-static-ilp64-seq") ("mkl-static-ilp64-iomp") ("mkl-dynamic-lp64-seq") ("mkl-dynamic-lp64-iomp") ("mkl-dynamic-ilp64-seq") ("mkl-dynamic-ilp64-iomp") ("download" "intel-mkl-tool/archive") ("default" "download" "mkl-static-ilp64-seq")))) (l "mkl_core")))

(define-public crate-intel-mkl-src-0.7.0+mkl2020.1 (c (n "intel-mkl-src") (v "0.7.0+mkl2020.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 1)) (d (n "intel-mkl-tool") (r "^0.3.0") (k 1)))) (h "0mgjqjkm1mkq1wily2zj9ki6w0b9p6hdkb5hf1sqkg70pf9hqw4i") (f (quote (("xdg-data-home") ("mkl-static-lp64-seq") ("mkl-static-lp64-iomp") ("mkl-static-ilp64-seq") ("mkl-static-ilp64-iomp") ("mkl-dynamic-lp64-seq") ("mkl-dynamic-lp64-iomp") ("mkl-dynamic-ilp64-seq") ("mkl-dynamic-ilp64-iomp") ("download" "intel-mkl-tool/archive") ("default" "mkl-static-ilp64-seq")))) (l "mkl_core") (r "1.56.0")))

(define-public crate-intel-mkl-src-0.8.0-rc.0 (c (n "intel-mkl-src") (v "0.8.0-rc.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 1)) (d (n "intel-mkl-tool") (r "^0.8.0-rc.0") (k 1)) (d (n "ocipkg") (r "^0.2.3") (d #t) (k 1)))) (h "1r4m6rrjl7pd8xfxkyd1rmmpf03sb1z6ljziana0f42xhhlnbyrm") (f (quote (("mkl-static-lp64-seq") ("mkl-static-lp64-iomp") ("mkl-static-ilp64-seq") ("mkl-static-ilp64-iomp") ("mkl-dynamic-lp64-seq") ("mkl-dynamic-lp64-iomp") ("mkl-dynamic-ilp64-seq") ("mkl-dynamic-ilp64-iomp") ("default")))) (l "mkl_core") (r "1.61.0")))

(define-public crate-intel-mkl-src-0.8.0-rc.1 (c (n "intel-mkl-src") (v "0.8.0-rc.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 1)) (d (n "intel-mkl-tool") (r "^0.8.0-rc.0") (k 1)) (d (n "ocipkg") (r "^0.2.3") (d #t) (k 1)))) (h "17bkgy9bm9ywdahql4vs9gs686g0k3if0jjl0x715mibm4cnl454") (f (quote (("mkl-static-lp64-seq") ("mkl-static-lp64-iomp") ("mkl-static-ilp64-seq") ("mkl-static-ilp64-iomp") ("mkl-dynamic-lp64-seq") ("mkl-dynamic-lp64-iomp") ("mkl-dynamic-ilp64-seq") ("mkl-dynamic-ilp64-iomp") ("default")))) (l "mkl_core") (r "1.61.0")))

(define-public crate-intel-mkl-src-0.8.0 (c (n "intel-mkl-src") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 1)) (d (n "intel-mkl-tool") (r "^0.8.0-rc.0") (k 1)) (d (n "ocipkg") (r "^0.2.6") (d #t) (k 1)))) (h "0y8w11jnn3d55arln5lljq2cm0kx0s1wr6ikpdb47avy04zpbi0r") (f (quote (("mkl-static-lp64-seq") ("mkl-static-lp64-iomp") ("mkl-static-ilp64-seq") ("mkl-static-ilp64-iomp") ("mkl-dynamic-lp64-seq") ("mkl-dynamic-lp64-iomp") ("mkl-dynamic-ilp64-seq") ("mkl-dynamic-ilp64-iomp") ("default")))) (l "mkl_core") (r "1.61.0")))

(define-public crate-intel-mkl-src-0.8.1 (c (n "intel-mkl-src") (v "0.8.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 1)) (d (n "intel-mkl-tool") (r "^0.8.0") (k 1)) (d (n "ocipkg") (r "^0.2.8") (d #t) (k 1)))) (h "0pkzxqvrk5rf43f4wapvy15ly3d9x91vv89rhwm7fgjvrn30brrf") (f (quote (("mkl-static-lp64-seq") ("mkl-static-lp64-iomp") ("mkl-static-ilp64-seq") ("mkl-static-ilp64-iomp") ("mkl-dynamic-lp64-seq") ("mkl-dynamic-lp64-iomp") ("mkl-dynamic-ilp64-seq") ("mkl-dynamic-ilp64-iomp") ("default")))) (l "mkl_core") (r "1.61.0")))

