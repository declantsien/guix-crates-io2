(define-module (crates-io in te interior_mutability_pointer) #:use-module (crates-io))

(define-public crate-interior_mutability_pointer-0.1.0 (c (n "interior_mutability_pointer") (v "0.1.0") (h "12jc7qwpcddj6s0l2mqhpz05508r8yi2kcw3gx4bbx6xzkj5zbkv") (y #t)))

(define-public crate-interior_mutability_pointer-0.1.1 (c (n "interior_mutability_pointer") (v "0.1.1") (h "1dd6h4xbicmqh4n53kc5bz9ilxq7k6c1v05zj9cn9nhry8jwr9z0") (y #t)))

(define-public crate-interior_mutability_pointer-0.1.2 (c (n "interior_mutability_pointer") (v "0.1.2") (h "0bvrpxi1l2q56j6m5n60iwlm3r26rda0kx7lsr5r5amc1a5x2rbd") (y #t)))

(define-public crate-interior_mutability_pointer-0.2.0 (c (n "interior_mutability_pointer") (v "0.2.0") (h "06iw0hx1v2bymi6y3z2q5a72wxzijrla7wz6gk6x30zbqygfwc6p") (y #t)))

(define-public crate-interior_mutability_pointer-0.3.0 (c (n "interior_mutability_pointer") (v "0.3.0") (h "1v10c09fan8pd3jfjcv07r3g3jlxy792z71j4rcni8v4lyhlzvzy") (f (quote (("compile_failure")))) (y #t)))

(define-public crate-interior_mutability_pointer-0.3.1 (c (n "interior_mutability_pointer") (v "0.3.1") (h "16y4wh97bgzp4yhmxf4nwrg4nkf8cyzbzkqrsyfkiia8d57lh12k") (f (quote (("compile_failure")))) (y #t)))

(define-public crate-interior_mutability_pointer-0.3.2 (c (n "interior_mutability_pointer") (v "0.3.2") (h "1hzq2m2g0r0vwk84rd9izpwx4bkk1civb8c3lka7mk8dcqqlnzac") (f (quote (("compile_failure")))) (y #t)))

(define-public crate-interior_mutability_pointer-0.3.3 (c (n "interior_mutability_pointer") (v "0.3.3") (h "1br3jz8xsysl28lgb2y42zf50kj92jmpzbv0kkxrzzp8hjlaxzxi") (f (quote (("compile_failure")))) (y #t)))

(define-public crate-interior_mutability_pointer-0.3.4 (c (n "interior_mutability_pointer") (v "0.3.4") (h "0rmilcziq93z5sljl9ak3l529mmvr3k42dxv7rragip81dmq5xi1") (f (quote (("compile_failure")))) (y #t)))

(define-public crate-interior_mutability_pointer-0.3.5 (c (n "interior_mutability_pointer") (v "0.3.5") (h "064b0q0nxz0qbc9akpmb36fp0aw1rhpksqamg13hbrky4a98mpma") (f (quote (("compile_failure")))) (y #t)))

(define-public crate-interior_mutability_pointer-0.3.6 (c (n "interior_mutability_pointer") (v "0.3.6") (h "0m06mrwpsm3qwxdijdc1iyrmmbsjbzpllvwcc81xmaz45lw5jjn1") (f (quote (("compile_failure"))))))

