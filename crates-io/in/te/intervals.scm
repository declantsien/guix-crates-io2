(define-module (crates-io in te intervals) #:use-module (crates-io))

(define-public crate-intervals-1.0.0 (c (n "intervals") (v "1.0.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde_crate") (r "^1.0") (f (quote ("std" "derive"))) (o #t) (k 0) (p "serde")) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "06mplp92mqq8fl95vv7wrfihliz2mq2rn4vjj1mg1l6i8b0g79fn") (f (quote (("serde" "serde_crate") ("default"))))))

(define-public crate-intervals-1.0.1 (c (n "intervals") (v "1.0.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde_crate") (r "^1.0") (f (quote ("std" "derive"))) (o #t) (k 0) (p "serde")) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0s3p63ycri4mxlk7d677vlvr8wwsh34m34my2s44wws5dxgf9xkn") (f (quote (("serde" "serde_crate") ("default"))))))

(define-public crate-intervals-2.0.0 (c (n "intervals") (v "2.0.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde_crate") (r "^1.0") (f (quote ("std" "derive"))) (o #t) (k 0) (p "serde")) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "19pgz84473nazz95a4h7j9mjik9n6yxys9gjf6wh4qhib13dvf6s") (f (quote (("serde" "serde_crate") ("default"))))))

(define-public crate-intervals-2.0.1 (c (n "intervals") (v "2.0.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde_crate") (r "^1.0") (f (quote ("std" "derive"))) (o #t) (k 0) (p "serde")) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0jq097pw7l3w6av68771yfyb31pi3kfgqcc3lk3qi3mxrc0l66p6") (f (quote (("serde" "serde_crate") ("default"))))))

(define-public crate-intervals-2.0.2 (c (n "intervals") (v "2.0.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde_crate") (r "^1.0") (f (quote ("std" "derive"))) (o #t) (k 0) (p "serde")) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "13krl20x46yc1y8frw4yr554xhq73r94dra20x1zx032l7xrxng1") (f (quote (("serde" "serde_crate") ("default"))))))

(define-public crate-intervals-2.1.0 (c (n "intervals") (v "2.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde_crate") (r "^1.0") (f (quote ("std" "derive"))) (o #t) (k 0) (p "serde")) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1byficf5g7dx0zvvzxv4galg2fidq3vwzlcwnzmz6s4if1bgp2pv") (f (quote (("serde" "serde_crate") ("default"))))))

