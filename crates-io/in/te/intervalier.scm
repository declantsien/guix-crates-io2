(define-module (crates-io in te intervalier) #:use-module (crates-io))

(define-public crate-intervalier-0.1.0 (c (n "intervalier") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.1") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "futures-timer") (r "^2.0") (d #t) (k 0)))) (h "0w4jmdyh91rqm9zlng1k18wxm9qy9hvkq1bnlc2w1z9jam26s347")))

(define-public crate-intervalier-0.2.0 (c (n "intervalier") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.1") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "futures-timer") (r "^2.0") (d #t) (k 0)))) (h "08lr6mspzhwlx832lq5fzj96yv9kramn4b67s9n5q9ibq5c7p8xv")))

(define-public crate-intervalier-0.3.0 (c (n "intervalier") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.1") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "futures-timer") (r "^2.0") (d #t) (k 0)))) (h "1agni9ikynr3nqy2ah4z21wc2l96wvagkbd5706sm80m0v0w43bm")))

(define-public crate-intervalier-0.3.1 (c (n "intervalier") (v "0.3.1") (d (list (d (n "futures") (r "^0.3.1") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "futures-timer") (r "^2.0") (d #t) (k 0)))) (h "0c9697hr554kfqj2irq1x3hf01w3zcg1rvcaf09yn693vich880l")))

(define-public crate-intervalier-0.4.0 (c (n "intervalier") (v "0.4.0") (d (list (d (n "futures") (r "^0.3.1") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "futures-timer") (r "^2.0") (d #t) (k 0)))) (h "0xgj5pn0i5icz0426fznsl507rqh1ms55mgf2vs97x5qqw713yk4")))

