(define-module (crates-io in te integra8_impl) #:use-module (crates-io))

(define-public crate-integra8_impl-0.0.1-alpha (c (n "integra8_impl") (v "0.0.1-alpha") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0p8kmqv8l1kznn4fc00wq0p3kxm0m9k6i7j81q999k41pmzi02bg") (f (quote (("tokio-runtime") ("async-std-runtime")))) (y #t)))

(define-public crate-integra8_impl-0.0.2-alpha (c (n "integra8_impl") (v "0.0.2-alpha") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ljg9f1r2amsn1snsgy8haagmbbyv0gwijiypjj1xc3vakgl63np") (f (quote (("tokio-runtime") ("async-std-runtime"))))))

(define-public crate-integra8_impl-0.0.3-alpha (c (n "integra8_impl") (v "0.0.3-alpha") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1y7f5wijavxn2qwzgvxnb3zjixzcjfy92jrrkbzpgmfn912qw0mc") (f (quote (("tokio-runtime") ("async-std-runtime"))))))

(define-public crate-integra8_impl-0.0.4-alpha (c (n "integra8_impl") (v "0.0.4-alpha") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xd1yc4i8q13pykxs5b085qkjdsfxnxv5gzg3fp85fsirvc311wx") (f (quote (("tokio-runtime") ("async-std-runtime"))))))

(define-public crate-integra8_impl-0.0.5-rc1 (c (n "integra8_impl") (v "0.0.5-rc1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0h2fxrrnj0lvq3i08zznl6mvb589dzlwn1p8314ayhapiyl30gin") (f (quote (("tokio-runtime") ("async-std-runtime"))))))

