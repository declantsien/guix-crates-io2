(define-module (crates-io in te intel-spi) #:use-module (crates-io))

(define-public crate-intel-spi-0.1.0 (c (n "intel-spi") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0yfyx5ycxsym3czbr6rr7yifbjdz48dsmx8h1mrvkniahb231wka")))

(define-public crate-intel-spi-0.1.1 (c (n "intel-spi") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dlqs13dmbdfh7a5vmmwpgpydjx3ydb89d3y59r7hcvxasgag2f5")))

(define-public crate-intel-spi-0.1.2 (c (n "intel-spi") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hj0af58v4f7vdmdr66cmnnzcmdmr008glxj9nhq3v411cy8ylxk")))

(define-public crate-intel-spi-0.1.3 (c (n "intel-spi") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "coreboot-fs") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qqkgygg172n1s9nx4cfh1sbp2cr1z5jn9jjylqm2kp3maw8kh3q")))

(define-public crate-intel-spi-0.1.4 (c (n "intel-spi") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "coreboot-fs") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "redox_intelflash") (r "^0.1.2") (d #t) (k 0)))) (h "1z3xcnfihd4pj3j0xr29j5mwahm9677y6myym0f5wfnd0crilhnx")))

(define-public crate-intel-spi-0.1.5 (c (n "intel-spi") (v "0.1.5") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "coreboot-fs") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "redox_intelflash") (r "^0.1.2") (d #t) (k 0)))) (h "0zzjba1afynnr7l5zz775ybscpmdmf6llvsdwksnd8agha5ahjr1")))

(define-public crate-intel-spi-0.1.6 (c (n "intel-spi") (v "0.1.6") (d (list (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "coreboot-fs") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "redox_intelflash") (r "^0.1.3") (d #t) (k 0)))) (h "1imj088rbnjbhsplcw4nl379ihbi3ghg1smmls6z7xdcw9ssb5m8")))

