(define-module (crates-io in te interception-mock) #:use-module (crates-io))

(define-public crate-interception-mock-0.0.1 (c (n "interception-mock") (v "0.0.1") (h "0am8cgbxdjdj5gzvvyyzrhjm8mcm3dn4yzhkvkbxpwilfqj1m5zb")))

(define-public crate-interception-mock-0.0.2 (c (n "interception-mock") (v "0.0.2") (h "0fqskarfvsinv4lr0lpwlsan212501q6yakcg2pi8h4cdzl9vlpg")))

