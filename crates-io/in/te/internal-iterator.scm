(define-module (crates-io in te internal-iterator) #:use-module (crates-io))

(define-public crate-internal-iterator-0.0.1 (c (n "internal-iterator") (v "0.0.1") (h "14lxg2zfl927fx8ypaa09z7zndhmkv6iilcdx3xcn8d0z2sfi1z2")))

(define-public crate-internal-iterator-0.0.2 (c (n "internal-iterator") (v "0.0.2") (h "0iyd27zdknclq8fibkm71jhcx721fp4m2swmgpfinkl997spz7fl") (f (quote (("std" "alloc") ("default" "alloc" "std") ("alloc"))))))

(define-public crate-internal-iterator-0.1.0 (c (n "internal-iterator") (v "0.1.0") (h "0z62mxv35ca83drhaqkw2kfrmcmq1jvvw17lm06ydyjfy7xq7kpm") (f (quote (("std" "alloc") ("default" "alloc" "std") ("alloc"))))))

(define-public crate-internal-iterator-0.1.1 (c (n "internal-iterator") (v "0.1.1") (h "036qkgm5adb3xkpbri3yzgsq93zgqh8jsp7l7qrfcw6hpg62cr3v") (f (quote (("std" "alloc") ("default" "alloc" "std") ("alloc"))))))

(define-public crate-internal-iterator-0.1.2 (c (n "internal-iterator") (v "0.1.2") (h "06np6dqcz5bdwvqf5zlzn9gfzc1chd38gfwjq23yhnm1ymgj5744") (f (quote (("std" "alloc") ("default" "alloc" "std") ("alloc"))))))

(define-public crate-internal-iterator-0.2.0 (c (n "internal-iterator") (v "0.2.0") (h "0v3fwdaxp45208w2gzzaf482lgw9l60plcmimkhxqy38hjqp2x5p") (f (quote (("std" "alloc") ("default" "alloc" "std") ("alloc"))))))

(define-public crate-internal-iterator-0.2.1 (c (n "internal-iterator") (v "0.2.1") (h "1x9k8hf45xx699qg7wjqg6if3v595l34hkypm5nkcqva0m3fys56") (f (quote (("std" "alloc") ("default" "alloc" "std") ("alloc"))))))

(define-public crate-internal-iterator-0.2.2 (c (n "internal-iterator") (v "0.2.2") (h "1bw1ayp69m3a0sfbaqcv0a6fn9in07syvs3j1ynlf8rk8394l44g") (f (quote (("std" "alloc") ("default" "alloc" "std") ("alloc"))))))

(define-public crate-internal-iterator-0.2.3 (c (n "internal-iterator") (v "0.2.3") (h "1f94a57k0jkc19b79y9gx4f0sq71nzcy8k2347mqhbpcd3yf77ln") (f (quote (("std" "alloc") ("default" "alloc" "std") ("alloc"))))))

