(define-module (crates-io in te interoptopus_backend_cpython_cffi) #:use-module (crates-io))

(define-public crate-interoptopus_backend_cpython_cffi-0.1.1 (c (n "interoptopus_backend_cpython_cffi") (v "0.1.1") (d (list (d (n "interoptopus") (r "^0.1") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.1") (d #t) (k 0)) (d (n "interoptopus_reference_project") (r "^0.1") (d #t) (k 2)))) (h "0rh1jmccs3kmaylyjzkdwqb2zivwiiy2g68ksz9gki8q5w2rr53d")))

(define-public crate-interoptopus_backend_cpython_cffi-0.1.3 (c (n "interoptopus_backend_cpython_cffi") (v "0.1.3") (d (list (d (n "interoptopus") (r "^0.1") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.1") (d #t) (k 0)) (d (n "interoptopus_reference_project") (r "^0.1") (d #t) (k 2)))) (h "06inca3xf4x4mxld93y9wv10ik7c9g8hlhdabbkwkgv0afzrmp1h")))

(define-public crate-interoptopus_backend_cpython_cffi-0.1.5 (c (n "interoptopus_backend_cpython_cffi") (v "0.1.5") (d (list (d (n "interoptopus") (r "^0.1") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.1") (d #t) (k 0)) (d (n "interoptopus_reference_project") (r "^0.1") (d #t) (k 2)))) (h "03cmady23xdsziylbzmyg38hn14c0mm6s3bxk1shf6za9q7ygqld")))

(define-public crate-interoptopus_backend_cpython_cffi-0.1.6 (c (n "interoptopus_backend_cpython_cffi") (v "0.1.6") (d (list (d (n "interoptopus") (r "^0.1") (d #t) (k 0)) (d (n "interoptopus") (r "^0.1") (f (quote ("testing"))) (d #t) (k 2)) (d (n "interoptopus_backend_c") (r "^0.1") (d #t) (k 0)) (d (n "interoptopus_reference_project") (r "^0.1") (d #t) (k 2)))) (h "189ifza06pvy8y26m5fyknclnsn923dyinb7hsis9g066a282cnp")))

(define-public crate-interoptopus_backend_cpython_cffi-0.1.7 (c (n "interoptopus_backend_cpython_cffi") (v "0.1.7") (d (list (d (n "interoptopus") (r "^0.1") (d #t) (k 0)) (d (n "interoptopus") (r "^0.1") (f (quote ("testing"))) (d #t) (k 2)) (d (n "interoptopus_backend_c") (r "^0.1") (d #t) (k 0)) (d (n "interoptopus_reference_project") (r "^0.1") (d #t) (k 2)))) (h "1j555swg2p3phh0p4lp0wzzi3v29bwnj9c5brx92k9639lm1s4mg")))

(define-public crate-interoptopus_backend_cpython_cffi-0.1.8 (c (n "interoptopus_backend_cpython_cffi") (v "0.1.8") (d (list (d (n "interoptopus") (r "^0.1") (d #t) (k 0)) (d (n "interoptopus") (r "^0.1") (f (quote ("testing"))) (d #t) (k 2)) (d (n "interoptopus_backend_c") (r "^0.1") (d #t) (k 0)) (d (n "interoptopus_reference_project") (r "^0.1") (d #t) (k 2)))) (h "1j1mq4pjdl6z2qvdp0x5scibdskkqrymrw982hpkd03zs2nkw3y9")))

(define-public crate-interoptopus_backend_cpython_cffi-0.1.12 (c (n "interoptopus_backend_cpython_cffi") (v "0.1.12") (d (list (d (n "interoptopus") (r "^0.1") (d #t) (k 0)) (d (n "interoptopus") (r "^0.1") (f (quote ("testing"))) (d #t) (k 2)) (d (n "interoptopus_backend_c") (r "^0.1") (d #t) (k 0)) (d (n "interoptopus_reference_project") (r "^0.1") (d #t) (k 2)))) (h "0gpry765c90z5jzzz6wygn2060zz056mqbbmyi4mxzagvfbjwhrw")))

(define-public crate-interoptopus_backend_cpython_cffi-0.1.13 (c (n "interoptopus_backend_cpython_cffi") (v "0.1.13") (d (list (d (n "interoptopus") (r "^0.1") (d #t) (k 0)) (d (n "interoptopus") (r "^0.1") (f (quote ("testing"))) (d #t) (k 2)) (d (n "interoptopus_backend_c") (r "^0.1") (d #t) (k 0)) (d (n "interoptopus_reference_project") (r "^0.1") (d #t) (k 2)))) (h "05c3gc2gbbm8wzvlmcgq2iw6m8xy7vhra6w5wmgyc515478linsr")))

(define-public crate-interoptopus_backend_cpython_cffi-0.1.14 (c (n "interoptopus_backend_cpython_cffi") (v "0.1.14") (d (list (d (n "interoptopus") (r "^0.1") (d #t) (k 0)) (d (n "interoptopus") (r "^0.1") (f (quote ("testing"))) (d #t) (k 2)) (d (n "interoptopus_backend_c") (r "^0.1") (d #t) (k 0)) (d (n "interoptopus_reference_project") (r "^0.1") (d #t) (k 2)))) (h "0x17jhnmlgm33wi77l6bv85hgyziqs8fghw9hci9xw3xljj0gm6p")))

(define-public crate-interoptopus_backend_cpython_cffi-0.1.15 (c (n "interoptopus_backend_cpython_cffi") (v "0.1.15") (d (list (d (n "interoptopus") (r "^0.1") (d #t) (k 0)) (d (n "interoptopus") (r "^0.1") (f (quote ("testing"))) (d #t) (k 2)) (d (n "interoptopus_backend_c") (r "^0.1") (d #t) (k 0)) (d (n "interoptopus_reference_project") (r "^0.1") (d #t) (k 2)))) (h "1dh3lh8rbm2yxkh99g1g9bjrkgymr55drzxrrm8mknk4wc7pgqcs")))

(define-public crate-interoptopus_backend_cpython_cffi-0.1.16 (c (n "interoptopus_backend_cpython_cffi") (v "0.1.16") (d (list (d (n "interoptopus") (r "^0.1") (d #t) (k 0)) (d (n "interoptopus") (r "^0.1") (f (quote ("testing"))) (d #t) (k 2)) (d (n "interoptopus_backend_c") (r "^0.1") (d #t) (k 0)) (d (n "interoptopus_reference_project") (r "^0.1") (d #t) (k 2)))) (h "1pl62ayjl1909zli6j4pvhq42xr4a2wfxpkxcji93wlkmk13g2dd")))

(define-public crate-interoptopus_backend_cpython_cffi-0.1.17 (c (n "interoptopus_backend_cpython_cffi") (v "0.1.17") (d (list (d (n "interoptopus") (r "^0.1") (d #t) (k 0)) (d (n "interoptopus") (r "^0.1") (f (quote ("testing"))) (d #t) (k 2)) (d (n "interoptopus_backend_c") (r "^0.1") (d #t) (k 0)) (d (n "interoptopus_reference_project") (r "^0.1") (d #t) (k 2)))) (h "08syfwh6z3rlnb25r9g6nkwhp33yldadgk47ri6crn7q0d5g6v5f")))

(define-public crate-interoptopus_backend_cpython_cffi-0.2.1 (c (n "interoptopus_backend_cpython_cffi") (v "0.2.1") (d (list (d (n "interoptopus") (r "^0.2.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.2.0") (d #t) (k 0)))) (h "08inzfniy992zz7mhw15mb12gwszxrdnj2ch261ba3zg08z51ni6")))

(define-public crate-interoptopus_backend_cpython_cffi-0.2.2 (c (n "interoptopus_backend_cpython_cffi") (v "0.2.2") (d (list (d (n "interoptopus") (r "^0.2.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.2.0") (d #t) (k 0)))) (h "167h7w9vgvqk9g89bwwcj8gcrhmxiw40rmy86qmis1ark8fhh6g2")))

(define-public crate-interoptopus_backend_cpython_cffi-0.2.3 (c (n "interoptopus_backend_cpython_cffi") (v "0.2.3") (d (list (d (n "interoptopus") (r "^0.2.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.2.0") (d #t) (k 0)))) (h "0ds1cr7ijb9ba38xp924pk4ax3kzrj1iacshd7hjjcpjxgnkcwq1")))

(define-public crate-interoptopus_backend_cpython_cffi-0.2.4 (c (n "interoptopus_backend_cpython_cffi") (v "0.2.4") (d (list (d (n "interoptopus") (r "^0.2.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.2.0") (d #t) (k 0)))) (h "18dk6gd4ln8a0kxdh2prsc4a4759wxd4021bz8fzqqki0l9m0bxn")))

(define-public crate-interoptopus_backend_cpython_cffi-0.2.5 (c (n "interoptopus_backend_cpython_cffi") (v "0.2.5") (d (list (d (n "interoptopus") (r "^0.2.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.2.0") (d #t) (k 0)))) (h "0rc1b5q7r1p0prz4d8hsdnc4wwsgamqq6rk02xs33551j89m55hc")))

(define-public crate-interoptopus_backend_cpython_cffi-0.2.6 (c (n "interoptopus_backend_cpython_cffi") (v "0.2.6") (d (list (d (n "interoptopus") (r "^0.2.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.2.0") (d #t) (k 0)))) (h "1c37rq48sdj9ci5nkw1ax94rf4yn3lwyjya7azydv3l1gc7mkysr")))

(define-public crate-interoptopus_backend_cpython_cffi-0.2.7 (c (n "interoptopus_backend_cpython_cffi") (v "0.2.7") (d (list (d (n "interoptopus") (r "^0.2.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.2.0") (d #t) (k 0)))) (h "1zxgqq10vdq5wfsmzwwn4z8mm61wj3084n50c3zl3zhan3l6l60n")))

(define-public crate-interoptopus_backend_cpython_cffi-0.3.0 (c (n "interoptopus_backend_cpython_cffi") (v "0.3.0") (d (list (d (n "interoptopus") (r "^0.3.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.3.0") (d #t) (k 0)))) (h "0mrr25jj7x0hzin9055gb9cwgykfzpsaqkvk2mvzdz0ylv2slim5")))

(define-public crate-interoptopus_backend_cpython_cffi-0.3.1 (c (n "interoptopus_backend_cpython_cffi") (v "0.3.1") (d (list (d (n "interoptopus") (r "^0.3.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.3.0") (d #t) (k 0)))) (h "081acn6iglxjimgzyhrij66q30l7bvcvpiz10jc19lazlkvgifdb")))

(define-public crate-interoptopus_backend_cpython_cffi-0.3.2 (c (n "interoptopus_backend_cpython_cffi") (v "0.3.2") (d (list (d (n "interoptopus") (r "^0.3.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.3.0") (d #t) (k 0)))) (h "0rbigl27nyh8yv5pk82fl033k6gcyp0fjknlcrspqnw68xahdscr")))

(define-public crate-interoptopus_backend_cpython_cffi-0.3.3 (c (n "interoptopus_backend_cpython_cffi") (v "0.3.3") (d (list (d (n "interoptopus") (r "^0.3.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.3.0") (d #t) (k 0)))) (h "1mm1wxkwayfmhkg6p056mnphqmlrl0gh0q98z55b5yh2zx63vk5r")))

(define-public crate-interoptopus_backend_cpython_cffi-0.3.4 (c (n "interoptopus_backend_cpython_cffi") (v "0.3.4") (d (list (d (n "interoptopus") (r "^0.3.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.3.0") (d #t) (k 0)))) (h "0jrxr0l3p7pniwyisjqih6v3khnfw0nb0wdss4rf4wxwv17m0d25")))

(define-public crate-interoptopus_backend_cpython_cffi-0.3.5 (c (n "interoptopus_backend_cpython_cffi") (v "0.3.5") (d (list (d (n "interoptopus") (r "^0.3.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.3.0") (d #t) (k 0)))) (h "1rrvgdx9gw6fhwqccwfj9vfy26r059vb5n4snmhhlcvf93yzc76r")))

(define-public crate-interoptopus_backend_cpython_cffi-0.4.0 (c (n "interoptopus_backend_cpython_cffi") (v "0.4.0") (d (list (d (n "interoptopus") (r "^0.4.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.4.0") (d #t) (k 0)))) (h "0angjj0l7klyxys1lhcmlvni98v7z5cksyf2xfxzn8dy4vdgr4lk")))

(define-public crate-interoptopus_backend_cpython_cffi-0.5.0 (c (n "interoptopus_backend_cpython_cffi") (v "0.5.0") (d (list (d (n "interoptopus") (r "^0.5.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.5.0") (d #t) (k 0)))) (h "0131zmhr348im08f91nr35gd13hicwzrfbiysh38hmd5cwk1v7l9")))

(define-public crate-interoptopus_backend_cpython_cffi-0.5.1 (c (n "interoptopus_backend_cpython_cffi") (v "0.5.1") (d (list (d (n "interoptopus") (r "^0.5.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.5.0") (d #t) (k 0)))) (h "0yh9qvs01vmy6hj0pn0dm4nxvrhxirwih4ss9pyl4zz3093hlcgb")))

(define-public crate-interoptopus_backend_cpython_cffi-0.6.0 (c (n "interoptopus_backend_cpython_cffi") (v "0.6.0") (d (list (d (n "interoptopus") (r "^0.6.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.6.0") (d #t) (k 0)))) (h "01v3ypkdr6vlcqk305mx9cw3y3f9y9finl6fn9xx30kzpk3m70hn")))

(define-public crate-interoptopus_backend_cpython_cffi-0.7.0 (c (n "interoptopus_backend_cpython_cffi") (v "0.7.0") (d (list (d (n "interoptopus") (r "^0.7.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.7.0") (d #t) (k 0)))) (h "0p79bgdrka9l318hv19yn9dzi4mrd8hhibqbywxghr7f6nspw02k")))

(define-public crate-interoptopus_backend_cpython_cffi-0.7.2 (c (n "interoptopus_backend_cpython_cffi") (v "0.7.2") (d (list (d (n "interoptopus") (r "^0.7.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.7.0") (d #t) (k 0)))) (h "144pd39vb296amajfd928aa73xpnd07zpmrjzvwl4bs5qg53q4lk")))

(define-public crate-interoptopus_backend_cpython_cffi-0.8.0 (c (n "interoptopus_backend_cpython_cffi") (v "0.8.0") (d (list (d (n "interoptopus") (r "^0.8.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.8.0") (d #t) (k 0)))) (h "0rapxf3jlgmr72qidgbxkygrxrnl58r4ram6f16a2aqmy2fa5zr2")))

(define-public crate-interoptopus_backend_cpython_cffi-0.8.1 (c (n "interoptopus_backend_cpython_cffi") (v "0.8.1") (d (list (d (n "interoptopus") (r "^0.8.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.8.0") (d #t) (k 0)))) (h "08haj3qiz25yhbskj5gnqq9w74iy3zasqjax9w18024nl751vncz")))

(define-public crate-interoptopus_backend_cpython_cffi-0.9.0 (c (n "interoptopus_backend_cpython_cffi") (v "0.9.0") (d (list (d (n "interoptopus") (r "^0.9.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.9.0") (d #t) (k 0)))) (h "1h4pbhby7mwllf6jaaqcaz6c2jn8879rzlc271s2qz6dippssy2m")))

(define-public crate-interoptopus_backend_cpython_cffi-0.9.1 (c (n "interoptopus_backend_cpython_cffi") (v "0.9.1") (d (list (d (n "interoptopus") (r "^0.9.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.9.0") (d #t) (k 0)))) (h "0fxsh49mryyih4mjjj0nhg3gpp1qhp57cy9ffdqcnfwzycr526y9")))

(define-public crate-interoptopus_backend_cpython_cffi-0.9.3 (c (n "interoptopus_backend_cpython_cffi") (v "0.9.3") (d (list (d (n "interoptopus") (r "^0.9.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.9.0") (d #t) (k 0)))) (h "0iifra5djf4417nj0kaksrj2lfjl2rc94nkjhzw5kzpqvxjlwk98")))

(define-public crate-interoptopus_backend_cpython_cffi-0.9.4 (c (n "interoptopus_backend_cpython_cffi") (v "0.9.4") (d (list (d (n "interoptopus") (r "^0.9.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.9.0") (d #t) (k 0)))) (h "0jsya6nx823dca4c1jlykr9sszzb1vd7pkbgnal5287qp6nicpm7")))

(define-public crate-interoptopus_backend_cpython_cffi-0.9.5 (c (n "interoptopus_backend_cpython_cffi") (v "0.9.5") (d (list (d (n "interoptopus") (r "^0.9.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.9.0") (d #t) (k 0)))) (h "07iz78h4jkd0mymk6rfimgnzdd8syimvl9v1jai384hwsqhq3ihy")))

(define-public crate-interoptopus_backend_cpython_cffi-0.10.0 (c (n "interoptopus_backend_cpython_cffi") (v "0.10.0") (d (list (d (n "interoptopus") (r "^0.10.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.10.0") (d #t) (k 0)))) (h "01k2h7hdl5mz66jbn9sjlrccm1wz1y6y361drzajagahj8x02kq1")))

(define-public crate-interoptopus_backend_cpython_cffi-0.10.1 (c (n "interoptopus_backend_cpython_cffi") (v "0.10.1") (d (list (d (n "interoptopus") (r "^0.10.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.10.0") (d #t) (k 0)))) (h "1cgvzgpx4b31h3wihczz7z50zfsavga8vsqkswmqwn577jblnxn8")))

(define-public crate-interoptopus_backend_cpython_cffi-0.11.0 (c (n "interoptopus_backend_cpython_cffi") (v "0.11.0") (d (list (d (n "interoptopus") (r "^0.11.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.11.0") (d #t) (k 0)))) (h "16vgd6hkg7zakiqgbx0xz2z62vjpapj8hgica6f7yl1arlznrvvv")))

(define-public crate-interoptopus_backend_cpython_cffi-0.12.0 (c (n "interoptopus_backend_cpython_cffi") (v "0.12.0") (d (list (d (n "interoptopus") (r "^0.12.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.12.0") (d #t) (k 0)))) (h "140h185d0hiwz1l98wd4i7psd9crml9bbvmgj02ha23xxiqplgh6")))

(define-public crate-interoptopus_backend_cpython_cffi-0.13.0 (c (n "interoptopus_backend_cpython_cffi") (v "0.13.0") (d (list (d (n "interoptopus") (r "^0.13.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.13.0") (d #t) (k 0)))) (h "1wzdbi7xgzy0ywz11f90piswllhfczaarszi06hl92khf54nim0d")))

(define-public crate-interoptopus_backend_cpython_cffi-0.13.3 (c (n "interoptopus_backend_cpython_cffi") (v "0.13.3") (d (list (d (n "interoptopus") (r "^0.13.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.13.0") (d #t) (k 0)))) (h "1w2ajp28xy7ndn8qvnahg9h9493i2ca4qsp4d3c9x0lp80scm615")))

(define-public crate-interoptopus_backend_cpython_cffi-0.13.5 (c (n "interoptopus_backend_cpython_cffi") (v "0.13.5") (d (list (d (n "interoptopus") (r "^0.13.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.13.0") (d #t) (k 0)))) (h "1smg1ii677v4q23dgj8n8gwhvi20h4ikzsly9sdrgydscji5dqhi")))

(define-public crate-interoptopus_backend_cpython_cffi-0.13.6 (c (n "interoptopus_backend_cpython_cffi") (v "0.13.6") (d (list (d (n "interoptopus") (r "^0.13.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.13.0") (d #t) (k 0)))) (h "0x9bgjhyrk6r8gy21jfhr6immrvlaxyg7bgpxwacxk7b427w8wql")))

(define-public crate-interoptopus_backend_cpython_cffi-0.13.8 (c (n "interoptopus_backend_cpython_cffi") (v "0.13.8") (d (list (d (n "interoptopus") (r "^0.13.0") (d #t) (k 0)) (d (n "interoptopus_backend_c") (r "^0.13.0") (d #t) (k 0)))) (h "093k35z4gr2957gddx01zaxm3a0xb59d511p0l9fwcscp986lmi3")))

