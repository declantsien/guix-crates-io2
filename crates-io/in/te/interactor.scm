(define-module (crates-io in te interactor) #:use-module (crates-io))

(define-public crate-interactor-0.1.0 (c (n "interactor") (v "0.1.0") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "termios") (r "^0") (d #t) (k 0)))) (h "1ncck61rysmp4ljv5p9bn6ncsdv9sszv20mnzgc6cwi5n3pdxgzg")))

(define-public crate-interactor-0.1.1 (c (n "interactor") (v "0.1.1") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "termios") (r "^0") (d #t) (k 0)))) (h "0bqspk119ypw0qfw2hsnf0iz0y3ayf78dzf1qvdqk6lqkb75gjbk")))

(define-public crate-interactor-0.1.2 (c (n "interactor") (v "0.1.2") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "termios") (r "^0") (d #t) (k 0)))) (h "1im1xc157jzj2gdryb8nn2xk6h2jyxijjdiqa2h5z4f0ccjmy7pj")))

(define-public crate-interactor-0.1.3 (c (n "interactor") (v "0.1.3") (d (list (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "termios") (r "^0") (d #t) (k 0)))) (h "1rx01z36zgl0s8lwv2v7g6fg3hbxn3y8i35pc0g4l3bsxfc1vrps")))

