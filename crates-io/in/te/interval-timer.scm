(define-module (crates-io in te interval-timer) #:use-module (crates-io))

(define-public crate-interval-timer-0.1.0 (c (n "interval-timer") (v "0.1.0") (d (list (d (n "rodio") (r "^0.13.0") (d #t) (k 0)))) (h "04hz181hp41cn6z0175f309ln410qm3py9n3z9f2cw18pxn3j7sp")))

(define-public crate-interval-timer-0.1.1 (c (n "interval-timer") (v "0.1.1") (d (list (d (n "rodio") (r "^0.13.0") (d #t) (k 0)))) (h "0h71dqrcfb65sca3d4v4yc40iig8f86mi92rnxm63g3x8vj2khm0")))

