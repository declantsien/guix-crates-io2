(define-module (crates-io in te interner) #:use-module (crates-io))

(define-public crate-interner-0.1.0 (c (n "interner") (v "0.1.0") (h "17036c5q35y5nb7rjh6ibxv307kjv8vm1gxba0axzgkhnwqhivxv")))

(define-public crate-interner-0.1.1 (c (n "interner") (v "0.1.1") (h "02v5iqhldixiy07k85xvkc1lsg42y2z5hgjsqi6vyqsppr2hckiv")))

(define-public crate-interner-0.2.0 (c (n "interner") (v "0.2.0") (h "0qvyddc24pxl3h7mnnx6pnp9ly751h2yz7b2g6yby4y9xrwq3gyi") (r "1.70.0")))

(define-public crate-interner-0.2.1 (c (n "interner") (v "0.2.1") (h "1l522xpvznavppizxx8xnq0xi4khi828fci1yabajdbb0n3hdip8") (r "1.70.0")))

