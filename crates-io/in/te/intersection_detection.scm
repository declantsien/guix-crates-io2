(define-module (crates-io in te intersection_detection) #:use-module (crates-io))

(define-public crate-intersection_detection-0.1.0 (c (n "intersection_detection") (v "0.1.0") (d (list (d (n "derive-new") (r "^0.6.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "point_like") (r "^0.1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0dq91iz9hzj83ilkls2410a5hbvzi9vj4kwnayzcx45qihvql9bw")))

(define-public crate-intersection_detection-0.1.1 (c (n "intersection_detection") (v "0.1.1") (d (list (d (n "derive-new") (r "^0.6.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "point_like") (r "^0.1.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1s7rfwskzsgz2cv4sigry2y9kmfxfp1c36dsz81gr39z6h9zq8gb")))

(define-public crate-intersection_detection-0.1.2 (c (n "intersection_detection") (v "0.1.2") (d (list (d (n "derive-new") (r "^0.6.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "point_like") (r "^0.1.2") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0gvk1wyng5vcdsc3md20lgzrc2xyp4sknyn7qsymf6gxlg6w59xb")))

(define-public crate-intersection_detection-0.1.3 (c (n "intersection_detection") (v "0.1.3") (d (list (d (n "derive-new") (r "^0.6.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "point_like") (r "^0.1.2") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1c30svkxjn8lq44ivh91v8q0hcpn8gm94kf8blwmxvhp2p8dc4nx")))

