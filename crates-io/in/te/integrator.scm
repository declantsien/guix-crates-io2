(define-module (crates-io in te integrator) #:use-module (crates-io))

(define-public crate-integrator-0.1.0 (c (n "integrator") (v "0.1.0") (h "0f93fbrygn0wshxik2b1y0w816jyryv43knxg0qyxkjf8d7yv0g3")))

(define-public crate-integrator-0.1.1 (c (n "integrator") (v "0.1.1") (h "16qz7bvj7wi8vznqacgb1jh871wvz96xyay510algws6x6py0f2h")))

(define-public crate-integrator-0.1.2 (c (n "integrator") (v "0.1.2") (h "18cfnkshjxxq0nrhwyf05r4242qk8pn3ni1vis5pk2lizla004h7")))

