(define-module (crates-io in te interpol) #:use-module (crates-io))

(define-public crate-interpol-0.1.0 (c (n "interpol") (v "0.1.0") (d (list (d (n "interpol-impl") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1ni81dbr1v0r951rmlhx2rrk5qmq4dxg83zzfkxylm61bs881dkr")))

(define-public crate-interpol-0.2.0 (c (n "interpol") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ic81llc7qfbnjdz9njg7iri3djsdkp1cbdj2swl20g4p0pkpiah")))

(define-public crate-interpol-0.2.1 (c (n "interpol") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0l5fasrv628w2w65nffkmagbmcd0m1aihali2l6h3x28lwmh6n7b")))

