(define-module (crates-io in te intern-str) #:use-module (crates-io))

(define-public crate-intern-str-0.1.0 (c (n "intern-str") (v "0.1.0") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 2)) (d (n "phf") (r "^0.11.1") (d #t) (k 2)))) (h "17pddqi5m02fbia8cnva49p49jax1533s1lqxhgy8088n6rrc47a") (f (quote (("std") ("builder"))))))

