(define-module (crates-io in te inter-unit) #:use-module (crates-io))

(define-public crate-inter-unit-0.1.0 (c (n "inter-unit") (v "0.1.0") (h "0ad7hz6gx36ic8570rmw00h41zwmpv0cxp4dxpvx58chbm7im4xj")))

(define-public crate-inter-unit-0.1.1 (c (n "inter-unit") (v "0.1.1") (h "02cwb00mznsdgzjrkll5f8ly3qh60iga0rhmcy7khkgnaq6kvf6c")))

(define-public crate-inter-unit-0.1.2 (c (n "inter-unit") (v "0.1.2") (h "06sqhs1gpr2n40wb7hymlzx7mdrd57iy0zbdbv4nl9jjb6j66kzw")))

(define-public crate-inter-unit-0.1.3 (c (n "inter-unit") (v "0.1.3") (h "1rdfml0vgph256xaa70xvwngvq2zpys9xacfzq0j9cryvgg5z5rw")))

(define-public crate-inter-unit-0.1.4 (c (n "inter-unit") (v "0.1.4") (h "11qx0y6wbh97mpdkd0slwgkiv0q2hssz84fyf93midsy9v5d9i81")))

(define-public crate-inter-unit-0.1.5 (c (n "inter-unit") (v "0.1.5") (h "0q6pinqwm67br5nxrg55csyhsz43ik65bwxx0xq9w90l6y7j8sq1")))

