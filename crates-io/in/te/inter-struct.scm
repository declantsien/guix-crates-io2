(define-module (crates-io in te inter-struct) #:use-module (crates-io))

(define-public crate-inter-struct-0.1.0 (c (n "inter-struct") (v "0.1.0") (d (list (d (n "inter-struct-codegen") (r "^0.1") (d #t) (k 0)))) (h "1frd9qbsbq2s33j1m99hwwy5ahl4nyp0awjz4h5hdxjwgm6aycip") (f (quote (("debug" "inter-struct-codegen/debug"))))))

(define-public crate-inter-struct-0.2.0 (c (n "inter-struct") (v "0.2.0") (d (list (d (n "inter-struct-codegen") (r "^0.2") (d #t) (k 0)))) (h "1kriw487a4f6byzwikv26jbgrdiv8airc8jpnxh91813hpjfqbij") (f (quote (("debug" "inter-struct-codegen/debug"))))))

(define-public crate-inter-struct-0.2.1 (c (n "inter-struct") (v "0.2.1") (d (list (d (n "inter-struct-codegen") (r "^0.2") (d #t) (k 0)))) (h "0kz17i7ad52ylx8b9x7b1rv5c80bihb4clp04dmasjhvc8lpynws") (f (quote (("debug" "inter-struct-codegen/debug")))) (r "1.62")))

