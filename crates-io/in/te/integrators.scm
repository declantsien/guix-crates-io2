(define-module (crates-io in te integrators) #:use-module (crates-io))

(define-public crate-integrators-0.0.1 (c (n "integrators") (v "0.0.1") (d (list (d (n "bindgen") (r "0.43.*") (d #t) (k 1)))) (h "0g5ddzvxaxyzbym93h85v16kd01d1r6gjk9hkffmavkqb9rkpng0") (f (quote (("gsl") ("default" "cuba" "gsl") ("cuba"))))))

(define-public crate-integrators-0.0.2 (c (n "integrators") (v "0.0.2") (d (list (d (n "bindgen") (r "0.43.*") (d #t) (k 1)))) (h "1mah7b4gkda2rjh1fqgy8f7l39kanvdzlfdvf0a7vgwz1lnq8pp2") (f (quote (("gsl") ("default" "cuba" "gsl") ("cuba"))))))

(define-public crate-integrators-0.0.3 (c (n "integrators") (v "0.0.3") (d (list (d (n "bindgen") (r "0.43.*") (d #t) (k 1)))) (h "0qyjjdsrffn49c9hrqk4gcl8kcw8ihaprm6fa2fcg9mbxs7f52dh") (f (quote (("gsl") ("default" "cuba" "gsl") ("cuba"))))))

