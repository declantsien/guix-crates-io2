(define-module (crates-io in te intern-all) #:use-module (crates-io))

(define-public crate-intern-all-0.1.0 (c (n "intern-all") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.14.2") (d #t) (k 0)))) (h "0yyazkn9jvm1w0afwy1gp4f737pkrnqci0fwwffpmn0sjwjjwy47")))

(define-public crate-intern-all-0.2.0 (c (n "intern-all") (v "0.2.0") (d (list (d (n "hashbrown") (r "^0.14.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1h9ngrq9qbd6vg7xcvim8q10xynrpx2v5iqg5rvgchz26bkmb7fp")))

(define-public crate-intern-all-0.2.1 (c (n "intern-all") (v "0.2.1") (d (list (d (n "hashbrown") (r "^0.14.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (o #t) (d #t) (k 0)) (d (n "trait-set") (r "^0.3.0") (d #t) (k 0)))) (h "0fv06124in02dgmg88dfj7riz5r29jgzjd9cj8z6gw8q0id0f3rz")))

(define-public crate-intern-all-0.2.2 (c (n "intern-all") (v "0.2.2") (d (list (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "trait-set") (r "^0.3") (d #t) (k 0)))) (h "16i7ng6qclwzmvsrljvzc46lrbcqj37cvxm8028qkk77rk052lzk")))

(define-public crate-intern-all-0.2.3 (c (n "intern-all") (v "0.2.3") (d (list (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "trait-set") (r "^0.3") (d #t) (k 0)))) (h "0h05x0l7dgdjcfw2dvk5npkcam85zi6plr9kg6v1jzcdlvshy3v3")))

(define-public crate-intern-all-0.2.4 (c (n "intern-all") (v "0.2.4") (d (list (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "trait-set") (r "^0.3") (d #t) (k 0)))) (h "1qgcwq0i90gx226fphsny01c0zksqhyik6a2jk3xiqz5qcanjk72") (y #t)))

(define-public crate-intern-all-0.3.0 (c (n "intern-all") (v "0.3.0") (d (list (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "trait-set") (r "^0.3") (d #t) (k 0)))) (h "0sdvlbr0x6asf7li7wscan7ynkdilz3nnq71vjpl9qf0j9avafrb")))

(define-public crate-intern-all-0.3.1 (c (n "intern-all") (v "0.3.1") (d (list (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "trait-set") (r "^0.3") (d #t) (k 0)))) (h "0wwicinbyp37cvxn56a5n5wlrysgcz2ncrx8mrwpmmv6plr6qi5g")))

(define-public crate-intern-all-0.4.0 (c (n "intern-all") (v "0.4.0") (d (list (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "trait-set") (r "^0.3") (d #t) (k 0)))) (h "1yak9y04nlm997bhqxx81rxc8zlni7vxjypsndplhhyryc4kkn5s")))

(define-public crate-intern-all-0.4.1 (c (n "intern-all") (v "0.4.1") (d (list (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "trait-set") (r "^0.3") (d #t) (k 0)))) (h "1xn04ci9nr04miys46d33cnsj052q4xckpcg76sgfwh5gdyvzj90")))

