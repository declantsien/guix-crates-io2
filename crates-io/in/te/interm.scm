(define-module (crates-io in te interm) #:use-module (crates-io))

(define-public crate-interm-0.1.0 (c (n "interm") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32") (f (quote ("rt" "macros" "sync" "time" "rt-multi-thread"))) (d #t) (k 0)))) (h "02ha4bs1a1ngy9kpli9z5dmvwnqjihnbdpqip56kracvy21iq3kb")))

(define-public crate-interm-0.1.1 (c (n "interm") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32") (f (quote ("rt" "macros" "sync" "time" "rt-multi-thread"))) (d #t) (k 0)))) (h "0p2zzrvy9f4262wgny6pc69z321ap3hywlni34j4pyr84b7fkzk4")))

