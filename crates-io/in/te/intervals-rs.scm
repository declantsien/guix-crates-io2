(define-module (crates-io in te intervals-rs) #:use-module (crates-io))

(define-public crate-intervals-rs-0.0.1 (c (n "intervals-rs") (v "0.0.1") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.8.1") (d #t) (k 2)))) (h "1q6qlqwpw57bcdxkq55ji1kl17ljj7piq5wjv815r7xf7d5mifsf")))

(define-public crate-intervals-rs-0.0.2 (c (n "intervals-rs") (v "0.0.2") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.8.1") (d #t) (k 2)))) (h "00j8vmvvb3xmwx3j2x3lpg4qkfv3y5s0zjqmx3gdkxvwlmwqvr1p")))

(define-public crate-intervals-rs-0.0.3 (c (n "intervals-rs") (v "0.0.3") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.8.1") (d #t) (k 2)))) (h "1fq2qy984iwmmg03jfzgnn820rsqk12p43534r6x1n41bnpqvzda")))

(define-public crate-intervals-rs-0.0.4 (c (n "intervals-rs") (v "0.0.4") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.8.1") (d #t) (k 2)))) (h "1f82rjykywcc6xpi6yc7vqm88pkanjndcm4xlskjfhrwfp8g9lqj")))

(define-public crate-intervals-rs-0.0.5 (c (n "intervals-rs") (v "0.0.5") (d (list (d (n "once_cell") (r "^1.7.2") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.8.1") (d #t) (k 2)))) (h "0l5rqfnhbq5p9f8428b2xyx0g9pahgf6r4qchya10yb3kgci54yf")))

