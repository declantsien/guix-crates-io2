(define-module (crates-io in te integra8_formatters) #:use-module (crates-io))

(define-public crate-integra8_formatters-0.0.1-alpha (c (n "integra8_formatters") (v "0.0.1-alpha") (d (list (d (n "integra8_components") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "integra8_results") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 0)))) (h "1dmbcz79rhggg747smpjbrqi64wcc9kvskg33lvkwi78cwvd4gm7") (y #t)))

(define-public crate-integra8_formatters-0.0.2-alpha (c (n "integra8_formatters") (v "0.0.2-alpha") (d (list (d (n "integra8_components") (r "^0.0.2-alpha") (d #t) (k 0)) (d (n "integra8_results") (r "^0.0.2-alpha") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 0)))) (h "1sr78piy6qdimrb772pwrxyfcs8pixibr2059z5dzrbm580p8hl9")))

(define-public crate-integra8_formatters-0.0.3-alpha (c (n "integra8_formatters") (v "0.0.3-alpha") (d (list (d (n "integra8_components") (r "^0.0.3-alpha") (d #t) (k 0)) (d (n "integra8_results") (r "^0.0.3-alpha") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0wk03b47p3fhq2fph0kgbiyimnn43cip27hgnmdis6kdf6kzp29i") (f (quote (("enable_serde" "integra8_results/enable_serde"))))))

(define-public crate-integra8_formatters-0.0.4-alpha (c (n "integra8_formatters") (v "0.0.4-alpha") (d (list (d (n "integra8_components") (r "^0.0.4-alpha") (d #t) (k 0)) (d (n "integra8_results") (r "^0.0.4-alpha") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "186nnrhkalwvy9dgi4n0v7xm9cwhx3sc65dc2213jjq3gd5xl19h") (f (quote (("enable_serde" "integra8_results/enable_serde"))))))

