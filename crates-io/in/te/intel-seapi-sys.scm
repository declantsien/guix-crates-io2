(define-module (crates-io in te intel-seapi-sys) #:use-module (crates-io))

(define-public crate-intel-seapi-sys-0.1.0 (c (n "intel-seapi-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.43.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pbm256x4i6n13yk7c76yqmjd76w93816sw2la87kxh3y86alizb")))

(define-public crate-intel-seapi-sys-0.1.1 (c (n "intel-seapi-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.43.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0iv197gskbbhmb6m5vy3nzx26jnwfvjr05cgd7inymz0j2h0hycg")))

(define-public crate-intel-seapi-sys-0.2.0 (c (n "intel-seapi-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.43.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14v6xsxjh99iwhqqvhi3drzklq5jgqn0mc2k6gdbdggd1gpi02gi")))

