(define-module (crates-io in te integral-exponential-polynomial) #:use-module (crates-io))

(define-public crate-integral-exponential-polynomial-0.1.0 (c (n "integral-exponential-polynomial") (v "0.1.0") (d (list (d (n "num") (r "^0.1.37") (d #t) (k 2)) (d (n "num-bigint") (r "^0.1.37") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "1kl1ggd4fs5ij89i352sj1spkvaa04szszdy3p36qd0xg12lanyg")))

(define-public crate-integral-exponential-polynomial-0.1.1 (c (n "integral-exponential-polynomial") (v "0.1.1") (d (list (d (n "num") (r "^0.1.37") (f (quote ("bigint"))) (k 2)) (d (n "num-bigint") (r "^0.1.37") (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "1kvdpnj7zp2s5z7vipam2whv878vi6n59316xd0968xwf7gfg8ff")))

(define-public crate-integral-exponential-polynomial-0.1.2 (c (n "integral-exponential-polynomial") (v "0.1.2") (d (list (d (n "num-bigint") (r "^0.1.37") (k 0)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)))) (h "0h08f0dmqcvfd4xadq1saab7ggwbwpzw90724z09yhvpxk1955ck")))

(define-public crate-integral-exponential-polynomial-0.2.0 (c (n "integral-exponential-polynomial") (v "0.2.0") (d (list (d (n "num-bigint") (r "^0.2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "0ly89gr8r2jbq6s2jibnghg52hrnhiig0svg7kz8qniq6z5jz29s")))

