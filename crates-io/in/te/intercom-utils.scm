(define-module (crates-io in te intercom-utils) #:use-module (crates-io))

(define-public crate-intercom-utils-0.2.0 (c (n "intercom-utils") (v "0.2.0") (d (list (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "if_chain") (r "^0.1.2") (d #t) (k 0)) (d (n "intercom-common") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "0xhkxg8bzpb2sg6hw9vvwhhvcp4rcmypybnq2z8a97ys3y7rvrs8")))

