(define-module (crates-io in te interactive-parse) #:use-module (crates-io))

(define-public crate-interactive-parse-0.1.0 (c (n "interactive-parse") (v "0.1.0") (d (list (d (n "inquire") (r "^0.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0w3vp0qcn3g93v9l7jbxgsg6i8j4frq32hx05l5avz7sz7h9ykdz")))

(define-public crate-interactive-parse-0.1.1 (c (n "interactive-parse") (v "0.1.1") (d (list (d (n "inquire") (r "^0.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ifkqx7h7140i7ln5w659dpxr2z1423pdj00ak7hly5x7igqnaaf")))

(define-public crate-interactive-parse-0.1.2 (c (n "interactive-parse") (v "0.1.2") (d (list (d (n "inquire") (r "^0.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ls2y8wiigzyvm6dkz58v4s67k1ld7m7h80s6b1g770lsxfl7bvr")))

(define-public crate-interactive-parse-0.1.3 (c (n "interactive-parse") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "inquire") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "05xd61pps6fk0xbvhr1r5b1hw997ympyxmx5rxl4z4wfini2pp4b")))

(define-public crate-interactive-parse-0.1.4 (c (n "interactive-parse") (v "0.1.4") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "inquire") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "02ji6bxjr3zcsnrlkz1xhs3bk2ygzwwcwsga0iissf09wpc1h6pw")))

(define-public crate-interactive-parse-0.1.5 (c (n "interactive-parse") (v "0.1.5") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "inquire") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1s45l81zb1jnfhqkydn6sinh9mc3al8akwpvr98npzf9cr6niil2")))

