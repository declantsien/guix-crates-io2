(define-module (crates-io in te internal) #:use-module (crates-io))

(define-public crate-internal-0.1.2 (c (n "internal") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19vd5vh16yyzihd55327j68d2i4ydnnkslcld9kk59gdhpx0cl87") (f (quote (("internal"))))))

(define-public crate-internal-0.1.3 (c (n "internal") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fkalgamvkqbnin8aj73idf76v7si15y0sdfff402b6isnicv85c") (f (quote (("internal"))))))

(define-public crate-internal-0.1.4 (c (n "internal") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rp22yh9m4avn77i165hm18wdhz3sabqi50zjk0lj3a0pfg4vhgz") (f (quote (("internal"))))))

