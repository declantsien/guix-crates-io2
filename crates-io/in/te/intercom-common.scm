(define-module (crates-io in te intercom-common) #:use-module (crates-io))

(define-public crate-intercom-common-0.1.0 (c (n "intercom-common") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "11jsxsxny4daxca7lynbs1agmjrw584nf3nxr9sdgbxba1aa0jyv")))

(define-public crate-intercom-common-0.2.0 (c (n "intercom-common") (v "0.2.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "sha1") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0vx7lkv3rj73nljhjvj0y4bi8842s216m27c0j3hfi8q1cx93kwk")))

(define-public crate-intercom-common-0.3.0 (c (n "intercom-common") (v "0.3.0") (d (list (d (n "difference") (r "^1.0.0") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "handlebars") (r "^0.29") (d #t) (k 0)) (d (n "ordermap") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "0hjzxr50v5vm96i8phfl3lsn9fslim11rcaaq72xwnlja9qk33ly")))

(define-public crate-intercom-common-0.4.0 (c (n "intercom-common") (v "0.4.0") (d (list (d (n "difference") (r "^1.0.0") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "13q5phyya6l7qhg8xn6piqp481bcj6rcnczs1hjxhad8vrf6wq7r")))

