(define-module (crates-io in te intel8080) #:use-module (crates-io))

(define-public crate-intel8080-0.5.0 (c (n "intel8080") (v "0.5.0") (h "18ivy9m179l7p1f9swy8ryix2s0f3ahjzqz49c164biamym5y4lr")))

(define-public crate-intel8080-0.6.0 (c (n "intel8080") (v "0.6.0") (h "0gbdl8641hrvn3bkhw7w99yx50dvxjghr8ck599l7qzxkxzp0n10")))

(define-public crate-intel8080-0.7.0 (c (n "intel8080") (v "0.7.0") (h "09yqr9idx548ck16d27alj6g85vzs8n2jxj1cwkl0d1fa6fh23a1")))

(define-public crate-intel8080-0.8.0 (c (n "intel8080") (v "0.8.0") (h "0y4324qsxbnf8w3sbbgzl1sk3yb2q47rciz3f57w0dqps4skpnl7")))

(define-public crate-intel8080-0.9.0 (c (n "intel8080") (v "0.9.0") (h "0237d41vmxnc00psi8bicvgk2b2dpjgmmhkhkkgrvnd935m2rsz7")))

(define-public crate-intel8080-0.10.0 (c (n "intel8080") (v "0.10.0") (h "1spylq5h24m2w5c8ba52vgmivp0axy99zvgy8jha0qql26mk3fbc")))

(define-public crate-intel8080-0.11.0 (c (n "intel8080") (v "0.11.0") (h "0linrb9ygb7drjwff5459kcc3bqrk0lmbvdn478y9al614zxqb7k")))

(define-public crate-intel8080-0.12.0 (c (n "intel8080") (v "0.12.0") (d (list (d (n "spin_sleep") (r "^1.0.0") (d #t) (t "cfg(windows)") (k 0)))) (h "05zbjlmssmpqzn73zwby4c8rwaijzs4fbksxl1473s3489rvi08w")))

(define-public crate-intel8080-0.13.0 (c (n "intel8080") (v "0.13.0") (d (list (d (n "spin_sleep") (r "^1.0.0") (d #t) (t "cfg(windows)") (k 0)))) (h "06m5gd8v5ax2bci6j7j8fna43nknmf71507czlav792fb4fvvfkg")))

(define-public crate-intel8080-0.14.0 (c (n "intel8080") (v "0.14.0") (d (list (d (n "spin_sleep") (r "^1.0.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0zq1wapwnawg0k9b9sgh9ciznpqs9q63vpvpyrrxjlcqd7rpm5i5")))

(define-public crate-intel8080-0.15.0 (c (n "intel8080") (v "0.15.0") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.0.0") (d #t) (t "cfg(windows)") (k 0)))) (h "17z77dik30izi4k6lsv2zi1kq682cr6sb00p9f8pibz6gl79ddg0")))

(define-public crate-intel8080-0.16.0 (c (n "intel8080") (v "0.16.0") (d (list (d (n "spin_sleep") (r "^1.0.0") (d #t) (t "cfg(windows)") (k 0)))) (h "14zpj47lxfawmzxhrlyx2kpw3av28vhi1kq74vf1p7i4gg4ckhds")))

(define-public crate-intel8080-0.17.0 (c (n "intel8080") (v "0.17.0") (h "03xzqqdgrlbz1z63vjfgdr49hq9b1z87cj8isfc45pryy317j02b")))

(define-public crate-intel8080-0.18.0 (c (n "intel8080") (v "0.18.0") (h "1bnhv3w509vgrb3d90fd9116bkhhi48zfvz06j1wb937wva7mjvk")))

(define-public crate-intel8080-0.19.0 (c (n "intel8080") (v "0.19.0") (h "121425mr2plijbdbcbv6mcfl9ffwphbv86i79zbp0jxlazci52d3")))

