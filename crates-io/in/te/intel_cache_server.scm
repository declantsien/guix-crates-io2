(define-module (crates-io in te intel_cache_server) #:use-module (crates-io))

(define-public crate-intel_cache_server-1.0.0 (c (n "intel_cache_server") (v "1.0.0") (d (list (d (n "intel-cache-lib") (r "^1.0.0") (d #t) (k 0)))) (h "0ml70j93ydxi220hq549vz6qq9isiyzrdy17kfw97d64h4ca9mfb")))

(define-public crate-intel_cache_server-1.1.0 (c (n "intel_cache_server") (v "1.1.0") (d (list (d (n "intel-cache-lib") (r "^1.1.0") (d #t) (k 0)) (d (n "rpassword") (r "^0.0.4") (d #t) (k 0)))) (h "1c1zchq2q1bz8zcwlvb116ixmsp7mh84kfzwdc27qhg95v8nb531")))

(define-public crate-intel_cache_server-2.0.0 (c (n "intel_cache_server") (v "2.0.0") (d (list (d (n "intel-cache-lib") (r "^2.0.0") (d #t) (k 0)) (d (n "rpassword") (r "^0.0.4") (d #t) (k 0)))) (h "0zwf6kc94ysnw9nyppj507d91kk703qh2wl6wbsfsbm4n8qcr1d4")))

