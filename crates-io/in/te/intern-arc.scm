(define-module (crates-io in te intern-arc) #:use-module (crates-io))

(define-public crate-intern-arc-0.1.0 (c (n "intern-arc") (v "0.1.0") (d (list (d (n "dashmap") (r "^3.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.3") (d #t) (k 0)))) (h "1j67j8azc0926mf1w69vfpisww5m1sfaap5zmf8dbzz1avlw6vpj")))

(define-public crate-intern-arc-0.2.0 (c (n "intern-arc") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "dashmap") (r "^3.11") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "0a0w21ig77wi1v2z10j0ij8kbi4fx5w0di2fc8xqqin7j8bpwzif") (f (quote (("println"))))))

(define-public crate-intern-arc-0.2.1 (c (n "intern-arc") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "dashmap") (r "^3.11") (d #t) (k 0)) (d (n "loom") (r "^0.4.0") (d #t) (t "cfg(loom)") (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "0k5a70pck40kwyl29kilclij2b0cmwws07pfhqwcxymhjgsisrn1") (f (quote (("println"))))))

(define-public crate-intern-arc-0.3.0 (c (n "intern-arc") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "loom") (r "^0.4.0") (d #t) (t "cfg(loom)") (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "074x1m0lxajjbv8kcva2vg27c36945nmgnh5szbrn5qwcwcgrys5") (f (quote (("println"))))))

(define-public crate-intern-arc-0.4.0 (c (n "intern-arc") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "loom") (r "^0.4.0") (d #t) (t "cfg(loom)") (k 0)) (d (n "once_cell") (r "^1.7.2") (f (quote ("parking_lot"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "09al4chvnpm3fm70rdlh4lsgnrb2crhc71a9j393av8qa89vyc9v") (f (quote (("println"))))))

(define-public crate-intern-arc-0.4.1 (c (n "intern-arc") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "loom") (r "^0.4.0") (d #t) (t "cfg(loom)") (k 0)) (d (n "once_cell") (r "^1.7.2") (f (quote ("parking_lot"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "0lj2pvklii4f9jsxlydfqqism2j6y9lwy3hskk6mmqf8m0rqx5ii") (f (quote (("println"))))))

(define-public crate-intern-arc-0.5.0 (c (n "intern-arc") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "loom") (r "^0.4.0") (d #t) (t "cfg(loom)") (k 0)) (d (n "once_cell") (r "^1.7.2") (f (quote ("parking_lot"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "1g17vjkdkrqy4v9h17i5ysf2wi33775f0l9jyq4df4mfbxpnfldc") (f (quote (("println"))))))

(define-public crate-intern-arc-0.5.1 (c (n "intern-arc") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "loom") (r "^0.4.0") (d #t) (t "cfg(loom)") (k 0)) (d (n "once_cell") (r "^1.7.2") (f (quote ("parking_lot"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "1wgx0wi6pmvvw98j45cm4jjwzq95vg7nh3sxmxgfn6rxa6ggnkgk") (f (quote (("println"))))))

(define-public crate-intern-arc-0.6.0 (c (n "intern-arc") (v "0.6.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "loom") (r "^0.5.6") (d #t) (t "cfg(loom)") (k 0)) (d (n "once_cell") (r "^1.17.1") (f (quote ("parking_lot"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "0ywi3vwi6228g8ya0wk9l0jp21ivqycwcijpfdxdjj6hjfn826df") (f (quote (("println"))))))

(define-public crate-intern-arc-0.6.1 (c (n "intern-arc") (v "0.6.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "loom") (r "^0.7.0") (d #t) (t "cfg(loom)") (k 0)) (d (n "once_cell") (r "^1.17.1") (f (quote ("parking_lot"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "14x1s3k95n0jx50h78gv0ssiwp47m1yix3n5jpkkxappd6amr3ij") (f (quote (("println"))))))

