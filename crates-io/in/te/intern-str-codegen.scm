(define-module (crates-io in te intern-str-codegen) #:use-module (crates-io))

(define-public crate-intern-str-codegen-0.1.0 (c (n "intern-str-codegen") (v "0.1.0") (d (list (d (n "intern-str") (r "^0.1") (d #t) (k 0)))) (h "1ayy8cjw7kl43qrwqysgry5vxrc2gvygxvryzjar3qvgxdy6hljk")))

(define-public crate-intern-str-codegen-0.1.1 (c (n "intern-str-codegen") (v "0.1.1") (d (list (d (n "intern-str") (r "^0.1") (d #t) (k 0)))) (h "1hh7y5drzj3lb3wy3ggx8wsclynxqgg5y82cn767xv09hnk0a44w")))

(define-public crate-intern-str-codegen-0.1.2 (c (n "intern-str-codegen") (v "0.1.2") (d (list (d (n "intern-str") (r "^0.1") (d #t) (k 0)))) (h "1akpprv5k72gzqvraw37nsxc5yxjla3kjm1i22nif9y1408j8xhs")))

