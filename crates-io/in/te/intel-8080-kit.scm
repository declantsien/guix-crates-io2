(define-module (crates-io in te intel-8080-kit) #:use-module (crates-io))

(define-public crate-intel-8080-kit-0.1.0 (c (n "intel-8080-kit") (v "0.1.0") (h "0ffd5mjfcgng0di68wl869di3jx2y8j0zixagfavrrmiqmxjmp6y")))

(define-public crate-intel-8080-kit-0.1.1 (c (n "intel-8080-kit") (v "0.1.1") (h "1azgfaml9dlk97xirs5ffpfcwfgjk0xp6xxh2ani9d8ddawl46ww")))

