(define-module (crates-io in te integral_square_root) #:use-module (crates-io))

(define-public crate-integral_square_root-0.0.0 (c (n "integral_square_root") (v "0.0.0") (h "1rj8jry3wj288mlj687fpv1mydnfkgfrvnrcwdv7w4xa7c3wdqxk")))

(define-public crate-integral_square_root-0.0.1 (c (n "integral_square_root") (v "0.0.1") (h "14i08c7vpn9fnyf5xvi0hhdd5ahcjb06gspybz3kkxlxzkldsnyr")))

(define-public crate-integral_square_root-1.0.0 (c (n "integral_square_root") (v "1.0.0") (h "163mcv7fhizw7p4w1kfszrsf0v00n1g6y1fd9qlzr4q97kqabw7b")))

(define-public crate-integral_square_root-1.0.1 (c (n "integral_square_root") (v "1.0.1") (h "1nqkjkp7y9x0qb361c5h4hyyjpbr6skbs10ssrvca8jw7zjgxm4b")))

