(define-module (crates-io in te internship) #:use-module (crates-io))

(define-public crate-internship-0.1.0 (c (n "internship") (v "0.1.0") (h "0s2102fpccggv2dxp99w5978gqlyrkdmmldnmp0v0b89d201sr27")))

(define-public crate-internship-0.1.1 (c (n "internship") (v "0.1.1") (h "07vvqayx8rys2dzi3kmrvjph2ahl5plnh3vb5sipf7bqla0a1d2z") (f (quote (("shared_from_slice2"))))))

(define-public crate-internship-0.2.0 (c (n "internship") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0hkv3k0yhsgacaggjgk3rlx7x3dh3vyp31a61sdvjj67nk7gs8d7") (f (quote (("shared_from_slice2") ("serde-compat" "serde") ("default" "serde-compat"))))))

(define-public crate-internship-0.3.0 (c (n "internship") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "00b2129m0y2slv38xqzsm6fc02hf163n0mrapgnd44zzpalszihw") (f (quote (("shared_from_slice2") ("serde-compat" "serde") ("default" "serde-compat"))))))

(define-public crate-internship-0.3.1 (c (n "internship") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0vafb181i77ghx5gbyqjvcid0zc5lxgmy2jmaxhdllwwzcwdsix8") (f (quote (("shared_from_slice2") ("serde-compat" "serde") ("default" "serde-compat"))))))

(define-public crate-internship-0.4.0 (c (n "internship") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1hap353kbcj4ac7zf2mg8wd3mcbawz9mxscbwnx2dwxzff0plx8m") (f (quote (("serde-compat" "serde") ("default" "serde-compat"))))))

(define-public crate-internship-0.5.0 (c (n "internship") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1s9zdw91ccc2n2gwqwd2scm9jvmgwg53k1rkkx70s4x5dn2nng7q") (f (quote (("serde-compat" "serde") ("default" "serde-compat"))))))

(define-public crate-internship-0.5.1 (c (n "internship") (v "0.5.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1bn63bsf9fwf276j29j6bhjh87pqbd64ykxkws0228z8j06dhwb0") (f (quote (("serde-compat" "serde") ("default" "serde-compat"))))))

(define-public crate-internship-0.5.2 (c (n "internship") (v "0.5.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0srsmxnnhyn85zi6g8cjra4yc7i1mszd4l4wmlqssmb3iybki1ml") (f (quote (("serde-compat" "serde") ("default" "serde-compat"))))))

(define-public crate-internship-0.5.3 (c (n "internship") (v "0.5.3") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1nrcajazmbpy76vm15ya6hz59w9rwskmm5bh3gmghllcdrkmslk0") (f (quote (("serde-compat" "serde") ("default" "serde-compat"))))))

(define-public crate-internship-0.6.0 (c (n "internship") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "15xm54vxdxnkfnb9s5cw3w3d88c7bkbnw21754mnj94idp080nxp") (f (quote (("serde-compat" "serde") ("default" "serde-compat"))))))

