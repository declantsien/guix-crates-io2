(define-module (crates-io in te internet-packet) #:use-module (crates-io))

(define-public crate-internet-packet-0.1.0 (c (n "internet-packet") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.3.3") (d #t) (k 2)) (d (n "internet-checksum") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0qpczs2w27c1hnf5l9m9qwginma532xlxx1bfdcamfhhf068i9a9") (f (quote (("checksums" "internet-checksum")))) (r "1.65.0")))

(define-public crate-internet-packet-0.2.0 (c (n "internet-packet") (v "0.2.0") (d (list (d (n "data-encoding") (r "^2.5.0") (d #t) (k 2)) (d (n "internet-checksum") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "smoltcp") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "1n2skb799xpvj7aaqj05y32s1ssaw5laa7spvwgfg4hssq5d5n4m") (f (quote (("checksums" "internet-checksum")))) (r "1.65.0")))

(define-public crate-internet-packet-0.2.1 (c (n "internet-packet") (v "0.2.1") (d (list (d (n "data-encoding") (r "^2.6.0") (d #t) (k 2)) (d (n "internet-checksum") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "smoltcp") (r ">=0.10, <0.12") (o #t) (d #t) (k 0)))) (h "0lz4rxyka8k7n1p8zw1h7q6palnifagzbdsx3rsqwxb8p04k49zh") (f (quote (("checksums" "internet-checksum")))) (r "1.65.0")))

