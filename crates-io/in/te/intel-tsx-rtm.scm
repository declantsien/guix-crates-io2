(define-module (crates-io in te intel-tsx-rtm) #:use-module (crates-io))

(define-public crate-intel-tsx-rtm-0.0.0 (c (n "intel-tsx-rtm") (v "0.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "raw-cpuid") (r "^3.0") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)) (d (n "rust_c") (r "^0.1.1") (f (quote ("macro"))) (d #t) (k 0)) (d (n "rust_c") (r "^0.1.1") (f (quote ("build"))) (d #t) (k 1)))) (h "1614mabh085g57qqa5gaci26bwl677h25snid1la2ynl3q3h1miw")))

(define-public crate-intel-tsx-rtm-0.0.1 (c (n "intel-tsx-rtm") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "raw-cpuid") (r "^3.0") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)) (d (n "rust_c") (r "^0.1.1") (f (quote ("macro"))) (d #t) (k 0)) (d (n "rust_c") (r "^0.1.1") (f (quote ("build"))) (d #t) (k 1)))) (h "0w6aikalmz8pri91jxgk87g33ym9jikh4cf605k1cc3vqcjf9rly")))

(define-public crate-intel-tsx-rtm-0.1.0 (c (n "intel-tsx-rtm") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "raw-cpuid") (r "^3.0") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 0)))) (h "17qj2fqly61jwm18vwfriy47qgyf11lbzbmjkhrgq98qbgl9zw6r")))

