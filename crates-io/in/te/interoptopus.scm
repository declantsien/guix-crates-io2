(define-module (crates-io in te interoptopus) #:use-module (crates-io))

(define-public crate-interoptopus-0.1.0 (c (n "interoptopus") (v "0.1.0") (d (list (d (n "interoptopus_proc") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0lwrcpag9axxh19j1wy4wavk69x3761j5zinjhrxislj091iizjl") (f (quote (("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.1.1 (c (n "interoptopus") (v "0.1.1") (d (list (d (n "interoptopus_proc") (r "^0.1") (o #t) (d #t) (k 0)))) (h "08fl3rvnv4sjra1ac3n42wdrcdpa7nlqvhvljv1681zs11z509ar") (f (quote (("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.1.2 (c (n "interoptopus") (v "0.1.2") (d (list (d (n "interoptopus_backend_csharp") (r "^0.1") (d #t) (k 2)) (d (n "interoptopus_proc") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1g80nd8z6dfqyizh92j26d5pbzy0jzm1gwiyb9g5wpy8l807al98") (f (quote (("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.1.3 (c (n "interoptopus") (v "0.1.3") (d (list (d (n "interoptopus_backend_csharp") (r "^0.1") (d #t) (k 2)) (d (n "interoptopus_proc") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1iy0095npc49s24q35fbgw6sp2q3d5w2b6xln9xlc9wjjqynx0b3") (f (quote (("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.1.5 (c (n "interoptopus") (v "0.1.5") (d (list (d (n "interoptopus_backend_csharp") (r "^0.1") (d #t) (k 2)) (d (n "interoptopus_proc") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1j71ya3783wkhry93j54ylnxcpqh5qqhw5lr15zg2b9lsvylb7mv") (f (quote (("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.1.6 (c (n "interoptopus") (v "0.1.6") (d (list (d (n "cc") (r "^1.0.68") (o #t) (d #t) (k 0)) (d (n "interoptopus_backend_csharp") (r "^0.1") (d #t) (k 2)) (d (n "interoptopus_proc") (r "^0.1") (o #t) (d #t) (k 0)))) (h "16g3b6wa67nnn39z5fyg2y7sjlpj2kbkdjw7fn3zk7snwssalv8f") (f (quote (("testing" "cc") ("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.1.7 (c (n "interoptopus") (v "0.1.7") (d (list (d (n "cc") (r "^1.0.68") (o #t) (d #t) (k 0)) (d (n "interoptopus_backend_csharp") (r "^0.1") (d #t) (k 2)) (d (n "interoptopus_proc") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1cw8hidlg6w9z6kknm0wfqb1kd7vn5976qgkcwffm4y71j1k393a") (f (quote (("testing" "cc") ("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.1.8 (c (n "interoptopus") (v "0.1.8") (d (list (d (n "cc") (r "^1.0.68") (o #t) (d #t) (k 0)) (d (n "interoptopus_backend_csharp") (r "^0.1") (d #t) (k 2)) (d (n "interoptopus_proc") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1i0m2fj024v12c40fclnm80jhvnqxi1jdikcn6p5ryxdvj4mcsp7") (f (quote (("testing" "cc") ("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.1.12 (c (n "interoptopus") (v "0.1.12") (d (list (d (n "cc") (r "^1.0.68") (o #t) (d #t) (k 0)) (d (n "interoptopus_backend_csharp") (r "^0.1") (d #t) (k 2)) (d (n "interoptopus_proc") (r "^0.1") (o #t) (d #t) (k 0)))) (h "13ac3ax3vnwgkhq6jfkbl4bhkyscvvy5kxy4lwwqn9mij3f36mfh") (f (quote (("testing" "cc") ("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.1.13 (c (n "interoptopus") (v "0.1.13") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "interoptopus_backend_csharp") (r "^0.1") (d #t) (k 2)) (d (n "interoptopus_proc") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "07a387fwffcg0xkpignk9pf8w6zg2i3l1p42ga8qaiwjq8jzw272") (f (quote (("testing" "cc") ("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.1.14 (c (n "interoptopus") (v "0.1.14") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "interoptopus_backend_csharp") (r "^0.1") (d #t) (k 2)) (d (n "interoptopus_proc") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1abmnpbnmza8v03xdhlb56ysdpm3qhs47ilrla489b6blbj4zh8i") (f (quote (("testing" "cc") ("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.1.15 (c (n "interoptopus") (v "0.1.15") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "interoptopus_backend_csharp") (r "^0.1") (d #t) (k 2)) (d (n "interoptopus_proc") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1q1zsjjhd13hzi7gdgijc2l8qijsaqgs7kwyvipbnabfcwcfwx2w") (f (quote (("testing" "cc") ("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.1.16 (c (n "interoptopus") (v "0.1.16") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "interoptopus_backend_csharp") (r "^0.1") (d #t) (k 2)) (d (n "interoptopus_proc") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1gknfclpy0wbw264rrgddi93nnzpg46z2fc9jb2lwzz4l4ag99vx") (f (quote (("testing" "cc") ("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.1.17 (c (n "interoptopus") (v "0.1.17") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "interoptopus_backend_csharp") (r "^0.1") (d #t) (k 2)) (d (n "interoptopus_proc") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1mzc0dgyypshv76alx4azyssa6466bgdsgdf47kvi8sgfch95x1i") (f (quote (("testing" "cc") ("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.2.1 (c (n "interoptopus") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "interoptopus_proc") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "158jsnwp9lx1ipsapzf02x2j8nax6y08icsaqfhwbg2sczs2cg8g") (f (quote (("testing" "cc") ("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.2.2 (c (n "interoptopus") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "interoptopus_proc") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0d7ha5zphaws1pidirap2lr7bssxy7vjgnf9jlfrzil3d7jp1kbh") (f (quote (("testing" "cc") ("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.2.3 (c (n "interoptopus") (v "0.2.3") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "interoptopus_proc") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1q910va45iiqmdq8xf5khz3b90a3ymbng5qqrmy57rf3djqfx7x4") (f (quote (("testing" "cc") ("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.2.5 (c (n "interoptopus") (v "0.2.5") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "interoptopus_proc") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1lgvw21rk1hzcjlqw2lr6b8j3v63mcsj0125akaavn1prh4439cb") (f (quote (("testing" "cc") ("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.3.0 (c (n "interoptopus") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "interoptopus_proc") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "11l21xh5hxivnich5649wii8bm80fnhjyy67p1jgqxkfjr0xcy7s") (f (quote (("testing" "cc") ("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.3.2 (c (n "interoptopus") (v "0.3.2") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "interoptopus_proc") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1nd6fh1skvdhy4viyzp9q266prypgw7d4rsdfc1s2g82nwa9nwc8") (f (quote (("testing" "cc") ("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.4.0 (c (n "interoptopus") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "interoptopus_proc") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1i5s0kbs1fb5636khy88a9jyxj4cpmbcsk6bpjy0sb725vhlw91w") (f (quote (("testing" "cc") ("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.4.1 (c (n "interoptopus") (v "0.4.1") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "interoptopus_proc") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1hv1b9n6jd0ik1ssjl8mi7jy9wpx5ar5pimcr018pavpi9hz11jk") (f (quote (("testing" "cc") ("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.4.2 (c (n "interoptopus") (v "0.4.2") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "interoptopus_proc") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1lw3jy89qdiqbk6f3y88s7qiyd8dhgfqldpqqlphp7bw779ipykq") (f (quote (("testing" "cc") ("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.5.0 (c (n "interoptopus") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "interoptopus_proc") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1l31wa8zf8j9dzl33m03f53nxxrb9aa2rm0c7vlyvn4pjmlnsmh3") (f (quote (("testing" "cc") ("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.5.1 (c (n "interoptopus") (v "0.5.1") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "interoptopus_proc") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "09l6lm15754yd2c47s8jjs3p9slmlh1z653dj3741g63dssb4s4b") (f (quote (("testing" "cc") ("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.5.2 (c (n "interoptopus") (v "0.5.2") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "interoptopus_proc") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1cbb5vb8qn52xcrp5s605b9scs7192k0l11jdgr2c4yy3h3gz2f0") (f (quote (("testing" "cc") ("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.6.0 (c (n "interoptopus") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "interoptopus_proc") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0b973h1zamiby0xydvl0p6l0hsh3yfxq0c9m08s4149fnywzk9z9") (f (quote (("testing" "cc") ("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.7.0 (c (n "interoptopus") (v "0.7.0") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "interoptopus_proc") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1vlnkfbxl7vnq6a04mk6a8yj30ykxlpk6i71b0v9mh1fayddmc7y") (f (quote (("testing" "cc") ("derive" "interoptopus_proc"))))))

(define-public crate-interoptopus-0.7.2 (c (n "interoptopus") (v "0.7.2") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "interoptopus_proc") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0dcrhd0agwjwyn1ksdlz15x7r9sv69ilg7nd4k0bjwfp9z7ks6s3") (f (quote (("testing" "cc") ("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.7.4 (c (n "interoptopus") (v "0.7.4") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "interoptopus_proc") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1cgc8k4wg2sx2dqs2cnqwl4qdzip0qshw978zdxvqbksd2z5hgl6") (f (quote (("testing" "cc") ("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.7.5 (c (n "interoptopus") (v "0.7.5") (d (list (d (n "cc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "interoptopus_proc") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "178v2r313337d46hwyhc3zj3yhi1lck022pyspj6843i0mg5kasv") (f (quote (("testing" "cc") ("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.8.0 (c (n "interoptopus") (v "0.8.0") (d (list (d (n "interoptopus_proc") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0mifp3hp27dmk1r9cdq28d7kdn18whfpsklx5qklwmgfidkbsazj") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.8.1 (c (n "interoptopus") (v "0.8.1") (d (list (d (n "interoptopus_proc") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0pamhky951hqv9a06rl5nffkap22plxri730sj7wr8a5l6rh7s20") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.8.2 (c (n "interoptopus") (v "0.8.2") (d (list (d (n "interoptopus_proc") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1fmz17183yflg4flffn39fafmh1l5775z32jldr2n0767wxnf6q5") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.9.0 (c (n "interoptopus") (v "0.9.0") (d (list (d (n "interoptopus_proc") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0axcy6jlr9yvyxk7zdl1y0vzkfhm8plf7xrw6am9hlabdbs6szsq") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.9.3 (c (n "interoptopus") (v "0.9.3") (d (list (d (n "interoptopus_proc") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "10jsg440xb0z9sxh99rdms62cidjlw3vcjdadvl2l7rrxfz97vpv") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.9.4 (c (n "interoptopus") (v "0.9.4") (d (list (d (n "interoptopus_proc") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kpcfx48hmji7mzi6lvfv6k8r23mccg6zzhg91ds89z00h8dd5na") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.10.0 (c (n "interoptopus") (v "0.10.0") (d (list (d (n "interoptopus_proc") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0jpvjy43i6hwlb3l922pig3a6jkb8lra600yd57y099ypplik7jz") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.10.3 (c (n "interoptopus") (v "0.10.3") (d (list (d (n "interoptopus_proc") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "176f1kqgl04ikg9xc010v7cl6nwn63l9b3vs65pibgq67xk5pydz") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.11.0 (c (n "interoptopus") (v "0.11.0") (d (list (d (n "interoptopus_proc") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1n07zyhsp9ncfzzha4xnr0dlgpm616szx6zpbijlg3qm1ixmasc0") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.11.3 (c (n "interoptopus") (v "0.11.3") (d (list (d (n "interoptopus_proc") (r "^0.11.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0rzcdx77in4gfzgbnckgrakbk6zq840a09y5crv3zxx945vs2dhf") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.12.0 (c (n "interoptopus") (v "0.12.0") (d (list (d (n "interoptopus_proc") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ipc212l10r8lka9l8slmifa0zsfkiiqh3f20afckwnlp9qp99ak") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.12.2 (c (n "interoptopus") (v "0.12.2") (d (list (d (n "interoptopus_proc") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0jj9ys2n0yzs6y8mpyb3lc9p0kx63432rwrp35ai4mhzd7gs7gbm") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.13.0 (c (n "interoptopus") (v "0.13.0") (d (list (d (n "interoptopus_proc") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1578nxhpnv1kwm2rqy2sr2xlk1qpmm6qybn4vdjyqnqiiaav0yf7") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.13.1 (c (n "interoptopus") (v "0.13.1") (d (list (d (n "interoptopus_proc") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1rb6bmds0jfs0dwm4mp1jmrpmqrp9ry8i74yxv9zmqy4k99rj8wy") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.13.2 (c (n "interoptopus") (v "0.13.2") (d (list (d (n "interoptopus_proc") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1x2j754hp28bg869zq495wl0b6nss2j0rbfgspgxnmln58vgg3ww") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.13.3 (c (n "interoptopus") (v "0.13.3") (d (list (d (n "interoptopus_proc") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jsp3ccl5qhilkvb36a88y3w61k68sxfapb2hja39a9il5sfhlf3") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.13.5 (c (n "interoptopus") (v "0.13.5") (d (list (d (n "interoptopus_proc") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1gqv18nbhx7d962aj452389vd34wfm40lz60bd0pzv8hhxf1wfx5") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.13.7 (c (n "interoptopus") (v "0.13.7") (d (list (d (n "interoptopus_proc") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "00dxa4r29bbn3jpdrv9fkcw31k1qmhyynkzl9p344pr6knnwpzyg") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.13.8 (c (n "interoptopus") (v "0.13.8") (d (list (d (n "interoptopus_proc") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "06nbs37di51415l8fyszymvakjsamkfnfi7czckwg7lhxiq66fh4") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.13.12 (c (n "interoptopus") (v "0.13.12") (d (list (d (n "interoptopus_proc") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0si1sfd0pyqy4r2chgn2lpr5w6502aq7bybv9y3cgwyvi3dp25da") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.13.15 (c (n "interoptopus") (v "0.13.15") (d (list (d (n "interoptopus_proc") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1q367nsbpzas5w3d70snvmxscxjjhp3k4cinplnyfy8f3d1a6cbd") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.14.0 (c (n "interoptopus") (v "0.14.0") (d (list (d (n "interoptopus_proc") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qhv17g2nrk6j4qgqw40aydrvxv3fim0n7v62gxnnah702nszj4i") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.14.1 (c (n "interoptopus") (v "0.14.1") (d (list (d (n "interoptopus_proc") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "19yh0vb1ph668xcm03vrfvz7sbfhvq8p5jvkqv27wf0whc2v7xgf") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.14.2 (c (n "interoptopus") (v "0.14.2") (d (list (d (n "interoptopus_proc") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0b77ci24jfk3byvz2mlmpr0yk86w1q6g9phf87501hw4p3cvpvs2") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.14.3 (c (n "interoptopus") (v "0.14.3") (d (list (d (n "interoptopus_proc") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "19myyih4bh2333bb93m4a02qqyaxh5na05n9plxkmshhwwsnsld3") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.14.4 (c (n "interoptopus") (v "0.14.4") (d (list (d (n "interoptopus_proc") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1816a207vqn5ld20p3qda672s1hlsp8sbw96kxwl7hih5rnfrrfh") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.14.5 (c (n "interoptopus") (v "0.14.5") (d (list (d (n "interoptopus_proc") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0fzkasab9yals0i4jaj2jmwx5pjqn0b49blnbly3y2pwjyzhj0my") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.14.6 (c (n "interoptopus") (v "0.14.6") (d (list (d (n "interoptopus_proc") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1vgib9dv5hdxbizd1zxjwjky3hmh9il56nbv1d005f898q3d8vgy") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.14.7 (c (n "interoptopus") (v "0.14.7") (d (list (d (n "interoptopus_proc") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0qkiy6gbz9b591mzdalhl4zz60xvxn9w80183rnw4pz8ibx2ncb0") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.14.8 (c (n "interoptopus") (v "0.14.8") (d (list (d (n "interoptopus_proc") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1n8ms7nc1z9p997vh0zginpa68n3gf5f2m35yg1nq2b7w1mh6h38") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.14.9 (c (n "interoptopus") (v "0.14.9") (d (list (d (n "interoptopus_proc") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0j8mgywjpz0r82kgk8sap872mrhs7zfmk4draykvs3k62kv165k7") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.14.10 (c (n "interoptopus") (v "0.14.10") (d (list (d (n "interoptopus_proc") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1g54m1kqvpwqkyv73gyg6cjb17d9p19krmjmlah6r6dwy802hqfd") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.14.13 (c (n "interoptopus") (v "0.14.13") (d (list (d (n "interoptopus_proc") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "11m3qg4pk8xznv6i8sa6cwp887jhp690nwc67kl67m9n6yj90cqk") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.14.14 (c (n "interoptopus") (v "0.14.14") (d (list (d (n "interoptopus_proc") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1yb7dh7fdfdld61hiijcw8zvcziz3yf4iyw18mrr5sgcprhynjmh") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.14.15 (c (n "interoptopus") (v "0.14.15") (d (list (d (n "interoptopus_proc") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "05f15bjqbz75lmj2qrc5ya270a5sy7bagcyg0l5zdzl3h5bv1vn0") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.14.18 (c (n "interoptopus") (v "0.14.18") (d (list (d (n "interoptopus_proc") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "03w91irr23bh2pn6ws2k70nali814pqdrp7j0h38am3njwhkakp2") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.14.19 (c (n "interoptopus") (v "0.14.19") (d (list (d (n "interoptopus_proc") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)))) (h "15446xsxk8ap795zr7jcmyhqxi9lblcbwq2z7m1nvqlsx7vbn20k") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.14.21 (c (n "interoptopus") (v "0.14.21") (d (list (d (n "interoptopus_proc") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)))) (h "0dryl32vwyn7kccg1y17mc0p772f3s78kw7vyrjscj0r2s7k4dp4") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

(define-public crate-interoptopus-0.14.23 (c (n "interoptopus") (v "0.14.23") (d (list (d (n "interoptopus_proc") (r "^0.14.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (o #t) (d #t) (k 0)))) (h "06igbvijzkb6kc3icpzyvcadmjx0iqdy30jf05is1p3h43sk7fdk") (f (quote (("derive" "interoptopus_proc") ("default" "derive"))))))

