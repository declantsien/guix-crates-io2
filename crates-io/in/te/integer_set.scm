(define-module (crates-io in te integer_set) #:use-module (crates-io))

(define-public crate-integer_set-0.0.1 (c (n "integer_set") (v "0.0.1") (d (list (d (n "interval") (r "^0.0.1") (d #t) (k 0)))) (h "18yysn4sdadbdjdkrha6ibhnknhip8vh7smblkgcinjcmyp4dd30")))

(define-public crate-integer_set-0.0.2 (c (n "integer_set") (v "0.0.2") (d (list (d (n "interval") (r "^0.0.1") (d #t) (k 0)))) (h "1429ninl2lr3jw0q09k5p6148kwlk2wjpsasnv1c0y3jwgn0pcj7")))

