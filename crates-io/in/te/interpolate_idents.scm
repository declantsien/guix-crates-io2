(define-module (crates-io in te interpolate_idents) #:use-module (crates-io))

(define-public crate-interpolate_idents-0.0.1 (c (n "interpolate_idents") (v "0.0.1") (h "1r1s772q73zhfjzq61qfjs6cnz58napdr0fqhlnhbqb4m1xs22gj")))

(define-public crate-interpolate_idents-0.0.2 (c (n "interpolate_idents") (v "0.0.2") (h "1g85x5l7zilq4bpp5rhxwfp1ri2lxxx87lcskin3bki0kp0472lm")))

(define-public crate-interpolate_idents-0.0.3 (c (n "interpolate_idents") (v "0.0.3") (h "0gczj09xhgh79b3xw56s8r2vxiv7xj8a6acz3i4z0mywg8xpiia3")))

(define-public crate-interpolate_idents-0.0.4 (c (n "interpolate_idents") (v "0.0.4") (h "1xv4drvl4j1a34js68hb5a3n0a33iyn4cyzqmjkvx79p2xwwjaf4")))

(define-public crate-interpolate_idents-0.0.5 (c (n "interpolate_idents") (v "0.0.5") (h "1dq4fl824rv8bs11mapfh5bdh3zdrjpwl9yyi7j0a1chv623i7gq")))

(define-public crate-interpolate_idents-0.0.6 (c (n "interpolate_idents") (v "0.0.6") (h "0hrkkfbcciqqi56f4ya6kc9wv0bybbdh0qxqbpz0302dvsdmw5bl")))

(define-public crate-interpolate_idents-0.0.7 (c (n "interpolate_idents") (v "0.0.7") (h "1wwc3b08v422wnacf0dksnqi6m8mn9cd97c3f3mfv8fp4wnk44r6")))

(define-public crate-interpolate_idents-0.0.8 (c (n "interpolate_idents") (v "0.0.8") (h "1yx940y1k5r40f4k1cwxczip06ghxa4qpvvk0h2k84ysf28l4zzg")))

(define-public crate-interpolate_idents-0.0.9 (c (n "interpolate_idents") (v "0.0.9") (h "1z05bkyvqlpsz7klhhz7ii3qv1xyikfrhc4d2la0anh7qwb04yqr")))

(define-public crate-interpolate_idents-0.0.10 (c (n "interpolate_idents") (v "0.0.10") (h "19ndxfadaimvc3nfdkr1x2siy7h517siac4bmlc5bp36aizd0vjk")))

(define-public crate-interpolate_idents-0.1.0 (c (n "interpolate_idents") (v "0.1.0") (h "0jwpyvzv8dbly13vckn8mfqcpxdrd0qk5lm4ijw4sbyn9h1mhk1l")))

(define-public crate-interpolate_idents-0.1.1 (c (n "interpolate_idents") (v "0.1.1") (h "08xjdxgyl7dvnsb6bna6c3wpm9x94vg4v1hw2iaik6y5d7svb3fa")))

(define-public crate-interpolate_idents-0.1.2 (c (n "interpolate_idents") (v "0.1.2") (h "0jv9nmwk71wlkfxsaipslz77cdkwaa09z3i9ypkxz6fa3015yg0m")))

(define-public crate-interpolate_idents-0.1.3 (c (n "interpolate_idents") (v "0.1.3") (h "1rp2a82zx9db5npcpj6lqcgvjfnqylh7xsr43kspb5fl661308zy")))

(define-public crate-interpolate_idents-0.1.4 (c (n "interpolate_idents") (v "0.1.4") (h "1vf7wrkyw5d5vliyk982a9s7qxdqj8f5h12n0kxnlyvy18558ppn")))

(define-public crate-interpolate_idents-0.1.5 (c (n "interpolate_idents") (v "0.1.5") (h "1pn24wccsidjdpvs6fa6fvlv9qscn86jydhccy4yh4jlqylmfdwg")))

(define-public crate-interpolate_idents-0.1.6 (c (n "interpolate_idents") (v "0.1.6") (h "0l4plhz98zfzk4iws4wzq1c5k27sriirpd8m9m4pr7qkp0cik0xn")))

(define-public crate-interpolate_idents-0.1.7 (c (n "interpolate_idents") (v "0.1.7") (h "0mv0n7i5vlhqhwmyzj79g24nmyxvi4hzdjhnw2ijn4h0lw4vfpdj")))

(define-public crate-interpolate_idents-0.1.8 (c (n "interpolate_idents") (v "0.1.8") (h "1ibb66ysvv39pxlkkb8h4m2ybd018rz02i76yhzp0kirwxpy6jsa")))

(define-public crate-interpolate_idents-0.2.0 (c (n "interpolate_idents") (v "0.2.0") (h "1qyl1lz0mpagvz3ky130s4mrz7vgbps98pjdqvbzqhcgj3x8h02d")))

(define-public crate-interpolate_idents-0.2.1 (c (n "interpolate_idents") (v "0.2.1") (h "18rwp7ls49b26i14849kz9gpkkhb1lqbw5r4qhdgh6kwm4b8zm2h")))

(define-public crate-interpolate_idents-0.2.2 (c (n "interpolate_idents") (v "0.2.2") (h "114xhmny23sbq7qsjf9xifib1m19m038dxxirq94dvzv18m3q552")))

(define-public crate-interpolate_idents-0.2.3 (c (n "interpolate_idents") (v "0.2.3") (h "18zhq55d2iiq6ngdkih5nyp3p9rn060vsmqpvbr2wphr0xy5ryi2")))

(define-public crate-interpolate_idents-0.2.4 (c (n "interpolate_idents") (v "0.2.4") (h "022f5aj7c7y6k2in8sj2zi6qrxbw235y9cviiyvm2qamn4k8a0wc")))

(define-public crate-interpolate_idents-0.2.5 (c (n "interpolate_idents") (v "0.2.5") (h "0kdz1khc9hv1k11awkmvxkqgq7ncn9x6gvagbcj9c95klj0nymwr")))

(define-public crate-interpolate_idents-0.2.6 (c (n "interpolate_idents") (v "0.2.6") (h "1k5d3za496acizlmj4i5c60mwl2xcp1l2dpwxvaxl5dwsn4qskv8")))

(define-public crate-interpolate_idents-0.2.7 (c (n "interpolate_idents") (v "0.2.7") (h "1yafcdpy4ai10knk9jkilw16mcka7lylfg23fb0k8gni91hdsmni")))

(define-public crate-interpolate_idents-0.2.8 (c (n "interpolate_idents") (v "0.2.8") (h "0mc2qvxz631884c5rizb1mnd9jwxjp83812525y3pdq1kqpah7q0")))

(define-public crate-interpolate_idents-0.2.9 (c (n "interpolate_idents") (v "0.2.9") (h "06n5dqvid8mf13grskdv45wwal38mnihxdw8f60qmmjvblbw0ajs")))

