(define-module (crates-io in te integra8_results) #:use-module (crates-io))

(define-public crate-integra8_results-0.0.1-alpha (c (n "integra8_results") (v "0.0.1-alpha") (d (list (d (n "integra8_components") (r "^0.0.1-alpha") (d #t) (k 0)))) (h "00kz1w8m54rfrhfi7k7wifip13vy6z8l9v321f76qfmmbfsaid94") (y #t)))

(define-public crate-integra8_results-0.0.2-alpha (c (n "integra8_results") (v "0.0.2-alpha") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "integra8_components") (r "^0.0.2-alpha") (d #t) (k 0)))) (h "06hvqvd8a1sj0dqpxj41h2hljq7ma197s6a89pfyy84yswxafym7")))

(define-public crate-integra8_results-0.0.3-alpha (c (n "integra8_results") (v "0.0.3-alpha") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "integra8_components") (r "^0.0.3-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (o #t) (d #t) (k 0)))) (h "0wma31bvzdrfpgha2j5z12gjhkh03wm5vsj4hh9iw23gmkz6n0vs") (f (quote (("enable_serde" "serde" "serde_bytes"))))))

(define-public crate-integra8_results-0.0.4-alpha (c (n "integra8_results") (v "0.0.4-alpha") (d (list (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "integra8_components") (r "^0.0.4-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (o #t) (d #t) (k 0)))) (h "0sz7f3pnxfd7y88ijyc239k7wk2qrb9p3dgd3l3f0vfjj7r317pf") (f (quote (("enable_serde" "serde" "serde_bytes"))))))

