(define-module (crates-io in te interning) #:use-module (crates-io))

(define-public crate-interning-0.1.0 (c (n "interning") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "stable-hash") (r "^0.4.3") (d #t) (k 0)))) (h "08m2klqinhza785a4j46gzh8d05ci70lf3b88jlcgdpcx9yh2swj")))

(define-public crate-interning-0.1.1 (c (n "interning") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "stable-hash") (r "^0.4.3") (d #t) (k 0)))) (h "0vrgzmgaqrld3xr72ygi1lhlgqp1fcb2pjgw9dv032c5bbrkll78")))

(define-public crate-interning-0.1.2 (c (n "interning") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "stable-hash") (r "^0.4.3") (d #t) (k 0)))) (h "1dwyryxzm2al07mhdqqnzgyp3mncq847gj6blywkb7gk770ibkhz")))

(define-public crate-interning-0.1.3 (c (n "interning") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "stable-hash") (r "^0.4.3") (d #t) (k 0)))) (h "00m9012lj3qsa144j42yrlblm5w4lhc0zs0gsk0p13lfp7xh3iix")))

(define-public crate-interning-0.1.4 (c (n "interning") (v "0.1.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "stable-hash") (r "^0.4.3") (d #t) (k 0)))) (h "1qvyd320jrgcjw5652j616x3567x68mypyf74cq8r8gcmjahka0s")))

(define-public crate-interning-0.2.0 (c (n "interning") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "stable-hash") (r "^0.4.3") (d #t) (k 0)))) (h "1an771ylrfq5ng5ky3kbmnmaw3rrgdccvq4ar622lnvn6iqwb7pi") (y #t)))

(define-public crate-interning-0.2.1 (c (n "interning") (v "0.2.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "stable-hash") (r "^0.4.3") (d #t) (k 0)))) (h "0ipxqkw0wzcs72cv3h2vqkgnznx6ggjqrp495y3p4dq5w79kxmfj") (y #t)))

(define-public crate-interning-0.2.2 (c (n "interning") (v "0.2.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "stable-hash") (r "^0.4.3") (d #t) (k 0)))) (h "0sh5fxgnjyjcpvrvx6bigaakxvczkm8rm0qfknbhw0fva76fb84l") (y #t)))

(define-public crate-interning-0.2.3 (c (n "interning") (v "0.2.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "stable-hash") (r "^0.4.3") (d #t) (k 0)))) (h "0mp78kgnb66a156bj8l8yvrcmhh1iiwgk2lvmcm1wb30znk9al0k")))

