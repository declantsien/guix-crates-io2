(define-module (crates-io in te interoptopus_proc) #:use-module (crates-io))

(define-public crate-interoptopus_proc-0.1.0 (c (n "interoptopus_proc") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1v1xmmiijm7b65j2r406hm4la8gfhq17lx0zh01q15wrq745plxa")))

(define-public crate-interoptopus_proc-0.1.1 (c (n "interoptopus_proc") (v "0.1.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "interoptopus") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lpw8533dbsslnav4w851iidrs32k7mp2csk5r5224jwg1mzs3r8")))

(define-public crate-interoptopus_proc-0.1.4 (c (n "interoptopus_proc") (v "0.1.4") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "interoptopus") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fj6jhz41vrwcf60wi9vrnsz6ldjag2ii9mpmim1n4m64kxy61qg")))

(define-public crate-interoptopus_proc-0.1.5 (c (n "interoptopus_proc") (v "0.1.5") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "interoptopus") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1756y75d4m60z8jrzsvv6l76cf7z3nlmncn9nfzycy93ddf6vffh")))

(define-public crate-interoptopus_proc-0.1.6 (c (n "interoptopus_proc") (v "0.1.6") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "interoptopus") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ldxrzyrrilwgjj77pihnm5bfz8szb5h5grh9v90pbbg8da5xaf3")))

(define-public crate-interoptopus_proc-0.1.7 (c (n "interoptopus_proc") (v "0.1.7") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "interoptopus") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mbhhvjwd3zmvazldg69aznv2ilfs9fdw494y8y0526v3in4fzfc")))

(define-public crate-interoptopus_proc-0.1.8 (c (n "interoptopus_proc") (v "0.1.8") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "interoptopus") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16wjz5aqxwq1bcab4k2h066y2xmkjgxkzz027030n59gm6jr0xnq")))

(define-public crate-interoptopus_proc-0.1.12 (c (n "interoptopus_proc") (v "0.1.12") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "interoptopus") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p00rza94nrljj2k1b74ribg8579g5ivqy02wvs69sdqdxzy4n36")))

(define-public crate-interoptopus_proc-0.1.13 (c (n "interoptopus_proc") (v "0.1.13") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "interoptopus") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ny3lil3pa90lilc9619fq5labkhs07q7pfnpp00xsgm6r1bgd4c")))

(define-public crate-interoptopus_proc-0.1.14 (c (n "interoptopus_proc") (v "0.1.14") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "interoptopus") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lsh4lnv7h8h2gipbs84cp2r5g3691zpm332y2kv2gk64cj64fam")))

(define-public crate-interoptopus_proc-0.1.15 (c (n "interoptopus_proc") (v "0.1.15") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "interoptopus") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17366xzg3zvz7kn21nxlqyc263s0bj7fwgplnh6032zi8mmksxbz")))

(define-public crate-interoptopus_proc-0.1.16 (c (n "interoptopus_proc") (v "0.1.16") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "interoptopus") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i0q9i5kl8gss8iw2sz4qdrg78fc374sq6b5rd9qwrh79i12hgch")))

(define-public crate-interoptopus_proc-0.1.17 (c (n "interoptopus_proc") (v "0.1.17") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "interoptopus") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rk8gmvrpcn5czlad7wjln4jgm1c33pk2ygnshillpxy09kdkl9h")))

(define-public crate-interoptopus_proc-0.2.1 (c (n "interoptopus_proc") (v "0.2.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wxg5bk93qj54sxiasc1250fb9l821h9mwxv1hm8d23c4rdg7k10")))

(define-public crate-interoptopus_proc-0.2.3 (c (n "interoptopus_proc") (v "0.2.3") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1m2a5fczdl3s1569p8g1bk7cwdm3caj9ha6hji26ypm0x5bgiq5f")))

(define-public crate-interoptopus_proc-0.3.0 (c (n "interoptopus_proc") (v "0.3.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18yfmc9b36sc8bmnlb6mv39g7wbfsmix0i7ry9zd88x4zm3xldcs")))

(define-public crate-interoptopus_proc-0.3.2 (c (n "interoptopus_proc") (v "0.3.2") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03daqrxdg1zpiblq4l55nv508cas1bgmq73xl4ng4a5qkqbx356f")))

(define-public crate-interoptopus_proc-0.3.3 (c (n "interoptopus_proc") (v "0.3.3") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1na9icsg6ymwx6bvw79q0s8mv6dq6nap1z0553rmdzbs20kgjq3k")))

(define-public crate-interoptopus_proc-0.3.4 (c (n "interoptopus_proc") (v "0.3.4") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13rwq08lifh94ss77z2a702q5kclq77w60cla3hbxxydchxxcin9")))

(define-public crate-interoptopus_proc-0.5.1 (c (n "interoptopus_proc") (v "0.5.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zs0plkj1pm8nw4m6gimw1vgmwfb4gxiv00xgwpr12hl2zfxjy2g")))

(define-public crate-interoptopus_proc-0.5.2 (c (n "interoptopus_proc") (v "0.5.2") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03i1aphz1c596abm09q41bmlzds5dkgq88mkmj9qf7r0pjg3xrdh")))

(define-public crate-interoptopus_proc-0.6.0 (c (n "interoptopus_proc") (v "0.6.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jnajsw915lw3kl1rmv1zlkc5ls11gff381ws3jnyvk12grlkrib")))

(define-public crate-interoptopus_proc-0.7.0 (c (n "interoptopus_proc") (v "0.7.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05b17bpn469k9pf1bfpxhw7mm74yw41q23m3ifdmasp74pky2rpb")))

(define-public crate-interoptopus_proc-0.7.2 (c (n "interoptopus_proc") (v "0.7.2") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rw9n2ska3yg9qpibdd6r9a7yjndgp0dcpcgs0avyik4awxmkgz2")))

(define-public crate-interoptopus_proc-0.7.4 (c (n "interoptopus_proc") (v "0.7.4") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17viilcf31i798rv9bgp4gd6jv38xfhi81ywacpl6v38yqj2xnsh")))

(define-public crate-interoptopus_proc-0.7.5 (c (n "interoptopus_proc") (v "0.7.5") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1idb09yxwnwh6gmjm0yfalj2yf6b0s56mwfz5qqv9nd2c99cxjp4")))

(define-public crate-interoptopus_proc-0.7.6 (c (n "interoptopus_proc") (v "0.7.6") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bkv278krsxxhchzkhca4ha45y7majmr1wz95p2xrjjyfvmbp2wl")))

(define-public crate-interoptopus_proc-0.8.0 (c (n "interoptopus_proc") (v "0.8.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18rngffbgz8skbmbi9j3hw1g1jmqyccvj8q9dp94ly18pk5j8m0w")))

(define-public crate-interoptopus_proc-0.8.1 (c (n "interoptopus_proc") (v "0.8.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0la0mjb20vqakam354dslp7mlpc0vp5jk4yf2j4msw3gkvkxcgri")))

(define-public crate-interoptopus_proc-0.9.0 (c (n "interoptopus_proc") (v "0.9.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0iz52fzy7c3cfwncc52mjjmrngd1wbnaaaswkgdq8x0zxy7ps3s7")))

(define-public crate-interoptopus_proc-0.10.0 (c (n "interoptopus_proc") (v "0.10.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0g5vfs1v8qb9c7cgw5s89rr8f0hdgxscrf2sr1mlgpb6w3lmnygb")))

(define-public crate-interoptopus_proc-0.10.1 (c (n "interoptopus_proc") (v "0.10.1") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16sf7qm016f3ckfc8y1yh3mfppbzjh975s2afzlbphk85b03q9y2")))

(define-public crate-interoptopus_proc-0.11.3 (c (n "interoptopus_proc") (v "0.11.3") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0z1p7a6r3jwjr1195l0h4mbc6a5sbslx5zw2gn7qd6c2w00yghha")))

(define-public crate-interoptopus_proc-0.12.0 (c (n "interoptopus_proc") (v "0.12.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cm6dwi5bk8hzxa46rxl18wnlrm1b27d1vzgw6r7skmyk76hl55x")))

(define-public crate-interoptopus_proc-0.13.0 (c (n "interoptopus_proc") (v "0.13.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dww853yaag58pzrz4s095m6acblcq3sbdg1swkf8n1g46fmhbbs")))

(define-public crate-interoptopus_proc-0.13.3 (c (n "interoptopus_proc") (v "0.13.3") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full"))) (d #t) (k 0)))) (h "04xfhklpp9bxmqcqc3z6kq28zalwzp1s42bblfqbmarjh1maw1ww")))

(define-public crate-interoptopus_proc-0.13.6 (c (n "interoptopus_proc") (v "0.13.6") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full"))) (d #t) (k 0)))) (h "0a59nyhxr1cq5lb34z54nz3b50isq3y1km0iyng7rrwic0iikc50")))

(define-public crate-interoptopus_proc-0.13.7 (c (n "interoptopus_proc") (v "0.13.7") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full"))) (d #t) (k 0)))) (h "1kqg94g46vj7ad99fxk0c2zzr40dhs1ng6wihi046l05j06y9dy5")))

(define-public crate-interoptopus_proc-0.13.8 (c (n "interoptopus_proc") (v "0.13.8") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full"))) (d #t) (k 0)))) (h "0h0853bbybxhxq5423nbygkldg2105qdclq8kpsdb6kj3d7y204x")))

(define-public crate-interoptopus_proc-0.13.10 (c (n "interoptopus_proc") (v "0.13.10") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full"))) (d #t) (k 0)))) (h "0idbznjszr5m5ikq3r1sy6bdc9ahh6dk5w4dvii7gjsxgvrndifd")))

(define-public crate-interoptopus_proc-0.13.13 (c (n "interoptopus_proc") (v "0.13.13") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full"))) (d #t) (k 0)))) (h "0db7s1dx44pz6a3fxpnkvhq3qik898zryz8nkbzag8vhnk3gz6il")))

(define-public crate-interoptopus_proc-0.14.0 (c (n "interoptopus_proc") (v "0.14.0") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "127kynaaljiavykwr19a9jmxrkp75f4711p84zcah16w1xyjxb3k")))

(define-public crate-interoptopus_proc-0.14.2 (c (n "interoptopus_proc") (v "0.14.2") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0w9a13hkmpidikphvdlbxldqpdjv67z8v3ha6mcbw7zahrm93xkk")))

(define-public crate-interoptopus_proc-0.14.6 (c (n "interoptopus_proc") (v "0.14.6") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "132cb87ngqxvgvz8smr0mh8qgvmxlnygfnbm4cqgdmwm9w0mbl49")))

(define-public crate-interoptopus_proc-0.14.8 (c (n "interoptopus_proc") (v "0.14.8") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "17qgd5p72d271bpbmdsfrc9jzdydrj6g92phf2gb2jiw5xag09pg")))

(define-public crate-interoptopus_proc-0.14.9 (c (n "interoptopus_proc") (v "0.14.9") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "01ps516sjbm5l1rd8jgl0f4m4znns67h1vqchghy1cc8sq6avcp2")))

(define-public crate-interoptopus_proc-0.14.18 (c (n "interoptopus_proc") (v "0.14.18") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "0kq6ckj5w0p9qvnskl4pc4zfg47kd2c18jdi6ss5jw1jbg6m0b1v")))

(define-public crate-interoptopus_proc-0.14.19 (c (n "interoptopus_proc") (v "0.14.19") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1rj1a9lmfxwmr1csq63znlyqwq69y5v89w35lxdg78rpwjyz9x7v")))

(define-public crate-interoptopus_proc-0.14.23 (c (n "interoptopus_proc") (v "0.14.23") (d (list (d (n "darling") (r "^0.13.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1f5ijin8v75d94d7byp2hkb6v2ipgvp2pzwc7da0rkaiaha6w7g2")))

