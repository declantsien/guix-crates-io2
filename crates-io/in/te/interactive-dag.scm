(define-module (crates-io in te interactive-dag) #:use-module (crates-io))

(define-public crate-interactive-dag-0.0.1 (c (n "interactive-dag") (v "0.0.1") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "generational-arena") (r "^0.2.8") (d #t) (k 0)))) (h "1hxkahcwkip04hz2n36gship99qfpd9rfpx0y9cahrr7p7agkksw") (f (quote (("test-utils" "arbitrary"))))))

