(define-module (crates-io in te intervaltree) #:use-module (crates-io))

(define-public crate-intervaltree-0.1.0 (c (n "intervaltree") (v "0.1.0") (h "0byv0xqq7npm83931rb0b4m9hywjlgw87l6vqz85c1jg63q99qmn")))

(define-public crate-intervaltree-0.2.0 (c (n "intervaltree") (v "0.2.0") (d (list (d (n "smallvec") (r "^0.4") (d #t) (k 0)))) (h "01p1m90c412xdnsxncw0cz5n8fm1v6scxb78wh83ywzb01s0xpz4")))

(define-public crate-intervaltree-0.2.1 (c (n "intervaltree") (v "0.2.1") (d (list (d (n "smallvec") (r "^0.4") (d #t) (k 0)))) (h "0xxvz5gcxhz6pbr62dlvp5dk6bmv3vf5cfrs4ysvki86ynn3s9vq")))

(define-public crate-intervaltree-0.2.2 (c (n "intervaltree") (v "0.2.2") (d (list (d (n "smallvec") (r "^0.4") (d #t) (k 0)))) (h "1khm4jaqllabf5mrip3c58z11z54pxhlnff0cmcqspmbnki32jwv")))

(define-public crate-intervaltree-0.2.3 (c (n "intervaltree") (v "0.2.3") (d (list (d (n "smallvec") (r "^0.4") (k 0)))) (h "06mkqxfmjvqw4q84x6z6bz2p9lxvlnpk0v3p1kbkfs171734jsaq") (f (quote (("std" "smallvec/std") ("default" "std"))))))

(define-public crate-intervaltree-0.2.4 (c (n "intervaltree") (v "0.2.4") (d (list (d (n "smallvec") (r "^0.6") (k 0)))) (h "10k40gsv79kwnsqrzwmnmm6psa5fqws8yggavmbggvymv16hffdg") (f (quote (("std" "smallvec/std") ("default" "std"))))))

(define-public crate-intervaltree-0.2.5 (c (n "intervaltree") (v "0.2.5") (d (list (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "0i1srdiac558xyifah3ybw9nnsfpqcf1by01s34k8iv6xb9asm42") (f (quote (("std") ("default" "std"))))))

(define-public crate-intervaltree-0.2.6 (c (n "intervaltree") (v "0.2.6") (d (list (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "0b53jlayfm732kmgfn7wicj399hpv649b70srh4mhp6cnnimlvan") (f (quote (("std") ("default" "std"))))))

(define-public crate-intervaltree-0.2.7 (c (n "intervaltree") (v "0.2.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (o #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "04fixpkg892mchgl1ikkchpi7iwx9l91r1wc3a0anz04ax7c62r7") (f (quote (("std") ("default" "std"))))))

