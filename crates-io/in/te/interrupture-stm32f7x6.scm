(define-module (crates-io in te interrupture-stm32f7x6) #:use-module (crates-io))

(define-public crate-interrupture-stm32f7x6-0.1.0 (c (n "interrupture-stm32f7x6") (v "0.1.0") (d (list (d (n "cortex-m-rt") (r "^0.6.4") (d #t) (k 0)) (d (n "interrupture") (r "^0.1.0") (d #t) (k 0)) (d (n "stm32f7") (r "^0.3.2") (f (quote ("stm32f7x6" "rt"))) (d #t) (k 0)))) (h "15ivczncf4ypqnd8y6jw67m5qzba7221kcyidwximz4sm753a89q")))

