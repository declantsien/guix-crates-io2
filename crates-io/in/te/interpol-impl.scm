(define-module (crates-io in te interpol-impl) #:use-module (crates-io))

(define-public crate-interpol-impl-0.1.0 (c (n "interpol-impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gpgvygx9r34xqyqz9hfqsybdhc3r4bl2sia7zqlwybyr7zjiaka")))

