(define-module (crates-io in te interaction) #:use-module (crates-io))

(define-public crate-interaction-0.1.0 (c (n "interaction") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)))) (h "08ilj08xckmq1d7xadlcl087lqggwavb90pfrpcyr24k53a2dp1f") (y #t)))

(define-public crate-interaction-0.2.0 (c (n "interaction") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)))) (h "0kibp4798il8x54cjf42nmcmlllq0g0f5g8939n0fl7vrmwvkfj5") (y #t)))

(define-public crate-interaction-0.3.0 (c (n "interaction") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)))) (h "0qhh77vq93496qns057jbi5hpiaqahlrvibapiwmx3hcicw68y6x")))

(define-public crate-interaction-0.3.1 (c (n "interaction") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)))) (h "17z9v77zxmk9zxrs3qm4ziid1p8cl7n475dmgn4dkx70pf6pbbfy")))

(define-public crate-interaction-0.3.2 (c (n "interaction") (v "0.3.2") (d (list (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)))) (h "0bqhrvqsq3ggzwh7y1m44vhhz1in3jbjjrvw10spsggrwj3n23l8")))

(define-public crate-interaction-0.3.3 (c (n "interaction") (v "0.3.3") (d (list (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)))) (h "0dkj3xaidl5q88mbnnrnbqxf972ndpgdcc5gha2zqd49iwxqqppz")))

(define-public crate-interaction-0.3.4 (c (n "interaction") (v "0.3.4") (d (list (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)))) (h "1h72glkv6x32zgi2r5wpn8gbm3nyvvmlmqk4n9d5qzl11p3r7s77")))

