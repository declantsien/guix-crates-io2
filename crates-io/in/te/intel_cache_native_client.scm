(define-module (crates-io in te intel_cache_native_client) #:use-module (crates-io))

(define-public crate-intel_cache_native_client-1.0.0 (c (n "intel_cache_native_client") (v "1.0.0") (d (list (d (n "intel-cache-lib") (r "^1.0.0") (d #t) (k 0)))) (h "1dwv9cmsc7b24x9y2nxdnl31w9chkiywfrffd0lfb6ki20ph5p78")))

(define-public crate-intel_cache_native_client-2.0.0 (c (n "intel_cache_native_client") (v "2.0.0") (d (list (d (n "intel-cache-lib") (r "^2.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.1") (d #t) (k 0)))) (h "0x8n9fwlxq4grq16qy56nah3fwvsb68bvlaax7izc5b2x93m0w4f")))

