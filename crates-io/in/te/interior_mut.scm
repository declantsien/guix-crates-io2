(define-module (crates-io in te interior_mut) #:use-module (crates-io))

(define-public crate-interior_mut-0.1.0-beta0 (c (n "interior_mut") (v "0.1.0-beta0") (h "1zlcpajxwx1r2h95knqki30s0vs078k8l22z2yml1bq99ralcfpf") (f (quote (("std") ("default" "std"))))))

(define-public crate-interior_mut-0.1.0 (c (n "interior_mut") (v "0.1.0") (h "1ldcrm2j7j1wsk79pgsl7ym5k1qfxasm8f3xn7ni4zrsvkvxrzvl") (f (quote (("std") ("default" "std"))))))

