(define-module (crates-io in te interception-sys) #:use-module (crates-io))

(define-public crate-interception-sys-0.1.0 (c (n "interception-sys") (v "0.1.0") (h "1gzb0zwzwmch8x28k5fp41vjvc3idnyv8wq33dqxh0a3gfps6x06")))

(define-public crate-interception-sys-0.1.1 (c (n "interception-sys") (v "0.1.1") (h "0ayhhr7kmii5kgf0dmqwbi0pdljadh424ddisdbj0gpql0c4s675")))

(define-public crate-interception-sys-0.1.2 (c (n "interception-sys") (v "0.1.2") (h "0xdqy0hr6lp77796nzmpsdp80xc2b9rfbjpazyh51fij8cdifllc")))

(define-public crate-interception-sys-0.1.3 (c (n "interception-sys") (v "0.1.3") (h "1lgwbml7gzq5a5rriy708w68gx6yiw9cdg7xy2c5vsrrck7pbs5b")))

