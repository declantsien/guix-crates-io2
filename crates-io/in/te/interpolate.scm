(define-module (crates-io in te interpolate) #:use-module (crates-io))

(define-public crate-interpolate-0.1.0 (c (n "interpolate") (v "0.1.0") (h "1b3c1c85am9c062yisn4n9yprl8m8ib6hq9k5g84c8vsc65q0pn5")))

(define-public crate-interpolate-0.1.1 (c (n "interpolate") (v "0.1.1") (h "1y6jm6bqp2p5pf8wnihz7gyzbv2pj6ad7alz9sb075rq7xclgs4v")))

(define-public crate-interpolate-0.1.2 (c (n "interpolate") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.4") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.1.0") (d #t) (k 0)))) (h "1bv71qb01aw887793fwfvzzwvk6vcm580i5yhg32sqvqkkhdrlyv")))

(define-public crate-interpolate-0.2.0 (c (n "interpolate") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.4") (d #t) (k 0)))) (h "1f1052dpw6jdfiq02d4h51x2df00vx7qys1i1is8v6fa080mcra4")))

(define-public crate-interpolate-0.2.1 (c (n "interpolate") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.4") (d #t) (k 0)))) (h "0lcyrsg4wdlibydrrzccgb20jra9l381cpcjmaf4k943blj9p38k")))

(define-public crate-interpolate-0.2.2 (c (n "interpolate") (v "0.2.2") (h "0f6z0d55c81sxh2v41p4sw5ivm1an00sjx1jjq09wxxc8yxbdmh0")))

(define-public crate-interpolate-0.2.3 (c (n "interpolate") (v "0.2.3") (h "1yz1m0638jn5kphafs2cjl5v2vvj0dqbc082h23hxy1wnba9pxvp")))

