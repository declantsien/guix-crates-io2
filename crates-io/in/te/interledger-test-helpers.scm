(define-module (crates-io in te interledger-test-helpers) #:use-module (crates-io))

(define-public crate-interledger-test-helpers-0.1.0 (c (n "interledger-test-helpers") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "interledger-ildcp") (r "^0.1.0") (d #t) (k 0)) (d (n "interledger-packet") (r "^0.1.0") (d #t) (k 0)) (d (n "interledger-service") (r "^0.1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7.1") (d #t) (k 0)))) (h "1750bvlbssj0v4wz7grxvxxys739miy2290xjx56yfb3ydy7mxy0")))

