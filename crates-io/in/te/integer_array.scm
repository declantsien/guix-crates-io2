(define-module (crates-io in te integer_array) #:use-module (crates-io))

(define-public crate-integer_array-0.1.0 (c (n "integer_array") (v "0.1.0") (d (list (d (n "num") (r "^0.4") (k 0)))) (h "1qp47fs04ndvnwrvmgrp2674zh53k8wnp9271500gq4kbch7fvfk")))

(define-public crate-integer_array-0.1.1 (c (n "integer_array") (v "0.1.1") (d (list (d (n "num") (r "^0.4") (k 0)))) (h "1ckas5m1p65lvnscpm5qwpxkm4dciv4bnrpdqrj4c1fs20qaj585")))

(define-public crate-integer_array-0.1.2 (c (n "integer_array") (v "0.1.2") (d (list (d (n "num") (r "^0.4") (k 0)))) (h "0wlhpp4kmjvhw0y1gwxl57j3r6k4zbcj5g2v0vgbhbvvsq0viwqg")))

(define-public crate-integer_array-0.1.3 (c (n "integer_array") (v "0.1.3") (d (list (d (n "num") (r "^0.4") (k 0)))) (h "1k7r0bxzdyflj7jygjvx62sbyybgfiz997g0ayrmq4497xd3pv5x")))

(define-public crate-integer_array-0.2.0 (c (n "integer_array") (v "0.2.0") (d (list (d (n "fixed") (r "^1.10.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "0iwm1c2qy7kfhkqv0g32ij7j45pvcqifw53cb9amip2qhqrcp1sf")))

(define-public crate-integer_array-0.2.1 (c (n "integer_array") (v "0.2.1") (d (list (d (n "fixed") (r "^1.10.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "1lggnd154ac65ckb3p2qkagrahsjag31dcax96ii0i078liw6c5b")))

(define-public crate-integer_array-0.3.0 (c (n "integer_array") (v "0.3.0") (d (list (d (n "fixed") (r "^1.10.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "spectrum-analyzer") (r "^1.1.2") (d #t) (k 0)))) (h "0w3qd6pkp87qjwz56zcpvjf8yh7qhrphab3a4dvdhqmxf8nzbibl")))

