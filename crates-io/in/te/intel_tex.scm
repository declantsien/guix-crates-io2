(define-module (crates-io in te intel_tex) #:use-module (crates-io))

(define-public crate-intel_tex-0.1.0 (c (n "intel_tex") (v "0.1.0") (d (list (d (n "ddsfile") (r "^0.2.3") (d #t) (k 2)) (d (n "image") (r "^0.21.0") (d #t) (k 2)) (d (n "ispc") (r "^0.3.8") (d #t) (k 0)) (d (n "ispc") (r "^0.3.8") (d #t) (k 1)))) (h "1s7iq7j8h1l527l9p9hfgbpznc198xlni7jjplwlrs243284qxz9")))

(define-public crate-intel_tex-0.1.1 (c (n "intel_tex") (v "0.1.1") (d (list (d (n "ddsfile") (r "^0.2.3") (d #t) (k 2)) (d (n "image") (r "^0.21.0") (d #t) (k 2)) (d (n "ispc") (r "^0.3.8") (d #t) (k 0)) (d (n "ispc") (r "^0.3.8") (d #t) (k 1)))) (h "18xb7qrvzcy7jd1xllynvpw5yddmphhbb457nyhm3ncsninmvjb7")))

(define-public crate-intel_tex-0.1.2 (c (n "intel_tex") (v "0.1.2") (d (list (d (n "ddsfile") (r "^0.2.3") (d #t) (k 2)) (d (n "image") (r "^0.21.0") (d #t) (k 2)) (d (n "ispc") (r "^0.3.8") (d #t) (k 0)) (d (n "ispc") (r "^0.3.8") (d #t) (k 1)))) (h "0wbcjy4f38i32ki2km0q46pr1mg2by4a4386bvdidj09954v4dbc")))

(define-public crate-intel_tex-0.1.3 (c (n "intel_tex") (v "0.1.3") (d (list (d (n "ddsfile") (r "^0.2.3") (d #t) (k 2)) (d (n "image") (r "^0.21.0") (d #t) (k 2)) (d (n "ispc_compile") (r "^1.0.4") (o #t) (d #t) (k 1)) (d (n "ispc_rt") (r "^1.0.2") (d #t) (k 0)) (d (n "ispc_rt") (r "^1.0.2") (d #t) (k 1)))) (h "0fa8wdrbr1c8qr3nzy29kxp7gfrisdssi8zfdvgqpmb6kxxvrddc") (f (quote (("ispc" "ispc_compile"))))))

(define-public crate-intel_tex-0.1.4 (c (n "intel_tex") (v "0.1.4") (d (list (d (n "ddsfile") (r "^0.2.3") (d #t) (k 2)) (d (n "image") (r "^0.21.0") (d #t) (k 2)) (d (n "ispc_compile") (r "^1.0.4") (o #t) (d #t) (k 1)) (d (n "ispc_rt") (r "^1.0.2") (d #t) (k 0)) (d (n "ispc_rt") (r "^1.0.2") (d #t) (k 1)))) (h "11ml0vygcfhpv8mr62sn6d100z1ifmhixz7id0zs0ms4502b4939") (f (quote (("ispc" "ispc_compile"))))))

