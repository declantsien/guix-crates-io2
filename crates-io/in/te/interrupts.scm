(define-module (crates-io in te interrupts) #:use-module (crates-io))

(define-public crate-interrupts-0.1.0 (c (n "interrupts") (v "0.1.0") (h "0m026xx09ik21pxjyljh9yx4y9k3bl73d8x68zks2j08hqa3r689") (y #t)))

(define-public crate-interrupts-0.1.1 (c (n "interrupts") (v "0.1.1") (d (list (d (n "nix") (r "^0.27") (f (quote ("signal"))) (d #t) (t "cfg(unix)") (k 0)))) (h "1b9ldgzpaw4c20whl5r2z2bi1mp5qh0j2r371kn5k3sh2szxqvn4") (y #t)))

(define-public crate-interrupts-0.1.2 (c (n "interrupts") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("signal"))) (d #t) (t "cfg(unix)") (k 0)))) (h "09fs9x10xf64kn82yd3bl1liraij7042wb1rb16l7cnwymgdxns8") (y #t)))

(define-public crate-interrupts-0.1.3 (c (n "interrupts") (v "0.1.3") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("signal"))) (d #t) (t "cfg(unix)") (k 0)))) (h "1gdm8igk8h6840nvks1mar1lpm2kq45vfd2bxnfs70hahzar1kyp")))

(define-public crate-interrupts-0.1.4 (c (n "interrupts") (v "0.1.4") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("signal"))) (d #t) (t "cfg(unix)") (k 0)))) (h "13jpmy8025rxfmr2jd45hk616k2kqkjfm9a5zhwi4wpkaxlx4yfj")))

(define-public crate-interrupts-0.1.5 (c (n "interrupts") (v "0.1.5") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0.28") (f (quote ("signal"))) (d #t) (t "cfg(unix)") (k 0)))) (h "1in82kkxfvmc6qcmywskcymhix5b9bcrngf2xdr5gz6fr6yv1k61")))

