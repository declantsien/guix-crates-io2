(define-module (crates-io in te integer-encoding) #:use-module (crates-io))

(define-public crate-integer-encoding-0.9.0 (c (n "integer-encoding") (v "0.9.0") (h "1z4v1116zfz7s17dzcipmi85cfavsb7n828ikywmdapjabik9sy1") (y #t)))

(define-public crate-integer-encoding-0.9.1 (c (n "integer-encoding") (v "0.9.1") (h "0sa18i7668acgxznr6fpm0ycnms2f3p1kanrd9ddvrsxbhfmsmna") (y #t)))

(define-public crate-integer-encoding-0.9.2 (c (n "integer-encoding") (v "0.9.2") (h "147hxnbn5js9qm0432pz1l3w4j57xvqgbj6mdpp8hpxlbivfkf9g") (y #t)))

(define-public crate-integer-encoding-0.10.0 (c (n "integer-encoding") (v "0.10.0") (h "0cdmd413l1wvsfqq8am7w75p3ljxn383kwwra5rzl92ympj82is4")))

(define-public crate-integer-encoding-0.11.0 (c (n "integer-encoding") (v "0.11.0") (h "1ndd9qb503pllj7hql4m2xwcis7nbkq01jbmvqnzjc0qvk9qk0kw")))

(define-public crate-integer-encoding-0.12.0 (c (n "integer-encoding") (v "0.12.0") (h "0yna2mrlbm886g0d32km2bd77b9l9i8nx2rbdp7izb82qnlsr1jx")))

(define-public crate-integer-encoding-1.0.0 (c (n "integer-encoding") (v "1.0.0") (h "1r7nka7gibm9bm0fhpzanszqij0raidsjjdpwjsqa66n5gm68jn9")))

(define-public crate-integer-encoding-1.0.1 (c (n "integer-encoding") (v "1.0.1") (h "03brxzi4y5yxl1s1baxczkqgynvp1mj6gvh9cqz4wwp8mch1cjk1")))

(define-public crate-integer-encoding-1.0.2 (c (n "integer-encoding") (v "1.0.2") (h "0369n85clm4zv01z1gac72iwypr6h29awz05lx1yzc1sh4fpfjya")))

(define-public crate-integer-encoding-1.0.3 (c (n "integer-encoding") (v "1.0.3") (h "1jwabf1xw1n6xh7fx3c92kglqbn6fv0pvhqjl0m1znxpvk3wjlx0")))

(define-public crate-integer-encoding-1.0.4 (c (n "integer-encoding") (v "1.0.4") (h "0z2m9hk7mxb9n5sj85gs4a48lz6ny0c4hfkmlxajlwz4awm872gb")))

(define-public crate-integer-encoding-1.0.5 (c (n "integer-encoding") (v "1.0.5") (h "19cz60vhxkbps9bsv1691z8rn1whzwh7ywcdx23zc2k85sy6qx16")))

(define-public crate-integer-encoding-1.0.6 (c (n "integer-encoding") (v "1.0.6") (h "0daspmpmkyyppsy4azib96sy9b8wmwzvb6lngmqvzlbxwyl7njz4")))

(define-public crate-integer-encoding-1.0.7 (c (n "integer-encoding") (v "1.0.7") (h "154rf8r5xab0pvbdf2icx8jhvcqhrc1hx9lcmq70zyicbv0qkv0s")))

(define-public crate-integer-encoding-1.0.8 (c (n "integer-encoding") (v "1.0.8") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)))) (h "12l8qdhjixx0vd3rq62f28px86gqna24gnvjgadmddwshbagv06a")))

(define-public crate-integer-encoding-1.1.0 (c (n "integer-encoding") (v "1.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "0fjgbc72pjg5ps5zvqm0abjyjc2k372p2rsl15k06kp0497jlyxc") (f (quote (("tokio_async" "tokio") ("futures_async" "futures-util"))))))

(define-public crate-integer-encoding-1.1.1 (c (n "integer-encoding") (v "1.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "1msn823y1549wcbc4mbhb8z24pnvsdcwgad7bi6iirdgss2g0gw3") (f (quote (("tokio_async" "tokio") ("futures_async" "futures-util"))))))

(define-public crate-integer-encoding-1.1.2 (c (n "integer-encoding") (v "1.1.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "0p5l9kcf4pvvia7rjkhn2h9mgj8vgn3wy32jk33z37zqal0jhfjm") (f (quote (("tokio_async" "tokio") ("futures_async" "futures-util"))))))

(define-public crate-integer-encoding-1.1.3 (c (n "integer-encoding") (v "1.1.3") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "16dji5yj6jv5z8qyp8cb0dqy5kjk9jrwyfiikpbbr60hg92cbsa0") (f (quote (("tokio_async" "tokio" "async-trait") ("futures_async" "futures-util" "async-trait"))))))

(define-public crate-integer-encoding-1.1.4 (c (n "integer-encoding") (v "1.1.4") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "06l93fb0rk3rx44gwi5a3wbs222xvxpp62i1mwx2i3azi58zwg08") (f (quote (("tokio_async" "tokio" "async-trait") ("futures_async" "futures-util" "async-trait"))))))

(define-public crate-integer-encoding-1.1.5 (c (n "integer-encoding") (v "1.1.5") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "0996hn3pr91niiwmgmwz04m08qan00cy5cz9fccz3q4vs85vskjg") (f (quote (("tokio_async" "tokio" "async-trait") ("futures_async" "futures-util" "async-trait"))))))

(define-public crate-integer-encoding-1.2.0 (c (n "integer-encoding") (v "1.2.0") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "1bs0s02f439fgbadjdx28law0q8x713m94chh1z9rb40c0iwdap2") (f (quote (("tokio_async" "tokio" "async-trait") ("futures_async" "futures-util" "async-trait")))) (y #t)))

(define-public crate-integer-encoding-1.2.1 (c (n "integer-encoding") (v "1.2.1") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "1vbx19appccn8mg7xlax49xia8l5gfi03jpz05j4nk0fdzlnyl8i") (f (quote (("tokio_async" "tokio" "async-trait") ("futures_async" "futures-util" "async-trait")))) (y #t)))

(define-public crate-integer-encoding-2.0.0 (c (n "integer-encoding") (v "2.0.0") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "19hjqaadak4qv6hrr1xldk8j1q2p75vl5bh2nkbg77xq8xkjd9a8") (f (quote (("tokio_async" "tokio" "async-trait") ("futures_async" "futures-util" "async-trait"))))))

(define-public crate-integer-encoding-2.1.0 (c (n "integer-encoding") (v "2.1.0") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("fs" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1af4ry3aal47bh7hsn252s758qcdqq1d1jgscsm1izsnywkw9gjc") (f (quote (("tokio_async" "tokio" "async-trait") ("futures_async" "futures-util" "async-trait"))))))

(define-public crate-integer-encoding-2.1.1 (c (n "integer-encoding") (v "2.1.1") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("fs" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0h4rzbf2v8br9rxdaa0grpq1kskwhja0bcmsqavjrfmqxzgy7nhx") (f (quote (("tokio_async" "tokio" "async-trait") ("futures_async" "futures-util" "async-trait"))))))

(define-public crate-integer-encoding-2.0.1 (c (n "integer-encoding") (v "2.0.1") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "0gyzm445bkvqn56pn5vdjrrszipg4clwxkvfxcydicw904jpmjvd") (f (quote (("tokio_async" "tokio" "async-trait") ("futures_async" "futures-util" "async-trait"))))))

(define-public crate-integer-encoding-3.0.0 (c (n "integer-encoding") (v "3.0.0") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "bencher") (r "~0.1") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("fs" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1xf8x2s4y9n1msbjcp58bnyphsavd5ihhh45b4g2f36jkvv008dw") (f (quote (("tokio_async" "tokio" "async-trait") ("futures_async" "futures-util" "async-trait"))))))

(define-public crate-integer-encoding-2.1.2 (c (n "integer-encoding") (v "2.1.2") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "bencher") (r "~0.1") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("fs" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "00dbd9kf8m5ax579y618689nh0fbkapij5yl3bs4q0zl5n5ggqk0") (f (quote (("tokio_async" "tokio" "async-trait") ("futures_async" "futures-util" "async-trait"))))))

(define-public crate-integer-encoding-1.2.2 (c (n "integer-encoding") (v "1.2.2") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "1c032xhif5d6a8nc4cak60g5ylwd6p9569122m5hm67brx2l5yhb") (f (quote (("tokio_async" "tokio" "async-trait") ("futures_async" "futures-util" "async-trait")))) (y #t)))

(define-public crate-integer-encoding-3.0.1 (c (n "integer-encoding") (v "3.0.1") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "bencher") (r "~0.1") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("fs" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "08rv52qfd5jrhq2w9bd4khgpknagj3gz5cwwvcq2x29yq4dc85sx") (f (quote (("tokio_async" "tokio" "async-trait") ("futures_async" "futures-util" "async-trait"))))))

(define-public crate-integer-encoding-1.1.6 (c (n "integer-encoding") (v "1.1.6") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "0rmwizlsdp68lh7lsgxmwjg16hkizf0gnz2ibrlkb22zqcclc47n") (f (quote (("tokio_async" "tokio" "async-trait") ("futures_async" "futures-util" "async-trait"))))))

(define-public crate-integer-encoding-3.0.2 (c (n "integer-encoding") (v "3.0.2") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "bencher") (r "~0.1") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("fs" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1f0mk7d694g2sz4my5zh7i5w4kijx5y16x6dip7fv0pazx013hch") (f (quote (("tokio_async" "tokio" "async-trait") ("futures_async" "futures-util" "async-trait"))))))

(define-public crate-integer-encoding-2.1.3 (c (n "integer-encoding") (v "2.1.3") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "bencher") (r "~0.1") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.3") (f (quote ("fs" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0isw3lz0c3ax6yvi4aymhmbiw2mgcvhyr0sg2g6mr9y3vj3gfzf2") (f (quote (("tokio_async" "tokio" "async-trait") ("futures_async" "futures-util" "async-trait"))))))

(define-public crate-integer-encoding-1.1.7 (c (n "integer-encoding") (v "1.1.7") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "0vxmsfxwsf8dclppdmikjklzh84w341crl0lb3bpydwv18c53p28") (f (quote (("tokio_async" "tokio" "async-trait") ("futures_async" "futures-util" "async-trait"))))))

(define-public crate-integer-encoding-2.0.2 (c (n "integer-encoding") (v "2.0.2") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "rt-threaded" "macros"))) (d #t) (k 2)))) (h "0abq305wrkd8q68xzqsqhjsviki9wa1gh9y5f861bpkb69j8jim9") (f (quote (("tokio_async" "tokio" "async-trait") ("futures_async" "futures-util" "async-trait"))))))

(define-public crate-integer-encoding-3.0.3 (c (n "integer-encoding") (v "3.0.3") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "bencher") (r "~0.1") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("fs" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1gkla0dbqn4cp7i42gq8w7c1gb3yvvycydg1d19qb30jk98a318f") (f (quote (("tokio_async" "tokio" "async-trait") ("futures_async" "futures-util" "async-trait"))))))

(define-public crate-integer-encoding-3.0.4 (c (n "integer-encoding") (v "3.0.4") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "bencher") (r "~0.1") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("fs" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "00ng7jmv6pqwqc8w1297f768bn0spwwicdr7hb40baax00r3gc4b") (f (quote (("tokio_async" "tokio" "async-trait") ("futures_async" "futures-util" "async-trait"))))))

(define-public crate-integer-encoding-4.0.0 (c (n "integer-encoding") (v "4.0.0") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "bencher") (r "~0.1") (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("io-util"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("fs" "rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1gz1r2w5cdv1wslrx57sxkrj0fzrjq5vpflhvnf7ybjfwbqg8kcj") (f (quote (("tokio_async" "tokio" "async-trait") ("futures_async" "futures-util" "async-trait"))))))

