(define-module (crates-io in te interoptopus_backend_cpython) #:use-module (crates-io))

(define-public crate-interoptopus_backend_cpython-0.13.0 (c (n "interoptopus_backend_cpython") (v "0.13.0") (d (list (d (n "interoptopus") (r "^0.13.0") (d #t) (k 0)))) (h "04y189ch073w1j8mzajwx8zjnlk7fpn12xm8v0l7pxr171gffi6p")))

(define-public crate-interoptopus_backend_cpython-0.13.1 (c (n "interoptopus_backend_cpython") (v "0.13.1") (d (list (d (n "interoptopus") (r "^0.13.0") (d #t) (k 0)))) (h "1iapxr9wixjq05vx0zw7wlz46mhp8rb7g7b3y82x1f54j0j5hyi3")))

(define-public crate-interoptopus_backend_cpython-0.13.3 (c (n "interoptopus_backend_cpython") (v "0.13.3") (d (list (d (n "interoptopus") (r "^0.13.0") (d #t) (k 0)))) (h "1dx9hxsilz7d96v0xdhhmnmkaf00wiiz3756619crwxvhkmssmc2")))

(define-public crate-interoptopus_backend_cpython-0.13.4 (c (n "interoptopus_backend_cpython") (v "0.13.4") (d (list (d (n "interoptopus") (r "^0.13.0") (d #t) (k 0)))) (h "0a192jdqqwnfapvz9ikhbg13zrb2h93wdyn551w1zjjicz1wsrqx")))

(define-public crate-interoptopus_backend_cpython-0.13.5 (c (n "interoptopus_backend_cpython") (v "0.13.5") (d (list (d (n "interoptopus") (r "^0.13.0") (d #t) (k 0)))) (h "1s22p6xzpvwk3c76lsif8pg2140pfkdbl0lv9g6696wkjrwjjsqa")))

(define-public crate-interoptopus_backend_cpython-0.13.6 (c (n "interoptopus_backend_cpython") (v "0.13.6") (d (list (d (n "interoptopus") (r "^0.13.0") (d #t) (k 0)))) (h "15hbyx0787kkbs54f22q5g31l9pi6hkw6rvs0fl9i9zw0z46m3ci")))

(define-public crate-interoptopus_backend_cpython-0.13.8 (c (n "interoptopus_backend_cpython") (v "0.13.8") (d (list (d (n "interoptopus") (r "^0.13.0") (d #t) (k 0)))) (h "0aag1h38zack69wixl8hn23x7gsmg5hxia4p1whq6zqnrw2llba6")))

(define-public crate-interoptopus_backend_cpython-0.13.9 (c (n "interoptopus_backend_cpython") (v "0.13.9") (d (list (d (n "interoptopus") (r "^0.13.0") (d #t) (k 0)))) (h "0w6jw2cp6v2a4klpybpfwz15r5ynz7xgxc8ci3j0kdg37xyw8w73")))

(define-public crate-interoptopus_backend_cpython-0.13.11 (c (n "interoptopus_backend_cpython") (v "0.13.11") (d (list (d (n "interoptopus") (r "^0.13.0") (d #t) (k 0)))) (h "0sk45ywriq7d21bb2wjlrd1nlf8njmjvzkf439avmgw77811rc3q")))

(define-public crate-interoptopus_backend_cpython-0.13.12 (c (n "interoptopus_backend_cpython") (v "0.13.12") (d (list (d (n "interoptopus") (r "^0.13.0") (d #t) (k 0)))) (h "01sjzmjqdvfmimqck4wmnf8jag3418gbi0ylyy4an70a6vyvzw6q")))

(define-public crate-interoptopus_backend_cpython-0.13.13 (c (n "interoptopus_backend_cpython") (v "0.13.13") (d (list (d (n "interoptopus") (r "^0.13.0") (d #t) (k 0)))) (h "100viy1fnaffn4qfnklq7y4afflknnh7qbyljv8dhha8kn7q2f5f")))

(define-public crate-interoptopus_backend_cpython-0.13.15 (c (n "interoptopus_backend_cpython") (v "0.13.15") (d (list (d (n "interoptopus") (r "^0.13.0") (d #t) (k 0)))) (h "1dma44jrikp10r6a5ajnaw0sqi5jzziwkj76w55pg2dp7w5awdm2")))

(define-public crate-interoptopus_backend_cpython-0.14.0 (c (n "interoptopus_backend_cpython") (v "0.14.0") (d (list (d (n "interoptopus") (r "^0.14.0") (d #t) (k 0)))) (h "0yx5g4qzzsd64lpaj5bcvfxywqx19xcg5j7gm1pdpqhrfmqpgw3l")))

(define-public crate-interoptopus_backend_cpython-0.14.2 (c (n "interoptopus_backend_cpython") (v "0.14.2") (d (list (d (n "interoptopus") (r "^0.14.0") (d #t) (k 0)))) (h "0x1481fjhw50z25zaqpp3902sbn6vinypc9q0a3zw0pas7d7vqkf")))

(define-public crate-interoptopus_backend_cpython-0.14.7 (c (n "interoptopus_backend_cpython") (v "0.14.7") (d (list (d (n "interoptopus") (r "^0.14.0") (d #t) (k 0)))) (h "14iijqprpah99sbg29n1kb1l91fxasxgk68k2q1dnns78w90digk")))

(define-public crate-interoptopus_backend_cpython-0.14.9 (c (n "interoptopus_backend_cpython") (v "0.14.9") (d (list (d (n "interoptopus") (r "^0.14.0") (d #t) (k 0)))) (h "0wr0qdwf5s0sm6p3rlgsvzc2a2cvvw58sbmr9byva1z8nmy7s713")))

(define-public crate-interoptopus_backend_cpython-0.14.13 (c (n "interoptopus_backend_cpython") (v "0.14.13") (d (list (d (n "interoptopus") (r "^0.14.0") (d #t) (k 0)))) (h "0a927chfsj8bclzijgzjs4v1h7r04cidd2f2xsf7v82girv9bgj6")))

(define-public crate-interoptopus_backend_cpython-0.14.14 (c (n "interoptopus_backend_cpython") (v "0.14.14") (d (list (d (n "interoptopus") (r "^0.14.0") (d #t) (k 0)))) (h "1pgz6ppfrg2zhlapfzhdjcviml2yx9h0h0k24hjxh3k8y597ixg9")))

(define-public crate-interoptopus_backend_cpython-0.14.15 (c (n "interoptopus_backend_cpython") (v "0.14.15") (d (list (d (n "interoptopus") (r "^0.14.0") (d #t) (k 0)))) (h "0alyy05qcgspkdl76yrrha1ivlzb1fh2qd3n8l920ix155dp2mlp")))

(define-public crate-interoptopus_backend_cpython-0.14.18 (c (n "interoptopus_backend_cpython") (v "0.14.18") (d (list (d (n "interoptopus") (r "^0.14.0") (d #t) (k 0)))) (h "191bifkxgvj73m8l42k43xc617c3hszn8haw1gnxxv21zggi2654")))

(define-public crate-interoptopus_backend_cpython-0.14.19 (c (n "interoptopus_backend_cpython") (v "0.14.19") (d (list (d (n "interoptopus") (r "^0.14.0") (d #t) (k 0)))) (h "1zlirjmrk14xx75l6z85xiinm8wz8hhi176ajv2aggmbny6pskf7")))

(define-public crate-interoptopus_backend_cpython-0.14.21 (c (n "interoptopus_backend_cpython") (v "0.14.21") (d (list (d (n "interoptopus") (r "^0.14.0") (d #t) (k 0)))) (h "0pap1hqkig1kszahcq1vyybyrrzj6nlrmckwfdbs2bywg22r6rz4")))

(define-public crate-interoptopus_backend_cpython-0.14.22 (c (n "interoptopus_backend_cpython") (v "0.14.22") (d (list (d (n "interoptopus") (r "^0.14.0") (d #t) (k 0)))) (h "0fmyy1261421lp33hd9la7xrbf96cj85h25dw6hzyhyv61kvj6k5")))

(define-public crate-interoptopus_backend_cpython-0.14.24 (c (n "interoptopus_backend_cpython") (v "0.14.24") (d (list (d (n "interoptopus") (r "^0.14.0") (d #t) (k 0)))) (h "0v2knj5pqfil44pg8q8gdihyyffxl1pgrj4rv46m6qgd12cq9apr")))

