(define-module (crates-io in te intel-pstate) #:use-module (crates-io))

(define-public crate-intel-pstate-0.1.0 (c (n "intel-pstate") (v "0.1.0") (h "04xdsjw2cmm5hlb8h79fl658ghwpjl2j632d7xkfcih8piivp3bq")))

(define-public crate-intel-pstate-0.1.1 (c (n "intel-pstate") (v "0.1.1") (d (list (d (n "smart-default") (r "= 0.2.0") (d #t) (k 0)))) (h "1njywdij9860624wxvzb6h00fgm7xic5n8l0jcjr5djcr6k0978p")))

(define-public crate-intel-pstate-0.2.0 (c (n "intel-pstate") (v "0.2.0") (d (list (d (n "err-derive") (r "^0.1") (d #t) (k 0)) (d (n "smart-default") (r "^0.5") (d #t) (k 0)))) (h "13k74r970ywnaw7n3fnx44ysgvmg4q683a7svwx1jk21n1h57l42")))

(define-public crate-intel-pstate-0.2.1 (c (n "intel-pstate") (v "0.2.1") (d (list (d (n "err-derive") (r "^0.2.4") (d #t) (k 0)) (d (n "smart-default") (r "^0.6.0") (d #t) (k 0)))) (h "0379mx0ibln1nfaqixgz51cy6gn64dcn9awm45drl53dia6zdrxh")))

(define-public crate-intel-pstate-0.2.2 (c (n "intel-pstate") (v "0.2.2") (d (list (d (n "smart-default") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "07y00i5camsyxrjnm69c98aibpzq7jxbdb7l6z0l8vl8yk14ig8d")))

(define-public crate-intel-pstate-0.2.3 (c (n "intel-pstate") (v "0.2.3") (d (list (d (n "smart-default") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1gmjpfxw6zl976hrda6hfw80kmgzdihc50q8l61v3v37b5ayc59k")))

(define-public crate-intel-pstate-1.0.0 (c (n "intel-pstate") (v "1.0.0") (d (list (d (n "derive_setters") (r "^0.1.5") (d #t) (k 0)) (d (n "smart-default") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0hmnax6jk1014fq4cky3y95ylqdfg7asria86gakfk68q1md49wi")))

(define-public crate-intel-pstate-1.0.1 (c (n "intel-pstate") (v "1.0.1") (d (list (d (n "derive_setters") (r "^0.1.5") (d #t) (k 0)) (d (n "smart-default") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0qymw6ir9gj9006ldk591kwn3q6c6cjisga0gq48jl6j00fv08hw")))

