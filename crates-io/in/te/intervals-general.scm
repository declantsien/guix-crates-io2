(define-module (crates-io in te intervals-general) #:use-module (crates-io))

(define-public crate-intervals-general-0.1.0 (c (n "intervals-general") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "either") (r "^1.5.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "0knjbdmq4dicqd451fq1qwsj6sfs63ka3hd7v0pk7vs3cr610qb2")))

