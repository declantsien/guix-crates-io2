(define-module (crates-io in te intern_string) #:use-module (crates-io))

(define-public crate-intern_string-0.1.0 (c (n "intern_string") (v "0.1.0") (d (list (d (n "codspeed-criterion-compat") (r "^2.3.3") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 2)))) (h "1kizcqibwp8bk49agbfnshbmd8jx5x9kxmaxyfr7895rvqf0fs5l")))

(define-public crate-intern_string-0.2.0 (c (n "intern_string") (v "0.2.0") (d (list (d (n "codspeed-criterion-compat") (r "^2.3.3") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 2)))) (h "1bhpdq9ys5iwxn1pprrkjsyqzv5dhvvcy87h3mzjs44571w14xvx")))

