(define-module (crates-io in te internationalization) #:use-module (crates-io))

(define-public crate-internationalization-0.0.0 (c (n "internationalization") (v "0.0.0") (h "1vqhwx4bzjjwavwpwis0bsnc2gahns4f0rcbxl9spw7r5syd649w")))

(define-public crate-internationalization-0.0.1 (c (n "internationalization") (v "0.0.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0qbdry092l86r67g9ff24b4601h8apkx4ak5pc69aws1slkbxz2k")))

(define-public crate-internationalization-0.0.2 (c (n "internationalization") (v "0.0.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0.2") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 1)))) (h "154smimy569rm20gf0ndm1xb1qm8bvi27xabqwz5daj8fgwpnxi2")))

(define-public crate-internationalization-0.0.3 (c (n "internationalization") (v "0.0.3") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 1)) (d (n "quote") (r "^1.0.2") (d #t) (k 1)) (d (n "regex") (r "^1.3.1") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 1)))) (h "1wcf34fzzkv5c9jq0ba877kj9xv0ci3ks7la7pb5ywliqw14svm6")))

