(define-module (crates-io in te interrupt-mutex) #:use-module (crates-io))

(define-public crate-interrupt-mutex-0.1.0 (c (n "interrupt-mutex") (v "0.1.0") (d (list (d (n "interrupts") (r "^0.1") (d #t) (k 0)) (d (n "lock_api") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("signal"))) (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (d #t) (k 2)))) (h "1h3ngx57xacc332r84pviahaamgph3fvxh756iwqbh7l0a4ahmrj")))

