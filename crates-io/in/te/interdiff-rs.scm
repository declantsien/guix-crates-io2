(define-module (crates-io in te interdiff-rs) #:use-module (crates-io))

(define-public crate-interdiff-rs-0.2.0 (c (n "interdiff-rs") (v "0.2.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "patch-rs") (r "^0.6.2") (d #t) (k 0)))) (h "0gc9f69fhsk0vpwyfd5sc27a76s8lzqgqxvbynhniis0hzidy6vr")))

