(define-module (crates-io in te interval_future) #:use-module (crates-io))

(define-public crate-interval_future-0.1.0 (c (n "interval_future") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0g64165jsd9bxr04qqg0kqis1h6q0xd6d4qiln205z2j4r0f0qw2")))

(define-public crate-interval_future-0.1.1 (c (n "interval_future") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0k8nbf2ncd6h9p8v0c0vsm7z5rbcsaji12bqck7izrkfzfmx8mpk")))

