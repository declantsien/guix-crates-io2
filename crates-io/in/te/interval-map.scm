(define-module (crates-io in te interval-map) #:use-module (crates-io))

(define-public crate-interval-map-0.1.0 (c (n "interval-map") (v "0.1.0") (h "0n1w65jgw0pfdabhsdn18rqvjg7qiwprp6mjij8i6amapkfqay4c")))

(define-public crate-interval-map-0.1.1 (c (n "interval-map") (v "0.1.1") (h "0wnndim10jchph91xg38shnvkddhpn40vs4h7ii1f8gxji6fxmf3")))

