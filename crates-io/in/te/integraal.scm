(define-module (crates-io in te integraal) #:use-module (crates-io))

(define-public crate-integraal-0.0.1 (c (n "integraal") (v "0.0.1") (d (list (d (n "rand") (r "^0.9.0-alpha.1") (f (quote ("small_rng"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.9.0-alpha.1") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rustversion") (r "^1.0.15") (d #t) (k 1)))) (h "0ic5m5dskn5mnqx0aijvyvaza5qyjigwbxy969yb03hbd76q8ypz") (s 2) (e (quote (("montecarlo" "dep:rand"))))))

(define-public crate-integraal-0.0.2 (c (n "integraal") (v "0.0.2") (d (list (d (n "rand") (r "^0.9.0-alpha.1") (f (quote ("small_rng"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.9.0-alpha.1") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rustversion") (r "^1.0.15") (d #t) (k 1)))) (h "1bhhhq3v751dg5dishpb9z4fira3r07qfnzh2lwpm00s2flipb4v") (s 2) (e (quote (("montecarlo" "dep:rand"))))))

