(define-module (crates-io in te intercom) #:use-module (crates-io))

(define-public crate-intercom-0.1.0 (c (n "intercom") (v "0.1.0") (d (list (d (n "intercom-attributes") (r "^0.1.0") (d #t) (k 0)))) (h "1qj6rx40qj3yqb0x5d6virgj52gdpgsqb4840qsi2sn856fv8nfw")))

(define-public crate-intercom-0.2.0 (c (n "intercom") (v "0.2.0") (d (list (d (n "intercom-attributes") (r "^0.2.0") (d #t) (k 0)))) (h "0kj1xv723p2g8mhd5wv6yds1hsdcqanykv823nrnb0q8ws08ixvf")))

(define-public crate-intercom-0.3.0 (c (n "intercom") (v "0.3.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "handlebars") (r "^0.29") (o #t) (d #t) (k 0)) (d (n "intercom-attributes") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1f2q3dd777qiwcnh8p3h9l4pw0drvrigwjabkbpdp8fpq89f4xnn")))

(define-public crate-intercom-0.4.0 (c (n "intercom") (v "0.4.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "handlebars") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "intercom-attributes") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "simple_logger") (r "^1.6") (k 2)))) (h "0n4svn62ggr3p4lmj895a43lp6bjc51xw7kccrcsry0z9hcdba9v")))

