(define-module (crates-io in te interrupt-bindgen) #:use-module (crates-io))

(define-public crate-interrupt-bindgen-0.1.0 (c (n "interrupt-bindgen") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_toml") (r "^0.0.1") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "11jq0cbb7yw0jdab6jsc5rf970ln5k0p9br1f1ixj0b1fjdr14gx") (y #t)))

(define-public crate-interrupt-bindgen-0.1.1 (c (n "interrupt-bindgen") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_toml") (r "^0.0.1") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "0z8r9zagccdg6y0qzk42f3idhjyrl8waakgylgqllxkkjq62d0fn")))

