(define-module (crates-io in te integral-enum) #:use-module (crates-io))

(define-public crate-integral-enum-1.0.0 (c (n "integral-enum") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0wrv9qkqljwcmxwj4v9vvd12pl1a4q57y4ww4xchxha9dwyyn9cr")))

(define-public crate-integral-enum-1.1.0 (c (n "integral-enum") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("parsing"))) (d #t) (k 0)))) (h "17avk49c4dngklr5d4rg2qz5y02cq88zd66nxy4b09sn00b4p2ps")))

(define-public crate-integral-enum-1.1.1 (c (n "integral-enum") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0gc08vk6gl11vj9hsr7f8nrsm83fdl2waa3cpj3nprzfajvikdh9")))

(define-public crate-integral-enum-1.1.2 (c (n "integral-enum") (v "1.1.2") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("parsing"))) (d #t) (k 0)))) (h "10xhhfhbv93r2nn3rhfp7cy16hhjkzm74nzc7mhzkpric1l233iv")))

(define-public crate-integral-enum-1.1.3 (c (n "integral-enum") (v "1.1.3") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1h8080dk78z61mfg3kr5y6qrizsjwj94qz1pn9r5gw6bwyc0wx0b")))

(define-public crate-integral-enum-1.1.4 (c (n "integral-enum") (v "1.1.4") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0fxyrsyi8p61p6i8dviznlpaxx93hb558qsdxwid5xd00biqd2gl")))

(define-public crate-integral-enum-1.1.5 (c (n "integral-enum") (v "1.1.5") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0xkslza1vdvb8wxh5gd52i9bwsigbah05smn1wfc2pqijw3jpbzp")))

(define-public crate-integral-enum-1.1.6 (c (n "integral-enum") (v "1.1.6") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0sdml962if3k5q8nlpd71pw1q4gps5qawlszfhpwnmk8z3v9v555")))

(define-public crate-integral-enum-1.1.7 (c (n "integral-enum") (v "1.1.7") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0l495s3m9vyz7y7rn1xy2sk92yy0iy29j6hkww15zy73b8b6z8l9")))

(define-public crate-integral-enum-1.2.0 (c (n "integral-enum") (v "1.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1vmynl28gmz89hnicz5lxk6vicjci682f3vl8pxx0l5mjmvjx47w")))

(define-public crate-integral-enum-1.2.1 (c (n "integral-enum") (v "1.2.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1qlsgs386z5gan09xwdzmz5vhzdi7f4kpm0rmp1j9sz4vv7xwib4")))

(define-public crate-integral-enum-2.0.0 (c (n "integral-enum") (v "2.0.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0g844i4j3hf65w953yvvpfc69c3am3pfiz5g5pnp4v9yhbghjgji") (y #t)))

(define-public crate-integral-enum-2.0.1 (c (n "integral-enum") (v "2.0.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "14qix689b4qn84g7g1xylk5z5ns52q18mf2dycj2p9glzn3q7rib") (y #t)))

(define-public crate-integral-enum-2.1.0 (c (n "integral-enum") (v "2.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "15zrsk44ghdq8d9zi2y72scvplcb7i0widnhcp6c9y0jm3vvj4w9") (y #t)))

(define-public crate-integral-enum-2.1.1 (c (n "integral-enum") (v "2.1.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "19ihbh8ha0fc80lxyjybkzbq3sncj0b1v5ycxs4455qk64617b5y")))

(define-public crate-integral-enum-3.0.0 (c (n "integral-enum") (v "3.0.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.54") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.11") (d #t) (k 0)))) (h "1rlh5hspb0cl9d8q6pz7l30dbwhvjcd5w47x8yicgccgy8wazj35")))

(define-public crate-integral-enum-3.0.1 (c (n "integral-enum") (v "3.0.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.54") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.11") (d #t) (k 0)))) (h "1s7jdd4pbww1i2y9253ds9jvzmpzqpxrg7q1nwm47d8855djj608")))

