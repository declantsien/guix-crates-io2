(define-module (crates-io in te interned) #:use-module (crates-io))

(define-public crate-interned-0.1.0 (c (n "interned") (v "0.1.0") (h "1kbsji3011jswlycx3q1l63yvjw443mm2zpvsx94dh0rn1kvq8kx")))

(define-public crate-interned-0.1.1 (c (n "interned") (v "0.1.1") (d (list (d (n "docify") (r "^0.2") (d #t) (k 0)) (d (n "staticize") (r "^0.1.1") (f (quote ("std"))) (d #t) (k 0)))) (h "0qdrdp5bpcnih5xijngylzgr3pch2jdwnwpzzl2kpwlrrds3wzjc") (f (quote (("generate-readme") ("default"))))))

(define-public crate-interned-0.1.2 (c (n "interned") (v "0.1.2") (d (list (d (n "docify") (r "^0.2") (d #t) (k 0)) (d (n "staticize") (r "^0.1.1") (f (quote ("std"))) (d #t) (k 0)))) (h "0qfszikgpjpbs43qji4gpjxnf6g1s5h9b0s57wcxn8mnmpnrz30l") (f (quote (("generate-readme") ("default"))))))

(define-public crate-interned-0.1.3 (c (n "interned") (v "0.1.3") (d (list (d (n "docify") (r "^0.2") (d #t) (k 0)) (d (n "staticize") (r "^0.1.1") (f (quote ("std"))) (d #t) (k 0)))) (h "0kz3cx63k9yknfjdxx5a6cg7nsvdalhyspfnbkwwydpkqzvdibvy") (f (quote (("generate-readme") ("default"))))))

(define-public crate-interned-0.1.4 (c (n "interned") (v "0.1.4") (d (list (d (n "docify") (r "^0.2") (d #t) (k 0)) (d (n "staticize") (r "^0.1.1") (f (quote ("std"))) (d #t) (k 0)))) (h "1qxajf99jmfi3g512jmaairfmz7dizw8w8pllp535wrpp805ysc2") (f (quote (("generate-readme") ("default"))))))

(define-public crate-interned-0.1.5 (c (n "interned") (v "0.1.5") (d (list (d (n "docify") (r "^0.2") (d #t) (k 0)) (d (n "staticize") (r "^0.1.1") (f (quote ("std"))) (d #t) (k 0)))) (h "0c302kbfrcbvf55bwc7wfnn73mjvgz58c37xayqfkky90wv056qq") (f (quote (("generate-readme") ("default"))))))

(define-public crate-interned-0.1.6 (c (n "interned") (v "0.1.6") (d (list (d (n "docify") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "docify") (r "^0.2") (d #t) (k 2)) (d (n "staticize") (r "^0.1.2") (f (quote ("std"))) (d #t) (k 0)))) (h "0wflyfs7aa4cb3s4asx0ravwl8xjvfpkrcrnlv71s4jw2x2sv8r8") (f (quote (("default")))) (s 2) (e (quote (("generate-readme" "dep:docify"))))))

