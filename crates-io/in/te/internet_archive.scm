(define-module (crates-io in te internet_archive) #:use-module (crates-io))

(define-public crate-internet_archive-0.1.0 (c (n "internet_archive") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (d #t) (k 0)))) (h "0f3b5vhy02r2lhrkgfypa6h6dipq01c6ixhbiq5lhd9bbq5yk0n9")))

(define-public crate-internet_archive-0.1.1 (c (n "internet_archive") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "03qwbh5p964pl50l3r4k2knla7sfb76b9rm8nypi53qad7xamnqk")))

(define-public crate-internet_archive-0.1.2 (c (n "internet_archive") (v "0.1.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "snafu") (r "^0.7.4") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0r4xdinvh5f50nj4fvzp2lyfffhh5ry727b6swydnpnfywjvbn3i")))

