(define-module (crates-io in te interaction-calculus) #:use-module (crates-io))

(define-public crate-interaction-calculus-0.2.1 (c (n "interaction-calculus") (v "0.2.1") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)))) (h "0vajsqwjclb5l4w3nb05p1y8ms2jqazm698a4l47lb5gsr1adhzi")))

(define-public crate-interaction-calculus-0.2.4 (c (n "interaction-calculus") (v "0.2.4") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (d #t) (k 0)))) (h "19zbrjkwbsdkjqwdwi6s2nlm4swk0mch0p3nvvdbr0kjxh248gl2")))

