(define-module (crates-io in te interruptor) #:use-module (crates-io))

(define-public crate-interruptor-0.1.0 (c (n "interruptor") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "simple-signal") (r "^1.1.1") (d #t) (k 0)))) (h "034v33xrj692ww67l6rscwgqh2s6gysbzm04an00rj0jjs1355n6")))

(define-public crate-interruptor-0.1.1 (c (n "interruptor") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "simple-signal") (r "^1.1.1") (d #t) (k 0)))) (h "02g703xfm1w0krl64n1zc6xf042f7aakgy6id5mlc48mxqf4chff")))

