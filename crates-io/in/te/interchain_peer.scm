(define-module (crates-io in te interchain_peer) #:use-module (crates-io))

(define-public crate-interchain_peer-0.1.0 (c (n "interchain_peer") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.3") (d #t) (k 0)))) (h "14sxj1w2v7b6xgvfmfjnzad90dlwg9345920584raiv568wgj6vz")))

