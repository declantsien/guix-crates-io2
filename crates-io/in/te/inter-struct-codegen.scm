(define-module (crates-io in te inter-struct-codegen) #:use-module (crates-io))

(define-public crate-inter-struct-codegen-0.1.0 (c (n "inter-struct-codegen") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0965y4ia5sgqah03q1wl7gwvxijxcg8ndvasa54jc8vfp852c8fr") (f (quote (("debug"))))))

(define-public crate-inter-struct-codegen-0.2.0 (c (n "inter-struct-codegen") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1dqs5zs5bg0pg8pn3ahl9vrwx4ab7cv5rjzhpp1zljn4i6y23kjq") (f (quote (("debug"))))))

(define-public crate-inter-struct-codegen-0.2.1 (c (n "inter-struct-codegen") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "09gw4k017cn38bsvy9gylvqalipy3jrs8h0i1zc1zv8mvj7ypk3i") (f (quote (("debug")))) (r "1.62")))

