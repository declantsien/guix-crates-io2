(define-module (crates-io in te interface-tests-helpers) #:use-module (crates-io))

(define-public crate-interface-tests-helpers-0.1.0 (c (n "interface-tests-helpers") (v "0.1.0") (d (list (d (n "mockito") (r "^0.9.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)))) (h "0ggqsp2fj2vlb8x7py59sbmgpp8mvcwmz0ikxglfzivl5qcc7sbi")))

(define-public crate-interface-tests-helpers-0.2.0 (c (n "interface-tests-helpers") (v "0.2.0") (d (list (d (n "mockito") (r "^0.17.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)))) (h "0mq94pl3aiskpp7b81w743y5z989mndhpr5s0dclhgapdb8j6zvc")))

