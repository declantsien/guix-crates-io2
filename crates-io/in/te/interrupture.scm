(define-module (crates-io in te interrupture) #:use-module (crates-io))

(define-public crate-interrupture-0.1.0 (c (n "interrupture") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.3") (d #t) (k 0)))) (h "0i03yfx5bpc78svaqb1w19k04kkq2driqd519510kpxi5qmxykj6")))

(define-public crate-interrupture-0.1.1 (c (n "interrupture") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.3") (d #t) (k 0)))) (h "0cvhhz9scm0g7rlh718vcyiknvklf767nnrdzjpha7p8i75w0wad")))

