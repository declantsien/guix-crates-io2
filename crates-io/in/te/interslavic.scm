(define-module (crates-io in te interslavic) #:use-module (crates-io))

(define-public crate-interslavic-0.1.0 (c (n "interslavic") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 2)) (d (n "bevy") (r "^0.13.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "ratatui") (r "^0.26.1") (k 0)))) (h "1g8jz2qsspg0bsqja7dxgwpfgg874px4iy59al5hgijhl5m2ppcs")))

(define-public crate-interslavic-0.1.1 (c (n "interslavic") (v "0.1.1") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.197") (d #t) (k 0)))) (h "19fqwm4hkicm2fqqkjxjrhraazl54gkycj39wfp37nn7jscf5v3s")))

(define-public crate-interslavic-0.1.2 (c (n "interslavic") (v "0.1.2") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.197") (d #t) (k 0)))) (h "1w9klcay38adhbcp1zydjr9j2h4lrwyw7hw9rfh25987kk20cnvi")))

(define-public crate-interslavic-0.1.3 (c (n "interslavic") (v "0.1.3") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.197") (d #t) (k 0)))) (h "15nq3rxv9xi834dqqb5prws5p9fzz2v4ryn0pi0bmw3ksw61aqr5")))

(define-public crate-interslavic-0.1.4 (c (n "interslavic") (v "0.1.4") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.197") (d #t) (k 0)))) (h "0wk0gpx8bjbnwx8lry57v0y65pixmf6wihm5giywl9vn42m4gr3z")))

