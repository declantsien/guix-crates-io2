(define-module (crates-io in te intertrait) #:use-module (crates-io))

(define-public crate-intertrait-0.1.0 (c (n "intertrait") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "intertrait-macros") (r "= 0.1.0") (d #t) (k 0)) (d (n "linkme") (r "^0.1.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.23") (d #t) (k 2)))) (h "1kqpqj03a7rfyy3h31sc8p6vwc8b4bzyzfli6ip9acml099fvmws")))

(define-public crate-intertrait-0.1.1 (c (n "intertrait") (v "0.1.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "intertrait-macros") (r "= 0.1.1") (d #t) (k 0)) (d (n "linkme") (r "^0.2.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.25") (d #t) (k 2)))) (h "08i0yd6gjimjy3c4rm15y00fa7jzkaj3bdzczlhxp25nrwylcd40")))

(define-public crate-intertrait-0.2.0 (c (n "intertrait") (v "0.2.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "intertrait-macros") (r "= 0.2.0") (d #t) (k 0)) (d (n "linkme") (r "^0.2.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.3.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.25") (d #t) (k 2)))) (h "1lly7gq2wbsd92aiqz79rf2kz5r56wgwl3x5146a4b34g61mgn9h")))

(define-public crate-intertrait-0.2.1 (c (n "intertrait") (v "0.2.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "intertrait-macros") (r "=0.2.1") (d #t) (k 0)) (d (n "linkme") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0hrvysc99isxlc11jck3fkkxn03rrdrg9n2650zbcf53f4s63pzq")))

(define-public crate-intertrait-0.2.2 (c (n "intertrait") (v "0.2.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "intertrait-macros") (r "=0.2.2") (d #t) (k 0)) (d (n "linkme") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "00as93pkwmx3h7a3x0d5kmdzbmv8ngqnwmcykpszr3c7gppwc3zh")))

