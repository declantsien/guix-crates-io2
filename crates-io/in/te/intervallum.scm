(define-module (crates-io in te intervallum) #:use-module (crates-io))

(define-public crate-intervallum-0.1.0 (c (n "intervallum") (v "0.1.0") (h "09r40y0j6qsgw505vglvzrzn1s0kahq4scjjgck7z5dmqa3grggb")))

(define-public crate-intervallum-0.2.0 (c (n "intervallum") (v "0.2.0") (d (list (d (n "num") (r "^0.1.22") (d #t) (k 0)))) (h "1bbz8v3b9p2qp75jhkkdawa43lksf4nzqrk49kgck1miy5app032")))

(define-public crate-intervallum-0.2.1 (c (n "intervallum") (v "0.2.1") (d (list (d (n "num") (r "^0.1.22") (d #t) (k 0)))) (h "1hazd45ry47jqamsarqldrg7km2hrlrlcmcp1mrsllsff496lxnk")))

(define-public crate-intervallum-0.2.2 (c (n "intervallum") (v "0.2.2") (d (list (d (n "num") (r "^0.1.24") (d #t) (k 0)))) (h "0s048cb5jx6xi03nsr335wcjdf8azj85xgzdp6rb662vn952zrcb")))

(define-public crate-intervallum-0.3.0 (c (n "intervallum") (v "0.3.0") (d (list (d (n "num") (r "^0.1.24") (d #t) (k 0)))) (h "0vh7byy911786cc7laz7v8gwjn8139hzd0a66jd0lkp7dfds1vfr")))

(define-public crate-intervallum-0.4.0 (c (n "intervallum") (v "0.4.0") (d (list (d (n "num") (r "^0.1.24") (d #t) (k 0)))) (h "12n7hvcs2724dqnhgmk9sbhf1y6rl51789wafn4z9ns46lgiwmsn")))

(define-public crate-intervallum-0.4.1 (c (n "intervallum") (v "0.4.1") (d (list (d (n "num") (r "^0.1.24") (d #t) (k 0)))) (h "0bran6jra468rsnr085h55bcrg00384w3kcgkmsdrahpd25x74zc")))

(define-public crate-intervallum-0.4.2 (c (n "intervallum") (v "0.4.2") (d (list (d (n "num") (r "^0.1.24") (d #t) (k 0)))) (h "1kqiwlv9b391mc1wwi6936z6qy88ijmgw4gnifd1ma5al0c15lbv")))

(define-public crate-intervallum-0.4.3 (c (n "intervallum") (v "0.4.3") (d (list (d (n "num") (r "^0.1.24") (d #t) (k 0)))) (h "12j8b178p3hgkhivxcdw0n5gwgqhxr972b4hprz3aqv32g9zpwj8")))

(define-public crate-intervallum-0.4.4 (c (n "intervallum") (v "0.4.4") (d (list (d (n "num") (r "^0.1.24") (d #t) (k 0)))) (h "0y50sgpqlh4l84jf0n7p99vbss2qysmb9h3w0xydjq8d2hkqjwga")))

(define-public crate-intervallum-0.4.5 (c (n "intervallum") (v "0.4.5") (d (list (d (n "num") (r "^0.1.24") (d #t) (k 0)))) (h "171k7lm3z6bn94fb4kgv1748f12diw29i81zaav0gi9gll103bjx")))

(define-public crate-intervallum-0.4.6 (c (n "intervallum") (v "0.4.6") (d (list (d (n "num") (r "^0.1.24") (d #t) (k 0)))) (h "1df83lcp4jdsps3wkq3r8hrskr65h6fywmh6i587favwjj091j45")))

(define-public crate-intervallum-0.4.7 (c (n "intervallum") (v "0.4.7") (d (list (d (n "num") (r "^0.1.24") (d #t) (k 0)))) (h "1hr4y6pss9cy9ms0yj2wh8iwkqzcddpy60kxlgw8m0sxqsbs36mj")))

(define-public crate-intervallum-0.4.8 (c (n "intervallum") (v "0.4.8") (d (list (d (n "num") (r "^0.1.24") (d #t) (k 0)))) (h "0k887hf5snksgg4af4awfrbw0pkfv2rc3z1pcr73gb3ax008n4wp")))

(define-public crate-intervallum-0.4.9 (c (n "intervallum") (v "0.4.9") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "1w3zyv8sf0ffrg1z5fbycjsvxxj5g5zxziga6acrfm65d8lrbaw8")))

(define-public crate-intervallum-0.4.10 (c (n "intervallum") (v "0.4.10") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)))) (h "16am4k90x9vya87bzvr74acvqijccpz557njmz8701k7gfbqkchc")))

(define-public crate-intervallum-0.4.11 (c (n "intervallum") (v "0.4.11") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)))) (h "0gr8r5j5shhm6fl8vzhkpiicl465bp3gqp92v8rc8wmmd6yg188j")))

(define-public crate-intervallum-0.4.12 (c (n "intervallum") (v "0.4.12") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)))) (h "126mdi2a2hand3vldydqxvqj35vfsig254wkn9118ball4xkjh26")))

(define-public crate-intervallum-0.5.1 (c (n "intervallum") (v "0.5.1") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "gcollections") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)))) (h "0lf1bls0abd9klnbyyxkgrgqrpcbn87zdd3kiaf5kwljldsh0las")))

(define-public crate-intervallum-0.5.3 (c (n "intervallum") (v "0.5.3") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "gcollections") (r "^0.2.1") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)))) (h "1h66bgzjmn7jnk4yfrza7jqx6xwz1birlba476b4ypqc73w2d4rn")))

(define-public crate-intervallum-0.5.4 (c (n "intervallum") (v "0.5.4") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "gcollections") (r "^0.2.1") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)))) (h "1ndadjlyzxvgqg63x29f72y52dnhv7la8i94kgzpxyvx6295ypgb")))

(define-public crate-intervallum-0.5.5 (c (n "intervallum") (v "0.5.5") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "gcollections") (r "^0.2.2") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)))) (h "1h361b7b6kyvl8qxzcxrpwjqldng57bfcmqxn4g6gh13d5vlc8q2")))

(define-public crate-intervallum-0.6.0 (c (n "intervallum") (v "0.6.0") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "gcollections") (r "^0.3.0") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)))) (h "1kwr4ndz8ixbqdy4y5j0c4hz84458iy6cc9kwyyv01dsr51pc17m")))

(define-public crate-intervallum-0.6.1 (c (n "intervallum") (v "0.6.1") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "gcollections") (r "^0.3.0") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)))) (h "0285ajcjsmrmf8jrmh483jg9sa7baiqgp35hhhynd4iisbn3h4d8")))

(define-public crate-intervallum-0.6.2 (c (n "intervallum") (v "0.6.2") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "gcollections") (r "^0.3.0") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)))) (h "1cv1d9mwx2p6q8pqzwa0imxaciiiprx8mm5fw1vc9hgvpyl5bl5r")))

(define-public crate-intervallum-0.6.3 (c (n "intervallum") (v "0.6.3") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "gcollections") (r "^0.3.0") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)))) (h "049s7mqbxhkcj6i08chid316k3mql3m04glxcfm8hsvbr35k9rj5")))

(define-public crate-intervallum-0.6.4 (c (n "intervallum") (v "0.6.4") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "gcollections") (r "^0.3.3") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)))) (h "0660r09j1lxvafac46w3flax2wkfgw0fs2j6w5gdayv7hmqb9mja")))

(define-public crate-intervallum-0.6.5 (c (n "intervallum") (v "0.6.5") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "gcollections") (r "^0.3.5") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)))) (h "01p3j30jspiw3qdywi2d9cld2swszf8dkb4hs5w10f53bjcr80rm")))

(define-public crate-intervallum-1.0.0 (c (n "intervallum") (v "1.0.0") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "gcollections") (r "^1.0.0") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)))) (h "0hcv2iy1c4iqyiw9vncz1721kjl9rcbnkr05mmd9m6l931lc07k2") (f (quote (("nightly"))))))

(define-public crate-intervallum-1.1.0 (c (n "intervallum") (v "1.1.0") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "gcollections") (r "^1.3.0") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)) (d (n "trilean") (r "^1.0.1") (d #t) (k 0)))) (h "0bwanp7szjn16l33k8rx99y4d28fyan3pkkf4k0l3c3mmsc7bnsp")))

(define-public crate-intervallum-1.2.0 (c (n "intervallum") (v "1.2.0") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "gcollections") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)) (d (n "trilean") (r "^1.0.1") (d #t) (k 0)))) (h "10n70hdyj6gkk0qr8f9vx5snb82ippn9f9y1p5wlq7s5basz8c0k")))

(define-public crate-intervallum-1.3.0 (c (n "intervallum") (v "1.3.0") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "gcollections") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)) (d (n "trilean") (r "^1.0.1") (d #t) (k 0)))) (h "1080idp98p5r1ninwcm48acil5af284alqigpjq1lp8chm18j08w")))

(define-public crate-intervallum-1.4.0 (c (n "intervallum") (v "1.4.0") (d (list (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)) (d (n "gcollections") (r "^1.5.0") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "trilean") (r "^1.1.0") (d #t) (k 0)))) (h "1qffh9db7d6f1byjh6m9pqdcjcifqdghvzy3rrg6jvv66kcfrk68")))

(define-public crate-intervallum-1.4.1 (c (n "intervallum") (v "1.4.1") (d (list (d (n "bit-set") (r "^0.5.0") (d #t) (k 0)) (d (n "gcollections") (r "^1.5.0") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "trilean") (r "^1.1.0") (d #t) (k 0)))) (h "0ng6avk6dpmax1h2ssxq2c7i5j6hhzpxai00z53sc2lkscjdmgqq")))

