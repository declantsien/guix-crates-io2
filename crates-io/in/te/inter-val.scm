(define-module (crates-io in te inter-val) #:use-module (crates-io))

(define-public crate-inter-val-0.1.0 (c (n "inter-val") (v "0.1.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.53") (d #t) (k 0)))) (h "08i7g4cd3nxvgb99h72bz4kb3irgq9c8lvb98l9fn2759585cqdr")))

(define-public crate-inter-val-0.1.1 (c (n "inter-val") (v "0.1.1") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.53") (d #t) (k 0)))) (h "1bmrwg0dr2c6ycsp1ldnad5zrw2pd1238wp9gacpjpplyz4pn3s8")))

(define-public crate-inter-val-0.1.2 (c (n "inter-val") (v "0.1.2") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.53") (d #t) (k 0)))) (h "0vlk3csscq5drpbpk6p9zs3r8d7r623dpbvbw59d9m7ddsqb38lz")))

