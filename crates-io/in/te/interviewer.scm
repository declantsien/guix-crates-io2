(define-module (crates-io in te interviewer) #:use-module (crates-io))

(define-public crate-interviewer-0.1.0 (c (n "interviewer") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.6") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)))) (h "0j8d8pqcrk4yys3hlpc7if48gcp06lkpcnbiv55b4p84r5v7f208")))

(define-public crate-interviewer-0.1.1 (c (n "interviewer") (v "0.1.1") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigfloat") (r "^1.3.1") (o #t) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "num-complex") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "num-rational") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)))) (h "18b7ih78dp78nw6ck0d74b8dwkwh1c7a6mv2lvxzcgzfn084w61g") (f (quote (("num" "num-bigint" "num-bigfloat" "num-complex" "num-rational") ("nightly")))) (s 2) (e (quote (("num-rational" "dep:num-rational") ("num-complex" "dep:num-complex") ("num-bigint" "dep:num-bigint") ("num-bigfloat" "dep:num-bigfloat"))))))

