(define-module (crates-io in te interact) #:use-module (crates-io))

(define-public crate-interact-0.1.0 (c (n "interact") (v "0.1.0") (d (list (d (n "interact_derive") (r "^0.1") (d #t) (k 0)) (d (n "pest") (r "^2.0.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "ron") (r "^0.4") (d #t) (k 0)))) (h "1lc3mkzbzchjil5fzw84vi2xyl40l8xxky00y3g79szclr0pj60a")))

(define-public crate-interact-0.2.0 (c (n "interact") (v "0.2.0") (d (list (d (n "interact_derive") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.0.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "ron") (r "^0.4") (d #t) (k 0)))) (h "1g773bnj5ygakgq6amlkgcfnnbllnv4l5zvymkxf42274l7ap2qg")))

(define-public crate-interact-0.3.0 (c (n "interact") (v "0.3.0") (d (list (d (n "interact_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "pest") (r "^2.0.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "ron") (r "^0.4") (d #t) (k 0)))) (h "076d9ig2gw8lh3aabap2i61al5lipaxssw1rlwnyvxacap6hw56c")))

(define-public crate-interact-0.3.1 (c (n "interact") (v "0.3.1") (d (list (d (n "interact_derive") (r "^0.3.1") (d #t) (k 0)) (d (n "pest") (r "^2.0.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "= 0.5") (d #t) (k 2)) (d (n "ron") (r "^0.4") (d #t) (k 0)))) (h "0kd0p1dcgcc9n1xlkcqafwiycx2qjcgraarbi2lv7az4gzk2ff2f")))

(define-public crate-interact-0.3.2 (c (n "interact") (v "0.3.2") (d (list (d (n "interact_derive") (r "^0.3.2") (d #t) (k 0)) (d (n "pest") (r "^2.0.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "= 0.5") (d #t) (k 2)) (d (n "ron") (r "^0.4") (d #t) (k 0)))) (h "09a208hlpykrrzjbzxgy492g4s50jkv5f3gpghchnhfw4npw3q10")))

(define-public crate-interact-0.3.3 (c (n "interact") (v "0.3.3") (d (list (d (n "interact_derive") (r "^0.3.3") (d #t) (k 0)) (d (n "pest") (r "^2.0.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "= 0.5") (d #t) (k 2)) (d (n "ron") (r "^0.4") (d #t) (k 0)))) (h "0w7cip86fibfpv3w1rb34dc06ca7kn2f2dmrfikd2j75scyss5ci")))

(define-public crate-interact-0.3.4 (c (n "interact") (v "0.3.4") (d (list (d (n "interact_derive") (r "^0.3.4") (d #t) (k 0)) (d (n "pest") (r "^2.0.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "= 0.5") (d #t) (k 2)) (d (n "ron") (r "^0.4") (d #t) (k 0)))) (h "1sl5c8xn8krkrywl3idgj5fs3f3csb30f2cp0x8jlhcfav1h11db")))

(define-public crate-interact-0.3.5 (c (n "interact") (v "0.3.5") (d (list (d (n "interact_derive") (r "^0.3.5") (d #t) (k 0)) (d (n "pest") (r "^2.0.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "= 0.5") (d #t) (k 2)) (d (n "ron") (r "^0.4") (d #t) (k 0)))) (h "1652y8kg3h795r6pqx2faalh42qazzs6glaxysbp9wfpm5z5fhn5")))

(define-public crate-interact-0.3.6 (c (n "interact") (v "0.3.6") (d (list (d (n "interact_derive") (r "^0.3.6") (d #t) (k 0)) (d (n "pest") (r "^2.0.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "= 0.5") (d #t) (k 2)) (d (n "ron") (r "^0.4") (d #t) (k 0)))) (h "04chp40kgj6l36p70xr3h1jxybrbnyc4vxgmvin164mnhn5bgcd8")))

