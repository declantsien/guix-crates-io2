(define-module (crates-io in te interval-heap) #:use-module (crates-io))

(define-public crate-interval-heap-0.0.1 (c (n "interval-heap") (v "0.0.1") (d (list (d (n "compare") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1gnrfgkhxy172ccm0krmi7j81cdg373ga4y40s0r0h9f2171fl07")))

(define-public crate-interval-heap-0.0.2 (c (n "interval-heap") (v "0.0.2") (d (list (d (n "compare") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "0g5p8wfza97a9wjnq9hwlvc0f4a2migc6svpm0bxv74krb8836aa")))

(define-public crate-interval-heap-0.0.3 (c (n "interval-heap") (v "0.0.3") (d (list (d (n "compare") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "02wl6v8wy2krxpdxsmsb7awzqhxczbczyfjp3sydpyxaf6ldfin8")))

(define-public crate-interval-heap-0.0.4 (c (n "interval-heap") (v "0.0.4") (d (list (d (n "compare") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1h567d7s3p0ar3k7lffxkg59n1fzisi4jqdmyx57ivg2gdailyn6")))

(define-public crate-interval-heap-0.0.5 (c (n "interval-heap") (v "0.0.5") (d (list (d (n "compare") (r "^0.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1ikap3f0lkb0qx8r20caniwqg6bfcav10afwzry61f49irg4w9qi") (f (quote (("drain"))))))

