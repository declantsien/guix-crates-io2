(define-module (crates-io in te integra8_tree_formatter) #:use-module (crates-io))

(define-public crate-integra8_tree_formatter-0.0.1-alpha (c (n "integra8_tree_formatter") (v "0.0.1-alpha") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "integra8_formatters") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "10k3l9f3m22k4qvddwlpq13h7rdpw82z49a1gzpm6a3zmcm37dki")))

(define-public crate-integra8_tree_formatter-0.0.2-alpha (c (n "integra8_tree_formatter") (v "0.0.2-alpha") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0-rc.2") (d #t) (k 0)) (d (n "integra8_formatters") (r "^0.0.2-alpha") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "18njmcghhl98l1ippra5xq28qgr8mk9x2ghhagbp4p2aa0rvshqp")))

(define-public crate-integra8_tree_formatter-0.0.3-alpha (c (n "integra8_tree_formatter") (v "0.0.3-alpha") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0-rc.2") (d #t) (k 0)) (d (n "integra8_formatters") (r "^0.0.3-alpha") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "005i901dp7biii7sa6vx8h4vlyicszmyjy7jvpfl31p0qv7qfgfl")))

(define-public crate-integra8_tree_formatter-0.0.4-alpha (c (n "integra8_tree_formatter") (v "0.0.4-alpha") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0-rc.2") (d #t) (k 0)) (d (n "integra8_formatters") (r "^0.0.4-alpha") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1iiavh5sz6fvrn6kkrs2p6bxbp7bygp7ms7wy46k72anrf1s7vwi")))

(define-public crate-integra8_tree_formatter-0.0.5-rc1 (c (n "integra8_tree_formatter") (v "0.0.5-rc1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "indicatif") (r "=0.17.0-rc.2") (d #t) (k 0)) (d (n "integra8") (r "^0.0.5-rc1") (f (quote ("formatters"))) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "07s2x41swnaiqqk4dhgwix2f59hdzwcqg1ka0q9ydnn7pvfgwqw0")))

