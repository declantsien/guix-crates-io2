(define-module (crates-io in te integer-sqrt) #:use-module (crates-io))

(define-public crate-integer-sqrt-0.1.0 (c (n "integer-sqrt") (v "0.1.0") (h "0845zsl9w0vsm1wvh8gymma59idrdaff4i5jjch514ji64n70cw8")))

(define-public crate-integer-sqrt-0.1.1 (c (n "integer-sqrt") (v "0.1.1") (h "0mvj0jnxk3mxvawdx5s5x0qsv80c1rpz91wxc0pvivdbg1zzwdkm")))

(define-public crate-integer-sqrt-0.1.2 (c (n "integer-sqrt") (v "0.1.2") (h "00y9n0jx9kzr4zj7swnfv6g9b0py0n69h60lbykq5wx67fxml5ga")))

(define-public crate-integer-sqrt-0.1.3 (c (n "integer-sqrt") (v "0.1.3") (h "12q8cvkvcl4whs87wfb3n8hfw74lfxi09cah6d3pm2a4gnzpfn7n")))

(define-public crate-integer-sqrt-0.1.4 (c (n "integer-sqrt") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1ah25kkyz7d6cwphci3jzr5w46gr6zphc40nwrqgsxx47ppkhc0x") (y #t)))

(define-public crate-integer-sqrt-0.1.5 (c (n "integer-sqrt") (v "0.1.5") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0w6pzmgvs1mldkhafbwg9x7wzr0af3ngkimyb1gy97jarcdw6vi7")))

