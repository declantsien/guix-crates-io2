(define-module (crates-io in te interfacer-http-util) #:use-module (crates-io))

(define-public crate-interfacer-http-util-0.1.0 (c (n "interfacer-http-util") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.2") (d #t) (k 0)) (d (n "const-concat") (r "^0.1.0") (d #t) (k 0)) (d (n "cookie") (r "^0.12") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.17") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "1ayhzn79h9vb7xjpi7x73ajrxw976r9a579nymwh9bg68mcm9x8z")))

(define-public crate-interfacer-http-util-0.1.1 (c (n "interfacer-http-util") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.2") (d #t) (k 0)) (d (n "const-concat") (r "^0.1.0") (d #t) (k 0)) (d (n "cookie") (r "^0.12") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "1an3gilqji2vvxybamqbc2waxgpkmy3kbcb38pplmkmnqixzwzdx")))

(define-public crate-interfacer-http-util-0.1.2 (c (n "interfacer-http-util") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "const-concat") (r "^0.1.0") (d #t) (k 0)) (d (n "cookie") (r "^0.12") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "1xajv6904zdf40sm1n8giz4yrj7qvs4ybxfzqf88a5b3cd5dd1ii")))

(define-public crate-interfacer-http-util-0.1.3 (c (n "interfacer-http-util") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "const-concat") (r "^0.1.0") (d #t) (k 0)) (d (n "cookie") (r "^0.12") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0cj0imi74lxhk6i2yk5r6cyw7xha6s0y2hxwxyvsq6f6db23prdz")))

(define-public crate-interfacer-http-util-0.1.4 (c (n "interfacer-http-util") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "const-concat") (r "^0.1") (d #t) (k 0)) (d (n "cookie") (r "^0.12") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "02ak0rkdm0xnxd2x0mppak9arn8f5p7ywz59a8iyaipaarbx7p2d")))

(define-public crate-interfacer-http-util-0.1.5 (c (n "interfacer-http-util") (v "0.1.5") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "cookie") (r "^0.12") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0j8jdd0b5zv957b9ysh25ywfhsg259r84c5khg1hlg8iq1aqsc5i")))

