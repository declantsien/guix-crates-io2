(define-module (crates-io in te intel-tee-quote-verification-rs) #:use-module (crates-io))

(define-public crate-intel-tee-quote-verification-rs-0.2.1 (c (n "intel-tee-quote-verification-rs") (v "0.2.1") (d (list (d (n "intel-tee-quote-verification-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0hk2zw6pxfza87ljkcyqghizmxp9flh0zg0nlywwwga2mwk9shs3")))

(define-public crate-intel-tee-quote-verification-rs-0.3.0 (c (n "intel-tee-quote-verification-rs") (v "0.3.0") (d (list (d (n "intel-tee-quote-verification-sys") (r "^0.2.1") (d #t) (k 0)))) (h "0bdiw30afwf7np7rj104fq8gz8hv4f3rdjld35kvv332c73jk2za")))

