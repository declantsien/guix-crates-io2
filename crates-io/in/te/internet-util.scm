(define-module (crates-io in te internet-util) #:use-module (crates-io))

(define-public crate-internet-util-0.1.0 (c (n "internet-util") (v "0.1.0") (d (list (d (n "encoding_rs") (r "^0.8.33") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.18.1") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 0)))) (h "15mxbrvx52vfzqdmllj66dkqbzjgr044v6zc0srpsahl9yzq6n1p")))

