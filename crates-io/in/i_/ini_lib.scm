(define-module (crates-io in i_ ini_lib) #:use-module (crates-io))

(define-public crate-ini_lib-0.1.0 (c (n "ini_lib") (v "0.1.0") (h "056cna3s0cy7wi81a8cwzzdf4l2j3ik17dqcd5wlanx431mjpyqa") (y #t)))

(define-public crate-ini_lib-0.1.1 (c (n "ini_lib") (v "0.1.1") (h "19dabpjinkjvf0jlr2d3h29m13v468hxfpahwa76p66hzff1lxa2") (y #t)))

(define-public crate-ini_lib-0.1.2 (c (n "ini_lib") (v "0.1.2") (h "05l3s706v8k8xp9bn4fgp7kcxlh761qm3r7f9986j7nsambhi9id") (y #t)))

(define-public crate-ini_lib-0.1.3 (c (n "ini_lib") (v "0.1.3") (h "0k1xbmkz6x02qx6690k38l2jdgbxbmgay7fn1fyp28i4azjd96db")))

(define-public crate-ini_lib-0.1.4 (c (n "ini_lib") (v "0.1.4") (h "070sw8hlq5qnyx84xh1cfjac3i703xqrxwi5211izxzrvrnwi1zp")))

