(define-module (crates-io in i_ ini_core) #:use-module (crates-io))

(define-public crate-ini_core-0.1.0 (c (n "ini_core") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "15bb9q71cvycg9hwldsyd227nk84bpjgkccjm4kqkdfr17k47ik2")))

(define-public crate-ini_core-0.2.0 (c (n "ini_core") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0q9sqxz6bjdml84mlgbh4izzbgrp8l41g7595wkbafglm4qpliks")))

