(define-module (crates-io in pl inplace-vec-builder) #:use-module (crates-io))

(define-public crate-inplace-vec-builder-0.1.0 (c (n "inplace-vec-builder") (v "0.1.0") (d (list (d (n "smallvec") (r "^1.7.0") (o #t) (d #t) (k 0)) (d (n "testdrop") (r "^0.1.2") (d #t) (k 2)))) (h "0v26rix6aizc01a3skfaabd0x3hwfwq1m9n9gl9dvpp9yp6j72bv") (f (quote (("stdvec") ("default" "stdvec"))))))

(define-public crate-inplace-vec-builder-0.1.1 (c (n "inplace-vec-builder") (v "0.1.1") (d (list (d (n "smallvec") (r "^1.7.0") (o #t) (d #t) (k 0)) (d (n "testdrop") (r "^0.1.2") (d #t) (k 2)))) (h "01ykl6cis06m2hw88413jhmjs4qvhsi8fx8j3ykr2s12r3nw4r6g") (f (quote (("stdvec") ("default" "stdvec"))))))

