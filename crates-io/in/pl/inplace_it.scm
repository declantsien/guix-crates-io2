(define-module (crates-io in pl inplace_it) #:use-module (crates-io))

(define-public crate-inplace_it-0.1.0 (c (n "inplace_it") (v "0.1.0") (h "095kyqg3r0a0qrf6z52fh90xgbpdqrk0z3vwz6hpnqbm56r9bci1")))

(define-public crate-inplace_it-0.2.0 (c (n "inplace_it") (v "0.2.0") (h "1k2npdmvjwxhfpcc9xb2z23d42vp5glqfyg4vbif3l5ba9nbc9pw")))

(define-public crate-inplace_it-0.2.1 (c (n "inplace_it") (v "0.2.1") (h "0kpg1jv6f4z9n6vlljymb09ppl5y9layra5d46cnwcpc97vfq16g")))

(define-public crate-inplace_it-0.2.2 (c (n "inplace_it") (v "0.2.2") (h "17maz4f0nijlabfnnkgvgawjqprxmmvw0a5rf8lps7x53r8yk37n")))

(define-public crate-inplace_it-0.3.0 (c (n "inplace_it") (v "0.3.0") (h "02qjvg8wrwfvdnxz5csrciapfbawhvxb2hg03k0h5bd5gh7zvpb7")))

(define-public crate-inplace_it-0.3.1 (c (n "inplace_it") (v "0.3.1") (h "1pqgag9z78d30j3p0gxym6mlwqiycg4bgx0zjcwa1pgpxnx306jr")))

(define-public crate-inplace_it-0.3.2 (c (n "inplace_it") (v "0.3.2") (h "0d5mbkff2ziwmqqpaki84ridgx3yd3m8ip12dgwrsf9g7yks40fx")))

(define-public crate-inplace_it-0.3.3 (c (n "inplace_it") (v "0.3.3") (h "1jpppakqdazlzmlqx8vmb78drzgv93w52gk4lii6vzkri8q3z5ch")))

(define-public crate-inplace_it-0.3.4 (c (n "inplace_it") (v "0.3.4") (h "02q7jav7xbhvcdq0nc8is9b4ajqw5ggavzby3qn3dxpk6rw39w37")))

(define-public crate-inplace_it-0.3.5 (c (n "inplace_it") (v "0.3.5") (h "0y5znpw0f42pf2q0ksdli299p7qkh2rhjbkhjxrvrm7ka264crz5")))

