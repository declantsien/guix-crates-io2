(define-module (crates-io in pl inplace) #:use-module (crates-io))

(define-public crate-inplace-0.0.1 (c (n "inplace") (v "0.0.1") (d (list (d (n "arraystring") (r "^0.3.0") (d #t) (k 2)) (d (n "typenum") (r "^1.11.2") (d #t) (k 2)))) (h "0yjfkybcp714fl4rw7jr60a7vjz6qnqbcfm49dbwk3fziqii842x") (y #t)))

(define-public crate-inplace-0.0.2 (c (n "inplace") (v "0.0.2") (d (list (d (n "arraystring") (r "^0.3.0") (d #t) (k 2)) (d (n "typenum") (r "^1.11.2") (d #t) (k 2)))) (h "0aipw5nhlkzkv2kvk4r84pl089q2fdq0rjpfd5xskv3psnmdcxhy") (y #t)))

(define-public crate-inplace-0.0.3 (c (n "inplace") (v "0.0.3") (d (list (d (n "arraystring") (r "^0.3.0") (d #t) (k 2)) (d (n "typenum") (r "^1.11.2") (d #t) (k 2)))) (h "0kmcqfwsiwpjp68kj3n3dgnvr2496b7wclkyvjgcpbqaijdrdmax") (y #t)))

(define-public crate-inplace-0.0.4 (c (n "inplace") (v "0.0.4") (d (list (d (n "arraystring") (r "^0.3.0") (d #t) (k 2)) (d (n "typenum") (r "^1.11.2") (d #t) (k 2)))) (h "0sar1py5pz89lmhwpfzipv6ms49x1b2k85wzlb95bs4s2cj2wlxk") (y #t)))

(define-public crate-inplace-0.0.5 (c (n "inplace") (v "0.0.5") (d (list (d (n "arraystring") (r "^0.3.0") (d #t) (k 2)) (d (n "typenum") (r "^1.11.2") (d #t) (k 2)))) (h "1vqzqavb0bm5qkxv2z8di3jzd59m4ijgsbi8x9r5pn5y3ipyd0sm") (y #t)))

