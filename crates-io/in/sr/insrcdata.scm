(define-module (crates-io in sr insrcdata) #:use-module (crates-io))

(define-public crate-insrcdata-0.1.0 (c (n "insrcdata") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.145") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0z36qd0hz6bj9vz0xh1r5l80fxd907bn040r25i4prbxzv29fq04")))

(define-public crate-insrcdata-0.2.0 (c (n "insrcdata") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.145") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0lz56ghq6aqylkyvw72damhx0l1a93mm8v64jwm6i20ghv0f445q")))

