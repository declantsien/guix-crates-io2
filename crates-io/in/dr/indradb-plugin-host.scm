(define-module (crates-io in dr indradb-plugin-host) #:use-module (crates-io))

(define-public crate-indradb-plugin-host-0.1.0 (c (n "indradb-plugin-host") (v "0.1.0") (d (list (d (n "indradb-lib") (r "^3.0.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "uuid") (r "~0.8.2") (d #t) (k 0)))) (h "0f16n9gvnzavadm7qsgfk4zyr9zpwrl8mgi2r3yzf83yjd9jaach")))

(define-public crate-indradb-plugin-host-0.2.0 (c (n "indradb-plugin-host") (v "0.2.0") (d (list (d (n "indradb-lib") (r "^3.0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "uuid") (r "~0.8.2") (d #t) (k 0)))) (h "1ccq0w9ysaly1s9bkzwwp34xyxhqj7z4qv3ib5zwvch8j9wily63")))

(define-public crate-indradb-plugin-host-0.2.1 (c (n "indradb-plugin-host") (v "0.2.1") (d (list (d (n "indradb-lib") (r "^3.0.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "uuid") (r "~0.8.2") (d #t) (k 0)))) (h "18w489wdz5062dxfr30bmbwhadf4ybvl253zhs9np8qx0bc2g49z")))

(define-public crate-indradb-plugin-host-0.2.2 (c (n "indradb-plugin-host") (v "0.2.2") (d (list (d (n "indradb-lib") (r "^3.0.3") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (d #t) (k 0)))) (h "17h6a4c5a3ji4m01l1d3znp849nl3p8nvdfi8969bd08b3wm8vn0")))

(define-public crate-indradb-plugin-host-0.2.3 (c (n "indradb-plugin-host") (v "0.2.3") (d (list (d (n "indradb-lib") (r "^3.0.4") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (d #t) (k 0)))) (h "133qjfa0cab4b04hh49cc8893jlmbsybw5iaijlkhji8h4z1abxi")))

(define-public crate-indradb-plugin-host-0.3.0 (c (n "indradb-plugin-host") (v "0.3.0") (d (list (d (n "indradb-lib") (r "^4.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "1r9x3d2zvpc7vq3wscvkf3q9zyr7sm9pqd8fb0cf0chgy4j2r2fw")))

