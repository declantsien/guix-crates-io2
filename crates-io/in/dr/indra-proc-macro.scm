(define-module (crates-io in dr indra-proc-macro) #:use-module (crates-io))

(define-public crate-indra-proc-macro-0.1.0 (c (n "indra-proc-macro") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "1j2i7dmi5wa0ys091la2kd53kg0r1zpjq2p8vi5hc6l11470nj7c")))

