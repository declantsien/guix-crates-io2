(define-module (crates-io in dr indradb-sled) #:use-module (crates-io))

(define-public crate-indradb-sled-0.1.0 (c (n "indradb-sled") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "indradb-lib") (r "^2.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "sled") (r "^0.34.6") (f (quote ("compression" "no_metrics"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (o #t) (d #t) (k 0)) (d (n "uuid") (r "~0.8.2") (f (quote ("v1" "serde"))) (d #t) (k 0)))) (h "11vx1vk6q5x8hpwqw7bsnjssl3kyzv7vvayxygrm9akbbb5s0v92") (f (quote (("test-suite" "indradb-lib/test-suite" "tempfile") ("default") ("bench-suite" "indradb-lib/bench-suite" "tempfile"))))))

