(define-module (crates-io in i- ini-roundtrip) #:use-module (crates-io))

(define-public crate-ini-roundtrip-0.1.0 (c (n "ini-roundtrip") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "1rrcd65fx0pcj639i3kiwj3wddwlk2zp8qcsbpa0nzmywxjaqkz2")))

(define-public crate-ini-roundtrip-0.1.1 (c (n "ini-roundtrip") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "1ryf4hc035kqh5l662j9h3f00wxkvm10pa4xk7f481w0d724wxzk")))

(define-public crate-ini-roundtrip-0.1.2 (c (n "ini-roundtrip") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "1hlvpzpdbschfy5hq89n7dq5acivjq2a3g23n3kq6nhahjsjcfcp")))

(define-public crate-ini-roundtrip-0.1.3 (c (n "ini-roundtrip") (v "0.1.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "0agj7z790cplxphr2qqppk0a47sjrrbv3a6h1q21zi0dw49iin9i") (r "1.70.0")))

