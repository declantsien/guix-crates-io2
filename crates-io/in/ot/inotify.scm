(define-module (crates-io in ot inotify) #:use-module (crates-io))

(define-public crate-inotify-0.0.1 (c (n "inotify") (v "0.0.1") (h "058zrzjgw1msp1bbxzpk9c6cyqvq4f9wp4dq84vlwr1rfpkmndkq")))

(define-public crate-inotify-0.1.0 (c (n "inotify") (v "0.1.0") (h "1vbrx7ycbcb0spfamw2fr9l1qbk7b2jcja2309dp9pyjz39an442")))

(define-public crate-inotify-0.1.1 (c (n "inotify") (v "0.1.1") (h "1q7glshn31fd3dbjx9czasc5l6ay8mnkrcdwg0ch5f67kc274rs5")))

(define-public crate-inotify-0.1.2 (c (n "inotify") (v "0.1.2") (h "1a2nsl912v0kbrzaw0i87a1g2bq93a0b5lp9z8mrrf6dqjlgp30d")))

(define-public crate-inotify-0.1.3 (c (n "inotify") (v "0.1.3") (h "12n56wcz8cji0jp0sp4f8vpsjd6h0s1nimjg0q6vgyyrfhbj38hs")))

(define-public crate-inotify-0.1.4 (c (n "inotify") (v "0.1.4") (h "0i9071sqhkakzjv1kzzjr23sxyhpw84fnmrlgyqx0s1li309sy1v")))

(define-public crate-inotify-0.1.5 (c (n "inotify") (v "0.1.5") (h "1vcbwynfdg4fxyh5rw31l7l40bfmqh8j9ikjp91jlz8ij2cqp1ky")))

(define-public crate-inotify-0.1.6 (c (n "inotify") (v "0.1.6") (h "0pbj518ssvkxvin6yvdmg1s2alms3frc7a110c0j12zk2bxxz43b")))

(define-public crate-inotify-0.1.7 (c (n "inotify") (v "0.1.7") (h "0i6lxqabnkn1ffgba0m800q2lm4r9r7cr6266529yscrzj2i8wra")))

(define-public crate-inotify-0.1.8 (c (n "inotify") (v "0.1.8") (h "1sk1vl2b0hys43hnjhi1mdlvswhcvw37iwhgic155fn81w4knjxp")))

(define-public crate-inotify-0.1.9 (c (n "inotify") (v "0.1.9") (h "10y10jycgwvjcszwsxsl0z5alf3bdd4bglxfl26l9k59swc4wily")))

(define-public crate-inotify-0.1.10 (c (n "inotify") (v "0.1.10") (h "03y9mm1gdg5dv7w9726fb5rihjwfs0wj6r1w2yldv2dfpd4a0lcl")))

(define-public crate-inotify-0.1.11 (c (n "inotify") (v "0.1.11") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "03dpr9vxzwrvzariia1zlg3pskcky89cfm1qss0g16d6f09f21q8")))

(define-public crate-inotify-0.1.12 (c (n "inotify") (v "0.1.12") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1vnqizb4kq90l8mhq92m7jlq7mi0001v06fw22d1m62cx2mlrwlp")))

(define-public crate-inotify-0.2.0 (c (n "inotify") (v "0.2.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)))) (h "1jq049pz1qvvisx8qmn43p4ws60pjapc41z3l1k05hmmrgfj3ipn")))

(define-public crate-inotify-0.2.1 (c (n "inotify") (v "0.2.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "04d2vp1nnzg238di6yjvinf4v9h39q7vbywry7iizfirkb1v1l2y")))

(define-public crate-inotify-0.2.2 (c (n "inotify") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0j78qv9z100aymbg206ny4sw37fqx305bgr3swzirx2xnv35la4d")))

(define-public crate-inotify-0.2.3 (c (n "inotify") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "06pk1zyq7jwwb37hfksx8vkn9ny32i6k1hz2h2f31bxxpl3qqig8")))

(define-public crate-inotify-0.3.0 (c (n "inotify") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0362cbi4y9248km3h74vmcdpn0hrlxwqa4m6ws2pmrrn04ccqzw8")))

(define-public crate-inotify-0.4.0 (c (n "inotify") (v "0.4.0") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "inotify-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0c8aark6mdkw8wxmr67d9psxbliiizbjcw2c6x75zrgad2h86p92")))

(define-public crate-inotify-0.4.1 (c (n "inotify") (v "0.4.1") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "inotify-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0wb7cpaib9nffg45fzla66ajyk02az4jh0460r2r8j11573qam69")))

(define-public crate-inotify-0.4.2 (c (n "inotify") (v "0.4.2") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "inotify-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0kvazw9wk15k02qpwglpwkym3a18b4ymbbvsjqhibv41k4hp3l70")))

(define-public crate-inotify-0.5.0 (c (n "inotify") (v "0.5.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "inotify-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1rgzl6mvmdykb32phz41w3x8hlbyxlx9jc3wq7v8yqm4g1jz9aj1")))

(define-public crate-inotify-0.5.1 (c (n "inotify") (v "0.5.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "inotify-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0621mdqvwfwnl4m052zmcsgsb6xmyb2ndgkz2x70zz5a2khbzl1s")))

(define-public crate-inotify-0.6.0 (c (n "inotify") (v "0.6.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "inotify-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio-reactor") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1d2d0dv26zqqhigmn965xg92a8lnccpw4lkxx2nx1qw93qn72gcd") (f (quote (("stream" "futures" "mio" "tokio-io" "tokio-reactor") ("default" "stream"))))))

(define-public crate-inotify-0.6.1 (c (n "inotify") (v "0.6.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "inotify-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio-reactor") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0627k5aq44knjlrc09hl017nxap3svpl79przf26y3ciycwlbda0") (f (quote (("stream" "futures" "mio" "tokio-io" "tokio-reactor") ("default" "stream"))))))

(define-public crate-inotify-0.7.0 (c (n "inotify") (v "0.7.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "inotify-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio-reactor") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1f5zpi3rvzgc1fm5lgnhjrx6dbgmirf4k5n7w2120knnsmphvr14") (f (quote (("stream" "futures" "mio" "tokio" "tokio-io" "tokio-reactor") ("default" "stream"))))))

(define-public crate-inotify-0.8.0 (c (n "inotify") (v "0.8.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 2)) (d (n "inotify-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2.2") (f (quote ("io-driver"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.1") (f (quote ("macros" "rt-core"))) (d #t) (k 2)))) (h "0wyr6a6i1vi6gi8pgi37gyhrr8nr8dzfg2w7mhkfcmq3i558lpy4") (f (quote (("stream" "futures-core" "mio" "tokio") ("default" "stream"))))))

(define-public crate-inotify-0.8.1 (c (n "inotify") (v "0.8.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 2)) (d (n "inotify-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2.2") (f (quote ("io-driver"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.1") (f (quote ("macros" "rt-core"))) (d #t) (k 2)))) (h "02fj7qibjsvg9h9dwrwbday1awq3clnk2yqbdhldfvynfa1m31hb") (f (quote (("stream" "futures-core" "mio" "tokio") ("default" "stream"))))))

(define-public crate-inotify-0.8.2 (c (n "inotify") (v "0.8.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 2)) (d (n "inotify-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2.2") (f (quote ("io-driver"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.1") (f (quote ("macros" "rt-core"))) (d #t) (k 2)))) (h "1mf66gz52lm4bvgfwmqrrjm6rsjv9iky240bmbw7n9hig2cywfdw") (f (quote (("stream" "futures-core" "mio" "tokio") ("default" "stream"))))))

(define-public crate-inotify-0.8.3 (c (n "inotify") (v "0.8.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 2)) (d (n "inotify-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.2.2") (f (quote ("io-driver"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.1") (f (quote ("macros" "rt-core"))) (d #t) (k 2)))) (h "1m74znskinrvfcp0hczwwdxvc7kvnrrailngkivk1iwknfa0mpa6") (f (quote (("stream" "futures-core" "mio" "tokio") ("default" "stream"))))))

(define-public crate-inotify-0.7.1 (c (n "inotify") (v "0.7.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "inotify-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio-reactor") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0byhq4x4b2rlbkmfrab5dni39wiq2ls1hv1nhggp7rla5inwc5j8") (f (quote (("stream" "futures" "mio" "tokio" "tokio-io" "tokio-reactor") ("default" "stream"))))))

(define-public crate-inotify-0.9.0 (c (n "inotify") (v "0.9.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 2)) (d (n "inotify-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.3.3") (f (quote ("net"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.3.3") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0p9ky8mmwwkm3n2cr15cd7l0zkhbv1x99ijwvk4k3p32ska0hsd5") (f (quote (("stream" "futures-core" "tokio") ("default" "stream"))))))

(define-public crate-inotify-0.9.1 (c (n "inotify") (v "0.9.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 2)) (d (n "inotify-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.3.3") (f (quote ("net"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.3.3") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1y7kpbin0bp2k2swy3mn8zxjpayirn134w9rn1x6900mzf6q9ih4") (f (quote (("stream" "futures-core" "tokio") ("default" "stream"))))))

(define-public crate-inotify-0.9.2 (c (n "inotify") (v "0.9.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 2)) (d (n "inotify-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0.1") (f (quote ("net"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "059zr5vvlbbnhy5lkjbmgp9szg9fz3f7dkd38fgd1nda3gdmg7yi") (f (quote (("stream" "futures-core" "tokio") ("default" "stream"))))))

(define-public crate-inotify-0.9.3 (c (n "inotify") (v "0.9.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 2)) (d (n "inotify-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0.1") (f (quote ("net"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1cs3xlkq3fnfca3mimvifazp01ayscinm07v38ifw0xin5f4fcdh") (f (quote (("stream" "futures-core" "tokio") ("default" "stream"))))))

(define-public crate-inotify-0.9.4 (c (n "inotify") (v "0.9.4") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 2)) (d (n "inotify-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0.1") (f (quote ("net"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0pmzvympgly140gqf491a5c29xq1v57bhrbffgc4qw8nwmbxg3nq") (f (quote (("stream" "futures-core" "tokio") ("default" "stream"))))))

(define-public crate-inotify-0.9.5 (c (n "inotify") (v "0.9.5") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 2)) (d (n "inotify-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0.1") (f (quote ("net"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "07wh1jw821g70rvabmj1w1p8jxjgvbzszj4n594wiads3pschpwy") (f (quote (("stream" "futures-core" "tokio") ("default" "stream"))))))

(define-public crate-inotify-0.9.6 (c (n "inotify") (v "0.9.6") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 2)) (d (n "inotify-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0.1") (f (quote ("net"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1zxb04c4qccp8wnr3v04l503qpxzxzzzph61amlqbsslq4z9s1pq") (f (quote (("stream" "futures-core" "tokio") ("default" "stream"))))))

(define-public crate-inotify-0.10.0 (c (n "inotify") (v "0.10.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 2)) (d (n "inotify-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0.1") (f (quote ("net"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1yfkp6k5yn1lyy2qbsnikaix22zikygdqj69nabh2aawazwqiy5b") (f (quote (("stream" "futures-core" "tokio") ("default" "stream"))))))

(define-public crate-inotify-0.10.1 (c (n "inotify") (v "0.10.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 2)) (d (n "inotify-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0.1") (f (quote ("net"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1dha4zd80c2is1jdvmqnlscvdh6q6dr6afw38nfg12w9zcam4czz") (f (quote (("stream" "futures-core" "tokio") ("default" "stream")))) (r "1.63")))

(define-public crate-inotify-0.10.2 (c (n "inotify") (v "0.10.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.1") (d #t) (k 2)) (d (n "inotify-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0.1") (f (quote ("net"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1k2m6a95827yspax1icmwiz4szr7c01w3dnn2b2bil4hfvcnilgx") (f (quote (("stream" "futures-core" "tokio") ("default" "stream")))) (r "1.63")))

