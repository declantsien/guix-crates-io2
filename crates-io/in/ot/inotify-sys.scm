(define-module (crates-io in ot inotify-sys) #:use-module (crates-io))

(define-public crate-inotify-sys-0.1.0 (c (n "inotify-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1m8dzn7p3g09hvwm6c39bf4xcz3q1qzrvynww14vmdlsg7q3sfy1")))

(define-public crate-inotify-sys-0.1.1 (c (n "inotify-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "093xacj1kzx5q8y27rmmig4ji1vr1j1ljnrxyymmyr331jng6kw5")))

(define-public crate-inotify-sys-0.1.2 (c (n "inotify-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1yz4kqqj66imshn8mpv8pqfidm1pj0gbrbyn9i6ay2zp8d6bkkkx")))

(define-public crate-inotify-sys-0.1.3 (c (n "inotify-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1h2nwgajz80qddjm4mpma94zahxw84nscbycy9pgzbjrgjl1ljp7")))

(define-public crate-inotify-sys-0.1.4 (c (n "inotify-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mcw4aa56gsv374gzbv2hir1lc4dkwphpgx5h0qsp1b5hmakamn4")))

(define-public crate-inotify-sys-0.1.5 (c (n "inotify-sys") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1syhjgvkram88my04kv03s0zwa66mdwa5v7ddja3pzwvx2sh4p70")))

