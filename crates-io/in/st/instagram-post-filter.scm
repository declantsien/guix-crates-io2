(define-module (crates-io in st instagram-post-filter) #:use-module (crates-io))

(define-public crate-instagram-post-filter-0.1.0 (c (n "instagram-post-filter") (v "0.1.0") (d (list (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "0x5qqxl97bd05h5djs2glgwnl99wgkysca2w0w7dk9ny7hivaccy")))

(define-public crate-instagram-post-filter-0.1.1 (c (n "instagram-post-filter") (v "0.1.1") (d (list (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "06y6f1iygl08qmzhfki7jnjs5aswbcrvxf3f9shx9fhm613bxvrd")))

