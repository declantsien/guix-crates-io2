(define-module (crates-io in st install-framework) #:use-module (crates-io))

(define-public crate-install-framework-1.0.0 (c (n "install-framework") (v "1.0.0") (d (list (d (n "install-framework-base") (r "^1.0.0") (d #t) (k 0)) (d (n "install-framework-cli") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "install-framework-core") (r "^1.0.0") (d #t) (k 0)) (d (n "install-framework-gui") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1h1pwc428lsgy5vv78qds8bgmz99226wzq34vhdxkw88qr7d6hf0") (f (quote (("keep-cache" "install-framework-base/keep-cache" "install-framework-gui/keep-cache" "install-framework-cli/keep-cache") ("gui" "install-framework-gui") ("cli" "install-framework-cli"))))))

