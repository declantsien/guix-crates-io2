(define-module (crates-io in st instagram-graph-api) #:use-module (crates-io))

(define-public crate-instagram-graph-api-0.0.0 (c (n "instagram-graph-api") (v "0.0.0") (h "0mr1g200yxk84izb9kdr3bdj535hvbx34qmmbk23zsg114nb86b9")))

(define-public crate-instagram-graph-api-0.1.0 (c (n "instagram-graph-api") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "error-macro") (r "^0.2") (k 0)) (d (n "facebook-graph-api-object-error") (r "^0.2") (k 0)) (d (n "facebook-graph-api-object-paging") (r "^0.1") (k 0)) (d (n "http-api-client-endpoint") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (k 0)) (d (n "serde-aux") (r "^4") (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (k 0)) (d (n "wrapping-macro") (r "^0.1") (k 0)))) (h "1wfg3200wkki4pc09az1jgw0r8mqqribirj8slr80f2ahiyay4vl")))

(define-public crate-instagram-graph-api-0.1.1 (c (n "instagram-graph-api") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (k 0)) (d (n "error-macro") (r "^0.2") (k 0)) (d (n "facebook-graph-api-object-error") (r "^0.2") (k 0)) (d (n "facebook-graph-api-object-paging") (r "^0.1") (k 0)) (d (n "http-api-client-endpoint") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (k 0)) (d (n "serde-aux") (r "^4") (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (k 0)) (d (n "wrapping-macro") (r "^0.1") (k 0)))) (h "121cmgi8czayxfdnknaaimrkdqy65gm2kz6xg04x8hhy5kw3s0hp")))

