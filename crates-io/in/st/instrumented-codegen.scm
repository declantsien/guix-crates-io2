(define-module (crates-io in st instrumented-codegen) #:use-module (crates-io))

(define-public crate-instrumented-codegen-0.1.0 (c (n "instrumented-codegen") (v "0.1.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xjl1w154kxh3zhxhaf95isb2c5qgqkhdq7m1bp9amlw6wrnz6bh")))

(define-public crate-instrumented-codegen-0.1.1 (c (n "instrumented-codegen") (v "0.1.1") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "instrumented") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0b0yqsbigvpdkh1gwx56gm8fvy5363h6rsx2f5sq3yx5sjzn3szz")))

(define-public crate-instrumented-codegen-0.1.2 (c (n "instrumented-codegen") (v "0.1.2") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "instrumented") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13rkbb2mphp3l70i3dfzk4y3za0wb881grwjl5ccaqdg960lcbxx")))

(define-public crate-instrumented-codegen-0.1.3 (c (n "instrumented-codegen") (v "0.1.3") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "instrumented") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07vyl69yd22vqkvvv213jk3rik38kb2bmrqnagipi25hzllafapb")))

