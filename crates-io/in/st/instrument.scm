(define-module (crates-io in st instrument) #:use-module (crates-io))

(define-public crate-instrument-0.1.0 (c (n "instrument") (v "0.1.0") (d (list (d (n "pitch_calc") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.4") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "time_calc") (r "^0.11.0") (d #t) (k 0)))) (h "1v0q4rn4scpq0f179wpdm46vbx9qld6i90kwwyrwf9qkkqyswnmn") (f (quote (("serde_serialization" "serde" "serde_json" "pitch_calc/serde_serialization" "time_calc/serde_serialization"))))))

(define-public crate-instrument-0.2.0 (c (n "instrument") (v "0.2.0") (d (list (d (n "pitch_calc") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.4") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "time_calc") (r "^0.11.0") (d #t) (k 0)))) (h "1lgfd7nh429wwv0dm8bfgz08viplbjk0nhdxrib5ghdqdz58qj1m") (f (quote (("serde_serialization" "serde" "serde_json" "pitch_calc/serde_serialization" "time_calc/serde_serialization"))))))

