(define-module (crates-io in st instaclone_backend) #:use-module (crates-io))

(define-public crate-instaclone_backend-0.1.0 (c (n "instaclone_backend") (v "0.1.0") (d (list (d (n "diesel") (r "^1.1.1") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.11.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.2") (d #t) (k 0)) (d (n "r2d2-diesel") (r "^1.0.0") (d #t) (k 0)) (d (n "rocket") (r "^0.3.6") (d #t) (k 0)) (d (n "rocket_codegen") (r "^0.3.6") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.3.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "1vv8bd5ipzkdxncj08yaj26if7hdl8xgapnyp85lxmz38sl6icm5")))

