(define-module (crates-io in st instrumenter-proc-macro) #:use-module (crates-io))

(define-public crate-instrumenter-proc-macro-0.1.0 (c (n "instrumenter-proc-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.46") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "10f08rh9syn0nsfcxdn3v9jxhw8lcn4z2q1842d559dj64v5xm3s") (f (quote (("enabled" "proc-macro2" "quote" "syn"))))))

(define-public crate-instrumenter-proc-macro-0.1.1 (c (n "instrumenter-proc-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0wazcz7r8hiibn5rvjc06bh01x4ybi3v8rag9rm9qz0hprprivfc") (f (quote (("enabled" "proc-macro2" "quote" "syn"))))))

