(define-module (crates-io in st instagram-media-shortcode) #:use-module (crates-io))

(define-public crate-instagram-media-shortcode-0.1.0 (c (n "instagram-media-shortcode") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 2)))) (h "13p419ms2fyv45qylz4c7y9d2qb5sgv2x8bdihijhjv5zyicr24v")))

(define-public crate-instagram-media-shortcode-0.1.1 (c (n "instagram-media-shortcode") (v "0.1.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 2)))) (h "0f7jbbqixyjvj9b0lbzxbb49zscin66dxsidrrs1zwrzxpljn7x0")))

(define-public crate-instagram-media-shortcode-0.1.2 (c (n "instagram-media-shortcode") (v "0.1.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 2)))) (h "1r0lr4hmbs4xss94kh5w10cslkkhncs5yidxrfi1b82dnwmg68km")))

(define-public crate-instagram-media-shortcode-0.1.3 (c (n "instagram-media-shortcode") (v "0.1.3") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 2)))) (h "1c1ip4bgdcx7f10f8zl9xd8h0rs5w4fkq1n1183id0dpvs9syd74")))

(define-public crate-instagram-media-shortcode-0.1.4 (c (n "instagram-media-shortcode") (v "0.1.4") (d (list (d (n "base64") (r "^0.13") (f (quote ("alloc"))) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 2)))) (h "0wkzn4n9fxg9vfyshbvxisa7qh4cfg6bkfyp88z44bjvlfh7j948")))

