(define-module (crates-io in st instagram-link) #:use-module (crates-io))

(define-public crate-instagram-link-0.1.0 (c (n "instagram-link") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "instagram-media-shortcode") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0n24kghc7al8ar88llj10m08lgy9g60rqddb793ydlyhmpg176j9")))

(define-public crate-instagram-link-0.1.1 (c (n "instagram-link") (v "0.1.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "instagram-media-shortcode") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1h3y7q5f9bnwrmdb584pawl7srsfmc503d0pdy4yd5p0hs7gf8ny")))

(define-public crate-instagram-link-0.1.2 (c (n "instagram-link") (v "0.1.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "instagram-media-shortcode") (r "=0.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0d8r37ahzlm1gi2asqbkvjhh6nc89ldd5nas5lj7mvrjbkz3xsbz")))

(define-public crate-instagram-link-0.1.3 (c (n "instagram-link") (v "0.1.3") (d (list (d (n "base64") (r "^0.13") (k 0)) (d (n "instagram-media-shortcode") (r "^0.1.4") (d #t) (k 0)) (d (n "url") (r "^2.2") (k 0)))) (h "1px3kcxwj624bianbdvj04199km8y8wrlk969s92xpiaxm4l408r")))

