(define-module (crates-io in st install-framework-base) #:use-module (crates-io))

(define-public crate-install-framework-base-1.0.0 (c (n "install-framework-base") (v "1.0.0") (d (list (d (n "install-framework-core") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("shobjidl_core"))) (d #t) (k 0)) (d (n "zip") (r "^0.5.11") (d #t) (k 0)))) (h "0vx5x929x6b1l8zmmyba7sw5dacyc16c2jclhrhxaih3i02pcq74") (f (quote (("keep-cache"))))))

