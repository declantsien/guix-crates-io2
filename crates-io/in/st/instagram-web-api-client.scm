(define-module (crates-io in st instagram-web-api-client) #:use-module (crates-io))

(define-public crate-instagram-web-api-client-0.0.0 (c (n "instagram-web-api-client") (v "0.0.0") (h "0sminf3qcvp07lizyz5lmmx214g3srbd6ncjwwffxiv45dzbzc9v") (y #t)))

(define-public crate-instagram-web-api-client-0.0.1 (c (n "instagram-web-api-client") (v "0.0.1") (h "1zql3m4a6k01q82rqx2zrzdb12hb0lqkwzh3fvm6hwpkx60ji80m")))

