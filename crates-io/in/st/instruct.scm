(define-module (crates-io in st instruct) #:use-module (crates-io))

(define-public crate-instruct-0.1.0 (c (n "instruct") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fern") (r "^0.6") (f (quote ("colored"))) (d #t) (k 0)) (d (n "figment") (r "^0.10") (f (quote ("toml" "env"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "=1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1p6n15gbb2bg43089jhrkszxk43k3qy8q2i8rsvkhcyrkiqbaiq9")))

