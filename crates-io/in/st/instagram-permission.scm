(define-module (crates-io in st instagram-permission) #:use-module (crates-io))

(define-public crate-instagram-permission-0.1.0 (c (n "instagram-permission") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0cg2l2dmdagpywfxjv4ahvxyw3sw12sh20n17lhkb52qfkyx3mxh")))

(define-public crate-instagram-permission-0.1.1 (c (n "instagram-permission") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde-enum-str") (r "^0.2") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0kq1sjw1j6md1wbi9vcn6gf5lh0c67kqm80vrzr4vw45br94p1lc")))

