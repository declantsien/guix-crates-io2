(define-module (crates-io in st instrumented) #:use-module (crates-io))

(define-public crate-instrumented-0.1.0 (c (n "instrumented") (v "0.1.0") (d (list (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "instrumented-codegen") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.7") (f (quote ("nightly" "process"))) (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "1vncv5l4vwgplg33b9cpl0c7lfpypzqz0s12sc5lvqy1nkrp7997")))

(define-public crate-instrumented-0.1.1 (c (n "instrumented") (v "0.1.1") (d (list (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "instrumented-codegen") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.7") (f (quote ("nightly" "process"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 2)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "15hw28sjxp3ikqa79299p2xr8nal284r9r7k6qwx0b0fv1spw5zx")))

(define-public crate-instrumented-0.1.2 (c (n "instrumented") (v "0.1.2") (d (list (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "instrumented-codegen") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.7") (f (quote ("nightly" "process"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 2)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "1157j9nyx3dqkq262kp0rcs344y511sjanqg5xdwdg1p6frc5xvy")))

(define-public crate-instrumented-0.1.3 (c (n "instrumented") (v "0.1.3") (d (list (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "instrumented-codegen") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prometheus") (r "^0.7") (f (quote ("nightly" "process"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 2)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "0x4d0ql31w0cdaz4ghdv16a3rxyas79h6n184wcr897a1mbj8b65")))

