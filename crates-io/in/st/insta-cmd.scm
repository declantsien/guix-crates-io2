(define-module (crates-io in st insta-cmd) #:use-module (crates-io))

(define-public crate-insta-cmd-0.1.0 (c (n "insta-cmd") (v "0.1.0") (d (list (d (n "insta") (r "^1.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "095xxyvxix4ymq7fk1h722gz5x6g9mmnfyqipgp63b5s7npwk3vr") (r "1.56.1")))

(define-public crate-insta-cmd-0.1.1 (c (n "insta-cmd") (v "0.1.1") (d (list (d (n "insta") (r "^1.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0r68xhzr3rah7gnmvvanpmcir60d8y5sb25w57g7s66bkrzpknaw") (r "1.57.0")))

(define-public crate-insta-cmd-0.2.0 (c (n "insta-cmd") (v "0.2.0") (d (list (d (n "insta") (r "^1.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "0ixfpchnccghl0hpdf9nhlwmqif2ndkcq1axbkpjiwphlqgn7jvn") (r "1.57.0")))

(define-public crate-insta-cmd-0.3.0 (c (n "insta-cmd") (v "0.3.0") (d (list (d (n "insta") (r "^1.29.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1l6q4bliifldw4658mmrakjg273dp52s8yf6hi267d3vrb7f9pj8") (r "1.57.0")))

(define-public crate-insta-cmd-0.4.0 (c (n "insta-cmd") (v "0.4.0") (d (list (d (n "insta") (r "^1.29.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "09sirdhr24fx487wn60zwj063cjwyx8r46bg431dbs6ns4ik17c0") (r "1.57.0")))

(define-public crate-insta-cmd-0.5.0 (c (n "insta-cmd") (v "0.5.0") (d (list (d (n "insta") (r "^1.29.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "18jjwj0b0dbqwasliahniwjami7d6n6zq35919kpb7xpjiwz300r") (r "1.57.0")))

(define-public crate-insta-cmd-0.6.0 (c (n "insta-cmd") (v "0.6.0") (d (list (d (n "insta") (r "^1.29.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1rix5nmswns1p5p5f7pj5l9wvm69awzby0fbkkacwp4j4ylyzvpz") (r "1.57.0")))

