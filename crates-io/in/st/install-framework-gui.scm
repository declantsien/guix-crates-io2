(define-module (crates-io in st install-framework-gui) #:use-module (crates-io))

(define-public crate-install-framework-gui-1.0.0 (c (n "install-framework-gui") (v "1.0.0") (d (list (d (n "async-channel") (r "^1.6.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "iced") (r "^0.3.0") (d #t) (k 0)) (d (n "iced_futures") (r "^0.3.0") (d #t) (k 0)) (d (n "iced_native") (r "^0.3.0") (d #t) (k 0)) (d (n "install-framework-base") (r "^1.0.0") (d #t) (k 0)) (d (n "install-framework-core") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "zip") (r "^0.5.11") (d #t) (k 0)))) (h "1qbdppv55lnp3wym0r6m2w1v7fgh7g89m4z08z1smqj52zc9kx6y") (f (quote (("keep-cache" "install-framework-base/keep-cache"))))))

