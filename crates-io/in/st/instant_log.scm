(define-module (crates-io in st instant_log) #:use-module (crates-io))

(define-public crate-instant_log-0.1.0 (c (n "instant_log") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)))) (h "0v8bz5bxm7a2bxbz7k5l5x2ak596812323s333jql8aikbrp2v1m") (y #t)))

(define-public crate-instant_log-0.1.1 (c (n "instant_log") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)))) (h "0lak6zik0wad361m0mnd5a54bqg45cgrgi8siygxcsfpms6q19jw")))

(define-public crate-instant_log-0.1.2 (c (n "instant_log") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)))) (h "0x96q6547lgm8743y35yylmfdzmiip1970p6krjxmky009swgkaq")))

