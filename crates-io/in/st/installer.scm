(define-module (crates-io in st installer) #:use-module (crates-io))

(define-public crate-installer-0.1.0-alpha1 (c (n "installer") (v "0.1.0-alpha1") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "manager") (r "^0.1.1") (d #t) (k 0)) (d (n "networking") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)))) (h "134di76r4mba2lk8wcj290bnjh5qw3nmkvpzp9v4yk1z67l3q1bw")))

