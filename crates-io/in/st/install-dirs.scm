(define-module (crates-io in st install-dirs) #:use-module (crates-io))

(define-public crate-install-dirs-0.1.0 (c (n "install-dirs") (v "0.1.0") (h "101m267w386s26kpn2ar17appq1zlw8wlmn44jx8295ba7lgzsn3")))

(define-public crate-install-dirs-0.2.0 (c (n "install-dirs") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "175pcd6ydpxbyy8a21mms82akidadla55frg2qwp54ypq42knb8s")))

(define-public crate-install-dirs-0.2.1 (c (n "install-dirs") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1i1npghlczy91y0zjvgnxf7z17ab26pxw809khrpqqbddcvfvvs1")))

(define-public crate-install-dirs-0.3.0 (c (n "install-dirs") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0gkrgyk09xmrkzmhgg7a50429xa2rps4414h1j4yyl1wcky1ws5m")))

(define-public crate-install-dirs-0.3.1 (c (n "install-dirs") (v "0.3.1") (d (list (d (n "serde") (r "^1.0.123, <=1.0.171") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1fcbzabl4i1xhvl9kyx8jyn3ph1mbn1mm6rkcjs9y92y1fbp3gwv")))

(define-public crate-install-dirs-0.3.2 (c (n "install-dirs") (v "0.3.2") (d (list (d (n "serde") (r "^1.0.123") (o #t) (d #t) (k 0)))) (h "0wl1lz1vvjxri3m79k060b70gxnbx2cyxg5p33g8zw1i2ywf1lx6")))

