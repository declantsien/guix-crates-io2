(define-module (crates-io in st instagram-graph-api-client) #:use-module (crates-io))

(define-public crate-instagram-graph-api-client-0.0.0 (c (n "instagram-graph-api-client") (v "0.0.0") (h "1s0azh4sf14vmbqafbnx14cfxb1wac3c8z7zyv8ja6g30wr81jfq")))

(define-public crate-instagram-graph-api-client-0.0.1 (c (n "instagram-graph-api-client") (v "0.0.1") (h "062nx9issvz5s84vrq8cip2ibi8fz6jy7b4hv6l8n0pxijgi6163")))

