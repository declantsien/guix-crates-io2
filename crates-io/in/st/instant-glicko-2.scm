(define-module (crates-io in st instant-glicko-2) #:use-module (crates-io))

(define-public crate-instant-glicko-2-0.1.0 (c (n "instant-glicko-2") (v "0.1.0") (h "18l44wj55mnjvg1wpz4xry59a35raahv56yvm7fnv2ddimq0mm54")))

(define-public crate-instant-glicko-2-0.2.0 (c (n "instant-glicko-2") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "10qcmkyh9q1drhrb70jdrkwhipcwwwax81nwi7f59kafhvkb7fd4") (r "1.58.1")))

