(define-module (crates-io in st instagen) #:use-module (crates-io))

(define-public crate-instagen-0.1.0 (c (n "instagen") (v "0.1.0") (d (list (d (n "assert_cmd") (r ">= 0.11.1") (d #t) (k 0)) (d (n "clap") (r ">= 2.33") (d #t) (k 0)) (d (n "reqwest") (r ">= 0.9.18") (d #t) (k 0)) (d (n "serde") (r ">= 1.0.97") (d #t) (k 0)))) (h "1v21sx6cgmah3zm6yp2q242586h33wqdb9rb1rhdylxbqrfjr7c9")))

(define-public crate-instagen-0.1.1 (c (n "instagen") (v "0.1.1") (d (list (d (n "assert_cmd") (r ">= 0.11.1") (d #t) (k 0)) (d (n "clap") (r ">= 2.33") (d #t) (k 0)) (d (n "maplit") (r ">= 1.0.1") (d #t) (k 0)) (d (n "reqwest") (r ">= 0.9.18") (d #t) (k 0)) (d (n "serde") (r ">= 1.0.97") (d #t) (k 0)))) (h "1k3lf7rzl68vs9q759n7w545s1r97722pnfw8bjmblmdf8c2iy0n")))

