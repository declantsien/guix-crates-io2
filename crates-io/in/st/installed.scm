(define-module (crates-io in st installed) #:use-module (crates-io))

(define-public crate-installed-0.1.0 (c (n "installed") (v "0.1.0") (d (list (d (n "plist") (r "^1.4") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows") (r "^0.44") (f (quote ("Win32_System_Registry"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "winreg") (r "^0.11") (d #t) (t "cfg(windows)") (k 0)))) (h "15z92ba60bij2zc43vpvy6p4659lfg324xg6pimwrc9wyrhr29rw")))

