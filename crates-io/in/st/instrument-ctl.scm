(define-module (crates-io in st instrument-ctl) #:use-module (crates-io))

(define-public crate-instrument-ctl-0.1.0 (c (n "instrument-ctl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rs-usbtmc") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "111sf2c89yj4f8p6n5q1abs40wccjdk9sqhkxp3yxbgxrbsq7h16")))

(define-public crate-instrument-ctl-0.1.1 (c (n "instrument-ctl") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rs-usbtmc") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jqb3dg2qsw2afi88njri1gwpwf91zhm05qg6hllhx8rrlqq0ckq") (y #t)))

(define-public crate-instrument-ctl-0.1.2 (c (n "instrument-ctl") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rs-usbtmc") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0d0jfs3hwcyrnaqqd9l611885kyddjdmsdrj8l5m319blb56lgg4") (y #t)))

(define-public crate-instrument-ctl-0.1.3 (c (n "instrument-ctl") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rs-usbtmc") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1kslr6a5iaa304v4m73drc02abs21r2mgjkp38wihhsyi3cld8cl") (y #t)))

(define-public crate-instrument-ctl-0.1.4 (c (n "instrument-ctl") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rs-usbtmc") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0z0ym479v8anm7i9a8c2s6z4fw5gyl4mzfyaakl44bjlqmy7vjgy")))

