(define-module (crates-io in st instant-timer) #:use-module (crates-io))

(define-public crate-instant-timer-0.1.0 (c (n "instant-timer") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "1pfa5k84djn140kmzxiziksk2lsb48c12bsaqkghlhlhjhf8wq64")))

(define-public crate-instant-timer-0.1.1 (c (n "instant-timer") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "0wc7agyjl5kn4lakxv3fkhzlrm261yy2w5jp2v17if5a8br0z90g")))

