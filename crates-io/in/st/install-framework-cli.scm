(define-module (crates-io in st install-framework-cli) #:use-module (crates-io))

(define-public crate-install-framework-cli-1.0.0 (c (n "install-framework-cli") (v "1.0.0") (d (list (d (n "clap") (r "^2.27.0") (d #t) (k 0)) (d (n "install-framework-base") (r "^1.0.0") (d #t) (k 0)) (d (n "install-framework-core") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "zip") (r "^0.5.11") (d #t) (k 0)))) (h "13pmhy19qh8fci5rgdmjwpxllxjlph7mpa1wvrwhwr2krdwvl8yx") (f (quote (("keep-cache" "install-framework-base/keep-cache"))))))

