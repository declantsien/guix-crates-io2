(define-module (crates-io in st instrs-derive) #:use-module (crates-io))

(define-public crate-instrs-derive-0.1.0 (c (n "instrs-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote_into") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1x211wpsyw1y2j2s9prs3wh298wlk4ycsk0pz90pfvffvw090p0b")))

