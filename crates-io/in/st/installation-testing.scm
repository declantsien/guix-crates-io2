(define-module (crates-io in st installation-testing) #:use-module (crates-io))

(define-public crate-installation-testing-0.0.1 (c (n "installation-testing") (v "0.0.1") (h "0k37flhv1sjrslp7sbjpjxn54d1kx529nj3wv4w39lfr77da5qi6")))

(define-public crate-installation-testing-0.0.2 (c (n "installation-testing") (v "0.0.2") (h "1jhi73sbqkbd0k7nrvfn6i542ikrzvzawgz4n9w8a36im7scf68m")))

(define-public crate-installation-testing-0.0.3 (c (n "installation-testing") (v "0.0.3") (h "1fm1qwd042bmp01q3cwgcqg67rc4varnmlqm6ywfn3qzqxgcblvg")))

(define-public crate-installation-testing-0.0.4 (c (n "installation-testing") (v "0.0.4") (h "1356cr8g482c2ri4wh0yzqc0cw8j9hhmykwqpjngpc41xddc3hh1")))

