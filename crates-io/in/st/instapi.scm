(define-module (crates-io in st instapi) #:use-module (crates-io))

(define-public crate-instapi-1.0.0 (c (n "instapi") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 2)) (d (n "dirs") (r "^4.0.0") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "open") (r "^2.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0qqc32l2135b99vpclba8rrqfwjxb56lqrq69pqw967bm7rfskqx")))

