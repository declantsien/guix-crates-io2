(define-module (crates-io in st instruction_serialiser) #:use-module (crates-io))

(define-public crate-instruction_serialiser-0.0.1 (c (n "instruction_serialiser") (v "0.0.1") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 1)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.9.0") (d #t) (k 1)))) (h "1wma5ixhibb23ygprikprrbn8d0x436jr2z9skm86zpyagp59z97")))

(define-public crate-instruction_serialiser-0.0.2 (c (n "instruction_serialiser") (v "0.0.2") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.9.0") (d #t) (k 1)))) (h "1k8b87kqhs1gaf4lh06ylfgav5gw0dsfjcxhk4grcn5ma44kyx7d")))

(define-public crate-instruction_serialiser-0.0.3 (c (n "instruction_serialiser") (v "0.0.3") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.9.0") (d #t) (k 1)))) (h "0f8svvi6dwhga4bfrl02c25lmlk8l4h9y6344lwyn61gg4iamvpx")))

(define-public crate-instruction_serialiser-0.0.4 (c (n "instruction_serialiser") (v "0.0.4") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.9.0") (d #t) (k 1)))) (h "0b31kbcn2xgp4l6xd7xkd5q6jzkbn443g8kp6n9dphsam9a6mmfw")))

(define-public crate-instruction_serialiser-0.0.5 (c (n "instruction_serialiser") (v "0.0.5") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.9.0") (d #t) (k 1)))) (h "1m7p3v4fjs67r4rwq96n8qbf5w3dx4vyljgmwa220cd7c3bqri9p")))

(define-public crate-instruction_serialiser-0.0.6 (c (n "instruction_serialiser") (v "0.0.6") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "config") (r "^0.12.0") (d #t) (k 1)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.9.0") (d #t) (k 1)))) (h "1r1f728hqvmm4x2ylabx700nah0cvl6jw69yq2mi4r150bpawk9b")))

