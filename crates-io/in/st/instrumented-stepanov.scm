(define-module (crates-io in st instrumented-stepanov) #:use-module (crates-io))

(define-public crate-instrumented-stepanov-0.1.0 (c (n "instrumented-stepanov") (v "0.1.0") (d (list (d (n "rand") (r "0.6.*") (d #t) (k 0)))) (h "0v7gjv7v21938jwzmgw9sxzwkangh4sik2dh2xmnvg7rqbabm7m9")))

(define-public crate-instrumented-stepanov-1.0.0 (c (n "instrumented-stepanov") (v "1.0.0") (d (list (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "0.6.*") (d #t) (k 0)))) (h "1ha401vbwg21dvb99pvfb75g8z2d3qmkkxbphiy87cshv9ig07lb")))

(define-public crate-instrumented-stepanov-1.1.1 (c (n "instrumented-stepanov") (v "1.1.1") (d (list (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "0.6.*") (d #t) (k 0)))) (h "187jnh4fmb4rm4w6vhvf47hmicmlrc6pky1n0vms3z00vgw1l1y3")))

(define-public crate-instrumented-stepanov-1.1.2 (c (n "instrumented-stepanov") (v "1.1.2") (d (list (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "rand") (r "0.6.*") (d #t) (k 0)))) (h "0a3xr71paj3dd5mgzb2b5qpkx9phbvhnbfai1csrbg0f419pbb0b")))

