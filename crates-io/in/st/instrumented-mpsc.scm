(define-module (crates-io in st instrumented-mpsc) #:use-module (crates-io))

(define-public crate-instrumented-mpsc-0.1.0 (c (n "instrumented-mpsc") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "prometheus") (r "^0.7") (d #t) (k 0)))) (h "1y74gak3v2akwqa0smxnv35g0w1w85hnnqwx5fk8iy7044d75vy9")))

