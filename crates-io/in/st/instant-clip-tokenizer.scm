(define-module (crates-io in st instant-clip-tokenizer) #:use-module (crates-io))

(define-public crate-instant-clip-tokenizer-0.1.0 (c (n "instant-clip-tokenizer") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.6") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "00p91714rs9z8xdik1rjbmck8a9pi9pa8661p8ahmn39x7djbnm0") (f (quote (("openai-vocabulary-file") ("default" "openai-vocabulary-file")))) (r "1.65")))

