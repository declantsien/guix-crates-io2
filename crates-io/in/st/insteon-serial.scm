(define-module (crates-io in st insteon-serial) #:use-module (crates-io))

(define-public crate-insteon-serial-0.1.0 (c (n "insteon-serial") (v "0.1.0") (d (list (d (n "serialport") (r "^3.2.0") (d #t) (k 0)))) (h "10l67cf44mp4m9fp6iwksk4pd624w384f877mjlvszkj80a7rcbr")))

(define-public crate-insteon-serial-0.1.1 (c (n "insteon-serial") (v "0.1.1") (d (list (d (n "serialport") (r "^3.2.0") (d #t) (k 0)))) (h "0glmlyvjfshraphh9rkqf88p4z8qq4cs4s40bbbhiqx44bwfv396")))

