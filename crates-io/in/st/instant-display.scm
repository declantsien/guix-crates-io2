(define-module (crates-io in st instant-display) #:use-module (crates-io))

(define-public crate-instant-display-0.1.0 (c (n "instant-display") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (t "cfg(all(unix, not(any(target_vendor = \"apple\", target_os = \"android\", target_os = \"redox\"))))") (k 0)) (d (n "raw-window-handle") (r "^0.5.2") (d #t) (k 0)) (d (n "x11rb") (r "^0.12.0") (f (quote ("allow-unsafe-code"))) (t "cfg(all(unix, not(any(target_vendor = \"apple\", target_os = \"android\", target_os = \"redox\"))))") (k 0)))) (h "1xbqdbimg4lvbnk4nsiwvz19psa21l64f10hsibshz76ir7lmay2")))

