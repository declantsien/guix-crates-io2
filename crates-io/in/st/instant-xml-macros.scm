(define-module (crates-io in st instant-xml-macros) #:use-module (crates-io))

(define-public crate-instant-xml-macros-0.1.0 (c (n "instant-xml-macros") (v "0.1.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1944giyzq8x8kip2qbxp4s4m4ax0449pwkvhjlpm54qdv4135s78") (r "1.58")))

(define-public crate-instant-xml-macros-0.2.0 (c (n "instant-xml-macros") (v "0.2.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1s5vjwsqryxkz73m8fxm5xcsysnq4xhpay69z4p8i22ibircxsjd") (r "1.58")))

(define-public crate-instant-xml-macros-0.3.0 (c (n "instant-xml-macros") (v "0.3.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1a4x2h3i6ld9md4k582lf7a1js9xqlvi2jyq0a3v62si7mjxxybv") (r "1.58")))

(define-public crate-instant-xml-macros-0.3.1 (c (n "instant-xml-macros") (v "0.3.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "12lqwvsxfywhzm1ga4n4qin8qcc15jn7ig88p6xsqrrd8x9r8ab1") (r "1.58")))

(define-public crate-instant-xml-macros-0.3.2 (c (n "instant-xml-macros") (v "0.3.2") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "14ak13sk5isn6zv00c7hwhij6qxmk0ilk27g70cg8w2sm2p4ildd") (r "1.58")))

(define-public crate-instant-xml-macros-0.4.0 (c (n "instant-xml-macros") (v "0.4.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1a7222kk6q46ydq2cvj393j27bx4phga0zpbqni9fglqkx807fkb") (r "1.61")))

(define-public crate-instant-xml-macros-0.4.1 (c (n "instant-xml-macros") (v "0.4.1") (d (list (d (n "heck") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0b5mkm33pxfwyfyf19p5afw310hrckrdji4qfy337qwa7wzwxf19") (r "1.61")))

(define-public crate-instant-xml-macros-0.4.2 (c (n "instant-xml-macros") (v "0.4.2") (d (list (d (n "heck") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1wnvwgqmqjcqyqbbapa4h6753f2r1d2bp4bhwry3j97kwx71m4a6") (r "1.61")))

