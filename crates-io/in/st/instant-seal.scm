(define-module (crates-io in st instant-seal) #:use-module (crates-io))

(define-public crate-instant-seal-0.0.0 (c (n "instant-seal") (v "0.0.0") (h "1ah354sh7qn1wp9xv4abf4pjga5bpkjh9pzi77sfvsbjfw1x5dkl")))

(define-public crate-instant-seal-0.1.0 (c (n "instant-seal") (v "0.1.0") (d (list (d (n "client-traits") (r "^0.1.0") (d #t) (k 0)) (d (n "common-types") (r "^0.1.0") (d #t) (k 0)) (d (n "enjen") (r "^0.1.0") (d #t) (k 0)) (d (n "mashina") (r "^0.1.0") (d #t) (k 0)) (d (n "tetsy-keccak-hash") (r "^0.4.0") (d #t) (k 0)) (d (n "tetsy-rlp") (r "^0.4.5") (d #t) (k 2)) (d (n "vapcore-trace") (r "^0.1.0") (d #t) (k 0)) (d (n "vapjson") (r "^0.1.0") (d #t) (k 0)) (d (n "vapory-types") (r "^0.8.0") (d #t) (k 0)))) (h "122qylybcrndcrgvss8j2afnf0nhc9l3whnca15sx2fww45100n9")))

