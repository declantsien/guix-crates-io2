(define-module (crates-io in st instagram-hashtag-parser) #:use-module (crates-io))

(define-public crate-instagram-hashtag-parser-0.1.0 (c (n "instagram-hashtag-parser") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "hashtag") (r "^1.0") (o #t) (k 0)) (d (n "once_cell") (r "^1.8") (f (quote ("std"))) (o #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 2)) (d (n "regex") (r "^1.5") (f (quote ("std" "unicode"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "199sawz007v4l9phiwlhxs18jsfs0fg99js07smq5v2z7j443a96") (f (quote (("with-regex" "regex" "once_cell") ("with-crate-hashtag" "hashtag") ("default" "with-crate-hashtag"))))))

