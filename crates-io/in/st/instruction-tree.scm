(define-module (crates-io in st instruction-tree) #:use-module (crates-io))

(define-public crate-instruction-tree-0.1.0 (c (n "instruction-tree") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "simple-md-ul-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)) (d (n "tui-tree-widget") (r "^0.11.0") (d #t) (k 0)))) (h "1r6ivlxw964121xjj0rqmpj074ajzig42cyg2hfdmnc7gks8wrig")))

