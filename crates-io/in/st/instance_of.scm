(define-module (crates-io in st instance_of) #:use-module (crates-io))

(define-public crate-instance_of-0.0.4 (c (n "instance_of") (v "0.0.4") (d (list (d (n "wtest") (r "~0") (d #t) (k 2)))) (h "1mwl0md25p20pf2576sy4ai62kgglmili67ka2qds1brh6j2vb0l")))

(define-public crate-instance_of-0.0.6 (c (n "instance_of") (v "0.0.6") (d (list (d (n "wtest") (r "~0") (d #t) (k 2)))) (h "12cpbq9g3pml92c707apdfz5jjhmmm9srpf1rsamwl008ymgj19l")))

(define-public crate-instance_of-0.0.7 (c (n "instance_of") (v "0.0.7") (d (list (d (n "implements") (r "~0") (d #t) (k 0)) (d (n "wtest") (r "~0") (d #t) (k 2)))) (h "1h2nxy3zlp6jqbhk2wzwny1la114iclnk9jzqsrjpah4kgv2dadr")))

(define-public crate-instance_of-0.0.8 (c (n "instance_of") (v "0.0.8") (d (list (d (n "implements") (r "~0") (d #t) (k 0)) (d (n "wtest_basic") (r "~0") (d #t) (k 2)))) (h "1h3gb77cnl7xnpj6g7l66x975qsva0hnxlqy6yr4b1166r1z3f03")))

(define-public crate-instance_of-0.1.0 (c (n "instance_of") (v "0.1.0") (d (list (d (n "implements") (r "~0.1") (d #t) (k 0)) (d (n "wtest_basic") (r "~0.1") (d #t) (k 2)))) (h "01888kr2azl76qc7dhd6hh762sngk8xbisq5c874c9cdyvac4bih")))

