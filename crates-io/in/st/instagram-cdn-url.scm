(define-module (crates-io in st instagram-cdn-url) #:use-module (crates-io))

(define-public crate-instagram-cdn-url-0.1.0 (c (n "instagram-cdn-url") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1jpmnbayb5f8r0gsiqsfck681qa8z493fn3r1bfc0j7q49h90xqg")))

(define-public crate-instagram-cdn-url-0.1.1 (c (n "instagram-cdn-url") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0dvwkjvny59h5y7cqi4l5vxgmkf88c8k71h4ksmq27ls3hkf1isk")))

(define-public crate-instagram-cdn-url-0.1.2 (c (n "instagram-cdn-url") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1mcm8arbms1vx8rbg9wbmahc6n6xvnyh2ixxx8h1ci18wlnyvahj")))

(define-public crate-instagram-cdn-url-0.1.3 (c (n "instagram-cdn-url") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1j6n52v2awbr1856g6asfkgmlibg1cq446h43xdrmclszcm5mgky")))

(define-public crate-instagram-cdn-url-0.1.4 (c (n "instagram-cdn-url") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (k 0)) (d (n "url") (r "^2.2") (k 0)))) (h "0xxkagfjkxbh576cyyqvrqyabzh68l343hmq4b0yymr5xsv8hw2s")))

