(define-module (crates-io in sc inscriber9000) #:use-module (crates-io))

(define-public crate-inscriber9000-0.1.0 (c (n "inscriber9000") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "subxt") (r "^0.33.0") (f (quote ("jsonrpsee"))) (d #t) (k 0)) (d (n "subxt-signer") (r "^0.33.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "0y0nzv006p5c10rxww65q2bqmb6z1hr98igfycj4p4jib75iqlk6") (y #t)))

(define-public crate-inscriber9000-0.2.0 (c (n "inscriber9000") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive" "cargo" "color"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "subxt") (r "^0.33.0") (f (quote ("jsonrpsee"))) (d #t) (k 0)) (d (n "subxt-signer") (r "^0.33.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("macros" "rt" "rt-multi-thread" "sync"))) (d #t) (k 0)))) (h "0lkqq4lwij4q0r33xqadkbdd55sn96m774ap8adrfcirbgidiz6a") (y #t)))

