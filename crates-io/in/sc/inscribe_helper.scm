(define-module (crates-io in sc inscribe_helper) #:use-module (crates-io))

(define-public crate-inscribe_helper-0.1.0 (c (n "inscribe_helper") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bitcoin") (r "^0.30.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "ord") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)))) (h "0akqdd9z2qrlr9wps1w8138vs3dciizkjxq8080522f6rhifi301")))

