(define-module (crates-io in vi invisible_unicode) #:use-module (crates-io))

(define-public crate-invisible_unicode-0.1.0 (c (n "invisible_unicode") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "16gfvysp0v14a0yxnc7lvwh7dpdn4zkjd8fj0z7zr3qinqp9rd75")))

(define-public crate-invisible_unicode-1.0.0 (c (n "invisible_unicode") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0zq58fz8fsa0l73jdnfwmgwxyclgshss4xaq0294hdmii9wjjhg7")))

