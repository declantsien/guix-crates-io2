(define-module (crates-io in er inert) #:use-module (crates-io))

(define-public crate-inert-0.1.0 (c (n "inert") (v "0.1.0") (h "1b7z86jgd4pb6fq2splw8fpl74zi185wdlvvnchcw7q05gyqc9zn") (f (quote (("std"))))))

(define-public crate-inert-0.2.0 (c (n "inert") (v "0.2.0") (h "1mzf44m9k8b9ggr22drixj26ziaa5xyl6jvgwp0nfvycs57yl5bv") (f (quote (("std"))))))

(define-public crate-inert-0.2.1 (c (n "inert") (v "0.2.1") (h "1gnnmfwask8a64852svxrsmxixlmdrf0rcnrfvid5jixn7lk8z0d") (f (quote (("std"))))))

(define-public crate-inert-0.2.2 (c (n "inert") (v "0.2.2") (d (list (d (n "inert_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0kz1zv1c620v0xklm300nfdvbjwz39iqwamdjq9n2b8dqsv62rj9") (f (quote (("std"))))))

(define-public crate-inert-0.2.3 (c (n "inert") (v "0.2.3") (d (list (d (n "inert_derive") (r "^0.1.1") (d #t) (k 0)))) (h "1mimvqwaaxqdbk2h5zjlr8p4576bamvjy2y3cbdq61730fqzz1in") (f (quote (("std"))))))

(define-public crate-inert-0.2.4 (c (n "inert") (v "0.2.4") (d (list (d (n "inert_derive") (r "^0.1.3") (d #t) (k 0)))) (h "1iwdbgminvphkpmmd1w0sc08wj7z1mikaxg89vaw6cd1xz8qqali") (f (quote (("std"))))))

(define-public crate-inert-0.3.0 (c (n "inert") (v "0.3.0") (d (list (d (n "inert_derive") (r "^0.1.3") (d #t) (k 0)))) (h "10bgmxz9m4a4d1p05qljd0x34db68qqp3zfalxvncmnmqqkjizfc") (f (quote (("std"))))))

