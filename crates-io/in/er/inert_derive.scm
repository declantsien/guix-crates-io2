(define-module (crates-io in er inert_derive) #:use-module (crates-io))

(define-public crate-inert_derive-0.1.0 (c (n "inert_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "0gck1bp9njwhk5pvvbx2sjlcy140qabvfk4n66djv8i2nhbrj9m6")))

(define-public crate-inert_derive-0.1.1 (c (n "inert_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.27") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1kacrgq3q5pdl89fw7g34c5dgs9lrkw270h6nfnpl8bgwdchc40a")))

(define-public crate-inert_derive-0.1.2 (c (n "inert_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.27") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "09z0yf3faxnw9czn8kgdkzisnjbnfwa54z4msy6kwwb976jf8jgn")))

(define-public crate-inert_derive-0.1.3 (c (n "inert_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.27") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0baij7p6pgp4nvxihyagva3wh4qvf4c8mc4rqbl60dsccp0h28dd")))

(define-public crate-inert_derive-0.1.4 (c (n "inert_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.27") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0xpy8fpxg6fii04f6smiqq6xdzmnr9pch1z4vis9l70zsakyvbk5")))

