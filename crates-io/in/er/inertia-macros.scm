(define-module (crates-io in er inertia-macros) #:use-module (crates-io))

(define-public crate-inertia-macros-0.1.0 (c (n "inertia-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "060l6zd9mbjaw4kc58hlbk7wcb6xmva2gpykai8vmh0xjpafpkpm")))

