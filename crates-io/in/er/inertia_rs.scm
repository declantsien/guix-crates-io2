(define-module (crates-io in er inertia_rs) #:use-module (crates-io))

(define-public crate-inertia_rs-0.1.0 (c (n "inertia_rs") (v "0.1.0") (d (list (d (n "rocket") (r "^0.5.0-rc.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1v1n43jfca9k4h6v340lflf4vh6mx5h7amfpriv0cwi7js11ccb9")))

(define-public crate-inertia_rs-0.2.0 (c (n "inertia_rs") (v "0.2.0") (d (list (d (n "rocket") (r "^0.5.0-rc.1") (f (quote ("json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vrdx1ar1715h5mrv2x8anlq65w34f1f0775kq3wq3rcfax0rvs8") (f (quote (("default" "rocket"))))))

