(define-module (crates-io in er inertia-algebra) #:use-module (crates-io))

(define-public crate-inertia-algebra-0.1.0 (c (n "inertia-algebra") (v "0.1.0") (d (list (d (n "approx") (r "^0.3") (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0nzd643h8fmhzn1b5gdh7c29kkilblwc9p1z4gwqy3k489xxild6") (f (quote (("structures") ("std") ("default" "std" "structures"))))))

