(define-module (crates-io in kw inkwell_internals) #:use-module (crates-io))

(define-public crate-inkwell_internals-0.1.0 (c (n "inkwell_internals") (v "0.1.0") (d (list (d (n "cargo_toml") (r "^0.8") (d #t) (k 1)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 1)) (d (n "syn") (r "^0.15") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0arzyvjjdwwa006gsp6pvpbkrr7pmpy0mqqqhgliaqpqiyyl562m")))

(define-public crate-inkwell_internals-0.2.0 (c (n "inkwell_internals") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0kmlz8824ksj72h9a3kq2fl1sc95kb6vgr6c0j1b8schl3nz8b5j")))

(define-public crate-inkwell_internals-0.3.0 (c (n "inkwell_internals") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "04dx59c244va36qwp5abzdkrn5n4fj2yhfjkcbp59v6c609zgqf2") (f (quote (("nightly"))))))

(define-public crate-inkwell_internals-0.4.0 (c (n "inkwell_internals") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "12hswc8imxfa49a8cmxn4xn7hp60p4xah2wj63gxx7wr136r0l8q") (f (quote (("nightly"))))))

(define-public crate-inkwell_internals-0.5.0 (c (n "inkwell_internals") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1ya9zf1f12hq5q3jq9v1x1qcshfagg4vhxhrm352811h7npr0w1w") (f (quote (("nightly"))))))

(define-public crate-inkwell_internals-0.6.0 (c (n "inkwell_internals") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0i11y0jwbbab4ay56k445z7j872id34q263hq32ijwz8gs394csl") (f (quote (("nightly"))))))

(define-public crate-inkwell_internals-0.7.0 (c (n "inkwell_internals") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "11gah33af1bbidmx1pdy6xqyr24ijpzpngf2bfz05kk4w8bhrl47") (f (quote (("nightly"))))))

(define-public crate-inkwell_internals-0.8.0 (c (n "inkwell_internals") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1sgqira2mpbjdcah551rhl27a47hzgc19yifa08h90nnd38fg1di") (f (quote (("nightly"))))))

(define-public crate-inkwell_internals-0.9.0 (c (n "inkwell_internals") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1cl4qh5648f1jl25y1c5vrnsclrjcfgs5afs5a41l1438kbxi92g") (f (quote (("nightly"))))))

