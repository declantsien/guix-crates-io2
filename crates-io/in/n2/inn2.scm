(define-module (crates-io in n2 inn2) #:use-module (crates-io))

(define-public crate-inn2-0.1.6 (c (n "inn2") (v "0.1.6") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0p4smm4pwzs31iqralnwpibyg3318di2i4y0rdlm0wsjbg1ybbws") (y #t)))

(define-public crate-inn2-0.0.1 (c (n "inn2") (v "0.0.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0rl540q996nvx10mwxbvlnyskpbaka9fn04jxwhfyrlfid2dvvav") (y #t)))

(define-public crate-inn2-0.0.2 (c (n "inn2") (v "0.0.2") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0bya0q9mgsd9lrifnr24d4cqasna738grzz996qbpparaj4s7b6g") (y #t)))

