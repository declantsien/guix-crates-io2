(define-module (crates-io in py inpyt) #:use-module (crates-io))

(define-public crate-inpyt-0.1.0 (c (n "inpyt") (v "0.1.0") (h "0c0zij42cvgn6vkcpq4pqv0gk5jdwssnl44mwfckrvyflkz1vkwi")))

(define-public crate-inpyt-0.1.1 (c (n "inpyt") (v "0.1.1") (h "0dpwl37f1g1p6kbbplhnvc1743fhh9gxwc0q3m40cpmb312vs63c")))

(define-public crate-inpyt-0.1.2 (c (n "inpyt") (v "0.1.2") (h "156ls57nf505f5w4abz3786mdzl5n81gpdsc4lj63h56k5c1jfyb")))

(define-public crate-inpyt-0.1.3 (c (n "inpyt") (v "0.1.3") (h "08kpv2n4n941hcg18baxwai9183zgp8nv2pv47cy64p7l2ixpv0v")))

