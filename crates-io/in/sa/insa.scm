(define-module (crates-io in sa insa) #:use-module (crates-io))

(define-public crate-insa-0.1.0-dev (c (n "insa") (v "0.1.0-dev") (d (list (d (n "fontdue") (r "^0.7") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)))) (h "19pjvms907ah0g3a4pc17cy68rv6bh6j51arrx9ddx3lsyy6qjvi")))

(define-public crate-insa-1.0.0 (c (n "insa") (v "1.0.0") (d (list (d (n "fontdue") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)))) (h "1maqlpir7w5p4vsy94lk1mv2y28sy0j1p696sq7vf3hb1cjmkwyk") (f (quote (("default" "fontdue"))))))

