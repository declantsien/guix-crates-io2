(define-module (crates-io in oh inohashmap) #:use-module (crates-io))

(define-public crate-inohashmap-0.1.0 (c (n "inohashmap") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 2)) (d (n "vint32") (r "^0.1.0") (d #t) (k 0)))) (h "0cdmlb4vb7xxash6zyvng8ry0k2r809s8pfqrk46d62zrbrrjj7h")))

(define-public crate-inohashmap-0.1.1 (c (n "inohashmap") (v "0.1.1") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 2)) (d (n "vint32") (r "^0.1.0") (d #t) (k 0)))) (h "1q2kfn702l110mja63vr591xnwkhdavdbkzqxfff71j9vvr0lrj2")))

(define-public crate-inohashmap-0.2.0 (c (n "inohashmap") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 2)) (d (n "vint32") (r "^0.1.0") (d #t) (k 0)))) (h "0fgbhrib3axczqlkmaiyv8br00qfwv2fs2zsld0wdn6rwy26y501")))

(define-public crate-inohashmap-0.2.1 (c (n "inohashmap") (v "0.2.1") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 2)) (d (n "vint32") (r "^0.1.0") (d #t) (k 0)))) (h "1gazvhdsxgp3gnl73l2vp3nbdd5kh79afvaf1cv05pykvws37i8h")))

(define-public crate-inohashmap-0.3.0 (c (n "inohashmap") (v "0.3.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 2)) (d (n "vint32") (r "^0.1.0") (d #t) (k 0)))) (h "063vnjzpzg3chr16qgarbzv2nhb1lm57dvfzmdyni9jm14s78mb0")))

