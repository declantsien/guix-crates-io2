(define-module (crates-io in ti intistelecom-rs) #:use-module (crates-io))

(define-public crate-intistelecom-rs-0.1.0 (c (n "intistelecom-rs") (v "0.1.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "025grqjsj9xc8mjvkpzppsyc40zhg8wglqmjzcl4x1v403sr10cf") (y #t)))

(define-public crate-intistelecom-rs-0.1.1 (c (n "intistelecom-rs") (v "0.1.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "02rqh05cj5mzc8yg8324d3bdm8hgcn965jkfvras6xhbzylyj13v") (y #t)))

(define-public crate-intistelecom-rs-0.1.2 (c (n "intistelecom-rs") (v "0.1.2") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03rln1v9dbfzc51g9w6bbl9711db0mbkjgbkv83c438ax853xxxr")))

