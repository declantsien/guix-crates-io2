(define-module (crates-io in ti intility_cloud_lib) #:use-module (crates-io))

(define-public crate-intility_cloud_lib-0.1.0 (c (n "intility_cloud_lib") (v "0.1.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "17mz8xv6g40aq6c4z9m9dkr50p2rv2w58lyahhdswb4jc590q4y6")))

(define-public crate-intility_cloud_lib-0.1.1 (c (n "intility_cloud_lib") (v "0.1.1") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "1fyvvzsw0d4xhjpg7hyddh6p8j4mc8d6d49nkp92jc228fq6chss")))

(define-public crate-intility_cloud_lib-0.2.0 (c (n "intility_cloud_lib") (v "0.2.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (d #t) (k 2)) (d (n "strum_macros") (r "^0.21") (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "071ns1mq1izlvyvbkvncj3n4z7p610z4m8ix7vb2dxh3cng4xga3")))

(define-public crate-intility_cloud_lib-0.3.0 (c (n "intility_cloud_lib") (v "0.3.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (d #t) (k 2)) (d (n "strum_macros") (r "^0.21") (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "0ps0c1c8nhfnplizlj7n364zlqra5yw6i8lg15r8v96qmdhyxp7h")))

(define-public crate-intility_cloud_lib-0.4.0 (c (n "intility_cloud_lib") (v "0.4.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (d #t) (k 2)) (d (n "strum_macros") (r "^0.21") (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "0clia97h9dw7lr38k3r48a51b575bfymllf6j7fgj9rc0mn1gc1l")))

(define-public crate-intility_cloud_lib-0.5.0 (c (n "intility_cloud_lib") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (d #t) (k 2)) (d (n "strum_macros") (r "^0.21") (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (d #t) (k 0)))) (h "06llmlf07zymacwwxx0zpymbhm8k1g0fa3wsfkpafqr5dvmk94dw")))

(define-public crate-intility_cloud_lib-0.6.0 (c (n "intility_cloud_lib") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "snafu") (r "^0.7") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 2)) (d (n "strum_macros") (r "^0.24") (d #t) (k 2)) (d (n "uuid") (r "^1.0.0") (d #t) (k 0)))) (h "0w7wrfvakx7xzk89k74ylic42f70f7clmx5lbn1smbs62wz6362y")))

(define-public crate-intility_cloud_lib-0.7.0 (c (n "intility_cloud_lib") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "snafu") (r "^0.7") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 2)) (d (n "strum_macros") (r "^0.24") (d #t) (k 2)) (d (n "uuid") (r "^1.0.0") (d #t) (k 0)))) (h "19d5wd4aln8zvsvl1w6nx9zbycnawpgfxalcfy2vsj9hji6bz0ay")))

(define-public crate-intility_cloud_lib-0.8.0 (c (n "intility_cloud_lib") (v "0.8.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "snafu") (r "^0.7") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 2)) (d (n "strum_macros") (r "^0.24") (d #t) (k 2)) (d (n "uuid") (r "^1.0.0") (d #t) (k 0)))) (h "0brn2md4snbrbyw9yvp1zacdzkd68j6l2c39cf2ww5z5vpga876r")))

