(define-module (crates-io in ti inti25_rust_lib1) #:use-module (crates-io))

(define-public crate-inti25_rust_lib1-0.0.1 (c (n "inti25_rust_lib1") (v "0.0.1") (h "1985jvq98wky7rl43pcskhxbsw91nrjyf8yb56cncz9scpd5yxg8")))

(define-public crate-inti25_rust_lib1-0.0.2 (c (n "inti25_rust_lib1") (v "0.0.2") (d (list (d (n "inti25_solana_lib") (r "^0.0.1") (d #t) (k 0)))) (h "1lr4hszg19bdfb0pqpwcgad0wwp2dmrfsa3jkqj87nacwdn2ar01")))

