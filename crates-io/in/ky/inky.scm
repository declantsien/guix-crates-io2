(define-module (crates-io in ky inky) #:use-module (crates-io))

(define-public crate-inky-0.1.0 (c (n "inky") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rppal") (r "^0.14.1") (d #t) (k 0)))) (h "0ggi4jg6d11m3rnv5bvgvfdyz8q3m81c9fd3cyri7px1amy11yy5")))

