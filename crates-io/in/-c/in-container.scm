(define-module (crates-io in -c in-container) #:use-module (crates-io))

(define-public crate-in-container-1.0.0-rc.1 (c (n "in-container") (v "1.0.0-rc.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "sysctl") (r "^0.4.0") (d #t) (t "x86_64-unknown-freebsd") (k 0)) (d (n "windows-service") (r "^0.1.0") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winreg") (r "^0.7.0") (d #t) (t "x86_64-pc-windows-gnu") (k 0)))) (h "0v7fqfq1yypq2lqw8vcgqsx5gbd67zm3akskhd4hb5lw1b97c6ka")))

(define-public crate-in-container-1.0.0 (c (n "in-container") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "sysctl") (r "^0.4.0") (d #t) (t "x86_64-unknown-freebsd") (k 0)) (d (n "windows-service") (r "^0.3.0") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winreg") (r "^0.7.0") (d #t) (t "x86_64-pc-windows-gnu") (k 0)))) (h "1hgxfmja6rlx3vr4xpmhk5hy6vavn0iwr40p0wp3lskmxl8sa369")))

(define-public crate-in-container-1.1.0 (c (n "in-container") (v "1.1.0") (d (list (d (n "clap") (r "^3.0.13") (f (quote ("cargo" "derive"))) (o #t) (d #t) (k 0)) (d (n "sysctl") (r "^0.4.3") (d #t) (t "x86_64-unknown-freebsd") (k 0)) (d (n "windows-service") (r "^0.4.0") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winreg") (r "^0.10.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)))) (h "1ca5zgw1qaib5lxdmplnsly3h2q2cjdqc8wjkkqsc1kbxnkmd089") (f (quote (("exe" "clap") ("default" "exe"))))))

