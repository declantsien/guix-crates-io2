(define-module (crates-io in to into_ext) #:use-module (crates-io))

(define-public crate-into_ext-0.1.0 (c (n "into_ext") (v "0.1.0") (h "19v5q7hlxircl60d4l6axvl9hcgp3mq3ryk6j9h3223jcj43rbnv")))

(define-public crate-into_ext-0.1.1 (c (n "into_ext") (v "0.1.1") (h "1f6bazd0hyjq1a6n2pcvli7v2256yaxyc9s9ijk4ldavic64yxwa")))

(define-public crate-into_ext-0.1.2 (c (n "into_ext") (v "0.1.2") (h "1dnvx4j0xk8c2gqwnsxgfnd26lcngsa1r7bhyc29icii2kcqsr6y")))

