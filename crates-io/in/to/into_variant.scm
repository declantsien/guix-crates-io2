(define-module (crates-io in to into_variant) #:use-module (crates-io))

(define-public crate-into_variant-0.1.0 (c (n "into_variant") (v "0.1.0") (d (list (d (n "into_variant_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.59") (d #t) (k 2)))) (h "0f87sfsnb721ymcimmfg79a6hkjvpynpfsk6a7y5nawfjh2bxnw2")))

(define-public crate-into_variant-0.1.1 (c (n "into_variant") (v "0.1.1") (d (list (d (n "into_variant_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.59") (d #t) (k 2)))) (h "0pcvzns0ksh66d6ryilqpkc7h7l1j6kwr3mlfz07s1rl17xg1mg4")))

(define-public crate-into_variant-0.2.0 (c (n "into_variant") (v "0.2.0") (d (list (d (n "into_variant_macro") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.59") (d #t) (k 2)))) (h "1axivw9bgh4hkp3y3nwd0xmvfa6pgv9z9h8ilsl52j0kg5c7igxj")))

(define-public crate-into_variant-0.3.0 (c (n "into_variant") (v "0.3.0") (d (list (d (n "into_variant_macro") (r "^0.3.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.59") (d #t) (k 2)))) (h "0fgfbxn5a0xpi8fak96f5liz4p8fq8r32lmw6rkkp68nzg8d0k3j")))

