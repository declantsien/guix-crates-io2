(define-module (crates-io in to into_query) #:use-module (crates-io))

(define-public crate-into_query-0.1.0 (c (n "into_query") (v "0.1.0") (d (list (d (n "diesel") (r "^1.4.5") (f (quote ("mysql"))) (d #t) (k 0)) (d (n "into_query_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0hp14vbzi7iry3nwcijz86ycg5d9m95j7hkgpfj18shdfhl708l2") (f (quote (("derive" "into_query_derive") ("default"))))))

(define-public crate-into_query-0.2.0 (c (n "into_query") (v "0.2.0") (d (list (d (n "diesel") (r "^1.4.5") (f (quote ("mysql"))) (d #t) (k 0)) (d (n "into_query_derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0gaxin6gwdlmqv8gn7afxdw181i02r408lqxcpq1n29biihaaqdw") (f (quote (("derive" "into_query_derive") ("default"))))))

(define-public crate-into_query-0.2.1 (c (n "into_query") (v "0.2.1") (d (list (d (n "diesel") (r "^1.4.5") (d #t) (k 0)) (d (n "into_query_derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0pwpgpq34mqxam1b184y44gmdvigr295hf8ik9bsgmfsfpkp7ph5") (f (quote (("sqlite" "diesel/sqlite" "into_query_derive/sqlite") ("postgres" "diesel/postgres" "into_query_derive/postgres") ("mysql" "diesel/mysql" "into_query_derive/mysql") ("default" "diesel/mysql" "into_query_derive"))))))

(define-public crate-into_query-0.2.3 (c (n "into_query") (v "0.2.3") (d (list (d (n "diesel") (r "^1.4.5") (d #t) (k 0)) (d (n "into_query_derive") (r "^0.2.2") (o #t) (d #t) (k 0)))) (h "0jvpjyzlzfd234fgf0cs6vbc1crnbbg8gsnn04g5giymw214fyy4") (f (quote (("sqlite" "diesel/sqlite" "into_query_derive/sqlite") ("postgres" "diesel/postgres" "into_query_derive/postgres") ("mysql" "diesel/mysql" "into_query_derive/mysql"))))))

(define-public crate-into_query-0.2.4 (c (n "into_query") (v "0.2.4") (d (list (d (n "diesel") (r "^1.4.5") (d #t) (k 0)) (d (n "into_query_derive") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "0bxaijazkfynya0xdbx6abp7cr0c24y6h4k19i7s31b366h8qqkw") (f (quote (("sqlite" "diesel/sqlite" "into_query_derive/sqlite") ("postgres" "diesel/postgres" "into_query_derive/postgres") ("mysql" "diesel/mysql" "into_query_derive/mysql"))))))

