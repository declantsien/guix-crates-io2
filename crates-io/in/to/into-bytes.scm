(define-module (crates-io in to into-bytes) #:use-module (crates-io))

(define-public crate-into-bytes-0.1.0 (c (n "into-bytes") (v "0.1.0") (d (list (d (n "impl-for") (r "^0.1.0") (d #t) (k 0)) (d (n "itoa") (r "^1.0") (d #t) (k 0)) (d (n "ryu") (r "^1.0") (d #t) (k 0)))) (h "00ll3iqrs0j5sy3sfz76hk3lkmixlqy2xy417x3py3c7aw2icz78")))

(define-public crate-into-bytes-0.1.1 (c (n "into-bytes") (v "0.1.1") (d (list (d (n "impl-for") (r "^0.2") (d #t) (k 0)) (d (n "itoa") (r "^1.0") (d #t) (k 0)) (d (n "ryu") (r "^1.0") (d #t) (k 0)))) (h "1yrd482zi1ibyiarhayq8cp8xvb8by7yx90zrn2s0cpbjij9nmxi")))

