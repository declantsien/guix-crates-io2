(define-module (crates-io in to into_variant_macro) #:use-module (crates-io))

(define-public crate-into_variant_macro-0.1.0 (c (n "into_variant_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full"))) (d #t) (k 0)))) (h "00c0cjwkr51m5qnwfydrkicb7dcspaqxfsy0d2x3yinqvf6a7ing")))

(define-public crate-into_variant_macro-0.2.0 (c (n "into_variant_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full"))) (d #t) (k 0)))) (h "1zs7h9wn17f3nfbw3hckr0fxm57fg1bc3ad31pfrbh5vc66zjmcz")))

(define-public crate-into_variant_macro-0.3.0 (c (n "into_variant_macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full"))) (d #t) (k 0)))) (h "0cv5p43xs14mnf7m0bi3iwsw6a3bwl8vxfm55afja10ap2iv6gxs")))

