(define-module (crates-io in to into-a-byte) #:use-module (crates-io))

(define-public crate-into-a-byte-1.0.0 (c (n "into-a-byte") (v "1.0.0") (h "14pj2hwynfsmyzb77laqbjyfd7d694p5qsxzx6s0ildw0hyrcmg5")))

(define-public crate-into-a-byte-1.0.1 (c (n "into-a-byte") (v "1.0.1") (h "02pp7xb3dn24r2bhckqy7dgnf6grg2s239y9jx4g2i3d8pwqirv0")))

