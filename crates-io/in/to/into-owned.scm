(define-module (crates-io in to into-owned) #:use-module (crates-io))

(define-public crate-into-owned-0.1.0 (c (n "into-owned") (v "0.1.0") (h "1wpwjkk1gj4vahsgfk79pc62hf14d7b5z60xqw3il0g9f07s8sap") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-into-owned-0.2.0 (c (n "into-owned") (v "0.2.0") (h "01k8hbrdjrlhqmynf1czx4sb1k22rk5049in4ds7vdw0igwk2cpm") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-into-owned-0.2.1 (c (n "into-owned") (v "0.2.1") (h "1pm217bn70m530d9x7al1k433ix16j354xgi79y75z4vbxl9nkza") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-into-owned-0.2.2 (c (n "into-owned") (v "0.2.2") (h "0s11nf47wpbglhffb14bk7kff2xp33acchpps5qxa8slx8mmhm8w") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-into-owned-0.3.0-alpha (c (n "into-owned") (v "0.3.0-alpha") (h "1xcqzlw67v1dmx9q2597magyanzzjyvr0ravl99v99wqhyp1wwf5") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-into-owned-0.3.0-alpha.2 (c (n "into-owned") (v "0.3.0-alpha.2") (h "14m6lf7ps1f23vpc0ghyc8nz6x3sy7ybn2pl1109rn6qczaxrdqx") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-into-owned-0.3.0-alpha.3 (c (n "into-owned") (v "0.3.0-alpha.3") (h "02kpncrhbb0qwc8x25mh4adkkniky3qs56cgi972w0a70kf9k9s1") (f (quote (("std") ("default" "std")))) (y #t)))

