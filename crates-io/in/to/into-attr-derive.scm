(define-module (crates-io in to into-attr-derive) #:use-module (crates-io))

(define-public crate-into-attr-derive-0.1.0 (c (n "into-attr-derive") (v "0.1.0") (d (list (d (n "dot-generator") (r "^0.1.0") (d #t) (k 0)) (d (n "dot-structures") (r "^0.1.0") (d #t) (k 0)) (d (n "into-attr") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0i9i1i7kh76ci3rz6ldmii3rzi84p6h6kbil160sf6i7hk3c723b")))

(define-public crate-into-attr-derive-0.2.0 (c (n "into-attr-derive") (v "0.2.0") (d (list (d (n "dot-generator") (r "^0.2.0") (d #t) (k 0)) (d (n "dot-structures") (r "^0.1.0") (d #t) (k 0)) (d (n "into-attr") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0api0mxxp1kb3ibjdg0qhgx1xbvfsvkmxjwsipa3cfq00knz7yzq")))

(define-public crate-into-attr-derive-0.2.1 (c (n "into-attr-derive") (v "0.2.1") (d (list (d (n "dot-generator") (r "^0.2.0") (d #t) (k 0)) (d (n "dot-structures") (r "^0.1.1") (d #t) (k 0)) (d (n "into-attr") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cfmf1fink7504iim0vaq9igylkzvj5il1nichx6lb6dwqd7rb7c")))

