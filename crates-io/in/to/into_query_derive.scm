(define-module (crates-io in to into_query_derive) #:use-module (crates-io))

(define-public crate-into_query_derive-0.1.0 (c (n "into_query_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "extra-traits"))) (d #t) (k 0)))) (h "0369dx1cdm4vqj2qp5hf5hy1vnr8gvyghfvfcnrqqjjwy95w76qc")))

(define-public crate-into_query_derive-0.1.1 (c (n "into_query_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "extra-traits"))) (d #t) (k 0)))) (h "1fnk9c7br3bk9jshs2k56jdxwqg0bcg95f4mdj6wrccmk9z5ggj8")))

(define-public crate-into_query_derive-0.2.0 (c (n "into_query_derive") (v "0.2.0") (d (list (d (n "diesel") (r "^1.4.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "extra-traits"))) (d #t) (k 0)))) (h "1mgwq1q1cczfil6ldfi4m26wdy4bzcp7m3mmyqlkqrb8a0w4gbb5") (f (quote (("sqlite" "diesel/sqlite") ("postgres" "diesel/postgres") ("mysql" "diesel/mysql") ("default" "mysql"))))))

(define-public crate-into_query_derive-0.2.2 (c (n "into_query_derive") (v "0.2.2") (d (list (d (n "diesel") (r "^1.4.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "extra-traits"))) (d #t) (k 0)))) (h "01xwpbrz9cnasf56wvg5j6jg3nls4wyc0y5m9spiqd8phmigqbcd") (f (quote (("sqlite" "diesel/sqlite") ("postgres" "diesel/postgres") ("mysql" "diesel/mysql"))))))

(define-public crate-into_query_derive-0.2.3 (c (n "into_query_derive") (v "0.2.3") (d (list (d (n "diesel") (r "^1.4.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro" "extra-traits"))) (d #t) (k 0)))) (h "1mmic6ycmqifcy0g3alfs8qk7qa4mndkmpq694d3kgi20ndkn7zy") (f (quote (("sqlite" "diesel/sqlite") ("postgres" "diesel/postgres") ("mysql" "diesel/mysql"))))))

