(define-module (crates-io in to into-result) #:use-module (crates-io))

(define-public crate-into-result-0.1.0 (c (n "into-result") (v "0.1.0") (h "0psbv7870vk7icfzrpffbrl2q5wczxsbkvjk6lpnz1m1qisbabi5") (f (quote (("std") ("default" "command" "std") ("command"))))))

(define-public crate-into-result-0.1.1 (c (n "into-result") (v "0.1.1") (h "1ckv75ca0cwkxd0wpx205438b3cbgvgwph167k9s20bg0ry1snks") (f (quote (("std") ("default" "command" "std") ("command"))))))

(define-public crate-into-result-0.2.0 (c (n "into-result") (v "0.2.0") (h "148y79hq6hzg760gh6nypcr9fv718q1nkibkccsncdr216b8bh60") (f (quote (("std") ("default" "command" "std") ("command"))))))

(define-public crate-into-result-0.3.0 (c (n "into-result") (v "0.3.0") (h "1ykvq7dx01zj5z9d2ywps23cslpcsdmad5zih2y3j56632la3s9y") (f (quote (("std") ("default" "command" "std") ("command"))))))

(define-public crate-into-result-0.3.1 (c (n "into-result") (v "0.3.1") (h "1yxrddxlj8wn02ky963q43d6skcbjya7ya8lp14p64scpn8k0i3a") (f (quote (("std") ("default" "command" "std") ("command"))))))

