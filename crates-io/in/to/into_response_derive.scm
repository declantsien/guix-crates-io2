(define-module (crates-io in to into_response_derive) #:use-module (crates-io))

(define-public crate-into_response_derive-0.1.0 (c (n "into_response_derive") (v "0.1.0") (d (list (d (n "axum") (r "^0.4.5") (d #t) (k 0)) (d (n "hyper") (r "^0.14.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "1c1y63lfij81kmjnc4vzj32an4810nb3ni29jkyf0b8pg6v680yy")))

(define-public crate-into_response_derive-0.2.0 (c (n "into_response_derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "1lmbyh3zs4w69zbbcvycbgaqg222yhkpys57vnd5zxmnrncp26kc")))

