(define-module (crates-io in to intovec) #:use-module (crates-io))

(define-public crate-intovec-0.0.1 (c (n "intovec") (v "0.0.1") (h "0zc7izwrqihbkdan7fc9ycxpykza6xsl1nqzl9ck451c23famh79")))

(define-public crate-intovec-0.0.2 (c (n "intovec") (v "0.0.2") (h "0xqixr8g3zbdzdxzy0ij3hwg72q651k1n9nk5nkwrgqyvb73rbpa")))

(define-public crate-intovec-0.0.3 (c (n "intovec") (v "0.0.3") (h "0fgsjmjfdndpwmx0yg7wm7njgj6scfk8ajcx7d49j52yzkx5mn3g")))

(define-public crate-intovec-0.0.4 (c (n "intovec") (v "0.0.4") (h "1mw3jgg9fhpsr5c8gj31hl3pfajj307f32yhqq86yczxpc7p4dww")))

(define-public crate-intovec-0.0.5 (c (n "intovec") (v "0.0.5") (h "1aqj686q3n9y5phbdjk7zppvrb078kacmp4m1ks80mpbsd62f9sv")))

(define-public crate-intovec-0.0.6 (c (n "intovec") (v "0.0.6") (h "051g7014l26xkba4nkxfmxrcqr31h6f4yq779wxgz8g3hb7szw72")))

