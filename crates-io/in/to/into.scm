(define-module (crates-io in to into) #:use-module (crates-io))

(define-public crate-into-0.1.0 (c (n "into") (v "0.1.0") (h "0i11qibwcp0c22aq08cv5xkhdxif0si7k6y7babya77fgdkyflpc")))

(define-public crate-into-0.0.0 (c (n "into") (v "0.0.0") (h "0m8j3a12b3zmhiws87aq57b074b8ikqznvpy938rfgx70gdqk4cm")))

