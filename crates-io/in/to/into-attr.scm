(define-module (crates-io in to into-attr) #:use-module (crates-io))

(define-public crate-into-attr-0.1.0 (c (n "into-attr") (v "0.1.0") (d (list (d (n "dot-structures") (r "^0.1.0") (d #t) (k 0)))) (h "1sb84k6bz41m4isf9x7xrg5whshmzf28k0bkvnajcpf093c0vv66")))

(define-public crate-into-attr-0.1.1 (c (n "into-attr") (v "0.1.1") (d (list (d (n "dot-structures") (r "^0.1.1") (d #t) (k 0)))) (h "0xmkz2rq6hh2fmva5sm1c538b9nvlx9kgv6ag3k0k9s9gr9qrd0q")))

