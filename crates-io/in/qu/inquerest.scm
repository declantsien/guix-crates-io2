(define-module (crates-io in qu inquerest) #:use-module (crates-io))

(define-public crate-inquerest-0.0.1 (c (n "inquerest") (v "0.0.1") (d (list (d (n "peg") (r "^0.3.3") (d #t) (k 0)))) (h "0rr18x3c644lzgv9f0smlwy6rnqipn1brp3zk5izf77if67fxlxn")))

(define-public crate-inquerest-0.0.2 (c (n "inquerest") (v "0.0.2") (d (list (d (n "peg") (r "^0.3.4") (d #t) (k 0)))) (h "1j8v0ib2r0na71mxda0qydwij35q1mivyk16hwzwp5jkdyv4nxvj")))

(define-public crate-inquerest-0.0.3 (c (n "inquerest") (v "0.0.3") (d (list (d (n "peg") (r "^0.3.4") (d #t) (k 0)))) (h "1wa5hy9q4sinim759zrdlyg8pkgfn44k4pkkil7i1zl4al08m6mq")))

(define-public crate-inquerest-0.0.4 (c (n "inquerest") (v "0.0.4") (d (list (d (n "peg") (r "^0.3.4") (d #t) (k 0)))) (h "1nm21iirf78hlmq4ad11gzgcm5whydjgyzs2blb583pyp7r8y21y")))

(define-public crate-inquerest-0.0.5 (c (n "inquerest") (v "0.0.5") (d (list (d (n "peg") (r "^0.3.4") (d #t) (k 0)))) (h "1q91jmwi20904xg6bvxpwpdclh098yxs3r0xpfslbrzk6ca9ya62")))

(define-public crate-inquerest-0.0.6 (c (n "inquerest") (v "0.0.6") (d (list (d (n "peg") (r "^0.3.4") (d #t) (k 0)))) (h "1xj2ripc3kyqb28xqplz51m5cy8msyi49qy9nvfhclf7kpy6d6bq")))

(define-public crate-inquerest-0.0.7 (c (n "inquerest") (v "0.0.7") (d (list (d (n "peg") (r "^0.3.6") (d #t) (k 0)))) (h "0vkdig3q23g1xf52vdxhi1hzjpq83pl19n5h6n5v4ss2av5qjkzl")))

(define-public crate-inquerest-0.0.8 (c (n "inquerest") (v "0.0.8") (d (list (d (n "peg") (r "^0.3.6") (d #t) (k 0)))) (h "10wgai7jc63mxs2x6x83fbdm5jsd8ifcq65qvy522fk1sbi4mqjb")))

(define-public crate-inquerest-0.0.9 (c (n "inquerest") (v "0.0.9") (d (list (d (n "peg") (r "^0.3") (d #t) (k 0)))) (h "1j7k7v42pbj0xxpafzcgy64cdjq3806y73ffphglrhfk075nxi3m")))

(define-public crate-inquerest-0.1.0 (c (n "inquerest") (v "0.1.0") (d (list (d (n "peg") (r "^0.3") (d #t) (k 0)))) (h "0pkla7ryxdrzjcm8dz7gfa224jqvjz173clibkxpl1jsj6cj1qx2")))

(define-public crate-inquerest-0.1.1 (c (n "inquerest") (v "0.1.1") (d (list (d (n "peg") (r "^0.3") (d #t) (k 0)))) (h "1g7vjliskpmg7r3djhpl2yjzqnv4fycz6gx7bzalc4w0jnb6qrjh")))

(define-public crate-inquerest-0.1.2 (c (n "inquerest") (v "0.1.2") (d (list (d (n "peg") (r "^0.3") (d #t) (k 0)))) (h "0wrhhnnghyfc961h1a58wg3xmw3ca3a0zc9m98nczqdz09iyska8")))

(define-public crate-inquerest-0.1.3 (c (n "inquerest") (v "0.1.3") (d (list (d (n "peg") (r "^0.3.18") (d #t) (k 0)))) (h "1d389pnbk40rz8rs9iars1b8c1yf6zj642z1rl491bvnraj1iqgs")))

(define-public crate-inquerest-0.1.4 (c (n "inquerest") (v "0.1.4") (d (list (d (n "peg") (r "^0.3.18") (d #t) (k 0)))) (h "0c0mdcnk0smkifbg1q1kwjxgfkk3kmyd98yz44dn24m5wcbkfx99")))

(define-public crate-inquerest-0.1.5 (c (n "inquerest") (v "0.1.5") (d (list (d (n "peg") (r "^0.3.18") (d #t) (k 0)))) (h "09gbsr01ws8xrg4nfiv3v1svx4iax5psqcn702k823wv2hzrvcas")))

(define-public crate-inquerest-0.2.0 (c (n "inquerest") (v "0.2.0") (d (list (d (n "nom") (r "^2") (d #t) (k 0)))) (h "0jk83iz7d4la33b6ijpnxs4an5raaq23b3gmcbfps8ibxyybdg96")))

(define-public crate-inquerest-0.2.1 (c (n "inquerest") (v "0.2.1") (d (list (d (n "nom") (r "^2") (d #t) (k 0)))) (h "1pvimdbng5f0fhxsi3icliadh441zmcbas99sz2ma0wxjaqlcwp5")))

(define-public crate-inquerest-0.2.2 (c (n "inquerest") (v "0.2.2") (d (list (d (n "nom") (r "^2") (d #t) (k 0)))) (h "06k838hqf4klmh82wc230vhjj2r9f3s88rc46zg9di3vaja1rx04")))

(define-public crate-inquerest-0.3.0 (c (n "inquerest") (v "0.3.0") (d (list (d (n "restq") (r "^0.1.0") (d #t) (k 0)) (d (n "restq-http") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "01swq1jiv4v1ddbp0pfwkn4vlmjx9zlykvyhd7c9swn0xl5pjxpi")))

(define-public crate-inquerest-0.3.1 (c (n "inquerest") (v "0.3.1") (d (list (d (n "restq") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0fbbcqf1qibyf95zxajrb9hxskv98nmvb9aps2zm91c9bqg001mv")))

