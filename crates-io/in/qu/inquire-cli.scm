(define-module (crates-io in qu inquire-cli) #:use-module (crates-io))

(define-public crate-inquire-cli-0.1.0 (c (n "inquire-cli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "inquire") (r "^0.2.1") (f (quote ("termion" "date" "editor" "chrono"))) (k 0)) (d (n "linked-hash-map") (r "^0.5.4") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0fqjdxwsjgbcq4808q3dyncamxdjanzhjr55fazz3ny10qf86zcd")))

