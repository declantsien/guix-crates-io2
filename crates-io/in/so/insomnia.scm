(define-module (crates-io in so insomnia) #:use-module (crates-io))

(define-public crate-insomnia-0.1.0-alpha1 (c (n "insomnia") (v "0.1.0-alpha1") (d (list (d (n "dbus") (r "^0.8") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "enumset") (r "^0.4.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("errhandlingapi" "impl-default" "handleapi" "winbase" "winnt"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "12djdkm3zhiygb0adkpb2x102xi71368ry6nsfpigakrp07z9p0p")))

(define-public crate-insomnia-0.1.0-alpha2 (c (n "insomnia") (v "0.1.0-alpha2") (d (list (d (n "dbus") (r "^0.9") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "wasmer_enumset") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("errhandlingapi" "impl-default" "handleapi" "minwinbase" "minwindef" "winbase" "winnt"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1af6fi1hdbjgbdi6sghas434p2vzw6snp3mqfn0xdsh06g8zd3w9")))

(define-public crate-insomnia-0.1.0 (c (n "insomnia") (v "0.1.0") (d (list (d (n "dbus") (r "^0.9") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "enumset") (r "^1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("errhandlingapi" "impl-default" "handleapi" "minwinbase" "minwindef" "winbase" "winnt"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1qn52wr93w0126rzvrkgq9bf0zkripjx8l9q93zhbs3z8crxr5zs")))

