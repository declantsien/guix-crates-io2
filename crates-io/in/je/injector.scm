(define-module (crates-io in je injector) #:use-module (crates-io))

(define-public crate-injector-0.0.1 (c (n "injector") (v "0.0.1") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1fr8bcavpyl29h2x4pr7mlwl4yznwpx2ljqk1kpfpb572ah4wxh3")))

