(define-module (crates-io in je injector-sys) #:use-module (crates-io))

(define-public crate-injector-sys-0.1.0 (c (n "injector-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "1fm5wm9b9h3x61bdf2nzhkx89rq8kkjnqnjwbh1mwy8n9jlvrqns") (r "1.56")))

(define-public crate-injector-sys-0.1.1 (c (n "injector-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "0a2v0zgg2zkkcpxb5gjzrlzp0k7fb7h3hdjczqwwd9yjvzyp9hcw") (r "1.56")))

