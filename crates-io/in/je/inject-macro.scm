(define-module (crates-io in je inject-macro) #:use-module (crates-io))

(define-public crate-inject-macro-0.1.0 (c (n "inject-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full"))) (d #t) (k 0)))) (h "1pwyk2xglw62jakcdp2hmzlk6rb6jr8l702kpl3k6jrsdrq0bw19")))

(define-public crate-inject-macro-0.1.1 (c (n "inject-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.17") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (f (quote ("full"))) (d #t) (k 0)))) (h "1ra14s3kwryr75v08zc0v7lyw173073179b9nndpkgs677x30y9j")))

