(define-module (crates-io in je inject) #:use-module (crates-io))

(define-public crate-inject-0.1.0 (c (n "inject") (v "0.1.0") (d (list (d (n "inject-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6.3") (d #t) (k 2)))) (h "08nycl6yykjlgb9zbdjlzsg26v5ls1i9xlhdgp5hb7xp8xbimygc")))

(define-public crate-inject-0.1.1 (c (n "inject") (v "0.1.1") (d (list (d (n "inject-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6.3") (d #t) (k 2)))) (h "03ajb3lv21bb35riy6w9j3pimgvmnmkmyfy9pnmx567bxwxixqaw")))

(define-public crate-inject-0.1.2 (c (n "inject") (v "0.1.2") (d (list (d (n "inject-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "rstest") (r "^0.6.3") (d #t) (k 2)))) (h "1g1g14cf5dhh8sw4yyiyn42lw396dj0vriq7nk5lg1p1bidnp1wd")))

(define-public crate-inject-0.1.3 (c (n "inject") (v "0.1.3") (d (list (d (n "inject-macro") (r "^0.1.1") (d #t) (k 0)) (d (n "rstest") (r "^0.6.3") (d #t) (k 2)))) (h "141v8lyb8zbhzw1m8ja5wvr30zczzzdnd8friy9ym8si430nz2jk")))

