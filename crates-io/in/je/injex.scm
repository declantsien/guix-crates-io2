(define-module (crates-io in je injex) #:use-module (crates-io))

(define-public crate-injex-0.1.0 (c (n "injex") (v "0.1.0") (d (list (d (n "dynasm") (r "^1.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "dynasmrt") (r "^1.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "goblin") (r "^0.4") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "nix") (r "^0.20") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "procfs") (r "^0.9") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "sysinfo") (r "^0.17") (d #t) (k 0)))) (h "1zglsn25n9dvc3iy70h0616nl9xhpr6pr92kz2zknjxs51jvxvbb")))

