(define-module (crates-io in po inpoly) #:use-module (crates-io))

(define-public crate-inpoly-0.1.1 (c (n "inpoly") (v "0.1.1") (d (list (d (n "gnuplot") (r "^0.0.43") (d #t) (k 2)) (d (n "meshgridrs") (r "^0.1.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.5.1") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)))) (h "04525fqq2k340fjnv2mcl3j4kn7y3j3khz3xc3s9kwv1pfsv37vr")))

