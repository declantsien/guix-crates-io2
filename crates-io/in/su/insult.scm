(define-module (crates-io in su insult) #:use-module (crates-io))

(define-public crate-insult-1.0.2 (c (n "insult") (v "1.0.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "06iyn1manm08xmy1yj4s7c230g4g4azpdaz3pmrvyipdsjy6c589")))

(define-public crate-insult-1.1.0 (c (n "insult") (v "1.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0jcvfw7lvnrj8h1mvqyy65zfpxhjh76jkrbzw1pay3k3f39dj94f")))

(define-public crate-insult-1.1.1 (c (n "insult") (v "1.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0m84905cqj2hhifinzqg94yk3khqkjiyjnlg81yyil6kv6mcvv3n")))

(define-public crate-insult-1.1.2 (c (n "insult") (v "1.1.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "14kjhz43fwrmh36fiz11606nn2jwicb5hqskdfi1wys6laqp93d0")))

(define-public crate-insult-1.1.3 (c (n "insult") (v "1.1.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0qja4b226d5qypg4z1y80lx2spcq0q2p9mj0yd5nrrb387y7nyaf")))

(define-public crate-insult-1.1.4 (c (n "insult") (v "1.1.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1f4582xgc22s5bj5pi3m3z3gmjisvbay923hm139v8ncrm18f3ih")))

(define-public crate-insult-2.0.0 (c (n "insult") (v "2.0.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "xdg") (r "^2.1.0") (d #t) (k 0)))) (h "05zyqk0hlgi0g098mzaacyclkrimmpn84r5hd8brmwff30w3bhih")))

(define-public crate-insult-2.0.1 (c (n "insult") (v "2.0.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "xdg") (r "^2.1.0") (d #t) (k 0)))) (h "02qpfrg9rjb1m4m8jz8zk0bpblcakwlfajwghj5say7gqaivzjll")))

(define-public crate-insult-2.0.2 (c (n "insult") (v "2.0.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "xdg") (r "^2.1.0") (d #t) (k 0)))) (h "0shwcqbmgl6yjxdrh97b256l75skr1knfj334a8nnqm3282x2xzz")))

(define-public crate-insult-2.0.3 (c (n "insult") (v "2.0.3") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "xdg") (r "^2.1.0") (d #t) (k 0)))) (h "1ngldv38fdcsj2xpcnh4js55hwp5f51mgzb1b3vf2kg5ydnzy7rh")))

