(define-module (crates-io in sl inslice) #:use-module (crates-io))

(define-public crate-inslice-1.0.0 (c (n "inslice") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)))) (h "0wm4ry7p73d147na3yxxdsa87fzmx0vvglda5pj68z85259mvvxl")))

(define-public crate-inslice-1.0.1 (c (n "inslice") (v "1.0.1") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)))) (h "0b07w7smlaqyxpvb1qj7zrz7wmw2bbdw17mlypkyw9v4wkmyh92v") (y #t)))

(define-public crate-inslice-1.1.0 (c (n "inslice") (v "1.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)))) (h "00cwfnv1lcpwc0nsjiv0r21fqqb4nvafrclawv96py9p8djsf29i")))

