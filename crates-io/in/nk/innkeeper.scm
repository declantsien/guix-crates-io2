(define-module (crates-io in nk innkeeper) #:use-module (crates-io))

(define-public crate-innkeeper-0.0.1 (c (n "innkeeper") (v "0.0.1") (h "07yi1nnza5g8gms7p4gnrjjf2yiay6iiq2q92kkfjmsa5fj2icc3") (y #t)))

(define-public crate-innkeeper-0.0.2 (c (n "innkeeper") (v "0.0.2") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0bp9q5pfwmrn5rga9gk5vlcm98dh0zxd209gmzcjwabz35rlka5x") (y #t)))

(define-public crate-innkeeper-0.0.3 (c (n "innkeeper") (v "0.0.3") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1n4ff9qks8hv6iivvvldp4hnh0hacfjg328p34971ds97rcakrx7") (y #t)))

(define-public crate-innkeeper-0.0.4 (c (n "innkeeper") (v "0.0.4") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "14x85j1nh1jfqs8k1l72i96dv4qa2lancc00dxz64m7j6d57w4jq") (y #t)))

(define-public crate-innkeeper-0.0.5 (c (n "innkeeper") (v "0.0.5") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0hi1kwyfsihfdcdn596d5dkfh2xc1x83mnq63haczss2sgq4pyl5") (y #t)))

(define-public crate-innkeeper-0.0.6 (c (n "innkeeper") (v "0.0.6") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1xm8vkm0a21cl2dk2qbh25j0fabmxymljqrb4kxywlvsby6zyqkm") (y #t)))

(define-public crate-innkeeper-0.0.7 (c (n "innkeeper") (v "0.0.7") (d (list (d (n "clap") (r "^4.3.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "169m7ac45pmxciqankvxmd7fm9ybp22wh1yssv1j62hrvfc3l5xb") (y #t)))

(define-public crate-innkeeper-0.0.8 (c (n "innkeeper") (v "0.0.8") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0m4qw9vnj14s5lzik5275cf85ickm3hr9jcrlki4lxd7rja51m2a") (y #t)))

(define-public crate-innkeeper-0.0.10 (c (n "innkeeper") (v "0.0.10") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1ykc81lnlna8733dhv0cn6awz9q3vs488ham6rpqsyl373f1d6wh") (y #t)))

(define-public crate-innkeeper-0.0.11 (c (n "innkeeper") (v "0.0.11") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "16h05afzxcndbfkrfm0931a2li7mnix4n85rxmp7idl0p1m7g979") (y #t)))

(define-public crate-innkeeper-0.0.14 (c (n "innkeeper") (v "0.0.14") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "044ymqcsfl5xy7n60iyn2v2i3pv79pwrysayqrrb3dq47cgrvshc") (y #t)))

(define-public crate-innkeeper-0.0.12 (c (n "innkeeper") (v "0.0.12") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1r0hysa4bcn2dlzfvzglvwkg6mw6xswbbn81lx2jshf58ibqpn25") (y #t)))

(define-public crate-innkeeper-0.0.13 (c (n "innkeeper") (v "0.0.13") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1jm45llnk32v3d4xjbrj66mhxd0rl9h76pdiyrabv9rrwcb977zc") (y #t)))

(define-public crate-innkeeper-0.0.15 (c (n "innkeeper") (v "0.0.15") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "06mpg6i3qnrxg5k31lbb40nypfmhbm9xhvrg0242km8hpfkdjlfj") (y #t)))

(define-public crate-innkeeper-0.0.16 (c (n "innkeeper") (v "0.0.16") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1m63za67f87s7zqva4q4f4q3zlvf6hrs6chm4m2p0px2zf8d9awg") (y #t)))

(define-public crate-innkeeper-0.0.17 (c (n "innkeeper") (v "0.0.17") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0w8yn0cx6rjq0ckqky64k45ryy4f8dxn9g7w9wx62g6byf2yddbl") (y #t)))

(define-public crate-innkeeper-0.1.0 (c (n "innkeeper") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0ldcvcg7l96lg7712yv645axcb2q7xx3gml16zxqk5kfiy4x5dya") (y #t)))

(define-public crate-innkeeper-0.1.1 (c (n "innkeeper") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0kq5hzlj5lxddxm1rw8s6abgvwppvqx5himn4pyf2lna9mjrjjn7") (y #t)))

(define-public crate-innkeeper-0.1.2 (c (n "innkeeper") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1yyz8491f1wzmvk1pald83v6369x42xkg6jb05awlgzxib6swbg9") (y #t)))

(define-public crate-innkeeper-0.1.3 (c (n "innkeeper") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0wbw11734x0wrmv77x41k9agdbca9y5xns0i7h2xl5bgn9cna7z9") (y #t)))

(define-public crate-innkeeper-0.1.4 (c (n "innkeeper") (v "0.1.4") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "18lndgf1lz37j2mplldz5ysx8k093g2vkq3hn2mm76l8ia24cs37") (y #t)))

(define-public crate-innkeeper-0.1.5 (c (n "innkeeper") (v "0.1.5") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0psismjqcjczamxw6fssy4pic1mp4zrj0jm5wsiy1v516ajyn1vy") (y #t)))

(define-public crate-innkeeper-0.1.6 (c (n "innkeeper") (v "0.1.6") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0z8h8qclhamjdddy36nqngq1b11v1xnncggdcl48zghb9wkhfhfs") (y #t)))

(define-public crate-innkeeper-0.1.7 (c (n "innkeeper") (v "0.1.7") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0msgq28k5kbjki78rd8f78p7i3wrg0q2aypdaicq8k8diwf1jb4r") (y #t)))

(define-public crate-innkeeper-0.1.8 (c (n "innkeeper") (v "0.1.8") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0ydi8xs5ydx42kdj4937xcwi2c31xnw3lfhqanlsrrzr4dkiywsy") (y #t)))

(define-public crate-innkeeper-0.1.9 (c (n "innkeeper") (v "0.1.9") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0csym6j2sxk2xs34m6xjjlhv4yqp638p4wplwf8r2h0lhfv3jwa4") (y #t)))

