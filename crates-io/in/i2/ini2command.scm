(define-module (crates-io in i2 ini2command) #:use-module (crates-io))

(define-public crate-ini2command-0.1.0 (c (n "ini2command") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "codepage") (r "^0.1.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.23") (d #t) (k 0)) (d (n "rust-ini") (r "^0.15") (d #t) (k 0)) (d (n "string-error") (r "^0.1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("winnls"))) (d #t) (k 0)))) (h "1cpl1mh9s3inam514sifnwxg9cc9bw3df29rvkn2vlgm1khi0mbl") (f (quote (("unstable_try_decode") ("default"))))))

(define-public crate-ini2command-0.1.1 (c (n "ini2command") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "codepage") (r "^0.1.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.23") (d #t) (k 0)) (d (n "rust-ini") (r "^0.15") (d #t) (k 0)) (d (n "string-error") (r "^0.1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("winnls"))) (d #t) (k 0)))) (h "1fnkpca49kiva3p6qnwldg4z4h15crklzz20d74w5g1vaxfaw2fl") (f (quote (("unstable_try_decode") ("default"))))))

(define-public crate-ini2command-0.1.2 (c (n "ini2command") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "codepage") (r "^0.1.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.23") (d #t) (k 0)) (d (n "rust-ini") (r "^0.15") (d #t) (k 0)) (d (n "string-error") (r "^0.1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("winnls"))) (d #t) (k 0)))) (h "1lm6cwzq1acaqfaaydggcnzg76xfrf10qz6qii42niyz8v3gsgyb") (f (quote (("unstable_try_decode") ("default"))))))

