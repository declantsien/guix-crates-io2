(define-module (crates-io in ks inksac) #:use-module (crates-io))

(define-public crate-inksac-0.1.0 (c (n "inksac") (v "0.1.0") (h "1y963w6f1p9cyzafiv0rhqi3ld1cdrbjsrfvhl2iw6ic52p57jn8") (y #t)))

(define-public crate-inksac-0.1.1 (c (n "inksac") (v "0.1.1") (h "057wx6z7kvi9dcn74388x73vx1hwzpxgdy17vh01153w6r3xfvid")))

(define-public crate-inksac-0.1.2 (c (n "inksac") (v "0.1.2") (h "0fjhd2h92rbka8nlmy1xn3hb26y01q14jrazrpvbyiy07hmlg0ch")))

(define-public crate-inksac-0.1.3 (c (n "inksac") (v "0.1.3") (h "0kpxdfl79fngcwi3wqgnnf25f9jahdcqdyk5yq0k6x5jdn6xfw4i")))

(define-public crate-inksac-0.1.4 (c (n "inksac") (v "0.1.4") (h "0p6gb6qsygcavfcads7qfz8lapv93p24v8gkj7kcjd1wim272khr")))

(define-public crate-inksac-0.2.0 (c (n "inksac") (v "0.2.0") (h "146kn1pp7zp1wghf5igv48bjd0dx6prwbwapi3c5bpyl726g3k5g")))

(define-public crate-inksac-0.3.0 (c (n "inksac") (v "0.3.0") (h "1hk5dkfg0mwajj6cssjm8wn01bfsay75phq2r7dg7pp1i0xkri2x")))

(define-public crate-inksac-0.3.1 (c (n "inksac") (v "0.3.1") (h "1kg8w18xqwh9gazyyx9mccy857b0419sxhyrhqpjasngbsl4aqkb") (y #t)))

(define-public crate-inksac-0.4.0 (c (n "inksac") (v "0.4.0") (h "12hjisbrlry9r95mmxfz1aq9panbpzipxn5nxnw5w2ldpv9sg4xj")))

