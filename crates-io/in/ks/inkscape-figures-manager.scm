(define-module (crates-io in ks inkscape-figures-manager) #:use-module (crates-io))

(define-public crate-inkscape-figures-manager-0.1.0 (c (n "inkscape-figures-manager") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cocoa") (r "^0.25.0") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "global-hotkey") (r "^0.4.2") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "objc") (r "^0.2.7") (d #t) (k 0)) (d (n "tao") (r "^0.26.0") (d #t) (k 0)) (d (n "tfc") (r "^0.7.0") (d #t) (k 0)))) (h "0806w4fxy33615c12zmgzwk7a39d014m1j070kjabnv9p2sk1wsa")))

