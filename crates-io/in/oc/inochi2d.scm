(define-module (crates-io in oc inochi2d) #:use-module (crates-io))

(define-public crate-inochi2d-0.1.0 (c (n "inochi2d") (v "0.1.0") (d (list (d (n "glfw") (r "^0.47") (k 0)) (d (n "glow") (r "^0.11.2") (k 0)) (d (n "image") (r "^0.24") (f (quote ("tga"))) (k 0)) (d (n "nom") (r "^7.1") (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "0j5wccd9mmq52p0g93mh7dcrvpky8ah7g2mlbhix5h366pdwcmxp")))

(define-public crate-inochi2d-0.1.1 (c (n "inochi2d") (v "0.1.1") (d (list (d (n "glfw") (r "^0.47") (k 0)) (d (n "glow") (r "^0.11.2") (k 0)) (d (n "image") (r "^0.24") (f (quote ("png"))) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "0vmhspr9msvlyblxpvkfy4i7bdvza2n058jr711yyc33fs9cq8vy") (f (quote (("parallel") ("default" "parallel"))))))

