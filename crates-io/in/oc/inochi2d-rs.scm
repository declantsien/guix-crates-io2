(define-module (crates-io in oc inochi2d-rs) #:use-module (crates-io))

(define-public crate-inochi2d-rs-0.1.0 (c (n "inochi2d-rs") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qdwcfhxdprj038bfyr9vcl8ywqcfcjqrimzcj7fc79v8gjz9cqq") (f (quote (("opengl") ("default")))) (y #t)))

