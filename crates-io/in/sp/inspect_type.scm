(define-module (crates-io in sp inspect_type) #:use-module (crates-io))

(define-public crate-inspect_type-0.0.7 (c (n "inspect_type") (v "0.0.7") (d (list (d (n "wtest_basic") (r "~0") (d #t) (k 2)))) (h "17qjs20g2cd67lhif5q718nkdspyg0j10jsxxsibp9c4jbrlc88m")))

(define-public crate-inspect_type-0.0.8 (c (n "inspect_type") (v "0.0.8") (d (list (d (n "wtest_basic") (r "~0") (d #t) (k 2)))) (h "0432fss181r5h7r0v7r5jwsz93si21qaivjiqlxsxbq3g2qmfzxp")))

(define-public crate-inspect_type-0.1.0 (c (n "inspect_type") (v "0.1.0") (d (list (d (n "rustversion") (r "~1.0") (d #t) (k 2)) (d (n "wtest_basic") (r "~0.1") (d #t) (k 2)))) (h "03n9qijmk551vqglli6cnqzvidmvwvvqjgpwb9wsnx747aw98mzk") (f (quote (("nightly") ("default"))))))

(define-public crate-inspect_type-0.1.1 (c (n "inspect_type") (v "0.1.1") (d (list (d (n "rustversion") (r "~1.0") (d #t) (k 2)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0czzawk7wdcg6bvx88163pqfny01rmk7rvniwyliqkm00pcak39s") (f (quote (("nightly") ("default"))))))

(define-public crate-inspect_type-0.1.2 (c (n "inspect_type") (v "0.1.2") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "15ip871xvf4zj0b5ngj5xsm14fa9al10lamrdd1wzskvci6rx0q9") (f (quote (("nightly") ("default"))))))

(define-public crate-inspect_type-0.2.0 (c (n "inspect_type") (v "0.2.0") (d (list (d (n "test_tools") (r "^0.1.5") (d #t) (k 2)))) (h "0p0ip5dhfm32h9dm4kqj9yzrnglxbkxlphwccjvhjg21mgdrqvdw") (f (quote (("use_alloc") ("no_std") ("nightly") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-inspect_type-0.3.0 (c (n "inspect_type") (v "0.3.0") (d (list (d (n "test_tools") (r "^0.1.5") (d #t) (k 2)))) (h "18ng2fvk74fqsw431njdmzkvkx5sav8jagnggkj7ijvplxkqfgqm") (f (quote (("use_alloc") ("no_std") ("nightly") ("full" "enabled" "nightly") ("enabled") ("default" "enabled" "nightly"))))))

(define-public crate-inspect_type-0.4.0 (c (n "inspect_type") (v "0.4.0") (d (list (d (n "test_tools") (r "^0.1.5") (d #t) (k 2)))) (h "06vs4kml6w3ywkmvq5jgd9k2qnx38m9g87kcinnq5ralllh6ayii") (f (quote (("use_alloc") ("no_std") ("nightly") ("full" "enabled" "nightly") ("enabled") ("default" "enabled" "nightly"))))))

(define-public crate-inspect_type-0.5.0 (c (n "inspect_type") (v "0.5.0") (d (list (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "test_tools") (r "~0.4.0") (d #t) (k 2)))) (h "1pfl0vbfgn5vhc2j60sb8s68dk3y5lzknbl9gg0ll4ix5b508wl7") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-inspect_type-0.6.0 (c (n "inspect_type") (v "0.6.0") (d (list (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)))) (h "0y2csk3qhwjql2qawmkjjdd6y834lfnh7rm79wwp8bh01xbh8pxh") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-inspect_type-0.7.0 (c (n "inspect_type") (v "0.7.0") (d (list (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "11wigk8w1kmn82pp98zkjxg3bp926n5hwykwayjl3pbiwg7k6j5l") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-inspect_type-0.8.0 (c (n "inspect_type") (v "0.8.0") (d (list (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "test_tools") (r "~0.7.0") (d #t) (k 2)))) (h "1gb41m312vhlhx11h1da8rp29rld2ii00f95ih97asxlfdm5dfsa") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-inspect_type-0.9.0 (c (n "inspect_type") (v "0.9.0") (d (list (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "test_tools") (r "~0.7.0") (d #t) (k 2)))) (h "1w2z1hdh4is8c5xp2bwh2gcsv0z9smwwhrg4cm7xhl9rcksib5fm") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-inspect_type-0.10.0 (c (n "inspect_type") (v "0.10.0") (d (list (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "test_tools") (r "~0.8.0") (d #t) (k 2)))) (h "1zih3vyckcwl7pc9j0x8f35r5370w157skc6hjphs4x3irjri3fj") (f (quote (("use_alloc" "no_std") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

