(define-module (crates-io in sp inspector-gadget) #:use-module (crates-io))

(define-public crate-inspector-gadget-0.3.0 (c (n "inspector-gadget") (v "0.3.0") (d (list (d (n "capstone") (r "^0.12.0") (d #t) (k 0)) (d (n "capstone-sys") (r "^0.16.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "object") (r "^0.35.0") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "0a246rlqqnhrs4f7hm0nfc0dpa98l6wwp8xdkfl4vh4sdfnxdbkq")))

