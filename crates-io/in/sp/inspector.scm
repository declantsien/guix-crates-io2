(define-module (crates-io in sp inspector) #:use-module (crates-io))

(define-public crate-inspector-0.0.0 (c (n "inspector") (v "0.0.0") (d (list (d (n "dont_panic") (r "^0.1.0") (d #t) (k 0)))) (h "0fmdw9n16nmbyg7nnviz3q707vy5wv8xiig50w0x131zx88xac7c") (f (quote (("pedantic"))))))

(define-public crate-inspector-0.0.1 (c (n "inspector") (v "0.0.1") (h "1k8g48fk0xasxa021bl7i7n9hzk575h5h1p9rjwbjsw7w49axw1d") (f (quote (("result") ("pedantic") ("option") ("iter") ("default" "option" "result"))))))

(define-public crate-inspector-0.0.2 (c (n "inspector") (v "0.0.2") (d (list (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1753khsa596x2ajcxaw8sl900gyg1j2gb553mpnz2nk4fiz2w2ms") (f (quote (("result") ("pedantic") ("option") ("iter") ("default" "futures" "option" "result"))))))

(define-public crate-inspector-0.0.3 (c (n "inspector") (v "0.0.3") (d (list (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "086zxkp9y876h7avg51pdw895jz43y7pcicwipxmqvpkrf194k41") (f (quote (("result") ("pedantic") ("option") ("iter") ("default" "futures" "option" "result"))))))

(define-public crate-inspector-0.0.4 (c (n "inspector") (v "0.0.4") (d (list (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "0m06wy95np5y80hklf03ss0myc604nbbm4mz10xx7i8bzvbwzhwx") (f (quote (("result") ("pedantic") ("option") ("inspect-release") ("default" "futures" "option" "result"))))))

(define-public crate-inspector-0.0.5 (c (n "inspector") (v "0.0.5") (d (list (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "1m6h60kng16bc0q0i2qg5m8p8b9qh56s5rvlpkxawzjhqzac7mjc") (f (quote (("result") ("pedantic") ("option") ("default" "futures" "option" "result") ("debug-only"))))))

(define-public crate-inspector-0.1.0 (c (n "inspector") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)))) (h "1qbyb0cj220jlfrhcn8hgky26rzmhzyxp1almrjfxkj4rjhifsj8") (f (quote (("result") ("pedantic") ("option") ("futures01" "futures") ("default" "futures01" "option" "result") ("debug-only"))))))

