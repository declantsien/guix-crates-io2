(define-module (crates-io in sp inspect) #:use-module (crates-io))

(define-public crate-inspect-0.1.0 (c (n "inspect") (v "0.1.0") (h "1mv38335z3jlp3abkvp03dz5cgvyglmv9ygg1zslr38mn0b4d64w")))

(define-public crate-inspect-0.1.1 (c (n "inspect") (v "0.1.1") (h "046ml0mr8f4wlx81n6p9lrcgbvny0xs4v7v7w6jspzjpr71j003m")))

(define-public crate-inspect-0.1.2 (c (n "inspect") (v "0.1.2") (h "033cnl5rki8jh6kx3hcb1n9fzyy8ka19zs38035p9c9vbxwy3zx0")))

