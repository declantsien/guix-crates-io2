(define-module (crates-io in fs infsprogs) #:use-module (crates-io))

(define-public crate-infsprogs-0.1.0 (c (n "infsprogs") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "const_format") (r "^0.2.32") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "xterm-js-rs") (r "^0.1.2") (d #t) (k 0)))) (h "0mmcx5hbrwz8mh8jqymgkddwl1hc1ar32cjmmysvkl36h7plb8v2")))

(define-public crate-infsprogs-0.1.1 (c (n "infsprogs") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "const_format") (r "^0.2.32") (d #t) (k 0)) (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "xterm-js-rs") (r "^0.1.2") (d #t) (k 0)))) (h "10jnjlr399vfdk04nsjl1lqhvnpmlmx2mh5mzn5hjpd6zsq8v34y")))

(define-public crate-infsprogs-0.1.2 (c (n "infsprogs") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "const_format") (r "^0.2.32") (d #t) (k 0)) (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "xterm-js-rs") (r "^0.1.2") (d #t) (k 0)))) (h "1p8rryj4nzj9fwd5mv80kaxmza3ybck80qk67c1in2446xvdar28")))

(define-public crate-infsprogs-0.1.3 (c (n "infsprogs") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.13") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "const_format") (r "^0.2.32") (d #t) (k 0)) (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "xterm-js-rs") (r "^0.1.2") (d #t) (k 0)))) (h "0ppzxxwka3hkbqc1f83rhi19fkriyj2hszy906mmriaw96kcnv07")))

