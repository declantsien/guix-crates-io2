(define-module (crates-io in ts intspector) #:use-module (crates-io))

(define-public crate-intspector-0.1.0 (c (n "intspector") (v "0.1.0") (d (list (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "0nqnrz733ysnlhng1ff0h7k4yxhyq354klaqlaq91930qndk13nn")))

(define-public crate-intspector-0.2.0 (c (n "intspector") (v "0.2.0") (d (list (d (n "arguably") (r "^0.1.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "033a03gj2g66fgim2m9m0j8byp4nsvkhj5rwz8cmvndklz7jnilg")))

(define-public crate-intspector-0.2.1 (c (n "intspector") (v "0.2.1") (d (list (d (n "arguably") (r "^0.1.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "0k6mp5cj5v6sbcyn6b859mwlzi5yypv35g7bjb9pw2zhqkhn6kix")))

(define-public crate-intspector-0.3.0 (c (n "intspector") (v "0.3.0") (d (list (d (n "arguably") (r "^0.1.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "1cpibsjm3d1hpvalra5v3nclrwnfj5fggl650zflh0dvbny6lssp")))

(define-public crate-intspector-0.3.1 (c (n "intspector") (v "0.3.1") (d (list (d (n "arguably") (r "^0.2.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "16183mhfjkx8wzd2k4052bhqqxmpzwyx6h6428sjxfjpb1ss5c8a")))

(define-public crate-intspector-0.3.2 (c (n "intspector") (v "0.3.2") (d (list (d (n "arguably") (r "^0.3.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "1jq4j3ya46i53sxggkwbh4x0j1x2n4nadd13l6x54b9b3gkha4wq")))

(define-public crate-intspector-0.3.3 (c (n "intspector") (v "0.3.3") (d (list (d (n "arguably") (r "^0.4.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "0n6s4a564in57gbfdryldvy4zhr7aiqiabviz1np4hzd0diafmjd")))

(define-public crate-intspector-0.4.0 (c (n "intspector") (v "0.4.0") (d (list (d (n "arguably") (r "^0.6.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "1q82chdyxq31dj0bzwglb903sfpghgshxx4m5260d742zn7bizr6")))

(define-public crate-intspector-0.4.1 (c (n "intspector") (v "0.4.1") (d (list (d (n "arguably") (r "^0.6.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "14v178gzm7g7bcvhiarpc2ap5yl55mz50c1ffsjhlj0yyg8vzi04")))

(define-public crate-intspector-0.4.2 (c (n "intspector") (v "0.4.2") (d (list (d (n "arguably") (r "^0.8") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "0411hiy5s20jn7hzs6hiy38hfv0vnbg2b43z9jsjsgkzd347hfqf")))

(define-public crate-intspector-0.4.3 (c (n "intspector") (v "0.4.3") (d (list (d (n "arguably") (r "^0.9") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "1k8rf09drbml0yfc1nhyggi26hrawnp9n7xq788pj5wvh0sddsbg")))

(define-public crate-intspector-1.0.0 (c (n "intspector") (v "1.0.0") (d (list (d (n "arguably") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "term_size") (r ">=0.3.2, <0.4.0") (d #t) (k 0)))) (h "16nvli3lgpnk2k61yn2lg2ljjcy2s17008j30ck94i7wwzq02i87")))

(define-public crate-intspector-1.0.1 (c (n "intspector") (v "1.0.1") (d (list (d (n "arguably") (r "^1.0.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "01m9r7czsx5403ki40yvdyznkbmj2z8y9if9dvydy5iqrwpyb4l7")))

(define-public crate-intspector-1.1.0 (c (n "intspector") (v "1.1.0") (d (list (d (n "arguably") (r "^1.0.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "144zpp16czjrxk0h5ndnjikmkb9wzpv23mdrczgymmd59rcg77ri")))

(define-public crate-intspector-1.1.1 (c (n "intspector") (v "1.1.1") (d (list (d (n "arguably") (r "^1.0.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "15h8g1k44szxsxr5g7m17zil3wmf5hh1lbwi021mcaj9af2dp1v3")))

