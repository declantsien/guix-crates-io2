(define-module (crates-io in ts intset) #:use-module (crates-io))

(define-public crate-intset-1.0.0 (c (n "intset") (v "1.0.0") (h "1vr40xdiqnd11ll2bwacpgvrmqwcv5pp98lcdsdbj0f94729qsbh") (y #t)))

(define-public crate-intset-1.0.1 (c (n "intset") (v "1.0.1") (h "0zb57qxzqy5ah1rlqzcdr3fh7a84j7rdzkcb6wz1lfvzvflclidn") (y #t)))

(define-public crate-intset-1.0.2 (c (n "intset") (v "1.0.2") (h "1chxivyinbjxn80lad30fhlk6f8jjrkp32kg3k82546hlll5f33c")))

