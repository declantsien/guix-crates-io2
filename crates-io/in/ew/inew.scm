(define-module (crates-io in ew inew) #:use-module (crates-io))

(define-public crate-inew-0.1.0 (c (n "inew") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1cwxx4an1g5k7sf6kv634yshm61v1klvk4fkk8jhznpsiyjg24xy")))

(define-public crate-inew-0.1.1 (c (n "inew") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1gvxna8v3nsclbv79432whv4k812qph44rlbwjskhxfy5w76f2qn")))

(define-public crate-inew-0.2.0 (c (n "inew") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0wrlpm00z50wdmv6innbzdyg52vcncxqzlcx8zgrxyxw3k3macc6")))

(define-public crate-inew-0.2.2 (c (n "inew") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1c4xgqc1ivdhaacy3zs9wnnzz2aldv1faxg6vdccs65ga6ywpkfy") (f (quote (("std") ("default" "std"))))))

