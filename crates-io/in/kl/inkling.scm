(define-module (crates-io in kl inkling) #:use-module (crates-io))

(define-public crate-inkling-0.9.0 (c (n "inkling") (v "0.9.0") (h "04bw5x8qh27gpyls7ksiyn0gy43fbc2md3w31in4n6929hiz3mms")))

(define-public crate-inkling-0.9.1 (c (n "inkling") (v "0.9.1") (h "1bbgrgvvikgvyrcx9j5apv5chsnz6kna3fhjxi2cw035zzhawvka")))

(define-public crate-inkling-0.9.2 (c (n "inkling") (v "0.9.2") (h "1abqvm7lzc45cg2ggiryr6hd79dpsl2c5lyykb511ccjif2ir4qj")))

(define-public crate-inkling-0.9.3 (c (n "inkling") (v "0.9.3") (h "065j194hpwsci05bhcs5z9b9c1d35b6526g6y9xzb1c2vvs9va6b")))

(define-public crate-inkling-0.10.0 (c (n "inkling") (v "0.10.0") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0m6gibj2ssf80v3srnfzadcbhpckqbzi878hm9sfc1fp4jqpx43h") (f (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.10.1 (c (n "inkling") (v "0.10.1") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1qkyqngw2q2y8f2qw6abnl7lpa8l2jgiv3vn1jkjz8nwx2phq311") (f (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.10.2 (c (n "inkling") (v "0.10.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1adj1fqz1a6mc1sqxkwikpz4bfkyqnr5lrq8vrxqp95xc99yz48r") (f (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.10.3 (c (n "inkling") (v "0.10.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "08chyiil7przsik5k069pzqg0cjhc392whspkhd8mgry133zsj18") (f (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.10.4 (c (n "inkling") (v "0.10.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "12q80f255hcwdwycl7v8mj8ja7v00g98snbvmb19h74fkzs4vs3x") (f (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.11.0 (c (n "inkling") (v "0.11.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "11z7nrrdvnwik1vma3f3ls87vganl94rf6q0y42qv23kjfxr71vl") (f (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.11.1 (c (n "inkling") (v "0.11.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1mxxhfahixhr6mpql605417wdbjm8vacszb0jcxp36k3d65vbivl") (f (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.11.2 (c (n "inkling") (v "0.11.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1wmwsknsqsv8wpc1m2zmyz73dwb8yglp2pi03wg0pzgjll25alf8") (f (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.11.3 (c (n "inkling") (v "0.11.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0q39qzyw5mr2z5m0n82a3sclfx0blkff0w0avm4hl70342f26ddh") (f (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.11.4 (c (n "inkling") (v "0.11.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1z4rqd12l6qxr1fh3kndzbd13iq926p0sl244rqdk7i71flm71x5") (f (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.11.5 (c (n "inkling") (v "0.11.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "09rjrlfnscvy61a9isnv2dhinna9v0989bhl7fh2s4q78sj66ci4") (f (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.11.6 (c (n "inkling") (v "0.11.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "18h5lpzb617r4vl2j1z4crczlb126z4nxkn7cdsmp9hng20zfsp8") (f (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.11.7 (c (n "inkling") (v "0.11.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1nssd8zkdc4acapk3kn6i8as0xjcliipchgv5yvkdqylb05fxza5") (f (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.12.0 (c (n "inkling") (v "0.12.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0g79h6k8cfwj2625hi789kw9ymkax2hh9610l4r2m312wkk9g75l") (f (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.12.1 (c (n "inkling") (v "0.12.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0h4v92rcgmv67ag1jdaj6vd1q9w54102x29kqyazrkypg7v88x5h") (f (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.12.2 (c (n "inkling") (v "0.12.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0g0w5plxk7s0bvg7scdc2afsi0ippbgqihvlylbbfhv4yz8svvgg") (f (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.12.3 (c (n "inkling") (v "0.12.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1wcz06jfksmnc8g6ydmr1jfk3b3wvkklxvfkmpc9hh6qgjhs66kr") (f (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.12.4 (c (n "inkling") (v "0.12.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1b2rij5n3njf12i6w2wvzril9zjg002y4vhr0axywgl5f9p99wy3") (f (quote (("serde_support" "serde/derive"))))))

(define-public crate-inkling-0.12.5 (c (n "inkling") (v "0.12.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1swba3271ijnz0g1xxk8614c5sb5amhv8j70mk8ybxhgvfzjyd6b") (f (quote (("serde_support" "serde/derive"))))))

