(define-module (crates-io in ci incinerator) #:use-module (crates-io))

(define-public crate-incinerator-0.0.1 (c (n "incinerator") (v "0.0.1") (d (list (d (n "atomic_enum") (r "^0.1.1") (d #t) (k 0)) (d (n "nested") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "thread_local") (r "^1.0.1") (d #t) (k 0)))) (h "1dqv0jdmrbadsl3c21s0dm8y4zgdzqnvsi378lf9ckwk5y6kf0fb")))

