(define-module (crates-io in cl include_data) #:use-module (crates-io))

(define-public crate-include_data-0.1.0 (c (n "include_data") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("min_const_generics" "wasm_simd" "aarch64_simd"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1j9cb5qz4a36hpfzql5ck10w2qnfi0bhhp77wgk5xjhwilqc6zjd")))

(define-public crate-include_data-0.1.1 (c (n "include_data") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1") (f (quote ("min_const_generics" "wasm_simd" "aarch64_simd"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0lw7yg4b8plavx2d222pad0sziyqb4s1ljy7fm3gzfzszf9vlr40")))

(define-public crate-include_data-0.2.0 (c (n "include_data") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("min_const_generics" "wasm_simd" "aarch64_simd"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0abh085qj2243ld3sr3sj4aj5i5d14hbxas1pjbpm25fnis0mvjl")))

(define-public crate-include_data-1.0.0 (c (n "include_data") (v "1.0.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("min_const_generics" "wasm_simd" "aarch64_simd"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0r9lxsnqvkcp05p4b6wvariqb5i9fcz75mva4wwnqz9056sx9pas")))

