(define-module (crates-io in cl include-bytes-zstd) #:use-module (crates-io))

(define-public crate-include-bytes-zstd-0.1.0 (c (n "include-bytes-zstd") (v "0.1.0") (d (list (d (n "include-bytes-zstd-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "ruzstd") (r "^0.3.0") (d #t) (k 0)))) (h "1kncgrs8r8p24sn7hngy69fn8ycrrdz2d3dhppcf0b05bg7j4zrk") (r "1.64")))

