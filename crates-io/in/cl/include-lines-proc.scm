(define-module (crates-io in cl include-lines-proc) #:use-module (crates-io))

(define-public crate-include-lines-proc-1.0.0 (c (n "include-lines-proc") (v "1.0.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1j00ig8f5f35llqd1qfqslal9bgdyizh3r4yv7vnnd7dla81imc3")))

(define-public crate-include-lines-proc-1.1.0 (c (n "include-lines-proc") (v "1.1.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0xsgd8m9yw32s13ba75nlcw052ysidvars0sb4dvpi8h51wz47yd")))

