(define-module (crates-io in cl include-bytes-plus) #:use-module (crates-io))

(define-public crate-include-bytes-plus-1.0.0 (c (n "include-bytes-plus") (v "1.0.0") (h "0kx0056fgscrpb3zx294w79kgjvdrrjn4syg6z7jwyb9yfbhn589") (y #t)))

(define-public crate-include-bytes-plus-1.0.1 (c (n "include-bytes-plus") (v "1.0.1") (h "092w8rp7zir25gh7393dygfh50kjxnncy9jvskda50di1n1krm2z")))

(define-public crate-include-bytes-plus-1.0.2 (c (n "include-bytes-plus") (v "1.0.2") (h "1g7sk5sml1v56mil61ibp2xx0vhgz40dwqfcj5qzbz9cxcdf8qyy")))

(define-public crate-include-bytes-plus-1.0.3 (c (n "include-bytes-plus") (v "1.0.3") (h "0q7h08kyh4q2fprh1l86d138k2kjf34mfyypfp1sinb6ybin9vzv")))

(define-public crate-include-bytes-plus-1.1.0 (c (n "include-bytes-plus") (v "1.1.0") (h "16fi4jqklkx8r796zd48kwkfrc7svhwp10q80fxl85fbkbmqfa0j")))

