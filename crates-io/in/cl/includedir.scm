(define-module (crates-io in cl includedir) #:use-module (crates-io))

(define-public crate-includedir-0.1.0 (c (n "includedir") (v "0.1.0") (d (list (d (n "phf_codegen") (r "^0.7.12") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "13p6lk3krz2xp30bsh5snlxpr5nlvc5d2279hzngr48x4cnzjicv")))

(define-public crate-includedir-0.1.1 (c (n "includedir") (v "0.1.1") (d (list (d (n "phf_codegen") (r "^0.7.12") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "1ww0873fqgpca2x5b4n80200z5bd8nzv20a6rm0yjmb3x8l43mfa")))

(define-public crate-includedir-0.2.0 (c (n "includedir") (v "0.2.0") (d (list (d (n "flate2") (r "^0.2.13") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.7.12") (d #t) (k 0)))) (h "1mmj891qrbl4k2kwgkcii97wknwkd8gi7l1vm0lsy9x8m7ryd0w2") (f (quote (("default" "flate2"))))))

(define-public crate-includedir-0.2.1 (c (n "includedir") (v "0.2.1") (d (list (d (n "flate2") (r "^0.2.13") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.7.12") (d #t) (k 0)))) (h "1z873c8hhxlb7kbqc3fh4nckgfcmc3mkpvnqnx6jmrps7zcn8csx") (f (quote (("default" "flate2"))))))

(define-public crate-includedir-0.2.2 (c (n "includedir") (v "0.2.2") (d (list (d (n "flate2") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.7.16") (d #t) (k 0)))) (h "0fjaxkp3lhl51psc9n7gxagibvzwhnhnm23ag63lbzhabhm0lizd") (f (quote (("default" "flate2"))))))

(define-public crate-includedir-0.3.0 (c (n "includedir") (v "0.3.0") (d (list (d (n "flate2") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)))) (h "1y0wlxr4chqvsc8iimagi8qnfgawsjc0vq9x3nxpcmy32ja808si") (f (quote (("default" "flate2"))))))

(define-public crate-includedir-0.4.0 (c (n "includedir") (v "0.4.0") (d (list (d (n "flate2") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)))) (h "1q4qdcpgpgbw2gfspd6c4xxc1yib37l3f18h3l3vmnqa1ymch9kb") (f (quote (("default" "flate2"))))))

(define-public crate-includedir-0.5.0 (c (n "includedir") (v "0.5.0") (d (list (d (n "flate2") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)))) (h "1dph3gcvl1nqpaa15wmgflizv32mnv1k2wdja7mfn6d5f3vh4x79") (f (quote (("default" "flate2"))))))

(define-public crate-includedir-0.6.0 (c (n "includedir") (v "0.6.0") (d (list (d (n "flate2") (r "^1.0.14") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (d #t) (k 0)))) (h "1dvdh6gcxh6c2wi8n24840ig8jw9l04icvf7klxc804cfyyjdldg") (f (quote (("default" "flate2"))))))

