(define-module (crates-io in cl include_glob) #:use-module (crates-io))

(define-public crate-include_glob-0.1.0 (c (n "include_glob") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)))) (h "1b2g8bppffsd96nlypiw1y662d8aby1ym040rsaaj408hb28cxf6")))

(define-public crate-include_glob-0.2.0 (c (n "include_glob") (v "0.2.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)))) (h "1ypw7y7fgv80znnrvvzcl0jqb9x3ygm44yvxraijfkgqxdhj4gar")))

