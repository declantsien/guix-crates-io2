(define-module (crates-io in cl include_wit) #:use-module (crates-io))

(define-public crate-include_wit-0.1.0 (c (n "include_wit") (v "0.1.0") (d (list (d (n "include_wit_macro") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (f (quote ("alloc" "race"))) (k 0)) (d (n "wit-component") (r "^0.14.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.11.0") (d #t) (k 0)))) (h "17bh7y7f2ik38v1sqxfx796y3w2hyssi9kkcfffvvmfdxalq3i46") (f (quote (("track_path" "include_wit_macro/track_path") ("relative_path" "include_wit_macro/relative_path"))))))

(define-public crate-include_wit-0.1.1 (c (n "include_wit") (v "0.1.1") (d (list (d (n "include_wit_macro") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (f (quote ("alloc" "race"))) (k 0)) (d (n "wit-component") (r "^0.14.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.11.0") (d #t) (k 0)))) (h "0aim9zc7vv45sjkw9aisk0sg61d11qchhy749z60dnpjvar9j0cp") (f (quote (("track_path" "include_wit_macro/track_path") ("relative_path" "include_wit_macro/relative_path"))))))

(define-public crate-include_wit-0.1.2 (c (n "include_wit") (v "0.1.2") (d (list (d (n "include_wit_macro") (r "=0.1.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (f (quote ("alloc" "race"))) (k 0)) (d (n "wit-component") (r "^0.14.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.11.0") (d #t) (k 0)))) (h "17l6qgwmyzfaxwpbxvm0v469ridspzwbkhcxif2xv3gx89zxhx4h") (f (quote (("track_path" "include_wit_macro/track_path") ("relative_path" "include_wit_macro/relative_path"))))))

(define-public crate-include_wit-0.1.3 (c (n "include_wit") (v "0.1.3") (d (list (d (n "include_wit_macro") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (f (quote ("alloc" "race"))) (k 0)) (d (n "wit-component") (r ">=0.20.2") (k 0)) (d (n "wit-parser") (r ">=0.13.2") (k 0)))) (h "14r6lsadzw868s8zkrvpafk0rdbl4clcxijam28ins30234riwhl") (f (quote (("track_path" "include_wit_macro/track_path") ("relative_path" "include_wit_macro/relative_path"))))))

