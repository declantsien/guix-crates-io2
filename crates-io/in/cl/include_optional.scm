(define-module (crates-io in cl include_optional) #:use-module (crates-io))

(define-public crate-include_optional-1.0.0 (c (n "include_optional") (v "1.0.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "printing" "proc-macro"))) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1yr0by01m1xdh98y40wcvwn30sg8dzpxlay2mcd11v1ai0fc7ki8")))

(define-public crate-include_optional-1.0.1 (c (n "include_optional") (v "1.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "printing" "proc-macro"))) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1z2mphjvkyyxbcs9xjzfw3gvygcp6hiv2w8zd5657dq5nb7aya1k")))

