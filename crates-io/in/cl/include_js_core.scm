(define-module (crates-io in cl include_js_core) #:use-module (crates-io))

(define-public crate-include_js_core-0.1.0 (c (n "include_js_core") (v "0.1.0") (d (list (d (n "Boa") (r "^0.11.0") (d #t) (k 0)))) (h "1cvba1djgg1lfmjg2lbfd0vj8xf5gn8ly60dvh6mx164am2b771r") (y #t)))

(define-public crate-include_js_core-0.1.1 (c (n "include_js_core") (v "0.1.1") (d (list (d (n "Boa") (r "^0.11.0") (d #t) (k 0)))) (h "1krlvamy3ir6v3988f0js1b3bva0lb6p7r9gzvlizkmk4fqs94ml") (y #t)))

(define-public crate-include_js_core-0.1.2 (c (n "include_js_core") (v "0.1.2") (d (list (d (n "Boa") (r "^0.11.0") (d #t) (k 0)))) (h "052qg1xbb1y1ymapb41xlmpq1fyiiyif0wv88v4fym4qhnmwb8xb")))

