(define-module (crates-io in cl include_bytes_aligned) #:use-module (crates-io))

(define-public crate-include_bytes_aligned-0.1.0 (c (n "include_bytes_aligned") (v "0.1.0") (h "0qjpw2vcqwahlwglzc75zjvrn0wcvh408kxkkpmrhml5dybwjnk6")))

(define-public crate-include_bytes_aligned-0.1.1 (c (n "include_bytes_aligned") (v "0.1.1") (h "13m24in5ydf9mljlqpanapmivvzqc1pyjc0d32ca05b00a00sm9v")))

(define-public crate-include_bytes_aligned-0.1.2 (c (n "include_bytes_aligned") (v "0.1.2") (h "1ahvsg9y7qcpsfzp95fjzxzgzif4da011z5y8nmrzlqlf75n12kc")))

(define-public crate-include_bytes_aligned-0.1.3 (c (n "include_bytes_aligned") (v "0.1.3") (h "0fzchwf36dv4wdzxb0mcw57wxabis0gxybfaj4m7418n9z1vfk70")))

