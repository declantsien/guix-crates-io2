(define-module (crates-io in cl include_gif) #:use-module (crates-io))

(define-public crate-include_gif-0.1.0 (c (n "include_gif") (v "0.1.0") (d (list (d (n "gif") (r "^0.11.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mqgpifwc7048n2cppalhrgzvimcm18slywzvxdfwjp3jvkwb52r") (y #t)))

(define-public crate-include_gif-1.0.0 (c (n "include_gif") (v "1.0.0") (d (list (d (n "gif") (r "^0.11.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qbnspgsgn5d9rjdbhnk3fgzyi8gnb6q8mfzlqwxgxck1hwx45rb")))

(define-public crate-include_gif-1.0.1 (c (n "include_gif") (v "1.0.1") (d (list (d (n "gif") (r "^0.11.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0996cnw2m34y0b059px5wphsmc2mx4394sp9j27qz1j2ib8908hk")))

(define-public crate-include_gif-1.1.0 (c (n "include_gif") (v "1.1.0") (d (list (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "gif") (r "^0.11.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nabxikssyqss63g5hfxcr10hxlgjr307k2628b0fzhqhnghk3y1")))

