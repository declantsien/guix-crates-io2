(define-module (crates-io in cl include_display_mode_tex) #:use-module (crates-io))

(define-public crate-include_display_mode_tex-0.1.0 (c (n "include_display_mode_tex") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "relative-path") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0bvbp5p3ar0m1wm7x2vk8db4pj4kdsazs34ambd58ync6b2m7l9s") (r "1.59.0")))

(define-public crate-include_display_mode_tex-0.1.1 (c (n "include_display_mode_tex") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "relative-path") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0gv495akhzk20hi2k507hia9k98cw15p2860lqkg13yxir8ny1b7") (r "1.59.0")))

(define-public crate-include_display_mode_tex-0.1.2 (c (n "include_display_mode_tex") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "relative-path") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("parsing"))) (d #t) (k 0)))) (h "02kc49bwiq3rnzlizvjkrghswqr50fxdkw0dgzd2717znpy7g2sy") (r "1.59.0")))

(define-public crate-include_display_mode_tex-0.1.3 (c (n "include_display_mode_tex") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "relative-path") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1zmp9qsdz71k8598357avjrxwy7cyllknjxxi5l704k4qjliylwl") (r "1.59.0")))

(define-public crate-include_display_mode_tex-0.1.4 (c (n "include_display_mode_tex") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "relative-path") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("parsing"))) (d #t) (k 0)))) (h "19hycfldb501fllswv8wy7yi5p3v8313w84rpyw67a285y4x892w") (r "1.59.0")))

(define-public crate-include_display_mode_tex-0.1.5 (c (n "include_display_mode_tex") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "relative-path") (r "^1.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1zp91im16ny353wyzq5cj86ckvp5vd80z5symijv6b3pn7q4phpz") (r "1.59.0")))

