(define-module (crates-io in cl include_data_uri) #:use-module (crates-io))

(define-public crate-include_data_uri-0.1.0 (c (n "include_data_uri") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1v32ybk0vm7lfqdwy7rhrixhh7zqdx545v177cbw2y34xd9b7n5i")))

(define-public crate-include_data_uri-0.1.1 (c (n "include_data_uri") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1k91vshnwni99lzf76cjalvbpc2a99zg84l7z8yqsdydja62ali3")))

(define-public crate-include_data_uri-0.1.2 (c (n "include_data_uri") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "03fk4gccr715q92sqdi2al9zizdr6gkfx6q0mng330q1qqfy1dbf")))

(define-public crate-include_data_uri-0.1.3 (c (n "include_data_uri") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0yqsmxwydi3idrmva2iwnnb1k9qfkhq4qjdnh4983jjriq279zym")))

