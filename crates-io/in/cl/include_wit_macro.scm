(define-module (crates-io in cl include_wit_macro) #:use-module (crates-io))

(define-public crate-include_wit_macro-0.1.0 (c (n "include_wit_macro") (v "0.1.0") (d (list (d (n "litrs") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "wit-component") (r "^0.14.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.11.0") (d #t) (k 0)))) (h "17xd3nzpvks3yclqqrwrffqx8zahsy0ag8lj4g9aqnnjjmqcapz1") (f (quote (("track_path") ("relative_path"))))))

(define-public crate-include_wit_macro-0.1.1 (c (n "include_wit_macro") (v "0.1.1") (d (list (d (n "litrs") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "wit-component") (r "^0.14.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.11.0") (d #t) (k 0)))) (h "090kag2bzb6mdv9lldx6gc7czb6lsbv9v1k9fkrnzhxd8vyvchri") (f (quote (("track_path") ("relative_path"))))))

(define-public crate-include_wit_macro-0.1.2 (c (n "include_wit_macro") (v "0.1.2") (d (list (d (n "litrs") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "wit-component") (r "^0.14.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.11.0") (d #t) (k 0)))) (h "1cmk9hq5081k5aix68il7lbk5m4yb86v2m9w9azypp87gx6nb124") (f (quote (("track_path") ("relative_path"))))))

(define-public crate-include_wit_macro-0.1.3 (c (n "include_wit_macro") (v "0.1.3") (d (list (d (n "litrs") (r "^0.4.1") (k 0)) (d (n "proc-macro2") (r "^1.0.78") (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "wit-component") (r ">=0.20.2") (k 0)) (d (n "wit-parser") (r ">=0.13.2") (k 0)))) (h "125z3dh1kkw1ckqj9zavx29fmhwm0hggnia8wm569qns4wkpnmya") (f (quote (("track_path") ("relative_path"))))))

