(define-module (crates-io in cl include-dir-macro) #:use-module (crates-io))

(define-public crate-include-dir-macro-0.2.0 (c (n "include-dir-macro") (v "0.2.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)) (d (n "tree_magic") (r "^0.2") (o #t) (d #t) (k 0)))) (h "10dr725h1jjq9f854w3g08ps69vyjdkiplywyai64ba5ajv1gshw") (f (quote (("web" "rocket" "tree_magic"))))))

