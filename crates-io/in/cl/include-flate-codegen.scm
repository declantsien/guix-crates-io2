(define-module (crates-io in cl include-flate-codegen) #:use-module (crates-io))

(define-public crate-include-flate-codegen-0.1.0 (c (n "include-flate-codegen") (v "0.1.0") (d (list (d (n "libflate") (r "^0.1.26") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.44") (f (quote ("full"))) (d #t) (k 0)))) (h "0ddg8p80mqrb3c2bh733v6plj4dbbs294h00kphss851a0mi6kv0")))

(define-public crate-include-flate-codegen-0.1.1 (c (n "include-flate-codegen") (v "0.1.1") (d (list (d (n "libflate") (r "^0.1.26") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.44") (f (quote ("full"))) (d #t) (k 0)))) (h "0vwlg9b6574qqjbbnnwcpkjic0ffhmqg4ayhclimvy5lxyjbqgjc")))

(define-public crate-include-flate-codegen-0.1.2 (c (n "include-flate-codegen") (v "0.1.2") (d (list (d (n "libflate") (r "^0.1.26") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.44") (f (quote ("full"))) (d #t) (k 0)))) (h "0gcjkw6lk0r2gclz4bq0s5z1mpv0nzclqs8shbhy94b7cmgpn42z")))

(define-public crate-include-flate-codegen-0.1.3 (c (n "include-flate-codegen") (v "0.1.3") (d (list (d (n "libflate") (r "^0.1.26") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.44") (f (quote ("full"))) (d #t) (k 0)))) (h "1mbw6fcczmfw4dabps4l90mdn2wdy9zhqwh60xx4fjcxqfsm9ksb") (f (quote (("stable" "proc-macro-hack"))))))

(define-public crate-include-flate-codegen-0.1.4 (c (n "include-flate-codegen") (v "0.1.4") (d (list (d (n "libflate") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.9") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "1s34ssq0l3d2sn8n3mxmkz3jbm600fbckd0213mjjcgs34a6wz9s") (f (quote (("stable" "proc-macro-hack"))))))

(define-public crate-include-flate-codegen-0.2.0 (c (n "include-flate-codegen") (v "0.2.0") (d (list (d (n "libflate") (r "^2.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1mz7zyalpn1f53zy12xnbjis93jfh23wy7n6hlxp5gk1c9n28nwc")))

(define-public crate-include-flate-codegen-0.3.0 (c (n "include-flate-codegen") (v "0.3.0") (d (list (d (n "include-flate-compress") (r "^0.3.0") (d #t) (k 0)) (d (n "libflate") (r "^2.0.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "zstd") (r "^0.13.0") (d #t) (k 0)))) (h "0dm361ivav8f4qdkkzydwm39y9zy9kw2ab0p7fpg24kyqw1pgq05") (f (quote (("no-compression-warnings") ("default"))))))

