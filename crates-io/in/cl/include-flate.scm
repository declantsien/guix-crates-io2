(define-module (crates-io in cl include-flate) #:use-module (crates-io))

(define-public crate-include-flate-0.1.0 (c (n "include-flate") (v "0.1.0") (d (list (d (n "include-flate-codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "libflate") (r "^0.1.26") (d #t) (k 0)))) (h "07gw8wngi9cp3g3f2vb26jxx4kjrkiw7bj3i139yph2qs0sidq72")))

(define-public crate-include-flate-0.1.1 (c (n "include-flate") (v "0.1.1") (d (list (d (n "include-flate-codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "libflate") (r "^0.1.26") (d #t) (k 0)))) (h "1x693h0r3pzykzlhmhg7maq08zgfkhvl6akd3rvxl82i2r8q5jyc")))

(define-public crate-include-flate-0.1.2 (c (n "include-flate") (v "0.1.2") (d (list (d (n "include-flate-codegen") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "libflate") (r "^0.1.26") (d #t) (k 0)))) (h "1wsfqn4zjnq8jp6jlwrxaxwvbd05savdb7zg08843r38ff5vcnci")))

(define-public crate-include-flate-0.1.3 (c (n "include-flate") (v "0.1.3") (d (list (d (n "include-flate-codegen-exports") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "libflate") (r "^0.1.26") (d #t) (k 0)))) (h "0xd63rhr03ax1w327ws46wc9zyw5k9jsaxfy24j6wg9km3xhfqii") (f (quote (("stable" "include-flate-codegen-exports/stable"))))))

(define-public crate-include-flate-0.1.4 (c (n "include-flate") (v "0.1.4") (d (list (d (n "include-flate-codegen-exports") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "libflate") (r "^1.0.0") (d #t) (k 0)))) (h "0j71284rcjl2pnnrnvp2i4r9dyikpw939gcvz0f7qm91qx4v9p6z") (f (quote (("stable" "include-flate-codegen-exports/stable"))))))

(define-public crate-include-flate-0.2.0 (c (n "include-flate") (v "0.2.0") (d (list (d (n "include-flate-codegen-exports") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "libflate") (r "^1.0.0") (d #t) (k 0)))) (h "1c5dsx6j9jwrd6calhxdgip85qjy45hc8v1740fr61k46ilibqf2") (f (quote (("stable" "include-flate-codegen-exports/stable"))))))

(define-public crate-include-flate-0.3.0 (c (n "include-flate") (v "0.3.0") (d (list (d (n "include-flate-codegen") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "libflate") (r "^2.0.0") (d #t) (k 0)))) (h "0ghiy1f2ipzw71nggrv6chard023nyjmvq2dyg0qcm39a1kw2jfz")))

