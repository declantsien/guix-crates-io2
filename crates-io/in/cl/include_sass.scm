(define-module (crates-io in cl include_sass) #:use-module (crates-io))

(define-public crate-include_sass-0.12.1 (c (n "include_sass") (v "0.12.1") (d (list (d (n "grass_compiler") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (k 0)) (d (n "syn") (r "^1.0.103") (k 0)))) (h "14sbhm8c375b5fwy7y9vpcg0jc086srxiwjdq617sdkazxmnja0a") (f (quote (("nightly"))))))

(define-public crate-include_sass-0.12.2 (c (n "include_sass") (v "0.12.2") (d (list (d (n "grass_compiler") (r "^0.12.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (k 0)) (d (n "syn") (r "^1.0.103") (k 0)))) (h "0a9ispzpa8pas31v0xi32hmfila7drhiizirb3ckfm4a20a493gd") (f (quote (("nightly"))))))

(define-public crate-include_sass-0.12.3 (c (n "include_sass") (v "0.12.3") (d (list (d (n "grass_compiler") (r "^0.12.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (k 0)) (d (n "syn") (r "^1.0.103") (k 0)))) (h "11bzylqnilvr912706j60a3syq1rgs6wgnxa7dwvv1kncccn1dfg") (f (quote (("nightly"))))))

(define-public crate-include_sass-0.12.4 (c (n "include_sass") (v "0.12.4") (d (list (d (n "grass_compiler") (r "^0.12.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (k 0)) (d (n "syn") (r "^1.0.103") (k 0)))) (h "0zrfbl96agzbpj1sr5rqcg12xdk87in611japkbnzbw5a5cgqhyr") (f (quote (("nightly"))))))

(define-public crate-include_sass-0.13.0 (c (n "include_sass") (v "0.13.0") (d (list (d (n "grass_compiler") (r "=0.13.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (k 0)) (d (n "syn") (r "^1.0.103") (k 0)))) (h "1mifwzcm7ggyz1yfw1f6cyvqyfk5h4p4iih78d4rrjdc1hbw02ca") (f (quote (("nightly"))))))

(define-public crate-include_sass-0.13.2 (c (n "include_sass") (v "0.13.2") (d (list (d (n "grass_compiler") (r "=0.13.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (k 0)) (d (n "syn") (r "^2") (k 0)))) (h "13fbk1f41m2j8ah5bmqqd4jg775k257c4dc56fssjp9p6nmr82an") (f (quote (("nightly")))) (r "1.70")))

(define-public crate-include_sass-0.13.3 (c (n "include_sass") (v "0.13.3") (d (list (d (n "grass_compiler") (r "=0.13.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (k 0)) (d (n "syn") (r "^2") (k 0)))) (h "0y2c3v9nvyp05yja3yi15fsblzdhzzfkl36myqs34mszwi4k8pni") (f (quote (("nightly")))) (r "1.70")))

