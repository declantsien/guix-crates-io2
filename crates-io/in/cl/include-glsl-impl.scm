(define-module (crates-io in cl include-glsl-impl) #:use-module (crates-io))

(define-public crate-include-glsl-impl-0.1.0 (c (n "include-glsl-impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "shaderc") (r "^0.6.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "04jgkxfh0d0w3v4gljwl5y8xz4rl2l5lw5ssflhamn4wgmihk8ji")))

(define-public crate-include-glsl-impl-0.2.0 (c (n "include-glsl-impl") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "shaderc") (r "^0.6.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "14qa8qf22dgca184l4pnpyanx025yivn6i8j3scga25apzxa9bhz")))

