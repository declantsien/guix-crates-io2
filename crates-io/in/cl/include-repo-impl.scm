(define-module (crates-io in cl include-repo-impl) #:use-module (crates-io))

(define-public crate-include-repo-impl-0.1.0 (c (n "include-repo-impl") (v "0.1.0") (d (list (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1nwjsjbk96kivraf14lmf8z4syczf9ii3gjxpirsycbl6yk62wnk")))

