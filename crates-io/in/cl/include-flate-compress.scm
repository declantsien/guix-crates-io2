(define-module (crates-io in cl include-flate-compress) #:use-module (crates-io))

(define-public crate-include-flate-compress-0.3.0 (c (n "include-flate-compress") (v "0.3.0") (d (list (d (n "libflate") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "zstd") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "18536d8z9kcwzsadgaq8m26n8iyjxvjbc5wdk77zp3h514gg2ax8") (f (quote (("default" "deflate" "zstd")))) (s 2) (e (quote (("zstd" "dep:zstd") ("deflate" "dep:libflate"))))))

