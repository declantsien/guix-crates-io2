(define-module (crates-io in cl include_uri) #:use-module (crates-io))

(define-public crate-include_uri-0.1.0 (c (n "include_uri") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("printing"))) (k 0)))) (h "09by71kyf4fw2c9vrvfd49h9cj8fy2ffwdxggy3dvwf32hs7dwdw")))

(define-public crate-include_uri-0.1.1 (c (n "include_uri") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (k 0)) (d (n "quote") (r "^1.0.33") (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "12jgckxa0c0qz2allva5vpf1mnfam9b4a026i7aapsnczdqzagk3") (f (quote (("rustls-tls-webpki-roots" "reqwest/rustls-tls-webpki-roots") ("rustls-tls-native-roots" "reqwest/rustls-tls-native-roots") ("rustls-tls-manual-roots" "reqwest/rustls-tls-manual-roots") ("native-tls-vendored" "reqwest/native-tls-vendored") ("native-tls-alpn" "reqwest/native-tls-alpn") ("default" "rustls-tls-webpki-roots"))))))

