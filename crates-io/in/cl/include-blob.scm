(define-module (crates-io in cl include-blob) #:use-module (crates-io))

(define-public crate-include-blob-0.1.0 (c (n "include-blob") (v "0.1.0") (d (list (d (n "ar") (r "^0.8.0") (d #t) (k 0) (p "sludge-ar-with-ranlib")) (d (n "include-blob-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "object") (r "^0.30.0") (f (quote ("write"))) (k 0)))) (h "196lfr2ri03z4x9l2hjpf8f5qrixnwvshnnk1j0471w6l0dc579h") (y #t)))

(define-public crate-include-blob-0.1.1 (c (n "include-blob") (v "0.1.1") (d (list (d (n "ar") (r "^0.8.0") (d #t) (k 0) (p "sludge-ar-with-ranlib")) (d (n "include-blob-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "object") (r "^0.30.0") (f (quote ("write"))) (k 0)))) (h "11w8ivfz8ksgl1b6vmnyvz428zc6l90g5131acqd7bqcm129cppi")))

(define-public crate-include-blob-0.1.2 (c (n "include-blob") (v "0.1.2") (d (list (d (n "ar") (r "^0.8.0") (d #t) (k 0) (p "sludge-ar-with-ranlib")) (d (n "include-blob-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "object") (r "^0.30.0") (f (quote ("write"))) (k 0)))) (h "123qkkj03p3b4mww09ck1qg0pl8airmw9rhwrr5nbvz72vm6ypbf")))

(define-public crate-include-blob-0.1.3 (c (n "include-blob") (v "0.1.3") (d (list (d (n "ar_archive_writer") (r "^0.1.4") (d #t) (k 0)) (d (n "include-blob-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "object") (r "^0.31.0") (f (quote ("write"))) (k 0)))) (h "0h792s8bqnvqha8xiwcmcjsd6psrmfpr5wfkmvdwyb4rq1pik6ms")))

(define-public crate-include-blob-0.1.4 (c (n "include-blob") (v "0.1.4") (d (list (d (n "ar_archive_writer") (r "^0.1.4") (d #t) (k 0)) (d (n "include-blob-macros") (r "=0.1.1") (d #t) (k 0)) (d (n "object") (r "^0.31.0") (f (quote ("write"))) (k 0)))) (h "149m727hrf1z7bvv9kx4j31yg0ybgn09j0cpnqfizb2gnlk96bjn")))

(define-public crate-include-blob-0.1.5 (c (n "include-blob") (v "0.1.5") (d (list (d (n "ar_archive_writer") (r "^0.3.0") (d #t) (k 0)) (d (n "include-blob-macros") (r "=0.1.2") (d #t) (k 0)) (d (n "object") (r "^0.35.0") (f (quote ("write"))) (k 0)))) (h "02gazl6y3x4az2r9326mrj065r7f0cqj7cfl8n2bzm74sg78kac3")))

(define-public crate-include-blob-1.0.0 (c (n "include-blob") (v "1.0.0") (d (list (d (n "ar_archive_writer") (r "^0.3.0") (d #t) (k 0)) (d (n "include-blob-macros") (r "=1.0.0") (d #t) (k 0)) (d (n "object") (r "^0.35.0") (f (quote ("write"))) (k 0)))) (h "1xrlrq3km5h0sdbgsfgffvvhs25x2jg9y6fv29ma5hy0r9v4v2lj")))

