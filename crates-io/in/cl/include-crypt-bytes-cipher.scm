(define-module (crates-io in cl include-crypt-bytes-cipher) #:use-module (crates-io))

(define-public crate-include-crypt-bytes-cipher-0.1.0 (c (n "include-crypt-bytes-cipher") (v "0.1.0") (d (list (d (n "chacha20poly1305") (r "^0.9") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rust-argon2") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zcv4j9rfhshs1msyn2r6j2vwsxcbf6r6c2nnfd29zgcc24bglvq")))

(define-public crate-include-crypt-bytes-cipher-0.1.1 (c (n "include-crypt-bytes-cipher") (v "0.1.1") (d (list (d (n "chacha20poly1305") (r "^0.10") (f (quote ("std"))) (d #t) (k 0)) (d (n "crypto-common") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rust-argon2") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04sy4c6km16hb99gmjs4jj1fgg1rrrkm9mc09qbncj127z4pk8z7")))

