(define-module (crates-io in cl incli) #:use-module (crates-io))

(define-public crate-incli-0.1.0 (c (n "incli") (v "0.1.0") (d (list (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)))) (h "041w4zdbvjmian8ddhzrkz8hx6cijkmnidpb3a8f6730dyx795nm") (y #t)))

(define-public crate-incli-0.1.1 (c (n "incli") (v "0.1.1") (d (list (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)))) (h "02z6f47xv0kmp5p2q5b6liac0znbp4j151bjwylybx4yz9ci2fqw")))

(define-public crate-incli-0.2.0 (c (n "incli") (v "0.2.0") (d (list (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)))) (h "12wh1k8av3h5zc739w1dq9k3mi689wz5y0yid3rkhskygm9npj1n")))

(define-public crate-incli-0.2.1 (c (n "incli") (v "0.2.1") (d (list (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)))) (h "06xck3080d78z865q0pnp5xga838kd47qdm4q5a6vg6cvl64zckv")))

(define-public crate-incli-0.3.0 (c (n "incli") (v "0.3.0") (d (list (d (n "sys-info-extended") (r "^0.1.2") (d #t) (k 0)))) (h "1b1m5cv0f6qvzlws7lcl1bx87jafvdnzvwsw120qlywg1vd2b2zi")))

(define-public crate-incli-0.3.1 (c (n "incli") (v "0.3.1") (d (list (d (n "sys-info-extended") (r "^0.2.0") (d #t) (k 0)))) (h "0sgphlh7020x7kwq6pdyi5ncavq4v1v7ia2rs91scam2in0cx46n")))

(define-public crate-incli-0.4.0 (c (n "incli") (v "0.4.0") (d (list (d (n "sys-info-extended") (r "^0.2.2") (d #t) (k 0)))) (h "1nw89kg9cxb8svdcl5izibzja0lgrlhckqnp5cnw95m37z2kxwcg")))

(define-public crate-incli-0.5.0 (c (n "incli") (v "0.5.0") (d (list (d (n "sys-info-extended") (r "^0.2.2") (d #t) (k 0)))) (h "06fza80r2k7jrfy3mby8ypjl2ha3zrplwzrd13c9a8n8xfm9vm5g")))

(define-public crate-incli-0.5.1 (c (n "incli") (v "0.5.1") (d (list (d (n "sys-info-extended") (r "^0.3.0") (d #t) (k 0)))) (h "0zxxhhww0zvxd4b1jayg6qj82fgvhhl6w8jfnn2gj8783fcnqgvd")))

(define-public crate-incli-0.5.2 (c (n "incli") (v "0.5.2") (d (list (d (n "sys-info-extended") (r "^0.3.0") (d #t) (k 0)))) (h "1wnyw1njqpmxwcy6b7a8pll6g4vyr438bj2qqp99cm1bv1iwiqf2")))

(define-public crate-incli-0.5.3 (c (n "incli") (v "0.5.3") (d (list (d (n "sys-info-extended") (r "^0.3.0") (d #t) (k 0)))) (h "03spw7fgyd6vn211zxjp0vg1zrfbbdfxm47q1smx3fix967wm7i2")))

(define-public crate-incli-0.5.4 (c (n "incli") (v "0.5.4") (d (list (d (n "sys-info-extended") (r "^0.4.0") (d #t) (k 0)))) (h "1pbcvabb7wina1i7pq5cg24ijigcyjk56dh6ws3w41vb1sxjsdrr")))

(define-public crate-incli-0.5.5 (c (n "incli") (v "0.5.5") (d (list (d (n "sys-info-extended") (r "^0.4.0") (d #t) (k 0)))) (h "0sh20kk86n00z8rh82f6prraxjh6y36gk1pw6zbhpjccf9nh56ja")))

(define-public crate-incli-0.5.6 (c (n "incli") (v "0.5.6") (d (list (d (n "sys-info-extended") (r "^0.4.0") (d #t) (k 0)))) (h "1svxphaqijmmi1mchirk0q6784j0nniv3sfd19p53f8958j46b4g")))

