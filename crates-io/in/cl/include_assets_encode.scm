(define-module (crates-io in cl include_assets_encode) #:use-module (crates-io))

(define-public crate-include_assets_encode-0.1.0 (c (n "include_assets_encode") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "include_assets_decode") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "smartstring") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.17") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0z6j3ib53kc3nqfgzdw2rpq0ipp5gp41q6q2ghnrnvbjpqdg3yy3") (f (quote (("zstd" "include_assets_decode/zstd") ("lz4" "include_assets_decode/lz4") ("deflate" "include_assets_decode/deflate") ("default"))))))

