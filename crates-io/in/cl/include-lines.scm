(define-module (crates-io in cl include-lines) #:use-module (crates-io))

(define-public crate-include-lines-1.0.0 (c (n "include-lines") (v "1.0.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1y0b7bbh3l69by7mgraf4kjacicsw2r138vd3pydlxsqj3aqgxwp")))

(define-public crate-include-lines-1.1.0 (c (n "include-lines") (v "1.1.0") (d (list (d (n "include-lines-proc") (r "^1.1.0") (d #t) (k 0)))) (h "1ic54w7s9f0ilkchjs2vka60fghfynhlklk6p740zv370ypvjjj8")))

(define-public crate-include-lines-1.1.1 (c (n "include-lines") (v "1.1.1") (d (list (d (n "include-lines-proc") (r "^1.1.0") (d #t) (k 0)))) (h "131dsfzgr8m49rki37bh8fwybb3zb6kd2c7vfhyd6701a5iazcs0")))

(define-public crate-include-lines-1.1.2 (c (n "include-lines") (v "1.1.2") (d (list (d (n "include-lines-proc") (r "^1.1.0") (d #t) (k 0)))) (h "1fgdry8bhips4lfgb0h2wp5nihiv2frjh0k8s3g7nkgpjjs3gq2b")))

