(define-module (crates-io in cl include_dir_bytes) #:use-module (crates-io))

(define-public crate-include_dir_bytes-0.1.0 (c (n "include_dir_bytes") (v "0.1.0") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)))) (h "1dni3nb23bc538ndrjsx7nziqsnmz426mfz3qfb0qglwkmcvk30w")))

(define-public crate-include_dir_bytes-0.2.0 (c (n "include_dir_bytes") (v "0.2.0") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)))) (h "1hw2y45lgqyc3rq77v1vw5z36hlhq616y45xky1jwj3b9rj6ak0j")))

(define-public crate-include_dir_bytes-0.2.1 (c (n "include_dir_bytes") (v "0.2.1") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)))) (h "0s87xv9khh2wfs713ic5bfilsgmck3w1xi625j9l26dcbdvmx8ld")))

