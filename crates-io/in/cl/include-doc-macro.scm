(define-module (crates-io in cl include-doc-macro) #:use-module (crates-io))

(define-public crate-include-doc-macro-0.2.0 (c (n "include-doc-macro") (v "0.2.0") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "^0.0.164") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (d #t) (k 0)))) (h "12rs3vp41300shcaq9pv3q6x7n7yb1k65s5kxy7x5g7f9g0062dv")))

(define-public crate-include-doc-macro-0.2.1 (c (n "include-doc-macro") (v "0.2.1") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "^0.0.164") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (d #t) (k 0)))) (h "0hhi5bl4h2a87bvaz31plxfj8fkdl5819rybdb3bz06xzwdixklb")))

