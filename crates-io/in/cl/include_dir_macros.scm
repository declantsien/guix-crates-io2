(define-module (crates-io in cl include_dir_macros) #:use-module (crates-io))

(define-public crate-include_dir_macros-0.7.0 (c (n "include_dir_macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1xgw2mxlxl8nlfcqglpk4r8llrzg3bcivm1wj2k8k539b9idqlfm") (f (quote (("nightly") ("metadata")))) (r "1.56")))

(define-public crate-include_dir_macros-0.7.1 (c (n "include_dir_macros") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0v82cqmyvkcs60fj6csnvip0w00ydrlds07r10r6fy5wd8asafr5") (f (quote (("nightly") ("metadata")))) (r "1.56")))

(define-public crate-include_dir_macros-0.7.2 (c (n "include_dir_macros") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "16z81fhma6w5rgcbcz9hvszj0s1xz9h1ifligi0029dbvqclq1sy") (f (quote (("nightly") ("metadata")))) (r "1.56")))

(define-public crate-include_dir_macros-0.7.3 (c (n "include_dir_macros") (v "0.7.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0gsa6z58wxgw9j58w60wyjpk2nv3pd86kimw2akwyzpmbi5jhfdi") (f (quote (("nightly") ("metadata")))) (r "1.56")))

