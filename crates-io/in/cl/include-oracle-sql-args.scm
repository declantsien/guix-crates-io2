(define-module (crates-io in cl include-oracle-sql-args) #:use-module (crates-io))

(define-public crate-include-oracle-sql-args-0.1.0 (c (n "include-oracle-sql-args") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1myz4685m00438d1k38vikgldaydn7fgzanxh4nhh0y08z23nnyx")))

(define-public crate-include-oracle-sql-args-0.1.1 (c (n "include-oracle-sql-args") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1d727hsffx2pd7nryrb43f9j198wramjfvdlnb7cba67iim4my5a")))

