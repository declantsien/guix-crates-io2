(define-module (crates-io in cl include-crypt-bytes-macro) #:use-module (crates-io))

(define-public crate-include-crypt-bytes-macro-0.1.0 (c (n "include-crypt-bytes-macro") (v "0.1.0") (d (list (d (n "include-crypt-bytes-cipher") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0rcgb44q6iabkphqs07c2q1jymk599v36cib0l7k9vi6x4shmg60")))

