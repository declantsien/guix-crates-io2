(define-module (crates-io in cl include-transformed) #:use-module (crates-io))

(define-public crate-include-transformed-0.1.0 (c (n "include-transformed") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "03g3zaar5wlykpzg4lwyc49lv3iybzwai1sbix99qx2x0qhm72xb")))

(define-public crate-include-transformed-0.2.0 (c (n "include-transformed") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "18xc23vxpnhsfpg92h1rd92ffjwdndad4g9scfkg0di7km9w7fcc")))

(define-public crate-include-transformed-0.2.1 (c (n "include-transformed") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1ilmn2zhppdv58zlvwpqpfxvbjzvrsdphc79kzlaw7sav17anqsv")))

(define-public crate-include-transformed-0.2.2 (c (n "include-transformed") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0gd1f2d8qlgl0q6nfkwjvrb6fwj4h9g508c25fxqllszg31hmpcz")))

