(define-module (crates-io in cl include-crypt-crypto) #:use-module (crates-io))

(define-public crate-include-crypt-crypto-0.1.0 (c (n "include-crypt-crypto") (v "0.1.0") (d (list (d (n "aes") (r "^0.6.0") (d #t) (k 0)) (d (n "cfb-mode") (r "^0.6.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0kjgp7mgpnbpk9f8yir4ksd4jsci0dy7hc20pd8fp2cb82xv3jzn")))

