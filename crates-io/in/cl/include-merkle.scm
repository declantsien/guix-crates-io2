(define-module (crates-io in cl include-merkle) #:use-module (crates-io))

(define-public crate-include-merkle-0.1.0 (c (n "include-merkle") (v "0.1.0") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "ptree") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "14fwagcjsb51z45f1p2mznandqakfx931xhd77zd16vn6j6qlgva")))

(define-public crate-include-merkle-0.1.1 (c (n "include-merkle") (v "0.1.1") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "ptree") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1rsid3xbaz81wvlmqxigblij3mhjv6xy14d82pjp1bw5n6dr8d4n")))

(define-public crate-include-merkle-0.1.2 (c (n "include-merkle") (v "0.1.2") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "ptree") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0v1p2lklr8r1yhqp6gl24h74gr2d8z62nggm3x0h4973rvdnm86z")))

(define-public crate-include-merkle-0.1.3 (c (n "include-merkle") (v "0.1.3") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "normalize-line-endings") (r "^0.3.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "ptree") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0hskpgmf6537jvbhm32v1c7892m9v46sqqkns3scnxzfmbis5svn")))

(define-public crate-include-merkle-0.1.4 (c (n "include-merkle") (v "0.1.4") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 0)) (d (n "chardet") (r "^0.2") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "normalize-line-endings") (r "^0.3.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.4.13") (d #t) (k 0)) (d (n "ptree") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1hrb1gp0hhqpnln4b0v8209ix3hhz6ciw9gx1gldrj4hi4g1743c")))

