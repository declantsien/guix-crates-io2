(define-module (crates-io in cl include_path) #:use-module (crates-io))

(define-public crate-include_path-0.1.0 (c (n "include_path") (v "0.1.0") (d (list (d (n "litrs") (r "^0.2.3") (d #t) (k 0)))) (h "0ch8j7lj7rx2ha8xxaz75drc64a4k2k5qq0ij29bwz4fwmzh0a7s")))

(define-public crate-include_path-0.1.1 (c (n "include_path") (v "0.1.1") (d (list (d (n "litrs") (r "^0.2.3") (d #t) (k 0)))) (h "1jnhpwm9aw71lb0rihwnz8g0zpm69kczfnisy1qjngwpjz3fcnb2")))

