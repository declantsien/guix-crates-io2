(define-module (crates-io in cl include-oracle-sql) #:use-module (crates-io))

(define-public crate-include-oracle-sql-0.1.0 (c (n "include-oracle-sql") (v "0.1.0") (d (list (d (n "include-oracle-sql-args") (r "^0.1") (d #t) (k 0)) (d (n "include-sql") (r "^0.2") (d #t) (k 0)) (d (n "sibyl") (r "^0.6") (f (quote ("blocking"))) (d #t) (k 2)))) (h "0am76z758m48cgkl8hq4hhrhs9ka6zk1i92ipzkk4p8g1vb2rhl7")))

(define-public crate-include-oracle-sql-0.1.1 (c (n "include-oracle-sql") (v "0.1.1") (d (list (d (n "include-oracle-sql-args") (r "^0.1") (d #t) (k 0)) (d (n "include-sql") (r "^0.2") (d #t) (k 0)) (d (n "sibyl") (r "^0.6") (f (quote ("blocking"))) (d #t) (k 2)))) (h "1rc9grllkcnd893lkikg91i1iqwyfj7jslk65k5y3qch3n6g4pn6")))

(define-public crate-include-oracle-sql-0.2.0 (c (n "include-oracle-sql") (v "0.2.0") (d (list (d (n "include-oracle-sql-args") (r "^0.1") (d #t) (k 0)) (d (n "include-sql") (r "^0.3") (d #t) (k 0)) (d (n "sibyl") (r "^0.6") (f (quote ("blocking"))) (d #t) (k 2)))) (h "069a13ar8vm4fc32j8z0fmasfa5hmagfvqwaca8kd5vz5mvljr06")))

(define-public crate-include-oracle-sql-0.2.1 (c (n "include-oracle-sql") (v "0.2.1") (d (list (d (n "include-oracle-sql-args") (r "^0.1") (d #t) (k 0)) (d (n "include-sql") (r "^0.3") (d #t) (k 0)) (d (n "sibyl") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0abxgv7paz8pc0f6v9wsb8xildhxzzvascv7xqmvqlz1aa9w7xig") (f (quote (("tokio" "include-sql/async" "sibyl/nonblocking" "sibyl/tokio") ("default" "sibyl/blocking"))))))

(define-public crate-include-oracle-sql-0.2.2 (c (n "include-oracle-sql") (v "0.2.2") (d (list (d (n "include-oracle-sql-args") (r "^0.1") (d #t) (k 0)) (d (n "include-sql") (r "^0.3") (d #t) (k 0)) (d (n "sibyl") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1cd5qbqncimzhzq446pn92mawr4sb4xdmyz2m1m58z1xlic1q57l") (f (quote (("tokio" "include-sql/async" "sibyl/nonblocking" "sibyl/tokio") ("default" "sibyl/blocking"))))))

