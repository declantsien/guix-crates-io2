(define-module (crates-io in cl include_js) #:use-module (crates-io))

(define-public crate-include_js-0.1.0 (c (n "include_js") (v "0.1.0") (d (list (d (n "handlebars") (r "^3.5.5") (o #t) (d #t) (k 0)) (d (n "include_js_codegen") (r "^0.1") (d #t) (k 0)) (d (n "include_js_core") (r "^0.1.1") (d #t) (k 0)))) (h "09ilfp51jc5f36jxarkd64lp4gkvbwzwhn84awl2rf6yh057bxj3") (f (quote (("template" "include_js_codegen/template" "handlebars") ("default" "template")))) (y #t)))

(define-public crate-include_js-0.1.1 (c (n "include_js") (v "0.1.1") (d (list (d (n "handlebars") (r "^3.5.5") (o #t) (d #t) (k 0)) (d (n "include_js_codegen") (r "^0.1") (d #t) (k 0)) (d (n "include_js_core") (r "^0.1.1") (d #t) (k 0)))) (h "0sfbyl4bxkq8ra5hpd5azm59jd5q363gmf5ycyci3knlxfs5kmfp") (f (quote (("template" "include_js_codegen/template" "handlebars") ("default" "template")))) (y #t)))

(define-public crate-include_js-0.1.2 (c (n "include_js") (v "0.1.2") (d (list (d (n "handlebars") (r "^3.5.5") (o #t) (d #t) (k 0)) (d (n "include_js_codegen") (r "^0.1.1") (d #t) (k 0)) (d (n "include_js_core") (r "^0.1.2") (d #t) (k 0)))) (h "01dad7j9napqd8x846ad02xbmm3am8c9iflhh6vcmmp4vc9kwp82") (f (quote (("template" "include_js_codegen/template" "handlebars") ("default" "template"))))))

