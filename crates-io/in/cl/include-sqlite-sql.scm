(define-module (crates-io in cl include-sqlite-sql) #:use-module (crates-io))

(define-public crate-include-sqlite-sql-0.1.0 (c (n "include-sqlite-sql") (v "0.1.0") (d (list (d (n "include-sql") (r "^0.2") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26") (f (quote ("bundled"))) (d #t) (k 2)))) (h "04fzrlb0fq9giy1wad3855aswnbb7p3srb4a0xjf9bv6y9wr2c4c")))

(define-public crate-include-sqlite-sql-0.1.1 (c (n "include-sqlite-sql") (v "0.1.1") (d (list (d (n "include-sql") (r "^0.2") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26") (f (quote ("bundled"))) (d #t) (k 2)))) (h "1f84y5iajwqswlvrhmwnhiwr8y1ai4fnfsr9w3jid1k3v0b3anfq")))

(define-public crate-include-sqlite-sql-0.1.2 (c (n "include-sqlite-sql") (v "0.1.2") (d (list (d (n "include-sql") (r "^0.2") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26") (d #t) (k 0)))) (h "1si5rsh1djw5l8licykw2mpvjs39k8cs3rrsplaazhj75qgxm1nd")))

(define-public crate-include-sqlite-sql-0.1.3 (c (n "include-sqlite-sql") (v "0.1.3") (d (list (d (n "include-sql") (r "^0.2") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26") (d #t) (k 0)))) (h "1srnkn11svddvf83203ml56ii4m16pbw0nqwpafsj9j1zx78ll5z")))

(define-public crate-include-sqlite-sql-0.1.5 (c (n "include-sqlite-sql") (v "0.1.5") (d (list (d (n "include-sql") (r "^0.2") (d #t) (k 0)) (d (n "rusqlite") (r ">=0.25") (d #t) (k 2)))) (h "0b4v75c8r95isryl8j805nflgrv1ga1r25js1hkcjs6b65glgi9z")))

(define-public crate-include-sqlite-sql-0.2.0 (c (n "include-sqlite-sql") (v "0.2.0") (d (list (d (n "include-sql") (r "^0.3") (d #t) (k 0)) (d (n "rusqlite") (r ">=0.25") (d #t) (k 2)))) (h "1n5r5hz3x1ir1h18kd8rwpyy591qfv1gkklbcpncr352bk66943l")))

(define-public crate-include-sqlite-sql-0.2.1 (c (n "include-sqlite-sql") (v "0.2.1") (d (list (d (n "include-sql") (r "^0.3") (d #t) (k 0)) (d (n "rusqlite") (r ">=0.25") (d #t) (k 2)))) (h "02fj7jg48qbw1phbi7rsganp9n6s14cak0p5bhqmshl7p332qvyn")))

