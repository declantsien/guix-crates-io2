(define-module (crates-io in cl include_file_path) #:use-module (crates-io))

(define-public crate-include_file_path-0.1.0 (c (n "include_file_path") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1hg0r090vrwjp16yzxm5xhirgxk95293ym6c16ph9azml8qkf5lf") (y #t)))

(define-public crate-include_file_path-0.1.1 (c (n "include_file_path") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0lpz1wvxahg7qvgndi97p0a0fx4l3pkwk3pxx4a0w9bmdqavng3w") (y #t)))

(define-public crate-include_file_path-0.1.2 (c (n "include_file_path") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1xj43qx3yrnif02c8x78qvs7zj9rjs2xsgz8kvszkys6prs3svbk") (y #t)))

