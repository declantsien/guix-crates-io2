(define-module (crates-io in cl includer_codegen) #:use-module (crates-io))

(define-public crate-includer_codegen-0.1.0 (c (n "includer_codegen") (v "0.1.0") (d (list (d (n "includer") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.14") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "14yfch9nnwila73wjsiz39870dcjjjgx559jazkhv4qc5svb4pph") (f (quote (("web"))))))

(define-public crate-includer_codegen-0.1.1 (c (n "includer_codegen") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0z0qqrs07lh9917k816k76m7cs3b4hnrkf53bvcfnngqiv045akr") (f (quote (("web"))))))

(define-public crate-includer_codegen-0.2.0 (c (n "includer_codegen") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1a5y2gxy6m7wjj4p31jx2y7k911n3l402pyqhn6m4jic0q13dy5x") (f (quote (("web") ("default" "web"))))))

(define-public crate-includer_codegen-0.2.1 (c (n "includer_codegen") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0xqmgbqs2yr9vyqfgd0jsdiskq71q93fjbsrd92p60gfmdni9cds") (f (quote (("web") ("default" "web"))))))

