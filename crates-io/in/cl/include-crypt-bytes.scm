(define-module (crates-io in cl include-crypt-bytes) #:use-module (crates-io))

(define-public crate-include-crypt-bytes-0.1.0 (c (n "include-crypt-bytes") (v "0.1.0") (d (list (d (n "include-crypt-bytes-cipher") (r "^0.1") (d #t) (k 0)) (d (n "include-crypt-bytes-macro") (r "^0.1") (d #t) (k 0)))) (h "0f4dg5iz9mjfq5g1zshnxcg0yjjln3yxj1gzanhlmxsdz6jzkl4b")))

(define-public crate-include-crypt-bytes-0.1.1 (c (n "include-crypt-bytes") (v "0.1.1") (d (list (d (n "include-crypt-bytes-cipher") (r "^0.1") (d #t) (k 0)) (d (n "include-crypt-bytes-macro") (r "^0.1") (d #t) (k 0)))) (h "0h0asbwn418zq7dv3x4v6y54m8lq9km4s3n07xkicd6h0jw4cwh1")))

(define-public crate-include-crypt-bytes-0.1.2 (c (n "include-crypt-bytes") (v "0.1.2") (d (list (d (n "include-crypt-bytes-cipher") (r "^0.1") (d #t) (k 0)) (d (n "include-crypt-bytes-macro") (r "^0.1") (d #t) (k 0)))) (h "0v2waf5vj4xfblp3gs9bavmsj1j3vw2wgsgdcp10qamrwd872qwd")))

(define-public crate-include-crypt-bytes-0.1.3 (c (n "include-crypt-bytes") (v "0.1.3") (d (list (d (n "include-crypt-bytes-cipher") (r "^0.1") (d #t) (k 0)) (d (n "include-crypt-bytes-macro") (r "^0.1") (d #t) (k 0)))) (h "18ixr22jp7h9xa64h42lm4axvlqkzrb8dlym0y3dxq4zd8vji32l")))

