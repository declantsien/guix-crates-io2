(define-module (crates-io in cl inclip) #:use-module (crates-io))

(define-public crate-inclip-1.0.0 (c (n "inclip") (v "1.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "clipboard") (r "^0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "0y0akdk2c1rh41ap8xsamwlwz81p62g1bivs56ynzwbxj8cbfnnv")))

(define-public crate-inclip-1.0.2 (c (n "inclip") (v "1.0.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "clipboard") (r "^0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "0m7yq5zbqdff14rq5qyafsd1jfpn776gq7iz649mn7bchjrcrrdq")))

(define-public crate-inclip-2.0.0 (c (n "inclip") (v "2.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "clipboard") (r "^0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "1nisrp5syn188f61xyxb02hwbdjfj43g80hfz5ra1ylrpg1iq9gh")))

(define-public crate-inclip-2.0.1 (c (n "inclip") (v "2.0.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "clipboard") (r "^0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "1xijk8xn4994fkipwm5qmhh24nj5jd8zb0w56vka95yq614vh2cy")))

