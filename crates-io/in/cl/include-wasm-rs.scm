(define-module (crates-io in cl include-wasm-rs) #:use-module (crates-io))

(define-public crate-include-wasm-rs-0.1.0 (c (n "include-wasm-rs") (v "0.1.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "0lhz6a4dd6bxiyjzd8vjrbdcrq839jxqiaq7xr52zpawxm033l8q")))

(define-public crate-include-wasm-rs-0.1.1 (c (n "include-wasm-rs") (v "0.1.1") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro" "printing" "full"))) (k 0)))) (h "1hyi7gks93bf6bbg56pdlx3flsl1z19p2ckp066nnmsjhdhazjx1")))

(define-public crate-include-wasm-rs-0.1.2 (c (n "include-wasm-rs") (v "0.1.2") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro" "printing" "full" "extra-traits"))) (k 0)))) (h "0d6ypvnzjzlvsakzgq9vvbwdgpnkd6sqflln3hw6nax732bqkfy2")))

(define-public crate-include-wasm-rs-0.1.3 (c (n "include-wasm-rs") (v "0.1.3") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro" "printing" "full" "extra-traits"))) (k 0)))) (h "0ffpkmxps1ydflpl2xs9hqcy9lpkaqy5d2zq60506y08c7qxiwps")))

