(define-module (crates-io in cl include-bytes-zstd-macro) #:use-module (crates-io))

(define-public crate-include-bytes-zstd-macro-0.1.0 (c (n "include-bytes-zstd-macro") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "zstd") (r "^0.12.3") (d #t) (k 0)))) (h "0xxh3yz8c2vnzyn9q7vn3bw1whkhay5snv99f6bccw3s0w7qaqq6") (r "1.64")))

