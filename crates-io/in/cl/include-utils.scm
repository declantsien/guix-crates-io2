(define-module (crates-io in cl include-utils) #:use-module (crates-io))

(define-public crate-include-utils-0.1.0 (c (n "include-utils") (v "0.1.0") (d (list (d (n "include-utils-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1p5wjm2sfq6ycnlkc03ypfj2qnprqb0shzqjmybs5sxa8x9y3487")))

(define-public crate-include-utils-0.1.1 (c (n "include-utils") (v "0.1.1") (d (list (d (n "include-utils-macro") (r "^0.1.1") (d #t) (k 0)))) (h "1672kqrjvm8x48ldh7xwipvzcfnhrffz1azrafvjss7hawqg5p9f")))

(define-public crate-include-utils-0.2.0 (c (n "include-utils") (v "0.2.0") (d (list (d (n "include-utils-macro") (r "^0.2.0") (d #t) (k 0)))) (h "0jd6q7x0bxrdlxnm6xdia8z0p3d1z9nlxx60mmaagrsgl32ls2md") (f (quote (("workspace" "include-utils-macro/workspace") ("default"))))))

(define-public crate-include-utils-0.2.1 (c (n "include-utils") (v "0.2.1") (d (list (d (n "include-utils-macro") (r "^0.2.0") (d #t) (k 0)))) (h "11l3sif4xqr774dkdkwpsfab38aqhaw08mr8gji5p0zspmqy3gym") (f (quote (("workspace" "include-utils-macro/workspace") ("default"))))))

(define-public crate-include-utils-0.2.2 (c (n "include-utils") (v "0.2.2") (d (list (d (n "include-utils-macro") (r "^0.2.0") (d #t) (k 0)))) (h "0ld2392bv1zkydrlxy0vygbl4qk15kxvnk6i0gjsvz8w0rah1fgi") (f (quote (("workspace" "include-utils-macro/workspace") ("default"))))))

