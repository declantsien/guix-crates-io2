(define-module (crates-io in cl include_wgsl) #:use-module (crates-io))

(define-public crate-include_wgsl-1.0.0 (c (n "include_wgsl") (v "1.0.0") (d (list (d (n "naga") (r "^0.4") (f (quote ("wgsl-in"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18nf6xy3zbnlpscrfklpmjbvqjnxcz985hv6rwwxrmcvn1k7vq2x")))

(define-public crate-include_wgsl-1.1.0 (c (n "include_wgsl") (v "1.1.0") (d (list (d (n "naga") (r "^0.7") (f (quote ("wgsl-in" "validate"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1l7wg4ym4zlblpv534kyxl5zb2fl8xpn8nm6iv72aianlzbayhnq") (f (quote (("spv-out" "naga/spv-out"))))))

(define-public crate-include_wgsl-1.1.1 (c (n "include_wgsl") (v "1.1.1") (d (list (d (n "naga") (r "^0.7") (f (quote ("wgsl-in" "validate"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ixsp4n4wxy3wfvm1zkpj5vqnm8siwz4by6b8zplm9kld51jib2p") (f (quote (("spv-out" "naga/spv-out"))))))

