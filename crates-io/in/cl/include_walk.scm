(define-module (crates-io in cl include_walk) #:use-module (crates-io))

(define-public crate-include_walk-0.1.0 (c (n "include_walk") (v "0.1.0") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0g7andqxhas1vmxdb6cf5j5pzd55vx59878ksab259ns4qwqpfym")))

(define-public crate-include_walk-0.1.1 (c (n "include_walk") (v "0.1.1") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "13p7lc39gckm3j278hs0r2q6ilwbrrp2088d7warjsf6hlm6vyh3")))

(define-public crate-include_walk-0.2.0 (c (n "include_walk") (v "0.2.0") (d (list (d (n "insta") (r "^0.13.1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0pr6kcxs51zwljsnhldynfrh2hs8wwpx3142chscbqm5215h85ca")))

(define-public crate-include_walk-0.2.1 (c (n "include_walk") (v "0.2.1") (d (list (d (n "common-path") (r "^1.0.0") (d #t) (k 0)) (d (n "insta") (r "^0.13.1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1q1bhw63izw4s4v66gs34d82kvbvzwks48liqnsy62c10c90p6q8")))

(define-public crate-include_walk-0.2.2 (c (n "include_walk") (v "0.2.2") (d (list (d (n "common-path") (r "^1.0.0") (d #t) (k 0)) (d (n "insta") (r "^0.13.1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0ax1vvxlbhr6fsgpxlhxjbnd11nxqjf82m6rc2ld8vkyw2wrdjap")))

(define-public crate-include_walk-0.3.0 (c (n "include_walk") (v "0.3.0") (d (list (d (n "common-path") (r "^1.0.0") (d #t) (k 0)) (d (n "insta") (r "^0.13.1") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "183pky44s28vgkcqg87j76kmq4f5z7dr81mgpwsryc5gzhpqkz4v")))

(define-public crate-include_walk-0.3.1 (c (n "include_walk") (v "0.3.1") (d (list (d (n "common-path") (r "^1.0.0") (d #t) (k 0)) (d (n "insta") (r "^0.15.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1xk078s135r9hdlh96an8mffpxsq7lmd1lcij7kfc0vwvifrii70")))

(define-public crate-include_walk-0.3.2 (c (n "include_walk") (v "0.3.2") (d (list (d (n "common-path") (r "^1.0.0") (d #t) (k 0)) (d (n "insta") (r "^0.16.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0lqgvq1fyav2fhyfg4s6pwi332yp5wf1ljrzc3afksxzp144srcf")))

