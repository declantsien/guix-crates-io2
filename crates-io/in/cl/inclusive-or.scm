(define-module (crates-io in cl inclusive-or) #:use-module (crates-io))

(define-public crate-inclusive-or-0.1.0 (c (n "inclusive-or") (v "0.1.0") (h "0hx9yn65mxrg79ba8jq4pb935yv0qll4w5vxjy7hwyaymvhb7flg") (y #t)))

(define-public crate-inclusive-or-0.0.1 (c (n "inclusive-or") (v "0.0.1") (h "0kyd3mjdx3phn4fih2rgyrrkp3wgzld9vcbzn9zmx1bi8pg6zfs7")))

