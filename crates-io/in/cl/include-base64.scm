(define-module (crates-io in cl include-base64) #:use-module (crates-io))

(define-public crate-include-base64-0.1.0 (c (n "include-base64") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro" "full"))) (k 0)))) (h "0w5ras8jqpl2scchfd36bkl5fxzm8sbxyhhmd0w2ssskwkfcg85n")))

