(define-module (crates-io in cl include-repo) #:use-module (crates-io))

(define-public crate-include-repo-0.1.0 (c (n "include-repo") (v "0.1.0") (d (list (d (n "include-repo-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4.0") (d #t) (k 0)))) (h "1i34f6qhc8188dh6ybx6jfypq8xg70a3x9s05lh5a3c06r0vqcs6")))

(define-public crate-include-repo-1.0.0 (c (n "include-repo") (v "1.0.0") (d (list (d (n "include-repo-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "libflate") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.70") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "15dacqjjd5vasqjbki7kkahpd3mzavxw0hy6jyk21qgq7spnwkyx")))

