(define-module (crates-io in cl includedir_codegen) #:use-module (crates-io))

(define-public crate-includedir_codegen-0.2.0 (c (n "includedir_codegen") (v "0.2.0") (d (list (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.12") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "0inwvgk2xafqryl6fcyafv3mg9x9a4cdg47qf28lczb11p9n1cpn")))

(define-public crate-includedir_codegen-0.2.1 (c (n "includedir_codegen") (v "0.2.1") (d (list (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.12") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "1imyhncs10kn2iqn5b7amilq4xf63klia3v9m5zn4kfbhk95kkhs")))

(define-public crate-includedir_codegen-0.2.2 (c (n "includedir_codegen") (v "0.2.2") (d (list (d (n "flate2") (r "^0.2.14") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.16") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "0pn9ydacwzz69i91256c83mz234nqbl5r1n73x4ap5llwy3bpcjc")))

(define-public crate-includedir_codegen-0.3.0 (c (n "includedir_codegen") (v "0.3.0") (d (list (d (n "flate2") (r "^1.0.1") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.21") (d #t) (k 0)) (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "009y4zjv3wyaapg8pfms1vz4zwcb9w4varpakc7d1z7vx59xjx9h")))

(define-public crate-includedir_codegen-0.4.0 (c (n "includedir_codegen") (v "0.4.0") (d (list (d (n "flate2") (r "^1.0.1") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.21") (d #t) (k 0)) (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "1p9y5n1agzdnf7mlzycs0icyw56n2bilma9pfkw60ny8n7vcl170")))

(define-public crate-includedir_codegen-0.5.0 (c (n "includedir_codegen") (v "0.5.0") (d (list (d (n "flate2") (r "^1.0.1") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.21") (d #t) (k 0)) (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "1vxdrhfdfirhjnldrl94fx90jk6c2rn57ywjas2q9z8kw4mm8zdg")))

(define-public crate-includedir_codegen-0.6.0 (c (n "includedir_codegen") (v "0.6.0") (d (list (d (n "flate2") (r "^1.0.14") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0j1i7qjgnzffidd3z0w32jqald9g02a3pv6412c7r5c0jw651h8a")))

