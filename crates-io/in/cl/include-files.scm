(define-module (crates-io in cl include-files) #:use-module (crates-io))

(define-public crate-include-files-0.1.0 (c (n "include-files") (v "0.1.0") (h "1pcgjy0dxdkayx4jhi8nyaww1dla0jdvi057wb1gldn28zc4h6x6") (y #t)))

(define-public crate-include-files-0.1.1 (c (n "include-files") (v "0.1.1") (h "0zxfxzhlj1gqf6n3arwjgdyys8ay8948vsc4j1f7ad1wy0p872b9") (y #t)))

(define-public crate-include-files-0.1.2 (c (n "include-files") (v "0.1.2") (h "1snf6j529hf559d4701alf6ihs0x2s2mlgdk06cp0f3bkbxvcf59")))

