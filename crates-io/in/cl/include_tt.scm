(define-module (crates-io in cl include_tt) #:use-module (crates-io))

(define-public crate-include_tt-1.0.0 (c (n "include_tt") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)))) (h "101gz6g1qaiw22y0p3qyc5bgxmvw6wg5svnhymp8vgkgrbw2l79l")))

(define-public crate-include_tt-1.0.1 (c (n "include_tt") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)))) (h "1ggv3amqz38dglzwkn1q44iz0jjbwzbdm0yx6aa17cy797519zkb")))

(define-public crate-include_tt-1.0.2 (c (n "include_tt") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)))) (h "0jmb0rfz96hv549l3q3h248vgcmzg7y0bbady1g9jnhrkd49ck11")))

(define-public crate-include_tt-1.0.3 (c (n "include_tt") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.57") (f (quote ("full"))) (d #t) (k 0)))) (h "1jkcvssds5plxmy9183lnkxq35dfhmlcbc93abmjhb61k7wvghdh")))

(define-public crate-include_tt-1.0.4 (c (n "include_tt") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.63") (f (quote ("parsing"))) (k 0)))) (h "1pdkx43cwjng8jqgkc2223amzklml8vqg3cf148akhvhb92isd00")))

