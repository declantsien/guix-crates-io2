(define-module (crates-io in cl include_assets_decode) #:use-module (crates-io))

(define-public crate-include_assets_decode-0.1.0 (c (n "include_assets_decode") (v "0.1.0") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "hexhex") (r "^1.0.0") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.10.0") (f (quote ("std" "safe-encode" "safe-decode"))) (o #t) (k 0)) (d (n "smartstring") (r "^1.0.1") (d #t) (k 0)) (d (n "yazi") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "zstd") (r "^0.12.3") (o #t) (k 0)))) (h "0wmpq9y9ahhk04xxmvkvchbn15kn7hj9wkxcjarpn00l163fh6bj") (f (quote (("default")))) (s 2) (e (quote (("zstd" "dep:zstd") ("lz4" "dep:lz4_flex") ("deflate" "dep:yazi"))))))

