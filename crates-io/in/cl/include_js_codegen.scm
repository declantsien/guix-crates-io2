(define-module (crates-io in cl include_js_codegen) #:use-module (crates-io))

(define-public crate-include_js_codegen-0.1.0 (c (n "include_js_codegen") (v "0.1.0") (d (list (d (n "Boa") (r "^0.11.0") (d #t) (k 0)) (d (n "handlebars") (r "^3.5.5") (o #t) (d #t) (k 0)) (d (n "include_js_core") (r "^0.1.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0q8ygcbd5z5pwh82p8cnymall554r1lnnp05dp2fgy7ga9hva8h8") (f (quote (("template" "handlebars") ("default" "template")))) (y #t)))

(define-public crate-include_js_codegen-0.1.1 (c (n "include_js_codegen") (v "0.1.1") (d (list (d (n "Boa") (r "^0.11.0") (d #t) (k 0)) (d (n "handlebars") (r "^3.5.5") (o #t) (d #t) (k 0)) (d (n "include_js_core") (r "^0.1.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zmcn82n5zbbgxghcngwpfzv4fn6v0ssinbqvl6mi72rjy3nbsp9") (f (quote (("template" "handlebars") ("default" "template"))))))

