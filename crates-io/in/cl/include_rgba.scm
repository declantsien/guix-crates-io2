(define-module (crates-io in cl include_rgba) #:use-module (crates-io))

(define-public crate-include_rgba-0.1.0 (c (n "include_rgba") (v "0.1.0") (d (list (d (n "image") (r "^0.23.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.12") (d #t) (k 0)))) (h "0hfbf3wmp8zmpw8gi5ancwz53cqzy2bwryc1cdsh3zriikb1qn3z")))

