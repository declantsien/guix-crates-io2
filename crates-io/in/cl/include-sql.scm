(define-module (crates-io in cl include-sql) #:use-module (crates-io))

(define-public crate-include-sql-0.1.0 (c (n "include-sql") (v "0.1.0") (d (list (d (n "include-sql-helper") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "oci_rs") (r "^0.8") (d #t) (k 2)) (d (n "oracle") (r "^0.2") (d #t) (k 2)) (d (n "postgres") (r "^0.15") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.17") (d #t) (k 2)) (d (n "string-error") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "02k33238gllfd01jwb7kh2vc2lad6c45363ygy2mv6q7hx4ibkhq")))

(define-public crate-include-sql-0.2.0 (c (n "include-sql") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qpvaksbjbw2bws2j8c0b9qkixccfar3phigqi5wmgccahzqki20")))

(define-public crate-include-sql-0.2.1 (c (n "include-sql") (v "0.2.1") (d (list (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13ss7mv3psjn5a57i00vk2z97n483aj7833jq3cyabqpls32m9ib")))

(define-public crate-include-sql-0.2.2 (c (n "include-sql") (v "0.2.2") (d (list (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16hpkkx5qz17xagjk69w222jcq3fmhdgialmzvw0k290g8b89n88")))

(define-public crate-include-sql-0.2.3 (c (n "include-sql") (v "0.2.3") (d (list (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06jmhzwq0wz09bi4mspv6jvcnx3j7z4fh65nzdz5v69fq8xfk4q3")))

(define-public crate-include-sql-0.2.4 (c (n "include-sql") (v "0.2.4") (d (list (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rp5a8macvic4dsqp9j0n8r9qj1mhn591sghwy04x39b8ysv2xxc")))

(define-public crate-include-sql-0.2.5 (c (n "include-sql") (v "0.2.5") (d (list (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11l4flhd6709n2gs478hhic9fi3qq7m6b42x963khllb9lphvi2m") (f (quote (("async"))))))

(define-public crate-include-sql-0.3.0 (c (n "include-sql") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "09r4d28vmcfg4dghfajky69k7fndcawlx5rhsvjjpnvq0anbkjy7") (f (quote (("async"))))))

(define-public crate-include-sql-0.3.1 (c (n "include-sql") (v "0.3.1") (d (list (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17swl09s6yblr9ihb6pbxvsrbjpsfslkbm9zbvw2i74yblrnlr2d") (f (quote (("async"))))))

