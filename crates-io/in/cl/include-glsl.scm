(define-module (crates-io in cl include-glsl) #:use-module (crates-io))

(define-public crate-include-glsl-0.1.0 (c (n "include-glsl") (v "0.1.0") (d (list (d (n "include-glsl-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "1xaja7h8h2q9hsdn5pqn9ic4yg8agr2mraxzqkxjqk391kl73fy8")))

(define-public crate-include-glsl-0.2.0 (c (n "include-glsl") (v "0.2.0") (d (list (d (n "include-glsl-impl") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)))) (h "07r5slmck6dgqsaq2mzfjmflm0v6ji84qinznp4w2f3ji312apgi")))

