(define-module (crates-io in cl include-crypt) #:use-module (crates-io))

(define-public crate-include-crypt-0.1.0 (c (n "include-crypt") (v "0.1.0") (d (list (d (n "include-crypt-codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "include-crypt-crypto") (r "^0.1.0") (d #t) (k 0)) (d (n "libflate") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "obfstr") (r "^0.2.4") (d #t) (k 0)))) (h "1kkyw6dc1fssmrffzvaxh7hqkf1bqbpjqxh7fskck2qvwggm6rx2") (f (quote (("force-build" "include-crypt-codegen/force-build") ("default") ("compression" "include-crypt-codegen/compression" "libflate"))))))

(define-public crate-include-crypt-0.1.1 (c (n "include-crypt") (v "0.1.1") (d (list (d (n "include-crypt-codegen") (r "^0.1.1") (d #t) (k 0)) (d (n "include-crypt-crypto") (r "^0.1.0") (d #t) (k 0)) (d (n "libflate") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "obfstr") (r "^0.2.4") (d #t) (k 0)))) (h "0fiqr0mmfmk7m3pa4mlw1mwm5nzvwb5rq8ahl4638xcvvqbc8fl8") (f (quote (("force-build" "include-crypt-codegen/force-build") ("default") ("compression" "include-crypt-codegen/compression" "libflate"))))))

