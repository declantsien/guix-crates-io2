(define-module (crates-io in cl include_directory) #:use-module (crates-io))

(define-public crate-include_directory-0.1.0 (c (n "include_directory") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.1") (o #t) (k 0)) (d (n "include_directory_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1hskh5skinxal37rsdlz82gcvg8s64raqacfwzjfc3zg2jxhlf7r") (f (quote (("nightly" "include_directory_macros/nightly") ("metadata" "include_directory_macros/metadata") ("default")))) (r "1.67.1")))

(define-public crate-include_directory-0.1.1 (c (n "include_directory") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.1") (o #t) (k 0)) (d (n "include_directory_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1afc65hyd3kgsh85f3gf35bmk6yygzfnm4mkal6nviy8v4hvylgw") (f (quote (("nightly" "include_directory_macros/nightly") ("metadata" "include_directory_macros/metadata") ("default")))) (r "1.67.1")))

