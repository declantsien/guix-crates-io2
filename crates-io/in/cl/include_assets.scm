(define-module (crates-io in cl include_assets) #:use-module (crates-io))

(define-public crate-include_assets-1.0.0 (c (n "include_assets") (v "1.0.0") (d (list (d (n "include_assets_decode") (r "^0.1.0") (d #t) (k 0)) (d (n "include_assets_encode") (r "^0.1.0") (d #t) (k 0)))) (h "1jy1zrrhdk7zq6i9r3ibw4wdic4i2fblr9gh841yh2qngcaz9bcv") (f (quote (("zstd" "include_assets_encode/zstd" "include_assets_decode/zstd") ("lz4" "include_assets_encode/lz4" "include_assets_decode/lz4") ("deflate" "include_assets_encode/deflate" "include_assets_decode/deflate") ("default" "all") ("all" "deflate" "lz4" "zstd"))))))

