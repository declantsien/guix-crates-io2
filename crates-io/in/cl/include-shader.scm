(define-module (crates-io in cl include-shader) #:use-module (crates-io))

(define-public crate-include-shader-0.1.0 (c (n "include-shader") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1rnl4l2wg5dfkr3lp2x3i3cp3jkmqf3j13wwhzvxjvvcgah0w7lv") (f (quote (("nightly"))))))

(define-public crate-include-shader-0.1.1 (c (n "include-shader") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1qc10msy1asvrxc0hamzv0vrynavr9r5ai8p2590zcb1igd0rqhm") (f (quote (("nightly"))))))

(define-public crate-include-shader-0.2.0 (c (n "include-shader") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1hvji44vfg6icpz6bqa8zzg3lcgpwa44dpz55xsadq6zhnxxh9bq") (f (quote (("track-path") ("relative-path") ("nightly" "track-path"))))))

