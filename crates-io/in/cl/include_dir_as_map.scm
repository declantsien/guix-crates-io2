(define-module (crates-io in cl include_dir_as_map) #:use-module (crates-io))

(define-public crate-include_dir_as_map-1.0.0 (c (n "include_dir_as_map") (v "1.0.0") (d (list (d (n "proc_include_dir_as_map") (r "^1") (d #t) (k 0)))) (h "1xd9370sxm647apsi1kp5id3paabrhliclv8c5mf949svybzws08")))

(define-public crate-include_dir_as_map-1.1.0 (c (n "include_dir_as_map") (v "1.1.0") (d (list (d (n "proc_include_dir_as_map") (r "^1") (d #t) (k 0)))) (h "0az8y53sc3841f2rvv7hk9gnsma4057bsymrjc95jhz9b6iswqms") (f (quote (("always-embed" "proc_include_dir_as_map/always-embed"))))))

