(define-module (crates-io in cl include_dir_impl) #:use-module (crates-io))

(define-public crate-include_dir_impl-0.2.0 (c (n "include_dir_impl") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.1") (d #t) (k 0)))) (h "1p1zqw31ha9sy7mvv4pbqy5ip1ayi055lkfhzsr7caqmxmkf9vj7")))

(define-public crate-include_dir_impl-0.2.1 (c (n "include_dir_impl") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.1") (d #t) (k 0)))) (h "0fxzwiarz6jr2ww54x2p0d56yzhr8cw65igx46czpw5fk68h4jqc")))

(define-public crate-include_dir_impl-0.4.0 (c (n "include_dir_impl") (v "0.4.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1gk0chsr5d8zzkbilfm0ckfj2h11621c38p4rwxp9i9lpzwhl6q4")))

(define-public crate-include_dir_impl-0.4.1 (c (n "include_dir_impl") (v "0.4.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1kjvq4ivji6gy4bi5ba1wi8z4yhab8f5ylyqlwnaf0pxy88b4iqg")))

(define-public crate-include_dir_impl-0.5.0 (c (n "include_dir_impl") (v "0.5.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1ldgs1k3hsciid8pc196bqs88c2wpra1h8ncjikz8781cixlyfzh")))

(define-public crate-include_dir_impl-0.6.0 (c (n "include_dir_impl") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "05bjyp1wa9f7kpx9gdws2llls52n324mr8nws4j9v0bl0nbnjy1j")))

(define-public crate-include_dir_impl-0.6.1 (c (n "include_dir_impl") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1mm33pal1ry3kq5bgd7cwvvids7pvz52r68kg1y1v4l1ywbkkbmg")))

(define-public crate-include_dir_impl-0.6.2 (c (n "include_dir_impl") (v "0.6.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1pw2nv1cy5483jjic5v8i9n3rgbb743wf122rrxsnjyshl68j30a")))

