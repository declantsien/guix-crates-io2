(define-module (crates-io in cl include_dir) #:use-module (crates-io))

(define-public crate-include_dir-0.1.0 (c (n "include_dir") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)) (d (n "tempfile") (r "^2.1.5") (d #t) (k 2)) (d (n "walkdir") (r "^1.0.7") (d #t) (k 0)))) (h "01hrd0gr2hbyldrj74wzkbf0qyc65mxbgxis5kigxj5vmdz4qlxy")))

(define-public crate-include_dir-0.1.1 (c (n "include_dir") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)) (d (n "tempfile") (r "^2.1.5") (d #t) (k 2)))) (h "0yg2593r6yqfpmik307krrgcv44hbz7hr80j8j2rrigl9kxnhdp2")))

(define-public crate-include_dir-0.1.2 (c (n "include_dir") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)) (d (n "tempfile") (r "^2.1.5") (d #t) (k 2)))) (h "0m3v852yb1xnj4skcgslpdhzwm573ar6n9sqcf5fllanbvx0gff2")))

(define-public crate-include_dir-0.1.3 (c (n "include_dir") (v "0.1.3") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)) (d (n "tempfile") (r "^2.1.5") (d #t) (k 2)))) (h "1mfc8ki270ky2gmsxx6rblm56c0aa8c1ql5kcpymfqcdmzgg55kd") (f (quote (("globs") ("default"))))))

(define-public crate-include_dir-0.1.4 (c (n "include_dir") (v "0.1.4") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)) (d (n "tempfile") (r "^2.1.6") (d #t) (k 2)))) (h "0a0shc6fp020rpizki01h776zi3ra23hsjhbnqwhxgr05ciqy692") (f (quote (("globs") ("default"))))))

(define-public crate-include_dir-0.1.5 (c (n "include_dir") (v "0.1.5") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "tempfile") (r "^2.2") (d #t) (k 2)))) (h "1wpvkj5bbxfd6d1ygqn2ckygfamr7vdp9npmnmafyxn6p72ndl55") (f (quote (("globs") ("default"))))))

(define-public crate-include_dir-0.2.0 (c (n "include_dir") (v "0.2.0") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "include_dir_impl") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "1wz0ls3sxsvzc0w1cbwmgyq8lhd6z6k3bzbp7b9km7wcfg4qnjiz") (f (quote (("example") ("default"))))))

(define-public crate-include_dir-0.2.1 (c (n "include_dir") (v "0.2.1") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "include_dir_impl") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "14xfn22gx67naljmi84ph4spc135ckbwq2cgbmszpcwl33p8n6pl") (f (quote (("example") ("default"))))))

(define-public crate-include_dir-0.4.0 (c (n "include_dir") (v "0.4.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "include_dir_impl") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "00lbzy951wbxl0j5256w3fb17nb7bz6jxpdwvqlh16cam1rgh557") (f (quote (("example-output") ("default"))))))

(define-public crate-include_dir-0.4.1 (c (n "include_dir") (v "0.4.1") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "include_dir_impl") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "12rhcyf2sb7hqin0r5jyw3n6ps522pamndmqsk3l8n87298167c7") (f (quote (("example-output") ("default"))))))

(define-public crate-include_dir-0.5.0 (c (n "include_dir") (v "0.5.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "include_dir_impl") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0sfphh6ylsb38abp9ynffdnm4z8kq1clqr6ab3yd02hhnrhqrv15") (f (quote (("example-output") ("default"))))))

(define-public crate-include_dir-0.6.0 (c (n "include_dir") (v "0.6.0") (d (list (d (n "glob") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "include_dir_impl") (r "= 0.6.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "06n6x9l8jnc3laifs8hgbp1h86bqcc84n23c228lc71bnbg8pm93") (f (quote (("search" "glob") ("example-output") ("default" "search"))))))

(define-public crate-include_dir-0.6.1 (c (n "include_dir") (v "0.6.1") (d (list (d (n "glob") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "include_dir_impl") (r "=0.6.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0js8ia6f0ib64f8sk8b2yzvzq2lj0qyy77pz5micfmjk6fyj9a9i") (f (quote (("search" "glob") ("example-output") ("default" "search"))))))

(define-public crate-include_dir-0.6.2 (c (n "include_dir") (v "0.6.2") (d (list (d (n "glob") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "include_dir_impl") (r "=0.6.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1arcwvyrpwm3kjxy0kz2hylc1l3hw089y0qgklgdd1v1gqa6xd94") (f (quote (("search" "glob") ("example-output") ("default" "search"))))))

(define-public crate-include_dir-0.7.0 (c (n "include_dir") (v "0.7.0") (d (list (d (n "glob") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "include_dir_macros") (r "^0.7.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1k66396bnipsff23270cjlmhg4bqhm30k1572ny9cgv5m0bjlz8d") (f (quote (("nightly" "include_dir_macros/nightly") ("metadata" "include_dir_macros/metadata") ("default")))) (r "1.56")))

(define-public crate-include_dir-0.7.1 (c (n "include_dir") (v "0.7.1") (d (list (d (n "glob") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "include_dir_macros") (r "^0.7.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "06338v4a02538hh6slpxmizwhmyvjq376s0v7z9h5dvffx6p7rvz") (f (quote (("nightly" "include_dir_macros/nightly") ("metadata" "include_dir_macros/metadata") ("default")))) (r "1.56")))

(define-public crate-include_dir-0.7.2 (c (n "include_dir") (v "0.7.2") (d (list (d (n "glob") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "include_dir_macros") (r "^0.7.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "17y62c3sam9dy561mljqcrzhnxi66d119ggxswjyszhb40ljwaj8") (f (quote (("nightly" "include_dir_macros/nightly") ("metadata" "include_dir_macros/metadata") ("default")))) (r "1.56")))

(define-public crate-include_dir-0.7.3 (c (n "include_dir") (v "0.7.3") (d (list (d (n "glob") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "include_dir_macros") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "17pinxhivh3chkbjmbg9sl0x3h7wwry2zc2p12gfh8kizyp2yxhq") (f (quote (("nightly" "include_dir_macros/nightly") ("metadata" "include_dir_macros/metadata") ("default")))) (r "1.56")))

