(define-module (crates-io in cl include-cargo-toml) #:use-module (crates-io))

(define-public crate-include-cargo-toml-0.1.0 (c (n "include-cargo-toml") (v "0.1.0") (d (list (d (n "cargo_toml") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)))) (h "1hximvsjpcjgwzabga0fy7nzz62f73x8m6524a9j9lzzwby6mfr4")))

