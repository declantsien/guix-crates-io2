(define-module (crates-io in v_ inv_manager) #:use-module (crates-io))

(define-public crate-inv_manager-0.2.0 (c (n "inv_manager") (v "0.2.0") (d (list (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "humantime") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "1775bkx9av3zwx5pzmw55vx99s71h9r8z5jndq88cm4ljv0434b8")))

