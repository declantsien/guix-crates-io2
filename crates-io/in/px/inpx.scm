(define-module (crates-io in px inpx) #:use-module (crates-io))

(define-public crate-inpx-0.1.0 (c (n "inpx") (v "0.1.0") (h "0i5zq7v4jagh33sa8kby0xryfl0kmnbq5xkn34j302hd4q61m21d") (r "1.58.1")))

(define-public crate-inpx-0.1.1 (c (n "inpx") (v "0.1.1") (h "01jx4nlfm19smsi6864kaqvsq84d7hdpdiapsnhfl69z32y1b1ha") (r "1.58.1")))

(define-public crate-inpx-0.1.2 (c (n "inpx") (v "0.1.2") (h "16licjlnrhzd3v45vf578h9gxx6flrwnrla7n6b25pfqfdpkgpy7") (r "1.58.1")))

