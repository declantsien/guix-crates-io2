(define-module (crates-io in c- inc-sha1) #:use-module (crates-io))

(define-public crate-inc-sha1-0.1.0 (c (n "inc-sha1") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "sha") (r "^1.0") (d #t) (k 0)))) (h "132274cwc672gvaj0m667j5vg48ilnhgk7vhr6mrsafkminrmp9x")))

