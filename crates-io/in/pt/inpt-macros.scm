(define-module (crates-io in pt inpt-macros) #:use-module (crates-io))

(define-public crate-inpt-macros-0.1.0 (c (n "inpt-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.25") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "1xchad369371n56p6ncfklm6ywpahar118bh2jz8zb7dzn9kh9pj")))

(define-public crate-inpt-macros-0.1.1 (c (n "inpt-macros") (v "0.1.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.25") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "0n5y81y9m5i7zjqpzzxkj7509wi4sc6c20c4waw162090wmiswl8")))

