(define-module (crates-io in pu input_validation) #:use-module (crates-io))

(define-public crate-input_validation-0.1.1 (c (n "input_validation") (v "0.1.1") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "1shl00q2j57a6fzjsmdysd742yvfdpvdlc67dizhzvmcijkxp1l7")))

(define-public crate-input_validation-0.1.2 (c (n "input_validation") (v "0.1.2") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)))) (h "1h64s78x46vfcf9cbv0zciriw121bj1qn7vvfghs2pjjqcxrp6my")))

