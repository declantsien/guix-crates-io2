(define-module (crates-io in pu input-actions) #:use-module (crates-io))

(define-public crate-input-actions-0.1.0 (c (n "input-actions") (v "0.1.0") (d (list (d (n "gilrs") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.25") (o #t) (d #t) (k 0)))) (h "0idia8dyld20r6gwaf6zcaswh31yzndbs4c9fx0jfjrr6gf94v8w") (f (quote (("default" "log" "winit"))))))

(define-public crate-input-actions-0.1.1-alpha.0 (c (n "input-actions") (v "0.1.1-alpha.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "enumset") (r "^1.0") (d #t) (k 0)) (d (n "gilrs") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.25") (o #t) (d #t) (k 0)))) (h "1vf6acap9k0q2a65jlphsl8m1hi43p4b0hqknqh2xkr3gckhlh0y") (f (quote (("default" "log" "winit")))) (y #t)))

(define-public crate-input-actions-0.1.1 (c (n "input-actions") (v "0.1.1") (d (list (d (n "gilrs") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.26") (o #t) (d #t) (k 0)))) (h "11g1bk3d0wz9ryqbhfdy8ks1s7byvpms5z4v9dh0p414rdi59brc") (f (quote (("default" "log" "winit"))))))

