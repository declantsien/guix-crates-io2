(define-module (crates-io in pu input-device) #:use-module (crates-io))

(define-public crate-input-device-0.1.0 (c (n "input-device") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "poller") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "195mhxz6nasdqg0fk7gg4av6qfzx6gck90l2sjfrhgim2030lai4") (f (quote (("linux-mousedev") ("linux-evdev"))))))

(define-public crate-input-device-0.2.0 (c (n "input-device") (v "0.2.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "poller") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1jpwb27rm3k25442ishszysd4ngpxjcv4ymivjjdmav3sip4mjyr") (f (quote (("linux-mousedev") ("linux-evdev"))))))

(define-public crate-input-device-0.2.1 (c (n "input-device") (v "0.2.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "poller") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0mjyvr2qvcxs5asswxid16x0mig78b1bpzckpa6lpl8b19l3cvpa") (f (quote (("linux-mousedev") ("linux-evdev"))))))

(define-public crate-input-device-0.2.2 (c (n "input-device") (v "0.2.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "poller") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0mqbi0gq7xspn925pbyjasi0nxi0gn93x30r03v9412n98l3ywsf") (f (quote (("linux-mousedev") ("linux-evdev"))))))

