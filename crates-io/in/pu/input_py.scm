(define-module (crates-io in pu input_py) #:use-module (crates-io))

(define-public crate-input_py-0.1.0 (c (n "input_py") (v "0.1.0") (h "1hzc7nsaxj4d60465dq2z60a16p3m524kbjd3rc294wa4hs5mzj9")))

(define-public crate-input_py-0.2.0 (c (n "input_py") (v "0.2.0") (h "1v5spc0ka1d9651zcl4j8vk9wfg54nc2zqa35k6qj9r1ggnn5mx7")))

