(define-module (crates-io in pu inputparser) #:use-module (crates-io))

(define-public crate-inputparser-0.1.0 (c (n "inputparser") (v "0.1.0") (h "1is8jlqmis4i3div6dfihccg9iz4bsfrjrhwlpmk10g3nbhxlklr")))

(define-public crate-inputparser-0.1.1 (c (n "inputparser") (v "0.1.1") (h "0f8h60l5rqfygq08ir9ks70gw7qn4pqd9sh4cl3ld9gczhw1b7n3")))

(define-public crate-inputparser-0.1.2 (c (n "inputparser") (v "0.1.2") (h "1lwwqzs49aqkngsixmxxma99dzw6l0cn9ixrhslwvap13564cj3z")))

(define-public crate-inputparser-0.1.3 (c (n "inputparser") (v "0.1.3") (h "0jjsi33kypgqda3218f8g2x2kamwvkdi6fvqlb845fr5qc8pwb1c")))

(define-public crate-inputparser-0.1.4 (c (n "inputparser") (v "0.1.4") (h "0j32wdx2ixb885qncaxaqxmjw64gmq2vp3yivhjnzrrlqfnp2sq0")))

(define-public crate-inputparser-0.1.41 (c (n "inputparser") (v "0.1.41") (h "0mwx6avqzlnnkxd97fafa029qkgqy2szqp4qr00llwdxjh1mhzzx")))

(define-public crate-inputparser-0.1.5 (c (n "inputparser") (v "0.1.5") (h "0ss4klfn7426qxg5cz6fbx479sibigdd7r1p1xqdjsy902n3jn6z") (y #t)))

(define-public crate-inputparser-0.1.51 (c (n "inputparser") (v "0.1.51") (h "12xjdbad5i1xffnv9fhk8maii22pvhlvwc0y1aplb8nh4x5wy2xm")))

(define-public crate-inputparser-0.1.52 (c (n "inputparser") (v "0.1.52") (h "0iaq7mn55m9nbxq3y14hrf5ziazfcx5754vm330wzjpby4w1q4h4")))

(define-public crate-inputparser-0.1.53 (c (n "inputparser") (v "0.1.53") (h "1xpfkidl8pcg2v5i0gmqsjyixzxwd4n25ky5911qkwdilg41191z")))

(define-public crate-inputparser-0.1.6 (c (n "inputparser") (v "0.1.6") (h "0sv4rmr3hi3h7g0p0wkygnyjq4yi31mw7rjzgmrljilcsn2a96hy") (y #t)))

(define-public crate-inputparser-0.1.60 (c (n "inputparser") (v "0.1.60") (h "09rxyqry3gwm14phj2jznv8h2lf9d91g42q02dd0klw3p036kfpn")))

(define-public crate-inputparser-0.1.70 (c (n "inputparser") (v "0.1.70") (h "1qa7akgf5825an2nzcp08dl2js3spn2jclx8maxkwmgj87qjmj7c")))

(define-public crate-inputparser-0.1.71 (c (n "inputparser") (v "0.1.71") (h "13hj6if4fxq94jvv6ig5jw9r67n6j0ssm38pw9r37rmy5l78v92d")))

(define-public crate-inputparser-0.1.72 (c (n "inputparser") (v "0.1.72") (h "1clas1h4was7caqjwjl1w3hpg0x0c0ka71przxf1bmp7wfzry917")))

