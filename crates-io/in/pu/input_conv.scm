(define-module (crates-io in pu input_conv) #:use-module (crates-io))

(define-public crate-input_conv-1.0.0 (c (n "input_conv") (v "1.0.0") (h "0cvd3cn1j0rk54cdb0r12apwcqx2n2gkpznjnyg8ydqqpmpjzai2")))

(define-public crate-input_conv-1.0.1 (c (n "input_conv") (v "1.0.1") (h "1hb2vmkhh88ipq5x1j5y59v28p9j9shjpag5rgyp43rvv4mhpp54")))

(define-public crate-input_conv-1.1.0 (c (n "input_conv") (v "1.1.0") (h "176qkpy90vblh2yq8x7iafrii5chb1pvdzrllnid589ckkd60rpb")))

(define-public crate-input_conv-1.1.1 (c (n "input_conv") (v "1.1.1") (h "0h5xxi2fgvy9bj7v20q9xfhmdxa07hjvsq6wim54zfpgf1848v9b")))

(define-public crate-input_conv-1.2.0 (c (n "input_conv") (v "1.2.0") (h "1qzd7akkqjsmrrmdlfwib02qxvlz2v1r3hb9hq6yyc53rxsrpg10")))

