(define-module (crates-io in pu input-stream) #:use-module (crates-io))

(define-public crate-input-stream-0.1.0 (c (n "input-stream") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.7.1") (d #t) (k 0)))) (h "04fxha30dkvbn55rgvk797mmfh25sbyjisjvp8z1fva0nimij7b3")))

(define-public crate-input-stream-0.1.1 (c (n "input-stream") (v "0.1.1") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "error-chain") (r "^0.7.1") (d #t) (k 0)))) (h "07rhqwc3s4778jg07hir9429a6pcdzsgk8l89iscpj1fnfr5d1n1") (f (quote (("default"))))))

(define-public crate-input-stream-0.2.0 (c (n "input-stream") (v "0.2.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "error-chain") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "0.*") (d #t) (k 2)))) (h "0xjk6xx27kmr8jk91q2cbjnl1alkzs1cg2qi93ab89z98x9w7rjc") (f (quote (("default"))))))

(define-public crate-input-stream-0.3.0 (c (n "input-stream") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "0.*") (d #t) (k 2)))) (h "1z29zly35rngvj06sxivyfrp55cc0dxbd7ymav4brsvgxfgqvcl5") (f (quote (("default"))))))

(define-public crate-input-stream-0.4.0 (c (n "input-stream") (v "0.4.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1k2gdhd0w35ad5xb18m06sls7vsyyk38p82aia4bbd6gvzcz1yzp") (f (quote (("default"))))))

