(define-module (crates-io in pu input-linux-sys) #:use-module (crates-io))

(define-public crate-input-linux-sys-0.0.1 (c (n "input-linux-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)))) (h "07x21a6pyf28y5fqhnvrln9mvrh65ym5aphrc8p2pcyl73wdy5wl")))

(define-public crate-input-linux-sys-0.0.2 (c (n "input-linux-sys") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)))) (h "1yazz5dj0r4ycfxj6y2fd9v69ip89rkv0h0yjm0n39p5dnb43zba")))

(define-public crate-input-linux-sys-0.0.3 (c (n "input-linux-sys") (v "0.0.3") (d (list (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)))) (h "0bk9yqlcqynd6jmqhwvwzf88m1a2phamjrmfqba8pf11df6qcdia")))

(define-public crate-input-linux-sys-0.1.0 (c (n "input-linux-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)))) (h "0bwz16b4rjv539dky3597fyj055ag929hyj1s8knapr7v3c7bll1")))

(define-public crate-input-linux-sys-0.1.1 (c (n "input-linux-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)))) (h "1ac4vj1a2mfv207qf8idfcsja54bwv5xwns642glinb04h8c897h")))

(define-public crate-input-linux-sys-0.1.2 (c (n "input-linux-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)))) (h "0cz3d5qjfmd08dvz4qj9l2v6rk46ljrkm7ljn8gsdfykv778k0n8")))

(define-public crate-input-linux-sys-0.1.3 (c (n "input-linux-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)))) (h "17mz6syzg2xnq8vfl30j7xnb7s2b61xhlmcb0hzcs0m25l6igy4p")))

(define-public crate-input-linux-sys-0.1.4 (c (n "input-linux-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)))) (h "08sdbim8v7saqg9zvmm7wh1k4213685vhycfixjjmr04rqi0qglh")))

(define-public crate-input-linux-sys-0.1.5 (c (n "input-linux-sys") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)))) (h "0b628v2z0741srljw1x5g7x7r15m670n348m40vdivzw73h66sn4")))

(define-public crate-input-linux-sys-0.2.0 (c (n "input-linux-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)))) (h "11iyw48w1q9jc1y1s887ps502hy0hcjmivmqy9kbbbrf720jmg8k")))

(define-public crate-input-linux-sys-0.2.1 (c (n "input-linux-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)))) (h "0irw66kcib3s0gq06wd9aizrr1kk88zmdzszzzry7shjfk98ynii")))

(define-public crate-input-linux-sys-0.3.0 (c (n "input-linux-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.36") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)))) (h "0ihs9kvx5lvcmp4gmfhk3jd6br73qxfnnw66igiwaghamg0ni33c")))

(define-public crate-input-linux-sys-0.3.1 (c (n "input-linux-sys") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.36") (d #t) (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)))) (h "115r28lcw24i1hlhr24ng3yr7a9x4b3ccsimrk74wz5ha0cbwwsq")))

(define-public crate-input-linux-sys-0.4.0 (c (n "input-linux-sys") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.36") (d #t) (k 0)) (d (n "nix") (r "^0.18.0") (d #t) (k 0)))) (h "0cahiajr3py7cfxhmf5n76mxhg0kby0s7cddgasbnxrakhx8jp8n")))

(define-public crate-input-linux-sys-0.5.0 (c (n "input-linux-sys") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.36") (d #t) (k 0)) (d (n "nix") (r "^0.18.0") (d #t) (k 0)))) (h "0y4jf7zy9q367wnkadzrq1z825z3xrs6kcmypllrz3ssb1wmis0v")))

(define-public crate-input-linux-sys-0.6.0 (c (n "input-linux-sys") (v "0.6.0") (d (list (d (n "libc") (r "^0.2.36") (d #t) (k 0)) (d (n "nix") (r "^0.19.0") (d #t) (k 0)))) (h "1xggr00jnvv1bqfqm3f0w2x5h1qzcyr9dp17h22n6n5jhgihcaq6")))

(define-public crate-input-linux-sys-0.7.0 (c (n "input-linux-sys") (v "0.7.0") (d (list (d (n "libc") (r "^0.2.36") (d #t) (k 0)) (d (n "nix") (r "^0.22.0") (d #t) (k 0)))) (h "19hrbh9fmbwmij9m914h2pxvrmgx7fajj404x98g8aq8hgzkhnqw")))

(define-public crate-input-linux-sys-0.8.0 (c (n "input-linux-sys") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)))) (h "1f4s55jsw1jnx97a757nb3ly2zzri5aklxakj7gjfc4plhjpls0s")))

