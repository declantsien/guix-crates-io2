(define-module (crates-io in pu input_event_codes_hashmap) #:use-module (crates-io))

(define-public crate-input_event_codes_hashmap-0.1.0 (c (n "input_event_codes_hashmap") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "15k61yz4wcw35jvnwzhic1lwwnmjwrkzmlb2xi1dwc37kx0srycy") (y #t)))

(define-public crate-input_event_codes_hashmap-0.1.1 (c (n "input_event_codes_hashmap") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0ajxs60ac6l0cjvfaawkij7yc2c9d95w7xyhdqg8bygk7ycrrqjp")))

