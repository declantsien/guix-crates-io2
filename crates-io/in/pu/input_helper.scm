(define-module (crates-io in pu input_helper) #:use-module (crates-io))

(define-public crate-input_helper-0.1.0 (c (n "input_helper") (v "0.1.0") (d (list (d (n "event_feed") (r "^0.1") (d #t) (k 0)) (d (n "gilrs") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.26") (o #t) (d #t) (k 0)))) (h "055arpdnk9crxb4yhg6cglqkwl5jyqf7qa5lwnfpfbb6vjd01cg4") (f (quote (("default")))) (s 2) (e (quote (("winit" "dep:winit") ("sdl" "dep:sdl2") ("gilrs" "dep:gilrs"))))))

(define-public crate-input_helper-0.1.2 (c (n "input_helper") (v "0.1.2") (d (list (d (n "event_feed") (r "^0.1") (d #t) (k 0)) (d (n "gilrs") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.26") (o #t) (d #t) (k 0)))) (h "0rmdbg07vc3zm1qgqxd3pxd2fx7cnqcc1xazb9arg2ldw5ml0xrk") (f (quote (("default")))) (s 2) (e (quote (("winit" "dep:winit") ("sdl" "dep:sdl2") ("gilrs" "dep:gilrs"))))))

