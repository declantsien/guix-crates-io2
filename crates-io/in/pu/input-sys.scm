(define-module (crates-io in pu input-sys) #:use-module (crates-io))

(define-public crate-input-sys-1.7.0 (c (n "input-sys") (v "1.7.0") (d (list (d (n "bindgen") (r "^0.22") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cp2c5c0rhd92hsaz6z3fgk9jp1vy68s9c04bqi9bmmfa1vmv772")))

(define-public crate-input-sys-1.7.1 (c (n "input-sys") (v "1.7.1") (d (list (d (n "bindgen") (r "^0.22") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0f79b1xdq2y7r613r8w2zhshj5nk6s62v88w6yn8irxb56psndm8") (f (quote (("gen" "bindgen"))))))

(define-public crate-input-sys-1.9.0 (c (n "input-sys") (v "1.9.0") (d (list (d (n "bindgen") (r "^0.31") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0y3q3d7sllcg3n8dgj46n6s3va2mj0nw7zrhchhcvzmnk77n8axf") (f (quote (("gen" "bindgen"))))))

(define-public crate-input-sys-1.15.0 (c (n "input-sys") (v "1.15.0") (d (list (d (n "bindgen") (r "^0.31") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mbajhvbs2l39a43l8gpwdpr1z8bl2xh6libv75s9ak5rxzxy1gy") (f (quote (("libinput_1_15") ("libinput_1_14") ("libinput_1_11") ("gen" "bindgen") ("default" "libinput_1_15"))))))

(define-public crate-input-sys-1.15.1 (c (n "input-sys") (v "1.15.1") (d (list (d (n "bindgen") (r "^0.57") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "111a2ps21y0hda0ifinps19p4d7pvgajn2fjk1cvlm1vcdlw9c8b") (f (quote (("update_bindings" "gen") ("libinput_1_15") ("libinput_1_14") ("libinput_1_11") ("gen" "bindgen") ("default" "libinput_1_15"))))))

(define-public crate-input-sys-1.16.0 (c (n "input-sys") (v "1.16.0") (d (list (d (n "bindgen") (r "^0.57") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wyaj418h0mwbd04dpcbfblpps82n68c9viz1kahqzfxqmkr9han") (f (quote (("update_bindings" "gen") ("libinput_1_19") ("libinput_1_15") ("libinput_1_14") ("libinput_1_11") ("gen" "bindgen") ("default" "libinput_1_15"))))))

(define-public crate-input-sys-1.16.1 (c (n "input-sys") (v "1.16.1") (d (list (d (n "bindgen") (r "^0.57") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0cqrar5zid2vdb0rb40ybycpfc0rwpd6q3v03h4mvfsym6fq97g0") (f (quote (("update_bindings" "gen") ("libinput_1_19") ("libinput_1_15") ("libinput_1_14") ("libinput_1_11") ("gen" "bindgen") ("default" "libinput_1_15"))))))

(define-public crate-input-sys-1.17.0 (c (n "input-sys") (v "1.17.0") (d (list (d (n "bindgen") (r "^0.57") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hnx7xh8dcvnxwh8cjqxh6nyn3vvz0x8cchfcqbp5flagshw5xh5") (f (quote (("update_bindings" "gen") ("libinput_1_21") ("libinput_1_19") ("libinput_1_15") ("libinput_1_14") ("libinput_1_11") ("gen" "bindgen") ("default" "libinput_1_21"))))))

(define-public crate-input-sys-1.18.0 (c (n "input-sys") (v "1.18.0") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "proc-macro2") (r "^1.0.76") (o #t) (d #t) (k 1)) (d (n "regex") (r "^1.10") (o #t) (d #t) (k 1)))) (h "1c4y24wf0jixi52js4f7cjspbgi0bzzaqfhn8m91qcq03i6mnkxx") (f (quote (("use_bindgen" "bindgen" "proc-macro2" "regex") ("update_bindings" "use_bindgen") ("libinput_1_21") ("libinput_1_19") ("libinput_1_15") ("libinput_1_14") ("libinput_1_11") ("default" "libinput_1_21"))))))

