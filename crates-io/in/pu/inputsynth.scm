(define-module (crates-io in pu inputsynth) #:use-module (crates-io))

(define-public crate-inputsynth-0.1.0 (c (n "inputsynth") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "x11rb") (r "^0.9.0") (f (quote ("xtest" "xkb" "allow-unsafe-code" "xinput"))) (d #t) (k 0)) (d (n "xcb") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "xkbcommon") (r "^0.5.0-beta.0") (f (quote ("x11"))) (d #t) (k 0)))) (h "09jcqj5yy15xjhqf2zg7vw23r8rkkk2dv6qfasdmslfl9qsslc0v")))

(define-public crate-inputsynth-0.1.1 (c (n "inputsynth") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "x11rb") (r "^0.9.0") (f (quote ("xtest" "xkb" "allow-unsafe-code" "xinput"))) (d #t) (k 0)) (d (n "xcb") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "xkbcommon") (r "^0.5.0-beta.0") (f (quote ("x11"))) (d #t) (k 0)))) (h "0v4x7iqng0gw27xnn0jqj4b580h3k3yg3yl1cqphn94x3bi35ygj")))

(define-public crate-inputsynth-0.1.2 (c (n "inputsynth") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "x11rb") (r "^0.11.1") (f (quote ("xtest" "xkb" "allow-unsafe-code" "xinput"))) (d #t) (k 0)) (d (n "xkbcommon") (r "^0.5.0") (f (quote ("x11"))) (d #t) (k 0)))) (h "0mg1q1g66ks1n9c1hrkh8hkgzjwvmaapmjvirq3lb25y89dx144p")))

