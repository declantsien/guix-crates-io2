(define-module (crates-io in pu input-utils) #:use-module (crates-io))

(define-public crate-input-utils-0.1.0 (c (n "input-utils") (v "0.1.0") (h "05ys9kvxxyr6hh5aa1hs9lzkxsps2c6wkvbsh0ls3zrvriwlf5ak")))

(define-public crate-input-utils-0.1.1 (c (n "input-utils") (v "0.1.1") (h "0gzd092h4xbdm12y2sn2a9x2w3f10v4c23wxzm0cc6md178ddcc8")))

