(define-module (crates-io in pu input-event-codes) #:use-module (crates-io))

(define-public crate-input-event-codes-0.1.0 (c (n "input-event-codes") (v "0.1.0") (h "0msnmbrpz68nnbx6w5xhyxx61cim8b2b80pcbsqdb59c0sxvjvn6")))

(define-public crate-input-event-codes-0.2.0 (c (n "input-event-codes") (v "0.2.0") (h "06x1b8yq5zab4wrviya06wx0b8n8pwasravb4im1ashd7y6bvkqv")))

(define-public crate-input-event-codes-0.2.1 (c (n "input-event-codes") (v "0.2.1") (h "04h7zamv0nsh187n23pn12pw7cn2w1f8mn4zlfwwf083zjh1vddc")))

(define-public crate-input-event-codes-0.2.2 (c (n "input-event-codes") (v "0.2.2") (h "1gda7y0xjavjy8bijnsp1qwxa9vpjp6pqpdgc3r9c1xmkimyap1y")))

(define-public crate-input-event-codes-5.16.8 (c (n "input-event-codes") (v "5.16.8") (h "0dk65hzncgbkis2cfmm92sg1830dqhaj05rgb7ds55pmay2hysvb") (r "1.56")))

(define-public crate-input-event-codes-5.17.0 (c (n "input-event-codes") (v "5.17.0") (h "017qgx1qlmsjic0k4k0bl56nv0lb7wbaysxq0wiy4ii8ffb22ssl") (r "1.56")))

(define-public crate-input-event-codes-5.18.0 (c (n "input-event-codes") (v "5.18.0") (h "1jni3klb7hanl2k0knwslh6l6s8nwwkj74y6f4z1d4gh8z6abc8i") (r "1.56")))

(define-public crate-input-event-codes-6.1.0 (c (n "input-event-codes") (v "6.1.0") (h "0dn51pzl0ffpy3jidhifkanp8mw9wvglqdsxq8xcdqfzk218fwzk") (r "1.56")))

(define-public crate-input-event-codes-6.2.0 (c (n "input-event-codes") (v "6.2.0") (h "1pcj6wi7w7fp8fhfij7fnh1jzk9hsqldxql0lcjhbhsnd1dvhdyl") (r "1.56")))

