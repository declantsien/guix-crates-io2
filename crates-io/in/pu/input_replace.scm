(define-module (crates-io in pu input_replace) #:use-module (crates-io))

(define-public crate-input_replace-0.1.0 (c (n "input_replace") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.135") (d #t) (k 0)))) (h "0nll4nsa226j34jy8wybz2pjkp4wd227rmppf1g1qml07pxq6lpl") (y #t)))

(define-public crate-input_replace-0.1.1 (c (n "input_replace") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.135") (d #t) (k 0)))) (h "1n98xw6qa1hhw8vpia7d334fks6b5sj0x6sp35m8m9dvnkhzayf3") (y #t)))

