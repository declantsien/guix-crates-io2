(define-module (crates-io in pu input_buffer) #:use-module (crates-io))

(define-public crate-input_buffer-0.1.0 (c (n "input_buffer") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.5") (d #t) (k 0)))) (h "1bpw2an1sz723w0i8vmjl79qq3drldmbzfx7hlrr2si84slksfzl") (y #t)))

(define-public crate-input_buffer-0.1.1 (c (n "input_buffer") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4.5") (d #t) (k 0)))) (h "1cgr39wahmld7cf56k84431ahyj5iyjsskiycqlcxrqm5zfm5z34")))

(define-public crate-input_buffer-0.2.0 (c (n "input_buffer") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4.5") (d #t) (k 0)))) (h "1gyzxv0wgns29lmlx2h7q5whmxfmirh82vqxjd8mb424r0n846wf")))

(define-public crate-input_buffer-0.3.0 (c (n "input_buffer") (v "0.3.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)))) (h "1a8byjsa6ab1xij0mmbn499l86w5zx192aydfh0i7qhb1azkp43d")))

(define-public crate-input_buffer-0.3.1 (c (n "input_buffer") (v "0.3.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)))) (h "0m4pamqvr00z90cmrgjj25iwpqy6fyac53k1ms63k86m8d9aka0r")))

(define-public crate-input_buffer-0.4.0 (c (n "input_buffer") (v "0.4.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)))) (h "04yl6pdjawq5grl946hn3imfs2cx0r0vrc0jvdyim3s4bybnfygr")))

(define-public crate-input_buffer-0.5.0 (c (n "input_buffer") (v "0.5.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)))) (h "044qxqdkcq6mv07bsvm35hl7hy3rmf87lrxjyz8zaq57i0xngvmc")))

