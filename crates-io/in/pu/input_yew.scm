(define-module (crates-io in pu input_yew) #:use-module (crates-io))

(define-public crate-input_yew-0.1.0 (c (n "input_yew") (v "0.1.0") (d (list (d (n "web-sys") (r "^0.3.64") (k 0)) (d (n "yew") (r "^0.20.0") (k 0)))) (h "18fcawnjni1gl2214xzkp7z2ldwxpzrzkg7yr2927jh15gpsq344")))

(define-public crate-input_yew-0.1.1 (c (n "input_yew") (v "0.1.1") (d (list (d (n "web-sys") (r "^0.3.64") (k 0)) (d (n "yew") (r "^0.20.0") (k 0)))) (h "0rwr25j6rs5giw27d4wrz7wajvbhd689hpg9g1h2hyhq01vcag2v")))

(define-public crate-input_yew-0.1.2 (c (n "input_yew") (v "0.1.2") (d (list (d (n "web-sys") (r "^0.3.64") (k 0)) (d (n "yew") (r "^0.20.0") (k 0)))) (h "0gjz87b00n4fwfv82n27l422s3ar21zrakrrsmzjf3vqrfcn16bl")))

(define-public crate-input_yew-0.1.3 (c (n "input_yew") (v "0.1.3") (d (list (d (n "web-sys") (r "^0.3.64") (k 0)) (d (n "yew") (r "^0.20.0") (k 0)))) (h "147vy9cc6lnz530qm26bd04szqbndvp13k7fjqyndz1fyz955jg3")))

(define-public crate-input_yew-0.1.4 (c (n "input_yew") (v "0.1.4") (d (list (d (n "web-sys") (r "^0.3.64") (k 0)) (d (n "yew") (r "^0.20.0") (k 0)))) (h "0pcjjlsgkbfjcxg78nwfic6jabi04fx8lywk302lyzppbnzw3zs8")))

(define-public crate-input_yew-0.1.5 (c (n "input_yew") (v "0.1.5") (d (list (d (n "web-sys") (r "^0.3.64") (k 0)) (d (n "yew") (r "^0.20.0") (k 0)))) (h "1fac69zwrmv66xa335cp2hj3a0j8590mh8mz05qksyn73xa17n1j")))

(define-public crate-input_yew-0.1.6 (c (n "input_yew") (v "0.1.6") (d (list (d (n "web-sys") (r "^0.3.64") (k 0)) (d (n "yew") (r "^0.20.0") (k 0)))) (h "0vsf0xncmh2cfidwqhy0lpmm2fqr90r21zkg4mi1s4x8fpdwzbmn")))

(define-public crate-input_yew-0.1.7 (c (n "input_yew") (v "0.1.7") (d (list (d (n "web-sys") (r "^0.3.64") (k 0)) (d (n "yew") (r "^0.20.0") (k 0)))) (h "1rbgq070bx3wyvgiwxs04vim7vb0sm6pb85fymwdc6akkrir2rrq")))

(define-public crate-input_yew-0.1.8 (c (n "input_yew") (v "0.1.8") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 2)) (d (n "web-sys") (r "^0.3.64") (k 0)) (d (n "yew") (r "^0.21.0") (k 0)))) (h "00f4yxr1n2mkxfd8svxrjc4m1gv4dw9xx7j1giv0dl0byp71bmd3")))

(define-public crate-input_yew-0.1.9 (c (n "input_yew") (v "0.1.9") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 2)) (d (n "web-sys") (r "^0.3.64") (k 0)) (d (n "yew") (r "^0.21.0") (k 0)))) (h "17b03rryzv57va282dj964zqkdwjjbbp6s2xbib17j4h8fwrds38")))

(define-public crate-input_yew-0.1.10 (c (n "input_yew") (v "0.1.10") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 2)) (d (n "web-sys") (r "^0.3.64") (k 0)) (d (n "yew") (r "^0.21.0") (k 0)))) (h "0v6s2k7d0kqpvb76f1d21w8rqdwrrsb022aydxjjddywg1xy1a9n")))

