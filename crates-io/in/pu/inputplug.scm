(define-module (crates-io in pu inputplug) #:use-module (crates-io))

(define-public crate-inputplug-0.4.0 (c (n "inputplug") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nix") (r ">=0.19, <1.0") (d #t) (k 0)) (d (n "pidfile-rs") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "x11rb") (r "^0.8") (f (quote ("xinput"))) (d #t) (k 0)))) (h "001fhhgzqbhbmb9kbrgyf5apicyy4jg08gbajmv64k8ij7qffz97") (f (quote (("pidfile" "pidfile-rs") ("default" "pidfile"))))))

