(define-module (crates-io in pu input-painter) #:use-module (crates-io))

(define-public crate-input-painter-0.1.0 (c (n "input-painter") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "15xn97svqmv246z9bbhmkkjvm2dhjmpphkv0mwkfg27pgmlmn2wp")))

(define-public crate-input-painter-0.1.1 (c (n "input-painter") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "156bxcmq39h3bg7mfsf3m99nkwm3vxbdpqv6garq9lj70yb5dgd1")))

(define-public crate-input-painter-0.1.2 (c (n "input-painter") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "1phb0mg2wznkv9zx1j8whx3181h3lrmp2whky63hj7hrl9b1v4ha")))

(define-public crate-input-painter-0.1.3 (c (n "input-painter") (v "0.1.3") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "1pzl8s9k4g5qb7jsrrzqj56y78vgmgbgsbrnqmlmwm4681yim2ga")))

(define-public crate-input-painter-0.1.4 (c (n "input-painter") (v "0.1.4") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "1h17h1g6q5cv3c6943zmzyrjc45li6df19zqy4vbcw4dcpwa1xq6")))

