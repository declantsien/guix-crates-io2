(define-module (crates-io in pu inputstat) #:use-module (crates-io))

(define-public crate-inputstat-0.1.0 (c (n "inputstat") (v "0.1.0") (d (list (d (n "evdev") (r "^0.11.4") (d #t) (k 0)))) (h "1zn2m1iaswq0xmm3vc9jx0sd2p8j8vzlwxkk9j87r12rbm6m7pnc")))

(define-public crate-inputstat-0.1.1 (c (n "inputstat") (v "0.1.1") (d (list (d (n "evdev") (r "^0.11.4") (d #t) (k 0)))) (h "11f5djh08csr0hj1hiikgz1ryrhhbb0acmxzc1xrsnk0ga2p15v4")))

(define-public crate-inputstat-0.1.2 (c (n "inputstat") (v "0.1.2") (d (list (d (n "evdev") (r "^0.11.4") (d #t) (k 0)))) (h "0frrlld9fhqzlqxf0dnga7m66fyxv6y39w1fvmkdnhy276wbcj1l")))

