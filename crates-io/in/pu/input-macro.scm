(define-module (crates-io in pu input-macro) #:use-module (crates-io))

(define-public crate-input-macro-0.1.0 (c (n "input-macro") (v "0.1.0") (h "1gkvd9jr35ynh9c3x1fyj5ay7wsnsljxg454msrpa5z6mj90rrbm") (f (quote (("emoji") ("default" "emoji" "alias") ("alias"))))))

(define-public crate-input-macro-0.1.1 (c (n "input-macro") (v "0.1.1") (h "0lagfkav99zdg4zzbniy8hb964ih6ayrr3xh71r73kqc4nzj6bi2") (f (quote (("emoji") ("default" "emoji" "alias") ("alias"))))))

(define-public crate-input-macro-0.1.2 (c (n "input-macro") (v "0.1.2") (h "1f4ldy2jgf9z1j9abi5rm6qv8i4qyg26c48l9wbgz2bsydks06j2") (f (quote (("emoji") ("default" "emoji" "alias") ("alias"))))))

(define-public crate-input-macro-0.1.3 (c (n "input-macro") (v "0.1.3") (h "10rpb3r0vkzs0k4107ifx89l22nmvqd7mdmhgm6251q1pvc8qs4i") (f (quote (("emoji") ("default" "emoji" "alias") ("alias"))))))

(define-public crate-input-macro-0.1.4 (c (n "input-macro") (v "0.1.4") (h "0gxqbdb38j74q29xqr58mldswaj2v57ynjcmf9951n3jw6jnjq5a") (f (quote (("emoji") ("default" "emoji" "alias") ("alias"))))))

(define-public crate-input-macro-0.1.5 (c (n "input-macro") (v "0.1.5") (h "00rm5zq8h1yk1gd8fidn55wsp19i3mgpsn9zzaldqijm5rv10s0k") (f (quote (("emoji") ("default" "emoji" "alias") ("alias"))))))

(define-public crate-input-macro-0.2.0 (c (n "input-macro") (v "0.2.0") (h "1hbnmrnimypm924pxil8llinkjdla7p0395zk5frk4nj94qn6hah")))

