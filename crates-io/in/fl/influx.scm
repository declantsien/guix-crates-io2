(define-module (crates-io in fl influx) #:use-module (crates-io))

(define-public crate-influx-0.1.0 (c (n "influx") (v "0.1.0") (h "1x9b9aym7gqc0ky78nhpvi8ls33r19c7sgf5dn6n8iabkpvapb6m")))

(define-public crate-influx-0.1.1 (c (n "influx") (v "0.1.1") (d (list (d (n "wdg-uri") (r "^0.1.9") (d #t) (k 0)))) (h "1kpjalzhjzsbcyibmkb1x5xkvnw684ygkwqdacp0x948c9a1lka0")))

(define-public crate-influx-0.1.2 (c (n "influx") (v "0.1.2") (d (list (d (n "wdg-uri") (r "^0.1.13") (d #t) (k 0)))) (h "0a142lp9fxy366cnizvspmq62jbqz22z1sl49i3k1kwmm6q1iivr")))

(define-public crate-influx-0.1.3 (c (n "influx") (v "0.1.3") (d (list (d (n "wdg-uri") (r "^0.1.13") (d #t) (k 0)))) (h "1p79k0iiazdpqdlsi8ggq6hvggsaabbpk5bchywrwl7wrwlvr8a0")))

(define-public crate-influx-0.1.4 (c (n "influx") (v "0.1.4") (d (list (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)) (d (n "wdg-uri") (r "^0.1.16") (d #t) (k 0)))) (h "1qapa2fx3miysldkwvlikpwcsngjc0cb2yll3f8451c4bjwl8xfm")))

(define-public crate-influx-0.1.5 (c (n "influx") (v "0.1.5") (d (list (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)) (d (n "wdg-uri") (r "^0.3.1") (d #t) (k 0)))) (h "19f8w6g1m1l4f7hd3hrrrhw6lfdy1s5m9ak0z713k0si6nsb550r")))

