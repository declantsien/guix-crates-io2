(define-module (crates-io in fl inflector) #:use-module (crates-io))

(define-public crate-Inflector-0.1.1 (c (n "Inflector") (v "0.1.1") (d (list (d (n "regex") (r "^0.1.8") (d #t) (k 0)))) (h "1lahqif7ywff6irp354rirmsg858mfccw12sy71gyrzk5793mxjd")))

(define-public crate-Inflector-0.1.2 (c (n "Inflector") (v "0.1.2") (d (list (d (n "regex") (r "^0.1.8") (d #t) (k 0)))) (h "0yng0d067207nmmmr5kppdpb45c1m8xjsszdkcvk7k49983lz22f")))

(define-public crate-Inflector-0.1.3 (c (n "Inflector") (v "0.1.3") (d (list (d (n "regex") (r "^0.1.41") (d #t) (k 0)))) (h "1jymcydm85bsx6bcapnv8km9yk70w3z1n0x8jhj52657am7b307r")))

(define-public crate-Inflector-0.1.4 (c (n "Inflector") (v "0.1.4") (d (list (d (n "regex") (r "^0.1.41") (d #t) (k 0)))) (h "154r14n1zn0a4q46qcv1n9x4pfgnbcmhzqbwm78sm8rqar4k5bp7")))

(define-public crate-Inflector-0.1.5 (c (n "Inflector") (v "0.1.5") (d (list (d (n "regex") (r "^0.1.41") (d #t) (k 0)))) (h "03mxfhywlw5jas05zv4d4533sw6ccjclrn2qmm52d3jgn0x7dm85")))

(define-public crate-Inflector-0.1.6 (c (n "Inflector") (v "0.1.6") (d (list (d (n "regex") (r "^0.1.41") (d #t) (k 0)))) (h "1ji7giz7l7iyny7zjfspy0p85gn83kjgcb6nrqd1v412h6pks3gv")))

(define-public crate-Inflector-0.2.0 (c (n "Inflector") (v "0.2.0") (d (list (d (n "regex") (r "^0.1.41") (d #t) (k 0)))) (h "0jr3pmpwc82yjvzh8vv3jlslj21b2c3py497xibf052hclkp2cd4")))

(define-public crate-Inflector-0.2.1 (c (n "Inflector") (v "0.2.1") (d (list (d (n "clippy") (r "^0.0.76") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.1.41") (d #t) (k 0)))) (h "0bviy0y94xnf9lqm8gq5vjrzjw11kvc7wscghxf5qqr2kn349vkl") (f (quote (("default"))))))

(define-public crate-Inflector-0.3.0 (c (n "Inflector") (v "0.3.0") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.41") (d #t) (k 0)))) (h "191zq94pnwck1zn01d9d0h4kgs5pabbqlaw24lm0k6cr1k1j0r8r")))

(define-public crate-Inflector-0.3.1 (c (n "Inflector") (v "0.3.1") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.73") (d #t) (k 0)))) (h "0g51kspwsl67kfldp65100z751qinhg4pbrfvw3g86cryy64cb3l")))

(define-public crate-Inflector-0.3.2 (c (n "Inflector") (v "0.3.2") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.73") (d #t) (k 0)))) (h "1hsqn0wg1rrg7xazfzfnjyld8p3y80y5as18zskyif63qn6xxh4d") (y #t)))

(define-public crate-Inflector-0.3.3 (c (n "Inflector") (v "0.3.3") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.73") (d #t) (k 0)))) (h "02g9wlykziq2rj8lcf6pq6i38r72smim6z7sjqv9zykyh5g14yxs")))

(define-public crate-Inflector-0.4.0 (c (n "Inflector") (v "0.4.0") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.73") (d #t) (k 0)))) (h "023p117zmvwn6jfmy2xym90x4ivyblwxqsq0bnyzir42fld9g8cx") (f (quote (("unstable"))))))

(define-public crate-Inflector-0.5.0 (c (n "Inflector") (v "0.5.0") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.73") (d #t) (k 0)))) (h "089wiqdz6mq9p8xc4nvwyb7yqx4kn0pijilxa9mgv4l4fbmg8mfg") (f (quote (("unstable"))))))

(define-public crate-Inflector-0.5.1 (c (n "Inflector") (v "0.5.1") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.73") (d #t) (k 0)))) (h "16r5463az8snxn5wp7pnfqwb8qy8mc4hh8mcvr4kwk0z8h35dqqn") (f (quote (("unstable"))))))

(define-public crate-Inflector-0.6.0 (c (n "Inflector") (v "0.6.0") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.73") (d #t) (k 0)))) (h "1x3z60s7dqlvwcfwqc0ix88l6khd0921qw6kj7v002n8jxvzmv0d") (f (quote (("unstable"))))))

(define-public crate-Inflector-0.7.0 (c (n "Inflector") (v "0.7.0") (d (list (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "14ifmg84zrr1xbr31k7b7la6nbzakhspg9iklr6rg0vk60jrvhf7") (f (quote (("unstable"))))))

(define-public crate-Inflector-0.8.0 (c (n "Inflector") (v "0.8.0") (d (list (d (n "lazy_static") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0i49ays1flmfdr9bhfyws1pvvbc6cygyqxw9wjs3qgss8axdbanb") (f (quote (("unstable") ("lightweight") ("default" "regex" "lazy_static"))))))

(define-public crate-Inflector-0.8.1 (c (n "Inflector") (v "0.8.1") (d (list (d (n "lazy_static") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.2") (o #t) (d #t) (k 0)))) (h "12c6nqhxcgkm8ccv05zy0kffvxzaysgjlpy36dwh81wqbmcgyhjb") (f (quote (("unstable") ("heavyweight" "regex" "lazy_static") ("default" "heavyweight"))))))

(define-public crate-Inflector-0.9.0 (c (n "Inflector") (v "0.9.0") (d (list (d (n "lazy_static") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1jb7dcjap2v8jnk7whvdz8mcb7lsnmyhz43zdwavfwaq3s71n8hd") (f (quote (("unstable") ("heavyweight" "regex" "lazy_static") ("default" "heavyweight"))))))

(define-public crate-Inflector-0.10.0 (c (n "Inflector") (v "0.10.0") (d (list (d (n "lazy_static") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1a9b3w59ribz5r2kvr5ql5x5nfqlzfy95fxk70mga1lhrl8hbm4d") (f (quote (("unstable") ("heavyweight" "regex" "lazy_static") ("default" "heavyweight"))))))

(define-public crate-Inflector-0.10.1 (c (n "Inflector") (v "0.10.1") (d (list (d (n "lazy_static") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.2") (o #t) (d #t) (k 0)))) (h "14jpsbxl64bi63p5l229jhp7pwv1lca6xmq7dmmkrs3x6xr2c7kg") (f (quote (("unstable") ("heavyweight" "regex" "lazy_static") ("default" "heavyweight"))))))

(define-public crate-Inflector-0.11.0 (c (n "Inflector") (v "0.11.0") (d (list (d (n "lazy_static") (r "^0.2.9") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0vl9y6rxrvxz4gycw8md1xcbymi5jlgj1xvdzc1bjcjdc6ibf6xr") (f (quote (("unstable") ("heavyweight" "regex" "lazy_static") ("default" "heavyweight"))))))

(define-public crate-Inflector-0.11.1 (c (n "Inflector") (v "0.11.1") (d (list (d (n "lazy_static") (r "^0.2.9") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1fvk9qpza2w01cii8f0vvqz7jrg6h11chp7w2z0zifpjvc0xf5hy") (f (quote (("unstable") ("heavyweight" "regex" "lazy_static") ("default" "heavyweight"))))))

(define-public crate-Inflector-0.11.2 (c (n "Inflector") (v "0.11.2") (d (list (d (n "lazy_static") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0k51ymw730d53xbxzii0z8473x6jfb3b7iskki9zqc1pcndwscqv") (f (quote (("unstable") ("heavyweight" "regex" "lazy_static") ("default" "heavyweight"))))))

(define-public crate-Inflector-0.11.3 (c (n "Inflector") (v "0.11.3") (d (list (d (n "lazy_static") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0pdyzrr0dk8xiwlyjnm4lhggqga5i74z36rmff15yq8zns5zjrs4") (f (quote (("unstable") ("heavyweight" "regex" "lazy_static") ("default" "heavyweight"))))))

(define-public crate-Inflector-0.11.4 (c (n "Inflector") (v "0.11.4") (d (list (d (n "lazy_static") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.1") (o #t) (d #t) (k 0)))) (h "1lqmcni21ifzyq41fhz6k1j2b23cmsx469s4g4sf01l78miqqhzy") (f (quote (("unstable") ("heavyweight" "regex" "lazy_static") ("default" "heavyweight"))))))

