(define-module (crates-io in fl influxdb-client) #:use-module (crates-io))

(define-public crate-influxdb-client-0.1.0 (c (n "influxdb-client") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "influxdb-derives") (r "^0.1.0") (d #t) (k 0)) (d (n "mockito") (r "^0.29.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.1") (k 0)) (d (n "tokio-test") (r "^0.4.0") (d #t) (k 2)))) (h "0fdixbms30mdrsn55bhv6arcckbll6hrlj3y69khnwq0i8rkh6aa")))

(define-public crate-influxdb-client-0.1.1 (c (n "influxdb-client") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "influxdb-derives") (r "^0.1.0") (d #t) (k 0)) (d (n "mockito") (r "^0.29.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.1") (k 0)) (d (n "tokio-test") (r "^0.4.0") (d #t) (k 2)))) (h "1hqzn92w879axc5af8af89gkdkp2n1wh3m4psgpy88gjmk2sil0b")))

(define-public crate-influxdb-client-0.1.2 (c (n "influxdb-client") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "influxdb-derives") (r "^0.1.0") (d #t) (k 0)) (d (n "mockito") (r "^0.29.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.1") (k 0)) (d (n "tokio") (r "^1.2.0") (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.0") (d #t) (k 2)))) (h "1xx4j7jp819hjm7bxh7zbcpkd01m7565xrfvnqa8wbl362yvjpp5")))

(define-public crate-influxdb-client-0.1.3 (c (n "influxdb-client") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "influxdb-derives") (r "^0.1.0") (d #t) (k 0)) (d (n "mockito") (r "^0.29.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.1") (k 0)) (d (n "tokio") (r "^1.2.0") (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.0") (d #t) (k 2)))) (h "0av11q32wy23xpim7pykxhksavc5wb07bl9vy9phcw0ah24azaz0")))

(define-public crate-influxdb-client-0.1.4 (c (n "influxdb-client") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "influxdb-derives") (r "^0.1.0") (d #t) (k 0)) (d (n "mockito") (r "^0.29.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.1") (k 0)) (d (n "tokio") (r "^1.2.0") (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.0") (d #t) (k 2)))) (h "06zc7crgchv16x6k2njf51nrijy4cilibj2v4l4l439flplb5q4r")))

