(define-module (crates-io in fl inflekt) #:use-module (crates-io))

(define-public crate-inflekt-0.1.0 (c (n "inflekt") (v "0.1.0") (d (list (d (n "inflections") (r "^1.1.1") (d #t) (k 0)))) (h "17s2scaxwxfcy178n0zkanlspj979yqhz3ddjnkrp6pk07lj76vj")))

(define-public crate-inflekt-0.2.0 (c (n "inflekt") (v "0.2.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "inflections") (r "^1.1.1") (d #t) (k 0)))) (h "0444l8pvb64ik2aschzzj6gx5illaxrivkkgxv41i640cxxhgkz3")))

(define-public crate-inflekt-0.2.1 (c (n "inflekt") (v "0.2.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "inflections") (r "^1.1.1") (d #t) (k 0)))) (h "0ydrafk72l47xn3sf3p45hzydb473653ljvd7f1ifhsb8i2cylbk")))

(define-public crate-inflekt-0.2.2 (c (n "inflekt") (v "0.2.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "inflections") (r "^1.1.1") (d #t) (k 0)))) (h "01x1cqys8hx78493vrb7rr2yz41rrv00s2ih9f54hnybx579hcb7")))

(define-public crate-inflekt-0.3.0 (c (n "inflekt") (v "0.3.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "inflections") (r "^1.1.1") (d #t) (k 0)))) (h "08q3r7v4jifn5wm77dx53b0x9xqk70bflbx9skyyl7i1iihgah6m")))

