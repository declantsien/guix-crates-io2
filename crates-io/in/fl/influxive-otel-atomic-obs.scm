(define-module (crates-io in fl influxive-otel-atomic-obs) #:use-module (crates-io))

(define-public crate-influxive-otel-atomic-obs-0.0.1-alpha.7 (c (n "influxive-otel-atomic-obs") (v "0.0.1-alpha.7") (d (list (d (n "opentelemetry_api") (r "^0.20.0-beta.1") (f (quote ("metrics"))) (d #t) (k 0) (p "ts_opentelemetry_api")))) (h "0zzqbwqy844dwsdg1fj8vy493i9mycnamy6ac4p4gzhw3dkz3gn4")))

(define-public crate-influxive-otel-atomic-obs-0.0.1-alpha.8 (c (n "influxive-otel-atomic-obs") (v "0.0.1-alpha.8") (d (list (d (n "opentelemetry_api") (r "^0.20.0-beta.1") (f (quote ("metrics"))) (d #t) (k 0) (p "ts_opentelemetry_api")))) (h "1bczm0zagqcxjxkajcsjwdckncplfh7jq4r380di00hcy1wsnx4d")))

(define-public crate-influxive-otel-atomic-obs-0.0.1-alpha.9 (c (n "influxive-otel-atomic-obs") (v "0.0.1-alpha.9") (d (list (d (n "opentelemetry_api") (r "^0.20.0-beta.1") (f (quote ("metrics"))) (d #t) (k 0) (p "ts_opentelemetry_api")))) (h "0952qp5xfrz9nh9qjyf6pj8hgzp30hhdx5jaxgp3myxj9xi4mlbm")))

(define-public crate-influxive-otel-atomic-obs-0.0.1-alpha.10 (c (n "influxive-otel-atomic-obs") (v "0.0.1-alpha.10") (d (list (d (n "opentelemetry_api") (r "^0.20.0-beta.1") (f (quote ("metrics"))) (d #t) (k 0) (p "ts_opentelemetry_api")))) (h "0bh37fr1kjmgqzqsp99hi243ymc4sniq1aaq97v5j29vfwrgz6jm")))

(define-public crate-influxive-otel-atomic-obs-0.0.1-alpha.11 (c (n "influxive-otel-atomic-obs") (v "0.0.1-alpha.11") (d (list (d (n "opentelemetry_api") (r "^0.20.0-beta.1") (f (quote ("metrics"))) (d #t) (k 0) (p "ts_opentelemetry_api")))) (h "05pxg8xb1ji95i0fxxvvz8yys1lm08kg17335n5pplk7j7kwqyxh")))

(define-public crate-influxive-otel-atomic-obs-0.0.2-alpha.1 (c (n "influxive-otel-atomic-obs") (v "0.0.2-alpha.1") (d (list (d (n "opentelemetry_api") (r "^0.20.0") (f (quote ("metrics"))) (d #t) (k 0)))) (h "0ppk9jlrha4mh9rgbz0d2qjjgc3nqg740qaxq532m1i83l8frh0s")))

