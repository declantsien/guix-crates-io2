(define-module (crates-io in fl influxdb2-derive) #:use-module (crates-io))

(define-public crate-influxdb2-derive-0.1.0 (c (n "influxdb2-derive") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0c5wdf4rmijz8nk3m8zhm3s9sr5nn7ys0cc8ym9z72inr3ihgq71")))

(define-public crate-influxdb2-derive-0.1.1 (c (n "influxdb2-derive") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "05ngmd2m15lssysx8gy0j9dkvg1crkikhybgq07i6c5a86c8j3wr")))

