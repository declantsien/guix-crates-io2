(define-module (crates-io in fl inflection) #:use-module (crates-io))

(define-public crate-inflection-0.1.0 (c (n "inflection") (v "0.1.0") (h "1iwg4zd8cksccggbcyrfpsdqynyis3hrbs7g43pjny8n7vc7zsw7")))

(define-public crate-inflection-0.0.1 (c (n "inflection") (v "0.0.1") (h "10jgp509rmkpdpgwhkylqiaibicravdbglzw3rxlg7xlr6n1brwv") (y #t)))

(define-public crate-inflection-0.1.1 (c (n "inflection") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "titlecase") (r "^1.0") (d #t) (k 0)))) (h "19vajgrlwzw3jj1fqnasxljdm3bfk0dw2m9n69b3k6r9p1rwq8cq")))

(define-public crate-inflection-0.1.2 (c (n "inflection") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "titlecase") (r "^1.0") (d #t) (k 0)))) (h "046cfgpsmclm1yh84hdpymsyk50vz9lvs9r0wz87vw9vvib5bxfn")))

