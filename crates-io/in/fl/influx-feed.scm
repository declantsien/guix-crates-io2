(define-module (crates-io in fl influx-feed) #:use-module (crates-io))

(define-public crate-influx-feed-0.1.0 (c (n "influx-feed") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "clokwerk") (r "^0.3.3") (d #t) (k 0)) (d (n "influxdb") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1yjgb1jg68ii0k1p4p79cqhnr410d9ybi1cxlfazi8qw8vafc8zi")))

