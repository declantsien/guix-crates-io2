(define-module (crates-io in fl influxdb_derive) #:use-module (crates-io))

(define-public crate-influxdb_derive-0.1.0 (c (n "influxdb_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0jn3dsf3i2ig7yxsd3ya0jvdmhnf0xriy54qkalil4cb579bm8a0")))

(define-public crate-influxdb_derive-0.2.0 (c (n "influxdb_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "12mrxcaikd8dzxw16ykx2iqmx1x6hsyn7i4fgq5rwhhc3pslfpbw")))

(define-public crate-influxdb_derive-0.3.0 (c (n "influxdb_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0vhyzcwkadqbg2jb17lb5wwqmnn7ka6jsid7pmrqp6ypss2inddx")))

(define-public crate-influxdb_derive-0.4.0 (c (n "influxdb_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1asn474mr46mclg5v409yqvfxg80gw3v6sxkaii7z0l7yvmla4rj")))

(define-public crate-influxdb_derive-0.5.0 (c (n "influxdb_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1ls10b4jh44fwsnd9n40z76k8965rgnvba05gxj6rh6fjyzyz0ax")))

(define-public crate-influxdb_derive-0.5.1 (c (n "influxdb_derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1yrk46djbc70b7krfv4zrmlk5wwkrj3bq80b5giwvl7gc0v6pjba")))

