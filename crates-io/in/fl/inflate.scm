(define-module (crates-io in fl inflate) #:use-module (crates-io))

(define-public crate-inflate-0.1.0 (c (n "inflate") (v "0.1.0") (h "192k5ysybxwdx828mmmv7rvdydxk2ypg5f1ggc6033bjga2j3486") (f (quote (("unstable") ("default"))))))

(define-public crate-inflate-0.1.1 (c (n "inflate") (v "0.1.1") (h "1fs3gkcfypag11bskzghka8gz8p8d8qra3bm2cppvwf25lnhdq77") (f (quote (("unstable") ("default"))))))

(define-public crate-inflate-0.2.0 (c (n "inflate") (v "0.2.0") (h "1r9pqj7mpgjajhfbn1vrl25vhm2qafchi63lgj597wsscwj8a8yi") (f (quote (("unstable") ("default"))))))

(define-public crate-inflate-0.3.0 (c (n "inflate") (v "0.3.0") (d (list (d (n "adler32") (r "^1.0.0") (d #t) (k 0)))) (h "0ya360v1hm12vcjsj1469anaks0zd0pzcvd458znh6z0yfqzykyz") (f (quote (("unstable") ("default"))))))

(define-public crate-inflate-0.3.1 (c (n "inflate") (v "0.3.1") (d (list (d (n "adler32") (r "^1.0.0") (d #t) (k 0)))) (h "1nv5h394ji3154pmh8lw87m89v2vc3f35nwjlv2gq8kyzdvz274d") (f (quote (("unstable") ("default"))))))

(define-public crate-inflate-0.3.2 (c (n "inflate") (v "0.3.2") (d (list (d (n "adler32") (r "^1.0.0") (d #t) (k 0)))) (h "0s78n6hbk0lqi5nfyr8p5qwi45rk40ym4r2kg91mxq9fp98pmwfj") (f (quote (("unstable") ("default"))))))

(define-public crate-inflate-0.3.3 (c (n "inflate") (v "0.3.3") (d (list (d (n "adler32") (r "^1.0.0") (d #t) (k 0)))) (h "0jy6sa2ivzlq93mhz8pd5dbi76j7rl4adky0idw5qz6zi9ihbv0h") (f (quote (("unstable") ("default"))))))

(define-public crate-inflate-0.3.4 (c (n "inflate") (v "0.3.4") (d (list (d (n "adler32") (r "^1.0.0") (d #t) (k 0)))) (h "1m2rxb68fyy5q3k7bzlips15m1pyi3fgl7i7a9j6m9z9d1sg9ygm") (f (quote (("unstable") ("default"))))))

(define-public crate-inflate-0.4.0 (c (n "inflate") (v "0.4.0") (d (list (d (n "adler32") (r "^1.0.2") (d #t) (k 0)))) (h "0pz4mr118myfbih5ij7q3b4b3rhgqnxpwzpyc6xs8y9af7mg9ylp") (f (quote (("unstable") ("default"))))))

(define-public crate-inflate-0.4.1 (c (n "inflate") (v "0.4.1") (d (list (d (n "adler32") (r "^1.0.2") (d #t) (k 0)))) (h "02kzg8kxc6krlz51in52ji2i4b9vwsg6vlnk24yw0a59mpqv1ki3") (f (quote (("unstable") ("default"))))))

(define-public crate-inflate-0.4.2 (c (n "inflate") (v "0.4.2") (d (list (d (n "adler32") (r "^1.0.2") (d #t) (k 0)))) (h "1k3d2k31f9d7yc1p6h02g8i5bkni1sv5zqz8bvk19z802ac8vhaf") (f (quote (("unstable") ("default"))))))

(define-public crate-inflate-0.4.3 (c (n "inflate") (v "0.4.3") (d (list (d (n "adler32") (r "^1.0.2") (d #t) (k 0)))) (h "0ia2pck6jmslabyma7p6ypx0xsbx4ysacg34z765f84fxq8vhlvg") (f (quote (("unstable") ("default"))))))

(define-public crate-inflate-0.4.4 (c (n "inflate") (v "0.4.4") (d (list (d (n "adler32") (r "^1.0.2") (d #t) (k 0)))) (h "19zbfr71a8q35chxjcbsdqmnf5s81v1mkr733f7knhfqwayq7il4") (f (quote (("unstable") ("default"))))))

(define-public crate-inflate-0.4.5 (c (n "inflate") (v "0.4.5") (d (list (d (n "adler32") (r "^1.0.2") (d #t) (k 0)))) (h "1zxjdn8iwa0ssxrnjmywm3r1v284wryvzrf8vkc7nyf5ijbjknqw") (f (quote (("unstable") ("default"))))))

