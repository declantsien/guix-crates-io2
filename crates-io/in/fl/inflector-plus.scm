(define-module (crates-io in fl inflector-plus) #:use-module (crates-io))

(define-public crate-inflector-plus-0.11.5 (c (n "inflector-plus") (v "0.11.5") (d (list (d (n "lazy_static") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.1") (o #t) (d #t) (k 0)))) (h "1f8ya9niq96k1l5rkf8j5n1h4ii6gx1bkl7cfv9840gxwv0lg0by") (f (quote (("unstable") ("heavyweight" "regex" "lazy_static") ("default" "heavyweight"))))))

(define-public crate-inflector-plus-0.11.6 (c (n "inflector-plus") (v "0.11.6") (d (list (d (n "lazy_static") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.1") (o #t) (d #t) (k 0)))) (h "1b6am5c84hxxac44frai4djx62a9jg1k0j9w95gpr1al64qdzcfc") (f (quote (("unstable") ("heavyweight" "regex" "lazy_static") ("default" "heavyweight"))))))

(define-public crate-inflector-plus-0.11.7 (c (n "inflector-plus") (v "0.11.7") (d (list (d (n "lazy_static") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.1") (o #t) (d #t) (k 0)))) (h "0507c0fnhsb801zprsdzvzxqc0lai7vhfz8d7xlpqqm1kzyj1g3b") (f (quote (("unstable") ("heavyweight" "regex" "lazy_static") ("default" "heavyweight"))))))

