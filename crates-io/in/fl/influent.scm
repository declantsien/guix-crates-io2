(define-module (crates-io in fl influent) #:use-module (crates-io))

(define-public crate-influent-0.1.0 (c (n "influent") (v "0.1.0") (d (list (d (n "hyper") (r "^0.6.8") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)))) (h "0q1rp0h1d5brgglvlqdlrby72fanxlbgb70s2phn77qdaic77cji")))

(define-public crate-influent-0.2.0 (c (n "influent") (v "0.2.0") (d (list (d (n "hyper") (r "^0.6.8") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)))) (h "1i607ya7sghxq5i5q49pl9qr920vpbhcdnqzhmrbwd2agxsnw6b4")))

(define-public crate-influent-0.2.1 (c (n "influent") (v "0.2.1") (d (list (d (n "hyper") (r "^0.6.8") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)))) (h "07jyk6bgc9vnvdw6fgdxik48n91042rg974122jzvdbjyhdhvh0g")))

(define-public crate-influent-0.2.2 (c (n "influent") (v "0.2.2") (d (list (d (n "hyper") (r "^0.7.2") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)))) (h "0zijaa14yj9xrrv9q17ymmfzjiy7dbh37cl3g2hmj8ndh5szhmvf")))

(define-public crate-influent-0.3.0 (c (n "influent") (v "0.3.0") (d (list (d (n "hyper") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)))) (h "1204cyvn8gvx6iv7n74aadn2bfjip9knvvmfmx55ranaavgxav2m") (f (quote (("http" "hyper") ("default" "http"))))))

(define-public crate-influent-0.3.1 (c (n "influent") (v "0.3.1") (d (list (d (n "hyper") (r "^0.6.8") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)))) (h "0i6dz5ndgl4aql7nq11v51k0dqmdj8nhym1x3q40d9k74zxy55c2") (f (quote (("http" "hyper") ("default" "http"))))))

(define-public crate-influent-0.4.0 (c (n "influent") (v "0.4.0") (d (list (d (n "hyper") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)))) (h "0b2zwmbhrkn38jp73a0yanlb8jsm87majv7rkamy66s3hcdk2ax2") (f (quote (("http" "hyper") ("default" "http"))))))

(define-public crate-influent-0.4.1 (c (n "influent") (v "0.4.1") (d (list (d (n "hyper") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "03h7hdmd3v2zwfz1aspw38wc27n9r0vwg361sdpy2fpmy7b6bhlv") (f (quote (("http" "hyper") ("default" "http"))))))

(define-public crate-influent-0.5.0 (c (n "influent") (v "0.5.0") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "1h28i9d795ym5wqki7qh0vjzqg55bmdcs9xrayzrq09jldgzc1lg")))

(define-public crate-influent-0.5.1 (c (n "influent") (v "0.5.1") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "0j5k1fik8p2d0zyhcqqpapapwa811r62jiki8ms04qqldwh5piyh")))

(define-public crate-influent-0.5.2 (c (n "influent") (v "0.5.2") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1w60d9d6zw7dq4278raqfwr4lmzc69mvc6yy04wvib2skfmfmzc7")))

(define-public crate-influent-0.5.3 (c (n "influent") (v "0.5.3") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "00j0qag9khlh558vxsijkpb5xhrazwpa43dkhd3xn8bp2fxqia04")))

