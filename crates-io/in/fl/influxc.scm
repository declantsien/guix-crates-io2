(define-module (crates-io in fl influxc) #:use-module (crates-io))

(define-public crate-influxc-0.4.2 (c (n "influxc") (v "0.4.2") (d (list (d (n "base32") (r "^0.4") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0yp9igxx96zdjx902hfr7qcpl0ycsphpbr3b41km8nr2d8jfm97q")))

