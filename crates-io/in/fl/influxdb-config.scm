(define-module (crates-io in fl influxdb-config) #:use-module (crates-io))

(define-public crate-influxdb-config-0.1.0 (c (n "influxdb-config") (v "0.1.0") (d (list (d (n "envconfig") (r "^0.7") (d #t) (k 0)) (d (n "envconfig_derive") (r "^0.7") (d #t) (k 0)) (d (n "influxdb") (r "^0.1") (d #t) (k 0)))) (h "1adxfal9i8b550c398xbz194vkzgm01i68hyldvswvsiwc72k8i1")))

(define-public crate-influxdb-config-0.2.0 (c (n "influxdb-config") (v "0.2.0") (d (list (d (n "envconfig") (r "^0.7") (d #t) (k 0)) (d (n "envconfig_derive") (r "^0.7") (d #t) (k 0)) (d (n "influxdb") (r "^0.2") (d #t) (k 0)))) (h "0qna6bydmka6ml8fa3dlvzjq9rs2s92xxmjw7rz0jfd32449g956")))

(define-public crate-influxdb-config-0.3.0 (c (n "influxdb-config") (v "0.3.0") (d (list (d (n "envconfig") (r "^0.7") (d #t) (k 0)) (d (n "envconfig_derive") (r "^0.7") (d #t) (k 0)) (d (n "influxdb") (r "^0.3") (d #t) (k 0)))) (h "1arfrkfk07bf72rxs2x05wrh046zcl690bhdlqkinlwylpwf8ih3")))

(define-public crate-influxdb-config-0.4.0 (c (n "influxdb-config") (v "0.4.0") (d (list (d (n "envconfig") (r "^0.7") (d #t) (k 0)) (d (n "envconfig_derive") (r "^0.7") (d #t) (k 0)) (d (n "influxdb") (r "^0.4") (d #t) (k 0)))) (h "1a4kj1qy0mzp58qllswkdzqn2z3i4psb0jnxaghv3l36qavsi8y6")))

(define-public crate-influxdb-config-0.4.1 (c (n "influxdb-config") (v "0.4.1") (d (list (d (n "envconfig") (r "^0.9") (d #t) (k 0)) (d (n "influxdb") (r "^0.4") (d #t) (k 0)))) (h "1cc1fnmq2dxqs0kbv8d6q3bhzh9vs7k6f1cqbn2f87ph9kfc95j8")))

(define-public crate-influxdb-config-0.5.2 (c (n "influxdb-config") (v "0.5.2") (d (list (d (n "envconfig") (r "^0.10.0") (d #t) (k 0)) (d (n "influxdb") (r "^0.5.2") (d #t) (k 0)))) (h "0j1q35bykfi3b5zzzpbkk8rzqgx2gm6cq9q0w3kp24m82r1mmlj6") (y #t)))

(define-public crate-influxdb-config-0.5.3 (c (n "influxdb-config") (v "0.5.3") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("env" "derive"))) (d #t) (k 0)) (d (n "compact_str") (r "^0.6.1") (d #t) (k 0)) (d (n "influxdb") (r "^0.5.2") (d #t) (k 0)))) (h "16np2rikp2hkrx90sgybnnc15v0fmkfq0zka46r6498ivygpdcgw") (y #t)))

(define-public crate-influxdb-config-0.5.5 (c (n "influxdb-config") (v "0.5.5") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("env" "derive"))) (d #t) (k 0)) (d (n "compact_str") (r "^0.6.1") (d #t) (k 0)) (d (n "influxdb") (r "^0.5.2") (d #t) (k 0)))) (h "0ysykirn1ggcq1znkgvkh2xc8aka7fxfhfxarp3czss3n3c6712k") (y #t)))

(define-public crate-influxdb-config-0.5.4 (c (n "influxdb-config") (v "0.5.4") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("env" "derive"))) (d #t) (k 0)) (d (n "compact_str") (r "^0.6.1") (d #t) (k 0)) (d (n "influxdb") (r "^0.5.2") (d #t) (k 0)))) (h "0bjpqx7kzq2pfa3v9iw2gcm5q11r7igiw186171ih1mja40hhm7f") (y #t)))

(define-public crate-influxdb-config-0.5.6 (c (n "influxdb-config") (v "0.5.6") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("env" "derive"))) (d #t) (k 0)) (d (n "compact_str") (r "^0.6.1") (d #t) (k 0)) (d (n "influxdb") (r "^0.5.2") (d #t) (k 0)))) (h "1fmxaivkks3jsy0lcgiabwan8lxmc7za4wlpsvnr4n3yfpafblwd")))

(define-public crate-influxdb-config-0.7.0 (c (n "influxdb-config") (v "0.7.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("env" "derive"))) (d #t) (k 0)) (d (n "compact_str") (r "^0.7.1") (d #t) (k 0)) (d (n "influxdb") (r "^0.7.2") (d #t) (k 0)))) (h "14q19q8hi7w7rllb1c74npfbkf4c45zbxd23pbh0kr6w5aab3y94")))

