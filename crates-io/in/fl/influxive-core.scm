(define-module (crates-io in fl influxive-core) #:use-module (crates-io))

(define-public crate-influxive-core-0.0.1-alpha.3 (c (n "influxive-core") (v "0.0.1-alpha.3") (h "1nln20vwgbij9rnrlldmjijfcpf82q24xzl5ip03469rmdqwz87m")))

(define-public crate-influxive-core-0.0.1-alpha.4 (c (n "influxive-core") (v "0.0.1-alpha.4") (h "0fhisa6q5plpl4f1xby32bzmfbg5kgm6q7ikhz0qmf5dyh7fz7i3")))

(define-public crate-influxive-core-0.0.1-alpha.5 (c (n "influxive-core") (v "0.0.1-alpha.5") (h "08f1rw7vs6v2ck103yig1qrvrrabnhbrjd87rpwiwkm912kmj2a6")))

(define-public crate-influxive-core-0.0.1-alpha.6 (c (n "influxive-core") (v "0.0.1-alpha.6") (h "0hm065q601p3lqgdq19b7s20gbjf6cq5cz2g5bn06ifsa9g8r905")))

(define-public crate-influxive-core-0.0.1-alpha.7 (c (n "influxive-core") (v "0.0.1-alpha.7") (h "02zhh9xhy4zxd2y7c4y4kmq4i8y4xnsj21dmkvhjdm91yccr3d77")))

(define-public crate-influxive-core-0.0.1-alpha.8 (c (n "influxive-core") (v "0.0.1-alpha.8") (h "1mx7y6c4mcbrivrifvqigx9sxm5rjqixz3v1makkq3180dbpsilp")))

(define-public crate-influxive-core-0.0.1-alpha.9 (c (n "influxive-core") (v "0.0.1-alpha.9") (h "03n33bdnrf50ssqj9agwfz9b1g1wsl1san5gi3dwb3zjvs45mc8n")))

(define-public crate-influxive-core-0.0.1-alpha.10 (c (n "influxive-core") (v "0.0.1-alpha.10") (h "1y9ai2viq1wl9kls5dsa2fm2jyz8c7w2ri5c69r4dbffa927kf0x")))

(define-public crate-influxive-core-0.0.1-alpha.11 (c (n "influxive-core") (v "0.0.1-alpha.11") (h "00pnyzikbhvjp2g0yv8g2izdpl8iryh91yxlvwnc0d3di52b46in")))

(define-public crate-influxive-core-0.0.2-alpha.1 (c (n "influxive-core") (v "0.0.2-alpha.1") (h "000djb8p2iqba0y914daxzvpr9kqi6rna6m6dg8vd5xv3sb7inzm")))

