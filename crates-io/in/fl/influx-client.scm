(define-module (crates-io in fl influx-client) #:use-module (crates-io))

(define-public crate-influx-client-0.1.0 (c (n "influx-client") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "ureq") (r "^0.11") (d #t) (k 0)))) (h "0pi4s56lzsayr882fx6qbjj452n152izchkj9nnaf6w3ijf7pvj8")))

(define-public crate-influx-client-0.2.0 (c (n "influx-client") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "ureq") (r "^0.11") (d #t) (k 0)))) (h "0f4g7hk6i1v84cwvxg8g8hkih5wgv63szamrwd9i5d07w9rp22a8")))

(define-public crate-influx-client-0.3.0 (c (n "influx-client") (v "0.3.0") (d (list (d (n "csv") (r "^1.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.1") (f (quote ("blocking"))) (d #t) (k 0)))) (h "18mcnclviz6mns7ps63mg680jmbj47ji8hfvcfnwqmq4734vml08")))

(define-public crate-influx-client-0.3.1 (c (n "influx-client") (v "0.3.1") (d (list (d (n "csv") (r "^1.1.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.1") (f (quote ("blocking"))) (d #t) (k 0)))) (h "14ljab1y24bp3pqxj5ds51x1acp1gzv8y0ryyq3x3fkvjlaih0n6")))

(define-public crate-influx-client-0.4.0 (c (n "influx-client") (v "0.4.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("full"))) (d #t) (k 2)))) (h "0r7nfk3y37kx6idrqn3sfm56z9dck96g1rv5wdfcnqqybbmcakn5")))

(define-public crate-influx-client-0.4.1 (c (n "influx-client") (v "0.4.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("full"))) (d #t) (k 2)))) (h "121k6wf9hlbz9abmjjw7a3yqas69ch4zraa1mhyfifwi2vxhcvs6")))

