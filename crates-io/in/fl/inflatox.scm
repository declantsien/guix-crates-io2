(define-module (crates-io in fl inflatox) #:use-module (crates-io))

(define-public crate-inflatox-0.1.0 (c (n "inflatox") (v "0.1.0") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "numpy") (r "^0.19") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "0v73997k7s32jj0msknzxz0smqr5ry0yfhk2p1p1qh8v8q4by8kq") (f (quote (("pyo3_extension_module" "pyo3/extension-module") ("default" "pyo3_extension_module")))) (y #t)))

(define-public crate-inflatox-0.2.0 (c (n "inflatox") (v "0.2.0") (d (list (d (n "libloading") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "numpy") (r "^0.19") (d #t) (k 0)) (d (n "pyo3") (r "^0.19") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "105sggz45pg69nslz3wmln209h7j9fjx3wc2rym4w553v9h6paai") (f (quote (("pyo3_extension_module" "pyo3/extension-module" "pyo3/generate-import-lib" "pyo3/abi3-py37") ("default" "pyo3_extension_module")))) (y #t)))

