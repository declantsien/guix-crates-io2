(define-module (crates-io in fl inflections) #:use-module (crates-io))

(define-public crate-inflections-1.0.0 (c (n "inflections") (v "1.0.0") (h "1rgqkjl0j828bxbmc0nxp2762k2jv3g2c0f6dq1n2wydjqmr69d8")))

(define-public crate-inflections-1.1.0 (c (n "inflections") (v "1.1.0") (h "13d6fqf1a91q5g1bgds8gl5d2ihbhbqlflzrghm7xch4fqf4py76")))

(define-public crate-inflections-1.1.1 (c (n "inflections") (v "1.1.1") (h "0yl3gas612q25c72lwf04405i87yxr02vgv3ckcnz2fyvhpmhmx2")))

