(define-module (crates-io in fl inflection-rs) #:use-module (crates-io))

(define-public crate-inflection-rs-0.1.0 (c (n "inflection-rs") (v "0.1.0") (d (list (d (n "deunicode") (r "^1.3.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0q24rg23nddin3hviqpf9ggpn51miwf2ssgx6l3bj506zyyclmrz") (r "1.61.0")))

(define-public crate-inflection-rs-0.1.1 (c (n "inflection-rs") (v "0.1.1") (d (list (d (n "deunicode") (r "^1.3.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "18vvnn5z5hr0yj4p8p89yi1ly8vh9f6i78q3mmm036zv31nw3r90") (r "1.61.0")))

(define-public crate-inflection-rs-0.1.2 (c (n "inflection-rs") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "deunicode") (r "^1.3.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "14j2ncpmwvy6qlywqpwrv4wrngn01c306wrlhzyjrfmx5cznr091") (r "1.61.0")))

(define-public crate-inflection-rs-0.2.0 (c (n "inflection-rs") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "deunicode") (r "^1.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "string-utility") (r "^0.2") (d #t) (k 0)))) (h "0ivqz3a17wxny856j86pqz2rqxsa28jg1pmy8vbr7aivfs89wfmv") (r "1.61.0")))

