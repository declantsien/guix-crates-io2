(define-module (crates-io in fl influxdb-line-protocol) #:use-module (crates-io))

(define-public crate-influxdb-line-protocol-0.1.0 (c (n "influxdb-line-protocol") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qjbyism89ipzzmq53h8hfk96afjr20zmg7mi855mihbskqzki61")))

(define-public crate-influxdb-line-protocol-0.1.1 (c (n "influxdb-line-protocol") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bn1fvy61hd9fyq7d926r377z6ygidqyradrmds614fcw9capg1b")))

(define-public crate-influxdb-line-protocol-0.2.0 (c (n "influxdb-line-protocol") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0wn0cp6zf87hdvrnhf3hkidqsb2ixlis274w4p8sjcrqr8wwbjwy") (f (quote (("std" "thiserror") ("default" "std"))))))

(define-public crate-influxdb-line-protocol-1.0.0 (c (n "influxdb-line-protocol") (v "1.0.0") (d (list (d (n "bytes") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("std"))) (k 0)) (d (n "smallvec") (r "^1.10.0") (f (quote ("union"))) (d #t) (k 0)) (d (n "snafu") (r "^0.7") (d #t) (k 0)))) (h "0yk9lanjq1kkpypp6d0gnd88m5r7iwscy47lq40x09778xl434hi")))

(define-public crate-influxdb-line-protocol-2.0.0 (c (n "influxdb-line-protocol") (v "2.0.0") (d (list (d (n "bytes") (r "^1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("std"))) (k 0)) (d (n "smallvec") (r "^1.11.1") (f (quote ("union"))) (d #t) (k 0)) (d (n "snafu") (r "^0.7") (d #t) (k 0)))) (h "1jwd8h4fjr42w5vlln7igklka21qr28jr5ibj6qs07j5pvk7xyi2")))

