(define-module (crates-io in fl influxdb2-structmap) #:use-module (crates-io))

(define-public crate-influxdb2-structmap-0.1.5 (c (n "influxdb2-structmap") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0v86gr38cqzi4gs1y60fr283sv58ryqg029g7w7zn4zvyc76r61y")))

(define-public crate-influxdb2-structmap-0.2.0 (c (n "influxdb2-structmap") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)))) (h "0rb25gvvzgwa05hr7g7sl377jgl3x122wwzzk5z3b1qp0l9ff20l")))

