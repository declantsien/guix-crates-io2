(define-module (crates-io in fl influxdb-derives) #:use-module (crates-io))

(define-public crate-influxdb-derives-0.1.0 (c (n "influxdb-derives") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.7.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("extra-traits" "derive" "printing"))) (d #t) (k 0)))) (h "07as3cjlx696vv0zqx357smn6lm50asvh87wgim3iznpjb8klivc")))

(define-public crate-influxdb-derives-0.1.1 (c (n "influxdb-derives") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.7.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("extra-traits" "parsing" "derive" "printing"))) (d #t) (k 0)))) (h "0fknmr61mv15y54phsvvfycn3cbaarngq481crq18a7dkv22kx5k")))

