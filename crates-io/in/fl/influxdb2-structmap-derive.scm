(define-module (crates-io in fl influxdb2-structmap-derive) #:use-module (crates-io))

(define-public crate-influxdb2-structmap-derive-0.1.5 (c (n "influxdb2-structmap-derive") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1p9fc1gxbkncbppvh9ll7fqmzgy0drsb8h9vi3im64nf62ggdwas")))

