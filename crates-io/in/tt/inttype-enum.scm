(define-module (crates-io in tt inttype-enum) #:use-module (crates-io))

(define-public crate-inttype-enum-0.1.0 (c (n "inttype-enum") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.20") (f (quote ("full"))) (d #t) (k 0)))) (h "1ys3nndj8qjyikpapl03p1fnlnifb2rxdwz7ml0wawwz5zpkyr68") (y #t)))

(define-public crate-inttype-enum-0.1.1 (c (n "inttype-enum") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.20") (f (quote ("full"))) (d #t) (k 0)))) (h "05pgfszfcnnzbxljba2dfqnkyr6x99dlky09l03w9r795gyb0hqs")))

(define-public crate-inttype-enum-0.1.2 (c (n "inttype-enum") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.20") (f (quote ("full"))) (d #t) (k 0)))) (h "0gywy2qkdvhqk5byjp386qqrwxiphp6xdl9x8sj6ahhy8d9w7bs2")))

