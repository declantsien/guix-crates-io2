(define-module (crates-io in gl ingl_macros) #:use-module (crates-io))

(define-public crate-ingl_macros-0.1.0 (c (n "ingl_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "03kx613rx33ddi58g5gj1hlxldk4nr5sgyx1z5zwwxp1q0pc1l9a")))

