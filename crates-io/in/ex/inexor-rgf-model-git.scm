(define-module (crates-io in ex inexor-rgf-model-git) #:use-module (crates-io))

(define-public crate-inexor-rgf-model-git-0.9.1 (c (n "inexor-rgf-model-git") (v "0.9.1") (d (list (d (n "git2") (r "^0.16") (d #t) (k 0)) (d (n "indradb-lib") (r "^3") (d #t) (k 0)) (d (n "inexor-rgf-core-model") (r "^0.9.1") (d #t) (k 0)) (d (n "inexor-rgf-model-base") (r "^0.9.1") (d #t) (k 0)) (d (n "inexor-rgf-model-file") (r "^0.9.1") (d #t) (k 0)) (d (n "inexor-rgf-model-http") (r "^0.9.1") (d #t) (k 0)) (d (n "inexor-rgf-model-runtime") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0qhjgfzsn2dqglvn4c1dz5ijg5g0lsnaxbqyplcayaba9bcvbs1j")))

