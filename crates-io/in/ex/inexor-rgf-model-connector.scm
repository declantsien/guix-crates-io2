(define-module (crates-io in ex inexor-rgf-model-connector) #:use-module (crates-io))

(define-public crate-inexor-rgf-model-connector-0.9.1 (c (n "inexor-rgf-model-connector") (v "0.9.1") (d (list (d (n "indradb-lib") (r "^3") (d #t) (k 0)) (d (n "inexor-rgf-core-model") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1y7f1lb675y45zv09mnzr6h7spjdcb301mc4m9b6i8vw9p4h8jr1")))

