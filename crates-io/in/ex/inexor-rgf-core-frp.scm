(define-module (crates-io in ex inexor-rgf-core-frp) #:use-module (crates-io))

(define-public crate-inexor-rgf-core-frp-0.9.0 (c (n "inexor-rgf-core-frp") (v "0.9.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std" "serde" "std" "serde"))) (d #t) (k 0)) (d (n "log4rs") (r "^1.2") (f (quote ("console_appender" "file_appender" "toml_format" "ansi_writer" "console_appender" "file_appender" "toml_format"))) (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("serde" "v4" "fast-rng" "v4"))) (d #t) (k 0)))) (h "1fxsvvkqm0s1cq2hdg6ihqli5nn6421wnrppf2rd7hk6a5lh62w8")))

(define-public crate-inexor-rgf-core-frp-0.9.1 (c (n "inexor-rgf-core-frp") (v "0.9.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std" "serde" "std" "serde"))) (d #t) (k 0)) (d (n "log4rs") (r "^1.2") (f (quote ("console_appender" "file_appender" "toml_format" "ansi_writer" "console_appender" "file_appender" "toml_format"))) (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("serde" "v4" "fast-rng" "v4"))) (d #t) (k 0)))) (h "1cmd2ykmmw1rafky4xdl3bcfi8nbwf2hv8m0vc5wy3pd29crl298")))

