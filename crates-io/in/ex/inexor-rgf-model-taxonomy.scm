(define-module (crates-io in ex inexor-rgf-model-taxonomy) #:use-module (crates-io))

(define-public crate-inexor-rgf-model-taxonomy-0.9.1 (c (n "inexor-rgf-model-taxonomy") (v "0.9.1") (d (list (d (n "indradb-lib") (r "^3") (d #t) (k 0)) (d (n "inexor-rgf-core-model") (r "^0.9.1") (d #t) (k 0)) (d (n "inexor-rgf-model-base") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1yqg94v5q9cwdaf2qf3faik4s5g20nlv2g592f7v504glxvxghy4")))

