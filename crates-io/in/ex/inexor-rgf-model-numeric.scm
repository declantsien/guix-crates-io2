(define-module (crates-io in ex inexor-rgf-model-numeric) #:use-module (crates-io))

(define-public crate-inexor-rgf-model-numeric-0.9.1 (c (n "inexor-rgf-model-numeric") (v "0.9.1") (d (list (d (n "indradb-lib") (r "^3") (d #t) (k 0)) (d (n "inexor-rgf-core-model") (r "^0.9.1") (d #t) (k 0)) (d (n "inexor-rgf-core-reactive") (r "^0.9.1") (d #t) (k 0)) (d (n "inexor-rgf-model-result") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1v8d5mj2klhinihq6grh63wc0f8s2p46vjwwiwccm89yf3azq67f")))

