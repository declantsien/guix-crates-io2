(define-module (crates-io in ex inexor-rgf-core-di) #:use-module (crates-io))

(define-public crate-inexor-rgf-core-di-0.9.0 (c (n "inexor-rgf-core-di") (v "0.9.0") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "inexor-rgf-core-di-codegen") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "1kkhljm5jij7pzinv6p7gbhhr2z15z759d626bqjg4kf9k337scp") (f (quote (("async" "inexor-rgf-core-di-codegen/async"))))))

(define-public crate-inexor-rgf-core-di-0.9.1 (c (n "inexor-rgf-core-di") (v "0.9.1") (d (list (d (n "config") (r "^0.13") (d #t) (k 0)) (d (n "inexor-rgf-core-di-codegen") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (d #t) (k 0)))) (h "180sndlsqfm65gc3d7lfns2vah91rcjqsl3f93qkj3hc1y71pm0b") (f (quote (("async" "inexor-rgf-core-di-codegen/async"))))))

