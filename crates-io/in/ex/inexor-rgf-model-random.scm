(define-module (crates-io in ex inexor-rgf-model-random) #:use-module (crates-io))

(define-public crate-inexor-rgf-model-random-0.9.1 (c (n "inexor-rgf-model-random") (v "0.9.1") (d (list (d (n "indradb-lib") (r "^3") (d #t) (k 0)) (d (n "inexor-rgf-core-model") (r "^0.9.1") (d #t) (k 0)) (d (n "inexor-rgf-model-result") (r "^0.9.1") (d #t) (k 0)) (d (n "inexor-rgf-model-runtime") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0k5vxassh7djh5rz4f27h1am2wrvnrpbk47lpx1l5x73plbwf975")))

