(define-module (crates-io in ex inexor-rgf-core-di-codegen) #:use-module (crates-io))

(define-public crate-inexor-rgf-core-di-codegen-0.9.0 (c (n "inexor-rgf-core-di-codegen") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "full"))) (d #t) (k 0)))) (h "18s0sqqsj1a325320sjnadwq7kfd4fgvzwpdzw2rpmwyg2a5bi5k") (f (quote (("async"))))))

(define-public crate-inexor-rgf-core-di-codegen-0.9.1 (c (n "inexor-rgf-core-di-codegen") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "full"))) (d #t) (k 0)))) (h "04b3mxmbc3al6lqnz8j65vbwwvq93mdz4a9gi21msfjychk2h26b") (f (quote (("async"))))))

