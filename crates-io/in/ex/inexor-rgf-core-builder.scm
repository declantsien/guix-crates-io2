(define-module (crates-io in ex inexor-rgf-core-builder) #:use-module (crates-io))

(define-public crate-inexor-rgf-core-builder-0.9.0 (c (n "inexor-rgf-core-builder") (v "0.9.0") (d (list (d (n "indradb-lib") (r "^3") (d #t) (k 0)) (d (n "inexor-rgf-core-model") (r "^0.9.0") (d #t) (k 0)) (d (n "random-string") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("serde" "v4" "serde" "v4"))) (d #t) (k 0)))) (h "1lwji2mxkzilkqz8ljwap7jzxmir343hqimxp4nvwnmf4pkifp55")))

(define-public crate-inexor-rgf-core-builder-0.9.1 (c (n "inexor-rgf-core-builder") (v "0.9.1") (d (list (d (n "indradb-lib") (r "^3") (d #t) (k 0)) (d (n "inexor-rgf-core-model") (r "^0.9.1") (d #t) (k 0)) (d (n "random-string") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("serde" "v4" "serde" "v4"))) (d #t) (k 0)))) (h "13hnb7xs9qgc6rakg03san5wigblyyycpsh02ryrhw4w8kjl05n0")))

