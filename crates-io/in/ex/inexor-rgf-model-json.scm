(define-module (crates-io in ex inexor-rgf-model-json) #:use-module (crates-io))

(define-public crate-inexor-rgf-model-json-0.9.1 (c (n "inexor-rgf-model-json") (v "0.9.1") (d (list (d (n "indradb-lib") (r "^3") (d #t) (k 0)) (d (n "inexor-rgf-core-model") (r "^0.9.1") (d #t) (k 0)) (d (n "inexor-rgf-model-base") (r "^0.9.1") (d #t) (k 0)) (d (n "inexor-rgf-model-file") (r "^0.9.1") (d #t) (k 0)) (d (n "inexor-rgf-model-result") (r "^0.9.1") (d #t) (k 0)) (d (n "inexor-rgf-model-runtime") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0lj4yr468mfwdjcf1zp22whgsfdw9iv2z7j4gpylmkf3q01665jx")))

