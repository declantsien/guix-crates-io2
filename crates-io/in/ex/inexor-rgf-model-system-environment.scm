(define-module (crates-io in ex inexor-rgf-model-system-environment) #:use-module (crates-io))

(define-public crate-inexor-rgf-model-system-environment-0.9.1 (c (n "inexor-rgf-model-system-environment") (v "0.9.1") (d (list (d (n "indradb-lib") (r "^3") (d #t) (k 0)) (d (n "inexor-rgf-core-model") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "uuid") (r "^1.2") (f (quote ("serde" "v4" "v5" "serde" "v4" "v5"))) (d #t) (k 0)))) (h "0rzp9b4krc7jxq1r2v07n76qw3mdxrybfkaylsf4arhggqi8ksl3")))

