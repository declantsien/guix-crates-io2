(define-module (crates-io in ex inexor-rgf-model-http) #:use-module (crates-io))

(define-public crate-inexor-rgf-model-http-0.9.1 (c (n "inexor-rgf-model-http") (v "0.9.1") (d (list (d (n "indradb-lib") (r "^3") (d #t) (k 0)) (d (n "inexor-rgf-core-model") (r "^0.9.1") (d #t) (k 0)) (d (n "inexor-rgf-model-result") (r "^0.9.1") (d #t) (k 0)) (d (n "inexor-rgf-model-runtime") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "1y2ch2padnfnmkqhgnrrmq6a34qi6wpr991sxvqp931i8vgilv1q")))

