(define-module (crates-io in ex inexor-rgf-model-trigger) #:use-module (crates-io))

(define-public crate-inexor-rgf-model-trigger-0.9.1 (c (n "inexor-rgf-model-trigger") (v "0.9.1") (d (list (d (n "indradb-lib") (r "^3") (d #t) (k 0)) (d (n "inexor-rgf-core-model") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "license") (r "^3.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1i4wv6v1cr3ngm924dps24pv1cyswkcmpz6yyb75cviz1z6kf4ba")))

