(define-module (crates-io in ox inox2d-opengl) #:use-module (crates-io))

(define-public crate-inox2d-opengl-0.1.0 (c (n "inox2d-opengl") (v "0.1.0") (d (list (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "glow") (r "^0.13.1") (d #t) (k 0)) (d (n "inox2d") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1kqbzchy9z9j1an0if6ib7h5gzdkvv64nl4ld87gdcf6hazy40qj")))

