(define-module (crates-io in ox inox2d-wgpu) #:use-module (crates-io))

(define-public crate-inox2d-wgpu-0.1.0 (c (n "inox2d-wgpu") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "encase") (r "^0.7.0") (f (quote ("glam"))) (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (f (quote ("bytemuck"))) (d #t) (k 0)) (d (n "inox2d") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.1") (f (quote ("webgl"))) (d #t) (k 0)))) (h "1mn3vscsa926j5r87yc6nphcnmjz1s1pzznjc304b7znsn09a815")))

