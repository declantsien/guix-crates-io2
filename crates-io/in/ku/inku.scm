(define-module (crates-io in ku inku) #:use-module (crates-io))

(define-public crate-inku-0.1.0 (c (n "inku") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.20.0") (d #t) (k 2)))) (h "0nsc6mf22phvymdcc4hw4pb47q7xnw32q9vrvp69xlppikp8qi27")))

(define-public crate-inku-0.2.0 (c (n "inku") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.20.0") (d #t) (k 2)))) (h "09pwz09c3r6fvziksyhma8gw3fxsaw63yqb4w3lmj15izi48c96l")))

(define-public crate-inku-0.3.0 (c (n "inku") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.20.0") (d #t) (k 2)))) (h "1bh0759xfm55blcpzcz9x7bpdmlqvvlyyga4rkb48axf8iq491ba")))

(define-public crate-inku-0.4.0 (c (n "inku") (v "0.4.0") (d (list (d (n "crossterm") (r "^0.20.0") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.20.0") (d #t) (k 2)))) (h "0v9cdx7114yngfnr13hnkbd5hjwjk784fn7crsssn0pylrlyfwhi") (f (quote (("color_debug" "crossterm"))))))

