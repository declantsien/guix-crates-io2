(define-module (crates-io in id inid_rs) #:use-module (crates-io))

(define-public crate-inid_rs-0.1.0 (c (n "inid_rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1w7fmsm8vnvzbpvmcc94hfzmchy3cmk9bgpr4hn3bijh38bxfcc6")))

(define-public crate-inid_rs-0.1.1 (c (n "inid_rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "011b3jnm4g07zcv8pnivv93h93m4xapzi0mk3y3k2kmig4jglld8")))

(define-public crate-inid_rs-0.1.2 (c (n "inid_rs") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1v3ifnzvxfspllwg45vbsgzamj4n6svz9j8k4ik53wcp69hmqz91")))

