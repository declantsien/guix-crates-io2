(define-module (crates-io in dv indvec) #:use-module (crates-io))

(define-public crate-indvec-0.1.0 (c (n "indvec") (v "0.1.0") (h "04xmf3sqnv708rni1f0pfqg2cjd1hmz5ri0qh4xi4rngj6yfrggw")))

(define-public crate-indvec-0.2.0 (c (n "indvec") (v "0.2.0") (h "1pv94vf2icvzcggln2vgwgy0r9z97j1hli53asykkr6g43hwaf35")))

