(define-module (crates-io in t_ int_traits) #:use-module (crates-io))

(define-public crate-int_traits-0.1.0 (c (n "int_traits") (v "0.1.0") (h "1s1ah3fnyln3dn1rnxl24j1gb4ldc14ns8m9ni1zhkqsr2djr46k")))

(define-public crate-int_traits-0.1.1 (c (n "int_traits") (v "0.1.1") (h "0ki09xrs25w7d1fch81wfv8ibxsfdcdyn9fwqi8x0rwxb5f9lg5k")))

