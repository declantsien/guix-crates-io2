(define-module (crates-io in t_ int_ranges) #:use-module (crates-io))

(define-public crate-int_ranges-0.1.0 (c (n "int_ranges") (v "0.1.0") (h "0dzlagvw8ya7rr7q4r04jnxa79k0fbaf6iyapa1ghp51clxrkgp1")))

(define-public crate-int_ranges-0.1.1 (c (n "int_ranges") (v "0.1.1") (h "151s4w7hqn5shln7znyhy2v5lbcp0iqgkz922yhrgm06nkg9x8mk")))

