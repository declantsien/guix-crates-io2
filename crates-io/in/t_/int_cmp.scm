(define-module (crates-io in t_ int_cmp) #:use-module (crates-io))

(define-public crate-int_cmp-0.1.0 (c (n "int_cmp") (v "0.1.0") (h "07ir01jwnfbpfrp3mgdx1c358acm8rgf28j1l9v9lkbm16kr132a")))

(define-public crate-int_cmp-0.2.0 (c (n "int_cmp") (v "0.2.0") (h "16p3vznp5vkjdpl5ma51h2a7m33kcv6i1drrzv4m69hm5g6y5127")))

(define-public crate-int_cmp-0.2.1 (c (n "int_cmp") (v "0.2.1") (h "059xkjbrdf6ppq6j1brias04c4f646plg7sblxf5sijxl7sg512i")))

