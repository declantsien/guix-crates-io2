(define-module (crates-io in t_ int_hash) #:use-module (crates-io))

(define-public crate-int_hash-0.1.0 (c (n "int_hash") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 2)) (d (n "rustc-hash") (r "^1") (d #t) (k 2)) (d (n "seahash") (r "^3") (d #t) (k 2)) (d (n "twox-hash") (r "^1") (d #t) (k 2)))) (h "0dgkrdjxgqvcrfjycj4csay82ci980y4r846zrzwgg9gwkzg81j3") (y #t)))

(define-public crate-int_hash-0.1.1 (c (n "int_hash") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 2)) (d (n "rustc-hash") (r "^1") (d #t) (k 2)) (d (n "seahash") (r "^3") (d #t) (k 2)) (d (n "twox-hash") (r "^1") (d #t) (k 2)))) (h "0qg9mz2dwbff7a1q7lzbjvv9bhysfbbba4m33i62dkad4yqhzcc2") (y #t)))

(define-public crate-int_hash-0.2.0 (c (n "int_hash") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 2)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "seahash") (r "^3") (d #t) (k 2)) (d (n "twox-hash") (r "^1") (d #t) (k 2)))) (h "0jwf543rpqr1jrg1q47i2qrmnikfm60c8wpxwfm55hydc0p721lp") (y #t)))

(define-public crate-int_hash-0.2.1 (c (n "int_hash") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 2)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "seahash") (r "^3") (d #t) (k 2)) (d (n "twox-hash") (r "^1") (d #t) (k 2)))) (h "1828fgxynndp54ily84lpnc4s4kagfy29x6vm1qyp6rpv0ia6wm1") (y #t)))

