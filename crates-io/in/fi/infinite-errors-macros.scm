(define-module (crates-io in fi infinite-errors-macros) #:use-module (crates-io))

(define-public crate-infinite-errors-macros-0.1.0 (c (n "infinite-errors-macros") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (f (quote ("std"))) (k 2)) (d (n "proc-macro2") (r "^1.0.78") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full" "parsing" "printing"))) (k 0)))) (h "1y1517g5jqyhi1cj90fjd2adqvy3b3dh3azghymf2y1i6bis8vpr")))

(define-public crate-infinite-errors-macros-0.2.0 (c (n "infinite-errors-macros") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (f (quote ("std"))) (k 2)) (d (n "proc-macro2") (r "^1.0.78") (f (quote ("proc-macro"))) (k 0)) (d (n "quote") (r "^1.0.35") (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full" "parsing" "printing"))) (k 0)))) (h "1skwb18fa5y0n0y2nmdsyrsxkpn15ylhp83ay60ry6xd81h405ya")))

