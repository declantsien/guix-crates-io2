(define-module (crates-io in fi infinity-sampler) #:use-module (crates-io))

(define-public crate-infinity-sampler-0.1.0 (c (n "infinity-sampler") (v "0.1.0") (d (list (d (n "delegate") (r "^0.12.0") (d #t) (k 0)))) (h "1sjgkivhvw8123j2ijyrz83ahb9m9cas3d3lak8jqjgqkpvrzy58")))

(define-public crate-infinity-sampler-0.1.1 (c (n "infinity-sampler") (v "0.1.1") (d (list (d (n "delegate") (r "^0.12.0") (d #t) (k 0)))) (h "12328f7zi7ajrv0fflgpzx4bqj5jwx9z6slgjjmnpsyhmbkb8wga")))

(define-public crate-infinity-sampler-0.2.0 (c (n "infinity-sampler") (v "0.2.0") (h "1y4ym08k38fkrgim6fg1x9anyd7r469vlrzpyxpjlcwsnn45bgn2") (f (quote (("microoptimizations"))))))

(define-public crate-infinity-sampler-0.3.0 (c (n "infinity-sampler") (v "0.3.0") (d (list (d (n "heapless") (r "^0.8") (d #t) (k 0)))) (h "068fr0lhzbrka0f7acvsp97ysvs6f7ad48kcj4arl5wygb886f13") (f (quote (("microoptimizations"))))))

