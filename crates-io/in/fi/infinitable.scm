(define-module (crates-io in fi infinitable) #:use-module (crates-io))

(define-public crate-infinitable-1.0.0 (c (n "infinitable") (v "1.0.0") (h "02vwfy4mk6fpk1z3sj520ba037hrrd352aqnvykzlm25r4fziaif")))

(define-public crate-infinitable-1.0.1 (c (n "infinitable") (v "1.0.1") (h "1qqrazg2l1x2l6x4n5wn68c5d8f6i7sa23vnhmnd76l3365h2rhm")))

(define-public crate-infinitable-1.1.0 (c (n "infinitable") (v "1.1.0") (h "0xp9jwnlx2mz0301nkxgz1z564c01g745ydfl579lvylm2519a37")))

(define-public crate-infinitable-1.2.0 (c (n "infinitable") (v "1.2.0") (h "04gpilmcw991alfm87rv35pr68lcfj5kv128lpw6l2525vh1vv9s")))

(define-public crate-infinitable-1.3.0 (c (n "infinitable") (v "1.3.0") (h "1rzf4yyzdk1d8kam5cis2cqxgkcnrdr578jwfrcds00nqpgmblkq")))

(define-public crate-infinitable-1.4.0 (c (n "infinitable") (v "1.4.0") (h "1bpah4wm2vj57m4i9q8ly0xjfzkcz800wkf0n4yrkiz4fplcsm6m")))

(define-public crate-infinitable-1.5.0 (c (n "infinitable") (v "1.5.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "1a89w4s7rm1035bf3mi4d6fnphm1nbm75zljn59x54cib36sn8b5")))

(define-public crate-infinitable-1.6.0 (c (n "infinitable") (v "1.6.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0aq8p0d3422rj65002kd6is21ng22fn1l7si5gnbkykyk0d4p3ik")))

