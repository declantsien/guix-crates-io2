(define-module (crates-io in fi infisearch_lang_ascii_stemmer) #:use-module (crates-io))

(define-public crate-infisearch_lang_ascii_stemmer-0.10.0 (c (n "infisearch_lang_ascii_stemmer") (v "0.10.0") (d (list (d (n "infisearch_common") (r "=0.10.0") (d #t) (k 0)) (d (n "infisearch_lang_ascii") (r "=0.10.0") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.0") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "1wh66imxy3lhhwq5zz9c0yjdj6ivp7988jdsw083p0r8pv41ihb0") (f (quote (("indexer"))))))

(define-public crate-infisearch_lang_ascii_stemmer-0.10.1 (c (n "infisearch_lang_ascii_stemmer") (v "0.10.1") (d (list (d (n "infisearch_common") (r "=0.10.1") (d #t) (k 0)) (d (n "infisearch_lang_ascii") (r "=0.10.1") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.0") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "0yrcxp469j7fdhk3v4p0dhdn97rkvj7l5a069643wj8h6ka691xb") (f (quote (("indexer"))))))

