(define-module (crates-io in fi infinidash) #:use-module (crates-io))

(define-public crate-infinidash-0.1.0 (c (n "infinidash") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0mz7mq5j6kz2pkbydq1g239lc89ns873r5nyfprjhw4dja161941")))

(define-public crate-infinidash-0.7.42 (c (n "infinidash") (v "0.7.42") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0mxn67dgnzbysyhdzbd5dx9f97cbfskgzqga9j6j1clhdbgx7skc")))

(define-public crate-infinidash-0.8.42 (c (n "infinidash") (v "0.8.42") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "web3") (r "^0.16.0") (d #t) (k 0)))) (h "0wlqw1hmhkg5pq8lmq1ann1c4kdq2j6ih8p1anq1ib5mnkrzljd8")))

