(define-module (crates-io in fi infisearch_common) #:use-module (crates-io))

(define-public crate-infisearch_common-0.8.0 (c (n "infisearch_common") (v "0.8.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "1hlf21kaqwz3lfypsx3hghql9kgnvyr5diwc3y12asykiz6vxn5a") (f (quote (("indexer" "serde") ("default"))))))

(define-public crate-infisearch_common-0.8.1 (c (n "infisearch_common") (v "0.8.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "1cpzkankd43ivd19vkrijwpq3kpw9ayiwcx1wkbj53ymdq26l6fa") (f (quote (("indexer" "serde") ("default"))))))

(define-public crate-infisearch_common-0.8.2 (c (n "infisearch_common") (v "0.8.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "1kn2dwc4pf4xsircq6adhfamxm73j6ad9s7s4f2685vx3wxyd3nz") (f (quote (("indexer" "serde") ("default"))))))

(define-public crate-infisearch_common-0.8.3 (c (n "infisearch_common") (v "0.8.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "0hzzmr96iakj5cl9nl9qna091jl9n5b4lqx9jdydcmib5fwyw3al") (f (quote (("indexer" "serde") ("default"))))))

(define-public crate-infisearch_common-0.8.4 (c (n "infisearch_common") (v "0.8.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "1w4hril0gri4llji03val2ax1cpj45j81c0jghs02nf5x50fqp22") (f (quote (("indexer" "serde") ("default"))))))

(define-public crate-infisearch_common-0.8.5 (c (n "infisearch_common") (v "0.8.5") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "00d89kw8c9bcxxlwi6sh3vff7r8fqkpi5ampa8323q00hnd4v9gz") (f (quote (("indexer" "serde") ("default"))))))

(define-public crate-infisearch_common-0.8.6 (c (n "infisearch_common") (v "0.8.6") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "0nsw036abjmzgpv0j4qlr2m4qpx2vrl1ybrl2xx1dzdrr8zp9psb") (f (quote (("indexer" "serde") ("default"))))))

(define-public crate-infisearch_common-0.8.7 (c (n "infisearch_common") (v "0.8.7") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "1xpxl54h29b8ssvxdp9in8396rz4j9lkj2fcwh7pcqsyh6z41ww6") (f (quote (("indexer" "serde") ("default"))))))

(define-public crate-infisearch_common-0.8.8 (c (n "infisearch_common") (v "0.8.8") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "10d2pjd0yk800qirrqkdcr68cv0fjn2pvc7zpwakgc0lpdi3ls9c") (f (quote (("indexer" "serde") ("default"))))))

(define-public crate-infisearch_common-0.9.0 (c (n "infisearch_common") (v "0.9.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "1jnfrnq2rpk2y0n8j8kzgpbh2f1gyc4qc6wf675ji6br47aj8cp5") (f (quote (("indexer" "serde") ("default"))))))

(define-public crate-infisearch_common-0.9.1 (c (n "infisearch_common") (v "0.9.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "1hvfr3ca30zpmqi1fjik6y3dz9827iwq21x95yqgv86zai4rvy2v") (f (quote (("indexer" "serde") ("default"))))))

(define-public crate-infisearch_common-0.10.0 (c (n "infisearch_common") (v "0.10.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "0gkjh9bl419j1qfcbc591h5lijv76hiv1zf1wxxk75pzb6sijwdm") (f (quote (("indexer" "serde") ("default"))))))

(define-public crate-infisearch_common-0.10.1 (c (n "infisearch_common") (v "0.10.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "0h6li13qdhygbhlsdjin271cx1syxhkvlqrl44qpsyq3kwgsv5m2") (f (quote (("indexer" "serde") ("default"))))))

