(define-module (crates-io in fi infisearch_lang_latin) #:use-module (crates-io))

(define-public crate-infisearch_lang_latin-0.8.0 (c (n "infisearch_lang_latin") (v "0.8.0") (d (list (d (n "infisearch_common") (r "=0.8.0") (d #t) (k 0)) (d (n "infisearch_lang_ascii") (r "=0.8.0") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.0") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "1wmv200zvd0qvvwcbbin7zvzi8h01z608s055k0r7jljjp8vam0g") (f (quote (("indexer"))))))

(define-public crate-infisearch_lang_latin-0.8.1 (c (n "infisearch_lang_latin") (v "0.8.1") (d (list (d (n "infisearch_common") (r "=0.8.1") (d #t) (k 0)) (d (n "infisearch_lang_ascii") (r "=0.8.1") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.0") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "0a4dkq9fry9ddlqshxc83agzajl72hwn2ibmwbmx91jp3rfkvna2") (f (quote (("indexer"))))))

(define-public crate-infisearch_lang_latin-0.8.2 (c (n "infisearch_lang_latin") (v "0.8.2") (d (list (d (n "infisearch_common") (r "=0.8.2") (d #t) (k 0)) (d (n "infisearch_lang_ascii") (r "=0.8.2") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.0") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "0pjqr7lzcdh55v3y5k0bj7jikbj7n0xrqqdwpdgxrdgpcb4hkqn0") (f (quote (("indexer"))))))

(define-public crate-infisearch_lang_latin-0.8.3 (c (n "infisearch_lang_latin") (v "0.8.3") (d (list (d (n "infisearch_common") (r "=0.8.3") (d #t) (k 0)) (d (n "infisearch_lang_ascii") (r "=0.8.3") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.0") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "0xisnprcw24p5bdaaf3gbp8bzdqqfyfq0vqivz1xcxnp428pczfn") (f (quote (("indexer"))))))

(define-public crate-infisearch_lang_latin-0.8.4 (c (n "infisearch_lang_latin") (v "0.8.4") (d (list (d (n "infisearch_common") (r "=0.8.4") (d #t) (k 0)) (d (n "infisearch_lang_ascii") (r "=0.8.4") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.0") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "1n1nfna58hy4wvj1sm64gjyf9kigxym3cwkr69bj652wrcrvl6ja") (f (quote (("indexer"))))))

(define-public crate-infisearch_lang_latin-0.8.5 (c (n "infisearch_lang_latin") (v "0.8.5") (d (list (d (n "infisearch_common") (r "=0.8.5") (d #t) (k 0)) (d (n "infisearch_lang_ascii") (r "=0.8.5") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.0") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "1imzn2zwhxx3bp38kd8sy7lgfr0vz3ca166qkasxkwsgimyaxf5d") (f (quote (("indexer"))))))

(define-public crate-infisearch_lang_latin-0.8.6 (c (n "infisearch_lang_latin") (v "0.8.6") (d (list (d (n "infisearch_common") (r "=0.8.6") (d #t) (k 0)) (d (n "infisearch_lang_ascii") (r "=0.8.6") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.0") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "19r56ss7zz4vg9i57x77yb2wmax9lxm3np480gnimid70mvr21pb") (f (quote (("indexer"))))))

(define-public crate-infisearch_lang_latin-0.8.7 (c (n "infisearch_lang_latin") (v "0.8.7") (d (list (d (n "infisearch_common") (r "=0.8.7") (d #t) (k 0)) (d (n "infisearch_lang_ascii") (r "=0.8.7") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.0") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "1xa3m5zasv4rfxags041gwlxv28rrqnpy5vil7gwcg1sw65l2ikc") (f (quote (("indexer"))))))

(define-public crate-infisearch_lang_latin-0.8.8 (c (n "infisearch_lang_latin") (v "0.8.8") (d (list (d (n "infisearch_common") (r "=0.8.8") (d #t) (k 0)) (d (n "infisearch_lang_ascii") (r "=0.8.8") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.0") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "19f9vbcyzikynwpr29zn4zifrp8haxldg7xs2fkcgzqz0f732zgq") (f (quote (("indexer"))))))

(define-public crate-infisearch_lang_latin-0.9.0 (c (n "infisearch_lang_latin") (v "0.9.0") (d (list (d (n "infisearch_common") (r "=0.9.0") (d #t) (k 0)) (d (n "infisearch_lang_ascii") (r "=0.9.0") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.0") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "13c5jr9a6k0b2xmbwzzgq6idmh6i4ik2pcck5lygp7xx60l9waaj") (f (quote (("indexer"))))))

(define-public crate-infisearch_lang_latin-0.9.1 (c (n "infisearch_lang_latin") (v "0.9.1") (d (list (d (n "infisearch_common") (r "=0.9.1") (d #t) (k 0)) (d (n "infisearch_lang_ascii") (r "=0.9.1") (d #t) (k 0)) (d (n "rust-stemmers") (r "^1.0") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "0hqyrzxlqcy1ilzasjksi6n83l6zsczsgp6y0gcd43v32q4bjwn2") (f (quote (("indexer"))))))

