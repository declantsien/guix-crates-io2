(define-module (crates-io in fi infisearch_lang_ascii) #:use-module (crates-io))

(define-public crate-infisearch_lang_ascii-0.8.0 (c (n "infisearch_lang_ascii") (v "0.8.0") (d (list (d (n "infisearch_common") (r "=0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "1x27icgszm1riax18l5a07z3hqr1yl01n8zd7mhjq8yxhv7nylf6") (f (quote (("indexer" "lazy_static" "regex"))))))

(define-public crate-infisearch_lang_ascii-0.8.1 (c (n "infisearch_lang_ascii") (v "0.8.1") (d (list (d (n "infisearch_common") (r "=0.8.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "1ma2zb54j2s3pbplgb5522injsilgas0h9q201k71vn629lzvls5") (f (quote (("indexer" "lazy_static" "regex"))))))

(define-public crate-infisearch_lang_ascii-0.8.2 (c (n "infisearch_lang_ascii") (v "0.8.2") (d (list (d (n "infisearch_common") (r "=0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "1i1cj2rjqs26dwsb06mnss1n8yww0z4j02zsqjpjypkx6dnyhvid") (f (quote (("indexer" "lazy_static" "regex"))))))

(define-public crate-infisearch_lang_ascii-0.8.3 (c (n "infisearch_lang_ascii") (v "0.8.3") (d (list (d (n "infisearch_common") (r "=0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "1p4jlgn117sixc13s3ljzb394lg3x1lv5a8bq97s8m6j12dxd13s") (f (quote (("indexer" "lazy_static" "regex"))))))

(define-public crate-infisearch_lang_ascii-0.8.4 (c (n "infisearch_lang_ascii") (v "0.8.4") (d (list (d (n "infisearch_common") (r "=0.8.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "08p80kb0cqnlq11c10aqf5vmw1d7c3lna3qcyz213ib2izimiqyz") (f (quote (("indexer" "lazy_static" "regex"))))))

(define-public crate-infisearch_lang_ascii-0.8.5 (c (n "infisearch_lang_ascii") (v "0.8.5") (d (list (d (n "infisearch_common") (r "=0.8.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "1pd9w0zycalh2mg43xx57jsa3j18nc7pw3gj6k3wn8bdr1f4y41r") (f (quote (("indexer" "lazy_static" "regex"))))))

(define-public crate-infisearch_lang_ascii-0.8.6 (c (n "infisearch_lang_ascii") (v "0.8.6") (d (list (d (n "infisearch_common") (r "=0.8.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "0w5g085xha1r6mwwzl3m66159dl5yzwkjck4n00hdqrra75zsgzm") (f (quote (("indexer" "lazy_static" "regex"))))))

(define-public crate-infisearch_lang_ascii-0.8.7 (c (n "infisearch_lang_ascii") (v "0.8.7") (d (list (d (n "infisearch_common") (r "=0.8.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "03lnj4vjscssk3q7grlz0ca97w5lpf8hd6n65dgdd7a5vv1p1sy6") (f (quote (("indexer" "lazy_static" "regex"))))))

(define-public crate-infisearch_lang_ascii-0.8.8 (c (n "infisearch_lang_ascii") (v "0.8.8") (d (list (d (n "infisearch_common") (r "=0.8.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "00p0b1ycss7d16r0n8mvwvpy1s3b1zxfk37mg8bajw5c0gkc8rnj") (f (quote (("indexer" "lazy_static" "regex"))))))

(define-public crate-infisearch_lang_ascii-0.9.0 (c (n "infisearch_lang_ascii") (v "0.9.0") (d (list (d (n "infisearch_common") (r "=0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "16fqz1x2mg06vcsmml7zgkbcmihnqljmrgaj40bpgz7ks5pnnqzq") (f (quote (("indexer" "lazy_static" "regex"))))))

(define-public crate-infisearch_lang_ascii-0.9.1 (c (n "infisearch_lang_ascii") (v "0.9.1") (d (list (d (n "infisearch_common") (r "=0.9.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "1y9mdpvl1xi3rnar6lxsfzj7ia5wcg5nji7rm7jahi9jhsh0c63x") (f (quote (("indexer" "lazy_static" "regex"))))))

(define-public crate-infisearch_lang_ascii-0.10.0 (c (n "infisearch_lang_ascii") (v "0.10.0") (d (list (d (n "infisearch_common") (r "=0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "0b4zww81vahizgrrk08zk30packz14cvgcvibnl2f3mp6mlfq2jr") (f (quote (("indexer" "lazy_static" "regex"))))))

(define-public crate-infisearch_lang_ascii-0.10.1 (c (n "infisearch_lang_ascii") (v "0.10.1") (d (list (d (n "infisearch_common") (r "=0.10.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "smartstring") (r "^0.2.7") (d #t) (k 0)))) (h "1hsj98hla5znalsqyscvbjm6czkiawbxhx8q3jlfg34krq3i34lc") (f (quote (("indexer" "lazy_static" "regex"))))))

