(define-module (crates-io in fi infinity) #:use-module (crates-io))

(define-public crate-infinity-0.1.1 (c (n "infinity") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.36") (d #t) (k 1)))) (h "1zncx9mxqfjibd978jwavibwq9lfb78sgnwyyy5a0cl9rbi8ax83") (l "infinity")))

(define-public crate-infinity-0.2.1 (c (n "infinity") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.36") (d #t) (k 1)))) (h "1x7f730dj5vb29g6y0cd3nr2bawpyrrg1g77q2vgximdyd3xid9y") (l "infinity")))

