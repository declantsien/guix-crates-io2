(define-module (crates-io in fi infinispan-fork) #:use-module (crates-io))

(define-public crate-infinispan-fork-0.1.0 (c (n "infinispan-fork") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "diqwest") (r "^2.0.0") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serial_test") (r "^0.5") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0pmn4il3i4ip946zwr4dmjizqsqjxhik460n5rzyyalbz1ygasrz")))

