(define-module (crates-io in fi infix_macro) #:use-module (crates-io))

(define-public crate-infix_macro-0.1.0 (c (n "infix_macro") (v "0.1.0") (h "05zl4myjviwfrpqj65plnqpjiv0gr5mjwf0hpfmbsxql13axp21c")))

(define-public crate-infix_macro-0.1.1 (c (n "infix_macro") (v "0.1.1") (h "0k9galh5sg0m2pnz1xxzsk48b90rwl2jbks5x02qq0k3qpsc2mr2")))

