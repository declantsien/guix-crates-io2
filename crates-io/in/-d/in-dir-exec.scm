(define-module (crates-io in -d in-dir-exec) #:use-module (crates-io))

(define-public crate-in-dir-exec-0.1.0 (c (n "in-dir-exec") (v "0.1.0") (d (list (d (n "colored") (r "^1.9.2") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.9") (d #t) (k 0)))) (h "03ma9q1ay161mv5kj62am4x9w53l7lb5h516k9dvp0pw8s5brzxl")))

(define-public crate-in-dir-exec-0.1.1 (c (n "in-dir-exec") (v "0.1.1") (d (list (d (n "colored") (r "^1.9.2") (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.9") (d #t) (k 0)))) (h "1h8k9j6839wb8b4ikm56927ijn8y5ipxpq6hdfbglrq379fyqzki")))

