(define-module (crates-io in -d in-directory) #:use-module (crates-io))

(define-public crate-in-directory-1.0.0 (c (n "in-directory") (v "1.0.0") (d (list (d (n "joinery") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.78") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "1yl0wqc21w45xklaphl5jadnivf6pipl5142bgwwgvf3bhbbqi5y")))

(define-public crate-in-directory-1.0.1 (c (n "in-directory") (v "1.0.1") (d (list (d (n "joinery") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.78") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.18") (d #t) (k 0)))) (h "1h77xq956105xzdrrsqbgf9sprk892bnirdpkbn9466nlayqfxjv")))

