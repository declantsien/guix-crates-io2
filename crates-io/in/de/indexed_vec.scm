(define-module (crates-io in de indexed_vec) #:use-module (crates-io))

(define-public crate-indexed_vec-1.0.0 (c (n "indexed_vec") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1704y7a742ab400dz9361492xpir7p93b23sn35p3db3qb2w5m0x") (f (quote (("serial" "serde_derive" "serde") ("default" "serial"))))))

(define-public crate-indexed_vec-1.0.1 (c (n "indexed_vec") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0p3wz8czigcxr2d8xbc6qq9krl68rck4qi9zxq6r86yisgh0wj83") (f (quote (("serial" "serde_derive" "serde") ("default" "serial"))))))

(define-public crate-indexed_vec-1.0.2 (c (n "indexed_vec") (v "1.0.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "01j8m85889l6viwjbn0brgi0msxf001ph2yh18lp4x6jhn9ighra") (f (quote (("serial" "serde_derive" "serde") ("default" "serial"))))))

(define-public crate-indexed_vec-1.1.0 (c (n "indexed_vec") (v "1.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "07nxd26p83asysks0jndn7kh2nmk39nzp68x981yh5hs7bnzn48x") (f (quote (("serial" "serde_derive" "serde") ("default" "serial")))) (y #t)))

(define-public crate-indexed_vec-1.1.1 (c (n "indexed_vec") (v "1.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "01vy1k9dcc6755chrvk3bm6hkg8aaw7qk2p0bnv583q2p2pgbqfj") (f (quote (("serial" "serde") ("default" "serial"))))))

(define-public crate-indexed_vec-1.1.2 (c (n "indexed_vec") (v "1.1.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0fqagbvyh79gsjhgl3pig17yybmvvq5ywzsyhcid9s5bj08gl5ym") (f (quote (("serial" "serde") ("default" "serial"))))))

(define-public crate-indexed_vec-1.2.0 (c (n "indexed_vec") (v "1.2.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0fqnidqhs1cjjd4nxqb9yzpjd0hmq9l5hzk9n0nwa5dknq75srns") (f (quote (("serial" "serde") ("default" "serial"))))))

(define-public crate-indexed_vec-1.2.1 (c (n "indexed_vec") (v "1.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "020d1hqwjyrsd5y6g4sn9n5khwplp5fd375yzc7a6r0dck8r0lyx") (f (quote (("serial" "serde") ("default" "serial"))))))

