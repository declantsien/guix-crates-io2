(define-module (crates-io in de index-pool) #:use-module (crates-io))

(define-public crate-index-pool-1.0.0 (c (n "index-pool") (v "1.0.0") (h "1iz8ymjq4nkpwx8baxnimqv3lhk70icg5619mpkbbgvp9sra7giz")))

(define-public crate-index-pool-1.0.1 (c (n "index-pool") (v "1.0.1") (h "0q993zk2dv4xrbay2lvqzqmk3csyb7bd4pkd3vkcdrnfkm719igy")))

(define-public crate-index-pool-1.0.2 (c (n "index-pool") (v "1.0.2") (h "1wr975sz1kaa5wvnv97kw34l142bz7iklk83k3nls0rdad0q0djk")))

(define-public crate-index-pool-1.0.3 (c (n "index-pool") (v "1.0.3") (h "0wpdqg4r3hx1q0s8wwcrihwazpry9x33vhn2q46wi4pix0ldg67g")))

(define-public crate-index-pool-1.0.4 (c (n "index-pool") (v "1.0.4") (h "1mw1lk426y64gfxjjm4lpvlyx7ia1ixij1nlgb207vx7z4nmg5km")))

(define-public crate-index-pool-1.0.5 (c (n "index-pool") (v "1.0.5") (d (list (d (n "free-ranges") (r "^0.1.3") (d #t) (k 0)))) (h "0pl69xzpvi8lqa44l1kpqcqsxx8lh9s4ird8k1vhpk4xysb7rsh8")))

(define-public crate-index-pool-1.0.6 (c (n "index-pool") (v "1.0.6") (d (list (d (n "free-ranges") (r "^1.0") (d #t) (k 0)))) (h "1n2rrqsxzpq63r82kxhyyf81l4546nhmphyb1z161h90l90fk00z")))

(define-public crate-index-pool-1.0.7 (c (n "index-pool") (v "1.0.7") (d (list (d (n "free-ranges") (r "^1.0.1") (d #t) (k 0)))) (h "0fh60i1pfrp5wdrrb86a7i5y7q37zkccl53fk7441gcg2yg4limy")))

(define-public crate-index-pool-1.0.8 (c (n "index-pool") (v "1.0.8") (d (list (d (n "free-ranges") (r "^1.0.2") (d #t) (k 0)))) (h "0410m0jn5i8p8x9bv0kjhyv4hbs764c3ir5jak3kyyiwdxdz4s3d") (y #t)))

(define-public crate-index-pool-1.0.9 (c (n "index-pool") (v "1.0.9") (d (list (d (n "free-ranges") (r "^1.0.2") (d #t) (k 0)))) (h "0byg6rm38vgdvj8scbf5yscq8binzd6281f26ag0q6x9kcjclx6x")))

(define-public crate-index-pool-1.0.10 (c (n "index-pool") (v "1.0.10") (d (list (d (n "free-ranges") (r "^1.0.4") (d #t) (k 0)))) (h "004kfsaaa1h0hq90giwlv4ylz4bc22n7png54lrws94q37akvb90")))

(define-public crate-index-pool-1.0.11 (c (n "index-pool") (v "1.0.11") (d (list (d (n "free-ranges") (r "^1.0.6") (d #t) (k 0)))) (h "14r56g57imazil3yp942wphkqzyxjib5hijby4cjjn610p0pb0j8")))

