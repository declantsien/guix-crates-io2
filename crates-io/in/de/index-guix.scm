(define-module (crates-io in de index-guix) #:use-module (crates-io))

(define-public crate-index-guix-1.0.0 (c (n "index-guix") (v "1.0.0") (d (list (d (n "gix") (r ">=0.53, <0.56") (f (quote ("basic" "blocking-network-client" "blocking-http-transport-reqwest-rust-tls"))) (o #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1qrqh0vzqjlynr0ddnrq5sz9l23l7k4s3fadkhlic3x9p782fmdx") (f (quote (("default" "git")))) (s 2) (e (quote (("git" "dep:gix"))))))

(define-public crate-index-guix-1.0.1 (c (n "index-guix") (v "1.0.1") (d (list (d (n "gix") (r ">=0.56, <0.59") (f (quote ("basic" "blocking-network-client" "blocking-http-transport-reqwest-rust-tls"))) (o #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0rbzm87y673pk68fsvcacdlmd8wnihyrx4h5jva98152wqr628rm") (f (quote (("default" "git")))) (s 2) (e (quote (("git" "dep:gix"))))))

(define-public crate-index-guix-1.0.2 (c (n "index-guix") (v "1.0.2") (d (list (d (n "gix") (r ">=0.56, <0.60") (f (quote ("basic" "blocking-network-client" "blocking-http-transport-reqwest-rust-tls"))) (o #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "19jzj3g52qy1akb32h2sxc0k5l86x15ygprnwpg8b6p9wm0rwh8v") (f (quote (("default" "git")))) (s 2) (e (quote (("git" "dep:gix"))))))

