(define-module (crates-io in de indexsort) #:use-module (crates-io))

(define-public crate-indexsort-0.0.1 (c (n "indexsort") (v "0.0.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0l4cz0ymc99fqp720c8v9bl5y89f16pqylx08k81s2z5b5qz0k5w") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-indexsort-0.1.0 (c (n "indexsort") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1c54ymzzlslgn8gqqnp8978sdrlcxha9ijkx3p95sckn16fi0iv5") (f (quote (("default" "alloc") ("alloc"))))))

