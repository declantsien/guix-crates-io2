(define-module (crates-io in de indeed_querybuilder) #:use-module (crates-io))

(define-public crate-indeed_querybuilder-0.1.0 (c (n "indeed_querybuilder") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.8.0") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1vsxb9pf066v39avn2rj987bd87w7wfryzl0f50sg79q15xskki2") (y #t)))

(define-public crate-indeed_querybuilder-0.2.0 (c (n "indeed_querybuilder") (v "0.2.0") (d (list (d (n "derive_builder") (r "^0.8.0") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0f7cdbnm4gfic3aafq31mbp9ykcxb7nzik1ghr4gl8qh44gmygxs") (y #t)))

(define-public crate-indeed_querybuilder-0.2.1 (c (n "indeed_querybuilder") (v "0.2.1") (d (list (d (n "derive_builder") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1w7w9b3piy17awmwaj7fvlbzm2vzafd7nnbp6zlji1wfayy3sr6b") (y #t)))

(define-public crate-indeed_querybuilder-0.2.2 (c (n "indeed_querybuilder") (v "0.2.2") (d (list (d (n "derive_builder") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "03sf26pqrya7982w55ywkkd3lgp38m1ijxrsibhaljffbynz8lbz") (y #t)))

