(define-module (crates-io in de index_datamanip) #:use-module (crates-io))

(define-public crate-index_datamanip-0.5.0 (c (n "index_datamanip") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "handlebars") (r "^3.0") (f (quote ("dir_source"))) (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qd7flxgmqhkblpih3cfwzbmbj71x2cdqdbw0jvpnrbhkl4860bn")))

(define-public crate-index_datamanip-0.5.1 (c (n "index_datamanip") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "handlebars") (r "^3.0") (f (quote ("dir_source"))) (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15yw4mxbdcw00jnlaj74kwg52i25gzb4dm5si8b7ngd4sq8jjiv1")))

(define-public crate-index_datamanip-0.5.2 (c (n "index_datamanip") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wra0nwhbzcb30d5mq7fjmgs675kwmbjg3b46wpm94z8xvd2lw9k")))

