(define-module (crates-io in de indexed-bitfield) #:use-module (crates-io))

(define-public crate-indexed-bitfield-0.1.0 (c (n "indexed-bitfield") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0yr729mdwz64lyz3c47gfbyjm9han1lzrhlb3bjrnibzjmdy6xkh")))

(define-public crate-indexed-bitfield-0.1.1 (c (n "indexed-bitfield") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "03jlfymkmy3zpxqpkc44nppflmjs79l101birq8s5n8xwh5npq42")))

