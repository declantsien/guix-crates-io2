(define-module (crates-io in de indexea) #:use-module (crates-io))

(define-public crate-indexea-1.0.0 (c (n "indexea") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0gvgmzjwl758m0v36q8qn84gjiybfskzvfqg9srqp4vw1vaial9r")))

