(define-module (crates-io in de indexing-str) #:use-module (crates-io))

(define-public crate-indexing-str-0.1.0 (c (n "indexing-str") (v "0.1.0") (d (list (d (n "new_debug_unreachable") (r "^1.0.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.4") (d #t) (k 2)))) (h "1gdmps8ljw9hp9mqnsj5mg8907xnclizy0fxwisvvjb2zypg04yi") (f (quote (("std") ("doc") ("default" "std")))) (y #t)))

