(define-module (crates-io in de index_graph) #:use-module (crates-io))

(define-public crate-index_graph-0.1.0 (c (n "index_graph") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "keyed_vec") (r "^0.1.0") (d #t) (k 0)))) (h "14b0dnzh0axhp5ghdaj5hw0azxc3vw595h2617h44150ws365d22")))

(define-public crate-index_graph-0.2.0 (c (n "index_graph") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.72") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "keyed_vec") (r "^0.1.0") (d #t) (k 0)))) (h "05i7nncqpfhap544bawqmpn4jn5wvvsq55zpcsps6psrsdajcp6c")))

