(define-module (crates-io in de indexor) #:use-module (crates-io))

(define-public crate-indexor-0.1.0 (c (n "indexor") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)))) (h "0a5qfdkdh3fwq8p5pvxshn34myzc2vv92k1znj81zqjqbjsjrcfm") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-indexor-0.2.0 (c (n "indexor") (v "0.2.0") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)))) (h "18c02yn1ii51mjkc6zdy3hcxadrfql68c74xgbvw551sak9ypi49") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-indexor-0.2.1 (c (n "indexor") (v "0.2.1") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)))) (h "1vkywaw72a8hd94l4h3cnfbq7f221lwvnf6mfys3m18303yl73n8") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-indexor-0.3.0 (c (n "indexor") (v "0.3.0") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)))) (h "0sxkadcxq84ff0qlrx40l5pnx7h9hcwpjk5bsmj21gi4wzgp7yw9") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-indexor-0.4.0 (c (n "indexor") (v "0.4.0") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)))) (h "0ps8nbjmdzg62xj68wvsfps42jc3c9dy67sfjrghx3fwc4ak7yxw") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-indexor-0.5.0 (c (n "indexor") (v "0.5.0") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)))) (h "10z9nxvbshzl3vfgpw0pq8scg9lwc3vfxfzi955058frfl3h7570") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

