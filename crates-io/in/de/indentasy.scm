(define-module (crates-io in de indentasy) #:use-module (crates-io))

(define-public crate-indentasy-1.0.0 (c (n "indentasy") (v "1.0.0") (d (list (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "17886j5202mwicgapgdc47y84663x25jsv7kcfswqi3lz0yqdkx8") (y #t)))

(define-public crate-indentasy-0.0.2 (c (n "indentasy") (v "0.0.2") (d (list (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1gmavf6ljpxm1gfqxcg4c4iqj7a9lsq162lrqgg1x7qj453mxqz0")))

(define-public crate-indentasy-0.0.3 (c (n "indentasy") (v "0.0.3") (d (list (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1s6ciy95grbm2m2sr0k691ggsvxvz5chqdb567rzp8kplr1d7ifv")))

(define-public crate-indentasy-0.1.0 (c (n "indentasy") (v "0.1.0") (d (list (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1jikr5jlr4k95gz3jnxrcljsk3rmqg7g1wh64ar2yvc49dbrm0xp")))

(define-public crate-indentasy-0.2.0 (c (n "indentasy") (v "0.2.0") (h "07d0ia2wvd5ql6bln0a60apkvysmz3rsiim3dw3kxd0sfbc91353")))

(define-public crate-indentasy-0.3.0 (c (n "indentasy") (v "0.3.0") (h "0hpf9bxa6pczymr1nl6wgvn6cqy9vkh340adcbm5fnpdh94xhb55")))

