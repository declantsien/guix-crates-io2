(define-module (crates-io in de indexed-line-reader) #:use-module (crates-io))

(define-public crate-indexed-line-reader-0.1.0 (c (n "indexed-line-reader") (v "0.1.0") (h "15v4p3b4h14qip800a8rad81fa386f4nj4sk67fzd1rsf0g53li5")))

(define-public crate-indexed-line-reader-0.2.0 (c (n "indexed-line-reader") (v "0.2.0") (h "1q3z9l5hg3l2cm6q01cxy1dcsm87g2xd9idg32xin6w9w44x4amh")))

(define-public crate-indexed-line-reader-0.2.1 (c (n "indexed-line-reader") (v "0.2.1") (h "1mws7llj5gb9b60n9d9jnmrcinrjkh2r669pxbdmm8j2zxsm3arl")))

