(define-module (crates-io in de indent-s-expr) #:use-module (crates-io))

(define-public crate-indent-s-expr-1.0.0 (c (n "indent-s-expr") (v "1.0.0") (d (list (d (n "insta") (r "^1.35.1") (d #t) (k 2)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "rstest_reuse") (r "^0.6.0") (d #t) (k 2)))) (h "0smq8q6n3rkz6qghyw4cc7hsp04vmr5ii2y6zy64wc8k436a2ipk") (y #t)))

(define-public crate-indent-s-expr-1.0.1 (c (n "indent-s-expr") (v "1.0.1") (d (list (d (n "insta") (r "^1.35.1") (d #t) (k 2)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "rstest_reuse") (r "^0.6.0") (d #t) (k 2)))) (h "0795an3636lqycfkfcv9sh5g9iy4jb461ixyzwy1q0z154k830y5") (y #t)))

