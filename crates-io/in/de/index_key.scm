(define-module (crates-io in de index_key) #:use-module (crates-io))

(define-public crate-index_key-0.1.0 (c (n "index_key") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)))) (h "11ydl75cwrssd6adf1zq86m52f1bad2yc49502v8v06ld2fydy87")))

(define-public crate-index_key-0.1.1 (c (n "index_key") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)))) (h "1rba73z0mzf028zaj8z0s03riqa72kmk1y9v66gfdg67qi60gdg5")))

(define-public crate-index_key-0.2.0 (c (n "index_key") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "04d14f52wzp415wdmcq6n6p9blq1iisgw9djijbhs5mz08d2kn90")))

(define-public crate-index_key-0.2.1 (c (n "index_key") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "1a9n8ljncfzv0zfjji2agq6fr2c2i48qd6ykzj2svkni13fh7c5r")))

(define-public crate-index_key-0.2.2 (c (n "index_key") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "0x6zzi97428r942iiz4gqhhxygmd43dd9pcgxpzdp1243c5k01r0")))

(define-public crate-index_key-0.3.0 (c (n "index_key") (v "0.3.0") (h "06n33a7axl1pkh3kp358mfv8sns095xs84nv8r0wjg441xay9v4j")))

(define-public crate-index_key-0.4.0 (c (n "index_key") (v "0.4.0") (h "08bdfnqxmpn73yrkiwg4zxwzqzrq9zfa4c0zpmi05g5f2lmxc6f7")))

(define-public crate-index_key-0.4.1 (c (n "index_key") (v "0.4.1") (h "158ij1f68zvravpgfxpfddxmjlf4dbkjaifwmnam4g9jmisqrx27")))

(define-public crate-index_key-0.5.0 (c (n "index_key") (v "0.5.0") (h "1738cf2wsbsf3zx3zrxbiwk01axijlhz1lvdgpzss2k4qbn26846")))

