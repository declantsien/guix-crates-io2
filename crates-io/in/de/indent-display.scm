(define-module (crates-io in de indent-display) #:use-module (crates-io))

(define-public crate-indent-display-0.1.0 (c (n "indent-display") (v "0.1.0") (h "0wdsv370rgkwq88slmybn324gc2adgsni96f67sxb6dwq2d5zvny")))

(define-public crate-indent-display-0.1.1 (c (n "indent-display") (v "0.1.1") (h "05dhvbyrwkd3aj8xzxcpnlhrib32zi2vxfi7zzkr0b1dlg0rxfzg")))

