(define-module (crates-io in de indeed_scraper) #:use-module (crates-io))

(define-public crate-indeed_scraper-0.1.0 (c (n "indeed_scraper") (v "0.1.0") (d (list (d (n "indeed_querybuilder") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1y6vlwqx7cf50hq60cmhq5sfyvy42cl4vgxaqrn17s3wydvlwhki")))

