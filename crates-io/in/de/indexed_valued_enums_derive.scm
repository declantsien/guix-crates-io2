(define-module (crates-io in de indexed_valued_enums_derive) #:use-module (crates-io))

(define-public crate-indexed_valued_enums_derive-1.0.0 (c (n "indexed_valued_enums_derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0hqfzcmm7yn8ir34irk3j3zvfnsmfz0vm3cgqsaqhr62hrzkphxi")))

