(define-module (crates-io in de indexvec) #:use-module (crates-io))

(define-public crate-indexvec-0.1.0 (c (n "indexvec") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "13dj52arbhk6azwh2dcf1w1l9xkqvh4gs20gh0xacv5ybvxx3ypi") (y #t)))

(define-public crate-indexvec-0.1.1 (c (n "indexvec") (v "0.1.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "14fcz22lmy40b6ws0a6k5xxnc1vfrlb0yqd6fxjlmlcwpp4z56z9") (y #t)))

(define-public crate-indexvec-0.1.2 (c (n "indexvec") (v "0.1.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0sh965q1cpq9k8xvc6s717n0dxzy0132d71qkk3qv71frdp7hm9j") (y #t)))

(define-public crate-indexvec-0.1.3 (c (n "indexvec") (v "0.1.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "10m2szbbkhra1yp02xr08i7k1rp4q2036n41akypab1h8mjwg058") (y #t)))

(define-public crate-indexvec-0.2.0 (c (n "indexvec") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "08w89irs8qr9l8nb0ab6lfa9icdx82r6lhapxvwf86j3l125f62m") (y #t)))

(define-public crate-indexvec-0.3.0 (c (n "indexvec") (v "0.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0bhwm4hw8icjjngvx71ry9hx0zimq80ks3sbincf6wp1dfd0yb5f") (y #t)))

(define-public crate-indexvec-0.3.1 (c (n "indexvec") (v "0.3.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1cr4hnfndf63hlrwmcbvq5m56dwmr4zqr2yqhdin3rmcsi7m9rhr") (y #t)))

(define-public crate-indexvec-0.3.2 (c (n "indexvec") (v "0.3.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "18i0qzbvdrh0h596v4m0zjf1ny717jfs3zjjn4qh25mn407lfzma") (y #t)))

(define-public crate-indexvec-0.4.0 (c (n "indexvec") (v "0.4.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1qmqs3crgfqsw030wi5r9gr7xi5qsbfckfvsfzwcf0qgb8kjirp7") (y #t)))

(define-public crate-indexvec-0.5.0 (c (n "indexvec") (v "0.5.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "170smnxndh8xpzvjcipk77jvwc1c3kb9d413m0ax00a28ay1pcxm")))

(define-public crate-indexvec-0.5.1 (c (n "indexvec") (v "0.5.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1jgw5wyzm23ngv6x88wpqh3r8d4mx8ki6m1c56gg8w44fjcr7an1")))

(define-public crate-indexvec-0.5.2 (c (n "indexvec") (v "0.5.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0i70d0sx0pvd5pgp7cj1b9rfp8hvzmv8b7c4c7i2xslj7vrnyvy9")))

(define-public crate-indexvec-0.5.3 (c (n "indexvec") (v "0.5.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1dzmxnqj8svqf2y41nbpc6m7i18jxk2qfp59f43zj7jydv82w9ap")))

(define-public crate-indexvec-0.5.4 (c (n "indexvec") (v "0.5.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "196gafm83gam5iam70wrjijbd8fnx0aargvsyr13124k4k937n7m")))

