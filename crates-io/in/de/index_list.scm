(define-module (crates-io in de index_list) #:use-module (crates-io))

(define-public crate-index_list-0.1.0 (c (n "index_list") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "153br5z8x58hqfb8xlbwnigskpvdssa2a9h8rhn5b5v86mxdl4gv")))

(define-public crate-index_list-0.2.0 (c (n "index_list") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0xkaw1j8ffn8z1pyxziny7lpwz32vvxbvyxv4sl6m3pcn6k6b7qs")))

(define-public crate-index_list-0.2.1 (c (n "index_list") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1gysy2n9rmi669y0r0rsi5kr01gr348316n9gqb7krf5fjrz5ai8")))

(define-public crate-index_list-0.2.2 (c (n "index_list") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "17l5ih1g5lya39j33irnzcyhi3yamqp51yf2fsi86wj4gvdcsyj1")))

(define-public crate-index_list-0.2.3 (c (n "index_list") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1fc8i5gq3lb3zxwf7sh5ldlk71zmw66ayrzc7lxxrcrlczwns9l9")))

(define-public crate-index_list-0.2.4 (c (n "index_list") (v "0.2.4") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0v82z1yzx3qm9hrial5pxw0l0sqs4rm7cdfy9gc3sjla60sfrkha")))

(define-public crate-index_list-0.2.5 (c (n "index_list") (v "0.2.5") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0ng241s7ldc54wl4a9zyyp23jpd66qzapmzgr5jvnzr26j8pf2a4")))

(define-public crate-index_list-0.2.6 (c (n "index_list") (v "0.2.6") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1lr9274zb6v74yqim89kk6a4pm8b2bjlz7n8rchasmn7zx408d6d")))

(define-public crate-index_list-0.2.7 (c (n "index_list") (v "0.2.7") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1kl2rc3clmgpb8q444rhh1gbf7l5smygqih9h442x4548a09d7as")))

(define-public crate-index_list-0.2.10 (c (n "index_list") (v "0.2.10") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0z2y06his4hsmk68jxkq2fqh78n9q0cm7mvvfj79wbdp6fggqs3z")))

(define-public crate-index_list-0.2.11 (c (n "index_list") (v "0.2.11") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0f268iqlg9j0c9db9i7cnlh9x7v9d5sv8y0irzglz14frf3152bh")))

