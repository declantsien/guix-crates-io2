(define-module (crates-io in de index-fixed) #:use-module (crates-io))

(define-public crate-index-fixed-0.0.0 (c (n "index-fixed") (v "0.0.0") (h "00s2g5ymi8gj36j48ff51w887nla5v3sf68xhjkqyy5r78c5j2zw")))

(define-public crate-index-fixed-0.0.1 (c (n "index-fixed") (v "0.0.1") (h "19b61gww3rqk08c5ac4kn2graz0cxd82il8ip3hx6s3bhm6w1nxm")))

(define-public crate-index-fixed-0.0.2 (c (n "index-fixed") (v "0.0.2") (h "0p2lyfi3q9whghnrs02w0wdxk3aaimz0rag3las9a0m744rlkxvd")))

(define-public crate-index-fixed-0.1.0 (c (n "index-fixed") (v "0.1.0") (h "18dsfzxhaxg7mhiw6al4x29g0rwwkbps9sr95nnbdx536skx7p4h")))

(define-public crate-index-fixed-0.2.0 (c (n "index-fixed") (v "0.2.0") (h "084hj9jg2x4ikv9h569v189li1c9mil48cygajvr1lw256m4ppki")))

(define-public crate-index-fixed-0.3.0 (c (n "index-fixed") (v "0.3.0") (h "1ymvckvif1vqd6g04bghr1rlw17mf98sb25h0gdjvs6aa17xyfn6")))

(define-public crate-index-fixed-0.3.1 (c (n "index-fixed") (v "0.3.1") (h "1blgg66nkmg1zk90xiz0a4x3km2abl4dmx82clzwvdj15ypwwqa1")))

