(define-module (crates-io in de indent_write) #:use-module (crates-io))

(define-public crate-indent_write-1.0.0 (c (n "indent_write") (v "1.0.0") (h "1fhfw99q4kfhvmczn5mfiqqnbg6r2xa6qhp15nbd3j7kcn7cdwcz")))

(define-public crate-indent_write-2.0.0 (c (n "indent_write") (v "2.0.0") (h "1izzjrm3prf0lcfm9znjrhs0arvfgq16j69ksawrg8hrb94nlh40")))

(define-public crate-indent_write-2.1.0 (c (n "indent_write") (v "2.1.0") (h "03p7w6z14xvwlb22jxq6mpfy94fwyahcgdda2yc9q9lavrwxy17r")))

(define-public crate-indent_write-2.2.0 (c (n "indent_write") (v "2.2.0") (h "1hqjp80argdskrhd66g9sh542yxy8qi77j6rc69qd0l7l52rdzhc") (f (quote (("std") ("default" "std"))))))

