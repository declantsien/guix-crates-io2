(define-module (crates-io in de indexed_data_file) #:use-module (crates-io))

(define-public crate-indexed_data_file-0.1.0 (c (n "indexed_data_file") (v "0.1.0") (d (list (d (n "file_mmap") (r "^0.1") (d #t) (k 0)) (d (n "tri_avltree") (r "^0.1") (d #t) (k 0)))) (h "095myyw7m782ij7wwyj1bq966if3ghh2lclb3adwlbc4wf1k8s5l") (y #t)))

(define-public crate-indexed_data_file-0.2.0 (c (n "indexed_data_file") (v "0.2.0") (d (list (d (n "file_mmap") (r "^0.1") (d #t) (k 0)) (d (n "tri_avltree") (r "^0.2") (d #t) (k 0)))) (h "1z90jbh7w99bilv02mgzflj1b0sx45rpzx1ygvz9nkzlxk6nwsgk") (y #t)))

(define-public crate-indexed_data_file-0.3.0 (c (n "indexed_data_file") (v "0.3.0") (d (list (d (n "file_mmap") (r "^0.1") (d #t) (k 0)) (d (n "tri_avltree") (r "^0.3") (d #t) (k 0)))) (h "0zlir7vpc55xiirkfxpgd82981vvx4cc32w3jcjgfyk9cxsazsw1") (y #t)))

(define-public crate-indexed_data_file-0.4.0 (c (n "indexed_data_file") (v "0.4.0") (d (list (d (n "file_mmap") (r "^0.1") (d #t) (k 0)) (d (n "tri_avltree") (r "^0.4") (d #t) (k 0)))) (h "14nar9g9bx87yw5frgarv6jq1x5cji050v2nv3g9bxad7yh4v759") (y #t)))

(define-public crate-indexed_data_file-0.5.0 (c (n "indexed_data_file") (v "0.5.0") (d (list (d (n "avltriee") (r "^0.5") (d #t) (k 0)) (d (n "file_mmap") (r "^0.1") (d #t) (k 0)))) (h "1znwh6x1256p7ph4n3j62mlc5j9bhvvgl95gif350b5bkayha20y") (y #t)))

(define-public crate-indexed_data_file-0.6.0 (c (n "indexed_data_file") (v "0.6.0") (d (list (d (n "avltriee") (r "^0.6") (d #t) (k 0)) (d (n "file_mmap") (r "^0.1") (d #t) (k 0)))) (h "0bbcrms9i4zb1a0692pck41p7v6xfrdqv48481h8s0ffrj39i19j") (y #t)))

(define-public crate-indexed_data_file-0.6.1 (c (n "indexed_data_file") (v "0.6.1") (d (list (d (n "avltriee") (r "^0.6") (d #t) (k 0)) (d (n "file_mmap") (r "^0.1") (d #t) (k 0)))) (h "0yzk90d5ckim1n4jj49jvna832q44j7hnwdwlpm02bii4wikjxal") (y #t)))

(define-public crate-indexed_data_file-0.6.2 (c (n "indexed_data_file") (v "0.6.2") (d (list (d (n "avltriee") (r "^0.6") (d #t) (k 0)) (d (n "file_mmap") (r "^0.1") (d #t) (k 0)))) (h "010a5iwsagfvprd2wx4mrzvfyv00k3amlwnxgf768l0fps0xc626") (y #t)))

(define-public crate-indexed_data_file-0.6.3 (c (n "indexed_data_file") (v "0.6.3") (d (list (d (n "avltriee") (r "^0.6") (d #t) (k 0)) (d (n "file_mmap") (r "^0.1") (d #t) (k 0)))) (h "1dyx1sznf01v5fbyin5rxhldgy9bq5i4wz0w698lfvkxgbap3xaq") (y #t)))

(define-public crate-indexed_data_file-0.6.4 (c (n "indexed_data_file") (v "0.6.4") (d (list (d (n "avltriee") (r "^0.6") (d #t) (k 0)) (d (n "file_mmap") (r "^0.1") (d #t) (k 0)))) (h "0xp3xkfybp53slb6cqzry0zj5kyam1aa50h98w1fvpmd2xym2c8k") (y #t)))

(define-public crate-indexed_data_file-0.6.5 (c (n "indexed_data_file") (v "0.6.5") (d (list (d (n "avltriee") (r "^0.6") (d #t) (k 0)) (d (n "file_mmap") (r "^0.1") (d #t) (k 0)))) (h "0si13j3l9m3470v915njf20r5y88mmq7sjhzmgny40nbx9n37mwg") (y #t)))

(define-public crate-indexed_data_file-0.6.6 (c (n "indexed_data_file") (v "0.6.6") (d (list (d (n "avltriee") (r "^0.6") (d #t) (k 0)) (d (n "file_mmap") (r "^0.1") (d #t) (k 0)))) (h "070q2fdb2h0mgaqjs52xhywrwv873q26gkqpdr2p4hbhzk3rp12r") (y #t)))

(define-public crate-indexed_data_file-0.6.7 (c (n "indexed_data_file") (v "0.6.7") (d (list (d (n "avltriee") (r "^0.6") (d #t) (k 0)) (d (n "file_mmap") (r "^0.2") (d #t) (k 0)))) (h "071h80f1112cwss5zzc8gilv53z2dvhmjay5ih84ck5nwg41v8hh") (y #t)))

