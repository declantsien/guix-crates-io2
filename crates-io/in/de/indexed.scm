(define-module (crates-io in de indexed) #:use-module (crates-io))

(define-public crate-indexed-0.1.0 (c (n "indexed") (v "0.1.0") (h "0wkz1m8qanj4wc10cxxlwpi0vz295hl162hwi5yp6gx06yh1mi3m") (f (quote (("no_std"))))))

(define-public crate-indexed-0.1.1 (c (n "indexed") (v "0.1.1") (h "0jvrff7wqwhjcxvy5b8yds83mjkgkphrmdnsb5p6l39lrxd1506l") (f (quote (("no_std"))))))

(define-public crate-indexed-0.2.0 (c (n "indexed") (v "0.2.0") (h "1ri76ilx64ygh4ixbzfx3awnarikcx8klgdja690mrwyp5vwmnb4") (f (quote (("no_std"))))))

