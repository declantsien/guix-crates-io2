(define-module (crates-io in de index-html-server) #:use-module (crates-io))

(define-public crate-index-html-server-0.1.0 (c (n "index-html-server") (v "0.1.0") (d (list (d (n "actix-files") (r "^0.1.3") (d #t) (k 0)) (d (n "actix-web") (r "^1.0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "014svd0s8dhz87c1fbbqjga698gsy84n5vf7zma180jpj00d4dqx")))

