(define-module (crates-io in de indent) #:use-module (crates-io))

(define-public crate-indent-0.1.0 (c (n "indent") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)))) (h "0gvgf1m7g2x5japw75vmiy2wv2lrqbl7kk8g0j92m8q5aqplqh0a")))

(define-public crate-indent-0.1.1 (c (n "indent") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)))) (h "19hg1mbmjpbcr7ixfdhn2dcq8kqzkwqyzy7x0kr70acpgmvs1wfr")))

