(define-module (crates-io in de indexed_map) #:use-module (crates-io))

(define-public crate-indexed_map-0.1.0 (c (n "indexed_map") (v "0.1.0") (h "0hx0ax5h5nha9apymy1p6gcxmhi7yvjn0617v20wq3mhc7cgjwsg")))

(define-public crate-indexed_map-0.1.1 (c (n "indexed_map") (v "0.1.1") (h "0wda6c2yz2qmd8mlyb3x63zw53f5wbj84v5iwflsdhfqapkcc2s0")))

