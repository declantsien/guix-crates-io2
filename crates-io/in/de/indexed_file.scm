(define-module (crates-io in de indexed_file) #:use-module (crates-io))

(define-public crate-indexed_file-0.1.0 (c (n "indexed_file") (v "0.1.0") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)))) (h "0lrp7l8jc9xh8zz6rpmn7nhyx9yp09q6sbrcb5n57ib7cpylzg21")))

(define-public crate-indexed_file-0.1.1 (c (n "indexed_file") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "02p55ra7qxij3qpjapbywp5cgq5fngc2jhmk2srl9hakixh1dah8")))

(define-public crate-indexed_file-0.1.2 (c (n "indexed_file") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0frmfwwaj8jzf66r0dxqa7wym7drq33dj0igzklss7d5pix3sw5q")))

