(define-module (crates-io in de indexed-pool) #:use-module (crates-io))

(define-public crate-indexed-pool-0.1.0 (c (n "indexed-pool") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (f (quote ("deadlock_detection"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "1ma4zhgcyv1v0cfk2a7w11yrd7i2z7nl0i92kmp1c0xh8751h09h") (y #t)))

(define-public crate-indexed-pool-0.2.0 (c (n "indexed-pool") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (f (quote ("deadlock_detection"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)))) (h "00fqnyz41h5syd9w6n65py6yjgprn1ga4wf8dw303lcf0sx1rznj")))

