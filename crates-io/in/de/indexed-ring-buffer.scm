(define-module (crates-io in de indexed-ring-buffer) #:use-module (crates-io))

(define-public crate-indexed-ring-buffer-0.1.0 (c (n "indexed-ring-buffer") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "0bb75kzcbq19090bxydbnc4ni4kfvp1w31bd7lpbg8zw46ziagas")))

(define-public crate-indexed-ring-buffer-0.1.1 (c (n "indexed-ring-buffer") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "16rnali6ri099ziidpn4xzdw2ygz04pxgfk4ank5r841sj6szilf")))

(define-public crate-indexed-ring-buffer-0.1.2 (c (n "indexed-ring-buffer") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "1la8zf5ql88hrsm9vgg0wan86ndycwh1rrqmkbc44frhpxh5s6sl")))

(define-public crate-indexed-ring-buffer-0.1.3 (c (n "indexed-ring-buffer") (v "0.1.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "068k9mwc1vbz322kaxhsvwrzgfdwmmfppawq3mm8vy39pf3gkzf5")))

