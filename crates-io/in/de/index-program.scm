(define-module (crates-io in de index-program) #:use-module (crates-io))

(define-public crate-index-program-0.1.0 (c (n "index-program") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)))) (h "15n1263vx7m68sswm2adrcnw070jg5wwv06h3srrcai2r2ikfh9z") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-index-program-0.1.1 (c (n "index-program") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)))) (h "14jszgrac4v80ns9njm02cxv2a1jf64in814s2phanblrc9clzsz") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-index-program-0.1.2 (c (n "index-program") (v "0.1.2") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)))) (h "0dbfis8ycrivq62c33hc5m6bw8hdd92dvnm0jmgb0p9wk4v4x66d") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-index-program-0.1.4 (c (n "index-program") (v "0.1.4") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)))) (h "0g1l0mv96pxr6wqz45f071fcvfwcq7vbzyp2p9l9sk5a8l6m2pzd") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-index-program-0.1.6 (c (n "index-program") (v "0.1.6") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)))) (h "0z5184p4gcsrymwl6b661sz1wcx3a04f0xd5jba6p3l5h9gm1n8s") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-index-program-0.1.7 (c (n "index-program") (v "0.1.7") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)))) (h "09a3mzpqf5m3c1y3b3mnzljwbzrf6ns47wjbrxgz5a7jjq5vp8jx") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-index-program-0.1.8 (c (n "index-program") (v "0.1.8") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)))) (h "1zk68w5lymgzp4zxmrjpln6d8l1ihjsfk7nq1lj2iv21c5pi0wjm") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-index-program-0.1.9 (c (n "index-program") (v "0.1.9") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)))) (h "02a0va0r1i8kvqkcvgyy58n32v3nj3gcpfr7bksy7il6jig56l4g") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-index-program-0.1.10 (c (n "index-program") (v "0.1.10") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)))) (h "0zamqzaxllq1ymy7yqi7lnsk6jb2ycd4hx5bl2fq1aw70dci0l11") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-index-program-0.1.11 (c (n "index-program") (v "0.1.11") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)))) (h "1wbdq3lgzfgafn2p92c7m8vvy6mhsrlsm8c320pbwrvd98xzj46m") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-index-program-0.1.12 (c (n "index-program") (v "0.1.12") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)))) (h "1h99c7avzd3iizkyhqjp00h3lp4b05zfwf4jvfw3b1zcrg0gjjly") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

