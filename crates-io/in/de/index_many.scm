(define-module (crates-io in de index_many) #:use-module (crates-io))

(define-public crate-index_many-0.1.0 (c (n "index_many") (v "0.1.0") (h "1vjnid4rrwyqvk19qda7c66bzxpkc1zbm7sv1psg8bswdw6rkpgf") (y #t)))

(define-public crate-index_many-0.1.1 (c (n "index_many") (v "0.1.1") (h "08bhvll6l7rfwpiq0ch163v53kj8gahs4c1hqa2nlxx5fidaiwcj") (y #t)))

(define-public crate-index_many-0.1.2 (c (n "index_many") (v "0.1.2") (h "0b7f9z0kbj2dmmqdw5awy1njqfz8ka2w25xvxd0v5asyryq5rai0")))

(define-public crate-index_many-0.2.0 (c (n "index_many") (v "0.2.0") (h "1m074lj2dhwx0w00snf0jyjvzcg3z7mfq3cx4d4f4809ij3g2p3w")))

(define-public crate-index_many-0.2.1 (c (n "index_many") (v "0.2.1") (h "0bksmvkjnb4jpn6hv6lf9f995xl46fr89ci6n67mqgisdxw6sdls")))

(define-public crate-index_many-0.2.2 (c (n "index_many") (v "0.2.2") (h "0lbg4k5b3f56j5nj2733skz7in87k1icbg7rirpzayzbvbm21kvb")))

(define-public crate-index_many-0.3.0 (c (n "index_many") (v "0.3.0") (h "0apdxgbddrj6s1bqdqqp6vy4kdp6kxvpnp34014y97b4dg93d0vr")))

(define-public crate-index_many-0.4.0 (c (n "index_many") (v "0.4.0") (h "0vsj9w2jif9qbdlm99q9pqvw35xsy0d5gqlbj0pkzxf281g850w4")))

(define-public crate-index_many-0.4.1 (c (n "index_many") (v "0.4.1") (h "0ngqwkz451f30pax1a49qxxdahbbwdywivrfyklr1fmn5i4csz0j")))

(define-public crate-index_many-0.5.0 (c (n "index_many") (v "0.5.0") (h "0hp9qmi5b4drdnqj8shvzgajgarcx6555mzfc7sf2nd7i62v4nsf")))

(define-public crate-index_many-0.5.1 (c (n "index_many") (v "0.5.1") (h "17vnm7fwbfxvankb4wykvvz4cmfc9malrczq18jgwm8cibfcvqpx")))

(define-public crate-index_many-0.5.2 (c (n "index_many") (v "0.5.2") (h "1mjq8vwy4z7p978pg5nkpgr677nqr2f0bxpgnl2gf7xfvh2zdbrq")))

(define-public crate-index_many-0.6.0 (c (n "index_many") (v "0.6.0") (h "1890kwgc6m0899dwr6c6r6r9js0nc8r971m69aphp9vlgb05lpd6")))

(define-public crate-index_many-0.6.1 (c (n "index_many") (v "0.6.1") (h "1h08hjzg3hjisf9xszl1w53mw3afq97rbyb3z3z6jmdf1kh4wlqk")))

