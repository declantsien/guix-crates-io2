(define-module (crates-io in de indextree) #:use-module (crates-io))

(define-public crate-indextree-0.1.0 (c (n "indextree") (v "0.1.0") (h "0vrkr8j1gyi9la08l6vjq8svgarkrmbaq1lzfs4bb7hkql2kyyb6")))

(define-public crate-indextree-0.1.1 (c (n "indextree") (v "0.1.1") (h "1rgjdzn5swy8799yf50c9xq48b55zzl3w4djpx98dplvflp6sidh")))

(define-public crate-indextree-1.0.0 (c (n "indextree") (v "1.0.0") (h "0r32da83db5051v4hhfhv3jly3yfw98d7xnx341vq6wkbsnhsg74")))

(define-public crate-indextree-1.0.1 (c (n "indextree") (v "1.0.1") (h "07jff06i7vna3nz5xpzvgjjabz6lica3rp2pfkwzy25zlvf8y2ig")))

(define-public crate-indextree-1.1.0 (c (n "indextree") (v "1.1.0") (h "1g8k5qsqrqhdcg4lpns1d8hv03dn6nz2irrnad5fn95nb2x9nyia")))

(define-public crate-indextree-1.1.1 (c (n "indextree") (v "1.1.1") (h "1h56ma855976snkm00h499k38jwj5nakvs868ix6jg7s3nm92j3z")))

(define-public crate-indextree-1.2.0 (c (n "indextree") (v "1.2.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "0zq58c7k42ip3h9hapkrq51sy6s6l2869qj0q25jbihpn2xpqalx") (f (quote (("deser" "serde" "serde_derive"))))))

(define-public crate-indextree-1.3.0 (c (n "indextree") (v "1.3.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "103b3r7smrdg4nx74w0zxddac6kwqc86q39b25qg95b6hpxvfwfb") (f (quote (("deser" "serde" "serde_derive"))))))

(define-public crate-indextree-2.0.0 (c (n "indextree") (v "2.0.0") (d (list (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "06lq78xlfl8a0hh7376sxllffzqmkrhwlcs5sc29gik8w59i6fgd") (f (quote (("deser" "serde" "serde_derive"))))))

(define-public crate-indextree-2.1.0 (c (n "indextree") (v "2.1.0") (d (list (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "1klf1hh98xdiw706y0jpwqxqlfw8av89kibldwpnm5l5z1lj9agz") (f (quote (("par_iter" "rayon") ("deser" "serde" "serde_derive"))))))

(define-public crate-indextree-3.0.0 (c (n "indextree") (v "3.0.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (o #t) (d #t) (k 0)))) (h "185cha05hh5dlq4b8nm8rmk4x1pgqapy2b5gy4i0p96jnsja33pp") (f (quote (("par_iter" "rayon") ("deser" "serde" "serde_derive"))))))

(define-public crate-indextree-3.1.0 (c (n "indextree") (v "3.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (o #t) (d #t) (k 0)))) (h "0x0xr63x2ivsddlkshgy88sia0bgnr80ck9s10wijljky8v4ilrk") (f (quote (("par_iter" "rayon") ("deser" "serde" "serde_derive"))))))

(define-public crate-indextree-3.1.1 (c (n "indextree") (v "3.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (o #t) (d #t) (k 0)))) (h "04hdacnbifqvrfhcjbjscsz9aig2lw5mmrzxiv1ddqzdr0j5fj31") (f (quote (("par_iter" "rayon") ("deser" "serde" "serde_derive"))))))

(define-public crate-indextree-3.2.0 (c (n "indextree") (v "3.2.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (o #t) (d #t) (k 0)))) (h "0bbvxqvi12djlr56hwn59cl8ib5a23jyq99bb35gh54p9rfbv7h6") (f (quote (("par_iter" "rayon") ("deser" "serde" "serde_derive") ("derive-eq"))))))

(define-public crate-indextree-3.3.0 (c (n "indextree") (v "3.3.0") (d (list (d (n "failure") (r "^0.1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (o #t) (d #t) (k 0)))) (h "11c2hqwjd4h9sini7nxq15pngd6wkvhkbd9wqjdalqjrxpycl9m6") (f (quote (("std") ("par_iter" "rayon") ("deser" "serde" "serde_derive") ("derive-eq") ("default" "std"))))))

(define-public crate-indextree-4.0.0 (c (n "indextree") (v "4.0.0") (d (list (d (n "rayon") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0s5058d166gcahjcbz09pfhzph6l8nrjzx38j0p9qdsr534g11nz") (f (quote (("std") ("par_iter" "rayon") ("deser" "serde") ("default" "std"))))))

(define-public crate-indextree-4.1.0 (c (n "indextree") (v "4.1.0") (d (list (d (n "rayon") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ymj1szlcr78lqrn5f78gnddgcl6i4gcvxclxj4d4y6hxvl1hnv4") (f (quote (("std") ("par_iter" "rayon") ("deser" "serde") ("default" "std"))))))

(define-public crate-indextree-4.2.0 (c (n "indextree") (v "4.2.0") (d (list (d (n "rayon") (r "^1.3.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1g5gxpq1n1a9dm9nk0b5l0828k2ilfngxfm1n10zz8xzm8vhvmfr") (f (quote (("std") ("par_iter" "rayon") ("deser" "serde") ("default" "std"))))))

(define-public crate-indextree-4.3.0 (c (n "indextree") (v "4.3.0") (d (list (d (n "rayon") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "15x426dx4p0l8662qf40lxfg630qqfvsk19fsfakxkqgxq1sjw95") (f (quote (("std") ("par_iter" "rayon") ("deser" "serde") ("default" "std"))))))

(define-public crate-indextree-4.3.1 (c (n "indextree") (v "4.3.1") (d (list (d (n "rayon") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1zlziz3b1l6vbqrbplpn0s3f07ld5g53x0ayyffvkjb8sb1q02cr") (f (quote (("std") ("par_iter" "rayon") ("deser" "serde") ("default" "std"))))))

(define-public crate-indextree-4.4.0 (c (n "indextree") (v "4.4.0") (d (list (d (n "rayon") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0rj0g4k5dg6fmwvb1xicamq8mx706qxv1xj4rpjxisqi6dmv9d22") (f (quote (("std") ("par_iter" "rayon") ("deser" "serde") ("default" "std"))))))

(define-public crate-indextree-4.5.0 (c (n "indextree") (v "4.5.0") (d (list (d (n "rayon") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0fsjl1h72axinqx73x2nzff4xpg509wafj2648s5rrzsq9m06zs9") (f (quote (("std") ("par_iter" "rayon") ("deser" "serde") ("default" "std"))))))

(define-public crate-indextree-4.6.0 (c (n "indextree") (v "4.6.0") (d (list (d (n "rayon") (r "^1.7.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1dnysxws5kpw8c3yvnhq3r9p2kfqqrgcw29x7hrg2gn6wp812164") (f (quote (("std") ("par_iter" "rayon") ("deser" "serde") ("default" "std"))))))

(define-public crate-indextree-4.6.1 (c (n "indextree") (v "4.6.1") (d (list (d (n "rayon") (r "^1.7.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "047yin6f8zj6ya3z59ica9grsr1hkyn23c0iyj9c97k1q4lpwvrs") (f (quote (("std") ("par_iter" "rayon") ("deser" "serde") ("default" "std"))))))

