(define-module (crates-io in de indexlist) #:use-module (crates-io))

(define-public crate-indexlist-0.1.0 (c (n "indexlist") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "generational-arena") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)))) (h "1f5rhxy4h9m0jmabi3j6w3bs3v7m36vxvm90qg48lyajyplk0kp0")))

