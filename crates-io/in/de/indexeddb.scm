(define-module (crates-io in de indexeddb) #:use-module (crates-io))

(define-public crate-indexeddb-0.1.0 (c (n "indexeddb") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.29") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.3.6") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.6") (f (quote ("Window" "DomException" "IdbDatabase" "IdbFactory" "IdbOpenDbRequest" "IdbRequest" "IdbRequestReadyState"))) (d #t) (k 0)))) (h "17q3w5syqhkibv33yal4ppgchc7a4fxfgiq9acq9z34xq0w95g1f")))

