(define-module (crates-io in de indexing) #:use-module (crates-io))

(define-public crate-indexing-0.1.0-alpha1 (c (n "indexing") (v "0.1.0-alpha1") (d (list (d (n "compiletest_rs") (r "^0.0.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1a1x42ly0i4zy5rd5gdshj3yk4b15bqn3ssv8vfb63lj41bxylyy")))

(define-public crate-indexing-0.1.0-alpha2 (c (n "indexing") (v "0.1.0-alpha2") (d (list (d (n "compiletest_rs") (r "^0.0.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1d5ia27dfqhxf7281i0jqnzxcvdpcpskqrljs5adz5pbfl05clsz")))

(define-public crate-indexing-0.1.0-alpha3 (c (n "indexing") (v "0.1.0-alpha3") (d (list (d (n "compiletest_rs") (r "^0.0.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1ari3jina0zcfxy2fgyjhylxyibkb134yxd21k9m8b15axf294g8")))

(define-public crate-indexing-0.1.0-alpha.4 (c (n "indexing") (v "0.1.0-alpha.4") (d (list (d (n "compiletest_rs") (r "^0.0.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1nhnmqmbd5rlr5jr5y2m6dpa3i2n6r2y9b40ij8x6zhdvp01zpwm")))

(define-public crate-indexing-0.1.0-alpha5 (c (n "indexing") (v "0.1.0-alpha5") (d (list (d (n "compiletest_rs") (r "^0.0.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0w9qhyja6xm49lgdkxn0mk3ba5zyyz0frcg95h30966523qrdip0")))

(define-public crate-indexing-0.1.0 (c (n "indexing") (v "0.1.0") (d (list (d (n "compiletest_rs") (r "^0.0.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "014d99bq11vpg8w0mfncb7d9k4cc28nkfwh57bvnhx3bmj1y1r7j")))

(define-public crate-indexing-0.1.1 (c (n "indexing") (v "0.1.1") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1q58xkkc462vpqz0dr6h0kx0xb79gc2ph9qhy8aabr1akkqr3zza")))

(define-public crate-indexing-0.1.2 (c (n "indexing") (v "0.1.2") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0362cqmapqp45wl372fjkkb49s4q9j21j8wswllmqkx6x2gz06fx")))

(define-public crate-indexing-0.2.0 (c (n "indexing") (v "0.2.0") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.0") (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "00yxz4rpfpfsz94xmnpk90v7zs8ni517x9ah6xkp21glr58vbid9") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-indexing-0.3.0 (c (n "indexing") (v "0.3.0") (d (list (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.0") (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "17484f8nc7grv2m1cnxrwxzrvmf45cg9kkm89nzyjvbh0gqvygic") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-indexing-0.3.1 (c (n "indexing") (v "0.3.1") (d (list (d (n "compiletest_rs") (r "^0.2.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.4.0") (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1n361xa8yhisgj5y2y79g7gwqsn2p8fnwcm4haqd3mg570dyqlmf") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-indexing-0.3.2 (c (n "indexing") (v "0.3.2") (d (list (d (n "compiletest_rs") (r "^0.3.3") (d #t) (k 2)) (d (n "quickcheck") (r "^0.5.0") (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1b3an37g4s61fg9clk4jk967hb5z55w7jbvvcg7qm4m2355insqb") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-indexing-0.4.0 (c (n "indexing") (v "0.4.0") (d (list (d (n "compiletest_rs") (r "^0.3.17") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0w21zp7cy9kx6jm48fmwrp95ln5zb9lnc2wzlz26spwrcdaffh90") (f (quote (("use_std") ("test_compiletest" "compiletest_rs") ("experimental_pointer_ranges") ("default" "use_std"))))))

(define-public crate-indexing-0.4.1 (c (n "indexing") (v "0.4.1") (d (list (d (n "compiletest_rs") (r "^0.3.17") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "046fbzg6injspb5s11mv8il8xr8pz12kgjm6ryw9809axcrdbi6k") (f (quote (("use_std") ("test_compiletest" "compiletest_rs") ("experimental_pointer_ranges") ("default" "use_std"))))))

