(define-module (crates-io in de indexed-merkle-tree) #:use-module (crates-io))

(define-public crate-indexed-merkle-tree-0.1.0 (c (n "indexed-merkle-tree") (v "0.1.0") (d (list (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "14vbxsqn5pcdvd3wjvvqwzgp0v3ign81g2kw93xjraj8sa6n5sl1")))

(define-public crate-indexed-merkle-tree-0.1.1 (c (n "indexed-merkle-tree") (v "0.1.1") (d (list (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1ikbv5jl8pqfrnf47afq4zii0z14an3qisyssg88pxswnc3zdymc")))

(define-public crate-indexed-merkle-tree-0.1.2 (c (n "indexed-merkle-tree") (v "0.1.2") (d (list (d (n "cargo-audit") (r "^0.20.0") (d #t) (k 0)) (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1mdyalbn0ad4gc16kphwabhhmdsk1822dqhxj7sj5zqbydv1r096")))

(define-public crate-indexed-merkle-tree-0.3.0 (c (n "indexed-merkle-tree") (v "0.3.0") (d (list (d (n "cargo-audit") (r "^0.20.0") (d #t) (k 0)) (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "19a90kjjfjcwrgplyj0nm3sxcsffmj2a17lqvr0a37i5nxsfi3bg")))

