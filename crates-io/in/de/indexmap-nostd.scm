(define-module (crates-io in de indexmap-nostd) #:use-module (crates-io))

(define-public crate-indexmap-nostd-0.1.0 (c (n "indexmap-nostd") (v "0.1.0") (h "0yc4l98xfbwdrjskc7c8g44mx55axaa91gvfb42bk114ywcrp81i") (f (quote (("std") ("default" "std"))))))

(define-public crate-indexmap-nostd-0.2.0 (c (n "indexmap-nostd") (v "0.2.0") (h "1fzdq5vci0v1n9f0kgd538w2l1ia746l20fhwrqyvl997si1k86n") (f (quote (("std") ("default" "std"))))))

(define-public crate-indexmap-nostd-0.3.0 (c (n "indexmap-nostd") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0jmc1kg142v85vg5lxxi6mbkpnig3yh95c80wym4pcmk2qh5b90f") (f (quote (("std") ("default" "std"))))))

(define-public crate-indexmap-nostd-0.4.0 (c (n "indexmap-nostd") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "145mrkrrnzzg8xbv6si8j3b8cw1pi3g13vrjgf1fm2415gyy414f") (f (quote (("std") ("default" "std"))))))

