(define-module (crates-io in de indenter) #:use-module (crates-io))

(define-public crate-indenter-0.1.0 (c (n "indenter") (v "0.1.0") (h "0ig7afrd2g6ifsib3r4k0jwigzv689ml924fcp5v4jrz6m01c9fk")))

(define-public crate-indenter-0.1.1 (c (n "indenter") (v "0.1.1") (h "0cc7m44icwi6p5s5vvkfw2idfqf65wysl9lwc5qmza4rqhyj2llw")))

(define-public crate-indenter-0.1.3 (c (n "indenter") (v "0.1.3") (h "02fzbj6lrxiim0h927ckfm3h0v16qdc15bm59b20bi3j6d4ssj03")))

(define-public crate-indenter-0.2.0 (c (n "indenter") (v "0.2.0") (h "1z5llxsayfk6hyn6i8wkrwmrjiq7sm5hvn39rssx9340r8wljyik")))

(define-public crate-indenter-0.3.0 (c (n "indenter") (v "0.3.0") (h "1dbg07kjm1x04i4k135vji6mwayr9p80bdcyh6k71n6r8hni3gg0")))

(define-public crate-indenter-0.3.1 (c (n "indenter") (v "0.3.1") (h "1nz96qmd4p58pr6kfx4fqrpxz6dkw0slfb9bw2vm6fqndaxp5dv8")))

(define-public crate-indenter-0.3.2 (c (n "indenter") (v "0.3.2") (h "1j0yjygkiy4wxvdw9jkspf4qfrfjia425p7sw1zjpv2g24pfpmgl")))

(define-public crate-indenter-0.3.3 (c (n "indenter") (v "0.3.3") (h "10y6i6y4ls7xsfsc1r3p5j2hhbxhaqnk5zzk8aj52b14v05ba8yf") (f (quote (("std") ("default"))))))

