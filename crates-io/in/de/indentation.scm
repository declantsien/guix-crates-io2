(define-module (crates-io in de indentation) #:use-module (crates-io))

(define-public crate-indentation-0.1.0 (c (n "indentation") (v "0.1.0") (h "0mna3i1ykfkc2rmx04jmg96588wxpz12jdvybsfk4qcfixqwa6za") (f (quote (("default"))))))

(define-public crate-indentation-0.1.1 (c (n "indentation") (v "0.1.1") (h "11gqnl51675h5jacf350vpks0m2wsjn0y9msxyr8j2j1z823fhpr") (f (quote (("default"))))))

(define-public crate-indentation-0.1.2 (c (n "indentation") (v "0.1.2") (h "1l353gjpbcvh97jpf9rsszd5fk99sxdnknd6lj5rvnlnsky56njh") (f (quote (("default"))))))

(define-public crate-indentation-0.1.3 (c (n "indentation") (v "0.1.3") (h "0dc1r9bwywsb5k45l1ny7r49yqidbd2zdiczhsm159z2hljmhzhl") (f (quote (("default"))))))

(define-public crate-indentation-0.1.4 (c (n "indentation") (v "0.1.4") (h "0ia6nv3acbhpzf5nz5y8ywx0jhxjwvbfn3wwd8mimlfyrqrlph4d") (f (quote (("default"))))))

(define-public crate-indentation-0.1.5 (c (n "indentation") (v "0.1.5") (h "072pl84xxsaskldyf07m4hz670g78lwjfncxiiiki4dr7dymavv9") (f (quote (("default"))))))

(define-public crate-indentation-0.1.6 (c (n "indentation") (v "0.1.6") (h "0dizgn5b2jidlz3ba2r3qwxv84ss0gbh9ik8ry4jp8hx59pwyl35") (f (quote (("default"))))))

