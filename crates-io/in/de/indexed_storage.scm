(define-module (crates-io in de indexed_storage) #:use-module (crates-io))

(define-public crate-indexed_storage-0.1.0 (c (n "indexed_storage") (v "0.1.0") (h "1sgs3xgsxmbj66r5khxhk8ml0zj27as45s2y4n3fdnidfgp15hdq")))

(define-public crate-indexed_storage-0.1.1 (c (n "indexed_storage") (v "0.1.1") (h "10nl3hq9bayvb254wzzs1ci0zawzhkr7airdils0pw6s3ym6zcay")))

