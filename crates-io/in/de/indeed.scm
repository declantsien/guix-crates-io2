(define-module (crates-io in de indeed) #:use-module (crates-io))

(define-public crate-indeed-0.1.0 (c (n "indeed") (v "0.1.0") (h "15ni2p7mki6pf8mh4q4n5cz6anq5wf862bd03fxn1mvjc3pd7f5g")))

(define-public crate-indeed-0.2.0 (c (n "indeed") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "020zj274m5fbi7wiymxhdm98gqa9vaixj2h22nk9agx3500v4b3j")))

(define-public crate-indeed-0.3.0 (c (n "indeed") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f1f4ny4a0qj82haqidk0lvawvm2j082g187yfkf6x2ly6gysavx")))

(define-public crate-indeed-0.4.0 (c (n "indeed") (v "0.4.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)))) (h "0gh93a3fggqzwh2ywz1xgrmyip5w7c2ic790i5ysf4axfx39w82y")))

