(define-module (crates-io in de index) #:use-module (crates-io))

(define-public crate-index-0.15.4 (c (n "index") (v "0.15.4") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pagecache") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0dghj0syz1zjb5plr252skrf5j2rxfx321wbcmwmrk6ldb7aivrm") (f (quote (("lock_free_delays") ("default"))))))

