(define-module (crates-io in de indexset) #:use-module (crates-io))

(define-public crate-indexset-0.1.0 (c (n "indexset") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "03ax9fa0plrsz7ljpaydypk8alfj3nn97bm7jfj3phfirg4y0gpw")))

(define-public crate-indexset-0.2.0 (c (n "indexset") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "00bi4kyh15k8xciibq74kgy04ja9bqayjh2l71mpjrahi75hqxd1") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-indexset-0.3.0 (c (n "indexset") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0gh2bba4gf5qz5l9ki3781qi5h7ii4vrcb4zpxq4m0ka392r8dy6") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-indexset-0.3.1 (c (n "indexset") (v "0.3.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ss4vjcy458rsx4absclvbkw75ws6gkmgcdjd3gjj982kqwkq0hd") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-indexset-0.3.2 (c (n "indexset") (v "0.3.2") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "ftree") (r "^0.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "11kn6s6kj2cbpfqqb86fshp1w22izlcfsz044mkgx98bnahg75iy") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-indexset-0.3.3 (c (n "indexset") (v "0.3.3") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "ftree") (r "^0.1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1sffn186kaw8izapl3r1574kgxsgy5vm71syx39fka527gvf3va4") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-indexset-0.3.4 (c (n "indexset") (v "0.3.4") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "ftree") (r "^1.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1y9n53rs41km5ri537x3gi3hy400r1z2yvbpsvdywr93sma74khx") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-indexset-0.3.5 (c (n "indexset") (v "0.3.5") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "ftree") (r "^1.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1n9mrhazk2yfazfyagchxpx6vypjbhd0csz1m12ymmqh0ggm4s7z") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-indexset-0.3.6 (c (n "indexset") (v "0.3.6") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "ftree") (r "^1.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "09xhmnjm1z14rps1q3w5844cwz38d83m5n99shbqxidaq6idnwi1") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-indexset-0.3.7 (c (n "indexset") (v "0.3.7") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "ftree") (r "^1.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0sqij217snvzsaz63gjr8cy2b7kwpxavkw1jqdc2a45kqqqw9738") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-indexset-0.3.8 (c (n "indexset") (v "0.3.8") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "ftree") (r "^1.0.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0jy8dzi6vsqs5rvzid7mrhansx9pg9zw8ap357h1gg54fiyjq9j8") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-indexset-0.4.0 (c (n "indexset") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "ftree") (r "^1.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1hx490zqq1fi0srzhskgwbbzpj32zbala3zdycgiwv044dn8dchq") (s 2) (e (quote (("serde" "dep:serde"))))))

