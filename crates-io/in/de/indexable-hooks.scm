(define-module (crates-io in de indexable-hooks) #:use-module (crates-io))

(define-public crate-indexable-hooks-0.1.0 (c (n "indexable-hooks") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.13") (d #t) (k 0)) (d (n "schemars") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02fq492r2hsb3xdm1nxdvrb3xk2n0z3lci52q6gdjs0f3qp94w0d")))

