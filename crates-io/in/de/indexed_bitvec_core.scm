(define-module (crates-io in de indexed_bitvec_core) #:use-module (crates-io))

(define-public crate-indexed_bitvec_core-2.0.0 (c (n "indexed_bitvec_core") (v "2.0.0") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0dsvrgw86mz8qi6yd4m8z0dibmrga87zzy5d08h0pkcdhqx6dlpn")))

(define-public crate-indexed_bitvec_core-2.1.0 (c (n "indexed_bitvec_core") (v "2.1.0") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.1") (d #t) (k 2)))) (h "06skvh5zcph72ily1a1gpav8hf3cb303b97yb4qgnxz1p968w0c9")))

(define-public crate-indexed_bitvec_core-2.2.0 (c (n "indexed_bitvec_core") (v "2.2.0") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.1") (d #t) (k 2)))) (h "0p1g2sj3wyfp8766vly5r227fydhbyjkdhzg2qrnhb70k1gcz8dv")))

(define-public crate-indexed_bitvec_core-3.0.0 (c (n "indexed_bitvec_core") (v "3.0.0") (d (list (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.1") (d #t) (k 2)))) (h "0gnpbbiiy69m99sadmnrsrgr7fnjd4gwxqr217w20i6jlw60w7gm") (f (quote (("implement_heapsize" "heapsize") ("default"))))))

(define-public crate-indexed_bitvec_core-3.1.0 (c (n "indexed_bitvec_core") (v "3.1.0") (d (list (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.1") (d #t) (k 2)))) (h "0aiz7dkj17b2393dsp2q6vljya8mly72c8mfvswdjh445px3i694") (f (quote (("implement_heapsize" "heapsize") ("default"))))))

(define-public crate-indexed_bitvec_core-3.1.1 (c (n "indexed_bitvec_core") (v "3.1.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.1") (d #t) (k 2)))) (h "08pvkymq5pwc3q6jq29pi8s0yzfs72pmp4ns1n9z3n7kak90jwdl") (f (quote (("implement_heapsize" "heapsize") ("default"))))))

(define-public crate-indexed_bitvec_core-4.0.0 (c (n "indexed_bitvec_core") (v "4.0.0") (d (list (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.1") (d #t) (k 2)))) (h "0apmy4a21a2b0r88nvczd3qm4pchi58ld1fx64ar1wd3hdm5ihnk")))

