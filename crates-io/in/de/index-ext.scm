(define-module (crates-io in de index-ext) #:use-module (crates-io))

(define-public crate-index-ext-0.0.1 (c (n "index-ext") (v "0.0.1") (h "0msl08sm6kcmqqsyjklhdqbbw3y7f66j0gr1dnwr2j36623dly0g") (f (quote (("nightly"))))))

(define-public crate-index-ext-0.0.2 (c (n "index-ext") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "00804vpx7bchn7p3f5bpgi1lyim2wcm81qwlw7lanzlw6sx10pjd") (f (quote (("nightly") ("alloc"))))))

(define-public crate-index-ext-1.0.0-beta.0 (c (n "index-ext") (v "1.0.0-beta.0") (d (list (d (n "generativity") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0z0vxij05v97p2k93xfr7fnrnma6p2b897hx1k8mn50iriy323cg") (f (quote (("nightly") ("alloc"))))))

(define-public crate-index-ext-1.0.0-beta.1 (c (n "index-ext") (v "1.0.0-beta.1") (d (list (d (n "generativity") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1abxl69rdrifr93p6rijjiihbc8yc41s7pxdgybajj3hp8rrqnii") (f (quote (("alloc"))))))

(define-public crate-index-ext-1.0.0-beta.2 (c (n "index-ext") (v "1.0.0-beta.2") (d (list (d (n "generativity") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "129rqh9f2p48i4l8gkja2vvxx1c9haqa2v411bfvfv4bwgbm398g") (f (quote (("alloc"))))))

(define-public crate-index-ext-1.0.0 (c (n "index-ext") (v "1.0.0") (d (list (d (n "generativity") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1lqixidibigxwcgdmvxkq0a00rlmrzr1kvyd6s7s5l0kppxl5v69") (f (quote (("alloc")))) (r "1.61")))

