(define-module (crates-io in de indented_text_writer) #:use-module (crates-io))

(define-public crate-indented_text_writer-0.1.0 (c (n "indented_text_writer") (v "0.1.0") (h "17jnprz66r8lbzh0iq7s6yvdxpl3gz6j1hbr05ja20jbzi5f0apa")))

(define-public crate-indented_text_writer-0.1.1 (c (n "indented_text_writer") (v "0.1.1") (h "09axyl74pbm7m4palpx029ybql6pkfhjazxs465k60s80b1cryzk")))

(define-public crate-indented_text_writer-0.2.0 (c (n "indented_text_writer") (v "0.2.0") (h "1zvmk12328gq57i1cnfn73awvhs4lcybwkfvsaiz2lg2dqj05hxa")))

(define-public crate-indented_text_writer-0.3.0 (c (n "indented_text_writer") (v "0.3.0") (h "1vzfj0102xcihdz3ws706nn0xsbwkv8qs07h4hrb56i2adk3mwlw")))

(define-public crate-indented_text_writer-0.4.0 (c (n "indented_text_writer") (v "0.4.0") (h "1c9da73arm8d5p8dvf4caw6x9a4cxqx3sjkny3k5h0hnfxzpfg3n")))

