(define-module (crates-io in de indexrs) #:use-module (crates-io))

(define-public crate-indexrs-0.1.0 (c (n "indexrs") (v "0.1.0") (d (list (d (n "unicode-normalization") (r "^0.1.17") (d #t) (k 0)))) (h "03062g28a2iwyja9d768yv12imj4d7ihr8r8dag3n7jnijabwngv")))

(define-public crate-indexrs-0.2.0 (c (n "indexrs") (v "0.2.0") (d (list (d (n "unicode-normalization") (r "^0.1.17") (d #t) (k 0)))) (h "0xbgdnbkgpyvs2lkcnfyrgiy1h1z67cmfq0cj65bdxxmskj22134")))

(define-public crate-indexrs-0.3.0 (c (n "indexrs") (v "0.3.0") (d (list (d (n "unicode-normalization") (r "^0.1.17") (d #t) (k 0)))) (h "1kmhmsn451rh9a0mxapzfyfwg495f6h54ha4y52aa4pya4ad5llf")))

(define-public crate-indexrs-0.4.0 (c (n "indexrs") (v "0.4.0") (d (list (d (n "unicode-normalization") (r "^0.1.17") (d #t) (k 0)))) (h "091l68q4677l7kq9jf9a5c1p6wiw7wx80a95lycwl25p1f2vmnsq")))

(define-public crate-indexrs-0.5.0 (c (n "indexrs") (v "0.5.0") (d (list (d (n "unicode-normalization") (r "^0.1.17") (d #t) (k 0)))) (h "1j1nqlbc52gsjqm8qphr6652vhzcrnifvgl90g9r0s1jza69v3hf")))

