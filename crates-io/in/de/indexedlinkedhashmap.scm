(define-module (crates-io in de indexedlinkedhashmap) #:use-module (crates-io))

(define-public crate-indexedlinkedhashmap-2.0.0 (c (n "indexedlinkedhashmap") (v "2.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0vsn4i17yq8rvxlqjjrcp6qj91apgmb51kzf63cmdxqmv8g98hla")))

(define-public crate-indexedlinkedhashmap-2.0.1 (c (n "indexedlinkedhashmap") (v "2.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1qpsw1pwjir9pr0rjkbvz425i8xw9nnz51n9c913h2q7ak8gygwq")))

(define-public crate-indexedlinkedhashmap-2.0.2 (c (n "indexedlinkedhashmap") (v "2.0.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "119majjnc2s67r8al6q2yba8s1dw8617nc21y4nvi60h5nbwm03a")))

(define-public crate-indexedlinkedhashmap-3.0.0 (c (n "indexedlinkedhashmap") (v "3.0.0") (h "0l0cn30wnzkpp1yd6bda6fngxplh8lcsi4xb6j4ssblzkpdmgg5d") (f (quote (("collections_ordering_vec") ("collections_ordering_binary_heap"))))))

