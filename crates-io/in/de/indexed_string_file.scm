(define-module (crates-io in de indexed_string_file) #:use-module (crates-io))

(define-public crate-indexed_string_file-0.1.0 (c (n "indexed_string_file") (v "0.1.0") (d (list (d (n "indexed_data_file") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "strings_set_file") (r "^0.1") (d #t) (k 0)))) (h "10bad7vsx4l1zl4175pmh0hrk0xism9mf6s4g77v82wqsi608rpd") (y #t)))

(define-public crate-indexed_string_file-0.2.0 (c (n "indexed_string_file") (v "0.2.0") (d (list (d (n "indexed_data_file") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "strings_set_file") (r "^0.1") (d #t) (k 0)))) (h "04cs06h8wxaj8ppl7xlbkdhhp6n9q0hm896sjcin3ylwcm6b3g7g") (y #t)))

(define-public crate-indexed_string_file-0.3.0 (c (n "indexed_string_file") (v "0.3.0") (d (list (d (n "indexed_data_file") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "strings_set_file") (r "^0.1") (d #t) (k 0)))) (h "1shj039ffbba5vwnmmxxsg92s43m1648zkf7b834bhk4g5bib4cn") (y #t)))

(define-public crate-indexed_string_file-0.4.0 (c (n "indexed_string_file") (v "0.4.0") (d (list (d (n "indexed_data_file") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "strings_set_file") (r "^0.1") (d #t) (k 0)))) (h "18ra8zga56hlg9lr6i9092ci8d7zmfz4v0sks7jc3nc4fd5ymhvz") (y #t)))

(define-public crate-indexed_string_file-0.5.0 (c (n "indexed_string_file") (v "0.5.0") (d (list (d (n "indexed_data_file") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "strings_set_file") (r "^0.1") (d #t) (k 0)))) (h "0310msa00128m2zjj8rqhrkn6cm5k50i76h0sg1j2j16iwsk1cha") (y #t)))

(define-public crate-indexed_string_file-0.6.0 (c (n "indexed_string_file") (v "0.6.0") (d (list (d (n "indexed_data_file") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "strings_set_file") (r "^0.1") (d #t) (k 0)))) (h "0fwvnahp586xikwm8i5m0pcsiqqasz7hlgxn5n5cqq21nf2vzdwl") (y #t)))

(define-public crate-indexed_string_file-0.6.1 (c (n "indexed_string_file") (v "0.6.1") (d (list (d (n "indexed_data_file") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "strings_set_file") (r "^0.1") (d #t) (k 0)))) (h "0pm3fyys7j15r7qwm0r5mwm1pb4bl900nbpsnwmmj1z91khjvazs") (y #t)))

(define-public crate-indexed_string_file-0.6.2 (c (n "indexed_string_file") (v "0.6.2") (d (list (d (n "indexed_data_file") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "strings_set_file") (r "^0.1") (d #t) (k 0)))) (h "07mgnrf8cmv1jqx4rq2rx75xd783xkqp67jg46dl9znhzq41knp5") (y #t)))

