(define-module (crates-io in de indextree-ng) #:use-module (crates-io))

(define-public crate-indextree-ng-1.0.0 (c (n "indextree-ng") (v "1.0.0") (h "0a4clp42pddwx1j82xnxym4p76kasfckw2mpsxxvc3hfwlw8dd3j")))

(define-public crate-indextree-ng-1.0.1 (c (n "indextree-ng") (v "1.0.1") (h "0vm6krna7xypj0w83v107ffybb7smhqm0c99rnrvxr1axvidf20n")))

(define-public crate-indextree-ng-1.0.2 (c (n "indextree-ng") (v "1.0.2") (h "14nv6di9x6inyl02xs3pqfpc66y7gzgkk5va96vxqqfl8qsckfzk")))

(define-public crate-indextree-ng-1.0.3 (c (n "indextree-ng") (v "1.0.3") (h "0y25nkfqnd3kshkbal8cvj007g12v1p20nywqnj5xq9sksrbs7s9")))

(define-public crate-indextree-ng-1.0.4 (c (n "indextree-ng") (v "1.0.4") (h "1403z8i6j2kqa8kcrdlfpi4hkkarcr301lfg2fr7whlhh4nkswqb")))

(define-public crate-indextree-ng-1.0.5 (c (n "indextree-ng") (v "1.0.5") (h "0ga2lrnw9bribg0bdzsp2xfisrmbw5m1vrjl0crnl6j4dgkx1wf6")))

