(define-module (crates-io in va invalid-mutations) #:use-module (crates-io))

(define-public crate-invalid-mutations-0.1.0 (c (n "invalid-mutations") (v "0.1.0") (d (list (d (n "move-binary-format") (r "^0.1.0") (d #t) (k 0) (p "mv-binary-format")) (d (n "move-core-types") (r "^0.1.0") (d #t) (k 0) (p "mv-core-types")) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)))) (h "1ngq8di444nb0ycyqikkjzc4masvgnx96rpgfksph5izv4npxjg6") (f (quote (("default")))) (y #t)))

(define-public crate-invalid-mutations-0.1.1 (c (n "invalid-mutations") (v "0.1.1") (d (list (d (n "move-binary-format") (r "^0.1.0") (d #t) (k 0) (p "mv-binary-format")) (d (n "move-core-types") (r "^0.1.0") (d #t) (k 0) (p "mv-core-types")) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)))) (h "0g67z8xnir1sgi1h16l6yz6rz634i2s2i81my8d1wm0w3pjamfp1") (f (quote (("default")))) (y #t)))

(define-public crate-invalid-mutations-0.1.2 (c (n "invalid-mutations") (v "0.1.2") (d (list (d (n "move-binary-format") (r "^0.1.0") (d #t) (k 0) (p "mv-binary-format")) (d (n "move-core-types") (r "^0.1.0") (d #t) (k 0) (p "mv-core-types")) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)))) (h "1k571baq53ad94ns79x142mahcy10a5jxd9pygafcca96jxyk8g2") (f (quote (("default")))) (y #t)))

(define-public crate-invalid-mutations-0.1.4 (c (n "invalid-mutations") (v "0.1.4") (d (list (d (n "mv-binary-format") (r "^0.1.0") (d #t) (k 0)) (d (n "mv-core-types") (r "^0.1.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)))) (h "0jw20sgmcy38rgzy8ilnh1x0r6lxzqsl889fl444h3z6qi89k2il") (f (quote (("default")))) (y #t)))

(define-public crate-invalid-mutations-0.1.5 (c (n "invalid-mutations") (v "0.1.5") (d (list (d (n "mv-binary-format") (r "^0.1.0") (d #t) (k 0)) (d (n "mv-core-types") (r "^0.1.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)))) (h "1pdscncr42msqi40sbgyijklaalmfa5cnc3sw71y17n1v6gb7qmg") (f (quote (("default")))) (y #t)))

(define-public crate-invalid-mutations-0.1.6 (c (n "invalid-mutations") (v "0.1.6") (d (list (d (n "mv-binary-format") (r "^0.1.0") (d #t) (k 0)) (d (n "mv-core-types") (r "^0.1.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)))) (h "0c00s250vbfh7jf23j53ccm8pdfwh4ksyd2w4nfrzlncappb9agv") (f (quote (("default")))) (y #t)))

(define-public crate-invalid-mutations-0.2.0 (c (n "invalid-mutations") (v "0.2.0") (d (list (d (n "mv-binary-format") (r "^0.2.0") (d #t) (k 0)) (d (n "mv-core-types") (r "^0.2.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)))) (h "1xdqq9c1z6jldx6m78j26s73lvy54ifwh7lkbgr9814rdlvm1i8w") (f (quote (("default")))) (y #t)))

(define-public crate-invalid-mutations-0.2.1 (c (n "invalid-mutations") (v "0.2.1") (d (list (d (n "mv-binary-format") (r "^0.2.0") (d #t) (k 0)) (d (n "mv-core-types") (r "^0.2.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)))) (h "1hicvr1maspb1b31ki5l9p4l028fyc47ayn60kq19p5p7w3h28yz") (f (quote (("default")))) (y #t)))

(define-public crate-invalid-mutations-0.3.0 (c (n "invalid-mutations") (v "0.3.0") (d (list (d (n "mv-binary-format") (r "^0.3.0") (d #t) (k 0)) (d (n "mv-core-types") (r "^0.3.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)))) (h "15c9gwc6ig2b9cwgdma7m46fhlb9snlb5y462aycklp43nklab7k") (f (quote (("default")))) (y #t)))

(define-public crate-invalid-mutations-0.3.1 (c (n "invalid-mutations") (v "0.3.1") (d (list (d (n "mv-binary-format") (r "^0.3.0") (d #t) (k 0)) (d (n "mv-core-types") (r "^0.3.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)))) (h "0vzhfyd4s5sq1iw22aqbsxzc8a6z6ncclivi1a3qn2p94652fwzi") (f (quote (("default")))) (y #t)))

(define-public crate-invalid-mutations-0.3.2 (c (n "invalid-mutations") (v "0.3.2") (d (list (d (n "mv-binary-format") (r "^0.3.0") (d #t) (k 0)) (d (n "mv-core-types") (r "^0.3.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 0)))) (h "0y3vgyyylbivmfbcrrbdsfypw3dfhwphjihsplsf7ircb1wx8d0b") (f (quote (("default")))) (y #t)))

