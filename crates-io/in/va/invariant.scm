(define-module (crates-io in va invariant) #:use-module (crates-io))

(define-public crate-invariant-0.0.1 (c (n "invariant") (v "0.0.1") (h "0qr1aw3wghzm5np57k2jnagh2qhdzk5hf1irnps0yqnc1r9fhw79")))

(define-public crate-invariant-0.0.2 (c (n "invariant") (v "0.0.2") (h "1mxk6s1yi9w0x6ysk62nj2a4857mp4i3sns06105lqwkdda2505y") (f (quote (("all-dependencies"))))))

