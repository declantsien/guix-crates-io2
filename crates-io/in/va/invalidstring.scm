(define-module (crates-io in va invalidstring) #:use-module (crates-io))

(define-public crate-invalidstring-0.1.0 (c (n "invalidstring") (v "0.1.0") (h "15sr7nhdvjprzz9lsd1dfx3yn9a24ayvzfbrw7x5grymvafckpji")))

(define-public crate-invalidstring-0.1.1 (c (n "invalidstring") (v "0.1.1") (h "1kg63zkjx7vpp8fmmvpzpwwwz3f75xi8s2bwj845jbycv8arslc4")))

(define-public crate-invalidstring-0.1.2 (c (n "invalidstring") (v "0.1.2") (h "1k7061smkxgjmix16wim2c4qb8gkdggaxilkd9yxhmza5lgpsmi2")))

(define-public crate-invalidstring-0.1.3 (c (n "invalidstring") (v "0.1.3") (h "0vxin67ybys7ihmwr5xvvwqv3q7lcn2pxg1skw14xr8kjpa0z5nj")))

