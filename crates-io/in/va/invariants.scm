(define-module (crates-io in va invariants) #:use-module (crates-io))

(define-public crate-invariants-0.1.0 (c (n "invariants") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0v5sy56zyjpn404rja6y3ccnx8d234c0g7cb0syf6bhqr97zrpy7")))

(define-public crate-invariants-0.1.1 (c (n "invariants") (v "0.1.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "02jrlh7kzv3m2kdn8l6fdvxm6473n535q4vsxnina3zysyc03vfk")))

(define-public crate-invariants-0.1.2 (c (n "invariants") (v "0.1.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0xi3mm9gpzghim156kx433wn9xsvq199nnq5kpyzbi8v96zz62v5")))

(define-public crate-invariants-0.1.3 (c (n "invariants") (v "0.1.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0kl28sqamm7lx4fh8sh1j23zm228vw9di2fv095fplrp2pg4cav4")))

