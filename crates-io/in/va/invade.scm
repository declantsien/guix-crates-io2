(define-module (crates-io in va invade) #:use-module (crates-io))

(define-public crate-invade-0.0.1 (c (n "invade") (v "0.0.1") (d (list (d (n "invade_derive") (r "^0.0.1") (d #t) (k 0)))) (h "1m1nviif2yrsvlkzv4z9hw7q45f1vqj2biws5833x90khzbaz79a")))

(define-public crate-invade-0.0.2 (c (n "invade") (v "0.0.2") (d (list (d (n "invade_derive") (r "^0.0.2") (d #t) (k 0)))) (h "0k4blkxk698l6ijzlbi694cdm264gyihr19k8i0myalz8ni7jk48")))

(define-public crate-invade-0.0.3 (c (n "invade") (v "0.0.3") (d (list (d (n "invade_derive") (r "^0.0.2") (d #t) (k 0)))) (h "152klvwcxav9di3hx0k533r4gfgicy8iviip7vh50jd1flh3nph9")))

(define-public crate-invade-0.0.4 (c (n "invade") (v "0.0.4") (d (list (d (n "invade_derive") (r "^0.0.4") (d #t) (k 0)))) (h "0qxk0cjp89vi3jmini54w0wmjsvky6pw669kk0pn3x5f1z487sja")))

(define-public crate-invade-0.0.5 (c (n "invade") (v "0.0.5") (d (list (d (n "invade_derive") (r "^0.0.5") (d #t) (k 0)))) (h "1gx059r9vmblrg0dqxcibcfyiync0p2nb5svil92nzikxgiyq6c7")))

(define-public crate-invade-0.0.6 (c (n "invade") (v "0.0.6") (d (list (d (n "invade_derive") (r "^0.0.6") (d #t) (k 0)))) (h "030yyznd4k982si0y12lydrm4src4iw08k87fdr3m5lzbvjccz5c")))

