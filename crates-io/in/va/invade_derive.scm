(define-module (crates-io in va invade_derive) #:use-module (crates-io))

(define-public crate-invade_derive-0.0.1 (c (n "invade_derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "022h3vq5d2m7kicckv2l41bdcccw7a673xwm0d1iqrdfg0fmz7mk")))

(define-public crate-invade_derive-0.0.2 (c (n "invade_derive") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0b9ccngphlc6xlhbxh05dawis83wiwwjm0mcyj4a24ml4dz7zjxb")))

(define-public crate-invade_derive-0.0.3 (c (n "invade_derive") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07icjy5n2m0qpq38fs0g7n9xjxx5cfayzcrpdnyykrqphn0aa3vp")))

(define-public crate-invade_derive-0.0.4 (c (n "invade_derive") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dqxh9r1jjph838p6lz5g5l7jak7id67g9hib5c0jj5n42gp25x5")))

(define-public crate-invade_derive-0.0.5 (c (n "invade_derive") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15bnyv3rf1h2dhi85qq0zpmmmzk6qmayk1xmj8kh9qvh1ljbapji")))

(define-public crate-invade_derive-0.0.6 (c (n "invade_derive") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rmmhvy1ib7gj2vp7rsdbq3r236vd2i5368xy2z8m8ysli25lksf")))

