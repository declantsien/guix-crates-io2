(define-module (crates-io in an inane-crypto) #:use-module (crates-io))

(define-public crate-inane-crypto-0.1.0 (c (n "inane-crypto") (v "0.1.0") (h "0g0bh23v18wdwc936il4wr0454idhaa5mbdjgjg0mwm6ad22wn2c") (y #t)))

(define-public crate-inane-crypto-0.2.0 (c (n "inane-crypto") (v "0.2.0") (h "1xj6c0fy3h1g6zhjvzv4r0n26jp43zwfx0gy4nlvf6fbc0v1dlir") (y #t)))

