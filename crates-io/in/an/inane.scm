(define-module (crates-io in an inane) #:use-module (crates-io))

(define-public crate-inane-1.0.0 (c (n "inane") (v "1.0.0") (h "1f47lca2lpcdd49m026ahr0asfmmbr2r3gn6ak66rav4f9mlfn23")))

(define-public crate-inane-1.0.5 (c (n "inane") (v "1.0.5") (h "1wc0y6qv6pwnhqhfan5aqiwpa7idczqb1228adr85p1ikacycj0h")))

(define-public crate-inane-1.0.6 (c (n "inane") (v "1.0.6") (h "1mp8s9g4ba35fn60x9b2f6qy8iv0gk5dqn7w63g99pqvdqdan1vj")))

(define-public crate-inane-1.0.8 (c (n "inane") (v "1.0.8") (h "1zar0wfr1byh4v1bq07jyvps1jgyx8h8p745dd5vz21wgq9y1s36")))

(define-public crate-inane-1.0.9 (c (n "inane") (v "1.0.9") (h "152grvdjcsm8qqimjxr6nfvngkq08az5f8kmb19jxmxfnq8g0xsx")))

(define-public crate-inane-1.0.10 (c (n "inane") (v "1.0.10") (h "04h3f1xjz2224wi8g5nhvadrck2hd8ifjdw3sb91hswp00z2cxm6")))

(define-public crate-inane-1.0.11 (c (n "inane") (v "1.0.11") (h "15bgw5rar39qkdhw8k3ynxg6ya7zrmf67ay60zvnbmcpj3fzdilq")))

(define-public crate-inane-1.0.12 (c (n "inane") (v "1.0.12") (h "18ki4n21fz0j2vfvww1nla47n3rhjfarirg4sk1dknz03a8f65cg")))

(define-public crate-inane-1.0.13 (c (n "inane") (v "1.0.13") (h "1za9dbjjinvkl5f985d0ik5bacawnm7r5kdzhjdhisfm80m7rnr9")))

(define-public crate-inane-1.0.14 (c (n "inane") (v "1.0.14") (h "18p1f85f5q18qk1h1wry518xbknq6fv3a0dlxpfm54k7xvq4wibw")))

(define-public crate-inane-1.0.15 (c (n "inane") (v "1.0.15") (h "0rmk73s52rn0hajx27ik76xg6i7dsc3ffrpbhszhz5nw6vhmsq0d")))

(define-public crate-inane-1.0.16 (c (n "inane") (v "1.0.16") (h "1n6xzg2nrsw26vl0d32mb1p1dxkzylidyjqcvg2zqm44rqpzqg3q")))

(define-public crate-inane-1.0.17 (c (n "inane") (v "1.0.17") (h "1njnzrxc935iqdy9zggbqrvy2x0rg2yl2mgwnd2pmdhj6kzdzm0z")))

(define-public crate-inane-1.0.18 (c (n "inane") (v "1.0.18") (h "0rncldxihz277x5k4z5a3zrfypxfzzv7rszijcn3qpr5lmprcyds")))

(define-public crate-inane-1.0.19 (c (n "inane") (v "1.0.19") (h "1xdf5kr13gjhjz556ix12mg3dwc3yx0688qxds3j7lwklcxk1bgl")))

(define-public crate-inane-1.0.20 (c (n "inane") (v "1.0.20") (h "1f1513n25r48lh8a10ijflaccl79ji84d12y8njbf3mqb128s9kb")))

