(define-module (crates-io in dy indy-blssignatures) #:use-module (crates-io))

(define-public crate-indy-blssignatures-0.1.0 (c (n "indy-blssignatures") (v "0.1.0") (d (list (d (n "amcl") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)))) (h "1nq0aaca01c8r4b97sf6q2nm8qkhazzn3z22x4xy3sjm85lgb93y") (f (quote (("default" "serde"))))))

