(define-module (crates-io in dy indy-wql) #:use-module (crates-io))

(define-public crate-indy-wql-0.4.0 (c (n "indy-wql") (v "0.4.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0nn26ykabjdkrni7npjlblb2xb8aw94ybxw1w1nnd1y3zg33gapl") (f (quote (("serde_support" "serde" "serde_json") ("default" "serde_support"))))))

