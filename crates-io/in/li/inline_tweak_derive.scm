(define-module (crates-io in li inline_tweak_derive) #:use-module (crates-io))

(define-public crate-inline_tweak_derive-1.0.0 (c (n "inline_tweak_derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro" "visit-mut" "full" "printing"))) (k 0)))) (h "0pdwibzsld2d3qcv4hfkw2hyfkrrp1np9imqm34cwsww4nvx2gdg") (f (quote (("release_tweak"))))))

(define-public crate-inline_tweak_derive-1.0.1 (c (n "inline_tweak_derive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro" "visit-mut" "full" "printing"))) (k 0)))) (h "184ikap4fblng106k2ia289z0yqidynwcxw6nfsk76d48s2v04k4")))

(define-public crate-inline_tweak_derive-1.1.0 (c (n "inline_tweak_derive") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro" "visit-mut" "full" "printing"))) (k 0)))) (h "1v75h05q7fjixl8c22imjjm5m386zg2az2wgs7r2hcibs4pxc3ny")))

(define-public crate-inline_tweak_derive-1.1.1 (c (n "inline_tweak_derive") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro" "visit-mut" "full" "printing"))) (k 0)))) (h "1hw7qhgfwjv9mpv4ws11b1dm2sc98swdmh9mliiwpkgqdfl7j2zw")))

(define-public crate-inline_tweak_derive-2.0.0 (c (n "inline_tweak_derive") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro" "visit-mut" "full" "printing"))) (k 0)))) (h "12mn2k9ki3bxcra1jpgrljxxfj0x78dz3wxsr4bc2wrha2x4x5hh")))

(define-public crate-inline_tweak_derive-3.0.0 (c (n "inline_tweak_derive") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro" "visit-mut" "full" "printing"))) (k 0)))) (h "187lfmx2j6wamkjd6lv2v074prwcfp6m2wlfxr74vw3a7c52mmj6")))

