(define-module (crates-io in li inline-xml-macros) #:use-module (crates-io))

(define-public crate-inline-xml-macros-0.1.0 (c (n "inline-xml-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0p4qn56gnz56xx1svyy6s8rbllaci11w1ww4gq5sxb3hlvb8fd1m")))

(define-public crate-inline-xml-macros-0.1.1 (c (n "inline-xml-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1rngk9hsc5lf1c3ard2np83qmxl4ziwx72kij1cnhwnl4dfc4vy3")))

(define-public crate-inline-xml-macros-0.1.2 (c (n "inline-xml-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1x2f57wyrwziqyh9b1y8qqc4i7zmi61a099vw40f07ikqcy13536")))

(define-public crate-inline-xml-macros-0.1.3 (c (n "inline-xml-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1w3j1p1igalb5wmab2jw70v5j664ghdy2mwi8ky8kj6rzmdvzbpr")))

(define-public crate-inline-xml-macros-0.1.4 (c (n "inline-xml-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1mfvmqszqb8lalw73a10m0zm3gx0wijwfggf9hqbqiymbnm7qz53")))

(define-public crate-inline-xml-macros-0.1.5 (c (n "inline-xml-macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0rnj1p0hpjkzbjk3h1byv8lm6dq2cmn9f8s0m8ssblg26pihdhqz")))

(define-public crate-inline-xml-macros-0.2.0 (c (n "inline-xml-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0cxmn46q9jsl27vinliflwqbzjvkjjnrvl9j3m7248m1bnkz9fvk")))

