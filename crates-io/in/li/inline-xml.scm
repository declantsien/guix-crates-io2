(define-module (crates-io in li inline-xml) #:use-module (crates-io))

(define-public crate-inline-xml-0.1.0 (c (n "inline-xml") (v "0.1.0") (d (list (d (n "inline-xml-macros") (r "=0.1.0") (d #t) (k 0)))) (h "16j4l5v7iramrp6jg24clm7754l47gra3srvpv1narcyp8q1cx6h")))

(define-public crate-inline-xml-0.1.1 (c (n "inline-xml") (v "0.1.1") (d (list (d (n "inline-xml-macros") (r "=0.1.0") (d #t) (k 0)))) (h "17wc4qfvqfd77d7sfyrq28aypaamm3pjy5i8l9nx2kysw9q5011h")))

(define-public crate-inline-xml-0.1.2 (c (n "inline-xml") (v "0.1.2") (d (list (d (n "inline-xml-macros") (r "=0.1.0") (d #t) (k 0)))) (h "025v0p3ljfin6bdrpiqddfd20sbqy5nj4q92n382vqy7241b4szk")))

(define-public crate-inline-xml-0.2.0 (c (n "inline-xml") (v "0.2.0") (d (list (d (n "inline-xml-macros") (r "=0.1.0") (d #t) (k 0)))) (h "12ykh2bbdl0bgn41i1lpy97z1hnlaz8fyjciwbylasm6qz8spmz0")))

(define-public crate-inline-xml-0.2.1 (c (n "inline-xml") (v "0.2.1") (d (list (d (n "inline-xml-macros") (r "=0.1.0") (d #t) (k 0)))) (h "0khpw4srxl98w32d5pmi22dr6s5m5b6ixhfqwjn59yg1lsh6kkcn") (y #t)))

(define-public crate-inline-xml-0.2.2 (c (n "inline-xml") (v "0.2.2") (d (list (d (n "inline-xml-macros") (r "=0.1.1") (d #t) (k 0)))) (h "1bdhkq8hva1vqfjj2vx1lywwc7lw09jqlnbjfbnl9imfbxw4pa1c")))

(define-public crate-inline-xml-0.2.3 (c (n "inline-xml") (v "0.2.3") (d (list (d (n "inline-xml-macros") (r "=0.1.2") (d #t) (k 0)))) (h "0473h4qfs276yaw54akl6rz36lbjavbpwx0v77ccg19af5gqsrqd")))

(define-public crate-inline-xml-0.2.4 (c (n "inline-xml") (v "0.2.4") (d (list (d (n "inline-xml-macros") (r "=0.1.3") (d #t) (k 0)))) (h "0rkx45bjljqx73kqvqhwz2cnrhka74aw68zahp5cap9zbpi14l96")))

(define-public crate-inline-xml-0.2.5 (c (n "inline-xml") (v "0.2.5") (d (list (d (n "inline-xml-macros") (r "=0.1.3") (d #t) (k 0)))) (h "19qdqf7fmp1izyvg4k1470rsbz9j5yaf2jm9qzvbrh8nv7a6iyg6")))

(define-public crate-inline-xml-0.3.0 (c (n "inline-xml") (v "0.3.0") (d (list (d (n "inline-xml-macros") (r "=0.1.4") (d #t) (k 0)))) (h "04hi75hrwmjl9ch5c6zzis537q5pcspab7kq4myfb1kz58vzgxai")))

(define-public crate-inline-xml-0.3.1 (c (n "inline-xml") (v "0.3.1") (d (list (d (n "inline-xml-macros") (r "=0.1.5") (d #t) (k 0)))) (h "0hl95j9z0c1qaqsi7sfyw1w77rqfrc0sy5g47pr967nd687lfx3y")))

(define-public crate-inline-xml-0.3.2 (c (n "inline-xml") (v "0.3.2") (d (list (d (n "inline-xml-macros") (r "=0.2.0") (d #t) (k 0)))) (h "0j7d3wmmba73l6hnrp03b3768k83qc21xghmn638xjnsdxb239qc")))

