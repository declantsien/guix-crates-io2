(define-module (crates-io in li inline-css) #:use-module (crates-io))

(define-public crate-inline-css-0.0.1 (c (n "inline-css") (v "0.0.1") (d (list (d (n "inline-css-macros") (r "=0.0.1") (d #t) (k 0)) (d (n "inline-xml") (r "^0.3.1") (o #t) (d #t) (k 0)))) (h "0nzi6clnv77x3yrgfhq3rdzhk7njqcv0r5firk2zi7issq9zff87") (s 2) (e (quote (("inline-xml" "dep:inline-xml"))))))

(define-public crate-inline-css-0.0.2 (c (n "inline-css") (v "0.0.2") (d (list (d (n "inline-css-macros") (r "=0.0.1") (d #t) (k 0)) (d (n "inline-xml") (r "^0.3.1") (o #t) (d #t) (k 0)))) (h "182hgn42c98xakr3hhdjrvcsdg675fvi4x9lmfnpp09pp4jlbknz") (s 2) (e (quote (("inline-xml" "dep:inline-xml"))))))

(define-public crate-inline-css-0.0.3 (c (n "inline-css") (v "0.0.3") (d (list (d (n "inline-css-macros") (r "=0.0.2") (d #t) (k 0)) (d (n "inline-xml") (r "^0.3.1") (o #t) (d #t) (k 0)))) (h "0ia4ww7r9zsqp049j0wqx5yfxx07wanwj8vnibwgqi13d0dgx3b3") (s 2) (e (quote (("inline-xml" "dep:inline-xml"))))))

