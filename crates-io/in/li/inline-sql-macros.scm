(define-module (crates-io in li inline-sql-macros) #:use-module (crates-io))

(define-public crate-inline-sql-macros-0.1.0 (c (n "inline-sql-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "0a9dnl7pmq8pm413bc2cfrj7v6pq746ypajlqqa30184i4shmghx")))

(define-public crate-inline-sql-macros-0.1.1 (c (n "inline-sql-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "1m8inpxcijqhn8y8mmr2as55dyi07mnd2zv1jimwhhm0rk605qzz")))

(define-public crate-inline-sql-macros-0.1.2 (c (n "inline-sql-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "00ws2s89mq1hgs8rb60qd6g921giqxgrsqa016zf604pjayqmd01")))

(define-public crate-inline-sql-macros-0.1.3 (c (n "inline-sql-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "0gbds57ady00zv3lk3rak1y5gvlijgr6c6xay2vnqa8f64sai7hv")))

(define-public crate-inline-sql-macros-0.1.4 (c (n "inline-sql-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "02300xzs9w6ndb7s8h7hpkm8b27sfsha990pa0s9zhax79ryamzz")))

(define-public crate-inline-sql-macros-0.2.0 (c (n "inline-sql-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "13m5bnk9yiyf3zxnpavxy1ac9nn7lv3ykhmn4npmfxi56plkhq0j")))

