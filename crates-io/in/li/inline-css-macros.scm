(define-module (crates-io in li inline-css-macros) #:use-module (crates-io))

(define-public crate-inline-css-macros-0.0.1 (c (n "inline-css-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "01cv849xp6wfcw195m3smaxdllmhpbawls6cqwbn2mf6g75k9l3r")))

(define-public crate-inline-css-macros-0.0.2 (c (n "inline-css-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0n3nm6l4pss4lvjxl4ng4k5vkxnxfryrrbzaakfndqnqml3fir28")))

