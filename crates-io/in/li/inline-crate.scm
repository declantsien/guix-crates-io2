(define-module (crates-io in li inline-crate) #:use-module (crates-io))

(define-public crate-inline-crate-0.1.0 (c (n "inline-crate") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)))) (h "0kbyqdcivmkl13p1g4h80dmxchj1z10qlmc135qh3m0isb3ikmfk")))

