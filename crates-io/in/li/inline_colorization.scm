(define-module (crates-io in li inline_colorization) #:use-module (crates-io))

(define-public crate-inline_colorization-0.1.0 (c (n "inline_colorization") (v "0.1.0") (h "0i4w1r3x8brp5qhcxr8kd1n9cni4x63ai60jcbml0cl3v0l10yzi")))

(define-public crate-inline_colorization-0.1.1 (c (n "inline_colorization") (v "0.1.1") (h "0jiknif9gsjip3bs38qf7z9wfw6jxhrcpwfgpsy8vimydzx0sz2d")))

(define-public crate-inline_colorization-0.1.2 (c (n "inline_colorization") (v "0.1.2") (h "0sh73lc9dmrp4sg23nd0g3xzz8akbnr78k7qq7gp09601kksk966")))

(define-public crate-inline_colorization-0.1.3 (c (n "inline_colorization") (v "0.1.3") (h "1xskwnzasp24585j0n9kg58365wgys2ajqxsj00gdsq18bxcpf50")))

(define-public crate-inline_colorization-0.1.4 (c (n "inline_colorization") (v "0.1.4") (h "06x94n2yzjldymgncfi9by2l56srygvqnhqqc5hwkhl5cljsci2j")))

(define-public crate-inline_colorization-0.1.5 (c (n "inline_colorization") (v "0.1.5") (h "18c7x1cqr6k6xl6n7nz4kxicjac1711impnzl7wrsm88gs2n9qlz")))

(define-public crate-inline_colorization-0.1.6 (c (n "inline_colorization") (v "0.1.6") (h "12prx6rwwgknivhz3a4p1c5jnkmqm1rp4000n9c4fy59nsyh866w")))

