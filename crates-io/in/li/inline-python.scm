(define-module (crates-io in li inline-python) #:use-module (crates-io))

(define-public crate-inline-python-0.1.0 (c (n "inline-python") (v "0.1.0") (d (list (d (n "inline-python-macros") (r "^0.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.6") (d #t) (k 0)))) (h "12xaaq82ikbg7fhhg69ynl9cd0mc23vadnp80ly38l69azpq4dnd")))

(define-public crate-inline-python-0.2.0 (c (n "inline-python") (v "0.2.0") (d (list (d (n "inline-python-macros") (r "= 0.2.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.6") (d #t) (k 0)))) (h "17r6pr9wmglnrd4rsmjq675g7f0i2czyzshf63q6ilkk3brsqd0w")))

(define-public crate-inline-python-0.3.0 (c (n "inline-python") (v "0.3.0") (d (list (d (n "inline-python-macros") (r "= 0.3.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.6") (d #t) (k 0)))) (h "12jljzq6hv7v6yg83xhkf9sfnpzr98nkm46yvj1k17hpdb75mng9")))

(define-public crate-inline-python-0.3.1 (c (n "inline-python") (v "0.3.1") (d (list (d (n "inline-python-macros") (r "= 0.3.1") (d #t) (k 0)) (d (n "pyo3") (r ">= 0.6, < 0.8") (d #t) (k 0)))) (h "1jfwg8pa3b040gygw1vwiq76i88zmy53mn1ygrzvwldbdcqq2p08")))

(define-public crate-inline-python-0.3.2 (c (n "inline-python") (v "0.3.2") (d (list (d (n "inline-python-macros") (r "= 0.3.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.1") (d #t) (k 0)))) (h "1p7k5qv8giz2sdcaqgmfv7dlw1zfwcrwfwkb43fr8d4cnj468mks")))

(define-public crate-inline-python-0.3.3 (c (n "inline-python") (v "0.3.3") (d (list (d (n "inline-python-macros") (r "= 0.3.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.1") (d #t) (k 0)))) (h "0qn61yw2a3sh4xvw524ql13qqpxras6lgz9b9qj4d2igcc968s3w")))

(define-public crate-inline-python-0.4.0 (c (n "inline-python") (v "0.4.0") (d (list (d (n "inline-python-macros") (r "= 0.3.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.1") (d #t) (k 0)))) (h "10gzi66q0iqf6l2avmzsbf0hghd2bw2049kn715mrsja58aw53q4")))

(define-public crate-inline-python-0.4.1 (c (n "inline-python") (v "0.4.1") (d (list (d (n "inline-python-macros") (r "= 0.4.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.9.0") (d #t) (k 0)))) (h "1mk2vhijm0w4a6ah1fa56gbr9jhigvrg36lrig57fln15yv6by3l")))

(define-public crate-inline-python-0.5.0 (c (n "inline-python") (v "0.5.0") (d (list (d (n "inline-python-macros") (r "= 0.5.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.9.0") (d #t) (k 0)))) (h "1xhlk9mck2nn1rvj09z5qly61rx2330qxdrviy8y5c78x1n4zgx6")))

(define-public crate-inline-python-0.5.1 (c (n "inline-python") (v "0.5.1") (d (list (d (n "inline-python-macros") (r "= 0.5.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.9.0") (d #t) (k 0)))) (h "18fvhjsbrsp6bdkrmyjgsjzw8zd48815941b40x3874zsn48gi30")))

(define-public crate-inline-python-0.5.3 (c (n "inline-python") (v "0.5.3") (d (list (d (n "inline-python-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.10.0") (k 0)))) (h "1pvsm275kwnl9ggvf3fa1667xqrbhs7skzscws40sfldmj8ckawj")))

(define-public crate-inline-python-0.6.0 (c (n "inline-python") (v "0.6.0") (d (list (d (n "inline-python-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.11.0") (k 0)))) (h "1k5dg1pr0lq0wc0nm4r66mvcbgr2vi2kdjs8qwf2z2fkf9azhh3w")))

(define-public crate-inline-python-0.7.0 (c (n "inline-python") (v "0.7.0") (d (list (d (n "inline-python-macros") (r "=0.7.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.14.0") (f (quote ("auto-initialize"))) (k 0)))) (h "13bqz071lf267a0a85klgl7j92y44ha43h6wzr9wv8npm4jyfy61")))

(define-public crate-inline-python-0.7.1 (c (n "inline-python") (v "0.7.1") (d (list (d (n "inline-python-macros") (r "=0.7.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.14.0") (f (quote ("auto-initialize"))) (k 0)))) (h "16d2wy3mjaj0db2vzymr4idpizr8bda8xc2vc2q64jp11h3hl2g2")))

(define-public crate-inline-python-0.8.0 (c (n "inline-python") (v "0.8.0") (d (list (d (n "inline-python-macros") (r "=0.8.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.15.0") (f (quote ("auto-initialize"))) (k 0)))) (h "17m9244cybdvcvyz9a95vcq2i7k7lqzzmm2dflz4zr45am33hvff")))

(define-public crate-inline-python-0.10.0 (c (n "inline-python") (v "0.10.0") (d (list (d (n "inline-python-macros") (r "=0.10.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.17.0") (f (quote ("auto-initialize"))) (k 0)))) (h "03cmv0v127fdbypg5arpf5vzcyk7dcnwwnml7v01pmcacvxvf7r1")))

(define-public crate-inline-python-0.11.0 (c (n "inline-python") (v "0.11.0") (d (list (d (n "inline-python-macros") (r "=0.11.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.19") (f (quote ("auto-initialize"))) (k 0)))) (h "0d00p1grzq7pi0yrwkchnzmxvilm92bdif1ii261vlwx05fa4xxz")))

(define-public crate-inline-python-0.12.0 (c (n "inline-python") (v "0.12.0") (d (list (d (n "inline-python-macros") (r "=0.12.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.19") (f (quote ("auto-initialize"))) (k 0)))) (h "04wmd3ypc069s9jd4nfzh13wx9gb1r5fbdj7fjgrqcwijj3hdqpr")))

