(define-module (crates-io in li inline-const) #:use-module (crates-io))

(define-public crate-inline-const-0.0.1 (c (n "inline-const") (v "0.0.1") (h "02b4qv98s4iwrblm3nmj7lbxbjh3s84nh4g9s35q1mp6cbalv1fb")))

(define-public crate-inline-const-0.1.0 (c (n "inline-const") (v "0.1.0") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)))) (h "1rkcz3wq8mx99j1p6z9zdlbqzjgp6q8c287yimcf2i9g6m4d13rs")))

