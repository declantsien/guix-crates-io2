(define-module (crates-io in li inline_tweak) #:use-module (crates-io))

(define-public crate-inline_tweak-1.0.0 (c (n "inline_tweak") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0d6dqvkikzw1sz9xvxx26v9h3l87awmin1pl96kqmjb34dlg7xs7")))

(define-public crate-inline_tweak-1.0.1 (c (n "inline_tweak") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "118zyi0sd01y5s9pi149dkyfh4bvmcvvdlj6230c3v8lbz8id4gz")))

(define-public crate-inline_tweak-1.0.2 (c (n "inline_tweak") (v "1.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1v1qbxl4pg3wxz4r1c77v41ymdkvzylyawr9i8399xkw693g8vmi")))

(define-public crate-inline_tweak-1.0.3 (c (n "inline_tweak") (v "1.0.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0hdj3vhwblym0mw36ncbwcq8bgb97bwkgfr4abxa7mfq783svz33") (y #t)))

(define-public crate-inline_tweak-1.0.4 (c (n "inline_tweak") (v "1.0.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "06w8cv174mvjbzw4sxvfyv2gav6aykcg82jdyvvsjlkqx5nch32l")))

(define-public crate-inline_tweak-1.0.5 (c (n "inline_tweak") (v "1.0.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1g8qrirnc3cpp3sw08psiayp7bcw4j8a2zxw3yqwca980hj96xig")))

(define-public crate-inline_tweak-1.0.6 (c (n "inline_tweak") (v "1.0.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1bwbp1gvp4fnjlvlz20mrq7v1jzahmyv3q4xh6g80dlyjhc3milz")))

(define-public crate-inline_tweak-1.0.7 (c (n "inline_tweak") (v "1.0.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "09crybknjarzdc9lzsksdwgv2gz9asx9f0j4z68gn51cy9dw7fr8") (f (quote (("release_tweak") ("default"))))))

(define-public crate-inline_tweak-1.0.8 (c (n "inline_tweak") (v "1.0.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0jzb01nhclqy8b3iyp9hs84gy6bpz901jv928g8c0z1741xyjcvh") (f (quote (("release_tweak") ("default"))))))

(define-public crate-inline_tweak-1.0.9 (c (n "inline_tweak") (v "1.0.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1z7ssjl1zlgaq827j4f1q8sa9bdbvc44z2absjki6ylkbc8i8mxv") (f (quote (("release_tweak") ("default"))))))

(define-public crate-inline_tweak-1.0.10 (c (n "inline_tweak") (v "1.0.10") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1fl91l0fm7kgcw5hscipsxck3mjidjiz8ryn5brb2fxiq4zgk7ah") (f (quote (("release_tweak") ("default"))))))

(define-public crate-inline_tweak-1.0.11 (c (n "inline_tweak") (v "1.0.11") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1prffm6klgqamvb3rk0bzz9lz629lwb07vhiw2gzizpgnrb59x8a") (f (quote (("release_tweak") ("default"))))))

(define-public crate-inline_tweak-1.0.12 (c (n "inline_tweak") (v "1.0.12") (d (list (d (n "inline_tweak_derive") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (o #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "full" "visit"))) (o #t) (k 0)))) (h "14qj5fbcwnfz95kihg2bd2v779jyqpd1gk5g8sv0cp046i52qhxa") (f (quote (("release_tweak" "inline_tweak_derive/release_tweak") ("default")))) (s 2) (e (quote (("derive" "dep:syn" "dep:proc-macro2" "dep:inline_tweak_derive"))))))

(define-public crate-inline_tweak-1.0.13 (c (n "inline_tweak") (v "1.0.13") (d (list (d (n "inline_tweak_derive") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (o #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "full" "visit"))) (o #t) (k 0)))) (h "1n197z305zqsh5wcwdv9wlk339s6447fz0yznhrwncn7sgpmiyjd") (f (quote (("release_tweak") ("default")))) (s 2) (e (quote (("derive" "dep:syn" "dep:proc-macro2" "dep:inline_tweak_derive"))))))

(define-public crate-inline_tweak-1.0.14 (c (n "inline_tweak") (v "1.0.14") (d (list (d (n "inline_tweak_derive") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (o #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "full" "visit"))) (o #t) (k 0)))) (h "017iaj32gbz5pjd15npidfi7kay76gvzi6xcyrs8g2nd3yldn4vd") (f (quote (("release_tweak") ("default")))) (s 2) (e (quote (("derive" "dep:syn" "dep:proc-macro2" "dep:inline_tweak_derive"))))))

(define-public crate-inline_tweak-1.0.15 (c (n "inline_tweak") (v "1.0.15") (d (list (d (n "inline_tweak_derive") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (o #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "full" "visit"))) (o #t) (k 0)))) (h "1bbfx738vla0w53xly3bx0flpdgszjn7sh5v87zcqj8k55bp7rgp") (f (quote (("release_tweak") ("default")))) (s 2) (e (quote (("derive" "dep:syn" "dep:proc-macro2" "dep:inline_tweak_derive"))))))

(define-public crate-inline_tweak-1.0.16 (c (n "inline_tweak") (v "1.0.16") (d (list (d (n "inline_tweak_derive") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (o #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "full" "visit"))) (o #t) (k 0)))) (h "0jvcdpfd1n1j3004mqf1lmgcwl0k3gl5v6i33ni5hza0b9xf5c6z") (f (quote (("release_tweak") ("default")))) (s 2) (e (quote (("derive" "dep:syn" "dep:proc-macro2" "dep:inline_tweak_derive"))))))

(define-public crate-inline_tweak-1.1.0 (c (n "inline_tweak") (v "1.1.0") (d (list (d (n "inline_tweak_derive") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (o #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "full" "visit"))) (o #t) (k 0)))) (h "1vr7zf5nmknkhl55r5bh8dplb85nlrxicla48c115brjmy0pbddp") (f (quote (("release_tweak") ("default")))) (s 2) (e (quote (("derive" "dep:syn" "dep:proc-macro2" "dep:inline_tweak_derive"))))))

(define-public crate-inline_tweak-1.1.1 (c (n "inline_tweak") (v "1.1.1") (d (list (d (n "inline_tweak_derive") (r "^3.0.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (o #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "full" "visit"))) (o #t) (k 0)))) (h "0psydj8rpfl5pw131gw4blv5rwwiyh9mbx977skzp2z0zazdvb66") (f (quote (("release_tweak") ("default")))) (s 2) (e (quote (("derive" "dep:syn" "dep:proc-macro2" "dep:inline_tweak_derive"))))))

