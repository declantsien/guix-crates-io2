(define-module (crates-io in li inline_format) #:use-module (crates-io))

(define-public crate-inline_format-0.1.0 (c (n "inline_format") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mfjczfi5vr14w2yx56msjpy0p7b5wqzvxpdq6hj13xqmh6sxg0h")))

(define-public crate-inline_format-0.2.1 (c (n "inline_format") (v "0.2.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13gkb6kwjs3i3m1gik9fq76k3b1smis28rqhimi3a28d3py1kkb7")))

(define-public crate-inline_format-0.2.2 (c (n "inline_format") (v "0.2.2") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15ir4gfl1sssw9y60b0h1sx4bvxhh1905nxlhziin69l7vjhgain")))

(define-public crate-inline_format-0.2.3 (c (n "inline_format") (v "0.2.3") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lhqrl90a4nr3yf6ijdsq10cbj8xfm5frcnval269fhf4nrm7ydc")))

