(define-module (crates-io in li inline) #:use-module (crates-io))

(define-public crate-inline-0.0.0-- (c (n "inline") (v "0.0.0--") (h "1s59h4h0libl5blf9x9cnq4qpfyrw0sy81h5j64kk5qics9zdh5x") (y #t)))

(define-public crate-inline-0.0.0-r0 (c (n "inline") (v "0.0.0-r0") (h "0dgi6qqbajn0wb1k07ngfbmw5s405r4vhjhgb7fk45h6mwnsjpxa")))

