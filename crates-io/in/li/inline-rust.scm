(define-module (crates-io in li inline-rust) #:use-module (crates-io))

(define-public crate-inline-rust-1.0.0 (c (n "inline-rust") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wxb781w5gl16y61r7yam5gj6swaarlx4c1knn6n66zphdwk5yqj")))

