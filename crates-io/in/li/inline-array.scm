(define-module (crates-io in li inline-array) #:use-module (crates-io))

(define-public crate-inline-array-0.1.0 (c (n "inline-array") (v "0.1.0") (d (list (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "1vnrwz6s32cy6fii844ifzh6499rzp282mpkdyc7fr5d8sfsh45m")))

(define-public crate-inline-array-0.1.1 (c (n "inline-array") (v "0.1.1") (d (list (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "0dyy4p0669i24284nxrd3sx8dy92d0b715zf1ixb8bjziam3p7yx")))

(define-public crate-inline-array-0.1.2 (c (n "inline-array") (v "0.1.2") (d (list (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "1bj4prglih24rzvvymvg46xjy07xmhn5w67mhp6jga345xb4zyhg")))

(define-public crate-inline-array-0.1.4 (c (n "inline-array") (v "0.1.4") (d (list (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "0sqmmgm2x59g3m9bljvir73dbxnq4d3b81wnxf4l2lmbp1f88n66")))

(define-public crate-inline-array-0.1.6 (c (n "inline-array") (v "0.1.6") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.171") (o #t) (d #t) (k 0)))) (h "0f82qifkifl4ib3j8r8j8rfasqj8r0fplpc9350apr4r2ygcfkif") (f (quote (("fake_32_bit"))))))

(define-public crate-inline-array-0.1.8 (c (n "inline-array") (v "0.1.8") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.171") (o #t) (d #t) (k 0)))) (h "0175s8a7ydbnqpj2yx90y90q4bax2im7sqy090p43myxnpf9m921") (f (quote (("fake_32_bit"))))))

(define-public crate-inline-array-0.1.9 (c (n "inline-array") (v "0.1.9") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "concurrent-map") (r "^5.0") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0nsv2fmzrvjy35ckawmmzdrmk7ramddf1z6vgf1c8vjphhnqhasd") (f (quote (("fake_32_bit") ("concurrent_map_minimum" "concurrent-map"))))))

(define-public crate-inline-array-0.1.10 (c (n "inline-array") (v "0.1.10") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "concurrent-map") (r "^5.0") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0dj6fxzkazr7674fk0i04gav4zbdcph1gcnbcahwqn34hfkdy8yh") (f (quote (("fake_32_bit") ("concurrent_map_minimum" "concurrent-map"))))))

(define-public crate-inline-array-0.1.11 (c (n "inline-array") (v "0.1.11") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "concurrent-map") (r "^5.0") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1lfwg8lmsp5v0nfbbpibr4182azinrkprn2pb0r6nj6xyc9h3pcq") (f (quote (("fake_32_bit") ("concurrent_map_minimum" "concurrent-map"))))))

(define-public crate-inline-array-0.1.12 (c (n "inline-array") (v "0.1.12") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "concurrent-map") (r "^5.0") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0ppv2cmqmk5qw3n8b5fqqawisp3xf2njhl0ly9ckl6b707q6hqp8") (f (quote (("fake_32_bit") ("concurrent_map_minimum" "concurrent-map"))))))

