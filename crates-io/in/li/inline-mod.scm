(define-module (crates-io in li inline-mod) #:use-module (crates-io))

(define-public crate-inline-mod-0.0.1 (c (n "inline-mod") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0axadka29lzkz0w9qjk3kilpxfb4kg1n96zjdqx190794ffirmyi") (f (quote (("docs") ("default"))))))

(define-public crate-inline-mod-0.0.2 (c (n "inline-mod") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0v7dpajflyj1822vdxwz0lggld541jfq0ax5mxpgci5rsa6m2skk") (f (quote (("docs") ("default"))))))

