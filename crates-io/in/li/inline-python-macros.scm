(define-module (crates-io in li inline-python-macros) #:use-module (crates-io))

(define-public crate-inline-python-macros-0.1.0 (c (n "inline-python-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0gbvmv1yca7c15is41kalbyirzwkf31gyqw7rdgzgn0v8zf9n9v0")))

(define-public crate-inline-python-macros-0.2.0 (c (n "inline-python-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1345zi33nl4hsmp1dmj9f6zqg0g17rfzppqcp8lprr7biq98vr5i")))

(define-public crate-inline-python-macros-0.3.0 (c (n "inline-python-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.6") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0g11v72qqgip04p28q0bfhnyps8i860kmw6r7gh5j34fwsax9w2y")))

(define-public crate-inline-python-macros-0.3.1 (c (n "inline-python-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "pyo3") (r ">= 0.6, < 0.8") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1gfh8sp4jk1wi5108nzbknaqwkmvd1m1085fqlkh7w2wpyrnjx55")))

(define-public crate-inline-python-macros-0.3.2 (c (n "inline-python-macros") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.8.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1rmnhfygj0bvp8kq2c6ihwxkafnl27crjpivyd9za8dc0vl0f68a")))

(define-public crate-inline-python-macros-0.4.1 (c (n "inline-python-macros") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.9.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0r9dw18vq96j3y22hwxr9nvlc48hszjcjny6698rl3r50capkj6i")))

(define-public crate-inline-python-macros-0.5.0 (c (n "inline-python-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.9.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1drnkxq0sz0jhpca20i4ri2zhd6mx24rqlkbzrwsjnbbrr27ph0p")))

(define-public crate-inline-python-macros-0.5.1 (c (n "inline-python-macros") (v "0.5.1") (d (list (d (n "libc") (r "^0.2.71") (d #t) (t "cfg(unix)") (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.10.0") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1a5pmh4njjksmadvlh5cjsd5npklnk4xh6bsz70mybya2xyvi5h6")))

(define-public crate-inline-python-macros-0.5.2 (c (n "inline-python-macros") (v "0.5.2") (d (list (d (n "libc") (r "^0.2.71") (d #t) (t "cfg(unix)") (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.11.0") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1hfl03sysag44j2mzg91rpwmglbnmb8cx91vilfrfxvdf3v1j8ba")))

(define-public crate-inline-python-macros-0.5.3 (c (n "inline-python-macros") (v "0.5.3") (d (list (d (n "libc") (r "^0.2.71") (d #t) (t "cfg(unix)") (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.11.0") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1h67l60l9328zjk31f8ha4pxh5pzgcibqji0fylxyv5s2rvwb3c5")))

(define-public crate-inline-python-macros-0.7.0 (c (n "inline-python-macros") (v "0.7.0") (d (list (d (n "libc") (r "^0.2.71") (d #t) (t "cfg(unix)") (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.14.0") (f (quote ("auto-initialize"))) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1g0h4sapgank0vlj472s8ihicwi1qkc2cp9c5a5mk6d39jhw8mki")))

(define-public crate-inline-python-macros-0.7.1 (c (n "inline-python-macros") (v "0.7.1") (d (list (d (n "libc") (r "^0.2.71") (d #t) (t "cfg(unix)") (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.14.0") (f (quote ("auto-initialize"))) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1z7s2wm1k4376m1mbc7dfnzq1wys7029iww48593klr8fcmmhwpp")))

(define-public crate-inline-python-macros-0.8.0 (c (n "inline-python-macros") (v "0.8.0") (d (list (d (n "libc") (r "^0.2.71") (d #t) (t "cfg(unix)") (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.15.0") (f (quote ("auto-initialize"))) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0rqi7yw7vjlvh3cpjwhb9pwwzj411xz9787hfgb4qv10mnvc5gp3")))

(define-public crate-inline-python-macros-0.10.0 (c (n "inline-python-macros") (v "0.10.0") (d (list (d (n "libc") (r "^0.2.71") (d #t) (t "cfg(unix)") (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.17.0") (f (quote ("auto-initialize"))) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0k4l6wmzga21d053rzgapmsjlgiy787c3s7xcf055q35szrss4bw")))

(define-public crate-inline-python-macros-0.11.0 (c (n "inline-python-macros") (v "0.11.0") (d (list (d (n "libc") (r "^0.2.71") (d #t) (t "cfg(unix)") (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.19") (f (quote ("auto-initialize"))) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "13d6n5prf4vkxkgrlsczx7n1zxgkjnj89yykb732xm9sw08dhwpr")))

(define-public crate-inline-python-macros-0.12.0 (c (n "inline-python-macros") (v "0.12.0") (d (list (d (n "libc") (r "^0.2.71") (d #t) (t "cfg(unix)") (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.19") (f (quote ("auto-initialize"))) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "07lhwv9cxygd9j0235nkbikmck4m32isvxg63zl6b7lmbydn2fim")))

