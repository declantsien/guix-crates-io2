(define-module (crates-io in li inline-vbs-macros) #:use-module (crates-io))

(define-public crate-inline-vbs-macros-0.1.0 (c (n "inline-vbs-macros") (v "0.1.0") (d (list (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)))) (h "1krcgkk4cfmg39amws5v78k4qfy6kaf73ybsqjif8ilm6m9560lr")))

(define-public crate-inline-vbs-macros-0.2.0 (c (n "inline-vbs-macros") (v "0.2.0") (d (list (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)))) (h "1jbabkw5wwsdq2q3kkz04zpxbg9z2v5dsvqm3skm47k8lczfs4ff")))

(define-public crate-inline-vbs-macros-0.4.0 (c (n "inline-vbs-macros") (v "0.4.0") (d (list (d (n "litrs") (r "^0.4.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)))) (h "1w6d4s4vn27rl3kliicrvnxrs186s3sc8y5nkv7n068qbxlii6z9")))

