(define-module (crates-io in li inline-c-macro) #:use-module (crates-io))

(define-public crate-inline-c-macro-0.1.0 (c (n "inline-c-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0bdzvk4ap2i82h89m27pg89aglz1winacn2mnm3c9zyjvjzpwh8q")))

(define-public crate-inline-c-macro-0.1.5 (c (n "inline-c-macro") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.3") (d #t) (k 1)))) (h "05dfhfjmid31vybkycs4zmr9rhpmck02ca3w3n4dmb5dqwg65x8p")))

