(define-module (crates-io in li inlined) #:use-module (crates-io))

(define-public crate-inlined-0.1.0 (c (n "inlined") (v "0.1.0") (h "1d8mpqxzjmfk9yib658h6gc2m9qp16z0qszhv2nv2kb1dkchhdsi") (y #t) (r "1.77.0")))

(define-public crate-inlined-0.1.1 (c (n "inlined") (v "0.1.1") (h "1cas391agwlkm2zaw2sggkj5rqr5zyq361b2nxl9ph4wld0v15hl") (r "1.77.0")))

