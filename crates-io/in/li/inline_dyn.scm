(define-module (crates-io in li inline_dyn) #:use-module (crates-io))

(define-public crate-inline_dyn-0.1.0 (c (n "inline_dyn") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "typenum") (r "^1.11") (d #t) (k 0)))) (h "0iw9icbqxk3shzb49i2kjlkcavjd5w6wrh1v9qmqklbgpng7lhl1") (f (quote (("std" "alloc") ("nightly") ("alloc"))))))

(define-public crate-inline_dyn-0.1.1 (c (n "inline_dyn") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "typenum") (r "^1.11") (d #t) (k 0)))) (h "0wqxcssx07l40li4wc9pwd2v8qc6z2myylyzfry9yrzmbg0734lj") (f (quote (("std" "alloc") ("nightly") ("alloc"))))))

(define-public crate-inline_dyn-0.2.0 (c (n "inline_dyn") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "elain") (r "^0.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "0i31j0xm7w9knk32797bfn0avrk1lp44r0a05s1pv7rvvfbm3qik") (f (quote (("std" "alloc") ("nightly") ("alloc"))))))

(define-public crate-inline_dyn-0.2.1 (c (n "inline_dyn") (v "0.2.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "elain") (r "^0.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "1rmcqab5vzjdlqlhxzfd1204qbkdn54mn6wy28l4h5mv3rkwdf6x") (f (quote (("std" "alloc") ("nightly") ("alloc"))))))

