(define-module (crates-io in li inline_newtype) #:use-module (crates-io))

(define-public crate-inline_newtype-0.1.0 (c (n "inline_newtype") (v "0.1.0") (h "1a1dp9a20naq0gsd4qfvd5dlwg5fa3zva3m7apb25kddvf2z6vj4")))

(define-public crate-inline_newtype-0.1.1 (c (n "inline_newtype") (v "0.1.1") (h "1p6w6jv42lahhqgcnbci9kl175czd387dfrgyk2whxpr3kz434gj")))

