(define-module (crates-io in li inline_assets) #:use-module (crates-io))

(define-public crate-inline_assets-0.1.1 (c (n "inline_assets") (v "0.1.1") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 2)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)))) (h "153ml2wxnw8l1mgahkxgwa69yxm7zlann1z5hkh7ppvs01hkl9cb") (y #t)))

(define-public crate-inline_assets-0.1.2 (c (n "inline_assets") (v "0.1.2") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 2)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)))) (h "1zwnfmynyn6m8j8901l1w3wn0fsvmia0mghjid5h6rkzm6555wpz") (y #t)))

(define-public crate-inline_assets-0.1.3 (c (n "inline_assets") (v "0.1.3") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 2)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)))) (h "0k0w52rkircpzhn2fxksma9i8m3z4yzf7jyql67gcg8dj2zijig8") (y #t)))

(define-public crate-inline_assets-0.1.4 (c (n "inline_assets") (v "0.1.4") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 2)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)))) (h "1y56y92l0damhswh8lvw2jli92symdj2ipkamp9xy70wdvn657pf")))

(define-public crate-inline_assets-0.2.0 (c (n "inline_assets") (v "0.2.0") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 2)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)))) (h "1j3habk39xcc3jv1mnmnq7v7aq4zj7yfc11sbkymlkgp7in07bdm")))

(define-public crate-inline_assets-0.2.1 (c (n "inline_assets") (v "0.2.1") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 2)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)))) (h "1r1b3w7kfh2cn55jwkkyb8hv120b6366zh8vkgg5ghhzkf6qxbjl")))

(define-public crate-inline_assets-0.3.0 (c (n "inline_assets") (v "0.3.0") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)))) (h "0lkfl5c2jp1p7b9xdrz4w0553vi6w9ndir78181xkm5nd0n3wzzm")))

(define-public crate-inline_assets-0.3.1 (c (n "inline_assets") (v "0.3.1") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "regex") (r "^1.0.1") (d #t) (k 0)))) (h "1yyx3n4arx3vhkyp9x4fn7alk0c9346n97i6q5gz1dwn3c8ljb8z")))

(define-public crate-inline_assets-0.4.1 (c (n "inline_assets") (v "0.4.1") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "html5ever") (r "^0.23.0") (d #t) (k 0)) (d (n "insta") (r "^0.10.1") (d #t) (k 2)) (d (n "kuchiki") (r "^0.7.3") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "0a3xw1i1vgnvpbs2533fkkiqyzjwxwk64f1hwksrnx24fr9y70b3")))

(define-public crate-inline_assets-0.5.0 (c (n "inline_assets") (v "0.5.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "html5ever") (r "^0.25.1") (d #t) (k 0)) (d (n "insta") (r "^1.12.0") (d #t) (k 2)) (d (n "kuchiki") (r "^0.8.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0bd2k4ahrj7gslqcd36hklwiqynqqslm2zzdxszl1xb8d011qz3x")))

