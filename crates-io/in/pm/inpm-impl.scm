(define-module (crates-io in pm inpm-impl) #:use-module (crates-io))

(define-public crate-inpm-impl-0.1.0 (c (n "inpm-impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0kbvq33cdr6fnq766wjjmg23hycqbjhg4d04j7wfc39v6791bm5x") (f (quote (("embed"))))))

(define-public crate-inpm-impl-0.1.1 (c (n "inpm-impl") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0210h24m21n6sfcx5b57j1szymk0x1wa6xp2vljl5q23zvdswfj9") (f (quote (("embed"))))))

(define-public crate-inpm-impl-0.2.0 (c (n "inpm-impl") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1bsxcf0kq3i4w2f7ifjbd6j4lkbnpz8nxcbmw46fcmgica7zbd1n") (f (quote (("embed")))) (y #t)))

(define-public crate-inpm-impl-0.2.1 (c (n "inpm-impl") (v "0.2.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "13rz0cc2q5xybkyy5znyz9f3a5gcmhy8cmyq9fc321bnxikbsy83") (f (quote (("embed"))))))

