(define-module (crates-io in pm inpm) #:use-module (crates-io))

(define-public crate-inpm-0.1.0 (c (n "inpm") (v "0.1.0") (d (list (d (n "inpm-impl") (r "^0.1") (d #t) (k 0)) (d (n "mime_guess") (r "^2") (o #t) (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "warpd") (r "^0.2") (o #t) (d #t) (k 0) (p "warp")))) (h "0m4y4dy3z5b4n8lg3zk8yybbfsbmr2v5fsyajfrdlhca9xw2fn69") (f (quote (("warp" "warpd" "mime_guess") ("embed" "inpm-impl/embed"))))))

(define-public crate-inpm-0.1.1 (c (n "inpm") (v "0.1.1") (d (list (d (n "inpm-impl") (r "^0.1") (d #t) (k 0)) (d (n "mime_guess") (r "^2") (o #t) (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "warpd") (r "^0.2") (o #t) (d #t) (k 0) (p "warp")))) (h "16d7pcvr3h30x2bxsiwq4cmzmzqb2439lxfkigxq25r3k2niyp93") (f (quote (("warp" "warpd" "mime_guess") ("embed" "inpm-impl/embed"))))))

(define-public crate-inpm-0.2.0 (c (n "inpm") (v "0.2.0") (d (list (d (n "inpm-impl") (r "^0.2") (d #t) (k 0)) (d (n "mime_guess") (r "^2") (o #t) (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)) (d (n "warpd") (r "^0.2") (o #t) (d #t) (k 0) (p "warp")))) (h "184hwq4xqnzl06103bg7dy0f3hcb0ida8w2hyd751a8rhf738qfq") (f (quote (("warp" "warpd" "mime_guess") ("embed" "inpm-impl/embed"))))))

