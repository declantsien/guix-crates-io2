(define-module (crates-io in k- ink-zombienet-prom-metrics-parser) #:use-module (crates-io))

(define-public crate-ink-zombienet-prom-metrics-parser-0.1.0-alpha.1 (c (n "ink-zombienet-prom-metrics-parser") (v "0.1.0-alpha.1") (d (list (d (n "pest") (r "^2.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1a3npy69g1552xi12qzr0jiv93iiy74phrch40hmxdzcgwjnnwbz") (r "1.70.0")))

