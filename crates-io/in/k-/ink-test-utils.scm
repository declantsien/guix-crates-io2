(define-module (crates-io in k- ink-test-utils) #:use-module (crates-io))

(define-public crate-ink-test-utils-0.1.0 (c (n "ink-test-utils") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)) (d (n "ink") (r "^4.0.0") (k 2)) (d (n "scale") (r "^3") (f (quote ("derive"))) (k 2) (p "parity-scale-codec")) (d (n "scale-info") (r "^2") (f (quote ("derive"))) (k 2)))) (h "1gl1v41b37zvr0dwy2xz4d8a9gr7vc54siprbrs79r00wzdb2vjh") (f (quote (("std" "ink/std" "scale/std" "scale-info/std") ("ink-as-dependency") ("default" "std")))) (y #t)))

