(define-module (crates-io in k- ink-zombienet-configuration) #:use-module (crates-io))

(define-public crate-ink-zombienet-configuration-0.1.0-alpha.1 (c (n "ink-zombienet-configuration") (v "0.1.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "multiaddr") (r "^0.18") (d #t) (k 0)) (d (n "regex") (r "^1.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "url") (r "^2.3") (f (quote ("serde"))) (d #t) (k 0)))) (h "1y3w0fsk1q1kg26vd6zvmli5a0daniqry4bm8j9722bwl2mfpl2w") (r "1.70.0")))

