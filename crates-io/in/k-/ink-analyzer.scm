(define-module (crates-io in k- ink-analyzer) #:use-module (crates-io))

(define-public crate-ink-analyzer-0.1.0 (c (n "ink-analyzer") (v "0.1.0") (d (list (d (n "ra_ap_syntax") (r "^0.0.149") (d #t) (k 0)))) (h "14vdr4kzasz6468k29cf3vmkswpn2yjfmjfh0qa01kdjsqf7igx3")))

(define-public crate-ink-analyzer-0.1.1 (c (n "ink-analyzer") (v "0.1.1") (d (list (d (n "ra_ap_syntax") (r "^0.0.149") (d #t) (k 0)))) (h "1s0jikflr5b40gkj4x6ymxcklab55cam9bdh2j5kyrxgfpn11jr0")))

(define-public crate-ink-analyzer-0.1.2 (c (n "ink-analyzer") (v "0.1.2") (d (list (d (n "ra_ap_syntax") (r "^0.0.149") (d #t) (k 0)))) (h "04m303m75kabmx3xswggnxfvgcxfg3ikxxhamx4s45pn2fx46ywh")))

(define-public crate-ink-analyzer-0.1.3 (c (n "ink-analyzer") (v "0.1.3") (d (list (d (n "ra_ap_syntax") (r "^0.0.149") (d #t) (k 0)))) (h "0amjnw4m85m47431xdz0iwbnyg7716g890dd9320azvksdp4jp35")))

(define-public crate-ink-analyzer-0.2.0 (c (n "ink-analyzer") (v "0.2.0") (d (list (d (n "ra_ap_syntax") (r "^0.0.149") (d #t) (k 0)))) (h "1n7inm4zvjhwcvnm81n7ng06h37g8r0rsgy2kh4bxnffcjrhq23c")))

(define-public crate-ink-analyzer-0.3.0 (c (n "ink-analyzer") (v "0.3.0") (d (list (d (n "ra_ap_syntax") (r "^0.0.149") (d #t) (k 0)))) (h "06h3iadixgziv13bdflfrxcad9x0rjhlzk1p6cs7dz7bq2pc9c6s")))

(define-public crate-ink-analyzer-0.4.0 (c (n "ink-analyzer") (v "0.4.0") (d (list (d (n "ink-analyzer-ir") (r "^0.1") (d #t) (k 0)))) (h "0g6kxy6zqz2an79ji836jx9mqh49yp8r0z9zs3qqbr6ck72rasm1")))

(define-public crate-ink-analyzer-0.4.1 (c (n "ink-analyzer") (v "0.4.1") (d (list (d (n "ink-analyzer-ir") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)))) (h "07ylw914k2r0sgcknmxgf2rfy8k8kkq13rf3sqrk86mqfdmxy56c")))

(define-public crate-ink-analyzer-0.5.0 (c (n "ink-analyzer") (v "0.5.0") (d (list (d (n "ink-analyzer-ir") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)))) (h "1ws3d5rl76qs7v7wvssj42n8cpq38hgcklms3y5fng1lg1nj7296")))

(define-public crate-ink-analyzer-0.6.0 (c (n "ink-analyzer") (v "0.6.0") (d (list (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "ink-analyzer-ir") (r "^0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)))) (h "103r6zjk7fgf2lb8qmp0r6k2w1k0z924wsmsy001g9d0c9bqnp2y")))

(define-public crate-ink-analyzer-0.6.1 (c (n "ink-analyzer") (v "0.6.1") (d (list (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "ink-analyzer-ir") (r "^0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)))) (h "07awfk5xcqwpdlzy2sz7nz6s0jiqyghn7axbib72f5i2v84fq1vf")))

(define-public crate-ink-analyzer-0.6.2 (c (n "ink-analyzer") (v "0.6.2") (d (list (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "ink-analyzer-ir") (r "^0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)))) (h "09cwhifrl7yl6pjqzal5v2z6b2hl2pihdzd3g9ghzpnj01brl9z0")))

(define-public crate-ink-analyzer-0.7.0 (c (n "ink-analyzer") (v "0.7.0") (d (list (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "ink-analyzer-ir") (r "^0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)))) (h "1b6vp23r0b102i89ga8wzfpxlpw4f9a6nm6az7jq726swlkcyw11")))

(define-public crate-ink-analyzer-0.7.1 (c (n "ink-analyzer") (v "0.7.1") (d (list (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "ink-analyzer-ir") (r "^0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)))) (h "02v70lq5v1y9gz7d80ry85ygfrsyys5g75729z5zcisxzqhfnmmy")))

(define-public crate-ink-analyzer-0.8.0 (c (n "ink-analyzer") (v "0.8.0") (d (list (d (n "ink-analyzer-ir") (r "^0.8") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "syn") (r "^2.0.29") (d #t) (k 2)))) (h "106h0p1myplxsv3sjr0y1pc358w1nb19fjl9118i8hq2id761niz")))

(define-public crate-ink-analyzer-0.8.1 (c (n "ink-analyzer") (v "0.8.1") (d (list (d (n "ink-analyzer-ir") (r "^0.8") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 2)))) (h "0g00b6bf49c2x8r4nhmhbyjmjsi9jxp6751jzpsys55j8yssm3nf")))

(define-public crate-ink-analyzer-0.8.2 (c (n "ink-analyzer") (v "0.8.2") (d (list (d (n "ink-analyzer-ir") (r "^0.8") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 2)))) (h "192hic0r20ls0fxip4vvkavg31y06cmkvnn91lxfdwf81ywa3czz")))

(define-public crate-ink-analyzer-0.8.3 (c (n "ink-analyzer") (v "0.8.3") (d (list (d (n "ink-analyzer-ir") (r "^0.8") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 2)))) (h "1sbyyn0fckrwwl225fik2kmnyd2h0i236qs0883ziqfn2ndgi504")))

(define-public crate-ink-analyzer-0.8.4 (c (n "ink-analyzer") (v "0.8.4") (d (list (d (n "ink-analyzer-ir") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 2)))) (h "1531wiyqhywdl4ma9jfli1166n3fb24dzz1qqilhyvlyg6pivdqb")))

(define-public crate-ink-analyzer-0.8.5 (c (n "ink-analyzer") (v "0.8.5") (d (list (d (n "ink-analyzer-ir") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 2)))) (h "06bmdnca7kbc590spzlzkh3vls48r5gicakdwkjr9jhhy0bazh6l")))

(define-public crate-ink-analyzer-0.8.6 (c (n "ink-analyzer") (v "0.8.6") (d (list (d (n "ink-analyzer-ir") (r "^0.9") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 2)))) (h "0j6fag4v2h4385hi39hyrmdh3r5aj7ks4z7s9cc5jg1y5nsa0ky2")))

(define-public crate-ink-analyzer-0.8.7 (c (n "ink-analyzer") (v "0.8.7") (d (list (d (n "ink-analyzer-ir") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 2)))) (h "0wvcipfmczvdlswflhndw9a3ndqyxq8bghgllwabadnjlwmf661g")))

(define-public crate-ink-analyzer-0.8.8 (c (n "ink-analyzer") (v "0.8.8") (d (list (d (n "ink-analyzer-ir") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 2)))) (h "1ss528g7wlcd4h7pxwxanbx2aza10lwy43pjkj03pvfvagpy6xv7")))

(define-public crate-ink-analyzer-0.8.9 (c (n "ink-analyzer") (v "0.8.9") (d (list (d (n "ink-analyzer-ir") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 2)))) (h "1l49jvrwbsa2byi2j18a41fdw9n6slsrpv30lkl5smax5jbx3sr7")))

(define-public crate-ink-analyzer-0.8.10 (c (n "ink-analyzer") (v "0.8.10") (d (list (d (n "ink-analyzer-ir") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 2)))) (h "1pz5dhk6mfbh3w00269x8hsb7jbvvi2bqar95mivmx44dg47f92c")))

(define-public crate-ink-analyzer-0.8.11 (c (n "ink-analyzer") (v "0.8.11") (d (list (d (n "ink-analyzer-ir") (r "^0.10") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 2)))) (h "1pan18ayscxxmg7ir6mnv824l8mp15ckch7hw1ya0054zhv8mmw7")))

(define-public crate-ink-analyzer-0.8.12 (c (n "ink-analyzer") (v "0.8.12") (d (list (d (n "ink-analyzer-ir") (r "^0.11") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 2)))) (h "17jacxpdnwn3cx0gkxvpdjp4xsw7pv42ndkzc170n4nmzpj09cai")))

(define-public crate-ink-analyzer-0.8.13 (c (n "ink-analyzer") (v "0.8.13") (d (list (d (n "ink-analyzer-ir") (r "^0.11") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 2)))) (h "1sdjbhrya2ripl550b4ib2jsnnvvcn0aclgpjjjfrxxbbc2acg32")))

(define-public crate-ink-analyzer-0.8.14 (c (n "ink-analyzer") (v "0.8.14") (d (list (d (n "ink-analyzer-ir") (r "^0.11") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 2)))) (h "1p54mn2gamwizh5s0zaxqqqqcirz9w9qzlwpxrys04w1v67nv5sh")))

(define-public crate-ink-analyzer-0.8.15 (c (n "ink-analyzer") (v "0.8.15") (d (list (d (n "ink-analyzer-ir") (r "^0.11") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 2)))) (h "1n9rhsnwijb7qhvsy6rbci9h5m7vw771k4hn8h38f9qnh44jh72m")))

(define-public crate-ink-analyzer-0.8.16 (c (n "ink-analyzer") (v "0.8.16") (d (list (d (n "ink-analyzer-ir") (r "^0.11") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 2)))) (h "0xmd7sr8g669zzam3fhbw0p9343vnnhpyhaqpf61fmijiwhq4cqi")))

(define-public crate-ink-analyzer-0.8.17 (c (n "ink-analyzer") (v "0.8.17") (d (list (d (n "ink-analyzer-ir") (r "^0.11") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 2)))) (h "0n0dw6k0by6mnwx6dlj0hslgf2przs99yff85q1mc68q84m1p3n4")))

(define-public crate-ink-analyzer-0.8.18 (c (n "ink-analyzer") (v "0.8.18") (d (list (d (n "ink-analyzer-ir") (r "^0.11") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 2)))) (h "1y28vkifncdzgv8gl8y2iwdlhyvy2b7nk8srljs38jqaahgdqsmn")))

(define-public crate-ink-analyzer-0.8.19 (c (n "ink-analyzer") (v "0.8.19") (d (list (d (n "ink-analyzer-ir") (r "^0.11") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 2)))) (h "0074vfh9jqizgdk29dkag0j2fjk0q3baxfh5x16cw7qqd304ax2x")))

(define-public crate-ink-analyzer-0.8.20 (c (n "ink-analyzer") (v "0.8.20") (d (list (d (n "ink-analyzer-ir") (r "^0.11") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 2)))) (h "107rcv9cvxymacjymkng68kb1pf9d9zx883y05savln78b22sbiv")))

(define-public crate-ink-analyzer-0.8.21 (c (n "ink-analyzer") (v "0.8.21") (d (list (d (n "ink-analyzer-ir") (r "^0.12") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.12") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 2)))) (h "0sxsxhlq43ss7drykjc639p0j312hvd2szrqmnw6il3p1b89mjzi")))

(define-public crate-ink-analyzer-0.8.22 (c (n "ink-analyzer") (v "0.8.22") (d (list (d (n "ink-analyzer-ir") (r "^0.12") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.16") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.35") (d #t) (k 2)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 2)))) (h "149ybyy7j9rwwqyh195l6fvfw4cqkrcsl605lj6gqk4w36cj9js3")))

(define-public crate-ink-analyzer-0.8.23 (c (n "ink-analyzer") (v "0.8.23") (d (list (d (n "ink-analyzer-ir") (r "^0.12") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.16") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.35") (d #t) (k 2)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 2)))) (h "1xcki6fab49dvd3jhblfr9fanlyl87sm0bsl412j3yxdp79mn7h5")))

(define-public crate-ink-analyzer-0.9.0 (c (n "ink-analyzer") (v "0.9.0") (d (list (d (n "ink-analyzer-ir") (r "^0.13") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.16") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.35") (d #t) (k 2)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 2)))) (h "07isjwy4vy9kg79q1pa2h6phbbf2p2vszaa8q1i3dqb11p7qvvrw")))

(define-public crate-ink-analyzer-0.9.1 (c (n "ink-analyzer") (v "0.9.1") (d (list (d (n "ink-analyzer-ir") (r "^0.13") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.16") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.35") (d #t) (k 2)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 2)))) (h "1ipci50zq4zqa7236c5b684f6j4cw7737ayjvrim2wrwgcyyvr5c")))

(define-public crate-ink-analyzer-0.9.2 (c (n "ink-analyzer") (v "0.9.2") (d (list (d (n "ink-analyzer-ir") (r "^0.13") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.16") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.35") (d #t) (k 2)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 2)))) (h "14m4vay3kjbkj57bsp8qdhav7zbrpvc9j20cskhzbd3vakpr4mjj")))

(define-public crate-ink-analyzer-0.9.3 (c (n "ink-analyzer") (v "0.9.3") (d (list (d (n "ink-analyzer-ir") (r "^0.13") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.16") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.35") (d #t) (k 2)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 2)))) (h "127sp34mlxrgyhhs0p3m5k1wn5f58ma2b1hx8dkhfw19wk3zgqx4")))

(define-public crate-ink-analyzer-0.9.4 (c (n "ink-analyzer") (v "0.9.4") (d (list (d (n "ink-analyzer-ir") (r "^0.14") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.16") (f (quote ("verbatim"))) (d #t) (k 2)) (d (n "quote") (r "^1.0.35") (d #t) (k 2)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 2)))) (h "0qgz16lpgm9frmb72jlhjwl7623pchm2aldkw1wg0bhr2if03bb8")))

