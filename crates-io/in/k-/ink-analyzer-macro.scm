(define-module (crates-io in k- ink-analyzer-macro) #:use-module (crates-io))

(define-public crate-ink-analyzer-macro-0.1.0 (c (n "ink-analyzer-macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1dffi1z39dmakk1fcqiv7gw4vf505d320iijm3mcywwwdp90al13")))

(define-public crate-ink-analyzer-macro-0.1.1 (c (n "ink-analyzer-macro") (v "0.1.1") (d (list (d (n "ink-analyzer-ir") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "1j3jivzxm48i7aqpgk726mhx811zwggqafsjc5sn0x60ymk9hd6f")))

(define-public crate-ink-analyzer-macro-0.2.0 (c (n "ink-analyzer-macro") (v "0.2.0") (d (list (d (n "ink-analyzer-ir") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "1z1i4w6v43rnxp4srk3qrlq6q3csv44vmwgzdf1ibwikfqiacdgj")))

(define-public crate-ink-analyzer-macro-0.2.1 (c (n "ink-analyzer-macro") (v "0.2.1") (d (list (d (n "ink-analyzer-ir") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "1d8dq1n7lbxc8i5i1y6i3v5sjjir1h7m1ahgskr0ji1zvhvs8733")))

(define-public crate-ink-analyzer-macro-0.3.0 (c (n "ink-analyzer-macro") (v "0.3.0") (d (list (d (n "ink-analyzer-ir") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "0fxbi8nmwg3mdvlq5n9lxrypp4c5lgpaz1cyacs4fxh1cffx30ya")))

(define-public crate-ink-analyzer-macro-0.3.1 (c (n "ink-analyzer-macro") (v "0.3.1") (d (list (d (n "ink-analyzer-ir") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "1bnzc9f4scp1d01f2lbd6kfa36yph3lk8nzk44p1gmksflcly770")))

(define-public crate-ink-analyzer-macro-0.4.0 (c (n "ink-analyzer-macro") (v "0.4.0") (d (list (d (n "ink-analyzer-ir") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "0kl2nginpfv0vjmcxh6zs4103lr44810inl44fg22bsz4syx397c")))

(define-public crate-ink-analyzer-macro-0.5.0 (c (n "ink-analyzer-macro") (v "0.5.0") (d (list (d (n "ink-analyzer-ir") (r "^0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "1ddkycf9s7bpn8cgmh3xnysj2paqqbmbhcq8pbm3q0cngjnbcdcb")))

(define-public crate-ink-analyzer-macro-0.6.0 (c (n "ink-analyzer-macro") (v "0.6.0") (d (list (d (n "ink-analyzer-ir") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "07nr9lzyz70vgmakalz4905n55z7r8kacdfcjnsal36crxm91kb0")))

(define-public crate-ink-analyzer-macro-0.6.1 (c (n "ink-analyzer-macro") (v "0.6.1") (d (list (d (n "ink-analyzer-ir") (r "^0.7") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "1c40cfj2kj1vnd9p0lz4v517ykyzmnmwabr2vajfhr3s9nvryi69")))

(define-public crate-ink-analyzer-macro-0.7.0 (c (n "ink-analyzer-macro") (v "0.7.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "07j34p345716hqf2vk4vrqg602v8qg15lw90i9hinysran71lvqb")))

(define-public crate-ink-analyzer-macro-0.7.1 (c (n "ink-analyzer-macro") (v "0.7.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "17nqpd2496fx3ssmij5mf9x9bw5fpkkwzd89sq4v4hkzywmj91j9")))

(define-public crate-ink-analyzer-macro-0.7.2 (c (n "ink-analyzer-macro") (v "0.7.2") (d (list (d (n "darling") (r "^0.20.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "070pg2l4ky566x5zci6j1lqfj2hhpb25nqirvczfhj7f7yrmf1zc")))

