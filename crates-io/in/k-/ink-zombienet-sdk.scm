(define-module (crates-io in k- ink-zombienet-sdk) #:use-module (crates-io))

(define-public crate-ink-zombienet-sdk-0.1.0-alpha.1 (c (n "ink-zombienet-sdk") (v "0.1.0-alpha.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "configuration") (r "^0.1.0-alpha.1") (d #t) (k 0) (p "ink-zombienet-configuration")) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "orchestrator") (r "^0.1.0-alpha.1") (d #t) (k 0) (p "ink-zombienet-orchestrator")) (d (n "provider") (r "^0.1.0-alpha.1") (d #t) (k 0) (p "ink-zombienet-provider")) (d (n "subxt") (r "^0.33.0") (d #t) (k 0)) (d (n "support") (r "^0.1.0-alpha.1") (d #t) (k 0) (p "ink-zombienet-support")) (d (n "tokio") (r "^1.28") (d #t) (k 0)))) (h "15v45r437zr63nkd531phhsz9v1gf7gqa484hyivib2bvdgy7nmv") (r "1.70.0")))

