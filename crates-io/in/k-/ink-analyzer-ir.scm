(define-module (crates-io in k- ink-analyzer-ir) #:use-module (crates-io))

(define-public crate-ink-analyzer-ir-0.1.0 (c (n "ink-analyzer-ir") (v "0.1.0") (d (list (d (n "ink-analyzer-macro") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "^0.0.149") (d #t) (k 0)))) (h "1zla27i2015saji6rql4pm1xp21ljy3bxi37hkr7vihjjiamvm9i")))

(define-public crate-ink-analyzer-ir-0.1.1 (c (n "ink-analyzer-ir") (v "0.1.1") (d (list (d (n "ink-analyzer-macro") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "^0.0.149") (d #t) (k 0)))) (h "0caff97isv4bzm365d871j7cvnzxgprzc3jc1rl0dg0bwfrk0fsw")))

(define-public crate-ink-analyzer-ir-0.1.2 (c (n "ink-analyzer-ir") (v "0.1.2") (d (list (d (n "ink-analyzer-macro") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "^0.0.149") (d #t) (k 0)))) (h "0l89gc3k5yz83gcq5lvm79fawd77nlmxhwb8gznw7gjsdfd4yx16")))

(define-public crate-ink-analyzer-ir-0.2.0 (c (n "ink-analyzer-ir") (v "0.2.0") (d (list (d (n "ink-analyzer-macro") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "^0.0.149") (d #t) (k 0)))) (h "1297kcrgsnzdi5kjczcvf3n7jyz1azymq7cdc9khd78cjmgd9b3b")))

(define-public crate-ink-analyzer-ir-0.3.0 (c (n "ink-analyzer-ir") (v "0.3.0") (d (list (d (n "ink-analyzer-macro") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "^0.0.149") (d #t) (k 0)))) (h "0yk1jbcdn0r7w60ybvqfcv9jfxxl8brc81x3jjfb0hrvnnqdq55w")))

(define-public crate-ink-analyzer-ir-0.4.0 (c (n "ink-analyzer-ir") (v "0.4.0") (d (list (d (n "ink-analyzer-macro") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "^0.0.149") (d #t) (k 0)))) (h "1hr9pb1bkl0a8s85kadmg5wvrfsnjc3z9nzbkinaqajazxhqs8d8")))

(define-public crate-ink-analyzer-ir-0.5.0 (c (n "ink-analyzer-ir") (v "0.5.0") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "ink-analyzer-macro") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "^0.0.149") (d #t) (k 0)))) (h "06bs50src3v3d99aky0aq646vgwpk541027n303ld8b3g8ba0mk3")))

(define-public crate-ink-analyzer-ir-0.6.0 (c (n "ink-analyzer-ir") (v "0.6.0") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "ink-analyzer-macro") (r "^0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "^0.0.149") (d #t) (k 0)))) (h "1qywlv8qj6g5i74smmivj7sf8wdqw0shq0pqdpyg974mc4nv5l25")))

(define-public crate-ink-analyzer-ir-0.7.0 (c (n "ink-analyzer-ir") (v "0.7.0") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "ink-analyzer-macro") (r "^0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "^0.0.149") (d #t) (k 0)))) (h "0brd8a7ygz7didrr56f0kib7235shk4lwmf1ma8nh8g9ybzivnyq")))

(define-public crate-ink-analyzer-ir-0.7.1 (c (n "ink-analyzer-ir") (v "0.7.1") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "ink-analyzer-macro") (r "^0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "ra_ap_syntax") (r "^0.0.149") (d #t) (k 0)))) (h "0x0wn6yr7gmlvycmivypi0wrvd3n783zgl9jrka38z6lriifd7dh")))

(define-public crate-ink-analyzer-ir-0.8.0 (c (n "ink-analyzer-ir") (v "0.8.0") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "ink-analyzer-macro") (r "^0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "ra_ap_syntax") (r "^0.0.149") (d #t) (k 0)))) (h "1q7pn119z4xc89cn71083ipaxaj77hcrczrdv3xqhlirvw2283nz")))

(define-public crate-ink-analyzer-ir-0.9.0 (c (n "ink-analyzer-ir") (v "0.9.0") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "ink-analyzer-macro") (r "^0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "ra_ap_syntax") (r "^0.0.149") (d #t) (k 0)))) (h "07bffwm3qr545hvcfanwbvjncj40ifsckngcylklx5mi2ll5a1p8")))

(define-public crate-ink-analyzer-ir-0.10.0 (c (n "ink-analyzer-ir") (v "0.10.0") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "ink-analyzer-macro") (r "^0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "ra_ap_syntax") (r "^0.0.173") (d #t) (k 0)))) (h "0r480v3avpl5c4h9af4d0icdz1dy1ss63a0vzqdlk5mp4n8nfvjy")))

(define-public crate-ink-analyzer-ir-0.10.1 (c (n "ink-analyzer-ir") (v "0.10.1") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "ink-analyzer-macro") (r "^0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "ra_ap_syntax") (r "^0.0.173") (d #t) (k 0)))) (h "12imr7yh81k60hi308w3nnvjnvgpa59bvw39f4rida2p4qnicyzk")))

(define-public crate-ink-analyzer-ir-0.10.2 (c (n "ink-analyzer-ir") (v "0.10.2") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "ink-analyzer-macro") (r "^0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "ra_ap_syntax") (r "^0.0.173") (d #t) (k 0)))) (h "1b1g27fcjv6xd9qrv41kaic96wp33v44cbc0i2x0h52sp23i38dp")))

(define-public crate-ink-analyzer-ir-0.10.3 (c (n "ink-analyzer-ir") (v "0.10.3") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "ink-analyzer-macro") (r "^0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "ra_ap_syntax") (r "^0.0.173") (d #t) (k 0)))) (h "137h5d8dfqgpvy5gvw2k9j9w45jyijl6sih7l5fdwxcgpcr5i2jh")))

(define-public crate-ink-analyzer-ir-0.10.4 (c (n "ink-analyzer-ir") (v "0.10.4") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "ink-analyzer-macro") (r "^0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "ra_ap_syntax") (r "^0.0.173") (d #t) (k 0)))) (h "0g1azwm98vj7pmqlpgahjc15vmqi609znwm092bdq9j8cdxbwybh")))

(define-public crate-ink-analyzer-ir-0.11.0 (c (n "ink-analyzer-ir") (v "0.11.0") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "ink-analyzer-macro") (r "^0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "ra_ap_syntax") (r "^0.0.187") (d #t) (k 0)))) (h "0hxblw667i1d3xpv0npk6x06w7q0k61vmpp0qqdhkpqi3wi3fzs8")))

(define-public crate-ink-analyzer-ir-0.11.1 (c (n "ink-analyzer-ir") (v "0.11.1") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "ink-analyzer-macro") (r "^0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "ra_ap_syntax") (r "^0.0.187") (d #t) (k 0)))) (h "05bi0zq4qn4apxc04l14nnrzh2wpkrd5kx5ilakxi4gpjqvnx9bp")))

(define-public crate-ink-analyzer-ir-0.11.2 (c (n "ink-analyzer-ir") (v "0.11.2") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "ink-analyzer-macro") (r "^0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "ra_ap_syntax") (r "^0.0.187") (d #t) (k 0)))) (h "14waq2d6i5q9ghnw3np8v8ir43yknk595925nvbikf43jgmv8ac1")))

(define-public crate-ink-analyzer-ir-0.11.3 (c (n "ink-analyzer-ir") (v "0.11.3") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "ink-analyzer-macro") (r "^0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "ra_ap_syntax") (r "^0.0.187") (d #t) (k 0)))) (h "02l0qcibvpzm40wv4xh9rrbg4z7mzz1nskq63h1ia8f2x5q2irp8")))

(define-public crate-ink-analyzer-ir-0.11.4 (c (n "ink-analyzer-ir") (v "0.11.4") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "ink-analyzer-macro") (r "^0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "ra_ap_syntax") (r "^0.0.187") (d #t) (k 0)))) (h "0baqnfrs5pqx7v01q50ll3jjkwk2m51vizcbz8jdkca36mhip6jh")))

(define-public crate-ink-analyzer-ir-0.12.0 (c (n "ink-analyzer-ir") (v "0.12.0") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "ink-analyzer-macro") (r "^0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 2)) (d (n "ra_ap_syntax") (r "^0.0.197") (d #t) (k 0)))) (h "0sx9rqkzsx0wdh0hnqbc5gywg7qqxgxb4fwg0vm67p7ry0amiwm0")))

(define-public crate-ink-analyzer-ir-0.12.1 (c (n "ink-analyzer-ir") (v "0.12.1") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "ink-analyzer-macro") (r "^0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 2)) (d (n "ra_ap_syntax") (r "^0.0.199") (d #t) (k 0)))) (h "13m9j68mijb4bnx4apxw9mkfv1wzpx1z9dxanvl4cnmmsiiarkqs")))

(define-public crate-ink-analyzer-ir-0.12.2 (c (n "ink-analyzer-ir") (v "0.12.2") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "ink-analyzer-macro") (r "^0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 2)) (d (n "ra_ap_syntax") (r "^0.0.199") (d #t) (k 0)))) (h "0q5y32gjrg38v04rqaza5kqxhyxxw9apvydnxd5zh7dj3zj1ygrf")))

(define-public crate-ink-analyzer-ir-0.13.0 (c (n "ink-analyzer-ir") (v "0.13.0") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "impl-serde") (r "^0.4.0") (k 0)) (d (n "ink-analyzer-macro") (r "^0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 2)) (d (n "ra_ap_syntax") (r "^0.0.199") (d #t) (k 0)))) (h "1n7bjfyzyqn9dlfys0zqv45wynhaljqidmbhas8yhnb9djfiaqcg")))

(define-public crate-ink-analyzer-ir-0.14.0 (c (n "ink-analyzer-ir") (v "0.14.0") (d (list (d (n "blake2") (r "^0.10.6") (d #t) (k 0)) (d (n "impl-serde") (r "^0.4.0") (k 0)) (d (n "ink-analyzer-macro") (r "^0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 2)) (d (n "ra_ap_syntax") (r "^0.0.199") (d #t) (k 0)))) (h "0l4zv33csm5fp545wzacf9wds1nj5k295dqbn91z42xfb2k0yg0s")))

