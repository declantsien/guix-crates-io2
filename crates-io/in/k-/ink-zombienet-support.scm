(define-module (crates-io in k- ink-zombienet-support) #:use-module (crates-io))

(define-public crate-ink-zombienet-support-0.1.0-alpha.1 (c (n "ink-zombienet-support") (v "0.1.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("signal"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "0cx90c6rrfv31vwc8q2qfx7l7lxhnh1izf72j754sx7mr4pg15g9") (r "1.70.0")))

