(define-module (crates-io in ta inta) #:use-module (crates-io))

(define-public crate-inta-0.1.0 (c (n "inta") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1zvabraxxhpnl3lpl1l3mfx1bw9pjip6v41bakvjs4g004kqjfcc")))

(define-public crate-inta-0.1.1 (c (n "inta") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "15m40jzql0i4hfvb2sdlsy9ffma8n6fvfy545fxay27485prkmhh")))

(define-public crate-inta-0.1.2 (c (n "inta") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0m82zjqypx5nn6hfs0f6kf4la09gmnq3a8pjiyhxv979ymxvv7ic")))

