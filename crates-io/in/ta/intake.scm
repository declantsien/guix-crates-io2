(define-module (crates-io in ta intake) #:use-module (crates-io))

(define-public crate-intake-0.0.1 (c (n "intake") (v "0.0.1") (h "046n4iw6lhy92yjhykqn8ijp2fjnf7lqsz85fwl0djwfg3kri3il") (y #t)))

(define-public crate-intake-0.0.2 (c (n "intake") (v "0.0.2") (h "0sgrj5il5npg1ijvskwmpzg5mdipglkhl9yyy2pmbbjm08a3ld1l") (y #t)))

(define-public crate-intake-0.0.3 (c (n "intake") (v "0.0.3") (h "0zc2vmlbfbdnyiqs93mrbmw4ljlb4smy1hw9s4nlf4hgbimf3wc3") (y #t)))

(define-public crate-intake-0.0.4 (c (n "intake") (v "0.0.4") (h "15rjxlbgygnrgqrrw5pprp3n479l2kkakyhl8ximqqqm6nza5674") (y #t)))

