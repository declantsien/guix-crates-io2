(define-module (crates-io in ta intaglio) #:use-module (crates-io))

(define-public crate-intaglio-1.0.1 (c (n "intaglio") (v "1.0.1") (d (list (d (n "bstr") (r "^0.2") (o #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 2)) (d (n "quickcheck") (r "^0.9") (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1yafyb863jcwnny4960krw73msip4dqfr6bfbwwi4hb4ylicqbb3") (f (quote (("default" "bytes") ("bytes" "bstr"))))))

(define-public crate-intaglio-1.1.0 (c (n "intaglio") (v "1.1.0") (d (list (d (n "bstr") (r "^0.2") (o #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 2)) (d (n "quickcheck") (r "^0.9") (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "135bigr8n8c3kq765nnra3zb8cjdv454svflx5374gjwmdcmr4hq") (f (quote (("default" "bytes") ("bytes" "bstr"))))))

(define-public crate-intaglio-1.1.1 (c (n "intaglio") (v "1.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"linux\")") (k 2)) (d (n "quickcheck") (r "^0.9") (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "01mj5sq0p0h0n0ka49qy5q62hyzsg79zn76wj8shzbc5s1jvx7vg") (f (quote (("default" "bytes") ("bytes"))))))

(define-public crate-intaglio-1.2.0 (c (n "intaglio") (v "1.2.0") (d (list (d (n "quickcheck") (r "^0.9") (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1rh1n8vsxk7amznral8pv72wb35x9qc07wnv5xhfxq503mzqfjys") (f (quote (("default" "bytes") ("bytes"))))))

(define-public crate-intaglio-1.2.1 (c (n "intaglio") (v "1.2.1") (d (list (d (n "quickcheck") (r "^1.0") (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9, >=0.9.2") (d #t) (k 2)))) (h "03zalzf9k6dgl4yclrdg8k370qw1nd2m9nh3n4sn68210cnh6yln") (f (quote (("default" "bytes") ("bytes"))))))

(define-public crate-intaglio-1.2.2 (c (n "intaglio") (v "1.2.2") (d (list (d (n "quickcheck") (r "^1.0") (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9, >=0.9.2") (d #t) (k 2)))) (h "1plwb553ma028xpf2qhxpqfr5j4hzdw1hv94vk9kj3f6p0q97g9a") (f (quote (("default" "bytes") ("bytes"))))))

(define-public crate-intaglio-1.3.0 (c (n "intaglio") (v "1.3.0") (d (list (d (n "quickcheck") (r "^1.0") (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9, >=0.9.2") (d #t) (k 2)))) (h "1lngl9hk63l82pmvq67bqxcdsdyxz2dy53gwwgks2pqhscp0swxj") (f (quote (("default" "bytes") ("bytes"))))))

(define-public crate-intaglio-1.4.0 (c (n "intaglio") (v "1.4.0") (d (list (d (n "quickcheck") (r "^1.0") (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9, >=0.9.2") (d #t) (k 2)))) (h "0dq40vm9jq1ymvkf8ddyq4j5452ys04yxmjw6673yyw2r96klj2p") (f (quote (("default" "bytes") ("bytes")))) (r "1.56.0")))

(define-public crate-intaglio-1.4.1 (c (n "intaglio") (v "1.4.1") (d (list (d (n "quickcheck") (r "^1.0") (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9, >=0.9.2") (d #t) (k 2)))) (h "1mm7arzfgd2zaakf064fva881lqc42gjaxl56q10gxaanvpf5rc2") (f (quote (("default" "bytes") ("bytes")))) (r "1.56.0")))

(define-public crate-intaglio-1.4.2 (c (n "intaglio") (v "1.4.2") (d (list (d (n "quickcheck") (r "^1.0") (k 2)) (d (n "quickcheck_macros") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9, >=0.9.2") (d #t) (k 2)))) (h "0banrgvxd5g5hx8978i10mpgzzxjan5a2i2lfnkd04wmsqbm9sqz") (f (quote (("default" "bytes") ("bytes")))) (r "1.56.0")))

(define-public crate-intaglio-1.5.0 (c (n "intaglio") (v "1.5.0") (d (list (d (n "quickcheck") (r "^1.0.3") (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "0sx7fc55sr693xmri0jidmqk5n7qm27hl0qrldnzj86xyzays8sb") (f (quote (("path") ("osstr") ("default" "bytes" "cstr" "osstr" "path") ("cstr") ("bytes")))) (r "1.56.0")))

(define-public crate-intaglio-1.6.0 (c (n "intaglio") (v "1.6.0") (d (list (d (n "quickcheck") (r "^1.0.3") (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "11l5lj1g7mph6b62z33r276pq7qaq9y3wcnadv9yrq2x1cq7a9rz") (f (quote (("path") ("osstr") ("default" "bytes" "cstr" "osstr" "path") ("cstr") ("bytes")))) (r "1.56.0")))

(define-public crate-intaglio-1.6.1 (c (n "intaglio") (v "1.6.1") (d (list (d (n "quickcheck") (r "^1.0.3") (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "04a28v7q6zb54b9nb4i4aahdjqrh5i0fm75r8h4ayiivnvsswjfx") (f (quote (("path") ("osstr") ("default" "bytes" "cstr" "osstr" "path") ("cstr") ("bytes")))) (r "1.56.0")))

(define-public crate-intaglio-1.7.0 (c (n "intaglio") (v "1.7.0") (d (list (d (n "quickcheck") (r "^1.0.3") (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "10gqwn4py9j15wxv0i2cqsv3z588m7k0841wi755ygal4qjxnr8n") (f (quote (("path") ("osstr") ("default" "bytes" "cstr" "osstr" "path") ("cstr") ("bytes")))) (r "1.56.0")))

(define-public crate-intaglio-1.8.0 (c (n "intaglio") (v "1.8.0") (d (list (d (n "quickcheck") (r "^1.0.3") (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "0izf44fwq8svcnimsrjayclkisc8g7jd9wla043wbnm28xm7akqv") (f (quote (("path") ("osstr") ("default" "bytes" "cstr" "osstr" "path") ("cstr") ("bytes")))) (r "1.56.0")))

(define-public crate-intaglio-1.9.0 (c (n "intaglio") (v "1.9.0") (d (list (d (n "quickcheck") (r "^1.0.3") (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "0cd0r9kpjadbnip68s03gvlv73jj6wwzglxyjz6nddbdnp16vcxg") (f (quote (("path") ("osstr") ("default" "bytes" "cstr" "osstr" "path") ("cstr") ("bytes")))) (y #t) (r "1.58.0")))

(define-public crate-intaglio-1.9.1 (c (n "intaglio") (v "1.9.1") (d (list (d (n "quickcheck") (r "^1.0.3") (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "045n39ri8a2zryd3dml703yhpyillj3g3sd1k7f9s3svw33v2gma") (f (quote (("path") ("osstr") ("default" "bytes" "cstr" "osstr" "path") ("cstr") ("bytes")))) (r "1.58.0")))

