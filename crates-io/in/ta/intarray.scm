(define-module (crates-io in ta intarray) #:use-module (crates-io))

(define-public crate-intarray-0.1.0 (c (n "intarray") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "digest") (r "^0.8.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.6.2") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.99") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 2)) (d (n "test-env-log") (r "^0.1.0") (d #t) (k 2)))) (h "1x0lspnvflgr2a8zki0vql92777mw5025hx5sv6mpxkpfr1psabf")))

