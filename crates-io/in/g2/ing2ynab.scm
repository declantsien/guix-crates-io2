(define-module (crates-io in g2 ing2ynab) #:use-module (crates-io))

(define-public crate-ing2ynab-0.0.0 (c (n "ing2ynab") (v "0.0.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)))) (h "1gpx7sk7r59hg5m14r3lbnlijkywh580iqj8cr72wv7gn77nvm2p")))

(define-public crate-ing2ynab-0.0.1 (c (n "ing2ynab") (v "0.0.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 1)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 1)))) (h "1rz05s67gza5g86n2p8d1zgkbdvmpbhv81xz42x1mlys5rk5v356")))

(define-public crate-ing2ynab-0.0.2 (c (n "ing2ynab") (v "0.0.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 1)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 1)))) (h "1l8xy49n3134fh96cam092bq8fqbsgkrpdrklmizqi4phskqhmxz")))

(define-public crate-ing2ynab-0.0.3 (c (n "ing2ynab") (v "0.0.3") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 1)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 1)))) (h "05kpy3lxkin0pffbn2qz1qjbd2hp7chn35asz90fg7g0cqc7g9n1")))

(define-public crate-ing2ynab-0.0.4 (c (n "ing2ynab") (v "0.0.4") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 1)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 1)))) (h "0ymdqm2hmz5hc8ifj8yxfwxxamcbsvpxpbk53d1r1mcxlp77ld8y")))

(define-public crate-ing2ynab-0.0.5 (c (n "ing2ynab") (v "0.0.5") (d (list (d (n "jane-eyre") (r "^0.3.0") (d #t) (k 0)) (d (n "jane-eyre") (r "^0.3.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "mktemp") (r "^0.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1cbw9gn8w692a4mzlinp1diq5hy9kphxnycxk2k4a9gz4bkcy12b")))

