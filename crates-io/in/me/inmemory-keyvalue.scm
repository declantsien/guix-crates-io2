(define-module (crates-io in me inmemory-keyvalue) #:use-module (crates-io))

(define-public crate-inmemory-keyvalue-0.4.0 (c (n "inmemory-keyvalue") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "wascc-codec") (r "^0.9.0") (d #t) (k 0)) (d (n "wasmcloud-actor-core") (r "^0.2.0") (d #t) (k 0)) (d (n "wasmcloud-actor-keyvalue") (r "^0.2.0") (d #t) (k 0)))) (h "1j5jrmq6jak2nbayyh7b65a6arzvzypqw79yv61z7nhqmqpcy21r") (f (quote (("static_plugin"))))))

