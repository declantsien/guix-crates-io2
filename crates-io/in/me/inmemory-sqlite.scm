(define-module (crates-io in me inmemory-sqlite) #:use-module (crates-io))

(define-public crate-inmemory-sqlite-0.1.0 (c (n "inmemory-sqlite") (v "0.1.0") (d (list (d (n "rusqlite") (r "^0.21") (d #t) (k 0)) (d (n "thread_local") (r "^1.0") (d #t) (k 0)))) (h "14mz820aispwwhhj29scblipl95kyx2rnh36ra9lbas0dmfbsfpl") (y #t)))

(define-public crate-inmemory-sqlite-0.1.1 (c (n "inmemory-sqlite") (v "0.1.1") (d (list (d (n "rusqlite") (r "^0.21") (d #t) (k 0)) (d (n "thread_local") (r "^1.0") (d #t) (k 0)))) (h "182ghvjv2qcgd42wpwwvwa701cdhgjavba7pkdl7a0m9aawza387") (y #t)))

(define-public crate-inmemory-sqlite-0.1.2 (c (n "inmemory-sqlite") (v "0.1.2") (d (list (d (n "rusqlite") (r "^0.21") (d #t) (k 0)) (d (n "thread_local") (r "^1.0") (d #t) (k 0)) (d (n "threadbound") (r "^0.1") (d #t) (k 0)))) (h "1p3kw8ds0cj9nkafnp26h8g77940gs1pny9giy0lp3vn1f5i9v7a") (y #t)))

(define-public crate-inmemory-sqlite-0.1.3 (c (n "inmemory-sqlite") (v "0.1.3") (d (list (d (n "rusqlite") (r "^0.21") (d #t) (k 0)) (d (n "thread_local") (r "^1.0") (d #t) (k 0)))) (h "1w0xx78q31qcwcylanslwp2liqzpq1cxfihhcm60bkb60wji8jyi") (y #t)))

(define-public crate-inmemory-sqlite-0.1.4 (c (n "inmemory-sqlite") (v "0.1.4") (d (list (d (n "rusqlite") (r "^0.21") (d #t) (k 0)) (d (n "thread_local") (r "^1.0") (d #t) (k 0)))) (h "14542hzrkkyl4q1fa19vsmfv8lc4wm8svpq29y23yfhxyix1qh1l") (y #t)))

(define-public crate-inmemory-sqlite-0.1.5 (c (n "inmemory-sqlite") (v "0.1.5") (d (list (d (n "rusqlite") (r "^0.21") (d #t) (k 0)) (d (n "thread_local") (r "^1.0") (d #t) (k 0)))) (h "1vapjkwdrmbzvcm9aycf7qpy4nf3m1w31cxyp7y6zrijqfndsil4") (y #t)))

(define-public crate-inmemory-sqlite-0.1.6 (c (n "inmemory-sqlite") (v "0.1.6") (d (list (d (n "rusqlite") (r "^0.21") (d #t) (k 0)) (d (n "thread_local") (r "^1.0") (d #t) (k 0)))) (h "1mcc1wk1h6hlhb2w27rravzv6v963j7cvi3863b7wl22131zbb65") (y #t)))

(define-public crate-inmemory-sqlite-0.1.7 (c (n "inmemory-sqlite") (v "0.1.7") (d (list (d (n "rusqlite") (r "^0.21") (d #t) (k 0)) (d (n "thread_local") (r "^1.0") (d #t) (k 0)))) (h "00dmw914kinklvlddn9wxfz3yawxy0rdmjqzddlwblfk5l83d084") (y #t)))

(define-public crate-inmemory-sqlite-0.1.8 (c (n "inmemory-sqlite") (v "0.1.8") (d (list (d (n "rusqlite") (r "^0.21") (d #t) (k 0)) (d (n "thread_local") (r "^1.0") (d #t) (k 0)))) (h "1pxjcrqdwfk9sdrbhx9isk34231gfszb7b9sa6ch8bbayh47z8hb") (y #t)))

(define-public crate-inmemory-sqlite-0.1.9 (c (n "inmemory-sqlite") (v "0.1.9") (d (list (d (n "rusqlite") (r "^0.21") (d #t) (k 0)) (d (n "thread_local") (r "^1.0") (d #t) (k 0)))) (h "0fp1lzbwlhw85l5m6q3s7lzd6wj6k9an22mq6fw5gz99wkg0cnvz") (y #t)))

(define-public crate-inmemory-sqlite-0.1.10 (c (n "inmemory-sqlite") (v "0.1.10") (d (list (d (n "rusqlite") (r "^0.21") (d #t) (k 0)) (d (n "thread_local") (r "^1.0") (d #t) (k 0)))) (h "1fbzd5piv53p3bfmg68igyj2s8lg8lkmb0hqnklhphizc43jp7r8") (y #t)))

