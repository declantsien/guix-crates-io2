(define-module (crates-io in _a in_addr) #:use-module (crates-io))

(define-public crate-in_addr-0.1.0 (c (n "in_addr") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("inaddr"))) (d #t) (t "cfg(windows)") (k 0)))) (h "19gdm7bg32l6ysp45641vjx2yb7jdlj7f78zv40p6c3k9f1p07vw") (f (quote (("no-std") ("default"))))))

(define-public crate-in_addr-0.1.1 (c (n "in_addr") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("inaddr"))) (d #t) (t "cfg(windows)") (k 0)))) (h "006klff8b948zzx0hjc9vhv5v4ik0dvhfi2a4bisff7n3kxsyndr") (f (quote (("no-std") ("default"))))))

(define-public crate-in_addr-0.1.2 (c (n "in_addr") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("inaddr"))) (d #t) (t "cfg(windows)") (k 0)))) (h "05n7353hkln8jp1q0cilwyydr69vq80l4ndk71kjmpgfs4zcvkpl") (f (quote (("no-std") ("default"))))))

(define-public crate-in_addr-0.2.0 (c (n "in_addr") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("inaddr"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1j2qyqglb6yfa2if5a8ik255nkf01pac7hfqfy47r92pm5hxw66k") (f (quote (("std") ("default" "std"))))))

(define-public crate-in_addr-1.0.0 (c (n "in_addr") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("inaddr"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0m614icsc433vg3kf2f5jzv2y1rabccy3r1d8sxndiwk70dy288r") (f (quote (("std") ("default" "std"))))))

