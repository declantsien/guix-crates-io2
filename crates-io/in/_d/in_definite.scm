(define-module (crates-io in _d in_definite) #:use-module (crates-io))

(define-public crate-in_definite-0.1.0 (c (n "in_definite") (v "0.1.0") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0m6xbjmlmwglg8hzhmzi0nhjqnddxlan1msc42azp9yw4ywnr121")))

(define-public crate-in_definite-0.1.1 (c (n "in_definite") (v "0.1.1") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "06ygdyw61nr9z4ghsy7yyf4w0xxxznigqvrrb22kwhs245j3q4kp")))

(define-public crate-in_definite-0.1.2 (c (n "in_definite") (v "0.1.2") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0vha51pkzkf4z83jnf2c7va9igvf22zghs5fbjq4lmwndzk7zdac")))

(define-public crate-in_definite-0.1.3 (c (n "in_definite") (v "0.1.3") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1i55xk61ifq79shv9q2zzca0rqpyk8rpbp225ii577j2rsz4f0yq")))

(define-public crate-in_definite-0.1.4 (c (n "in_definite") (v "0.1.4") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0fp6w0dpxyfzcb47imp2sy0da1zy8vb7kp4s03l341qb7dkggv67")))

(define-public crate-in_definite-0.1.5 (c (n "in_definite") (v "0.1.5") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1mddnip510i78926qgwzaa6sx5pxprnx5pi1l11m355xrghd95qa")))

(define-public crate-in_definite-0.1.6 (c (n "in_definite") (v "0.1.6") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1mm4bly2gks2p5bfmay8s02d7kfxyb4yzq2h9jjlfkiwxxq3s9kg")))

(define-public crate-in_definite-0.1.7 (c (n "in_definite") (v "0.1.7") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1hsy9624c0dnm7zvfrgbrydbkpzlchha605x7a20v7gvb11sg4p6")))

(define-public crate-in_definite-0.1.8 (c (n "in_definite") (v "0.1.8") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "15klig6vafhggdrddvicsafrv006s36b4r1ppm5li0kaq4sb32d5")))

(define-public crate-in_definite-0.1.9 (c (n "in_definite") (v "0.1.9") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1k24rhb8aj89mxrr2f340kn0jq7rvk6cdpscg13p4g9d53f5b6ry")))

(define-public crate-in_definite-0.1.10 (c (n "in_definite") (v "0.1.10") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1vvj8w91gdw0d24dpgcw5j11q377amd6vhq5fvjbla5angpv41pn")))

(define-public crate-in_definite-0.2.0 (c (n "in_definite") (v "0.2.0") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0x6fpxa91xakcajbbz3mfm3wwi6iqn134g4gy2d5yw10izp34lkx")))

(define-public crate-in_definite-0.2.1 (c (n "in_definite") (v "0.2.1") (d (list (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1avlkpj2hxir1xjvw346xcws4jsll4kbwcxq35mar4mdj4nxx6rn")))

(define-public crate-in_definite-0.2.2 (c (n "in_definite") (v "0.2.2") (h "1q15k89phdirb8df9q4glclmh0kh46yj6m3n3nqsvcz111pjf2zv")))

(define-public crate-in_definite-0.2.3 (c (n "in_definite") (v "0.2.3") (h "1wlc8941k9h4gwi12lqc08sh79wzyg9c4mqq62p93kl52vna6mx6")))

(define-public crate-in_definite-0.2.4 (c (n "in_definite") (v "0.2.4") (h "1312ppkbqdq08839j8js0n78x662apzlcbx66ic1xaql9dkz0hzg")))

(define-public crate-in_definite-0.2.5 (c (n "in_definite") (v "0.2.5") (h "1q100af8cm08zs42pb703gj9dfpv8jqmspa7b9k7gkzz6av52s59")))

(define-public crate-in_definite-1.0.0 (c (n "in_definite") (v "1.0.0") (h "1pl1ndjw8hzgpz7rws04pvy41xrjjxddckgzlwm8938qr2ilggdm")))

