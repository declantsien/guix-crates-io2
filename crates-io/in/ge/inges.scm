(define-module (crates-io in ge inges) #:use-module (crates-io))

(define-public crate-inges-0.1.0 (c (n "inges") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gkzr8x3g8rkqy1c42xagp7bwhzk1cyq6lsygw36rajf3rqkzw7f")))

(define-public crate-inges-0.2.0 (c (n "inges") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "18fmrvjf4p6hxk9h6s7l0rm6xc2d14px89v30fqmdvk3vm89y2rp")))

