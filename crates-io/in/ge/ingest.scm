(define-module (crates-io in ge ingest) #:use-module (crates-io))

(define-public crate-ingest-0.0.1 (c (n "ingest") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "stellar-xdr") (r "^0.0.17") (f (quote ("std" "curr" "next" "base64" "serde"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "18y125d34sz0vgha381p3i2gb5lyxs3vz8ybpbmiq39yrb6g0nn6")))

(define-public crate-ingest-0.0.2 (c (n "ingest") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "stellar-xdr") (r "^0.0.17") (f (quote ("std" "curr" "next" "base64" "serde"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0x5r9a4842y147vvvgq00wwmwxmp0sgs31lqgrm3iiwrwnwzyp1a")))

(define-public crate-ingest-0.0.2-alpha.0 (c (n "ingest") (v "0.0.2-alpha.0") (d (list (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "stellar-xdr") (r "^0.0.17") (f (quote ("std" "curr" "next" "base64" "serde"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0qschv28va8fan2x8jwfg9l7ij3b7y2irmihxb667kq35dkznqih") (y #t)))

(define-public crate-ingest-0.0.3-alpha.0 (c (n "ingest") (v "0.0.3-alpha.0") (d (list (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "stellar-xdr") (r "^0.0.17") (f (quote ("std" "curr" "next" "base64" "serde"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "02s4r001mglxqlzif34d9bz2vrg8g34xghyg74bnib1mgvx37vzw")))

(define-public crate-ingest-0.0.3-alpha.0.1 (c (n "ingest") (v "0.0.3-alpha.0.1") (d (list (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "stellar-xdr") (r "^0.0.17") (f (quote ("std" "curr" "next" "base64" "serde"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0agdk4xm8d24n4xpqa0xvrf3zvipiw7b2gzq8jkzy7m1kciz70rn")))

(define-public crate-ingest-0.0.3 (c (n "ingest") (v "0.0.3") (d (list (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "stellar-xdr") (r "^20.0.0-rc1") (f (quote ("std" "curr" "next" "base64" "serde"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0s4sip8vy0y0lrsh310d731v5mxqrz1gwajr9bmxmfim6pqp5zg6")))

