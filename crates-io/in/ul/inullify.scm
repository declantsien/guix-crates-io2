(define-module (crates-io in ul inullify) #:use-module (crates-io))

(define-public crate-inullify-0.1.0 (c (n "inullify") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "inotify") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0wx41jibgjq088bfaqrn3swdfpaa3f7prcr59n4a8r7zsdc7cjyw")))

(define-public crate-inullify-0.1.1 (c (n "inullify") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "inotify") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1c0m3plpgpvvdlpy04bjmxkn3gah8ldhbh5r5ni04l6vsn5xp4v1")))

(define-public crate-inullify-0.1.2 (c (n "inullify") (v "0.1.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "inotify") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "09k0wwfk42kmp0fx9n5sxvphw5hixb76nhqnmqn14cppkgs6rglb")))

(define-public crate-inullify-0.1.3 (c (n "inullify") (v "0.1.3") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "inotify") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "01ckhscr7qk8v6cdhawid2i9xsa26ayd1vxk5pra5p297qwjac1b")))

(define-public crate-inullify-0.1.4 (c (n "inullify") (v "0.1.4") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "inotify") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1sslh6c110sy3jhrlfylizng0fdf59r5mig9alill43kgn8dqpka")))

