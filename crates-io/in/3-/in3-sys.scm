(define-module (crates-io in #{3-}# in3-sys) #:use-module (crates-io))

(define-public crate-in3-sys-0.0.1 (c (n "in3-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.53.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vfcf9lf13y1wgwf5fgdnf1pkiv71lmsyzyvri6vpik8msxm0fm6") (l "in3")))

(define-public crate-in3-sys-0.0.2 (c (n "in3-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.53.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vs7amlrqlqy73pgv3zfv4cb5mpcbbgwq4knizidq50b52s828l3") (l "in3")))

(define-public crate-in3-sys-0.1.4 (c (n "in3-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.53.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1593xcic5nd88pi5s1w4zpjqhzfsmspf7jgh4s16w4gn5c5npsyl") (l "in3")))

(define-public crate-in3-sys-0.1.5 (c (n "in3-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.53.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "049mjraq983hk8drzn40jdqn6b3smkbmlwg2pk1fwpd6jv73rky4") (l "in3")))

(define-public crate-in3-sys-0.1.6 (c (n "in3-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.53.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "146rbw3mnw4bfvng4763naw4pz0xbyg6gknar3mdvcilf67a66g3") (l "in3")))

(define-public crate-in3-sys-3.0.6 (c (n "in3-sys") (v "3.0.6") (d (list (d (n "bindgen") (r "^0.53.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1i013n64wrs43lrqscwmiz79z9lgyhbnb19l3v8qj4b8jqjvd5pg") (l "in3")))

(define-public crate-in3-sys-3.1.0 (c (n "in3-sys") (v "3.1.0") (d (list (d (n "bindgen") (r "^0.53.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gv9ds2hcyvf1dbd1hbhspd64zfs2ajqgi49fbyyc9cj49px4s9d") (l "in3")))

(define-public crate-in3-sys-3.3.0 (c (n "in3-sys") (v "3.3.0") (d (list (d (n "bindgen") (r "^0.53.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0h9bgvwg4lpp79pj9xbd8xbnz1ls0rg454ijrp4j1xx6bbh5vknd") (l "in3")))

(define-public crate-in3-sys-3.3.3 (c (n "in3-sys") (v "3.3.3") (d (list (d (n "bindgen") (r "^0.53.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gwapis67cxxg44jmdkrh4pxzb2cnznxpwavlpp3lbzrl82lqxi3") (l "in3")))

