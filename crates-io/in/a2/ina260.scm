(define-module (crates-io in a2 ina260) #:use-module (crates-io))

(define-public crate-ina260-0.1.0 (c (n "ina260") (v "0.1.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.1.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0nayq61sz6kfs2lab1rqqjpcl88gby6f0lb0b4ziddfrm0rxlkjb")))

(define-public crate-ina260-0.1.1 (c (n "ina260") (v "0.1.1") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.1.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0gi0q3b4hw1zpa30r0v1m1yfswmzn1n2d7jd0ljcwxdzgkfwh29h")))

(define-public crate-ina260-0.2.0 (c (n "ina260") (v "0.2.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.0") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1vyi6k4f6akd1l2svnxdvs0b780zyl0m6iw44csc69jbjzlvrjdk")))

(define-public crate-ina260-0.2.1 (c (n "ina260") (v "0.2.1") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0bi0f4xklmqn62hhh8bqv3v88wfxn1x9xs0j9lzr245cs6n6x6m3")))

(define-public crate-ina260-0.2.2 (c (n "ina260") (v "0.2.2") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0jfx9c86nl1zf6i7yxhn59mf5kyrfll8abi0dqv02k161k0sp00y")))

(define-public crate-ina260-0.2.3 (c (n "ina260") (v "0.2.3") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)))) (h "10ma0409nyxv5dni3bqz8l301m9xfbz154bws1lyj0bvwz1d43sg")))

(define-public crate-ina260-0.2.4 (c (n "ina260") (v "0.2.4") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1gczq1wx5lhh5ymw5y8ap06v382xfhcwwx5xm6gsdh6h0dsh38yf")))

(define-public crate-ina260-0.2.5 (c (n "ina260") (v "0.2.5") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "08syvl84582v0l2hhhysp32b8mcws6ac9nsjwddbcdsiryzzi8m6")))

(define-public crate-ina260-0.2.6 (c (n "ina260") (v "0.2.6") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0dlin41pk6nr0lr0qnp76ls4781ya22cjyyxky4356x2awb3zhw8")))

(define-public crate-ina260-0.3.0 (c (n "ina260") (v "0.3.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)))) (h "17qcswpfnfmcfg00ss93karlm0si02pi4qvrrhbwn0z1a2b21011")))

(define-public crate-ina260-0.3.1 (c (n "ina260") (v "0.3.1") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0axpx0jgndns675sbagbiw4mvgzf5s33nmmb0nnjlr0fz62v9715")))

