(define-module (crates-io in a2 ina260-async) #:use-module (crates-io))

(define-public crate-ina260-async-0.1.0 (c (n "ina260-async") (v "0.1.0") (d (list (d (n "bondrewd") (r "^0.1.14") (f (quote ("derive" "hex_fns" "slice_fns"))) (k 0)) (d (n "defmt") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "0rk4hz1aq3aqrjhxpikc12bzhvlhc10s7ayl7zdfk9aqmhdya7bp") (f (quote (("default")))) (y #t) (s 2) (e (quote (("defmt" "dep:defmt"))))))

