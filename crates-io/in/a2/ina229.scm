(define-module (crates-io in a2 ina229) #:use-module (crates-io))

(define-public crate-ina229-0.1.0 (c (n "ina229") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "embedded-hal-mock") (r "^0.9") (d #t) (k 2)))) (h "106dfz1w8p6fhk5h234wnlfy7f1pd4wbs17sn9x1m1h5kc8pzi6l")))

