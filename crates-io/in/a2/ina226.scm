(define-module (crates-io in a2 ina226) #:use-module (crates-io))

(define-public crate-ina226-0.1.0 (c (n "ina226") (v "0.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)))) (h "0vdpf1l17isk19c156rl029zizlpmx77cpzh9wcnjqw42rddv74d")))

(define-public crate-ina226-0.1.1 (c (n "ina226") (v "0.1.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (d #t) (k 2)))) (h "10gsxyr9a3l01w9ngb7wjaqsxfg7c4ybbdqmb70s0qri8n2alwp8")))

(define-public crate-ina226-0.2.0 (c (n "ina226") (v "0.2.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (d #t) (k 2)))) (h "0y1xsibmfqphlkpwlkx6s7cjlc5hhiixnyl6494fadfj3ljw5m1k")))

(define-public crate-ina226-0.3.0 (c (n "ina226") (v "0.3.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (d #t) (k 2)) (d (n "maybe-async-cfg") (r "^0.2.3") (d #t) (k 0)))) (h "0b5x9hggvikjgrs8axb0al4fd20zqnxvgb2v39qi8cawgz180kkd") (s 2) (e (quote (("async" "dep:embedded-hal-async"))))))

