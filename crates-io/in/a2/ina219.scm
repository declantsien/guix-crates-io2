(define-module (crates-io in a2 ina219) #:use-module (crates-io))

(define-public crate-ina219-0.0.2 (c (n "ina219") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.2") (d #t) (k 0)))) (h "0vh9rz2pff8zf94xa5f4jlzrrqd4xranlha1bzhaws53mj06npyl")))

(define-public crate-ina219-0.1.0 (c (n "ina219") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.1") (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "0zk41sc0kn68nf4jjfnmz2gr1bwc7rzljzlzswjmbclh4psdr4va")))

