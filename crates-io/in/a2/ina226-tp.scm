(define-module (crates-io in a2 ina226-tp) #:use-module (crates-io))

(define-public crate-ina226-tp-0.1.0 (c (n "ina226-tp") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9.0") (d #t) (k 2)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "1gb1aqhzh5m4n3ry8m1k4g1sibh15r6rk45jw7nphmgsjc3y4ffb")))

(define-public crate-ina226-tp-0.2.0 (c (n "ina226-tp") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10.0") (d #t) (k 2)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)))) (h "0wip1qb97l2kflq80i629qzjkphfxzn0y7xsdcqdjyp93jakf6b4")))

