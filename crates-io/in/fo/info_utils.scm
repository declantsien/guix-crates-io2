(define-module (crates-io in fo info_utils) #:use-module (crates-io))

(define-public crate-info_utils-1.0.0 (c (n "info_utils") (v "1.0.0") (h "0ixj9dsvb6w5s5lsz1iijdirl67hlkgvq11awc0k752ra66h34qy")))

(define-public crate-info_utils-1.0.1 (c (n "info_utils") (v "1.0.1") (h "0nkr0pv9v28szw58kf1hczd59ffxrz6lw5z76xfnr2vp9i9k7p17")))

(define-public crate-info_utils-1.2.0 (c (n "info_utils") (v "1.2.0") (h "02k5m426m6crirwc69iy31k16dz3dx744c49k0qd2cz8rjbshl7w")))

(define-public crate-info_utils-1.3.0 (c (n "info_utils") (v "1.3.0") (h "1xsrsnj0p45jip3zmw3dgsrxnbykpsxcfii30c2g0pxqfy3676ll")))

(define-public crate-info_utils-1.3.1 (c (n "info_utils") (v "1.3.1") (h "1rcac261fz22gf8qkak7hwz7875nnqpr6gk9k3yfvrsr21zr5sqi")))

(define-public crate-info_utils-2.0.0 (c (n "info_utils") (v "2.0.0") (h "0dmcdf965fr4ymvdw2fjab4hzlqypf4dvrmp1c1w6dz2ydmsvskx")))

(define-public crate-info_utils-2.2.0 (c (n "info_utils") (v "2.2.0") (h "0l190d02kfjcfrvq6va01j3hadc110b9xpajafg9d2v2f62zkbdn")))

(define-public crate-info_utils-2.2.1 (c (n "info_utils") (v "2.2.1") (h "0pl6p9shykd58jnw2b4khsmlfg1vraa6bb12ypyqd6mrmwyv66h6")))

(define-public crate-info_utils-2.2.2 (c (n "info_utils") (v "2.2.2") (h "0aj9l6x3g8ncsv5l6n9sfzvkvwcfx1pjm7dvznzdsy674cmhqcmk")))

(define-public crate-info_utils-2.2.3 (c (n "info_utils") (v "2.2.3") (h "125hz046n0mhsbcww40his208pr4r0n14fcli0cj4p55gf91y7sv")))

