(define-module (crates-io in fo infoit) #:use-module (crates-io))

(define-public crate-infoit-0.0.0 (c (n "infoit") (v "0.0.0") (d (list (d (n "infoit-derive") (r "^0.0.0") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "viewit") (r "^0.0.1") (d #t) (k 0)))) (h "0frdm267xwhb2ajxf9lgng1lk55n44fkw9f4z89m67gjwhv099fi") (f (quote (("nightly" "infoit-derive/nightly") ("default" "serde" "infoit-derive/default"))))))

(define-public crate-infoit-0.1.1 (c (n "infoit") (v "0.1.1") (d (list (d (n "infoit-derive") (r "^0.0.0") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "viewit") (r "^0.0.1") (d #t) (k 0)))) (h "0bnzf7snkn1bxi3mjr2wlw1vfjbrz0xlg4n8gphfr449rjq6fa5d") (f (quote (("nightly" "infoit-derive/nightly") ("default" "serde" "infoit-derive/default"))))))

(define-public crate-infoit-0.1.2 (c (n "infoit") (v "0.1.2") (d (list (d (n "infoit-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "viewit") (r "^0.1.2") (d #t) (k 0)))) (h "15dmf20zl64mlmh9m0whg8dlrbjzwp2cjqk8ipg4kbgc344xqslg") (f (quote (("default" "serde"))))))

