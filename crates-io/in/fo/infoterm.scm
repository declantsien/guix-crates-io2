(define-module (crates-io in fo infoterm) #:use-module (crates-io))

(define-public crate-infoterm-0.1.0 (c (n "infoterm") (v "0.1.0") (d (list (d (n "dirs") (r "^5.0.1") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "09xyg317i9qsxjbn68fv8wp8dgh0wfgwrr5q90lk3d7myp03g4qs") (f (quote (("search" "dirs") ("index" "phf") ("expand") ("default" "index" "expand" "search"))))))

(define-public crate-infoterm-0.1.1 (c (n "infoterm") (v "0.1.1") (d (list (d (n "dirs") (r "^5.0.1") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1j7yl41gl045b3maa092dxh556r6z9gdpbdzb3dzqbxaahh547rj") (f (quote (("search" "dirs") ("index" "phf") ("expand") ("default" "index" "expand" "search"))))))

