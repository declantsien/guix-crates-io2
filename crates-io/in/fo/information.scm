(define-module (crates-io in fo information) #:use-module (crates-io))

(define-public crate-information-0.1.0 (c (n "information") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.63") (d #t) (k 0)) (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)))) (h "0ysfz6rfa79b4caand8vckch7wmp3iyzjz501ya7cvw18yw5v8h8")))

(define-public crate-information-0.1.1 (c (n "information") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.63") (d #t) (k 0)) (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)))) (h "12j7w5bzw04059w2avyf1dz4jbznj4gapsm1y7zimwhsgj252hg1")))

