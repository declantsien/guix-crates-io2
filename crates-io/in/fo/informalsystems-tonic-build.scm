(define-module (crates-io in fo informalsystems-tonic-build) #:use-module (crates-io))

(define-public crate-informalsystems-tonic-build-0.5.2 (c (n "informalsystems-tonic-build") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.8") (o #t) (d #t) (k 0) (p "informalsystems-prost-build")) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02mgcpdf0fcf9xapimmfxl6ncigssdlbcw5sfbyg2726p33h5qyg") (f (quote (("transport") ("rustfmt") ("prost" "prost-build") ("default" "transport" "rustfmt" "prost") ("compression"))))))

