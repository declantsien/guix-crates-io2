(define-module (crates-io in fo informalsystems-prost-types) #:use-module (crates-io))

(define-public crate-informalsystems-prost-types-0.8.1 (c (n "informalsystems-prost-types") (v "0.8.1") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.8.1") (f (quote ("prost-derive"))) (k 0) (p "informalsystems-prost")))) (h "06j36kh5giry62v5l18alnqh667gy67aaii1357cadhq1zc99c21") (f (quote (("std" "prost/std") ("default" "std"))))))

