(define-module (crates-io in fo infogetter) #:use-module (crates-io))

(define-public crate-infogetter-0.0.14 (c (n "infogetter") (v "0.0.14") (h "02hi9jsj5p2hg9r5pl3y1l3jhdlpb9pl6z3abg09rdpwqng2bhxx")))

(define-public crate-infogetter-0.0.15 (c (n "infogetter") (v "0.0.15") (h "1zqqrp3zgb4ymcj7pqf9zswc1c6nj0597fya9nxllrcxdswqz1bd")))

(define-public crate-infogetter-0.0.16 (c (n "infogetter") (v "0.0.16") (h "06la0qmnkninqx6lira6f3xz0lz1pn5v49ss0qpiggq1xmbrvjh9")))

