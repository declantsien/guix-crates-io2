(define-module (crates-io in fo informa) #:use-module (crates-io))

(define-public crate-informa-0.1.0 (c (n "informa") (v "0.1.0") (h "1kg3c1dycqk54dvn77fbrxfgqhzjjb8i1f9f0a1nlmqydh3pwxl0")))

(define-public crate-informa-0.2.0 (c (n "informa") (v "0.2.0") (d (list (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "permutohedron") (r "^0.2.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.2") (d #t) (k 2)) (d (n "spans") (r "^1.0.0") (d #t) (k 0)))) (h "0lwf8zk6xc7nsghcylgyghmivd0yisrgxq1k1j1h6y2crhk5jhhs")))

