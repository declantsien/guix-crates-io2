(define-module (crates-io in fo informalsystems-prost-derive) #:use-module (crates-io))

(define-public crate-informalsystems-prost-derive-0.8.1 (c (n "informalsystems-prost-derive") (v "0.8.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1fly67frqmvbr7y87p5nfwh5dwsb11g0b650kf3pzg7m9rczcsiw")))

