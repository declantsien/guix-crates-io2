(define-module (crates-io in fo informal) #:use-module (crates-io))

(define-public crate-informal-1.0.0 (c (n "informal") (v "1.0.0") (d (list (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "10bxmxbpf27bs85ls5mii7pczwy9b6hiymw6blp3w7p3s8203290") (y #t)))

(define-public crate-informal-1.0.1 (c (n "informal") (v "1.0.1") (d (list (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "10kyc044jn483hz29r2sn508kv8xc981mirgjbpi76sxcgz3n4cz") (y #t)))

(define-public crate-informal-1.0.2 (c (n "informal") (v "1.0.2") (d (list (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "0qzcfagf0iq0hx0mqxgxnih6v1r5q7lzr9r1wldsnxdh5wxnr0d7") (y #t)))

(define-public crate-informal-2.0.0 (c (n "informal") (v "2.0.0") (d (list (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "1bj8fkk0r52mrjn8p7i5qh9m1sssxdwziwyvl6cpbb9fyngvl110") (y #t)))

(define-public crate-informal-2.1.0 (c (n "informal") (v "2.1.0") (d (list (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "0gd0kpcag2kjkcif7fdgipf53q0p9l5w7cjlzpa35r8s99i5iwnh") (y #t)))

(define-public crate-informal-3.0.0 (c (n "informal") (v "3.0.0") (d (list (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "1zfyf0c898mccdfpfysk32bkpa80yxw6da0301kcrkxixwkvx44a") (y #t)))

(define-public crate-informal-3.1.0 (c (n "informal") (v "3.1.0") (d (list (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "1hn1ci1091m69s3dvf4nf2ll9p6lj8q9pq3q1aaq4raad34kl00f") (y #t)))

(define-public crate-informal-4.0.0 (c (n "informal") (v "4.0.0") (d (list (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "1djgcm9mzs2lhf43p4awsnzdk9m78xdbm50ysq4pvby5iwddrck6") (y #t)))

(define-public crate-informal-4.1.0 (c (n "informal") (v "4.1.0") (d (list (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "1p65g872v8jxln1abld92n0hh5wa4k7z29fk4ipyx762jjwm55xc") (y #t)))

(define-public crate-informal-4.1.1 (c (n "informal") (v "4.1.1") (d (list (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "00rrnvzaq4ffv6j7a2090lws8rcbhbnx9bwxbb83jfjkbjxrk250") (y #t)))

