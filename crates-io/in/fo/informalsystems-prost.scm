(define-module (crates-io in fo informalsystems-prost) #:use-module (crates-io))

(define-public crate-informalsystems-prost-0.8.1 (c (n "informalsystems-prost") (v "0.8.1") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.8") (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "prost-derive") (r "^0.8.1") (o #t) (d #t) (k 0) (p "informalsystems-prost-derive")) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0yv4mx62mjw73hd3jjkzv7jwlrbk80i7iq73prqpdhjk6qfy882n") (f (quote (("std") ("no-recursion-limit") ("default" "prost-derive" "std"))))))

