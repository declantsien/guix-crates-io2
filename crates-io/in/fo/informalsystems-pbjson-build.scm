(define-module (crates-io in fo informalsystems-pbjson-build) #:use-module (crates-io))

(define-public crate-informalsystems-pbjson-build-0.6.0 (c (n "informalsystems-pbjson-build") (v "0.6.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "0m406vf62spwkv9xnv82mb479axpfcwvacmgwmg6i2g8h5b3abv7")))

(define-public crate-informalsystems-pbjson-build-0.7.0 (c (n "informalsystems-pbjson-build") (v "0.7.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "066ky09vb1ybyysw0v6wqrqpirmz9vkywkl5mqag5wa59vg5rm3b")))

