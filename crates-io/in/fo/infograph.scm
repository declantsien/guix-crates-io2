(define-module (crates-io in fo infograph) #:use-module (crates-io))

(define-public crate-infograph-0.1.0 (c (n "infograph") (v "0.1.0") (h "0szl429ff847xvq7jbw5kxqgnvxcq149np55vqirib6ib3ci3x3g")))

(define-public crate-infograph-0.1.1 (c (n "infograph") (v "0.1.1") (h "1bimcnzwg7gh87mvndlaj0k9w17l0wqbffnlm36lmkhd6fv3c7bm")))

