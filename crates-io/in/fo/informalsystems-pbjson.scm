(define-module (crates-io in fo informalsystems-pbjson) #:use-module (crates-io))

(define-public crate-informalsystems-pbjson-0.6.0 (c (n "informalsystems-pbjson") (v "0.6.0") (d (list (d (n "base64") (r "^0.13") (f (quote ("alloc"))) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)))) (h "1yz1g9w2dfgj2jz8r2chh8991qxiysafzilimhp43skvz28cvvml") (f (quote (("std" "serde/std" "base64/std") ("default" "std"))))))

(define-public crate-informalsystems-pbjson-0.7.0 (c (n "informalsystems-pbjson") (v "0.7.0") (d (list (d (n "base64") (r "^0.21") (f (quote ("alloc"))) (k 0)) (d (n "bytes") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "alloc"))) (k 0)))) (h "0g9s1qab7zlsx3zrc3yaqvhjxpvq9qsm826p018jjyc31jca194s") (f (quote (("std" "serde/std" "base64/std") ("default" "std"))))))

