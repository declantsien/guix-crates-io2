(define-module (crates-io in t- int-conv) #:use-module (crates-io))

(define-public crate-int-conv-0.1.0 (c (n "int-conv") (v "0.1.0") (d (list (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "1wvf4xqb1y8zi318dsjbj419bh1jl9352rj4ynwpagq5yapx2z6k")))

(define-public crate-int-conv-0.1.1 (c (n "int-conv") (v "0.1.1") (d (list (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "1b0fmxf5ppq0l2rynl8bpjcbn1k7c33a8ifn6h02miqz6514vznm")))

(define-public crate-int-conv-0.1.2 (c (n "int-conv") (v "0.1.2") (d (list (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "0xbsf3w63x7b28s5736qbjmfi3qkp6wymby4f52qfk47d5l8zcf6")))

(define-public crate-int-conv-0.1.3 (c (n "int-conv") (v "0.1.3") (d (list (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "0hbgl13x9p9nq6b0xs2x2vpjbgvmfcz1mn2y2is7isi7bq2kbh9f")))

(define-public crate-int-conv-0.1.4 (c (n "int-conv") (v "0.1.4") (d (list (d (n "static_assertions") (r "^1.1") (d #t) (k 0)))) (h "0k7rw57wpjb7miqjsi60j8sncil6ahh08pm2lcd3fwknh4l9n950")))

