(define-module (crates-io in t- int-vec-2d) #:use-module (crates-io))

(define-public crate-int-vec-2d-0.1.0 (c (n "int-vec-2d") (v "0.1.0") (d (list (d (n "either") (r "^1.6.1") (k 0)) (d (n "enum-derive-2018") (r "^2.0.0") (k 0)) (d (n "macro-attr-2018") (r "^2.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "panicking") (r "^0.2.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "03h0x4nvg4g9dpnfkdhmd26h7yn51afpddng889qix41na9hbxd6")))

(define-public crate-int-vec-2d-0.1.1 (c (n "int-vec-2d") (v "0.1.1") (d (list (d (n "either") (r "^1.6.1") (k 0)) (d (n "enum-derive-2018") (r "^2.0.0") (k 0)) (d (n "macro-attr-2018") (r "^2.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "0j0npwg42ygpnq17szrbabwqc4fmml33487j7ya7r9ra74bx6afs")))

(define-public crate-int-vec-2d-0.1.2 (c (n "int-vec-2d") (v "0.1.2") (d (list (d (n "either") (r "^1.6.1") (k 0)) (d (n "enum-derive-2018") (r "^2.0.0") (k 0)) (d (n "macro-attr-2018") (r "^2.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "0xjwnpk3mcsgm5f8pgsagdx33ii428s0yznc7315djrf6lxqzq8n")))

(define-public crate-int-vec-2d-0.1.3 (c (n "int-vec-2d") (v "0.1.3") (d (list (d (n "either") (r "^1.6.1") (k 0)) (d (n "enum-derive-2018") (r "^3.0.1") (k 0)) (d (n "macro-attr-2018") (r "^3.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "0hkngpkxcvxw2npdlq787cgyywir185894p65y6awfzn76gkdrg4")))

(define-public crate-int-vec-2d-0.1.4 (c (n "int-vec-2d") (v "0.1.4") (d (list (d (n "either") (r "^1.6.1") (k 0)) (d (n "enum-derive-2018") (r "^3.0.1") (k 0)) (d (n "macro-attr-2018") (r "^3.0.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "0ljvy5fdn3vxahd1hyg5iymkv3z16xkjwwa1g2hlg0wq9qx03aq5")))

