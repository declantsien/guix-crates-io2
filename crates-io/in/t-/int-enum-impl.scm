(define-module (crates-io in t- int-enum-impl) #:use-module (crates-io))

(define-public crate-int-enum-impl-0.1.0 (c (n "int-enum-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vn40whv64k2pf2pgdc3h3w9ghhbvcvp4hf7kl29hfm9cbll8dc0")))

(define-public crate-int-enum-impl-0.2.0 (c (n "int-enum-impl") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0j6h7jdgpaxdb47nhgpql6p8pnr1bq0426p2i1a470gw0dxzclv5")))

(define-public crate-int-enum-impl-0.3.0 (c (n "int-enum-impl") (v "0.3.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jcg11yrvjwsng929dagspc5kkyh7a7l4gc74lfkjyr6zyaqf80i") (f (quote (("std") ("serialize") ("default") ("convert") ("alloc"))))))

(define-public crate-int-enum-impl-0.4.0 (c (n "int-enum-impl") (v "0.4.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "0w9nzfxgk18zz7pds95ncdxs1fg2dmlhql23101ab8ccmp5cxhx2") (f (quote (("std") ("serde") ("default") ("convert"))))))

(define-public crate-int-enum-impl-0.5.0 (c (n "int-enum-impl") (v "0.5.0") (d (list (d (n "proc-macro-crate") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0d2x5s8cwyq0phd1akg33s20qa995smzbxbpzjix3bbmhq32y7yz") (f (quote (("std") ("serde") ("convert")))) (r "1.60")))

