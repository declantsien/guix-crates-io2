(define-module (crates-io in t- int-compression-4-wise) #:use-module (crates-io))

(define-public crate-int-compression-4-wise-0.1.0 (c (n "int-compression-4-wise") (v "0.1.0") (h "1hv91amdn68yhbnh1mz5lihn63ddlk44iyn644d0wdjb9w72fznj")))

(define-public crate-int-compression-4-wise-0.1.1 (c (n "int-compression-4-wise") (v "0.1.1") (d (list (d (n "get-size") (r "^0.1.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.9.0") (d #t) (k 0)))) (h "0as5a2vx416why7g1s6k42q4021sg704nc8n1xv2vwib8snaw66j")))

