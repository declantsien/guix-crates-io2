(define-module (crates-io in si inside-baseball) #:use-module (crates-io))

(define-public crate-inside-baseball-0.1.0 (c (n "inside-baseball") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "byteordered") (r "^0.6.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 0)) (d (n "tracing-tree") (r "^0.2.1") (d #t) (k 0)))) (h "0s9dwa264fab46z30f88did16azmxlr9xdidns0hi3xr2k19gi7p") (f (quote (("strict"))))))

