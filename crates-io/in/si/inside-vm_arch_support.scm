(define-module (crates-io in si inside-vm_arch_support) #:use-module (crates-io))

(define-public crate-inside-vm_arch_support-0.2.0 (c (n "inside-vm_arch_support") (v "0.2.0") (h "0byz7my6scl77m66v6kf34qpc6x063pislv4wf32p3rkl2yg981c")))

(define-public crate-inside-vm_arch_support-0.3.0 (c (n "inside-vm_arch_support") (v "0.3.0") (h "1n1lfcb6m3822lrxbkqm60qqjx6dgnp93r0clw1dn97ndjwsjj4w")))

(define-public crate-inside-vm_arch_support-0.3.1 (c (n "inside-vm_arch_support") (v "0.3.1") (h "1hpqfbv6nk34x78jh65bfg3cyf45cc918b033vv7fzb5996ffpmj")))

(define-public crate-inside-vm_arch_support-0.3.2 (c (n "inside-vm_arch_support") (v "0.3.2") (h "1gyiss3qmbv24y7vkfclnrqkpn5hz92pq1adh59bm5rspi3pvcpq")))

