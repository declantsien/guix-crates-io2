(define-module (crates-io in si inside-vm) #:use-module (crates-io))

(define-public crate-inside-vm-0.1.0 (c (n "inside-vm") (v "0.1.0") (h "0i8ljak7yj0kpayhj52c2g2sifawc2xjp79wnbdwcf9jfps4972v")))

(define-public crate-inside-vm-0.2.0 (c (n "inside-vm") (v "0.2.0") (h "0a9qdw6awz94gibzhq21mxim5qi28x0hyf4cnh6j50k0724y461m")))

