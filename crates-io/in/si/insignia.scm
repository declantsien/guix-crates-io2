(define-module (crates-io in si insignia) #:use-module (crates-io))

(define-public crate-insignia-0.1.0 (c (n "insignia") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "lofty") (r "^0.1.1") (d #t) (k 0)))) (h "0c01lk51pqc4b7wnxd0hh7ar25a9wfphd0nscsg11qm0mdsj0df1")))

