(define-module (crates-io in si insideout) #:use-module (crates-io))

(define-public crate-insideout-0.1.0 (c (n "insideout") (v "0.1.0") (h "1cpy1kykcpaijlc74pg0s3ymfs75kdn7fxp703gjqlmyjqh7skpg")))

(define-public crate-insideout-0.2.0 (c (n "insideout") (v "0.2.0") (h "0rq9gdmgmbkdc77wbc4y5cggixraxirjcsggsyrl78drs3659ld2")))

