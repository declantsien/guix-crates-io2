(define-module (crates-io in si insim_smx) #:use-module (crates-io))

(define-public crate-insim_smx-1.0.0 (c (n "insim_smx") (v "1.0.0") (d (list (d (n "insim_core") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0v6knnmpzwd3bnls1cacila935sr21q2zxs8h3a66k3mv3sk5lli") (r "1.66")))

(define-public crate-insim_smx-1.1.0 (c (n "insim_smx") (v "1.1.0") (d (list (d (n "insim_core") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1zr7pqfj97jbnqd04516ncpw35xpzzialp0ws9r89id37mj74c0p") (r "1.66")))

