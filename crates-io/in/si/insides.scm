(define-module (crates-io in si insides) #:use-module (crates-io))

(define-public crate-insides-0.1.0 (c (n "insides") (v "0.1.0") (d (list (d (n "dilate") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.6") (d #t) (k 2)))) (h "11v1frs6bfpwkh9kphy74bmq0b9dkqdachliby1ch3bf9hbwhmb3")))

(define-public crate-insides-0.1.1 (c (n "insides") (v "0.1.1") (d (list (d (n "dilate") (r "^0.6.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.6") (d #t) (k 2)))) (h "0cpqzfrkgx6dn5795ni6k70hw3vj1w9f320x6mzh96k2g6wzgww7")))

(define-public crate-insides-0.1.2 (c (n "insides") (v "0.1.2") (d (list (d (n "dilate") (r "^0.6.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.6") (d #t) (k 2)))) (h "0ljnz201wdgf0lpza95mg1mhg381gimmhas11svx5r7yjfvfyhmv")))

