(define-module (crates-io in si insightface) #:use-module (crates-io))

(define-public crate-insightface-0.0.1 (c (n "insightface") (v "0.0.1") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.1") (d #t) (k 0)) (d (n "onnxruntime") (r "^0.0.14") (d #t) (k 0)))) (h "0bwd6mlk9yvj1jrh74yq5zgsl54a91g7l4f2r834p25bnhjirr7p")))

(define-public crate-insightface-0.0.2 (c (n "insightface") (v "0.0.2") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.1") (d #t) (k 0)) (d (n "onnxruntime") (r "^0.0.14") (d #t) (k 0)))) (h "1yvwhv2imafyjjrd2rq59k5c3gxl1bjlqr04ni40ns2c17c2r761")))

