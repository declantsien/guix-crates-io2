(define-module (crates-io in si insim_pth) #:use-module (crates-io))

(define-public crate-insim_pth-1.0.0 (c (n "insim_pth") (v "1.0.0") (d (list (d (n "insim_core") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "15c7lv0h6wqf620mw2v8dzphfhdml5ldimrqphlmm1k5sw6nnfs3") (r "1.66")))

(define-public crate-insim_pth-1.1.0 (c (n "insim_pth") (v "1.1.0") (d (list (d (n "insim_core") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1byd25wjwz364j2qjw84f8sbbp65igmzandn6w254x1px4xnrcz7") (r "1.66")))

