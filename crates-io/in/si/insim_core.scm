(define-module (crates-io in si insim_core) #:use-module (crates-io))

(define-public crate-insim_core-1.0.0-alpha.1 (c (n "insim_core") (v "1.0.0-alpha.1") (d (list (d (n "binrw") (r "^0.13.3") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "if_chain") (r "^1.0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0vp3ph7i62p07fiays3r6m5843whb6525fkw5bmnp0f49km7pgad") (r "1.66")))

(define-public crate-insim_core-1.0.0 (c (n "insim_core") (v "1.0.0") (d (list (d (n "binrw") (r "^0.13.3") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "if_chain") (r "^1.0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1x4m3msaqrzg6mjjmw1s8ljl7qdmw9yx9rw459wbjdb9n55b0835") (r "1.66")))

