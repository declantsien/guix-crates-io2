(define-module (crates-io in rs inrs) #:use-module (crates-io))

(define-public crate-inrs-0.1.0-pre.0 (c (n "inrs") (v "0.1.0-pre.0") (d (list (d (n "clap") (r "=3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "18rjlmzqcsa8a4y6x4b44f8ibga9fvwxdj1sm9mw1d4wq1pkf38g") (y #t) (r "1.61")))

(define-public crate-inrs-0.1.0-pre.1 (c (n "inrs") (v "0.1.0-pre.1") (d (list (d (n "clap") (r "=3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0xp4n8rqgfjfhggf2631v4l35v89jw728sdmlixy75b3pf8vbx8r") (y #t) (r "1.61")))

(define-public crate-inrs-0.1.0 (c (n "inrs") (v "0.1.0") (d (list (d (n "clap") (r "=3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1z1qmjlaq43axarwqll1967v5ff3dwkfxp33hph8f53g2y5wip61") (y #t) (r "1.61")))

(define-public crate-inrs-0.2.0 (c (n "inrs") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "comfy-table") (r "^7.0.1") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0rlcb751hjhc31w56w68z7672a20hyqw7p5wnw2s6yj2q91bzncb") (y #t) (r "1.61")))

(define-public crate-inrs-1.0.0 (c (n "inrs") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "comfy-table") (r "^7.0.1") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1nyi6m1r8hjlhzpj3q5nqlqcj6ai8s71pppqk6b7lnzc5g2187yp") (y #t) (r "1.70.0")))

(define-public crate-inrs-1.0.1 (c (n "inrs") (v "1.0.1") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "comfy-table") (r "^7.0.1") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "10ladf4lykl6ch1wjykb85x1vhd60w5r1v9jwcvjqd9c698a5jm6") (y #t) (r "1.70.0")))

