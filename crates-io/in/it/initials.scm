(define-module (crates-io in it initials) #:use-module (crates-io))

(define-public crate-initials-0.1.0 (c (n "initials") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.6.4") (d #t) (k 0)))) (h "1pglshrparqxn4imzrly5zm6lwzvk14b6qka3j6fvwzcbiq4gd2q")))

(define-public crate-initials-0.1.1 (c (n "initials") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.6.4") (d #t) (k 0)))) (h "15i966mhmgf5446n9cjzazf3b2lpq9dapsyndq58is6fnjzshc81")))

(define-public crate-initials-0.1.2 (c (n "initials") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.6.4") (d #t) (k 0)))) (h "0ydk5bg8sw6919gh347bvn6gn7v2zi92j9x7jpkcasvhcj4812sx")))

