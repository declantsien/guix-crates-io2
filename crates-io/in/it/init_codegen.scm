(define-module (crates-io in it init_codegen) #:use-module (crates-io))

(define-public crate-init_codegen-0.1.0 (c (n "init_codegen") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "01kr2g9qndj20igsgscrdz2msdnjbfqbrq8mq61l0finh5cfpg1k")))

(define-public crate-init_codegen-0.2.0 (c (n "init_codegen") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "1jn1vjgrz4pic50pphs7a5b4d7y0dg8a15hwg9x0hah8k79rklwg")))

