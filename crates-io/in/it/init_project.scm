(define-module (crates-io in it init_project) #:use-module (crates-io))

(define-public crate-init_project-0.1.1 (c (n "init_project") (v "0.1.1") (d (list (d (n "clap") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "17s5mrp4lw5a7ka13h2bvlmc1aqmvncwhyl30v81yvabjnmq6cmn")))

(define-public crate-init_project-0.1.3 (c (n "init_project") (v "0.1.3") (d (list (d (n "clap") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0ffbkiskvf0hbyy0a7csswrgaaz909lffqqcbb8acb21xchnq87w")))

(define-public crate-init_project-0.1.4 (c (n "init_project") (v "0.1.4") (d (list (d (n "clap") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0c6bqafsjjwjyjxgfhd77xhc895p6rj14cf03xrra43ln854plsw")))

(define-public crate-init_project-0.1.5 (c (n "init_project") (v "0.1.5") (d (list (d (n "clap") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0407ryf17n9ddznz0wqb47m31kw7cdvz2gx53vxh0w46x70kn8ha")))

