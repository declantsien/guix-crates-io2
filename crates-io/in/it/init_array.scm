(define-module (crates-io in it init_array) #:use-module (crates-io))

(define-public crate-init_array-0.1.0 (c (n "init_array") (v "0.1.0") (h "1c4wgzr8yxavaivvd9mz130m4h24hwikv2grznvdwakvvjfz8307") (f (quote (("nightly") ("default"))))))

(define-public crate-init_array-0.1.1 (c (n "init_array") (v "0.1.1") (h "0kwmqcgdps5nz1jap0vw6sb9f03lsc965zb336535frrck6pg0wx") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-init_array-0.1.2 (c (n "init_array") (v "0.1.2") (h "10w15rahdzm8naqh7dlhrkbrf1kk2sp8q2fgrjqpdimn2hgsgzcg") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-init_array-0.2.0 (c (n "init_array") (v "0.2.0") (h "0fhng9xw0naysgdvmjk6dhxank2f35ary12j4zrblirbh6bzh4gv") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-init_array-0.2.1 (c (n "init_array") (v "0.2.1") (h "1wsq4a4krrvz02wrbzamlpn0b2lbg442bl49s4bjjqdw6n52lp4b") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-init_array-0.2.2 (c (n "init_array") (v "0.2.2") (h "1z278ns6pwvsyhrwskldw912v1n9igzmcs2hx8gnj0q2vj6j3i11") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-init_array-0.3.0 (c (n "init_array") (v "0.3.0") (h "00wd94cy0zb86qk09zgwf21v6qmflq90v1lkdvyg65c3lbrxmi2i") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

