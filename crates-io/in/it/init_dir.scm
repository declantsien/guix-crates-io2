(define-module (crates-io in it init_dir) #:use-module (crates-io))

(define-public crate-init_dir-0.1.2 (c (n "init_dir") (v "0.1.2") (d (list (d (n "aok") (r "^0.1.11") (d #t) (k 2)) (d (n "ifs") (r "^0.1.32") (f (quote ("fs"))) (k 0)) (d (n "loginit") (r "^0.1.10") (d #t) (k 2)) (d (n "static_init") (r "^1.0.3") (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 2)))) (h "0fxp8sddk3nrjgkzzqnwyp99wqbyf3xgf70v8i592a3pgcbkm8wk")))

