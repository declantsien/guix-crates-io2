(define-module (crates-io in it initial_conditions) #:use-module (crates-io))

(define-public crate-initial_conditions-0.1.0 (c (n "initial_conditions") (v "0.1.0") (d (list (d (n "num") (r "^0.1.42") (d #t) (k 0)))) (h "111i9v87pda8jllcxg5v5qjqgaapnxs7k46rkgrgankyjwxdqasw")))

(define-public crate-initial_conditions-0.2.0 (c (n "initial_conditions") (v "0.2.0") (d (list (d (n "num") (r "^0.1.42") (d #t) (k 0)))) (h "0xyr521z8l9sjyvic9ka82z5006d550mqr95msvjw733l4s632sy")))

(define-public crate-initial_conditions-0.3.0 (c (n "initial_conditions") (v "0.3.0") (d (list (d (n "num") (r "^0.1.42") (d #t) (k 0)))) (h "1cdfmamhdg6zbigm9pzwbzf23xq5gnwxliaav3z772hmaw69ablh")))

(define-public crate-initial_conditions-0.3.1 (c (n "initial_conditions") (v "0.3.1") (d (list (d (n "num") (r "^0.1.42") (d #t) (k 0)))) (h "0r1h2fq2qscz4s71cvhypym8323y4jfx38m4c1smphjqldpgg3l3")))

(define-public crate-initial_conditions-0.3.2 (c (n "initial_conditions") (v "0.3.2") (d (list (d (n "num") (r "^0.1.42") (d #t) (k 0)))) (h "107x12ixfjw7v6z99nrq9nzk6j3185s13ajn5gqy59l4qg40bnxx")))

(define-public crate-initial_conditions-0.4.0 (c (n "initial_conditions") (v "0.4.0") (d (list (d (n "num") (r "^0.1.42") (d #t) (k 0)))) (h "08g9bjj9481n22mrklfsbb1n0rivqfbllsa8v6c5qpj3kxjidpvd")))

