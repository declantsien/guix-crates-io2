(define-module (crates-io in it initials-revamped) #:use-module (crates-io))

(define-public crate-initials-revamped-0.1.0 (c (n "initials-revamped") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "1nwnfnp4v6jn2vcl8vx9czjbgnss5sjiclkphaly788js8c740ij")))

(define-public crate-initials-revamped-0.1.1 (c (n "initials-revamped") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "1zydg9i7fbg3xx20naknajnkyb3h7dlakk0zkkvn4qzy7vpvfy8w")))

(define-public crate-initials-revamped-0.1.2 (c (n "initials-revamped") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "1kxal5n4hr9qd6lkfhw4pb17j62hpcngc85ymkpk2s3wy9lasrpa")))

