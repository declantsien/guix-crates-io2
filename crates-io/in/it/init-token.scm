(define-module (crates-io in it init-token) #:use-module (crates-io))

(define-public crate-init-token-0.1.0 (c (n "init-token") (v "0.1.0") (h "0smbqyrnhlgsasq0bj866c2m8jhsscgfaw1pp7hgbsacz2rn1m3q")))

(define-public crate-init-token-0.1.1 (c (n "init-token") (v "0.1.1") (h "0w455jbszyxbbrb7pbfjzlmlvk1gi29zn7h21xngq3m8lwdf6iv4")))

