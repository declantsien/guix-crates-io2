(define-module (crates-io in it init_with) #:use-module (crates-io))

(define-public crate-init_with-0.1.0 (c (n "init_with") (v "0.1.0") (d (list (d (n "nodrop") (r "^0.1.8") (k 0)))) (h "0c3qczbidzf049dgn4jhga25spmvjz2njzcz0iglb3xbyrc7qnb7")))

(define-public crate-init_with-0.1.1 (c (n "init_with") (v "0.1.1") (d (list (d (n "nodrop") (r "^0.1.8") (k 0)))) (h "0b762x65311b2y05iisrp373fx5g7p6w1w5hgvhxy9fkyizwkrp4")))

(define-public crate-init_with-0.1.2 (c (n "init_with") (v "0.1.2") (h "13a2vgj87nd116rc0i0mdnbiw218208ww5wp8aa4zq349h4a9h81")))

(define-public crate-init_with-1.0.0 (c (n "init_with") (v "1.0.0") (h "1rhwmx10awpzbbbf5plh9r6s9cmvfwm2vy3lbvq78rl33fgjsqg5")))

(define-public crate-init_with-1.0.1 (c (n "init_with") (v "1.0.1") (h "193fcxah940afmz46qnpi2nzpd0qv7003hwz4fm0mjkdpyl3yf7c")))

(define-public crate-init_with-1.0.2 (c (n "init_with") (v "1.0.2") (h "06h63z0ww5mq1j1ys9r5wpxcld075qffj7l7fw18snd2j8xvgdf7")))

(define-public crate-init_with-1.1.0 (c (n "init_with") (v "1.1.0") (h "16bn5f6l29cw6r774yniqlk55ij8rg85l5amywxih06f2lwgcx81")))

