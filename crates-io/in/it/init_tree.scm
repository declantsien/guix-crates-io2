(define-module (crates-io in it init_tree) #:use-module (crates-io))

(define-public crate-init_tree-0.1.0 (c (n "init_tree") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "0sbac34irdw5vq9y822g1mfxxh5ma2sqnryl2q4p2zglvnjajp7g")))

(define-public crate-init_tree-0.1.1 (c (n "init_tree") (v "0.1.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "0c51f8avz30nhwvk58pf6sa93vp9igi5a6inbmbnqbnqzrydfjyx")))

(define-public crate-init_tree-0.1.2 (c (n "init_tree") (v "0.1.2") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "11yyddjkga41d35jjasdjn5hwr2w36y3dyx28mgx0ai89brkxkfs")))

(define-public crate-init_tree-0.1.3 (c (n "init_tree") (v "0.1.3") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "0wzry9iq9in7vbv159mdf443cgah889rizxn80a236g41fyzlw2v")))

(define-public crate-init_tree-0.2.0 (c (n "init_tree") (v "0.2.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1r514s61d5jjm17hydgnq2bz5svy0p2z4z5x45w994yn4wsfqj7c") (f (quote (("default" "cache") ("cache" "serde"))))))

(define-public crate-init_tree-0.2.1 (c (n "init_tree") (v "0.2.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0lvzg8likqiwdzsjjyykd9xm8sq8shazi6q8nj3pzsslrrly73l7") (f (quote (("default" "cache") ("cache" "serde"))))))

(define-public crate-init_tree-0.2.2 (c (n "init_tree") (v "0.2.2") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0501akvwm8vw78lpxawxxcd26468i0icanhnjfmz5bk77a9jfgh3") (f (quote (("default" "cache") ("cache" "serde"))))))

