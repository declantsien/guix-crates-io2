(define-module (crates-io in it init-rs) #:use-module (crates-io))

(define-public crate-init-rs-0.0.1 (c (n "init-rs") (v "0.0.1") (h "0lqhkm37maj41kv3416fjybv10qqw2xh9pw3wml00wj741lvv1z6")))

(define-public crate-init-rs-0.0.2 (c (n "init-rs") (v "0.0.2") (h "1lda5lcs9yb8b9wfhjr3cs7difhdsbp1wpjlacasacjrnchcg4p6") (f (quote (("just") ("fd") ("default" "just"))))))

(define-public crate-init-rs-0.0.3 (c (n "init-rs") (v "0.0.3") (h "0p51xmln07fn6z8rsq4jp1w4gppg96jr26kx07fsinxq8j15cn5z") (f (quote (("just") ("fd") ("default" "just"))))))

(define-public crate-init-rs-0.0.4 (c (n "init-rs") (v "0.0.4") (d (list (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "102shklc0dz1mivslkwahwjfmd5h41ajniv6lj6xp0prd1y38saf") (f (quote (("just") ("fd") ("default" "just"))))))

(define-public crate-init-rs-0.0.5 (c (n "init-rs") (v "0.0.5") (d (list (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "0wab3jbdz8svnfxr1cjjl12n44sibv7l7pdzsqhwszj2kj08adjs") (f (quote (("just") ("fd") ("default" "just"))))))

(define-public crate-init-rs-0.0.6 (c (n "init-rs") (v "0.0.6") (d (list (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "0zqj9yrxzr30jk1r0qjl59vhh3rxv53b80fcdl82a0afypyy3wyz") (f (quote (("just") ("fd") ("default" "just"))))))

(define-public crate-init-rs-0.0.7 (c (n "init-rs") (v "0.0.7") (d (list (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "1yiglj449yb5kg3xm3q3l173br798p0xggk9manyxv2js1a15wzb") (f (quote (("just") ("fd") ("default" "just"))))))

(define-public crate-init-rs-0.0.8 (c (n "init-rs") (v "0.0.8") (d (list (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "0plby6r5ad7f1wx5jmsk9dnp46n0psmj4hfbqxn4317l282kaq47") (f (quote (("just") ("fd") ("default" "just"))))))

(define-public crate-init-rs-0.0.9 (c (n "init-rs") (v "0.0.9") (d (list (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "16b2nhqw9x2lyb2w6jpbbx91cfsivz1iylm7ms46c7jqrlmqp0qa") (f (quote (("just") ("fd") ("default" "just"))))))

(define-public crate-init-rs-0.0.11 (c (n "init-rs") (v "0.0.11") (d (list (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "1mkljx00x954lzcrv61pg6xhl9hz4f9zmrm6ih3fs5nn3bk62rxk") (f (quote (("just") ("fd") ("default" "just"))))))

(define-public crate-init-rs-0.0.16 (c (n "init-rs") (v "0.0.16") (d (list (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "0630c0h8hkpirwccfvh6kf5ppqxi556ylw7gfswl7vsfci5p26fy") (f (quote (("lll") ("just") ("fd") ("default" "just"))))))

(define-public crate-init-rs-0.0.17 (c (n "init-rs") (v "0.0.17") (d (list (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "1iqxm3sva79kpwgkinby8g5bla5g6d7glajbb2z12jl48km83wd4") (f (quote (("tcping") ("lll") ("just") ("fd") ("default" "just"))))))

