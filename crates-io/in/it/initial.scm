(define-module (crates-io in it initial) #:use-module (crates-io))

(define-public crate-initial-0.1.0 (c (n "initial") (v "0.1.0") (h "079hpaz7v20w8r6fmlxlni77jdb8idkh1nxsaqrb0v2rlmhcv5xv")))

(define-public crate-initial-0.1.1 (c (n "initial") (v "0.1.1") (d (list (d (n "cargo-bump") (r "^1.1.0") (d #t) (k 0)))) (h "17bmw1fd9hfdsf9m9drwki1cckxn8zqajxc2lw9bkhlii8r7p2c0")))

(define-public crate-initial-0.1.2 (c (n "initial") (v "0.1.2") (d (list (d (n "cargo-bump") (r "^1.1.0") (d #t) (k 0)))) (h "16lwkp9mn73vl0zxnl63x4famdi4xgrc0plh6yzfii28q96vlxm6")))

(define-public crate-initial-0.1.3 (c (n "initial") (v "0.1.3") (d (list (d (n "cargo-bump") (r "^1.1.0") (d #t) (k 0)))) (h "1qk82br94kn1i2rx37ksv2y4a5mafykpf1dzarhl5sc25j1ghxgg")))

(define-public crate-initial-0.1.4 (c (n "initial") (v "0.1.4") (d (list (d (n "cargo-bump") (r "^1.1.0") (d #t) (k 0)))) (h "1yqxpbnbiyvib3gbsihlhrr3wn9vx53wr994lbnf0krr3l5p6vq4")))

(define-public crate-initial-0.2.0 (c (n "initial") (v "0.2.0") (d (list (d (n "cargo-bump") (r "^1.1.0") (d #t) (k 0)))) (h "04bxp41yrxslhip738yqig7gzm6i2n7rkmq0vw56hdyy0kya4946")))

(define-public crate-initial-1.0.0 (c (n "initial") (v "1.0.0") (d (list (d (n "cargo-bump") (r "^1.1.0") (d #t) (k 0)))) (h "0bxqs2ypdxwkiaq82vxpg8gdhg9ny6d03058sq8wzd5vnbwjhhsf")))

(define-public crate-initial-1.0.1 (c (n "initial") (v "1.0.1") (d (list (d (n "cargo-bump") (r "^1.1.0") (d #t) (k 0)))) (h "16yc9l3x4cingh0snyl9ssrlkwxsp7gdnz2nbmx66f6l8l2061ph")))

(define-public crate-initial-1.0.2 (c (n "initial") (v "1.0.2") (d (list (d (n "cargo-bump") (r "^1.1.0") (d #t) (k 0)))) (h "1jqx96zsx79z3261xrfkqvnlqh45hz9bpqqha5x5sqp2nbsc9qm6")))

(define-public crate-initial-1.1.0 (c (n "initial") (v "1.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "17j3x6v2sk1blhfbirn6zy59wc1cnp09m356wlqlixxx1f989kpw")))

(define-public crate-initial-1.1.1 (c (n "initial") (v "1.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1pldn2yj90dsihjgsks9g4nsrnr9cnxghz65645677cgwk25hps6")))

(define-public crate-initial-1.2.0 (c (n "initial") (v "1.2.0") (h "0w1xy76j1jgm1lfrv61inhja8xcpfmblch5rf1s08ls2c404wzdn")))

