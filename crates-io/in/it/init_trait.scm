(define-module (crates-io in it init_trait) #:use-module (crates-io))

(define-public crate-init_trait-0.1.0 (c (n "init_trait") (v "0.1.0") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0aksz7fza7kajqx38ykxs2z15pzh0zsz2dg1jfc8yxybbj5l7plb") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-init_trait-0.2.0 (c (n "init_trait") (v "0.2.0") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "05k9n4bfy6sm5vs9yycyww618dqbkbfrmidpcfmr0gd7cvd9z7jd") (f (quote (("std") ("default" "std") ("alloc"))))))

