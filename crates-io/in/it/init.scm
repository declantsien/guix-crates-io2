(define-module (crates-io in it init) #:use-module (crates-io))

(define-public crate-init-0.1.0 (c (n "init") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 0)) (d (n "init_codegen") (r "^0.1.0") (d #t) (k 0)))) (h "1in67mg1f8wfv442inh0v2j60f0gqdgkdrkdxjf0wlbca88phg7y")))

(define-public crate-init-0.1.1 (c (n "init") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 0)) (d (n "init_codegen") (r "^0.1.0") (d #t) (k 0)))) (h "0hrqchrjcdspyjvhda872igldw8acn8r5x3rdpjanhlxy6v31b1m")))

(define-public crate-init-0.2.0 (c (n "init") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 0)) (d (n "init_codegen") (r "^0.2.0") (d #t) (k 0)))) (h "0wl10bgl7gljrk7kanjywqdpqfgh8r2277bl7dw0dlx23yx9djgc")))

