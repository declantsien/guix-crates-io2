(define-module (crates-io in it initrs) #:use-module (crates-io))

(define-public crate-initrs-0.1.0 (c (n "initrs") (v "0.1.0") (d (list (d (n "clap") (r "~2.23") (d #t) (k 0)) (d (n "env_logger") (r "~0.5") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "nix") (r "^0.8.1") (f (quote ("signalfd"))) (d #t) (k 0)))) (h "1s3qffiky5bs404ifzg40fp54flfvw8z2n91wwpcg6hw3p2y9h7f") (y #t)))

(define-public crate-initrs-0.1.1 (c (n "initrs") (v "0.1.1") (d (list (d (n "clap") (r "~2.23") (d #t) (k 0)) (d (n "env_logger") (r "~0.5") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "nix") (r "^0.8.1") (f (quote ("signalfd"))) (d #t) (k 0)))) (h "1506vzwgkk701145y6g7zzm2lbv6x9mwin77jqphqadlj7mfl3y8") (y #t)))

