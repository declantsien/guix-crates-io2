(define-module (crates-io in it initramfs) #:use-module (crates-io))

(define-public crate-initramfs-0.1.0 (c (n "initramfs") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (f (quote ("alloc"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0vzbqzid2281k2milxcg0gh87zbcwdkfl5lq9j4b2i72zr0i41di") (f (quote (("std" "env_logger") ("default" "std"))))))

(define-public crate-initramfs-0.2.0 (c (n "initramfs") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (f (quote ("alloc"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0rfv761lxdf3dqlzr3gf78m972s3995hcnz9vmr6ar4pbn9xd2na") (f (quote (("std" "env_logger") ("default" "std"))))))

