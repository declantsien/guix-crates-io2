(define-module (crates-io in km inkmate-common) #:use-module (crates-io))

(define-public crate-inkmate-common-0.0.1 (c (n "inkmate-common") (v "0.0.1") (d (list (d (n "alloy-primitives") (r "^0.3.1") (d #t) (k 0)) (d (n "alloy-sol-types") (r "^0.3.1") (d #t) (k 0)) (d (n "ethers") (r "^2.0") (d #t) (k 2)) (d (n "eyre") (r "^0.6.8") (d #t) (k 2)) (d (n "k256") (r "^0.13.3") (f (quote ("ecdsa"))) (k 2)) (d (n "mockall") (r "^0.10.2") (d #t) (k 2)) (d (n "secp256k1") (r "^0.29.0") (f (quote ("alloc" "recovery" "rand" "global-context"))) (k 2)))) (h "1kap37428i9d6y9xsx7jn98ib7q3jjn44ffcy75b7h8i4a8mkwzk")))

