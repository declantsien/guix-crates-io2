(define-module (crates-io in km inkmate-contracts) #:use-module (crates-io))

(define-public crate-inkmate-contracts-0.0.1 (c (n "inkmate-contracts") (v "0.0.1") (d (list (d (n "alloy-primitives") (r "^0.3.1") (d #t) (k 0)) (d (n "alloy-sol-types") (r "^0.3.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "inkmate-common") (r "^0.0.1") (d #t) (k 0)) (d (n "mini-alloc") (r "^0.4.2") (d #t) (k 0)) (d (n "stylus-sdk") (r "^0.4.3") (d #t) (k 0)))) (h "0956ypf47i93yc8qqz08k4116c5ln1p2spvbwm6n58k7j341js1h") (f (quote (("export-abi" "stylus-sdk/export-abi") ("erc20") ("debug" "stylus-sdk/debug"))))))

