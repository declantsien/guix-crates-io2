(define-module (crates-io in -p in-place) #:use-module (crates-io))

(define-public crate-in-place-0.1.0 (c (n "in-place") (v "0.1.0") (d (list (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)) (d (n "tmp_env") (r "^0.1.1") (d #t) (k 2)))) (h "0ipnbz6sv6874cmgs3m66g28r44whd1xa890cdarhpb0bjg80g09") (r "1.65")))

(define-public crate-in-place-0.2.0 (c (n "in-place") (v "0.2.0") (d (list (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)) (d (n "tmp_env") (r "^0.1.1") (d #t) (k 2)))) (h "1h584yx2j3mf49n57havjvwpnh908sgpkknf20r4jpzxmprnknqw") (r "1.70")))

