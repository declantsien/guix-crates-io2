(define-module (crates-io in tp intptr) #:use-module (crates-io))

(define-public crate-intptr-0.1.0 (c (n "intptr") (v "0.1.0") (d (list (d (n "dataview") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1hikjkila4ziv372jpl98vwph9d6hi76fj5ybpskcyq1zarcdnl6")))

(define-public crate-intptr-0.1.1 (c (n "intptr") (v "0.1.1") (d (list (d (n "dataview") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "08qyr72kp4ijy9qwmfyd7k7xa5g9nhi826fw8s89087987d77551")))

(define-public crate-intptr-0.1.2 (c (n "intptr") (v "0.1.2") (d (list (d (n "dataview") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1wcg9mz7yv13j1g02q694glk8kyqkb7pr1fvhf03b2ba7kf5wpmn") (f (quote (("nightly"))))))

(define-public crate-intptr-0.1.3 (c (n "intptr") (v "0.1.3") (d (list (d (n "dataview") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0i8gf5sq905az2gcznh4zfw2q76vbi6yhgnlah96krk2sf617cgv") (f (quote (("nightly"))))))

(define-public crate-intptr-0.1.4 (c (n "intptr") (v "0.1.4") (d (list (d (n "dataview") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1asgv4ms0vr92i6k373malzx7k0ykmw0q30wd7ysmf2vfv8ysjmv") (f (quote (("nightly"))))))

(define-public crate-intptr-0.1.5 (c (n "intptr") (v "0.1.5") (d (list (d (n "dataview") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1m2r3hpnck265lr85w5p4az8rh82p95ny6lisgaihywwzyrn0vdf") (f (quote (("nightly") ("int2ptr"))))))

(define-public crate-intptr-0.1.6 (c (n "intptr") (v "0.1.6") (d (list (d (n "dataview_0_1") (r "^0.1") (o #t) (k 0) (p "dataview")) (d (n "dataview_1") (r "^1.0") (o #t) (k 0) (p "dataview")) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0zg7527nvbyb2gq3rvhva39xd9h7wk66kks2zqw9lc4qa3c0aw18") (f (quote (("nightly") ("int2ptr") ("dataview" "dataview_0_1" "dataview_1"))))))

