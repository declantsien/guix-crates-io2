(define-module (crates-io in et inet) #:use-module (crates-io))

(define-public crate-inet-0.1.0 (c (n "inet") (v "0.1.0") (h "0kqghyscwwvc72x1g32kb5p89k964d36g3saj27xdz53mjb5p4h6")))

(define-public crate-inet-0.1.1 (c (n "inet") (v "0.1.1") (h "1cw99zx97675dlvppccnwp9a8l12x8r8kpjv0czff22sjc7ca2an")))

