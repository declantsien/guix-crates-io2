(define-module (crates-io in et inetnum) #:use-module (crates-io))

(define-public crate-inetnum-0.1.0 (c (n "inetnum") (v "0.1.0") (d (list (d (n "arbitrary") (r "^1.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.165") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "02gxwpbgh1f12vahci601agfpnvfkac4jgcqw625kzcqix7p1dkh") (r "1.71")))

