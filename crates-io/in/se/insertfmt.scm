(define-module (crates-io in se insertfmt) #:use-module (crates-io))

(define-public crate-insertfmt-1.0.0 (c (n "insertfmt") (v "1.0.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "sqlparser") (r "^0.32.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0ll4877kn7ni8q9wy37a1izy3vivn58va2dh1dmxxab828q552rl")))

(define-public crate-insertfmt-1.0.1 (c (n "insertfmt") (v "1.0.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "sqlparser") (r "^0.32.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1xg68snlkhcg9rlqwfs2f9q5psjl97m9nhyavgg93r1pmqnzzjkr")))

(define-public crate-insertfmt-1.0.2 (c (n "insertfmt") (v "1.0.2") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "sqlparser") (r "^0.32.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)))) (h "14jhq4haj47h0gpvfgy1dkvvlq90wywi6baxbrgp6l5k4d3vfj6z")))

(define-public crate-insertfmt-1.0.3 (c (n "insertfmt") (v "1.0.3") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "sqlparser") (r "^0.33.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)))) (h "0vzz458dpql5lf5qdyzbshyakm4w76r4gjxi190v2vjj9jji75x4")))

(define-public crate-insertfmt-1.0.4 (c (n "insertfmt") (v "1.0.4") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "sqlparser") (r "^0.45.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)))) (h "0ywkjhqrgdiz8zqvl0xqdv336hcz0lxsg1xdkkm1fjyrhbpcji65")))

