(define-module (crates-io in se insert_multiple) #:use-module (crates-io))

(define-public crate-insert_multiple-0.1.0 (c (n "insert_multiple") (v "0.1.0") (h "0rrhxmqsbphvkmzsbh05gic2najz5i3d22r3w9iaxsjbpj1763qg")))

(define-public crate-insert_multiple-0.1.1 (c (n "insert_multiple") (v "0.1.1") (h "08fpb7glwsw1k2f8krdgxv6p0b52n5ld6jwr3mf7pz4v8s45agb8")))

(define-public crate-insert_multiple-0.2.0 (c (n "insert_multiple") (v "0.2.0") (h "079sq8vdy4lvkzhnfz98fbz593fys009px36h2jcbar9pr05vxhb")))

