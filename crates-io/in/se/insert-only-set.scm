(define-module (crates-io in se insert-only-set) #:use-module (crates-io))

(define-public crate-insert-only-set-0.1.0 (c (n "insert-only-set") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13m72b5k886i1sydslg1ipn5pdkgzaxf3di13gqj33ra14jkknxd")))

(define-public crate-insert-only-set-0.2.0 (c (n "insert-only-set") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1r3aq1vxl5bqpszsa0p766nam85rgsvd7zdllrwaj6fi7h37fwjj")))

(define-public crate-insert-only-set-0.3.0 (c (n "insert-only-set") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hpnd3lc1147sxzjins3yrfipsnang7jgdsza02fi5lb7kpsfisd")))

(define-public crate-insert-only-set-0.3.1 (c (n "insert-only-set") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nln6qskks2k82ffqnwnsihvmxqdsnly607dnbi2b91jdvgb1v8k")))

