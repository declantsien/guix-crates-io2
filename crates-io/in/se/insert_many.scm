(define-module (crates-io in se insert_many) #:use-module (crates-io))

(define-public crate-insert_many-0.1.0 (c (n "insert_many") (v "0.1.0") (d (list (d (n "smallvec") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0xg3zyc8qc10m3j4mr8y8fsfa63b1jypj1sr6zdaq4jdjnvpcl2v") (f (quote (("default" "smallvec"))))))

(define-public crate-insert_many-0.1.1 (c (n "insert_many") (v "0.1.1") (d (list (d (n "smallvec") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "09ih6qg2b5h6q73w3qd067kb6iiizq386n6r9lvc644q4ydw050m") (f (quote (("default" "smallvec"))))))

