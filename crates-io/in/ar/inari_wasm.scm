(define-module (crates-io in ar inari_wasm) #:use-module (crates-io))

(define-public crate-inari_wasm-0.1.0 (c (n "inari_wasm") (v "0.1.0") (d (list (d (n "forward_ref") (r "^1.0.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.3") (d #t) (k 0)))) (h "11dwfc6dw9pgf4n3k1pcqgaqp7w73pr5nvm58nis95ypi4wfhy1r") (y #t)))

(define-public crate-inari_wasm-0.1.1 (c (n "inari_wasm") (v "0.1.1") (d (list (d (n "forward_ref") (r "^1.0.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.3") (d #t) (k 0)))) (h "0cbjif2ghs5mzlq0jjbjbf0rnhn75x8ck6jq9j77byyw2lznpwkf")))

(define-public crate-inari_wasm-0.1.2 (c (n "inari_wasm") (v "0.1.2") (d (list (d (n "forward_ref") (r "^1.0.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.3") (d #t) (k 0)))) (h "0mzgac6a3vblfp7sg7ip37n0lpglas2wffa25z2cs5vasrai3fxn")))

