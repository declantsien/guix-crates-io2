(define-module (crates-io in ar inarybay-runtime) #:use-module (crates-io))

(define-public crate-inarybay-runtime-0.1.0 (c (n "inarybay-runtime") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)))) (h "0k1b6gkr432vlwknmwqyniwyzzp04j0yw2gilyjf3jygvmwv06bn") (f (quote (("default")))) (s 2) (e (quote (("async" "dep:futures"))))))

(define-public crate-inarybay-runtime-0.1.1 (c (n "inarybay-runtime") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0p18w8fyhzsrngy14zx9smfl28rfhrsw4vjsr5v8bfsh3cc6pnw2") (f (quote (("default")))) (s 2) (e (quote (("async" "dep:futures"))))))

