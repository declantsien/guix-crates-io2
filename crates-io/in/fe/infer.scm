(define-module (crates-io in fe infer) #:use-module (crates-io))

(define-public crate-infer-0.1.0 (c (n "infer") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0bmijpi8fwss5k6b0har1p43ds0jrna4rdy0p2wig77qw460833i")))

(define-public crate-infer-0.1.1 (c (n "infer") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0kn8qid1swixks0a7wmz9nkrgwj437r4bi5mffsvmz8y1lnz6mnn")))

(define-public crate-infer-0.1.2 (c (n "infer") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1drcj2nbhii1d6vv0xgy7wpig3bvx89ar1yn47gygxszr3k99ypm")))

(define-public crate-infer-0.1.3 (c (n "infer") (v "0.1.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0fa9w9ic2d7zydw98mhv3js5d4w312n1m8hl0xp0ff8784zz67qv")))

(define-public crate-infer-0.1.4 (c (n "infer") (v "0.1.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1fwj4wzcvs9cw3anm9gxypww30rmm3q4zprlksiybp9mzvn9g37m")))

(define-public crate-infer-0.1.5 (c (n "infer") (v "0.1.5") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0kmq50ks7qxhlklkvl82nc59isk93yi330nw6cllmcslc18zb80p")))

(define-public crate-infer-0.1.6 (c (n "infer") (v "0.1.6") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "08s1ilqcms0jxfnh3jlf0xcn5c1wnyblpv99m13b6khnfrm40p6m")))

(define-public crate-infer-0.1.7 (c (n "infer") (v "0.1.7") (h "02j974n3msn261m44l41ch3x90rmhhkpwj4g8hdbmyf4vmvxsm38")))

(define-public crate-infer-0.2.0 (c (n "infer") (v "0.2.0") (h "1311vhdmckadn4qmn8j6aqbi9lvyiywp7i6fs8xaq9vx2raknzg0")))

(define-public crate-infer-0.2.1 (c (n "infer") (v "0.2.1") (h "1c5iwamn7wbn0x7crm22hbryy6d93bqgjzhnw1a04a9pwz73lw21")))

(define-public crate-infer-0.2.2 (c (n "infer") (v "0.2.2") (h "1rd0nfjw43hz6lxdffpfryhqrpb2mgaa1bln89l77d4wmw04mkl3")))

(define-public crate-infer-0.2.3 (c (n "infer") (v "0.2.3") (h "1b4ziqcv0d1wga5yfqf620dkgzijsdw3ylnzq61bfaxla2d85sb4")))

(define-public crate-infer-0.3.0 (c (n "infer") (v "0.3.0") (h "14pifr8k6df9jg24bwxzjpmvbspad1hvscmylm2z6blsbi3f3l75") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-infer-0.3.1 (c (n "infer") (v "0.3.1") (h "1k8xfihxapzi5wjvzav60cbqvj02lr34y806wq77mw8pwjql0aln") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-infer-0.3.2 (c (n "infer") (v "0.3.2") (h "05qxldbqmvc4h1y0finqd19aqdxqq8zl6nd8vsw4p4q8r5gy6nfg") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-infer-0.3.3 (c (n "infer") (v "0.3.3") (h "01v2li2fzajz42vazwbvk44gk9ysajxgynygxcizdnm6i95a20d6") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-infer-0.3.4 (c (n "infer") (v "0.3.4") (h "07qc4xf35s8iyq6zg4ppq22wk11avq8srl4zfn7mp0fdi0jm21l9") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-infer-0.3.5 (c (n "infer") (v "0.3.5") (h "1rj7jhc8ia5ls84z9cyf3s8zrzixw03sw2nd9lvs2r9gqiahgb9l") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-infer-0.3.6 (c (n "infer") (v "0.3.6") (h "0d5pkpqf40z9lnvav5rkwkb1jy46r89naalhd1n9bl0ijmdp60q8") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-infer-0.3.7 (c (n "infer") (v "0.3.7") (h "0mfgrmv6h3gm22654rxd80vjlc0xz9538c9c872d494fmrc8lpl6") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-infer-0.4.0 (c (n "infer") (v "0.4.0") (d (list (d (n "cfb") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "14c34nrpayp5q7d4idcd1d4maim18k1ikx83gi1fiyarnzd42azr") (f (quote (("std" "alloc" "cfb") ("default" "std") ("alloc"))))))

(define-public crate-infer-0.5.0 (c (n "infer") (v "0.5.0") (d (list (d (n "cfb") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "0v4clh7k8ml91bxl48xkkwk1d5an6h3j7azbxph10ab294236w7a") (f (quote (("std" "alloc" "cfb") ("default" "std") ("alloc"))))))

(define-public crate-infer-0.6.0 (c (n "infer") (v "0.6.0") (d (list (d (n "cfb") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "1sarawa7bqqi29kxb781qzfynyc2jd5vyx1n4hva3qk0pshizr9z") (f (quote (("std" "alloc" "cfb") ("default" "std") ("alloc"))))))

(define-public crate-infer-0.7.0 (c (n "infer") (v "0.7.0") (d (list (d (n "cfb") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "0fxva7y1bg07bh4b323d58qr2bhwj97ksick0ybsv73v2crvbci0") (f (quote (("std" "alloc" "cfb") ("default" "std") ("alloc"))))))

(define-public crate-infer-0.8.0 (c (n "infer") (v "0.8.0") (d (list (d (n "cfb") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "1hw6mi993blh9zz4knnzrwc7k293pihsbbdgc7lg2zlx11b8iy8n") (f (quote (("std" "alloc" "cfb") ("default" "std") ("alloc"))))))

(define-public crate-infer-0.8.1 (c (n "infer") (v "0.8.1") (d (list (d (n "cfb") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "0mbsb0mlgfc4h3z0nnwwzqk60pggnh7gmjgzmpaj22vfabgcwdg0") (f (quote (("std" "alloc" "cfb") ("default" "std") ("alloc"))))))

(define-public crate-infer-0.9.0 (c (n "infer") (v "0.9.0") (d (list (d (n "cfb") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "0ylk7iw1ajqrixh565xf8wd702dvlljplkrgbakll27yvcffcy7i") (f (quote (("std" "alloc" "cfb") ("default" "std") ("alloc"))))))

(define-public crate-infer-0.10.0 (c (n "infer") (v "0.10.0") (d (list (d (n "cfb") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "1rvc9wy6pvkabjz52sfgwmvlgkhgiqsww3inmg7jv7kmvqh41gww") (f (quote (("std" "alloc" "cfb") ("default" "std") ("alloc"))))))

(define-public crate-infer-0.11.0 (c (n "infer") (v "0.11.0") (d (list (d (n "cfb") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "0178zsl23s9a9ymss06x72z39c6y9japy7cvxfp2cnv63aqicv0a") (f (quote (("std" "alloc" "cfb") ("default" "std") ("alloc"))))))

(define-public crate-infer-0.12.0 (c (n "infer") (v "0.12.0") (d (list (d (n "cfb") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "1cwq4nv3lv74i297abdwfibgqh1ws58mgkhlcvlzqwqnjnvy9658") (f (quote (("std" "alloc" "cfb") ("default" "std") ("alloc"))))))

(define-public crate-infer-0.13.0 (c (n "infer") (v "0.13.0") (d (list (d (n "cfb") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "1z5p0k5zk79apncbfzfw9y4q2n78kmsx3c3xa63gjs4zlg1zhlgm") (f (quote (("std" "alloc" "cfb") ("default" "std") ("alloc"))))))

(define-public crate-infer-0.14.0 (c (n "infer") (v "0.14.0") (d (list (d (n "cfb") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "13mq1imrbc61zpbh5dkl2a3hzrppb3kj4wzpkw5g73h59968zdxv") (f (quote (("std" "alloc" "cfb") ("default" "std") ("alloc"))))))

(define-public crate-infer-0.15.0 (c (n "infer") (v "0.15.0") (d (list (d (n "cfb") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "16d1b83h5m87h6kq4z8kwjrzll5dq6rijg2iz437m008m4nn4cyb") (f (quote (("std" "alloc" "cfb") ("default" "std") ("alloc"))))))

