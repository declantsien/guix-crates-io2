(define-module (crates-io in fe infer-len) #:use-module (crates-io))

(define-public crate-infer-len-0.1.0 (c (n "infer-len") (v "0.1.0") (h "16vcxchhdxaigcd7rf709bwfhpbrn650i0nd6ngbx05k505n4xbl") (f (quote (("no-core")))) (y #t)))

(define-public crate-infer-len-0.1.1 (c (n "infer-len") (v "0.1.1") (h "0bdfzi1xayw1pni9xdr7cn5nlpxs9an442xbg7297zs6a2x5vyjs") (f (quote (("no-core")))) (y #t)))

(define-public crate-infer-len-0.1.2 (c (n "infer-len") (v "0.1.2") (h "1isi57060hnx16lf67ciajjzfzf6qgj70kziksvs9vc05xs3afq3") (f (quote (("no-core")))) (y #t)))

(define-public crate-infer-len-0.2.0 (c (n "infer-len") (v "0.2.0") (h "04v2vv31m2phglv33wvg50b0r9pcj5y24hnaig17xriirqwwwa1y") (f (quote (("no-core")))) (y #t)))

(define-public crate-infer-len-0.2.1 (c (n "infer-len") (v "0.2.1") (h "0a12yr05yslimqcjkgggisbxwh4mq26qpkc7h0csq48xnhpxn2h0") (f (quote (("no-core")))) (y #t)))

(define-public crate-infer-len-0.2.2 (c (n "infer-len") (v "0.2.2") (h "0hfcj8vgph6xlcwbv8sv6q2f8ryzpbfrc56sri2pmvzrq3rg3nf6") (f (quote (("no-core")))) (y #t)))

