(define-module (crates-io in fe infect) #:use-module (crates-io))

(define-public crate-infect-0.0.0 (c (n "infect") (v "0.0.0") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "16b4zl1accxzdlqipgmsdg449z7vh90v21jxrzbalb45k7ld63fc") (y #t)))

(define-public crate-infect-0.0.1 (c (n "infect") (v "0.0.1") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1mbicm33ajdsw4ksz7hfrcswhgr9gd63lv17r8clsfp0fhm4gxpy") (y #t)))

(define-public crate-infect-0.0.2 (c (n "infect") (v "0.0.2") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1vjsfx0qdz4yhdzf11ihrp2iplfcsw72lhf3q2i6j1wvxv93k0js") (y #t)))

(define-public crate-infect-0.0.3 (c (n "infect") (v "0.0.3") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1afysfgh34f56hvkyfj06fspwp5ym7pwnnf8ibkrv26g1xb6vqsb") (y #t)))

(define-public crate-infect-0.0.4 (c (n "infect") (v "0.0.4") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "18zjh7ikk7fc9lxaldv0sv41wzrf1cviw2pj9lyk6aqlps5lqhcz") (y #t)))

(define-public crate-infect-0.0.5 (c (n "infect") (v "0.0.5") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1wsxkj3gg73g35dv9xrbsgsfpiag45pnjs4hhcag9gv5na89s5rw")))

(define-public crate-infect-0.0.6 (c (n "infect") (v "0.0.6") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0ax2dp6w0593azbhaa71jn3cd9s9r5j87gwajkak87cy0j0ri17j") (y #t)))

(define-public crate-infect-0.0.7 (c (n "infect") (v "0.0.7") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0b6wldr3cqahy1hb547x2ycg997mgp23rcy2h05bbdr2fj1fawbd") (y #t)))

(define-public crate-infect-0.0.8 (c (n "infect") (v "0.0.8") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "009i8jdgmmpx5ssnrhjl59i0a3g9ax61x585qj4vs9n1rxiga1q4") (y #t)))

(define-public crate-infect-0.0.9 (c (n "infect") (v "0.0.9") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "08ykyf8fgxadgym741w2w2p42yki3wqdq9s0p93x8rzi7mjnvsg9") (y #t)))

(define-public crate-infect-0.0.11 (c (n "infect") (v "0.0.11") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "16mhamwkwc792zv6kvlkn8yxvshvbbiz505q7n3s09qm9s2afqqr") (y #t)))

(define-public crate-infect-0.0.12 (c (n "infect") (v "0.0.12") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "09a0vnx0y08bi8if3rad8xlhc3r4i1pc35l98ac96ar9nw5y5aky")))

(define-public crate-infect-0.0.13 (c (n "infect") (v "0.0.13") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "14kzsr0cq4b9xx59fsw9bxr69kka6854542ksri8gzk663n09k6y") (y #t)))

(define-public crate-infect-0.0.14 (c (n "infect") (v "0.0.14") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0a154a1faf9792sf2kfbm0c2qharccjzk8qya3ky66zd1ci7xdbr")))

(define-public crate-infect-0.0.15 (c (n "infect") (v "0.0.15") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0ppa52cvcs0xi962f7r0csn2dzrjrncbkx38bpwqxyq463fl0c06")))

(define-public crate-infect-0.0.16 (c (n "infect") (v "0.0.16") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0g49jid2w5md1ymxlkvpfyrcqd5pv24a7npi7q7vbmbbz3r7i9d4")))

(define-public crate-infect-0.0.17 (c (n "infect") (v "0.0.17") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1jdfr5w725zafxrwfn0q7ldplcfgja29dcbw9vjbxcv4r6y1m53c")))

(define-public crate-infect-0.0.18 (c (n "infect") (v "0.0.18") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0mg5jlxzz6x6gh2a56szpymxwycl25wkx9d8zx0kn3hdw0mdpian")))

(define-public crate-infect-0.0.19 (c (n "infect") (v "0.0.19") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "12pjiph5rk53qcfbg6grp9spbbzxpighwgw5mmrkpv95dsi1ahyy")))

(define-public crate-infect-0.0.20 (c (n "infect") (v "0.0.20") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "13j4aan9wx9h2m4kznkibqpw9bmwr0mjg7j6hb6y0y7hrak8xz3h")))

(define-public crate-infect-0.0.21 (c (n "infect") (v "0.0.21") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0hmsd8l1sgd7nc55kl5aqif72qg5zibb4s672mp743ymj4cn2fa5")))

(define-public crate-infect-0.0.22 (c (n "infect") (v "0.0.22") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "06s761r0l6p1m9hi1qg3f300y17ik1xhq6g48mc5nf1mm7bag8vg")))

(define-public crate-infect-0.0.23 (c (n "infect") (v "0.0.23") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "01gc76268injzwb649vsyaz5psg7vv0llz2mswjdm878mlfk4pxm") (y #t)))

(define-public crate-infect-0.0.24 (c (n "infect") (v "0.0.24") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0q9ba0fa9can83s51fmy31idxxjgmw1czx99ngmz5anzs8mxx6da")))

(define-public crate-infect-0.0.25 (c (n "infect") (v "0.0.25") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0fssicb0nddq6fh01nag4gplky0i10vkdhgwmxqs2ph09nznl20l")))

(define-public crate-infect-0.0.26 (c (n "infect") (v "0.0.26") (d (list (d (n "futures") (r "^0.3.27") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1iprn1zxmwgghyfpcydlb30siznkfzb12hipr3pjzjgqk2nv1dis")))

(define-public crate-infect-0.0.27 (c (n "infect") (v "0.0.27") (d (list (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1w54fzmcnsxn4xjdkx8akfjl5ld787p40wn1pd500qcxqz5akgvp")))

(define-public crate-infect-0.0.28 (c (n "infect") (v "0.0.28") (d (list (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0d4nxc7nmyawzpcm4xw1bsj1jqjrnaha49fcjakvgizmkf8w134j")))

(define-public crate-infect-0.0.29 (c (n "infect") (v "0.0.29") (d (list (d (n "futures") (r "^0.3.28") (f (quote ("std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0rh7hffbbc095a1mq8nbw1jr9hjdcimzz6ij25fp0nhkqzmn2vnm")))

(define-public crate-infect-0.0.30 (c (n "infect") (v "0.0.30") (d (list (d (n "futures-channel") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1zh18sqrw1ag6rmfd47fl1w44l5jrjmnvr9klg6ssq18h08nqbys")))

