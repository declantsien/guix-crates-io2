(define-module (crates-io in fe infernal) #:use-module (crates-io))

(define-public crate-infernal-0.0.0 (c (n "infernal") (v "0.0.0") (h "0qj6a9mp5iqaf537vk3slp20z05xc5ljls9bx4gshqa86ybrygxk") (f (quote (("default")))) (r "1.70")))

(define-public crate-infernal-0.0.1 (c (n "infernal") (v "0.0.1") (h "1acg8rp0hmk6kz0gkpsxw1xl19kswf6k85pba26v4xhmjrn0i53m") (f (quote (("default")))) (r "1.70")))

