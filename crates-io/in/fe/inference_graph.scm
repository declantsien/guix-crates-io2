(define-module (crates-io in fe inference_graph) #:use-module (crates-io))

(define-public crate-inference_graph-0.1.0 (c (n "inference_graph") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "03kr4sqpkmc43y44y44hgxjhba3snh4i6d1byinxzlyxy2vwnxk0")))

