(define-module (crates-io in fe infers-jsonschema) #:use-module (crates-io))

(define-public crate-infers-jsonschema-0.1.0 (c (n "infers-jsonschema") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14grz3j1z1865bbsnhqmc6zkrsyma9pdxxnjlxmr0lvs8w73s53a")))

