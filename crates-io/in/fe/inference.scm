(define-module (crates-io in fe inference) #:use-module (crates-io))

(define-public crate-inference-0.3.0 (c (n "inference") (v "0.3.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "redpanda") (r "^0.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-test") (r "^0.2") (d #t) (k 2)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "06gw8zc3af5g0xpfn58khgznspc8k4yk1134h58axbzirbsrgbz2")))

