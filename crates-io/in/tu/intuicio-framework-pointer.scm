(define-module (crates-io in tu intuicio-framework-pointer) #:use-module (crates-io))

(define-public crate-intuicio-framework-pointer-0.27.0 (c (n "intuicio-framework-pointer") (v "0.27.0") (d (list (d (n "intuicio-core") (r "^0.27") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.27") (d #t) (k 0)))) (h "0hcnaw91zizxbww161q3axxaz9lw7jn3nlbayqw6bljly35s710g")))

(define-public crate-intuicio-framework-pointer-0.27.1 (c (n "intuicio-framework-pointer") (v "0.27.1") (d (list (d (n "intuicio-core") (r "^0.27") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.27") (d #t) (k 2)))) (h "0jxg6lxv6sbqjsd6dphw2037j2wmplsnhxb3jcp6fw78jxa7xik3")))

(define-public crate-intuicio-framework-pointer-0.27.2 (c (n "intuicio-framework-pointer") (v "0.27.2") (d (list (d (n "intuicio-core") (r "^0.27") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.27") (d #t) (k 2)) (d (n "intuicio-derive") (r "^0.27") (d #t) (k 2)))) (h "0p3bkhfizqp07qpf77bvh0q5p78ybi9dwc5yyayzcwcmd9p2cxai")))

(define-public crate-intuicio-framework-pointer-0.28.0 (c (n "intuicio-framework-pointer") (v "0.28.0") (d (list (d (n "intuicio-core") (r "^0.28") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.28") (d #t) (k 2)) (d (n "intuicio-derive") (r "^0.28") (d #t) (k 2)))) (h "1aww4mhk89w3jszzg203lfw3wfswd6af7w7kp3jfzda4pq3acvl9")))

(define-public crate-intuicio-framework-pointer-0.28.1 (c (n "intuicio-framework-pointer") (v "0.28.1") (d (list (d (n "intuicio-core") (r "^0.28") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.28") (d #t) (k 2)) (d (n "intuicio-derive") (r "^0.28") (d #t) (k 2)))) (h "0b3az5y50237gr4cfgf9y6i56zpb10w8jv6n9gm87v1f2vbkq35b")))

(define-public crate-intuicio-framework-pointer-0.28.2 (c (n "intuicio-framework-pointer") (v "0.28.2") (d (list (d (n "intuicio-core") (r "^0.28") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.28") (d #t) (k 2)) (d (n "intuicio-derive") (r "^0.28") (d #t) (k 2)))) (h "142f9jdghw0a9lr95dcbijlb8bkhm5vq9lxz5blrc9kjrqbyspch")))

(define-public crate-intuicio-framework-pointer-0.28.3 (c (n "intuicio-framework-pointer") (v "0.28.3") (d (list (d (n "intuicio-core") (r "^0.28") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.28") (d #t) (k 2)) (d (n "intuicio-derive") (r "^0.28") (d #t) (k 2)))) (h "122isjp5h54pwq5idzrzk54swy9cbm1ck10qk8zm358fgzhqrjsx")))

(define-public crate-intuicio-framework-pointer-0.29.0 (c (n "intuicio-framework-pointer") (v "0.29.0") (d (list (d (n "intuicio-core") (r "^0.29") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.29") (d #t) (k 2)) (d (n "intuicio-derive") (r "^0.29") (d #t) (k 2)))) (h "1ingg0gxc9xh381z9784yk310l3mxxjr533zqdakimj20ps3j3hr")))

(define-public crate-intuicio-framework-pointer-0.29.1 (c (n "intuicio-framework-pointer") (v "0.29.1") (d (list (d (n "intuicio-core") (r "^0.29") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.29") (d #t) (k 2)) (d (n "intuicio-derive") (r "^0.29") (d #t) (k 2)))) (h "1ijyz0i6plx2l242c563j0pzszaxigyrkwm5cb5n87k87827wijr")))

(define-public crate-intuicio-framework-pointer-0.30.0 (c (n "intuicio-framework-pointer") (v "0.30.0") (d (list (d (n "intuicio-core") (r "^0.30") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.30") (d #t) (k 2)) (d (n "intuicio-derive") (r "^0.30") (d #t) (k 2)))) (h "03wnxbr6a8izsjvyyhkbmq5ia4931a830jhv2ldcpbmfj6ayjl81")))

(define-public crate-intuicio-framework-pointer-0.31.0 (c (n "intuicio-framework-pointer") (v "0.31.0") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.31") (d #t) (k 2)) (d (n "intuicio-derive") (r "^0.31") (d #t) (k 2)))) (h "18g7gl2zblpriqz6dsfx1wn2svgjqxpkbz6m317glgh3j19wxby0")))

(define-public crate-intuicio-framework-pointer-0.31.1 (c (n "intuicio-framework-pointer") (v "0.31.1") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.31") (d #t) (k 2)) (d (n "intuicio-derive") (r "^0.31") (d #t) (k 2)))) (h "0clnf0548vxv18hyj1lcxgqbbfc4hfwl72dyjgfmnhharqrwyi7d")))

(define-public crate-intuicio-framework-pointer-0.31.2 (c (n "intuicio-framework-pointer") (v "0.31.2") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.31") (d #t) (k 2)) (d (n "intuicio-derive") (r "^0.31") (d #t) (k 2)))) (h "12gsx99v4a9xz04lmmmw44qz6im7s7zv74h5vkjydnfrkdd4rwzl")))

(define-public crate-intuicio-framework-pointer-0.31.3 (c (n "intuicio-framework-pointer") (v "0.31.3") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.31") (d #t) (k 2)) (d (n "intuicio-derive") (r "^0.31") (d #t) (k 2)))) (h "0arypzkchmlgdh8134pk8alpirr4i8xvx6wf487sq9gblq15wm82")))

(define-public crate-intuicio-framework-pointer-0.31.4 (c (n "intuicio-framework-pointer") (v "0.31.4") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.31") (d #t) (k 2)) (d (n "intuicio-derive") (r "^0.31") (d #t) (k 2)))) (h "1qbmhnl2b6qc8q8hjbbdb2whkgbgbprsv0i9k3s89lsmcqvxndw5")))

(define-public crate-intuicio-framework-pointer-0.31.5 (c (n "intuicio-framework-pointer") (v "0.31.5") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.31") (d #t) (k 2)) (d (n "intuicio-derive") (r "^0.31") (d #t) (k 2)))) (h "1v669wv1mas9acpap1cg41lynin6m3c7pn0l1qcf8zng997rh9mi")))

(define-public crate-intuicio-framework-pointer-0.31.6 (c (n "intuicio-framework-pointer") (v "0.31.6") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.31") (d #t) (k 2)) (d (n "intuicio-derive") (r "^0.31") (d #t) (k 2)))) (h "1210d12z7c27af93mzlckmkgkbzy290r8sm4r2r1k5wvp6v3xgmw")))

