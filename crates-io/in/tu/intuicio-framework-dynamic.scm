(define-module (crates-io in tu intuicio-framework-dynamic) #:use-module (crates-io))

(define-public crate-intuicio-framework-dynamic-0.27.0 (c (n "intuicio-framework-dynamic") (v "0.27.0") (d (list (d (n "intuicio-core") (r "^0.27") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.27") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.27") (d #t) (k 2)))) (h "0d800pw0sip0mvj9ihs70556c3z4rpw4kkaj4qbpmpv95vbmyxnf")))

(define-public crate-intuicio-framework-dynamic-0.27.1 (c (n "intuicio-framework-dynamic") (v "0.27.1") (d (list (d (n "intuicio-core") (r "^0.27") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.27") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.27") (d #t) (k 2)))) (h "14j6sf272j84ny8skzk3d977z511vjhrnayrwcca93pnnm1bc7wf")))

(define-public crate-intuicio-framework-dynamic-0.27.2 (c (n "intuicio-framework-dynamic") (v "0.27.2") (d (list (d (n "intuicio-core") (r "^0.27") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.27") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.27") (d #t) (k 2)))) (h "0p21lplhkq0m2mrc35zfbgfyys4j8y86z5x61jawdcibviymg89i")))

(define-public crate-intuicio-framework-dynamic-0.28.0 (c (n "intuicio-framework-dynamic") (v "0.28.0") (d (list (d (n "intuicio-core") (r "^0.28") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.28") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.28") (d #t) (k 2)))) (h "187s2zdsdckq543m03q3bbjgkcjyr89kqp55vadvwkx31p0wklpz")))

(define-public crate-intuicio-framework-dynamic-0.28.1 (c (n "intuicio-framework-dynamic") (v "0.28.1") (d (list (d (n "intuicio-core") (r "^0.28") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.28") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.28") (d #t) (k 2)))) (h "02vasgzhyb0ngxihc9dds592lir7wxs08ykbnkrff7fwjwpwk2ff")))

(define-public crate-intuicio-framework-dynamic-0.28.2 (c (n "intuicio-framework-dynamic") (v "0.28.2") (d (list (d (n "intuicio-core") (r "^0.28") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.28") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.28") (d #t) (k 2)))) (h "1ysmf4ppkp01fg6pnzkzg934bbnj6qjb1bbwqmpwqpmi21mvb2yg")))

(define-public crate-intuicio-framework-dynamic-0.28.3 (c (n "intuicio-framework-dynamic") (v "0.28.3") (d (list (d (n "intuicio-core") (r "^0.28") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.28") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.28") (d #t) (k 2)))) (h "12knska5482gkvfi1ggjww5ycjalsys8i4wrc9gn8da3a9wbd4lp")))

(define-public crate-intuicio-framework-dynamic-0.29.0 (c (n "intuicio-framework-dynamic") (v "0.29.0") (d (list (d (n "intuicio-core") (r "^0.29") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.29") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.29") (d #t) (k 2)))) (h "1jqb5kvndz1fh63j93rxgc8a5d7sl2r1qqfx2a18s5m4p295wrwy")))

(define-public crate-intuicio-framework-dynamic-0.29.1 (c (n "intuicio-framework-dynamic") (v "0.29.1") (d (list (d (n "intuicio-core") (r "^0.29") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.29") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.29") (d #t) (k 2)))) (h "06qz3ypn0w2nqsm4zz9v8wmxy1n6174vrshv6fkpymi7v13i314w")))

(define-public crate-intuicio-framework-dynamic-0.30.0 (c (n "intuicio-framework-dynamic") (v "0.30.0") (d (list (d (n "intuicio-core") (r "^0.30") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.30") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.30") (d #t) (k 2)))) (h "1dp8jlqbdajy8x63d8ic6l7zm2ah0sybn6m25b8mhqn6vh2bbnhl")))

(define-public crate-intuicio-framework-dynamic-0.31.0 (c (n "intuicio-framework-dynamic") (v "0.31.0") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.31") (d #t) (k 2)))) (h "0skplgxc4nmjmxdivkdj01fc570xqn66d2gnxmm6k05sik3v3g8s")))

(define-public crate-intuicio-framework-dynamic-0.31.1 (c (n "intuicio-framework-dynamic") (v "0.31.1") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.31") (d #t) (k 2)))) (h "06fjih8pwdy4mvsi1fzjmlcy5539r1i2l9lqfvkn2my169j3g0f1")))

(define-public crate-intuicio-framework-dynamic-0.31.2 (c (n "intuicio-framework-dynamic") (v "0.31.2") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.31") (d #t) (k 2)))) (h "1a35p5vhyl3kfdl3qx6h758av5l27calkgvcvbia98imcwr0w01h")))

(define-public crate-intuicio-framework-dynamic-0.31.3 (c (n "intuicio-framework-dynamic") (v "0.31.3") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.31") (d #t) (k 2)))) (h "1aglhwhhx6jp92ws8q1v4455y8fcm49hbs7cwnvhgrs6pr53sz6p")))

(define-public crate-intuicio-framework-dynamic-0.31.4 (c (n "intuicio-framework-dynamic") (v "0.31.4") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.31") (d #t) (k 2)))) (h "1pv8rf6w8fbwvzpp7jrk4yp085c8yi4zwfknyrhqr0h65f7gisw0")))

(define-public crate-intuicio-framework-dynamic-0.31.5 (c (n "intuicio-framework-dynamic") (v "0.31.5") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.31") (d #t) (k 2)))) (h "0js0da3hyjmhxi4gg4w2dc4yb8d1dz5mc18z4v9gwcdwa52m0wjh")))

(define-public crate-intuicio-framework-dynamic-0.31.6 (c (n "intuicio-framework-dynamic") (v "0.31.6") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.31") (d #t) (k 2)))) (h "1mfvxcy5prl20dm5vwwiwy91h1fmx3n4k9n5yd2rzdvjp0dzawx2")))

