(define-module (crates-io in tu intuicio-data) #:use-module (crates-io))

(define-public crate-intuicio-data-0.1.0 (c (n "intuicio-data") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0wfcwfrnm6hwqh37ywjyz7p4qqgk6cilcc9lw2ib2g6h46x8c27w")))

(define-public crate-intuicio-data-0.2.0 (c (n "intuicio-data") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0qdqvkii5175mhhf7z5x33fmvhv9w7kij0ixf1s8j0wzwirj594p")))

(define-public crate-intuicio-data-0.3.0 (c (n "intuicio-data") (v "0.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1bbw9qhikysrj1kcmbqrgsj4yr28d66lig6vybd2bdwkrnad24z1")))

(define-public crate-intuicio-data-0.3.1 (c (n "intuicio-data") (v "0.3.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0f0jrals77p71mc5f29jzhxwzgsh5yhy65fi7bb9b352dwvivjyz")))

(define-public crate-intuicio-data-0.3.2 (c (n "intuicio-data") (v "0.3.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1kpi3vga55gxmv2c7x03268x3mqzn77dby1bci6mvxm31gaq4mmv")))

(define-public crate-intuicio-data-0.3.3 (c (n "intuicio-data") (v "0.3.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1ni9izqpmsk2z1qnxx5kz02d1dqaqky8xd6qzapvax2z0gknv1h2")))

(define-public crate-intuicio-data-0.3.4 (c (n "intuicio-data") (v "0.3.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1vmhppl9y3bswm7ahm25yd99wgryn4z9ybni5alyggi6fi0iwmsa")))

(define-public crate-intuicio-data-0.3.5 (c (n "intuicio-data") (v "0.3.5") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "19lf23lbn63m609xbmmda5bli6yqrj587dxvj411jba6dngsq7rj")))

(define-public crate-intuicio-data-0.3.6 (c (n "intuicio-data") (v "0.3.6") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "10lbsg7a4rvll4smf29kyf16c4l57asycn4gk2zly32bsygaynmk")))

(define-public crate-intuicio-data-0.3.7 (c (n "intuicio-data") (v "0.3.7") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "11vrznwjilfgvcpaw0ii7q3gbmn8sbf8x7ryjz2jwd179nwrcp43")))

(define-public crate-intuicio-data-0.4.0 (c (n "intuicio-data") (v "0.4.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0d7w8qjafwljdk14sgc4nr8l68hlp3pajy9nk8wbafn9dsg44m49")))

(define-public crate-intuicio-data-0.5.0 (c (n "intuicio-data") (v "0.5.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0iq694sjrz1salr9rpxwj0nxx61i8vh6rbfmlpanrjrgah2prwpd")))

(define-public crate-intuicio-data-0.5.1 (c (n "intuicio-data") (v "0.5.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0qhy7s112ld2x026qqd1h4mdf9ndd83iniygk9dx28mc517s8ikn")))

(define-public crate-intuicio-data-0.5.2 (c (n "intuicio-data") (v "0.5.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "13a3w9hhnvnj89k0fap27j5ld9rcn7q66lq6jdllgyx7nribj2r0")))

(define-public crate-intuicio-data-0.6.0 (c (n "intuicio-data") (v "0.6.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1hv7frwb386cy5z0ckb8xnrmy2cs2z5kxf1ir7agkf6164svyi01")))

(define-public crate-intuicio-data-0.6.1 (c (n "intuicio-data") (v "0.6.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0sszx8nqr7g18sw78hyl9dj57qfjq41pyqznngqwkr5l3ppdas7y")))

(define-public crate-intuicio-data-0.7.0 (c (n "intuicio-data") (v "0.7.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "156rqri3q01gfkv2k12drfvwsq3zbw49slgja8yp35c9cr2h6kj5")))

(define-public crate-intuicio-data-0.8.0 (c (n "intuicio-data") (v "0.8.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1sm8dh2scnfa89fdz3qnb9b7gwddjm068p243plc55pisk0y4b47")))

(define-public crate-intuicio-data-0.8.1 (c (n "intuicio-data") (v "0.8.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0ir9wpv5s72ac407ixsan96l9q3pzn5kz3cick1wk78vcvgln7n2")))

(define-public crate-intuicio-data-0.9.0 (c (n "intuicio-data") (v "0.9.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "17bhsxnknvp3g9rrxv1fv6hb6mr0scvbf8dr5x8yndhv8yr51x25")))

(define-public crate-intuicio-data-0.10.0 (c (n "intuicio-data") (v "0.10.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "039yh41km71jkllkv0sf1mhpzvg40r2v9137z18n8g4pg8whxbab")))

(define-public crate-intuicio-data-0.10.1 (c (n "intuicio-data") (v "0.10.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "11kr3h3iyrp24a178kdgxb7yqd0j2ynbnpgl4wdlqdfajlry87w8")))

(define-public crate-intuicio-data-0.11.0 (c (n "intuicio-data") (v "0.11.0") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0hal6jlcrp9z48d21i7iardx027yrfqhd7344032v3ar9hzjkdnb")))

(define-public crate-intuicio-data-0.11.1 (c (n "intuicio-data") (v "0.11.1") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1aqc69f9gz3qw83wjy9g56vri8n7069vb1rzbhmd7jvsr8szg8kf")))

(define-public crate-intuicio-data-0.11.2 (c (n "intuicio-data") (v "0.11.2") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0zgyyb94fvg4rqaq8n522q5zmgc6yj97lv7g4s23447nanpwlq7m")))

(define-public crate-intuicio-data-0.12.0 (c (n "intuicio-data") (v "0.12.0") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0fsc7l4yyyz6pb37v5h3mndj1hmaig4pa17fly7rccnyddfffsrr")))

(define-public crate-intuicio-data-0.12.1 (c (n "intuicio-data") (v "0.12.1") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1w07n8789x0j28qi7hhhknd2ryx3qsr0hlh6lhr2vzklbzp483hf")))

(define-public crate-intuicio-data-0.12.2 (c (n "intuicio-data") (v "0.12.2") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1z77xgyz0d21jq879bydaq97zw466jnzb9pkkg4ll262q9qsvvsz")))

(define-public crate-intuicio-data-0.12.3 (c (n "intuicio-data") (v "0.12.3") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0id2ga0ah4azv0rs09r97gvf33jip3cn62j8pyddfb5kva737xkp")))

(define-public crate-intuicio-data-0.13.0 (c (n "intuicio-data") (v "0.13.0") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1w1algsql3pwqnq6cxz8qrph2qs72s3rbl2ql3bwygfj8j99633q")))

(define-public crate-intuicio-data-0.13.1 (c (n "intuicio-data") (v "0.13.1") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "023qpdzqqj976kn9fx1vzavkr3p9vmdvxrachfjv0n3r9a79rn3q")))

(define-public crate-intuicio-data-0.14.0 (c (n "intuicio-data") (v "0.14.0") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1533nk25skydz0q625y30p56gpkqp0vbiizb09zgzpyxbph32bxh")))

(define-public crate-intuicio-data-0.15.0 (c (n "intuicio-data") (v "0.15.0") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1rvnay4dcqb7cidi168k6lvp0j52zbw5j098hkj4w60azl0b3fls")))

(define-public crate-intuicio-data-0.16.0 (c (n "intuicio-data") (v "0.16.0") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1xhzk792npmvg81xz44lgxangm176gqr57k9ampdqw52972qxn5j")))

(define-public crate-intuicio-data-0.17.0 (c (n "intuicio-data") (v "0.17.0") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0r7x8n20h3w3annmx2iwib4v10692ykv7yz1vwmpl1sv7r3hdiqh")))

(define-public crate-intuicio-data-0.17.1 (c (n "intuicio-data") (v "0.17.1") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1dxmrypds879sng4qx4rzvnxvn04h38xn5g64y9phr079ap70k7x")))

(define-public crate-intuicio-data-0.17.2 (c (n "intuicio-data") (v "0.17.2") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1zdfkaxjy0b26fz6kj4k5yi95lfsii9q9c9mn6d88jv9fsp332yv")))

(define-public crate-intuicio-data-0.17.3 (c (n "intuicio-data") (v "0.17.3") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0k9vxwm89n66sbc011wfg55zdwz86xfizm4v87ah8glr995kbani")))

(define-public crate-intuicio-data-0.18.0 (c (n "intuicio-data") (v "0.18.0") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0kwbaa05zdmwrppdn7l4famzbp1wsc47q7nnhb8qrsg91ib1jdkc")))

(define-public crate-intuicio-data-0.18.1 (c (n "intuicio-data") (v "0.18.1") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1r2dpf6x7632nq1jhjgfmzid5cssscqyxykbqvm9jf9csi48r4fb")))

(define-public crate-intuicio-data-0.18.2 (c (n "intuicio-data") (v "0.18.2") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "13a2rf4f566f20n62fi7z3hk2072hr3vmmaq8s2vhvc38a6gxqz4")))

(define-public crate-intuicio-data-0.18.3 (c (n "intuicio-data") (v "0.18.3") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "16m92xkq04kh9p8wjg39fsa4xmj15dk3w4dk6zmf342bqfz6y6s1")))

(define-public crate-intuicio-data-0.19.0 (c (n "intuicio-data") (v "0.19.0") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0yg4xn4z57rrkvkl0z031vkbmbmk037wil7avyyx9m3q773inl7p")))

(define-public crate-intuicio-data-0.19.1 (c (n "intuicio-data") (v "0.19.1") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1biiqdbmlrfg5rcwqipcc412mza3b9mz12fj9h473y9wdv31n1xd")))

(define-public crate-intuicio-data-0.20.0 (c (n "intuicio-data") (v "0.20.0") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "134566mk6d8j1kznz1zxl186yvh2g8jrzym8yn0hw68a9xfq4bcw")))

(define-public crate-intuicio-data-0.20.1 (c (n "intuicio-data") (v "0.20.1") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0a9ynkmdnz8mnn80j3srd1sfjdbbmpcka953rg0bfwcyydydysx3")))

(define-public crate-intuicio-data-0.20.2 (c (n "intuicio-data") (v "0.20.2") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1bn60gsvmh3j6zwi3mhl6hd5zfy5pbgx270hfgirwx28wcapi3ik")))

(define-public crate-intuicio-data-0.20.3 (c (n "intuicio-data") (v "0.20.3") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1wfkwvhla60nhgw9gmkpk5by8ffqd68j3sr5crgs5r649h8dgyfv")))

(define-public crate-intuicio-data-0.20.4 (c (n "intuicio-data") (v "0.20.4") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "10v68bll59gz38mdgpyrrl72hpxslvxkjm20fqsg4nkbqslppl0a")))

(define-public crate-intuicio-data-0.20.5 (c (n "intuicio-data") (v "0.20.5") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0ml4rglxacx0vlkz4vqa7as1smbxxahxhra4a3ja6hynkbarqbhs")))

(define-public crate-intuicio-data-0.21.0 (c (n "intuicio-data") (v "0.21.0") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1igi7xh3lgz1a45k5zdnr6cd3qd9m8wrasgnwmjv0n5rhwin6kcv")))

(define-public crate-intuicio-data-0.22.0 (c (n "intuicio-data") (v "0.22.0") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0xpf4p8z1glf15wbrg03h2hgvkpckma3ynkv3w41lrryyjrmcx09")))

(define-public crate-intuicio-data-0.22.1 (c (n "intuicio-data") (v "0.22.1") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "17p4q21awspyl93ljg3hfxwbfzxkpr0pniqq1jwjd45w9ikcr6zr")))

(define-public crate-intuicio-data-0.23.0 (c (n "intuicio-data") (v "0.23.0") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "08zihs6fh82yfid9r9k715jycklzj99rdf4w1kijf9f99kddcmwy")))

(define-public crate-intuicio-data-0.23.1 (c (n "intuicio-data") (v "0.23.1") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "096j23bwrsya74bvf7z5ja7jkvzrz90fa2dxg84fkw9b5fgghqws")))

(define-public crate-intuicio-data-0.24.0 (c (n "intuicio-data") (v "0.24.0") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1wxlj5hcw3fbvz7ww6bzil46923gk3w1r667h7l32sgvqm3rlblz")))

(define-public crate-intuicio-data-0.24.1 (c (n "intuicio-data") (v "0.24.1") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1fhj69f8rrggaxqh467jxhsjp0qk01ngs1xdblna3b6agyq65gn9")))

(define-public crate-intuicio-data-0.24.2 (c (n "intuicio-data") (v "0.24.2") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0yckawkzac48r63s9775l6xfxzppyqcnscqv67rpbmw0d5i5q203")))

(define-public crate-intuicio-data-0.25.0 (c (n "intuicio-data") (v "0.25.0") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0rswyzplj8iigivpsciil7l5sccvcd4bnjy37bg8x8abxgcacqy1")))

(define-public crate-intuicio-data-0.26.0 (c (n "intuicio-data") (v "0.26.0") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0r8ylahd12y2k88dixnkpwvlgmba8dqws62w4qiwi6vg2b6jakyl")))

(define-public crate-intuicio-data-0.27.0 (c (n "intuicio-data") (v "0.27.0") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0psqssy3r9l27z4xnrjjqj2szmp82v5rx8wa5v8p25lqcr8zm740")))

(define-public crate-intuicio-data-0.27.1 (c (n "intuicio-data") (v "0.27.1") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0a73qf70gwzjxmbnaf7jqpqnhki2r31lr4w8vfjk01yyxvacf5zj")))

(define-public crate-intuicio-data-0.27.2 (c (n "intuicio-data") (v "0.27.2") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1n92vs8sc6dnhg7pwaw95f1spgi27vq5aaxrgric99ac5lmh4ls6")))

(define-public crate-intuicio-data-0.28.0 (c (n "intuicio-data") (v "0.28.0") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "15i1iikpysvk4534xkbj6sks5y6aw5qk9iacxfpcyxwy5l50wllb")))

(define-public crate-intuicio-data-0.28.1 (c (n "intuicio-data") (v "0.28.1") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1ckcjd2r1kgkvafsk41a84xbn7in0zgndbp9v1mkfqfxa92cl19l")))

(define-public crate-intuicio-data-0.28.2 (c (n "intuicio-data") (v "0.28.2") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "17qsy48nhfc60nb002p08zwb588dc1a27krsba8scdx6f6z1gx9q")))

(define-public crate-intuicio-data-0.28.3 (c (n "intuicio-data") (v "0.28.3") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1zzal8d9m0gwrbw2mybag03wlih4cgg7vd9xxrh8n7m7pj0mrr0h")))

(define-public crate-intuicio-data-0.29.0 (c (n "intuicio-data") (v "0.29.0") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0d4adgp8hjrx4r693cgz6fq63zb0z6jsnvxcsgyr95v3avhmlahb")))

(define-public crate-intuicio-data-0.29.1 (c (n "intuicio-data") (v "0.29.1") (d (list (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0f8cyyr2zrx4qds4imzhilmrbvw9bp6dr7fbz58csmfjvsycz2h0")))

(define-public crate-intuicio-data-0.30.0 (c (n "intuicio-data") (v "0.30.0") (d (list (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.13") (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1n50fcm2qf6snyjldck9vxxmhi5cm61wl2fgd7rgam02h40asblp")))

(define-public crate-intuicio-data-0.31.0 (c (n "intuicio-data") (v "0.31.0") (d (list (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.13") (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0imxnv74rd6aaav493nn3qk3abmwahwkxray2yr1h7wbz5nxv310")))

(define-public crate-intuicio-data-0.31.1 (c (n "intuicio-data") (v "0.31.1") (d (list (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.13") (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1pkbkfj2bb713jjfrlh4npc33xv72aw3zpmji2njqf1dkxd6v6dq")))

(define-public crate-intuicio-data-0.31.2 (c (n "intuicio-data") (v "0.31.2") (d (list (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.13") (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0497nadpg076vshvm9sc80hiyn4gabx89d2djg5dxva1hbwq362b")))

(define-public crate-intuicio-data-0.31.3 (c (n "intuicio-data") (v "0.31.3") (d (list (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.13") (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0jmjs6ns3bgclgqv7kwi4zfa9b5p8s0npjh92sz3qbmznpnblpml")))

(define-public crate-intuicio-data-0.31.4 (c (n "intuicio-data") (v "0.31.4") (d (list (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.13") (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "190f5cn7lzf4bky52qlls44zpj0w0rj0spizx99shyp75xpr8g8w")))

(define-public crate-intuicio-data-0.31.5 (c (n "intuicio-data") (v "0.31.5") (d (list (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.13") (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "060kdifkwahp7x86bhghw2yc06qx02qc8zyyg960jgb18jldp288")))

(define-public crate-intuicio-data-0.31.6 (c (n "intuicio-data") (v "0.31.6") (d (list (d (n "rustc-hash") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^1.13") (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0g1nx5z2d59disgam7bg4cq9rx8gssm4d2kb2sgfi25pn3gmva7j")))

