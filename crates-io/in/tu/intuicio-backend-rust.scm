(define-module (crates-io in tu intuicio-backend-rust) #:use-module (crates-io))

(define-public crate-intuicio-backend-rust-0.1.0 (c (n "intuicio-backend-rust") (v "0.1.0") (d (list (d (n "intuicio-core") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0lrq2ly85w7jrcplsszcsvn9dcpzcqndmp164bpbmrwfsvdwmjif")))

(define-public crate-intuicio-backend-rust-0.2.0 (c (n "intuicio-backend-rust") (v "0.2.0") (d (list (d (n "intuicio-core") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1yh2dqk71vr67znqj0gcxjb0va9r63lkwbbd22bgz3zypavy7nid")))

(define-public crate-intuicio-backend-rust-0.3.0 (c (n "intuicio-backend-rust") (v "0.3.0") (d (list (d (n "intuicio-core") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0iwy7bx7iyxycqn5b4aidh37b3slw77lvbv0kp9dpk6xi2ami8cq")))

(define-public crate-intuicio-backend-rust-0.3.1 (c (n "intuicio-backend-rust") (v "0.3.1") (d (list (d (n "intuicio-core") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1jcizxwsri4n7vxa5z8c2q15a1m71c6ahl1k4ncvlypk594spa1i")))

(define-public crate-intuicio-backend-rust-0.3.2 (c (n "intuicio-backend-rust") (v "0.3.2") (d (list (d (n "intuicio-core") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0fmxdwf2a5ahvldr9bhcp925fsri4lsvlvk9nw2yjikgmy1klf6g")))

(define-public crate-intuicio-backend-rust-0.3.3 (c (n "intuicio-backend-rust") (v "0.3.3") (d (list (d (n "intuicio-core") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0bc4jaca6vskcflsrsj1lkkyblz8ib8xgz3d35kgbbp3vwqqgqa1")))

(define-public crate-intuicio-backend-rust-0.3.4 (c (n "intuicio-backend-rust") (v "0.3.4") (d (list (d (n "intuicio-core") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0ldnqx05zwjwp4b5dwrrjkac046p4r90baplhbkvp00x1vi6zkbj")))

(define-public crate-intuicio-backend-rust-0.3.5 (c (n "intuicio-backend-rust") (v "0.3.5") (d (list (d (n "intuicio-core") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1aalidvshs204ajjr9fvzbwn6frh3rg1ah7b7lr6283xsvs47pz1")))

(define-public crate-intuicio-backend-rust-0.3.6 (c (n "intuicio-backend-rust") (v "0.3.6") (d (list (d (n "intuicio-core") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0gbv5xq9h8p7bc1qr3cqp6y1ix5s3sp5qyq93bzzf2jm0zdc73gz")))

(define-public crate-intuicio-backend-rust-0.3.7 (c (n "intuicio-backend-rust") (v "0.3.7") (d (list (d (n "intuicio-core") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0nw33n79802f5z427n3nlf39y4fh7is9m9cmvsjdwp8mm064qm04")))

(define-public crate-intuicio-backend-rust-0.4.0 (c (n "intuicio-backend-rust") (v "0.4.0") (d (list (d (n "intuicio-core") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0ksgv31bc6mglivqyx956scb2c5ljns1k5dmanrkif3c5h2x49cl")))

(define-public crate-intuicio-backend-rust-0.5.0 (c (n "intuicio-backend-rust") (v "0.5.0") (d (list (d (n "intuicio-core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0rqarkl9fvl3w5sgwz2m4xkks2qkz0yxi8lxssxc2732hin8sbkr")))

(define-public crate-intuicio-backend-rust-0.5.1 (c (n "intuicio-backend-rust") (v "0.5.1") (d (list (d (n "intuicio-core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1d5a0nm9y6m1fnd9cdypv8imhbssdxw8g6q38104q4wlpcy1wgfj")))

(define-public crate-intuicio-backend-rust-0.5.2 (c (n "intuicio-backend-rust") (v "0.5.2") (d (list (d (n "intuicio-core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1axahysa8dyjwdxrdxkg0m9nqaa6p9pz5lv9ycnipsynil42l4rq")))

(define-public crate-intuicio-backend-rust-0.6.0 (c (n "intuicio-backend-rust") (v "0.6.0") (d (list (d (n "intuicio-core") (r "^0.6") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1330f7bby1gcfw50r20zh1hdjxm9mp3wvmp2d36mbr1iaj5ibvrp")))

(define-public crate-intuicio-backend-rust-0.6.1 (c (n "intuicio-backend-rust") (v "0.6.1") (d (list (d (n "intuicio-core") (r "^0.6") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1qjjmjp9443fxll1hv1c5c9skhy2794mkviwv1i2cfaxwi5lgxrz")))

(define-public crate-intuicio-backend-rust-0.7.0 (c (n "intuicio-backend-rust") (v "0.7.0") (d (list (d (n "intuicio-core") (r "^0.7") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1vq84cjwbwrgs0gvp7x2s1dmfvh0kdyn12992gqfhvjq4x7f2zh1")))

(define-public crate-intuicio-backend-rust-0.8.0 (c (n "intuicio-backend-rust") (v "0.8.0") (d (list (d (n "intuicio-core") (r "^0.8") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1p72hqikq8cijy5h32zshijgdnr4mlfhiy2h0ys0dg8blrskbg9w")))

(define-public crate-intuicio-backend-rust-0.8.1 (c (n "intuicio-backend-rust") (v "0.8.1") (d (list (d (n "intuicio-core") (r "^0.8") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1kqr6pq14zhzwda6lwl1kv46hjh545prx20xc8584kwm5g74v67n")))

(define-public crate-intuicio-backend-rust-0.9.0 (c (n "intuicio-backend-rust") (v "0.9.0") (d (list (d (n "intuicio-core") (r "^0.9") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "16a4cgl4srf4pp8zw1iinvxg4yh5wn0fyr8l0va65mrqr3ijc5qw")))

(define-public crate-intuicio-backend-rust-0.10.0 (c (n "intuicio-backend-rust") (v "0.10.0") (d (list (d (n "intuicio-core") (r "^0.10") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0qzqdkndyc4321r0268kjpyz8383kvah5bzhx8s571j1ha9djsrl")))

(define-public crate-intuicio-backend-rust-0.10.1 (c (n "intuicio-backend-rust") (v "0.10.1") (d (list (d (n "intuicio-core") (r "^0.10") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0mwhk6dqvx3hqfghs1wga0xa5n1pzcia5imlg0v6w2ziq87h4kzx")))

(define-public crate-intuicio-backend-rust-0.11.0 (c (n "intuicio-backend-rust") (v "0.11.0") (d (list (d (n "intuicio-core") (r "^0.11") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1n3nfr0wlmljbmimsnrj1f47y6h6rln64ixjrd8qflxgi1w790pq")))

(define-public crate-intuicio-backend-rust-0.11.1 (c (n "intuicio-backend-rust") (v "0.11.1") (d (list (d (n "intuicio-core") (r "^0.11") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "033xssgvd51i2drrcg5ps9dyl7qflbrpsfrd88i1xmdvgxalxwrv")))

(define-public crate-intuicio-backend-rust-0.11.2 (c (n "intuicio-backend-rust") (v "0.11.2") (d (list (d (n "intuicio-core") (r "^0.11") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0798aqykaq1bwzx3g57199az76xl0kj9yyi24dmmcqwhw5w7iwf9")))

(define-public crate-intuicio-backend-rust-0.12.0 (c (n "intuicio-backend-rust") (v "0.12.0") (d (list (d (n "intuicio-core") (r "^0.12") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1427pawpsha5aghg9ffnx1syqbklm9634n923zkr7qk70c185584")))

(define-public crate-intuicio-backend-rust-0.12.1 (c (n "intuicio-backend-rust") (v "0.12.1") (d (list (d (n "intuicio-core") (r "^0.12") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "15jm1ylkvnxj9a9bmj1iwasfyxpb08fs0sm3ppjabd2j1frlv80z")))

(define-public crate-intuicio-backend-rust-0.12.2 (c (n "intuicio-backend-rust") (v "0.12.2") (d (list (d (n "intuicio-core") (r "^0.12") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0db4n0q0wl876a21dnv1wc6hc3vcv8iwv9nbrhbalf2zl1az70hk")))

(define-public crate-intuicio-backend-rust-0.12.3 (c (n "intuicio-backend-rust") (v "0.12.3") (d (list (d (n "intuicio-core") (r "^0.12") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0m40gnzwfxp1cp2fyg3dzmla7pxrghw8q2q42m3xnd9kmh360iz5")))

(define-public crate-intuicio-backend-rust-0.13.0 (c (n "intuicio-backend-rust") (v "0.13.0") (d (list (d (n "intuicio-core") (r "^0.13") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1d0c96nd661prb0c8lwmpvm2gf6bbam0bsxciq3122f1zx37n05i")))

(define-public crate-intuicio-backend-rust-0.13.1 (c (n "intuicio-backend-rust") (v "0.13.1") (d (list (d (n "intuicio-core") (r "^0.13") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0d1kmr8pvgmw8shq0vscji2a40jb3cvv2a9l4w68iiz5lfsll7zj")))

(define-public crate-intuicio-backend-rust-0.14.0 (c (n "intuicio-backend-rust") (v "0.14.0") (d (list (d (n "intuicio-core") (r "^0.14") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1aj1k8j7wlkn3jrwrljji2yifwii1yfv5n3b9fk8cmh23rpmcizx")))

(define-public crate-intuicio-backend-rust-0.15.0 (c (n "intuicio-backend-rust") (v "0.15.0") (d (list (d (n "intuicio-core") (r "^0.15") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1c40vfqrx727k7mcwz57mqp39221kkxghj4q3qh8bky8piy2dpvw")))

(define-public crate-intuicio-backend-rust-0.16.0 (c (n "intuicio-backend-rust") (v "0.16.0") (d (list (d (n "intuicio-core") (r "^0.16") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1kzma7rsa7swcyvrvy682j9aanbs36szfwrxl1gp7h67ilfhh5ax")))

(define-public crate-intuicio-backend-rust-0.17.0 (c (n "intuicio-backend-rust") (v "0.17.0") (d (list (d (n "intuicio-core") (r "^0.17") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "143h5848gsslfbn31g09z7chw5kk1236an4bpy7v5r3j8168w5m0")))

(define-public crate-intuicio-backend-rust-0.17.1 (c (n "intuicio-backend-rust") (v "0.17.1") (d (list (d (n "intuicio-core") (r "^0.17") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1wjljkf3k4j6r5hgxxxa458x00xilq6brzgnrfyvlwxp26fhji52")))

(define-public crate-intuicio-backend-rust-0.17.2 (c (n "intuicio-backend-rust") (v "0.17.2") (d (list (d (n "intuicio-core") (r "^0.17") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0zzbvvz23ii0qsh37hbslwkqwmn85hkpipib0dc9s1s79ymmjacc")))

(define-public crate-intuicio-backend-rust-0.17.3 (c (n "intuicio-backend-rust") (v "0.17.3") (d (list (d (n "intuicio-core") (r "^0.17") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "06a5ny50zkz8a91qbm2bdya4fsxcm3ssl26pnpdbicbjnwpm4f9w")))

(define-public crate-intuicio-backend-rust-0.18.0 (c (n "intuicio-backend-rust") (v "0.18.0") (d (list (d (n "intuicio-core") (r "^0.18") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0rl6dyja8xx4szf9rn8qkkjvcmnzrk3q6wvdl1xvfqd2lb8272lf")))

(define-public crate-intuicio-backend-rust-0.18.1 (c (n "intuicio-backend-rust") (v "0.18.1") (d (list (d (n "intuicio-core") (r "^0.18") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1nj9490vjyfnkqm8vv6qpwycagc3dv0315qawf7czx00ws9nb2r8")))

(define-public crate-intuicio-backend-rust-0.18.2 (c (n "intuicio-backend-rust") (v "0.18.2") (d (list (d (n "intuicio-core") (r "^0.18") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "06ximyqdlk528pdgzkd2fd1h4bb6z5sla6drqx1fksgps3fjkdwx")))

(define-public crate-intuicio-backend-rust-0.18.3 (c (n "intuicio-backend-rust") (v "0.18.3") (d (list (d (n "intuicio-core") (r "^0.18") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "08npg12wxh3yxlb3iki7lcpgrqi185x2wrx1g9q7fk6zjgqshpg6")))

(define-public crate-intuicio-backend-rust-0.19.0 (c (n "intuicio-backend-rust") (v "0.19.0") (d (list (d (n "intuicio-core") (r "^0.19") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1h4s0bb320gq68g1r7l2c4npwjk8d01092ljrrqd151fnkpy6iba")))

(define-public crate-intuicio-backend-rust-0.19.1 (c (n "intuicio-backend-rust") (v "0.19.1") (d (list (d (n "intuicio-core") (r "^0.19") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0c9q9b5s363w5a39bxk0wahy3ak3pz35iv24wnf0z99n2qvg5a6b")))

(define-public crate-intuicio-backend-rust-0.20.0 (c (n "intuicio-backend-rust") (v "0.20.0") (d (list (d (n "intuicio-core") (r "^0.20") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "12bxdwgy6d89y1r8cmwxlzvwks2dksmcdc0hhdi6i8skq9qr20cm")))

(define-public crate-intuicio-backend-rust-0.20.1 (c (n "intuicio-backend-rust") (v "0.20.1") (d (list (d (n "intuicio-core") (r "^0.20") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "09bfbbk0lkj35d7x228v3pab0x6hlysyjp7yc18a1miqpnm1gwk9")))

(define-public crate-intuicio-backend-rust-0.20.2 (c (n "intuicio-backend-rust") (v "0.20.2") (d (list (d (n "intuicio-core") (r "^0.20") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "05ck5zcv93qq77d7j4azdy4dxbpb3ck0wyi905nyayhi1nr5bxnv")))

(define-public crate-intuicio-backend-rust-0.20.3 (c (n "intuicio-backend-rust") (v "0.20.3") (d (list (d (n "intuicio-core") (r "^0.20") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "083n81rq0grdz137lqi2zdqy1svpsrplz4zf8mva9zvafcara5x0")))

(define-public crate-intuicio-backend-rust-0.20.4 (c (n "intuicio-backend-rust") (v "0.20.4") (d (list (d (n "intuicio-core") (r "^0.20") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "178218ac5490np38xawncw8ib4d25m6y5vv3x29s9ykp1c6b0a8p")))

(define-public crate-intuicio-backend-rust-0.20.5 (c (n "intuicio-backend-rust") (v "0.20.5") (d (list (d (n "intuicio-core") (r "^0.20") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "03h9jc74vjsfflg3di41vqmxzjxrz3wlkydglvk5jsfia7zry5z6")))

(define-public crate-intuicio-backend-rust-0.21.0 (c (n "intuicio-backend-rust") (v "0.21.0") (d (list (d (n "intuicio-core") (r "^0.21") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0934j900sb0h2gkz82q3wrjkxahx82a39ziy9kb778jm5dgcn4f9")))

(define-public crate-intuicio-backend-rust-0.22.0 (c (n "intuicio-backend-rust") (v "0.22.0") (d (list (d (n "intuicio-core") (r "^0.22") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1kvbcckxnkyqgr02f2ifw4svj7ivi68b6d0sayx58qbzh3fkjrb8")))

(define-public crate-intuicio-backend-rust-0.22.1 (c (n "intuicio-backend-rust") (v "0.22.1") (d (list (d (n "intuicio-core") (r "^0.22") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0mk899l5il54s8xlzg5b0hpamc679mx711ic21mhliyf32fm01n6")))

(define-public crate-intuicio-backend-rust-0.23.0 (c (n "intuicio-backend-rust") (v "0.23.0") (d (list (d (n "intuicio-core") (r "^0.23") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1h5z6w99ak7vslj49jvqlq4gg3vavgzcc7labmg8vj1ss6nk4fmq")))

(define-public crate-intuicio-backend-rust-0.23.1 (c (n "intuicio-backend-rust") (v "0.23.1") (d (list (d (n "intuicio-core") (r "^0.23") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1974jhyrl6hsindwbprcz7qlghfg208za0hn5da6ngzwjgnfxx9z")))

(define-public crate-intuicio-backend-rust-0.24.0 (c (n "intuicio-backend-rust") (v "0.24.0") (d (list (d (n "intuicio-core") (r "^0.24") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1chf634s0rshikr652n308b4bw7dwz0kw554hppqdcb5vz48v8jh")))

(define-public crate-intuicio-backend-rust-0.24.1 (c (n "intuicio-backend-rust") (v "0.24.1") (d (list (d (n "intuicio-core") (r "^0.24") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "098h52ckraav801h9jyzv8d0d4g496yphlaxk7p51w3j95lf270z")))

(define-public crate-intuicio-backend-rust-0.24.2 (c (n "intuicio-backend-rust") (v "0.24.2") (d (list (d (n "intuicio-core") (r "^0.24") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0hzbcrgp0ym05ipsyqr085mnd0wspywq3y5qczggwaxdn7wh7ysn")))

(define-public crate-intuicio-backend-rust-0.25.0 (c (n "intuicio-backend-rust") (v "0.25.0") (d (list (d (n "intuicio-core") (r "^0.25") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.25") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1cgz7k811h8x0h1rl7vfx93rpjvk3604352723m18l8nfsai3qlm")))

(define-public crate-intuicio-backend-rust-0.26.0 (c (n "intuicio-backend-rust") (v "0.26.0") (d (list (d (n "intuicio-core") (r "^0.26") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.26") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1dcj27hjqk439b20qhgqvd909s5cdj8xwvr3bvh56abyf4l93rcn")))

(define-public crate-intuicio-backend-rust-0.27.0 (c (n "intuicio-backend-rust") (v "0.27.0") (d (list (d (n "intuicio-core") (r "^0.27") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1icc4wl8srqj0bcwxbmhkqmjicfzxkz91yr6kkl3w8n08cmssnj7")))

(define-public crate-intuicio-backend-rust-0.27.1 (c (n "intuicio-backend-rust") (v "0.27.1") (d (list (d (n "intuicio-core") (r "^0.27") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1z4dhmcfh351x34z7gjm0arm4gpiyz9dlqlgh44wm3101fzr7v5y")))

(define-public crate-intuicio-backend-rust-0.27.2 (c (n "intuicio-backend-rust") (v "0.27.2") (d (list (d (n "intuicio-core") (r "^0.27") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1z9mlcdr4wa767h999srkh1xfz9z818cxmcm8rdm4x4fx01r16mh")))

(define-public crate-intuicio-backend-rust-0.28.0 (c (n "intuicio-backend-rust") (v "0.28.0") (d (list (d (n "intuicio-core") (r "^0.28") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0dgh668xyn89lx14n8q29hk0nimn0mwryvskw6ilfllzg9h75j1i")))

(define-public crate-intuicio-backend-rust-0.28.1 (c (n "intuicio-backend-rust") (v "0.28.1") (d (list (d (n "intuicio-core") (r "^0.28") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1782a1xrr0yxi5sq70nywyn8d3n8b18l1jmjw36k0bmfhbkg7sww")))

(define-public crate-intuicio-backend-rust-0.28.2 (c (n "intuicio-backend-rust") (v "0.28.2") (d (list (d (n "intuicio-core") (r "^0.28") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0fl83vivywnpr1q47786h5p5qj26p6fig9nvarxrv3rnwvfhza1y")))

(define-public crate-intuicio-backend-rust-0.28.3 (c (n "intuicio-backend-rust") (v "0.28.3") (d (list (d (n "intuicio-core") (r "^0.28") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "07zm6qkpa98lm5midxsvbahi0lmq53q03z646gb6k8sqmgj1rz1k")))

(define-public crate-intuicio-backend-rust-0.29.0 (c (n "intuicio-backend-rust") (v "0.29.0") (d (list (d (n "intuicio-core") (r "^0.29") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.29") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "100pklkggp4w6q918awlk5b62lgchkcikyj1jqg708pjary76bmb")))

(define-public crate-intuicio-backend-rust-0.29.1 (c (n "intuicio-backend-rust") (v "0.29.1") (d (list (d (n "intuicio-core") (r "^0.29") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.29") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "17gky5dxmmf26p3s2qwai24cbkvgpnvi7m08jj39z9vlwsy8mwzy")))

(define-public crate-intuicio-backend-rust-0.30.0 (c (n "intuicio-backend-rust") (v "0.30.0") (d (list (d (n "intuicio-core") (r "^0.30") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.30") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1msr9wq07smpfmd8kh8hhwwlqldvxbbcl7ckx1crgg4a4xqhxj1s")))

