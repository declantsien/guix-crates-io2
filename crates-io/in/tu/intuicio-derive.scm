(define-module (crates-io in tu intuicio-derive) #:use-module (crates-io))

(define-public crate-intuicio-derive-0.1.0 (c (n "intuicio-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1snj8vpy5bv4fczanxa75dyxw93a9i56jmfhxyhklnvzfsrpx91a")))

(define-public crate-intuicio-derive-0.2.0 (c (n "intuicio-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ss2awpkn2kynk6ycgmvj6151gq7bpsrmvdifl5w6kb6qysbnhhj")))

(define-public crate-intuicio-derive-0.3.0 (c (n "intuicio-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lxjnlhndwiar5qk28xvdah525igcbkaxv2p8cl86p9g6q2b4wnj")))

(define-public crate-intuicio-derive-0.3.1 (c (n "intuicio-derive") (v "0.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jyahwbw4hn2rd8q4as4g92qfrcasp86wk229x0qijj8k70h8baf")))

(define-public crate-intuicio-derive-0.3.2 (c (n "intuicio-derive") (v "0.3.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1z04r0xqr1sbj570kks2qw4w7j20ykhjy2cpbsfvld44gwi2w7iy")))

(define-public crate-intuicio-derive-0.3.3 (c (n "intuicio-derive") (v "0.3.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lfpqnx6h30sc110idrxqz43fc9f7prqrs7yjbnm8mbq8sfg7vhm")))

(define-public crate-intuicio-derive-0.3.4 (c (n "intuicio-derive") (v "0.3.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0g04i8xafk5vv9jyjl52lqpxaq97gyvhxrw7mdykfm9awi0m3sij")))

(define-public crate-intuicio-derive-0.3.5 (c (n "intuicio-derive") (v "0.3.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "131fvkm9yxnn75xdgm4smyjr11rig6pk518ygpdgknv2hmv4k8fd")))

(define-public crate-intuicio-derive-0.3.6 (c (n "intuicio-derive") (v "0.3.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xnw5ybrsgid64x3j8grqpsibq3gryw3yjjl3l4m6ia6nwh8m00w")))

(define-public crate-intuicio-derive-0.3.7 (c (n "intuicio-derive") (v "0.3.7") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "151kayc1ii5kqxm8n3m2a62kjw0m2fb27mvirixjw7ab1hkm8zyj")))

(define-public crate-intuicio-derive-0.4.0 (c (n "intuicio-derive") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0k2888gfxfgs0d56y73ph5dxvy674w0qrwrxzhhl80j61clgk8iq")))

(define-public crate-intuicio-derive-0.5.0 (c (n "intuicio-derive") (v "0.5.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f45383ha8psmjj9dncay5yc7y8aj57d1h4avvzr6p09x3x8v8w2")))

(define-public crate-intuicio-derive-0.5.1 (c (n "intuicio-derive") (v "0.5.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1izlhn9379x32cmizbyvcna35gg75dsc4l4y4yy9f4qfp1wr592j")))

(define-public crate-intuicio-derive-0.5.2 (c (n "intuicio-derive") (v "0.5.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "017i5hvvgy1hywsdfn5as01h3n6lmsmpgx52gpmc5aarsqpibl3p")))

(define-public crate-intuicio-derive-0.6.0 (c (n "intuicio-derive") (v "0.6.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vwkv4n2nrgyh92nbd2rj9s448vdmxq18qgn43psc91ini43a90w")))

(define-public crate-intuicio-derive-0.6.1 (c (n "intuicio-derive") (v "0.6.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "041c18bapnvl489mfpchwppapk10d2bzsa1d3dbrx07kqsszwdmf")))

(define-public crate-intuicio-derive-0.7.0 (c (n "intuicio-derive") (v "0.7.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sknm3sx3d9g84arlxqz6ny781g90y9vkbq9mh4qqmamh7rxy886")))

(define-public crate-intuicio-derive-0.8.0 (c (n "intuicio-derive") (v "0.8.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0s65aalxy5kq4hq4zp2bajlybv1pj8c12smc2y7l0cc17w703g96")))

(define-public crate-intuicio-derive-0.8.1 (c (n "intuicio-derive") (v "0.8.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1m4zzbzlgp7pagyr279dmza4kc6004wy3z4shfd8x5bcw1qpby8y")))

(define-public crate-intuicio-derive-0.9.0 (c (n "intuicio-derive") (v "0.9.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1r6g4244jv6816ryyx7739y74qrik60w6zg0336c9hnxvw12wca1")))

(define-public crate-intuicio-derive-0.10.0 (c (n "intuicio-derive") (v "0.10.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05g9px3f5psbcdgwg3p67gs25j2mfdg21j63cqd1674d9q7lk1a9")))

(define-public crate-intuicio-derive-0.10.1 (c (n "intuicio-derive") (v "0.10.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1a7m6f2czi7pbfannxfpvjawi3i385j5d0k3153q66lzzysmhayg")))

(define-public crate-intuicio-derive-0.11.0 (c (n "intuicio-derive") (v "0.11.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "015ldnjd5a51ckyvi7xbrm8ac7w36rgs6w5cyl762crhxid942li")))

(define-public crate-intuicio-derive-0.11.1 (c (n "intuicio-derive") (v "0.11.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03nx0dbf0157hqk40iz4h8q0kqg28m16qzjk9cd5zmwwpf9sbfzz")))

(define-public crate-intuicio-derive-0.11.2 (c (n "intuicio-derive") (v "0.11.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1w283xm6108ykvddm1ra0sgnfa7rq6jv7118ldn4cj2yns61h6s6")))

(define-public crate-intuicio-derive-0.12.0 (c (n "intuicio-derive") (v "0.12.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "12as0cbp5m7mq8rwqsqxqcaw0jyi39wp9sl1w8q7aizava6kg24g")))

(define-public crate-intuicio-derive-0.12.1 (c (n "intuicio-derive") (v "0.12.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "13i1w1ny514lcjnpvvbmpgsq9xi45r45frpj7nx9in54sldk84c2")))

(define-public crate-intuicio-derive-0.12.2 (c (n "intuicio-derive") (v "0.12.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ranc08lzrjncqw4h21l000v9qgjzbbypwpfv9civky45rl7gc10")))

(define-public crate-intuicio-derive-0.12.3 (c (n "intuicio-derive") (v "0.12.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0scx9wc02wb4zcwbscs242775xcbca8s3z8cxcx98fgg04ivzh8n")))

(define-public crate-intuicio-derive-0.13.0 (c (n "intuicio-derive") (v "0.13.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0jl1iajbh7ky9j0xsvs9xr3xf8mcgy9qydn2m0las437vl1iyw0b")))

(define-public crate-intuicio-derive-0.13.1 (c (n "intuicio-derive") (v "0.13.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hk586larv6bpbf85qg30gna4l9iswwwn8dm5rqyizyxgvkq6jha")))

(define-public crate-intuicio-derive-0.14.0 (c (n "intuicio-derive") (v "0.14.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wl9l0l9l9h3lyhmkqynl02jmc9mqw0bqnigfzs97jl49gigagns")))

(define-public crate-intuicio-derive-0.15.0 (c (n "intuicio-derive") (v "0.15.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0548jcwaxm2m0cm4445jfaj7qf7m3dah6dfnc22g1yjrng6ms1cd")))

(define-public crate-intuicio-derive-0.16.0 (c (n "intuicio-derive") (v "0.16.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jinv2pqrmygmprz2qlsdd5inyjycw4fm04r19h1h9v1vvd8c8cy")))

(define-public crate-intuicio-derive-0.17.0 (c (n "intuicio-derive") (v "0.17.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11hbsyyp5bcmqzi86wm6l5z898dvb1x4zx69ip1al8cqy0cx44l8")))

(define-public crate-intuicio-derive-0.17.1 (c (n "intuicio-derive") (v "0.17.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17ffbfshb0i3h0nv1x6l7nb2rcaxcdn0gc29a2i88gwmw2lp1j9d")))

(define-public crate-intuicio-derive-0.17.2 (c (n "intuicio-derive") (v "0.17.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xhpyxim7lwb80lwf1nxqvd4xii4yqlxzb1jxx78zr97vl7qlv64")))

(define-public crate-intuicio-derive-0.17.3 (c (n "intuicio-derive") (v "0.17.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0nkda72vp71bgi5lv40980g38gqz17d09b2wzg35a3h2rm9n933g")))

(define-public crate-intuicio-derive-0.18.0 (c (n "intuicio-derive") (v "0.18.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09z0qi9vfizp0f350zg54grlig7m23avwm6v26lcngvh02sxlcyd")))

(define-public crate-intuicio-derive-0.18.1 (c (n "intuicio-derive") (v "0.18.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gpwzrk1kl978swz524mh5l0pb7n7w69hik8hvg16ax3y1vb9awh")))

(define-public crate-intuicio-derive-0.18.2 (c (n "intuicio-derive") (v "0.18.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ifbxnlhkjywy2fpqfpdc2w4q91wh2dbibz746fcasbb57m3is4c")))

(define-public crate-intuicio-derive-0.18.3 (c (n "intuicio-derive") (v "0.18.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1sqml37rbyizcczmw9a30nvr6iwsjy18rd5wph97hh4y0758an2b")))

(define-public crate-intuicio-derive-0.19.0 (c (n "intuicio-derive") (v "0.19.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qsz2fmvqihxblirrpcskixc1rafpy7l0c4ys90148hzmymsjdhb")))

(define-public crate-intuicio-derive-0.19.1 (c (n "intuicio-derive") (v "0.19.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19yypzqi07666gjdwnwrq319vnyr93cmk66hpzg0jaw5nkgz78kr")))

(define-public crate-intuicio-derive-0.20.0 (c (n "intuicio-derive") (v "0.20.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16nskivdbdff7l27gnq9kf4lplsypg1naqdlsqjdssclpn8smn5g")))

(define-public crate-intuicio-derive-0.20.1 (c (n "intuicio-derive") (v "0.20.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0y3y3jq29zrir4rjvrrry3l2xr8y6k2nqdbwpdfkf5vagy0riy0g")))

(define-public crate-intuicio-derive-0.20.2 (c (n "intuicio-derive") (v "0.20.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0c0jqsbkwzlivz5fawrdzp04khsr8g5c6k4g1s4k0jdwn8cr834p")))

(define-public crate-intuicio-derive-0.20.3 (c (n "intuicio-derive") (v "0.20.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cq0ldngdpl6iwb8xa5jfcyml07sm1lssaba31hab66ajl30jppd")))

(define-public crate-intuicio-derive-0.20.4 (c (n "intuicio-derive") (v "0.20.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1li47lqiacdd3llv5irvvx9il4xvyz2bc28idmphv6vlsh7ciwl7")))

(define-public crate-intuicio-derive-0.20.5 (c (n "intuicio-derive") (v "0.20.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00klw8rbf1fvchyi7691xav4mljf3x6n32606d4kir4k8aj9vyns")))

(define-public crate-intuicio-derive-0.21.0 (c (n "intuicio-derive") (v "0.21.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xyjhkdm1r2wgnhla38430c09g4af0ia7vnv4q4sbcl6qrjr5ac5")))

(define-public crate-intuicio-derive-0.22.0 (c (n "intuicio-derive") (v "0.22.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1aql5vm7b7nyk23zzsqhf1ha82yg07z6j59ap144i3la3mvz4x03")))

(define-public crate-intuicio-derive-0.22.1 (c (n "intuicio-derive") (v "0.22.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09z80ic7wm4068v6zvm5cw4xbrbmzk6cwmsx8469agwq7z7i311s")))

(define-public crate-intuicio-derive-0.23.0 (c (n "intuicio-derive") (v "0.23.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0k67rzj5b4swm2b8mswwgzvpf4wid8sf1q7lfwmkw4wn3c3wrva8")))

(define-public crate-intuicio-derive-0.23.1 (c (n "intuicio-derive") (v "0.23.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1adf1cgzjypi42z7s4bfkfh0lg49ggzmjsyyvinbg6jq6dh0wryr")))

(define-public crate-intuicio-derive-0.24.0 (c (n "intuicio-derive") (v "0.24.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0irxayr9mhanxy9g7b0gni8b0zzrm7slwmyl97mgq8il1s6qsz3z")))

(define-public crate-intuicio-derive-0.24.1 (c (n "intuicio-derive") (v "0.24.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1137p8cljpdhdnq4zjbrviy5gq4gz9k7xiw1d8vm7qzr4knb0sal")))

(define-public crate-intuicio-derive-0.24.2 (c (n "intuicio-derive") (v "0.24.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "163w4yws7yhm98d56g7zg6f7q0l39xkl2nffxg01hhs6dzvp8lk6")))

(define-public crate-intuicio-derive-0.25.0 (c (n "intuicio-derive") (v "0.25.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vvgl62wm6rx5lqv11183x0w8s06p97rrbm51c2892ml9n3r7hfn")))

(define-public crate-intuicio-derive-0.26.0 (c (n "intuicio-derive") (v "0.26.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04dd4k1xqx7ibljjla6brwy8qgyxn5dfa8l602x61b715mdbp2x1")))

(define-public crate-intuicio-derive-0.27.0 (c (n "intuicio-derive") (v "0.27.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qcfjxry9hp68nb6kfxg28916qqj7wia0nh3v4fas7jpyx0l9y8x")))

(define-public crate-intuicio-derive-0.27.1 (c (n "intuicio-derive") (v "0.27.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ismyv6rgxlb856g0cjjrki43nl687pvp43lvx1qm9f0c25i9f40")))

(define-public crate-intuicio-derive-0.27.2 (c (n "intuicio-derive") (v "0.27.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0liw1hijgpsdz5m6kwa5l2pp2hm92dyx12qh8rxsppzhywazs8wj")))

(define-public crate-intuicio-derive-0.28.0 (c (n "intuicio-derive") (v "0.28.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "185j7qqsjz7lp8hqiks1n1gwmp9q0hqqnrik5lmq5sn7lmk5b7fv")))

(define-public crate-intuicio-derive-0.28.1 (c (n "intuicio-derive") (v "0.28.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0x5n824pz2arw601zqkh02vmrr39w47806cx84gj8bqssqw2p39l")))

(define-public crate-intuicio-derive-0.28.2 (c (n "intuicio-derive") (v "0.28.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02lf3zk4d6yn154anylpgmmm8k0i4k6940a1zcmzsvqz965x9l5d")))

(define-public crate-intuicio-derive-0.28.3 (c (n "intuicio-derive") (v "0.28.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fgwzzq9hpy59lmzcjg319g982s8pa54m0lb3j3kxpbhp5sgdrhx")))

(define-public crate-intuicio-derive-0.29.0 (c (n "intuicio-derive") (v "0.29.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rb9577wsm0h2mpx9y9n13rb67f7bgd0lxpgl3gqlp2l4j9njv97")))

(define-public crate-intuicio-derive-0.29.1 (c (n "intuicio-derive") (v "0.29.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1a6ncdnp56d18r5fb9jscs0fpsynqy6w0yv99z5a4y4qlrc2m1i1")))

(define-public crate-intuicio-derive-0.30.0 (c (n "intuicio-derive") (v "0.30.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1200n420zvfr6ryldm16frhb2w98cvbnhc0scv45cjafgl98yn8l")))

(define-public crate-intuicio-derive-0.31.0 (c (n "intuicio-derive") (v "0.31.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mczzkmm5vnwi1jxlalyd8nlsp1kirbjlzj435a5avha1rlkc96s")))

(define-public crate-intuicio-derive-0.31.1 (c (n "intuicio-derive") (v "0.31.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pylh47nzs6gfn0gx7f2rmbnxhy4jhcrzpg53wfz0im5414kbmgi")))

(define-public crate-intuicio-derive-0.31.2 (c (n "intuicio-derive") (v "0.31.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ncv8h0vssf8cy2yaw69s3yls701xj4f07is5cx4vn31idmn6hi4")))

(define-public crate-intuicio-derive-0.31.3 (c (n "intuicio-derive") (v "0.31.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01blfaz2sz22cjl5ay7wqy67hwbrsaxxc0967rxs0nb9fpqcp7rb")))

(define-public crate-intuicio-derive-0.31.4 (c (n "intuicio-derive") (v "0.31.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qvn5flqi1f27ywqmxvggj8k0wwi29jdq4m6fmc4a765c2yblcvp")))

(define-public crate-intuicio-derive-0.31.5 (c (n "intuicio-derive") (v "0.31.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wxklwl7gqzc9k7h44ablfk8qlmxd39l01bqwjwz76z4r0p0k2bf")))

(define-public crate-intuicio-derive-0.31.6 (c (n "intuicio-derive") (v "0.31.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09455dxdm9zpm1z8dq3vv50hg33bk4djcm6bljs89vw9s43bj3mh")))

