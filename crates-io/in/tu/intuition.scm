(define-module (crates-io in tu intuition) #:use-module (crates-io))

(define-public crate-intuition-0.1.0 (c (n "intuition") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1c1sj8r7pgsf55vjslpf6kvpcl3d13v7wh4v5pbsc6wsvlqbd8k5")))

(define-public crate-intuition-0.2.0 (c (n "intuition") (v "0.2.0") (d (list (d (n "concat-idents") (r "^1.1.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "108h4722lhk31qihbq547idjq1lrdal1qc68lah7skzv19aispr6")))

(define-public crate-intuition-0.2.1 (c (n "intuition") (v "0.2.1") (d (list (d (n "concat-idents") (r "^1.1.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "00fwy1sfkdf02mlag36xnm7x27gzgk8i1drabkg9flkncda38fj1")))

(define-public crate-intuition-0.2.2 (c (n "intuition") (v "0.2.2") (d (list (d (n "concat-idents") (r "^1.1.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "0cdx9dlac8rizqhvb8mr9d709zrrg3c8x93f81ykdh5mvffl7qhs")))

