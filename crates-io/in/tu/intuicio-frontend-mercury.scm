(define-module (crates-io in tu intuicio-frontend-mercury) #:use-module (crates-io))

(define-public crate-intuicio-frontend-mercury-0.25.0 (c (n "intuicio-frontend-mercury") (v "0.25.0") (d (list (d (n "intuicio-backend-vm") (r "^0.25") (d #t) (k 2)) (d (n "intuicio-core") (r "^0.25") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.25") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "snailquote") (r "^0.3") (d #t) (k 0)))) (h "1m2f3a307nxp7s0xf63hxdiciddhvyl212m32icy282ahn825453")))

(define-public crate-intuicio-frontend-mercury-0.26.0 (c (n "intuicio-frontend-mercury") (v "0.26.0") (d (list (d (n "intuicio-backend-vm") (r "^0.26") (d #t) (k 2)) (d (n "intuicio-core") (r "^0.26") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.26") (d #t) (k 0)) (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "snailquote") (r "^0.3") (d #t) (k 0)))) (h "03wgj7ppx8l0k819sd8s8bfykkx2hhgl8wi518i8m9y0j92a2ia3")))

