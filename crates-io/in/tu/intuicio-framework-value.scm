(define-module (crates-io in tu intuicio-framework-value) #:use-module (crates-io))

(define-public crate-intuicio-framework-value-0.29.1 (c (n "intuicio-framework-value") (v "0.29.1") (d (list (d (n "intuicio-core") (r "^0.29") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.29") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.29") (d #t) (k 2)))) (h "0ihqxv1ssyizr3l8r6fmllhhqqgw6jvhjk0g5ijgg15s6ai28g48")))

(define-public crate-intuicio-framework-value-0.30.0 (c (n "intuicio-framework-value") (v "0.30.0") (d (list (d (n "intuicio-data") (r "^0.30") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.30") (d #t) (k 2)))) (h "0wgdghgdpizg3qk838zy93a7qqj20j6hd2g0kdhszmb6ygysjdli")))

(define-public crate-intuicio-framework-value-0.31.0 (c (n "intuicio-framework-value") (v "0.31.0") (d (list (d (n "intuicio-data") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.31") (d #t) (k 2)))) (h "0wdym9gdjbc5hjik9bprib8ixk9if5w7psbcps4af8g6hlv6sfck")))

(define-public crate-intuicio-framework-value-0.31.1 (c (n "intuicio-framework-value") (v "0.31.1") (d (list (d (n "intuicio-data") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.31") (d #t) (k 2)))) (h "13jvra8x1xflgjcn6xbnfxqn46m6lwbrnqk8c539x3rfa0yhqicj")))

(define-public crate-intuicio-framework-value-0.31.2 (c (n "intuicio-framework-value") (v "0.31.2") (d (list (d (n "intuicio-data") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.31") (d #t) (k 2)))) (h "092wgnw2143v15fqiwfcnjvqyg6al3840cypmv5pnpgb00dz26i8")))

(define-public crate-intuicio-framework-value-0.31.3 (c (n "intuicio-framework-value") (v "0.31.3") (d (list (d (n "intuicio-data") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.31") (d #t) (k 2)))) (h "1wh212pz50k9s7dl188qc495bsairqvhssi8ccll985m3g7pr8s5")))

(define-public crate-intuicio-framework-value-0.31.4 (c (n "intuicio-framework-value") (v "0.31.4") (d (list (d (n "intuicio-data") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.31") (d #t) (k 2)))) (h "18rd569d937ll94pr7jan892pgwnzj4ldac51x6cq0r2cv3dxjkx")))

(define-public crate-intuicio-framework-value-0.31.5 (c (n "intuicio-framework-value") (v "0.31.5") (d (list (d (n "intuicio-data") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.31") (d #t) (k 2)))) (h "1vg07qr3sjg6xlfwib0c12w0z51r17bibdz7vs03gizpzqjyxs4r")))

(define-public crate-intuicio-framework-value-0.31.6 (c (n "intuicio-framework-value") (v "0.31.6") (d (list (d (n "intuicio-data") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-derive") (r "^0.31") (d #t) (k 2)))) (h "1hxmshhb6zlrp9z7kdsyi60r15divichbf1bhyy4zgvynsc2y64g")))

