(define-module (crates-io in tu intuple) #:use-module (crates-io))

(define-public crate-intuple-0.1.0 (c (n "intuple") (v "0.1.0") (d (list (d (n "intuple_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1halggkfd5fnc98ha67z5xg1dignjh186qg57i4nix5f7is3i0ck")))

(define-public crate-intuple-0.2.0 (c (n "intuple") (v "0.2.0") (d (list (d (n "intuple_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1lxc0cac0z5bn57vvh6b2hlbwi48myr7mfp8wpf3lj1y07ms57ri")))

