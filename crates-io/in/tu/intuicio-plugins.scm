(define-module (crates-io in tu intuicio-plugins) #:use-module (crates-io))

(define-public crate-intuicio-plugins-0.6.0 (c (n "intuicio-plugins") (v "0.6.0") (d (list (d (n "intuicio-core") (r "^0.6") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1i1kbl1hhnv6qrx68l36zvlqpdkk9034y131idf1s9sf71jysks0")))

(define-public crate-intuicio-plugins-0.6.1 (c (n "intuicio-plugins") (v "0.6.1") (d (list (d (n "intuicio-core") (r "^0.6") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "166mih44z1r8ppa59iz3hj58qp6p8ikji7vh07ckmcf0kq0as10w")))

(define-public crate-intuicio-plugins-0.7.0 (c (n "intuicio-plugins") (v "0.7.0") (d (list (d (n "intuicio-core") (r "^0.7") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1ndy509az579dzpdvq3nik4dsjyqr1y66z5gbh0sppmc388i18qq")))

(define-public crate-intuicio-plugins-0.8.0 (c (n "intuicio-plugins") (v "0.8.0") (d (list (d (n "intuicio-core") (r "^0.8") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1anw8z6xd35wvy3xw5iw5v1znwi2qa8j7v1pr405yrkkl09knfgd")))

(define-public crate-intuicio-plugins-0.8.1 (c (n "intuicio-plugins") (v "0.8.1") (d (list (d (n "intuicio-core") (r "^0.8") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "193yryay1qxgglr4id9794saf8hm87h4vlvjwrwdr9jpxy10c7dr")))

(define-public crate-intuicio-plugins-0.9.0 (c (n "intuicio-plugins") (v "0.9.0") (d (list (d (n "intuicio-core") (r "^0.9") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0394fh0lllfan0la5z3fpic9sls2zvn2zbni1nm4ck8d1p0b72pd")))

(define-public crate-intuicio-plugins-0.10.0 (c (n "intuicio-plugins") (v "0.10.0") (d (list (d (n "intuicio-core") (r "^0.10") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1016licsiy8nxly83a4nb24qk4amc8jhs1gh7nhl3w92hq2qsc3w")))

(define-public crate-intuicio-plugins-0.10.1 (c (n "intuicio-plugins") (v "0.10.1") (d (list (d (n "intuicio-core") (r "^0.10") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1r5fkpc9i2j5z9s13zyqxdqay0ds4kiczkanhc5bz385dz0q7x4x")))

(define-public crate-intuicio-plugins-0.11.0 (c (n "intuicio-plugins") (v "0.11.0") (d (list (d (n "intuicio-core") (r "^0.11") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "06haawfrnfdv94rf6yvd75aljbmawrypf3cahbjl327hqvqihy0n")))

(define-public crate-intuicio-plugins-0.11.1 (c (n "intuicio-plugins") (v "0.11.1") (d (list (d (n "intuicio-core") (r "^0.11") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "17q3fnb8xpggygbnxn7wlj3cagqvmjgyq691l35ir3bzdrn5q3jr")))

(define-public crate-intuicio-plugins-0.11.2 (c (n "intuicio-plugins") (v "0.11.2") (d (list (d (n "intuicio-core") (r "^0.11") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1azjdxh5jb41y5is2752cc7iqavipykwkni3npdh04fwvpvcgpk3")))

(define-public crate-intuicio-plugins-0.12.0 (c (n "intuicio-plugins") (v "0.12.0") (d (list (d (n "intuicio-core") (r "^0.12") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "17lak8pzjxfyf2cymr89jgnv43j0k4iz3vpm4wyv625ba6snzqa4")))

(define-public crate-intuicio-plugins-0.12.1 (c (n "intuicio-plugins") (v "0.12.1") (d (list (d (n "intuicio-core") (r "^0.12") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1h6qp9ky72dz631a9462rgz1x14bs5m54p02dd7njhb7mk5lhbm8")))

(define-public crate-intuicio-plugins-0.12.2 (c (n "intuicio-plugins") (v "0.12.2") (d (list (d (n "intuicio-core") (r "^0.12") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0wj5xbax4iiz2m4zjvy8ll2nd8wwbgjpkwc5xfhj8njz81yng4rx")))

(define-public crate-intuicio-plugins-0.12.3 (c (n "intuicio-plugins") (v "0.12.3") (d (list (d (n "intuicio-core") (r "^0.12") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0rjvrmsiyx5f6w7jpi4srfkyh8i5hjs06r2sg50sipnlw6p3mxip")))

(define-public crate-intuicio-plugins-0.13.0 (c (n "intuicio-plugins") (v "0.13.0") (d (list (d (n "intuicio-core") (r "^0.13") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0mgjsdylj5bcmjn2div3qk4cj9rzri1l3g2kqmvvssvaxlivgggr")))

(define-public crate-intuicio-plugins-0.13.1 (c (n "intuicio-plugins") (v "0.13.1") (d (list (d (n "intuicio-core") (r "^0.13") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "018fanirgwkdmflx73wx41bbsbi7k7g17f5w7c7amjbb16cwjqal")))

(define-public crate-intuicio-plugins-0.14.0 (c (n "intuicio-plugins") (v "0.14.0") (d (list (d (n "intuicio-core") (r "^0.14") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0nxixzy9sk477saw4nbx3ksj23ch00plln1nvpwsz1nxmjaw120j")))

(define-public crate-intuicio-plugins-0.15.0 (c (n "intuicio-plugins") (v "0.15.0") (d (list (d (n "intuicio-core") (r "^0.15") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "17z5n3x0dxbiqy7w00mdmky17zwh0kgcrdpkwkjclkqcz5p7m8b5")))

(define-public crate-intuicio-plugins-0.16.0 (c (n "intuicio-plugins") (v "0.16.0") (d (list (d (n "intuicio-core") (r "^0.16") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0qrvb37v6r4s2b6gbwvz3pgc72c75k0xp31hcy10dwikgm5nh4s7")))

(define-public crate-intuicio-plugins-0.17.0 (c (n "intuicio-plugins") (v "0.17.0") (d (list (d (n "intuicio-core") (r "^0.17") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0gqaknqww1pm572ky55r30bpkpig35qxy5z0w6x4cn73yrfc6a8z")))

(define-public crate-intuicio-plugins-0.17.1 (c (n "intuicio-plugins") (v "0.17.1") (d (list (d (n "intuicio-core") (r "^0.17") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "01ppagpz2l2vy9ikvaxakwwsn7ffs1f6bl2lqs7fnxq2s0nzr1pm")))

(define-public crate-intuicio-plugins-0.17.2 (c (n "intuicio-plugins") (v "0.17.2") (d (list (d (n "intuicio-core") (r "^0.17") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0mznd1563kh693v3x3wp3ailr6czdlly68q71hvc34z4rc26n7c1")))

(define-public crate-intuicio-plugins-0.17.3 (c (n "intuicio-plugins") (v "0.17.3") (d (list (d (n "intuicio-core") (r "^0.17") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "072bpz28vr89ggmnw4hf9hz0ckmzf0z8khrkcv2g8m0gp4qin1xh")))

(define-public crate-intuicio-plugins-0.18.0 (c (n "intuicio-plugins") (v "0.18.0") (d (list (d (n "intuicio-core") (r "^0.18") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1dir3lyysq941agrkx727zzsjh4csp4na9k2iy70106z7x5pzm62")))

(define-public crate-intuicio-plugins-0.18.1 (c (n "intuicio-plugins") (v "0.18.1") (d (list (d (n "intuicio-core") (r "^0.18") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0aqqs2gjkdizqhxp4z4zxki9b78nq7xrij9dy34ig9hlwsgsj26b")))

(define-public crate-intuicio-plugins-0.18.2 (c (n "intuicio-plugins") (v "0.18.2") (d (list (d (n "intuicio-core") (r "^0.18") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0gqjzciw7ysjqwflfjbxg2clyhxz1kv4khygpgaxfyzfz8jbq28p")))

(define-public crate-intuicio-plugins-0.18.3 (c (n "intuicio-plugins") (v "0.18.3") (d (list (d (n "intuicio-core") (r "^0.18") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0d9441w08333mws6fs8jqv7n0hbxy44k6nmzjp54wfvl0w91qsi4")))

(define-public crate-intuicio-plugins-0.19.0 (c (n "intuicio-plugins") (v "0.19.0") (d (list (d (n "intuicio-core") (r "^0.19") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "01n9b7jdbqpsiqx63f102vw7b7xpmygvflxzvz49n1rs6b4ds51a")))

(define-public crate-intuicio-plugins-0.19.1 (c (n "intuicio-plugins") (v "0.19.1") (d (list (d (n "intuicio-core") (r "^0.19") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1q6xx5kcxzm55dq7mld0px3sg12p9jl8zzysdqysn339ljsfwwx2")))

(define-public crate-intuicio-plugins-0.20.0 (c (n "intuicio-plugins") (v "0.20.0") (d (list (d (n "intuicio-core") (r "^0.20") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0005i9822dm2nmjhv8q0f4jnsskw8b5x2y267apap66l3dbfvfi5")))

(define-public crate-intuicio-plugins-0.20.1 (c (n "intuicio-plugins") (v "0.20.1") (d (list (d (n "intuicio-core") (r "^0.20") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0lfp6z3gf7xqxz2czryj6nl3yn4dqjv0rh0qvv57gf466js3qzvq")))

(define-public crate-intuicio-plugins-0.20.2 (c (n "intuicio-plugins") (v "0.20.2") (d (list (d (n "intuicio-core") (r "^0.20") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1yfv8kwv7752vyyc0h6q2lvriy9m7zwv1bn7d09hhk6ikarnz824")))

(define-public crate-intuicio-plugins-0.20.3 (c (n "intuicio-plugins") (v "0.20.3") (d (list (d (n "intuicio-core") (r "^0.20") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "07r7yf2zad1ipb7hv5k9qam34ydmb6dpv1qg25ahmvbgk7hfinfk")))

(define-public crate-intuicio-plugins-0.20.4 (c (n "intuicio-plugins") (v "0.20.4") (d (list (d (n "intuicio-core") (r "^0.20") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1rbb63ywsw2jr4s9q21ml8n1rsn4z3ybzzs2cwd600i2clipvnbr")))

(define-public crate-intuicio-plugins-0.20.5 (c (n "intuicio-plugins") (v "0.20.5") (d (list (d (n "intuicio-core") (r "^0.20") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0ysnhs102s95f4idf0gd0ggbrg2ldlrq00a31d0bb33v643rwgd5")))

(define-public crate-intuicio-plugins-0.21.0 (c (n "intuicio-plugins") (v "0.21.0") (d (list (d (n "intuicio-core") (r "^0.21") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1bxgbavldqx306lcsqws0zm71ic4ls9nva9cc5nww8sjddvzmax2")))

(define-public crate-intuicio-plugins-0.22.0 (c (n "intuicio-plugins") (v "0.22.0") (d (list (d (n "intuicio-core") (r "^0.22") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0vc1l1n64svmxa9hxdv574g8kz576kv17maivpg496jh6rr0vwnj")))

(define-public crate-intuicio-plugins-0.22.1 (c (n "intuicio-plugins") (v "0.22.1") (d (list (d (n "intuicio-core") (r "^0.22") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1anr2anvvyf0yl638687vahxqgnwh01h4h5d0cissr8nidh9m40q")))

(define-public crate-intuicio-plugins-0.23.0 (c (n "intuicio-plugins") (v "0.23.0") (d (list (d (n "intuicio-core") (r "^0.23") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0jkmk1l240wh9p76npw5mls1c563dyn7axc205vr0insmvg46fav")))

(define-public crate-intuicio-plugins-0.23.1 (c (n "intuicio-plugins") (v "0.23.1") (d (list (d (n "intuicio-core") (r "^0.23") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "17l81yb9n5176xk3m9hdlsyj7shn4flhld2rzk199i6gjq7lzbpg")))

(define-public crate-intuicio-plugins-0.24.0 (c (n "intuicio-plugins") (v "0.24.0") (d (list (d (n "intuicio-core") (r "^0.24") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0y2g8y4bc4lm2ahn2q6ch5lg467b6pfbqwq4zwqhdr11nhdcyjzy")))

(define-public crate-intuicio-plugins-0.24.1 (c (n "intuicio-plugins") (v "0.24.1") (d (list (d (n "intuicio-core") (r "^0.24") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0vp9pijc0jqdjfx7rrzx3c6xdkkka87svgmr3p82x7wjdqd2yd4p")))

(define-public crate-intuicio-plugins-0.24.2 (c (n "intuicio-plugins") (v "0.24.2") (d (list (d (n "intuicio-core") (r "^0.24") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "13gc4rzfpffka2vwv1c5slw5n3x5d38jw26xlh3qsabfks1fy2q9")))

(define-public crate-intuicio-plugins-0.25.0 (c (n "intuicio-plugins") (v "0.25.0") (d (list (d (n "intuicio-core") (r "^0.25") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1ml3k8bgdli0nnmmjr0y4ns85xpf1kb2pdfb90asq9hk907swqkz")))

(define-public crate-intuicio-plugins-0.26.0 (c (n "intuicio-plugins") (v "0.26.0") (d (list (d (n "intuicio-core") (r "^0.26") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1qgg8zggsamw7glmfp77g9c6k0l9ynb5183ia43g1yjgrqh93287")))

(define-public crate-intuicio-plugins-0.27.0 (c (n "intuicio-plugins") (v "0.27.0") (d (list (d (n "intuicio-core") (r "^0.27") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "15x0ws07r975ipcldmpd64jr82zkkvvnvxz687xk4khms2pccp4x")))

(define-public crate-intuicio-plugins-0.27.1 (c (n "intuicio-plugins") (v "0.27.1") (d (list (d (n "intuicio-core") (r "^0.27") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "07fnb8z50h1hakf38931blw3f2cw15amd2nnn29zadlil9f2n98g")))

(define-public crate-intuicio-plugins-0.27.2 (c (n "intuicio-plugins") (v "0.27.2") (d (list (d (n "intuicio-core") (r "^0.27") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0lprn3r76035jb5a7saqshzqpfrh6m4fcmp0pck667s58g9p8qba")))

(define-public crate-intuicio-plugins-0.28.0 (c (n "intuicio-plugins") (v "0.28.0") (d (list (d (n "intuicio-core") (r "^0.28") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0z5d3k5bsryvd0qxjbcb4jbhkpwl8qmpwaicgmyb6jbcgq8d9zph")))

(define-public crate-intuicio-plugins-0.28.1 (c (n "intuicio-plugins") (v "0.28.1") (d (list (d (n "intuicio-core") (r "^0.28") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "01yffin2ijwzdwcw45r5dcck4d6gmk02xn3iix9lhdi5sq4nvgrc")))

(define-public crate-intuicio-plugins-0.28.2 (c (n "intuicio-plugins") (v "0.28.2") (d (list (d (n "intuicio-core") (r "^0.28") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0rdbxymmiwh1v3qiwz2jspmh20yaglsrv33bw71619hix2w7345m")))

(define-public crate-intuicio-plugins-0.28.3 (c (n "intuicio-plugins") (v "0.28.3") (d (list (d (n "intuicio-core") (r "^0.28") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1mgyqgj209qy4yw12k6qf5pjcp2ydx95qm68w9cn7j6d3g5m2f1p")))

(define-public crate-intuicio-plugins-0.29.0 (c (n "intuicio-plugins") (v "0.29.0") (d (list (d (n "intuicio-core") (r "^0.29") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "00f0ap852zh1cchzqs11h330kwyd3k7lx5gr8wdmfajwmq59nfl9")))

(define-public crate-intuicio-plugins-0.29.1 (c (n "intuicio-plugins") (v "0.29.1") (d (list (d (n "intuicio-core") (r "^0.29") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1mwl5q95m9k2gg7zflcybyycxjzkkddwd7l1lbkx1jdzn07w8y9m")))

(define-public crate-intuicio-plugins-0.30.0 (c (n "intuicio-plugins") (v "0.30.0") (d (list (d (n "intuicio-core") (r "^0.30") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0pb61m5z5xharznmdnaypqgc3ml3vi517lvwir0zr6sycxag02vy")))

(define-public crate-intuicio-plugins-0.31.0 (c (n "intuicio-plugins") (v "0.31.0") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0ri2k9vmays5098ynw5l1lm9hz348jav9d8lad4d79a5h9vmw9mk")))

(define-public crate-intuicio-plugins-0.31.1 (c (n "intuicio-plugins") (v "0.31.1") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1h2z4j00sl0c27x3jkzzrl5il3m1qabzngxjbh28nqyjxj7qwf17")))

(define-public crate-intuicio-plugins-0.31.2 (c (n "intuicio-plugins") (v "0.31.2") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "0qh4wbjxbir2c4pxd9p0i9qjifmlda6w4i32dgkgn3h9bqcvrfi6")))

(define-public crate-intuicio-plugins-0.31.3 (c (n "intuicio-plugins") (v "0.31.3") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "06k9ymf1znfiyhn7i6dkrbmd7n31pigpxy0bgp33a47rs6j79ljl")))

(define-public crate-intuicio-plugins-0.31.4 (c (n "intuicio-plugins") (v "0.31.4") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "11dhka69ibqxf4l9h5k5x6qy002qah41k6b0rnjs4nsm3hacvld5")))

(define-public crate-intuicio-plugins-0.31.5 (c (n "intuicio-plugins") (v "0.31.5") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1dp1v9ghdavdf65qq7w1l78zk274y70ia704cak4zva35yw4v9vj")))

(define-public crate-intuicio-plugins-0.31.6 (c (n "intuicio-plugins") (v "0.31.6") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 0)))) (h "1va49sxqm3ybbaabkkd896j8xmr2nhdwv1g52m15vx3ngj7z1vgp")))

