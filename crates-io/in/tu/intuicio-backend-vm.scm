(define-module (crates-io in tu intuicio-backend-vm) #:use-module (crates-io))

(define-public crate-intuicio-backend-vm-0.1.0 (c (n "intuicio-backend-vm") (v "0.1.0") (d (list (d (n "intuicio-core") (r "^0.1") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "04jl24fh8yccvkbfbmya71929fj3zh0vgp1b9vkii3ain97j73hl")))

(define-public crate-intuicio-backend-vm-0.2.0 (c (n "intuicio-backend-vm") (v "0.2.0") (d (list (d (n "intuicio-core") (r "^0.2") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1ami1pacg2xwc3gqm8gcahm87sj0sq3mnlwip3f06z67pd5i287p")))

(define-public crate-intuicio-backend-vm-0.3.0 (c (n "intuicio-backend-vm") (v "0.3.0") (d (list (d (n "intuicio-core") (r "^0.3") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1fvh16djlqlakr9lbr1g77cvbvsszrhyda4cwp1mg961g21ghiwa")))

(define-public crate-intuicio-backend-vm-0.3.1 (c (n "intuicio-backend-vm") (v "0.3.1") (d (list (d (n "intuicio-core") (r "^0.3") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0xgw34529wismkph7dy5ji1ymamkjc2h0k3fhbq6228a3g2jnp1i")))

(define-public crate-intuicio-backend-vm-0.3.2 (c (n "intuicio-backend-vm") (v "0.3.2") (d (list (d (n "intuicio-core") (r "^0.3") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1r3q4qr0wq6abcryn090vhsammfp8h9jp5psb51gd48mi94j4vms")))

(define-public crate-intuicio-backend-vm-0.3.3 (c (n "intuicio-backend-vm") (v "0.3.3") (d (list (d (n "intuicio-core") (r "^0.3") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1nkf75lga9g8h4b9h25vqmc2nvq1dd9hjv2brbz2kwpvlnyz0fab")))

(define-public crate-intuicio-backend-vm-0.3.4 (c (n "intuicio-backend-vm") (v "0.3.4") (d (list (d (n "intuicio-core") (r "^0.3") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1h2isk4x17bww0k8vxby4yqg4b33qq99ihvxf5na7mizpzxy92fs")))

(define-public crate-intuicio-backend-vm-0.3.5 (c (n "intuicio-backend-vm") (v "0.3.5") (d (list (d (n "intuicio-core") (r "^0.3") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0lhbzqqnpk6802b7gn6hb3sjffd814cwnyr8bp0j3cflls88lg9r")))

(define-public crate-intuicio-backend-vm-0.3.6 (c (n "intuicio-backend-vm") (v "0.3.6") (d (list (d (n "intuicio-core") (r "^0.3") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "17zihdvrw91p4baxmx6ayhvslvwafi9a3hdhbiii6hkbbw54i2i2")))

(define-public crate-intuicio-backend-vm-0.3.7 (c (n "intuicio-backend-vm") (v "0.3.7") (d (list (d (n "intuicio-core") (r "^0.3") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1rfav9ls1wbwja478j3r6rz7fc4rc8zkn95kd2609w9fv5g33hw6")))

(define-public crate-intuicio-backend-vm-0.4.0 (c (n "intuicio-backend-vm") (v "0.4.0") (d (list (d (n "intuicio-core") (r "^0.4") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1mjlql3mwyir5lyrr5i85bbil1fzan22kd6shkzzwr763b7fd3md")))

(define-public crate-intuicio-backend-vm-0.5.0 (c (n "intuicio-backend-vm") (v "0.5.0") (d (list (d (n "intuicio-core") (r "^0.5") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0n5pr6qxsqay1ksrb293n3cfg958kby4q2r2gpgaj4z1z3x6ah98")))

(define-public crate-intuicio-backend-vm-0.5.1 (c (n "intuicio-backend-vm") (v "0.5.1") (d (list (d (n "intuicio-core") (r "^0.5") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "058hgvf7znjg3fryllavf7mxc8xavhjb6777wzgdl3i5vwaya7d9")))

(define-public crate-intuicio-backend-vm-0.5.2 (c (n "intuicio-backend-vm") (v "0.5.2") (d (list (d (n "intuicio-core") (r "^0.5") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "064kzx9mki7yqiv0m2gkcnbpl4z27j61jfp2d837ycq2s6bszqkb")))

(define-public crate-intuicio-backend-vm-0.6.0 (c (n "intuicio-backend-vm") (v "0.6.0") (d (list (d (n "intuicio-core") (r "^0.6") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1101gpdiprz0f4sz5qdf71nr568zkcfqjws2yqyx0ygsca2ycw3i")))

(define-public crate-intuicio-backend-vm-0.6.1 (c (n "intuicio-backend-vm") (v "0.6.1") (d (list (d (n "intuicio-core") (r "^0.6") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0bx2z3mw9fiwjnbjwq5a8r61ivgyvm81mcariaskkcdd6wgif33b")))

(define-public crate-intuicio-backend-vm-0.7.0 (c (n "intuicio-backend-vm") (v "0.7.0") (d (list (d (n "intuicio-core") (r "^0.7") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "017608xsyvv2piyxw67k8k89ivfqsk0dv38qa6h9llsy6jcbsvmw")))

(define-public crate-intuicio-backend-vm-0.8.0 (c (n "intuicio-backend-vm") (v "0.8.0") (d (list (d (n "intuicio-core") (r "^0.8") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "012vyn5jqm3fwpkjrimrc16h1y1g9p4c6fh7vbg20pr1j16v0jjl")))

(define-public crate-intuicio-backend-vm-0.8.1 (c (n "intuicio-backend-vm") (v "0.8.1") (d (list (d (n "intuicio-core") (r "^0.8") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0s8w5b1ll0qzyxhvb3njxg33k2p8h3v4fp0y7v7rcf8p1jxrqy0b")))

(define-public crate-intuicio-backend-vm-0.9.0 (c (n "intuicio-backend-vm") (v "0.9.0") (d (list (d (n "intuicio-core") (r "^0.9") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1d7nlan9hm3lkgqwvi7ij12wr2syg9k4kx3mqz96kzqap7rvqlmj")))

(define-public crate-intuicio-backend-vm-0.10.0 (c (n "intuicio-backend-vm") (v "0.10.0") (d (list (d (n "intuicio-core") (r "^0.10") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0xndqa5dvvhq54kwgxd7rwjpqzxppfx5k66zj82wg7w786746mkc")))

(define-public crate-intuicio-backend-vm-0.10.1 (c (n "intuicio-backend-vm") (v "0.10.1") (d (list (d (n "intuicio-core") (r "^0.10") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0la1q476jfncfsl1g5pd0m9j8fbv6gpsycmpg1p5fnlv90fk0nhw")))

(define-public crate-intuicio-backend-vm-0.11.0 (c (n "intuicio-backend-vm") (v "0.11.0") (d (list (d (n "intuicio-core") (r "^0.11") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0d3s0l8d0n1gsswr1l13h7m4wnnhrlg5p9wyks6ynr33gdj373mk")))

(define-public crate-intuicio-backend-vm-0.11.1 (c (n "intuicio-backend-vm") (v "0.11.1") (d (list (d (n "intuicio-core") (r "^0.11") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0f0kcd089i5qpqpkwy7219mbk4z4ir4abkzrg7kzzg61b6xi4cal")))

(define-public crate-intuicio-backend-vm-0.11.2 (c (n "intuicio-backend-vm") (v "0.11.2") (d (list (d (n "intuicio-core") (r "^0.11") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1pq1vw0qqwfx1dsspk6r0fy0p29sn4rd1pwsf0h4cq4g0xv4pl9n")))

(define-public crate-intuicio-backend-vm-0.12.0 (c (n "intuicio-backend-vm") (v "0.12.0") (d (list (d (n "intuicio-core") (r "^0.12") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1b3hzlr0xzyf9ncm5sh1hdm0g7kr1i2jyzia43aj83b1y47w0vp9")))

(define-public crate-intuicio-backend-vm-0.12.1 (c (n "intuicio-backend-vm") (v "0.12.1") (d (list (d (n "intuicio-core") (r "^0.12") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0jabiy5zx92dj4szykpsn51nk5c1g6vlw16fzrqnkcwl1h3f18qm")))

(define-public crate-intuicio-backend-vm-0.12.2 (c (n "intuicio-backend-vm") (v "0.12.2") (d (list (d (n "intuicio-core") (r "^0.12") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0qqnzii4hl9ddqp9h408dsl3r66pd46f95hyfg241y86gxndlhs7")))

(define-public crate-intuicio-backend-vm-0.12.3 (c (n "intuicio-backend-vm") (v "0.12.3") (d (list (d (n "intuicio-core") (r "^0.12") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0sq7lk5k367xi4ag7gqfkrpyv1jjxw26qr0vp06bcilv5lywp4mc")))

(define-public crate-intuicio-backend-vm-0.13.0 (c (n "intuicio-backend-vm") (v "0.13.0") (d (list (d (n "intuicio-core") (r "^0.13") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "18y8d26kpirjyw18pll13wzwnka3cwx5xnirhs1022365gvp4v79")))

(define-public crate-intuicio-backend-vm-0.13.1 (c (n "intuicio-backend-vm") (v "0.13.1") (d (list (d (n "intuicio-core") (r "^0.13") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0xc6186qffl72pjvy3cpi4l1vqlw3cpvs03zw53d4pbbgxv8xvc3")))

(define-public crate-intuicio-backend-vm-0.14.0 (c (n "intuicio-backend-vm") (v "0.14.0") (d (list (d (n "intuicio-core") (r "^0.14") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0zi6gvr92avmnbm4p80q491y8zxiibq9dkqfdigbl0vkd7yp6q94")))

(define-public crate-intuicio-backend-vm-0.15.0 (c (n "intuicio-backend-vm") (v "0.15.0") (d (list (d (n "intuicio-core") (r "^0.15") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "12kpz8hxy9mg91pa24yrn40jvxnl5g5kwkzx8wj2jw75b51ncxiw")))

(define-public crate-intuicio-backend-vm-0.16.0 (c (n "intuicio-backend-vm") (v "0.16.0") (d (list (d (n "intuicio-core") (r "^0.16") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.16") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1xcxahdgcizdykpzmm2ww2j4aqxhhxblgj09abmvdhl7l1g98rzf")))

(define-public crate-intuicio-backend-vm-0.17.0 (c (n "intuicio-backend-vm") (v "0.17.0") (d (list (d (n "intuicio-core") (r "^0.17") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1nfyqhf9snki154ssy6w7mqs4kc8gla16l6kv6c6gqshw338za1c")))

(define-public crate-intuicio-backend-vm-0.17.1 (c (n "intuicio-backend-vm") (v "0.17.1") (d (list (d (n "intuicio-core") (r "^0.17") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0v07w9mc1m721vybwkr3kkzmy32kv95dhgkyb8ds25a18m7r3rkj")))

(define-public crate-intuicio-backend-vm-0.17.2 (c (n "intuicio-backend-vm") (v "0.17.2") (d (list (d (n "intuicio-core") (r "^0.17") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1rgqhpjy7kja4g018ddbryaqwn30kq2pg3ihi15kgga1k8iw29i1")))

(define-public crate-intuicio-backend-vm-0.17.3 (c (n "intuicio-backend-vm") (v "0.17.3") (d (list (d (n "intuicio-core") (r "^0.17") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "062ky8in83vj8a4s7vpbry55h04fgirdzxrjyq8wilhzj7fbzcmw")))

(define-public crate-intuicio-backend-vm-0.18.0 (c (n "intuicio-backend-vm") (v "0.18.0") (d (list (d (n "intuicio-core") (r "^0.18") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "14y782l24rspkz8wh1chfmjydx559zw03spfgi2nr8p1dclcw5nc")))

(define-public crate-intuicio-backend-vm-0.18.1 (c (n "intuicio-backend-vm") (v "0.18.1") (d (list (d (n "intuicio-core") (r "^0.18") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "158an3j0qb999m7fhgr4mqjyanrszkb2126nl9s1xiqb5gwpy2si")))

(define-public crate-intuicio-backend-vm-0.18.2 (c (n "intuicio-backend-vm") (v "0.18.2") (d (list (d (n "intuicio-core") (r "^0.18") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1d0fjlas7vzpplq5avm64gmkczs7jyllgj2dsxpm1w95cna05s6n")))

(define-public crate-intuicio-backend-vm-0.18.3 (c (n "intuicio-backend-vm") (v "0.18.3") (d (list (d (n "intuicio-core") (r "^0.18") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0i1yz53bcp87309f5wr2f5aqhh0ridjlqqzas0h1zcgj39n3wwza")))

(define-public crate-intuicio-backend-vm-0.19.0 (c (n "intuicio-backend-vm") (v "0.19.0") (d (list (d (n "intuicio-core") (r "^0.19") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1d1yb9l4prsif0sva6s4irhz68v7mr4dh33xz61jxqbdn593qh8f")))

(define-public crate-intuicio-backend-vm-0.19.1 (c (n "intuicio-backend-vm") (v "0.19.1") (d (list (d (n "intuicio-core") (r "^0.19") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0mank01ykgdd9xdgrz73ma47fmmdc7mils88dk3z8nvkl6gjdxgm")))

(define-public crate-intuicio-backend-vm-0.20.0 (c (n "intuicio-backend-vm") (v "0.20.0") (d (list (d (n "intuicio-core") (r "^0.20") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1k65jk65np56g07s3nrs5wmyjn0ljrhf4dwyw8vc1mxw19nxl94j")))

(define-public crate-intuicio-backend-vm-0.20.1 (c (n "intuicio-backend-vm") (v "0.20.1") (d (list (d (n "intuicio-core") (r "^0.20") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1h6p3n8slqmgg7g4j8yv1mnpyq42qpaxvjjiwn3aj6f0ni2w43kb")))

(define-public crate-intuicio-backend-vm-0.20.2 (c (n "intuicio-backend-vm") (v "0.20.2") (d (list (d (n "intuicio-core") (r "^0.20") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1c1afqhy06a65bid5mb6g27a3yb5f6s9q2hfakm30zmmn53kgcnx")))

(define-public crate-intuicio-backend-vm-0.20.3 (c (n "intuicio-backend-vm") (v "0.20.3") (d (list (d (n "intuicio-core") (r "^0.20") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1k45ndsbi1aadjyxhldgzfrqildj16wxq2zm0d6d62aa7kryf77d")))

(define-public crate-intuicio-backend-vm-0.20.4 (c (n "intuicio-backend-vm") (v "0.20.4") (d (list (d (n "intuicio-core") (r "^0.20") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "04dnvpla41746w7w7763d1fr29p4phcynmycwfdwxj6a237smf41")))

(define-public crate-intuicio-backend-vm-0.20.5 (c (n "intuicio-backend-vm") (v "0.20.5") (d (list (d (n "intuicio-core") (r "^0.20") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1ljndmkxxh4806hprr1hl20pa8hnfrf4zv2464b2lnhybjwgmqji")))

(define-public crate-intuicio-backend-vm-0.21.0 (c (n "intuicio-backend-vm") (v "0.21.0") (d (list (d (n "intuicio-core") (r "^0.21") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "06c8zbmmsvgkxf7x3hizk8mvni13pv3ivbw0zak78q5qdkbyx3vv")))

(define-public crate-intuicio-backend-vm-0.22.0 (c (n "intuicio-backend-vm") (v "0.22.0") (d (list (d (n "intuicio-core") (r "^0.22") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0i85h0ln21hdq3vayznfnba02lmzx9pj2izzyib887fl6151kjv4")))

(define-public crate-intuicio-backend-vm-0.22.1 (c (n "intuicio-backend-vm") (v "0.22.1") (d (list (d (n "intuicio-core") (r "^0.22") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.22") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1saipkm6kwam0i9w6mqv1y7766853lq6jf5n3ii65sxkxmgsk5ns")))

(define-public crate-intuicio-backend-vm-0.23.0 (c (n "intuicio-backend-vm") (v "0.23.0") (d (list (d (n "intuicio-core") (r "^0.23") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0a9h0q8swkv9shz7wmjsdyhpj2w48hrd399va1xjfq2x0qc7ix6x")))

(define-public crate-intuicio-backend-vm-0.23.1 (c (n "intuicio-backend-vm") (v "0.23.1") (d (list (d (n "intuicio-core") (r "^0.23") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.23") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1yylffh9316mqz14wy9iz6sa2ggy7xfif11zqprw9d3djgwbra8c")))

(define-public crate-intuicio-backend-vm-0.24.0 (c (n "intuicio-backend-vm") (v "0.24.0") (d (list (d (n "intuicio-core") (r "^0.24") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0p7g0hbkw006llharifgdx8k8pcblppi60lppl68z76y7s849ckd")))

(define-public crate-intuicio-backend-vm-0.24.1 (c (n "intuicio-backend-vm") (v "0.24.1") (d (list (d (n "intuicio-core") (r "^0.24") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1w1xf99qdijqf4zpy8b9yabn5l669389gnfn4gy7bg5igbj365w5")))

(define-public crate-intuicio-backend-vm-0.24.2 (c (n "intuicio-backend-vm") (v "0.24.2") (d (list (d (n "intuicio-core") (r "^0.24") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0wzlyc6n7i17bhj7vljcvpd3p2m33vy8h6sbdr0sllqa13hx12sq")))

(define-public crate-intuicio-backend-vm-0.25.0 (c (n "intuicio-backend-vm") (v "0.25.0") (d (list (d (n "intuicio-core") (r "^0.25") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.25") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0j9gqzdd6rgphkwmdmdg8fz5z0jw0w3wgb93caj58yddilsczzln")))

(define-public crate-intuicio-backend-vm-0.26.0 (c (n "intuicio-backend-vm") (v "0.26.0") (d (list (d (n "intuicio-core") (r "^0.26") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.26") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1v7k2nh2qshrac5nm8r9x76f2a36rfd4vdck9kfp9hgvazswbcnf")))

(define-public crate-intuicio-backend-vm-0.27.0 (c (n "intuicio-backend-vm") (v "0.27.0") (d (list (d (n "intuicio-core") (r "^0.27") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1rkxiigksjzsf82sy89mvmh9lgdxhxw254qk4xvxn2mh9nqrlypi")))

(define-public crate-intuicio-backend-vm-0.27.1 (c (n "intuicio-backend-vm") (v "0.27.1") (d (list (d (n "intuicio-core") (r "^0.27") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1azyw7hvh0facgs1f36wbqz8dhqdrlcr82rf30x85jb9969f5amr")))

(define-public crate-intuicio-backend-vm-0.27.2 (c (n "intuicio-backend-vm") (v "0.27.2") (d (list (d (n "intuicio-core") (r "^0.27") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.27") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0fg285g5ysypypxa1g1mhfc86xzwnn48hppc36cyjp8q7a6a53aa")))

(define-public crate-intuicio-backend-vm-0.28.0 (c (n "intuicio-backend-vm") (v "0.28.0") (d (list (d (n "intuicio-core") (r "^0.28") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0j7m20yjwnmqx9blsb511n2nzmwz04df5k0rb74pmj63hcxwsy4q")))

(define-public crate-intuicio-backend-vm-0.28.1 (c (n "intuicio-backend-vm") (v "0.28.1") (d (list (d (n "intuicio-core") (r "^0.28") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "18jc5l9i5rm7ggmcwp509kaaqhd9sdalzza7ylxakki9474dfpzk")))

(define-public crate-intuicio-backend-vm-0.28.2 (c (n "intuicio-backend-vm") (v "0.28.2") (d (list (d (n "intuicio-core") (r "^0.28") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0p0cfilz0aqv4q2bkxsa348dgkqqiswlxm3id4w434q4yabwfsd5")))

(define-public crate-intuicio-backend-vm-0.28.3 (c (n "intuicio-backend-vm") (v "0.28.3") (d (list (d (n "intuicio-core") (r "^0.28") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.28") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "15cfx040w5v9ws6srfws2x9ycvwsqv3z851lv8l2cmr1qd47j9qb")))

(define-public crate-intuicio-backend-vm-0.29.0 (c (n "intuicio-backend-vm") (v "0.29.0") (d (list (d (n "intuicio-core") (r "^0.29") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.29") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1vwvj7r3prm5pxajav1pkxj7v0xjgk4paps73i281l0axvmsl8n7")))

(define-public crate-intuicio-backend-vm-0.29.1 (c (n "intuicio-backend-vm") (v "0.29.1") (d (list (d (n "intuicio-core") (r "^0.29") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.29") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1hwghrlbkrvj3ggazi64zjlr3543cnzqwmwi81l024xhqg3jwb3k")))

(define-public crate-intuicio-backend-vm-0.30.0 (c (n "intuicio-backend-vm") (v "0.30.0") (d (list (d (n "intuicio-core") (r "^0.30") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.30") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1w10acdv9yx70kncrrcpz095g5h4vpzv96fl7mxhqs50qzfic10d")))

(define-public crate-intuicio-backend-vm-0.31.0 (c (n "intuicio-backend-vm") (v "0.31.0") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.31") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0bj5wigrxignggm4kziamijjgwirs5kiyn1h1apy5wzm8kmwibc2")))

(define-public crate-intuicio-backend-vm-0.31.1 (c (n "intuicio-backend-vm") (v "0.31.1") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.31") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "04fm2xbgxi2y4z7r0pcl405bh90j1byyw3hzgh7qcjknpah2v3am")))

(define-public crate-intuicio-backend-vm-0.31.2 (c (n "intuicio-backend-vm") (v "0.31.2") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.31") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1bwxd22zxsgwx86vxn9i6d17n2f8j6lk50vzwnx9rwxayagllzyj")))

(define-public crate-intuicio-backend-vm-0.31.3 (c (n "intuicio-backend-vm") (v "0.31.3") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.31") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "14ixgbr34m81p6kvvf289kz68lybhi8jy1ryy5p9l54jik2wjcx0")))

(define-public crate-intuicio-backend-vm-0.31.4 (c (n "intuicio-backend-vm") (v "0.31.4") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.31") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "12ms6132fk61nqji7cflfxg2sdl0wkyhqp4mdw7hvxhaz81a1yf8")))

(define-public crate-intuicio-backend-vm-0.31.5 (c (n "intuicio-backend-vm") (v "0.31.5") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.31") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1qwi8rapffbnl3c9nv2hma8h9hragc3il18l3pmc8iz6f8s078fp")))

(define-public crate-intuicio-backend-vm-0.31.6 (c (n "intuicio-backend-vm") (v "0.31.6") (d (list (d (n "intuicio-core") (r "^0.31") (d #t) (k 0)) (d (n "intuicio-data") (r "^0.31") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0j21gn9bb4phs47684h9m15gxwpfwijmhaw2gbpfgsci8p011x3v")))

