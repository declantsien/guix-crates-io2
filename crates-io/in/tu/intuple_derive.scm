(define-module (crates-io in tu intuple_derive) #:use-module (crates-io))

(define-public crate-intuple_derive-0.1.0 (c (n "intuple_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1aszmnk04q1zl92ikjqyw7n7ygj5lis03216r24r20lyvscbrxrb")))

(define-public crate-intuple_derive-0.2.0 (c (n "intuple_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0l539bxg7yfnjjmzrmag6y5qna9wy7r3qn59wqn76cldss9gp4yz")))

