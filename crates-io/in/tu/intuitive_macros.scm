(define-module (crates-io in tu intuitive_macros) #:use-module (crates-io))

(define-public crate-intuitive_macros-0.1.0 (c (n "intuitive_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jrwjf3rq73c2h1knkryc1casi22fz18qyaavw8g1ldcr6y7m72d")))

(define-public crate-intuitive_macros-0.2.0 (c (n "intuitive_macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "101wa2g6ib2dl325fmc69s6kp5l0k7an0kc4jiaxz2i8pzbg6y3l")))

(define-public crate-intuitive_macros-0.2.2 (c (n "intuitive_macros") (v "0.2.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qw1cgpb6mcxjihwiayf9pcv9s5n7vwvl4azag2l7hb7z1h6b8bv")))

(define-public crate-intuitive_macros-0.3.0 (c (n "intuitive_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1imkiiifv7m7ai5snic7qsbrwzlwh7r8kvp4i17zij5mzikbl6wj")))

(define-public crate-intuitive_macros-0.3.1 (c (n "intuitive_macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1b0vwrw9xbwdbr1x5hsba3yvrnnpff1k125fclwi97k6skvmj63q")))

(define-public crate-intuitive_macros-0.4.0 (c (n "intuitive_macros") (v "0.4.0") (d (list (d (n "proc-macro-crate") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vwb8wmqxcrs23mcnfah1jkacw7h03l0swlgpp7abvzqic0ncp2y")))

(define-public crate-intuitive_macros-0.5.0 (c (n "intuitive_macros") (v "0.5.0") (d (list (d (n "proc-macro-crate") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yikmvnq8ki1m8q0wqjpvq7sl3xrij5bqpfj7rz48ygn7xnm49nq")))

(define-public crate-intuitive_macros-0.6.0 (c (n "intuitive_macros") (v "0.6.0") (d (list (d (n "proc-macro-crate") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fgjk4hmyv9xmlvd07iwr6gaal978brh9zhy76shg8zki4yzfb5s")))

(define-public crate-intuitive_macros-0.6.2 (c (n "intuitive_macros") (v "0.6.2") (d (list (d (n "proc-macro-crate") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dx1kcspycdxpb7pvzr01k373jvaf93bbp677xxdzxaki48lk3wg")))

(define-public crate-intuitive_macros-0.7.0-alpha.0 (c (n "intuitive_macros") (v "0.7.0-alpha.0") (d (list (d (n "proc-macro-crate") (r "^1.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vzcmczdhcbarhg1yq4py712ixmpqwjn8rksw5w78nykksbgknmi")))

