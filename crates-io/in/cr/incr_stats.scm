(define-module (crates-io in cr incr_stats) #:use-module (crates-io))

(define-public crate-incr_stats-1.0.0 (c (n "incr_stats") (v "1.0.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 0)) (d (n "float_eq") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1xd048dvhv5yfpmvqalq158dhn0swnnjy78m152y81xqc3drcjiy")))

(define-public crate-incr_stats-1.0.1 (c (n "incr_stats") (v "1.0.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 0)) (d (n "float_eq") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1jgxv86v7bhhif27s7qggvlpb17v8y3fqy7n046c4pb4lcw7684y")))

(define-public crate-incr_stats-1.0.2 (c (n "incr_stats") (v "1.0.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "float_eq") (r "^1.0.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.12") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1i4kcbj96bwci754vqg423imsa3ym3kcqrs48cgdzakx3pdj2lfp")))

