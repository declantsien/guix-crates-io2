(define-module (crates-io in cr incremental-writer) #:use-module (crates-io))

(define-public crate-incremental-writer-0.1.0 (c (n "incremental-writer") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0iignjl0c1gmwcxzr639kxirk0w3lfpc4q03fajymllmrx4zrih9")))

(define-public crate-incremental-writer-0.1.1 (c (n "incremental-writer") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0c8m961zwmjlmy28a8dyhq9qa64ffpk460fyxjq1fz93pbdkdn7g")))

(define-public crate-incremental-writer-0.1.2 (c (n "incremental-writer") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nla0jrlc8841np11lbipbvmm1xl3l1hdk0zvvg1zlni21v0m7fz")))

