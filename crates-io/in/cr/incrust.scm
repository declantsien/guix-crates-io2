(define-module (crates-io in cr incrust) #:use-module (crates-io))

(define-public crate-incrust-0.0.1 (c (n "incrust") (v "0.0.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "~1.2.2") (d #t) (k 0)))) (h "1nvgigh1mp0gf59g79fifmdbv6h7gw1gxw6v2pwfxnkhlr9lw13g") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-incrust-0.0.3 (c (n "incrust") (v "0.0.3") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "~1.2.2") (d #t) (k 0)))) (h "17mbs95mscb9v1knzbz7xp05fbpfn2rdp2pd85llwwvb11dpkl0l") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-incrust-0.0.4 (c (n "incrust") (v "0.0.4") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "~1.2.2") (d #t) (k 0)))) (h "050ig0qwnw5ph027xnp6gdcypzvvxpq9y4v1ngx4xzc0j5l2yify") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-incrust-0.0.5 (c (n "incrust") (v "0.0.5") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "~1.2.2") (d #t) (k 0)))) (h "1ahbs87q8q3kwkzk4x8yk3a8v013jg7am4z5b5dmvg34vaxwdgk7") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-incrust-0.0.6 (c (n "incrust") (v "0.0.6") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "~1.2.2") (d #t) (k 0)))) (h "0pyxlwxvy7h4arpiwd89ilrjqkgbcxk3y6adc3ym5vys4k3qjayb") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-incrust-0.0.7 (c (n "incrust") (v "0.0.7") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "~1.2.2") (d #t) (k 0)))) (h "0gddc4ljbayjsabfdxb20kh333siczz0w1aysrs2qwrqdg0jy0sc") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-incrust-0.0.8 (c (n "incrust") (v "0.0.8") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^1.2") (d #t) (k 0)))) (h "0sq504xr8cdv88n11khqs3fk8wk1mpf5byd3n8vxi3vp3apkp1vw") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-incrust-0.0.11 (c (n "incrust") (v "0.0.11") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^1.2") (d #t) (k 0)))) (h "1rq3rdnqlknscsryq0ab6frjf49pjpk70xs8dvxs2yjacmhrmqgn") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-incrust-0.0.13 (c (n "incrust") (v "0.0.13") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^1.2") (d #t) (k 0)))) (h "1f3bz2q180vhvndqcx3gd0xkcfmcs04701j6hafr0nh60vz7gggj") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-incrust-0.0.14 (c (n "incrust") (v "0.0.14") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^1.2") (d #t) (k 0)))) (h "0b66219z9wzc01xgds0fhjzdcipza7rr15m7hcqpvfhjrlhgqgxa") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-incrust-0.0.15 (c (n "incrust") (v "0.0.15") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^1.2") (d #t) (k 0)))) (h "1ji79lha9hjlhyqwjc2fc1gyb8gy3i88h9q7q7885wql4671kxhy") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-incrust-0.1.0 (c (n "incrust") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^1.2") (d #t) (k 0)))) (h "1xblk50cgnrnqrzh1kch8qyccibpnk26j7amd43bni245wa4fb5h") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-incrust-0.1.1 (c (n "incrust") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^1.2") (d #t) (k 0)))) (h "0fi6w1zll4mwll3gja68gp0k7j30hsykv9ll2zwgs6h22976hnqi") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-incrust-0.1.2 (c (n "incrust") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^1.2") (d #t) (k 0)))) (h "10afvd76amvwpfgs2mb1fs4rmwp90q8y350rqdkl9rrzlffyzzis") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-incrust-0.1.4 (c (n "incrust") (v "0.1.4") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^1.2") (d #t) (k 0)))) (h "0yjj7qwydx9k3a22pc4w3lm26r1zk7a96y3y1qzg491gk7jspr85") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-incrust-0.2.2 (c (n "incrust") (v "0.2.2") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "1acix9b81chbs23vqd5235qm2f7j53s6c0r1w485sickhv2c8pq1") (f (quote (("verbose" "clippy") ("quiet") ("default"))))))

(define-public crate-incrust-0.2.3 (c (n "incrust") (v "0.2.3") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "0i8hhgigmlxwkjicfplw9wv40bl7dmqkyf5l96n40nlk1qrlm04z") (f (quote (("verbose" "clippy") ("quiet") ("default"))))))

(define-public crate-incrust-0.2.4 (c (n "incrust") (v "0.2.4") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "0v1xcah47fs9dr28qc56q4zqbmy4rvnb9k6zb1hiqpghmwg355lk") (f (quote (("verbose" "clippy") ("quiet") ("default"))))))

(define-public crate-incrust-0.2.5 (c (n "incrust") (v "0.2.5") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "0jkhx582g76nlgm2pzmym9aqgh3rmfy6s606g0220sc5iij636s9") (f (quote (("verbose" "clippy") ("quiet") ("default"))))))

(define-public crate-incrust-0.2.7 (c (n "incrust") (v "0.2.7") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "091bq7yxfkxmdndlr1km441fg23r477gas5g7jpmlpdikwpqipfj") (f (quote (("verbose" "clippy") ("quiet") ("default"))))))

(define-public crate-incrust-0.2.8 (c (n "incrust") (v "0.2.8") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "12bglwynb64pfj51mwr0d9xk580v9n1cjn99cil56h7phy434cjd") (f (quote (("verbose" "clippy") ("quiet") ("default"))))))

(define-public crate-incrust-0.2.9 (c (n "incrust") (v "0.2.9") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "03v49wc99nh6pshs1rppdk29h10mygydapaiw9v97pb7zckxn414") (f (quote (("verbose" "clippy") ("quiet") ("default"))))))

(define-public crate-incrust-0.2.10 (c (n "incrust") (v "0.2.10") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "1aksrvx1flg37354qqyi1dn08mw6w2r5k8chmngwd96ra737n5n1") (f (quote (("verbose" "clippy") ("quiet") ("default"))))))

(define-public crate-incrust-0.2.11 (c (n "incrust") (v "0.2.11") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "0z81ay0zwfqdcsg1vgd58c8j8y0dawdsg71nk2mrymyqd07fkp3i") (f (quote (("verbose" "clippy") ("quiet") ("default"))))))

(define-public crate-incrust-0.2.12 (c (n "incrust") (v "0.2.12") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "1d7js0wc3734i6lamkyyh5sc6srjgybbdlb6ljs7rmklcvygma7x") (f (quote (("verbose" "clippy") ("quiet") ("default"))))))

(define-public crate-incrust-0.2.15 (c (n "incrust") (v "0.2.15") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)) (d (n "marksman_escape") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "0j9mxh2wxqhj5z7yzrx6bv5ly395n7771461b9ik0wkm7y828wnw") (f (quote (("verbose") ("quiet") ("default"))))))

