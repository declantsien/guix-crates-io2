(define-module (crates-io in cr increment) #:use-module (crates-io))

(define-public crate-increment-0.1.0 (c (n "increment") (v "0.1.0") (h "0c231vsz9n8k7kvgl5cd7wln5w4wpx7158v3jkl4jy47iyvafmwq") (y #t)))

(define-public crate-increment-0.1.1 (c (n "increment") (v "0.1.1") (h "10rcrar7nfkcxbi16p7l8yncwajzqd2im51i27x0khl3m53s21zs")))

(define-public crate-increment-0.2.0 (c (n "increment") (v "0.2.0") (h "1xxrs7qv4r28frir93f5s89l0fj96lbvcl9sz4vlqgvqlw4ym1kd")))

(define-public crate-increment-0.3.0 (c (n "increment") (v "0.3.0") (h "0z9q2yzp0j3qfrssrk4184b3y56bq8srlbyd37fjlrcm1nhj43jb")))

