(define-module (crates-io in cr incrementalmerkletree) #:use-module (crates-io))

(define-public crate-incrementalmerkletree-0.1.0 (c (n "incrementalmerkletree") (v "0.1.0") (d (list (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "00mgplcipyl240bri84whdlnb5dl0xbfzab5lv7chk4q8armzmqn")))

(define-public crate-incrementalmerkletree-0.2.0 (c (n "incrementalmerkletree") (v "0.2.0") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "15jy5dxx3dnhmqy3g0c2p8qisd3vkbg11d409dfqdb5fjamx6vqq")))

(define-public crate-incrementalmerkletree-0.3.0-beta.1 (c (n "incrementalmerkletree") (v "0.3.0-beta.1") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pmllrbfl9jbgx28wfcgd9apqswxbw6ip074xddc1bw95hlbxmna")))

(define-public crate-incrementalmerkletree-0.3.0-beta.2 (c (n "incrementalmerkletree") (v "0.3.0-beta.2") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "049krl5szq4cv5dddsi263f0zn190l89lbj08aw98xkal37z44jq")))

(define-public crate-incrementalmerkletree-0.3.0 (c (n "incrementalmerkletree") (v "0.3.0") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "05ijr02x1m8g90cv89f7wy6x511dsaq1xksmcr9mav8067fmp306")))

(define-public crate-incrementalmerkletree-0.3.1 (c (n "incrementalmerkletree") (v "0.3.1") (d (list (d (n "proptest") (r "~1.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yz19khn8hwmrmq5xdvr5klpd932ry4nazspkm2lanbrynil7bfm")))

(define-public crate-incrementalmerkletree-0.4.0 (c (n "incrementalmerkletree") (v "0.4.0") (d (list (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1w22wg5lgwp3kha3nayglv0y30vv294mr986j1v7kz0vr601gf9f") (f (quote (("test-dependencies" "proptest") ("legacy-api")))) (r "1.60")))

(define-public crate-incrementalmerkletree-0.5.0 (c (n "incrementalmerkletree") (v "0.5.0") (d (list (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "08s66pz3flil527n1m64rjfl2f88h042cjxyhkrd9nfl4iw4c71n") (f (quote (("test-dependencies" "proptest") ("legacy-api")))) (r "1.60")))

(define-public crate-incrementalmerkletree-0.5.1 (c (n "incrementalmerkletree") (v "0.5.1") (d (list (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "rand_core") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 2)))) (h "1glwahxqhsd8lxnhjq4vgds2dv7khsgdwg8miimv09dp1y0p467b") (f (quote (("legacy-api")))) (s 2) (e (quote (("test-dependencies" "dep:proptest" "dep:rand" "dep:rand_core")))) (r "1.60")))

