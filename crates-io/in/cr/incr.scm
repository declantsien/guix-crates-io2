(define-module (crates-io in cr incr) #:use-module (crates-io))

(define-public crate-incr-0.1.0 (c (n "incr") (v "0.1.0") (h "0cndv18f1c6kdgm6chylknw9a83pf2a45iqxmzaymmkqdzgzg690") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-incr-0.1.1 (c (n "incr") (v "0.1.1") (h "0qk0r3274lwmv9m1hhsyxvx113sgpglc06nzz0il5xfimf5nl9km") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-incr-0.2.0-rc.1 (c (n "incr") (v "0.2.0-rc.1") (d (list (d (n "fnv") (r "^1") (o #t) (d #t) (k 0)))) (h "1m75x605cd57lbgand343jf896163njanng2a00x1ryfl8k2k9rk") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-incr-0.2.0 (c (n "incr") (v "0.2.0") (d (list (d (n "fnv") (r "^1") (o #t) (d #t) (k 0)))) (h "0wrvqag2ps5x2hvsa7jdh1fmdy88ahjxzr5hjmxqfr1cm5z6a70c") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-incr-0.2.1 (c (n "incr") (v "0.2.1") (d (list (d (n "fnv") (r "^1") (o #t) (d #t) (k 0)))) (h "0wi9jp2w31ji4myqqi0ij5v53aadm5636n8i8fgrhlb94pimq79x") (f (quote (("nightly") ("default" "nightly"))))))

