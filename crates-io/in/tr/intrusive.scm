(define-module (crates-io in tr intrusive) #:use-module (crates-io))

(define-public crate-intrusive-0.0.1 (c (n "intrusive") (v "0.0.1") (h "08lhd46b7kysi31j94mn4q20ix5gf6n1k2b6byq700958kl4djcm")))

(define-public crate-intrusive-0.0.2 (c (n "intrusive") (v "0.0.2") (h "0h07k5iprifpbxyy8g6mpkcipiyfzr0c3j4gy4jmcj6p1cvf2slq")))

(define-public crate-intrusive-0.0.3 (c (n "intrusive") (v "0.0.3") (d (list (d (n "stainless") (r "*") (d #t) (k 2)))) (h "03lxs08g2hjixzb2srcj4bj4l6vi1433fl8man12xxv6qb8xwwhj")))

(define-public crate-intrusive-0.0.4 (c (n "intrusive") (v "0.0.4") (d (list (d (n "stainless") (r "*") (d #t) (k 2)))) (h "1a8xscbag622vm1v77jzrrpfqsy01c912nq6vf6vlp0a27crazay")))

