(define-module (crates-io in tr intrigue) #:use-module (crates-io))

(define-public crate-intrigue-0.0.1 (c (n "intrigue") (v "0.0.1") (h "0y6yvksja1grjgajrs1d95bqp50fpbcn99fs1vwaz3kdfaq69axc")))

(define-public crate-intrigue-0.0.2 (c (n "intrigue") (v "0.0.2") (h "0lmrrrkhq7fz1l8d4qnz74a0z0yz3w6kp1af65pahkjjhiv4qjdb")))

