(define-module (crates-io in tr intricate-macros) #:use-module (crates-io))

(define-public crate-intricate-macros-0.3.0 (c (n "intricate-macros") (v "0.3.0") (d (list (d (n "opencl3") (r "^0.8.1") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "savefile") (r "^0.10") (d #t) (k 2)) (d (n "savefile-derive") (r "^0.10") (d #t) (k 2)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "02vjnp954ksk6x9jycgfv707k2314pqll9ixxqfq83wsrlvcz173")))

(define-public crate-intricate-macros-0.3.1 (c (n "intricate-macros") (v "0.3.1") (d (list (d (n "opencl3") (r "^0.8.1") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "savefile") (r "^0.10") (d #t) (k 2)) (d (n "savefile-derive") (r "^0.10") (d #t) (k 2)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1gzg1cyjg0k18f2jpfhlmyzdjq6bq6sq26v8pwcadl4fcxa2lr2x")))

(define-public crate-intricate-macros-0.3.2 (c (n "intricate-macros") (v "0.3.2") (d (list (d (n "opencl3") (r "^0.8.1") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "savefile") (r "^0.10") (d #t) (k 2)) (d (n "savefile-derive") (r "^0.10") (d #t) (k 2)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0mrsl7zyifjryakyri2xzbnqgma38swjihmaizl1dnry838b5w21")))

(define-public crate-intricate-macros-0.3.3 (c (n "intricate-macros") (v "0.3.3") (d (list (d (n "opencl3") (r "^0.8.1") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "savefile") (r "^0.10") (d #t) (k 2)) (d (n "savefile-derive") (r "^0.10") (d #t) (k 2)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "166m1ja0grxzdqras3yfjw7r22q2xsqh55x0fzrf1ngklhklfjv2")))

(define-public crate-intricate-macros-0.3.4 (c (n "intricate-macros") (v "0.3.4") (d (list (d (n "opencl3") (r "^0.8.1") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "savefile") (r "^0.10") (d #t) (k 2)) (d (n "savefile-derive") (r "^0.10") (d #t) (k 2)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "09yja9wnz1w4nv0a46ajgsv3vb9wzbiil8ll4nqb2ajfjg9jn3lk")))

(define-public crate-intricate-macros-0.3.5 (c (n "intricate-macros") (v "0.3.5") (d (list (d (n "opencl3") (r "^0.8.1") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "savefile") (r "^0.10") (d #t) (k 2)) (d (n "savefile-derive") (r "^0.10") (d #t) (k 2)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0gmjgacpfk9zmlwi5d7jiv9snk0xa13d4dqa1x5f1argd5ybgy21")))

(define-public crate-intricate-macros-0.3.6 (c (n "intricate-macros") (v "0.3.6") (d (list (d (n "opencl3") (r "^0.8.1") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "savefile") (r "^0.10") (d #t) (k 2)) (d (n "savefile-derive") (r "^0.10") (d #t) (k 2)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "07crzi14hlf7qcm4yjz0ymyb7llx3kvs2nr878fk5dybhdhh49bn")))

(define-public crate-intricate-macros-0.3.7 (c (n "intricate-macros") (v "0.3.7") (d (list (d (n "opencl3") (r "^0.8.1") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "savefile") (r "^0.10") (d #t) (k 2)) (d (n "savefile-derive") (r "^0.10") (d #t) (k 2)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0hqr69kbl04dlyjqcf8qbmm3wylzjmwlwvkf90ksbikcis8q417h")))

(define-public crate-intricate-macros-0.3.8 (c (n "intricate-macros") (v "0.3.8") (d (list (d (n "opencl3") (r "^0.8.1") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "savefile") (r "^0.10") (d #t) (k 2)) (d (n "savefile-derive") (r "^0.10") (d #t) (k 2)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1sdigr2cbrnx7ygh8x11lzp3y2hwqbzpnc2yp06d4dl58h7n0i4b")))

(define-public crate-intricate-macros-0.3.9 (c (n "intricate-macros") (v "0.3.9") (d (list (d (n "opencl3") (r "^0.8.1") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "savefile") (r "^0.10") (d #t) (k 2)) (d (n "savefile-derive") (r "^0.10") (d #t) (k 2)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1pya44pwq77i7m95lqisxx7m44h3q0c0ih67ikispa1vw0mvjckb")))

(define-public crate-intricate-macros-0.3.10 (c (n "intricate-macros") (v "0.3.10") (d (list (d (n "opencl3") (r "^0.8.1") (d #t) (k 2)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "savefile") (r "^0.10") (d #t) (k 2)) (d (n "savefile-derive") (r "^0.10") (d #t) (k 2)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0ry00cly2c19h22hznxpyhi6apji19zsgs3r6yvj1g1ca8v0hc03")))

(define-public crate-intricate-macros-0.4.0 (c (n "intricate-macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "0g6phmr2my73krja996q3q9v0f6a8500cwm64bjcbz8z8mglbd5g")))

(define-public crate-intricate-macros-0.6.0 (c (n "intricate-macros") (v "0.6.0") (d (list (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "0nhsplfnjrqb3wjmh0wq0lgqwyh9zcxz2dvgmcm3adfi93xaylmc")))

(define-public crate-intricate-macros-0.7.0 (c (n "intricate-macros") (v "0.7.0") (d (list (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "044i39cv454xqxpqx8zrnzbwsw4r5zqy4fgsm50j07c2w8a364vb")))

