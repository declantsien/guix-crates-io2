(define-module (crates-io in tr intrusive-collections) #:use-module (crates-io))

(define-public crate-intrusive-collections-0.1.0 (c (n "intrusive-collections") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0g8pbc8q7wvhp61x7rki06wrc9nnbjdnhljh1zm3wwg1awh94cd5") (f (quote (("nightly"))))))

(define-public crate-intrusive-collections-0.2.0 (c (n "intrusive-collections") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1kphkfphxkvm8y9zcvgwbgycwbqvb7cgjqgnvp4d9ap1j49dv37b") (f (quote (("nightly") ("default" "box") ("box"))))))

(define-public crate-intrusive-collections-0.3.0 (c (n "intrusive-collections") (v "0.3.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1l5azqbhrv6rqlhm8vbv8xvwvwnb7wc3w645hbf0nv15zpr42icf") (f (quote (("nightly") ("default" "box") ("box"))))))

(define-public crate-intrusive-collections-0.4.0 (c (n "intrusive-collections") (v "0.4.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0gzp7r6hlk12ysrdfi1w2viqdpfnm2q9as64k59f421a81706vr7") (f (quote (("nightly") ("default" "box") ("box"))))))

(define-public crate-intrusive-collections-0.4.1 (c (n "intrusive-collections") (v "0.4.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "11iy74c69pjg80yla9qjdb7ivhvdy051c0dxqz9hvxczdxvaib7j") (f (quote (("nightly") ("default" "box") ("box"))))))

(define-public crate-intrusive-collections-0.5.0 (c (n "intrusive-collections") (v "0.5.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2") (d #t) (k 2)))) (h "0kkdapw4rbddzr85lx5cyb4bjamz5m396np6j07m8n5qm23ai7w8") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.6.0 (c (n "intrusive-collections") (v "0.6.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2") (d #t) (k 2)))) (h "1d4xbamjlzdvl6y6q22pclbwl6ip72nh5082v8s71g1fw9j6lihl") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.6.1 (c (n "intrusive-collections") (v "0.6.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2") (d #t) (k 2)))) (h "0dqqvma89nw16vgd5q35lfqqpzi2ki7f3kgsmnfgrx7pr51ba9b1") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.6.2 (c (n "intrusive-collections") (v "0.6.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2") (d #t) (k 2)))) (h "1mykrap183ja48b0bnaqymz6r2wr05cmqk1ybqncjhhmjr34aglk") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.6.3 (c (n "intrusive-collections") (v "0.6.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2") (d #t) (k 2)))) (h "0afck1c1grfsk90yryyr5d30g9sipn89bgsvq1sljgly51ijqqdc") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.6.4 (c (n "intrusive-collections") (v "0.6.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2") (d #t) (k 2)))) (h "15aqm75zs17w6vv01fg9xva1kwlcc59g225aijngcanl8z41jrfz") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.6.5 (c (n "intrusive-collections") (v "0.6.5") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2") (d #t) (k 2)))) (h "0pgxyaxs9snakrv5v33lmnzvs6y6rky0f028wzsq9r8f4r2jm6jm") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.6.6 (c (n "intrusive-collections") (v "0.6.6") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2") (d #t) (k 2)))) (h "1pa2kzcp7ybhgfz2fz6gpk4swri007n2g5vfmwfn0bi5hirzfr6g") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.6.7 (c (n "intrusive-collections") (v "0.6.7") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2") (d #t) (k 2)))) (h "182il08y3vi5dsk6bqls62l7gxwal70c7m0jwvqqnyw8yrnd2fl3") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.6.8 (c (n "intrusive-collections") (v "0.6.8") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2") (d #t) (k 2)))) (h "0czm4jbl92039xsgkxqiaja740vy70yk7gkvvg1q255yjgiqg4qq") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.7.0 (c (n "intrusive-collections") (v "0.7.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2") (d #t) (k 2)))) (h "0a92zzq21pjdjbgd8p6bny804bvlwq0sbzajgvh9hlbh6n9wm9yc") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.7.1 (c (n "intrusive-collections") (v "0.7.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2") (d #t) (k 2)))) (h "0xkqzgy17nw5h93f4si4x2lcss3f9fa21276lqgz3aad47lh01s2") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.7.2 (c (n "intrusive-collections") (v "0.7.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2") (d #t) (k 2)))) (h "065jvra6ybbp4n3cxlfc01pn5mf7z16d0xjz1ld4ccmv6n8xizma") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.7.3 (c (n "intrusive-collections") (v "0.7.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2") (d #t) (k 2)))) (h "0dphw2mpi6jxnhmk59qg75xksz3ivx6l368jpwqw4ng46k29pdx9") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.7.4 (c (n "intrusive-collections") (v "0.7.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2") (d #t) (k 2)))) (h "05qsdpkj9gs04jvdz8hc4xgwnpc52wr0a57ql0zlc39p04fcfwfg") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.7.5 (c (n "intrusive-collections") (v "0.7.5") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2") (d #t) (k 2)))) (h "1zjs8c2y38dr73s6sj6q1843qlfd3iwpw58940jwl7mzkf3ssdap") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.7.6 (c (n "intrusive-collections") (v "0.7.6") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2") (d #t) (k 2)))) (h "0l0xn0s29qwmavcw5q3a1lsv2g045x6halwchbdm8791hsl7lihi") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.7.7 (c (n "intrusive-collections") (v "0.7.7") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2") (d #t) (k 2)))) (h "1r3c9ay3b9r30jd2axihsld6ss6gnqjfvknqzkrimgdiphba206a") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.7.8 (c (n "intrusive-collections") (v "0.7.8") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2") (d #t) (k 2)))) (h "0zhpznpv4ri94xbs3hwj1krnbkf4667sj0s1kmb3vcfh4cypq87h") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.8.0 (c (n "intrusive-collections") (v "0.8.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2") (d #t) (k 2)))) (h "03zpj331ffjfmwrh79n0q72d08w4sbgahz1yyspbbaa6y63sh2q5") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.8.1 (c (n "intrusive-collections") (v "0.8.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2") (d #t) (k 2)))) (h "0qhjdxh19087pd53sz9r6d10gl5ais52pizxrr5imphg8ymjygsy") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.8.2 (c (n "intrusive-collections") (v "0.8.2") (d (list (d (n "memoffset") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2") (d #t) (k 2)))) (h "04fd6jlpjzyn0kp4c3qm0rgl212waz4sl8blvlbnr3m7mir6w4sz") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.8.3 (c (n "intrusive-collections") (v "0.8.3") (d (list (d (n "memoffset") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2") (d #t) (k 2)))) (h "1i58ifvikj2cp2h90dp9gvg7v3d3l9hmdd6sv3bika0ma7qg8gqn") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.8.4 (c (n "intrusive-collections") (v "0.8.4") (d (list (d (n "memoffset") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2") (d #t) (k 2)))) (h "1j4yfk6lx394xb4jjya7jkz6v3ka757h9921kk7rfh6slvqhbmsj") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.8.5 (c (n "intrusive-collections") (v "0.8.5") (d (list (d (n "memoffset") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typed-arena") (r "^1.2") (d #t) (k 2)))) (h "16hbxlz3zr6spn3bjs7rn3mmcphag8yyi6ji09ynngfmb9c3n5fh") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.9.0 (c (n "intrusive-collections") (v "0.9.0") (d (list (d (n "memoffset") (r "^0.5.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 2)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 2)))) (h "1ny37qhp5mm8a5b6cpdnxx3yp9jwf3imi6m7vpa61k9ip05qrjjb") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.9.1 (c (n "intrusive-collections") (v "0.9.1") (d (list (d (n "memoffset") (r "^0.5.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 2)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 2)))) (h "1jiv28vkpwxjv7nbriyhk876zi6c57q0342vlgdnmig5lmr0079w") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.9.2 (c (n "intrusive-collections") (v "0.9.2") (d (list (d (n "memoffset") (r "^0.5.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 2)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 2)))) (h "1k8icl06iwf7b9mqz7pkn3xmh3hs6f8782ccw7b6n76gnijd2kpb") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.9.3 (c (n "intrusive-collections") (v "0.9.3") (d (list (d (n "memoffset") (r "^0.5.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 2)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 2)))) (h "03f196sccw0mvl9nxhxh9mz6mysvdg89qkpq3dbpzbgbliq4nj7v") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.9.4 (c (n "intrusive-collections") (v "0.9.4") (d (list (d (n "memoffset") (r "^0.5.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 2)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 2)))) (h "12l80x569di2nsvn0krmly5l2m2zv4n3ykvxw7rj0wcxg2kk3rdz") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.9.5 (c (n "intrusive-collections") (v "0.9.5") (d (list (d (n "memoffset") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 2)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 2)))) (h "0057n66qasv9b43wjz43iy44zghc4cnhi3hgzgpzv08jn2pr0krz") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

(define-public crate-intrusive-collections-0.9.6 (c (n "intrusive-collections") (v "0.9.6") (d (list (d (n "memoffset") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3.0") (d #t) (k 2)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 2)))) (h "0bhj0vvmr591860ykx1mnjm3gwc0nw9ysakdc9saigf3f2gxr55n") (f (quote (("nightly") ("default" "alloc") ("alloc"))))))

