(define-module (crates-io in tr introspect-core) #:use-module (crates-io))

(define-public crate-introspect-core-0.1.0 (c (n "introspect-core") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1094bnqm6dbd6iyi5cv7b35d557a07ci081nhn6l4na1j6blxfj1")))

(define-public crate-introspect-core-0.1.1 (c (n "introspect-core") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "04n45h8mj42y8l4mb1cx992f6bq0z38z07jib0y059a1wfymxmqn")))

