(define-module (crates-io in tr introspection) #:use-module (crates-io))

(define-public crate-introspection-0.1.0 (c (n "introspection") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1zs4cc8rjsvwmig1snscp6mkp3qcyq1bhzjysc3r2akhiv69dvwa") (f (quote (("serde_support" "serde" "serde_derive" "serde_json") ("default"))))))

