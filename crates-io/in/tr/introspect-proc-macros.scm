(define-module (crates-io in tr introspect-proc-macros) #:use-module (crates-io))

(define-public crate-introspect-proc-macros-0.1.0 (c (n "introspect-proc-macros") (v "0.1.0") (d (list (d (n "introspect-core") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "06w81k0lgmiw2mh0nk1m9gf7kxxyccwq4rjy4g3dyl215nfdahrv")))

