(define-module (crates-io in tr intrusive_splay_tree) #:use-module (crates-io))

(define-public crate-intrusive_splay_tree-0.1.0 (c (n "intrusive_splay_tree") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.6.0") (d #t) (k 2)) (d (n "typed-arena") (r "^1.3.0") (d #t) (k 2)) (d (n "unreachable") (r "^1.0.0") (d #t) (k 0)))) (h "0fnsw4n9hgw7m6d5p21jifsxpm0ambf03k0hqq42h6qhx5a7yyag")))

(define-public crate-intrusive_splay_tree-0.2.0 (c (n "intrusive_splay_tree") (v "0.2.0") (d (list (d (n "bumpalo") (r "^3.4.0") (d #t) (k 2)) (d (n "memoffset") (r "^0.5.5") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.0") (d #t) (k 2)) (d (n "unreachable") (r "^1.0.0") (d #t) (k 0)))) (h "0x3xw3zdnh51dq9wq1qyv3aa5lcla11mxikxwqmxzl70q9w99fgs")))

