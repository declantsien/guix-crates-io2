(define-module (crates-io in tr introspectable) #:use-module (crates-io))

(define-public crate-introspectable-0.1.0 (c (n "introspectable") (v "0.1.0") (h "1gknankl04ji7mcmc2d7wjmqqna4hsk47kxjpmdjpb4qf31g0xfp") (f (quote (("specialized_std") ("default" "specialized_std"))))))

(define-public crate-introspectable-0.2.0 (c (n "introspectable") (v "0.2.0") (h "197barihvia2gvpsf4q5nasqm83c1lhbrsprfk0q4yilfrjv8ksp") (f (quote (("specialized_std") ("default" "specialized_std"))))))

(define-public crate-introspectable-0.3.0 (c (n "introspectable") (v "0.3.0") (h "0rw318cy33897h5fk334zb6w4006qardswrnqp8wvsgd0x502xc1") (f (quote (("specialized_std") ("default" "specialized_std"))))))

(define-public crate-introspectable-0.3.1 (c (n "introspectable") (v "0.3.1") (h "01ijzhc972ag5cmvw3c1r989xajr3b6dy9wpyimb6ncymz84qpdq") (f (quote (("specialized_std") ("default" "specialized_std"))))))

(define-public crate-introspectable-0.4.0 (c (n "introspectable") (v "0.4.0") (h "01p9gvs5iz3sb42x5m70jv2zlrpwi33y7wprqii06dsyazc3mmr9") (f (quote (("specialized_std") ("default" "specialized_std"))))))

(define-public crate-introspectable-0.5.0 (c (n "introspectable") (v "0.5.0") (d (list (d (n "introspectable_derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0zhx3hcsp32p3gzzpgfp2kcqvba15rxp94mrhkjw02b8nn44v691") (f (quote (("specialized_std") ("derive" "introspectable_derive") ("default" "specialized_std" "derive"))))))

