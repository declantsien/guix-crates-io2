(define-module (crates-io in tr introsort) #:use-module (crates-io))

(define-public crate-introsort-0.3.0 (c (n "introsort") (v "0.3.0") (d (list (d (n "rand") (r "^0.3.4") (d #t) (k 2)))) (h "0yxm6z00w86pjgzgcxdzyvr1fdv3k44c9j1i5xmy12lyq6p92i1r")))

(define-public crate-introsort-0.4.0 (c (n "introsort") (v "0.4.0") (d (list (d (n "rand") (r "^0.3.7") (d #t) (k 2)))) (h "0rzyr6zpyngswzj1ij2fbwdhfsskhlxzkvnbzfr1yxngqxb46pdv") (f (quote (("float") ("default" "float"))))))

(define-public crate-introsort-0.4.1 (c (n "introsort") (v "0.4.1") (d (list (d (n "rand") (r "^0.3.7") (d #t) (k 2)))) (h "1f1wv6sndn04pc5asvhspi4r02gl2h81lvni7dzqhzsrsfz15wch") (f (quote (("float") ("default" "float"))))))

(define-public crate-introsort-0.5.0 (c (n "introsort") (v "0.5.0") (d (list (d (n "num") (r "*") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3.7") (d #t) (k 2)))) (h "1z2x3vz13nv0f9x8biwg35q7ia7dpiwrcxirjlkb75xjbmamm9xr") (f (quote (("float" "num") ("default" "float"))))))

(define-public crate-introsort-0.5.1 (c (n "introsort") (v "0.5.1") (d (list (d (n "num") (r "^0.1.25") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3.8") (d #t) (k 2)))) (h "1hj66qzwb49pb588yzl2qlj7brjkiddwlvgz70qfnb57lip0b3mk") (f (quote (("float" "num") ("default" "float"))))))

(define-public crate-introsort-0.5.2 (c (n "introsort") (v "0.5.2") (d (list (d (n "num") (r "^0.1.25") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3.8") (d #t) (k 2)) (d (n "unreachable") (r "^0.0.2") (o #t) (d #t) (k 0)))) (h "10zxl3wzin527cghk62sj3nk1lm6j10mqf7fr9nj2alkc1mpb8f5") (f (quote (("float" "num" "unreachable") ("default" "float"))))))

(define-public crate-introsort-0.5.3 (c (n "introsort") (v "0.5.3") (d (list (d (n "num") (r "^0.1.25") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3.11") (d #t) (k 2)) (d (n "unreachable") (r "^0.0.2") (o #t) (d #t) (k 0)))) (h "0jq4qmxjz2aslngmhhxvmia489130mha95a79syvks8frd9lw02z") (f (quote (("float" "num" "unreachable") ("default" "float"))))))

(define-public crate-introsort-0.6.0 (c (n "introsort") (v "0.6.0") (d (list (d (n "num") (r "^0.1.25") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3.11") (d #t) (k 2)) (d (n "unreachable") (r "^0.0.2") (o #t) (d #t) (k 0)))) (h "00hw2dcsl56mh0s1jxk2kv36p8h8g53f256bfl85svzkig25wygy") (f (quote (("float" "num" "unreachable") ("default" "float"))))))

