(define-module (crates-io in tr intrepion_x_fizz_buzz) #:use-module (crates-io))

(define-public crate-intrepion_x_fizz_buzz-0.1.0 (c (n "intrepion_x_fizz_buzz") (v "0.1.0") (h "1dknx7ay4jbdpmk6wc7rd2h17ig58cfll88fz4xas6fv6hxwwagl") (y #t)))

(define-public crate-intrepion_x_fizz_buzz-0.1.1 (c (n "intrepion_x_fizz_buzz") (v "0.1.1") (h "0gvpr5x4hh221gij6y7b22mfxbh6fyn1s8f3n0974slilhqaa2ii") (y #t)))

