(define-module (crates-io in tr introspecter) #:use-module (crates-io))

(define-public crate-introspecter-0.1.0 (c (n "introspecter") (v "0.1.0") (h "1kyqd45qiphbnn19dxals60j8r2rmwif71qv5p08m0wsg7a6pwzf")))

(define-public crate-introspecter-0.0.0 (c (n "introspecter") (v "0.0.0") (h "0i3j8xkdh9l9zb99kq2ds8vsvdj3k56ghl27i05qa9pl6cnf6fm3")))

