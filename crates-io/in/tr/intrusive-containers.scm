(define-module (crates-io in tr intrusive-containers) #:use-module (crates-io))

(define-public crate-intrusive-containers-0.0.1 (c (n "intrusive-containers") (v "0.0.1") (h "0qrg4rvph2y10q9kr18k8430injyj6cpqbh2dfz2qmvlhira2g1w")))

(define-public crate-intrusive-containers-0.0.2 (c (n "intrusive-containers") (v "0.0.2") (d (list (d (n "rand") (r "^0.1") (d #t) (k 0)))) (h "058syabmlc0paz9ayyiwzm49mfzin5q1kld1mlqiv35frk8jz3lw")))

(define-public crate-intrusive-containers-0.0.3 (c (n "intrusive-containers") (v "0.0.3") (d (list (d (n "rand") (r "^0.2") (d #t) (k 0)))) (h "08mbbh9sf5fln1y9ydgdfny9scknwxancirdfysmp92p9g2z9xzi")))

(define-public crate-intrusive-containers-0.0.4 (c (n "intrusive-containers") (v "0.0.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1ik4p53dlj8i9lgnh8950gx26r6xz93wahj0v11bhd61m19kskbw") (f (quote (("nostd"))))))

(define-public crate-intrusive-containers-0.1.0 (c (n "intrusive-containers") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "03v6p1xhfsixg9z018a4h3mjp5gsqvkfypw8hmh45ls9filmym5q") (f (quote (("nostd"))))))

(define-public crate-intrusive-containers-0.1.1 (c (n "intrusive-containers") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1lsd7c5wgb5kfhiiy21sq4m728ajk4scznhpjdqyp8nhkbnn2d78") (f (quote (("nostd"))))))

(define-public crate-intrusive-containers-0.2.0 (c (n "intrusive-containers") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0jzqpgfg8bzll37n6cf0zf7xzb2njpk4qvy4mwf72qmbx609wvwi") (f (quote (("nostd"))))))

(define-public crate-intrusive-containers-0.2.1 (c (n "intrusive-containers") (v "0.2.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "18yb6lkzl33rk5qraj4hbw9n270xqsd2jzprl4zyacpnjl303c4k") (f (quote (("nostd"))))))

(define-public crate-intrusive-containers-0.2.2 (c (n "intrusive-containers") (v "0.2.2") (d (list (d (n "rand") (r "^0.3.8") (d #t) (k 2)))) (h "0l03yzjr16087f833ry521c0qdd9dp69kl695y860hvmk241iw86") (f (quote (("nostd"))))))

(define-public crate-intrusive-containers-0.2.3 (c (n "intrusive-containers") (v "0.2.3") (d (list (d (n "rand") (r "^0.3.8") (d #t) (k 2)))) (h "03lf4gsg3a7f9zgqlavbz6fym4iaqvf6iwvh63dnffl6zal0mkls") (f (quote (("nostd"))))))

(define-public crate-intrusive-containers-0.2.4 (c (n "intrusive-containers") (v "0.2.4") (d (list (d (n "rand") (r "^0.3.8") (d #t) (k 2)))) (h "0z1npbmyvad8ghg9jkf12ib86l2dlyiw91jbyx6nix2n4992qcix") (f (quote (("nostd"))))))

