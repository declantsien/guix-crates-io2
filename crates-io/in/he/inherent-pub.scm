(define-module (crates-io in he inherent-pub) #:use-module (crates-io))

(define-public crate-inherent-pub-0.1.0 (c (n "inherent-pub") (v "0.1.0") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1jngimvsdpl0vjy2y6qvb3p7bb683qaf3zmykx86qy2lskr6vfbv")))

(define-public crate-inherent-pub-0.2.0 (c (n "inherent-pub") (v "0.2.0") (d (list (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1ifh73dv417zfqg65511bn6vn31jzn156iyd1610384kva05pxm7")))

(define-public crate-inherent-pub-0.2.1 (c (n "inherent-pub") (v "0.2.1") (d (list (d (n "async-std") (r "^1.4.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.22") (d #t) (k 2)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ij3kfllv10pk3lmw2q5axr8ja32a3i8jvbmxwwhxls3bmi792md")))

