(define-module (crates-io in he inherit) #:use-module (crates-io))

(define-public crate-inherit-0.0.0 (c (n "inherit") (v "0.0.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.47") (d #t) (k 0)))) (h "10bbws7igbcnncbcpr9lwm0g0m4439lm6lsg4k3csfw974ws6kzn")))

(define-public crate-inherit-0.0.1 (c (n "inherit") (v "0.0.1") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.47") (d #t) (k 0)))) (h "0n79c0qk96xfs8rhk3z8x89hdcq22c7wwadd35rqp3nsz0zpc6hz")))

(define-public crate-inherit-0.0.2 (c (n "inherit") (v "0.0.2") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.47") (d #t) (k 0)))) (h "1592ddf7fnjsxdiyf16h7i24c67w089d5xq8hpfkh9s4sskma7rx")))

(define-public crate-inherit-0.0.3 (c (n "inherit") (v "0.0.3") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.47") (d #t) (k 0)))) (h "1137mdl0vws90nxhbc00lm6m5bcahbxnd23pab7j7sq6sf7ndsjv")))

