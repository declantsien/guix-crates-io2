(define-module (crates-io in he inherent) #:use-module (crates-io))

(define-public crate-inherent-0.1.0 (c (n "inherent") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rustversion") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "19njrmfqf8pccr944ynrhsn5mmf5mk1hyah98c9k7zd11r15rqa0")))

(define-public crate-inherent-0.1.1 (c (n "inherent") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0simjwr6753ca9r4yilc92pljs76vhjsicvl8y5ihj5fqhzgm8z1")))

(define-public crate-inherent-0.1.2 (c (n "inherent") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0qb6rxhc80y0bmdmx9qd902jn3argsi8rki9ysl8wd42yc5jiwdl")))

(define-public crate-inherent-0.1.3 (c (n "inherent") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^0.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0qnfnxqw4z67gv1m6mpyba603zbv1kzcrcnm2bhqfzzkq3vqsg4z")))

(define-public crate-inherent-0.1.4 (c (n "inherent") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "1pyay5bsyi831k5m2rpz1f47p6kdpqkpaay9zraypygdfn243ny2")))

(define-public crate-inherent-0.1.5 (c (n "inherent") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "15g9450zy86fq9nbjfs4rc2k9isv7if7qyi7fhrab4r209d358gk")))

(define-public crate-inherent-0.1.6 (c (n "inherent") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "14q67czxdybvma4b68r3bj24966c3vcy8wb4d7qbnl105ks4p5zk")))

(define-public crate-inherent-1.0.0 (c (n "inherent") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.75") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "1yxrq8ph4pb8xz60flad03mg4ap34z534nm38g4lzr2k1m0mmh7k")))

(define-public crate-inherent-1.0.1 (c (n "inherent") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.75") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "034245djfvw128zk7nhgz4q0i6q510lzp87hnhbyx45anrf179fs") (r "1.31")))

(define-public crate-inherent-1.0.2 (c (n "inherent") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.75") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "03pa84p5frr8z88rbsqblcj3j0qjlhayl6v2fwg24gspgasp3f9g") (r "1.31")))

(define-public crate-inherent-1.0.3 (c (n "inherent") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.75") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1qfj4siy862ahnc4w02yk4bf7wwxp9w9n7nb492h4vp826634dm0") (r "1.31")))

(define-public crate-inherent-1.0.4 (c (n "inherent") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.75") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0imw0q673ys2gynmlgx53abgmdaa2wqv8gdid32rsv5gqicrsrfb") (r "1.31")))

(define-public crate-inherent-1.0.5 (c (n "inherent") (v "1.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.75") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0lx08ibj8s5s4lfflgbq6i46lyzwyr6by3ngl5pslkhfb83v4dna") (r "1.31")))

(define-public crate-inherent-1.0.6 (c (n "inherent") (v "1.0.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1g6d7pgfdflhlnc2jilay2abl62jq6xkfgz8nwyk3vfh5wvkxh06") (r "1.56")))

(define-public crate-inherent-1.0.7 (c (n "inherent") (v "1.0.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.10") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "00adcbl3i1fb2p9hbgmakilqs4qsxk957b6b4hxsvc843z2q7k10") (r "1.56")))

(define-public crate-inherent-1.0.8 (c (n "inherent") (v "1.0.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0.10") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "01c8kb2qvnv90l9sf1sv3i5bc7ci3x8x3c9pbviphf0l8bgj4f4f") (r "1.56")))

(define-public crate-inherent-1.0.9 (c (n "inherent") (v "1.0.9") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "syn") (r "^2.0.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "1q0cqzkl3ckdsqlna7cy46i9v0b56g4zwqnrfk71iq2gbjwg64dk") (r "1.56")))

(define-public crate-inherent-1.0.10 (c (n "inherent") (v "1.0.10") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "syn") (r "^2.0.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "0s0zgp0ilkh0pgxs9j0v0grlkmk3xhs60fycy4lc1zv2z8dkn96f") (r "1.56")))

(define-public crate-inherent-1.0.11 (c (n "inherent") (v "1.0.11") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "syn") (r "^2.0.46") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "0iyrs8f3i5n1d9wxxixiqwsda926rajm5xs9miilmrhp848vf8h1") (r "1.56")))

