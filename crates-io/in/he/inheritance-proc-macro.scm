(define-module (crates-io in he inheritance-proc-macro) #:use-module (crates-io))

(define-public crate-inheritance-proc-macro-0.0.1-alpha (c (n "inheritance-proc-macro") (v "0.0.1-alpha") (d (list (d (n "fstrings") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "129fkdjzsbil70a2k3c0qg45fsy62db5b4jqwf4izb2b0b8yn5pd") (f (quote (("verbose-expansions" "syn/extra-traits") ("default"))))))

(define-public crate-inheritance-proc-macro-0.0.1-alpha.2 (c (n "inheritance-proc-macro") (v "0.0.1-alpha.2") (d (list (d (n "fstrings") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "13xvz03fcc8d9gwn4zwh2jxff4s9j12cb8h17sivwncmp4sss7m2") (f (quote (("verbose-expansions" "syn/extra-traits") ("specialization") ("default"))))))

