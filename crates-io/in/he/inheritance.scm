(define-module (crates-io in he inheritance) #:use-module (crates-io))

(define-public crate-inheritance-0.0.0 (c (n "inheritance") (v "0.0.0") (h "03bjy9ingl8n749dzcsxp88blb8c96873gs7i5zxpqi6qi00ifj8")))

(define-public crate-inheritance-0.0.1-alpha (c (n "inheritance") (v "0.0.1-alpha") (d (list (d (n "proc_macro") (r "^0.0.1-alpha") (d #t) (k 0) (p "inheritance-proc-macro")))) (h "16wpjvzz82wgvi41qhxdis3f4zrgiw36rdpfijl0vbpr41jv01da") (f (quote (("verbose-expansions" "proc_macro/verbose-expansions") ("default"))))))

(define-public crate-inheritance-0.0.1-alpha.2 (c (n "inheritance") (v "0.0.1-alpha.2") (d (list (d (n "proc_macro") (r "^0.0.1-alpha.2") (d #t) (k 0) (p "inheritance-proc-macro")))) (h "1680lp8ck9pfbvdkn3q42d1x4hmbckv9hir9mqkki44hhkxi7b53") (f (quote (("verbose-expansions" "proc_macro/verbose-expansions") ("specialization" "proc_macro/specialization") ("external_doc") ("default"))))))

