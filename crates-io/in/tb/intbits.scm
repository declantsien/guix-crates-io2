(define-module (crates-io in tb intbits) #:use-module (crates-io))

(define-public crate-intbits-0.1.0 (c (n "intbits") (v "0.1.0") (h "0cr48aal9qbi82rj6p0yg9ajwb1gki8pc241qal9ljb0yxg2s2hq") (y #t)))

(define-public crate-intbits-0.1.1 (c (n "intbits") (v "0.1.1") (h "1zq8d29br68apn8li06k0hs2bzxkk8fzqp8r940hh50p5a1jlaq8") (y #t)))

(define-public crate-intbits-0.1.2 (c (n "intbits") (v "0.1.2") (h "05w77f51qmzhq09in6kj7kxqgfyzc7mrr2qv5r88v2q7siq2m136")))

(define-public crate-intbits-0.2.0 (c (n "intbits") (v "0.2.0") (h "1r9hcgagj6g7hxh9q36whjh5zivv22zcr5cxrfyc2afsxk4c4w2i")))

