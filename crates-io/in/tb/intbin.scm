(define-module (crates-io in tb intbin) #:use-module (crates-io))

(define-public crate-intbin-0.0.5 (c (n "intbin") (v "0.0.5") (h "0pk5zxnr52yfzl11wsr8kdczi6pr077xf9wnx50yqaid2icwvl7p")))

(define-public crate-intbin-0.1.1 (c (n "intbin") (v "0.1.1") (h "19gy8byvd87zfj196ygm1jmshwb3lshm9kwy3p4c8kmrii0k8imp")))

(define-public crate-intbin-0.1.2 (c (n "intbin") (v "0.1.2") (h "1bw094d4f0v64nld0hs7952xnr27p4clj4hnbmkdxf3ln93gqfjk")))

