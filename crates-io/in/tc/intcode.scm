(define-module (crates-io in tc intcode) #:use-module (crates-io))

(define-public crate-intcode-0.1.0 (c (n "intcode") (v "0.1.0") (h "1wk7895b354267rgfcj08cfpd4fxr322j2hjngszpc6k6ynz8m9g")))

(define-public crate-intcode-0.1.1 (c (n "intcode") (v "0.1.1") (h "08ssrgvzm186ajrikm78j86ar2nks8rdx49j0acfs6mkwz7nw1f6")))

(define-public crate-intcode-0.1.2 (c (n "intcode") (v "0.1.2") (h "0z00vb4wlziiijbb51chakywnd0n9mpcr2mg3ywkgc8q0bj5mpp7")))

(define-public crate-intcode-0.2.0 (c (n "intcode") (v "0.2.0") (h "0hlxvil25v8hvnyx1zwbgwkwkd8g5f1gg223ggiw29bc83qdpz48")))

(define-public crate-intcode-0.2.1 (c (n "intcode") (v "0.2.1") (h "0mwhf9f906l6rd5p363mq8xhs0yiryslaj0npj8lf3cfxb6aydhd")))

(define-public crate-intcode-0.3.1 (c (n "intcode") (v "0.3.1") (h "0p25m1769577m3d31sv22ac3w17l6v482m3vpmb2b5whr6lqyz4a")))

(define-public crate-intcode-0.3.2 (c (n "intcode") (v "0.3.2") (h "1dp29nybjwh334qmipajpav7kvnckb8dzsznhr1sdh1049fa13mj")))

