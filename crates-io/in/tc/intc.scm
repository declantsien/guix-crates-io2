(define-module (crates-io in tc intc) #:use-module (crates-io))

(define-public crate-intc-0.1.0 (c (n "intc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "06nrldbbmjfvaflwllvg4m7yd7gzn84jm47nqvfja1pgj0m7j2sd")))

(define-public crate-intc-0.1.1 (c (n "intc") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0k394xvhxqyq05qrmy6nh4l5pgp52b5j8vccbafkwcnxzs71rqa0")))

(define-public crate-intc-0.1.2 (c (n "intc") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "073q5l81hq5wf4b5h186j7rsf9spg3ddd9vrjlk7zmaiva76w03r")))

(define-public crate-intc-0.1.4 (c (n "intc") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0wg0haph364n56axkdia25rd39fccc4xg83g72wpkhcxy14cli5z")))

(define-public crate-intc-0.2.0 (c (n "intc") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "039m24zmv8prlxhs44kbh2ckiampjh8fp2lkrb4mp86hi2z7mhaf")))

(define-public crate-intc-0.2.1 (c (n "intc") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "08bzkbswikk49k3hlzhmwkwygdhrrg96y3ympi2p61p69prsm5fa")))

(define-public crate-intc-0.3.0 (c (n "intc") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "15hwy7934b17v02qvkzz8g3hmvpvfnd9cvjmmbgrvhw3gpl3lsf7")))

(define-public crate-intc-0.3.1 (c (n "intc") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "1c09ym52nhaqgmrkdpb5hhwyrxv2kdla32b9qam26pxpn88aaai3")))

(define-public crate-intc-0.3.2 (c (n "intc") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "109xdrii9j0m10dwijzyw5ii4321pi73pksqrr8wkaikv52lhmlm")))

(define-public crate-intc-0.3.3 (c (n "intc") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "statrs") (r "^0.16.0") (d #t) (k 0)))) (h "0xxijx8pkflxdbnbakzx6n4lk64h9cmxxa2idjma2wlh22qvjdrx")))

