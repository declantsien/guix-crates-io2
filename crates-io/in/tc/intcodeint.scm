(define-module (crates-io in tc intcodeint) #:use-module (crates-io))

(define-public crate-intcodeint-0.1.0 (c (n "intcodeint") (v "0.1.0") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0848w96x0lg9rffw652c5mrv3z8wmac22iligmxp7f41dz5m3bk7")))

(define-public crate-intcodeint-0.1.1 (c (n "intcodeint") (v "0.1.1") (h "0854f9hks6firsqpzs0r6d78ddrn2jif4bzi87jbkhgl4zvnlil5")))

(define-public crate-intcodeint-0.2.0 (c (n "intcodeint") (v "0.2.0") (h "1wzgz3jnn9swa05x4qyvpw5r4gw0abjxj4vgmhxxs3dgmnfd8f63")))

