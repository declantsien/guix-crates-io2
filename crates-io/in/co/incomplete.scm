(define-module (crates-io in co incomplete) #:use-module (crates-io))

(define-public crate-incomplete-0.1.0 (c (n "incomplete") (v "0.1.0") (h "1df8mf474952znwxn6c0a86ylcxz3x7j8cbsi7p1b5j3ag16fzsa")))

(define-public crate-incomplete-0.1.1 (c (n "incomplete") (v "0.1.1") (h "0p604shjk44hs0a15mphq5xrcs2yiy0kglkmc4zina8v8jpdr5a2")))

(define-public crate-incomplete-0.1.2 (c (n "incomplete") (v "0.1.2") (h "09x6db78mi3mbzx1sxzc5j54s9hhqs53c4jmywva81q3dnv93fzx")))

(define-public crate-incomplete-0.1.3 (c (n "incomplete") (v "0.1.3") (h "0cm1p2c05964bn0wb3czgf9w5wkls0qpgv67h90cnbm2vbblqbqi")))

