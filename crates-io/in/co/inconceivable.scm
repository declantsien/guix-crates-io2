(define-module (crates-io in co inconceivable) #:use-module (crates-io))

(define-public crate-inconceivable-0.9.0 (c (n "inconceivable") (v "0.9.0") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0623asg4ykmqr2s2rl5x1x7ng4khzb804gv5b4wiv5j6nk2ff446") (f (quote (("ub_inconceivable") ("std") ("default" "std") ("RUSTC_VERSION_GE_1_27"))))))

