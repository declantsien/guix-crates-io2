(define-module (crates-io in le inle_diagnostics) #:use-module (crates-io))

(define-public crate-inle_diagnostics-0.1.0 (c (n "inle_diagnostics") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "14zhbbdfb912vzzwrlanc136vhp4mhmw5p4r5jkhmbzl0pcxmbsq") (f (quote (("tracer" "rayon") ("default" "tracer"))))))

(define-public crate-inle_diagnostics-0.1.1 (c (n "inle_diagnostics") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "1rnp0y1srm5w79ys1yc4188c8xyydq2invbn44s9x8dca3fvvw0m") (f (quote (("tracer" "rayon") ("default" "tracer"))))))

