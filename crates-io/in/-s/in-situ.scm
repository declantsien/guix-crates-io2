(define-module (crates-io in -s in-situ) #:use-module (crates-io))

(define-public crate-in-situ-0.1.0 (c (n "in-situ") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "1p2mk5249jp4cr6wqmbr0czhdbzv9f78vh6329qdskz0jxv1938p") (f (quote (("i128" "byteorder/i128"))))))

(define-public crate-in-situ-0.2.0 (c (n "in-situ") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "0z72zy4z2d29vym8zcwwbn9ssjblj1magsdxbsqinf2v7897wmyn") (f (quote (("i128" "byteorder/i128"))))))

(define-public crate-in-situ-0.3.0 (c (n "in-situ") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.2") (f (quote ("i128"))) (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "0bsikmd08m3w9ibhm06h6l9frms47bsnzphyidzbvg78bjv1vsm3")))

(define-public crate-in-situ-0.4.0 (c (n "in-situ") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)))) (h "1ixb26y0mh3plyg9kak4cjkkwbvbj0hj2prywyqz2gpy6axd6xj1")))

