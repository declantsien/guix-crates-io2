(define-module (crates-io in kv inkview-sys) #:use-module (crates-io))

(define-public crate-inkview-sys-0.1.0 (c (n "inkview-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 1)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "tinybmp") (r "^0.2.3") (d #t) (k 0)))) (h "0645mdl35b4w7z19anvrjbclkm32p5162ps9fdrl3isr115b2yhx") (f (quote (("sdk_v6") ("sdk_v4")))) (l "inkview")))

(define-public crate-inkview-sys-0.1.1 (c (n "inkview-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 1)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "tinybmp") (r "^0.2.3") (d #t) (k 0)))) (h "05i90xgf0k0q9cx2vgnr44z3z206hbjmvq0mkj4k983z3b8ck4rz") (f (quote (("sdk_v6") ("sdk_v4")))) (l "inkview")))

(define-public crate-inkview-sys-0.1.2 (c (n "inkview-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 1)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "tinybmp") (r "^0.2.3") (d #t) (k 0)))) (h "1kj3ffsjzzcyfxghm9nqm7d60vw6h9m2l4lwzjs6k7crf46iyw79") (f (quote (("sdk_v6") ("sdk_v4")))) (l "inkview")))

