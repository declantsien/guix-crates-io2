(define-module (crates-io in ou inout) #:use-module (crates-io))

(define-public crate-inout-0.0.0 (c (n "inout") (v "0.0.0") (h "1vzyh4g2yflq65cg2pd52l01nl5j99g24fkrxnf02p1hvc52169j")))

(define-public crate-inout-0.1.0 (c (n "inout") (v "0.1.0") (d (list (d (n "block-padding") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "1d58x42280plqbfwxi1mrfs83qq9ihxjjvwc0zhx4n26n076442z") (f (quote (("std" "block-padding/std")))) (r "1.56")))

(define-public crate-inout-0.1.1 (c (n "inout") (v "0.1.1") (d (list (d (n "block-padding") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "12ilbifw7c6sylhxssn25xyriqh3h2yc6vbj3n3gzai8gx6p7n61") (f (quote (("std" "block-padding/std")))) (r "1.56")))

(define-public crate-inout-0.1.2 (c (n "inout") (v "0.1.2") (d (list (d (n "block-padding") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "0hzllb01g5x1ssrd674x4c4gq59abp0qd7r15pncjpadmga067wy") (f (quote (("std" "block-padding/std")))) (r "1.56")))

(define-public crate-inout-0.1.3 (c (n "inout") (v "0.1.3") (d (list (d (n "block-padding") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "1xf9gf09nc7y1a261xlfqsf66yn6mb81ahlzzyyd1934sr9hbhd0") (f (quote (("std" "block-padding/std")))) (r "1.56")))

(define-public crate-inout-0.2.0-pre.1 (c (n "inout") (v "0.2.0-pre.1") (d (list (d (n "block-padding") (r "=0.4.0-pre.1") (o #t) (d #t) (k 0)) (d (n "hybrid-array") (r "=0.2.0-pre.6") (d #t) (k 0)))) (h "1j0fjgjq14adb750fl0wlfla4p1z2dmn2qb3kzlbcd13cah1bm7f") (f (quote (("std" "block-padding/std")))) (r "1.56")))

(define-public crate-inout-0.2.0-pre.2 (c (n "inout") (v "0.2.0-pre.2") (d (list (d (n "block-padding") (r "=0.4.0-pre.2") (o #t) (d #t) (k 0)) (d (n "hybrid-array") (r "=0.2.0-pre.7") (d #t) (k 0)))) (h "1vl692b450hkl7y9nyckk4i7lwx9qb7z5iyklwk7y8llsyzrq115") (f (quote (("std" "block-padding/std")))) (r "1.56")))

(define-public crate-inout-0.2.0-pre.3 (c (n "inout") (v "0.2.0-pre.3") (d (list (d (n "block-padding") (r "=0.4.0-pre.3") (o #t) (d #t) (k 0)) (d (n "hybrid-array") (r "=0.2.0-pre.8") (d #t) (k 0)))) (h "1cxcyfni8i3x4v0qf9xwkv7bzlqnygwh17q3rmvx3s7xw639ksln") (f (quote (("std" "block-padding/std")))) (r "1.56")))

(define-public crate-inout-0.2.0-pre.4 (c (n "inout") (v "0.2.0-pre.4") (d (list (d (n "block-padding") (r "=0.4.0-pre.4") (o #t) (d #t) (k 0)) (d (n "hybrid-array") (r "^0.2.0-rc.0") (d #t) (k 0)))) (h "1znjjihl8i1j4m6yss13zhcwihpz13jn8kl2mx2b7hqcj9dw6b0a") (f (quote (("std" "block-padding/std")))) (r "1.56")))

