(define-module (crates-io in ou inoutflow) #:use-module (crates-io))

(define-public crate-inoutflow-1.0.0 (c (n "inoutflow") (v "1.0.0") (d (list (d (n "clap") (r "^3.1.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "evmscan") (r "^0.6.0") (d #t) (k 0)) (d (n "isahc") (r "^1.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "primitive-types") (r "^0.11.1") (f (quote ("impl-serde" "fp-conversion"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "07d177r08ijdhf5vlpd4anrkl8ggssxgbi2jcjdykxsql7qjwb4n")))

