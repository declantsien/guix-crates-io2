(define-module (crates-io in ou inout_port-rs) #:use-module (crates-io))

(define-public crate-inout_port-rs-0.1.0 (c (n "inout_port-rs") (v "0.1.0") (h "1zvzl0fs6ba8ldp87g5r9ls8ykc1rhfgxpfd1idzx9c03syl91g0") (y #t)))

(define-public crate-inout_port-rs-0.1.1 (c (n "inout_port-rs") (v "0.1.1") (h "1ypf902rr7j3hhhrjzdh73i9rdsk2hhhqlxy1n8lcjr2v05q2k8j")))

