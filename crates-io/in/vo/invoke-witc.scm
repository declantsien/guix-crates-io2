(define-module (crates-io in vo invoke-witc) #:use-module (crates-io))

(define-public crate-invoke-witc-0.1.0 (c (n "invoke-witc") (v "0.1.0") (d (list (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0jcv9kldq6akgsjdwqvkwn35xs9v2gd3fw1r3zpilkk4j4y68q74")))

(define-public crate-invoke-witc-0.2.0 (c (n "invoke-witc") (v "0.2.0") (d (list (d (n "syn") (r "^1") (d #t) (k 0)))) (h "19d19fljwwc1h5rgkfshinp7zgrfwmhpjc9akkrwwmk52s3bv523")))

(define-public crate-invoke-witc-0.2.1 (c (n "invoke-witc") (v "0.2.1") (d (list (d (n "syn") (r "^1") (d #t) (k 0)))) (h "08dc837a7pjcm7xgh251dw0h16sg27zp73nr5n0iqj0xnpi9m46g")))

(define-public crate-invoke-witc-0.3.0 (c (n "invoke-witc") (v "0.3.0") (d (list (d (n "syn") (r "^1") (d #t) (k 0)))) (h "06gagnj5b34qahncgzfln5rhv4015ffn76wgi3vrpmdlhcpw8l1j")))

(define-public crate-invoke-witc-0.3.1 (c (n "invoke-witc") (v "0.3.1") (d (list (d (n "syn") (r "^1") (d #t) (k 0)))) (h "13lqvqm1x29akg5j8472lds244c1sd80xm2dmds05s09d2fn2zw9")))

