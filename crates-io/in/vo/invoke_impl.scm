(define-module (crates-io in vo invoke_impl) #:use-module (crates-io))

(define-public crate-invoke_impl-0.1.0 (c (n "invoke_impl") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gj48xhg6giw3p95fmjhl8ay53rdkm5gxck1s2d5z5nw7g9g1cy2")))

(define-public crate-invoke_impl-0.1.1 (c (n "invoke_impl") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fwx9h2nm8qwi1hrxh6bdbp4k0ymhvd1v0hkh2zwmy85s9fyn4vz")))

