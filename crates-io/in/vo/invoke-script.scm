(define-module (crates-io in vo invoke-script) #:use-module (crates-io))

(define-public crate-invoke-script-0.1.0 (c (n "invoke-script") (v "0.1.0") (d (list (d (n "bind-args") (r "^0.2.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1s9l3sjcq4pvyvm8mby7hj6bn0vw9gr8wdv8cjm4fl120y6c0dsa")))

(define-public crate-invoke-script-0.2.0 (c (n "invoke-script") (v "0.2.0") (d (list (d (n "bind-args") (r "^0.2.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0pg6k9rza5m2dfwfqpw87ffgnys8p0whwcppdp5likcagg5pqilw")))

(define-public crate-invoke-script-0.3.0 (c (n "invoke-script") (v "0.3.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "038nm2rwar8gys25j3gci5cvxqna5csdvlqprkk69rg72hk2z831")))

(define-public crate-invoke-script-0.4.0 (c (n "invoke-script") (v "0.4.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "18p17lk5pa8q2bil2kzwdmj9ivir7fvx6m72apffbiamh9yknafx")))

(define-public crate-invoke-script-0.5.0 (c (n "invoke-script") (v "0.5.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "watercolor") (r "^0.1.0") (d #t) (k 0)))) (h "1igq5hgb53w6nkhqw9b28jsrwpn5qnqha8y7xzmd482ncwy8ppnf")))

(define-public crate-invoke-script-0.6.0 (c (n "invoke-script") (v "0.6.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "watercolor") (r "^0.1.0") (d #t) (k 0)))) (h "18qjnvbjz9l8caapvhwi231316s91wh0ls45agxjxhnr3psn4ga1")))

(define-public crate-invoke-script-0.7.0 (c (n "invoke-script") (v "0.7.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "watercolor") (r "^0.1.0") (d #t) (k 0)))) (h "1bjxgb64imbsixa1nkma88095jjlflrnl335fsfnn7c5mh30y2rv")))

