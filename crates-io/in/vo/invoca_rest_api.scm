(define-module (crates-io in vo invoca_rest_api) #:use-module (crates-io))

(define-public crate-invoca_rest_api-0.1.0 (c (n "invoca_rest_api") (v "0.1.0") (h "1f42pcw522kfy8pa9a2fy6idijz6k7bkqcb2f7yh9gwkanj33hhl")))

(define-public crate-invoca_rest_api-0.1.1 (c (n "invoca_rest_api") (v "0.1.1") (h "1v0c9l3f89gia5cb3vclpaa49c3zww271dl2qn0am6bjry61kx3w")))

