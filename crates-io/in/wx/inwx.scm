(define-module (crates-io in wx inwx) #:use-module (crates-io))

(define-public crate-inwx-0.0.1 (c (n "inwx") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.8.0") (d #t) (k 0)) (d (n "xmlrpc") (r "^0.11.1") (d #t) (k 0)))) (h "070mn7ghgzrf525dp6ba53zqgm90d2zxa4f88pdil2i03zfks06v")))

(define-public crate-inwx-0.0.2 (c (n "inwx") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.8.0") (d #t) (k 0)) (d (n "xmlrpc") (r "^0.11.1") (d #t) (k 0)))) (h "0zhlhjscwa38z2hqz46q9v6wsbl0k93hgkgnb8rg8cqy7ay60g1n")))

(define-public crate-inwx-0.0.3 (c (n "inwx") (v "0.0.3") (d (list (d (n "igd") (r "^0.7.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.4.6") (d #t) (k 2)) (d (n "xmlrpc") (r "^0.11.1") (d #t) (k 0)))) (h "0bsavgh30svnh5ylmr8n4bra71vbkwrnmc30afbrp346gkv9385w")))

(define-public crate-inwx-0.1.0 (c (n "inwx") (v "0.1.0") (d (list (d (n "igd") (r "^0.8") (d #t) (k 2)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 2)) (d (n "xmlrpc") (r "^0.13") (d #t) (k 0)))) (h "1pr97448n9m6kv9abfvsagq035ip3c0lhq3n4v5s6a9qry5vjh8l")))

