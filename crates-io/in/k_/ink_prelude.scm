(define-module (crates-io in k_ ink_prelude) #:use-module (crates-io))

(define-public crate-ink_prelude-0.0.0 (c (n "ink_prelude") (v "0.0.0") (h "0m1rmacrymzipazlnalfwmnhc34a9xikga9f3ri0xfzqyi4zj876") (y #t)))

(define-public crate-ink_prelude-3.0.0-rc1 (c (n "ink_prelude") (v "3.0.0-rc1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "07rpng9r1pjfskmh9rrzpzx3b1phavrlf8893zldjqixlbkacplk") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-3.0.0-rc2 (c (n "ink_prelude") (v "3.0.0-rc2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0n0858wpliwcqhmryqb2yr21949g4xsdzhzy5j96r02zjhi9kamg") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-3.0.0-rc3 (c (n "ink_prelude") (v "3.0.0-rc3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1lhlfd123rfl92srzaamb3m819yxccg2dh4nv70r7fn9h36icwi1") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-3.0.0-rc4 (c (n "ink_prelude") (v "3.0.0-rc4") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1qilax01dhnmxski6pm1yy39ycdl1z12dg9y6hyvc0k9604jl76p") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-3.0.0-rc5 (c (n "ink_prelude") (v "3.0.0-rc5") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "12vh4dj2cr2qxhrz8ny41w3z4bx2xlrgl0xlz20ipxpwhp9xj0y8") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-3.0.0-rc6 (c (n "ink_prelude") (v "3.0.0-rc6") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0iia8lp1rbmill4gmgdyc1malh4sa91q3hqs0wx0w52i6i1nvyik") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-3.0.0-rc7 (c (n "ink_prelude") (v "3.0.0-rc7") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "10sgg30igpyv0n2c86j8p27p6g56z777d3gcsb7g9z1bf8pcq78b") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-3.0.0-rc8 (c (n "ink_prelude") (v "3.0.0-rc8") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0xp9dpv1mxpz0ki80fdy7kwq87gbdqv9l2dm5lsq0h1lapvfqb32") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-3.0.0-rc9 (c (n "ink_prelude") (v "3.0.0-rc9") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1s72s8k0l7vbx4cx7fcszld5n1awsc5pxmi9a6353xji78w2pv4b") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-3.0.0 (c (n "ink_prelude") (v "3.0.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1m5ywmp4rd0zc5fyplbxwrg0nq6s0vmrqsfc7fhsg34i6xlmb8dr") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-3.0.1 (c (n "ink_prelude") (v "3.0.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0zzx4y7zyl4pb2kvyiqx6d7kifd70ybfhc5h3jzcfx33zlgya5s9") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-3.1.0 (c (n "ink_prelude") (v "3.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "096vyiadx3003jgmqggn9zirj0vgvfhd9f993yxhf3davqx17ach") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-3.2.0 (c (n "ink_prelude") (v "3.2.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0nm6kghipz2alv9rnxr9snqg7zsihnxbg8wb0k52wmfz2myksi0p") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-3.3.0 (c (n "ink_prelude") (v "3.3.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0xy7blbwzbdm4n7n0l8j77flms5xg0z3bbyjr6b38msyq7gg9xxi") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-3.3.1 (c (n "ink_prelude") (v "3.3.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1wrkhshgi3l6g4xaqwdfwqxnqqvxjid6ca3xf8jk9v6gcapp33g3") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-4.0.0-alpha.1 (c (n "ink_prelude") (v "4.0.0-alpha.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1flbz2j1arpidhq0932hj3mf89mq06w9lnds2c716fijswsi97vr") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-4.0.0-alpha.3 (c (n "ink_prelude") (v "4.0.0-alpha.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "012d28hhbs028hmdsq0llp52vc2pr65lj9g0xk3njq2kn5x17l0w") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-3.4.0 (c (n "ink_prelude") (v "3.4.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "03jcn53fiprvd4mip7cmjjwjcv77490vz2dhi1r4lncmhimiw0qz") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-4.0.0-beta (c (n "ink_prelude") (v "4.0.0-beta") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0nli5ivsykq96j8026z62z768p0jg7awp49k12rw4jrhg1gi4qjr") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-4.0.0-beta.1 (c (n "ink_prelude") (v "4.0.0-beta.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "069h6xr1mwjg56nr21wgqsnal0a02hlv2pqyw3id0y3hvgqzsb54") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-4.0.0-rc (c (n "ink_prelude") (v "4.0.0-rc") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1gpwc8bfh8srgkrxwmnnv72wz7asbjsnirargmj45lhyrnlfy776") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-4.0.0 (c (n "ink_prelude") (v "4.0.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0ws1j8rv00ipyh5rxl8f3n9h98mavv6xikblayb27y92r6q6yqn2") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-4.0.1 (c (n "ink_prelude") (v "4.0.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0s52kavkzy84w77sz8fmnj93q6wyjpljyxy7q9sik4mfwlsh427q") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-4.1.0 (c (n "ink_prelude") (v "4.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0qa0hp3hj2v5sm03w3jn4jxidl9m5kr6shp4bzppvpvp33lwl385") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-4.2.0 (c (n "ink_prelude") (v "4.2.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1fbmnqiqr30l7dq7sn80r4j0wl8yxzwnxkinx87nz8ja3nx641kd") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-4.2.1 (c (n "ink_prelude") (v "4.2.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1hdszdm42br81qxil5pm9wdl897kb60ss5k7wsmjky9gfi6ifvva") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-4.3.0 (c (n "ink_prelude") (v "4.3.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1xlfjhs7br2gd1i2zd337w1kvgbgzn03gp9lzf7g0hmlsa8xzkyq") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-5.0.0-alpha (c (n "ink_prelude") (v "5.0.0-alpha") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "19vaxlxsc7bxz3dc49liqj9753nrnnsj8mb7jzdwlg4rsq05qw3l") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-5.0.0-rc (c (n "ink_prelude") (v "5.0.0-rc") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0yx4cvnh73xvvvvppcsbjijvyxxman6mb4jammbf8y6gw1avsgzw") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-5.0.0-rc.1 (c (n "ink_prelude") (v "5.0.0-rc.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1wv97hbchhs6v3958wi6qslkq753b903bka1kqm48wl2hi8xcbmx") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-5.0.0-rc.2 (c (n "ink_prelude") (v "5.0.0-rc.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1fdpvswvaj25qmwlqqrhqnn4d7jx0mxcb8l17wbjs2is7pr9k6m6") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-5.0.0-rc.3 (c (n "ink_prelude") (v "5.0.0-rc.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "00wp4c829r8ipxqsj3sm95mibs6wdsqnd22jrbrfkqxwp93xdznx") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_prelude-5.0.0 (c (n "ink_prelude") (v "5.0.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1jfmrbsc0mii4yglc8dsxy8ig9fq9xi7bbn8b4paf2n8b38385za") (f (quote (("std") ("default" "std"))))))

