(define-module (crates-io in k_ ink_contract_decoder) #:use-module (crates-io))

(define-public crate-ink_contract_decoder-0.1.0 (c (n "ink_contract_decoder") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ss8vd099hr5lc22lzlfv1rll5xlqs6qcwawazghbnw9ac6c4d83")))

(define-public crate-ink_contract_decoder-0.1.1 (c (n "ink_contract_decoder") (v "0.1.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15m7lyijbgnpab2bfxgv0mivf6lg6kxnysl052xv3i70akcay1d8")))

