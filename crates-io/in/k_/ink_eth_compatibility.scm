(define-module (crates-io in k_ ink_eth_compatibility) #:use-module (crates-io))

(define-public crate-ink_eth_compatibility-3.0.0-rc6 (c (n "ink_eth_compatibility") (v "3.0.0-rc6") (d (list (d (n "ink_env") (r "^3.0.0-rc6") (k 0)) (d (n "libsecp256k1") (r "^0.3.5") (k 0)))) (h "0ifimafkfgc7fc3yy1fbs8d2bq7laky0ksqc16z67cynn2vmr6rg") (f (quote (("std" "ink_env/std") ("default" "std"))))))

(define-public crate-ink_eth_compatibility-3.0.0-rc7 (c (n "ink_eth_compatibility") (v "3.0.0-rc7") (d (list (d (n "ink_env") (r "^3.0.0-rc7") (k 0)) (d (n "libsecp256k1") (r "^0.7.0") (k 0)))) (h "0c1xs04cx4zb2c8ai5jas7s40fy7yznxg58pdppb3sic1mgn9bh6") (f (quote (("std" "ink_env/std") ("default" "std"))))))

(define-public crate-ink_eth_compatibility-3.0.0-rc8 (c (n "ink_eth_compatibility") (v "3.0.0-rc8") (d (list (d (n "ink_env") (r "^3.0.0-rc8") (k 0)) (d (n "libsecp256k1") (r "^0.7.0") (t "cfg(not(target_os = \"windows\"))") (k 0)))) (h "1dmc58ik8dg5xz0ln0069qx372qlk132j05z46xpc05xynnqj8rk") (f (quote (("std" "ink_env/std") ("default" "std"))))))

(define-public crate-ink_eth_compatibility-3.0.0-rc9 (c (n "ink_eth_compatibility") (v "3.0.0-rc9") (d (list (d (n "ink_env") (r "^3.0.0-rc9") (k 0)) (d (n "libsecp256k1") (r "^0.7.0") (t "cfg(not(target_os = \"windows\"))") (k 0)))) (h "00dbh56xc9dmgkadw89arr6nk521ing1kdjc1jp95qhgfr6xn05g") (f (quote (("std" "ink_env/std") ("default" "std"))))))

(define-public crate-ink_eth_compatibility-3.0.0 (c (n "ink_eth_compatibility") (v "3.0.0") (d (list (d (n "ink_env") (r "^3.0.0") (k 0)) (d (n "libsecp256k1") (r "^0.7.0") (t "cfg(not(target_os = \"windows\"))") (k 0)))) (h "02nlgib0vcsx8b91plk6rz6cl1q9kijhiww24shvmlhxn9iawkr2") (f (quote (("std" "ink_env/std") ("default" "std"))))))

(define-public crate-ink_eth_compatibility-3.0.1 (c (n "ink_eth_compatibility") (v "3.0.1") (d (list (d (n "ink_env") (r "^3.0.1") (k 0)) (d (n "libsecp256k1") (r "^0.7.0") (t "cfg(not(target_os = \"windows\"))") (k 0)))) (h "0400slqisyqhlcj6502m35lm34g4m54d17b0sg4v89wx3l69garl") (f (quote (("std" "ink_env/std") ("default" "std"))))))

(define-public crate-ink_eth_compatibility-3.3.0 (c (n "ink_eth_compatibility") (v "3.3.0") (d (list (d (n "ink_env") (r "^3.3.0") (k 0)) (d (n "libsecp256k1") (r "^0.7.0") (t "cfg(not(target_os = \"windows\"))") (k 0)))) (h "1k833nn1izidgmg4p4nl0pbz8vlrwdnidqq97gdqc501hpvg55xs") (f (quote (("std" "ink_env/std") ("default" "std"))))))

(define-public crate-ink_eth_compatibility-3.3.1 (c (n "ink_eth_compatibility") (v "3.3.1") (d (list (d (n "ink_env") (r "^3.3.1") (k 0)) (d (n "libsecp256k1") (r "^0.7.0") (t "cfg(not(target_os = \"windows\"))") (k 0)))) (h "13y96whgvawh7mwnkdy2l0wqnq7ff748wd6kgn4z55hjan5l0ha6") (f (quote (("std" "ink_env/std") ("default" "std"))))))

(define-public crate-ink_eth_compatibility-3.4.0 (c (n "ink_eth_compatibility") (v "3.4.0") (d (list (d (n "ink_env") (r "^3.4.0") (k 0)) (d (n "libsecp256k1") (r "^0.7.0") (t "cfg(not(target_os = \"windows\"))") (k 0)))) (h "0b2dqf0b47dqihfjvdfbn254csivz9q115mkq5w8w1z2s1djnhpl") (f (quote (("std" "ink_env/std") ("default" "std"))))))

