(define-module (crates-io in k_ ink_allocator) #:use-module (crates-io))

(define-public crate-ink_allocator-3.0.0-rc1 (c (n "ink_allocator") (v "3.0.0-rc1") (d (list (d (n "wee_alloc") (r "^0.4") (k 0)))) (h "1zvh2jhlzw8kpyxlpyi35625wwdi187bqz3387g7r73m8mw2h7bc") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_allocator-3.0.0-rc2 (c (n "ink_allocator") (v "3.0.0-rc2") (d (list (d (n "wee_alloc") (r "^0.4") (k 0)))) (h "1rc1fgb8cn400m2y4dlsdjcgh61xmblszkm9ddhdixlpxixliqgb") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_allocator-3.0.0-rc3 (c (n "ink_allocator") (v "3.0.0-rc3") (d (list (d (n "wee_alloc") (r "^0.4") (k 0)))) (h "118q2k7xh6s0fpjj2ijx7ivz9rcrjfb05bzcgl8m4fk9hbfixd64") (f (quote (("std") ("default" "std"))))))

(define-public crate-ink_allocator-3.0.0-rc4 (c (n "ink_allocator") (v "3.0.0-rc4") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (o #t) (k 0)))) (h "05dbvxc85f3ssgrfc65vzw9sprzj77zh05cswjllg7w222i88xny") (f (quote (("wee-alloc" "wee_alloc") ("std") ("default" "std"))))))

(define-public crate-ink_allocator-3.0.0-rc5 (c (n "ink_allocator") (v "3.0.0-rc5") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (o #t) (k 0)))) (h "1qzln2zyj3zs50q3yfz05cqlbkcprpvzrmn6zysprjkdx56raf8g") (f (quote (("wee-alloc" "wee_alloc") ("std") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-3.0.0-rc6 (c (n "ink_allocator") (v "3.0.0-rc6") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (o #t) (k 0)))) (h "08jzym270lvxzanz8xsfl6s5icdj9imwvsjcqw7yczj64i429525") (f (quote (("wee-alloc" "wee_alloc") ("std") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-3.0.0-rc7 (c (n "ink_allocator") (v "3.0.0-rc7") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (o #t) (k 0)))) (h "17casyg91bbi33shc9c0nvm11v4m5zaxzikl3rb3k6yf0spsm3qp") (f (quote (("wee-alloc" "wee_alloc") ("std") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-3.0.0-rc8 (c (n "ink_allocator") (v "3.0.0-rc8") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (o #t) (k 0)))) (h "0dka049nr86ninakfyri3gjqx37znshdvb2sjhkmkgqdcjhgqb95") (f (quote (("wee-alloc" "wee_alloc") ("std") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-3.0.0-rc9 (c (n "ink_allocator") (v "3.0.0-rc9") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (o #t) (k 0)))) (h "0g8265khb3pbg0iq8nrcb83kx8cyc9yl0bqy11gx0y5y6z9fkv5q") (f (quote (("wee-alloc" "wee_alloc") ("std") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-3.0.0 (c (n "ink_allocator") (v "3.0.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (o #t) (k 0)))) (h "1nd1rg9vh3xqrrqqrgl65jfxwjqcqg491xvhcvil0mv5qgrh0igz") (f (quote (("wee-alloc" "wee_alloc") ("std") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-3.0.1 (c (n "ink_allocator") (v "3.0.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (o #t) (k 0)))) (h "04fypl9v2hwsxnc4ckz1m9bqp4wbnv2s84p7lw5n846gmjci6v8m") (f (quote (("wee-alloc" "wee_alloc") ("std") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-3.1.0 (c (n "ink_allocator") (v "3.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (o #t) (k 0)))) (h "0s93g1244094f0flj48j2555167dlmrgc68l17fq9kf6sq4sdrmb") (f (quote (("wee-alloc" "wee_alloc") ("std") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-3.2.0 (c (n "ink_allocator") (v "3.2.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (o #t) (k 0)))) (h "1lvc596kpngarjzbyinc53l48h0kfjngqcibdak6pr8svkiaacg1") (f (quote (("wee-alloc" "wee_alloc") ("std") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-3.3.0 (c (n "ink_allocator") (v "3.3.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (o #t) (k 0)))) (h "0j9hf697p2jx4g9rx3a4pjfd3nykp2130vfgxd8x13i9fkg4klly") (f (quote (("wee-alloc" "wee_alloc") ("std") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-3.3.1 (c (n "ink_allocator") (v "3.3.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (o #t) (k 0)))) (h "01l9qmv5mrd8805ssjrya9xmpshaihmi08ff06xs7dqhwc8z94d2") (f (quote (("wee-alloc" "wee_alloc") ("std") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-4.0.0-alpha.1 (c (n "ink_allocator") (v "4.0.0-alpha.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (o #t) (k 0)))) (h "1i6p54qmh87z2vzacdx4d6pmv4mphgkd5lfzgkvbmgrd6s36awai") (f (quote (("wee-alloc" "wee_alloc") ("std") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-4.0.0-alpha.3 (c (n "ink_allocator") (v "4.0.0-alpha.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1wq2f46rv3d0ddgx9jgwsf3n8m3g9dmp03ckvnh9zvyrzmay068x") (f (quote (("std") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-3.4.0 (c (n "ink_allocator") (v "3.4.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4") (o #t) (k 0)))) (h "04lb16mg3l8i727v39ip0mmc0xxab8dx2g0mnb09g28fkajqi58w") (f (quote (("wee-alloc" "wee_alloc") ("std") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-4.0.0-beta (c (n "ink_allocator") (v "4.0.0-beta") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1nd2b158ni82xmnwv2px7zcwqwyffka552qr3kzb0cvyvknasvdk") (f (quote (("std") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-4.0.0-beta.1 (c (n "ink_allocator") (v "4.0.0-beta.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1qjsxnrizyxnnswz836i9ivm3q2b0qsc37cggcd43l99i33c6ilj") (f (quote (("std") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-4.0.0-rc (c (n "ink_allocator") (v "4.0.0-rc") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1jfhvcww3886vl0c58ylyhpgv8l881acv5rz21cgwxv19w32q18i") (f (quote (("std") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-4.0.0 (c (n "ink_allocator") (v "4.0.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1s8cgq8x8x3cs9w11z0cv8sdd0j1dnly5jv2ahnny9h077sd88sk") (f (quote (("std") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-4.0.1 (c (n "ink_allocator") (v "4.0.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "0c8n483qncjsf6r3z9gm8j83qkfsr76qsqif0spbr5qgzfpvnkd5") (f (quote (("std") ("no-allocator") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-4.1.0 (c (n "ink_allocator") (v "4.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1smffag2m7x7dm2p5fvyvxlyidv2hp6gb6ppqw4n5m8741jmh5qj") (f (quote (("std") ("no-allocator") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-4.2.0 (c (n "ink_allocator") (v "4.2.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1qcp7pnl8mdgjzhvjcyfiq5i9f4ar89821jkn3bhq3ldqfxrvp0w") (f (quote (("std") ("no-allocator") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-4.2.1 (c (n "ink_allocator") (v "4.2.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1a6lcxdfbl889n9sl2ync6n2l6jnqb3chhi1a71kfskz427igcsw") (f (quote (("std") ("no-allocator") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-4.3.0 (c (n "ink_allocator") (v "4.3.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1zm1kbszpndix0z0dz97y5x82qmm30cmsg1dyjipgzbh0jbi82c7") (f (quote (("std") ("no-allocator") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-5.0.0-alpha (c (n "ink_allocator") (v "5.0.0-alpha") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1s32ygmx7znfcsk2jlc0rcv4pj7a1j5qxav3lbv03qdn4sn3g2k8") (f (quote (("std") ("no-allocator") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-5.0.0-rc (c (n "ink_allocator") (v "5.0.0-rc") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1r5x9178ysmnz9k4jwlvbfnq0nzzlxl9kamcdl3yzcs5nkfjw7bw") (f (quote (("std") ("no-allocator") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-5.0.0-rc.1 (c (n "ink_allocator") (v "5.0.0-rc.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "0f674wjp7jdsc97lbkl7v2ssi2sybvq98hsr6pky9xhj3sw9jsg6") (f (quote (("std") ("no-allocator") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-5.0.0-rc.2 (c (n "ink_allocator") (v "5.0.0-rc.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1m6gzdnggrcnx6mr1x10y0890rgsna2bynwxcbs4llyj3drpmf0x") (f (quote (("std") ("no-allocator") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-5.0.0-rc.3 (c (n "ink_allocator") (v "5.0.0-rc.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "04z483cwhqap1h5vh0x36a2rqng67d7h8apjqy69iqxfgqg502a5") (f (quote (("std") ("no-allocator") ("ink-fuzz-tests" "std") ("default" "std"))))))

(define-public crate-ink_allocator-5.0.0 (c (n "ink_allocator") (v "5.0.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "0gny1z3szm0cq38jz0imjqya7fk97dglr52y8a6r4vdcbc2mdvjw") (f (quote (("std") ("no-allocator") ("ink-fuzz-tests" "std") ("default" "std"))))))

