(define-module (crates-io in k_ ink_linting_clippy_utils) #:use-module (crates-io))

(define-public crate-ink_linting_clippy_utils-0.1.73 (c (n "ink_linting_clippy_utils") (v "0.1.73") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "if_chain") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rustc-semver") (r "^1.1") (d #t) (k 0)))) (h "1197gr20m24cpqw1zmlnf7n6ylgcnhjq23zmwm52jqlksf5m6knk") (f (quote (("internal") ("deny-warnings"))))))

