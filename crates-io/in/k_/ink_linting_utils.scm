(define-module (crates-io in k_ ink_linting_utils) #:use-module (crates-io))

(define-public crate-ink_linting_utils-5.0.0-rc (c (n "ink_linting_utils") (v "5.0.0-rc") (d (list (d (n "if_chain") (r "^1.0.2") (d #t) (k 0)) (d (n "parity_clippy_utils") (r "^0.1.73") (d #t) (k 0)))) (h "1nshnfnfpfyymdl7526k73yix35ihalw68zj4d3s7kl440w7g08l")))

