(define-module (crates-io in k_ ink_storage_derive) #:use-module (crates-io))

(define-public crate-ink_storage_derive-3.0.0-rc1 (c (n "ink_storage_derive") (v "3.0.0-rc1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "0j77z78hy2pr27il24ql857i9a69izcrrmcd10511k83z0xskjl4")))

(define-public crate-ink_storage_derive-3.0.0-rc2 (c (n "ink_storage_derive") (v "3.0.0-rc2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "0wm3fd0wa3w3sr884h285biq5ds1sp6gkywldw34w6xhy78b3kam")))

(define-public crate-ink_storage_derive-3.0.0-rc3 (c (n "ink_storage_derive") (v "3.0.0-rc3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "10vvpnrkb27aknq166xwyd0c01rw9wk96r1raf38nhfvgj3rn0j3")))

(define-public crate-ink_storage_derive-3.0.0-rc4 (c (n "ink_storage_derive") (v "3.0.0-rc4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "0il15gp90gkyssar5ysmcv0cb0d0fv8aak6r7l1bnkgcpamiwjbw")))

(define-public crate-ink_storage_derive-3.0.0-rc5 (c (n "ink_storage_derive") (v "3.0.0-rc5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "137slcpxp8agx18p0vr0v8lfywsn5rkkjl1bpf139gi4lz3if9y5")))

(define-public crate-ink_storage_derive-3.0.0-rc6 (c (n "ink_storage_derive") (v "3.0.0-rc6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "1w5h036mkkan1iwb3x8i5gfg2zzdd2qp95m9xwxgcz9fjhh4bcxh")))

(define-public crate-ink_storage_derive-3.0.0-rc7 (c (n "ink_storage_derive") (v "3.0.0-rc7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "18l58s88jjxzxkdnpcan5731f26zz4jan7jm58fdr5sm5kd2gw2w")))

(define-public crate-ink_storage_derive-3.0.0-rc8 (c (n "ink_storage_derive") (v "3.0.0-rc8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "0wzc5asl528ciz05vbfv192g5mxmnp0sdh2ps974ymngnvy9lzak")))

(define-public crate-ink_storage_derive-3.0.0-rc9 (c (n "ink_storage_derive") (v "3.0.0-rc9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "1wss2p8x98fyi690fnq3rxlkad7mgghjp2hycnmnm29kkd9sd5l7")))

(define-public crate-ink_storage_derive-3.0.0 (c (n "ink_storage_derive") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "0wc9fs5cvg5a8451m2hh4hc6v709hdwbjrsmc09ldvin5y5w03sv")))

(define-public crate-ink_storage_derive-3.0.1 (c (n "ink_storage_derive") (v "3.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "05n5f2w43q5vs5g0rrfnrmhiks542fh1z3vx7qrvdk29657r3ya6")))

(define-public crate-ink_storage_derive-3.1.0 (c (n "ink_storage_derive") (v "3.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "1i1d36i95dfqg50j58zw731jwgaq4f2nr9fxf1m18fnggsylm7kr")))

(define-public crate-ink_storage_derive-3.2.0 (c (n "ink_storage_derive") (v "3.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "1695a306l7g04rlyly8jiccy3b7r7faa1rms0nagpcjxia3kbgkz")))

(define-public crate-ink_storage_derive-3.3.0 (c (n "ink_storage_derive") (v "3.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "0p2qkf09daiwb0l2yf9bxphh3gjdxvbnhj4jl7fjg0ryx4j8xdmg")))

(define-public crate-ink_storage_derive-3.3.1 (c (n "ink_storage_derive") (v "3.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "scale") (r "^3") (f (quote ("derive" "full"))) (k 2) (p "parity-scale-codec")) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "0fc4i34gxah3b8c2yqzklpxv9hzr7k3jxzbz4nqnh8hbblnlbvcc")))

(define-public crate-ink_storage_derive-4.0.0-alpha.1 (c (n "ink_storage_derive") (v "4.0.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "0nf9d1vvm07m9yafyah717frrpwpmm1drrg8myid23mprrjh1sxq")))

(define-public crate-ink_storage_derive-3.4.0 (c (n "ink_storage_derive") (v "3.4.0") (d (list (d (n "ink_env") (r "^3.4.0") (d #t) (k 2)) (d (n "ink_metadata") (r "^3.4.0") (d #t) (k 2)) (d (n "ink_prelude") (r "^3.4.0") (d #t) (k 2)) (d (n "ink_primitives") (r "^3.4.0") (d #t) (k 2)) (d (n "ink_storage") (r "^3.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "scale") (r "^3") (f (quote ("derive" "full"))) (k 2) (p "parity-scale-codec")) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "1fp2gmdr4w77vk627nnimaa9g7civgnq38caisnik4nw0f4ivgxs")))

