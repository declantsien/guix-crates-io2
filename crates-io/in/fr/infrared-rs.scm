(define-module (crates-io in fr infrared-rs) #:use-module (crates-io))

(define-public crate-infrared-rs-0.1.0 (c (n "infrared-rs") (v "0.1.0") (d (list (d (n "rppal") (r "^0.13.1") (d #t) (k 0)))) (h "1g04bil4wy4hl4av8f69y7wvh0q1qlr6j6akvxiqwaq9ly5dp7lb")))

(define-public crate-infrared-rs-0.2.0 (c (n "infrared-rs") (v "0.2.0") (d (list (d (n "rppal") (r "^0.13.1") (d #t) (k 0)))) (h "1gj6slvdq5wrzkwqqs6p72qlz1gkgdij4mrlph6j416phashdx4j")))

(define-public crate-infrared-rs-0.3.0 (c (n "infrared-rs") (v "0.3.0") (d (list (d (n "gpio-cdev") (r "^0.5.1") (d #t) (k 0)) (d (n "rppal") (r "^0.14.1") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (k 0)))) (h "1gnbgjkb73l1x69ma85ax83j0ny862krqr17p82qn6viljnyx4dz")))

(define-public crate-infrared-rs-0.3.1 (c (n "infrared-rs") (v "0.3.1") (d (list (d (n "gpio-cdev") (r "^0.5.1") (d #t) (k 0)) (d (n "rppal") (r "^0.14.1") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.1.1") (d #t) (k 0)))) (h "17klmjgs0r962jxhwiqx76k5af77xlal777bclky8630wbbsbr4k")))

