(define-module (crates-io in fr infrared) #:use-module (crates-io))

(define-public crate-infrared-0.1.0 (c (n "infrared") (v "0.1.0") (h "1s4hdi2lw78lb0a5r6l5ql759m5763w4s4b1ry9givxzwrdbaw3g")))

(define-public crate-infrared-0.2.0 (c (n "infrared") (v "0.2.0") (h "14qb7c549n55z4wh4d04l3b5f0gmipfpq8z9vb54d6rfic283fkk")))

(define-public crate-infrared-0.3.0 (c (n "infrared") (v "0.3.0") (h "1gvk4dcsjfaf6v62j5qg8bbyk793hj5db15lg9j2pds2999wnw9d")))

(define-public crate-infrared-0.3.1 (c (n "infrared") (v "0.3.1") (h "0plyasgvfx3b7rrffjvbywk82dxpcs583qqfqg0ybznhbbsz4kn8")))

(define-public crate-infrared-0.3.2 (c (n "infrared") (v "0.3.2") (h "0sihhy5wg50pasy57jj6al57rgl87wpv5h5v2czd5wg6na0siyp9")))

(define-public crate-infrared-0.4.0 (c (n "infrared") (v "0.4.0") (h "17p16y4690gfb4hb67vgk186qpgsp12ymwrzk0fxyd9axrq0lal7") (f (quote (("rc6") ("rc5") ("protocol-dev") ("nec") ("default" "nec" "rc5" "rc6"))))))

(define-public crate-infrared-0.4.1 (c (n "infrared") (v "0.4.1") (h "02ndb96bwgnvjp5db7hhd14kf3z2wxgkmbi1vbaapg5m7s1d4ir8") (f (quote (("rc6") ("rc5") ("protocol-dev") ("nec") ("default" "nec" "rc5" "rc6"))))))

(define-public crate-infrared-0.5.0 (c (n "infrared") (v "0.5.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (o #t) (d #t) (k 0)))) (h "04la1gvkcv4kg104fwddjy5s4rz889yspb43nhi0hc6a1zbwfnls") (f (quote (("std") ("sbp") ("remotes") ("rc6") ("rc5") ("protocol-dev") ("nec") ("default" "nec" "rc5" "rc6" "sbp"))))))

(define-public crate-infrared-0.6.0 (c (n "infrared") (v "0.6.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (o #t) (d #t) (k 0)))) (h "11pkx0ywdn9yahv5a1d36fs1wp2zz4pcjy19m9759xyr73b97bq0") (f (quote (("std") ("sbp") ("remotes") ("rc6") ("rc5") ("protocol-debug") ("nec") ("default" "nec" "rc5" "rc6" "sbp" "embedded-hal" "remotes"))))))

(define-public crate-infrared-0.7.0 (c (n "infrared") (v "0.7.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (o #t) (d #t) (k 0)))) (h "0rw4715735hg6hzr4ycnxlajn3m2s2gbckhsc8jjjxiydfa2v3r5") (f (quote (("std") ("sbp") ("remotes") ("rc6") ("rc5") ("nec") ("default" "nec" "rc5" "rc6" "sbp" "embedded-hal" "remotes"))))))

(define-public crate-infrared-0.8.0 (c (n "infrared") (v "0.8.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (o #t) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "1sahqv0vix3k5p2b62ih6sm4b5jic27v3waw60v5bdv1lky5sqw7") (f (quote (("std") ("sbp") ("remotes") ("rc6") ("rc5") ("nec") ("default" "nec" "rc5" "rc6" "sbp" "embedded-hal" "remotes"))))))

(define-public crate-infrared-0.9.0 (c (n "infrared") (v "0.9.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (o #t) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "1a60sin8804422izrxpk7ajrb6zhzl5wxccn64xfff51by4k0plc") (f (quote (("std") ("sbp") ("remotes") ("rc6") ("rc5") ("nec") ("default" "nec" "rc5" "rc6" "sbp" "embedded-hal" "remotes"))))))

(define-public crate-infrared-0.10.0 (c (n "infrared") (v "0.10.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (o #t) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "0pj07m4djg8mnk26vqbk5pny7wlias2f10w4w4453d3gr21la9wy") (f (quote (("std") ("sbp") ("remotes") ("rc6") ("rc5") ("nec") ("denon") ("default" "nec" "rc5" "rc6" "sbp" "denon" "embedded-hal" "remotes"))))))

(define-public crate-infrared-0.11.0 (c (n "infrared") (v "0.11.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (o #t) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "1a7dgnpylvhabj2x2fdikgskv4i997a6ag1pp2gzr3i1iracca33") (f (quote (("sbp") ("remotes") ("rc6") ("rc5") ("nec") ("denon") ("default" "nec" "rc5" "rc6" "sbp" "denon" "embedded-hal" "remotes"))))))

(define-public crate-infrared-0.12.0 (c (n "infrared") (v "0.12.0") (d (list (d (n "dummy-pin") (r "^0.1.1") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (o #t) (d #t) (k 0)) (d (n "vcd") (r "^0.6.1") (d #t) (k 2)))) (h "164m0azxx3m33w0r55y9qqxq9qvfxb7ivcn71bik3p6jp3njy3pq") (f (quote (("sbp") ("remotes") ("rc6") ("rc5") ("nec") ("denon") ("default" "nec" "rc5" "rc6" "sbp" "denon" "remotes" "embedded-hal"))))))

(define-public crate-infrared-0.13.0 (c (n "infrared") (v "0.13.0") (d (list (d (n "defmt") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "dummy-pin") (r "^0.1.1") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.11") (o #t) (d #t) (k 0)))) (h "0p8a5nsgyfapkb39cdfkbrpwh0ih98226lc1ciac3yz2pa1jmr2x") (f (quote (("sbp") ("remotes") ("rc6") ("rc5") ("nec") ("denon") ("defmt-warn") ("defmt-trace") ("defmt-info") ("defmt-error") ("defmt-default") ("defmt-debug") ("default" "nec" "rc5" "rc6" "sbp" "denon" "remotes" "embedded-hal"))))))

(define-public crate-infrared-0.14.0 (c (n "infrared") (v "0.14.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "dummy-pin") (r "^0.1.1") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (o #t) (d #t) (k 0)) (d (n "fugit") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0zbdsalxh3z226f17mmawi0c4wvv7dgl5xfdhh14vb858kjs4lzq") (f (quote (("sbp") ("remotes") ("rc6") ("rc5") ("nec") ("embedded" "embedded-hal" "fugit") ("denon") ("default" "nec" "rc5" "rc6" "sbp" "denon" "remotes" "embedded"))))))

(define-public crate-infrared-0.14.1 (c (n "infrared") (v "0.14.1") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "dummy-pin") (r "^0.1.1") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (o #t) (d #t) (k 0)) (d (n "fugit") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0lql95dvpll1qi7yy30rsaqrzs60sp716h39jc5ngmpv87i44nvd") (f (quote (("sbp") ("remotes") ("rc6") ("rc5") ("nec") ("embedded" "embedded-hal" "fugit") ("denon") ("default" "nec" "rc5" "rc6" "sbp" "denon" "remotes" "embedded"))))))

(define-public crate-infrared-0.14.2 (c (n "infrared") (v "0.14.2") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (o #t) (d #t) (k 0)) (d (n "fugit") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "dummy-pin") (r "^0.1.1") (d #t) (k 2)))) (h "1vdvji7m0d3bfb5rgvbkys0b85nkrgs06r350zgm4vsbam2i138p") (f (quote (("sbp") ("remotes") ("rc6") ("rc5") ("nec") ("embedded" "embedded-hal" "fugit") ("denon") ("default" "nec" "rc5" "rc6" "sbp" "denon" "remotes" "embedded"))))))

