(define-module (crates-io in kp inkpad-seal) #:use-module (crates-io))

(define-public crate-inkpad-seal-0.1.0 (c (n "inkpad-seal") (v "0.1.0") (d (list (d (n "inkpad-derive") (r "^0") (d #t) (k 0)) (d (n "inkpad-executor") (r "^0") (d #t) (k 0)) (d (n "inkpad-sandbox") (r "^0") (d #t) (k 0)) (d (n "inkpad-std") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^2") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nzih85abl213yidflq5lkfg6z6m75i0k7lzh20dq0akl5v1wzsk") (f (quote (("std" "inkpad-std/std" "inkpad-sandbox/std" "inkpad-executor/std") ("default"))))))

