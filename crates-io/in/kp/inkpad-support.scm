(define-module (crates-io in kp inkpad-support) #:use-module (crates-io))

(define-public crate-inkpad-support-0.1.0 (c (n "inkpad-support") (v "0.1.0") (d (list (d (n "derivative") (r "^2.2.0") (f (quote ("use_core"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "inkpad-std") (r "^0") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "parity-wasm") (r "^0.42.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "00bvnbkhwy4zpmbzxcm3rmcwpwdh107i3p9hwszrwwschjcxk7cr") (f (quote (("std" "inkpad-std/std") ("default" "std"))))))

