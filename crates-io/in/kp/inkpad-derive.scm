(define-module (crates-io in kp inkpad-derive) #:use-module (crates-io))

(define-public crate-inkpad-derive-0.1.0 (c (n "inkpad-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "05ygwxpjl7n8w2sadsvi2qhz1wfw1vmjnm29hb8japdrf9p1790b")))

