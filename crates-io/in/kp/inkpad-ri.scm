(define-module (crates-io in kp inkpad-ri) #:use-module (crates-io))

(define-public crate-inkpad-ri-0.1.0 (c (n "inkpad-ri") (v "0.1.0") (d (list (d (n "blake2-rfc") (r "^0.2.18") (d #t) (k 0)) (d (n "inkpad-executor") (r "^0") (d #t) (k 0)) (d (n "inkpad-sandbox") (r "^0") (d #t) (k 0)) (d (n "inkpad-seal") (r "^0") (d #t) (k 0)) (d (n "inkpad-std") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "1sky1khpanwqhyjccqknaajzdzj2423ffbllw98ipnz1z3gc2w5n") (f (quote (("std" "inkpad-seal/std" "inkpad-sandbox/std" "inkpad-executor/std") ("default"))))))

