(define-module (crates-io in fa infallible-sonyflake) #:use-module (crates-io))

(define-public crate-infallible-sonyflake-0.1.0 (c (n "infallible-sonyflake") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pnet") (r "^0.28.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rwgydhwm8w0qz0m9qkmvx4p725h1c97y990ggv0jxgq7fs1h8ba")))

(define-public crate-infallible-sonyflake-0.1.1 (c (n "infallible-sonyflake") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pnet") (r "^0.28.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p3g7xlr8hfnxcyg30zrxl8jnqg16q1gz590xg4w38qyn8ylpzcy")))

