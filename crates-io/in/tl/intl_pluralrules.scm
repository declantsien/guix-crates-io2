(define-module (crates-io in tl intl_pluralrules) #:use-module (crates-io))

(define-public crate-intl_pluralrules-0.8.0 (c (n "intl_pluralrules") (v "0.8.0") (d (list (d (n "matches") (r "^0.1.6") (d #t) (k 0)))) (h "087hl8vm26d86g5f1dy7m88f5zw48kkcj0x0ynvzx1hyrhg7rfif")))

(define-public crate-intl_pluralrules-0.8.1 (c (n "intl_pluralrules") (v "0.8.1") (d (list (d (n "matches") (r "^0.1.6") (d #t) (k 0)))) (h "1x0yjmnfxwhs33ma6vxpgi4xcm5prb3cr9k97hs7ny6mh5xl0am6")))

(define-public crate-intl_pluralrules-0.8.2 (c (n "intl_pluralrules") (v "0.8.2") (d (list (d (n "matches") (r "^0.1.6") (d #t) (k 0)))) (h "0np0l9a81vahknxzrv5w2f5psjs0hcszqnr3ykzns4acj5nrcqg2")))

(define-public crate-intl_pluralrules-0.9.0 (c (n "intl_pluralrules") (v "0.9.0") (d (list (d (n "matches") (r "^0.1.6") (d #t) (k 0)))) (h "0i0mam1lxdy2w71j5nd4fgfwz93p8hawkldxd6kw772wqxdq3pix")))

(define-public crate-intl_pluralrules-0.9.1 (c (n "intl_pluralrules") (v "0.9.1") (d (list (d (n "matches") (r "^0.1.6") (d #t) (k 0)))) (h "1kjgs5nwikrcc89qppbjzirkl174ml8jnskirh4ln2ff00y3zyf1")))

(define-public crate-intl_pluralrules-1.0.0 (c (n "intl_pluralrules") (v "1.0.0") (d (list (d (n "matches") (r "^0.1.6") (d #t) (k 0)) (d (n "phf") (r "^0.7.22") (d #t) (k 0)))) (h "0mp5n1iwdcv68vc72d79jd6mzxq52q56lyzf3wpfdb2gdfms90iv")))

(define-public crate-intl_pluralrules-1.0.1 (c (n "intl_pluralrules") (v "1.0.1") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 0)) (d (n "phf") (r "^0.7.23") (d #t) (k 0)))) (h "0z6d2xyppvx04ak0hy0zjcxhv0inv7r21q4d37hlzaxdx5xnzcdf")))

(define-public crate-intl_pluralrules-1.0.2 (c (n "intl_pluralrules") (v "1.0.2") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 0)) (d (n "phf") (r "^0.7.24") (d #t) (k 0)))) (h "1k1a8j7wrvqnxxnfxykv7zp7wjxdvl9hzfnyfmsn9hn0gxc1yg76")))

(define-public crate-intl_pluralrules-1.0.3 (c (n "intl_pluralrules") (v "1.0.3") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 0)) (d (n "phf") (r "^0.7.24") (d #t) (k 0)))) (h "1bhq3c9jf6ygys1rfyvisa2bj3phv6mas4jwrs8q33yk9j8czs24")))

(define-public crate-intl_pluralrules-1.1.0 (c (n "intl_pluralrules") (v "1.1.0") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 0)) (d (n "phf") (r "^0.7.24") (d #t) (k 0)) (d (n "unic-langid") (r "^0.3.0") (d #t) (k 0)))) (h "11l23sjaxzh7dr65hrnwb2bjlqp8x2dqjqmabqzflzs155jf5c2q") (y #t)))

(define-public crate-intl_pluralrules-2.0.0 (c (n "intl_pluralrules") (v "2.0.0") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 0)) (d (n "phf") (r "^0.7.24") (d #t) (k 0)) (d (n "unic-langid") (r "^0.4") (d #t) (k 0)))) (h "0x3i13anz9jdfc2fj7bkp27xllaqlcv900b0j71gxc4cgdkbyk4g")))

(define-public crate-intl_pluralrules-3.0.0 (c (n "intl_pluralrules") (v "3.0.0") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 0)) (d (n "phf") (r "^0.7.24") (d #t) (k 0)) (d (n "unic-langid") (r "^0.5") (d #t) (k 0)))) (h "0jal659csddfqi4xs4qlmrnj0p8d0r9cs7ijvlvx4ykf7im5p943")))

(define-public crate-intl_pluralrules-4.0.0 (c (n "intl_pluralrules") (v "4.0.0") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 0)) (d (n "phf") (r "^0.7.24") (d #t) (k 0)) (d (n "unic-langid") (r "^0.6") (d #t) (k 0)))) (h "03shwcijzk7gw0k5dnh70hj523qyb0c9g2fbjj8crkpigd31rqkb")))

(define-public crate-intl_pluralrules-4.0.1 (c (n "intl_pluralrules") (v "4.0.1") (d (list (d (n "matches") (r "^0.1.8") (d #t) (k 0)) (d (n "phf") (r "^0.7.24") (d #t) (k 0)) (d (n "unic-langid") (r "^0.6") (d #t) (k 0)))) (h "1kpd0qzvfj0kq1crrch7x4fakr4bk29ai4cf20rb64pcmwqgskci")))

(define-public crate-intl_pluralrules-5.0.0 (c (n "intl_pluralrules") (v "5.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tinystr") (r "^0.3.2") (d #t) (k 0)) (d (n "unic-langid") (r "^0.7.1") (d #t) (k 0)) (d (n "unic-langid") (r "^0.7") (f (quote ("macros"))) (d #t) (k 2)))) (h "0waq6g7qvxlwq7966pramn55rq4zkb6lbzq111pn9wh8m50j26m3")))

(define-public crate-intl_pluralrules-5.0.1 (c (n "intl_pluralrules") (v "5.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tinystr") (r "^0.3.2") (d #t) (k 0)) (d (n "unic-langid") (r "^0.7.1") (d #t) (k 0)) (d (n "unic-langid") (r "^0.7") (f (quote ("macros"))) (d #t) (k 2)))) (h "12n727yjq9cifjq9wnsbn37f93n9vhbl5pjy7m0jpxnxy0zbirlv")))

(define-public crate-intl_pluralrules-5.0.2 (c (n "intl_pluralrules") (v "5.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tinystr") (r "^0.3.2") (d #t) (k 0)) (d (n "unic-langid") (r "^0.7.1") (d #t) (k 0)) (d (n "unic-langid") (r "^0.7") (f (quote ("macros"))) (d #t) (k 2)))) (h "1gpi7bjbhajyqzd71k1591pfmzvqbjx87qr1g5nq6m05baicnbkm")))

(define-public crate-intl_pluralrules-6.0.0 (c (n "intl_pluralrules") (v "6.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tinystr") (r "^0.3.2") (d #t) (k 0)) (d (n "unic-langid") (r "^0.8") (d #t) (k 0)) (d (n "unic-langid") (r "^0.8") (f (quote ("macros"))) (d #t) (k 2)))) (h "00jhm0a64v6a5nb4s7j0xwf1yyzrsfj8dkp0acrw0hnfxvc18b6q")))

(define-public crate-intl_pluralrules-7.0.0 (c (n "intl_pluralrules") (v "7.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tinystr") (r "^0.3.2") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (f (quote ("macros"))) (d #t) (k 2)))) (h "1hwbm7vzkqdbsr0p6xgnf3r73yc1wqz9qq8pl2rzxa8j3zdiq9vc")))

(define-public crate-intl_pluralrules-7.0.1 (c (n "intl_pluralrules") (v "7.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tinystr") (r "^0.3.2") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (f (quote ("macros"))) (d #t) (k 2)))) (h "1ksy3hxqs8if3nbvcin0a8390lpkzbk2br1brik70z96hj1ri3xi")))

(define-public crate-intl_pluralrules-7.0.2 (c (n "intl_pluralrules") (v "7.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (f (quote ("macros"))) (d #t) (k 2)))) (h "0wprd3h6h8nfj62d8xk71h178q7zfn3srxm787w4sawsqavsg3h7")))

