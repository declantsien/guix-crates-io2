(define-module (crates-io in tl intl-memoizer) #:use-module (crates-io))

(define-public crate-intl-memoizer-0.2.0 (c (n "intl-memoizer") (v "0.2.0") (d (list (d (n "fluent-langneg") (r "^0.12") (d #t) (k 2)) (d (n "intl_pluralrules") (r "^6.0") (d #t) (k 2)) (d (n "type-map") (r "^0.3") (d #t) (k 0)) (d (n "unic-langid") (r "^0.8") (d #t) (k 0)))) (h "1yiclvbq87jz6k4h0gx4755kcv8lalp5fny7rcx3qvqczipilphv")))

(define-public crate-intl-memoizer-0.3.0 (c (n "intl-memoizer") (v "0.3.0") (d (list (d (n "fluent-langneg") (r "^0.12") (d #t) (k 2)) (d (n "intl_pluralrules") (r "^6.0") (d #t) (k 2)) (d (n "type-map") (r "^0.3") (d #t) (k 0)) (d (n "unic-langid") (r "^0.8") (d #t) (k 0)))) (h "038wkssvx362b05f3vdaz5cl5vjrmjrrid18yfsahmm975wkwlzz")))

(define-public crate-intl-memoizer-0.4.0 (c (n "intl-memoizer") (v "0.4.0") (d (list (d (n "fluent-langneg") (r "^0.12") (d #t) (k 2)) (d (n "intl_pluralrules") (r "^6.0") (d #t) (k 2)) (d (n "type-map") (r "^0.3") (d #t) (k 0)) (d (n "unic-langid") (r "^0.8") (d #t) (k 0)))) (h "07rr0p3n3j22h4apc1isx6247a9rns3hzv8p8brnx4w2bpbf4rwq")))

(define-public crate-intl-memoizer-0.5.0 (c (n "intl-memoizer") (v "0.5.0") (d (list (d (n "fluent-langneg") (r "^0.13") (d #t) (k 2)) (d (n "intl_pluralrules") (r "^7.0") (d #t) (k 2)) (d (n "type-map") (r "^0.3") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "02v3v52a4ps67l7jgvs0vyf03qmrr5py3md7m7w4k788ls5xa3la")))

(define-public crate-intl-memoizer-0.5.1 (c (n "intl-memoizer") (v "0.5.1") (d (list (d (n "fluent-langneg") (r "^0.13") (d #t) (k 2)) (d (n "intl_pluralrules") (r "^7.0.1") (d #t) (k 2)) (d (n "type-map") (r "^0.4") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "0vx6cji8ifw77zrgipwmvy1i3v43dcm58hwjxpb1h29i98z46463")))

(define-public crate-intl-memoizer-0.5.2 (c (n "intl-memoizer") (v "0.5.2") (d (list (d (n "fluent-langneg") (r "^0.13") (d #t) (k 2)) (d (n "intl_pluralrules") (r "^7.0.1") (d #t) (k 2)) (d (n "type-map") (r "^0.5") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (d #t) (k 0)))) (h "1nkvql7c7b76axv4g68di1p2m9bnxq1cbn6mlqcawf72zhhf08py")))

