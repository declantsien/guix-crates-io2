(define-module (crates-io in tl intl-rs) #:use-module (crates-io))

(define-public crate-intl-rs-1.0.0 (c (n "intl-rs") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)))) (h "0swxfzfkqgjxb362wp8bpxb0rzwvacn08zfgsyrzyxh3pdryxh9r")))

(define-public crate-intl-rs-1.1.0 (c (n "intl-rs") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)) (d (n "string_template") (r "^0.2.1") (d #t) (k 0)))) (h "00hbcjdjzgwmm8mr2xyy5nzr7wiz1z5hqdixjvi5jaac72mm2zjm")))

(define-public crate-intl-rs-1.2.0 (c (n "intl-rs") (v "1.2.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)) (d (n "string_template") (r "^0.2.1") (d #t) (k 0)))) (h "0bplfm9qn0y80c86jaagiiw7sq4xjf67snjxwyvpwhasf8pyh8dd")))

(define-public crate-intl-rs-2.0.0 (c (n "intl-rs") (v "2.0.0") (d (list (d (n "accept-language") (r "^2.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)) (d (n "string_template") (r "^0.2.1") (d #t) (k 0)))) (h "036sfilgjhnlnnk1p8arajp6kv9vfmd9m03kqz1zywfhy10pa96l")))

