(define-module (crates-io in jr injrs) #:use-module (crates-io))

(define-public crate-injrs-0.1.0 (c (n "injrs") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "minwindef" "ntdef" "tlhelp32" "psapi" "securitybaseapi" "libloaderapi" "synchapi" "wow64apiset" "processthreadsapi" "handleapi" "winbase" "impl-default"))) (d #t) (k 0)))) (h "07z7458f9s908v723q9bfn658zi1ivyliyg4lrfwr0vjva6pzs92") (y #t)))

(define-public crate-injrs-0.1.1 (c (n "injrs") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "minwindef" "ntdef" "tlhelp32" "psapi" "securitybaseapi" "libloaderapi" "synchapi" "wow64apiset" "processthreadsapi" "handleapi" "winbase" "impl-default"))) (d #t) (k 0)))) (h "010apqbriy9qny79dbiz6k3wfm9pzf3by73jazsqvh09px6bsdkv")))

(define-public crate-injrs-0.1.2 (c (n "injrs") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "minwindef" "ntdef" "winuser" "tlhelp32" "psapi" "securitybaseapi" "libloaderapi" "synchapi" "wow64apiset" "processthreadsapi" "handleapi" "winbase" "impl-default"))) (d #t) (k 0)))) (h "0lpqj0flrhk45nkblccfglklg2hxp409cff2z99b8p8giph2alpl")))

