(define-module (crates-io in -k in-keys) #:use-module (crates-io))

(define-public crate-in-keys-0.1.0 (c (n "in-keys") (v "0.1.0") (d (list (d (n "derived-deref") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0scq5zazzq233c8n99166336q3sbw2786ldna30midrpizygsk64")))

(define-public crate-in-keys-0.1.1 (c (n "in-keys") (v "0.1.1") (d (list (d (n "derived-deref") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fhvlc3jy6hhmkf4gs43zqy8lgayrxpl2rxv6axds4hqfxfgmdrp")))

(define-public crate-in-keys-0.2.0 (c (n "in-keys") (v "0.2.0") (d (list (d (n "derived-deref") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "100c35nkqx4d89l73arknd7lxvfg4x93pg6xabi5vr5swpv21am2")))

