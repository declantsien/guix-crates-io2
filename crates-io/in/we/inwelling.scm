(define-module (crates-io in we inwelling) #:use-module (crates-io))

(define-public crate-inwelling-0.1.0 (c (n "inwelling") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.12") (d #t) (k 0)) (d (n "pals") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hw6rw31pc616vxskdwi44jbhh3f5s4y1fg28a562j4v6p0xbjdr")))

(define-public crate-inwelling-0.1.1 (c (n "inwelling") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.12") (d #t) (k 0)) (d (n "pals") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11y6dcs7lw681z40a7lqvwni02dklf52ld0kc69away7vvj4mrj0")))

(define-public crate-inwelling-0.1.2 (c (n "inwelling") (v "0.1.2") (d (list (d (n "cargo_metadata") (r "^0.12") (d #t) (k 0)) (d (n "pals") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0g3a9cfvqf02zsqznc47d3fgbda93svwwji7m6xrqb2aksp2dvi0")))

(define-public crate-inwelling-0.2.0 (c (n "inwelling") (v "0.2.0") (d (list (d (n "cargo_metadata") (r "^0.12") (d #t) (k 0)) (d (n "pals") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0s9vmmh2mg61idzgzi3imqh8kgb8i24y9qn7v8qkby5m72jasqwc")))

(define-public crate-inwelling-0.3.0 (c (n "inwelling") (v "0.3.0") (d (list (d (n "cargo_metadata") (r "^0.12") (d #t) (k 0)) (d (n "pals") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0i1h9igr4dgy5nr6kxr9k60x77yqdfr0790vq3v2kw62mj7vb98x")))

(define-public crate-inwelling-0.4.0 (c (n "inwelling") (v "0.4.0") (d (list (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0dkj21zf0wrbcx6b49g0cz05c1b7vn2amlgrnsdic9w3h3b5xnqz")))

(define-public crate-inwelling-0.5.0 (c (n "inwelling") (v "0.5.0") (d (list (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "040gncrgsn1ccan16fsqrghkc26dlwkg0ns7izazpbs7bwr1bxha")))

(define-public crate-inwelling-0.5.1 (c (n "inwelling") (v "0.5.1") (d (list (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "16jwfbiab9f2w3s7wil9klq5iblavqq9jg2qgskxx8h4dbwsqhiy")))

(define-public crate-inwelling-0.5.2 (c (n "inwelling") (v "0.5.2") (d (list (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "1fv9bmbzq6g288g34l9yxhx6m2xqpfac7phs5f4zpsd7g576f26z")))

(define-public crate-inwelling-0.5.3 (c (n "inwelling") (v "0.5.3") (d (list (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0yffbnv6ppmvw1pnyvar3h61hgzmlrshwjnvhj1pc0zz3nf5n81i")))

(define-public crate-inwelling-0.5.4 (c (n "inwelling") (v "0.5.4") (d (list (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "1m9kpx2vqmgmddj7rlaxn7dm72n00383cdmdzgxyc6i1h7s2skrw")))

(define-public crate-inwelling-0.5.5 (c (n "inwelling") (v "0.5.5") (d (list (d (n "toml") (r "^0.7.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "135grwkcsivaxmyd7wbpcgcpjpc8s9dcm5cgraagm8gwiz394qig")))

