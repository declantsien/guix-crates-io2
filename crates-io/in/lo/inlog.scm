(define-module (crates-io in lo inlog) #:use-module (crates-io))

(define-public crate-inlog-0.1.0 (c (n "inlog") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "08vxbglpb95wgfvd1wc5mybl83c6hbkb56f2wy4naczi8av1jmbv")))

(define-public crate-inlog-0.1.1 (c (n "inlog") (v "0.1.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "05svjhm6k83hg38fz4a9973a77bl4d1x76ds3baj2a9x1fcvyxrx")))

