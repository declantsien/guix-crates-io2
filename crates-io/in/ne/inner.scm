(define-module (crates-io in ne inner) #:use-module (crates-io))

(define-public crate-inner-0.1.0 (c (n "inner") (v "0.1.0") (h "1b0x9inv3awz94bh118yq8a5w8fslwqn0xbqz86baa0sf65ckvb8") (y #t)))

(define-public crate-inner-0.1.1 (c (n "inner") (v "0.1.1") (h "1036irf6wkp3mlifar7m80d1ylfkzbf2vvbql1f1q9rg6a9qwdnr")))

