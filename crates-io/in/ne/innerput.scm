(define-module (crates-io in ne innerput) #:use-module (crates-io))

(define-public crate-innerput-0.0.1 (c (n "innerput") (v "0.0.1") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("impl-default" "winuser" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0djmy462i1yilc64k1s9k7dvw9jjmbhh4xh926y29nkxyfglcx91")))

(define-public crate-innerput-0.0.2 (c (n "innerput") (v "0.0.2") (d (list (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("impl-default" "winuser" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "16sr0xyyg3jihi1rwrvy5a2s0h2n47509xrlhxzj7c1kcbmx0jx5")))

