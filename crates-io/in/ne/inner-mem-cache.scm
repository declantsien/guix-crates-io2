(define-module (crates-io in ne inner-mem-cache) #:use-module (crates-io))

(define-public crate-inner-mem-cache-0.1.0 (c (n "inner-mem-cache") (v "0.1.0") (h "0aynxw8n95c5kg8wn5iwcmp9hvc295kpksg2vf8dirjasx13qv2h")))

(define-public crate-inner-mem-cache-0.1.1 (c (n "inner-mem-cache") (v "0.1.1") (h "1va41p5l4zymdm0z77haxkrlccs48dmr5w3gsszm7i8wgqcag8yw")))

(define-public crate-inner-mem-cache-0.1.2 (c (n "inner-mem-cache") (v "0.1.2") (h "1f57vr5r22ykrzk7iiicfam5qi1xgjn4h2xwdv0p0p64b0k0yd22")))

(define-public crate-inner-mem-cache-0.1.3 (c (n "inner-mem-cache") (v "0.1.3") (h "1aqb7al5xfs65cjqb1yvj7jbpnl9ddiva8ns11xqr71pr0bi2d0j")))

(define-public crate-inner-mem-cache-0.1.4 (c (n "inner-mem-cache") (v "0.1.4") (h "15y3d4rj80yd1an8pwq6a42ynmsqnadwr1yc2lfr6pjn72l3xsdj")))

(define-public crate-inner-mem-cache-0.1.5 (c (n "inner-mem-cache") (v "0.1.5") (h "0v94zfb37rbh01svk8al4z6d8fxxhbic876wcnf59kzwk0l8g7hm")))

(define-public crate-inner-mem-cache-0.1.6 (c (n "inner-mem-cache") (v "0.1.6") (h "0rcn3rwfsnghi82qj43kdmh74i2fzy7jl900cma7agvapj2iz8sr")))

