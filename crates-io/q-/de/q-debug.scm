(define-module (crates-io q- de q-debug) #:use-module (crates-io))

(define-public crate-q-debug-0.1.0 (c (n "q-debug") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 2)))) (h "14h0br5hhcvqaak9npivb3k17faxy5h67xlrsm4b8qxq0afizag6") (y #t)))

(define-public crate-q-debug-0.1.1 (c (n "q-debug") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 2)))) (h "1yq3p0vlb2a78ibp2a297g5zi0lkd2phfgj6khvh37v2z03ppjp7")))

