(define-module (crates-io ia st iastconvert) #:use-module (crates-io))

(define-public crate-iastconvert-0.1.0 (c (n "iastconvert") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "iasthk") (r ">=1.0.0") (d #t) (k 0)))) (h "0pasm8l2nmygy50zzgkmskhbdlqzz2lign887q84j53pnlc9j5ry")))

(define-public crate-iastconvert-0.1.1 (c (n "iastconvert") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "iasthk") (r ">=1.0.0") (d #t) (k 0)))) (h "04fdh56cfq9iw2z2wmzf11rrgwh9z5lfkx8nmcg72q1nnh4pqr67")))

(define-public crate-iastconvert-0.1.2 (c (n "iastconvert") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "iasthk") (r ">=1.0.0") (d #t) (k 0)))) (h "1hk8hwicf18jbp2z3jkkcr4lxpmi98h5ysrigqg0915nxiszy3yd")))

