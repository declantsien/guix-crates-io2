(define-module (crates-io ia st iasthk) #:use-module (crates-io))

(define-public crate-iasthk-1.0.0 (c (n "iasthk") (v "1.0.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.21") (d #t) (k 0)))) (h "0x7s69qy4slpbypnwjn5xi5g3m6kbc3hkfi2i9vscgv4f833n9cy")))

(define-public crate-iasthk-1.0.1 (c (n "iasthk") (v "1.0.1") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.21") (d #t) (k 0)))) (h "0r51l11bksp20r92r1njbazc0364vyw6xa97b1iqwbw86sfzsz13")))

