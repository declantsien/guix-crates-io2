(define-module (crates-io ia it iaith) #:use-module (crates-io))

(define-public crate-iaith-0.0.1 (c (n "iaith") (v "0.0.1") (h "1v8p05crhk5w1ynghf8l02brr3pqjj28yp86wsz1v7nj4pqslszp")))

(define-public crate-iaith-0.0.2-beta.1 (c (n "iaith") (v "0.0.2-beta.1") (h "1f9b7k25k5y4q5rd0pvqkpchvf5dajginsr4b2ka8lgvr4rjsbyl")))

(define-public crate-iaith-0.0.3-beta.1 (c (n "iaith") (v "0.0.3-beta.1") (h "0cpm233xsrb6fsi13pdy15bvv1x4xnmizxd4has4qj48vjgr9k45")))

(define-public crate-iaith-0.0.4-beta.1 (c (n "iaith") (v "0.0.4-beta.1") (h "0rkk34nxva1jdv7r1bjcimnvf5i4zxbnh2myc1rf0l0wswz99cy5")))

