(define-module (crates-io ia sc iasc) #:use-module (crates-io))

(define-public crate-iasc-0.1.0 (c (n "iasc") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nady3f47ca5jdd62w970bwq1jwv5snqhib4sn2fmxz2an73qlkr")))

