(define-module (crates-io ia ta iata-types) #:use-module (crates-io))

(define-public crate-iata-types-0.1.0 (c (n "iata-types") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "04rllqgblmsap3vjm0pjjwhgz3r7icnr23mhj3n94bprbf6hiclh")))

(define-public crate-iata-types-0.1.1 (c (n "iata-types") (v "0.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0hl5zw4w5sd0njw6rm73qbwbyj8zsh9zv96z4r46iic74z19xrcg")))

(define-public crate-iata-types-0.1.2 (c (n "iata-types") (v "0.1.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0bpjx09cnv5b7d5k64clkcdq335af8qk4bglgp9qhdxqqg4a1pl1")))

(define-public crate-iata-types-0.1.3 (c (n "iata-types") (v "0.1.3") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0mvqs8q0a0nyfsr2yizaiil034q5akiqkv3y5jz792xbnlcxyjxj")))

(define-public crate-iata-types-0.1.4 (c (n "iata-types") (v "0.1.4") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "168rbz6bx6zrwxml96r422d625xyp343myrbxna2fnmk955i9ajd")))

