(define-module (crates-io ia ta iata_bcbp) #:use-module (crates-io))

(define-public crate-iata_bcbp-0.1.0 (c (n "iata_bcbp") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0b72qq0x3gc1sni8pdkzinfx2kk2jcghqpd2v8v9xrn6wjnvqm24") (y #t)))

(define-public crate-iata_bcbp-0.1.1 (c (n "iata_bcbp") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0qh4zqlx100475lr2zhydbpsa2gi7z2by6zswgb96i9vb9y3zvlg")))

(define-public crate-iata_bcbp-1.0.0 (c (n "iata_bcbp") (v "1.0.0") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1i18687k63x2v9f15y33kdr9351zd9c98z2ba9f8ssrqgfdvcqdk")))

