(define-module (crates-io ia i_ iai_macro) #:use-module (crates-io))

(define-public crate-iai_macro-0.1.0 (c (n "iai_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "12fyx0y1by9vrh3vpkzwarpwgvhskc3bmcrjib3hp5jjh66167y9")))

(define-public crate-iai_macro-0.1.1 (c (n "iai_macro") (v "0.1.1") (d (list (d (n "iai") (r "^0.1.1") (f (quote ("macro"))) (k 2)) (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "12xx8qn2740dhxldivc2zhhvqmfb488ry1dr2qyxw1n4ps2pyli5")))

