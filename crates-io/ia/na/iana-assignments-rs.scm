(define-module (crates-io ia na iana-assignments-rs) #:use-module (crates-io))

(define-public crate-iana-assignments-rs-0.0.1 (c (n "iana-assignments-rs") (v "0.0.1") (h "004kwk5cd5nd9s312krahcq11hb1dj7am6vq1kap2291335w2g2r")))

(define-public crate-iana-assignments-rs-0.0.2 (c (n "iana-assignments-rs") (v "0.0.2") (h "0dlkry83iiwdqg362pxavm475cxhqpjz9xr4mlria3z4m39pgwh9")))

