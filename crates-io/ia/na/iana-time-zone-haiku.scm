(define-module (crates-io ia na iana-time-zone-haiku) #:use-module (crates-io))

(define-public crate-iana-time-zone-haiku-0.1.0 (c (n "iana-time-zone-haiku") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0.34") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.34") (d #t) (k 1)))) (h "1ai6i7p2mcrsf9dfp4r2pbn9xflh8nk8pjfkkqsyjqzkrvbfvrpx")))

(define-public crate-iana-time-zone-haiku-0.1.1 (c (n "iana-time-zone-haiku") (v "0.1.1") (d (list (d (n "cxx") (r "^1.0.34") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.34") (d #t) (k 1)))) (h "1jix9qrqxclj9r4wkg7d3fr987d77vdg3qy2c5hl4ry19wlaw0q7")))

(define-public crate-iana-time-zone-haiku-0.1.2 (c (n "iana-time-zone-haiku") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "17r6jmj31chn7xs9698r122mapq85mfnv98bb4pg6spm0si2f67k")))

