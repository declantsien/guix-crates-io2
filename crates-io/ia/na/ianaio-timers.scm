(define-module (crates-io ia na ianaio-timers) #:use-module (crates-io))

(define-public crate-ianaio-timers-0.1.0 (c (n "ianaio-timers") (v "0.1.0") (d (list (d (n "futures-channel") (r "^0.3.30") (o #t) (d #t) (k 0)) (d (n "futures-core") (r "^0.3.30") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 2)) (d (n "js-sys") (r "^0.3.68") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.91") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.41") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3.41") (d #t) (k 2)))) (h "1lqjla5kldnycmmyq2isqjnvm1pxvzhi4ssh8zq2ww16g642w7gw") (f (quote (("futures" "futures-core" "futures-channel") ("default")))) (r "1.78.0")))

