(define-module (crates-io ia na iana-media-types) #:use-module (crates-io))

(define-public crate-iana-media-types-0.1.1 (c (n "iana-media-types") (v "0.1.1") (h "03b9w5q7mj0nxn6bvd0mx37s0761p1mmaxpyw7ksw58fhh1xdqx1")))

(define-public crate-iana-media-types-0.1.2 (c (n "iana-media-types") (v "0.1.2") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 2)))) (h "0b98zlb1kj91n0x4kpkiywgni3bfhfkj7m4bsk6lmqp52zpx747x")))

