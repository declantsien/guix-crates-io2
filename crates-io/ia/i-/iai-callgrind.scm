(define-module (crates-io ia i- iai-callgrind) #:use-module (crates-io))

(define-public crate-iai-callgrind-0.1.0 (c (n "iai-callgrind") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1s6iz0whasl9mbfb6nl593l69qwcj93p9wvpks1m4ci250wzsss0") (r "1.56.0")))

(define-public crate-iai-callgrind-0.2.0 (c (n "iai-callgrind") (v "0.2.0") (h "0qfj5f5m3lmmnfchsgmamkn9dm537bnjkxq276lm32x97689hpxy") (r "1.56.0")))

(define-public crate-iai-callgrind-0.3.0 (c (n "iai-callgrind") (v "0.3.0") (h "0l6rc91icdipfz92fgsbskpqya66gvddkf9zb86cs835hzxs2rxb") (r "1.56.0")))

(define-public crate-iai-callgrind-0.3.1 (c (n "iai-callgrind") (v "0.3.1") (h "1hm8wn3hlw36ny3p1hh30xi8r2ck9xq81mvr8wvdmmc3ldi9fmvn") (r "1.56.0")))

(define-public crate-iai-callgrind-0.4.0 (c (n "iai-callgrind") (v "0.4.0") (h "17qcm3l4iamzdz7vynmnqbvm4df0qsf6z3flda4za67ncc01xx2z") (r "1.60.0")))

(define-public crate-iai-callgrind-0.5.0 (c (n "iai-callgrind") (v "0.5.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.177") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sr77n986a209k4f8iv3s62h7rmifgkkmsg8nawxm86vjjmz2i14") (r "1.60.0")))

(define-public crate-iai-callgrind-0.6.0 (c (n "iai-callgrind") (v "0.6.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "iai-callgrind-runner") (r "=0.6.0") (f (quote ("api"))) (k 0)) (d (n "serde") (r "^1.0.177") (f (quote ("derive"))) (d #t) (k 0)))) (h "07zxb6ky13pkg0xra7chmb8l5km0blf4i42b4pgdzyfzk75gfwbz") (r "1.60.0")))

(define-public crate-iai-callgrind-0.6.1 (c (n "iai-callgrind") (v "0.6.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "iai-callgrind-runner") (r "=0.6.1") (f (quote ("api"))) (k 0)) (d (n "serde") (r "^1.0.177") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c3ry1ms5a2mjni5qfk8l9gqmvkznfcqdi2fhsxdqn2qw10q8nvj") (r "1.60.0")))

(define-public crate-iai-callgrind-0.6.2 (c (n "iai-callgrind") (v "0.6.2") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "iai-callgrind-runner") (r "=0.6.2") (f (quote ("api"))) (k 0)))) (h "0b3nbi9wqbsv2s2j8737kjq2116kblqj3mqmc127xa1b2h4idihv") (r "1.60.0")))

(define-public crate-iai-callgrind-0.7.0 (c (n "iai-callgrind") (v "0.7.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "iai-callgrind-macros") (r "=0.1.0") (d #t) (k 0)) (d (n "iai-callgrind-runner") (r "=0.7.0") (f (quote ("api"))) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1y195x58skncygcvvdcm04jn4jc78ll7xllncjvrq4pmnknqrjj9") (f (quote (("ui_tests") ("default")))) (r "1.60.0")))

(define-public crate-iai-callgrind-0.7.1 (c (n "iai-callgrind") (v "0.7.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "iai-callgrind-macros") (r "=0.1.0") (d #t) (k 0)) (d (n "iai-callgrind-runner") (r "=0.7.1") (f (quote ("api"))) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0wwgw409hiclpz1q6pk2wvagffr6qk13xx1vn9zary42lrdvz4aw") (f (quote (("ui_tests") ("default")))) (r "1.60.0")))

(define-public crate-iai-callgrind-0.7.2 (c (n "iai-callgrind") (v "0.7.2") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "iai-callgrind-macros") (r "=0.1.0") (d #t) (k 0)) (d (n "iai-callgrind-runner") (r "=0.7.2") (f (quote ("api"))) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0igyznj183nj1xj0gwvds34lhp3sqh3sgz3rix4his7njicrqjd0") (f (quote (("ui_tests") ("default")))) (r "1.60.0")))

(define-public crate-iai-callgrind-0.7.3 (c (n "iai-callgrind") (v "0.7.3") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "iai-callgrind-macros") (r "=0.1.0") (d #t) (k 0)) (d (n "iai-callgrind-runner") (r "=0.7.3") (f (quote ("api"))) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1lc2ahn8r3yai9404ycvxv7vqfgpwask2fhzbfs34llz79cdwy96") (f (quote (("ui_tests") ("default")))) (r "1.60.0")))

(define-public crate-iai-callgrind-0.8.0 (c (n "iai-callgrind") (v "0.8.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "iai-callgrind-macros") (r "=0.1.0") (d #t) (k 0)) (d (n "iai-callgrind-runner") (r "=0.8.0") (f (quote ("api"))) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1z3zir8mhmlndgrfdpnd6y4bj9sz97w6hff5rwlvi89sq5n73axi") (f (quote (("ui_tests") ("default")))) (r "1.60.0")))

(define-public crate-iai-callgrind-0.9.0 (c (n "iai-callgrind") (v "0.9.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "iai-callgrind-macros") (r "=0.1.0") (d #t) (k 0)) (d (n "iai-callgrind-runner") (r "=0.9.0") (f (quote ("api"))) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1xzkpyfwjlp29q00rv9s3s11zxzwck1214w202wbhfwnmsq6ggby") (f (quote (("ui_tests") ("default")))) (r "1.60.0")))

(define-public crate-iai-callgrind-0.10.0 (c (n "iai-callgrind") (v "0.10.0") (d (list (d (n "bincode") (r "^1") (o #t) (d #t) (k 0)) (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cfg-if") (r "^1") (o #t) (d #t) (k 0)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "iai-callgrind-macros") (r "=0.2.0") (o #t) (d #t) (k 0)) (d (n "iai-callgrind-runner") (r "=0.10.0") (f (quote ("api"))) (o #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "trybuild") (r "^1.0.18") (d #t) (k 2)))) (h "0r28wd92jsg9dcw82j1wqvzqpl1wmkzjkf53f7j9jcnyi0f4vgcx") (f (quote (("ui_tests") ("default" "benchmark") ("client_requests" "client_requests_defs")))) (s 2) (e (quote (("client_requests_defs" "dep:cty" "dep:cfg-if") ("benchmark" "dep:bincode" "dep:iai-callgrind-macros" "dep:iai-callgrind-runner")))) (r "1.66.0")))

(define-public crate-iai-callgrind-0.10.1 (c (n "iai-callgrind") (v "0.10.1") (d (list (d (n "bincode") (r "^1") (o #t) (d #t) (k 0)) (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cfg-if") (r "^1") (o #t) (d #t) (k 0)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "iai-callgrind-macros") (r "=0.2.0") (o #t) (d #t) (k 0)) (d (n "iai-callgrind-runner") (r "=0.10.1") (f (quote ("api"))) (o #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "trybuild") (r "^1.0.18") (d #t) (k 2)))) (h "16rk83y7pzxj9qvhlr6319ny38019c0jrh5raw1v16vy31vmn56y") (f (quote (("ui_tests") ("default" "benchmark") ("client_requests" "client_requests_defs")))) (s 2) (e (quote (("client_requests_defs" "dep:cty" "dep:cfg-if") ("benchmark" "dep:bincode" "dep:iai-callgrind-macros" "dep:iai-callgrind-runner")))) (r "1.66.0")))

(define-public crate-iai-callgrind-0.10.2 (c (n "iai-callgrind") (v "0.10.2") (d (list (d (n "bincode") (r "^1") (o #t) (d #t) (k 0)) (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cfg-if") (r "^1") (o #t) (d #t) (k 0)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "iai-callgrind-macros") (r "=0.2.0") (o #t) (d #t) (k 0)) (d (n "iai-callgrind-runner") (r "=0.10.2") (f (quote ("api"))) (o #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "trybuild") (r "^1.0.18") (d #t) (k 2)))) (h "107w5wpsvajdzd8cx282zgxk78j6vapl0kq1fdiaq4vb95pz56z9") (f (quote (("ui_tests") ("default" "benchmark") ("client_requests" "client_requests_defs")))) (s 2) (e (quote (("client_requests_defs" "dep:cty" "dep:cfg-if") ("benchmark" "dep:bincode" "dep:iai-callgrind-macros" "dep:iai-callgrind-runner")))) (r "1.66.0")))

(define-public crate-iai-callgrind-0.11.0 (c (n "iai-callgrind") (v "0.11.0") (d (list (d (n "bincode") (r "^1") (o #t) (d #t) (k 0)) (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cfg-if") (r "^1") (o #t) (d #t) (k 0)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "iai-callgrind-macros") (r "=0.2.0") (o #t) (d #t) (k 0)) (d (n "iai-callgrind-runner") (r "=0.11.0") (f (quote ("api"))) (o #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)) (d (n "trybuild") (r "^1.0.18") (d #t) (k 2)))) (h "00x9a8zdc0ravzb561icsf6dh87vcb7xglxmainjlh8jqac0qy3v") (f (quote (("ui_tests") ("default" "benchmark") ("client_requests" "client_requests_defs")))) (s 2) (e (quote (("client_requests_defs" "dep:cty" "dep:cfg-if") ("benchmark" "dep:bincode" "dep:iai-callgrind-macros" "dep:iai-callgrind-runner")))) (r "1.66.0")))

