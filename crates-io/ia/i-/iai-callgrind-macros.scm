(define-module (crates-io ia i- iai-callgrind-macros) #:use-module (crates-io))

(define-public crate-iai-callgrind-macros-0.1.0 (c (n "iai-callgrind-macros") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1.0.60, <2") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r ">=2.0.1, <3") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yqny5ks38993is7ad2423826cvx6k32jqqgiixhjl73hmmzcnmg") (r "1.60.0")))

(define-public crate-iai-callgrind-macros-0.2.0 (c (n "iai-callgrind-macros") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^1.1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.1") (f (quote ("full" "extra-traits" "full" "extra-traits"))) (d #t) (k 0)))) (h "0mid4pxc2a9k1bfjqg2k7yi8vg9fyl5gg9pwijic14jm48wvp972") (r "1.66.0")))

