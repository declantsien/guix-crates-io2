(define-module (crates-io ia i- iai-parse) #:use-module (crates-io))

(define-public crate-iai-parse-0.1.0 (c (n "iai-parse") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "git2") (r "^0.16.0") (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "19hfkaf86zdnx0ksfmx23nf1f4fpbvnl0a7lw2gnyd4cpsxm8lbf")))

(define-public crate-iai-parse-0.1.1 (c (n "iai-parse") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "git2") (r "^0.16.0") (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1x9pba19mfbh25v018s5bf6pbxpbpscl12m221vjps1v4r7fcqw0")))

(define-public crate-iai-parse-0.1.2 (c (n "iai-parse") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "git2") (r "^0.17.2") (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1a6lr0xpz363sl1a8wdm94gysmshp19pswh63v1wkja5knhg0807") (r "1.64")))

(define-public crate-iai-parse-0.1.3 (c (n "iai-parse") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "git2") (r "^0.18.2") (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1dnhhx6s7z14g4s3qjm1dbb9q2qv6h4lhrnzmax8cs0drs5yvdkv") (r "1.64")))

