(define-module (crates-io ia q- iaq-core) #:use-module (crates-io))

(define-public crate-iaq-core-0.1.0 (c (n "iaq-core") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "nb") (r "^0.1") (d #t) (k 0)))) (h "1ihx8n1i6afagyjb7jc66wapnq1rk0ca3gk4yw80jm3q23syv9hh")))

