(define-module (crates-io jp l- jpl-sys) #:use-module (crates-io))

(define-public crate-jpl-sys-0.0.1 (c (n "jpl-sys") (v "0.0.1") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)))) (h "0lqiplvq2sxcz8ylr8vlba1nyimf1rzcsj208mk0rhqs33ya1zqx") (y #t) (l "jpl")))

(define-public crate-jpl-sys-0.0.2 (c (n "jpl-sys") (v "0.0.2") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)))) (h "0m4wskz3aylm7bwslbajkz2r5vi74xs968jc9qr03r18y81g89jg") (l "jpl")))

