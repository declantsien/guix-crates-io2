(define-module (crates-io jp st jpst) #:use-module (crates-io))

(define-public crate-jpst-0.1.0 (c (n "jpst") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "jsonpath-plus") (r "^0.1.9") (d #t) (k 0)) (d (n "once_cell") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ji85a334cwm306pljc22y3brim7spbbsrzxb9x1bf2nj51ykciv")))

(define-public crate-jpst-0.1.1 (c (n "jpst") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "jsonpath-plus") (r "^0.1.9") (d #t) (k 0)) (d (n "once_cell") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1aiwplcvn9i9dky83wlpsviks5h9vj5n3nim3lbgn3ylsmmimdpy")))

