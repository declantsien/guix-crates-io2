(define-module (crates-io jp _p jp_partition) #:use-module (crates-io))

(define-public crate-jp_partition-0.1.0 (c (n "jp_partition") (v "0.1.0") (d (list (d (n "multimap") (r "^0.1.0") (d #t) (k 0) (p "jp_multimap")) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0v92ddsc3g63dywdlvn55jn8zsac9ni3aifpbxf1xdvxlk3jkc6b")))

