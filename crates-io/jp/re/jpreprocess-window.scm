(define-module (crates-io jp re jpreprocess-window) #:use-module (crates-io))

(define-public crate-jpreprocess-window-0.1.0 (c (n "jpreprocess-window") (v "0.1.0") (h "005kafq4wb39gb36rl4yisyw9fkv4pkwvlliqh52qb6c9wm2i6n4")))

(define-public crate-jpreprocess-window-0.1.1 (c (n "jpreprocess-window") (v "0.1.1") (h "0fvgb6w9202fx3svmyhv1nl8yykkz32x6p9j7l0y91w2mfq0rwn9")))

(define-public crate-jpreprocess-window-0.2.0 (c (n "jpreprocess-window") (v "0.2.0") (h "0m1wn23hnv55l1mxjc8v6kcsd6sa72di6zsfs8ks7b6vimg9a347") (r "1.65.0")))

(define-public crate-jpreprocess-window-0.3.0 (c (n "jpreprocess-window") (v "0.3.0") (h "0ap6kbg9zvnvywp2rzmzwvl3hci401r8w6iaqabqa2l6ka4h4s7m") (r "1.65.0")))

(define-public crate-jpreprocess-window-0.4.0 (c (n "jpreprocess-window") (v "0.4.0") (h "0drghnnky7zcg39ryh5ci6h457vvm3q8z3daay4r8x59lwcb80jq") (r "1.65.0")))

(define-public crate-jpreprocess-window-0.5.0 (c (n "jpreprocess-window") (v "0.5.0") (h "083nsl2xp5vr5j59s44qa4m9fcha0ai2c2qgbpcypgmxqldqy4iv") (r "1.65.0")))

(define-public crate-jpreprocess-window-0.5.1 (c (n "jpreprocess-window") (v "0.5.1") (h "0xrvs8ia2n7kgg5xc734r4d656zqhndlalfdacy5cdmaw6774ibw") (r "1.65.0")))

(define-public crate-jpreprocess-window-0.6.0 (c (n "jpreprocess-window") (v "0.6.0") (h "1dip2361phg8dmn87q9vnfcp2igsyw0n00hn9a9q8s3m60cq0sy5") (r "1.65.0")))

(define-public crate-jpreprocess-window-0.6.1 (c (n "jpreprocess-window") (v "0.6.1") (h "18dmmvid1hdm31cdn2qm9k17iyy8m8lkpf8z7y4ndzx858h69kfn") (r "1.65.0")))

(define-public crate-jpreprocess-window-0.6.2 (c (n "jpreprocess-window") (v "0.6.2") (h "03662hng3rxfhf6fhf2cspgzx5d1jghj54x784gd6sbqvwd1mbp2") (r "1.65.0")))

(define-public crate-jpreprocess-window-0.6.3 (c (n "jpreprocess-window") (v "0.6.3") (h "1152q6h7jsjh68v7zc1xb0whxvfd0gkn11w528s4cmkf24w8fc8a") (r "1.65.0")))

(define-public crate-jpreprocess-window-0.7.0 (c (n "jpreprocess-window") (v "0.7.0") (h "11ipwp5aarhvx61ky0brqminm73rqdaag8ranr50l6h3ijj1f2nq") (r "1.65.0")))

(define-public crate-jpreprocess-window-0.8.0 (c (n "jpreprocess-window") (v "0.8.0") (h "121rya710cm51zcf0h783rs70pjmridfrbblkplkvy47kcvh86wl") (r "1.71.1")))

(define-public crate-jpreprocess-window-0.8.1 (c (n "jpreprocess-window") (v "0.8.1") (h "19q2mmh6bqcl0zag78h34a36y6c5l4i9v2yqsvmg9azz841hijdp") (r "1.73.0")))

(define-public crate-jpreprocess-window-0.9.0 (c (n "jpreprocess-window") (v "0.9.0") (h "1s5d90fii0mfb1n6nlfdy222r47y9vdkqb4h0r97f92wf0378d4n") (r "1.73.0")))

(define-public crate-jpreprocess-window-0.9.1 (c (n "jpreprocess-window") (v "0.9.1") (h "049zd49zypsspvql16h1r1q8rfqpics35sg8xy96cwl0jjgmlajl") (r "1.73.0")))

