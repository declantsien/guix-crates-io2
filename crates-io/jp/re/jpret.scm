(define-module (crates-io jp re jpret) #:use-module (crates-io))

(define-public crate-jpret-0.1.0 (c (n "jpret") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "json-color") (r "^0.7.1") (d #t) (k 0)) (d (n "linkify") (r "^0.4.0") (d #t) (k 0)))) (h "0gr2dp9km3a6c0x3prqs2fi6qfrfss57xg67q8nrg28x641fkx1s") (y #t)))

(define-public crate-jpret-0.1.1 (c (n "jpret") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "json-color") (r "^0.7.1") (d #t) (k 0)) (d (n "linkify") (r "^0.4.0") (d #t) (k 0)))) (h "0wngw09qbgi408gy802afnmd351jns7df8215xr310hsr0g7vf5l")))

