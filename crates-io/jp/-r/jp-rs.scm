(define-module (crates-io jp -r jp-rs) #:use-module (crates-io))

(define-public crate-jp-rs-0.1.0 (c (n "jp-rs") (v "0.1.0") (d (list (d (n "pa-rs") (r "^0.1.1") (d #t) (k 0)))) (h "0nnm5krff400x7ildf9krmc9k9kmxcnfjrqmq2f39ckn5vq6fjpj")))

(define-public crate-jp-rs-0.1.1 (c (n "jp-rs") (v "0.1.1") (d (list (d (n "pa-rs") (r "^0.1.1") (d #t) (k 0)))) (h "1sm4gl1dbja7my36sjb0hyb6fa41dmasnirp92jmglhi3zcb7dky")))

(define-public crate-jp-rs-0.1.2 (c (n "jp-rs") (v "0.1.2") (d (list (d (n "pa-rs") (r "^0.1.2") (d #t) (k 0)))) (h "150w90mbszs3y5vgnrlh7229sx74w3aals1s5avk6zw6wd8lr5pb")))

(define-public crate-jp-rs-0.1.3 (c (n "jp-rs") (v "0.1.3") (d (list (d (n "pa-rs") (r "^0.1.4") (d #t) (k 0)))) (h "05nxqzd4l98gfy0pnl2nzlrq2a30lqp9w2y04dkxbhhc44dpy6q5")))

(define-public crate-jp-rs-0.1.4 (c (n "jp-rs") (v "0.1.4") (d (list (d (n "pa-rs") (r "^0.1.5") (d #t) (k 0)))) (h "0nw8nj2b1ik76nnc9ca6imjp4l7kiw31snfi9qwn2hqzfvxbqajg")))

