(define-module (crates-io jp ki jpki) #:use-module (crates-io))

(define-public crate-jpki-0.1.0 (c (n "jpki") (v "0.1.0") (d (list (d (n "ring") (r "^0.17.0-alpha.10") (d #t) (k 0)) (d (n "x509-certificate") (r "^0.4") (d #t) (k 0)) (d (n "yasna") (r "^0.4") (d #t) (k 0)))) (h "08rml952jvgjv997n3s68sch0m6l2ahdy02ikqqg93chsqs98ljw")))

(define-public crate-jpki-0.1.1 (c (n "jpki") (v "0.1.1") (d (list (d (n "ring") (r "^0.17.0-alpha.10") (d #t) (k 0)) (d (n "x509-certificate") (r "^0.4") (d #t) (k 0)) (d (n "yasna") (r "^0.4") (d #t) (k 0)))) (h "1ynj5bcqrxgwf8ganczzi9i01q4iqk8zlfqxzs72c0kihqw8hjpg")))

(define-public crate-jpki-0.1.2 (c (n "jpki") (v "0.1.2") (d (list (d (n "ring") (r "^0.17.0-alpha.10") (d #t) (k 0)) (d (n "x509-certificate") (r "^0.4") (d #t) (k 0)) (d (n "yasna") (r "^0.4") (d #t) (k 0)))) (h "17ifxqlwqwaifys0cgydbc7wmmajhbsx53i1q57j7dy0s7h12pnc") (y #t)))

(define-public crate-jpki-0.1.3 (c (n "jpki") (v "0.1.3") (d (list (d (n "ring") (r "^0.17.0-alpha.10") (d #t) (k 0)) (d (n "x509-certificate") (r "^0.4") (d #t) (k 0)) (d (n "yasna") (r "^0.4") (d #t) (k 0)))) (h "1q06jwgxl5hsw5x9b03rw8jr6msdp1fz13620pc43pdwhksi324f") (y #t)))

(define-public crate-jpki-0.1.4 (c (n "jpki") (v "0.1.4") (d (list (d (n "ring") (r "^0.17.0-alpha.10") (d #t) (k 0)) (d (n "x509-certificate") (r "^0.4") (d #t) (k 0)) (d (n "yasna") (r "^0.4") (d #t) (k 0)))) (h "07ky44bfyypv9ijww1f8l33q41929zzb8gfr84406c3732pj786w") (y #t)))

(define-public crate-jpki-0.1.5 (c (n "jpki") (v "0.1.5") (d (list (d (n "ring") (r "^0.17.0-alpha.10") (d #t) (k 0)) (d (n "x509-certificate") (r "^0.4") (d #t) (k 0)) (d (n "yasna") (r "^0.4") (d #t) (k 0)))) (h "0jr888fb01i6laayqnfdn0qgmf46x1w7f2wpp88gflm8c3ai9xkn")))

(define-public crate-jpki-0.1.6 (c (n "jpki") (v "0.1.6") (d (list (d (n "ring") (r "^0.17.0-alpha.10") (d #t) (k 0)) (d (n "x509-certificate") (r "^0.4") (d #t) (k 0)) (d (n "yasna") (r "^0.4") (d #t) (k 0)))) (h "1lrzjr9p76b07jharjq9z5nlbq52jald7zyqdgiz0y29m4nmmw7g")))

(define-public crate-jpki-0.1.7 (c (n "jpki") (v "0.1.7") (d (list (d (n "ring") (r "^0.17.0-alpha.10") (d #t) (k 0)) (d (n "x509-certificate") (r "^0.4") (d #t) (k 0)) (d (n "yasna") (r "^0.4") (d #t) (k 0)))) (h "1pz0rgqs6qzs2ix9f5019xvhfhqsbnih7kgh0wv8jvq0nn8d2rgq") (y #t)))

(define-public crate-jpki-0.1.8 (c (n "jpki") (v "0.1.8") (d (list (d (n "ring") (r "^0.17.0-alpha.10") (d #t) (k 0)) (d (n "x509-certificate") (r "^0.4") (d #t) (k 0)) (d (n "yasna") (r "^0.4") (d #t) (k 0)))) (h "1dsfza1y6i9q3z82lasykad2hjhgiaqi3gjf32qgqdif3kp99dsa")))

(define-public crate-jpki-0.2.0 (c (n "jpki") (v "0.2.0") (d (list (d (n "ring") (r "^0.17.0-alpha.10") (o #t) (d #t) (k 0)) (d (n "x509-certificate") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "yasna") (r "^0.4") (o #t) (d #t) (k 0)))) (h "00nk1fdq51biz49xbxf77hrmf09vkvkdskwdcl34l64qlivirfw7") (f (quote (("digest" "ring" "yasna" "x509-certificate"))))))

(define-public crate-jpki-0.2.1 (c (n "jpki") (v "0.2.1") (d (list (d (n "ring") (r "^0.17.0-alpha.10") (o #t) (d #t) (k 0)) (d (n "x509-certificate") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "yasna") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1zflsrlzvlq7qmd2fz3cifs38sh0a0vaw1ja8z527ihq5l8pld7n") (f (quote (("digest" "ring" "yasna" "x509-certificate"))))))

(define-public crate-jpki-0.2.2 (c (n "jpki") (v "0.2.2") (d (list (d (n "apdu") (r "^0.2") (d #t) (k 0)) (d (n "ring") (r "^0.17.0-alpha.10") (o #t) (d #t) (k 0)) (d (n "x509-certificate") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "yasna") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0gljby1whfbnbwpz6113b426s7n26z3b11675vj6swil0dri58d8") (f (quote (("digest" "ring" "yasna" "x509-certificate"))))))

(define-public crate-jpki-0.3.0 (c (n "jpki") (v "0.3.0") (d (list (d (n "apdu") (r "^0.2") (d #t) (k 0)) (d (n "ring") (r "^0.17.0-alpha.10") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "x509-certificate") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "yasna") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1ax6m5b6j6mx40qyycyr790x6w7skslxa5iw2p5zwyns58qrsbwc") (f (quote (("digest" "ring" "yasna" "x509-certificate"))))))

(define-public crate-jpki-0.4.0 (c (n "jpki") (v "0.4.0") (d (list (d (n "apdu") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "pcsc") (r "^2.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1wkiixrsj5by57xxh7124wsna5q30mvhfwal0gni1ncyvhr87yl7") (f (quote (("default")))) (s 2) (e (quote (("tracing" "dep:tracing") ("pcsc" "dep:pcsc" "hex" "thiserror"))))))

(define-public crate-jpki-0.4.1 (c (n "jpki") (v "0.4.1") (d (list (d (n "apdu") (r "^0.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "pcsc") (r "^2.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "11q6zkgsdbm8kfgfr9fsch5ycamyhpqhkpa9yp18rwaiwjpayvkv") (f (quote (("default")))) (s 2) (e (quote (("tracing" "dep:tracing") ("pcsc" "dep:pcsc" "hex" "thiserror"))))))

(define-public crate-jpki-0.4.2 (c (n "jpki") (v "0.4.2") (d (list (d (n "apdu") (r "^0.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "pcsc") (r "^2.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0nalx0cszxs276rv22v3afsx92n4hl09x25dqg0g32fmn1nkmvqr") (f (quote (("default")))) (s 2) (e (quote (("tracing" "dep:tracing") ("pcsc" "dep:pcsc" "hex"))))))

(define-public crate-jpki-0.4.3 (c (n "jpki") (v "0.4.3") (d (list (d (n "apdu") (r "^0.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "pcsc") (r "^2.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "089kny69ilib47sv12zy5nsf3abn8n7d5194r4qwg9vjmwlrn8lp") (f (quote (("default")))) (s 2) (e (quote (("tracing" "dep:tracing") ("pcsc" "dep:pcsc" "hex"))))))

