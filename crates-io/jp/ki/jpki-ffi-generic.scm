(define-module (crates-io jp ki jpki-ffi-generic) #:use-module (crates-io))

(define-public crate-jpki-ffi-generic-0.4.0 (c (n "jpki-ffi-generic") (v "0.4.0") (d (list (d (n "cbindgen") (r "^0.24") (d #t) (k 1)) (d (n "jpki") (r "=0.4.0") (d #t) (k 0)))) (h "1p42y1im2yd8zqnyz3yaxhgk76jsmy9bdc36kl7bvacxn8w2dsgs")))

(define-public crate-jpki-ffi-generic-0.4.1 (c (n "jpki-ffi-generic") (v "0.4.1") (d (list (d (n "cbindgen") (r "^0.24") (d #t) (k 1)) (d (n "jpki") (r "=0.4.1") (d #t) (k 0)))) (h "1g7lz4w4nvhhs502cq77ssyhgm8ajgac947bclh5j9fvslm6g41q")))

(define-public crate-jpki-ffi-generic-0.4.3 (c (n "jpki-ffi-generic") (v "0.4.3") (d (list (d (n "cbindgen") (r "^0.24") (d #t) (k 1)) (d (n "jpki") (r "=0.4.3") (d #t) (k 0)))) (h "1nwrzy824cc6922l2nv4xg2xz2pfibnbqqbj58z689fhqbbqak17")))

