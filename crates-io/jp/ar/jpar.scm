(define-module (crates-io jp ar jpar) #:use-module (crates-io))

(define-public crate-jpar-0.1.0 (c (n "jpar") (v "0.1.0") (d (list (d (n "bytecount") (r "^0.6.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "memchr") (r "^2.3.4") (d #t) (k 0)))) (h "0lf0x7ani4kxp05cs0cwghskqwmvv9iqij5kq7s80xvci53zv9dj") (f (quote (("alloc"))))))

