(define-module (crates-io jp ar jpark011-guessing_game) #:use-module (crates-io))

(define-public crate-jpark011-guessing_game-0.1.0 (c (n "jpark011-guessing_game") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1a7kn558xs51zwdp1883mjnw3mp94jkjkyv55nl21zk9qmwjjkjp")))

(define-public crate-jpark011-guessing_game-0.1.2 (c (n "jpark011-guessing_game") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "07wrk8glr8nc4yj3fvmrp7h7shy1xm1qg5b46ixp9vk09ki575vp") (y #t)))

(define-public crate-jpark011-guessing_game-1.0.0 (c (n "jpark011-guessing_game") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.1") (d #t) (k 0)))) (h "0h8h487kfh5cf5imyyg7bm5qs7xcq9y60q7kcysb8rmnkzbffbf7")))

(define-public crate-jpark011-guessing_game-1.1.0 (c (n "jpark011-guessing_game") (v "1.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.1") (d #t) (k 0)))) (h "1h7r14mw49g3apy96mjyw2nw68wbsyscgjg9css0ij97w6b8f5pm")))

