(define-module (crates-io jp ng jpng) #:use-module (crates-io))

(define-public crate-jpng-0.1.0 (c (n "jpng") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.19") (f (quote ("jpeg" "jpeg_rayon" "png_codec"))) (d #t) (k 0)))) (h "0fcgz4sspwfmf4zdxc5cwfs3wrslv47m42jq70f4j3vbls07vf52")))

