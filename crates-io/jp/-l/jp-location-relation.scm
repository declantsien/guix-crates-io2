(define-module (crates-io jp -l jp-location-relation) #:use-module (crates-io))

(define-public crate-jp-location-relation-0.1.0 (c (n "jp-location-relation") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1l266bw2gm2gvsc8qqq9wg7znimfcqag3cm5gy7a3mcc6i5daj6j")))

(define-public crate-jp-location-relation-0.1.1 (c (n "jp-location-relation") (v "0.1.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0pg5ys96gxi569xgmww7fnkby5v5gabcm16qgqj9nmhyvc3zl45j")))

