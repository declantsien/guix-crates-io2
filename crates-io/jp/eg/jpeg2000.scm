(define-module (crates-io jp eg jpeg2000) #:use-module (crates-io))

(define-public crate-jpeg2000-0.1.0 (c (n "jpeg2000") (v "0.1.0") (d (list (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "openjpeg2-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1sn2j0nply1kh6gi5459x9vc9809qcq1y23500p701m5zb0n7j08")))

(define-public crate-jpeg2000-0.1.1 (c (n "jpeg2000") (v "0.1.1") (d (list (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "openjpeg2-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "slog") (r "^2.0") (d #t) (k 0)) (d (n "slog-async") (r "^2.2") (d #t) (k 2)) (d (n "slog-term") (r "^2.3") (d #t) (k 2)))) (h "04zh7ni7qxi6lvb94d1nxpzn32ai24gm1slvk76rzg8ps45gsrfj")))

(define-public crate-jpeg2000-0.2.0 (c (n "jpeg2000") (v "0.2.0") (d (list (d (n "image") (r "^0.20") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "openjpeg2-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "slog") (r "^2.0") (d #t) (k 0)) (d (n "slog-async") (r "^2.2") (d #t) (k 2)) (d (n "slog-term") (r "^2.3") (d #t) (k 2)))) (h "0y2xp0a1773fi5ps42d27mlbmmybji55i3slgq723p9drlv05s74")))

(define-public crate-jpeg2000-0.3.0 (c (n "jpeg2000") (v "0.3.0") (d (list (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "libc") (r "^0.2.34") (d #t) (k 0)) (d (n "openjpeg2-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "slog") (r "^2.0") (d #t) (k 0)) (d (n "slog-async") (r "^2.2") (d #t) (k 2)) (d (n "slog-term") (r "^2.3") (d #t) (k 2)))) (h "01cvxhk5z25q78ljyr5dly1lbjn9c9ikwx5rvf0zh4nz9yn7cp0v")))

