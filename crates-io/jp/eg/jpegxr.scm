(define-module (crates-io jp eg jpegxr) #:use-module (crates-io))

(define-public crate-jpegxr-0.1.0 (c (n "jpegxr") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.67") (d #t) (k 1)) (d (n "libc") (r "^0.2.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19qpyxbz8anqpl17167g2rvmggspcbfnn327kx1ycgjqv0564xqc")))

(define-public crate-jpegxr-0.2.0 (c (n "jpegxr") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.67") (d #t) (k 1)) (d (n "libc") (r "^0.2.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0kcharpsq6h1h1lxab5ly4j6v09nb5m7bs2qrbpvxlwlx1s5wdlz")))

(define-public crate-jpegxr-0.2.1 (c (n "jpegxr") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.67") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0r9ba4d42llk6f2l98gljk0j0fgxmvcz84sln6lyvwrgnbvp3dww")))

(define-public crate-jpegxr-0.2.2 (c (n "jpegxr") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.67") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1sh90w80rbb4sxwhhnjavsjjxwqi7b9lzgh8zzln3vsqc2k9q5ng")))

(define-public crate-jpegxr-0.2.3 (c (n "jpegxr") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.67") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14fjzmw33zjvj2cxxsqrnag5g5rmgbch45va6cy0fzyqzk4sqdbb")))

(define-public crate-jpegxr-0.2.4 (c (n "jpegxr") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.67") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13yi72rn9zhfw9x4dmxv3ikn5hnh5lnk7rlbn0myp2naq2jl4sa5")))

(define-public crate-jpegxr-0.2.5 (c (n "jpegxr") (v "0.2.5") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.67") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05y39wqmb3p94xgc5y2ckacvr6dby69p511djqg5y7dxvswbv215")))

(define-public crate-jpegxr-0.2.6 (c (n "jpegxr") (v "0.2.6") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.67") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rymzckc3d5wi9gp4ndb64zfhp86ncaawl17pa9mvhdl9ff3gldk") (f (quote (("defaults") ("bin"))))))

(define-public crate-jpegxr-0.3.1 (c (n "jpegxr") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1gqgi5y5h9nld9xp61sb7zzp98zgvam2xdw0bl1ijka88n635810")))

