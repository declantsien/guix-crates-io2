(define-module (crates-io jp eg jpeg_extractor) #:use-module (crates-io))

(define-public crate-jpeg_extractor-0.1.0 (c (n "jpeg_extractor") (v "0.1.0") (h "0s9ajfxhd194r3dn3b9is38k2z921xk0ffx030rf1fcxamflxlhr")))

(define-public crate-jpeg_extractor-0.1.1 (c (n "jpeg_extractor") (v "0.1.1") (h "0ci63inlmy9qcswj89dwdlmpvagmay3m5yjcg4z6ifbk8x4vxl9f")))

(define-public crate-jpeg_extractor-0.1.2 (c (n "jpeg_extractor") (v "0.1.2") (h "1fbrvijs4c0gl40qgp9y4018vlyfj4vm9rq38n6hp36ira71jadc") (y #t)))

(define-public crate-jpeg_extractor-0.1.3 (c (n "jpeg_extractor") (v "0.1.3") (h "1sc52zabgv9jns10ail4klgv466x2bx3qmpd3i198h8a6f4rn1fz")))

