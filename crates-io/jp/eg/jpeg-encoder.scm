(define-module (crates-io jp eg jpeg-encoder) #:use-module (crates-io))

(define-public crate-jpeg-encoder-0.0.1 (c (n "jpeg-encoder") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.23") (k 2)) (d (n "jpeg-decoder") (r "^0.1.22") (d #t) (k 2)))) (h "0sdi0f32jjwxjg8jlsh62yarsj3jhmhh6svz94hvpi7nmlafkqm5")))

(define-public crate-jpeg-encoder-0.0.2 (c (n "jpeg-encoder") (v "0.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.23") (k 2)) (d (n "jpeg-decoder") (r "^0.1.22") (d #t) (k 2)))) (h "1l38c5mxiwd00clqksmcs9dkdp54n1irqci3d6c6c53xjn59amrn")))

(define-public crate-jpeg-encoder-0.1.0 (c (n "jpeg-encoder") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.23") (k 2)) (d (n "jpeg-decoder") (r "^0.1.22") (d #t) (k 2)))) (h "1sz63pdkia3m0nr3h5p66kgffgcwqjr2cs0hnnfmiz7m0sqip447")))

(define-public crate-jpeg-encoder-0.2.0 (c (n "jpeg-encoder") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.23") (k 2)) (d (n "jpeg-decoder") (r "^0.1.22") (d #t) (k 2)))) (h "05zmyn42vxr4av5kpk84y5ww8iqsv66ja0i6w40qd8wins283mx4")))

(define-public crate-jpeg-encoder-0.3.0 (c (n "jpeg-encoder") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.23") (k 2)) (d (n "jpeg-decoder") (r "^0.1.22") (d #t) (k 2)))) (h "1chc35k8k3gfi2brwshpjiqdbyl1xlgbfwml4h9flswa9bv0av7b") (f (quote (("simd") ("default"))))))

(define-public crate-jpeg-encoder-0.4.0 (c (n "jpeg-encoder") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.23") (k 2)) (d (n "jpeg-decoder") (r "^0.1.22") (d #t) (k 2)))) (h "0k28ji5dapdipciminl5af5s627h5r88fhy8hqqgwshcs17qzn15") (f (quote (("std") ("simd") ("default" "std"))))))

(define-public crate-jpeg-encoder-0.4.1 (c (n "jpeg-encoder") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.23") (k 2)) (d (n "jpeg-decoder") (r "^0.1.22") (d #t) (k 2)))) (h "1i0mxk06is385glkl1786ak5a9cig6vpngmrdmwfxak0z5zlrcyg") (f (quote (("std") ("simd" "std") ("default" "std"))))))

(define-public crate-jpeg-encoder-0.5.0 (c (n "jpeg-encoder") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "jpeg-decoder") (r "^0.2") (d #t) (k 2)))) (h "1laixv4aifwa8l79g47r8d0lfs571p9aaqyhn3290pxlpyl2d01w") (f (quote (("std") ("simd" "std") ("default" "std"))))))

(define-public crate-jpeg-encoder-0.5.1 (c (n "jpeg-encoder") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "jpeg-decoder") (r "^0.2") (d #t) (k 2)))) (h "01j9wslaqs0ragv3i5xkkjz2l8lbarspbv4h4scz3ngz4zzazwrc") (f (quote (("std") ("simd" "std") ("default" "std"))))))

(define-public crate-jpeg-encoder-0.6.0 (c (n "jpeg-encoder") (v "0.6.0") (d (list (d (n "jpeg-decoder") (r "^0.3") (k 2)))) (h "09prr9s8gbgs67wh5rvs37v97xksqcvcqlywf9hq7yhjzfjfbvrg") (f (quote (("std") ("simd" "std") ("default" "std"))))))

