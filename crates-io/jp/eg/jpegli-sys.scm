(define-module (crates-io jp eg jpegli-sys) #:use-module (crates-io))

(define-public crate-jpegli-sys-0.1.0+0.10.2 (c (n "jpegli-sys") (v "0.1.0+0.10.2") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.29") (d #t) (k 1)))) (h "1a4i72jnd9hh8cpqiamd2xa283sjf0wa4yfj7wgy3plp8j2rn0hz")))

