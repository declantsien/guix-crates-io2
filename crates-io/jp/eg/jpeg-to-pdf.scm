(define-module (crates-io jp eg jpeg-to-pdf) #:use-module (crates-io))

(define-public crate-jpeg-to-pdf-0.1.1 (c (n "jpeg-to-pdf") (v "0.1.1") (d (list (d (n "jpeg-decoder") (r "^0.1") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "printpdf") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1bv4l7wmxazrawab50z515nh0mcdam0j9f2jnv4mjb9g0aw7vw20")))

(define-public crate-jpeg-to-pdf-0.2.0 (c (n "jpeg-to-pdf") (v "0.2.0") (d (list (d (n "img-parts") (r "^0.2") (d #t) (k 0)) (d (n "jpeg-decoder") (r "^0.1") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "printpdf") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0c6f9kialxgv7x0iqvmmsq7gm56pk7x3rwkcr2rzqcdp4whbkrw8")))

(define-public crate-jpeg-to-pdf-0.2.1 (c (n "jpeg-to-pdf") (v "0.2.1") (d (list (d (n "img-parts") (r "^0.2") (d #t) (k 0)) (d (n "jpeg-decoder") (r "^0.1") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "printpdf") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1m2cnflnvvnvwq2zry0cr661y4b8xagnqpwc6lpmwr59mwfhc71q")))

(define-public crate-jpeg-to-pdf-0.2.2 (c (n "jpeg-to-pdf") (v "0.2.2") (d (list (d (n "img-parts") (r "^0.2") (d #t) (k 0)) (d (n "jpeg-decoder") (r "^0.1") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "printpdf") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0ycbwynw8gzq6lm7wp8w2mbnznsqi404fswwip93px1wph8vr7dy")))

(define-public crate-jpeg-to-pdf-0.2.3 (c (n "jpeg-to-pdf") (v "0.2.3") (d (list (d (n "img-parts") (r "^0.2") (d #t) (k 0)) (d (n "jpeg-decoder") (r "^0.1") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5") (d #t) (k 0)) (d (n "printpdf") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1yi6csa4ma1cll8bf5jpgngivlccg90b671qjakmxslp8zm8llhy")))

