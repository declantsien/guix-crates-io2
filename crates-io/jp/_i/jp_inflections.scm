(define-module (crates-io jp _i jp_inflections) #:use-module (crates-io))

(define-public crate-jp_inflections-0.1.0 (c (n "jp_inflections") (v "0.1.0") (h "0lajb7g7d0fgdxvx3wm6wmh0vvidp5b09vqazbdywnrlldcli55h")))

(define-public crate-jp_inflections-0.1.1 (c (n "jp_inflections") (v "0.1.1") (h "06ga9cvcqwwzashg47b29pfpzk579fqv49752nhbx9jx96hsc9zq")))

(define-public crate-jp_inflections-0.1.2 (c (n "jp_inflections") (v "0.1.2") (h "13j0gg9ski15prfjqgwj0hh82yprq6257bv4lm8y7by9gjcsny5d")))

(define-public crate-jp_inflections-0.1.3 (c (n "jp_inflections") (v "0.1.3") (h "0s44pq9ly7klbw0q69rjz0ycapfbwci5g6hgcxyryd23v0b8f1l5")))

