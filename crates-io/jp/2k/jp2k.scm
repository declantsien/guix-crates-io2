(define-module (crates-io jp #{2k}# jp2k) #:use-module (crates-io))

(define-public crate-jp2k-0.1.0 (c (n "jp2k") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rv6k8g366v9jfc4gjs9k31zcvycnlzs62mp9mdcg5rakygjb85w")))

(define-public crate-jp2k-0.2.0 (c (n "jp2k") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rips") (r "^0.2") (d #t) (k 2)) (d (n "stopwatch") (r "^0.0.7") (d #t) (k 0)))) (h "1q8gmgw1c2809nwz1lcx320ikq9p07fymapkq9sybanjni46n36k")))

(define-public crate-jp2k-0.3.0 (c (n "jp2k") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "env_logger") (r "^0.7.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)) (d (n "rips") (r "^0.2") (d #t) (k 2)) (d (n "stopwatch") (r "^0.0.7") (d #t) (k 2)))) (h "06wiqwlqmkb6sl05nb5pgvhcf84gc6sqypifnqs8k0y75qqhcki0")))

(define-public crate-jp2k-0.3.1 (c (n "jp2k") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "env_logger") (r "^0.7.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)) (d (n "rips") (r "^0.2") (d #t) (k 2)) (d (n "stopwatch") (r "^0.0.7") (d #t) (k 2)))) (h "1l1i2aa93z3yjcpdb6dkgc67zjlsy1nxb0s4pgc105vwmrrnmjk4") (f (quote (("docs-rs"))))))

