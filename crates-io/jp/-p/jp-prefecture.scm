(define-module (crates-io jp -p jp-prefecture) #:use-module (crates-io))

(define-public crate-jp-prefecture-0.1.0 (c (n "jp-prefecture") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "test-case") (r "^2.0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "17hkbj4f4iqzzzb8gy1dkpflhr3rkadws6v9k8ld9cc5zsjfhrzj")))

(define-public crate-jp-prefecture-0.1.1 (c (n "jp-prefecture") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "test-case") (r "^2.0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1x8cn8qd3rdmfbv2f2cjzr0563yrh6x0nl37009s3y0xwavl6k01")))

(define-public crate-jp-prefecture-1.0.0 (c (n "jp-prefecture") (v "1.0.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "test-case") (r "^2.0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "01mizadq0yzbvw9877cydc7sgji62l0am16f2myb2nx8wr88dviv")))

(define-public crate-jp-prefecture-1.0.1 (c (n "jp-prefecture") (v "1.0.1") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "test-case") (r "^2.0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "07l7iszcjij80icp72l0781rzr07qm8vxi6kfikvcqiy7sd4f730")))

(define-public crate-jp-prefecture-1.0.2 (c (n "jp-prefecture") (v "1.0.2") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "test-case") (r "^2.0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "10a32i76fk1knkwskfczxzwzyf05c847pkcshybm9qv9mqz7rp29")))

(define-public crate-jp-prefecture-1.0.3 (c (n "jp-prefecture") (v "1.0.3") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "test-case") (r "^2.0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1lpi8rd1x625vqjxs5q6hyf1jqqr44znhvcjyy5xv1yy0z43g1zp")))

(define-public crate-jp-prefecture-1.0.4 (c (n "jp-prefecture") (v "1.0.4") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "test-case") (r "^2.0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1vnz8r5ggp0b88m1h1y5lnaqxcbkkddz4ym6s4di35sk1dxh5qrz")))

(define-public crate-jp-prefecture-1.0.5 (c (n "jp-prefecture") (v "1.0.5") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "1zj624kr5299dxs52rc41cspg9cizg7yf6bgl5n1y6n67vv41ral")))

(define-public crate-jp-prefecture-2.0.0 (c (n "jp-prefecture") (v "2.0.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "08y9zrcd69c18iyky4gk70m3f55ff779lkrlj0h0d9y5fiywl1l3")))

(define-public crate-jp-prefecture-3.0.0 (c (n "jp-prefecture") (v "3.0.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "1baq4ybfph6ycgh5gj7nfxc8ih9pkmvp9g051c7lj949ffzvg015")))

(define-public crate-jp-prefecture-3.1.0 (c (n "jp-prefecture") (v "3.1.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "0baci0lyp18g3rn4w5x5l6vwgc0lh6d2sn5piwlpdxv6xx93s0iw")))

