(define-module (crates-io jp ut jput) #:use-module (crates-io))

(define-public crate-jput-0.1.0 (c (n "jput") (v "0.1.0") (d (list (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0yd4j8hd0k57jghkfl07ln192ahxknimhl2bn2hpxrpjrsmhzs9s") (y #t)))

(define-public crate-jput-0.1.1 (c (n "jput") (v "0.1.1") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1cksjw6qyr2p4p885yjsyzysshx25731gpz20hj7kxqmfdba8kqs") (y #t)))

(define-public crate-jput-0.1.2 (c (n "jput") (v "0.1.2") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1prrdawr8n6kqyr2p8wqilv4br3lggpsnb282x1isk1f28zsf5x6")))

