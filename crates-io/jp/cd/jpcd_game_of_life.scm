(define-module (crates-io jp cd jpcd_game_of_life) #:use-module (crates-io))

(define-public crate-jpcd_game_of_life-0.0.1 (c (n "jpcd_game_of_life") (v "0.0.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "084lsf7pxnf1jl0i93d3kf2i6gcp0ggb4mkr6ajhd0sxpbjanwd2")))

(define-public crate-jpcd_game_of_life-0.0.2 (c (n "jpcd_game_of_life") (v "0.0.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0cvfvxkp81ddph3j03c4rddks19wfbrcknj9qwxqm94p2w1y7kj2")))

