(define-module (crates-io jp db jpdb) #:use-module (crates-io))

(define-public crate-jpdb-0.1.0 (c (n "jpdb") (v "0.1.0") (h "11ykjjyax46sycibdsks8r9svpjb4rmpkadblj2y2wcv9g1mq2rg")))

(define-public crate-jpdb-0.2.0 (c (n "jpdb") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.6") (f (quote ("json" "gzip" "tls"))) (d #t) (k 0)))) (h "16nchipfaxxravmka8r9q0h1q5kj4f4p4djhx00kgi9dpq79kvyh")))

(define-public crate-jpdb-0.2.1 (c (n "jpdb") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.6") (f (quote ("json" "gzip" "tls"))) (d #t) (k 0)))) (h "07hv72qaqhn29fdq14y7sd0kv3xinc7c33ira9whf9fchhblnlcj")))

(define-public crate-jpdb-0.3.0 (c (n "jpdb") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.6") (f (quote ("json" "gzip" "tls"))) (d #t) (k 0)))) (h "0d804p1fad31k6ak7da2adn07wj1z2r2a0kk5hgmfz9s1vwq25i8")))

(define-public crate-jpdb-0.4.0 (c (n "jpdb") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.6") (f (quote ("json" "gzip" "tls"))) (d #t) (k 0)))) (h "1dq1gs57wnd4541rmg5byqgfk0zwdcd8nnjnh9ra8c3hbc1x55qa")))

(define-public crate-jpdb-0.5.0 (c (n "jpdb") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.6") (f (quote ("json" "gzip" "tls"))) (d #t) (k 0)))) (h "0z7zk0q9lqd2rm4d9a05s19ziwpsflayvk6r8fzf5z6p1z1wnkb1")))

