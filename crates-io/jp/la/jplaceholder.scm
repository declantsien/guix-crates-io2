(define-module (crates-io jp la jplaceholder) #:use-module (crates-io))

(define-public crate-jplaceholder-0.1.0 (c (n "jplaceholder") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.74") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.74") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)))) (h "1nf5bras20sga6fmlqgb609wxbhs4g5d363d3xqxflbbil5ciw3r")))

(define-public crate-jplaceholder-1.0.0 (c (n "jplaceholder") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.74") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.74") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)))) (h "1lhb3lj6mdip1dfvbyimf4f5n68p88wq8bk89np0pr2ark8imm5f")))

(define-public crate-jplaceholder-1.0.1 (c (n "jplaceholder") (v "1.0.1") (d (list (d (n "reqwest") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.74") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.74") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)))) (h "1swyyvzga8dzdb8g357vb1mxjsl2hihwlqnz5vwyvlxdga96vhsa")))

(define-public crate-jplaceholder-1.5.0 (c (n "jplaceholder") (v "1.5.0") (d (list (d (n "reqwest") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.74") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.74") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)))) (h "03frsg7qjxkzvlm3n0xxdhs2bmn6wj0m40x5dqgadkrzhbl45qzl")))

(define-public crate-jplaceholder-1.5.1 (c (n "jplaceholder") (v "1.5.1") (d (list (d (n "reqwest") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.74") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.74") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)))) (h "02xwf8vm6gbw8zfi1qvr8ihk71n8lznhp8gajjzl2vj02y08jfg4")))

