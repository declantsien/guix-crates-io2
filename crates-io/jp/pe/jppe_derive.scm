(define-module (crates-io jp pe jppe_derive) #:use-module (crates-io))

(define-public crate-jppe_derive-0.2.0 (c (n "jppe_derive") (v "0.2.0") (d (list (d (n "virtue") (r "^0") (d #t) (k 0)))) (h "0319dav53scscqhvc393b13bcjm26rczksh5la87589w21sxxpws") (r "1.56")))

(define-public crate-jppe_derive-0.3.0 (c (n "jppe_derive") (v "0.3.0") (d (list (d (n "virtue") (r "^0") (d #t) (k 0)))) (h "12g4wc08bksirzvy3crw06llb395hhkjsl1ii1mrii3ziw45ksz4") (r "1.56")))

(define-public crate-jppe_derive-0.4.0 (c (n "jppe_derive") (v "0.4.0") (d (list (d (n "virtue") (r "^0") (d #t) (k 0)))) (h "0jgfh1wglm283jixmqskgnznjfxg2a3cy9j5lm28r979a2l560qp") (r "1.56")))

(define-public crate-jppe_derive-0.5.0 (c (n "jppe_derive") (v "0.5.0") (d (list (d (n "virtue") (r "^0") (d #t) (k 0)))) (h "0xv69mqyywm77hsm7s4wfvcc89bx93kxsmqcaqvbkdgzca6naanm") (r "1.56")))

(define-public crate-jppe_derive-0.6.0 (c (n "jppe_derive") (v "0.6.0") (d (list (d (n "virtue") (r "^0") (d #t) (k 0)))) (h "14ycfn9ki002kl6hlnf4jf85b6ngc7sb8nalyr64jjspsbhcbjaf") (r "1.56")))

(define-public crate-jppe_derive-0.7.0 (c (n "jppe_derive") (v "0.7.0") (d (list (d (n "virtue") (r "^0") (d #t) (k 0)))) (h "17mf9y5sg7rh8r1dzvh7dbrcf68b18dkd7ds40jl5w429n8rkv7r") (r "1.56")))

(define-public crate-jppe_derive-0.8.0 (c (n "jppe_derive") (v "0.8.0") (d (list (d (n "virtue") (r "^0") (d #t) (k 0)))) (h "06b3v3nh4injpqs1ipn27ycy2iw9kvvvwpsdnzw9ckd5fr34f06h") (r "1.56")))

(define-public crate-jppe_derive-0.8.1 (c (n "jppe_derive") (v "0.8.1") (d (list (d (n "virtue") (r "^0") (d #t) (k 0)))) (h "0gv7dl51s835md18ingrpz43x7bbrgkfymxs2dv4i3gy5j1ig701") (r "1.56")))

(define-public crate-jppe_derive-0.9.0 (c (n "jppe_derive") (v "0.9.0") (d (list (d (n "virtue") (r "^0") (d #t) (k 0)))) (h "1hffvi8qgfv0n1my86nzz0l8vrfcvai65i6binicmhp3a7aqx5ak") (r "1.56")))

(define-public crate-jppe_derive-0.10.0 (c (n "jppe_derive") (v "0.10.0") (d (list (d (n "virtue") (r "^0") (d #t) (k 0)))) (h "01i937vp7j4qz8j6c80aawflhf187lg0cc7fjvwlclv6rxbvnm8z") (r "1.56")))

(define-public crate-jppe_derive-1.0.0 (c (n "jppe_derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "virtue") (r "^0") (d #t) (k 0)))) (h "1fcm58vcjcgf07akdyc93jnpc3sqdnzqyf1xcb9gny9gxi8ffzwx") (r "1.56")))

