(define-module (crates-io jp _c jp_cli) #:use-module (crates-io))

(define-public crate-jp_cli-0.1.0 (c (n "jp_cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "0f9va1laibpy4y3qlk9s2r8y63avpc8j510c2101mbg7l7mvn6wi")))

(define-public crate-jp_cli-0.2.0 (c (n "jp_cli") (v "0.2.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "1cfb5zaqww1v2bpbndf5s4r7kdkars116sck1va0f24vr5w22ix0")))

(define-public crate-jp_cli-0.3.0 (c (n "jp_cli") (v "0.3.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "exit") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "0jgx323zhi3i9m57w032gvvj5r5w8wcz0b9pdmh29gcfkazxyq5k")))

(define-public crate-jp_cli-0.4.0 (c (n "jp_cli") (v "0.4.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "exit") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "0dv53wq54508q326fi5n60zqpza9iffsz2g24ns46iijkfpjkddd")))

