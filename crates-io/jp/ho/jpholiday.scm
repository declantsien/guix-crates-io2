(define-module (crates-io jp ho jpholiday) #:use-module (crates-io))

(define-public crate-jpholiday-0.1.0 (c (n "jpholiday") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.2") (d #t) (k 0)))) (h "1jxrpagbdmy0ih5bsjfsv7p0394pb2m30lsgq9cl6l7544z32i9v") (y #t)))

(define-public crate-jpholiday-0.1.1 (c (n "jpholiday") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.2") (d #t) (k 0)))) (h "012v66dggpb3bla9fkbi2lxp21q5jd3f1i4s4zx9p6j55f6y1lq5") (y #t)))

(define-public crate-jpholiday-0.1.2 (c (n "jpholiday") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.2") (d #t) (k 0)))) (h "1v8rxj45ngi0g7qairfkb36hswd3a25cbz3iq2vfvrh45iddbf4s")))

(define-public crate-jpholiday-0.1.3 (c (n "jpholiday") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.2") (d #t) (k 0)))) (h "18vsd2cv1cj4fmqbwalpnkvsc279ls4rr87yxv1sg9df7l7fwbqd")))

(define-public crate-jpholiday-0.1.4 (c (n "jpholiday") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.2") (d #t) (k 0)))) (h "1pwvr0qdxl8yz82l0cizjncqhdbv2hprxnvf8s27hwxsgwwjd7ki")))

