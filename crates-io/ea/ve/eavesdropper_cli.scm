(define-module (crates-io ea ve eavesdropper_cli) #:use-module (crates-io))

(define-public crate-eavesdropper_cli-0.1.0 (c (n "eavesdropper_cli") (v "0.1.0") (d (list (d (n "abi_stable") (r "^0.9.3") (k 0)) (d (n "cliargs_t") (r "^0.1.0") (d #t) (k 0)) (d (n "diesel") (r "^1.4.6") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "eframework") (r "^0.1.0") (d #t) (k 0)) (d (n "etherparse") (r "^0.9.0") (d #t) (k 0)) (d (n "log_t") (r "^0.1.0") (d #t) (k 0)) (d (n "pcap") (r "^0.7.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)) (d (n "rustyline") (r "^7.1.0") (d #t) (k 0)))) (h "0q763gkqwpnis7k2hb8x4ar31jmn71hwkj5hisjrslpw98dz4gzc")))

