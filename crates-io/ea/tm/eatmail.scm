(define-module (crates-io ea tm eatmail) #:use-module (crates-io))

(define-public crate-eatmail-0.1.0 (c (n "eatmail") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libsql-client") (r "^0.6.1") (f (quote ("reqwest_backend"))) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "0y6f4fdcpfk3ac8gsl12pnzbcgsc6djp0ik3x01hbmii7ip67193")))

(define-public crate-eatmail-0.1.1 (c (n "eatmail") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libsql-client") (r "^0.6.1") (f (quote ("reqwest_backend"))) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "0lixadww1c8ljn14mb6qrjd6xjp39ycimb8hf40hh0r428aq5n61")))

(define-public crate-eatmail-0.1.4 (c (n "eatmail") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libsql-client") (r "^0.15.1") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "15a3an9zws6lcvln6dgxisq0xhijgri9jn8xcp5aakfhas0hdx7k")))

(define-public crate-eatmail-0.1.5 (c (n "eatmail") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libsql-client") (r "^0.15.1") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1j9a0k2aa5s7bgssm0kplgrlmvyrm9hjmkal0xcbygka1444223z")))

(define-public crate-eatmail-0.2.0 (c (n "eatmail") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libsql-client") (r "^0.24") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "0hwv89ifhr129hcshi2qfah8cnvv93xyzvadhjyymdh2jqhnk508")))

(define-public crate-eatmail-0.2.1 (c (n "eatmail") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libsql-client") (r "^0.24") (f (quote ("local_backend" "reqwest_backend"))) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "00znqxpkk9s325fybp996mxm5a30f73ghqphsmlq7ikvqh7wvnj2")))

