(define-module (crates-io ea ns eansearch) #:use-module (crates-io))

(define-public crate-eansearch-1.0.0 (c (n "eansearch") (v "1.0.0") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "engine") (r "^0.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.0") (d #t) (k 0)))) (h "1nw5xm59ics13mmppmbakxmcs1m2rqr7mpyql6v27gff5ln6vlfd")))

(define-public crate-eansearch-1.0.1 (c (n "eansearch") (v "1.0.1") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "engine") (r "^0.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.0") (d #t) (k 0)))) (h "010f0rajiyyvin3vdla77js5s2zhrf4h63w5lp0qwk96qp649p2g")))

(define-public crate-eansearch-1.0.2 (c (n "eansearch") (v "1.0.2") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "engine") (r "^0.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.0") (d #t) (k 0)))) (h "0ryghmay8z628g2yf7zk4gvfav70k0z63fn9yr702r7rk6jgr3fn")))

