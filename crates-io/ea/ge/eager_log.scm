(define-module (crates-io ea ge eager_log) #:use-module (crates-io))

(define-public crate-eager_log-0.1.0 (c (n "eager_log") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.16") (f (quote ("serde" "clock"))) (d #t) (k 0)))) (h "0nchwpw8yxd3xhfpc63iccf4ajhkizyk80c8xfsgmy83mh36825f") (y #t)))

(define-public crate-eager_log-0.1.1 (c (n "eager_log") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.16") (f (quote ("serde" "clock"))) (d #t) (k 0)))) (h "06cirbhn0vjq9jkdfxxmy0k3shwbsbvzzqhp1jz3fh82f6azjs6c")))

