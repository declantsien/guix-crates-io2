(define-module (crates-io ea ge eager_json) #:use-module (crates-io))

(define-public crate-eager_json-0.1.0 (c (n "eager_json") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0g0cwfr6j40x505xx270k8zcwdx2mjf93r50kbnj033p265afbdz")))

