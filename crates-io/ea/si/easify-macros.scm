(define-module (crates-io ea si easify-macros) #:use-module (crates-io))

(define-public crate-easify-macros-0.1.0 (c (n "easify-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0g1551sapsm0s95rlf2xhxd8z3hxb9yl35qsvrkmzy1vv0n0fcly")))

(define-public crate-easify-macros-0.1.1 (c (n "easify-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1j6k7nm2dj97k5kva3zjs0lk291cx7j6wr0bgjwk2w27752cwyww")))

