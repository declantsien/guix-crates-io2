(define-module (crates-io ea si easing) #:use-module (crates-io))

(define-public crate-easing-0.0.1 (c (n "easing") (v "0.0.1") (h "1yd9anhp0px09wjjglillxvmhkgp6k9938kx4yh5s6a86z2sk3cv")))

(define-public crate-easing-0.0.2 (c (n "easing") (v "0.0.2") (h "06npafnxxpl0w9cq4m3ji3kkzzg1jbx9nyldz0m5s96j2r4j4px4")))

(define-public crate-easing-0.0.3 (c (n "easing") (v "0.0.3") (h "1jnnq3nj9ibhz6bix4sgm0hxmkqvaz7qwy5002mx5mw2bwcjdf2c")))

(define-public crate-easing-0.0.5 (c (n "easing") (v "0.0.5") (h "0gszvzchknjl7jdavyq72avamxij4nnjnzpkwlx6mycx5s6wzcch")))

