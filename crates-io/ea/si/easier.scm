(define-module (crates-io ea si easier) #:use-module (crates-io))

(define-public crate-easier-0.1.0 (c (n "easier") (v "0.1.0") (h "1zha1x28xdl1l70yif6snnjk5rl798a14zbqjcbsfqzhqi6dczfq")))

(define-public crate-easier-0.2.0 (c (n "easier") (v "0.2.0") (h "19hxsndl962805j0njskzasq76fg62jkb2c7vgq2li7s79pba4im")))

