(define-module (crates-io ea si easings-rs) #:use-module (crates-io))

(define-public crate-easings-rs-0.1.0 (c (n "easings-rs") (v "0.1.0") (h "0rrynz5hgv27c3zawifd9xcyppnx6cxdjcpamzx3jzfphix2y42n")))

(define-public crate-easings-rs-0.1.1 (c (n "easings-rs") (v "0.1.1") (h "1lpa0a1xbpx2ns2zv1aj334gl4pqn7fpyx7yqb6v760zvkkmr7fk")))

