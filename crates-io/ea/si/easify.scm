(define-module (crates-io ea si easify) #:use-module (crates-io))

(define-public crate-easify-0.1.0 (c (n "easify") (v "0.1.0") (h "1k6yi2x2bn8q1jqsy8ia94y6fz5kxy475kw2bs4lsp163zy63rwl")))

(define-public crate-easify-0.2.0 (c (n "easify") (v "0.2.0") (d (list (d (n "easify-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0gi4rm0hg019gm62gljrrs4nvcd5hqnlxigm77xyrh4s34pw2cmk")))

(define-public crate-easify-0.2.1 (c (n "easify") (v "0.2.1") (d (list (d (n "easify-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1q0lnwqd7ry62yq55kz1vb0n3c55psqbbm9j9gcbxjmynakyxw58")))

(define-public crate-easify-0.2.2 (c (n "easify") (v "0.2.2") (d (list (d (n "easify-macros") (r "^0.1.1") (d #t) (k 0)))) (h "1w77m707vjfzbz3zml3755rfkdwd07ipd69i827z8ignm3b5qq1r")))

