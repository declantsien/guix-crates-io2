(define-module (crates-io ea rt earthquaked) #:use-module (crates-io))

(define-public crate-earthquaked-0.1.0 (c (n "earthquaked") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.25") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.17") (d #t) (k 0)))) (h "14wg549v9aw0qznf6kiyjwfnqg6y8hm7nkmzdhaivbdiynmmcxi0")))

