(define-module (crates-io ea rt earth) #:use-module (crates-io))

(define-public crate-earth-0.0.2 (c (n "earth") (v "0.0.2") (d (list (d (n "chrono") (r "^0.2.25") (d #t) (k 0)) (d (n "html5ever") (r "^0.5.4") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3.4") (d #t) (k 0)))) (h "1yjddl02773abxvpccg85q73sff1x3jjrdl3ja522ikvc6vaf120")))

