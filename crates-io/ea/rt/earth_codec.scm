(define-module (crates-io ea rt earth_codec) #:use-module (crates-io))

(define-public crate-earth_codec-0.0.1 (c (n "earth_codec") (v "0.0.1") (d (list (d (n "better-panic") (r "^0.2.0") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.7.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "keyphrase") (r "^0.0.1") (d #t) (k 0)))) (h "02iscp49jwjnqqh4awjza7y7mvhvlc0lwkr7p4045if430px443r")))

