(define-module (crates-io ea s- eas-sdk-rs) #:use-module (crates-io))

(define-public crate-eas-sdk-rs-0.1.0 (c (n "eas-sdk-rs") (v "0.1.0") (d (list (d (n "ethers") (r "^2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "1i9p3188hf06y4fa0bbmmjk9g5fq06193y3p6n6s1yhbjrzgsskc")))

