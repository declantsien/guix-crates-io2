(define-module (crates-io ea s- eas-env) #:use-module (crates-io))

(define-public crate-eas-env-0.1.0 (c (n "eas-env") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07fmsg7g46dng37n2cvc4ibz3z322v18xz61qaxcli3ysgydq2b4")))

(define-public crate-eas-env-0.1.1 (c (n "eas-env") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09wv2wcx18x5gmsj3vh2gxph6swm2v241zc16jrq1z8sznhbj3qm")))

(define-public crate-eas-env-0.2.0 (c (n "eas-env") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0svygj0kwn9ssnj63dibkj4caln82pffkqqprp9bxvfwz456z86v")))

(define-public crate-eas-env-0.2.1 (c (n "eas-env") (v "0.2.1") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0g811jxpfxllwdgvidrjfxy6xmrj55vdnwcznw7lmvbfz9bc1wwr")))

(define-public crate-eas-env-0.2.2 (c (n "eas-env") (v "0.2.2") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wk6i7dvsjwzzsc69zs29gzm0m0ghvczqy2skfr86yi8kry48ipz")))

