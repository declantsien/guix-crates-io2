(define-module (crates-io ea t- eat-nom) #:use-module (crates-io))

(define-public crate-eat-nom-0.1.0 (c (n "eat-nom") (v "0.1.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1wq7ccpmgvx63jm3jkd49lbnxzs503iwfps6qs1hb98kxfxqckzi")))

(define-public crate-eat-nom-0.1.2 (c (n "eat-nom") (v "0.1.2") (d (list (d (n "nom") (r "^6.0.0-alpha1") (d #t) (k 0)))) (h "0hmv8m3p9yga1kxbw13mj6zjkss52c1hfrkb2mqxi4nzsgmsa288")))

(define-public crate-eat-nom-0.1.3 (c (n "eat-nom") (v "0.1.3") (d (list (d (n "nom") (r "^6.0.0-alpha1") (d #t) (k 0)))) (h "1h07adfpj4a71b4z3d65sahgf0li14a1vbpvgb1296dxg1fx38bg")))

(define-public crate-eat-nom-0.1.4 (c (n "eat-nom") (v "0.1.4") (d (list (d (n "nom") (r "^6.0.0-alpha1") (d #t) (k 0)))) (h "06510s6dhqm5i9w9sl3zw56z63j7ccici9hisdcxca1vpv9dskzd")))

(define-public crate-eat-nom-0.1.5 (c (n "eat-nom") (v "0.1.5") (d (list (d (n "nom") (r "^6.0.0-alpha1") (d #t) (k 0)))) (h "0wg4jqj0l6ddwv20gnb8d7yxiwpi421xfvzl25zkf41xdalk7cc8")))

