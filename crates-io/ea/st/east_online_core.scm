(define-module (crates-io ea st east_online_core) #:use-module (crates-io))

(define-public crate-east_online_core-0.1.0 (c (n "east_online_core") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0d86h3ipbxclzcv3vwwcda2kyqhwk3634fxw417249c4915iij1q")))

(define-public crate-east_online_core-0.1.1 (c (n "east_online_core") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "139y2gyzy39ly5kzssfpjihxx36iria38nd5pg8hdzijc2wgzk2c")))

(define-public crate-east_online_core-0.1.2 (c (n "east_online_core") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jigg7inn7n3imab7dsch3p337hdc3i5mai0a8526vx8p8iiq3ls")))

(define-public crate-east_online_core-0.1.3 (c (n "east_online_core") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q636c48phz83pq1gi9s6glkizwjj7zh5h7sj2jn6m8g97jmjr48")))

(define-public crate-east_online_core-0.1.4 (c (n "east_online_core") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q8apby5v7hzqmlfi2k8fw6l6vq4zn237darbkcy52ahq2nazk5w")))

(define-public crate-east_online_core-0.2.0 (c (n "east_online_core") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1brd2y64d10srfaw8rkah0q946wssxn389jmhi553629vy0613g6")))

(define-public crate-east_online_core-0.2.1 (c (n "east_online_core") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "06gajaxd1flmbfzz4dh2ayviz54yx9qdrvhz234jfhxvsx2sz4i3")))

(define-public crate-east_online_core-0.2.2 (c (n "east_online_core") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "08s5h186dkcs7pvb6sc9zkxg4c46c2q305bbz8lwk4xls9mww1qd")))

(define-public crate-east_online_core-0.2.3 (c (n "east_online_core") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fyhzf79rmg9mly84nxkk3pmcazs0274h8slj4xxw8q2v144lpyj")))

(define-public crate-east_online_core-0.2.4 (c (n "east_online_core") (v "0.2.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01i2avx8r8nph5fqqa9lv6gpks79ywmw99rzialibcvhz7142l0l")))

