(define-module (crates-io ea st easter) #:use-module (crates-io))

(define-public crate-easter-0.0.1 (c (n "easter") (v "0.0.1") (d (list (d (n "joker") (r "^0.0.1") (d #t) (k 0)))) (h "0pkd6yysq0w068rhzy2rycgcbrif1g7di5s2m4pb79dayr89v24l")))

(define-public crate-easter-0.0.2 (c (n "easter") (v "0.0.2") (d (list (d (n "joker") (r "^0.0.2") (d #t) (k 0)))) (h "10zjazmzz6j0gs8pm937mpn2agv8g8169j2592j19f04a7qk4il5")))

(define-public crate-easter-0.0.3 (c (n "easter") (v "0.0.3") (d (list (d (n "joker") (r "^0.0.3") (d #t) (k 0)))) (h "07pmag661hyrmdp2v4x9iwlv85sfqgbpvfnch7i20f8if3f66ss8")))

(define-public crate-easter-0.0.4 (c (n "easter") (v "0.0.4") (d (list (d (n "joker") (r "^0.0.4") (d #t) (k 0)))) (h "04rxlcn9vciiimigqwyy66piia4a0gq40h2nj450ygf4y8013w1w")))

(define-public crate-easter-0.0.5 (c (n "easter") (v "0.0.5") (d (list (d (n "joker") (r "^0.0.5") (d #t) (k 0)))) (h "0f7fsi06pgh041wsymlczq252fa2hklcgznsq7hz48q4h3r3yydk")))

