(define-module (crates-io ea st east-macro) #:use-module (crates-io))

(define-public crate-east-macro-0.1.0 (c (n "east-macro") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1b86z7bx77yfnpnd9kjpxpp6rm4rhhlmpbzjmqg0w3hzr48lys26")))

(define-public crate-east-macro-0.2.0 (c (n "east-macro") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wgmds1nwyvkbjsngrqyrxkwbbjzrzh3254r77gza7isv1ffbz0l")))

