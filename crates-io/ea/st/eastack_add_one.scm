(define-module (crates-io ea st eastack_add_one) #:use-module (crates-io))

(define-public crate-eastack_add_one-0.1.0 (c (n "eastack_add_one") (v "0.1.0") (h "1p83bwn5c44nxax6xvvxx9l1pv9klhvxsrz6f59fs7l2plm0y0jl")))

(define-public crate-eastack_add_one-0.1.1 (c (n "eastack_add_one") (v "0.1.1") (h "11sl69ksyr0w8mgd0d2ghj6zh4nlpslj7qxv9jkiinxlxliiv0f5")))

