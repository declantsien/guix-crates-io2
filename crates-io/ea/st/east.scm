(define-module (crates-io ea st east) #:use-module (crates-io))

(define-public crate-east-0.1.0 (c (n "east") (v "0.1.0") (h "0hy9qzj8wqcy61lhqk79xljim474bb9mm2ay801p0z12c0bv5xq0")))

(define-public crate-east-0.2.0 (c (n "east") (v "0.2.0") (d (list (d (n "east-macro") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sycamore") (r "^0.8") (f (quote ("ssr" "hydrate"))) (d #t) (k 0)))) (h "14slzcynbxf2wby5lbms1s305g802n95fma1a23146rvd7zgwpsr")))

