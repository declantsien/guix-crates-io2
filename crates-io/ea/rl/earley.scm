(define-module (crates-io ea rl earley) #:use-module (crates-io))

(define-public crate-earley-0.1.0 (c (n "earley") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3.2") (d #t) (k 2)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^0.1.2") (d #t) (k 0)))) (h "14b9d85bp2z1lnmfcqlnc7z19sn23i80lz4yyvph01hiv8hs1mn1")))

