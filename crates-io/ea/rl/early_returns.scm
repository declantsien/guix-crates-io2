(define-module (crates-io ea rl early_returns) #:use-module (crates-io))

(define-public crate-early_returns-0.1.0 (c (n "early_returns") (v "0.1.0") (h "0rbv3sd6qfzms6czx3qij3fhpb3w0zsh4jjm3kzdjv2b61z2cv7k")))

(define-public crate-early_returns-0.2.1 (c (n "early_returns") (v "0.2.1") (h "068szg82wdn35m9yqjavb7igdq5liab80lkiklx4z62ss085qvr4")))

(define-public crate-early_returns-0.3.0 (c (n "early_returns") (v "0.3.0") (h "0jsg8hkkl19q7fdm18dby8086nyaslxxlhag845zdawc7fydgkfm")))

(define-public crate-early_returns-0.3.1 (c (n "early_returns") (v "0.3.1") (h "08fb4088vxzsr6fi8x4b6c0dg98fh84lwvl6b6cr18wwrg3dhi6a")))

(define-public crate-early_returns-0.4.0 (c (n "early_returns") (v "0.4.0") (h "1fk5030zqkmpglznjs5b3qmsyw5i0iyasv2c4a3fy9k7av93jngk")))

