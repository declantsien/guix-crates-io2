(define-module (crates-io ea rl earlgrey) #:use-module (crates-io))

(define-public crate-earlgrey-0.0.4 (c (n "earlgrey") (v "0.0.4") (d (list (d (n "lexers") (r "^0.0.4") (d #t) (k 0)) (d (n "linenoise-rust") (r "^0.2") (d #t) (k 2)) (d (n "regex") (r "^0.1") (d #t) (k 2)))) (h "12d4wcbfcmmh3dfi4mzgd8q4bzbvzz554lprjq0gyxm0cxbwmnc1")))

(define-public crate-earlgrey-0.0.5 (c (n "earlgrey") (v "0.0.5") (d (list (d (n "lexers") (r "^0.0.4") (d #t) (k 0)) (d (n "linenoise-rust") (r "^0.2") (d #t) (k 2)) (d (n "regex") (r "^0.1") (d #t) (k 2)))) (h "1mwy0vvaqhxx90gli21hylf1sv0hy4rd0yy82m49xswq8pqdz3c5")))

(define-public crate-earlgrey-0.0.6 (c (n "earlgrey") (v "0.0.6") (d (list (d (n "lexers") (r "^0.0.5") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 2)))) (h "1k22v1fmw6s4mgl22k5f7k8spy71k2prk993kcpr5qn912zv2x1c")))

(define-public crate-earlgrey-0.0.7 (c (n "earlgrey") (v "0.0.7") (d (list (d (n "lexers") (r "^0.0.5") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 2)))) (h "0a73hrgf503ndk72wkzfq6mqcdamqbw5xj3q60rpnzfy3n17p6nd")))

(define-public crate-earlgrey-0.1.0 (c (n "earlgrey") (v "0.1.0") (d (list (d (n "lexers") (r "^0.0.5") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 2)))) (h "1adps29zllljrj00zwmnw1h03gg7i3n5cv9q9j4adml85fwr0njg")))

(define-public crate-earlgrey-0.2.0 (c (n "earlgrey") (v "0.2.0") (d (list (d (n "lexers") (r "^0.0.6") (d #t) (k 2)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 2)))) (h "0390gj3mw3cygp9di3sfq2njkqi88x61sl1s2cdd34y2gffgc2fy")))

(define-public crate-earlgrey-0.2.1 (c (n "earlgrey") (v "0.2.1") (d (list (d (n "lexers") (r "^0.0.7") (d #t) (k 2)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 2)))) (h "1hzq7b2gx025xrhjx0cr821bra4w318jl2n7aqs68r2m516xxbdd")))

(define-public crate-earlgrey-0.2.3 (c (n "earlgrey") (v "0.2.3") (d (list (d (n "lexers") (r "^0.0.8") (d #t) (k 2)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 2)))) (h "1a2xz02ms4f1hnyk3is477x6s9bbxprlnxc0g9vl8bmvb72jfx79") (f (quote (("debug"))))))

(define-public crate-earlgrey-0.2.4 (c (n "earlgrey") (v "0.2.4") (d (list (d (n "lexers") (r "^0.1.2") (d #t) (k 2)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 2)))) (h "1y98sq3x8s4rg3hc6i94nnay52c0g7crcr3j823faarzy3qksq9m") (f (quote (("debug"))))))

(define-public crate-earlgrey-0.3.0 (c (n "earlgrey") (v "0.3.0") (d (list (d (n "lexers") (r "^0.1.2") (d #t) (k 2)) (d (n "rustyline") (r "^5.0.0") (d #t) (k 2)))) (h "15n638mzxcbpd2caxndrgfqnzjal9brs61qivmjplhrxnb6y9jal") (f (quote (("debug"))))))

(define-public crate-earlgrey-0.3.1 (c (n "earlgrey") (v "0.3.1") (d (list (d (n "lexers") (r "^0.1.4") (d #t) (k 2)) (d (n "rustyline") (r "^9.1.0") (d #t) (k 2)))) (h "1fphnyh5sxwhp916dl8y2aqhiim8n5bhrj4hxf1jkv0kz4sqfca3") (f (quote (("debug"))))))

(define-public crate-earlgrey-0.3.2 (c (n "earlgrey") (v "0.3.2") (d (list (d (n "lexers") (r "^0.1") (d #t) (k 2)) (d (n "rustyline") (r "^9.1") (d #t) (k 2)))) (h "1ka5dcx04jp76hz8y55kk7x57174b5ywsamd1bny7bqdq3hw827k") (f (quote (("debug"))))))

