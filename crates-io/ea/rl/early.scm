(define-module (crates-io ea rl early) #:use-module (crates-io))

(define-public crate-early-0.1.0 (c (n "early") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1") (d #t) (k 0)))) (h "124wq6f2rlz97fdx7r485igyjv0s6hnh7ksjjfjxr75xvq84s19v")))

