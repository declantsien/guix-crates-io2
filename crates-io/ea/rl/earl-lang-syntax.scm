(define-module (crates-io ea rl earl-lang-syntax) #:use-module (crates-io))

(define-public crate-earl-lang-syntax-0.1.0 (c (n "earl-lang-syntax") (v "0.1.0") (h "19vcr4wz6wzr1dwky973ik16b16wy40b17v093yjd6yrp0wpkn51")))

(define-public crate-earl-lang-syntax-1.0.0 (c (n "earl-lang-syntax") (v "1.0.0") (h "0nx5js6hiqsmpn47myqv4xfvascm2rfq96aw1pxv5anh2gx95xx6")))

