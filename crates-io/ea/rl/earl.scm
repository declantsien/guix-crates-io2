(define-module (crates-io ea rl earl) #:use-module (crates-io))

(define-public crate-earl-0.0.0 (c (n "earl") (v "0.0.0") (h "1ca72kh258i5zrcyxb3ck2gk8g3cdpfkyh9mfvl4jpdyk777f2xf")))

(define-public crate-earl-0.1.0 (c (n "earl") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "encoding") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "heapsize") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "idna") (r "^0.1.0") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.0") (d #t) (k 0)) (d (n "rustc-test") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.94") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0j4an7hrdc6mfg3yswbn1f350mb1skn9ps9p9ygfwryi0wdsaqbd") (f (quote (("query_encoding" "encoding") ("heap_size" "heapsize")))) (y #t)))

