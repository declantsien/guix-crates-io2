(define-module (crates-io ea rw earwax) #:use-module (crates-io))

(define-public crate-earwax-0.1.0 (c (n "earwax") (v "0.1.0") (d (list (d (n "ao_rs") (r "^0.1.0") (d #t) (k 2)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0irfv67wzw58z73y4imvp6kl927h3n5rw2i1svk7vdrzzcgsbqr1")))

(define-public crate-earwax-0.1.1 (c (n "earwax") (v "0.1.1") (d (list (d (n "ao_rs") (r "^0.1.0") (d #t) (k 2)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "13j4ppfgnj3xf6c3d6jmqzzklby8gxvyaz1vdvlf69zsdhmvkbn1")))

(define-public crate-earwax-0.1.2 (c (n "earwax") (v "0.1.2") (d (list (d (n "ao_rs") (r "^0.1.0") (d #t) (k 2)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0qvv9wmcibf5ca2gf2l8pr9m0wrf0ap1bhwxx1p6vv7bpcq1izf4")))

(define-public crate-earwax-0.1.3 (c (n "earwax") (v "0.1.3") (d (list (d (n "ao_rs") (r "^0.1.0") (d #t) (k 2)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "13v991v0v1r1a47z77pd8y3hj14j7pmzkdfv9c5dncag9y1p98z5")))

(define-public crate-earwax-0.1.4 (c (n "earwax") (v "0.1.4") (d (list (d (n "ao_rs") (r "^0.1.0") (d #t) (k 2)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1jj6rikrfm220w5flg887mqvv9skxfw2sinqxv21bw3m186qq0ki")))

(define-public crate-earwax-0.1.6 (c (n "earwax") (v "0.1.6") (d (list (d (n "ao_rs") (r "^0.1.0") (d #t) (k 2)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0gka4b04kbf45vbwpgv0jpn7bqbxmv5vc0qdmma2chximr104ay1")))

(define-public crate-earwax-0.1.7 (c (n "earwax") (v "0.1.7") (d (list (d (n "ao_rs") (r "^0.1.0") (d #t) (k 2)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1vszshq3ihg329lhxybsvhspj35h1yrjb8cmfhhgx522sy80a380")))

