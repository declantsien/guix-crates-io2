(define-module (crates-io ea -b ea-big) #:use-module (crates-io))

(define-public crate-ea-big-0.1.0 (c (n "ea-big") (v "0.1.0") (h "04hd9048iav2mwrwkhv8hznzf9xsd86ig6brip2ybwylsfrhcz56") (y #t)))

(define-public crate-ea-big-0.1.1 (c (n "ea-big") (v "0.1.1") (h "0zzlw21hfzksd046hdg1lvpq6s3whbjfrx1c09pby9kazq36vqk9")))

