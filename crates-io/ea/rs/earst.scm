(define-module (crates-io ea rs earst) #:use-module (crates-io))

(define-public crate-earst-0.1.0 (c (n "earst") (v "0.1.0") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jykw6ha6706d4gw786mprnn0cgjccvg1g4cx7djwhw90kh70qr8")))

(define-public crate-earst-0.1.1 (c (n "earst") (v "0.1.1") (d (list (d (n "nix") (r "^0.27.1") (f (quote ("fs"))) (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ksyp4qk81phyggsxyw32nal4bqb7bypxw7zvydglvjkcvb6zlda")))

