(define-module (crates-io ea rs ears) #:use-module (crates-io))

(define-public crate-ears-0.3.1 (c (n "ears") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ax8zs9amm55cla210jb37wfrpblv3j41bi39c989np5d9j1f6dx")))

(define-public crate-ears-0.3.2 (c (n "ears") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zshp9k9x53n9l6pdvc8358w2qwkjx2saail6r46h7xc56ldfix3")))

(define-public crate-ears-0.3.3 (c (n "ears") (v "0.3.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bmw658al4g9wzchjkz1janrmqb1wyj5mr12adhlvr73ig6b88bp")))

(define-public crate-ears-0.3.4 (c (n "ears") (v "0.3.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15d7id5fxrql38c547l8mh4yvj0mrxi0jnfawbm31370aq2gd05b")))

(define-public crate-ears-0.3.5 (c (n "ears") (v "0.3.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qmhgxa534x6zpdkwglg3pwb9z87yvmmf3cg399vs36p047aqs6r")))

(define-public crate-ears-0.4.0 (c (n "ears") (v "0.4.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "08nwxfdl2izf2x0bpz9ipm76m23ihjjmy0pxw0dj5i5c5mbbrj6p")))

(define-public crate-ears-0.5.0 (c (n "ears") (v "0.5.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ysck1afd7lycl00v2di431j8b9l0mkyclcg45j0qn89qjrs1klc")))

(define-public crate-ears-0.5.1 (c (n "ears") (v "0.5.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11k9xiiwjxbb5fy65lgf9a2a0m1q5lmldq42b5fb2s9v7vh973j0")))

(define-public crate-ears-0.6.0 (c (n "ears") (v "0.6.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fkhs4b716jvb75h120brsaqgr2d79i08qpga231zdzbph6w4vm0")))

(define-public crate-ears-0.7.0 (c (n "ears") (v "0.7.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00cwn1s2939pr34qbgxvcnwd4h3b51v4wy091k94pmhzdgi4hncd")))

(define-public crate-ears-0.8.0 (c (n "ears") (v "0.8.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "04i1kf73pbbp0vp6h8n5828rakv2s9na5z1pr03qy87cg0qcahg7")))

