(define-module (crates-io ea gr eagre-asn1) #:use-module (crates-io))

(define-public crate-eagre-asn1-0.1.0 (c (n "eagre-asn1") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)))) (h "1ln2zyfqry7s2fw3qv6v1pjaxm6k8223014h967zjbfgfhmj6i55")))

(define-public crate-eagre-asn1-0.2.0 (c (n "eagre-asn1") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)))) (h "1fk8hchsgf7mriz1miqhl8fkynyyjgrgh1173qq1bxxjzi3wfmx7")))

(define-public crate-eagre-asn1-0.3.0 (c (n "eagre-asn1") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)))) (h "0pqg7x36cb497c9lshmw0cj4p2r2rr2ywdbi774z70mazmqrc6d4")))

