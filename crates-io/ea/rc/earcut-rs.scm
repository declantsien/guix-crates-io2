(define-module (crates-io ea rc earcut-rs) #:use-module (crates-io))

(define-public crate-earcut-rs-0.3.1 (c (n "earcut-rs") (v "0.3.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("float_roundtrip"))) (d #t) (k 2)))) (h "0qczcad2cy7nfks45pgh57nwi61vs1whpbd03178kd1sii4a3mf5") (y #t)))

(define-public crate-earcut-rs-0.3.2 (c (n "earcut-rs") (v "0.3.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("float_roundtrip"))) (d #t) (k 2)))) (h "17rbhk73qq686wqjqbid62w7dy4hgmf1l38av9gdkpxm04q0iyq1") (y #t)))

