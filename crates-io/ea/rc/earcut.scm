(define-module (crates-io ea rc earcut) #:use-module (crates-io))

(define-public crate-earcut-0.3.2 (c (n "earcut") (v "0.3.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("float_roundtrip"))) (d #t) (k 2)))) (h "0iwz25vbc5mcqljzfw7js9di45v9wdm3alim1nyccy208ic23k1h") (y #t)))

(define-public crate-earcut-0.3.3 (c (n "earcut") (v "0.3.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("float_roundtrip"))) (d #t) (k 2)))) (h "0i9iag7kgmabjwg76zj9s7jlwsbr0ffq4x0vy457mk4cnnb73bwx") (y #t)))

(define-public crate-earcut-0.3.4 (c (n "earcut") (v "0.3.4") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("float_roundtrip"))) (d #t) (k 2)))) (h "0j37xv1hy01zdic5arsdx6wlaqr50ipf3lq49h6pckd1zq3sb25q") (y #t)))

(define-public crate-earcut-0.3.5 (c (n "earcut") (v "0.3.5") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("float_roundtrip"))) (d #t) (k 2)))) (h "0sdl94k91yywzvzlvvjm5v26lzfvn0il106ny4sip3fj8s2ywfza")))

(define-public crate-earcut-0.3.6 (c (n "earcut") (v "0.3.6") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("float_roundtrip"))) (d #t) (k 2)))) (h "0b6cz4vpric1mlwyr75lgjsm4x03d4qjar263q0sx5hwmfx5hv0g")))

(define-public crate-earcut-0.4.0 (c (n "earcut") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("float_roundtrip"))) (d #t) (k 2)))) (h "14bkjcdksw6ihcacqclxy0y648zj73y2gdcl0mv6lbwridm29mrp")))

(define-public crate-earcut-0.4.1 (c (n "earcut") (v "0.4.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (f (quote ("float_roundtrip"))) (d #t) (k 2)))) (h "13l1d2zjqg75bmd1ayn5p6ryva3pk67jzdcybz5hrmcg758wj9lm")))

