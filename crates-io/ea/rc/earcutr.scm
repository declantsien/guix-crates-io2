(define-module (crates-io ea rc earcutr) #:use-module (crates-io))

(define-public crate-earcutr-0.1.0 (c (n "earcutr") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 2)))) (h "1syjbm99gw2ndj15ns2apckadkz2lw91ckrahy3gj5p1l6m81i9n")))

(define-public crate-earcutr-0.1.1 (c (n "earcutr") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 2)))) (h "0vbwkk6q2g20pqlb22fsk85c2yya28vdfi3rmin9ayyml0142wz9")))

(define-public crate-earcutr-0.2.0 (c (n "earcutr") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 2)))) (h "0r6yglxn747kdxba8m3c3njwp522hs9hmlvq5nl6panahf531ra6")))

(define-public crate-earcutr-0.3.0 (c (n "earcutr") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 2)))) (h "183nfw71rz304g8ir2842p8cwamwklizl4p49i7iwn35imc1r7x0")))

(define-public crate-earcutr-0.4.0 (c (n "earcutr") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 2)))) (h "1cjiif699k6smx2f7zd8flryj8wk9l105npxrhb4z1n3yq9vkcv1")))

(define-public crate-earcutr-0.4.1 (c (n "earcutr") (v "0.4.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 2)))) (h "0v0d8l3aghnjx71dr1bszv2p9h6nrxvgzjllqy8izp2q731v6jdv")))

(define-public crate-earcutr-0.4.2 (c (n "earcutr") (v "0.4.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 2)))) (h "0gfashlri7dpxkqvnrjshag80znyr60xmc7wx3yka7cmjx3b84h8")))

(define-public crate-earcutr-0.4.3 (c (n "earcutr") (v "0.4.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)))) (h "00dddrgzsrkbv8ifsmakcxwxrdzzgia7i6cy81y6imw5kbapw4kr")))

