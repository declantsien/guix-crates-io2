(define-module (crates-io ea sy easy-fs) #:use-module (crates-io))

(define-public crate-easy-fs-0.1.0 (c (n "easy-fs") (v "0.1.0") (h "0d3fvhpznq9my8vickn1cr4cpnv7ig272alwfpv5jbqw32p9i0rg")))

(define-public crate-easy-fs-0.1.1 (c (n "easy-fs") (v "0.1.1") (h "0mhymxca2yx1nhgv4lasf0w3ki0zk63asnb5y6bns8n29dfpjkia")))

