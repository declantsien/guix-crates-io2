(define-module (crates-io ea sy easyrand) #:use-module (crates-io))

(define-public crate-easyrand-0.1.0 (c (n "easyrand") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1xdmwqv4z2yap37qaaxiz3f2ppv6bqd1j35rwqppdc1w8psbqqxl")))

(define-public crate-easyrand-0.2.0 (c (n "easyrand") (v "0.2.0") (d (list (d (n "doc-comment") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1vr5x6d01jqq0bdgfyb8dzbsz7dac1fmqi3z20b8ccixrjl0zixn")))

(define-public crate-easyrand-0.3.0 (c (n "easyrand") (v "0.3.0") (d (list (d (n "doc-comment") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1s4822c31y2s8g56kjy8zb4sx2kbgza5ijv3rj7z74m79b7p02il")))

(define-public crate-easyrand-0.4.0 (c (n "easyrand") (v "0.4.0") (d (list (d (n "doc-comment") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "02g7m024pfpcd6d2hqcgy2ywmanvqv7ydxvwd11wi7gd6kyh8560")))

