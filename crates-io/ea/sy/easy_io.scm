(define-module (crates-io ea sy easy_io) #:use-module (crates-io))

(define-public crate-easy_io-0.1.0 (c (n "easy_io") (v "0.1.0") (h "0xd0ldj10z85glci00d7i66b2d0d57nns1mfj6amxxj3xd2cbdbs")))

(define-public crate-easy_io-0.2.0 (c (n "easy_io") (v "0.2.0") (h "1fp3fj2jygp76x3ylgl4zz5v1b63ya6mkya404lg0dpi2awhws1y")))

(define-public crate-easy_io-0.2.1 (c (n "easy_io") (v "0.2.1") (h "04hkbr8crcd3g704hiwfdwci9w75kgqfxrlg5ajfw5yr1s45v3wr")))

(define-public crate-easy_io-0.2.2 (c (n "easy_io") (v "0.2.2") (h "1j3p220qypz8w7h738jl6k7q9xk06ah33sg4gkih5srm03wifmvj")))

(define-public crate-easy_io-0.3.0 (c (n "easy_io") (v "0.3.0") (h "1f6qqninwjnwkqvd0r9yja651a9ljpsszjnvqw58321lm28rfijd")))

