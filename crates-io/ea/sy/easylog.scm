(define-module (crates-io ea sy easylog) #:use-module (crates-io))

(define-public crate-easylog-0.1.0 (c (n "easylog") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)))) (h "0qhisq413951zm2yjdh88pzdjb9cziam0w10igallli8fwml5sar")))

(define-public crate-easylog-0.2.0 (c (n "easylog") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)))) (h "0gisih0yhfqh24z72jdgxs9fg22067ph42c7id5f7mgrjk9ij158")))

(define-public crate-easylog-0.2.1 (c (n "easylog") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)))) (h "1pi0i230iqyimjc8fppgpji3q4cv01hkj1a7331kb9afn84zlkij")))

(define-public crate-easylog-0.2.2 (c (n "easylog") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)))) (h "029hn8j4ll4hwwlsznwyyz66hmq6kq08c5wgczr2nbk7b9rwhj02")))

(define-public crate-easylog-0.2.3 (c (n "easylog") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)))) (h "1q92a51igghdh2jj34kq6n05lghp7271xwngm0jajnzs40q5qzk9")))

(define-public crate-easylog-0.2.4 (c (n "easylog") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)))) (h "1gw6vizy25f6q8pvginpjxmfpl08l7iamwpc9gijcrz0b9zwid5j")))

(define-public crate-easylog-0.2.5 (c (n "easylog") (v "0.2.5") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)))) (h "11lx8jxg3piga946xj3mhhdjg5nnx46b8n0vvw7psf4g39n72rf7")))

(define-public crate-easylog-0.2.6 (c (n "easylog") (v "0.2.6") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)))) (h "0m00i65qa0x9klq93yw6s8q70h2kkn52y6cham7chz2lwfdy8msq")))

(define-public crate-easylog-0.2.7 (c (n "easylog") (v "0.2.7") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)))) (h "1b21sn49ihjff1883708x9w4xkd1a3mkgl0wgnhj5lcsz7njh6xf")))

(define-public crate-easylog-0.2.8 (c (n "easylog") (v "0.2.8") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)))) (h "1whi8kg3lfs70q58r8rchfp52fgqlg3llvra4f8bpyj5hssvdfys")))

(define-public crate-easylog-0.2.9 (c (n "easylog") (v "0.2.9") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)))) (h "0h0azij4nzm1f7avrlba6pgfwld4hnrrbhl8hkbbq42g0x750z85")))

(define-public crate-easylog-0.2.10 (c (n "easylog") (v "0.2.10") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)))) (h "1npvjxj1c2901bgazzxw3sv0inbqi94b75nh44klbngdf80ryyjq")))

(define-public crate-easylog-1.0.0 (c (n "easylog") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)))) (h "16dzqkk0giam2q10pjywnivpjscm4xcfivryq6h8z4fimlsd1h2d")))

(define-public crate-easylog-1.0.1 (c (n "easylog") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)))) (h "1dhns9dy649gzjpk0h64ly90h8i7fk4ns23bm81h5npmjf8zqpg1")))

(define-public crate-easylog-1.0.2 (c (n "easylog") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)))) (h "0cz165f93ksynlbq7q815y9a0jdjz0fmwz0bspvh782ilprjvp5h")))

(define-public crate-easylog-1.0.3 (c (n "easylog") (v "1.0.3") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)))) (h "1gds06czfr3kfywdand1k3xv1aw6s6i54n3sdp5syyk5wl3gjphg")))

(define-public crate-easylog-1.0.4 (c (n "easylog") (v "1.0.4") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)))) (h "1pk7maqbig8s8j65zs4bk4rk0xnxjjgzjjhfnby69mlsibgqjdf0")))

(define-public crate-easylog-1.0.5 (c (n "easylog") (v "1.0.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0ihr0y69bpislzlydrf4zgg81bjmm45zvj8db436528mzi9f3j88")))

(define-public crate-easylog-1.0.6 (c (n "easylog") (v "1.0.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0dggy4yg8l858wzx7hwhvzz01rzwmrdqg604z70i0r0sxh7jzpgr")))

(define-public crate-easylog-1.1.0 (c (n "easylog") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1mwfq5sa5yc89aby5ljhr6a4y5slg7p3bx4ynvmd984pq8pha0pv")))

(define-public crate-easylog-1.1.1 (c (n "easylog") (v "1.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "10y399g5smbldknb9ip8vp3vavk5i3w0n2gfva8d4dq7m8kmjjgp")))

(define-public crate-easylog-1.1.2 (c (n "easylog") (v "1.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0hjiwlmmrkii5sxgv0b8p4ga4wb8m288l6b1l6q12gls447pb0fr")))

(define-public crate-easylog-1.1.3 (c (n "easylog") (v "1.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "06r9k5yagbjfjf4lyb7zdbgpxa3y5dhmq57wirr6b3q69myhyxwb")))

(define-public crate-easylog-1.2.0 (c (n "easylog") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1igif63vv7q2d4hxz5q65jscpmd857fbrisq717vszgwdchj1lvp")))

(define-public crate-easylog-1.3.0 (c (n "easylog") (v "1.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0zbzfy5hkda70ivpg8f51dg19zv50lb46k8bm8847xxsjkmkgqck")))

(define-public crate-easylog-1.4.1 (c (n "easylog") (v "1.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0pxksbdi9cxnyj8vz54vdl6f9i0m92w76wp4kxi9bdpy64jm131a")))

(define-public crate-easylog-1.4.2 (c (n "easylog") (v "1.4.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1p1dlqla0pg507mrirw35kmfgzj9wjbscx2pgkli87fkl3ll12is")))

(define-public crate-easylog-1.4.3 (c (n "easylog") (v "1.4.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "09p18x05swrx3yrkwwq1z6mmrycb6ff5392irdlxdfzijx050nf6") (y #t)))

(define-public crate-easylog-1.4.4 (c (n "easylog") (v "1.4.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "16a6cz1b7yi1s0f0bhfbq43vch7qb4bkl8n0vl8idabn66qnh3vl")))

(define-public crate-easylog-1.4.5 (c (n "easylog") (v "1.4.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0x5q5g41v2xlxn73npj0rh5vma4mm6sc1qdnapxsyib5b0gkvxx4")))

(define-public crate-easylog-1.4.6 (c (n "easylog") (v "1.4.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "10xb32xdbycmy0hpmj8748hrqz8dv34bxl6n63hjfg6sbbni21yj")))

(define-public crate-easylog-2.0.0 (c (n "easylog") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1yh8g57gixi89ax3cmf0h15hrg31bgyw02cww3gsbr6f3zvy5qsp")))

(define-public crate-easylog-2.0.1 (c (n "easylog") (v "2.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "022b5mp2jmzqvs55ykms55x3hflb8nlf3vppzk7ry14z2dbmmryh")))

