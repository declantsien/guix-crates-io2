(define-module (crates-io ea sy easy_process) #:use-module (crates-io))

(define-public crate-easy_process-0.1.0 (c (n "easy_process") (v "0.1.0") (d (list (d (n "checked_command") (r "^0.2.2") (d #t) (k 0)) (d (n "cmdline_words_parser") (r "^0.0.2") (d #t) (k 0)))) (h "1imjlir2kwxphhwsbvi4611p50vxvzmvyid9kif53qw3j6jjcq8m")))

(define-public crate-easy_process-0.1.1 (c (n "easy_process") (v "0.1.1") (d (list (d (n "checked_command") (r "^0.2.2") (d #t) (k 0)) (d (n "cmdline_words_parser") (r "^0.0.2") (d #t) (k 0)))) (h "05zg0284jn91rvx653vdkib5x08libldf0k9djr4yc2ibv7d5k7m")))

(define-public crate-easy_process-0.1.2 (c (n "easy_process") (v "0.1.2") (d (list (d (n "checked_command") (r "^0.2.2") (d #t) (k 0)) (d (n "cmdline_words_parser") (r "^0.0.2") (d #t) (k 0)))) (h "1ikl4b4kvyzh7spc16df8ifiicp43w4clk0adw465ik35l3p9x9a")))

(define-public crate-easy_process-0.1.3 (c (n "easy_process") (v "0.1.3") (d (list (d (n "checked_command") (r "^0.2.2") (d #t) (k 0)) (d (n "cmdline_words_parser") (r "^0.0.2") (d #t) (k 0)))) (h "1jjdy6x94d0iazy0l0ax6j2pm4byk4s34xlay875f8pqp16ycmbs")))

(define-public crate-easy_process-0.1.4 (c (n "easy_process") (v "0.1.4") (d (list (d (n "checked_command") (r "^0.2.2") (d #t) (k 0)) (d (n "cmdline_words_parser") (r "^0.0.2") (d #t) (k 0)))) (h "1a2vdxria26znri561bxjdh7a67gxym113p4j8777fgyf2dfsxm6")))

(define-public crate-easy_process-0.1.5 (c (n "easy_process") (v "0.1.5") (d (list (d (n "checked_command") (r "^0.2.2") (d #t) (k 0)) (d (n "cmdline_words_parser") (r "^0.0.2") (d #t) (k 0)))) (h "16rvzrhwwb56i0mb92fps2k5d6lx4lg6vwpw0zfb0ddg543skiba")))

(define-public crate-easy_process-0.1.6 (c (n "easy_process") (v "0.1.6") (d (list (d (n "checked_command") (r "^0.2.2") (d #t) (k 0)) (d (n "cmdline_words_parser") (r "^0.1.0") (d #t) (k 0)))) (h "0kcsjixqd57wbqbsh3pcadrm8h0456yps8185y337ppkcm93hffv")))

(define-public crate-easy_process-0.1.7 (c (n "easy_process") (v "0.1.7") (d (list (d (n "checked_command") (r "^0.2.2") (d #t) (k 0)) (d (n "cmdline_words_parser") (r "^0.1.0") (d #t) (k 0)))) (h "1nv66ra7gixzackxfi2caw5c8nbiwky22dq90zqcx47hw7pffj4w")))

(define-public crate-easy_process-0.1.8 (c (n "easy_process") (v "0.1.8") (d (list (d (n "checked_command") (r "^0.2.2") (d #t) (k 0)) (d (n "cmdline_words_parser") (r "^0.2.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (f (quote ("display" "from" "error"))) (k 0)))) (h "16i3igbp2w7zjqv0bv2di27m9kpyzhphclshf7na45vwznf40igm")))

(define-public crate-easy_process-0.2.0 (c (n "easy_process") (v "0.2.0") (d (list (d (n "checked_command") (r "^0.2.2") (d #t) (k 0)) (d (n "cmdline_words_parser") (r "^0.2.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (f (quote ("display" "from" "error"))) (k 0)))) (h "0hnkczw2z67hwsfb2ap62k4hx5n7fmrhn5h35sb0yhwcyswj7xa6")))

(define-public crate-easy_process-0.2.1 (c (n "easy_process") (v "0.2.1") (d (list (d (n "checked_command") (r "^0.2.2") (d #t) (k 0)) (d (n "cmdline_words_parser") (r "^0.2.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.5") (f (quote ("display" "from" "error"))) (k 0)))) (h "191iy36c6ccg971xbh3shcn3cpv6yigc18mn6015z7z2vpsknr79")))

