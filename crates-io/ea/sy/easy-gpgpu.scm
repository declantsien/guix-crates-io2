(define-module (crates-io ea sy easy-gpgpu) #:use-module (crates-io))

(define-public crate-easy-gpgpu-0.1.0 (c (n "easy-gpgpu") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pollster") (r "^0.2") (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (d #t) (k 0)))) (h "0j30im89dha4xvc0rmalhgqfkr686pyzzv9l6gl2qdm2m7vsc9dz") (y #t)))

(define-public crate-easy-gpgpu-0.1.1 (c (n "easy-gpgpu") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pollster") (r "^0.2") (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (d #t) (k 0)))) (h "1c9m81jr1n6bh1wxzg7zsnllijywcq3q4h6jdm7a0v7855i626qi") (y #t)))

(define-public crate-easy-gpgpu-0.1.2 (c (n "easy-gpgpu") (v "0.1.2") (d (list (d (n "bytemuck") (r "^1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pollster") (r "^0.2") (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (d #t) (k 0)))) (h "04r4i32a8xclyv1c3m6mv8h5gpk209pyj73r7sv9dwdp2kkfdhy1")))

(define-public crate-easy-gpgpu-0.1.3 (c (n "easy-gpgpu") (v "0.1.3") (d (list (d (n "bytemuck") (r "^1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pollster") (r "^0.2") (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (d #t) (k 0)))) (h "170ji79i6g8fdr3j3ccn2ql7g1myx3kqga2p1fkpsk2aw04cyzb2")))

(define-public crate-easy-gpgpu-0.1.4 (c (n "easy-gpgpu") (v "0.1.4") (d (list (d (n "bytemuck") (r "^1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pollster") (r "^0.2") (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (d #t) (k 0)))) (h "18957kvpjr8jgvvy2y526b5x00arw2lg08aqa8hi3rsw7x7rh7ll")))

(define-public crate-easy-gpgpu-0.1.5 (c (n "easy-gpgpu") (v "0.1.5") (d (list (d (n "bytemuck") (r "^1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pollster") (r "^0.2") (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (d #t) (k 0)))) (h "0qdhbqvi0sd0bbp71y6fdyx96i5kwdnnqyjskxmkv9bj53fadbn1")))

(define-public crate-easy-gpgpu-0.1.6 (c (n "easy-gpgpu") (v "0.1.6") (d (list (d (n "bytemuck") (r "^1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pollster") (r "^0.2") (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (d #t) (k 0)))) (h "0wgza6a0jj984y5pyrn37vbfpw8yywszvkpsr9zl7j9fq2ph6vyx")))

