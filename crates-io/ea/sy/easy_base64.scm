(define-module (crates-io ea sy easy_base64) #:use-module (crates-io))

(define-public crate-easy_base64-0.1.0 (c (n "easy_base64") (v "0.1.0") (h "0d0gl05xanzcdzmfyx2h86byrciqhx9r7c8y4f7db2f3vbx6f5qf") (y #t) (r "1.56")))

(define-public crate-easy_base64-0.1.1 (c (n "easy_base64") (v "0.1.1") (h "0yy76zxpm4iv8raqgvy1c2cmz5hdjhqm585gdjd4zyg1dsv44rhc") (y #t) (r "1.56")))

(define-public crate-easy_base64-0.1.2 (c (n "easy_base64") (v "0.1.2") (h "0v7brwjb7kyjkjp9fjpyvd6izn6wf1fln7sk5mxjjhivim5xvv0p") (y #t) (r "1.56")))

(define-public crate-easy_base64-0.1.3 (c (n "easy_base64") (v "0.1.3") (h "0pnr613y29w1pw1995bxzfsv1kwjyc6qv3iww36vssxlajx27w0i") (f (quote (("default")))) (y #t) (r "1.56")))

(define-public crate-easy_base64-0.1.4 (c (n "easy_base64") (v "0.1.4") (h "14gj5a8nj7cc26sxg2kc6fdcgp0iya775yd3fc9bs6w00g83a1lc") (f (quote (("default")))) (y #t) (r "1.56")))

(define-public crate-easy_base64-0.1.5 (c (n "easy_base64") (v "0.1.5") (h "146c84q6fs0p38vp5xgac1c1sd72px1f769v8qgmyxpinl534mgd") (f (quote (("default")))) (y #t) (r "1.56")))

(define-public crate-easy_base64-0.1.6 (c (n "easy_base64") (v "0.1.6") (h "0cl0mi3jszd3vrjmv90db2zg3qhqgs0j1pf8h3a5g091v0hrxskm") (f (quote (("default")))) (y #t) (r "1.56")))

(define-public crate-easy_base64-0.1.7 (c (n "easy_base64") (v "0.1.7") (h "0msqc56l4x5wvb6klk6wz6ydy1wkvp6q7h5q0imv8gh72ycgwd31") (f (quote (("default")))) (r "1.56")))

