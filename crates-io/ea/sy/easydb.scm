(define-module (crates-io ea sy easydb) #:use-module (crates-io))

(define-public crate-easydb-0.1.0 (c (n "easydb") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "104bi6bp7rnjlaav8h5vlkfa193c9778vvcayrm710d4njffnprc")))

(define-public crate-easydb-0.2.0 (c (n "easydb") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "15mnq1ajf02v205ickma1l8jvavh3dm1zzhsnhhycbhjjlaki09g")))

