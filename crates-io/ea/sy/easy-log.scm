(define-module (crates-io ea sy easy-log) #:use-module (crates-io))

(define-public crate-easy-log-0.1.0 (c (n "easy-log") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0jzv8wa1f3d4yy4s7bpwa2vc954r2kdhw3zi24bngvk7ribslkja") (y #t)))

(define-public crate-easy-log-0.1.1 (c (n "easy-log") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0rfqvyw3cspp1m3yay5gxd9swqlw0xq0rav3bg28inr0xfd9sn8z")))

(define-public crate-easy-log-0.1.2 (c (n "easy-log") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0rjbcp6wzbh9897bxvf1gxina48i2bgsy6zv58cl4bk4p40hflkx")))

