(define-module (crates-io ea sy easy-min-max) #:use-module (crates-io))

(define-public crate-easy-min-max-0.1.0 (c (n "easy-min-max") (v "0.1.0") (h "0grc4mp9cl0xbn1w28byp18zz1hqizj7a6rmh4vrvvz5gimd25d9")))

(define-public crate-easy-min-max-0.1.1 (c (n "easy-min-max") (v "0.1.1") (h "1swfxrclz6c5zii5lkrv4v48is384f502bh6dgyshcb4q7fl376l")))

(define-public crate-easy-min-max-1.0.0 (c (n "easy-min-max") (v "1.0.0") (h "1m23hmz1sb6bfw58nbv3g2i233fdd8kjaqchkhwmlfim9ab7p5i7")))

