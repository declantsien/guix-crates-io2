(define-module (crates-io ea sy easy-signed-distance-field) #:use-module (crates-io))

(define-public crate-easy-signed-distance-field-0.1.0 (c (n "easy-signed-distance-field") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("png"))) (o #t) (k 0)) (d (n "ttf-parser") (r "^0.15.2") (o #t) (d #t) (k 0)))) (h "0b2dyz8yfpl4v0g3kmxrxmb2701n23k5b40x1q4nkvsmzz2nzcs8") (f (quote (("render") ("font" "ttf-parser") ("export" "image") ("default"))))))

(define-public crate-easy-signed-distance-field-0.1.1 (c (n "easy-signed-distance-field") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("png"))) (o #t) (k 0)) (d (n "ttf-parser") (r "^0.15.2") (o #t) (d #t) (k 0)))) (h "0bs7hhhqylgsf86qg0fnd1lf8sjxprndjgw8rvcwzxhxjxs5ppz1") (f (quote (("render") ("font" "ttf-parser") ("export" "image") ("default"))))))

