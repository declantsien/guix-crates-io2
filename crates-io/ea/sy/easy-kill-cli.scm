(define-module (crates-io ea sy easy-kill-cli) #:use-module (crates-io))

(define-public crate-easy-kill-cli-0.1.0 (c (n "easy-kill-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.1.0") (d #t) (k 0)) (d (n "easy-kill") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)))) (h "0v1xcflzd399sjqa22qahiccflx9sc32704ywja7zyyhcmh4swa6")))

(define-public crate-easy-kill-cli-0.1.1 (c (n "easy-kill-cli") (v "0.1.1") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.1.0") (d #t) (k 0)) (d (n "easy-kill") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)))) (h "0046gb0c8qhwhq00xvavy4vxfwb3k5xa0phppm6l5fig93rhqvaa")))

(define-public crate-easy-kill-cli-0.1.2 (c (n "easy-kill-cli") (v "0.1.2") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "console") (r "^0.6.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.1.0") (d #t) (k 0)) (d (n "easy-kill") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)))) (h "062f8nj1ha9f8lk40y78mzwwf11bixcfyc53k3ky1gynm0h56b5a")))

(define-public crate-easy-kill-cli-0.1.3 (c (n "easy-kill-cli") (v "0.1.3") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "console") (r "^0.6.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.1.0") (d #t) (k 0)) (d (n "easy-kill") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)))) (h "1x9izlx3r5d1m7wci8l40y7sqfigailkv0pszsy0hb5lh2hkyxj1")))

