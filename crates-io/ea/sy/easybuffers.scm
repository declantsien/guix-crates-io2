(define-module (crates-io ea sy easybuffers) #:use-module (crates-io))

(define-public crate-easybuffers-1.1.1 (c (n "easybuffers") (v "1.1.1") (d (list (d (n "time") (r "^0.1.39") (d #t) (k 0)))) (h "1x77dig41h85ap9pm5l4l5dbiipnknbqaqxq298si3wrj1lx80xp")))

(define-public crate-easybuffers-2.0.0 (c (n "easybuffers") (v "2.0.0") (d (list (d (n "time") (r "^0.1.39") (d #t) (k 0)))) (h "02mscqc56jrv2h3jr346f4x7ncmik9yxhirbmmvd0klyns94scav")))

(define-public crate-easybuffers-2.0.1 (c (n "easybuffers") (v "2.0.1") (d (list (d (n "time") (r "^0.1.39") (d #t) (k 0)))) (h "1rgys50fxlg0k3vlf6wcsw36xck4cxqm4c1mbh9szpam0516wb1y")))

(define-public crate-easybuffers-2.0.2 (c (n "easybuffers") (v "2.0.2") (d (list (d (n "time") (r "^0.1.39") (d #t) (k 0)))) (h "1z4m6pczx470cja799dbg7kii74py757q7wy4crp67plf731zpc8")))

