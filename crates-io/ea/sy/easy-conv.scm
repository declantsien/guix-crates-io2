(define-module (crates-io ea sy easy-conv) #:use-module (crates-io))

(define-public crate-easy-conv-0.1.0 (c (n "easy-conv") (v "0.1.0") (h "04sk8r4fsriafv7700mw6115f6fhdbiccv4jxq5ws8fnq8x5c23w")))

(define-public crate-easy-conv-0.1.1 (c (n "easy-conv") (v "0.1.1") (h "1x50zx7z6dmkvyprmyjz2gvc5hgjjgnkl8q1lghgni0q3i9pni1x")))

(define-public crate-easy-conv-0.1.2 (c (n "easy-conv") (v "0.1.2") (h "1573xd1ih8888w2fkn7sh05kyxkhpmsahd3zv3hrnadkkw6cbg33")))

