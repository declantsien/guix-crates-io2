(define-module (crates-io ea sy easy-assert) #:use-module (crates-io))

(define-public crate-easy-assert-0.1.0 (c (n "easy-assert") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0513bdz080i71hzj6f6didwzyq3wcxjmgm9sc1by8pm0smpq5l5l")))

(define-public crate-easy-assert-0.1.2 (c (n "easy-assert") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "13sgl1g19sg0mxmvb83bkwmnl28acg1bgafm3n2nxdrgb2rr7cba")))

(define-public crate-easy-assert-0.1.4 (c (n "easy-assert") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1gavmvh9i5hycjs5ydkv6kcbkx696gimlnim82vsjq8zlzgsp720")))

(define-public crate-easy-assert-0.1.5 (c (n "easy-assert") (v "0.1.5") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0qjfxki5f4sdqagds495vl8b8q1jfk9n9mw3hwrdcycgzb5mvpra")))

(define-public crate-easy-assert-0.2.1 (c (n "easy-assert") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "14rm0dfbi2j7rb3p646va3c4dcp15fq93xhrw5084nrwz8ivyyz7")))

