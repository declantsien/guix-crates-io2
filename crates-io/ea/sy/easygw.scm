(define-module (crates-io ea sy easygw) #:use-module (crates-io))

(define-public crate-easyGW-0.0.1 (c (n "easyGW") (v "0.0.1") (d (list (d (n "softbuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "winit") (r "^0.29.7") (d #t) (k 0)))) (h "1q71d8slpk0kj95d154axlcmfi86m79igjilcwisn340n60x2y56") (y #t)))

(define-public crate-easyGW-0.0.2 (c (n "easyGW") (v "0.0.2") (d (list (d (n "softbuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "winit") (r "^0.29.7") (d #t) (k 0)))) (h "10d8g3ch8vksha6jm1572g4wbkvbgw3af7lcfmb7kawvil5bm8vf") (y #t)))

(define-public crate-easyGW-0.0.3 (c (n "easyGW") (v "0.0.3") (d (list (d (n "softbuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "winit") (r "^0.29.7") (d #t) (k 0)))) (h "1g6cx331g9ppzq7gqm211mrla436wdri3zcs3iyxl4fgnpl1rnbs") (y #t)))

(define-public crate-easyGW-0.0.4 (c (n "easyGW") (v "0.0.4") (d (list (d (n "winit") (r "^0.29.7") (d #t) (k 0)))) (h "0d2ra5pj0d4j784h1n9lql79f8ab7q0jk0zi2b1pa5wjf3j22dlg")))

(define-public crate-easyGW-0.0.5 (c (n "easyGW") (v "0.0.5") (d (list (d (n "softbuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "winit") (r "^0.29.7") (d #t) (k 0)))) (h "1k5sw59ff8k0jbfh1q4fw315w3hxy44pxinlsc45n7x8avzpfhpn") (y #t)))

(define-public crate-easyGW-0.0.6 (c (n "easyGW") (v "0.0.6") (d (list (d (n "softbuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "winit") (r "^0.29.7") (d #t) (k 0)))) (h "0sfxkkm0vr7x0z9p1d2lq8a5dviv1xph5pnrwhliwzjvcbq3z965")))

(define-public crate-easyGW-0.0.7 (c (n "easyGW") (v "0.0.7") (d (list (d (n "softbuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "winit") (r "^0.29.7") (d #t) (k 0)))) (h "1dnydc9iprp55rj5bw19c0im8v6xyi069ilpwr1gvn58zm7p1fyv")))

(define-public crate-easyGW-0.0.8 (c (n "easyGW") (v "0.0.8") (d (list (d (n "softbuffer") (r "^0.4.0") (d #t) (k 0)) (d (n "winit") (r "^0.29.7") (d #t) (k 0)))) (h "1qqjwm28j0ags2pav16hfa2wfash7v9858d9kx9m81vc9fgy75kp")))

(define-public crate-easyGW-0.0.9-test (c (n "easyGW") (v "0.0.9-test") (d (list (d (n "beryllium") (r "^0.13.3") (d #t) (k 0)))) (h "08zlp4kkrm8ckh88601jqf1cla1wj3bgy11wnqs36gpjh0nq8s4w") (y #t)))

(define-public crate-easyGW-0.0.10-test (c (n "easyGW") (v "0.0.10-test") (d (list (d (n "beryllium") (r "^0.13.3") (d #t) (k 0)))) (h "04fyf2cis2h8b8c4q62c1mw6sq5mdk0fiwpspiij7vi92ca3h8vm") (y #t)))

