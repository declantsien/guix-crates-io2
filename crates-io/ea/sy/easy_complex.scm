(define-module (crates-io ea sy easy_complex) #:use-module (crates-io))

(define-public crate-easy_complex-0.2.0 (c (n "easy_complex") (v "0.2.0") (h "0g7n0awnzjksnm94yp2jmr5jnq3fhk552rg9jm1xxhblz753rg3f") (y #t)))

(define-public crate-easy_complex-0.2.5 (c (n "easy_complex") (v "0.2.5") (h "0gblmm05bg71fgiwn450k81263395jgp0z4m4fmwpr1icxmai0pg") (y #t)))

(define-public crate-easy_complex-0.3.0 (c (n "easy_complex") (v "0.3.0") (h "0xahhxw6dfaq1f1p0c54ajvflgf4c1snz20j9rjiq3g650g2mbrb") (y #t)))

(define-public crate-easy_complex-0.3.1 (c (n "easy_complex") (v "0.3.1") (h "03yzbzppgb7ljndcn9fa1bsvx20gm0xwdiw14kgbkbhlmcspdh2a")))

(define-public crate-easy_complex-0.3.2 (c (n "easy_complex") (v "0.3.2") (h "1pqhf3r3hv0f6b7a5pcif9nka7ws1p0l5lby29kfnawk0wpnrliq")))

(define-public crate-easy_complex-0.3.3 (c (n "easy_complex") (v "0.3.3") (h "0ib39vjrw7mkyl88lzzj0fjsv8rgg79f0jvv1ykbb59y9cbwaacb")))

(define-public crate-easy_complex-0.3.4 (c (n "easy_complex") (v "0.3.4") (h "183clzw6f5k8zxm0kqad8daxsmwm3jsm6xc8fkd5dc7055kj76x5")))

(define-public crate-easy_complex-0.4.1 (c (n "easy_complex") (v "0.4.1") (h "1xn8ydn0fijhy57dgx8q36w30f4aw3xm2gvycadxvc370f6hzkw1")))

