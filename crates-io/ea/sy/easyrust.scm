(define-module (crates-io ea sy easyrust) #:use-module (crates-io))

(define-public crate-easyrust-0.1.0 (c (n "easyrust") (v "0.1.0") (h "0vbdd0fbj3vaaj8v51zrbr430i8ybj5yzd3xwj8xf665bnn9rc3r") (y #t)))

(define-public crate-easyrust-0.1.1 (c (n "easyrust") (v "0.1.1") (h "1qjn960hlkick9srcgds1qmi56xnxkizv0rv37pqm5d798sn03aw")))

