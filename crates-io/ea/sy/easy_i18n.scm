(define-module (crates-io ea sy easy_i18n) #:use-module (crates-io))

(define-public crate-easy_i18n-0.1.0 (c (n "easy_i18n") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0zg0y5q4pm910b7hkrxgvv5cxnwl9s6yxv4nfx6c22zwirr8mx75")))

(define-public crate-easy_i18n-0.1.1 (c (n "easy_i18n") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0hpf2m7da52zkdwnlkk2rcdpqxr703y40k7wvgwp7g178g4ngpm4")))

