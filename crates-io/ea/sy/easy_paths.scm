(define-module (crates-io ea sy easy_paths) #:use-module (crates-io))

(define-public crate-easy_paths-0.1.0 (c (n "easy_paths") (v "0.1.0") (d (list (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "0xrmgn4l73lw552dzxp3lff8zjr45ghjnhslx5bxah5zc8fz4zmx")))

(define-public crate-easy_paths-0.1.1 (c (n "easy_paths") (v "0.1.1") (d (list (d (n "shellexpand") (r "^3.0.0") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)))) (h "04ac971iayyh95lb2h2810cvp8mg8nw7mcjv4djfs9li6jzxs4qa")))

