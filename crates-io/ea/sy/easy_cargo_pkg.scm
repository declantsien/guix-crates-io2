(define-module (crates-io ea sy easy_cargo_pkg) #:use-module (crates-io))

(define-public crate-easy_cargo_pkg-0.1.0 (c (n "easy_cargo_pkg") (v "0.1.0") (h "11hk1kb1vgrzf19x110mfi5202p331xl42jpcyl4byzr0z23l6bd")))

(define-public crate-easy_cargo_pkg-1.0.0 (c (n "easy_cargo_pkg") (v "1.0.0") (h "1aa4z6mkl1hq0ay47znb4zfs7lzk3hpw5lscly5n2hqvk9c9hagp")))

(define-public crate-easy_cargo_pkg-1.0.0-patch (c (n "easy_cargo_pkg") (v "1.0.0-patch") (d (list (d (n "easy_cargo_dep_A") (r "^1.0.0") (d #t) (k 0)) (d (n "easy_cargo_dep_B") (r "^1.1.0") (d #t) (k 0)))) (h "0q1x7iq94wixkpdxjw6405bk5xxzn47n561mq4ydxwdi9f0ixm8m")))

(define-public crate-easy_cargo_pkg-2.0.0 (c (n "easy_cargo_pkg") (v "2.0.0") (d (list (d (n "easy_cargo_dep_A") (r "^1.0.0") (d #t) (k 0)) (d (n "easy_cargo_dep_B") (r "^1.1.0") (d #t) (k 0)))) (h "1b65q72inn00f0wlcycnmp4b5h4xkizlins4744nj9fi5b4snjck")))

