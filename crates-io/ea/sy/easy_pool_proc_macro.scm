(define-module (crates-io ea sy easy_pool_proc_macro) #:use-module (crates-io))

(define-public crate-easy_pool_proc_macro-0.1.0 (c (n "easy_pool_proc_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1zgsn4khhyp5w0m9vmgwac6w7yvja0gycbmgb0hc0ygvk47p4kmz")))

