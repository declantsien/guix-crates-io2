(define-module (crates-io ea sy easy_proc) #:use-module (crates-io))

(define-public crate-easy_proc-0.1.0 (c (n "easy_proc") (v "0.1.0") (d (list (d (n "easy_proc_common") (r "^0.1.0") (d #t) (k 0)) (d (n "easy_proc_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "1k8fc04wjzcnyk694qhl411p03fny0f5zq661vzk21080cn8gdn1")))

(define-public crate-easy_proc-0.1.1 (c (n "easy_proc") (v "0.1.1") (d (list (d (n "easy_proc_common") (r "^0.1.0") (d #t) (k 0)) (d (n "easy_proc_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "07qxb4qh7h9k26c43vhpa6kpvp8bh0ml51alkyvf72d8hxw87mnz")))

(define-public crate-easy_proc-0.2.0 (c (n "easy_proc") (v "0.2.0") (d (list (d (n "easy_proc_common") (r "^0.2.0") (d #t) (k 0)) (d (n "easy_proc_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "1g06hcjys2y8a7hficwlh28lrnqv1ysy18nbvcgr9bjswxfymp0a")))

(define-public crate-easy_proc-0.3.0 (c (n "easy_proc") (v "0.3.0") (d (list (d (n "easy_proc_common") (r "^0.3.0") (d #t) (k 0)) (d (n "easy_proc_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (d #t) (k 0)))) (h "03svcjn2kdcvbsz15rrp2parxy89qs3h75lz3qanbsivyf1qx08h")))

