(define-module (crates-io ea sy easy-modbus) #:use-module (crates-io))

(define-public crate-easy-modbus-0.0.3 (c (n "easy-modbus") (v "0.0.3") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.0") (f (quote ("codec"))) (d #t) (k 0)))) (h "1j4pfl0l6mdh4cf5lxh1fcr2xpzvzsnwwrxqdnii0y3rlzp5rjcn")))

(define-public crate-easy-modbus-0.0.4 (c (n "easy-modbus") (v "0.0.4") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-serial") (r "^5.4.1") (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.0") (f (quote ("codec"))) (d #t) (k 0)))) (h "1gl5a4qgwspb93gkg3d1vv47ckisjbvvq9nfci3sn2fql7588gxx")))

(define-public crate-easy-modbus-0.0.5 (c (n "easy-modbus") (v "0.0.5") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-serial") (r "^5.4.1") (d #t) (k 2)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 2)) (d (n "tokio-util") (r "^0.7.0") (f (quote ("codec"))) (d #t) (k 0)))) (h "1zmja4b69y3f46smbyqn8ysvh1vi4blxr2q5v7i17jmsiy09zn73")))

