(define-module (crates-io ea sy easy_cargo_dep_b) #:use-module (crates-io))

(define-public crate-easy_cargo_dep_B-0.1.0 (c (n "easy_cargo_dep_B") (v "0.1.0") (h "1f79f35nl1rbagzdrphqqr88azl7dzn2ji4303zy6x7zadqzpixp")))

(define-public crate-easy_cargo_dep_B-0.1.1 (c (n "easy_cargo_dep_B") (v "0.1.1") (h "00wnwiqc1h80mq988hs3ic2bqnwz5pz02lw8fq43rpa4dz64bd7h")))

(define-public crate-easy_cargo_dep_B-1.0.0 (c (n "easy_cargo_dep_B") (v "1.0.0") (h "1wmqdziss06sb0jipx6jbnddcsz8csyxk1aa4p6920yqzz9v32as")))

(define-public crate-easy_cargo_dep_B-1.1.0 (c (n "easy_cargo_dep_B") (v "1.1.0") (h "1c5dslsbdl6cf9zkp81s34rq4i95ahs0l2jdsqgza5mjdm3wvdzz")))

(define-public crate-easy_cargo_dep_B-2.0.0 (c (n "easy_cargo_dep_B") (v "2.0.0") (h "0ksqhf6l8svawb60vrhqzjar1s3zpvxk6rfs2kvkzr7i0ziwr3xc")))

(define-public crate-easy_cargo_dep_B-2.1.0 (c (n "easy_cargo_dep_B") (v "2.1.0") (h "0ihm87ghllyc93p5zadf61ckjg189w9rdnl00gs3zvawd0c49bi2")))

(define-public crate-easy_cargo_dep_B-2.1.1 (c (n "easy_cargo_dep_B") (v "2.1.1") (h "1zszxamwigjiaamj4bx8d7s0z2mg8ij118k8wqpm97d7rwl49i2a")))

(define-public crate-easy_cargo_dep_B-2.1.2 (c (n "easy_cargo_dep_B") (v "2.1.2") (h "11jrhpgdk7yrndk6jdxxsp1q3f6qsl5n8d21yg9awcipqg9v9jyf")))

(define-public crate-easy_cargo_dep_B-3.0.0 (c (n "easy_cargo_dep_B") (v "3.0.0") (h "173nm1q9i51y10yw0k3zj2blj9n4f3gnp3b0ldisj56hd7a7gczi")))

(define-public crate-easy_cargo_dep_B-3.1.0 (c (n "easy_cargo_dep_B") (v "3.1.0") (h "06rfcqradbjdilsk57gz8iiikbdihpsf9k6rkl443px386jxhydv")))

(define-public crate-easy_cargo_dep_B-3.1.1 (c (n "easy_cargo_dep_B") (v "3.1.1") (h "0s5gjgyi56354qdvjklyyg62m30m3j11764s4rmn0djkx0jwc4di")))

