(define-module (crates-io ea sy easy_node) #:use-module (crates-io))

(define-public crate-easy_node-0.1.0 (c (n "easy_node") (v "0.1.0") (d (list (d (n "drop_tracer") (r "^0.1") (d #t) (k 2)))) (h "19icfmjayzdcqv0gnyxfijk01ym7wzcn5iyn6vayygs91rq92f22")))

(define-public crate-easy_node-0.2.0 (c (n "easy_node") (v "0.2.0") (d (list (d (n "drop_tracer") (r "^0.1") (d #t) (k 2)))) (h "0zx0kw33k3ai8jay3ccn3zkfzih8r77l990796v2dn5d2vjz9ysh")))

