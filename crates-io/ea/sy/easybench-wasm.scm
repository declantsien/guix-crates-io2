(define-module (crates-io ea sy easybench-wasm) #:use-module (crates-io))

(define-public crate-easybench-wasm-0.2.0 (c (n "easybench-wasm") (v "0.2.0") (d (list (d (n "humantime") (r "^1.3") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2.50") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Performance"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 2)))) (h "0l2hlnj377fg0a8icd18cj16rh3mi4cbm6f0yap4h6r1w7acf5d3")))

(define-public crate-easybench-wasm-0.2.1 (c (n "easybench-wasm") (v "0.2.1") (d (list (d (n "humantime") (r "^1.3") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2.50") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Performance"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 2)))) (h "07diiianqraq5hpp7ki4hb73fhn9yz78p5b0j3vr7750larx8rdi")))

