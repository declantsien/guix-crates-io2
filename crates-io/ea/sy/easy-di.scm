(define-module (crates-io ea sy easy-di) #:use-module (crates-io))

(define-public crate-easy-di-1.0.0-beta.2 (c (n "easy-di") (v "1.0.0-beta.2") (d (list (d (n "ahash") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jvbqysjf068fa0hh864mik97fx6nzjw5z9c2s0wn89lqmgn33l0")))

(define-public crate-easy-di-1.0.0-beta.3 (c (n "easy-di") (v "1.0.0-beta.3") (d (list (d (n "ahash") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "totems") (r "^0.2.7") (d #t) (k 2)))) (h "0id3977cvpxhjz93v9cliils7f0h53g2acdxnd5q190d84cs8yr0")))

