(define-module (crates-io ea sy easy-jsonrpc-proc-macro-mw) #:use-module (crates-io))

(define-public crate-easy-jsonrpc-proc-macro-mw-0.5.1 (c (n "easy-jsonrpc-proc-macro-mw") (v "0.5.1") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full"))) (d #t) (k 0)))) (h "09179x8a44by3hs0dzckay89ip3xj54lfskfzj2gp1b65jyqsdm6")))

