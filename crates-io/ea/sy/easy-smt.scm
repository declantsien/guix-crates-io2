(define-module (crates-io ea sy easy-smt) #:use-module (crates-io))

(define-public crate-easy-smt-0.1.0 (c (n "easy-smt") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.0") (d #t) (k 0)))) (h "044hl0p8f8xjnyncnmq7bqwckjcx57ixcn10z5px759065fld57p")))

(define-public crate-easy-smt-0.1.1 (c (n "easy-smt") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.0") (d #t) (k 0)))) (h "03by84mx54zhlj4041sn02z2nahnxvijy8dji7z52857lgyk1hdp")))

(define-public crate-easy-smt-0.1.2 (c (n "easy-smt") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.0") (d #t) (k 0)))) (h "0vqakpqchpdjny30nwijdlia6kav0wgp2774ly7r5rp13mchsr2v")))

(define-public crate-easy-smt-0.2.0 (c (n "easy-smt") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.0") (d #t) (k 0)))) (h "0dmi6nrmahln5fdhqsg40kzll2lfrrskr969yhh3jmnllfss1yqw")))

(define-public crate-easy-smt-0.2.1 (c (n "easy-smt") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.0") (d #t) (k 0)))) (h "1f76mmyfx2cwbwhsnpkw11jbbhdqsl2ck50c9b8hbxi4vmhghn3y")))

