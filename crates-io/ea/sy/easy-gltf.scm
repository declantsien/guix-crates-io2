(define-module (crates-io ea sy easy-gltf) #:use-module (crates-io))

(define-public crate-easy-gltf-0.1.0 (c (n "easy-gltf") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "gltf") (r "^0.15.2") (f (quote ("KHR_lights_punctual"))) (d #t) (k 0)) (d (n "image") (r "^0.23.6") (d #t) (k 0)))) (h "0gyvmx9wcc20201d2armq4c3p14gzhgfbxaif73lp7xbz67vyk7m")))

(define-public crate-easy-gltf-0.1.1 (c (n "easy-gltf") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "gltf") (r "^0.15.2") (f (quote ("KHR_lights_punctual"))) (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)))) (h "07axvpk8rj9b1pwdy7ai41l31vcsgszmbcki4r8wsdlp6blxz92j")))

(define-public crate-easy-gltf-0.1.2 (c (n "easy-gltf") (v "0.1.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "gltf") (r "^0.15.2") (f (quote ("KHR_lights_punctual"))) (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)))) (h "1ka5dy8jd5x0f9rjnlk78xzlz8pb4kfys82hnzbcv84m8fww834b")))

(define-public crate-easy-gltf-0.1.3 (c (n "easy-gltf") (v "0.1.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "gltf") (r "^0.15.2") (f (quote ("KHR_lights_punctual"))) (d #t) (k 0)) (d (n "image") (r "^0.23.12") (d #t) (k 0)))) (h "1ncx28pv0szq4js6l9cfaz37vjnh5bza4asf6kgfrmxrsz7mlqy5")))

(define-public crate-easy-gltf-0.1.4 (c (n "easy-gltf") (v "0.1.4") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "gltf") (r "^0.15.2") (f (quote ("KHR_lights_punctual"))) (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "0nkya5zacn7im37c1iwcwgdg5jzn5fxd1c93136fc2s3c9dcsc3l")))

(define-public crate-easy-gltf-0.1.5 (c (n "easy-gltf") (v "0.1.5") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "gltf") (r "^1.0.0") (f (quote ("KHR_lights_punctual"))) (d #t) (k 0)) (d (n "image") (r "^0.24.1") (d #t) (k 0)))) (h "0l6a6xs3bjf8dv267n76xi5hv2x8swgpdb0llrrpmxhgfipx7s9r")))

(define-public crate-easy-gltf-1.0.0 (c (n "easy-gltf") (v "1.0.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "gltf") (r "^1.0.0") (f (quote ("KHR_lights_punctual"))) (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)))) (h "0mrglbs32yy87jl18jfvnzs6cg71iy4hq416hwifxp2jbsqbdg0b")))

(define-public crate-easy-gltf-1.1.0 (c (n "easy-gltf") (v "1.1.0") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "gltf") (r "^1.2.0") (f (quote ("KHR_lights_punctual"))) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)))) (h "0nryz96ahlgxiy3vr120qhlfscrdq21dn9sbdcm1m1l49whvv7v5") (f (quote (("names" "gltf/names") ("extras" "gltf/extras") ("default"))))))

(define-public crate-easy-gltf-1.1.1 (c (n "easy-gltf") (v "1.1.1") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "gltf") (r "^1.3.0") (f (quote ("KHR_lights_punctual"))) (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)))) (h "0mb1791bb7bb1fgmx3szk0ghgl6h8f07l1l4niyrqb7sv7vyq26p") (f (quote (("names" "gltf/names") ("extras" "gltf/extras") ("default"))))))

(define-public crate-easy-gltf-1.1.2 (c (n "easy-gltf") (v "1.1.2") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "gltf") (r "^1.4.0") (f (quote ("KHR_lights_punctual"))) (d #t) (k 0)) (d (n "image") (r "^0.25.0") (d #t) (k 0)))) (h "1pm3rpxj4a98x5v7g960gmaf7i2bqyq2pblydpi7x74yjrian4iw") (f (quote (("names" "gltf/names") ("extras" "gltf/extras") ("default"))))))

