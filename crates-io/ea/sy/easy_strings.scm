(define-module (crates-io ea sy easy_strings) #:use-module (crates-io))

(define-public crate-easy_strings-0.1.0 (c (n "easy_strings") (v "0.1.0") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "owning_ref") (r "^0.2.4") (d #t) (k 0)))) (h "1ig2c1q2h8xmqbkb0gad14mq6w7p7wrrz78d9xjxqpzd48h9bm0w")))

(define-public crate-easy_strings-0.2.0 (c (n "easy_strings") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "owning_ref") (r "^0.3.3") (d #t) (k 0)))) (h "123j3dw8xl5f2bg170i0bihz6v6sjdb6dr1061zg7qgpdqa5fsib")))

