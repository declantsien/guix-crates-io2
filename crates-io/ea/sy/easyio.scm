(define-module (crates-io ea sy easyio) #:use-module (crates-io))

(define-public crate-easyio-0.1.0 (c (n "easyio") (v "0.1.0") (h "0kkrb8dq1cg684x1121171yi4f0cg4vf9g0hs3h30l4bkn234r43")))

(define-public crate-easyio-0.2.0 (c (n "easyio") (v "0.2.0") (h "11z0rpwjmm5qiskrwkh5kzahmfjjxs3gbk53zyyc7sbvrbsi73rc")))

