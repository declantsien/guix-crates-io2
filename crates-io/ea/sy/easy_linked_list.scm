(define-module (crates-io ea sy easy_linked_list) #:use-module (crates-io))

(define-public crate-easy_linked_list-0.1.0 (c (n "easy_linked_list") (v "0.1.0") (h "1h3aqa5wzgwg2mxlzg0hj60cx8355f3vb81gfr7nb2cca036pqxa")))

(define-public crate-easy_linked_list-0.1.1 (c (n "easy_linked_list") (v "0.1.1") (h "0730kg94ardpliqx5lqixcx3gdd2m1vxgfldlz4k0cz7r1vd29ny")))

(define-public crate-easy_linked_list-0.1.2 (c (n "easy_linked_list") (v "0.1.2") (h "02prkpd8sy5g3834wm97ziarlqz5wr4rgq80ifcg5b49hqd01vr6")))

(define-public crate-easy_linked_list-0.1.3 (c (n "easy_linked_list") (v "0.1.3") (h "1z430qzriqkdsbh6slziq37r8003012amk01mpff7v54jxh04z98")))

(define-public crate-easy_linked_list-0.1.4 (c (n "easy_linked_list") (v "0.1.4") (h "1acm9f5wxfjlldgkr64ndw6d2b380miym0ci99i3jk7sbc8fxbmj")))

