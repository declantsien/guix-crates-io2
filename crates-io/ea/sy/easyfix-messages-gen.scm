(define-module (crates-io ea sy easyfix-messages-gen) #:use-module (crates-io))

(define-public crate-easyfix-messages-gen-0.1.0 (c (n "easyfix-messages-gen") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0c6wm9wrflkmzs0dc9b96i314bknz7w6mnvc7hmpp1l3glmdl3as")))

(define-public crate-easyfix-messages-gen-0.1.1 (c (n "easyfix-messages-gen") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "120zxsr7g4ly2mycwm4652w8bx5hahqnkh1k5afm3k7lbiq7s79g")))

(define-public crate-easyfix-messages-gen-0.1.2 (c (n "easyfix-messages-gen") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "02i9xp8pgrw3b0d03igip5dcg0kgflf27771qcxs8fcalx0smx5h")))

(define-public crate-easyfix-messages-gen-0.1.3 (c (n "easyfix-messages-gen") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1dcbfp5n9p9jkqb15i1a64zkrhqa807n23c25ah920as5mwhl6xq")))

(define-public crate-easyfix-messages-gen-0.1.4 (c (n "easyfix-messages-gen") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1b4jkzx1hb4xbqp8ga9l7q4y37d3lzmf2312cqfiqx46c4wqqwl6")))

(define-public crate-easyfix-messages-gen-0.1.5 (c (n "easyfix-messages-gen") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "165kp81zldl6nvsxdz8z02cs27w7sw4xcgxncwc0c2q3jbma41rk")))

(define-public crate-easyfix-messages-gen-0.1.6 (c (n "easyfix-messages-gen") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0j0jbv2jfz010rr1ljlws7310i752f154jzw9wygaql1ip7iwq3m")))

(define-public crate-easyfix-messages-gen-0.1.7 (c (n "easyfix-messages-gen") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "19jfram97zdd827y6daj9yji2x49919cr06swkmxg9g8lq1mc2yn")))

(define-public crate-easyfix-messages-gen-0.1.8 (c (n "easyfix-messages-gen") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0hjqrbjr07h55acc0p62ly3j5yik1zfjgvr1lfz2qs7zqdhw2bw5")))

(define-public crate-easyfix-messages-gen-0.1.9 (c (n "easyfix-messages-gen") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "05knb499c41sxk7wcwvq7g8wsv2flksnsjfyzvacjhq0m4qcabql")))

(define-public crate-easyfix-messages-gen-0.1.10 (c (n "easyfix-messages-gen") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0dshi058f6wa79lw14sy28fa5prkiikn7xmadxsk3aq8zpd5wskv")))

(define-public crate-easyfix-messages-gen-0.1.11 (c (n "easyfix-messages-gen") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1z46qq41k6kyvyc6acq26p7za6zbww6g828hclrhvkspakp2cbjr")))

(define-public crate-easyfix-messages-gen-0.2.0 (c (n "easyfix-messages-gen") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0zm1af8h5qzmxklz9b8xhyi8f08rw1gix0v7fvlvp34wgpq6g53r")))

(define-public crate-easyfix-messages-gen-0.3.0 (c (n "easyfix-messages-gen") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "01v498irwk4bccry8jkjwzczlh5hq726wm91hrsm4ip14przg33v")))

(define-public crate-easyfix-messages-gen-0.4.0 (c (n "easyfix-messages-gen") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1wvnf6vhfrqh7dg6m58g9aaali9jv1ipi2g3acgplqmp2iwsfawd")))

(define-public crate-easyfix-messages-gen-0.5.0 (c (n "easyfix-messages-gen") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1im3anxq1i6lvvgxg8jw24r4c8q58jhkcfn6ywzar176p7kkigmr")))

(define-public crate-easyfix-messages-gen-0.5.1 (c (n "easyfix-messages-gen") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1ir2ph6ayqq8fl9jxmxi28j8sp0irbj195n4f47nni1xrxmksq7b")))

(define-public crate-easyfix-messages-gen-0.5.2 (c (n "easyfix-messages-gen") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1q0h6z6a3n8ja6vzcwrcxls0x9ymal8ar9dw4q87pkw2vbsc0gjw")))

(define-public crate-easyfix-messages-gen-0.5.3 (c (n "easyfix-messages-gen") (v "0.5.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "135bpkwi9zz18r12sab0vfh77659rn30gapd4q9dp80g8cqmv87z")))

