(define-module (crates-io ea sy easy-shortcuts) #:use-module (crates-io))

(define-public crate-easy-shortcuts-0.1.0 (c (n "easy-shortcuts") (v "0.1.0") (h "0gl1ghivf22w13f74zdk34r2ri379q9jar939gm9f0r2sv1whhi0")))

(define-public crate-easy-shortcuts-0.2.0 (c (n "easy-shortcuts") (v "0.2.0") (h "1hbx148id3hl07f384qninx8cdsvhwdfmfg1jfll7krd8rrrs78s")))

(define-public crate-easy-shortcuts-0.3.0 (c (n "easy-shortcuts") (v "0.3.0") (h "1fg9n9z9m44951vyp2r3z07j5dyhyx8pl5jlq8yf16vyshskv8xg")))

