(define-module (crates-io ea sy easygpu-lyon) #:use-module (crates-io))

(define-public crate-easygpu-lyon-0.0.1 (c (n "easygpu-lyon") (v "0.0.1") (d (list (d (n "easygpu") (r "^0.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "lyon_tessellation") (r "^0.16") (d #t) (k 0)) (d (n "winit") (r "^0.23") (d #t) (k 2)))) (h "19h4fvnvij7x3b6p599xzsi4sxjriydy4ac8h1q10l26yqb4napn")))

(define-public crate-easygpu-lyon-0.0.2 (c (n "easygpu-lyon") (v "0.0.2") (d (list (d (n "easygpu") (r "^0.0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "lyon_tessellation") (r "^0.16") (d #t) (k 0)) (d (n "winit") (r "^0.23") (d #t) (k 2)))) (h "037kqxqi1i3844pbm2vw4y6zvxaix0g1zz0iw9aqifz7d1g0limx")))

(define-public crate-easygpu-lyon-0.0.3 (c (n "easygpu-lyon") (v "0.0.3") (d (list (d (n "easygpu") (r "^0.0.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "lyon_tessellation") (r "^0.16") (d #t) (k 0)) (d (n "winit") (r "^0.23") (d #t) (k 2)))) (h "14bi96l53qy8zrmyjrkg47klg6la1csgwyyccf72dw9sqnkq36hx")))

(define-public crate-easygpu-lyon-0.0.4 (c (n "easygpu-lyon") (v "0.0.4") (d (list (d (n "easygpu") (r "^0.0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "lyon_tessellation") (r "^0.16") (d #t) (k 0)) (d (n "winit") (r "^0.24") (d #t) (k 2)))) (h "106rcjc9xvrl8p6mnnx81z52i0y5mqsyfpshw5ah4cv4lgzll7qp")))

(define-public crate-easygpu-lyon-0.0.5 (c (n "easygpu-lyon") (v "0.0.5") (d (list (d (n "easygpu") (r "^0.0.5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "lyon_tessellation") (r "^0.16") (d #t) (k 0)) (d (n "winit") (r "^0.24") (d #t) (k 2)))) (h "130038qc3fn5n8qbhbdaza72g2pl7dkljhpr87bf88pkgy0fg3vi")))

(define-public crate-easygpu-lyon-0.0.6 (c (n "easygpu-lyon") (v "0.0.6") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "easygpu") (r "^0.0.6") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "lyon_tessellation") (r "^0.16") (d #t) (k 0)) (d (n "winit") (r "^0.24") (d #t) (k 2)))) (h "1svak28gq37a0lklihfqhdyi4rzhnq09vyg0kyf80lcm4qbdxr5p")))

(define-public crate-easygpu-lyon-0.0.7 (c (n "easygpu-lyon") (v "0.0.7") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "easygpu") (r "^0.0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "lyon_tessellation") (r "^0.16") (d #t) (k 0)) (d (n "winit") (r "^0.24") (d #t) (k 2)))) (h "1ywvazsh6gq439v4blx4nnjvj6h5i1j36i04n8aiby6rmbgc7i7n")))

(define-public crate-easygpu-lyon-0.0.8 (c (n "easygpu-lyon") (v "0.0.8") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "easygpu") (r "^0.0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "lyon_tessellation") (r "^0.16") (d #t) (k 0)) (d (n "winit") (r "^0.24") (d #t) (k 2)))) (h "0ij6mxv5kmn2hnyld7r55a0dhvz00kzagry50wjgx18h42ss6nj1")))

(define-public crate-easygpu-lyon-0.0.9 (c (n "easygpu-lyon") (v "0.0.9") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "easygpu") (r "^0.0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "lyon_tessellation") (r "^0.16") (d #t) (k 0)) (d (n "winit") (r "^0.24") (d #t) (k 2)))) (h "0f8ifqs3nrjk5mi60bc550scw9jsnllaflcy9cm8kf5zsqy9q0sj")))

(define-public crate-easygpu-lyon-0.0.10 (c (n "easygpu-lyon") (v "0.0.10") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "easygpu") (r "^0.0.10") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "lyon_tessellation") (r "^0.16") (d #t) (k 0)) (d (n "winit") (r "^0.24") (d #t) (k 2)))) (h "1nmy08xs4d9axf2kval4xhkcj17z91852vhk2p39vidxldgkl14w")))

(define-public crate-easygpu-lyon-0.0.11 (c (n "easygpu-lyon") (v "0.0.11") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "easygpu") (r "^0.0.11") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "lyon_tessellation") (r "^0.16") (d #t) (k 0)) (d (n "winit") (r "^0.24") (d #t) (k 2)))) (h "1zdaxw8hy06ip3p204i5h71f0ahp8k6zn0w71sd3518c1qqb6pfs")))

(define-public crate-easygpu-lyon-0.0.12 (c (n "easygpu-lyon") (v "0.0.12") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "easygpu") (r "^0.0.12") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "lyon_tessellation") (r "^0.16") (d #t) (k 0)) (d (n "winit") (r "^0.25") (d #t) (k 2)))) (h "02bzrwdhgwl7xv7hmpf16xywjykwai2ffjmjps0jnqwj1y01pjzc")))

(define-public crate-easygpu-lyon-0.0.13 (c (n "easygpu-lyon") (v "0.0.13") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "easygpu") (r "^0.0.13") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "lyon_tessellation") (r "^0.17") (d #t) (k 0)) (d (n "winit") (r "^0.25") (d #t) (k 2)))) (h "1bha5qf6jg5jdswkd18pqc4h5saz7a5jbrjidz3s505j1wdsn6lp")))

(define-public crate-easygpu-lyon-0.0.14 (c (n "easygpu-lyon") (v "0.0.14") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "easygpu") (r "^0.0.14") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "lyon_tessellation") (r "^0.17") (d #t) (k 0)) (d (n "winit") (r "^0.25") (d #t) (k 2)))) (h "0g2fr1hr4cc30b5h2aa5w7kifq9y5j6hvby6lg7g9an5ldmssn8i")))

(define-public crate-easygpu-lyon-0.0.15 (c (n "easygpu-lyon") (v "0.0.15") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "easygpu") (r "^0.0.15") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "lyon_tessellation") (r "^0.17") (d #t) (k 0)) (d (n "winit") (r "^0.26") (d #t) (k 2)))) (h "13dafz7ir374wlk0c70aqxdl0s4lqk10yd2g5n5lkiiacgls9424")))

(define-public crate-easygpu-lyon-0.1.0 (c (n "easygpu-lyon") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.7.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "easygpu") (r "^0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "lyon_tessellation") (r "^0.17.10") (d #t) (k 0)) (d (n "winit") (r "^0.26.1") (d #t) (k 2)))) (h "1fr98bmasd450in0wz49xqksbhdrvcimz2xfdww8c29j5v805rp7")))

(define-public crate-easygpu-lyon-0.2.0 (c (n "easygpu-lyon") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.7.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "easygpu") (r "^0.2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "lyon_tessellation") (r "^1.0.1") (d #t) (k 0)) (d (n "winit") (r "^0.26.1") (d #t) (k 2)))) (h "0fdg92dhl5fx6qcvi1i46r6sqcclnj2ry1d2ligsk5ysadl89v7g")))

(define-public crate-easygpu-lyon-0.3.0 (c (n "easygpu-lyon") (v "0.3.0") (d (list (d (n "bytemuck") (r "^1.7.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "easygpu") (r "^0.3.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "lyon_tessellation") (r "^1.0.1") (d #t) (k 0)) (d (n "winit") (r "^0.27.4") (d #t) (k 2)))) (h "1vrs87n3jzpjz4ry1hn218zzvfspyxsi0wnr6b9wg1hsmmjhr54m")))

(define-public crate-easygpu-lyon-0.4.0 (c (n "easygpu-lyon") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "bytemuck") (r "^1.7.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "easygpu") (r "^0.4.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "lyon_tessellation") (r "^1.0.1") (d #t) (k 0)) (d (n "winit") (r "^0.27.4") (d #t) (k 2)))) (h "1bh34g7dz3g182p4nsrj4fjfbxvks65zmqzxdm3czx3wzw39ly67")))

(define-public crate-easygpu-lyon-0.5.0 (c (n "easygpu-lyon") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "bytemuck") (r "^1.7.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "easygpu") (r "^0.5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.19") (d #t) (k 2)) (d (n "lyon_tessellation") (r "^1.0.1") (d #t) (k 0)) (d (n "winit") (r "^0.28.3") (d #t) (k 2)))) (h "0mdw34bivhi8pkgczhxad3kip405pfl49y47r6zgpkg3n67dxq12")))

