(define-module (crates-io ea sy easyid) #:use-module (crates-io))

(define-public crate-easyid-1.0.0 (c (n "easyid") (v "1.0.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.5") (d #t) (k 0)) (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0mx9gr2yhs7r4vv46sa2784l4xx1nfk34p0dxmflcs68l93038a1")))

