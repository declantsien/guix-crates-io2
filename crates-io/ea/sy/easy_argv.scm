(define-module (crates-io ea sy easy_argv) #:use-module (crates-io))

(define-public crate-easy_argv-0.1.0 (c (n "easy_argv") (v "0.1.0") (h "1a9swr43nxlx28hbd3j4vxmziz0lmls0qf1qnr54civmf2asj3pk")))

(define-public crate-easy_argv-0.1.2 (c (n "easy_argv") (v "0.1.2") (h "1j79ji1i7z09qwxkybnbr7q4pflx4flmf7l6c7h8ljs4jwxiy8j1")))

