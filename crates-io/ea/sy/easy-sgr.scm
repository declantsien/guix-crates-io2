(define-module (crates-io ea sy easy-sgr) #:use-module (crates-io))

(define-public crate-easy-sgr-0.0.1 (c (n "easy-sgr") (v "0.0.1") (h "0lwq45rhcif1dp66c7qiadsi3yhrh8kbr86yy3j1j5n5nzbbb6zd")))

(define-public crate-easy-sgr-0.0.2 (c (n "easy-sgr") (v "0.0.2") (h "0fij68v554q00hxsmadw73wgdq19nkmvwnhwchq8sxnh260ihx3g")))

(define-public crate-easy-sgr-0.0.3 (c (n "easy-sgr") (v "0.0.3") (h "16xw5vfkp08jwi3bzdn4x68k58008whzqip7zv23g0f943xx6awr") (f (quote (("partial"))))))

(define-public crate-easy-sgr-0.0.4 (c (n "easy-sgr") (v "0.0.4") (h "1z7b5ww8r522823w487000yqsrllb360b15yv29c24mgfqv4bn8p") (f (quote (("partial"))))))

(define-public crate-easy-sgr-0.0.5 (c (n "easy-sgr") (v "0.0.5") (h "167lkivpgp2nsiv03q4jjr0qn60ys4261fzx1ix2w20wl1hmcall") (f (quote (("partial"))))))

(define-public crate-easy-sgr-0.0.6 (c (n "easy-sgr") (v "0.0.6") (h "0a3a4rm58k80lkmn54ih5c8w3dx57d6a9w1w05n43g8fh7y4jap1") (f (quote (("partial"))))))

(define-public crate-easy-sgr-0.0.7 (c (n "easy-sgr") (v "0.0.7") (h "0vwfdncr4fgnlqa3848yh05h15q04msqbgc96n1m2yzv24r5hmx7") (f (quote (("partial") ("from-str"))))))

(define-public crate-easy-sgr-0.0.8 (c (n "easy-sgr") (v "0.0.8") (h "0zx4vyjnx2f7v14kx8vpwkynvcyyswgz3gkn0psmxb8zgjxanjfd") (f (quote (("partial") ("from-str"))))))

(define-public crate-easy-sgr-0.1.0 (c (n "easy-sgr") (v "0.1.0") (d (list (d (n "easy-sgr-macros") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "160d8jyniwg6j4lgb0k7dzsv5vmshw740jq5y3wiizc0ax05hz7b") (f (quote (("partial") ("macro-only" "macros") ("from-str")))) (s 2) (e (quote (("macros" "dep:easy-sgr-macros"))))))

(define-public crate-easy-sgr-0.1.1 (c (n "easy-sgr") (v "0.1.1") (d (list (d (n "easy-sgr-macros") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "00izd35pf830j722z6bv7q7d5s4991la828iqxr5dz1nxjphyhyb") (f (quote (("partial") ("macro-only" "macros") ("from-str")))) (s 2) (e (quote (("macros" "dep:easy-sgr-macros"))))))

