(define-module (crates-io ea sy easy_cargo_dep_a) #:use-module (crates-io))

(define-public crate-easy_cargo_dep_A-0.1.0 (c (n "easy_cargo_dep_A") (v "0.1.0") (h "1rs5y4v8f43rb07g20xp0mvm1jh6gxp0zz269d2wvmfrdg95x07i")))

(define-public crate-easy_cargo_dep_A-1.0.0 (c (n "easy_cargo_dep_A") (v "1.0.0") (d (list (d (n "easy_cargo_dep_B") (r "^1.0.0") (d #t) (k 0)))) (h "08dj4mzz719hn79fijmc2iimlg8fvgnqsgmx19y826jgl278j6i1")))

(define-public crate-easy_cargo_dep_A-1.0.1 (c (n "easy_cargo_dep_A") (v "1.0.1") (d (list (d (n "easy_cargo_dep_B") (r "^1.0.0") (d #t) (k 0)))) (h "0phss4wbhml2lmwpsj71siyqmrwyn3p4r90g7v87qblxba8ry0wa")))

(define-public crate-easy_cargo_dep_A-1.0.2 (c (n "easy_cargo_dep_A") (v "1.0.2") (d (list (d (n "easy_cargo_dep_B") (r "^2.1.1") (d #t) (k 0)))) (h "0jmxhfk6j6wpi8xgjwwacvi2xkxyzhjgrpiibzc3f5ziv2lm76g4")))

(define-public crate-easy_cargo_dep_A-3.0.0 (c (n "easy_cargo_dep_A") (v "3.0.0") (d (list (d (n "easy_cargo_dep_B") (r "=3.0.0") (d #t) (k 0)))) (h "1n5gxhydv61ynm2czsz7j8w2cx4iyj89h57kb99hd12iddy6xy5w")))

(define-public crate-easy_cargo_dep_A-3.0.1 (c (n "easy_cargo_dep_A") (v "3.0.1") (d (list (d (n "easy_cargo_dep_B") (r "^3.1.0") (d #t) (k 0)))) (h "0ihsd4180nph366dz364qblfcz9v7snxycyqvryh1kp2a80nv9gb")))

(define-public crate-easy_cargo_dep_A-3.0.2 (c (n "easy_cargo_dep_A") (v "3.0.2") (d (list (d (n "easy_cargo_dep_B") (r "=3.1.0") (d #t) (k 0)))) (h "1wfspvpfacliyc2fsgc7hxxx7alzkz26w490ggl8z3xlzbxkp3xr")))

(define-public crate-easy_cargo_dep_A-3.0.3 (c (n "easy_cargo_dep_A") (v "3.0.3") (d (list (d (n "easy_cargo_dep_B") (r "^3.1.1") (d #t) (k 0)))) (h "0xfl8zlf7ns5p99iwvhf021b1by8zhkdqw3bjg6mdhlk4c724czx")))

(define-public crate-easy_cargo_dep_A-3.0.4 (c (n "easy_cargo_dep_A") (v "3.0.4") (d (list (d (n "easy_cargo_dep_B") (r "^3.1.0") (d #t) (k 0)))) (h "1vjn7vdwd6qzq2sychmrn238xgwpfzad95kmm031a10pnwrf2c3a")))

(define-public crate-easy_cargo_dep_A-3.0.5 (c (n "easy_cargo_dep_A") (v "3.0.5") (d (list (d (n "easy_cargo_dep_B") (r "=3.1.0") (d #t) (k 0)))) (h "0ar5lbd51pahhl8nqwb9gz43886d31pgxsg5clbnbjy6q82f92kn")))

(define-public crate-easy_cargo_dep_A-3.0.6 (c (n "easy_cargo_dep_A") (v "3.0.6") (d (list (d (n "easy_cargo_dep_B") (r "=3.1.0") (d #t) (k 0)))) (h "0k5p6hiazmvxidrhw4yd3n4p089s7lj0s10aw9nq1cvssd5yp8ic") (y #t)))

(define-public crate-easy_cargo_dep_A-2.0.6 (c (n "easy_cargo_dep_A") (v "2.0.6") (d (list (d (n "easy_cargo_dep_B") (r "^3.1.0") (d #t) (k 0)))) (h "0zpdaiq4c56jlgh24wsm3xqvchp738gkgk81s4phr78kk46rvc5d")))

