(define-module (crates-io ea sy easy-auth) #:use-module (crates-io))

(define-public crate-easy-auth-0.3.0 (c (n "easy-auth") (v "0.3.0") (d (list (d (n "bcrypt") (r "^0.14.0") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)))) (h "0fr40r26j2hvy54y4kr6kvzgg2pd8b31wbbvx8drbhzg6jnp63f1")))

