(define-module (crates-io ea sy easy_json_serde) #:use-module (crates-io))

(define-public crate-easy_json_serde-0.1.0 (c (n "easy_json_serde") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "01k3w8yjif2x07grhmh0g54yc02rl7jid436dzm6ns69kcnnwdp1")))

(define-public crate-easy_json_serde-0.1.1 (c (n "easy_json_serde") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1hjwf79fdw6xpm89pww1ykbjfdisd2axs6qf2ci8wv8c74nv7jbj")))

(define-public crate-easy_json_serde-0.1.2 (c (n "easy_json_serde") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1a9cmamn0yxrwb8ack9l0yipgi0rqwrmd12caybbfzxmq6v4lryc")))

(define-public crate-easy_json_serde-0.2.0 (c (n "easy_json_serde") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0qrg78l8916xkgia7w822xdp92qb68nki8n15dvaaym1dmyqw5fz")))

(define-public crate-easy_json_serde-0.2.1 (c (n "easy_json_serde") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1afqa3qj1dhydvwmq6hkvyj5ydb0c4jywn1yald7l8bl44hmkrk0")))

(define-public crate-easy_json_serde-0.2.2 (c (n "easy_json_serde") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0vgx4dnw51fbj9p48k1mk23b7jzp3fv9v4krh1r5hlnrfgbhm13i")))

