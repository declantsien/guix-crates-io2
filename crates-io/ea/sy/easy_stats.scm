(define-module (crates-io ea sy easy_stats) #:use-module (crates-io))

(define-public crate-easy_stats-0.1.0 (c (n "easy_stats") (v "0.1.0") (h "10j5wrfxwavkfp75v19rm5sg6szlpyif8934pvx3r16rmr0f8pd1")))

(define-public crate-easy_stats-0.1.1 (c (n "easy_stats") (v "0.1.1") (h "1i0bh92dxfgdkra3g3idqq8plnxy0x5963k2a7jp0k8s95axqyc4")))

