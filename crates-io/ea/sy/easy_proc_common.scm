(define-module (crates-io ea sy easy_proc_common) #:use-module (crates-io))

(define-public crate-easy_proc_common-0.1.0 (c (n "easy_proc_common") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (d #t) (k 0)))) (h "1fbi5ak4cadb3z965y3yb5n2gq57hza9njhz4461p2953kgsika7")))

(define-public crate-easy_proc_common-0.1.1 (c (n "easy_proc_common") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (d #t) (k 0)))) (h "08x5bwmcq7lvp2mjvaaxr94n4na5qjcfd07jxrs4xnrc2ivfqyzv")))

(define-public crate-easy_proc_common-0.2.0 (c (n "easy_proc_common") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (d #t) (k 0)))) (h "12qnyavx58mpj38rfranczk9189v976h0fvwn86s3zyiyrmjqllk")))

(define-public crate-easy_proc_common-0.3.0 (c (n "easy_proc_common") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^2.0.22") (d #t) (k 0)))) (h "0ddgzp94i676j4la9ww6b3l8hxchvbkpviv8pf1lisw12qn3cyah")))

