(define-module (crates-io ea sy easy_trajectories) #:use-module (crates-io))

(define-public crate-easy_trajectories-0.1.1 (c (n "easy_trajectories") (v "0.1.1") (d (list (d (n "egui") (r "^0.17.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.2") (d #t) (k 0)))) (h "0ig4vfqbdszfwk0x2gz0l7zrg2aa7n1vkrgmpavklvshx41mw28d") (y #t)))

(define-public crate-easy_trajectories-0.1.2 (c (n "easy_trajectories") (v "0.1.2") (d (list (d (n "egui") (r "^0.17.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.2") (d #t) (k 0)))) (h "1inpw7fv99f1p9a9py2vrbaa10173948cdxapxvnvfafypm9wn5l")))

