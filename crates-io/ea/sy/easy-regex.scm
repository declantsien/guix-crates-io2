(define-module (crates-io ea sy easy-regex) #:use-module (crates-io))

(define-public crate-easy-regex-0.1.0 (c (n "easy-regex") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0f7l4c5bygj2hpy4020ricmzfx7s0rjam80bf0cm3889nk68674q") (y #t)))

(define-public crate-easy-regex-0.1.1 (c (n "easy-regex") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0n8xdqia728inhcp3p1ql22csfhgkxf6f197jn7rb7fygkain7s2") (y #t)))

(define-public crate-easy-regex-0.7.3 (c (n "easy-regex") (v "0.7.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0w8yhm7h17aaq0dnfqsspldyprf6xb0ica43ic7xyflz4ghvxvcp") (y #t)))

(define-public crate-easy-regex-0.7.4 (c (n "easy-regex") (v "0.7.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "13ic3aj6r803yy2ksibidd9rnbg52kf2mhprgd3pdv465y34cxhh") (y #t)))

(define-public crate-easy-regex-0.10.6 (c (n "easy-regex") (v "0.10.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1sm62gp92s8iv0pxhhdmgwnw1k237j1bz44hwy7kgmiz3l3m20bx") (y #t)))

(define-public crate-easy-regex-0.11.7 (c (n "easy-regex") (v "0.11.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0gdbpzcb44c4gjxvp01hhij613mhbz47m73fgzc1a6ii0yr066y2")))

