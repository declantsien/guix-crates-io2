(define-module (crates-io ea sy easyblink) #:use-module (crates-io))

(define-public crate-easyblink-0.1.0 (c (n "easyblink") (v "0.1.0") (d (list (d (n "blinkt") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "16i5j3cbz0q39wczq6lgs5qr3xbkkri3rb3ys0plpk8ks1q3jai6")))

