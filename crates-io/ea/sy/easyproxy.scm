(define-module (crates-io ea sy easyproxy) #:use-module (crates-io))

(define-public crate-easyproxy-0.1.0 (c (n "easyproxy") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "172l0zgfnkx3hl29slvcgsdyq8glnknicxzjxr1cssnpyl078in1")))

(define-public crate-easyproxy-0.1.1 (c (n "easyproxy") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03i5j1wgjx6c1xyc5yjz1mh7j7j4bn7dhkban22sd72nyb3m9l21")))

(define-public crate-easyproxy-0.1.2 (c (n "easyproxy") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1j2a5axpfmmx64sdmjyg5ibvlasg9rl4m3j418f77cb8q6dhpcxq")))

