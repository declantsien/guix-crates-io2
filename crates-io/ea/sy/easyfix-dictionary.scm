(define-module (crates-io ea sy easyfix-dictionary) #:use-module (crates-io))

(define-public crate-easyfix-dictionary-0.1.0 (c (n "easyfix-dictionary") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 0)))) (h "13amnchvpxx9f653cgrnld9jv51y1wzw2lvw1pa6qp184c67qjxp")))

(define-public crate-easyfix-dictionary-0.1.1 (c (n "easyfix-dictionary") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 0)))) (h "1jh4vl0hqd41r8by4cw93mski11p9y4lb792aqvdzn6m4pmfdqyy")))

(define-public crate-easyfix-dictionary-0.1.2 (c (n "easyfix-dictionary") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 0)))) (h "1s51a2nkldd2ji6r5pwixzckrqlqbcvc9xxlrjkzg415fas7vigv")))

(define-public crate-easyfix-dictionary-0.2.0 (c (n "easyfix-dictionary") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 0)))) (h "1q4wparffq386v3yy5zpl0mg9x7v960xiln57c61f0wnwrmmxgl3")))

(define-public crate-easyfix-dictionary-0.3.0 (c (n "easyfix-dictionary") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 0)))) (h "1x3i2w8byxzhwxp4wg8vgyydimfs4mh45yaky176rdbf9yqni47x")))

(define-public crate-easyfix-dictionary-0.3.1 (c (n "easyfix-dictionary") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.10") (d #t) (k 0)))) (h "11zrqn0sli7n061wz5m9w259qbiv143y0h8r8rysxxm2m87vj66j")))

