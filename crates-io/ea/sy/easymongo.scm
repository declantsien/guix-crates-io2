(define-module (crates-io ea sy easymongo) #:use-module (crates-io))

(define-public crate-easymongo-0.1.0 (c (n "easymongo") (v "0.1.0") (d (list (d (n "bson") (r "^2.5.0") (d #t) (k 0)) (d (n "mongodb") (r "^2.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "10w5hahyqrqn97l7q8zcd1glz6q65zqk79jpfpj65dlv85zqgnib")))

(define-public crate-easymongo-0.1.1 (c (n "easymongo") (v "0.1.1") (d (list (d (n "bson") (r "^2.6.1") (d #t) (k 0)) (d (n "mongodb") (r "^2.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)))) (h "13hjaf7csn6b6wanw3ry1brva3pqq9n17kfhpfl4jq5293p98v05")))

