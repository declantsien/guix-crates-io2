(define-module (crates-io ea sy easybench) #:use-module (crates-io))

(define-public crate-easybench-0.1.0 (c (n "easybench") (v "0.1.0") (h "1c8bbhzssf70a750hqadzg3n4z2i5by3zi4cs41fmm3c2dd17w0j")))

(define-public crate-easybench-0.1.1 (c (n "easybench") (v "0.1.1") (h "1fh59iqav05ydkzwphx6b7z548hy8cxjnn3bqmzbwv5zn4f5f255")))

(define-public crate-easybench-0.1.2 (c (n "easybench") (v "0.1.2") (h "0vqjzslb0a5p3fs543j8cqvmfy475hldjs4m38r31wwhmc9p11sb")))

(define-public crate-easybench-0.1.3 (c (n "easybench") (v "0.1.3") (h "1251lq8iqqp2yi2ynpas3h2rg22yfp03343pzx3wl73nswkva5ml")))

(define-public crate-easybench-0.1.4 (c (n "easybench") (v "0.1.4") (d (list (d (n "humantime") (r "^1.1.1") (d #t) (k 0)))) (h "0wlfsjwk7mfkch7vb3spcgc6vzf01l5n6glwl051dklvfs1v8rgj")))

(define-public crate-easybench-1.0.0 (c (n "easybench") (v "1.0.0") (d (list (d (n "humantime") (r "^1.3") (d #t) (k 0)))) (h "0dgcy4hfnmpbxkzyp6q81g259b9zr5k5drxssp2339izl2d0bkwh")))

(define-public crate-easybench-1.1.0 (c (n "easybench") (v "1.1.0") (h "111qimzxx3sf3h7kypihacsd9vbb2jdkqcfmb42dpi6xjp7ialim")))

(define-public crate-easybench-1.1.1 (c (n "easybench") (v "1.1.1") (h "0cjsg93a3gczp72r73pk9y0p2z45dgzv84339cc3gav3r33y1r8g")))

