(define-module (crates-io ea sy easyinvoice) #:use-module (crates-io))

(define-public crate-easyinvoice-0.1.0 (c (n "easyinvoice") (v "0.1.0") (h "1ingjw5v44792ba4fjsdbib1wmdkjm16ngqcaxg5imk465a6bpgg")))

(define-public crate-easyinvoice-1.0.0 (c (n "easyinvoice") (v "1.0.0") (h "0s393gndbn231iibaba5z67k9fcf2h781g848phajjxknr7xn106")))

