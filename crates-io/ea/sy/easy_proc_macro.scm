(define-module (crates-io ea sy easy_proc_macro) #:use-module (crates-io))

(define-public crate-easy_proc_macro-0.1.0 (c (n "easy_proc_macro") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1bl455i2q1vy9gs8lsmdjsbx3x992qn3y7c2gg406jan5syixqw4") (f (quote (("trace") ("nightly") ("local" "nightly") ("default"))))))

