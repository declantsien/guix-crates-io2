(define-module (crates-io ea sy easy-parallel) #:use-module (crates-io))

(define-public crate-easy-parallel-1.0.0 (c (n "easy-parallel") (v "1.0.0") (h "02iv454qhc67k1jwl3bhyrh6ssz0f5nyw0bw1vwn069lqpb5l71d")))

(define-public crate-easy-parallel-1.0.1 (c (n "easy-parallel") (v "1.0.1") (h "039gnykm8bc5pf05zchp026zr4ik1xdzd5q3slzaay08cy85sph1")))

(define-public crate-easy-parallel-2.0.0 (c (n "easy-parallel") (v "2.0.0") (h "1ggvmf2gpj8y3lp8z42m2m02dlcribf6npyf22kqb9vlhc5dr0bi")))

(define-public crate-easy-parallel-2.1.0 (c (n "easy-parallel") (v "2.1.0") (h "02lr7z53ihf7fr9sprkgc1fh59ds1927p39pmi1fdg7xy0ms7n1l")))

(define-public crate-easy-parallel-2.2.0 (c (n "easy-parallel") (v "2.2.0") (h "0nhpdyq32k50mkigdfbl7xkvrbqw17wnqram3xzl0pggs789r13p")))

(define-public crate-easy-parallel-3.0.0 (c (n "easy-parallel") (v "3.0.0") (h "0n7z32xxlyr4vi8pz6rb52knnp5padgb0g73nqgkfq1whdrmc61y")))

(define-public crate-easy-parallel-3.1.0 (c (n "easy-parallel") (v "3.1.0") (h "1x28z540fc4g8fqm1sbpqbpdfbi40mkas4xr57s3yn0jjbbszm0x")))

(define-public crate-easy-parallel-3.2.0 (c (n "easy-parallel") (v "3.2.0") (h "0imr216x3xvi26qcq3qs6k6b23l43sn9lgai7x7izk6djd9y41v9")))

(define-public crate-easy-parallel-3.3.0 (c (n "easy-parallel") (v "3.3.0") (h "00g0fvbjkdbqm6z5xcx3b1wzi8rlrhqb840ybavgijhg74iljq64") (r "1.35")))

(define-public crate-easy-parallel-3.3.1 (c (n "easy-parallel") (v "3.3.1") (h "0g12xq122hy170pprldvgmpml0zz1mn9n4hq5c6ly3pnmsqbkyra") (r "1.63")))

