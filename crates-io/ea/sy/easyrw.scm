(define-module (crates-io ea sy easyrw) #:use-module (crates-io))

(define-public crate-easyrw-0.1.0 (c (n "easyrw") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "minwindef" "ntdef" "winuser" "tlhelp32" "psapi" "securitybaseapi" "libloaderapi" "synchapi" "wow64apiset" "processthreadsapi" "handleapi" "winbase" "impl-default" "wincon"))) (d #t) (k 0)))) (h "1h2v97482r4piin00x3k2ks4mq98pghlg8kr2m6jvg3i2m77sj31")))

(define-public crate-easyrw-0.1.1 (c (n "easyrw") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "minwindef" "ntdef" "winuser" "tlhelp32" "psapi" "securitybaseapi" "libloaderapi" "synchapi" "wow64apiset" "processthreadsapi" "handleapi" "winbase" "impl-default" "wincon"))) (d #t) (k 0)))) (h "1c9nzczdaqwzlc67y4is8lyslxzpibj0fynkd70qcsfyjnp5a3sy")))

(define-public crate-easyrw-0.1.2 (c (n "easyrw") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "minwindef" "ntdef" "winuser" "tlhelp32" "psapi" "securitybaseapi" "libloaderapi" "synchapi" "wow64apiset" "processthreadsapi" "handleapi" "winbase" "impl-default" "wincon"))) (d #t) (k 0)))) (h "1m6c128khxfnh2pxap86w5b68g680q4d1h612a1zvm2x5qbs8ksc")))

(define-public crate-easyrw-0.1.3 (c (n "easyrw") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "minwindef" "ntdef" "winuser" "tlhelp32" "psapi" "securitybaseapi" "libloaderapi" "synchapi" "wow64apiset" "processthreadsapi" "handleapi" "winbase" "impl-default" "wincon"))) (d #t) (k 0)))) (h "0i2319g1ghd83ssq3n93r8ypnpprwkkz300sg13498ngp7nhmssq")))

(define-public crate-easyrw-0.1.4 (c (n "easyrw") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "minwindef" "ntdef" "winuser" "tlhelp32" "psapi" "securitybaseapi" "libloaderapi" "synchapi" "wow64apiset" "processthreadsapi" "handleapi" "winbase" "impl-default" "wincon"))) (d #t) (k 0)))) (h "10hdswwq408g79kqbapczkfm8mgh7pcs7zpbjzxx9yzw46a23h9g")))

(define-public crate-easyrw-0.1.5 (c (n "easyrw") (v "0.1.5") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "minwindef" "ntdef" "winuser" "tlhelp32" "psapi" "securitybaseapi" "libloaderapi" "synchapi" "wow64apiset" "processthreadsapi" "handleapi" "winbase" "impl-default" "wincon"))) (d #t) (k 0)))) (h "0lvcysr7hn8q7lmkily4ihzn8szfakff049aynjw59zw1dxrwd7r")))

(define-public crate-easyrw-0.1.6 (c (n "easyrw") (v "0.1.6") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "minwindef" "ntdef" "winuser" "tlhelp32" "psapi" "securitybaseapi" "libloaderapi" "synchapi" "wow64apiset" "processthreadsapi" "handleapi" "winbase" "impl-default" "wincon"))) (d #t) (k 0)))) (h "1yx2kajhkyyviky0ny3nkj7jf4k5ij4zz2nfz69xsgi10mc6l3zz")))

(define-public crate-easyrw-0.1.7 (c (n "easyrw") (v "0.1.7") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "minwindef" "ntdef" "winuser" "tlhelp32" "psapi" "securitybaseapi" "libloaderapi" "synchapi" "wow64apiset" "processthreadsapi" "handleapi" "winbase" "impl-default" "wincon"))) (d #t) (k 0)))) (h "1bsn9ggcznni96df4af5a79iy21srsl4qh2dhcc8id4afvjv0yml")))

(define-public crate-easyrw-0.1.8 (c (n "easyrw") (v "0.1.8") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "minwindef" "ntdef" "winuser" "tlhelp32" "psapi" "securitybaseapi" "libloaderapi" "synchapi" "wow64apiset" "processthreadsapi" "handleapi" "winbase" "impl-default" "wincon"))) (d #t) (k 0)))) (h "1g0swgkms28bgicsvbs11arnx3hgzcad387ryd0dyqsswb7c7223")))

(define-public crate-easyrw-0.1.9 (c (n "easyrw") (v "0.1.9") (d (list (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "minwindef" "ntdef" "winuser" "tlhelp32" "psapi" "securitybaseapi" "libloaderapi" "synchapi" "wow64apiset" "processthreadsapi" "handleapi" "winbase" "impl-default" "wincon"))) (d #t) (k 0)))) (h "0fapvb0w0cqdbblar0vjpz3i233ihgj431pbh651s5021jwkjgqm")))

(define-public crate-easyrw-0.2.0 (c (n "easyrw") (v "0.2.0") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("processthreadsapi" "tlhelp32" "memoryapi" "handleapi" "impl-default"))) (d #t) (k 0)))) (h "1z6yvf141z68pns7mdilnw54pvyh9smrscy11x2k8y3mklzwxx93")))

(define-public crate-easyrw-0.2.1 (c (n "easyrw") (v "0.2.1") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("processthreadsapi" "tlhelp32" "memoryapi" "handleapi" "impl-default"))) (d #t) (k 0)))) (h "189mag45zpxwdcv1hlm1m5h7gpskqjyglcz4xgbc075xswk0kw6v")))

(define-public crate-easyrw-0.2.2 (c (n "easyrw") (v "0.2.2") (d (list (d (n "winapi") (r "^0.3.9") (f (quote ("processthreadsapi" "tlhelp32" "memoryapi" "handleapi" "impl-default"))) (d #t) (k 0)))) (h "1xxrmd39b64q0cr1yvhbmiywrlfvsqki8nwhs5668j1s093jfx7k")))

