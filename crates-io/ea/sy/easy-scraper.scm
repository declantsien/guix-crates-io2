(define-module (crates-io ea sy easy-scraper) #:use-module (crates-io))

(define-public crate-easy-scraper-0.1.0 (c (n "easy-scraper") (v "0.1.0") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "kuchiki") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "00lq6dgjdzkf6xx27dg4vyrgvz6f3mgxnpy90l1001k6bvangr7x")))

(define-public crate-easy-scraper-0.1.1 (c (n "easy-scraper") (v "0.1.1") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "kuchiki") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0gjjby36yz4qcyzp3jlydax1j83kz7f4iypxzgsa7g5xc0fadqg1")))

(define-public crate-easy-scraper-0.2.0 (c (n "easy-scraper") (v "0.2.0") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "kuchiki") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1kkzc3gvas96ki3pfmxa21lsi3wbdg8vbxil8a3h9bmm06y5ga0q")))

