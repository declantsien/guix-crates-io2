(define-module (crates-io ea sy easyfs-packer) #:use-module (crates-io))

(define-public crate-easyfs-packer-0.1.0 (c (n "easyfs-packer") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "easyfs") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "1rjrq58j1x5bxx3z4x4jrrv9yk2npkhn4ih524kx412j2nadpzw5")))

