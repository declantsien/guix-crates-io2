(define-module (crates-io ea sy easy-i3-neovim-nav) #:use-module (crates-io))

(define-public crate-easy-i3-neovim-nav-1.0.0 (c (n "easy-i3-neovim-nav") (v "1.0.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "i3ipc") (r "^0.10.1") (d #t) (k 0)) (d (n "neovim-lib") (r "^0.6.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1vvkk0vx0xdrripc51c4jyy04jiz3n0qn8z0qjvgvhwnwxa70h7w")))

