(define-module (crates-io ea sy easy-bluez) #:use-module (crates-io))

(define-public crate-easy-bluez-0.1.0 (c (n "easy-bluez") (v "0.1.0") (d (list (d (n "basic_scheduler") (r "^0.1") (d #t) (k 0)) (d (n "blurz") (r "^0.2.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "eui48") (r "^0.3") (f (quote ("serde"))) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mvdb") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "uuid") (r "^0.5") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1hmw2m2pmcqpgi06vnnzwxskazlw97f1mf30p636jq3m7qf520pr")))

