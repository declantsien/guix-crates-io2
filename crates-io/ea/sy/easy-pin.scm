(define-module (crates-io ea sy easy-pin) #:use-module (crates-io))

(define-public crate-easy-pin-0.0.1-alpha (c (n "easy-pin") (v "0.0.1-alpha") (d (list (d (n "easy-pin-proc-macro") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 2)))) (h "0y30xkvd1apdkmki9xrmliabk4fbkd1p4y8zl4js8gs101qn4nr1")))

