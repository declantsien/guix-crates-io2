(define-module (crates-io ea sy easy-cast) #:use-module (crates-io))

(define-public crate-easy-cast-0.1.0 (c (n "easy-cast") (v "0.1.0") (h "09wwj0wkyyf3zgv2ggf0p5nwh58dvk7wg62jjwyw0zgfmwzc0xpd")))

(define-public crate-easy-cast-0.2.0 (c (n "easy-cast") (v "0.2.0") (h "1r0b8f08cd0dg13nhhdbjpjfdl0gkg8hp1zqhlf60dpvp6ln126z") (f (quote (("assert_range") ("assert_non_neg") ("assert_float") ("always_assert" "assert_float" "assert_non_neg" "assert_range"))))))

(define-public crate-easy-cast-0.3.0 (c (n "easy-cast") (v "0.3.0") (d (list (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0v1hc0ixmc3lvnc6j17w0xv47v4w1z551zw9974snybvgrz74cpw") (f (quote (("std") ("default" "std") ("assert_range") ("assert_non_neg") ("assert_float") ("always_assert" "assert_float" "assert_non_neg" "assert_range"))))))

(define-public crate-easy-cast-0.4.0 (c (n "easy-cast") (v "0.4.0") (d (list (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1dfrlddlpzc89gfyw71j1qyfaapqqzbsjnk2sc5kdcv6lyyfjjdh") (f (quote (("std") ("default" "std") ("assert_int") ("assert_float") ("always_assert" "assert_float" "assert_int"))))))

(define-public crate-easy-cast-0.4.1 (c (n "easy-cast") (v "0.4.1") (d (list (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1vwravzm77364bm5nqa6xwx645mydsvv86h8skwyjc35p9hw1vn4") (f (quote (("std") ("default" "std") ("assert_int") ("assert_float") ("always_assert" "assert_float" "assert_int"))))))

(define-public crate-easy-cast-0.4.2 (c (n "easy-cast") (v "0.4.2") (d (list (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0n65s68037hgsr22c40rk3gwxbbissbjk6spn7x05cf4avin25f2") (f (quote (("std") ("default" "std") ("assert_int") ("assert_float") ("always_assert" "assert_float" "assert_int"))))))

(define-public crate-easy-cast-0.4.3 (c (n "easy-cast") (v "0.4.3") (d (list (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1yvr90ncaq9xnmyk5rxyhw8vf8xqjh7vnc5ybmgzbgsji4yzkds4") (f (quote (("std") ("default" "std") ("assert_int") ("assert_float") ("always_assert" "assert_float" "assert_int"))))))

(define-public crate-easy-cast-0.4.4 (c (n "easy-cast") (v "0.4.4") (d (list (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "13ilmaplp2s4vw1vjh4pwbzk7jdxrn0kpf0rk5sli0s1ikp05lab") (f (quote (("std") ("default" "std") ("assert_int") ("assert_float") ("always_assert" "assert_float" "assert_int"))))))

(define-public crate-easy-cast-0.5.0 (c (n "easy-cast") (v "0.5.0") (d (list (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1k3sza5crzd4xzwafr0fa3wbywvwparms02d7a57bpgaw751nhfz") (f (quote (("std") ("default" "std") ("assert_int") ("assert_float") ("always_assert" "assert_float" "assert_int"))))))

(define-public crate-easy-cast-0.5.1 (c (n "easy-cast") (v "0.5.1") (d (list (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "13bcs7da0yf6lr6fp6sjiv0y50x5h7v32d5q87dxfgqhni5kx5fj") (f (quote (("std") ("default" "std") ("assert_int") ("assert_float") ("assert_digits") ("always_assert" "assert_float" "assert_int" "assert_digits"))))))

(define-public crate-easy-cast-0.5.2 (c (n "easy-cast") (v "0.5.2") (d (list (d (n "libm") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1mizff08p0rlmgvi81rqdblji372rhr17xlvzmqylfsz2iw6g4qh") (f (quote (("std") ("default" "std") ("assert_int") ("assert_float") ("assert_digits") ("always_assert" "assert_float" "assert_int" "assert_digits"))))))

