(define-module (crates-io ea sy easy-threadpool) #:use-module (crates-io))

(define-public crate-easy-threadpool-0.1.0 (c (n "easy-threadpool") (v "0.1.0") (h "08aa0z3z7ab3x0j221qh97y5lxk1qkn5f85a103933bqg7wdnh7g") (r "1.72.0")))

(define-public crate-easy-threadpool-0.2.0 (c (n "easy-threadpool") (v "0.2.0") (h "16z6mix9fxydfzfh3nssx8ng3ggk32jvg1mw7ilf11v1szfk8h58") (y #t) (r "1.72.0")))

(define-public crate-easy-threadpool-0.3.0 (c (n "easy-threadpool") (v "0.3.0") (h "10m57r08hkyvps9x9hcwvvrxzvc0fnqji8kml69x7sv8rgfi2z56") (r "1.72.0")))

(define-public crate-easy-threadpool-0.3.1 (c (n "easy-threadpool") (v "0.3.1") (h "0wgd3zhz9frg6kfjlml4yd08ik3diwghdq5hryjl7hw7s7a1v1vg") (r "1.72.0")))

(define-public crate-easy-threadpool-0.3.2 (c (n "easy-threadpool") (v "0.3.2") (h "1hqjvnh98j6vrznkym3wg4a49q189kr0is44fdmzhlr8zxnpaay2") (r "1.72.0")))

