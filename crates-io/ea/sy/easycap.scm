(define-module (crates-io ea sy easycap) #:use-module (crates-io))

(define-public crate-easycap-0.0.0 (c (n "easycap") (v "0.0.0") (d (list (d (n "rusb") (r "^0.9.1") (d #t) (k 0)) (d (n "rusb-async") (r "^0.0.1-alpha") (d #t) (k 0)))) (h "0g94766a76679mf0qc6z9pqfn9pc3083rr75byd305fynbv3raa2")))

