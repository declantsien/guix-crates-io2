(define-module (crates-io ea sy easy-cli) #:use-module (crates-io))

(define-public crate-easy-cli-1.0.0 (c (n "easy-cli") (v "1.0.0") (d (list (d (n "clap") (r "^4") (f (quote ("string"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "06w678c3c37qbqy723n2mqx3grm6h72b4irckbnibqn3fcdblkk0")))

(define-public crate-easy-cli-1.0.1 (c (n "easy-cli") (v "1.0.1") (d (list (d (n "clap") (r "^4") (f (quote ("string"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4") (d #t) (k 0)) (d (n "indoc") (r "^1.0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "stringreader") (r "^0.1.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "17j44a2yyrsm5hn70yk3yksvvv6krlm0lard8w1gpj24qcr028y3")))

