(define-module (crates-io ea sy easyjack) #:use-module (crates-io))

(define-public crate-easyjack-0.1.0 (c (n "easyjack") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "chan-signal") (r "^0.1.6") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dwxqp47r1i0c1lp80czq8pmbakri0l3xa4a618mb5y828gh2sbk")))

(define-public crate-easyjack-0.0.1 (c (n "easyjack") (v "0.0.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "chan-signal") (r "^0.1.6") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0k56193fjinhxzcr8njhqk5k7jbg08kp44zlx2xwcrcj3avjkpl2")))

(define-public crate-easyjack-0.1.1 (c (n "easyjack") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "getopts") (r "^0.2.4") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.7.0") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1pm5xzkk4lyqz9pnphxl9dj9av73gir9l0swv1s5x99f9wqvggfp")))

(define-public crate-easyjack-0.1.2 (c (n "easyjack") (v "0.1.2") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "getopts") (r "^0.2.4") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.7.0") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1wlgaq9agdg8r55q2vzhaaz1mpszhwzaqjy0wkw09kvkzfrq4a3r")))

