(define-module (crates-io ea sy easy-plugin) #:use-module (crates-io))

(define-public crate-easy-plugin-0.1.0 (c (n "easy-plugin") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.23") (o #t) (d #t) (k 0)))) (h "1c0rzi2hsc0baanqjfbph286k002g5krpsl3c9zhchm1m4l4kghc")))

(define-public crate-easy-plugin-0.1.1 (c (n "easy-plugin") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.23") (o #t) (d #t) (k 0)))) (h "1i7mn3a7y4pcqn6brv66bxzf2yl0d3i9bgcx1jgs16n9paz9vwda")))

(define-public crate-easy-plugin-0.1.2 (c (n "easy-plugin") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0.27") (o #t) (d #t) (k 0)))) (h "05fnp3pbwshgrzz3rq86qr5d6b3i7rkkw7cmz1l6sl7ilksqz590")))

(define-public crate-easy-plugin-0.2.0 (c (n "easy-plugin") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0.27") (o #t) (d #t) (k 0)))) (h "01vcgijsn0a3f5pgmyyz085rynrqyzd8yzrw6pv4wgdr3vvclijq")))

(define-public crate-easy-plugin-0.2.1 (c (n "easy-plugin") (v "0.2.1") (d (list (d (n "clippy") (r "^0.0.29") (o #t) (d #t) (k 0)))) (h "03lwd5pmdsdaqqykzsm8pdnknafxpscnw79a9p93a1vn390hbrzn")))

(define-public crate-easy-plugin-0.2.2 (c (n "easy-plugin") (v "0.2.2") (d (list (d (n "clippy") (r "^0.0.30") (o #t) (d #t) (k 0)))) (h "1v4na8yi3vgv9w9kr87ixyq7xllcvww4c0hsfs3z4d61xyngkg1d")))

(define-public crate-easy-plugin-0.2.3 (c (n "easy-plugin") (v "0.2.3") (d (list (d (n "clippy") (r "^0.0.34") (o #t) (d #t) (k 0)))) (h "0xpv4y8i01g5aka433z1b92mrnild5fzc4n11wr3b0xh4h9b8p53")))

(define-public crate-easy-plugin-0.2.4 (c (n "easy-plugin") (v "0.2.4") (d (list (d (n "clippy") (r "^0.0.35") (o #t) (d #t) (k 0)))) (h "0vfmxgcy8dxhjkbfz8ww3pnn4q7ac1c9y7hvms5d0g80jm20ldsh")))

(define-public crate-easy-plugin-0.2.5 (c (n "easy-plugin") (v "0.2.5") (d (list (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)))) (h "16ibfrx8k7g94vlaij8awhqa8pzcvin6amvzr5l4ddxn97vc6vxc")))

(define-public crate-easy-plugin-0.2.6 (c (n "easy-plugin") (v "0.2.6") (d (list (d (n "clippy") (r "^0.0.41") (o #t) (d #t) (k 0)))) (h "086h87039kv1jfg9pxnc9xfcqdykz5bi2p65wh39n3jvb9iqd07q")))

(define-public crate-easy-plugin-0.2.7 (c (n "easy-plugin") (v "0.2.7") (d (list (d (n "clippy") (r "^0.0.46") (o #t) (d #t) (k 0)))) (h "1jja6gaqpdhhj9pgk10a4givs4vinqxh7y1gm88z96gadgn717by")))

(define-public crate-easy-plugin-0.2.8 (c (n "easy-plugin") (v "0.2.8") (d (list (d (n "clippy") (r "^0.0.50") (o #t) (d #t) (k 0)))) (h "0dfkbhx7w6x546a5c6jrk1ys3hrxdjs4bhxbx5x0dqr21hi5xpzy")))

(define-public crate-easy-plugin-0.2.9 (c (n "easy-plugin") (v "0.2.9") (d (list (d (n "clippy") (r "^0.0.53") (o #t) (d #t) (k 0)))) (h "0rlbhc76n6131qg82zpw8s1723c9q4kjq4ng46n95a5b9gkzryj6")))

(define-public crate-easy-plugin-0.2.10 (c (n "easy-plugin") (v "0.2.10") (d (list (d (n "clippy") (r "^0.0.60") (o #t) (d #t) (k 0)))) (h "1h99z3d2fjv71d768880nw0jwczzvlzb4hyymyfwkhg10xm5iv4q")))

(define-public crate-easy-plugin-0.3.0 (c (n "easy-plugin") (v "0.3.0") (d (list (d (n "clippy") (r "^0.0.60") (o #t) (d #t) (k 0)))) (h "05m1zzqfpkg4zxig94yykgsfwwivgdgm6wz4ssj1wbhvw6gmhw3r")))

(define-public crate-easy-plugin-0.3.1 (c (n "easy-plugin") (v "0.3.1") (d (list (d (n "clippy") (r "^0.0.61") (o #t) (d #t) (k 0)))) (h "17h6zlwdap3160wgs6l4vm58h53jiiy42pqcazhnhbb1s82g731h")))

(define-public crate-easy-plugin-0.3.2 (c (n "easy-plugin") (v "0.3.2") (d (list (d (n "clippy") (r "^0.0.62") (o #t) (d #t) (k 0)))) (h "00dxmfxigliz9dwk6y3yvibdg0nvcginc27c7wbsb22y2h0hdna2")))

(define-public crate-easy-plugin-0.3.3 (c (n "easy-plugin") (v "0.3.3") (d (list (d (n "clippy") (r "^0.0.64") (o #t) (d #t) (k 0)))) (h "1balwj151asg1irysibzwjbxr0i09kzs655p46rn776ja69hgq2l")))

(define-public crate-easy-plugin-0.3.4 (c (n "easy-plugin") (v "0.3.4") (d (list (d (n "clippy") (r "^0.0.64") (o #t) (d #t) (k 0)))) (h "0yr0lg3xm6mzzali51yl37wdlsbd4xnqcd52s9shjfa9ham8rbb2")))

(define-public crate-easy-plugin-0.4.0 (c (n "easy-plugin") (v "0.4.0") (d (list (d (n "clippy") (r "^0.0.67") (o #t) (d #t) (k 0)) (d (n "easy-plugin-plugins") (r "^0.4.0") (d #t) (k 0)))) (h "1a914ys6qlb6fqd19m57lp1pj0ycjamh46pmcb6gi2y5fyhpf8j7")))

(define-public crate-easy-plugin-0.5.0 (c (n "easy-plugin") (v "0.5.0") (d (list (d (n "clippy") (r "^0.0.67") (o #t) (d #t) (k 0)) (d (n "easy-plugin-plugins") (r "^0.5.0") (d #t) (k 0)))) (h "1k2w0c90cimkiqn6crcik5j5wn324djqc0ihf9dg270hiz1rfh3d")))

(define-public crate-easy-plugin-0.6.0 (c (n "easy-plugin") (v "0.6.0") (d (list (d (n "clippy") (r "^0.0.70") (o #t) (d #t) (k 0)) (d (n "easy-plugin-plugins") (r "^0.6.0") (d #t) (k 0)))) (h "10iffnkvjyzn8sq3b7vx8rskld8pydzblq5wy6pd6q9xlz8g3caz")))

(define-public crate-easy-plugin-0.6.1 (c (n "easy-plugin") (v "0.6.1") (d (list (d (n "clippy") (r "^0.0.70") (o #t) (d #t) (k 0)) (d (n "easy-plugin-plugins") (r "^0.6.0") (d #t) (k 0)))) (h "1wbqcn0vg4lkk5ddyr1qfl82bsrllkas7pybdkz3lmr2f67w4gy8")))

(define-public crate-easy-plugin-0.6.2 (c (n "easy-plugin") (v "0.6.2") (d (list (d (n "clippy") (r "^0.0.70") (o #t) (d #t) (k 0)) (d (n "easy-plugin-plugins") (r "^0.6.0") (d #t) (k 0)))) (h "1r39zgnmkmx3l5p6jyadr2m3ng7mwfpmyzi7s7ifxg9kjp1zqdjz")))

(define-public crate-easy-plugin-0.7.0 (c (n "easy-plugin") (v "0.7.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "easy-plugin-plugins") (r "^0.7.0") (d #t) (k 0)))) (h "0yyp7fhh5w0clxalqh2x5vvga9yn5f2005x45d1biq2qjnrssjxq")))

(define-public crate-easy-plugin-0.7.1 (c (n "easy-plugin") (v "0.7.1") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "easy-plugin-plugins") (r "^0.7.1") (d #t) (k 0)))) (h "0ilsycl23wi5xnr2xkb8g6phglwxsqqhqcb8nyfp8sr3snlxbzm2")))

(define-public crate-easy-plugin-0.7.2 (c (n "easy-plugin") (v "0.7.2") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "easy-plugin-plugins") (r "^0.7.2") (d #t) (k 0)))) (h "1db4v8nrw4ik8nz51vl9gbnyhjvscizgnkg55lx410wr2sim4mrq")))

(define-public crate-easy-plugin-0.7.3 (c (n "easy-plugin") (v "0.7.3") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "easy-plugin-plugins") (r "^0.7.3") (d #t) (k 0)))) (h "1hlhpy966xfx25nd2s2g5sn601h8icxnw7hzg2s0n3isipmp3l52")))

(define-public crate-easy-plugin-0.8.0 (c (n "easy-plugin") (v "0.8.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "easy-plugin-plugins") (r "^0.8.0") (d #t) (k 0)) (d (n "easy-plugin-plugins") (r "^0.8.0") (d #t) (k 1)) (d (n "syntex") (r "^0.39.0") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.39.0") (o #t) (d #t) (k 1)) (d (n "syntex_errors") (r "^0.39.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.39.0") (o #t) (d #t) (k 0)) (d (n "synthax") (r "^0.2.0") (d #t) (k 0)) (d (n "synthax") (r "^0.2.0") (d #t) (k 1)))) (h "1lxfsd0hbrqpng7y8pkcskvd0gmgshpmjqp66ng9d976xwahsm0n") (f (quote (("stable" "syntex" "syntex_errors" "syntex_syntax" "synthax/stable" "easy-plugin-plugins/stable"))))))

(define-public crate-easy-plugin-0.8.1 (c (n "easy-plugin") (v "0.8.1") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "easy-plugin-plugins") (r "^0.8.1") (d #t) (k 0)) (d (n "easy-plugin-plugins") (r "^0.8.1") (d #t) (k 1)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 1)) (d (n "syntex_errors") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "0.*") (o #t) (d #t) (k 0)) (d (n "synthax") (r "^0.3.1") (d #t) (k 0)) (d (n "synthax") (r "^0.3.1") (d #t) (k 1)))) (h "1v95an0m8159r8pynwjhrcdbkim005s18dn2aa4sdkjkq3yp9jdc") (f (quote (("stable" "syntex" "syntex_errors" "syntex_syntax" "synthax/stable" "easy-plugin-plugins/stable"))))))

(define-public crate-easy-plugin-0.9.0 (c (n "easy-plugin") (v "0.9.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "easy-plugin-plugins") (r "^0.9.0") (d #t) (k 0)) (d (n "easy-plugin-plugins") (r "^0.9.0") (d #t) (k 1)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 1)) (d (n "syntex_errors") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "0.*") (o #t) (d #t) (k 0)) (d (n "synthax") (r "^0.3.1") (d #t) (k 0)) (d (n "synthax") (r "^0.3.1") (d #t) (k 1)))) (h "1r242m30pi4abzi03wmx5mnkzgkshisywlvqggzbqjw53pd027i6") (f (quote (("stable" "syntex" "syntex_errors" "syntex_syntax" "synthax/stable" "easy-plugin-plugins/stable"))))))

(define-public crate-easy-plugin-0.9.1 (c (n "easy-plugin") (v "0.9.1") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "easy-plugin-plugins") (r "^0.9.1") (d #t) (k 0)) (d (n "easy-plugin-plugins") (r "^0.9.1") (d #t) (k 1)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 1)) (d (n "syntex_errors") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "0.*") (o #t) (d #t) (k 0)) (d (n "synthax") (r "^0.3.2") (d #t) (k 0)) (d (n "synthax") (r "^0.3.2") (d #t) (k 1)))) (h "090vvfgaz5mwa0hsx3mslxg2lwpnvzk9b1njz4xv8cq132qh5k77") (f (quote (("stable" "syntex" "syntex_errors" "syntex_syntax" "synthax/stable" "easy-plugin-plugins/stable"))))))

(define-public crate-easy-plugin-0.10.0 (c (n "easy-plugin") (v "0.10.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "easy-plugin-parsers") (r "^0.10.0") (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 1)) (d (n "syntex_errors") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "0.*") (o #t) (d #t) (k 0)) (d (n "synthax") (r "^0.3.2") (d #t) (k 0)) (d (n "synthax") (r "^0.3.2") (d #t) (k 1)))) (h "1fal8fcwikyjivz33v2aknfj7iqpg0pnyak00c2qx0bfpkj99fsy") (f (quote (("stable" "easy-plugin-parsers/stable" "syntex" "syntex_errors" "syntex_syntax" "synthax/stable"))))))

(define-public crate-easy-plugin-0.11.0 (c (n "easy-plugin") (v "0.11.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "easy-plugin-parsers") (r "^0.11.0") (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 1)) (d (n "syntex_errors") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "0.*") (o #t) (d #t) (k 0)) (d (n "synthax") (r "^0.3.2") (d #t) (k 0)) (d (n "synthax") (r "^0.3.2") (d #t) (k 1)))) (h "0bs26i6xsl7dxlajnf1arkp3vj9hx4rcdss4xkf8gggf66kxsr2b") (f (quote (("stable" "easy-plugin-parsers/stable" "syntex" "syntex_errors" "syntex_syntax" "synthax/stable"))))))

(define-public crate-easy-plugin-0.11.1 (c (n "easy-plugin") (v "0.11.1") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "easy-plugin-parsers") (r "^0.11.1") (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 1)) (d (n "syntex_errors") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "0.*") (o #t) (d #t) (k 0)) (d (n "synthax") (r "^0.3.2") (d #t) (k 0)) (d (n "synthax") (r "^0.3.2") (d #t) (k 1)))) (h "12b3w06hyjm4as23azph13vfapyvrsyllk9v6md9ybmk8j478fnr") (f (quote (("stable" "easy-plugin-parsers/stable" "syntex" "syntex_errors" "syntex_syntax" "synthax/stable"))))))

(define-public crate-easy-plugin-0.11.2 (c (n "easy-plugin") (v "0.11.2") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "easy-plugin-parsers") (r "^0.11.2") (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 1)) (d (n "syntex_errors") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "0.*") (o #t) (d #t) (k 0)) (d (n "synthax") (r "^0.3.2") (d #t) (k 0)) (d (n "synthax") (r "^0.3.2") (d #t) (k 1)))) (h "0y5cz74r13h557nz1kkgg7lvif33ca8ajfbvzglwscrsqa0xv0i1") (f (quote (("stable" "easy-plugin-parsers/stable" "syntex" "syntex_errors" "syntex_syntax" "synthax/stable"))))))

(define-public crate-easy-plugin-0.11.3 (c (n "easy-plugin") (v "0.11.3") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "easy-plugin-parsers") (r "^0.11.3") (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 1)) (d (n "syntex_errors") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "0.*") (o #t) (d #t) (k 0)) (d (n "synthax") (r "^0.3.5") (d #t) (k 0)) (d (n "synthax") (r "^0.3.5") (d #t) (k 1)))) (h "025xwb8k0zngxy4bm2vaf4h3q8cdmm3zbbnah12f8my5z9pikdiz") (f (quote (("stable" "easy-plugin-parsers/stable" "syntex" "syntex_errors" "syntex_syntax" "synthax/stable"))))))

(define-public crate-easy-plugin-0.11.4 (c (n "easy-plugin") (v "0.11.4") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "easy-plugin-parsers") (r "^0.11.4") (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 1)) (d (n "syntex_errors") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "0.*") (o #t) (d #t) (k 0)) (d (n "synthax") (r "^0.3.5") (d #t) (k 0)) (d (n "synthax") (r "^0.3.5") (d #t) (k 1)))) (h "0wr1wl2wbn24pgzzabl3vvig3q3gw7xx4lacqmla72zpw2zdg4zm") (f (quote (("stable" "easy-plugin-parsers/stable" "syntex" "syntex_errors" "syntex_syntax" "synthax/stable"))))))

(define-public crate-easy-plugin-0.11.5 (c (n "easy-plugin") (v "0.11.5") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "easy-plugin-parsers") (r "^0.11.5") (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 1)) (d (n "syntex_errors") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "0.*") (o #t) (d #t) (k 0)) (d (n "synthax") (r "^0.3.5") (d #t) (k 0)) (d (n "synthax") (r "^0.3.5") (d #t) (k 1)))) (h "1pvli70sd62073byiz46vjk2w0m0mcgp5m65acjlfvn4m8wckfbj") (f (quote (("stable" "easy-plugin-parsers/stable" "syntex" "syntex_errors" "syntex_syntax" "synthax/stable"))))))

(define-public crate-easy-plugin-0.11.6 (c (n "easy-plugin") (v "0.11.6") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "easy-plugin-parsers") (r "^0.11.6") (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 1)) (d (n "syntex_errors") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "0.*") (o #t) (d #t) (k 0)) (d (n "synthax") (r "^0.3.5") (d #t) (k 0)) (d (n "synthax") (r "^0.3.5") (d #t) (k 1)))) (h "0bwy33djl6q2ksxabcjgk3y417v8q88xzhiy5lyrdn1gy63dl7w0") (f (quote (("stable" "easy-plugin-parsers/stable" "syntex" "syntex_errors" "syntex_syntax" "synthax/stable"))))))

(define-public crate-easy-plugin-0.11.7 (c (n "easy-plugin") (v "0.11.7") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "easy-plugin-parsers") (r "^0.11.7") (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 1)) (d (n "syntex_errors") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "0.*") (o #t) (d #t) (k 0)) (d (n "synthax") (r "^0.3.5") (d #t) (k 0)) (d (n "synthax") (r "^0.3.5") (d #t) (k 1)))) (h "1g3yx2n1i8v2w4qrmdpffpimay6cgjk5gsjaz1k8dr31c4kp1shl") (f (quote (("stable" "easy-plugin-parsers/stable" "syntex" "syntex_errors" "syntex_syntax" "synthax/stable"))))))

(define-public crate-easy-plugin-0.11.8 (c (n "easy-plugin") (v "0.11.8") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "easy-plugin-parsers") (r "^0.11.8") (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 1)) (d (n "syntex_errors") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "0.*") (o #t) (d #t) (k 0)) (d (n "synthax") (r "^0.3.5") (d #t) (k 0)) (d (n "synthax") (r "^0.3.5") (d #t) (k 1)))) (h "0xf2njr1xbn1s5yk2jqgylrdpr3zgvjyb1acjc7fmglfndxh4kip") (f (quote (("stable" "easy-plugin-parsers/stable" "syntex" "syntex_errors" "syntex_syntax" "synthax/stable"))))))

