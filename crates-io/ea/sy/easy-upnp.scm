(define-module (crates-io ea sy easy-upnp) #:use-module (crates-io))

(define-public crate-easy-upnp-0.1.0 (c (n "easy-upnp") (v "0.1.0") (d (list (d (n "cidr-utils") (r "^0.5.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "get_if_addrs") (r "^0.5.3") (d #t) (k 0)) (d (n "igd") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xdh9hi7png7h97s0lzpcvz6d4dx3v88j248hdmk2z5x1m07ibqa")))

(define-public crate-easy-upnp-0.1.1 (c (n "easy-upnp") (v "0.1.1") (d (list (d (n "cidr-utils") (r "^0.5.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "get_if_addrs") (r "^0.5.3") (d #t) (k 0)) (d (n "igd") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0drf8dndfl8ga8ifngjn95gcr18ff5lin52hp171ahmx8gd75fad")))

(define-public crate-easy-upnp-0.2.0 (c (n "easy-upnp") (v "0.2.0") (d (list (d (n "cidr-utils") (r "^0.5.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "get_if_addrs") (r "^0.5.3") (d #t) (k 0)) (d (n "igd") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1ybpv0z7dcm6790qz463bibh5zxrm6ghygqm8b1m016g5vphb5xa")))

