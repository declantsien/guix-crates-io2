(define-module (crates-io ea sy easy-logging) #:use-module (crates-io))

(define-public crate-easy-logging-0.1.0 (c (n "easy-logging") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0x3y48zvzanrxxpciqniqdg1820gmlf4ii85h79cwg08xczmmblh")))

(define-public crate-easy-logging-0.1.1 (c (n "easy-logging") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "135scwdzw6ryalzrxqpq8xk4ys1fif79z66csgfwvc86f8pshq4i")))

(define-public crate-easy-logging-0.1.2 (c (n "easy-logging") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0yfp2s9zxgnc8kjyqdqj9b0gapplxnm27gz301rs50pa11zhxli9")))

(define-public crate-easy-logging-0.2.0 (c (n "easy-logging") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fern") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0l8c2vw4qr2s95yp931p15cnywmqyjnbmqxp6za25lqmfz5qr602")))

