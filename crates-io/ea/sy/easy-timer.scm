(define-module (crates-io ea sy easy-timer) #:use-module (crates-io))

(define-public crate-easy-timer-0.1.0 (c (n "easy-timer") (v "0.1.0") (h "0i6zfb09mzrkcjqxv4612zz35gf8xi9kkf60svq4d8s7vh6p6jfa") (y #t)))

(define-public crate-easy-timer-1.0.0 (c (n "easy-timer") (v "1.0.0") (h "05ndq8k7fs9w6mf2wvwy1whczi7jp756j31a3qz9cg8bzr13w6sk") (y #t)))

(define-public crate-easy-timer-1.0.1 (c (n "easy-timer") (v "1.0.1") (h "047jwycz0y0mpm277m786pph5s02cvq3gk9b2k1sqhilhkpgyir8") (y #t)))

(define-public crate-easy-timer-1.0.2 (c (n "easy-timer") (v "1.0.2") (h "0x1rvwwyh2k6pi25kaf4yrn2kcrh6v1rqkb7alsvs3d256cbisyr") (y #t)))

