(define-module (crates-io ea sy easy-hasher) #:use-module (crates-io))

(define-public crate-easy-hasher-0.9.9 (c (n "easy-hasher") (v "0.9.9") (d (list (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "0ymh0kars58r2y6rrfi6gj88604v5a51dm8m944hs8kfv5qrslzj")))

(define-public crate-easy-hasher-0.10.0 (c (n "easy-hasher") (v "0.10.0") (d (list (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "0s6bmv4nh05l3q80c8jbgw6yiq1bq3b3y0g21a3nph6wx38pcqjq")))

(define-public crate-easy-hasher-0.10.1 (c (n "easy-hasher") (v "0.10.1") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "19n2i3l3w79p0n3pns6y4j8yqgg1fr7a6zyzhb5y402glmbq97a9")))

(define-public crate-easy-hasher-0.10.2 (c (n "easy-hasher") (v "0.10.2") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "1ck71qgf693ynbv9lfhg5ab4b8hypymj77zh0yk2lwdr91mqpn78")))

(define-public crate-easy-hasher-0.11.0 (c (n "easy-hasher") (v "0.11.0") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "crc8") (r "^0.1.1") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "1z784h85g0kw5mj5szjbrjgmapavq2yvg6sn1g8fgzf804nl4fg6")))

(define-public crate-easy-hasher-0.12.0 (c (n "easy-hasher") (v "0.12.0") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "crc8") (r "^0.1.1") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "11kxw1wp6waq41a9jv01j77jy1fbja2n0gh1lbk9dfpy7j60kmy7")))

(define-public crate-easy-hasher-0.12.1 (c (n "easy-hasher") (v "0.12.1") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "crc8") (r "^0.1.1") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "02j7jalqh08cz6v42z7qws67n25wgzd5qscxg8nvlp97ysq66il1")))

(define-public crate-easy-hasher-0.12.2 (c (n "easy-hasher") (v "0.12.2") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "crc8") (r "^0.1.1") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "1bqkyyk5xfy0sb0rpa8fnrnpw4dnxjcib8k5mkx1ak2jxr3iphna")))

(define-public crate-easy-hasher-0.12.3 (c (n "easy-hasher") (v "0.12.3") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "crc8") (r "^0.1.1") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "0x7cfq90ljf567mva0bxahlmfhm9qkn13kmq7clknfwxh4v0g4yw")))

(define-public crate-easy-hasher-0.12.4 (c (n "easy-hasher") (v "0.12.4") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "crc8") (r "^0.1.1") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "0x5pdw40gn11759pk56a6szp5xypg4fi7d7b60f9kyjk2l2i3jrs")))

(define-public crate-easy-hasher-1.0.0-rc (c (n "easy-hasher") (v "1.0.0-rc") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "crc8") (r "^0.1.1") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "005y7cyybyi2p6l5jqrf77m8r574qml0dmcsvrzq5qw3nmi31yip")))

(define-public crate-easy-hasher-2.0.0-alpha (c (n "easy-hasher") (v "2.0.0-alpha") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "crc8") (r "^0.1.1") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "04z0yv8d7wrlbk3dg6rwjwa6ff5y0cf2d8qf006c5yd9pwh06k3r")))

(define-public crate-easy-hasher-2.0.0 (c (n "easy-hasher") (v "2.0.0") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "crc8") (r "^0.1.1") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.2") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "1kn4jlpb2l68i65f5ifki92gh82lgr9qpqbgaf9vqddcd4dvwf60")))

(define-public crate-easy-hasher-2.0.1 (c (n "easy-hasher") (v "2.0.1") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "crc8") (r "^0.1.1") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.2") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "13i7g0w6inaqzdl0g8qvvyavi04rzsvg0787xz99lgxignja55v4")))

(define-public crate-easy-hasher-2.0.2 (c (n "easy-hasher") (v "2.0.2") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "crc8") (r "^0.1.1") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.2") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "1w1j7j16a492zqlb11nva6595sw88yzika6n38r5s8n0b7vmc8pi")))

(define-public crate-easy-hasher-2.1.0 (c (n "easy-hasher") (v "2.1.0") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "crc8") (r "^0.1.1") (d #t) (k 0)) (d (n "md2") (r "^0.8.0") (d #t) (k 0)) (d (n "md4") (r "^0.8.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.2") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "10nd24jklvhi7g68hl8l4rvwc98ph1qvysj2sfqpqs1lisglndc9")))

(define-public crate-easy-hasher-2.1.1 (c (n "easy-hasher") (v "2.1.1") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "crc8") (r "^0.1.1") (d #t) (k 0)) (d (n "md2") (r "^0.8.0") (d #t) (k 0)) (d (n "md4") (r "^0.8.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.2") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "0q4mni5lvcwc0sa0v18b6rml0bnk8l9r182chkq068mnd8z00042")))

(define-public crate-easy-hasher-2.2.0 (c (n "easy-hasher") (v "2.2.0") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "crc8") (r "^0.1.1") (d #t) (k 0)) (d (n "md2") (r "^0.8.0") (d #t) (k 0)) (d (n "md4") (r "^0.8.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.2") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "15azvisrbvhxrzjciz68fbmmj9p6sgi252sshyjmb7q74gv8ji5z")))

(define-public crate-easy-hasher-2.2.1 (c (n "easy-hasher") (v "2.2.1") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "crc8") (r "^0.1.1") (d #t) (k 0)) (d (n "md2") (r "^0.8.0") (d #t) (k 0)) (d (n "md4") (r "^0.8.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.2") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)))) (h "04j697cbl47gfdgdzmr1lwsggssi2d5k9v56b3wbsgam67dkslci")))

