(define-module (crates-io ea sy easy_retry) #:use-module (crates-io))

(define-public crate-easy_retry-0.1.0 (c (n "easy_retry") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.30") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("time"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1s6zg281sp69hlk9nrk5cmfgmyglk16wfk1mqiiyscs0h3rvz1dn") (f (quote (("async" "futures" "tokio"))))))

