(define-module (crates-io ea sy easygradecalculator) #:use-module (crates-io))

(define-public crate-easygradecalculator-0.1.1 (c (n "easygradecalculator") (v "0.1.1") (h "0ypp9nbmx89v99l1gcvpl3183vsj0miz9nz1h6gbyjjw8gxrnj94")))

(define-public crate-easygradecalculator-0.1.2 (c (n "easygradecalculator") (v "0.1.2") (h "1qgkhcvl2mfk242z2y3jcvbfv0nq2qjdr4vd1qkb6zlgz73cydag")))

(define-public crate-easygradecalculator-0.1.3 (c (n "easygradecalculator") (v "0.1.3") (h "1j8szzmr1k3sv62xs0dqkm7sg3q1mrzl7xamyr6kwdizdqr9wwzz")))

