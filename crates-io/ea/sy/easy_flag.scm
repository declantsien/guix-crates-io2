(define-module (crates-io ea sy easy_flag) #:use-module (crates-io))

(define-public crate-easy_flag-0.1.0 (c (n "easy_flag") (v "0.1.0") (h "0piqb5prkcddk30nyik8dnw78sk50gd79bmrqhkp0b9bfsq3lzvp")))

(define-public crate-easy_flag-0.1.1 (c (n "easy_flag") (v "0.1.1") (h "0724lq00fh53baqmkkdkz66ankpasm861r81ndlsfpswx1infvn0")))

(define-public crate-easy_flag-0.1.2 (c (n "easy_flag") (v "0.1.2") (h "0jhchv12waamqzlcq3xlq0s9njf30fxqbl09wvg1a0fsbdn8ibg7")))

