(define-module (crates-io ea sy easy_switch) #:use-module (crates-io))

(define-public crate-easy_switch-0.1.0 (c (n "easy_switch") (v "0.1.0") (d (list (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)))) (h "0gydq2mi2acdyagnzpynjx7xlk3w4m5s5m67yfg6ylxv7gv6j5n7") (r "1.56.1")))

(define-public crate-easy_switch-0.2.0 (c (n "easy_switch") (v "0.2.0") (d (list (d (n "macrotest") (r "^1.0.9") (d #t) (k 2)))) (h "1y0pb2vkcn4k6qma28j837wv4q89idyg3sx006rks8p5slwzz5rf") (r "1.56.1")))

