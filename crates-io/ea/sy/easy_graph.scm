(define-module (crates-io ea sy easy_graph) #:use-module (crates-io))

(define-public crate-easy_graph-0.1.0 (c (n "easy_graph") (v "0.1.0") (h "0naf12r8ay456x76xx52c5c0dvniqc0wd3w9smbr1p12075qn163")))

(define-public crate-easy_graph-0.1.1 (c (n "easy_graph") (v "0.1.1") (h "1yb5a15xiknszkn6p65d3cg889n1nfz2315dj0s7y61hb559l0w6")))

(define-public crate-easy_graph-0.1.2 (c (n "easy_graph") (v "0.1.2") (h "1hkj6j75k5y8gbdy4qpijdg0x84izjdajzrdjbzq9p7ir99b7yj9")))

(define-public crate-easy_graph-0.1.3 (c (n "easy_graph") (v "0.1.3") (h "058ksk1w8sxwprf2gvpdzgs446v99yzd9qvdnhnv6jbw55rmi8lv")))

(define-public crate-easy_graph-0.1.4 (c (n "easy_graph") (v "0.1.4") (h "1qr8vjdpg3pw4wf24jcacg8kpq79f3imsg5rrz9iy5nnrl5j3dk1")))

(define-public crate-easy_graph-0.1.5 (c (n "easy_graph") (v "0.1.5") (h "00np7d6whn4x42bp8g8w3nbsqrfmcazd0kzdmqsgblssmayilxww")))

