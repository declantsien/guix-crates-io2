(define-module (crates-io ea sy easy_ga) #:use-module (crates-io))

(define-public crate-easy_ga-0.1.0 (c (n "easy_ga") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0bwddmniwxx1m3annpy6fznvjf8p0vsy7f37pdyqqr06m50sdy00")))

(define-public crate-easy_ga-0.1.1 (c (n "easy_ga") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0mzcaln3kdva81r17hq67ap21c7syinmssw1d3xiqqw6n4r2d0kq")))

(define-public crate-easy_ga-1.0.0 (c (n "easy_ga") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0vw7f0za13n5swngrrkpfsap821g6kms4mjf3vs9rbi8l5kcmhl7")))

(define-public crate-easy_ga-1.0.1 (c (n "easy_ga") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "10y6w9sg1v3zvb23ric6czl7j0llmmpd50axdj71wqlsvnn5nvcb")))

(define-public crate-easy_ga-1.1.0 (c (n "easy_ga") (v "1.1.0") (d (list (d (n "cpu-time") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1wz8jsgcdjjg2xiakhhplmaqr6nrwb655scs7gvk5dsla1jbshpw")))

(define-public crate-easy_ga-1.1.1 (c (n "easy_ga") (v "1.1.1") (d (list (d (n "cpu-time") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0np2h2pb6y4iscscb6z0gmw6dr4ad58mb4jfhccshx1pyc1q9dsi")))

(define-public crate-easy_ga-1.2.0 (c (n "easy_ga") (v "1.2.0") (d (list (d (n "cpu-time") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "13m4mkjr8rnrkv70c73l7806l6az1qlsar3p1xzwdgpmvrfi8w5d")))

(define-public crate-easy_ga-1.2.1 (c (n "easy_ga") (v "1.2.1") (d (list (d (n "cpu-time") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1azxdkafn4r60a55vd5wnjh0bkli0bmg4n9hzvsv8cgb3lfbds6q")))

