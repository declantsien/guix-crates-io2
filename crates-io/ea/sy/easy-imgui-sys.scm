(define-module (crates-io ea sy easy-imgui-sys) #:use-module (crates-io))

(define-public crate-easy-imgui-sys-0.1.0 (c (n "easy-imgui-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "xshell") (r "^0.2") (d #t) (k 1)))) (h "0m82hs7kkccmm5dzwql60lc8bvgjfw6n7r5wk89lm5a4iylgznjm") (f (quote (("freetype") ("docking")))) (l "imgui")))

(define-public crate-easy-imgui-sys-0.1.1 (c (n "easy-imgui-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "xshell") (r "^0.2") (d #t) (k 1)))) (h "1xxn8qrjyrff8lwmg9r9h2py28bsz9f8ic0smipkl5y335ibfzqw") (f (quote (("freetype") ("docking")))) (l "imgui")))

(define-public crate-easy-imgui-sys-0.2.0 (c (n "easy-imgui-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "xshell") (r "^0.2") (d #t) (k 1)))) (h "17ias29xssh067f1b9cimrs90yxxf70si8mxa1hj8857bhl5aq3f") (f (quote (("freetype") ("docking")))) (l "imgui")))

(define-public crate-easy-imgui-sys-0.2.1 (c (n "easy-imgui-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "xshell") (r "^0.2") (d #t) (k 1)))) (h "06z38piskddwd1m2bqhc72hchq3nhdizz2qaflyq30rcb1sks246") (f (quote (("freetype") ("docking")))) (l "imgui")))

(define-public crate-easy-imgui-sys-0.2.2 (c (n "easy-imgui-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "xshell") (r "^0.2") (d #t) (k 1)))) (h "05mf33q4jp12i7cyxp6dp2zv9kydl8j14m6702zsr3psmy53lkd9") (f (quote (("freetype") ("docking")))) (l "imgui")))

(define-public crate-easy-imgui-sys-0.2.3 (c (n "easy-imgui-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "xshell") (r "^0.2") (d #t) (k 1)))) (h "1l1ax86zphijix1x0bg3l718pnd77r04w5r1didnzygqb76hcvx4") (f (quote (("freetype") ("docking")))) (l "imgui")))

(define-public crate-easy-imgui-sys-0.2.4 (c (n "easy-imgui-sys") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "xshell") (r "^0.2") (d #t) (k 1)))) (h "1hsv45gkk3ps5xgk5nmb0ypkbanalk1jz4fy6ji173934cvs68vp") (f (quote (("freetype") ("docking")))) (l "imgui")))

(define-public crate-easy-imgui-sys-0.3.0 (c (n "easy-imgui-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "xshell") (r "^0.2") (d #t) (k 1)))) (h "0c0l813lgwym11q85ib9agrsyndc0bvpvrhzpxi80z56y02qk6mb") (f (quote (("freetype") ("docking")))) (l "imgui")))

(define-public crate-easy-imgui-sys-0.4.0 (c (n "easy-imgui-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "xshell") (r "^0.2") (d #t) (k 1)))) (h "1pcjxbhfif3hj4h7ac3d9c20kwbiiz1m5jk1h31sz57w6295zh3m") (f (quote (("freetype") ("docking")))) (l "imgui")))

(define-public crate-easy-imgui-sys-0.4.1 (c (n "easy-imgui-sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "xshell") (r "^0.2") (d #t) (k 1)))) (h "06fqh7zzx9ygx1x1a24n9rlzjrig2xvkj7rhb9kcz7m18rn5sf73") (f (quote (("freetype") ("docking")))) (l "imgui")))

