(define-module (crates-io ea sy easy-csv) #:use-module (crates-io))

(define-public crate-easy-csv-0.1.0 (c (n "easy-csv") (v "0.1.0") (d (list (d (n "csv") (r "^0.14") (d #t) (k 0)) (d (n "easy-csv-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1ihsvnyg35708m69lmf600rm9kjrgfj6kfhayzn8jg164nc4hmgk")))

(define-public crate-easy-csv-0.1.1 (c (n "easy-csv") (v "0.1.1") (d (list (d (n "csv") (r "^0.14") (d #t) (k 0)) (d (n "easy-csv-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0vmbc3500qaicxvx75py2jdpnaihxxrl33v99f4jq7w9sqam7fjd")))

(define-public crate-easy-csv-0.2.0 (c (n "easy-csv") (v "0.2.0") (d (list (d (n "csv") (r "^0.14") (d #t) (k 0)) (d (n "easy-csv-derive") (r "^0.2.0") (d #t) (k 0)))) (h "0spn92k9h4w2g7vzqh87gb3c2z9yi85fhsxzr7yndcw4s3xddmxc")))

(define-public crate-easy-csv-0.2.1 (c (n "easy-csv") (v "0.2.1") (d (list (d (n "csv") (r "^0.15") (d #t) (k 0)) (d (n "easy-csv-derive") (r "^0.2.0") (d #t) (k 0)))) (h "1k6sgpam20sa3v441cqmg2xsfw60xw79j5w2pxivv9c0bkajd5jx")))

(define-public crate-easy-csv-0.3.0 (c (n "easy-csv") (v "0.3.0") (d (list (d (n "csv") (r "^0.15") (d #t) (k 0)) (d (n "easy-csv-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1iry3gh4wjh1jjxcn7fvrxyvpj2l24igy057p1agq86mxwrfdq5m")))

(define-public crate-easy-csv-0.3.1 (c (n "easy-csv") (v "0.3.1") (d (list (d (n "csv") (r "^0.15") (d #t) (k 0)) (d (n "easy-csv-derive") (r "^0.3.1") (d #t) (k 0)))) (h "1janzm79kjzz6y2fik90nvwydv4bj13hm1z8pnbdkwjhrscihs3y")))

(define-public crate-easy-csv-0.3.2 (c (n "easy-csv") (v "0.3.2") (d (list (d (n "csv") (r "^0.15") (d #t) (k 0)) (d (n "easy-csv-derive") (r "^0.3.2") (d #t) (k 0)))) (h "0zk0mripk2wymhz10404h0qzh8pbagvz0qn7956c41jfhnjmzwgv")))

