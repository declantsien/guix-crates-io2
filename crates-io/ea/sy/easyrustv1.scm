(define-module (crates-io ea sy easyrustv1) #:use-module (crates-io))

(define-public crate-easyrustv1-0.1.0 (c (n "easyrustv1") (v "0.1.0") (h "1b6lygqhjbacx1ck3xmzwkcwhjkjnnlv8x3mlpn8azikgdrwx9j6") (y #t)))

(define-public crate-easyrustv1-0.1.2 (c (n "easyrustv1") (v "0.1.2") (h "0wdfyrfvf6w8z54qrgbjj1x94g149vmkbgric1qrcvvsz5km8mx2") (y #t)))

(define-public crate-easyrustv1-0.1.1 (c (n "easyrustv1") (v "0.1.1") (h "04pw5zs97a2v25r3lp9363a1l1xsskx9jlhhvwvr63n40jsn2lrw") (y #t)))

(define-public crate-easyrustv1-0.1.3 (c (n "easyrustv1") (v "0.1.3") (h "0p7kxk7aric01yx2lcclb4d0rr9wmavzjla3x4v5m9987dz40yk3") (y #t)))

(define-public crate-easyrustv1-0.1.4 (c (n "easyrustv1") (v "0.1.4") (h "0b4lcff7h6xfv4dk5i7czrr5xmw7wwv6v59hgcw4b5d4rjnsdqjw")))

