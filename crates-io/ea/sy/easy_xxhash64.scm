(define-module (crates-io ea sy easy_xxhash64) #:use-module (crates-io))

(define-public crate-easy_xxhash64-1.1.5 (c (n "easy_xxhash64") (v "1.1.5") (d (list (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)))) (h "0basrcspyw1wimggbbnfh8c7ji04pgjc011pzzsm3zp4incik8zj")))

(define-public crate-easy_xxhash64-1.1.6 (c (n "easy_xxhash64") (v "1.1.6") (d (list (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)))) (h "10c6jbb5755ziimsrbixcwz9g292plrvhkxyl5c7fly2p44pf561")))

