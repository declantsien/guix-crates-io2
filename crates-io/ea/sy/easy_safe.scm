(define-module (crates-io ea sy easy_safe) #:use-module (crates-io))

(define-public crate-easy_safe-0.1.0 (c (n "easy_safe") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "predicates") (r "^1.0.5") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0c2jjf11v46klp6gxkqn48lkphakv8srsmbf3jk5x0wic4k07fng") (y #t)))

(define-public crate-easy_safe-0.1.1 (c (n "easy_safe") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "predicates") (r "^1.0.5") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1r2izxbjlpqaxgrlmab7wf8zx3vp7hpws79278fm4sixnriqsajc") (y #t)))

(define-public crate-easy_safe-0.1.2 (c (n "easy_safe") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "predicates") (r "^1.0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1h0ns1dysf7y6i2jaw2136whsi2wp2n48jqbiq71db7bgd51jy1s")))

(define-public crate-easy_safe-0.1.3 (c (n "easy_safe") (v "0.1.3") (d (list (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "predicates") (r "^1.0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fg97xsjv6xdbila29iad0pnv3ilxd6kr9582acqwwvy2f9ivx89")))

(define-public crate-easy_safe-0.1.4 (c (n "easy_safe") (v "0.1.4") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "076xvc0yz9byz48ysq1bgpyw00mdbhlyagld09k20j3n4qi2jxah")))

(define-public crate-easy_safe-0.1.5 (c (n "easy_safe") (v "0.1.5") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gm2m3sf1jqb8v933iaqx4z85p3s2n713c43lwidias7g203cwmv")))

(define-public crate-easy_safe-0.1.6 (c (n "easy_safe") (v "0.1.6") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cvdp3awgr5l7padv6cq87p9nwycg9isq7af5a88zbz6bb5f8jd7")))

(define-public crate-easy_safe-0.1.7 (c (n "easy_safe") (v "0.1.7") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1pjfi52dw03ci6zcf4wgcx3kjg12fr6f5g3904mc7i1sbzps27xa")))

(define-public crate-easy_safe-0.1.8 (c (n "easy_safe") (v "0.1.8") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1r8p96556bjcga6nhxxcikw2djmfclf6w3bkjvmshrwjnswxh4mj")))

(define-public crate-easy_safe-0.1.9 (c (n "easy_safe") (v "0.1.9") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09zlwwc1vj6kmbx6cgsk6x03j7lq8riqi1jjic4kyjypgzwag62g")))

