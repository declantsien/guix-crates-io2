(define-module (crates-io ea sy easy-jsonrpc) #:use-module (crates-io))

(define-public crate-easy-jsonrpc-0.1.0 (c (n "easy-jsonrpc") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "easy-jsonrpc-proc-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^10.0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "09g2g6cxfdi2yb9780dil9g8745dmjmmps6ibfv6qmzsc20wncyb")))

(define-public crate-easy-jsonrpc-0.1.1 (c (n "easy-jsonrpc") (v "0.1.1") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "easy-jsonrpc-proc-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^10.0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1glhl1vinywhdz6b6dlklnsan5im02pkmakgabpk2q8zqsnva2w7")))

(define-public crate-easy-jsonrpc-0.1.2 (c (n "easy-jsonrpc") (v "0.1.2") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "easy-jsonrpc-proc-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^10.0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "18yspnq9vf6p8xrfa9dq6a74xi9bhpd3kd6krmdzr8jjc85mpcqf")))

(define-public crate-easy-jsonrpc-0.1.3 (c (n "easy-jsonrpc") (v "0.1.3") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "easy-jsonrpc-proc-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^10.0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1pyf1hi02lcwdm1hvmshyx49kmr5rx5j0y2z5q837ymx8s35ly3x")))

(define-public crate-easy-jsonrpc-0.2.0 (c (n "easy-jsonrpc") (v "0.2.0") (d (list (d (n "easy-jsonrpc-proc-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^10.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0hkybdhv3anaf5lakd0vc45lhmy0h300z4q21h14sih4vd0bf32f")))

(define-public crate-easy-jsonrpc-0.3.0 (c (n "easy-jsonrpc") (v "0.3.0") (d (list (d (n "easy-jsonrpc-proc-macro") (r "^0.3.0") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^10.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0nbwiiscz3ilsq1lg6lfq114wrm8mgh16d2sqf1njspz1k3v4ghb")))

(define-public crate-easy-jsonrpc-0.4.1 (c (n "easy-jsonrpc") (v "0.4.1") (d (list (d (n "easy-jsonrpc-proc-macro") (r "^0.4.1") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^10.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1znbx83rrvh45cd18nzv6w4alh0c2j3da6knjwvdg7jsvym8w39g")))

(define-public crate-easy-jsonrpc-0.5.0 (c (n "easy-jsonrpc") (v "0.5.0") (d (list (d (n "easy-jsonrpc-proc-macro") (r "^0.5.0") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^10.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0nvzsic6gl3rfahb7rn5d4jw4yfib80yx4m743ld1kkavk61avxh")))

(define-public crate-easy-jsonrpc-0.5.1 (c (n "easy-jsonrpc") (v "0.5.1") (d (list (d (n "easy-jsonrpc-proc-macro") (r "^0.5.0") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^10.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0gxr04vx6kwpc4lv2idxlmqps6xh80da3x91bbpaaihwmswllhnb")))

(define-public crate-easy-jsonrpc-0.5.2 (c (n "easy-jsonrpc") (v "0.5.2") (d (list (d (n "easy-jsonrpc-proc-macro") (r "^0.5.0") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^10.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "06naz1dgr6q0gk2sv864abdfgiy31pmpyj7d1nv90mzdw3w53a6l")))

(define-public crate-easy-jsonrpc-0.5.3 (c (n "easy-jsonrpc") (v "0.5.3") (d (list (d (n "easy-jsonrpc-proc-macro") (r "^0.5.0") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^10.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "warp") (r "^0.1.16") (d #t) (k 2)))) (h "0cxff2vxspna6nvjbrylm72sy2lydjlq9pnqxpyvcnvwn1n5rq07")))

