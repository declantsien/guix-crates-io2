(define-module (crates-io ea sy easy_adds) #:use-module (crates-io))

(define-public crate-easy_adds-0.1.0 (c (n "easy_adds") (v "0.1.0") (h "1zwv4abr67gnknkfdykdlhls3w3ynhmz8578lql6di3zm0kd6qvw")))

(define-public crate-easy_adds-0.1.1 (c (n "easy_adds") (v "0.1.1") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "0rx1yp0w24a2a5wnlj1wj9f5qz7yzyfdpl5pp695m6y7wsmk6vwp")))

