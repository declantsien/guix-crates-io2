(define-module (crates-io ea sy easyneural) #:use-module (crates-io))

(define-public crate-easyneural-0.1.0 (c (n "easyneural") (v "0.1.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "if_chain") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ys3xvsd2rmrsxnvshf3nd53kgy0wabr1ai0r63zgjyrj0vx2mpp")))

