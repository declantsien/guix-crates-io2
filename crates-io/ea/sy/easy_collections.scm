(define-module (crates-io ea sy easy_collections) #:use-module (crates-io))

(define-public crate-easy_collections-0.1.0 (c (n "easy_collections") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "1c9f33a1ircn77hzm9nh3sja5cpxl9zkwbrkz6lnpxqfa8v20rbv")))

(define-public crate-easy_collections-0.1.1 (c (n "easy_collections") (v "0.1.1") (d (list (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "00crd48g571cb384n328hi45xln65xfl0nnd7z39rhiimmh80zqp")))

(define-public crate-easy_collections-0.2.0 (c (n "easy_collections") (v "0.2.0") (d (list (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "11bp456sxjz8w4m9k6c7scm09ypghjs3j59vqsqpc0gi28jrapa6")))

(define-public crate-easy_collections-0.3.0 (c (n "easy_collections") (v "0.3.0") (d (list (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "0da5hyskzfjx5w2dc2nhhdhhk6akrsb2hnhcpwzvzn0f4w4xfmy9")))

(define-public crate-easy_collections-0.3.1 (c (n "easy_collections") (v "0.3.1") (d (list (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "1wd18b59w2cs0gyxf6qiyr2p7ccw719b5i1qlxcxzfd1i6q50cyv")))

(define-public crate-easy_collections-0.3.2 (c (n "easy_collections") (v "0.3.2") (d (list (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "02jrc605cihwl1nbj0hsq98wrsvjmsfawzazic5zjfshg2bb4y83")))

(define-public crate-easy_collections-0.4.0 (c (n "easy_collections") (v "0.4.0") (d (list (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "0w4j25al8yzr9sd6gyqp5i48ircrq58akzrrfc7d3sczxvs0y8ww")))

