(define-module (crates-io ea sy easy_rss) #:use-module (crates-io))

(define-public crate-easy_rss-1.0.1 (c (n "easy_rss") (v "1.0.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.18.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0q14b5bjzsn78ddqf0v3krra3hfiww239n4lm449zwr0fklpf9zb")))

