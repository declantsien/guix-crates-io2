(define-module (crates-io ea sy easy) #:use-module (crates-io))

(define-public crate-easy-0.1.0 (c (n "easy") (v "0.1.0") (h "16jpnrq2y1571lpmkvjnk8fdb9y6fsii9rl9lvdb8pbm9asp2wx4")))

(define-public crate-easy-0.1.1 (c (n "easy") (v "0.1.1") (h "0bmylf1l7xprrrhk3n3614fzks055vd7wxdran0wf4k0m2iwq9kd")))

