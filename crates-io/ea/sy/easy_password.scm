(define-module (crates-io ea sy easy_password) #:use-module (crates-io))

(define-public crate-easy_password-0.1.0 (c (n "easy_password") (v "0.1.0") (d (list (d (n "bcrypt") (r "^0.2") (d #t) (k 0)) (d (n "hmac") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)))) (h "1d9wgg753zm0zvi9g9qjkxnfh7pkmkqi8f58ml92gnkhadlf9ang")))

(define-public crate-easy_password-0.1.1 (c (n "easy_password") (v "0.1.1") (d (list (d (n "bcrypt") (r "^0.2") (d #t) (k 0)) (d (n "hmac") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)))) (h "100v4kip4bkmc1wvmh7ii884sqn01i0w0i7hams9nra611qy6175")))

(define-public crate-easy_password-0.1.2 (c (n "easy_password") (v "0.1.2") (d (list (d (n "bcrypt") (r "^0.5.0") (d #t) (k 0)) (d (n "hmac") (r "^0.7.1") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "140z3zm9385bnqv0qg86pqr9js60aqqv8hm2i6z4zw0yavz7dhpk")))

(define-public crate-easy_password-0.1.3 (c (n "easy_password") (v "0.1.3") (d (list (d (n "bcrypt") (r "^0.13.0") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1bwy6c4knic77n16i9yiir91an4yzpszgajm6l9k0b5s9a5n5f5b")))

(define-public crate-easy_password-0.1.4 (c (n "easy_password") (v "0.1.4") (d (list (d (n "bcrypt") (r "^0.14.0") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1xl55j0fhrzc96kzc8x7ghzy69071ip62x8wzqnsds6nv4p8vp16")))

