(define-module (crates-io ea sy easycookie) #:use-module (crates-io))

(define-public crate-easycookie-1.0.0 (c (n "easycookie") (v "1.0.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "native-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rwjifp1idvs51sbdb8ab5c99insqdg5w22vm0fvj70h7d9amrqa")))

(define-public crate-easycookie-1.0.1 (c (n "easycookie") (v "1.0.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "native-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)))) (h "1d2ikvp0sv42q8gss9aqw0cl8i3lwkjrcjs3ri5z1k6i453qcs4v")))

(define-public crate-easycookie-1.0.2 (c (n "easycookie") (v "1.0.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "native-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)))) (h "18b8gwag0ik6l063vbb60rmr8r5fmjwg7rpfqrvc18gasnw601p5")))

(define-public crate-easycookie-1.0.3 (c (n "easycookie") (v "1.0.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "native-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v425f6zyh46iycnxb2dqx6piszv4591mv8sv46s7hbzry3c8wy7")))

