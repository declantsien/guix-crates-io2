(define-module (crates-io ea sy easy-ffi-wrapper) #:use-module (crates-io))

(define-public crate-easy-ffi-wrapper-0.1.0 (c (n "easy-ffi-wrapper") (v "0.1.0") (h "1jfsr82rzydajw6fv9dsb0i6ysz2kpcg62rv9islv0jyccbdqn3w")))

(define-public crate-easy-ffi-wrapper-0.1.1 (c (n "easy-ffi-wrapper") (v "0.1.1") (h "04qzqq116mrnw9nxny2nri707xgdmy77rng4v0wblbyra3hr1yyb")))

