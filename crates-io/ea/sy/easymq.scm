(define-module (crates-io ea sy easymq) #:use-module (crates-io))

(define-public crate-easymq-0.1.0 (c (n "easymq") (v "0.1.0") (d (list (d (n "async-stream") (r "^0.3.2") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.59") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.0") (d #t) (k 0)) (d (n "lapin") (r "^2.2.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1wll9rda984dya6013v6481nr93mkv1wvmpamw0hrcxfi31pbdqg") (f (quote (("default" "lapin")))) (s 2) (e (quote (("lapin" "dep:lapin"))))))

(define-public crate-easymq-0.2.0 (c (n "easymq") (v "0.2.0") (d (list (d (n "async-stream") (r "^0.3.2") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.59") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11.0") (d #t) (k 0)) (d (n "lapin") (r "^2.2.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1i26q86gcp8n3ic355p641ybnlymn93xy0ymim43pbmj322r76c6") (f (quote (("default" "lapin")))) (s 2) (e (quote (("lapin" "dep:lapin"))))))

