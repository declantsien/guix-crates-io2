(define-module (crates-io ea sy easyenv) #:use-module (crates-io))

(define-public crate-easyenv-0.0.1 (c (n "easyenv") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1cjl1jg9arrbzbd1hnf4nccy14kwqfv5zy7nbpfby1kb7061xy7f")))

(define-public crate-easyenv-0.0.2 (c (n "easyenv") (v "0.0.2") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0s4qx7vra2f9jxpjyj3bn7rjrygmpsd4x29qjqfa6pg8p51wfhp3")))

(define-public crate-easyenv-0.0.3 (c (n "easyenv") (v "0.0.3") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "13x2zk41zqza7jzdbmrifcwl5bpki865xh9dghmpgj55j7hrcgih")))

(define-public crate-easyenv-0.0.4 (c (n "easyenv") (v "0.0.4") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1hc242ffs4b6ipz0xvdjfrn2ca2fr7q0abqjb54f7kjr7vmwcvsi")))

(define-public crate-easyenv-0.0.5 (c (n "easyenv") (v "0.0.5") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1m50hz37z8fn41vy3vc1dci3b337pxy99gchx3xn9wwlwyz58845")))

(define-public crate-easyenv-0.0.6 (c (n "easyenv") (v "0.0.6") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0i2ryj78q3nr21cl1j1li96d399ssgxxmag29rqcn21l2njlw178")))

(define-public crate-easyenv-0.0.7 (c (n "easyenv") (v "0.0.7") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1afg1sz79gr04wmq68rh1w411ibgjrm7vlbqhy875ap278pnsmqz")))

(define-public crate-easyenv-0.0.8 (c (n "easyenv") (v "0.0.8") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "17agd3j2kl7483r38jy3d5zjc5yvvl7h4ar4isb3jbag5cib165r")))

(define-public crate-easyenv-0.0.9 (c (n "easyenv") (v "0.0.9") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "19vmsm3gsamxnrsf2v2pkhy3g1mg2vs01vbdgwl0b1n2ml3d7wqr")))

