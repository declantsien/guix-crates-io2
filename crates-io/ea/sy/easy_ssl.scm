(define-module (crates-io ea sy easy_ssl) #:use-module (crates-io))

(define-public crate-easy_ssl-0.0.1 (c (n "easy_ssl") (v "0.0.1") (d (list (d (n "openssl") (r "^0.10.26") (d #t) (k 0)))) (h "06wx70b6a5d7mhbhbiysw0a0mxdfy3gdwwyh33sd0vli76d7djba")))

(define-public crate-easy_ssl-0.0.2 (c (n "easy_ssl") (v "0.0.2") (d (list (d (n "openssl") (r "^0.10.26") (d #t) (k 0)))) (h "0md2k1310x09bf5p2h3y4vw6ng5vpzij2vggb65bbs8hfanfz5ir")))

(define-public crate-easy_ssl-0.0.3 (c (n "easy_ssl") (v "0.0.3") (d (list (d (n "openssl") (r "^0.10.26") (d #t) (k 0)))) (h "05gxi00a69flxzi67l3088kqdlpm1x35li89sr90zan6zg1cm6rz")))

(define-public crate-easy_ssl-0.0.4 (c (n "easy_ssl") (v "0.0.4") (d (list (d (n "openssl") (r "^0.10.26") (d #t) (k 0)))) (h "0hjc9saz4q6i051nc676garn6ans9yl0862xd1g4mll6gs5062fp")))

