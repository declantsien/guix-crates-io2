(define-module (crates-io ea sy easy-error) #:use-module (crates-io))

(define-public crate-easy-error-0.1.0 (c (n "easy-error") (v "0.1.0") (h "1239sbqbsdbssh5vkbym7aw29vpxh7azmg1li7ba4yxc1why6ci8")))

(define-public crate-easy-error-0.1.1 (c (n "easy-error") (v "0.1.1") (h "0s6zjv3mia1r8k0i2ybjak01kjz30lfgi0i3ncg30k6acn03k87n")))

(define-public crate-easy-error-0.2.0 (c (n "easy-error") (v "0.2.0") (h "0d5imgka0f0i4n39b1v8mpxrmwwgzdl59am1ab696if6xfsqh080")))

(define-public crate-easy-error-0.3.0 (c (n "easy-error") (v "0.3.0") (h "1d3vx6mf81r1l9sfs27xrkg9cncmrhnh4h7z1sc9z59j60w9jn5q") (y #t)))

(define-public crate-easy-error-0.3.1 (c (n "easy-error") (v "0.3.1") (h "0glj8h5z40bpll4pv4z0d0ljpy0lvax6v5wq1g7sj0ri614sj1vk")))

(define-public crate-easy-error-1.0.0 (c (n "easy-error") (v "1.0.0") (h "05if47rpkq0gndk2zdbwvwmym0kygikvbsqn1pshha8xqqbrgk04")))

