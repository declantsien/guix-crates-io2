(define-module (crates-io ea sy easy-xml) #:use-module (crates-io))

(define-public crate-easy-xml-0.1.0 (c (n "easy-xml") (v "0.1.0") (d (list (d (n "easy-xml-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1kqsdx6dz6jks1ijv4jivpxiby7mnk3r8a6ddgml9jk5kviwxbda")))

(define-public crate-easy-xml-0.1.1 (c (n "easy-xml") (v "0.1.1") (d (list (d (n "easy-xml-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "0pvm9w6rpmih9py6gakqfpyadikvf03yg1ic9raqq909cwpmw2aa")))

(define-public crate-easy-xml-0.1.2-beta (c (n "easy-xml") (v "0.1.2-beta") (d (list (d (n "easy-xml-derive") (r "^0.1.2-beta") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "09vj9zb8fsbd5gpvzr5fd84prqcg45gxhwa00nzrs4s8dkssfw5f")))

(define-public crate-easy-xml-0.1.2-beta.1 (c (n "easy-xml") (v "0.1.2-beta.1") (d (list (d (n "easy-xml-derive") (r "^0.1.2-beta.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "117c63rl6b7cnhdhzqjl10pj26fczd125lsvwqgwiahgcskz7vj6")))

(define-public crate-easy-xml-0.1.2-beta.2 (c (n "easy-xml") (v "0.1.2-beta.2") (d (list (d (n "easy-xml-derive") (r "^0.1.2-beta.2") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "0xrfbfcarwj4n5xr9dkw09vxikpdzq69nsni477d39kkf0p27fyw")))

(define-public crate-easy-xml-0.1.2 (c (n "easy-xml") (v "0.1.2") (d (list (d (n "easy-xml-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1n5rzl3kn6zm3lpcmkc0lmhd29f1rz51yjkpqnjy0n0a8a1mgc60")))

(define-public crate-easy-xml-0.1.3 (c (n "easy-xml") (v "0.1.3") (d (list (d (n "easy-xml-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "06iyb5xrxkj0fyn1gbiv585i4i0zf2zrj54q6s8vpyi08sr9k5hg")))

(define-public crate-easy-xml-0.1.4 (c (n "easy-xml") (v "0.1.4") (d (list (d (n "easy-xml-derive") (r "^0.1.4") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1wgmb0zk6sajimxnwwq2iw8ij87nbmff62y8hf2yhq5l9mrjvrk3")))

