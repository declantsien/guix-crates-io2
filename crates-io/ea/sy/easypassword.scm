(define-module (crates-io ea sy easypassword) #:use-module (crates-io))

(define-public crate-easypassword-0.1.0 (c (n "easypassword") (v "0.1.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1af83cq32wgv8ms6c4l4vd48zdgpc09mr0bgid13g25ki7y5myri")))

(define-public crate-easypassword-0.1.1 (c (n "easypassword") (v "0.1.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0zahl0ly1g28pxmxsvz6h62rlrp25iwbal7fhjrp5il6kzgp97dc")))

(define-public crate-easypassword-0.1.2 (c (n "easypassword") (v "0.1.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0ii13dg7k82j08y7kvqijkg2mxhx3v24hrm93y3z6qvs2s8h7667")))

(define-public crate-easypassword-0.1.4 (c (n "easypassword") (v "0.1.4") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0gxkcnvwlbyyfy8281il4rx7wqsxwxq7viz3rbyx1603h4sg8qh0")))

(define-public crate-easypassword-0.1.5 (c (n "easypassword") (v "0.1.5") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "08x7jphlylrzv5lqxnysqqnjxlnx36b0pfp2l8sg4675da1mchf1")))

(define-public crate-easypassword-0.1.6 (c (n "easypassword") (v "0.1.6") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "142cam95aa5cnfxcjsji7p4f8h7rzyrixm9bwknin3j9z4gd220p")))

(define-public crate-easypassword-0.2.0 (c (n "easypassword") (v "0.2.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0mh0wa2hs04b1423d0nkw1ljixyw0acryvxjsjyn1pv9537smafi")))

(define-public crate-easypassword-0.2.1 (c (n "easypassword") (v "0.2.1") (d (list (d (n "exitcode") (r "^1.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1jivi2mh4psahmbmzxh0a78lhk4d599ynf4i61466vypwsxbryli")))

