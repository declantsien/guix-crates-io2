(define-module (crates-io ea sy easy-rsa-registry) #:use-module (crates-io))

(define-public crate-easy-rsa-registry-0.1.0 (c (n "easy-rsa-registry") (v "0.1.0") (d (list (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0bs3q8ncrxmp8i6bhg4hzlkxvxs9ixypbc4kdkx0rpz4mbpkn9hp")))

(define-public crate-easy-rsa-registry-0.2.0 (c (n "easy-rsa-registry") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0jfm0qqpqdkz52d4wc9lr6a3xwg8qc33gdx7k3hfbya88vriwjqf")))

(define-public crate-easy-rsa-registry-0.2.1 (c (n "easy-rsa-registry") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0ds27c8dzfnk0vqpraa79bfkk6q5gd7v9256ci30ghnb0h51d7ds")))

(define-public crate-easy-rsa-registry-0.2.2 (c (n "easy-rsa-registry") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1iy0vyfdhjv2z55zams875yha9hl694z6yl693viw2ccjnw4f0hx")))

(define-public crate-easy-rsa-registry-0.2.3 (c (n "easy-rsa-registry") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1rrc3vfagm4kyvmhk36zw493wz2mh01aj9krqcqn57mzfafzyln4")))

(define-public crate-easy-rsa-registry-0.3.0 (c (n "easy-rsa-registry") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1hp66gq7lwyfqi0ql05bs5jk2xqzyqhzvw99r9gd1f31mww4zgmw")))

