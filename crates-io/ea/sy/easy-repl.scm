(define-module (crates-io ea sy easy-repl) #:use-module (crates-io))

(define-public crate-easy-repl-0.1.0 (c (n "easy-repl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustyline") (r "^8.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.4") (d #t) (k 0)) (d (n "shell-words") (r "^1.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trie-rs") (r "^0.1") (d #t) (k 0)))) (h "0m0cg09snhbbyk9v68kvmdcmssmlzgzmgrw1p5cvsw3fcdfqllpf")))

(define-public crate-easy-repl-0.2.0 (c (n "easy-repl") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustyline") (r "^8.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.4") (d #t) (k 0)) (d (n "shell-words") (r "^1.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trie-rs") (r "^0.1") (d #t) (k 0)))) (h "13a5ai34fj5hdks4s75bm3psfzsr456za93k314q866bzrds5siz")))

(define-public crate-easy-repl-0.2.1 (c (n "easy-repl") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6") (d #t) (k 0)) (d (n "shell-words") (r "^1.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "trie-rs") (r "^0.1") (d #t) (k 0)))) (h "05panwswfcifl9ipmixsigbsaik5i924swnnz1biqiaws8g01l41")))

