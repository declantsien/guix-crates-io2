(define-module (crates-io ea sy easy_random) #:use-module (crates-io))

(define-public crate-easy_random-0.1.0 (c (n "easy_random") (v "0.1.0") (h "158ahfy1kqn51mwxz6w5pl53xi6ybi0347vk24jfz7isir3bl2c0")))

(define-public crate-easy_random-0.1.1 (c (n "easy_random") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1nzhdiy5qp97w135ah6rx636b08vl3cmpl43qgsqr1l93wv8k1fr")))

(define-public crate-easy_random-0.1.3 (c (n "easy_random") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0frqfg5svyi7y1w1y35gj41prqw0any3dig6csjjrw2gqdvpbsi6")))

(define-public crate-easy_random-0.1.4 (c (n "easy_random") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "05pfyy5fbq2ksdyql76c7dqn3hcja7zsihxb3zwfi15ayg4fvamn")))

(define-public crate-easy_random-0.1.5 (c (n "easy_random") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1q5sgnmw11amdq3jw3bpbv1i44g5bjlpvq5z4ghi92kdahflzqjj")))

(define-public crate-easy_random-0.2.0 (c (n "easy_random") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0mdhwnh9n4s1jhx44rx8gw48zs35if1jcbvcv0a9841jppgh1qxp")))

(define-public crate-easy_random-0.2.1 (c (n "easy_random") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0j3wg8qcbs8l3xgcn1r9jhm8x44nvl0x42bq7mrf94cprx9l1lwr")))

(define-public crate-easy_random-0.2.3 (c (n "easy_random") (v "0.2.3") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1qy54kc71m5awribj8c1yxdvkl35s90ql7hlcpcf2pkndxb2wsgb")))

(define-public crate-easy_random-0.2.4 (c (n "easy_random") (v "0.2.4") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "03wfq3nc64g5fmj3hs1dcznl3vabmkhhid9gmxp6yj8bx5m70ka9")))

(define-public crate-easy_random-0.2.5 (c (n "easy_random") (v "0.2.5") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1qk65jgy8jhvx6fnsxda0s35vrkaixhhlr4sk2n1mgdnr2xc9dr6")))

