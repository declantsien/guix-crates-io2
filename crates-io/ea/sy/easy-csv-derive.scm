(define-module (crates-io ea sy easy-csv-derive) #:use-module (crates-io))

(define-public crate-easy-csv-derive-0.1.0 (c (n "easy-csv-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "1xp59cbn0h4dgxqfjp5dvzlhcd0zl91vp6pdlbml7cpzsk3fpiz9")))

(define-public crate-easy-csv-derive-0.1.1 (c (n "easy-csv-derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "1lvb6zc49ib1zc4fcdyfsmac5qz1sdcc6vzh4rs3zpmpv61mf5d4")))

(define-public crate-easy-csv-derive-0.2.0 (c (n "easy-csv-derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "0jxsfz1xyvq38060jh3a2d13iwdrb1fa0qdxlh5kn9z2nwz2iw4w")))

(define-public crate-easy-csv-derive-0.3.0 (c (n "easy-csv-derive") (v "0.3.0") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "1d2mif8d38ylyjhsjvsph54d5bp3d6wsclnqicgms78070mc49cx")))

(define-public crate-easy-csv-derive-0.3.1 (c (n "easy-csv-derive") (v "0.3.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "054f0nd67g16mg2605mv411l1b8zb5jf31qimhlrc9x81kkhjjr6")))

(define-public crate-easy-csv-derive-0.3.2 (c (n "easy-csv-derive") (v "0.3.2") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "13941halx2nl6nx4a63q3nwqazhcask6zfc9qxmvhlm7yrbv4hwz")))

