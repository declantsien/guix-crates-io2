(define-module (crates-io ea sy easy_aes) #:use-module (crates-io))

(define-public crate-easy_aes-0.1.0 (c (n "easy_aes") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1jpwp4rb7namvcia346vlx2ljsx5yma0xrdll92m2bna5c7hcg7g")))

(define-public crate-easy_aes-0.1.1 (c (n "easy_aes") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "18p9pb52yfsw9pkf6zsb299bcx1rph43kkx5ajg1l6d7p6hayk44")))

(define-public crate-easy_aes-0.1.2 (c (n "easy_aes") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "19dk0fvhq21amnmi6mfrbvka7acs4j6gk7l5hbcyb1jsdj8vin8v")))

(define-public crate-easy_aes-0.1.3 (c (n "easy_aes") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0m3xi5yc2y9695zs3fh28xdvdzwidxyx04jbffln66zgqchlcp5w")))

(define-public crate-easy_aes-0.1.4 (c (n "easy_aes") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "15yqfqhqvylmarw3ilyf4vl57ggvnn1xm9j5b14lnk9cllx8b7n9")))

(define-public crate-easy_aes-0.1.5 (c (n "easy_aes") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0pc2zjg0543rag6pxlz57fn22xw8jmvd8rbc5lf7ann093r4vs1c")))

