(define-module (crates-io ea sy easy_reader) #:use-module (crates-io))

(define-public crate-easy_reader-0.1.0 (c (n "easy_reader") (v "0.1.0") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0dbszr6d82h9mmag5clsrav13mpjxzc49flhcv4y5jispa4nccys")))

(define-public crate-easy_reader-0.1.1 (c (n "easy_reader") (v "0.1.1") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0b4qdscmgyx0ybpz39qz2rmgpxlsb3awljm4jf8dv6ndd17mylg6")))

(define-public crate-easy_reader-0.2.0 (c (n "easy_reader") (v "0.2.0") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0a6dic1pwsnl8fc5xk6ybgl78y5ykqnm8dz5hwrm3sf7bnn6s6s8")))

(define-public crate-easy_reader-0.3.0 (c (n "easy_reader") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "10c8h277c7yrn1zasacv8p93vg0mc5npipvxgi7nlm3997wvbn0q")))

(define-public crate-easy_reader-0.3.1 (c (n "easy_reader") (v "0.3.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0kxmk6wx9bmyv8igr7zdfpi9y10h5az9y6s1nmya06r7p325xacv")))

(define-public crate-easy_reader-0.4.0 (c (n "easy_reader") (v "0.4.0") (d (list (d (n "criterion") (r "~0.2") (d #t) (k 2)) (d (n "fnv") (r "~1.0") (d #t) (k 0)) (d (n "rand") (r "~0.6") (d #t) (k 0)))) (h "0xpjg8f0l3g9qnnh84fl2sfjxr9sca0jiglyq9d2vn0zsj2hzazq")))

(define-public crate-easy_reader-0.5.0 (c (n "easy_reader") (v "0.5.0") (d (list (d (n "criterion") (r "~0.3") (d #t) (k 2)) (d (n "fnv") (r "~1.0") (d #t) (k 0)) (d (n "rand") (r "~0.7") (o #t) (d #t) (k 0)))) (h "1w9midip34cc2jfjjyz8ggnymlzrjvc493nlmlwxcs9hgr3nqlhi") (f (quote (("default" "rand"))))))

(define-public crate-easy_reader-0.5.1 (c (n "easy_reader") (v "0.5.1") (d (list (d (n "criterion") (r "~0.3") (d #t) (k 2)) (d (n "fnv") (r "~1.0") (d #t) (k 0)) (d (n "rand") (r "~0.8") (o #t) (d #t) (k 0)))) (h "095ypiqc5y5iwzbi1vrpn2b7shdfdaiqaz6j7kxz38zdy0ik9q3w") (f (quote (("default" "rand"))))))

(define-public crate-easy_reader-0.5.2 (c (n "easy_reader") (v "0.5.2") (d (list (d (n "criterion") (r "~0.3") (d #t) (k 2)) (d (n "fnv") (r "~1.0") (d #t) (k 0)) (d (n "rand") (r "~0.8") (o #t) (d #t) (k 0)))) (h "1svp72mvldccjd3224q41piphfs2m14hyidq1wrsdhdhyvbycp2g") (f (quote (("default" "rand"))))))

