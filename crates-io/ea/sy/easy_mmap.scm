(define-module (crates-io ea sy easy_mmap) #:use-module (crates-io))

(define-public crate-easy_mmap-0.1.0 (c (n "easy_mmap") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmap") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "06iir8lg5j90yyplrqjnqzalpfc4c8s5ng88jcf8nbcwy20nzc09")))

(define-public crate-easy_mmap-0.1.1 (c (n "easy_mmap") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmap") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1kr16xbsh8rj1j1zkark63b8r6ryrrx1sqdcqcldw4q3vxl7fj2n")))

(define-public crate-easy_mmap-0.2.0 (c (n "easy_mmap") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmap") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "133qjk6ycwfvslb22w7p8dh97rybns700jh5nwqqigdp4wkpxsf1") (y #t)))

(define-public crate-easy_mmap-0.2.1 (c (n "easy_mmap") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmap") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "19p20d22qvi9y0bp4lv0rz7jpjzdnqrwgxfvhag07j699mack04c")))

(define-public crate-easy_mmap-0.2.2 (c (n "easy_mmap") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmap") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1pnmv19hdcyndgk41v3r7pnn9m2637p14w8avk068myy3cr980pp")))

(define-public crate-easy_mmap-0.2.3 (c (n "easy_mmap") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmap") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1v0qfdzp5rrdvc8af1qmr1b8c4wrb4ja4gkbs9xp3n1cjbh110q9")))

(define-public crate-easy_mmap-0.3.0 (c (n "easy_mmap") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmap") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1h4cqcfdbv0r7k7q3r4byibvy16n00jir5bazlfs0wclyfws83mb")))

(define-public crate-easy_mmap-0.3.1 (c (n "easy_mmap") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mmap") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "017d7x4l8bx5s3p3ch2fiiy56l7izf8h18mplkm8d69q11lb74g2")))

