(define-module (crates-io ea sy easy_file_system) #:use-module (crates-io))

(define-public crate-easy_file_system-0.1.0 (c (n "easy_file_system") (v "0.1.0") (h "1p19nm3lnqhzv914w3gd5sh2c16anq8lv7m3paxdz5id17xkr1fv")))

(define-public crate-easy_file_system-0.1.1 (c (n "easy_file_system") (v "0.1.1") (h "0p3lhnr51fpb7d0vdpdy7lsynfz7hnalfkwrj6a2z2l2bc0856fn")))

(define-public crate-easy_file_system-0.1.2 (c (n "easy_file_system") (v "0.1.2") (h "0z3faf5bkzc6c4wijvnlg2fbmckisyslc6prxax6yq7ln5kgndd4")))

