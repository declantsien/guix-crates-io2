(define-module (crates-io ea sy easy_progress) #:use-module (crates-io))

(define-public crate-easy_progress-0.1.0 (c (n "easy_progress") (v "0.1.0") (h "1kvsvif3qzhb0nijzd8d605ijxnl4hzpwc0yrl3jiyvylzr3z66l")))

(define-public crate-easy_progress-0.1.1 (c (n "easy_progress") (v "0.1.1") (h "03ppyj82bchbaxhdix4pfybkkfgk5vfq34kjaqyz1nv6djnmi734")))

