(define-module (crates-io ea sy easy-tun) #:use-module (crates-io))

(define-public crate-easy-tun-1.0.0 (c (n "easy-tun") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "ioctl-gen") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "profiling") (r "^1.0.9") (d #t) (k 0)) (d (n "rtnetlink") (r "^0.13.0") (d #t) (k 0)))) (h "0nphb030mkrgwpp3if1mif9lghx0g43478y76xsv70r89dkzpl7c") (f (quote (("profile-puffin" "profiling/profile-with-puffin") ("default"))))))

(define-public crate-easy-tun-2.0.0 (c (n "easy-tun") (v "2.0.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "ioctl-gen") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "profiling") (r "^1.0.9") (d #t) (k 0)) (d (n "rtnetlink") (r "^0.13.0") (d #t) (k 0)))) (h "0vrwq73mmy8j3m741yfbyj3dz2xfcgwlhwxl6zvnr2hq6bpis6yr") (f (quote (("profile-puffin" "profiling/profile-with-puffin") ("default"))))))

