(define-module (crates-io ea sy easytime) #:use-module (crates-io))

(define-public crate-easytime-0.1.0 (c (n "easytime") (v "0.1.0") (h "1n2b8dzh1b3d87rx34lkpbd1chjqghy38y40b59is9m9br7abimw") (f (quote (("std") ("default" "std"))))))

(define-public crate-easytime-0.1.1 (c (n "easytime") (v "0.1.1") (h "0k0kc1yd7jnznpc8dz3g003jsviwldq62dq3pcnymn8bdzqp37sv") (f (quote (("std") ("default" "std"))))))

(define-public crate-easytime-0.1.2 (c (n "easytime") (v "0.1.2") (h "1nzj8cm0711s7yb2gmmdfy49idp52r59wx1yz01avjks6c9rlh9k") (f (quote (("std") ("default" "std"))))))

(define-public crate-easytime-0.2.0 (c (n "easytime") (v "0.2.0") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "const_fn") (r "^0.4") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)))) (h "07dh4b12ynrg0hs1dq5cc8f9a0c7vvc6x04j9rzfmjsyc2v4fpn4") (f (quote (("std") ("default" "std"))))))

(define-public crate-easytime-0.2.1 (c (n "easytime") (v "0.2.1") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "const_fn") (r "^0.4") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 2)))) (h "1rcxchdfrs203k7880mckxisifjddphs93zcvfs1qap16x73v86z") (f (quote (("std") ("default" "std"))))))

(define-public crate-easytime-0.2.2 (c (n "easytime") (v "0.2.2") (d (list (d (n "autocfg") (r "^1") (d #t) (k 1)) (d (n "const_fn") (r "^0.4") (d #t) (k 0)))) (h "1wrw1vdaj8pmn4wzv24hm5hhnvzjkd8mb4yi9k57zfn6yibcvy2x") (f (quote (("std") ("default" "std"))))))

(define-public crate-easytime-0.2.3 (c (n "easytime") (v "0.2.3") (d (list (d (n "const_fn") (r "^0.4") (d #t) (k 0)))) (h "17snvpg50fhhfrmf1hc4iy0n63akvnpsymk4w6ivs67hjbhqfzk2") (f (quote (("std") ("default" "std")))) (r "1.34")))

(define-public crate-easytime-0.2.4 (c (n "easytime") (v "0.2.4") (d (list (d (n "const_fn") (r "^0.4") (d #t) (k 0)))) (h "1v1y5hmd98nxfkp4zgnnnq49m7y7vlsccw9blabiggvwwf2inna2") (f (quote (("std") ("default" "std")))) (r "1.34")))

(define-public crate-easytime-0.2.5 (c (n "easytime") (v "0.2.5") (h "0v971cln25cww8a7ay0w9vyrszmi6arm11xgydhzkv7ckxbihj6c") (f (quote (("std") ("default" "std")))) (r "1.34")))

(define-public crate-easytime-0.2.6 (c (n "easytime") (v "0.2.6") (h "19cvf03sn8vijfbaais62fqk2s9hygw711isnkg9gn6d69pb1wq7") (f (quote (("std") ("default" "std")))) (r "1.58")))

(define-public crate-easytime-0.2.7 (c (n "easytime") (v "0.2.7") (h "1gbjbmvdvfxxv17s044m4c73fjizhx2algk91h7iq52grynjnrc7") (f (quote (("std") ("default" "std")))) (r "1.58")))

