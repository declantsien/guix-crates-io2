(define-module (crates-io ea sy easynn) #:use-module (crates-io))

(define-public crate-easynn-0.1.0-alpha (c (n "easynn") (v "0.1.0-alpha") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "0kzxckqgr394nkf6xm53q80r91d5x0vkn138i8c4nnpbv5jjvrz8") (y #t)))

(define-public crate-easynn-0.1.1-alpha (c (n "easynn") (v "0.1.1-alpha") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "0b4fr16ajsg9vj11cpmms44i7r35h048qvm940mii5qdg0sf0hs8") (y #t)))

(define-public crate-easynn-0.1.3-alpha (c (n "easynn") (v "0.1.3-alpha") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "087h5j7w0jqc74lvwbn9vx1bziz5z10c6r4mxm0yqra7i4bgxp9a") (y #t)))

(define-public crate-easynn-0.1.4-beta (c (n "easynn") (v "0.1.4-beta") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "15wg1r0x3y99d9cvrm22z6d02dlhp71fc7flqgz61rfvmi8l3xjw")))

(define-public crate-easynn-0.1.5-beta (c (n "easynn") (v "0.1.5-beta") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "0j3c1l9vqp1znkrw6vmkwyr4kxk5rz4ms4kljvr0bzg63xrlna3l")))

(define-public crate-easynn-0.1.6-beta (c (n "easynn") (v "0.1.6-beta") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "0xnbdk2gnchjhhg2shgi6id5ng597al431wcrg059fy5kl541rnf")))

(define-public crate-easynn-0.1.7-beta (c (n "easynn") (v "0.1.7-beta") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "0pjck791ff55r56l78fgp7ljmjv7khay6pdw7bkcjiavz06a1bnq")))

