(define-module (crates-io ea sy easy-jni) #:use-module (crates-io))

(define-public crate-easy-jni-0.3.0 (c (n "easy-jni") (v "0.3.0") (d (list (d (n "jni") (r "^0.21.1") (f (quote ("invocation"))) (d #t) (k 0)))) (h "06vdpfr6k0vkpxnhfqpbq85nps2dfqmivzmjj441a8426vckprjm") (f (quote (("simple_types") ("full" "simple_types" "complex_types" "conversion") ("default" "simple_types") ("conversion" "simple_types") ("complex_types" "simple_types"))))))

