(define-module (crates-io ea sy easytiming) #:use-module (crates-io))

(define-public crate-easytiming-0.0.0 (c (n "easytiming") (v "0.0.0") (h "1sjrn8pnwwgkv436zzgdz2xyf7chi18543yx3ngajiqq3cc446d8")))

(define-public crate-easytiming-0.0.2 (c (n "easytiming") (v "0.0.2") (h "13lcw5l7i9kli5kp9if4axfjizg1yqxyzwvyy41z5qnfkxvzdvc5")))

(define-public crate-easytiming-0.0.4 (c (n "easytiming") (v "0.0.4") (d (list (d (n "log") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "slog") (r "^2.0") (o #t) (d #t) (k 0)))) (h "0z94azh1rrxnj1m5jyifsd40kjfinb3v4cm0g1qran0y0wa0dqx8")))

(define-public crate-easytiming-0.0.5 (c (n "easytiming") (v "0.0.5") (d (list (d (n "log") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "slog") (r "^2.0") (o #t) (d #t) (k 0)))) (h "1hx40ph3amlfma16lb0y2wqm438qzv3jivh5mq9x96gf9r03s6ml")))

