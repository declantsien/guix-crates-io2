(define-module (crates-io ea sy easy-logs) #:use-module (crates-io))

(define-public crate-easy-logs-0.0.1 (c (n "easy-logs") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)))) (h "0xndnfmjdmiwidb4pb8avcskri7fx7l13mbp472i5a8cfaw4hb0v") (y #t)))

(define-public crate-easy-logs-0.0.2 (c (n "easy-logs") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)))) (h "1n7vmnhw7vvly1gccvylqg01q4158620bxqvk5a2wg2gkbar4wg3") (y #t)))

(define-public crate-easy-logs-0.1.0 (c (n "easy-logs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)))) (h "196lsni58g05kcnq8js0rcqb8z5p5r55s504zqksxsc9iqmln7bi") (y #t)))

(define-public crate-easy-logs-0.1.1 (c (n "easy-logs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)))) (h "1y3v9bxx0igvq0v14frhlzivjkxas0vc6gv2nyrmqq03r0kj8b27") (y #t)))

(define-public crate-easy-logs-0.1.2 (c (n "easy-logs") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)))) (h "0x38l2bj5dhigc3xxgfd4x9f82a8jxsd2yzpb8nmn0l20nbikfi8") (y #t)))

(define-public crate-easy-logs-0.1.3 (c (n "easy-logs") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)))) (h "0d43jgjz439c361pzxji6j843a98frml49af7fz6m86xkbqz2vcr") (y #t)))

(define-public crate-easy-logs-0.1.4 (c (n "easy-logs") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)))) (h "18wrz80962d8qcy2fpjfz5l2hln78xvya76wcqwfi6bld57phxkm") (y #t)))

