(define-module (crates-io ea sy easyfix) #:use-module (crates-io))

(define-public crate-easyfix-0.1.0 (c (n "easyfix") (v "0.1.0") (h "1r78979qaqhfccnyx938l2y19dwv51zksn0fj9ij60glsxr40bsp")))

(define-public crate-easyfix-0.1.1 (c (n "easyfix") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.1") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.1.1") (d #t) (k 0)))) (h "146zypakkwia5b76b6qzmvpb275p917iw33igw9clinjfqmm5vjw")))

(define-public crate-easyfix-0.1.2 (c (n "easyfix") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.1") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.1.2") (d #t) (k 0)))) (h "0cyzyi0cy916rwkgy1yvk4fhchd77axqbcim4nm77kra12qc69wn")))

(define-public crate-easyfix-0.1.3 (c (n "easyfix") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.1") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.1.3") (d #t) (k 0)))) (h "0y71n6n7f1bqj98zcjx6qnb71iwrc9004rbwrk9wf68gjs2yv0wr")))

(define-public crate-easyfix-0.1.4 (c (n "easyfix") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.1") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.1.4") (d #t) (k 0)))) (h "1dx47znj5qwwydmh3w79rmbxy4ajvkkcyn4lp4wr5jlrlg7c4667")))

(define-public crate-easyfix-0.1.5 (c (n "easyfix") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.1") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.1.5") (d #t) (k 0)))) (h "0d71s24r1rbf8444fwq71hkkiq120673r33i1d094zz9jy1f2r38")))

(define-public crate-easyfix-0.1.6 (c (n "easyfix") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.1") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.1.6") (d #t) (k 0)))) (h "1a1id9gzaznfp7lasbc2w07mcl02f3rzcazfyn56rr5bhb9hi2r0")))

(define-public crate-easyfix-0.1.7 (c (n "easyfix") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.1") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.1.7") (d #t) (k 0)))) (h "1lqgcg4d0qrk1c96psm3dr7bklmyagdxr8jcw47b0z5agag0da19")))

(define-public crate-easyfix-0.1.8 (c (n "easyfix") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.1") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.1.8") (d #t) (k 0)))) (h "13im43qnc6gzv4wsll9bljpijavj28z9cv2j3p8lg9h6nzkyw9c5")))

(define-public crate-easyfix-0.1.9 (c (n "easyfix") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.1") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.1.9") (d #t) (k 0)))) (h "1gvf6gxx6slbnacwp9005kilzlb4ygiq5p7whwfn3b23g37qc3lz")))

(define-public crate-easyfix-0.1.11 (c (n "easyfix") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.2") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.1.11") (d #t) (k 0)))) (h "0ywgkrahlmv6gbb6fzba7vbspx5i95yb91293y2phx8npqjckkqx")))

(define-public crate-easyfix-0.1.12 (c (n "easyfix") (v "0.1.12") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.2") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.1.11") (d #t) (k 0)))) (h "0iyd89svimjdp47d7vv6gi9xkamzla7nkbp6jjgqcn3h93fnwgmg")))

(define-public crate-easyfix-0.1.13 (c (n "easyfix") (v "0.1.13") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.2") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.1.12") (d #t) (k 0)))) (h "0rjg4qzwjzm2xwyyhvfcrmsvivzk5q33ilxhjx2mrkhnlqw8wyxj")))

(define-public crate-easyfix-0.1.14 (c (n "easyfix") (v "0.1.14") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.2") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.1.13") (d #t) (k 0)))) (h "1d9jk0bvdjjvvpkmfv4fbqkb8x1i3w2a9c1pv3r0n6c6mwghckmy")))

(define-public crate-easyfix-0.1.15 (c (n "easyfix") (v "0.1.15") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.1.2") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.1.14") (d #t) (k 0)))) (h "03jgs1h5l0b2h0h3vy5g154nr84dwrxs3vjha6jlxv746pap4nss")))

(define-public crate-easyfix-0.2.0 (c (n "easyfix") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.2.0") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.2.0") (d #t) (k 0)))) (h "117czqjgiyczqvbyi74i1yli0254b9bz20jhxh4109myjfw353pc")))

(define-public crate-easyfix-0.2.1 (c (n "easyfix") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.2.0") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.2.1") (d #t) (k 0)))) (h "19a1jda18pd83zsk6wighrks8v6wydg3vgvm9dnpsxyibxi5fjvl")))

(define-public crate-easyfix-0.2.2 (c (n "easyfix") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.2.0") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.2.2") (d #t) (k 0)))) (h "1fmsw8s1n4jz7hmj42znw1lxas0i5n74wpq95187shn29jdndk59")))

(define-public crate-easyfix-0.2.3 (c (n "easyfix") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "easyfix-dictionary") (r "^0.2.0") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.2.3") (d #t) (k 0)))) (h "1sk5r0n6ghr85vs6rx617qj5xz5yc9d09pmfaxfkh549vss1vpjm")))

(define-public crate-easyfix-0.3.0 (c (n "easyfix") (v "0.3.0") (d (list (d (n "easyfix-dictionary") (r "^0.3.0") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.3.0") (d #t) (k 0)) (d (n "easyfix-session") (r "^0.1.0") (d #t) (k 0)))) (h "0gmgfsyh4bfdw17zc2604z0pmqzpnd2vv8183klfgw9z9srqr7rv")))

(define-public crate-easyfix-0.3.1 (c (n "easyfix") (v "0.3.1") (d (list (d (n "easyfix-dictionary") (r "^0.3.0") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.3.1") (d #t) (k 0)) (d (n "easyfix-session") (r "^0.1.1") (d #t) (k 0)))) (h "11f8iv67kna2wlrh1ak0fm7yis0fxdykljz6xsgyzd74cl1fsjjp")))

(define-public crate-easyfix-0.3.2 (c (n "easyfix") (v "0.3.2") (d (list (d (n "easyfix-dictionary") (r "^0.3.0") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.3.1") (d #t) (k 0)) (d (n "easyfix-session") (r "^0.1.2") (d #t) (k 0)))) (h "1l3dlli5zl788hq05rh3z5cxx66ijqabpb14127riri7bqlmlp1v")))

(define-public crate-easyfix-0.3.3 (c (n "easyfix") (v "0.3.3") (d (list (d (n "easyfix-dictionary") (r "^0.3.0") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.3.1") (d #t) (k 0)) (d (n "easyfix-session") (r "^0.1.3") (d #t) (k 0)))) (h "1av9ql890zs45g7nd3jj327hhr6p68n3sk33k0499w1sk4sbadf9") (y #t)))

(define-public crate-easyfix-0.3.4 (c (n "easyfix") (v "0.3.4") (d (list (d (n "easyfix-dictionary") (r "^0.3.0") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.3.1") (d #t) (k 0)) (d (n "easyfix-session") (r "^0.1.4") (d #t) (k 0)))) (h "1hjcl5hppqyi7xsfi2bmnwm1wi6jfqvydj8g7ij0yh42qpyxhalj")))

(define-public crate-easyfix-0.4.0 (c (n "easyfix") (v "0.4.0") (d (list (d (n "easyfix-dictionary") (r "^0.3.0") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.3.2") (d #t) (k 0)) (d (n "easyfix-session") (r "^0.2.0") (d #t) (k 0)))) (h "012wn1mxnn2pk136wwyxn5p8i98c8bcwsya18mzjrckb4h5d4mi0")))

(define-public crate-easyfix-0.4.1 (c (n "easyfix") (v "0.4.1") (d (list (d (n "easyfix-dictionary") (r "^0.3.0") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.3.2") (d #t) (k 0)) (d (n "easyfix-session") (r "^0.2.1") (d #t) (k 0)))) (h "09cshybdq49afrddf4gsdl29l4fhyhlg8kxlj69nlz46ibanchb9")))

(define-public crate-easyfix-0.4.2 (c (n "easyfix") (v "0.4.2") (d (list (d (n "easyfix-dictionary") (r "^0.3.0") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.3.2") (d #t) (k 0)) (d (n "easyfix-session") (r "^0.2.2") (d #t) (k 0)))) (h "0g17i0xj472f95rblk3754ff6ady6snlhqabf2141azdxhvzld15")))

(define-public crate-easyfix-0.5.0 (c (n "easyfix") (v "0.5.0") (d (list (d (n "easyfix-dictionary") (r "^0.3.0") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.3.2") (d #t) (k 0)) (d (n "easyfix-session") (r "^0.3.0") (d #t) (k 0)))) (h "126iabz1pmxlwmqal91338yfh5mw7gvxx060bm89vgrp3qmy7qd0")))

(define-public crate-easyfix-0.6.0 (c (n "easyfix") (v "0.6.0") (d (list (d (n "easyfix-dictionary") (r "^0.3.0") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.4.0") (d #t) (k 0)) (d (n "easyfix-session") (r "^0.4.0") (d #t) (k 0)))) (h "0s3plxm0qahf6h246837wmz1ba7f7jk2misw0iyic4vdhiyihf7l")))

(define-public crate-easyfix-0.7.0 (c (n "easyfix") (v "0.7.0") (d (list (d (n "easyfix-dictionary") (r "^0.3.0") (d #t) (k 0)) (d (n "easyfix-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.5.0") (d #t) (k 0)) (d (n "easyfix-session") (r "^0.5.0") (d #t) (k 0)))) (h "0s3s1fg2sk2bibd313b253gwq6xnfn8clhnyx9fk0ccwgdirxc39")))

(define-public crate-easyfix-0.7.1 (c (n "easyfix") (v "0.7.1") (d (list (d (n "easyfix-dictionary") (r "^0.3.0") (d #t) (k 0)) (d (n "easyfix-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.5.1") (d #t) (k 0)) (d (n "easyfix-session") (r "^0.5.1") (d #t) (k 0)))) (h "0g3vw6g9x82kynfan0q04aipn5nkr9spq600a7dsbljxam2bxmb3")))

(define-public crate-easyfix-0.7.2 (c (n "easyfix") (v "0.7.2") (d (list (d (n "easyfix-dictionary") (r "^0.3.0") (d #t) (k 0)) (d (n "easyfix-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.5.1") (d #t) (k 0)) (d (n "easyfix-session") (r "^0.5.1") (d #t) (k 0)))) (h "16xihqbq00a8hkz013hk5qphqirrhgflkyysk6jygygv7b6q7mjr")))

(define-public crate-easyfix-0.8.0 (c (n "easyfix") (v "0.8.0") (d (list (d (n "easyfix-dictionary") (r "^0.3.0") (d #t) (k 0)) (d (n "easyfix-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.5.1") (d #t) (k 0)) (d (n "easyfix-session") (r "^0.6.0") (d #t) (k 0)))) (h "12hqlvhrg8xq9g4imm1hk3pfbpcmlcdi23g28n5rjny7qyvg589f")))

(define-public crate-easyfix-0.8.1 (c (n "easyfix") (v "0.8.1") (d (list (d (n "easyfix-dictionary") (r "^0.3.0") (d #t) (k 0)) (d (n "easyfix-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.5.2") (d #t) (k 0)) (d (n "easyfix-session") (r "^0.6.1") (d #t) (k 0)))) (h "0nv2gj3lrkj98bn3zghp5150h9bqyganzyk4r4p4i912y6d03hxr")))

(define-public crate-easyfix-0.8.2 (c (n "easyfix") (v "0.8.2") (d (list (d (n "easyfix-dictionary") (r "^0.3.0") (d #t) (k 0)) (d (n "easyfix-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.5.2") (d #t) (k 0)) (d (n "easyfix-session") (r "^0.6.2") (d #t) (k 0)))) (h "13214gl32wvrgaq3v525m0mms5ph6jf6p2pjnv9np03yhncr831m")))

(define-public crate-easyfix-0.8.3 (c (n "easyfix") (v "0.8.3") (d (list (d (n "easyfix-dictionary") (r "^0.3.0") (d #t) (k 0)) (d (n "easyfix-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.5.2") (d #t) (k 0)) (d (n "easyfix-session") (r "^0.6.3") (d #t) (k 0)))) (h "03sw3wiiah8z6i6jxyh28jmvg94cd93yklqvs3kapn3j7qcrqq0x")))

(define-public crate-easyfix-0.8.4 (c (n "easyfix") (v "0.8.4") (d (list (d (n "easyfix-dictionary") (r "^0.3.1") (d #t) (k 0)) (d (n "easyfix-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.5.3") (d #t) (k 0)) (d (n "easyfix-session") (r "^0.6.3") (d #t) (k 0)))) (h "1gjba8sw6mh8x9jdfjr6qc525vyl4f2rmkzsnd46lza6mj68lr5r")))

(define-public crate-easyfix-0.8.5 (c (n "easyfix") (v "0.8.5") (d (list (d (n "easyfix-dictionary") (r "^0.3.1") (d #t) (k 0)) (d (n "easyfix-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.5.3") (d #t) (k 0)) (d (n "easyfix-session") (r "^0.6.4") (d #t) (k 0)))) (h "1lf3mkk2y8l1qqsjikrmbmyb5vz5r4lzw7gl6fhkrcj5zbgl9prd")))

(define-public crate-easyfix-0.8.6 (c (n "easyfix") (v "0.8.6") (d (list (d (n "easyfix-dictionary") (r "^0.3.1") (d #t) (k 0)) (d (n "easyfix-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.5.4") (d #t) (k 0)) (d (n "easyfix-session") (r "^0.6.6") (d #t) (k 0)))) (h "15sl51cx2gf7vrjvifcv900m9fqi723w8lb2rmv14a7lfkrh5qfx")))

(define-public crate-easyfix-0.8.7 (c (n "easyfix") (v "0.8.7") (d (list (d (n "easyfix-dictionary") (r "^0.3.1") (d #t) (k 0)) (d (n "easyfix-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "easyfix-messages") (r "^0.5.4") (d #t) (k 0)) (d (n "easyfix-session") (r "^0.6.7") (d #t) (k 0)))) (h "0732s38d2cvr8f732acd8w24sn4sirgks4zrrfzn8x1h97zz85gp")))

