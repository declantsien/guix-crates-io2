(define-module (crates-io ea sy easy-logger) #:use-module (crates-io))

(define-public crate-easy-logger-0.0.1 (c (n "easy-logger") (v "0.0.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "time") (r "^0.3.9") (f (quote ("formatting" "local-offset" "std"))) (d #t) (k 0)))) (h "0yj0rrv3qzpshz2vkff7wdgr792mg4vs0h632zbid3k6ihzyn3j8")))

(define-public crate-easy-logger-0.0.2 (c (n "easy-logger") (v "0.0.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "time") (r "^0.3.9") (f (quote ("formatting" "local-offset" "std"))) (d #t) (k 0)))) (h "02vi12s8ml94r3v3dyfjy7amcykmfzhry9i68ikfipsr5yplh1la")))

(define-public crate-easy-logger-0.0.3 (c (n "easy-logger") (v "0.0.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "time") (r "^0.3.9") (f (quote ("formatting" "local-offset" "std"))) (d #t) (k 0)))) (h "04s0ziwawcb92jpbwg7dmzvjw6mfbpfxfb5s481cs1pakp6hiyyz")))

