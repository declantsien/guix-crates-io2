(define-module (crates-io ea sy easy-jsonrpc-mw) #:use-module (crates-io))

(define-public crate-easy-jsonrpc-mw-0.5.3 (c (n "easy-jsonrpc-mw") (v "0.5.3") (d (list (d (n "easy-jsonrpc-proc-macro-mw") (r "^0.5.1") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^10.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "warp") (r "^0.1.16") (d #t) (k 2)))) (h "0i2q6fv42s6n366mb0b8znpq28wwc67dzwfhs0h7dj52lgisiw66")))

(define-public crate-easy-jsonrpc-mw-0.5.4 (c (n "easy-jsonrpc-mw") (v "0.5.4") (d (list (d (n "easy-jsonrpc-proc-macro-mw") (r "^0.5.1") (d #t) (k 0)) (d (n "jsonrpc-core") (r "^10.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "warp") (r "^0.1.16") (d #t) (k 2)))) (h "1qzl0nxci5lqhnp93pfqm3lccy2dylpb5swz7jx3n3nmd4askcfk")))

