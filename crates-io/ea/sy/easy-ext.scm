(define-module (crates-io ea sy easy-ext) #:use-module (crates-io))

(define-public crate-easy-ext-0.1.0 (c (n "easy-ext") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0h69lc0zf2gf6ln9avbai2mzrm8v10qd3l5nm73zka199is443a9") (y #t)))

(define-public crate-easy-ext-0.1.1 (c (n "easy-ext") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0q66x6ynjdbr33zmmbld0ix0lj77n3xfrif83251k2shfq4rmyhp") (y #t)))

(define-public crate-easy-ext-0.1.2 (c (n "easy-ext") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1x378jf7w5vvhs0d0kac222gwpdy2vb7b8m7ifzssw338giif3im")))

(define-public crate-easy-ext-0.1.3 (c (n "easy-ext") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.19") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hwpb5s4l6vh0q2mjmkvkj96jz3zrvh8ybfvmbriij0vla3ki9id")))

(define-public crate-easy-ext-0.1.4 (c (n "easy-ext") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4.13") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.29") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14kf34pc7q31r8v7ld249y93hvb7mpq44iz1zlpdvvslyn33i6y0")))

(define-public crate-easy-ext-0.1.5 (c (n "easy-ext") (v "0.1.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08n87x9kp5zjykqwiclgzs0395whj11issb735g76ddbphr1ph3i")))

(define-public crate-easy-ext-0.1.6 (c (n "easy-ext") (v "0.1.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0b161fmiarh926xa2f27k09b8raq85zxxpb3ddkslwnca82d8ygd")))

(define-public crate-easy-ext-0.1.7 (c (n "easy-ext") (v "0.1.7") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1gxcvym3l5qclb4m9rn7gy2p99qb9p0zlks1sh5p1q6yrfj42fw4")))

(define-public crate-easy-ext-0.1.8 (c (n "easy-ext") (v "0.1.8") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0myyn55iyig9ba0978rpdvcw405x3n2dpga2i6h73qhywnqzfc82")))

(define-public crate-easy-ext-0.2.0 (c (n "easy-ext") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.3") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1rsjxhs4y1s7vzdk5jckx2kmazpadx0fd4y0l3pwk3r4jqdv3ag2")))

(define-public crate-easy-ext-0.2.1 (c (n "easy-ext") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.3") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1ljwajswam1z9zllcsgid66zjbvcky9s6gj2q2ikab8xf9ncazcx")))

(define-public crate-easy-ext-0.2.2 (c (n "easy-ext") (v "0.2.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.3") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1s3yjqlh3rcwd9lsd8mxldc6fb0mn0v4ppdqbj2wxb9ybm1ypa1k")))

(define-public crate-easy-ext-0.2.3 (c (n "easy-ext") (v "0.2.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.3") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0dx7rwljib2mg8437c4179wc4grgr8vcrqb0s1bs7k8qmdrn7fvk")))

(define-public crate-easy-ext-0.2.4 (c (n "easy-ext") (v "0.2.4") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0.3") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "03vjvs93z5xlr1fykzsyp5lwrz9m2d8x600z394d10fa3c746wbh")))

(define-public crate-easy-ext-0.2.5 (c (n "easy-ext") (v "0.2.5") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0.3") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1rwjky5sbfg1pqn18v40s7lg0ar6cx5x4y0cvddd5qgicwl407i0")))

(define-public crate-easy-ext-0.2.6 (c (n "easy-ext") (v "0.2.6") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1d590qz6z0hw7021mpjb60lricvmy13sy575xq1wcs3br7hiwn40")))

(define-public crate-easy-ext-0.2.7 (c (n "easy-ext") (v "0.2.7") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0.56") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1832jap6hfcdysp05cr6ghz2fyxscs92cdzacjyr0mm1yhikyiv2")))

(define-public crate-easy-ext-0.2.8 (c (n "easy-ext") (v "0.2.8") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0z31kj0xflzdmgxgn67wd3myrbczw1qc01xpj2dk14ldmgpbnz4h") (y #t)))

(define-public crate-easy-ext-0.2.9 (c (n "easy-ext") (v "0.2.9") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0cgfd3nvlf5in4ap6w46wkc6ywh6f7218nybrmd250diq7yzdbsk")))

(define-public crate-easy-ext-1.0.0 (c (n "easy-ext") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1h2n7fzyf7yymw124jzvx557nc5qyanvpmmd2hqyh11rdmfxnbgd")))

(define-public crate-easy-ext-1.0.1 (c (n "easy-ext") (v "1.0.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (d #t) (k 2)))) (h "0ndfsx7k3zygrpqr2w4f0483pixpl2134a4q9iwlhmp6qwj7aia9") (r "1.31")))

