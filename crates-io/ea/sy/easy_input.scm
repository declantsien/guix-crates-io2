(define-module (crates-io ea sy easy_input) #:use-module (crates-io))

(define-public crate-easy_input-0.0.0 (c (n "easy_input") (v "0.0.0") (h "0s0fni4acycdxbbv2kzp1hgvv71d388ghk7d2dzr5ll29gda2cpf")))

(define-public crate-easy_input-0.1.0 (c (n "easy_input") (v "0.1.0") (h "1kvkhradslrpha2cbbw7l5dwa3jyac9r52prlk5yzmn87x8vhfmn")))

(define-public crate-easy_input-0.1.1 (c (n "easy_input") (v "0.1.1") (h "06afvfgl15dgs4h6pcbc5861bpj0qyl4w4zlnf0vgh1szalihgx0")))

(define-public crate-easy_input-0.1.2 (c (n "easy_input") (v "0.1.2") (h "15b3jg359jmqcripccbnzbpfss6siihhxfc7y85wzzwjsf6c2vdl")))

