(define-module (crates-io ea sy easy-opengl) #:use-module (crates-io))

(define-public crate-easy-opengl-0.1.0 (c (n "easy-opengl") (v "0.1.0") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "stb_image") (r "^0.2.4") (d #t) (k 0)))) (h "05fzd7r6wjdcnw5zx44b5p2liz8b1jcka6jdkva1b58c70kn71wh") (y #t)))

(define-public crate-easy-opengl-0.1.1 (c (n "easy-opengl") (v "0.1.1") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "stb_image") (r "^0.2.4") (d #t) (k 0)))) (h "0vpyz8vjbnw540jbyif1p4x1scsry98zga8cpxbammkf4vajjx7b") (y #t)))

(define-public crate-easy-opengl-0.1.2 (c (n "easy-opengl") (v "0.1.2") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "stb_image") (r "^0.2.4") (d #t) (k 0)))) (h "0kl3wiak8qfrsmc3cra5mz44brfyjvw871pnlyipf78p0zvbvb2j")))

(define-public crate-easy-opengl-0.1.3 (c (n "easy-opengl") (v "0.1.3") (d (list (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "stb_image") (r "^0.2.4") (d #t) (k 0)))) (h "0m6ifzpvdcpwjmnbhklrxv0w7aslvn1lfbw571xcg380ixr8dp6q")))

