(define-module (crates-io ea sy easyfix-macros) #:use-module (crates-io))

(define-public crate-easyfix-macros-0.1.0 (c (n "easyfix-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1vvzn1alzklvjbcr0w61307pmnv17msa23dlgs0381v04xil63pa")))

(define-public crate-easyfix-macros-0.1.1 (c (n "easyfix-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1rr11anpf5a74936d7a7q5q9za1n22y1986adlr40sxqg9pgmm3p")))

(define-public crate-easyfix-macros-0.1.2 (c (n "easyfix-macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "123rhhpipdiwwxp46f2pc9yfmql89k8gb6nvfa8mr6djrlw1vnl9")))

