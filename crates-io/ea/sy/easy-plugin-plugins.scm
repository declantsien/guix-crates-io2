(define-module (crates-io ea sy easy-plugin-plugins) #:use-module (crates-io))

(define-public crate-easy-plugin-plugins-0.4.0 (c (n "easy-plugin-plugins") (v "0.4.0") (h "0p09cjv4a81mrd05jfm4h051q3qzh3w1fax29a3bpq54xmsn0d50")))

(define-public crate-easy-plugin-plugins-0.5.0 (c (n "easy-plugin-plugins") (v "0.5.0") (h "0h64cy5rh3v1b6kazk1f84fqagqfi13n32p0l2023maqimfzl1sw")))

(define-public crate-easy-plugin-plugins-0.6.0 (c (n "easy-plugin-plugins") (v "0.6.0") (h "0wl5amxgdlnkyv02pcym0lq92m8x747wwrc69fggkyirqms2nr12")))

(define-public crate-easy-plugin-plugins-0.7.0 (c (n "easy-plugin-plugins") (v "0.7.0") (h "0vn1yc84gk04zdr3fv9drr4ayba2p1p21aqjd02mpygrxk3hxq1y")))

(define-public crate-easy-plugin-plugins-0.7.1 (c (n "easy-plugin-plugins") (v "0.7.1") (h "1ws4b93xg81ff6s21b5wx4jd8iklbpf6b6dgg05czphl78k0xpza")))

(define-public crate-easy-plugin-plugins-0.7.2 (c (n "easy-plugin-plugins") (v "0.7.2") (h "0i4cjh4qq0q2lxv96y17xixwrcplfqianap9x94hmqvh9w2qdsx0")))

(define-public crate-easy-plugin-plugins-0.7.3 (c (n "easy-plugin-plugins") (v "0.7.3") (h "0slf5vbhvjz1f7vr0ndi0npzd1p0w9g9ybrakjhi2x2g4ibzz2sr")))

(define-public crate-easy-plugin-plugins-0.8.0 (c (n "easy-plugin-plugins") (v "0.8.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.39.0") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.39.0") (o #t) (d #t) (k 0)) (d (n "synthax") (r "^0.2.0") (d #t) (k 0)) (d (n "synthax") (r "^0.2.0") (d #t) (k 1)))) (h "016pfzv39bfi79arzqpkmdl88nqkmsgazyv3d0ckv9v3jwg6kh14") (f (quote (("stable" "syntex" "syntex_syntax" "synthax/stable"))))))

(define-public crate-easy-plugin-plugins-0.8.1 (c (n "easy-plugin-plugins") (v "0.8.1") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "0.*") (o #t) (d #t) (k 0)) (d (n "synthax") (r "^0.3.1") (d #t) (k 0)) (d (n "synthax") (r "^0.3.1") (d #t) (k 1)))) (h "1c3bpgyapbcvxjh9xjlclb42hypxmmzrcg2s1ig6wbl48q91p74g") (f (quote (("stable" "syntex" "syntex_syntax" "synthax/stable"))))))

(define-public crate-easy-plugin-plugins-0.9.0 (c (n "easy-plugin-plugins") (v "0.9.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "0.*") (o #t) (d #t) (k 0)) (d (n "synthax") (r "^0.3.1") (d #t) (k 0)) (d (n "synthax") (r "^0.3.1") (d #t) (k 1)))) (h "1xdsljj4akvqys3w0s4zsilh08xz1z57l6d4a3grsdhiyvcif1zb") (f (quote (("stable" "syntex" "syntex_syntax" "synthax/stable"))))))

(define-public crate-easy-plugin-plugins-0.9.1 (c (n "easy-plugin-plugins") (v "0.9.1") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "syntex") (r "0.*") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "0.*") (o #t) (d #t) (k 0)) (d (n "synthax") (r "^0.3.2") (d #t) (k 0)) (d (n "synthax") (r "^0.3.2") (d #t) (k 1)))) (h "1fc38gwkiqndl85bbi6r2h5a73pab49za9j3rlmj9xnjg9ngprp6") (f (quote (("stable" "syntex" "syntex_syntax" "synthax/stable"))))))

