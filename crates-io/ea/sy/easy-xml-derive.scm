(define-module (crates-io ea sy easy-xml-derive) #:use-module (crates-io))

(define-public crate-easy-xml-derive-0.1.0 (c (n "easy-xml-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "1ly90ip0l2l9anj09m0vbx9v93wi6s4ybjqm32dib1f2drlr1qbh")))

(define-public crate-easy-xml-derive-0.1.1 (c (n "easy-xml-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "0r6ajp7p6pbwibfkwy60llhi1p15wynpyhmm434sw6941wa1y3sj")))

(define-public crate-easy-xml-derive-0.1.2-beta (c (n "easy-xml-derive") (v "0.1.2-beta") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "0z1mkjala6vz00jaba8cclvbyshmvzrzjr4ksrgnf73mn8lrhpbf")))

(define-public crate-easy-xml-derive-0.1.2-beta.1 (c (n "easy-xml-derive") (v "0.1.2-beta.1") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "1giafgy8jd0ylwd83fbhb7ddy96r4qj1fhyysc0arhh7fhpsvar9")))

(define-public crate-easy-xml-derive-0.1.2-beta.2 (c (n "easy-xml-derive") (v "0.1.2-beta.2") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "1p28bdqw70zx2m843gkywmjywv03qprn893b5s0krdyw0pdw6cfv")))

(define-public crate-easy-xml-derive-0.1.2 (c (n "easy-xml-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "1ayl175n2frhzy80bh27gz6w4i7y4zyrvz55d2gv16ssc8lnb674")))

(define-public crate-easy-xml-derive-0.1.3 (c (n "easy-xml-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "1r3m3sqmh303b0735vz87was8w6jskkzqxydx9zrxd59ylkpfpjf")))

(define-public crate-easy-xml-derive-0.1.4 (c (n "easy-xml-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (d #t) (k 0)))) (h "1xq5mmskcbwij3sax26mzbb9r54qsanqyfq0sqny8h6dkzmdfy26")))

