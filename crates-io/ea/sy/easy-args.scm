(define-module (crates-io ea sy easy-args) #:use-module (crates-io))

(define-public crate-easy-args-0.1.0 (c (n "easy-args") (v "0.1.0") (h "1r8rrzd19488dyygfh5rswd2zapa7n8py5441wwnc3j0r422c4xn")))

(define-public crate-easy-args-0.1.1 (c (n "easy-args") (v "0.1.1") (h "111q3fk7x07wf20ski5a73h8w9cfv47qdy28jwhrlvg73dh34p6n")))

(define-public crate-easy-args-0.2.0 (c (n "easy-args") (v "0.2.0") (h "1mjqvh5hbws1y0lx3skm8sbwpnblhks38cbnjydlpqszfs9w5wji")))

(define-public crate-easy-args-0.3.0 (c (n "easy-args") (v "0.3.0") (h "1f13xd50jyqxbv271n30qswad6039436xa1ppki85xkr98y9a7sq")))

