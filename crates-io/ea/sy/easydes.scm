(define-module (crates-io ea sy easydes) #:use-module (crates-io))

(define-public crate-easydes-0.1.0 (c (n "easydes") (v "0.1.0") (d (list (d (n "clap") (r "^3.1") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)))) (h "0dr8avclrbqxjfa5iiikk9g4542knfczxq9j5m99kpa8i5c01gfj") (f (quote (("default" "clap")))) (s 2) (e (quote (("clap" "dep:clap"))))))

(define-public crate-easydes-0.1.1 (c (n "easydes") (v "0.1.1") (d (list (d (n "clap") (r "^3.1") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)))) (h "18ai00cav6fxx1cgva6kpldn9bcf37mqlx3vshzzi0846lrzck6n") (f (quote (("default" "clap")))) (s 2) (e (quote (("clap" "dep:clap"))))))

(define-public crate-easydes-0.1.2 (c (n "easydes") (v "0.1.2") (d (list (d (n "clap") (r "^3.1") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)))) (h "1sgx741jmirckjfh8q3nh550z8rgsfk2cxifw6v51xdxfavy6pfs") (f (quote (("default" "clap")))) (s 2) (e (quote (("clap" "dep:clap"))))))

(define-public crate-easydes-0.1.3 (c (n "easydes") (v "0.1.3") (d (list (d (n "clap") (r "^3.1") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)))) (h "1w3ylml0d1290ijk5q46kwyw8kw45glj3s5drdj3vy0jjbgmgma5") (f (quote (("default" "clap")))) (s 2) (e (quote (("clap" "dep:clap"))))))

(define-public crate-easydes-0.1.4 (c (n "easydes") (v "0.1.4") (d (list (d (n "clap") (r "^3.1") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)))) (h "0knn0j1d6qba0iwhiqs2y1cp9pfi336g0m850jz5dn9mkl8cwsg5") (f (quote (("default" "clap")))) (s 2) (e (quote (("clap" "dep:clap"))))))

(define-public crate-easydes-0.1.5 (c (n "easydes") (v "0.1.5") (d (list (d (n "clap") (r "^3.1") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)))) (h "1c4jrnvsl69ih08cmpgvirv3dic5d79xkc5062qmczc7yh1rjflh") (f (quote (("default" "clap")))) (s 2) (e (quote (("clap" "dep:clap"))))))

(define-public crate-easydes-0.1.6 (c (n "easydes") (v "0.1.6") (d (list (d (n "clap") (r "^3.1") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)))) (h "0kw4x7xijg7k4drgigi6d3nqmkwa03qg2glwhcqg59mjv4q1spsn") (f (quote (("default" "clap")))) (s 2) (e (quote (("clap" "dep:clap"))))))

