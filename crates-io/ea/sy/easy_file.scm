(define-module (crates-io ea sy easy_file) #:use-module (crates-io))

(define-public crate-easy_file-0.1.0 (c (n "easy_file") (v "0.1.0") (h "1xykx754yq16b2kkvphs4ang3m2v5l1w72y4azvrpsgk0xcq2z6v")))

(define-public crate-easy_file-0.1.1 (c (n "easy_file") (v "0.1.1") (h "17mk6q1sbbhjs4k3y1cppdcjsb4b0k56cqcd1yvg9qmisfsglxmq")))

(define-public crate-easy_file-0.1.2 (c (n "easy_file") (v "0.1.2") (h "1vcnmc72m55ggp7rwpmqk8b02201vb1xha0p4bi9sg4alibz7a81")))

(define-public crate-easy_file-0.1.3 (c (n "easy_file") (v "0.1.3") (h "105apnqw3hcg1666mbbni985d7mh5ldl8jgm05px0yp067dcrgm9")))

(define-public crate-easy_file-0.1.4 (c (n "easy_file") (v "0.1.4") (h "12q94nqs21c5v3z6id1vk1vc6ayns6gkjb5lk4bwz0zmrq254gj3")))

(define-public crate-easy_file-0.1.5 (c (n "easy_file") (v "0.1.5") (h "0wvzcixqzawdnkmgmcdsw5qpiyvssg5dzb9pfm1cqi5m08dzghvw")))

(define-public crate-easy_file-0.1.6 (c (n "easy_file") (v "0.1.6") (h "0z9j1mijbahdpb5z7vhid9mnz764r697jbp4khb04jw7bd0qrykm")))

(define-public crate-easy_file-0.1.7 (c (n "easy_file") (v "0.1.7") (h "08frhsv9cn5wl9j9l11nh4ij7xvqpawhpwp60f5gwfvkmf98y18m")))

(define-public crate-easy_file-0.1.8 (c (n "easy_file") (v "0.1.8") (h "1arz607ayyax1hzp9dnpw6y4hnlffh0mmnlhax9x4hfqpj2fg5vl")))

(define-public crate-easy_file-0.1.9 (c (n "easy_file") (v "0.1.9") (h "1g4jmwbi93hah6d0pdkfdz2d74vfp0km2xdk9jwjmmc1w8mdd57g")))

(define-public crate-easy_file-0.1.10 (c (n "easy_file") (v "0.1.10") (h "1wp18i0fb3k8yjn3fj1dn1xjm9ysbiw03pq5svfxgqmnh17assqg")))

(define-public crate-easy_file-0.1.11 (c (n "easy_file") (v "0.1.11") (h "06ck5290zyyaw7ww5am2nprmkj7g1sc0j7j75vhmaw1pldnmgzng")))

(define-public crate-easy_file-0.1.12 (c (n "easy_file") (v "0.1.12") (h "0kggbibgd29zqb57flnhlyqnkmmm3fgg93sqd2qg3bqsf90xxags")))

(define-public crate-easy_file-0.1.13 (c (n "easy_file") (v "0.1.13") (h "1716q00mkzzhgrpyblya3b9kapwz44f04k11w9a7i5ibkr4bfasa")))

(define-public crate-easy_file-0.1.14 (c (n "easy_file") (v "0.1.14") (h "1a2ajdlkbazdhdh5sci0y3lpzkgjyyq7dqfpq4qz1l3ykc381apq")))

(define-public crate-easy_file-0.1.15 (c (n "easy_file") (v "0.1.15") (h "1mkrlrk6wgg863g3flx6vdp7fkz2r006sfpbxfplbr4axjgl1sx3")))

(define-public crate-easy_file-0.1.16 (c (n "easy_file") (v "0.1.16") (h "1ck1i95cxm8r6cwd56vn0cz3z2a40mpyxnhajgzs8mp62nhdgmrn")))

(define-public crate-easy_file-0.1.17 (c (n "easy_file") (v "0.1.17") (h "09pxpwc04p58kdlwcwxdi9qapq62qp47d06ahm78rvg2ghws0zph")))

(define-public crate-easy_file-0.1.18 (c (n "easy_file") (v "0.1.18") (h "1q90ghspzhvh1iwy925qvvviapd8hfiv3h86wf80cxf8ks7ggamj")))

(define-public crate-easy_file-0.1.19 (c (n "easy_file") (v "0.1.19") (h "0bnvk8wq3nmia9cxhr7pmansfkaw4iqphqqs9ls6lnrdzl3043vq")))

(define-public crate-easy_file-0.1.20 (c (n "easy_file") (v "0.1.20") (h "1rfvv3nma43spmgwvq528v7qb9vb5gfqv65ynfmj0vkjfksyffs3")))

