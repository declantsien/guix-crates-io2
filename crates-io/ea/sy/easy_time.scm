(define-module (crates-io ea sy easy_time) #:use-module (crates-io))

(define-public crate-easy_time-0.1.0 (c (n "easy_time") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "0zxf2x2bfcp3ql0rl192kwk112hqzsliji04kx2rjqb1ak3pqfmw")))

(define-public crate-easy_time-0.1.1 (c (n "easy_time") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "1rwr7r70v9a1hg0ssffyialyv5hpp2acsmljmhv0552f8mdajzsv")))

(define-public crate-easy_time-0.1.2 (c (n "easy_time") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "0389wy11hgg2q2i9f2ngsvmfqg3k7hm116y8dn15zsrqqvn8k5zx")))

(define-public crate-easy_time-0.1.3 (c (n "easy_time") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "0fz0ai53rmzwc4cz7sff9glnlrjzfb9j43j9262ifc0vbkf2rzwg")))

(define-public crate-easy_time-0.1.4 (c (n "easy_time") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 0)))) (h "0h05k9b4nizhj20rn9bixwabz953x4iavgdxf61ia5qrh4vh51j4")))

