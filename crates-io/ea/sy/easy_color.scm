(define-module (crates-io ea sy easy_color) #:use-module (crates-io))

(define-public crate-easy_color-0.1.0 (c (n "easy_color") (v "0.1.0") (h "052hf58w6msz4yjmgbsgs1lbjndkxalwplkxyhsn1szlzwldhpfc")))

(define-public crate-easy_color-0.1.1 (c (n "easy_color") (v "0.1.1") (h "1gfd375fxrmfm5fi4s1drgrjxh7cv476w9cz0k2bzxwiy8ci95sa")))

(define-public crate-easy_color-0.1.2 (c (n "easy_color") (v "0.1.2") (h "1ir8arc2l8ma0cn7xkxypv94jrkv8gd4i52v8xlmz2r4f3zj7z87")))

(define-public crate-easy_color-0.1.3 (c (n "easy_color") (v "0.1.3") (h "1z8f3mgg4ndyfjqwz3gi6dkmqccvlhvd9b5cjh1s369g65s5yl8d")))

(define-public crate-easy_color-0.1.4 (c (n "easy_color") (v "0.1.4") (h "1jxnr2dzbjf5k0xk2py0qm1gkwimxz4wpp1fh0kxrrr8wvaiv8jm")))

(define-public crate-easy_color-0.1.5 (c (n "easy_color") (v "0.1.5") (h "0qi8cgj1pdg6z7xhry65lr0m9bl6jp0iw3pw56srhcs8lz2hs92k")))

(define-public crate-easy_color-0.1.6 (c (n "easy_color") (v "0.1.6") (h "0wykj3qy850kxjq3770hzndyqryhis3q1jyj5l9vj9r13vgaj6xa")))

(define-public crate-easy_color-0.1.7 (c (n "easy_color") (v "0.1.7") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "059pw1vrildc1lr12jzvx48018i9vg9gwgfz41541i5jafi1d9x2")))

(define-public crate-easy_color-0.1.8 (c (n "easy_color") (v "0.1.8") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1y5lpy0yblynigg5sn5vsic28734g8m8134ggfhxffanfcpgj1a9")))

(define-public crate-easy_color-0.1.9 (c (n "easy_color") (v "0.1.9") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0gvk6ykff426nn7dw654vnl1w3wv3gyljsci5v6i7q4g1fzvz8hg")))

(define-public crate-easy_color-0.1.10 (c (n "easy_color") (v "0.1.10") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0v3dcgvccx0myn7x1nv1pwkays3n4m8nv0r65b93wnqhkvl1jkpg")))

(define-public crate-easy_color-0.1.11 (c (n "easy_color") (v "0.1.11") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0li2xdfkbwq6dndrdim47jajx3ydpp53fa70j7ky4kkr6pylvsrg")))

(define-public crate-easy_color-0.1.12 (c (n "easy_color") (v "0.1.12") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "14p0mk3r2v21860fk8yv33bddl8wyzndx7q3s1l9xm4f84lg5qvj")))

(define-public crate-easy_color-0.1.13 (c (n "easy_color") (v "0.1.13") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1l587k07blbw8b7bvfqs1lrw78mapc5m6v1bb70dfywr61hc2bha")))

