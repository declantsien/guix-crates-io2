(define-module (crates-io ea sy easycrypto) #:use-module (crates-io))

(define-public crate-EasyCrypto-0.1.0 (c (n "EasyCrypto") (v "0.1.0") (h "01vymwdnsgb7c5ndl11a9mncn6jwq9xzbg57wqvdq5ixb1hk7g6a")))

(define-public crate-EasyCrypto-0.1.1 (c (n "EasyCrypto") (v "0.1.1") (h "0w1nna8xv69rxipwivx0939zl02ampbwsykfjjp3yamylrj5vnb9")))

(define-public crate-EasyCrypto-0.1.3 (c (n "EasyCrypto") (v "0.1.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "1c74n6jdcq5dyrxa1834r17d0ky650xnpp08763yrbvjpc0dg1ij")))

(define-public crate-EasyCrypto-0.2.1 (c (n "EasyCrypto") (v "0.2.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "18xmqi4fbx2knc20wv1fgimy1975r4v23xvcjgnpyzc725m86rxb")))

(define-public crate-EasyCrypto-0.4.1 (c (n "EasyCrypto") (v "0.4.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "0p1sxbzlf680zkz50wmvlcbd7684igbnccipvca1a178jx99w2n7")))

(define-public crate-EasyCrypto-0.5.0 (c (n "EasyCrypto") (v "0.5.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "0i1dl5r64grfl8q7b5jk310k732rbxzf0a38j281drpfpr3iidzh")))

(define-public crate-EasyCrypto-0.6.0 (c (n "EasyCrypto") (v "0.6.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "1sap0q9mrr0yhbswj651r9vajbbpwqiqskn801d9i56iapsshkl0")))

(define-public crate-EasyCrypto-0.6.1 (c (n "EasyCrypto") (v "0.6.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "12iqgnbmxch7ax0jh9pxgj32layh5sr8d8y47byiq0lsaz9zim8c")))

(define-public crate-EasyCrypto-0.7.0 (c (n "EasyCrypto") (v "0.7.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "0rvajlj7c7vabc3lg4v5ihshq5r6j4ji8b0n3ijhhzxvb1lanr28")))

(define-public crate-EasyCrypto-0.8.0 (c (n "EasyCrypto") (v "0.8.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "07jan1bchfc3b6ghflw3f96wlm415bcd7n63j65zy3p2hd8izarg")))

(define-public crate-EasyCrypto-0.8.1 (c (n "EasyCrypto") (v "0.8.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)))) (h "03dfrm09kq2i1fm5ph4qq378b4vi5k16p9mqic6xbj5rbhzvvq2s")))

