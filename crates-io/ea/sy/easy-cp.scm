(define-module (crates-io ea sy easy-cp) #:use-module (crates-io))

(define-public crate-easy-cp-0.1.0 (c (n "easy-cp") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n7jsclkgnsx81xfdp7hvni2sfjbdh8vdrlvcvaipcq6zqqxgk7s")))

(define-public crate-easy-cp-0.1.1 (c (n "easy-cp") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y9yg24p9cpbhbq08blhp7vwj06yb04ifaal044f11mlz700l2qr")))

(define-public crate-easy-cp-0.1.2 (c (n "easy-cp") (v "0.1.2") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z10xs9abhwph34jdw4yckc0kmgjkhcrii7qkm34693d8z13x3v9")))

(define-public crate-easy-cp-0.2.1 (c (n "easy-cp") (v "0.2.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "103lqvqq6cdwkkjxb2zk9p7k90imf3lkhq25d16rahzirq17n88n")))

(define-public crate-easy-cp-0.2.2 (c (n "easy-cp") (v "0.2.2") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ihdgczpv75mpdlckwlqbk7rmp1lr6s17wx7lac3ykck906x6avq")))

(define-public crate-easy-cp-0.2.3 (c (n "easy-cp") (v "0.2.3") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pa14lnx74slrw3d1znbqky2qrzrc13sw2ldg1di0h204kgk9vs9")))

