(define-module (crates-io ea sy easy-pin-proc-macro) #:use-module (crates-io))

(define-public crate-easy-pin-proc-macro-0.0.1-alpha (c (n "easy-pin-proc-macro") (v "0.0.1-alpha") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kdc5kwxaa71dgij6nq5sb92dwsxkf2pzw88vqv9f6z0mklbdgn5") (f (quote (("extra-traits" "syn/extra-traits") ("default"))))))

