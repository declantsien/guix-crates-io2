(define-module (crates-io ea sy easy-pool) #:use-module (crates-io))

(define-public crate-easy-pool-0.1.0 (c (n "easy-pool") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)))) (h "0d8rg58hwz49c0mkpmr231jf943i4qwyhy7whq3gxjf8syfw78xg")))

(define-public crate-easy-pool-0.1.1 (c (n "easy-pool") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)))) (h "0lcjr54a6p9c3b681004zkq9zvg9w9spi40sgxhwk6412h1c685k")))

(define-public crate-easy-pool-0.1.2 (c (n "easy-pool") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "1dz46ihckwhypsy28l468f3w6v8hzq5yqz4yw6cps1z7hd7zp3sj")))

(define-public crate-easy-pool-0.1.3 (c (n "easy-pool") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "1dchdlf20g56nq23qj0irz4d747qdaiaifi618frw5y0w9k5xkg6")))

(define-public crate-easy-pool-0.2.0 (c (n "easy-pool") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "easy_pool_proc_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "0cvrr7dk3rs6bv3sn8awq7xagh7lhrf2bjiq2bgjdl5s3xq551ib")))

(define-public crate-easy-pool-0.2.1 (c (n "easy-pool") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "easy_pool_proc_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "0dljjw1kn1axq5p7smd9bx936lj6wsbhz56r5z4mzzvi1lhw1zrc")))

(define-public crate-easy-pool-0.2.2 (c (n "easy-pool") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "easy_pool_proc_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "1xswy1jnab46wfdqn4r67b2cj9p930nyhbgszn6m1p1sgbmfk15v")))

(define-public crate-easy-pool-0.2.3 (c (n "easy-pool") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "easy_pool_proc_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "1sq6aphgmpjky1ncz306k83g454yn3wiilz6x24mbhfd5d1plcbi")))

(define-public crate-easy-pool-0.2.4 (c (n "easy-pool") (v "0.2.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "easy_pool_proc_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "1h6ygqzc48sppavwpxy4gvri2aic30hlfckd463abnc94sri2k17")))

(define-public crate-easy-pool-0.2.5 (c (n "easy-pool") (v "0.2.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "easy_pool_proc_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "1bf7s9js301waz5sjk5fakb0ln1laqffrmx2wm4sr191dc6vpp4r")))

(define-public crate-easy-pool-0.2.6 (c (n "easy-pool") (v "0.2.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "easy_pool_proc_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "1h7l22mg0b93q7s54vjwpcq7v5id58xy8x6bsn05mn74yvg39c4s")))

(define-public crate-easy-pool-0.2.7 (c (n "easy-pool") (v "0.2.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "easy_pool_proc_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "0lba3pwnlh5ymmi18w3wrhn8aaapl5xp7psc47jdnfcgz4bzzrgp")))

