(define-module (crates-io ea se easer) #:use-module (crates-io))

(define-public crate-easer-0.1.0 (c (n "easer") (v "0.1.0") (d (list (d (n "approx") (r "^0.1.0") (d #t) (k 0)))) (h "1rnalxj3vssnx4miwrklbnipjdm6394nmm62h16k3mvamkiqz3n8")))

(define-public crate-easer-0.1.1 (c (n "easer") (v "0.1.1") (d (list (d (n "approx") (r "^0.1.0") (d #t) (k 2)) (d (n "gnuplot") (r "^0.0.20") (d #t) (k 2)))) (h "1ilngmfa0zqzn18wvqzsv4554qz7w6d0jp55qqqq9ncqy39av8nx")))

(define-public crate-easer-0.2.0 (c (n "easer") (v "0.2.0") (d (list (d (n "approx") (r "^0.1") (d #t) (k 2)) (d (n "gnuplot") (r "^0.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "09rhjrh4l3fiz8c0008d1dwn02gxccnr5z9b4wmsgxpjzq7a2kb0")))

(define-public crate-easer-0.2.1 (c (n "easer") (v "0.2.1") (d (list (d (n "approx") (r "^0.2") (d #t) (k 2)) (d (n "gnuplot") (r "^0.0.25") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "06hr0sjavhsnvk092dqdc9m6hq85jv7pmjjk9m9dcxmpxh64dpld")))

(define-public crate-easer-0.3.0 (c (n "easer") (v "0.3.0") (d (list (d (n "approx") (r "^0.2") (d #t) (k 2)) (d (n "gnuplot") (r "^0.0.25") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "05zrb310izfdd10xb9l90jc827wxvqkidfy20bg5p71wp3w299gv") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

