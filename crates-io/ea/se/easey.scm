(define-module (crates-io ea se easey) #:use-module (crates-io))

(define-public crate-easey-1.0.0 (c (n "easey") (v "1.0.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "07fqjllfy46y1j8fcyps7mhx2gpfvy3q3xs1vyklxbj5xg7wqg1c")))

(define-public crate-easey-1.1.0 (c (n "easey") (v "1.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "0gq4dj4syp2n5pdabyg9428a8cd0wj1vhb59s2vr46g9gfmsiskg")))

(define-public crate-easey-1.1.1 (c (n "easey") (v "1.1.1") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "18id6n9vj04ryg2vlhpmc63cqkd7a77h87z96lwzwnzynxpcm1v4")))

(define-public crate-easey-1.1.2 (c (n "easey") (v "1.1.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "1jyq0ggc2jjs70z19l29an835z04vviqi879cjmyd0i7qm64w923")))

(define-public crate-easey-1.2.0 (c (n "easey") (v "1.2.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "1727x7rrsjj1qrglab155c1vhx645xk7akmlhjdybh2r8zzm3gpj")))

(define-public crate-easey-2.0.0 (c (n "easey") (v "2.0.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "0qb8jl5sagz3i9fa0xfz2amzdlz6kz92v9d7h784pgx9h6m38ryh")))

(define-public crate-easey-2.1.0 (c (n "easey") (v "2.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)))) (h "0w6lrc1h7c4vr45p3gp6fmwxsfl0wycb8ki1ipcslz6rizg44nyh")))

