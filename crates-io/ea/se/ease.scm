(define-module (crates-io ea se ease) #:use-module (crates-io))

(define-public crate-ease-0.1.0 (c (n "ease") (v "0.1.0") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1c4cs0yfzcp95ilpjsbmlkdyynk269q8n9pkk27wsypiqnv5rkcg")))

(define-public crate-ease-0.2.0 (c (n "ease") (v "0.2.0") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0q4kxbx1aaqiavsp0yyp0a5ajkqh36lxgnysyjalx6k6cfbprp0r")))

(define-public crate-ease-0.2.1 (c (n "ease") (v "0.2.1") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1ssi2hz9brzf6jarvfiaw29kx7n7vnz7n4jj0im88wryylbwwckm")))

(define-public crate-ease-0.3.0 (c (n "ease") (v "0.3.0") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0g01dcmbg3inxirhwc95jhw24zrxx0j31rxmp9vl1xrywlkyfmsg")))

(define-public crate-ease-0.4.1 (c (n "ease") (v "0.4.1") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_json") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "*") (o #t) (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "0i48c3cfraxfvwdh2syk2mag72fma7f872kyg2nsyrc208x2ngfv") (f (quote (("nightly" "serde_macros"))))))

(define-public crate-ease-0.4.2 (c (n "ease") (v "0.4.2") (d (list (d (n "hyper") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^0.6.11") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.6.11") (o #t) (d #t) (k 0)) (d (n "url") (r "^0.5.3") (d #t) (k 0)))) (h "0wygcjarygjqpv6l8gyvld29hqqgf956r23njvw2341h3pwx5b4d") (f (quote (("nightly" "serde_macros"))))))

(define-public crate-ease-0.5.0 (c (n "ease") (v "0.5.0") (d (list (d (n "hyper") (r "^0.9.4") (d #t) (k 0)) (d (n "serde") (r "^0.7.10") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.1") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.10") (d #t) (k 2)) (d (n "url") (r "^1.1.1") (d #t) (k 0)))) (h "1chrzjxhr6r9bbvy4arc8bf2r4j6sjkwsasvnqlgjjdddw5xq365")))

(define-public crate-ease-0.6.0 (c (n "ease") (v "0.6.0") (d (list (d (n "hyper") (r "^0.9.4") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (d #t) (k 2)) (d (n "url") (r "^1.1.1") (d #t) (k 0)))) (h "1ssgbgd2ilwr3wv8yq1j991lgm8p8dh169kh3j41sc3866pjph3l")))

(define-public crate-ease-0.6.1 (c (n "ease") (v "0.6.1") (d (list (d (n "hyper") (r "^0.9.4") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (d #t) (k 2)) (d (n "url") (r "^1.1.1") (d #t) (k 0)))) (h "1wkc6jzq8sr4qyz4brgz1m0603n44jdyi6hlbpn0zkslrlsnzx7k")))

