(define-module (crates-io ea se easegress-macros) #:use-module (crates-io))

(define-public crate-easegress-macros-0.1.0 (c (n "easegress-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1hmqhshvy8vahz1wmk57i5wdgj753ifn6pc1nzpv47z7sbzx28kg")))

(define-public crate-easegress-macros-0.1.1 (c (n "easegress-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0km2y54p42fmkykj0mrkm65y5jg5vmd7swalyn333syillh4ppqr")))

