(define-module (crates-io ea r2 ear2ctl) #:use-module (crates-io))

(define-public crate-ear2ctl-0.1.0 (c (n "ear2ctl") (v "0.1.0") (d (list (d (n "bluer") (r "^0.16.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (d #t) (k 0)))) (h "15q158j5r5yglkdjnf75a38hz8q64b1km6lcfsk8fqsq8knf3kgp")))

