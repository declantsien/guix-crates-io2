(define-module (crates-io ea tn eatnp) #:use-module (crates-io))

(define-public crate-eatnp-0.1.4 (c (n "eatnp") (v "0.1.4") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)))) (h "0pw3yh8rm2z92smvpb2jrldcvyw15jfj11is8kxzjbsjgljag20g")))

(define-public crate-eatnp-0.1.5 (c (n "eatnp") (v "0.1.5") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)))) (h "1b7d0j0szgvbi5yy9qqfmfy0igcps7ff97wz6b990043nxv7rp7r")))

