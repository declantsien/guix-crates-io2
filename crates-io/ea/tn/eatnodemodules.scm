(define-module (crates-io ea tn eatnodemodules) #:use-module (crates-io))

(define-public crate-eatnodemodules-0.1.0 (c (n "eatnodemodules") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)))) (h "161vhwkvqgjkgm6iqcbagza0zfc75ja6lbgj4p5bgiyxq2rmwfbb")))

(define-public crate-eatnodemodules-0.1.1 (c (n "eatnodemodules") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)))) (h "07n091c4kxh0zc8kq43flgr2civ5i86d7kfw2ccpbydyr4cvxhss")))

(define-public crate-eatnodemodules-0.1.2 (c (n "eatnodemodules") (v "0.1.2") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)))) (h "0gjbrifr61x34ddm1szzsqyy3kyb8dppjms621kj3mbd36gsdrcl")))

(define-public crate-eatnodemodules-0.1.3 (c (n "eatnodemodules") (v "0.1.3") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)))) (h "0sf0hp791wkjrjkzy98k2kd63xbvqd5bx23wwg5carkl296476ys")))

(define-public crate-eatnodemodules-0.1.4 (c (n "eatnodemodules") (v "0.1.4") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)))) (h "1idla2w20vw0m633dch69jli4wrnq1dk041dr2qwgzjs01p5mppp")))

