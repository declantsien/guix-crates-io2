(define-module (crates-io ea te eater_rangeset) #:use-module (crates-io))

(define-public crate-eater_rangeset-0.1.0 (c (n "eater_rangeset") (v "0.1.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1") (f (quote ("const_new"))) (o #t) (d #t) (k 0)))) (h "1jig89r28gxsvpqih9qwszwsfwx50cv2x87mac2zy5m50kldfayv") (f (quote (("default" "smallvec")))) (s 2) (e (quote (("smallvec" "dep:smallvec") ("serde" "dep:serde" "smallvec/serde"))))))

(define-public crate-eater_rangeset-0.1.1 (c (n "eater_rangeset") (v "0.1.1") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1") (f (quote ("const_new"))) (o #t) (d #t) (k 0)))) (h "18129lprlv33mfas90xyjkapry2llsns1m7bgjyrydn4dc3w2cr9") (f (quote (("default" "smallvec")))) (s 2) (e (quote (("smallvec" "dep:smallvec") ("serde" "dep:serde" "smallvec/serde"))))))

