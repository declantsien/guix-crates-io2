(define-module (crates-io ea te eater_domainmatcher) #:use-module (crates-io))

(define-public crate-eater_domainmatcher-0.1.0 (c (n "eater_domainmatcher") (v "0.1.0") (d (list (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)))) (h "0x29lq655baf4s6ndb5r3pg4kxnn68rhindgnn2drz1mrzjprv3x") (s 2) (e (quote (("smallvec" "dep:smallvec"))))))

(define-public crate-eater_domainmatcher-0.1.1 (c (n "eater_domainmatcher") (v "0.1.1") (d (list (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)))) (h "1cilkq5qvkk91bnk5dyxkh02lzjirwsjz7w5j8jdydsdq60d41i2") (s 2) (e (quote (("smallvec" "dep:smallvec"))))))

(define-public crate-eater_domainmatcher-0.1.2 (c (n "eater_domainmatcher") (v "0.1.2") (d (list (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)))) (h "0ngcbmwhbfbvrrdb8rgzv41zhmz13xfpjmn8495nb795xx9afsl6") (s 2) (e (quote (("smallvec" "dep:smallvec"))))))

