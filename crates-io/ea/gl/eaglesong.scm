(define-module (crates-io ea gl eaglesong) #:use-module (crates-io))

(define-public crate-eaglesong-0.1.0 (c (n "eaglesong") (v "0.1.0") (d (list (d (n "blake2b-rs") (r "^0.1.4") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "095q35a1aymc0nfgzzqncv4zs0n6z4zxkj0gbjdsps23sgaqp5wd")))

