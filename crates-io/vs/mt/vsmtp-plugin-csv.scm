(define-module (crates-io vs mt vsmtp-plugin-csv) #:use-module (crates-io))

(define-public crate-vsmtp-plugin-csv-1.4.0-rc.10 (c (n "vsmtp-plugin-csv") (v "1.4.0-rc.10") (d (list (d (n "anyhow") (r "^1.0.65") (f (quote ("std"))) (k 0)) (d (n "csv") (r "^1.1.6") (k 0)) (d (n "rhai") (r "^1.11.0") (f (quote ("unchecked" "sync" "internals" "no_closure" "metadata"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("std" "derive"))) (k 0)) (d (n "vsmtp-plugin-vsl") (r "=1.4.0-rc.10") (d #t) (k 0)))) (h "1qpypyyr7jpd3p8k6wf1bfqlj40h0pb2hsrj1jrhvshfy31b7m7a") (r "1.63.0")))

