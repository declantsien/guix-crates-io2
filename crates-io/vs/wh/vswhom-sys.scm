(define-module (crates-io vs wh vswhom-sys) #:use-module (crates-io))

(define-public crate-vswhom-sys-0.1.0 (c (n "vswhom-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(target_os = \"windows\")") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0clm4dx4amwlhg5lkh52fmvvwq6c7s7b9xqljw39mryhsc158bzw")))

(define-public crate-vswhom-sys-0.1.1 (c (n "vswhom-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(target_os = \"windows\")") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fbwynm58168cag0kd1cgvimnj8y1fvk6sga43wyn0xrirnmy0i2")))

(define-public crate-vswhom-sys-0.1.2 (c (n "vswhom-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(target_os = \"windows\")") (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "065s8kx9sbrgjpmzxgiczyhb90xzxq9d95nd0s2v58n8yvhpmcfk")))

