(define-module (crates-io vs qx vsqx) #:use-module (crates-io))

(define-public crate-vsqx-0.1.0 (c (n "vsqx") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "quick-xml") (r "^0.18.1") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)) (d (n "zip") (r "^0.5.6") (f (quote ("deflate"))) (k 0)))) (h "0m5p7l41rdadc5wnr8wyjpryrl669r4f1pc4f5lqpm8wvgcsfsnn")))

