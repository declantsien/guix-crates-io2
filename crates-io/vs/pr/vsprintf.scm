(define-module (crates-io vs pr vsprintf) #:use-module (crates-io))

(define-public crate-vsprintf-1.0.0 (c (n "vsprintf") (v "1.0.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0r9nicq1aikiq4kbhpjwvaax0j3xjicfanh51n0cbrgsf38b6kbq")))

(define-public crate-vsprintf-1.0.1 (c (n "vsprintf") (v "1.0.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1yn0nh8wd128cv1z7i37hfdc4zmziwrdjg2az3dvflshdfip4fby")))

(define-public crate-vsprintf-1.0.3 (c (n "vsprintf") (v "1.0.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ai7g87zjf4vylb0jakr1nn7c7zn4mqs0gzkyv8vxc57gp7k73bg")))

(define-public crate-vsprintf-2.0.0 (c (n "vsprintf") (v "2.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qsx4qq955l9inf186rb3kl5l78xly6pwkvbfya341nafldzihmf")))

