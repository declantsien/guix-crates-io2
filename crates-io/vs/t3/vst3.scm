(define-module (crates-io vs t3 vst3) #:use-module (crates-io))

(define-public crate-vst3-0.1.0 (c (n "vst3") (v "0.1.0") (d (list (d (n "com-scrape-types") (r "^0.1.0") (d #t) (k 0)) (d (n "vst3-bindgen") (r "^0.2.0") (d #t) (k 1)))) (h "15n0f016sbn2m0il5j92pp7927cbjvs9fsclpc56ahbnxycygj5j")))

(define-public crate-vst3-0.1.1 (c (n "vst3") (v "0.1.1") (d (list (d (n "com-scrape-types") (r "^0.1.0") (d #t) (k 0)) (d (n "vst3-bindgen") (r "^0.2.1") (d #t) (k 1)))) (h "1jd8jh54sxr5wi70acngr2m9ds6cvsdck0034xalxs85kwsxxix5")))

(define-public crate-vst3-0.1.2 (c (n "vst3") (v "0.1.2") (d (list (d (n "com-scrape-types") (r "^0.1.1") (d #t) (k 0)) (d (n "vst3-bindgen") (r "^0.2.2") (d #t) (k 1)))) (h "1rq1hjcx6zrla921159r402xxhbsmgawrmm0gp1ikxshcf9n71sz")))

