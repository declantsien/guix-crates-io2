(define-module (crates-io vs t3 vst3-bindgen) #:use-module (crates-io))

(define-public crate-vst3-bindgen-0.1.0 (c (n "vst3-bindgen") (v "0.1.0") (d (list (d (n "com-scrape") (r "^0.1.0") (d #t) (k 1)) (d (n "com-scrape-types") (r "^0.1.0") (d #t) (k 0)))) (h "1h1jp01hvdzarxngj5kc4118w5n6fq9qxdzfr0hr9rhi0sq3l7gl")))

(define-public crate-vst3-bindgen-0.1.1 (c (n "vst3-bindgen") (v "0.1.1") (d (list (d (n "com-scrape") (r "^0.1.1") (d #t) (k 1)) (d (n "com-scrape-types") (r "^0.1.0") (d #t) (k 0)))) (h "1c3qrm67n8dr0y7dllqldaslkxb63fglfszbqv5n34206jpnqlja")))

(define-public crate-vst3-bindgen-0.2.0 (c (n "vst3-bindgen") (v "0.2.0") (d (list (d (n "com-scrape") (r "^0.1.1") (d #t) (k 0)))) (h "0wm4mw2miqxg3ficah3cil09x92kw88f8g9ci15qxwwiwqjcxq4p")))

(define-public crate-vst3-bindgen-0.2.1 (c (n "vst3-bindgen") (v "0.2.1") (d (list (d (n "com-scrape") (r "^0.1.2") (d #t) (k 0)))) (h "1cbpikk6nnyn1cb7yiidjaza935bfv0c000w0cpd7nvqqdyffipg")))

(define-public crate-vst3-bindgen-0.2.2 (c (n "vst3-bindgen") (v "0.2.2") (d (list (d (n "com-scrape") (r "^0.1.3") (d #t) (k 0)))) (h "1rpf56k1gwh25x0gaxp3aad6lxpafhgppsnmvhfhlb8b9clc3hip")))

