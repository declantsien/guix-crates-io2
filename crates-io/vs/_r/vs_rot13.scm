(define-module (crates-io vs _r vs_rot13) #:use-module (crates-io))

(define-public crate-vs_rot13-0.1.0 (c (n "vs_rot13") (v "0.1.0") (h "0r2frqsr240xchmwv0x2bjhld1z7rqz5llwl5cbbpbnhlhmhzvfk")))

(define-public crate-vs_rot13-0.1.1 (c (n "vs_rot13") (v "0.1.1") (h "1q0z4cw3q6v1kyviplqyv9ip3g5qiqzja45kc2sv4igksz70d6by")))

