(define-module (crates-io vs t2 vst2-sys) #:use-module (crates-io))

(define-public crate-vst2-sys-0.1.0 (c (n "vst2-sys") (v "0.1.0") (h "12yyf4zmk33kn26iir76ybkarn799w7idahs9jpn4143rs13fwpd")))

(define-public crate-vst2-sys-0.1.1 (c (n "vst2-sys") (v "0.1.1") (h "0z8cdp8k8iz8l0kxl3158dpcf4lhi2wspy6ligfhfivsc6pmzank")))

(define-public crate-vst2-sys-0.2.0 (c (n "vst2-sys") (v "0.2.0") (h "1nnss4jdg9n0pibd3knarwyv5fw58a4g31czsbnzyvvi6mwgs7zh")))

