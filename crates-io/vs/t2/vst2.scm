(define-module (crates-io vs t2 vst2) #:use-module (crates-io))

(define-public crate-vst2-0.0.1 (c (n "vst2") (v "0.0.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "12k21px6kyqa9hf2146w94ji6l6qsnj7i1jgs4yk2449nl7zvdp6")))

