(define-module (crates-io vs co vscode-sys) #:use-module (crates-io))

(define-public crate-vscode-sys-0.1.0 (c (n "vscode-sys") (v "0.1.0") (d (list (d (n "electron-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.19") (d #t) (k 0)) (d (n "node-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.42") (f (quote ("serde-serialize" "nightly"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.19") (d #t) (k 0)))) (h "07hrvs1k7fr7p8vz3n1vcdqa6sc37x9096y1jjpl13g05pzjsip1")))

