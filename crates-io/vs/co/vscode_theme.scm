(define-module (crates-io vs co vscode_theme) #:use-module (crates-io))

(define-public crate-vscode_theme-0.1.0 (c (n "vscode_theme") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zfqy43295cmixbccb7rhdwhfkvg5y39k967fblnhc6g0pfjw867")))

(define-public crate-vscode_theme-0.2.0 (c (n "vscode_theme") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lkrh7qsjbl900wircdi23lmf4c9q7pwk6rm5jxfdwj434hncdlb")))

