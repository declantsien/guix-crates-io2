(define-module (crates-io vs or vsort) #:use-module (crates-io))

(define-public crate-vsort-0.1.0 (c (n "vsort") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)))) (h "05nxznxypg4yyg3799xp0a6q9s6rbc43rn7lfhsab6dm1zksibw4")))

(define-public crate-vsort-0.2.0 (c (n "vsort") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)))) (h "1y7b0x18a2mpn7hk3qfybl2ksy08klwl3bps4yxsd7pr90dilcqi")))

