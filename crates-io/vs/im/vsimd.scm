(define-module (crates-io vs im vsimd) #:use-module (crates-io))

(define-public crate-vsimd-0.0.1 (c (n "vsimd") (v "0.0.1") (h "0xrd4azbp3i0abw8zvj7iq1j6hzvfa734rr46mf4a1km1fqf3r9c") (f (quote (("unstable") ("std" "alloc") ("detect" "std") ("alloc")))) (y #t) (r "1.63")))

(define-public crate-vsimd-0.8.0 (c (n "vsimd") (v "0.8.0") (d (list (d (n "const-str") (r "^0.5.3") (d #t) (k 2)) (d (n "getrandom") (r "^0.2.8") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3.33") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0r4wn54jxb12r0x023r5yxcrqk785akmbddqkcafz9fm03584c2w") (f (quote (("unstable") ("std" "alloc") ("detect" "std") ("alloc")))) (r "1.63")))

