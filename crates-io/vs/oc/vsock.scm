(define-module (crates-io vs oc vsock) #:use-module (crates-io))

(define-public crate-vsock-0.1.0 (c (n "vsock") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "nix") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "sha2") (r ">= 0.8.0") (d #t) (k 2)))) (h "0ws23h5m6g87g1p7xyxh5b8dr1mih4a68di8kr39b5zcasyvr95l")))

(define-public crate-vsock-0.1.1 (c (n "vsock") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "nix") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "sha2") (r ">= 0.8.0") (d #t) (k 2)))) (h "1kjkzkls23bqr5vzw3izj9hm9c35s2fdaxkh8js3afb436zi47ga")))

(define-public crate-vsock-0.1.2 (c (n "vsock") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "nix") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "sha2") (r ">= 0.8.0") (d #t) (k 2)))) (h "1ah9nbjgw014zzff41znw0nnibkmqbn428wkh9x4qnz8m5vz37b3")))

(define-public crate-vsock-0.1.4 (c (n "vsock") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "nix") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "sha2") (r ">= 0.8.0") (d #t) (k 2)))) (h "0qncpjfw2lkp6zqrqa4j6bnfy2d0lpjiaa6y954xa7l4i4x5mpg4")))

(define-public crate-vsock-0.1.5 (c (n "vsock") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "nix") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "sha2") (r ">= 0.8.0") (d #t) (k 2)))) (h "1207gzbdzhjksv1z8pph3yzxyd1rvd53x9mim01jc9gfmd6kbd33")))

(define-public crate-vsock-0.2.0 (c (n "vsock") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "sha2") (r ">=0.8.0") (d #t) (k 2)))) (h "0l45srbdz4dmzlwmc87dghjgybyi62sfncp4fxw1wzgx2m9s8fx0")))

(define-public crate-vsock-0.2.1 (c (n "vsock") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "sha2") (r ">=0.8.0") (d #t) (k 2)))) (h "14c4nhwr1zgb6zx7xpiw1j6ijpjfx9wkhrbwdhhz5h27f0sn786v")))

(define-public crate-vsock-0.2.2 (c (n "vsock") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.79") (d #t) (k 0)) (d (n "nix") (r "^0.19.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "sha2") (r ">=0.8.0") (d #t) (k 2)))) (h "0k4h8mmb8nnvfb7g15bg93dz1m92byq1xi8k4hwjqy554jg9z8q7")))

(define-public crate-vsock-0.2.3 (c (n "vsock") (v "0.2.3") (d (list (d (n "libc") (r "^0.2.79") (d #t) (k 0)) (d (n "nix") (r "^0.19.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "sha2") (r ">=0.8.0") (d #t) (k 2)))) (h "1ili00jgfcqaqds9vga06hhdwrlfwp014ram4hyj07afhc4yzqjh")))

(define-public crate-vsock-0.2.4 (c (n "vsock") (v "0.2.4") (d (list (d (n "libc") (r "^0.2.79") (d #t) (k 0)) (d (n "nix") (r "^0.19.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "sha2") (r ">=0.8.0") (d #t) (k 2)))) (h "03hmx6wq88p6n92hvx0x3mz3hicg3gglfnmynbvz7s302mlvwcn9")))

(define-public crate-vsock-0.2.5 (c (n "vsock") (v "0.2.5") (d (list (d (n "libc") (r "^0.2.79") (d #t) (k 0)) (d (n "nix") (r "^0.22.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "sha2") (r ">=0.8.0") (d #t) (k 2)))) (h "16bwd143vhjn0fr47i505g3hv1pj3qbcm69jvsxyfyfa6aix0351")))

(define-public crate-vsock-0.2.6 (c (n "vsock") (v "0.2.6") (d (list (d (n "libc") (r "^0.2.79") (d #t) (k 0)) (d (n "nix") (r "^0.23.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "sha2") (r ">=0.8.0") (d #t) (k 2)))) (h "0cy1gpiahygfzxms4z170qj672c7n8cjvd8a9hkxzr9w5gp7a9p3")))

(define-public crate-vsock-0.3.0 (c (n "vsock") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "nix") (r "^0.24.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "sha2") (r ">=0.8.0") (d #t) (k 2)))) (h "0mwwcjk2yd3fi5ryv5xhd7569bmcj0dlsmi4bh4jh6qypzq1v3jc")))

(define-public crate-vsock-0.4.0 (c (n "vsock") (v "0.4.0") (d (list (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("ioctl" "socket"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "sha2") (r ">=0.8.0") (d #t) (k 2)))) (h "0d9g87dvwm6zzq8n6rrnj57fmlfyr4km4rd7ykqi42c3fix6xyrd")))

(define-public crate-vsock-0.5.0 (c (n "vsock") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (f (quote ("ioctl" "socket"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "sha2") (r ">=0.8.0") (d #t) (k 2)))) (h "0bg3sjz1sazp3p0c2lw8r0w2aqpb41l1p1nd0zb7fn2k9482jbpq")))

