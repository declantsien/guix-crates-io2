(define-module (crates-io vs oc vsock-proxy) #:use-module (crates-io))

(define-public crate-vsock-proxy-0.1.0 (c (n "vsock-proxy") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net" "rt" "io-util"))) (d #t) (k 0)) (d (n "tokio-vsock") (r "^0.3.2") (d #t) (k 0)))) (h "0wdd9p8bmgl4zjls1w0hxcb9cvgjy9w689674ri8sskr28n57cng") (y #t)))

(define-public crate-vsock-proxy-0.1.1 (c (n "vsock-proxy") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net" "rt" "io-util"))) (d #t) (k 0)) (d (n "tokio-vsock") (r "^0.3.2") (d #t) (k 0)))) (h "1br02fzk4xi7s0hbjbvcm167pfbip0y0610aacsp4l9yqyh2mhpn") (y #t)))

(define-public crate-vsock-proxy-0.1.2 (c (n "vsock-proxy") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("net" "rt" "io-util"))) (d #t) (k 0)) (d (n "tokio-vsock") (r "^0.3.2") (d #t) (k 0)))) (h "1czdgv1g3cws37vhllnjaqbnxnv66yml4h3ixpq4dgx0rnd06qjs")))

