(define-module (crates-io vs d- vsd-mp4) #:use-module (crates-io))

(define-public crate-vsd-mp4-0.1.0 (c (n "vsd-mp4") (v "0.1.0") (d (list (d (n "base64") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "prost") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (o #t) (d #t) (k 1)) (d (n "quick-xml") (r "^0.28") (f (quote ("serialize"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "13kmsh4hy272rf7zs58pzkc8ckxxa9m599df4kvl9s2wbbhbbh7p") (f (quote (("text-vtt")))) (s 2) (e (quote (("text-ttml" "dep:serde" "dep:quick-xml") ("pssh" "dep:base64" "dep:hex" "dep:prost" "dep:prost-build" "dep:serde"))))))

