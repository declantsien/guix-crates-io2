(define-module (crates-io vs db vsdb_derive) #:use-module (crates-io))

(define-public crate-vsdb_derive-0.1.0 (c (n "vsdb_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0i2cdnk6zigai2h89xx9nmqv65fpqxnp7k9mzih7bjkahkhc91j8") (y #t)))

(define-public crate-vsdb_derive-0.1.1 (c (n "vsdb_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1g5aacm3jcwa2j2hrhcnj6si5x1s4pwvya4b3avwm9d6ff4ldgv9") (y #t)))

(define-public crate-vsdb_derive-0.1.2 (c (n "vsdb_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1yksmq0mf8kvmrcnrji0lkmnq6fcbd6vlchsn34mr4x4qnqld0f9") (y #t)))

(define-public crate-vsdb_derive-0.2.0 (c (n "vsdb_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bx2p1xr2bfrifn9brdb1pqrqd4d1vr0amf43a18n45sajnmaypb") (y #t)))

(define-public crate-vsdb_derive-0.2.1 (c (n "vsdb_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "149qfr6vqhm1fcbswlirm6l2y7v72axxwiha4b7jqrsyf2nakhcl") (y #t)))

(define-public crate-vsdb_derive-0.2.2 (c (n "vsdb_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "192w4n83wh8rzsl2s0ar76qy59728aadpd03gv96kszcc594y80d") (y #t)))

(define-public crate-vsdb_derive-0.2.3 (c (n "vsdb_derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1lgnibbwfx9fin2qx63wigj5bf5h46wavnqs4v6h7xhvd3ry0sv3")))

(define-public crate-vsdb_derive-0.21.1 (c (n "vsdb_derive") (v "0.21.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0z81iz595v03xi02g87aw0xbfpyxx9qgfyq5j3ivnr266p1np307")))

(define-public crate-vsdb_derive-0.27.0 (c (n "vsdb_derive") (v "0.27.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rmg3mn30c5jfnl9misy239vj0q560bi2m7irvwp6dims3vc2y7j")))

(define-public crate-vsdb_derive-0.27.1 (c (n "vsdb_derive") (v "0.27.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16n70s9gjqdahcq0dr48rdj6m4nkrfafjlb80pjmyzjlp81j6px4")))

(define-public crate-vsdb_derive-0.28.0 (c (n "vsdb_derive") (v "0.28.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1c2b5rzjaka6xfbr3c7c4xgis5416fvjbrqy57njxk8rpsr5d8vh") (y #t)))

(define-public crate-vsdb_derive-0.28.1 (c (n "vsdb_derive") (v "0.28.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0syjv7xdmc7bss7jyc8pz2mqhf7yx1pnhydi0y23chq1lgmli67m")))

(define-public crate-vsdb_derive-0.30.0 (c (n "vsdb_derive") (v "0.30.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0maw5i3zc52gsxw1xq5fxvfkr7w880ms1r579avd4b3i7xq6qn1j") (y #t)))

(define-public crate-vsdb_derive-0.30.1 (c (n "vsdb_derive") (v "0.30.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rd56mkwmkkzhi33q53nqfxvjpp6wc0qhsl4mzv4k8a5asykzipj")))

(define-public crate-vsdb_derive-0.34.0 (c (n "vsdb_derive") (v "0.34.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1yl80wndb7jc4m633zwmnfdgq76w82xgjl329qklcd1hcmaylp6v")))

(define-public crate-vsdb_derive-0.35.3 (c (n "vsdb_derive") (v "0.35.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0sm1dclfdadha9f34lqqncavy20zy3xwaa931axh2jrqd1imr8cf")))

(define-public crate-vsdb_derive-0.37.1 (c (n "vsdb_derive") (v "0.37.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1di8lwjb7pa1rxpjdfrhhr9c151qb37w57grw57d8zzmdh12lrrk")))

(define-public crate-vsdb_derive-0.40.0 (c (n "vsdb_derive") (v "0.40.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "15nd5cdsnh7mwj8mzszj99fbaawxz1s5cfydjr8x6anpp4apq15s")))

(define-public crate-vsdb_derive-0.42.0 (c (n "vsdb_derive") (v "0.42.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04nlvdv6vzjzjdg666g3gyfp4csih1y1g810w2cif775qb24ngl2")))

(define-public crate-vsdb_derive-0.44.0 (c (n "vsdb_derive") (v "0.44.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jam3ylapd5f5d9633brccyl9pjn03zpsy588jlj93vjjbzfpyq7")))

(define-public crate-vsdb_derive-0.45.0 (c (n "vsdb_derive") (v "0.45.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17470ll4qvaywifyva8fi6fc5y7h1aa18n0dzrmly4hkklg3vl8w")))

(define-public crate-vsdb_derive-0.53.0 (c (n "vsdb_derive") (v "0.53.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1mcvzj77dxcr8nppnpilrg8irz6jpll0hzjzck8vi0xypw2qssxh")))

