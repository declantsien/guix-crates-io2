(define-module (crates-io vs ss vsss-rust) #:use-module (crates-io))

(define-public crate-vsss-rust-0.1.0 (c (n "vsss-rust") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-prime") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "1fi9a1sk11dzcbpdykcchf37rhrcrvzv6ywfi3wq11nn0p2mhaw7")))

