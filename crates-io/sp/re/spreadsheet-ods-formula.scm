(define-module (crates-io sp re spreadsheet-ods-formula) #:use-module (crates-io))

(define-public crate-spreadsheet-ods-formula-0.19.0 (c (n "spreadsheet-ods-formula") (v "0.19.0") (d (list (d (n "kparse") (r "^3.0") (d #t) (k 2)) (d (n "nom") (r "^7.1") (f (quote ("alloc"))) (d #t) (k 2)) (d (n "nom_locate") (r "^4.2") (d #t) (k 2)) (d (n "spreadsheet-ods") (r "^0.19.0") (f (quote ("use_decimal" "all_locales"))) (d #t) (k 0)))) (h "1rpybpwdgbkzy79s5dany7p3qyvk9fp4kspbwrcqfi28zfkdwwlk")))

(define-public crate-spreadsheet-ods-formula-0.19.1 (c (n "spreadsheet-ods-formula") (v "0.19.1") (d (list (d (n "kparse") (r "^3.0") (d #t) (k 2)) (d (n "nom") (r "^7.1") (f (quote ("alloc"))) (d #t) (k 2)) (d (n "nom_locate") (r "^4.2") (d #t) (k 2)) (d (n "spreadsheet-ods") (r "^0.19.0") (f (quote ("use_decimal" "all_locales"))) (d #t) (k 0)))) (h "005hhdi5i22w2v7z2zl90n0xm1dbk6bxg1dgqz2i79ahc5k4xzsx")))

(define-public crate-spreadsheet-ods-formula-0.19.2 (c (n "spreadsheet-ods-formula") (v "0.19.2") (d (list (d (n "kparse") (r "^3.0") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "nom_locate") (r "^4.2") (d #t) (k 0)) (d (n "spreadsheet-ods") (r "^0.19") (f (quote ("use_decimal" "all_locales"))) (d #t) (k 0)))) (h "077jbg7viyfsx21lh7f5pr12kdqgqamgnqvh21qq4bs22pfvik0l")))

(define-public crate-spreadsheet-ods-formula-0.20.0 (c (n "spreadsheet-ods-formula") (v "0.20.0") (d (list (d (n "kparse") (r "^3.0") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "nom_locate") (r "^4.2") (d #t) (k 0)) (d (n "spreadsheet-ods") (r "^0.20") (f (quote ("use_decimal" "all_locales"))) (d #t) (k 0)))) (h "13h41l8ryyz2j8prvbng7bxc96h74p3jmyknv71ilndapdg9pf53")))

(define-public crate-spreadsheet-ods-formula-0.20.1 (c (n "spreadsheet-ods-formula") (v "0.20.1") (d (list (d (n "kparse") (r "^3.0") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "nom_locate") (r "^4.2") (d #t) (k 0)) (d (n "spreadsheet-ods") (r "^0.20") (f (quote ("use_decimal" "all_locales"))) (d #t) (k 0)))) (h "0fpia6j7kcagql55wnqadynglp7igbcbr05x90f9m3hh65mff6pb")))

(define-public crate-spreadsheet-ods-formula-0.21.0 (c (n "spreadsheet-ods-formula") (v "0.21.0") (d (list (d (n "kparse") (r "^3.0") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "nom_locate") (r "^4.2") (d #t) (k 0)) (d (n "spreadsheet-ods") (r "^0.21") (f (quote ("use_decimal" "all_locales"))) (d #t) (k 0)))) (h "1qqr397hzacy63grgxzl8ym4c0ssa2gvbd46m5arb1d8x1kpgi5q")))

(define-public crate-spreadsheet-ods-formula-0.22.0 (c (n "spreadsheet-ods-formula") (v "0.22.0") (d (list (d (n "kparse") (r "^3.0") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "nom_locate") (r "^4.2") (d #t) (k 0)) (d (n "spreadsheet-ods") (r "^0.22") (f (quote ("use_decimal" "all_locales"))) (d #t) (k 0)))) (h "19n09z3gia9szmdy1m9lz5n43kxqdm7v6cpiv1l7l7j30769yd7n")))

(define-public crate-spreadsheet-ods-formula-0.22.1 (c (n "spreadsheet-ods-formula") (v "0.22.1") (d (list (d (n "kparse") (r "^3.0") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "nom_locate") (r "^4.2") (d #t) (k 0)) (d (n "spreadsheet-ods") (r "^0.22.1") (f (quote ("use_decimal" "all_locales"))) (d #t) (k 0)))) (h "0cxy8q0j77hc6nldni54lwm9737s0bz4vqy5vqcabiy5zvaa0jkh")))

