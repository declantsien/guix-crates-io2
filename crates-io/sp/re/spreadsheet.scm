(define-module (crates-io sp re spreadsheet) #:use-module (crates-io))

(define-public crate-spreadsheet-0.0.0 (c (n "spreadsheet") (v "0.0.0") (h "1jsm9kq3hrqd9bm2z2irzrs6svr3h4s96r9rq80bj9ca9gqxfxyb")))

(define-public crate-spreadsheet-0.0.1 (c (n "spreadsheet") (v "0.0.1") (h "02srnlkypswwdgfbycgysfkv2rxba7s0qb4rmzwjil33ib2jd34l")))

(define-public crate-spreadsheet-0.0.2 (c (n "spreadsheet") (v "0.0.2") (h "04p88ixc8z1r34f9c0dw9a67w2w5f11536vifwknn5vqf9gxc1iz")))

