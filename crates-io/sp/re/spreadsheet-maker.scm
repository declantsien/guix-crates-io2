(define-module (crates-io sp re spreadsheet-maker) #:use-module (crates-io))

(define-public crate-spreadsheet-maker-0.1.0 (c (n "spreadsheet-maker") (v "0.1.0") (d (list (d (n "image-builder") (r "^0.3.3") (d #t) (k 0)))) (h "1sfq4cmhyi6r2myavcsf4xl56xdawylkgr079qq0akq7bw6l8h1x")))

(define-public crate-spreadsheet-maker-0.2.0 (c (n "spreadsheet-maker") (v "0.2.0") (d (list (d (n "image-builder") (r "^0.3.4") (d #t) (k 0)))) (h "1d16crjfy9xmyfsx0d2wg9y7ipr8nlxcc6gwf3bhp8ck6mqiqp46")))

