(define-module (crates-io sp re spreading_colors_ca) #:use-module (crates-io))

(define-public crate-spreading_colors_ca-0.2.0 (c (n "spreading_colors_ca") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "inquire") (r "^0.6.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1y383jg0hn9vrbghibva4i95dn05yxi2nyfjyiaya7mjhh9gi2fw")))

