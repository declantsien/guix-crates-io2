(define-module (crates-io sp re spreadsheet_textconv) #:use-module (crates-io))

(define-public crate-spreadsheet_textconv-0.1.0 (c (n "spreadsheet_textconv") (v "0.1.0") (d (list (d (n "calamine") (r "^0.11.8") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.9") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "0ljpfnyzirxj41rii3hmva25abr6wg1c9qc669ph7jmsj92ic3ci")))

