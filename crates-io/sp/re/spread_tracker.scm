(define-module (crates-io sp re spread_tracker) #:use-module (crates-io))

(define-public crate-spread_tracker-0.1.0 (c (n "spread_tracker") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.34") (d #t) (k 0)) (d (n "supabase_rs") (r "^0.2.2") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (d #t) (k 0)))) (h "0pj8zkvw2xskkjgkd7gq3jhprh1r7hhi2bqicfi0pkdb7bj1ghdi")))

