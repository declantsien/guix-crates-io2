(define-module (crates-io sp re spreadsheet_addresses) #:use-module (crates-io))

(define-public crate-spreadsheet_addresses-1.0.0 (c (n "spreadsheet_addresses") (v "1.0.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0fqcjsgai6svvdfyr4m6ch2c8hi8vk04si3g06i6yrpn42pxvvqb")))

(define-public crate-spreadsheet_addresses-1.0.1 (c (n "spreadsheet_addresses") (v "1.0.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "10gghi4120lda6hclg829n3agdmhwfdl0k2w1x5qbhzmifvl2mms")))

(define-public crate-spreadsheet_addresses-1.0.2 (c (n "spreadsheet_addresses") (v "1.0.2") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "12jayc6rzadzin15fgkn7w6ficgnf5xidwyvbw5pbjymyjjkhm3g")))

(define-public crate-spreadsheet_addresses-1.0.3 (c (n "spreadsheet_addresses") (v "1.0.3") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "1jpfyz4a44yxm0ym2jy18r23gzxb5qxnaw87f9hfbi7xhdz1mv3j")))

