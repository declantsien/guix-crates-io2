(define-module (crates-io sp re spread_macros) #:use-module (crates-io))

(define-public crate-spread_macros-0.1.0 (c (n "spread_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (d #t) (k 0)))) (h "14c2mvxi88g3hpknrpkqv0x7dmqga0s81nq9r2xb7z6nmqpmafzz") (f (quote (("serde_derive"))))))

(define-public crate-spread_macros-0.1.1 (c (n "spread_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (d #t) (k 0)))) (h "0zx0i0dffpbcgaw7j2fvbcqbpmxy39xzdgb5kv7fzhr6r2b2pigi") (f (quote (("serde_derive"))))))

