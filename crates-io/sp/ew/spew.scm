(define-module (crates-io sp ew spew) #:use-module (crates-io))

(define-public crate-spew-0.1.0 (c (n "spew") (v "0.1.0") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy") (r "^0.10") (f (quote ("bevy_asset" "bevy_winit" "x11" "bevy_render" "bevy_core_pipeline" "bevy_scene" "bevy_sprite" "bevy_ui"))) (k 2)))) (h "0wwkap8k1mqlzqwi2cbl510rx4pn1pv75iangpgwzgzn2qx8gwc1")))

(define-public crate-spew-0.1.1 (c (n "spew") (v "0.1.1") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy") (r "^0.10") (f (quote ("bevy_asset" "bevy_winit" "x11" "bevy_render" "bevy_core_pipeline" "bevy_scene" "bevy_sprite" "bevy_ui"))) (k 2)))) (h "16imi5zmxifgl5q83b53vv59gwh6xk5qpi1k3asq00ddkqxw1k7a")))

(define-public crate-spew-0.2.0 (c (n "spew") (v "0.2.0") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy") (r "^0.10") (f (quote ("bevy_asset" "bevy_winit" "x11" "bevy_render" "bevy_core_pipeline" "bevy_scene" "bevy_sprite" "bevy_ui"))) (k 2)))) (h "1ka0v2xzv3p4pw55ha1hlmd5aqbk81l1dpcys0g8vkai8fr7qn2f")))

(define-public crate-spew-0.2.1 (c (n "spew") (v "0.2.1") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy") (r "^0.10") (f (quote ("bevy_asset" "bevy_winit" "x11" "bevy_render" "bevy_core_pipeline" "bevy_scene" "bevy_sprite" "bevy_ui"))) (k 2)))) (h "1aiqyjbwbj6fag2b7ir9ps9r78v737zvac5d8rn6p9d0jwvxxplb")))

(define-public crate-spew-0.2.2 (c (n "spew") (v "0.2.2") (d (list (d (n "bevy") (r "^0.10") (k 0)) (d (n "bevy") (r "^0.10") (f (quote ("bevy_asset" "bevy_winit" "x11" "bevy_render" "bevy_core_pipeline" "bevy_scene" "bevy_sprite" "bevy_ui"))) (k 2)))) (h "1yghx8srggmrf2gvlr0k75h944gd0dylk3bw3d6w2pm8mijbfphx")))

(define-public crate-spew-0.3.0 (c (n "spew") (v "0.3.0") (d (list (d (n "bevy") (r "^0.11") (k 0)) (d (n "bevy") (r "^0.11") (f (quote ("bevy_asset" "bevy_winit" "x11" "bevy_render" "bevy_core_pipeline" "bevy_scene" "bevy_sprite" "bevy_ui"))) (k 2)))) (h "1g3ppjj3hf3h7hnrmnb37j87aydympcrwf5wpmbbfng9a79xhwa0")))

(define-public crate-spew-0.4.0 (c (n "spew") (v "0.4.0") (d (list (d (n "bevy") (r "^0.12") (k 0)) (d (n "bevy") (r "^0.12") (f (quote ("bevy_asset" "bevy_winit" "x11" "bevy_render" "bevy_core_pipeline" "bevy_scene" "bevy_sprite" "bevy_ui"))) (k 2)))) (h "0sqqwfx3gjqr12ci6piz4v30h3i8x1hybwvrh7yr590i89svqw62")))

(define-public crate-spew-0.5.0 (c (n "spew") (v "0.5.0") (d (list (d (n "bevy") (r "^0.13.0") (k 0)) (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_asset" "bevy_winit" "x11" "bevy_render" "bevy_core_pipeline" "bevy_scene" "bevy_sprite" "bevy_ui"))) (k 2)))) (h "15m6cg7s9lw76q7vy8ayj5mbvy4jmf6g318acqdcnq4a9lx3wafc")))

(define-public crate-spew-0.5.1 (c (n "spew") (v "0.5.1") (d (list (d (n "bevy") (r "^0.13.0") (k 0)) (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_asset" "bevy_winit" "x11" "bevy_render" "bevy_core_pipeline" "bevy_scene" "bevy_sprite" "bevy_ui"))) (k 2)))) (h "0i4pr74wvfxinx57w22sddd57frjcyz8saajl235cvacwv92x8hd")))

