(define-module (crates-io sp -d sp-database) #:use-module (crates-io))

(define-public crate-sp-database-2.0.0-alpha.7 (c (n "sp-database") (v "2.0.0-alpha.7") (d (list (d (n "kvdb") (r "^0.5.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)))) (h "1fyxifgpkicgnpfazxqhm18xj9ss6vlxsjr5g2g13adpqc6havn9")))

(define-public crate-sp-database-2.0.0-alpha.8 (c (n "sp-database") (v "2.0.0-alpha.8") (d (list (d (n "kvdb") (r "^0.6.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)))) (h "16vdnc12wb3hb4rfz7agb4phf2n7b3s3bvahf4gh78xfz1jmpfmn")))

(define-public crate-sp-database-2.0.0-rc1 (c (n "sp-database") (v "2.0.0-rc1") (d (list (d (n "kvdb") (r "^0.6.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)))) (h "1cg1cl9xq1im8x28b3za49mdg0xxyc3n7yxz33x49nk1fd8z7226")))

(define-public crate-sp-database-2.0.0-rc2 (c (n "sp-database") (v "2.0.0-rc2") (d (list (d (n "kvdb") (r "^0.6.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)))) (h "0ri2n0kmgdgx15901q6147qnd0yr0346kz2q1ra7v1v1nfxgcpjm")))

(define-public crate-sp-database-2.0.0-rc3 (c (n "sp-database") (v "2.0.0-rc3") (d (list (d (n "kvdb") (r "^0.6.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)))) (h "1mra436xxg0dzbqbr4zgmpgp6r9cwizadkwsd1j613krrpnqs4p8")))

(define-public crate-sp-database-2.0.0-rc4 (c (n "sp-database") (v "2.0.0-rc4") (d (list (d (n "kvdb") (r "^0.6.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)))) (h "08yq44as60n9jyq34ws2va0qpakbsfp8fc5j58myw02v2il1dsma")))

(define-public crate-sp-database-2.0.0-rc5 (c (n "sp-database") (v "2.0.0-rc5") (d (list (d (n "kvdb") (r "^0.7.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)))) (h "0zz7mq3kbqlm7yfa300szphxgpjshk3dsbc34d9c8jyzif97i949")))

(define-public crate-sp-database-2.0.0-rc6 (c (n "sp-database") (v "2.0.0-rc6") (d (list (d (n "kvdb") (r "^0.7.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)))) (h "09rhbzvfzbrvbh06i5bx1jhyvq4p2vas18kbq2hdl3b63i7fpyi3")))

(define-public crate-sp-database-2.0.0 (c (n "sp-database") (v "2.0.0") (d (list (d (n "kvdb") (r "^0.7.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)))) (h "1pap4jfhxm2isqn6y555mjvp3icg0gys44vzqbgvrrggrqp3a72c")))

(define-public crate-sp-database-2.0.1 (c (n "sp-database") (v "2.0.1") (d (list (d (n "kvdb") (r "^0.7.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)))) (h "0ya92mkcqqfqnfcb2620pn8pxlakcbm3jlsyvwfbhdm2fbjiphaj")))

(define-public crate-sp-database-3.0.0 (c (n "sp-database") (v "3.0.0") (d (list (d (n "kvdb") (r "^0.9.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "14h4dxgbvngm6kx59rkhhv95ki2raxy6a968jlfikvryj4cg1hy8")))

(define-public crate-sp-database-4.0.0 (c (n "sp-database") (v "4.0.0") (d (list (d (n "kvdb") (r "^0.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "17d9l30v1g2l6b10klc0jm4azpgqvfrnnfg6c029bx7h42yl1z4c")))

(define-public crate-sp-database-5.0.0 (c (n "sp-database") (v "5.0.0") (d (list (d (n "kvdb") (r "^0.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0r4d5p0qxyphp98r6dzq7b4b3mmrayli1ss9bdfr9qd9licyzmjd")))

(define-public crate-sp-database-4.1.0-dev1 (c (n "sp-database") (v "4.1.0-dev1") (d (list (d (n "kvdb") (r "^0.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0qjcvcpy43phm8rzlaylxn9mpks9zg01gr79p8mmfvn3pa4cd71w")))

(define-public crate-sp-database-5.1.0-dev1 (c (n "sp-database") (v "5.1.0-dev1") (d (list (d (n "kvdb") (r "^0.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "03blb3ksng1p1jq0f9wkmlf6x94vh5kxcn9szsh4q1bc84jzxgj2")))

(define-public crate-sp-database-5.1.0-dev.2 (c (n "sp-database") (v "5.1.0-dev.2") (d (list (d (n "kvdb") (r "^0.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0bmziia1fkbnhc706f7vvxppyqmccixidcsg70ld7dxa6j35bpsg")))

(define-public crate-sp-database-5.1.0-dev.3 (c (n "sp-database") (v "5.1.0-dev.3") (d (list (d (n "kvdb") (r "^0.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1n04d4n0brf2bs78bm59my6l7q2hkw68fldwcrj7nascmwkr2ahl")))

(define-public crate-sp-database-5.1.0-dev.4 (c (n "sp-database") (v "5.1.0-dev.4") (d (list (d (n "kvdb") (r "^0.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "18pq5kn3v94ybvg99drnklliidm4x5rmnhnkbwipmxndx3d7bnwy")))

(define-public crate-sp-database-5.1.0-dev.5 (c (n "sp-database") (v "5.1.0-dev.5") (d (list (d (n "kvdb") (r "^0.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1rjm6ljcppr7yb35ydimcgk8qx9yv174ah8hxlvv2rf4c42w05i9")))

(define-public crate-sp-database-5.1.0-dev.6 (c (n "sp-database") (v "5.1.0-dev.6") (d (list (d (n "kvdb") (r "^0.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1zz4spvsqxky04svb3aw6q8v8gbb725h3qgym66lar899zw8g2lv")))

(define-public crate-sp-database-6.0.0 (c (n "sp-database") (v "6.0.0") (d (list (d (n "kvdb") (r "^0.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1wc7bg2qksp1a4j1ywa12dpazp8r8sy5dbb0mxz6sy7pi75x213l")))

(define-public crate-sp-database-7.0.0 (c (n "sp-database") (v "7.0.0") (d (list (d (n "kvdb") (r "^0.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1nm5zgj8d74g7iyw3nh51bvizs76fcxq9c86g8ddb79h9wdwl5mc")))

(define-public crate-sp-database-8.0.0-dev.1 (c (n "sp-database") (v "8.0.0-dev.1") (d (list (d (n "kvdb") (r "^0.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0v06rh3wgbvym8443dpvba9w004nixgm7j1c37fnvmv8f6agqnpx")))

(define-public crate-sp-database-8.0.0 (c (n "sp-database") (v "8.0.0") (d (list (d (n "kvdb") (r "^0.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1giy1wv0hwpl34sw14ajlllrkdb8c3snnkmqiq5032dgd2agf9db")))

(define-public crate-sp-database-9.0.0 (c (n "sp-database") (v "9.0.0") (d (list (d (n "kvdb") (r "^0.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "150asvivv6s5pyx80r44jgbnl7rxkjx9sbglyqm52wba1mqqqvlw")))

(define-public crate-sp-database-10.0.0 (c (n "sp-database") (v "10.0.0") (d (list (d (n "kvdb") (r "^0.13.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0ab9k36v82zyvks1y35y47i7spz9a7mpzl6v6y0mg52vpz6vwb3j")))

