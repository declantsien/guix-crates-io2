(define-module (crates-io sp -d sp-dto) #:use-module (crates-io))

(define-public crate-sp-dto-0.1.0 (c (n "sp-dto") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4" "stdweb"))) (d #t) (k 0)))) (h "1hf1jwq1cmss72428sdgvvwvm8rx01kkvd942km5j7ni56f71iy8")))

