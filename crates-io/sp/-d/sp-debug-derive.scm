(define-module (crates-io sp -d sp-debug-derive) #:use-module (crates-io))

(define-public crate-sp-debug-derive-2.0.0-alpha.2 (c (n "sp-debug-derive") (v "2.0.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "0scn19vn435az43ys2w6bs8n6nfryicx0pbv4ax24ycm77nfg3k7") (f (quote (("std"))))))

(define-public crate-sp-debug-derive-2.0.0-alpha.3 (c (n "sp-debug-derive") (v "2.0.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "19h225b05ly19mbqy4853m34mc2n5g2rmnbrygs43x1fap2m6fr7") (f (quote (("std"))))))

(define-public crate-sp-debug-derive-2.0.0-alpha.5 (c (n "sp-debug-derive") (v "2.0.0-alpha.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "1cjs3f0fwv9w2hlh52785flpr6fa25465k0m8y33jwc95px3ykqj") (f (quote (("std"))))))

(define-public crate-sp-debug-derive-2.0.0-alpha.6 (c (n "sp-debug-derive") (v "2.0.0-alpha.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "0d41fc59kg5sqi448z81iplgqvl70h4y74bysdjkyfwx5a98m5nz") (f (quote (("std"))))))

(define-public crate-sp-debug-derive-2.0.0-alpha.7 (c (n "sp-debug-derive") (v "2.0.0-alpha.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "1s6046kbz7aw3fb60r3i4krdaxbair0j9w1wg6bdz53np80cpi5j") (f (quote (("std"))))))

(define-public crate-sp-debug-derive-2.0.0-alpha.8 (c (n "sp-debug-derive") (v "2.0.0-alpha.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "1c7lw8mmkqrda99y18m3zpmg2v81ydp2f3r9q5mqm1i3y0cbvzdf") (f (quote (("std"))))))

(define-public crate-sp-debug-derive-2.0.0-rc1 (c (n "sp-debug-derive") (v "2.0.0-rc1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "0wjl8bj5swxcssxarx27v083nj09dq601zjzgqzw6a0wgw41dkfb") (f (quote (("std"))))))

(define-public crate-sp-debug-derive-2.0.0-rc2 (c (n "sp-debug-derive") (v "2.0.0-rc2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "00pnl0zr5phlg837kx1bb63knppxcmcwkybkx7ipzdv9fplj0nzp") (f (quote (("std"))))))

(define-public crate-sp-debug-derive-2.0.0-rc3 (c (n "sp-debug-derive") (v "2.0.0-rc3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "1vx4rmm92bdik570693z16czwhd0rldk7gy7igpjj9nd6fsakrxa") (f (quote (("std"))))))

(define-public crate-sp-debug-derive-2.0.0-rc4 (c (n "sp-debug-derive") (v "2.0.0-rc4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "0q4m5d2kswk0wxpnq8gbksqv8dyqf5d2hgnad0w2nkhc5jzg73ay") (f (quote (("std"))))))

(define-public crate-sp-debug-derive-2.0.0-rc5 (c (n "sp-debug-derive") (v "2.0.0-rc5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "1a8npdzl4bdw9yl5v741bz972s0dd4y3aii61m5dk22g49s84f6v") (f (quote (("std"))))))

(define-public crate-sp-debug-derive-2.0.0-rc6 (c (n "sp-debug-derive") (v "2.0.0-rc6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "15gi0iiqng8h8lqi62zwdyj2m3dbj54vfpx3q6k0l0k4ph26xicg") (f (quote (("std"))))))

(define-public crate-sp-debug-derive-2.0.0 (c (n "sp-debug-derive") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "1p26hhia449ns34qa0g05zy9gfmi617rfjl3drzngx70hjq50dwa") (f (quote (("std"))))))

(define-public crate-sp-debug-derive-2.0.1 (c (n "sp-debug-derive") (v "2.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "1a7vaxgvlzg4n4p87xybqq3k49czhxi3p4l2wc69bazyy8j1m2jm") (f (quote (("std"))))))

(define-public crate-sp-debug-derive-3.0.0 (c (n "sp-debug-derive") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (d #t) (k 0)))) (h "07pkjyz6r65b2zv4b57fs83p63jk2vq90pzc9psshysf7gr7a0p8") (f (quote (("std"))))))

(define-public crate-sp-debug-derive-4.0.0 (c (n "sp-debug-derive") (v "4.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (d #t) (k 0)))) (h "1wkgjln5zdfsbjar087mxp8n3x0xqjzfg0bf2yb0fap2f94ncxnn") (f (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-5.0.0 (c (n "sp-debug-derive") (v "5.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "0m346hrb6zkfvdvq6yccdv16n744gx207sapmaj9cfvpa9rvmsd9") (f (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-6.0.0 (c (n "sp-debug-derive") (v "6.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "0bfzqcblfmpn92rsp8aya8ic2rmxw2v0b9b2xmxpvpjl7p39vyv6") (f (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-7.0.0 (c (n "sp-debug-derive") (v "7.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "1aw96cbzx67hvawyddyrv54zrr4fkz66ri9pv2ww9nprkvniw8b2") (f (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-8.0.0 (c (n "sp-debug-derive") (v "8.0.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "030lh6qrk0ddhcmn6jbp9bzr8zfg1j1jhi678i8rj5ig9n0k3xf7") (f (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-8.1.0-dev1 (c (n "sp-debug-derive") (v "8.1.0-dev1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "0vn88hpyw2ws2rzpsbxpfll0ri2ifpcs85fdjaa4wgp7mg0dh0ap") (f (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-8.1.0-dev.2 (c (n "sp-debug-derive") (v "8.1.0-dev.2") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "08q6kwz3a6zf9wjypqdzdarc72sjywcdbj35dd4k293ys14g9r7i") (f (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-8.1.0-dev.3 (c (n "sp-debug-derive") (v "8.1.0-dev.3") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "0dilfwcplzq5qz5b63q2p24rzizg1pxmx65vwv68rn0m1cj16hs4") (f (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-8.1.0-dev.4 (c (n "sp-debug-derive") (v "8.1.0-dev.4") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "1na3zwgv5pawyjbs3zmh1h6nk5wc0ayaivdnjbhc2s3srbfxysjn") (f (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-8.1.0-dev.5 (c (n "sp-debug-derive") (v "8.1.0-dev.5") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "0w52nlsll3xr748dpv9gz3y03ldd7c38ird1d191sxzfap3j34fs") (f (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-8.1.0-dev.6 (c (n "sp-debug-derive") (v "8.1.0-dev.6") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "0lf4qrx660g5igww1bc1vwsndgfv6aa7k5bw40rf8fr986psmlnw") (f (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-9.0.0 (c (n "sp-debug-derive") (v "9.0.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "1rglm58x8gj4nh1077cj772vg79hv5my2k7x7z8jbs61yryawbgi") (f (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-10.0.0 (c (n "sp-debug-derive") (v "10.0.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "159xlbd0qj1s3c9qpwdwfxdihnysaj1qqwhady7da93imnh3bcml") (f (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-11.0.0-dev.1 (c (n "sp-debug-derive") (v "11.0.0-dev.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "0jy1q04dx2nzwa3rq4dn1ablvb7n4ijd02axgb81bi5snqg5lv3r") (f (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-11.0.0 (c (n "sp-debug-derive") (v "11.0.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1y0bwndvvfsh4s5q11msjrm6dl5qljszaaw825p5d405c5sx7xqn") (f (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-12.0.0 (c (n "sp-debug-derive") (v "12.0.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "18g4s9z8nvm8hxmp291ckikwh766zaz9xdcm25fbmlq8awd5wlsh") (f (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-13.0.0 (c (n "sp-debug-derive") (v "13.0.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "063ak44kyjvyza4q3xh3cpb46d0hrj1v28297zwhv51y1ik2rzch") (f (quote (("std") ("force-debug") ("default" "std"))))))

(define-public crate-sp-debug-derive-14.0.0 (c (n "sp-debug-derive") (v "14.0.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1gkx01f91gknvvbawj416ix0y8167n2y6np23sw9yagplnh9zl28") (f (quote (("std") ("force-debug") ("default" "std"))))))

