(define-module (crates-io sp yp spyparty) #:use-module (crates-io))

(define-public crate-spyparty-0.1.0 (c (n "spyparty") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "032h8rqln472sskn02k7w44fx1pqzasj24ag9v4j4glipppcrcvs")))

(define-public crate-spyparty-0.1.1 (c (n "spyparty") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0j525400qmmyp0kwlwx28vyzybgzyqa941fzn9zrl8n02ldshcrb")))

(define-public crate-spyparty-0.1.2 (c (n "spyparty") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0hhs7y4mbr7g2bjjy2imzfzq9aiq8gp5vr31vrhdnzxvr87mr5gk")))

(define-public crate-spyparty-0.1.3 (c (n "spyparty") (v "0.1.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0j5cnllgwr6s1lslcqrr2wwqkaqsj7j9zllxqg54nsrgdypw5rzx")))

(define-public crate-spyparty-0.1.4 (c (n "spyparty") (v "0.1.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1xb7hj6s3jm0xks9frw7dgra00wjzz9pylzd95pr58h67p05adz2")))

(define-public crate-spyparty-0.1.5 (c (n "spyparty") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "12w1dymbrik25cp2xd3lcc5d4qkijvsq952wmh4525s4pgs1bj03")))

(define-public crate-spyparty-0.1.6 (c (n "spyparty") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "030cwdbl7k0cfh589c9pd46b53ibf2akiz2yf7pkfrd40whdaz9d")))

(define-public crate-spyparty-0.1.7 (c (n "spyparty") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "05phv1qqvcms93qfv7y4qb97zcgm9lm2lv8y1i8d0mxa28vij78k")))

(define-public crate-spyparty-0.1.8 (c (n "spyparty") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1rc46nkm91h227d4f5d5h1qzvhhwp13ims7g91f57clk9vajzn20")))

(define-public crate-spyparty-0.1.9 (c (n "spyparty") (v "0.1.9") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1sgacadmy6d4d4zjy8g4c4mfgsa7a7fixn00vaz9i3ci8ssl97am")))

