(define-module (crates-io sp hr sphrs) #:use-module (crates-io))

(define-public crate-sphrs-0.0.1 (c (n "sphrs") (v "0.0.1") (d (list (d (n "ndarray") (r "^0.13") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.12") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "openblas-src") (r "^0.7") (d #t) (k 0)))) (h "0s427l1748nqvg5dsngix3mddvsj89ywgqxifz4fp5nxs9vwhqi6")))

(define-public crate-sphrs-0.1.0 (c (n "sphrs") (v "0.1.0") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.2.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.10") (d #t) (k 0)))) (h "1s1z3r6ar50rsx2y4mmdjk1856g6krm6iz9h3h07zlivpshpx3yg")))

(define-public crate-sphrs-0.1.1 (c (n "sphrs") (v "0.1.1") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.2.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.10") (d #t) (k 0)))) (h "1xcvzksrg6vvj6ps8vxiwb7bgx2djl90h4rw0q60s8fcq5z8xpk9")))

(define-public crate-sphrs-0.1.2 (c (n "sphrs") (v "0.1.2") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.2.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.10") (d #t) (k 0)))) (h "0s541ihqlw179lmys6m368191880i9fkkynm1cj81vl1jmyr079d")))

(define-public crate-sphrs-0.1.3 (c (n "sphrs") (v "0.1.3") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.2.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.10") (d #t) (k 0)))) (h "0464l09fk356iww2p8f67jgrhdp8q5vmj88xcg6mmjj5086mr0x5")))

(define-public crate-sphrs-0.2.0 (c (n "sphrs") (v "0.2.0") (d (list (d (n "approx") (r "^0.5") (f (quote ("num-complex"))) (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1h6fzkcgf52r26571avbzs5i6mkmi3kdnkbv5gn3alg0vrvnln2i")))

(define-public crate-sphrs-0.2.1 (c (n "sphrs") (v "0.2.1") (d (list (d (n "approx") (r "^0.5") (f (quote ("num-complex"))) (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1d8bw1mzpk8aa46rw14ayhlb9va04k1f25pg55xmj4vqh5dv6w3i")))

(define-public crate-sphrs-0.2.2 (c (n "sphrs") (v "0.2.2") (d (list (d (n "approx") (r "^0.5") (f (quote ("num-complex"))) (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "18bxj8hr9fzs01hwzidcx9zzzfd1nhgmhilybc3rrqinl4vqp451")))

