(define-module (crates-io sp -u sp-utils) #:use-module (crates-io))

(define-public crate-sp-utils-2.0.0-alpha.6 (c (n "sp-utils") (v "2.0.0-alpha.6") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.8.0") (d #t) (k 0)))) (h "1qqcp3mnqqfh1dnzx9z0vzn7rrz040npsgs8v78ms43diyndfg5f") (f (quote (("metered") ("default" "metered"))))))

(define-public crate-sp-utils-2.0.0-alpha.7 (c (n "sp-utils") (v "2.0.0-alpha.7") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.8.0") (d #t) (k 0)))) (h "114kygk93cx7jad99k8l8j5pnn1a0ff4sn2076mwrdqv6v65map3") (f (quote (("metered") ("default" "metered"))))))

(define-public crate-sp-utils-2.0.0-alpha.8 (c (n "sp-utils") (v "2.0.0-alpha.8") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.8.0") (d #t) (k 0)))) (h "055f7ip1gm4ps7a0m9g2b4a0ki484ssga5v239vlcqm69irfdbwr") (f (quote (("metered") ("default" "metered"))))))

(define-public crate-sp-utils-2.0.0-rc1 (c (n "sp-utils") (v "2.0.0-rc1") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.8.0") (d #t) (k 0)))) (h "093vbisfqlwzdag41xjfpdynvnns5wgaryllmn8z7wxiyqssic9n") (f (quote (("metered") ("default" "metered"))))))

(define-public crate-sp-utils-2.0.0-rc2 (c (n "sp-utils") (v "2.0.0-rc2") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.8.0") (d #t) (k 0)))) (h "0czisnwwj38kwbq80cyf65q7dyf883s3ypla08z23wznn4afk6m1") (f (quote (("metered") ("default" "metered"))))))

(define-public crate-sp-utils-2.0.0-rc3 (c (n "sp-utils") (v "2.0.0-rc3") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.8.0") (d #t) (k 0)))) (h "0lws0pxask2xdyrp3mqmbn3cgwbllww2q32awzkp18vd4cqp20i3") (f (quote (("metered") ("default" "metered"))))))

(define-public crate-sp-utils-2.0.0-rc4 (c (n "sp-utils") (v "2.0.0-rc4") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.8.0") (d #t) (k 0)))) (h "157zy9cv3rhlpvd9pikldn8g1443c969ihyy60dwsnqyiac0cvly") (f (quote (("metered") ("default" "metered"))))))

(define-public crate-sp-utils-2.0.0-rc5 (c (n "sp-utils") (v "2.0.0-rc5") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.8.0") (d #t) (k 0)))) (h "0f1wwwjin77r6ikksgwlk5fln4v3mh51mp6s42zj551ha4sbqpsx") (f (quote (("metered") ("default" "metered"))))))

(define-public crate-sp-utils-2.0.0-rc6 (c (n "sp-utils") (v "2.0.0-rc6") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.9.0") (k 0)))) (h "10balyczcacfncds8b0lcd1ipbsrb8iarx2mgpks16fp9cki6n6l") (f (quote (("metered") ("default" "metered"))))))

(define-public crate-sp-utils-2.0.0 (c (n "sp-utils") (v "2.0.0") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.10.0") (k 0)))) (h "0l0ik4cjikc336mjmadb7hz7b27x2h29ibypi3ibb2dcw810lcc4") (f (quote (("metered") ("default" "metered"))))))

(define-public crate-sp-utils-2.0.1 (c (n "sp-utils") (v "2.0.1") (d (list (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.10.0") (k 0)))) (h "1y1inwibvxxvzxiyk1zjqjqjg5c59i76sd37mm8a48c7zrhfpxas") (f (quote (("metered") ("default" "metered"))))))

(define-public crate-sp-utils-3.0.0 (c (n "sp-utils") (v "3.0.0") (d (list (d (n "futures") (r "^0.3.9") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.11.0") (k 0)))) (h "18f1v3m5bybmq35s8w8ww6hdyz6q5g0g7y2rx78zyyjgj9gjmv5l") (f (quote (("metered") ("default" "metered"))))))

