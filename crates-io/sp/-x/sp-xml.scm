(define-module (crates-io sp -x sp-xml) #:use-module (crates-io))

(define-public crate-sp-xml-0.1.1 (c (n "sp-xml") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "yaserde") (r "^0.3.16") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.16") (d #t) (k 0)))) (h "1glz9ixi2mxbrlr8brqrkk41pyh6rwx9w5yniay1csgv0j6j8h0m")))

(define-public crate-sp-xml-0.1.2 (c (n "sp-xml") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "yaserde") (r "^0.3.16") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.16") (d #t) (k 0)))) (h "1z329shimy96xifyyvjilc2yrsn61kixni36fynksjvhs1kgik9r")))

(define-public crate-sp-xml-0.1.3 (c (n "sp-xml") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "yaserde") (r "^0.3.16") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.16") (d #t) (k 0)))) (h "1j5qpv085g6rijw4ln221qdnqmi409fn4l4ns7rzp0qmxdpdrxbs")))

(define-public crate-sp-xml-0.1.4 (c (n "sp-xml") (v "0.1.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "yaserde") (r "^0.3.16") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.16") (d #t) (k 0)))) (h "074kldj2xiyvrwxna3zdvfka4bac3pyjnp1kykdvqc05kf98k5qs")))

(define-public crate-sp-xml-0.1.5 (c (n "sp-xml") (v "0.1.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "yaserde") (r "^0.3.16") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.16") (d #t) (k 0)))) (h "0j9p6g13ivsc3m9vdzsiyab3c71l8ha3d8p6lj5dh7ar3xq1dvx4")))

(define-public crate-sp-xml-0.1.6 (c (n "sp-xml") (v "0.1.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "yaserde") (r "^0.3.16") (d #t) (k 0)) (d (n "yaserde_derive") (r "^0.3.16") (d #t) (k 0)))) (h "1i2plf4xzla21lyxn76brgxcdd7bp5csahjij61k8rib0n8qg8cx")))

