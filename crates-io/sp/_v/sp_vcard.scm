(define-module (crates-io sp _v sp_vcard) #:use-module (crates-io))

(define-public crate-sp_vcard-0.1.0 (c (n "sp_vcard") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "mime") (r "^0.3.17") (d #t) (k 0)) (d (n "sp_vcard_derive") (r "^0.1") (d #t) (k 0)))) (h "0d4havvkk3r1y8m8m57yh7n5fvnshgfprcqjlwf9z4mbb4ly0iip")))

(define-public crate-sp_vcard-0.2.0 (c (n "sp_vcard") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "sp_vcard_derive") (r "^0") (d #t) (k 0)))) (h "0wvmjhqcjrczvzj244s5h9582nnq8qg4j4akc83arh9ngcwplx6k")))

