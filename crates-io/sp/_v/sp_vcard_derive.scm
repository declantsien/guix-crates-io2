(define-module (crates-io sp _v sp_vcard_derive) #:use-module (crates-io))

(define-public crate-sp_vcard_derive-0.1.0 (c (n "sp_vcard_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0pdb7iqkdq2rzz3310mxxrsxdcrrrww05rvc5jb9j09k9vnj4bp5")))

(define-public crate-sp_vcard_derive-0.2.0 (c (n "sp_vcard_derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "000xd5jz1im75njlcajbrxfapm91llrwd2i0p35dlcg129jnmz5g")))

