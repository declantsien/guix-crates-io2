(define-module (crates-io sp ez spez-macros) #:use-module (crates-io))

(define-public crate-spez-macros-0.1.0 (c (n "spez-macros") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "1p6i2z84a8jq0k3v4i7aq60l57mw5q32j48mvkd6rjwcgl1hdyn7")))

