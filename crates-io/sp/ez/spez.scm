(define-module (crates-io sp ez spez) #:use-module (crates-io))

(define-public crate-spez-0.1.0 (c (n "spez") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "spez-macros") (r "= 0.1.0") (d #t) (k 0)))) (h "0h08wfhzp4nxk8b6pdzqrnzivlsqnzacd72aqk42la83gki960hx")))

(define-public crate-spez-0.1.1 (c (n "spez") (v "0.1.1") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "spez-macros") (r "= 0.1.0") (d #t) (k 0)))) (h "12rnryxfb68psyw3ynnnm8p8fby2g58cva3v41xzfycqlzb2yk9h")))

(define-public crate-spez-0.1.2 (c (n "spez") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1s4619r9p3z6avb1hxhbg548kgj6sa6xxgbbp3p8h9ya9l7rczn8")))

