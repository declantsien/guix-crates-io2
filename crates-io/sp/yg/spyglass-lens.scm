(define-module (crates-io sp yg spyglass-lens) #:use-module (crates-io))

(define-public crate-spyglass-lens-0.1.0 (c (n "spyglass-lens") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1is6b7aq8rnbwycshwjbh1agnf7w1zy3g0yw8hnpp9i7gjk4qj5p")))

(define-public crate-spyglass-lens-0.1.1 (c (n "spyglass-lens") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ws7dgkzr37a8dbxl4b9ir1iv820z1vr0g7mlrndii0gdkc2syas")))

(define-public crate-spyglass-lens-0.1.2 (c (n "spyglass-lens") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "125h3j29jrlr1fcbgib7hdg02xmnlvlspypc7v0yf61hfp3fp8za")))

(define-public crate-spyglass-lens-0.1.3 (c (n "spyglass-lens") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "blake2") (r "^0.10.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gy4r8adcwrqfjrw38ch9rnm66a8wy5kwh9qmcjckldcwppb2nsx")))

(define-public crate-spyglass-lens-0.1.4 (c (n "spyglass-lens") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "blake2") (r "^0.10.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cksivkp5qi3v8xr5x93yqg1vbk0p6v572a614w606in7lsb5mw1")))

(define-public crate-spyglass-lens-0.1.5 (c (n "spyglass-lens") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "blake2") (r "^0.10.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sayir5z2h32gd1d97m7hmhbcicn2yjagn11n490dhkx9bfp4qab")))

(define-public crate-spyglass-lens-0.1.6 (c (n "spyglass-lens") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "blake2") (r "^0.10.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w3zhvj4771hnr39fbvh07iaw8j764dkqsfwdkwfp8pmxz8bnbjf")))

(define-public crate-spyglass-lens-0.1.7 (c (n "spyglass-lens") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "blake2") (r "^0.10.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ki86r2smzirkypvzdrvkrr0vkx7cgdv83badkjhr8r92z61ikjn")))

