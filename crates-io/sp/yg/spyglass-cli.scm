(define-module (crates-io sp yg spyglass-cli) #:use-module (crates-io))

(define-public crate-spyglass-cli-0.1.0 (c (n "spyglass-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.15") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0izjkfy9yvkrmpjhr0yqrwdkgyzlyzciinvmv0l4cwkp52bm6897")))

