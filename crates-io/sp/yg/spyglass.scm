(define-module (crates-io sp yg spyglass) #:use-module (crates-io))

(define-public crate-spyglass-0.1.0 (c (n "spyglass") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "deunicode") (r "^1.1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lwdac2qf0xbhbm19vp2rmspgrxighsqsdb52g613vnd527dg71w")))

(define-public crate-spyglass-1.1.0 (c (n "spyglass") (v "1.1.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "deunicode") (r "^1.1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bs3dh0fwmclm6877nv630993hdchlx223h01kdwbhkhvhgiysnp")))

