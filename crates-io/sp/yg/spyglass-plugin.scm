(define-module (crates-io sp yg spyglass-plugin) #:use-module (crates-io))

(define-public crate-spyglass-plugin-0.1.0 (c (n "spyglass-plugin") (v "0.1.0") (d (list (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lhypxkbzv15yzf4hcpxvmqgqaw01mdhzh2i3mb1wsj49xf5dz0x")))

