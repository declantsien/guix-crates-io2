(define-module (crates-io sp ms spms_ring) #:use-module (crates-io))

(define-public crate-spms_ring-0.1.1 (c (n "spms_ring") (v "0.1.1") (d (list (d (n "generic-array") (r "^0.14.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "1bfkm7ba0rnaj1di978p1l0q67cp8b09znsr6h35ng0vm4f337y5")))

(define-public crate-spms_ring-0.1.2 (c (n "spms_ring") (v "0.1.2") (d (list (d (n "generic-array") (r "^0.14.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "17256rpyji4isnmgg2m76d0bmv8k47v4fyz83mnc2y15p083dlhv")))

(define-public crate-spms_ring-0.1.3 (c (n "spms_ring") (v "0.1.3") (d (list (d (n "generic-array") (r "^0.14.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "1xbzq9s3nkjn6p7al1bkwsnn0aq1s7xr9kmd987pb537y4h6cj6z")))

(define-public crate-spms_ring-0.1.4 (c (n "spms_ring") (v "0.1.4") (d (list (d (n "generic-array") (r "^0.14.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "1mkhji3blhv5jx5vdp7rdfjbzaqx4568jk518d8639g87z603smk")))

