(define-module (crates-io sp i- spi-flash) #:use-module (crates-io))

(define-public crate-spi-flash-0.1.0 (c (n "spi-flash") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "jep106") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1qqcx6496cpwxxlj706q38qxbddkfnyhjx4dr3lc2w6ng0almggv")))

(define-public crate-spi-flash-0.2.0 (c (n "spi-flash") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "jep106") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0gnv4ml9qyh32cf5hby398kvnawhzrsk5wfr2siz69czxwqmmrc4")))

(define-public crate-spi-flash-0.2.1 (c (n "spi-flash") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "jep106") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "07y882big8q549p44m35234mcg752z4d72n8987pz8yzynxa5wr0")))

(define-public crate-spi-flash-0.2.2 (c (n "spi-flash") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "jep106") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "11lg611isbpslwcc1n2j63x611ah9n9vpgdkwj3309whyplvsldh")))

(define-public crate-spi-flash-0.3.0 (c (n "spi-flash") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.58") (o #t) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (o #t) (d #t) (k 0)) (d (n "jep106") (r "^0.2.6") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (k 0)) (d (n "thiserror") (r "^1.0.31") (o #t) (d #t) (k 0)))) (h "029s3v827c9zy9b9qzh0fsx7cn37l77jxgfqv9dplzrzzzmbpb7h") (f (quote (("std" "jep106" "thiserror" "anyhow" "indicatif" "num_enum/std") ("default" "std"))))))

