(define-module (crates-io sp i- spi-memory) #:use-module (crates-io))

(define-public crate-spi-memory-0.1.0 (c (n "spi-memory") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.8") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.3") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (o #t) (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.5.2") (d #t) (k 2)) (d (n "stm32f4xx-hal") (r "^0.5.0") (f (quote ("stm32f401"))) (d #t) (k 2)))) (h "01q394dgz0kvlxckigpjybnnp4g9vsbnd0pqavglv87xmnnir279")))

(define-public crate-spi-memory-0.2.0 (c (n "spi-memory") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.8") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.3") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (o #t) (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.5.2") (d #t) (k 2)) (d (n "stm32f4xx-hal") (r "^0.7.0") (f (quote ("stm32f401"))) (d #t) (k 2)))) (h "1ag4rr55mbii6xvc52xcl5v7kp2wp09l2gqspq2kwrymdfgayk7r")))

