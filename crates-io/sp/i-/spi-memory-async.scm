(define-module (crates-io sp i- spi-memory-async) #:use-module (crates-io))

(define-public crate-spi-memory-async-0.1.0 (c (n "spi-memory-async") (v "0.1.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.8") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.3") (d #t) (k 2)) (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1") (d #t) (k 0)) (d (n "embedded-storage") (r "^0.3.0") (d #t) (k 0)) (d (n "embedded-storage-async") (r "^0.4.0") (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.5.2") (d #t) (k 2)) (d (n "stm32f4xx-hal") (r "^0.7.0") (f (quote ("stm32f401"))) (d #t) (k 2)))) (h "0hcbn0w87if8knmifp91iwqkjx5q7ckf41cdr36dn2jfp1fxlafh")))

