(define-module (crates-io sp gl spglib-sys) #:use-module (crates-io))

(define-public crate-spglib-sys-0.1.0 (c (n "spglib-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "0s8dq9p9aw5pp2bf65n5ldg59v5bzzl2m7m9khp27wxfbaas98ca")))

(define-public crate-spglib-sys-0.1.1 (c (n "spglib-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "0njrpp9m9ls76zfayx5ni0853zfs410kb4zxh83lb0dg378grbd6")))

(define-public crate-spglib-sys-1.15.1 (c (n "spglib-sys") (v "1.15.1") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "06p7vyf6vmc4lnd7hw5di0csa8cdimfqzq9kcvvp1ak9f67lmsxd") (y #t)))

(define-public crate-spglib-sys-1.15.1+patch1 (c (n "spglib-sys") (v "1.15.1+patch1") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0932zlmvq116dxrz058wrvxcmbvlwjf5244b3sh4wwqpmpw1f8vk")))

(define-public crate-spglib-sys-1.16.1 (c (n "spglib-sys") (v "1.16.1") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1pc0yvz4bxgmi9izg4dq5spvk2jzmfa0ad7k5zwydwxm7rh4qjb3")))

