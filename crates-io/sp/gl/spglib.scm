(define-module (crates-io sp gl spglib) #:use-module (crates-io))

(define-public crate-spglib-0.1.0 (c (n "spglib") (v "0.1.0") (d (list (d (n "spglib-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1n071xx9pzrcvn9xx01laxm1j1pasxr6ra7vpj45pj96kajhrjbs") (y #t)))

(define-public crate-spglib-0.1.1 (c (n "spglib") (v "0.1.1") (d (list (d (n "spglib-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1gas2609dm6j053i7fpxpwjygw7pcz8lvx5znflb047n2z2qwx27") (y #t)))

(define-public crate-spglib-0.1.2 (c (n "spglib") (v "0.1.2") (d (list (d (n "spglib-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0lqih76gy5gz3pj9mq96h4vd259ripanjy09kif46mlr7pbw675a")))

(define-public crate-spglib-0.1.3 (c (n "spglib") (v "0.1.3") (d (list (d (n "spglib-sys") (r "^0.1.1") (d #t) (k 0)))) (h "01fx7is57xf7w6v0kgk5n4glc9alwrxswcixq8njggxv6yig335y")))

(define-public crate-spglib-1.15.1 (c (n "spglib") (v "1.15.1") (d (list (d (n "spglib-sys") (r "^1.15") (d #t) (k 0)))) (h "070iyb23zqcaxgd9kd03xccp5cmj44g9cbp5dcdbdc0hpkf67kpw")))

