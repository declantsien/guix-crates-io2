(define-module (crates-io sp ri springtime-web-axum-derive) #:use-module (crates-io))

(define-public crate-springtime-web-axum-derive-0.1.0 (c (n "springtime-web-axum-derive") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0q085j6lvpyfh7pshl50a2nla2n23i06lph7mql5hcvac1fwzw7p")))

(define-public crate-springtime-web-axum-derive-0.1.1 (c (n "springtime-web-axum-derive") (v "0.1.1") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "10lpi1kzgv40pwn51d9a4wj3gjvlnficbx0z8c9jdk528nimm9rm")))

