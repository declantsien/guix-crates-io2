(define-module (crates-io sp ri sprig) #:use-module (crates-io))

(define-public crate-sprig-0.1.1 (c (n "sprig") (v "0.1.1") (d (list (d (n "data-encoding") (r "^2.0") (d #t) (k 0)) (d (n "gtmpl") (r "^0.5") (d #t) (k 0)) (d (n "gtmpl_value") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0zhl6d24c0bijd4fwkax5zy7ljwi9a6j0mcmaj4bvnmc4p2asqpa")))

(define-public crate-sprig-0.1.2 (c (n "sprig") (v "0.1.2") (d (list (d (n "data-encoding") (r "^2.0") (d #t) (k 0)) (d (n "gtmpl") (r "^0.5") (d #t) (k 0)) (d (n "gtmpl_value") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0zvsbzbg6q39xxbq37spf0l7dj6av9gh09m425fvbh7cfr88ga9z")))

(define-public crate-sprig-0.1.3 (c (n "sprig") (v "0.1.3") (d (list (d (n "data-encoding") (r "^2.0") (d #t) (k 0)) (d (n "gtmpl") (r "^0.5") (d #t) (k 0)) (d (n "gtmpl_value") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0z4ln0jivcpq0x23cjv6hkzcwa46ggvwim5zj7wv3dx46kcldq5d")))

(define-public crate-sprig-0.2.0 (c (n "sprig") (v "0.2.0") (d (list (d (n "data-encoding") (r "^2.0") (d #t) (k 0)) (d (n "gtmpl") (r "^0.6") (d #t) (k 0)) (d (n "gtmpl_value") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0qggf471mxhrid9crazdrlxcby3ffz1f7v3m4kvhbckch8gqnq9i")))

(define-public crate-sprig-0.3.0 (c (n "sprig") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "data-encoding") (r "^2.0") (d #t) (k 0)) (d (n "gtmpl") (r "^0.7") (d #t) (k 0)) (d (n "gtmpl_value") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0sypnm22f9gjjgafwrs48r132gl9yyl0lbljw90b9ndsv955f44q")))

