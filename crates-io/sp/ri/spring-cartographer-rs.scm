(define-module (crates-io sp ri spring-cartographer-rs) #:use-module (crates-io))

(define-public crate-spring-cartographer-rs-0.1.0 (c (n "spring-cartographer-rs") (v "0.1.0") (d (list (d (n "binrw") (r "^0.11.1") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "texpresso") (r "^2.0.1") (d #t) (k 0)))) (h "0k1kjfgh64dpjh9l2v5rrpsci46r2irlhhkamyl6x6g7z6qjm521")))

(define-public crate-spring-cartographer-rs-0.1.1 (c (n "spring-cartographer-rs") (v "0.1.1") (d (list (d (n "binrw") (r "^0.11.1") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "texpresso") (r "^2.0.1") (d #t) (k 0)))) (h "0agdyl472d4ciwqka6zz7dv1c269nq9jpjyy5wjii3y88ij90yih")))

