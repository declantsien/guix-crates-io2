(define-module (crates-io sp ri sprity) #:use-module (crates-io))

(define-public crate-sprity-0.1.0 (c (n "sprity") (v "0.1.0") (d (list (d (n "sprity-aseprite") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0nbkpwdjynxpjjmyly2q5lp0g46lylv0vqgi6h1sya56fr5qyfad") (f (quote (("default" "aseprite") ("aseprite" "sprity-aseprite"))))))

(define-public crate-sprity-0.2.0 (c (n "sprity") (v "0.2.0") (h "14748b0kx6iw5zfwdw7r0g1354km7iq23m5mlqdjl5g88cnsdbl0")))

