(define-module (crates-io sp ri sprites7800) #:use-module (crates-io))

(define-public crate-sprites7800-0.2.0 (c (n "sprites7800") (v "0.2.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_derive") (r "^4") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "15kascz1nmmsjd6zkxw9a3addz5vp4wxk3xprm0m3akm5g639h3d")))

(define-public crate-sprites7800-0.2.1 (c (n "sprites7800") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_derive") (r "^4") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1ix0c6y6zj9vip2wjkwyvhg2y44hy6kighbz0y6j538pdkwjrnxm")))

(define-public crate-sprites7800-0.3.0 (c (n "sprites7800") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_derive") (r "^4") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0jnhsdx5jgqcpdidnabwr46zcd8zq3r58i4szfyswi58phlrayba")))

(define-public crate-sprites7800-0.3.1 (c (n "sprites7800") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_derive") (r "^4") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0w1ha4hv1m5pad8g0kd05j53mhz9nnbnwfzns86xz9qbzddyifqd")))

(define-public crate-sprites7800-0.3.2 (c (n "sprites7800") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_derive") (r "^4") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "15dvnk71yrnhsi9hh8yp9mx0d0ka2d9f899wn6sz54hcp0wjpjkd")))

