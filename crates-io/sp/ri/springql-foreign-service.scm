(define-module (crates-io sp ri springql-foreign-service) #:use-module (crates-io))

(define-public crate-springql-foreign-service-0.1.0 (c (n "springql-foreign-service") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nzvzk3m9h3c52nc862wisxay44022zhc8a1vb96c4f6hkrkn2mk")))

(define-public crate-springql-foreign-service-0.1.1 (c (n "springql-foreign-service") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mfs4lbvja8nbp6wxq59z6053zyzzqslcwq14iqb404x6jmcw3pq")))

(define-public crate-springql-foreign-service-0.2.0 (c (n "springql-foreign-service") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wbw4bjpch48lywdxf5s8y6331ng7kidkrw4izig7rg6i3lcc9bi")))

(define-public crate-springql-foreign-service-0.4.0 (c (n "springql-foreign-service") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vkr134affvafjamiik5a7hacwvc1yvr64ss28sy764xjj0d9wpx")))

