(define-module (crates-io sp ri springtime-di-derive) #:use-module (crates-io))

(define-public crate-springtime-di-derive-0.2.0 (c (n "springtime-di-derive") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0fjsxaykjpn7xfh3w93w1yyz3bglqgadbfzxyi8sz408vf9bfydb") (f (quote (("threadsafe") ("default" "threadsafe") ("async")))) (r "1.65")))

(define-public crate-springtime-di-derive-0.3.0 (c (n "springtime-di-derive") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "15wyrg71886m3r3cy7mbs6j8axlisar9mfcg58vq7mfjdkhkqzng") (f (quote (("threadsafe") ("default" "threadsafe") ("async")))) (r "1.65")))

(define-public crate-springtime-di-derive-0.3.1 (c (n "springtime-di-derive") (v "0.3.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "10myjpj128dksf7vgsqbsdwgn5v4qvrb2g8af77kj3yqmgyn5x65") (f (quote (("threadsafe") ("default" "threadsafe") ("async")))) (r "1.65")))

