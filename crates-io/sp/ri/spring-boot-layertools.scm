(define-module (crates-io sp ri spring-boot-layertools) #:use-module (crates-io))

(define-public crate-spring-boot-layertools-0.1.0 (c (n "spring-boot-layertools") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "mmarinus") (r "^0.4.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "1gcmzx88a9w0x6dl0vpyni9f6kcwgif5fsznxip0yh1adagw94rz")))

(define-public crate-spring-boot-layertools-0.1.3 (c (n "spring-boot-layertools") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "mmarinus") (r "^0.4.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "0wk5fbbdknd28wcyq1pmq1haxwxsdnyiykabr956bx47xiv6avb5")))

(define-public crate-spring-boot-layertools-0.2.0 (c (n "spring-boot-layertools") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "mmarinus") (r "^0.4.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "1w0wfhhmf383zx84jbh1fzqgl3h9wg93v0z8i1zl9vgjsb6fnizv")))

(define-public crate-spring-boot-layertools-0.2.1 (c (n "spring-boot-layertools") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "mmarinus") (r "^0.4.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "1jc5cbh1d77x75samgjbwjx6bqh55h5wjnz1n2rl90rjc7l1bv3d")))

(define-public crate-spring-boot-layertools-1.0.0 (c (n "spring-boot-layertools") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "mmarinus") (r "^0.4.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "154p9i1f203nx7464ilfa7bypvwx7k9wdarglzdnhxj5s9qnmii4")))

