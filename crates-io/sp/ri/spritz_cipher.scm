(define-module (crates-io sp ri spritz_cipher) #:use-module (crates-io))

(define-public crate-spritz_cipher-0.1.0 (c (n "spritz_cipher") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "statistical") (r "^1.0") (d #t) (k 2)) (d (n "zeroize") (r "^1.0") (f (quote ("zeroize_derive"))) (k 0)))) (h "1s5ybcpign979cisklz8m69xr9bky21a911a27zm3dvz7j7qgi5z") (f (quote (("default"))))))

