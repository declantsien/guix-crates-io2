(define-module (crates-io sp ri sprinkles) #:use-module (crates-io))

(define-public crate-sprinkles-1.0.0 (c (n "sprinkles") (v "1.0.0") (d (list (d (n "clap") (r "^4.3.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termcolor") (r "^1.2.0") (d #t) (k 0)))) (h "09id1b7x44xih98fklg2ycfaz2zpnmz75dykfabsd982h17wipk9")))

