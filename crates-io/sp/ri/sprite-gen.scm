(define-module (crates-io sp ri sprite-gen) #:use-module (crates-io))

(define-public crate-sprite-gen-0.1.0 (c (n "sprite-gen") (v "0.1.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1p4ndac07k9k00w9xra1ji5bsdz2qlqrbm6gyns7r9a33a946vky")))

(define-public crate-sprite-gen-0.1.1 (c (n "sprite-gen") (v "0.1.1") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0x8zqj2cf4xvkflc8lfa89645r19n6sy4yk0lg6x1mpmkfbnq4vw")))

(define-public crate-sprite-gen-0.1.2 (c (n "sprite-gen") (v "0.1.2") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1szvmbqy1wd4vypqsncj08xmmlijl39ngp5gcf4dhavpy652m6g1")))

(define-public crate-sprite-gen-0.1.3 (c (n "sprite-gen") (v "0.1.3") (d (list (d (n "hsl") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1yf6gsf6cr47m6aqk2l60ng99a0b996fbad72jpr1swdpr3g6c9l")))

(define-public crate-sprite-gen-0.1.4 (c (n "sprite-gen") (v "0.1.4") (d (list (d (n "hsl") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0qd91biw15fmik5fab8ak886xb9nlgl19cfs4mszf1rkdc35pp7a")))

(define-public crate-sprite-gen-0.1.5 (c (n "sprite-gen") (v "0.1.5") (d (list (d (n "hsl") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0403j46j1462g0qc3lgkyj9pkb54zsn41h0fh4290xg329ajnqw5")))

(define-public crate-sprite-gen-0.1.6 (c (n "sprite-gen") (v "0.1.6") (d (list (d (n "hsl") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "021mawn0a8kb2rcxwa0n9jmlwah6n7vd1vzadmp1hv8dif7217lz")))

(define-public crate-sprite-gen-0.1.7 (c (n "sprite-gen") (v "0.1.7") (d (list (d (n "hsl") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "13ax1l8b3drp58x2dwzb21y92z998snx8yykc6msx04mhamjvsgg")))

(define-public crate-sprite-gen-0.1.8 (c (n "sprite-gen") (v "0.1.8") (d (list (d (n "hsl") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0526ghc7dsqwd0r7mx71c2r35lw0wrsng678yfz610ggkfngdwzg")))

(define-public crate-sprite-gen-0.1.9 (c (n "sprite-gen") (v "0.1.9") (d (list (d (n "hsl") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "03dr0cvfzxfrysk62vywa3qa7br3xnxr07ppic3y6q7xzzskfl03")))

(define-public crate-sprite-gen-0.2.0 (c (n "sprite-gen") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "hsl") (r "^0.1.1") (d #t) (k 0)) (d (n "randomize") (r "^3.0.1") (d #t) (k 0)))) (h "0qbxjs6iszpgm4d0zv2rdxfll5z50bp133v0qb4ia9q9g3xsdvb8")))

