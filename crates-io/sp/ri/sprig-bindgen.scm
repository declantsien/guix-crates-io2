(define-module (crates-io sp ri sprig-bindgen) #:use-module (crates-io))

(define-public crate-sprig-bindgen-0.1.0 (c (n "sprig-bindgen") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)))) (h "12w8bnp561waryva6l0nk8ascvm9l4v1sh9c8fp7akjywsp738ww")))

