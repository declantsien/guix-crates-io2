(define-module (crates-io sp ri sprint) #:use-module (crates-io))

(define-public crate-sprint-0.1.0 (c (n "sprint") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "bunt") (r "^0.2.8") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "shlex") (r "^1.2.0") (d #t) (k 0)))) (h "10llr7rm8sx6lybnkqvsy5alks4rphhfpyzr7058smh08f9ii4ir")))

(define-public crate-sprint-0.1.1 (c (n "sprint") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "bunt") (r "^0.2.8") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "shlex") (r "^1.2.0") (d #t) (k 0)))) (h "0ci3117ybazr0mf9x9fk2z4509ddfrclylblk3zg9qs5n5yl2x42")))

(define-public crate-sprint-0.1.2 (c (n "sprint") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.76") (d #t) (k 0)) (d (n "bunt") (r "^0.2.8") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "shlex") (r "^1.2.0") (d #t) (k 0)))) (h "0wr1k27v7185cd2mviylvj52dw89fm67fm2qqrnp09p7y4m7pmkz")))

(define-public crate-sprint-0.2.0 (c (n "sprint") (v "0.2.0") (d (list (d (n "bunt") (r "^0.2.8") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "shlex") (r "^1.2.0") (d #t) (k 0)))) (h "0w74646w1ilgjkpj9jhzmh7hp1dns9m9r4636aljgljqhjcpmi7a")))

(define-public crate-sprint-0.3.0 (c (n "sprint") (v "0.3.0") (d (list (d (n "bunt") (r "^0.2.8") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "shlex") (r "^1.2.0") (d #t) (k 0)))) (h "1r1zh7m690wc734s6lhiyq4854q27x34xjacim3i8rc9dv5jzl2l")))

(define-public crate-sprint-0.4.0 (c (n "sprint") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.77") (d #t) (k 0)) (d (n "bunt") (r "^0.2.8") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "shlex") (r "^1.2.0") (d #t) (k 0)))) (h "054b25gnc46iph6pjgkij81gqmqr132fjl5cjlniffw0gbqf3r7w")))

(define-public crate-sprint-0.5.0 (c (n "sprint") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bunt") (r "^0.2.8") (d #t) (k 0)) (d (n "clap") (r "^4.4.13") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "shlex") (r "^1.2.0") (d #t) (k 0)))) (h "13r7ir2bki1n9q18z9q579nz2rsq5mhh2i7ni5lxfpdcjr7vb964")))

(define-public crate-sprint-0.6.0 (c (n "sprint") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bunt") (r "^0.2.8") (d #t) (k 0)) (d (n "clap") (r "^4.4.13") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "shlex") (r "^1.2.0") (d #t) (k 0)))) (h "1l1ln8if2m971276rq6jasscxda3p0jvj0rrgp3pm60mf0pzk3n6")))

