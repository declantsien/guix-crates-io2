(define-module (crates-io sp ri springql-test-logger) #:use-module (crates-io))

(define-public crate-springql-test-logger-0.1.0 (c (n "springql-test-logger") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0h9i47mnl9p09vmlg7wppx63z1v85rrzcc62jgympypvcv7x7wxr")))

(define-public crate-springql-test-logger-0.2.0 (c (n "springql-test-logger") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log-panics") (r "^2.0") (d #t) (k 0)))) (h "078jmzxxhfigyf8f25hpwh1pzlxr5z2xfam0zjf10mckmx41r2sr")))

(define-public crate-springql-test-logger-0.4.0 (c (n "springql-test-logger") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log-panics") (r "^2.0") (d #t) (k 0)))) (h "0zk82wppfkr06r20k8v118xq8hjfinz76ggbpmnqw28hawa9dmab")))

