(define-module (crates-io sp ri sprint-dir) #:use-module (crates-io))

(define-public crate-sprint-dir-0.0.1 (c (n "sprint-dir") (v "0.0.1") (d (list (d (n "bytemuck") (r "^1.2") (d #t) (k 0)) (d (n "index-ext") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.1") (d #t) (k 2)))) (h "13cd1bacnvdzqh6gr5l8maq41rnfp5a358zv1wrlj4aa943dxyrr")))

