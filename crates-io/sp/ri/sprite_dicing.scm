(define-module (crates-io sp ri sprite_dicing) #:use-module (crates-io))

(define-public crate-sprite_dicing-0.1.0 (c (n "sprite_dicing") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.19") (d #t) (k 2)))) (h "0hjhvany099smkgh3jjm74ycmc8sp30h14zkg907pjdbg7pd2fbb")))

(define-public crate-sprite_dicing-0.1.1 (c (n "sprite_dicing") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.19") (d #t) (k 2)))) (h "17nclf62j3hqmq2xniw1ydzir1sjvr5gypzbkkqnaq24jsdikj9b")))

(define-public crate-sprite_dicing-0.1.2 (c (n "sprite_dicing") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.19") (d #t) (k 2)))) (h "14mpn2124bjari1ifd2nm5nc7qg68g4maazjpa5zayff43rb77jb")))

