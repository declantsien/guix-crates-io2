(define-module (crates-io sp ri springtime-migrate-refinery-macros) #:use-module (crates-io))

(define-public crate-springtime-migrate-refinery-macros-0.1.0 (c (n "springtime-migrate-refinery-macros") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "refinery-core") (r "^0.8.7") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "05d7j4142jfnfmsc94nfzw16r4nb2ipny7rdzkgp86cw2wfgjxl0") (r "1.58")))

(define-public crate-springtime-migrate-refinery-macros-0.1.1 (c (n "springtime-migrate-refinery-macros") (v "0.1.1") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "refinery-core") (r "^0.8.11") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0mjnlv3hjz34b2a3ymv7cxa0l6p04hicpj1hlrdyl05bfzb03hjr") (r "1.58")))

