(define-module (crates-io sp ri spritebot_storage) #:use-module (crates-io))

(define-public crate-spritebot_storage-0.1.0 (c (n "spritebot_storage") (v "0.1.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "vfs") (r "^0.10.0") (d #t) (k 0)))) (h "1vg6anvdv519c3ipp5yva8px9ygm210asfhin7ld0j35kb8zqjzi")))

(define-public crate-spritebot_storage-0.1.1 (c (n "spritebot_storage") (v "0.1.1") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "vfs") (r "^0.10.0") (d #t) (k 0)))) (h "0fbz3ydh5j03vl1ixfmgik61s40iyma8mi7fkqsjzknh840689cq")))

(define-public crate-spritebot_storage-0.2.0 (c (n "spritebot_storage") (v "0.2.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "vfs") (r "^0.10.0") (d #t) (k 0)))) (h "08w5iy2wzvvmfdqhczjlpzr5nc6p9h0r72xmcmw5hn6q4dygfd69")))

