(define-module (crates-io sp ri sprity-aseprite) #:use-module (crates-io))

(define-public crate-sprity-aseprite-0.1.0 (c (n "sprity-aseprite") (v "0.1.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1li370vidn4phc5wvc6pdlflh8g79p293x1ijnf8xlxz1jyzrmdk")))

(define-public crate-sprity-aseprite-0.1.1 (c (n "sprity-aseprite") (v "0.1.1") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1mpcckdv5bdq3gw6k5z7mw3rjvqqqimcip4s78xw29ci8c6vkhgz")))

(define-public crate-sprity-aseprite-0.1.2 (c (n "sprity-aseprite") (v "0.1.2") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lccmdh0rbk3cklvyhc075mb7cvv60j0a497xqyjzgs93a5c94jq")))

(define-public crate-sprity-aseprite-0.2.0 (c (n "sprity-aseprite") (v "0.2.0") (h "1psvcb8162ak6fyj9b7dkg1z776zifxin14n7hqjlhm5vkigrx0c")))

