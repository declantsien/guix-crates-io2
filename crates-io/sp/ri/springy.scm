(define-module (crates-io sp ri springy) #:use-module (crates-io))

(define-public crate-springy-0.1.0 (c (n "springy") (v "0.1.0") (d (list (d (n "bevy") (r "^0.8") (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "bevy-inspector-egui") (r "^0.13") (d #t) (k 0)) (d (n "bevy_editor_pls") (r "^0.1") (d #t) (k 2)) (d (n "bevy_rapier2d") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.17") (o #t) (d #t) (k 0)))) (h "0bhpg62w1j6hjkw1zfi7c7731f9bqmpw6ppnc60997rm58m4h8yk") (f (quote (("rapier3d" "bevy_rapier3d") ("rapier2d" "bevy_rapier2d"))))))

