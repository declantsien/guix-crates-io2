(define-module (crates-io sp ri sprintf) #:use-module (crates-io))

(define-public crate-sprintf-0.1.0 (c (n "sprintf") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1sp0l63xfq2nf99s7fy4fba3m7mc2dn64gcfm8qgx95xnl8w39ck")))

(define-public crate-sprintf-0.1.1 (c (n "sprintf") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1shczjdwyyfwjldkhn1xc07bzybyq52mygx4vqjh0kj4vw52f33y")))

(define-public crate-sprintf-0.1.2 (c (n "sprintf") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0h4388f2xjfsjsq8q4jfyq19ym537f4qybs4k05cg4c6dhkbjb08")))

(define-public crate-sprintf-0.1.3 (c (n "sprintf") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1vws2pq8ll2r5s0yjrnhr15hl2bh13919jcdcwizdcix2wgpmrnw")))

(define-public crate-sprintf-0.1.4 (c (n "sprintf") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0n6v93ii8h7s8lnqzn1cb5jmd3k1n7kr8w32gz2yf1halajxw33c")))

(define-public crate-sprintf-0.2.0 (c (n "sprintf") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0d3s54jn88p3z2plmrdjxadlzcg9rlcrzxwx9h9imfbkn6axdx1s")))

(define-public crate-sprintf-0.2.1 (c (n "sprintf") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0fdxfl8pyrylsi7ipdvr51ni1iwxzfliji9g21nrxzjd36srr0gj")))

