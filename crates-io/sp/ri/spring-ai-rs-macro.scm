(define-module (crates-io sp ri spring-ai-rs-macro) #:use-module (crates-io))

(define-public crate-spring-ai-rs-macro-0.1.0 (c (n "spring-ai-rs-macro") (v "0.1.0") (d (list (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("full"))) (d #t) (k 0)))) (h "0nkvwfy31dn8srvwn1dawffh4h0fsp8dpaqy8pngqm3k306ps50m")))

(define-public crate-spring-ai-rs-macro-0.1.1 (c (n "spring-ai-rs-macro") (v "0.1.1") (d (list (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pas523f3wlnwwn5hbzhqnrvx9zcdi18a6jmkpvi5w3zzsbwmp71")))

