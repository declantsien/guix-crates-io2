(define-module (crates-io sp ri spring83) #:use-module (crates-io))

(define-public crate-spring83-0.0.1 (c (n "spring83") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1snar54aysfbmmf4nmkfwjsn0pzzn3cxvjr3vn0a8vhflar08x0x")))

