(define-module (crates-io sp ri spriteron) #:use-module (crates-io))

(define-public crate-spriteron-0.1.0 (c (n "spriteron") (v "0.1.0") (d (list (d (n "png") (r "^0.14") (d #t) (k 0)) (d (n "ron") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "163l8ipbgmqlq972jb9q81w9hzp06bk4fxjlnnkldd9hgy1gn6bp")))

