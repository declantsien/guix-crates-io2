(define-module (crates-io sp -r sp-ropey) #:use-module (crates-io))

(define-public crate-sp-ropey-0.1.0 (c (n "sp-ropey") (v "0.1.0") (d (list (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "sp-std") (r "^3") (k 0)) (d (n "unicode-segmentation") (r "^1.3") (d #t) (k 2)))) (h "0v4q2i18xbpa66rdjkz1rigzml3n7yjgszazbrix2alxg4lll5qk")))

(define-public crate-sp-ropey-0.2.0 (c (n "sp-ropey") (v "0.2.0") (d (list (d (n "proptest") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.3") (d #t) (k 2)))) (h "1kjyvx7a342wla1jjwgvficilyp34l45g88p3hx0jdcvinnp4rkg")))

