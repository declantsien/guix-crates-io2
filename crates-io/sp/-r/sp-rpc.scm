(define-module (crates-io sp -r sp-rpc) #:use-module (crates-io))

(define-public crate-sp-rpc-2.0.0-alpha.2 (c (n "sp-rpc") (v "2.0.0-alpha.2") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-alpha.2") (d #t) (k 0)))) (h "1dyglr3wszzszav3mgplqvs2vwcdy5dnjrvbndlhyixf8kp7d7w6")))

(define-public crate-sp-rpc-2.0.0-alpha.3 (c (n "sp-rpc") (v "2.0.0-alpha.3") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-alpha.2") (d #t) (k 0)))) (h "1jd05y7vmf0s57sllws2s34v8dljqyzbjgbb9y99c2h1mw5qmy1s")))

(define-public crate-sp-rpc-2.0.0-alpha.5 (c (n "sp-rpc") (v "2.0.0-alpha.5") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-alpha.5") (d #t) (k 0)))) (h "1xg1yigik453w6bxj0r2mi14pb3k9ac727kammhspgr27r2qmjb9")))

(define-public crate-sp-rpc-2.0.0-alpha.6 (c (n "sp-rpc") (v "2.0.0-alpha.6") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-alpha.6") (d #t) (k 0)))) (h "0fp5z9wlmj59w8v2dk4y7dy8rznc0k3412s0xipgj5qkyn7aaacx")))

(define-public crate-sp-rpc-2.0.0-alpha.7 (c (n "sp-rpc") (v "2.0.0-alpha.7") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-alpha.7") (d #t) (k 0)))) (h "0d634f5skpig3hvwkxi3nb5966gp1sfwwi78dhsr0ickd8gsxl3v")))

(define-public crate-sp-rpc-2.0.0-alpha.8 (c (n "sp-rpc") (v "2.0.0-alpha.8") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-alpha.8") (d #t) (k 0)))) (h "003m4mc956nlp70azlqwn938ikfdrrz26glbkz77f6dahxpb5c4k")))

(define-public crate-sp-rpc-2.0.0-rc1 (c (n "sp-rpc") (v "2.0.0-rc1") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-rc1") (d #t) (k 0)))) (h "0jfmyhlhqkyydskrfyhqm9mzj9n8769s2zag2xkqmfn4awx275r0")))

(define-public crate-sp-rpc-2.0.0-rc2 (c (n "sp-rpc") (v "2.0.0-rc2") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-rc2") (d #t) (k 0)))) (h "0l6mc5l7kvim4x8zj9sl2qvvl0g30r7mv5zz0x0ficqac63yr2cv")))

(define-public crate-sp-rpc-2.0.0-rc3 (c (n "sp-rpc") (v "2.0.0-rc3") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-rc3") (d #t) (k 0)))) (h "0la6ln78xhdls7r048lgr45yiwsha0xvr2b1pkjc08r0avlyxgf1")))

(define-public crate-sp-rpc-2.0.0-rc4 (c (n "sp-rpc") (v "2.0.0-rc4") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-rc4") (d #t) (k 0)))) (h "01h5v1xcnrmb47k3asf2kg5yd1ssjbfggqndw28y8qkrw8jv7cg3")))

(define-public crate-sp-rpc-2.0.0-rc5 (c (n "sp-rpc") (v "2.0.0-rc5") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-rc5") (d #t) (k 0)))) (h "1kpfb3hqpzk4j9g5fjflx1wn07hw2wkf432b2r5giiwyf12a97vl")))

(define-public crate-sp-rpc-2.0.0-rc6 (c (n "sp-rpc") (v "2.0.0-rc6") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-rc6") (d #t) (k 0)))) (h "0bmjqy7k42s01ayvz4d5jifd4n4pj39aknmfpshm16pvvgqhfp3z")))

(define-public crate-sp-rpc-2.0.0 (c (n "sp-rpc") (v "2.0.0") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0") (d #t) (k 0)))) (h "04d91pa2b4fm8sq793fgy6gc40x4h466m2gkrmniwhj29f7ngip5")))

(define-public crate-sp-rpc-2.0.1 (c (n "sp-rpc") (v "2.0.1") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0") (d #t) (k 0)))) (h "1yawc3g9zm1x5igghxzlhky58fbaya958m4y65gjrzqiwf52p7q0")))

(define-public crate-sp-rpc-3.0.0 (c (n "sp-rpc") (v "3.0.0") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-core") (r "^3.0.0") (d #t) (k 0)))) (h "067fbx1ibww704frv4dk08g28iazi54b055rff8484axcpnv3rd2")))

(define-public crate-sp-rpc-5.0.0 (c (n "sp-rpc") (v "5.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-core") (r "^5.0.0") (d #t) (k 0)))) (h "1p1d0yj3klcwx4hpkxpfbzp6x60vsnf1zinqc3q86k2wsypn8f4f")))

(define-public crate-sp-rpc-6.0.0 (c (n "sp-rpc") (v "6.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-core") (r "^6.0.0") (d #t) (k 0)))) (h "1sl6dqq6416zx8gfrdjwqhx8m5zv0yy2n94hgzgxi014fzrasv6c")))

(define-public crate-sp-rpc-7.0.0 (c (n "sp-rpc") (v "7.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "sp-core") (r "^8.0.0") (d #t) (k 0)))) (h "1p79x5c13kz1m5bbq1m9r0gpmhhnv6dz90zif0rx10kb4ijjs7ca")))

(define-public crate-sp-rpc-8.0.0 (c (n "sp-rpc") (v "8.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "sp-core") (r "^9.0.0") (d #t) (k 0)))) (h "1nz7f1wqd7w17cj59hc9wcby1f9i18cbiiljhwjvc0rp5q1wxhji")))

(define-public crate-sp-rpc-9.0.0 (c (n "sp-rpc") (v "9.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "sp-core") (r "^10.0.0") (d #t) (k 0)))) (h "1zjvb2fy5ph5dnda8y964h2ywm64s86svpmjgmpjmf9jv83gby9p")))

(define-public crate-sp-rpc-10.0.0 (c (n "sp-rpc") (v "10.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "sp-core") (r "^11.0.0") (d #t) (k 0)))) (h "03ji1da196r3ch7jdwkd2crdj5v3yngpgd52g3ljqcmy9pz8y42c")))

(define-public crate-sp-rpc-11.0.0 (c (n "sp-rpc") (v "11.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "sp-core") (r "^12.0.0") (d #t) (k 0)))) (h "10a1abgmj8rf410pxak2yf8qql6pd0vmgdg210ibbzjsyg39pslk")))

(define-public crate-sp-rpc-12.0.0 (c (n "sp-rpc") (v "12.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "sp-core") (r "^13.0.0") (d #t) (k 0)))) (h "1251vy941zla4qxxhncwnbzinlsms31fc02yj74jc9rxiq2c8iff")))

(define-public crate-sp-rpc-13.0.0 (c (n "sp-rpc") (v "13.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "sp-core") (r "^14.0.0") (d #t) (k 0)))) (h "095pd3x9l54qrw9zd21rpp34dmq1xl0q955whwgp9h0mg9hsc4yh")))

(define-public crate-sp-rpc-14.0.0 (c (n "sp-rpc") (v "14.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "sp-core") (r "^15.0.0") (d #t) (k 0)))) (h "11s9dy0a4inrdk5jcf7ad0rqlijapkvdql5qim040az33jy338ar")))

(define-public crate-sp-rpc-15.0.0 (c (n "sp-rpc") (v "15.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "sp-core") (r "^16.0.0") (d #t) (k 0)))) (h "1wgzyqpsnyvj66ramk4y0iycawwqdjsclishd4kxbxh46hmdlsh2")))

(define-public crate-sp-rpc-16.0.0 (c (n "sp-rpc") (v "16.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "sp-core") (r "^17.0.0") (d #t) (k 0)))) (h "1319vpfcaszq06hlcmmc9j3a0iw3ncxcmgl33mgg156nvb1bdsmz")))

(define-public crate-sp-rpc-17.0.0 (c (n "sp-rpc") (v "17.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "sp-core") (r "^18.0.0") (d #t) (k 0)))) (h "16yxl630hcx7sv1lwib8wh3456qraxmlwznx37fpiw7r55f9b2vv")))

(define-public crate-sp-rpc-18.0.0 (c (n "sp-rpc") (v "18.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "sp-core") (r "^19.0.0") (d #t) (k 0)))) (h "1ih0612x4mjbq4n1hb175c040hznfpz60dp7dfq9ldi7nrd94hn1")))

(define-public crate-sp-rpc-19.0.0 (c (n "sp-rpc") (v "19.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "sp-core") (r "^20.0.0") (d #t) (k 0)))) (h "08npyipvq3ahdxx8w8igba77ph03zp126fga83ayr3hmfq48wmc2")))

(define-public crate-sp-rpc-19.1.0-dev.2 (c (n "sp-rpc") (v "19.1.0-dev.2") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "sp-core") (r "^21.1.0-dev.2") (d #t) (k 0)))) (h "08ar6xxm1hin1rf2n2k9sbnql82p6rkpkqqv9wringg1drd601sd")))

(define-public crate-sp-rpc-19.1.0-dev.3 (c (n "sp-rpc") (v "19.1.0-dev.3") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "sp-core") (r "^21.1.0-dev.3") (d #t) (k 0)))) (h "1nbdfw90gg4g4vwiama7cc948iy8c5hl5xm53q1s1snqsfks1fja")))

(define-public crate-sp-rpc-19.1.0-dev.4 (c (n "sp-rpc") (v "19.1.0-dev.4") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "sp-core") (r "^21.1.0-dev.4") (d #t) (k 0)))) (h "1c8vn5ijs0x5wcagscvyaklcxziy68c563nwih4pgn92sk6bzzgz")))

(define-public crate-sp-rpc-19.1.0-dev.5 (c (n "sp-rpc") (v "19.1.0-dev.5") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "sp-core") (r "=21.1.0-dev.5") (d #t) (k 0)))) (h "1rzpf4s6wyf6r2pqm72rxgm6bjnk5xglz811mz7am18yl0s0pb39")))

(define-public crate-sp-rpc-19.1.0-dev.6 (c (n "sp-rpc") (v "19.1.0-dev.6") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "sp-core") (r "=21.1.0-dev.6") (d #t) (k 0)))) (h "0mbw31s2af0sh47ayvfddkd18h82nx17xrn29zw6gaxc656754vk")))

(define-public crate-sp-rpc-20.0.0 (c (n "sp-rpc") (v "20.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "sp-core") (r "^22.0.0") (d #t) (k 0)))) (h "1wskbbqa4d5ccjq5260566r1mcg36ch2vz7jppf26prdmqw7lyg0")))

(define-public crate-sp-rpc-21.0.0 (c (n "sp-rpc") (v "21.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)) (d (n "sp-core") (r "^23.0.0") (d #t) (k 0)))) (h "0bm14ghb7zzjnzsfvxarq5dvkpjl6c7p4wd52bpawa0jsl6h4a2q")))

(define-public crate-sp-rpc-22.0.0-dev.1 (c (n "sp-rpc") (v "22.0.0-dev.1") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)) (d (n "sp-core") (r "=24.0.0-dev.1") (d #t) (k 0)))) (h "0vlqadzzj8g4h8q3iw8in581lhw6p6dq0zpbnrczkhvvdz4g3m5z")))

(define-public crate-sp-rpc-22.0.0 (c (n "sp-rpc") (v "22.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)) (d (n "sp-core") (r "^24.0.0") (d #t) (k 0)))) (h "0gfbanlyfq63cx3d8khkm0vnchr7vb9z0jq5jc5gfhv01ap3918x")))

(define-public crate-sp-rpc-23.0.0 (c (n "sp-rpc") (v "23.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)) (d (n "sp-core") (r "^25.0.0") (d #t) (k 0)))) (h "0siyb9wx7w3pjv5qxwypx203npwvamrz61gm5fmbnm0zjbm7z1ji")))

(define-public crate-sp-rpc-24.0.0 (c (n "sp-rpc") (v "24.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)) (d (n "sp-core") (r "^26.0.0") (d #t) (k 0)))) (h "0s194z2gv7gyblhcff7pmv393q28pd9r2cr5r9xsx3fdp6crqn5x")))

(define-public crate-sp-rpc-25.0.0 (c (n "sp-rpc") (v "25.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)) (d (n "sp-core") (r "^27.0.0") (d #t) (k 0)))) (h "1miqv64djka5iglgwn4v3ywma0lmlsbba0pldfnz9p93xndq289r")))

(define-public crate-sp-rpc-26.0.0 (c (n "sp-rpc") (v "26.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 2)) (d (n "sp-core") (r "^28.0.0") (d #t) (k 0)))) (h "1yz6h1zpqlc7fdk7gynjf5v1l02z5vzfqci9s8krcmqbcv3z4mca")))

(define-public crate-sp-rpc-27.0.0 (c (n "sp-rpc") (v "27.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 2)) (d (n "sp-core") (r "^29.0.0") (d #t) (k 0)))) (h "05jd9y491xf457yyd69ifqhyf9g5qi401afc84b8pn6xwwzvgx4s")))

(define-public crate-sp-rpc-28.0.0 (c (n "sp-rpc") (v "28.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)) (d (n "sp-core") (r "^30.0.0") (d #t) (k 0)))) (h "1pc2b3r3xvn1yrnfwsdiply5jn7sqf58nfs9a2avgryhq57r5agz")))

(define-public crate-sp-rpc-29.0.0 (c (n "sp-rpc") (v "29.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)) (d (n "sp-core") (r "^31.0.0") (d #t) (k 0)))) (h "0725zhc06hvbqdkzwx7r1j4jm4gahblgazb3ikyll82wgxs50rmj")))

(define-public crate-sp-rpc-30.0.0 (c (n "sp-rpc") (v "30.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)) (d (n "sp-core") (r "^32.0.0") (d #t) (k 0)))) (h "0iqqzmswffwq7w5zni07fhz39a09siyligddc61rwv4xmcy4q42i")))

(define-public crate-sp-rpc-31.0.0 (c (n "sp-rpc") (v "31.0.0") (d (list (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)) (d (n "sp-core") (r "^33.0.0") (d #t) (k 0)))) (h "1ssy41pciq8704chg6b53pw6rvmj6a79gzxd4k38z27f8chkayvg")))

