(define-module (crates-io sp li splitterrust_db) #:use-module (crates-io))

(define-public crate-splitterrust_db-0.1.0 (c (n "splitterrust_db") (v "0.1.0") (d (list (d (n "diesel") (r "^1.0") (f (quote ("postgres" "r2d2"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hvl8aliqmvjll4k8g6s4bg1l2fp5gz7rgq33s0bzj1mzjhcabr8")))

