(define-module (crates-io sp li splits_iter) #:use-module (crates-io))

(define-public crate-splits_iter-1.0.0 (c (n "splits_iter") (v "1.0.0") (h "14id860k6bi2ff3l4a0az0l90mng628i9qqrmvklpdrs4mn7ck38")))

(define-public crate-splits_iter-1.1.0 (c (n "splits_iter") (v "1.1.0") (h "0rrvdsw3zzgybgsz5a43v89mkj4nwc4mbs44rjimpn39fd2vc5ij")))

