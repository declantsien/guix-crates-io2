(define-module (crates-io sp li split-csv) #:use-module (crates-io))

(define-public crate-split-csv-1.0.0-alpha.0 (c (n "split-csv") (v "1.0.0-alpha.0") (d (list (d (n "clap") (r "^3.0.0-rc.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "00s0xpkyvvxppv6dr3h0593ad83xzlidqnybqwppyx9n5br4v9p9") (y #t) (r "1.57")))

(define-public crate-split-csv-0.0.1 (c (n "split-csv") (v "0.0.1") (d (list (d (n "clap") (r "^3.0.0-rc.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "10h6wjrq49nsiqigq98gfa63sk2i88gbvdlbx1n82gv76ja36481") (y #t) (r "1.57")))

(define-public crate-split-csv-0.0.2 (c (n "split-csv") (v "0.0.2") (d (list (d (n "clap") (r "^3.0.0-rc.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "indexed_file") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "03mda00p0p6pd47561lc9sfri8jh5wkzn9z3bzghgns5718bvzxs") (y #t) (r "1.57")))

(define-public crate-split-csv-0.0.3 (c (n "split-csv") (v "0.0.3") (d (list (d (n "clap") (r "^3.0.0-rc.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "indexed_file") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "1pk2sv3qgsilsic5a4ck0gr4xb5ih77937y72nhi6zrbv0j0ngxb") (y #t) (r "1.57")))

(define-public crate-split-csv-0.0.4 (c (n "split-csv") (v "0.0.4") (d (list (d (n "clap") (r "^3.0.0-rc.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "indexed_file") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "0ggb6ba4vwjvs43f406q8q2pwy89dq300iq2nm31w2vapj1jn4r5") (y #t) (r "1.57")))

(define-public crate-split-csv-0.0.6 (c (n "split-csv") (v "0.0.6") (d (list (d (n "clap") (r "^3.0.0-rc.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "indexed_file") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "00gjqn524vw21ww2xvszsf2lcg74dpzy8ckrsp6x8zssfnvjpp4c") (y #t) (r "1.57")))

(define-public crate-split-csv-0.0.7 (c (n "split-csv") (v "0.0.7") (d (list (d (n "clap") (r "^3.0.0-rc.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "indexed_file") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "0gxf9xjxxzlpak4sgpnxgmnhx1aivwziqbs8q949ix31hrbbvr8c") (y #t) (r "1.57")))

(define-public crate-split-csv-0.0.8 (c (n "split-csv") (v "0.0.8") (d (list (d (n "clap") (r "^3.0.0-rc.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "indexed_file") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "0z7h78lf3px1gwkyn64p0j0nd3s4xap2xd4p5mz4bdcm4a6ndsf0") (y #t) (r "1.57")))

(define-public crate-split-csv-0.0.9 (c (n "split-csv") (v "0.0.9") (d (list (d (n "clap") (r "^3.0.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "indexed_file") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "14ksx3j8l1sy2iflf4q3blw7lmppnbg3mslx5bi8gddk4r67q68k") (r "1.57")))

(define-public crate-split-csv-0.1.0 (c (n "split-csv") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "indexed_file") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0plb0b5gqy8hfy1mqhwfr9mi3jx0y5y2lyva0qsad20qjwzraqsv") (y #t) (r "1.57")))

(define-public crate-split-csv-0.1.1 (c (n "split-csv") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "indexed_file") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1i0jyc9i3jp8hvlxk6scrgkyb5wfv99sm4mdgnzyjqk0819x3fym") (y #t) (r "1.57")))

(define-public crate-split-csv-0.1.2 (c (n "split-csv") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "indexed_file") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0c02d6i71lwciiyhqlbd0jyiqj2vixyddkc4yv77mqfsayzxq200") (r "1.57")))

