(define-module (crates-io sp li splitterrust_discord) #:use-module (crates-io))

(define-public crate-splitterrust_discord-0.1.0 (c (n "splitterrust_discord") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serenity") (r "^0.7") (f (quote ("framework" "standard_framework"))) (d #t) (k 0)) (d (n "splitterrust_db") (r "^0.1.0") (d #t) (k 0)))) (h "1zbk5kz2q4ygcdn6h1yvk84nyw313x5b049q50mik3msiqxbggpa")))

