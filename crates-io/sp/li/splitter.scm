(define-module (crates-io sp li splitter) #:use-module (crates-io))

(define-public crate-splitter-0.1.0 (c (n "splitter") (v "0.1.0") (d (list (d (n "splitter-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "19dn8myywqh57sbx5bqjirh3jgrjmlp5zb8bsjzmi2mkw3aqzcry") (f (quote (("std") ("infos") ("impls") ("full" "std" "impls" "infos" "derive")))) (s 2) (e (quote (("derive" "dep:splitter-derive"))))))

(define-public crate-splitter-0.1.1 (c (n "splitter") (v "0.1.1") (d (list (d (n "splitter-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0w4vafj0asrjvs330g296kjrm93180rpm3xx40q7rfycwm7maa7g") (f (quote (("std") ("infos") ("impls") ("full" "std" "impls" "infos" "derive")))) (s 2) (e (quote (("derive" "dep:splitter-derive"))))))

(define-public crate-splitter-0.1.2 (c (n "splitter") (v "0.1.2") (d (list (d (n "splitter-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1bvsna7w2iahhb5gw164zyfs37wz3g534nynrp0qm72kf85w5q31") (f (quote (("std") ("infos") ("impls") ("full" "std" "impls" "infos" "derive")))) (s 2) (e (quote (("derive" "dep:splitter-derive"))))))

(define-public crate-splitter-0.1.3 (c (n "splitter") (v "0.1.3") (d (list (d (n "splitter-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "08nv68cmr0rpw6m335v1dgiv3gaz18kxv7lkn1adda49jm34h0lg") (f (quote (("std") ("infos") ("impls") ("full" "std" "impls" "infos" "derive")))) (s 2) (e (quote (("derive" "dep:splitter-derive"))))))

(define-public crate-splitter-0.2.0 (c (n "splitter") (v "0.2.0") (d (list (d (n "splitter-derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1s3hmvmkd7h2a4sf93q8bqrmr15ggdxl5d7bxdsijs26y4gicp4g") (f (quote (("std") ("infos") ("impls") ("full" "std" "impls" "infos" "derive")))) (s 2) (e (quote (("derive" "dep:splitter-derive"))))))

(define-public crate-splitter-0.2.1 (c (n "splitter") (v "0.2.1") (d (list (d (n "splitter-derive") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "04kkhvrbrms7551lvs9vlvxr6rcc2jhk94wmskn2cr6gxssbazxa") (f (quote (("std") ("infos") ("impls") ("full" "std" "impls" "infos" "derive")))) (y #t) (s 2) (e (quote (("derive" "dep:splitter-derive"))))))

(define-public crate-splitter-0.2.2 (c (n "splitter") (v "0.2.2") (d (list (d (n "splitter-derive") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "1mp7vsx6pdz84d68kf7g38d6mb73qbzm626s1mghjdd2d50lb6s2") (f (quote (("std") ("infos") ("impls") ("full" "std" "impls" "infos" "derive")))) (s 2) (e (quote (("derive" "dep:splitter-derive"))))))

(define-public crate-splitter-0.2.3 (c (n "splitter") (v "0.2.3") (d (list (d (n "splitter-derive") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "01zxhdssk6axiz6ijx2lqhm7ili7nnnfiw9flm6alcsc4mdyf6g4") (f (quote (("std") ("infos") ("impls") ("full" "std" "impls" "infos" "derive")))) (s 2) (e (quote (("derive" "dep:splitter-derive"))))))

(define-public crate-splitter-0.2.4 (c (n "splitter") (v "0.2.4") (d (list (d (n "splitter-derive") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "1qrwla18jljkac4gksmlfj3a280fac7hqchyh61qvszqihxmh428") (f (quote (("std") ("infos") ("impls") ("full" "std" "impls" "infos" "derive")))) (s 2) (e (quote (("derive" "dep:splitter-derive"))))))

(define-public crate-splitter-0.2.5 (c (n "splitter") (v "0.2.5") (d (list (d (n "splitter-derive") (r "^0.1.6") (o #t) (d #t) (k 0)))) (h "0lffpzh6ci7hf1hwz7h8i0ip09plnrfcsnnms19krv8kf3d2svrg") (f (quote (("std") ("infos") ("impls") ("full" "std" "impls" "infos" "derive")))) (s 2) (e (quote (("derive" "dep:splitter-derive"))))))

