(define-module (crates-io sp li split_ext) #:use-module (crates-io))

(define-public crate-split_ext-0.1.0 (c (n "split_ext") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 2)) (d (n "regex") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "rental") (r "^0.5.3") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1") (d #t) (k 0)))) (h "15fiwxirczc5inqp1wh3fhj77x708w6lzhjrpdjhnyhn07pb80qq") (f (quote (("default" "regex"))))))

(define-public crate-split_ext-0.1.1 (c (n "split_ext") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.3") (d #t) (k 2)) (d (n "regex") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "rental") (r "^0.5.3") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.1") (d #t) (k 0)))) (h "1nryxz16akr7bd2kdgrv1vkr8bzfbr9ryp7g19vka6cacykibxk9") (f (quote (("default" "regex"))))))

