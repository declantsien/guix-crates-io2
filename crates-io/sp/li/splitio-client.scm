(define-module (crates-io sp li splitio-client) #:use-module (crates-io))

(define-public crate-splitio-client-0.1.0 (c (n "splitio-client") (v "0.1.0") (h "1gvlnplq7aw2sr4kpc3yb6bknz7s2m0wxgf08bx67sba5fh45cg7")))

(define-public crate-splitio-client-0.1.1 (c (n "splitio-client") (v "0.1.1") (h "1r11jgljg81igz3c6prnyvyll19fl44i3pnkvl573hklhvi5ifrw")))

