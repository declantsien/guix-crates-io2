(define-module (crates-io sp li splix) #:use-module (crates-io))

(define-public crate-splix-0.1.0 (c (n "splix") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)))) (h "05wa1qk9f8194ddns4mfsr5wbpjv781xzr2hhj6dkq04cw8lv5z3")))

(define-public crate-splix-0.1.1 (c (n "splix") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)))) (h "1ks4q4vg0r4aw3hhn90906krzbsm0xv5fajv0b6smb2ss4jsg8hq")))

(define-public crate-splix-0.2.0 (c (n "splix") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "0ikxinv4ihm576mips3faxf4kjnw91xkn1kwwjknj4f8ik834lbh")))

(define-public crate-splix-0.2.1 (c (n "splix") (v "0.2.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "1q3canwf0pnv7gk0k7w0m9p7adqcc3ljxy78kc7vk7a6ri9p57ix")))

