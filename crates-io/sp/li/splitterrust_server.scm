(define-module (crates-io sp li splitterrust_server) #:use-module (crates-io))

(define-public crate-splitterrust_server-0.1.0 (c (n "splitterrust_server") (v "0.1.0") (d (list (d (n "actix-web") (r "^1.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "splitterrust_db") (r "^0.1.0") (d #t) (k 0)))) (h "1iw6ipncx1xar3gplfyacgq8w0cgnr19pyr0y0ki07k4j9krg8zr")))

