(define-module (crates-io sp li splitter-derive) #:use-module (crates-io))

(define-public crate-splitter-derive-0.1.0 (c (n "splitter-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gn1fg51pcb5wjgv83pmiwb7q908nmi4qhxly4ii11yhzhijh97w")))

(define-public crate-splitter-derive-0.1.1 (c (n "splitter-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fwasfnqxqzdmhl90kihqj92lfngfa0ws3749a6cgljw4lgxwk1l")))

(define-public crate-splitter-derive-0.1.2 (c (n "splitter-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1g5218hkjqj2d5mbccvfc1bmazzbf370c9h7gl69hr9qh12m8229") (y #t)))

(define-public crate-splitter-derive-0.1.3 (c (n "splitter-derive") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0zbj0rlc0vfxs4aav5kgv48lsp50si029l7li67f6wsrbprkgmyz") (y #t)))

(define-public crate-splitter-derive-0.1.4 (c (n "splitter-derive") (v "0.1.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fb1na2wfgcabqr1hhy7mzdv0xh266rdpcy0f8zh66yxwwac39h3")))

(define-public crate-splitter-derive-0.1.5 (c (n "splitter-derive") (v "0.1.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pgq83dbmzgnn3481hyz55cp24nv68408xwnylwk6162fm2lhcb0")))

(define-public crate-splitter-derive-0.1.6 (c (n "splitter-derive") (v "0.1.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1l7jbngdws7jya5dsvzdddqv58d2h0ha1la058ac9dcyx5my62vq")))

