(define-module (crates-io sp li split-tail-out) #:use-module (crates-io))

(define-public crate-split-tail-out-0.1.0 (c (n "split-tail-out") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)))) (h "1nrqnqmry9c6f8x0nz5sllfgpkfry961dch18fqcvnnciq3mi0kr")))

