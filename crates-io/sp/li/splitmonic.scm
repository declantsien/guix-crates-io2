(define-module (crates-io sp li splitmonic) #:use-module (crates-io))

(define-public crate-splitmonic-0.1.0 (c (n "splitmonic") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "splitmonic_bip39") (r "^1.0") (f (quote ("zeroize"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.2") (d #t) (k 0)))) (h "1kzy795d2w6smhxrgd344z2cza8r38by2f9lglrzgqfcnraznbv8")))

