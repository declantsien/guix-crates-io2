(define-module (crates-io sp li split_array) #:use-module (crates-io))

(define-public crate-split_array-0.1.0 (c (n "split_array") (v "0.1.0") (d (list (d (n "bool_traits") (r "^0.1.1") (d #t) (k 0)))) (h "01hdblryrr09l7i1qv1qm861q0xbk5xzi7fvclw8i8a8m6dm9p2k") (r "1.79.0")))

(define-public crate-split_array-0.1.1 (c (n "split_array") (v "0.1.1") (d (list (d (n "bool_traits") (r "^0.1.1") (d #t) (k 0)))) (h "0pwgjh8gaw6788hwg5d8gqrjwq33ssg9ha2s3np2riwgagiyzi33") (r "1.79.0")))

(define-public crate-split_array-0.2.0 (c (n "split_array") (v "0.2.0") (h "1y2hmfhvh2511dppcriiarcnbaa7407xx47apri05wncslwldnwx") (r "1.79.0")))

