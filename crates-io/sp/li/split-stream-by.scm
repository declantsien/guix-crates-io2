(define-module (crates-io sp li split-stream-by) #:use-module (crates-io))

(define-public crate-split-stream-by-0.0.0 (c (n "split-stream-by") (v "0.0.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "0gdak80fvhnjzfijciamhhmb53zfd3j887nnzj9z3y77liagzg3c")))

(define-public crate-split-stream-by-0.1.0 (c (n "split-stream-by") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1sz46z06gjzz8g25s00wcs3ldrcfvhw99fjc2cw1q5f58zqlx4vr")))

