(define-module (crates-io sp li splinify) #:use-module (crates-io))

(define-public crate-splinify-0.1.0 (c (n "splinify") (v "0.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "dierckx-sys") (r "^0.1") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "spliny") (r "^0.1") (d #t) (k 0)))) (h "1813b3aamfk3m1wcqlg8vfzh4nr38aig1m6z2pgyarj42cmjh86h")))

