(define-module (crates-io sp li splitv) #:use-module (crates-io))

(define-public crate-splitv-0.1.0 (c (n "splitv") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "1jszajvgxmg13hfgxdkipbwz14v1cah8a04v9sy18wzp8p5z3ggp")))

(define-public crate-splitv-0.1.1 (c (n "splitv") (v "0.1.1") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "1bg5ipz48wym62gmanzq2yh80dn41cni41l20phcswcgr2nk1nlj")))

(define-public crate-splitv-0.2.0 (c (n "splitv") (v "0.2.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "0z4xxm4bcmxbid6sznw94smiq3rbk6788a48ra1w1w573nbvndg5")))

(define-public crate-splitv-0.2.1 (c (n "splitv") (v "0.2.1") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "1wjsklyiykrpy1fv4dmh4cnmlfrq9c0x6gz0pgzd9v3vrk2ky917")))

