(define-module (crates-io sp li split_read) #:use-module (crates-io))

(define-public crate-split_read-0.1.0 (c (n "split_read") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0sxw5larynmvdb6g5y9zw12j1njykcs09szhvcmz9ihq140al4rj") (y #t)))

