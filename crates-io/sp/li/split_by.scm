(define-module (crates-io sp li split_by) #:use-module (crates-io))

(define-public crate-split_by-0.1.0 (c (n "split_by") (v "0.1.0") (d (list (d (n "aho-corasick") (r "^0.5") (d #t) (k 0)))) (h "1mp6m4mkdvqy9zhi2gw4wnmljvvky324sfmkqfbfr6syqiabpcd6")))

(define-public crate-split_by-0.2.0 (c (n "split_by") (v "0.2.0") (d (list (d (n "aho-corasick") (r "^0.5") (d #t) (k 0)))) (h "0f4gj7d5r2f58s9pqfbv4ba8wfdfk6288v2bmak67jqgza8r1j4p")))

(define-public crate-split_by-0.2.1 (c (n "split_by") (v "0.2.1") (d (list (d (n "aho-corasick") (r "^0.5") (d #t) (k 0)))) (h "0zmfdsppkk75rrraq7sk17vfgh1cfx823s5hlvbgqiw1wcdrq7fs")))

(define-public crate-split_by-0.2.2 (c (n "split_by") (v "0.2.2") (d (list (d (n "aho-corasick") (r "^0.6") (d #t) (k 0)))) (h "0qcnx1ky90ni8q0zfqrs7f1h1mijwj99ksy5y0m9vnb7ahzyd9ar")))

