(define-module (crates-io sp li splitp) #:use-module (crates-io))

(define-public crate-splitp-0.1.0 (c (n "splitp") (v "0.1.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "needletail") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q5i8y0gc1cvrxp4zykdpwabkfrv24n545gykdvpyjmqpcyyh6z1")))

