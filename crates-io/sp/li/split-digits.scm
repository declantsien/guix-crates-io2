(define-module (crates-io sp li split-digits) #:use-module (crates-io))

(define-public crate-split-digits-0.1.0 (c (n "split-digits") (v "0.1.0") (h "0avfcsnfgwsgkkzk66kpggmlwq2xgfymkz9mvp23v6b54617cj3c")))

(define-public crate-split-digits-0.1.1 (c (n "split-digits") (v "0.1.1") (h "1s3j6147nqbmh7l7zpwpf8vbv2mqc58s6n36r072w0invazvhhlf")))

(define-public crate-split-digits-0.2.0 (c (n "split-digits") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "047pz83h84bd3nnf0gx438jh4bd5rkib96zkc4iicna2yak7mn2d")))

(define-public crate-split-digits-0.2.1 (c (n "split-digits") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "1f0l9k65971lm1imbpvzljfbs3mfpjniwqvb8lzzv3nmnv7sv9y3")))

