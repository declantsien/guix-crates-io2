(define-module (crates-io sp li splitty) #:use-module (crates-io))

(define-public crate-splitty-0.1.0 (c (n "splitty") (v "0.1.0") (h "107ffm63gbpizpc9v0cm8kzm2kgcmhygfdam0af0w26li672k441")))

(define-public crate-splitty-1.0.0 (c (n "splitty") (v "1.0.0") (h "1akwihnn1cm9f5hbv8afjpm2inbnk2a5ymycajac9bi6rs20y1jk")))

(define-public crate-splitty-1.0.1 (c (n "splitty") (v "1.0.1") (h "0flwkhbjwms60bz5bh9xk3hxqmc2vnygdc1p44x2vp6mbfm6ibld")))

