(define-module (crates-io sp li split-yew) #:use-module (crates-io))

(define-public crate-split-yew-0.1.0 (c (n "split-yew") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1b8sh943w3wy00iqaawizswxxrbbkq5n18isgabxahl15hmx56r6")))

(define-public crate-split-yew-0.1.1 (c (n "split-yew") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "0lpz272niz8b8bg46qc4avz5ivf67x78y4zx93ig48ysy0vvrf27")))

(define-public crate-split-yew-0.1.2 (c (n "split-yew") (v "0.1.2") (d (list (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "yew") (r "^0.20") (d #t) (k 0)))) (h "1g4ql7616j1jwrs92jqafkn0pjdal8mxrskbpn5s4ma0cklpx2kn")))

(define-public crate-split-yew-0.2.0 (c (n "split-yew") (v "0.2.0") (d (list (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "yew") (r "^0.21") (d #t) (k 0)))) (h "1d1g6k06vx0d2z3rhwmqzf3ky9wi0fi7ad69pwr7l7ldz0zi5wmx")))

(define-public crate-split-yew-0.2.1 (c (n "split-yew") (v "0.2.1") (d (list (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "yew") (r "^0.21") (d #t) (k 0)))) (h "0ym99idr9n4xmvwf2mq5p3ma2mf82w3z7iq74zx9jds5kmz85bic")))

