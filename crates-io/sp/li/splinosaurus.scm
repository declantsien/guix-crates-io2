(define-module (crates-io sp li splinosaurus) #:use-module (crates-io))

(define-public crate-splinosaurus-0.1.0 (c (n "splinosaurus") (v "0.1.0") (d (list (d (n "az") (r "^1.2") (d #t) (k 0)) (d (n "fixed") (r "^1.24") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "pixel-canvas") (r "^0.2") (d #t) (k 2)) (d (n "stl_io") (r "^0.7") (d #t) (k 2)))) (h "1xbdxzzxzv1c4r31qzixh2qv9n3qk45ygxrr9yq609y2bkcpg8m9")))

(define-public crate-splinosaurus-0.2.0 (c (n "splinosaurus") (v "0.2.0") (d (list (d (n "az") (r "^1.2") (d #t) (k 0)) (d (n "fixed") (r "^1.24") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "pixel-canvas") (r "^0.2") (d #t) (k 2)) (d (n "stl_io") (r "^0.7") (d #t) (k 2)))) (h "0w7qbcn0b2s2z4a4msarrmwwf24hmh871j9qpdd67x2xlx0cd57v")))

(define-public crate-splinosaurus-0.2.1 (c (n "splinosaurus") (v "0.2.1") (d (list (d (n "az") (r "^1.2") (d #t) (k 0)) (d (n "fixed") (r "^1.24") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "pixel-canvas") (r "^0.2") (d #t) (k 2)) (d (n "stl_io") (r "^0.7") (d #t) (k 2)))) (h "0sj937ngzrxj5v5yvvi6q3zhzbwfk524xc7vz0kjymp788jjblsl")))

(define-public crate-splinosaurus-0.2.2 (c (n "splinosaurus") (v "0.2.2") (d (list (d (n "az") (r "^1.2") (d #t) (k 0)) (d (n "fixed") (r "^1.24") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "obj") (r "^0.10") (d #t) (k 2)) (d (n "svg") (r "^0.15") (d #t) (k 2)))) (h "1f5zchqcw3j5k6qy816b86i7s0b0dh24pgh9vnwwp1518ryz5gd9")))

