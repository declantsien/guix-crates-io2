(define-module (crates-io sp li splitdiff-rs) #:use-module (crates-io))

(define-public crate-splitdiff-rs-0.4.0 (c (n "splitdiff-rs") (v "0.4.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lsdiff-rs") (r "^0.1.1") (d #t) (k 0)))) (h "0l5df192bas199h5dvbb60jp1s2c6szwvwq7jscgxskabifr5n2z")))

(define-public crate-splitdiff-rs-0.4.1 (c (n "splitdiff-rs") (v "0.4.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lsdiff-rs") (r "^0.1.2") (d #t) (k 0)))) (h "0anfld677rm8677xc6ri0yl4hay4ksq8njdqpp53cr7bz41hl14h")))

