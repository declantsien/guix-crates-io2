(define-module (crates-io sp li spliny) #:use-module (crates-io))

(define-public crate-spliny-0.1.0 (c (n "spliny") (v "0.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "plotters") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ssa5g005r2qi0y8g4354qmqb3adjcgxs1rx8c5syj19qzziljrc")))

