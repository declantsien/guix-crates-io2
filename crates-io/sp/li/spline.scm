(define-module (crates-io sp li spline) #:use-module (crates-io))

(define-public crate-spline-0.0.1 (c (n "spline") (v "0.0.1") (h "1wiicx2j2w1dcddcqfamqxary8v3hkj7knyxbjlbsrmd6qs9b6wr")))

(define-public crate-spline-0.1.0 (c (n "spline") (v "0.1.0") (h "0yirjf4m25gbvalzjlhi94p3ypsg0bb9vjqi9mlpgv4sbn7z5c18")))

(define-public crate-spline-0.2.0 (c (n "spline") (v "0.2.0") (h "1if2xcxskhzj5bcr8s5zsfkcs7fm2jz9cr40jid5bfbwq2ymz12c")))

