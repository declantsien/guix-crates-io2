(define-module (crates-io sp li splitmut) #:use-module (crates-io))

(define-public crate-splitmut-0.1.0 (c (n "splitmut") (v "0.1.0") (h "11z6b5dbrqaad4dsa7z1nyv9gsbk3a017ay44bbn693sqbgmprys") (y #t)))

(define-public crate-splitmut-0.1.1 (c (n "splitmut") (v "0.1.1") (h "12xa6bymh2fc5z6v5abycy9382siybc2dpm8awch3rvd9c5nbyr2")))

(define-public crate-splitmut-0.2.1 (c (n "splitmut") (v "0.2.1") (h "0slmxv91wz537ibzjg8lv6d5zxnxfcsyh22qlwiqn3ilhbrp0l68")))

