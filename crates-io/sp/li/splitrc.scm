(define-module (crates-io sp li splitrc) #:use-module (crates-io))

(define-public crate-splitrc-0.1.0 (c (n "splitrc") (v "0.1.0") (h "1i04pk89ki5wf1ly4kd7bfgp5ywwh1c5sb37k4ry6imw8m3q3752")))

(define-public crate-splitrc-0.1.1 (c (n "splitrc") (v "0.1.1") (h "0jivnkxih9spa8m96g6kdyw0rg1ch2fdjgnjxyn2jx6zmv6xhg45") (r "1.56.1")))

(define-public crate-splitrc-0.1.2 (c (n "splitrc") (v "0.1.2") (h "1m13jnx8i9r71xl5zpkb3q1qs26yj483yi0sz269n5gl93y3gpkd") (r "1.56.1")))

(define-public crate-splitrc-0.1.3 (c (n "splitrc") (v "0.1.3") (h "0cksz6vxfmmswi2n3ydlrggx7slsv97adx00nklj4wsvab1mkcl3") (r "1.56.1")))

(define-public crate-splitrc-0.1.4 (c (n "splitrc") (v "0.1.4") (h "16qvh7lb7h3y6cj817cpdyx9h88i6indpbmrdh2vc6f8qs30rnvz") (r "1.56.1")))

(define-public crate-splitrc-0.1.5 (c (n "splitrc") (v "0.1.5") (h "1jlsgfsisa7vnishz33j21nssmxryndpxw7i8adajb16g2j9gsp2") (r "1.56.1")))

(define-public crate-splitrc-0.1.6 (c (n "splitrc") (v "0.1.6") (h "177cvchlnr89rhi76ygb63qfxvksnz8h7q9gabzs3rrpm5b3dh1k") (r "1.56.1")))

