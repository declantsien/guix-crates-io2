(define-module (crates-io sp li splitfile) #:use-module (crates-io))

(define-public crate-splitfile-0.1.0 (c (n "splitfile") (v "0.1.0") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "066n87arffh9x0ifzcif5q8jlwfak01qp7k576icyh8nzhkf4z6a")))

(define-public crate-splitfile-0.1.1 (c (n "splitfile") (v "0.1.1") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "01yg1pvxqnwpxdcxyq6vxfwlpp3kn5jfz5c4k7p5jxkf5ndiyks2")))

(define-public crate-splitfile-0.1.2 (c (n "splitfile") (v "0.1.2") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0rx99xvkzkkvxl6mp7wbdqk7m7qwgczv0v7gp94zp5k0bfq0y3bc")))

(define-public crate-splitfile-0.2.0 (c (n "splitfile") (v "0.2.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "076c0lbxvagnymn0r771vc6sv6j8ja8wxzdd33gi5fn0yd48wdy9")))

