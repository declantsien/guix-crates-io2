(define-module (crates-io sp li split_aud) #:use-module (crates-io))

(define-public crate-split_aud-0.1.0 (c (n "split_aud") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2.21") (d #t) (k 0)) (d (n "clap") (r "^2.2.5") (d #t) (k 0)) (d (n "regex") (r "^0.1.62") (d #t) (k 0)))) (h "1x1nyxhd72899g00gcyg98ir3fy9wjayb2s4cdnjfcnnmiyh9bll")))

