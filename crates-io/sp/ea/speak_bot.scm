(define-module (crates-io sp ea speak_bot) #:use-module (crates-io))

(define-public crate-speak_bot-0.1.0 (c (n "speak_bot") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "serenity") (r "^0.10.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1894pl7yf988p2a629b8g8l2866vay7p4xh6r0kb6j6jka31hgjh")))

(define-public crate-speak_bot-1.0.0 (c (n "speak_bot") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serenity") (r "^0.10.8") (f (quote ("client" "gateway" "http" "model" "utils" "rustls_backend"))) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "00bldvy3pfw9w5y8280jhi2j0za18n7wg59km8202j114qc3f52s")))

