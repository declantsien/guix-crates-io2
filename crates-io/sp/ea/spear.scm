(define-module (crates-io sp ea spear) #:use-module (crates-io))

(define-public crate-spear-0.1.0 (c (n "spear") (v "0.1.0") (h "18qmyv0q5bi0dhwgbk3fmps91b6rm69a7knzrvfx3889alv12zzd")))

(define-public crate-spear-0.1.1 (c (n "spear") (v "0.1.1") (h "09ibrrf3gfzabpg8l9jg8ljifwm28c7z3lhk93pyi4fy01njd055")))

