(define-module (crates-io sp ea speakln) #:use-module (crates-io))

(define-public crate-speakln-0.0.1 (c (n "speakln") (v "0.0.1") (d (list (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "tts") (r "^0.24") (d #t) (k 0)))) (h "1r5m2py1ag5lkh4ssx1jlm772j2zw3d9iavmp750wsx0vdmgrqhq")))

