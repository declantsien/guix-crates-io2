(define-module (crates-io sp ea speare_macro) #:use-module (crates-io))

(define-public crate-speare_macro-0.0.1 (c (n "speare_macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "053xfwym3f3bdgzhhgwd8swj97gk9cn0xvsavbfixxv9xl2fc4b1")))

(define-public crate-speare_macro-0.0.2 (c (n "speare_macro") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1cmlbyiv6ykn5hkl3213vhqga0qbwzvnz17yvbzsb7dafl1h72j1")))

(define-public crate-speare_macro-0.0.3 (c (n "speare_macro") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "02bi6pqswlbcmkvyarzw02kcvf2dyf2w997izknl9vnns435463f")))

(define-public crate-speare_macro-0.0.4 (c (n "speare_macro") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1cajw8mgryd3jfbk2kiciv31a7h94y3pqn5vxn1rxa101r3rip2k")))

(define-public crate-speare_macro-0.0.5 (c (n "speare_macro") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1cf66i2nnk1iykm528i3xdsgkjqfg74iczxh3lpynfhkrhkpili9")))

(define-public crate-speare_macro-0.0.6 (c (n "speare_macro") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "07ddghmfj5jbl937yvy62f4ryqa6w564hjy1s6d71c6byi9l9yzh")))

(define-public crate-speare_macro-0.0.7 (c (n "speare_macro") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "08pf8g4jb9qfw70mb6wrqysnc2xi7s0am7cmckfhh50nicwmq4cp")))

(define-public crate-speare_macro-0.0.8 (c (n "speare_macro") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1dk2lqly0wwp72nnp11gsmv6gnfqgk481bld32qd0a1b582g11kj")))

