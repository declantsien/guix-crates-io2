(define-module (crates-io sp ea speare) #:use-module (crates-io))

(define-public crate-speare-0.0.1 (c (n "speare") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)))) (h "12wrydn2a26nzd7knn1kbagzdc1fplsv3hr68gw44f3i5aqjkpmm")))

(define-public crate-speare-0.0.2 (c (n "speare") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "speare_macro") (r "^0.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)))) (h "0kkczl7wizs081ljddknq00c720d2yxb9aarsxnanbs9kwxvl4vx")))

(define-public crate-speare-0.0.3 (c (n "speare") (v "0.0.3") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "speare_macro") (r "^0.0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)))) (h "0vmyxjssalqj87cvfam1pa1l2g8hwg96zikfdbis29i97aqcirj6")))

(define-public crate-speare-0.0.4 (c (n "speare") (v "0.0.4") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "speare_macro") (r "^0.0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)))) (h "089cfcj9183a7cwhfka2sfv77xzw9fy8cx8v4ixkhx8i6fngprm4")))

(define-public crate-speare-0.0.5 (c (n "speare") (v "0.0.5") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "speare_macro") (r "^0.0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)))) (h "0d1n1bdyh2kkj953l996s9ikxr74wwic55bj4l8xn1zz630w2q8n")))

(define-public crate-speare-0.0.6 (c (n "speare") (v "0.0.6") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "speare_macro") (r "^0.0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)))) (h "1yw5gr2dczh28whgfzd0q5anjplsdv063nb4dhfc2ck6fqvnn8h5")))

(define-public crate-speare-0.0.7 (c (n "speare") (v "0.0.7") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "speare_macro") (r "^0.0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)))) (h "10jb27zl89gx748lgqmjskb98x8g1zx7ypravk6dniai6w4c8brw")))

(define-public crate-speare-0.0.8 (c (n "speare") (v "0.0.8") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "speare_macro") (r "^0.0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)))) (h "0ldd1w6kjsaszydblr1vffckqda3jzqig3nfndkhkd1hq643nglf")))

(define-public crate-speare-0.0.9 (c (n "speare") (v "0.0.9") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "speare_macro") (r "^0.0.7") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)))) (h "0vzyis7g518hg60xhdrn5c4sc9m9vn89qpg8pb399lalrhvq87y2")))

(define-public crate-speare-0.0.10 (c (n "speare") (v "0.0.10") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "speare_macro") (r "^0.0.7") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)))) (h "1mzmpnp9w3bc82iajfkjn6kpvprl7fc26lcxm14n0kq6091zld65")))

(define-public crate-speare-0.0.11 (c (n "speare") (v "0.0.11") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "speare_macro") (r "^0.0.7") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)))) (h "10pf8jqfgkl0rxrz6riwc2mxyvichdib24djg18b9fs01f9zzzkr")))

(define-public crate-speare-0.0.12 (c (n "speare") (v "0.0.12") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "speare_macro") (r "^0.0.7") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)))) (h "0k3ggpgyiphhd1xh00j1qpmlys1dd8chvxakid1f3zyrx3q7b5m2")))

(define-public crate-speare-0.1.0 (c (n "speare") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "speare_macro") (r "^0.0.7") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt" "sync" "time" "test-util"))) (d #t) (k 2)))) (h "17zc4hs5kci47fvjriwxpqgkbrnd73x4d2mnci3jxs7w4mkii6mi")))

(define-public crate-speare-0.1.1 (c (n "speare") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt" "sync" "time" "test-util"))) (d #t) (k 2)))) (h "0r08yiiz3n8czklr5a0i6gjp43vrq05263r5z98dky27az6ky09a")))

(define-public crate-speare-0.1.2 (c (n "speare") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt" "sync" "time" "test-util"))) (d #t) (k 2)))) (h "14ry1dlp90i1kn066a2s0gvrf030c52fp5wvgwnvb1s2072vixzm")))

(define-public crate-speare-0.1.3 (c (n "speare") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt" "sync" "time" "test-util"))) (d #t) (k 2)))) (h "1z9n9gjbhpc4kndkb9hn99kscg5d71bis0hazr1a9jnmv7mk2ggv")))

(define-public crate-speare-0.1.4 (c (n "speare") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt" "sync" "time" "test-util"))) (d #t) (k 2)))) (h "13kibcrda81hs5sg0vpibx04z2df6kdkfgm893p02bgl6h2716da")))

(define-public crate-speare-0.1.5 (c (n "speare") (v "0.1.5") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt" "sync" "time" "test-util"))) (d #t) (k 2)))) (h "1g4grbbz6ksafpn07jl8jgyk9zvkq4lgm5b19k9878kc3mvh7mnb")))

(define-public crate-speare-0.1.6 (c (n "speare") (v "0.1.6") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt" "sync" "time" "test-util"))) (d #t) (k 2)))) (h "1wdmq58kahrlii7nljxv9kl1zndpgicfsd0pg7hglbnr2lw3mgsm")))

(define-public crate-speare-0.1.7 (c (n "speare") (v "0.1.7") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt" "sync" "time" "test-util"))) (d #t) (k 2)))) (h "199zxp4b8rkmwabm8pbdiws17mndkys90aa1wqzjl4k0gjl835xi")))

(define-public crate-speare-0.1.8 (c (n "speare") (v "0.1.8") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt" "rt-multi-thread" "sync" "time" "test-util"))) (d #t) (k 2)))) (h "1p4xsfbzw2s4zsxfks26kmfg41bhzzrsq2xdxi9i9f6gb1ag4knl")))

(define-public crate-speare-0.1.9 (c (n "speare") (v "0.1.9") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt" "rt-multi-thread" "sync" "time" "test-util"))) (d #t) (k 2)))) (h "18zha2zq8zrjkmwl7smc4dn7pkq8khxp26n5kbf47b3pimsanf2q")))

(define-public crate-speare-0.1.10 (c (n "speare") (v "0.1.10") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt" "sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt" "rt-multi-thread" "sync" "time" "test-util"))) (d #t) (k 2)))) (h "090dqvnfbm53dsm63d36g17hdg855gd2q28gyk1fwzkd06zm0kav")))

