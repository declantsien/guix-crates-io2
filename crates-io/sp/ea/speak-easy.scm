(define-module (crates-io sp ea speak-easy) #:use-module (crates-io))

(define-public crate-speak-easy-0.1.0 (c (n "speak-easy") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1.32") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "1l1mbaiizsp3fl9r2pqymx3qr7rnkm7wmsxkdf6mcwdhnlf9brby")))

(define-public crate-speak-easy-0.1.1 (c (n "speak-easy") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("time" "rt"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2") (d #t) (k 0)) (d (n "tracing-core") (r "^0.1.32") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "1cnnr5scnmaa6qngw4xjj5ifd09m00gm0456k0cg8zwsqg6qg7gm")))

