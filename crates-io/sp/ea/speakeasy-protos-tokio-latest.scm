(define-module (crates-io sp ea speakeasy-protos-tokio-latest) #:use-module (crates-io))

(define-public crate-speakeasy-protos-tokio-latest-0.1.0 (c (n "speakeasy-protos-tokio-latest") (v "0.1.0") (d (list (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tower") (r "^0.4") (d #t) (k 0)))) (h "1jrx20gny8fscpfs777n6qqwr0ycrz7mjc5753kzxyzshwv9rs25")))

(define-public crate-speakeasy-protos-tokio-latest-0.2.0 (c (n "speakeasy-protos-tokio-latest") (v "0.2.0") (d (list (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tower") (r "^0.4") (d #t) (k 0)))) (h "1142zmv1d4zhyzxfc9xmm0xrxd66m5708n5wdv8yxn8zzc91whjj")))

