(define-module (crates-io sp ek speki-tui) #:use-module (crates-io))

(define-public crate-speki-tui-0.1.0 (c (n "speki-tui") (v "0.1.0") (d (list (d (n "ascii_tree") (r "^0.1.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rasciigraph") (r "^0.2.0") (d #t) (k 0)) (d (n "speki-backend") (r "^0.1.1") (d #t) (k 0)))) (h "13ilk4s7gzam5ghf3x33l8hl643rf7vidyiwdknma5nrs6nzhcr0")))

