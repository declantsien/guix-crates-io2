(define-module (crates-io sp ek spekt) #:use-module (crates-io))

(define-public crate-spekt-0.1.0 (c (n "spekt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-trait") (r "^0.1.37") (d #t) (k 0)))) (h "1g2m1w719x6fxl5f18s0x9sfcaw313pn5gq520n8q1qkghcg7gj4")))

(define-public crate-spekt-0.1.1 (c (n "spekt") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "async-trait") (r "^0.1.37") (d #t) (k 0)))) (h "1b3v32fbjybkpwqi03lnrzfnrdn72q67wgnizqffghcsm4hw6iyn")))

