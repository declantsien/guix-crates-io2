(define-module (crates-io sp sh spsheet) #:use-module (crates-io))

(define-public crate-spsheet-0.1.0 (c (n "spsheet") (v "0.1.0") (d (list (d (n "chrono") (r "~0.4.0") (d #t) (k 0)) (d (n "nom") (r "~3.2.1") (d #t) (k 0)) (d (n "quick-xml") (r "~0.10.1") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "~0.3.5") (o #t) (d #t) (k 0)) (d (n "time") (r "~0.1.38") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "~2.0.1") (o #t) (d #t) (k 0)) (d (n "zip") (r "~0.2.6") (o #t) (d #t) (k 0)))) (h "10hqkhlmyk6pgd5jihkc4zx0n8wzllsa2brycg25jcsc1gxd3dcg") (f (quote (("xlsx" "quick-xml" "tempdir" "time" "walkdir" "zip") ("ods" "quick-xml" "tempdir" "walkdir" "zip"))))))

