(define-module (crates-io sp -m sp-maybe-compressed-blob) #:use-module (crates-io))

(define-public crate-sp-maybe-compressed-blob-4.0.0-dev (c (n "sp-maybe-compressed-blob") (v "4.0.0-dev") (d (list (d (n "ruzstd") (r "^0.2.2") (d #t) (t "cfg(target_os = \"unknown\")") (k 0)) (d (n "zstd") (r "^0.6.0") (t "cfg(not(target_os = \"unknown\"))") (k 0)))) (h "15cxdv912sqjphrwl3bjyz6kmka4cnyaddp0qljqbl2v9xvfmi8j")))

(define-public crate-sp-maybe-compressed-blob-4.1.0 (c (n "sp-maybe-compressed-blob") (v "4.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zstd") (r "^0.11.2") (k 0)))) (h "1g2x6aki5sdklzp3fggd7gpclkvmc2kfpnvx5sadxc8ggaji2sif")))

(define-public crate-sp-maybe-compressed-blob-5.0.0 (c (n "sp-maybe-compressed-blob") (v "5.0.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zstd") (r "^0.11.2") (k 0)))) (h "18vz2j68hsha3j4nfm89clxi7003zxmwfb8lkf0kw30kdwn77xsx")))

(define-public crate-sp-maybe-compressed-blob-5.1.0-dev1 (c (n "sp-maybe-compressed-blob") (v "5.1.0-dev1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zstd") (r "^0.12.3") (k 0)))) (h "0kz7y62ml8hf79ssq1zyxwqwadklgi53lgnwa8l7mqcwmnmqq79k")))

(define-public crate-sp-maybe-compressed-blob-5.1.0-dev.2 (c (n "sp-maybe-compressed-blob") (v "5.1.0-dev.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zstd") (r "^0.12.3") (k 0)))) (h "0m018dwpriijbk1648pz970q9s2isark5kzbl6awd4dnf0xzdx6s")))

(define-public crate-sp-maybe-compressed-blob-5.1.0-dev.3 (c (n "sp-maybe-compressed-blob") (v "5.1.0-dev.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zstd") (r "^0.12.3") (k 0)))) (h "05hgk5500xs2x4gvjr60lxk59gvz20jnq9mg8r01wsibpgyyn5ym")))

(define-public crate-sp-maybe-compressed-blob-5.1.0-dev.4 (c (n "sp-maybe-compressed-blob") (v "5.1.0-dev.4") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zstd") (r "^0.12.3") (k 0)))) (h "1qbi3wz5gn7dm341a0qk8dfl2mc2xndig7zjiapy2312i9w8f1y7")))

(define-public crate-sp-maybe-compressed-blob-5.1.0-dev.5 (c (n "sp-maybe-compressed-blob") (v "5.1.0-dev.5") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zstd") (r "^0.12.3") (k 0)))) (h "05q0zcrllgxxj8xgdk7687wak5z7p2f64w59nmjczgxy2nlc3j18")))

(define-public crate-sp-maybe-compressed-blob-5.1.0-dev.6 (c (n "sp-maybe-compressed-blob") (v "5.1.0-dev.6") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zstd") (r "^0.12.3") (k 0)))) (h "12hajbhlgdmx0wbxdk03r7r2fl7ad07q67ia6lwi4gwbnqs4vgzh")))

(define-public crate-sp-maybe-compressed-blob-6.0.0 (c (n "sp-maybe-compressed-blob") (v "6.0.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zstd") (r "^0.12.3") (k 0)))) (h "03622waf4f8n9nyc1sk62hs0swac6apswv72cxjqbz6jzk28hypp")))

(define-public crate-sp-maybe-compressed-blob-7.0.0 (c (n "sp-maybe-compressed-blob") (v "7.0.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (k 0)))) (h "06qs0gcgmwfsxln91w9as5616mwng76nx37c8dmz4676h9ks5c8w")))

(define-public crate-sp-maybe-compressed-blob-8.0.0-dev.1 (c (n "sp-maybe-compressed-blob") (v "8.0.0-dev.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (k 0)))) (h "0gacjsrhb4lx76ss1aw2jkkwjr8ifj7jfp89qs67gg3wamxhz2gw")))

(define-public crate-sp-maybe-compressed-blob-8.0.0 (c (n "sp-maybe-compressed-blob") (v "8.0.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (k 0)))) (h "1ca2icafjfwwr14j1vprdnizcyjk09kvlbsn0hwai884l4i0wyip")))

(define-public crate-sp-maybe-compressed-blob-9.0.0 (c (n "sp-maybe-compressed-blob") (v "9.0.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (k 0)))) (h "1m10w07mk6bry8dkd2i7xvgnxg74pqzm5azn95z24ab40f7pcil8")))

(define-public crate-sp-maybe-compressed-blob-10.0.0 (c (n "sp-maybe-compressed-blob") (v "10.0.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (k 0)))) (h "1y6g0xd10dq487f5dwrpxa03qfn12dxlv098gs3b8pswvf722l09")))

(define-public crate-sp-maybe-compressed-blob-11.0.0 (c (n "sp-maybe-compressed-blob") (v "11.0.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zstd") (r "^0.12.4") (k 0)))) (h "1zxf5234vps8cy3aaf6dk7qnl8x43n8pcs1qj2h9irpv3b0niizh")))

