(define-module (crates-io sp it spitzenfinder) #:use-module (crates-io))

(define-public crate-spitzenfinder-0.4.4 (c (n "spitzenfinder") (v "0.4.4") (d (list (d (n "plotters") (r "^0.3.5") (d #t) (k 0)) (d (n "unit_conversion") (r "^0.2.0") (d #t) (k 0)) (d (n "user_query") (r "^0.1.0") (d #t) (k 0)))) (h "15j62il57iha8ghra1q6vca9zwq2d8xr2czcsn62999f4lnn1l96")))

