(define-module (crates-io sp it spit) #:use-module (crates-io))

(define-public crate-spit-0.1.0 (c (n "spit") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0dz28pbpy4glq14lwxsf7lcy44xs3g8qf2h2xxfriv1sfq2jqp7m")))

(define-public crate-spit-0.2.0 (c (n "spit") (v "0.2.0") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1i7y8pcxggf26l54ls0hps6p2rg8rwmc6wd3php8yrm6xjf0jkby")))

