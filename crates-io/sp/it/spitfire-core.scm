(define-module (crates-io sp it spitfire-core) #:use-module (crates-io))

(define-public crate-spitfire-core-0.1.0 (c (n "spitfire-core") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "124ixpwww4g473rwg1y74pqa318yafadf22hgi9n9g8ryvcpkldw")))

(define-public crate-spitfire-core-0.1.1 (c (n "spitfire-core") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pr4f2bz075bf33c2vlm2lh5akkb8gn06jpr7zxggdxs91nywd55")))

(define-public crate-spitfire-core-0.2.0 (c (n "spitfire-core") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1a4nih8jq87fy57n8x90dr4yb0mkxlkbq93lqs535hpchrv2kdam")))

(define-public crate-spitfire-core-0.3.0 (c (n "spitfire-core") (v "0.3.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1an87m4r9m6x7hic9fn3nbc86z5lp81k4icbc47wcv463vhg7aas")))

(define-public crate-spitfire-core-0.4.0 (c (n "spitfire-core") (v "0.4.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0g9scvkiljir6fjny7n4d9xrhdkwhrb4g22dwx5fi96cv2lhhzgz")))

(define-public crate-spitfire-core-0.5.0 (c (n "spitfire-core") (v "0.5.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q9aphnqlmg24kzdrpc46gma6v8x0bx1m5f5yajvc16ir7igqqxx")))

(define-public crate-spitfire-core-0.5.1 (c (n "spitfire-core") (v "0.5.1") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sm4j60irf4xxxnsck2kmi5fll9a41p81ndq0k3g1jf84nx39xxy")))

(define-public crate-spitfire-core-0.6.0 (c (n "spitfire-core") (v "0.6.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lrn1qr8d0h99480x6j4vm51p160zz6b9j3wp7px3mspcg36hsmv")))

(define-public crate-spitfire-core-0.7.0 (c (n "spitfire-core") (v "0.7.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "099swcc73ryky0fygra13livdqby8cdbamq726cpwihxlapxc8l8")))

(define-public crate-spitfire-core-0.7.1 (c (n "spitfire-core") (v "0.7.1") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1505s6pk5bv3j4biq7054cs95spzj4dancw1gdj6wr8sgm4i42x8")))

(define-public crate-spitfire-core-0.8.0 (c (n "spitfire-core") (v "0.8.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "018jsrx8zwf4ab93zd4mfv2k4s34vy0r6b2qqc7jqj5qvkvs6yih")))

(define-public crate-spitfire-core-0.9.0 (c (n "spitfire-core") (v "0.9.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i7v1i9qyf6vr0h5bl6valk1hb0a9s7sf7ca6ri8xz4wq9vv5ccg")))

(define-public crate-spitfire-core-0.10.0 (c (n "spitfire-core") (v "0.10.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y1zry2ibi2phjmlg8f2q2anaq4a8pk2nsf5rabc0d2nwxcvg8b4")))

(define-public crate-spitfire-core-0.11.0 (c (n "spitfire-core") (v "0.11.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r7jv2z84lhhbgi4r4q6lwv3di6aqax1w52yirj44p6s497pk6b9")))

(define-public crate-spitfire-core-0.12.0 (c (n "spitfire-core") (v "0.12.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l4alw0ij37mk2d889dra6kan4dw5w058xvk2sycm4hdgrv9rz4a")))

(define-public crate-spitfire-core-0.13.0 (c (n "spitfire-core") (v "0.13.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "009vynbdlycg4nbyrsh5n73x85ikds1zsb6dl9ryl0k9jywj6s6c")))

(define-public crate-spitfire-core-0.13.1 (c (n "spitfire-core") (v "0.13.1") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l2rsl8lzx5b7mxpr1j6pxi0slpz8kcdwidb6viql3cdiz68n7ml")))

(define-public crate-spitfire-core-0.14.0 (c (n "spitfire-core") (v "0.14.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "06llyx0ha7mb6r52syld52mgmmdmryw4pfqpqv8zr3bvilwqkrsx")))

(define-public crate-spitfire-core-0.15.0 (c (n "spitfire-core") (v "0.15.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cll9p797yzxfn6afwmlbkpyy5dw8z5simki5wghvwv2llx0da1r")))

(define-public crate-spitfire-core-0.15.1 (c (n "spitfire-core") (v "0.15.1") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nh5l9c7dmvclk13p1kdksp2354mm3mp40xipmrlsd1k481lrwvw")))

(define-public crate-spitfire-core-0.16.0 (c (n "spitfire-core") (v "0.16.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "12f3fzmcabmxbxbzg3my4jpxps834f06wc8b4h02f4i7pxybbrkv")))

(define-public crate-spitfire-core-0.17.0 (c (n "spitfire-core") (v "0.17.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gsy8qh607kfc5lhk80xns8sbdpdrpizd15m6saqig1haniv32f2")))

(define-public crate-spitfire-core-0.17.1 (c (n "spitfire-core") (v "0.17.1") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hcxp90gjwjmj51fsaags72sfln84hvg6dzfp6w6dczci1krnjiq")))

(define-public crate-spitfire-core-0.17.2 (c (n "spitfire-core") (v "0.17.2") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "17mkhbd13rqp77cry2mq0gwzpf9ln3av2blyxx1b6nmgll5i125w")))

(define-public crate-spitfire-core-0.17.3 (c (n "spitfire-core") (v "0.17.3") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yhqbb8kchjdfpnlbzyhzmx53gw7bff3vagqr7gd80az3rj3jdjj")))

(define-public crate-spitfire-core-0.17.4 (c (n "spitfire-core") (v "0.17.4") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "16hkjnsnjpbv0qwdbls4k87m06wchaap8j03x2kcl5lxj3zcrjhf")))

(define-public crate-spitfire-core-0.17.5 (c (n "spitfire-core") (v "0.17.5") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xrfrvs9a2a9zg6spqvcq4lz9mvh9fs4sjz0i4jdzqvc8bdar9pg")))

(define-public crate-spitfire-core-0.17.6 (c (n "spitfire-core") (v "0.17.6") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "06p322hkazg00dx3aa4ljzr0gh6ry0s0i1lqqd8cgigmqjl98iyh")))

(define-public crate-spitfire-core-0.18.0 (c (n "spitfire-core") (v "0.18.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q63qynrc9k44yl299qna0irqcds3w9nbf904yxdyrrq1ysizbgl")))

(define-public crate-spitfire-core-0.19.0 (c (n "spitfire-core") (v "0.19.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rbhaxm73i194wji0wpsw5d3hy6mm3jy83d30k2sxrl0cry1j8xz")))

(define-public crate-spitfire-core-0.19.1 (c (n "spitfire-core") (v "0.19.1") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lppcasfy5rzgfls049941p466shpmlv99xaj59q8nls809sy43v")))

(define-public crate-spitfire-core-0.19.2 (c (n "spitfire-core") (v "0.19.2") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "14kd0qfn78lgckmzb2f8hs91bd6g07sy42bd4h14nn67agrs1zhz")))

(define-public crate-spitfire-core-0.19.3 (c (n "spitfire-core") (v "0.19.3") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pahwvyk4lxjjvpk53rmgjw8mbb499d11c6nmpgdqfymp9g3ji74")))

(define-public crate-spitfire-core-0.19.4 (c (n "spitfire-core") (v "0.19.4") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xsb6x5ihvym530cfc8193f51yc01krvqb2i31zynv0zxv58ggan")))

(define-public crate-spitfire-core-0.19.5 (c (n "spitfire-core") (v "0.19.5") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lr5vimwbsvnnyicn35xn2kvpbbr59c8x6a2i0kxbpamprnji1d6")))

(define-public crate-spitfire-core-0.20.0 (c (n "spitfire-core") (v "0.20.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b8yrnl8x2i3p8xnbayv6gaxmglk2qr88zp2k441xdhj17sh82ks")))

(define-public crate-spitfire-core-0.20.1 (c (n "spitfire-core") (v "0.20.1") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ldq6lb832psijkjfl1h69ryqfl7hb5lwqyhhcmk4li9qia9i9b5")))

(define-public crate-spitfire-core-0.21.0 (c (n "spitfire-core") (v "0.21.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "11lwkxn5yv78fznq871v2hchp540nvwqcarhm07qb8vyh900vdqb")))

(define-public crate-spitfire-core-0.22.0 (c (n "spitfire-core") (v "0.22.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "15khhd6610gkp81l9m1cgndm24zmkzpdp7c13ppxm6r6fqmwl5yp")))

(define-public crate-spitfire-core-0.23.0 (c (n "spitfire-core") (v "0.23.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fpajdnvm7xd68ah6dz3bka68wh0fidhahi28jbdkrxbyxqmp6kl")))

(define-public crate-spitfire-core-0.23.1 (c (n "spitfire-core") (v "0.23.1") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "18qacfiz32g1bhi9d2hpjj0nmhqm1rcjg9hkn9z4lkl97ksikaih")))

(define-public crate-spitfire-core-0.23.2 (c (n "spitfire-core") (v "0.23.2") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p9zicmvrdsrgj2l5sf4gsk4mbzcphiw0sq0l96kryg09q95s9gk")))

(define-public crate-spitfire-core-0.24.0 (c (n "spitfire-core") (v "0.24.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bdzfhmc3rp6nlid9vprax6ip4kwj1p6v3bwp9z0kxp4dwnxlj0m")))

(define-public crate-spitfire-core-0.25.0 (c (n "spitfire-core") (v "0.25.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bavndjamwdjk7ycmvvl7h4qksl2ppad0bdx5a46vg4n1lv8d81h")))

(define-public crate-spitfire-core-0.26.0 (c (n "spitfire-core") (v "0.26.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "05h7dl9z1c9wzzfvziybkm4kicxdlbyvpnzbzfwxvglb0hb7ahjm")))

(define-public crate-spitfire-core-0.27.0 (c (n "spitfire-core") (v "0.27.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r3vqq90z44hnpcld7cwzx55bqz950pnrdhgcvnq65c5r8jcv1kn")))

(define-public crate-spitfire-core-0.27.1 (c (n "spitfire-core") (v "0.27.1") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "01p9vjdkl6cjw9pfsmvfxk9jq1pq8j4cfpw571jblc3pzjygynv7")))

(define-public crate-spitfire-core-0.27.2 (c (n "spitfire-core") (v "0.27.2") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zwfhi59izbd6479awfxvn15kpb5nyx31fwm5qm11pdklqxn4xki")))

(define-public crate-spitfire-core-0.28.0 (c (n "spitfire-core") (v "0.28.0") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "197gi65w0daxipa2hh3m7cawnr956hcjx01hij52vh1cssyp8s0i")))

(define-public crate-spitfire-core-0.28.1 (c (n "spitfire-core") (v "0.28.1") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1g6z8zy9nqzgfl7hl6jlv1l1z7v9bgpxjaj6c8xnmgis7y8abcwv")))

(define-public crate-spitfire-core-0.28.2 (c (n "spitfire-core") (v "0.28.2") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gd623hwlq3079wmx684y30nzag3i2rcdgknwlarhfizgbyx99qc")))

(define-public crate-spitfire-core-0.28.3 (c (n "spitfire-core") (v "0.28.3") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zn65bz2d1ffdyrmswis8whmv8nsh5s3vm41hi6pry6ng00whv44")))

(define-public crate-spitfire-core-0.28.4 (c (n "spitfire-core") (v "0.28.4") (d (list (d (n "bytemuck") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wpd4xab6maq0nb4ivmdmyz5kncqc5milfckcbnw5726fwdz1cda")))

