(define-module (crates-io sp it spitfire-input) #:use-module (crates-io))

(define-public crate-spitfire-input-0.11.0 (c (n "spitfire-input") (v "0.11.0") (d (list (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "spitfire-core") (r "^0.11") (d #t) (k 0)) (d (n "spitfire-glow") (r "^0.11") (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "12khph3d0p8p9cirwzzw4q7pvfp7gdwq5y923mmd5v259m7y14qj")))

(define-public crate-spitfire-input-0.12.0 (c (n "spitfire-input") (v "0.12.0") (d (list (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1bjcswfrjzj4fa6p1ga35qb8j49mv3g545vknjwsp6nm1gsvf5zl")))

(define-public crate-spitfire-input-0.13.0 (c (n "spitfire-input") (v "0.13.0") (d (list (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "14njr94m3ifjhxajpyjzka2ck0cmzzpkzz9c4nqjk7r0541jnfzr")))

(define-public crate-spitfire-input-0.13.1 (c (n "spitfire-input") (v "0.13.1") (d (list (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1j6xyy9xcbjjg664kslr0wy3cjg8dp1ccmjdqb5s60silc1wcaia")))

(define-public crate-spitfire-input-0.14.0 (c (n "spitfire-input") (v "0.14.0") (d (list (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "090v97z5cnvs0i8abs5ba2zyiv8hw0s657rvyvq40jla55kkf8kc")))

(define-public crate-spitfire-input-0.15.0 (c (n "spitfire-input") (v "0.15.0") (d (list (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "0fhny28j7mhl3nh73qbgmxsrbz2sai1vqpxy48vmil571vyq25f8")))

(define-public crate-spitfire-input-0.15.1 (c (n "spitfire-input") (v "0.15.1") (d (list (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1l1d2792a4n9z9b2f5sg72pgs86qskga5yvscp81i0cr0s4qs86g")))

(define-public crate-spitfire-input-0.16.0 (c (n "spitfire-input") (v "0.16.0") (d (list (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "05mpkn8acdcrmnz15hc51nm48hk5w4m1bd39qz480ymiwapaqinc")))

(define-public crate-spitfire-input-0.17.0 (c (n "spitfire-input") (v "0.17.0") (d (list (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1q1nwg2x5dl1x8plv1d3zxkd8mnvj4nvk11xkkyq315dg9nvl5qj")))

(define-public crate-spitfire-input-0.17.1 (c (n "spitfire-input") (v "0.17.1") (d (list (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "05f1vj4n38xj9f9zfc39jbnfpbss4vg44wd6a5f7rxyi5pb1maqg")))

(define-public crate-spitfire-input-0.17.2 (c (n "spitfire-input") (v "0.17.2") (d (list (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1hphdgci0ykkbfk7wb4mnqn3vgzyxncwvkrpki8nnvwq649xbqjd")))

(define-public crate-spitfire-input-0.17.3 (c (n "spitfire-input") (v "0.17.3") (d (list (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "001b7l5vxk4hfz7icgf5yxqndv8rkc3sg32dni54xw5hqvlvkl0l")))

(define-public crate-spitfire-input-0.17.4 (c (n "spitfire-input") (v "0.17.4") (d (list (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "162hfhlxjb2sx2m2c12dgm80m421rkk7m0r73swz343b5kxh6h43")))

(define-public crate-spitfire-input-0.17.5 (c (n "spitfire-input") (v "0.17.5") (d (list (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1j73kvnmlixj180ddy1wk6ykad74sbxqsv99ylisz2rikkpk1d2k")))

(define-public crate-spitfire-input-0.17.6 (c (n "spitfire-input") (v "0.17.6") (d (list (d (n "glutin") (r "^0.28") (d #t) (k 0)) (d (n "typid") (r "^1") (d #t) (k 0)))) (h "1lyi35070i2br4nsl82x6m95slmihwir65j31m5g7pp8nwbv4z3x")))

(define-public crate-spitfire-input-0.18.0 (c (n "spitfire-input") (v "0.18.0") (d (list (d (n "glutin") (r "^0.28") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (f (quote ("web"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0s3c1ri190ki8185hvj5dqxjcnv3jgip9x5w50ng0z80g6f341c3")))

(define-public crate-spitfire-input-0.19.0 (c (n "spitfire-input") (v "0.19.0") (d (list (d (n "glutin") (r "^0.28") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (f (quote ("web"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1nvvjr6rpk1cy4a59dz7plxdd6cdn8i1b32fvfdwknv7h5l3s3f0")))

(define-public crate-spitfire-input-0.19.1 (c (n "spitfire-input") (v "0.19.1") (d (list (d (n "glutin") (r "^0.28") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (f (quote ("web"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "03qmxzcmvq6q9c0zy2zkpkz999bq3a5q70bvcb655v87xmsz9jhl")))

(define-public crate-spitfire-input-0.19.2 (c (n "spitfire-input") (v "0.19.2") (d (list (d (n "glutin") (r "^0.28") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (f (quote ("web"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "028pvxzvrnjcfddz9jym4wi3h1jp0b8qbapadpdwlw7ika2kpf9j")))

(define-public crate-spitfire-input-0.19.3 (c (n "spitfire-input") (v "0.19.3") (d (list (d (n "glutin") (r "^0.28") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (f (quote ("web"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0yvflrw0s4h4baqblv3hv0nyyqz90ps3r089wadj0ffpr3jp69a1")))

(define-public crate-spitfire-input-0.19.4 (c (n "spitfire-input") (v "0.19.4") (d (list (d (n "glutin") (r "^0.28") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (f (quote ("web"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0ajylqnp45ikfsd20c04rvzmx6qywywynwclayjinki1dmbijb3j")))

(define-public crate-spitfire-input-0.19.5 (c (n "spitfire-input") (v "0.19.5") (d (list (d (n "glutin") (r "^0.28") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (f (quote ("web"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1cxg41bc7clbcjm252kkia03hypbf5ywsc32xmnxnk3v4fh0rgzk")))

(define-public crate-spitfire-input-0.20.0 (c (n "spitfire-input") (v "0.20.0") (d (list (d (n "glutin") (r "^0.28") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (f (quote ("web"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1wrzjd1wnqy6p7siwrxhdniqzi41ypxgjqbrwz67plwqkinsfmmd")))

(define-public crate-spitfire-input-0.20.1 (c (n "spitfire-input") (v "0.20.1") (d (list (d (n "glutin") (r "^0.28") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (f (quote ("web"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1rjb625ycijp8mnnzixm1a3kk8fwbgd0hqs2jd13wgip2wjq7yyv")))

(define-public crate-spitfire-input-0.21.0 (c (n "spitfire-input") (v "0.21.0") (d (list (d (n "glutin") (r "^0.28") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (f (quote ("web"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1dk8wmjqch4ra93zn9k3mqssy22rjgagj6szgaays4z2dbwqgcc2")))

(define-public crate-spitfire-input-0.22.0 (c (n "spitfire-input") (v "0.22.0") (d (list (d (n "glutin") (r "^0.28") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (f (quote ("web"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1q5a79gy4pg32rixz44amfik82c6dmsxqfl6948j4cjl25rsdca8")))

(define-public crate-spitfire-input-0.23.0 (c (n "spitfire-input") (v "0.23.0") (d (list (d (n "glutin") (r "^0.28") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (f (quote ("web"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0cf33srhrp7ddf2dfdnhxg03wk01kfzwqkgfddk78c243614ar8y")))

(define-public crate-spitfire-input-0.23.1 (c (n "spitfire-input") (v "0.23.1") (d (list (d (n "glutin") (r "^0.28") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (f (quote ("web"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "17grcnf2h2cx33iisqfnngkm1m9jjl3b8mfs6jvv0ax4w5fpcnzk")))

(define-public crate-spitfire-input-0.23.2 (c (n "spitfire-input") (v "0.23.2") (d (list (d (n "glutin") (r "^0.28") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (f (quote ("web"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1yxk997rniivm4bqyx42d57kr9j9ywsn7lnyiz8193qyk3g1ansh")))

(define-public crate-spitfire-input-0.24.0 (c (n "spitfire-input") (v "0.24.0") (d (list (d (n "glutin") (r "^0.28") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (f (quote ("web"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1a22gskx2ck7appnqbvnm28mnyk8qkyc537jyrfaa5myvbzf05lb")))

(define-public crate-spitfire-input-0.25.0 (c (n "spitfire-input") (v "0.25.0") (d (list (d (n "glutin") (r "^0.28") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (f (quote ("web"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "03h52xcmpzi1j00fxvn93hp5n13nfp6zaj571jxcga503x2ry0v7")))

(define-public crate-spitfire-input-0.26.0 (c (n "spitfire-input") (v "0.26.0") (d (list (d (n "glutin") (r "^0.28") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (f (quote ("web"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "09zpb330w1dhz6xz6a51xkxn8vr5hn1aawfjjcdplgprn6f0yx97")))

(define-public crate-spitfire-input-0.27.0 (c (n "spitfire-input") (v "0.27.0") (d (list (d (n "glutin") (r "^0.28") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (f (quote ("web"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1p3431b5301hlphkvpnymdvfjrk7hpxh2fs2d5nzlhkcbml0ndpw")))

(define-public crate-spitfire-input-0.27.1 (c (n "spitfire-input") (v "0.27.1") (d (list (d (n "glutin") (r "^0.28") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (f (quote ("web"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1km1l0vczs0j06wbl117myhqj3xmgwipvb2341ixw3rvw3b6gaia")))

(define-public crate-spitfire-input-0.27.2 (c (n "spitfire-input") (v "0.27.2") (d (list (d (n "glutin") (r "^0.28") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (f (quote ("web"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "13yxrhxsivjxcwhzl81y6ccc1swlfg2l4fvqd9039dlfpfxw9lv1")))

(define-public crate-spitfire-input-0.28.0 (c (n "spitfire-input") (v "0.28.0") (d (list (d (n "glutin") (r "^0.28") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (f (quote ("web"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0rydcnyf0n8h3pan2kdyhwqwkhk3zw1s5fb41snzackzl0ns27pq")))

(define-public crate-spitfire-input-0.28.1 (c (n "spitfire-input") (v "0.28.1") (d (list (d (n "glutin") (r "^0.28") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (f (quote ("web"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0b6kr26770n14yl66446fzvaa60ni8fmvmzfll2a7jcpvm89027w")))

(define-public crate-spitfire-input-0.28.2 (c (n "spitfire-input") (v "0.28.2") (d (list (d (n "glutin") (r "^0.28") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (f (quote ("web"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0b443z91ij722fww3w7smihhrczcfb58byjlswnylk1zwr46xvsa")))

(define-public crate-spitfire-input-0.28.3 (c (n "spitfire-input") (v "0.28.3") (d (list (d (n "glutin") (r "^0.28") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (f (quote ("web"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0887qhngmmvdx24axdr386b2ndsbar4phnd1vs8ycardnrnc1x0x")))

(define-public crate-spitfire-input-0.28.4 (c (n "spitfire-input") (v "0.28.4") (d (list (d (n "glutin") (r "^0.28") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "typid") (r "^1") (f (quote ("web"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "177bcs5w4zjzpg8i9jwxx62nzy43w94m146d89xqcb0w7fk09k52")))

