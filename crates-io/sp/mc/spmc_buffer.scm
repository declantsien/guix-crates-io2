(define-module (crates-io sp mc spmc_buffer) #:use-module (crates-io))

(define-public crate-spmc_buffer-0.1.0 (c (n "spmc_buffer") (v "0.1.0") (h "1ajsx5ap6aahmk91gx80jd5ffyy81pdhfg0699n9y4l33pbx1rj2")))

(define-public crate-spmc_buffer-0.2.0 (c (n "spmc_buffer") (v "0.2.0") (h "1yh0xyrz62p8lj75byrahfm2znjq2ww7idjqp4nhxbycn850hmsa")))

(define-public crate-spmc_buffer-0.3.0 (c (n "spmc_buffer") (v "0.3.0") (h "0fxywyhswq6i07i94p87fcyv9v968drmbpkiwwsr8jbfam1h47i3")))

(define-public crate-spmc_buffer-0.3.1 (c (n "spmc_buffer") (v "0.3.1") (d (list (d (n "testbench") (r "^0") (d #t) (k 0)))) (h "05qf2gn6a43q96n0hf09d7viny3wnr2sgy1svcafxmys3zwv4nic")))

(define-public crate-spmc_buffer-0.4.0 (c (n "spmc_buffer") (v "0.4.0") (d (list (d (n "testbench") (r "^0") (d #t) (k 0)))) (h "0k0773ksq9amhhqkliqydwdrigpnc08a227a11qpchbgj55isk3f")))

(define-public crate-spmc_buffer-0.4.1 (c (n "spmc_buffer") (v "0.4.1") (d (list (d (n "testbench") (r "^0") (d #t) (k 0)))) (h "0f9vnf34csff7r7i84plmh5ms2jk8c3xg8mqi35rzmnr3kdc063w")))

(define-public crate-spmc_buffer-0.4.2 (c (n "spmc_buffer") (v "0.4.2") (d (list (d (n "testbench") (r "^0") (d #t) (k 0)))) (h "0sn6nf4s183vib3wp7najyyldbc064nnm1g46zn7dp0wzp8ynqgh")))

(define-public crate-spmc_buffer-0.5.0 (c (n "spmc_buffer") (v "0.5.0") (d (list (d (n "testbench") (r "^0") (d #t) (k 0)))) (h "1pww8z6gys05ygm5zvv53z4w7xl7ps470lx0n2zr0wdw2zgwr9j2")))

(define-public crate-spmc_buffer-0.5.1 (c (n "spmc_buffer") (v "0.5.1") (d (list (d (n "testbench") (r "^0") (d #t) (k 0)))) (h "0wcda2zn3fcj7gnp6ifcshy8a864sszpg70ik3yvs36v8fvp0363")))

