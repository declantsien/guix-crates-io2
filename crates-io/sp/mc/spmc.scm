(define-module (crates-io sp mc spmc) #:use-module (crates-io))

(define-public crate-spmc-0.1.0 (c (n "spmc") (v "0.1.0") (h "1a0h8azqk8qz3f02z7cygzzmqncl7i4pfz0j7nl78rbvnpihxnsb") (y #t)))

(define-public crate-spmc-0.2.0 (c (n "spmc") (v "0.2.0") (h "07jwh5f2zqhfl5bkh4sxmzpz3w161zy715fjqf7rgj6wz1lk6k64") (y #t)))

(define-public crate-spmc-0.2.1 (c (n "spmc") (v "0.2.1") (h "1pssc006fwcbxjji67kzy8nzzsjrm3zqhwyiqj8ya4x4q5hspgck") (y #t)))

(define-public crate-spmc-0.2.2 (c (n "spmc") (v "0.2.2") (h "06mdy6zzjwld5jgb1zfrmzrg5yvf31dbiq2wwls1im2zzg8i27yd") (y #t)))

(define-public crate-spmc-0.2.3 (c (n "spmc") (v "0.2.3") (d (list (d (n "loom") (r "^0.2.2") (d #t) (k 2)))) (h "1nhbjc65avbb4nffk6b49spbv7rsmmnrppj2qnx39mhwi57spgiw") (y #t)))

(define-public crate-spmc-0.3.0 (c (n "spmc") (v "0.3.0") (d (list (d (n "loom") (r "^0.2.2") (d #t) (k 2)))) (h "1rgcqgj6b3d0cshi7277akr2xk0cx11rkmviaahy7a3pla6l5a02")))

