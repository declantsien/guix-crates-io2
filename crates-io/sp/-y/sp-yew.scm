(define-module (crates-io sp -y sp-yew) #:use-module (crates-io))

(define-public crate-sp-yew-0.1.0 (c (n "sp-yew") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sp-dto") (r "^0.1") (d #t) (k 0)) (d (n "yew") (r "^0.17") (f (quote ("web_sys"))) (d #t) (k 0)))) (h "129jvwqlhhc0sgkp6mvb7mjks5mfg90vbjjj9nkp0vcvm8ixigsz")))

