(define-module (crates-io sp pp sppparse_derive) #:use-module (crates-io))

(define-public crate-sppparse_derive-0.1.0 (c (n "sppparse_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0bc4c273rrr10z219hl7xg46phafhsrrn4bbpnbs2qq182b4xaw9")))

(define-public crate-sppparse_derive-0.1.1 (c (n "sppparse_derive") (v "0.1.1") (d (list (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "09mjgh6ac3fivpsk05pd2fflnrw5ip4wcfk2nvf5dqqvjrlwyx94")))

(define-public crate-sppparse_derive-0.1.2 (c (n "sppparse_derive") (v "0.1.2") (d (list (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1kx2mqn4j8p7vwzxbx2vnkb33qwp9bb7zhzrg49ma6wyj348z3pb")))

(define-public crate-sppparse_derive-0.1.3 (c (n "sppparse_derive") (v "0.1.3") (d (list (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0jny2j8f0g8x83qrxyrnjxgs3fa1h7is7xkylzbbg34nshyrp27p")))

