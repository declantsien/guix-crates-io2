(define-module (crates-io sp -k sp-keyring) #:use-module (crates-io))

(define-public crate-sp-keyring-2.0.0-alpha.2 (c (n "sp-keyring") (v "2.0.0-alpha.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-alpha.2") (d #t) (k 0)) (d (n "sp-runtime") (r "^2.0.0-alpha.2") (d #t) (k 0)) (d (n "strum") (r "^0.16.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "180ld2y05h0wc4dnka8wz8wpq144wna8w6z29lrycs9xzrk0chsb")))

(define-public crate-sp-keyring-2.0.0-alpha.3 (c (n "sp-keyring") (v "2.0.0-alpha.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-alpha.2") (d #t) (k 0)) (d (n "sp-runtime") (r "^2.0.0-alpha.2") (d #t) (k 0)) (d (n "strum") (r "^0.16.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lz0jsld1gm6x1d4p8r8lfh4xdl2dl8b93bk34jvjyqraziy8l1s")))

(define-public crate-sp-keyring-2.0.0-alpha.5 (c (n "sp-keyring") (v "2.0.0-alpha.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-alpha.5") (d #t) (k 0)) (d (n "sp-runtime") (r "^2.0.0-alpha.5") (d #t) (k 0)) (d (n "strum") (r "^0.16.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10hx22iv9q7l6hfbhlxvzdxg3f6b2j5s9d7i2jfacgzhri8avz0f")))

(define-public crate-sp-keyring-2.0.0-alpha.6 (c (n "sp-keyring") (v "2.0.0-alpha.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "sp-runtime") (r "^2.0.0-alpha.6") (d #t) (k 0)) (d (n "strum") (r "^0.16.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vk8d0yi8nla1s62didzpnrgh4pwr255b19p1lr4v2ixn5gfcwcz")))

(define-public crate-sp-keyring-2.0.0-alpha.7 (c (n "sp-keyring") (v "2.0.0-alpha.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-alpha.7") (d #t) (k 0)) (d (n "sp-runtime") (r "^2.0.0-alpha.7") (d #t) (k 0)) (d (n "strum") (r "^0.16.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "092w420h3ffcyilckg98w0p73iifz7il0cgarcpm7in8p2l3k7jh")))

(define-public crate-sp-keyring-2.0.0-alpha.8 (c (n "sp-keyring") (v "2.0.0-alpha.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-alpha.8") (d #t) (k 0)) (d (n "sp-runtime") (r "^2.0.0-alpha.8") (d #t) (k 0)) (d (n "strum") (r "^0.16.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1krbw5d8jlw88bp55g99kd46r0x51c59hzcgsvl16d2vznhi8msi")))

(define-public crate-sp-keyring-2.0.0-rc1 (c (n "sp-keyring") (v "2.0.0-rc1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-rc1") (d #t) (k 0)) (d (n "sp-runtime") (r "^2.0.0-rc1") (d #t) (k 0)) (d (n "strum") (r "^0.16.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hwwkmzcqhkyq2brjlpyry2vi3bssmllzi2hxxgfjppparwsiikl")))

(define-public crate-sp-keyring-2.0.0-rc2 (c (n "sp-keyring") (v "2.0.0-rc2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-rc2") (d #t) (k 0)) (d (n "sp-runtime") (r "^2.0.0-rc2") (d #t) (k 0)) (d (n "strum") (r "^0.16.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xw8jxa1gv4d2vraq4rl6g702kjgv5lc9kc7qxb8cnkbadc53r08")))

(define-public crate-sp-keyring-2.0.0-rc3 (c (n "sp-keyring") (v "2.0.0-rc3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-rc3") (d #t) (k 0)) (d (n "sp-runtime") (r "^2.0.0-rc3") (d #t) (k 0)) (d (n "strum") (r "^0.16.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14h6la7qhphyf5f83vcc4hgv7p3rf00kzd6hxpycir39rmxzhrm9")))

(define-public crate-sp-keyring-2.0.0-rc4 (c (n "sp-keyring") (v "2.0.0-rc4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-rc4") (d #t) (k 0)) (d (n "sp-runtime") (r "^2.0.0-rc4") (d #t) (k 0)) (d (n "strum") (r "^0.16.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gfm9k7hi1bksc48kfkxrikpyyvyw9ixhvm7nbaar6adfidsp2f7")))

(define-public crate-sp-keyring-2.0.0-rc5 (c (n "sp-keyring") (v "2.0.0-rc5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-rc5") (d #t) (k 0)) (d (n "sp-runtime") (r "^2.0.0-rc5") (d #t) (k 0)) (d (n "strum") (r "^0.16.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gndsgaasbapjv0izycf0rnx074i04ad985qzii9xzlgwhp2gxm8")))

(define-public crate-sp-keyring-2.0.0-rc6 (c (n "sp-keyring") (v "2.0.0-rc6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0-rc6") (d #t) (k 0)) (d (n "sp-runtime") (r "^2.0.0-rc6") (d #t) (k 0)) (d (n "strum") (r "^0.16.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ibgmqpq8hfxx0v2wk192hivm59jvdvy54n4ffchp9b1mh42jiwr")))

(define-public crate-sp-keyring-2.0.0 (c (n "sp-keyring") (v "2.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^2.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.16.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04sclpvs1fwmb0x1rgm3jsyw68mn13bs47lk4f2xa65j4zmzwxiz")))

(define-public crate-sp-keyring-2.0.1 (c (n "sp-keyring") (v "2.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^2.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^2.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.16.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hnhws5cvkpd0lk2v6dz8x4fdksr1pjglnfjmphrcd5k0zqdqhcp")))

(define-public crate-sp-keyring-3.0.0 (c (n "sp-keyring") (v "3.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^3.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^3.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ypgfk70z6rwvn1s09yz79fw1r7jxr0fp9jak3a4h82bx6qg4nbv")))

(define-public crate-sp-keyring-4.0.0 (c (n "sp-keyring") (v "4.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^4.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^4.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.22.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xr090zg98zadg8g667k110j3bv2yvlyjfc3190n27hn9hqfyvc8")))

(define-public crate-sp-keyring-5.0.0 (c (n "sp-keyring") (v "5.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^5.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^5.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.23.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vjdxh3jpngwxwbv980dsv48v76v2db66vllcvlvgj2paxf9ampk")))

(define-public crate-sp-keyring-6.0.0 (c (n "sp-keyring") (v "6.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^6.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^6.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.23.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wsg014psyllf7mv81xr0zg46mal0z8fn56vi838fir2k8shbblj")))

(define-public crate-sp-keyring-7.0.0 (c (n "sp-keyring") (v "7.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^7.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^7.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ncm85wxw4f7xwc65v418lblxgjg3nx44ljxsvky3dwzcx3camzz")))

(define-public crate-sp-keyring-8.0.0 (c (n "sp-keyring") (v "8.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^8.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^8.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1shfqamb97k1hgiyfyixri42q96618fcgaz83xxkfj47g9zz1v5n")))

(define-public crate-sp-keyring-9.0.0 (c (n "sp-keyring") (v "9.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^9.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^9.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1b2lw8hfxvwsrs7f1lmgygv5bb4jq7bgv5d7cxc99ch8v4qv19y3")))

(define-public crate-sp-keyring-10.0.0 (c (n "sp-keyring") (v "10.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^10.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^10.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zfasccp8rzlqkfl3ircrcq1mcwwj66qq32fsbv9zzb3rw5w3pgl")))

(define-public crate-sp-keyring-11.0.0 (c (n "sp-keyring") (v "11.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^10.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^11.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "03md0273s86yx0yk61kcpd4z96h22r19b1vn7bcsbmsmlx6vhvmp")))

(define-public crate-sp-keyring-12.0.0 (c (n "sp-keyring") (v "12.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^11.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^12.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "019qn3kgq4hz1kxgkqsgzxycjsf2f99ibzk8slajlgsg542jaf8a")))

(define-public crate-sp-keyring-13.0.0 (c (n "sp-keyring") (v "13.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^11.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^13.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ndg1iv3a6hxk92qh1xsg4a0bswxr9ffpcgf00wp8n1n6k51zj0s")))

(define-public crate-sp-keyring-14.0.0 (c (n "sp-keyring") (v "14.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^12.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^14.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "15yy2vyzlrwzdnq7b1n75dzwm212p6fvib2981zwyrb5clgv26iv")))

(define-public crate-sp-keyring-15.0.0 (c (n "sp-keyring") (v "15.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^13.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^15.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "04mrbrcja56887rnhkpz3axnjb5snhzzcwxzjmkah0a19lci7303")))

(define-public crate-sp-keyring-16.0.0 (c (n "sp-keyring") (v "16.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^14.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^16.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "079szpz59811dkj2d05hs1x15gqx7vpfdlykmp2hx19hlbd48kf4")))

(define-public crate-sp-keyring-17.0.0 (c (n "sp-keyring") (v "17.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^15.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^17.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nss9w3vvxx5w118x7zgjk3h7pk4qlb17j78gd0mvcyk1ddc84pr")))

(define-public crate-sp-keyring-18.0.0 (c (n "sp-keyring") (v "18.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^16.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^18.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (k 0)))) (h "137w03m0pdj4625pd4wm49757yma7r769s6bnmfihpn1rmla24yc")))

(define-public crate-sp-keyring-19.0.0 (c (n "sp-keyring") (v "19.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^17.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^19.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (k 0)))) (h "13ysvjsn6lhx31l97d9j5vw0alcw52xw5pfbw321vclsb7j935g2")))

(define-public crate-sp-keyring-20.0.0 (c (n "sp-keyring") (v "20.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^18.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^20.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (k 0)))) (h "0v2r97rwrxayw0hlbbahzag1rcgk61s168jbq0rm77wfj19w6whp")))

(define-public crate-sp-keyring-21.0.0 (c (n "sp-keyring") (v "21.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^18.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^21.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (k 0)))) (h "0y97z2a282q70fvlzl6rjsw426sw7m37q7is2ivi4pwdlxva3iyg")))

(define-public crate-sp-keyring-22.0.0 (c (n "sp-keyring") (v "22.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^19.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^22.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (k 0)))) (h "18qqsm2r7g40iqfz6nkrfk7jpwfnhsbhffcw8y61a2m2r4q6krz4")))

(define-public crate-sp-keyring-23.0.0 (c (n "sp-keyring") (v "23.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^20.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^23.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (k 0)))) (h "0z6plyqf18l855qb5bmskaqy8zklpzim0xp88f9d64chz1vgrw7m")))

(define-public crate-sp-keyring-24.0.0 (c (n "sp-keyring") (v "24.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^21.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^24.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (k 0)))) (h "1cfsl4ncp7fs1w07xlncz8niijswn9d622afpjl081aq91940ws6")))

(define-public crate-sp-keyring-24.1.0-dev.2 (c (n "sp-keyring") (v "24.1.0-dev.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^21.1.0-dev.2") (d #t) (k 0)) (d (n "sp-runtime") (r "^24.1.0-dev.2") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (k 0)))) (h "07aqw1bs9khz97krb57q22dpfkqrvi74bg8c2c49y8djaiicl6w2")))

(define-public crate-sp-keyring-24.1.0-dev.3 (c (n "sp-keyring") (v "24.1.0-dev.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^21.1.0-dev.3") (d #t) (k 0)) (d (n "sp-runtime") (r "^24.1.0-dev.3") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (k 0)))) (h "1b5jda3d9aq2dvnrnxwwplnbn3iy9d7iirys5wc3rwhbk6114p0z")))

(define-public crate-sp-keyring-24.1.0-dev.4 (c (n "sp-keyring") (v "24.1.0-dev.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^21.1.0-dev.4") (d #t) (k 0)) (d (n "sp-runtime") (r "^24.1.0-dev.4") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (k 0)))) (h "14c8g1n2nilhhs7c3drbh3xv91xg24ifi48hjjicn64nib490wpp")))

(define-public crate-sp-keyring-24.1.0-dev.5 (c (n "sp-keyring") (v "24.1.0-dev.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "=21.1.0-dev.5") (d #t) (k 0)) (d (n "sp-runtime") (r "=24.1.0-dev.5") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (k 0)))) (h "08rnzc8d15xpxlrp4p6v8chg4981fify24rbsfrvg9r04bafwqfh")))

(define-public crate-sp-keyring-24.1.0-dev.6 (c (n "sp-keyring") (v "24.1.0-dev.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "=21.1.0-dev.6") (d #t) (k 0)) (d (n "sp-runtime") (r "=24.1.0-dev.6") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (k 0)))) (h "0p45wg4dljyx4ij8jbzmylanc85g4k15vh71212pd3bk1h3x3kkd")))

(define-public crate-sp-keyring-25.0.0 (c (n "sp-keyring") (v "25.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^22.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^25.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (k 0)))) (h "1cb821x1619xf1ii37apwshqask2gzady31214hrkzj7w0myka58")))

(define-public crate-sp-keyring-26.0.0 (c (n "sp-keyring") (v "26.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^23.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^26.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (k 0)))) (h "12pi71dg9ac1gkbng0ia2z4p5f19ybp4ssx5jz8mz79lmlpwmz4x")))

(define-public crate-sp-keyring-27.0.0-dev.1 (c (n "sp-keyring") (v "27.0.0-dev.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "=24.0.0-dev.1") (d #t) (k 0)) (d (n "sp-runtime") (r "=27.0.0-dev.1") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (k 0)))) (h "142r9cm819v5krabq304vznqbrljks12v069mh0mmqvaqpvx8xfy")))

(define-public crate-sp-keyring-27.0.0 (c (n "sp-keyring") (v "27.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^24.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^27.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (k 0)))) (h "1cwpmkz1s59fpb7ykxkqc7v0ykamb33s1d746phk68jdackrkw05")))

(define-public crate-sp-keyring-28.0.0 (c (n "sp-keyring") (v "28.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^25.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^28.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (k 0)))) (h "15gf02d40y02wmk7bk6ffawdwn3xn1vacfijzclr1jxrbjrw0pk5")))

(define-public crate-sp-keyring-29.0.0 (c (n "sp-keyring") (v "29.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^26.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^29.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (k 0)))) (h "1k48ajgd5ppvw5ri842d5a9i88lx0z5r4kaxspl6b503chnbykk7")))

(define-public crate-sp-keyring-30.0.0 (c (n "sp-keyring") (v "30.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "sp-core") (r "^27.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^30.0.1") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (k 0)))) (h "0drc2b0nyxhc2xgv9alzdg7mqzd3k2ck369hgsx8d02njgqgsp94")))

(define-public crate-sp-keyring-31.0.0 (c (n "sp-keyring") (v "31.0.0") (d (list (d (n "sp-core") (r "^28.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^31.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (k 0)))) (h "1njpynargagb5c1r827dckd2mcz8dzavpiirinwcva15qvkmq5lq")))

(define-public crate-sp-keyring-32.0.0 (c (n "sp-keyring") (v "32.0.0") (d (list (d (n "sp-core") (r "^29.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^32.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (k 0)))) (h "00hvw710larpagwzh8aaxmfq239cpjdv7j9br5plcn0ri0phmkv9")))

(define-public crate-sp-keyring-33.0.0 (c (n "sp-keyring") (v "33.0.0") (d (list (d (n "sp-core") (r "^30.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^33.0.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (k 0)))) (h "1mmhrpklcg6r1dgdv5qxdn0r22pxxps39pzxklq4afsrirj7973z")))

(define-public crate-sp-keyring-34.0.0 (c (n "sp-keyring") (v "34.0.0") (d (list (d (n "sp-core") (r "^31.0.0") (k 0)) (d (n "sp-runtime") (r "^34.0.0") (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (k 0)))) (h "0kzkly72w4ba73f3dr0d17wd4zxgjyl89msq6hx5nw3db7d32ym0") (f (quote (("std" "sp-core/std" "sp-runtime/std" "strum/std") ("default" "std"))))))

(define-public crate-sp-keyring-35.0.0 (c (n "sp-keyring") (v "35.0.0") (d (list (d (n "sp-core") (r "^32.0.0") (k 0)) (d (n "sp-runtime") (r "^35.0.0") (k 0)) (d (n "strum") (r "^0.26.2") (f (quote ("derive"))) (k 0)))) (h "00gjc9hw9pxfg4xr46imiqrsr4d3yj3y4kb6vqv4lssaik8ab788") (f (quote (("std" "sp-core/std" "sp-runtime/std" "strum/std") ("default" "std"))))))

(define-public crate-sp-keyring-36.0.0 (c (n "sp-keyring") (v "36.0.0") (d (list (d (n "sp-core") (r "^32.0.0") (k 0)) (d (n "sp-runtime") (r "^36.0.0") (k 0)) (d (n "strum") (r "^0.26.2") (f (quote ("derive"))) (k 0)))) (h "1kk68ijkq62czi5fl01caln9p53lr22rfqzc0kz43lcb4jaw9lik") (f (quote (("std" "sp-core/std" "sp-runtime/std" "strum/std") ("default" "std"))))))

(define-public crate-sp-keyring-37.0.0 (c (n "sp-keyring") (v "37.0.0") (d (list (d (n "sp-core") (r "^33.0.0") (k 0)) (d (n "sp-runtime") (r "^37.0.0") (k 0)) (d (n "strum") (r "^0.26.2") (f (quote ("derive"))) (k 0)))) (h "1v164ini918ypwj14j25qly9vd89a3ilb72d7dpddi77x434b8k5") (f (quote (("std" "sp-core/std" "sp-runtime/std" "strum/std") ("default" "std"))))))

