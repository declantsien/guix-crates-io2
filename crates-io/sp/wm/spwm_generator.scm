(define-module (crates-io sp wm spwm_generator) #:use-module (crates-io))

(define-public crate-spwm_generator-0.1.0 (c (n "spwm_generator") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rayon") (r "^1.6.0") (d #t) (k 0)))) (h "027can4ly9cqzn968lxn6m8krf2rxfxp6h9p8n4jci36phlblk7q")))

(define-public crate-spwm_generator-0.2.0 (c (n "spwm_generator") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rayon") (r "^1.6.0") (d #t) (k 0)))) (h "185ms5dlww8x9b4dzpv6z9aagi7jhd722qd87jds64kw4nrffh57")))

(define-public crate-spwm_generator-0.3.0 (c (n "spwm_generator") (v "0.3.0") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)) (d (n "rayon") (r "^1.6.0") (d #t) (k 0)))) (h "15sl8bbi1j91mm4ljzb1cjzgzh8xjmx9yp317y6w1q0an7v4d6nx")))

