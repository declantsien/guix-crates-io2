(define-module (crates-io sp l_ spl_frontend_macros) #:use-module (crates-io))

(define-public crate-spl_frontend_macros-0.1.0 (c (n "spl_frontend_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "13awf3hvjb0zxnl1d7slpv7lg7mhp0q5zilb10k91mbmbp5p7icj")))

(define-public crate-spl_frontend_macros-0.1.1 (c (n "spl_frontend_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "059xhpfl640j3dmqm90aqlp9nnl0rxiax8mc5hbmbddh98f0v4l9")))

