(define-module (crates-io sp l_ spl_tool) #:use-module (crates-io))

(define-public crate-spl_tool-0.1.0 (c (n "spl_tool") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1pkf09b8pb93anyzzlnc8jv0v4h1d8jbqyww8l6mqmrvwnv6gcfx") (f (quote (("cli" "clap" "env_logger"))))))

(define-public crate-spl_tool-0.1.1 (c (n "spl_tool") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1cb7m3mpfwqkjngq76j3wilhwv83pahv4slj14x8h5qc44wk6983") (f (quote (("cli" "clap" "env_logger"))))))

(define-public crate-spl_tool-0.1.2 (c (n "spl_tool") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "16dicpd24j4rnk8gw4wrqkd4kaavlp5glxgi9j3wc2hbg7y8wkwn") (f (quote (("cli" "clap" "env_logger"))))))

(define-public crate-spl_tool-0.2.0 (c (n "spl_tool") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "19myk613z0ws8qqlxklz2yzcx1c1hj1sx697bad190ilsm7c2q5p") (f (quote (("cli" "clap" "env_logger"))))))

