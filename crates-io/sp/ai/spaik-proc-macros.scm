(define-module (crates-io sp ai spaik-proc-macros) #:use-module (crates-io))

(define-public crate-spaik-proc-macros-0.1.0 (c (n "spaik-proc-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19w10w6jkhslw6jblm2l4jfg4d5pllv70p6mg45298awnw3fc7l8")))

(define-public crate-spaik-proc-macros-0.2.0 (c (n "spaik-proc-macros") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08crqmqn6qaak33fwvhgmznp49sxzvd3xp2qx9ighw7zmmm7khkf")))

(define-public crate-spaik-proc-macros-0.2.1 (c (n "spaik-proc-macros") (v "0.2.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nzmdy2jj13p92mknhdv8v436fc988rpjgsmg8arxmswphrgqrh7")))

(define-public crate-spaik-proc-macros-0.3.0 (c (n "spaik-proc-macros") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mm0skaj0hn6yh542yvdwqwrmqy9q8bza47yvy9nvcck2v1cava0")))

(define-public crate-spaik-proc-macros-0.4.0 (c (n "spaik-proc-macros") (v "0.4.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0730gaqdflhix1aszqpg327bpm8z8hcbmznz9zz3fa33pgy8wpja")))

(define-public crate-spaik-proc-macros-0.5.0 (c (n "spaik-proc-macros") (v "0.5.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dnlxi54ras6wlwk8x44s7s7mk1p9jp16pmnn5pxnjg8ca0iqyd2")))

(define-public crate-spaik-proc-macros-0.5.1 (c (n "spaik-proc-macros") (v "0.5.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sriw2sp8ri2jxyx6x22gc6bjk6if87vzbgg78fraylbgi27is6c")))

(define-public crate-spaik-proc-macros-0.5.2 (c (n "spaik-proc-macros") (v "0.5.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zv3parkdvvs20g0cl0pilsf5d6fmdq6r44gnxyj9rpw9bxjiriz")))

