(define-module (crates-io sp ai spain-vat-id) #:use-module (crates-io))

(define-public crate-spain-vat-id-0.1.0 (c (n "spain-vat-id") (v "0.1.0") (h "1xl9jcss775chpc6fnv7c2s61595rpyxaj9jgx2l91j27qhjlhci") (y #t)))

(define-public crate-spain-vat-id-0.1.1 (c (n "spain-vat-id") (v "0.1.1") (h "0nadyg3dhvambphly34k0gyd8rnsmb12ipfc0lgnb1sxj163pczf")))

(define-public crate-spain-vat-id-0.1.2 (c (n "spain-vat-id") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0y9jgjbqjvn82c55a7hwsh8lqr9bcfkql6jw4hi27na0ljvz5lqa")))

