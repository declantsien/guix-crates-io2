(define-module (crates-io sp of spofy-cli) #:use-module (crates-io))

(define-public crate-spofy-cli-0.1.0 (c (n "spofy-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "directories") (r "^1.0.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.13.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde-value") (r "^0.5.3") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "spofy-core") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.18") (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "08bpjcnw0nl6yijknfi4v80vj1hq3gi48sx1s8h455d25cik9plm")))

