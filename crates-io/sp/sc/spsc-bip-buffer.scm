(define-module (crates-io sp sc spsc-bip-buffer) #:use-module (crates-io))

(define-public crate-spsc-bip-buffer-0.1.0 (c (n "spsc-bip-buffer") (v "0.1.0") (d (list (d (n "deterministic") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0bpwmg56zlzg42rh5pja6llpshz2gx6dd4c854dz6jk5l2m5zp2b") (f (quote (("test_nosched") ("default"))))))

(define-public crate-spsc-bip-buffer-0.1.1 (c (n "spsc-bip-buffer") (v "0.1.1") (d (list (d (n "cache_line_size") (r "^1.0") (d #t) (k 0)) (d (n "core_affinity") (r "^0.5.9") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0pvbncpf8yrwi8qav6l5w1bh50nzg08l6vx1w4z8bhbxf3jc93lw") (f (quote (("default"))))))

(define-public crate-spsc-bip-buffer-0.2.0 (c (n "spsc-bip-buffer") (v "0.2.0") (d (list (d (n "cache_line_size") (r "^1.0") (d #t) (k 0)) (d (n "core_affinity") (r "^0.5.9") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "streaming-harness-hdrhist") (r "^0.2") (d #t) (k 2)))) (h "1r7wlpz3p8242664p662dw4zr2g0kkvw973407asd759av7a8037") (f (quote (("default") ("debug"))))))

(define-public crate-spsc-bip-buffer-0.2.1 (c (n "spsc-bip-buffer") (v "0.2.1") (d (list (d (n "cache_line_size") (r "^1.0") (d #t) (k 0)) (d (n "core_affinity") (r "^0.5.9") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "streaming-harness-hdrhist") (r "^0.2") (d #t) (k 2)))) (h "0gwxg69iiypv4vz2k7fqzqskp5872h5pbdlc9kbi1d1q5r8kb28j") (f (quote (("nightly_perf_example") ("default") ("debug"))))))

