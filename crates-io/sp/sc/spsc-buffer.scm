(define-module (crates-io sp sc spsc-buffer) #:use-module (crates-io))

(define-public crate-spsc-buffer-0.1.0 (c (n "spsc-buffer") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "186yl97j4h9xfclzpqqn0bki5mhr15grc7img0hsw92ciyig19g4")))

(define-public crate-spsc-buffer-0.1.1 (c (n "spsc-buffer") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0fsv5zpxkax2n46flxhyajq1yblgh8f33la39gp86hksqcwkyv5y")))

