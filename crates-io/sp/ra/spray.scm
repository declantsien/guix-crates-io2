(define-module (crates-io sp ra spray) #:use-module (crates-io))

(define-public crate-spray-0.1.0 (c (n "spray") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "bytesize") (r "^1.0.1") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1g27rw1nkvqhm4ab1n1lc4id5mnyim6m7hsxw5mlbf3x212fl1dq")))

