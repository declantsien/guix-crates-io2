(define-module (crates-io sp ra sprattus-derive) #:use-module (crates-io))

(define-public crate-sprattus-derive-0.0.1 (c (n "sprattus-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0knzxi5ajjgbyf2yvyrsqwmg200sc8cfr11m6yljjqgniws8bc47")))

