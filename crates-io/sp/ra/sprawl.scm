(define-module (crates-io sp ra sprawl) #:use-module (crates-io))

(define-public crate-sprawl-0.1.0 (c (n "sprawl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "httptest") (r "^0.15.4") (d #t) (k 2)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (d #t) (k 0)) (d (n "soup") (r "^0.5.1") (d #t) (k 2)) (d (n "tokio") (r "^1.18.2") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.34") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1abq6imcrmd4sghf83q7lix0ll9y2xwlimnmw12g5sd3cnb6bs9n")))

