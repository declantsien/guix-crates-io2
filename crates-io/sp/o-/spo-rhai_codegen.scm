(define-module (crates-io sp o- spo-rhai_codegen) #:use-module (crates-io))

(define-public crate-spo-rhai_codegen-2.0.0 (c (n "spo-rhai_codegen") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("full" "parsing" "printing" "proc-macro" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.0") (d #t) (k 2)))) (h "1h3gminnxa25q3jwg2n9jxcal05a76jknpa3n24d3w305ms7mn6h") (f (quote (("metadata") ("default"))))))

