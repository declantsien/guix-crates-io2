(define-module (crates-io sp ol spolyfy) #:use-module (crates-io))

(define-public crate-spolyfy-1.0.0 (c (n "spolyfy") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "dbus") (r "^0.9.5") (d #t) (k 0)))) (h "0p1v1wmgkg1i67zb01zij1rdbvp8ipzb17igg7d5wlpj94zdkxi7") (y #t)))

(define-public crate-spolyfy-1.0.1 (c (n "spolyfy") (v "1.0.1") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "dbus") (r "^0.9.5") (d #t) (k 0)))) (h "1iidj7vdp45437jjbjf0pad91ni08lh376a85vc3xrmysi2ypfab") (y #t)))

(define-public crate-spolyfy-1.0.3 (c (n "spolyfy") (v "1.0.3") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "dbus") (r "^0.9.5") (d #t) (k 0)))) (h "0h32xih2dywhh8zksnlmx22m7ag8as6f4c0qxii4mnaqgq4zmrf6") (y #t)))

(define-public crate-spolyfy-1.0.5 (c (n "spolyfy") (v "1.0.5") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "dbus") (r "^0.9.5") (d #t) (k 0)))) (h "1lms3vyn1l6ld0b85h7s0fz5ls810ja7rkwd83ihmrs03qv5iry1")))

