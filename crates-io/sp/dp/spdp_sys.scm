(define-module (crates-io sp dp spdp_sys) #:use-module (crates-io))

(define-public crate-spdp_sys-0.1.0 (c (n "spdp_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "09ril3y43n5g7wm2y6b0kry799cb0nh2hxq554dkj0ahngba8wqs") (l "spdp")))

