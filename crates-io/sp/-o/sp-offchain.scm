(define-module (crates-io sp -o sp-offchain) #:use-module (crates-io))

(define-public crate-sp-offchain-2.0.0-alpha.2 (c (n "sp-offchain") (v "2.0.0-alpha.2") (d (list (d (n "sp-api") (r "^2.0.0-alpha.2") (k 0)) (d (n "sp-runtime") (r "^2.0.0-alpha.2") (k 0)))) (h "02nhj7mi6gjxj7rsbh48jczy4pqny0vmz1aki81nwc178p9jy0wa") (f (quote (("std" "sp-api/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-2.0.0-alpha.3 (c (n "sp-offchain") (v "2.0.0-alpha.3") (d (list (d (n "sp-api") (r "^2.0.0-alpha.2") (k 0)) (d (n "sp-runtime") (r "^2.0.0-alpha.2") (k 0)))) (h "01l8apd8amfq2bjbmc3fhnbb7mf1hyigwxnlncdps67ss797vli7") (f (quote (("std" "sp-api/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-2.0.0-alpha.5 (c (n "sp-offchain") (v "2.0.0-alpha.5") (d (list (d (n "sp-api") (r "^2.0.0-alpha.5") (k 0)) (d (n "sp-runtime") (r "^2.0.0-alpha.5") (k 0)))) (h "1ymspkvpn43jn9jjamvs0a4jdlqnyncflb7f28phjr3y7saifbr7") (f (quote (("std" "sp-api/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-2.0.0-alpha.6 (c (n "sp-offchain") (v "2.0.0-alpha.6") (d (list (d (n "sp-api") (r "^2.0.0-alpha.6") (k 0)) (d (n "sp-runtime") (r "^2.0.0-alpha.6") (k 0)))) (h "1z6hk8l8i64qmz2nrzvcwqa6yhlk2sjahn4nk7lfgazpa7m05z3d") (f (quote (("std" "sp-api/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-2.0.0-alpha.7 (c (n "sp-offchain") (v "2.0.0-alpha.7") (d (list (d (n "sp-api") (r "^2.0.0-alpha.7") (k 0)) (d (n "sp-core") (r "^2.0.0-alpha.7") (k 0)) (d (n "sp-runtime") (r "^2.0.0-alpha.7") (k 0)))) (h "14b5dpvyagzkhzp5vrbkb248vdrp4gv6k88l3abr1z6qxk094c76") (f (quote (("std" "sp-core/std" "sp-api/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-2.0.0-alpha.8 (c (n "sp-offchain") (v "2.0.0-alpha.8") (d (list (d (n "sp-api") (r "^2.0.0-alpha.8") (k 0)) (d (n "sp-core") (r "^2.0.0-alpha.8") (k 0)) (d (n "sp-runtime") (r "^2.0.0-alpha.8") (k 0)))) (h "12dslm8yq14cmd077fbf97y09my0znryqlk349spfskd6j8kkvra") (f (quote (("std" "sp-core/std" "sp-api/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-2.0.0-rc1 (c (n "sp-offchain") (v "2.0.0-rc1") (d (list (d (n "sp-api") (r "^2.0.0-rc1") (k 0)) (d (n "sp-core") (r "^2.0.0-rc1") (k 0)) (d (n "sp-runtime") (r "^2.0.0-rc1") (k 0)))) (h "1m0zbyl5nlzv115r4sz3k4b6nybzb5a7v3wfzs3kymwylv9h95gg") (f (quote (("std" "sp-core/std" "sp-api/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-2.0.0-rc2 (c (n "sp-offchain") (v "2.0.0-rc2") (d (list (d (n "sp-api") (r "^2.0.0-rc2") (k 0)) (d (n "sp-core") (r "^2.0.0-rc2") (k 0)) (d (n "sp-runtime") (r "^2.0.0-rc2") (k 0)))) (h "1dih6qkdw2l409xn5mkd9lr0vvjxd4p27rixh7kv56v8r00h4x41") (f (quote (("std" "sp-core/std" "sp-api/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-2.0.0-rc3 (c (n "sp-offchain") (v "2.0.0-rc3") (d (list (d (n "sp-api") (r "^2.0.0-rc3") (k 0)) (d (n "sp-core") (r "^2.0.0-rc3") (k 0)) (d (n "sp-runtime") (r "^2.0.0-rc3") (k 0)))) (h "15b0fgjvilvkzisbw8hl6dyv7d0p04sr5nd3ma6ykcagprm3ms51") (f (quote (("std" "sp-core/std" "sp-api/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-2.0.0-rc4 (c (n "sp-offchain") (v "2.0.0-rc4") (d (list (d (n "sp-api") (r "^2.0.0-rc4") (k 0)) (d (n "sp-core") (r "^2.0.0-rc4") (k 0)) (d (n "sp-runtime") (r "^2.0.0-rc4") (k 0)))) (h "00imz2ikmscpcmc6sxi8f5pairj4w6jmvpiiwy74c0dzs4c2lz4x") (f (quote (("std" "sp-core/std" "sp-api/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-2.0.0-rc5 (c (n "sp-offchain") (v "2.0.0-rc5") (d (list (d (n "sp-api") (r "^2.0.0-rc5") (k 0)) (d (n "sp-core") (r "^2.0.0-rc5") (k 0)) (d (n "sp-runtime") (r "^2.0.0-rc5") (k 0)))) (h "10cqyc4z1fjs6ps7yw6lkz6mn4qnmwwj5zpdki70n73da7ayyak2") (f (quote (("std" "sp-core/std" "sp-api/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-2.0.0-rc6 (c (n "sp-offchain") (v "2.0.0-rc6") (d (list (d (n "sp-api") (r "^2.0.0-rc6") (k 0)) (d (n "sp-core") (r "^2.0.0-rc6") (k 0)) (d (n "sp-runtime") (r "^2.0.0-rc6") (k 0)))) (h "05rjhr27s8jkh0i1sbl5ygrc5w4h67grpq7f08933irlgy0j95zk") (f (quote (("std" "sp-core/std" "sp-api/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-2.0.0 (c (n "sp-offchain") (v "2.0.0") (d (list (d (n "sp-api") (r "^2.0.0") (k 0)) (d (n "sp-core") (r "^2.0.0") (k 0)) (d (n "sp-runtime") (r "^2.0.0") (k 0)))) (h "1b85689lg9l7kjw3j7mb88979p2gm7k8jigbv95dh2jin80y3mgv") (f (quote (("std" "sp-core/std" "sp-api/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-2.0.1 (c (n "sp-offchain") (v "2.0.1") (d (list (d (n "sp-api") (r "^2.0.0") (k 0)) (d (n "sp-core") (r "^2.0.0") (k 0)) (d (n "sp-runtime") (r "^2.0.0") (k 0)))) (h "0vrgjfsy04diafsxz6axzpjzbw1jaxqsx7l77kvdd7j5lnkm0kfm") (f (quote (("std" "sp-core/std" "sp-api/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-3.0.0 (c (n "sp-offchain") (v "3.0.0") (d (list (d (n "sp-api") (r "^3.0.0") (k 0)) (d (n "sp-core") (r "^3.0.0") (k 0)) (d (n "sp-runtime") (r "^3.0.0") (k 0)))) (h "0qqfaxrhfr8ncshiywv2m0alg45nj2nmjvf2fwp39j6wrvnhxn0b") (f (quote (("std" "sp-core/std" "sp-api/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-4.0.0 (c (n "sp-offchain") (v "4.0.0") (d (list (d (n "sp-api") (r "^4.0.0") (k 0)) (d (n "sp-core") (r "^8.0.0") (k 0)) (d (n "sp-runtime") (r "^8.0.0") (k 0)))) (h "050j4i6fj45i0885q8isrw911dv36837h5sg8fyg4xi13nk8y0ky") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-5.0.0 (c (n "sp-offchain") (v "5.0.0") (d (list (d (n "sp-api") (r "^5.0.0") (k 0)) (d (n "sp-core") (r "^9.0.0") (k 0)) (d (n "sp-runtime") (r "^9.0.0") (k 0)))) (h "11xzp1xinhxikl3p2h8q54nzx9l2lbb0wdnfwppr5g0qjbp85gsp") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-6.0.0 (c (n "sp-offchain") (v "6.0.0") (d (list (d (n "sp-api") (r "^6.0.0") (k 0)) (d (n "sp-core") (r "^10.0.0") (k 0)) (d (n "sp-runtime") (r "^10.0.0") (k 0)))) (h "0m73qb31ppnask640m21f184agzsfmlfs1yikx29xvsgci4idj4c") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-7.0.0 (c (n "sp-offchain") (v "7.0.0") (d (list (d (n "sp-api") (r "^7.0.0") (k 0)) (d (n "sp-core") (r "^10.0.0") (k 0)) (d (n "sp-runtime") (r "^11.0.0") (k 0)))) (h "12ga0ixi2vyjfiw84x332w0ml1rql3mbniqq9i4g0rdrkpwjrb02") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-8.0.0 (c (n "sp-offchain") (v "8.0.0") (d (list (d (n "sp-api") (r "^8.0.0") (k 0)) (d (n "sp-core") (r "^11.0.0") (k 0)) (d (n "sp-runtime") (r "^12.0.0") (k 0)))) (h "03pqjnrfqz3y5nz7plndjzka1d813hf8fpg0knlzxkda9afbdzvq") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-9.0.0 (c (n "sp-offchain") (v "9.0.0") (d (list (d (n "sp-api") (r "^9.0.0") (k 0)) (d (n "sp-core") (r "^11.0.0") (k 0)) (d (n "sp-runtime") (r "^13.0.0") (k 0)))) (h "05kp1blxiplkasra9x0fsjqj629ak4dn4q52fah77w5g6fw6yfzn") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-10.0.0 (c (n "sp-offchain") (v "10.0.0") (d (list (d (n "sp-api") (r "^10.0.0") (k 0)) (d (n "sp-core") (r "^12.0.0") (k 0)) (d (n "sp-runtime") (r "^14.0.0") (k 0)))) (h "0ksz6h5lmaxylwk8qzl0vddh2bbm8bxnx8z66z413fd93hl9p1f5") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-11.0.0 (c (n "sp-offchain") (v "11.0.0") (d (list (d (n "sp-api") (r "^11.0.0") (k 0)) (d (n "sp-core") (r "^13.0.0") (k 0)) (d (n "sp-runtime") (r "^15.0.0") (k 0)))) (h "1x8q3iq76f6mvfp8xgcl9ilkj8j4kly2c8kicy1ncizm0k2shxq8") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-12.0.0 (c (n "sp-offchain") (v "12.0.0") (d (list (d (n "sp-api") (r "^12.0.0") (k 0)) (d (n "sp-core") (r "^14.0.0") (k 0)) (d (n "sp-runtime") (r "^16.0.0") (k 0)))) (h "19kd35ijkrrfislfbssy2yg83ckhrc1f6wvgq8vlg0fsrbx0g406") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-13.0.0 (c (n "sp-offchain") (v "13.0.0") (d (list (d (n "sp-api") (r "^13.0.0") (k 0)) (d (n "sp-core") (r "^15.0.0") (k 0)) (d (n "sp-runtime") (r "^17.0.0") (k 0)))) (h "1nmbkrj2pspfkqpv5wz53m13868aaarf5q70bydk56sjhr2dkiba") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-14.0.0 (c (n "sp-offchain") (v "14.0.0") (d (list (d (n "sp-api") (r "^14.0.0") (k 0)) (d (n "sp-core") (r "^16.0.0") (k 0)) (d (n "sp-runtime") (r "^18.0.0") (k 0)))) (h "1k29w102af00vypzc8vb9w6xzsr5n4pzvb0lxhb0lqzj0dlqrl8s") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-15.0.0 (c (n "sp-offchain") (v "15.0.0") (d (list (d (n "sp-api") (r "^15.0.0") (k 0)) (d (n "sp-core") (r "^17.0.0") (k 0)) (d (n "sp-runtime") (r "^19.0.0") (k 0)))) (h "0xczp4rjq81zy0z7kblsyhcs7qk2n93zzf5bwaiw8rjfhvkkp5d6") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-16.0.0 (c (n "sp-offchain") (v "16.0.0") (d (list (d (n "sp-api") (r "^16.0.0") (k 0)) (d (n "sp-core") (r "^18.0.0") (k 0)) (d (n "sp-runtime") (r "^20.0.0") (k 0)))) (h "18hxq2jqsi06yvw9ikw1i0cm20pxnafradvwg7z976cdy3bfwid0") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-17.0.0 (c (n "sp-offchain") (v "17.0.0") (d (list (d (n "sp-api") (r "^17.0.0") (k 0)) (d (n "sp-core") (r "^18.0.0") (k 0)) (d (n "sp-runtime") (r "^21.0.0") (k 0)))) (h "001fqh2f8i4ks6rpr0rj6plq4djv0sd58ix5l3dz2xqsh2wyxcln") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-18.0.0 (c (n "sp-offchain") (v "18.0.0") (d (list (d (n "sp-api") (r "^18.0.0") (k 0)) (d (n "sp-core") (r "^19.0.0") (k 0)) (d (n "sp-runtime") (r "^22.0.0") (k 0)))) (h "0qn6zd8j4f0x1976m4ans7xif7wziwmgh40i72rdp2w4glydhfvv") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-19.0.0 (c (n "sp-offchain") (v "19.0.0") (d (list (d (n "sp-api") (r "^19.0.0") (k 0)) (d (n "sp-core") (r "^20.0.0") (k 0)) (d (n "sp-runtime") (r "^23.0.0") (k 0)))) (h "0ny3xnxx67n3mic9qvxc6w2krivz4i11w4brdgykkyh4whaa6v19") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-19.1.0-dev.2 (c (n "sp-offchain") (v "19.1.0-dev.2") (d (list (d (n "sp-api") (r "^19.1.0-dev.2") (k 0)) (d (n "sp-core") (r "^21.1.0-dev.2") (k 0)) (d (n "sp-runtime") (r "^24.1.0-dev.2") (k 0)))) (h "1jq7jqka6sl6zmb85iln243dvc7yv34ad5ypzn2xlvz8g9bxw19n") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-19.1.0-dev.3 (c (n "sp-offchain") (v "19.1.0-dev.3") (d (list (d (n "sp-api") (r "^19.1.0-dev.3") (k 0)) (d (n "sp-core") (r "^21.1.0-dev.3") (k 0)) (d (n "sp-runtime") (r "^24.1.0-dev.3") (k 0)))) (h "00jm9cixh09pawyap59rb0m1ynmkwq0nrprgkrydsg8xnxjhmb5s") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-19.1.0-dev.5 (c (n "sp-offchain") (v "19.1.0-dev.5") (d (list (d (n "sp-api") (r "=19.1.0-dev.5") (k 0)) (d (n "sp-core") (r "=21.1.0-dev.5") (k 0)) (d (n "sp-runtime") (r "=24.1.0-dev.5") (k 0)))) (h "15112kfz3zqjw8yq3167ja1xbhjg9zpwbcr92xgqc2grw9lramfh") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-19.1.0-dev.6 (c (n "sp-offchain") (v "19.1.0-dev.6") (d (list (d (n "sp-api") (r "=19.1.0-dev.6") (k 0)) (d (n "sp-core") (r "=21.1.0-dev.6") (k 0)) (d (n "sp-runtime") (r "=24.1.0-dev.6") (k 0)))) (h "1kyk4ncjqagns613lf8dklvgm3ljzn6bb259d7hx1kwa0bxqifkw") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-20.0.0 (c (n "sp-offchain") (v "20.0.0") (d (list (d (n "sp-api") (r "^20.0.0") (k 0)) (d (n "sp-core") (r "^22.0.0") (k 0)) (d (n "sp-runtime") (r "^25.0.0") (k 0)))) (h "0q44fx4dmz2jjgqmf8ln95qvq4p2rgilw20dm0r859y8mfzdzi37") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-21.0.0 (c (n "sp-offchain") (v "21.0.0") (d (list (d (n "sp-api") (r "^21.0.0") (k 0)) (d (n "sp-core") (r "^23.0.0") (k 0)) (d (n "sp-runtime") (r "^26.0.0") (k 0)))) (h "0sk3bmkxxgrbdwmpl4yr7pkhg2jcd3bnpmp60sfa7rh4c2cvkqf1") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-22.0.0-dev.1 (c (n "sp-offchain") (v "22.0.0-dev.1") (d (list (d (n "sp-api") (r "=22.0.0-dev.1") (k 0)) (d (n "sp-core") (r "=24.0.0-dev.1") (k 0)) (d (n "sp-runtime") (r "=27.0.0-dev.1") (k 0)))) (h "0xl0n6brxinvvy794lqi2q0lv5ydmnqzkvj62sm44d45vskrdd1d") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-22.0.0 (c (n "sp-offchain") (v "22.0.0") (d (list (d (n "sp-api") (r "^22.0.0") (k 0)) (d (n "sp-core") (r "^24.0.0") (k 0)) (d (n "sp-runtime") (r "^27.0.0") (k 0)))) (h "0vv5q45w118zcpi632wcd55l68dvr9fchj7zc88lgrpdnhg51cah") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-23.0.0 (c (n "sp-offchain") (v "23.0.0") (d (list (d (n "sp-api") (r "^23.0.0") (k 0)) (d (n "sp-core") (r "^25.0.0") (k 0)) (d (n "sp-runtime") (r "^28.0.0") (k 0)))) (h "0pfq1wyjffj2xzbdr589l0qi8im74lq6i6j4n1vrin9y0izj444k") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-24.0.0 (c (n "sp-offchain") (v "24.0.0") (d (list (d (n "sp-api") (r "^24.0.0") (k 0)) (d (n "sp-core") (r "^26.0.0") (k 0)) (d (n "sp-runtime") (r "^29.0.0") (k 0)))) (h "1va5nllbgipl3ylrkrvvw241qg3rlkxc8ywfxvf5hjdpz80n4sk3") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-25.0.0 (c (n "sp-offchain") (v "25.0.0") (d (list (d (n "sp-api") (r "^25.0.0") (k 0)) (d (n "sp-core") (r "^27.0.0") (k 0)) (d (n "sp-runtime") (r "^30.0.1") (k 0)))) (h "0p2xqibf52nicwba55mrxslyypf3xzswbrdbj3r5bajmrfkfc5jh") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-26.0.0 (c (n "sp-offchain") (v "26.0.0") (d (list (d (n "sp-api") (r "^26.0.0") (k 0)) (d (n "sp-core") (r "^28.0.0") (k 0)) (d (n "sp-runtime") (r "^31.0.0") (k 0)))) (h "1j8sysrp7fhk6iw3af78c1dm47yb1cfn939drf0pp76wjziv6igv") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-27.0.0 (c (n "sp-offchain") (v "27.0.0") (d (list (d (n "sp-api") (r "^27.0.0") (k 0)) (d (n "sp-core") (r "^29.0.0") (k 0)) (d (n "sp-runtime") (r "^32.0.0") (k 0)))) (h "06bcnq3s8dvhv92g0v61n5ig6s0i65qhbxn3pr1x3dp0viavk0sd") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-28.0.0 (c (n "sp-offchain") (v "28.0.0") (d (list (d (n "sp-api") (r "^28.0.0") (k 0)) (d (n "sp-core") (r "^30.0.0") (k 0)) (d (n "sp-runtime") (r "^33.0.0") (k 0)))) (h "1mcicxzbdphf4lh6456kg1z8hw10kbnz2pjgsg745lfnppkyy9pq") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-29.0.0 (c (n "sp-offchain") (v "29.0.0") (d (list (d (n "sp-api") (r "^29.0.0") (k 0)) (d (n "sp-core") (r "^31.0.0") (k 0)) (d (n "sp-runtime") (r "^34.0.0") (k 0)))) (h "1mqhpll6p26m93qlpkc94r4fgnma4lacxkiy0wp1vgyyswrdjhf0") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-30.0.0 (c (n "sp-offchain") (v "30.0.0") (d (list (d (n "sp-api") (r "^30.0.0") (k 0)) (d (n "sp-core") (r "^32.0.0") (k 0)) (d (n "sp-runtime") (r "^35.0.0") (k 0)))) (h "0id1gy2zq4pckazqvz94s857bp349xm9q2grx5chrr68vx2fmvsh") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-31.0.0 (c (n "sp-offchain") (v "31.0.0") (d (list (d (n "sp-api") (r "^31.0.0") (k 0)) (d (n "sp-core") (r "^32.0.0") (k 0)) (d (n "sp-runtime") (r "^36.0.0") (k 0)))) (h "0yq9104cszgi4km0k4vmbrb0h318r84chs7jz7vc4d6sdw4x5frc") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

(define-public crate-sp-offchain-32.0.0 (c (n "sp-offchain") (v "32.0.0") (d (list (d (n "sp-api") (r "^32.0.0") (k 0)) (d (n "sp-core") (r "^33.0.0") (k 0)) (d (n "sp-runtime") (r "^37.0.0") (k 0)))) (h "1qaw3l8pq52p1d8qvppk16z6grah92n9imc9gl49rdjcc7dbvryr") (f (quote (("std" "sp-api/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

