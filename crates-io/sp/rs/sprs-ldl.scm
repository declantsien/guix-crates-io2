(define-module (crates-io sp rs sprs-ldl) #:use-module (crates-io))

(define-public crate-sprs-ldl-0.1.0 (c (n "sprs-ldl") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)) (d (n "sprs") (r "^0.4.0-alpha.4") (d #t) (k 0)))) (h "00ga7bw7lfmxvhnmhkg8b09wwwi7xphk2vim4qqvpzcrivzv44ki")))

(define-public crate-sprs-ldl-0.2.0 (c (n "sprs-ldl") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.6.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "sprs") (r "^0.4.0") (d #t) (k 0)))) (h "049ck7bbs181a3ff30434wzplbr8lwyb0cprd581jnf9ph988sw6")))

(define-public crate-sprs-ldl-0.2.1 (c (n "sprs-ldl") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "sprs") (r "^0.5.0") (d #t) (k 0)))) (h "1ya2k4mkn5kb7nblia9kh9aadpdghz8rlcp1p8rk53dwwlpsmzrx")))

(define-public crate-sprs-ldl-0.3.0 (c (n "sprs-ldl") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "sprs") (r "^0.6.0") (d #t) (k 0)))) (h "0y302h8j92s076vp6scdhpkawc889jqm04bpj27s3pnxl759qpb2")))

(define-public crate-sprs-ldl-0.4.0 (c (n "sprs-ldl") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "sprs") (r "^0.6.0") (d #t) (k 0)))) (h "14m70fyvl2zgpvvvd8x2rmrwz59bp26lgbdmaccdiybzxmw8g79m")))

(define-public crate-sprs-ldl-0.4.1 (c (n "sprs-ldl") (v "0.4.1") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "sprs") (r "^0.6.5") (d #t) (k 0)))) (h "10pmlajrb66y4z4v4y40g2br2gpr6f67gpg5nzdb499kpmc9qb91")))

(define-public crate-sprs-ldl-0.5.0 (c (n "sprs-ldl") (v "0.5.0") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "sprs") (r "^0.7.0") (d #t) (k 0)))) (h "1sz3xzajpd6sv940z390a193xfff82c23dc6qd4qlsy8aj58q86l")))

(define-public crate-sprs-ldl-0.6.0 (c (n "sprs-ldl") (v "0.6.0") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "sprs") (r "^0.8.0") (d #t) (k 0)) (d (n "sprs_suitesparse_ldl") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "00bilg2s8vmxrkaj3z482w1jpnrbk9rq3zggvyrbqi9x1r7dnn6h")))

(define-public crate-sprs-ldl-0.6.1 (c (n "sprs-ldl") (v "0.6.1") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "sprs") (r "^0.8.0") (d #t) (k 0)) (d (n "sprs_suitesparse_ldl") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "1w9rv5qndwbbg9xbag6hfnfywqa1a0y6g099i3xf3hp6j11rbxs3")))

(define-public crate-sprs-ldl-0.7.0 (c (n "sprs-ldl") (v "0.7.0") (d (list (d (n "ndarray") (r ">=0.11.0, <0.14") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)) (d (n "sprs") (r "^0.9.0") (d #t) (k 0)) (d (n "sprs_suitesparse_camd") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "sprs_suitesparse_ldl") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "1600yprahnrbsqmnwwzjp04bvgz117ir1hl5yz4g659ggmgvjky0")))

(define-public crate-sprs-ldl-0.9.0 (c (n "sprs-ldl") (v "0.9.0") (d (list (d (n "ndarray") (r ">=0.11.0, <0.15") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "sprs") (r "^0.10.0") (d #t) (k 0)) (d (n "sprs_suitesparse_camd") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "sprs_suitesparse_ldl") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "0kqzr2zz8qrspr0mypzmapc6rwivhqxyjiw395x3p72mcixmndjs")))

(define-public crate-sprs-ldl-0.10.0 (c (n "sprs-ldl") (v "0.10.0") (d (list (d (n "ndarray") (r ">=0.11.0, <0.16") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "sprs") (r "^0.11.0") (d #t) (k 0)) (d (n "sprs_suitesparse_camd") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "sprs_suitesparse_ldl") (r "^0.8.0") (o #t) (d #t) (k 0)))) (h "10lyqrsx9xk4c3snnwk04bwfhj2li8n3knli7fcf1ka76xxjs5dw")))

