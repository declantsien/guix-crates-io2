(define-module (crates-io sp rs sprs-superlu) #:use-module (crates-io))

(define-public crate-sprs-superlu-0.1.0 (c (n "sprs-superlu") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "sprs") (r "^0.11") (d #t) (k 0)) (d (n "superlu-sys") (r "^0.3.3") (d #t) (k 0)))) (h "10apscghmjpzgaig5dclj2g07rgkrm17sjfhlhs8cqk3zc4wvfqa")))

(define-public crate-sprs-superlu-0.1.1 (c (n "sprs-superlu") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "sprs") (r "^0.11") (d #t) (k 0)) (d (n "superlu-sys") (r "^0.3.3") (d #t) (k 0)))) (h "106d6iiirfv4932vazhvjfkiaa66slw5kj7kh1hj2qg8r62kbxn2")))

(define-public crate-sprs-superlu-0.1.2 (c (n "sprs-superlu") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "sprs") (r "^0.11") (d #t) (k 0)) (d (n "superlu-sys") (r "^0.3.5") (d #t) (k 0)))) (h "1i3h7myab6rd40175jk4kmjr435k98i3awdbncxqmzcy2ii0rdya")))

(define-public crate-sprs-superlu-0.1.3 (c (n "sprs-superlu") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "sprs") (r "^0.11") (d #t) (k 0)) (d (n "superlu-sys") (r "^0.3.5") (d #t) (k 0)))) (h "0l2xpqq55d1j2cfkrdw74xsrfbi3i485npsaqzvnij639ddin8z1")))

(define-public crate-sprs-superlu-0.1.4 (c (n "sprs-superlu") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "sprs") (r "^0.11") (d #t) (k 0)) (d (n "superlu-sys") (r "^0.3.5") (d #t) (k 0)))) (h "109jw9r7kmi9z0b7vnw5nwx02pzy67vxr3lrf685pgila7lvcj7r")))

(define-public crate-sprs-superlu-0.1.5 (c (n "sprs-superlu") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "sprs") (r "^0.11") (d #t) (k 0)) (d (n "superlu-sys") (r "^0.3.5") (d #t) (k 0)))) (h "0cv88mhd0i4r3mqngidy20zm9i8r13w020syl2xdann0zrgzva7a")))

