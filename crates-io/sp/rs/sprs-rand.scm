(define-module (crates-io sp rs sprs-rand) #:use-module (crates-io))

(define-public crate-sprs-rand-0.1.0 (c (n "sprs-rand") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2.1") (d #t) (k 0)) (d (n "sprs") (r "^0.8.0") (d #t) (k 0)))) (h "0a8f1kjb92jrm8rd29rllknkjg9rwjhq3nf8prx5cblsz6s5j4iy")))

(define-public crate-sprs-rand-0.2.0 (c (n "sprs-rand") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2.1") (d #t) (k 0)) (d (n "sprs") (r "^0.9.0") (d #t) (k 0)))) (h "0izq0ba8l4qyrvz9rx7drjxxpldr4n6px4x9hrz91fkp2f234xy0")))

(define-public crate-sprs-rand-0.3.0 (c (n "sprs-rand") (v "0.3.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2.1") (d #t) (k 0)) (d (n "sprs") (r "^0.10.0") (d #t) (k 0)))) (h "1ldwig1w8ixbwgvlz98k34yhv25r3m7vk6jx2r6d2jrz724vn4q2")))

(define-public crate-sprs-rand-0.4.0 (c (n "sprs-rand") (v "0.4.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.2.1") (d #t) (k 0)) (d (n "sprs") (r "^0.11.0") (d #t) (k 0)))) (h "03yvrqn7sgh9w29g4m109dc2q6d8z00mwnjr1z9dhs047yzl1m89")))

