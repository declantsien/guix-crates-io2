(define-module (crates-io sp rs sprs_suitesparse_camd) #:use-module (crates-io))

(define-public crate-sprs_suitesparse_camd-0.1.0 (c (n "sprs_suitesparse_camd") (v "0.1.0") (d (list (d (n "sprs") (r "^0.9.0") (d #t) (k 0)) (d (n "suitesparse_camd_sys") (r "^0.1.0") (d #t) (k 0)))) (h "0clcw437667l0cjksaffxvq60831538xi571qvnhp1cp4x1jyicy")))

(define-public crate-sprs_suitesparse_camd-0.1.1 (c (n "sprs_suitesparse_camd") (v "0.1.1") (d (list (d (n "sprs") (r "^0.9.0") (d #t) (k 0)) (d (n "suitesparse_camd_sys") (r "^0.1.1") (d #t) (k 0)))) (h "0nyrn4cara4n7kjzd3j55b60a2fc6ckwbmmkk38i8fxhgjlifh6w")))

(define-public crate-sprs_suitesparse_camd-0.2.0 (c (n "sprs_suitesparse_camd") (v "0.2.0") (d (list (d (n "sprs") (r "^0.10.0") (d #t) (k 0)) (d (n "suitesparse_camd_sys") (r "^0.1.1") (d #t) (k 0)))) (h "0p59f94fhds8773dwq4ixcvix4znrx2fhaca5aj96l62wa0dg6ab")))

(define-public crate-sprs_suitesparse_camd-0.3.0 (c (n "sprs_suitesparse_camd") (v "0.3.0") (d (list (d (n "sprs") (r "^0.11.0") (d #t) (k 0)) (d (n "suitesparse_camd_sys") (r "^0.1.1") (d #t) (k 0)))) (h "1kcqkmzw3xsrpjiamq6fcqgjcq8jr41ilpjgin2y458196bfva93")))

