(define-module (crates-io sp ro sprocket_engine) #:use-module (crates-io))

(define-public crate-sprocket_engine-0.2.1 (c (n "sprocket_engine") (v "0.2.1") (d (list (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "glfw") (r "^0.40.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0hgb0wzqryqia9hwhcn6zaaqc9j3fi78dgvjwsfwxb9nxrd5ac4w")))

