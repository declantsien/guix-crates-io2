(define-module (crates-io sp ro sprongen) #:use-module (crates-io))

(define-public crate-sprongen-0.0.1 (c (n "sprongen") (v "0.0.1") (d (list (d (n "png") (r "^0.15.3") (d #t) (k 0)) (d (n "ron") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (k 0)))) (h "1q9dy4a6qqqi4ssz5bnc1kgx8vzlq1c0hx46j09nhf00dhyq6c2q")))

(define-public crate-sprongen-0.0.2 (c (n "sprongen") (v "0.0.2") (d (list (d (n "png") (r "^0.15.3") (d #t) (k 0)) (d (n "ron") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (k 0)))) (h "0zvd1z5q4lyp3q08g3b72s4qij6651gm0qgcnwrzs53ba0cq6bxg")))

