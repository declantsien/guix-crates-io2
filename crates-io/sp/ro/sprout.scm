(define-module (crates-io sp ro sprout) #:use-module (crates-io))

(define-public crate-sprout-0.1.0 (c (n "sprout") (v "0.1.0") (d (list (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "trees") (r "^0.4.2") (d #t) (k 0)))) (h "0hxmvjfijk3wx07a4aiq3wypjw90ncjr0j267gyalljm9mm2if8z") (y #t)))

(define-public crate-sprout-0.1.1 (c (n "sprout") (v "0.1.1") (d (list (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "trees") (r "^0.4.2") (d #t) (k 0)))) (h "1i3xgvlyh301v021cl015xz5g9q79fjmp6snasxs5zldzqsxg69v")))

(define-public crate-sprout-0.2.0 (c (n "sprout") (v "0.2.0") (d (list (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "trees") (r "^0.4.2") (d #t) (k 0)))) (h "1rxjd3m2ha1xjlvzxma5srnjh1pccjjhknsqlbxi6dfymrjaz97d")))

(define-public crate-sprout-0.2.0+1 (c (n "sprout") (v "0.2.0+1") (d (list (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "trees") (r "^0.4.2") (d #t) (k 0)))) (h "0cas73zn55396jrmrkm3k6v5nylf8p01kk0x47psli9rmlh8fs2x")))

(define-public crate-sprout-0.2.1 (c (n "sprout") (v "0.2.1") (d (list (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "trees") (r "^0.4.2") (d #t) (k 0)))) (h "13ghwp3yav7mlz6hnc7bcgvpi6bga6aag1rrq20p52bl79x81a9h")))

(define-public crate-sprout-1.0.0 (c (n "sprout") (v "1.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "trees") (r "^0.4.2") (d #t) (k 0)))) (h "0y6ziz2pnpljwhk312rd76xwc0nvivl9gq74pdzjlyb34f9zrc5w") (r "1.65")))

