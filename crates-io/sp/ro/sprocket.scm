(define-module (crates-io sp ro sprocket) #:use-module (crates-io))

(define-public crate-sprocket-0.0.0 (c (n "sprocket") (v "0.0.0") (h "0rzdb2msd7xlk3gxlycm71090p58bjzir3l64d1rfgzyabrdzjza") (y #t)))

(define-public crate-sprocket-0.1.0 (c (n "sprocket") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive" "string"))) (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "git-testament") (r "^0.2.5") (d #t) (k 0)) (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "nonempty") (r "^0.9.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (f (quote ("pretty-print"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)) (d (n "wdl") (r "^0.2.0") (f (quote ("ast" "core" "grammar"))) (d #t) (k 0)))) (h "0hcqcmz62mc7nkp5pg697fl6kvskqqv592k2i2pwg2nkicg8nhji")))

