(define-module (crates-io sp ro sproutml) #:use-module (crates-io))

(define-public crate-sproutml-0.1.0 (c (n "sproutml") (v "0.1.0") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "0d80z0rbwsfbfd7a3vhss0v8i7qn37p4b6g4wl6pj1iy5d6kf8ss")))

(define-public crate-sproutml-0.1.1 (c (n "sproutml") (v "0.1.1") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)))) (h "1mzrikj9j137c6i5i0mvhx60wg3aw645b8ky4hvv6c4lqh00jybp")))

