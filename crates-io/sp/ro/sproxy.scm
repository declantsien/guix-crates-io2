(define-module (crates-io sp ro sproxy) #:use-module (crates-io))

(define-public crate-sproxy-0.1.0 (c (n "sproxy") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "http") (r "^0.2.6") (d #t) (k 0)) (d (n "httparse") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1zzjkax17n0pvwd6gxwiql6n46gg9g6rapksamra0v110bmbfr4l") (f (quote (("sync") ("default") ("async" "tokio"))))))

(define-public crate-sproxy-0.2.0 (c (n "sproxy") (v "0.2.0") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "httparse") (r "^1.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "017zqhilbkzylzli5xwyw38qx1d9zqzz83m7jrxcfxaxv1936m3z") (f (quote (("sync") ("default") ("async" "tokio"))))))

