(define-module (crates-io sp v- spv-rs) #:use-module (crates-io))

(define-public crate-spv-rs-0.3.9 (c (n "spv-rs") (v "0.3.9") (d (list (d (n "glam") (r "^0.20.2") (d #t) (k 0)))) (h "1rq88lg7mjxka4ial6nd596h7x09xhfbvlgdiifiksvvgl5d0lk3")))

(define-public crate-spv-rs-0.3.10 (c (n "spv-rs") (v "0.3.10") (d (list (d (n "glam") (r "^0.20.2") (d #t) (k 0)))) (h "02551c9ai4x7lq4839c8iprhaq0r7c3wzhaix5nmzh54kn20xabw")))

(define-public crate-spv-rs-0.4.0 (c (n "spv-rs") (v "0.4.0") (d (list (d (n "glam") (r "^0.20.2") (d #t) (k 0)))) (h "1wp7prn1sn5ixiavsj61shrcwmhgr5w16yp88j4sg7xqnjjvl4vl")))

(define-public crate-spv-rs-0.5.0 (c (n "spv-rs") (v "0.5.0") (d (list (d (n "glam") (r "^0.20.2") (d #t) (k 0)))) (h "0hvdb86m0gdabym2is6c3jd1i0414hb4fw5ndv79ai24y1k5mk24")))

(define-public crate-spv-rs-0.6.0 (c (n "spv-rs") (v "0.6.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "glam") (r "^0.20.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "06zk80d5fk86ncbwa1flr7mnapdcbsgc4vk1h1hggyzfrk4infjn")))

(define-public crate-spv-rs-0.6.1 (c (n "spv-rs") (v "0.6.1") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "glam") (r "^0.20.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lgdn5mcyvcd4j2ykdhrd03skfbskvicakji8jj5xm40k50k252n")))

(define-public crate-spv-rs-0.6.2 (c (n "spv-rs") (v "0.6.2") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "glam") (r "^0.20.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j8r6mfk0b19xys7mj3wk8d050f8yy6r1k7l4bz622kwbiwvi3k3")))

(define-public crate-spv-rs-0.7.0 (c (n "spv-rs") (v "0.7.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "glam") (r "^0.20.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vnkb6mb60nnf65xl35aj45wjp4aiqb2mkn151p7kl298qs3c7l5")))

