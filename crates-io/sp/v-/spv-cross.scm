(define-module (crates-io sp v- spv-cross) #:use-module (crates-io))

(define-public crate-spv-cross-0.23.1 (c (n "spv-cross") (v "0.23.1") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 1)) (d (n "js-sys") (r "^0.3.10") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "10cihk9gj5img0fdcay4392gvwa4hvc8kkg5hf7a3hkry7hsixrp") (f (quote (("msl") ("hlsl") ("glsl") ("default"))))))

(define-public crate-spv-cross-0.23.2 (c (n "spv-cross") (v "0.23.2") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 1)) (d (n "js-sys") (r "^0.3.10") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "1inh117fbsfbzjscv6pgiz483ba5a66qzxzqr8wa1b6b6zyclqg8") (f (quote (("msl") ("hlsl") ("glsl") ("default"))))))

(define-public crate-spv-cross-0.23.3 (c (n "spv-cross") (v "0.23.3") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 1)) (d (n "js-sys") (r "^0.3.10") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "1aa7cfb9pn88yggda5f0w3qpp7nvxs52aif0nkjmlpz09da118jf") (f (quote (("msl") ("hlsl") ("glsl") ("default"))))))

(define-public crate-spv-cross-0.23.4 (c (n "spv-cross") (v "0.23.4") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 1)) (d (n "js-sys") (r "^0.3.10") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "0r7fcy4x55xpmd7bf2awiydd0x998iqv1wq2gyb83lsiznrkrnh9") (f (quote (("msl") ("hlsl") ("glsl") ("default"))))))

(define-public crate-spv-cross-0.23.5 (c (n "spv-cross") (v "0.23.5") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 1)) (d (n "js-sys") (r "^0.3.10") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "0i9lxpsd39gc9ggi1jrjgrwaggmxwyg9y04ssh0l0v1gkz9ha7hx") (f (quote (("msl") ("hlsl") ("glsl") ("default"))))))

