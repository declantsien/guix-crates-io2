(define-module (crates-io sp aw spawnflake-cli) #:use-module (crates-io))

(define-public crate-spawnflake-cli-0.2.5 (c (n "spawnflake-cli") (v "0.2.5") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "spawnflake") (r "^0.2.5") (d #t) (k 0)))) (h "02hk31vxjr1x86w2y3vlyvkmz2fdjc8sqn3wr4kdx06vk1fwg657")))

(define-public crate-spawnflake-cli-0.2.6 (c (n "spawnflake-cli") (v "0.2.6") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "spawnflake") (r "^0.2.6") (d #t) (k 0)))) (h "1rqbkxn8lzxfi8cc0bda3s6lykgrw0k0q2gp6k7z61nci97vlql9")))

(define-public crate-spawnflake-cli-0.2.7 (c (n "spawnflake-cli") (v "0.2.7") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "spawnflake") (r "^0.2.7") (d #t) (k 0)))) (h "034lnrh558rvw3mlb4yhmiykk1bfri08p3l0qrhr6xipsg34pv78")))

(define-public crate-spawnflake-cli-0.2.8 (c (n "spawnflake-cli") (v "0.2.8") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "spawnflake") (r "^0.2.8") (d #t) (k 0)))) (h "1kjjcpa1qvain0rv2nki3px6dxi0qj7p57q8dkvqpxw8zr302h1b")))

