(define-module (crates-io sp aw spawns-executor) #:use-module (crates-io))

(define-public crate-spawns-executor-0.1.0 (c (n "spawns-executor") (v "0.1.0") (d (list (d (n "async-executor") (r "^1.11.0") (d #t) (k 0)) (d (n "async-net") (r "^2.0.0") (d #t) (k 2)) (d (n "async-shutdown") (r "^0.2.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-lite") (r "^2.3.0") (d #t) (k 2)) (d (n "spawns-core") (r "^1.0.0") (d #t) (k 0)))) (h "0mwjmhvqdqpxy6ycac41k6xgb9i8k6171fpqi6l2m89rh46r44lp") (f (quote (("default"))))))

(define-public crate-spawns-executor-0.1.1 (c (n "spawns-executor") (v "0.1.1") (d (list (d (n "async-executor") (r "^1.11.0") (d #t) (k 0)) (d (n "async-net") (r "^2.0.0") (d #t) (k 2)) (d (n "async-shutdown") (r "^0.2.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "futures-lite") (r "^2.3.0") (d #t) (k 2)) (d (n "spawns-core") (r "^1.0.1") (d #t) (k 0)))) (h "07gyfzvyyaf1hs63n7l8l3h8gm7fylfm7slvxnkwygn4ghc8809h") (f (quote (("default"))))))

