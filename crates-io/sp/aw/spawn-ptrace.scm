(define-module (crates-io sp aw spawn-ptrace) #:use-module (crates-io))

(define-public crate-spawn-ptrace-0.1.0 (c (n "spawn-ptrace") (v "0.1.0") (d (list (d (n "nix") (r "^0.7.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.5") (d #t) (k 1)) (d (n "skeptic") (r "^0.5") (d #t) (k 2)))) (h "038086gr7six91ymibmg5l07hw1hsgbjmdf0q5mymclhy0mrblsg")))

(define-public crate-spawn-ptrace-0.1.1 (c (n "spawn-ptrace") (v "0.1.1") (d (list (d (n "nix") (r "^0.7.0") (d #t) (k 0)))) (h "152nyfav426h2h0qdbvqaid6561n772cwahb61rxm4s2kq2wvq9n")))

(define-public crate-spawn-ptrace-0.1.2 (c (n "spawn-ptrace") (v "0.1.2") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "nix") (r "^0.18") (d #t) (k 0)))) (h "0mppyidjacyr13rm3z8hygh49hv3p24wljwmc4g0x90sr01jamrb")))

