(define-module (crates-io sp aw spawn_groups) #:use-module (crates-io))

(define-public crate-spawn_groups-0.1.0 (c (n "spawn_groups") (v "0.1.0") (d (list (d (n "async-mutex") (r "^1.4.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "cooked-waker") (r "^5.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "0wp1ym91zp5ml34m1q8icxx8rahpi634hgkniqy35ij9l4v4vm8p")))

(define-public crate-spawn_groups-0.1.1 (c (n "spawn_groups") (v "0.1.1") (d (list (d (n "async-mutex") (r "^1.4.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "cooked-waker") (r "^5.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "0i87hiax37fz382w8lp6irwcnwhsnc8437360bzk3m0fx0y8w8ag")))

(define-public crate-spawn_groups-1.0.0 (c (n "spawn_groups") (v "1.0.0") (d (list (d (n "async-mutex") (r "^1.4.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "cooked-waker") (r "^5.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.13.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1bhi5226sal37f1vax2ypvvhx9jq7hildjrjhz721d1ss24f277g")))

