(define-module (crates-io sp aw spawn-editor) #:use-module (crates-io))

(define-public crate-spawn-editor-0.0.1 (c (n "spawn-editor") (v "0.0.1") (d (list (d (n "default-editor") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1w2bw1sdz0bdygmbrizqwd496x8x3mp823yzp6b61832xkn0x5wh")))

(define-public crate-spawn-editor-0.0.2 (c (n "spawn-editor") (v "0.0.2") (d (list (d (n "default-editor") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "19fz0rpnx94gilm564x7hs0c80v8n5ixilvyj75zhzs7xwijxvnj")))

(define-public crate-spawn-editor-0.0.3 (c (n "spawn-editor") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.1") (d #t) (k 0)) (d (n "default-editor") (r "^0.1.0") (d #t) (k 0)))) (h "0kzlbaxf0ihqr558hkygxrhrfbpsgj4c9xfmg2xskad68n56x7ar")))

(define-public crate-spawn-editor-0.0.4 (c (n "spawn-editor") (v "0.0.4") (d (list (d (n "default-editor") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.1") (d #t) (k 0)))) (h "0knfva87xs2da573b5vsc7wna7h7vn6izs94p64whwqcgj6nx6pr")))

(define-public crate-spawn-editor-0.0.5 (c (n "spawn-editor") (v "0.0.5") (d (list (d (n "default-editor") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.1") (d #t) (k 0)))) (h "0wv3dvlccmsf8a3mb694m8g652gbf85h0xrwng8xlwvfinn47q0q")))

(define-public crate-spawn-editor-0.0.6 (c (n "spawn-editor") (v "0.0.6") (h "0y09p42rgay7p23qhy75i65hb0d4977r5si7irdnvycd197vz2za")))

