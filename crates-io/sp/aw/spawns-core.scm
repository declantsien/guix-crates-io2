(define-module (crates-io sp aw spawns-core) #:use-module (crates-io))

(define-public crate-spawns-core-1.0.0 (c (n "spawns-core") (v "1.0.0") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 2)) (d (n "linkme") (r "^0.3.25") (o #t) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1p94i4xvkpigglv34ynxlp5vqqyis5r7zg81khhdv9m6v086912b") (f (quote (("test-compat-global2" "compat" "test-compat-global1") ("test-compat-global1" "compat") ("default") ("compat" "linkme"))))))

(define-public crate-spawns-core-1.0.1 (c (n "spawns-core") (v "1.0.1") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 2)) (d (n "linkme") (r "^0.3.25") (o #t) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1wl7cmd6dbykiz8w6g4fvqrrh52hjz8d5zngigadvk1nira8jrnk") (f (quote (("test-compat-global2" "compat" "test-compat-global1") ("test-compat-global1" "compat") ("default") ("compat" "linkme"))))))

(define-public crate-spawns-core-1.0.2 (c (n "spawns-core") (v "1.0.2") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 2)) (d (n "linkme") (r "^0.3.25") (o #t) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0nwhhwkxzz724c1l2p21ffpj2krlx1rmbnkml7w1vm27xqdm0fgv") (f (quote (("test-compat-global2" "compat" "test-compat-global1") ("test-compat-global1" "compat") ("default") ("compat" "linkme"))))))

(define-public crate-spawns-core-1.1.0 (c (n "spawns-core") (v "1.1.0") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 2)) (d (n "linkme") (r "^0.3.25") (o #t) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0rgys4bh49ap1shg05h7v7finqr2wwfpx35chska45kzad5p1v5x") (f (quote (("test-named-global") ("test-compat-global2" "compat" "test-compat-global1") ("test-compat-global1" "compat") ("panic-multiple-global-spawners") ("default") ("compat" "linkme"))))))

(define-public crate-spawns-core-1.1.1 (c (n "spawns-core") (v "1.1.1") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 2)) (d (n "linkme") (r "^0.3.25") (o #t) (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0dk27h4l0m9q10drmwykp41qxh65h0f0cxnvqikwa9d8cypg7m1z") (f (quote (("test-named-global") ("test-compat-global2" "compat" "test-compat-global1") ("test-compat-global1" "compat") ("panic-multiple-global-spawners") ("default") ("compat" "linkme"))))))

