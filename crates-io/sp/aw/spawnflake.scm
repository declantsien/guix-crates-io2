(define-module (crates-io sp aw spawnflake) #:use-module (crates-io))

(define-public crate-spawnflake-0.1.0 (c (n "spawnflake") (v "0.1.0") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "18wzw2rvmp3pc94sicmq0w44nqrvv431ybydxwvxv713ksr86lqv") (y #t)))

(define-public crate-spawnflake-0.1.1 (c (n "spawnflake") (v "0.1.1") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "1dafjq5g9ja9ip1kksvz986v06c06c991i4dv1vbdz5hywdcvvs1") (y #t)))

(define-public crate-spawnflake-0.1.2 (c (n "spawnflake") (v "0.1.2") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "1hdh83wjqcff17svwh6kas79i1v7j1dadf9ad0mmb7iwyhk7z63j") (y #t)))

(define-public crate-spawnflake-0.1.3 (c (n "spawnflake") (v "0.1.3") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "0g3mli6x8kzl4prx3mj0s81n3swc0yp9mb1jpw3n7c8blid8iy8r") (y #t)))

(define-public crate-spawnflake-0.1.4 (c (n "spawnflake") (v "0.1.4") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "1hw0d5923lyidwgz6ib2fdgr359cshwq6fxgdmm24a45y6iab773") (y #t)))

(define-public crate-spawnflake-0.1.5 (c (n "spawnflake") (v "0.1.5") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "1l88l2414j7k198na9zin46bxjvw3mh2isbfd19xhd4zr7pl5ygp") (y #t)))

(define-public crate-spawnflake-0.1.6 (c (n "spawnflake") (v "0.1.6") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "1yhwlg6li77rvrc2kszk7jdg4ldvavs3i5mh9329migz9m7l41xw")))

(define-public crate-spawnflake-0.1.7 (c (n "spawnflake") (v "0.1.7") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "1ndmld4r1qk1sscsmj5l6p4hgm6an937qjk864xjn1vvx0mgmql8")))

(define-public crate-spawnflake-0.1.8 (c (n "spawnflake") (v "0.1.8") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "17b5d7ydcwc49fcf1sl3a05g2pcwq6abkvhmk94wagwy9xd2h7ar")))

(define-public crate-spawnflake-0.1.9 (c (n "spawnflake") (v "0.1.9") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "07c6sca81n5khp5ar5pdq8haq6r4qq4cmbqwys0yrzqsknc6lzfr")))

(define-public crate-spawnflake-0.2.0 (c (n "spawnflake") (v "0.2.0") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "postgres") (r "^0.19.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "0pqlqif0qz3awq0yfa52gmagad01pnzlayxz5ssyn3ma9lg0mhdk")))

(define-public crate-spawnflake-0.2.1 (c (n "spawnflake") (v "0.2.1") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "postgres") (r "^0.19.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "15cdr4xg953l2jcsnd899h71r8i7j9vzqpb3cz0v7v01fmp34w0j")))

(define-public crate-spawnflake-0.2.2 (c (n "spawnflake") (v "0.2.2") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "postgres") (r "^0.19.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "10b5v2vzc4icgvfl0kqazqgy5aqd3cy9lbviahc3hh3c1aar1381")))

(define-public crate-spawnflake-0.2.3 (c (n "spawnflake") (v "0.2.3") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "0x0vy10hm21z7zk7va0878vj9x9w1w0cghbfkfynvd6xmf2q3ygp")))

(define-public crate-spawnflake-0.2.5 (c (n "spawnflake") (v "0.2.5") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "0bm63pvqsl25mgvrx326gm7q294z7s2c7fkx7ppx6v7637nzsyki")))

(define-public crate-spawnflake-0.2.6 (c (n "spawnflake") (v "0.2.6") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "09s0prishc9zivbq5g3ql4yzbzal48dcfabjjk5xx8m4kiwhrfw0")))

(define-public crate-spawnflake-0.2.7 (c (n "spawnflake") (v "0.2.7") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "1r4l8cf7xha7qiyl934k5krl8p0fvhq048ya7f183hfd1hh4rnnh")))

(define-public crate-spawnflake-0.2.8 (c (n "spawnflake") (v "0.2.8") (d (list (d (n "mysql") (r "^20.1.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "045257lq4r023z49q6vm9f5vhq9r5s8zbxiig5qidjk48v918vhf")))

