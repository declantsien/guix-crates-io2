(define-module (crates-io sp aw spawner) #:use-module (crates-io))

(define-public crate-spawner-0.1.0 (c (n "spawner") (v "0.1.0") (h "1657gn6vqsfrqmr38zsd2gqncd8yw3fkkiixg0yy8rf99nqnar4x") (f (quote (("nightly"))))))

(define-public crate-spawner-0.2.0 (c (n "spawner") (v "0.2.0") (h "0yvni03rvf6a7jzfw0j307kd5v4lyc1dqhpl7bpcjv1sijp5jz5v") (f (quote (("nightly"))))))

(define-public crate-spawner-0.3.0 (c (n "spawner") (v "0.3.0") (h "11ccr4mqlwjxcwq34bdj62cbafs5w5pzyi3if9qbimnn7w2jqad1") (f (quote (("nightly")))) (y #t)))

(define-public crate-spawner-0.3.1 (c (n "spawner") (v "0.3.1") (h "1lf420b9370rnsm2xx0kn97rz5p0p4cxgk0knrg6qis3dncg9l55") (f (quote (("nightly"))))))

(define-public crate-spawner-0.3.2 (c (n "spawner") (v "0.3.2") (h "0j3gv3rcy8iw22aq8m3d46f7z7ihgp70y7pbsrq1z38nal471spg") (f (quote (("nightly"))))))

