(define-module (crates-io sp aw spawn-wait) #:use-module (crates-io))

(define-public crate-spawn-wait-0.1.0 (c (n "spawn-wait") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.117") (k 0)) (d (n "signal-hook") (r "^0.3") (f (quote ("iterator"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10rffll2p4bk6y663apwhi20v9izykplazldlr91sjzmiv1nkx7w") (f (quote (("default" "blocking_wait_any") ("blocking_wait_any" "signal-hook"))))))

(define-public crate-spawn-wait-0.2.0 (c (n "spawn-wait") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.117") (k 0)) (d (n "signal-hook") (r "^0.3") (f (quote ("iterator"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "161p5430q8xl98div9pwaxbqpq7k75i9ifqibjici9bqbglvhvr6") (f (quote (("default" "blocking_wait_any") ("blocking_wait_any" "signal-hook"))))))

(define-public crate-spawn-wait-0.2.1 (c (n "spawn-wait") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.117") (k 0)) (d (n "signal-hook") (r "^0.3") (f (quote ("iterator"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02f5vszxg6mwxm1l7gmxrqy3incwsbllw6ngk2nkx4hj2iqja1xq") (f (quote (("default" "blocking_wait_any") ("blocking_wait_any" "signal-hook"))))))

(define-public crate-spawn-wait-0.2.2 (c (n "spawn-wait") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.117") (k 0)) (d (n "signal-hook") (r "^0.3") (f (quote ("iterator"))) (o #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1f8313vi8svgngyb895mmx5zmsn1pdf9c1bkk3i6ag777jnd26mc") (f (quote (("default" "blocking_wait_any") ("blocking_wait_any" "signal-hook"))))))

