(define-module (crates-io sp aw spawn-task-port) #:use-module (crates-io))

(define-public crate-spawn-task-port-0.1.0 (c (n "spawn-task-port") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "mach") (r "^0.1") (d #t) (k 0)) (d (n "skeptic") (r "^0.5") (d #t) (k 1)) (d (n "skeptic") (r "^0.5") (d #t) (k 2)) (d (n "uuid") (r "^0.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "0d89ivddmhvg0mqns7b30yajxjf6y1bbw43i6d510xx3bzcnxzfg")))

(define-public crate-spawn-task-port-0.1.1 (c (n "spawn-task-port") (v "0.1.1") (d (list (d (n "docmatic") (r "^0.1.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "mach") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "12w9cg755lg6rv4fvlqd86q0sifsx80wxzcdcw82k6p5xf1sijnz")))

