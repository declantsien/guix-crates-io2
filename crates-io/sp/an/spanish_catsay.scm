(define-module (crates-io sp an spanish_catsay) #:use-module (crates-io))

(define-public crate-spanish_catsay-0.1.0 (c (n "spanish_catsay") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.10") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "predicates") (r "^3.0.2") (d #t) (k 0)))) (h "1h4ifr5fl7ff8mzbdbl846d0ppyfh27648632nw6bg4mf4vhfk3i") (y #t)))

(define-public crate-spanish_catsay-0.1.1 (c (n "spanish_catsay") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.10") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "predicates") (r "^3.0.2") (d #t) (k 0)))) (h "1s6d35sz06h5r627hm61mdwc988mglqpwjf2j463a46fgdw33xn5")))

