(define-module (crates-io sp an spanr) #:use-module (crates-io))

(define-public crate-spanr-0.1.0 (c (n "spanr") (v "0.1.0") (d (list (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (f (quote ("span-locations"))) (d #t) (k 0)))) (h "09yvinfa4s56r0b7bkvqi305nm5l7gj1y6ws9iyyvyjq6ac8c2l2")))

