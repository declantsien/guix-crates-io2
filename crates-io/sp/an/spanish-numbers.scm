(define-module (crates-io sp an spanish-numbers) #:use-module (crates-io))

(define-public crate-spanish-numbers-0.1.0 (c (n "spanish-numbers") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0l12kzxvpdxmic2ln7c9fs1jb7qf327wwk6liq1ly234nj5skkhc")))

(define-public crate-spanish-numbers-0.1.1 (c (n "spanish-numbers") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0qb7ykaxhdsir6dna11sykcvh3qp6n624cdmg1bfq3pmrssm1iff")))

(define-public crate-spanish-numbers-0.1.2 (c (n "spanish-numbers") (v "0.1.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "10awmj5svkz1jay6nm5jh8swf99xlpwjphmg8hi1kcx7amh1fz38")))

(define-public crate-spanish-numbers-0.1.3 (c (n "spanish-numbers") (v "0.1.3") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1a3ly48kwkvlm3vmp1xci2b90wa8qpx59whr3h4q0s70sri19aig")))

(define-public crate-spanish-numbers-0.1.4 (c (n "spanish-numbers") (v "0.1.4") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1frg4g6h2qm3za1rai8rlw9893kg8ixp1f4ji32z4w6faqc94xx9")))

