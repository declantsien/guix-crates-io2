(define-module (crates-io sp an spaniel) #:use-module (crates-io))

(define-public crate-spaniel-0.1.0 (c (n "spaniel") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "bytes") (r "^0.4.8") (d #t) (k 0)) (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.7") (d #t) (k 0)))) (h "0sv9ym0wklb54ckfzpb0sz3i9syang8wab52inav2is7l590cgxl")))

