(define-module (crates-io sp an spanned) #:use-module (crates-io))

(define-public crate-spanned-0.1.0 (c (n "spanned") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)))) (h "09ccbqf16wv4rlig6agcagim13ys6zawplhciyxia60bcm0s61q1")))

(define-public crate-spanned-0.1.1 (c (n "spanned") (v "0.1.1") (d (list (d (n "bstr") (r "^1.7.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)))) (h "0p25c6m32hjcdradzvvig2bdlyrdwbwjpmwrmi693n6c5nlxxbp1")))

(define-public crate-spanned-0.1.2 (c (n "spanned") (v "0.1.2") (d (list (d (n "bstr") (r "^1.7.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)))) (h "0hlfv6sq8vq4qzh39mfwkvm7dbr8dr3d3cwz1wngxgdqisafr5gx")))

(define-public crate-spanned-0.1.3 (c (n "spanned") (v "0.1.3") (d (list (d (n "bstr") (r "^1.7.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)))) (h "18j32cnw19k1i7bwb5mdkmzjlg3vgzg3dsz9g14ajhsvp9jbgd0g")))

(define-public crate-spanned-0.1.4 (c (n "spanned") (v "0.1.4") (d (list (d (n "bstr") (r "^1.7.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)))) (h "0cw7iapiqj723cv25yh7nshhwcd3s7yx5ms1jrgrnz2kwjgvj4gp")))

(define-public crate-spanned-0.1.5 (c (n "spanned") (v "0.1.5") (d (list (d (n "bstr") (r "^1.7.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)))) (h "10npw6ilgcw99xwflgcxsijdl3bapgl992yd4p25cvp8k32xszx0")))

(define-public crate-spanned-0.1.6 (c (n "spanned") (v "0.1.6") (d (list (d (n "bstr") (r "^1.6.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)))) (h "0bfrn337xzz7400gp2203vj5i1vvfrqc2z0hb1qclkkky3cxwj5d")))

(define-public crate-spanned-0.2.0 (c (n "spanned") (v "0.2.0") (d (list (d (n "bstr") (r "^1.6.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)))) (h "13h9afrvfn73kld95vqjgkznz6h7i4kl83g8ybagprmpj1alzpyc")))

(define-public crate-spanned-0.2.1 (c (n "spanned") (v "0.2.1") (d (list (d (n "bstr") (r "^1.6.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)))) (h "0pb0p01g0f3kafbgwmc30nh821aablc4jb5svbain9429f5vl57d")))

