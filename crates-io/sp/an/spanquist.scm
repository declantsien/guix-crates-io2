(define-module (crates-io sp an spanquist) #:use-module (crates-io))

(define-public crate-spanquist-0.1.0 (c (n "spanquist") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0f6569fphgxd4p8g2dms6db0p3fbl975lj5j8xbdbig6sbr2dkjz")))

(define-public crate-spanquist-0.1.1 (c (n "spanquist") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0rv3yfff5mv98lms38cqgqgi1f85niy7d7ydz90my16bmjbh9fyi")))

