(define-module (crates-io sp an spanned_json_parser) #:use-module (crates-io))

(define-public crate-spanned_json_parser-0.1.0 (c (n "spanned_json_parser") (v "0.1.0") (d (list (d (n "bytecount") (r "^0.6.7") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "memchr") (r "^2.6.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (d #t) (k 0)))) (h "1kpk0i59pn3g7i54z7xyv41vyk5q0snb3s34gp0zydk5h4cz61jj")))

(define-public crate-spanned_json_parser-0.1.1 (c (n "spanned_json_parser") (v "0.1.1") (d (list (d (n "bytecount") (r "^0.6.7") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "memchr") (r "^2.6.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (d #t) (k 0)))) (h "1pchjjxqiad8961hpsss7255hgbssp8bm0qipvsqy9cmgyxsn1wc")))

(define-public crate-spanned_json_parser-0.2.0 (c (n "spanned_json_parser") (v "0.2.0") (d (list (d (n "bytecount") (r "^0.6.7") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "memchr") (r "^2.6.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0a4z6b82pbvg884l20rpyjpw7s20r8ybs085wjjzj9hzlwr77byh") (f (quote (("wasm" "wasm-bindgen"))))))

