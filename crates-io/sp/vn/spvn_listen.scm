(define-module (crates-io sp vn spvn_listen) #:use-module (crates-io))

(define-public crate-spvn_listen-0.1.0 (c (n "spvn_listen") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "tokio") (r "^1.9") (f (quote ("sync" "net" "sync"))) (d #t) (k 0)))) (h "0llfa82wk3p3z3gjd8jrrp7yig67yikfpwshd55xz3f39zb5y8kd") (y #t) (r "1.69.0")))

