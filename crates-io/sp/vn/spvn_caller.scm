(define-module (crates-io sp vn spvn_caller) #:use-module (crates-io))

(define-public crate-spvn_caller-0.1.0 (c (n "spvn_caller") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "cpython") (r "^0.7") (f (quote ("python3-sys"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.144") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simple_logger") (r "^4.1.0") (d #t) (k 0)))) (h "1lwwn84rwyxx0413ym13ixw6vv9wy0pvahi1hi2cb2nlb2fibd9y") (r "1.69.0")))

