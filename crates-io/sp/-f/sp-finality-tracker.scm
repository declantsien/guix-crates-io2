(define-module (crates-io sp -f sp-finality-tracker) #:use-module (crates-io))

(define-public crate-sp-finality-tracker-2.0.0-alpha.2 (c (n "sp-finality-tracker") (v "2.0.0-alpha.2") (d (list (d (n "codec") (r "^1.0.0") (k 0) (p "parity-scale-codec")) (d (n "sp-inherents") (r "^2.0.0-alpha.2") (k 0)) (d (n "sp-std") (r "^2.0.0-alpha.2") (k 0)))) (h "1jhi3sjw9jcfywfqr2gylksb31al73w4k5rhcn7zys1kkdc7cjka") (f (quote (("std" "codec/std" "sp-std/std" "sp-inherents/std") ("default" "std"))))))

(define-public crate-sp-finality-tracker-2.0.0-alpha.3 (c (n "sp-finality-tracker") (v "2.0.0-alpha.3") (d (list (d (n "codec") (r "^1.0.0") (k 0) (p "parity-scale-codec")) (d (n "sp-inherents") (r "^2.0.0-alpha.2") (k 0)) (d (n "sp-std") (r "^2.0.0-alpha.2") (k 0)))) (h "1cr1sggf46sx6pajypy1kgn912laicl8w6jbbg5q68mas7amydpl") (f (quote (("std" "codec/std" "sp-std/std" "sp-inherents/std") ("default" "std"))))))

(define-public crate-sp-finality-tracker-2.0.0-alpha.5 (c (n "sp-finality-tracker") (v "2.0.0-alpha.5") (d (list (d (n "codec") (r "^1.2.0") (k 0) (p "parity-scale-codec")) (d (n "sp-inherents") (r "^2.0.0-alpha.5") (k 0)) (d (n "sp-std") (r "^2.0.0-alpha.5") (k 0)))) (h "05avb6z4mr1mmhvc49hdckjzldcmjilg8gklr93zfrsngrc9xr0b") (f (quote (("std" "codec/std" "sp-std/std" "sp-inherents/std") ("default" "std"))))))

(define-public crate-sp-finality-tracker-2.0.0-alpha.6 (c (n "sp-finality-tracker") (v "2.0.0-alpha.6") (d (list (d (n "codec") (r "^1.3.0") (k 0) (p "parity-scale-codec")) (d (n "sp-inherents") (r "^2.0.0-alpha.6") (k 0)) (d (n "sp-std") (r "^2.0.0-alpha.6") (k 0)))) (h "090nr0jpw79l0ddca7fjy7hd2g2yrw0a4fdm05xi7zbm3vxiixx5") (f (quote (("std" "codec/std" "sp-std/std" "sp-inherents/std") ("default" "std"))))))

(define-public crate-sp-finality-tracker-2.0.0-alpha.7 (c (n "sp-finality-tracker") (v "2.0.0-alpha.7") (d (list (d (n "codec") (r "^1.3.0") (k 0) (p "parity-scale-codec")) (d (n "sp-inherents") (r "^2.0.0-alpha.7") (k 0)) (d (n "sp-std") (r "^2.0.0-alpha.7") (k 0)))) (h "139lgc7aq3g0b9gkl4ywfxgcffbjrzz549c73bv66yz0y4chy1cg") (f (quote (("std" "codec/std" "sp-std/std" "sp-inherents/std") ("default" "std"))))))

(define-public crate-sp-finality-tracker-2.0.0-alpha.8 (c (n "sp-finality-tracker") (v "2.0.0-alpha.8") (d (list (d (n "codec") (r "^1.3.0") (k 0) (p "parity-scale-codec")) (d (n "sp-inherents") (r "^2.0.0-alpha.8") (k 0)) (d (n "sp-std") (r "^2.0.0-alpha.8") (k 0)))) (h "1wd2d8894bq7wcn7q6cm2mwv0kylw6afy7plcsf18licf9wj6kz6") (f (quote (("std" "codec/std" "sp-std/std" "sp-inherents/std") ("default" "std"))))))

(define-public crate-sp-finality-tracker-2.0.0-rc1 (c (n "sp-finality-tracker") (v "2.0.0-rc1") (d (list (d (n "codec") (r "^1.3.0") (k 0) (p "parity-scale-codec")) (d (n "sp-inherents") (r "^2.0.0-rc1") (k 0)) (d (n "sp-std") (r "^2.0.0-rc1") (k 0)))) (h "0r9yf4wdgj5j4bkjmwcx595g1ap7mlxi0ifi92flp4m1dpmxpwz5") (f (quote (("std" "codec/std" "sp-std/std" "sp-inherents/std") ("default" "std"))))))

(define-public crate-sp-finality-tracker-2.0.0-rc2 (c (n "sp-finality-tracker") (v "2.0.0-rc2") (d (list (d (n "codec") (r "^1.3.0") (k 0) (p "parity-scale-codec")) (d (n "sp-inherents") (r "^2.0.0-rc2") (k 0)) (d (n "sp-std") (r "^2.0.0-rc2") (k 0)))) (h "03fsf1pa5c84llmk2g34wv3v8gymfzszhqjgv6ypx3nbhx8h9w9a") (f (quote (("std" "codec/std" "sp-std/std" "sp-inherents/std") ("default" "std"))))))

(define-public crate-sp-finality-tracker-2.0.0-rc3 (c (n "sp-finality-tracker") (v "2.0.0-rc3") (d (list (d (n "codec") (r "^1.3.0") (k 0) (p "parity-scale-codec")) (d (n "sp-inherents") (r "^2.0.0-rc3") (k 0)) (d (n "sp-std") (r "^2.0.0-rc3") (k 0)))) (h "1mylb0y8rsmlrdpcplivk9zix4zgyzl30s4a3iw0s2g6j90mvdg4") (f (quote (("std" "codec/std" "sp-std/std" "sp-inherents/std") ("default" "std"))))))

(define-public crate-sp-finality-tracker-2.0.0-rc4 (c (n "sp-finality-tracker") (v "2.0.0-rc4") (d (list (d (n "codec") (r "^1.3.1") (k 0) (p "parity-scale-codec")) (d (n "sp-inherents") (r "^2.0.0-rc4") (k 0)) (d (n "sp-std") (r "^2.0.0-rc4") (k 0)))) (h "0bbdh5ww1hibggilzxb050q959hwp2jpg35dqm75vr1bxdzw0n7h") (f (quote (("std" "codec/std" "sp-std/std" "sp-inherents/std") ("default" "std"))))))

(define-public crate-sp-finality-tracker-2.0.0-rc5 (c (n "sp-finality-tracker") (v "2.0.0-rc5") (d (list (d (n "codec") (r "^1.3.1") (k 0) (p "parity-scale-codec")) (d (n "sp-inherents") (r "^2.0.0-rc5") (k 0)) (d (n "sp-std") (r "^2.0.0-rc5") (k 0)))) (h "170cyivbfx2vcy5cpkzkvf3fy1yyi2aw953s1j024ilv3gawpxxl") (f (quote (("std" "codec/std" "sp-std/std" "sp-inherents/std") ("default" "std"))))))

(define-public crate-sp-finality-tracker-2.0.0-rc6 (c (n "sp-finality-tracker") (v "2.0.0-rc6") (d (list (d (n "codec") (r "^1.3.1") (k 0) (p "parity-scale-codec")) (d (n "sp-inherents") (r "^2.0.0-rc6") (k 0)) (d (n "sp-std") (r "^2.0.0-rc6") (k 0)))) (h "1y3dvbqg7ly8lq360nr410nh630s595b18bn9lf9in48gn2y8cs6") (f (quote (("std" "codec/std" "sp-std/std" "sp-inherents/std") ("default" "std"))))))

(define-public crate-sp-finality-tracker-2.0.0 (c (n "sp-finality-tracker") (v "2.0.0") (d (list (d (n "codec") (r "^1.3.1") (k 0) (p "parity-scale-codec")) (d (n "sp-inherents") (r "^2.0.0") (k 0)) (d (n "sp-std") (r "^2.0.0") (k 0)))) (h "0m41arqpjw7bj31lbnnwkwxd7myvryd1nm8h20j1d8bk693k6m2x") (f (quote (("std" "codec/std" "sp-std/std" "sp-inherents/std") ("default" "std"))))))

(define-public crate-sp-finality-tracker-2.0.1 (c (n "sp-finality-tracker") (v "2.0.1") (d (list (d (n "codec") (r "^1.3.1") (k 0) (p "parity-scale-codec")) (d (n "sp-inherents") (r "^2.0.0") (k 0)) (d (n "sp-std") (r "^2.0.0") (k 0)))) (h "0dsw8a4zyy3rbv7f9n9kkm8nnd44v907c7qvb4lws86ncc9wd22n") (f (quote (("std" "codec/std" "sp-std/std" "sp-inherents/std") ("default" "std"))))))

