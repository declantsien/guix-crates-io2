(define-module (crates-io sp ng spng-sys) #:use-module (crates-io))

(define-public crate-spng-sys-0.1.0-alpha+1 (c (n "spng-sys") (v "0.1.0-alpha+1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "0z54p3yn06m26cx3mrghn9dr10vyqzlx35qwi867kb3npgb5d711") (l "spng")))

(define-public crate-spng-sys-0.1.0-alpha+2 (c (n "spng-sys") (v "0.1.0-alpha+2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "19vxy0dy63b0x2krfxjhg5br0l1wlqpcaz0gxvsh96v01zsc1byn") (l "spng")))

(define-public crate-spng-sys-0.1.0-alpha.3 (c (n "spng-sys") (v "0.1.0-alpha.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "1caww0x2b309hr5ylsc094adgzn13xlp7mclb61m13ww98c5f8dq") (l "spng")))

(define-public crate-spng-sys-0.1.0-alpha.4 (c (n "spng-sys") (v "0.1.0-alpha.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "1pczfy4ilkynqkmn49dqs6qyqvi8yycc69sqsaiar0m2v5708b9r") (l "spng")))

(define-public crate-spng-sys-0.1.0-alpha.5 (c (n "spng-sys") (v "0.1.0-alpha.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (d #t) (k 0)))) (h "0w50izm8pizn87rhyd6bd6vbpp2jjxpswgxn4vi7g82ax8vv19bl") (l "spng")))

(define-public crate-spng-sys-0.1.0-alpha.6 (c (n "spng-sys") (v "0.1.0-alpha.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.1.2") (f (quote ("libc" "static"))) (k 0)))) (h "09rc5s7vfz22yr2q3mfrx9pjabkn8v72absf19x22yvql1xbh2j5") (f (quote (("zlib-ng" "libz-sys/zlib-ng") ("default")))) (l "spng")))

(define-public crate-spng-sys-0.1.0 (c (n "spng-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.1.2") (f (quote ("libc" "static"))) (k 0)))) (h "0mc1dg698wv45cfh1jb6a9mrsr6s2p8gzzcn7ihzwfyl4xcb9xag") (f (quote (("zlib-ng" "libz-sys/zlib-ng") ("default")))) (l "spng")))

(define-public crate-spng-sys-0.2.0-alpha.1 (c (n "spng-sys") (v "0.2.0-alpha.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.1.2") (f (quote ("libc" "static"))) (k 0)))) (h "1ga8wkbyrdnxgyk12rav80nm3mgrbyyr8blcys7n9wikdmq4rz0i") (f (quote (("zlib-ng" "libz-sys/zlib-ng") ("default")))) (l "spng")))

(define-public crate-spng-sys-0.2.0-alpha.2 (c (n "spng-sys") (v "0.2.0-alpha.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libz-sys") (r "^1.1.2") (f (quote ("libc" "static"))) (k 0)))) (h "046y7r9mlm9w00zkf5frkk0r00hd1zz4zsxl9qahvq20jwxsqn7d") (f (quote (("zlib-ng" "libz-sys/zlib-ng") ("default")))) (l "spng")))

