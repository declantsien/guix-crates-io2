(define-module (crates-io sp ng spng) #:use-module (crates-io))

(define-public crate-spng-0.1.0-alpha+1 (c (n "spng") (v "0.1.0-alpha+1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "spng-sys") (r "^0.1.0-alpha") (d #t) (k 0)))) (h "1kql6mwsvnrmgrw49ibzmcydc9a537q7iz7067l69zc269h59764")))

(define-public crate-spng-0.1.0-alpha+2 (c (n "spng") (v "0.1.0-alpha+2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "spng-sys") (r "^0.1.0-alpha") (d #t) (k 0)))) (h "1a5cpwfxfs3pds0w8cd7w009sgy9fxyzjln9cchgc8gvapj6f2vp")))

(define-public crate-spng-0.1.0-alpha.3 (c (n "spng") (v "0.1.0-alpha.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "spng-sys") (r "^0.1.0-alpha") (d #t) (k 0)))) (h "0gj6q3qx59iahdvz9ic8ix5p7kyd4k3370lavy69jk6v3wzi662m")))

(define-public crate-spng-0.1.0-alpha.4 (c (n "spng") (v "0.1.0-alpha.4") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "spng-sys") (r "^0.1.0-alpha") (d #t) (k 0)))) (h "0c4mbiqif7q78wsi1hj7zc190bmvcq77y16c0hcrdri5qkncik4w")))

(define-public crate-spng-0.1.0-alpha.5 (c (n "spng") (v "0.1.0-alpha.5") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "spng-sys") (r "^0.1.0-alpha") (d #t) (k 0)))) (h "12sgi4s0l38rnp0p74r42jzbrq3424hn8mg810v7rdsrlncrvly0")))

(define-public crate-spng-0.1.0-alpha.6 (c (n "spng") (v "0.1.0-alpha.6") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "spng-sys") (r "^0.1.0-alpha") (d #t) (k 0)))) (h "0dggrmc1w2g105wymvv6nv3wp9y39wv254ch07jvsqdgkcm98gik") (f (quote (("zlib-ng" "spng-sys/zlib-ng") ("default"))))))

(define-public crate-spng-0.1.0 (c (n "spng") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "spng-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1nvg1ddhys2yyr02x5xyr6yc14n560gawkr9j2p8vc9j8pwsa3ac") (f (quote (("zlib-ng" "spng-sys/zlib-ng") ("default"))))))

(define-public crate-spng-0.2.0-alpha.1 (c (n "spng") (v "0.2.0-alpha.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "spng-sys") (r "^0.2.0-alpha.1") (d #t) (k 0)))) (h "1axnabdgyx59k88781q4jc3c3kr78hivb88558xf8xx5l1wgazz3") (f (quote (("zlib-ng" "spng-sys/zlib-ng") ("default"))))))

(define-public crate-spng-0.2.0-alpha.2 (c (n "spng") (v "0.2.0-alpha.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "spng-sys") (r "^0.2.0-alpha.2") (d #t) (k 0)))) (h "03lvmd7jhf8d41x2fc6smf72s63yiw78rda2nrwl1xxw3r58ch4c") (f (quote (("zlib-ng" "spng-sys/zlib-ng") ("default"))))))

