(define-module (crates-io sp yx spyxpo) #:use-module (crates-io))

(define-public crate-spyxpo-0.0.1 (c (n "spyxpo") (v "0.0.1") (h "11i02s35r6swkhs1m6640kmv1yfqn2gqrrmnfzmwqjnak4agr68f")))

(define-public crate-spyxpo-0.0.2 (c (n "spyxpo") (v "0.0.2") (h "0n00l947w7dcykb6wrh1ng6k422wa21cvnibibh14np0292jgq0m")))

