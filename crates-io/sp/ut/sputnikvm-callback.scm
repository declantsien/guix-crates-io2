(define-module (crates-io sp ut sputnikvm-callback) #:use-module (crates-io))

(define-public crate-sputnikvm-callback-0.1.0 (c (n "sputnikvm-callback") (v "0.1.0") (d (list (d (n "etcommon-bigint") (r "^0.2.7") (d #t) (k 0)) (d (n "sputnikvm") (r "^0.7.7") (d #t) (k 0)))) (h "0z6majckxgns353za91n2pfs71q87wyh0fd3vqwqdvz4nlxaz9yr")))

