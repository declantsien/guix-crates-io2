(define-module (crates-io sp ut sputnikvm-network-classic) #:use-module (crates-io))

(define-public crate-sputnikvm-network-classic-0.10.0 (c (n "sputnikvm-network-classic") (v "0.10.0") (d (list (d (n "etcommon-bigint") (r "^0.2") (k 0)) (d (n "sputnikvm") (r "^0.10") (d #t) (k 0)))) (h "0py810wn88xy719wv6ffwhxf04zxpw8h0yw2kw2laqfgj1f45kpm")))

(define-public crate-sputnikvm-network-classic-0.10.1 (c (n "sputnikvm-network-classic") (v "0.10.1") (d (list (d (n "etcommon-bigint") (r "^0.2") (k 0)) (d (n "sputnikvm") (r "^0.10") (k 0)))) (h "0r8gxpnh3f9qzl6dnnfzp7gwvw12j3x1kzqpqri54i9gssm3r53v") (f (quote (("std" "sputnikvm/std") ("rust-secp256k1" "sputnikvm/rust-secp256k1") ("rlp" "etcommon-bigint/rlp") ("default" "std" "c-secp256k1") ("c-secp256k1" "sputnikvm/c-secp256k1"))))))

(define-public crate-sputnikvm-network-classic-0.11.0-beta.0 (c (n "sputnikvm-network-classic") (v "0.11.0-beta.0") (d (list (d (n "etcommon-bigint") (r "^0.2") (k 0)) (d (n "sputnikvm") (r "^0.11.0-beta") (k 0)) (d (n "sputnikvm-precompiled-bn128") (r "^0.11.0-beta") (k 0)) (d (n "sputnikvm-precompiled-modexp") (r "^0.11.0-beta") (k 0)))) (h "1vh7x3zv247wpyc7lrhy23qdmf4713ncg06sxk7lvr3bf39g9fmg") (f (quote (("std" "sputnikvm/std") ("rust-secp256k1" "sputnikvm/rust-secp256k1" "sputnikvm-precompiled-bn128/rust-secp256k1" "sputnikvm-precompiled-modexp/rust-secp256k1") ("rlp" "etcommon-bigint/rlp") ("default" "std" "c-secp256k1") ("c-secp256k1" "sputnikvm/c-secp256k1" "sputnikvm-precompiled-bn128/c-secp256k1" "sputnikvm-precompiled-modexp/c-secp256k1"))))))

