(define-module (crates-io sp ut sputnikvm-precompiled-bn128) #:use-module (crates-io))

(define-public crate-sputnikvm-precompiled-bn128-0.10.0 (c (n "sputnikvm-precompiled-bn128") (v "0.10.0") (d (list (d (n "bn-plus") (r "^0.4") (d #t) (k 0)) (d (n "etcommon-bigint") (r "^0.2") (k 0)) (d (n "sputnikvm") (r "^0.10") (d #t) (k 0)))) (h "0vssc3gln1b077hri1ywnkmdnk6yzrdildk9fsbjag4q5s77irfb")))

(define-public crate-sputnikvm-precompiled-bn128-0.10.1 (c (n "sputnikvm-precompiled-bn128") (v "0.10.1") (d (list (d (n "bn-plus") (r "^0.4") (d #t) (k 0)) (d (n "etcommon-bigint") (r "^0.2") (k 0)) (d (n "sputnikvm") (r "^0.10") (k 0)))) (h "0hq0qa70f04c9ykf2spf7kpcmzxc44956jd6fn9srfyl6dzcy26h") (f (quote (("std" "sputnikvm/std") ("rust-secp256k1" "sputnikvm/rust-secp256k1") ("rlp" "etcommon-bigint/rlp") ("default" "std" "c-secp256k1") ("c-secp256k1" "sputnikvm/c-secp256k1"))))))

(define-public crate-sputnikvm-precompiled-bn128-0.11.0-beta.0 (c (n "sputnikvm-precompiled-bn128") (v "0.11.0-beta.0") (d (list (d (n "bn-plus") (r "^0.4") (d #t) (k 0)) (d (n "etcommon-bigint") (r "^0.2") (k 0)) (d (n "sputnikvm") (r "^0.11.0-beta") (k 0)))) (h "0i8wjrvdzbm39aix87chcq4ibgipmzd0v3lz9d5cjp5zp8ci44lx") (f (quote (("std" "sputnikvm/std") ("rust-secp256k1" "sputnikvm/rust-secp256k1") ("rlp" "etcommon-bigint/rlp") ("default" "std" "c-secp256k1") ("c-secp256k1" "sputnikvm/c-secp256k1"))))))

