(define-module (crates-io sp ut sputnikvm-precompiled-modexp) #:use-module (crates-io))

(define-public crate-sputnikvm-precompiled-modexp-0.10.0 (c (n "sputnikvm-precompiled-modexp") (v "0.10.0") (d (list (d (n "etcommon-bigint") (r "^0.2") (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "num-bigint") (r "^0.1") (d #t) (k 0)) (d (n "sputnikvm") (r "^0.10") (d #t) (k 0)))) (h "17pbfgh7859522259b2yz57q9mgn4wci7v4dhf2fjygypmm57f7z")))

(define-public crate-sputnikvm-precompiled-modexp-0.10.1 (c (n "sputnikvm-precompiled-modexp") (v "0.10.1") (d (list (d (n "etcommon-bigint") (r "^0.2") (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "num-bigint") (r "^0.1") (d #t) (k 0)) (d (n "sputnikvm") (r "^0.10") (k 0)))) (h "1vvvh7m4w4lfmv3mz5f21hl3n8fn2cxfc3aypphb8fkxdyd8bihd") (f (quote (("std" "sputnikvm/std") ("rust-secp256k1" "sputnikvm/rust-secp256k1") ("rlp" "etcommon-bigint/rlp") ("default" "std" "c-secp256k1") ("c-secp256k1" "sputnikvm/c-secp256k1"))))))

(define-public crate-sputnikvm-precompiled-modexp-0.11.0-beta.0 (c (n "sputnikvm-precompiled-modexp") (v "0.11.0-beta.0") (d (list (d (n "etcommon-bigint") (r "^0.2") (k 0)) (d (n "etcommon-hexutil") (r "^0.2") (d #t) (k 2)) (d (n "num-bigint") (r "^0.1") (d #t) (k 0)) (d (n "sputnikvm") (r "^0.11.0-beta") (k 0)))) (h "14d443947imrgw1d8ziz1fj8nf9dmlz9vbcrlqbd16zma3rq3sq4") (f (quote (("std" "sputnikvm/std") ("rust-secp256k1" "sputnikvm/rust-secp256k1") ("rlp" "etcommon-bigint/rlp") ("default" "std" "c-secp256k1") ("c-secp256k1" "sputnikvm/c-secp256k1"))))))

