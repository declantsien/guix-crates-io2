(define-module (crates-io sp ut sputnikvm-network-ellaism) #:use-module (crates-io))

(define-public crate-sputnikvm-network-ellaism-0.10.0 (c (n "sputnikvm-network-ellaism") (v "0.10.0") (d (list (d (n "etcommon-bigint") (r "^0.2") (k 0)) (d (n "sputnikvm") (r "^0.10") (d #t) (k 0)))) (h "0sjqj9pz0l3v4chs0h07jqxmp60kfglxc08lzx4dn8w75vwjniy8")))

(define-public crate-sputnikvm-network-ellaism-0.10.1 (c (n "sputnikvm-network-ellaism") (v "0.10.1") (d (list (d (n "etcommon-bigint") (r "^0.2") (k 0)) (d (n "sputnikvm") (r "^0.10") (k 0)))) (h "0iz2j7j3bd14x84y0rlm6sl4vq54np75xys50n22n5g94gn44bmz") (f (quote (("std" "sputnikvm/std") ("rust-secp256k1" "sputnikvm/rust-secp256k1") ("rlp" "etcommon-bigint/rlp") ("default" "std" "c-secp256k1") ("c-secp256k1" "sputnikvm/c-secp256k1"))))))

(define-public crate-sputnikvm-network-ellaism-0.11.0-beta.0 (c (n "sputnikvm-network-ellaism") (v "0.11.0-beta.0") (d (list (d (n "etcommon-bigint") (r "^0.2") (k 0)) (d (n "sputnikvm") (r "^0.11.0-beta") (k 0)))) (h "0b2v4p2mavfqi5zd4l92cxwh4skpfd5qhdcrx9xc064fayd7aisa") (f (quote (("std" "sputnikvm/std") ("rust-secp256k1" "sputnikvm/rust-secp256k1") ("rlp" "etcommon-bigint/rlp") ("default" "std" "c-secp256k1") ("c-secp256k1" "sputnikvm/c-secp256k1"))))))

