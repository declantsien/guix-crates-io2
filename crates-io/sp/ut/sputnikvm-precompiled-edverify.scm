(define-module (crates-io sp ut sputnikvm-precompiled-edverify) #:use-module (crates-io))

(define-public crate-sputnikvm-precompiled-edverify-0.1.0 (c (n "sputnikvm-precompiled-edverify") (v "0.1.0") (d (list (d (n "bn-plus") (r "^0.4") (d #t) (k 0)) (d (n "ed25519") (r "^0.0.0") (d #t) (k 0)) (d (n "etcommon-bigint") (r "^0.2") (k 0)) (d (n "etcommon-hexutil") (r "^0.2.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "sputnikvm") (r "^0.10") (k 0)))) (h "0fh32hf1001rngbpc0gfn94g1jfxn757d62kms34acgkambd2np0") (f (quote (("std" "sputnikvm/std") ("rust-secp256k1" "sputnikvm/rust-secp256k1") ("rlp" "etcommon-bigint/rlp") ("default" "std" "c-secp256k1") ("c-secp256k1" "sputnikvm/c-secp256k1"))))))

