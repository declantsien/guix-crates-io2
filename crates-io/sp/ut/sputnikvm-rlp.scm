(define-module (crates-io sp ut sputnikvm-rlp) #:use-module (crates-io))

(define-public crate-sputnikvm-rlp-0.2.0 (c (n "sputnikvm-rlp") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "elastic-array") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1plphs1gra14pcv2p3jgb389pqxvmhqqc9li3chs1j9nkb45pm63")))

