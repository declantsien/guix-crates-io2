(define-module (crates-io sp oi spoilerowobot) #:use-module (crates-io))

(define-public crate-spoilerowobot-1.0.0 (c (n "spoilerowobot") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tbot") (r "^0.6.7") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1q1fanaa2axiiis45k297gjb93hyksqfyz8vk1a4wv341cnay4rj")))

(define-public crate-spoilerowobot-1.1.0 (c (n "spoilerowobot") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tbot") (r "^0.6.7") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "122j2c1y8bsr28cpknksf6w7y3svzqcgjyg2d4lsm97vfxlqzdmv")))

