(define-module (crates-io sp ef spef-parser) #:use-module (crates-io))

(define-public crate-spef-parser-0.1.0 (c (n "spef-parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cbindgen") (r "^0.26") (d #t) (k 1)) (d (n "pest") (r "^2.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6") (d #t) (k 0)))) (h "0js6h51s6nlpjb2rm9rjj1dn3ir2b7m8y0drqfjhrws5a25jl8gv") (y #t)))

(define-public crate-spef-parser-0.2.0 (c (n "spef-parser") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cbindgen") (r "^0.26") (d #t) (k 1)) (d (n "pest") (r "^2.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6") (d #t) (k 0)))) (h "1b69zbgsp8dh4fdpdpri9gpr3ah32pih40ba5bxh3iwhrgh4icz6")))

(define-public crate-spef-parser-0.2.1 (c (n "spef-parser") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cbindgen") (r "^0.26") (d #t) (k 1)) (d (n "pest") (r "^2.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6") (d #t) (k 0)))) (h "04gz1w2vicjxspvkplp0gyhq8lx5prb5n57m319s0581bqxcax72")))

(define-public crate-spef-parser-0.2.2 (c (n "spef-parser") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cbindgen") (r "^0.26") (d #t) (k 1)) (d (n "pest") (r "^2.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6") (d #t) (k 0)))) (h "1kwdmmqsvm5vnvf9qxay4srrv5r8g1fwrw413yz8zlbhmffda0h3")))

(define-public crate-spef-parser-0.2.3 (c (n "spef-parser") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cbindgen") (r "^0.26") (d #t) (k 1)) (d (n "pest") (r "^2.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6") (d #t) (k 0)))) (h "1n8i0wh8mh75pdp9hxm5kin6q0jidmry9r14n08ainbcldqs4h6v")))

(define-public crate-spef-parser-0.2.4 (c (n "spef-parser") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cbindgen") (r "^0.26") (d #t) (k 1)) (d (n "pest") (r "^2.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6") (d #t) (k 0)))) (h "0yd5ab56ysjg4vvymawkvlp2hzqhi065n9p8g9z22hsvs1ydvas8")))

