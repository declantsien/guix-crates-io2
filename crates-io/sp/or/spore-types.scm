(define-module (crates-io sp or spore-types) #:use-module (crates-io))

(define-public crate-spore-types-0.1.0 (c (n "spore-types") (v "0.1.0") (d (list (d (n "molecule") (r "^0.7.3") (k 0)))) (h "1vzd2cy50q19k5aapfph78p4m92z36b87yhmcp3wkb4ic4v02gnk")))

(define-public crate-spore-types-0.2.0 (c (n "spore-types") (v "0.2.0") (d (list (d (n "molecule") (r "^0.7.3") (k 0)))) (h "113ighbvydxcpfbpc21w255mv9pdddw7pl406shwgzzx95g0pqm9")))

