(define-module (crates-io sp or sports) #:use-module (crates-io))

(define-public crate-sports-0.1.0 (c (n "sports") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0yszhr2di661022gagqlc676bwwpmhflmmf6k50h5h2i3z6b9fls")))

