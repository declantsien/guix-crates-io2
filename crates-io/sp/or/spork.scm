(define-module (crates-io sp or spork) #:use-module (crates-io))

(define-public crate-spork-0.0.1 (c (n "spork") (v "0.0.1") (h "0qaq8rwnrq429xfq4rvrafsncl6dn1anmrsbrvin1hlvh7rw1f5r") (y #t)))

(define-public crate-spork-0.0.2 (c (n "spork") (v "0.0.2") (d (list (d (n "num_cpus") (r "^0.2.12") (d #t) (k 0)))) (h "11bh98i9a9d658zq1yhydw5xgcmdbsdnqwz19f1yfshbcw4p14g8") (y #t)))

(define-public crate-spork-0.0.3 (c (n "spork") (v "0.0.3") (d (list (d (n "num_cpus") (r "^0.2.12") (d #t) (k 0)))) (h "0b1bj34440wc93xahbfgf6gdr4csw5m9x5x6766ap9r29wndvlyg") (y #t)))

(define-public crate-spork-0.0.4 (c (n "spork") (v "0.0.4") (d (list (d (n "num_cpus") (r "^0.2.12") (d #t) (k 0)))) (h "0daxj6vb47pjwc6d563s1l9l6l5lawc3vk66pcfnw6rhvn4gxqma") (y #t)))

(define-public crate-spork-0.0.5 (c (n "spork") (v "0.0.5") (d (list (d (n "num_cpus") (r "^0.2.12") (d #t) (k 0)))) (h "0kb6542kdq7a6by754smi72gzl14m7agb756sy0nw04s5cir95a2") (y #t)))

(define-public crate-spork-0.1.0 (c (n "spork") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.1") (f (quote ("std"))) (k 0)) (d (n "ipc-channel") (r "^0.16.0") (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std_rng" "std"))) (k 0)) (d (n "rmp-serde") (r "^1.1.1") (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.37") (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1qyn4wnkskqsc14d2djj3nbag91c7m3206ixyb8ib8b26xcpis68")))

