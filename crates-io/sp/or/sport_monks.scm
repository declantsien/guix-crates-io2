(define-module (crates-io sp or sport_monks) #:use-module (crates-io))

(define-public crate-sport_monks-0.1.0 (c (n "sport_monks") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "mockito") (r "^0.13.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.8") (f (quote ("hyper-011"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cmi3nza19q0rn4fva7184nk2amv7aj7baxih7zwbjmhy4v9glrq")))

(define-public crate-sport_monks-0.1.1 (c (n "sport_monks") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "mockito") (r "^0.13.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.8") (f (quote ("hyper-011"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18ybs3gk8f9446blapfm0m9qsrxba2vcfmlf5nvk5fg3f3d0askq")))

(define-public crate-sport_monks-0.1.2 (c (n "sport_monks") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "mockito") (r "^0.13.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.8") (f (quote ("hyper-011"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0aamxxdx9cpj2iw59wk6kpnh1wq5c5gsabs5vk736zl8kj2yg0zh")))

(define-public crate-sport_monks-0.1.3 (c (n "sport_monks") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "mockito") (r "^0.13.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xmxfl7i7lgxv132hml737ysk26rlihdhi8d3fwyl1n7dl0lvmyh")))

(define-public crate-sport_monks-0.1.4 (c (n "sport_monks") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "mockito") (r "^0.13.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.16") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06zzxwfyc6vvv8z99pfi6i1cyh4yd8ndi1vl6x002bnyxmqlp9z2")))

