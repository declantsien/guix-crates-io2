(define-module (crates-io sp am spamprotection) #:use-module (crates-io))

(define-public crate-spamprotection-0.1.0 (c (n "spamprotection") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "09bd7dnq3831nql4mh8jj7j2ss2fv93hr58806acxakbkqnxwk0v") (y #t)))

(define-public crate-spamprotection-0.1.0-1 (c (n "spamprotection") (v "0.1.0-1") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ixv3fh41hqlskvnlnp2bdrfigl8kgz8gy87dchivc0knramy2a1")))

(define-public crate-spamprotection-0.1.0-2 (c (n "spamprotection") (v "0.1.0-2") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gvpgasgk7mifbghcpgl6q7sgl52riixyiifcjq0jd4lks0mlp8v")))

