(define-module (crates-io sp pg sppg) #:use-module (crates-io))

(define-public crate-sppg-0.1.0 (c (n "sppg") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0m9nsal7wdpddkph561y2w7gljay6vjlzh2647qql2pnqxcbgdsw")))

(define-public crate-sppg-0.1.1 (c (n "sppg") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1m5v8jknwrgpcfzlqgxfxbjb0rlvqb5ld5sjl9iw9g5p35y3kj1g")))

(define-public crate-sppg-0.2.0 (c (n "sppg") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0zn9snva8ihn0zw9g3smnzm84vksb4hd9cxf0qdv8m7qhp1rjblh")))

(define-public crate-sppg-0.3.0 (c (n "sppg") (v "0.3.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0s7yp2y9rmcwi6i44sxzq0a7wcyc014ffjnay6ajlav808f0jr4l")))

