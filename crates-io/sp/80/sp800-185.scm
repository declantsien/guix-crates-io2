(define-module (crates-io sp #{80}# sp800-185) #:use-module (crates-io))

(define-public crate-sp800-185-0.1.0 (c (n "sp800-185") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.3") (d #t) (k 0)))) (h "1bgkjmv8744sp33m5f0a7mvyfwsp0w38ps5xkza53h7nnqzdg6iq") (f (quote (("parallelhash" "rayon"))))))

(define-public crate-sp800-185-0.1.1 (c (n "sp800-185") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "rayon") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.3") (d #t) (k 0)))) (h "0rndsr4nb31q1z4bb3xl6jkm7p3sx84bzdqrnllr1w1wril6mkjp") (f (quote (("parallelhash" "rayon"))))))

(define-public crate-sp800-185-0.1.2 (c (n "sp800-185") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "rayon") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.3") (d #t) (k 0)))) (h "17wsi1xj397ianrqbby8nmr78988wcwr939k8gk6d0q06gv096ns") (f (quote (("parallelhash" "rayon"))))))

(define-public crate-sp800-185-0.2.0 (c (n "sp800-185") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0") (k 0)) (d (n "rayon") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4") (d #t) (k 0)))) (h "1jagn8nqh3yz389hh9yl3ydqxywfvxpclnj2jnqr1w6v3lxqxcf0") (f (quote (("parallelhash" "rayon"))))))

