(define-module (crates-io sp ik spike) #:use-module (crates-io))

(define-public crate-spike-0.0.0 (c (n "spike") (v "0.0.0") (h "1bh9fwa93cpmiy4ypczxabxa1wyfqqggsqygy6wlyrd9hh78xbzn")))

(define-public crate-spike-0.0.1 (c (n "spike") (v "0.0.1") (d (list (d (n "matchit") (r "^0.7.2") (d #t) (k 0)) (d (n "touche") (r "^0.0.8") (f (quote ("server"))) (k 0)))) (h "1ggnywwzb8vbjrvwc4nwjkly6bkyjjilf9cm2x7lx5jxdixh33qw")))

(define-public crate-spike-0.0.2 (c (n "spike") (v "0.0.2") (d (list (d (n "matchit") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "touche") (r "^0.0.9") (f (quote ("server"))) (k 0)))) (h "0xl1pyxipw12wmcnwyvcp1pnjs5i0jk2kvmz0qgx49pl178k3xxq") (f (quote (("json" "serde" "serde_json"))))))

