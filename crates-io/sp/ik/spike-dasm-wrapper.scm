(define-module (crates-io sp ik spike-dasm-wrapper) #:use-module (crates-io))

(define-public crate-spike-dasm-wrapper-0.0.1 (c (n "spike-dasm-wrapper") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0jhhcidajkspdb9fkabqbhzbsqslhiv3di3v79zap1ss38qmq1mp")))

(define-public crate-spike-dasm-wrapper-0.0.2 (c (n "spike-dasm-wrapper") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "096p2x9lf0m2cs35rhbcddb4b107dqbpylh9g53ml11wrvvmanr3")))

