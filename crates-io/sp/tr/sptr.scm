(define-module (crates-io sp tr sptr) #:use-module (crates-io))

(define-public crate-sptr-0.1.0 (c (n "sptr") (v "0.1.0") (h "11hy27q9b475zvnr2habrlr156hglrk83ixnv21isq4zr260p48h") (f (quote (("deprecate_problems") ("default" "deprecate_problems"))))))

(define-public crate-sptr-0.1.1 (c (n "sptr") (v "0.1.1") (h "0jw6qzqfnizjld617rkpk506jn3zsfzszqiw58rkmgwaga8sbl7x") (f (quote (("deprecate_problems") ("default" "deprecate_problems"))))))

(define-public crate-sptr-0.2.0 (c (n "sptr") (v "0.2.0") (h "0shnn9pc77hbc5yayhs9fszvvkfbw4rayvcx042jqwnk3cmdaqjw") (f (quote (("uptr") ("opaque_fn") ("deprecate_problems") ("default" "deprecate_problems"))))))

(define-public crate-sptr-0.2.1 (c (n "sptr") (v "0.2.1") (h "0psinx1rdxn56ansx0xrp59gp9250gck9cisinzim7d1rfyzpczr") (f (quote (("uptr") ("opaque_fn") ("deprecate_problems") ("default" "deprecate_problems"))))))

(define-public crate-sptr-0.2.2 (c (n "sptr") (v "0.2.2") (h "18a1xrgzk6k4j289nk0frl8zblcnvbbl1dzbvrml821hj129cvgm") (f (quote (("uptr") ("opaque_fn") ("deprecate_problems") ("default" "deprecate_problems"))))))

(define-public crate-sptr-0.2.3 (c (n "sptr") (v "0.2.3") (h "03jvvr822x5pffyvswz6crmzaf8s4y3igj85ngsc27hv6y2yxpjw") (f (quote (("uptr") ("opaque_fn") ("deprecate_problems") ("default" "deprecate_problems"))))))

(define-public crate-sptr-0.3.0 (c (n "sptr") (v "0.3.0") (h "1g4ag30pwgrm7x7bawihy4iwia2gml3yrxrpbh8bjvgaf5m52wga") (f (quote (("uptr") ("opaque_fn") ("default"))))))

(define-public crate-sptr-0.3.1 (c (n "sptr") (v "0.3.1") (h "0hcryk2wh9s7bhi121xgmqyk61605777gnjq559fi48v29jl638y") (f (quote (("uptr") ("opaque_fn") ("default"))))))

(define-public crate-sptr-0.3.2 (c (n "sptr") (v "0.3.2") (h "0shddkys046nnrng929mrnjjrh31mlxl95ky7dgxd6i4kclkk6rv") (f (quote (("uptr") ("opaque_fn") ("default"))))))

