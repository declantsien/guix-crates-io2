(define-module (crates-io sp la splay) #:use-module (crates-io))

(define-public crate-splay-0.1.0 (c (n "splay") (v "0.1.0") (h "0p0q9p24a7pa08jmfbzrxxbhk2wdyxbj4prpfajm25hsn5is4rjh")))

(define-public crate-splay-0.1.2 (c (n "splay") (v "0.1.2") (h "0dnmfc82zaqmnh7bdlpbfqhxhlkap6avn313nkcjshxdh8qcljvf")))

(define-public crate-splay-0.1.3 (c (n "splay") (v "0.1.3") (h "0lqx4jy45rn3al242wdy311d47kbydpx135nckx94094nkcdmkwh")))

(define-public crate-splay-0.1.4 (c (n "splay") (v "0.1.4") (d (list (d (n "rand") (r "^0.1") (d #t) (k 2)))) (h "13vchax33z65gvw82p1y5sy7s9qdzmv1y4ngaqhjkcz4n2l2y4xa")))

(define-public crate-splay-0.1.5 (c (n "splay") (v "0.1.5") (d (list (d (n "rand") (r "^0.1") (d #t) (k 2)))) (h "08r5zjr6p3gkhslpyv7k3zvjw4d507gax0m50d6k270ifwd09ph5")))

(define-public crate-splay-0.1.6 (c (n "splay") (v "0.1.6") (d (list (d (n "rand") (r "^0.2") (d #t) (k 2)))) (h "1lc411vb6v7mnl59vnngrx1ypm6hfbhik46r4r5a0l70s9fvfb44")))

(define-public crate-splay-0.1.7 (c (n "splay") (v "0.1.7") (d (list (d (n "rand") (r "^0.2") (d #t) (k 2)))) (h "0zp3c4r0qmp1jsylnfx050v192vk2qmb015n58l0k3gv61id4yqw")))

(define-public crate-splay-0.1.8 (c (n "splay") (v "0.1.8") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1kg8sy0kmfakjd2riv5bwknrv5kvagpcv3nb0x3kvlagask9j0cr")))

