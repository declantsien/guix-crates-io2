(define-module (crates-io sp la splay-safe-rs) #:use-module (crates-io))

(define-public crate-splay-safe-rs-0.7.10 (c (n "splay-safe-rs") (v "0.7.10") (d (list (d (n "compare") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand-op") (r "^0.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zxp6yyv219a1f2dw68crdpc28c538zda00hb9w2kngqln9fdm95")))

(define-public crate-splay-safe-rs-0.7.11 (c (n "splay-safe-rs") (v "0.7.11") (d (list (d (n "compare") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand-op") (r "^0.1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rafvmr69gffwwvc4dv1chwhq8lywlhs3vwwsn4lp6k0ygqanb8n") (f (quote (("std") ("default" "std"))))))

