(define-module (crates-io sp la splat_derive) #:use-module (crates-io))

(define-public crate-splat_derive-0.1.0 (c (n "splat_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0z7z04gcfq9rxvjb39zqsq7i8ivdcmkccbv5vazvxvigsyxnls6y")))

(define-public crate-splat_derive-0.1.1 (c (n "splat_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1gldwrvhwcybczqi3dmwsxxrn6zs4cgnwvcjv6h3h6ny2b9la6gh")))

