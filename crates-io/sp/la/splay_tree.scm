(define-module (crates-io sp la splay_tree) #:use-module (crates-io))

(define-public crate-splay_tree-0.1.0 (c (n "splay_tree") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0ljbrc67dnis966lh8rlwlmk3rxqyiv0b84fd8mdbsg0amrgwv6z")))

(define-public crate-splay_tree-0.1.1 (c (n "splay_tree") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "154abysjpfsp49fc1qi33ydfyvdrgnn02slz2pz1max484s4sxjs")))

(define-public crate-splay_tree-0.2.0 (c (n "splay_tree") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1h2rqxk5iqihzf1fdkrl35llx6v8dd8qwscwfpjiimp9bxwglczw")))

(define-public crate-splay_tree-0.2.1 (c (n "splay_tree") (v "0.2.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "10nfk712r7zmd6acj70b9wgkjhml22v8llk9azvsgc4hf55qzz8x")))

(define-public crate-splay_tree-0.2.2 (c (n "splay_tree") (v "0.2.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0g8alzc91sfc072gphzaakvswisrf3vs5izlcv9ifmscb6fpbapy")))

(define-public crate-splay_tree-0.2.3 (c (n "splay_tree") (v "0.2.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "17fkwaz8269195b2gfdn7h06wqvxck1i7agjzflmr8ldj2y1wd6v")))

(define-public crate-splay_tree-0.2.4 (c (n "splay_tree") (v "0.2.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1bwayj8gfigpzs27gvlr9833n3lb50bm1shsjam2yjz5hmrwfz3d")))

(define-public crate-splay_tree-0.2.5 (c (n "splay_tree") (v "0.2.5") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0qw1xsf9s09jz92damcjxkg3r5dzbznkhvpzjk5aszrjkg9frlap")))

(define-public crate-splay_tree-0.2.6 (c (n "splay_tree") (v "0.2.6") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1b22k3f85dfvmhdh73b00wh61dvcn9794a0ngx97vih3z3rshwbp")))

(define-public crate-splay_tree-0.2.7 (c (n "splay_tree") (v "0.2.7") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (o #t) (k 0)))) (h "1smfvcv1qlhn9bh7h24s5h00s2wc5kfip5v31zrc9hbgpvk7krdj") (f (quote (("std") ("default" "std"))))))

(define-public crate-splay_tree-0.2.8 (c (n "splay_tree") (v "0.2.8") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (o #t) (k 0)))) (h "1nfj70cbxkz0iv1xppbmsj1wwvc82364yb7rwpmdxwiiz1jhij5c") (f (quote (("std") ("default" "std"))))))

(define-public crate-splay_tree-0.2.9 (c (n "splay_tree") (v "0.2.9") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1rnjwycaw09vhrbxaxl9yqm9y79wn623xvq3fw1n3y507dpa6h89") (f (quote (("std") ("default" "std"))))))

(define-public crate-splay_tree-0.2.10 (c (n "splay_tree") (v "0.2.10") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1hgcjpqgfjhz35mdi62dns0lgag105painfbaalgga60jc6yx79h") (f (quote (("std") ("default" "std"))))))

(define-public crate-splay_tree-0.3.0 (c (n "splay_tree") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0c1s1m6gcp1bhgs302bdypiy67bbs536p84lrg2xh0dyrb8967zv") (f (quote (("std") ("default" "std"))))))

(define-public crate-splay_tree-0.3.1 (c (n "splay_tree") (v "0.3.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("alloc" "derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0zrmhp67q1sp807na9qjdyd87mwz0jipcb1n31dk5wglahwgjhgl") (f (quote (("std") ("default" "std"))))))

