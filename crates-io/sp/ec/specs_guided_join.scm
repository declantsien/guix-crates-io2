(define-module (crates-io sp ec specs_guided_join) #:use-module (crates-io))

(define-public crate-specs_guided_join-0.1.0 (c (n "specs_guided_join") (v "0.1.0") (d (list (d (n "hibitset") (r "^0.3") (d #t) (k 0)) (d (n "specs") (r "^0.10") (d #t) (k 0)))) (h "1hrqspw86lamrlii8mjx5mz996dqdiv5ivcxn3cwv95xp5zdwzw3")))

(define-public crate-specs_guided_join-0.1.1 (c (n "specs_guided_join") (v "0.1.1") (d (list (d (n "hibitset") (r "^0.3") (d #t) (k 0)) (d (n "specs") (r "^0.10") (d #t) (k 0)))) (h "0lgim3gay7zy4ba7xzzj4hwfq8bs3l2l7xs0zds1qmmqd7g72j9j")))

(define-public crate-specs_guided_join-0.1.3 (c (n "specs_guided_join") (v "0.1.3") (d (list (d (n "hibitset") (r "^0.3") (d #t) (k 0)) (d (n "specs") (r "^0.10") (d #t) (k 0)))) (h "0fbvkvrs0r9mqgzcspqmlb3jglhs40zzky7w1wy1nd8h93n4hsgq")))

(define-public crate-specs_guided_join-0.1.4 (c (n "specs_guided_join") (v "0.1.4") (d (list (d (n "hibitset") (r "^0.3") (d #t) (k 0)) (d (n "specs") (r "^0.10") (d #t) (k 0)))) (h "0sinfvjfpddhrbc4qxnaa5c261nr9lrr49b4hc0r74xp2r1v0k16")))

(define-public crate-specs_guided_join-0.1.5 (c (n "specs_guided_join") (v "0.1.5") (d (list (d (n "hibitset") (r "^0.3") (d #t) (k 0)) (d (n "specs") (r "^0.10") (d #t) (k 0)))) (h "09k7nhb92w14jyzs08jl7l1cbd6804p0pzi24yvxdwqcfagl7am5")))

(define-public crate-specs_guided_join-0.2.0 (c (n "specs_guided_join") (v "0.2.0") (d (list (d (n "hibitset") (r ">= 0.3") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "specs") (r ">= 0.9") (d #t) (k 0)))) (h "1ndccmb4fj0slrn26vm1fp6w4z1pn1crm3h9avycnavv29dcjn6q")))

(define-public crate-specs_guided_join-0.2.1 (c (n "specs_guided_join") (v "0.2.1") (d (list (d (n "hibitset") (r ">= 0.3") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "specs") (r ">= 0.9") (d #t) (k 0)))) (h "1d8fqzxrh2hqqng3kk1pj2lcbkaarxrkfdd3845v9974vlz7h94l")))

