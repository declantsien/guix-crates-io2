(define-module (crates-io sp ec spectrusty-audio) #:use-module (crates-io))

(define-public crate-spectrusty-audio-0.1.0 (c (n "spectrusty-audio") (v "0.1.0") (d (list (d (n "cpal") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.34") (o #t) (d #t) (k 0)) (d (n "spectrusty-core") (r "^0.1") (d #t) (k 0)))) (h "1xzhwh05idb1ay1ghzv45v65r54arj2kjm93camkmqm8s10m5gqb") (f (quote (("default"))))))

(define-public crate-spectrusty-audio-0.2.0 (c (n "spectrusty-audio") (v "0.2.0") (d (list (d (n "cpal") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.34") (o #t) (d #t) (k 0)) (d (n "spectrusty-core") (r "^0.2") (d #t) (k 0)))) (h "1l21lq3jyxmza5hmk88mmcv98jv11c3nklmky35bbbzdizzv5cf7") (f (quote (("default"))))))

(define-public crate-spectrusty-audio-0.2.1 (c (n "spectrusty-audio") (v "0.2.1") (d (list (d (n "cpal") (r "^0.13.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.34") (o #t) (d #t) (k 0)) (d (n "spectrusty-core") (r "^0.2.1") (d #t) (k 0)))) (h "1wws64ds79013k8pmrmg93gg7q98if36w0x3i70d11znvy19vrv4") (f (quote (("default"))))))

(define-public crate-spectrusty-audio-0.3.0 (c (n "spectrusty-audio") (v "0.3.0") (d (list (d (n "cpal") (r "^0.14.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (o #t) (d #t) (k 0)) (d (n "spectrusty-core") (r "^0.3.0") (d #t) (k 0)))) (h "0f0ymr5mis32rlj4yfqysa0gfipwhzqj2pnk060vx56j6j614a8z") (f (quote (("default"))))))

(define-public crate-spectrusty-audio-0.4.0 (c (n "spectrusty-audio") (v "0.4.0") (d (list (d (n "cpal") (r "^0.14.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (o #t) (d #t) (k 0)) (d (n "spectrusty-core") (r "^0.4.0") (d #t) (k 0)))) (h "1y9myci96qxpcsqg4pm7vzdgq3nvmm2vlkaih1rwj4q28vifiv38") (f (quote (("default"))))))

