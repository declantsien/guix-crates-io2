(define-module (crates-io sp ec spectacles-proxy) #:use-module (crates-io))

(define-public crate-spectacles-proxy-0.4.0 (c (n "spectacles-proxy") (v "0.4.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1.16") (d #t) (k 0)) (d (n "tokio-tls") (r "^0.2") (d #t) (k 0)) (d (n "unicase") (r "^2.3") (d #t) (k 0)))) (h "1z954vajj54mx6p3z0f0cr0kcpgg4i0jbabfk3f9dg3xz1r51qza")))

