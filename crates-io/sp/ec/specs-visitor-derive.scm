(define-module (crates-io sp ec specs-visitor-derive) #:use-module (crates-io))

(define-public crate-specs-visitor-derive-0.1.0 (c (n "specs-visitor-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "specs") (r "^0.14.1") (d #t) (k 2)) (d (n "specs-visitor") (r "^0.1.0") (d #t) (k 2)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "01a7kr88sbxl5fdxr15000b4wq6p27jiv8ps8k5fd98vxwhhbpqv")))

(define-public crate-specs-visitor-derive-0.1.1 (c (n "specs-visitor-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "specs") (r "^0.14.1") (d #t) (k 2)) (d (n "specs-visitor") (r "^0.1.0") (d #t) (k 2)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "1pc5jrdn71bi69jr38jf4ckh3davw1b2ivbngfncfa2km3xpjgl6")))

(define-public crate-specs-visitor-derive-0.2.0 (c (n "specs-visitor-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "specs") (r "^0.14.1") (d #t) (k 2)) (d (n "specs-visitor") (r "^0.2.0") (d #t) (k 2)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "0apgfy8hv1g840x4yr3rry1mfda75x3928492brlh52v52kv3yzi")))

(define-public crate-specs-visitor-derive-0.3.0 (c (n "specs-visitor-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "specs") (r "^0.14.3") (d #t) (k 2)) (d (n "specs-visitor") (r "^0.3.0") (d #t) (k 2)) (d (n "synstructure") (r "^0.10.1") (d #t) (k 0)))) (h "19y4nmnks8g72fiflr04v746jzjsy8jbscg4mj0bnak3g79ygdrm")))

