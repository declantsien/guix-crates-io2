(define-module (crates-io sp ec spectrodraw) #:use-module (crates-io))

(define-public crate-spectrodraw-0.1.0 (c (n "spectrodraw") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "small_morse") (r "^0.1.0") (d #t) (k 0)) (d (n "wav") (r "^1.0.0") (d #t) (k 0)))) (h "0g3y1gkygdxl7cm4p55jbjq0pfxin8m5nkn27sn4ywp4nw3jh93s")))

