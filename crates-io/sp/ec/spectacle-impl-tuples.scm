(define-module (crates-io sp ec spectacle-impl-tuples) #:use-module (crates-io))

(define-public crate-spectacle-impl-tuples-0.1.0 (c (n "spectacle-impl-tuples") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "181i9v9pwsqrxamrgq2p5s0hk1hjyw2sfz3ccy01xff9ac92fsd9")))

(define-public crate-spectacle-impl-tuples-0.2.0 (c (n "spectacle-impl-tuples") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "177jk1i6h28ayqzjqsx7kchb408r9z9cc120i6fyngf3gwdwvx1f")))

