(define-module (crates-io sp ec special-folder) #:use-module (crates-io))

(define-public crate-special-folder-0.1.0 (c (n "special-folder") (v "0.1.0") (d (list (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "windows-sys") (r "^0.36") (f (quote ("Win32_UI_Shell" "Win32_Foundation"))) (d #t) (k 0)))) (h "1hms0lq5smksffs622yqpib5ql3xg2pv19lj2sqqp7zbg8fr8y3k")))

(define-public crate-special-folder-0.2.0 (c (n "special-folder") (v "0.2.0") (d (list (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "windows-sys") (r "^0.36") (f (quote ("Win32_UI_Shell" "Win32_Foundation"))) (d #t) (k 0)))) (h "1rvv10y9jx112i0ig2m5w8wbj6gyw1wdjcm9j61s886i5mr7kv0g")))

(define-public crate-special-folder-0.2.2 (c (n "special-folder") (v "0.2.2") (d (list (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "windows-sys") (r "^0.36") (f (quote ("Win32_UI_Shell" "Win32_Foundation"))) (d #t) (k 0)))) (h "15y5592ibyfj343qaqbrq24jaqivpnwhm0yg3nh8nv820hny8frd")))

