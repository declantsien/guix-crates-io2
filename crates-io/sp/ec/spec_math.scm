(define-module (crates-io sp ec spec_math) #:use-module (crates-io))

(define-public crate-spec_math-0.1.0 (c (n "spec_math") (v "0.1.0") (h "1kq2lcsdh7a4qqr20p145s0zjgw4dq9i37z8zlah8qyw7rszlmh3")))

(define-public crate-spec_math-0.1.1 (c (n "spec_math") (v "0.1.1") (h "0w8qs9619rk68d4nvcj6lbl8mvbck1x3naypn1rpq51vhbyl2is4")))

(define-public crate-spec_math-0.1.2 (c (n "spec_math") (v "0.1.2") (h "11478lm0dlkaviyx52gybpp73m8zgxnyj1jmvs7m5w9s57ib6zc1")))

(define-public crate-spec_math-0.1.3 (c (n "spec_math") (v "0.1.3") (h "1qbskywnrh4l1pa92iamazjm3vczxnjf1m8iv4qmaqhl87zl5za6")))

(define-public crate-spec_math-0.1.4 (c (n "spec_math") (v "0.1.4") (h "086dfh9z9ik1gvj8bqg0q1qhabb911pl93ldncikqrv3nfjhlj9y")))

(define-public crate-spec_math-0.1.5 (c (n "spec_math") (v "0.1.5") (h "1aswq4lplri1slkjdrgfmb07d3q1ss0zcds2bycx9c44s2j19cpi")))

