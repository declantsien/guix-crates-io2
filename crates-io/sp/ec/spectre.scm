(define-module (crates-io sp ec spectre) #:use-module (crates-io))

(define-public crate-spectre-0.1.0 (c (n "spectre") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.30.1") (d #t) (k 0)))) (h "040kdd7d0jy1pyfnnk51c4v0c25bgwhznx9k6w33kv3zn2mqqpzr")))

(define-public crate-spectre-0.2.0 (c (n "spectre") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.30.1") (d #t) (k 0)))) (h "1py8wgm7rxsg2117nrdzif2qwcsx7csvqhk01p99a7q3iyn3s4g0")))

(define-public crate-spectre-0.3.0 (c (n "spectre") (v "0.3.0") (d (list (d (n "nalgebra") (r "^0.30.1") (d #t) (k 0)))) (h "0rnp79x1rvylalk5xlr2qn65jdsj1p2y9zxp1ipqxv03kiswpaqm")))

(define-public crate-spectre-0.4.0 (c (n "spectre") (v "0.4.0") (d (list (d (n "nalgebra") (r "^0.31") (d #t) (k 0)))) (h "1falyqjip4jgq5yvndd1yv4nr3qh280y2gy7zgh7haw2qh5bgj8y")))

(define-public crate-spectre-0.5.0 (c (n "spectre") (v "0.5.0") (d (list (d (n "nalgebra") (r "^0.31") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0h1rhamzhacgbx9swd0h6llz110wxmk5wi923gf65psa2w4pn2d8")))

(define-public crate-spectre-0.5.1 (c (n "spectre") (v "0.5.1") (d (list (d (n "nalgebra") (r "^0.31") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1bb4j4vg0sxwvna804dhqj5j2bdn3a3cc288wx7c3n80b6bfr9ah")))

(define-public crate-spectre-0.5.2 (c (n "spectre") (v "0.5.2") (d (list (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "017gh9h8ir0x5dh6lwr84d81y08k8dxsg2jryk6cfs25mxvlvfgk")))

(define-public crate-spectre-0.6.0 (c (n "spectre") (v "0.6.0") (d (list (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0ws5vm5j181vmxyxkayimxmmlx978ggb2fhqbxs40phw9y4n28wj")))

(define-public crate-spectre-0.7.0 (c (n "spectre") (v "0.7.0") (d (list (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0g67f8nkqpyd964mjjxkbnmyaqi54a3qnvx0gh8x6ancvknn3b56")))

