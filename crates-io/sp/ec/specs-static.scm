(define-module (crates-io sp ec specs-static) #:use-module (crates-io))

(define-public crate-specs-static-0.1.0 (c (n "specs-static") (v "0.1.0") (d (list (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "hibitset") (r "^0.3") (d #t) (k 0)) (d (n "shred") (r "^0.5") (d #t) (k 0)) (d (n "specs") (r "^0.10") (d #t) (k 0)))) (h "1yahvk92hhscmqgrqz5vfryvvaavqs205614d3yvpyl80zgjap2l")))

(define-public crate-specs-static-0.1.1 (c (n "specs-static") (v "0.1.1") (d (list (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "hibitset") (r "^0.3") (d #t) (k 0)) (d (n "shred") (r "^0.5") (d #t) (k 0)) (d (n "specs") (r "^0.10") (d #t) (k 0)))) (h "101qqvfcw3k48rm0mg8ks8ff6gvz4bzsppl0ap30dnkpg087hp2i")))

(define-public crate-specs-static-0.2.0 (c (n "specs-static") (v "0.2.0") (d (list (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "hibitset") (r "^0.5") (d #t) (k 0)) (d (n "shred") (r "^0.7") (d #t) (k 0)) (d (n "specs") (r "^0.11") (d #t) (k 0)))) (h "1ircrs28xa4z16zbffdnim0j413ns12m0ym46qcwzy9cv1l15dx7")))

(define-public crate-specs-static-0.3.0 (c (n "specs-static") (v "0.3.0") (d (list (d (n "derivative") (r "^1") (d #t) (k 0)) (d (n "hibitset") (r "^0.5") (d #t) (k 0)) (d (n "shred") (r "^0.7") (d #t) (k 0)) (d (n "shrev") (r "^1.0") (d #t) (k 0)) (d (n "specs") (r "^0.14") (d #t) (k 0)))) (h "0zvnwk1ijp9x0kvrih4pjixp5zmyqm5w8096cgnh7dvl9lcsyf3c")))

