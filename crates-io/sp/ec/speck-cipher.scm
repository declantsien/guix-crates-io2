(define-module (crates-io sp ec speck-cipher) #:use-module (crates-io))

(define-public crate-speck-cipher-0.0.0 (c (n "speck-cipher") (v "0.0.0") (h "0wlm4b3yrnd27rg261zjr70fg1zgr95z1g0szg8x5fmmkf9p4irl")))

(define-public crate-speck-cipher-0.0.1 (c (n "speck-cipher") (v "0.0.1") (d (list (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "1bvld7943ly6ylr3l64634ah6ajlg1r3xyszb0ccc41z6yhmpmp8") (r "1.56")))

