(define-module (crates-io sp ec specialized-div-rem) #:use-module (crates-io))

(define-public crate-specialized-div-rem-0.0.0-alpha0 (c (n "specialized-div-rem") (v "0.0.0-alpha0") (d (list (d (n "rand") (r "^0.5.1") (f (quote ("i128_support"))) (d #t) (k 2)))) (h "0h3bc801syvanww545p4plq78gcsg5wa87n6h01mc50hkkawapdb")))

(define-public crate-specialized-div-rem-0.0.0-alpha1 (c (n "specialized-div-rem") (v "0.0.0-alpha1") (d (list (d (n "rand") (r "^0.5.1") (f (quote ("i128_support"))) (d #t) (k 2)))) (h "0c3hbp2m4nw73xjckrpp4vdlnzj00fcic8y5jyvdvx06skp762ka")))

(define-public crate-specialized-div-rem-0.0.0-alpha2 (c (n "specialized-div-rem") (v "0.0.0-alpha2") (d (list (d (n "rand") (r "^0.5.1") (f (quote ("i128_support"))) (d #t) (k 2)))) (h "00cqx7dv2j1lji73hk7f5841d7f8v80sigwgkvanf6idpyk9sl3v")))

(define-public crate-specialized-div-rem-0.0.1 (c (n "specialized-div-rem") (v "0.0.1") (d (list (d (n "rand") (r "^0.5.1") (f (quote ("i128_support"))) (d #t) (k 2)))) (h "0hihazdqq8pmhl7qf8fdpqv926r476rnwrnb9c1jmsa04q9wdm7d")))

(define-public crate-specialized-div-rem-0.0.2 (c (n "specialized-div-rem") (v "0.0.2") (d (list (d (n "rand") (r "^0.5.4") (f (quote ("i128_support"))) (d #t) (k 2)))) (h "1hk0fliika8pdqiqi47f4nz8k7a88mgrzyvil0ydshfmgjnwycr1")))

(define-public crate-specialized-div-rem-0.0.3 (c (n "specialized-div-rem") (v "0.0.3") (d (list (d (n "rand") (r "^0.5.5") (f (quote ("i128_support"))) (d #t) (k 2)))) (h "0xyw7sk5vd0ccn2c1s2bn0dnc5w46vsj47y5q9m1bsjh3wpiafaw")))

(define-public crate-specialized-div-rem-0.0.4 (c (n "specialized-div-rem") (v "0.0.4") (d (list (d (n "rand") (r "^0.6.4") (f (quote ("i128_support"))) (d #t) (k 2)))) (h "1y8idip212wmcb2x3pxn9pqypliv1d4ylm29cmvwx9gmy1yhxapw")))

(define-public crate-specialized-div-rem-0.0.5 (c (n "specialized-div-rem") (v "0.0.5") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "1dspl2nrphyxsc4f88si5mbjd5v6vl5zhkm4hl5md5qvxl30kjk3") (f (quote (("asm"))))))

(define-public crate-specialized-div-rem-0.1.0 (c (n "specialized-div-rem") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1frssm57sb58399b833x0140wxxv29nqk0bxg43xsh2aadb3i9ya") (f (quote (("no_std") ("asm"))))))

(define-public crate-specialized-div-rem-0.2.0 (c (n "specialized-div-rem") (v "0.2.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0ssn3rakcdsx5kmpwhvhiyjf55ld157x7b5rrimlrghyny9bgxk9") (f (quote (("no_std") ("asm"))))))

(define-public crate-specialized-div-rem-0.3.0 (c (n "specialized-div-rem") (v "0.3.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "002l8iinx20a6xl4861dgz30f6qdl5mclx9nj5khd53jj8g8rfbm") (f (quote (("no_std") ("asm"))))))

(define-public crate-specialized-div-rem-1.0.0 (c (n "specialized-div-rem") (v "1.0.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1falj5ibs6jnsjb8vn51p5d82h74a0yqpakn88cscfgayj2cmi4p") (f (quote (("std") ("no_lz") ("implement") ("default" "asm" "implement" "std") ("asm"))))))

(define-public crate-specialized-div-rem-1.0.1 (c (n "specialized-div-rem") (v "1.0.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1xbdhl1jk0494r3079w2lm90llk1h2mw2jyjfi0bl4j8kq61r3zz") (f (quote (("std") ("no_lz") ("implement") ("default" "asm" "implement" "std") ("asm"))))))

(define-public crate-specialized-div-rem-1.1.0 (c (n "specialized-div-rem") (v "1.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "10b9nx0bs9d4lnyp5xwyvpa9n8wilb837pnha7ilq3bs6kb0wsf5") (f (quote (("std") ("no_lz") ("implement") ("default" "asm" "implement" "std") ("asm"))))))

