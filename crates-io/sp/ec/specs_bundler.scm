(define-module (crates-io sp ec specs_bundler) #:use-module (crates-io))

(define-public crate-specs_bundler-0.1.0 (c (n "specs_bundler") (v "0.1.0") (d (list (d (n "specs") (r "^0.10") (d #t) (k 0)))) (h "0pqzrr5w1qickcqwfb1s453h2dp7b3rkinma6zbv8b2ifk0pc5wb")))

(define-public crate-specs_bundler-0.1.1 (c (n "specs_bundler") (v "0.1.1") (d (list (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "specs") (r "^0.10") (d #t) (k 0)))) (h "0kafmbb5shlwmyy7ng1ky7gxpxb7mpw3xmk76nx7wac4g23ng300")))

(define-public crate-specs_bundler-0.2.0 (c (n "specs_bundler") (v "0.2.0") (d (list (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "specs") (r "^0.10") (d #t) (k 0)))) (h "0x8jw2v0zq0h8f2zn8gkv1iv868yzpcz27jihz1qhp1vdv5jhwxp")))

(define-public crate-specs_bundler-0.2.1 (c (n "specs_bundler") (v "0.2.1") (d (list (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "specs") (r "^0.10") (d #t) (k 0)))) (h "057nawkx73srwzjq6w0gjanavn9xhp5mf104v81npswmdjlmm94x")))

(define-public crate-specs_bundler-0.3.0 (c (n "specs_bundler") (v "0.3.0") (d (list (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "specs") (r "^0.11") (d #t) (k 0)))) (h "1z4sjqawg5168x8r973c7pvn51269f5yxr1nabxnil2d9hjmyar5")))

(define-public crate-specs_bundler-0.3.1 (c (n "specs_bundler") (v "0.3.1") (d (list (d (n "rayon") (r ">= 1.0") (d #t) (k 0)) (d (n "specs") (r ">= 0.11") (d #t) (k 0)))) (h "1948hha1ibqh157hhbg66kpkj54w017vm675gglbpbrizqwhkxsz")))

(define-public crate-specs_bundler-0.3.2 (c (n "specs_bundler") (v "0.3.2") (d (list (d (n "rayon") (r ">= 1.0") (d #t) (k 0)) (d (n "specs") (r ">= 0.11") (d #t) (k 0)))) (h "0lkmyilp18jy3p6wqvjzvr7sa0i3ck30yj9qiwya5h85jakkqj7i")))

(define-public crate-specs_bundler-0.4.0 (c (n "specs_bundler") (v "0.4.0") (d (list (d (n "rayon") (r ">= 1.0") (d #t) (k 0)) (d (n "specs") (r ">= 0.11") (d #t) (k 0)))) (h "016wkhpy0rffbkpa43wygxvlrrggcrhy3v450zaa03jwcrmck046")))

(define-public crate-specs_bundler-0.5.0 (c (n "specs_bundler") (v "0.5.0") (d (list (d (n "rayon") (r ">= 1.0") (d #t) (k 0)) (d (n "specs") (r ">= 0.11") (d #t) (k 0)))) (h "1yv92cg1nzdfa20jh013zy3sibz0cb8rijr3m8070d55yqn0d9p4")))

(define-public crate-specs_bundler-0.6.0 (c (n "specs_bundler") (v "0.6.0") (d (list (d (n "rayon") (r ">= 1.1") (d #t) (k 0)) (d (n "specs") (r ">= 0.15") (d #t) (k 0)))) (h "1hhsjzzkvyg80k368dwryzxvmjk1g8j99pbklyacy7w0qxw4axn2")))

