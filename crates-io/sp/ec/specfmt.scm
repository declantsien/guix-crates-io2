(define-module (crates-io sp ec specfmt) #:use-module (crates-io))

(define-public crate-specfmt-0.1.0 (c (n "specfmt") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "test-generator") (r "^0.3.0") (d #t) (k 2)))) (h "0bzkp7gnqlb0wlmdnwxnf8jvjkgpbrdyz3v9n5p9a58188brinn5")))

(define-public crate-specfmt-0.2.0 (c (n "specfmt") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "test-generator") (r "^0.3.0") (d #t) (k 2)))) (h "0m47jrf7xl4wgxwz36cf0csfwqsf29sqn7vkq3rq7wvhj7pmvq36")))

(define-public crate-specfmt-0.2.1 (c (n "specfmt") (v "0.2.1") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "test-generator") (r "^0.3.0") (d #t) (k 2)))) (h "03vjvsfy6ljm4d3qz4xy28a3b70mfr809qqyxsar4220n56q6x27")))

(define-public crate-specfmt-0.2.2 (c (n "specfmt") (v "0.2.2") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "test-generator") (r "^0.3.0") (d #t) (k 2)))) (h "1c5cwbxg2mmfrcxsrh5qfjja332ab5ij014rv890lbrk6ksbl1g2")))

(define-public crate-specfmt-0.2.3 (c (n "specfmt") (v "0.2.3") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "test-generator") (r "^0.3.0") (d #t) (k 2)))) (h "0vjka610hp1455v6s8al75lrq0w2s1dkp6kqqfjqhlp6qjdcbjkj")))

