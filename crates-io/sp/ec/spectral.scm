(define-module (crates-io sp ec spectral) #:use-module (crates-io))

(define-public crate-spectral-0.1.0 (c (n "spectral") (v "0.1.0") (h "14p1f93cla23i1knrzp5r8j3hrrh4hkgpcgym1qb79zc4lbw7p1b")))

(define-public crate-spectral-0.2.0 (c (n "spectral") (v "0.2.0") (h "15l4mw6a30vm57r1f9b96gacvy9rmr9pvrw5jpgjddf8k61lxx94")))

(define-public crate-spectral-0.3.0 (c (n "spectral") (v "0.3.0") (h "1ql9mp517n5ndrw46gs60192z2qbzg96qwmsvj28hm8qjwa7ki2m")))

(define-public crate-spectral-0.4.0 (c (n "spectral") (v "0.4.0") (d (list (d (n "num") (r "^0.1.36") (o #t) (d #t) (k 0)))) (h "1c5x8a2fskbp9ki4rz58f0gkyq89giaymjwvr26p5nnzl6cj4x0p") (f (quote (("default" "num"))))))

(define-public crate-spectral-0.5.0 (c (n "spectral") (v "0.5.0") (d (list (d (n "num") (r "^0.1.36") (o #t) (d #t) (k 0)))) (h "1qya753gd5q1aa8pcajha5jk4p2asqbw7cyx06zmxzl4vz9vjrfs") (f (quote (("default" "num"))))))

(define-public crate-spectral-0.5.1 (c (n "spectral") (v "0.5.1") (d (list (d (n "num") (r "^0.1.36") (o #t) (d #t) (k 0)))) (h "1y1bidakrj5fizn6fdmq1ajl15mly648cid20n4mbh14ir0h9i9w") (f (quote (("default" "num"))))))

(define-public crate-spectral-0.5.2 (c (n "spectral") (v "0.5.2") (d (list (d (n "num") (r "^0.1.36") (o #t) (d #t) (k 0)))) (h "1fpf4gjxa9icia2sw5fb3rkc14fcsls6hmh9qwp4yiva7a3md45d") (f (quote (("default" "num"))))))

(define-public crate-spectral-0.6.0 (c (n "spectral") (v "0.6.0") (d (list (d (n "num") (r "^0.1.36") (o #t) (d #t) (k 0)))) (h "1fn596332gjcjc6dm1lwrqb4fxnjqkpglgmcx8pfa52b3wc1ag5f") (f (quote (("default" "num"))))))

