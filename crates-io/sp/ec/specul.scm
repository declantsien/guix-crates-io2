(define-module (crates-io sp ec specul) #:use-module (crates-io))

(define-public crate-specul-0.1.0 (c (n "specul") (v "0.1.0") (d (list (d (n "err-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("io-util"))) (d #t) (k 0)))) (h "0l6pxmw2vs8s90y089giwc1801am159pwd2y29xblkqnk8nziiw5")))

