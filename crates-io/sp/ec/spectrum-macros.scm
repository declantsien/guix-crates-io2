(define-module (crates-io sp ec spectrum-macros) #:use-module (crates-io))

(define-public crate-spectrum-macros-0.0.0 (c (n "spectrum-macros") (v "0.0.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "rustc_version") (r "^0.3.0") (d #t) (k 1)) (d (n "spectrum") (r "^0.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tt-call") (r "^1.0.7") (d #t) (k 0)))) (h "1mvz330wvpjxp056glq132djjl9ysb4ygjsfnpydwr5rpdqmhxax")))

