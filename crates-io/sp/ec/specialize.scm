(define-module (crates-io sp ec specialize) #:use-module (crates-io))

(define-public crate-specialize-0.0.1 (c (n "specialize") (v "0.0.1") (h "0ha1i1l1dzmdhp0zlbdj7iqbn0b0c2163a3z1jx4im0nb7rz6ijw")))

(define-public crate-specialize-0.0.2 (c (n "specialize") (v "0.0.2") (h "0v66yjj2xlcc7m3rl9nszsisjpkrwqq59i0ldb306q31h977d6zb")))

(define-public crate-specialize-0.0.3 (c (n "specialize") (v "0.0.3") (h "1nrby61c61rqqjracmkkmng731jnlwcfj48mi9vs3832a2ik6lr0")))

