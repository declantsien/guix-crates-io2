(define-module (crates-io sp ec specs_scene_graph) #:use-module (crates-io))

(define-public crate-specs_scene_graph-0.1.0 (c (n "specs_scene_graph") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "hibitset") (r "^0.5") (d #t) (k 0)) (d (n "shred") (r "^0.7") (d #t) (k 0)) (d (n "shrev") (r "^1.0") (d #t) (k 0)) (d (n "specs") (r "^0.12") (d #t) (k 0)) (d (n "specs_bundler") (r "^0.4") (d #t) (k 0)) (d (n "type-name") (r "^0.1") (d #t) (k 0)))) (h "0rzvrbbp5yb9kmdxcsnlwgrapy0hcny1ycwkkmgyp97g5n3kh4ks")))

(define-public crate-specs_scene_graph-0.2.0 (c (n "specs_scene_graph") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "hibitset") (r "^0.5") (d #t) (k 0)) (d (n "shred") (r "^0.7") (d #t) (k 0)) (d (n "shrev") (r "^1.0") (d #t) (k 0)) (d (n "specs") (r "^0.12") (d #t) (k 0)) (d (n "specs_bundler") (r "^0.5") (d #t) (k 0)) (d (n "type-name") (r "^0.1") (d #t) (k 0)))) (h "1q48s9zkhd3w84q6qck0whcigck35iibbv6xd7w9fq30fvbbd64x")))

(define-public crate-specs_scene_graph-0.2.1 (c (n "specs_scene_graph") (v "0.2.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "hibitset") (r "^0.5") (d #t) (k 0)) (d (n "shred") (r "^0.7") (d #t) (k 0)) (d (n "shrev") (r "^1.0") (d #t) (k 0)) (d (n "specs") (r "^0.12") (d #t) (k 0)) (d (n "specs_bundler") (r "^0.5") (d #t) (k 0)) (d (n "type-name") (r "^0.1") (d #t) (k 0)))) (h "0my07sggspbjzb3drnjjlydq05ywda25sn482xi6wcg51x622lfv")))

(define-public crate-specs_scene_graph-0.2.3 (c (n "specs_scene_graph") (v "0.2.3") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "hibitset") (r "^0.5") (d #t) (k 0)) (d (n "shred") (r "^0.7") (d #t) (k 0)) (d (n "shrev") (r "^1.0") (d #t) (k 0)) (d (n "specs") (r "^0.14") (d #t) (k 0)) (d (n "specs_bundler") (r "^0.5") (d #t) (k 0)) (d (n "type-name") (r "^0.1") (d #t) (k 0)))) (h "1mr3bkabvcag9xhzxwy82nhjbfnwhr6gwfifrayh39cam06wbbic")))

(define-public crate-specs_scene_graph-0.3.0 (c (n "specs_scene_graph") (v "0.3.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "hibitset") (r "^0.6") (d #t) (k 0)) (d (n "shred") (r "^0.9") (d #t) (k 0)) (d (n "shrev") (r "^1.1") (d #t) (k 0)) (d (n "specs") (r "^0.15") (d #t) (k 0)) (d (n "specs_bundler") (r "^0.6") (d #t) (k 0)) (d (n "type-name") (r "^0.1") (d #t) (k 0)))) (h "04aj023lyphslrh29860ca6lsr6x7bfnlbkpyxmdpfb18cdzcpk2")))

