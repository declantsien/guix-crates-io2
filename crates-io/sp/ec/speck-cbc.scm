(define-module (crates-io sp ec speck-cbc) #:use-module (crates-io))

(define-public crate-speck-cbc-0.1.0 (c (n "speck-cbc") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.1.0") (f (quote ("i128"))) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)) (d (n "speck") (r "^1.1.0") (d #t) (k 0)))) (h "1f9al4snrrsv7zai6i5xzvl2pxcw7qkqic2qm7w4rrdy3ikzz5vd")))

(define-public crate-speck-cbc-0.1.1 (c (n "speck-cbc") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.1.0") (f (quote ("i128"))) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)) (d (n "speck") (r "^1.1.0") (d #t) (k 0)))) (h "0kzw56xcb6l7gqhaj8hy77j8ym5gi4afa5lxiy9y2l993d2r9cd2")))

(define-public crate-speck-cbc-0.1.2 (c (n "speck-cbc") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.1.0") (f (quote ("i128"))) (k 0)) (d (n "pkcs7") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)) (d (n "speck") (r "^1.1.0") (d #t) (k 0)))) (h "08w2yk3fxhs95jj90cnivilacz32vmpdpxgh5k41f3gpi7adq98f")))

