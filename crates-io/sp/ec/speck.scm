(define-module (crates-io sp ec speck) #:use-module (crates-io))

(define-public crate-speck-0.1.0 (c (n "speck") (v "0.1.0") (h "03cl1nc61f9wak3hm38wmad9di54y6zfi9d6iifs8rjd6i55ga0s")))

(define-public crate-speck-0.1.1 (c (n "speck") (v "0.1.1") (h "1zcigc2mzm0j0ap6g79f1i7laqc014bgmv9pl8din972x07gsmfg")))

(define-public crate-speck-0.2.0 (c (n "speck") (v "0.2.0") (h "0qkzpd4h8cr544n26s1k9sr11xyhm13l1mzklyskl9f1ch8cm25k")))

(define-public crate-speck-1.0.0 (c (n "speck") (v "1.0.0") (h "1lw0y3v3l2q2n713y5582dmmydnv0dv06n6s59qbllr86hm2ci85")))

(define-public crate-speck-1.1.0 (c (n "speck") (v "1.1.0") (h "1s75llcxsb2asvr8idd28f88cl2lyf50bv0vyqpqibdlhw0v3fj0")))

