(define-module (crates-io sp ec spec) #:use-module (crates-io))

(define-public crate-spec-0.1.0 (c (n "spec") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.30") (f (quote ("full"))) (d #t) (k 0)))) (h "0vzjcmhra7wxdjdi67hfnb8w4908kkcwm4sxb0xc8xywsg7rfbwj")))

(define-public crate-spec-0.2.0 (c (n "spec") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^0.15.30") (f (quote ("full"))) (d #t) (k 0)))) (h "0jy3a3mhlr60ijzijajkb7wvjny7ypbznrcr45jbf1p6ahim5mnm")))

