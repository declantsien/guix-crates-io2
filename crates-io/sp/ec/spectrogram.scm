(define-module (crates-io sp ec spectrogram) #:use-module (crates-io))

(define-public crate-spectrogram-0.1.0 (c (n "spectrogram") (v "0.1.0") (d (list (d (n "goertzel") (r "*") (d #t) (k 0)) (d (n "hound") (r "*") (d #t) (k 0)) (d (n "image") (r "*") (d #t) (k 0)))) (h "1v11y2b6dmlw8kcb784zb9486m8sb5ivngb0w47a9w7i470i40h4")))

