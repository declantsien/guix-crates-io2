(define-module (crates-io sp ec spectacle) #:use-module (crates-io))

(define-public crate-spectacle-0.1.0 (c (n "spectacle") (v "0.1.0") (d (list (d (n "im") (r "^14.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "spectacle-impl-tuples") (r "^0.1") (d #t) (k 0)))) (h "0jzjsbym9dhhz1dcsssf6zziiiwls03f9zwdj7vms88srx11f16f") (f (quote (("serde-json" "serde_json") ("default" "collections") ("collections"))))))

(define-public crate-spectacle-0.2.0 (c (n "spectacle") (v "0.2.0") (d (list (d (n "im") (r "^14.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "spectacle-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "spectacle-impl-tuples") (r "^0.2") (d #t) (k 0)))) (h "1k5ndlkjzngh1gw2bhvinj9a0364nn8faq0cxcqngqjj134qmymh") (f (quote (("serde-json" "serde_json") ("derive" "spectacle-derive") ("default" "collections" "derive") ("collections"))))))

