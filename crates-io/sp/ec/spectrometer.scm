(define-module (crates-io sp ec spectrometer) #:use-module (crates-io))

(define-public crate-spectrometer-0.0.1 (c (n "spectrometer") (v "0.0.1") (d (list (d (n "winapi") (r "^0.3.7") (f (quote ("processthreadsapi" "sysinfoapi" "impl-debug" "fileapi" "winbase" "lmcons" "std"))) (d #t) (k 0)))) (h "0v5wv5b2ivaiqvr6ii9hhdc8q5h9q4qlfzk8sywnwsb6ild4xcz2")))

(define-public crate-spectrometer-0.0.2 (c (n "spectrometer") (v "0.0.2") (d (list (d (n "winapi") (r "^0.3.7") (f (quote ("processthreadsapi" "sysinfoapi" "impl-debug" "fileapi" "winbase" "lmcons" "std"))) (d #t) (k 0)))) (h "1a88bz9xdqvdm2zrg65m05xa1hzvkmpm7dz9qsjywqdk4afmci5h")))

(define-public crate-spectrometer-0.0.3 (c (n "spectrometer") (v "0.0.3") (d (list (d (n "winapi") (r "^0.3.7") (f (quote ("processthreadsapi" "sysinfoapi" "impl-debug" "fileapi" "winbase" "lmcons" "std"))) (d #t) (t "cfg(windows)") (k 0)))) (h "18mzqhqar7dm3qvaq0qyi3f22vn0qlva47rrnm1nsg84h05p1mry")))

