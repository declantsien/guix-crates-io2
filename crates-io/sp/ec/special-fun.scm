(define-module (crates-io sp ec special-fun) #:use-module (crates-io))

(define-public crate-special-fun-0.1.2 (c (n "special-fun") (v "0.1.2") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0d6h8rp4vb5qrv4k82xk8vy4i6ykav97fpff6r2x0gngaywjqffq")))

(define-public crate-special-fun-0.1.3 (c (n "special-fun") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0sw7x8hniv044lkz5bwg7w8n38vqzikqqvpzdlc3d1n1wykn1hrm")))

(define-public crate-special-fun-0.1.4 (c (n "special-fun") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 2)))) (h "1x16kqhxsn3r51d264ws5zc04k8chlwdd09a9w7nsmqli1clrjv7")))

(define-public crate-special-fun-0.1.5 (c (n "special-fun") (v "0.1.5") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 2)))) (h "1brx3rnpj8inpncwkfv0wdnsj2hq151dakn9cfk26jkjscwrjhdq")))

(define-public crate-special-fun-0.1.6 (c (n "special-fun") (v "0.1.6") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 2)))) (h "10bb4w11xj77gq08w15sanpmc9qbnjl863ykl1081gv263cl7pjw")))

(define-public crate-special-fun-0.2.0 (c (n "special-fun") (v "0.2.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)))) (h "13kbh48lbz2sdpssqnhivjh99mafxvsng7y0n02p0r0v1pn4xsd1")))

(define-public crate-special-fun-0.3.0 (c (n "special-fun") (v "0.3.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)))) (h "0g3qd2pvy37g6qpwj6cy9ykf2c70153r44hk97m5xsxjp0jbvd4y")))

