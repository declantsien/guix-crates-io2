(define-module (crates-io sp ec specialized-dispatch) #:use-module (crates-io))

(define-public crate-specialized-dispatch-0.1.0 (c (n "specialized-dispatch") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04hqjn5cm9c7wqzpam7qjca4xl3p5fvvw7f1akj3sn4isl5cx3j7")))

(define-public crate-specialized-dispatch-0.1.1 (c (n "specialized-dispatch") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03k5hrk0x4nj1iivz3ydnx9d8gj5saa3dljvpslja1wz21pi9jl7")))

(define-public crate-specialized-dispatch-0.1.2 (c (n "specialized-dispatch") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1kp947k9qnldq42s5lnf92kzb2vkyy2gaj95kzr21v909q8g5sv8")))

(define-public crate-specialized-dispatch-0.2.0 (c (n "specialized-dispatch") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qxsii0lyyd91jhp3aldg5jy4bs684vb8g1yzrb4rz91vmkx916y")))

(define-public crate-specialized-dispatch-0.2.1 (c (n "specialized-dispatch") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1h2szhnlm43c6gkcdyyf9ns6hap2laiv7s44428c4vz7m3npivr7")))

