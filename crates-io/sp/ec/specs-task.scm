(define-module (crates-io sp ec specs-task) #:use-module (crates-io))

(define-public crate-specs-task-0.1.0 (c (n "specs-task") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "specs") (r "^0.15.1") (f (quote ("shred-derive" "specs-derive"))) (k 0)))) (h "0bf8qqhhxs649pbc25cw11ij67x0h67985w7mnlp6p5rrm4dpv8i")))

(define-public crate-specs-task-0.1.1 (c (n "specs-task") (v "0.1.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "specs") (r "^0.15.1") (f (quote ("shred-derive" "specs-derive"))) (k 0)))) (h "1y2xdl05zfl51q2jn3mvcy95589zgrpa8zfd2g85rzi6sfny9nsp")))

(define-public crate-specs-task-0.1.2 (c (n "specs-task") (v "0.1.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "specs") (r "^0.15.1") (f (quote ("shred-derive" "specs-derive"))) (k 0)))) (h "0mrfbli20hck7cxb3fpmmlmz1dlwhf441cpbfk65v2lly1cskyy4")))

(define-public crate-specs-task-0.1.3 (c (n "specs-task") (v "0.1.3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "specs") (r "^0.15.1") (f (quote ("shred-derive" "specs-derive"))) (k 0)))) (h "12hhf6w7zmzil44j5xg7pmfff50ald3kfd30drvl0pvvvyq66zy6")))

(define-public crate-specs-task-0.1.4 (c (n "specs-task") (v "0.1.4") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "specs") (r "^0.15.1") (f (quote ("shred-derive" "specs-derive"))) (k 0)))) (h "0k418p2158ba603z879c0bqzmaf2kmfkxfdprw6d1vc5lsy8sbnp")))

(define-public crate-specs-task-0.1.5 (c (n "specs-task") (v "0.1.5") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "specs") (r "^0.15.1") (f (quote ("shred-derive" "specs-derive"))) (k 0)))) (h "0fgnz7sgki0svvjaap94n98h2abbh21rln0jcaibgkxkrb1kqcjk")))

(define-public crate-specs-task-0.1.6 (c (n "specs-task") (v "0.1.6") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "specs") (r "^0.15.1") (f (quote ("shred-derive" "specs-derive"))) (k 0)))) (h "0niizjddqvayxg6f9hmfqi509z0bb3fdgzd9gh0fl8g5hz0k006p")))

(define-public crate-specs-task-0.1.7 (c (n "specs-task") (v "0.1.7") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "specs") (r "^0.15.1") (f (quote ("shred-derive" "specs-derive"))) (k 0)))) (h "14lb5rc39wr9gnyhi9qhp71rc6xmvbcf1b4h0qqgy88s3cwwimsi")))

(define-public crate-specs-task-0.1.8 (c (n "specs-task") (v "0.1.8") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "specs") (r "^0.15.1") (f (quote ("shred-derive" "specs-derive"))) (k 0)))) (h "19ngchr0n6vcc84cm2x0fs3hgc6vjw2mhk289h32rl9l4vz1bhd9")))

(define-public crate-specs-task-0.2.0 (c (n "specs-task") (v "0.2.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "specs") (r "^0.16.0") (f (quote ("shred-derive" "specs-derive"))) (k 0)))) (h "1wrism44qkgmzf8vp6vnjiy27j3v53imhqbym2ncdnllh8pnilvg")))

(define-public crate-specs-task-0.2.1 (c (n "specs-task") (v "0.2.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "specs") (r "^0.16.0") (f (quote ("shred-derive" "specs-derive"))) (k 0)))) (h "0p6902z8bk9q0d114n2qavqhp7xzv7vr22ac69hpvs6r2jx306ps")))

(define-public crate-specs-task-0.3.0 (c (n "specs-task") (v "0.3.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "specs") (r "^0.16.0") (f (quote ("shred-derive" "specs-derive"))) (k 0)))) (h "06prnpr7fmav4rsxx3mgvavszyl7imibc1nnwg1jwhn6ykn7q180")))

(define-public crate-specs-task-0.4.0 (c (n "specs-task") (v "0.4.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "specs") (r "^0.16.0") (f (quote ("shred-derive" "specs-derive"))) (k 0)))) (h "1jl05mbfyvnxwrc27cm1w5242bda617myrshw1x4vqzhjj09rl1g")))

