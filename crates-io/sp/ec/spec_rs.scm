(define-module (crates-io sp ec spec_rs) #:use-module (crates-io))

(define-public crate-spec_rs-0.1.0 (c (n "spec_rs") (v "0.1.0") (d (list (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1zq4yf4g97czbrlbr2sq19jlh52hlpiiiiq5fry000zsnfz052b8") (y #t)))

(define-public crate-spec_rs-0.2.0 (c (n "spec_rs") (v "0.2.0") (d (list (d (n "actix-http") (r "=3.0.0-beta.15") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "003mfawa3w1yy07w4m48fa35nprcyr4hp9mwd71ycg3hifd2580z") (y #t)))

