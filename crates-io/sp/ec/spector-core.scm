(define-module (crates-io sp ec spector-core) #:use-module (crates-io))

(define-public crate-spector-core-0.0.1 (c (n "spector-core") (v "0.0.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0dm242b4ki6pjwmw7mgqxj8ivg47rk6c9a7vi5yz6g1a20ykyd6y")))

