(define-module (crates-io sp ec speck-rs) #:use-module (crates-io))

(define-public crate-speck-rs-0.1.0 (c (n "speck-rs") (v "0.1.0") (h "0i08n9rk8xaxzlfdw0qp2k1lh3scgq32r6011wpjsr4rfz5slc2n")))

(define-public crate-speck-rs-0.1.1 (c (n "speck-rs") (v "0.1.1") (h "03bs3zvshgfk7xvvb246llzkfswgwvghg4fjc0fw5ym1aj50hjgd")))

