(define-module (crates-io sp ec spectacle-derive) #:use-module (crates-io))

(define-public crate-spectacle-derive-0.1.0 (c (n "spectacle-derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "0w5y17hw7cq1kx7iazb4qm51fdrh4j4dd31l63pv67dz81z99ihc")))

