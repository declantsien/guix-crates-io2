(define-module (crates-io sp ec special) #:use-module (crates-io))

(define-public crate-special-0.0.5 (c (n "special") (v "0.0.5") (d (list (d (n "assert") (r "^0.0.4") (d #t) (k 2)))) (h "0rlkivvzi30qgk6slc9549hbgxv6g5wqy4rxd7k2hbad2xi6va5i")))

(define-public crate-special-0.1.0 (c (n "special") (v "0.1.0") (d (list (d (n "assert") (r "^0.0.4") (d #t) (k 2)))) (h "0qfhisk4b50l3f42gl98m4s4bjlw2k10l13h1q3gp7p2l5zyv2g9")))

(define-public crate-special-0.1.1 (c (n "special") (v "0.1.1") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)))) (h "05kz6d247andp9lhj8bq6h0dxq50grlgs05m1wskh4byjmm1ln7j")))

(define-public crate-special-0.1.2 (c (n "special") (v "0.1.2") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)))) (h "1n72a4xx4fmb1dxawq32icg4bqvj5h4y9l280vv4a244rdsavy38")))

(define-public crate-special-0.1.3 (c (n "special") (v "0.1.3") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)))) (h "14im11r53mvi640s67mh72nzjpxzk4syxqqm11233imnd33hxg83")))

(define-public crate-special-0.1.4 (c (n "special") (v "0.1.4") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)))) (h "0cl2qnf41p2ah9cpmz4ybg5m794zhssxrvjl3h9pdi7005lws6nf")))

(define-public crate-special-0.1.6 (c (n "special") (v "0.1.6") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "rand") (r "^0.1") (d #t) (k 2)))) (h "1b1g1idibkb90zwn0q4jzv9j740qwi6809d1cr3m98ypxyafnx0b")))

(define-public crate-special-0.1.7 (c (n "special") (v "0.1.7") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "rand") (r "^0.2") (d #t) (k 2)))) (h "0h4wk1bj86f34xkpngvv1pv01inw0cy8zlzz0lpqyn8q5njmhs6p")))

(define-public crate-special-0.1.8 (c (n "special") (v "0.1.8") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "0lal7fmqxx20m96zlnlrcfjivylllwndjvv6rndscl8z3bm72g33")))

(define-public crate-special-0.1.9 (c (n "special") (v "0.1.9") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1ai2l3jgyj3b6v7a9fxjd6gnm3b3xa3cbqhjrk92hp0779k49493")))

(define-public crate-special-0.2.0 (c (n "special") (v "0.2.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1kvlvb62mrfdap7lhp88v3pc4bf7c7q0r0n0gjbz0j52nn0q933p")))

(define-public crate-special-0.3.0 (c (n "special") (v "0.3.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "1kwrc57caavff9svb985rxzsgy94fcqyf6gg3nr8xmghzp7bg917")))

(define-public crate-special-0.3.1 (c (n "special") (v "0.3.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "0zssdvkqryhhawhhhwmjzdj6qdfyl2lc8c8s0xjrq1rbgwykhmpb")))

(define-public crate-special-0.3.2 (c (n "special") (v "0.3.2") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)))) (h "0pj2790bz9r9lmgacxf1jmpwb4lma1z1sqx8184y8xszjq2bw176")))

(define-public crate-special-0.4.0 (c (n "special") (v "0.4.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)))) (h "1y139z5fwz2qilyz36931caqv4jdjdmrvf0w649gwwwml3cq4hip")))

(define-public crate-special-0.4.1 (c (n "special") (v "0.4.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)))) (h "1x8mc45a2z8586y8bc8m3p00649dx3f11c7bzlq0bsx5k0lfjzzj")))

(define-public crate-special-0.4.2 (c (n "special") (v "0.4.2") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)))) (h "1789nnl6d181rk5ws5bzsa3yny7pxvlv7dak2singb537s5fn9k5")))

(define-public crate-special-0.5.0 (c (n "special") (v "0.5.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "random") (r "*") (d #t) (k 2)))) (h "1nyr0v5k2kp96y1kbp5j3f8rvzyia3rja2rz26kjdi76lmraf2zq")))

(define-public crate-special-0.5.1 (c (n "special") (v "0.5.1") (d (list (d (n "assert") (r "^0.6") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "random") (r "^0.9") (d #t) (k 2)))) (h "01nkndkmpp25561508dxwmgi5m9q958daypzl3d9x9mlm6i32k4g")))

(define-public crate-special-0.5.2 (c (n "special") (v "0.5.2") (d (list (d (n "assert") (r "^0.6") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "random") (r "^0.9") (d #t) (k 2)))) (h "1f6rqsmcgxdidsrcp24j0rv930mpszsxdw2vmhjc4387gvi3yxm3")))

(define-public crate-special-0.5.3 (c (n "special") (v "0.5.3") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "random") (r "^0.10") (d #t) (k 2)))) (h "11hk8kgvsp4j3k98hlfgm6hzdnis29i7j9psvaai2ygmn0j6bhrk")))

(define-public crate-special-0.6.0 (c (n "special") (v "0.6.0") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "random") (r "^0.10") (d #t) (k 2)))) (h "1px5b9cpzq13rqr067h973d1vd9883q3zmpfwf7adly8hf4sa1wr")))

(define-public crate-special-0.7.0 (c (n "special") (v "0.7.0") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "random") (r "^0.10") (d #t) (k 2)))) (h "1ma31rhggfmavw0411hdb758z05vh1qz5pqjdzxs8lmvn0ljdcg5")))

(define-public crate-special-0.7.1 (c (n "special") (v "0.7.1") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "random") (r "^0.11") (d #t) (k 2)))) (h "0yl8b2jjv3mjhhxygv5y4wz6bq77a6gc2hzjir08ks21brcgrvra")))

(define-public crate-special-0.7.2 (c (n "special") (v "0.7.2") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "09i8s5ffbrz02zm5kqiw0mka08jmlj7i04ky548zqyhlq6zc370i")))

(define-public crate-special-0.7.3 (c (n "special") (v "0.7.3") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "1ax5763y30jzq587nfrx4hcll8hr58mhv5bi163iqzmkqkgsgsi1")))

(define-public crate-special-0.7.4 (c (n "special") (v "0.7.4") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "0xh72h1w86gxmaz60q14x5xxbvb6gjsl00dy7kmg0cqm2y1s7cqk")))

(define-public crate-special-0.7.5 (c (n "special") (v "0.7.5") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "0x2acp137lrazg9fajay8bkgk3asfm76sx7x0m8svpjx4y1bfi2y")))

(define-public crate-special-0.7.6 (c (n "special") (v "0.7.6") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "0g9d5vvkagng5yrsjdfn0kl3282dd7i402x4h2v7siphiwn73jsi")))

(define-public crate-special-0.7.7 (c (n "special") (v "0.7.7") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "06rhkw6fipcv6jssc9qr00xxc5x60ak05frxzlcnm27msa515gh0")))

(define-public crate-special-0.7.8 (c (n "special") (v "0.7.8") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "0r41dasnq6ngdpzhgzzqkqkhvfrm3raq8dbpc93crmng9bms8w29")))

(define-public crate-special-0.8.0 (c (n "special") (v "0.8.0") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "0hvnkarbi608mnmf2p7kcxzj9l2bmxc0jhkbpfm1f9dsgir9bsrg")))

(define-public crate-special-0.8.1 (c (n "special") (v "0.8.1") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "0dknpky1bbl7xp4qwp3vjqjarfl846mk6irs2z7mvdsr843mx9i4")))

(define-public crate-special-0.9.0 (c (n "special") (v "0.9.0") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "0id6l099lf70hdhy8qyaid99khidyx5rqw3w5zqba27yspjia0gp")))

(define-public crate-special-0.10.0 (c (n "special") (v "0.10.0") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "1jjhrqp0vqv3nfp8ya9pajski088ysm62d2cjfmpyag39dc30gin") (y #t)))

(define-public crate-special-0.10.1 (c (n "special") (v "0.10.1") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "1fzmqk1i5jspwkq5pb4fwlh2klxr7vqpq9n0nf2mdrky979s6mnj")))

(define-public crate-special-0.10.2 (c (n "special") (v "0.10.2") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "0nlc47zwmnnrcvvsq6fzji6qa5g1phvjlfxmnjmkrpb3l11x0bij")))

(define-public crate-special-0.10.3 (c (n "special") (v "0.10.3") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)))) (h "10h3fq4mzjgxjmi3bpax3nzslhas86nbyl3k17cgsfg63bbz175q")))

