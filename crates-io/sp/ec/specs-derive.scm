(define-module (crates-io sp ec specs-derive) #:use-module (crates-io))

(define-public crate-specs-derive-0.1.0 (c (n "specs-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0yi0b0lwkxmvblqk0zaqrh30g1q6whsnyy0lgprqqd7mfl484rv8")))

(define-public crate-specs-derive-0.2.0 (c (n "specs-derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0s1g1fchkvs26dj60cy3j5rpy0l54h0w9j4gnwvky8bihp2bb0zh")))

(define-public crate-specs-derive-0.3.0 (c (n "specs-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0rp4s0vjniw4f78y6m0505q6jblm0cjbw650a0cnv4jws12cm59b")))

(define-public crate-specs-derive-0.4.0 (c (n "specs-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1rb9nb03av2gyyp9i13gmnl9y32xy8lc6d36kvlpcvsa1px4jdd6")))

(define-public crate-specs-derive-0.4.1 (c (n "specs-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)))) (h "049m23figxid6vmd3h28m5fqs5fk36gcs8j2xh7iklpkc29y08ry")))

