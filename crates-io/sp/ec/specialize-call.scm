(define-module (crates-io sp ec specialize-call) #:use-module (crates-io))

(define-public crate-specialize-call-0.1.0 (c (n "specialize-call") (v "0.1.0") (h "1f1xp87lyxwfrn71s6xipvzqrhk0230ya18i1kmwmb33x9vv43c3")))

(define-public crate-specialize-call-0.1.1 (c (n "specialize-call") (v "0.1.1") (h "1011l2nxqf2m4hm6a8wfmv817s89gm6vvg80wh5d5hs7m3d920zh")))

(define-public crate-specialize-call-0.1.2 (c (n "specialize-call") (v "0.1.2") (h "02f42gx6nk99fy76hvwvng8n6pymzj97jaz5wc4iabmwishsm3d6")))

