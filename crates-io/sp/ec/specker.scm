(define-module (crates-io sp ec specker) #:use-module (crates-io))

(define-public crate-specker-0.1.0 (c (n "specker") (v "0.1.0") (d (list (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "0hv99njbq7zjfd2glqayfxacl1vfxr9z0c4fkzzcqpn5ysncv7yl")))

(define-public crate-specker-0.1.1 (c (n "specker") (v "0.1.1") (d (list (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "18jsin9a0v0pwb4pqhip5pff8dvk9wcrd23bxvk8xvka0496srp1")))

(define-public crate-specker-0.1.2 (c (n "specker") (v "0.1.2") (d (list (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "0gyw1ymww3ygq78cgm4bmihfg1b03gnw8hrbfbdx6c554gdcd0vk")))

(define-public crate-specker-0.2.0 (c (n "specker") (v "0.2.0") (d (list (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "0ljgwl5d77y78khrrzj38321i4vy71alchzpjkbl8r3kiijvma9z")))

(define-public crate-specker-0.3.0 (c (n "specker") (v "0.3.0") (d (list (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "07y1vxw0xxkcd4w96a8racr3p13i252sdb68hi5x2c2gy3c4w6gl")))

(define-public crate-specker-0.3.1 (c (n "specker") (v "0.3.1") (d (list (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "1pdk7140rfkwvnkwvdhgss59c9lqlbvhqmb6qwink15wrsqax4wm")))

(define-public crate-specker-0.3.2 (c (n "specker") (v "0.3.2") (d (list (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "0wshdn6v2dm6623vjdicplxc1sw88nmwskv2yvsa1c5kmh6sl85z")))

(define-public crate-specker-0.3.3 (c (n "specker") (v "0.3.3") (d (list (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "0mfddq2vn93l3l3a8yzizbp37xhdxiqm0f8b5hql7r7gdkmzsdwy")))

(define-public crate-specker-0.3.4 (c (n "specker") (v "0.3.4") (d (list (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "05n85n9nbqmzwrwvanwymh5glllxppqiq5pi19s0c2psg7mhz9lv")))

(define-public crate-specker-0.3.5 (c (n "specker") (v "0.3.5") (d (list (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1kbk8r7a6yb2229ly8g85sms72jskirl4ywawn0826izr5ndxk1l")))

