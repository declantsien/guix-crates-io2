(define-module (crates-io sp ec specs_engine) #:use-module (crates-io))

(define-public crate-specs_engine-0.0.1 (c (n "specs_engine") (v "0.0.1") (d (list (d (n "specs") (r "^0.7.0") (d #t) (k 0)))) (h "1id1xj9hr93knvf1h0gbjhsz8mnn33nny30xz5pwapxhj2dfc5is") (y #t)))

(define-public crate-specs_engine-0.0.2 (c (n "specs_engine") (v "0.0.2") (d (list (d (n "specs") (r "^0.7.0") (d #t) (k 0)))) (h "0x8vffng8hdnqisrlda6bi8zhfr61blmw4w55plnrljiqp1p3cb8") (y #t)))

(define-public crate-specs_engine-0.0.3 (c (n "specs_engine") (v "0.0.3") (d (list (d (n "slog") (r "^1.3.2") (d #t) (k 0)) (d (n "slog-term") (r "^1.3.3") (d #t) (k 0)) (d (n "specs") (r "^0.7.0") (d #t) (k 0)))) (h "1r0n0yba2v7cjn60zx10d0bbb7h45aphdq85wlj579zswfvs66l6") (y #t)))

