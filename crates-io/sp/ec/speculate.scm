(define-module (crates-io sp ec speculate) #:use-module (crates-io))

(define-public crate-speculate-0.0.1 (c (n "speculate") (v "0.0.1") (h "13higb6b5rgp4illyhjbba6d0ymplav26p8hyndk3dkxf7srb657")))

(define-public crate-speculate-0.0.2 (c (n "speculate") (v "0.0.2") (h "0hz2p9pjh184bxcajprvg4xsarzgb2vh8h56fhpzvg3av540nl5p")))

(define-public crate-speculate-0.0.3 (c (n "speculate") (v "0.0.3") (h "1xyzyrr1k267b05avp677b59w8ypbirzvsr9p5pyiap9d56vahfw")))

(define-public crate-speculate-0.0.4 (c (n "speculate") (v "0.0.4") (h "148diii5wys5iky2vf8070n0zfr09lcs5xm20w90x50b4iqp4xh2")))

(define-public crate-speculate-0.0.5 (c (n "speculate") (v "0.0.5") (h "1lbn7sif09v96lmybm876h0q1xsm5xsn6xr66m7cjyf6km1d7r0y")))

(define-public crate-speculate-0.0.6 (c (n "speculate") (v "0.0.6") (h "0ha44nyzv5a20v0g3sj2mn5955dcs87jnygdkk1406h9c9kysa2g")))

(define-public crate-speculate-0.0.7 (c (n "speculate") (v "0.0.7") (h "1ppdvr5hlcz553apfkajicq3267fqzd7f36q0d36bfq0w2ardkqd")))

(define-public crate-speculate-0.0.8 (c (n "speculate") (v "0.0.8") (h "0nz086s4y3f4ijhs2q636vp5wznijlrfmxyhy5v0487bp1a44rbg")))

(define-public crate-speculate-0.0.9 (c (n "speculate") (v "0.0.9") (h "0sv7927wcgpnc1nxcfc4makmxjw8cy0i678jc7x9zvkkqzbfcmz6")))

(define-public crate-speculate-0.0.10 (c (n "speculate") (v "0.0.10") (h "0m9ckqfj67pvw4lkqfdvp7m8khv261sgva4dnav7vxkqn2r9250j")))

(define-public crate-speculate-0.0.11 (c (n "speculate") (v "0.0.11") (h "01m3dbmzydlx1s8qy3m36hk0lqhplrqnp27c9rm1m6bncxb3vbfr")))

(define-public crate-speculate-0.0.12 (c (n "speculate") (v "0.0.12") (h "1d6jv3h4bfmfh92dhj97f2xjwh0cskff81wxsah9mpkd68xnd8lb")))

(define-public crate-speculate-0.0.13 (c (n "speculate") (v "0.0.13") (h "0li3g4vqan1rnp8av98xiazgj1mkbpvkm3ychy3kzssar6gxhp5y")))

(define-public crate-speculate-0.0.14 (c (n "speculate") (v "0.0.14") (h "0m6ry6l6720gz1vf9a2sv9j1zh0swx8fhhnad5mh0rwlmnafy1yl")))

(define-public crate-speculate-0.0.15 (c (n "speculate") (v "0.0.15") (h "1r46br90chzsdl5scwa1cw90s68sa2lmh6ha1g9r4g62zh7n20sj")))

(define-public crate-speculate-0.0.16 (c (n "speculate") (v "0.0.16") (h "07s1nykvp2q0av8ppkrhdyym1drsybp3y563fs1adxcnhblpqysi")))

(define-public crate-speculate-0.0.17 (c (n "speculate") (v "0.0.17") (h "1wq39cxs4jimk69fzsxyhhjhry7zik4c3m6h9knnji0rqfl9pi8x")))

(define-public crate-speculate-0.0.18 (c (n "speculate") (v "0.0.18") (h "1yafaw715wg6fsfmw2w2n1vqnkxbmls94h427fg3klanq7c6jggk")))

(define-public crate-speculate-0.0.19 (c (n "speculate") (v "0.0.19") (h "0vib8h4k9jphdr06zgb8yfdcp7wk7sxkj36sa7h1l19s4lkxd23g")))

(define-public crate-speculate-0.0.20 (c (n "speculate") (v "0.0.20") (h "0fm5bgc4bcm8lzgp4qq0d0szxr7nrh2ay7jm0g8kwkikrygjykf9")))

(define-public crate-speculate-0.0.21 (c (n "speculate") (v "0.0.21") (h "1brvhilwy8sfn6350zspza7i0sxs336d1a798karfd69jx10lxsf")))

(define-public crate-speculate-0.0.22 (c (n "speculate") (v "0.0.22") (h "1gmzy6k6yfffn88sxcphg1spsj2im7mxvwykx9gnkxdazj7ijs3h")))

(define-public crate-speculate-0.0.23 (c (n "speculate") (v "0.0.23") (h "1cddxwch3lqsqz7vk7p5lj9d0nhvij9lhpx57f1pvpsylpm852y3")))

(define-public crate-speculate-0.0.24 (c (n "speculate") (v "0.0.24") (h "1x770vnzigmg3f54zr2qjzl5d4shzy8wbxviywwyr67zv7qfsba5")))

(define-public crate-speculate-0.0.25 (c (n "speculate") (v "0.0.25") (h "01064m8kd753c987snzbmb6bjsraiqphxyg0yvkiafcsgr53p3ix")))

(define-public crate-speculate-0.0.26 (c (n "speculate") (v "0.0.26") (h "1z3d6nbih22ihr28bl7lhwp9g5qzr2i8x5dab5zl2lrchh25wxay")))

(define-public crate-speculate-0.0.27 (c (n "speculate") (v "0.0.27") (h "0qcj5bxw2kk7p7y05jhpnn6h627cxp78nxfpyjqyza4v4kdgyxv2")))

(define-public crate-speculate-0.1.0 (c (n "speculate") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "1v5knaklplzqz1niv9bwm1gr0zkg28pjyq42q1f8mjxin77f9xwl")))

(define-public crate-speculate-0.1.1 (c (n "speculate") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "0amnsrza5mc5d2c44mh02fail661sx4d1c0lw35nx0b97hx63lms") (f (quote (("nightly") ("default"))))))

(define-public crate-speculate-0.1.2 (c (n "speculate") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "unicode-xid") (r "^0.1") (d #t) (k 0)))) (h "0ph01n3fqkmnfr1wd13dqsi4znv06xy6p4h3hqqdzk81r0r5vd1w") (f (quote (("nightly") ("default"))))))

