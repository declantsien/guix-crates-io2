(define-module (crates-io sp ec spectrum) #:use-module (crates-io))

(define-public crate-spectrum-0.0.0 (c (n "spectrum") (v "0.0.0") (d (list (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "derive-new") (r "^0.5.8") (d #t) (k 0)) (d (n "format") (r "^0.2.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "pretty") (r "^0.10.0") (d #t) (k 0)))) (h "17lai4bgld4dc5pw6kidk0xmsl38ixmsal9glhq4ackmldi009xm")))

