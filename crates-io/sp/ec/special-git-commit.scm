(define-module (crates-io sp ec special-git-commit) #:use-module (crates-io))

(define-public crate-special-git-commit-0.1.0 (c (n "special-git-commit") (v "0.1.0") (d (list (d (n "git2") (r "^0.18.3") (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)))) (h "149nhh7g5132adjs489i8xvg6b6mp4hwb3jl46dgmlkx5d9d120q")))

(define-public crate-special-git-commit-0.1.1 (c (n "special-git-commit") (v "0.1.1") (d (list (d (n "git2") (r "^0.18.3") (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)))) (h "0fc69nb10fvykd7a3knaxcgq0ha380x8ybzkffffbacj26mjcx1z")))

(define-public crate-special-git-commit-0.1.2 (c (n "special-git-commit") (v "0.1.2") (d (list (d (n "git2") (r "^0.18.3") (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)))) (h "1qd1lpdmwhcyl9vr2l26b8w901s53ba5hs29466syy6wfxjfha4h")))

