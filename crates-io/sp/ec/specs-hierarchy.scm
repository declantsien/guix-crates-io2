(define-module (crates-io sp ec specs-hierarchy) #:use-module (crates-io))

(define-public crate-specs-hierarchy-0.1.0-alpha1 (c (n "specs-hierarchy") (v "0.1.0-alpha1") (d (list (d (n "hibitset") (r "^0.5") (d #t) (k 0)) (d (n "shred") (r "^0.7.0-alpha2") (d #t) (k 0)) (d (n "shred-derive") (r "^0.5.0-alpha1") (d #t) (k 0)) (d (n "shrev") (r "^0.8") (d #t) (k 0)) (d (n "specs") (r "^0.11.0-alpha1") (d #t) (k 0)))) (h "0cxjihgkpmi3kpslxr60h6p8ybjs5184aq5rwcdjjrwkrw0vpb2c")))

(define-public crate-specs-hierarchy-0.1.0-alpha2 (c (n "specs-hierarchy") (v "0.1.0-alpha2") (d (list (d (n "hibitset") (r "^0.5") (d #t) (k 0)) (d (n "shred") (r "^0.7.0-alpha3") (d #t) (k 0)) (d (n "shred-derive") (r "^0.5.0-alpha1") (d #t) (k 0)) (d (n "shrev") (r "^0.8") (d #t) (k 0)) (d (n "specs") (r "^0.11.0-alpha1") (d #t) (k 0)))) (h "13i98prq0l7z0d0znbl184sndfyij8bw0iimk3r4wwa2yk18qc33")))

(define-public crate-specs-hierarchy-0.1.0-alpha3 (c (n "specs-hierarchy") (v "0.1.0-alpha3") (d (list (d (n "hibitset") (r "^0.5") (d #t) (k 0)) (d (n "shred") (r "^0.7.0-alpha4") (d #t) (k 0)) (d (n "shred-derive") (r "^0.5.0-alpha1") (d #t) (k 0)) (d (n "shrev") (r "^0.8") (d #t) (k 0)) (d (n "specs") (r "^0.11.0-alpha2") (d #t) (k 0)))) (h "15y7xlv0v86aqg9k378ai7wn65y1hpq5q5bd07imvvbjbmr37r0d")))

(define-public crate-specs-hierarchy-0.1.0-alpha4 (c (n "specs-hierarchy") (v "0.1.0-alpha4") (d (list (d (n "hibitset") (r "^0.5") (d #t) (k 0)) (d (n "shred") (r "^0.7.0-alpha5") (d #t) (k 0)) (d (n "shred-derive") (r "^0.5.0-alpha1") (d #t) (k 0)) (d (n "shrev") (r "^0.8") (d #t) (k 0)) (d (n "specs") (r "^0.11.0-alpha4") (d #t) (k 0)))) (h "09qldny1zci8l7cby23qa49n9r0raw4xqpfnpcqdyfzgrzx1fyv0")))

(define-public crate-specs-hierarchy-0.1.0-alpha5 (c (n "specs-hierarchy") (v "0.1.0-alpha5") (d (list (d (n "hibitset") (r "^0.5") (d #t) (k 0)) (d (n "shred") (r "^0.7.0-alpha5") (d #t) (k 0)) (d (n "shred-derive") (r "^0.5.0-alpha1") (d #t) (k 0)) (d (n "shrev") (r "^0.8") (d #t) (k 0)) (d (n "specs") (r "^0.11.0-alpha5") (d #t) (k 0)))) (h "1m44636xkpy9jrf316pxycmgkdx2j2fw936cw4ayh9fdp3dpxlv8")))

(define-public crate-specs-hierarchy-0.1.0-alpha6 (c (n "specs-hierarchy") (v "0.1.0-alpha6") (d (list (d (n "hibitset") (r "^0.5") (d #t) (k 0)) (d (n "shred") (r "^0.7.0-alpha5") (d #t) (k 0)) (d (n "shred-derive") (r "^0.5.0-alpha1") (d #t) (k 0)) (d (n "shrev") (r "^1.0.0-alpha1") (d #t) (k 0)) (d (n "specs") (r "^0.11.0-alpha6") (d #t) (k 0)))) (h "1kzi95gcq8bal4dj5cjkjdxcxnxaz94xqls9ajzhz744v2052ybm")))

(define-public crate-specs-hierarchy-0.1.0-alpha8 (c (n "specs-hierarchy") (v "0.1.0-alpha8") (d (list (d (n "hibitset") (r "^0.5") (d #t) (k 0)) (d (n "shred") (r "^0.7.0-alpha6") (d #t) (k 0)) (d (n "shred-derive") (r "^0.5.0-alpha2") (d #t) (k 0)) (d (n "shrev") (r "^1.0.0") (d #t) (k 0)) (d (n "specs") (r "^0.11.0-alpha7") (d #t) (k 0)))) (h "1s56fmz83w0jjxr2y0qfx26qz0w07q4z9f0ny98c5np2wxxaqs2v")))

(define-public crate-specs-hierarchy-0.1.0 (c (n "specs-hierarchy") (v "0.1.0") (d (list (d (n "hibitset") (r "^0.5") (d #t) (k 0)) (d (n "shred") (r "^0.7") (d #t) (k 0)) (d (n "shred-derive") (r "^0.5") (d #t) (k 0)) (d (n "shrev") (r "^1.0") (d #t) (k 0)) (d (n "specs") (r "^0.11") (d #t) (k 0)))) (h "08n967s13ra22cqfd5xh94gz7bvl0y1b2b5hvki79gh31jll2a2b")))

(define-public crate-specs-hierarchy-0.2.0 (c (n "specs-hierarchy") (v "0.2.0") (d (list (d (n "hibitset") (r "^0.5") (d #t) (k 0)) (d (n "shred") (r "^0.7") (d #t) (k 0)) (d (n "shred-derive") (r "^0.5") (d #t) (k 0)) (d (n "shrev") (r "^1.0") (d #t) (k 0)) (d (n "specs") (r "^0.12") (d #t) (k 0)))) (h "1ac7n0g1qp53mxb41wdf8sjr74n345fnnkgvj5h3n0yyklkrp03l")))

(define-public crate-specs-hierarchy-0.2.1 (c (n "specs-hierarchy") (v "0.2.1") (d (list (d (n "hibitset") (r "^0.5") (d #t) (k 0)) (d (n "shred") (r "^0.7") (d #t) (k 0)) (d (n "shred-derive") (r "^0.5") (d #t) (k 0)) (d (n "shrev") (r "^1.0") (d #t) (k 0)) (d (n "specs") (r "^0.12") (d #t) (k 0)))) (h "0kkdjxn3ydqkzhhz74yilnm8zsj2rj8vjisyr42kga9yqglkmsc9")))

(define-public crate-specs-hierarchy-0.3.0 (c (n "specs-hierarchy") (v "0.3.0") (d (list (d (n "hibitset") (r "^0.5") (d #t) (k 0)) (d (n "shred") (r "^0.7") (d #t) (k 0)) (d (n "shred-derive") (r "^0.5") (d #t) (k 0)) (d (n "shrev") (r "^1.0") (d #t) (k 0)) (d (n "specs") (r "^0.14") (d #t) (k 0)))) (h "1mp722rj7qq5qpz5b83bj4kizig3ag1sg8jc3322q53dxbr09pg2")))

(define-public crate-specs-hierarchy-0.3.1 (c (n "specs-hierarchy") (v "0.3.1") (d (list (d (n "hibitset") (r "^0.5") (k 0)) (d (n "shred") (r "^0.7") (k 0)) (d (n "shred-derive") (r "^0.5") (d #t) (k 0)) (d (n "shrev") (r "^1.0") (d #t) (k 0)) (d (n "specs") (r "^0.14") (k 0)))) (h "0l32j4ka25q193ry5h62v19i3ba3122wpc4ss4y7b16fyi8k67cc") (f (quote (("parallel" "specs/parallel" "shred/parallel" "hibitset/parallel") ("nightly" "specs/nightly" "shred/nightly") ("default" "parallel"))))))

(define-public crate-specs-hierarchy-0.4.0 (c (n "specs-hierarchy") (v "0.4.0") (d (list (d (n "hibitset") (r "^0.6") (k 0)) (d (n "shred") (r "^0.9") (k 0)) (d (n "shred-derive") (r "^0.6") (d #t) (k 0)) (d (n "shrev") (r "^1.1") (d #t) (k 0)) (d (n "specs") (r "^0.15") (k 0)))) (h "0hajw2h10vldckfmkd018rdrrla6vmd8ap9g5w32lmqdx9pi0lnf") (f (quote (("parallel" "specs/parallel" "shred/parallel" "hibitset/parallel") ("nightly" "specs/nightly" "shred/nightly") ("default" "parallel"))))))

(define-public crate-specs-hierarchy-0.5.0 (c (n "specs-hierarchy") (v "0.5.0") (d (list (d (n "hibitset") (r "^0.6") (k 0)) (d (n "shred") (r "^0.9") (k 0)) (d (n "shred-derive") (r "^0.6") (d #t) (k 0)) (d (n "shrev") (r "^1.1") (d #t) (k 0)) (d (n "specs") (r "^0.15") (k 0)))) (h "10idg1gpcp57vpq28snpn4c5zb1pvr7qvs77ix2l6q3l0rp301ns") (f (quote (("parallel" "specs/parallel" "shred/parallel" "hibitset/parallel") ("nightly" "specs/nightly" "shred/nightly") ("default" "parallel"))))))

(define-public crate-specs-hierarchy-0.5.1 (c (n "specs-hierarchy") (v "0.5.1") (d (list (d (n "hibitset") (r "^0.6") (k 0)) (d (n "shred") (r "^0.9") (k 0)) (d (n "shred-derive") (r "^0.6") (d #t) (k 0)) (d (n "shrev") (r "^1.1") (d #t) (k 0)) (d (n "specs") (r "^0.15") (k 0)))) (h "1qsmjbxsg53lbbrkw082h36r1wvh9jda1aywk88ray8lqfjrjccw") (f (quote (("parallel" "specs/parallel" "shred/parallel" "hibitset/parallel") ("nightly" "specs/nightly" "shred/nightly") ("default" "parallel"))))))

(define-public crate-specs-hierarchy-0.6.0 (c (n "specs-hierarchy") (v "0.6.0") (d (list (d (n "hibitset") (r "^0.6.2") (k 0)) (d (n "shrev") (r "^1.1.1") (d #t) (k 0)) (d (n "specs") (r "^0.16.0") (f (quote ("shred-derive"))) (k 0)))) (h "0hpbhk9piq6pcizyzcm8rzbjvvs9j2qm9010hls13k8jrca2p4wc") (f (quote (("parallel" "specs/parallel" "hibitset/parallel") ("default" "parallel"))))))

