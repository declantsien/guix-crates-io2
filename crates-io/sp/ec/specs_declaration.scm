(define-module (crates-io sp ec specs_declaration) #:use-module (crates-io))

(define-public crate-specs_declaration-0.1.0 (c (n "specs_declaration") (v "0.1.0") (d (list (d (n "specs") (r "^0.16.1") (d #t) (k 2)))) (h "1jixr7x4rmdni8fgy5yps4yzwsd41021isp45xa0zn7pny3b6r4j")))

(define-public crate-specs_declaration-0.3.0 (c (n "specs_declaration") (v "0.3.0") (d (list (d (n "specs") (r "^0.16.1") (d #t) (k 2)))) (h "0vzz4rp5jaqpia58wr11gw94ccxjfvdw2fp3n7mlhpzw4p3gbdmp")))

