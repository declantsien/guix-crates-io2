(define-module (crates-io sp ec speculate2) #:use-module (crates-io))

(define-public crate-speculate2-0.2.0 (c (n "speculate2") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.47") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.4") (d #t) (k 0)))) (h "01qshiglc6cdqd3xa04z9w61x20d7sr4107ngkylq0104accqima") (f (quote (("default"))))))

