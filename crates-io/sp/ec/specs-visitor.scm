(define-module (crates-io sp ec specs-visitor) #:use-module (crates-io))

(define-public crate-specs-visitor-0.1.0 (c (n "specs-visitor") (v "0.1.0") (d (list (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "specs") (r "^0.14.1") (d #t) (k 0)))) (h "12wa8fcclwi37jhd1y328wdqx0cy2a1x7hi7cq660f20yf71r0mm")))

(define-public crate-specs-visitor-0.1.1 (c (n "specs-visitor") (v "0.1.1") (d (list (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "specs") (r "^0.14.1") (d #t) (k 0)))) (h "0q0mx8cwjpkwbmzfjws73dlbf3k4v72rv8lm8zqjkj8fzhkr53g9")))

(define-public crate-specs-visitor-0.2.0 (c (n "specs-visitor") (v "0.2.0") (d (list (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "specs") (r "^0.14.1") (d #t) (k 0)))) (h "07a307fcshsml6kqv5m7hwmayj3vwxlyf1m1m3rfbwvyi1jahsbz")))

(define-public crate-specs-visitor-0.3.0 (c (n "specs-visitor") (v "0.3.0") (d (list (d (n "rayon") (r "^1.0.3") (o #t) (d #t) (k 0)) (d (n "specs") (r "^0.14.3") (k 0)))) (h "1k0ci3zn3c8cy74hc79sq87x9xmj7c4j3friay7fp1djrfv3p55p") (f (quote (("test-render-graphs") ("parallel" "rayon" "specs/parallel") ("default" "parallel"))))))

