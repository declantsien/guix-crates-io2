(define-module (crates-io sp ec specinfra) #:use-module (crates-io))

(define-public crate-specinfra-0.1.0 (c (n "specinfra") (v "0.1.0") (d (list (d (n "downcast-rs") (r "^1.0.0") (d #t) (k 0)) (d (n "uname") (r "^0.1.1") (d #t) (k 0)))) (h "152q7pm56b972m98z94n7v8m8xbnl5ja4ml9sq2i3fzj6g7a6x5q")))

(define-public crate-specinfra-0.2.0 (c (n "specinfra") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.24") (d #t) (k 0)) (d (n "ssh2") (r "^0.3.1") (d #t) (k 0)) (d (n "uname") (r "^0.1.1") (d #t) (k 0)))) (h "0c0cj7vqmxdjanjb527j27y1s27yhkcrynm1q8yzxvmkmjv34dc6")))

(define-public crate-specinfra-0.3.0 (c (n "specinfra") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "md5") (r "^0.3.5") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)) (d (n "sha2") (r "^0.6.0") (d #t) (k 0)) (d (n "ssh2") (r "^0.3.1") (d #t) (k 0)) (d (n "uname") (r "^0.1.1") (d #t) (k 0)) (d (n "users") (r "^0.5.0") (d #t) (k 0)))) (h "1q9dpnfxwsm3q5rnaznz72ihpnpwsf8khwln9lmygl535krh3f6w")))

