(define-module (crates-io sp ec specs-mirror) #:use-module (crates-io))

(define-public crate-specs-mirror-0.1.0 (c (n "specs-mirror") (v "0.1.0") (d (list (d (n "cassowary") (r "^0.3.0") (d #t) (k 2)) (d (n "hibitset") (r "^0.5.0") (d #t) (k 0)) (d (n "shrev") (r "^1.0.0") (d #t) (k 0)) (d (n "specs") (r "^0.11.2") (d #t) (k 0)))) (h "09x3rsg24llk9w5q8w2yaagankbnkay70mvf3m6gv8gw398nqraw")))

(define-public crate-specs-mirror-0.2.0 (c (n "specs-mirror") (v "0.2.0") (d (list (d (n "cassowary") (r "^0.3.0") (d #t) (k 2)) (d (n "hibitset") (r "^0.5.0") (d #t) (k 0)) (d (n "shrev") (r "^1.0.0") (d #t) (k 0)) (d (n "specs") (r "^0.11.2") (d #t) (k 0)))) (h "010kwfj995v5jqrcpd4vqiwzim123zi8d90x681vrcxcpfhy3iib")))

(define-public crate-specs-mirror-0.3.0 (c (n "specs-mirror") (v "0.3.0") (d (list (d (n "cassowary") (r "^0.3.0") (d #t) (k 2)) (d (n "hibitset") (r "^0.5.0") (d #t) (k 0)) (d (n "shrev") (r "^1.0.1") (d #t) (k 0)) (d (n "specs") (r "^0.11.2") (d #t) (k 0)))) (h "0pmmln8nw2aa5pr7ra868zj55akwgiya0i2f71lwcynrbg078531")))

(define-public crate-specs-mirror-0.4.0 (c (n "specs-mirror") (v "0.4.0") (d (list (d (n "cassowary") (r "^0.3.0") (d #t) (k 2)) (d (n "hibitset") (r "^0.5.0") (d #t) (k 0)) (d (n "shrev") (r "^1.0.1") (d #t) (k 0)) (d (n "specs") (r "^0.11.2") (d #t) (k 0)))) (h "0qivhx1gprr5dj9ndnixh2q2gz6qps7zy3v38iycdl59kqsfzdp9")))

(define-public crate-specs-mirror-0.4.1 (c (n "specs-mirror") (v "0.4.1") (d (list (d (n "cassowary") (r "^0.3.0") (d #t) (k 2)) (d (n "hibitset") (r "^0.5.0") (d #t) (k 0)) (d (n "shrev") (r "^1.0.1") (d #t) (k 0)) (d (n "specs") (r "^0.11.2") (d #t) (k 0)))) (h "018clm62jk4x5aa92r2jrmlkqdgi4rz7gpnscj5mi7sy3ryrjczz")))

(define-public crate-specs-mirror-0.5.0 (c (n "specs-mirror") (v "0.5.0") (d (list (d (n "cassowary") (r "^0.3.0") (d #t) (k 2)) (d (n "hibitset") (r "^0.5.0") (d #t) (k 0)) (d (n "shrev") (r "^1.0.1") (d #t) (k 0)) (d (n "specs") (r "^0.12.0") (d #t) (k 0)))) (h "0rm2mhkf4zsi6svgvqkvs7l592rxrnavbp174gflyy58gw0j70gm")))

