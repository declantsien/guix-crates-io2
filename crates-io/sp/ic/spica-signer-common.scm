(define-module (crates-io sp ic spica-signer-common) #:use-module (crates-io))

(define-public crate-spica-signer-common-0.1.0 (c (n "spica-signer-common") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "totp-rs") (r "^4.2.0") (o #t) (d #t) (k 0)))) (h "0b0agjy964l8pp9z9dq8sg4vrxldbz1ly0sdqr7yw2iyhxyh878k") (f (quote (("std" "sign") ("default" "std")))) (s 2) (e (quote (("sign" "dep:totp-rs"))))))

(define-public crate-spica-signer-common-0.1.1 (c (n "spica-signer-common") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "totp-rs") (r "^4.2.0") (o #t) (d #t) (k 0)))) (h "1zgjnry1bfbsm8ln3hrak35r03cfnqig7fvdnzsv5qjxf5aldsk8") (f (quote (("std" "sign" "serde") ("default" "std")))) (s 2) (e (quote (("sign" "dep:totp-rs"))))))

(define-public crate-spica-signer-common-0.2.0 (c (n "spica-signer-common") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "totp-rs") (r "^4.2.0") (o #t) (d #t) (k 0)))) (h "0p3h96apl9a0ay1448vjyji29qgf8c2xk85z66yzr91ya5kg61ka") (f (quote (("std" "sign" "serde") ("default" "std")))) (s 2) (e (quote (("sign" "dep:totp-rs"))))))

