(define-module (crates-io sp ic spice21procs) #:use-module (crates-io))

(define-public crate-spice21procs-0.1.5 (c (n "spice21procs") (v "0.1.5") (d (list (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spice21int") (r "^0.1.5") (d #t) (k 0) (p "spice21int")) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bnlfr7rnsj5pcz8albzj68xqjqrkfw1d8g0mpigl8jv45saf7zd")))

