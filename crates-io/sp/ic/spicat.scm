(define-module (crates-io sp ic spicat) #:use-module (crates-io))

(define-public crate-spicat-0.1.0 (c (n "spicat") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.60") (d #t) (k 0)) (d (n "spidev") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "1d18mx8abl36p6r00mmgbzn502v886464virf5i7vfb8zx49764x")))

(define-public crate-spicat-0.1.1 (c (n "spicat") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.60") (d #t) (k 0)) (d (n "spidev") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "05jvqcyvl1az714f74zlpzr1ias5zpbpfmim8lvqijaxdb35gwhm")))

(define-public crate-spicat-0.1.3 (c (n "spicat") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.60") (d #t) (k 0)) (d (n "spidev") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "0f9c81v9v68z86182iwp013m0xrmbd7pzhcb367b0l92xx6q97wd")))

(define-public crate-spicat-0.1.4 (c (n "spicat") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.60") (d #t) (k 0)) (d (n "spidev") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "1q1lv7amkdmmbksp88brjj3b5jbsyxnf4xamg2zcwg3d48d8gsm3")))

(define-public crate-spicat-0.1.5 (c (n "spicat") (v "0.1.5") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)) (d (n "spidev") (r "^0.6") (d #t) (k 0)))) (h "1y75nn0i9fja3wrywjiwii9brpjad8vr2kgwgc98s22fw0xblji0")))

