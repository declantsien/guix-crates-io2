(define-module (crates-io sp ic spice-client-glib) #:use-module (crates-io))

(define-public crate-spice-client-glib-0.1.0 (c (n "spice-client-glib") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.0.1") (d #t) (k 0) (p "spice-client-glib-sys")) (d (n "gio") (r "^0.14") (d #t) (k 0)) (d (n "glib") (r "^0.14") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "rusb") (r "^0.9.0") (d #t) (k 0)))) (h "053jgljz93pgi5f8hk9pkjrll87vc5h8p5wy3zal8n0488gs4qpm")))

(define-public crate-spice-client-glib-0.2.0 (c (n "spice-client-glib") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.2.0") (d #t) (k 0) (p "spice-client-glib-sys")) (d (n "gio") (r "^0.15") (d #t) (k 0)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "rusb") (r "^0.9.0") (d #t) (k 0)))) (h "03bm0a4d75wbl214rqhjylffrjl74dlgwqd3947jrcyhnxgkyygf")))

(define-public crate-spice-client-glib-0.3.0 (c (n "spice-client-glib") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.3.0") (d #t) (k 0) (p "spice-client-glib-sys")) (d (n "gio") (r "^0.16") (d #t) (k 0)) (d (n "glib") (r "^0.16") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "rusb") (r "^0.9.0") (d #t) (k 0)))) (h "1ql1g8zbvgnmdrws3mfan6fs0ypfsh7gm9jv7xrhi484g62lbbzg")))

(define-public crate-spice-client-glib-0.4.0 (c (n "spice-client-glib") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.4.0") (d #t) (k 0) (p "spice-client-glib-sys")) (d (n "gio") (r "^0.17") (d #t) (k 0)) (d (n "glib") (r "^0.17") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "rusb") (r "^0.9.0") (d #t) (k 0)))) (h "0fj72lzjbjqiggbaf54zxz74xwp8i02x41lal16d085pqshjfcm3")))

