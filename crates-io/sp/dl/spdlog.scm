(define-module (crates-io sp dl spdlog) #:use-module (crates-io))

(define-public crate-spdlog-0.1.0 (c (n "spdlog") (v "0.1.0") (h "1mxw71f1wmg84ny1b193qldhl6ik3hr7sr9xkvil5p1mbbiw7b0g") (y #t)))

(define-public crate-spdlog-0.1.1 (c (n "spdlog") (v "0.1.1") (h "1y5jn7hpqj5qmrrx0iq1c5qgd55h1ngkacl9xgl1g67bg1jlwkg2") (y #t)))

