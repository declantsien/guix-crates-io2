(define-module (crates-io sp dl spdlog-src) #:use-module (crates-io))

(define-public crate-spdlog-src-0.1.0 (c (n "spdlog-src") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0m85f974nzpskzl5jcdkrphip5grds7a6524li4zsw07gihs5gwq")))

(define-public crate-spdlog-src-0.1.0+v1.8.4 (c (n "spdlog-src") (v "0.1.0+v1.8.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1k5cq0radphb1ylfi2bm2pvnvj47wpyh3yb4adnlp1dc84i7pr6m")))

(define-public crate-spdlog-src-0.1.0+v1.8.5 (c (n "spdlog-src") (v "0.1.0+v1.8.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0gsd49lcjynqga1rmf73gbcjwqclrpcvi79hkxwc9j8kviwhqdwd")))

(define-public crate-spdlog-src-0.1.1+v1.9.0 (c (n "spdlog-src") (v "0.1.1+v1.9.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0g9vm3d6844ldh35vbkjaganpsa071farnj6vi712my10jnrmaks")))

(define-public crate-spdlog-src-0.1.1+v1.9.1 (c (n "spdlog-src") (v "0.1.1+v1.9.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1hbkvsc504vc06kyk0nf6pqg4g3hsajy5nkwmmpckzx9a3akv9xk")))

(define-public crate-spdlog-src-0.1.2+v1.9.1 (c (n "spdlog-src") (v "0.1.2+v1.9.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0798zvqxd17wcqy9znniwb019knlik3zs1a9p260271vfmi7534a") (y #t)))

(define-public crate-spdlog-src-0.1.1+v1.9.2 (c (n "spdlog-src") (v "0.1.1+v1.9.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0y7zljj7wq056c8c3brrf10lx1bva7kpfl1fks1as1cdkyzb0jg5")))

