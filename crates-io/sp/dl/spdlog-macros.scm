(define-module (crates-io sp dl spdlog-macros) #:use-module (crates-io))

(define-public crate-spdlog-macros-0.1.0 (c (n "spdlog-macros") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "13cdvqaqmcr1vcmi2sb950iwpyaviiilyxlvz8m1rm36wf4pg1zb") (r "1.56")))

