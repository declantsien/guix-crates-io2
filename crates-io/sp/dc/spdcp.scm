(define-module (crates-io sp dc spdcp) #:use-module (crates-io))

(define-public crate-spdcp-0.1.0 (c (n "spdcp") (v "0.1.0") (h "1z87ydwxm9dlxbj7kihjf2rvnd0avzizddrx4bsxlbm7fw88jv07")))

(define-public crate-spdcp-0.2.0 (c (n "spdcp") (v "0.2.0") (h "1gbixvh6cmxb646ng42r2xl5l6dhzvr195fhh9zgq9py0z65ign7")))

(define-public crate-spdcp-0.3.0 (c (n "spdcp") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "198q37n7ykbl5l00d0jmgc5ybdbl8iymwqlkrc7kc154m5kg7117")))

(define-public crate-spdcp-0.4.0 (c (n "spdcp") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (o #t) (d #t) (k 0)))) (h "0a96m1a8n1h9114k831al4pyknwdvwfn733g1z88lnxaj7va77h0") (f (quote (("wasm" "serde" "wasm-bindgen") ("default"))))))

(define-public crate-spdcp-0.4.1 (c (n "spdcp") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (o #t) (d #t) (k 0)))) (h "0saf7qqp11p6nghpsi65jp0kgyj5ad254wfrzj7xszb115i2r215") (f (quote (("wasm" "serde" "wasm-bindgen") ("default"))))))

(define-public crate-spdcp-0.4.2 (c (n "spdcp") (v "0.4.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "061xaac19n6kcmcm4xzra0swy8iadq6lg6gsfbhnrsjdbfa6dx4n") (f (quote (("default"))))))

(define-public crate-spdcp-0.4.3 (c (n "spdcp") (v "0.4.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1sh7h17jzn8iykhc33klkfhmy4h5ff779ipcra1v0ikc2ca4wh74") (f (quote (("default"))))))

(define-public crate-spdcp-0.4.4 (c (n "spdcp") (v "0.4.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xgp022j2j4sqhqdrqm68zy3x8nlw83zlzrph8p4iiyx7903gzi5") (f (quote (("default"))))))

