(define-module (crates-io sp p- spp-rust) #:use-module (crates-io))

(define-public crate-spp-rust-0.1.0 (c (n "spp-rust") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)))) (h "0j15423zmhdvcq39v9v5ama7vx6lhmn8v6qr3xlxw5n1dcc6wgbn")))

(define-public crate-spp-rust-0.1.1 (c (n "spp-rust") (v "0.1.1") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)))) (h "08f6j01maza6792bjzdapi73mp6vvpp80af70vrsp86821cs6mjc")))

(define-public crate-spp-rust-0.2.0 (c (n "spp-rust") (v "0.2.0") (d (list (d (n "bitvec") (r "^1.0.1") (k 0)))) (h "0im7fd3h480nncl98r9h7mp2kv34brgvn6ba82c33vqh1y68rgzb")))

(define-public crate-spp-rust-0.2.1 (c (n "spp-rust") (v "0.2.1") (d (list (d (n "bitvec") (r "^1.0.1") (k 0)))) (h "03ng06wyj0nw8zsih878hyb3swds2xc04iv0i02sf9j546ckhlxx")))

(define-public crate-spp-rust-0.2.2 (c (n "spp-rust") (v "0.2.2") (d (list (d (n "bitvec") (r "^1.0.1") (k 0)))) (h "19ig6lp7l3s9h55r9fgh1mpyqkqaa0yjxbw59abd88jj390pmgmd")))

(define-public crate-spp-rust-0.2.3 (c (n "spp-rust") (v "0.2.3") (d (list (d (n "bitvec") (r "^1.0.1") (k 0)))) (h "0fvznbq1pz65fsfjly29943d7ni30zidfzgaf85z86d306nyp38l")))

