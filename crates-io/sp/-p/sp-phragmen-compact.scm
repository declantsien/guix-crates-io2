(define-module (crates-io sp -p sp-phragmen-compact) #:use-module (crates-io))

(define-public crate-sp-phragmen-compact-2.0.0-dev.1 (c (n "sp-phragmen-compact") (v "2.0.0-dev.1") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1fp0kps2yji9nla6dxpk2r7m0bawc9b5vpq0pyrigrnmcsd0sd3i") (y #t)))

(define-public crate-sp-phragmen-compact-2.0.0-alpha.7 (c (n "sp-phragmen-compact") (v "2.0.0-alpha.7") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "00mkyzj0ahcmakkapib6iq64cjr8vjhvpx2k89cazsha0fxdxgz2")))

(define-public crate-sp-phragmen-compact-2.0.0-alpha.8 (c (n "sp-phragmen-compact") (v "2.0.0-alpha.8") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0g01mkg94ipdavxz05l02dzpzzcvv9qigr6gfksqbzymc963vc3b")))

(define-public crate-sp-phragmen-compact-2.0.0-rc1 (c (n "sp-phragmen-compact") (v "2.0.0-rc1") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1iw6f714dqi1qppjjipgzrzr908b1xvj50p4rvvyyjlwrlz80xjd")))

(define-public crate-sp-phragmen-compact-2.0.0-rc2 (c (n "sp-phragmen-compact") (v "2.0.0-rc2") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0md2gvw6y04wgf1xr8h26339lzik6q779vqyg2x9jyvhlkpldsxw")))

