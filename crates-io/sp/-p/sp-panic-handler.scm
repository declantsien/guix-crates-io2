(define-module (crates-io sp -p sp-panic-handler) #:use-module (crates-io))

(define-public crate-sp-panic-handler-2.0.0-alpha.2 (c (n "sp-panic-handler") (v "2.0.0-alpha.2") (d (list (d (n "backtrace") (r "^0.3.38") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0mx6xyjr98zlnwnq04wb0lla1g4v47pp83rb43ilks87vis3f467")))

(define-public crate-sp-panic-handler-2.0.0-alpha.3 (c (n "sp-panic-handler") (v "2.0.0-alpha.3") (d (list (d (n "backtrace") (r "^0.3.38") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1d7pjjrk3c24rjrjmnvr62a3fnaqi5rl2ml13lzfic644phwrbs5")))

(define-public crate-sp-panic-handler-2.0.0-alpha.5 (c (n "sp-panic-handler") (v "2.0.0-alpha.5") (d (list (d (n "backtrace") (r "^0.3.38") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "00zpy3yibkq8rcgqkjdds4qa5q5zm3glqf924hs5358ivkrfjfk4")))

(define-public crate-sp-panic-handler-2.0.0-alpha.6 (c (n "sp-panic-handler") (v "2.0.0-alpha.6") (d (list (d (n "backtrace") (r "^0.3.38") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1r71413xy66pj4728hvq07ny89aiqx9g7fi3lyyf1g34lrg9x47g")))

(define-public crate-sp-panic-handler-2.0.0-alpha.7 (c (n "sp-panic-handler") (v "2.0.0-alpha.7") (d (list (d (n "backtrace") (r "^0.3.38") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "083dczljx208cyk5mh13k7d8gl5vsmb0yhmdxmrrcl8ksr667v5c")))

(define-public crate-sp-panic-handler-2.0.0-alpha.8 (c (n "sp-panic-handler") (v "2.0.0-alpha.8") (d (list (d (n "backtrace") (r "^0.3.38") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0pq564j0xd8c8mrz9a53a15l1mm9f6d7zd1ca1zczsx01zyazavr")))

(define-public crate-sp-panic-handler-2.0.0-rc1 (c (n "sp-panic-handler") (v "2.0.0-rc1") (d (list (d (n "backtrace") (r "^0.3.38") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0fi58ql1p5f9fxmj27xcl1vp7vfjjsm7f33g20agjdd6d41cwhm2")))

(define-public crate-sp-panic-handler-2.0.0-rc2 (c (n "sp-panic-handler") (v "2.0.0-rc2") (d (list (d (n "backtrace") (r "^0.3.38") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1clanmkyq0v49fs762l3b2avcv97kag67z7w2xdz4w58imlm9fff")))

(define-public crate-sp-panic-handler-2.0.0-rc3 (c (n "sp-panic-handler") (v "2.0.0-rc3") (d (list (d (n "backtrace") (r "^0.3.38") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0w3hwxw3ll348j6hr09pb6b2vcsih6pq1nr0rw9ha60pjrppycj8")))

(define-public crate-sp-panic-handler-2.0.0-rc4 (c (n "sp-panic-handler") (v "2.0.0-rc4") (d (list (d (n "backtrace") (r "^0.3.38") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1qyg9qfkhdm89mzwj9paf9np07hjrbl2p5r33xwxxq54ndm1ja10")))

(define-public crate-sp-panic-handler-2.0.0-rc5 (c (n "sp-panic-handler") (v "2.0.0-rc5") (d (list (d (n "backtrace") (r "^0.3.38") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1abp350bznc1ibcwn69br0kwa3vi9jgvphn3m15n2y3lwbmfhisl")))

(define-public crate-sp-panic-handler-2.0.0-rc6 (c (n "sp-panic-handler") (v "2.0.0-rc6") (d (list (d (n "backtrace") (r "^0.3.38") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0pm354wzk5w2ip1izsqzn13znw9dqr12k868hsm1i8pc8xnfg8sn")))

(define-public crate-sp-panic-handler-2.0.0 (c (n "sp-panic-handler") (v "2.0.0") (d (list (d (n "backtrace") (r "^0.3.38") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0wy53w60y338ydjad5kjidchhxyk4xjrv4p4wi06ffvbfvmjc8a9")))

(define-public crate-sp-panic-handler-2.0.1 (c (n "sp-panic-handler") (v "2.0.1") (d (list (d (n "backtrace") (r "^0.3.38") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1s5jxnx41mf3a99zhy89dj8zd27dl7z051m3xkcmqw4l94fh6m8h")))

(define-public crate-sp-panic-handler-3.0.0 (c (n "sp-panic-handler") (v "3.0.0") (d (list (d (n "backtrace") (r "^0.3.38") (d #t) (k 0)))) (h "112pjzmcan06rjb6w4hpb3n2rm0j4ryljnh6sh6qg2hwkw82ww2l")))

(define-public crate-sp-panic-handler-4.0.0 (c (n "sp-panic-handler") (v "4.0.0") (d (list (d (n "backtrace") (r "^0.3.38") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "197956qfssdk4nglvmbppmhdr07dm5gcaq8fzgygrspwap2z6091")))

(define-public crate-sp-panic-handler-5.0.0 (c (n "sp-panic-handler") (v "5.0.0") (d (list (d (n "backtrace") (r "^0.3.64") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "09p56916kragmv3ac72y2fbh1jgph1l6nrawkwmdp2djkk4rwm4p")))

(define-public crate-sp-panic-handler-6.0.0 (c (n "sp-panic-handler") (v "6.0.0") (d (list (d (n "backtrace") (r "^0.3.64") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1ln6hzj7qxg24ggmglic2adkp4nrvmm6frdhbbv24djv7nfdggja")))

(define-public crate-sp-panic-handler-7.0.0 (c (n "sp-panic-handler") (v "7.0.0") (d (list (d (n "backtrace") (r "^0.3.64") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "017ylxf5ywhbf141h74gpg678k6zcj00hj68s3vf15yq2z4nr63m")))

(define-public crate-sp-panic-handler-8.0.0 (c (n "sp-panic-handler") (v "8.0.0") (d (list (d (n "backtrace") (r "^0.3.64") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0cqh2c1ww1imk3mgza8b90k6mqrfshfag3434qj23a1z013dxlpb")))

(define-public crate-sp-panic-handler-8.1.0-dev1 (c (n "sp-panic-handler") (v "8.1.0-dev1") (d (list (d (n "backtrace") (r "^0.3.64") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "08mzdy5v1z0m009wbncpw8wpid1pmqqs4yflybdgv0vsbn4fami2")))

(define-public crate-sp-panic-handler-8.1.0-dev.2 (c (n "sp-panic-handler") (v "8.1.0-dev.2") (d (list (d (n "backtrace") (r "^0.3.64") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "10r3wlwzrfsjrlr0z60rwnp657vwlfb4hgkkcww5h4pkkflx07w5")))

(define-public crate-sp-panic-handler-8.1.0-dev.3 (c (n "sp-panic-handler") (v "8.1.0-dev.3") (d (list (d (n "backtrace") (r "^0.3.64") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1qx0bad5963vc52i146d68s1jxkrvzhggfraxswcrmxa5hny16d6")))

(define-public crate-sp-panic-handler-8.1.0-dev.4 (c (n "sp-panic-handler") (v "8.1.0-dev.4") (d (list (d (n "backtrace") (r "^0.3.64") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0z934m23lgrg0pq47p3qnh4dllpsyn2pdh52ff7im4nj0hfh5nbg")))

(define-public crate-sp-panic-handler-8.1.0-dev.5 (c (n "sp-panic-handler") (v "8.1.0-dev.5") (d (list (d (n "backtrace") (r "^0.3.64") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1qaalshs1arjidvg1bxr4sz39jhrc66hy0hznwpd7w0drkqgr6my")))

(define-public crate-sp-panic-handler-8.1.0-dev.6 (c (n "sp-panic-handler") (v "8.1.0-dev.6") (d (list (d (n "backtrace") (r "^0.3.64") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1d4zf6f4vhs9a2423xgq4wlzm523s1m6pr7klfrnvym8nxvb6g0k")))

(define-public crate-sp-panic-handler-9.0.0 (c (n "sp-panic-handler") (v "9.0.0") (d (list (d (n "backtrace") (r "^0.3.64") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1rzbxiqf435sd0yyx8dchc77nv57ic37bja3xwbk07fil2q7xijp")))

(define-public crate-sp-panic-handler-10.0.0 (c (n "sp-panic-handler") (v "10.0.0") (d (list (d (n "backtrace") (r "^0.3.64") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "04z95vkfi8sn5wy1rbhpixs3zy5q2pf7nkvvb3vird5l1p674596")))

(define-public crate-sp-panic-handler-11.0.0-dev.1 (c (n "sp-panic-handler") (v "11.0.0-dev.1") (d (list (d (n "backtrace") (r "^0.3.64") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "034n4aq1ykm4d47m5j98y54njgf4q8d9v97fk14z9y4dvmkzv1nk")))

(define-public crate-sp-panic-handler-11.0.0 (c (n "sp-panic-handler") (v "11.0.0") (d (list (d (n "backtrace") (r "^0.3.64") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1vb03qmi5hnd14n7h37wmmz2bjd31x26x9csqz8fbgy1ssi9n2fd")))

(define-public crate-sp-panic-handler-12.0.0 (c (n "sp-panic-handler") (v "12.0.0") (d (list (d (n "backtrace") (r "^0.3.64") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0sk9z82jidfy14divggib4w67ia56mrpq0sv2iziiq6kgs2l03mh")))

(define-public crate-sp-panic-handler-13.0.0 (c (n "sp-panic-handler") (v "13.0.0") (d (list (d (n "backtrace") (r "^0.3.64") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "05n4k1kj90y3cpls616chchy1rrcnglbc741ifd05phi19xa3xfq")))

