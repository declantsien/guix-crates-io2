(define-module (crates-io sp -p sp-plugin-rust-test-test) #:use-module (crates-io))

(define-public crate-sp-plugin-rust-test-test-0.3.7 (c (n "sp-plugin-rust-test-test") (v "0.3.7") (d (list (d (n "assert-str") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.2") (d #t) (k 0)))) (h "1arq1wfvz77cf6zx38vr4ghb6q14xir343rzcw3f4vsv94g7p2h0") (r "1.64")))

