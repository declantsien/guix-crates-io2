(define-module (crates-io sp ad spades) #:use-module (crates-io))

(define-public crate-spades-0.1.0 (c (n "spades") (v "0.1.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "0am0ic44alwqnywh5jqb54bb4bwg9im58z3isrshi9fcqpp7kn3k")))

(define-public crate-spades-0.1.1 (c (n "spades") (v "0.1.1") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "0wfghcgx6m0kiyzmak5kbig41468d4s04746m1d3n9ia6g5ybxq4")))

(define-public crate-spades-0.1.2 (c (n "spades") (v "0.1.2") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "1ng3j38qv309g6w1i6rbsrlzvnm1if3agg366wcynb0p7w4fhyzg")))

(define-public crate-spades-1.0.0 (c (n "spades") (v "1.0.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "19qmxw5mdpag2967zdkksj6sb6g5l2zvjxwbc7gg31ld72jdlbm4")))

