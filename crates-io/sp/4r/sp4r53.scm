(define-module (crates-io sp #{4r}# sp4r53) #:use-module (crates-io))

(define-public crate-sp4r53-0.1.0 (c (n "sp4r53") (v "0.1.0") (d (list (d (n "blake3") (r "^0.3") (d #t) (k 0)) (d (n "ethnum") (r "^1.0") (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1yr2v6pmfmwcysa2lb1a00ny6f7vm233wvnivg1a290wng6sjq7f") (f (quote (("default" "thiserror"))))))

(define-public crate-sp4r53-0.1.1 (c (n "sp4r53") (v "0.1.1") (d (list (d (n "blake3") (r "^0.3") (d #t) (k 0)) (d (n "ethnum") (r "^1.0") (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0pga8akf03c2mqwffzybasy7jkw4vp1nvx11nxfcd2qgpyqag70y") (f (quote (("default" "thiserror"))))))

(define-public crate-sp4r53-0.1.2 (c (n "sp4r53") (v "0.1.2") (d (list (d (n "blake3") (r "^0.3") (d #t) (k 0)) (d (n "ethnum") (r "^1.0") (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1h0p9432pafi6na1b3m058vynvxy6wcjq3zxyasyxrxji8hkri2a") (f (quote (("default" "thiserror"))))))

(define-public crate-sp4r53-0.1.3 (c (n "sp4r53") (v "0.1.3") (d (list (d (n "blake3") (r "^0.3") (d #t) (k 0)) (d (n "ethnum") (r "^1.0") (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0rihf8p8nmz1chmhpdf9ljay0fhikwhsvh29fa19rgi1qzjhxilk") (f (quote (("default" "thiserror"))))))

(define-public crate-sp4r53-0.1.4 (c (n "sp4r53") (v "0.1.4") (d (list (d (n "blake3") (r "^0.3") (d #t) (k 0)) (d (n "ethnum") (r "^1.0") (d #t) (k 0)) (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0ybjfan6p1m897c9n7sh1kbnvmj8wzzhhaa2j74fzjgj6hw58kmd") (f (quote (("default" "thiserror"))))))

