(define-module (crates-io sp al spalm) #:use-module (crates-io))

(define-public crate-spalm-0.1.0 (c (n "spalm") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "0gwsyfszqq1jnjmk3hih73p4yrsk0axhhal5wccdsbskfqp5g1ya") (y #t)))

(define-public crate-spalm-0.1.0-alpha (c (n "spalm") (v "0.1.0-alpha") (d (list (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "1lcpwrdzd60p3231q2fd4jmffhfirqa407wngs3sl9hlh3gncaga")))

(define-public crate-spalm-0.1.1-dev (c (n "spalm") (v "0.1.1-dev") (d (list (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "1r7c5n979sjwwarr2zh3zgxknqz6azkxs7na3pqz009vpql2ackw")))

