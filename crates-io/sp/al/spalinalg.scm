(define-module (crates-io sp al spalinalg) #:use-module (crates-io))

(define-public crate-spalinalg-0.0.0 (c (n "spalinalg") (v "0.0.0") (h "10zz2safj5xbmf78waim7kawmai491dm0yyznbpdizgwas8haxpc")))

(define-public crate-spalinalg-0.0.1 (c (n "spalinalg") (v "0.0.1") (h "1g1zx6r0bysm5yqac2l54yclm32g99w39r57n00jdrgfad00crdc")))

(define-public crate-spalinalg-0.0.2 (c (n "spalinalg") (v "0.0.2") (h "00bs91djsdlf7wz9s5qnp7lrlq7kpqkph17rdpz3j44nz5d3zrvp")))

