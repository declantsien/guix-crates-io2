(define-module (crates-io sp on sponge-preview) #:use-module (crates-io))

(define-public crate-sponge-preview-0.1.0 (c (n "sponge-preview") (v "0.1.0") (h "1fz7r0rnh0pi9l24c9qy2bk11z8fisdwrm92nyd8mh0yjv15zlsb")))

(define-public crate-sponge-preview-0.2.0 (c (n "sponge-preview") (v "0.2.0") (h "073v4s30qq07in8ihb7lvxsyivrqgff7cn974bqq6j74h6dsypm5")))

