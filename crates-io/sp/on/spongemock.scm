(define-module (crates-io sp on spongemock) #:use-module (crates-io))

(define-public crate-spongemock-0.0.0 (c (n "spongemock") (v "0.0.0") (h "1b62i9bfxhwcnxkxn47j45g0gf03pyn3k2h8gpvxdrbb5f9w0ja5")))

(define-public crate-spongemock-0.0.1 (c (n "spongemock") (v "0.0.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1n6xy3fwal93dgr5y7cpz6aa8yqbshcp992wfyqr3kmpdb8q8b9m")))

(define-public crate-spongemock-0.1.0 (c (n "spongemock") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0jnvbxajq8kbnl0aghcv8hq647fismvfwsdng3xdbz4y5ych8y05")))

(define-public crate-spongemock-0.1.1 (c (n "spongemock") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1pqczi75pf702xi6jwjiwvgz6dwfnw3v7w86b2p2c4gc6xw20f35")))

