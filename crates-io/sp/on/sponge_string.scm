(define-module (crates-io sp on sponge_string) #:use-module (crates-io))

(define-public crate-sponge_string-0.1.0 (c (n "sponge_string") (v "0.1.0") (h "1zcdykh6ahv9cj20jfz83695432f6r0cwqzaggaz6b9v4mhsi9ck")))

(define-public crate-sponge_string-0.1.1 (c (n "sponge_string") (v "0.1.1") (h "16rs2db484iwdh8lz9ng8wqshsgcscf34jvvsfpykc6k2wkb7cpq")))

