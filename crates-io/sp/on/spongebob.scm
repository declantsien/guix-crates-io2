(define-module (crates-io sp on spongebob) #:use-module (crates-io))

(define-public crate-spongebob-1.0.0 (c (n "spongebob") (v "1.0.0") (d (list (d (n "arboard") (r "^3.3.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.5.0") (f (quote ("derive" "unicode"))) (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0b67ic6snywqzjpcby7i7xq00c85c48f6ala1kpd484y9qjs9d8d")))

(define-public crate-spongebob-1.0.1 (c (n "spongebob") (v "1.0.1") (d (list (d (n "arboard") (r "^3.3.1") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.5.0") (f (quote ("derive" "unicode"))) (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0zqp38swkkfyyf5x7ap6d6gc222wfbdyvg0k5ip56gmpy4m4m7y7")))

(define-public crate-spongebob-1.1.0 (c (n "spongebob") (v "1.1.0") (d (list (d (n "arboard") (r "^3.4") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.5") (f (quote ("derive" "unicode"))) (d #t) (k 0)) (d (n "human-panic") (r "^2.0") (d #t) (k 0)) (d (n "predicates") (r "^3.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "13qm7j4q4xl70zwzplm9432cybagi6am2xf5wcildih5b4ibhwg8")))

(define-public crate-spongebob-1.1.1 (c (n "spongebob") (v "1.1.1") (d (list (d (n "anstyle") (r "^1.0.7") (d #t) (k 0)) (d (n "arboard") (r "^3.4") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.5") (f (quote ("derive" "unicode"))) (d #t) (k 0)) (d (n "human-panic") (r "^2.0") (d #t) (k 0)) (d (n "predicates") (r "^3.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1mr7lqm5nmcmxkbygamc78vrwyxk556xn2cfhli58q9j6iyjqcbm")))

