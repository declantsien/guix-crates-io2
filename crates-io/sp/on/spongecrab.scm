(define-module (crates-io sp on spongecrab) #:use-module (crates-io))

(define-public crate-spongecrab-0.2.3 (c (n "spongecrab") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.14") (f (quote ("derive" "string"))) (d #t) (k 0)))) (h "0bnjg2n2dmij8pwp2xavmic7rrbva93k5r4z0sl3ybn0nr5i5i45")))

