(define-module (crates-io sp ex spex) #:use-module (crates-io))

(define-public crate-spex-0.1.0 (c (n "spex") (v "0.1.0") (d (list (d (n "sipp") (r "^0.1.0") (d #t) (k 0)))) (h "1n66f28gzhnd2kswmjldlv33b8ncbgmhza8qr95c7ws8ggbfraw1")))

(define-public crate-spex-0.2.0 (c (n "spex") (v "0.2.0") (d (list (d (n "sipp") (r "^0.2.0") (d #t) (k 0)))) (h "1hpvgz5xxn2si2djxgv8r48ngw6gqz75c7brrz2ih17mb9f09gsh")))

(define-public crate-spex-0.2.1 (c (n "spex") (v "0.2.1") (d (list (d (n "sipp") (r "^0.2.0") (d #t) (k 0)))) (h "024h6qrahs7x8cgv6asvwvlmbkx8a06d57nmc094xr2qgjydylch")))

