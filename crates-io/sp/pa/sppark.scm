(define-module (crates-io sp pa sppark) #:use-module (crates-io))

(define-public crate-sppark-0.1.0 (c (n "sppark") (v "0.1.0") (h "0yvr73r3kv5bb650ipyz6y6hlr7m79igkcz36i4p9cdrp6pi6mvy") (l "sppark")))

(define-public crate-sppark-0.1.1 (c (n "sppark") (v "0.1.1") (h "1zc4virvp7aspgqjjpvvaa9sbhs48xwn34wrj9hiia96l4vwah6i") (l "sppark")))

(define-public crate-sppark-0.1.2 (c (n "sppark") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.70") (d #t) (k 1)) (d (n "which") (r "^4.0") (d #t) (k 1)))) (h "1ll8wy7z78pxhc705lfi5jp96sl1b9qq8bs2nlwkq6jc89cc0ar0") (l "sppark")))

(define-public crate-sppark-0.1.3 (c (n "sppark") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.70") (d #t) (k 1)) (d (n "which") (r "^4.0") (d #t) (k 1)))) (h "0sl9jjg0yrw9vcflraaxw4hx6nyzsi8p9wjdghcs2p5kmrmliffg") (l "sppark")))

(define-public crate-sppark-0.1.4 (c (n "sppark") (v "0.1.4") (d (list (d (n "cc") (r "^1.0.70") (d #t) (k 1)) (d (n "which") (r "^4.0") (d #t) (k 1)))) (h "0knzhfkddn9xr2z1fyyphxz2mxiy03z6r3rw2f05gf4w8r8dafm9") (y #t) (l "sppark")))

(define-public crate-sppark-0.1.5 (c (n "sppark") (v "0.1.5") (d (list (d (n "cc") (r "^1.0.70") (d #t) (k 1)) (d (n "which") (r "^4.0") (d #t) (k 1)))) (h "0iiw0mqcih1zz36dnyc9xvzfgmjn5nkx8p012y4ylrkqjfc6syms") (l "sppark")))

(define-public crate-sppark-0.1.6 (c (n "sppark") (v "0.1.6") (d (list (d (n "cc") (r "^1.0.70") (d #t) (k 1)) (d (n "which") (r "^4.0") (d #t) (k 1)))) (h "0jwsd0ixy16k8sn0g45v65pd94pwp0vf3mvpsiq3dmgav8ann9pv") (l "sppark")))

