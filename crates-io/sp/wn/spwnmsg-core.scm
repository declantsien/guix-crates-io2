(define-module (crates-io sp wn spwnmsg-core) #:use-module (crates-io))

(define-public crate-spwnmsg-core-0.1.0 (c (n "spwnmsg-core") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "0k76hirlh695fq46lrjb4jp1rarxp1ncyfy5nfyxmjhfij1zc6sy")))

(define-public crate-spwnmsg-core-0.1.1 (c (n "spwnmsg-core") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "0xmwqhnvvfk7xq7pni3rp5xnkds4s90sfv1srmhzrv49hbzpk669")))

