(define-module (crates-io sp q- spq-as) #:use-module (crates-io))

(define-public crate-spq-as-0.1.4 (c (n "spq-as") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spq-spvasm") (r "^0.1.4") (d #t) (k 0)))) (h "0f15p5x0vbprpgfav2bc35fdvn9hnq45hfrggflv8ianv2466ivn")))

