(define-module (crates-io sp q- spq-dis) #:use-module (crates-io))

(define-public crate-spq-dis-0.1.4 (c (n "spq-dis") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spq-spvasm") (r "^0.1.4") (d #t) (k 0)))) (h "0rsyrlqs7i0iwc5xrgfyxjrxqv0c2z69rkb6bf0365i1cxca06hw")))

