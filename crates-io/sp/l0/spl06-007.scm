(define-module (crates-io sp l0 spl06-007) #:use-module (crates-io))

(define-public crate-spl06-007-0.1.0 (c (n "spl06-007") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)))) (h "14f8ff8g5m3klnmsbkdn7q0hz0fvjfj5drs8djmvmwbcbg99zh3j")))

(define-public crate-spl06-007-0.2.0 (c (n "spl06-007") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)))) (h "16dd5f7fm2y21yd1zg6glqk5rs1wfg7a54k5c22mw90apfgahhfv")))

(define-public crate-spl06-007-0.2.1 (c (n "spl06-007") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)))) (h "003f0i0hncxxh3v06rdm2nf7nz8qirvbnh36y8m8j3aadn90jsvx")))

(define-public crate-spl06-007-0.2.2 (c (n "spl06-007") (v "0.2.2") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9") (d #t) (k 2)))) (h "0yy4aa58mpihlz8aw5kqsfrapskg81z1hfgmn7ms7z5w6f3bh46y")))

(define-public crate-spl06-007-0.3.0 (c (n "spl06-007") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9") (d #t) (k 2)))) (h "0bz4mms9nnba4ni2kmrcpak882b2pg4azkhb2cykfygvxs8y1hfc")))

(define-public crate-spl06-007-0.3.1 (c (n "spl06-007") (v "0.3.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9") (d #t) (k 2)))) (h "11h2pc3p8qr7q7qdf53mx76piiapw9ns59r4m896hpgn4yyvkn6r")))

(define-public crate-spl06-007-0.3.2 (c (n "spl06-007") (v "0.3.2") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9") (d #t) (k 2)))) (h "03msjrr28famwswy0v4rjymmys36z3bj207jcj84kn24zkdjyy7i")))

(define-public crate-spl06-007-0.3.3 (c (n "spl06-007") (v "0.3.3") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9") (d #t) (k 2)))) (h "163zcpb1h7l2vb1pagkngyah5yz1ig4fpa0ycksrqk7wh43da71p")))

