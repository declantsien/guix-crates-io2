(define-module (crates-io sp he spherical-harmonics) #:use-module (crates-io))

(define-public crate-spherical-harmonics-0.1.0 (c (n "spherical-harmonics") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.18.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1c4gxi6cb9pvr2jag3gss6592h6g5hv3s6jgk56f3bxvvkiqixya")))

