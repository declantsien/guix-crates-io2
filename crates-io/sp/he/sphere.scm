(define-module (crates-io sp he sphere) #:use-module (crates-io))

(define-public crate-sphere-0.1.0 (c (n "sphere") (v "0.1.0") (h "1sq1acn1xyvxplh9j5c5sykrs8yvw36r6z6plqfvml1i7p89iqlj")))

(define-public crate-sphere-0.2.0 (c (n "sphere") (v "0.2.0") (d (list (d (n "num") (r "^0.1.31") (d #t) (k 0)))) (h "0why5v7b7inmcqbn7f2qxvss73pnchd1aa111hl8xpd5ii726bb3")))

(define-public crate-sphere-0.3.0 (c (n "sphere") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.1.32") (d #t) (k 0)))) (h "0q52saniyvr25hhbnkhryxaf57vrgkfhyq1c0n2scw86m25br1f8")))

