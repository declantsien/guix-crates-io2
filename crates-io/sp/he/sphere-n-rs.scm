(define-module (crates-io sp he sphere-n-rs) #:use-module (crates-io))

(define-public crate-sphere-n-rs-0.1.0 (c (n "sphere-n-rs") (v "0.1.0") (d (list (d (n "approx_eq") (r "^0.1.8") (d #t) (k 2)) (d (n "interp") (r "^1.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "lds-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)))) (h "1g5parbhcflwb1icbdzlxh6ggk10iak115kx6msyq6bxshs8g8qb")))

