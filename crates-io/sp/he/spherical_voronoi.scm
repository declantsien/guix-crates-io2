(define-module (crates-io sp he spherical_voronoi) #:use-module (crates-io))

(define-public crate-spherical_voronoi-0.1.0 (c (n "spherical_voronoi") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.12.0") (d #t) (k 0)) (d (n "ideal") (r "^0.1.0") (d #t) (k 0)))) (h "075bsmzkm90nz9xb588r6ka6bskjw0xr5s3c84drqzy3q0d3hxpy")))

(define-public crate-spherical_voronoi-0.2.0 (c (n "spherical_voronoi") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.12.0") (d #t) (k 0)) (d (n "ideal") (r "^0.1.0") (d #t) (k 0)))) (h "0339j5g15bjxpwjm0r2v8i658ypnx8jn91j94ryh6kglyqzzl06f")))

(define-public crate-spherical_voronoi-0.2.1 (c (n "spherical_voronoi") (v "0.2.1") (d (list (d (n "cgmath") (r "^0.12.0") (d #t) (k 0)) (d (n "ideal") (r "^0.1.0") (d #t) (k 0)))) (h "1l0xswnmv1wx27jdv11107glvb6vd6hshhid2h36czbxlcymc3yz")))

(define-public crate-spherical_voronoi-0.3.0 (c (n "spherical_voronoi") (v "0.3.0") (d (list (d (n "cgmath") (r "^0.12.0") (d #t) (k 0)) (d (n "ideal") (r "^0.1.0") (d #t) (k 0)))) (h "1bm56jx1hl5dka4a9kzd9yfddrdd1l7ac4n1m4cxh9rp6vfw248s")))

(define-public crate-spherical_voronoi-0.3.1 (c (n "spherical_voronoi") (v "0.3.1") (d (list (d (n "cgmath") (r "^0.12.0") (d #t) (k 0)) (d (n "ideal") (r "^0.1.0") (d #t) (k 0)))) (h "1firmbp57jmqc5yg71iw3xjaxr18sx9wk43pa9wswy8gnxwwdxjl")))

(define-public crate-spherical_voronoi-0.3.2 (c (n "spherical_voronoi") (v "0.3.2") (d (list (d (n "cgmath") (r "^0.12.0") (d #t) (k 0)) (d (n "ideal") (r "^0.1.0") (d #t) (k 0)))) (h "0dgv23k7mj5mnhb0dwx3i2wwva4li5fr0ns8l6pny3014rnsy7yb")))

(define-public crate-spherical_voronoi-0.3.3 (c (n "spherical_voronoi") (v "0.3.3") (d (list (d (n "cgmath") (r "^0.12.0") (d #t) (k 0)) (d (n "ideal") (r "^0.1.0") (d #t) (k 0)))) (h "1g2j7fsz3qac0fvkq78n4kb88macqfjxhmn0xx73sgcvif9dwvjb")))

(define-public crate-spherical_voronoi-0.4.0 (c (n "spherical_voronoi") (v "0.4.0") (d (list (d (n "cgmath") (r "^0.13") (d #t) (k 0)) (d (n "ideal") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)))) (h "1spbs314rrp0wal92cffw8b6m725f8cmzjxbm4il1q8813n8xr64")))

(define-public crate-spherical_voronoi-0.5.0 (c (n "spherical_voronoi") (v "0.5.0") (d (list (d (n "cgmath") (r "^0.14") (d #t) (k 0)))) (h "1lfscxj1yx7ii4zxrc1q9xibkg1knajgyq9byyjdccfx123iw4w2")))

(define-public crate-spherical_voronoi-0.5.1 (c (n "spherical_voronoi") (v "0.5.1") (d (list (d (n "cgmath") (r "^0.14") (d #t) (k 0)))) (h "1n3i9880lc5ncx15msikfvrib4a97cxnyi6z0gbwrj00iyzgxc5p")))

