(define-module (crates-io sp he sphere_pack_from_json) #:use-module (crates-io))

(define-public crate-sphere_pack_from_json-0.1.0 (c (n "sphere_pack_from_json") (v "0.1.0") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "spherical-cow") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1n8png7ayd57xhkmjzclbdbcm4c4s7sm3k5rjc4gzzmcmf13g3gh")))

(define-public crate-sphere_pack_from_json-0.1.1 (c (n "sphere_pack_from_json") (v "0.1.1") (d (list (d (n "clap") (r "~2.27.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.24.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "spherical-cow") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1xgmv74hq2m88nbb9bf24nxrq3ai22045f28fykwbr1did74dhgw")))

