(define-module (crates-io sp uz spuz_piston) #:use-module (crates-io))

(define-public crate-spuz_piston-0.0.0 (c (n "spuz_piston") (v "0.0.0") (h "10l318iq7kr13cpksi2az65v36ri6fn2vlij51w03gkg0x3ggcca")))

(define-public crate-spuz_piston-0.1.0 (c (n "spuz_piston") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "01kymm74rkx3g8n96bzg016361kmjy0jvd271plw9zajqqadcdys")))

(define-public crate-spuz_piston-0.1.1 (c (n "spuz_piston") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "03dny33k623kf746py0hby1hbahivbazjzfm2d3lrmjrkh414wvl")))

