(define-module (crates-io sp oa spoa-sys) #:use-module (crates-io))

(define-public crate-spoa-sys-0.1.0 (c (n "spoa-sys") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "1fkk602ds0yy8p35156z8yrg8yl78gr3n9si1rm2i7zjlld88dyg") (f (quote (("simde") ("default")))) (l "spoa")))

(define-public crate-spoa-sys-0.1.1 (c (n "spoa-sys") (v "0.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "0sqrljvqw47mhmvyg31gr3kaiivmcyvahr5qs18zbx5d5gx3qs4l") (f (quote (("simde") ("default")))) (l "spoa")))

