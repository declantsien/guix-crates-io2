(define-module (crates-io sp oa spoa) #:use-module (crates-io))

(define-public crate-spoa-0.1.0 (c (n "spoa") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "spoa-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 2)))) (h "1ppb4cks37cyf3pnphl006knbv5ws3h46kfkw0ymaf4van38yag4") (f (quote (("simde" "spoa-sys/simde") ("default"))))))

(define-public crate-spoa-0.1.1 (c (n "spoa") (v "0.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "spoa-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 2)))) (h "1y08d314q2cj1rgd121rdnp00vjb68nbhi3fr4295fmh7mch0hca") (f (quote (("simde" "spoa-sys/simde") ("default"))))))

