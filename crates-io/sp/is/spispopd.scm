(define-module (crates-io sp is spispopd) #:use-module (crates-io))

(define-public crate-spispopd-0.1.0 (c (n "spispopd") (v "0.1.0") (d (list (d (n "clipboard-win") (r "^4.0.3") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "x11-clipboard") (r "^0.5.3") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)))) (h "1d29rf3xa5m381capy66wg870v254ayrbypfylydc2zci9wnwk9n")))

