(define-module (crates-io sp dk spdkit) #:use-module (crates-io))

(define-public crate-spdkit-0.0.0 (c (n "spdkit") (v "0.0.0") (h "0idx50hprqwv5ngvaxaivd7d9g0xw8i64pfx2xr6fg4hnjri9x0h") (y #t)))

(define-public crate-spdkit-0.0.17 (c (n "spdkit") (v "0.0.17") (d (list (d (n "gchemol") (r ">= 0.0.39") (f (quote ("adhoc"))) (d #t) (k 0)) (d (n "gut") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "vecfx") (r "^0.1") (d #t) (k 0)))) (h "15qwimbyckmbq2bcw2za1sr7pgs3wp7vz48fi498qm4kwrjjicxz") (y #t)))

(define-public crate-spdkit-0.0.18 (c (n "spdkit") (v "0.0.18") (d (list (d (n "gchemol") (r ">= 0.0.39") (f (quote ("adhoc"))) (d #t) (k 0)) (d (n "gut") (r "^0.1") (d #t) (k 0) (p "gchemol-gut")) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "vecfx") (r "^0.1") (d #t) (k 0)))) (h "0vwn8jygxpx9y5n41nxs7y27xla738aqlp24x7y5rgps83fcbmsl")))

(define-public crate-spdkit-0.0.19 (c (n "spdkit") (v "0.0.19") (d (list (d (n "envy") (r "^0.4") (d #t) (k 0)) (d (n "gchemol") (r ">=0.0.41") (f (quote ("adhoc"))) (d #t) (k 0)) (d (n "gut") (r "^0.2") (d #t) (k 0) (p "gchemol-gut")) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "vecfx") (r "^0.1") (d #t) (k 0)))) (h "12p4gip3jipcvr3k2vl7idczrwr2206v748v76wzvi7a00wfqakf")))

(define-public crate-spdkit-0.0.20 (c (n "spdkit") (v "0.0.20") (d (list (d (n "envy") (r "^0.4") (d #t) (k 0)) (d (n "gchemol") (r "^0.0.42") (f (quote ("adhoc"))) (d #t) (k 0)) (d (n "gut") (r "^0.4") (d #t) (k 0) (p "gchemol-gut")) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "nauty") (r "^0.1.1") (d #t) (k 0) (p "spdkit-nauty")) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "vecfx") (r "^0.1.5") (d #t) (k 0)))) (h "1y6ynyzwk3b7c1gc6h58ndsgdx83fs0avviq9qs5wg9bbh7cc87x")))

(define-public crate-spdkit-0.1.0 (c (n "spdkit") (v "0.1.0") (d (list (d (n "envy") (r "^0.4") (d #t) (k 0)) (d (n "gchemol") (r "^0.1.0") (f (quote ("adhoc"))) (d #t) (k 0)) (d (n "gut") (r "^0.4") (d #t) (k 0) (p "gchemol-gut")) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "nauty") (r "^0.1.1") (d #t) (k 0) (p "spdkit-nauty")) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "vecfx") (r "^0.1.5") (d #t) (k 0)))) (h "1npb98kiw09ss67kk5y0m02qr739gvbgg578zjl8jbvhgagip30j")))

