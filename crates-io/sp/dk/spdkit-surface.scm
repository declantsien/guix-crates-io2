(define-module (crates-io sp dk spdkit-surface) #:use-module (crates-io))

(define-public crate-spdkit-surface-0.1.0 (c (n "spdkit-surface") (v "0.1.0") (d (list (d (n "gchemol") (r "^0.1") (f (quote ("adhoc"))) (d #t) (k 0)) (d (n "gut") (r "^0.4") (d #t) (k 0) (p "gchemol-gut")) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "vecfx") (r "^0.1") (f (quote ("nalgebra"))) (d #t) (k 0)))) (h "14isc6nnhcnraa464xysrrcabjvkgsfbjfkjzpqwck5jjj4ssvnw") (f (quote (("adhoc"))))))

(define-public crate-spdkit-surface-0.1.1 (c (n "spdkit-surface") (v "0.1.1") (d (list (d (n "gchemol") (r "^0.1") (f (quote ("adhoc"))) (d #t) (k 0)) (d (n "gut") (r "^0.4") (d #t) (k 0) (p "gchemol-gut")) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "vecfx") (r "^0.1.6") (f (quote ("nalgebra"))) (d #t) (k 0)))) (h "1zgf9id0409x5cn84qp0l5kjbgxa3aibx4jssl79vykxjb7krff1") (f (quote (("adhoc"))))))

