(define-module (crates-io sp dk spdkit-nauty) #:use-module (crates-io))

(define-public crate-spdkit-nauty-0.1.0 (c (n "spdkit-nauty") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1pbpvx7x3qds0cqz0kmqgnnampdb7fm00yn1ca0s0yx0mh08a6j5") (f (quote (("adhoc"))))))

(define-public crate-spdkit-nauty-0.1.1 (c (n "spdkit-nauty") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "170ir5pxg14svr66l5bby2ly54bqk1pcjrfgwrin1x5ia5f8x571") (f (quote (("adhoc"))))))

