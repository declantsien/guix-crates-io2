(define-module (crates-io sp m_ spm_precompiled) #:use-module (crates-io))

(define-public crate-spm_precompiled-0.1.0 (c (n "spm_precompiled") (v "0.1.0") (d (list (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "16ipdhr77ri01kkqxbkqdx23zd7rfm73ibmyprdp2sdp37n254il")))

(define-public crate-spm_precompiled-0.1.1 (c (n "spm_precompiled") (v "0.1.1") (d (list (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "1idmr974iwbyp27s825n5jpvx328ig4ng3rlq2krkj7gr62yi2zp")))

(define-public crate-spm_precompiled-0.1.2 (c (n "spm_precompiled") (v "0.1.2") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "05d77q286xm4zkvb7fqi650im0wq3jk08r5wi9kk16fm707yzqs6")))

(define-public crate-spm_precompiled-0.1.3 (c (n "spm_precompiled") (v "0.1.3") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "nom") (r "^6.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "0pyr1xnhlvsxbsk3lz3kihi1wys5w7qjg2pl2nvym32bzl1a85nz")))

(define-public crate-spm_precompiled-0.1.4 (c (n "spm_precompiled") (v "0.1.4") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.9") (d #t) (k 0)))) (h "09pkdk2abr8xf4pb9kq3rk80dgziq6vzfk7aywv3diik82f6jlaq")))

