(define-module (crates-io sp ag spaghetti) #:use-module (crates-io))

(define-public crate-spaghetti-0.1.0 (c (n "spaghetti") (v "0.1.0") (h "1kv9zipwfpbfya436b5hiknhmz4yhp0jnjisrqi7akkzk60qzb19")))

(define-public crate-spaghetti-0.1.1 (c (n "spaghetti") (v "0.1.1") (h "11w4576qd45d67vi25j10zsgc7ikc6shby45n2kwv2fhaznyvrjc")))

