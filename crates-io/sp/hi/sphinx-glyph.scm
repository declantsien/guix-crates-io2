(define-module (crates-io sp hi sphinx-glyph) #:use-module (crates-io))

(define-public crate-sphinx-glyph-0.1.0 (c (n "sphinx-glyph") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "sphinx-auther") (r "^0.1.10") (d #t) (k 0)))) (h "0gh9c0mr3hi9fhs1fhmbrvsjafvfyzhin7v1jghrykjl02rjf1fg")))

(define-public crate-sphinx-glyph-0.1.1 (c (n "sphinx-glyph") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "sphinx-auther") (r "^0.1.10") (d #t) (k 0)))) (h "0wbn62xk4rxn065fa846b1a8dgxa2x8ywk3q0q009xhra557vayv")))

(define-public crate-sphinx-glyph-0.1.2 (c (n "sphinx-glyph") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sphinx-auther") (r "^0.1.12") (d #t) (k 0)))) (h "095fkfhihi68vz1069a2r4v9hhlr8nkyihyg9a15s3ld91nxvyk2")))

