(define-module (crates-io sp hi sphinx-use-state) #:use-module (crates-io))

(define-public crate-sphinx-use-state-0.0.1 (c (n "sphinx-use-state") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0w95jhh3bnaybkw27p0k2rxv5ag6hhm6fabm856v70mppnc2cf0f")))

