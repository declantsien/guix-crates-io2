(define-module (crates-io sp hi sphinxad-sys) #:use-module (crates-io))

(define-public crate-sphinxad-sys-0.1.0 (c (n "sphinxad-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0slrz0nnxsj2ycs19mcjsxqcdw1cm35jk0wx9ss9y0xp0hq3j02p")))

(define-public crate-sphinxad-sys-0.1.1 (c (n "sphinxad-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0lm5qya6pyzfiv5qp4lx5ha24d05x73br3ffafd5vvz72bffasvn")))

(define-public crate-sphinxad-sys-0.1.2 (c (n "sphinxad-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "10ix28hswzb8f2sw9q6666p8kp5ip5l63y1vncxsmwhi4cab2izl")))

(define-public crate-sphinxad-sys-0.1.3 (c (n "sphinxad-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "0cwbyqlfhmnamhykxsm9410bqxg1nlz4f9skwpa05n4350zbv8d4")))

