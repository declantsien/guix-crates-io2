(define-module (crates-io sp hi sphinx-rustdocgen) #:use-module (crates-io))

(define-public crate-sphinx-rustdocgen-0.1.0 (c (n "sphinx-rustdocgen") (v "0.1.0") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0c8da0ccakczi8sdsbk4nqjjb6jjh3kfijbhs7bkgnhx78g73yp3") (y #t)))

(define-public crate-sphinx-rustdocgen-0.1.1 (c (n "sphinx-rustdocgen") (v "0.1.1") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0q1j5y3a8l7kkn19349a1a3g7h9xp2q6fdi31sqgnci4pshy0man") (y #t)))

(define-public crate-sphinx-rustdocgen-0.2.0 (c (n "sphinx-rustdocgen") (v "0.2.0") (d (list (d (n "log") (r "^0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fdhd392gx1d2z45l5x0zn0jy96s9blfwq5gj0fyz15hx6zp5k4v")))

