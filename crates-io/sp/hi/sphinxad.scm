(define-module (crates-io sp hi sphinxad) #:use-module (crates-io))

(define-public crate-sphinxad-0.1.0 (c (n "sphinxad") (v "0.1.0") (d (list (d (n "sphinxad-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0g3jb0lad86n1vnmnlrbc097iasnr74g01dgqyapqdrcnwnkjzfg")))

(define-public crate-sphinxad-0.1.1 (c (n "sphinxad") (v "0.1.1") (d (list (d (n "sphinxad-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1lm70f495biah9s3w8x76m8xm8vqlhgz0gbsyp7afgrx5k0i2ijg")))

(define-public crate-sphinxad-0.1.2 (c (n "sphinxad") (v "0.1.2") (d (list (d (n "sphinxad-sys") (r "^0.1.3") (d #t) (k 0)))) (h "1cspc1232ziz5qmymvqxswcm3qh706xnpfgs5hs25q310nrkw915")))

