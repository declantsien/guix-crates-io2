(define-module (crates-io sp ac spacetrace) #:use-module (crates-io))

(define-public crate-spacetrace-0.1.0 (c (n "spacetrace") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "vek") (r "^0.15") (f (quote ("libm" "serde"))) (k 0)))) (h "1lnqbiihx24bf2xg5iyy50rqmzwf0dbi6i57k5kn45jvl2d7zynv") (f (quote (("simd" "vek/repr_simd") ("default"))))))

