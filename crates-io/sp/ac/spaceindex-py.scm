(define-module (crates-io sp ac spaceindex-py) #:use-module (crates-io))

(define-public crate-spaceindex-py-0.3.0 (c (n "spaceindex-py") (v "0.3.0") (d (list (d (n "pyo3") (r "^0.9.0-alpha.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "spaceindex") (r "^0.3.0") (d #t) (k 0)))) (h "04d7ina9h3ygaqaffksl7vhz68vg1nzrmmzppyd0xbnrw4mn4grx")))

