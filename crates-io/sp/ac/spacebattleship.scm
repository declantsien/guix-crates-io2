(define-module (crates-io sp ac spacebattleship) #:use-module (crates-io))

(define-public crate-spacebattleship-0.1.0 (c (n "spacebattleship") (v "0.1.0") (d (list (d (n "enumflags2") (r "^0.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "086b4m2pa4v0ycagnnb2xw98immfz4nzi5kq452bh9nccyindwn0") (f (quote (("rng_gen" "rand" "once_cell"))))))

