(define-module (crates-io sp ac spacebadgers) #:use-module (crates-io))

(define-public crate-spacebadgers-1.0.0 (c (n "spacebadgers") (v "1.0.0") (d (list (d (n "htmlize") (r "^1.0.2") (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)))) (h "01wkp89g9fnhifcis6j5c907a9fc20c0in64b57lq01dp6kzl440") (y #t)))

(define-public crate-spacebadgers-1.0.1 (c (n "spacebadgers") (v "1.0.1") (d (list (d (n "htmlize") (r "^1.0.2") (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)))) (h "08kf4hb9m2jkmdd1xj0m8i7zv6v592q2jx5ff0m3240ak0haw49r")))

(define-public crate-spacebadgers-1.1.0 (c (n "spacebadgers") (v "1.1.0") (d (list (d (n "htmlize") (r "^1.0.2") (d #t) (k 0)) (d (n "indoc") (r "^2.0.1") (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)))) (h "17a75yisjfrjd4j8mp29q5g71x1fz97z0g1kh8xiljyb7vsd4hpv")))

(define-public crate-spacebadgers-1.1.1 (c (n "spacebadgers") (v "1.1.1") (d (list (d (n "htmlize") (r "^1.0.2") (d #t) (k 0)) (d (n "indoc") (r "^2.0.1") (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)))) (h "19fwqax5aq7v6hinzir13v14w5cdsq35vpgl5w53wdj60ilbv6cg")))

(define-public crate-spacebadgers-1.2.0 (c (n "spacebadgers") (v "1.2.0") (d (list (d (n "htmlize") (r "^1.0.2") (d #t) (k 0)) (d (n "indoc") (r "^2.0.1") (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)))) (h "1fdgjc9wqbc3gwdkj6jqmim4kv8xmksp40v8zrgqr2131lgzzazc")))

(define-public crate-spacebadgers-1.3.0 (c (n "spacebadgers") (v "1.3.0") (d (list (d (n "criterion") (r "^0.5.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "htmlize") (r "^1.0.2") (d #t) (k 0)) (d (n "indoc") (r "^2.0.1") (d #t) (k 0)) (d (n "indoc") (r "^2.0.1") (d #t) (k 1)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "spacebadgers-utils") (r "^1") (d #t) (k 0)) (d (n "spacebadgers-utils") (r "^1") (d #t) (k 1)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "191gswb4mp7w7skjc7msznvkmw64f3ym33zmrnbb5749f7j7hqdh") (s 2) (e (quote (("serde" "dep:serde" "phf/serde"))))))

(define-public crate-spacebadgers-1.3.1 (c (n "spacebadgers") (v "1.3.1") (d (list (d (n "criterion") (r "^0.5.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "htmlize") (r "^1.0.2") (d #t) (k 0)) (d (n "indoc") (r "^2.0.1") (d #t) (k 0)) (d (n "indoc") (r "^2.0.1") (d #t) (k 1)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "spacebadgers-utils") (r "^1") (d #t) (k 0)) (d (n "spacebadgers-utils") (r "^1") (d #t) (k 1)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "1ks42b5p51nssw4dyp5n4lsyrcm4n7aiwjsh3gkig5blk4q0v0vs") (s 2) (e (quote (("serde" "dep:serde" "phf/serde"))))))

(define-public crate-spacebadgers-1.3.2 (c (n "spacebadgers") (v "1.3.2") (d (list (d (n "criterion") (r "^0.5.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "htmlize") (r "^1.0.2") (d #t) (k 0)) (d (n "indoc") (r "^2.0.1") (d #t) (k 0)) (d (n "indoc") (r "^2.0.1") (d #t) (k 1)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "spacebadgers-utils") (r "^1") (d #t) (k 0)) (d (n "spacebadgers-utils") (r "^1") (d #t) (k 1)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "0kwnmjdx6q3hid7gdh10hj8nnwxs6hzvixylkwrk6wwyn1gyfwxp") (s 2) (e (quote (("serde" "dep:serde" "phf/serde"))))))

