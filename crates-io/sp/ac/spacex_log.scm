(define-module (crates-io sp ac spacex_log) #:use-module (crates-io))

(define-public crate-spacex_log-0.1.0 (c (n "spacex_log") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "106fab1jbwh6qkw16qf1qwgw6s6x9yqpb2qk0bar3ykbap35c1c1")))

(define-public crate-spacex_log-0.1.1 (c (n "spacex_log") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1vrljfhwrq2sdbcg8fkzi74rx8df5wv8lhsybwggclkcc4dn88kg")))

