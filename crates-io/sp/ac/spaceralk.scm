(define-module (crates-io sp ac spaceralk) #:use-module (crates-io))

(define-public crate-spaceralk-0.1.0 (c (n "spaceralk") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 0)) (d (n "hyper") (r "^0.10.4") (d #t) (k 0)) (d (n "serde") (r "^0.9.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.9.5") (d #t) (k 0)) (d (n "xmlrpc") (r "^0.4.0") (d #t) (k 0)))) (h "06gjhfjyjbnkmkwfdjwd9ff6091chjgi5dnyd62fgbzfa1bgqh7z") (y #t)))

