(define-module (crates-io sp ac space-macro) #:use-module (crates-io))

(define-public crate-space-macro-0.1.0 (c (n "space-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1a419mnakj7xqgd28krsqa7xn79ahj64fg8dx1fan1hpscfb7hsl")))

(define-public crate-space-macro-0.2.0 (c (n "space-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "187mkr9fr2wbjn5wm7wmlzxxbjyh16359ka44s6rkwxyhnmvavsz")))

(define-public crate-space-macro-0.2.1 (c (n "space-macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1n2m8vxc61yxhkgrvis8zxlb6mmbq9z76b7y98wn10pafwdbpjkb")))

