(define-module (crates-io sp ac spaced-review) #:use-module (crates-io))

(define-public crate-spaced-review-0.1.0 (c (n "spaced-review") (v "0.1.0") (h "00zj7s2i9h75322dpgm32vgf6qmnpscn6abh05l5qcv7c7hfh9y1")))

(define-public crate-spaced-review-0.1.1 (c (n "spaced-review") (v "0.1.1") (h "1qk4l1hx6dhk73jc2rxg6gv57iyd6w4zg06xicxq6739r38c8vrf")))

