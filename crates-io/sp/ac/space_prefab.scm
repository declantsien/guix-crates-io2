(define-module (crates-io sp ac space_prefab) #:use-module (crates-io))

(define-public crate-space_prefab-0.3.0 (c (n "space_prefab") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.21") (f (quote ("bevy_pbr" "highlight_changes"))) (d #t) (k 0)) (d (n "bevy-scene-hook") (r "^9") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "space_shared") (r "^0.3.0") (d #t) (k 0)) (d (n "space_undo") (r "^0.3.0") (d #t) (k 0)))) (h "1h6238yhxa0fdx23x218yj49zwn3fkj0aks4nh0c2k5nwjg428ag")))

(define-public crate-space_prefab-0.3.1 (c (n "space_prefab") (v "0.3.1") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.21") (f (quote ("bevy_pbr" "highlight_changes"))) (d #t) (k 0)) (d (n "bevy-scene-hook") (r "^9") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "space_shared") (r "^0.3.1") (d #t) (k 0)) (d (n "space_undo") (r "^0.3.1") (d #t) (k 0)))) (h "1fm4pd5jj1dsn8znbnkq6j464fd1k8v0lzx6x1a7s2v6qmbr3px8")))

