(define-module (crates-io sp ac spacepacket) #:use-module (crates-io))

(define-public crate-spacepacket-0.1.0 (c (n "spacepacket") (v "0.1.0") (d (list (d (n "asynchronous-codec") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "~1.4") (d #t) (k 0)) (d (n "bytes") (r "~1.4") (o #t) (d #t) (k 0)) (d (n "crc") (r "^3.0") (o #t) (d #t) (k 0)) (d (n "futures") (r "~0.3") (d #t) (k 2)) (d (n "rstest") (r "~0.15") (d #t) (k 2)) (d (n "tokio-util") (r "~0.7") (f (quote ("codec"))) (o #t) (d #t) (k 0)))) (h "0dsk3i0q0825p27vk052cc41rnw1lk3gw2vqx89hipmq8kfglbcl") (f (quote (("tokio-codec" "bytes" "tokio-util/codec") ("async-codec" "asynchronous-codec" "bytes")))) (s 2) (e (quote (("crc" "dep:crc")))) (r "1.63")))

