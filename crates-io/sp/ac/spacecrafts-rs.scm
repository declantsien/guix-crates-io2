(define-module (crates-io sp ac spacecrafts-rs) #:use-module (crates-io))

(define-public crate-spacecrafts-rs-0.1.0-beta.1 (c (n "spacecrafts-rs") (v "0.1.0-beta.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "0d6x2735dy97v7ckc5xfqipii9rw2xd2zjjn8y1kr61qbx33jszv") (y #t)))

