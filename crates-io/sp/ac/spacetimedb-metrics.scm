(define-module (crates-io sp ac spacetimedb-metrics) #:use-module (crates-io))

(define-public crate-spacetimedb-metrics-0.8.0 (c (n "spacetimedb-metrics") (v "0.8.0") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.13.0") (d #t) (k 0)))) (h "0yiz2c7975zdpf5p7d7v7j7a209i7bn9z3ljcwz34m34m3x73lm3")))

(define-public crate-spacetimedb-metrics-0.8.1 (c (n "spacetimedb-metrics") (v "0.8.1") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.13.0") (d #t) (k 0)))) (h "0n2kbcgq2fmm25vibnrjl8qdlziax6l6w4cfqfhsv1x04c0k8znr")))

(define-public crate-spacetimedb-metrics-0.8.2 (c (n "spacetimedb-metrics") (v "0.8.2") (d (list (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.13.0") (d #t) (k 0)))) (h "0l7bakyw3adcrf8sq818x61hpflh548pxyxvv93k5yswxfvqmcxn")))

(define-public crate-spacetimedb-metrics-0.9.0 (c (n "spacetimedb-metrics") (v "0.9.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.13.0") (d #t) (k 0)))) (h "1c0s8ywi78jbc9jlbhvi4z67yc5sj23yni4d6gap36k9k5q00pym")))

(define-public crate-spacetimedb-metrics-0.9.1 (c (n "spacetimedb-metrics") (v "0.9.1") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.13.0") (d #t) (k 0)))) (h "08wcxf2846bal36zlffa9mm8818ljv163xlssn8xsvxid8nrl962")))

(define-public crate-spacetimedb-metrics-0.9.2 (c (n "spacetimedb-metrics") (v "0.9.2") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.13.0") (d #t) (k 0)))) (h "09wrba0d36sc513cbik3bpqc9df8mfr2qqarh63jjdj328hl8q7d")))

