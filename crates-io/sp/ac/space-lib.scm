(define-module (crates-io sp ac space-lib) #:use-module (crates-io))

(define-public crate-space-lib-0.1.0 (c (n "space-lib") (v "0.1.0") (d (list (d (n "rmp-serde") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0q8gq1sl8ym740fscc1iwd705h3psaqyg59d2pichg0i0hkymx1n")))

(define-public crate-space-lib-0.1.1 (c (n "space-lib") (v "0.1.1") (d (list (d (n "rmp-serde") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zr5rdnv51xg5c88dlrnajnfs7rlcmib66a7c412ix6zx4idlj44")))

(define-public crate-space-lib-0.2.0 (c (n "space-lib") (v "0.2.0") (d (list (d (n "rmp-serde") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0y35ibmmwrsrw0f58s7c325k37g4njni83a46ysh0dhrc6cxclg8")))

(define-public crate-space-lib-0.3.0 (c (n "space-lib") (v "0.3.0") (d (list (d (n "rmp-serde") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19sr49fs6zqw32a92zc0s5aycgi18kys0ks07kr3hcwn0lfw38la")))

(define-public crate-space-lib-0.4.0 (c (n "space-lib") (v "0.4.0") (d (list (d (n "rmp-serde") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "space-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0n97jrv028ks7gl6d146wmr3ab4nzq68g21n0ziw9amz4n5lzcxy")))

(define-public crate-space-lib-0.5.0 (c (n "space-lib") (v "0.5.0") (d (list (d (n "rmp-serde") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "space-macro") (r "^0.2") (d #t) (k 0)))) (h "1v5kxyhkycghf6g8dxapmv6amnc5rpqmsg3a6n1a86jcr0w0i514") (f (quote (("json" "serde_json"))))))

(define-public crate-space-lib-0.5.1 (c (n "space-lib") (v "0.5.1") (d (list (d (n "rmp-serde") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "space-macro") (r "^0.2") (d #t) (k 0)))) (h "11niirnkqf9imv2xs99vxfmqq4m7xpya0xsxr3ra3ra29irilyxn") (f (quote (("json" "serde_json"))))))

