(define-module (crates-io sp ac spacerocks) #:use-module (crates-io))

(define-public crate-spacerocks-0.1.0 (c (n "spacerocks") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)))) (h "0kw9r5lr8idz69ss7im6kxbm8b98b6npqv0jc64z6nv5clwpa33m")))

(define-public crate-spacerocks-0.1.1 (c (n "spacerocks") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)))) (h "02k14zf0r1pamnsnzpxpd4dxdin4pmk2cgj4rli8347vylc10vl5")))

(define-public crate-spacerocks-0.1.2 (c (n "spacerocks") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)))) (h "1iz80cq2sn1gs6bvp096hwkrz6mg6l13jr8ixr9jdjhygbv6m81m")))

(define-public crate-spacerocks-0.1.3 (c (n "spacerocks") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "rust-spice") (r "^0.7.4") (d #t) (k 0)))) (h "0qwpr80lzws81hq2a07xpdnbniqr5qvd7vi1k124mzx3pphc9a40")))

(define-public crate-spacerocks-0.1.4 (c (n "spacerocks") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "rust-spice") (r "^0.7.4") (f (quote ("noclang"))) (t "x86_64-unknown-linux-gnu") (k 0)) (d (n "rust-spice") (r "^0.7.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0qpjx49d8skz52gvl1l1rvk0wyywxj6yzvax5rq8xir4z0rbaphz")))

