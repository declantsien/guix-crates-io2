(define-module (crates-io sp ac spacebadgers-utils) #:use-module (crates-io))

(define-public crate-spacebadgers-utils-1.0.0 (c (n "spacebadgers-utils") (v "1.0.0") (d (list (d (n "criterion") (r "^0.5.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.2") (f (quote ("std" "perf" "unicode-perl"))) (k 0)))) (h "18iraj7nhfzsymyygkwvj7yad3g6d7d8hj0ymxf1vgdb42ncb0pk")))

