(define-module (crates-io sp ac spaces) #:use-module (crates-io))

(define-public crate-spaces-0.0.1 (c (n "spaces") (v "0.0.1") (d (list (d (n "ndarray") (r "^0.11") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "rusty-machine") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 0)))) (h "1n1084azz8drxxyjwxfv1nqrw8j5lwsazpgzkp1d71bwjvnbznc7")))

(define-public crate-spaces-0.1.0 (c (n "spaces") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.11") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "rusty-machine") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 0)))) (h "1yfxzbay2m2sqsl4psxvnvafh6gjvcsdcbh52p858m9p629l4raw")))

(define-public crate-spaces-1.0.0 (c (n "spaces") (v "1.0.0") (d (list (d (n "ndarray") (r "^0.11") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1k7hlhxf5hwbj0wvx6rw9ij81yzalk5cp6xvl6850xi1sld2jvwl")))

(define-public crate-spaces-2.0.0 (c (n "spaces") (v "2.0.0") (d (list (d (n "ndarray") (r "^0.11") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "115h98yw4gfaqqjgh8i1xn9gkk1qf9jwxl7chgv752xg9pzig881")))

(define-public crate-spaces-2.1.0 (c (n "spaces") (v "2.1.0") (d (list (d (n "ndarray") (r "^0.11") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0s6cqg7m2zdyw8p5r8hsp7g0sl6d92if6alg0hxb0b4v7r6k7ch7")))

(define-public crate-spaces-2.2.0 (c (n "spaces") (v "2.2.0") (d (list (d (n "ndarray") (r "^0.11") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1fahpp6a65s394f0wp0cff50q3miqymkqa22njbp0m5ayfn16i0h")))

(define-public crate-spaces-2.2.1 (c (n "spaces") (v "2.2.1") (d (list (d (n "ndarray") (r "^0.12") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1vayl0pmpxmdqdqib0ryvis153agw04jbqnrdawb3sdmra7fn91p")))

(define-public crate-spaces-3.0.0 (c (n "spaces") (v "3.0.0") (d (list (d (n "ndarray") (r "^0.12") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0k7zsjvm4bwladzigkmlvj83x6qnk35jbsj85srqnv5c1nqsvz9g")))

(define-public crate-spaces-3.1.0 (c (n "spaces") (v "3.1.0") (d (list (d (n "ndarray") (r "^0.12") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1dl80mfysxl0fwbx407s83zrp3rffljw136hy53pqx3yxqswhrpx")))

(define-public crate-spaces-3.1.1 (c (n "spaces") (v "3.1.1") (d (list (d (n "ndarray") (r "^0.12") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0k68y4allqv70jn8xqbd9r0n56bk8dpdyvlm2bicb8p5qamisj1l")))

(define-public crate-spaces-3.2.0 (c (n "spaces") (v "3.2.0") (d (list (d (n "ndarray") (r "^0.12") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1kg9kldqbvp71a9vzj0vln20dxp7fqvddka0j43ix883md9i8bl5")))

(define-public crate-spaces-3.3.0 (c (n "spaces") (v "3.3.0") (d (list (d (n "ndarray") (r "^0.12") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1d5w1imdxf4kis141vf9rmfw3x98sqp94wsks4171af2mjyz1x6c")))

(define-public crate-spaces-3.4.0 (c (n "spaces") (v "3.4.0") (d (list (d (n "ndarray") (r "^0.12") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1z7bmqz6w5wh1k6s4q240iyc6cax1sh8kalx1asyp0p92bsyn8bi")))

(define-public crate-spaces-4.0.0 (c (n "spaces") (v "4.0.0") (d (list (d (n "ndarray") (r "^0.12") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "113skhzvyx2pnnfxzpqkyjm777ybrx50hclcxpa9jx3kq2hwdlm8")))

(define-public crate-spaces-4.1.0 (c (n "spaces") (v "4.1.0") (d (list (d (n "ndarray") (r "^0.12") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1h3ak3liwmgzw9jqvbr095add6nlhh1246fnan6qamgy678sxkxw")))

(define-public crate-spaces-4.2.0 (c (n "spaces") (v "4.2.0") (d (list (d (n "ndarray") (r "^0.12") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0kkhkfjrp8jiq2filxql7nz18jpgzsbmfgndvpkcypwz57cjglb6")))

(define-public crate-spaces-4.3.0 (c (n "spaces") (v "4.3.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0i6wg2wvjqg1mdi1alf1cbi5xq9fswjprv1k1ykiybnqlnzbsarg")))

(define-public crate-spaces-4.4.0 (c (n "spaces") (v "4.4.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0h1f9adqmwdqa5k0dpmcld081w5kp2prjlgdhcrbwn061y63rdn8")))

(define-public crate-spaces-4.5.0 (c (n "spaces") (v "4.5.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0jf2mrchrr1kycyirfp1i05bs2siqscpkqbg1ly6aq37cr0gq0im")))

(define-public crate-spaces-4.6.0 (c (n "spaces") (v "4.6.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1mr0kx0x34hzqm34fsx8wjilczybs0w8jr8fbksn6lggmbwccqqd")))

(define-public crate-spaces-4.7.0 (c (n "spaces") (v "4.7.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1fz4i97hmy57ipkhskwg2zl09ql7qhv47ykx00pyi9340s5n477m")))

(define-public crate-spaces-4.7.1 (c (n "spaces") (v "4.7.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0qmnjd7y5i6vkzw940xbbq44idaaliqps2fpfaiqwlzddvqki80g")))

(define-public crate-spaces-5.0.0 (c (n "spaces") (v "5.0.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1srw35gf5ns0w92y8j1cc5bf0rxw6qhcsrs2hk5f3kpgwr4fz5sq") (f (quote (("serialize" "serde") ("default"))))))

(define-public crate-spaces-6.0.0 (c (n "spaces") (v "6.0.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1s549g591xgrs43fdgdnprwdpc5i1aiyz9dzj2jmgw3n2xfbilwm") (f (quote (("serialize" "serde") ("default"))))))

