(define-module (crates-io sp ac space-time) #:use-module (crates-io))

(define-public crate-space-time-0.1.0 (c (n "space-time") (v "0.1.0") (d (list (d (n "num-integer") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "time") (r "^0.2") (k 0)))) (h "0vi8xp0hd25m5x1iwv4b3rikkwcvx5jcb0w992y0bb98k8a23pqh")))

(define-public crate-space-time-0.1.1 (c (n "space-time") (v "0.1.1") (d (list (d (n "num-integer") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "146pc879xd2pxilfqplv3a28500f6f4ibcniv5x22fsasffg15vv")))

(define-public crate-space-time-0.1.2 (c (n "space-time") (v "0.1.2") (d (list (d (n "num-integer") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "1xl6z6xsrd773iq0y4w1s8q594n6j9p39qn5g3ymdwbl9gwvhdbc")))

(define-public crate-space-time-0.1.3 (c (n "space-time") (v "0.1.3") (d (list (d (n "num-integer") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "1j2954x89i46lpy7xsygj0j68vgzq31qzmswbxibmnjmx0m1wby1")))

(define-public crate-space-time-0.2.0 (c (n "space-time") (v "0.2.0") (d (list (d (n "num-integer") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)))) (h "09782v26hmdr6r26g9sanfxv1mlkd29s5d7l6xa02lgh77wwwk1s")))

