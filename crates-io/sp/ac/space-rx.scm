(define-module (crates-io sp ac space-rx) #:use-module (crates-io))

(define-public crate-space-rx-0.1.0 (c (n "space-rx") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.22") (d #t) (k 0)) (d (n "url") (r "^1.7.1") (d #t) (k 0)))) (h "095m4vj6x478jzl0pz8i7cfqdfbgafgr79cwfq4n67b0zg63p69s")))

(define-public crate-space-rx-0.2.0 (c (n "space-rx") (v "0.2.0") (d (list (d (n "derive_builder") (r "^0.6.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.76") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.76") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)) (d (n "url") (r "^1.7.1") (d #t) (k 0)))) (h "0m91zfskmqjcb0bdqwaza443gmmif4m94pww859mk3qbyddzqgyb")))

