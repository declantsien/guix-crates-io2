(define-module (crates-io sp ac spacenav-plus) #:use-module (crates-io))

(define-public crate-spacenav-plus-0.1.0 (c (n "spacenav-plus") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libspnav-bindings") (r "^0.1.0") (d #t) (k 0)))) (h "1wvf9cnxsyh53ldqynkfp68m57kb3s6xr3fmfs5fj6fqjbs2f13v")))

(define-public crate-spacenav-plus-0.1.1 (c (n "spacenav-plus") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libspnav-bindings") (r "^0.1.0") (d #t) (k 0)))) (h "13s44g0kg4p9rai0jxn41gi7dz48z8zx04vxqvxmywd3mxgf3siv")))

