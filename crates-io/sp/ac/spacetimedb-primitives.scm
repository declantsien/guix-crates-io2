(define-module (crates-io sp ac spacetimedb-primitives) #:use-module (crates-io))

(define-public crate-spacetimedb-primitives-0.7.2 (c (n "spacetimedb-primitives") (v "0.7.2") (d (list (d (n "nonempty") (r "^0.8.1") (d #t) (k 0)))) (h "0nvz3b6gjsr0fcp8c21886i0dw94fwncf5jk0385jlf91a2flfqp")))

(define-public crate-spacetimedb-primitives-0.7.3 (c (n "spacetimedb-primitives") (v "0.7.3") (d (list (d (n "nonempty") (r "^0.8.1") (d #t) (k 0)))) (h "1szvm9lp0854xj227fx70vcyv38jyl0pm8r50gdj0zm5fmsr1z74")))

(define-public crate-spacetimedb-primitives-0.8.0 (c (n "spacetimedb-primitives") (v "0.8.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "nonempty") (r "^0.8.1") (d #t) (k 0)))) (h "1jywsads2hvxs9baq9q68ahrqpxhq657lvhagf4f68r7y66fh9hz")))

(define-public crate-spacetimedb-primitives-0.8.1 (c (n "spacetimedb-primitives") (v "0.8.1") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "nonempty") (r "^0.8.1") (d #t) (k 0)))) (h "07w98pxga936ihsvqn7dsh8ywvw3692v46qyk6sqgf6bblklp0cm")))

(define-public crate-spacetimedb-primitives-0.8.2 (c (n "spacetimedb-primitives") (v "0.8.2") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "either") (r "^1.9") (d #t) (k 0)) (d (n "proptest") (r "^1.4") (d #t) (k 2)))) (h "04j9m06l2ckpa7cprvwzby5q36sx46jcrbldqvmxi5q9dk424qnk")))

(define-public crate-spacetimedb-primitives-0.9.0 (c (n "spacetimedb-primitives") (v "0.9.0") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "either") (r "^1.9") (d #t) (k 0)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "spacetimedb-data-structures") (r "^0.9.0") (d #t) (k 0)))) (h "16lmj92q43kw7hp0vs25yvfhph1ln53ayfj3bjb4w8zpgnfzsl43")))

(define-public crate-spacetimedb-primitives-0.9.1 (c (n "spacetimedb-primitives") (v "0.9.1") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "either") (r "^1.9") (d #t) (k 0)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "spacetimedb-data-structures") (r "^0.9.1") (d #t) (k 0)))) (h "1lyqqkkij43acfsb83i4jx3sbsnvpsnr75ava2ymib9plibhqf1w")))

(define-public crate-spacetimedb-primitives-0.9.2 (c (n "spacetimedb-primitives") (v "0.9.2") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "either") (r "^1.9") (d #t) (k 0)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "spacetimedb-data-structures") (r "^0.9.2") (d #t) (k 0)))) (h "1fpf0pmk0s2vnw9zjw28g7gvmjdlzmlfgvnb03f7kkygslfvk7cb")))

