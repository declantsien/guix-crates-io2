(define-module (crates-io sp ac spacegate-ext-axum) #:use-module (crates-io))

(define-public crate-spacegate-ext-axum-0.2.0-alpha.1 (c (n "spacegate-ext-axum") (v "0.2.0-alpha.1") (d (list (d (n "axum") (r "^0.7.5") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.8") (f (quote ("io"))) (d #t) (k 0)) (d (n "tower") (r "^0.4") (d #t) (k 0)) (d (n "tower-http") (r "^0.5") (f (quote ("trace"))) (d #t) (k 0)) (d (n "tracing") (r "^0") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter" "env-filter"))) (d #t) (k 0)))) (h "0xb5bsh0p8dpwn5b8gh7s74lb5xkndm2abk5s20d6anw8jz9bpbj") (r "1.76")))

