(define-module (crates-io sp ac spacey) #:use-module (crates-io))

(define-public crate-spacey-1.0.0 (c (n "spacey") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.5") (d #t) (k 0)) (d (n "getch") (r "^0.3.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "1752nbg04bqnjmi1kx1z147lpy8vqgw9xwjk7swyghy1an1vzca1")))

(define-public crate-spacey-1.0.1 (c (n "spacey") (v "1.0.1") (d (list (d (n "clap") (r "^3.2.5") (d #t) (k 0)) (d (n "getch") (r "^0.3.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "0q6by0brgnql2jw3kjv284sf065w1dxqnikqmf07h66r22ll7kn0")))

(define-public crate-spacey-1.1.0 (c (n "spacey") (v "1.1.0") (d (list (d (n "clap") (r "^3.2.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "getch") (r "^0.3.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "14myvdgm624vld42xiz12layk52s7sk6wwdfqk4m4h2x0sazxkpc")))

(define-public crate-spacey-1.2.0 (c (n "spacey") (v "1.2.0") (d (list (d (n "clap") (r "^3.2.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "getch") (r "^0.3.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)))) (h "0sgkg84kkpicr0j6d9dwpcmcbmsq9mc3m6m42gn9gx5lp3k1dmm7")))

