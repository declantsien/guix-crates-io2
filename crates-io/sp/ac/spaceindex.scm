(define-module (crates-io sp ac spaceindex) #:use-module (crates-io))

(define-public crate-spaceindex-0.1.0 (c (n "spaceindex") (v "0.1.0") (h "1spfygmhj5v3wkngn1y0gyp64sbcmrggr7hcafqafqm9xq30pvrp")))

(define-public crate-spaceindex-0.2.0 (c (n "spaceindex") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "generational-arena") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "imageproc") (r "^0.19") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rustc-ap-graphviz") (r "^643.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0pm7xa195iybhvgm3qkyl3fd0ln6n4frq608bpllxaa1am4wsp6b")))

(define-public crate-spaceindex-0.2.1 (c (n "spaceindex") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "generational-arena") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "imageproc") (r "^0.19") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rustc-ap-graphviz") (r "^643.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qgf333y5z0b8wivk3z60r1jn0qlsx20f5a68n8ja76wgr729pb5")))

(define-public crate-spaceindex-0.2.2 (c (n "spaceindex") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "generational-arena") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "imageproc") (r "^0.19") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rustc-ap-graphviz") (r "^643.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1pq280449n40p2a8kzz12l8dg1nsh7432m6h0fcxi7f37cb2b0mk")))

(define-public crate-spaceindex-0.3.0 (c (n "spaceindex") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "generational-arena") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "imageproc") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rustc-ap-graphviz") (r "^645.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03m901jmsafivg4cxkn5c8gfp5dyjpmy2p6xjd9h91ircmqzjdkw") (f (quote (("imagegen" "image" "imageproc") ("graphviz" "rustc-ap-graphviz") ("default" "imagegen" "graphviz"))))))

