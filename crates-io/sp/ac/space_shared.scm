(define-module (crates-io sp ac space_shared) #:use-module (crates-io))

(define-public crate-space_shared-0.3.0 (c (n "space_shared") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.21") (f (quote ("bevy_pbr" "highlight_changes"))) (d #t) (k 0)) (d (n "egui_file") (r "^0.11") (d #t) (k 0)))) (h "106aq6ij7gm1fiyvs75bqp6a53p89i7gf7qr1rbls45jyc5qjlgp")))

(define-public crate-space_shared-0.3.1 (c (n "space_shared") (v "0.3.1") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.21") (f (quote ("bevy_pbr" "highlight_changes"))) (d #t) (k 0)) (d (n "egui_file") (r "^0.11") (d #t) (k 0)))) (h "0x75hkjxvx875dnbiqkby78nzfjsw62jy66s452nw2p06dmvayh0")))

