(define-module (crates-io sp ac spaceapi) #:use-module (crates-io))

(define-public crate-spaceapi-0.1.0 (c (n "spaceapi") (v "0.1.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "13cw7fyz270xjnjnhmha01fcbykd7dwv3ps4dzy2a4zhw0q1h2h4")))

(define-public crate-spaceapi-0.1.1 (c (n "spaceapi") (v "0.1.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1iigvv3xxv4qafkizvhawqxxfn24m18jmqq3xgg7vjd80lnmwaya")))

(define-public crate-spaceapi-0.2.0 (c (n "spaceapi") (v "0.2.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "07ykpq2mbcd0ly16c6p44sfwcgv6rnfj3kflh885iw8lzsagw3z3")))

(define-public crate-spaceapi-0.3.0 (c (n "spaceapi") (v "0.3.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "05h955b850nsvwz025yrk7afxxah25kx2i82p25xsg2w6knbjrsx")))

(define-public crate-spaceapi-0.3.1 (c (n "spaceapi") (v "0.3.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0gzp3ccjkaxmijkfnk1cx46kgryniypxg3jlq1b3w3d78xs2df7j")))

(define-public crate-spaceapi-0.4.0 (c (n "spaceapi") (v "0.4.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "0zzlq5yifj83yq55vfm2c06cysafmcczy0ygp732daysfdrd1258")))

(define-public crate-spaceapi-0.4.1 (c (n "spaceapi") (v "0.4.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "01ymxfaz5gb7dhg3nmfch1wc9hhc28ir3wvn9wdikwwm67yw1ys4")))

(define-public crate-spaceapi-0.4.2 (c (n "spaceapi") (v "0.4.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "09nh2rkrd0qxxbpa9w1ak1887dsjksvidijay2y94p77rfidldng")))

(define-public crate-spaceapi-0.5.0 (c (n "spaceapi") (v "0.5.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vp2rb3h86yib0w4p0h157mdilvg60gbb27j3vfp0z4zscg86fmv")))

(define-public crate-spaceapi-0.5.1 (c (n "spaceapi") (v "0.5.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02n3a2p2jsl9m949g1rvdvhy729j0s4hws1gp28rrj8z6qnfz3j1")))

(define-public crate-spaceapi-0.6.0 (c (n "spaceapi") (v "0.6.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1smgp1jfjp0mix84v0fghvwyikq784i89jfjhj330fjr32qdsik4")))

(define-public crate-spaceapi-0.7.0 (c (n "spaceapi") (v "0.7.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wxaghn8020pdzwjhmvvdw17w0jiqw4bq5l61nl652ygyplm7vpq")))

(define-public crate-spaceapi-0.8.0 (c (n "spaceapi") (v "0.8.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0v9npb8fs14nx02hwlsckypbws6fw38cpj4i42bddsmb8gfm80z0")))

(define-public crate-spaceapi-0.8.1 (c (n "spaceapi") (v "0.8.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1s5w16xndzn6g66hmxaip1vqvl7fn8ii7sm7pd2inwybqdch3c6z")))

(define-public crate-spaceapi-0.9.0 (c (n "spaceapi") (v "0.9.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10ckj2c9sr77p226f3s0fqb6cisl5srmkjf2icnddy4w57fllz61") (r "1.56")))

