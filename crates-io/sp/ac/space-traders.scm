(define-module (crates-io sp ac space-traders) #:use-module (crates-io))

(define-public crate-space-traders-0.1.0 (c (n "space-traders") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "0yj3jmpdg6c48f7b8r6mkpmwqdn7kpp1nfm5227r4kzvi8k8jayn")))

(define-public crate-space-traders-0.1.1 (c (n "space-traders") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "0km04hmg0m8sl7zvppf7rfqkxn170k0mmxi5mkz902pz9wlgfpy0")))

(define-public crate-space-traders-0.1.2-3ff410c (c (n "space-traders") (v "0.1.2-3ff410c") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "07810dnkmycd8b322p2gakw52a40mbdr7n287swb27l100m71k0y")))

