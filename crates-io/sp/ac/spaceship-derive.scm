(define-module (crates-io sp ac spaceship-derive) #:use-module (crates-io))

(define-public crate-spaceship-derive-0.1.0 (c (n "spaceship-derive") (v "0.1.0") (d (list (d (n "deluxe") (r "^0.5.0") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (d #t) (k 0)))) (h "06j1wi1vlpjbbvrs3ykyvgvhslczya4sqxkrmdx6gqv9byb23wg1")))

