(define-module (crates-io sp ac spaceapi-dezentrale-server) #:use-module (crates-io))

(define-public crate-spaceapi-dezentrale-server-0.6.0 (c (n "spaceapi-dezentrale-server") (v "0.6.0") (d (list (d (n "iron") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.8.7") (d #t) (k 0)) (d (n "redis") (r "^0.21.1") (f (quote ("r2d2"))) (d #t) (k 0)) (d (n "router") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "spaceapi-dezentrale") (r "^0.8.999") (d #t) (k 0)) (d (n "urlencoded") (r "^0.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)))) (h "18di53bsi46dnrbscw455yv2bkj1d5idp6m4dwmqmfs1cv0dcshd")))

