(define-module (crates-io sp ac spaceship) #:use-module (crates-io))

(define-public crate-spaceship-0.1.0 (c (n "spaceship") (v "0.1.0") (h "04894i0iqhqx88cpkisjvwgj8ybvcw79nwlqnmlb2a8xa135lic3")))

(define-public crate-spaceship-0.1.1 (c (n "spaceship") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.74") (d #t) (k 0)) (d (n "axum") (r "^0.6.20") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spaceship-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.31.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1crnzkvxcc85i7n8ajfj47xhi51pkldp2pxlg78x3lcx989fkq0g")))

