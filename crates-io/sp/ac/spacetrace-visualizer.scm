(define-module (crates-io sp ac spacetrace-visualizer) #:use-module (crates-io))

(define-public crate-spacetrace-visualizer-0.1.0 (c (n "spacetrace-visualizer") (v "0.1.0") (d (list (d (n "autojson") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "indicatif") (r "^0.16") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "spacetrace") (r "^0.1") (d #t) (k 0)) (d (n "vek") (r "^0.15") (f (quote ("libm" "serde"))) (k 0)))) (h "0nzdlcxx13yhmfgvrz474j5yqp20xdzlmm4dg4q6hdhj9yckga60")))

