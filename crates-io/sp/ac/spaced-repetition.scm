(define-module (crates-io sp ac spaced-repetition) #:use-module (crates-io))

(define-public crate-spaced-repetition-1.0.0 (c (n "spaced-repetition") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "179y038002756fvz2vhjh1zvx5jzrgwqr8s9jg0l36izr5j63h14") (r "1.58.1")))

(define-public crate-spaced-repetition-1.1.0 (c (n "spaced-repetition") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ags59nn4jik2phc9bpnrs6a500b21sc0lmkymn69cs4h9vby8r1") (s 2) (e (quote (("serde" "dep:serde" "chrono/serde")))) (r "1.60.0")))

