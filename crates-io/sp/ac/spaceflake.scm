(define-module (crates-io sp ac spaceflake) #:use-module (crates-io))

(define-public crate-spaceflake-1.0.0 (c (n "spaceflake") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0hf4sy4cwixb6f7rz3m6r5bshyisc7rgs3ndws0mrs5d6405fvbw")))

(define-public crate-spaceflake-1.0.1 (c (n "spaceflake") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1l0w2m63pvfix0krgxh4661nkx0nbsmlyimimmbx5psszgjs0xf4")))

(define-public crate-spaceflake-1.0.2 (c (n "spaceflake") (v "1.0.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0crcgbpdwry5y3s4mw0jamz1ylxj03f44jv2b2ja9dq1jmv2vx4i")))

(define-public crate-spaceflake-1.1.0 (c (n "spaceflake") (v "1.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "09sjnysyrnnivmjxn99cvlllalxyl5l19b4jqjnnx9smz2damajb")))

(define-public crate-spaceflake-1.1.1 (c (n "spaceflake") (v "1.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0n8mcsv3nv58zk06vlrl3mkkqi5r7jmzlr4lcz2rk4kmpdym587w")))

