(define-module (crates-io sp ac spacex-api-wrapper) #:use-module (crates-io))

(define-public crate-spacex-api-wrapper-0.1.0 (c (n "spacex-api-wrapper") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.23") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.71") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)))) (h "06xpcj8pqaly2sbny4kqpfs7v1g4fvhj0g9hlp0vdl5q3z21ad8b")))

(define-public crate-spacex-api-wrapper-0.2.0 (c (n "spacex-api-wrapper") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.23") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.71") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.26") (d #t) (k 0)))) (h "06cb83qwhhqk2233n6fjsn2cvcgcpyvbcixadbwjchkjhwmv7i2a")))

