(define-module (crates-io sp ac space_undo) #:use-module (crates-io))

(define-public crate-space_undo-0.3.0 (c (n "space_undo") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "space_shared") (r "^0.3.0") (d #t) (k 0)))) (h "1nrb9m2vk8zb9s8280fb05l7h2m82s8x36jw6r18ln5h28s4rs4k")))

(define-public crate-space_undo-0.3.1 (c (n "space_undo") (v "0.3.1") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "pretty-type-name") (r "^1.0.1") (d #t) (k 0)) (d (n "space_shared") (r "^0.3.1") (d #t) (k 0)))) (h "0zi1qsyvza878r7drakjb9wz1ff7zd7mwqiyzjvdspxdzvw0b499")))

