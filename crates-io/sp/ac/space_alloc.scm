(define-module (crates-io sp ac space_alloc) #:use-module (crates-io))

(define-public crate-space_alloc-0.1.0 (c (n "space_alloc") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "nonzero_ext") (r "^0.3.0") (d #t) (k 2)))) (h "170j8dd6j6zmpb0bgyp729yqn1h8wkim8s1fz6rz48bvfizf6f8q")))

(define-public crate-space_alloc-0.1.1 (c (n "space_alloc") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "nonzero_ext") (r "^0.3.0") (d #t) (k 2)))) (h "04yxxbyyjnfg9frykzf8q2r10fz6sq1dvkvnyamldlbzg6z3mq81")))

