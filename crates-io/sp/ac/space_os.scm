(define-module (crates-io sp ac space_os) #:use-module (crates-io))

(define-public crate-space_os-1.0.0 (c (n "space_os") (v "1.0.0") (d (list (d (n "find_folder") (r "^0.3.0") (d #t) (k 0)) (d (n "fps_counter") (r "^1.0.0") (d #t) (k 0)) (d (n "piston") (r "^0.37.0") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.26.0") (d #t) (k 0)) (d (n "piston2d-opengl_graphics") (r "^0.53.0") (d #t) (k 0)) (d (n "piston_window") (r "^0.80.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "012cjmdyjylpav9p2l942i410ks49wnd3yw198hcvcw18g4zqx8l")))

