(define-module (crates-io sp ac space_bevy_xpbd_plugin) #:use-module (crates-io))

(define-public crate-space_bevy_xpbd_plugin-0.3.0 (c (n "space_bevy_xpbd_plugin") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "bevy_xpbd_3d") (r "^0.3.2") (f (quote ("3d" "f32" "collider-from-mesh" "debug-plugin"))) (k 0)) (d (n "space_editor_ui") (r "^0.3.0") (f (quote ("persistence_editor"))) (d #t) (k 0)))) (h "13y4i7193bg4h0a1x7iz9lgmq1xbn2mp46kgjlryymdknjv3317h")))

(define-public crate-space_bevy_xpbd_plugin-0.3.1 (c (n "space_bevy_xpbd_plugin") (v "0.3.1") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "bevy_xpbd_3d") (r "^0.3.2") (f (quote ("3d" "f32" "collider-from-mesh" "debug-plugin"))) (k 0)) (d (n "space_editor_ui") (r "^0.3.1") (f (quote ("persistence_editor"))) (d #t) (k 0)))) (h "0adnnlb4wyl03v51acnc7sq71166p697b89rnaqznglmywwgnhsh")))

