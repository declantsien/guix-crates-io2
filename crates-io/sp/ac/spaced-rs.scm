(define-module (crates-io sp ac spaced-rs) #:use-module (crates-io))

(define-public crate-spaced-rs-0.1.0 (c (n "spaced-rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1wnkky397ss4qjkpza6pajzqdydpcnndzc6fyi1bj7srkmk8nng2")))

(define-public crate-spaced-rs-0.1.1 (c (n "spaced-rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0hbznpxislhq5hdh4db5zhdzh6bzb4r5raxbav1sy2j9xa1szcp3")))

(define-public crate-spaced-rs-0.2.0 (c (n "spaced-rs") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "163d5fakxyrymnhw190m37lx2ihhjwn7p6mwkqc7b8dr6pvhgizq")))

(define-public crate-spaced-rs-0.3.0 (c (n "spaced-rs") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1qw0v8vh388k2a00y1666q86w6jj612yhxinrnc0fpyx3zk3mvp6")))

(define-public crate-spaced-rs-0.3.1 (c (n "spaced-rs") (v "0.3.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rnc66n7zjxafji2xjrr6bybxm1c3kwv7vzp3180078ip1825ns8")))

