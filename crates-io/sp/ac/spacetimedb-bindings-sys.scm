(define-module (crates-io sp ac spacetimedb-bindings-sys) #:use-module (crates-io))

(define-public crate-spacetimedb-bindings-sys-0.2.1 (c (n "spacetimedb-bindings-sys") (v "0.2.1") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("custom"))) (o #t) (d #t) (k 0)))) (h "1fsxl4f6ylmk9j61h6nrih37cy3l2lrv0955wi5xd3cjbpjqbf8k")))

(define-public crate-spacetimedb-bindings-sys-0.2.2 (c (n "spacetimedb-bindings-sys") (v "0.2.2") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("custom"))) (o #t) (d #t) (k 0)))) (h "0j4smql42lqzlihbdl71404kpv2vvmgicxszy66vn84g5ncnfl36")))

(define-public crate-spacetimedb-bindings-sys-0.2.3 (c (n "spacetimedb-bindings-sys") (v "0.2.3") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("custom"))) (o #t) (d #t) (k 0)))) (h "16kvh1ib8r47qs1cxdx8qzs8lbwpczfh95m8c7gfg9vy6jqzq6n6")))

(define-public crate-spacetimedb-bindings-sys-0.3.0 (c (n "spacetimedb-bindings-sys") (v "0.3.0") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("custom"))) (o #t) (d #t) (k 0)))) (h "12z84klz9rrxcvyj5wkf7ynvxsjh64cg13j018ih2wllcj6h35iw")))

(define-public crate-spacetimedb-bindings-sys-0.3.1 (c (n "spacetimedb-bindings-sys") (v "0.3.1") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("custom"))) (o #t) (d #t) (k 0)))) (h "1wrfh0y9m81cn5bmsc9jkjibqklm6qp2hm0hhmah91rrfiqr1wc6")))

(define-public crate-spacetimedb-bindings-sys-0.3.2 (c (n "spacetimedb-bindings-sys") (v "0.3.2") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("custom"))) (o #t) (d #t) (k 0)))) (h "1k3k6k3wrvchbng40dpzjyfdybf217c7dl91b1grcgrzyifp6m9g")))

(define-public crate-spacetimedb-bindings-sys-0.3.3 (c (n "spacetimedb-bindings-sys") (v "0.3.3") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("custom"))) (o #t) (d #t) (k 0)))) (h "01y9grrgwvc2mdr19fr58ax043zi7zlk5svmsqx6llcxyxbjgik7")))

(define-public crate-spacetimedb-bindings-sys-0.3.4 (c (n "spacetimedb-bindings-sys") (v "0.3.4") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("custom"))) (o #t) (d #t) (k 0)))) (h "1cg3c2b6iayf9aq729zw3i7k8abjjzc4wwj9qmmia84wjpchz9qg")))

(define-public crate-spacetimedb-bindings-sys-0.4.0 (c (n "spacetimedb-bindings-sys") (v "0.4.0") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("custom"))) (o #t) (d #t) (k 0)))) (h "0h9yh6h5q6zfkbr68g446f84nn7bk54s6p87hwgciby01msv17by")))

(define-public crate-spacetimedb-bindings-sys-0.4.1 (c (n "spacetimedb-bindings-sys") (v "0.4.1") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("custom"))) (o #t) (d #t) (k 0)))) (h "04g0107l10b325gvhw227y110pd7d02vgv4xqii53fps1rnwpivx")))

(define-public crate-spacetimedb-bindings-sys-0.5.0 (c (n "spacetimedb-bindings-sys") (v "0.5.0") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("custom"))) (o #t) (d #t) (k 0)))) (h "16xxvz96r1ylaldyw54ly61wdpksk2jvz52x6pag4jiygkhzzqmx")))

(define-public crate-spacetimedb-bindings-sys-0.6.0 (c (n "spacetimedb-bindings-sys") (v "0.6.0") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("custom"))) (o #t) (d #t) (k 0)))) (h "18bbzd4sshzf7yly91a6mfcp89lfvd1rzlv9s1iwbxf080pqy1rn")))

(define-public crate-spacetimedb-bindings-sys-0.6.1 (c (n "spacetimedb-bindings-sys") (v "0.6.1") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("custom"))) (o #t) (d #t) (k 0)))) (h "1nczxagsnkq6p6l2w01wf5yfbi5fb1ck3kxiihy41slf7jngwj34")))

(define-public crate-spacetimedb-bindings-sys-0.7.0 (c (n "spacetimedb-bindings-sys") (v "0.7.0") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("custom"))) (o #t) (d #t) (k 0)))) (h "1rvqh12703cjm6zwrk3v3n1inakwspjskiap7j3pxwwld4p0x8rb")))

(define-public crate-spacetimedb-bindings-sys-0.7.1 (c (n "spacetimedb-bindings-sys") (v "0.7.1") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("custom"))) (o #t) (d #t) (k 0)))) (h "1j55p23914jm3ka189b1gfg2qgqxb5n2azl2imyh9mvndsg5q3wv")))

(define-public crate-spacetimedb-bindings-sys-0.7.2 (c (n "spacetimedb-bindings-sys") (v "0.7.2") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("custom"))) (o #t) (d #t) (k 0)) (d (n "spacetimedb-primitives") (r "^0.7.2") (d #t) (k 0)))) (h "1rcn0jvq7n1fs3s0fid9s9mvxkx0wsndj8qxz5r81mh20j517i6z")))

(define-public crate-spacetimedb-bindings-sys-0.7.3 (c (n "spacetimedb-bindings-sys") (v "0.7.3") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("custom"))) (o #t) (d #t) (k 0)) (d (n "spacetimedb-primitives") (r "^0.7.3") (d #t) (k 0)))) (h "1g5sc1r95km24l54pjsf1y8k6vkmnl984wxzr5b66hwj9mr0nh4c")))

(define-public crate-spacetimedb-bindings-sys-0.8.0 (c (n "spacetimedb-bindings-sys") (v "0.8.0") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("custom"))) (o #t) (d #t) (k 0)) (d (n "spacetimedb-primitives") (r "^0.8.0") (d #t) (k 0)))) (h "07ghfa34wsycr7czr9gvy4sjq8v152s089z18sc6kj1xs7hx3phz")))

(define-public crate-spacetimedb-bindings-sys-0.8.1 (c (n "spacetimedb-bindings-sys") (v "0.8.1") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("custom"))) (o #t) (d #t) (k 0)) (d (n "spacetimedb-primitives") (r "^0.8.1") (d #t) (k 0)))) (h "1aagix6f0y284rzc1f9frwk8rnrcmsadgl429w5wrl8g8w2z9pv9")))

(define-public crate-spacetimedb-bindings-sys-0.8.2 (c (n "spacetimedb-bindings-sys") (v "0.8.2") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("custom"))) (o #t) (d #t) (k 0)) (d (n "spacetimedb-primitives") (r "^0.8.2") (d #t) (k 0)))) (h "01b853qzr6qacvfavcrx94n2cpxill9shmqqlndp0cddsq6y0hhh") (r "1.74.0")))

(define-public crate-spacetimedb-bindings-sys-0.9.0 (c (n "spacetimedb-bindings-sys") (v "0.9.0") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("custom"))) (o #t) (d #t) (k 0)) (d (n "spacetimedb-primitives") (r "^0.9.0") (d #t) (k 0)))) (h "1rkcnq7jir46sw39h0hc6x6cmq40l9rv9cb82vl1j40jlgsn8vgm") (r "1.77.0")))

(define-public crate-spacetimedb-bindings-sys-0.9.1 (c (n "spacetimedb-bindings-sys") (v "0.9.1") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("custom"))) (o #t) (d #t) (k 0)) (d (n "spacetimedb-primitives") (r "^0.9.1") (d #t) (k 0)))) (h "1lw5p2qmccwcvps2w699v1zwnadg3bsv6d4gfbddiiyn1vhamldf") (r "1.77.0")))

(define-public crate-spacetimedb-bindings-sys-0.9.2 (c (n "spacetimedb-bindings-sys") (v "0.9.2") (d (list (d (n "getrandom") (r "^0.2.7") (f (quote ("custom"))) (o #t) (d #t) (k 0)) (d (n "spacetimedb-primitives") (r "^0.9.2") (d #t) (k 0)))) (h "1r7b64mvqv1zv4g4v7m480b8sj4z328gg41i5bn76py13qpf0xjl") (r "1.77.0")))

