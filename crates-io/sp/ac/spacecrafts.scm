(define-module (crates-io sp ac spacecrafts) #:use-module (crates-io))

(define-public crate-spacecrafts-0.1.0-beta.1 (c (n "spacecrafts") (v "0.1.0-beta.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "12vsdpfmvqiwp4b0h2inq8f4j1v08n1ph0nn2f6s8basih00gkxf")))

(define-public crate-spacecrafts-0.1.0-beta.2 (c (n "spacecrafts") (v "0.1.0-beta.2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07pwplffij6izrfvwx3bnlfwqn0c1211awm09lgfaf6kjlj64fzd")))

(define-public crate-spacecrafts-0.1.0-beta.3 (c (n "spacecrafts") (v "0.1.0-beta.3") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12ka6q0gsd79s4gygym0gfad70g7y0xmpw35vl6bff5bzjpmx3z5")))

