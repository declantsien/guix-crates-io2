(define-module (crates-io sp ac spaceapi-validator) #:use-module (crates-io))

(define-public crate-spaceapi-validator-0.1.0 (c (n "spaceapi-validator") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "valico") (r "^1.0") (d #t) (k 0)))) (h "0prbc6jckgy76dgmh35gbjbah0yjqv06bh9z9xpmq4gv1ygbkgyq")))

