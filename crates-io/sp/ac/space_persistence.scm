(define-module (crates-io sp ac space_persistence) #:use-module (crates-io))

(define-public crate-space_persistence-0.3.0 (c (n "space_persistence") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1zh6432095i4ia6vw9lz39aagz635cyc4b6ygmrnq5363qqzgf9p")))

(define-public crate-space_persistence-0.3.1 (c (n "space_persistence") (v "0.3.1") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0cmkcxbcr4x0qllfxvn3jd96ii3mfgdn7l758ww6x5lyff2gsfdd")))

