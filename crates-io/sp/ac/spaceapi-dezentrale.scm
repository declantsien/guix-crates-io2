(define-module (crates-io sp ac spaceapi-dezentrale) #:use-module (crates-io))

(define-public crate-spaceapi-dezentrale-0.8.1 (c (n "spaceapi-dezentrale") (v "0.8.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jgk1ysg0hkn3i6ky54g8hxh3jyv88mcfksfwzz9147jf9v6lay1")))

(define-public crate-spaceapi-dezentrale-0.8.2 (c (n "spaceapi-dezentrale") (v "0.8.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11y30fyq4dlg03hp0i947xydx5j21j1h562rn3wgxl89b90j3n7d")))

(define-public crate-spaceapi-dezentrale-0.8.2-rc1 (c (n "spaceapi-dezentrale") (v "0.8.2-rc1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09vnig2g1g6d8k7n20sfnakpdzzmy3ckni9nm8l00rr1wkk13n8f")))

(define-public crate-spaceapi-dezentrale-0.8.2-rc1.1 (c (n "spaceapi-dezentrale") (v "0.8.2-rc1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0r3b4m4bmsw9w1hgwrn73zkl4ylvq13bnsc3kqb5ad18pkjmp7ba")))

(define-public crate-spaceapi-dezentrale-0.8.999 (c (n "spaceapi-dezentrale") (v "0.8.999") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xxhra1zswwmyam2ishf4n5467iz9a0jppsivn0pxjqq6jm004xc") (y #t)))

