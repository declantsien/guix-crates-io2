(define-module (crates-io sp ac space_track) #:use-module (crates-io))

(define-public crate-space_track-0.1.0 (c (n "space_track") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.12.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "16g1jcllwzldi73m0sgg8s5m2xylxbygxjpkwdkcnr71ykg05iag")))

