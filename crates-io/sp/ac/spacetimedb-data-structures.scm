(define-module (crates-io sp ac spacetimedb-data-structures) #:use-module (crates-io))

(define-public crate-spacetimedb-data-structures-0.9.0 (c (n "spacetimedb-data-structures") (v "0.9.0") (d (list (d (n "hashbrown") (r "^0.14") (f (quote ("rayon" "serde"))) (d #t) (k 0)) (d (n "nohash-hasher") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "05a7qy0h82y8cg8kxirpf3c93q342w1s3slw8ic08k24nc3ywpdk") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-spacetimedb-data-structures-0.9.1 (c (n "spacetimedb-data-structures") (v "0.9.1") (d (list (d (n "hashbrown") (r "^0.14") (f (quote ("rayon" "serde"))) (d #t) (k 0)) (d (n "nohash-hasher") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1cp3xgmx8k3ymcib7av3dnrfy78jhg435n3blq7x2jidh8iqp9wn") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-spacetimedb-data-structures-0.9.2 (c (n "spacetimedb-data-structures") (v "0.9.2") (d (list (d (n "hashbrown") (r "^0.14") (f (quote ("rayon" "serde"))) (d #t) (k 0)) (d (n "nohash-hasher") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0pwx58g8n33rwljwyw5wr1d2bs5day0dqdjhy34ybm1igx28x8z2") (s 2) (e (quote (("serde" "dep:serde"))))))

