(define-module (crates-io sp ac spacegate-impl-hyper) #:use-module (crates-io))

(define-public crate-spacegate-impl-hyper-0.1.0-alpha1 (c (n "spacegate-impl-hyper") (v "0.1.0-alpha1") (d (list (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "spacegate-kernel") (r "^0.1.0-alpha1") (d #t) (k 0)))) (h "0lh64dbg37cx1z3b81b0081979hx8h4ndbhzd5q3dakflah2c85h") (y #t)))

