(define-module (crates-io sp ac spacex_sdk) #:use-module (crates-io))

(define-public crate-spacex_sdk-0.1.0 (c (n "spacex_sdk") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "cached") (r "^0.40") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "jwfetch") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nanoid") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0hz5ppqxj9dmn8i3yivaha70yz1figd9lxkhg990z46xqh3k64bj")))

