(define-module (crates-io sp ac space_editor_core) #:use-module (crates-io))

(define-public crate-space_editor_core-0.3.0 (c (n "space_editor_core") (v "0.3.0") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "space_persistence") (r "^0.3.0") (d #t) (k 0)) (d (n "space_prefab") (r "^0.3.0") (d #t) (k 0)) (d (n "space_shared") (r "^0.3.0") (d #t) (k 0)) (d (n "space_undo") (r "^0.3.0") (d #t) (k 0)))) (h "08h2y1n5gpbsd8jd5x22gc5i9cac74g1n788vd8i5vrzj7q3qibg") (f (quote (("persistence_editor"))))))

(define-public crate-space_editor_core-0.3.1 (c (n "space_editor_core") (v "0.3.1") (d (list (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "space_persistence") (r "^0.3.1") (d #t) (k 0)) (d (n "space_prefab") (r "^0.3.1") (d #t) (k 0)) (d (n "space_shared") (r "^0.3.1") (d #t) (k 0)) (d (n "space_undo") (r "^0.3.1") (d #t) (k 0)))) (h "12w6iypl979fy70yrwvmay43q3i1pj16pzyaa40wckldn61jacha") (f (quote (("persistence_editor"))))))

