(define-module (crates-io sp ac space-render) #:use-module (crates-io))

(define-public crate-space-render-0.1.1 (c (n "space-render") (v "0.1.1") (d (list (d (n "amethyst") (r "^0.15.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "glsl-layout") (r "^0.3.2") (d #t) (k 0)) (d (n "glsl-to-spirv") (r "^0.1.7") (d #t) (k 1)) (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)))) (h "1vjln1vis1mcdxjjiz7xgiwa3rynfmrr52fvz2vi3w3ccjdrr0pi") (f (quote (("vulkan" "amethyst/vulkan") ("metal" "amethyst/metal") ("empty" "amethyst/empty") ("default" "vulkan"))))))

