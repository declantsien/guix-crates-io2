(define-module (crates-io sp ac spacework) #:use-module (crates-io))

(define-public crate-spacework-0.0.1 (c (n "spacework") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "~3.0.0-beta.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "01pvkp863wy9h9vnsxxkpxayc5sklkrrj4h3264nnlh1mfsyhh4r")))

(define-public crate-spacework-0.0.2 (c (n "spacework") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "~3.0.0-beta.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "1k8j0q840ww3wxja84d0ln16z6l3vmk4h8sk6kry45v97ymlnf7v")))

(define-public crate-spacework-0.0.3 (c (n "spacework") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "0agshzm3fh5lqycky5izjdcp1h768y418aab6piz3l3cv0wc2vg5")))

