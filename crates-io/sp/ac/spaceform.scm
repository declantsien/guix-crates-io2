(define-module (crates-io sp ac spaceform) #:use-module (crates-io))

(define-public crate-spaceform-0.0.1 (c (n "spaceform") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1n6lp56v5m8yd8wk5441xai9cs8x6q81yn6pwiq21pb493z88vhq") (f (quote (("simd") ("default" "simd"))))))

(define-public crate-spaceform-0.0.2 (c (n "spaceform") (v "0.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0g990hhb2bwlcg274wcsp2kv7vs2g36nn9nsizy4g4ac71iwrw2r") (f (quote (("simd") ("default" "simd"))))))

(define-public crate-spaceform-0.1.0 (c (n "spaceform") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "19mqvak4qv19zdcfrs78rl42bpbykkr0i0k3w00ahzhfspygd7r2") (f (quote (("simd") ("default" "simd"))))))

