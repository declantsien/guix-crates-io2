(define-module (crates-io sp ac spaceship-cli) #:use-module (crates-io))

(define-public crate-spaceship-cli-0.1.0 (c (n "spaceship-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.21") (d #t) (k 0)))) (h "1ygy55dg72zsglvhvvwv9fd51nbvy0s699lazcxb8xqac4ykxzfk")))

