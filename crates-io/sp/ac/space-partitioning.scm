(define-module (crates-io sp ac space-partitioning) #:use-module (crates-io))

(define-public crate-space-partitioning-0.1.0 (c (n "space-partitioning") (v "0.1.0") (h "1qc9r230x07vrvk5sl5zcxw08yl037gxhvz6ndwq8fymbm51i50s")))

(define-public crate-space-partitioning-0.2.0 (c (n "space-partitioning") (v "0.2.0") (d (list (d (n "piston_window") (r "^0.120.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "const_generics"))) (d #t) (k 0)))) (h "1f2lh31v45l7b2fwipnyaxrpxdigbgslds46h9adrg59lnxv609l")))

(define-public crate-space-partitioning-0.3.0 (c (n "space-partitioning") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "piston_window") (r "^0.120.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "const_generics"))) (d #t) (k 0)))) (h "14xa0hjxxg3ilwyyb7l2lyk21q2wbas9kwlqap95f0di24jjnjz7")))

(define-public crate-space-partitioning-0.4.0 (c (n "space-partitioning") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "hashbrown") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "piston_window") (r "^0.120.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "const_generics"))) (d #t) (k 0)))) (h "0w1776ygl9a18lmq3pljzjqhln54gvg4cmr0sqpi8y4hg85d36rf") (f (quote (("default" "hashbrown"))))))

(define-public crate-space-partitioning-0.5.0 (c (n "space-partitioning") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 2)) (d (n "piston_window") (r "^0.120.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "smallvec") (r "^1.6.1") (f (quote ("union" "const_generics"))) (d #t) (k 0)))) (h "09higg7pnnn0f2qawnmarv813w2nm2isg9q2cqiyifkdcx0n8w2p")))

