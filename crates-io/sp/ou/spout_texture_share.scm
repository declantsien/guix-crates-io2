(define-module (crates-io sp ou spout_texture_share) #:use-module (crates-io))

(define-public crate-spout_texture_share-0.1.0 (c (n "spout_texture_share") (v "0.1.0") (d (list (d (n "autocxx") (r "^0.22.4") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.22.4") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "miette") (r "^4.3") (f (quote ("fancy"))) (d #t) (k 1)))) (h "1zw16yxnr0fgaqidp0fmj1k1fnxqa79w6bhmj1d4pqa27giacfk5")))

