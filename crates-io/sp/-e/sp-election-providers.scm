(define-module (crates-io sp -e sp-election-providers) #:use-module (crates-io))

(define-public crate-sp-election-providers-3.0.0 (c (n "sp-election-providers") (v "3.0.0") (d (list (d (n "codec") (r "^2.0.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-arithmetic") (r "^3.0.0") (k 0)) (d (n "sp-npos-elections") (r "^3.0.0") (k 0)) (d (n "sp-std") (r "^3.0.0") (k 0)))) (h "0i4kfpalfdlpj2k7fn96z0wssl371v0gg6zc38k8bqqp3naiwxwh") (f (quote (("std" "codec/std" "sp-std/std" "sp-npos-elections/std" "sp-arithmetic/std") ("runtime-benchmarks") ("default" "std"))))))

