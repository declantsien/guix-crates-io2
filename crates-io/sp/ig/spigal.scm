(define-module (crates-io sp ig spigal) #:use-module (crates-io))

(define-public crate-spigal-0.1.0 (c (n "spigal") (v "0.1.0") (d (list (d (n "cool_asserts") (r "^2") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "0rwzaj0pavqf3fn6x450glw1vadi7j0dix1pkd9rzl4lgj841agi")))

(define-public crate-spigal-0.1.1 (c (n "spigal") (v "0.1.1") (d (list (d (n "cool_asserts") (r "^2") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "1x8gmwaxxnf1a2h6f8psw5df0g3lq8yv7xd1xvv9h3sxz6ra63pr")))

(define-public crate-spigal-0.1.2 (c (n "spigal") (v "0.1.2") (d (list (d (n "cool_asserts") (r "^2") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "0zzp6gl8ljn97v55zj2df9al6qacr326cpw74djr73bg98hgyyzq")))

(define-public crate-spigal-0.2.0 (c (n "spigal") (v "0.2.0") (d (list (d (n "cool_asserts") (r "^2") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "0zx475dzdanpk8kkz6i72nw72pywjwc3fwgixsfmqp9ljzcfldxz")))

(define-public crate-spigal-0.3.0 (c (n "spigal") (v "0.3.0") (d (list (d (n "cool_asserts") (r "^2") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "0shbrv8kyjshvnvss3iwjgsbq5q7gf1pxpkdjhrzqaibw0pc434z") (f (quote (("advance_by"))))))

