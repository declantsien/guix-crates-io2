(define-module (crates-io sp ig spiget) #:use-module (crates-io))

(define-public crate-spiget-0.1.0 (c (n "spiget") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "httpclient") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0z6nadpywx8v90bzhn1qdf9rs5bn147nn68lp3slv8jvpwk172yw")))

