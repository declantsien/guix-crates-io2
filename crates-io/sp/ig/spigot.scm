(define-module (crates-io sp ig spigot) #:use-module (crates-io))

(define-public crate-spigot-0.1.0 (c (n "spigot") (v "0.1.0") (h "10f51xcbqdfgdl8ia97lb05yh0n6q7qq2shzyilrjd9ak4kcfqdn")))

(define-public crate-spigot-0.1.1 (c (n "spigot") (v "0.1.1") (h "1sifakrswfqn106im218ivrawcd8nbkwx7nh9dmsbsi3y276ik2d")))

(define-public crate-spigot-0.1.2 (c (n "spigot") (v "0.1.2") (h "1i48xfjiay2rb13gmn7n06l47rdax026c3kbs8jzr0wc9i1rl1xf")))

