(define-module (crates-io sp l- spl-discriminator) #:use-module (crates-io))

(define-public crate-spl-discriminator-0.1.0 (c (n "spl-discriminator") (v "0.1.0") (d (list (d (n "borsh") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.16.1") (d #t) (k 0)) (d (n "spl-discriminator-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0gq3fpiyq9glv3r4ssmf20xr096wzywxzq5zvcnbpwcfnmixbrfc") (s 2) (e (quote (("borsh" "dep:borsh"))))))

(define-public crate-spl-discriminator-0.1.1 (c (n "spl-discriminator") (v "0.1.1") (d (list (d (n "borsh") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1.14.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-program") (r ">=1.17.17, <=2") (d #t) (k 0)) (d (n "spl-discriminator-derive") (r "^0.1.2") (d #t) (k 0)))) (h "0qyjsjzkg3lvlf0pc8y5vgd3x1qdcjp9nwb16a92xwsnzvr019ns") (s 2) (e (quote (("borsh" "dep:borsh"))))))

(define-public crate-spl-discriminator-0.1.2 (c (n "spl-discriminator") (v "0.1.2") (d (list (d (n "bytemuck") (r "^1.15.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-program") (r ">=1.18.2, <=2") (d #t) (k 0)) (d (n "spl-discriminator-derive") (r "^0.1.2") (d #t) (k 0)))) (h "1c9gvrlxcfdj8901psgbqii68dkb83vfvnvdqmw8k49xsw82viya") (y #t)))

(define-public crate-spl-discriminator-0.2.0 (c (n "spl-discriminator") (v "0.2.0") (d (list (d (n "borsh") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1.15.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-program") (r ">=1.18.2, <=2") (d #t) (k 0)) (d (n "spl-discriminator-derive") (r "^0.1.2") (d #t) (k 0)))) (h "0v5zvnz71b5vyzz500bzhvycf1gvhyq79j86ypzpwmkw08hlypv2") (y #t) (s 2) (e (quote (("borsh" "dep:borsh"))))))

(define-public crate-spl-discriminator-0.2.1 (c (n "spl-discriminator") (v "0.2.1") (d (list (d (n "borsh") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1.15.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-program") (r ">=1.18.2, <=2") (d #t) (k 0)) (d (n "spl-discriminator-derive") (r "^0.1.2") (d #t) (k 0)))) (h "069p11fvsidkdjv8c69c5lr6q9wp9qyabg26wd477q0aqpmgvq51") (y #t) (s 2) (e (quote (("borsh" "dep:borsh"))))))

(define-public crate-spl-discriminator-0.2.2 (c (n "spl-discriminator") (v "0.2.2") (d (list (d (n "borsh") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1.15.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-program") (r ">=1.18.2, <=2") (d #t) (k 0)) (d (n "spl-discriminator-derive") (r "^0.2.0") (d #t) (k 0)))) (h "1b1fh9vhcwx5zl2hpkmdhk87lh7xhdpi5h82rp2hi2z90r283l9l") (s 2) (e (quote (("borsh" "dep:borsh"))))))

