(define-module (crates-io sp l- spl-name-service) #:use-module (crates-io))

(define-public crate-spl-name-service-0.2.0 (c (n "spl-name-service") (v "0.2.0") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "solana-program") (r "^1.8.1") (d #t) (k 0)) (d (n "solana-program-test") (r "^1.8.1") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.8.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1pwaw9xma0wrp6lna8jhfs3nazr1v63rfii45b8dngrffsh20aac") (f (quote (("test-bpf") ("no-entrypoint"))))))

(define-public crate-spl-name-service-0.3.0 (c (n "spl-name-service") (v "0.3.0") (d (list (d (n "borsh") (r "^0.10") (d #t) (k 0)) (d (n "num-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "solana-program") (r "^1.16.3") (d #t) (k 0)) (d (n "solana-program-test") (r "^1.16.3") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.16.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1jmzl1s3jsb6l22xb9syzppcmp9l3myn5gi707rv7an86s0di1nd") (f (quote (("test-sbf") ("no-entrypoint"))))))

