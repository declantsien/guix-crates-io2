(define-module (crates-io sp l- spl-governance-addin-api) #:use-module (crates-io))

(define-public crate-spl-governance-addin-api-0.1.0 (c (n "spl-governance-addin-api") (v "0.1.0") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)) (d (n "spl-governance-tools") (r "^0.1.1") (d #t) (k 0)))) (h "1lbfxp50blvk4jxynh45428cd5qlv6k5y9h2hxn3hnaklzbbavfc")))

(define-public crate-spl-governance-addin-api-0.1.1 (c (n "spl-governance-addin-api") (v "0.1.1") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.5") (d #t) (k 0)) (d (n "spl-governance-tools") (r "^0.1.2") (d #t) (k 0)))) (h "1wd7md2m88c0di5qz82901m9ag3k4wcmx5l5l905lqvyf1pgalcl")))

(define-public crate-spl-governance-addin-api-0.1.2 (c (n "spl-governance-addin-api") (v "0.1.2") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.9.9") (d #t) (k 0)) (d (n "spl-governance-tools") (r "^0.1.2") (d #t) (k 0)))) (h "1z6mg41g7kixhj6bzq5ycl9p42fpzq6sh67gaya6i27dd8i21xjx")))

(define-public crate-spl-governance-addin-api-0.1.3 (c (n "spl-governance-addin-api") (v "0.1.3") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "solana-program") (r "^1.14.10") (d #t) (k 0)) (d (n "spl-governance-tools") (r "^0.1.3") (d #t) (k 0)))) (h "0sqh2i697k8fykpng91c57lwfvgw1cdvpqslk7rl5yky5kdys3qy")))

(define-public crate-spl-governance-addin-api-0.1.4 (c (n "spl-governance-addin-api") (v "0.1.4") (d (list (d (n "borsh") (r "^0.10") (d #t) (k 0)) (d (n "solana-program") (r "^1.17.13") (d #t) (k 0)) (d (n "spl-governance-tools") (r "^0.1.4") (d #t) (k 0)))) (h "1dkkvj2za3i1w5ngn7a5x0v3vlswcizy3nzy6dlk67mpz28bxhcx")))

