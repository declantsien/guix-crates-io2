(define-module (crates-io sp l- spl-type-length-value-derive) #:use-module (crates-io))

(define-public crate-spl-type-length-value-derive-0.1.0 (c (n "spl-type-length-value-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xizkmnn353ddr9qfshyq4xqd1652vmrml4vk50pspvhg8k2p4a0")))

