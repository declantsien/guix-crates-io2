(define-module (crates-io sp l- spl-token-lending-cli) #:use-module (crates-io))

(define-public crate-spl-token-lending-cli-0.1.0 (c (n "spl-token-lending-cli") (v "0.1.0") (h "11vyad0vi1s3g7g7j6sp288hy1dmg4d5fqazxrcvsisygby2fs65") (y #t)))

(define-public crate-spl-token-lending-cli-0.2.0 (c (n "spl-token-lending-cli") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "solana-clap-utils") (r "=1.10.10") (d #t) (k 0)) (d (n "solana-cli-config") (r "=1.10.10") (d #t) (k 0)) (d (n "solana-client") (r "=1.10.10") (d #t) (k 0)) (d (n "solana-logger") (r "=1.10.10") (d #t) (k 0)) (d (n "solana-program") (r "=1.10.10") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.10.10") (d #t) (k 0)) (d (n "spl-token") (r "^3.3") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "spl-token-lending") (r "^0.2") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0wm15kx6al8afvdwkllgjw57ls1qp9j1iyvasnb4zfb46a1f68s7")))

