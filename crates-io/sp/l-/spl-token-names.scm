(define-module (crates-io sp l- spl-token-names) #:use-module (crates-io))

(define-public crate-spl-token-names-0.1.0 (c (n "spl-token-names") (v "0.1.0") (h "05iy67lxhnc84si2k261byvlz5zv6f8b2mv9xm9nybgd3yqhlj77")))

(define-public crate-spl-token-names-0.2.0 (c (n "spl-token-names") (v "0.2.0") (h "134i6v6b95d0qqlfqmg0f78dv791a53y4aq5aldv6pwj08nw76cf")))

