(define-module (crates-io sp l- spl-discriminator-derive) #:use-module (crates-io))

(define-public crate-spl-discriminator-derive-0.1.0 (c (n "spl-discriminator-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spl-discriminator-syn") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xfa2y3x69q68cl49aixfh2b490lms0wj5yzf72hlpjwkd08zyml")))

(define-public crate-spl-discriminator-derive-0.1.1 (c (n "spl-discriminator-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spl-discriminator-syn") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0yrqahrj1lrdj2400z6p3ys0dfwp4s373gbjr8aq4rrw9znfznzs")))

(define-public crate-spl-discriminator-derive-0.1.2 (c (n "spl-discriminator-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spl-discriminator-syn") (r "^0.1.2") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14wa6g923z0b91nw4ibr0phj7a06xgbl23h96h7gpy2gzic7iz87")))

(define-public crate-spl-discriminator-derive-0.2.0 (c (n "spl-discriminator-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spl-discriminator-syn") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0l67xmjji7g5gpl81d288k044mf7s9208bvi3jqcz796ls743s6r")))

