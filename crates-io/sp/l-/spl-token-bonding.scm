(define-module (crates-io sp l- spl-token-bonding) #:use-module (crates-io))

(define-public crate-spl-token-bonding-3.2.5 (c (n "spl-token-bonding") (v "3.2.5") (d (list (d (n "anchor-lang") (r "^0.22.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.22.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "spl-token") (r "^3.2.0") (d #t) (k 0)) (d (n "uint") (r "^0.9") (d #t) (k 0)))) (h "1ppa302cj15817bnkharqndgi7gd70v6shwx1n68dmgv0diax2xa") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

