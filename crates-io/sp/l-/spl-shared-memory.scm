(define-module (crates-io sp l- spl-shared-memory) #:use-module (crates-io))

(define-public crate-spl-shared-memory-2.0.6 (c (n "spl-shared-memory") (v "2.0.6") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "solana-program") (r "=1.10.10") (d #t) (k 0)) (d (n "solana-program-test") (r "=1.10.10") (d #t) (k 2)) (d (n "solana-sdk") (r "=1.10.10") (d #t) (k 2)))) (h "1lqak3jkhxxf79ri3xjmk8lqcb7m2zbri4gvnv8wq2fiydj4ls4p") (f (quote (("test-bpf"))))))

