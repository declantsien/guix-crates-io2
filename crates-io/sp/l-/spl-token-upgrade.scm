(define-module (crates-io sp l- spl-token-upgrade) #:use-module (crates-io))

(define-public crate-spl-token-upgrade-0.0.1 (c (n "spl-token-upgrade") (v "0.0.1") (h "0jqgygswknwivwh8ybnszx7yn8gdg1m2aashgwrnf90rb31kd98x")))

(define-public crate-spl-token-upgrade-0.1.0 (c (n "spl-token-upgrade") (v "0.1.0") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.4") (d #t) (k 0)) (d (n "solana-program") (r "^1.14.6") (d #t) (k 0)) (d (n "solana-program-test") (r "^1.14.6") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.14.6") (d #t) (k 2)) (d (n "spl-token") (r "^3.5") (f (quote ("no-entrypoint"))) (d #t) (k 2)) (d (n "spl-token-2022") (r "^0.5") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "spl-token-client") (r "^0.3") (d #t) (k 2)) (d (n "test-case") (r "^2.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1kh563cwv23c8lncqx5k817rk4dgs5z1mvmaw90r3wa79h5l461f") (f (quote (("test-sbf") ("no-entrypoint"))))))

