(define-module (crates-io sp l- spl-merkle-tree-reference) #:use-module (crates-io))

(define-public crate-spl-merkle-tree-reference-0.1.0 (c (n "spl-merkle-tree-reference") (v "0.1.0") (d (list (d (n "solana-program") (r "^1.10.33") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1fsafk3mxz52pizkdna2jm4n0qgaycwr4j4s4avvc3bzgihpqhr8")))

