(define-module (crates-io sp l- spl-noop) #:use-module (crates-io))

(define-public crate-spl-noop-0.1.0 (c (n "spl-noop") (v "0.1.0") (d (list (d (n "solana-program") (r "^1.10.29") (d #t) (k 0)))) (h "0yxx43d303x7pa54hy5l3p529hmsrkfx8gbp73ap1wblzxrhw2pw") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-spl-noop-0.1.1 (c (n "spl-noop") (v "0.1.1") (d (list (d (n "solana-program") (r "^1.10.29") (d #t) (k 0)))) (h "1aq1vxnfz5zhhqv2vn2689zwpznr3ncvbp9k2iwxgpd39vdnalb3") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-spl-noop-0.1.2 (c (n "spl-noop") (v "0.1.2") (d (list (d (n "solana-program") (r "^1.10.29") (d #t) (k 0)))) (h "15pjhnfdw9ymcnq67hivgpq2lrlpimn7pb2vys5vgjp1jr545nbn") (f (quote (("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-spl-noop-0.1.3 (c (n "spl-noop") (v "0.1.3") (d (list (d (n "solana-program") (r "^1.10.29") (d #t) (k 0)))) (h "1nhchdvxjlg7d3b5hxnsfz5dfk21vg6kkjmz2f0h3vasbg3kd1am") (f (quote (("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-spl-noop-0.2.0 (c (n "spl-noop") (v "0.2.0") (d (list (d (n "solana-program") (r "^1.16.3") (d #t) (k 0)))) (h "0l3r8cwmg3v0ll1i5kivl2297x4mjrps8p8z2kzi42h7s2ipxmkd") (f (quote (("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

