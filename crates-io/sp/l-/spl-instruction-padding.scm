(define-module (crates-io sp l- spl-instruction-padding) #:use-module (crates-io))

(define-public crate-spl-instruction-padding-0.0.1 (c (n "spl-instruction-padding") (v "0.0.1") (h "1ychjdrrcf74chcd9y6pla1jqkiyl5dkgyhh7jyp3qyqailg37r3")))

(define-public crate-spl-instruction-padding-0.1.0 (c (n "spl-instruction-padding") (v "0.1.0") (d (list (d (n "num_enum") (r "^0.5.4") (d #t) (k 0)) (d (n "solana-program") (r "^1.14.4") (d #t) (k 0)) (d (n "solana-program-test") (r "^1.14.4") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.14.4") (d #t) (k 2)))) (h "1mci2549ks6iqchqp2ajd86blqjig46nxsrz0pwzfd0s53n5fmcc") (f (quote (("test-sbf") ("no-entrypoint"))))))

(define-public crate-spl-instruction-padding-0.1.1 (c (n "spl-instruction-padding") (v "0.1.1") (d (list (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)) (d (n "solana-program") (r ">=1.17.17, <=2") (d #t) (k 0)) (d (n "solana-program-test") (r ">=1.17.17, <=2") (d #t) (k 2)) (d (n "solana-sdk") (r ">=1.17.17, <=2") (d #t) (k 2)))) (h "19hwgmbnlzqp2k9bwmizi8rxlj25n3fhnwdmfz5zqbgbnr9hqgxy") (f (quote (("test-sbf") ("no-entrypoint"))))))

