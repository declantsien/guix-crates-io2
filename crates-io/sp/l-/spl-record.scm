(define-module (crates-io sp l- spl-record) #:use-module (crates-io))

(define-public crate-spl-record-0.1.0 (c (n "spl-record") (v "0.1.0") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "borsh-derive") (r "^0.9.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "solana-program") (r "^1.10.10") (d #t) (k 0)) (d (n "solana-program-test") (r "^1.10.10") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.10.10") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1nkmnr87q0d6qn261d2xk6215zp8wssbnsrclsigids8k58p1137") (f (quote (("test-bpf") ("no-entrypoint"))))))

