(define-module (crates-io sp l- spl-token-vault) #:use-module (crates-io))

(define-public crate-spl-token-vault-0.0.1 (c (n "spl-token-vault") (v "0.0.1") (d (list (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "solana-program") (r "^1.7.6") (d #t) (k 0)) (d (n "spl-token") (r "^3.1.1") (f (quote ("no-entrypoint"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ngaffv3wj23biw3ycrvhifn58bi8myy83xdcl1ff2sdgjs6wchr") (f (quote (("test-bpf") ("no-entrypoint"))))))

