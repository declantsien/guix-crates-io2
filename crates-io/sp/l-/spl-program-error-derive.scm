(define-module (crates-io sp l- spl-program-error-derive) #:use-module (crates-io))

(define-public crate-spl-program-error-derive-0.1.0 (c (n "spl-program-error-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1a4sxih2rs8ldvgyl66j6hg04lygfsy8s4np9v1pk3jdn9dgwfky")))

(define-public crate-spl-program-error-derive-0.2.0 (c (n "spl-program-error-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rfy2gif30kv37fj5w9hjwpyszv1kwlnfnrskcc84y440v2kqgqp")))

(define-public crate-spl-program-error-derive-0.3.0 (c (n "spl-program-error-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.16.3") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cxr67a9hjr287h25xfhs7pz170786klsinq5c7p7yzy85grqw56")))

(define-public crate-spl-program-error-derive-0.3.1 (c (n "spl-program-error-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0b55bz9k1y7y0716kqpaw3c7n0as6m8mmwrfanv1gnk8x346jlmb")))

(define-public crate-spl-program-error-derive-0.3.2 (c (n "spl-program-error-derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0x5lbifhzhxj0xlc1rhrmrrykbsphmsjwx1j48w713yn3zkxyi8q")))

(define-public crate-spl-program-error-derive-0.4.0 (c (n "spl-program-error-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1m2dfr0lc06yvcq86a6bi3a7p9i6zkd42p8vj8g4hn0xdc8s66k4")))

(define-public crate-spl-program-error-derive-0.4.1 (c (n "spl-program-error-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gkb4nrhmlszaj5mdar153vqz4whnkdw54z0adiq65y5fvfpblz6")))

