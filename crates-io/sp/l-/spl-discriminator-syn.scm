(define-module (crates-io sp l- spl-discriminator-syn) #:use-module (crates-io))

(define-public crate-spl-discriminator-syn-0.1.0 (c (n "spl-discriminator-syn") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.16.1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bya5rblzrkrwq4hgnc6d153c2z7zv1q0igj172j99phl9yqv5i1")))

(define-public crate-spl-discriminator-syn-0.1.1 (c (n "spl-discriminator-syn") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1wn3zzwj1ajwd6mxyxxnhvyiz6n7k7jmq9fiaj6r7j22r9220pqf")))

(define-public crate-spl-discriminator-syn-0.1.2 (c (n "spl-discriminator-syn") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qws67kfrl21f2cx9n075863mh4ph2aym0hpf888vn8vhnzagzhq")))

(define-public crate-spl-discriminator-syn-0.2.0 (c (n "spl-discriminator-syn") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1fb9ggz816lh7cvnszh3bqsrdyzay8h9fc6arb3ymabw7dcha7wc")))

