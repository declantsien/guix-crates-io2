(define-module (crates-io sp a- spa-server) #:use-module (crates-io))

(define-public crate-spa-server-0.1.0 (c (n "spa-server") (v "0.1.0") (d (list (d (n "actix-cors") (r "^0.5") (d #t) (k 0)) (d (n "actix-files") (r "^0.6.0-beta.2") (d #t) (k 0)) (d (n "actix-identity") (r "^0.3") (d #t) (k 0)) (d (n "actix-web") (r "^4.0.0-beta.3") (d #t) (k 0)) (d (n "include-flate") (r "^0.1") (f (quote ("stable"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "spa-server-derive") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)))) (h "0kwygnrgql3zkfic248x4sflx6z21w0n41kjq3xsl4nq4z5h4yi9")))

