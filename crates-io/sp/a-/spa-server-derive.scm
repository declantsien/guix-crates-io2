(define-module (crates-io sp a- spa-server-derive) #:use-module (crates-io))

(define-public crate-spa-server-derive-0.1.0 (c (n "spa-server-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0ddb4ga4krrafis5rbh80kf3hdxqdx43sdv8mv92mpmkh924q5n6")))

