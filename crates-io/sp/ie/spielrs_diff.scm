(define-module (crates-io sp ie spielrs_diff) #:use-module (crates-io))

(define-public crate-spielrs_diff-0.1.0 (c (n "spielrs_diff") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "macros" "rt-threaded" "stream"))) (d #t) (k 0)))) (h "09adz1hh5dibqq4qs30j54yzdn66k912nqaj0zppfp3xmnv2napi")))

(define-public crate-spielrs_diff-0.2.0 (c (n "spielrs_diff") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "macros" "rt-threaded" "stream"))) (d #t) (k 0)))) (h "0jc6kfq3s1wdw8li1szazmq4sa9fyksf6v4sn5mvlc3chbj2jyyg")))

(define-public crate-spielrs_diff-0.2.1 (c (n "spielrs_diff") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.7.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)))) (h "1jn3lfsgzbkk9m513q1z3wdchja540aqb1cz3d6inbjmqp8kzbba")))

