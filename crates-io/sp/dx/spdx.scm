(define-module (crates-io sp dx spdx) #:use-module (crates-io))

(define-public crate-spdx-0.1.0 (c (n "spdx") (v "0.1.0") (h "1xggzv30a2scr26jkg02q09h1kzlj9p1khskn260cr5sxyjzhcsw")))

(define-public crate-spdx-0.2.0 (c (n "spdx") (v "0.2.0") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (f (quote ("std"))) (k 0)) (d (n "smallvec") (r "^0.6.10") (d #t) (k 0)))) (h "10jadi3w3bq2c9k7zlp7yn51q5fmjai51jik5hl69hvcfxxj977a")))

(define-public crate-spdx-0.2.1 (c (n "spdx") (v "0.2.1") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (f (quote ("std"))) (k 0)) (d (n "smallvec") (r "^0.6.10") (d #t) (k 0)))) (h "1l604ic3hgcgwjhy6v375pb0l84i877l618lkizmdd0rk1w2329v")))

(define-public crate-spdx-0.2.2 (c (n "spdx") (v "0.2.2") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (f (quote ("std"))) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "111ica26dh650aqz8imicmhg1yypbyg225qiqljhm7g8p281lpcp")))

(define-public crate-spdx-0.2.3 (c (n "spdx") (v "0.2.3") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (f (quote ("std"))) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "0mvdh5lk39hljcnsrgfiz2i4c2k2njphkc0bzk5irpvsz308yd7l")))

(define-public crate-spdx-0.2.4 (c (n "spdx") (v "0.2.4") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (f (quote ("std"))) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "1b0liy0r6l1nfpqjxbj9dkk48vxarrbmf8ha1fcmilmiq8f7waw4")))

(define-public crate-spdx-0.3.0 (c (n "spdx") (v "0.3.0") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (f (quote ("std"))) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "12dshqmaw5xvbbzbkz4ydn1y94f2rh8w1s35x69fh4wpn60bxh4d")))

(define-public crate-spdx-0.3.1 (c (n "spdx") (v "0.3.1") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (f (quote ("std"))) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "1yy58kgyfh6mcxdc7vgg7npxwjjxblw2rrl9n2cnzzc9a2i4jd4x") (y #t)))

(define-public crate-spdx-0.3.2 (c (n "spdx") (v "0.3.2") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (f (quote ("std"))) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "1p5kvr46gjwaqm10bm4hq2fb98vcmhgfbk2xdc3ys8kibj140z1r")))

(define-public crate-spdx-0.3.3 (c (n "spdx") (v "0.3.3") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (f (quote ("std"))) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "12bxq72r592kfg1k6nnll63viwpfqiq3l5xm7zk821shy3ppjwkf")))

(define-public crate-spdx-0.3.4 (c (n "spdx") (v "0.3.4") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (f (quote ("std"))) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "0mc2n86dsy6fnj76cx302nvffxyr0kh2l7j022m64xxar5sghs0s")))

(define-public crate-spdx-0.3.5 (c (n "spdx") (v "0.3.5") (d (list (d (n "difference") (r "^2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.4") (f (quote ("std"))) (k 0)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)))) (h "1qang474dmd37r1s602fdjgcdqjwyzv1njk5qdv46cykg70pd04a")))

(define-public crate-spdx-0.3.6 (c (n "spdx") (v "0.3.6") (d (list (d (n "difference") (r "^2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.4") (f (quote ("std"))) (k 0)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)))) (h "03qavnn6si0xqikhydpp123nml36dxy3c02zl1jasddnfg3nqssf")))

(define-public crate-spdx-0.4.0 (c (n "spdx") (v "0.4.0") (d (list (d (n "difference") (r "^2.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.4") (f (quote ("std"))) (k 0)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)))) (h "10r7r75zcqlcjrawdvdx2c95s39639rxhsjayfpkrj5cdkvy9ssm")))

(define-public crate-spdx-0.4.1 (c (n "spdx") (v "0.4.1") (d (list (d (n "difference") (r "^2.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)))) (h "0q327g1ly4nhpxfyzf86qz4pr8yabxv1nawq6pa98rh5gvph3f9z")))

(define-public crate-spdx-0.5.0 (c (n "spdx") (v "0.5.0") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)))) (h "1qgr89x2w3413751fy906zr0d19kvhdfwgpl3dxz8plg3h2agxir")))

(define-public crate-spdx-0.6.0 (c (n "spdx") (v "0.6.0") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)))) (h "0xiwy9ly5ryw0dfrpkqd2fvpa408b61822ghbrpp4235b5gxbb84")))

(define-public crate-spdx-0.6.1 (c (n "spdx") (v "0.6.1") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.7") (d #t) (k 0)))) (h "1wibqzdx4y8i51bb4pj98q498fj0n70xbgjb1lcr6nv1s1ym70xb")))

(define-public crate-spdx-0.6.2 (c (n "spdx") (v "0.6.2") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.7") (d #t) (k 0)))) (h "02q3yhrf8dqpni062jxsyhd4hjmkhd4w5r2wp27f8412hjfgy6vy") (f (quote (("text"))))))

(define-public crate-spdx-0.7.0 (c (n "spdx") (v "0.7.0") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.7") (d #t) (k 0)))) (h "1cc7w79mh17kj3cz9b5i51bjxkg4d7n5q5rfzcrh8kyiiqr42i5p") (f (quote (("text")))) (r "1.56.1")))

(define-public crate-spdx-0.8.0 (c (n "spdx") (v "0.8.0") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.7") (d #t) (k 0)))) (h "1qv0lwcwjkwdsipn0jar4yj68rzihwkzxhjy7q7s3g73gznq64m3") (f (quote (("text")))) (r "1.56.1")))

(define-public crate-spdx-0.8.1 (c (n "spdx") (v "0.8.1") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.7") (d #t) (k 0)))) (h "0bj20gh57gvqzhabh6pzzvxwqlic5d1yrg8bmj7av7p4dfrfbg3q") (f (quote (("text")))) (r "1.56.1")))

(define-public crate-spdx-0.9.0 (c (n "spdx") (v "0.9.0") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.7") (d #t) (k 0)))) (h "09b4qcs5ly8gn5ww4wm6r4qzhrinisjx963bp7wpcxyh7ydr0im3") (f (quote (("text")))) (r "1.56.1")))

(define-public crate-spdx-0.10.0 (c (n "spdx") (v "0.10.0") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.7") (d #t) (k 0)))) (h "0kzkv6r05wa1msgivfl5xv2j83k863an8806lsfdg86x5n1lipaj") (f (quote (("text")))) (r "1.65.0")))

(define-public crate-spdx-0.10.1 (c (n "spdx") (v "0.10.1") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.7") (d #t) (k 0)))) (h "12fxp363sz0n0x0z0rs1kgw1fmkc6naiyfzpfihz8ad63ilwnw99") (f (quote (("text")))) (r "1.65.0")))

(define-public crate-spdx-0.10.2 (c (n "spdx") (v "0.10.2") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.7") (d #t) (k 0)))) (h "0wdzpc2fhg6ikl5gxm08dfh4azjpq42z2brhfhqv56l9dpnk56xi") (f (quote (("text")))) (r "1.65.0")))

(define-public crate-spdx-0.10.3 (c (n "spdx") (v "0.10.3") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.7") (d #t) (k 0)))) (h "12v37yksb6bsqj54m9fkk7lr69indsl9v6zwq8zzkf89icwy3gb2") (f (quote (("text")))) (r "1.65.0")))

(define-public crate-spdx-0.10.4 (c (n "spdx") (v "0.10.4") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.7") (d #t) (k 0)))) (h "1jd066k3fndm3x7131xajsmh19xfi7zj7ny8f8lw56p3l47imvr9") (f (quote (("text")))) (r "1.65.0")))

