(define-module (crates-io sp dx spdx-expression) #:use-module (crates-io))

(define-public crate-spdx-expression-0.1.0 (c (n "spdx-expression") (v "0.1.0") (h "1czji0wkm6fxhahvhyby79ngi69yvlsqp0mzi74dnrdjqaiy55my")))

(define-public crate-spdx-expression-0.2.0 (c (n "spdx-expression") (v "0.2.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "121faxp18i7f8lvbazpm0sn8za7mm7n6fry14fnl1dxngprw8lqc")))

(define-public crate-spdx-expression-0.3.0 (c (n "spdx-expression") (v "0.3.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "084xwrgrjrppkd1yr4hqllsgajsrvh09kqmfw76l4mc7ic1bgiav")))

(define-public crate-spdx-expression-0.4.0 (c (n "spdx-expression") (v "0.4.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "1i5bn1kvinrya8gr1l7fw33m41zzkp8g8n3j3p3zcyz3fchhgj5a")))

(define-public crate-spdx-expression-0.5.0 (c (n "spdx-expression") (v "0.5.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "0w4hwd6nigfd5g5723frz12s825bxxpbjp8ai4p230lf9csgnwbg")))

(define-public crate-spdx-expression-0.5.1 (c (n "spdx-expression") (v "0.5.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "1zmirnhcwvssbs8ywmmldz8xjk1ikxcvnz2nayv5s7qz2a9vaxny")))

(define-public crate-spdx-expression-0.5.2 (c (n "spdx-expression") (v "0.5.2") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "0xxd44h390n559rwlp1x3fs1jjhaw8avhvcx0j2jsmvwqq1srmsk")))

