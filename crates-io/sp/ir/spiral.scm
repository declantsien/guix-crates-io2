(define-module (crates-io sp ir spiral) #:use-module (crates-io))

(define-public crate-spiral-0.1.0 (c (n "spiral") (v "0.1.0") (h "1id4bh69q2644vvf1iv3yw3lr4bs01wzf4z216ixphjzr1nallyv")))

(define-public crate-spiral-0.1.1 (c (n "spiral") (v "0.1.1") (h "15l1r28qy9pjjgi9bh8cd4a6zgycr712h03b7qdy29klw18hg5ry")))

(define-public crate-spiral-0.1.2 (c (n "spiral") (v "0.1.2") (h "0chi9angv2n9ayrd2bp93y0n05mj7lnb0salj9l3gf0apjs9vrcb")))

(define-public crate-spiral-0.1.3 (c (n "spiral") (v "0.1.3") (h "1wa3vcba9871gavv0v3vyv7hl00p734wcc8cxfhmnga7jpx9p52y")))

(define-public crate-spiral-0.1.4 (c (n "spiral") (v "0.1.4") (h "138p815fl1rcca74an0qv0hr67c8kr380py6qp948fq1gnphfm24")))

(define-public crate-spiral-0.1.5 (c (n "spiral") (v "0.1.5") (h "0037b829lvrdlfzwwfv9valirvan6pl2dpviida583hq1x0yz4s8")))

(define-public crate-spiral-0.1.6 (c (n "spiral") (v "0.1.6") (h "16md69yc43xwcygcpl0vk1k66lqrn8yf33wrql54l62bpaxj1lgn")))

(define-public crate-spiral-0.1.7 (c (n "spiral") (v "0.1.7") (h "0v4839r6a9qjw42jfrvbs2rv47446d66az77dbvwvrj1y0jx06q5")))

(define-public crate-spiral-0.1.8 (c (n "spiral") (v "0.1.8") (h "0qxwyi33vvdmw6qqsciszqyk0jv59lxnial065y3crw08p5207c0")))

(define-public crate-spiral-0.1.9 (c (n "spiral") (v "0.1.9") (h "1qfjbj1nzwk5aqlk1h7hn0p49wkwb21r4jj9j705nsyk023hdqxz")))

(define-public crate-spiral-0.2.0 (c (n "spiral") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.6") (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "pprof") (r "^0.10.1") (f (quote ("flamegraph" "criterion"))) (d #t) (k 2)))) (h "1nn1w2cyhpzyia9ilnb0s6flvh581xiirs71j83plalrsfhkhx9k")))

(define-public crate-spiral-0.2.1 (c (n "spiral") (v "0.2.1") (d (list (d (n "criterion") (r "^0.5.1") (k 2)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "pprof") (r "^0.13.0") (f (quote ("flamegraph" "criterion"))) (d #t) (k 2)))) (h "1h0hh932hsxjyyz68msq966xf89zaq0gzhmmvh2np69xrddpjhhg")))

