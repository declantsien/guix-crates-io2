(define-module (crates-io sp ir spiralizer) #:use-module (crates-io))

(define-public crate-spiralizer-0.1.0 (c (n "spiralizer") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "image") (r "^0.10") (d #t) (k 0)) (d (n "memmap") (r "^0.4") (d #t) (k 0)) (d (n "pbr") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "16i0g8z2q9xck4fmfr3jfmzr2zp9i2bi0x48lrfimxhsryy2spsw")))

