(define-module (crates-io sp ir spirv-std) #:use-module (crates-io))

(define-public crate-spirv-std-0.1.0 (c (n "spirv-std") (v "0.1.0") (h "0034ivbisjsk0y0lavwy6m6pr801q25sl0kbzslhzk4mcayx9svh")))

(define-public crate-spirv-std-0.2.0 (c (n "spirv-std") (v "0.2.0") (d (list (d (n "glam") (r "^0.11.1") (f (quote ("libm"))) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)))) (h "1lgjqz9ycgg894nsj3gaqa35h9hi7srm1969f3imiqjc97fy038b") (f (quote (("std" "glam/std") ("default"))))))

(define-public crate-spirv-std-0.2.1 (c (n "spirv-std") (v "0.2.1") (d (list (d (n "glam") (r "^0.11.1") (f (quote ("libm"))) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "spirv-std-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1m3d9rjaa1ysvx4mgkmpg0pia7dk08x86jx29w2b4qig1m41c3gy") (f (quote (("std" "glam/std") ("default"))))))

(define-public crate-spirv-std-0.3.0 (c (n "spirv-std") (v "0.3.0") (d (list (d (n "glam") (r "^0.12.0") (f (quote ("libm" "scalar-math"))) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "spirv-std-macros") (r "^0.3.0") (d #t) (k 0)))) (h "08pp3l3653nr1zlbw9bqsqbivyrn0cbq7x5ra64sypq4yyvlsid9") (f (quote (("std" "glam/std") ("default"))))))

(define-public crate-spirv-std-0.4.0-alpha.0 (c (n "spirv-std") (v "0.4.0-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "spirv-std-macros") (r "^0.4.0-alpha.0") (d #t) (k 0)))) (h "0wyqjbypq13385xxb7npx6k8akyw2mq2kcsh526swfxr6s71g4p7") (f (quote (("default") ("const-generics"))))))

(define-public crate-spirv-std-0.4.0-alpha.1 (c (n "spirv-std") (v "0.4.0-alpha.1") (d (list (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "spirv-std-macros") (r "^0.4.0-alpha.0") (d #t) (k 0)))) (h "12zwawlf7wi7w5c7v6wsf7kizcpljcz6lgshvprjfsgjv36v5alg") (f (quote (("default") ("const-generics"))))))

(define-public crate-spirv-std-0.4.0-alpha.2 (c (n "spirv-std") (v "0.4.0-alpha.2") (d (list (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "spirv-std-macros") (r "^0.4.0-alpha.0") (d #t) (k 0)))) (h "047xxn2ypv41vy8mk7xngrzhb48g59ngh8z0bnavxwqnlf93pzqx") (f (quote (("default") ("const-generics"))))))

(define-public crate-spirv-std-0.4.0-alpha.3 (c (n "spirv-std") (v "0.4.0-alpha.3") (d (list (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "spirv-std-macros") (r "^0.4.0-alpha.0") (d #t) (k 0)))) (h "1pb04h6ysh355g83jbwlwf2kngv65jrqxprgw3lw7pq402wg3rkd") (f (quote (("default") ("const-generics"))))))

(define-public crate-spirv-std-0.4.0-alpha.4 (c (n "spirv-std") (v "0.4.0-alpha.4") (d (list (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "spirv-std-macros") (r "^0.4.0-alpha.0") (d #t) (k 0)))) (h "0wd1fkv0k4g8llyg12xvvrs264rzblfxyjs1r8aqparx1srqcnl9") (f (quote (("default") ("const-generics"))))))

(define-public crate-spirv-std-0.4.0-alpha.5 (c (n "spirv-std") (v "0.4.0-alpha.5") (d (list (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "spirv-std-macros") (r "^0.4.0-alpha.0") (d #t) (k 0)))) (h "1i6rp8dllyvp6p5srbs2zxgi3yrm37fsyjafh0qsj4f1kd2xxsl4") (f (quote (("default") ("const-generics"))))))

(define-public crate-spirv-std-0.4.0-alpha.6 (c (n "spirv-std") (v "0.4.0-alpha.6") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "spirv-std-macros") (r "^0.4.0-alpha.3") (d #t) (k 0)) (d (n "spirv-types") (r "^0.4.0-alpha.3") (d #t) (k 0)))) (h "1ad0yrpkqq5cplfd19ksljyri4kr9wb8k6gd73r2mn9bq5bj6nwh") (f (quote (("default") ("const-generics"))))))

(define-public crate-spirv-std-0.4.0-alpha.7 (c (n "spirv-std") (v "0.4.0-alpha.7") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "spirv-std-macros") (r "^0.4.0-alpha.3") (d #t) (k 0)) (d (n "spirv-types") (r "^0.4.0-alpha.3") (d #t) (k 0)))) (h "14npd838bqnw09iab0zq94xs4yl69m6c0cdfzxa9fal0zxqlqv3j") (f (quote (("default") ("const-generics"))))))

(define-public crate-spirv-std-0.4.0-alpha.8 (c (n "spirv-std") (v "0.4.0-alpha.8") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "glam") (r "^0.15.2") (f (quote ("libm"))) (o #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "spirv-std-macros") (r "^0.4.0-alpha.8") (d #t) (k 0)) (d (n "spirv-types") (r "^0.4.0-alpha.8") (d #t) (k 0)))) (h "0gkja8xc1bjzi409l6s7wr1mni12q2zbarmj8b3vfic3wprpc9hf") (f (quote (("default") ("const-generics"))))))

(define-public crate-spirv-std-0.4.0-alpha.10 (c (n "spirv-std") (v "0.4.0-alpha.10") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "glam") (r "^0.16.0") (f (quote ("libm"))) (o #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "spirv-std-macros") (r "^0.4.0-alpha.10") (d #t) (k 0)) (d (n "spirv-types") (r "^0.4.0-alpha.10") (d #t) (k 0)))) (h "1lfzs7gf1pfds0ssr1fj37nl2a6xrilpdz1aa1f3qxhc7b7imb88") (f (quote (("default"))))))

(define-public crate-spirv-std-0.4.0-alpha.11 (c (n "spirv-std") (v "0.4.0-alpha.11") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "glam") (r "^0.17.0") (f (quote ("libm"))) (o #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "spirv-std-macros") (r "^0.4.0-alpha.11") (d #t) (k 0)) (d (n "spirv-types") (r "^0.4.0-alpha.11") (d #t) (k 0)))) (h "1hm014fyjnvrr7xznd524ys1n4zq9bipqsspzxbn1lw6gv6x7bq1") (f (quote (("default"))))))

(define-public crate-spirv-std-0.4.0-alpha.12 (c (n "spirv-std") (v "0.4.0-alpha.12") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "glam") (r "^0.17.0") (f (quote ("libm"))) (o #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "spirv-std-macros") (r "^0.4.0-alpha.12") (d #t) (k 0)) (d (n "spirv-types") (r "^0.4.0-alpha.12") (d #t) (k 0)))) (h "1b8j1cvjvk5xp930b2m1sqwdbhl2nq89nvzldv74mb1jv38wpm4j") (f (quote (("default"))))))

(define-public crate-spirv-std-0.4.0-alpha.13 (c (n "spirv-std") (v "0.4.0-alpha.13") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "glam") (r ">=0.17, <=0.21") (f (quote ("libm"))) (o #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "spirv-std-macros") (r "^0.4.0-alpha.13") (d #t) (k 0)) (d (n "spirv-types") (r "^0.4.0-alpha.13") (d #t) (k 0)))) (h "069an0h29l1g437ha9aafkq7dlffi7rjy6gdpfc3cs802b20f2mx") (f (quote (("default"))))))

(define-public crate-spirv-std-0.4.0-alpha.14 (c (n "spirv-std") (v "0.4.0-alpha.14") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "glam") (r ">=0.17, <=0.21") (f (quote ("libm"))) (o #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "spirv-std-macros") (r "^0.4.0-alpha.13") (d #t) (k 0)) (d (n "spirv-std-types") (r "^0.4.0-alpha.14") (d #t) (k 0)))) (h "02da5p5lhqrwdp4xaxkwjxc2hz5wcjwny12af5girpv5zn4bwc5r") (f (quote (("default"))))))

(define-public crate-spirv-std-0.4.0-alpha.15 (c (n "spirv-std") (v "0.4.0-alpha.15") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "glam") (r ">=0.17, <=0.21") (f (quote ("libm"))) (o #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "spirv-std-macros") (r "^0.4.0-alpha.13") (d #t) (k 0)) (d (n "spirv-std-types") (r "^0.4.0-alpha.15") (d #t) (k 0)))) (h "18x5w8qjxf1k3lv5l94mcrgmlbxl5bvf80glvskxskf87rhsv6aj") (f (quote (("default"))))))

(define-public crate-spirv-std-0.4.0-alpha.16 (c (n "spirv-std") (v "0.4.0-alpha.16") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "glam") (r "^0.22") (f (quote ("libm"))) (o #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "spirv-std-macros") (r "^0.4.0-alpha.13") (d #t) (k 0)) (d (n "spirv-std-types") (r "^0.4.0-alpha.16") (d #t) (k 0)))) (h "0ra5jrz2k5dhhmyk5dq6jxmrzl2wsvl8r2745d55c0aqhmyi8vzq") (f (quote (("default"))))))

(define-public crate-spirv-std-0.4.0-alpha.17 (c (n "spirv-std") (v "0.4.0-alpha.17") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "glam") (r "^0.22") (f (quote ("libm"))) (o #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "spirv-std-macros") (r "=0.4.0-alpha.17") (d #t) (k 0)) (d (n "spirv-std-types") (r "=0.4.0-alpha.17") (d #t) (k 0)))) (h "16mymhwlm57nkv1503f9avmnlcqjp4lrag2r653fwc3j1q0iachv") (f (quote (("default"))))))

(define-public crate-spirv-std-0.4.0 (c (n "spirv-std") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "glam") (r "^0.22") (f (quote ("libm"))) (o #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "spirv-std-macros") (r "=0.4.0") (d #t) (k 0)) (d (n "spirv-std-types") (r "=0.4.0") (d #t) (k 0)))) (h "0256fs8x1xlf6bhywl8wnddg9zznawkc0dzpscj1awka98kpbj40") (f (quote (("default"))))))

(define-public crate-spirv-std-0.5.0 (c (n "spirv-std") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "glam") (r "^0.22") (f (quote ("libm"))) (o #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "spirv-std-macros") (r "=0.5.0") (d #t) (k 0)) (d (n "spirv-std-types") (r "=0.5.0") (d #t) (k 0)))) (h "1p0ri9ln5jlz94fwh50wfsl0zl89kf7ixzh7ssz1l15clpr84091") (f (quote (("default"))))))

(define-public crate-spirv-std-0.6.0 (c (n "spirv-std") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "glam") (r "^0.22") (f (quote ("libm"))) (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "spirv-std-macros") (r "=0.6.0") (d #t) (k 0)) (d (n "spirv-std-types") (r "=0.6.0") (d #t) (k 0)))) (h "10r3m6xrq0dcmdbnjxz463qai2vcvzb6fizzgzxh4xjdgjigdy84") (f (quote (("default"))))))

(define-public crate-spirv-std-0.6.1 (c (n "spirv-std") (v "0.6.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "glam") (r "^0.22") (f (quote ("libm"))) (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "spirv-std-macros") (r "=0.6.1") (d #t) (k 0)) (d (n "spirv-std-types") (r "=0.6.1") (d #t) (k 0)))) (h "0m5qcvyhwlycibd989rxjz5gzh5w47yshbdqmqv63jq4b5jj1g8r") (f (quote (("default"))))))

(define-public crate-spirv-std-0.7.0 (c (n "spirv-std") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "glam") (r "^0.22") (f (quote ("libm"))) (k 0)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "spirv-std-macros") (r "=0.7.0") (d #t) (k 0)) (d (n "spirv-std-types") (r "=0.7.0") (d #t) (k 0)))) (h "0riy5lp2qhl6ml4c6iam5nbjv17pr0966dgvkl7zqb8w096bv5ri") (f (quote (("default"))))))

(define-public crate-spirv-std-0.8.0 (c (n "spirv-std") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "glam") (r ">=0.22, <=0.24") (f (quote ("libm"))) (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "spirv-std-macros") (r "=0.8.0") (d #t) (k 0)) (d (n "spirv-std-types") (r "=0.8.0") (d #t) (k 0)))) (h "02sfz4naqq2dhmqqysr7v799q8wpyf7bhg18qrm3xska43q6pbak") (f (quote (("default"))))))

(define-public crate-spirv-std-0.9.0 (c (n "spirv-std") (v "0.9.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "glam") (r ">=0.22, <=0.24") (f (quote ("libm"))) (k 0)) (d (n "num-traits") (r "^0.2.15") (f (quote ("libm"))) (k 0)) (d (n "spirv-std-macros") (r "=0.9.0") (d #t) (k 0)) (d (n "spirv-std-types") (r "=0.9.0") (d #t) (k 0)))) (h "0myph3ag94yzbvy7h7vaidasma9pg5zzxwla5jz9mxrd5abw1hv8") (f (quote (("default"))))))

