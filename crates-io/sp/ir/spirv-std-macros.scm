(define-module (crates-io sp ir spirv-std-macros) #:use-module (crates-io))

(define-public crate-spirv-std-macros-0.1.0 (c (n "spirv-std-macros") (v "0.1.0") (h "1qn1yc113hpbpskgjqiax58cn4q8l27gnjy13z2c9fqyz6vcm515")))

(define-public crate-spirv-std-macros-0.3.0 (c (n "spirv-std-macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "0q6wynx1xblcwr9wq15zi8kck5n82g730nqv0z3gi67wm32zvv6p")))

(define-public crate-spirv-std-macros-0.4.0-alpha.0 (c (n "spirv-std-macros") (v "0.4.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "1jhk6gzq0cca6aw37167mc2y0m3hgacpnjsgd6rrg7k1jdi5943y")))

(define-public crate-spirv-std-macros-0.4.0-alpha.1 (c (n "spirv-std-macros") (v "0.4.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "0dkg82rjjawa481jkj0jgbjkgsf0iphrfw6illndswmvvcxvw9as")))

(define-public crate-spirv-std-macros-0.4.0-alpha.2 (c (n "spirv-std-macros") (v "0.4.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "1pbrf3nyd855md2ixgzzg3glb5xpkngp3zsvaf0lgl40143vg868")))

(define-public crate-spirv-std-macros-0.4.0-alpha.3 (c (n "spirv-std-macros") (v "0.4.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "0bbgipi2z2k8gh3m2gmk0ynvay5d2p1zniyc8rxdavr3nn1215zl")))

(define-public crate-spirv-std-macros-0.4.0-alpha.4 (c (n "spirv-std-macros") (v "0.4.0-alpha.4") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "154q1fwgpsrq7ka43blk6jpwidpsrgz4w5jfqnkhs3gkp6mxjxjv")))

(define-public crate-spirv-std-macros-0.4.0-alpha.5 (c (n "spirv-std-macros") (v "0.4.0-alpha.5") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "1ip7ipi27z4a9kgby8pjkfayhspdlmk7ifs8frn4kv9prpm077kq")))

(define-public crate-spirv-std-macros-0.4.0-alpha.6 (c (n "spirv-std-macros") (v "0.4.0-alpha.6") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "spirv-types") (r "^0.4.0-alpha.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "0ca0d5w09jzjazs5gx2dbwm9ckcvryqjlfg02hj8yh7j1ryn56ip")))

(define-public crate-spirv-std-macros-0.4.0-alpha.7 (c (n "spirv-std-macros") (v "0.4.0-alpha.7") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "spirv-types") (r "^0.4.0-alpha.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "0fl19fp9i2p0ilj10ilp9iabis9gaxr1j9hbmssndi6c1xad3arf")))

(define-public crate-spirv-std-macros-0.4.0-alpha.8 (c (n "spirv-std-macros") (v "0.4.0-alpha.8") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "spirv-types") (r "^0.4.0-alpha.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "01mg71jdlmkbjfbkdszxsh84fcp8d9wc1nhf95lgspdmf2zwv933")))

(define-public crate-spirv-std-macros-0.4.0-alpha.10 (c (n "spirv-std-macros") (v "0.4.0-alpha.10") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "spirv-types") (r "^0.4.0-alpha.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "1f3wdbxs53whsclmxy1rgg3l6n17rb9xkc3iw3irml9a4x7k76wb")))

(define-public crate-spirv-std-macros-0.4.0-alpha.11 (c (n "spirv-std-macros") (v "0.4.0-alpha.11") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "spirv-types") (r "^0.4.0-alpha.11") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "0y38rf75qv7190ihv0dc5zj2i39n3rai7ijpl6gnfzn8aq9n7ims")))

(define-public crate-spirv-std-macros-0.4.0-alpha.12 (c (n "spirv-std-macros") (v "0.4.0-alpha.12") (d (list (d (n "heck") (r "^0.3.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "spirv-types") (r "^0.4.0-alpha.12") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "19cc15vq9ybdap39qxpf4vsz5q6li21mzgv47zqbyj5vvwaqazi0")))

(define-public crate-spirv-std-macros-0.4.0-alpha.13 (c (n "spirv-std-macros") (v "0.4.0-alpha.13") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "spirv-types") (r "^0.4.0-alpha.13") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "0vhdwqwhgm2k20v8qvki905hvqx3nds5zs1qy08scfdcda09j3zr")))

(define-public crate-spirv-std-macros-0.4.0-alpha.14 (c (n "spirv-std-macros") (v "0.4.0-alpha.14") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "spirv-std-types") (r "^0.4.0-alpha.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "1j3z8929w596r7csgqafr0a000xvgg2v7jlimmyl5vrihv8i3dbg")))

(define-public crate-spirv-std-macros-0.4.0-alpha.15 (c (n "spirv-std-macros") (v "0.4.0-alpha.15") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "spirv-std-types") (r "^0.4.0-alpha.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "1bsfnb69qakv5ssc5jwdbjsdr7m39wykaskrywqvvr4vsjajk62f")))

(define-public crate-spirv-std-macros-0.4.0-alpha.16 (c (n "spirv-std-macros") (v "0.4.0-alpha.16") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "spirv-std-types") (r "^0.4.0-alpha.16") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "1hh3balvqhm1z6q1yr9cf2lzrfd5kzl1cf9lfrh5qjflx716vd0d")))

(define-public crate-spirv-std-macros-0.4.0-alpha.17 (c (n "spirv-std-macros") (v "0.4.0-alpha.17") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "spirv-std-types") (r "=0.4.0-alpha.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "0k2jlhrsv7ag75zlwr336j5yss3i42wcg8sxyzri7rjc5b82w0xc")))

(define-public crate-spirv-std-macros-0.4.0 (c (n "spirv-std-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "spirv-std-types") (r "=0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "1065120yaggvyxga8nfdhs019mvzcvr78xa57qy8w3h4zpyvan1h")))

(define-public crate-spirv-std-macros-0.5.0 (c (n "spirv-std-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "spirv-std-types") (r "=0.5.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "0m100phmm21f5qzsy6g268bcjlhjg8hbqnk4ivc4zw9f546lyv5r")))

(define-public crate-spirv-std-macros-0.6.0 (c (n "spirv-std-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "spirv-std-types") (r "=0.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "15qmkkc9afgk5xy11fs5z90bj299l43l2rwrdw1hc5l5a2a2rkir")))

(define-public crate-spirv-std-macros-0.6.1 (c (n "spirv-std-macros") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "spirv-std-types") (r "=0.6.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "1pb887z0a6qa817yspcp79qxgv0sx0432z03h4pppb2c1dww5r0n")))

(define-public crate-spirv-std-macros-0.7.0 (c (n "spirv-std-macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "spirv-std-types") (r "=0.7.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0wjdsi5fna68kv07l784m0wijbxm3kavax2b3k33m7db4vbgmbxv")))

(define-public crate-spirv-std-macros-0.8.0 (c (n "spirv-std-macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "spirv-std-types") (r "=0.8.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1kxjx02q8nc702jp04674zc4jrkjwvigxg80ggp33vz7hmsyyn10")))

(define-public crate-spirv-std-macros-0.9.0 (c (n "spirv-std-macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "spirv-std-types") (r "=0.9.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0p7l3lmjd56306d4bmrbb69idpqzf59pfpgirxxfm5r8kyzpdxvk")))

