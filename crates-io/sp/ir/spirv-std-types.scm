(define-module (crates-io sp ir spirv-std-types) #:use-module (crates-io))

(define-public crate-spirv-std-types-0.4.0-alpha.14 (c (n "spirv-std-types") (v "0.4.0-alpha.14") (h "1pbsci60a2rdi008rnc5jsrn920rc3sfqfkg73gyaggaqrkhirwz")))

(define-public crate-spirv-std-types-0.4.0-alpha.15 (c (n "spirv-std-types") (v "0.4.0-alpha.15") (h "14xczdf24cz8rzxs0z20jccji4fkvfwzn5ijnwvymz3j2l5r7s7b")))

(define-public crate-spirv-std-types-0.4.0-alpha.16 (c (n "spirv-std-types") (v "0.4.0-alpha.16") (h "0xb28hsm31yx9dv65gx6vpswjwlns71fns93fpxdqa42j7ffmaav")))

(define-public crate-spirv-std-types-0.4.0-alpha.17 (c (n "spirv-std-types") (v "0.4.0-alpha.17") (h "0d4m2vhfk4ydh85mxksmcjw22lza51j8bcqf4hqqj26ggj4iapwv")))

(define-public crate-spirv-std-types-0.4.0 (c (n "spirv-std-types") (v "0.4.0") (h "1bysffz74jyz4dfgxsdi5xidj69s08xcxpmf8qdz4c4ip8fnw72s")))

(define-public crate-spirv-std-types-0.5.0 (c (n "spirv-std-types") (v "0.5.0") (h "0mqqsw6zjjkscx3v57lvkilh956ym0dyj0ngkpvkarxcsr2a57cj")))

(define-public crate-spirv-std-types-0.6.0 (c (n "spirv-std-types") (v "0.6.0") (h "1lc2pk98dj1wr07v0ggwzs2br898wm8ypazf5b9sdx66f84dxy5v")))

(define-public crate-spirv-std-types-0.6.1 (c (n "spirv-std-types") (v "0.6.1") (h "0qvgpj2dnjkiqdck9h5lcrs78wmd51hhy0wpmvmbwhabyqj773ss")))

(define-public crate-spirv-std-types-0.7.0 (c (n "spirv-std-types") (v "0.7.0") (h "0an4xblj47bb79dvj51p0z051wrh0yajsk82hz307j0vhmg8g0xb")))

(define-public crate-spirv-std-types-0.8.0 (c (n "spirv-std-types") (v "0.8.0") (h "06qf2c9ydpams4ra609qa1pbbj5vigxj6y464yksvrxrvs1j3kjw")))

(define-public crate-spirv-std-types-0.9.0 (c (n "spirv-std-types") (v "0.9.0") (h "1afk31n85djm2sfdydbzzzabmg5lwfscw3c4bjcv959dsyvifd57")))

