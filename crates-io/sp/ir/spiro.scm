(define-module (crates-io sp ir spiro) #:use-module (crates-io))

(define-public crate-spiro-1.0.0 (c (n "spiro") (v "1.0.0") (d (list (d (n "glifparser") (r "^1.2") (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1wv3bdjfmiawr52vk7m11i4dzxfjwjj5q4h8wkdp9xgfmarm8bk9") (f (quote (("default" "log" "glifparser"))))))

(define-public crate-spiro-1.0.1 (c (n "spiro") (v "1.0.1") (d (list (d (n "glifparser") (r "^1.2") (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "076p28pimxghfzdf8224r0p53fn7wgmhc9kvx3knjgxgjg2hknaf") (f (quote (("default" "log" "glifparser"))))))

(define-public crate-spiro-1.0.2 (c (n "spiro") (v "1.0.2") (d (list (d (n "glifparser") (r "^1.2") (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0415ixm847h80wcm5c0h1vx97p16r45sn2ic08y08dwrw4phcslh") (f (quote (("default" "log" "glifparser"))))))

(define-public crate-spiro-1.0.3 (c (n "spiro") (v "1.0.3") (d (list (d (n "glifparser") (r "^1.2") (o #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1qkv4ylqvyvsgr4p9knpzp8zmzxryqbd38jphkw1hizn06bqadld") (f (quote (("default" "log" "glifparser"))))))

