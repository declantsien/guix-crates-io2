(define-module (crates-io sp ir spirv-to-dxil) #:use-module (crates-io))

(define-public crate-spirv-to-dxil-0.1.1-beta.3 (c (n "spirv-to-dxil") (v "0.1.1-beta.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.0") (d #t) (k 2)) (d (n "spirv-to-dxil-sys") (r "^0.1.1-beta.3") (d #t) (k 0)))) (h "1j1gsw216x12h2h84zcb2dcxp2hi1j7x29slr95xh056zyy0ysmx")))

(define-public crate-spirv-to-dxil-0.2.1 (c (n "spirv-to-dxil") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.0") (d #t) (k 2)) (d (n "spirv-to-dxil-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1cdaykma2a4xkjw68lq4ybb2138fwckrf33np4897m9gqzxrhksg")))

(define-public crate-spirv-to-dxil-0.2.2 (c (n "spirv-to-dxil") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.0") (d #t) (k 2)) (d (n "spirv-to-dxil-sys") (r "^0.2.0") (d #t) (k 0)))) (h "133xaz2kr74ji55zcfhwa4xg22w255s2h91387rh3nbzz1415nhn") (f (quote (("included-bindings" "spirv-to-dxil-sys/included-bindings"))))))

(define-public crate-spirv-to-dxil-0.2.4 (c (n "spirv-to-dxil") (v "0.2.4") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.0") (d #t) (k 2)) (d (n "spirv-to-dxil-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0irr5f7rg9a3r7cwvb3mqn3wwwbhp07z676lkni89cnhv3vb0kr6") (f (quote (("included-bindings" "spirv-to-dxil-sys/included-bindings"))))))

(define-public crate-spirv-to-dxil-0.2.5 (c (n "spirv-to-dxil") (v "0.2.5") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.0") (d #t) (k 2)) (d (n "spirv-to-dxil-sys") (r "^0.2") (d #t) (k 0)))) (h "1lycvr9rgmrkw7knd3yfzcz6m770pisnzfi1rmcqzd8xzd7jkciy") (f (quote (("included-bindings" "spirv-to-dxil-sys/included-bindings"))))))

(define-public crate-spirv-to-dxil-0.2.6 (c (n "spirv-to-dxil") (v "0.2.6") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.0") (d #t) (k 2)) (d (n "spirv-to-dxil-sys") (r "^0.2") (d #t) (k 0)))) (h "121hd26kwsi3i4a3vzan57jffghssr5mhk82zq2yng4l1shg5h6p")))

(define-public crate-spirv-to-dxil-0.3.0 (c (n "spirv-to-dxil") (v "0.3.0") (d (list (d (n "bytemuck") (r "^1.13.0") (d #t) (k 0)) (d (n "spirv-to-dxil-sys") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1rvf6cbmdadz6if9gmahc8fr4x8hbjs14g2ngkcdiydihl2nlhgy")))

(define-public crate-spirv-to-dxil-0.4.0 (c (n "spirv-to-dxil") (v "0.4.0") (d (list (d (n "bytemuck") (r "^1.13") (d #t) (k 0)) (d (n "spirv-to-dxil-sys") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0yj1341i2jc5bx6hji2wpqfmw38maqrqd4p7ynl8br7haxilddxb")))

(define-public crate-spirv-to-dxil-0.4.1 (c (n "spirv-to-dxil") (v "0.4.1") (d (list (d (n "bytemuck") (r "^1.13") (d #t) (k 0)) (d (n "spirv-to-dxil-sys") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15aqslhr01slf9d78ca66rvvswc7vwykmm4rlnynlrsfzmgb7s0w")))

(define-public crate-spirv-to-dxil-0.4.6 (c (n "spirv-to-dxil") (v "0.4.6") (d (list (d (n "bytemuck") (r "^1.13") (d #t) (k 0)) (d (n "spirv-to-dxil-sys") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01jx13j71r9sw1hkv3lj0r3wzq0s1wh8039v8id1mh53jasv98cs")))

(define-public crate-spirv-to-dxil-0.4.7 (c (n "spirv-to-dxil") (v "0.4.7") (d (list (d (n "bytemuck") (r "^1.13") (d #t) (k 0)) (d (n "mach-siegbert-vogt-dxcsa") (r "^0.1.2") (d #t) (k 0)) (d (n "spirv-to-dxil-sys") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03jbknaf92fyfnh1qxf7fklnqd568x3af63f5p7hp3r8ihcb8gss")))

