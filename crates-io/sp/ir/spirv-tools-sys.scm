(define-module (crates-io sp ir spirv-tools-sys) #:use-module (crates-io))

(define-public crate-spirv-tools-sys-0.1.0 (c (n "spirv-tools-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0sb7injc57aiyvds2cvy2m04ivxl1pi57hz6lb4nw40wkff10gvj") (f (quote (("use-installed-tools") ("use-compiled-tools"))))))

(define-public crate-spirv-tools-sys-0.1.1 (c (n "spirv-tools-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0lpv8yl558746lvmdy33i19gsd5accxh3hv50s60wf1ifwhka7zy") (f (quote (("use-installed-tools") ("use-compiled-tools") ("default" "use-compiled-tools"))))))

(define-public crate-spirv-tools-sys-0.2.0 (c (n "spirv-tools-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1lckngvyx1dx6xjgi8xfmz1vh3h2v23h8vbb2rfd9kz9xwxspcfw") (f (quote (("use-installed-tools") ("use-compiled-tools") ("default" "use-compiled-tools"))))))

(define-public crate-spirv-tools-sys-0.3.0 (c (n "spirv-tools-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0918ssgjixh2y6zab0sfd52k04xf0gvrp7wdg2n65y3jdq1f7idm") (f (quote (("use-installed-tools") ("use-compiled-tools") ("default" "use-compiled-tools"))))))

(define-public crate-spirv-tools-sys-0.4.0 (c (n "spirv-tools-sys") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "09vljb1q7mfc245wxwzzva389xly2gd5yfwnqf478g1q7xwvx7m4") (f (quote (("use-installed-tools") ("use-compiled-tools") ("default" "use-compiled-tools"))))))

(define-public crate-spirv-tools-sys-0.4.1 (c (n "spirv-tools-sys") (v "0.4.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "10qr10qb6yf1i65r2xn6pr1v8cy0j0xxrnwnsinzy89p19w87c6b") (f (quote (("use-installed-tools") ("use-compiled-tools") ("default" "use-compiled-tools"))))))

(define-public crate-spirv-tools-sys-0.5.0 (c (n "spirv-tools-sys") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "068ijvhh52v6lzik0gkzrknj4ac5zqdk4xiq6yhggk36js0xnrxr") (f (quote (("use-installed-tools") ("use-compiled-tools") ("default" "use-compiled-tools"))))))

(define-public crate-spirv-tools-sys-0.6.0 (c (n "spirv-tools-sys") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "17d5cbxmb4sd0379qcyjvk3bj1xc9bl3b2nwn7zfvdd9nasb1019") (f (quote (("use-installed-tools") ("use-compiled-tools") ("default" "use-compiled-tools")))) (l "spirv-tools")))

(define-public crate-spirv-tools-sys-0.7.0 (c (n "spirv-tools-sys") (v "0.7.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "16lwclbw9k8p4y3rf2mpnx7np55m0h989gfnvi86pkb9cjfjvcxl") (f (quote (("use-installed-tools") ("use-compiled-tools") ("default" "use-compiled-tools")))) (l "spirv-tools")))

(define-public crate-spirv-tools-sys-0.8.0 (c (n "spirv-tools-sys") (v "0.8.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1rysbg2763hn681bniz1fdwajxaq89vj8vqa05p8b9ksm5aqprj8") (f (quote (("use-installed-tools") ("use-compiled-tools") ("default" "use-compiled-tools")))) (l "spirv-tools")))

