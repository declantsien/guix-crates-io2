(define-module (crates-io sp ir spirv-compiler) #:use-module (crates-io))

(define-public crate-spirv-compiler-0.1.0 (c (n "spirv-compiler") (v "0.1.0") (d (list (d (n "shaderc") (r "^0.7") (d #t) (k 0)))) (h "1q46y6h7ija0zivvxdgzr6y42b1kgzyq8cvnj552y4lbxldh906d")))

(define-public crate-spirv-compiler-0.2.0 (c (n "spirv-compiler") (v "0.2.0") (d (list (d (n "shaderc") (r "^0.7") (d #t) (k 0)))) (h "05fwn6f7yk9slkzb0p4pqmmqbyj8wvbzwbi89npncrjq2gkqdr2l")))

