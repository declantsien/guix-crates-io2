(define-module (crates-io sp ir spire-achievements-tracker) #:use-module (crates-io))

(define-public crate-spire-achievements-tracker-0.1.2 (c (n "spire-achievements-tracker") (v "0.1.2") (d (list (d (n "eframe") (r "^0.15.0") (d #t) (k 0)) (d (n "egui") (r "^0.15.0") (d #t) (k 0)) (d (n "rodio") (r "^0.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)))) (h "0mnd7fqvbd5dpxk5k46hzrfzym4ikaqy34hippy95vjpq22m6vjv")))

(define-public crate-spire-achievements-tracker-0.1.3 (c (n "spire-achievements-tracker") (v "0.1.3") (d (list (d (n "eframe") (r "^0.15.0") (d #t) (k 0)) (d (n "egui") (r "^0.15.0") (d #t) (k 0)) (d (n "rodio") (r "^0.17.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)))) (h "0l6wydq79vyxzlb2j2y9ww2k0kwy4b53ngcsr45nwgilrh9v4f5v")))

