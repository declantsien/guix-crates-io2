(define-module (crates-io sp ir spirq-spvasm) #:use-module (crates-io))

(define-public crate-spirq-spvasm-0.1.0 (c (n "spirq-spvasm") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "half") (r "^2.3") (f (quote ("num-traits"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "spirq") (r "^1.1.0") (d #t) (k 0)) (d (n "spirq-core") (r "^1.0.1") (d #t) (k 0)))) (h "1pcz1cn94v4c258by5sazw2qfhx6m5fpd5gkgi6y12sfycrchnyw")))

(define-public crate-spirq-spvasm-0.1.1 (c (n "spirq-spvasm") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "half") (r "^2.3") (f (quote ("num-traits"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "spirq") (r "^1.1.0") (d #t) (k 0)) (d (n "spirq-core") (r "^1.0.1") (d #t) (k 0)))) (h "0hag984fw68ai71g2sqp0v6mf7mhpgirgs2sj7yfw74gavc8xqb4")))

(define-public crate-spirq-spvasm-0.1.2 (c (n "spirq-spvasm") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "half") (r "^2.3") (f (quote ("num-traits"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "spirq") (r "^1.1.1") (d #t) (k 0)) (d (n "spirq-core") (r "^1.0.2") (d #t) (k 0)))) (h "0pcd5hych7pk5v9y7awvir3nibyxk60dxcxjz7zg1wc09axs5a8q")))

(define-public crate-spirq-spvasm-0.1.3 (c (n "spirq-spvasm") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "half") (r "^2.3") (f (quote ("num-traits"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "spirq") (r "^1.1.1") (d #t) (k 0)) (d (n "spirq-core") (r "^1.0.2") (d #t) (k 0)))) (h "0q85kg2xxk5g5lfrc7ag079hpkc5bzm7jz2rnj71bp5pp3an15k4")))

(define-public crate-spirq-spvasm-0.1.4 (c (n "spirq-spvasm") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "half") (r "^2.3") (f (quote ("num-traits"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "spirq") (r "^1.1.1") (d #t) (k 0)) (d (n "spirq-core") (r "^1.0.4") (d #t) (k 0)))) (h "0lqg83lbb9lfwrk83n8l9sssyk1ygir3mrpqk7w2d79cbdmc8h89")))

