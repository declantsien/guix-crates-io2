(define-module (crates-io sp ir spirv-utils) #:use-module (crates-io))

(define-public crate-spirv-utils-0.1.0 (c (n "spirv-utils") (v "0.1.0") (h "1bpsirq9vr3ks0xnmsn2z6qkwarmsxmri5ia5mcv0pccn7srgaf7")))

(define-public crate-spirv-utils-0.2.0 (c (n "spirv-utils") (v "0.2.0") (d (list (d (n "lalrpop-util") (r "^0.11") (d #t) (k 1)))) (h "0i1khynylihfl54xy1ii0zbkjj0jpchcx17xgjar13cq1zi7zykh")))

(define-public crate-spirv-utils-0.2.1 (c (n "spirv-utils") (v "0.2.1") (d (list (d (n "lalrpop-util") (r "^0.11") (d #t) (k 1)))) (h "1cxr7g3anxl4jlafcq2m1acjmaxms9q9q6apbblxy0mb75klkfw3")))

