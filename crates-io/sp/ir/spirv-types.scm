(define-module (crates-io sp ir spirv-types) #:use-module (crates-io))

(define-public crate-spirv-types-0.4.0-alpha.6 (c (n "spirv-types") (v "0.4.0-alpha.6") (h "1a5dvlx76xy1n6v269qfmcqgv2v0irgy55da35z0yi357gvcvssd")))

(define-public crate-spirv-types-0.4.0-alpha.7 (c (n "spirv-types") (v "0.4.0-alpha.7") (h "183wc3xsksw3xjzrxjhi74l40m9b6hx5yjs5wxhl5h20al58hzrd")))

(define-public crate-spirv-types-0.4.0-alpha.8 (c (n "spirv-types") (v "0.4.0-alpha.8") (h "1lvrxf77kgl1bmvyhpc2l99y4ngb6xiyxqbjylddbicmk5vh9zfg")))

(define-public crate-spirv-types-0.4.0-alpha.10 (c (n "spirv-types") (v "0.4.0-alpha.10") (h "0lqwy073c7954adj68ais065kiqd5y83ab7rwmsj2p8q29r3byqw")))

(define-public crate-spirv-types-0.4.0-alpha.11 (c (n "spirv-types") (v "0.4.0-alpha.11") (h "0sq5q5j791i47zkza742z1mfj7vc12krqyx4iy6zz4yk4y9c3skb")))

(define-public crate-spirv-types-0.4.0-alpha.12 (c (n "spirv-types") (v "0.4.0-alpha.12") (h "0sxcic2n04xzywww3c7mj9ah8iwr20vz3d6xvsgla1y7gs7lpk3i")))

(define-public crate-spirv-types-0.4.0-alpha.13 (c (n "spirv-types") (v "0.4.0-alpha.13") (h "05bl3wvy25x4r7sm5mwbd2i16772gnf4nwf572hwb2zrxy0vaxiy")))

(define-public crate-spirv-types-0.4.0-alpha.14 (c (n "spirv-types") (v "0.4.0-alpha.14") (h "0kl4jm5fnjd4rp56il9pv6m977ndrnlhj4spwi6lj6q5nllzw2bm")))

