(define-module (crates-io sp ir spirq-reflect) #:use-module (crates-io))

(define-public crate-spirq-reflect-0.0.1 (c (n "spirq-reflect") (v "0.0.1") (d (list (d (n "clap") (r "^4.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "spirq") (r "^0.6.0") (d #t) (k 0)))) (h "0ljqmny67ggwki3dz4ms1ar5fx1pkbi496n84zh7qg18gg14n48r")))

(define-public crate-spirq-reflect-0.1.0 (c (n "spirq-reflect") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "spirq") (r "^0.6.0") (d #t) (k 0)))) (h "0sm1mww3qkqfh68rr43c1vs053f2gps1c2xsr9s5nsxm1nh2lqr3")))

(define-public crate-spirq-reflect-0.1.1 (c (n "spirq-reflect") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "spirq") (r "^0.6.0") (d #t) (k 0)))) (h "1yr8wzrjc3q7nygg3291n9abzr3x01p0z1py642j8ylh8pz16zh5")))

