(define-module (crates-io sp ir spirv_headers) #:use-module (crates-io))

(define-public crate-spirv_headers-1.1.2 (c (n "spirv_headers") (v "1.1.2") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.1") (d #t) (k 0)))) (h "00kb1xjvvh0p6czbx1kg4m6d9b0n7xvcppf42k300szm879jrhs1")))

(define-public crate-spirv_headers-1.1.3 (c (n "spirv_headers") (v "1.1.3") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.1") (d #t) (k 0)))) (h "06siw9sqcjm6i3ii59rx2ws6w0cnxha7hjmgcxinrl7wg8lh0bvv")))

(define-public crate-spirv_headers-1.1.4 (c (n "spirv_headers") (v "1.1.4") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.1") (d #t) (k 0)))) (h "0h41sj9sybgj0754563wn5ra9fr0l847g0y1wxfmpzm0c6bbwrmy")))

(define-public crate-spirv_headers-1.1.5 (c (n "spirv_headers") (v "1.1.5") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.1") (d #t) (k 0)))) (h "1q3l4imny0d6c8543xi4ii4j1hr88d8ifibsf7bpjpcl877vzlrb")))

(define-public crate-spirv_headers-1.1.6 (c (n "spirv_headers") (v "1.1.6") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.1") (d #t) (k 0)))) (h "0wqm2w3lp0j1ry7q3f1l873z66ibzn3kssg92iy5yrgjjl5n75m9")))

(define-public crate-spirv_headers-1.1.7 (c (n "spirv_headers") (v "1.1.7") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.1") (d #t) (k 0)))) (h "1xzyb60am0i5v0vn9cqy65s32dvc5xg38y1gwvmysmw4k92a53v0")))

(define-public crate-spirv_headers-1.1.8 (c (n "spirv_headers") (v "1.1.8") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.1") (d #t) (k 0)))) (h "01y5sc9y0c19amjpq8mldajw0k8i96yfkhfh1q8fcr3igg6nbhxa")))

(define-public crate-spirv_headers-1.0.12 (c (n "spirv_headers") (v "1.0.12") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.1") (d #t) (k 0)))) (h "1jaqm7mrj0nkr82l6v5wg9lwvxg7c243f5i2d3m8y014yjj0as4w")))

(define-public crate-spirv_headers-1.2.2 (c (n "spirv_headers") (v "1.2.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.1") (d #t) (k 0)))) (h "0b5hpwgv89mk43hg1lsr5gjkbb06wa8q7159sqkkffpjrsxgkdmm")))

(define-public crate-spirv_headers-1.3.1 (c (n "spirv_headers") (v "1.3.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.1") (d #t) (k 0)))) (h "1m73xby4xnsgjamq1i35zr86cjmcf1lqr39d5nryszwzgn14k2iq")))

(define-public crate-spirv_headers-1.3.4 (c (n "spirv_headers") (v "1.3.4") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.1") (d #t) (k 0)))) (h "0il8sgh0w3yighcvpim792lcjz0ka1cd8rhg050msfsrknkq3szb")))

(define-public crate-spirv_headers-1.4.1 (c (n "spirv_headers") (v "1.4.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1zzg3qmzsdqbw6r3jyr36q6g4ysd9c2bv4c83imi9pf5xiv68al4") (y #t)))

(define-public crate-spirv_headers-1.4.2 (c (n "spirv_headers") (v "1.4.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "08i60gzn86n5sjy2y2bclz4ynbpr9vs3rax3zwki4j0n7nc1h51z")))

(define-public crate-spirv_headers-1.5.0 (c (n "spirv_headers") (v "1.5.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0b2kgh2zpj6vpg6rafd1lp5ranbnh4spwmrmyffhdb5i60ji6nqz") (f (quote (("serialize" "serde") ("deserialize" "serde"))))))

(define-public crate-spirv_headers-1.5.1 (c (n "spirv_headers") (v "1.5.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1aikrqbxl4mb66h2zk72m9i4sscn1igrffvxj7g71hal9mc8jcd4") (f (quote (("serialize" "serde") ("deserialize" "serde")))) (y #t)))

