(define-module (crates-io sp ir spiropath) #:use-module (crates-io))

(define-public crate-spiropath-0.1.0 (c (n "spiropath") (v "0.1.0") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "ordered-float") (r "^2.2.0") (d #t) (k 0)) (d (n "svg2polylines") (r "^0.5.2") (d #t) (k 0)))) (h "0hihyz943kh7yf0fm7dxks61c408jv5iy5yipswvs3mbjll78msd") (y #t)))

(define-public crate-spiropath-0.1.1 (c (n "spiropath") (v "0.1.1") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "ordered-float") (r "^2.2.0") (d #t) (k 0)) (d (n "svg2polylines") (r "^0.5.2") (d #t) (k 0)))) (h "1653pbq7c737zlqqbch3ww8i8w4fdd9kyh2iqjvzvd5xrd0n24ag")))

