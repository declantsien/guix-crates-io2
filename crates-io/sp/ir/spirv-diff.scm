(define-module (crates-io sp ir spirv-diff) #:use-module (crates-io))

(define-public crate-spirv-diff-0.1.0 (c (n "spirv-diff") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "spirv-tools") (r "^0.7.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "17f8avwgjqpm2scismkjlj8fj0avcfw4valwxmkppnmixwf3aqfx")))

