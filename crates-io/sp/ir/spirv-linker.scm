(define-module (crates-io sp ir spirv-linker) #:use-module (crates-io))

(define-public crate-spirv-linker-0.1.0 (c (n "spirv-linker") (v "0.1.0") (d (list (d (n "rspirv") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "topological-sort") (r "^0.1") (d #t) (k 0)))) (h "1jqsn2x7xyyd8i4m525wka86z3a2zb822wnm32g811vkxian48sx")))

