(define-module (crates-io sp ir spira) #:use-module (crates-io))

(define-public crate-spira-0.0.1 (c (n "spira") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.12") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0mv3a0qhn194by0nlvz6bv7bmdj3kxvgxmw6s2br5ma3yi5xf10d")))

(define-public crate-spira-0.0.2 (c (n "spira") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.12") (d #t) (k 0)))) (h "06nvcn4hqlrian1rvjjql9pdq2z1k5ndhymmvgf91hxw3qj5pyjj")))

(define-public crate-spira-0.0.3 (c (n "spira") (v "0.0.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.12") (d #t) (k 0)))) (h "1jwknx2fvg2m4nrii6ff36gzj5rmz0386nb8m0sl6p4mbq4zvnn1")))

(define-public crate-spira-0.0.4 (c (n "spira") (v "0.0.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.12") (d #t) (k 0)))) (h "0kc8sxgyia5488hallq21ad08la43iml767qfpi3wfdg3mzzq6dq")))

(define-public crate-spira-0.0.5 (c (n "spira") (v "0.0.5") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.12") (d #t) (k 0)))) (h "03liz0mmffril4dfdm8jrwg7hgkbssxq6y4fia8hxs5y4i1zablf")))

(define-public crate-spira-0.0.6 (c (n "spira") (v "0.0.6") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^2.0") (d #t) (k 0)))) (h "0vch45w89c9h415w85gg692aynh3df7wpb7gs96m2fdlsnpgz0dx")))

