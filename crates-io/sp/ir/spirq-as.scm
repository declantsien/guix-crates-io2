(define-module (crates-io sp ir spirq-as) #:use-module (crates-io))

(define-public crate-spirq-as-0.1.0 (c (n "spirq-as") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spirq-spvasm") (r "^0.1.0") (d #t) (k 0)))) (h "1c15x5zkvsqq8d0h9h0736ir7n61fw0wj4xng8shmhx1r338ygnb")))

(define-public crate-spirq-as-0.1.1 (c (n "spirq-as") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spirq-spvasm") (r "^0.1.1") (d #t) (k 0)))) (h "0ziyyl941j2ym4inwwn4b6b1hsi19dixyjpq2wk5zdpigc5sq41d")))

(define-public crate-spirq-as-0.1.2 (c (n "spirq-as") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spirq-spvasm") (r "^0.1.2") (d #t) (k 0)))) (h "07346dr9a1smab4ldr0rp5b86v0qfran2ribj13jfb2ljr3yidz6")))

(define-public crate-spirq-as-0.1.3 (c (n "spirq-as") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spirq-spvasm") (r "^0.1.3") (d #t) (k 0)))) (h "0r4xxywg2lg4jsdpby78jw7ycqrjp1nhmxc19xisq2a3bq1dfyrf")))

(define-public crate-spirq-as-0.1.4 (c (n "spirq-as") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spirq-spvasm") (r "^0.1.4") (d #t) (k 0)))) (h "063rcsmbi80nsy08axqmv76rgzp9s3ynnxm4m488cj4lk5pxx7qg")))

