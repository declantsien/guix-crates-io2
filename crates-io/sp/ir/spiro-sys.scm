(define-module (crates-io sp ir spiro-sys) #:use-module (crates-io))

(define-public crate-spiro-sys-0.1.0 (c (n "spiro-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dos2unix") (r "=0.1.0") (d #t) (k 1)))) (h "1qs1y52zkgal3xx67a0fn2vq578llyai6n2l0k03qdiygyvm28zh") (l "spiro")))

(define-public crate-spiro-sys-0.1.1 (c (n "spiro-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.55") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dos2unix") (r "=0.1.0") (d #t) (t "cfg(target_family = \"windows\")") (k 1)))) (h "0cc1238813b62azjiz0qqfnr2yp4wy1p04bz055wjkdg541wlj5i") (f (quote (("build-bindgen" "bindgen")))) (l "spiro")))

