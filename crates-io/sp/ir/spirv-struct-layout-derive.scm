(define-module (crates-io sp ir spirv-struct-layout-derive) #:use-module (crates-io))

(define-public crate-spirv-struct-layout-derive-0.1.0 (c (n "spirv-struct-layout-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spirq") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0f06hw0v12572igjg3drh7c5qk49x8zk18sraw4i9zim9i9qssw4")))

(define-public crate-spirv-struct-layout-derive-0.1.1 (c (n "spirv-struct-layout-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spirq") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ncchp6avywkijdci3lswp8bc1nppnizmv9r25pywj6czwz9j5ip")))

(define-public crate-spirv-struct-layout-derive-0.1.2 (c (n "spirv-struct-layout-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spirq") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1yckpvdam4ykdgk39dacqkb0ncfi77yjadr8xwqyj1pfc6ii27x4")))

(define-public crate-spirv-struct-layout-derive-0.1.3 (c (n "spirv-struct-layout-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spirq") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06khdajis6vgmg86rx4g7arld5rcbn50p1mm2cazind3m4a5ppfy")))

(define-public crate-spirv-struct-layout-derive-0.1.4 (c (n "spirv-struct-layout-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spirq") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nnw2nz63c2myz8gq10aqmj1n04mnbsrzzpcd8sx9fk7nhz9xp5y")))

