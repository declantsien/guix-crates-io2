(define-module (crates-io sp ir spirv) #:use-module (crates-io))

(define-public crate-spirv-0.1.0 (c (n "spirv") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1vj21ds71jcqjg2b0ah9a3734zxfs1i5fsybznbnfgs3sxla5hnr") (y #t)))

(define-public crate-spirv-0.1.0+1.5.4 (c (n "spirv") (v "0.1.0+1.5.4") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kk6rzibz22216i0wnn9zp794r4vdczb6jvj2lczikjvwn54jjn0") (f (quote (("serialize" "serde") ("deserialize" "serde"))))))

(define-public crate-spirv-0.2.0+1.5.4 (c (n "spirv") (v "0.2.0+1.5.4") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0c7qjinqpwcfxk00qx0j46z7i31lnzg2qnnar3gz3crxzqwglsr4") (f (quote (("serialize" "serde") ("deserialize" "serde"))))))

(define-public crate-spirv-0.3.0+sdk-1.3.268.0 (c (n "spirv") (v "0.3.0+sdk-1.3.268.0") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0i3qj7yvvprai1s03dvll2gkfy8398nl64wvllkhaaa4vh1i197d") (f (quote (("serialize" "serde" "bitflags/serde") ("deserialize" "serde" "bitflags/serde"))))))

