(define-module (crates-io sp ir spirv-layout) #:use-module (crates-io))

(define-public crate-spirv-layout-0.1.0 (c (n "spirv-layout") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0b7rm8gy7fwdh1d4nhl5vda55i8qllsq9r903251r0kirpr775vm") (y #t)))

(define-public crate-spirv-layout-0.1.1 (c (n "spirv-layout") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "196bysq2q14i329hw888mjgdw20z71jqrsl84gj53p2f0ackppas") (y #t)))

(define-public crate-spirv-layout-0.1.2 (c (n "spirv-layout") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1qw85nqgy0kw0a6c9v6jlvr7qk6721pqf1l890xn2k9g8m6d05id") (y #t)))

(define-public crate-spirv-layout-0.1.3 (c (n "spirv-layout") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "00pfnma59iljfqvm1aslrd4068bk99f6sqv087k7986pywkvz8kn")))

(define-public crate-spirv-layout-0.2.0 (c (n "spirv-layout") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1b7x4b13952dp75vkgppzz84k4x40sarzid2bxgxrd8fsqha6jbi")))

(define-public crate-spirv-layout-0.2.1 (c (n "spirv-layout") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "078n1yp00j1a86jci2yv1pb8a8wclz0v255vp3ij19km75j1iq93")))

(define-public crate-spirv-layout-1.0.0 (c (n "spirv-layout") (v "1.0.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1g17p2gq26348k81rdqqxkqq10cmj1wpzyhh2pna8zbilivbkbqp") (y #t)))

(define-public crate-spirv-layout-0.3.0 (c (n "spirv-layout") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0gz7rbsspq56wify5mafv706q84f2xpkqkvgac9xb5zw6gv5n761")))

(define-public crate-spirv-layout-0.4.0 (c (n "spirv-layout") (v "0.4.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "12qkakdwyv89l7p3rbvr4krawka7llzmpssf1wvcslv9x3qfnpxm")))

