(define-module (crates-io sp ir spire) #:use-module (crates-io))

(define-public crate-spire-0.1.0 (c (n "spire") (v "0.1.0") (d (list (d (n "spire-core") (r "^0.1.0") (d #t) (k 0)) (d (n "spire-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (o #t) (k 0)))) (h "15c9qkl8dx3xwslar19326pwyspj3dlql7k4wnq1xb2nb251z703") (f (quote (("util") ("full") ("default")))) (s 2) (e (quote (("tracing" "dep:tracing") ("macros" "dep:spire-macros"))))))

