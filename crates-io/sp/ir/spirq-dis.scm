(define-module (crates-io sp ir spirq-dis) #:use-module (crates-io))

(define-public crate-spirq-dis-0.1.0 (c (n "spirq-dis") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spirq-spvasm") (r "^0.1.0") (d #t) (k 0)))) (h "190x297dbypc404r80l4qz0kkpzfniw7lhv8v8lxxqcgmxw4sc9j")))

(define-public crate-spirq-dis-0.1.1 (c (n "spirq-dis") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spirq-spvasm") (r "^0.1.1") (d #t) (k 0)))) (h "0vgyb4la3lb7v9frvw6d854w51wap5gqbr5hj9ik1k9k4xpl0ivp")))

(define-public crate-spirq-dis-0.1.2 (c (n "spirq-dis") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spirq-spvasm") (r "^0.1.2") (d #t) (k 0)))) (h "13ksq5fxl0w3hdc9pyy7v0lgc8f9j5rnlln3rrw335wh5yilhg9h")))

(define-public crate-spirq-dis-0.1.3 (c (n "spirq-dis") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spirq-spvasm") (r "^0.1.3") (d #t) (k 0)))) (h "0mprpckqbr44gbxa0d9qa82q1jj28al9iahzlgd80a0b9f9j6526")))

(define-public crate-spirq-dis-0.1.4 (c (n "spirq-dis") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "spirq-spvasm") (r "^0.1.4") (d #t) (k 0)))) (h "1wpr3qdw0cm67jm0krll1g5cldj92imm50lrkz9l0zwmaqb6gh1b")))

