(define-module (crates-io sp ir spirq-core) #:use-module (crates-io))

(define-public crate-spirq-core-1.0.0 (c (n "spirq-core") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.4") (d #t) (k 0)) (d (n "spirv") (r "^0.2") (d #t) (k 0)))) (h "1594cy3sinzll58asn0r6310p3k79b26hqvvfybbkgwnim4x2kbp")))

(define-public crate-spirq-core-1.0.1 (c (n "spirq-core") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "half") (r "^2.3") (f (quote ("num-traits"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.4") (d #t) (k 0)) (d (n "spirv") (r "^0.2") (d #t) (k 0)))) (h "1qgnncr2kgqy180ramz326nqhpwk2lczmlj64a9gr9bfz5rks99b")))

(define-public crate-spirq-core-1.0.2 (c (n "spirq-core") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "half") (r "^2.3") (f (quote ("num-traits"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.4") (d #t) (k 0)) (d (n "spirv") (r "^0.2") (d #t) (k 0)))) (h "1bvgw57g4z0vixzg8dwalaqh6c23sqgkxfw9xzmwdrv3cbhklzb4")))

(define-public crate-spirq-core-1.0.3 (c (n "spirq-core") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "half") (r "^2.3") (f (quote ("num-traits"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.4") (d #t) (k 0)) (d (n "spirv") (r ">=0.2") (d #t) (k 0)))) (h "0xb00gb1qhshy01byry2wh321wlhigip93nqif31fbbj1p4qmhi6")))

(define-public crate-spirq-core-1.0.4 (c (n "spirq-core") (v "1.0.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "half") (r "^2.3") (f (quote ("num-traits"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.4") (d #t) (k 0)) (d (n "spirv") (r "^0.3") (d #t) (k 0)))) (h "12xrcvs0mhqwzfvap18wvnm2jlzj5js3iz6v5drka7spz3mdgv8a")))

