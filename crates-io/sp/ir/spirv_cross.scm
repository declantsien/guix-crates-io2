(define-module (crates-io sp ir spirv_cross) #:use-module (crates-io))

(define-public crate-spirv_cross-0.1.0 (c (n "spirv_cross") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0gc28hypqk39bl4h053j8xcdrvx8k7chi1i4s7xq8svfkmx7n83g")))

(define-public crate-spirv_cross-0.1.1 (c (n "spirv_cross") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1i6vikcd57kbf94q2c17mv1v7iy11ijcgzx1v5mgvhp966jxsysh")))

(define-public crate-spirv_cross-0.1.2 (c (n "spirv_cross") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1isylb6qh14xqif5aakp64anzmldbarvay94h8ij76fa1l5vkws8")))

(define-public crate-spirv_cross-0.1.3 (c (n "spirv_cross") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "04jhyvyb74wk93rzgh9mvn4azn97dqjlblna4hxph33v37l2jyn6")))

(define-public crate-spirv_cross-0.1.4 (c (n "spirv_cross") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0m5qk7lh10511yfakahliy8v5fl33x0wx9d7p030gpm0p6xjg1b2")))

(define-public crate-spirv_cross-0.1.5 (c (n "spirv_cross") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1swb1dics4f453js5ls0xxiqh741vv59nwv6arrv8aid7c93b66v")))

(define-public crate-spirv_cross-0.1.6 (c (n "spirv_cross") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1irn1y26gjad1s0rwnbjgivn7rqkq8k6ksvmgy52n4jn72fh4zy5")))

(define-public crate-spirv_cross-0.1.7 (c (n "spirv_cross") (v "0.1.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0qbv60w7bxs8mnsmcgbfrnrv3qkxwrv0br5y6dv6xb6c1f475xvj")))

(define-public crate-spirv_cross-0.1.8 (c (n "spirv_cross") (v "0.1.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0waddiz2k1y43pi1hr52sc4m3ddq4zgm7n9541q73lihzzskhdh9")))

(define-public crate-spirv_cross-0.1.9 (c (n "spirv_cross") (v "0.1.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1j99k1g787lfqyfrw90xkhd4f1hmar45i2664gdw7sx804qc12li") (y #t)))

(define-public crate-spirv_cross-0.2.0 (c (n "spirv_cross") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0n3z89lddwgikxqgasxx835k4ipjwf0ijfr6hvvhw9xhq2a9g864")))

(define-public crate-spirv_cross-0.2.1 (c (n "spirv_cross") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "05m2iyddkkrsi6hzz3p4nlcsi01k1d0qw89hc0h20xf62zap21hc")))

(define-public crate-spirv_cross-0.3.0 (c (n "spirv_cross") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0zrmykjc8zbqvsn25cx66p7al9bk2dl9bq0nldsmn4ckqr26zgj1")))

(define-public crate-spirv_cross-0.3.1 (c (n "spirv_cross") (v "0.3.1") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)))) (h "0mps9k1imjgwrlx672a000vvx0z0r772nbfvynny4idqxv4mrwmh")))

(define-public crate-spirv_cross-0.4.0 (c (n "spirv_cross") (v "0.4.0") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)))) (h "1w0ryhjhqlm6105n1w889dfxs2j69f3fgyfpgixwpgr3wwa8isag")))

(define-public crate-spirv_cross-0.4.1 (c (n "spirv_cross") (v "0.4.1") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)))) (h "04qymn7f2py50crm7fwq1za31fdn2vpdg7ax34x3xqzaqap117bh")))

(define-public crate-spirv_cross-0.4.2 (c (n "spirv_cross") (v "0.4.2") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)))) (h "13xldszfwc87m6pyi7mm4hrws3y44yznv49rnib1dr7i434pxjfi")))

(define-public crate-spirv_cross-0.4.3 (c (n "spirv_cross") (v "0.4.3") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)))) (h "0hfva0z2affjh60w2s5z1dhpg3wl6ksb73w8i5fwf23k3rpnf7ij")))

(define-public crate-spirv_cross-0.4.4 (c (n "spirv_cross") (v "0.4.4") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)))) (h "0w5ambnpv5phz1kwq17qnssrcy0qdpwjzrv2f7pyyx91rwsnmc1i")))

(define-public crate-spirv_cross-0.4.5 (c (n "spirv_cross") (v "0.4.5") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "0cx8n1gz0bx9lgmq0lyygjrcdylh55z437v5lv7s7dqgkj0x16rz")))

(define-public crate-spirv_cross-0.4.6 (c (n "spirv_cross") (v "0.4.6") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "0913yl3lqyq58zkl9mqqagi8rjkj4wjn993y3ga9l84v10b29gwv")))

(define-public crate-spirv_cross-0.5.0 (c (n "spirv_cross") (v "0.5.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "06q7c18bzcgybql2rp425xzaj0xxff3vz8xwp1zbk7lz4fcmrh2s")))

(define-public crate-spirv_cross-0.6.0 (c (n "spirv_cross") (v "0.6.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "0gzby0y7xl1am1fcfmx60m5gh428z43dqichdqj93xjysxm98k4h")))

(define-public crate-spirv_cross-0.7.0 (c (n "spirv_cross") (v "0.7.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "0qbhk7zq25lkgzvs4n3f4dplkrl9gwzwfc2dm1pwg1j2i7sjazbi")))

(define-public crate-spirv_cross-0.7.1 (c (n "spirv_cross") (v "0.7.1") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "0hfwzb4m2h1n2r58fh57p2qwzxiwd9k12sgr79n133agznlsg26j")))

(define-public crate-spirv_cross-0.7.2 (c (n "spirv_cross") (v "0.7.2") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "02xh97gn1cxxky097lirm6fg2zx2k8dj2b3c3nzvv2fpi4vwihv5")))

(define-public crate-spirv_cross-0.7.3 (c (n "spirv_cross") (v "0.7.3") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "1gpxy7p0i0d5c1wj7g2hapdkyfkancfxznybvmq47m5rj7v6y82l")))

(define-public crate-spirv_cross-0.7.4 (c (n "spirv_cross") (v "0.7.4") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "0y21ywgcci5j5qkrgs14gvxh44739x9ckxs785i84gv3yx88m8p0")))

(define-public crate-spirv_cross-0.8.0 (c (n "spirv_cross") (v "0.8.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "033m23f7ib1pcn97vzp1k1ask5v6847zydhmxajf8syilr83hjkr")))

(define-public crate-spirv_cross-0.8.1 (c (n "spirv_cross") (v "0.8.1") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "1kw3vay784pv1qxbs123kc5kq5l3mjyfvrvzs22d4p2xvvji4yiq")))

(define-public crate-spirv_cross-0.9.0 (c (n "spirv_cross") (v "0.9.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "1qbcvyhl78d3ikchg3fl1y02ajv10lm8lvny77bx44spyg237y0s")))

(define-public crate-spirv_cross-0.9.1 (c (n "spirv_cross") (v "0.9.1") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "1gqd8b3jdzjmvl2prfp0kdiq47pxw9k9ggiq9sdvpynwnxx1ig73")))

(define-public crate-spirv_cross-0.9.2 (c (n "spirv_cross") (v "0.9.2") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "0sav4c3b9q8ndb7gi0fl62zcr76h0sg02xfd9fclgncsppfhmhz5")))

(define-public crate-spirv_cross-0.9.3 (c (n "spirv_cross") (v "0.9.3") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "0n4c5mkb79dm8wy6rpldld88bdzvszydcibr0af17v88icd0cm1d")))

(define-public crate-spirv_cross-0.9.4 (c (n "spirv_cross") (v "0.9.4") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "0cz8dgmlmw54w3pqcw5mwqmkbp95zn651m5f27ww7r6j5h3czmw3")))

(define-public crate-spirv_cross-0.9.5 (c (n "spirv_cross") (v "0.9.5") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "0xqa933xrx5ar4drmd0cz2rndqf7s99nwykw50ynjq220q0zn38p")))

(define-public crate-spirv_cross-0.9.6 (c (n "spirv_cross") (v "0.9.6") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "0j45s80703z7p7widq7p4lca12qv32fdzp7k1hb9jsg2k7djx9kk")))

(define-public crate-spirv_cross-0.9.7 (c (n "spirv_cross") (v "0.9.7") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "10xsjrpnh7s61zizcy9qiwv66iila7f89dqrxqqqlphcyrzh630d")))

(define-public crate-spirv_cross-0.9.8 (c (n "spirv_cross") (v "0.9.8") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "1zcijmhjqhda0d71kah2w6vkwfiy29l6pnixd6qrac0as105h995")))

(define-public crate-spirv_cross-0.10.0 (c (n "spirv_cross") (v "0.10.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "1h8bg4lbpqxjd5ffv6sk9c5ixd09h14c562rvx6xcs79c60fmkvq")))

(define-public crate-spirv_cross-0.10.1 (c (n "spirv_cross") (v "0.10.1") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "04shhhv0pfa7mamli9lw6nm8lxfsl8saqja7vs9c7zk7i7j0nb0w")))

(define-public crate-spirv_cross-0.11.0 (c (n "spirv_cross") (v "0.11.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "000f3nb9vglz20apj1pngcg7w8wnvxwcxdkn9wg4mbgyh13ykfl6")))

(define-public crate-spirv_cross-0.11.1 (c (n "spirv_cross") (v "0.11.1") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "0f795kd7pj9gabya2x2102341pw3929k5piz49p0mrb13zhb9c71")))

(define-public crate-spirv_cross-0.11.2 (c (n "spirv_cross") (v "0.11.2") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "1x19zn97qwbk8msrpibi107vr2r01xq4gxbl87flyal2yzraxhrc")))

(define-public crate-spirv_cross-0.12.0 (c (n "spirv_cross") (v "0.12.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "02nqik1h36jszpj46v6kp5cr5mf80xx6kw4qb2d47h599nwg6f2w")))

(define-public crate-spirv_cross-0.12.1 (c (n "spirv_cross") (v "0.12.1") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)))) (h "08hgym4lb0x1v5qna4n1xbvwssw3d4qiwmn4fpxdcapmz0lzfmyr")))

(define-public crate-spirv_cross-0.13.0 (c (n "spirv_cross") (v "0.13.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 1)) (d (n "js-sys") (r "^0.3.10") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "1apmxsb229hmkpshhpqs89gqav7kzhz5bcy3w47jdxjxi49a3lvj") (f (quote (("msl") ("hlsl") ("glsl") ("default"))))))

(define-public crate-spirv_cross-0.14.0 (c (n "spirv_cross") (v "0.14.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 1)) (d (n "js-sys") (r "^0.3.10") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "0j6lqcc8ahdg3c6im9lp5vkpb5q2iq0qv0yndpqirdjmh7h4z3jj") (f (quote (("msl") ("hlsl") ("glsl") ("default"))))))

(define-public crate-spirv_cross-0.14.1 (c (n "spirv_cross") (v "0.14.1") (d (list (d (n "cc") (r "^1.0.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 1)) (d (n "js-sys") (r "^0.3.10") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "18r91kzf5h2r8nh8bkajdsdhjz4qrnzivfb42x1i37ykak7pf7x2") (f (quote (("msl") ("hlsl") ("glsl") ("default"))))))

(define-public crate-spirv_cross-0.14.2 (c (n "spirv_cross") (v "0.14.2") (d (list (d (n "cc") (r "^1.0.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 1)) (d (n "js-sys") (r "^0.3.10") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "1k0hrfj452y32sbqri0fyj1mdpxrmi7kndnwd5mrg7rkza35jfxh") (f (quote (("msl") ("hlsl") ("glsl") ("default"))))))

(define-public crate-spirv_cross-0.15.0 (c (n "spirv_cross") (v "0.15.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 1)) (d (n "js-sys") (r "^0.3.10") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "17r89762dk8kapkdcrvx2p8cdm6dkkrvmpskyksc4zlgxqzl0wnd") (f (quote (("msl") ("hlsl") ("glsl") ("default"))))))

(define-public crate-spirv_cross-0.16.0 (c (n "spirv_cross") (v "0.16.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 1)) (d (n "js-sys") (r "^0.3.10") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "1fv7qapj92sfd7pkwy8mrr0s4wnkkcik8lphlkk0mv6878dl9gpv") (f (quote (("msl") ("hlsl") ("glsl") ("default"))))))

(define-public crate-spirv_cross-0.17.0 (c (n "spirv_cross") (v "0.17.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 1)) (d (n "js-sys") (r "^0.3.10") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "1c0mvk77cxsy5in3rdpwhi4926k2zy72ralwg91q2zgwa7m9zl91") (f (quote (("msl") ("hlsl") ("glsl") ("default")))) (y #t)))

(define-public crate-spirv_cross-0.17.1 (c (n "spirv_cross") (v "0.17.1") (d (list (d (n "cc") (r "^1.0.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 1)) (d (n "js-sys") (r "^0.3.10") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "0ng77jqchnw40i35mzza8m42jzc3d68a649l8w8kh21k3q2xcxfs") (f (quote (("msl") ("hlsl") ("glsl") ("default"))))))

(define-public crate-spirv_cross-0.18.0 (c (n "spirv_cross") (v "0.18.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 1)) (d (n "js-sys") (r "^0.3.10") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "1pnv8v8f25scxyh96qplzkhsr810vkl5x6avxbirjw9zg7w1cqll") (f (quote (("msl") ("hlsl") ("glsl") ("default"))))))

(define-public crate-spirv_cross-0.19.0 (c (n "spirv_cross") (v "0.19.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 1)) (d (n "js-sys") (r "^0.3.10") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "0l0hcz61k0n0s6rcv4z697yfm37cjgbafj47ang9khyzq409sxdd") (f (quote (("msl") ("hlsl") ("glsl") ("default"))))))

(define-public crate-spirv_cross-0.20.0 (c (n "spirv_cross") (v "0.20.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 1)) (d (n "js-sys") (r "^0.3.10") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "0dqvkwafi9w85dsdw4dmfi69qfkhfkhdw1add7fq51y7x5w98fm3") (f (quote (("msl") ("hlsl") ("glsl") ("default"))))))

(define-public crate-spirv_cross-0.21.0 (c (n "spirv_cross") (v "0.21.0") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 1)) (d (n "js-sys") (r "^0.3.10") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "000fy09xzijkv6i7smmw4bh5rh8sf4hrdqxp9x5f7wq8c6avscdn") (f (quote (("msl") ("hlsl") ("glsl") ("default"))))))

(define-public crate-spirv_cross-0.22.0 (c (n "spirv_cross") (v "0.22.0") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 1)) (d (n "js-sys") (r "^0.3.10") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "0cw63k95ps09brkn67l2qb96vipjpygf4kxxmr3l8fpmxd51y8nq") (f (quote (("msl") ("hlsl") ("glsl") ("default"))))))

(define-public crate-spirv_cross-0.22.1 (c (n "spirv_cross") (v "0.22.1") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 1)) (d (n "js-sys") (r "^0.3.10") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "15nll6lwvc8s7ygpd1gal5nhfigsf6z8m48ic7lnycj0rr14r5pa") (f (quote (("msl") ("hlsl") ("glsl") ("default"))))))

(define-public crate-spirv_cross-0.22.2 (c (n "spirv_cross") (v "0.22.2") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 1)) (d (n "js-sys") (r "^0.3.10") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "04rprabv2d6r8rqv2d16hddi89hf59z18mqb57bfr0xy6splkg8f") (f (quote (("msl") ("hlsl") ("glsl") ("default"))))))

(define-public crate-spirv_cross-0.23.0 (c (n "spirv_cross") (v "0.23.0") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 1)) (d (n "js-sys") (r "^0.3.10") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "066ldr1f1307252z9iggal2fjmgyxsb2hgkqjcapd3sinvbnpnq6") (f (quote (("msl") ("hlsl") ("glsl") ("default"))))))

(define-public crate-spirv_cross-0.23.1 (c (n "spirv_cross") (v "0.23.1") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 1)) (d (n "js-sys") (r "^0.3.10") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "051fbc2y1hrlq7jym39b8k7ka658ldy7m9ky1lpsgi43pynpyr30") (f (quote (("msl") ("hlsl") ("glsl") ("default"))))))

