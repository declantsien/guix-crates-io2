(define-module (crates-io sp ir spirv-to-dxil-sys) #:use-module (crates-io))

(define-public crate-spirv-to-dxil-sys-0.1.1-beta.3 (c (n "spirv-to-dxil-sys") (v "0.1.1-beta.3") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0zr98rkli88181b9kkmij37f48b891da6m7wg1x52n18lvmjqw7p") (f (quote (("included-bindings"))))))

(define-public crate-spirv-to-dxil-sys-0.2.1 (c (n "spirv-to-dxil-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0arj3vzx16fcf9pr6zp6604cq9bawkzj3r5whg2pv3xlyasnqf20") (f (quote (("included-bindings"))))))

(define-public crate-spirv-to-dxil-sys-0.2.2 (c (n "spirv-to-dxil-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0mzkv2c3l2fvwl814sdw5b4m0lpfm5h865zydmmix78ldvzlmj2r") (f (quote (("included-bindings"))))))

(define-public crate-spirv-to-dxil-sys-0.2.3 (c (n "spirv-to-dxil-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "11fdzqwryfvnx2v1kr3ndlxxpm0vipq1f6iarykjfkyvsc7bcpra") (f (quote (("included-bindings"))))))

(define-public crate-spirv-to-dxil-sys-0.2.6 (c (n "spirv-to-dxil-sys") (v "0.2.6") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1xb50qxcvfs9v810b5xqa1xgac8yj8gkfpizdsryjk38imzybxym") (f (quote (("included-bindings"))))))

(define-public crate-spirv-to-dxil-sys-0.3.0 (c (n "spirv-to-dxil-sys") (v "0.3.0") (d (list (d (n "bytemuck") (r "^1.13.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1vv5jvncx2fq82rgpip7xn02d414wkik1z7l47flk69nz1issa4y")))

(define-public crate-spirv-to-dxil-sys-0.4.0 (c (n "spirv-to-dxil-sys") (v "0.4.0") (d (list (d (n "bytemuck") (r "^1.13.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0pd2j6qd6jy96sk5lfbcckfwx6vljciv9iax4mc55b3yq6ls0fb4")))

(define-public crate-spirv-to-dxil-sys-0.4.1 (c (n "spirv-to-dxil-sys") (v "0.4.1") (d (list (d (n "bytemuck") (r "^1.13.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "00wa6xkjw3yk9c9l1qd876nm6aan445l6lwd0bzws5lxhph2v8a1")))

(define-public crate-spirv-to-dxil-sys-0.4.2 (c (n "spirv-to-dxil-sys") (v "0.4.2") (d (list (d (n "bytemuck") (r "^1.13.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0f7ysr5lva67c4x0czfxfv6dkzb0l8xxdxqf2mbwnh8mcnmfqc7w")))

(define-public crate-spirv-to-dxil-sys-0.4.3 (c (n "spirv-to-dxil-sys") (v "0.4.3") (d (list (d (n "bytemuck") (r "^1.13.0") (d #t) (k 0)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0zf1zyslvzv7jijgh3lkhzqigf581kn06in3xjkhsvhx86s8lfyw")))

(define-public crate-spirv-to-dxil-sys-0.4.4-beta (c (n "spirv-to-dxil-sys") (v "0.4.4-beta") (d (list (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "bytemuck") (r "^1.13.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0kibl7abpkmyg0a2skw47rfdpqhrxfqfkd90wnvg9afqk33jfi4i")))

(define-public crate-spirv-to-dxil-sys-0.4.5 (c (n "spirv-to-dxil-sys") (v "0.4.5") (d (list (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "bytemuck") (r "^1.13.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0phyl3cpnyngi4jrmykxai348rpf4wnsg5xkjg0lhxr7q151b7qm")))

(define-public crate-spirv-to-dxil-sys-0.4.6 (c (n "spirv-to-dxil-sys") (v "0.4.6") (d (list (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "bytemuck") (r "^1.13.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1rbf87vs4pr569jp9lzfrrxvyhxgn6jzqlb6v0apq90a1vk7lsc4")))

(define-public crate-spirv-to-dxil-sys-0.4.7 (c (n "spirv-to-dxil-sys") (v "0.4.7") (d (list (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "bytemuck") (r "^1.13.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0h0iqpyhaywralfzj0hjin88c57nw62m2mzlmz923c913zhhcyh3")))

