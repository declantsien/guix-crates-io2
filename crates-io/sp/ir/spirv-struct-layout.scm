(define-module (crates-io sp ir spirv-struct-layout) #:use-module (crates-io))

(define-public crate-spirv-struct-layout-0.1.0 (c (n "spirv-struct-layout") (v "0.1.0") (d (list (d (n "spirq") (r "^0.2") (d #t) (k 0)) (d (n "spirv-struct-layout-derive") (r "^0.1") (d #t) (k 0)))) (h "00rym4lpdjzaff07jzcc4xw1fqa1sxmgwi3qgsdhr6zvgvi051l9")))

(define-public crate-spirv-struct-layout-0.1.1 (c (n "spirv-struct-layout") (v "0.1.1") (d (list (d (n "spirq") (r "^0.2") (d #t) (k 0)) (d (n "spirv-struct-layout-derive") (r "^0.1") (d #t) (k 0)))) (h "02iizsvhxzqg8fvwr3d3r0pzjqx0l8xh7bbwlw3nvwq5h120ybgl")))

(define-public crate-spirv-struct-layout-0.1.2 (c (n "spirv-struct-layout") (v "0.1.2") (d (list (d (n "spirq") (r "^0.2") (d #t) (k 0)) (d (n "spirv-struct-layout-derive") (r "^0.1") (d #t) (k 0)))) (h "0bb5wvjcazpqzbx3wd40248dzq9ixlii1axkdik5g9nfdd4nxsza")))

