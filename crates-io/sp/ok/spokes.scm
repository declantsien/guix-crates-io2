(define-module (crates-io sp ok spokes) #:use-module (crates-io))

(define-public crate-spokes-0.1.0 (c (n "spokes") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "priority-queue") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "12pcmaz7wk2asns9m4vag9ai8k4hbwbi7y627vz7yxxc3rcr550j") (f (quote (("serde1" "serde") ("default"))))))

