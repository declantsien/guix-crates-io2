(define-module (crates-io sp ot spotify-api) #:use-module (crates-io))

(define-public crate-spotify-api-0.1.0 (c (n "spotify-api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jwgk0rkbyk7p6qmm4sm8gad0wd34gpdcdggmrkccnvmjqz5q6g6")))

(define-public crate-spotify-api-0.1.1 (c (n "spotify-api") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qy4xnxrvyn7n5yd9i0yff9fr3y4gh76k2dzvz8alh7r5x2z5vwn")))

(define-public crate-spotify-api-0.1.2 (c (n "spotify-api") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "0szp0gi8pj74ady5ck6z4rickssh17677248cddvp2kdzcdnpbzf")))

