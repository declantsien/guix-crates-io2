(define-module (crates-io sp ot spotifyctl) #:use-module (crates-io))

(define-public crate-spotifyctl-0.3.0 (c (n "spotifyctl") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "lovely_env_logger") (r "^0.6") (d #t) (k 0)) (d (n "rspotify") (r "^0.11") (f (quote ("cli" "env-file" "client-reqwest" "reqwest-native-tls"))) (d #t) (k 0)) (d (n "tokio") (r "^1.30") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1w7q04h89pdcmj27yprlzgsid5wi36m88i8imxxr40mv7b1l5dci")))

