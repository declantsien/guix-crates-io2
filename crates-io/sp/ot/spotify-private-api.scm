(define-module (crates-io sp ot spotify-private-api) #:use-module (crates-io))

(define-public crate-spotify-private-api-0.1.0 (c (n "spotify-private-api") (v "0.1.0") (d (list (d (n "random-string") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "serde_with") (r "^2.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vg2p8y4zjmb4j50crckmg5l2lmxahrp3dxzf29fb670wnsdk0aj") (f (quote (("system-tests"))))))

(define-public crate-spotify-private-api-0.1.1 (c (n "spotify-private-api") (v "0.1.1") (d (list (d (n "random-string") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "serde_with") (r "^2.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0c4hmid00l9rv0lx59603459m70f5b9cxqaxslqd4dcq9f6wbmln") (f (quote (("system-tests"))))))

