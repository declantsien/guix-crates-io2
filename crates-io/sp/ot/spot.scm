(define-module (crates-io sp ot spot) #:use-module (crates-io))

(define-public crate-spot-0.1.0 (c (n "spot") (v "0.1.0") (h "154si7gz2crl66qbvzlvix7r7p7dngd55mc1k49id7fn88789xs4") (y #t)))

(define-public crate-spot-0.1.1 (c (n "spot") (v "0.1.1") (h "14av4b7rkgq6c28j9ah0yr2nfl2cdl9fxycprrn40ybgfkcyabbl")))

(define-public crate-spot-0.1.2 (c (n "spot") (v "0.1.2") (d (list (d (n "threadpool") (r "^1.0") (d #t) (k 0)))) (h "0i81pnxvr0zcw0kffdgjq5k9jkawjg9hbfynj5y7qax4yv8qv0rq") (y #t)))

(define-public crate-spot-0.1.3 (c (n "spot") (v "0.1.3") (h "1h2rihqxz9ghxpr2m4cvzq1l8zkmc2hn4qqd1j6gssq927qw69cf")))

(define-public crate-spot-0.1.4 (c (n "spot") (v "0.1.4") (h "1gaw8j26llksjjgjdhnqc8w5gvagag2lq4w3i49sw8yv3ncijwq6")))

(define-public crate-spot-0.1.5 (c (n "spot") (v "0.1.5") (h "177bq1pwj0kx5maa64kfsvj6lqjgnmbv30hdhqkq63g5pad658sk")))

(define-public crate-spot-0.1.6 (c (n "spot") (v "0.1.6") (h "0n0lrknxdvi774agb44mylyypcz29692rih9m6mihy23cafv4vf7")))

