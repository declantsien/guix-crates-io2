(define-module (crates-io sp ot spotify_downloader) #:use-module (crates-io))

(define-public crate-spotify_downloader-0.1.0 (c (n "spotify_downloader") (v "0.1.0") (d (list (d (n "rspotify") (r "^0.11.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "ytd-rs") (r "^0.1.6") (f (quote ("yt-dlp"))) (d #t) (k 0)))) (h "0gid603nwpnqpiamhr7939pkk279fc7bp048xfszfcillb0njd32") (y #t)))

(define-public crate-spotify_downloader-0.1.1 (c (n "spotify_downloader") (v "0.1.1") (d (list (d (n "rspotify") (r "^0.11.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "ytd-rs") (r "^0.1.6") (f (quote ("yt-dlp"))) (d #t) (k 0)))) (h "0j8blw5vqkfz57aqpniab44rrzjg1dnh4krnnicfcbhj6nv15x7y")))

