(define-module (crates-io sp ot spotify-genres) #:use-module (crates-io))

(define-public crate-spotify-genres-0.1.0 (c (n "spotify-genres") (v "0.1.0") (d (list (d (n "histogram") (r "^0.6.9") (d #t) (k 0)) (d (n "rspotify") (r "^0.7.0") (d #t) (k 0)))) (h "189iw43xwxhhwj0x0ck8xz6px7gvqy1lfij8cn57vklr1glk6wrx")))

(define-public crate-spotify-genres-0.1.1 (c (n "spotify-genres") (v "0.1.1") (d (list (d (n "histogram") (r "^0.6.9") (d #t) (k 0)) (d (n "rspotify") (r "^0.7.0") (d #t) (k 0)))) (h "034vbbgxak8b2zwmmvycz670i18q2mhws2a3c9437s55lq76dfxy")))

