(define-module (crates-io sp ot spot_dwl) #:use-module (crates-io))

(define-public crate-spot_dwl-0.0.1 (c (n "spot_dwl") (v "0.0.1") (h "0ss9ky60vacx0q60j7gafvq4il2y9lbyadz7hlx04h9hky6lixax") (y #t)))

(define-public crate-spot_dwl-0.0.2 (c (n "spot_dwl") (v "0.0.2") (h "0f6ncy0z3qkmd329g8a3cdbvir0zlzj8dgn3l658rqd1zyid8zad") (y #t)))

(define-public crate-spot_dwl-0.0.3 (c (n "spot_dwl") (v "0.0.3") (d (list (d (n "notify-rust") (r "^4.8.0") (d #t) (k 0)))) (h "0nj7d97a1ymd8kjj5jmkdf0snxhjzlm5x7qj882qb5lqiqd3prmc") (y #t)))

(define-public crate-spot_dwl-1.0.0 (c (n "spot_dwl") (v "1.0.0") (d (list (d (n "notify-rust") (r "^4.10.0") (d #t) (k 0)))) (h "10scxi2h8mp75kabb13zf9q43qfb70wqpz2nqpnrv2xh7j6fs5s8") (y #t)))

(define-public crate-spot_dwl-1.0.1 (c (n "spot_dwl") (v "1.0.1") (h "1bhqabxjc1fi2g21d7rm3k4bdkbmrxc76yac1l8fk3js8gv7cnc1") (y #t)))

(define-public crate-spot_dwl-1.0.2 (c (n "spot_dwl") (v "1.0.2") (d (list (d (n "notifme") (r "^0.0.2") (d #t) (k 0)))) (h "01md48rgv9vkmlx7dfksd6nhx72zcblyln9b0bwgwh59by26314w") (y #t)))

(define-public crate-spot_dwl-1.1.0 (c (n "spot_dwl") (v "1.1.0") (d (list (d (n "notifme") (r "^0.0.2") (d #t) (k 0)))) (h "0rpr121yl4a5zh2viw2p9hpg0diw39h3dvkyz9h9myhv4nv34awj") (y #t)))

