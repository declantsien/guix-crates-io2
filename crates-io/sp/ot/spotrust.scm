(define-module (crates-io sp ot spotrust) #:use-module (crates-io))

(define-public crate-spotrust-0.0.1 (c (n "spotrust") (v "0.0.1") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1z7dmjj9h57a6y3rw1hzp5i49rl03i00hvkfc5jl8g14fghp5a22")))

(define-public crate-spotrust-0.0.2 (c (n "spotrust") (v "0.0.2") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kk89m17518sj8vvwbq8n64sf82jlrnkc92yyicfylm1g3ahvg35")))

(define-public crate-spotrust-0.0.3 (c (n "spotrust") (v "0.0.3") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xrs2whhr71c01cypf67dm3p3w8zdh5f61v6vs04qq6qnj202pbd")))

(define-public crate-spotrust-0.0.4 (c (n "spotrust") (v "0.0.4") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18b2bw9cn69qj56bcmy3fqbsyb84djh80z5lpf8d1hwlr7byk1l4")))

(define-public crate-spotrust-0.0.5 (c (n "spotrust") (v "0.0.5") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dndysy9mqy638jbs2kgybfirqsjmvvgr8q02gqgy2f8400sp02d")))

