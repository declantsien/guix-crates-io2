(define-module (crates-io sp ot spotify_info) #:use-module (crates-io))

(define-public crate-spotify_info-0.1.0 (c (n "spotify_info") (v "0.1.0") (d (list (d (n "tungstenite") (r "^0.16.0") (d #t) (k 0)))) (h "0ci9l8b1p6bi4avnapjva22bl5jgy28nfcqnzq8xc5ghpxzfbs8z")))

(define-public crate-spotify_info-0.1.1 (c (n "spotify_info") (v "0.1.1") (d (list (d (n "tungstenite") (r "^0.16.0") (d #t) (k 0)))) (h "1yx9wh9iqqjq9fwg0haairrfsswkdp55rglry5jsd3dihlxq43ks")))

(define-public crate-spotify_info-0.2.0 (c (n "spotify_info") (v "0.2.0") (d (list (d (n "futures-channel") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("sink" "std"))) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("net"))) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("io-std" "macros" "net" "rt-multi-thread" "time"))) (k 2)) (d (n "tokio-tungstenite") (r "^0.16") (d #t) (k 0)))) (h "1j964qyh6iwvjnfs8iy841y4qjlqxpvg96l9qvg453sdh6f1a5w3")))

(define-public crate-spotify_info-0.2.1 (c (n "spotify_info") (v "0.2.1") (d (list (d (n "futures-channel") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("sink" "std"))) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("net"))) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("io-std" "macros" "net" "rt-multi-thread" "time"))) (k 2)) (d (n "tokio-tungstenite") (r "^0.16") (d #t) (k 0)))) (h "0i873czndy6hw1a5c7g94ar5jnj7r85g4n1gwdqy97qkx0brvdn9")))

(define-public crate-spotify_info-0.2.2 (c (n "spotify_info") (v "0.2.2") (d (list (d (n "futures-channel") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("sink" "std"))) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("net"))) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("io-std" "macros" "net" "rt-multi-thread" "time"))) (k 2)) (d (n "tokio-tungstenite") (r "^0.16") (d #t) (k 0)))) (h "1q3jmdi5a6b2l1gxlzwp9cs24ly82afdkdpqv1ipd4q9f3b2ngqg")))

(define-public crate-spotify_info-0.3.0 (c (n "spotify_info") (v "0.3.0") (d (list (d (n "futures-channel") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("sink" "std"))) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("net"))) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("io-std" "macros" "net" "rt-multi-thread" "time"))) (k 2)) (d (n "tokio-tungstenite") (r "^0.16") (d #t) (k 0)))) (h "1fpxahxaxxdvf8ri6ns4jxrkd7gfj49j4ryvsgp1jbzrfdhgigxn")))

(define-public crate-spotify_info-0.4.0 (c (n "spotify_info") (v "0.4.0") (d (list (d (n "futures-channel") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("sink" "std"))) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("net"))) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("io-std" "macros" "net" "rt-multi-thread" "time"))) (k 2)) (d (n "tokio-tungstenite") (r "^0.17") (d #t) (k 0)))) (h "0njsypxpmk3r36x6ylvpa0kslah45jk7p2k0gbxnaq4v8praikrz")))

(define-public crate-spotify_info-0.5.0 (c (n "spotify_info") (v "0.5.0") (d (list (d (n "futures-channel") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("sink" "std"))) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("net"))) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("io-std" "macros" "net" "rt-multi-thread" "time"))) (k 2)) (d (n "tokio-tungstenite") (r "^0.17") (d #t) (k 0)))) (h "1an6675kwkr3887avzf7vnz6pqy0cmdpkyxpylm5sw4c2rfv24in")))

