(define-module (crates-io sp lo splotch) #:use-module (crates-io))

(define-public crate-splotch-0.0.1 (c (n "splotch") (v "0.0.1") (d (list (d (n "footile") (r "^0.7") (d #t) (k 0)) (d (n "pointy") (r "^0.3") (d #t) (k 0)))) (h "11x9qzxmrv1gfc4wfzmfn2k7cp8pd31jcs9k8fikiw4310dsc64z")))

