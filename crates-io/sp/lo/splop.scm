(define-module (crates-io sp lo splop) #:use-module (crates-io))

(define-public crate-splop-0.1.0 (c (n "splop") (v "0.1.0") (h "1z5pz7xqbac0bmxmdv2r375qv2dlc09mmj99wg9r9cjj5anbznv8")))

(define-public crate-splop-0.2.0 (c (n "splop") (v "0.2.0") (h "0kjp073gbr7xqi2lyfaq0vpm7psf1lpd6i5xzn9shv5flmxzlgs5")))

