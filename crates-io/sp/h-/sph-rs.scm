(define-module (crates-io sp h- sph-rs) #:use-module (crates-io))

(define-public crate-Sph-rs-1.0.1 (c (n "Sph-rs") (v "1.0.1") (h "0k49s8jgx1nyya28nfrv7z39svlc2vxgavm3k0kwag6axr8xg5kk")))

(define-public crate-Sph-rs-1.0.2 (c (n "Sph-rs") (v "1.0.2") (h "12qykxssv9919aik7sphv8yfncgpxxprbab3gacwpmi3q8anr3ij")))

