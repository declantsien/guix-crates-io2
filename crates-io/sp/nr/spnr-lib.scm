(define-module (crates-io sp nr spnr-lib) #:use-module (crates-io))

(define-public crate-spnr-lib-0.0.1 (c (n "spnr-lib") (v "0.0.1") (d (list (d (n "candid") (r "^0.7") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "11asg4l0azzakr8dzjrvczf7igzhph255laxjbjd3cw0qqhypnwx") (y #t)))

(define-public crate-spnr-lib-0.0.2 (c (n "spnr-lib") (v "0.0.2") (d (list (d (n "candid") (r "^0.7") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0k7hhv9skr1iczs8b4zxppahq5y5f6z5xss6xr7q5yg0q8zc6ghf") (y #t)))

(define-public crate-spnr-lib-0.0.3 (c (n "spnr-lib") (v "0.0.3") (d (list (d (n "candid") (r "^0.7") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1khdn40snjg8fnscg9vfq99ddi7khfrzlajg656q9vwpmjis63z1") (y #t)))

(define-public crate-spnr-lib-0.0.4 (c (n "spnr-lib") (v "0.0.4") (d (list (d (n "candid") (r "^0.7") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "02lr9fppgcx6ywr7map20v32w72943l13dzr44b9mgk8p0qbl44r")))

(define-public crate-spnr-lib-0.1.0 (c (n "spnr-lib") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "candid") (r "^0.7") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.5") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "06r0fvhn1xv2siwsbd1x44pkzd2mz8gv9q134sbbn5pzvwn6mzbm")))

(define-public crate-spnr-lib-0.2.0 (c (n "spnr-lib") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "candid") (r "^0.7") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.5") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0njyzm1zkdz3lc2wncnsnz21hmk2s14lqxp8hf5yvkcqks7gdwlr")))

(define-public crate-spnr-lib-0.2.1 (c (n "spnr-lib") (v "0.2.1") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "candid") (r "^0.7") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.5") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0ap87ld7myq5wn8pa1mbrby52ddqz4pchxbf79ajdb4kz9fjwkmp")))

(define-public crate-spnr-lib-0.2.2 (c (n "spnr-lib") (v "0.2.2") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "candid") (r "^0.7") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.5") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1hnviahsj5f69v5ga2z2f64d83a1j2m7wkc2a532fj9iiy8qdnai")))

(define-public crate-spnr-lib-0.2.3 (c (n "spnr-lib") (v "0.2.3") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "candid") (r "^0.7") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.5") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0j363rm9c3vqsqfi7ilw49sxjmb6sa2mw3c42r54f04s1g2nhvsn")))

