(define-module (crates-io sp ry spry) #:use-module (crates-io))

(define-public crate-spry-0.0.1 (c (n "spry") (v "0.0.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^2.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("time" "macros"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "066gpsvg56csca17p0ccvlfg55r0hvkffix9yi552cq5ylhg22vz") (y #t)))

(define-public crate-spry-0.0.2 (c (n "spry") (v "0.0.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^2.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("time" "macros"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "132yxiwjxylnas6c1srnn1wh6dp7myi16j334lgql6jlmj0w1j6f") (y #t)))

(define-public crate-spry-0.0.3 (c (n "spry") (v "0.0.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^2.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("time" "macros"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "18k9nmqp0545grzy6pj35wd5g11vyg65j9pi4c6xy3yy1s34as6p") (y #t)))

(define-public crate-spry-0.0.4 (c (n "spry") (v "0.0.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^2.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("time" "macros"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "15lwmphx85zp1q8irbr41g3crykbs989s2ljdqzpb0kklcs5k1h4")))

