(define-module (crates-io sp in spin-executor) #:use-module (crates-io))

(define-public crate-spin-executor-3.0.1 (c (n "spin-executor") (v "3.0.1") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.16.0") (d #t) (k 0)))) (h "0xnylns6igp8g9qfd4c0469a5i2c4c67f539xb4ji9khrkiabw9d") (r "1.73")))

