(define-module (crates-io sp in spine-sys) #:use-module (crates-io))

(define-public crate-spine-sys-0.1.0 (c (n "spine-sys") (v "0.1.0") (h "1w5qn1x5bsm2y47d5wwi1wsw7qmk5dywggp6n8aljgb45v9zb19y")))

(define-public crate-spine-sys-0.1.1 (c (n "spine-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gpw8x5fi15f4mix9c8ygfhzpawyhwb7bxrgr8r4pzgfxybarcwq")))

