(define-module (crates-io sp in spinny) #:use-module (crates-io))

(define-public crate-spinny-0.1.0 (c (n "spinny") (v "0.1.0") (d (list (d (n "lock_api") (r "^0.4.0") (d #t) (k 0)))) (h "1n3n1kx64wfjhx0bkia3dchvn2kyb2sq3f08f3c3g136xpbf5kv5") (f (quote (("owning_ref" "lock_api/owning_ref") ("nightly" "lock_api/nightly") ("default"))))))

(define-public crate-spinny-0.1.1 (c (n "spinny") (v "0.1.1") (d (list (d (n "lock_api") (r "^0.4.0") (d #t) (k 0)))) (h "0wqyf183f6p22nvnrqg0sx4g1nzq04mxh5x74sp694swnspm1ba6") (f (quote (("owning_ref" "lock_api/owning_ref") ("nightly" "lock_api/nightly") ("default"))))))

(define-public crate-spinny-0.2.0 (c (n "spinny") (v "0.2.0") (d (list (d (n "lock_api") (r "^0.4.0") (d #t) (k 0)))) (h "1ywn8x263n003z2sx73p9b8zkkxkdf2wd8rqr3y6sc4am3fy78ax") (f (quote (("owning_ref" "lock_api/owning_ref") ("nightly" "lock_api/nightly") ("default"))))))

(define-public crate-spinny-0.2.1 (c (n "spinny") (v "0.2.1") (d (list (d (n "lock_api") (r "^0.4.0") (d #t) (k 0)) (d (n "loom") (r "^0.3.5") (d #t) (t "cfg(loom)") (k 0)) (d (n "once_cell") (r "^1.4.1") (d #t) (t "cfg(loom)") (k 0)))) (h "0m57ld7jbd6zpfmmgd59zd0l3ny2nvk99b6wsffn8v9a7q4all9k") (f (quote (("owning_ref" "lock_api/owning_ref") ("nightly" "lock_api/nightly") ("default"))))))

(define-public crate-spinny-0.2.2 (c (n "spinny") (v "0.2.2") (d (list (d (n "lock_api") (r "^0.4.0") (d #t) (k 0)) (d (n "loom") (r "^0.3.5") (d #t) (t "cfg(loom)") (k 0)) (d (n "once_cell") (r "^1.4.1") (d #t) (t "cfg(loom)") (k 0)))) (h "1952sz343zb6j867q2w1amsvrgbfjhblgyhdd9b68f3fih0f5xb6") (f (quote (("owning_ref" "lock_api/owning_ref") ("nightly" "lock_api/nightly") ("default"))))))

(define-public crate-spinny-0.2.4 (c (n "spinny") (v "0.2.4") (d (list (d (n "lock_api") (r "^0.4.0") (d #t) (k 0)) (d (n "loom") (r "^0.5.4") (d #t) (t "cfg(loom)") (k 0)) (d (n "once_cell") (r "^1.4.1") (d #t) (t "cfg(loom)") (k 0)))) (h "0cli5b8y0qad5a5r3zl3vz6pidd0xqx1by270gpvbblzc3nwv66c") (f (quote (("owning_ref" "lock_api/owning_ref") ("nightly" "lock_api/nightly") ("default"))))))

