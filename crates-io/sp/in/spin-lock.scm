(define-module (crates-io sp in spin-lock) #:use-module (crates-io))

(define-public crate-spin-lock-0.1.0 (c (n "spin-lock") (v "0.1.0") (h "01vjhskn8s0iq9aaqaas49i8dmzzaaka0ddpjr3lhkca4vz0iv66")))

(define-public crate-spin-lock-0.2.1 (c (n "spin-lock") (v "0.2.1") (h "0h0p821vk6cf904zkbrnnnkxvcxnqypwynrxcfbmrp0kb8v0349d")))

