(define-module (crates-io sp in spinlock) #:use-module (crates-io))

(define-public crate-spinlock-0.1.0 (c (n "spinlock") (v "0.1.0") (h "1fbxvday94vambcr3nm9a6d8599znmngcj7icbcz29b78aqrssmx")))

(define-public crate-spinlock-0.1.1 (c (n "spinlock") (v "0.1.1") (h "1ab3nip22i2nf84a1vph66p4fz5q37n409iz6jar2ww37fisgvp9")))

(define-public crate-spinlock-0.1.2 (c (n "spinlock") (v "0.1.2") (h "1k42jys43dj2qjas1j9nlm0mn9r7n5x85wly9m7yk5dwh84hi8dq")))

