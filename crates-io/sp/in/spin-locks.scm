(define-module (crates-io sp in spin-locks) #:use-module (crates-io))

(define-public crate-spin-locks-0.0.0 (c (n "spin-locks") (v "0.0.0") (d (list (d (n "intel-tsx-hle") (r "^0.0.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "1nz4qqg87azhzbra7j849zlzawl70sh36s701xy79frbv0j636xf")))

(define-public crate-spin-locks-0.0.1 (c (n "spin-locks") (v "0.0.1") (d (list (d (n "intel-tsx-hle") (r "^0.0.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "01i2c3p4a5avcbsy9nj4lrw2r5n0j7z00c1d512zfkgfv4krdkdb")))

(define-public crate-spin-locks-0.0.2 (c (n "spin-locks") (v "0.0.2") (d (list (d (n "intel-tsx-hle") (r "^0.0.0") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 0)))) (h "0m3p4mdy72dmqb1h3rj4xa6h1p3zch7ymknsrlwjwjclyrlndsz1")))

