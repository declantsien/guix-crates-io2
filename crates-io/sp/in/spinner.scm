(define-module (crates-io sp in spinner) #:use-module (crates-io))

(define-public crate-spinner-0.1.0 (c (n "spinner") (v "0.1.0") (h "1p2cbh2j3yqvwbl15fq3i0k1a4rz074dfl1ad8d3r6x8misljgga")))

(define-public crate-spinner-0.2.0 (c (n "spinner") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 0)) (d (n "term") (r "^0.2") (d #t) (k 0)))) (h "1bwrraf24bm0rdldv5f4afnnvl627924i68233gkdkwzb49ml8kq")))

(define-public crate-spinner-0.2.1 (c (n "spinner") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 0)) (d (n "term") (r "^0.2") (d #t) (k 0)))) (h "16kpcn3s968bda9q97fvrdjan30fbnam66l47dzvxfkjs45bwg9y")))

(define-public crate-spinner-0.3.0 (c (n "spinner") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 0)) (d (n "term") (r "^0.2") (d #t) (k 0)))) (h "0swidcnayqw5qv07mcricbbpyfw5afx7w9yc2wh1myf6zj1rq2rh")))

(define-public crate-spinner-0.4.0 (c (n "spinner") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 0)) (d (n "term") (r "^0.2") (d #t) (k 0)))) (h "099qpc9wg8n91amvg821jmhz86h42slmvn1n3byg4vbb1fhszwhz")))

(define-public crate-spinner-0.5.0 (c (n "spinner") (v "0.5.0") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 0)) (d (n "term") (r "^0.6.1") (d #t) (k 0)))) (h "0wn2w072csmq3qyqbhpxq8hv4nfbjbb7fml1c8zf9dr52v87qfkf")))

