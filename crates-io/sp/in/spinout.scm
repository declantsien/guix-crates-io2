(define-module (crates-io sp in spinout) #:use-module (crates-io))

(define-public crate-spinout-0.0.1 (c (n "spinout") (v "0.0.1") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1wrjchi1lnz13rxjp5hdxmn9bpv3vmrabsfkv2ndwdaihj96ch07")))

(define-public crate-spinout-0.0.11 (c (n "spinout") (v "0.0.11") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0wbcyxpyh0psx7642pvvwvjr6vrp8h1qn16kijb20sqb34w3sn7f")))

(define-public crate-spinout-0.0.12 (c (n "spinout") (v "0.0.12") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rayon") (r "^1.5.3") (d #t) (k 2)))) (h "07r7x9rv6mib3ry2qd77j019i5qmwn7yna6cmkcf38fyblpgl6pm")))

