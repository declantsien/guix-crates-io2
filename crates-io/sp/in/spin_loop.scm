(define-module (crates-io sp in spin_loop) #:use-module (crates-io))

(define-public crate-spin_loop-0.1.0 (c (n "spin_loop") (v "0.1.0") (h "14bvzgs3z8xxi61cl5fdid20vbry9b7s65b66cwxiknb3gz33ksp") (f (quote (("std") ("default"))))))

(define-public crate-spin_loop-0.1.1 (c (n "spin_loop") (v "0.1.1") (h "02r5nwxnxwx6ya31q3b3hxgrxxklnhpzjlpwrcd3pa04mady8wlz")))

