(define-module (crates-io sp in spin-contrib-http) #:use-module (crates-io))

(define-public crate-spin-contrib-http-0.0.2 (c (n "spin-contrib-http") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin-sdk") (r "^3.0.1") (d #t) (k 0)))) (h "16hkv93rh7c6ma179s13fsvh08blzgi6p7w860yhym5dp0s7rwsx")))

(define-public crate-spin-contrib-http-0.0.3 (c (n "spin-contrib-http") (v "0.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spin-sdk") (r "^3.0.1") (d #t) (k 0)))) (h "1iqm40pz73677v61cvk58q0kyn3bbximb0dsbxzmj6ddm4ssvl02")))

(define-public crate-spin-contrib-http-0.0.4 (c (n "spin-contrib-http") (v "0.0.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "spin-sdk") (r "^3.0.1") (d #t) (k 0)))) (h "1wkqxgd63ghdvv1hcahh8hp7cfq3ag2q25c5lgn1n88yjrd6ly4c")))

(define-public crate-spin-contrib-http-0.0.5 (c (n "spin-contrib-http") (v "0.0.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "spin-sdk") (r "^3.0.1") (d #t) (k 0)))) (h "0r7maxvv9x8rl9ywk6zixaghs2lmfjdpvabyxw7kp9shv1gp12q8")))

(define-public crate-spin-contrib-http-0.0.6 (c (n "spin-contrib-http") (v "0.0.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "spin-sdk") (r "^3.0.1") (d #t) (k 0)))) (h "1ha5z3k1s0kgk888bqljq9059sn9j697pqnmvp3nwbzym66nxc8x")))

(define-public crate-spin-contrib-http-0.0.7 (c (n "spin-contrib-http") (v "0.0.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "spin-sdk") (r "^3.0.1") (d #t) (k 0)))) (h "0ihn65ryba0c7q4wgbaipayhq4f3fx28kcickbgafsymczj3jf5l")))

