(define-module (crates-io sp in spine-data) #:use-module (crates-io))

(define-public crate-spine-data-0.1.0 (c (n "spine-data") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.2") (d #t) (k 0)))) (h "1flscb5gcn869pdsprjm59gsybh5g1fvg6ksd5g0ny1yisxhji2x")))

(define-public crate-spine-data-0.1.1 (c (n "spine-data") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.2") (d #t) (k 0)))) (h "1z3px8ahz8s3f1x9rc6dcy6xsw8mzkaigya2grrs0h7v674lhzhq")))

(define-public crate-spine-data-0.1.2 (c (n "spine-data") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.2") (d #t) (k 0)))) (h "1143vbsvzkx4j26k9470v052wy198hgf39cjqvbk4s9y4j6sv08g")))

(define-public crate-spine-data-0.2.0 (c (n "spine-data") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.2") (d #t) (k 0)))) (h "1jf2jds7vdl1blagyhkbq85rbdqqjjcxl4maibzkxrv0bk63j6fl")))

(define-public crate-spine-data-0.2.1 (c (n "spine-data") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.2") (d #t) (k 0)))) (h "0fagawl94s1gjhxnclki0wmnl8gxyzq741asj9wqfm7cwyfwpr7d")))

(define-public crate-spine-data-0.2.2 (c (n "spine-data") (v "0.2.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.2") (d #t) (k 0)))) (h "1jmhfrgd2l8hq2v0khjmsnzxp4l9jwr9n8qjyw9c95yz02h1igxq")))

