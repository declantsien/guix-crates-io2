(define-module (crates-io sp in spin-macro) #:use-module (crates-io))

(define-public crate-spin-macro-0.1.0 (c (n "spin-macro") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hnbz6p9kwrw96dxlhxkfg3y8vgndwcmfmdxbfw2ykl3xxps8ddm")))

(define-public crate-spin-macro-2.1.0 (c (n "spin-macro") (v "2.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1w61m1p7kvq5s9hfhydpmqknp9maqq5aba55dz6cw5w5jl0p7r5q")))

(define-public crate-spin-macro-2.2.0 (c (n "spin-macro") (v "2.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11mxvl9j4xhfdwz8kjbgck5lzlbflrvl75czwa36b1wzmds48lhk")))

(define-public crate-spin-macro-3.0.0 (c (n "spin-macro") (v "3.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06j2mzckzi594861w79sl8qwv5zqgkc83bayappq51fyxwpswqd1") (r "1.73")))

(define-public crate-spin-macro-3.0.1 (c (n "spin-macro") (v "3.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02p6594gn0ns3fknaf7y7wysdnrr9dhg2fnfbbc439h5lbjh6ggg") (r "1.73")))

