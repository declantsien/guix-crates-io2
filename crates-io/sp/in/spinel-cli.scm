(define-module (crates-io sp in spinel-cli) #:use-module (crates-io))

(define-public crate-spinel-cli-0.1.0 (c (n "spinel-cli") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "serialport") (r "^4.3.0") (f (quote ("usbportinfo-interface"))) (d #t) (k 0)) (d (n "spinel") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "bytes" "full"))) (d #t) (k 0)) (d (n "tokio-serial") (r "^5.4.4") (f (quote ("bytes" "codec"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (d #t) (k 0)))) (h "13wnl35nlaqa1hix6nb3dw5pj6sjbkvw1zh5aqfm4am3v63yn08z")))

