(define-module (crates-io sp in spin_sleep) #:use-module (crates-io))

(define-public crate-spin_sleep-0.1.0 (c (n "spin_sleep") (v "0.1.0") (h "0aqafi30irv3bv4xjs218mmdimsdi7mvqr5v2f36cxv852n45yry")))

(define-public crate-spin_sleep-0.2.0 (c (n "spin_sleep") (v "0.2.0") (h "15sgn579ajjh2m87iw3snhfamj4612i7y3d1gbcwrw5lyky3d8vc") (y #t)))

(define-public crate-spin_sleep-0.2.1 (c (n "spin_sleep") (v "0.2.1") (h "1j3gp7gydiv13dv5f7qqv5pq31bbdl2hk5d039g5xfhb0rc47yhs")))

(define-public crate-spin_sleep-0.3.0 (c (n "spin_sleep") (v "0.3.0") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 2)))) (h "0sa0w886x4g9r9dg67n2awk1760dn6n967j2rng3zly3p7r15g0y") (f (quote (("nondeterministic_tests")))) (y #t)))

(define-public crate-spin_sleep-0.3.1 (c (n "spin_sleep") (v "0.3.1") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 2)))) (h "0w5snbnnxfzbhz54hx3d8zidi9gimk85cw56dmjl476kxbnrnyc1") (f (quote (("nondeterministic_tests"))))))

(define-public crate-spin_sleep-0.3.2 (c (n "spin_sleep") (v "0.3.2") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 2)))) (h "0zg1svm9img2a8qlhyvzfs8qwh0k99nkbikyj0pwpd2i2b7hny5a") (f (quote (("nondeterministic_tests"))))))

(define-public crate-spin_sleep-0.3.3 (c (n "spin_sleep") (v "0.3.3") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winmm-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0ak6gl2x951ybfsyyd5fjfhvskbcpbaz5ykgjb7wwq87j9ilvpbz") (f (quote (("nondeterministic_tests"))))))

(define-public crate-spin_sleep-0.3.4 (c (n "spin_sleep") (v "0.3.4") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "mmsystem" "timeapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0yq9g7gd9bggbr6lhccy7c3c0lvrwm2xrdk1yda40bfdx1nyvd3w") (f (quote (("nondeterministic_tests"))))))

(define-public crate-spin_sleep-0.3.5 (c (n "spin_sleep") (v "0.3.5") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "mmsystem" "timeapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "052fpih443ylyn5jva5nssgnh8lp26xz5xa6lv45a54yhn68px7g") (f (quote (("nondeterministic_tests"))))))

(define-public crate-spin_sleep-0.3.6 (c (n "spin_sleep") (v "0.3.6") (d (list (d (n "approx") (r "^0.2") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "mmsystem" "timeapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0c8nlwb5agl6919qbwa1yvjn9mm6lrq04avvakswa6fypa78mjp2") (f (quote (("nondeterministic_tests"))))))

(define-public crate-spin_sleep-0.3.7 (c (n "spin_sleep") (v "0.3.7") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "mmsystem" "timeapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0361h0cjfqykzigi10k4h1kl4yd3j59xfd4djdw9cnwabzpkc649") (f (quote (("nondeterministic_tests"))))))

(define-public crate-spin_sleep-1.0.0 (c (n "spin_sleep") (v "1.0.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "mmsystem" "timeapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1ixcab0amdr1a9yakxzi3h6za516vnqg0aiwf69f2crqvhdi161a") (f (quote (("nondeterministic_tests"))))))

(define-public crate-spin_sleep-1.1.0 (c (n "spin_sleep") (v "1.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "mmsystem" "timeapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0siq9zsds7q7mr0abd8m890rdh3xkc4v4hcv2spqbn6y5hz3y9z7") (f (quote (("nondeterministic_tests"))))))

(define-public crate-spin_sleep-1.1.1 (c (n "spin_sleep") (v "1.1.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "mmsystem" "timeapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0d1inb9z45yp9zyba4zac11s2fd8swjmw0n7vda46pq8vc07kyna") (f (quote (("nondeterministic_tests"))))))

(define-public crate-spin_sleep-1.2.0 (c (n "spin_sleep") (v "1.2.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "windows-sys") (r "^0.52") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System" "Win32_System_Threading" "Win32_Media"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0zrllbf1ksc6srjgbslk2fn5c9fp5f1rrrw2n0301xza9639g2in") (f (quote (("nondeterministic_tests"))))))

