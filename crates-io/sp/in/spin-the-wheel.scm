(define-module (crates-io sp in spin-the-wheel) #:use-module (crates-io))

(define-public crate-spin-the-wheel-0.0.1 (c (n "spin-the-wheel") (v "0.0.1") (d (list (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1g1rph342kpm7ry8nqsayfp4g54rb3f2ar92w3jkdapq3iwjah8r")))

(define-public crate-spin-the-wheel-0.0.2 (c (n "spin-the-wheel") (v "0.0.2") (d (list (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0wdkl8dkj2igwdpj9gab3c3xhpilcry9q2v2rb798cgiwq76nyqq")))

(define-public crate-spin-the-wheel-0.0.3 (c (n "spin-the-wheel") (v "0.0.3") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0pm60myzlak2dv8w2assijh4gw5wqdj8zaqbg04fnm0lrhqnj3rg")))

