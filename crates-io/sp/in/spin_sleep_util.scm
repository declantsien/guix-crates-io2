(define-module (crates-io sp in spin_sleep_util) #:use-module (crates-io))

(define-public crate-spin_sleep_util-0.1.0 (c (n "spin_sleep_util") (v "0.1.0") (d (list (d (n "spin_sleep") (r "^1") (d #t) (k 0)))) (h "145ckhsrj493d5mk6gham0j5hlnfcny8hy5213c0hply4kraiqy1")))

(define-public crate-spin_sleep_util-0.1.1 (c (n "spin_sleep_util") (v "0.1.1") (d (list (d (n "spin_sleep") (r "^1.2") (d #t) (k 0)))) (h "1zbyiqd9v4fjry9j47fyvmd5wr63kfnfp2ywmmzl9z7nvw1882iz")))

