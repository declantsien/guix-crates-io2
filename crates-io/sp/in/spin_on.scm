(define-module (crates-io sp in spin_on) #:use-module (crates-io))

(define-public crate-spin_on-0.1.0 (c (n "spin_on") (v "0.1.0") (d (list (d (n "pin-utils") (r "^0.1.0") (d #t) (k 0)))) (h "094qgxhwl2zysca58fkm4zjnrmbf9xpjrs9bvsjai3a05fb5z6f2")))

(define-public crate-spin_on-0.1.1 (c (n "spin_on") (v "0.1.1") (d (list (d (n "pin-utils") (r "^0.1.0") (d #t) (k 0)))) (h "18idc7jfa4m2cap721nh5lva19z3ykjyz1w2hfm6960vshz10vh7")))

