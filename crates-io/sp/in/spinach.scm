(define-module (crates-io sp in spinach) #:use-module (crates-io))

(define-public crate-spinach-1.0.0 (c (n "spinach") (v "1.0.0") (h "0x5a7g7nd967flcz48d5l75ls52pajk8i325g9kpsyvgcblmf60n") (y #t)))

(define-public crate-spinach-1.0.1 (c (n "spinach") (v "1.0.1") (h "10vn1p2ypgw59bn3ihv4yk4yvdx7f5pgpc3jlfx568hq37bbyrqq")))

(define-public crate-spinach-1.0.2 (c (n "spinach") (v "1.0.2") (h "0psl04svdjh1i9q0as330fkwrichjnlzhgqi8rgbl3xgn0pkwjmv")))

(define-public crate-spinach-1.1.0 (c (n "spinach") (v "1.1.0") (h "1s1ziz628fv5i704wvs4lj24ijaj8vm04dswaq69as8zjmyswzgn")))

(define-public crate-spinach-1.2.0 (c (n "spinach") (v "1.2.0") (h "0hr4jgn3y08qpzjkr7sxgi4jz6chq6zjp6fxb39xpi6fwssgc7ar")))

(define-public crate-spinach-2.0.0 (c (n "spinach") (v "2.0.0") (h "0zfld12rv9557xvdmk9m921nyalypxk5yiwk5q4i20v1aj967kib")))

(define-public crate-spinach-2.1.0 (c (n "spinach") (v "2.1.0") (h "1xdxhvfkr2cp89lqm8sp15n3d8fd05kjfddklhyhxdrsamp0r5qn")))

