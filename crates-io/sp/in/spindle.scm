(define-module (crates-io sp in spindle) #:use-module (crates-io))

(define-public crate-spindle-0.1.0 (c (n "spindle") (v "0.1.0") (h "04cqm82mcqi2lv6qdykh4cfmf3kz14qb54vrd2484k8bskhcv1y2")))

(define-public crate-spindle-0.1.1 (c (n "spindle") (v "0.1.1") (d (list (d (n "spindle_macros") (r "^0.1.1") (d #t) (k 0)))) (h "0nj7y31rvxnly01l20dhnp3rm73gcjf6xpdsqvbrqyyqyzh24jnn")))

(define-public crate-spindle-0.1.3 (c (n "spindle") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "cudarc") (r "^0.9.12") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 2)) (d (n "spindle_macros") (r "^0.1.3") (d #t) (k 0)))) (h "19yv9117bwmp4lqcwk431ci8mv0pp60jrrlgb5phcvvx64f1wv8y")))

(define-public crate-spindle-0.1.4 (c (n "spindle") (v "0.1.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "cudarc") (r "^0.9.12") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 2)) (d (n "spindle_macros") (r "^0.1.3") (d #t) (k 0)))) (h "00b74rpycbrcjb6cabwg6csyqhnc0hmq0ihc9fxhhic1rm59rn2z")))

(define-public crate-spindle-0.1.5 (c (n "spindle") (v "0.1.5") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "cudarc") (r "^0.9.12") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 2)) (d (n "spindle_macros") (r "^0.1.5") (d #t) (k 0)))) (h "0n0dybvd5masb1wcy8f29i6nz31jhand6iawa95wc6pszqfpk567")))

(define-public crate-spindle-0.1.7 (c (n "spindle") (v "0.1.7") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "cudarc") (r "^0.9.12") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 2)) (d (n "spindle_macros") (r "^0.1.9") (d #t) (k 0)))) (h "0v7idzb96xaiqnhmr60lcqixgxy0abkh5z7wl3zmqnhvnxl2jizi")))

(define-public crate-spindle-0.1.9 (c (n "spindle") (v "0.1.9") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "cudarc") (r "^0.9.12") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 2)) (d (n "spindle_macros") (r "^0.1.11") (d #t) (k 0)))) (h "0a96acpz2xm6v0ycsa69v3kjma7zq6xkxyvna47x6imh8hv984fk")))

