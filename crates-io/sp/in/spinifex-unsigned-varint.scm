(define-module (crates-io sp in spinifex-unsigned-varint) #:use-module (crates-io))

(define-public crate-spinifex-unsigned-varint-0.2.2 (c (n "spinifex-unsigned-varint") (v "0.2.2") (d (list (d (n "thiserror") (r "^1.0.14") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 0)))) (h "16ihffydzqf4rdnwz7j40z81qw547fjfrqy9ayb3xn32j18gyxck")))

