(define-module (crates-io sp in spingus) #:use-module (crates-io))

(define-public crate-spingus-0.1.0 (c (n "spingus") (v "0.1.0") (d (list (d (n "annotate-snippets") (r "^0.10.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "logos") (r "^0.13.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1jqvpgm6jqhd2wahcni5c7n8w942llgv1m1fw725qb4wgr5sf61h") (y #t)))

