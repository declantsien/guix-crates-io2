(define-module (crates-io sp in spinning) #:use-module (crates-io))

(define-public crate-spinning-0.0.0 (c (n "spinning") (v "0.0.0") (d (list (d (n "lock_api") (r "^0.3") (d #t) (k 0)))) (h "0b7i9n5qjfp17kyxzrnphidvs1pqyv7annvx41aviygr8gvwl9hp") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-spinning-0.0.1 (c (n "spinning") (v "0.0.1") (d (list (d (n "lock_api") (r "^0.3") (d #t) (k 0)))) (h "0hz4g4l5p4gvdr26jmdyn943vnxyvjcm97gzhmp9fr8c4g88gi2q") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-spinning-0.0.2 (c (n "spinning") (v "0.0.2") (d (list (d (n "lock_api") (r "^0.4") (d #t) (k 0)))) (h "0ckh8whki967bvkyv5cd6r6z3h6qdvpsxq95hvxp1cnypsszhb8m") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-spinning-0.0.3 (c (n "spinning") (v "0.0.3") (d (list (d (n "lock_api") (r "^0.4") (d #t) (k 0)))) (h "1wf98b5cl6j6yl8fibs15danp4pkwr7r5lshc1byigl9w9pibn6f") (f (quote (("std") ("default" "std"))))))

(define-public crate-spinning-0.1.0 (c (n "spinning") (v "0.1.0") (d (list (d (n "lock_api") (r "^0.4") (d #t) (k 0)))) (h "0yrs2lzyyrwvs58pya2h22pfdx3vv0h76w1av5c2dbbw5630wkrd") (f (quote (("std") ("default" "std"))))))

