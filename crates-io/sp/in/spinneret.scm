(define-module (crates-io sp in spinneret) #:use-module (crates-io))

(define-public crate-spinneret-0.1.0 (c (n "spinneret") (v "0.1.0") (h "0f05wpkf6lxbigj5zyzw0m4qjhbswg9kffck0kk5b0y78svsb2ac") (y #t)))

(define-public crate-spinneret-0.0.0 (c (n "spinneret") (v "0.0.0") (h "1qfalncsbdjbi5n89zk0j563glj8qv8wq1i3acadwjqzizr8i2yj")))

