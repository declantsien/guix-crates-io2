(define-module (crates-io sp in spin-sync) #:use-module (crates-io))

(define-public crate-spin-sync-0.0.1 (c (n "spin-sync") (v "0.0.1") (h "1kxf5lpnp7wsdnb4j37hfdmxn32pgn7p54zhcfrwlzzcq68bkz04")))

(define-public crate-spin-sync-0.1.0 (c (n "spin-sync") (v "0.1.0") (h "0ll4arsfhgfgjl2smknfhwp5dzh91pcs0p47a03gg35sfp5hz8gz")))

(define-public crate-spin-sync-0.1.1 (c (n "spin-sync") (v "0.1.1") (h "1wj86yxy7hy8z1pbr5dxy2v5n5jm73sqcllzz1kkig7n46ms7jqs")))

(define-public crate-spin-sync-0.1.2 (c (n "spin-sync") (v "0.1.2") (h "1z6iisgh4w1f54cj90swx022nc3jrsi5730nmyx5csdqjcdc5gsk")))

(define-public crate-spin-sync-0.2.0 (c (n "spin-sync") (v "0.2.0") (h "1vhz0zvxcl0ryqpzqj3bwam3yz4cbhlskbypgqjivxg76cqf1n06")))

(define-public crate-spin-sync-0.2.1 (c (n "spin-sync") (v "0.2.1") (h "1dadknf6alkwqlgjq5dyzdj0mxzzb5ydvjcllrdq6b9xs5vlkry9")))

(define-public crate-spin-sync-0.3.0 (c (n "spin-sync") (v "0.3.0") (h "1cdvp62r1093qp2ya9vig347alpwbdlrqx4k1qii80aipdnmjk98")))

(define-public crate-spin-sync-0.3.1 (c (n "spin-sync") (v "0.3.1") (h "1ynrzpxzhs9ya91rqbdlgxg9fm7rml3fji17bwjm6cld4hd4m1ms")))

(define-public crate-spin-sync-0.3.2 (c (n "spin-sync") (v "0.3.2") (h "0na14dw5d5sz4z61yls3wb0hhs3wg4lac1fwm18ja7nbii3pn723")))

