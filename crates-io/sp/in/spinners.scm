(define-module (crates-io sp in spinners) #:use-module (crates-io))

(define-public crate-spinners-0.0.1 (c (n "spinners") (v "0.0.1") (d (list (d (n "ansi-escapes") (r "^0.1.0") (d #t) (k 2)) (d (n "ansi_term") (r "^0.9") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "spinner") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.8.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.8.0") (d #t) (k 0)) (d (n "term") (r "^0.4.6") (d #t) (k 2)))) (h "0win27z76li6rxq3s5midllwx22hvs11jim8l1g83lghc4j1dqwd")))

(define-public crate-spinners-0.0.2 (c (n "spinners") (v "0.0.2") (d (list (d (n "ansi-escapes") (r "^0.1.0") (d #t) (k 2)) (d (n "ansi_term") (r "^0.9") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "spinner") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.8.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.8.0") (d #t) (k 0)) (d (n "term") (r "^0.4.6") (d #t) (k 2)))) (h "1y9hi5cl9dq25qhjkpawprfabv7cz0lg8anly7hd5l30rpwhcw9r")))

(define-public crate-spinners-1.0.0 (c (n "spinners") (v "1.0.0") (d (list (d (n "ansi-escapes") (r "^0.1.0") (d #t) (k 2)) (d (n "ansi_term") (r "^0.9") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "spinner") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.8.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.8.0") (d #t) (k 0)) (d (n "term") (r "^0.4.6") (d #t) (k 2)))) (h "0w8hr6gwila9i5xjdvbcz369ivmb7ai1w02g2zm8s1q1q9292sdz")))

(define-public crate-spinners-1.1.0 (c (n "spinners") (v "1.1.0") (d (list (d (n "ansi-escapes") (r "^0.1.0") (d #t) (k 2)) (d (n "ansi_term") (r "^0.9") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "spinner") (r "^0.5.0") (d #t) (k 0)) (d (n "strum") (r "^0.8.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.8.0") (d #t) (k 0)) (d (n "term") (r "^0.4.6") (d #t) (k 2)))) (h "0bk0xn770nd0n641hkm08wdsva56rf3ax2axk1rq4n1zcm9d1sb3")))

(define-public crate-spinners-1.2.0 (c (n "spinners") (v "1.2.0") (d (list (d (n "ansi-escapes") (r "^0.1.0") (d #t) (k 2)) (d (n "ansi_term") (r "^0.9") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "spinner") (r "^0.5.0") (d #t) (k 0)) (d (n "strum") (r "^0.8.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.8.0") (d #t) (k 0)) (d (n "term") (r "^0.4.6") (d #t) (k 2)))) (h "0wk43cwk0z8j45vxd1hgzx8f101z2m9ikfwppqv16vn9rsmmcvp2")))

(define-public crate-spinners-2.0.0 (c (n "spinners") (v "2.0.0") (d (list (d (n "ansi-escapes") (r "^0.1.0") (d #t) (k 2)) (d (n "ansi_term") (r "^0.9") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "spinner") (r "^0.5") (d #t) (k 0)) (d (n "strum") (r "^0.21") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21") (d #t) (k 0)) (d (n "term") (r "^0.4.6") (d #t) (k 2)))) (h "0lp18qig3rcqai4n4r9jd9p88p46a5l76p3sbrdnj4bw0k3r7wjx")))

(define-public crate-spinners-3.0.0 (c (n "spinners") (v "3.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "strum") (r "^0.23.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09jsl8431bmnpqbdipngfbb344mcgg2fdn5950gfz5wxcbl7syia")))

(define-public crate-spinners-3.0.1 (c (n "spinners") (v "3.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "strum") (r "^0.23.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "19kkdxnr441dz60nwd2xs35ab69rvrl4jj1s4fhzq8z4hskxxm4h")))

(define-public crate-spinners-3.1.0 (c (n "spinners") (v "3.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15zhxgwddq30g1zl84m6y8jwb0jar095i963ms78w7pa5kg3w1kj")))

(define-public crate-spinners-4.0.0 (c (n "spinners") (v "4.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0497dak179s2zdwfh2bsqx59adbvb49g7m2i0lb6cm8cz93b5arv")))

(define-public crate-spinners-4.1.0 (c (n "spinners") (v "4.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1axw2cdvkv59z4568rhzrcaw66caqs8jig39k64xjrq0fkm5wq88")))

(define-public crate-spinners-4.1.1 (c (n "spinners") (v "4.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10jgzdy6x79ipnfhavn46zbg4hlx98mcfr7p4f4j774b6mzr9vx0")))

