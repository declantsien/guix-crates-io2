(define-module (crates-io sp in spinning_top) #:use-module (crates-io))

(define-public crate-spinning_top-0.1.0 (c (n "spinning_top") (v "0.1.0") (d (list (d (n "lock_api") (r "^0.3.3") (d #t) (k 0)))) (h "094d8cvlpfksxgk3f2jqqn7n6pwymdf8vvszz1qm1krvlnih3n1j") (f (quote (("nightly" "lock_api/nightly"))))))

(define-public crate-spinning_top-0.1.1 (c (n "spinning_top") (v "0.1.1") (d (list (d (n "lock_api") (r "^0.3.3") (d #t) (k 0)))) (h "004s4ya7jr9qvffcpm2c3c49rz2cn1mlzalp5h4y0njzvzb32w04") (f (quote (("nightly" "lock_api/nightly"))))))

(define-public crate-spinning_top-0.2.0 (c (n "spinning_top") (v "0.2.0") (d (list (d (n "lock_api") (r "^0.4.0") (d #t) (k 0)))) (h "0kar0z96yvar02qqf6ahmb0n85jcfghj60jw2xx00l4l8gas59hf") (f (quote (("nightly" "lock_api/nightly"))))))

(define-public crate-spinning_top-0.2.1 (c (n "spinning_top") (v "0.2.1") (d (list (d (n "lock_api") (r "^0.4.0") (d #t) (k 0)))) (h "1kgnij6c3fyh8c26qncpp5s3j725nns1y60mlyfwdijylf5vms1s") (f (quote (("nightly" "lock_api/nightly"))))))

(define-public crate-spinning_top-0.2.2 (c (n "spinning_top") (v "0.2.2") (d (list (d (n "lock_api") (r "^0.4.0") (d #t) (k 0)))) (h "0dmbb627i05qla1lnxy7r6hpiia76c8kb40zcgrbar0dx1rrslky") (f (quote (("owning_ref" "lock_api/owning_ref") ("nightly" "lock_api/nightly"))))))

(define-public crate-spinning_top-0.2.3 (c (n "spinning_top") (v "0.2.3") (d (list (d (n "lock_api") (r "^0.4.0") (d #t) (k 0)))) (h "0yc89szj9vldjiph762z7fy9a381f0vhvf836nb2sp9piimspl4b") (f (quote (("owning_ref" "lock_api/owning_ref") ("nightly" "lock_api/nightly"))))))

(define-public crate-spinning_top-0.2.4 (c (n "spinning_top") (v "0.2.4") (d (list (d (n "lock_api") (r "^0.4.0") (d #t) (k 0)))) (h "0g19z7wvaqyaljzcb8xx4v8dicfsy784ybfa5kxj3dc4xs2avbbm") (f (quote (("owning_ref" "lock_api/owning_ref") ("nightly" "lock_api/nightly"))))))

(define-public crate-spinning_top-0.2.5 (c (n "spinning_top") (v "0.2.5") (d (list (d (n "lock_api") (r "^0.4.7") (d #t) (k 0)))) (h "1c6x734rlvvhjw1prk8k3y7d5z65459br6pzl2ila564yjib37jv") (f (quote (("owning_ref" "lock_api/owning_ref") ("nightly"))))))

(define-public crate-spinning_top-0.3.0 (c (n "spinning_top") (v "0.3.0") (d (list (d (n "lock_api") (r "^0.4.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "001kjbiz1gg111rsqxc4pq9a1izx7wshkk38f69h1dbgf4fjsvfr") (f (quote (("owning_ref" "lock_api/owning_ref") ("arc_lock" "lock_api/arc_lock"))))))

