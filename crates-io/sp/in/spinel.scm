(define-module (crates-io sp in spinel) #:use-module (crates-io))

(define-public crate-spinel-0.1.0 (c (n "spinel") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.10") (f (quote ("codec"))) (d #t) (k 0)))) (h "1fr657whzd5bxl8a1s91zfk6q4rzrgj39m6n11nac89g84nnxbpx") (f (quote (("std") ("default" "std"))))))

