(define-module (crates-io sp in spinners-jdxcode) #:use-module (crates-io))

(define-public crate-spinners-jdxcode-4.1.0 (c (n "spinners-jdxcode") (v "4.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gx31pp8hyak3aczr03yxyiixj439s946rqswnx0qwn4c83d1fyp")))

