(define-module (crates-io sp in spin4) #:use-module (crates-io))

(define-public crate-spin4-0.0.1 (c (n "spin4") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)))) (h "04ddjl62n1jdxi2xyhypy2j5chcw8fw3y40pbfm8y4c3qzq60jzn")))

(define-public crate-spin4-0.0.2 (c (n "spin4") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)))) (h "1mcdrfy3kj7ibbmbzvccy51jrag9yjb85r7wcfb8lhxrdm9gxvk6")))

(define-public crate-spin4-0.0.3 (c (n "spin4") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)))) (h "1v74nx18z8c797pbycwza3l39ys32y5wkpcd6cznw255hqsjq32p")))

