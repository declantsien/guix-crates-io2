(define-module (crates-io sp in spinup) #:use-module (crates-io))

(define-public crate-spinup-0.1.0 (c (n "spinup") (v "0.1.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "kira") (r "^0.6") (d #t) (k 0)) (d (n "symphonia") (r "^0.5") (d #t) (k 0)) (d (n "tui") (r "^0.18") (d #t) (k 0)))) (h "0dsw36ij2yffhb9mcnpvxr9fwg1fhy9swa1knvvhm72cabrq72fi")))

