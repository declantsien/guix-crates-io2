(define-module (crates-io sp in spinning_cube) #:use-module (crates-io))

(define-public crate-spinning_cube-0.1.0 (c (n "spinning_cube") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "1d1l7ck8bir1j5qhai3kxjbbg5jsil7w36xqm6f427jpzvi5cssj")))

(define-public crate-spinning_cube-0.1.1 (c (n "spinning_cube") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "1fwwyr79w6454isvzf1mwilgjinjzb56873jrh1r3aj929s3ar6l")))

(define-public crate-spinning_cube-0.1.2 (c (n "spinning_cube") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "0sjwycpi0bbvhw03kq558m90ic17n48sabqayvdy375vfz73mq8g")))

(define-public crate-spinning_cube-0.1.3 (c (n "spinning_cube") (v "0.1.3") (d (list (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)))) (h "1bv1p4sqzbp13b6lim5h0h1swdjxjnb7d20bn119gwdr8m78kmc3")))

