(define-module (crates-io sp in spinners-rs) #:use-module (crates-io))

(define-public crate-spinners-rs-1.0.0 (c (n "spinners-rs") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.23.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23.1") (d #t) (k 0)))) (h "0cp7l82vyyv64y98h65k328hdgbz61qvx5p8qvn3v24qzhbv4cd9")))

(define-public crate-spinners-rs-1.1.0 (c (n "spinners-rs") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.23.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.23.1") (d #t) (k 0)))) (h "1iamf4zpvndwidzwba4480q9gf5k0y1hjnaxlc85ivkcvhv5x2v1")))

(define-public crate-spinners-rs-1.2.0 (c (n "spinners-rs") (v "1.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "strum") (r "^0.23.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p4qxwryizq98d3gyl194647wszb8s11iykbjl5v0c4ws6ikxwp9")))

(define-public crate-spinners-rs-1.2.1 (c (n "spinners-rs") (v "1.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "strum") (r "^0.23.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bbb20h127i8z4xh5vf3qikkc94wfq2zzrfl6kcg87nis8raiv8n")))

(define-public crate-spinners-rs-1.2.2 (c (n "spinners-rs") (v "1.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "strum") (r "^0.23.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01p0y9my3ik00fbh6hs6bcicvshb2bg9qdjhwlbypb5fj8pn9bcv")))

(define-public crate-spinners-rs-1.2.3 (c (n "spinners-rs") (v "1.2.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "strum") (r "^0.23.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ry656vv9lbba7q0asz4mvkwwixqq2rqbn59mz1476j29375liqb")))

(define-public crate-spinners-rs-1.2.4 (c (n "spinners-rs") (v "1.2.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cwcyb32d883bq5wi2nj08xpkl9qanhjxcwd0r4dggjchy2da3z1")))

(define-public crate-spinners-rs-2.0.0 (c (n "spinners-rs") (v "2.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0w4kd2qyzfmrnv5b9n31sb1sjismkl0rgqhnng8b5jj91h9ddj9p")))

(define-public crate-spinners-rs-2.0.1 (c (n "spinners-rs") (v "2.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0s32z98m9vyndzkwjzx2dvsgvc7gcgjg3hpscixcvgm0l52da2s9")))

(define-public crate-spinners-rs-2.1.0 (c (n "spinners-rs") (v "2.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "040sqjb6c3rmf3739yw6ab33vkz8jm7c4k49g4ydnj66k7dqh4ax")))

(define-public crate-spinners-rs-2.2.0 (c (n "spinners-rs") (v "2.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0720zqz5mkk5kvij0mghy9c9v6jzficq9ssfi5wfsrx4bb9nmk4b")))

(define-public crate-spinners-rs-2.2.1 (c (n "spinners-rs") (v "2.2.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "strum") (r "^0.24.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "03v6f1v3z4qpx7wfwgkyv3fnzqg9828q8r7v051ibnwcbikj2cdr")))

(define-public crate-spinners-rs-2.2.2 (c (n "spinners-rs") (v "2.2.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "strum") (r "^0.24.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0xwrrgz9dfxbz6ky2syvh0wj503ls8pkb3swsn9ylmylbi2166f0")))

(define-public crate-spinners-rs-2.2.3 (c (n "spinners-rs") (v "2.2.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "strum") (r "^0.24.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0mb9gk323000vb5lh0161m8mbx61v04q5zssvwca41pdvh5wf3hc")))

(define-public crate-spinners-rs-2.3.0 (c (n "spinners-rs") (v "2.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "strum") (r "^0.24.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wn82685sd23psjyigaqq28g73i3lapg3rvfzmismbsf6k1vr39q")))

