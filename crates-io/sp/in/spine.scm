(define-module (crates-io sp in spine) #:use-module (crates-io))

(define-public crate-spine-0.1.0 (c (n "spine") (v "0.1.0") (h "164s26rv92xc7mry6ppl80x0izpdpaihx832alnl99svifl6p4yb")))

(define-public crate-spine-0.1.1 (c (n "spine") (v "0.1.1") (d (list (d (n "spine-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1hjax4c4fa76cc51g6x5dbdprp2kw74x5cba8mp7kglqm1ncawqr")))

