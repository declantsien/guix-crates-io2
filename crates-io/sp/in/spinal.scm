(define-module (crates-io sp in spinal) #:use-module (crates-io))

(define-public crate-spinal-0.0.1 (c (n "spinal") (v "0.0.1") (d (list (d (n "bevy_math") (r "^0.8.0") (d #t) (k 0)) (d (n "bevy_utils") (r "^0.8.0") (d #t) (k 0)) (d (n "miette") (r "^5.2.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (f (quote ("trace"))) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "13jqps0iv0ln5457b18csv8c6jhk828zzm5716cn51v95hxz23dd")))

