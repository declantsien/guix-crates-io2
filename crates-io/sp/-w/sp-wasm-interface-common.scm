(define-module (crates-io sp -w sp-wasm-interface-common) #:use-module (crates-io))

(define-public crate-sp-wasm-interface-common-7.0.0 (c (n "sp-wasm-interface-common") (v "7.0.0") (d (list (d (n "codec") (r "^3.2.2") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-std") (r "^5.0.0") (k 0)) (d (n "wasmi") (r "^0.13.2") (o #t) (k 0)))) (h "00yhbdn5yhgdlnh4f5fxz238sfhy2jqv8fpmc3n1pccpf61920w2") (f (quote (("std" "codec/std" "sp-std/std" "wasmi/std") ("default" "std"))))))

(define-public crate-sp-wasm-interface-common-7.0.1 (c (n "sp-wasm-interface-common") (v "7.0.1") (d (list (d (n "codec") (r "^3.2.2") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-std") (r "^5.0.0") (k 0)) (d (n "wasmi") (r "^0.13.2") (o #t) (k 0)))) (h "104iqmkl8qkq80wdbcm3wphixf0dw502lvyr1s42hfqzl0dj4k15") (f (quote (("std" "codec/std" "sp-std/std" "wasmi/std") ("default" "std"))))))

(define-public crate-sp-wasm-interface-common-7.0.2 (c (n "sp-wasm-interface-common") (v "7.0.2") (d (list (d (n "codec") (r "^3.2.2") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-std") (r "^5.0.0") (k 0)) (d (n "wasmi") (r "^0.13.2") (o #t) (k 0)))) (h "14jphl2081b3538k1gwy4khr0pwl74pgdr7g59xcbrv45d498cay") (f (quote (("std" "codec/std" "sp-std/std" "wasmi/std") ("default" "std"))))))

(define-public crate-sp-wasm-interface-common-15.0.0 (c (n "sp-wasm-interface-common") (v "15.0.0") (d (list (d (n "codec") (r "^3.2.2") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-std") (r "^9.0.0") (k 0)) (d (n "wasmi") (r "^0.13.2") (o #t) (k 0)))) (h "1bplhfpyqkwsyjsq6pcbz8qzkaw33x87cnzb85ihyjg1i3kkw8h3") (f (quote (("std" "codec/std" "sp-std/std" "wasmi/std") ("default" "std"))))))

