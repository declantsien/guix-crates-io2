(define-module (crates-io sp -w sp-wasm-memfs) #:use-module (crates-io))

(define-public crate-sp-wasm-memfs-0.2.0 (c (n "sp-wasm-memfs") (v "0.2.0") (d (list (d (n "path-clean") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tool") (r "^0.2") (d #t) (k 0)))) (h "0rq0hb8q59kmd9bg768q7ds9s137062xv0mr3i7hhfp4ba23wr1n")))

