(define-module (crates-io sp as spaserver) #:use-module (crates-io))

(define-public crate-spaserver-0.2.0 (c (n "spaserver") (v "0.2.0") (d (list (d (n "actix") (r "^0.7") (d #t) (k 0)) (d (n "actix-web") (r "^0.7") (d #t) (k 0)) (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "simplelog") (r "^0.5") (d #t) (k 0)))) (h "1965yyr8ss2drj116ypvfx3jqdyl99agkjxx168r6ndyc9qbiy3h")))

