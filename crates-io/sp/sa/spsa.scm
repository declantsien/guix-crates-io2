(define-module (crates-io sp sa spsa) #:use-module (crates-io))

(define-public crate-spsa-0.1.0 (c (n "spsa") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "packed_simd_2") (r "^0.3.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0fqgfh4dc9ysjlvs3i464cnlkf11zhwkfrsc5ndi1s7lfqj3r5j8")))

(define-public crate-spsa-0.2.0 (c (n "spsa") (v "0.2.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "packed_simd_2") (r "^0.3.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0vq1vvr9dg1686xs01d5dsqj90i324m2dk2mpr8f1dkyz39b882a")))

(define-public crate-spsa-0.2.1 (c (n "spsa") (v "0.2.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "packed_simd_2") (r "^0.3.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0njxzdx4vxi4hw13pyjycx32n9bxm4ixd50qxks9ld45yfbmh66a")))

