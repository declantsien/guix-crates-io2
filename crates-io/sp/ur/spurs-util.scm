(define-module (crates-io sp ur spurs-util) #:use-module (crates-io))

(define-public crate-spurs-util-0.1.0 (c (n "spurs-util") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "spurs") (r "^0.5.0") (d #t) (k 0)) (d (n "spurs") (r "^0.5.0") (f (quote ("test"))) (d #t) (k 2)))) (h "0d910raq8as9yl7hw2k0fzrd7h435b45zqcbwy6xd1h6rq903ljj")))

(define-public crate-spurs-util-0.2.0 (c (n "spurs-util") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "spurs") (r "^0.6.0") (d #t) (k 0)) (d (n "spurs") (r "^0.6.0") (f (quote ("test"))) (d #t) (k 2)))) (h "0g3cx93dpnd3mz8qyjs9ym4sd949c9jy79gv9xj4idnsh8xy340j")))

(define-public crate-spurs-util-0.2.1 (c (n "spurs-util") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "spurs") (r "^0.7.0") (d #t) (k 0)) (d (n "spurs") (r "^0.7.0") (f (quote ("test"))) (d #t) (k 2)))) (h "1naxxqclni32b72a1z04kz06qn3ypgay57z2nvq0y5681f9jwqd9")))

(define-public crate-spurs-util-0.2.2 (c (n "spurs-util") (v "0.2.2") (d (list (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "spurs") (r "^0.8.1") (d #t) (k 0)) (d (n "spurs") (r "^0.8.1") (f (quote ("test"))) (d #t) (k 2)))) (h "1z4kzbz1k9fxi8m7fab0889cp3zli4rhrc3k9lhk41b52092r5nc")))

(define-public crate-spurs-util-0.3.0 (c (n "spurs-util") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "spurs") (r "^0.9.0") (d #t) (k 0)) (d (n "spurs") (r "^0.9.0") (f (quote ("test"))) (d #t) (k 2)))) (h "05rga7yy2v26p5nhzk92rqb7774081sgrq3s1z5bb4pjhvq0grx4")))

(define-public crate-spurs-util-0.3.1 (c (n "spurs-util") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "spurs") (r "^0.9.2") (d #t) (k 0)) (d (n "spurs") (r "^0.9.2") (f (quote ("test"))) (d #t) (k 2)))) (h "0n1ks2d32xff879xv7a4b7bkij9q9sf6k0xcf3mc14pbbrwqhj20")))

