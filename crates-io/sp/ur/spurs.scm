(define-module (crates-io sp ur spurs) #:use-module (crates-io))

(define-public crate-spurs-0.1.0 (c (n "spurs") (v "0.1.0") (d (list (d (n "console") (r "^0.6.2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "ssh2") (r "^0.3.3") (d #t) (k 0)))) (h "1c85zvlkpzccp4nycp2q8ammxmxacf3799rkd3vrsnhng3lqajr6")))

(define-public crate-spurs-0.1.1 (c (n "spurs") (v "0.1.1") (d (list (d (n "console") (r "^0.6.2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "ssh2") (r "^0.3.3") (d #t) (k 0)))) (h "114jna70v89awa3xwxamiga11dj1f7m9vfxlrs4c0bkvfg9by5h6")))

(define-public crate-spurs-0.1.2 (c (n "spurs") (v "0.1.2") (d (list (d (n "console") (r "^0.6.2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "ssh2") (r "^0.3.3") (d #t) (k 0)))) (h "0f6302sla35wmfzzba0niw9h60z2pcc2l496fvizjgs28frspkc2")))

(define-public crate-spurs-0.1.3 (c (n "spurs") (v "0.1.3") (d (list (d (n "console") (r "^0.6.2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "ssh2") (r "^0.3.3") (d #t) (k 0)))) (h "194rhdmbj8fahbficmvr9gi5v0xcvpbd5rpbfv8gcz03hdvin8ls")))

(define-public crate-spurs-0.1.4 (c (n "spurs") (v "0.1.4") (d (list (d (n "console") (r "^0.6.2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "ssh2") (r "^0.3.3") (d #t) (k 0)))) (h "0fsz8b5ygwwn2nk99mirpnhivs24il56ailk8sy03jq12rzm4zpl")))

(define-public crate-spurs-0.2.0 (c (n "spurs") (v "0.2.0") (d (list (d (n "console") (r "^0.7.2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.4") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ssh2") (r "^0.3.3") (d #t) (k 0)))) (h "0h13l8sknnp69p85z9j4zmwbarhwlj6s7zs9slncbr9l4ij9jm5p")))

(define-public crate-spurs-0.3.0 (c (n "spurs") (v "0.3.0") (d (list (d (n "console") (r "^0.7.2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.4") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ssh2") (r "^0.3.3") (d #t) (k 0)))) (h "1vmh2x2xlcncccj6x72cnqkj7ac1a8pg0yz4ran0adcby0fddcbf")))

(define-public crate-spurs-0.4.0 (c (n "spurs") (v "0.4.0") (d (list (d (n "console") (r "^0.7.2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.4") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ssh2") (r "^0.3.3") (d #t) (k 0)))) (h "0l864g9n2knxmaakdajhh331wzp3s8yrjd0cbmcmhcnw5sa4qfqy")))

(define-public crate-spurs-0.4.1 (c (n "spurs") (v "0.4.1") (d (list (d (n "console") (r "^0.7.2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.4") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ssh2") (r "^0.3.3") (d #t) (k 0)))) (h "1sqkv0p39xv5fb4vmampf8bqpgkrxlvb9qb9n9civks240z8wam7")))

(define-public crate-spurs-0.5.0 (c (n "spurs") (v "0.5.0") (d (list (d (n "console") (r "^0.7.2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.4") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ssh2") (r "^0.3.3") (d #t) (k 0)))) (h "0hcgbggl6mmkv8h569qrh2ja3dfvc6nb5579mfhkirwj1cba9b3d") (f (quote (("test") ("default"))))))

(define-public crate-spurs-0.6.0 (c (n "spurs") (v "0.6.0") (d (list (d (n "console") (r "^0.7.2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ssh2") (r "^0.3.3") (d #t) (k 0)))) (h "1f6nlfhmhk416fyymr7w7c6yz86jbhvp6zkq3np5zw1ssfw0qp4a") (f (quote (("test") ("default"))))))

(define-public crate-spurs-0.7.0 (c (n "spurs") (v "0.7.0") (d (list (d (n "console") (r "^0.7.2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ssh2") (r "^0.3.3") (d #t) (k 0)))) (h "1p2khxj2ykqjk6dczzlcpvgbc0cwxib314ny0djb8a0aiddflil7") (f (quote (("test") ("default"))))))

(define-public crate-spurs-0.8.0 (c (n "spurs") (v "0.8.0") (d (list (d (n "console") (r "^0.7.2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ssh2") (r "^0.3.3") (d #t) (k 0)))) (h "1gzz20nd95drl7x0qc7avy1cz2mnm8wsx8kd2a9w7fq3hrsd9a89") (f (quote (("test") ("default"))))))

(define-public crate-spurs-0.8.1 (c (n "spurs") (v "0.8.1") (d (list (d (n "console") (r "^0.7.2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ssh2") (r "^0.3.3") (d #t) (k 0)))) (h "0wmsxw9r7h1jcmhrr468pr3818nf60jbrsv0l3z7h6g6gqkxflq7") (f (quote (("test") ("default"))))))

(define-public crate-spurs-0.8.2 (c (n "spurs") (v "0.8.2") (d (list (d (n "console") (r "^0.7.2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ssh2") (r "^0.3.3") (d #t) (k 0)))) (h "1ykd14hp3mrdcak4dmy02zb1f1zizf1m1hnlpnl3kz7c362fmbw9") (f (quote (("test") ("default"))))))

(define-public crate-spurs-0.9.0 (c (n "spurs") (v "0.9.0") (d (list (d (n "console") (r "^0.7.2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ssh2") (r "^0.3.3") (d #t) (k 0)))) (h "0vbqw3mp4qgbdh3larhj4vz7fzqrcmm999rlrsbb408iyh9s3lfv") (f (quote (("test") ("default"))))))

(define-public crate-spurs-0.9.1 (c (n "spurs") (v "0.9.1") (d (list (d (n "console") (r "^0.7.2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ssh2") (r "^0.3.3") (d #t) (k 0)))) (h "1m1p6ghndjrq6p3qgqkyyykp47mpipfy72lwnavcrdppcw2mw04x") (f (quote (("test") ("default"))))))

(define-public crate-spurs-0.9.2 (c (n "spurs") (v "0.9.2") (d (list (d (n "console") (r "^0.7.2") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "ssh2") (r "^0.3.3") (d #t) (k 0)))) (h "1dqpk59bjfi4yf8kr9whdd3k3mhx4xbv1gyllljzadq0zc8n1imj") (f (quote (("test") ("default"))))))

