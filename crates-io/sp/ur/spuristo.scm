(define-module (crates-io sp ur spuristo) #:use-module (crates-io))

(define-public crate-spuristo-0.1.0 (c (n "spuristo") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "glam") (r "^0.23.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "png") (r "^0.17.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "1f5rnaq7i97q4gqw3zqa6v0a6cjzvqj8xvfw4vhihp2nyrlwd635") (y #t)))

(define-public crate-spuristo-0.1.1 (c (n "spuristo") (v "0.1.1") (d (list (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "glam") (r "^0.23.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "png") (r "^0.17.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)) (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "1rpdi4ydkyzyl0m66cc8qjmbf0gba4fx0vk332xjmgyrv3mdk3d1") (y #t)))

