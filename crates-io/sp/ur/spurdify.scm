(define-module (crates-io sp ur spurdify) #:use-module (crates-io))

(define-public crate-spurdify-0.1.1 (c (n "spurdify") (v "0.1.1") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "clap") (r "^2.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1p1yqyp1kcclag5qkd2gkglrmyahq7i8gn85c42pgmh6m08marrx")))

