(define-module (crates-io sp #{1-}# sp1-token) #:use-module (crates-io))

(define-public crate-sp1-token-1.0.0 (c (n "sp1-token") (v "1.0.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.4") (d #t) (k 0)) (d (n "solana-program") (r "^1.8.1") (d #t) (k 0)) (d (n "solana-program-test") (r "^1.8.1") (d #t) (k 2)) (d (n "solana-sdk") (r "^1.8.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01r68p42hi1sqryhbkd756xnq88h6rhvzrfvm5575aa1pqbvhjnp") (f (quote (("test-bpf") ("no-entrypoint"))))))

