(define-module (crates-io sp yd spyder) #:use-module (crates-io))

(define-public crate-spyder-0.1.0 (c (n "spyder") (v "0.1.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "lol_html") (r "^1.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "08dqwxpg0bdk72z1fsl3ssmcdnjinpvb8r7kigq7rzjzdqqsh805")))

(define-public crate-spyder-0.2.0 (c (n "spyder") (v "0.2.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "lol_html") (r "^1.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "18in0ff0rza416a2mlaz63fsh4n20ir80bnp8v2g2n3x3a15ljfd")))

(define-public crate-spyder-0.3.0 (c (n "spyder") (v "0.3.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "lol_html") (r "^1.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "14jv4p60f45cnpm9qsja60mj8qz4cx4sn1xaf97bs42ic5f98igg")))

(define-public crate-spyder-0.4.0 (c (n "spyder") (v "0.4.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "lol_html") (r "^1.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1szv3gpfah2qgmjl3f132p589mxyv5c6rf73v4w6fbcfdvdhbbq1")))

(define-public crate-spyder-0.5.0 (c (n "spyder") (v "0.5.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "lol_html") (r "^1.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "12ngf8wxr3fj49rlaiy9wiamwglxps8idcqjm4ynsr9vvmbrxhsi")))

(define-public crate-spyder-0.5.1 (c (n "spyder") (v "0.5.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "lol_html") (r "^1.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1y97ggns6azbifpqqv5jw4dl64fisr9shfnzka6lgc0cmwzbviqc")))

