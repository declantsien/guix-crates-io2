(define-module (crates-io sp at spatialos-sys) #:use-module (crates-io))

(define-public crate-spatialos-sys-0.0.1 (c (n "spatialos-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "1007lr8g1lii1ylqjar31ym09yk1indfi1rwaah3aq09jzjr59cd")))

(define-public crate-spatialos-sys-0.0.3 (c (n "spatialos-sys") (v "0.0.3") (d (list (d (n "bindgen") (r ">=0.53.1, <0.54.0") (d #t) (k 1)) (d (n "bytevec") (r ">=0.2.0, <0.3.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "gdi32-sys") (r ">=0.2.0, <0.3.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 1)) (d (n "user32-sys") (r ">=0.1.2, <0.2.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0jdgr6fq9cy9r6gndg5adl86388kwng0d4mbz3j15zbkp5gq9dx8")))

(define-public crate-spatialos-sys-0.0.4 (c (n "spatialos-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "bytevec") (r "^0.2.0") (d #t) (k 0)) (d (n "gdi32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "user32-sys") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)))) (h "051m6b4q88dyzyqv9c2y1s24mmc4r1byglfr4q33bh5bv7b2bmfi")))

(define-public crate-spatialos-sys-0.0.5 (c (n "spatialos-sys") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "bytevec") (r "^0.2.0") (d #t) (k 0)) (d (n "gdi32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "user32-sys") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)))) (h "03x0ajp4w0qi15d0hhdklnw8l76vyv97zasvjxmqil8ym1drsvl8")))

(define-public crate-spatialos-sys-0.1.0 (c (n "spatialos-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "bytevec") (r "^0.2.0") (d #t) (k 0)) (d (n "gdi32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "user32-sys") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1dzjlllj2vk1fxkq6kp1ra0pv6ndsprzpwir58836b3vnxznd1fi")))

(define-public crate-spatialos-sys-0.2.0 (c (n "spatialos-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "gdi32-sys") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "user32-sys") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1x7isig4bkacirwqciavs6nrjkmfnd0q2mp07p8mshxyaqvds7vg")))

