(define-module (crates-io sp at spatial_table) #:use-module (crates-io))

(define-public crate-spatial_table-0.1.0 (c (n "spatial_table") (v "0.1.0") (d (list (d (n "entity_table") (r "^0.1") (d #t) (k 0)) (d (n "grid_2d") (r "^0.14") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "07kzxxjc7cddlx5xxdhv416r89g154vvyiiqdmvvnpgzwqpq2sxh")))

(define-public crate-spatial_table-0.1.1 (c (n "spatial_table") (v "0.1.1") (d (list (d (n "entity_table") (r "^0.1") (d #t) (k 0)) (d (n "grid_2d") (r "^0.14") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "0nk2v0rp7k28bhykmnhw2vvq77mzxam7gva14lxm4cq23apjlwx5")))

(define-public crate-spatial_table-0.2.0 (c (n "spatial_table") (v "0.2.0") (d (list (d (n "entity_table") (r "^0.2") (d #t) (k 0)) (d (n "grid_2d") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0q91dirx11948hadynv63f0mljiww89axw96r6cfxwnd6msj3f97") (f (quote (("serialize" "serde" "entity_table/serialize" "grid_2d/serialize"))))))

(define-public crate-spatial_table-0.2.1 (c (n "spatial_table") (v "0.2.1") (d (list (d (n "entity_table") (r "^0.2") (d #t) (k 0)) (d (n "grid_2d") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1xw8zm712nksifpa563z4i3qhkdnsy5b65hpf040bwafzh4sj0gk") (f (quote (("serialize" "serde" "entity_table/serialize" "grid_2d/serialize"))))))

(define-public crate-spatial_table-0.2.2 (c (n "spatial_table") (v "0.2.2") (d (list (d (n "entity_table") (r "^0.2") (d #t) (k 0)) (d (n "grid_2d") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0av186b5f6940rm0jzp7c9sc82paggq2zbs58cpm9z9gklf29w1h") (f (quote (("serialize" "serde" "entity_table/serialize" "grid_2d/serialize"))))))

(define-public crate-spatial_table-0.2.3 (c (n "spatial_table") (v "0.2.3") (d (list (d (n "entity_table") (r "^0.2") (d #t) (k 0)) (d (n "grid_2d") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "16dviljmyh92gc1q10726p8dhcmshm2dkh0axmr2d3p3bz0dnyi4") (f (quote (("serialize" "serde" "entity_table/serialize" "grid_2d/serialize"))))))

(define-public crate-spatial_table-0.3.0 (c (n "spatial_table") (v "0.3.0") (d (list (d (n "entity_table") (r "^0.2") (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0hjjkd8qr2nrmxr0gjl0kj70f2sb5g3x5fb3p8b9p21xsf54mlqd") (f (quote (("serialize" "serde" "entity_table/serialize" "grid_2d/serialize"))))))

(define-public crate-spatial_table-0.3.1 (c (n "spatial_table") (v "0.3.1") (d (list (d (n "entity_table") (r "^0.2") (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "10bp5asy20l38syar4w7xvs2q9i77vvh1pv6mm53fkgiyn5vhr6a") (f (quote (("serialize" "serde" "entity_table/serialize" "grid_2d/serialize"))))))

(define-public crate-spatial_table-0.4.0 (c (n "spatial_table") (v "0.4.0") (d (list (d (n "entity_table") (r "^0.2") (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "04nxykrwr6idbkx3hn9ayhhkpkcfk7j0nd4sw35k7hnhd76k040s") (f (quote (("serialize" "serde" "entity_table/serialize" "grid_2d/serialize"))))))

(define-public crate-spatial_table-0.4.1 (c (n "spatial_table") (v "0.4.1") (d (list (d (n "entity_table") (r "^0.2") (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "102an3dz5n4hbpl0if1lwsvbgaci9kh3a39k21jws2i39x4mfrk3") (f (quote (("serialize" "serde" "entity_table/serialize" "grid_2d/serialize"))))))

(define-public crate-spatial_table-0.4.2 (c (n "spatial_table") (v "0.4.2") (d (list (d (n "entity_table") (r "^0.2") (d #t) (k 0)) (d (n "grid_2d") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0a7b2id7kvy6g32z8l24afyhhb1xjam378pgw6vp3w77lqsa127q") (f (quote (("serialize" "serde" "entity_table/serialize" "grid_2d/serialize"))))))

