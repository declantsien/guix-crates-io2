(define-module (crates-io sp at spatial-neighbors) #:use-module (crates-io))

(define-public crate-spatial-neighbors-0.1.0 (c (n "spatial-neighbors") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0qp8d8nd767c76g8wiap2hyf2rp79rhyhpgzjj8cn4fir4kxysqi")))

(define-public crate-spatial-neighbors-0.1.1 (c (n "spatial-neighbors") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1pp01kg61g0is51syv8z4vljrzb3ygzrj55384n3fw3vqjvkcc02")))

(define-public crate-spatial-neighbors-0.1.2 (c (n "spatial-neighbors") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0j2hm2fgfgd7n7fpzfmqavikl73ipmgbpq5c6mq38n1m5d8b17ng")))

(define-public crate-spatial-neighbors-0.2.0 (c (n "spatial-neighbors") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1bx5d78jfq35m9bsk1v356d49i3qirw9311zazc4m36f5rmhva2r")))

(define-public crate-spatial-neighbors-0.2.1 (c (n "spatial-neighbors") (v "0.2.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1cy3w0xzcp011b1rv2sjvnzl6ydijw8za9zmg1r139kz44dm0cif")))

