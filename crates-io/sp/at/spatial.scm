(define-module (crates-io sp at spatial) #:use-module (crates-io))

(define-public crate-spatial-0.1.0 (c (n "spatial") (v "0.1.0") (d (list (d (n "approx") (r "^0.1.0") (d #t) (k 2)) (d (n "generic-array") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "ordered-float") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typenum") (r "^1.3") (d #t) (k 0)))) (h "0l0cp82lxb9jq69nxzg4pmpcll92fx889db5fh5911y5av6nnv1i")))

(define-public crate-spatial-0.1.1 (c (n "spatial") (v "0.1.1") (d (list (d (n "approx") (r "^0.1.0") (d #t) (k 2)) (d (n "generic-array") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "ordered-float") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typenum") (r "^1.3") (d #t) (k 0)))) (h "0dbx7rdxf33c544rgh0gzkkdzw9w8xlmmb6pvzy8mi9j3c0z27ih")))

(define-public crate-spatial-0.2.0-rc1 (c (n "spatial") (v "0.2.0-rc1") (d (list (d (n "approx") (r "^0.1.0") (d #t) (k 2)) (d (n "generic-array") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "ordered-float") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typenum") (r "^1.3") (d #t) (k 0)))) (h "0hsih205gxfp6733mxbnr85zj66q1j82jpdrxmkv6fvk6pr4r5w2")))

(define-public crate-spatial-0.2.0-rc2 (c (n "spatial") (v "0.2.0-rc2") (d (list (d (n "approx") (r "^0.1.0") (d #t) (k 2)) (d (n "generic-array") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "ordered-float") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "typenum") (r "^1.3") (d #t) (k 0)))) (h "1lim7fb4csj5f899rnbb9g1l2746q97vgswh93blz2cydhqp8f96")))

(define-public crate-spatial-1.0.0 (c (n "spatial") (v "1.0.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "ordered-float") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1h3nmpmxkbhp4wngykdzhqa4mxmlzfr9596hzvj9sln1q41ickij")))

(define-public crate-spatial-1.0.1 (c (n "spatial") (v "1.0.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "ordered-float") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1ydhyjk2sjdvw0iihi6l9n1kgz1vxyxibz6p3fbg4rzp7qiq6hgr") (r "1.61")))

