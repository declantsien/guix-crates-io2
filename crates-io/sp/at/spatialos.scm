(define-module (crates-io sp at spatialos) #:use-module (crates-io))

(define-public crate-spatialos-0.1.0 (c (n "spatialos") (v "0.1.0") (d (list (d (n "bytevec") (r "^0.2.0") (d #t) (k 0)) (d (n "spatialos-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1mv4fh44a7yxj0ffbv2bcvfzh77m030id8x6f9764bj72cfdjr6d")))

(define-public crate-spatialos-0.1.1 (c (n "spatialos") (v "0.1.1") (d (list (d (n "bytevec") (r "^0.2") (d #t) (k 0)) (d (n "spatialos-sys") (r "^0.2") (d #t) (k 0)))) (h "03lp2sjm7gpgg5sxfhrwr2izwvi4awggy5sjh4m7s2kjh0j3k7hs")))

(define-public crate-spatialos-0.1.2 (c (n "spatialos") (v "0.1.2") (d (list (d (n "bytevec") (r "^0.2") (d #t) (k 0)) (d (n "spatialos-sys") (r "^0.2") (d #t) (k 0)))) (h "1383qid289b42yhdl619qp4ff7bxlyw1ayxz545r50zmr6za2sqm")))

