(define-module (crates-io sp at spatialite-rs) #:use-module (crates-io))

(define-public crate-spatialite-rs-0.1.0 (c (n "spatialite-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "sqlite") (r "^0.25.3") (k 0)) (d (n "sqlite3-sys") (r "^0.12.0") (k 0)))) (h "09j3g288b5s7s1575fc791jwbq0hw1xqcx67yf3f3vwc6g837bjb") (f (quote (("linkage" "sqlite3-sys/linkage" "sqlite/linkage") ("default" "linkage"))))))

(define-public crate-spatialite-rs-0.1.1 (c (n "spatialite-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 2)) (d (n "sqlite") (r "^0.25.3") (k 0)) (d (n "sqlite3-sys") (r "^0.12.0") (k 0)))) (h "1klf9nvfqcpiz5x18rwvsnmdbar31wxcljiq94kl3ks37z6b2r58") (f (quote (("linkage" "sqlite3-sys/linkage" "sqlite/linkage") ("default" "linkage"))))))

(define-public crate-spatialite-rs-0.1.2 (c (n "spatialite-rs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 2)) (d (n "sqlite") (r "^0.25.3") (k 0)) (d (n "sqlite3-sys") (r "^0.12.0") (k 0)))) (h "13fpicnyjaw08xvdpjwfpxxl9x9zc53cwkiclzlp23014ahcfqvf") (f (quote (("linkage" "sqlite3-sys/linkage" "sqlite/linkage") ("default" "linkage"))))))

(define-public crate-spatialite-rs-0.1.3 (c (n "spatialite-rs") (v "0.1.3") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 2)) (d (n "sqlite") (r "^0") (k 0)) (d (n "sqlite3-sys") (r "^0") (k 0)))) (h "1xjm1kq6hhn01vvxdrkpfkvm6ldjskw12xvgqfqpi4idxzpyxiid") (f (quote (("linkage" "sqlite3-sys/linkage" "sqlite/linkage") ("default" "linkage"))))))

(define-public crate-spatialite-rs-0.1.4-alpha.0 (c (n "spatialite-rs") (v "0.1.4-alpha.0") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 2)) (d (n "sqlite") (r "^0") (k 0)) (d (n "sqlite3-sys") (r "^0") (k 0)))) (h "12hyjdx676h62hiv4xgcz9vrvin4sq68d1qd7x6szzwi02ahg4z9") (f (quote (("linkage" "sqlite3-sys/linkage" "sqlite/linkage") ("default" "linkage"))))))

(define-public crate-spatialite-rs-0.1.4 (c (n "spatialite-rs") (v "0.1.4") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 2)) (d (n "sqlite") (r "^0") (k 0)) (d (n "sqlite3-sys") (r "^0") (k 0)))) (h "1amfcd7dw6lra98d1sv7s2i4fcccclbjdwz2dkhyyi46vqwhysj3") (f (quote (("linkage" "sqlite3-sys/linkage" "sqlite/linkage") ("default" "linkage"))))))

(define-public crate-spatialite-rs-0.1.5 (c (n "spatialite-rs") (v "0.1.5") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 2)) (d (n "sqlite") (r "^0") (k 0)) (d (n "sqlite3-sys") (r "^0") (k 0)))) (h "19pi77c7vj0slf58ixwafq8r59d94nfivmlmd4mq0v71rbm9g10y") (f (quote (("linkage" "sqlite3-sys/linkage" "sqlite/linkage") ("default" "linkage"))))))

(define-public crate-spatialite-rs-0.1.6 (c (n "spatialite-rs") (v "0.1.6") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 2)) (d (n "sqlite") (r "^0") (k 0)) (d (n "sqlite3-sys") (r "^0") (k 0)))) (h "0ghr668v80b7820scfhi59rji0fxaxm76xhm91vqqfq2js32ll3h") (f (quote (("linkage" "sqlite3-sys/linkage" "sqlite/linkage") ("default" "linkage"))))))

