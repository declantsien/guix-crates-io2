(define-module (crates-io sp at spatialos-sdk) #:use-module (crates-io))

(define-public crate-spatialos-sdk-0.1.1 (c (n "spatialos-sdk") (v "0.1.1") (d (list (d (n "spatialos-sys") (r "^0.0.4") (d #t) (k 0)))) (h "1wq1rr824qglk3gd5lhm08a0zj69zmsakklw23ir6rdiniyl0rq5")))

(define-public crate-spatialos-sdk-0.1.2 (c (n "spatialos-sdk") (v "0.1.2") (d (list (d (n "spatialos-sys") (r "^0.0.5") (d #t) (k 0)))) (h "12ghqacy2cq66li50zay9p6ypylkq2vgxjgl0vnf1k1jc7la4j76")))

(define-public crate-spatialos-sdk-0.1.3 (c (n "spatialos-sdk") (v "0.1.3") (d (list (d (n "spatialos-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1kw7n2rhaq6idm6g8q96vk4h1mq5nk8hqdnwj80zrj9ib6ixi78d")))

(define-public crate-spatialos-sdk-0.1.5 (c (n "spatialos-sdk") (v "0.1.5") (d (list (d (n "spatialos") (r "^0.1") (d #t) (k 0)))) (h "1dqx2rqnlvrz8qv99yrqv92ky0lnqrccl9mrzrd4b30wqkyahvgn")))

