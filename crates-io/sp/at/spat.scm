(define-module (crates-io sp at spat) #:use-module (crates-io))

(define-public crate-spat-0.1.0 (c (n "spat") (v "0.1.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("cargo"))) (d #t) (k 0)))) (h "0lacz4dg1in9ky6brksrzj4m0mqa2mvlvjks12gnaph8wa8im9ws")))

(define-public crate-spat-0.1.1 (c (n "spat") (v "0.1.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("cargo"))) (d #t) (k 0)))) (h "0m3b8n077yk2clb658sj2jl5jamkhrrvb1kkr0y15zlfqhzk6cq1")))

(define-public crate-spat-0.2.0 (c (n "spat") (v "0.2.0") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "16xnp96nrq77nj27k82j238ik0zida5j36rpj3gdbvln8n1zk1ji") (f (quote (("default" "cli") ("cli" "pico-args"))))))

(define-public crate-spat-0.2.1 (c (n "spat") (v "0.2.1") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "06rgacfsnir3b3xdg0ypdx93z847nvsq53hxlrvf4i9ki6agnr3s") (f (quote (("default" "cli") ("cli" "pico-args"))))))

(define-public crate-spat-0.2.2 (c (n "spat") (v "0.2.2") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "056w08v6zimzaci6agalq7rw7vaz7gsshgxj3cr9n3a2mkrrx39n") (f (quote (("default" "cli") ("cli" "pico-args"))))))

(define-public crate-spat-0.2.3 (c (n "spat") (v "0.2.3") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "01hkr3j78q8h75715w22xvj6f8zn9l9l2f4jb9dw882innx2smlf") (f (quote (("default" "cli") ("cli" "pico-args"))))))

