(define-module (crates-io sp at spatialos-macro) #:use-module (crates-io))

(define-public crate-spatialos-macro-0.1.0 (c (n "spatialos-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0grwbra233yqxjnzsi41z7dp1nnwafjpya7f5n40wvj1fj7b7jkv")))

(define-public crate-spatialos-macro-0.1.1 (c (n "spatialos-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06qic6b3r3a03h8i43b53vjgfprwqjq611713dh4clv2sv5hcdkk")))

(define-public crate-spatialos-macro-0.1.2 (c (n "spatialos-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "spatialos-sdk") (r "^0.1.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1kzhif1ck0zar96szpvi44k7kqrpnp05ar1pzgbd3sxfw1xni592")))

(define-public crate-spatialos-macro-0.2.0 (c (n "spatialos-macro") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "spatialos-sdk") (r "^0.1.2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zc9462hzwh0kk6lrjdr3czkc23vgakj7r7khiimrrjgqfnamic2")))

(define-public crate-spatialos-macro-0.2.1 (c (n "spatialos-macro") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "spatialos-sdk") (r "^0.1.2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qaslx1fakbsgwkgn4zy33swmrw2wlck7afg56sgq7agslsk2c07")))

(define-public crate-spatialos-macro-0.2.2 (c (n "spatialos-macro") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "spatialos-sdk") (r "^0.1.3") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1aprzfviw4a5g3j4jp39kylbsaawfnym1l9m10mzfg5fldkpbx9m")))

(define-public crate-spatialos-macro-0.2.3 (c (n "spatialos-macro") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "spatialos-sdk") (r "^0.1.5") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06a0dy2kdsdpd4ryxd42b2aawa1kx97586qx6k9prq28f8yv8b8w")))

(define-public crate-spatialos-macro-0.2.4 (c (n "spatialos-macro") (v "0.2.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "spatialos-sdk") (r "^0.1.5") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1icbxqwzq68gajvhhvjzgn2pw47y573zwkfqxdwvshcs4g60lcsw")))

