(define-module (crates-io sp at spatialos-rs) #:use-module (crates-io))

(define-public crate-spatialos-rs-0.1.0 (c (n "spatialos-rs") (v "0.1.0") (d (list (d (n "bytevec") (r "^0.2.0") (d #t) (k 0)) (d (n "spatialos-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1cvcgg6yf19d5gqrv1d8z5jyldf8vwah8pw620dxiix8467zri2v") (y #t)))

