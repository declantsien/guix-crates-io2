(define-module (crates-io sp at spatialindex) #:use-module (crates-io))

(define-public crate-spatialindex-0.1.0 (c (n "spatialindex") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.29.0") (d #t) (k 1)))) (h "1xc14c8dmsf8mjxz0x6hdiiafyazpwkj03ybrj1rnb992npm5wci") (y #t)))

(define-public crate-spatialindex-0.2.0 (c (n "spatialindex") (v "0.2.0") (d (list (d (n "spatialindex-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1g5105vwb4jwj7rsqms61n8vjjlwbad11i4y53r0kfsprnf15ns0") (y #t)))

(define-public crate-spatialindex-0.2.1 (c (n "spatialindex") (v "0.2.1") (d (list (d (n "spatialindex-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0yaiz50dkyk4y3lxmm97hhpsfzf5pyska7w4ryx3izc53yr99z9a") (y #t)))

