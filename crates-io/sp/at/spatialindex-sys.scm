(define-module (crates-io sp at spatialindex-sys) #:use-module (crates-io))

(define-public crate-spatialindex-sys-0.1.0 (c (n "spatialindex-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.25") (d #t) (k 1)))) (h "1nv4n0jx8w3dyvn22h62l8jpknxmckph927mj4kis0dz335zfpv9") (y #t)))

