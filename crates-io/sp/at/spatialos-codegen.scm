(define-module (crates-io sp at spatialos-codegen) #:use-module (crates-io))

(define-public crate-spatialos-codegen-0.1.0 (c (n "spatialos-codegen") (v "0.1.0") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0n92q9p5lw9vsaais0pcwdj8ln1kwclcn031wwcj044d6vd2cmb1")))

(define-public crate-spatialos-codegen-0.1.1 (c (n "spatialos-codegen") (v "0.1.1") (d (list (d (n "nom") (r ">=6.0.0, <7.0.0") (d #t) (k 0)) (d (n "walkdir") (r ">=2.3.1, <3.0.0") (d #t) (k 0)))) (h "1nwip1vkrbv4w27a6vagxja8l2lkdblb1q531kdsaw9ss3xmmy5h")))

(define-public crate-spatialos-codegen-0.1.2 (c (n "spatialos-codegen") (v "0.1.2") (d (list (d (n "nom") (r ">=6.0.0, <7.0.0") (d #t) (k 0)) (d (n "walkdir") (r ">=2.3.1, <3.0.0") (d #t) (k 0)))) (h "1cp6ka6yf2l3r51wq42i6qb7l591nvwxnpcsdiil48xhax77dsp5")))

(define-public crate-spatialos-codegen-0.2.0 (c (n "spatialos-codegen") (v "0.2.0") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0vhj9ir599ks6njkxxbpypnk71fmf4qb10cdgvmq51cbsfwza9k3")))

(define-public crate-spatialos-codegen-0.2.1 (c (n "spatialos-codegen") (v "0.2.1") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0dw1c6z2fq0l3cz4q89jwhfqyp65smbcskl5496fbi4wnl58igi7")))

(define-public crate-spatialos-codegen-0.2.2 (c (n "spatialos-codegen") (v "0.2.2") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1zv12pnr49v4zpqjnapm9m0ld4js8lsxi6fgngbb4r2rmh1gmxmn")))

