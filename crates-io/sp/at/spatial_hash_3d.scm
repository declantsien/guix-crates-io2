(define-module (crates-io sp at spatial_hash_3d) #:use-module (crates-io))

(define-public crate-spatial_hash_3d-0.1.0 (c (n "spatial_hash_3d") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_derive") (r "^0.5.0") (d #t) (k 2)))) (h "15pj13xbvy7rdzj640q5rjniacsdr0c9idpwqnny3qlym5a28ylj")))

(define-public crate-spatial_hash_3d-0.1.2 (c (n "spatial_hash_3d") (v "0.1.2") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_derive") (r "^0.5.0") (d #t) (k 2)))) (h "0bgk2mydf8cjdk7sqn0kbrrcrlxypbv1v186cm9h7idzn23k2q0r")))

(define-public crate-spatial_hash_3d-0.1.3 (c (n "spatial_hash_3d") (v "0.1.3") (d (list (d (n "cgmath") (r "~0.18") (d #t) (k 0)) (d (n "criterion") (r "~0.4") (d #t) (k 2)) (d (n "itertools") (r "~0.10") (d #t) (k 0)) (d (n "rand") (r "~0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_derive") (r "~0.5") (d #t) (k 2)))) (h "0iv60rjk6mfr5141v4f4xyk0nx6hk6f3c5kidwh6mb93l54514aq")))

(define-public crate-spatial_hash_3d-0.1.4 (c (n "spatial_hash_3d") (v "0.1.4") (d (list (d (n "cgmath") (r "~0.18") (d #t) (k 0)) (d (n "criterion") (r "~0.5") (d #t) (k 2)) (d (n "itertools") (r "~0.12") (d #t) (k 0)) (d (n "rand") (r "~0.8") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "rand_derive") (r "~0.5") (d #t) (k 2)))) (h "1a8cv95m58ax2825k3acnmf9pikn1adbi6mcs7z01247jh2lypvk")))

