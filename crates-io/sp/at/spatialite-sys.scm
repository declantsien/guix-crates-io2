(define-module (crates-io sp at spatialite-sys) #:use-module (crates-io))

(define-public crate-spatialite-sys-0.1.0 (c (n "spatialite-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1vb85sy9wwgg43x3712gnzrjdn1ijdn4gk5xd0wkpqbbz2p38a8j")))

(define-public crate-spatialite-sys-0.2.0 (c (n "spatialite-sys") (v "0.2.0") (d (list (d (n "autotools") (r "^0.1") (d #t) (k 1)) (d (n "bindgen") (r "^0.47") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0qwb4j8aqkz7ahq1j9q38h0yya5271nfjrz8zmrvv8krp7g9vnhc") (l "spatialite")))

