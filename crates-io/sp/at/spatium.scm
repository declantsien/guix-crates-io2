(define-module (crates-io sp at spatium) #:use-module (crates-io))

(define-public crate-spatium-0.1.0 (c (n "spatium") (v "0.1.0") (h "1jlc1gq1q99aymh8xgwwkkvs34wx4dlndyczkzwpm90zrw6wdk1s")))

(define-public crate-spatium-0.1.1 (c (n "spatium") (v "0.1.1") (h "02z31342r64daf4q7q9k3plrhnzbybqnjqxx0lv8xpwd4waap0c6")))

