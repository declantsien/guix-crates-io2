(define-module (crates-io sp at spatial_hash) #:use-module (crates-io))

(define-public crate-spatial_hash-0.1.0 (c (n "spatial_hash") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "either") (r "^1.8.0") (d #t) (k 2)))) (h "0i3s3jfkcj7764h8z2bgaar7gw6rnldnwj4kkz58l8bhpfm96k50")))

(define-public crate-spatial_hash-0.2.0 (c (n "spatial_hash") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "either") (r "^1.8.0") (d #t) (k 2)))) (h "05w60raq1brlhnpnshf2jrs3jkdglrx8h4iawph26g1f4sn20hdc")))

