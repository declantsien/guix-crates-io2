(define-module (crates-io sp -s sp-sized-chunks) #:use-module (crates-io))

(define-public crate-sp-sized-chunks-0.1.0 (c (n "sp-sized-chunks") (v "0.1.0") (d (list (d (n "arbitrary") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "array-ops") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "bitmaps") (r "^3.1.0") (k 0)) (d (n "refpool") (r "^0.4.3") (o #t) (d #t) (k 0)))) (h "14qfzyj6i8953zb9pl1wh22r9w0pbccdwm2yms558j8y6sf1zfa8") (f (quote (("std") ("ringbuffer" "array-ops") ("default" "std"))))))

