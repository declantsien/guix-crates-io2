(define-module (crates-io sp -s sp-std) #:use-module (crates-io))

(define-public crate-sp-std-2.0.0-alpha.2 (c (n "sp-std") (v "2.0.0-alpha.2") (h "1rwn7nk1hm56h05qsz3mg4xdmasn4xa1fz0aw82by37l3wz25xwm") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-2.0.0-alpha.3 (c (n "sp-std") (v "2.0.0-alpha.3") (h "1iwn5rlp4sk9mvlkhwq91hg436hjl73vfk9gym4i30myz6wq2rx0") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-2.0.0-alpha.5 (c (n "sp-std") (v "2.0.0-alpha.5") (h "04dlf656ib9jdzmyc2sf44rspr7j3nwiylrpayxjchkhb61h6bkm") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-2.0.0-alpha.6 (c (n "sp-std") (v "2.0.0-alpha.6") (h "1444l3kgvzzd2b1wzhyrd9p8y31g54qjw16si4n0iy2x7m90da7c") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-2.0.0-alpha.7 (c (n "sp-std") (v "2.0.0-alpha.7") (h "17myq6r7ma6xqfi2r4kbfmclb42c7g2zwwsw8h7s42rm40d28ckf") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-2.0.0-alpha.8 (c (n "sp-std") (v "2.0.0-alpha.8") (h "0rx105dafrg84xidyxksn1mmvwxky5qqjc5w3gsg4vdb83rk8nxy") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-2.0.0-rc1 (c (n "sp-std") (v "2.0.0-rc1") (h "12lavrd91riizf5vwd1yr9g5aq5zfjf5gqwy8v6yh5266xwsdcz1") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-2.0.0-rc2 (c (n "sp-std") (v "2.0.0-rc2") (h "12lk389g5ixk8zd9jg16xahk3p89jxbdihn5g6f4k89nw3qr0bk5") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-2.0.0-rc3 (c (n "sp-std") (v "2.0.0-rc3") (h "0ibbl510vsfd7c5sq2bp4mddbjvbv113vxwzdjyr8nvfciqkpp5k") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-2.0.0-rc4 (c (n "sp-std") (v "2.0.0-rc4") (h "0q7i1zyam694wq5ryjzljlj91sav0k22p95d1ff1ch01vfr616my") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-2.0.0-rc5 (c (n "sp-std") (v "2.0.0-rc5") (h "16hh1vvq2f0k1db1y1w518851b7skh6p7nv6b8q75zdvh08c4n1a") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-2.0.0-rc6 (c (n "sp-std") (v "2.0.0-rc6") (h "16q5ikaigqzl4xyfg4rv1gvr2afwaq1j4498spd7k8czvm66asfx") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-2.0.0 (c (n "sp-std") (v "2.0.0") (h "1ia2pb84c21lgl6hlxmijq17ssq7gmw0xznqsfqx7lpadhb6wbgs") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-2.0.1 (c (n "sp-std") (v "2.0.1") (h "1hk6vacbfm3zvgswm3rs6wsw8kfs2x2srsycz7157pjgby7zp195") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-3.0.0 (c (n "sp-std") (v "3.0.0") (h "04lvscg2m0nmzswx2r3x2bcg7yw8fr1mnk89rdlyhppsfjliwf9m") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-4.0.0 (c (n "sp-std") (v "4.0.0") (h "0gy0ljicg7q9lj4rzlxmn3zqd0yr11wz2rdn8213hypfd5h4v00l") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-5.0.0 (c (n "sp-std") (v "5.0.0") (h "14qwk1rnicc8y66lxvvhr4hb6dwspvad7bxvdqg11gh4sg0x8gyg") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-6.0.0 (c (n "sp-std") (v "6.0.0") (h "03kn2bzzlxw4yjxaysdnzclw06pj9wwb2nxcchpjfmc4z63f43mg") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-7.0.0 (c (n "sp-std") (v "7.0.0") (h "11pr5wjmiglfysxha432rdqcy9i9xlxlk73ig1sbkdb2k7ryxs0x") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-8.0.0 (c (n "sp-std") (v "8.0.0") (h "03dcgcyxifqa1cy8f5j80g7czs58pqs0kv016j5njlyzawy8wiak") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-8.1.0-dev1 (c (n "sp-std") (v "8.1.0-dev1") (h "1x5rn8radl684a1nyk30qxh0ysbavhq61nnd51yjy40axz0h66zq") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-8.1.0-dev.2 (c (n "sp-std") (v "8.1.0-dev.2") (h "0s4pj7g4a18y7h4968lpl1xvg8lxsmzwjd3n86la33w2vb3g8ark") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-8.1.0-dev.3 (c (n "sp-std") (v "8.1.0-dev.3") (h "0ndhibdan79sj612rrjvqrny2d6rr3idm8q53cs3sml06zx6415i") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-8.1.0-dev.4 (c (n "sp-std") (v "8.1.0-dev.4") (h "0p02xwj4h54vqf6mfjwl560wxif9wyqnffq9347anakjd72l71xf") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-8.1.0-dev.5 (c (n "sp-std") (v "8.1.0-dev.5") (h "0gkrfikjdjvn17rpgplwakdgqgxjm988g0h492v36jp0lnsnkmg0") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-8.1.0-dev.6 (c (n "sp-std") (v "8.1.0-dev.6") (h "1kwq550igvb0nxik5ayklj1xqpyj58hd777g0i37l6cv2pdbs55q") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-9.0.0 (c (n "sp-std") (v "9.0.0") (h "0dm3wxr4v0if3qpm3wgf80yna66840lrpkbwkdm1nz92769vqnrd") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-10.0.0 (c (n "sp-std") (v "10.0.0") (h "174z1ilixgniybjdhxg41c9jhid9z4f9ywvz9fvfk2qffvqrxl1y") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-11.0.0-dev.1 (c (n "sp-std") (v "11.0.0-dev.1") (h "19285w1s3h4hsb746g7iacfdcpr95z37qmpq4s5cy1dff7yp00y5") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-11.0.0 (c (n "sp-std") (v "11.0.0") (h "1l5hh2vw3vhbfy5a2n1pb38d2ngcx8iwjpnfk10a222x2qpd74cc") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-12.0.0 (c (n "sp-std") (v "12.0.0") (h "01df33phwyji47lv3ir1qbswhb0ss32h6dhmgg66h9b8crd8risl") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-13.0.0 (c (n "sp-std") (v "13.0.0") (h "19xs7y1s8f4ayr8kn93if11w9bnz7ixkkki3278qb40qbwxklcki") (f (quote (("std") ("default" "std"))))))

(define-public crate-sp-std-14.0.0 (c (n "sp-std") (v "14.0.0") (h "0d28hwvdzi0r74m9i6s4518kmkc380ppcxhpfjnskc0lcjcfxy0j") (f (quote (("std") ("default" "std"))))))

