(define-module (crates-io sp -s sp-serializer) #:use-module (crates-io))

(define-public crate-sp-serializer-2.0.0-alpha.2 (c (n "sp-serializer") (v "2.0.0-alpha.2") (d (list (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1ngd2zb47f45wl7c46pkf8n81538d70dpi0pamiya0wh8krp3n42")))

(define-public crate-sp-serializer-2.0.0-alpha.3 (c (n "sp-serializer") (v "2.0.0-alpha.3") (d (list (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0m50bqyfy5w462wbki7qmmrbx8bq48h2k6w7r25n1n512a4xfwzm")))

(define-public crate-sp-serializer-2.0.0-alpha.5 (c (n "sp-serializer") (v "2.0.0-alpha.5") (d (list (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1lgq3540vjw1n642vabadljlm0n5203mh9775g66ibdr3xhy3awi")))

(define-public crate-sp-serializer-2.0.0-alpha.6 (c (n "sp-serializer") (v "2.0.0-alpha.6") (d (list (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "057qgj2f4nsl8zq7pr25b48zm8wdfwi2qxcxxxdg4jmmbvhhdczp")))

(define-public crate-sp-serializer-2.0.0-alpha.7 (c (n "sp-serializer") (v "2.0.0-alpha.7") (d (list (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "08p659kagmyygzxp7yrma6lww7svj28k1bwc15mkpphbhrhd4zkl")))

(define-public crate-sp-serializer-2.0.0-alpha.8 (c (n "sp-serializer") (v "2.0.0-alpha.8") (d (list (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "03rws0j3785vra6573fnf82g5gg84118p6sqv9d8szd1srcfmf43")))

(define-public crate-sp-serializer-2.0.0-rc1 (c (n "sp-serializer") (v "2.0.0-rc1") (d (list (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "11qy4g3pn9hyxncciri2aksgdzg1rdi6sg4rk62d9adj1r6mha45")))

(define-public crate-sp-serializer-2.0.0-rc2 (c (n "sp-serializer") (v "2.0.0-rc2") (d (list (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1pg9rn7i9hhp0clnkq2n3hl1ylkkwpskfjzrj30w7jg1nfsnnv6d")))

(define-public crate-sp-serializer-2.0.0-rc3 (c (n "sp-serializer") (v "2.0.0-rc3") (d (list (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0z8wcbd3kc9xms6lqajk0n8nxk78ijlr7yr2k9bcdrjyglwjx84a")))

(define-public crate-sp-serializer-2.0.0-rc4 (c (n "sp-serializer") (v "2.0.0-rc4") (d (list (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1fbpplg6cnaaqdxp2rk2bl0wx4zha2zlafawg6q0f5gkqgzaf3l6")))

(define-public crate-sp-serializer-2.0.0-rc5 (c (n "sp-serializer") (v "2.0.0-rc5") (d (list (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1zzb502mnm8k7248ig6q57nlmxqzxz04qbbm6c8rr1dnlkvlg3cy")))

(define-public crate-sp-serializer-2.0.0-rc6 (c (n "sp-serializer") (v "2.0.0-rc6") (d (list (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0is099gikz60j82dvk23ikwzb40kkdalw09z70gavaqwqvj036mr")))

(define-public crate-sp-serializer-2.0.0 (c (n "sp-serializer") (v "2.0.0") (d (list (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1fdjnp7dhdh316nvjns05qk5m62hf9f02yxjsk4r944pf7lk6fb4")))

(define-public crate-sp-serializer-2.0.1 (c (n "sp-serializer") (v "2.0.1") (d (list (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "02vhm3i3jk5h2jl19fg692pw8199czxbzgnv8ya0lgzsimjw5f73")))

(define-public crate-sp-serializer-3.0.0 (c (n "sp-serializer") (v "3.0.0") (d (list (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "03rhkkgc8zd7idfp3pwy1ngqn1kh2613pipz62gym7pb04zpjbcq")))

(define-public crate-sp-serializer-4.0.0 (c (n "sp-serializer") (v "4.0.0") (d (list (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1saypqj1qg7g2jl6a0jsy4r43b3hlm8h2lp76n1dpfl62fc6lvv2")))

(define-public crate-sp-serializer-5.0.0 (c (n "sp-serializer") (v "5.0.0") (d (list (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "02czcpz9ha9pxwq7l6w34ndgl5qarlkxnqi422mnik73y6zb2ncj")))

