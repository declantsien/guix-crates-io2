(define-module (crates-io sp el spelltest) #:use-module (crates-io))

(define-public crate-spelltest-0.1.0 (c (n "spelltest") (v "0.1.0") (h "09j93nqxaafdb18plh06xsgghbrsigqbyyg4vqnikm0n5wl7yp2x")))

(define-public crate-spelltest-0.1.2 (c (n "spelltest") (v "0.1.2") (h "0aakin61a42rswxkgc1yisiksixnc7wgpxn3lrvp39sfrwgng3va")))

(define-public crate-spelltest-0.1.3 (c (n "spelltest") (v "0.1.3") (h "1aik1ps1n4b0qkg5s63m4vwkg0h5yxc5xwdylgfg88sh40pzr6f4")))

(define-public crate-spelltest-0.2.0 (c (n "spelltest") (v "0.2.0") (h "17kg298pay9k6y6j988wsxzak4rhhjv5cw3sl9acrhql4wnkkhdy")))

