(define-module (crates-io sp el spelling_corrector) #:use-module (crates-io))

(define-public crate-spelling_corrector-0.1.0 (c (n "spelling_corrector") (v "0.1.0") (d (list (d (n "generator") (r "^0.6.19") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "1vmvp88qzag8i1yms70qnji8icj6p7l9q5qjrxp7vy4mpcgbl1d4")))

(define-public crate-spelling_corrector-0.1.1 (c (n "spelling_corrector") (v "0.1.1") (d (list (d (n "generator") (r "^0.6.19") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "0wfzfyhwzv4a7p4d39agqg86drn58ppq9fhvhvz92lmrzxpv9hkp")))

(define-public crate-spelling_corrector-0.2.0 (c (n "spelling_corrector") (v "0.2.0") (d (list (d (n "generator") (r "^0.6.19") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "0326xjlrn6a8zqbqandbmhzmlavkjsj5pca44h9mkr8lvrk6kqpk")))

(define-public crate-spelling_corrector-0.2.1 (c (n "spelling_corrector") (v "0.2.1") (d (list (d (n "generator") (r "^0.6.19") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "0g591aspr0vqjff08xaxsx0qq5zlkpv9l4d4c79sg6wcczf05fp8")))

(define-public crate-spelling_corrector-0.2.2 (c (n "spelling_corrector") (v "0.2.2") (d (list (d (n "generator") (r "^0.6.19") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "1m6nnzgw2mcg3qds0ldxj9k2pq2cd9xgz0ki25f0l4x533xqfbjr")))

(define-public crate-spelling_corrector-0.2.3 (c (n "spelling_corrector") (v "0.2.3") (d (list (d (n "generator") (r "^0.6.19") (d #t) (k 0)) (d (n "quicli") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "1jxdch966x0k7nyjlilj5wxrd3g1kysv3vqwr0mpbf7qzndlaqd8")))

