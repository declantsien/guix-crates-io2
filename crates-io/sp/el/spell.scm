(define-module (crates-io sp el spell) #:use-module (crates-io))

(define-public crate-spell-0.1.0 (c (n "spell") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1fd4nqbf40jsjxbdc4mccizxkcx7iy885wkm51cbz2lbg8mrn0sg")))

(define-public crate-spell-0.1.1 (c (n "spell") (v "0.1.1") (d (list (d (n "csv") (r "^1.0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "09gx6dh9fvpdjw603gbwj3pzypcf2i7zjnm0blv6pccywnw543l2")))

(define-public crate-spell-0.1.2 (c (n "spell") (v "0.1.2") (d (list (d (n "csv") (r "^1.0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "097jffl8mwj910m313z64m68hp4asmfm50k6m2ma8zzbb78mldw9")))

(define-public crate-spell-0.1.6 (c (n "spell") (v "0.1.6") (d (list (d (n "csv") (r "^1.0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1n685y32izlm2bbc49n5wgbi7v1dj0ghyqwvf98py7y0y6gr1g75")))

