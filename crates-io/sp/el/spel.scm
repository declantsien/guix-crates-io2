(define-module (crates-io sp el spel) #:use-module (crates-io))

(define-public crate-spel-0.1.0 (c (n "spel") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "1d552kdjwplww0488z8icmqqs1f9ksnj49v1cs8vy7b35vml7q3l")))

(define-public crate-spel-0.1.1 (c (n "spel") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pallrq7ay1vypknivbz18zav8mk7hgrxiqfzc3sdnq4aykij46k")))

(define-public crate-spel-0.1.2 (c (n "spel") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kqv2aqrpzvlp06mrjp4ybxffwqkdjcrg1kddz3qfjf8riwszr7i")))

(define-public crate-spel-0.1.3 (c (n "spel") (v "0.1.3") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)))) (h "1pkiybs0xc1vwvva2x1rl36i6sx48acml9w37y8gh3cg1m5qlpgn")))

(define-public crate-spel-0.1.4 (c (n "spel") (v "0.1.4") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "1g1f2v96k2jmiplgsgpmrcq8qxp1qclbk2kvacnc0ip0ql0nhbws")))

