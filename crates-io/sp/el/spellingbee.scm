(define-module (crates-io sp el spellingbee) #:use-module (crates-io))

(define-public crate-spellingbee-0.2.0 (c (n "spellingbee") (v "0.2.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sk4qk5g3nd1in88ddi1avpkr3w3hzw1lj9g7gvy188pl1017xx1")))

(define-public crate-spellingbee-0.3.0 (c (n "spellingbee") (v "0.3.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jw2hsnz41p6pac8n4ay5hdlbdl4rxi2cpxralflxcsyygskszy3")))

(define-public crate-spellingbee-1.0.0 (c (n "spellingbee") (v "1.0.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vdg2gj69znd55pj61jwsqv3l0ayc0k5f6xfn2lgm9k0by1w6bi3")))

