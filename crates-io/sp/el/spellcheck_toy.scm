(define-module (crates-io sp el spellcheck_toy) #:use-module (crates-io))

(define-public crate-spellcheck_toy-0.2.0 (c (n "spellcheck_toy") (v "0.2.0") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "stringedits") (r "^0.1.0") (d #t) (k 0)))) (h "05hgxf5c271wchrsmzz623ydbjfg9ibspsp2akj7p9i6lgb2vma0")))

(define-public crate-spellcheck_toy-0.3.0 (c (n "spellcheck_toy") (v "0.3.0") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "stringedits") (r "^0.1.1") (d #t) (k 0)))) (h "1hdxjs2nzgff16arnbl55bznyl05a6ckkdq2rmyn3iqxx43xijxi")))

(define-public crate-spellcheck_toy-0.3.1 (c (n "spellcheck_toy") (v "0.3.1") (d (list (d (n "stringedits") (r "^0.1.3") (d #t) (k 0)))) (h "0y14c3j36vhn8z5dr1nai6c0y4sfkz1hbswpkiglrx9vyh6w54v4")))

(define-public crate-spellcheck_toy-0.3.2 (c (n "spellcheck_toy") (v "0.3.2") (d (list (d (n "stringedits") (r "^0.2.0") (d #t) (k 0)))) (h "1dnqyw4mjk3n2iydqb2cvmam2a10p6zj4snx6vzml14bdrmflqj0")))

