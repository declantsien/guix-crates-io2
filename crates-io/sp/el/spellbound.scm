(define-module (crates-io sp el spellbound) #:use-module (crates-io))

(define-public crate-spellbound-0.1.0 (c (n "spellbound") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)) (d (n "cocoa") (r "^0.20.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "hunspell-sys") (r "^0.1.3") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "objbase" "spellcheck"))) (d #t) (t "cfg(windows)") (k 0)))) (h "039rmbxj8534z8sy82kgxldjwf1yvz3w7zc517x65v5kr7f6wxdx")))

(define-public crate-spellbound-0.1.1 (c (n "spellbound") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1.6") (d #t) (k 0)) (d (n "cocoa") (r "^0.20.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "hunspell-sys") (r "^0.1.3") (d #t) (t "cfg(all(unix, not(target_os = \"macos\")))") (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("combaseapi" "objbase" "spellcheck"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0f2qiy1mc902x25z6xwvlzb36jagyvzly87b07la3ypslahx7fx6")))

