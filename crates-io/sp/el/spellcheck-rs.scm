(define-module (crates-io sp el spellcheck-rs) #:use-module (crates-io))

(define-public crate-spellcheck-rs-0.1.0-beta.0 (c (n "spellcheck-rs") (v "0.1.0-beta.0") (d (list (d (n "fasthash") (r "^0.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.6.0-alpha.4") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "0ahqf1abn8f3m948d2bkijiqrczvcdbdyzxjryvm7d7hbrl1qnll")))

