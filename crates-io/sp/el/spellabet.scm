(define-module (crates-io sp el spellabet) #:use-module (crates-io))

(define-public crate-spellabet-0.1.0 (c (n "spellabet") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)))) (h "147v3lwcll0kafdmnx6bnjk30daqd0s8llwrx54v59jzrbhwg4ig") (r "1.64")))

(define-public crate-spellabet-0.1.1 (c (n "spellabet") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)))) (h "0ynkkaaijk96nsq6cmfi7l6h3m41i24fck0f7qix3a1bpf3kywkr") (r "1.64")))

(define-public crate-spellabet-0.2.0 (c (n "spellabet") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "insta") (r "^1.29.0") (d #t) (k 2)))) (h "1jjza056p337zj0dbkjdp65x9knqmp6qcy36blxrfw8khbivv7k9") (r "1.65")))

