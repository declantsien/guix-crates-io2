(define-module (crates-io sp el spelling) #:use-module (crates-io))

(define-public crate-spelling-0.1.0 (c (n "spelling") (v "0.1.0") (d (list (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1iv270g48h84hf6f00r9nhvwf9d8n15rgp80x1yb3zaarhfkf3nd")))

(define-public crate-spelling-0.2.0 (c (n "spelling") (v "0.2.0") (d (list (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "0r6j2hyvm4l1nb13c4vf2d41y970ab7fr45nsy8fqyfaj7mbn3k0") (f (quote (("use_rayon"))))))

(define-public crate-spelling-0.2.1 (c (n "spelling") (v "0.2.1") (d (list (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "0frpvmlk36pdb5pzijzgjwvplh9ripasfk80dx7f3m9zvxpf1vdc") (f (quote (("use_rayon"))))))

(define-public crate-spelling-0.2.2 (c (n "spelling") (v "0.2.2") (d (list (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "06q79r31ifczl78q5q7f9sx7ikzsrj9mhn5jm18b9wzl3zshy2n7") (f (quote (("use_rayon"))))))

(define-public crate-spelling-0.2.3 (c (n "spelling") (v "0.2.3") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0ljv153yxkfa98lc2lpslg7yf7350rly24xmshff1q0a5miqzk11") (f (quote (("use_rayon" "rayon") ("default" "use_rayon"))))))

(define-public crate-spelling-0.2.4 (c (n "spelling") (v "0.2.4") (d (list (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0arzdzm9y65pj2kzmflfvbsd911zkiiri4r26s1wjcxqxlzhllvh") (f (quote (("use_rayon" "rayon") ("default" "use_rayon"))))))

