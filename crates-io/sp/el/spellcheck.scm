(define-module (crates-io sp el spellcheck) #:use-module (crates-io))

(define-public crate-spellcheck-0.1.0 (c (n "spellcheck") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "10lwylvd5qjxylr1wcplyp4mfn2lfq4604dzyy5jb1mv56ajdvah")))

(define-public crate-spellcheck-0.1.1 (c (n "spellcheck") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0msqrihbsbppj2a78ggz2rqh340yrqqz23k0ial5bzqglb2l0hk4")))

(define-public crate-spellcheck-0.1.2 (c (n "spellcheck") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1dwm38rh4zzjvcwi9ivq05cplgmilvphqigkr65d65wramz1i1nr")))

