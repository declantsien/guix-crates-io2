(define-module (crates-io sp el spellcast) #:use-module (crates-io))

(define-public crate-spellcast-0.1.0 (c (n "spellcast") (v "0.1.0") (d (list (d (n "clone_box") (r "^0.1.3") (d #t) (k 2)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0q2r45qbl5c1ygr432aywx1afqqvwfil2jyb47axvxpam44jyrj5")))

(define-public crate-spellcast-0.1.1 (c (n "spellcast") (v "0.1.1") (d (list (d (n "clone_box") (r "^0.1.3") (d #t) (k 2)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1xj9fdj2j4nhnb67gixm2j49276c5w6xgxbm32vbx9sxhqdd4nwx")))

(define-public crate-spellcast-0.1.2 (c (n "spellcast") (v "0.1.2") (d (list (d (n "clone_box") (r "^0.1.3") (d #t) (k 2)) (d (n "moddef") (r "^0.2.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1w8c52j96b1m3vw0k4ykql93nl3fql0604ip76km2kh19gdaiixg")))

