(define-module (crates-io sp ar sparse-bitfield) #:use-module (crates-io))

(define-public crate-sparse-bitfield-0.1.0 (c (n "sparse-bitfield") (v "0.1.0") (d (list (d (n "memory-pager") (r "^0.4.1") (d #t) (k 0)))) (h "1kws1dv98bsml0fslmpx34f81cghzh14cgr2m232ypcknmnv4nh6")))

(define-public crate-sparse-bitfield-0.1.1 (c (n "sparse-bitfield") (v "0.1.1") (d (list (d (n "memory-pager") (r "^0.4.3") (d #t) (k 0)))) (h "0znf2ycfzfwa69xhbywhaicrf0zsskngnpiy3n2a6qm8284lqsmr")))

(define-public crate-sparse-bitfield-0.1.2 (c (n "sparse-bitfield") (v "0.1.2") (d (list (d (n "memory-pager") (r "^0.4.3") (d #t) (k 0)))) (h "0yn1ihkma6najcprd10xvqxpw8vz176a6apy3blk40m944wykj0p")))

(define-public crate-sparse-bitfield-0.1.3 (c (n "sparse-bitfield") (v "0.1.3") (d (list (d (n "memory-pager") (r "^0.4.3") (d #t) (k 0)))) (h "1hpx6y0k2pc86w5ik07wjv79w0487c3m09f4h71w33nlaaqfbs8c")))

(define-public crate-sparse-bitfield-0.1.4 (c (n "sparse-bitfield") (v "0.1.4") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "memory-pager") (r "^0.4.3") (d #t) (k 0)))) (h "19pknmiipqfn87a33bxnmqw625c4k4vmr2akiva4fr4jyhjgzjqq")))

(define-public crate-sparse-bitfield-0.2.0 (c (n "sparse-bitfield") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "memory-pager") (r "^0.4.3") (d #t) (k 0)))) (h "02vyji9397sy9d186a067nl8scv175kjx8d496q8f59jvnwxqwms")))

(define-public crate-sparse-bitfield-0.3.0 (c (n "sparse-bitfield") (v "0.3.0") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "memory-pager") (r "^0.4.3") (d #t) (k 0)))) (h "14k8jcz7ziq897m1fkw314jhsr3jk6a545zr6mx690d3bkkp3x1i")))

(define-public crate-sparse-bitfield-0.3.1 (c (n "sparse-bitfield") (v "0.3.1") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "memory-pager") (r "^0.4.3") (d #t) (k 0)))) (h "0hchzz6sby8mgz5c5xhcdx7mx6xc4a0nnjj02rh5bgfwyg2bh9qx")))

(define-public crate-sparse-bitfield-0.3.2 (c (n "sparse-bitfield") (v "0.3.2") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "memory-pager") (r "^0.4.6") (d #t) (k 0)))) (h "0n5vqy0w82p5plf0142gnsmr4q8kg40q05yygbdy7r8asn8a49bh")))

(define-public crate-sparse-bitfield-0.3.3 (c (n "sparse-bitfield") (v "0.3.3") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "memory-pager") (r "^0.4.6") (d #t) (k 0)) (d (n "proptest") (r "^0.6.0") (d #t) (k 2)))) (h "08g4ywvxhs9lsvnrpw6m8h7s4dx8vz838a9lrkfkhg78rbp797nq")))

(define-public crate-sparse-bitfield-0.4.0 (c (n "sparse-bitfield") (v "0.4.0") (d (list (d (n "memory-pager") (r "^0.4.6") (d #t) (k 0)) (d (n "proptest") (r "^0.6.0") (d #t) (k 2)))) (h "1nkp1p25fljiqd6zrcdsyy491l2jrhl6sh26b8rygrs4hmccdvla")))

(define-public crate-sparse-bitfield-0.5.0 (c (n "sparse-bitfield") (v "0.5.0") (d (list (d (n "memory-pager") (r "^0.6.0") (d #t) (k 0)) (d (n "proptest") (r "^0.6.0") (d #t) (k 2)))) (h "1d6iyhllsl9mc6jgni0dvyy8b2c4vba2gwhy8594k0wcv3pks9qq")))

(define-public crate-sparse-bitfield-0.6.0 (c (n "sparse-bitfield") (v "0.6.0") (d (list (d (n "memory-pager") (r "^0.6.0") (d #t) (k 0)) (d (n "proptest") (r "^0.6.0") (d #t) (k 2)))) (h "1vgrmhc75z24cphk5cap55bsmcf9jjb6ia829qrp6851cckpd6h0")))

(define-public crate-sparse-bitfield-0.7.0 (c (n "sparse-bitfield") (v "0.7.0") (d (list (d (n "memory-pager") (r "^0.6.0") (d #t) (k 0)) (d (n "proptest") (r "^0.6.0") (d #t) (k 2)))) (h "1wrnmafw8dmxvkv5a37x2mcpi1axxcsr6jdbzawv0p3fzq9f8mmb")))

(define-public crate-sparse-bitfield-0.7.1 (c (n "sparse-bitfield") (v "0.7.1") (d (list (d (n "memory-pager") (r "^0.6.0") (d #t) (k 0)) (d (n "proptest") (r "^0.6.0") (d #t) (k 2)))) (h "1xz1hkkw5g4fp1gpfar4qswi5xz50i1c2w3a0s39r6lhgklcyyxx")))

(define-public crate-sparse-bitfield-0.8.0 (c (n "sparse-bitfield") (v "0.8.0") (d (list (d (n "memory-pager") (r "^0.6.1") (d #t) (k 0)) (d (n "proptest") (r "^0.6.0") (d #t) (k 2)))) (h "0b8xnk1wf4y5j8479vnxrhd2azqjw853qka0mlkgkkb5cvkbx81p")))

(define-public crate-sparse-bitfield-0.8.1 (c (n "sparse-bitfield") (v "0.8.1") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "memory-pager") (r "^0.8.0") (d #t) (k 0)) (d (n "proptest") (r "^0.6.0") (d #t) (k 2)))) (h "01x6jj5lm0b3d4ah25d48jzq335pmd4ky68dkzdbs78bkj4f2cmw")))

(define-public crate-sparse-bitfield-0.9.0 (c (n "sparse-bitfield") (v "0.9.0") (d (list (d (n "memory-pager") (r "^0.9.0") (d #t) (k 0)) (d (n "proptest") (r "^0.6.0") (d #t) (k 2)))) (h "14y9lgx8lapf3gvsp0a1qzwklddxgw0zla4c5b5brnn08dp4645k")))

(define-public crate-sparse-bitfield-0.10.0 (c (n "sparse-bitfield") (v "0.10.0") (d (list (d (n "memory-pager") (r "^0.9.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.0") (d #t) (k 2)))) (h "1ka3g82p8gc8vjhirpwbglbpcjh5dzi7bym4d2w2qgxm23r21rq6")))

(define-public crate-sparse-bitfield-0.11.0 (c (n "sparse-bitfield") (v "0.11.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 2)) (d (n "memory-pager") (r "^0.9.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9.5") (d #t) (k 2)))) (h "0x70vsmpwmjiwnihr15z6gqihpqisfkj5y6zcwdvvjrccjfjm3pr")))

