(define-module (crates-io sp ar spare) #:use-module (crates-io))

(define-public crate-spare-0.0.1 (c (n "spare") (v "0.0.1") (d (list (d (n "num") (r ">=0.3.1") (d #t) (k 0)) (d (n "palett") (r ">=0.0.4") (d #t) (k 0)) (d (n "veho") (r ">=0.0.9") (d #t) (k 0)))) (h "1w8frpzqh4zbig21fy8p1lsxhqmcw9klkzkdacz7b1cvaqc7awrx")))

(define-public crate-spare-0.0.3 (c (n "spare") (v "0.0.3") (d (list (d (n "aryth") (r ">=0.0.11") (d #t) (k 0)) (d (n "num") (r ">=0.4.0") (d #t) (k 0)) (d (n "palett") (r ">=0.0.8") (d #t) (k 0)) (d (n "texting") (r ">=0.0.7") (d #t) (k 0)) (d (n "veho") (r ">=0.0.20") (d #t) (k 0)))) (h "0sawgrg7zpcdl9v6ablf83g0kpsb6p5x2f554gj3bb2yys46ylxc")))

