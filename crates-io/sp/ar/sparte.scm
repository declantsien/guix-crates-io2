(define-module (crates-io sp ar sparte) #:use-module (crates-io))

(define-public crate-sparte-0.0.1 (c (n "sparte") (v "0.0.1") (h "1yvzj38flhnw3wgip8w5m6gbhv71yi6hrjsbzahqxmwzrpr9s346")))

(define-public crate-sparte-0.0.2 (c (n "sparte") (v "0.0.2") (h "063711x4msgafir531jsdz6rdckvhbiv04h03yzpy6zzvqgmm5jd")))

