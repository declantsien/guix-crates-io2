(define-module (crates-io sp ar sparse_set) #:use-module (crates-io))

(define-public crate-sparse_set-0.1.0 (c (n "sparse_set") (v "0.1.0") (d (list (d (n "arbitrary") (r "^1.1.3") (o #t) (d #t) (k 0)) (d (n "coverage-helper") (r "^0.1.0") (d #t) (k 2)))) (h "1mmf5j33fzb6c0wzxfk4pyv06xdv1nfi9ml7wlf6ikh41awr0kmv")))

(define-public crate-sparse_set-0.2.0 (c (n "sparse_set") (v "0.2.0") (d (list (d (n "any_vec") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "arbitrary") (r "^1.1.3") (o #t) (d #t) (k 0)) (d (n "coverage-helper") (r "^0.1.0") (d #t) (k 2)))) (h "1h50h92xkvdb99wf2szy63w9sxb3f4wijxzn0sh6dhcmc7m6y6jh") (f (quote (("unstable" "any_vec"))))))

(define-public crate-sparse_set-0.3.0 (c (n "sparse_set") (v "0.3.0") (d (list (d (n "any_vec") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "arbitrary") (r "^1.1.3") (o #t) (d #t) (k 0)) (d (n "coverage-helper") (r "^0.1.0") (d #t) (k 2)))) (h "0zym3fdnz69v7c1y3lpd9skbamrkd1m1bra2r11rv18l3my1wj94") (f (quote (("unstable" "any_vec"))))))

(define-public crate-sparse_set-0.4.0 (c (n "sparse_set") (v "0.4.0") (d (list (d (n "any_vec") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "arbitrary") (r "^1.1.3") (o #t) (d #t) (k 0)) (d (n "coverage-helper") (r "^0.1.0") (d #t) (k 2)))) (h "0js7jam3xzmvi9fd896027k8jzp4wyj1sdz9jgzxkyvlpxb1dkcg") (f (quote (("unstable" "any_vec"))))))

(define-public crate-sparse_set-0.4.1 (c (n "sparse_set") (v "0.4.1") (d (list (d (n "any_vec") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "arbitrary") (r "^1.1.3") (o #t) (d #t) (k 0)) (d (n "coverage-helper") (r "^0.1.0") (d #t) (k 2)))) (h "1abbsww40csn24jsdl6k8zkm5nbggdg881jz35n7nq2b89dylsjl") (f (quote (("unstable" "any_vec"))))))

(define-public crate-sparse_set-0.5.0 (c (n "sparse_set") (v "0.5.0") (d (list (d (n "any_vec") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "arbitrary") (r "^1.1.3") (o #t) (d #t) (k 0)) (d (n "coverage-helper") (r "^0.1.0") (d #t) (k 2)))) (h "0vc7n96j25640q8pdsq4xf6p6b7vwapd37wsjj90sap3965dq951") (f (quote (("unstable" "any_vec"))))))

(define-public crate-sparse_set-0.6.0 (c (n "sparse_set") (v "0.6.0") (d (list (d (n "any_vec") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "arbitrary") (r "^1.1.3") (o #t) (d #t) (k 0)) (d (n "coverage-helper") (r "^0.1.0") (d #t) (k 2)))) (h "1amv1pgm7h08x94l2wk71mkq018l0p7wy537k3gnafgpakw6hay9") (f (quote (("unstable" "any_vec"))))))

(define-public crate-sparse_set-0.6.1 (c (n "sparse_set") (v "0.6.1") (d (list (d (n "any_vec") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "arbitrary") (r "^1.1.3") (o #t) (d #t) (k 0)) (d (n "coverage-helper") (r "^0.1.0") (d #t) (k 2)))) (h "1a1phfa4wwxhcnfxnnxw15x38z9h99cxlmyfdaa82lkcsv4fqfcb") (f (quote (("unstable" "any_vec"))))))

(define-public crate-sparse_set-0.7.0 (c (n "sparse_set") (v "0.7.0") (d (list (d (n "any_vec") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "arbitrary") (r "^1.1.3") (o #t) (d #t) (k 0)) (d (n "coverage-helper") (r "^0.1.0") (d #t) (k 2)))) (h "0xl45kda85lhrzjfr62nc1qxy95ka85gb63pbajipzl5bzmfnylh") (f (quote (("unstable" "any_vec"))))))

(define-public crate-sparse_set-0.7.1 (c (n "sparse_set") (v "0.7.1") (d (list (d (n "any_vec") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "arbitrary") (r "^1.1.3") (o #t) (d #t) (k 0)) (d (n "coverage-helper") (r "^0.1.0") (d #t) (k 2)))) (h "0gq031q2q51jrns54123gkhf3sjqjnk4w72fwyiwwq3d6qfw5bj1") (f (quote (("unstable" "any_vec"))))))

(define-public crate-sparse_set-0.8.0 (c (n "sparse_set") (v "0.8.0") (d (list (d (n "arbitrary") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "coverage-helper") (r "^0.1.0") (d #t) (k 2)))) (h "1fxhifv5ljmd480krxjr4vwrmjv22rk32c93kfpnii909k6258va")))

(define-public crate-sparse_set-0.8.1 (c (n "sparse_set") (v "0.8.1") (d (list (d (n "arbitrary") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "coverage-helper") (r "^0.1.0") (d #t) (k 2)))) (h "1r0xdmn0gjzry40giv4qxksqzlmi690qqss16imhfpl49vm013fj")))

(define-public crate-sparse_set-0.8.2 (c (n "sparse_set") (v "0.8.2") (d (list (d (n "arbitrary") (r "^1.3.0") (o #t) (d #t) (k 0)) (d (n "coverage-helper") (r "^0.1.0") (d #t) (k 2)))) (h "1j8mwxrr4fbmapd4sc7d7dr8s5h1s7r6ydc7pfpi26rnnlfl0i5q")))

