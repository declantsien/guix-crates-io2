(define-module (crates-io sp ar sparking-lot-core) #:use-module (crates-io))

(define-public crate-sparking-lot-core-0.1.0 (c (n "sparking-lot-core") (v "0.1.0") (d (list (d (n "loom") (r "^0.7") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)))) (h "0frag61raafwxn76fafik00fgv9wiw2dm6xfh5k1h8vckp4r8877") (f (quote (("more-concurrency"))))))

(define-public crate-sparking-lot-core-0.1.1 (c (n "sparking-lot-core") (v "0.1.1") (d (list (d (n "loom") (r "^0.7") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)))) (h "11q1p5x02apnmib0nrv4972h659mpdzcb40213yw16gyfsiavinl") (f (quote (("more-concurrency")))) (y #t)))

(define-public crate-sparking-lot-core-0.1.2 (c (n "sparking-lot-core") (v "0.1.2") (d (list (d (n "loom") (r "^0.7") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)))) (h "1vh1bwr8qjw419d2c1hbm2xw92smy4h5jap3vp51l0nj1g0i16ix") (f (quote (("more-concurrency"))))))

(define-public crate-sparking-lot-core-0.1.3 (c (n "sparking-lot-core") (v "0.1.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "loom") (r "^0.7") (f (quote ("checkpoint"))) (d #t) (t "cfg(loom)") (k 0)))) (h "1wbx00d6nwvpvrbjgi4add6x79knmd93a92l0sywc6a6nvqbgyhs") (f (quote (("thread-parker") ("more-concurrency") ("loom-test"))))))

