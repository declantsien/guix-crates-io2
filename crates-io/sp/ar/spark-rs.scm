(define-module (crates-io sp ar spark-rs) #:use-module (crates-io))

(define-public crate-spark-rs-0.4.2 (c (n "spark-rs") (v "0.4.2") (d (list (d (n "clap") (r "^4.0.32") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)))) (h "1fhfy3icj8b69z46vm4wckny4swaysqg502bcx1v2znmf1mw7z7f")))

(define-public crate-spark-rs-0.5.0 (c (n "spark-rs") (v "0.5.0") (d (list (d (n "assert_cmd") (r "^2.0.7") (d #t) (k 2)) (d (n "clap") (r "^4.0.32") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "predicates") (r "^2.1.4") (d #t) (k 2)) (d (n "sparklines") (r "^0.1.1") (d #t) (k 0)))) (h "1mj1559fnca1kbc30931gqz2fcr4zp4swm1qbqvwndpfi0hcpv80")))

