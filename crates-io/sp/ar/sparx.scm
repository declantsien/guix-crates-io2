(define-module (crates-io sp ar sparx) #:use-module (crates-io))

(define-public crate-sparx-0.1.0 (c (n "sparx") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "clippy") (r "~0") (o #t) (d #t) (k 0)))) (h "02x3a2j1l70i7kszhnyczwzkxz40r737na1crsdrj2sis34g2n1a")))

(define-public crate-sparx-0.1.1 (c (n "sparx") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "clippy") (r "~0") (o #t) (d #t) (k 0)))) (h "0pr4lgbp9wrc2rxsgdrhdz237vc4fx8y6x9fpxg7118pilacn5xm")))

(define-public crate-sparx-0.1.2 (c (n "sparx") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (k 0)))) (h "1il47fv20937n2a0fb08i6dzrv777794w4jqqqwgrv5civ15f8nc")))

