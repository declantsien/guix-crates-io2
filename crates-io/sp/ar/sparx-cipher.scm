(define-module (crates-io sp ar sparx-cipher) #:use-module (crates-io))

(define-public crate-sparx-cipher-0.1.0 (c (n "sparx-cipher") (v "0.1.0") (h "0y23g1ss1l1x2xz1irhly7chq2dfi9b82s4jwrfqky598y32blpp") (f (quote (("x64_128") ("x128_256") ("x128_128") ("default" "x128_128"))))))

(define-public crate-sparx-cipher-0.1.1 (c (n "sparx-cipher") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0") (k 0)))) (h "04rmwmfms5fbi74x5gpfx4ygfv05yx4lb3lza7fdfy7mvm60y23x") (f (quote (("x64_128") ("x128_256") ("x128_128") ("default" "x128_128"))))))

(define-public crate-sparx-cipher-0.1.2 (c (n "sparx-cipher") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.0") (k 0)))) (h "1x45saizgyczz6blrcp673ls85cvrvkvnjqwnakvgl6awmpd62jr") (f (quote (("x64_128") ("x128_256") ("x128_128") ("default" "x128_128"))))))

