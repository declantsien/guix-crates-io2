(define-module (crates-io sp ar spartan-codec) #:use-module (crates-io))

(define-public crate-spartan-codec-0.1.0 (c (n "spartan-codec") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rayon") (r "^1.5.0") (o #t) (d #t) (k 0)) (d (n "rug") (r "^1.12.0") (d #t) (k 0)))) (h "06dd34sz07hx5qadnj2j52x8ffwxj16ffvjljf1qjgqavrcfxj9q") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

