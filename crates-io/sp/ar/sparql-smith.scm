(define-module (crates-io sp ar sparql-smith) #:use-module (crates-io))

(define-public crate-sparql-smith-0.1.0-alpha.1 (c (n "sparql-smith") (v "0.1.0-alpha.1") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "05bqzmarlqai0qd81i02k9wikkq2brcjdlw35g99g1gmfqz5jdz2") (f (quote (("sep-0006") ("default"))))))

(define-public crate-sparql-smith-0.1.0-alpha.2 (c (n "sparql-smith") (v "0.1.0-alpha.2") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w6827fvh5xvmwr9fzbls8d3xqbpyd8ijqdkjgnrb3q0pbl03jq3") (f (quote (("sep-0006") ("default"))))))

(define-public crate-sparql-smith-0.1.0-alpha.3 (c (n "sparql-smith") (v "0.1.0-alpha.3") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dq9wz0wq9qgwz5gxg1lip6jvi4g8jv8mdqvq5qggjdlnfhic4pw") (f (quote (("sep-0006") ("default"))))))

(define-public crate-sparql-smith-0.1.0-alpha.4 (c (n "sparql-smith") (v "0.1.0-alpha.4") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "110f8dqm4jqq4kiw2nm2ilpwbypas6gciqil26gky77gvryp00md") (f (quote (("sep-0006") ("order") ("limit-offset" "order") ("default"))))))

(define-public crate-sparql-smith-0.1.0-alpha.5 (c (n "sparql-smith") (v "0.1.0-alpha.5") (d (list (d (n "arbitrary") (r "^1.3") (f (quote ("derive"))) (d #t) (k 0)))) (h "09mdhq50h90vix1i633v5xd78f3nw9mvyc2qnf041sbfbmaxwlvc") (f (quote (("sep-0006") ("order") ("limit-offset" "order") ("default")))) (r "1.70")))

(define-public crate-sparql-smith-0.1.0-alpha.6 (c (n "sparql-smith") (v "0.1.0-alpha.6") (d (list (d (n "arbitrary") (r "^1.3") (f (quote ("derive"))) (d #t) (k 0)))) (h "15hjpfqzdywyw51hayg4hvpr61kvnjifiw1vh2jk61w0njbaxac2") (f (quote (("sep-0006") ("order") ("limit-offset" "order") ("default")))) (r "1.70")))

(define-public crate-sparql-smith-0.1.0-alpha.7 (c (n "sparql-smith") (v "0.1.0-alpha.7") (d (list (d (n "arbitrary") (r "^1.3") (f (quote ("derive"))) (d #t) (k 0)))) (h "07r47zwr00w309n0a0zr5ln1qbzsvi03zfskdham4vny0q4mss5y") (f (quote (("sep-0006") ("order") ("limit-offset" "order") ("default")))) (r "1.70")))

