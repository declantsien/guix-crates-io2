(define-module (crates-io sp ar sparse_complex) #:use-module (crates-io))

(define-public crate-sparse_complex-0.1.0 (c (n "sparse_complex") (v "0.1.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 0)) (d (n "sparse21") (r "^0.2.1") (d #t) (k 0)))) (h "0q1vcd39xg4xg7h0i8a0y3mq8xdg64kks2chlkxjdhyib731zafz") (y #t)))

(define-public crate-sparse_complex-0.1.1 (c (n "sparse_complex") (v "0.1.1") (d (list (d (n "approx") (r "^0.4") (d #t) (k 0)) (d (n "sparse21") (r "^0.2.1") (d #t) (k 0)))) (h "0n71g8pxlb5rbvjfhw2y2kvr62i0v9y64vhcb7fq59a35rsa39lp")))

(define-public crate-sparse_complex-0.1.2 (c (n "sparse_complex") (v "0.1.2") (d (list (d (n "approx") (r "^0.4") (d #t) (k 0)) (d (n "sparse21") (r "^0.2.1") (d #t) (k 0)))) (h "1zi45sgy76kzdbb6rn9d0xzzl4w60ih0xwjq4asmgplslnlv4d2g")))

(define-public crate-sparse_complex-0.1.3 (c (n "sparse_complex") (v "0.1.3") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 1)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 1)))) (h "1f1v7cy3yqs6hnnkjm6vi53z1bl36wdmfhgmcsad1dywydzxpb0q") (y #t)))

(define-public crate-sparse_complex-0.1.4 (c (n "sparse_complex") (v "0.1.4") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "curl") (r "^0.4") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 1)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (o #t) (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)))) (h "05vpfjar7cvgfiajzlq2y4snlcncy1823zkwxmy6b54wghyn61ng")))

