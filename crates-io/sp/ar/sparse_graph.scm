(define-module (crates-io sp ar sparse_graph) #:use-module (crates-io))

(define-public crate-sparse_graph-0.1.0 (c (n "sparse_graph") (v "0.1.0") (h "12pvnd7b0hzi5cgvllrwgnabn3hwy0jsq0b99pbbj7vwz3issl3i")))

(define-public crate-sparse_graph-0.1.1 (c (n "sparse_graph") (v "0.1.1") (h "17g5jpz1pqybyabv42ksnjs6rcxqrcvwfrs437w110wy8jigis46")))

(define-public crate-sparse_graph-0.1.2 (c (n "sparse_graph") (v "0.1.2") (h "1vjb8mcgq88qaqg4kw0by8ni1dzkr6q9wgg7dz2vfa3k74x2qfhg")))

(define-public crate-sparse_graph-0.1.3 (c (n "sparse_graph") (v "0.1.3") (h "0ypn93pylnpagyzmzdc0rlq470sgfzlwr7pllx9akfa8a1rz5ncn")))

