(define-module (crates-io sp ar spark-ser7seg) #:use-module (crates-io))

(define-public crate-spark-ser7seg-0.1.0 (c (n "spark-ser7seg") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "0m888xiaab2249z18217xv2cn18j8wzi41w8zdfg3g42r4fv2sf7")))

(define-public crate-spark-ser7seg-0.1.1 (c (n "spark-ser7seg") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "14453ldn6z3j4qix0fxy75czm72fnvvdy5qc1ddnrcsk547m1pjg")))

(define-public crate-spark-ser7seg-0.2.0 (c (n "spark-ser7seg") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "0carfryzjbjkn92pinxp4z2w4azmxk6n8bajnlyh03nwnp5zpcyq")))

