(define-module (crates-io sp ar sparklet-cli) #:use-module (crates-io))

(define-public crate-sparklet-cli-0.1.0 (c (n "sparklet-cli") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0") (d #t) (k 2)))) (h "1br7cb5dw500iqp8wznkr7bqw2g2cjqy73a1ljylyqy74wmfqxdd")))

