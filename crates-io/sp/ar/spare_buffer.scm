(define-module (crates-io sp ar spare_buffer) #:use-module (crates-io))

(define-public crate-spare_buffer-0.1.0 (c (n "spare_buffer") (v "0.1.0") (h "14qz0ih3b1c5v4va1kgjm3h3w212iy6wa3ndyb1pmzllabl6dp34")))

(define-public crate-spare_buffer-0.1.1 (c (n "spare_buffer") (v "0.1.1") (h "0wycm6jz2b6c0ryggqgjm80vadc0yg1kgg8hswb2i5hm2cg1y2rg")))

