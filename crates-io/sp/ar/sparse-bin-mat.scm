(define-module (crates-io sp ar sparse-bin-mat) #:use-module (crates-io))

(define-public crate-sparse-bin-mat-0.1.0 (c (n "sparse-bin-mat") (v "0.1.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "12sx8qx4y2gfcggnpl0m9nyg7w2fs3vc9zxs0x5h0n6xw1vck453")))

(define-public crate-sparse-bin-mat-0.2.0 (c (n "sparse-bin-mat") (v "0.2.0") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "1a55smdg7gf0hwc0n8y9m58srhn68ighfmi634dgkfklwra71qfl")))

(define-public crate-sparse-bin-mat-0.2.1 (c (n "sparse-bin-mat") (v "0.2.1") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "1sjz8w065vxsjxf37wa8rk3hczpqbdcbx0nb3cg3kc513addywxw")))

(define-public crate-sparse-bin-mat-0.2.2 (c (n "sparse-bin-mat") (v "0.2.2") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "1i3948nacwj9gz4zi400x62gnaf43rzw3hin2p8sr20z67i7mgql")))

(define-public crate-sparse-bin-mat-0.2.3 (c (n "sparse-bin-mat") (v "0.2.3") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "13wsvc9fbipdawqnv03n502wv7fv8kj22skm00frzhf1qdvnm353")))

(define-public crate-sparse-bin-mat-0.2.4 (c (n "sparse-bin-mat") (v "0.2.4") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "0ra1cxqfnjwmz3a4362lrwpflvwrs8fbqvxw920jiqk61icblrxn")))

(define-public crate-sparse-bin-mat-0.2.5 (c (n "sparse-bin-mat") (v "0.2.5") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "06fw6rna2z8aczs0ywv9nnhr6vb9j550rhgjz3dpzyis2li7yxcn")))

(define-public crate-sparse-bin-mat-0.3.0 (c (n "sparse-bin-mat") (v "0.3.0") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "0chpia60apya1a5g0nw6ib97jybp9iacgxwqk3d1ha3cq0i8rsg3")))

(define-public crate-sparse-bin-mat-0.4.0 (c (n "sparse-bin-mat") (v "0.4.0") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "06q1c4ralasg4084f9fzpwgllp78cja01c3j2f676l5jyv2a40wp")))

(define-public crate-sparse-bin-mat-0.4.1 (c (n "sparse-bin-mat") (v "0.4.1") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "0ih9i3n0mbnh8b7jr53llvjmd9zh2chnpr960a7c456i4ii6yncs")))

(define-public crate-sparse-bin-mat-0.4.2 (c (n "sparse-bin-mat") (v "0.4.2") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "1yyby3hcn1gh3ppmshsap3l7pnnr6zrnd3zcd9fs31j2pay63kq4")))

(define-public crate-sparse-bin-mat-0.4.3 (c (n "sparse-bin-mat") (v "0.4.3") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "1hiwas8svp147v5fnr0ljfcwzsf38zchbky0barasr8vykyfc1a7")))

(define-public crate-sparse-bin-mat-0.4.4 (c (n "sparse-bin-mat") (v "0.4.4") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)))) (h "08vs9gcibsfd5ld5442kn29whwkzifxr86qdbdy0sdpj48l9c0yw")))

(define-public crate-sparse-bin-mat-0.4.5 (c (n "sparse-bin-mat") (v "0.4.5") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.123") (d #t) (k 2)))) (h "0m8yfidfh4h7dralcbgdqysfppp4hnbdfqzgz3lw34lqrry7k87f")))

(define-public crate-sparse-bin-mat-0.5.0 (c (n "sparse-bin-mat") (v "0.5.0") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.123") (d #t) (k 2)))) (h "0g1lmyhs4nbvfpqx2im88xvbi9xhh1clb6h0x24rli9mrm3k7v9d")))

(define-public crate-sparse-bin-mat-0.5.1 (c (n "sparse-bin-mat") (v "0.5.1") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.123") (d #t) (k 2)))) (h "0zd0v6fll7lqafa7v08khqs50zg5i7b7v0vil7m9vm82vv2c5bpz")))

(define-public crate-sparse-bin-mat-0.5.2 (c (n "sparse-bin-mat") (v "0.5.2") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.123") (d #t) (k 2)))) (h "0lbgn1z12zsm5r9kysg30qc0dbs7b99yakjwy8kc8s3aikkm4wm7")))

(define-public crate-sparse-bin-mat-0.6.0 (c (n "sparse-bin-mat") (v "0.6.0") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.123") (d #t) (k 2)))) (h "1kp1k130741g6ykj6h8379d8hd09k9nwbvqmmrxwgp1fi8wfyqxq")))

(define-public crate-sparse-bin-mat-0.6.1 (c (n "sparse-bin-mat") (v "0.6.1") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.123") (d #t) (k 2)))) (h "1rsqw232sw7q6cmyng3y3gxdk5mahfjng3yzbj5imsdwmw2jwj88")))

(define-public crate-sparse-bin-mat-0.6.2 (c (n "sparse-bin-mat") (v "0.6.2") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.123") (d #t) (k 2)))) (h "1jsqqy66g9qlzdijp26bi93vnpjgnfa6ym4fj5i4cm48adpah55b")))

(define-public crate-sparse-bin-mat-0.7.0 (c (n "sparse-bin-mat") (v "0.7.0") (d (list (d (n "is_sorted") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.123") (d #t) (k 2)))) (h "1xd2gl95vr8c1w7wfzid3386xr5sv7d48ixpaz49d3hfmhzcrmr0")))

