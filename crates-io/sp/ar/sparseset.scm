(define-module (crates-io sp ar sparseset) #:use-module (crates-io))

(define-public crate-sparseset-0.1.0 (c (n "sparseset") (v "0.1.0") (h "01lcalkc5p0z6xahcqi1a62qbch1bbm7gf6gzn8m3bs1fcsz13qr")))

(define-public crate-sparseset-0.1.0-1 (c (n "sparseset") (v "0.1.0-1") (h "0650vxh1hki6kdmzilhnh2562zih44h5wm06kkadqcj6y340k52z")))

(define-public crate-sparseset-0.1.0-2 (c (n "sparseset") (v "0.1.0-2") (h "0k49dsxbzy51x2w2pbzk0wh8fyxx6rp03v9m3bwmc2spnxgvj5rv")))

(define-public crate-sparseset-1.0.0 (c (n "sparseset") (v "1.0.0") (h "1wwgga71wjlaxfmvr58sbihpwap0aldim02vgab0vdbbmxk6n3zp")))

(define-public crate-sparseset-1.0.1 (c (n "sparseset") (v "1.0.1") (h "0vdg2vb7m7r8da18payhsrpqn46i1jvh81a86c0b0qs46dwcrkaj")))

