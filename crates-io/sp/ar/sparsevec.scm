(define-module (crates-io sp ar sparsevec) #:use-module (crates-io))

(define-public crate-sparsevec-0.1.0 (c (n "sparsevec") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "packedvec") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "vob") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "109kg4cr9a5n85pla2hqzggx5bhjygs32h2sh7kqv1278i6nyp5y")))

(define-public crate-sparsevec-0.1.1 (c (n "sparsevec") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "packedvec") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "vob") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1gn1j3hq2j43fw606614hnaqhrn556ics0lckr7xxmj10qalhdq4")))

(define-public crate-sparsevec-0.1.2 (c (n "sparsevec") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "packedvec") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "vob") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "024jv4982m9bzcx0xpr8fnh2w3y21hbg4lgq0j2z76h6724f8h0x")))

(define-public crate-sparsevec-0.1.3 (c (n "sparsevec") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "packedvec") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "vob") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1frbwjn3s66dafjf78dxkjad4rc0s8ycq17a7qp8lgvm6vya30m1")))

(define-public crate-sparsevec-0.1.4 (c (n "sparsevec") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "packedvec") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "vob") (r "^3.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1v6qipdi73mvxy0wq3rw05ijfa6xniyw6aiwck2wibh0vzsix3cj")))

(define-public crate-sparsevec-0.2.0 (c (n "sparsevec") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "packedvec") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "vob") (r ">=3.0.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1f4ky8q9dyya4glb68j1cbrzgb5k1adx8kjvxkvz6a8bb0p5vprm")))

