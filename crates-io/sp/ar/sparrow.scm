(define-module (crates-io sp ar sparrow) #:use-module (crates-io))

(define-public crate-sparrow-0.1.0 (c (n "sparrow") (v "0.1.0") (d (list (d (n "chrono") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "rstest") (r ">=0.6.0, <0.7.0") (d #t) (k 2)))) (h "02cw9vhmi89idfdr4s8y0ivqsvr627c612bn2l3c177iks9459s2")))

