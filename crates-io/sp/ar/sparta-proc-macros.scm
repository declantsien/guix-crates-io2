(define-module (crates-io sp ar sparta-proc-macros) #:use-module (crates-io))

(define-public crate-sparta-proc-macros-0.1.0 (c (n "sparta-proc-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full" "visit" "derive"))) (d #t) (k 0)))) (h "1y2z1dxvwlzq2p4cyaa4qh0z9q6wrpqw9m3zrzns43fgily04jwy")))

