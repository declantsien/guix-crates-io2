(define-module (crates-io sp ar sparse21) #:use-module (crates-io))

(define-public crate-sparse21-0.1.0 (c (n "sparse21") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "1q77ril5f67rdpflbv81g27y7i60njlsyx9k56ivbxjjvvk8iy7l")))

(define-public crate-sparse21-0.1.1 (c (n "sparse21") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "0zqvj9jfqwjf8qa18qyibklfa3x8svwnvjyj690z68cbfv37j4li")))

(define-public crate-sparse21-0.1.2 (c (n "sparse21") (v "0.1.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "0qkm6s2c5cv5g1hmzjzwgwwbmd36wnz4ihvykyikq76276iz7bin")))

(define-public crate-sparse21-0.2.0 (c (n "sparse21") (v "0.2.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "137sp8p868adjzbgf2qk45xf7m0vdi62yahyrs6wa7140v4k76c4")))

(define-public crate-sparse21-0.2.1 (c (n "sparse21") (v "0.2.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "0qzr0dic1lnq2ybby5c4lvvnar6gflpd0gfq2d29gh99rsk6llx7")))

