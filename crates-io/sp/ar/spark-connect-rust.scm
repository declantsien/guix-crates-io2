(define-module (crates-io sp ar spark-connect-rust) #:use-module (crates-io))

(define-public crate-spark-connect-rust-0.0.1-alpha.1 (c (n "spark-connect-rust") (v "0.0.1-alpha.1") (d (list (d (n "arrow") (r "^44.0.0") (f (quote ("prettyprint"))) (d #t) (k 0)) (d (n "arrow-ipc") (r "^44.0.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.9.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.9.2") (d #t) (k 1)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "11b8da08689a3q0097skv8gmqkq8c38cmdr36mfllnhnpgzghfsa")))

