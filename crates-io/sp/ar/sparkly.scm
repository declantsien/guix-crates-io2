(define-module (crates-io sp ar sparkly) #:use-module (crates-io))

(define-public crate-sparkly-0.1.0 (c (n "sparkly") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.10.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (o #t) (d #t) (k 0)))) (h "0mkyiamnmhc8ycvfbr53yka93cnh5c927kw5icm7mwvrnycv7601")))

(define-public crate-sparkly-0.1.1 (c (n "sparkly") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.10.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (o #t) (d #t) (k 0)))) (h "00y7gai6m84rvcg817n9ycmrddq3b8zblvs9nz5qmys81k7kxfzb")))

(define-public crate-sparkly-0.1.2 (c (n "sparkly") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.10.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (o #t) (d #t) (k 0)))) (h "01q89zby94vq6hj1gvb75paj43whaahin3wm9xqhfaci6c997igz")))

(define-public crate-sparkly-0.1.3 (c (n "sparkly") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.10.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (o #t) (d #t) (k 0)))) (h "0lfj04yb1fv7d235swk4cfpq3wznlsv4h3hg94fqvh412azhcnpg")))

(define-public crate-sparkly-0.1.4 (c (n "sparkly") (v "0.1.4") (d (list (d (n "ansi_term") (r "^0.10.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "proptest") (r "^0.4.0") (d #t) (k 2)) (d (n "termion") (r "^1.5.1") (o #t) (d #t) (k 0)))) (h "0j2cc4fim9xr3d4cyaz4jsdy6fq31gbjp8p815vpgjxqlnmb2kky")))

(define-public crate-sparkly-0.1.5 (c (n "sparkly") (v "0.1.5") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "proptest") (r "^0.7.0") (d #t) (k 2)) (d (n "termion") (r "^1.5.1") (o #t) (d #t) (k 0)))) (h "1h4dwbkknsz7dlmy1hgr1vawg0p8sfxdprayqvw7143nxjpbr69x")))

