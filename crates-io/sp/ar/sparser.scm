(define-module (crates-io sp ar sparser) #:use-module (crates-io))

(define-public crate-sparser-0.1.0 (c (n "sparser") (v "0.1.0") (d (list (d (n "pest") (r "^1.0.0-beta.6") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta.6") (d #t) (k 0)))) (h "0l72ldxhb47dfw8cad15ph0kqgp07xcsnwygrq5j63c3myipa96g")))

(define-public crate-sparser-0.1.1 (c (n "sparser") (v "0.1.1") (d (list (d (n "pest") (r "^1.0.0-beta.6") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta.6") (d #t) (k 0)))) (h "0cl8ilz3m1y8ax1mk75qqyc8s74lrmag6n463660jhrblyy388s3")))

