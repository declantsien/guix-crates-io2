(define-module (crates-io sp ar sparkey-sys) #:use-module (crates-io))

(define-public crate-sparkey-sys-0.1.0 (c (n "sparkey-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.39") (d #t) (k 1)))) (h "07pln3ymjin35c8qw1fkp1bia0zrigkqw7d7y8x2w29hzmb8vad5")))

(define-public crate-sparkey-sys-0.1.1 (c (n "sparkey-sys") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3.39") (d #t) (k 1)))) (h "0klpwwkq6381fc0pzz6kfib8671fr24law58v7vx1b1bxv8qgal9")))

