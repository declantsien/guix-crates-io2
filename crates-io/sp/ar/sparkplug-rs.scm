(define-module (crates-io sp ar sparkplug-rs) #:use-module (crates-io))

(define-public crate-sparkplug-rs-0.1.2 (c (n "sparkplug-rs") (v "0.1.2") (d (list (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3") (d #t) (k 1)) (d (n "protoc-rust") (r "^2.27.1") (d #t) (k 1)))) (h "0613r206w0s649ksfp1a225wz1z642zp8l0fbxg2v6hbl0085xkj")))

(define-public crate-sparkplug-rs-0.1.3 (c (n "sparkplug-rs") (v "0.1.3") (d (list (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3") (d #t) (k 1)) (d (n "protoc-rust") (r "^2.27.1") (d #t) (k 1)))) (h "1rdsvpczydlb9cnhsrrf471hnxgr60kiarigriq12j0mihm8i9l2")))

(define-public crate-sparkplug-rs-0.2.0 (c (n "sparkplug-rs") (v "0.2.0") (d (list (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3") (d #t) (k 1)) (d (n "protoc-rust") (r "^2.27.1") (d #t) (k 1)))) (h "1ian0fbvv7kvs0pr15q517ilg4rr3lvrky82d7i05nldyx93br32")))

(define-public crate-sparkplug-rs-0.2.1 (c (n "sparkplug-rs") (v "0.2.1") (d (list (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3") (d #t) (k 1)) (d (n "protoc-rust") (r "^2.27.1") (d #t) (k 1)))) (h "1ghqapa91zin5znsahwhbv4xpilw3a2j6bqpj6c2cccazv00kcw4")))

(define-public crate-sparkplug-rs-0.2.2 (c (n "sparkplug-rs") (v "0.2.2") (d (list (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3") (d #t) (k 1)) (d (n "protoc-rust") (r "^2.27.1") (d #t) (k 1)))) (h "1xlfg32jb2vfsgrlhp60cq5m0ih79arcndmhkgzazrywvn9850bl")))

(define-public crate-sparkplug-rs-0.3.0 (c (n "sparkplug-rs") (v "0.3.0") (d (list (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3") (d #t) (k 1)) (d (n "protoc-rust") (r "^2.27.1") (d #t) (k 1)))) (h "03wfdc76d399fba720rfdwjfy5yx4cb0j5gxz0r776b6z1zvxs0n") (y #t)))

(define-public crate-sparkplug-rs-0.3.1 (c (n "sparkplug-rs") (v "0.3.1") (d (list (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3") (d #t) (k 1)) (d (n "protoc-rust") (r "^2.27.1") (d #t) (k 1)))) (h "0m1b7bbxf2kxl9x9c8b7b9g3mqm1zf4ik6hplbfnr86rg8pj5cv7")))

(define-public crate-sparkplug-rs-0.3.2 (c (n "sparkplug-rs") (v "0.3.2") (d (list (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3") (d #t) (k 1)) (d (n "protoc-rust") (r "^2.27.1") (d #t) (k 1)))) (h "0f4kg2dciw4mib3ngmm8589kwaacyfyxsd4mxzvm4qljpjclghhf") (r "1.56.1")))

(define-public crate-sparkplug-rs-0.3.3 (c (n "sparkplug-rs") (v "0.3.3") (d (list (d (n "protobuf") (r "^2.27.1") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3") (d #t) (k 1)) (d (n "protoc-rust") (r "^2.27.1") (d #t) (k 1)))) (h "0w8y83g55y05vlgmac913zw0lbqvx4wzngspp4wswifjp81bq433") (r "1.56.1")))

(define-public crate-sparkplug-rs-0.4.0 (c (n "sparkplug-rs") (v "0.4.0") (d (list (d (n "protobuf") (r "^3.0.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.0.2") (d #t) (k 1)))) (h "19zbfhh68h8rv55czkyll7qb3cg4cgq7rxy15krw212lg3mjhg4r") (r "1.56.1")))

