(define-module (crates-io sp ar sparsetools) #:use-module (crates-io))

(define-public crate-sparsetools-0.1.0 (c (n "sparsetools") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "opimps") (r "^0.2") (d #t) (k 0)) (d (n "pretty_dtoa") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2") (d #t) (k 0)))) (h "0q5mlbwgvx3gwa6jnzcph6kq5bqqrhwaxpvhjwrvav5csv0bmsy4")))

(define-public crate-sparsetools-0.1.1 (c (n "sparsetools") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "opimps") (r "^0.2") (d #t) (k 0)) (d (n "pretty_dtoa") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2") (d #t) (k 0)))) (h "027ryh8gh9abscb34rcgk49jywg243jdjkz9i269qzvkif9s9pld")))

(define-public crate-sparsetools-0.1.2 (c (n "sparsetools") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "opimps") (r "^0.2") (d #t) (k 0)) (d (n "pretty_dtoa") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2") (d #t) (k 0)))) (h "0p0xh5m3hc6wwcca8yxqc32g1jprb1fx6x6fibyiwjbpdacs8n5k")))

(define-public crate-sparsetools-0.2.0 (c (n "sparsetools") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "opimps") (r "^0.2") (d #t) (k 0)) (d (n "pretty_dtoa") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2") (d #t) (k 0)))) (h "07v0mjkxpmz4b1layfm4fyvzy2bfkpx44xk4ak1ryhnchx5cwsfz")))

(define-public crate-sparsetools-0.2.1 (c (n "sparsetools") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "opimps") (r "^0.2") (d #t) (k 0)) (d (n "pretty_dtoa") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2") (d #t) (k 0)))) (h "1fs2v0k9yfyjwqy9b27gn9zvjsbxbngzh3fdqc57495jisjykh7v")))

(define-public crate-sparsetools-0.2.2 (c (n "sparsetools") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "opimps") (r "^0.2") (d #t) (k 0)) (d (n "pretty_dtoa") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2") (d #t) (k 0)))) (h "1l5097w1ijqmqswnxahxpzyssf92fdr0g3f5y70wx65cwg1armvp")))

