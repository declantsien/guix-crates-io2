(define-module (crates-io sp ar sparse_matrix) #:use-module (crates-io))

(define-public crate-sparse_matrix-0.1.0 (c (n "sparse_matrix") (v "0.1.0") (h "1l30qkiwxxpj740ph00y3kw1rsj3zngkd1093y3ypw8vwsh51wd2") (y #t)))

(define-public crate-sparse_matrix-0.1.1 (c (n "sparse_matrix") (v "0.1.1") (h "0nrcgqz51f8k5p9plpkjzg49wz6hp9s9jgcym6p526v0pidi4pwi") (y #t)))

(define-public crate-sparse_matrix-0.1.2 (c (n "sparse_matrix") (v "0.1.2") (h "1aqg6l6398h2b385dblahnsx3w1pp589vhgxrvzg1ajqh6hggpwd") (y #t)))

(define-public crate-sparse_matrix-0.1.3 (c (n "sparse_matrix") (v "0.1.3") (h "0srgln8k469sy32lrfsf8r01rgkwz3wkjym2gz3yy0d3ps95ph5v") (y #t)))

(define-public crate-sparse_matrix-0.2.0 (c (n "sparse_matrix") (v "0.2.0") (h "073dlm24q953z182h8ig8nlszr8h4w888s5jsb0mnd6igx01ddsk") (y #t)))

(define-public crate-sparse_matrix-0.2.1 (c (n "sparse_matrix") (v "0.2.1") (h "1hb7vsriqsxrs6zjhmpkr76n86vcssrdngc97v54jw1r2hibpp35") (y #t)))

(define-public crate-sparse_matrix-0.2.2 (c (n "sparse_matrix") (v "0.2.2") (h "1142zc5aa3lc7806jvfadkbxbmn86k2fshc61j7v2qjd51lbcfm6") (y #t)))

(define-public crate-sparse_matrix-0.3.0 (c (n "sparse_matrix") (v "0.3.0") (h "0q66rzq36m92bjlkdigycz9ajk98xs3q2qhr77bns04spx8ra7n5")))

(define-public crate-sparse_matrix-0.3.1 (c (n "sparse_matrix") (v "0.3.1") (h "1658y5szdnbhhhw0k9his0inrw5cax9dkrkkzbqlckjx5kkaddwx")))

(define-public crate-sparse_matrix-0.3.2 (c (n "sparse_matrix") (v "0.3.2") (h "0d95l596yxyqy1mqjhiwpnpw8wc9q633vxfl0gnc0imx3wdbfnqv")))

(define-public crate-sparse_matrix-0.3.3 (c (n "sparse_matrix") (v "0.3.3") (h "0lj3smv261jqklf6ng37049y5h6r4ap6q5ral5v97idsb8ygwipc")))

(define-public crate-sparse_matrix-0.3.4 (c (n "sparse_matrix") (v "0.3.4") (h "1y3f7hnicx4d0vsbmsz8r1r6ks4ixng1xrgjgd66sd9sqkxs7l2s")))

(define-public crate-sparse_matrix-0.4.0 (c (n "sparse_matrix") (v "0.4.0") (h "1jbl7162c5ps360373pndl0jhjakbh3ssczi8asxnpb2rrkfnprc")))

(define-public crate-sparse_matrix-0.4.1 (c (n "sparse_matrix") (v "0.4.1") (h "1wqlra5wyv8sap75f58vfy61144pjzmraxs4bpxwxkfpcgm3mval")))

(define-public crate-sparse_matrix-0.4.2 (c (n "sparse_matrix") (v "0.4.2") (h "0vw8is06638dkmgi7d8al712qj9i4sms9sk2ii9zvapfn61xwb84")))

(define-public crate-sparse_matrix-0.4.3 (c (n "sparse_matrix") (v "0.4.3") (h "1ycxdyc2vjkzhp7ylv5733fg5156n8ng5pv81p8yjd2j8lchx10c")))

(define-public crate-sparse_matrix-0.4.4 (c (n "sparse_matrix") (v "0.4.4") (h "1bvhv2y30xa5md5qx8wrid1cjb1ayqqxd877zyw34aqdbyxywzrb")))

(define-public crate-sparse_matrix-0.4.5 (c (n "sparse_matrix") (v "0.4.5") (h "1dxx4z9i1i3rfg1wkd76hvvps9v545r2nih3i96f8cxvfifk48zp")))

(define-public crate-sparse_matrix-0.4.6 (c (n "sparse_matrix") (v "0.4.6") (h "11lqjaxh7czc5jasshrvpj196afz7kfp02zw4ih02vrxs2zaw3pb")))

(define-public crate-sparse_matrix-0.5.0 (c (n "sparse_matrix") (v "0.5.0") (h "007fgar34bh4qars9agsp975xp9kilvdwf9q02zldrigsl554gq6")))

(define-public crate-sparse_matrix-0.6.0 (c (n "sparse_matrix") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ggpw61i4w7p5jdj838is3z6c1grgvmxzbaf8z62m7y09l9cgxhj")))

(define-public crate-sparse_matrix-0.6.1 (c (n "sparse_matrix") (v "0.6.1") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)))) (h "1504l69i698x30xbli2wznh3jq9p84h57pil0kjd8z6kh4qxghz4")))

