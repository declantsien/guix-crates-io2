(define-module (crates-io sp ar sparta) #:use-module (crates-io))

(define-public crate-sparta-0.1.0 (c (n "sparta") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0p93wc077316n00njqiy8xwadjqcyyr3zq75dafhg9h9sr21j9iq") (y #t)))

(define-public crate-sparta-0.1.1 (c (n "sparta") (v "0.1.1") (d (list (d (n "disjoint-sets") (r "^0.4.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "smallvec") (r "^1.9.0") (f (quote ("const_generics"))) (d #t) (k 0)) (d (n "sparta-proc-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0fiqcdr40xwpr6p7bk4nf5q927zybm0q3bq5b7hlrg6myylsxbzz")))

(define-public crate-sparta-0.1.2 (c (n "sparta") (v "0.1.2") (d (list (d (n "disjoint-sets") (r "^0.4.2") (d #t) (k 0)) (d (n "im") (r "^15.1.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "smallvec") (r "^1.9.0") (f (quote ("const_generics"))) (d #t) (k 0)) (d (n "sparta-proc-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1w0czcx2imscsa9v6l3qpp720vv839a713xmj7drk8m7qlhi2xks")))

