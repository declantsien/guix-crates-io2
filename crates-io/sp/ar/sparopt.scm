(define-module (crates-io sp ar sparopt) #:use-module (crates-io))

(define-public crate-sparopt-0.1.0-alpha.1 (c (n "sparopt") (v "0.1.0-alpha.1") (d (list (d (n "oxrdf") (r "^0.2.0-alpha.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "spargebra") (r "^0.3.0-alpha.1") (d #t) (k 0)))) (h "019kpks1aml265kbs4v2nz1kjlwjr4p2z0piw2amnb0qm7yzhlsq") (f (quote (("sep-0006" "spargebra/sep-0006") ("sep-0002" "spargebra/sep-0002") ("rdf-star" "oxrdf/rdf-star" "spargebra/rdf-star") ("default")))) (r "1.70")))

(define-public crate-sparopt-0.1.0-alpha.2 (c (n "sparopt") (v "0.1.0-alpha.2") (d (list (d (n "oxrdf") (r "^0.2.0-alpha.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "spargebra") (r "^0.3.0-alpha.2") (d #t) (k 0)))) (h "0sa2zfs8fwfnvs5mx41ka79z80gyya1qxwbjyci1ixhi9iqshvyl") (f (quote (("sep-0006" "spargebra/sep-0006") ("sep-0002" "spargebra/sep-0002") ("rdf-star" "oxrdf/rdf-star" "spargebra/rdf-star") ("default")))) (r "1.70")))

(define-public crate-sparopt-0.1.0-alpha.3 (c (n "sparopt") (v "0.1.0-alpha.3") (d (list (d (n "oxrdf") (r "^0.2.0-alpha.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "spargebra") (r "^0.2.0-alpha.3") (d #t) (k 0)))) (h "06sipr7kmyn0gr71w9yzf52lzxaiwv9vgn1sip1pzbagyykdkd9q") (f (quote (("sep-0006" "spargebra/sep-0006") ("sep-0002" "spargebra/sep-0002") ("rdf-star" "oxrdf/rdf-star" "spargebra/rdf-star") ("default")))) (r "1.70")))

(define-public crate-sparopt-0.1.0-alpha.4 (c (n "sparopt") (v "0.1.0-alpha.4") (d (list (d (n "oxrdf") (r "^0.2.0-alpha.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "spargebra") (r "^0.3.0-alpha.4") (d #t) (k 0)))) (h "1djfxc8i1msfmbg4bfjr8z7sq7ljkvyrf4wqj90rnby0qpgl7b4a") (f (quote (("sep-0006" "spargebra/sep-0006") ("sep-0002" "spargebra/sep-0002") ("rdf-star" "oxrdf/rdf-star" "spargebra/rdf-star") ("default")))) (r "1.70")))

(define-public crate-sparopt-0.1.0-alpha.5 (c (n "sparopt") (v "0.1.0-alpha.5") (d (list (d (n "oxrdf") (r "=0.2.0-alpha.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "spargebra") (r "=0.3.0-alpha.5") (d #t) (k 0)))) (h "1ympcb31slpqb8bf7ry4n08ns4lb83bvhyhkc444d328n574nf59") (f (quote (("sep-0006" "spargebra/sep-0006") ("sep-0002" "spargebra/sep-0002") ("rdf-star" "oxrdf/rdf-star" "spargebra/rdf-star") ("default")))) (r "1.70")))

