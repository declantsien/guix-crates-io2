(define-module (crates-io sp ar sparse-interp) #:use-module (crates-io))

(define-public crate-sparse-interp-0.0.1 (c (n "sparse-interp") (v "0.0.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0mpajpwbknrmdi0rj2292qnb279p5f42bhdklk0q67dby2yrr39s")))

(define-public crate-sparse-interp-0.0.2 (c (n "sparse-interp") (v "0.0.2") (d (list (d (n "num-rational") (r "^0.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0lnjvrpx5plhlfcjpgypi22rymmwc2i7shra578gf8hrhxqcahwb")))

(define-public crate-sparse-interp-0.0.3 (c (n "sparse-interp") (v "0.0.3") (d (list (d (n "num-rational") (r "^0.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0004nkfixdph3g0nrm2vjlggmriq9iydsi3ygvhn38flls9dawy4")))

(define-public crate-sparse-interp-0.0.4 (c (n "sparse-interp") (v "0.0.4") (d (list (d (n "custom_error") (r "^1.8.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "03g3ijhvfw7ra5bx1p1p8rpqkqsh6rb5dc3r63simmd93kb4i4zm")))

