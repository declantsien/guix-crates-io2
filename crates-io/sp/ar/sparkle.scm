(define-module (crates-io sp ar sparkle) #:use-module (crates-io))

(define-public crate-sparkle-0.1.0 (c (n "sparkle") (v "0.1.0") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "0kyyc1s673wkaa7wcsb05xqgypch8zlv3rqkq54kcsvi952771lc")))

(define-public crate-sparkle-0.1.1 (c (n "sparkle") (v "0.1.1") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "0gr4cpf7qzidhmn5fj698fpg1w3srii6hqna492v8r76lxay4lhm")))

(define-public crate-sparkle-0.1.2 (c (n "sparkle") (v "0.1.2") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "1in1h4s0c56sspnwby88xi0xf5gxwbkiiv06j4yqn98rqajs2yln")))

(define-public crate-sparkle-0.1.3 (c (n "sparkle") (v "0.1.3") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "0zawhwbygn25gs4hvcvll4bx971h81mrdsv6fxsr0imgbvj89s61")))

(define-public crate-sparkle-0.1.4 (c (n "sparkle") (v "0.1.4") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "03pjqpd0rv39ymc5dx69fkzy6qa1wby7jkil2xla0gs9jyf5z1n5")))

(define-public crate-sparkle-0.1.5 (c (n "sparkle") (v "0.1.5") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "1bj65rpsr3d3z7djwsmc7hf6yzhyj5dym69ijd1lyrl8nrs61c4i")))

(define-public crate-sparkle-0.1.6 (c (n "sparkle") (v "0.1.6") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "0sd77vc7d4qf1xpz1ndkkjmx7l2016v0d8zh8dr2ay7gi3r10ph3")))

(define-public crate-sparkle-0.1.7 (c (n "sparkle") (v "0.1.7") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "0fanqids0zc10jxci9b9h9jhwcakqlmb6b6d1p2bsmqx4iawaqwf")))

(define-public crate-sparkle-0.1.8 (c (n "sparkle") (v "0.1.8") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "12897i8hxfgwdvslvahxh364q7rkr00p01wb434hy94hfzwr7q95")))

(define-public crate-sparkle-0.1.9 (c (n "sparkle") (v "0.1.9") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "0va1l3a7c2f1jn750va36q8raqa8ybi019chia1izkigshb3nxmp")))

(define-public crate-sparkle-0.1.10 (c (n "sparkle") (v "0.1.10") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "1550axkmqms2051lqpgm9wmfacb8ri2wc6ikg8jqph6w3mikbfc4")))

(define-public crate-sparkle-0.1.11 (c (n "sparkle") (v "0.1.11") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "0h2i7m8vgh0md2gi5gd9r843qcgzg6vh0iw3yny402c6q30xn1w7")))

(define-public crate-sparkle-0.1.12 (c (n "sparkle") (v "0.1.12") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "1m3x9iv7vgk03l6j2pkzcbr56ph417kg0r9lrchzwmry2fjq4zzg")))

(define-public crate-sparkle-0.1.13 (c (n "sparkle") (v "0.1.13") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "1hdzj795l7dfm1pw7fhwq4vd67ic7kih6q342h4ps5919x34z93g")))

(define-public crate-sparkle-0.1.14 (c (n "sparkle") (v "0.1.14") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "02a3h08g7pl08gl9cqf55slqkxccfx875q982yvs6dayilqfx9sw")))

(define-public crate-sparkle-0.1.15 (c (n "sparkle") (v "0.1.15") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "082fr2bjcg877bkwidsb61xvl3jbsbhqqpyfvgslsnqs2inrql25")))

(define-public crate-sparkle-0.1.16 (c (n "sparkle") (v "0.1.16") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "0aj4ph3lsjfk1q7wgzq4h407mkag353dffi4mjvq0qlr6br5dzv9")))

(define-public crate-sparkle-0.1.17 (c (n "sparkle") (v "0.1.17") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "0zlfgi962swvf3jww59dwqs73qpr3r5c3jdlsfbf15726z9a7xn4")))

(define-public crate-sparkle-0.1.18 (c (n "sparkle") (v "0.1.18") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "0927fs86amsi12gj29llsk9p6402vxvfgzdhvgz61bfg06pwlx3m")))

(define-public crate-sparkle-0.1.19 (c (n "sparkle") (v "0.1.19") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "0aabr4fkxf0cqssfvzj8502kjdszani0pacc1z0hsif9jmyl0d7k")))

(define-public crate-sparkle-0.1.20 (c (n "sparkle") (v "0.1.20") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "0d35iy9l508zhn2dynl1dwsl4xcfmjd5imgd0pdrzd82riqvx2fw")))

(define-public crate-sparkle-0.1.21 (c (n "sparkle") (v "0.1.21") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "08gfcdq5zwc9hj993yfyrkjnrz5c9w6s841gz04vxml0k2c0ivfg")))

(define-public crate-sparkle-0.1.22 (c (n "sparkle") (v "0.1.22") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "1by001ds15axch664j7n8r1l6gq39qj7m5di8pwfn5bbhs7rra9l")))

(define-public crate-sparkle-0.1.23 (c (n "sparkle") (v "0.1.23") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "0m84b33rhbdcmq6ibh05dkk8bxl3p2a9lchxa4jrfabf1k6rc29k")))

(define-public crate-sparkle-0.1.24 (c (n "sparkle") (v "0.1.24") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "0x318qikhbibf546sfiwfasr2f56bd4syfg2lz1379b14qlpzkdp")))

(define-public crate-sparkle-0.1.25 (c (n "sparkle") (v "0.1.25") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)))) (h "1yi5i1q9pc94qfa3cphj0d5cczr0hksigvwgv9n11gyxv1g2i13i")))

(define-public crate-sparkle-0.1.26 (c (n "sparkle") (v "0.1.26") (d (list (d (n "gl_generator") (r "^0.14") (d #t) (k 1)))) (h "1q2q5vpb55k03rf0m4dzmmi3q8lwf0b0f8wlk3g7g4fs8rinrz3l")))

