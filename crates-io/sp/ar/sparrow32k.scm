(define-module (crates-io sp ar sparrow32k) #:use-module (crates-io))

(define-public crate-sparrow32k-0.1.0 (c (n "sparrow32k") (v "0.1.0") (h "0wsis16l9q823jn97gwkmg222r7qwn3kwlzqnvk0206vbjl9kpdn")))

(define-public crate-sparrow32k-0.1.1 (c (n "sparrow32k") (v "0.1.1") (h "00d98i649nl03khgxclzyagrvq0r9ijxakb7gvy6g6fpil4m2wif")))

(define-public crate-sparrow32k-0.1.2 (c (n "sparrow32k") (v "0.1.2") (h "1q5k2glpl56b8xv42knidzfwwwz6qzgz2asazcqd2m1a6rchyzrf")))

