(define-module (crates-io sp ar sparklet) #:use-module (crates-io))

(define-public crate-sparklet-0.1.0 (c (n "sparklet") (v "0.1.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)))) (h "0w6zdd57viqw71q9dk6sac6clwl7kiqv109kwqp19r5jsvy42frk")))

(define-public crate-sparklet-0.1.1 (c (n "sparklet") (v "0.1.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)))) (h "1340x4rss0li2mvgb7k7nsdslxybar8ni762g7cgnwlkf8k0ck3d")))

