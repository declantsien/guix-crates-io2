(define-module (crates-io sp ar spark) #:use-module (crates-io))

(define-public crate-spark-0.3.0 (c (n "spark") (v "0.3.0") (h "0qwcdakw12wqm7fcb9k5b5hrik2r0287v45cr603700pssy9q0x6")))

(define-public crate-spark-0.3.1 (c (n "spark") (v "0.3.1") (h "1r281mk581416w6hp3fw8zg51il5fn6sfb041b7h1f8dn657n2cf")))

(define-public crate-spark-0.3.2 (c (n "spark") (v "0.3.2") (h "0ba7mbqnpxfj9rsni941x81c9g5gmfxf3qdjn14s92cdv3wa68ma")))

(define-public crate-spark-0.4.0 (c (n "spark") (v "0.4.0") (h "1js70d972h518dv8brg6d078dh4v05vysa1x0alzlsiiwm6brzfd")))

