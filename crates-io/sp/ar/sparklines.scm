(define-module (crates-io sp ar sparklines) #:use-module (crates-io))

(define-public crate-sparklines-0.0.0 (c (n "sparklines") (v "0.0.0") (h "0a47pjawq0hmh56jxvq3qnhg1g63jb16wrcglv8nsjmyrn3p6mnb")))

(define-public crate-sparklines-0.0.1 (c (n "sparklines") (v "0.0.1") (d (list (d (n "test-case") (r "^2.2.2") (d #t) (k 2)))) (h "0vb12pmyy67ms6kcbg2wny2ymhvqrkgjysnzx5wba59ls81f0qj5")))

(define-public crate-sparklines-0.1.0 (c (n "sparklines") (v "0.1.0") (d (list (d (n "test-case") (r "^2.2.2") (d #t) (k 2)))) (h "138v1cy6f88h8aglnv2gnnwqqasq2dhkikv8x4vhpr1wz2swslg6")))

(define-public crate-sparklines-0.1.1 (c (n "sparklines") (v "0.1.1") (d (list (d (n "test-case") (r "^2.2.2") (d #t) (k 2)))) (h "0kg4b759hhcbghnj76x93p71nd61vgixkkkgb83531rx4l9lazww")))

(define-public crate-sparklines-0.2.0 (c (n "sparklines") (v "0.2.0") (d (list (d (n "test-case") (r "^2.2.2") (d #t) (k 2)))) (h "1i8035gm5j7rj6imlspx7yphvgdzglk38vnyr1sybjkn8mc978ll")))

(define-public crate-sparklines-0.2.1 (c (n "sparklines") (v "0.2.1") (d (list (d (n "test-case") (r "^2.2.2") (d #t) (k 2)))) (h "16dfp7mv053lfhgvsayjbrlrkh3z2l745b20arblgcvsndr27vy6")))

