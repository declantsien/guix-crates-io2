(define-module (crates-io sp ar sparse-flash) #:use-module (crates-io))

(define-public crate-sparse-flash-0.1.0 (c (n "sparse-flash") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.15") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "nix") (r "^0.24.2") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)))) (h "0c7wchp6crwibr3sskm84flh957fgs55nbzsmcla642rs0x6mfd9")))

