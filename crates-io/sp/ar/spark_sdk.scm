(define-module (crates-io sp ar spark_sdk) #:use-module (crates-io))

(define-public crate-spark_sdk-0.0.1 (c (n "spark_sdk") (v "0.0.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "fuels") (r "^0.43.0") (f (quote ("fuel-core-lib"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0xgs9phphk4ngvsw40m7b075ic5qvy2l03j7znkwk3fnlchs6yf7")))

(define-public crate-spark_sdk-0.0.2 (c (n "spark_sdk") (v "0.0.2") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "fuels") (r "^0.41.1") (f (quote ("fuel-core-lib"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "src20_sdk") (r "^0.0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "088v4snxj8gya3xmmxdmmj59bkxskkd8rhyf3ki3hxcc7d0qq6hz")))

