(define-module (crates-io sp ar sparklet-tui) #:use-module (crates-io))

(define-public crate-sparklet-tui-0.1.0 (c (n "sparklet-tui") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "pancurses") (r "^0.17.0") (d #t) (k 0)) (d (n "predicates") (r "^3.0") (d #t) (k 2)))) (h "1if2fpb0sfg3i40nbrxzliswjrg1iw4q0rc6i2xxiz01fcx6a8j5")))

