(define-module (crates-io sp ar sparse) #:use-module (crates-io))

(define-public crate-sparse-0.0.0 (c (n "sparse") (v "0.0.0") (h "1liwjg4qi747x8ckn7qizygrlxwcdgzzh26mdsb88d3g13y5lhjr") (y #t)))

(define-public crate-sparse-0.0.1 (c (n "sparse") (v "0.0.1") (h "1v9zmbb5nfmb05mj1f3b2bsg149b7pacnifxwx9i4bhnnacj0ydc") (y #t)))

(define-public crate-sparse-0.1.0 (c (n "sparse") (v "0.1.0") (h "0yy6sdcp8bmgxclc01hfc5jsfsadaxqqj2ynz7aac3h5kvkgsd6x") (y #t)))

(define-public crate-sparse-0.2.0 (c (n "sparse") (v "0.2.0") (h "1m4yjlhf35ic2f5dyn73j91qz6gwllxy98wwrz7fz87sbw46iv1k") (y #t)))

(define-public crate-sparse-0.3.0 (c (n "sparse") (v "0.3.0") (h "0y5wz1m3mr6w7kalxbvqjfwsf4dm5by0z76nd8kp340nqna0a587") (y #t)))

(define-public crate-sparse-0.0.2 (c (n "sparse") (v "0.0.2") (h "1pvr5sp5vln71gis5bin995dznp5v6wrwwbk9gj3w1dz66vqnd4d") (y #t)))

(define-public crate-sparse-0.1.1 (c (n "sparse") (v "0.1.1") (h "18fspkjar6qsdlhf3zb0aw2c2dpwxhqqh4sf16k26bdla9r2krm9")))

(define-public crate-sparse-0.1.2 (c (n "sparse") (v "0.1.2") (h "0p2pr0v6qwnwm1zpl7wyq9ydbjxnqhcqvh30nr0mmar65bv1x7sj")))

