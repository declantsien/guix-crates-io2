(define-module (crates-io sp ar sparkv) #:use-module (crates-io))

(define-public crate-sparkv-0.1.0 (c (n "sparkv") (v "0.1.0") (h "02m61immdvi4fry7dgx6ibnfy3nrif99bcb77sqbrdavxyvfpx20")))

(define-public crate-sparkv-0.1.1 (c (n "sparkv") (v "0.1.1") (h "0h2w6k6rcbxs73clz1j3i97jxfbcp6914gc9vw0659mhdx7fygjf")))

