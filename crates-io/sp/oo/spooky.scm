(define-module (crates-io sp oo spooky) #:use-module (crates-io))

(define-public crate-spooky-0.1.0 (c (n "spooky") (v "0.1.0") (d (list (d (n "clap") (r "^2.3") (d #t) (k 0)) (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1vd17bjj79srqkwh78xbryv0v5p819yc72c9aiapprj86c7vrj8b")))

(define-public crate-spooky-0.1.1 (c (n "spooky") (v "0.1.1") (d (list (d (n "clap") (r "^2.3") (d #t) (k 0)) (d (n "crc") (r "^1.2") (d #t) (k 0)) (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1wjxym8xqh1lwvs5nlhmxp3gg74k12jpixkd4zyjfra5q1md2x3y")))

