(define-module (crates-io sp oo spooks) #:use-module (crates-io))

(define-public crate-spooks-0.0.0 (c (n "spooks") (v "0.0.0") (d (list (d (n "easy_reader") (r "^0.5.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0pfi7bgx47n3rlw20b0l5mri6i4hb6kl89kjcvwhm1r56kycxdp3")))

(define-public crate-spooks-0.1.0 (c (n "spooks") (v "0.1.0") (d (list (d (n "easy_reader") (r "^0.5.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0xvra84x7r9aj6l75y2241bxkwkqa1k2rxndlj9gpbx0s0drln72")))

(define-public crate-spooks-0.1.2 (c (n "spooks") (v "0.1.2") (d (list (d (n "easy_reader") (r "^0.5.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0cxlsjc6l6791sfb0f7kjl9w3vmsqy2g0dx0pjd0jnppid6gl3sm")))

(define-public crate-spooks-0.2.0 (c (n "spooks") (v "0.2.0") (d (list (d (n "easy_reader") (r "^0.5.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0lhvg374gkbas2fspglca1xmjnd449azjv427rk0xqfp88k6xkzm")))

(define-public crate-spooks-0.2.1 (c (n "spooks") (v "0.2.1") (d (list (d (n "easy_reader") (r "^0.5.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0w1b0piglic7hz0kc610w0l8fq77ylmkp0vsz9gkricdak8ddps8")))

(define-public crate-spooks-0.2.2 (c (n "spooks") (v "0.2.2") (d (list (d (n "easy_reader") (r "^0.5.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1mg5c7fpk51nml01d0qqm3kiizvcnzq08x1f93bp28mhijkipacq")))

(define-public crate-spooks-0.2.3 (c (n "spooks") (v "0.2.3") (d (list (d (n "easy_reader") (r "^0.5.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0fn3csqzif1yksppki68h2zsij2ckry08df1wmm14hbn7gsm7na3")))

(define-public crate-spooks-0.2.4 (c (n "spooks") (v "0.2.4") (d (list (d (n "easy_reader") (r "^0.5.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0chacn06rgwh4y480ww2jxiar7i55k0kqpcd20sppqy5klxf3kf3")))

(define-public crate-spooks-0.2.5 (c (n "spooks") (v "0.2.5") (d (list (d (n "easy_reader") (r "^0.5.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0k6cppnwblw5ax8fw9yaj98gvly27b6a3pl5saxw2znz2pmj1pgc")))

(define-public crate-spooks-0.2.6 (c (n "spooks") (v "0.2.6") (d (list (d (n "easy_reader") (r "^0.5.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0jxd9qw5xhnp01v58w4i48vifbb5533a8jcq4v17qgqwar61n04d")))

