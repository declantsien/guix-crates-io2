(define-module (crates-io sp oo spooky-season) #:use-module (crates-io))

(define-public crate-spooky-season-0.0.0 (c (n "spooky-season") (v "0.0.0") (h "10hbqmg45lxgiafjsg9mhy91kkq9cdm59abqdrvfk9875fy682ay")))

(define-public crate-spooky-season-1.0.0 (c (n "spooky-season") (v "1.0.0") (d (list (d (n "time") (r "^0.3.3") (d #t) (k 0)))) (h "1cj2fbszdigsgl8b1c631kq9pkn04y9sv2zks6qmzmr5yi41nk65")))

