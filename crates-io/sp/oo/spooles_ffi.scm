(define-module (crates-io sp oo spooles_ffi) #:use-module (crates-io))

(define-public crate-spooles_ffi-0.1.0 (c (n "spooles_ffi") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "03851dmbay44adhnv6i1bb5gv16gw8hcj64mmgiwa2g9p5ssznh5") (y #t)))

(define-public crate-spooles_ffi-0.1.1 (c (n "spooles_ffi") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "1wb0qbg4ify8x3clsvvbb35p3xznzvr54z44zz0zacmcy1xgchpk")))

