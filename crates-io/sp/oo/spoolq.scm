(define-module (crates-io sp oo spoolq) #:use-module (crates-io))

(define-public crate-spoolq-0.1.0 (c (n "spoolq") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "notify") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "textnonce") (r "^0.4.1") (d #t) (k 0)))) (h "1n47dfvjkgnn9mg7jn62q5gclbafa5nl9bpwra440rwm2aj5cmsm")))

(define-public crate-spoolq-0.1.1 (c (n "spoolq") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "textnonce") (r "^0.4.1") (d #t) (k 0)))) (h "1iakd6jv60h78sa7wx98cbfnigngfvbapygbl6g6mxf9ynhkls7s")))

(define-public crate-spoolq-0.2.0 (c (n "spoolq") (v "0.2.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "textnonce") (r "^0.4.1") (d #t) (k 0)))) (h "1x2ivyg0s56baqwnv1zdb2sq6x44a4mhhl888g2j374ih9d605na")))

(define-public crate-spoolq-0.2.1 (c (n "spoolq") (v "0.2.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "textnonce") (r "^0.4.1") (d #t) (k 0)))) (h "00gbx2bab2gcf7h92jwh2pdmxp6nifmvsn0b0chbyymd3j8wsx7g")))

(define-public crate-spoolq-0.2.2 (c (n "spoolq") (v "0.2.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "textnonce") (r "^0.4.1") (d #t) (k 0)))) (h "05vm7wgjn71fvg67y53s1zb1kb35xsba83w8nv52lwxwaypmfcf3")))

(define-public crate-spoolq-0.2.3 (c (n "spoolq") (v "0.2.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "textnonce") (r "^0.4.1") (d #t) (k 0)))) (h "03a1s6rprcs1pq5dbxx84sh4wy9nr3wqgl2yjlr4ibl8k93glsv1")))

