(define-module (crates-io sp oo spool) #:use-module (crates-io))

(define-public crate-spool-0.1.0 (c (n "spool") (v "0.1.0") (h "1icyf5mdr7js0arfjbnd71hfi21czgrn2phgp1fpafwk5zk3k8y7")))

(define-public crate-spool-0.1.1 (c (n "spool") (v "0.1.1") (h "1r2307k0fqczmxw601w2h5xzlcyvsyl0cdr4rv6s212765b2il7f")))

(define-public crate-spool-0.1.2 (c (n "spool") (v "0.1.2") (h "00ddn40v67yd393gn07fisc1sjjncsgl1y8ahmlf1dw4ags9x87d")))

(define-public crate-spool-0.1.3 (c (n "spool") (v "0.1.3") (h "16p8jgpy3zs9h1c0021h26y0s1vmmia3i86nimz3y81ch2zvmzrk")))

