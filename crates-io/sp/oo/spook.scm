(define-module (crates-io sp oo spook) #:use-module (crates-io))

(define-public crate-spook-0.1.0 (c (n "spook") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "httparse") (r "^1.3.3") (d #t) (k 0)) (d (n "notify") (r "^4.0.11") (d #t) (k 0)))) (h "1jwpp75agkg1gzs91lh3gv2swbsf53cdny61wryj8v9mm983isnn")))

(define-public crate-spook-0.1.1 (c (n "spook") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "httparse") (r "^1.3.3") (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)))) (h "07m816lvhl5pkzmw67ick54iixa8ifqqx0a7iv63pwm09770qrw8")))

(define-public crate-spook-0.1.2 (c (n "spook") (v "0.1.2") (d (list (d (n "clap") (r "^2.34") (f (quote ("suggestions"))) (k 0)) (d (n "httparse") (r "^1.3.3") (d #t) (k 0)) (d (n "notify") (r "^4.0.17") (d #t) (k 0)))) (h "14syiy7dqzsd2y6vz6nzqlbqphzrvbbgkcn0223ihj7fivmfa1pn")))

