(define-module (crates-io sp ee speex-sys) #:use-module (crates-io))

(define-public crate-speex-sys-1.2.1 (c (n "speex-sys") (v "1.2.1") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1j3h6w9qzpip0mp1xfdb8wsaivflxiiwm0n64ili3pkdrmj97pq8") (y #t) (l "speex")))

(define-public crate-speex-sys-0.1.0 (c (n "speex-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1i7as8jp9rvl004ary2gijv4ihvf4qqiczk414k6xzjizxbbihfc") (l "speex")))

(define-public crate-speex-sys-0.2.0 (c (n "speex-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0q5gm99h3fhsq76ayavblzkjgw7xp2pa2bn1g4ppqcqzy895ysqf") (l "speex")))

(define-public crate-speex-sys-0.3.0 (c (n "speex-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0qffarh9a8bbwfgqj0pkxfvpbf1pwpcrkac5hpj3w1lrdgdrrijq") (l "speex")))

(define-public crate-speex-sys-0.4.0 (c (n "speex-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1q44jab4y5xcvzwjh2dakjyak7r8rdsfkdvwhs38aghm2niy9kaa") (l "speex")))

