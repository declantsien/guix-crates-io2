(define-module (crates-io sp ee speedate) #:use-module (crates-io))

(define-public crate-speedate-0.1.0 (c (n "speedate") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "iso8601") (r "^0.4.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)))) (h "1yjh9l2f9ncmm2br1x82hmfwfd7v58bglcbzhj1cmikvvcx47l95")))

(define-public crate-speedate-0.1.1 (c (n "speedate") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "iso8601") (r "^0.4.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0qdfbsyyxckn8ns6dy701y3kb6lmdxcyavvncfmgq89amyxwx4wq")))

(define-public crate-speedate-0.2.0 (c (n "speedate") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "iso8601") (r "^0.4.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0g8knd8xcs3nlwnjzd4bn8xzwvwyrip9zfy4m7v5bblnvrd58770")))

(define-public crate-speedate-0.3.0 (c (n "speedate") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "iso8601") (r "^0.4.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1ml3sqpjamia0ilaz90jlgaz5hgsfibshd9lkcpp6mb1jjsidrxa")))

(define-public crate-speedate-0.4.0 (c (n "speedate") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "iso8601") (r "^0.4.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "03gab0b17wpp8nb1fsj5p50x73zpis9fjxy4152d1hy29i8kzv83")))

(define-public crate-speedate-0.4.1 (c (n "speedate") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "iso8601") (r "^0.4.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "05n9079q1pag3k7vzby6jiyrbhddh74lqq07w5byiasl4152hqr6")))

(define-public crate-speedate-0.5.0 (c (n "speedate") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "iso8601") (r "^0.4.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0wjhdvpgg1rkmadv99s7m4wbl3r1qx0pa635cpscgpznzq8j353q")))

(define-public crate-speedate-0.6.0 (c (n "speedate") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "iso8601") (r "^0.4.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0866nni4sz3bqlrijvx7wfhxnq5l426m1si74c50avfwspav16ji")))

(define-public crate-speedate-0.7.0 (c (n "speedate") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "iso8601") (r "^0.4.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1f8klfqqxc4xhjx9jd8d7mnn7xzrv5x2zvz4zbpwrh2zdg3f1nlx")))

(define-public crate-speedate-0.8.0 (c (n "speedate") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "iso8601") (r "^0.4.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1v17jk143r3ylm5g33c53k4fy12aqlpiv73m4hw7kl5g44avd7ig")))

(define-public crate-speedate-0.8.1 (c (n "speedate") (v "0.8.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "iso8601") (r "^0.4.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1c312wsk71lyspc34rpl662iz1s7aq1im0qjyjy6vqnwnhk9pz8f")))

(define-public crate-speedate-0.9.0 (c (n "speedate") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "iso8601") (r "^0.4.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "0xcfd7j06r2kzr9sj05lc3v345jldlwpvqlxnvbxnv7rwz5h6bii")))

(define-public crate-speedate-0.9.1 (c (n "speedate") (v "0.9.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "iso8601") (r "^0.4.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "1xps23ikjdf27i6g3bmnw6s5raryb34slx4r0bddxi213j0sykgb")))

(define-public crate-speedate-0.10.0 (c (n "speedate") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "iso8601") (r "^0.4.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "05l6jkbc91z076v3jgyf76sr3mc0l925jw8chxxbvrwsm4ayyidg")))

(define-public crate-speedate-0.11.0 (c (n "speedate") (v "0.11.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "iso8601") (r "^0.4.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "18par3qa6ni99a90w142kvs8xxgsf139gxi3cas6rkqh4nj4wkp5")))

(define-public crate-speedate-0.12.0 (c (n "speedate") (v "0.12.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "iso8601") (r "^0.4.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "0cnkzrz9ih56vygs36jhxh8xqcs1ipjb6d7qylj347vwwqby2a60")))

(define-public crate-speedate-0.13.0 (c (n "speedate") (v "0.13.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "iso8601") (r "^0.4.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "16xi4d98ab3ag7rkba2fv37kk3d0fgg0l287hq4vz36i1z2pcbr4")))

(define-public crate-speedate-0.14.0 (c (n "speedate") (v "0.14.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "iso8601") (r "^0.4.1") (d #t) (k 2)) (d (n "paste") (r "^1.0.7") (d #t) (k 2)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.25") (d #t) (k 0)))) (h "10675dm75pkf7jglvkp3rzxykk2464j6swkz58d5lnnfzvkc88y3")))

