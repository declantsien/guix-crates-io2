(define-module (crates-io sp ee speed_test) #:use-module (crates-io))

(define-public crate-speed_test-0.1.0 (c (n "speed_test") (v "0.1.0") (d (list (d (n "colour") (r "^0.7.0") (d #t) (k 0)))) (h "0027n6xz4ysiii1yr5wzj86g5pp9pmm9p1vmanqd3n373xylkcd5")))

(define-public crate-speed_test-0.1.1 (c (n "speed_test") (v "0.1.1") (d (list (d (n "colour") (r "^0.7.0") (d #t) (k 0)))) (h "0cppwj3vcgb8af3wrzjlinl2w8dkjx3awh85bcmr8v30m0785zs9") (y #t)))

(define-public crate-speed_test-0.1.2 (c (n "speed_test") (v "0.1.2") (d (list (d (n "colour") (r "^0.7.0") (d #t) (k 0)))) (h "1sxyvl7j914jikr1myc48dlsm2wfr01ni7pkkfbl2z74x9rjmddf") (y #t)))

(define-public crate-speed_test-0.1.3 (c (n "speed_test") (v "0.1.3") (d (list (d (n "colour") (r "^0.7.0") (d #t) (k 0)))) (h "084ynq5v4c4gv6mmbqm5b0xq0i7qzzf6vfjmcgngbl09dd5f05pk") (y #t)))

(define-public crate-speed_test-0.1.4 (c (n "speed_test") (v "0.1.4") (d (list (d (n "colour") (r "^0.7.0") (d #t) (k 0)))) (h "101q0anq32hankv0ii23qbi3r9ds429hkqc6n8mlsrlvq91aaq6c")))

(define-public crate-speed_test-0.1.5 (c (n "speed_test") (v "0.1.5") (d (list (d (n "colour") (r "^0.7.0") (d #t) (k 0)))) (h "161ivqrk36qi34bi7mzlg26f07vrj62yccqmmr4gacc9p5iz3d5f")))

(define-public crate-speed_test-0.1.6 (c (n "speed_test") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "colour") (r "^0.7.0") (d #t) (k 0)))) (h "1rlkwd51riff4zkr014566s1kvy9amwdjcvhfzgz621fjd6d1waa")))

(define-public crate-speed_test-0.1.7 (c (n "speed_test") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "colour") (r "^0.7.0") (d #t) (k 0)))) (h "0fz4m07f8l9sh3aq2fd4rz3rx8j2rmp59v81hnczly35myhbjsj0")))

(define-public crate-speed_test-0.1.8 (c (n "speed_test") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "colour") (r "^0.7.0") (d #t) (k 0)))) (h "1g88hxwrzna8w74ii7z3slg2m2iwx368li5asdpcf3w7svc9rc17")))

