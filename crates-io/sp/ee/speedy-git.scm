(define-module (crates-io sp ee speedy-git) #:use-module (crates-io))

(define-public crate-speedy-git-0.1.0 (c (n "speedy-git") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05y8yf2v9is35v9djdflknqari8d1pa4wvm8ya23054lqg1kyds4") (y #t)))

