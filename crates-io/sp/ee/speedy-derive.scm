(define-module (crates-io sp ee speedy-derive) #:use-module (crates-io))

(define-public crate-speedy-derive-0.1.0 (c (n "speedy-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "117cnb7cfcyh5zv0zm3i6h9d88i5mhsx2jajd3smy6gkxswxnpna")))

(define-public crate-speedy-derive-0.3.0 (c (n "speedy-derive") (v "0.3.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "02dh42f3sqy3ryvzn4wlr7zll6qdmhmbxljp855v7p0xvwr30fw6")))

(define-public crate-speedy-derive-0.3.1 (c (n "speedy-derive") (v "0.3.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1278pxq3z2nayfk6iyjydl9fj963v5m28zfbydy9kp49kx8jbyy5")))

(define-public crate-speedy-derive-0.3.2 (c (n "speedy-derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1nfi3599hkl4n3hhx1clr94l0y1p5d345dzlixlgnpv2g6vc8gsi")))

(define-public crate-speedy-derive-0.3.3 (c (n "speedy-derive") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0jvv559nv4jkfvx92a7j2m7dc7rl4188rdnawvc9m58zgwx5ggbw") (y #t)))

(define-public crate-speedy-derive-0.3.4 (c (n "speedy-derive") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1rnjdhgd3klk6s5hlpryf5a9jjlzmaq269dvrk7gkf8r1i718pyy")))

(define-public crate-speedy-derive-0.3.5 (c (n "speedy-derive") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0kk70pgn5dx496m9jcgsvpi15k9q2xc8j3kh5k7rc3m72jj5hx00") (y #t)))

(define-public crate-speedy-derive-0.3.6 (c (n "speedy-derive") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0pbmw2n0y4a528cr20g4d8iyz1lsl85xslk2ckb8vxg866pjmdsb")))

(define-public crate-speedy-derive-0.5.0 (c (n "speedy-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1ds08q2cxzqc9c0phbh9f2fsk09lh4kqgidr5vpaixq75p6j01zy")))

(define-public crate-speedy-derive-0.6.0 (c (n "speedy-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1fc8fdwbin5zgvycfy6bxlvs2hlygp06l8rvrk46kx7wwlcyf3h6")))

(define-public crate-speedy-derive-0.7.0 (c (n "speedy-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0wc8hmh233zrxhbpjkp2jk2gw9blv607j690l87ip73cwpkyz5qr")))

(define-public crate-speedy-derive-0.7.1 (c (n "speedy-derive") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0h818wb48h1sskd435d1gq9xz520rlnsqqnb7w7a0hgj5igj0nf3")))

(define-public crate-speedy-derive-0.8.0 (c (n "speedy-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1b992il2rdr0xxnda7qlycmwpa7s07kgwwkdzglw02k42szqcqca")))

(define-public crate-speedy-derive-0.8.3 (c (n "speedy-derive") (v "0.8.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "02mg8qjhcx0kmalb095g3za0z0fw6sy43b0vavk9vc542ar6rm80")))

(define-public crate-speedy-derive-0.8.4 (c (n "speedy-derive") (v "0.8.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1r4j6yazxwmgcj3005n6fmkjgiq589xw5w57npk0s1myvalrdznv")))

(define-public crate-speedy-derive-0.8.5 (c (n "speedy-derive") (v "0.8.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1xx4v0h2i6ncnvi7v5y5l44xh12v4pjfkakahk6f27c0c084lazb")))

(define-public crate-speedy-derive-0.8.6 (c (n "speedy-derive") (v "0.8.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1ixn6kx8axs72cs3sshg63h4qxmgq0646yppa18n4y37rdk5hfbx")))

(define-public crate-speedy-derive-0.8.7 (c (n "speedy-derive") (v "0.8.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1b65pb088xwn8c5mgmw8izkaqcp0nhb87a2zsvgw74kb4yjjr3v5")))

