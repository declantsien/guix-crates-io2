(define-module (crates-io sp ee speedracer) #:use-module (crates-io))

(define-public crate-speedracer-0.1.0 (c (n "speedracer") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0jngp1288yk3y2w04sl1wyk6f500pznlj94x9314pdnvjnadddl0") (y #t)))

(define-public crate-speedracer-0.1.1 (c (n "speedracer") (v "0.1.1") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("rt" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("rt" "time" "macros"))) (d #t) (k 2)))) (h "10i3dkvfk6qy5jadff552zvg9hcrcn6ihd87pnpr5v49j0iylavl")))

(define-public crate-speedracer-0.1.2 (c (n "speedracer") (v "0.1.2") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("rt" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("rt" "time" "macros"))) (d #t) (k 2)))) (h "1mdkrfwwwdy7i6nz1wbpdmbqwkbsjnw8gz91kpk1acnhv9fh96y7")))

