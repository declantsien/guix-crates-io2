(define-module (crates-io sp ee speech-synthesis) #:use-module (crates-io))

(define-public crate-speech-synthesis-0.0.1 (c (n "speech-synthesis") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (k 0)) (d (n "ssml") (r "=0.0.4") (k 0)))) (h "0ramc865938m598wf5k6qk48qa758r2gq3zdbcmmb7cvafg3vj2g") (r "1.56")))

(define-public crate-speech-synthesis-0.0.2 (c (n "speech-synthesis") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (k 0)) (d (n "ssml") (r "=0.0.4") (k 0)))) (h "0895ppar4ld4a4g2b51xl3yhn9njrz5ksk0w3gh1xbwvpbw8q5fq") (r "1.56")))

(define-public crate-speech-synthesis-0.0.3 (c (n "speech-synthesis") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (k 0)) (d (n "ssml") (r "=0.0.5") (k 0)))) (h "1vbs3dwfdjv43q4z3pq3frhk0flr2izk3lyng7b0ms93r6plyc3i") (r "1.56")))

(define-public crate-speech-synthesis-0.0.4 (c (n "speech-synthesis") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (k 0)) (d (n "ssml") (r "=0.0.5") (k 0)))) (h "01llrjnzajv9amb8pk0wrvddq5cchpxbf98zcbbjxlfjrcyb20gq") (r "1.56")))

(define-public crate-speech-synthesis-0.1.0 (c (n "speech-synthesis") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (k 0)) (d (n "ssml") (r "^0.1") (k 0)))) (h "1jry49gf0ylq5xn3d4768h0r8h2zkvwwvkyz0kg88m23dybb4c73") (r "1.56")))

(define-public crate-speech-synthesis-0.2.0 (c (n "speech-synthesis") (v "0.2.0") (d (list (d (n "futures-core") (r "^0.3") (k 0)) (d (n "ssml") (r "^0.1") (k 0)))) (h "1cx8n055dydb0d8gmipsbp6i8iirbnjgbc02h7z570q7p6ndkkw1") (r "1.75")))

