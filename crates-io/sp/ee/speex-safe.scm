(define-module (crates-io sp ee speex-safe) #:use-module (crates-io))

(define-public crate-speex-safe-0.1.0 (c (n "speex-safe") (v "0.1.0") (d (list (d (n "speex-sys") (r "^0.4.0") (d #t) (k 0)))) (h "0csnjhkm24z809hixpiyaylnplw2pmajj392ym45rmgzbw92h7vq") (y #t)))

(define-public crate-speex-safe-0.2.0 (c (n "speex-safe") (v "0.2.0") (d (list (d (n "speex-sys") (r "^0.4.0") (d #t) (k 0)))) (h "0dcv4wlg60xiqjikbxmd052xklqmkdpqdjqlfazzrn35yjqgnw29")))

(define-public crate-speex-safe-0.3.0 (c (n "speex-safe") (v "0.3.0") (d (list (d (n "speex-sys") (r "^0.4.0") (d #t) (k 0)))) (h "0rs5k8yaxf9cg0wkgx7asdvahfx73k2n1983d9wb42b76gwskz09")))

(define-public crate-speex-safe-0.3.1 (c (n "speex-safe") (v "0.3.1") (d (list (d (n "speex-sys") (r "^0.4.0") (d #t) (k 0)))) (h "0i3vjb4gh284hn2kqx7mj2x7qz8vbkd8ncdsp92xdxnfhxibcdps")))

(define-public crate-speex-safe-0.4.0 (c (n "speex-safe") (v "0.4.0") (d (list (d (n "speex-sys") (r "^0.4.0") (d #t) (k 0)))) (h "1m3cdsnaw4f2xjnf893ilxz75bf2nxv8r7k60dblmr3ri6z8jvhx")))

(define-public crate-speex-safe-0.5.0 (c (n "speex-safe") (v "0.5.0") (d (list (d (n "speex-sys") (r "^0.4.0") (d #t) (k 0)))) (h "1isak3p1zckxg8s06ffhc891i2iy982wsj26pz1dvbpd07wg6ldx")))

(define-public crate-speex-safe-0.6.0 (c (n "speex-safe") (v "0.6.0") (d (list (d (n "speex-sys") (r "^0.4.0") (d #t) (k 0)))) (h "14wdjyfphm03n9ywqxbh7klp17xf3k28f3lfvplh3j6sdrpslzdi")))

