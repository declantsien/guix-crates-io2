(define-module (crates-io sp ee speech-dispatcher-sys) #:use-module (crates-io))

(define-public crate-speech-dispatcher-sys-0.1.0 (c (n "speech-dispatcher-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0n9xs3777b19x5lzq74mmmlbxa8v45b92cmcic4g24aw29j09ybl")))

(define-public crate-speech-dispatcher-sys-0.2.0 (c (n "speech-dispatcher-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "19vhji0cphyrkn6af06cv9yl0a6j12sndxpanzz3j1hn7j0xsk3i")))

(define-public crate-speech-dispatcher-sys-0.3.0 (c (n "speech-dispatcher-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1rp31v8vzmn4jv7x8dgp0i3995mbhjz8p4dx2j4arl9n6dvy55g2")))

(define-public crate-speech-dispatcher-sys-0.4.0 (c (n "speech-dispatcher-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "11njnij576sjlxqr8xvb5s2i444qdhrvib5wy6hgjf14ymnmawid")))

(define-public crate-speech-dispatcher-sys-0.4.1 (c (n "speech-dispatcher-sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1k93nx6s3n3mqrfxf3aqk954ycsy54rxgplbfacyicy793f2bi03")))

(define-public crate-speech-dispatcher-sys-0.4.2 (c (n "speech-dispatcher-sys") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0a07sl2mkwbiildpnfljav031s9gb1d4clhagq9b4fmw11g5bgds")))

(define-public crate-speech-dispatcher-sys-0.5.0 (c (n "speech-dispatcher-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "192yvk22ikxmk59x40vhxny281w06jgmsxa6d8xjkhgrbdgjjrbg")))

(define-public crate-speech-dispatcher-sys-0.5.1 (c (n "speech-dispatcher-sys") (v "0.5.1") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1llfgd3322pcnj751mgb4ngnsrnxnqisbzcwm82f73r2qhyjvckx")))

(define-public crate-speech-dispatcher-sys-0.5.2 (c (n "speech-dispatcher-sys") (v "0.5.2") (d (list (d (n "bindgen") (r ">=0.54") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1d9d5d5h6fx8mgwmsi66ls1mdh582z3rf0ngxl8kv7g3wwgsjqmn")))

(define-public crate-speech-dispatcher-sys-0.6.0 (c (n "speech-dispatcher-sys") (v "0.6.0") (d (list (d (n "bindgen") (r ">=0.54") (d #t) (k 1)))) (h "1f8rkpk5g0yz1fhbhh8g2jp1284pkcxy9rpxdlfsfnrbb1nbi7hj")))

(define-public crate-speech-dispatcher-sys-0.7.0 (c (n "speech-dispatcher-sys") (v "0.7.0") (d (list (d (n "bindgen") (r ">=0.54") (d #t) (k 1)))) (h "139q20m6fksq79qzwmca9p4lrpry5ysl0fw1y49vpx5iyb6qlgkc")))

