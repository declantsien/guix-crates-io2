(define-module (crates-io sp ee speech-dispatcher) #:use-module (crates-io))

(define-public crate-speech-dispatcher-0.1.0 (c (n "speech-dispatcher") (v "0.1.0") (d (list (d (n "speech-dispatcher-sys") (r "^0.2") (d #t) (k 0)))) (h "076xpaysfgvw0rzyn8sg2ldgab68pjp2ipc37lwhib4x0q263xfv")))

(define-public crate-speech-dispatcher-0.2.0 (c (n "speech-dispatcher") (v "0.2.0") (d (list (d (n "speech-dispatcher-sys") (r "^0.2") (d #t) (k 0)))) (h "1qlnhw27nc7y0zj762laxcmr9c50rm5416r8633hzx1qmckf6blj")))

(define-public crate-speech-dispatcher-0.3.0 (c (n "speech-dispatcher") (v "0.3.0") (d (list (d (n "speech-dispatcher-sys") (r "^0.3") (d #t) (k 0)))) (h "03ir4n1mkllza2syvxlv344imzhv880a4sgq9zqz1yv2kj19rgcn")))

(define-public crate-speech-dispatcher-0.3.1 (c (n "speech-dispatcher") (v "0.3.1") (d (list (d (n "speech-dispatcher-sys") (r "^0.3") (d #t) (k 0)))) (h "0xs1xmj11qckirj1ci8clm4d4bvnvz4hf1455fqdb9ffpcwyy5zb")))

(define-public crate-speech-dispatcher-0.3.2 (c (n "speech-dispatcher") (v "0.3.2") (d (list (d (n "speech-dispatcher-sys") (r "^0.3") (d #t) (k 0)))) (h "1808wblpfmcgl9dkajs4wyzyjkl7nia73pjgnbyg63ygwvhl7yv2")))

(define-public crate-speech-dispatcher-0.4.1 (c (n "speech-dispatcher") (v "0.4.1") (d (list (d (n "speech-dispatcher-sys") (r "^0.4.0") (d #t) (k 0)))) (h "0njx19gzj2zbsfdz5nn140dh2m60fkk7xl1y4dgq47fpqfflrz50")))

(define-public crate-speech-dispatcher-0.4.2 (c (n "speech-dispatcher") (v "0.4.2") (d (list (d (n "speech-dispatcher-sys") (r "^0.4") (d #t) (k 0)))) (h "03znwhgkb5fivgn5y8rqlsz7gidkc0qibsb7c8ifw8ydwasj2j5h")))

(define-public crate-speech-dispatcher-0.4.3 (c (n "speech-dispatcher") (v "0.4.3") (d (list (d (n "speech-dispatcher-sys") (r "^0.4") (d #t) (k 0)))) (h "18264w77c3lin1jw9hccs9kh5mr93r4jrh2ygkfy4jy416yilwss")))

(define-public crate-speech-dispatcher-0.5.0 (c (n "speech-dispatcher") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "speech-dispatcher-sys") (r "^0.5") (d #t) (k 0)))) (h "1i4vbcj21g8ivf9705xiym6025y8pqyxx734dv9m2wd0hzdvizlr")))

(define-public crate-speech-dispatcher-0.6.0 (c (n "speech-dispatcher") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "speech-dispatcher-sys") (r "^0.5") (d #t) (k 0)))) (h "0gn02m2hryrqhhxzc0v1wmvvns33pw27g4plsfajnnppzmjvjs7n")))

(define-public crate-speech-dispatcher-0.7.0 (c (n "speech-dispatcher") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "speech-dispatcher-sys") (r "^0.5") (d #t) (k 0)))) (h "04j42cpnl2cylzg9wxj4cm7h23hkf03w70ma3v25q71sq39glvh7")))

(define-public crate-speech-dispatcher-0.8.0 (c (n "speech-dispatcher") (v "0.8.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "speech-dispatcher-sys") (r "^0.5") (d #t) (k 0)))) (h "1halik3wzwhnd3f8c9074s3vqvsvs24c4n7v5zn756v6rfsnk7am")))

(define-public crate-speech-dispatcher-0.9.0 (c (n "speech-dispatcher") (v "0.9.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "speech-dispatcher-sys") (r "^0.5") (d #t) (k 0)))) (h "0mb9v6xi2aidl4srlg68jzbfma7qi5wn61l5cprgwqanc594as4v")))

(define-public crate-speech-dispatcher-0.10.0 (c (n "speech-dispatcher") (v "0.10.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "speech-dispatcher-sys") (r "^0.5") (d #t) (k 0)))) (h "0l10dacvcfcny8df240srr2v7anggs7nndbms60in3cq0ih0wkn0")))

(define-public crate-speech-dispatcher-0.11.0 (c (n "speech-dispatcher") (v "0.11.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "speech-dispatcher-sys") (r "^0.6") (d #t) (k 0)))) (h "00v8nbhsf8gp26km0h9rdwybf952fyin838kpcywi8p2xj7kdjbi")))

(define-public crate-speech-dispatcher-0.12.0 (c (n "speech-dispatcher") (v "0.12.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "speech-dispatcher-sys") (r "^0.7") (d #t) (k 0)))) (h "0lg196hmyf9mw77phs9ss5bm3z00ankmng62pkcrbsvnw5zm3azv")))

(define-public crate-speech-dispatcher-0.13.0 (c (n "speech-dispatcher") (v "0.13.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "speech-dispatcher-sys") (r "^0.7") (d #t) (k 0)))) (h "1bq2jynlcx099hpqv3zs1ccv6ijdbbjjfppj3ns326hafmb4y6h1") (f (quote (("default" "0_10") ("0_10"))))))

(define-public crate-speech-dispatcher-0.13.1 (c (n "speech-dispatcher") (v "0.13.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "speech-dispatcher-sys") (r "^0.7") (d #t) (k 0)))) (h "0ihky2998ic9rz2nkbdpsanyjv14wgafpyhdyi4l1l24qm2gark6") (f (quote (("default" "0_10") ("0_10"))))))

(define-public crate-speech-dispatcher-0.14.0 (c (n "speech-dispatcher") (v "0.14.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "speech-dispatcher-sys") (r "^0.7") (d #t) (k 0)))) (h "0h3gslya50naf13l4bnzwk18663m31h5nn5h61gpwjnl6cba0krr") (f (quote (("default" "0_10_2") ("0_10_2" "0_10") ("0_10"))))))

(define-public crate-speech-dispatcher-0.15.0 (c (n "speech-dispatcher") (v "0.15.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "speech-dispatcher-sys") (r "^0.7") (d #t) (k 0)))) (h "0ay8fbxv4j9j9p63xxdi5bz96aaasrlzhmhxm2qh8phpddl1p29f") (f (quote (("default" "0_11") ("0_11" "0_10") ("0_10"))))))

(define-public crate-speech-dispatcher-0.15.1 (c (n "speech-dispatcher") (v "0.15.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2.125") (d #t) (k 0)) (d (n "speech-dispatcher-sys") (r "^0.7") (d #t) (k 0)))) (h "07j4gv3bkrz2acw78325ngjgmbbj2xdvi310zp64nirmn0h2gmn3") (f (quote (("default" "0_11") ("0_11" "0_10") ("0_10"))))))

(define-public crate-speech-dispatcher-0.16.0 (c (n "speech-dispatcher") (v "0.16.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2.125") (d #t) (k 0)) (d (n "speech-dispatcher-sys") (r "^0.7") (d #t) (k 0)))) (h "0wi9xdchgc3n4frhxvngai4ag5pq7hh7vbc4fyhav9ab8wyda9sp") (f (quote (("default" "0_11") ("0_9") ("0_11" "0_10") ("0_10"))))))

