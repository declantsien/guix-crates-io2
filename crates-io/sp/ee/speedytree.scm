(define-module (crates-io sp ee speedytree) #:use-module (crates-io))

(define-public crate-speedytree-0.1.0 (c (n "speedytree") (v "0.1.0") (d (list (d (n "bit-set") (r "^0.5.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dtoa") (r "^1.0.9") (d #t) (k 0)) (d (n "fixedbitset") (r "^0.4.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "rb_tree") (r "^0.5.0") (d #t) (k 0)))) (h "0rhnq60s7chsbwqw2893krp34p2lvklr6p895h01pkj54h2j4ibz")))

