(define-module (crates-io sp ee speexdsp) #:use-module (crates-io))

(define-public crate-speexdsp-0.1.0 (c (n "speexdsp") (v "0.1.0") (d (list (d (n "speexdsp-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1p3nfp8xanljmk389wj25g7vkvf5nr0yvvvjs9bbf81ndlrydc2q") (f (quote (("build" "speexdsp-sys/build"))))))

(define-public crate-speexdsp-0.1.1 (c (n "speexdsp") (v "0.1.1") (d (list (d (n "speexdsp-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1nwb84wflrvmcifa6ki0hydp1adm1rxiqcvsdbdy1cgzx82ckb2d") (f (quote (("build" "speexdsp-sys/build"))))))

(define-public crate-speexdsp-0.1.2 (c (n "speexdsp") (v "0.1.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "byteorder") (r "^1.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "interpolate_name") (r "^0.2.2") (d #t) (k 2)) (d (n "speexdsp-resampler") (r "^0.2") (d #t) (k 0)) (d (n "speexdsp-sys") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "10ll4i00g02fxv10ql8i81a8z5313fs7hj15p070lhb5k0bb3xky") (f (quote (("sys" "speexdsp-sys") ("sse3" "speexdsp-resampler/sse3") ("dynnative" "speexdsp-resampler/dynnative") ("avx" "speexdsp-resampler/avx"))))))

