(define-module (crates-io sp ee speedometer) #:use-module (crates-io))

(define-public crate-speedometer-0.1.0 (c (n "speedometer") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "07kdnjws5bg3nabzsnaykg2b4gcx8kvd50f8s53badh3p4fqmj9l")))

(define-public crate-speedometer-0.2.0 (c (n "speedometer") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "16464qivzcd2yv5pmrhpyyijbvaxdmi0s2zjyxy3zbsn9nry6hpw")))

(define-public crate-speedometer-0.2.1 (c (n "speedometer") (v "0.2.1") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "11kg2h11sx58h4qh8dzlz8rq7svn8hrih65zh5ahdpwy8xch7wqv")))

(define-public crate-speedometer-0.2.2 (c (n "speedometer") (v "0.2.2") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)))) (h "07jwr529djj12wgnrgcpypq93jv0nfsar445mx5v88gsj9h77297")))

