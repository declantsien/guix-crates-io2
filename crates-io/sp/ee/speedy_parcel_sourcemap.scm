(define-module (crates-io sp ee speedy_parcel_sourcemap) #:use-module (crates-io))

(define-public crate-speedy_parcel_sourcemap-2.0.2 (c (n "speedy_parcel_sourcemap") (v "2.0.2") (d (list (d (n "js-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "napi") (r "^2") (o #t) (k 0)) (d (n "napi-derive") (r "^2") (k 0)) (d (n "rkyv") (r "^0.6.7") (d #t) (k 0)) (d (n "vlq") (r "^0.5.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0bcldy8vp06xrbjcy13z2sa7fd6db2sgk4syh2q8qzk61f6baiw5") (f (quote (("wasm" "js-sys" "wasm-bindgen" "napi-derive/noop") ("skip_napi" "napi-derive/noop") ("native" "napi") ("default" "native"))))))

