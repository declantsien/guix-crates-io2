(define-module (crates-io sp ee speedrun) #:use-module (crates-io))

(define-public crate-speedrun-0.0.1 (c (n "speedrun") (v "0.0.1") (h "1b9x03j0iaaxas0cs3jkky5wskrr368jjsy8n2z8alyk9q475zrz") (y #t)))

(define-public crate-speedrun-0.0.0-- (c (n "speedrun") (v "0.0.0--") (h "1dg7rq1q5l6g8wfx887v2zsrck28ldil5fz2cn0znhn3ril8dxc7") (y #t)))

