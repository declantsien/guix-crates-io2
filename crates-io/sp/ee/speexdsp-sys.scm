(define-module (crates-io sp ee speexdsp-sys) #:use-module (crates-io))

(define-public crate-speexdsp-sys-0.1.0 (c (n "speexdsp-sys") (v "0.1.0") (d (list (d (n "autotools") (r "^0.1") (d #t) (k 1)) (d (n "bindgen") (r "^0.33") (d #t) (k 1)) (d (n "metadeps") (r "^1.1") (d #t) (k 1)))) (h "1yk0gs3jpx9wnvqqvsi86wa2nvgia4cdg6kznpxnihfm8w4dnv86") (f (quote (("build"))))))

(define-public crate-speexdsp-sys-0.1.1 (c (n "speexdsp-sys") (v "0.1.1") (d (list (d (n "autotools") (r "^0.1") (d #t) (k 1)) (d (n "bindgen") (r "^0.33") (d #t) (k 1)) (d (n "metadeps") (r "^1.1") (d #t) (k 1)))) (h "0qmkin32zs1pryjkp9lvby0iz9il8q7qj7igljzc7dpmzl13z45m") (f (quote (("build"))))))

(define-public crate-speexdsp-sys-0.1.2 (c (n "speexdsp-sys") (v "0.1.2") (d (list (d (n "autotools") (r "^0.1") (d #t) (k 1)) (d (n "bindgen") (r "^0.37") (d #t) (k 1)) (d (n "metadeps") (r "^1.1") (d #t) (k 1)))) (h "1dxd1advcr6n17lxyb45n1xl5sfy1n2887iwvni32h539r70zcks") (f (quote (("build"))))))

