(define-module (crates-io sp ee speedruns_utils) #:use-module (crates-io))

(define-public crate-speedruns_utils-0.21.6-dev.15 (c (n "speedruns_utils") (v "0.21.6-dev.15") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "no-panic") (r "^0.1.12") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "1s1l7zhfjwdwxkcvmm1w1w6k4jvda883cn4zmk1fdz33yvi2817i") (y #t)))

(define-public crate-speedruns_utils-0.21.6-dev.16 (c (n "speedruns_utils") (v "0.21.6-dev.16") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "no-panic") (r "^0.1.12") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "1g8h70wrnp8dfmrzhddll22xwbv87kxqhg3v6j6ba6z8wga742a0") (y #t)))

(define-public crate-speedruns_utils-0.21.6-dev.17 (c (n "speedruns_utils") (v "0.21.6-dev.17") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "no-panic") (r "^0.1.12") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "0i10spb4dj7fwcb83x48b55z2pszwrwrjyfm4az7hxcxbqxa1pbw") (y #t)))

(define-public crate-speedruns_utils-0.21.6-dev.18 (c (n "speedruns_utils") (v "0.21.6-dev.18") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "no-panic") (r "^0.1.12") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "1k2d5g3a52l3d87skvc1bir98h49cifvvc7hzv0c5lsm8f935agm") (y #t)))

(define-public crate-speedruns_utils-0.21.6-dev.19 (c (n "speedruns_utils") (v "0.21.6-dev.19") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "no-panic") (r "^0.1.12") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "1xrf7h15b65i0ny30ans12il8lq3mjsj4s1g1svmjvy0m72mfhpd") (y #t)))

(define-public crate-speedruns_utils-0.21.6-dev.20 (c (n "speedruns_utils") (v "0.21.6-dev.20") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "no-panic") (r "^0.1.12") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "119lsq92q42w54gw3fib7hhzwap25wmzfcmz0gbgjz74qklpw6hd") (y #t)))

(define-public crate-speedruns_utils-0.21.6-dev.21 (c (n "speedruns_utils") (v "0.21.6-dev.21") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "no-panic") (r "^0.1.12") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "0fzlhj5h7f71lkzi9h3v4iq9r2kfp8rpc7h8ac58947g5p395a3i") (y #t)))

(define-public crate-speedruns_utils-0.21.6-dev.22 (c (n "speedruns_utils") (v "0.21.6-dev.22") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "no-panic") (r "^0.1.12") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "07wc1v900s9fp9c72q2mg1gz9a528q6x1gfj014lyksj0iybssw0") (y #t)))

(define-public crate-speedruns_utils-0.21.6-dev.23 (c (n "speedruns_utils") (v "0.21.6-dev.23") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "no-panic") (r "^0.1.12") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "1lv852vai8p8mbhm40xnwgg66c3pr5zmas8d4jwah7n1cv8vgjnv") (y #t)))

(define-public crate-speedruns_utils-0.21.6-dev.24 (c (n "speedruns_utils") (v "0.21.6-dev.24") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "no-panic") (r "^0.1.12") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "00gsm2lwrcbk7c8gnnsyva840f6b7qvmzhkvkyfqzd2spjrg4zzk") (y #t)))

(define-public crate-speedruns_utils-0.21.6-dev.25 (c (n "speedruns_utils") (v "0.21.6-dev.25") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "no-panic") (r "^0.1.12") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "04bkn1cxv6w8v46vlgl4s49sr72944211hffrxyv8vpzvl6xs8wy") (y #t)))

(define-public crate-speedruns_utils-0.21.6-dev.26 (c (n "speedruns_utils") (v "0.21.6-dev.26") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "no-panic") (r "^0.1.12") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "0x7lmgnwwidvq93nwmd5yiwkp2q8accfa2cdxfsi1fjm7qwjf0ac") (y #t)))

(define-public crate-speedruns_utils-0.21.6-dev.27 (c (n "speedruns_utils") (v "0.21.6-dev.27") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "no-panic") (r "^0.1.12") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "02db0y79xak7j25755xp33nras23822ci97cnh98lrrfjyaksfvv") (y #t)))

(define-public crate-speedruns_utils-0.21.6-dev.28 (c (n "speedruns_utils") (v "0.21.6-dev.28") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "no-panic") (r "^0.1.12") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "0ihx25bqycl7zss0k1jpjzmz96nfx2q6ph136kjv4mq718ddahw8") (y #t)))

(define-public crate-speedruns_utils-0.21.6-dev.29 (c (n "speedruns_utils") (v "0.21.6-dev.29") (d (list (d (n "derive_more") (r "^0.99.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "no-panic") (r "^0.1.12") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "0vp1b3cwn303qr4m59m0f01v6f1dv2gmgfawyv221r7rfi2d3i7v") (y #t)))

(define-public crate-speedruns_utils-0.21.6-dev.30 (c (n "speedruns_utils") (v "0.21.6-dev.30") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "no-panic") (r "^0.1.12") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "06i05i0hlkhfsj3z4b1vk6n692rjgiq07ygih1q77w1y9asw4wdk") (y #t)))

(define-public crate-speedruns_utils-0.21.6-dev.31 (c (n "speedruns_utils") (v "0.21.6-dev.31") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "no-panic") (r "^0.1.12") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "103b0jyadjg5kwaqpngl8dnl92dkl2src7vz596djxsxjzq3jcp2") (y #t)))

(define-public crate-speedruns_utils-0.21.6-dev.32 (c (n "speedruns_utils") (v "0.21.6-dev.32") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "no-panic") (r "^0.1.12") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "0r27sm5m54s84z0a51r44c9ln94bjrdd4ryvnviwxdr614s3x8mc") (y #t)))

(define-public crate-speedruns_utils-0.21.6-dev.33 (c (n "speedruns_utils") (v "0.21.6-dev.33") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "no-panic") (r "^0.1.12") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "0g6w2sxx0qbq8z8i0a12mjqwl3hiikvh0z66gr80wlf196d1pbzj") (y #t)))

(define-public crate-speedruns_utils-0.21.6-dev.35 (c (n "speedruns_utils") (v "0.21.6-dev.35") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "no-panic") (r "^0.1.12") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "1mnhn7sbx4b9ppp1nfnxj1pvlyzpdpyazfxix7acz61ixk6fgpyg") (y #t)))

(define-public crate-speedruns_utils-0.21.6-dev.36 (c (n "speedruns_utils") (v "0.21.6-dev.36") (d (list (d (n "derive_more") (r "^0.99.3") (d #t) (k 0)) (d (n "err-derive") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "no-panic") (r "^0.1.12") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)))) (h "1nysi1zz1qzfz8qxsalfl26ar21rdhi4yzdpaa39ibwyy4i98wiy") (y #t)))

(define-public crate-speedruns_utils-0.21.7-speedruns-utils (c (n "speedruns_utils") (v "0.21.7-speedruns-utils") (h "1nqr0pg4q8ii40zxy4v9qaanva5w3mgzi855phij8pa0jjxz4r20") (y #t)))

(define-public crate-speedruns_utils-0.0.0-- (c (n "speedruns_utils") (v "0.0.0--") (h "0kda7vy4jjix9q122qdycmldv2d2xkr0xycjhdkl5fdywjaw06rj") (y #t)))

