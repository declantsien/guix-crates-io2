(define-module (crates-io sp ee speedy-spritesheets) #:use-module (crates-io))

(define-public crate-speedy-spritesheets-0.1.0 (c (n "speedy-spritesheets") (v "0.1.0") (d (list (d (n "sdl2") (r "^0.36.0") (d #t) (k 0)))) (h "1m6z5x125p60ikrpyf6nxakiarg2iisqqw1zq0kfjwbpw40nzk47")))

(define-public crate-speedy-spritesheets-0.1.1 (c (n "speedy-spritesheets") (v "0.1.1") (d (list (d (n "sdl2") (r "^0.36.0") (d #t) (k 0)))) (h "158ky31yfvs65fpjhcq8gwk4v0bmyws5n22vzb9nsdf49cf48m0h")))

(define-public crate-speedy-spritesheets-0.2.0 (c (n "speedy-spritesheets") (v "0.2.0") (d (list (d (n "eframe") (r "^0.24.1") (f (quote ("__screenshot"))) (d #t) (k 0)) (d (n "egui_extras") (r "^0.24.2") (f (quote ("image"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (f (quote ("auto-color" "humantime"))) (k 0)) (d (n "png") (r "^0.17.10") (d #t) (k 0)) (d (n "rfd") (r "^0.12.1") (d #t) (k 0)))) (h "1948kj6lzc3bgpbcllinjiswl17dwlbwaw3g3jrybqgcflr7wrz6")))

(define-public crate-speedy-spritesheets-0.2.2 (c (n "speedy-spritesheets") (v "0.2.2") (d (list (d (n "eframe") (r "^0.24.1") (f (quote ("__screenshot"))) (d #t) (k 0)) (d (n "egui_extras") (r "^0.24.2") (f (quote ("all_loaders"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (f (quote ("auto-color" "humantime"))) (k 0)) (d (n "image") (r "^0.24.7") (f (quote ("png"))) (k 0)) (d (n "rfd") (r "^0.12.1") (d #t) (k 0)))) (h "0xl9ynrywah0bmagzvyss79rafxcbn1v1slmq8hf0dwrmbzpwg5x")))

(define-public crate-speedy-spritesheets-0.3.0 (c (n "speedy-spritesheets") (v "0.3.0") (d (list (d (n "eframe") (r "^0.24.1") (f (quote ("__screenshot"))) (d #t) (k 0)) (d (n "egui_extras") (r "^0.24.2") (f (quote ("all_loaders"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (f (quote ("auto-color" "humantime"))) (k 0)) (d (n "image") (r "^0.24.7") (f (quote ("png"))) (k 0)) (d (n "rfd") (r "^0.12.1") (d #t) (k 0)))) (h "198mzfhmk2qzcdc52s0sxva8zj8q45ck8pqnpwpr2w9v5b7pc8kh")))

(define-public crate-speedy-spritesheets-0.3.1 (c (n "speedy-spritesheets") (v "0.3.1") (d (list (d (n "eframe") (r "^0.25.0") (f (quote ("__screenshot"))) (d #t) (k 0)) (d (n "egui_extras") (r "^0.25.0") (f (quote ("all_loaders"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.1") (f (quote ("auto-color" "humantime"))) (k 0)) (d (n "image") (r "^0.24.8") (f (quote ("jpeg" "png"))) (k 0)) (d (n "pkg-version") (r "^1.0.0") (d #t) (k 0)) (d (n "rfd") (r "^0.13.0") (d #t) (k 0)))) (h "1yva5bjq0xn49vr7n79kvp8l9hq361ky3h5wln4dlar91x03aink")))

