(define-module (crates-io sp ee speedrunapi) #:use-module (crates-io))

(define-public crate-speedrunapi-0.1.0 (c (n "speedrunapi") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1cnp3zyf74854yskkh4dkkm08vw362nlpxb76r1jf26ip25fw7q3")))

(define-public crate-speedrunapi-0.1.1 (c (n "speedrunapi") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.64") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vbsx91pwhnbvlwgdbz5wwp2q1y1gsg8i7801vr3niirciab1v7q")))

(define-public crate-speedrunapi-0.1.2 (c (n "speedrunapi") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.64") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1sfzxbyl2smdv3wacw1iz6ma8z1pyzrz0glggxvk2zcnkcsvzp4v")))

(define-public crate-speedrunapi-0.2.0 (c (n "speedrunapi") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.64") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0c29xhzyy9vli7cf6yrv03g48fxv2g28hxqbkjdlr85ljsps4fak")))

(define-public crate-speedrunapi-0.3.0 (c (n "speedrunapi") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.64") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14v7f3kn3gs35xan64x27ilg91l4z77z0jnfzy0ksgmgiqbar4qn") (f (quote (("translations") ("default" "translations"))))))

