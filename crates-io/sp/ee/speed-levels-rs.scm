(define-module (crates-io sp ee speed-levels-rs) #:use-module (crates-io))

(define-public crate-speed-levels-rs-0.1.0 (c (n "speed-levels-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "spreadsheet-ods") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0g16jjalc0hzayh54rvpl3mr2m4kkncww9nmkm43akcx7s90khvk")))

(define-public crate-speed-levels-rs-0.2.0 (c (n "speed-levels-rs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "spreadsheet-ods") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0xg67iaq3fjp2jpcw2jvg75v5pmaql2h88v0q7ni15pbjdxczn31")))

(define-public crate-speed-levels-rs-0.2.1 (c (n "speed-levels-rs") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "spreadsheet-ods") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0fg5yflylpgj1nhj3h2mm4zcv65dw3144yzj12hf0jzvk2k3pnaa")))

(define-public crate-speed-levels-rs-0.2.2 (c (n "speed-levels-rs") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "spreadsheet-ods") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0dc6sf0mgxvll6lrj8f3805glgnz482albsfi77rd4p4xmh71422")))

(define-public crate-speed-levels-rs-0.2.3 (c (n "speed-levels-rs") (v "0.2.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "spreadsheet-ods") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1ns3r3rwywqik5y0z9lv2s1bbapcrsv3ly0zxhl3r2jvars9xyj5")))

(define-public crate-speed-levels-rs-0.2.4 (c (n "speed-levels-rs") (v "0.2.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "platform-info") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "spreadsheet-ods") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1diji8pw0xqdgdw4qsyfhdlbwyiix5wxbdbanwhy919jxg9jz3r3")))

(define-public crate-speed-levels-rs-0.2.5 (c (n "speed-levels-rs") (v "0.2.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "icu_locid") (r "^1.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "platform-info") (r "^2.0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "spreadsheet-ods") (r "^0.19") (d #t) (k 0)))) (h "0lyzvlimd5aznyw3b50yvy0bpha7qpb2rnvlgyfz1x9mmjaicip4")))

