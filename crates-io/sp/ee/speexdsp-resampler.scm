(define-module (crates-io sp ee speexdsp-resampler) #:use-module (crates-io))

(define-public crate-speexdsp-resampler-0.1.0 (c (n "speexdsp-resampler") (v "0.1.0") (h "1lv3nfqcr7wqcq82yy06hd84j9hxnb722zhxi4gvwpanbh6m8bdp")))

(define-public crate-speexdsp-resampler-0.2.0 (c (n "speexdsp-resampler") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "1a75nd9hwll0rzl5fi7l04zkphglqg86bh1s0vlrs6nr7s12i6ig") (f (quote (("sse3") ("dynnative" "avx") ("avx" "sse3")))) (y #t)))

