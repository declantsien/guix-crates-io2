(define-module (crates-io sp ee speedy_refs) #:use-module (crates-io))

(define-public crate-speedy_refs-0.1.0 (c (n "speedy_refs") (v "0.1.0") (d (list (d (n "num-format") (r "^0.4.4") (d #t) (k 0)) (d (n "thread_runner") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1533yicf8785mx0dyq7r02ngdpq63c6506a1la2y5w9gdmz3lk98")))

(define-public crate-speedy_refs-0.2.0 (c (n "speedy_refs") (v "0.2.0") (d (list (d (n "num-format") (r "^0.4.4") (d #t) (k 0)))) (h "11ga4zw1chglayzwyvzr2fgmyhk3vpnlqiyjw9f2nwhnnlg86d8v")))

(define-public crate-speedy_refs-0.2.1 (c (n "speedy_refs") (v "0.2.1") (d (list (d (n "num-format") (r "^0.4.4") (d #t) (k 0)))) (h "16h6afp15l20c0vnhhd0fwg4bnpijsa6qq3lnz0024jhgd3kldp8")))

(define-public crate-speedy_refs-0.2.2 (c (n "speedy_refs") (v "0.2.2") (d (list (d (n "num-format") (r "^0.4.4") (d #t) (k 0)))) (h "082063kk1i11jvs4xfdvw07hgq7205nn7z8dn5dckqmcv0mrq3dd")))

(define-public crate-speedy_refs-0.2.3 (c (n "speedy_refs") (v "0.2.3") (h "1q28yvbmh5hsicg5flxmn26sriffid9q5y5nfznii3j22ydn88kj")))

(define-public crate-speedy_refs-0.2.4 (c (n "speedy_refs") (v "0.2.4") (h "0b63i8b39ss6vy9is7i37n8llspvaily6f3appz3r98ngc8wws7s")))

(define-public crate-speedy_refs-0.2.5 (c (n "speedy_refs") (v "0.2.5") (h "1dmn0zmy1mbzrcr86vf609f7wvjbnxx54hdjd27kc91akgi48las")))

(define-public crate-speedy_refs-0.2.6 (c (n "speedy_refs") (v "0.2.6") (d (list (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "07jsiqdjr31vn4p6wgixdnd17a127j51agakmzxb4yga9dy5pa29")))

(define-public crate-speedy_refs-0.2.7 (c (n "speedy_refs") (v "0.2.7") (d (list (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "0d6x26f5z84frh7px0k68av9ac32wsp4cx22k0hzg5085xiyjsx7")))

