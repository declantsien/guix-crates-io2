(define-module (crates-io sp -n sp-npos-elections-compact) #:use-module (crates-io))

(define-public crate-sp-npos-elections-compact-2.0.0-rc3 (c (n "sp-npos-elections-compact") (v "2.0.0-rc3") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "14h10q0zznb1vayzkp39g67cd99izw96npnj5mg9xhawx7xc4az4")))

(define-public crate-sp-npos-elections-compact-2.0.0-rc4 (c (n "sp-npos-elections-compact") (v "2.0.0-rc4") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1r7qb5j52xvz1h97ilbsdradpzqmasyagdxynlsiyhjhzvfc6lql")))

(define-public crate-sp-npos-elections-compact-2.0.0-rc5 (c (n "sp-npos-elections-compact") (v "2.0.0-rc5") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0xn5aknb632mf3nlgqksjxvn8r56g8kghpqx6gzmn4djnyyy0zzx")))

(define-public crate-sp-npos-elections-compact-2.0.0-rc6 (c (n "sp-npos-elections-compact") (v "2.0.0-rc6") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0y5d7chw4scinf86khv1rbs04kk99j8v32a87zsbcgjjcdkqwpy1")))

(define-public crate-sp-npos-elections-compact-2.0.0 (c (n "sp-npos-elections-compact") (v "2.0.0") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "11sc8prabfma2j10n5svd14iqlc7zpmpx6rmrfjgzc14d1bwb16p")))

(define-public crate-sp-npos-elections-compact-2.0.1 (c (n "sp-npos-elections-compact") (v "2.0.1") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1khl2d7r6fvafy8il30vmv1psc097dmkg3zhf9jbqgr08f25pb54")))

(define-public crate-sp-npos-elections-compact-3.0.0 (c (n "sp-npos-elections-compact") (v "3.0.0") (d (list (d (n "proc-macro-crate") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1lwkmz43kyh1qrxdqiibn0wd0fxw13kw2zq44iss319dlwdbnpq8")))

