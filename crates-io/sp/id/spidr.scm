(define-module (crates-io sp id spidr) #:use-module (crates-io))

(define-public crate-spidr-0.1.0 (c (n "spidr") (v "0.1.0") (h "1i0qxm9gpqf8i2dzc86nf0l3n5f8rc7sj61b1j49kpmgqc3yvi5s")))

(define-public crate-spidr-0.1.1 (c (n "spidr") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)))) (h "12svkqz5qinj9wnq35izw85k219kd8m1a6kqprakk5lqamyv7fl9")))

