(define-module (crates-io sp id spidriver) #:use-module (crates-io))

(define-public crate-spidriver-0.1.0 (c (n "spidriver") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "serial-embedded-hal") (r "^0.1.2") (d #t) (k 2)))) (h "1hzrn2r93q9rpg67040wmhf8q1mggbkdqyrlmm63skxqcgw03a8z")))

