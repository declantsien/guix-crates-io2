(define-module (crates-io sp id spidap) #:use-module (crates-io))

(define-public crate-spidap-0.1.0 (c (n "spidap") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "clap") (r "^2.33.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "jtagdap") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "spi-flash") (r "^0.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1349albpk719n5dgb1wmlpjyfyan7x4x943avfbkl9kxm9gyvsml")))

