(define-module (crates-io sp id spider-web-search) #:use-module (crates-io))

(define-public crate-spider-web-search-0.1.0 (c (n "spider-web-search") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "open") (r "^5.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)))) (h "0ar9qgjmxj9n6rwsyndhvnf59fslwaabwkkpmayd63hs1cp9fapb")))

(define-public crate-spider-web-search-0.1.1 (c (n "spider-web-search") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "open") (r "^5.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)))) (h "1152glkmj7mmlci4x1h02r25dhmk5p9j46nzhif7lwk52mcsjcx1")))

(define-public crate-spider-web-search-0.1.2 (c (n "spider-web-search") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "open") (r "^5.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)))) (h "09ng1wxw3q8ckx00gs2rdflq99rb635sx5shmv121fy9v1par37z")))

