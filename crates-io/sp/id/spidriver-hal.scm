(define-module (crates-io sp id spidriver-hal) #:use-module (crates-io))

(define-public crate-spidriver-hal-0.1.0 (c (n "spidriver-hal") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "serial-embedded-hal") (r "^0.1.2") (d #t) (k 2)) (d (n "spidriver") (r "^0.1.0") (d #t) (k 0)))) (h "0pya623qy42ki0ihsc2hcqwnjlbgrgm7xr2ny5x5gaksn45rcj8r")))

