(define-module (crates-io sp id spidev) #:use-module (crates-io))

(define-public crate-spidev-0.1.0 (c (n "spidev") (v "0.1.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "nix") (r "^0.3.6") (d #t) (k 0)))) (h "01i34cgsz391qf0wvk18ffikfxii095w93kbinwvzyfinz38737f")))

(define-public crate-spidev-0.2.0 (c (n "spidev") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.3.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "nix") (r "^0.4.2") (d #t) (k 0)))) (h "13pi3hihcm5mk2k28xs3ipl6cfysz0z5xjx5yns449ffwrdfv9l3")))

(define-public crate-spidev-0.2.1 (c (n "spidev") (v "0.2.1") (d (list (d (n "bitflags") (r "^0.3.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "nix") (r "^0.5.0") (d #t) (k 0)))) (h "16j7kd3q5qj9h3xdaa85srg8ybhkxwv2d4wyri7h824kga78r3kl")))

(define-public crate-spidev-0.3.0 (c (n "spidev") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.3.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "nix") (r "^0.6.0") (d #t) (k 0)))) (h "011y5727i5ldc23hgmz2jn9rjg5kw79nriyazscfhdraz4z1v80v")))

(define-public crate-spidev-0.4.0 (c (n "spidev") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.14") (d #t) (k 0)))) (h "1sczfwgi1rni1aq6pfjn29h24ma7zgw7x4clykylw3y2hwxajnma")))

(define-public crate-spidev-0.4.1 (c (n "spidev") (v "0.4.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.14") (d #t) (k 0)))) (h "03l9hz2m4z9ci4d8scws691170ggsqqa9hizcilmgg7sdiad5ml3")))

(define-public crate-spidev-0.5.0 (c (n "spidev") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.22") (d #t) (k 0)))) (h "0c6c36d04ivfi5xkard38bcw1s0dr3svamcmb40fv2nplpcd0qqy")))

(define-public crate-spidev-0.5.1 (c (n "spidev") (v "0.5.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "17vh9jx49267vbdcc9c4q87f57lmj66hry0hkghc3azimn8yhhsw")))

(define-public crate-spidev-0.5.2 (c (n "spidev") (v "0.5.2") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "0zgzp7jaah8rzy58rdr50s3i805ziafvf9k3mzzhj8bzink4q82s")))

(define-public crate-spidev-0.6.0 (c (n "spidev") (v "0.6.0") (d (list (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "06iwz12jvfjc23mmk6fjnyz1iq270hhd9dnwq6c7kirqjc90r062")))

