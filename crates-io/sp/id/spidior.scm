(define-module (crates-io sp id spidior) #:use-module (crates-io))

(define-public crate-spidior-0.2.1 (c (n "spidior") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.4") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "03ybyk9dfxfyn9m7divj5in1hb45vww2p43ddd7rnykyq9n26a68")))

(define-public crate-spidior-0.2.2 (c (n "spidior") (v "0.2.2") (d (list (d (n "clap") (r "^3.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.4") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1xb56sh0gj1ncc68i9xzc3arqd8dwf1v6mp8mvmnl2hzd5r5qwrl")))

