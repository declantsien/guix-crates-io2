(define-module (crates-io sp ak spak) #:use-module (crates-io))

(define-public crate-spak-0.1.0 (c (n "spak") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)))) (h "013amwj72ja6c5b8gpbpzzg4sq3dy199r1yl8b6xyliss2zws9j7")))

