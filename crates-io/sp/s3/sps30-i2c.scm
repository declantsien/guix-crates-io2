(define-module (crates-io sp s3 sps30-i2c) #:use-module (crates-io))

(define-public crate-sps30-i2c-0.0.1 (c (n "sps30-i2c") (v "0.0.1") (h "0g05ib6hl9lgq5h3r7mqyfw5h614c741ks2iqyfx43yg8qr6nlv9") (y #t)))

(define-public crate-sps30-i2c-0.1.0 (c (n "sps30-i2c") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "03yp25477wpvyczz18vgmprv7pad7j4f2kdqbs7zq0921pwkmdzg")))

