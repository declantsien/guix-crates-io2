(define-module (crates-io sp s3 sps30) #:use-module (crates-io))

(define-public crate-sps30-0.1.0 (c (n "sps30") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.5.1") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "ieee754") (r "^0.2") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "sensirion-hdlc") (r "^0.1.0") (d #t) (k 0)))) (h "17my9fx0248gi631nfmsf44rlfjhjqy30qh4rczm8q7j5sgd20mv")))

