(define-module (crates-io sp -c sp-crypto-hashing) #:use-module (crates-io))

(define-public crate-sp-crypto-hashing-0.0.0 (c (n "sp-crypto-hashing") (v "0.0.0") (h "1p4719adn80qrfyxdgj9yb1j2rj15j19v96x5by10p21jsf62w2v")))

(define-public crate-sp-crypto-hashing-0.1.0 (c (n "sp-crypto-hashing") (v "0.1.0") (d (list (d (n "blake2b_simd") (r "^1.0.1") (k 0)) (d (n "byteorder") (r "^1.3.2") (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "digest") (r "^0.10.3") (k 0)) (d (n "sha2") (r "^0.10.7") (k 0)) (d (n "sha3") (r "^0.10.0") (k 0)) (d (n "twox-hash") (r "^1.6.3") (f (quote ("digest_0_10"))) (k 0)))) (h "1sy5fh0l3zb8bimwfgkiv4mx290kr1wak94qi9dysd0kz2kjg6dw") (f (quote (("std" "blake2b_simd/std" "byteorder/std" "digest/std" "sha2/std" "sha3/std" "twox-hash/std") ("default" "std"))))))

