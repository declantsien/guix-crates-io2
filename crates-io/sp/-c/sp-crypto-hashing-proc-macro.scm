(define-module (crates-io sp -c sp-crypto-hashing-proc-macro) #:use-module (crates-io))

(define-public crate-sp-crypto-hashing-proc-macro-0.0.0 (c (n "sp-crypto-hashing-proc-macro") (v "0.0.0") (h "0xpniv04abfc7m97g9wz6cb2zaam03m6pnrp5mwhvlkhbwyn5hm0")))

(define-public crate-sp-crypto-hashing-proc-macro-0.1.0 (c (n "sp-crypto-hashing-proc-macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "sp-crypto-hashing") (r "^0.1.0") (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "16s86zyxaxbiii36z7p7wcli562lx0z20j1axcbqdga43qghypdq")))

