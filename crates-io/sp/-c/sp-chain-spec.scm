(define-module (crates-io sp -c sp-chain-spec) #:use-module (crates-io))

(define-public crate-sp-chain-spec-2.0.0-alpha.6 (c (n "sp-chain-spec") (v "2.0.0-alpha.6") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "09xv3455gqvsfj3r6nd6749jagj06zrm8vir9av7fi91giakvmy0")))

(define-public crate-sp-chain-spec-2.0.0-alpha.7 (c (n "sp-chain-spec") (v "2.0.0-alpha.7") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "04a2q1xv12ffayjv2j05465p0i6bwyh6f8jzdk0gw1dnlmplb5r9")))

(define-public crate-sp-chain-spec-2.0.0-alpha.8 (c (n "sp-chain-spec") (v "2.0.0-alpha.8") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0x9cyx1bpjliv5qq6aickg69csdybx537p9jf416pi64ssi6bbxr")))

(define-public crate-sp-chain-spec-2.0.0-rc1 (c (n "sp-chain-spec") (v "2.0.0-rc1") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0y5hdm1qc5y771z26xfkrsd5gqmlrqs6v6yc38z235b4lvrca9yv")))

(define-public crate-sp-chain-spec-2.0.0-rc2 (c (n "sp-chain-spec") (v "2.0.0-rc2") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "17gpiz88qj7jacw5m56przhawqs0y4ch0p5wawd97vm9vqnf3qcz")))

(define-public crate-sp-chain-spec-2.0.0-rc3 (c (n "sp-chain-spec") (v "2.0.0-rc3") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0dis3z10wfsf5fnrnay5g3w0d8fy4a00dbqfghp3js3rrc1hjfz9")))

(define-public crate-sp-chain-spec-2.0.0-rc4 (c (n "sp-chain-spec") (v "2.0.0-rc4") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "0mf3knyr3ijzqckaxvcmrnfvpcf4clyyifhl1ib6g3cmn7brrjkj")))

(define-public crate-sp-chain-spec-2.0.0-rc5 (c (n "sp-chain-spec") (v "2.0.0-rc5") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "17xzl6p1yfxsd978kb4v7x44k5l9ank6jsjzbjlkc5mrs8fhd4z8")))

(define-public crate-sp-chain-spec-2.0.0-rc6 (c (n "sp-chain-spec") (v "2.0.0-rc6") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "06bbq4ggykq58qfg6s4annc5gmd3z2233132wir53jahbwxbi7ig")))

(define-public crate-sp-chain-spec-2.0.0 (c (n "sp-chain-spec") (v "2.0.0") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "12z2v630gg3n9cnx55rzmwpyz9bi7fmn90sak98d1m023mkff30m")))

(define-public crate-sp-chain-spec-2.0.1 (c (n "sp-chain-spec") (v "2.0.1") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "03bj33hq48ar80ssphrw7ap3v1n1sk1fjibky6zbasjpwwqyw2pj")))

(define-public crate-sp-chain-spec-3.0.0 (c (n "sp-chain-spec") (v "3.0.0") (d (list (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "15yncpdglm9qq025qy1h6x50wjbcjqzmzh6hx9awgl4036qvw91s")))

