(define-module (crates-io sp -c sp-core-hashing-proc-macro) #:use-module (crates-io))

(define-public crate-sp-core-hashing-proc-macro-5.0.0 (c (n "sp-core-hashing-proc-macro") (v "5.0.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "sp-core-hashing") (r "^4.0.0") (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1mbk5n40mbvswjf3vvj8fdv1q50lp0kafjkavyyr50a7s65vz9wa")))

(define-public crate-sp-core-hashing-proc-macro-6.0.0 (c (n "sp-core-hashing-proc-macro") (v "6.0.0") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "sp-core-hashing") (r "^6.0.0") (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "00n65ibp4k4dn2c990v0dc7qksapkrg3z7c4jch3z27r209r4csq")))

(define-public crate-sp-core-hashing-proc-macro-7.0.0 (c (n "sp-core-hashing-proc-macro") (v "7.0.0") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "sp-core-hashing") (r "^7.0.0") (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1fxx0ag01g672bp5h0g3xrhjh6d9r86rwhry7i1msvdq6qfj6vf8")))

(define-public crate-sp-core-hashing-proc-macro-8.0.0 (c (n "sp-core-hashing-proc-macro") (v "8.0.0") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "sp-core-hashing") (r "^8.0.0") (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "07ia19582rffax1hj5j33kx0ygfrhq9rjrbrram5hl4p22xis1i3")))

(define-public crate-sp-core-hashing-proc-macro-9.0.0 (c (n "sp-core-hashing-proc-macro") (v "9.0.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "sp-core-hashing") (r "^9.0.0") (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0cn9kgwl7ng892lxjm271ijc7mlxaqq8cx8j2kmi76p589ypqzbj")))

(define-public crate-sp-core-hashing-proc-macro-9.1.0-dev1 (c (n "sp-core-hashing-proc-macro") (v "9.1.0-dev1") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "sp-core-hashing") (r "^9.1.0-dev1") (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0flvrmaa6pqcbrs7mrc8bq85ikg7fw4lzzcx9kz47dj758p8dhmm")))

(define-public crate-sp-core-hashing-proc-macro-9.1.0-dev.2 (c (n "sp-core-hashing-proc-macro") (v "9.1.0-dev.2") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "sp-core-hashing") (r "^9.1.0-dev.2") (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1agz4ppqv1b6ikb0q956hnmi3fw1jdzv1sabg99za7kyayjrz6da")))

(define-public crate-sp-core-hashing-proc-macro-9.1.0-dev.3 (c (n "sp-core-hashing-proc-macro") (v "9.1.0-dev.3") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "sp-core-hashing") (r "^9.1.0-dev.3") (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1z8gz91yfnvi1543s2dlyf7zmcgr71xx3j8maz3kp9pw0aw2v2d7")))

(define-public crate-sp-core-hashing-proc-macro-9.1.0-dev.4 (c (n "sp-core-hashing-proc-macro") (v "9.1.0-dev.4") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "sp-core-hashing") (r "^9.1.0-dev.4") (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "01cxj65db2cdgkkkm535yjg61f45avkh57fax4ssdx4mrv8b55qp")))

(define-public crate-sp-core-hashing-proc-macro-9.1.0-dev.5 (c (n "sp-core-hashing-proc-macro") (v "9.1.0-dev.5") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "sp-core-hashing") (r "=9.1.0-dev.5") (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1mih1xp3xs2yd2iyrn8ch7qk0y2q0ab6ydc9nlbkmlpk2j9gr5nx")))

(define-public crate-sp-core-hashing-proc-macro-9.1.0-dev.6 (c (n "sp-core-hashing-proc-macro") (v "9.1.0-dev.6") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "sp-core-hashing") (r "=9.1.0-dev.6") (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "05cjal14v169l14dsz354kzdzgcsyk6iadqr4g7zpqwy3xgxh5pd")))

(define-public crate-sp-core-hashing-proc-macro-10.0.0 (c (n "sp-core-hashing-proc-macro") (v "10.0.0") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "sp-core-hashing") (r "^10.0.0") (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "19a0y10ny1i4iy5a367qa34pkp5kihr3wy0095c5s5dzypchgiwd")))

(define-public crate-sp-core-hashing-proc-macro-11.0.0 (c (n "sp-core-hashing-proc-macro") (v "11.0.0") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "sp-core-hashing") (r "^11.0.0") (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "06h0b0icgj640rz9j6r0bp0lgwcdsw8cnbvj5fx09xbc2gx831jx")))

(define-public crate-sp-core-hashing-proc-macro-12.0.0-dev.1 (c (n "sp-core-hashing-proc-macro") (v "12.0.0-dev.1") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "sp-core-hashing") (r "=12.0.0-dev.1") (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "00wiza0b2xb4ib0c7sf2a62c6asvm8yic8xm0kylxkhm3k3lg3lv")))

(define-public crate-sp-core-hashing-proc-macro-12.0.0 (c (n "sp-core-hashing-proc-macro") (v "12.0.0") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "sp-core-hashing") (r "^12.0.0") (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0fri1ilksl89kqnllz8y60qb1jv9qzabrl6k5r4qsykp42i2fhrs")))

(define-public crate-sp-core-hashing-proc-macro-13.0.0 (c (n "sp-core-hashing-proc-macro") (v "13.0.0") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "sp-core-hashing") (r "^13.0.0") (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "18426nwva1vlgv5nxnfwhl8glnb1m3psq7x8glcnjdrh65lkxkj2")))

(define-public crate-sp-core-hashing-proc-macro-14.0.0 (c (n "sp-core-hashing-proc-macro") (v "14.0.0") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "sp-core-hashing") (r "^14.0.0") (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1n4vq698988sq16gg7xxby9f2kcnch6q96dvkb0ajjr0zjcdr5w4")))

(define-public crate-sp-core-hashing-proc-macro-15.0.0 (c (n "sp-core-hashing-proc-macro") (v "15.0.0") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "sp-core-hashing") (r "^15.0.0") (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1666bj1ss6xi2xnyxm8vfjwhj5xqvjnyzl1ckc042z36lzfzh9vm")))

(define-public crate-sp-core-hashing-proc-macro-16.0.0 (c (n "sp-core-hashing-proc-macro") (v "16.0.0") (d (list (d (n "sp-crypto-hashing-proc-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0lbz233z2yf0gkpcxwxavs470r7vv38a66p2rp629vh8bh3zhjm2")))

