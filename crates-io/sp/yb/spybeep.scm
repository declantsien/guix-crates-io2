(define-module (crates-io sp yb spybeep) #:use-module (crates-io))

(define-public crate-spybeep-0.1.0 (c (n "spybeep") (v "0.1.0") (d (list (d (n "freqiterator") (r "^0.1.0") (d #t) (k 0)) (d (n "mki") (r "^0.2.3") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (k 0)))) (h "1bv6mhbiavp9n60j0i9r7yb7bbkis07nlk4r0wa03fiiaxz54asy")))

(define-public crate-spybeep-0.1.1 (c (n "spybeep") (v "0.1.1") (d (list (d (n "freqiterator") (r "^1.0.0") (d #t) (k 0)) (d (n "mki") (r "^0.2.3") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (k 0)))) (h "0xljl1rixyqdsv5xmdda7kiap9i2igf3sbi7q91mc91dcsfvqcrk")))

(define-public crate-spybeep-0.2.0 (c (n "spybeep") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "freqiterator") (r "^1.0.0") (d #t) (k 0)) (d (n "mki") (r "^0.2.3") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (k 0)))) (h "0k1yj3vipdrpi1zjxb1zfm72wxvkmi2sd6wq1wa9jf8wpannl7i5")))

(define-public crate-spybeep-0.2.1 (c (n "spybeep") (v "0.2.1") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "freqiterator") (r "^1.0.0") (d #t) (k 0)) (d (n "mki") (r "^0.2.3") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (k 0)))) (h "08rcwyjbvzdr6xwvydabzy1zf920czf6p04m5sg8y4j21jfpk2xn")))

(define-public crate-spybeep-0.2.2 (c (n "spybeep") (v "0.2.2") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "freqiterator") (r "^1.0.0") (d #t) (k 0)) (d (n "mki") (r "^0.2.3") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (k 0)))) (h "0znggihf51vxmg2g7fxdpyh9vwwzph38fh7yv10r5rkxwxvfgw1f")))

(define-public crate-spybeep-0.2.3 (c (n "spybeep") (v "0.2.3") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "freqiterator") (r "^1.0.0") (d #t) (k 0)) (d (n "mki") (r "^0.2.3") (d #t) (k 0)) (d (n "rodio") (r "^0.17.3") (k 0)))) (h "111hr2lq7r8jxv1l3cjsp373lg29w8xn667fsbs0bnxmmpa6vz1h")))

(define-public crate-spybeep-0.3.0 (c (n "spybeep") (v "0.3.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "freqiterator") (r "^2.0") (d #t) (k 0)) (d (n "mki") (r "^0.2") (d #t) (k 0)) (d (n "rodio") (r "^0.17") (k 0)))) (h "1xhghjigzyqznhlgvbhzyr89kh40wf3dkpkb6rfi0rk7f7l7w0q3")))

