(define-module (crates-io sp ru spruce) #:use-module (crates-io))

(define-public crate-spruce-0.1.0 (c (n "spruce") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.7.1") (d #t) (k 0)) (d (n "newtype_derive") (r "^0.1.6") (d #t) (k 0)) (d (n "ptree") (r "^0.2.1") (f (quote ("ansi_term"))) (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)))) (h "0ryi10rj693rng7mzlg87lz4cvb2hf0w3h0s6229438w8bhxjw6i")))

(define-public crate-spruce-0.2.0 (c (n "spruce") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.7.1") (d #t) (k 0)) (d (n "ptree") (r "^0.2.1") (f (quote ("ansi_term"))) (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "1ziy43isr0a07a4cki86hyfvnfrqjfz91gbml1zh2mrsdicpgnzn")))

(define-public crate-spruce-0.3.0 (c (n "spruce") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.8.0") (d #t) (k 0)) (d (n "ptree") (r "^0.2.1") (f (quote ("ansi_term"))) (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "1nwf5xb85nl8wpwj30ni3i6b2lxhcsy0a7bda4p6abwvwzrgzq2b")))

