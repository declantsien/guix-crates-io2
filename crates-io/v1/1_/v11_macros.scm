(define-module (crates-io v1 #{1_}# v11_macros) #:use-module (crates-io))

(define-public crate-v11_macros-0.0.5 (c (n "v11_macros") (v "0.0.5") (d (list (d (n "procedural-masquerade") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r "0.3.*") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 0)) (d (n "syntex") (r "0.58.*") (d #t) (k 0)) (d (n "syntex_syntax") (r "0.58.*") (d #t) (k 0)))) (h "06xj0dv0rfml1dhmm5zlhg39l84kqnd2rj2kazz2qdhycychrymb")))

