(define-module (crates-io dq cs dqcsim-openqasm) #:use-module (crates-io))

(define-public crate-dqcsim-openqasm-0.1.0 (c (n "dqcsim-openqasm") (v "0.1.0") (d (list (d (n "dqcsim") (r "^0.0") (d #t) (k 0)) (d (n "meval") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "qasm") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0dvzdkbb1aykqy7a562chy8xzaal3wz91yh470x9ng8bqd5gjc6k")))

(define-public crate-dqcsim-openqasm-0.2.0 (c (n "dqcsim-openqasm") (v "0.2.0") (d (list (d (n "dqcsim") (r "^0.2") (d #t) (k 0)) (d (n "meval") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "qasm") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0my0z7zininm2kaq2rdvgvmqjicw6626d0rcw0dgh1g8qz1gzqh8")))

(define-public crate-dqcsim-openqasm-0.3.0 (c (n "dqcsim-openqasm") (v "0.3.0") (d (list (d (n "dqcsim") (r "^0.2") (d #t) (k 0)) (d (n "meval") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.3") (d #t) (k 0)) (d (n "qasm") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0fz20rg10k8i908a5irw9hj4zb6w71ajvrpaly0p94nq6yq9s0n2")))

