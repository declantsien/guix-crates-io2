(define-module (crates-io dq #{1-}# dq1-password) #:use-module (crates-io))

(define-public crate-dq1-password-0.1.0 (c (n "dq1-password") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 2)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "phf") (r "^0.10.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)) (d (n "structopt") (r "^0.3.23") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1l7ccsyk8wxjjz3z08czds0vm7h4rchnirq6qmx3mqiana06m19v")))

