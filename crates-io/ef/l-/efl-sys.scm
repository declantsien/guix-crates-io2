(define-module (crates-io ef l- efl-sys) #:use-module (crates-io))

(define-public crate-efl-sys-0.1.0 (c (n "efl-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "1443h31lkk8nq96nhlp5ajz8fbfswpvs50mn2xah9gkgd3icbva4") (l "efl")))

(define-public crate-efl-sys-0.2.0 (c (n "efl-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "17hpargr0ys5d7p3qddx8k6hxlxamrbwkfl0ai1cxd10l9ymjizj") (l "efl")))

