(define-module (crates-io ef i- efi-runner) #:use-module (crates-io))

(define-public crate-efi-runner-0.1.0 (c (n "efi-runner") (v "0.1.0") (d (list (d (n "tempfile") (r "^3.8.0") (d #t) (k 0)) (d (n "uefi-run") (r "^0.6.1") (d #t) (k 0)))) (h "1km4qgc7m7kgxg3qvkniyry6g2lkv4xx2230vr7c4f5ilxzkqhpd")))

(define-public crate-efi-runner-0.1.1 (c (n "efi-runner") (v "0.1.1") (d (list (d (n "tempfile") (r "^3.8.0") (d #t) (k 0)) (d (n "uefi-run") (r "^0.6.1") (d #t) (k 0)))) (h "06d1qx8z5sjcc60g63nzzr5b0ajf3qvgrb0andsj9lp767hll5ch")))

