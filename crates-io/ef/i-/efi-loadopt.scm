(define-module (crates-io ef i- efi-loadopt) #:use-module (crates-io))

(define-public crate-efi-loadopt-0.1.0 (c (n "efi-loadopt") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "0xp2q1w9z25c5ch0vaxikq7g1mipdfdigkryflrsnfhax1a2lim2")))

(define-public crate-efi-loadopt-0.2.0 (c (n "efi-loadopt") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03kmcgm78vggrcmbwg4dbh7gpv6maz6m81sw1s2zysyphfn5zcgw")))

(define-public crate-efi-loadopt-0.2.1 (c (n "efi-loadopt") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "18zyj31xaq4bg6lh7vlirb062wwi3lgh85cfl08x0skz3pafdbnl")))

