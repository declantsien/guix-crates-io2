(define-module (crates-io ef es efes) #:use-module (crates-io))

(define-public crate-efes-1.0.0 (c (n "efes") (v "1.0.0") (d (list (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "0zvhp2c6zj6psa5dlldwlcl8ll3ks4pw1yyb34l8pc123qnwv3gg")))

(define-public crate-efes-1.0.1 (c (n "efes") (v "1.0.1") (d (list (d (n "tempfile") (r "^3.2") (d #t) (k 2)))) (h "057nl1pdb8d0ij13gci67i5vzqnmih3h5g9rk03j0y37v3fqn4hb")))

