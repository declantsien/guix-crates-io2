(define-module (crates-io ef ib efibootnext-cli) #:use-module (crates-io))

(define-public crate-efibootnext-cli-0.3.0 (c (n "efibootnext-cli") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "efibootnext") (r "^0.3") (d #t) (k 0)) (d (n "embed-resource") (r "^2.2") (d #t) (k 1)))) (h "19a1pfpsxlin6b2v3ayvvmba50vj5wpy6j4k7qclmqyjzdkm3apb")))

(define-public crate-efibootnext-cli-0.4.0 (c (n "efibootnext-cli") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "efibootnext") (r "^0.4") (d #t) (k 0)) (d (n "embed-resource") (r "^2.2") (d #t) (k 1)))) (h "18wzfgs0fv1bhy4hinwp14al5fqp58k7w0kf2kspx4qnpfqzzdvl")))

(define-public crate-efibootnext-cli-0.5.0 (c (n "efibootnext-cli") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "efibootnext") (r "^0.5") (d #t) (k 0)) (d (n "embed-resource") (r "^2.2") (d #t) (k 1)))) (h "1iqlxyjfyras1hha92dm9zn4p8s9hsykndpb83qbss46v112vwi0")))

