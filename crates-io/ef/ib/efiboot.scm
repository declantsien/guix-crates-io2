(define-module (crates-io ef ib efiboot) #:use-module (crates-io))

(define-public crate-efiboot-1.0.1 (c (n "efiboot") (v "1.0.1") (d (list (d (n "efivar") (r "^1.0.1") (f (quote ("store"))) (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (f (quote ("paw"))) (d #t) (k 0)))) (h "1gp40nnacqgyv7ca9y97mvsqh3wa022k75nqy4kqikdhmxjpyhxc")))

(define-public crate-efiboot-1.0.2 (c (n "efiboot") (v "1.0.2") (d (list (d (n "efivar") (r "^1.0.2") (f (quote ("store"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (f (quote ("paw"))) (d #t) (k 0)))) (h "0z5gysyx01hmx0wfm6r1zlmvhxazyh6j67ciwaflm28x909jqaiz")))

(define-public crate-efiboot-1.0.3 (c (n "efiboot") (v "1.0.3") (d (list (d (n "efivar") (r "^1.0.3") (f (quote ("store"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (f (quote ("paw"))) (d #t) (k 0)))) (h "1fz0smn3k65ivs5pzss3gsxzyc4285r7f9mxy8qly4zc5h8hp1px")))

(define-public crate-efiboot-1.0.4 (c (n "efiboot") (v "1.0.4") (d (list (d (n "efivar") (r "^1.0.4") (f (quote ("store"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (f (quote ("paw"))) (d #t) (k 0)))) (h "0vsq4ql9nr5vnv4sjzm5z00c17sc9v7wmgdnigb5rhnrvy2xig8z")))

(define-public crate-efiboot-1.0.5 (c (n "efiboot") (v "1.0.5") (d (list (d (n "efivar") (r "^1.0.5") (f (quote ("store"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (f (quote ("paw"))) (d #t) (k 0)))) (h "1am1rdlbkxck6n8lxz2q3pf28cayddn1mr2bhx1xqjmwm11lyd09")))

(define-public crate-efiboot-1.0.6 (c (n "efiboot") (v "1.0.6") (d (list (d (n "efivar") (r "^1.0.6") (f (quote ("store"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (f (quote ("paw"))) (d #t) (k 0)))) (h "0r6cg56m3628px04fyap3f1jvg29560h0aanby1bs4198vbc0vqd")))

(define-public crate-efiboot-1.0.7 (c (n "efiboot") (v "1.0.7") (d (list (d (n "efivar") (r "^1.0.7") (f (quote ("store"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (f (quote ("paw"))) (d #t) (k 0)))) (h "03jkz77s8rklsh5b49z6lw864mqa69254rcg3kx0kv1jclxam168")))

(define-public crate-efiboot-1.0.8 (c (n "efiboot") (v "1.0.8") (d (list (d (n "efivar") (r "^1.0.8") (f (quote ("store"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (f (quote ("paw"))) (d #t) (k 0)))) (h "1a3rx1vbd8arhixnjidc36svdgm4mkvh725dp0hb6kj7az3l6nhv")))

(define-public crate-efiboot-1.0.9 (c (n "efiboot") (v "1.0.9") (d (list (d (n "efivar") (r "^1.0.9") (f (quote ("store"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (f (quote ("paw"))) (d #t) (k 0)))) (h "18241yb5s92a3prgax4spgzj5argznqnbg781vgaby09bi2k2hz4")))

(define-public crate-efiboot-1.0.10 (c (n "efiboot") (v "1.0.10") (d (list (d (n "efivar") (r "^1.0.10") (f (quote ("store"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (f (quote ("paw"))) (d #t) (k 0)))) (h "1zqdc8zyi5k58qvkijdfj2cxk01kk68jpm3ygwmavphvhnzlw7v7")))

(define-public crate-efiboot-1.0.11 (c (n "efiboot") (v "1.0.11") (d (list (d (n "efivar") (r "^1.0.11") (f (quote ("store"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (f (quote ("paw"))) (d #t) (k 0)))) (h "0qbkz64vxk16siczz9mf7q6469kq92x74kv4sdkf1wbdjcnpy7c1")))

(define-public crate-efiboot-1.0.12 (c (n "efiboot") (v "1.0.12") (d (list (d (n "efivar") (r "^1.0.12") (f (quote ("store"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (f (quote ("paw"))) (d #t) (k 0)))) (h "03ddgnmdiaxw6g11mrgi7bnlmw5f1viv458il3gmp8qb1gpwk1bk")))

(define-public crate-efiboot-1.0.13 (c (n "efiboot") (v "1.0.13") (d (list (d (n "efivar") (r "^1.0.13") (f (quote ("store"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (f (quote ("paw"))) (d #t) (k 0)))) (h "10iqq4fgl67jir2av9ab92d1x368r6m0sk3i6srkcqwgkp25zgvf")))

(define-public crate-efiboot-1.0.14 (c (n "efiboot") (v "1.0.14") (d (list (d (n "efivar") (r "^1.0.14") (f (quote ("store"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (f (quote ("paw"))) (d #t) (k 0)))) (h "1py4h6ciqdrm7z9bjd68sk6sz0zhxlgb44lrgmrz0wnb3mx2n7x0")))

(define-public crate-efiboot-1.1.0 (c (n "efiboot") (v "1.1.0") (d (list (d (n "efivar") (r "^1.1.0") (f (quote ("store"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (f (quote ("paw"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.4") (d #t) (k 0)))) (h "1k3sn0vq7yj3r2p3fy02fnczdagckf7cwdm59df9r0m3a3dmkcyl")))

(define-public crate-efiboot-1.1.1 (c (n "efiboot") (v "1.1.1") (d (list (d (n "efivar") (r "^1.1.1") (f (quote ("store"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (f (quote ("paw"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.4") (d #t) (k 0)))) (h "0y5lialrchfic9gvxmiwd18q2xw763a5r2vbg9pkqi8qxn51pvm3")))

(define-public crate-efiboot-1.1.2 (c (n "efiboot") (v "1.1.2") (d (list (d (n "efivar") (r "^1.1.2") (f (quote ("store"))) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (f (quote ("paw"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.4") (d #t) (k 0)))) (h "1cg000qnp1jbzdxsl8jxld2dfzdfah9bm8b1w7656f6dk19yyfi2")))

(define-public crate-efiboot-1.1.3 (c (n "efiboot") (v "1.1.3") (d (list (d (n "efivar") (r "^1.1.3") (f (quote ("store"))) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (f (quote ("paw"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (d #t) (k 0)))) (h "00sa3hh94havd7sia86w921fxdgkmvw08dqxjmvcx2kfz2hm0h4y")))

(define-public crate-efiboot-1.3.0 (c (n "efiboot") (v "1.3.0") (d (list (d (n "efivar") (r "^1.3.0") (f (quote ("store"))) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (f (quote ("paw"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (d #t) (k 0)))) (h "11irsl1id5ghqlyzgq8dfyzramin2w4af6scrqld9vndbixgzhws")))

(define-public crate-efiboot-1.4.0 (c (n "efiboot") (v "1.4.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "efivar") (r "^1.4.0") (f (quote ("store"))) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (f (quote ("paw"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (d #t) (k 0)))) (h "1xj69l9kwrkknagrvdwdapx44whck9zv80v41x24ci9pl58p9kf9")))

