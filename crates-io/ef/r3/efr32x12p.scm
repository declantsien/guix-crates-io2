(define-module (crates-io ef r3 efr32x12p) #:use-module (crates-io))

(define-public crate-efr32x12p-0.1.0 (c (n "efr32x12p") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0rhdqr6pzjfqmzccl8931nw7274xshdpbq4kldprfxhygdvbcgm4") (f (quote (("rt" "cortex-m-rt")))) (y #t)))

