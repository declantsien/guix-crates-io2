(define-module (crates-io ef r3 efr32xg12p) #:use-module (crates-io))

(define-public crate-efr32xg12p-0.1.0 (c (n "efr32xg12p") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1afdrzh6vrpw9sgrfpc69a1xv711j00mm917bvqz6nvwvq0ncrwz") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-efr32xg12p-0.1.1 (c (n "efr32xg12p") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1yxf3i30yniagvjsn4vbfmfmi72nhh05c7giqz4rdqwh454ff4iy") (f (quote (("rt" "cortex-m-rt"))))))

