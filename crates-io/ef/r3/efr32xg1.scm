(define-module (crates-io ef r3 efr32xg1) #:use-module (crates-io))

(define-public crate-efr32xg1-0.1.0 (c (n "efr32xg1") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "184qf2ms358qr1jq75zcv4idm8rb1lm20pdjzm6177a46f5snqrz") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-efr32xg1-0.1.1 (c (n "efr32xg1") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "04qcykbj8c5l6ml4gvxhv6kbkqyf5ad0wx7n05ynxfykrsr04amq") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-efr32xg1-0.2.0 (c (n "efr32xg1") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0ixgk7b5v0nkvi1xxfq65ndp60p28wzfnq73yn20f9rhzn9phfm4") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-efr32xg1-0.2.1 (c (n "efr32xg1") (v "0.2.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0gxflfnlpb08fdlxlav5awl2j7brbjap5cgb5g7p3mwsdpzhvxps") (f (quote (("rt" "cortex-m-rt/device"))))))

