(define-module (crates-io ef mt efmt_derive) #:use-module (crates-io))

(define-public crate-efmt_derive-0.0.1 (c (n "efmt_derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1dmsm3xs295c57qh6p8rvx2gnjg7n7xpa8vab36xcdyna90gs0f7")))

(define-public crate-efmt_derive-0.0.2 (c (n "efmt_derive") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "13c9c15pf7h5bvnv5ks4s79dyqchkdgwfijsvspac574fwynl9h4")))

(define-public crate-efmt_derive-0.0.3 (c (n "efmt_derive") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1rnjqx25nlkmj0qm2cibdxfyyp3mh2hihl3n8pb1b34ck6bjrjvd")))

(define-public crate-efmt_derive-0.1.0 (c (n "efmt_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1ly1h059bbiac4srs78mb4iscbqfpnqnapk9xmkg0fdz7m02s1nc")))

