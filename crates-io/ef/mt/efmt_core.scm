(define-module (crates-io ef mt efmt_core) #:use-module (crates-io))

(define-public crate-efmt_core-0.1.0 (c (n "efmt_core") (v "0.1.0") (d (list (d (n "efmt_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "erl_tokenize") (r "^0.5") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "similar-asserts") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1wpscyvbyqlyrc9rkgc68nskq9vbrw1mhc63h6a8ys4yniv6n59j")))

(define-public crate-efmt_core-0.1.1 (c (n "efmt_core") (v "0.1.1") (d (list (d (n "efmt_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "erl_tokenize") (r "^0.5") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "similar-asserts") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1g51p9psl81bhn2kplv9x9ny3l6hfiabpxrf419ly8269r67l5sx")))

(define-public crate-efmt_core-0.1.2 (c (n "efmt_core") (v "0.1.2") (d (list (d (n "efmt_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "erl_tokenize") (r "^0.5") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "similar-asserts") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0q5addw353bqr1anamw2mv7qn3zz14m86k6cppw0lf2ckpj3jfca")))

(define-public crate-efmt_core-0.1.3 (c (n "efmt_core") (v "0.1.3") (d (list (d (n "efmt_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "erl_tokenize") (r "^0.5") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "similar-asserts") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1fxgpq93z9c3myi97qf019kq2lp5gydwpp5clpil8380adgrbk1a")))

(define-public crate-efmt_core-0.1.4 (c (n "efmt_core") (v "0.1.4") (d (list (d (n "efmt_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "erl_tokenize") (r "^0.5") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "similar-asserts") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "195fss2j2jn44s1307k1s6sir3y5n8h9l5km3d88cg6a622prl9g")))

(define-public crate-efmt_core-0.2.0 (c (n "efmt_core") (v "0.2.0") (d (list (d (n "efmt_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "erl_tokenize") (r "^0.6") (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "similar-asserts") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0hfgm59ckpnl5vlw9q8yys1xyzg15pww177xh27b254wyf2crimr")))

