(define-module (crates-io ef il efiloader) #:use-module (crates-io))

(define-public crate-efiloader-0.0.1 (c (n "efiloader") (v "0.0.1") (d (list (d (n "const-utf16") (r "^0.2.1") (d #t) (k 0)) (d (n "crc") (r "^3.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 2)) (d (n "linked_list_allocator") (r "^0.10.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "widestring") (r "^1.0.2") (f (quote ("alloc"))) (k 0)))) (h "0lyx30dcdk872mb8q8wh5ikklzralqrhh0i6hl13qha59pvnk5cl") (f (quote (("strict_nx") ("default" "strict_nx"))))))

