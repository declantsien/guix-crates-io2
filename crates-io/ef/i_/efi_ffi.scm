(define-module (crates-io ef i_ efi_ffi) #:use-module (crates-io))

(define-public crate-efi_ffi-0.1.0 (c (n "efi_ffi") (v "0.1.0") (h "1p0k4ya8psyihrsy1kpqjh1ddk0zacwm7cip1fp4z8rq0f20j9q4")))

(define-public crate-efi_ffi-0.1.1 (c (n "efi_ffi") (v "0.1.1") (h "1l07ic08dssrych3bkl9sxh885mnpg2xhki4f8vnwcw9bcavnz40")))

