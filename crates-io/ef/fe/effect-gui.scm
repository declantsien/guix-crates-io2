(define-module (crates-io ef fe effect-gui) #:use-module (crates-io))

(define-public crate-effect-gui-0.2.2-alpha (c (n "effect-gui") (v "0.2.2-alpha") (h "16l3hrcf5zbsa5m36zs59cps12kmzsbxaifp367n03qwhc78zv0w")))

(define-public crate-effect-gui-0.2.6-alpha (c (n "effect-gui") (v "0.2.6-alpha") (h "1f3338zmqsjz1jg5jpmi599k77spcvh575zisb3jp02jbhz79nf8")))

