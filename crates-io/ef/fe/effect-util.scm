(define-module (crates-io ef fe effect-util) #:use-module (crates-io))

(define-public crate-effect-util-0.2.2-alpha (c (n "effect-util") (v "0.2.2-alpha") (h "0xzj9wnpnbvs2v3r4rr9gxhr9ifmvk96k3k6sl0gqi0z0v0yrvpa")))

(define-public crate-effect-util-0.2.6-alpha (c (n "effect-util") (v "0.2.6-alpha") (h "1q0msg8rp5706aidn5q49kp508ckpw940gicv9lzgfdxv2zdpv5k")))

