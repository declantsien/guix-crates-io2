(define-module (crates-io ef fe effect-monad) #:use-module (crates-io))

(define-public crate-effect-monad-0.1.0 (c (n "effect-monad") (v "0.1.0") (h "1ap84q7lqcnxga3z2c0yinmsp3797svrqa0lxwcydy000j0snkzb")))

(define-public crate-effect-monad-0.2.0 (c (n "effect-monad") (v "0.2.0") (h "10pkhivqjhsx9w756xr6zwhzv38qhcrz6ji8cwyylrawqcij4i88")))

(define-public crate-effect-monad-0.3.0 (c (n "effect-monad") (v "0.3.0") (h "07frx8dq9sbxyj4jj5ljcckak00njamxk1qn0iccy0rfw0a2wk06") (f (quote (("unstable"))))))

(define-public crate-effect-monad-0.3.1 (c (n "effect-monad") (v "0.3.1") (h "0jwnkylh5g9yfvda9sw5pz29hw3c14ky137kz6ncsv8c647xqqcp") (f (quote (("unstable"))))))

