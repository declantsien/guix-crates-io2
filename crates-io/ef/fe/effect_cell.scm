(define-module (crates-io ef fe effect_cell) #:use-module (crates-io))

(define-public crate-effect_cell-0.1.0 (c (n "effect_cell") (v "0.1.0") (h "0sj7c3bdhsl3p1wv40pvdl2li1dy00cswmfisa3n74x817ky08r5")))

(define-public crate-effect_cell-0.1.1 (c (n "effect_cell") (v "0.1.1") (h "00wsbj9sypkyn8m7wr4sgx3fzjah0zp7r8rh0pc260hpl0yr46qq")))

