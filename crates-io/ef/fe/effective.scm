(define-module (crates-io ef fe effective) #:use-module (crates-io))

(define-public crate-effective-0.1.0 (c (n "effective") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)))) (h "1rw6va8drvfj11x314s12h893b7vfbfzgcyadcx6s4fzyz71knmx")))

(define-public crate-effective-0.2.0 (c (n "effective") (v "0.2.0") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (o #t) (d #t) (k 0)))) (h "0i467wnj1nxsrwyg5h6qgmjnkp6l3faxv1sdc8bwxw5xq16vlgw1")))

(define-public crate-effective-0.3.0 (c (n "effective") (v "0.3.0") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (o #t) (d #t) (k 0)))) (h "0s090g25fnmgr0ixzjsxdv6myfg2h77vmkaglxhrg5dw0sykzjmq")))

(define-public crate-effective-0.3.1 (c (n "effective") (v "0.3.1") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (o #t) (d #t) (k 0)))) (h "0bplaqln5hy5dzh29rj9j9py0ki8002jc96dlx7h19nzq568qmxz")))

