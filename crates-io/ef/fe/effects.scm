(define-module (crates-io ef fe effects) #:use-module (crates-io))

(define-public crate-effects-0.0.1 (c (n "effects") (v "0.0.1") (d (list (d (n "async-recursion") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 0)))) (h "1yikbjnwks4abjc8c6f0258p69znyf67macs61m2acrnbr305rzn")))

