(define-module (crates-io ef fe effect) #:use-module (crates-io))

(define-public crate-effect-0.1.0 (c (n "effect") (v "0.1.0") (h "1qcnah2klihnsqmzwy8zp7a0kpn5xpbmjm9nb8m8czbz14zdxl20") (y #t)))

(define-public crate-effect-0.1.1 (c (n "effect") (v "0.1.1") (h "04shqvsbhg66yppll070gyggdqdhcciw25jfrh56vhnphkwqksqd")))

