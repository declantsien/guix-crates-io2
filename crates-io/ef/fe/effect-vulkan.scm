(define-module (crates-io ef fe effect-vulkan) #:use-module (crates-io))

(define-public crate-effect-vulkan-0.2.2-alpha (c (n "effect-vulkan") (v "0.2.2-alpha") (h "1rvl06axcvh47vhyq6lfg3ayid272jsd60r0fixa89ck9wk5gj5y")))

(define-public crate-effect-vulkan-0.2.6-alpha (c (n "effect-vulkan") (v "0.2.6-alpha") (h "0fqy2ic9xxvwk6gdiv7ay1s74j7bm57fcs6m6qhh2smx7m7i40z4")))

