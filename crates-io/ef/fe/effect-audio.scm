(define-module (crates-io ef fe effect-audio) #:use-module (crates-io))

(define-public crate-effect-audio-0.2.2-alpha (c (n "effect-audio") (v "0.2.2-alpha") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "effect-core") (r "^0.2.2-alpha") (d #t) (k 0) (p "effect-core")) (d (n "effect-util") (r "^0.2.2-alpha") (d #t) (k 0) (p "effect-util")) (d (n "rodio") (r "^0.17") (d #t) (k 0)))) (h "14h9aj46wsxflqgzhisraq9qhjrqkhlx2r6piay4iwrszlgkqlhg")))

(define-public crate-effect-audio-0.2.6-alpha (c (n "effect-audio") (v "0.2.6-alpha") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "effect-core") (r "^0.2.6-alpha") (d #t) (k 0) (p "effect-core")) (d (n "effect-util") (r "^0.2.6-alpha") (d #t) (k 0) (p "effect-util")) (d (n "rodio") (r "^0.17") (d #t) (k 0)))) (h "066qvwfz3zc7saw60gsfnrsbxd45jycxkzhqvjjjqxq8h8r9ih0g")))

