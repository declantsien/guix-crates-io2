(define-module (crates-io ef bu efbuilder) #:use-module (crates-io))

(define-public crate-efbuilder-0.0.1 (c (n "efbuilder") (v "0.0.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1q34sbskd49jv4kd7p9brhg18bkajaa24lhi6yvw9k3znlv4wlkg") (y #t)))

(define-public crate-efbuilder-0.0.2 (c (n "efbuilder") (v "0.0.2") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1il1hcj8g36r8lv5qqjwx6sjfqyl3h3v6qvh2p4hz9jb0c8rzkf0") (y #t)))

(define-public crate-efbuilder-0.0.3 (c (n "efbuilder") (v "0.0.3") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0k0vkqzbr01vmkjl8nx3g45ppqbsskmrvdkldzc63d9cp8jsm0ii")))

(define-public crate-efbuilder-0.0.4 (c (n "efbuilder") (v "0.0.4") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "18hc91za1h5b4b79wyaj2lwi3z6ybnvxd418sbf6bkfr2mp7naig")))

