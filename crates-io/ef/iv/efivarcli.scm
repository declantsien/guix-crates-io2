(define-module (crates-io ef iv efivarcli) #:use-module (crates-io))

(define-public crate-efivarcli-2.0.0 (c (n "efivarcli") (v "2.0.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "efivar") (r "^2.0.0") (f (quote ("store"))) (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)) (d (n "uuid") (r "^1.4.1") (d #t) (k 0)))) (h "0gybgmxx90d7rbcv8bnlyg2mp3nwcaln4g6anz32vl551ga24xx6")))

