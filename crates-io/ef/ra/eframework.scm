(define-module (crates-io ef ra eframework) #:use-module (crates-io))

(define-public crate-eframework-0.1.0 (c (n "eframework") (v "0.1.0") (d (list (d (n "abi_stable") (r "^0.9.3") (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "semver") (r "^0.11.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1845ig6jwqipxgrsblpd2jk0yk5r40clg8zz1gskas8q74kglx8d")))

