(define-module (crates-io ef f_ eff_data) #:use-module (crates-io))

(define-public crate-eff_data-0.1.0 (c (n "eff_data") (v "0.1.0") (d (list (d (n "binrw") (r "^0.12.0") (d #t) (k 0)) (d (n "eff_lib") (r "^0.1.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1xc9narfrm3qpxkv0n9nk4mjkhrr05svirqqygk0spyv6nydr8kk") (s 2) (e (quote (("serde" "dep:serde" "eff_lib/serde"))))))

