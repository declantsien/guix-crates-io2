(define-module (crates-io ef f_ eff_lib) #:use-module (crates-io))

(define-public crate-eff_lib-0.1.0 (c (n "eff_lib") (v "0.1.0") (d (list (d (n "binrw") (r "^0.12.0") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1a6mzji50nynm23w5icmalwh4ap3lin4a4xmphbpm7v2xr6apbd6") (s 2) (e (quote (("serde" "dep:serde"))))))

