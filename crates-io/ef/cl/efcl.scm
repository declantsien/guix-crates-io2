(define-module (crates-io ef cl efcl) #:use-module (crates-io))

(define-public crate-efcl-0.1.0 (c (n "efcl") (v "0.1.0") (h "0lqrhqy4g0i24bwvkj7fw6m9ym39bfrb8krhwni9ivd51h4jzrvj")))

(define-public crate-efcl-0.1.1 (c (n "efcl") (v "0.1.1") (h "0b9a8qjsy4v5vfs6nqyvcw7khrin3yxfvwyp3h9ild5rxqwmz5fa")))

(define-public crate-efcl-0.1.2 (c (n "efcl") (v "0.1.2") (h "13m102763c5adagya802zx9r0xr7hr2rk99r44rs88mnk2a73iac")))

