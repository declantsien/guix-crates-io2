(define-module (crates-io ef fi effing-macros) #:use-module (crates-io))

(define-public crate-effing-macros-0.1.0 (c (n "effing-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)))) (h "1skfav3m5bqia3biihsjgl1k8v30752ax5r9qphsffdy4j4hmp0m")))

