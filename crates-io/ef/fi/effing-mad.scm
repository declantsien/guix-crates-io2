(define-module (crates-io ef fi effing-mad) #:use-module (crates-io))

(define-public crate-effing-mad-0.1.0 (c (n "effing-mad") (v "0.1.0") (d (list (d (n "effing-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "frunk") (r "^0.4.0") (k 0)) (d (n "futures") (r "^0.3.23") (k 2)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking"))) (k 2)) (d (n "tokio") (r "^1.20.1") (f (quote ("rt"))) (k 2)))) (h "059dv8cn5yyngsqx9bndrkfrfqv7dd42rmbrhl6pmqnsckxhbs43") (f (quote (("alloc"))))))

