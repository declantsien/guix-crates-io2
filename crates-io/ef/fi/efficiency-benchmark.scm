(define-module (crates-io ef fi efficiency-benchmark) #:use-module (crates-io))

(define-public crate-efficiency-benchmark-0.1.0 (c (n "efficiency-benchmark") (v "0.1.0") (d (list (d (n "battery") (r "^0.7.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "17dzdj787mdqqrvbfjns1gz0w51zc0b78h506bcbvq389xm9sa02")))

(define-public crate-efficiency-benchmark-0.1.1 (c (n "efficiency-benchmark") (v "0.1.1") (d (list (d (n "battery") (r "^0.7.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1l80x3c1x413379vsjyjnmlhrbknvg4jvhrjdqy9xdbjv4d5clr7")))

(define-public crate-efficiency-benchmark-0.2.0 (c (n "efficiency-benchmark") (v "0.2.0") (d (list (d (n "battery") (r "^0.7.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1lmh0whzddlq6z47k4wlgqk8h4zq3785m22k0kj51p8pvha72hky")))

(define-public crate-efficiency-benchmark-0.2.1 (c (n "efficiency-benchmark") (v "0.2.1") (d (list (d (n "battery") (r "^0.7.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0q48nn2yfa055kgzav2am4vwimdq3qpp330sb6c65vn9w8y2bcb6")))

(define-public crate-efficiency-benchmark-0.2.2 (c (n "efficiency-benchmark") (v "0.2.2") (d (list (d (n "battery") (r "^0.7.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "15wh2n9m0zh5klg0islfnkb4lrp548nz2c6prkk82fy770srmbx6")))

(define-public crate-efficiency-benchmark-0.2.3 (c (n "efficiency-benchmark") (v "0.2.3") (d (list (d (n "battery") (r "^0.7.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0iibp9qhr86x7zkn59ndw1ijncqkdyl536phmcdg7cylzdr6wxsh")))

(define-public crate-efficiency-benchmark-0.2.4 (c (n "efficiency-benchmark") (v "0.2.4") (d (list (d (n "battery") (r "^0.7.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0l0i3pfrs1k4qk1lb6zz8vvnxl1ampmmg0hn4wz0qvvmypyr5pc2")))

(define-public crate-efficiency-benchmark-0.2.5 (c (n "efficiency-benchmark") (v "0.2.5") (d (list (d (n "battery") (r "^0.7.8") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0nxi35yic8ws17i0n06rbaql6c8p9xm59s8j8j12hsc2bzyfl94g")))

