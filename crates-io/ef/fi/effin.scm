(define-module (crates-io ef fi effin) #:use-module (crates-io))

(define-public crate-effin-0.0.1 (c (n "effin") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.9.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1s6paw61hwi9axbds7xprwww0ajn13rbzpvabnxv4g66jck6wc06")))

