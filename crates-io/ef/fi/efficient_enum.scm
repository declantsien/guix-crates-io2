(define-module (crates-io ef fi efficient_enum) #:use-module (crates-io))

(define-public crate-efficient_enum-0.1.0 (c (n "efficient_enum") (v "0.1.0") (h "1w7lidrp56y3k17adzms1zixcbwyz1k6203ydrg3lrgrq2sch9z9")))

(define-public crate-efficient_enum-0.1.1 (c (n "efficient_enum") (v "0.1.1") (h "113x0310924picw3d2k0ph5fqr6vsj1py4fnwn3nsqz39v0fdzpj")))

(define-public crate-efficient_enum-0.2.0 (c (n "efficient_enum") (v "0.2.0") (h "1rsaacrm7gzv0rmzgh36n365kvhwncfgr2n32nx1ic9jzlfj0phr")))

(define-public crate-efficient_enum-0.2.1 (c (n "efficient_enum") (v "0.2.1") (h "1rylcg6rki55yzxlry8fk1cmk49g0rp9b61birygcr6475mx33ai")))

(define-public crate-efficient_enum-0.2.2 (c (n "efficient_enum") (v "0.2.2") (h "1nk2dlghy1bpn96fl15vsp2p2naqrlmyz2wyfpmyr6907wxfja59")))

(define-public crate-efficient_enum-0.3.0 (c (n "efficient_enum") (v "0.3.0") (h "0w18zkns3wa9yvkdbw0gcwcs0mvpxn1qc4gx984r57y4kji6qvca")))

(define-public crate-efficient_enum-0.3.1 (c (n "efficient_enum") (v "0.3.1") (h "1bfqd62gn2jxkwsdxqjrcwv7g23920n3n6ccbpm8km17q7wijkvv")))

