(define-module (crates-io ef fi efficient-sm2) #:use-module (crates-io))

(define-public crate-efficient-sm2-0.0.1 (c (n "efficient-sm2") (v "0.0.1") (d (list (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "libsm") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1jhavwaf81hqrmif4vq90frbq44jkzzi1354bmncpzzx2x27d0f6") (f (quote (("internal_benches")))) (y #t)))

(define-public crate-efficient-sm2-0.1.0 (c (n "efficient-sm2") (v "0.1.0") (d (list (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "libsm") (r "^0.3.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0099fi51i2vmqlnvaycrybr8c0bzrwfdy1873pycibfk6z3z63yp") (f (quote (("internal_benches")))) (y #t)))

(define-public crate-efficient-sm2-0.1.1 (c (n "efficient-sm2") (v "0.1.1") (d (list (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "libsm") (r "^0.3.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "06kwfa6s8dy14ymq94kmkbdjz8x4v0p0qi3m0rf2k5wbnn83gg4a") (f (quote (("internal_benches")))) (y #t)))

(define-public crate-efficient-sm2-0.1.2 (c (n "efficient-sm2") (v "0.1.2") (d (list (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "libsm") (r "^0.3.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1qqz4dykl6w9mkx9px4bdlbks1drqf404w4fvh30lqh0d2mk5vxa") (f (quote (("internal_benches")))) (y #t)))

(define-public crate-efficient-sm2-0.1.3 (c (n "efficient-sm2") (v "0.1.3") (d (list (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "libsm") (r "^0.3.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1a60p87cbvq58pkqvjhnlri9x7v6dwd36lf68yabxvipi0jcv8ww") (f (quote (("internal_benches")))) (y #t)))

(define-public crate-efficient-sm2-0.1.4 (c (n "efficient-sm2") (v "0.1.4") (d (list (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "libsm") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "02nl07blca20v6ypbyy3kr0qyg3999x2m0c95rylqilyzjwfprgp") (f (quote (("internal_benches"))))))

(define-public crate-efficient-sm2-0.1.5 (c (n "efficient-sm2") (v "0.1.5") (d (list (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "libsm") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1mkbrmybs9x5hxm1jik53n1fxymv5ga7hhg02k40yhqh6la47d8y") (f (quote (("internal_benches"))))))

(define-public crate-efficient-sm2-0.1.6 (c (n "efficient-sm2") (v "0.1.6") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "libsm") (r "^0.5") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1abn19mvailharswgs7r9i5i8gwdf5477y0pr47asi3qdk0irsrj") (f (quote (("internal_benches")))) (y #t)))

(define-public crate-efficient-sm2-0.2.0 (c (n "efficient-sm2") (v "0.2.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "libsm") (r "^0.5") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "16zlsaxdqbbi9dr4bmipv4mj5ban793qm5kb9a4mlb6sx8qvsx6x") (f (quote (("internal_benches"))))))

(define-public crate-efficient-sm2-0.2.1 (c (n "efficient-sm2") (v "0.2.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "libsm") (r "^0.5") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0r9aqjh270qy2i0xbnrp8a2igrk9616mpnfcrx3xglb4xbzd8ln7") (f (quote (("internal_benches"))))))

(define-public crate-efficient-sm2-0.2.2 (c (n "efficient-sm2") (v "0.2.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "libsm") (r "^0.5") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "11jml478bzx49fjhf6g7r0y34lkfqmp9v8rbvd5w7c1mp7w6lnxj") (f (quote (("internal_benches"))))))

(define-public crate-efficient-sm2-0.2.3 (c (n "efficient-sm2") (v "0.2.3") (d (list (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "libsm") (r "^0.5") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0f8439nvfga5r7fn8gcbvm9f0hl2dacxd31yak5gq50vgcjhl3f7") (f (quote (("internal_benches"))))))

