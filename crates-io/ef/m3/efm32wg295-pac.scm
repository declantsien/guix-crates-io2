(define-module (crates-io ef m3 efm32wg295-pac) #:use-module (crates-io))

(define-public crate-efm32wg295-pac-0.1.0 (c (n "efm32wg295-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1y2saz84ls4riibw4zskab5wf4ni9qa65458kyqslbhd2li0vv1f") (f (quote (("rt" "cortex-m-rt/device"))))))

