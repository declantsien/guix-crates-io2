(define-module (crates-io ef m3 efm32zg210-pac) #:use-module (crates-io))

(define-public crate-efm32zg210-pac-0.1.0 (c (n "efm32zg210-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0dgh8dnqsx0xxnhv49x81gwy2lfmnip01ca00ivwcm9r7r5hhizz") (f (quote (("rt" "cortex-m-rt/device"))))))

