(define-module (crates-io ef m3 efm32hg350-pac) #:use-module (crates-io))

(define-public crate-efm32hg350-pac-0.1.0 (c (n "efm32hg350-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0kx40yq3jmrargr4r1imj92qpfaia5biffkijbgw0jlcyhm2cxpx") (f (quote (("rt" "cortex-m-rt/device"))))))

