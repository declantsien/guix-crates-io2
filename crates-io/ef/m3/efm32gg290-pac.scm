(define-module (crates-io ef m3 efm32gg290-pac) #:use-module (crates-io))

(define-public crate-efm32gg290-pac-0.1.0 (c (n "efm32gg290-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0xbxr19d3ha1ra5a45sk55zzrdajwwwsd11krf5cc88gw76vz8wr") (f (quote (("rt" "cortex-m-rt/device"))))))

