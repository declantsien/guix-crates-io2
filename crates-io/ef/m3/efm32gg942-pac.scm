(define-module (crates-io ef m3 efm32gg942-pac) #:use-module (crates-io))

(define-public crate-efm32gg942-pac-0.1.0 (c (n "efm32gg942-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "09ngy34kqiv86pwapnl0npfxh30yb2glyhhxp219fmdz7mim2pzv") (f (quote (("rt" "cortex-m-rt/device"))))))

