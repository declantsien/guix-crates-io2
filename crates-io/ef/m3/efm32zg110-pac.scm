(define-module (crates-io ef m3 efm32zg110-pac) #:use-module (crates-io))

(define-public crate-efm32zg110-pac-0.1.0 (c (n "efm32zg110-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "01k2g5v74g8nd4rij601fdgbq8px2abgzqiy0qxkv8yg96nzwg6z") (f (quote (("rt" "cortex-m-rt/device"))))))

