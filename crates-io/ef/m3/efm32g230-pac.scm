(define-module (crates-io ef m3 efm32g230-pac) #:use-module (crates-io))

(define-public crate-efm32g230-pac-0.1.0 (c (n "efm32g230-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0zrhywm9xpm9k5l1mv6xah3ak92048gdl8501xgfn2aw7d94vkrs") (f (quote (("rt" "cortex-m-rt/device"))))))

