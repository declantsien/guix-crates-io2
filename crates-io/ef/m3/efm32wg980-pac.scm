(define-module (crates-io ef m3 efm32wg980-pac) #:use-module (crates-io))

(define-public crate-efm32wg980-pac-0.1.0 (c (n "efm32wg980-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1hxf7p62xhjz1fgf2ympa12qwfn7lvwbb1f6gssv42258ilyzmhy") (f (quote (("rt" "cortex-m-rt/device"))))))

