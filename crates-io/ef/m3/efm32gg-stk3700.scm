(define-module (crates-io ef m3 efm32gg-stk3700) #:use-module (crates-io))

(define-public crate-efm32gg-stk3700-0.1.0 (c (n "efm32gg-stk3700") (v "0.1.0") (d (list (d (n "cortex-m-rt") (r "^0.5.0") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3.0") (d #t) (k 0)) (d (n "efm32gg-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "efm32gg990") (r "^0.1.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.2.0") (d #t) (k 0)))) (h "115sy97aajm5719ll1mvsdqi6620z4pwfxm3sbh1k5wa244cw0j4")))

