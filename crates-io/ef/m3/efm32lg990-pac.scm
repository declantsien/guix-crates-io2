(define-module (crates-io ef m3 efm32lg990-pac) #:use-module (crates-io))

(define-public crate-efm32lg990-pac-0.1.0 (c (n "efm32lg990-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "003cv16yd7xwai3kbpc42rwx73wxbb6cal35nfb7vxai2cjv4wx2") (f (quote (("rt" "cortex-m-rt/device"))))))

