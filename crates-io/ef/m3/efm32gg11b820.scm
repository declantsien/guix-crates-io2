(define-module (crates-io ef m3 efm32gg11b820) #:use-module (crates-io))

(define-public crate-efm32gg11b820-0.1.0 (c (n "efm32gg11b820") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.3.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.9") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0ljily14rccrx85ixcxndfmixsbz1hqhkhv8zbq94pl371azvhwd") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-efm32gg11b820-0.2.0 (c (n "efm32gg11b820") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.12") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "02g1cpz78l0gpfsnm1a0g4k7xc35fb1xnc5z5h87bk1dy7mxa853") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-efm32gg11b820-0.3.0 (c (n "efm32gg11b820") (v "0.3.0") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1yd2yj398k4jsr34w4vk8kp039ymwg8zasqw64m8n4i818sk0g62") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-efm32gg11b820-0.4.0 (c (n "efm32gg11b820") (v "0.4.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0kwg3c075v8zfv1qgrfjw7klgp7ps10w4pxg3xvkbkf20mil7gwx") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-efm32gg11b820-0.5.0 (c (n "efm32gg11b820") (v "0.5.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0cfc2vdzb6binff7rnp1pxaw5mh38ilw203p1mnqzpxc31ha4p7w") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-efm32gg11b820-0.5.1 (c (n "efm32gg11b820") (v "0.5.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0g4qpyqr9k4gam830p20hsldx8n2my2fal157n61m62cpp1pqmxi") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-efm32gg11b820-0.6.0 (c (n "efm32gg11b820") (v "0.6.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1nfzxlmkg0rlkxjh195f3dpw6v8576pa6g7nrkvzhjiwnaw33hb4") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-efm32gg11b820-0.7.0 (c (n "efm32gg11b820") (v "0.7.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0zjii7q0xmdy03jrnm4zmxlb56b1hy4xlb3kbndw2a1k73salh50") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-efm32gg11b820-0.9.0 (c (n "efm32gg11b820") (v "0.9.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "03mr7r1vka3wjpxa6ahskxx9vlamibpik837livvs1q6h25sdzjk") (f (quote (("rt" "cortex-m-rt/device"))))))

