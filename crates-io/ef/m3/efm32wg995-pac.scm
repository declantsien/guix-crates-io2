(define-module (crates-io ef m3 efm32wg995-pac) #:use-module (crates-io))

(define-public crate-efm32wg995-pac-0.1.0 (c (n "efm32wg995-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1inra26zf91zqkd7ax82am2larh7dcpm2wch5y66s37qdy03dk6c") (f (quote (("rt" "cortex-m-rt/device"))))))

