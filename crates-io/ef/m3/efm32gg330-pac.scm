(define-module (crates-io ef m3 efm32gg330-pac) #:use-module (crates-io))

(define-public crate-efm32gg330-pac-0.1.0 (c (n "efm32gg330-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "13p9xn7hnjz17a4hn3hql1aaqqiszq328z1i9fax0f5aiz9g86f1") (f (quote (("rt" "cortex-m-rt/device"))))))

