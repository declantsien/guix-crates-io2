(define-module (crates-io ef m3 efm32wg895-pac) #:use-module (crates-io))

(define-public crate-efm32wg895-pac-0.1.0 (c (n "efm32wg895-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1hsf9sqz3mms7qqp06f6vg3v7f8xx5yb6pvghfcp8fbfzlh2g8g1") (f (quote (("rt" "cortex-m-rt/device"))))))

