(define-module (crates-io ef m3 efm32gg12b830-pac) #:use-module (crates-io))

(define-public crate-efm32gg12b830-pac-0.1.0 (c (n "efm32gg12b830-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "12n7z0wj6ha8d6p23ib8ryvzj81gqbrjdvzay6mwnw1cxya898p5") (f (quote (("rt" "cortex-m-rt/device"))))))

