(define-module (crates-io ef m3 efm32g210-pac) #:use-module (crates-io))

(define-public crate-efm32g210-pac-0.1.0 (c (n "efm32g210-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0jic43zbvf6scw5lsdi04n8x68mknm89g9mqz530mllzkz6jcf7q") (f (quote (("rt" "cortex-m-rt/device"))))))

