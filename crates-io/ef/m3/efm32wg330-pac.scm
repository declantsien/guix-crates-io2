(define-module (crates-io ef m3 efm32wg330-pac) #:use-module (crates-io))

(define-public crate-efm32wg330-pac-0.1.0 (c (n "efm32wg330-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1i56bgfj66s4z7vmlzgc4rr8b7zg3df0h4xh662p56i6d05k6j22") (f (quote (("rt" "cortex-m-rt/device"))))))

