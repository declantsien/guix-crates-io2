(define-module (crates-io ef m3 efm32lg880-pac) #:use-module (crates-io))

(define-public crate-efm32lg880-pac-0.1.0 (c (n "efm32lg880-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0pja113wr19s4alq4x97jd5d2k7mhg7xygam23wsjb1590sznz3x") (f (quote (("rt" "cortex-m-rt/device"))))))

