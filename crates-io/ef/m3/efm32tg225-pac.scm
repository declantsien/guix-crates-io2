(define-module (crates-io ef m3 efm32tg225-pac) #:use-module (crates-io))

(define-public crate-efm32tg225-pac-0.1.0 (c (n "efm32tg225-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "08gkq63i80m6iq0ddx58i4n5bl5xhg2yib52rn262zflxfg74v47") (f (quote (("rt" "cortex-m-rt/device"))))))

