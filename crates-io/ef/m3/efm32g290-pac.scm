(define-module (crates-io ef m3 efm32g290-pac) #:use-module (crates-io))

(define-public crate-efm32g290-pac-0.1.0 (c (n "efm32g290-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "05il3012725qxm67dxf2ddzxhff3374nxwljmfp1l6zfb7yhl6lr") (f (quote (("rt" "cortex-m-rt/device"))))))

