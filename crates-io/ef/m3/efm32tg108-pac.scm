(define-module (crates-io ef m3 efm32tg108-pac) #:use-module (crates-io))

(define-public crate-efm32tg108-pac-0.1.0 (c (n "efm32tg108-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0z0jd8rja9yak1sd89wpqnwz9kvyf5plzrh1fa93csn8v5in68s5") (f (quote (("rt" "cortex-m-rt/device"))))))

