(define-module (crates-io ef m3 efm32gg840-pac) #:use-module (crates-io))

(define-public crate-efm32gg840-pac-0.1.0 (c (n "efm32gg840-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1l5ikawjfan5fircx0hl1m85z0w460pwi6y2ppz4d0q4863ikqh8") (f (quote (("rt" "cortex-m-rt/device"))))))

