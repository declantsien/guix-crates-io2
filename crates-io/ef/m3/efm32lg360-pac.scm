(define-module (crates-io ef m3 efm32lg360-pac) #:use-module (crates-io))

(define-public crate-efm32lg360-pac-0.1.0 (c (n "efm32lg360-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1zg4s87dp42994r7bzylc33fccdnhibyns3fmv1aj15nsv6116qh") (f (quote (("rt" "cortex-m-rt/device"))))))

