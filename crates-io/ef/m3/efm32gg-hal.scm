(define-module (crates-io ef m3 efm32gg-hal) #:use-module (crates-io))

(define-public crate-efm32gg-hal-0.1.0 (c (n "efm32gg-hal") (v "0.1.0") (d (list (d (n "efm32gg990") (r "^0.1.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.0") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0d9ip0r5n6wg1dg510xmwnpl1j6kr6v41g408yyy753wvyxrhr0n") (f (quote (("unproven") ("default" "unproven"))))))

(define-public crate-efm32gg-hal-0.2.0 (c (n "efm32gg-hal") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.5.2") (d #t) (k 0)) (d (n "efm32gg990") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "efr32xg1") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.0") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1y9yw7fz414fb8s5c3scad1f2iaq75y1b2dg9xmhjjykhk66as53") (f (quote (("unproven") ("default" "unproven") ("chip-efr32xg1" "efr32xg1") ("chip-efm32gg" "efm32gg990"))))))

(define-public crate-efm32gg-hal-0.3.0 (c (n "efm32gg-hal") (v "0.3.0") (d (list (d (n "cortex-m") (r "^0.5.2") (d #t) (k 0)) (d (n "efm32gg990") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "efr32xg1") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.0") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1dc8gpbb9pifa8i207prp5l9xb4mrg0p4bk435yhm9hlsrh4v8jq") (f (quote (("unproven") ("default" "unproven") ("chip-efr32xg1" "efr32xg1") ("chip-efm32gg" "efm32gg990" "_has_timer2") ("_has_timer2"))))))

