(define-module (crates-io ef m3 efm32zg108-pac) #:use-module (crates-io))

(define-public crate-efm32zg108-pac-0.1.0 (c (n "efm32zg108-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "12ym5z2ix4j8v1g6ngi8rmi9f416cqawzaig2i2zllg96w3hnnk3") (f (quote (("rt" "cortex-m-rt/device"))))))

