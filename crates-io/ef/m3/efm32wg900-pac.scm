(define-module (crates-io ef m3 efm32wg900-pac) #:use-module (crates-io))

(define-public crate-efm32wg900-pac-0.1.0 (c (n "efm32wg900-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0z2j6ggsr5qi5j0cmgv2w161k4swgdcvyp7z1q4gpcmraqsl1k5q") (f (quote (("rt" "cortex-m-rt/device"))))))

