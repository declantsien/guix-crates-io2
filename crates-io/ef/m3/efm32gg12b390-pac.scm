(define-module (crates-io ef m3 efm32gg12b390-pac) #:use-module (crates-io))

(define-public crate-efm32gg12b390-pac-0.1.0 (c (n "efm32gg12b390-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1sx1q74by0v696cn8q174b02pdwyaixy9ar4s09rrawpxp2xxjv9") (f (quote (("rt" "cortex-m-rt/device"))))))

