(define-module (crates-io ef m3 efm32g800-pac) #:use-module (crates-io))

(define-public crate-efm32g800-pac-0.1.0 (c (n "efm32g800-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1r8ss8dr4w8d6lvmrj8lqb3ywj4xy0d3i2wy2m41wxczhy31dx5r") (f (quote (("rt" "cortex-m-rt/device"))))))

