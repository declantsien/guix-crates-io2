(define-module (crates-io ef m3 efm32pg1b200-pac) #:use-module (crates-io))

(define-public crate-efm32pg1b200-pac-0.1.0 (c (n "efm32pg1b200-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1rkxjr0344cdm19jci1yppjs5b5hnd3afjxb6w629m10hyd8759m") (f (quote (("rt" "cortex-m-rt/device"))))))

