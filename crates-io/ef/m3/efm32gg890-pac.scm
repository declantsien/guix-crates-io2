(define-module (crates-io ef m3 efm32gg890-pac) #:use-module (crates-io))

(define-public crate-efm32gg890-pac-0.1.0 (c (n "efm32gg890-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0z81ibc93d9gjjhic1gqk0r5r9xak4y62qy507cb9y1i11m7nl7h") (f (quote (("rt" "cortex-m-rt/device"))))))

