(define-module (crates-io ef m3 efm32hg309-pac) #:use-module (crates-io))

(define-public crate-efm32hg309-pac-0.1.0 (c (n "efm32hg309-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "081s616rkjpd7s0amhgphyyskfzv8jplhhig52pgjxsyi5g1ir0j") (f (quote (("rt" "cortex-m-rt/device"))))))

