(define-module (crates-io ef m3 efm32zg222-pac) #:use-module (crates-io))

(define-public crate-efm32zg222-pac-0.1.0 (c (n "efm32zg222-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0pc6b8y4pmyhyyipfgnl6kl460ls757w2grw4iz71xlgwn79j6lr") (f (quote (("rt" "cortex-m-rt/device"))))))

