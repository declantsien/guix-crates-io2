(define-module (crates-io ef m3 efm32tg11b520-pac) #:use-module (crates-io))

(define-public crate-efm32tg11b520-pac-0.1.0 (c (n "efm32tg11b520-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1p23m8a3j8sfk6a66n2d5127v7kd224kklfwhvdh82dxh6wpv0kj") (f (quote (("rt" "cortex-m-rt/device"))))))

