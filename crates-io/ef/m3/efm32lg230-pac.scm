(define-module (crates-io ef m3 efm32lg230-pac) #:use-module (crates-io))

(define-public crate-efm32lg230-pac-0.1.0 (c (n "efm32lg230-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0zr9x1h12snh7j1r7bwpdx82dmr8sz35yr07cqr8fdd24z9mz5r3") (f (quote (("rt" "cortex-m-rt/device"))))))

