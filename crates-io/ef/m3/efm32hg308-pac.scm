(define-module (crates-io ef m3 efm32hg308-pac) #:use-module (crates-io))

(define-public crate-efm32hg308-pac-0.1.0 (c (n "efm32hg308-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "05rapzhibhn6ldb9mn379gplyc6nawaj60ak19g5w4w4a2glwqkx") (f (quote (("rt" "cortex-m-rt/device"))))))

