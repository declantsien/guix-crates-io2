(define-module (crates-io ef m3 efm32lg995-pac) #:use-module (crates-io))

(define-public crate-efm32lg995-pac-0.1.0 (c (n "efm32lg995-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0qh4glc9j60fgy0hm305m2hmd4cs70rdc4cwd2vjwmz95ld3mmp8") (f (quote (("rt" "cortex-m-rt/device"))))))

