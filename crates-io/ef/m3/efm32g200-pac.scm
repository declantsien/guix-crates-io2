(define-module (crates-io ef m3 efm32g200-pac) #:use-module (crates-io))

(define-public crate-efm32g200-pac-0.1.0 (c (n "efm32g200-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1v8a6hw5caxyp9fw21yxcs4vdjfcmywl1wx1hbx7lcmyyc62njch") (f (quote (("rt" "cortex-m-rt/device"))))))

