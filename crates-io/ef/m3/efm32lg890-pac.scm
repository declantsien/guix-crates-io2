(define-module (crates-io ef m3 efm32lg890-pac) #:use-module (crates-io))

(define-public crate-efm32lg890-pac-0.1.0 (c (n "efm32lg890-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "107d70vbvhri58gdhmvc8aiw1k320w6bv2vnhpwhn5k5dnlilj8b") (f (quote (("rt" "cortex-m-rt/device"))))))

