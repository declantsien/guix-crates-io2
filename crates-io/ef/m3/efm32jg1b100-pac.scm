(define-module (crates-io ef m3 efm32jg1b100-pac) #:use-module (crates-io))

(define-public crate-efm32jg1b100-pac-0.1.0 (c (n "efm32jg1b100-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1x6aj2z9knd17gc9gdcl81zfmfcwqqfq49alk51x69z8vlhw6gi9") (f (quote (("rt" "cortex-m-rt/device"))))))

