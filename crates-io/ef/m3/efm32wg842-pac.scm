(define-module (crates-io ef m3 efm32wg842-pac) #:use-module (crates-io))

(define-public crate-efm32wg842-pac-0.1.0 (c (n "efm32wg842-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0krj152gsjz872lj676gx6j7xq8i9k1sribcpxxgjrdwqaaprq4b") (f (quote (("rt" "cortex-m-rt/device"))))))

