(define-module (crates-io ef m3 efm32wg840-pac) #:use-module (crates-io))

(define-public crate-efm32wg840-pac-0.1.0 (c (n "efm32wg840-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1zcp9njwxs2gbg1b17wqcy47z19gzrz2g5kgz4f9xn7s558xrd37") (f (quote (("rt" "cortex-m-rt/device"))))))

