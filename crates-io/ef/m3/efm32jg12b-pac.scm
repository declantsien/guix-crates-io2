(define-module (crates-io ef m3 efm32jg12b-pac) #:use-module (crates-io))

(define-public crate-efm32jg12b-pac-0.1.1 (c (n "efm32jg12b-pac") (v "0.1.1") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1ywcx1ddn6fhfy8jbjflyinn1q19waxmc0s2c6vf22kknaspx5xd") (f (quote (("rt" "cortex-m-rt/device") ("efm32jg12b500") ("default" "rt"))))))

(define-public crate-efm32jg12b-pac-0.1.2 (c (n "efm32jg12b-pac") (v "0.1.2") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "~1") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "~1") (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1mam7al9a41dfzcddqggcbkprf7g0namfiv716sn9pr7xd80m6k3") (f (quote (("rt" "cortex-m-rt/device") ("efm32jg12b500") ("default" "rt"))))))

(define-public crate-efm32jg12b-pac-0.1.3 (c (n "efm32jg12b-pac") (v "0.1.3") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "~1") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "~1") (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0xk09gpn32r97gnyi598hmk3c2jg8z0a9i6wf753b52pwwhc22na") (f (quote (("rt" "cortex-m-rt/device") ("efm32jg12b500") ("default" "rt"))))))

(define-public crate-efm32jg12b-pac-0.1.4 (c (n "efm32jg12b-pac") (v "0.1.4") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "~1") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "~1") (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "178g42bgwnzzygys5yxh721wixjdrc84d6xxn664xb8bizgd5y0x") (f (quote (("rt" "cortex-m-rt/device") ("efm32jg12b500") ("default" "rt")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

