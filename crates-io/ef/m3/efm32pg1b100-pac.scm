(define-module (crates-io ef m3 efm32pg1b100-pac) #:use-module (crates-io))

(define-public crate-efm32pg1b100-pac-0.1.0 (c (n "efm32pg1b100-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1nqs8i5z981f3kfigda9yg59qf8hmmccwpzz8xhs1q5hai51yrz0") (f (quote (("rt" "cortex-m-rt/device"))))))

