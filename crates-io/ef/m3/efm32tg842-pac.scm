(define-module (crates-io ef m3 efm32tg842-pac) #:use-module (crates-io))

(define-public crate-efm32tg842-pac-0.1.0 (c (n "efm32tg842-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1scdjbz8v86jxsxfvy69ixwygdi4n9v00jpqfv4l3a2mr7ly0f4y") (f (quote (("rt" "cortex-m-rt/device"))))))

