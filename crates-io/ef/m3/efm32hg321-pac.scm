(define-module (crates-io ef m3 efm32hg321-pac) #:use-module (crates-io))

(define-public crate-efm32hg321-pac-0.1.0 (c (n "efm32hg321-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1x1caab3j5156yyxh15s087wxd6l86f4iczhppz27gwj2yl219sr") (f (quote (("rt" "cortex-m-rt/device"))))))

