(define-module (crates-io ef m3 efm32tg825-pac) #:use-module (crates-io))

(define-public crate-efm32tg825-pac-0.1.0 (c (n "efm32tg825-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0lr060a907fv6d80yvdbx6dcqg3jinydzfyrnjmdbvb750gs3in0") (f (quote (("rt" "cortex-m-rt/device"))))))

