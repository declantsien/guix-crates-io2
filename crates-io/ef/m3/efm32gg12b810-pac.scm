(define-module (crates-io ef m3 efm32gg12b810-pac) #:use-module (crates-io))

(define-public crate-efm32gg12b810-pac-0.1.0 (c (n "efm32gg12b810-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "01swvmcr4yr709vg7rnmv20296di27isssk8j88018r6wzzjsac1") (f (quote (("rt" "cortex-m-rt/device"))))))

