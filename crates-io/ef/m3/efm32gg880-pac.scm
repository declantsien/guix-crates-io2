(define-module (crates-io ef m3 efm32gg880-pac) #:use-module (crates-io))

(define-public crate-efm32gg880-pac-0.1.0 (c (n "efm32gg880-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "12ga3qvxd1g2n54qh0gddh4a1142f5k9p4jpg37dqdxpjsgbm0f3") (f (quote (("rt" "cortex-m-rt/device"))))))

