(define-module (crates-io ef m3 efm32lg842-pac) #:use-module (crates-io))

(define-public crate-efm32lg842-pac-0.1.0 (c (n "efm32lg842-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "021n47sanijrrq5hicpg6dnapf8zvf8r4ymyq3hrj7xyskczmrw3") (f (quote (("rt" "cortex-m-rt/device"))))))

