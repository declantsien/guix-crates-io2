(define-module (crates-io ef m3 efm32hg309f64-pac) #:use-module (crates-io))

(define-public crate-efm32hg309f64-pac-0.2.1 (c (n "efm32hg309f64-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1l8s62adlzzj7jqwa525fhh0rhrygbi05i2rx6gwraczkdcdhfqg") (f (quote (("rt" "cortex-m-rt/device"))))))

