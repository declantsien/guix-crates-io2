(define-module (crates-io ef m3 efm32tg232-pac) #:use-module (crates-io))

(define-public crate-efm32tg232-pac-0.1.0 (c (n "efm32tg232-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0xkc3safv2r1flw9spgca8bk0zqxhgqwv4rlb6l8r33y0z5dc7sg") (f (quote (("rt" "cortex-m-rt/device"))))))

