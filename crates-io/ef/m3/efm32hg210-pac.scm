(define-module (crates-io ef m3 efm32hg210-pac) #:use-module (crates-io))

(define-public crate-efm32hg210-pac-0.1.0 (c (n "efm32hg210-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1011g2wzn1f7l9i2mhqzvpx7b8w1br72xjclws1qwbp47979wz38") (f (quote (("rt" "cortex-m-rt/device"))))))

