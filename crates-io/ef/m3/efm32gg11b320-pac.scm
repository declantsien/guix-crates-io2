(define-module (crates-io ef m3 efm32gg11b320-pac) #:use-module (crates-io))

(define-public crate-efm32gg11b320-pac-0.1.0 (c (n "efm32gg11b320-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1kkkk9x09gbw8wmpdqslajz3j44xj65wyqq14qaj8y8y0s302ipp") (f (quote (("rt" "cortex-m-rt/device"))))))

