(define-module (crates-io ef m3 efm32wg332-pac) #:use-module (crates-io))

(define-public crate-efm32wg332-pac-0.1.0 (c (n "efm32wg332-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0wmgz3g2lyn40mjiz49impc3rm2abn8249p9jrbx4p3jn3angjyr") (f (quote (("rt" "cortex-m-rt/device"))))))

