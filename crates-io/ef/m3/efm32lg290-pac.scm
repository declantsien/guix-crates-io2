(define-module (crates-io ef m3 efm32lg290-pac) #:use-module (crates-io))

(define-public crate-efm32lg290-pac-0.1.0 (c (n "efm32lg290-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1ncnc3ry8nbplh9izcryvy99jakdgdfd7zjrzl728q1sdbbmgi0a") (f (quote (("rt" "cortex-m-rt/device"))))))

