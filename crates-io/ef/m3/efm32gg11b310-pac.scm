(define-module (crates-io ef m3 efm32gg11b310-pac) #:use-module (crates-io))

(define-public crate-efm32gg11b310-pac-0.1.0 (c (n "efm32gg11b310-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1n9p9fmwjv8nw7xj8apfzsv2560w96ishax0fsdxnnnfd2q5qgsd") (f (quote (("rt" "cortex-m-rt/device"))))))

