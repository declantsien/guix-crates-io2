(define-module (crates-io ef m3 efm32pg22-pac) #:use-module (crates-io))

(define-public crate-efm32pg22-pac-0.1.1 (c (n "efm32pg22-pac") (v "0.1.1") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1sbc6halfw5r0jga6c1raxdsjcf76iflyivfp9ma34kgf7k9kw9v") (f (quote (("rt" "cortex-m-rt/device") ("efm32pg22c200") ("default" "rt"))))))

(define-public crate-efm32pg22-pac-0.1.2 (c (n "efm32pg22-pac") (v "0.1.2") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "~1") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "~1") (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "13335vra95l38bf83rd183c1kxv3968z1wvbz3i3hvbivw0a5s9w") (f (quote (("rt" "cortex-m-rt/device") ("efm32pg22c200") ("default" "rt"))))))

(define-public crate-efm32pg22-pac-0.1.3 (c (n "efm32pg22-pac") (v "0.1.3") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "~1") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "~1") (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "17c0hmra24ar5ps3l6kr2q7hdgaq3yb90sd1fxasqprsbjncq59s") (f (quote (("rt" "cortex-m-rt/device") ("efm32pg22c200") ("default" "rt"))))))

(define-public crate-efm32pg22-pac-0.1.4 (c (n "efm32pg22-pac") (v "0.1.4") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "~1") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "~1") (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1am3flycdb6gd4xy9213z5a40az5pr76srmaw65zr5b1nvnz40r4") (f (quote (("rt" "cortex-m-rt/device") ("efm32pg22c200") ("default" "rt")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

