(define-module (crates-io ef m3 efm32gg940-pac) #:use-module (crates-io))

(define-public crate-efm32gg940-pac-0.1.0 (c (n "efm32gg940-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0mag20mb7bmn1bn2dispa32r6gk1wb99r4s3sikf2dxwcvr0w3v0") (f (quote (("rt" "cortex-m-rt/device"))))))

