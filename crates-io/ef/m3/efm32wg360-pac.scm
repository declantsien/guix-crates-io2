(define-module (crates-io ef m3 efm32wg360-pac) #:use-module (crates-io))

(define-public crate-efm32wg360-pac-0.1.0 (c (n "efm32wg360-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0am8nfmgm3mikn084n78id4iplzp709s0vb86i2r96a1awvjqjd3") (f (quote (("rt" "cortex-m-rt/device"))))))

