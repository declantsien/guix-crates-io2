(define-module (crates-io ef m3 efm32gg280-pac) #:use-module (crates-io))

(define-public crate-efm32gg280-pac-0.1.0 (c (n "efm32gg280-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0qr6m7wm4di0fj8z4vnp35imnrxqrixd6s1abplbxzfhysw4cif2") (f (quote (("rt" "cortex-m-rt/device"))))))

