(define-module (crates-io ef m3 efm32wg232-pac) #:use-module (crates-io))

(define-public crate-efm32wg232-pac-0.1.0 (c (n "efm32wg232-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "19l5biab5xsqwv86z9mdjnf6p9b8jlf40cnbdy9asnqal1pf5dyz") (f (quote (("rt" "cortex-m-rt/device"))))))

