(define-module (crates-io ef m3 efm32wg990-pac) #:use-module (crates-io))

(define-public crate-efm32wg990-pac-0.1.0 (c (n "efm32wg990-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1j98kw3wam7b53qsfhnrfnrd2aq4lla6c29n1k7wz2zvmywqcj3b") (f (quote (("rt" "cortex-m-rt/device"))))))

