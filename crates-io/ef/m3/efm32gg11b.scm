(define-module (crates-io ef m3 efm32gg11b) #:use-module (crates-io))

(define-public crate-efm32gg11b-0.1.0 (c (n "efm32gg11b") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "01br3jgz9rrn5xwd0yrif6r49s6ip0gws1dwsr7w3zbrwaw8lchn") (f (quote (("rt" "cortex-m-rt/device"))))))

