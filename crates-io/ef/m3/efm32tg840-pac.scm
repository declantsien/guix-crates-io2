(define-module (crates-io ef m3 efm32tg840-pac) #:use-module (crates-io))

(define-public crate-efm32tg840-pac-0.1.0 (c (n "efm32tg840-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0m75641gx1aj2g242wh5sfq1z7kg4zv7ffcnblwx3fa9svf7pg67") (f (quote (("rt" "cortex-m-rt/device"))))))

