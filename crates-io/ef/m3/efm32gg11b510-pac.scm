(define-module (crates-io ef m3 efm32gg11b510-pac) #:use-module (crates-io))

(define-public crate-efm32gg11b510-pac-0.1.0 (c (n "efm32gg11b510-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "08f9acvcxcy0xnircgvb8wqz4vs0qdav8mxw119mg7had5fkw3f7") (f (quote (("rt" "cortex-m-rt/device"))))))

