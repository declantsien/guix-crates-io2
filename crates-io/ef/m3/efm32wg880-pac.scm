(define-module (crates-io ef m3 efm32wg880-pac) #:use-module (crates-io))

(define-public crate-efm32wg880-pac-0.1.0 (c (n "efm32wg880-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1jv1gh8abk8qgg20ixwjwr2757nijpri1b6gmfn9c52nrjx8lgvp") (f (quote (("rt" "cortex-m-rt/device"))))))

