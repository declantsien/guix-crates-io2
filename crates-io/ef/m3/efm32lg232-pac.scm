(define-module (crates-io ef m3 efm32lg232-pac) #:use-module (crates-io))

(define-public crate-efm32lg232-pac-0.1.0 (c (n "efm32lg232-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0pdb5kipgkdjmcq1dk4ml7030ddkzanimgb522clly25r3xlmv5r") (f (quote (("rt" "cortex-m-rt/device"))))))

