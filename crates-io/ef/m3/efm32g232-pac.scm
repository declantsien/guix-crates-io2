(define-module (crates-io ef m3 efm32g232-pac) #:use-module (crates-io))

(define-public crate-efm32g232-pac-0.1.0 (c (n "efm32g232-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0wxikkqg6qajh6h0lcl7y42f7iwaffv933xviq9z51vyyb0238p3") (f (quote (("rt" "cortex-m-rt/device"))))))

