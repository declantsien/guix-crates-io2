(define-module (crates-io ef m3 efm32pg12-pac) #:use-module (crates-io))

(define-public crate-efm32pg12-pac-0.1.0 (c (n "efm32pg12-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "108qcxf1c0xnvcxraapb79k7yy4xb21lcp470vyh4zabnvnz4zly") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-efm32pg12-pac-0.2.0 (c (n "efm32pg12-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1gygc3rdk5x7zjrzqidji8xjbvy6knz8q5mbyxvlacf1z7wkxcra") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-efm32pg12-pac-0.2.1 (c (n "efm32pg12-pac") (v "0.2.1") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0zvd2nm0fr0pjapgma9h5i1mvd5nnq5953vcah4181pq1s0w7m63") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-efm32pg12-pac-0.3.0 (c (n "efm32pg12-pac") (v "0.3.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0qb22fpcc9qx2w2h7jz8z1gijw6k90vw0hmcak8b0pnn6k63y4cp") (f (quote (("rt" "cortex-m-rt/device"))))))

