(define-module (crates-io ef m3 efm32hg222-pac) #:use-module (crates-io))

(define-public crate-efm32hg222-pac-0.1.0 (c (n "efm32hg222-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1nba65x03hxf21rg04ndwjldq1d6py1ragk4n75hdxa9jwgvdkj6") (f (quote (("rt" "cortex-m-rt/device"))))))

