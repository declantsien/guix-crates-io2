(define-module (crates-io ef m3 efm32g880-pac) #:use-module (crates-io))

(define-public crate-efm32g880-pac-0.1.0 (c (n "efm32g880-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0s5y6gpza8qsbh5f4myl8d8rb4h6md8f9gxq7y7394l0i8mgxwk2") (f (quote (("rt" "cortex-m-rt/device"))))))

