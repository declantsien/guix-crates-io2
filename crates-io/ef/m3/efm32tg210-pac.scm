(define-module (crates-io ef m3 efm32tg210-pac) #:use-module (crates-io))

(define-public crate-efm32tg210-pac-0.1.0 (c (n "efm32tg210-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1p2084abl6v0sb1lr515vg9m614ylzlpmnf90zybijsg586b20zw") (f (quote (("rt" "cortex-m-rt/device"))))))

