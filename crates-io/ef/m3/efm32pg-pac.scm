(define-module (crates-io ef m3 efm32pg-pac) #:use-module (crates-io))

(define-public crate-efm32pg-pac-0.1.1 (c (n "efm32pg-pac") (v "0.1.1") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1iqplsmj89ds3zgyv8p3n37q1rlbgqip0k903zjl2hi7vnwfffcp") (f (quote (("rt" "cortex-m-rt/device") ("efm32pg1b200") ("efm32pg1b100") ("efm32pg12b500") ("default" "rt"))))))

(define-public crate-efm32pg-pac-0.1.2 (c (n "efm32pg-pac") (v "0.1.2") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "~1") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "~1") (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0pn0bkbibj0819jhj0c183rj1x3v8vvvlw8d0x35s2wwib9d3dbz") (f (quote (("rt" "cortex-m-rt/device") ("efm32pg1b200") ("efm32pg1b100") ("efm32pg12b500") ("default" "rt"))))))

(define-public crate-efm32pg-pac-0.1.3 (c (n "efm32pg-pac") (v "0.1.3") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "~1") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "~1") (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0zxs0ncjjfhm9573prc434209768kz8yzaamghhnpxhszifzdkp5") (f (quote (("rt" "cortex-m-rt/device") ("efm32pg1b200") ("efm32pg1b100") ("efm32pg12b500") ("default" "rt"))))))

(define-public crate-efm32pg-pac-0.1.4 (c (n "efm32pg-pac") (v "0.1.4") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "~1") (o #t) (d #t) (k 0)) (d (n "portable-atomic") (r "~1") (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "12livpmmgr8x22sb2l4l57zh10hb7b79zzlgkd07l1zw7fs444hm") (f (quote (("rt" "cortex-m-rt/device") ("efm32pg1b200") ("efm32pg1b100") ("efm32pg12b500") ("default" "rt")))) (s 2) (e (quote (("critical-section" "dep:critical-section"))))))

