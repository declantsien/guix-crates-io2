(define-module (crates-io ef m3 efm32lg390-pac) #:use-module (crates-io))

(define-public crate-efm32lg390-pac-0.1.0 (c (n "efm32lg390-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "19y6czaxzg6rkqw1clclxmch2cpji0wyh5avgng7nw78gimgxxwb") (f (quote (("rt" "cortex-m-rt/device"))))))

