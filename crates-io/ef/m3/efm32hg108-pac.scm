(define-module (crates-io ef m3 efm32hg108-pac) #:use-module (crates-io))

(define-public crate-efm32hg108-pac-0.1.0 (c (n "efm32hg108-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "01z40pzfw2fszzcyxh97fwiqmqs2jlp67g67038g1m966qx145vc") (f (quote (("rt" "cortex-m-rt/device"))))))

