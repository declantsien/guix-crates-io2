(define-module (crates-io ef m3 efm32gg990) #:use-module (crates-io))

(define-public crate-efm32gg990-0.1.0 (c (n "efm32gg990") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1gv5hl97s22dvqrwvd6j6fymcdvw707w58gw8nymhm2c0hcrs4i1") (f (quote (("rt" "cortex-m-rt")))) (y #t)))

(define-public crate-efm32gg990-0.1.1 (c (n "efm32gg990") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1awj0yfg0c5g693gdqyjm1zyw12hq4fg7i9j5vcksv2j4kwd8r57") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-efm32gg990-0.1.2 (c (n "efm32gg990") (v "0.1.2") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0j0w3rvrk369915al40397zhzwjh3anhpfa61s2j4agn7ldp8sls") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-efm32gg990-0.1.3 (c (n "efm32gg990") (v "0.1.3") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.2") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.13") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0gcywh8ikp7zqfkyz2jdw7mjjmnwlmiqgbw7vy4fvy6jfbhapkw4") (f (quote (("rt" "cortex-m-rt"))))))

