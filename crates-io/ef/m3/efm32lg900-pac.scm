(define-module (crates-io ef m3 efm32lg900-pac) #:use-module (crates-io))

(define-public crate-efm32lg900-pac-0.1.0 (c (n "efm32lg900-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1ghz920h85nz24sx227gl906cyqx8z4w8hvdm1cm9jqsb9lqsfds") (f (quote (("rt" "cortex-m-rt/device"))))))

