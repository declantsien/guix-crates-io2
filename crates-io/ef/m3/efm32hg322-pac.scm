(define-module (crates-io ef m3 efm32hg322-pac) #:use-module (crates-io))

(define-public crate-efm32hg322-pac-0.1.0 (c (n "efm32hg322-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1x8scvq0mfqj04dm38sajg0n04q8cb2y7czd78nhyvxsas43pkv4") (f (quote (("rt" "cortex-m-rt/device"))))))

