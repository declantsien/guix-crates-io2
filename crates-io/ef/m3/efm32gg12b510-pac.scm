(define-module (crates-io ef m3 efm32gg12b510-pac) #:use-module (crates-io))

(define-public crate-efm32gg12b510-pac-0.1.0 (c (n "efm32gg12b510-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0l4j7330k2whz65b480j6d3bavfkpj660z7bh10kpmy897bk68vv") (f (quote (("rt" "cortex-m-rt/device"))))))

