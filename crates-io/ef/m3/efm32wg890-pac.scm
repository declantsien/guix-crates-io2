(define-module (crates-io ef m3 efm32wg890-pac) #:use-module (crates-io))

(define-public crate-efm32wg890-pac-0.1.0 (c (n "efm32wg890-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "1jl6i4hljy98aj40d8dxq0a23qxwnw078hjv91c1kxdmcbsnxw9v") (f (quote (("rt" "cortex-m-rt/device"))))))

