(define-module (crates-io ef m3 efm32jg12b500-pac) #:use-module (crates-io))

(define-public crate-efm32jg12b500-pac-0.1.0 (c (n "efm32jg12b500-pac") (v "0.1.0") (d (list (d (n "cortex-m") (r "~0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.7") (o #t) (d #t) (k 0)) (d (n "vcell") (r "~0.1") (d #t) (k 0)))) (h "0jk3drhy5ccz83svjxg31ia5njnf455l5c1rpmxfwfpyc7p7j0fg") (f (quote (("rt" "cortex-m-rt/device"))))))

