(define-module (crates-io ef m3 efm32gg12b810) #:use-module (crates-io))

(define-public crate-efm32gg12b810-0.1.0 (c (n "efm32gg12b810") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.11") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1srjjic204i41qq5952cpjz6mnckikz8af6x8xpvg91l3g4szbvh") (f (quote (("rt" "cortex-m-rt/device"))))))

