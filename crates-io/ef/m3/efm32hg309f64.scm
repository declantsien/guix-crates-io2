(define-module (crates-io ef m3 efm32hg309f64) #:use-module (crates-io))

(define-public crate-efm32hg309f64-0.1.0 (c (n "efm32hg309f64") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "cortex-m") (r "^0.6.1") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "112yfk4xd1ks1q3bikbx6p79zqzv80sb7hisn5pjm88dpp5fbi1b") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

