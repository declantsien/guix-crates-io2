(define-module (crates-io ef m3 efm32hg222f64) #:use-module (crates-io))

(define-public crate-efm32hg222f64-0.1.0 (c (n "efm32hg222f64") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1bzrmnbsilm942kdpj82nmm12sy4x4fh22pgwajn4pqrwz9bvjbp") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-efm32hg222f64-0.1.1 (c (n "efm32hg222f64") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0fg8sqxm0wlw7sqxk7bvp0v6fkybvnkcxjmj1w57mdm64kc64gqr") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-efm32hg222f64-0.2.0 (c (n "efm32hg222f64") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.1.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0rkln87isndqlyxw0my1bljsflj7r2nrqp7bv23gs4d00c2iyxk6") (f (quote (("rt" "cortex-m-rt"))))))

