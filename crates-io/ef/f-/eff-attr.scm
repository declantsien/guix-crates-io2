(define-module (crates-io ef f- eff-attr) #:use-module (crates-io))

(define-public crate-eff-attr-0.0.2 (c (n "eff-attr") (v "0.0.2") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0dgb6vlyx38v5czzbgf2zpck2vbh3wjwf0qs0vky1r7psdg4wxzh")))

(define-public crate-eff-attr-0.0.3 (c (n "eff-attr") (v "0.0.3") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "13fb4crvs21pkpan8lnsl1p9kwkry0mn5f6nvb5h3ljv0559pd78")))

(define-public crate-eff-attr-0.0.4 (c (n "eff-attr") (v "0.0.4") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0bpzj9bf0dsxsj87x5i14lhxcvx7byvjw3nxxa30hrxxlaik8znh")))

(define-public crate-eff-attr-0.0.5 (c (n "eff-attr") (v "0.0.5") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "195vdf6cbbqfxrqzvziqr0wg0fyq5nbv1c74wbpfsaa8nx5sa5pz")))

(define-public crate-eff-attr-0.0.6 (c (n "eff-attr") (v "0.0.6") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1mj4dyjq7ylf68pn6nd88smjmria2f5s060m87l6a2n1f98j6g7s")))

(define-public crate-eff-attr-0.0.7 (c (n "eff-attr") (v "0.0.7") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0mkmgpi9cyg75xzp9r7q3qamasch5ylp7i5g0svmbm12rwf5rxnp")))

(define-public crate-eff-attr-0.0.8 (c (n "eff-attr") (v "0.0.8") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0hz9b6185dnqv3cgxwd7rr4mm2wi8gwkzz6sbyx8cf83q3746mf8")))

(define-public crate-eff-attr-0.1.0 (c (n "eff-attr") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("full"))) (d #t) (k 0)))) (h "0igg3zp371g7rpapr0hwyx48ll5ajcvam3kqg8ha6wi3x91fmvyj")))

