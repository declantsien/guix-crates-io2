(define-module (crates-io ef f- eff-wordlist) #:use-module (crates-io))

(define-public crate-eff-wordlist-0.1.0 (c (n "eff-wordlist") (v "0.1.0") (h "09h54w4hm60drwaz4mis5shxza1f5k7359aiwvq0pl2w0rdi4qfg")))

(define-public crate-eff-wordlist-0.1.1 (c (n "eff-wordlist") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.4.10") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1hgvc848wvg7wyl27rahzrj3jfjmb85k5naf31d082qx2k3mid9r")))

(define-public crate-eff-wordlist-0.1.2 (c (n "eff-wordlist") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.4.10") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "04rzknnr3hnpvmayp6xs02an3x7kmik49ckqkxl61kdsis3saib2")))

(define-public crate-eff-wordlist-1.0.0 (c (n "eff-wordlist") (v "1.0.0") (d (list (d (n "arrayvec") (r "^0.4.10") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0qlwf3wg0rxikhqkdhib8zpvxz1dac2vskvr8macn60higxk764j")))

(define-public crate-eff-wordlist-1.0.1 (c (n "eff-wordlist") (v "1.0.1") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0mm8mzk4hvh5dkgz2jspadmv17pny13fs9rhkr8j88izy5482y31")))

(define-public crate-eff-wordlist-1.0.2 (c (n "eff-wordlist") (v "1.0.2") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "092cngjpdg9zhzvwx8fykr395sk6lclxvph69m29yxkbi9cc4v12")))

(define-public crate-eff-wordlist-1.0.3 (c (n "eff-wordlist") (v "1.0.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0av1n35vpwyvlhaci8yfvwk529ygbq3lnybhw5q5yg7mj9pwi2yd")))

