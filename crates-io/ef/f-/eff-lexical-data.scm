(define-module (crates-io ef f- eff-lexical-data) #:use-module (crates-io))

(define-public crate-eff-lexical-data-1.0.0-rc.1 (c (n "eff-lexical-data") (v "1.0.0-rc.1") (h "1z1949p4namv2v71jklg3ap99jzlfg8i02w9r2c69sw22zhkch66")))

(define-public crate-eff-lexical-data-1.0.0 (c (n "eff-lexical-data") (v "1.0.0") (h "0vk7caxvr8kbj08adbv57zpkwylqdzzfcixccg50gpjxqqaqgva9")))

