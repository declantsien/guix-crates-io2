(define-module (crates-io uv ge uvgen) #:use-module (crates-io))

(define-public crate-uvgen-0.1.0 (c (n "uvgen") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rectutils") (r "^0.1.0") (d #t) (k 0)))) (h "06vsn51xp5dxlfblprjdz7irfjjnsvc3mwc5hy7dp6bzfcfnxzlh") (r "1.72")))

