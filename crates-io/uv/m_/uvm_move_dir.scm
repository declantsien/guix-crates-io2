(define-module (crates-io uv m_ uvm_move_dir) #:use-module (crates-io))

(define-public crate-uvm_move_dir-0.1.0 (c (n "uvm_move_dir") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1y3vnmigbqapknl3cp2s799m6bcbyw75k0mb8z4qrhv9zr4vqd98")))

(define-public crate-uvm_move_dir-0.1.1 (c (n "uvm_move_dir") (v "0.1.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "widestring") (r "^0.4.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1jyrb72x75bvyf54rv6nbfq2n0d7rcm1h8yfc0bk1ynyn51h7kyi")))

