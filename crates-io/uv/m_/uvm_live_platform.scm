(define-module (crates-io uv m_ uvm_live_platform) #:use-module (crates-io))

(define-public crate-uvm_live_platform-0.1.0 (c (n "uvm_live_platform") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "indoc") (r "^2.0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("serde_json" "blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "typed-builder") (r "^0.15.2") (d #t) (k 0)))) (h "1xrmlfisq1ix4vwi00bgnhmmbhqi11dbimppsakx0hp1cvjn6gkb")))

(define-public crate-uvm_live_platform-0.1.1 (c (n "uvm_live_platform") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "indoc") (r "^2.0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("serde_json" "blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "typed-builder") (r "^0.15.2") (d #t) (k 0)))) (h "13azd43v96zw66qbg26l6754b6nppa63vm9mf5inzkmhypw97d70")))

