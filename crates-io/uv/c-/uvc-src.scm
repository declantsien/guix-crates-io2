(define-module (crates-io uv c- uvc-src) #:use-module (crates-io))

(define-public crate-uvc-src-0.1.0 (c (n "uvc-src") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "05vkva9hiq6i3b09v6dixihzrbjm1yf96m14jwblxsyjxf44cr0x")))

(define-public crate-uvc-src-0.1.1 (c (n "uvc-src") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "0ip6gzzdxynda0r9f1qyxxzbqdxfy09igjg0hhwvkc22101frd1n")))

(define-public crate-uvc-src-0.1.2 (c (n "uvc-src") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "1kj02dk0b3dfgybd06nakaxzw1yfbapdx2hq0lnvm4l69ql2fgj1")))

(define-public crate-uvc-src-0.2.0 (c (n "uvc-src") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "mozjpeg-sys") (r "^0.10.4") (k 0)))) (h "07mnyrvz1hl8cngb9z9kb4j1w5ws8ly8khdwh9p0rm7awn9irxrl")))

(define-public crate-uvc-src-0.2.1 (c (n "uvc-src") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "libusb-sys") (r "^0.4.2") (d #t) (k 0) (p "libusb1-sys")) (d (n "mozjpeg-sys") (r "^0.10.4") (o #t) (k 0)))) (h "0dfly6pixqwhfvwwczf17fp7rnkx8flh05rw29f8z15dnrhzpj7q") (f (quote (("jpeg" "mozjpeg-sys")))) (l "uvcsrc")))

