(define-module (crates-io mx _t mx_tool) #:use-module (crates-io))

(define-public crate-mx_tool-0.0.2 (c (n "mx_tool") (v "0.0.2") (h "0l9ncf24rmgn4fq1ygshcj6lk1svmhc0pxiz758vjrzry7dyqhq5")))

(define-public crate-mx_tool-0.0.3 (c (n "mx_tool") (v "0.0.3") (d (list (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "16zi4lgmlq6gc9r22f452rpmbl6iqsdxv9rbwg97h3iy61ky2q67")))

(define-public crate-mx_tool-0.0.4 (c (n "mx_tool") (v "0.0.4") (d (list (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "144gvhv1dy52ffy0pzqzqfcchl1bq1mqncwapjsrpnkkviddcxqm")))

(define-public crate-mx_tool-0.0.5 (c (n "mx_tool") (v "0.0.5") (d (list (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "0d0yyvg8cfdvizf2ma46pac7rrq5lpyaw2kwd98nlfg14x3jf0g5")))

(define-public crate-mx_tool-0.0.6 (c (n "mx_tool") (v "0.0.6") (d (list (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "13108cvixkkypbmnhqn1w2iay9w2b2lk3i807lbsbsm1zax2d0im")))

