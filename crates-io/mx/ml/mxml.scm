(define-module (crates-io mx ml mxml) #:use-module (crates-io))

(define-public crate-mxml-0.0.1 (c (n "mxml") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1m4lzkifybd3i1vcgn39x62v3hiz8dc7cklswhwnndwb7m8gar2q")))

(define-public crate-mxml-0.0.2 (c (n "mxml") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00qkw2nkhm457pal8030cnrqgks2p2vn59v9clq8799ya319jzcv")))

(define-public crate-mxml-0.1.0 (c (n "mxml") (v "0.1.0") (d (list (d (n "mxml_dep") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16l2dz9sz4maaqs11nf1z56g8ra43ic9qc0mqq4a0lv0cq2pvdqr") (y #t)))

(define-public crate-mxml-0.1.1 (c (n "mxml") (v "0.1.1") (d (list (d (n "mxml_dep") (r "^0.1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1b64jwjn9vyi4n63sh3sr0lkl2hrs7pvva65s7pgwcwb9r8v57ns") (y #t)))

(define-public crate-mxml-0.1.2 (c (n "mxml") (v "0.1.2") (d (list (d (n "mxml_dep") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lkn3gpjd4v42v6w7bbfc0wixns4kxrmgx5l6cr1h7r8ilhzygbb") (y #t)))

