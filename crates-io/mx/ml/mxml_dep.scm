(define-module (crates-io mx ml mxml_dep) #:use-module (crates-io))

(define-public crate-mxml_dep-0.0.1 (c (n "mxml_dep") (v "0.0.1") (h "0a5l8lbg9sgrdfikzncqk5ybi9v5za4gp5himv0r8540r9fsjvw0") (y #t)))

(define-public crate-mxml_dep-0.1.0 (c (n "mxml_dep") (v "0.1.0") (d (list (d (n "html_parser") (r "^0.4.2") (d #t) (k 0)))) (h "060wgwal8nxk452w50jz8lba8fsagvrkjchxc0dr7k99mcv5vk7p") (y #t)))

(define-public crate-mxml_dep-0.1.1 (c (n "mxml_dep") (v "0.1.1") (d (list (d (n "html_parser") (r "^0.4.2") (d #t) (k 0)))) (h "0r0pm4md6spp8l1whzj8b28b8r2cmi33p1gfc1nalrazrgimny4g") (y #t)))

(define-public crate-mxml_dep-0.1.2 (c (n "mxml_dep") (v "0.1.2") (d (list (d (n "html_parser") (r "^0.4.2") (d #t) (k 0)))) (h "1paj2dj3xvxsf1s21297bax1iv8wsf87wpjpgfrzbj19jqix1ggr") (y #t)))

(define-public crate-mxml_dep-0.1.3 (c (n "mxml_dep") (v "0.1.3") (d (list (d (n "html_parser") (r "^0.4.2") (d #t) (k 0)))) (h "1daghy3hi8ymnyfw5zs02i0xa1mxzis31nk2kgy543z0xa7z67d1") (y #t)))

