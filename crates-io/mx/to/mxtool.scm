(define-module (crates-io mx to mxtool) #:use-module (crates-io))

(define-public crate-mxtool-0.0.1 (c (n "mxtool") (v "0.0.1") (h "187wmymqdpwplxdg6rg7jiid3rdpxv9q9hxpq4myy9w44qhil5m3")))

(define-public crate-mxtool-0.0.2 (c (n "mxtool") (v "0.0.2") (h "0b6vf0n5lcl1b07w5p8jm8r9yvha5n142vca6nbmlgamqjqskzay")))

