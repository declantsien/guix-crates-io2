(define-module (crates-io mx o_ mxo_env_logger) #:use-module (crates-io))

(define-public crate-mxo_env_logger-0.1.0 (c (n "mxo_env_logger") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "0xc432g49r2f6k8yggw8gh5r7kwxkiszy6vvgl33j8gz1b9f91rd")))

