(define-module (crates-io mx dn mxdns) #:use-module (crates-io))

(define-public crate-mxdns-0.1.0 (c (n "mxdns") (v "0.1.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "resolv-conf") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "trust-dns") (r "^0.15") (d #t) (k 0)))) (h "1sds7z747l5sq7v46zk87v4a39an494pp36a5pc394yjd9f47j1m") (f (quote (("no-network-tests"))))))

(define-public crate-mxdns-0.2.0 (c (n "mxdns") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "resolv-conf") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "trust-dns") (r "= 0.15.0") (d #t) (k 0)))) (h "103r1apny5jap3lwqb9vq02y60w2hn786fsl52fp9g3mi7f5af6l") (f (quote (("no-network-tests"))))))

(define-public crate-mxdns-0.2.1 (c (n "mxdns") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "resolv-conf") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "trust-dns") (r "^0.15") (d #t) (k 0)))) (h "1cvjmcdhjq40dvngc928ga25zcf84rvzqz1dndysjpzj89w25x23") (f (quote (("no-network-tests"))))))

(define-public crate-mxdns-0.2.2 (c (n "mxdns") (v "0.2.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "resolv-conf") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-udp") (r "^0.1") (d #t) (k 0)) (d (n "trust-dns") (r "^0.17") (d #t) (k 0)))) (h "08na0s2d0z8s7j2ljgbq135zxcld9hg42k0akhdh2wf4x3dikrl5") (f (quote (("no-network-tests"))))))

(define-public crate-mxdns-0.3.0 (c (n "mxdns") (v "0.3.0") (d (list (d (n "display_bytes") (r "^0.2") (d #t) (k 2)) (d (n "dnsclientx") (r "^0.3") (f (quote ("smol-async"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.6") (d #t) (k 0)) (d (n "resolv-conf") (r "^0.7") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0niwwabgmkjnr1g795g3rvhf13irqycnx50xmvchblfmaacq38nx")))

(define-public crate-mxdns-0.4.0 (c (n "mxdns") (v "0.4.0") (d (list (d (n "display_bytes") (r "^0.2") (d #t) (k 2)) (d (n "dnsclientx") (r "^0.4") (f (quote ("smol-async"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "resolv-conf") (r "^0.7") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0minh7c6j1wyf1nwki0p6xjcmip9657jdp9sjf6hbz11705nq79c")))

(define-public crate-mxdns-0.4.1 (c (n "mxdns") (v "0.4.1") (d (list (d (n "display_bytes") (r "^0.2") (d #t) (k 2)) (d (n "dnsclientx") (r "^0.4") (f (quote ("smol-async"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "resolv-conf") (r "^0.7") (d #t) (k 0)) (d (n "smol") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0bbicx93scdihqp7hjwwy7s20751kl7mh724s6xsd1xcrw34lhrc")))

