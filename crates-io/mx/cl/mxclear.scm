(define-module (crates-io mx cl mxclear) #:use-module (crates-io))

(define-public crate-mxclear-0.1.0 (c (n "mxclear") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("env"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ckgz5lrnhgxnilspj69bcs8drc6lzb4ac35m00db2ldr6nnz302")))

