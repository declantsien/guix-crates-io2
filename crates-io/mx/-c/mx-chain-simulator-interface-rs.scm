(define-module (crates-io mx -c mx-chain-simulator-interface-rs) #:use-module (crates-io))

(define-public crate-mx-chain-simulator-interface-rs-0.0.1 (c (n "mx-chain-simulator-interface-rs") (v "0.0.1") (d (list (d (n "mx-chain-simulator-interface-config-rs") (r "^0.0.1") (d #t) (k 1)) (d (n "nix") (r "^0.28.0") (f (quote ("signal"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.27") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.2") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.11") (d #t) (k 0)))) (h "0dncyhml9153vkd6g5fghp8qb4riy2rzjfim6f6ycnkcc8y1rzyp")))

