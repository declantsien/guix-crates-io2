(define-module (crates-io jr ef jreflection) #:use-module (crates-io))

(define-public crate-jreflection-0.0.0-git (c (n "jreflection") (v "0.0.0-git") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "bugsalot") (r "^0.2.0") (d #t) (k 0)) (d (n "jimage") (r "^0.2.2") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "16nr534mcvs86s6g7mca1x76dri9478aiazysfrksvdd9cd5xv2q")))

(define-public crate-jreflection-0.0.11 (c (n "jreflection") (v "0.0.11") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "bugsalot") (r "^0.2.0") (d #t) (k 0)) (d (n "jimage") (r "^0.2.2") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "1zll4m3a1zj05vmix48sx68i3gwa19cmr4h7kd7cv7rc1zq3vnq0")))

