(define-module (crates-io jr ep jrep) #:use-module (crates-io))

(define-public crate-jrep-0.1.3 (c (n "jrep") (v "0.1.3") (d (list (d (n "atty") (r "~0.2.0") (d #t) (k 0)) (d (n "clap") (r "~2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "exitcode") (r "~1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "term") (r "^0.7.0") (d #t) (k 0)))) (h "1x0izwg5909l647jy32qa2lhmkk0irip0cwqj862jcqa0q2v85xh")))

