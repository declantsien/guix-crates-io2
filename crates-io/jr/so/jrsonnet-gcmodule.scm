(define-module (crates-io jr so jrsonnet-gcmodule) #:use-module (crates-io))

(define-public crate-jrsonnet-gcmodule-0.3.3 (c (n "jrsonnet-gcmodule") (v "0.3.3") (d (list (d (n "gcmodule_derive") (r "=0.3.3") (o #t) (d #t) (k 0) (p "jrsonnet-gcmodule-derive")) (d (n "parking_lot") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (k 2)))) (h "0hzd5nqa0bhgainyvnrjla4qp5cjfls03hmgz293qh39lxij3sp5") (f (quote (("testutil") ("sync" "parking_lot") ("nightly") ("derive" "gcmodule_derive") ("default" "derive" "sync") ("debug"))))))

(define-public crate-jrsonnet-gcmodule-0.3.4 (c (n "jrsonnet-gcmodule") (v "0.3.4") (d (list (d (n "jrsonnet-gcmodule-derive") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (k 2)))) (h "12n7r0xfbra0sx0aa1c8nilhxprhlc916w5kvvsy8qab5rby3m12") (f (quote (("testutil") ("sync" "parking_lot") ("nightly") ("derive" "jrsonnet-gcmodule-derive") ("default" "derive" "sync") ("debug"))))))

(define-public crate-jrsonnet-gcmodule-0.3.5 (c (n "jrsonnet-gcmodule") (v "0.3.5") (d (list (d (n "jrsonnet-gcmodule-derive") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (k 2)))) (h "0whzlx8s6v6b4k4zvsmifd4z8ynaw9a8qygkd6a2w0qkl2k3l91m") (f (quote (("testutil") ("sync" "parking_lot") ("nightly") ("derive" "jrsonnet-gcmodule-derive") ("default" "derive" "sync") ("debug"))))))

(define-public crate-jrsonnet-gcmodule-0.3.6 (c (n "jrsonnet-gcmodule") (v "0.3.6") (d (list (d (n "jrsonnet-gcmodule-derive") (r "^0.3.6") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (k 2)))) (h "0mvd31qk0dydbbq7gqnhznjp2ih95szwr64yc4cv9y57824vj7y1") (f (quote (("testutil") ("sync" "parking_lot") ("nightly") ("derive" "jrsonnet-gcmodule-derive") ("default" "derive" "sync") ("debug"))))))

(define-public crate-jrsonnet-gcmodule-0.3.7 (c (n "jrsonnet-gcmodule") (v "0.3.7") (d (list (d (n "jrsonnet-gcmodule-derive") (r "^0.3.7") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.3") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (k 2)))) (h "1j1qpjxsp6fclql1i0gr524f2jm0xparyi0dxsn06iabn9rm95s7") (f (quote (("testutil") ("sync" "parking_lot") ("nightly") ("derive" "jrsonnet-gcmodule-derive") ("default" "derive" "sync") ("debug"))))))

