(define-module (crates-io jr so jrsonnet-gc-derive) #:use-module (crates-io))

(define-public crate-jrsonnet-gc-derive-0.4.1 (c (n "jrsonnet-gc-derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "04idv6i1g499yd3nhl5pxvnj2vi90m4p4kfc0r7hajxnhz1skjxd")))

