(define-module (crates-io jr so jrsonnet-gc) #:use-module (crates-io))

(define-public crate-jrsonnet-gc-0.4.1 (c (n "jrsonnet-gc") (v "0.4.1") (d (list (d (n "jrsonnet-gc-derive") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "jrsonnet-gc-derive") (r "^0.4.1") (d #t) (k 2)))) (h "03bbxm2n40q7liin5k510jkqhdshr0gb4h3j86qgycn3ab22q57m") (f (quote (("unstable-stats") ("unstable-config") ("nightly") ("derive" "jrsonnet-gc-derive"))))))

(define-public crate-jrsonnet-gc-0.4.2 (c (n "jrsonnet-gc") (v "0.4.2") (d (list (d (n "jrsonnet-gc-derive") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "jrsonnet-gc-derive") (r "^0.4.1") (d #t) (k 2)))) (h "1plxj6fbkrbrf9fmirh09161wf8x7gq7m1xq7cvv25q1y318pnk8") (f (quote (("unstable-stats") ("unstable-config") ("nightly") ("derive" "jrsonnet-gc-derive"))))))

