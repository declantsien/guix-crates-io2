(define-module (crates-io jr so jrsonnet-types) #:use-module (crates-io))

(define-public crate-jrsonnet-types-0.3.6 (c (n "jrsonnet-types") (v "0.3.6") (d (list (d (n "peg") (r "^0.7.0") (d #t) (k 0)))) (h "04cbl9ch3i58pql72yirvc7ikdvg5xzswg9y6f6n7wv4nb3zhvxv")))

(define-public crate-jrsonnet-types-0.3.8 (c (n "jrsonnet-types") (v "0.3.8") (d (list (d (n "peg") (r "^0.7.0") (d #t) (k 0)))) (h "0ghmmssq06h5p9ik7gylmab59qwvdlqrzqbhhd3qjr4d1i0fl3rf")))

(define-public crate-jrsonnet-types-0.4.1 (c (n "jrsonnet-types") (v "0.4.1") (d (list (d (n "jrsonnet-gc") (r "^0.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "peg") (r "^0.7.0") (d #t) (k 0)))) (h "00635j039vpifwjvbm2l4c8r4whb6f5qvf9dnh422cjzw5ahhm5q")))

(define-public crate-jrsonnet-types-0.4.2 (c (n "jrsonnet-types") (v "0.4.2") (d (list (d (n "jrsonnet-gc") (r "^0.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "peg") (r "^0.7.0") (d #t) (k 0)))) (h "0p7ycng22vkva29cq15jp0d43kz62b8ixbqllmbz67kik7g3i6wh")))

(define-public crate-jrsonnet-types-0.5.0-pre6 (c (n "jrsonnet-types") (v "0.5.0-pre6") (d (list (d (n "jrsonnet-gcmodule") (r "^0.3.4") (d #t) (k 0)) (d (n "peg") (r "^0.8.1") (d #t) (k 0)))) (h "144igl2nv3wnjrd5bzaxmzpi23pmry666sas1mw2g60gqkr06m8p")))

(define-public crate-jrsonnet-types-0.5.0-pre7 (c (n "jrsonnet-types") (v "0.5.0-pre7") (d (list (d (n "jrsonnet-gcmodule") (r "^0.3.5") (d #t) (k 0)) (d (n "peg") (r "^0.8.1") (d #t) (k 0)))) (h "1b5b60mfk75g7vh0r23viscjsbskm94gnl9papgf436ycncblk74")))

(define-public crate-jrsonnet-types-0.5.0-pre8 (c (n "jrsonnet-types") (v "0.5.0-pre8") (d (list (d (n "jrsonnet-gcmodule") (r "^0.3.5") (d #t) (k 0)) (d (n "peg") (r "^0.8.1") (d #t) (k 0)))) (h "0h9khll2yrxvfqxvha9gnrgrpgfwikr84jhrzc1dw2qw1gf01gbn") (f (quote (("exp-bigint"))))))

(define-public crate-jrsonnet-types-0.5.0-pre9 (c (n "jrsonnet-types") (v "0.5.0-pre9") (d (list (d (n "jrsonnet-gcmodule") (r "^0.3.5") (d #t) (k 0)) (d (n "peg") (r "^0.8.1") (d #t) (k 0)))) (h "1p6mlsmlhd1vhwdkx6073679083cmhdp5l559cdn84q2yy126lx1") (f (quote (("exp-bigint"))))))

(define-public crate-jrsonnet-types-0.5.0-pre10 (c (n "jrsonnet-types") (v "0.5.0-pre10") (d (list (d (n "jrsonnet-gcmodule") (r "^0.3.5") (d #t) (k 0)) (d (n "peg") (r "^0.8.1") (d #t) (k 0)))) (h "1vkj705cdd0n9g0c2n0wlym2vcvyda4japv1cfj6a0fz9bddmvq4") (f (quote (("exp-bigint"))))))

(define-public crate-jrsonnet-types-0.5.0-pre11 (c (n "jrsonnet-types") (v "0.5.0-pre11") (d (list (d (n "jrsonnet-gcmodule") (r "^0.3.5") (d #t) (k 0)) (d (n "peg") (r "^0.8.1") (d #t) (k 0)))) (h "1pdnk039wp3kbw8hncs5w280sh9ng9as1k7v596pv9q0j5k2gnvi") (f (quote (("exp-bigint"))))))

(define-public crate-jrsonnet-types-0.5.0-pre91 (c (n "jrsonnet-types") (v "0.5.0-pre91") (d (list (d (n "jrsonnet-gcmodule") (r "^0.3.5") (d #t) (k 0)) (d (n "peg") (r "^0.8.1") (d #t) (k 0)))) (h "04n8w6la6x4h28rwnhcin8f20m9pkvib2cj0wm7idd73ggmk6fpq") (f (quote (("exp-bigint"))))))

(define-public crate-jrsonnet-types-0.5.0-pre94 (c (n "jrsonnet-types") (v "0.5.0-pre94") (d (list (d (n "jrsonnet-gcmodule") (r "^0.3.5") (d #t) (k 0)) (d (n "peg") (r "^0.8.1") (d #t) (k 0)))) (h "0p83lvk6xb76r0mkylm6i12xg5p06d1grg4f9hfnli7mfx4hvy8y") (f (quote (("exp-bigint"))))))

(define-public crate-jrsonnet-types-0.5.0-pre95 (c (n "jrsonnet-types") (v "0.5.0-pre95") (d (list (d (n "jrsonnet-gcmodule") (r "^0.3.6") (d #t) (k 0)) (d (n "peg") (r "^0.8.1") (d #t) (k 0)))) (h "15qd3rnghrajl3dgnn67nm8c8734csagkqhl6ymahwh14mcj4lk8") (f (quote (("exp-bigint"))))))

