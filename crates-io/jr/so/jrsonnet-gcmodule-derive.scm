(define-module (crates-io jr so jrsonnet-gcmodule-derive) #:use-module (crates-io))

(define-public crate-jrsonnet-gcmodule-derive-0.3.3 (c (n "jrsonnet-gcmodule-derive") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rkyxlcdgisp1kazrrhnqv4h2wb87s5hm3qwqdvyb18vn7y308b7")))

(define-public crate-jrsonnet-gcmodule-derive-0.3.4 (c (n "jrsonnet-gcmodule-derive") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "00hj1wp7zqz7fs6i349wph3a55vskryjl1mrnnc902lfsgvknbxf")))

(define-public crate-jrsonnet-gcmodule-derive-0.3.5 (c (n "jrsonnet-gcmodule-derive") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nkfx3g8kqiwiir3p1737nbyh2bvwszi6cfjpdxjbmblmvs6n84n")))

(define-public crate-jrsonnet-gcmodule-derive-0.3.6 (c (n "jrsonnet-gcmodule-derive") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xmccqgcaxgxbv32g64zapihm1ma69kwv0j4x1pchvx8gd5pgvkb")))

(define-public crate-jrsonnet-gcmodule-derive-0.3.7 (c (n "jrsonnet-gcmodule-derive") (v "0.3.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wlgg8783nmlwzgfd30p5xk1vwjh6gnjdzyy9fxybx9iczbx30j7")))

