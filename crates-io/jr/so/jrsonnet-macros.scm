(define-module (crates-io jr so jrsonnet-macros) #:use-module (crates-io))

(define-public crate-jrsonnet-macros-0.5.0-pre6 (c (n "jrsonnet-macros") (v "0.5.0-pre6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13wd5vkqnl1s5jn43wx92hvi0bb3jx588nc2kw5k4829bl19hldi")))

(define-public crate-jrsonnet-macros-0.5.0-pre7 (c (n "jrsonnet-macros") (v "0.5.0-pre7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1a0zjf95cw09vkj7k1479smz2hv3vk742qayll5ksm2dlr75zghv")))

(define-public crate-jrsonnet-macros-0.5.0-pre8 (c (n "jrsonnet-macros") (v "0.5.0-pre8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vr076j6zz9g4nyxqp89k0k7mcq3ln8ih64y0315a9mk0g0hmwpg")))

(define-public crate-jrsonnet-macros-0.5.0-pre9 (c (n "jrsonnet-macros") (v "0.5.0-pre9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1c82sr33hfbab71lk68jix4gn7ic07qm2v6qbab0w7xsv7ias0db")))

(define-public crate-jrsonnet-macros-0.5.0-pre10 (c (n "jrsonnet-macros") (v "0.5.0-pre10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cz2xz9f476qka5l7548fax3pq1qvfs8i9w2cqsbbwjlszc1xg0j")))

(define-public crate-jrsonnet-macros-0.5.0-pre11 (c (n "jrsonnet-macros") (v "0.5.0-pre11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17sfagcqnzz8w1v85ljx2aa457jhgfzy165qhw5zww5cf3zi92cj")))

(define-public crate-jrsonnet-macros-0.5.0-pre91 (c (n "jrsonnet-macros") (v "0.5.0-pre91") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kg97v7f2iw6p8yxkc4rg76w2nppg88chya10flwp6wl6ppfvy60")))

(define-public crate-jrsonnet-macros-0.5.0-pre94 (c (n "jrsonnet-macros") (v "0.5.0-pre94") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00cmg4krrnj1k9x8sz5ynprb1c8ff3jq40vrd12irvjzzb090vjz")))

(define-public crate-jrsonnet-macros-0.5.0-pre95 (c (n "jrsonnet-macros") (v "0.5.0-pre95") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1x9paw5xg8j9zivfvz18bl000wdfpngjxplilzq94jadza4x5d74")))

