(define-module (crates-io jr in jrinx-abi) #:use-module (crates-io))

(define-public crate-jrinx-abi-0.1.0 (c (n "jrinx-abi") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "077wg4s6hlmbskqkpvvqicc4w6rj6n4fd8g808p3l5zyl662i898") (y #t)))

(define-public crate-jrinx-abi-0.1.1 (c (n "jrinx-abi") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "0ss90q9g19byv1cg2lfcrxji456sgm9p2zm220xq3z0yydc8iwxh")))

(define-public crate-jrinx-abi-0.1.2 (c (n "jrinx-abi") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "jrinx-apex") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1hrh0drpqgi10hipm63mkz7ng3rypg9d38dhvfwdyn8xrxpwfva7") (s 2) (e (quote (("sysfn" "dep:jrinx-apex"))))))

(define-public crate-jrinx-abi-0.1.3 (c (n "jrinx-abi") (v "0.1.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "jrinx-apex") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0ipgqkc69jwa4x9j0p3dda6svaqqf7pyj4s6i58q8zijpb4fk7dr") (s 2) (e (quote (("sysfn" "dep:jrinx-apex"))))))

