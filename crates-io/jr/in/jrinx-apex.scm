(define-module (crates-io jr in jrinx-apex) #:use-module (crates-io))

(define-public crate-jrinx-apex-0.1.0 (c (n "jrinx-apex") (v "0.1.0") (h "00cgzdyw166gjfsavsy25ilwakgk5ykmmkgkvhjadpahz60wlgqa")))

(define-public crate-jrinx-apex-0.2.0 (c (n "jrinx-apex") (v "0.2.0") (h "07s24wspg8z6dgjlfa1gv0q635b2abrw0lcjc409ac246z7snb5x")))

