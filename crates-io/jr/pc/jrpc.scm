(define-module (crates-io jr pc jrpc) #:use-module (crates-io))

(define-public crate-jrpc-0.0.1 (c (n "jrpc") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.40") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.40") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.15") (d #t) (k 0)) (d (n "std_prelude") (r "^0.2.12") (d #t) (k 0)))) (h "0krly8zvdy7h4h7y9w5m6i8g7dyriysr55lm4lcw67kh0i660xcj")))

(define-public crate-jrpc-0.2.0 (c (n "jrpc") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.40") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.40") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.15") (d #t) (k 0)) (d (n "std_prelude") (r "^0.2.12") (d #t) (k 0)))) (h "0sbr6c6ir60m73l3vcm0z86sby7h7f7hh30ahgcxin9fj97cddvy")))

(define-public crate-jrpc-0.2.1 (c (n "jrpc") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.40") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.40") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.15") (d #t) (k 0)) (d (n "std_prelude") (r "^0.2.12") (d #t) (k 0)))) (h "135n4d2ll8c5g4w45cxl1ngmrd0vnv6f1460v11jnjp9zwhqvwa0")))

(define-public crate-jrpc-0.2.2 (c (n "jrpc") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.40") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.40") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.15") (d #t) (k 0)) (d (n "std_prelude") (r "^0.2.12") (d #t) (k 0)))) (h "1ljk8i7w1p5r2d511sc617bj60xpjr51mzrxnjg702cblx0jm52h")))

(define-public crate-jrpc-0.3.0 (c (n "jrpc") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.40") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.40") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.15") (d #t) (k 0)) (d (n "std_prelude") (r "^0.2.12") (d #t) (k 0)))) (h "1hr3ymw3hyzwdgkvlh3rxnhsvdpzv2rslld8fj8999yakxbc3rfd")))

(define-public crate-jrpc-0.4.0 (c (n "jrpc") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.40") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.40") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.15") (d #t) (k 0)) (d (n "std_prelude") (r "^0.2.12") (d #t) (k 0)))) (h "1y7pr37jx3k8gpzsk3k1zm18gfk6hgfzv5r4d75pwc6v5jx9gais")))

(define-public crate-jrpc-0.4.1 (c (n "jrpc") (v "0.4.1") (d (list (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "std_prelude") (r "^0.2.12") (d #t) (k 0)))) (h "1d8xphkqs9gav45wbk5av20za3l0s3szi7pllmdkswdqx8r5qmbj")))

