(define-module (crates-io jr pc jrpc-macro) #:use-module (crates-io))

(define-public crate-jrpc-macro-1.0.0 (c (n "jrpc-macro") (v "1.0.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "parsing" "printing" "proc-macro" "visit"))) (d #t) (k 0)))) (h "11ajamwqida8axb4p3dw4z03kgbv5km5k5dk95v4c6nb2zxsw5w4")))

