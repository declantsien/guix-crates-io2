(define-module (crates-io jr us jrust) #:use-module (crates-io))

(define-public crate-jrust-0.1.0 (c (n "jrust") (v "0.1.0") (h "0zr4xb5lvydh70sbvai4wqdgwm875jyhmlpc0v7wkg50y3nmlmfz")))

(define-public crate-jrust-0.2.0 (c (n "jrust") (v "0.2.0") (h "19dl489gpmmbfw8gw6qfbv9p5rqbr35519ss9pl4vgisgbwgx33k")))

