(define-module (crates-io jr es jrest_hooks) #:use-module (crates-io))

(define-public crate-jrest_hooks-0.2.0 (c (n "jrest_hooks") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("extra-traits" "fold" "full"))) (d #t) (k 0)))) (h "05ygn02cq8psl0pb93p1yis1xrkdg50fp93rnr29r4jcbfqflprk") (y #t) (r "1.70.0")))

(define-public crate-jrest_hooks-0.2.1 (c (n "jrest_hooks") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("extra-traits" "fold" "full"))) (d #t) (k 0)))) (h "1cqvhcvldjhvwsz1lkpaqs13dcshfbv3n5mrsvrn6rhybzhvc506") (r "1.70.0")))

(define-public crate-jrest_hooks-0.2.2 (c (n "jrest_hooks") (v "0.2.2") (d (list (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("extra-traits" "fold" "full"))) (d #t) (k 0)))) (h "1q2rk9fwkxzz9gyrb0yhjjln9bjd9zrvv4cc3b4ipj39j16k9kcr") (r "1.70.0")))

(define-public crate-jrest_hooks-0.2.3 (c (n "jrest_hooks") (v "0.2.3") (d (list (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("extra-traits" "fold" "full"))) (d #t) (k 0)))) (h "0dznhlvnx3vgdxazvdx5pp1x1p81g8baa1nk595ahx3ypana7r0h") (r "1.70.0")))

