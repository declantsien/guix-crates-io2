(define-module (crates-io jr es jrest) #:use-module (crates-io))

(define-public crate-jrest-0.1.0 (c (n "jrest") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "182w1x45n8visgr5pq9bysqyzirbixry7g09lggh16zpbqhg1p2l") (r "1.70.0")))

(define-public crate-jrest-0.1.1 (c (n "jrest") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0ynivrj2wg7vfqghynsb111aszk4fj2c1465rhjv3m5q5wdl6ndf") (r "1.70.0")))

(define-public crate-jrest-0.1.2 (c (n "jrest") (v "0.1.2") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1s9dnj4s7pz5gcrcqcgbwcs13n148x48nm66lgz301i9x0402s0v") (r "1.70.0")))

(define-public crate-jrest-0.2.0 (c (n "jrest") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jrest_hooks") (r "=0.2.0") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("extra-traits" "fold" "full"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("process" "full"))) (d #t) (k 0)))) (h "092qc126yqi6cg457ar6262rb0m4wir9v41prgr7aaz5g8ay9mjp") (y #t) (r "1.70.0")))

(define-public crate-jrest-0.2.1 (c (n "jrest") (v "0.2.1") (d (list (d (n "jrest_hooks") (r "=0.2.1") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1sp3zwkzi4hmfv8zqdxvpmxn5874xv6xykp4qm5brizvwv4aa8wa") (r "1.70.0")))

(define-public crate-jrest-0.2.2 (c (n "jrest") (v "0.2.2") (d (list (d (n "jrest_hooks") (r "=0.2.2") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0n8q4vm86ywq6lv55g35lj86bgkvwjn0nma45z533kps7r2f8mlm") (r "1.70.0")))

(define-public crate-jrest-0.2.3 (c (n "jrest") (v "0.2.3") (d (list (d (n "jrest_hooks") (r "=0.2.3") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "171j1jxi8b684f8z25pxw4lyjljk3k15prckmxdvl2sa9fmcq9vb") (r "1.70.0")))

