(define-module (crates-io ev pk evpkdf) #:use-module (crates-io))

(define-public crate-evpkdf-0.1.0 (c (n "evpkdf") (v "0.1.0") (d (list (d (n "digest") (r "^0.8.1") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2.1") (d #t) (k 2)) (d (n "md-5") (r "^0.8.0") (d #t) (k 2)) (d (n "sha-1") (r "^0.8.2") (d #t) (k 2)))) (h "1mhk8gkhlkmf0q65y3s11kqif5rakpssl92zpc8bksj3sj2hxiv5")))

(define-public crate-evpkdf-0.1.1 (c (n "evpkdf") (v "0.1.1") (d (list (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.9") (d #t) (k 2)) (d (n "sha-1") (r "^0.9") (d #t) (k 2)))) (h "0nbkacgp4xval2j1bkdmq5x4rx09ma2ps1jww93ykim6a20vgkv3")))

(define-public crate-evpkdf-0.2.0 (c (n "evpkdf") (v "0.2.0") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.10") (d #t) (k 2)) (d (n "sha-1") (r "^0.10") (d #t) (k 2)))) (h "1ncqb5rshr2h1fqi1rc1kj7vxqkwr9k4954iy9c08m5lcqbngffz")))

