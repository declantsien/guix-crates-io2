(define-module (crates-io ev an evan-test-pallet) #:use-module (crates-io))

(define-public crate-evan-test-pallet-3.0.0 (c (n "evan-test-pallet") (v "3.0.0") (d (list (d (n "codec") (r "^2.0.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "frame-support") (r "^3.0.0") (k 0)) (d (n "frame-system") (r "^3.0.0") (k 0)) (d (n "serde") (r "^1.0.119") (d #t) (k 2)) (d (n "sp-core") (r "^3.0.0") (k 2)) (d (n "sp-io") (r "^3.0.0") (k 2)) (d (n "sp-runtime") (r "^3.0.0") (k 2)))) (h "0miisq82wva4mw5xiy3b2v0bpaffqz4jkdz7rscn967kn8ki7sps") (f (quote (("std" "codec/std" "frame-support/std" "frame-system/std") ("default" "std"))))))

