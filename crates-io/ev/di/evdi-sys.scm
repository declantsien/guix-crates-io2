(define-module (crates-io ev di evdi-sys) #:use-module (crates-io))

(define-public crate-evdi-sys-0.1.0 (c (n "evdi-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.66") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "1f35n9c5gfqmvi0gbkywiwi9g1xb23rkssk7y6k4fan0zkn4mll7")))

(define-public crate-evdi-sys-0.1.1 (c (n "evdi-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.66") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "16syjs71fi5xaailgsab8xw0jzxp71wy3qlqbaznh79fpbk1z6rp")))

(define-public crate-evdi-sys-0.1.2 (c (n "evdi-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.66") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "0mfbkjcwb7qa8bdgxfjb2nrwqg47bg7rxisql3yrim8p147ampb0")))

(define-public crate-evdi-sys-0.2.0 (c (n "evdi-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.66") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "1107651i86hzsyzi8jzh3v77f3crj70szsswa870sdx7xz5rcm5a")))

(define-public crate-evdi-sys-0.3.0 (c (n "evdi-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.66") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "0a8b36g9kqwwjqdwz2sjbmids58j5l2n3r3pz0myp6mq67lwrl6j")))

(define-public crate-evdi-sys-0.3.1 (c (n "evdi-sys") (v "0.3.1") (d (list (d (n "cc") (r "^1.0.66") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "1dvpyqwwcmd7b4iqrh5gpmh7l3sanyhmmcqrl8fmsimkzhrqly6p")))

(define-public crate-evdi-sys-0.4.0 (c (n "evdi-sys") (v "0.4.0") (d (list (d (n "cc") (r "^1.0.66") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "10xqkw01w0w5y3liihbqlm30afkcj4y97407l9gn2skwsmm9hwck")))

