(define-module (crates-io ev mo evmole) #:use-module (crates-io))

(define-public crate-evmole-0.3.1 (c (n "evmole") (v "0.3.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "ruint") (r "^1.11.1") (d #t) (k 0)))) (h "07z0wlni2j7v9jxhvma2fqzb04mg8b8gn8wfpyhq7vsqksad34pl")))

(define-public crate-evmole-0.3.2 (c (n "evmole") (v "0.3.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "ruint") (r "^1.11.1") (d #t) (k 0)))) (h "0agys0sbkwijl7jly47p26wprndhibc2f13n79n4ihrzy7y7vxcf")))

(define-public crate-evmole-0.3.3 (c (n "evmole") (v "0.3.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "ruint") (r "^1.11.1") (d #t) (k 0)))) (h "1zpmy8lwcdfhb0vxk6x82i1kziryqgm8yddx0yjwnsrh9jphaknd")))

