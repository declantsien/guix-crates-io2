(define-module (crates-io ev it evitable) #:use-module (crates-io))

(define-public crate-evitable-0.1.0 (c (n "evitable") (v "0.1.0") (d (list (d (n "evitable-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "01jbfwdichah7hf2k2pa8db3m6wjzi6s3mxnv8pjcgdalg3q7nin") (f (quote (("derive" "evitable-derive") ("default" "derive"))))))

(define-public crate-evitable-0.1.1 (c (n "evitable") (v "0.1.1") (d (list (d (n "evitable-derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "120kbpsjskrznxngzgvzmpaj1grj2ljag6iv6rz2z8paj10qf872") (f (quote (("derive" "evitable-derive") ("default" "derive"))))))

(define-public crate-evitable-0.2.0 (c (n "evitable") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "evitable-derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "09xcphg0ccimm451lq1qjcvclc7hdndri34x5hzgxyy7rn8a30xv") (f (quote (("derive" "evitable-derive") ("default" "derive"))))))

(define-public crate-evitable-0.3.0 (c (n "evitable") (v "0.3.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "evitable-derive") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "1szld8pssm2b9bicm4lk5zq2q4immd6zwds7i08lrayml9k5gxss") (f (quote (("derive" "evitable-derive") ("default" "derive"))))))

(define-public crate-evitable-0.3.1 (c (n "evitable") (v "0.3.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "evitable-derive") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "12a6nvcsn8zz012fvfysy9p58dgnw1ifmpfsimh41kdy31a6gh7d") (f (quote (("derive" "evitable-derive") ("default" "derive"))))))

(define-public crate-evitable-0.4.0 (c (n "evitable") (v "0.4.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "evitable-derive") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "07i7yx0blfgkkxzmr0zp082z5mmn796v22p12gv9djsc0g994fis") (f (quote (("derive" "evitable-derive") ("default" "derive"))))))

(define-public crate-evitable-0.5.0 (c (n "evitable") (v "0.5.0") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "evitable-derive") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "05nb17mbkgafpq2gsvpvyil8gpskhyll1nchg1rp9dh45w1jn4gv") (f (quote (("derive" "evitable-derive") ("default" "derive"))))))

