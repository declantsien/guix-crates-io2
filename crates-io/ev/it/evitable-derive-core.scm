(define-module (crates-io ev it evitable-derive-core) #:use-module (crates-io))

(define-public crate-evitable-derive-core-0.1.0 (c (n "evitable-derive-core") (v "0.1.0") (d (list (d (n "evitable-syn-meta-ext") (r "^0.1") (d #t) (k 0)) (d (n "ident_case") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fqfklir42mzpxfilfi610a12nh81i0a3bqpyhijik3b7lc7mwjb")))

(define-public crate-evitable-derive-core-0.1.1 (c (n "evitable-derive-core") (v "0.1.1") (d (list (d (n "evitable-syn-meta-ext") (r "^0.1.1") (d #t) (k 0)) (d (n "ident_case") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02i9r84mn181wjf4ssxb59ik9kdwcvkd2yrz5rlwm3bzrmj43jzh")))

(define-public crate-evitable-derive-core-0.2.0 (c (n "evitable-derive-core") (v "0.2.0") (d (list (d (n "evitable-syn-meta-ext") (r "^0.2.0") (d #t) (k 0)) (d (n "ident_case") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fbjd79z1mr5cd9zxv500732d7yc64aa49rack58kwrag5w5vbp2")))

(define-public crate-evitable-derive-core-0.3.0 (c (n "evitable-derive-core") (v "0.3.0") (d (list (d (n "evitable-syn-meta-ext") (r "^0.3.0") (d #t) (k 0)) (d (n "ident_case") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qxscfdjr23rqj3p2aydaa89dmqrjakqpgkchz8pgy8ysghy8y0a")))

(define-public crate-evitable-derive-core-0.4.0 (c (n "evitable-derive-core") (v "0.4.0") (d (list (d (n "evitable-syn-meta-ext") (r "^0.4.0") (d #t) (k 0)) (d (n "ident_case") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1q0wvrmxmrckp2j96fpcv3qx0kq6x0x78r3shfc9fqw2c9ymvqf8")))

(define-public crate-evitable-derive-core-0.5.0 (c (n "evitable-derive-core") (v "0.5.0") (d (list (d (n "evitable-syn-meta-ext") (r "^0.5.0") (d #t) (k 0)) (d (n "ident_case") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0kliixv2bc3knvvkv1diy3fm1qmy9jlla4j8ynpsyl433hninc51")))

