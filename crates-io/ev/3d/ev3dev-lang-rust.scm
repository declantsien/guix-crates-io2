(define-module (crates-io ev #{3d}# ev3dev-lang-rust) #:use-module (crates-io))

(define-public crate-ev3dev-lang-rust-0.1.0 (c (n "ev3dev-lang-rust") (v "0.1.0") (h "0nwjzjl01bx0x7d6fr8jn7j4791qnlpn3hgynn171fw6zb3rkzgb")))

(define-public crate-ev3dev-lang-rust-0.2.0 (c (n "ev3dev-lang-rust") (v "0.2.0") (h "1qr9yj5h9y00n4sq938zwgrzb685xpn4z25ajjcml412d4bdgzvk")))

(define-public crate-ev3dev-lang-rust-0.3.0 (c (n "ev3dev-lang-rust") (v "0.3.0") (h "1557m4mwjafzik191nqsmjslamam3a1im13zfw34ywrgia4bclm3")))

(define-public crate-ev3dev-lang-rust-0.4.0 (c (n "ev3dev-lang-rust") (v "0.4.0") (h "0v2xnfmyq37hwjy4wwamvwg6rhjhbzq4z0147hgg3h341bzpjvmc")))

(define-public crate-ev3dev-lang-rust-0.4.1 (c (n "ev3dev-lang-rust") (v "0.4.1") (h "1kpsj1hpp931rblixnl3d5rv279kiwwrx87sfyknqfyypnvn904z")))

(define-public crate-ev3dev-lang-rust-0.5.0 (c (n "ev3dev-lang-rust") (v "0.5.0") (d (list (d (n "ev3dev-lang-rust-derive") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ysbdlrd12aasq43qkf1zz6klg9xgan0i3s719ssgrqfqfk83znn")))

(define-public crate-ev3dev-lang-rust-0.5.1 (c (n "ev3dev-lang-rust") (v "0.5.1") (d (list (d (n "ev3dev-lang-rust-derive") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rv2cjmif64wdfpvhczk332b1qd96hma36rhmrlr7g7rnd8a1xpk")))

(define-public crate-ev3dev-lang-rust-0.6.0 (c (n "ev3dev-lang-rust") (v "0.6.0") (d (list (d (n "ev3dev-lang-rust-derive") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0bfikb34wkzhm06xl0iajrsv2kc7qcfmgi673sgc94mrl5rgn03m")))

(define-public crate-ev3dev-lang-rust-0.7.0 (c (n "ev3dev-lang-rust") (v "0.7.0") (d (list (d (n "ev3dev-lang-rust-derive") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xj3b05kwnqf2qw1ni2w31rglj4lqmbddrm3yyxn5n8nbjb74kyy")))

(define-public crate-ev3dev-lang-rust-0.8.0 (c (n "ev3dev-lang-rust") (v "0.8.0") (d (list (d (n "ev3dev-lang-rust-derive") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05xvhm1mf5mckl8m7j0s1cr59rqspmc3anska41zsr9kyfcrm943")))

(define-public crate-ev3dev-lang-rust-0.9.0 (c (n "ev3dev-lang-rust") (v "0.9.0") (d (list (d (n "ev3dev-lang-rust-derive") (r "^0.8") (d #t) (k 0)) (d (n "framebuffer") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23.8") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dm61dbqy62gzjxwflw3j672msadbjfwr2s1p8sl8kgdrfwnrafn") (f (quote (("screen" "framebuffer" "image"))))))

(define-public crate-ev3dev-lang-rust-0.9.1 (c (n "ev3dev-lang-rust") (v "0.9.1") (d (list (d (n "ev3dev-lang-rust-derive") (r "^0.8") (d #t) (k 0)) (d (n "framebuffer") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23.8") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03xydllj35ym5npd307hbazrszhidpmp6zphm4s3p2mllbqpvb49") (f (quote (("screen" "framebuffer" "image"))))))

(define-public crate-ev3dev-lang-rust-0.9.2 (c (n "ev3dev-lang-rust") (v "0.9.2") (d (list (d (n "ev3dev-lang-rust-derive") (r "^0.8") (d #t) (k 0)) (d (n "framebuffer") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23.8") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1apig44nrrapfkxklj1rqwynfbjh6qp7rcb5bqsxxgvvqlc3k70r") (f (quote (("screen" "framebuffer" "image"))))))

(define-public crate-ev3dev-lang-rust-0.9.3 (c (n "ev3dev-lang-rust") (v "0.9.3") (d (list (d (n "ev3dev-lang-rust-derive") (r "^0.8") (d #t) (k 0)) (d (n "framebuffer") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23.8") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hkvrqsx6pwk3fmq0b658w8bbrg4pzpgy01ffmjmdi1c33w9mzrx") (f (quote (("screen" "framebuffer" "image"))))))

(define-public crate-ev3dev-lang-rust-0.9.4 (c (n "ev3dev-lang-rust") (v "0.9.4") (d (list (d (n "ev3dev-lang-rust-derive") (r "^0.8") (d #t) (k 0)) (d (n "framebuffer") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23.8") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12rxism0myvh9g35rczh4hfza31dzifqi464bky62w6m2rxpw6n1") (f (quote (("screen" "framebuffer" "image"))))))

(define-public crate-ev3dev-lang-rust-0.9.5 (c (n "ev3dev-lang-rust") (v "0.9.5") (d (list (d (n "ev3dev-lang-rust-derive") (r "^0.8") (d #t) (k 0)) (d (n "framebuffer") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23.8") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bdcy98qk8mvrq01969cxc52m6k70f9ybmb30zng7m2mfpyw95kg") (f (quote (("screen" "framebuffer" "image"))))))

(define-public crate-ev3dev-lang-rust-0.9.6 (c (n "ev3dev-lang-rust") (v "0.9.6") (d (list (d (n "ev3dev-lang-rust-derive") (r "^0.8") (d #t) (k 0)) (d (n "framebuffer") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23.8") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mh142gq9ia2hgqa0zmg8dkzhb2lh6ia4p89hj9x8sm30yxfg7b0") (f (quote (("screen" "framebuffer" "image"))))))

(define-public crate-ev3dev-lang-rust-0.10.0 (c (n "ev3dev-lang-rust") (v "0.10.0") (d (list (d (n "ev3dev-lang-rust-derive") (r "^0.10") (d #t) (k 0)) (d (n "framebuffer") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23.8") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hliyncz66fcn5ygmwmnfrwbpnc5jws1qqls0lx35xv1rh14rybv") (f (quote (("screen" "framebuffer" "image"))))))

(define-public crate-ev3dev-lang-rust-0.10.1 (c (n "ev3dev-lang-rust") (v "0.10.1") (d (list (d (n "ev3dev-lang-rust-derive") (r "^0.10") (d #t) (k 0)) (d (n "framebuffer") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23.8") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0dqld814jsg6d0520v4jihid76dpbqs4apws6sr88ish27xzgvjh") (f (quote (("screen" "framebuffer" "image"))))))

(define-public crate-ev3dev-lang-rust-0.10.2 (c (n "ev3dev-lang-rust") (v "0.10.2") (d (list (d (n "ev3dev-lang-rust-derive") (r "^0.10") (d #t) (k 0)) (d (n "framebuffer") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23.8") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0s0lwvkjsbz7hcl09qxh08x1qcx52jjp0hfc9jdniahycl40y5gl") (f (quote (("screen" "framebuffer" "image"))))))

(define-public crate-ev3dev-lang-rust-0.11.0 (c (n "ev3dev-lang-rust") (v "0.11.0") (d (list (d (n "ev3dev-lang-rust-derive") (r "^0.10") (d #t) (k 0)) (d (n "framebuffer") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23.8") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1d9w1bsg37iqarabvj9mxc277fgxyh1dfljzpbm2j55rz7636xk1") (f (quote (("screen" "framebuffer" "image"))))))

(define-public crate-ev3dev-lang-rust-0.11.1 (c (n "ev3dev-lang-rust") (v "0.11.1") (d (list (d (n "ev3dev-lang-rust-derive") (r "^0.10") (d #t) (k 0)) (d (n "framebuffer") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "113jnzx3k3gr2aac6mskfxijsn5n6xdp2aa59mvmfln88jhnzcrk") (f (quote (("screen" "framebuffer" "image"))))))

(define-public crate-ev3dev-lang-rust-0.12.0 (c (n "ev3dev-lang-rust") (v "0.12.0") (d (list (d (n "ev3dev-lang-rust-derive") (r "^0.10") (d #t) (k 0)) (d (n "framebuffer") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xxirc2x87l88hjapn31c9snz9vncdfypi5igm6ariqzzpfy68zs") (f (quote (("screen" "framebuffer" "image"))))))

(define-public crate-ev3dev-lang-rust-0.12.1 (c (n "ev3dev-lang-rust") (v "0.12.1") (d (list (d (n "ev3dev-lang-rust-derive") (r "^0.10") (d #t) (k 0)) (d (n "framebuffer") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qk8537x76hqmrrjcnk8vd1hcfxf9xxy6s9kfmcbp3fj95wv8xfj") (f (quote (("screen" "framebuffer" "image"))))))

(define-public crate-ev3dev-lang-rust-0.13.0 (c (n "ev3dev-lang-rust") (v "0.13.0") (d (list (d (n "ev3dev-lang-rust-derive") (r "^0.10") (d #t) (k 0)) (d (n "framebuffer") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vikr8p7j4sbg7f3rxi1ir02s3yy8alxpxbr8x2mh5lf9dqk54q2") (f (quote (("screen" "framebuffer" "image") ("override-driver-path") ("ev3") ("default" "ev3") ("brickpi3") ("brickpi"))))))

(define-public crate-ev3dev-lang-rust-0.14.0 (c (n "ev3dev-lang-rust") (v "0.14.0") (d (list (d (n "ev3dev-lang-rust-derive") (r "^0.10") (d #t) (k 0)) (d (n "framebuffer") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "195pb0rp2nzjxg2d73znp9jzxwshgxc2kw0l9j5f7c6yz500636x") (f (quote (("screen" "framebuffer" "image") ("override-driver-path") ("ev3") ("default" "ev3") ("brickpi3") ("brickpi"))))))

