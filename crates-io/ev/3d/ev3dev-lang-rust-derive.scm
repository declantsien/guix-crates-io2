(define-module (crates-io ev #{3d}# ev3dev-lang-rust-derive) #:use-module (crates-io))

(define-public crate-ev3dev-lang-rust-derive-0.5.0 (c (n "ev3dev-lang-rust-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1db02g8vzxkmrjwv3ghgdxclcyp4klalkhm32yv8dwfzyszjvic0")))

(define-public crate-ev3dev-lang-rust-derive-0.8.0 (c (n "ev3dev-lang-rust-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1dc49k1j4ski2z9bgdyl1knjndnypc6bmvwd3093rq9slqbm893p")))

(define-public crate-ev3dev-lang-rust-derive-0.10.0 (c (n "ev3dev-lang-rust-derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19p992vgvc7qzdba1dcp1hk3vhx18wm6m2xhs258whg98daskmyx")))

