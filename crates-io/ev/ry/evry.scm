(define-module (crates-io ev ry evry) #:use-module (crates-io))

(define-public crate-evry-0.1.3 (c (n "evry") (v "0.1.3") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "08nj0zsgf0s0vjp0f6ywgm1mbzcbbpcia84waagzghv0p7fnfm2s")))

(define-public crate-evry-0.1.4 (c (n "evry") (v "0.1.4") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0b9dnd28p03xg7561hrjz35p41bcwpfs9qp6sc1cki4zr2hbb767")))

(define-public crate-evry-0.1.5 (c (n "evry") (v "0.1.5") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0209wi79xzwb40pdwngy92mhw8x7ky4h7zaxz25sp55rvpn2398f")))

(define-public crate-evry-0.1.6 (c (n "evry") (v "0.1.6") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0lr4b75v191d7p3gzbi9zh8zx8y72mzbjndawrv4xdcprxd73f02")))

(define-public crate-evry-0.1.7 (c (n "evry") (v "0.1.7") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "02mwrk5y338g4r5zv3fqv5w1zlvmfrzjgynpk4xrbqygss0ac2qm")))

(define-public crate-evry-0.1.8 (c (n "evry") (v "0.1.8") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "183pdkm9drz96zsmknhzymr8nh83yalk5jy2ynja4i8lmbj42kkn")))

(define-public crate-evry-0.1.9 (c (n "evry") (v "0.1.9") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "04mni3p43qliic83ji8094hzfwb7qgmw88n724fsi9v8livmafvw")))

(define-public crate-evry-0.1.10 (c (n "evry") (v "0.1.10") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1506xnvs3ry6g4q3w09jczjs0rnpf5vs9cmwla2rws9qb6zni9jj")))

(define-public crate-evry-0.2.0 (c (n "evry") (v "0.2.0") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1bsvhgrdz4q97jqcxys8hqi4yfvwdqann2zm0vj6v4w1j1zwjfjz")))

(define-public crate-evry-0.3.0 (c (n "evry") (v "0.3.0") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0wvirpc69wrzsk7al8k0dmkny86isbg28ilbv2v72zfhdk5akkg2")))

(define-public crate-evry-0.3.1 (c (n "evry") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "04vzlvahwh6cscpmha9nf98pai3d3h20sspvfi18lbm1vsyw7s4j")))

(define-public crate-evry-0.3.2 (c (n "evry") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0rnh7p4jc82iaxyd95zs3g0xkyvhwf8s5ji4v8y56hzy9wd8gvlz")))

(define-public crate-evry-0.3.3 (c (n "evry") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0lg64nbwhf6d583lgjc6dk1vgkk7b070b6wfa2pdi1g478amasji")))

(define-public crate-evry-0.3.4 (c (n "evry") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "03m9bl9xan48qdxkbmkyq6wnh0rw6kl6abx63pnfg1q1vqyscgqr")))

