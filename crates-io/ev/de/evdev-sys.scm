(define-module (crates-io ev de evdev-sys) #:use-module (crates-io))

(define-public crate-evdev-sys-0.0.1 (c (n "evdev-sys") (v "0.0.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1xax7753ybx9k3k9kdnz2pxqknsqxpf2dpf4plgibymk4ssy7z05")))

(define-public crate-evdev-sys-0.1.0 (c (n "evdev-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0jpk7cm6zn1wgkbk883avd4nqss16c9i8cp3c7gl9jwaai7hg0ca") (l "evdev")))

(define-public crate-evdev-sys-0.2.0 (c (n "evdev-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1h1qp4xxccasg8krj768b386slc9ynj2c0vqfxqns0q9cri671rp") (l "evdev")))

(define-public crate-evdev-sys-0.2.1 (c (n "evdev-sys") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1lcycdpvwjx116xibrcxgnfwma0v0z2dbvy9cqppy9n707la8slw") (l "evdev")))

(define-public crate-evdev-sys-0.2.2 (c (n "evdev-sys") (v "0.2.2") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "07vmd7pwi14p15522d4k3b56imyx8ixirjplfbls2v56akkfrmwr") (l "evdev")))

(define-public crate-evdev-sys-0.2.3 (c (n "evdev-sys") (v "0.2.3") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1qm6v5s7pwcaqc8a66b8wkkspmgdpkdghhkd7y7c1i4imadwc431") (l "evdev")))

(define-public crate-evdev-sys-0.2.4 (c (n "evdev-sys") (v "0.2.4") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1m77x5shxjkc2wqfxw5cmxi8bw3ifyqhygvhg63l32zjy5c19iw0") (f (quote (("libevdev-1-10")))) (l "evdev")))

(define-public crate-evdev-sys-0.2.5 (c (n "evdev-sys") (v "0.2.5") (d (list (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0vgax74wjsm22nrx6ikh8m7bj3nggf83s961i5qd85bvahmx9shl") (f (quote (("libevdev-1-10")))) (l "evdev")))

