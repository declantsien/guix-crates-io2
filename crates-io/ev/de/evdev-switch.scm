(define-module (crates-io ev de evdev-switch) #:use-module (crates-io))

(define-public crate-evdev-switch-0.0.0 (c (n "evdev-switch") (v "0.0.0") (h "1kgrisgfr2fr0wlx0vi4ml0vg926m6ihvm24an6pvilms8y87qha")))

(define-public crate-evdev-switch-0.1.0 (c (n "evdev-switch") (v "0.1.0") (d (list (d (n "input-linux") (r "^0.5.0") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1vi24mschvv95v5cjrh2calkzl80390nnfgwm7c34lgxk8a8fn6m") (y #t)))

