(define-module (crates-io ev de evdev-rs-tokio) #:use-module (crates-io))

(define-public crate-evdev-rs-tokio-0.4.0 (c (n "evdev-rs-tokio") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "evdev-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0irklgmwhl67wm2pp7hp2lin5wcv88akqq0yllfjzdr5kd3z1a9j") (f (quote (("default"))))))

