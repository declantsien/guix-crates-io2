(define-module (crates-io ev de evdev-rs) #:use-module (crates-io))

(define-public crate-evdev-rs-0.0.1 (c (n "evdev-rs") (v "0.0.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "evdev-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nix") (r "^0.7") (d #t) (k 0)))) (h "0w20cgf48qmxkcb6z8sdyizn1wl5wglk2337cwk0bjfx6bcqxxkn")))

(define-public crate-evdev-rs-0.1.0 (c (n "evdev-rs") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "evdev-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nix") (r "^0.7") (d #t) (k 0)))) (h "1r0zacxbg4ird0j2ik2psapl98ywl1gnjh74hjpks3nhnahmqfxs")))

(define-public crate-evdev-rs-0.2.0 (c (n "evdev-rs") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "evdev-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "1if8zwxwslh89bffyxqxa3q0c0jh67fanadiqq6gn84a984wc40n")))

(define-public crate-evdev-rs-0.3.0 (c (n "evdev-rs") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "evdev-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "1kxrgnfqzr0vx2s94flfnrgy804ymykx757v4ry3rgli9rsw4f15")))

(define-public crate-evdev-rs-0.3.1 (c (n "evdev-rs") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "evdev-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "16cp6q9ya9p1zcs6vhi54f12ixmxhjb0a7ymm9487wlx06nkvxwm")))

(define-public crate-evdev-rs-0.4.0 (c (n "evdev-rs") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "evdev-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0893w31267lhfpjivp0la2dlm3y6mckddr6y81j4y7pxslqbqamr")))

(define-public crate-evdev-rs-0.5.0 (c (n "evdev-rs") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "evdev-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1kv7appqdpwpkd66yskkc46wbmqlaix8vmlddrp5l85kysmm3nyp") (f (quote (("default"))))))

(define-public crate-evdev-rs-0.6.0 (c (n "evdev-rs") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "evdev-sys") (r "^0.2.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1bqwvrh5sdlm1gvh3kgy0i64vj90d2hfyhas7r3g052xjxsl0l26") (f (quote (("libevdev-1-10" "evdev-sys/libevdev-1-10") ("default"))))))

(define-public crate-evdev-rs-0.6.1 (c (n "evdev-rs") (v "0.6.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "evdev-sys") (r "^0.2.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1fkdw23kjjn8an3xjpi2g74yhdfkv89ngsrkjd2cxz5n1xwxa4lq") (f (quote (("libevdev-1-10" "evdev-sys/libevdev-1-10") ("default"))))))

