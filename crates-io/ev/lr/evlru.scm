(define-module (crates-io ev lr evlru) #:use-module (crates-io))

(define-public crate-evlru-0.0.1-beta.1 (c (n "evlru") (v "0.0.1-beta.1") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "evmap") (r "^10.0.2") (d #t) (k 0)))) (h "1sm1lvfx998ap10cj9f35qdan64jlh8nq85yr5f35f11hi6ywbwr") (y #t)))

(define-public crate-evlru-0.0.1-beta.2 (c (n "evlru") (v "0.0.1-beta.2") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "evmap") (r "^10.0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("rt"))) (d #t) (k 0)))) (h "1v63rjich4wl8qma4pvb51915337zdbd8c6p6960922gjsq695jp") (y #t)))

(define-public crate-evlru-0.0.1-beta.3 (c (n "evlru") (v "0.0.1-beta.3") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "evmap") (r "^10.0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("rt"))) (d #t) (k 0)))) (h "0vldp7smcldml4ykr8pbicw376hdlcgwfc6gzc4j6g3icrg009mf") (y #t)))

(define-public crate-evlru-0.0.1-beta.4 (c (n "evlru") (v "0.0.1-beta.4") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "evmap") (r "^10.0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("rt"))) (d #t) (k 0)))) (h "1svsly71zrsj80srlyyhdaipfqji364sn7rm8byk9injy6d3qfr3") (y #t)))

(define-public crate-evlru-0.0.1-beta.5 (c (n "evlru") (v "0.0.1-beta.5") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "evmap") (r "^10.0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("rt"))) (d #t) (k 0)))) (h "1kchdfcn2spwf8pmb2fpc33bas6fibrha98f7r60qqdvf70084n7")))

(define-public crate-evlru-0.0.1 (c (n "evlru") (v "0.0.1") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "evmap") (r "^10.0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "153dnldnbrcjifrvhjgyzr583w0h07mfqb16hq86cjkgkarrfhi9")))

(define-public crate-evlru-0.0.2 (c (n "evlru") (v "0.0.2") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "evmap") (r "^10.0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1wkq1x56vgqhsg1zrxxv2pdxgmxqp1yf01808p3cmb7rf4yxwzh4")))

(define-public crate-evlru-0.1.0 (c (n "evlru") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "evmap") (r "^10.0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1j9pjx3dkjn5xx39g21axs8fgbzsggb8wmxz7dvzanxgdcpq42a0")))

(define-public crate-evlru-0.1.1 (c (n "evlru") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "evmap") (r "^10.0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (d #t) (k 0)))) (h "14rwy44h6jl2qp4xc1hy4c0na1a2a28cn505rsk3ivb61lqlwil7")))

