(define-module (crates-io ev cx evcxr_runtime) #:use-module (crates-io))

(define-public crate-evcxr_runtime-1.0.0 (c (n "evcxr_runtime") (v "1.0.0") (h "12qw3fjfrqivw8qdcy1zw47lkvfjrsy6sr3vsmv30qh3n82sa6vz")))

(define-public crate-evcxr_runtime-1.1.0 (c (n "evcxr_runtime") (v "1.1.0") (d (list (d (n "base64") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "1x3kdfiibjkhv8zfv4inm9ynkm6w8g184j3v8idmbciwxad64zfn") (f (quote (("bytes" "base64"))))))

