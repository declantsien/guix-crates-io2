(define-module (crates-io ev cx evcxr_image) #:use-module (crates-io))

(define-public crate-evcxr_image-1.0.0 (c (n "evcxr_image") (v "1.0.0") (d (list (d (n "base64") (r "^0.10.0") (d #t) (k 0)) (d (n "evcxr_runtime") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.20.1") (d #t) (k 0)))) (h "0v9108nzm3pj6mdjyvxhzwyvi7yfmfn10j0ay4vyzcyn31pdpkgd")))

(define-public crate-evcxr_image-1.1.0 (c (n "evcxr_image") (v "1.1.0") (d (list (d (n "evcxr_runtime") (r "^1.1.0") (f (quote ("bytes"))) (d #t) (k 0)) (d (n "image") (r "^0.23.12") (f (quote ("png"))) (k 0)))) (h "176rl2cb0w1rz2qzzkllrkc5q4yb1g6nq0xhn05wa36lbndh60sm")))

