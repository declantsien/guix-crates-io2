(define-module (crates-io ev cx evcxr_ssg_macro) #:use-module (crates-io))

(define-public crate-evcxr_ssg_macro-0.1.0 (c (n "evcxr_ssg_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full"))) (d #t) (k 0)))) (h "1jdq43c572sacc65k96qj4apbl8z344prsfv6hbf4xz2zsbvm8bn")))

(define-public crate-evcxr_ssg_macro-0.1.1 (c (n "evcxr_ssg_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full"))) (d #t) (k 0)))) (h "1vlygz7kaz1j8icblgsd89gqq4p58kh9c8i5djw32sjcy88w4ks3")))

(define-public crate-evcxr_ssg_macro-0.1.2 (c (n "evcxr_ssg_macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full"))) (d #t) (k 0)))) (h "1f9hfpngbccchc5y64j0qbqps1xx5ijhvgd99nm9396xw6r9sqvr")))

(define-public crate-evcxr_ssg_macro-0.1.3 (c (n "evcxr_ssg_macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full"))) (d #t) (k 0)))) (h "09i74h4blfnqqvqjiii6wsj1fzws6wmhxg0bhyj75768pbhdlcls")))

(define-public crate-evcxr_ssg_macro-0.1.4 (c (n "evcxr_ssg_macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full"))) (d #t) (k 0)))) (h "0h6cii6r3a1dcd03523y2s5kccl06dnb6adixzy8w78ddi8c2qdv")))

