(define-module (crates-io ev cx evcxr_ssg) #:use-module (crates-io))

(define-public crate-evcxr_ssg-0.1.0 (c (n "evcxr_ssg") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "evcxr_ssg_macro") (r "^0.1.1") (d #t) (k 0)) (d (n "postcard") (r "^1.0.8") (f (quote ("use-std"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1k6z8qhm9bka4gq7zpyr5b58qm2mmkah7i1mxi9vxwq0ygs2agrh")))

(define-public crate-evcxr_ssg-0.1.1 (c (n "evcxr_ssg") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "evcxr_ssg_macro") (r "^0.1.2") (d #t) (k 0)) (d (n "postcard") (r "^1.0.8") (f (quote ("use-std"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1fbahbp51adb4m2gc6d0k4lv8x0q31vrpvzwrsc699x88fgmrg1m")))

(define-public crate-evcxr_ssg-0.1.3 (c (n "evcxr_ssg") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "evcxr_ssg_macro") (r "^0.1.4") (d #t) (k 0)) (d (n "postcard") (r "^1.0.8") (f (quote ("use-std"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1j4nj3x9hx2as6hxnwrs8vvi51xvkm52cvb1ymw9j3wh58jwyh58")))

