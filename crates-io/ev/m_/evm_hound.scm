(define-module (crates-io ev m_ evm_hound) #:use-module (crates-io))

(define-public crate-evm_hound-0.1.0 (c (n "evm_hound") (v "0.1.0") (d (list (d (n "ethers") (r "^2.0") (d #t) (k 2)) (d (n "eyre") (r "^0.6") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1dpjdibrbchn2cay1z1gsb0nqi7dds4zbfj4pqc41qz0w5mq639f") (y #t)))

(define-public crate-evm_hound-0.1.1 (c (n "evm_hound") (v "0.1.1") (d (list (d (n "ethers") (r "^2.0") (d #t) (k 2)) (d (n "eyre") (r "^0.6") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0i1iyhmvzp7jchmq5y1y998yrky1bzdig9hffxf7lf7h96hya6kc") (y #t)))

(define-public crate-evm_hound-0.1.2 (c (n "evm_hound") (v "0.1.2") (d (list (d (n "ethers") (r "^2.0") (d #t) (k 2)) (d (n "eyre") (r "^0.6") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1fgxp54cdza32sq4c8rzd3lg0rzhc8fscsgkasx73r3z6pwfk1z7")))

(define-public crate-evm_hound-0.1.3 (c (n "evm_hound") (v "0.1.3") (d (list (d (n "ethers") (r "^2.0") (d #t) (k 2)) (d (n "eyre") (r "^0.6") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "03xq9mmzqnrppzwgkkkysszb5n7gw70g0r61339pqi6pq208cvim")))

(define-public crate-evm_hound-0.1.4 (c (n "evm_hound") (v "0.1.4") (d (list (d (n "ethers") (r "^2.0") (d #t) (k 2)) (d (n "eyre") (r "^0.6") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0kgsk3xxnyq01kc27bfzy1a83g6by9jwn18c4sdhpy7q1499wnfz")))

