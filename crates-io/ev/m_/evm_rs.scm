(define-module (crates-io ev m_ evm_rs) #:use-module (crates-io))

(define-public crate-evm_rs-0.1.0 (c (n "evm_rs") (v "0.1.0") (d (list (d (n "ethnum") (r "^1.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "02dhxj7a2dgbbi210pd0728qhc45mr2ssww53bl0pcr2vr4rcyv7")))

(define-public crate-evm_rs-0.1.1 (c (n "evm_rs") (v "0.1.1") (d (list (d (n "ethnum") (r "^1.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0spnfk13imh52r76jwckdigprpf3jircvv17imr0ywxfbhk16wxi")))

(define-public crate-evm_rs-0.1.2 (c (n "evm_rs") (v "0.1.2") (d (list (d (n "ethnum") (r "^1.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "1n48qz198vaad7qbikri7dqrhhcbj0hkj8bjc5s2lpfmnfrh58c5")))

(define-public crate-evm_rs-0.1.3 (c (n "evm_rs") (v "0.1.3") (d (list (d (n "ethnum") (r "^1.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "07nrsj4203jj6jz8nb3wfz3xfqidjls94b8n1c673ia92nn1hnch")))

(define-public crate-evm_rs-0.2.0 (c (n "evm_rs") (v "0.2.0") (d (list (d (n "ethnum") (r "^1.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0brbkad2hdjsr4xzmanw9dqi41qsbs8hq9irv3cf527c0rpbx5hn")))

(define-public crate-evm_rs-0.3.0 (c (n "evm_rs") (v "0.3.0") (d (list (d (n "ethnum") (r "^1.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0jw29aafnnqv181pq5hniw31bcip8zn16iar6ax0rk9dfmglj3w5")))

(define-public crate-evm_rs-0.3.1 (c (n "evm_rs") (v "0.3.1") (d (list (d (n "ethnum") (r "^1.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "1wiwm4j232bwn4468cnqrv1zs1m50vx0l0c364fingf6vjmvy7dy")))

(define-public crate-evm_rs-0.3.2 (c (n "evm_rs") (v "0.3.2") (d (list (d (n "ethnum") (r "^1.2.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0s2vi7cxns3j9wvz89qymjgn0ypf5882l64fk80jq0a51gw9lqv8")))

