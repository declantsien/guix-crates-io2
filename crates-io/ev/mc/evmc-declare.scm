(define-module (crates-io ev mc evmc-declare) #:use-module (crates-io))

(define-public crate-evmc-declare-7.1.0 (c (n "evmc-declare") (v "7.1.0") (d (list (d (n "evmc-vm") (r "^7.1.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p3769h3ipid1lj11jzmy705y30l1r6s2gjylkxgpmscq444xi7y")))

(define-public crate-evmc-declare-7.2.0 (c (n "evmc-declare") (v "7.2.0") (d (list (d (n "evmc-vm") (r "^7.2.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ydvyn6m5maf05gxbk50ax9abw2yhgfrxz5qk61wmkwpr4nljmj1")))

(define-public crate-evmc-declare-7.3.0 (c (n "evmc-declare") (v "7.3.0") (d (list (d (n "evmc-vm") (r "^7.3.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hizjyih80228m4wjvlljls2h597p8awlm01xrvf15rnfs25bjv0")))

(define-public crate-evmc-declare-7.4.0 (c (n "evmc-declare") (v "7.4.0") (d (list (d (n "evmc-vm") (r "^7.4.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0db2hg6w0kldnpcxjsa3bq49n38hyx4zfvhjj7z9dc7dw9prxr0a")))

(define-public crate-evmc-declare-7.5.0 (c (n "evmc-declare") (v "7.5.0") (d (list (d (n "evmc-vm") (r "^7.5.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i2w4sd9km9g177bv2h83y9x7fyif273h6sl3fjcrz6ff30j19nn")))

(define-public crate-evmc-declare-8.0.0 (c (n "evmc-declare") (v "8.0.0") (d (list (d (n "evmc-vm") (r "^8.0.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "134np663lv8s75bbw19pdgsclgaax1faana25xj46jw05ji8pg9k")))

(define-public crate-evmc-declare-9.0.0 (c (n "evmc-declare") (v "9.0.0") (d (list (d (n "evmc-vm") (r "^9.0.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jsr3spd32sy79qma59li0i2xq4g3rn0vr444z8pbqjwv3ddkc66")))

