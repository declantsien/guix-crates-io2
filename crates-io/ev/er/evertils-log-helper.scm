(define-module (crates-io ev er evertils-log-helper) #:use-module (crates-io))

(define-public crate-evertils-log-helper-0.1.0 (c (n "evertils-log-helper") (v "0.1.0") (h "05wr2h2ff9kal70yyxwiika8r4yncdqhp24a966q6aa8m1c2m18i")))

(define-public crate-evertils-log-helper-0.2.0 (c (n "evertils-log-helper") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1npqanmbn2m1fhlpldihpbg7jvvjgbbljv8qii9w4k1rkamlzpf1")))

(define-public crate-evertils-log-helper-0.3.0 (c (n "evertils-log-helper") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0spk268rhd6g3w5k1n4l1x7jxvrh2x3vimhiznvf5zzq1vcaps1f")))

(define-public crate-evertils-log-helper-0.3.1 (c (n "evertils-log-helper") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "11b470yjxgfkhy2b0z90c9rk15gjxy06jxphw9gd61fsh8lzpfhl")))

(define-public crate-evertils-log-helper-0.4.0 (c (n "evertils-log-helper") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "10vppx3sbaj2jbw30xg0zgj85avk1m5jkdjy8h6wy1jhrff5g22q")))

(define-public crate-evertils-log-helper-0.4.1 (c (n "evertils-log-helper") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0z8lmxfrn1fwvccp4pz1z1avh369z6f2hinskrh26l537fprsjp7")))

