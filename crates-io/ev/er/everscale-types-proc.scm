(define-module (crates-io ev er everscale-types-proc) #:use-module (crates-io))

(define-public crate-everscale-types-proc-0.1.0 (c (n "everscale-types-proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "0qmm8ps6ps7vvc87g1vx4rh6bjk8bmq63pmz6ykjv0icih8mg6fn")))

(define-public crate-everscale-types-proc-0.1.1 (c (n "everscale-types-proc") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1qnvggqr2gxic7p3iflgsk18qlclmwvylmlm0k2qq8a2lnqwhc4m")))

(define-public crate-everscale-types-proc-0.1.2 (c (n "everscale-types-proc") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1qvk9lncqf45amjdsmcqxvxws5j1l56igqzp76bv929axqf3i7mj")))

(define-public crate-everscale-types-proc-0.1.3 (c (n "everscale-types-proc") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1sjam0w1fxjhzmf9h77calkc6w32rlcxjijvk7iqn69s8938is3i")))

(define-public crate-everscale-types-proc-0.1.4 (c (n "everscale-types-proc") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "028cvfihvz0rwb5f5wpxc0vypza22mzh0bfpn9pc3qkbqxhqng9j")))

