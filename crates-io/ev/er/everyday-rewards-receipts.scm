(define-module (crates-io ev er everyday-rewards-receipts) #:use-module (crates-io))

(define-public crate-everyday-rewards-receipts-0.1.0 (c (n "everyday-rewards-receipts") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0szghhn4s4mqpz0bc320zkvsdxmr6rdpzvi5x4gnbn7ld39f90md")))

(define-public crate-everyday-rewards-receipts-0.1.1 (c (n "everyday-rewards-receipts") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1adqpz7w0q5jr0mxcbnnmkczgq193y0p1qzx3ccxw87wb7dlwkb5")))

