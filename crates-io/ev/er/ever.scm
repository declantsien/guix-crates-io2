(define-module (crates-io ev er ever) #:use-module (crates-io))

(define-public crate-ever-0.1.0 (c (n "ever") (v "0.1.0") (d (list (d (n "ever-macro") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "12jnjiqr3nyg8hzik7zbrcxiyv82f9vdci7y5jvl5l5ljmd1zzcy")))

(define-public crate-ever-0.2.0 (c (n "ever") (v "0.2.0") (d (list (d (n "cargo-lock") (r "^6.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "ever-macro") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 2)) (d (n "semver") (r "^0.11") (d #t) (k 2)))) (h "07ria4i0llm740jdjz6yns3lzrsjwhq0qx1p4wlvypdwkfzx7k71")))

