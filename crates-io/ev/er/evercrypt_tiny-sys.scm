(define-module (crates-io ev er evercrypt_tiny-sys) #:use-module (crates-io))

(define-public crate-evercrypt_tiny-sys-0.1.0 (c (n "evercrypt_tiny-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "1lvg8cchl2lxhwgysvbppnsa3b5plzw0k67din53inbg3qdw80sb") (f (quote (("default"))))))

(define-public crate-evercrypt_tiny-sys-0.1.1 (c (n "evercrypt_tiny-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "0sszyi5ai8mnv2253j17m4yqvgbrblsy9fx1xdb86g9rdck3rj1g") (f (quote (("default"))))))

