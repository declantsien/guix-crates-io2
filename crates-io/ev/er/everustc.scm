(define-module (crates-io ev er everustc) #:use-module (crates-io))

(define-public crate-everustc-0.1.0 (c (n "everustc") (v "0.1.0") (d (list (d (n "everust") (r "^0.2.0") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)))) (h "1fr724lcawswgxm7igbmsi85ajvl3a25hy9cjjrdb3ik26gksmik")))

