(define-module (crates-io ev er everybody_loops) #:use-module (crates-io))

(define-public crate-everybody_loops-0.1.0 (c (n "everybody_loops") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "fs-err") (r "^2.7.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.1.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1lg1b54m33wddvn3a2kb9sbp671w1w9kk7kq0cmh91x6r9mwhywv")))

(define-public crate-everybody_loops-0.1.1 (c (n "everybody_loops") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "fs-err") (r "^2.7.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.1.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0arfxm51gwlzixjvsgwh68l9bhnqa6c5qgbbdpmhl5cbha020jxc")))

