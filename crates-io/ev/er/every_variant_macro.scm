(define-module (crates-io ev er every_variant_macro) #:use-module (crates-io))

(define-public crate-every_variant_macro-0.3.0 (c (n "every_variant_macro") (v "0.3.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1p7qpzavvxnbd9226949h1scna8p2166h8dmhpra7xmdy8iwdnm3")))

(define-public crate-every_variant_macro-0.3.1 (c (n "every_variant_macro") (v "0.3.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "148alqknylsp0rm54arhc46m8sqcrzglgxkhgagvc23qjj45nbng")))

