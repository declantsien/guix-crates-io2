(define-module (crates-io ev er everust) #:use-module (crates-io))

(define-public crate-everust-0.1.0 (c (n "everust") (v "0.1.0") (d (list (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "1zh2f03wi5w6q5pqaz11ayl514np446llsk5nl8afwf8jfl4s0jk")))

(define-public crate-everust-0.1.1 (c (n "everust") (v "0.1.1") (d (list (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "1jg2yk8brz7airry4n498y8fq60pkjbah0xh43rn045mj5w14r7h")))

(define-public crate-everust-0.2.0 (c (n "everust") (v "0.2.0") (d (list (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "1p4ymk6ciwz634scd1skyf6d6bi26q5ffh0v29l79s5p619m9z0s")))

(define-public crate-everust-0.3.0 (c (n "everust") (v "0.3.0") (d (list (d (n "tempdir") (r "^0.3.5") (d #t) (k 0)))) (h "0z3i70xz5pik39xpbgdn73vwx9yx2ngh28kbs32dal8skllgmr2m")))

