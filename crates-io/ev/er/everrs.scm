(define-module (crates-io ev er everrs) #:use-module (crates-io))

(define-public crate-everrs-0.1.0 (c (n "everrs") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "04lr9v1cyag3yf901z3yfi01pg7y0vfv3id9sbqcag3vrx2yc9g8")))

(define-public crate-everrs-0.2.0 (c (n "everrs") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "1d4qidiq1wx17ic6rf9vziz3xqvnkfjsf6x4h26p541fgwngx2nd") (f (quote (("native") ("default"))))))

(define-public crate-everrs-0.2.1 (c (n "everrs") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (d #t) (k 0)))) (h "0zchavy4yljw7xdk6xs908ckvy9x62ikg0nkafm1kn6026r87wsy") (f (quote (("unstable") ("native") ("default"))))))

