(define-module (crates-io ev er eversal-lib) #:use-module (crates-io))

(define-public crate-eversal-lib-0.1.0 (c (n "eversal-lib") (v "0.1.0") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.7.4") (f (quote ("runtime-tokio" "postgres" "chrono" "uuid"))) (d #t) (k 0)))) (h "0sxfdsfhaknk2pkj10ik9pd3w1bqnvlvg8jgail7m19b6rds479r")))

(define-public crate-eversal-lib-0.1.1 (c (n "eversal-lib") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^9.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "oauth2") (r "^4.4.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.4") (f (quote ("runtime-tokio" "postgres" "chrono" "uuid"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full" "test-util"))) (d #t) (k 2)))) (h "1c5ky17f0fr16z2vqkfzq6196bnf0zpzbi8cwbk9xnmffkhrlc28")))

