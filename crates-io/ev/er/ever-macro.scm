(define-module (crates-io ev er ever-macro) #:use-module (crates-io))

(define-public crate-ever-macro-0.1.0 (c (n "ever-macro") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1lpxy74fpzafvpw2nsi6q79yhxf8ffj5vvmc7a2s91j3sdrp65l0")))

(define-public crate-ever-macro-0.2.0 (c (n "ever-macro") (v "0.2.0") (d (list (d (n "cargo_metadata") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "rustc_version") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "whoami") (r "^1.0") (d #t) (k 0)))) (h "0544mxlfrsval5yjnxj0kxh6h0ahfkn4rcp0l900dkpg68f00k3c")))

