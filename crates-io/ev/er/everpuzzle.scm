(define-module (crates-io ev er everpuzzle) #:use-module (crates-io))

(define-public crate-everpuzzle-0.1.0 (c (n "everpuzzle") (v "0.1.0") (d (list (d (n "fixedstep") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "oorandom") (r "^11.1") (d #t) (k 0)) (d (n "pix") (r "^0.7.0") (d #t) (k 0)) (d (n "png_pong") (r "^0.1") (d #t) (k 0)) (d (n "vek") (r "^0.9.11") (d #t) (k 0)) (d (n "wgpu") (r "^0.4") (d #t) (k 0)) (d (n "wgpu_glyph") (r "^0.6") (d #t) (k 0)) (d (n "winit") (r "^0.21") (d #t) (k 0)))) (h "035ygjhqrsmc4r4d781rszjj7j21rahlap7xq0v0cr5a2k2inhfc")))

