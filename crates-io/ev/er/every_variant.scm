(define-module (crates-io ev er every_variant) #:use-module (crates-io))

(define-public crate-every_variant-0.1.0 (c (n "every_variant") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0fj7ilxr4asf1dg7sa1irycn0yanjb1pxs86jj1biwiwxprrhhb6") (y #t)))

(define-public crate-every_variant-0.1.1 (c (n "every_variant") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rpxsagw4qy50d8pa823lpfbi4mvg68l6sg63j69rq61bmnx74k2") (y #t)))

(define-public crate-every_variant-0.1.2 (c (n "every_variant") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1bi828igdqzs4v0g4ibzy4hhkc9smdbdr8730sjj5a4va03f4s10") (y #t)))

(define-public crate-every_variant-0.1.3 (c (n "every_variant") (v "0.1.3") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rs910r7lmagwm4yasvb2hrapqjiaiydlqpafpsqjpx8h1m0q64l") (y #t)))

(define-public crate-every_variant-0.1.4 (c (n "every_variant") (v "0.1.4") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0y6q8ajid0v4l2vvbblqfn7frakz2ms5h4lz0xvkzl5cglzp6zkd") (y #t)))

(define-public crate-every_variant-0.1.5 (c (n "every_variant") (v "0.1.5") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16xbka8z0x5qvaxl2a99rrxnbqs18iywg31gmzyf8bmjpc1mw55a") (y #t)))

(define-public crate-every_variant-0.1.6 (c (n "every_variant") (v "0.1.6") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09wd2q7yphgd5lcslnxb7mnr9klpv4hlacjbl911f2ywrl6p72cw")))

(define-public crate-every_variant-0.2.0 (c (n "every_variant") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0rk25vpqvxy6sip2xry3cgsjixcfdpj3hbb39z5dczi0f1937dvg") (y #t)))

(define-public crate-every_variant-0.2.1 (c (n "every_variant") (v "0.2.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07h9vfng7igflivhzrawhpgabajlf1m141hsrz1vg63jh5h90n7n")))

(define-public crate-every_variant-0.3.0 (c (n "every_variant") (v "0.3.0") (d (list (d (n "every_variant_macro") (r "^0.3.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "0z3bnvdc9d48bp6cqi3wyhfadmz35wc3ypf12x278clraixx8svm") (f (quote (("ev_heapless" "heapless") ("default")))) (y #t)))

(define-public crate-every_variant-0.3.1 (c (n "every_variant") (v "0.3.1") (d (list (d (n "every_variant_macro") (r "^0.3.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "1k7qgwd40adh0fm0zpa7dyaaackvhlgk1l5wr277s7pplw451x7h") (f (quote (("ev_heapless" "heapless") ("default"))))))

(define-public crate-every_variant-0.4.0 (c (n "every_variant") (v "0.4.0") (d (list (d (n "every_variant_macro") (r "^0.3.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "1j67bmdkdn93pdirbzmc411djdn02lnqgrim6zadi9ijgp50vv7q") (f (quote (("ev_heapless" "heapless") ("default")))) (y #t)))

(define-public crate-every_variant-0.4.1 (c (n "every_variant") (v "0.4.1") (d (list (d (n "every_variant_macro") (r "^0.3.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "0gp8dw4bhbpwczw9xba3jv1pnxy3yva56lflhvfgjlg9dfibivfk") (f (quote (("ev_heapless" "heapless") ("default"))))))

(define-public crate-every_variant-0.4.2 (c (n "every_variant") (v "0.4.2") (d (list (d (n "every_variant_macro") (r "^0.3.1") (d #t) (k 0)) (d (n "heapless") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "1lm4lvpxccp909kp5bfrsc3zl8868zdxq97byh6dnlamfdigdzcq") (f (quote (("ev_heapless" "heapless") ("default"))))))

(define-public crate-every_variant-0.4.3 (c (n "every_variant") (v "0.4.3") (d (list (d (n "every_variant_macro") (r "^0.3.1") (d #t) (k 0)) (d (n "heapless") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "156zyj4p8z05rsshzqank0mns9a65nq7lv5y4l4c7y5fjhsd2n1m") (f (quote (("ev_heapless" "heapless") ("default"))))))

(define-public crate-every_variant-0.4.4 (c (n "every_variant") (v "0.4.4") (d (list (d (n "every_variant_macro") (r "^0.3.1") (d #t) (k 0)) (d (n "heapless") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "1nk46jx3p297j4b05wkslc3qd9y1xvcrq084bw027j5kb1pid7lw") (f (quote (("ev_heapless" "heapless") ("default"))))))

(define-public crate-every_variant-0.4.5 (c (n "every_variant") (v "0.4.5") (d (list (d (n "every_variant_macro") (r "^0.3.1") (d #t) (k 0)) (d (n "heapless") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0djxvjvcnxk9waizlxfwc971734vv12lhppkzhnznfrc1m2yabh6") (f (quote (("ev_heapless" "heapless") ("default")))) (s 2) (e (quote (("uuid" "dep:uuid"))))))

