(define-module (crates-io ev er everblue) #:use-module (crates-io))

(define-public crate-everblue-0.1.1 (c (n "everblue") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Document" "NodeList" "Element"))) (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "0wsjfyb6s115ji5a9fkigzlz8gls9bzpzjvhr2r9cb9hxdiygyj8") (y #t)))

(define-public crate-everblue-0.1.11 (c (n "everblue") (v "0.1.11") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Document" "NodeList" "Element"))) (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "1vnn16bf952yxbp69pb7b4ycr7qs6y63i7dvaachpcslnyah49i1") (y #t)))

(define-public crate-everblue-0.1.111 (c (n "everblue") (v "0.1.111") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Document" "NodeList" "Element"))) (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "0wmz53g1b95i0znmpvm62xhp2y5hxvvkrqhhq7lgrmhqrfsxyn82") (y #t)))

