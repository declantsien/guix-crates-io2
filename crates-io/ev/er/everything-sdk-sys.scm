(define-module (crates-io ev er everything-sdk-sys) #:use-module (crates-io))

(define-public crate-everything-sdk-sys-0.0.2+2 (c (n "everything-sdk-sys") (v "0.0.2+2") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "windows") (r "^0.52") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1yzg17hdfyd4x8m23zn1p8n2vqaarc5jvc8vg8dif9j1qzdy9jfi") (f (quote (("vendored") ("dll") ("default" "vendored")))) (l "everything-sdk")))

(define-public crate-everything-sdk-sys-0.0.3+2 (c (n "everything-sdk-sys") (v "0.0.3+2") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "windows") (r "^0.52") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "09pjyq3ppfj1bxnlqn3l0f1vkq2cv3a6jp981qa3hn8hygpg9ix4") (f (quote (("vendored") ("dll") ("default" "vendored")))) (l "everything-sdk")))

