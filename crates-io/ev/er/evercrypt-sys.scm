(define-module (crates-io ev er evercrypt-sys) #:use-module (crates-io))

(define-public crate-evercrypt-sys-0.0.1 (c (n "evercrypt-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1gb4v74lwd40ppf01bvijj7y44w1z0ma5zfbk8qid680qp5g21j1") (y #t) (l "evercrypt")))

(define-public crate-evercrypt-sys-0.0.3-dev (c (n "evercrypt-sys") (v "0.0.3-dev") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0nmg7hyasn89j1yk8dh6hrqwb7cmfp7rbysfwjaa6v7xgjly3nzj") (y #t) (l "evercrypt")))

(define-public crate-evercrypt-sys-0.0.3 (c (n "evercrypt-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1icqc9ibm0l5aciwapb43fjz5a4bgm2v7z8z167kk78wczcv2fsr") (l "evercrypt")))

(define-public crate-evercrypt-sys-0.0.4 (c (n "evercrypt-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "1v6xd9mlsvs7wb3nwgrvi3yfmx8xlx22cyrihdq35gwkb93nhird") (l "evercrypt")))

(define-public crate-evercrypt-sys-0.0.5 (c (n "evercrypt-sys") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "11pm5zkd2l102s409v8b2977hxwx8h4rimibv1iqkf6yns7sxnbs") (l "evercrypt")))

(define-public crate-evercrypt-sys-0.0.6 (c (n "evercrypt-sys") (v "0.0.6") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0sl4zfnywwd6xhkk2zyas7kaa6ka3sjrhh3idp1myhmgvpc0vb89") (l "evercrypt")))

(define-public crate-evercrypt-sys-0.0.7 (c (n "evercrypt-sys") (v "0.0.7") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "13f7xdi51qhv2d6mkjbl42cdnfdjlyj2x668fg2fn9hyyzcbx96c") (l "evercrypt")))

(define-public crate-evercrypt-sys-0.0.8 (c (n "evercrypt-sys") (v "0.0.8") (d (list (d (n "bindgen") (r "^0.58") (d #t) (t "cfg(not(windows))") (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0ma1iscvhj49q93hw3izqr4cnz90dbdwj9w47jhsc809bqdwzskk") (l "evercrypt")))

(define-public crate-evercrypt-sys-0.0.9 (c (n "evercrypt-sys") (v "0.0.9") (d (list (d (n "bindgen") (r "^0.58") (d #t) (t "cfg(not(windows))") (k 1)) (d (n "libc") (r "^0.2") (k 0)))) (h "0hr5ifnqclfsssivh0rjm1fs9sqsrkgv42rbkfqwfdmgjr9x0yxm") (l "evercrypt")))

