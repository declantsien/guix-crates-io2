(define-module (crates-io ev er everytime) #:use-module (crates-io))

(define-public crate-everytime-0.1.0 (c (n "everytime") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1s0p8k70fxb3kkx6m3wipyfzxjrklv54vwf5j3brbj5jyl2wgb1v")))

(define-public crate-everytime-0.2.0 (c (n "everytime") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.9.0") (f (quote ("case-insensitive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1c2ccffrwmsi8047mf3byyanyz4pal7qbmw66cpi54czw947c9gg")))

(define-public crate-everytime-0.3.0 (c (n "everytime") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.9.0") (f (quote ("case-insensitive"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0is89xfgsj72r6xqxh56zf3fjrzv1pw689v16ivvrr7614mvqhhv")))

(define-public crate-everytime-0.4.0 (c (n "everytime") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.9.0") (f (quote ("case-insensitive"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zpf2g3vr5mwdnd5rj2rsn1vbr88q5cnfxy41c1akamnfhyn4dia")))

(define-public crate-everytime-0.4.1 (c (n "everytime") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.9.0") (f (quote ("case-insensitive"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qcbn02l2ivfhyz7nicngdjzj6v2klch8kr1a99h69dlfpq0zb1r")))

(define-public crate-everytime-0.4.2 (c (n "everytime") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.9.0") (f (quote ("case-insensitive"))) (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.5") (d #t) (k 0)))) (h "1dl0xvc4nvqf6qxdpjms3fmf454hwz220msn70jfyrc6v96698fh")))

