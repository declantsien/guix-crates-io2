(define-module (crates-io ev er everyday_macros) #:use-module (crates-io))

(define-public crate-everyday_macros-0.1.0 (c (n "everyday_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.55") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0xm842j95vvv78dxjj3hdxfplv6nln5d1mjjz9h4a9bidhrhm1k6")))

