(define-module (crates-io ev er everything-sys-bindgen) #:use-module (crates-io))

(define-public crate-everything-sys-bindgen-0.1.0 (c (n "everything-sys-bindgen") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 2)))) (h "00rd1d5qhyc8rm0a0k6q4w4dgvbbgh3m1rdswzy1c2pskqaf2r21")))

(define-public crate-everything-sys-bindgen-0.1.1 (c (n "everything-sys-bindgen") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 2)))) (h "08a1d4lzy98zlphwa0zs8mvyqq4ny367fwdzcb22dj8v9zgf8kl9") (y #t)))

(define-public crate-everything-sys-bindgen-0.1.2 (c (n "everything-sys-bindgen") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 2)))) (h "0ij7w5x3nh0gvpdb8cwl8m223k5as0a1fk9zyzl4s8vcbdwkgcvx") (y #t)))

(define-public crate-everything-sys-bindgen-0.1.3 (c (n "everything-sys-bindgen") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 2)))) (h "0nmbpwcl4p5nixqc6y7z0l2z8qz56fhvlwpnjlb8d7hj27k0gmc5") (y #t)))

(define-public crate-everything-sys-bindgen-0.1.4 (c (n "everything-sys-bindgen") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 2)))) (h "1hghw5gdsjxnsh0gfwnicbqbz8nd86y7rjvn8ir21fcbif6rjgix") (y #t)))

(define-public crate-everything-sys-bindgen-0.1.5 (c (n "everything-sys-bindgen") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "widestring") (r "^1.0.2") (d #t) (k 2)))) (h "1lpw7026rzkrk3gd0k1c2hajp6apbsbwms1z3jag4walzj8xsl1z")))

