(define-module (crates-io ev er everywhen) #:use-module (crates-io))

(define-public crate-everywhen-0.1.0 (c (n "everywhen") (v "0.1.0") (h "1i6cnpa9bij0ag3zdkl4mwx1d37cylbv8lg86lab3g905k1j0z1h")))

(define-public crate-everywhen-0.1.1 (c (n "everywhen") (v "0.1.1") (h "1h834g0mvn4gj6nr84ayzv7c7czvlz5pvna7jhvr547n016qmwwd")))

