(define-module (crates-io ev er evermore) #:use-module (crates-io))

(define-public crate-evermore-0.1.0 (c (n "evermore") (v "0.1.0") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (k 0)) (d (n "tracing-futures") (r "^0.2") (o #t) (k 0)))) (h "0b48ljxb9ckmckjvwfn0j9g1jdwrlmwdikz0wkz2f8k1aqapn47a") (f (quote (("with-tracing" "tracing" "tracing-futures") ("with-log" "log"))))))

