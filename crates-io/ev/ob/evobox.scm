(define-module (crates-io ev ob evobox) #:use-module (crates-io))

(define-public crate-evobox-0.1.0 (c (n "evobox") (v "0.1.0") (h "0la8agjfbs8v0bwmajn4i4pd7vi41yf1kjy3m9nmxn6rdwbwn8b4")))

(define-public crate-evobox-0.1.1 (c (n "evobox") (v "0.1.1") (h "1whzaq8vzxfv95rfh8sh8rg8akw9i7zvivhvph5vd8ckg0c20mhk")))

