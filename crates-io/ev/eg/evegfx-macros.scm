(define-module (crates-io ev eg evegfx-macros) #:use-module (crates-io))

(define-public crate-evegfx-macros-0.3.0 (c (n "evegfx-macros") (v "0.3.0") (d (list (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (f (quote ("full"))) (d #t) (k 0)))) (h "0gg0s06w5sf7vsfxiywn4f6gx1mh66ch1dv0igffivi70lsmbq3w")))

