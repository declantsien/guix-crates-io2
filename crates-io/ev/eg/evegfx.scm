(define-module (crates-io ev eg evegfx) #:use-module (crates-io))

(define-public crate-evegfx-0.4.0 (c (n "evegfx") (v "0.4.0") (d (list (d (n "evegfx-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (k 0)))) (h "0bwwrkb8cannsi1k5ra8nicbb5rjsliaxfc2qscrinrfl4rsjzsp")))

(define-public crate-evegfx-0.6.0 (c (n "evegfx") (v "0.6.0") (d (list (d (n "evegfx-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (k 0)))) (h "1hi24xb70yz0398yqavsnrnc7npcplnb8imn6hhgz1mbj44m7cw1")))

