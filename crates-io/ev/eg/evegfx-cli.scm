(define-module (crates-io ev eg evegfx-cli) #:use-module (crates-io))

(define-public crate-evegfx-cli-0.5.0 (c (n "evegfx-cli") (v "0.5.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "evegfx") (r "^0.4.0") (d #t) (k 0)) (d (n "evegfx-spidriver") (r "^0.4.0") (d #t) (k 0)) (d (n "serial-core") (r "^0.4.0") (d #t) (k 0)) (d (n "serial-embedded-hal") (r "^0.1.2") (d #t) (k 0)) (d (n "spidriver") (r "^0.1.0") (d #t) (k 0)))) (h "06j1a2am7nqclb51bgy2fqw3n9c22n56hdjb0inlhav4g5w3hf66")))

(define-public crate-evegfx-cli-0.6.0 (c (n "evegfx-cli") (v "0.6.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "evegfx") (r "^0.6.0") (d #t) (k 0)) (d (n "evegfx-spidriver") (r "^0.6.0") (d #t) (k 0)) (d (n "serial-core") (r "^0.4.0") (d #t) (k 0)) (d (n "serial-embedded-hal") (r "^0.1.2") (d #t) (k 0)) (d (n "spidriver") (r "^0.1.0") (d #t) (k 0)))) (h "0b7381fw33ldsg2zzkaizx5mxma2gqcxk3qaakh0xyjbbrbm2900")))

