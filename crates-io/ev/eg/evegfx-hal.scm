(define-module (crates-io ev eg evegfx-hal) #:use-module (crates-io))

(define-public crate-evegfx-hal-0.4.0 (c (n "evegfx-hal") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "evegfx") (r "^0.4.0") (d #t) (k 0)))) (h "0qgg5na99k282mibg1ihdiplhcaqyab6q34vwdl3kf2g5gdv6a9b")))

(define-public crate-evegfx-hal-0.6.0 (c (n "evegfx-hal") (v "0.6.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "evegfx") (r "^0.6.0") (d #t) (k 0)))) (h "1w2nnbanl1l6gqhhkczxazx1yq1l7fbg8ycw6q1k7jp2srfjnf14")))

