(define-module (crates-io ev eg evegfx-spidriver) #:use-module (crates-io))

(define-public crate-evegfx-spidriver-0.4.0 (c (n "evegfx-spidriver") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "evegfx") (r "^0.4.0") (d #t) (k 0)) (d (n "evegfx-hal") (r "^0.4.0") (d #t) (k 0)) (d (n "spidriver") (r "^0.1.0") (d #t) (k 0)))) (h "1nldb0kff2xbbljkz5fda1rh8kizbv2l34hw2kjhfgak524kgv5b")))

(define-public crate-evegfx-spidriver-0.6.0 (c (n "evegfx-spidriver") (v "0.6.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "evegfx") (r "^0.6.0") (d #t) (k 0)) (d (n "evegfx-hal") (r "^0.6.0") (d #t) (k 0)) (d (n "spidriver") (r "^0.1.0") (d #t) (k 0)))) (h "1c9lvbqfb380bfg0gzhabd5nw0hrmqx4a42c3k7bxv9r1cn4rwzp")))

