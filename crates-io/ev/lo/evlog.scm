(define-module (crates-io ev lo evlog) #:use-module (crates-io))

(define-public crate-evlog-0.1.0 (c (n "evlog") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "16ysx9ifpakz4ah728rzy20rbl38mjk64qp0b5mdchl5zyn9k8hh")))

(define-public crate-evlog-0.2.0 (c (n "evlog") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1j25lmqsn6iyrn45pnpipqc4k3afikv2w8ky98da140j8m09kxsg")))

