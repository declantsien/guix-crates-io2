(define-module (crates-io ev -d ev-dice) #:use-module (crates-io))

(define-public crate-ev-dice-0.4.0 (c (n "ev-dice") (v "0.4.0") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.1.71") (d #t) (k 0)))) (h "0km4yzdhg4al4d49nxpf0sb2if5xa5m73z9swz6q4n4c7idpg9kb")))

(define-public crate-ev-dice-0.4.1 (c (n "ev-dice") (v "0.4.1") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.1.71") (d #t) (k 0)))) (h "1bqk5v9iyfmyfv8vvqpr7y2avd2072mb9jk90h9nnz8svhbdkfgn")))

(define-public crate-ev-dice-0.5.0 (c (n "ev-dice") (v "0.5.0") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)))) (h "0592avigv7cm35ay43gigwiardf4wlhqh63jihnrpxkpva5gvq8v")))

(define-public crate-ev-dice-0.5.1 (c (n "ev-dice") (v "0.5.1") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)))) (h "1qhpxwf9ag8x22m7l1pqziv7kkryflg3x1yqk6k433x3xsyjrj3i")))

