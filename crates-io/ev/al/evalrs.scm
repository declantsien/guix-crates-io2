(define-module (crates-io ev al evalrs) #:use-module (crates-io))

(define-public crate-evalrs-0.0.1 (c (n "evalrs") (v "0.0.1") (d (list (d (n "clap") (r "^2.19") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1ppd5xhq5m6x0bk19qhy5ji4drjc78404a8r2g0k88zahf73jf02")))

(define-public crate-evalrs-0.0.2 (c (n "evalrs") (v "0.0.2") (d (list (d (n "clap") (r "^2.19") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0bxh7a80j41nk1sdkzfv7dhvh7491vdd2iamyg70fbr8qjl7l6lx")))

(define-public crate-evalrs-0.0.3 (c (n "evalrs") (v "0.0.3") (d (list (d (n "clap") (r "^2.19") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "06y05iik4apqqv3944q68iksvfnxlydmbqafyz5b9sd1zkvx9g7n")))

(define-public crate-evalrs-0.0.4 (c (n "evalrs") (v "0.0.4") (d (list (d (n "clap") (r "^2.19") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0qjh9ms6jnlszfy2xcm3xf2va391bis19xdi4s6kxdhxxddbr88c")))

(define-public crate-evalrs-0.0.5 (c (n "evalrs") (v "0.0.5") (d (list (d (n "clap") (r "^2.19") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1mk5drdaaxl0ij89zqnpkw6nylxy3q331i4m8nhl44c2niky96dh")))

(define-public crate-evalrs-0.0.6 (c (n "evalrs") (v "0.0.6") (d (list (d (n "clap") (r "^2.19") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1lki9ig2xav62rrvw2zjc92y3nchc1x1wphfwl5rbyb2vyx81g9p")))

(define-public crate-evalrs-0.0.7 (c (n "evalrs") (v "0.0.7") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0bzirnyqapr4la8z6ax19sjlszz7lawhgwb7drz9s5df1lly9wki")))

(define-public crate-evalrs-0.0.8 (c (n "evalrs") (v "0.0.8") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1kw0ac2xh9xls8g2invgq2hq2paryynlxmxbah95qwq379hp57ad")))

(define-public crate-evalrs-0.0.9 (c (n "evalrs") (v "0.0.9") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1wy9nqvdhw4zx6d4v9yi4vphhjihnys5m31fnswvxnfz0z877md1")))

(define-public crate-evalrs-0.0.10 (c (n "evalrs") (v "0.0.10") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "10dwb4rx4r46w3kxiisi2v78xpbxw00yhwyl84m4dd7gzx1yf3i5")))

(define-public crate-evalrs-0.0.11 (c (n "evalrs") (v "0.0.11") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0hjp4dhqxbiv2qjyyzdfd4wdzv9477l5yfxc9nbx55vn56glmfz9")))

(define-public crate-evalrs-0.0.12 (c (n "evalrs") (v "0.0.12") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1vff8w85ky8ngirp168r244qpvysbswn47825m1xivvss2mxjnw9")))

(define-public crate-evalrs-0.0.13 (c (n "evalrs") (v "0.0.13") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1a1wxcs8a3h86f15rav8ard8i6bcv5zqyiz6639br9fy615qdfyg")))

