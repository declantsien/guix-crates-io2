(define-module (crates-io ev al evaluator) #:use-module (crates-io))

(define-public crate-evaluator-0.2.0 (c (n "evaluator") (v "0.2.0") (h "0l8rq0cq0k8l0arz3drbvvrfz724dg3xdj7a95v64wp08199kqxl")))

(define-public crate-evaluator-0.2.1 (c (n "evaluator") (v "0.2.1") (h "09jrjcg0rz9j7h69yiacm8gz0g91baxmcjlqaxssr3pwcd9lk47p")))

(define-public crate-evaluator-1.0.0 (c (n "evaluator") (v "1.0.0") (h "16i8kp5vir8sxnbmfvmdcpb2hyjcfy0hjd8k0nvlrl6jvp3fvhxv") (y #t)))

(define-public crate-evaluator-1.0.1 (c (n "evaluator") (v "1.0.1") (h "0hymp8784zlvpb7faj3r4fz9ncrl5p5sg5fagpsa44nilrmr6lhs")))

