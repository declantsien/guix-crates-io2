(define-module (crates-io ev al evalchroma) #:use-module (crates-io))

(define-public crate-evalchroma-0.1.0 (c (n "evalchroma") (v "0.1.0") (d (list (d (n "imgref") (r "^1.3.5") (d #t) (k 0)) (d (n "rgb") (r "^0.8.10") (d #t) (k 0)))) (h "0fyf6k694cnfq8pcqh11zj6shg26ijiai1jd522sy3098sl802w3") (y #t)))

(define-public crate-evalchroma-0.1.1 (c (n "evalchroma") (v "0.1.1") (d (list (d (n "imgref") (r "^1.3.5") (d #t) (k 0)) (d (n "rgb") (r "^0.8.11") (d #t) (k 0)))) (h "1xlafdc5k44fa4rbk9rpblan643pihnsjrk0s7imcg135nrinb4n") (y #t)))

(define-public crate-evalchroma-0.1.2 (c (n "evalchroma") (v "0.1.2") (d (list (d (n "imgref") (r "^1.7.0") (d #t) (k 0)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)))) (h "1ahrys7mhlwd14iv70gyn09dw4pnh4sf47k378jz29aaiz50rxwb") (y #t)))

(define-public crate-evalchroma-1.0.0 (c (n "evalchroma") (v "1.0.0") (d (list (d (n "imgref") (r "^1.9.0") (d #t) (k 0)) (d (n "rgb") (r "^0.8.27") (d #t) (k 0)))) (h "0qzaspz83qigj5f0yz1l9ijfqgfr44avgdwih0y8msmbmn36iifc")))

(define-public crate-evalchroma-1.0.1 (c (n "evalchroma") (v "1.0.1") (d (list (d (n "imgref") (r "^1.9.1") (d #t) (k 0)) (d (n "rgb") (r "^0.8.30") (d #t) (k 0)))) (h "0rmn1jm7b96i30xcsnf7kyc6g5dkls0ynv0h3vc9gxnh211vjnxp")))

(define-public crate-evalchroma-1.0.2 (c (n "evalchroma") (v "1.0.2") (d (list (d (n "imgref") (r "^1.9.1") (d #t) (k 0)) (d (n "lodepng") (r "^3.7") (d #t) (k 2)) (d (n "rgb") (r "^0.8.30") (d #t) (k 0)))) (h "0sr3f5r83pxvpv9cq9ybsybgmgkca8i9kifv28xsbks6p6hbw33m")))

