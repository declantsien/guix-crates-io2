(define-module (crates-io ev al evaltrees) #:use-module (crates-io))

(define-public crate-evaltrees-0.1.0 (c (n "evaltrees") (v "0.1.0") (d (list (d (n "display_attr") (r "^0.1.1") (d #t) (k 0)) (d (n "either") (r "^1.5.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lalrpop") (r "^0.15.2") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.15.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.1") (d #t) (k 0)) (d (n "linked_hash_set") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "symbol") (r "^0.1.2") (d #t) (k 0)))) (h "0vawy3d0ssjijgb6cfywxsawyrip4slzgpx95qf2nb9rjzwnghh5")))

