(define-module (crates-io ev al evaluroll) #:use-module (crates-io))

(define-public crate-evaluroll-0.1.0 (c (n "evaluroll") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.18") (d #t) (k 2)) (d (n "peg") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_hc") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (d #t) (k 2)))) (h "0wwbk3ycsgs1gziqyg97rnmfpq5xyj23skla113v46fiyc8g03na") (f (quote (("trace" "peg/trace"))))))

(define-public crate-evaluroll-0.1.1 (c (n "evaluroll") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.18") (d #t) (k 2)) (d (n "peg") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_hc") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^1.8") (d #t) (k 2)))) (h "1z19h0qkvckbgw739gh0xdvn7kwvzi7lffd519qgazx1b6gmkhn0") (f (quote (("trace" "peg/trace"))))))

