(define-module (crates-io ev al evaluator_rs) #:use-module (crates-io))

(define-public crate-evaluator_rs-0.1.0 (c (n "evaluator_rs") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.19.7") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "08d09p6w7rpmdyjxpmwy27hb8iaz4skx22ddjicvk1rh43m49vbd")))

(define-public crate-evaluator_rs-0.1.1 (c (n "evaluator_rs") (v "0.1.1") (d (list (d (n "lalrpop") (r "^0.19.7") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "07vyxq8dl39i6y38yj0ph495ca3s6s936fb3qi0v5dbxdpmpkp2d")))

(define-public crate-evaluator_rs-0.1.2 (c (n "evaluator_rs") (v "0.1.2") (d (list (d (n "lalrpop") (r "^0.19.7") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "19x7qjlf74ni6bvxbw2pdm14nw0dfhzgji346jq42f2fwa81kp7d")))

(define-public crate-evaluator_rs-0.1.3 (c (n "evaluator_rs") (v "0.1.3") (d (list (d (n "lalrpop") (r "^0.19") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0sln4bm0if7b6xqsgk8cdvwx5r8p70bvrdw1123z8am1n92zfzji")))

