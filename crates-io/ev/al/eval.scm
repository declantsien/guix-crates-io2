(define-module (crates-io ev al eval) #:use-module (crates-io))

(define-public crate-eval-0.1.0 (c (n "eval") (v "0.1.0") (d (list (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1zj0w5c2g5yyygjv1svbdrc68wai69k7zr5d32hfgdj345kcag83")))

(define-public crate-eval-0.1.1 (c (n "eval") (v "0.1.1") (d (list (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1pz0vqcqn6ii5npviw184xjrkqpl7zn4lv2vmw0xw05ygv4z7ysz")))

(define-public crate-eval-0.2.0 (c (n "eval") (v "0.2.0") (d (list (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "129swn6q7dngplw4y44x0addmbsfhnnyyvkxk5qq2xjf3w5fi72r")))

(define-public crate-eval-0.3.0 (c (n "eval") (v "0.3.0") (d (list (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "017cc1sfg4bpg2dvdspms4rsz4wcqypn6vnmj3h37qsw0j3rlnca")))

(define-public crate-eval-0.3.1 (c (n "eval") (v "0.3.1") (d (list (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "11rl3ar82g1wyc5kbi89xvnm72m3s6si3wpy7cqhi8vbg1f6j03d") (f (quote (("unstable"))))))

(define-public crate-eval-0.3.2 (c (n "eval") (v "0.3.2") (d (list (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "149zdwba74pzh4y0w3d1nz97ky5n625v28d4rw6w5vah1ahpb39v") (f (quote (("unstable"))))))

(define-public crate-eval-0.4.0 (c (n "eval") (v "0.4.0") (d (list (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0bzjbriqh8vb98fds91hk0d6p0qv3pr6mq8ay8qn3zsizdizdjd5") (f (quote (("unstable"))))))

(define-public crate-eval-0.4.1 (c (n "eval") (v "0.4.1") (d (list (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1qcims60bg2s3jx8lbw5iabyp406skhg6zlwmbkr46mi6k1cx443") (f (quote (("unstable"))))))

(define-public crate-eval-0.4.2 (c (n "eval") (v "0.4.2") (d (list (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r ">= 0.9, < 2") (d #t) (k 0)) (d (n "serde_json") (r ">= 0.9, < 2") (d #t) (k 0)))) (h "048ms62plqy47b0p39axiy585aksnck7g5m291lcx5mvcwny3ks6") (f (quote (("unstable"))))))

(define-public crate-eval-0.4.3 (c (n "eval") (v "0.4.3") (d (list (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r ">= 0.9, < 2") (d #t) (k 0)) (d (n "serde_json") (r ">= 0.9, < 2") (d #t) (k 0)))) (h "0hicaxmvsw8qq427mhf42dacppfx2cnv8h23laqayrqcxndw345s") (f (quote (("unstable"))))))

