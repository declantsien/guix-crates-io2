(define-module (crates-io ev al eval-md) #:use-module (crates-io))

(define-public crate-eval-md-0.1.0 (c (n "eval-md") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "08ayk0d5b4270906idb1qjp11rd3h9p9h0f278k1s6akh9fizhgd")))

(define-public crate-eval-md-0.2.0 (c (n "eval-md") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ir49jk7v275lv19z0vppcrs4s46gh16mywmdcm13mdabdnn5n12")))

(define-public crate-eval-md-0.3.0 (c (n "eval-md") (v "0.3.0") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h29mx14aqhiz75xgycad2s3w6cc66xdbbqdl7v4y1b059538aqm")))

(define-public crate-eval-md-0.4.0 (c (n "eval-md") (v "0.4.0") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "02fx71h964zjg6aj3p4i57f1mxf13x6k3jm5s96f8g4z547dyxhn")))

(define-public crate-eval-md-0.4.1 (c (n "eval-md") (v "0.4.1") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "14l39mj9f93wkmzpvb4kdgah1mbihbgmgmrgcg91gq8m6rb172zr")))

(define-public crate-eval-md-0.5.0 (c (n "eval-md") (v "0.5.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pgj5gkwlpvwk3mf1ycjpj144y48x4g9gydjxzmba08mr4wrv5n3")))

(define-public crate-eval-md-0.5.1 (c (n "eval-md") (v "0.5.1") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "05hwh6ngm6x2b36xa0w1pdl1qp88i0hr7g00hlrm7rz7m7s0xzsr")))

(define-public crate-eval-md-0.5.2 (c (n "eval-md") (v "0.5.2") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x28izv4y1acgxdkl85b7jzgcd8d8034zdmr0hrsryj9imrq5gzi")))

(define-public crate-eval-md-0.5.3 (c (n "eval-md") (v "0.5.3") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "15kl2wavj9j040jmi5hw5fb48a7kh102kabs2bq0427jjqhf7hfz")))

(define-public crate-eval-md-0.5.4 (c (n "eval-md") (v "0.5.4") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vjsa2g2sqcfkrhj135ad0jyjalgnb7m0kr9gz8vwx22dbw6qalr")))

(define-public crate-eval-md-1.0.0 (c (n "eval-md") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xh8fppzy4nlnajbs1fhj5shdiq0shvl1ky81nb1y4hwihrmxswp")))

(define-public crate-eval-md-1.1.0 (c (n "eval-md") (v "1.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z38rrggsfky49fhjj9yngf982scvnd7plwf4zhxmhnrnk80ms0w")))

