(define-module (crates-io ev al evaluatorrs) #:use-module (crates-io))

(define-public crate-evaluatorrs-0.0.1 (c (n "evaluatorrs") (v "0.0.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "evalexpr") (r "^11.0.1") (d #t) (k 2)) (d (n "libm") (r "^0.2.7") (o #t) (d #t) (k 0)))) (h "05b0qb8mbzk9rr2x7jrc6hpzy3s852y5370mvq1qih1nhq8iy8rn") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("libm" "dep:libm")))) (r "1.72")))

