(define-module (crates-io ev m- evm-network-ellaism) #:use-module (crates-io))

(define-public crate-evm-network-ellaism-0.11.0-beta.0 (c (n "evm-network-ellaism") (v "0.11.0-beta.0") (d (list (d (n "etcommon-bigint") (r "^0.2") (k 0)) (d (n "evm") (r "^0.11.0-beta") (k 0)))) (h "16rg7mkl0m18f2737vllm86c4m5lsqricg9ic71ksvp1cqwzsr8d") (f (quote (("std" "evm/std") ("rust-secp256k1" "evm/rust-secp256k1") ("rlp" "etcommon-bigint/rlp") ("default" "std" "c-secp256k1") ("c-secp256k1" "evm/c-secp256k1"))))))

(define-public crate-evm-network-ellaism-0.11.0-beta.2 (c (n "evm-network-ellaism") (v "0.11.0-beta.2") (d (list (d (n "ethereum-bigint") (r "^0.2") (k 0)) (d (n "evm") (r "^0.11.0-beta.2") (k 0)))) (h "0g5a3y8n1ziip1kg135lb54im7r7c3bbhkrsslmzy8ql6jg55aa0") (f (quote (("std" "evm/std") ("rust-secp256k1" "evm/rust-secp256k1") ("rlp" "ethereum-bigint/rlp") ("default" "std" "rust-secp256k1") ("c-secp256k1" "evm/c-secp256k1"))))))

(define-public crate-evm-network-ellaism-0.11.0-beta.3 (c (n "evm-network-ellaism") (v "0.11.0-beta.3") (d (list (d (n "ethereum-bigint") (r "^0.2") (k 0)) (d (n "evm") (r "^0.11.0-beta.3") (k 0)))) (h "05cby9500b9d6br4g7a0ap30c5hglg25shw2lbxf4lfri2lxkr5f") (f (quote (("std" "evm/std") ("rust-secp256k1" "evm/rust-secp256k1") ("rlp" "ethereum-bigint/rlp") ("default" "std" "rust-secp256k1") ("c-secp256k1" "evm/c-secp256k1"))))))

(define-public crate-evm-network-ellaism-0.11.0 (c (n "evm-network-ellaism") (v "0.11.0") (d (list (d (n "ethereum-bigint") (r "^0.2") (k 0)) (d (n "evm") (r "^0.11") (k 0)))) (h "09j9b4rchcwhkcm7f55w21vny1wfsfcgh1hrkdnyfinkl22nk2wr") (f (quote (("std" "evm/std") ("rust-secp256k1" "evm/rust-secp256k1") ("rlp" "ethereum-bigint/rlp") ("default" "std" "rust-secp256k1") ("c-secp256k1" "evm/c-secp256k1"))))))

