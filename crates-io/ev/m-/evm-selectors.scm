(define-module (crates-io ev m- evm-selectors) #:use-module (crates-io))

(define-public crate-evm-selectors-0.1.0 (c (n "evm-selectors") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "ethers") (r "^2.0.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (o #t) (d #t) (k 0)))) (h "167rz6h2m391knhai34gjcpnm3256rpxghj0k29zxhskpm2bpjnb") (f (quote (("download" "reqwest") ("default" "download"))))))

(define-public crate-evm-selectors-0.1.1 (c (n "evm-selectors") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "ethers") (r "^2.0.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (o #t) (d #t) (k 0)))) (h "1k1d2bj5gg8w98sp9ajgm4l4id6rhxvpfn6i7ljfx7914k108fbf") (f (quote (("download" "reqwest") ("default" "download"))))))

