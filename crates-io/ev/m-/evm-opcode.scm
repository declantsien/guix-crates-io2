(define-module (crates-io ev m- evm-opcode) #:use-module (crates-io))

(define-public crate-evm-opcode-0.0.0 (c (n "evm-opcode") (v "0.0.0") (h "0dcx18q4favsm2all8kxsgphz0sg110cnbsx8avn2xjqkifsm743")))

(define-public crate-evm-opcode-0.1.0 (c (n "evm-opcode") (v "0.1.0") (h "1gnkqp7rjc84k64mrmknv86b1hscz8f7yagm9bw3xx3afmp06198")))

