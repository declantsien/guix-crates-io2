(define-module (crates-io ev m- evm-precompiles-derive) #:use-module (crates-io))

(define-public crate-evm-precompiles-derive-0.2.1 (c (n "evm-precompiles-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1yi3ifgsmf3b8bnmdh1vwrilbcz36dlncgzfrj275y1pal6w86x3")))

