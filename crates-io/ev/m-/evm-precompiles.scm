(define-module (crates-io ev m- evm-precompiles) #:use-module (crates-io))

(define-public crate-evm-precompiles-0.2.1 (c (n "evm-precompiles") (v "0.2.1") (d (list (d (n "ethereum") (r "^0.11.1") (f (quote ("with-serde"))) (k 0)) (d (n "evm") (r "^0.33.1") (f (quote ("with-serde"))) (k 0)) (d (n "evm-precompiles-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.6") (k 0)) (d (n "primitive-types") (r "^0.10.1") (k 0)) (d (n "sha3") (r "^0.10.1") (k 0)))) (h "1n75z7ivafdchq0kqym0920f6kmj4qdpmdz9r094ygy90wsfmm27")))

