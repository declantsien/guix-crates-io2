(define-module (crates-io ev m- evm-disassembler) #:use-module (crates-io))

(define-public crate-evm-disassembler-0.1.0 (c (n "evm-disassembler") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "ethers") (r "^2.0.0") (d #t) (k 2)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0340b004pw1z9m643w1dbs5xkgh7k9a3aczq279r05wyzn185c7n")))

(define-public crate-evm-disassembler-0.2.0 (c (n "evm-disassembler") (v "0.2.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "ethers") (r "^2.0.0") (d #t) (k 2)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1adl9n2jpdw87lvaqm7l6n6fcaqhxzjng2b19mncfbbx2ws76kjq")))

(define-public crate-evm-disassembler-0.3.0 (c (n "evm-disassembler") (v "0.3.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "ethers") (r "^2.0.0") (d #t) (k 2)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0bd5bs39kj7a96z0g2ahm3w6kf2xqxzgmn61lym29fppy1wbgy3y")))

(define-public crate-evm-disassembler-0.4.0 (c (n "evm-disassembler") (v "0.4.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "ethers") (r "^2.0.0") (d #t) (k 2)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1ic4ncr9nc14x3ilmih4ybdp71xx19pp89mawr9h28d36b3wjvqg")))

(define-public crate-evm-disassembler-0.5.0 (c (n "evm-disassembler") (v "0.5.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "ethers") (r "^2.0.0") (d #t) (k 2)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0n3jp8xp66h66ina8cdm37dq5rziws27svm5kdlgy5bky3cqbmny")))

