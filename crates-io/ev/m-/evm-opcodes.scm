(define-module (crates-io ev m- evm-opcodes) #:use-module (crates-io))

(define-public crate-evm-opcodes-0.0.0 (c (n "evm-opcodes") (v "0.0.0") (h "04rk6wwy8dcybdfa6vr8mcxrcpics080zrplvksb85icc1ins316")))

(define-public crate-evm-opcodes-0.0.1 (c (n "evm-opcodes") (v "0.0.1") (d (list (d (n "paste") (r "^1.0.13") (d #t) (k 0)))) (h "0blb8bs8mqmiidqm40m6mcmwznjmw35xh9im5hxzm1qi6f481b1f")))

(define-public crate-evm-opcodes-0.0.2 (c (n "evm-opcodes") (v "0.0.2") (d (list (d (n "paste") (r "^1.0.13") (d #t) (k 0)))) (h "1pndq7h5qlxlvrmb39yihkfs6mfk6fa0wm0gmnm28nfxyck747cm")))

(define-public crate-evm-opcodes-0.0.3 (c (n "evm-opcodes") (v "0.0.3") (d (list (d (n "paste") (r "^1.0.13") (d #t) (k 0)))) (h "0aj655zwdps92c62qify6q2sjws40mfizjvp8zrjw3n2sf0q2gj9") (f (quote (("data"))))))

