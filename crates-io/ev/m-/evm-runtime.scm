(define-module (crates-io ev m- evm-runtime) #:use-module (crates-io))

(define-public crate-evm-runtime-0.13.0 (c (n "evm-runtime") (v "0.13.0") (d (list (d (n "evm-core") (r "^0.13") (k 0)) (d (n "primitive-types") (r "^0.6") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "1iixnb6fslpbxmkf108yzwr0237aqw6ry47w7pj36x9vyqdb94qg") (f (quote (("std" "evm-core/std" "primitive-types/std" "sha3/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.13.1 (c (n "evm-runtime") (v "0.13.1") (d (list (d (n "evm-core") (r "^0.13") (k 0)) (d (n "primitive-types") (r "^0.6") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "06cyf9cisazj8c8s5gcjgqw3dllhc1bxcmrf9rgwh60amnvxmw60") (f (quote (("std" "evm-core/std" "primitive-types/std" "sha3/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.14.0 (c (n "evm-runtime") (v "0.14.0") (d (list (d (n "evm-core") (r "^0.14") (k 0)) (d (n "primitive-types") (r "^0.6") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "17wspvc8kpqi9vsvkk8kfviv925l4cvdz9sw52j4irkiq0z4w8zq") (f (quote (("std" "evm-core/std" "primitive-types/std" "sha3/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.14.1 (c (n "evm-runtime") (v "0.14.1") (d (list (d (n "evm-core") (r "^0.14") (k 0)) (d (n "primitive-types") (r "^0.6") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "0jrf24hmka2jifrgiqr7xvlaqh1d8ggqv9vvq4i3g33156fwifqd") (f (quote (("std" "evm-core/std" "primitive-types/std" "sha3/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.15.0 (c (n "evm-runtime") (v "0.15.0") (d (list (d (n "evm-core") (r "^0.15") (k 0)) (d (n "primitive-types") (r "^0.6") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "13s25cy1zw7bqhdnxi4gdvjghddl1afa92kcqylp2sdjgx24p7iq") (f (quote (("std" "evm-core/std" "primitive-types/std" "sha3/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.16.0 (c (n "evm-runtime") (v "0.16.0") (d (list (d (n "evm-core") (r "^0.16") (k 0)) (d (n "primitive-types") (r "^0.7") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "19afq7zs1f8faf1l92rgjvlp5l69fc4xr9krwsw96g3dj1dxq1fa") (f (quote (("std" "evm-core/std" "primitive-types/std" "sha3/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.16.1 (c (n "evm-runtime") (v "0.16.1") (d (list (d (n "evm-core") (r "^0.16") (k 0)) (d (n "primitive-types") (r "^0.7") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "00slp7gf80y6c2r4g8f7l4b3am33c9cgy54fq8y7s00qjlpw4cqq") (f (quote (("std" "evm-core/std" "primitive-types/std" "sha3/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.17.0 (c (n "evm-runtime") (v "0.17.0") (d (list (d (n "evm-core") (r "^0.17") (k 0)) (d (n "primitive-types") (r "^0.7") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "010vdfg3fa7dm132ky6g8lx9yygrjs7ypc02r8zks82jg9kza43l") (f (quote (("std" "evm-core/std" "primitive-types/std" "sha3/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.17.1 (c (n "evm-runtime") (v "0.17.1") (d (list (d (n "evm-core") (r "^0.17") (k 0)) (d (n "primitive-types") (r "^0.7") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "0nnlgh1ylgdi4ak67yn87sjgkbyi55mfli1a4vvz6kizhfgz58zb") (f (quote (("std" "evm-core/std" "primitive-types/std" "sha3/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.17.2 (c (n "evm-runtime") (v "0.17.2") (d (list (d (n "evm-core") (r "^0.17") (k 0)) (d (n "primitive-types") (r "^0.7") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "1f20pkcznq4g9zvccylilkdr1msp4iq0v29ca643g0vgx7zx3hgj") (f (quote (("std" "evm-core/std" "primitive-types/std" "sha3/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.18.0 (c (n "evm-runtime") (v "0.18.0") (d (list (d (n "evm-core") (r "^0.18") (k 0)) (d (n "primitive-types") (r "^0.7") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "1ihsbrwsli5kxm2rnjml36ybgq6annnmbly9vfrrw5lrqqw6771s") (f (quote (("std" "evm-core/std" "primitive-types/std" "sha3/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.18.3 (c (n "evm-runtime") (v "0.18.3") (d (list (d (n "evm-core") (r "^0.18") (k 0)) (d (n "primitive-types") (r "^0.7") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "1r014v9wmp4hxys2k3mdklkrwlisvz4kqv1wl0dg62iy3fnli8b1") (f (quote (("std" "evm-core/std" "primitive-types/std" "sha3/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.19.0 (c (n "evm-runtime") (v "0.19.0") (d (list (d (n "evm-core") (r "^0.19") (k 0)) (d (n "primitive-types") (r "^0.7") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "0rsyz76m3z37gbn2nk3hpwddp4f8viv1sj531xacl4fdvw1lsllm") (f (quote (("std" "evm-core/std" "primitive-types/std" "sha3/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.20.0 (c (n "evm-runtime") (v "0.20.0") (d (list (d (n "evm-core") (r "^0.20") (k 0)) (d (n "primitive-types") (r "^0.7") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "1ypkplc314hb4p0y0ws0qd37jv657phqgcv4l8sl7wvp6c66syjh") (f (quote (("std" "evm-core/std" "primitive-types/std" "sha3/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.21.0 (c (n "evm-runtime") (v "0.21.0") (d (list (d (n "evm-core") (r "^0.21") (k 0)) (d (n "primitive-types") (r "^0.8") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "0l1248r46i4r5s0ih8mlk3gj3cqbsy6vviq6cw270j7z6lqyh8hx") (f (quote (("std" "evm-core/std" "primitive-types/std" "sha3/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.22.0 (c (n "evm-runtime") (v "0.22.0") (d (list (d (n "evm-core") (r "^0.22") (k 0)) (d (n "primitive-types") (r "^0.8") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "1bzwga08sp9ji1mpc16n37mns6r7zbsi6zi3z7q3w9xbm0z3mar5") (f (quote (("std" "evm-core/std" "primitive-types/std" "sha3/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.23.0 (c (n "evm-runtime") (v "0.23.0") (d (list (d (n "evm-core") (r "^0.23") (k 0)) (d (n "primitive-types") (r "^0.8") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "11i1033kd9mb6mv4lifvqjhm53p5a1cnh3ihxv8nxxy5vcc62wlm") (f (quote (("std" "evm-core/std" "primitive-types/std" "sha3/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.24.0 (c (n "evm-runtime") (v "0.24.0") (d (list (d (n "evm-core") (r "^0.24") (k 0)) (d (n "primitive-types") (r "^0.9") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "11sdm9p40lqgry2vfdm6zfy0xai56b2vbfc2x5x5mg46m96m0iw4") (f (quote (("std" "evm-core/std" "primitive-types/std" "sha3/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.25.0 (c (n "evm-runtime") (v "0.25.0") (d (list (d (n "evm-core") (r "^0.25") (k 0)) (d (n "primitive-types") (r "^0.9") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "01vnkg75h2g7pmbswkpjwiwl35k9g112v99a70maifx8kkr05hsl") (f (quote (("std" "evm-core/std" "primitive-types/std" "sha3/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.26.0 (c (n "evm-runtime") (v "0.26.0") (d (list (d (n "evm-core") (r "^0.26") (k 0)) (d (n "primitive-types") (r "^0.9") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "18a59cacx4hjmallbm5sl660zj2dy8zvkfdda8iywp2kwl8ga20c") (f (quote (("std" "evm-core/std" "primitive-types/std" "sha3/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.27.0 (c (n "evm-runtime") (v "0.27.0") (d (list (d (n "environmental") (r "^1.1.2") (o #t) (k 0)) (d (n "evm-core") (r "^0.27") (k 0)) (d (n "primitive-types") (r "^0.9") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "06x3dj474621gqqzp1hmd58y40wjzaf1zb9kd6r39cy32ss9c8lm") (f (quote (("tracing" "environmental") ("std" "evm-core/std" "primitive-types/std" "sha3/std" "environmental/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.28.0 (c (n "evm-runtime") (v "0.28.0") (d (list (d (n "environmental") (r "^1.1.2") (o #t) (k 0)) (d (n "evm-core") (r "^0.28") (k 0)) (d (n "primitive-types") (r "^0.9") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "0lvx46d1lcs2jdqazl3y7axv14djc4ds7n4pk5hisjq06gl8lysb") (f (quote (("tracing" "environmental") ("std" "evm-core/std" "primitive-types/std" "sha3/std" "environmental/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.29.0 (c (n "evm-runtime") (v "0.29.0") (d (list (d (n "environmental") (r "^1.1.2") (o #t) (k 0)) (d (n "evm-core") (r "^0.29") (k 0)) (d (n "primitive-types") (r "^0.10") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "1iskkcshwj8q33qha4vcr7s3afpqyrp9v7cj0jddj70q8is4dmv2") (f (quote (("tracing" "environmental") ("std" "evm-core/std" "primitive-types/std" "sha3/std" "environmental/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.30.0 (c (n "evm-runtime") (v "0.30.0") (d (list (d (n "environmental") (r "^1.1.2") (o #t) (k 0)) (d (n "evm-core") (r "^0.30") (k 0)) (d (n "primitive-types") (r "^0.10") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "00av2b09mzy6hjiyxn6c501lg57ghrkfybrrjckd24ygcj4sq89r") (f (quote (("tracing" "environmental") ("std" "evm-core/std" "primitive-types/std" "sha3/std" "environmental/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.31.0 (c (n "evm-runtime") (v "0.31.0") (d (list (d (n "auto_impl") (r "^0.4.1") (d #t) (k 0)) (d (n "environmental") (r "^1.1.2") (o #t) (k 0)) (d (n "evm-core") (r "^0.31") (k 0)) (d (n "primitive-types") (r "^0.10") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "191lla2n3aqid5fcb2prv5dp0cjgiq7cibv7bgqixhq4l7n8l5c6") (f (quote (("tracing" "environmental") ("std" "evm-core/std" "primitive-types/std" "sha3/std" "environmental/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.32.0 (c (n "evm-runtime") (v "0.32.0") (d (list (d (n "environmental") (r "^1.1.2") (o #t) (k 0)) (d (n "evm-core") (r "^0.32") (k 0)) (d (n "primitive-types") (r "^0.10") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "1v5l209z4rdpmzqzmzx87g208lprv1mdcldk8kck3vrqy3hnhqf8") (f (quote (("tracing" "environmental") ("std" "evm-core/std" "primitive-types/std" "sha3/std" "environmental/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.33.0 (c (n "evm-runtime") (v "0.33.0") (d (list (d (n "environmental") (r "^1.1.2") (o #t) (k 0)) (d (n "evm-core") (r "^0.33") (k 0)) (d (n "primitive-types") (r "^0.10") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "16y4zzs4iy9apqh2jfwi80cjsn34c901k6n09f50m1bfmhs897j1") (f (quote (("tracing" "environmental") ("std" "evm-core/std" "primitive-types/std" "sha3/std" "environmental/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.34.0 (c (n "evm-runtime") (v "0.34.0") (d (list (d (n "auto_impl") (r "^0.5.0") (d #t) (k 0)) (d (n "environmental") (r "^1.1.2") (o #t) (k 0)) (d (n "evm-core") (r "^0.34") (k 0)) (d (n "primitive-types") (r ">=0.10, <=0.11") (k 0)) (d (n "sha3") (r "^0.8") (k 0)))) (h "0yh0jqf0j5y6ksvzshf0dwd4czm7xicrsy8gc45y5c109sfjc2f7") (f (quote (("tracing" "environmental") ("std" "evm-core/std" "primitive-types/std" "sha3/std" "environmental/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.35.0 (c (n "evm-runtime") (v "0.35.0") (d (list (d (n "auto_impl") (r "^0.5.0") (d #t) (k 0)) (d (n "environmental") (r "^1.1.2") (o #t) (k 0)) (d (n "evm-core") (r "^0.35") (k 0)) (d (n "primitive-types") (r "^0.11") (k 0)) (d (n "sha3") (r "^0.10") (k 0)))) (h "0pbiqyq3magvip059m33xywp2jlp4pxa3bmgh4swy3x73kwpl5gn") (f (quote (("tracing" "environmental") ("std" "evm-core/std" "primitive-types/std" "sha3/std" "environmental/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.36.0 (c (n "evm-runtime") (v "0.36.0") (d (list (d (n "auto_impl") (r "^0.5.0") (d #t) (k 0)) (d (n "environmental") (r "^1.1.2") (o #t) (k 0)) (d (n "evm-core") (r "^0.36") (k 0)) (d (n "primitive-types") (r "^0.11") (k 0)) (d (n "sha3") (r "^0.10") (k 0)))) (h "1kcddwx3nl5ki6rvvdj7x7f38wq326prcjhqlp8s9qq2b9ac4j2d") (f (quote (("tracing" "environmental") ("std" "evm-core/std" "primitive-types/std" "sha3/std" "environmental/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.37.0 (c (n "evm-runtime") (v "0.37.0") (d (list (d (n "auto_impl") (r "^1.0") (d #t) (k 0)) (d (n "environmental") (r "^1.1.2") (o #t) (k 0)) (d (n "evm-core") (r "^0.37") (k 0)) (d (n "primitive-types") (r "^0.12") (k 0)) (d (n "sha3") (r "^0.10") (k 0)))) (h "1b8kbfqajzqqnlbgqkmqggakrracfq9l0z1ri23a5wb4rrcr96y7") (f (quote (("tracing" "environmental") ("std" "environmental/std" "primitive-types/std" "sha3/std" "evm-core/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.38.0 (c (n "evm-runtime") (v "0.38.0") (d (list (d (n "auto_impl") (r "^1.0") (d #t) (k 0)) (d (n "environmental") (r "^1.1.2") (o #t) (k 0)) (d (n "evm-core") (r "^0.38") (k 0)) (d (n "primitive-types") (r "^0.12") (k 0)) (d (n "sha3") (r "^0.10") (k 0)))) (h "1cydzd88g32rc5bhi1i0x463dxjcxkb0ly3vymfhfwgf2167xp9y") (f (quote (("tracing" "environmental") ("std" "environmental/std" "primitive-types/std" "sha3/std" "evm-core/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.39.0 (c (n "evm-runtime") (v "0.39.0") (d (list (d (n "auto_impl") (r "^1.0") (d #t) (k 0)) (d (n "environmental") (r "^1.1.2") (o #t) (k 0)) (d (n "evm-core") (r "^0.39") (k 0)) (d (n "primitive-types") (r "^0.12") (k 0)) (d (n "sha3") (r "^0.10") (k 0)))) (h "0kvzwcw7810qwinwnz9v4s8h54l4qzjh904pa5b2ln7cb4pv799a") (f (quote (("tracing" "environmental") ("std" "environmental/std" "primitive-types/std" "sha3/std" "evm-core/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.40.0 (c (n "evm-runtime") (v "0.40.0") (d (list (d (n "auto_impl") (r "^1.0") (d #t) (k 0)) (d (n "environmental") (r "^1.1.2") (o #t) (k 0)) (d (n "evm-core") (r "^0.40") (k 0)) (d (n "primitive-types") (r "^0.12") (k 0)) (d (n "sha3") (r "^0.10") (k 0)))) (h "04y7d0jlnxk32n7lhr9k7i18a3kz8ccp6wwnd935p83blkjrrbrx") (f (quote (("tracing" "environmental") ("std" "environmental/std" "primitive-types/std" "sha3/std" "evm-core/std") ("default" "std"))))))

(define-public crate-evm-runtime-0.41.0 (c (n "evm-runtime") (v "0.41.0") (d (list (d (n "auto_impl") (r "^1.0") (d #t) (k 0)) (d (n "environmental") (r "^1.1.2") (o #t) (k 0)) (d (n "evm-core") (r "^0.41") (k 0)) (d (n "primitive-types") (r "^0.12") (k 0)) (d (n "sha3") (r "^0.10") (k 0)))) (h "1ka2xy4anj1k8idg38gnnk04ldx6znv1p3042jas44xfcjdy1fw4") (f (quote (("tracing" "environmental") ("std" "environmental/std" "primitive-types/std" "sha3/std" "evm-core/std") ("default" "std"))))))

