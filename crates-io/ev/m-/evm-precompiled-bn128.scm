(define-module (crates-io ev m- evm-precompiled-bn128) #:use-module (crates-io))

(define-public crate-evm-precompiled-bn128-0.11.0-beta.0 (c (n "evm-precompiled-bn128") (v "0.11.0-beta.0") (d (list (d (n "bn-plus") (r "^0.4") (d #t) (k 0)) (d (n "etcommon-bigint") (r "^0.2") (k 0)) (d (n "evm") (r "^0.11.0-beta") (k 0)))) (h "01wjpg0bddkkfws0w4ml2bnzp49cznrdxx57pcmjjhxl9xzmzcga") (f (quote (("std" "evm/std") ("rust-secp256k1" "evm/rust-secp256k1") ("rlp" "etcommon-bigint/rlp") ("default" "std" "c-secp256k1") ("c-secp256k1" "evm/c-secp256k1"))))))

(define-public crate-evm-precompiled-bn128-0.11.0-beta.2 (c (n "evm-precompiled-bn128") (v "0.11.0-beta.2") (d (list (d (n "bn-plus") (r "^0.4") (d #t) (k 0)) (d (n "ethereum-bigint") (r "^0.2") (k 0)) (d (n "evm") (r "^0.11.0-beta.2") (k 0)))) (h "13dh6m6qlg8kxfyf6lwh808v9vm1ppmll12mv7pmbfjdr550cqm0") (f (quote (("std" "evm/std") ("rust-secp256k1" "evm/rust-secp256k1") ("rlp" "ethereum-bigint/rlp") ("default" "std" "rust-secp256k1") ("c-secp256k1" "evm/c-secp256k1"))))))

(define-public crate-evm-precompiled-bn128-0.11.0-beta.3 (c (n "evm-precompiled-bn128") (v "0.11.0-beta.3") (d (list (d (n "bn-plus") (r "^0.4") (d #t) (k 0)) (d (n "ethereum-bigint") (r "^0.2") (k 0)) (d (n "evm") (r "^0.11.0-beta.3") (k 0)))) (h "174ghii8w2jhp6rh33r6y4wv4gmcraq0488vqg11h4nqf3bkkg9v") (f (quote (("std" "evm/std") ("rust-secp256k1" "evm/rust-secp256k1") ("rlp" "ethereum-bigint/rlp") ("default" "std" "rust-secp256k1") ("c-secp256k1" "evm/c-secp256k1"))))))

(define-public crate-evm-precompiled-bn128-0.11.0 (c (n "evm-precompiled-bn128") (v "0.11.0") (d (list (d (n "bn-plus") (r "^0.4") (d #t) (k 0)) (d (n "ethereum-bigint") (r "^0.2") (k 0)) (d (n "evm") (r "^0.11") (k 0)))) (h "0mv6fsfrvsv8hmxy5nzdj5p0ax90l437nj0w59756jldlj7zjvyj") (f (quote (("std" "evm/std") ("rust-secp256k1" "evm/rust-secp256k1") ("rlp" "ethereum-bigint/rlp") ("default" "std" "rust-secp256k1") ("c-secp256k1" "evm/c-secp256k1"))))))

