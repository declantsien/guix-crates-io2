(define-module (crates-io ev ma evmasm) #:use-module (crates-io))

(define-public crate-evmasm-0.1.0 (c (n "evmasm") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "04i866f4x3c6w2wshrh7ydmi076182v1y98fnxa9cn0ygbm81rk4") (y #t)))

(define-public crate-evmasm-0.1.1 (c (n "evmasm") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "104kq9p1d7sk65x6ziydfssssifxz8xc4hvlhv3dhghijn25794d")))

(define-public crate-evmasm-0.1.2 (c (n "evmasm") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "0wakjh2ga7y90ichb0bz3wdmri0nxnl8hpczwnk8kvz5kf4gyx21")))

(define-public crate-evmasm-0.1.3 (c (n "evmasm") (v "0.1.3") (d (list (d (n "hex") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "num") (r "^0.1.40") (d #t) (k 0)))) (h "0y30qm8l2ccn6zl6sbgvbfaf78zmpfz2h2hs7w3cja2ql6a87wlw")))

