(define-module (crates-io ev ma evmap-derive) #:use-module (crates-io))

(define-public crate-evmap-derive-0.1.0 (c (n "evmap-derive") (v "0.1.0") (d (list (d (n "evmap") (r "^10") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.24") (d #t) (k 2)))) (h "0x535n3jsa9q6d2ml6a7zjbcbn7zp8hc02vyyrqdd3s1ya08sky4")))

(define-public crate-evmap-derive-0.2.0 (c (n "evmap-derive") (v "0.2.0") (d (list (d (n "evmap") (r "^11.0.0-alpha.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.24") (d #t) (k 2)))) (h "06snhaczi0g05l2jr8qqmp91rx76k9704dv8wzyd4zjvf0vijark")))

