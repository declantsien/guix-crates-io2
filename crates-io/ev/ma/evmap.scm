(define-module (crates-io ev ma evmap) #:use-module (crates-io))

(define-public crate-evmap-0.1.0 (c (n "evmap") (v "0.1.0") (h "05pf7h9ipq6wlr69yv1mgyhlhf4f1w5hz2hhlyq7qfhqi66njydl")))

(define-public crate-evmap-0.1.1 (c (n "evmap") (v "0.1.1") (h "1w2m48zr48mvsvjiigawkb0qld70jml12m5lzv2hsgwgpkmqdyjh")))

(define-public crate-evmap-0.1.2 (c (n "evmap") (v "0.1.2") (h "1fd2gp40hcbpvny2qd9wwr5q64bb3nsvxajkc8bk5hxjsymainax")))

(define-public crate-evmap-0.1.3 (c (n "evmap") (v "0.1.3") (h "0wvq13m09f9cg5l6kk9zqpbf4zwj81q9l14x16yxgm9hwmhalyaa")))

(define-public crate-evmap-0.2.0 (c (n "evmap") (v "0.2.0") (h "0fb7p0l9i2gfnpsw8xc0ncq9ppw4pj2v549fmbpr47w5gksl6kza")))

(define-public crate-evmap-0.3.0 (c (n "evmap") (v "0.3.0") (h "13a0zp24y3hl2pddfa2i7ggprbryr8rly8ic6cs5phsvi7760jmh")))

(define-public crate-evmap-0.3.1 (c (n "evmap") (v "0.3.1") (h "1m91jp1mbkymqd5mksy5y1p9kb34j5xfi6jiv82zz1nxn6fwxnid")))

(define-public crate-evmap-0.4.0 (c (n "evmap") (v "0.4.0") (h "169fhh1h75is3bcan4p9adhkxxg0n2h3dg42mriw1mnp68yyspna")))

(define-public crate-evmap-0.5.0 (c (n "evmap") (v "0.5.0") (h "0i4va2k0rb4cvaqm2f9xjz7g0yn7mdnnzl51q4klksn862wxiiwx")))

(define-public crate-evmap-0.6.0 (c (n "evmap") (v "0.6.0") (h "1rabphcfn7x2h5zgpwnnzzl4czr9g3qldf14c2kznw512s02hzix")))

(define-public crate-evmap-0.6.3 (c (n "evmap") (v "0.6.3") (h "139136nx5n5q7kkcidh50131l2zs3ly1mdga6s9wv1if0a9givf4")))

(define-public crate-evmap-0.6.4 (c (n "evmap") (v "0.6.4") (h "0acq9dd73rhkmf3730d3l511x2rr5hncj5byc456z9ljmbianrsv")))

(define-public crate-evmap-0.6.5 (c (n "evmap") (v "0.6.5") (h "1kqpy6jdhv5afvk5h5s9k6klm16s4k4ls7bmpz256gmg79ilw5vm")))

(define-public crate-evmap-1.0.0 (c (n "evmap") (v "1.0.0") (h "03pmxrvfz11wvj6b7yp0lc3a5l5qs1jpri54xhzpwxvx3fds9bbb")))

(define-public crate-evmap-1.0.1 (c (n "evmap") (v "1.0.1") (h "104cc0140vrgz9b6wx14qj8lxvqf0gd26mg8sgwadkfk27g9j1vp")))

(define-public crate-evmap-1.0.2 (c (n "evmap") (v "1.0.2") (h "0v8mn6s8migxvq26rgw8hyfww5j0zcb85pvsd3xjninqdwzgz3lv")))

(define-public crate-evmap-2.0.0 (c (n "evmap") (v "2.0.0") (h "02zz7k902hv5bzfsfnjq0w8nj8hifgj61fl32v2m99qxx02r2pqy")))

(define-public crate-evmap-2.0.1 (c (n "evmap") (v "2.0.1") (h "1swp2dz4pf8jzisn21al4m5xw38sgrdac6wzrrmcnxn3vpv3c1nw")))

(define-public crate-evmap-2.0.2 (c (n "evmap") (v "2.0.2") (h "1mynz54b0wipf23l9d71icmyk0fmkhsl53pl4a18lg7bqx8dx33s")))

(define-public crate-evmap-3.0.0 (c (n "evmap") (v "3.0.0") (h "12m9wiwfkn561qdisd69jg0klhgba51a63h0kz6nnk8kka2954x5")))

(define-public crate-evmap-3.0.1 (c (n "evmap") (v "3.0.1") (h "1zxxjj1g7j52wckzcpy8zq356pl7fbc5jnii8s1njca2qwcjybah")))

(define-public crate-evmap-4.0.0 (c (n "evmap") (v "4.0.0") (h "15mdi22lasg8daq7s9g65dnnf95ir30cjwqvhyq9ihi0zzrc8xbn")))

(define-public crate-evmap-4.0.1 (c (n "evmap") (v "4.0.1") (h "04sqhy4nxkhr6ihw3h6kfqgf4dssk0ghs472s7giiyfqba4alxws")))

(define-public crate-evmap-4.1.0 (c (n "evmap") (v "4.1.0") (d (list (d (n "hashbrown") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (o #t) (d #t) (k 0)))) (h "14h0ra1jhdbfcv8lvvy3kr0qqyji7h9hqwkkav33fs98pkdiaxjh") (f (quote (("nightly_smallvec" "smallvec/union") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec"))))))

(define-public crate-evmap-4.1.1 (c (n "evmap") (v "4.1.1") (d (list (d (n "hashbrown") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (o #t) (d #t) (k 0)))) (h "0pr9jxq2axrmafpq94gwdad7lqw6yg2j4dl2b4hdndrizvkvbfxf") (f (quote (("nightly_smallvec" "smallvec/union") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec"))))))

(define-public crate-evmap-4.2.0 (c (n "evmap") (v "4.2.0") (d (list (d (n "hashbrown") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (o #t) (d #t) (k 0)))) (h "02k86rxgsd3x912n6j8ay4zsdq6gw2qlhcv313p2qff4sqzylqsy") (f (quote (("nightly_smallvec" "smallvec/union" "smallvec/may_dangle" "smallvec/specialization") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec"))))))

(define-public crate-evmap-4.2.1 (c (n "evmap") (v "4.2.1") (d (list (d (n "hashbrown") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (o #t) (d #t) (k 0)))) (h "0g5c0vi7a7fv1d9cc1z47n1s1g58hwxfp95vil31f54pydjyj8a5") (f (quote (("nightly_smallvec" "smallvec/union" "smallvec/may_dangle" "smallvec/specialization") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec")))) (y #t)))

(define-public crate-evmap-4.2.2 (c (n "evmap") (v "4.2.2") (d (list (d (n "hashbrown") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (o #t) (d #t) (k 0)))) (h "0nfrsq0l33bflr38c5rccvh2bd995291bzryfdrfrks7b7lyg9aj") (f (quote (("nightly_smallvec" "smallvec/union" "smallvec/may_dangle" "smallvec/specialization") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec"))))))

(define-public crate-evmap-4.2.3 (c (n "evmap") (v "4.2.3") (d (list (d (n "hashbrown") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (o #t) (d #t) (k 0)))) (h "1i1wfq13d8bcrrp1270d8l25m3yis1q0xll46sk041s38y7pmh8q") (f (quote (("nightly_smallvec" "smallvec/union" "smallvec/may_dangle" "smallvec/specialization") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec"))))))

(define-public crate-evmap-5.0.0 (c (n "evmap") (v "5.0.0") (d (list (d (n "hashbrown") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (o #t) (d #t) (k 0)))) (h "1kaxsm2wjvmdzny0sw32q07m3v5vb9p8npq8dz8a2s4yrqivvj21") (f (quote (("nightly_smallvec" "smallvec/union" "smallvec/may_dangle" "smallvec/specialization") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec"))))))

(define-public crate-evmap-5.0.1 (c (n "evmap") (v "5.0.1") (d (list (d (n "hashbrown") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (o #t) (d #t) (k 0)))) (h "0cnrpph9l4yw9cn9psk9xzva0sbss466957pbmzp9l5rny50vvk6") (f (quote (("nightly_smallvec" "smallvec/union" "smallvec/may_dangle" "smallvec/specialization") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec"))))))

(define-public crate-evmap-5.0.2 (c (n "evmap") (v "5.0.2") (d (list (d (n "hashbrown") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (o #t) (d #t) (k 0)))) (h "1s81cg5dwwmd7k4mp3whzbagirys7m5rjbdf024qrs602zx1yanz") (f (quote (("nightly_smallvec" "smallvec/union" "smallvec/may_dangle" "smallvec/specialization") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec"))))))

(define-public crate-evmap-5.1.0 (c (n "evmap") (v "5.1.0") (d (list (d (n "bytes") (r "^0.4.12") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (o #t) (d #t) (k 0)))) (h "1jivls4y9wiljcvsd6g6jal1vm3hfnx1dhv91gdbq15qq8rcjwbb") (f (quote (("nightly_smallvec" "smallvec/union" "smallvec/may_dangle" "smallvec/specialization") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec"))))))

(define-public crate-evmap-5.1.1 (c (n "evmap") (v "5.1.1") (d (list (d (n "bytes") (r "^0.4.12") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (o #t) (d #t) (k 0)))) (h "0hicdsikdnn4z120dhngafwnb921yq8zrqwkmw2ncihc36plm032") (f (quote (("nightly_smallvec" "smallvec/union" "smallvec/may_dangle" "smallvec/specialization") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec"))))))

(define-public crate-evmap-6.0.0 (c (n "evmap") (v "6.0.0") (d (list (d (n "bytes") (r "^0.4.12") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (o #t) (d #t) (k 0)))) (h "1rlwvjka82jqkgc83imp8fvh49z1xf2xwn33j87jr6jqy1051s7a") (f (quote (("nightly_smallvec" "smallvec/union" "smallvec/may_dangle" "smallvec/specialization") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec"))))))

(define-public crate-evmap-6.0.1 (c (n "evmap") (v "6.0.1") (d (list (d (n "bytes") (r "^0.4.12") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (o #t) (d #t) (k 0)))) (h "03rqfb1rp824dgq3pxz7v9lbc8jxp18m7yh2hwgwk0lv9h3n1nvg") (f (quote (("nightly_smallvec" "smallvec/union" "smallvec/may_dangle" "smallvec/specialization") ("nightly_hashbrown" "hashbrown/nightly") ("default" "hashbrown" "smallvec"))))))

(define-public crate-evmap-7.0.0 (c (n "evmap") (v "7.0.0") (d (list (d (n "bytes") (r "^0.4.12") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (o #t) (d #t) (k 0)))) (h "0jzp19l5b5zh6nrdl6r3prmlz434wxzs8jkvwk1i5rlc4kn7c5p7") (f (quote (("default" "smallvec"))))))

(define-public crate-evmap-7.1.0 (c (n "evmap") (v "7.1.0") (d (list (d (n "bytes") (r "^0.4.12") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (o #t) (d #t) (k 0)))) (h "1cvshrcc2xsx0f3yp5k7ycv6wfl249w71d9wj9pnhhcjgbhsq23y") (f (quote (("indexed" "indexmap") ("default" "smallvec"))))))

(define-public crate-evmap-7.1.1 (c (n "evmap") (v "7.1.1") (d (list (d (n "bytes") (r "^0.4.12") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^0.6.7") (o #t) (d #t) (k 0)))) (h "14r7ws3jil8b0rr8m34cf18krg6hli5nvc5l0ky1w6kdrlgwhrjg") (f (quote (("indexed" "indexmap") ("default" "smallvec"))))))

(define-public crate-evmap-7.1.2 (c (n "evmap") (v "7.1.2") (d (list (d (n "bytes") (r "^0.4.12") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "09f2apllb5893w2cdrd9rbp1j7m52hml6jc5xalaz7dv049y65a1") (f (quote (("indexed" "indexmap") ("default" "smallvec"))))))

(define-public crate-evmap-7.1.3 (c (n "evmap") (v "7.1.3") (d (list (d (n "bytes") (r "^0.4.12") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "15azxbz0phrd2ww6vba1yx76ngc00mkdxxpkq9f4didzdc4wx2am") (f (quote (("indexed" "indexmap") ("default" "smallvec"))))))

(define-public crate-evmap-8.0.0 (c (n "evmap") (v "8.0.0") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1037689c7ihxwld23vynzrd25770wmgw7b6h26k5cqndjf9cn4hq") (f (quote (("indexed" "indexmap") ("default" "smallvec"))))))

(define-public crate-evmap-8.0.1 (c (n "evmap") (v "8.0.1") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0992mdb4wn9smrlkc92kj73imin2ycxl84n3xzypld37ks9zxp8m") (f (quote (("indexed" "indexmap") ("default" "smallvec"))))))

(define-public crate-evmap-9.0.0-beta.1 (c (n "evmap") (v "9.0.0-beta.1") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "hashbag") (r "^0.1.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "1lmh7s3jg9qym0337iw063qv73yx2hqyh1sgs7abnacplmib8m5v") (f (quote (("indexed" "indexmap") ("default"))))))

(define-public crate-evmap-9.0.0-beta.2 (c (n "evmap") (v "9.0.0-beta.2") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "hashbag") (r "^0.1.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "16wjph2n8ckvqg1i0pxhd70dplxafvxlf5h1xrflf236482k61g1") (f (quote (("indexed" "indexmap") ("default"))))))

(define-public crate-evmap-9.0.0 (c (n "evmap") (v "9.0.0") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "hashbag") (r "^0.1.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "1f8nfal8mzpkrk90m83lnqijngvmhq909nbvqa25xx7pr0yzqy97") (f (quote (("indexed" "indexmap") ("default"))))))

(define-public crate-evmap-9.0.1 (c (n "evmap") (v "9.0.1") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "hashbag") (r "^0.1.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "09n7whfl0s29n4jcx3mav2h33yy9lvam36l65bcnywij3fki7bhw") (f (quote (("indexed" "indexmap") ("default"))))))

(define-public crate-evmap-10.0.0 (c (n "evmap") (v "10.0.0") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "hashbag") (r "^0.1.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "19nr17zyfrrjiza34c6qwbdrs4wnj7n7c8gb432v58qb8ka4yr6f") (f (quote (("indexed" "indexmap") ("default"))))))

(define-public crate-evmap-10.0.1 (c (n "evmap") (v "10.0.1") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "hashbag") (r "^0.1.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "1i8j7qrfm1ya3q559wjjga2f75pg4f30vxdi4f9v2cpvxj73lw40") (f (quote (("indexed" "indexmap") ("default"))))))

(define-public crate-evmap-10.0.2 (c (n "evmap") (v "10.0.2") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "hashbag") (r "^0.1.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "13amvib52g8fhxf8lmd52w3qyhcvf9x1xr86xg13szgrhdma0gkf") (f (quote (("indexed" "indexmap") ("default"))))))

(define-public crate-evmap-11.0.0-alpha.1 (c (n "evmap") (v "11.0.0-alpha.1") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "hashbag") (r "^0.1.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "1l1vg6cljzisy9si7cw3j4j8wc7p369ixhhh9ybh4jgzb8hm9lar") (f (quote (("indexed" "indexmap") ("eviction" "indexed" "rand") ("default"))))))

(define-public crate-evmap-11.0.0-alpha.2 (c (n "evmap") (v "11.0.0-alpha.2") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "hashbag") (r "^0.1.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "left-right") (r "^0.9.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "1qkv4cv2lhvq9jqlc5zyakizr04cpz4lf6mkrsbxhwn0gvp34i68") (f (quote (("indexed" "indexmap") ("eviction" "indexed" "rand") ("default"))))))

(define-public crate-evmap-11.0.0-alpha.3 (c (n "evmap") (v "11.0.0-alpha.3") (d (list (d (n "hashbag") (r "^0.1.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "left-right") (r "^0.9.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "0gjlvjx1dc0l82m0vn2y3zxvs4zpmdf68y2wrcxyknib0cckqb5y") (f (quote (("indexed" "indexmap") ("eviction" "indexed" "rand") ("default"))))))

(define-public crate-evmap-11.0.0-alpha.4 (c (n "evmap") (v "11.0.0-alpha.4") (d (list (d (n "hashbag") (r "^0.1.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "left-right") (r "^0.10.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "0z6f6agnw36l7cq456j9wgkg8ad987c11x44hg3cmg2sal5s9ylc") (f (quote (("indexed" "indexmap") ("eviction" "indexed" "rand") ("default"))))))

(define-public crate-evmap-11.0.0-alpha.5 (c (n "evmap") (v "11.0.0-alpha.5") (d (list (d (n "hashbag") (r "^0.1.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "left-right") (r "^0.11.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "1glsjpd52vr97xmncvya4dmnbyvq12046121c1bg1cggbn16wbxm") (f (quote (("indexed" "indexmap") ("eviction" "indexed" "rand") ("default"))))))

(define-public crate-evmap-11.0.0-alpha.6 (c (n "evmap") (v "11.0.0-alpha.6") (d (list (d (n "hashbag") (r "^0.1.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (o #t) (d #t) (k 0)) (d (n "indexmap-amortized") (r "^1.6.1") (o #t) (d #t) (k 0)) (d (n "left-right") (r "^0.11.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "0hj5631ygdz2kv7dc7x1xa2jhdppc0rhgxfhrmb8d9fajzhk31d3") (f (quote (("indexed" "indexmap") ("eviction" "indexed" "rand") ("default") ("amortize" "indexmap-amortized" "hashbag/amortize"))))))

(define-public crate-evmap-11.0.0-alpha.7 (c (n "evmap") (v "11.0.0-alpha.7") (d (list (d (n "hashbag") (r "^0.1.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.1") (o #t) (d #t) (k 0)) (d (n "indexmap-amortized") (r "^1.6.1") (o #t) (d #t) (k 0)) (d (n "left-right") (r "^0.11.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "rand") (r "^0.7") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "0djhn60ca0xhayrfnknyz1az09jkzl4iis7mxq9nfbjgg3ijhym4") (f (quote (("indexed" "indexmap") ("eviction" "indexed" "rand") ("default") ("amortize" "indexmap-amortized" "hashbag/amortize"))))))

