(define-module (crates-io ev ms evmscan) #:use-module (crates-io))

(define-public crate-evmscan-0.6.0 (c (n "evmscan") (v "0.6.0") (d (list (d (n "isahc") (r "^1.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "primitive-types") (r "^0.11.1") (f (quote ("impl-serde" "fp-conversion"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "07yc5kz6s8pna6c3czya6g9nby3zncafi0dmillxg6x0jkh0ydq7")))

