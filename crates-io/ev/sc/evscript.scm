(define-module (crates-io ev sc evscript) #:use-module (crates-io))

(define-public crate-evscript-0.1.0 (c (n "evscript") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "lalrpop") (r "^0.20.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.0") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1i1lg3nahlp6yalzy1l3wflv82f61q49v5gx0jqxrjli5ymb4c93")))

