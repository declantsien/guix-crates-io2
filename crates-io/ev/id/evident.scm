(define-module (crates-io ev id evident) #:use-module (crates-io))

(define-public crate-evident-0.1.0 (c (n "evident") (v "0.1.0") (h "0qwprf5dfz12sf6frk9ais0fppjsfmc7c8vq5zf5lxj7jjadjd6z")))

(define-public crate-evident-0.2.0 (c (n "evident") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 2)) (d (n "uuid") (r "^1.3") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1n9hjxjgmbafz831813205rmkwwx0yxn296bmp91x60lvqib7p9b")))

(define-public crate-evident-0.3.0 (c (n "evident") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 2)) (d (n "uuid") (r "^1.3") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1i7797zw3jqcs1myp4j57hljgnj6avv2j0jc3m870vg14vmb5zcj")))

(define-public crate-evident-0.4.0 (c (n "evident") (v "0.4.0") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 2)) (d (n "uuid") (r "^1.3") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "02ls7shaixkrmfqg3bgjvrl7r9is25g12frmvpv2q0xy7vdz1z4d")))

(define-public crate-evident-0.5.0 (c (n "evident") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 2)) (d (n "uuid") (r "^1.3") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0ynxyak25k7ycqihrgqmxf3mw0l7ajnx4y9x1jcb7gvrhdpg09b5")))

(define-public crate-evident-0.6.0 (c (n "evident") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 2)) (d (n "uuid") (r "^1.3") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0avxhiswnmbi1lnry09xzl1bpj8nwy49cwgbrgc229bqk76f51f8")))

(define-public crate-evident-0.9.0 (c (n "evident") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 2)) (d (n "uuid") (r "^1.3") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "07qvl9290bl872mz3jzbmk0l42p00776qd0brq5m1qwasz913bwh")))

(define-public crate-evident-0.10.0 (c (n "evident") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 2)) (d (n "uuid") (r "^1.3") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "1p90p1z2ghl6nl6mzn73056423kw4nm6wwg052xf2wkl5x965knm")))

(define-public crate-evident-0.11.0 (c (n "evident") (v "0.11.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 2)) (d (n "uuid") (r "^1.3") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0mgvg9dlsiiadlk2kaxp8iiyfjav9mh4qn9j4m7dnizxxgwi18nd")))

(define-public crate-evident-0.12.0 (c (n "evident") (v "0.12.0") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 2)) (d (n "uuid") (r "^1.3") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "06sky9nw53yiwd5055wmmhl50gxj1i23p7f9mkx84aqcai352i5m")))

(define-public crate-evident-0.12.2 (c (n "evident") (v "0.12.2") (d (list (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 2)) (d (n "uuid") (r "^1.3") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "0k21yix9p3sf0s0gr64jqz42b6fn4bv2z2y72x07v1lpny5qdm7k")))

