(define-module (crates-io ev ic evic) #:use-module (crates-io))

(define-public crate-evic-0.1.0 (c (n "evic") (v "0.1.0") (d (list (d (n "clippy") (r "*") (o #t) (d #t) (k 0)) (d (n "getopts") (r "^0.2.14") (d #t) (k 0)))) (h "173z1gnakkys57frchz08389sh02339wp76iwll4bjk5v7rs6s5c")))

(define-public crate-evic-0.1.1 (c (n "evic") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "getopts") (r "^0.2.14") (d #t) (k 0)))) (h "1hs31z0xc719vygr92d60v71raz408bsj1pbc18w7ssvjydip141")))

