(define-module (crates-io ev ic evicting_cache_map) #:use-module (crates-io))

(define-public crate-evicting_cache_map-0.3.1 (c (n "evicting_cache_map") (v "0.3.1") (d (list (d (n "ordered_hash_map") (r "^0.3.1") (d #t) (k 0)))) (h "1q3w2kx0r3fzg0bcl3cqrpgr2k56wy564dhk2g9glvpb4h99v59g") (r "1.65.0")))

(define-public crate-evicting_cache_map-0.4.0 (c (n "evicting_cache_map") (v "0.4.0") (d (list (d (n "ordered_hash_map") (r "^0.4.0") (d #t) (k 0)))) (h "0af73zghhi9mpfdv58d3a05k69i5z473fl4qxw0zf8ssvjjnzaq4") (r "1.65.0")))

