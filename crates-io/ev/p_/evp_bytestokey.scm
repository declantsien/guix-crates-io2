(define-module (crates-io ev p_ evp_bytestokey) #:use-module (crates-io))

(define-public crate-evp_bytestokey-0.1.0 (c (n "evp_bytestokey") (v "0.1.0") (d (list (d (n "md-5") (r "^0.10.5") (d #t) (k 0)))) (h "0saz5m0xf1sly80p4wksfrkm0p4bp04jlag2zs5yqvq4783hwvxw")))

(define-public crate-evp_bytestokey-0.2.0 (c (n "evp_bytestokey") (v "0.2.0") (d (list (d (n "md-5") (r "^0.10.5") (d #t) (k 0)))) (h "160rh4qhpqk1i1q5nbd7grs05sd67sv56yz7p9f51vrd26ias4lc")))

