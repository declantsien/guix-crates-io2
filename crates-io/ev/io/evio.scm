(define-module (crates-io ev io evio) #:use-module (crates-io))

(define-public crate-evio-0.1.0 (c (n "evio") (v "0.1.0") (h "021jaq3aqqhsnq4kn1svp415zllcx4x5p4l2vxcrbg7rglxw2ays")))

(define-public crate-evio-0.2.0 (c (n "evio") (v "0.2.0") (h "02qdyfpxkdidl5y2gqglxkg1c9rz2p6l0377clafva88yc0jlp36")))

