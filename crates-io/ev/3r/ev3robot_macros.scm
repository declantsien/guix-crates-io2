(define-module (crates-io ev #{3r}# ev3robot_macros) #:use-module (crates-io))

(define-public crate-ev3robot_macros-0.1.0 (c (n "ev3robot_macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("default" "full"))) (d #t) (k 0)) (d (n "tap") (r "^1") (d #t) (k 0)))) (h "1h3gb821jbkrb9iik110c2h1c6jzvcl3ql2s0z5skqw8gz3dvn85")))

(define-public crate-ev3robot_macros-0.2.0 (c (n "ev3robot_macros") (v "0.2.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("default" "full"))) (d #t) (k 0)) (d (n "tap") (r "^1") (d #t) (k 0)))) (h "0rzxcywqflvbm9b2m8khac8qrvxb5k9slnw54r6z00234ri36vph")))

(define-public crate-ev3robot_macros-0.2.1 (c (n "ev3robot_macros") (v "0.2.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("default" "full"))) (d #t) (k 0)) (d (n "tap") (r "^1") (d #t) (k 0)))) (h "0r7w47nsz9wlgra6dljr3wgjnrpcdnxp72bn7fz13lnanf7a041y")))

(define-public crate-ev3robot_macros-0.3.0 (c (n "ev3robot_macros") (v "0.3.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("default" "full"))) (d #t) (k 0)) (d (n "tap") (r "^1") (d #t) (k 0)))) (h "1qh65v1m2r2kwcrzwmbgfd0magh9aw66j2zi7dlk754jkw6d85c5")))

(define-public crate-ev3robot_macros-0.3.1 (c (n "ev3robot_macros") (v "0.3.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("default" "full"))) (d #t) (k 0)) (d (n "tap") (r "^1") (d #t) (k 0)))) (h "16mdprwdpkbma012r3rmh03xdfv5pb1ygzxmlz8q5cfaba2jzid8")))

