(define-module (crates-io ev #{3r}# ev3robot) #:use-module (crates-io))

(define-public crate-ev3robot-0.1.0 (c (n "ev3robot") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "ev3robot_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "tap") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1m4zzcn11q0m9ay5amh9i1ihfkdfg2bqcvxj4s9174vknxd99bva")))

(define-public crate-ev3robot-0.2.0 (c (n "ev3robot") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "ev3robot_macros") (r "^0.2.0") (d #t) (k 0)) (d (n "tap") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0zhavsxs01zf9qwdkp5s0khdhfzmnqjlyfsg6wbfsixcy86r31hp")))

(define-public crate-ev3robot-0.2.1 (c (n "ev3robot") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "ev3robot_macros") (r "^0.2.1") (d #t) (k 0)) (d (n "tap") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1r4g0k9pj1sdgcab89q4iz7aji45zdr3cwjwirglh7i8jxwillw4")))

(define-public crate-ev3robot-0.3.0 (c (n "ev3robot") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "ev3robot_macros") (r "^0.3.0") (d #t) (k 0)) (d (n "tap") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0avbasfjyvzvgj2ha2vrg5hy10qldbhs9g2nbp59fyand43xrjzc")))

(define-public crate-ev3robot-0.3.1 (c (n "ev3robot") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "ev3robot_macros") (r "^0.3.0") (d #t) (k 0)) (d (n "tap") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "11cgs41wbwy480s95i3r4dhrawj40z2bp4gr12pmiiw00rb13d8g")))

