(define-module (crates-io ev #{3r}# ev3rt) #:use-module (crates-io))

(define-public crate-ev3rt-0.1.0 (c (n "ev3rt") (v "0.1.0") (h "1zgag38bx27jvfp05dric25ym2n413x0l96lzsv8pqf6ds8qnpwg")))

(define-public crate-ev3rt-0.2.0 (c (n "ev3rt") (v "0.2.0") (h "1gvsk3ayx0pjvglkiv4pnp0d0q38d5r3k6l11jj9ndf7d2x4aq32")))

(define-public crate-ev3rt-0.3.0 (c (n "ev3rt") (v "0.3.0") (h "0862gwhh3ajrr6mvbxcrv47qrg31f25sxaj0ncv50184wlqn6vp7")))

(define-public crate-ev3rt-0.4.0 (c (n "ev3rt") (v "0.4.0") (h "0bn1j5x3qn2qhjs1a83l53snf214jz7z28w5h6g89n3zi0sw1480")))

(define-public crate-ev3rt-0.5.0 (c (n "ev3rt") (v "0.5.0") (h "03ivhpcl0xlvkbszj0373dx8j9s2m9hmdf2rqfr67901vilkjsyd")))

