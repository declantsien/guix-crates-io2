(define-module (crates-io ev co evco) #:use-module (crates-io))

(define-public crate-evco-0.2.0 (c (n "evco") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0.114") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "05zv1097gvrkhzybxhbcym09dv878qq3rxskwyqpyqrjq0c73hrp") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-evco-0.2.1 (c (n "evco") (v "0.2.1") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0rsxyyjx8s9f10dq4z8axk7hckjv21cy7flb46257952mjwcpxgc") (f (quote (("dev" "clippy") ("default"))))))

