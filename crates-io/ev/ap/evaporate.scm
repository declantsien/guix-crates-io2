(define-module (crates-io ev ap evaporate) #:use-module (crates-io))

(define-public crate-evaporate-0.2.1 (c (n "evaporate") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rusqlite") (r "^0.27") (d #t) (k 0)))) (h "0dsaa2f6z7kq5v2llazys4sybwmr5njqrdx122acsix0jzaz4r81")))

