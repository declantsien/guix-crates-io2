(define-module (crates-io ev -a ev-apple) #:use-module (crates-io))

(define-public crate-ev-apple-0.1.0 (c (n "ev-apple") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "state") (r "^0.4.1") (f (quote ("tls"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("full"))) (d #t) (k 0)))) (h "17928lxijn8l999rgyqg6kk60bbmzpa1wbri5zs1a5kdcmil3w0k")))

(define-public crate-ev-apple-0.1.1 (c (n "ev-apple") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "state") (r "^0.4.1") (f (quote ("tls"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("full"))) (d #t) (k 0)))) (h "0k3akrq83rw4lgn5gqvfchq0wf0fc0q05alk74hdww0jrwgqp0i3")))

