(define-module (crates-io ev _s ev_slotmap) #:use-module (crates-io))

(define-public crate-ev_slotmap-0.1.0 (c (n "ev_slotmap") (v "0.1.0") (d (list (d (n "evmap") (r "^10.0.2") (d #t) (k 0)) (d (n "one_way_slot_map") (r "^0.2.0") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 2)))) (h "1q23m35qv5zrg8va42wnnrcj8yv307r5fjam2x4ijc00nnb3gr2y")))

(define-public crate-ev_slotmap-0.1.1 (c (n "ev_slotmap") (v "0.1.1") (d (list (d (n "evmap") (r "^10.0.2") (d #t) (k 0)) (d (n "one_way_slot_map") (r "^0.2.0") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 2)))) (h "12x4wckfh8sg3pslihyjs2fymds7vs919fig7mgx8icpj2iz7r7k")))

(define-public crate-ev_slotmap-0.1.2 (c (n "ev_slotmap") (v "0.1.2") (d (list (d (n "evmap") (r "^10.0.2") (d #t) (k 0)) (d (n "one_way_slot_map") (r "^0.2.0") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 2)))) (h "15mw7cavwan0ssynv28f6cid92n4gabbqpff5vghxh40yn5pd8xc")))

(define-public crate-ev_slotmap-0.1.3 (c (n "ev_slotmap") (v "0.1.3") (d (list (d (n "evmap") (r "^10.0.2") (d #t) (k 0)) (d (n "one_way_slot_map") (r "^0.2.1") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 2)))) (h "1xhjfhwxlyjfbas4hmgl5042m4m35zz3s92h5ka91bi4jy8vcj9w")))

(define-public crate-ev_slotmap-0.2.0 (c (n "ev_slotmap") (v "0.2.0") (d (list (d (n "evmap") (r "^10.0.2") (d #t) (k 0)) (d (n "one_way_slot_map") (r "^0.3.1") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 2)))) (h "0vybrnlajbwcfvg104xi75wr0afyah4q6m5mbh2ghwv3rdfqlrx1")))

(define-public crate-ev_slotmap-0.2.1 (c (n "ev_slotmap") (v "0.2.1") (d (list (d (n "evmap") (r "^10.0.2") (d #t) (k 0)) (d (n "one_way_slot_map") (r "^0.3.1") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 2)))) (h "1szkmjinpvmggdy2dfi7f764h7r5mnngjgwb8an4zazd9pi98hys")))

