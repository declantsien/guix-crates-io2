(define-module (crates-io ev e_ eve_esi) #:use-module (crates-io))

(define-public crate-eve_esi-0.1.0 (c (n "eve_esi") (v "0.1.0") (d (list (d (n "axum") (r "^0.7.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4.37") (f (quote ("serde"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "1mw9z82wc9a1jwjfx4jp1sk4wm4l9f5ad83wd7b2plbg5az7rrpp")))

