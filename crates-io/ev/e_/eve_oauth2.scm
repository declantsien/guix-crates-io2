(define-module (crates-io ev e_ eve_oauth2) #:use-module (crates-io))

(define-public crate-eve_oauth2-0.1.0 (c (n "eve_oauth2") (v "0.1.0") (d (list (d (n "jsonwebtoken") (r "^8.3.0") (d #t) (k 0)) (d (n "oauth2") (r "^4.4.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vwa8102bkkyx9mxhs66v90plbgwwkskwnm11k6ng3xsm5p6yvmb") (y #t)))

(define-public crate-eve_oauth2-0.2.0 (c (n "eve_oauth2") (v "0.2.0") (d (list (d (n "axum") (r "^0.7.5") (d #t) (k 2)) (d (n "cached") (r "^0.49.2") (f (quote ("async"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "jsonwebtoken") (r "^9.2.0") (d #t) (k 0)) (d (n "oauth2") (r "^4.4.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 2)) (d (n "time") (r "^0.3.34") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (d #t) (k 2)) (d (n "tower-sessions") (r "^0.12.0") (d #t) (k 2)))) (h "0h1c0v35vmsj1as88cmpvirxfkz79h3jnpk4jqidv8362j8qhrqh")))

