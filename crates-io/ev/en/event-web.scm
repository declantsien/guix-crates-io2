(define-module (crates-io ev en event-web) #:use-module (crates-io))

(define-public crate-event-web-0.1.0 (c (n "event-web") (v "0.1.0") (d (list (d (n "actix") (r "^0.5") (d #t) (k 0)) (d (n "actix-web") (r "^0.6") (d #t) (k 0)) (d (n "bcrypt") (r "^0.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "maud") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1pdns215z0wfwwh2z3i1082wi4favl0wk2xnbvwdf89anmfinv85")))

