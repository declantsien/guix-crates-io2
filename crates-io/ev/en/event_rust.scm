(define-module (crates-io ev en event_rust) #:use-module (crates-io))

(define-public crate-event_rust-0.1.0 (c (n "event_rust") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "time") (r "^0.1.31") (d #t) (k 0)))) (h "0avdy3x0rwdw5q2s4rczxqdkn5jv59xlny0rspx7i72w9nwfrw2x")))

(define-public crate-event_rust-0.1.1 (c (n "event_rust") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "time") (r "^0.1.31") (d #t) (k 0)))) (h "0kqxd3ra073jadsr0rrk0xr0da78bb8ak0yibjgmhkprgmvmjnzs")))

