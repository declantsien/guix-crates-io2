(define-module (crates-io ev en event) #:use-module (crates-io))

(define-public crate-event-0.0.1 (c (n "event") (v "0.0.1") (d (list (d (n "typemap") (r ">= 0.0.0") (d #t) (k 0)) (d (n "unsafe-any") (r ">= 0.0.0") (d #t) (k 0)))) (h "1sv6v3c2l2d59nccan6jwh2cv9gv32in32i9q2fnmr6qs3r9d0ha")))

(define-public crate-event-0.1.1 (c (n "event") (v "0.1.1") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "mio") (r "*") (d #t) (k 0)))) (h "00cayf6zar8sh8h43sr8qb42xnzvzdxvpa0aaqcs1hcfg99ly6fd")))

(define-public crate-event-0.2.0 (c (n "event") (v "0.2.0") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "mio") (r "*") (d #t) (k 0)))) (h "0isghrbbzwlys36qw5fnqjc0gz9pdv9yafcsdrr2xgiq4bydyw3k")))

(define-public crate-event-0.2.1 (c (n "event") (v "0.2.1") (d (list (d (n "log") (r "*") (d #t) (k 0)) (d (n "mio") (r "*") (d #t) (k 0)))) (h "0al4lgm11bdk85c3b893nvn31yr6nybjp15a3z7ky8yk2f16dasd")))

