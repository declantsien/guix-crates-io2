(define-module (crates-io ev en eventum) #:use-module (crates-io))

(define-public crate-eventum-0.1.0 (c (n "eventum") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio") (r "^0.7.0") (f (quote ("os-poll" "os-util" "uds"))) (d #t) (k 0)) (d (n "quick-error") (r "^1.2.3") (d #t) (k 0)) (d (n "simplelog") (r "^0.7.4") (d #t) (k 0)))) (h "1bdnjd2yb1iv3hmv7bch3xhbjkcg7b6vgn5srdxk4jhgm4jm3x3f")))

(define-public crate-eventum-0.1.1 (c (n "eventum") (v "0.1.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mio") (r "^0.7.0") (f (quote ("os-poll" "os-util" "uds"))) (d #t) (k 0)) (d (n "quick-error") (r "^1.2.3") (d #t) (k 0)) (d (n "simplelog") (r "^0.7.4") (d #t) (k 0)))) (h "0bxh8bwfrjg3k9p9mjgaldrbf5nsidwphfl6vv8fxgbyy8nsldw0")))

