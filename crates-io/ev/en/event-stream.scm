(define-module (crates-io ev en event-stream) #:use-module (crates-io))

(define-public crate-event-stream-0.1.0 (c (n "event-stream") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)))) (h "00sip3nklgyswqq09saxng6f7zgrvxkhh4f7p10ap12nhj5g446w")))

(define-public crate-event-stream-0.1.1 (c (n "event-stream") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)))) (h "0qycpsxn7qizycyydpayzscvf0yc1697m02496mg3ypzd180ch5c")))

