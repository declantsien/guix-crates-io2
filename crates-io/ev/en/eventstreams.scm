(define-module (crates-io ev en eventstreams) #:use-module (crates-io))

(define-public crate-eventstreams-0.1.0 (c (n "eventstreams") (v "0.1.0") (d (list (d (n "mediawiki") (r "^0.1.28") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sse-client") (r "^1.1.0") (f (quote ("native-tls"))) (d #t) (k 0)))) (h "1nnzg8i6b776nc84df0hsp6sk96nkksp3mv6l7viwcsrrjjy0b8y") (f (quote (("mediawiki-api" "mediawiki") ("default"))))))

(define-public crate-eventstreams-0.2.0 (c (n "eventstreams") (v "0.2.0") (d (list (d (n "mediawiki") (r "^0.1.28") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sse-client") (r "^1.1.0") (f (quote ("native-tls"))) (d #t) (k 0)))) (h "0pqy1rff08809ql6njs6mailvl4m0bxhpy2pl9v7igby2a6ixpnp") (f (quote (("mediawiki-api" "mediawiki") ("default"))))))

(define-public crate-eventstreams-0.3.0 (c (n "eventstreams") (v "0.3.0") (d (list (d (n "async-stream") (r "^0.3.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "surf-sse") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "050nzx7ppdn20zxs0g6sqx0fd3j609va2sshx9hnh81qqi6p8sza")))

(define-public crate-eventstreams-0.3.1 (c (n "eventstreams") (v "0.3.1") (d (list (d (n "async-stream") (r "^0.3.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "surf-sse") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0zn58fahpb6irl3iw18q3slpf160c4a83k0idizxah86g5qggnrp")))

