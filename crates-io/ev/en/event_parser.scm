(define-module (crates-io ev en event_parser) #:use-module (crates-io))

(define-public crate-event_parser-0.1.0 (c (n "event_parser") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "date_time_parser") (r "^0.1.0") (d #t) (k 0)) (d (n "icalendar") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1cwdc6q87cmk5cy45zfsrzlh7sy5qbxq7y5vxaid96xq590wqibd")))

(define-public crate-event_parser-0.1.1 (c (n "event_parser") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "date_time_parser") (r "^0.1.1") (d #t) (k 0)) (d (n "icalendar") (r "^0.10.0") (d #t) (k 0)) (d (n "iso8601") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.3") (d #t) (k 0)))) (h "0qcscmwp93c8s2v6xbp7rvcrbyh896k2mqzf7k0ndixv7w41z12f")))

