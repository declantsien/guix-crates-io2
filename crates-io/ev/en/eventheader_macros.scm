(define-module (crates-io ev en eventheader_macros) #:use-module (crates-io))

(define-public crate-eventheader_macros-0.2.0 (c (n "eventheader_macros") (v "0.2.0") (h "0i6dydm6hp949n88q6g1p3m175q97l0gz96m0n4y44z2hq0q5iqi") (r "1.63")))

(define-public crate-eventheader_macros-0.3.0 (c (n "eventheader_macros") (v "0.3.0") (h "0zn3y5xhrdyqi22kyf4z8m6a37fh882gw6l8f89gjbdgblny0vnk") (r "1.63")))

