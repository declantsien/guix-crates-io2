(define-module (crates-io ev en eventsource-threaded) #:use-module (crates-io))

(define-public crate-eventsource-threaded-0.1.0 (c (n "eventsource-threaded") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.9") (f (quote ("blocking"))) (o #t) (d #t) (k 0)))) (h "0x0bgd4lp1wrhdpkd6ijjhbs2kfxpjy694hz794jv1gr4qqlancx") (f (quote (("with-reqwest" "reqwest") ("default" "with-reqwest"))))))

