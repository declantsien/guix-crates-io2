(define-module (crates-io ev en eventbus) #:use-module (crates-io))

(define-public crate-eventbus-0.1.0 (c (n "eventbus") (v "0.1.0") (d (list (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "12j0x36igxa200h11spjkiqp3k13zm3k8sarfxlh0yawdx0rvl75")))

(define-public crate-eventbus-0.1.1 (c (n "eventbus") (v "0.1.1") (d (list (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1x52r85xdcxvd1db1gkfk0ci7jv5prhx5b28ws4vhwzacphdc05q")))

(define-public crate-eventbus-0.1.2 (c (n "eventbus") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.2.0") (d #t) (k 2)) (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1x1gais587gwddaph8a8s7lg5l7nh64dm463wmmjm8i8mmiy2j9c")))

(define-public crate-eventbus-0.1.3 (c (n "eventbus") (v "0.1.3") (d (list (d (n "lazy_static") (r "^0.2.0") (d #t) (k 2)) (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1rqffafxkb5acp48nys2qsdmw7f93v797p9s9pyc7f9jqq1gldh1")))

(define-public crate-eventbus-0.1.4 (c (n "eventbus") (v "0.1.4") (d (list (d (n "lazy_static") (r "^0.2.0") (d #t) (k 2)) (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ihp26x4b2y9jamc8gp54nyczkvd0567bs156wpbybwxgzl2rfmj")))

(define-public crate-eventbus-0.1.5 (c (n "eventbus") (v "0.1.5") (d (list (d (n "lazy_static") (r "^0.2.0") (d #t) (k 2)) (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1bm2x76c31vgl34fsxcg9543vjlxl09mg85a1hrsyy2hpjrbx7ly")))

(define-public crate-eventbus-0.2.0 (c (n "eventbus") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)))) (h "1flixflf8h49r2izvxk9vb612rj7ykjv8zp8xjrijxqydyja816b")))

(define-public crate-eventbus-0.3.0 (c (n "eventbus") (v "0.3.0") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "0k8icvpqfzx20xpd8jc76vsacpijznxk9bgy8j3aya8mcifpijyh")))

(define-public crate-eventbus-0.3.1 (c (n "eventbus") (v "0.3.1") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "1mgglrzbg650vphzm4bmx66bgv90r2nxs4g328w18ank0v9kk4rs")))

(define-public crate-eventbus-0.3.2 (c (n "eventbus") (v "0.3.2") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "10szaz99jp7g7vw08qnhbfr2p0w9mycxji99y0qhy4s3pz1aplgw")))

(define-public crate-eventbus-0.4.0 (c (n "eventbus") (v "0.4.0") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "1n5spqpi8xldhqbx9xvlvik9i1y9grw4az7vaaqbm2vc5g396kli")))

(define-public crate-eventbus-0.5.0 (c (n "eventbus") (v "0.5.0") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "itertools") (r "^0.7.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "1wcy51fnvfvlwyrnfyk2zmk0pswjqgjvnm8pifrwsk831van8y6f")))

(define-public crate-eventbus-0.5.1 (c (n "eventbus") (v "0.5.1") (d (list (d (n "anymap") (r "^0.12.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "itertools") (r "^0.7.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "0l0dymvhm32mxs8b6smjbmcrqjw8rxgiq9nfk5hfc46nckvngymk")))

