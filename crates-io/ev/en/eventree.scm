(define-module (crates-io ev en eventree) #:use-module (crates-io))

(define-public crate-eventree-0.1.0 (c (n "eventree") (v "0.1.0") (d (list (d (n "expect-test") (r "^1.1") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "text-size") (r "^1.1") (d #t) (k 0)))) (h "0nj0f4liih90wxln7nip5ya00n3g77zxg1idq159szn4s3k3f0h9") (y #t)))

(define-public crate-eventree-0.2.0 (c (n "eventree") (v "0.2.0") (d (list (d (n "expect-test") (r "^1.1") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "text-size") (r "^1.1") (d #t) (k 0)))) (h "0a545n7b42i5w7kmfhasgpngv9spv37w8m3a4phfjjycd4sypdk6") (y #t)))

(define-public crate-eventree-0.3.0 (c (n "eventree") (v "0.3.0") (d (list (d (n "expect-test") (r "^1.1") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "text-size") (r "^1.1") (d #t) (k 0)))) (h "1y8r6d0k5kq30wvypjv8nh44mfkvwxxcyw25fpwxdjn5z5rqnh0f") (y #t)))

(define-public crate-eventree-0.4.0 (c (n "eventree") (v "0.4.0") (d (list (d (n "expect-test") (r "^1.1") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "text-size") (r "^1.1") (d #t) (k 0)))) (h "1jl3lvyfjf7c3cx28f2qmckq2cg49zqymj1lm369qsn252vjfcqs") (y #t)))

(define-public crate-eventree-0.4.1 (c (n "eventree") (v "0.4.1") (d (list (d (n "expect-test") (r "^1.1") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "text-size") (r "^1.1") (d #t) (k 0)))) (h "1yxyhwgx86gfy341gb9p7zh5icn5gmdx55x1gpgzcngmzc93gnc9")))

(define-public crate-eventree-0.5.0 (c (n "eventree") (v "0.5.0") (d (list (d (n "expect-test") (r "^1.1") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "text-size") (r "^1.1") (d #t) (k 0)))) (h "0slpjx0d556klcsc04sdx4mnx8a1z7s0yqlgxd1d8q6kwavbm937")))

(define-public crate-eventree-0.6.0 (c (n "eventree") (v "0.6.0") (d (list (d (n "expect-test") (r "^1.1") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "text-size") (r "^1.1") (d #t) (k 0)))) (h "12ajrmbxs0b7v16kdgz3k2rqzw5r3bc0cbfg5l3h0ch6klhm9zn0")))

(define-public crate-eventree-0.6.1 (c (n "eventree") (v "0.6.1") (d (list (d (n "expect-test") (r "^1.1") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "text-size") (r "^1.1") (d #t) (k 0)))) (h "0f4mnw21yywfr3gscy9wxzyzzj5615l36p7klkvinrm7p76zm72m")))

(define-public crate-eventree-0.6.2 (c (n "eventree") (v "0.6.2") (d (list (d (n "expect-test") (r "^1.1") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 0)) (d (n "text-size") (r "^1.1") (d #t) (k 0)))) (h "0364frjrgx6rhrnf98hixcfh2i3fc7w1i46v6q8kjz8fzn459m47")))

(define-public crate-eventree-0.7.0 (c (n "eventree") (v "0.7.0") (d (list (d (n "expect-test") (r "^1.4.1") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "0888wql8jgkqac102l837svjlq60b3x01il5lkvkplnqnhxargwb")))

