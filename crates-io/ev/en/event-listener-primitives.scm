(define-module (crates-io ev en event-listener-primitives) #:use-module (crates-io))

(define-public crate-event-listener-primitives-0.1.0 (c (n "event-listener-primitives") (v "0.1.0") (h "0l468zcmvgx2gbkgllhcyvnfa798kjv1nm8mgw42y8whzwnwlvki")))

(define-public crate-event-listener-primitives-0.1.1 (c (n "event-listener-primitives") (v "0.1.1") (h "0cxybawsz048nmi2b0phrb5y1z4csg7y2ik8nalrw16nc7vzbb1c")))

(define-public crate-event-listener-primitives-0.2.0 (c (n "event-listener-primitives") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "tinyvec") (r "^1.1.0") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1wvmf8a8zljqhmvzxvhk8r2ln2yq07xi19vakh67jvslg7yp8b5z") (y #t)))

(define-public crate-event-listener-primitives-0.2.1 (c (n "event-listener-primitives") (v "0.2.1") (d (list (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "tinyvec") (r "^1.1.0") (f (quote ("alloc"))) (d #t) (k 0)))) (h "0rqjkzr3h43a2g90jlib227fdv9xg3kbphw6di5dicj7hz0zx4mz")))

(define-public crate-event-listener-primitives-0.2.2 (c (n "event-listener-primitives") (v "0.2.2") (d (list (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "tinyvec") (r "^1.1.0") (f (quote ("alloc"))) (d #t) (k 0)))) (h "0n3aa99w7848xm9f46vsf3gnqhz78zfcv39lvlqa8slsf17vwlhd")))

(define-public crate-event-listener-primitives-1.0.0 (c (n "event-listener-primitives") (v "1.0.0") (d (list (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "00wsy041l5f6yjr40h4yivc3ll90p9h22fj78d04d6pinbsi8nl6")))

(define-public crate-event-listener-primitives-2.0.0 (c (n "event-listener-primitives") (v "2.0.0") (d (list (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "1qrbf0jl1izi157217macin0nhzg3iyzpsj4jq9rva0s12gad9b9")))

(define-public crate-event-listener-primitives-2.0.1 (c (n "event-listener-primitives") (v "2.0.1") (d (list (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (d #t) (k 0)))) (h "1302jflraj45ay19cpq2n79pm5qr9zfw63rgqk43qfam88xhk12k")))

