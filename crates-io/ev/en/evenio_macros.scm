(define-module (crates-io ev en evenio_macros) #:use-module (crates-io))

(define-public crate-evenio_macros-0.1.0 (c (n "evenio_macros") (v "0.1.0") (h "1hq7qn461w70l69ygcdyfwgl2kwphvrkgr0h8rkdc9iws8haimf0") (y #t)))

(define-public crate-evenio_macros-0.1.1 (c (n "evenio_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1kswfglh6nqg22b0nki18fa2d03zjhdw8w25r1znbs3a0w7rwlwb")))

(define-public crate-evenio_macros-0.2.0 (c (n "evenio_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "0rdcn6hvyzzmi83hrl9bb4hh9bj1f19p5wxvvh4r5mkqh9rajy3b") (y #t)))

(define-public crate-evenio_macros-0.2.1 (c (n "evenio_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1a6k96qfcp8vrvff83sp9ykr0kzc9db749b8xjn6f7w14q1dcldm")))

(define-public crate-evenio_macros-0.2.2 (c (n "evenio_macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "0vkk3l9kd12bwn4c5dv6q3azrb3n70j4icn1c6dljjmkjaagnfix")))

(define-public crate-evenio_macros-0.3.0 (c (n "evenio_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "0sfk4mg08z685j1lbnwm5n91b1mzkdd0aycq9q6x5hs6lf8am47q")))

(define-public crate-evenio_macros-0.4.0 (c (n "evenio_macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "0ch5h0l611gff03i7mi522pjkrkgywcpdhdkga73y2w31lppdlss")))

(define-public crate-evenio_macros-0.4.1 (c (n "evenio_macros") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "12mm3nhvwq3p65z6kb1sb4214j7h5rvqdvjk8jgxryhj5zyasmdz")))

(define-public crate-evenio_macros-0.4.2 (c (n "evenio_macros") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1n7h2i85ly4siv4wswr26mwvbasy17zyifl70kfk61c4xy3vvj0z")))

(define-public crate-evenio_macros-0.5.0 (c (n "evenio_macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "0hhjpi6qiwv6apm4g662dvqbysv8ncmd4dwmnk6cfzqn9lp2mrr4")))

(define-public crate-evenio_macros-0.6.0 (c (n "evenio_macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "15q0rxkl95hv7ldv0lpb6p1hg201dymwpcxx9xrxbd7x5qcw3a2s") (r "1.78.0")))

