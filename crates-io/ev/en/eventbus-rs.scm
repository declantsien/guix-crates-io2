(define-module (crates-io ev en eventbus-rs) #:use-module (crates-io))

(define-public crate-eventbus-rs-0.1.0 (c (n "eventbus-rs") (v "0.1.0") (h "1zflx0h2ygpbf5fi9l0ikyxnk5a2gmqi0dr85rmh210x2hbvhakv") (y #t)))

(define-public crate-eventbus-rs-1.0.0 (c (n "eventbus-rs") (v "1.0.0") (h "1z66fq4pmnzkf04858i66f8f3376dsh8id9nd1f82w7nrc135kxm") (y #t)))

(define-public crate-eventbus-rs-1.0.1 (c (n "eventbus-rs") (v "1.0.1") (h "16xf10iyn9qy1vr5r10s4p13dz6ij1q5a82vxqwa7d689j9rswsq") (y #t)))

(define-public crate-eventbus-rs-1.1.0 (c (n "eventbus-rs") (v "1.1.0") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (f (quote ("thread-pool"))) (d #t) (k 2)))) (h "067xaakx1328zjfp79qffp7g0gzp46pgcrr3qi7008mf4slylwh7") (y #t)))

(define-public crate-eventbus-rs-1.1.1 (c (n "eventbus-rs") (v "1.1.1") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (f (quote ("thread-pool"))) (d #t) (k 2)))) (h "1n0ngmdfmjxksxx5p25kz89cz24y9f6f4q4z4530p7abq0sl53b0") (y #t)))

(define-public crate-eventbus-rs-1.1.2 (c (n "eventbus-rs") (v "1.1.2") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (f (quote ("thread-pool"))) (d #t) (k 2)))) (h "0a8y2fsrcdp5g3f6c56p870dgp3xh3091q7fw5abraisyz125nq1") (y #t)))

(define-public crate-eventbus-rs-1.1.4 (c (n "eventbus-rs") (v "1.1.4") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (f (quote ("thread-pool"))) (d #t) (k 2)))) (h "1nl0j1rhyildzxbvrq80rgfxdf8n8bbln288dsn63brwp628v1gy") (y #t)))

(define-public crate-eventbus-rs-1.1.5 (c (n "eventbus-rs") (v "1.1.5") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (f (quote ("thread-pool"))) (d #t) (k 2)))) (h "1lqy2hi1yaa4l24y6nwfvmxqvn1yd7fa3jk5a7ybvxmlx0y0w5v9") (y #t)))

(define-public crate-eventbus-rs-1.1.6 (c (n "eventbus-rs") (v "1.1.6") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (f (quote ("thread-pool"))) (d #t) (k 2)))) (h "0gbsr5aph02qasp5qahaj4gx5kbyvzh3arwix5zajv9vs5zngg5b") (y #t)))

(define-public crate-eventbus-rs-1.2.0 (c (n "eventbus-rs") (v "1.2.0") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (f (quote ("thread-pool"))) (d #t) (k 2)))) (h "09sm9x9xv219myxm5slcg43lj31nd60s9pmkb4gz3838mkha8yml") (y #t)))

(define-public crate-eventbus-rs-1.3.0 (c (n "eventbus-rs") (v "1.3.0") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (f (quote ("thread-pool"))) (d #t) (k 2)))) (h "01a7dkixh0jzl9qh9g4ji0c5v4ygk9c8wfnkwb8azad91fif84k7") (y #t)))

(define-public crate-eventbus-rs-1.3.1 (c (n "eventbus-rs") (v "1.3.1") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (f (quote ("thread-pool"))) (d #t) (k 2)))) (h "1l9pk6viqnid2vyr2j5x3dgmakia9z4rr1vchbwwfjhmh318q17j") (y #t)))

(define-public crate-eventbus-rs-1.3.2 (c (n "eventbus-rs") (v "1.3.2") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (d #t) (k 0)) (d (n "futures") (r "^0.3.19") (f (quote ("thread-pool"))) (d #t) (k 2)))) (h "0kwhwzl1h64pxcbfakbj77w63bm4xz023bqnxdq0kdsw9zhm0d0y") (y #t)))

