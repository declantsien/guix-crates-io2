(define-module (crates-io ev en eventsourcing-derive) #:use-module (crates-io))

(define-public crate-eventsourcing-derive-0.1.0 (c (n "eventsourcing-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "1xrhzzx5kyl0s5xwl4sf3cc2f0smqdh9rr9hr9zmi3mv8h07ldb9")))

(define-public crate-eventsourcing-derive-0.1.1 (c (n "eventsourcing-derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "14dm4gajw24gcqxisdj7q3b4jdbaqsnp01gfv8cgn85n26qxmnd8")))

(define-public crate-eventsourcing-derive-0.1.2 (c (n "eventsourcing-derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "0n69lrxlk8dyqp9gw7vfa1bpdiwi8sxcsk8g1flnw1nj60qzgvpn")))

(define-public crate-eventsourcing-derive-0.1.3 (c (n "eventsourcing-derive") (v "0.1.3") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "1lnn5v68nh48j46jjnllnzg68i5b5g7c09a7yad4mycpyr1zq8br")))

