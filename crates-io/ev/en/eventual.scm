(define-module (crates-io ev en eventual) #:use-module (crates-io))

(define-public crate-eventual-0.0.1 (c (n "eventual") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.3.0") (d #t) (k 2)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "syncbox") (r "^0.1.0") (d #t) (k 0)) (d (n "time") (r "^0.1.22") (d #t) (k 2)))) (h "13vvh1fizrwgmzjsbrsc633pgpf5jlkmn8q8ay1llkm60fgzsv1z")))

(define-public crate-eventual-0.1.0 (c (n "eventual") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3.0") (d #t) (k 2)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "syncbox") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "^0.1.22") (d #t) (k 2)))) (h "03frlbgk39hjd1az7c2f13wg0lzipvz3igk8vrfz2xnxfzmjcavz")))

(define-public crate-eventual-0.1.1 (c (n "eventual") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.3.0") (d #t) (k 2)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "syncbox") (r "^0.2.1") (d #t) (k 0)) (d (n "time") (r "^0.1.25") (d #t) (k 0)))) (h "110m4j91c0r16r3b3dpb1sh5b4pvx8zlpjcb2ncfg4wzz3kpgysf")))

(define-public crate-eventual-0.1.2 (c (n "eventual") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.3.0") (d #t) (k 2)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "syncbox") (r "^0.2.1") (d #t) (k 0)) (d (n "time") (r "^0.1.25") (d #t) (k 0)))) (h "0ivchmg3g5qn43s3pzc383x6ma9v7mc5khly8lx445ibyrsqg136")))

(define-public crate-eventual-0.1.3 (c (n "eventual") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.3.0") (d #t) (k 2)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "syncbox") (r "^0.2.3") (d #t) (k 0)) (d (n "time") (r "^0.1.25") (d #t) (k 0)))) (h "1nk0gg5b6lmcyzapkrffvx1bjfvy4dxn7yc094b0gqwrzqwwqxqn")))

(define-public crate-eventual-0.1.4 (c (n "eventual") (v "0.1.4") (d (list (d (n "env_logger") (r "^0.3.0") (d #t) (k 2)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "syncbox") (r "^0.2.3") (d #t) (k 0)) (d (n "time") (r "^0.1.25") (d #t) (k 0)))) (h "1v4kbxgivway5ash8hkx7jr8lj5cmhzy3rshiq1708zmbgnhg7wy")))

(define-public crate-eventual-0.1.5 (c (n "eventual") (v "0.1.5") (d (list (d (n "env_logger") (r "^0.3.0") (d #t) (k 2)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "syncbox") (r "^0.2.3") (d #t) (k 0)) (d (n "time") (r "^0.1.25") (d #t) (k 0)))) (h "15yy5m5zf06k1zkg57wp60raxrh25m1yk75lp606yvfw5p3axq7z")))

(define-public crate-eventual-0.1.6 (c (n "eventual") (v "0.1.6") (d (list (d (n "env_logger") (r "^0.3.0") (d #t) (k 2)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "syncbox") (r "^0.2.3") (d #t) (k 0)) (d (n "time") (r "^0.1.25") (d #t) (k 0)))) (h "1ij7zs1h2s11cbrin3xp2fnwvnf8qkg9sgl9yifp0c2464h5885s")))

(define-public crate-eventual-0.1.7 (c (n "eventual") (v "0.1.7") (d (list (d (n "env_logger") (r "^0.3.0") (d #t) (k 2)) (d (n "log") (r "^0.3.0") (d #t) (k 0)) (d (n "syncbox") (r "^0.2.3") (d #t) (k 0)) (d (n "time") (r "^0.1.25") (d #t) (k 0)))) (h "0jzmlfgqgzqwyndbxf4pmj5rnc11lkybbznnyd8cld5li78adgdr")))

