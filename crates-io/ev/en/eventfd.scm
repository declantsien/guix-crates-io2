(define-module (crates-io ev en eventfd) #:use-module (crates-io))

(define-public crate-eventfd-0.0.1 (c (n "eventfd") (v "0.0.1") (h "1ws7sh2szswx2f8q2ab9lndbjhvs61gj80m0y9l64znc8b91wmsj")))

(define-public crate-eventfd-0.0.2 (c (n "eventfd") (v "0.0.2") (h "0lh2qv5rzqipxc5myqbha5b8drqi8wzf32k9pnb57304q51y6hl8")))

(define-public crate-eventfd-0.0.3 (c (n "eventfd") (v "0.0.3") (h "17pnniaci2jic5pf650abdjj6vkf2fxgmphmz873sna839iasri4")))

(define-public crate-eventfd-0.0.4 (c (n "eventfd") (v "0.0.4") (h "1bckbn473lrvkh45fb1pijy4pdmsrg11pkjvardjmglhiymxp3f6")))

(define-public crate-eventfd-0.0.5 (c (n "eventfd") (v "0.0.5") (h "0r7fjs8sp1q4ayfvrrhxl2ns4dqdjnwi8p5nd4wiydvc8rh7j6b9")))

(define-public crate-eventfd-0.1.0 (c (n "eventfd") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1hprdh2fa2h3ch6xhx5pnnfkywnp2mn0vky20m6xixzlm09bplin")))

(define-public crate-eventfd-0.2.0 (c (n "eventfd") (v "0.2.0") (d (list (d (n "nix") (r "^0.24") (d #t) (k 0)))) (h "1l60jpqhbzhx35cs66s8mq0p5z0d5hhgp8mp0rwqhk01m4pdwkwy")))

