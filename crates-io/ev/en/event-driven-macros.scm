(define-module (crates-io ev en event-driven-macros) #:use-module (crates-io))

(define-public crate-event-driven-macros-0.5.0 (c (n "event-driven-macros") (v "0.5.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0s0jw9i21cky9kg37chlh6wdqd96gbag00mil6zhsf5fsk08qiqf")))

(define-public crate-event-driven-macros-0.6.0 (c (n "event-driven-macros") (v "0.6.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0x6rxpia3a8cnln987adyk3a5wbbfdhy5a7ijcyasc7izlzfnqpm")))

(define-public crate-event-driven-macros-0.7.0 (c (n "event-driven-macros") (v "0.7.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0a43sdy38v618imbpvxq7qmh9qpqrlnb0hq8rcwpaiq6q101h8bn")))

