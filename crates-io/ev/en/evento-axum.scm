(define-module (crates-io ev en evento-axum) #:use-module (crates-io))

(define-public crate-evento-axum-0.6.0 (c (n "evento-axum") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.74") (d #t) (k 0)) (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "060v2v68af58hlcm71cjr6hhpbjjfayrl9z2k6b17ynpkh2bw9kj")))

(define-public crate-evento-axum-0.6.1 (c (n "evento-axum") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.74") (d #t) (k 0)) (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "0xdx4q36xmwc0f9mxx9jjj333rqvf40xcr721cqhypd30my65242")))

(define-public crate-evento-axum-0.6.2 (c (n "evento-axum") (v "0.6.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "1a7iq6lpl8yrrndjblxrzgpq5jm5sg19j0crg04ff0dvz8sfyv8h")))

(define-public crate-evento-axum-0.6.3 (c (n "evento-axum") (v "0.6.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "17nzn7y7rnfb2zrn968cbahc10lj9629fn5h3jl3s88vq9p4li5i")))

(define-public crate-evento-axum-0.7.0 (c (n "evento-axum") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0hn8an0hyw70iwrv7mjp1angi2bka6dmcl2sdmbf2vbcwskfwsxb")))

(define-public crate-evento-axum-0.7.1 (c (n "evento-axum") (v "0.7.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0zabdi3ygqa04x5acjjqcyd2f1kb6sq6nkxi9rhp3ysh49bbajl1")))

(define-public crate-evento-axum-0.7.2 (c (n "evento-axum") (v "0.7.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "140gaqjb23qmywrkkqgdrk79l08aax8975qx8mps57jz4pqagc0x")))

(define-public crate-evento-axum-0.8.0 (c (n "evento-axum") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0p396wflj2pamghyxj517g0pkc0wnd2qqywn5lm71k15h19i5zqk")))

(define-public crate-evento-axum-0.9.0 (c (n "evento-axum") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0jz9qlznsnwjdrj99mki9ka7g8z8wzvxs561bcgzbp7hs0w25xz6")))

(define-public crate-evento-axum-0.9.1 (c (n "evento-axum") (v "0.9.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "047ybncrg6j99q3qjqsvcs9mnsl6zv9dc9ql9yzjn99wlpg3mvkm")))

(define-public crate-evento-axum-1.0.0-alpha.1 (c (n "evento-axum") (v "1.0.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1xmnvlbcmcszm5dyvkssxrvb17lfcb27il3qhq9dfaxny5jfbgpp")))

(define-public crate-evento-axum-1.0.0-alpha.2 (c (n "evento-axum") (v "1.0.0-alpha.2") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "evento") (r "^1.0.0-alpha.2") (d #t) (k 0)) (d (n "evento-store") (r "^1.0.0-alpha.2") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "tracing") (r "^0.1.39") (d #t) (k 0)) (d (n "validator") (r "^0.16.1") (d #t) (k 0)))) (h "0fz8sqvnhr2rd0x5975mlrlvw057l61b8fwx9h0hy9sykmvig2s6")))

(define-public crate-evento-axum-1.0.0-alpha.4 (c (n "evento-axum") (v "1.0.0-alpha.4") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "axum") (r "^0.6.20") (d #t) (k 0)) (d (n "evento") (r "^1.0.0-alpha.4") (d #t) (k 0)) (d (n "evento-store") (r "^1.0.0-alpha.4") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "tracing") (r "^0.1.39") (d #t) (k 0)) (d (n "validator") (r "^0.16.1") (d #t) (k 0)))) (h "13rpf8m86nxymhs9l2w1wsi56rc2rivmp1852s81awrqa0p0pspx")))

(define-public crate-evento-axum-1.0.0-alpha.5 (c (n "evento-axum") (v "1.0.0-alpha.5") (d (list (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "axum") (r "^0.7.3") (d #t) (k 0)) (d (n "evento") (r "^1.0.0-alpha.5") (d #t) (k 0)) (d (n "evento-store") (r "^1.0.0-alpha.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "validator") (r "^0.16.1") (d #t) (k 0)))) (h "0jlza5dfrjg3f13n0w1c8h0llp9drmh25i0fivsg0rajlj6rk32l")))

(define-public crate-evento-axum-1.0.0-alpha.6 (c (n "evento-axum") (v "1.0.0-alpha.6") (d (list (d (n "async-trait") (r "^0.1.75") (d #t) (k 0)) (d (n "axum") (r "^0.7.3") (d #t) (k 0)) (d (n "evento") (r "^1.0.0-alpha.6") (d #t) (k 0)) (d (n "evento-store") (r "^1.0.0-alpha.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "validator") (r "^0.16.1") (d #t) (k 0)))) (h "1jpigia0mymigzd8h2p2pfyznppxw9jq58i66412q7rd3i21zhx0")))

(define-public crate-evento-axum-1.0.0-alpha.7 (c (n "evento-axum") (v "1.0.0-alpha.7") (d (list (d (n "axum") (r "^0.7.3") (d #t) (k 0)))) (h "1s9b25hb2s9agnbbng27z8l7c1fbf5ksbb9b94fnwswipqzhkbkr")))

(define-public crate-evento-axum-1.0.0-alpha.8 (c (n "evento-axum") (v "1.0.0-alpha.8") (d (list (d (n "axum") (r "^0.7.3") (d #t) (k 0)))) (h "0z75l6cs1jvcj75cn2k3n0mxz2rcyr0ahirixfcgizbv74z312fl")))

(define-public crate-evento-axum-0.10.0 (c (n "evento-axum") (v "0.10.0") (d (list (d (n "axum") (r "^0.7.3") (d #t) (k 0)))) (h "12x7434nv7l0zm3xcrj5ixwv5b6swvifwviyr9a5pq83fadzzgxq")))

(define-public crate-evento-axum-0.10.1 (c (n "evento-axum") (v "0.10.1") (d (list (d (n "axum") (r "^0.7.4") (d #t) (k 0)))) (h "1a715n6sf1myzyykd4g6vygmf2l9vhrk2j98j20vmbm0qf1fwlyk")))

(define-public crate-evento-axum-0.10.2 (c (n "evento-axum") (v "0.10.2") (d (list (d (n "axum") (r "^0.7.4") (d #t) (k 0)))) (h "17amci16xk34bz63hddspg2xwnl5syq841jmhxl7cmy1cmdy3sv6")))

