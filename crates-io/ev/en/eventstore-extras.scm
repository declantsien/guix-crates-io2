(define-module (crates-io ev en eventstore-extras) #:use-module (crates-io))

(define-public crate-eventstore-extras-0.1.0 (c (n "eventstore-extras") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "eventstore") (r "^2.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1j74kfhxfmqh2zn8fii3ii6hsv82qi9dixg9izcp86sq8p0ldaml")))

