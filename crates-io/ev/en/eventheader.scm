(define-module (crates-io ev en eventheader) #:use-module (crates-io))

(define-public crate-eventheader-0.1.0 (c (n "eventheader") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (t "cfg(target_os = \"linux\")") (k 0)) (d (n "uuid") (r ">=1.1") (d #t) (k 2)))) (h "10wvxbz6s1sdqks80kqfy4qlw5hdkrw8ir93qwp0a46ihfzr1436") (f (quote (("user_events") ("default" "user_events")))) (r "1.63")))

(define-public crate-eventheader-0.2.0 (c (n "eventheader") (v "0.2.0") (d (list (d (n "eventheader_macros") (r "=0.2.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (t "cfg(target_os = \"linux\")") (k 0)) (d (n "uuid") (r ">=1.1") (d #t) (k 2)))) (h "0kldjcdp45wxnpws2yrizpasvrpk176hrw0znq0dx3yr7a0a6zjk") (f (quote (("user_events") ("default" "user_events" "macros")))) (s 2) (e (quote (("macros" "dep:eventheader_macros")))) (r "1.63")))

(define-public crate-eventheader-0.3.0 (c (n "eventheader") (v "0.3.0") (d (list (d (n "eventheader_macros") (r "=0.3.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (t "cfg(target_os = \"linux\")") (k 0)) (d (n "uuid") (r ">=1.1") (d #t) (k 2)))) (h "1q29wmmwccqsa3x7874mj2ad018sxq7m9dclgx1sws0cn5v6g0ya") (f (quote (("user_events") ("default" "user_events" "macros")))) (s 2) (e (quote (("macros" "dep:eventheader_macros")))) (r "1.63")))

(define-public crate-eventheader-0.3.1 (c (n "eventheader") (v "0.3.1") (d (list (d (n "eventheader_macros") (r "=0.3.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (t "cfg(target_os = \"linux\")") (k 0)) (d (n "uuid") (r ">=1.1") (d #t) (k 2)))) (h "0h31lybpgdqdi3fhn8a8s8b70j3jf0sbwlly4xwmh1p52iyk0gmr") (f (quote (("user_events") ("default" "user_events" "macros")))) (s 2) (e (quote (("macros" "dep:eventheader_macros")))) (r "1.63")))

(define-public crate-eventheader-0.3.2 (c (n "eventheader") (v "0.3.2") (d (list (d (n "eventheader_macros") (r "=0.3.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (t "cfg(target_os = \"linux\")") (k 0)) (d (n "uuid") (r ">=1.1") (d #t) (k 2)))) (h "0brkgcfzng7s0wb6axfcnfmaid7krivg0194y0wgkmcnnrj7kwl6") (f (quote (("user_events") ("default" "user_events" "macros")))) (s 2) (e (quote (("macros" "dep:eventheader_macros")))) (r "1.63")))

(define-public crate-eventheader-0.3.4 (c (n "eventheader") (v "0.3.4") (d (list (d (n "eventheader_macros") (r "=0.3.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (t "cfg(target_os = \"linux\")") (k 0)) (d (n "uuid") (r ">=1.1") (d #t) (k 2)))) (h "1rly72vqai91bk2mxla5sn82m3c8wv0mvc5nd5dkkn4mf24m65gz") (f (quote (("user_events") ("default" "user_events" "macros")))) (s 2) (e (quote (("macros" "dep:eventheader_macros")))) (r "1.63")))

(define-public crate-eventheader-0.3.5 (c (n "eventheader") (v "0.3.5") (d (list (d (n "eventheader_macros") (r "=0.3.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (t "cfg(target_os = \"linux\")") (k 0)) (d (n "uuid") (r ">=1.1") (d #t) (k 2)))) (h "0n6bsx4mgq8q7qh3zcnv47i71sk4ndx5cw0qz49f96qgr77x1wxb") (f (quote (("user_events") ("default" "user_events" "macros")))) (s 2) (e (quote (("macros" "dep:eventheader_macros")))) (r "1.63")))

(define-public crate-eventheader-0.4.0 (c (n "eventheader") (v "0.4.0") (d (list (d (n "eventheader_macros") (r "=0.3.0") (o #t) (d #t) (k 0)) (d (n "tracepoint") (r "=0.4.0") (k 0)) (d (n "uuid") (r ">=1.1") (d #t) (k 2)))) (h "0kkvda4adfn6is2hxqyzmqjd081fird7k6nsv43xjg7s0ly635mc") (f (quote (("user_events" "tracepoint/user_events") ("default" "user_events" "macros")))) (s 2) (e (quote (("macros" "dep:eventheader_macros")))) (r "1.63")))

