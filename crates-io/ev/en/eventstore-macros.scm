(define-module (crates-io ev en eventstore-macros) #:use-module (crates-io))

(define-public crate-eventstore-macros-0.0.1 (c (n "eventstore-macros") (v "0.0.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0f4zx4k55pbj6lg47prx89qinb3l56f4phn2i0dfpxgq6prgcmcm")))

