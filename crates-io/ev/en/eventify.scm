(define-module (crates-io ev en eventify) #:use-module (crates-io))

(define-public crate-eventify-0.0.0 (c (n "eventify") (v "0.0.0") (h "1cbibsnhz3pzhs6lg231h89i8w0slbbm6033hi81832fya9fr5kv") (y #t)))

(define-public crate-eventify-0.1.0-alpha.0 (c (n "eventify") (v "0.1.0-alpha.0") (h "0h3fqjh8qmgckc03ss1nkgidydcck538bb4azdr4pglfvdi9chxn")))

