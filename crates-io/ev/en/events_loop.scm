(define-module (crates-io ev en events_loop) #:use-module (crates-io))

(define-public crate-events_loop-0.1.0 (c (n "events_loop") (v "0.1.0") (h "17yb4506kgg99m8l2c361dalx7ig5s7bmzfp6nzfbqmgdccfvk80")))

(define-public crate-events_loop-0.1.1 (c (n "events_loop") (v "0.1.1") (h "1qyaz9755mjrxwkxnafh1n6vaqjlpmlpmv3dmnx884w3avwcxn16")))

