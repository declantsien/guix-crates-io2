(define-module (crates-io ev en eventful-test-shared) #:use-module (crates-io))

(define-public crate-eventful-test-shared-0.1.0-alpha.0 (c (n "eventful-test-shared") (v "0.1.0-alpha.0") (d (list (d (n "eventful") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kr848w71r7b426ifsgrdby4zk19xp87apym679rvlj3sqm08gn3")))

