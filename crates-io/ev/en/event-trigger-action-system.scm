(define-module (crates-io ev en event-trigger-action-system) #:use-module (crates-io))

(define-public crate-event-trigger-action-system-0.1.0 (c (n "event-trigger-action-system") (v "0.1.0") (d (list (d (n "btreemultimap-value-ord") (r "^0.3.1") (d #t) (k 0)))) (h "1gzxbpfndcg8fl8qafy1cdzkgg6a8rwbrm6li32r90i29jx558ix") (r "1.56.1")))

(define-public crate-event-trigger-action-system-0.2.0 (c (n "event-trigger-action-system") (v "0.2.0") (d (list (d (n "btreemultimap-value-ord") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0srnq0b6ahb5fyx481yrvkdzy0xw99nfmf0a25522airawsq840c") (s 2) (e (quote (("serde" "btreemultimap-value-ord/serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-event-trigger-action-system-0.3.0 (c (n "event-trigger-action-system") (v "0.3.0") (d (list (d (n "btreemultimap-value-ord") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1n6lvbhla8v1j2cbvsxdrf452yy9nvhpyg0jpz1xr9hkpl01rd51") (s 2) (e (quote (("serde" "btreemultimap-value-ord/serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-event-trigger-action-system-0.4.0 (c (n "event-trigger-action-system") (v "0.4.0") (d (list (d (n "btreemultimap-value-ord") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0s8060l6pmb4z5jscj81n9djm7qpw9ai1mnaaz8qywaba6symxy1") (s 2) (e (quote (("serde" "btreemultimap-value-ord/serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-event-trigger-action-system-0.5.0 (c (n "event-trigger-action-system") (v "0.5.0") (d (list (d (n "btreemultimap-value-ord") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "03hhppp929hxq78rclm5jd03dwd39n0j98gqazpzgv1sdq3x5y2r") (s 2) (e (quote (("serde" "btreemultimap-value-ord/serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-event-trigger-action-system-0.6.0 (c (n "event-trigger-action-system") (v "0.6.0") (d (list (d (n "btreemultimap-value-ord") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1hfy12wh2zq1ar1c55jgay7siamssa0j6jm5ih8scq6nbhiaprl1") (s 2) (e (quote (("serde" "btreemultimap-value-ord/serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-event-trigger-action-system-0.6.1 (c (n "event-trigger-action-system") (v "0.6.1") (d (list (d (n "btreemultimap-value-ord") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0lz97xv5yj3lb3lchc43r8sc0y36nfdwv86igs537sg8db7c28x6") (s 2) (e (quote (("serde" "btreemultimap-value-ord/serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-event-trigger-action-system-0.6.2 (c (n "event-trigger-action-system") (v "0.6.2") (d (list (d (n "btreemultimap-value-ord") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0avdg04z4v5nvbjck7bmhp7avzwvxcvigv6kl12a60inwyf64h99") (s 2) (e (quote (("serde" "btreemultimap-value-ord/serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-event-trigger-action-system-0.6.3 (c (n "event-trigger-action-system") (v "0.6.3") (d (list (d (n "btreemultimap-value-ord") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "03imlzz7raxcidl6x00f6xlwwp3vy5mxm33bvdvj18y121qn1dms") (s 2) (e (quote (("serde" "btreemultimap-value-ord/serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-event-trigger-action-system-0.7.0 (c (n "event-trigger-action-system") (v "0.7.0") (d (list (d (n "btreemultimap-value-ord") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0bas08nyi8zb8k8gva0zb7fr0kl04dg3vlk904fva1jm6abibgpv") (s 2) (e (quote (("serde" "btreemultimap-value-ord/serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-event-trigger-action-system-0.7.1 (c (n "event-trigger-action-system") (v "0.7.1") (d (list (d (n "btreemultimap-value-ord") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "026m6xq440vqrxp502mbr9iyyxm86972lldyfabx53mm25vrwf9c") (s 2) (e (quote (("serde" "btreemultimap-value-ord/serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-event-trigger-action-system-0.7.2 (c (n "event-trigger-action-system") (v "0.7.2") (d (list (d (n "btreemultimap-value-ord") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "165r4zzdayn4kdvkc7mmch6k124y2wn534ml12ljzm4g1j7fs2an") (s 2) (e (quote (("serde" "btreemultimap-value-ord/serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-event-trigger-action-system-0.7.3 (c (n "event-trigger-action-system") (v "0.7.3") (d (list (d (n "btreemultimap-value-ord") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1x8s6zpjf3gz8lz5hnikwkdla1id856jqrfzb3wx5jjg1c5xlkpz") (s 2) (e (quote (("serde" "btreemultimap-value-ord/serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-event-trigger-action-system-0.7.4 (c (n "event-trigger-action-system") (v "0.7.4") (d (list (d (n "btreemultimap-value-ord") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1xcmdnbin3ic0hggsia3gk6wn2sniy12k3yv8i0jkr1606la6rzm") (s 2) (e (quote (("serde" "btreemultimap-value-ord/serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-event-trigger-action-system-0.7.5 (c (n "event-trigger-action-system") (v "0.7.5") (d (list (d (n "btreemultimap-value-ord") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xg3696kw95c661y5pf72yv56kkjlp83d6yb44vf493fdjjggzmd") (s 2) (e (quote (("serde" "btreemultimap-value-ord/serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-event-trigger-action-system-0.7.6 (c (n "event-trigger-action-system") (v "0.7.6") (d (list (d (n "btreemultimap-value-ord") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0y7a8h9dg1phkv7wjzygmmvf3hiaiqhvrjmb57imz8hzk2s3mxvd") (s 2) (e (quote (("serde" "btreemultimap-value-ord/serde" "dep:serde")))) (r "1.60.0")))

