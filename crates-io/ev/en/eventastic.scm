(define-module (crates-io ev en eventastic) #:use-module (crates-io))

(define-public crate-eventastic-0.1.0 (c (n "eventastic") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1bjcs8vcqk1l7akzprkxr9v34qaig9j06a51xnpqfdkddcvhxf98") (y #t)))

(define-public crate-eventastic-0.2.0 (c (n "eventastic") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "139sj99hxncv8k0k6ivsxb7cq0rs7vly88fg3mdlppfb4an9majs") (y #t)))

(define-public crate-eventastic-0.2.1 (c (n "eventastic") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1rqllnc2ljighk5bnk5r3llv89wqbn22jqjxdf8kfkxd4b18dikh") (y #t)))

(define-public crate-eventastic-0.3.0 (c (n "eventastic") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1r5ycciwmydyaigaags3w88iplhd33yvzq06mayy328l707zx3zf")))

(define-public crate-eventastic-0.3.1 (c (n "eventastic") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0a07xxp7h8bryv1vs62vzlv75qk6jfr0icdymgn4xj28s9kjp0jm")))

