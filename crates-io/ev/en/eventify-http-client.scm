(define-module (crates-io ev en eventify-http-client) #:use-module (crates-io))

(define-public crate-eventify-http-client-0.0.0-reserved (c (n "eventify-http-client") (v "0.0.0-reserved") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0xb2rj3wdy2w2m2195g8d4y9vnh6s9dffrny1iqzrzkhbbl2bifr")))

