(define-module (crates-io ev en evenodd_checker) #:use-module (crates-io))

(define-public crate-evenodd_checker-0.1.0 (c (n "evenodd_checker") (v "0.1.0") (h "00zrpv7y3hxggzvd0i9rrvly4i33d149472zx8k8r3dpisqfik6s")))

(define-public crate-evenodd_checker-0.2.0 (c (n "evenodd_checker") (v "0.2.0") (h "157dmvbzhqy9ghgpfz4zidyxdb46v3hahz1ljdkswvwif6s52npi")))

