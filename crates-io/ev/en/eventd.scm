(define-module (crates-io ev en eventd) #:use-module (crates-io))

(define-public crate-eventd-0.1.0 (c (n "eventd") (v "0.1.0") (d (list (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "16v8l12larcmp469y9ghyb774m613lg8nl0bpr93didip46v6pfg")))

(define-public crate-eventd-0.1.1 (c (n "eventd") (v "0.1.1") (d (list (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0a07dimy712zrdparq6fg43xxq0plyq8iaq18ypifwb24yk45w96")))

(define-public crate-eventd-0.2.0 (c (n "eventd") (v "0.2.0") (d (list (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0kq00q8k8jd04w3lalcw1rwd51rj7ggnl5cph5l3p90mazm9ixq0")))

(define-public crate-eventd-0.3.0 (c (n "eventd") (v "0.3.0") (d (list (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "1yxcj9m1l094b615nvf0spqvznp34g7wpawl710qw0m2xy12s4jm")))

(define-public crate-eventd-0.3.1 (c (n "eventd") (v "0.3.1") (d (list (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "1r2l43p9syki8a8m6pblr4fxqkqv4j66xszqvpbm0mc42syvk81z")))

(define-public crate-eventd-0.3.2 (c (n "eventd") (v "0.3.2") (d (list (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "1sslygck7d1hb34qpxfjgp64pxsxjalz3f0cgmadd9ma8s5rfscn")))

