(define-module (crates-io ev en event-countdown) #:use-module (crates-io))

(define-public crate-event-countdown-1.0.4 (c (n "event-countdown") (v "1.0.4") (d (list (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13yay0427wvr11igxix6ysddvkqb88irz0qxvmwprlyfzz47vvyh")))

