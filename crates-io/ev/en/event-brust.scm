(define-module (crates-io ev en event-brust) #:use-module (crates-io))

(define-public crate-event-brust-0.1.0 (c (n "event-brust") (v "0.1.0") (h "0nrr5pg61c75v7cvk5zmy44pb41ampx1ar3f78vawdayay6m6v9w") (y #t)))

(define-public crate-event-brust-0.1.1 (c (n "event-brust") (v "0.1.1") (h "1xzia7c193kf155g9mk95qcswm52caixbh7fqmc3c1m1as462m29") (y #t)))

(define-public crate-event-brust-0.1.2 (c (n "event-brust") (v "0.1.2") (h "1j9489a4fc9v80rrca7kgp6x2kvvwgg9gj7iaa6fqk530rvg6hq7") (y #t)))

(define-public crate-event-brust-0.1.3 (c (n "event-brust") (v "0.1.3") (d (list (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)))) (h "04sz4c3zw2a6642k070g54573rbqwb6nx3pp8gn2cdrbla7h65i2") (y #t)))

(define-public crate-event-brust-0.2.0 (c (n "event-brust") (v "0.2.0") (d (list (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)))) (h "06i764kwd4s6dpj4i0qxird9992ngcgk28v2k5wvf1hxbp9zdx46") (y #t)))

(define-public crate-event-brust-0.2.1 (c (n "event-brust") (v "0.2.1") (d (list (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)))) (h "0g76jr10i9rgx5asr8w8cmwad3jr05v271kd6c81s3yq48ryg80v") (y #t)))

(define-public crate-event-brust-0.2.2 (c (n "event-brust") (v "0.2.2") (d (list (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)))) (h "0fbbykrsshvwplkgs8nni81lq37dx0pa6jf2rmbmmw6ihl4acib2") (y #t)))

(define-public crate-event-brust-0.2.3 (c (n "event-brust") (v "0.2.3") (d (list (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)))) (h "0v6mdh0x2mmack6kh0f0sv2fxbgj50575c1g9acawxs9qrm5iqx6") (y #t)))

