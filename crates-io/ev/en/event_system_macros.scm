(define-module (crates-io ev en event_system_macros) #:use-module (crates-io))

(define-public crate-event_system_macros-0.1.0 (c (n "event_system_macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0jdhjzmcqiabn8jm6gvv3ffb9jc68s76krl8jqj9fkc0a3kk38xb")))

(define-public crate-event_system_macros-0.1.1 (c (n "event_system_macros") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "034fq7k0v4bgva41l00kjmdx7imif0431il1r69lry644w1ykafj")))

