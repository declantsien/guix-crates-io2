(define-module (crates-io ev en even2) #:use-module (crates-io))

(define-public crate-even2-0.0.0 (c (n "even2") (v "0.0.0") (d (list (d (n "as-any") (r "^0.2") (d #t) (k 0)))) (h "14z8svwwlblh4b8mhphv703dm07h0nr1c0l74h16x5g1p0mqwq8f")))

(define-public crate-even2-0.0.1 (c (n "even2") (v "0.0.1") (d (list (d (n "as-any") (r "^0.3") (d #t) (k 0)))) (h "0zgdk0nphww43vz4456rgc2iscf9ypqa6xmkmgwm53by1dya31qd")))

