(define-module (crates-io ev en event-notify) #:use-module (crates-io))

(define-public crate-event-notify-0.1.0 (c (n "event-notify") (v "0.1.0") (h "00f445jdyzj38fnmfmg2qhgk8yplbp34fmkjvq2xw4nh2qs6s07d") (r "1.38")))

(define-public crate-event-notify-0.1.1 (c (n "event-notify") (v "0.1.1") (h "0vl92rzc91m0qns64xad1n2r3xlvgizn9hh9fp6fi5yv6z6ggkzd") (r "1.47")))

