(define-module (crates-io ev en eventsim) #:use-module (crates-io))

(define-public crate-eventsim-0.1.0 (c (n "eventsim") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1czh1mziv0iiwy38j77a2gkl2rdanc4n3vjsf6bz7d0g1kpimx2y")))

