(define-module (crates-io ev en event-emitter-rs) #:use-module (crates-io))

(define-public crate-event-emitter-rs-0.1.0 (c (n "event-emitter-rs") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "09bvj6b5c1xddls9ib2sdv6mpglngdgkabh2gzj2m69867j5nd6w")))

(define-public crate-event-emitter-rs-0.1.1 (c (n "event-emitter-rs") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1qz510m5nsv8n6k6dxrs8h5la252qs974vwicj3fvdr39jx54n7w")))

(define-public crate-event-emitter-rs-0.1.2 (c (n "event-emitter-rs") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "17z3n7anza9jf46n3qs91d8r4xlkdx344vi23yg7zx7y4j8kcqf8")))

(define-public crate-event-emitter-rs-0.1.3 (c (n "event-emitter-rs") (v "0.1.3") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1rmpjbzdhm8glacj84z2lkw815jw51d5f9arjkm6z3dzp59a78ag")))

(define-public crate-event-emitter-rs-0.1.4 (c (n "event-emitter-rs") (v "0.1.4") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1gby05583wmkl60cfd9hz4n9y2553i1nvhj4ri9s6ys5b45dvk3x")))

