(define-module (crates-io ev en event-driven-macro) #:use-module (crates-io))

(define-public crate-event-driven-macro-0.1.0 (c (n "event-driven-macro") (v "0.1.0") (d (list (d (n "event-driven-core") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0z71rqc15mlnm6sq2j9c55p99gfhl2pjq6kxhq85y5nzlsycpck5")))

(define-public crate-event-driven-macro-0.1.5 (c (n "event-driven-macro") (v "0.1.5") (d (list (d (n "event-driven-core") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0rk5w1qpmsydqdjz2s6w01pgxlidiningw3fzkdysm87yjkgmqis")))

(define-public crate-event-driven-macro-0.1.8 (c (n "event-driven-macro") (v "0.1.8") (d (list (d (n "event-driven-core") (r "^0.1.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1k0zs0bpci32wpg1ykw60q8w089jn4wjch2vjirllzqzf1m6v337")))

(define-public crate-event-driven-macro-0.1.10 (c (n "event-driven-macro") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1gp13pw5qa7vd0hfm11cxacwixpk54nbfbk38q6yg1pfkhzhflq3")))

(define-public crate-event-driven-macro-0.1.11 (c (n "event-driven-macro") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0x9j6cg5jaxz8cpq84k6jagipi51c3kjghqqqnqghci6ilgv3jn5")))

(define-public crate-event-driven-macro-0.1.12 (c (n "event-driven-macro") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "16ld47p25r5rjpgis8yh90c6b3dafs16h2ha973q26zqvg5wqnqg")))

(define-public crate-event-driven-macro-0.1.13 (c (n "event-driven-macro") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1zkqdrx6gkg70iqp4k2adp63fl0hxyk0dljhkriqj557k5cwzyrb")))

(define-public crate-event-driven-macro-0.1.14 (c (n "event-driven-macro") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1dz6fz3ha8aki4092zvkw3wcw623pxw650qii77v7g7rp6x2l4wh")))

(define-public crate-event-driven-macro-0.1.38 (c (n "event-driven-macro") (v "0.1.38") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "00rpv4inyp3rzqjqcvyjdrpr94dadlbjskq0r2gkjrpisz12nlpy")))

(define-public crate-event-driven-macro-0.1.39 (c (n "event-driven-macro") (v "0.1.39") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "19lbiqkjsb4glhrmkn2g7g3n6sr96a6fspd61vrgpbij980p7s0w")))

(define-public crate-event-driven-macro-0.1.40 (c (n "event-driven-macro") (v "0.1.40") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0wisn23bgkbn1lrq9kr9wm5skv0as1k93gilg4mx7lvgiqvxgcnj")))

(define-public crate-event-driven-macro-0.1.41 (c (n "event-driven-macro") (v "0.1.41") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "12r7i418wbxfj55lh805vlaz4dq8gsivh7kz01cdksaxchi11mzz")))

(define-public crate-event-driven-macro-0.1.42 (c (n "event-driven-macro") (v "0.1.42") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "07b0mv9m1wnnf77ajjv0si9s7aqhlxh180qvp340nhfyc82g4wdp")))

(define-public crate-event-driven-macro-0.1.43 (c (n "event-driven-macro") (v "0.1.43") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1v2gfy2iqrrwhg7w9abvhjwpfh07pqcqgfg9dnjgjbqinkji5m2n")))

(define-public crate-event-driven-macro-0.1.44 (c (n "event-driven-macro") (v "0.1.44") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1x0fkwvkj145ijsrwxcwf52wfwhbvk76yfcrqbakj4pjc5nmf05n")))

(define-public crate-event-driven-macro-0.1.45 (c (n "event-driven-macro") (v "0.1.45") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0311clq5ifykgrvvnfai2jfx6l5y4bzxzfb8zzn8ij06yry03asm")))

(define-public crate-event-driven-macro-0.1.46 (c (n "event-driven-macro") (v "0.1.46") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1a4zsbpzs6sdf87ny0d1yx2aa9j39xz6n5i15xl8f61bikaljh02")))

(define-public crate-event-driven-macro-0.1.47 (c (n "event-driven-macro") (v "0.1.47") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0wsxl00rygx34a4zz24mgc0nxchk90yc0x5f8bg36wzqfgf35yvs")))

(define-public crate-event-driven-macro-0.1.48 (c (n "event-driven-macro") (v "0.1.48") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1ix1fsmlx9qiahkgsrsnq1hvg3zv3n59lnb114mslxcbzfklw0p5")))

(define-public crate-event-driven-macro-0.1.49 (c (n "event-driven-macro") (v "0.1.49") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "06cs7a2gflm4vly7962ghslxckdqhxmargqncdn107h3yp5bybbp")))

