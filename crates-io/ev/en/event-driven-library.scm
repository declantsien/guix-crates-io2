(define-module (crates-io ev en event-driven-library) #:use-module (crates-io))

(define-public crate-event-driven-library-0.1.0 (c (n "event-driven-library") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "downcast-rs") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "1cqb5cxwypx1lb5zv63kh2rr4clmj674wx14ssvyn75pn0mh5ika")))

(define-public crate-event-driven-library-0.1.1 (c (n "event-driven-library") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "downcast-rs") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "macro_rules_attribute") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "116s0834arkw5yw3wxs7h3vkgya8mhlqq75hbhd9xaxwdvj3aqs4")))

(define-public crate-event-driven-library-0.1.2 (c (n "event-driven-library") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "downcast-rs") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "macro_rules_attribute") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "0hw1r32irysm5gm6b9xsqri72bbn4i3b0zarmn5zs5hdzy0vc78k")))

(define-public crate-event-driven-library-0.1.3 (c (n "event-driven-library") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "downcast-rs") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "macro_rules_attribute") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "1qxbqkybif94p3d0s3i551la2in21v5p27yvnadmvyv4cgg9cc0x")))

(define-public crate-event-driven-library-0.1.4 (c (n "event-driven-library") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "downcast-rs") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "macro_rules_attribute") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "1vr6slbb1w01yl8g53avv02k0f0mwy2a08yzz6v6gc8js6kyp3rk")))

(define-public crate-event-driven-library-0.1.5 (c (n "event-driven-library") (v "0.1.5") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "downcast-rs") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "macro_rules_attribute") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "0wm59f3809qpmwm4q6w87g3xplbi23fagdyiy8905isbllf08iax")))

(define-public crate-event-driven-library-0.1.6 (c (n "event-driven-library") (v "0.1.6") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "downcast-rs") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "macro_rules_attribute") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "1j71h3wd3jmxg44zfbsmqg91z77nn3gh7bkdiidspnba7liiyq9v")))

(define-public crate-event-driven-library-0.1.7 (c (n "event-driven-library") (v "0.1.7") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "downcast-rs") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "macro_rules_attribute") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "1dwya72wb4dnhvjjzk1xr591rhn1il20lgj9gpsg81f8bhr28zv1")))

(define-public crate-event-driven-library-0.1.8 (c (n "event-driven-library") (v "0.1.8") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "downcast-rs") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "macro_rules_attribute") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "1fkcpmazg9rcgaxhs01z1kk7lkgwas7fnsf4a2m17n1dv0mj6zbl")))

(define-public crate-event-driven-library-0.1.9 (c (n "event-driven-library") (v "0.1.9") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "downcast-rs") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "macro_rules_attribute") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "1miggra7njzxg7dpb7j2y9qi2g0zx52bwa512gg7n7cxalm8dzzf")))

(define-public crate-event-driven-library-0.1.10 (c (n "event-driven-library") (v "0.1.10") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "downcast-rs") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "macro_rules_attribute") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "0mn2a2mlk5nkzb2pkfdrmrcw86ms9zg12bignwyx1m46g3wd34zi")))

(define-public crate-event-driven-library-0.1.11 (c (n "event-driven-library") (v "0.1.11") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "downcast-rs") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "macro_rules_attribute") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "1nm1yglzxdx7s82bwngirql0kb33jabikjgx17brhmw2vfrcb8r4")))

(define-public crate-event-driven-library-0.1.12 (c (n "event-driven-library") (v "0.1.12") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "downcast-rs") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "macro_rules_attribute") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4"))) (d #t) (k 0)))) (h "1gddiylzmc3cx7f439adcyhg4hfrm9bmwq68yibra1xjjl193jv2")))

(define-public crate-event-driven-library-0.1.14 (c (n "event-driven-library") (v "0.1.14") (d (list (d (n "event-driven-core") (r "^0.1.0") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0jpgx311nwrch7rvq748jnm0r7cg2c4nk1ishrhp7ix37rrp7l7a")))

(define-public crate-event-driven-library-0.1.18 (c (n "event-driven-library") (v "0.1.18") (d (list (d (n "event-driven-core") (r "^0.1.4") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.4") (d #t) (k 0)))) (h "09bqf49lcyq6vmsc4b0mkqqn7sz4j4vipp0y520pyzwh0hlc34bq")))

(define-public crate-event-driven-library-0.1.20 (c (n "event-driven-library") (v "0.1.20") (d (list (d (n "event-driven-core") (r "^0.1.4") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.4") (d #t) (k 0)))) (h "04qkf38jd5yxd4m5n5ddqicnpzlq3c5lpyf1qvrqyqc7dcan54y6")))

(define-public crate-event-driven-library-0.1.21 (c (n "event-driven-library") (v "0.1.21") (d (list (d (n "event-driven-core") (r "^0.1.8") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.8") (d #t) (k 0)))) (h "0qm72n4614rhmcxamdh4xdqzqb47j1lzxmdl04n49sm563dv2nxk")))

(define-public crate-event-driven-library-0.1.22 (c (n "event-driven-library") (v "0.1.22") (d (list (d (n "event-driven-core") (r "^0.1.9") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.8") (d #t) (k 0)))) (h "1h19m8vjp2km817v801x0ija216a91dsid03dnki53l1x37b12kk")))

(define-public crate-event-driven-library-0.1.23 (c (n "event-driven-library") (v "0.1.23") (d (list (d (n "event-driven-core") (r "^0.1.10") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.10") (d #t) (k 0)))) (h "15j8r3w98znx25dv5iddi1q79jvhssrrx6i8p373bfraj8ayxljk")))

(define-public crate-event-driven-library-0.1.24 (c (n "event-driven-library") (v "0.1.24") (d (list (d (n "event-driven-core") (r "^0.1.10") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.10") (d #t) (k 0)))) (h "0jizkrnkcd2p8dxdyqqy6a679x0f65610cyikj34bkfpfjkvxzpz")))

(define-public crate-event-driven-library-0.1.25 (c (n "event-driven-library") (v "0.1.25") (d (list (d (n "event-driven-core") (r "^0.1.10") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.10") (d #t) (k 0)))) (h "1qi3swr6fr2nr57a4pdx4qyb4agx0qddlr4gc2a1dd8zv0nbic2h")))

(define-public crate-event-driven-library-0.1.26 (c (n "event-driven-library") (v "0.1.26") (d (list (d (n "event-driven-core") (r "^0.1.11") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.10") (d #t) (k 0)))) (h "189gwcpa12iz17hax47ii5ajqrvdiga69ak5sijyy6nnqsydymm0")))

(define-public crate-event-driven-library-0.1.27 (c (n "event-driven-library") (v "0.1.27") (d (list (d (n "event-driven-core") (r "^0.1.12") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.10") (d #t) (k 0)))) (h "0g0p14gj9mx8j12rcgfz345ddgkpgqbbfvi86d7sb1v0cggvjcp6")))

(define-public crate-event-driven-library-0.1.28 (c (n "event-driven-library") (v "0.1.28") (d (list (d (n "event-driven-core") (r "^0.1.13") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.10") (d #t) (k 0)))) (h "1caady8cw2x5hyv244l5m4rfviwxqzdxhlq15r20ir9wjda2dhlb")))

(define-public crate-event-driven-library-0.1.29 (c (n "event-driven-library") (v "0.1.29") (d (list (d (n "event-driven-core") (r "^0.1.13") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.10") (d #t) (k 0)))) (h "06grpqj2c64cmi5b3mzs9jpbybkf9pan5kmilxi9jqa41isg6l43")))

(define-public crate-event-driven-library-0.1.30 (c (n "event-driven-library") (v "0.1.30") (d (list (d (n "event-driven-core") (r "^0.1.13") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.10") (d #t) (k 0)))) (h "1wz8i0566m6fq92bzzi9wj6ly4a0ip9dlalmw7wvn4822knqajiz")))

(define-public crate-event-driven-library-0.1.31 (c (n "event-driven-library") (v "0.1.31") (d (list (d (n "event-driven-core") (r "^0.1.14") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.11") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1asjn84262kp5wqrkxd4np7pf53r49m66iyqs8bjz2yyzc2sbfrx")))

(define-public crate-event-driven-library-0.1.32 (c (n "event-driven-library") (v "0.1.32") (d (list (d (n "event-driven-core") (r "^0.1.14") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.12") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "06i3ww6r2hjb9hahj9k53383viwf3vfyf3d9yz6vqhvg50s53h1h")))

(define-public crate-event-driven-library-0.1.33 (c (n "event-driven-library") (v "0.1.33") (d (list (d (n "event-driven-core") (r "^0.1.14") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.13") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0lsih6qggwwsnwkqwyqbplnrmd2m1gzhfcgywrfrixvazdk76q5a")))

(define-public crate-event-driven-library-0.1.34 (c (n "event-driven-library") (v "0.1.34") (d (list (d (n "event-driven-core") (r "^0.1.14") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.13") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "097gk5x433igdsc1ynkc8379abk12f8g7c7daxlx4m481i8qzwa6")))

(define-public crate-event-driven-library-0.1.36 (c (n "event-driven-library") (v "0.1.36") (d (list (d (n "event-driven-core") (r "^0.1.17") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.13") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1mr3xw35pqcz0af1nvcyy9z7cjxjak11vslz766zak1fnviap06h")))

(define-public crate-event-driven-library-0.1.37 (c (n "event-driven-library") (v "0.1.37") (d (list (d (n "event-driven-core") (r "^0.1.17") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.14") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "16xskzh3kiv50l0imkik5ca8vkwjk4nr41vqfkc54a7i2h0gfr40")))

(define-public crate-event-driven-library-0.1.38 (c (n "event-driven-library") (v "0.1.38") (d (list (d (n "event-driven-core") (r "^0.1.38") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.38") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "025jvqbx4frxr867p5w50csgc1j56x1xmsjk0sgy1schs2jbsnwn")))

(define-public crate-event-driven-library-0.1.39 (c (n "event-driven-library") (v "0.1.39") (d (list (d (n "event-driven-core") (r "^0.1.39") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.39") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0hz9fnq9mxizdjri76k0dcw5r8kbn29aw5q6p8zgn2ky2x04f1ax")))

(define-public crate-event-driven-library-0.1.40 (c (n "event-driven-library") (v "0.1.40") (d (list (d (n "event-driven-core") (r "^0.1.40") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.40") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "03a0cvyarjnl7kqgiij28k2i6mwgahnvhf4pr4iqsjpvvwr2lqds")))

(define-public crate-event-driven-library-0.1.41 (c (n "event-driven-library") (v "0.1.41") (d (list (d (n "event-driven-core") (r "^0.1.41") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.41") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "06mlgd1zzqa4jv8smiq5scbydqxq71c2n7j3hw1ywx1qm71v5145")))

(define-public crate-event-driven-library-0.1.42 (c (n "event-driven-library") (v "0.1.42") (d (list (d (n "event-driven-core") (r "^0.1.42") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.42") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0057n8xhdc551pzy7cmzjgvwmr56rjjfnl7ybcjahk69iy9x7w81")))

(define-public crate-event-driven-library-0.1.43 (c (n "event-driven-library") (v "0.1.43") (d (list (d (n "event-driven-core") (r "^0.1.43") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.43") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0by4g4nwm1qb5vvqz8yq37alwbv272fay2xfd7hg7r0r47rkrkpq")))

(define-public crate-event-driven-library-0.1.44 (c (n "event-driven-library") (v "0.1.44") (d (list (d (n "event-driven-core") (r "^0.1.44") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.44") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1hc8ws785ll96il40yj77hkcnsxqqxbbqvp5q524w2w9hy2nnrav")))

(define-public crate-event-driven-library-0.1.45 (c (n "event-driven-library") (v "0.1.45") (d (list (d (n "event-driven-core") (r "^0.1.45") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.45") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "19040n2h9hcszkj77zsl26sg1jq32il72k4wgkn42m3wdkaj9apr")))

(define-public crate-event-driven-library-0.1.46 (c (n "event-driven-library") (v "0.1.46") (d (list (d (n "event-driven-core") (r "^0.1.46") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.46") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1lvfa1j5wys02gxk74caksyd2dx66fpdfr11w1cdcrh7822nv8nr")))

(define-public crate-event-driven-library-0.1.47 (c (n "event-driven-library") (v "0.1.47") (d (list (d (n "event-driven-core") (r "^0.1.47") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.47") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1mhnf587gqkfmm7djyj4n2sj60ag96rhy74qy506rg7rx5gjnqds")))

(define-public crate-event-driven-library-0.1.48 (c (n "event-driven-library") (v "0.1.48") (d (list (d (n "event-driven-core") (r "^0.1.48") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.48") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "06h3acq2l3d8q2nas0jrina39g0pm7r35pvpl2652y1dlqjx6q0d")))

(define-public crate-event-driven-library-0.1.49 (c (n "event-driven-library") (v "0.1.49") (d (list (d (n "event-driven-core") (r "^0.1.49") (d #t) (k 0)) (d (n "event-driven-macro") (r "^0.1.49") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "07qppzpv16yqmi3jrk06cpi69r9nvgmihlff63sgkg0dwxbwd45b")))

