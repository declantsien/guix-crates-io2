(define-module (crates-io ev en events_emitter) #:use-module (crates-io))

(define-public crate-events_emitter-0.1.0 (c (n "events_emitter") (v "0.1.0") (h "15w8blv4rrd75yjn9vvci8q2f929465idc5ywpm4s3gkzr8dnxvz")))

(define-public crate-events_emitter-0.1.1 (c (n "events_emitter") (v "0.1.1") (h "1f27l8i9h5paw402hc1cj0hdz4l7zv3ailmh1nl4p3ymbigl3vi1")))

(define-public crate-events_emitter-0.1.2 (c (n "events_emitter") (v "0.1.2") (h "15w1bphdfc13ys580j1pzarj0d12xcmzv2mad29jfqjxnzapc7l7")))

(define-public crate-events_emitter-0.1.3 (c (n "events_emitter") (v "0.1.3") (h "0sj83j5m3fsk2d82z6dc9sp5ligzy840cdiw0g7rsywja8kq5shr")))

