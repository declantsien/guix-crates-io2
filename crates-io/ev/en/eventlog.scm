(define-module (crates-io ev en eventlog) #:use-module (crates-io))

(define-public crate-eventlog-0.0.0 (c (n "eventlog") (v "0.0.0") (h "0j5fwz901r6imbiw04xzjplbwjbvsb8phmrk48k24j0bmv4qkirz")))

(define-public crate-eventlog-0.1.0 (c (n "eventlog") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "regex") (r "^1.3.9") (f (quote ("std" "unicode-perl"))) (k 1)) (d (n "registry") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)))) (h "1j9hx96dlrm0gymngb60rpk6xd7kflvq8y47xl6v29yn5hlzskcw")))

(define-public crate-eventlog-0.1.1 (c (n "eventlog") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "regex") (r "^1.3.9") (f (quote ("std" "unicode-perl"))) (k 1)) (d (n "registry") (r "^1.0.0-alpha.1") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase"))) (d #t) (k 0)))) (h "1blynqkp22slvadxfxmgh6sj4xvx479rqjc3pb354yx3qdzg8rfv")))

(define-public crate-eventlog-0.2.0 (c (n "eventlog") (v "0.2.0") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "registry") (r "^1.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winbase"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.7.1") (f (quote ("std" "unicode-perl"))) (k 1)) (d (n "sha2") (r "^0.10.6") (d #t) (k 1)))) (h "0dzncfadkhx3pmdp5lbyzzi44giyljfxszr12jjqks1k2l2a4vjc")))

(define-public crate-eventlog-0.2.1 (c (n "eventlog") (v "0.2.1") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "registry") (r "^1.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winbase"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.7.1") (f (quote ("std" "unicode-perl"))) (k 1)) (d (n "sha2") (r "^0.10.6") (d #t) (k 1)))) (h "0d950s8gcfb0p1xnz2n111vgngd7lwq31y5941g0n05zv1g758l4")))

(define-public crate-eventlog-0.2.2 (c (n "eventlog") (v "0.2.2") (d (list (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "registry") (r "^1.2.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winbase"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.7.1") (f (quote ("std" "unicode-perl"))) (k 1)) (d (n "sha2") (r "^0.10.6") (d #t) (k 1)))) (h "1xx346wnswy50g8jc2n0s1fma25jn44rimrlwx4ih5db4f1ba0yl")))

