(define-module (crates-io ev en event-manager) #:use-module (crates-io))

(define-public crate-event-manager-0.1.0 (c (n "event-manager") (v "0.1.0") (d (list (d (n "criterion") (r "=0.3.0") (d #t) (k 2)) (d (n "libc") (r ">=0.2.39") (d #t) (k 0)) (d (n "vmm-sys-util") (r ">=0.6.0") (d #t) (k 0)))) (h "1fsxd0jf4i35hwy4anf1z1lgfmiq75a03yj3mz72f04fmhgijldb") (f (quote (("test_utilities") ("remote_endpoint")))) (y #t)))

(define-public crate-event-manager-0.2.0 (c (n "event-manager") (v "0.2.0") (d (list (d (n "criterion") (r "=0.3.0") (d #t) (k 2)) (d (n "libc") (r ">=0.2.39") (d #t) (k 0)) (d (n "vmm-sys-util") (r ">=0.6.0") (d #t) (k 0)))) (h "0akyl79j1ih2x43p10sz06h87sz8h38rb50x66zaplvlzpk3f633") (f (quote (("test_utilities") ("remote_endpoint"))))))

(define-public crate-event-manager-0.2.1 (c (n "event-manager") (v "0.2.1") (d (list (d (n "criterion") (r "=0.3.0") (d #t) (k 2)) (d (n "libc") (r ">=0.2.39") (d #t) (k 0)) (d (n "vmm-sys-util") (r ">=0.8.0") (d #t) (k 0)))) (h "0h76rcls408vr0ygmk93b1ybz0aldmd6bqhqd8wj7gjz2f8sazrp") (f (quote (("test_utilities") ("remote_endpoint"))))))

(define-public crate-event-manager-0.3.0 (c (n "event-manager") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "vmm-sys-util") (r "^0.11.0") (d #t) (k 0)))) (h "0pfpzrlfq4q7ygaqj9vpgcyzrd7by49zn2pf9mjmi94m1irmzfrb") (f (quote (("test_utilities") ("remote_endpoint"))))))

(define-public crate-event-manager-0.4.0 (c (n "event-manager") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "vmm-sys-util") (r "^0.12.1") (d #t) (k 0)))) (h "0ax4j9vg7k1f68g3sr60wbsjwhgj0isrzknfqz4n048s2vjnzcch") (f (quote (("test_utilities") ("remote_endpoint"))))))

