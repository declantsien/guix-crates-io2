(define-module (crates-io ev en event-stream-processor) #:use-module (crates-io))

(define-public crate-event-stream-processor-1.0.0 (c (n "event-stream-processor") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.12") (d #t) (k 0)) (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)))) (h "1ldm14zxb88q0sfagfcspp2xbpzf1j00v37p3h714dw72b22i2kf")))

