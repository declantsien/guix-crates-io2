(define-module (crates-io ev en event_dispatcher) #:use-module (crates-io))

(define-public crate-event_dispatcher-0.0.1 (c (n "event_dispatcher") (v "0.0.1") (h "0fanm0p7aa444qj1y1l7pqin1wbd37w0kvfgxfpwag5iqg6ysmsh")))

(define-public crate-event_dispatcher-0.0.2 (c (n "event_dispatcher") (v "0.0.2") (h "1szddj7gz820hy155zh2zjplv4czr18711bisz9bplb2sk4ya2iw")))

