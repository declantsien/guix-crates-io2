(define-module (crates-io ev en event-engine) #:use-module (crates-io))

(define-public crate-event-engine-0.1.0 (c (n "event-engine") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "0j0l48aw7l110bhby4qqhr8253db6wd9qcsz6vf8qxkbgn256xdz")))

(define-public crate-event-engine-0.1.1 (c (n "event-engine") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "1xmcma278rpn8amkrpngriwpkjh72nq91l74mqv82dxvrlb2xj1k")))

(define-public crate-event-engine-0.1.2 (c (n "event-engine") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "192iwnww6r8rw6jksz8gfbh3i05i5m84nhbbqf3k8r1fv5mk6mph")))

(define-public crate-event-engine-0.1.3 (c (n "event-engine") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "1d9xjkfcdmn7hg6ygi2fdlhbirda8aps1n6v9znb0sx3sqklmcq1")))

(define-public crate-event-engine-0.2.0 (c (n "event-engine") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4"))) (d #t) (k 0)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "1q9h37rx2gh3kg5jx56ril0c3x4zcs61cx3yydc22nkwzk1klssm")))

