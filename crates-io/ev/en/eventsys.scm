(define-module (crates-io ev en eventsys) #:use-module (crates-io))

(define-public crate-eventsys-0.1.0 (c (n "eventsys") (v "0.1.0") (d (list (d (n "anythingy") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "17aj32rkj7vzqx0j5ka8qk8yn65k0rb8dphh1iqimvrlika35r8q")))

(define-public crate-eventsys-0.1.1 (c (n "eventsys") (v "0.1.1") (d (list (d (n "anythingy") (r "^0.1.2") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "005y06nxjl1ci1lh81dd70g58q6naw56xspqzawpd9sd66b2m84f")))

