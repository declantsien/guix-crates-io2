(define-module (crates-io ev en eventmill_derive) #:use-module (crates-io))

(define-public crate-eventmill_derive-0.1.0 (c (n "eventmill_derive") (v "0.1.0") (d (list (d (n "eventmill") (r "^0.1.0") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0f2g51h466qxf1jhdyfjwrk21wy5h5pczhx0l1zfcr1mz7bpxq1g")))

(define-public crate-eventmill_derive-0.2.0 (c (n "eventmill_derive") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0mim6ganmkx79a00zfm8wzl9nzjblr0isdw53p5cysf2dslpp9xh")))

(define-public crate-eventmill_derive-0.3.0 (c (n "eventmill_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0jkp9rmq33wylj020674jb7k52slg5xjsm3kdjfqyvyfp6bb4lbk")))

