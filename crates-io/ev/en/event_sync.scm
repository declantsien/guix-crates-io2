(define-module (crates-io ev en event_sync) #:use-module (crates-io))

(define-public crate-event_sync-0.1.0 (c (n "event_sync") (v "0.1.0") (h "00dqjyj55q7vbsspgvrdg50rnz8jarln87vxb01dipjj6ga4p6l3")))

(define-public crate-event_sync-0.1.1 (c (n "event_sync") (v "0.1.1") (h "0a1i2qip4adqqdnbjyv2085n5a9p44qj8bgxdrkm4g8nv6bii64g")))

(define-public crate-event_sync-0.1.2 (c (n "event_sync") (v "0.1.2") (h "0h17ci8q0d4m9zfiaqwipsjkv7vv57hqc6qbj2j8c6x46niszsw5")))

(define-public crate-event_sync-0.1.3 (c (n "event_sync") (v "0.1.3") (h "09j3b4qlygpl10mj0bvjn7v5f2rv47ih30akmq7ibkn3sq2cvfa0")))

(define-public crate-event_sync-0.1.4 (c (n "event_sync") (v "0.1.4") (d (list (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1s9ywhpf351wf17wjdnhv2dyyy0fxqqixya51mpa5ch98kmgppi8")))

(define-public crate-event_sync-0.1.6 (c (n "event_sync") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1apvbkarxpk69pa62daar2gv9iwpixax5lifs2b9qdg0rmp8a9bj")))

(define-public crate-event_sync-0.1.7 (c (n "event_sync") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0i6g68v0862yhgbpaqhxv8s86zm8mqcfx20741j4jjcyf0ic21bk")))

(define-public crate-event_sync-0.2.0 (c (n "event_sync") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "serde") (r "1.0.*") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "066828pr0y2q34si8jwv7gllgsvw4xrjnlzrjy3zzpz47s64n011")))

(define-public crate-event_sync-0.2.1 (c (n "event_sync") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "serde") (r "1.0.*") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1qwv7vsglk87v8wv9rgbpdlchh8mxi7vpr57j4436vikb38ln4zi")))

(define-public crate-event_sync-0.3.0 (c (n "event_sync") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "serde") (r "1.0.*") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "038v3cl9h1is752c7l47hixcagb451sd9bh912bq52b5qndyhgnd")))

(define-public crate-event_sync-0.4.0 (c (n "event_sync") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "serde") (r "1.0.*") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1p4a851ym3j8n112c00zfz2kfk4c6zrp7rh3x3gymdm47gnyxaf6")))

(define-public crate-event_sync-0.4.1 (c (n "event_sync") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "serde") (r "1.0.*") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "10ghngddkgjw4sbbmsz8y7vc1zdg24wl6rd4yq3wm1v4fjrqw2z3")))

(define-public crate-event_sync-0.4.2 (c (n "event_sync") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "serde") (r "1.0.*") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0cgfpvkxhxj8530gy9aqpadcwg1wd25xy7s5ha56fj724n8d4r9f")))

(define-public crate-event_sync-0.4.3 (c (n "event_sync") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "serde") (r "1.0.*") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1r9k3bip0adl8jcda90v0j71944a0mqm7z11b730n98ld9vcckpn")))

(define-public crate-event_sync-0.4.4 (c (n "event_sync") (v "0.4.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "serde") (r "1.0.*") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0c9lsman7gj66zqc841s8ri1z4lqxiywz65zp09jbrrjd9s1ikps")))

