(define-module (crates-io ev en eventor) #:use-module (crates-io))

(define-public crate-eventor-0.7.0 (c (n "eventor") (v "0.7.0") (d (list (d (n "elicit") (r "^0.8") (k 0)) (d (n "hash_combine") (r "^0.3") (k 0)) (d (n "libc") (r "^0.2") (k 0)) (d (n "log") (r "^0.4") (k 0)))) (h "1yvvfdk5hyqaaa99nddzgz6bqqhqbq4srskj9xby2ypg4vc18y6k") (y #t)))

