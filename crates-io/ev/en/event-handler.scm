(define-module (crates-io ev en event-handler) #:use-module (crates-io))

(define-public crate-event-handler-0.1.0 (c (n "event-handler") (v "0.1.0") (h "1cg3l95wfgkhnvglppcyhh8fqvhnln2mvc4ysmi207aw8h13qaba") (r "1.68.0")))

(define-public crate-event-handler-0.1.1 (c (n "event-handler") (v "0.1.1") (h "0k8b9rcrps572vcrvm7szdfsjdsz5q4skgwlbcj8llix9mbh2z7l") (r "1.68.0")))

