(define-module (crates-io ev en eventdb) #:use-module (crates-io))

(define-public crate-eventdb-0.1.0 (c (n "eventdb") (v "0.1.0") (h "18divadfl9a2xq92xivg4y0d416mjg9n3mx55bcff6pxhmwqj7rm") (y #t)))

(define-public crate-eventdb-0.1.0-alpha (c (n "eventdb") (v "0.1.0-alpha") (h "1n80zh1iqk87wc7chvg2gc0wf385zfcpbi7lrz0zpy8j6p7zl8d5") (y #t)))

(define-public crate-eventdb-0.1.1-alpha (c (n "eventdb") (v "0.1.1-alpha") (d (list (d (n "clap") (r "^4.5.1") (d #t) (k 0)) (d (n "rocksdb") (r "^0.22.0") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (d #t) (k 0)))) (h "15d13kq8nj4q884bqz0r1154x06xa7sz6j1gmx2hmfangzlkghx2") (y #t)))

(define-public crate-eventdb-0.1.2-alpha (c (n "eventdb") (v "0.1.2-alpha") (d (list (d (n "clap") (r "^4.5.1") (d #t) (k 0)) (d (n "rocksdb") (r "^0.22.0") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (d #t) (k 0)))) (h "16j1wp1yx11bm18gbgzm1qrahvjg7qykb7ckgr1lrmawzb2bbywi") (y #t)))

(define-public crate-eventdb-0.1.3-alpha (c (n "eventdb") (v "0.1.3-alpha") (d (list (d (n "clap") (r "^4.5.1") (d #t) (k 0)) (d (n "rocksdb") (r "^0.22.0") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (d #t) (k 0)))) (h "1nya4dimjicsnd12gchc8a39jffv8c9ysqnsbd2ww42y1iyilhmw") (y #t)))

(define-public crate-eventdb-0.1.4-alpha (c (n "eventdb") (v "0.1.4-alpha") (d (list (d (n "clap") (r "^4.5.1") (d #t) (k 0)) (d (n "rocksdb") (r "^0.22.0") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (d #t) (k 0)))) (h "00fsvyj0j13mvgrzpydx5bn9li1vws65r8b6a8gc2c3j0nilha5j") (y #t)))

