(define-module (crates-io ev en eventemitter) #:use-module (crates-io))

(define-public crate-EventEmitter-0.0.1 (c (n "EventEmitter") (v "0.0.1") (h "154lcs7h3bz8xpd9qkgll9bfvca7fr82593nhw32nq1ijakrripi") (f (quote (("nightly") ("default"))))))

(define-public crate-EventEmitter-0.0.2 (c (n "EventEmitter") (v "0.0.2") (h "09411mlrsk1g5ffajbi0sbi3336wrxjay2dj4aay49i85pciy99v") (f (quote (("nightly") ("default"))))))

(define-public crate-EventEmitter-0.0.3 (c (n "EventEmitter") (v "0.0.3") (h "1d102rx3xal2sb1w70r82pr46wj1b5zx8p4y7v58mgg99hvzj4h4") (f (quote (("nightly") ("default"))))))

(define-public crate-EventEmitter-0.0.4 (c (n "EventEmitter") (v "0.0.4") (h "0nxpyz22ff6kr1rhj2nsc87l61f3pjk7z04wf0ni5yb8z73mwds3") (f (quote (("nightly") ("default"))))))

