(define-module (crates-io ev en eventsource) #:use-module (crates-io))

(define-public crate-eventsource-0.1.0 (c (n "eventsource") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)))) (h "18037v9yam4fjyhqrlc5zx619kyj7k1w10qgfnja26ljb9cx3lwm")))

(define-public crate-eventsource-0.2.0 (c (n "eventsource") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.8.1") (d #t) (k 0)) (d (n "mime") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "1n9yp3105lcgvssiq3nsvv8nvagam13xbg5hmj28vb6hh591xdqx") (f (quote (("with-reqwest" "reqwest" "mime") ("default" "with-reqwest"))))))

(define-public crate-eventsource-0.3.0 (c (n "eventsource") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.0") (o #t) (d #t) (k 0)))) (h "0d07m3f4pb1hkclm7vxg1p581iawp247hipk7xxwik4qysbhsidm") (f (quote (("with-reqwest" "reqwest") ("default" "with-reqwest"))))))

(define-public crate-eventsource-0.4.0 (c (n "eventsource") (v "0.4.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "mime") (r "^0.3.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.2") (o #t) (d #t) (k 0)))) (h "1mq7m0lg83d4d8mgzainn4wr2lfrckd6zzfqggb2yafk9gh9wq4m") (f (quote (("with-reqwest" "reqwest") ("default" "with-reqwest"))))))

(define-public crate-eventsource-0.5.0 (c (n "eventsource") (v "0.5.0") (d (list (d (n "error-chain") (r "^0.12.2") (d #t) (k 0)) (d (n "mime") (r "^0.3.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking"))) (o #t) (d #t) (k 0)))) (h "1vlyzmcsx180331g9zc7qw7q4r52fl7bq7vm4z2qk7mg1x869jdj") (f (quote (("with-reqwest" "reqwest") ("default" "with-reqwest"))))))

