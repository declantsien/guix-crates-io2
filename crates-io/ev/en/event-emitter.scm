(define-module (crates-io ev en event-emitter) #:use-module (crates-io))

(define-public crate-event-emitter-0.0.1 (c (n "event-emitter") (v "0.0.1") (d (list (d (n "event") (r "*") (d #t) (k 0)) (d (n "forever") (r "*") (d #t) (k 0)) (d (n "typemap") (r "*") (d #t) (k 0)) (d (n "unsafe-any") (r "*") (d #t) (k 0)) (d (n "uuid") (r "*") (d #t) (k 0)))) (h "1i7lliarz7n6d4ap6aiwxm32ysf1i5gp9vh3rdx888a6b0j38ljx")))

