(define-module (crates-io ev en event_system) #:use-module (crates-io))

(define-public crate-event_system-0.1.0 (c (n "event_system") (v "0.1.0") (d (list (d (n "event_system_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1yicsg0x386sdv9ngh34b1bb9blnxsa756xbzk98hrzw9lffk0kl")))

(define-public crate-event_system-0.1.1 (c (n "event_system") (v "0.1.1") (d (list (d (n "event_system_macros") (r "^0.1") (d #t) (k 0)))) (h "0l6vzklzsmnq2bvrlkdyql37z9g7qknhvbjgl5mcdqdrmwlvwv3l")))

