(define-module (crates-io ev ol evolution_rs) #:use-module (crates-io))

(define-public crate-evolution_rs-0.1.0 (c (n "evolution_rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1gcwlsa048xh9fn0qzjj4xrgdxfz6c5a8sxv4l2ldfbbn7w5f242") (y #t)))

(define-public crate-evolution_rs-0.2.0 (c (n "evolution_rs") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1sp6fjq25c8y90nfafarwji8f692qr45rpjq39ivpc0sf3agppif") (y #t)))

