(define-module (crates-io ev ol evolutionary) #:use-module (crates-io))

(define-public crate-evolutionary-0.1.0 (c (n "evolutionary") (v "0.1.0") (d (list (d (n "dyn-clone") (r "^1.0.13") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.183") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0mxjkskf26z0wmsfplvbpnflkjj4177xdp3lrn6aq93wv4y3lwyn")))

(define-public crate-evolutionary-0.1.1 (c (n "evolutionary") (v "0.1.1") (d (list (d (n "dyn-clone") (r "^1.0.13") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.183") (d #t) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)))) (h "0rbi4swcdb6s1wspcic1mvvdq7hpgsxqwfnn2c2yiv0zd84lf599")))

