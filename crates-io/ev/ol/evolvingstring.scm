(define-module (crates-io ev ol evolvingstring) #:use-module (crates-io))

(define-public crate-evolvingstring-0.1.0 (c (n "evolvingstring") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "16fdci4i49lasm0q7s8gpxc7cvq49bmhajzbq1zxcmcy8wcglsh6")))

