(define-module (crates-io ev a- eva-sdk-derive) #:use-module (crates-io))

(define-public crate-eva-sdk-derive-0.1.0 (c (n "eva-sdk-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full"))) (d #t) (k 0)))) (h "1966gncbyal7z6ric22brabi7w9p42vzl7nbx4d0j4g241fg8cdj")))

(define-public crate-eva-sdk-derive-0.1.1 (c (n "eva-sdk-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full"))) (d #t) (k 0)))) (h "00fnfkdi30d18am9k02fxr5wrjavcfrkw1yg53005m78rp4l39cs")))

(define-public crate-eva-sdk-derive-0.1.2 (c (n "eva-sdk-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full"))) (d #t) (k 0)))) (h "09548psl29sdy4xs2dx2k532pzjb0s5lqm06j8j2y43l9gzp2pm2")))

