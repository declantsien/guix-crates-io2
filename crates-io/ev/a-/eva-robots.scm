(define-module (crates-io ev a- eva-robots) #:use-module (crates-io))

(define-public crate-eva-robots-0.1.0 (c (n "eva-robots") (v "0.1.0") (d (list (d (n "eva-common") (r "^0.1.166") (f (quote ("actions"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1v18r86hg7g7ra6yfgamq9ldl5aq2bkgmkpysygpxlyjvfdp011c")))

(define-public crate-eva-robots-0.1.1 (c (n "eva-robots") (v "0.1.1") (d (list (d (n "eva-common") (r "^0.1.166") (f (quote ("actions"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1w5fm3fnbasjhxyn1h3xy985dn6kx8znlx4yiymz5l42a0gxs965")))

(define-public crate-eva-robots-0.1.2 (c (n "eva-robots") (v "0.1.2") (d (list (d (n "eva-common") (r "^0.1.166") (f (quote ("actions"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0akg1hx9mkwch88d61v8254k9qs85203draa9946kyva8dzylbxh")))

(define-public crate-eva-robots-0.1.3 (c (n "eva-robots") (v "0.1.3") (d (list (d (n "eva-common") (r "^0.1.166") (f (quote ("actions"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1ly3k21cjcv96dg2qjmk9i7l54vnv4x3cpfzmi7gfj5nc12z9vvp")))

(define-public crate-eva-robots-0.1.4 (c (n "eva-robots") (v "0.1.4") (d (list (d (n "eva-common") (r "^0.1.166") (f (quote ("actions"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1hc5ixgw3as1mfzl2ip8czb2ji74xs4ww4hq05728q0k1vdaajqm")))

(define-public crate-eva-robots-0.1.5 (c (n "eva-robots") (v "0.1.5") (d (list (d (n "eva-common") (r "^0.1.166") (f (quote ("actions"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0yzvmzpzd60fjk6swwimfdnhhd8nnqc8j2wdfhkqn8znqpchsvgi")))

(define-public crate-eva-robots-0.1.6 (c (n "eva-robots") (v "0.1.6") (d (list (d (n "eva-common") (r "^0.1.166") (f (quote ("actions"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1vpvsg5dxhdqxqiacggl4xrcn6j81acqaf7c029npvnq76wp05m6")))

(define-public crate-eva-robots-0.1.7 (c (n "eva-robots") (v "0.1.7") (d (list (d (n "eva-common") (r "^0.1.166") (f (quote ("actions"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "05fyd1q3l1lp7ispg18f57ic0llr6dfazcvv1hzlpxrn6czir8hg")))

(define-public crate-eva-robots-0.2.0 (c (n "eva-robots") (v "0.2.0") (d (list (d (n "eva-common") (r "^0.2.1") (f (quote ("actions"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1x8w9y4i58z7ys2wlc1f75pb6d9jk7ki8qks6z7fdcpjgj30k93k")))

(define-public crate-eva-robots-0.3.0 (c (n "eva-robots") (v "0.3.0") (d (list (d (n "eva-common") (r "^0.3.0") (f (quote ("actions"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1cv868k6j0zaq8vbfriij83falp6q88jh8bqy8wjdmdb8gd595gm")))

