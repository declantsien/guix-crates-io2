(define-module (crates-io ev a- eva-sim-modbus) #:use-module (crates-io))

(define-public crate-eva-sim-modbus-0.1.0 (c (n "eva-sim-modbus") (v "0.1.0") (d (list (d (n "busrt") (r "^0.4.6") (f (quote ("rpc"))) (d #t) (k 0)) (d (n "eva-common") (r "^0.3.1") (f (quote ("bus-rpc"))) (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "rmodbus") (r "^0.7.3") (d #t) (k 0)) (d (n "uuid") (r "^1.4.0") (d #t) (k 0)))) (h "10xw7wfdgg9bcdh8mpa8p3d108iy1ixij0pidi7kji35qw07mc2n")))

