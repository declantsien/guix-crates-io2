(define-module (crates-io ev il evilcrate) #:use-module (crates-io))

(define-public crate-evilcrate-0.1.0 (c (n "evilcrate") (v "0.1.0") (h "0v73mcqcnv0lsgxw9ba593sq3dsi867r5akn98raxkcb970c31j0")))

(define-public crate-evilcrate-0.1.1 (c (n "evilcrate") (v "0.1.1") (h "0j6hn869s6bkqa3198wjr55qxggxlrj05gmflvqkn7yxd0jrm869")))

(define-public crate-evilcrate-0.1.2 (c (n "evilcrate") (v "0.1.2") (h "1nh24lrps0n8p737v5nn7cwzn2kmfzlb4hd9ykn3128rxn7j4pnl")))

