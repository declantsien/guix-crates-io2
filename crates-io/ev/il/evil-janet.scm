(define-module (crates-io ev il evil-janet) #:use-module (crates-io))

(define-public crate-evil-janet-1.11.0 (c (n "evil-janet") (v "1.11.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "11ffslpnjl6lxhcwnfmvziq14w7pzv1pbajslxnnlpsns5ryc60i") (f (quote (("link-amalg") ("default"))))))

(define-public crate-evil-janet-1.11.3 (c (n "evil-janet") (v "1.11.3") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0wwa47ha0h1bf1x09rplbhm56974wiv5prx1mmck7j2vfx7kmrap") (f (quote (("link-amalg") ("default"))))))

(define-public crate-evil-janet-1.11.4 (c (n "evil-janet") (v "1.11.4") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1jv9ifz68596pl19l8p6x5jvdf12yx5mh0xkxli1cbga9hjmr64f") (f (quote (("system") ("link-amalg") ("default"))))))

(define-public crate-evil-janet-1.11.5 (c (n "evil-janet") (v "1.11.5") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "065cdmr9s5snifplwsfi1fa651glsd0f7wljs6nnpgkjci2g4gjn") (f (quote (("system") ("link-system") ("link-amalg") ("default"))))))

(define-public crate-evil-janet-1.12.1 (c (n "evil-janet") (v "1.12.1") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0jaqsv6g1figfmy112wn0305d89sb7rcc37jm5v5pkgdcvahkzns") (f (quote (("system") ("link-system") ("link-amalg") ("default"))))))

(define-public crate-evil-janet-1.12.2 (c (n "evil-janet") (v "1.12.2") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0gx27lf6d7c17bl9lr4ac60ibpyca9anl13c5mycz0i3b2zingl1") (f (quote (("system") ("link-system") ("link-amalg") ("default"))))))

(define-public crate-evil-janet-1.12.3 (c (n "evil-janet") (v "1.12.3") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1jww3jmakf6i71487hn3862hvyggz2ygxwn3ji2xi50jw3nisi1f") (f (quote (("system") ("link-system") ("link-amalg") ("default")))) (y #t)))

(define-public crate-evil-janet-1.12.4 (c (n "evil-janet") (v "1.12.4") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0fsr7z7x8iizjy09cxsqpm15bqckr4ykxa8gsmfx7jsjp3r67fvs") (f (quote (("system") ("link-system") ("link-amalg") ("default"))))))

(define-public crate-evil-janet-1.13.1 (c (n "evil-janet") (v "1.13.1") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0j833b5p05k4r5rb9ikjkabfpxhbazwwzhzycdl3c5yahr1zd64v") (f (quote (("system") ("link-system") ("link-amalg") ("default"))))))

(define-public crate-evil-janet-1.14.1 (c (n "evil-janet") (v "1.14.1") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "024l6sgi1g22hrpsvp2aqkqyivwdkm2ymryvrninzz58lllbsp1i") (f (quote (("system") ("link-system") ("link-amalg") ("default"))))))

(define-public crate-evil-janet-1.15.2 (c (n "evil-janet") (v "1.15.2") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1sbsn100xz4knzqkz8adsdv62hj2yhn9idy61gzmqlhxp83cxri0") (f (quote (("system") ("link-system") ("link-amalg") ("default"))))))

(define-public crate-evil-janet-1.15.3 (c (n "evil-janet") (v "1.15.3") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1nkysk8nilxkn1sf0nj0318hib0747d9fbyjravagl89z10wcskn") (f (quote (("system") ("link-system") ("link-amalg") ("default"))))))

(define-public crate-evil-janet-1.15.4 (c (n "evil-janet") (v "1.15.4") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "05vxsxjl5l22ll1xygm656knn3gn7fkb4752nbkgmgbfcb3y7dci") (f (quote (("system") ("link-system") ("link-amalg") ("default"))))))

(define-public crate-evil-janet-1.15.5 (c (n "evil-janet") (v "1.15.5") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1ss2xj0jryidpb2gfq82rmzn7iga09iwxgnhrlfjpj6jp9d81zhs") (f (quote (("system") ("link-system") ("link-amalg") ("default"))))))

(define-public crate-evil-janet-1.16.0 (c (n "evil-janet") (v "1.16.0") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wqxiq6y46f48wnzx4v0dagfa47qwwgyk7hwf90yzdq0xfi19k6a") (f (quote (("system") ("link-system") ("link-amalg") ("default") ("debug-symbols"))))))

(define-public crate-evil-janet-1.16.1 (c (n "evil-janet") (v "1.16.1") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wc64ffdlxphbpb1c8r219mqwwz4v17qkxlj5dr6spivyd3vyqbb") (f (quote (("system") ("link-system") ("link-amalg") ("default") ("debug-symbols"))))))

(define-public crate-evil-janet-1.16.2 (c (n "evil-janet") (v "1.16.2") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0c52bijgni963d7s2zh8xr5fkd0i06aqj5wrxj1i2rngrsfl0a6n") (f (quote (("system") ("link-system") ("link-amalg") ("default") ("debug-symbols"))))))

(define-public crate-evil-janet-1.17.0 (c (n "evil-janet") (v "1.17.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1k7qppma19qa85brgnwj513y82qi7ds0b6k23jkj11bq2b2vkr9r") (f (quote (("system") ("link-system") ("link-amalg") ("default") ("debug-symbols"))))))

(define-public crate-evil-janet-1.17.1 (c (n "evil-janet") (v "1.17.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vlbrahva4v4bgd5qbnvd2nagyixpcqrg8i2gd6qykhkg753s708") (f (quote (("system") ("link-system") ("link-amalg") ("default") ("debug-symbols"))))))

(define-public crate-evil-janet-1.17.2 (c (n "evil-janet") (v "1.17.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05yykbw17fzdnkal1ff8bwjgamhh7q1l2b8045kllr9wxzlhsmfd") (f (quote (("system") ("link-system") ("link-amalg") ("default") ("debug-symbols"))))))

(define-public crate-evil-janet-1.18.0 (c (n "evil-janet") (v "1.18.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0nr8ggmvvsp2dnhkwzrpsgdzzb8dkc2kz410v1yflz3xk0a9i1z7") (f (quote (("system") ("link-system") ("link-amalg") ("default") ("debug-symbols"))))))

(define-public crate-evil-janet-1.18.1 (c (n "evil-janet") (v "1.18.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zywvhq6xijzdd5irsg7qilgqdjyw399gc21lcysnqirpd6xip3w") (f (quote (("system") ("link-system") ("link-amalg") ("default") ("debug-symbols"))))))

(define-public crate-evil-janet-1.19.0 (c (n "evil-janet") (v "1.19.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "02mn7rv8cbabqv1vhv3l12l6gr8hmm9w7426zwn64d2ii8ipbnpf") (f (quote (("system") ("link-system") ("link-amalg") ("default") ("debug-symbols"))))))

(define-public crate-evil-janet-1.19.2 (c (n "evil-janet") (v "1.19.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pzi6p49sbfbz7kcf8cpklwgcpk4r08rmi39inp7iks2wway7rwg") (f (quote (("system") ("link-system") ("link-amalg") ("default") ("debug-symbols"))))))

(define-public crate-evil-janet-1.20.0 (c (n "evil-janet") (v "1.20.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1m13spwpsgckm7dv3p30vkrggn0z86j6qwbfi4wpdzdvfvjlkac2") (f (quote (("system") ("link-system") ("link-amalg") ("default") ("debug-symbols"))))))

(define-public crate-evil-janet-1.21.1 (c (n "evil-janet") (v "1.21.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ygybx7xsp74pn5hi6gaabmgmjbjwb2r3mig945camzbvm40wl99") (f (quote (("system") ("link-system") ("link-amalg") ("default") ("debug-symbols"))))))

(define-public crate-evil-janet-1.22.0 (c (n "evil-janet") (v "1.22.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jn2gig45yw865qm5nz5v80d7dr01j21a78if8pcmmch2kl59h6z") (f (quote (("system") ("link-system") ("link-amalg") ("default") ("debug-symbols"))))))

(define-public crate-evil-janet-1.23.0 (c (n "evil-janet") (v "1.23.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0510kw78mn9j2g53kz2d0rlgrp33vs7y4rpv4hp3cd4rj416jkkx") (f (quote (("system") ("link-system") ("link-amalg") ("default") ("debug-symbols"))))))

(define-public crate-evil-janet-1.24.1 (c (n "evil-janet") (v "1.24.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "079xalj65xg5f731hlkz22j1mfv9acfda8nap0pdhbn5h8vqpiwn") (f (quote (("system") ("link-system") ("link-amalg") ("default") ("debug-symbols"))))))

(define-public crate-evil-janet-1.25.1 (c (n "evil-janet") (v "1.25.1") (d (list (d (n "bindgen") (r "^0.61.0") (f (quote ("runtime" "which-rustfmt"))) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hfkxly2s6asbap00r7wmfpgi3n9qlwx7jrq726hjfx88fyhw30n") (f (quote (("system") ("link-system") ("link-amalg") ("default") ("debug-symbols"))))))

(define-public crate-evil-janet-1.26.0 (c (n "evil-janet") (v "1.26.0") (d (list (d (n "bindgen") (r "^0.61.0") (f (quote ("runtime" "which-rustfmt"))) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0z7clyshjpdj5rl1lr2zlchqybajzr9fnv7l9dhalapv27jf4723") (f (quote (("system") ("link-system") ("link-amalg") ("default") ("debug-symbols"))))))

(define-public crate-evil-janet-1.27.0 (c (n "evil-janet") (v "1.27.0") (d (list (d (n "bindgen") (r "^0.64.0") (f (quote ("runtime" "which-rustfmt"))) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1nsk5rn4qvpq9i2yqpanm1ziz5v3rn6cipl7f78iayphhpv204mf") (f (quote (("system") ("link-system") ("link-amalg") ("default") ("debug-symbols"))))))

(define-public crate-evil-janet-1.29.1 (c (n "evil-janet") (v "1.29.1") (d (list (d (n "bindgen") (r "^0.64.0") (f (quote ("runtime" "which-rustfmt"))) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09vixrwaxd0f0w3aa4wybiq6x65l1as870hhbmwg5ddlmwd6mpax") (f (quote (("system") ("link-system") ("link-amalg") ("default") ("debug-symbols"))))))

(define-public crate-evil-janet-1.30.0 (c (n "evil-janet") (v "1.30.0") (d (list (d (n "bindgen") (r "^0.66.1") (f (quote ("runtime" "which-rustfmt"))) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11fnla93wlcy40zyjhbnqv8f6sac190vsaz9b467qjkf5458piyb") (f (quote (("system") ("link-system") ("link-amalg") ("default") ("debug-symbols")))) (r "1.71.0")))

(define-public crate-evil-janet-1.30.1 (c (n "evil-janet") (v "1.30.1") (d (list (d (n "bindgen") (r "^0.68.1") (f (quote ("runtime" "which-rustfmt"))) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0bws7cispy58pabvga6m1js0wdhva23dxa455a1pgly1jsl3f7h5") (f (quote (("system") ("link-system") ("link-amalg") ("default") ("debug-symbols")))) (r "1.71.0")))

(define-public crate-evil-janet-1.31.0 (c (n "evil-janet") (v "1.31.0") (d (list (d (n "bindgen") (r "^0.69.0") (f (quote ("runtime" "which-rustfmt"))) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "16a5pk2m65wgggkvvvfwkb4hq646w4qn67dnhjfj3jya6x0gq9vr") (f (quote (("system") ("link-system") ("link-amalg") ("default") ("debug-symbols")))) (r "1.71.0")))

(define-public crate-evil-janet-1.32.0 (c (n "evil-janet") (v "1.32.0") (d (list (d (n "bindgen") (r "^0.69.0") (f (quote ("runtime" "which-rustfmt"))) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0j6yzl93grhm51bcf7czjwpbjhz6yhipp8mpxpkvz3p588b4wl2g") (f (quote (("system") ("link-system") ("link-amalg") ("default") ("debug-symbols")))) (r "1.71.0")))

(define-public crate-evil-janet-1.32.1 (c (n "evil-janet") (v "1.32.1") (d (list (d (n "bindgen") (r "^0.69.0") (f (quote ("runtime" "which-rustfmt"))) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "08jcp4fbivqzrxb72vzg549gdv1gli6sdbzw27a8z10dv7x3krxg") (f (quote (("system") ("link-system") ("link-amalg") ("default") ("debug-symbols")))) (r "1.71.0")))

(define-public crate-evil-janet-1.33.0 (c (n "evil-janet") (v "1.33.0") (d (list (d (n "bindgen") (r "^0.69.0") (f (quote ("runtime" "which-rustfmt"))) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0bcw3z5zr2pwjsl2ns2nmcnvmbmvv72xzizn7mfdr25mr954dl1k") (f (quote (("system") ("link-system") ("link-amalg") ("default") ("debug-symbols")))) (r "1.71.0")))

