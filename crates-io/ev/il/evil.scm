(define-module (crates-io ev il evil) #:use-module (crates-io))

(define-public crate-evil-0.0.0 (c (n "evil") (v "0.0.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)) (d (n "snafu") (r "^0.6.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.2") (d #t) (k 0)))) (h "14mrnpaczmxndl145c0ybnjmgdl16dx59ymyvh7d1rf9605fq9lz")))

