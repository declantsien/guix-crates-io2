(define-module (crates-io ev eb evebox-suricata-rule-parser) #:use-module (crates-io))

(define-public crate-evebox-suricata-rule-parser-0.1.0 (c (n "evebox-suricata-rule-parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "0m1mjqn3n4hhzihc2c4rb19gj4yzdycs8hhhqygyj5ikczp05cij") (y #t)))

(define-public crate-evebox-suricata-rule-parser-0.2.0 (c (n "evebox-suricata-rule-parser") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "1sg391zkwy9hx0q4s20yywlzwnad8r315gkvili7ch0x5gackgag") (y #t)))

