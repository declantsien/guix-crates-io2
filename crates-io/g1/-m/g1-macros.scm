(define-module (crates-io g1 -m g1-macros) #:use-module (crates-io))

(define-public crate-g1-macros-0.1.0-alpha.3 (c (n "g1-macros") (v "0.1.0-alpha.3") (d (list (d (n "g1-common") (r "^0.1.0-alpha.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "0im67i43ywnaan18sdyxi7q1mv9mywsl85376bf1wm8mxhfl7r93")))

