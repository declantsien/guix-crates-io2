(define-module (crates-io r1 #{8-}# r18-trans-support) #:use-module (crates-io))

(define-public crate-r18-trans-support-0.1.0 (c (n "r18-trans-support") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ykn3snbvszdkhjb4bm2pk3z89k8kf04k39mlrdlhb91775rvx60")))

(define-public crate-r18-trans-support-0.2.0 (c (n "r18-trans-support") (v "0.2.0") (d (list (d (n "mod_use") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wyczvyv4mzz8xdh15zgm5rbnvmsjrgvrjllavw7gknh2r3fm17f")))

(define-public crate-r18-trans-support-0.3.0 (c (n "r18-trans-support") (v "0.3.0") (d (list (d (n "mod_use") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gwkz89yy714g52q2dxa4bzihk260nky5ns52kg07pd229337jc3")))

(define-public crate-r18-trans-support-0.3.1 (c (n "r18-trans-support") (v "0.3.1") (d (list (d (n "mod_use") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vd123pi518bplzwbg2fkh25iysc9xqxh3q0yyarz64j78l3kz8q")))

(define-public crate-r18-trans-support-0.4.0 (c (n "r18-trans-support") (v "0.4.0") (d (list (d (n "mod_use") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1a9cwl8kv2yfbw15akp5k9zjcijvjzgg1ka3r46kx4ssnqzi709f")))

(define-public crate-r18-trans-support-0.4.1 (c (n "r18-trans-support") (v "0.4.1") (d (list (d (n "mod_use") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07a0llkxibk17zy49v7rdb8vj8y068fdj95g8pc4p7k0waxkwba8")))

(define-public crate-r18-trans-support-0.4.2 (c (n "r18-trans-support") (v "0.4.2") (d (list (d (n "mod_use") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y4fi7lcfxav4is39vz22qf0rcg7fq1lbhl88z1l62insa20syqd")))

(define-public crate-r18-trans-support-0.5.0 (c (n "r18-trans-support") (v "0.5.0") (d (list (d (n "mod_use") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lm82pn2b9f268bm9fi9yqw87ms0kcf8qqacj6yyhc7ggw4lribs")))

