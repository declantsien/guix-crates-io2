(define-module (crates-io r1 cs r1cs-zkinterface) #:use-module (crates-io))

(define-public crate-r1cs-zkinterface-0.1.0 (c (n "r1cs-zkinterface") (v "0.1.0") (d (list (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "r1cs") (r "^0.4.1") (d #t) (k 0)) (d (n "zkinterface") (r "^1.0.6") (d #t) (k 0)))) (h "1gryqs7lvzn1bz339ixfn3xrcjikd1zzgfsqdjnwla0q3nsps95y")))

(define-public crate-r1cs-zkinterface-0.1.1 (c (n "r1cs-zkinterface") (v "0.1.1") (d (list (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "r1cs") (r "^0.4.1") (d #t) (k 0)) (d (n "zkinterface") (r "^1.0.6") (d #t) (k 0)))) (h "17axdcs65vbqywn0s1kb4ibq32wb352ygq2d7k1pdmb6dlc39fm5")))

(define-public crate-r1cs-zkinterface-0.1.2 (c (n "r1cs-zkinterface") (v "0.1.2") (d (list (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "r1cs") (r "^0.4.7") (d #t) (k 0)) (d (n "zkinterface") (r "^1.0.6") (d #t) (k 0)))) (h "18b2nsfmf9ibmn3mkzsysjhxq7ikh125x8lps977gl9czk58gjb0")))

