(define-module (crates-io r1 cs r1cs-file) #:use-module (crates-io))

(define-public crate-r1cs-file-0.1.0 (c (n "r1cs-file") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)))) (h "1jnw4ziabajsh85skz8cr6ic0qknxygdk50yqikphxn0a0xfrhb2")))

(define-public crate-r1cs-file-0.2.0 (c (n "r1cs-file") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)))) (h "1apn554m103shid623ag0yrkd8ka878qn4xk80ja5vb2hpj9nlw1")))

(define-public crate-r1cs-file-0.2.1 (c (n "r1cs-file") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)))) (h "1z9c35c8ryi5vjwmyx7l77awdd315bvq75ffnmwmdbdrm9qhb3b0")))

(define-public crate-r1cs-file-0.3.0 (c (n "r1cs-file") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)))) (h "1f3d8qlliw25zykk4fl2ch1y2p48zdm5mx5ggpkl8fz03f8krjpb")))

