(define-module (crates-io r1 cs r1cs-bellman) #:use-module (crates-io))

(define-public crate-r1cs-bellman-0.1.0 (c (n "r1cs-bellman") (v "0.1.0") (d (list (d (n "bellman") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "pairing") (r "^0.14.2") (d #t) (k 0)) (d (n "r1cs") (r "^0.3.8") (d #t) (k 0)))) (h "0mr3qi6vbwid24fc6f30vlxmyf5d92mcjwqj8pizxnxkj45g5m24")))

(define-public crate-r1cs-bellman-0.1.1 (c (n "r1cs-bellman") (v "0.1.1") (d (list (d (n "bellman") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "pairing") (r "^0.14.2") (d #t) (k 0)) (d (n "r1cs") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0nw30zhxp1mjl5k5ivbm3rv4k3jccr455rngvxzi6yc13bpplgd8")))

