(define-module (crates-io r1 cs r1cs) #:use-module (crates-io))

(define-public crate-r1cs-0.1.0 (c (n "r1cs") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0pm58y80bs3pyqk94vbdbmva13my7g0mjm02nrzls62n0zn36b69")))

(define-public crate-r1cs-0.1.1 (c (n "r1cs") (v "0.1.1") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1ilc5cf8znplfpgfxvp2nngab8ids7042n8nygxlg9zsz95dcw4h")))

(define-public crate-r1cs-0.1.2 (c (n "r1cs") (v "0.1.2") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0rasqp0cd92r6gzxr8kzjnmqqskpwq01bw8rygj8m347axm75kqr")))

(define-public crate-r1cs-0.1.3 (c (n "r1cs") (v "0.1.3") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1rc35jhcap4lwyh5y10yksdm16s77zpq4jcxbpjz17lrbvykf5wr")))

(define-public crate-r1cs-0.1.4 (c (n "r1cs") (v "0.1.4") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "16r5xazzj2w2axjnj4kplgiqmgvcvsiwnl1f5y9qij67cvkbj7yi")))

(define-public crate-r1cs-0.1.5 (c (n "r1cs") (v "0.1.5") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "014p62kc715sb2sjbspw7yglv45sg39kmlwykvj3gik70f8qm1p8")))

(define-public crate-r1cs-0.1.6 (c (n "r1cs") (v "0.1.6") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "1gx910cp97hcgppcpg10fi2wnk4fywpgb440rl3h8jk5jxadx7h4")))

(define-public crate-r1cs-0.1.7 (c (n "r1cs") (v "0.1.7") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0c45bi4wivrfxcwkhdagvdjpx09qibrvj46cb084svyyljx9qy26")))

(define-public crate-r1cs-0.1.8 (c (n "r1cs") (v "0.1.8") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0048v8wfghyx27ybcmhsk05fx92y99bk98jxsmifib1ryml7nfb4")))

(define-public crate-r1cs-0.1.9 (c (n "r1cs") (v "0.1.9") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "08mg9q7svx0dyvdl83i6hhyi6d4ba0brbgjdigr9s9ngwvq6jgnq")))

(define-public crate-r1cs-0.1.10 (c (n "r1cs") (v "0.1.10") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0pgy2yv407jsns9kp6lxzp274fdlbwrlww55xgclgmr1lml6x0pp")))

(define-public crate-r1cs-0.1.11 (c (n "r1cs") (v "0.1.11") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "084d834h5y3b6332w7fbdcpkvn2xsz871ghjdrxvrfnq9rhq5gky")))

(define-public crate-r1cs-0.1.12 (c (n "r1cs") (v "0.1.12") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "16zbpyirrbyk74wgfc3h9phavhy2h4ms7lazwhvqps04hgymqqnf")))

(define-public crate-r1cs-0.1.13 (c (n "r1cs") (v "0.1.13") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "177z5f16wg98nx033p2l8087lzh8xp0pp8q0b5nbjp056m00bgpb")))

(define-public crate-r1cs-0.1.14 (c (n "r1cs") (v "0.1.14") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.1.1") (d #t) (k 0)))) (h "1x8z6mzrqv0x3wpgq2y9mj5sn2pllmlhl79ipkc5mk8bc369ania")))

(define-public crate-r1cs-0.1.15 (c (n "r1cs") (v "0.1.15") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.1.1") (d #t) (k 0)))) (h "18p6liqlv2fd4zi4h8k4zrp4rhjq46fh1d9vpq1fsilzgh6g69vh")))

(define-public crate-r1cs-0.2.0 (c (n "r1cs") (v "0.2.0") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.1.1") (d #t) (k 0)))) (h "10dc8cl8ivh8bxsm3hiwg7idjcrd4s539ffyr1w9imdcxaabd49b")))

(define-public crate-r1cs-0.2.1 (c (n "r1cs") (v "0.2.1") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.1.1") (d #t) (k 0)))) (h "0vh9ypcpns6kcmgixwl95b4sdjfl9lsxs8yvcavabqk2i3ysjp6w")))

(define-public crate-r1cs-0.2.2 (c (n "r1cs") (v "0.2.2") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.1.1") (d #t) (k 0)))) (h "150hhgrrh88fq9y7h8ccsjhnqcrdp09xjlkhpabk3102yhywx0nq")))

(define-public crate-r1cs-0.2.3 (c (n "r1cs") (v "0.2.3") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.1.1") (d #t) (k 0)))) (h "1b6bi6afa13g2v996m99r5raxr2rlsspf8gmvhfqmsfcbnsnwj2n")))

(define-public crate-r1cs-0.2.4 (c (n "r1cs") (v "0.2.4") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.1.1") (d #t) (k 0)))) (h "0ms7dpd7621km713asihp9gkxwswql11lcv0fik87vvc0vszs58v")))

(define-public crate-r1cs-0.3.0 (c (n "r1cs") (v "0.3.0") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.1.1") (d #t) (k 0)))) (h "1fk90csn24pp013z77511sc7r24q0ppc6ibm4lwr7ms0hd88zh2f")))

(define-public crate-r1cs-0.3.1 (c (n "r1cs") (v "0.3.1") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.1.1") (d #t) (k 0)))) (h "053xb356f1zgj2ijsf5ijdxp286arhfmqidygdwzgc321qh9pmqi")))

(define-public crate-r1cs-0.3.2 (c (n "r1cs") (v "0.3.2") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.1.1") (d #t) (k 0)))) (h "0i0jmggmbqn46bxaw8s6hy8zrpvpi0906jszh2pybc478ir18r42")))

(define-public crate-r1cs-0.3.3 (c (n "r1cs") (v "0.3.3") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.1.1") (d #t) (k 0)))) (h "1s6yzlx00m521q49y3j5wmryflc8aq31pkhf9b8fnz4rcx4gbq2q")))

(define-public crate-r1cs-0.3.4 (c (n "r1cs") (v "0.3.4") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.1.1") (d #t) (k 0)))) (h "1kvsw676zrwqrb6k54f8gv8vs0qlv10ma8z6dcdalr7h5rbbfvqk")))

(define-public crate-r1cs-0.3.5 (c (n "r1cs") (v "0.3.5") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.1.1") (d #t) (k 0)))) (h "1sv09a92h398d4a6s8jmdgaj96gjbsq8wsjx5wm8n6s9xq40j165")))

(define-public crate-r1cs-0.3.6 (c (n "r1cs") (v "0.3.6") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.1.1") (d #t) (k 0)))) (h "0gzykbd9hp98aam5swdq3vrx8qrf5gmnl3iahcm4j3qifxx0whzb")))

(define-public crate-r1cs-0.3.7 (c (n "r1cs") (v "0.3.7") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.1.1") (d #t) (k 0)))) (h "188ximyjnlswzm5lhz5ck0x2nj02harffaa4kxdkfic3cha83s92")))

(define-public crate-r1cs-0.3.8 (c (n "r1cs") (v "0.3.8") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.1.1") (d #t) (k 0)))) (h "123f2i1zmlfhxhmrw1y1p1jpbzqffs08za4nf76801c0grl9s110")))

(define-public crate-r1cs-0.3.9 (c (n "r1cs") (v "0.3.9") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.1.1") (d #t) (k 0)))) (h "0832023ldf3ghzc68xlh4sswyzq2x6kf07sxcipm827a8lik6y93")))

(define-public crate-r1cs-0.3.10 (c (n "r1cs") (v "0.3.10") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.1.1") (d #t) (k 0)))) (h "0nkj122y5pvfajsbfrbafsj1znahs9rsc6l6xlrdwvha80mnkbv1")))

(define-public crate-r1cs-0.4.0 (c (n "r1cs") (v "0.4.0") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "01a9cgxl76x2n03wp8l6nqp4bk6frb86kj4svwynhx7p8i6p66pp") (f (quote (("std" "num/std" "num-traits/std" "itertools/use_std" "bimap/std") ("default" "std"))))))

(define-public crate-r1cs-0.4.1 (c (n "r1cs") (v "0.4.1") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "0apdvadhzbyyy3ppv2lwbnc1ql7hp36mbaa7nxlw6yspz977ilil") (f (quote (("std" "num/std" "num-traits/std" "itertools/use_std" "bimap/std") ("default" "std"))))))

(define-public crate-r1cs-0.4.3 (c (n "r1cs") (v "0.4.3") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "151jan00cjab68y8v0avrviyxkfmzqhk5q0rzhavw7g6zbnm5xq2") (f (quote (("std" "num/std" "num-traits/std" "itertools/use_std" "bimap/std") ("default" "std"))))))

(define-public crate-r1cs-0.4.4 (c (n "r1cs") (v "0.4.4") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "04s3wwv0w6a2lzpnyf4j35yby2xz17amwf7nr6vwccw0bpr05yjc") (f (quote (("std" "num/std" "num-traits/std" "itertools/use_std" "bimap/std") ("default" "std"))))))

(define-public crate-r1cs-0.4.5 (c (n "r1cs") (v "0.4.5") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "0l2m8pbq9w25525kcqbh6hy4g796s1wp12syk0x0bqad2czp31f0") (f (quote (("std" "num/std" "num-traits/std" "itertools/use_std" "bimap/std") ("default" "std"))))))

(define-public crate-r1cs-0.4.7 (c (n "r1cs") (v "0.4.7") (d (list (d (n "bimap") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (f (quote ("rand"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "0iin37d5bvxxn1k6cx3qfs09nnbi0bszzxmp7hlwp9wl031izs54") (f (quote (("std" "num/std" "num-traits/std" "itertools/use_std" "bimap/std") ("default" "std"))))))

