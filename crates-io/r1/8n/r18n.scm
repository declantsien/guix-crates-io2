(define-module (crates-io r1 #{8n}# r18n) #:use-module (crates-io))

(define-public crate-r18n-0.0.1 (c (n "r18n") (v "0.0.1") (d (list (d (n "toml") (r "*") (d #t) (k 0)))) (h "1qixvzxg54qqb9lxlg58g2pvdg7q48whdxa173gygzj7r1f09bbg")))

(define-public crate-r18n-0.0.2 (c (n "r18n") (v "0.0.2") (d (list (d (n "toml") (r "*") (d #t) (k 0)))) (h "0y7adc530p524k58ql5jwngv6yj7fs8rf5d38mr9fkhcx6649vzl")))

