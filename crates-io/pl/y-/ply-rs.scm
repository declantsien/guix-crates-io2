(define-module (crates-io pl y- ply-rs) #:use-module (crates-io))

(define-public crate-ply-rs-0.1.0 (c (n "ply-rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.4.2") (d #t) (k 0)) (d (n "peg") (r "^0.5.3") (d #t) (k 1)) (d (n "skeptic") (r "^0.9") (d #t) (k 1)) (d (n "skeptic") (r "^0.9") (d #t) (k 2)))) (h "1wifzfcg0w39r68mwfrxrlrnxgb856l80ynbyfamp3ylsr0fgynm")))

(define-public crate-ply-rs-0.1.1 (c (n "ply-rs") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.0") (d #t) (k 0)) (d (n "peg") (r "^0.5.4") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)))) (h "0bxkyvwf50703cc5p8q7i8zphd6z9841m3fcxla3fhw4yahnwndj")))

(define-public crate-ply-rs-0.1.2 (c (n "ply-rs") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.1") (d #t) (k 0)) (d (n "peg") (r "^0.5.4") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.4") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.4") (d #t) (k 2)))) (h "0jir9q32xfz79v3rf2vcr3abkssyvx1q13zk3fbxxr4z4yd9fsyn")))

(define-public crate-ply-rs-0.1.3 (c (n "ply-rs") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.1") (d #t) (k 0)) (d (n "peg") (r "^0.6.0") (d #t) (k 0)) (d (n "skeptic") (r "^0.13.4") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.4") (d #t) (k 2)))) (h "0qw9p0lbd7vqpq0d11f638fidn7v9zk6z0349kg1dmbr9b5zkbfv")))

