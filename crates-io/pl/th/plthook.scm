(define-module (crates-io pl th plthook) #:use-module (crates-io))

(define-public crate-plthook-0.1.0 (c (n "plthook") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1h8ny2gx46rw4nwprrja3rjf3x52r8m8c4gn3ki96jqr0492c2b1")))

(define-public crate-plthook-0.2.0 (c (n "plthook") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "072irvq160mjjjbpfzc5gihq6zbp0r5giijp417al2h1ahg62l0b")))

(define-public crate-plthook-0.2.1 (c (n "plthook") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "15jg9m5g1apcwza06kxn04qrwwsvhs33f0jcgbpdjvsmn8bmcf9j")))

(define-public crate-plthook-0.2.2 (c (n "plthook") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "10rvsrb28zx2cr82028541y36sfpib70l9vxyzgcyrwhgwdjcg2n")))

