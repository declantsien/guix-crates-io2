(define-module (crates-io pl od plod_derive) #:use-module (crates-io))

(define-public crate-plod_derive-0.2.1 (c (n "plod_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13flj9a7zawdrmx4diipysnhipkdx5d4s8izhdmh97kday0426vp")))

(define-public crate-plod_derive-0.2.2 (c (n "plod_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mwdpyk0frvbcngi1y8gchsjjibwiqv9vhn1ksm2d7v2zwf3j4r7")))

(define-public crate-plod_derive-0.3.0 (c (n "plod_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sskchf050z94k5p3wri6pjxl45idlz4hj9hzd0wx11njsy3yjk9")))

(define-public crate-plod_derive-0.4.0 (c (n "plod_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hfsrwh2wlwrrh16n3r1gcwnhgna983sm2ynv7myk6hiypmq7bg4")))

