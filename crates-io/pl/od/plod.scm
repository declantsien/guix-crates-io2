(define-module (crates-io pl od plod) #:use-module (crates-io))

(define-public crate-plod-0.2.1 (c (n "plod") (v "0.2.1") (d (list (d (n "plod_derive") (r "^0.2") (d #t) (k 0)))) (h "1kcb4yirqwx9fsbmf9vihd9wnvglcnaag4fb1bzfzp0caak7ww1k")))

(define-public crate-plod-0.2.2 (c (n "plod") (v "0.2.2") (d (list (d (n "plod_derive") (r "^0.2") (d #t) (k 0)))) (h "1xqfzdk5j0y4dakj3f2drg6cvysrx38hrdjw7lb5pdgqazc3v2n3")))

(define-public crate-plod-0.3.0 (c (n "plod") (v "0.3.0") (d (list (d (n "plod_derive") (r "^0.3") (d #t) (k 0)))) (h "0jpxxcd9pnq29r7yp6afpp7bp3l2vdqn30d3c658pmskixc4lrr6")))

(define-public crate-plod-0.4.0 (c (n "plod") (v "0.4.0") (d (list (d (n "plod_derive") (r "^0.4") (d #t) (k 0)))) (h "1f3224saxcrks66x3838ayxlwcpmiba8afhx9xqrp0g7r8sapz9a")))

