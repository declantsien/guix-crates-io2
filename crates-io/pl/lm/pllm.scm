(define-module (crates-io pl lm pllm) #:use-module (crates-io))

(define-public crate-pllm-0.1.0 (c (n "pllm") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "06g6m0f625jp5h39p3dv731cs0xmy1inxc8drsamlkibqrfz5fym") (y #t)))

(define-public crate-pllm-0.2.0 (c (n "pllm") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0avxa1b899vpa7vvlnrqhdxsykka94y92j9h1364nz2xxdl879s9")))

(define-public crate-pllm-0.3.0 (c (n "pllm") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1a0yjs0bln0w24bpfcrj59a7n271p5w3xn92nxjpl162n4vjasb3")))

(define-public crate-pllm-0.3.1 (c (n "pllm") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1srqgqvr9vy3z5gqmq5irrwbhdmk5lndvpvrznhga4mi9kzf14sq")))

(define-public crate-pllm-0.4.0 (c (n "pllm") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "half") (r "^2.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1c6f02lccccxm062xr8bcb026fa8y8nyz7j31rvfz201sz9mx1jd")))

(define-public crate-pllm-0.4.1 (c (n "pllm") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "half") (r "^2.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1p4zfv0j0g89g184qffwcslr9s1940qbfpgnw1k5a27s9p151k2x")))

