(define-module (crates-io pl yk plykit) #:use-module (crates-io))

(define-public crate-plykit-0.1.1 (c (n "plykit") (v "0.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "04ma4b2s3m7zky61yyknwiqqh3agwbkf4c8khd33lik35kbb7gsf")))

(define-public crate-plykit-0.1.2 (c (n "plykit") (v "0.1.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1p1alr30q2z48yblp43v6gvsc6rdpvh5ysyy181j7kn5ycd50497")))

(define-public crate-plykit-0.1.3 (c (n "plykit") (v "0.1.3") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1c1x5fcy4cj8926ah6iw8sarqpxqk5aa23lzmd460h1l83fvj34k")))

(define-public crate-plykit-0.1.4 (c (n "plykit") (v "0.1.4") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0w6xkz2bmrqjm4qy22gql5i1978nsabggia47kb921xyhp8b7n5g")))

(define-public crate-plykit-0.1.5 (c (n "plykit") (v "0.1.5") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1db7apyd8sq1j98dnhslgdwxshyl4jclz1lzxxzhzm9qfd3cgsa2")))

(define-public crate-plykit-0.1.6 (c (n "plykit") (v "0.1.6") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "09dvll2d0a62wvng9c164kqg8l039xkdycywxrplccmypmcjpfl1")))

(define-public crate-plykit-0.1.7 (c (n "plykit") (v "0.1.7") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0h67a0kk04dfhfknd2gdhagkxq6fxbls0ih1730nn2gqkacwzxwm")))

