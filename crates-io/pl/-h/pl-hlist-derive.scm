(define-module (crates-io pl -h pl-hlist-derive) #:use-module (crates-io))

(define-public crate-pl-hlist-derive-1.0.0 (c (n "pl-hlist-derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "0a8pvp0vlskvxf2pa3gmcg4mpfnwvddrnkfd003njlhs9r2fr5hj")))

