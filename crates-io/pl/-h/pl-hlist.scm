(define-module (crates-io pl -h pl-hlist) #:use-module (crates-io))

(define-public crate-pl-hlist-1.0.0 (c (n "pl-hlist") (v "1.0.0") (d (list (d (n "pl-hlist-derive") (r "^1.0.0") (d #t) (k 0)))) (h "1walb6s3jgdwywkjgbjjidfb4dlz9qwp85np34n3mhgv2bgqq5x9")))

(define-public crate-pl-hlist-1.0.1 (c (n "pl-hlist") (v "1.0.1") (d (list (d (n "pl-hlist-derive") (r "^1.0.0") (d #t) (k 0)))) (h "1127l32c25kkvvbqm9bbs1mcl8ghi91fj0s1hl5gnpy88kshfwq7")))

