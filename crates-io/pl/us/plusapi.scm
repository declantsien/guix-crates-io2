(define-module (crates-io pl us plusapi) #:use-module (crates-io))

(define-public crate-plusapi-0.1.0 (c (n "plusapi") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1kh6alzbl6769213vbp77q592fmk2lcq15fi9cxcv6mhz7ph5rf3") (f (quote (("blocking" "reqwest/blocking"))))))

