(define-module (crates-io pl us plus_minus) #:use-module (crates-io))

(define-public crate-plus_minus-0.1.0 (c (n "plus_minus") (v "0.1.0") (h "0ncz2v2d6dgvphsz5agzs1wj9jvxhb5iy93503n8w10xv34dmlv8")))

(define-public crate-plus_minus-0.1.1 (c (n "plus_minus") (v "0.1.1") (h "060f9zbgn3cnfzbw5rjn71022c36m1jl55mxjm04yp79zivmiw3p")))

