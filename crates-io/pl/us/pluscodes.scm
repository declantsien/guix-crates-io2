(define-module (crates-io pl us pluscodes) #:use-module (crates-io))

(define-public crate-pluscodes-0.1.0 (c (n "pluscodes") (v "0.1.0") (d (list (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "099dpakb9w6aqfh9d30g8yfpp8mpxi5kwf2pgbc02mnf1c40slw1")))

(define-public crate-pluscodes-0.2.0 (c (n "pluscodes") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "15hy2hmc9fv1a8gd9768n0k3zjk637hixq2vp6m1pziiw9yw8hn1")))

(define-public crate-pluscodes-0.3.0 (c (n "pluscodes") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1f49q0wncngc6r5xiqw98pzgni18ccvzflrn3h5rkd3qhayphdsc")))

(define-public crate-pluscodes-0.4.0 (c (n "pluscodes") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1k4dbb5x4i2w9mzd8z39vf2ln25v6z1npxidsb3c5z94k53mmvbh")))

(define-public crate-pluscodes-0.5.0 (c (n "pluscodes") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1yif11xk9qpp243gi2r6jmry99p6c1ssgjvif1526kv00a72ilz5")))

