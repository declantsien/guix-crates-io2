(define-module (crates-io pl us plushy) #:use-module (crates-io))

(define-public crate-plushy-0.0.0 (c (n "plushy") (v "0.0.0") (h "0dd7352fxwiiib8w4l0z8myik2gsbg1ql1immv708qwkrl7ma6p4")))

(define-public crate-plushy-0.1.0 (c (n "plushy") (v "0.1.0") (d (list (d (n "thunderdome") (r "^0.6.1") (d #t) (k 0)))) (h "0vdnmzr0vp4dkacq67vgksan04q5rcj9dys416s209qpi7w16rbk")))

(define-public crate-plushy-0.1.1 (c (n "plushy") (v "0.1.1") (d (list (d (n "thunderdome") (r "^0.6.1") (d #t) (k 0)))) (h "0jyai2x1z382b4ir4msclrd3axg1f45dsv0w5qhk7xjxsqx20lpx")))

(define-public crate-plushy-0.1.2 (c (n "plushy") (v "0.1.2") (d (list (d (n "thunderdome") (r "^0.6.1") (d #t) (k 0)))) (h "0af4gd81ymw54lninr2v87x38ccfjw4aigds4hmy0bp76cyrcr24")))

(define-public crate-plushy-0.1.3 (c (n "plushy") (v "0.1.3") (d (list (d (n "thunderdome") (r "^0.6.1") (d #t) (k 0)))) (h "1sm86ipqz8ka50c33m8if20vhlzj2k033rldlf1dhblibynwrfpp")))

