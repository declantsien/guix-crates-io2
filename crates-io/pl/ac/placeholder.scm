(define-module (crates-io pl ac placeholder) #:use-module (crates-io))

(define-public crate-placeholder-0.1.0 (c (n "placeholder") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0zxm5vha1qji00kf006f3jl51fmf59v5dcx47mib1hj0d2cjx8ms")))

(define-public crate-placeholder-0.1.2 (c (n "placeholder") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0hi492kir9q14kp6502lhk48414f5v8z049bpzgclw7f0j228al7")))

(define-public crate-placeholder-1.1.3 (c (n "placeholder") (v "1.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "05kccryqhxjyrqqi18gkvar4nn33kmchwa4mij4mhy0ms5s8cj4a")))

(define-public crate-placeholder-1.1.4 (c (n "placeholder") (v "1.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "087b5nmdcxf82vrsyrvfapz1a0k0qq4v9fi5yyrj1v6icvm93dpi")))

