(define-module (crates-io pl ac place_macro_proc) #:use-module (crates-io))

(define-public crate-place_macro_proc-1.0.0 (c (n "place_macro_proc") (v "1.0.0") (d (list (d (n "place_macro_core") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)))) (h "1asmhywvra6m42ml4pjzhh55ljyyya70phnk34g6m3sc0iihv3r7")))

