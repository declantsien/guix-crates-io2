(define-module (crates-io pl ac place_macro_core) #:use-module (crates-io))

(define-public crate-place_macro_core-1.0.0 (c (n "place_macro_core") (v "1.0.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "litrs") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)))) (h "17cfj5w6lf1s8j4mb6crqr7ip5ik049a7szvdxs7w89812rax90m")))

