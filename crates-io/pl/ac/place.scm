(define-module (crates-io pl ac place) #:use-module (crates-io))

(define-public crate-place-0.0.0 (c (n "place") (v "0.0.0") (h "08smrcs1na4nb1x2ivmd8nxcny9wrrc26q6m0fpffcmzah41m93f")))

(define-public crate-place-0.1.0 (c (n "place") (v "0.1.0") (h "16wnqla46mamya09gsvgdrf16c6cjraxx5sakk2a09dcdcnrksds")))

