(define-module (crates-io pl ac place_capitals) #:use-module (crates-io))

(define-public crate-place_capitals-0.1.0 (c (n "place_capitals") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "0qw6y0wkvzgjqdm06xrqsb05kfc7qf9s4gr4f7mhryf4aczhsg0v")))

(define-public crate-place_capitals-0.1.1 (c (n "place_capitals") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "1phirgvk3sipc6c72piqww2rd1rh496r1kda6pbh6jglbhm808hn")))

