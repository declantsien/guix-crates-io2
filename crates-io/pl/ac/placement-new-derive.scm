(define-module (crates-io pl ac placement-new-derive) #:use-module (crates-io))

(define-public crate-placement-new-derive-0.1.0 (c (n "placement-new-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0p35lsr2j0lgvldrvww65ijp1mq1zq0rhvkwa2kgsymdibcnqyd5") (y #t)))

(define-public crate-placement-new-derive-0.2.0 (c (n "placement-new-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15b3nkhs8chsw15kbh62a3kkr3d6dnbb4xmnzyfdb0sxxddbnfk4") (y #t)))

(define-public crate-placement-new-derive-0.2.1 (c (n "placement-new-derive") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1drinxksw1xj7y70q8g1c3cbjm4hfd2mj7s6vbrmmgn14wfzqrjg") (y #t)))

(define-public crate-placement-new-derive-0.3.0 (c (n "placement-new-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kvy77yl4cm390caz6gw5xvnmhbkqr6zl55gx8ld7i34mn947ifn") (y #t)))

