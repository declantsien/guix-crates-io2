(define-module (crates-io pl ac placeholder_closure) #:use-module (crates-io))

(define-public crate-placeholder_closure-0.1.0 (c (n "placeholder_closure") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "0x74ys481va4pn7n1d7w51mh2x35niypifwmk8iy27blj1b159k3")))

