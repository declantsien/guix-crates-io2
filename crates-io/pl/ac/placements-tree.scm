(define-module (crates-io pl ac placements-tree) #:use-module (crates-io))

(define-public crate-placements-tree-0.1.0 (c (n "placements-tree") (v "0.1.0") (h "0psr109hn9llhrq1bc15jmc6z7ggnnwb7hj1jhv72bkkwwgg79zs") (y #t)))

(define-public crate-placements-tree-0.1.1 (c (n "placements-tree") (v "0.1.1") (h "03iinfdb8cb71scj54dj8ms55913kf69hxiqvirq08khcmffmqmw") (y #t)))

(define-public crate-placements-tree-0.1.2 (c (n "placements-tree") (v "0.1.2") (h "19vks1abmp3swjnplz3b62qjq39n9q824pn9h2gwrkvyl1xhp4vk") (y #t)))

(define-public crate-placements-tree-0.1.3 (c (n "placements-tree") (v "0.1.3") (h "1bclwnc6x7zamk9ykj3w16kglxic1iyywvnzpv3shfb2hyx8lpa6") (y #t)))

(define-public crate-placements-tree-0.1.4 (c (n "placements-tree") (v "0.1.4") (h "05s43w32plchvdvhhql0yp4agxlw4zzgljqmpvfkvafcm43gzkn5") (y #t)))

(define-public crate-placements-tree-0.1.5 (c (n "placements-tree") (v "0.1.5") (h "03s41d2ha5klw9lp5d631aklnzgk9zj2byqqg687ssj19934ganx") (y #t)))

(define-public crate-placements-tree-0.1.6 (c (n "placements-tree") (v "0.1.6") (h "11rnwrwicakvlhpjs2yqd0igilhjsg8nyms87p7682yx8145nrq3") (y #t)))

(define-public crate-placements-tree-0.1.7 (c (n "placements-tree") (v "0.1.7") (h "0bdifkfgmpma89y20z59zglfj9li4dr94j0g1h99dmanpk31znxa")))

