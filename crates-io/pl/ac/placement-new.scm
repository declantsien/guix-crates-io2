(define-module (crates-io pl ac placement-new) #:use-module (crates-io))

(define-public crate-placement-new-0.1.0 (c (n "placement-new") (v "0.1.0") (d (list (d (n "placement-new-derive") (r "=0.1.0") (o #t) (d #t) (k 0)))) (h "13n6idq3v1bbxxi86x1prg5g9wd8999xj9kxjh7ihmkbb68g5z1v") (f (quote (("derive" "placement-new-derive") ("default" "alloc" "derive") ("alloc")))) (y #t)))

(define-public crate-placement-new-0.2.0 (c (n "placement-new") (v "0.2.0") (d (list (d (n "placement-new-derive") (r "=0.2.0") (o #t) (d #t) (k 0)))) (h "0q0s1yn6w2k4bhfj0iqzqp5jxg4i2v26j0006g3bwid6hffy2yzs") (f (quote (("derive" "placement-new-derive") ("default" "alloc" "derive") ("alloc")))) (y #t)))

(define-public crate-placement-new-0.2.1 (c (n "placement-new") (v "0.2.1") (d (list (d (n "placement-new-derive") (r "=0.2.1") (o #t) (d #t) (k 0)))) (h "19vg753cqv2kzw4x3k3i9lq12395r162rzpvr5gs0x7pqhwp0gzk") (f (quote (("derive" "placement-new-derive") ("default" "alloc" "derive") ("alloc")))) (y #t)))

(define-public crate-placement-new-0.3.0 (c (n "placement-new") (v "0.3.0") (d (list (d (n "placement-new-derive") (r "=0.3.0") (o #t) (d #t) (k 0)))) (h "1r9pqimzy0476bs9xjaa8b6nvam13vn3336c9kn6884s3l85bdg1") (f (quote (("derive" "placement-new-derive") ("default" "alloc" "derive") ("alloc")))) (y #t)))

