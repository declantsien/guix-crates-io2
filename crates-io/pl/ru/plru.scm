(define-module (crates-io pl ru plru) #:use-module (crates-io))

(define-public crate-plru-0.1.0 (c (n "plru") (v "0.1.0") (h "04q0mmzb083rxs15kqyk592bklwqbvza01qfhqzwfiz7s7b5gplf") (f (quote (("no_std"))))))

(define-public crate-plru-0.1.1 (c (n "plru") (v "0.1.1") (h "0yz90dpz0rpfq91bqzraw73hx8iazff0an1mwi546nq6k4j8yz5g") (f (quote (("no_std"))))))

