(define-module (crates-io pl ru plrust-trusted-pgx) #:use-module (crates-io))

(define-public crate-plrust-trusted-pgx-0.0.0-do-not-use (c (n "plrust-trusted-pgx") (v "0.0.0-do-not-use") (d (list (d (n "pgx") (r "=0.7.2") (f (quote ("no-schema-generation"))) (k 0)))) (h "1cvx6bbikhnq066wc83m2pwkygx2ld568gazq65f44snjk01xxax") (f (quote (("pg15" "pgx/pg15") ("pg14" "pgx/pg14") ("pg13" "pgx/pg13"))))))

(define-public crate-plrust-trusted-pgx-1.0.0-rc.0 (c (n "plrust-trusted-pgx") (v "1.0.0-rc.0") (d (list (d (n "pgx") (r "=0.7.2") (f (quote ("no-schema-generation"))) (k 0)))) (h "02myqc78bihbr25mkx6pmmg3gwb9fy8dir624nr24p9lh7bmbh77") (f (quote (("pg15" "pgx/pg15") ("pg14" "pgx/pg14") ("pg13" "pgx/pg13"))))))

(define-public crate-plrust-trusted-pgx-1.0.0-rc.1 (c (n "plrust-trusted-pgx") (v "1.0.0-rc.1") (d (list (d (n "pgx") (r "=0.7.4") (f (quote ("no-schema-generation"))) (k 0)))) (h "0gbzdvam9dm520zrm7rgvb2jdnppp85zgqng5693sbfc6zyaj0ia") (f (quote (("pg15" "pgx/pg15") ("pg14" "pgx/pg14") ("pg13" "pgx/pg13"))))))

(define-public crate-plrust-trusted-pgx-1.0.0 (c (n "plrust-trusted-pgx") (v "1.0.0") (d (list (d (n "pgx") (r "=0.7.4") (f (quote ("no-schema-generation"))) (k 0)))) (h "1j9sfnvdacz02254lzl7cnvybcc3dnbg5ahf9p9js6jddg8hcsg1") (f (quote (("pg15" "pgx/pg15") ("pg14" "pgx/pg14") ("pg13" "pgx/pg13"))))))

