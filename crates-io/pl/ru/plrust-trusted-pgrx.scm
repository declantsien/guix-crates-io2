(define-module (crates-io pl ru plrust-trusted-pgrx) #:use-module (crates-io))

(define-public crate-plrust-trusted-pgrx-1.0.0 (c (n "plrust-trusted-pgrx") (v "1.0.0") (d (list (d (n "pgrx") (r "=0.7.4") (f (quote ("no-schema-generation"))) (k 0)))) (h "0qah6rrn0jxl004m1iiccxl9xbp4ylzw11ac9blhnz9y28pq5f2g") (f (quote (("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1.1.0 (c (n "plrust-trusted-pgrx") (v "1.1.0") (d (list (d (n "pgrx") (r "=0.8.1") (f (quote ("no-schema-generation"))) (k 0)))) (h "17h7dpfc3cvl4wsk0p2jshflrb7wqs3vcxh992fqndsdqspz8c0i") (f (quote (("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1.1.1 (c (n "plrust-trusted-pgrx") (v "1.1.1") (d (list (d (n "pgrx") (r "=0.8.2") (f (quote ("no-schema-generation"))) (k 0)))) (h "1s5gvb26icm0ccrgdz5qrky0k1w476glv2yi95ifgrs1913654ij") (f (quote (("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1.1.2 (c (n "plrust-trusted-pgrx") (v "1.1.2") (d (list (d (n "pgrx") (r "=0.8.3") (f (quote ("no-schema-generation"))) (k 0)))) (h "038ksph9l9zsnpqkjkaamdfrp4nbvh4n4rg3bmaw7lklqs5ayxac") (f (quote (("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1.1.3 (c (n "plrust-trusted-pgrx") (v "1.1.3") (d (list (d (n "pgrx") (r "=0.8.4") (f (quote ("no-schema-generation"))) (k 0)))) (h "0pj38rvy5nd670l3f8wgkw8iygs3brh5159fhmvsk5g385ylyd8i") (f (quote (("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1.2.0 (c (n "plrust-trusted-pgrx") (v "1.2.0") (d (list (d (n "pgrx") (r "=0.9.5") (f (quote ("no-schema-generation"))) (k 0)))) (h "19c0xwhpbkm74wbcacf6br2jx7504xk14bc3k73h543flv0lkciv") (f (quote (("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1.2.1 (c (n "plrust-trusted-pgrx") (v "1.2.1") (d (list (d (n "pgrx") (r "=0.9.6") (f (quote ("no-schema-generation"))) (k 0)))) (h "1jpf6vrvn18dj9pp3pm62f6pd04fssnx8w0i8qi59z5hwlraddyw") (f (quote (("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1.2.2 (c (n "plrust-trusted-pgrx") (v "1.2.2") (d (list (d (n "pgrx") (r "=0.9.7") (f (quote ("no-schema-generation"))) (k 0)))) (h "0di3ls1196iidm0mqh3b2l1p7cq3wfzn90cfy0l3bhd4izihlngv") (f (quote (("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1.2.3 (c (n "plrust-trusted-pgrx") (v "1.2.3") (d (list (d (n "pgrx") (r "=0.9.7") (f (quote ("no-schema-generation"))) (k 0)))) (h "03yb99p71nzih295y7mwwhzpjg6pjmsqgnbbpw4gm0xkkzd4nih7") (f (quote (("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1.2.4-beta.pg16b2 (c (n "plrust-trusted-pgrx") (v "1.2.4-beta.pg16b2") (d (list (d (n "pgrx") (r "^0.10.0-beta.0") (f (quote ("no-schema-generation"))) (k 0)))) (h "0fmklfi6c6qhgs5g5l7782yzk7dkm3h4axc1n5ynxa950vxr68id") (f (quote (("pg16" "pgrx/pg16") ("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1.2.4-beta.pg16b3 (c (n "plrust-trusted-pgrx") (v "1.2.4-beta.pg16b3") (d (list (d (n "pgrx") (r "^0.10.0-beta.3") (f (quote ("no-schema-generation"))) (k 0)))) (h "093xxv58znjjnmig3vxq49h5nlary9fp7jpcqif51afz36f0b5wg") (f (quote (("pg16" "pgrx/pg16") ("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1.2.4 (c (n "plrust-trusted-pgrx") (v "1.2.4") (d (list (d (n "pgrx") (r "=0.10.0") (f (quote ("no-schema-generation"))) (k 0)))) (h "04rwgk88wqwhcskj50l4pn0snjr0h3xzpbjfm5r053d1688h50vx") (f (quote (("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13")))) (y #t)))

(define-public crate-plrust-trusted-pgrx-1.2.5 (c (n "plrust-trusted-pgrx") (v "1.2.5") (d (list (d (n "pgrx") (r "=0.10.0") (f (quote ("no-schema-generation"))) (k 0)))) (h "1bph07jclxipqywr146kx0hmbvp68cnlwnj250gz7mvq33kqhgfd") (f (quote (("pg16" "pgrx/pg16") ("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1.2.6 (c (n "plrust-trusted-pgrx") (v "1.2.6") (d (list (d (n "pgrx") (r "=0.10.1") (f (quote ("no-schema-generation"))) (k 0)))) (h "1zmr65gsvrsld9zqci7i0fhi7lnhy8m4gw99l375g0g82qdwzk15") (f (quote (("pg16" "pgrx/pg16") ("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1.2.7 (c (n "plrust-trusted-pgrx") (v "1.2.7") (d (list (d (n "pgrx") (r "=0.11.0") (f (quote ("no-schema-generation"))) (k 0)))) (h "0zizp87lhdfw85d2xvsfxj0gq3jixck9shs7njq28x3267biczgc") (f (quote (("pg16" "pgrx/pg16") ("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1.2.8 (c (n "plrust-trusted-pgrx") (v "1.2.8") (d (list (d (n "pgrx") (r "=0.11.0") (f (quote ("no-schema-generation"))) (k 0)))) (h "0llf7ygqvk7fwksrq5xs4vgb3m441zjzn46yixbgmzm26iran3hc") (f (quote (("pg16" "pgrx/pg16") ("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

