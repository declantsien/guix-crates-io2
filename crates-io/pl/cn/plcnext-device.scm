(define-module (crates-io pl cn plcnext-device) #:use-module (crates-io))

(define-public crate-plcnext-device-0.1.0 (c (n "plcnext-device") (v "0.1.0") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)))) (h "0clj3rxzm1v77ad38hkfcm81mzw6w1zb69sy55nngnbp08ywdr8x")))

