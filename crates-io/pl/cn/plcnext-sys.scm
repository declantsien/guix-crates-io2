(define-module (crates-io pl cn plcnext-sys) #:use-module (crates-io))

(define-public crate-plcnext-sys-0.1.0 (c (n "plcnext-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)))) (h "1zzh9wgin3716kncr1irvv2r04wv6ns7wrzkn4yxb9n7r5mrjas4")))

(define-public crate-plcnext-sys-0.2.0 (c (n "plcnext-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)))) (h "1bn6xjvfhrcmdcpf38ghwlx14ypdskw45szp0jdv6kvh5zvk38hp")))

