(define-module (crates-io pl cn plcnext) #:use-module (crates-io))

(define-public crate-plcnext-0.1.0 (c (n "plcnext") (v "0.1.0") (h "0c3h5p2cmr1sm51z4119c9xwda5bk12163fc1scbkzi2l216bhzj")))

(define-public crate-plcnext-0.2.0 (c (n "plcnext") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "plcnext-sys") (r "^0.2.0") (d #t) (k 0)))) (h "06n2gkcskyfk3kj3xamlbiv30d3gcmc8y0xybxv4zxv8m6rhpgm2")))

