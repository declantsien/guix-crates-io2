(define-module (crates-io pl cn plcnext-axioline) #:use-module (crates-io))

(define-public crate-plcnext-axioline-0.1.0 (c (n "plcnext-axioline") (v "0.1.0") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "115d1cs55kisk3mkabfry40ayh6hif2b4x40rmxg7q40shinvm9m")))

