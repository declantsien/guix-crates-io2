(define-module (crates-io pl rs plrs) #:use-module (crates-io))

(define-public crate-plrs-0.1.3 (c (n "plrs") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.13.2") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "0fzcv642z12iv3dpqvf6z4a7z9z4kq7v24kx2a8sv1nyxxzf7rpg")))

