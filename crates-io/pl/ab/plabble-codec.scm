(define-module (crates-io pl ab plabble-codec) #:use-module (crates-io))

(define-public crate-plabble-codec-0.1.0 (c (n "plabble-codec") (v "0.1.0") (d (list (d (n "chacha20") (r ">=0.9") (d #t) (k 0)) (d (n "chacha20poly1305") (r ">=0.10") (d #t) (k 0)) (d (n "hkdf") (r ">=0.12") (d #t) (k 0)) (d (n "itertools") (r ">=0.10") (d #t) (k 0)) (d (n "poly1305") (r ">=0.8") (d #t) (k 0)) (d (n "rand") (r ">=0.8") (d #t) (k 0)) (d (n "sha2") (r ">=0.10") (d #t) (k 0)) (d (n "time") (r ">=0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r ">=0.2") (d #t) (k 0)) (d (n "time-macros") (r ">=0.2") (d #t) (k 2)) (d (n "getrandom") (r ">=0.2") (f (quote ("js"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0yswnqja4anqh6g89mqplrpbv6ny48jawi0dxf2z9hwcxgg4vqiv")))

