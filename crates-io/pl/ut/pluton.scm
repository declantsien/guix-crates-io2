(define-module (crates-io pl ut pluton) #:use-module (crates-io))

(define-public crate-pluton-0.1.0 (c (n "pluton") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "hid") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.34") (d #t) (k 0)))) (h "0fmqfwlxd8dd8akwzpw63ra08jpn04pbin7bznhn4zi9ysvg3w08")))

