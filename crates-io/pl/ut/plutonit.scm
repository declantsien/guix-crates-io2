(define-module (crates-io pl ut plutonit) #:use-module (crates-io))

(define-public crate-plutonit-0.0.0 (c (n "plutonit") (v "0.0.0") (h "1d641dsfjshvkikyw20gih23iw7nxw1g5m7b0k5rz7akv2imxdnq")))

(define-public crate-plutonit-0.0.0-0 (c (n "plutonit") (v "0.0.0-0") (h "13w1ihi84aj9nddflbbzgvdd2fkvy656lxapzvq742gd4phmj6ma")))

(define-public crate-plutonit-0.0.0-1 (c (n "plutonit") (v "0.0.0-1") (h "0ifv0cc10nswby48cqvmwf4plq6ps0a7kwy13mp3zwdqfkwj537k")))

