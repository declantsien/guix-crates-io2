(define-module (crates-io pl ut plutchik) #:use-module (crates-io))

(define-public crate-plutchik-0.1.0 (c (n "plutchik") (v "0.1.0") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "0iaz597qwnn6li86g0v7b3265hkwmnfxv09psixrhk8wfm1pzh34")))

(define-public crate-plutchik-0.1.1 (c (n "plutchik") (v "0.1.1") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "05qilfz50s81b6gv3g0qc4npz150z2r9pdbjmhsp65h2ljjpx297")))

(define-public crate-plutchik-0.1.2 (c (n "plutchik") (v "0.1.2") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "13mh967yp67khm5gpcpa1yp9n4qydal2cc8zpv771zamyxyy2cqs")))

(define-public crate-plutchik-0.2.0 (c (n "plutchik") (v "0.2.0") (d (list (d (n "rand") (r "^0.3.11") (d #t) (k 0)))) (h "0zibqi03khv7yzpjmbn1q1s7cavx1ikvrfkr8fxpv6nk94s74kj6")))

