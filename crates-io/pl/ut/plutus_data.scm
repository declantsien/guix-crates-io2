(define-module (crates-io pl ut plutus_data) #:use-module (crates-io))

(define-public crate-plutus_data-0.0.1 (c (n "plutus_data") (v "0.0.1") (d (list (d (n "cardano-multiplatform-lib") (r "^3.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "plutus_data_derive") (r "^0.0.1") (d #t) (k 0)))) (h "1j3yh2759xg27b5pmh9yrp7062wv0dzjb6dqdk6zlclhrf136dfj")))

(define-public crate-plutus_data-0.0.2 (c (n "plutus_data") (v "0.0.2") (d (list (d (n "cardano-multiplatform-lib") (r "^3.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "plutus_data_derive") (r "^0.0.2") (d #t) (k 0)))) (h "1rvw1mr67418ixkq67jyqj5zl56m6saza9rbrr45zln19lhg0yc7")))

(define-public crate-plutus_data-0.0.3 (c (n "plutus_data") (v "0.0.3") (d (list (d (n "cardano-multiplatform-lib") (r "^3.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "plutus_data_derive") (r "^0.0.3") (d #t) (k 0)))) (h "1ylnls387jf8ij4kz5zb17iyj01pg9xzsbry493kamjfz0rxdg64")))

(define-public crate-plutus_data-0.0.4 (c (n "plutus_data") (v "0.0.4") (d (list (d (n "cardano-multiplatform-lib") (r "^3.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "plutus_data_derive") (r "^0.0.4") (d #t) (k 0)))) (h "105wkylbavxlnyqzmhvz58m973wrsbfyiaqvavixvr23lfdlfcvv")))

(define-public crate-plutus_data-0.0.5 (c (n "plutus_data") (v "0.0.5") (d (list (d (n "cardano-multiplatform-lib") (r "^3.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "plutus_data_derive") (r "^0.0.5") (d #t) (k 0)))) (h "0b6d5fnn7ilny7lyvgwlmigrp748pmiqy4jw8gmq2jq5qgbxwb4r")))

(define-public crate-plutus_data-0.0.6 (c (n "plutus_data") (v "0.0.6") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pallas") (r "^0.18.1") (d #t) (k 0)) (d (n "pallas-codec") (r "^0.18.1") (d #t) (k 0)) (d (n "pallas-primitives") (r "^0.18.1") (d #t) (k 0)) (d (n "plutus_data_derive") (r "^0.0.6") (d #t) (k 0)))) (h "127pg392lwi1qms524ld3b4ibxwjn8ksv3p14qz59626pkqp6b1s")))

(define-public crate-plutus_data-0.0.7 (c (n "plutus_data") (v "0.0.7") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pallas") (r "^0.18.1") (d #t) (k 0)) (d (n "pallas-codec") (r "^0.18.1") (d #t) (k 0)) (d (n "pallas-primitives") (r "^0.18.1") (d #t) (k 0)) (d (n "plutus_data_derive") (r "^0.0.7") (d #t) (k 0)))) (h "0zb0xb8n8iw8bfjpg6lj0pkvw5qhvpli98chz9s7lhxxxk8adq4k")))

(define-public crate-plutus_data-0.0.8 (c (n "plutus_data") (v "0.0.8") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pallas") (r "^0.18.1") (d #t) (k 0)) (d (n "pallas-codec") (r "^0.18.1") (d #t) (k 0)) (d (n "pallas-primitives") (r "^0.18.1") (d #t) (k 0)) (d (n "plutus_data_derive") (r "^0.0.7") (d #t) (k 0)))) (h "1ng2s07isgmy4r4cxhx6001qj43dilds3bs2bj2da8qv13lb55ya")))

(define-public crate-plutus_data-0.0.9 (c (n "plutus_data") (v "0.0.9") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pallas") (r "^0.18.1") (d #t) (k 0)) (d (n "pallas-codec") (r "^0.18.1") (d #t) (k 0)) (d (n "pallas-primitives") (r "^0.18.1") (d #t) (k 0)) (d (n "plutus_data_derive") (r "^0.0.8") (d #t) (k 0)))) (h "062448g21j3a0d41ih7fa5k9gswmpdnkfrih6qslizl2ji96knz1")))

(define-public crate-plutus_data-0.0.91 (c (n "plutus_data") (v "0.0.91") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pallas") (r "^0.18.1") (d #t) (k 0)) (d (n "pallas-codec") (r "^0.18.1") (d #t) (k 0)) (d (n "pallas-primitives") (r "^0.18.1") (d #t) (k 0)) (d (n "plutus_data_derive") (r "^0.0.8") (d #t) (k 0)))) (h "15jyxkjq4875rlkykxddg6v8pjy9nc2pvymzia98ijy4jfxfm254")))

(define-public crate-plutus_data-0.0.92 (c (n "plutus_data") (v "0.0.92") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pallas") (r "^0.19.1") (d #t) (k 0)) (d (n "pallas-codec") (r "^0.19.1") (d #t) (k 0)) (d (n "pallas-primitives") (r "^0.19.1") (d #t) (k 0)) (d (n "plutus_data_derive") (r "^0.0.8") (d #t) (k 0)))) (h "0cvnn31l7m43as7sq7qkjkpm5dkh69w3xs7jqy1bzk06vafqpc6c")))

(define-public crate-plutus_data-0.0.93 (c (n "plutus_data") (v "0.0.93") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "pallas") (r "^0.19.1") (d #t) (k 0)) (d (n "pallas-codec") (r "^0.19.1") (d #t) (k 0)) (d (n "pallas-primitives") (r "^0.19.1") (d #t) (k 0)) (d (n "plutus_data_derive") (r "^0.0.81") (d #t) (k 0)))) (h "1syvpyk60b81mazbh0rlhnvc4srywrvlb2fijnc55vf9m6v2nvhv")))

