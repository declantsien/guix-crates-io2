(define-module (crates-io pl ut plutus) #:use-module (crates-io))

(define-public crate-plutus-0.1.0 (c (n "plutus") (v "0.1.0") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "curl") (r "^0.2.11") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.11.13") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)))) (h "1glpf7w5mplhb6irqv7cmc0z7d416paf5krpwgap92d1vga5cxb3")))

