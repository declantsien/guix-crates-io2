(define-module (crates-io pl ut pluto) #:use-module (crates-io))

(define-public crate-pluto-0.0.1 (c (n "pluto") (v "0.0.1") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "iron") (r "^0.2.2") (d #t) (k 0)) (d (n "mount") (r "*") (d #t) (k 0)) (d (n "postgres") (r "^0.9.6") (d #t) (k 0)) (d (n "router") (r "*") (d #t) (k 0)) (d (n "staticfile") (r "*") (d #t) (k 0)))) (h "0iah6awi5isgij0wkviglc08gm9nqp8zvckyjyc7npzyidmaradd")))

(define-public crate-pluto-0.0.2 (c (n "pluto") (v "0.0.2") (d (list (d (n "handlebars-iron") (r "^0.15.1") (d #t) (k 0)) (d (n "iron") (r "^0.3.0") (d #t) (k 0)) (d (n "logger") (r "^0.0.3") (d #t) (k 0)) (d (n "mount") (r "^0.1.0") (d #t) (k 0)) (d (n "postgres") (r "^0.11.0") (d #t) (k 0)) (d (n "router") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)) (d (n "staticfile") (r "^0.2.0") (d #t) (k 0)))) (h "13qjn96x4qhla31xdswk9dps76fvy2m48qslq9c8pmfhw682air4")))

