(define-module (crates-io pl ut plutus_data_derive) #:use-module (crates-io))

(define-public crate-plutus_data_derive-0.0.1 (c (n "plutus_data_derive") (v "0.0.1") (d (list (d (n "cardano-multiplatform-lib") (r "^3.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1vih9rdy9a67ig77nxaldm5g7z5wb7vgwjm92rjzm3hbwswqalkw")))

(define-public crate-plutus_data_derive-0.0.2 (c (n "plutus_data_derive") (v "0.0.2") (d (list (d (n "cardano-multiplatform-lib") (r "^3.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0x718asq7i260l0s3ykjmnn9iqvwjpqgk70pwnngi11hjvw0pxs4")))

(define-public crate-plutus_data_derive-0.0.3 (c (n "plutus_data_derive") (v "0.0.3") (d (list (d (n "cardano-multiplatform-lib") (r "^3.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0brx2mh2n4xgb3mikqshjz64cgg9vp2khz1zsa3qdaig1ks53zbk")))

(define-public crate-plutus_data_derive-0.0.4 (c (n "plutus_data_derive") (v "0.0.4") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0mx1n7ybjiwbqkfqyf45kpg99vi54023pv36n748zvqs6nljhwbd")))

(define-public crate-plutus_data_derive-0.0.5 (c (n "plutus_data_derive") (v "0.0.5") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1gxay1s02r8xn8c2jdrjk6qjdqf5r3w254mv8rynsnv1b45mh8f2")))

(define-public crate-plutus_data_derive-0.0.6 (c (n "plutus_data_derive") (v "0.0.6") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1m2x49jk6lvzig1pqi4mcw838wxac8hm2c1rsbnhpca0mmbkqni9")))

(define-public crate-plutus_data_derive-0.0.7 (c (n "plutus_data_derive") (v "0.0.7") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0hg41v0msadb4waz9ixwx62w6wrafyv1m5c40bizk0na7z280az7")))

(define-public crate-plutus_data_derive-0.0.8 (c (n "plutus_data_derive") (v "0.0.8") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("parsing"))) (d #t) (k 0)))) (h "1ydf7iif0i5pv4yj5p2xsnkryighjj00pxg2rflizrjb4j3nz2d6")))

(define-public crate-plutus_data_derive-0.0.81 (c (n "plutus_data_derive") (v "0.0.81") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0mkpl1awsis3z1jgdk7hran6mjz06jyl3xl5nj8y7mxqwgrq7yrd")))

