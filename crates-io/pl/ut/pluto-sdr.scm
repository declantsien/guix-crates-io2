(define-module (crates-io pl ut pluto-sdr) #:use-module (crates-io))

(define-public crate-pluto-sdr-0.1.0 (c (n "pluto-sdr") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (d #t) (k 2)) (d (n "industrial-io") (r "^0.5.2") (d #t) (k 0)))) (h "16g5z7dkz91xsiydgvb99h1ghi1b43lfjnn6lp4iil00c05nn311")))

(define-public crate-pluto-sdr-0.1.1 (c (n "pluto-sdr") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 2)) (d (n "ctrlc") (r "^3.4.1") (d #t) (k 2)) (d (n "industrial-io") (r "^0.5.2") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 2)))) (h "0ymk9g85fix564s05n52khlny0i93930w1ni78nbyqpiq10jp4wa") (r "1.60.0")))

