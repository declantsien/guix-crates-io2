(define-module (crates-io pl ut plutonium) #:use-module (crates-io))

(define-public crate-plutonium-0.1.0 (c (n "plutonium") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)))) (h "0s4fn2rb5vw3gkdy8fgggd3wm3zk5gdlx1cx98q3zl87j4nvb79n")))

(define-public crate-plutonium-0.2.0 (c (n "plutonium") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1k9i3fwbcy4r4ckbvklffmpbnikp4r97r2h3vdgsxys5wldl8lmr")))

(define-public crate-plutonium-0.2.1 (c (n "plutonium") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "18ahl5x2cwi3z5q9n4smm7wkxz6d0zni8hji693cc47yykjvblcs")))

(define-public crate-plutonium-0.2.2 (c (n "plutonium") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "1j7h1rbq5f5spmn8535r618pigfhb50cdwmnyskhb7vzv083zsi5")))

(define-public crate-plutonium-0.3.0 (c (n "plutonium") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0jc70pcd6bj5crblwcd42daa1rb31ywhf3vz267p3rqwyzw7j6ni")))

(define-public crate-plutonium-0.3.1 (c (n "plutonium") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "1cmcabjm98am481j5xpr0xzm9gm0wmg2l85k3qbri71bhkiqb8k2")))

(define-public crate-plutonium-0.3.1-experimental.0 (c (n "plutonium") (v "0.3.1-experimental.0") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0iwwjjd3di3qh9k1lfyly90mypvhysy3qyass3wgzlppym0bv73p")))

(define-public crate-plutonium-0.3.1-experimental.1 (c (n "plutonium") (v "0.3.1-experimental.1") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "1gc6ykjp5yzym68chj965hjfj8i7hmn8m54bxcyz9wab8acdglrh")))

(define-public crate-plutonium-0.3.1-experimental.2 (c (n "plutonium") (v "0.3.1-experimental.2") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "04pf4yzbhhh5qfwv5qb9ng9iihd1k8qzzfx15sz2bcj59k6phja3")))

(define-public crate-plutonium-0.3.2 (c (n "plutonium") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "16q9k5b9c8svl11x9q7z1qnlvq1m42v43af50nizw96b7n3ifz8l")))

(define-public crate-plutonium-0.4.0-experimental.0 (c (n "plutonium") (v "0.4.0-experimental.0") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0r51jmslqixl06ps94kd7kn743gjdv7jfkccgkmzn1yas1vmc258")))

(define-public crate-plutonium-0.4.0 (c (n "plutonium") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "02q34yf8c1fhw9ay15xd5p1irqvxaix5ygadv46mrn16i07i90yg")))

(define-public crate-plutonium-0.5.0-experimental.0 (c (n "plutonium") (v "0.5.0-experimental.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "199gd4spchfw8gsrpgq0l2mp4vfngwzv2fwvfhz4y03fz1kbys5w")))

(define-public crate-plutonium-0.5.0 (c (n "plutonium") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "168waf1b97d4wpp17j5l590f2bjx0bxan0gk4w0nx5dx39b0xppi")))

(define-public crate-plutonium-0.5.1 (c (n "plutonium") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0d6bfb9acmgjm3aqll2y86kva9lji34yj5084l3fz047128ns4bh")))

(define-public crate-plutonium-0.5.2 (c (n "plutonium") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "syn") (r "^1.0.14") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0qh0drx68lanbhvaf83bfav61cv033x7rwq7vci9mac2sg41zvx1")))

