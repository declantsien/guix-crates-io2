(define-module (crates-io pl is plistt) #:use-module (crates-io))

(define-public crate-plistt-0.1.0 (c (n "plistt") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.1") (f (quote ("env" "derive" "cargo" "wrap_help"))) (d #t) (k 0)) (d (n "plist") (r "^1.4.3") (d #t) (k 0)) (d (n "seek_bufread") (r "^1.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive" "rc" "alloc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (f (quote ("alloc" "raw_value" "unbounded_depth" "preserve_order"))) (d #t) (k 0)))) (h "04znxkawp54n154ni3p2cj2hxp7hm0lkgmzv4lhl65xqsp6cdrmj")))

(define-public crate-plistt-0.1.1 (c (n "plistt") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.1") (f (quote ("env" "derive" "cargo" "wrap_help"))) (d #t) (k 0)) (d (n "plist") (r "^1.4.3") (d #t) (k 0)) (d (n "seek_bufread") (r "^1.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive" "rc" "alloc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (f (quote ("alloc" "raw_value" "unbounded_depth" "preserve_order"))) (d #t) (k 0)))) (h "1bfwh4h4ngj23dzkky54xk5p485lp4aq1dn0f8wx2yfwimvisr5b")))

