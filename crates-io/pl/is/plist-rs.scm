(define-module (crates-io pl is plist-rs) #:use-module (crates-io))

(define-public crate-plist-rs-0.1.0 (c (n "plist-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "cocoa") (r "^0.4") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "objc") (r "^0.2") (d #t) (t "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "1vgs2x7qcfdmsa6i5i0nx9y0w9caw6039r2d13gnk1zz3qf1zp8j") (f (quote (("libplist"))))))

