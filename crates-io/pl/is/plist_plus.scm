(define-module (crates-io pl is plist_plus) #:use-module (crates-io))

(define-public crate-plist_plus-0.1.0 (c (n "plist_plus") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.122") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1nidszxk24y0y7shil83pgj9irwi8hkbmn1nssz56s8whkhi10fn") (f (quote (("static") ("pls-generate") ("dynamic"))))))

(define-public crate-plist_plus-0.2.0 (c (n "plist_plus") (v "0.2.0") (d (list (d (n "autotools") (r "^0.2.5") (d #t) (k 1)) (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.122") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0p4gi6ag7hifinz3z9qk9rfqk10xja165yhvqy2pc2nn7iilcqdi") (f (quote (("vendored" "static") ("static") ("pls-generate") ("dynamic"))))))

(define-public crate-plist_plus-0.2.1 (c (n "plist_plus") (v "0.2.1") (d (list (d (n "autotools") (r "^0.2.5") (d #t) (k 1)) (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.122") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "06m2gczqi2isqvsxr2dcdvmn32zaxp1vw3nk35gis1ivxmljv4kp") (f (quote (("vendored" "static") ("static") ("pls-generate") ("dynamic"))))))

(define-public crate-plist_plus-0.2.2 (c (n "plist_plus") (v "0.2.2") (d (list (d (n "autotools") (r "^0.2.5") (d #t) (k 1)) (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.122") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0w7290qwikalyiz6x4bxb37s955i0yp7dakc656lk59sqycspj1j") (f (quote (("vendored" "static") ("static") ("pls-generate") ("dynamic"))))))

