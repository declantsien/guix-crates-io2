(define-module (crates-io pl is plist-sys) #:use-module (crates-io))

(define-public crate-plist-sys-0.1.1 (c (n "plist-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.31") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1psg83g3a61n1j1mh35378k9zryh9k23fj8420dqw84kyhh2g5vq") (y #t)))

(define-public crate-plist-sys-0.1.2 (c (n "plist-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.31") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "0i98lpklgi676dl91gmh5nr3p1b3g6c5f9zzzcjhgd6wlhcvnzs9")))

(define-public crate-plist-sys-0.1.3 (c (n "plist-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.31") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "0ihwj8325id50p5hawcxyng1dvw2qng9m978irmcng59az9ymq7k")))

(define-public crate-plist-sys-0.1.4 (c (n "plist-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.31") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1xpa8pjdjk95l524llv6p7kspwygxnshfk6rhrk6a9w57bhy0scx")))

(define-public crate-plist-sys-0.1.5 (c (n "plist-sys") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.31") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1icppmr5jzmprxdwq53wvbc6639nfik3pfcrb9w4rraacwzp8p42")))

