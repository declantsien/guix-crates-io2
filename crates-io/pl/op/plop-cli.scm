(define-module (crates-io pl op plop-cli) #:use-module (crates-io))

(define-public crate-plop-cli-0.1.0 (c (n "plop-cli") (v "0.1.0") (d (list (d (n "aws-config") (r "^0.46.0") (d #t) (k 0)) (d (n "aws-sdk-s3") (r "^0.16.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wma3dfl2mwk1mmgi4kyqn8chafh3cqfaqskw9lzlis5gzm40lpi")))

