(define-module (crates-io pl _0 pl_0) #:use-module (crates-io))

(define-public crate-pl_0-0.1.0 (c (n "pl_0") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "project-root") (r "^0.2.2") (d #t) (k 0)))) (h "03wpqg8h2d3qr6rxkkknf0r70f38s5v6929g8399j1fd3ywi1nvs") (f (quote (("default" "debug") ("debug"))))))

(define-public crate-pl_0-0.1.1 (c (n "pl_0") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)) (d (n "project-root") (r "^0.2.2") (d #t) (k 0)))) (h "1a0ghrd94xwg2cpxrch7m73c06kg3qgj05nblr21xlz6qnkyz3ll") (f (quote (("default" "debug") ("debug"))))))

