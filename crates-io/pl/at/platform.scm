(define-module (crates-io pl at platform) #:use-module (crates-io))

(define-public crate-platform-1.0.0 (c (n "platform") (v "1.0.0") (h "0mbdc0id9hry2lwf0kg5wy7pks7ndg0fhcalbch10wainnm28y73")))

(define-public crate-platform-1.0.1 (c (n "platform") (v "1.0.1") (h "1gg9lyyy8sbv3ckpkaic24g8yl4jhn6r89hg9a2sxijqsg88w77k")))

(define-public crate-platform-1.0.2 (c (n "platform") (v "1.0.2") (h "1gz8kqm4ys156bxxqp215l21rpf2w4b73xy4nn25g89w0vw0g29c")))

