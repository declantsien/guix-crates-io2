(define-module (crates-io pl at platinum) #:use-module (crates-io))

(define-public crate-platinum-0.1.0 (c (n "platinum") (v "0.1.0") (h "17qbd76lmimk46yxsqj599q35zjm183m09p207hzyc12mqn21syw") (y #t)))

(define-public crate-platinum-0.0.0 (c (n "platinum") (v "0.0.0") (h "1n84mz38n0a2m89apq1nb1sz5vr8dkva3acvs6j1vsviar9g2p7b")))

