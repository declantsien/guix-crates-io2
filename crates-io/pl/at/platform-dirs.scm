(define-module (crates-io pl at platform-dirs) #:use-module (crates-io))

(define-public crate-platform-dirs-0.1.0 (c (n "platform-dirs") (v "0.1.0") (d (list (d (n "dirs") (r "^2.0.1") (d #t) (k 0)))) (h "0hjbq3dwhdwvbykk1d3qxsc42vihvyrw5lf0xv15b1h1rlndbq4i")))

(define-public crate-platform-dirs-0.1.1 (c (n "platform-dirs") (v "0.1.1") (d (list (d (n "dirs") (r "^2.0.1") (d #t) (k 0)))) (h "0i37hf0a1a4qd9ilr1yhr6170887rshph87gcfax7lgf1yykqz28")))

(define-public crate-platform-dirs-0.1.2 (c (n "platform-dirs") (v "0.1.2") (d (list (d (n "dirs") (r "^2.0.1") (d #t) (k 0)))) (h "1wlkm5380kl5l7c8p0lrfp2vyrif01hy9636qbbvdrjk3xxgamzb")))

(define-public crate-platform-dirs-0.2.0 (c (n "platform-dirs") (v "0.2.0") (d (list (d (n "dirs") (r "^2.0.1") (d #t) (k 0)))) (h "0f5ymhmz1ldwyr6w91haiv4icwh5gb3c4x4355xx5qwp1h6g3rpi")))

(define-public crate-platform-dirs-0.3.0 (c (n "platform-dirs") (v "0.3.0") (d (list (d (n "dirs-next") (r "^1.0.1") (d #t) (k 0)))) (h "0f4ak4yfwy207anjncnnbdza5wb3l99lhimmg1gri4m6q51x1271")))

