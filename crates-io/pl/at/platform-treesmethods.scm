(define-module (crates-io pl at platform-treesmethods) #:use-module (crates-io))

(define-public crate-platform-treesmethods-0.1.0-aplha.0 (c (n "platform-treesmethods") (v "0.1.0-aplha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "platform-num") (r "^0.1.0-aplha.0") (d #t) (k 0)))) (h "1gwjm0zabgp96ld76lpf26hafal7zwd39nyz21mk0llgq5nmvdl2")))

(define-public crate-platform-treesmethods-0.1.0-aplha.1 (c (n "platform-treesmethods") (v "0.1.0-aplha.1") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "platform-num") (r "^0.1.0-aplha.0") (d #t) (k 0)))) (h "1zmy1jv88vclppgh9v3flqbb7qjydacrph6icns0mi7w69sli6hx")))

(define-public crate-platform-treesmethods-0.1.0-aplha.2 (c (n "platform-treesmethods") (v "0.1.0-aplha.2") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "platform-num") (r "^0.1.0-alpha.1") (d #t) (k 0)))) (h "16pfdiilabxwg6qms89irq762fp1cblrsmpszs4zm5y9q9dbsnsk")))

