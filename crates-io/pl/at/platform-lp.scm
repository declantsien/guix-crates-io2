(define-module (crates-io pl at platform-lp) #:use-module (crates-io))

(define-public crate-platform-lp-0.2.0 (c (n "platform-lp") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "173qgxx5ff1dlcf6mkw9rijy0vvxcl1fcyd08s47a6vbpr3m9sgv")))

(define-public crate-platform-lp-0.2.1 (c (n "platform-lp") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "155jhq2gbq2gb5cjmjzdc5s8851v1pv489dxss1s0fx3ya9gphsp")))

(define-public crate-platform-lp-0.2.2 (c (n "platform-lp") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1rnakls8v3rrsqn59n7kv4iy6m95604r9afjggjr7867ggzqqjss")))

