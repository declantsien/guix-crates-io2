(define-module (crates-io pl at platform-info) #:use-module (crates-io))

(define-public crate-platform-info-0.0.1 (c (n "platform-info") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "processthreadsapi" "sysinfoapi" "winbase" "winver"))) (d #t) (k 0)))) (h "1s6p9pq57591qb5aw8ggbci9fnb7mzrhnc3fvrs87af7rmm0gzgj")))

(define-public crate-platform-info-0.1.0 (c (n "platform-info") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "processthreadsapi" "sysinfoapi" "winbase" "winver"))) (d #t) (k 0)))) (h "1lrnfjgd4ka73dknqryyx5xq1l7a7g966wvcgcwgpgw93p99rshn")))

(define-public crate-platform-info-0.2.0 (c (n "platform-info") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "processthreadsapi" "sysinfoapi" "winbase" "winver"))) (d #t) (k 0)))) (h "187jxpbpjy7mmf522s7p6i557vffcdgf6hx1brppwmixw16jqcw4")))

(define-public crate-platform-info-1.0.0 (c (n "platform-info") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "processthreadsapi" "sysinfoapi" "winbase" "winver"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0qsm5wx8mz42sxj78j1n4zjab0zz9xrq99x897ssr7f7a3ynvwlm")))

(define-public crate-platform-info-1.0.1 (c (n "platform-info") (v "1.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "processthreadsapi" "sysinfoapi" "winbase" "winver"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "12qxkpr68pdhc08rb484i4iqnvsn3pi0k6xfbcfskj939asv4y22")))

(define-public crate-platform-info-1.0.2 (c (n "platform-info") (v "1.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "processthreadsapi" "sysinfoapi" "winbase" "winver"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "04mywqa3aapbpvj8zw3vha6gmgbpz99h20a3krny0nkjmv7j6z2f")))

(define-public crate-platform-info-2.0.0 (c (n "platform-info") (v "2.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "processthreadsapi" "sysinfoapi" "winbase" "winver"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1d6pxc4bmi39mfqzp9f5cd7z1drw8fmaq9v9wf8bhcq57kq7k6yc")))

(define-public crate-platform-info-2.0.1 (c (n "platform-info") (v "2.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "processthreadsapi" "sysinfoapi" "winbase" "winver"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1azgk8l41nc6w8kj6qz96c6kjfv732nba4dzia6d8c8km3vw8zc2")))

(define-public crate-platform-info-2.0.2 (c (n "platform-info") (v "2.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "processthreadsapi" "sysinfoapi" "winbase" "winver"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "03rhwsfhdr3sb6fxr0bmf7xav745m132y6vg05jzcfz5c149q9fn")))

(define-public crate-platform-info-2.0.3 (c (n "platform-info") (v "2.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("libloaderapi" "processthreadsapi" "sysinfoapi" "winbase" "winver"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "05w2v06wwz1kwkv6c9bjshchk2vcrpgg061wjzdgwhj6kimk3zym")))

