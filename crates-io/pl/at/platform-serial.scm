(define-module (crates-io pl at platform-serial) #:use-module (crates-io))

(define-public crate-platform-serial-1.0.0 (c (n "platform-serial") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "nb") (r "^1.1.0") (d #t) (k 0)))) (h "1w848h11b5v20555vx47yxvfkd2x12rb2g1w19q18ghk43hd8jkb")))

(define-public crate-platform-serial-1.0.1 (c (n "platform-serial") (v "1.0.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "nb") (r "^0.1.3") (d #t) (k 0)))) (h "0py7jpfa2hda2z47pl02r813wq3vr2sa57xh85yp8gfpd4j7gmiv")))

(define-public crate-platform-serial-1.0.2 (c (n "platform-serial") (v "1.0.2") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "nb") (r "^0.1.3") (d #t) (k 0)))) (h "0ayz3vgbndqwi0hkqjqw0dw4ha3618c4qg309qkl25zh1m9bk5m6")))

