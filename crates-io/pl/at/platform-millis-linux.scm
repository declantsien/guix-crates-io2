(define-module (crates-io pl at platform-millis-linux) #:use-module (crates-io))

(define-public crate-platform-millis-linux-1.0.0 (c (n "platform-millis-linux") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "platform-millis") (r "^1.0.0") (d #t) (k 0)))) (h "1pd1ax6xjyw0z5cscyd46bdcijn906v06na718bcp8xicwjaspw6")))

(define-public crate-platform-millis-linux-2.0.0 (c (n "platform-millis-linux") (v "2.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "platform-millis") (r "^2.0.0") (d #t) (k 0)))) (h "0pi4kcw3yi4ysq9dcq7pm31xzl2ccp1givq4pi81rhkfbr7w52w9")))

