(define-module (crates-io pl at platform-serial-linux) #:use-module (crates-io))

(define-public crate-platform-serial-linux-1.0.0 (c (n "platform-serial-linux") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.3") (d #t) (k 0)) (d (n "platform-serial") (r "^1.0.1") (d #t) (k 0)) (d (n "serial-core") (r "^0.4.0") (d #t) (k 0)) (d (n "serial-embedded-hal") (r "^0.1.2") (d #t) (k 0)))) (h "1i7jds725jgy88yjkc0mb1rg43pg927j48159k5j1l29r0z0kc3q")))

(define-public crate-platform-serial-linux-1.0.1 (c (n "platform-serial-linux") (v "1.0.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.3") (d #t) (k 0)) (d (n "platform-serial") (r "^1.0.1") (d #t) (k 0)) (d (n "serial-core") (r "^0.4.0") (d #t) (k 0)) (d (n "serial-embedded-hal") (r "^0.1.2") (d #t) (k 0)))) (h "0zgx0mwhzlrlqv88wyy49vzjdi2cbbr39z7mynazz3jk5gqbv49d")))

(define-public crate-platform-serial-linux-1.0.2 (c (n "platform-serial-linux") (v "1.0.2") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nb") (r "^0.1.3") (d #t) (k 0)) (d (n "platform-serial") (r "^1.0.1") (d #t) (k 0)) (d (n "serial-core") (r "^0.4.0") (d #t) (k 0)) (d (n "serial-embedded-hal") (r "^0.1.2") (d #t) (k 0)) (d (n "ufmt") (r "^0.2.0") (d #t) (k 0)))) (h "1xi690if1fdpb128h8j50j91fn5srwfhpi804sq5169hg17z90dy")))

