(define-module (crates-io pl at platform-millis) #:use-module (crates-io))

(define-public crate-platform-millis-1.0.0 (c (n "platform-millis") (v "1.0.0") (h "1f4nl9d5ik3ls29vi4mjmyccw4xp7p8skd0j2fwfgd47671sbqy9")))

(define-public crate-platform-millis-2.0.0 (c (n "platform-millis") (v "2.0.0") (h "17vq5ai7fqfj5jry907fhh58r4j1d5jwh230bm9jynm5vvh64i1q")))

