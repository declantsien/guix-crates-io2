(define-module (crates-io pl at platform-data) #:use-module (crates-io))

(define-public crate-platform-data-0.1.0-aplha.0 (c (n "platform-data") (v "0.1.0-aplha.0") (d (list (d (n "memmap2") (r "^0.5.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "platform-mem") (r "^0.1.0-aplha.0") (d #t) (k 0)) (d (n "platform-num") (r "^0.1.0-aplha.0") (d #t) (k 0)) (d (n "platform-treesmethods") (r "^0.1.0-aplha.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0b2jj5jsprhl8ijjcvw6z5v8c2a38p6li068amddjkixlk7ivz71")))

(define-public crate-platform-data-0.1.0-aplha.1 (c (n "platform-data") (v "0.1.0-aplha.1") (d (list (d (n "memmap2") (r "^0.5.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "platform-num") (r "^0.1.0-aplha.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1zs3y67ffkqbxj3qx2irac8va6w2jagz9n49g230jrlrqbq75n2y")))

(define-public crate-platform-data-0.1.0-beta.1 (c (n "platform-data") (v "0.1.0-beta.1") (d (list (d (n "memmap2") (r "^0.5.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "platform-num") (r "^0.1.0-aplha.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0j63nylrsawh7i971ldmmm94w9n5iqh0j5cz12d3ww3c09637wvw")))

(define-public crate-platform-data-0.1.0-beta.3 (c (n "platform-data") (v "0.1.0-beta.3") (d (list (d (n "funty") (r "^2.0.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0xhwrfqjggnr104bk6y15fv5nmbp0jc86kahj74gw8y4cdjdgxaj")))

