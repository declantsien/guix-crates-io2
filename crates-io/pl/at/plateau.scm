(define-module (crates-io pl at plateau) #:use-module (crates-io))

(define-public crate-plateau-0.1.0 (c (n "plateau") (v "0.1.0") (d (list (d (n "itermore") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parquet") (r "^5.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.26") (f (quote ("time"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "0rnrn540cijjh60brv3s2xxp1wnrg9r9fk9vsga6spdp79gwm7yb")))

