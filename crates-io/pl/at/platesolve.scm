(define-module (crates-io pl at platesolve) #:use-module (crates-io))

(define-public crate-platesolve-0.0.0-reserved (c (n "platesolve") (v "0.0.0-reserved") (h "0zif8lfkqcm4jzbkisjbash0pqvsjyl5k3hqpjhkzvy8vrfkdzi9")))

(define-public crate-platesolve-0.0.0-wip (c (n "platesolve") (v "0.0.0-wip") (d (list (d (n "acap") (r "^0.3.0") (d #t) (k 0)) (d (n "stardetect") (r "^0.2.0") (d #t) (k 0)))) (h "1w12sbcaix9gv6zjprlhpiv8q3j2m99sbrmnlaxqic2nigg8dh7k") (y #t)))

(define-public crate-platesolve-0.0.1-wip (c (n "platesolve") (v "0.0.1-wip") (d (list (d (n "acap") (r "^0.3.0") (d #t) (k 0)) (d (n "stardetect") (r "^0.2.0") (d #t) (k 0)))) (h "0jqc793arv9jg8krcyfw902cvdph3x43imvxfrwx3dpv5h5mggrh")))

