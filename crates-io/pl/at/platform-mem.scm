(define-module (crates-io pl at platform-mem) #:use-module (crates-io))

(define-public crate-platform-mem-0.1.0-aplha.0 (c (n "platform-mem") (v "0.1.0-aplha.0") (d (list (d (n "memmap2") (r "^0.5.3") (d #t) (k 0)) (d (n "platform-num") (r "^0.1.0-aplha.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1cfp9qz1hd8fpw6vlwd0g4prl0k1q9vhrchw1jyn2xh01p22iygw")))

(define-public crate-platform-mem-0.1.0-aplha.1 (c (n "platform-mem") (v "0.1.0-aplha.1") (d (list (d (n "memmap2") (r "^0.5.3") (d #t) (k 0)) (d (n "platform-num") (r "^0.1.0-aplha.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "148c3cg37wssj6f2bngr3727x8j18jpmwcd8z7byjzblc4w66jsy") (y #t)))

(define-public crate-platform-mem-0.1.0-aplha.2 (c (n "platform-mem") (v "0.1.0-aplha.2") (d (list (d (n "memmap2") (r "^0.5.3") (d #t) (k 0)) (d (n "platform-num") (r "^0.1.0-aplha.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1wmrmb7abb0hiik7rrzabqm32flvyqpspmis102bh54wjbbpj1b7")))

(define-public crate-platform-mem-0.1.0-aplha.3 (c (n "platform-mem") (v "0.1.0-aplha.3") (d (list (d (n "memmap2") (r "^0.5.3") (d #t) (k 0)) (d (n "platform-num") (r "^0.1.0-aplha.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "05gr3861szhmhqy8kzalzr120lyjjifx0p9lkkwvjrk2p7jgdcn7") (y #t)))

(define-public crate-platform-mem-0.1.0-aplha.4 (c (n "platform-mem") (v "0.1.0-aplha.4") (d (list (d (n "memmap2") (r "^0.5.3") (d #t) (k 0)) (d (n "platform-num") (r "^0.1.0-aplha.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "1z7qkf17d1f0bs5l3nngc755qj9hm14h5wr2d54x2pf8670r7ygs") (y #t)))

(define-public crate-platform-mem-0.1.0-aplha.5 (c (n "platform-mem") (v "0.1.0-aplha.5") (d (list (d (n "memmap2") (r "^0.5.3") (d #t) (k 0)) (d (n "platform-num") (r "^0.1.0-aplha.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "13nz80a665v19qnn9i40nv5qxfwwad85sfcba6fir2ig2475an8h")))

(define-public crate-platform-mem-0.1.0-beta.1 (c (n "platform-mem") (v "0.1.0-beta.1") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "platform-num") (r "^0.1.0-aplha.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "1ajayag9fypm0ifsk6dlsmlqgz7y18ssqfk6g808rr336bz8pkac")))

(define-public crate-platform-mem-0.1.0-pre+beta.2 (c (n "platform-mem") (v "0.1.0-pre+beta.2") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "platform-num") (r "^0.1.0-aplha.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "13sg0frmjknh1s0cn83gx31wllqps5yhlsj1vszi6wjy7d0jhjz7")))

