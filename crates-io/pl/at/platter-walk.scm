(define-module (crates-io pl at platter-walk) #:use-module (crates-io))

(define-public crate-platter-walk-0.1.0 (c (n "platter-walk") (v "0.1.0") (d (list (d (n "btrfs") (r "^1.2.1") (d #t) (k 0)))) (h "0mwbbnfm73pv857q5dsjxfp17gjdryxr9iwf6d8n0dr4mhv4sapa")))

(define-public crate-platter-walk-0.1.2 (c (n "platter-walk") (v "0.1.2") (d (list (d (n "btrfs2") (r "^1.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.24") (d #t) (k 0)) (d (n "mnt") (r "^0.3.0") (d #t) (k 0)))) (h "1ymrdaki5iv98dp48vlzxkkiydbvsbwgwik9n0igvjv9s860qd60")))

(define-public crate-platter-walk-0.1.3 (c (n "platter-walk") (v "0.1.3") (d (list (d (n "btrfs2") (r "^1.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.24") (d #t) (k 0)) (d (n "mnt") (r "^0.3.0") (d #t) (k 0)))) (h "1mg78qg1qdsm7cxdznnp1xmx7dgnydl64kf13j08d0v7y6krmv37")))

