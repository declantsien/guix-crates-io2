(define-module (crates-io pl at platform-switch) #:use-module (crates-io))

(define-public crate-platform-switch-0.1.0 (c (n "platform-switch") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror-core") (r "^1.0") (o #t) (k 0)))) (h "07md85hnkmznp33g638v557isvsgwym8k6g3i2zl2b1pc6xl874h") (f (quote (("thiserror") ("std" "std_error") ("default" "std" "log")))) (s 2) (e (quote (("std_error" "thiserror" "dep:thiserror") ("log" "dep:log") ("defmt" "dep:defmt") ("core_error" "thiserror" "dep:thiserror-core"))))))

