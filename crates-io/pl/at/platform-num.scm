(define-module (crates-io pl at platform-num) #:use-module (crates-io))

(define-public crate-platform-num-0.1.0-aplha.0 (c (n "platform-num") (v "0.1.0-aplha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1zsfj16261faddzl457xqwwdgmg3ds7zk5clv29waabk2kmqyyhj")))

(define-public crate-platform-num-0.1.0-aplha.1 (c (n "platform-num") (v "0.1.0-aplha.1") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1gdpzwj0ybs2bp6kz22y7rk4jsva1l7wdbi354gn32211flxiwv4")))

