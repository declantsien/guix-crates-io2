(define-module (crates-io pl at platforms) #:use-module (crates-io))

(define-public crate-platforms-0.0.1 (c (n "platforms") (v "0.0.1") (h "1l5a93v7gfk7y9h4x01bd15lpqmbaxm0lcblv94f2wfljac3a6dj") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-platforms-0.1.0 (c (n "platforms") (v "0.1.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1sbyxjg9i1x8qlffsfd99jfc8yb4bw4qz1r8jx34frmvshmd6dpq") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-platforms-0.1.1 (c (n "platforms") (v "0.1.1") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0wma39s6f7bf2i2adprlr8c6zp9ds5rhg49bq0zsrjhvf68n6wsq") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-platforms-0.1.2 (c (n "platforms") (v "0.1.2") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1a3qvjniky7lnhbrzaiw148v3w732c456hcbyz3aa8af5aybhgbh") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-platforms-0.1.3 (c (n "platforms") (v "0.1.3") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0zyrrsczrhkjd63cbqm00bncyjdig8jvbm7xkrgzfn800cijknhk") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-platforms-0.1.4 (c (n "platforms") (v "0.1.4") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0xq3rpaqy7s60yrd6f16l8vh7h605lx9l04dcm94c2kndkb8i5gv") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-platforms-0.2.0 (c (n "platforms") (v "0.2.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0v10sdn9vr069wk2k4ywggvr1iqpbl4svanfjkrkmcammkdc1zkc") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-platforms-0.2.1 (c (n "platforms") (v "0.2.1") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "03llamd0bpz0khpza82kbp4mhxw8bwr71r3fvas612iv0fqv5czy") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-platforms-1.0.0 (c (n "platforms") (v "1.0.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "14v0fd3ws0igd7r4wjgminmz8kq38mlhx39wshw2p2mzzf83n794") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-platforms-1.0.1 (c (n "platforms") (v "1.0.1") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1df5ncjbwzxyrlrqq2fpjqgl3ls543nwzgcyz4vcdq72aajg77vv") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-platforms-1.0.2 (c (n "platforms") (v "1.0.2") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0y78aayzavdah2y4mp8144w1v9mh67fdxih4104w6wli3f73ya8v") (f (quote (("std") ("default" "std"))))))

(define-public crate-platforms-1.0.3 (c (n "platforms") (v "1.0.3") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "09f4v99fw1h4cdy78rddifcm19a2nxjs4akpxbrwp0lr6bya6xyc") (f (quote (("std") ("default" "std"))))))

(define-public crate-platforms-1.1.0 (c (n "platforms") (v "1.1.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "09fkncjyjgxljv2wwrywz43k42hsd6175ih74nhc989c5q0l77cq") (f (quote (("std") ("default" "std"))))))

(define-public crate-platforms-2.0.0 (c (n "platforms") (v "2.0.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "152cnf65zzr6vj5xyap1aqp6ajmfqdhlij2x1lx02hhjazryxl78") (f (quote (("std") ("default" "std"))))))

(define-public crate-platforms-3.0.0 (c (n "platforms") (v "3.0.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0cimvcz1yiabjrwy4g86zq8pdkm22mc9lqn3yljifq05158dplc6") (f (quote (("std") ("default" "std"))))))

(define-public crate-platforms-3.0.1 (c (n "platforms") (v "3.0.1") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "01gds28prw5gh3v2b9r618hccqls850r44khgkmwyzszs8zjkv6q") (f (quote (("std") ("default" "std"))))))

(define-public crate-platforms-3.0.2 (c (n "platforms") (v "3.0.2") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0c1ns75i9b9ng6aj2r1nm2qaz84vc1jgvc4slxqvf3lys2pdvmz3") (f (quote (("std") ("default" "std"))))))

(define-public crate-platforms-3.1.1 (c (n "platforms") (v "3.1.1") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1cnxa6ha5v0yflszb94anf91m7hsnr4i7idy1v4rc6yqwfwzwhni") (f (quote (("std") ("default" "std"))))))

(define-public crate-platforms-3.1.2 (c (n "platforms") (v "3.1.2") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1y19ji0r9lk1cpli4ijyylp3nh66nia9abjqm44ywb7h7c2gl0s5") (f (quote (("std") ("default" "std"))))))

(define-public crate-platforms-3.2.0 (c (n "platforms") (v "3.2.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1c6bzwn877aqdbbmyqsl753ycbciwvbdh4lpzijb8vrfb4zsprhl") (f (quote (("std") ("default" "std"))))))

(define-public crate-platforms-3.3.0 (c (n "platforms") (v "3.3.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0k7q6pigmnvgpfasvssb12m2pv3pc94zrhrfg9by3h3wmhyfqvb2") (f (quote (("std") ("default" "std"))))))

(define-public crate-platforms-3.4.0 (c (n "platforms") (v "3.4.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1rzyw9y1v1qnh69smjmbslynw19x01jzji269n7mi1ljcw4d88yv") (f (quote (("std") ("default" "std"))))))

