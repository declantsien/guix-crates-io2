(define-module (crates-io pl ur plurals) #:use-module (crates-io))

(define-public crate-plurals-0.1.0 (c (n "plurals") (v "0.1.0") (h "1iyrm6lhsmdwhxd1mzx1w14l017yamnn734ph44r6pbazsbaxybi")))

(define-public crate-plurals-0.2.0 (c (n "plurals") (v "0.2.0") (h "0hvn4jvgddzd843rw78kzyk3qn22845q48k84p06y2ld7kacxnir")))

(define-public crate-plurals-0.3.0 (c (n "plurals") (v "0.3.0") (h "0lnqkb2dxrdfnahiipa23lkrwx97nz1mbkqad1vj6zbqa3nsgdc4")))

(define-public crate-plurals-0.4.0 (c (n "plurals") (v "0.4.0") (h "14nlk91j1ylwashicppb1qi8z7bm5kjx5g4aijg68hm4ajrsb9p8")))

