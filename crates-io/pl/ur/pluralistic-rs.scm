(define-module (crates-io pl ur pluralistic-rs) #:use-module (crates-io))

(define-public crate-pluralistic-rs-0.1.0 (c (n "pluralistic-rs") (v "0.1.0") (d (list (d (n "arr_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0z5jaq1xrfk46dml5m1mlsrxsnxyxh5xzchn9raz397yf9fvjy6d")))

(define-public crate-pluralistic-rs-0.2.0 (c (n "pluralistic-rs") (v "0.2.0") (d (list (d (n "arr_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1fh1aq79ghrfw0z0wbmf363npvizfj5dzm8gimjvx5pwrb3kv2h6")))

