(define-module (crates-io pl ur pluralize-rs) #:use-module (crates-io))

(define-public crate-pluralize-rs-0.1.0 (c (n "pluralize-rs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "voca_rs") (r "^1.12.0") (d #t) (k 0)))) (h "1vskdkp68kix0s0z6r77s97j5b2l6lg108apmb9hhdzc4shh8g80")))

