(define-module (crates-io pl ur pluralize) #:use-module (crates-io))

(define-public crate-pluralize-0.1.0 (c (n "pluralize") (v "0.1.0") (h "0g2hqkllkfyvbpjk1ijd9npcil6zgf5pqm2n3jnbp7b8mvd7qp92") (f (quote (("std") ("default" "std"))))))

(define-public crate-pluralize-0.2.0 (c (n "pluralize") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)))) (h "1xl7xjb4dxrcvyqqfb367ap49h2nfz7xpl3lz3hraxgm7jd222cn") (f (quote (("Remover") ("Options")))) (y #t)))

(define-public crate-pluralize-0.2.1 (c (n "pluralize") (v "0.2.1") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)))) (h "0qx2ds82bpav0kqz2fpwh9yg9z7fbqjq0fgsar1pn8xy3i4vxk05") (f (quote (("Remover") ("Options"))))))

