(define-module (crates-io pl ur pluralizer-cli) #:use-module (crates-io))

(define-public crate-pluralizer-cli-0.1.0 (c (n "pluralizer-cli") (v "0.1.0") (d (list (d (n "pluralizer") (r "^0.3.2") (d #t) (k 0)))) (h "0489znsgzb7hqilpdi5nsggpmj3sglszibns9wglzw9nw7lgp6kd")))

(define-public crate-pluralizer-cli-0.1.1 (c (n "pluralizer-cli") (v "0.1.1") (d (list (d (n "pluralizer") (r "^0.3.2") (d #t) (k 0)))) (h "0rkvhf12zy6pwh80xasj0mchvpglp0chimwgg2kqyraxbmv5aipq")))

(define-public crate-pluralizer-cli-0.1.2 (c (n "pluralizer-cli") (v "0.1.2") (d (list (d (n "pluralizer") (r "^0.3.2") (d #t) (k 0)))) (h "0xjf4s519pxxhsqvny4ahk4c2lprs7fz8gvrx6fwnh7ksd7g0mbv")))

