(define-module (crates-io pl ur plurid_delog) #:use-module (crates-io))

(define-public crate-plurid_delog-0.0.0 (c (n "plurid_delog") (v "0.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qn27bkjv60qmx4lzgbr6d72whipls6h3yrllw7sn14h33xncxhn")))

