(define-module (crates-io pl ur pluralize_derive) #:use-module (crates-io))

(define-public crate-pluralize_derive-0.1.0 (c (n "pluralize_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "0axwc1gi6i0zdp2989985x73ds4jnijwnz5dvzlxf9wwk8rnpi5r") (y #t)))

(define-public crate-pluralize_derive-0.1.1 (c (n "pluralize_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "1ccjyrpmmgsq89awsicja8sskgmz02gkkir684cq8m2xmahkb7gi")))

