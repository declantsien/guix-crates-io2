(define-module (crates-io pl ur pluralizer) #:use-module (crates-io))

(define-public crate-pluralizer-0.1.0 (c (n "pluralizer") (v "0.1.0") (h "19j0hlllfk4lk1xdqrh2p07iisqvah05fqbpvqn551lj841986v0")))

(define-public crate-pluralizer-0.2.0 (c (n "pluralizer") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1ya1f95ghlrblc4h0w5hxzdnypsr673j5qv8b116cyx33ww9j7l4")))

(define-public crate-pluralizer-0.3.0 (c (n "pluralizer") (v "0.3.0") (d (list (d (n "ctor") (r "^0.1.21") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0fs9ms1dz29lkrknaakdklfl9s4mf5s74gnpk1q1vjq2vr7i40v9")))

(define-public crate-pluralizer-0.3.1 (c (n "pluralizer") (v "0.3.1") (d (list (d (n "ctor") (r "^0.1.21") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0bhv0dmh7fyv4p4bq58v173hs14jy010ijfdkh3cx6x9n5p6lmpp")))

(define-public crate-pluralizer-0.3.2 (c (n "pluralizer") (v "0.3.2") (d (list (d (n "ctor") (r "^0.1.21") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1qcpblf93r1sl7rx2zh0fpbkv4rraq86dzzb9w5mx9zdxj9ggfa8")))

(define-public crate-pluralizer-0.4.0 (c (n "pluralizer") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "136kfwk9w8l9fr1nzlb9sm3j39j1y15rv9kfhihqnyxnjip63r1m")))

