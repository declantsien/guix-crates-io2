(define-module (crates-io pl -l pl-lens-derive) #:use-module (crates-io))

(define-public crate-pl-lens-derive-1.0.0 (c (n "pl-lens-derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bmxymsn0wjzffjbjk9zf0yfwcjc8h5n1bb89lkw4kd3hb5lfzyr")))

(define-public crate-pl-lens-derive-1.0.1 (c (n "pl-lens-derive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ycg3fjxdlabglfmdqi251jf3nyma4s6qaqs6dcc0gkizvqidlvi")))

