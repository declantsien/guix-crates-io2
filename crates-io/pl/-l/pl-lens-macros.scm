(define-module (crates-io pl -l pl-lens-macros) #:use-module (crates-io))

(define-public crate-pl-lens-macros-1.0.0 (c (n "pl-lens-macros") (v "1.0.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "006lgaw49d6nsacn8dkq71iaznpcfyy7j541gs21jskwdl152600")))

(define-public crate-pl-lens-macros-1.0.1 (c (n "pl-lens-macros") (v "1.0.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "170fzxiv4f4mq0yqp6yby3d9ccc37qi9ckyrhpkfv304fdfvkm3q")))

