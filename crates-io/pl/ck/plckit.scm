(define-module (crates-io pl ck plckit) #:use-module (crates-io))

(define-public crate-plckit-0.1.0 (c (n "plckit") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "03xb20k21i0yh6wn939c1ar4xqpvsp9x8gyi7gnappi78bk7gram")))

(define-public crate-plckit-0.1.1 (c (n "plckit") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0wmrgqfx66yw6p20zbxi0pyd3yw4qz664lxvmdni7yswml1xwsai")))

(define-public crate-plckit-0.1.2 (c (n "plckit") (v "0.1.2") (d (list (d (n "bma-ts") (r "^0.1.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "012xv91hj7xfkaki8asr3ybx1d3kvx3gbb0dsfx3mav27p7ldjbv")))

