(define-module (crates-io pl oc ploc-bvh) #:use-module (crates-io))

(define-public crate-ploc-bvh-0.0.1 (c (n "ploc-bvh") (v "0.0.1") (d (list (d (n "bevy_math") (r "^0.12") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("cargo_bench_support"))) (k 2)) (d (n "fastrand") (r "^1.9") (d #t) (k 2)) (d (n "radsort") (r "^0.1") (d #t) (k 0)))) (h "1q81sh021aa11wv3qb06g3xkpmnngl9f8jk91c72kz8lmc7db7jr")))

(define-public crate-ploc-bvh-0.1.0 (c (n "ploc-bvh") (v "0.1.0") (d (list (d (n "bevy_math") (r "^0.13") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("cargo_bench_support"))) (k 2)) (d (n "fastrand") (r "^1.9") (d #t) (k 2)) (d (n "radsort") (r "^0.1") (d #t) (k 0)))) (h "16kjiw6kjq076gsihhrzil3xd8n1lyn7zab26ws9layrd1v0vrww")))

