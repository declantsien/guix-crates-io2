(define-module (crates-io pl ed pledge) #:use-module (crates-io))

(define-public crate-pledge-0.1.0 (c (n "pledge") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1z33rqbdk96svh5b54kwi8jxsyamfz574jsz6c25q7vywy034rg4")))

(define-public crate-pledge-0.2.0 (c (n "pledge") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1kc9058d5apim1vhgvm7amsxbc23114hw8w94y31v0bxnj56v1b4")))

(define-public crate-pledge-0.2.1 (c (n "pledge") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06r547kgp3k67hzpfv0y8r1npkrn4w2dbwm508vz012a7mncqd9y") (y #t)))

(define-public crate-pledge-0.2.2 (c (n "pledge") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1friqxjnwhvc4bk36jmw1zd2rjq6sdhl8yinm3yhiyfbmq790a53")))

(define-public crate-pledge-0.3.0 (c (n "pledge") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1g8vmxqay48xgm567kb9bdyg61biba0hzaj8vr2givy0n3iyd8lf")))

(define-public crate-pledge-0.3.1 (c (n "pledge") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rgbnvl97ks25aanxm680687df6li6y8h3f5mvdw3806rwz8xcg2")))

(define-public crate-pledge-0.4.0 (c (n "pledge") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wd56ybk4cybhbydyfxqdsyhxyqn7sgxnms5mhr1790axd1hmzx4")))

(define-public crate-pledge-0.4.1 (c (n "pledge") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ylk9j84y6y9x0mzwwz9apphzrjbg6gp1ld3s76kr05wnpi5g70r")))

(define-public crate-pledge-0.4.2 (c (n "pledge") (v "0.4.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rc1r71hk0qi4ldbwbhbj6l6d1l4n28dfgf6znvl76kxgd0rj995")))

