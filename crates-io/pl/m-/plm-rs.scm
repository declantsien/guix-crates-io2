(define-module (crates-io pl m- plm-rs) #:use-module (crates-io))

(define-public crate-plm-rs-0.1.4 (c (n "plm-rs") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "~3.0.0-beta.2") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "diesel") (r "^1.4") (f (quote ("sqlite" "chrono"))) (d #t) (k 0)) (d (n "diesel_migrations") (r "^1.4") (d #t) (k 0)) (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "10j8gfq4h8i7qlmjsk5cf73k22yh1zhmgl43r0xwcp99za8k02f0")))

