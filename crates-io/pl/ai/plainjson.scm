(define-module (crates-io pl ai plainjson) #:use-module (crates-io))

(define-public crate-plainjson-0.1.0 (c (n "plainjson") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.1") (d #t) (k 0)))) (h "0fkyslnpf2k9qdaw382x74q9cscn7hmwlf1n0grdhfhnhyzdv6h4")))

(define-public crate-plainjson-0.1.1 (c (n "plainjson") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.1") (d #t) (k 0)))) (h "085pam98wcxbss24wbr198acvpwf14gif19l3bfq678hgf7vgd80")))

(define-public crate-plainjson-0.1.2 (c (n "plainjson") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "unicode_reader") (r "^1.0.1") (d #t) (k 0)))) (h "1vnvykhk9v5a2w14l0mpm8bxqkn95vmp6g26dh5i0cwbap720wiq")))

