(define-module (crates-io pl ai plain_hasher) #:use-module (crates-io))

(define-public crate-plain_hasher-0.1.0 (c (n "plain_hasher") (v "0.1.0") (d (list (d (n "crunchy") (r "^0.1") (d #t) (k 0)))) (h "15frg7m34nb5pm520znvixi3cmny8infrl075ha13xcj763q1bl3")))

(define-public crate-plain_hasher-0.2.0 (c (n "plain_hasher") (v "0.2.0") (d (list (d (n "crunchy") (r "^0.1.6") (d #t) (k 0)))) (h "03x98j8b3hkyb9g7hqv21qck8z5ybn4d4zcvvc5ayjnkn6367ylm") (f (quote (("std") ("default" "std"))))))

(define-public crate-plain_hasher-0.2.2 (c (n "plain_hasher") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "crunchy") (r "^0.2.2") (k 0)))) (h "0aha0xljkjcsmvbmp36ckqd2gddyw6ial2hvlam56shs0vsj874g") (f (quote (("std" "crunchy/std") ("default" "std"))))))

(define-public crate-plain_hasher-0.2.3 (c (n "plain_hasher") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "crunchy") (r "^0.2.2") (k 0)))) (h "1k5lwdi76gz4c47z0bbkxgr5i9y89cclq3yp8cn7rs6x3d4yc68y") (f (quote (("std" "crunchy/std") ("default" "std"))))))

