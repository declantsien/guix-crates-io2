(define-module (crates-io pl ai plain) #:use-module (crates-io))

(define-public crate-plain-0.0.1 (c (n "plain") (v "0.0.1") (h "05r30m5fq9634c4y52j3y6by861f2ag40a2qi0x592n4zjg475kd")))

(define-public crate-plain-0.0.2 (c (n "plain") (v "0.0.2") (h "1bkg9ijslpasy9dqhd43yf94dsqbnkckpmwfl2hxdjwhd5830n2r")))

(define-public crate-plain-0.1.0 (c (n "plain") (v "0.1.0") (h "1l9j84ax4mbfrc5dfdb84yhg83cn7ichbld6ppzn0709xkgqc776")))

(define-public crate-plain-0.2.0 (c (n "plain") (v "0.2.0") (h "01m3a63ailmcm0z9jxm0rx0j0gccr2gs0l9c27j81l51lrl94v8i") (y #t)))

(define-public crate-plain-0.2.1 (c (n "plain") (v "0.2.1") (h "08iix96dsybqd8g3622pr0762s82p640y0nf0dskbvh4awyl4mfs") (y #t)))

(define-public crate-plain-0.2.2 (c (n "plain") (v "0.2.2") (h "08ssdqsx665qc789df923sz7fnxqhq401i167nv6y0dbgrxf1sha") (y #t)))

(define-public crate-plain-0.2.3 (c (n "plain") (v "0.2.3") (h "19n1xbxb4wa7w891268bzf6cbwq4qvdb86bik1z129qb0xnnnndl")))

