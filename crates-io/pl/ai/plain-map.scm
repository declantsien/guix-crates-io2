(define-module (crates-io pl ai plain-map) #:use-module (crates-io))

(define-public crate-plain-map-0.1.0 (c (n "plain-map") (v "0.1.0") (h "02l3b4s054yjhsz0f5wgpkn2js6ginbyqh0k9jl96nwdzxy7mxkh")))

(define-public crate-plain-map-1.0.0 (c (n "plain-map") (v "1.0.0") (d (list (d (n "smallvec") (r "^0.6.10") (d #t) (k 0)))) (h "0i4cmcralif8jzw9wwwz9izz306v1bv6w7mffhf6ax7z13b18arl")))

(define-public crate-plain-map-2.0.0 (c (n "plain-map") (v "2.0.0") (d (list (d (n "smallvec-stableunion") (r "^0.6.10") (d #t) (k 0)))) (h "1pa0wdjcgvvbkm90c2wla4zdq7m9ki03hwas9n7kwpifja7bkjdq")))

