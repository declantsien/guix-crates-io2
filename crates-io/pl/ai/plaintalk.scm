(define-module (crates-io pl ai plaintalk) #:use-module (crates-io))

(define-public crate-plaintalk-0.0.1 (c (n "plaintalk") (v "0.0.1") (h "1vxw4m41qd154v98mffnlk5swcx0xz3k4lzks5rngzk6hmldd76n")))

(define-public crate-plaintalk-0.0.2 (c (n "plaintalk") (v "0.0.2") (d (list (d (n "num") (r "~0.1") (d #t) (k 0)))) (h "0hh5rrmmgzfj5x4688ycjahjvrrngpf9zcjp51rsvahv5y133f9h")))

(define-public crate-plaintalk-0.0.3 (c (n "plaintalk") (v "0.0.3") (d (list (d (n "num") (r "~0.1") (d #t) (k 0)))) (h "1vj6fbqq6h2jaab82s99q6znxbrlza020cd1cw98afa5cqb3zd9f")))

(define-public crate-plaintalk-0.0.4 (c (n "plaintalk") (v "0.0.4") (d (list (d (n "num") (r "~0.1") (d #t) (k 0)))) (h "0ar53aggq688cf9x1wzrdpwmnbsmzm77jdrzin94j5mizgpqlrzi")))

(define-public crate-plaintalk-0.0.5 (c (n "plaintalk") (v "0.0.5") (d (list (d (n "num") (r "~0.1") (d #t) (k 0)))) (h "1mfpqh8q33fwf7y7vdg8qng9ypkg2mssvc4wk7c6gagg699xq8k0")))

(define-public crate-plaintalk-0.0.6 (c (n "plaintalk") (v "0.0.6") (d (list (d (n "num") (r "~0.1") (d #t) (k 0)))) (h "1khfckryi10iw9vsj0zxm0bjr4xby6mz2hicjhc5k3sfhar2drps")))

(define-public crate-plaintalk-0.0.7 (c (n "plaintalk") (v "0.0.7") (d (list (d (n "num") (r "~0.1") (d #t) (k 0)))) (h "0j98435ksl2w8d1amhyqwk7bhl1ih36zxhyyhmivak4pkd7k6vb5")))

(define-public crate-plaintalk-0.0.8 (c (n "plaintalk") (v "0.0.8") (d (list (d (n "num") (r "~0.1") (d #t) (k 0)))) (h "1assbyh13l1cz00a7gg1jchdjdcaww4bjxqhwjiw1bwl07avlp1j")))

(define-public crate-plaintalk-0.0.9 (c (n "plaintalk") (v "0.0.9") (d (list (d (n "num") (r "~0.1") (d #t) (k 0)))) (h "08cfjc86wl3ixspfgmx5fxnmrqc1v5ivmnqr9sp2anxrdw3v33yq")))

(define-public crate-plaintalk-0.0.10 (c (n "plaintalk") (v "0.0.10") (d (list (d (n "num") (r "~0.1") (d #t) (k 0)))) (h "0776n7sbhmqcjjhqil7ccz4hvsl6hi23iahc3vxlc97y1c97s1g5")))

(define-public crate-plaintalk-0.0.11 (c (n "plaintalk") (v "0.0.11") (d (list (d (n "num") (r "~0.1") (d #t) (k 0)))) (h "1jrha0fhny3rg06dc5jn1iaz66s4h81ky76liamgxchzmkyfs9k4")))

(define-public crate-plaintalk-0.0.12 (c (n "plaintalk") (v "0.0.12") (d (list (d (n "num") (r "~0.1") (d #t) (k 0)))) (h "00xzh1kk7dvn5kjhgfc86dlij863xlmj7w724vfj8m902g10n73m")))

(define-public crate-plaintalk-0.0.13 (c (n "plaintalk") (v "0.0.13") (d (list (d (n "num") (r "~0.1") (d #t) (k 0)))) (h "18s84jxrf659g36wcn1wwa0wikqwibvr1qbq8y5wrngsrczirwhk")))

(define-public crate-plaintalk-0.0.13-1 (c (n "plaintalk") (v "0.0.13-1") (d (list (d (n "num") (r "~0.1") (d #t) (k 0)))) (h "1g702s2f8i4wngmflrjr90fs83k23j0lpyv8i97ynkl7fhpfd1ij")))

(define-public crate-plaintalk-0.0.14 (c (n "plaintalk") (v "0.0.14") (d (list (d (n "num") (r "~0.1") (d #t) (k 0)))) (h "1jqxpibszfja9nvgzzyh5w04m9psfg3iral62j669sjv3jbxs2v0")))

(define-public crate-plaintalk-0.0.15 (c (n "plaintalk") (v "0.0.15") (d (list (d (n "num") (r "~0.1") (d #t) (k 0)))) (h "0ga6mdbk5scaq1m8gb3k9x1pbi5l4n453dsw2r542ghn37ynrwr2")))

