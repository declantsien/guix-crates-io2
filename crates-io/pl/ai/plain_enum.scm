(define-module (crates-io pl ai plain_enum) #:use-module (crates-io))

(define-public crate-plain_enum-0.1.0 (c (n "plain_enum") (v "0.1.0") (h "1fcky00h9hj67qcmfj2warqcv3glqk4giqy6jgikvm0zxg89g60j")))

(define-public crate-plain_enum-0.1.1 (c (n "plain_enum") (v "0.1.1") (h "1l5fqjpj6k6yx2pqd147l2mbw3n4as862j8bwc447blc3by5gvic")))

(define-public crate-plain_enum-0.1.2 (c (n "plain_enum") (v "0.1.2") (h "0dyy5l10m93y0ykqnqj46m6kb0mbwx9lc13l4ys4ja0j0jiyypzl")))

(define-public crate-plain_enum-0.1.3 (c (n "plain_enum") (v "0.1.3") (h "1l1dl8xvdmqi9kx2kmmn0qpybwh5dgfww88qvv2nd6mf3r9a0yqn")))

(define-public crate-plain_enum-0.1.4 (c (n "plain_enum") (v "0.1.4") (h "0k5gsnfysw3lzgvz797jkbsip3jnkc65d95x64dgz8f8vfckbnfp")))

(define-public crate-plain_enum-0.2.0 (c (n "plain_enum") (v "0.2.0") (h "0s4rm2cbapnnnr45fwv7wa36f46j70fw1f1kd1hsjbr2llijxicm")))

(define-public crate-plain_enum-0.3.0 (c (n "plain_enum") (v "0.3.0") (h "0vgfgjpxawz1rn3gvfbcp1bz3dlgbns6zk9qq9gyfmj7r1d8wgnp")))

(define-public crate-plain_enum-0.3.1 (c (n "plain_enum") (v "0.3.1") (h "004204gap9ndyfyirkzvlqp386xmi3l8h4ny1dciknxg527ykash")))

(define-public crate-plain_enum-0.4.0 (c (n "plain_enum") (v "0.4.0") (h "02lz355cs1blpqfkrf92fzvzyi9zjhb3r2dzwr07jzdq3yw7bz09")))

(define-public crate-plain_enum-0.5.0 (c (n "plain_enum") (v "0.5.0") (h "0hjbjvi6diy60n2yij16bxpbk0cdpbdzih6wqgv0s93rfirsihcn")))

(define-public crate-plain_enum-0.6.0 (c (n "plain_enum") (v "0.6.0") (h "1n01w081r4s0airfgbcbzh7k2a08b7mx1fcqm4gashl5nf8mridd")))

(define-public crate-plain_enum-0.6.1 (c (n "plain_enum") (v "0.6.1") (h "1y3mrbmdn4w42y9n112zsn7x6j24jv8s7rzbj2g17n67r27vs9zi")))

(define-public crate-plain_enum-0.6.2 (c (n "plain_enum") (v "0.6.2") (h "06agnhzrczf4izlw396g4h76lm56rvxsn4hj2jjqw0v39zkri7kw")))

(define-public crate-plain_enum-0.6.3 (c (n "plain_enum") (v "0.6.3") (h "0mish4wrwpq0mr94bd7j8mv48ir9z3f84h55z44fb4g8nz0ydfm5")))

(define-public crate-plain_enum-0.7.0 (c (n "plain_enum") (v "0.7.0") (h "1rqz2fb1y5dlg57s0nfvdlq77qsa2i29ax40s0441h70rrv24whm")))

(define-public crate-plain_enum-0.7.1 (c (n "plain_enum") (v "0.7.1") (h "0y5s29mcjnzy1v2cic4wv0d26b89fqlmdsns6i5bvb7agwr7h5f6")))

(define-public crate-plain_enum-0.8.0 (c (n "plain_enum") (v "0.8.0") (h "11n2gwk78vxq75wxsq41lr30k4r6nr46ix7ykdxgl8hxvasfiwiq")))

(define-public crate-plain_enum-0.9.0 (c (n "plain_enum") (v "0.9.0") (h "1qjf2b09j1xy71cdfv3nhsimqhgx9pfllfrr91c6a7fdf68ag8ia")))

(define-public crate-plain_enum-0.9.1 (c (n "plain_enum") (v "0.9.1") (h "07f3gdlh99k5mrn6fcyzji9cdcmrdb14rllk137rxvjv9x3wi48v")))

(define-public crate-plain_enum-0.9.2 (c (n "plain_enum") (v "0.9.2") (h "0hdd76kss03j4fckrfgx32lla6j0j9xlskrf45fpb3jrwnck9syz")))

(define-public crate-plain_enum-0.9.3 (c (n "plain_enum") (v "0.9.3") (h "0i7wvwp379fc7r5iy6n28wvdhvkfcfi2zc3zfp21pv6m3vwzk5xw")))

(define-public crate-plain_enum-0.9.4 (c (n "plain_enum") (v "0.9.4") (h "0i5n53145hbvv10zv4w4fd5639jz25jx5mijfniagbrqsq3ykdy8")))

(define-public crate-plain_enum-0.9.5 (c (n "plain_enum") (v "0.9.5") (h "14mm0avhjw7w5ggmwn2dhqcyg1akpqzcmif8fc67px04s5lz9bx5")))

(define-public crate-plain_enum-0.9.6 (c (n "plain_enum") (v "0.9.6") (h "1vvcrxpqb7bh0dpagn7hj1vphxq5i69il6538cfrpxx45agzql81")))

(define-public crate-plain_enum-0.9.7 (c (n "plain_enum") (v "0.9.7") (h "1iw6ylw5br2jqy006rx5jgplhnmiscdbm545z8v4lfv4vd3d55vk")))

(define-public crate-plain_enum-0.9.8 (c (n "plain_enum") (v "0.9.8") (h "1jw8n2ngw9w5i96y1h8ncg12x7lijdrb9hvpkk75z3cfnkd7ps8s")))

(define-public crate-plain_enum-0.9.9 (c (n "plain_enum") (v "0.9.9") (h "16z3k17k41dy5drakfhfzmwgl005gqq88nkn1gfhmwsgzgfnb85b")))

(define-public crate-plain_enum-0.9.10 (c (n "plain_enum") (v "0.9.10") (h "1cf9n8d053sh2c64imgfsshxb5cy6h8jrdqw06p6jnk4fzkjrddm")))

(define-public crate-plain_enum-0.9.11 (c (n "plain_enum") (v "0.9.11") (h "13dmanm2wgg0ca58xz9k30fg98p2d5pw7ia3fm074mazm4zw138d")))

(define-public crate-plain_enum-0.9.12 (c (n "plain_enum") (v "0.9.12") (h "1sy3pl9a5fc1dl74vpv8pjb0p5ys2ck74yzfpg09ynz2a87k6qgi")))

(define-public crate-plain_enum-0.10.0 (c (n "plain_enum") (v "0.10.0") (h "13p1jgd54nbavkmr7djkj2wfrj4vfz0qip991pk25qxpm6zri656")))

(define-public crate-plain_enum-0.11.0 (c (n "plain_enum") (v "0.11.0") (h "0p0gsi8jivm5l73gzv6m5d42v54fr0wrv8iqaqanxwx9r5djck06")))

(define-public crate-plain_enum-0.12.0 (c (n "plain_enum") (v "0.12.0") (h "1va1cvf2r7lnbnv4lwfcyqsp8hn9dlnmsy59nkfy9rn345mxwc7j")))

