(define-module (crates-io pl am plamo) #:use-module (crates-io))

(define-public crate-plamo-0.1.0 (c (n "plamo") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.43.1") (d #t) (k 1)))) (h "18vqqxm0a42aqrjh3k0hm6mmvb3q8vgdibrgwaqx6f6gnq5lgm5c")))

(define-public crate-plamo-0.2.0 (c (n "plamo") (v "0.2.0") (h "08248k4xi4phgvmg5r5gaq2r8acn1rna1jlpbf4q7zmm0chal8xj")))

(define-public crate-plamo-0.3.0 (c (n "plamo") (v "0.3.0") (h "0nqc3jjbp5pnhmpm4pz2nngxy5d18yjl2y9bjrs1cgv786ysxdxa")))

