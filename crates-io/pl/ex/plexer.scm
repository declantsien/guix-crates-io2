(define-module (crates-io pl ex plexer) #:use-module (crates-io))

(define-public crate-plexer-0.1.0 (c (n "plexer") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1c2rp85wa8cvpzs59d2y0apcsgz723l9cx77iqn6ipi7l1sdl168")))

(define-public crate-plexer-0.1.1 (c (n "plexer") (v "0.1.1") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "19pz7ga8j9clydmzk4fwmgd4diygrdcxq94ypiv043ijcjkvwz5r")))

(define-public crate-plexer-0.1.2 (c (n "plexer") (v "0.1.2") (d (list (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "05jis58f1g8wi9mmwh94s314l7xpwwa1p3p4sf9yihs7qn9lalxp")))

