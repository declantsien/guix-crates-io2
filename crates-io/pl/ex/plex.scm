(define-module (crates-io pl ex plex) #:use-module (crates-io))

(define-public crate-plex-0.0.1 (c (n "plex") (v "0.0.1") (d (list (d (n "lalr") (r "^0.0.1") (d #t) (k 0)) (d (n "redfa") (r "^0.0.1") (d #t) (k 0)) (d (n "scoped-tls") (r "^0.1.0") (d #t) (k 0)))) (h "0560h0nggdr9c6i1r2l87bd4lmy33c56zz6zqqqyby5nnjvhvgzp")))

(define-public crate-plex-0.0.2 (c (n "plex") (v "0.0.2") (d (list (d (n "lalr") (r "^0.0.1") (d #t) (k 0)) (d (n "redfa") (r "^0.0.1") (d #t) (k 0)) (d (n "scoped-tls") (r "^0.1.0") (d #t) (k 0)))) (h "0nz9bx452qvni9444k295z3nzpskhr09p9fmcnx3f89yzmfh9sg7")))

(define-public crate-plex-0.0.3 (c (n "plex") (v "0.0.3") (d (list (d (n "lalr") (r "^0.0.1") (d #t) (k 0)) (d (n "redfa") (r "^0.0.2") (d #t) (k 0)))) (h "1vcjpp2q8zdygsvgic9g1c3qd6v77mfdypw6wqh5d31rry11jmgb")))

(define-public crate-plex-0.2.0 (c (n "plex") (v "0.2.0") (d (list (d (n "lalr") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.2.1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "redfa") (r "^0.0.2") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1n1qx3x02m6qgkq2b3lnqc1z9lsabafx084mvk33p9p4fgn4fcmn")))

(define-public crate-plex-0.2.1 (c (n "plex") (v "0.2.1") (d (list (d (n "lalr") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3.6") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "redfa") (r "^0.0.2") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1n67c3pww1jnd8m9k185pq1jnxg6d6dgh9kiv4i6f7rlf8viaj9f")))

(define-public crate-plex-0.2.2 (c (n "plex") (v "0.2.2") (d (list (d (n "lalr") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.3") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6.0") (d #t) (k 0)) (d (n "redfa") (r "^0.0.2") (d #t) (k 0)) (d (n "syn") (r "^0.14.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0m40h5w7yih8ryagwp9v2jdg27cw4h15gyxg0km8149wwlrjxrlf")))

(define-public crate-plex-0.2.3 (c (n "plex") (v "0.2.3") (d (list (d (n "lalr") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.3") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6.0") (d #t) (k 0)) (d (n "redfa") (r "^0.0.2") (d #t) (k 0)) (d (n "syn") (r "^0.14.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0in1f1fj08q6arf6whm8xhzjwdwn5f5pw8aqkrhy42rx9gbrpmsr")))

(define-public crate-plex-0.2.4 (c (n "plex") (v "0.2.4") (d (list (d (n "lalr") (r "^0.0.2") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.3") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6.0") (d #t) (k 0)) (d (n "redfa") (r "^0.0.2") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.14.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1ggi2k1bwwn1jbdp2yrlzyffqs9qvfpsbrvvzlynwm9jksi8m63k") (f (quote (("parser" "lalr") ("lexer" "redfa") ("default" "lexer" "parser"))))))

(define-public crate-plex-0.2.5 (c (n "plex") (v "0.2.5") (d (list (d (n "lalr") (r "^0.0.2") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.3") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6.0") (d #t) (k 0)) (d (n "redfa") (r "^0.0.2") (o #t) (d #t) (k 0)) (d (n "syn") (r "^0.15.36") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1z4303kf5lqr4c6v3kfcjpihlcfgdzydx0y87byyv4hyaws458aa") (f (quote (("parser" "lalr") ("lexer" "redfa") ("default" "lexer" "parser"))))))

(define-public crate-plex-0.3.0 (c (n "plex") (v "0.3.0") (d (list (d (n "lalr") (r "^0.0.2") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "redfa") (r "^0.0.2") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0jzzk5i0142asmzs3jwcs69zbn71039p3nicd4izjmav93379nr3") (f (quote (("parser" "lalr") ("lexer" "redfa") ("default" "lexer" "parser"))))))

