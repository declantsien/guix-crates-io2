(define-module (crates-io pl ex plex-cli) #:use-module (crates-io))

(define-public crate-plex-cli-0.0.1 (c (n "plex-cli") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "plex-api") (r ">=0.0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 0)) (d (n "xflags") (r "^0.2") (d #t) (k 0)))) (h "0vnddazq0gjp07n9fw5vzn1xw4l6afhddgycz6lz33dkha2nn3j9")))

(define-public crate-plex-cli-0.0.2 (c (n "plex-cli") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "plex-api") (r "^0.0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 0)) (d (n "xflags") (r "^0.3") (d #t) (k 0)))) (h "0zvx50mc5adzj0k8myk3j0g7zbyjsdiy5g7vwmacrw2r12xw4p7i")))

(define-public crate-plex-cli-0.0.3 (c (n "plex-cli") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "plex-api") (r ">=0.0.10") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("macros" "rt-multi-thread" "time" "fs"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.7") (f (quote ("compat" "futures-io"))) (d #t) (k 0)) (d (n "xflags") (r "^0.3") (d #t) (k 0)))) (h "0b2av74844q4vwj2i5nbhszi2pid9k89aqbqkza6aa2kch285537")))

