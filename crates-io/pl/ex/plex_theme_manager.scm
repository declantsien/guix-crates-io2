(define-module (crates-io pl ex plex_theme_manager) #:use-module (crates-io))

(define-public crate-plex_theme_manager-0.1.0 (c (n "plex_theme_manager") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kq7swphylwr8jb9an75fqrd09wabyvvcisr1fmi4c1y0vl7ymm7")))

(define-public crate-plex_theme_manager-0.1.1 (c (n "plex_theme_manager") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18glrqhfwhaj912xizzmv5pra57p0g4n36a5p9lmxhpf473717gr")))

(define-public crate-plex_theme_manager-0.1.2 (c (n "plex_theme_manager") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m2g8dni7fq044wba1kqn5xfmf30ns6w64scaq78zsmxlkiknj3q")))

(define-public crate-plex_theme_manager-0.1.3 (c (n "plex_theme_manager") (v "0.1.3") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0672rhvbaxn7p5gqx0977wlj6iabiwdr4jb9ig64lc48agq64ar5")))

