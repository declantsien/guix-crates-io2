(define-module (crates-io pl ct plctag-sys) #:use-module (crates-io))

(define-public crate-plctag-sys-0.1.0 (c (n "plctag-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0rb5qwbx4grbyr3mypzcv3qjrc1cn4gg8zyxhkdwy3i1zwfy5f4v")))

(define-public crate-plctag-sys-0.2.0 (c (n "plctag-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0c34k59mswgmbrbxl1yvk1ciral5iqd5i096752498j6nfnfnc37")))

(define-public crate-plctag-sys-0.2.1 (c (n "plctag-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1izj5cb3mclhfm7b98xqsswi68sf3553293mhggfwyfdq0sk8kb9")))

(define-public crate-plctag-sys-0.3.0 (c (n "plctag-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0s62qwqdvf3ck8hr9kymb2ymbilgrl2srjwpb3266vlymv250sfp")))

(define-public crate-plctag-sys-0.3.1 (c (n "plctag-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1wkbalkicdrcs6jgm41if35qsrqklxwp8aij47f1kppi61vli8yi")))

(define-public crate-plctag-sys-0.2.2 (c (n "plctag-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0bznzmzp4nmi6qbxzq24sh1za2m45hkk47yidv9hs6jl9wqr1i42")))

(define-public crate-plctag-sys-0.3.2 (c (n "plctag-sys") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "040j1492q7a8g1b36k7f57ly0qv255gfg0rs59w75f8pd5500hzf")))

