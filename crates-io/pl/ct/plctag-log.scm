(define-module (crates-io pl ct plctag-log) #:use-module (crates-io))

(define-public crate-plctag-log-0.1.0 (c (n "plctag-log") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plctag-core") (r "^0.1") (d #t) (k 0)))) (h "1dd2q5xx5s1qsdclp88gcry3x19p0cli9pfxmjf0clkr8q44hp05")))

(define-public crate-plctag-log-0.2.0 (c (n "plctag-log") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plctag-core") (r "^0.2") (d #t) (k 0)))) (h "0i8mf94yzvny66srq4da42dqz5p3sih5ci7k3rfb0cfwfd9xqaz3")))

(define-public crate-plctag-log-0.2.1 (c (n "plctag-log") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plctag-core") (r "^0.2") (d #t) (k 0)))) (h "1rd6q627gl5dgmj1qmifkk9f1aizx112dh3qh3l3gm13k0gq493h")))

(define-public crate-plctag-log-0.3.0 (c (n "plctag-log") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "plctag-core") (r "^0.3") (f (quote ("builder"))) (k 0)))) (h "1b42xiifxxy4qxzbbnv72fpb6x4pcbvdds0wvkl6xqgqkkkg6m3c")))

(define-public crate-plctag-log-0.3.1 (c (n "plctag-log") (v "0.3.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plctag-core") (r "^0.4") (f (quote ("builder"))) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 2)))) (h "0n1jgpsn4chqadwz8yhwrg4xbpgmddvl2sb9ivjh9k8851p6pr92")))

