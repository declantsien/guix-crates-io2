(define-module (crates-io pl ct plctag-derive) #:use-module (crates-io))

(define-public crate-plctag-derive-0.1.0 (c (n "plctag-derive") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "18ghm64l43sp2x0xy4lw68qwnp7ys9kij3i3s2v555lk3fdd56nn")))

(define-public crate-plctag-derive-0.2.0 (c (n "plctag-derive") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0gs4rhrr38fy7gx79130fdsjw1vmd4r18wdi5lx04cz3xrgvj3ij")))

(define-public crate-plctag-derive-0.2.1 (c (n "plctag-derive") (v "0.2.1") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0apcyas3g5fcv99is6f41fadh56y8svqp8bviiqgj64yabb18nxc")))

(define-public crate-plctag-derive-0.2.2 (c (n "plctag-derive") (v "0.2.2") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0m9paxl24l8kz9ll7b604j74dl6r6rcxk5zjx4sqpmp0w43f5kl4")))

(define-public crate-plctag-derive-0.3.1 (c (n "plctag-derive") (v "0.3.1") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0piw5srvqhy8ixcmgv4mkp26svnr1rhkwkfkrpxxinbxsy2vsa75")))

