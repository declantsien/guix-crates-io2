(define-module (crates-io pl as plasma-cash-token) #:use-module (crates-io))

(define-public crate-plasma-cash-token-0.1.0 (c (n "plasma-cash-token") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.14") (f (quote ("alloc"))) (k 0)) (d (n "eth-secp256k1") (r "^0.5.7") (d #t) (k 2)) (d (n "ethabi") (r "^8.0") (d #t) (k 2)) (d (n "ethereum-types") (r "^0.6") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "keccak-hash") (r "^0.2") (d #t) (k 2)))) (h "1ll2jqp16lh6kgnfzp6fkpnl029jsw4myckx3ih64bpq0hwx06xg") (f (quote (("std" "bitvec/std") ("default" "std"))))))

