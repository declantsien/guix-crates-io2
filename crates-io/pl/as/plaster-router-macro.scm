(define-module (crates-io pl as plaster-router-macro) #:use-module (crates-io))

(define-public crate-plaster-router-macro-0.1.0 (c (n "plaster-router-macro") (v "0.1.0") (d (list (d (n "plaster") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0nhqi806rmkmmyf7l18k9qid82vw41xlknwv6q7ydksr559xh5qm")))

(define-public crate-plaster-router-macro-0.1.1 (c (n "plaster-router-macro") (v "0.1.1") (d (list (d (n "plaster") (r "^0.2") (d #t) (k 2)) (d (n "plaster-router") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.40") (d #t) (k 2)))) (h "1ywp36d574qmhqhdjjpks0aj0imrvba6zd352pdvzsdv78pn6q17")))

