(define-module (crates-io pl as plaster-forms) #:use-module (crates-io))

(define-public crate-plaster-forms-0.1.0 (c (n "plaster-forms") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plaster") (r "^0.1") (d #t) (k 0)))) (h "0d5ihrhybyhml3k5vgy0mf41mzq4ziyax1qrriwwafbhz51frf7d")))

(define-public crate-plaster-forms-0.1.1 (c (n "plaster-forms") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plaster") (r "^0.1") (d #t) (k 0)))) (h "1w3jp9v9i4a9pfyjcpawjm6f8j1pza86jlfbc7ddypwqqfb18lb5")))

(define-public crate-plaster-forms-0.1.2 (c (n "plaster-forms") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plaster") (r "^0.1") (d #t) (k 0)))) (h "0j3r6a1bnqgwc12ng0h7ypai0jxqmzz1098zf32xkkwg2mbs4992")))

(define-public crate-plaster-forms-0.1.3 (c (n "plaster-forms") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plaster") (r "^0.1") (d #t) (k 0)))) (h "0af6c3alw9711ng56fxxivgi7vah1jdkczbfwcyq21ypfp8x64lw")))

(define-public crate-plaster-forms-0.1.4 (c (n "plaster-forms") (v "0.1.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plaster") (r "^0.1") (d #t) (k 0)))) (h "1394qi0as4h3kh6gfyfzmrvw35rhbdk0sn0dxdvjgqx2d3ylmwc9")))

(define-public crate-plaster-forms-0.1.5 (c (n "plaster-forms") (v "0.1.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plaster") (r "^0.1") (d #t) (k 0)))) (h "0v1irhry4q54zvp3chk90n0f47kq6q2aixx60miyk7kskq35qm4q")))

(define-public crate-plaster-forms-0.1.6 (c (n "plaster-forms") (v "0.1.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plaster") (r "^0.2") (d #t) (k 0)))) (h "1ryc9z1pwxr1accbx751hcxvscfa93aw6yyqckb54ib0n9sm1dbl")))

(define-public crate-plaster-forms-0.1.7 (c (n "plaster-forms") (v "0.1.7") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plaster") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.40") (d #t) (k 2)))) (h "1q46igj683ick0zfrbdxh10s3iqxw731djsw92h55rw77lkzaidx")))

(define-public crate-plaster-forms-0.1.8 (c (n "plaster-forms") (v "0.1.8") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plaster") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.40") (d #t) (k 2)))) (h "0injlcj5xbvn0dw5xprdfpkfy1fi46gb6p5qqk41r0xc46vp89gs")))

(define-public crate-plaster-forms-0.1.9 (c (n "plaster-forms") (v "0.1.9") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plaster") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.40") (d #t) (k 2)))) (h "0sf0n7fb5q7cifcbnipsfwp45nhbjihprsv7qynblqhkkhymnj8d")))

(define-public crate-plaster-forms-0.1.10 (c (n "plaster-forms") (v "0.1.10") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plaster") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.40") (d #t) (k 2)))) (h "0bvlb2c9mghn2bhxjw1nwsq94r395pjmqcirnvfxglrl68nq8c1a")))

(define-public crate-plaster-forms-0.1.11 (c (n "plaster-forms") (v "0.1.11") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plaster") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.40") (d #t) (k 2)))) (h "00am5w81730l5ihxyij4ialgdz8rd742li4rj868xdzfw53ig9l3")))

(define-public crate-plaster-forms-0.1.12 (c (n "plaster-forms") (v "0.1.12") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plaster") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.40") (d #t) (k 2)))) (h "19rmbvwpyfh04jx4s1h0sv3a57jq85qj2vrx9kz0qjkl7hqqr20w")))

(define-public crate-plaster-forms-0.1.13 (c (n "plaster-forms") (v "0.1.13") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plaster") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.40") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("CustomEvent" "Event"))) (o #t) (d #t) (k 0)))) (h "11xxv3wfp0653yfi505gil6pwp76sp54y8j4m87mfxgkkf3l728b") (f (quote (("ionic" "wasm-bindgen" "web-sys" "serde" "serde_derive"))))))

(define-public crate-plaster-forms-0.1.14 (c (n "plaster-forms") (v "0.1.14") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plaster") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.40") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("File"))) (d #t) (k 0)))) (h "14qx8xnxnpm9c9j6mp25829m47pgkdk39vk2ymh4l3b1c75jnn8s") (f (quote (("ionic" "wasm-bindgen" "web-sys/CustomEvent" "web-sys/Event" "web-sys/HtmlInputElement" "serde" "serde_derive"))))))

(define-public crate-plaster-forms-0.1.15 (c (n "plaster-forms") (v "0.1.15") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plaster") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.40") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("File"))) (d #t) (k 0)))) (h "0l35xakazyqxnapzxx60aq7cq4gbqqzmwaix1cjqgr0zyj36bdq6") (f (quote (("ionic" "wasm-bindgen" "web-sys/CustomEvent" "web-sys/Event" "web-sys/HtmlInputElement" "serde" "serde_derive"))))))

(define-public crate-plaster-forms-0.1.16 (c (n "plaster-forms") (v "0.1.16") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plaster") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "= 0.2.40") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("File"))) (d #t) (k 0)))) (h "0vknnjvq9psl1qpzann7vijxv3va0k7nq3s8vpp7vzygibhgaayd") (f (quote (("ionic" "wasm-bindgen" "web-sys/CustomEvent" "web-sys/Event" "web-sys/HtmlInputElement" "serde" "serde_derive"))))))

