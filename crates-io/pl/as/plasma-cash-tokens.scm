(define-module (crates-io pl as plasma-cash-tokens) #:use-module (crates-io))

(define-public crate-plasma-cash-tokens-0.1.0 (c (n "plasma-cash-tokens") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.14") (f (quote ("alloc"))) (k 0)) (d (n "eth-secp256k1") (r "^0.5.7") (d #t) (k 2)) (d (n "ethabi") (r "^8.0") (d #t) (k 2)) (d (n "ethereum-types") (r "^0.6") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "keccak-hash") (r "^0.2") (d #t) (k 2)))) (h "0pp7cy975wm3h43p4z38ydkvf30vxkcnjlvr0qwfkz5hnh50n27j") (f (quote (("std" "bitvec/std") ("default" "std"))))))

