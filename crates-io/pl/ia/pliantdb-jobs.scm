(define-module (crates-io pl ia pliantdb-jobs) #:use-module (crates-io))

(define-public crate-pliantdb-jobs-0.1.0-dev-0 (c (n "pliantdb-jobs") (v "0.1.0-dev-0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0llvia4gdafsxb30d5biqfqfn9li77l8x8nd029j8bd85c9z50vb") (y #t)))

(define-public crate-pliantdb-jobs-0.1.0-dev-1 (c (n "pliantdb-jobs") (v "0.1.0-dev-1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "02k7kivyy4625fxm1a4wk99wcz1cncqv5lxg3iks3h1v3v8qvlky") (y #t)))

(define-public crate-pliantdb-jobs-0.1.0-dev-2 (c (n "pliantdb-jobs") (v "0.1.0-dev-2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1mwnmf2vgnspg92kwls04dhy88v8gvlvp51yp9a0040srz8sk2ap") (y #t)))

(define-public crate-pliantdb-jobs-0.1.0-dev.3 (c (n "pliantdb-jobs") (v "0.1.0-dev.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1854knfpx0gj4rl2c0cl5qrs39sfanny0mj8bjjgs4r1s8ajnd90") (y #t)))

(define-public crate-pliantdb-jobs-0.1.0-dev.4 (c (n "pliantdb-jobs") (v "0.1.0-dev.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1hgv5q55k7pwp4paz0qsfkd8fp83qy212g7565vj8rv43d5q767m") (y #t)))

