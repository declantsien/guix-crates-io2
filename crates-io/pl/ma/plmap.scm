(define-module (crates-io pl ma plmap) #:use-module (crates-io))

(define-public crate-plmap-0.0.1 (c (n "plmap") (v "0.0.1") (d (list (d (n "crossbeam-channel") (r ">0.3") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)))) (h "1g5jf0kf96bqmajrgkia898yc040gs01dw0sz8gdvgvgb1n405x1")))

(define-public crate-plmap-0.2.0 (c (n "plmap") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r ">0.3") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)))) (h "0x9pi107rkparkhw5xmkjmydc8cv6q1nymb4bxb1cg3yvpnmgcd3")))

(define-public crate-plmap-0.2.1 (c (n "plmap") (v "0.2.1") (d (list (d (n "crossbeam-channel") (r ">0.3") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)))) (h "1r15gj64hgb6lkz1isvd8279m3mh89ikzf9x16jq0fyd6b1w75gz")))

(define-public crate-plmap-0.2.2 (c (n "plmap") (v "0.2.2") (d (list (d (n "crossbeam-channel") (r ">0.3") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)))) (h "02mz1xx13swjhlg34nr5w8wxscln36s6ni876vjxwbp1735fqaa0")))

(define-public crate-plmap-0.2.3 (c (n "plmap") (v "0.2.3") (d (list (d (n "crossbeam-channel") (r ">0.3") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)))) (h "0753gdx97gppndykwy4rjzca76c685lkpgyidhvadd8c05him4xr")))

(define-public crate-plmap-0.3.0 (c (n "plmap") (v "0.3.0") (d (list (d (n "crossbeam-channel") (r ">0.3") (d #t) (k 0)) (d (n "crossbeam-utils") (r ">0.3") (d #t) (k 0)))) (h "0k0yx20hjaxqhygv1d69jvi062svbgmmyl2d2afxmsa3f4bshndh")))

