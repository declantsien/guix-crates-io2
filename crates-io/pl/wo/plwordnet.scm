(define-module (crates-io pl wo plwordnet) #:use-module (crates-io))

(define-public crate-plwordnet-0.0.3 (c (n "plwordnet") (v "0.0.3") (d (list (d (n "bstringify") (r "^0.1.2") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28.2") (d #t) (k 0)))) (h "07wbw6fiv77zpzp3h2gldidx6dm43qmpiqbjn4cscfci6yn0bvl3") (y #t)))

(define-public crate-plwordnet-0.0.4 (c (n "plwordnet") (v "0.0.4") (d (list (d (n "bstringify") (r "^0.1.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 2)) (d (n "quick-xml") (r "^0.28.2") (d #t) (k 0)))) (h "1l1g6vb6p514s02qjcfxbwsphyfv60ii8bb2k9k23yf6ari0sg2m")))

