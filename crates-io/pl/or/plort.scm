(define-module (crates-io pl or plort) #:use-module (crates-io))

(define-public crate-plort-0.1.0 (c (n "plort") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)))) (h "062bpxm3k351827w3675y5a3mqr6j4v59za5417p1sfcd56jf368")))

