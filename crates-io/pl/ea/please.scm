(define-module (crates-io pl ea please) #:use-module (crates-io))

(define-public crate-please-0.1.0 (c (n "please") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "diesel") (r "^1.3.3") (f (quote ("postgres" "chrono"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.13.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "1bynxwba76vxq2lba7q15b8273qhrbcvh41x55bfagjnnb0zd6lg")))

(define-public crate-please-0.1.1 (c (n "please") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "diesel") (r "^1.3.3") (f (quote ("postgres" "chrono"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.13.0") (d #t) (k 2)))) (h "0a0y8bl3f2dr69nais0yabrqv75qwnjiixzpqgs0l0lj0hghlwgf")))

(define-public crate-please-0.1.2 (c (n "please") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "diesel") (r "^1.3.3") (f (quote ("postgres" "chrono"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.13.0") (d #t) (k 2)))) (h "0cnqbfij45pwdfkvjw04izl5dfsxh05p7abidm4hqzlvp2v9pjb1")))

(define-public crate-please-0.1.3 (c (n "please") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "diesel") (r "^1.3.3") (f (quote ("postgres" "chrono"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.13.0") (d #t) (k 2)))) (h "11ld04l1cfpkijpkgc48ps6xfm9z2x7g8wbhj4lmrrfzfhy12j4d")))

(define-public crate-please-0.2.0 (c (n "please") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "diesel") (r "^1.3.3") (f (quote ("postgres" "chrono"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.13.0") (d #t) (k 2)))) (h "11aj23j6ywdk8cmfrbx0a2rifn7jqcqmxcz14vymbmqddbga85w6")))

(define-public crate-please-0.2.1 (c (n "please") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "diesel") (r "^1.3.3") (f (quote ("postgres" "chrono"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.13.0") (d #t) (k 2)))) (h "11zwh5l4ss2xw5v95lijr01i75m4yvcgda8f5ghq3arclg69mr5x")))

