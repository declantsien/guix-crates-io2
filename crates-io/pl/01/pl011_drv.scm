(define-module (crates-io pl #{01}# pl011_drv) #:use-module (crates-io))

(define-public crate-pl011_drv-0.1.0 (c (n "pl011_drv") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.3") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "1pay1mvjgr7bsw4yavn4zvzmg7pbnakvxzibw3c35a32fnfy29ja")))

