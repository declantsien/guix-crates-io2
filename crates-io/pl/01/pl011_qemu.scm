(define-module (crates-io pl #{01}# pl011_qemu) #:use-module (crates-io))

(define-public crate-pl011_qemu-0.1.0 (c (n "pl011_qemu") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.3") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "0pwlx8ba21r7lbgaaarqcppq823qym15mmzl0j6qs62h75mzrggi")))

(define-public crate-pl011_qemu-0.2.0 (c (n "pl011_qemu") (v "0.2.0") (d (list (d (n "cortex-m") (r "^0.6.0") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.10") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.3") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "volatile-register") (r "^0.2.0") (d #t) (k 0)))) (h "1dwql43x3p43hkrkn6c7vymmsmr8f8785df3807q5c86s6ixgmfk")))

