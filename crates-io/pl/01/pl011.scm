(define-module (crates-io pl #{01}# pl011) #:use-module (crates-io))

(define-public crate-pl011-0.1.0 (c (n "pl011") (v "0.1.0") (h "04h0ri3x0l0w0gk3l0flfhybjm6dwhi7jw5wkrwlyv02b18dap1g") (r "1.71")))

(define-public crate-pl011-0.1.1 (c (n "pl011") (v "0.1.1") (d (list (d (n "processor") (r "^0.1") (d #t) (k 0)) (d (n "simink_serial") (r "^0.1") (d #t) (k 0)) (d (n "simink_spinlock") (r "^0.1") (d #t) (k 0)) (d (n "tock-registers") (r "^0.8.1") (d #t) (k 0)))) (h "0xi6q84sdwxmghmrwnlh15f97nj4r8rzkcfa1ibq2ykqfjiys4cf") (r "1.71")))

