(define-module (crates-io pl ei pleingres-sql-plugin) #:use-module (crates-io))

(define-public crate-pleingres-sql-plugin-0.1.0 (c (n "pleingres-sql-plugin") (v "0.1.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0m2pi3hsrk193hg4kzkzypkkcrkwmbkx7syz0lc4d545pyv0jc1h")))

(define-public crate-pleingres-sql-plugin-0.2.0 (c (n "pleingres-sql-plugin") (v "0.2.0") (d (list (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0svdi30a1dy2qwh35606w5z7gs5sp3c543ynm1ndyzbphn7v8m9x")))

