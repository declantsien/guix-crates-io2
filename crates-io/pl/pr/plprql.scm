(define-module (crates-io pl pr plprql) #:use-module (crates-io))

(define-public crate-plprql-0.1.0 (c (n "plprql") (v "0.1.0") (d (list (d (n "pgrx") (r "=0.11.3") (d #t) (k 0)) (d (n "pgrx-tests") (r "=0.11.3") (d #t) (k 2)) (d (n "prqlc") (r "^0.11.3") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1r59a1gsyf8hnnb6awwnws40mywz6qcldc5iqdbndbg75gb2b2rc") (f (quote (("pg_test") ("pg16" "pgrx/pg16" "pgrx-tests/pg16") ("pg15" "pgrx/pg15" "pgrx-tests/pg15") ("pg14" "pgrx/pg14" "pgrx-tests/pg14") ("pg13" "pgrx/pg13" "pgrx-tests/pg13") ("pg12" "pgrx/pg12" "pgrx-tests/pg12") ("default" "pg16"))))))

