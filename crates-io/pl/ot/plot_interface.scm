(define-module (crates-io pl ot plot_interface) #:use-module (crates-io))

(define-public crate-plot_interface-0.1.0 (c (n "plot_interface") (v "0.1.0") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "16lk1nx1r5z87l20494mx01qiamvnmf5zyph17h1x379jzdqs2d7") (f (quote (("use_std") ("use_alloc") ("full" "use_std" "use_alloc") ("default" "use_std"))))))

(define-public crate-plot_interface-0.1.1 (c (n "plot_interface") (v "0.1.1") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0rs5z98xgqxgz9w795yrd4bhl06n6vakpkcdh1zkyygnsplcixvj") (f (quote (("use_std") ("use_alloc") ("full" "use_std" "use_alloc") ("default" "use_std"))))))

(define-public crate-plot_interface-0.1.2 (c (n "plot_interface") (v "0.1.2") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)) (d (n "wplot") (r "~0.1") (d #t) (k 0)))) (h "0hhqzjndpm57yz6b8yzg8hbgyk842wm6knnqxf1f374xb084a2f0") (f (quote (("use_std") ("use_alloc") ("full" "use_std" "use_alloc") ("default" "use_std"))))))

(define-public crate-plot_interface-0.1.3 (c (n "plot_interface") (v "0.1.3") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)) (d (n "wplot") (r "~0.1") (d #t) (k 0)))) (h "0bqv77n8vi3g2vicyab8yz909aq0dk5whz6wabhh8vsg47df8yp0") (f (quote (("use_std") ("use_alloc") ("full" "use_std" "use_alloc") ("default" "use_std"))))))

