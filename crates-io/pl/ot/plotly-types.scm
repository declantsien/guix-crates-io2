(define-module (crates-io pl ot plotly-types) #:use-module (crates-io))

(define-public crate-plotly-types-1.54.0-0.1.0 (c (n "plotly-types") (v "1.54.0-0.1.0") (d (list (d (n "convert_case") (r "^0.3") (d #t) (k 1)) (d (n "error-rules") (r "^1.0") (d #t) (k 1)) (d (n "json") (r "^0.12") (d #t) (k 1)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1v8sxa4lgcbc5vg6hcx68gj0bm4s6hvibxaigr3afxbnj3p5s6vy")))

(define-public crate-plotly-types-1.54.0-0.1.1 (c (n "plotly-types") (v "1.54.0-0.1.1") (d (list (d (n "convert_case") (r "^0.3") (d #t) (k 1)) (d (n "error-rules") (r "^1.0") (d #t) (k 1)) (d (n "json") (r "^0.12") (d #t) (k 1)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1rgxgb3xdc8bxqlrw87c9b3vw8px6kmb46bjdli2vivjnk6al2sq")))

(define-public crate-plotly-types-1.54.1-0.1.3 (c (n "plotly-types") (v "1.54.1-0.1.3") (d (list (d (n "convert_case") (r "^0.3") (d #t) (k 1)) (d (n "error-rules") (r "^1.0") (d #t) (k 1)) (d (n "json") (r "^0.12") (d #t) (k 1)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1z88ykpr3k91li6hr2i9fkj4zpnj78gnchp5nlbmffzklfkn99sf")))

(define-public crate-plotly-types-1.54.1-0.1.4 (c (n "plotly-types") (v "1.54.1-0.1.4") (d (list (d (n "convert_case") (r "^0.3") (d #t) (k 1)) (d (n "error-rules") (r "^1.0") (d #t) (k 0)) (d (n "error-rules") (r "^1.0") (d #t) (k 1)) (d (n "json") (r "^0.12") (d #t) (k 1)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0f4cyrnm015isc784iikayflx2gkvmb3h8spnif5a4d7bydplbmg")))

