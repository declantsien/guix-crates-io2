(define-module (crates-io pl ot plotters-solstice) #:use-module (crates-io))

(define-public crate-plotters-solstice-0.1.0 (c (n "plotters-solstice") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plotters-backend") (r "^0.3") (d #t) (k 0)) (d (n "solstice-2d") (r "^0.2.14") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("WebGlRenderingContext"))) (d #t) (k 0)))) (h "1w5w2fsxychixcbzzh3g4xrmsfxmkdhj2hy3l7q1mi43ldlm09vi")))

(define-public crate-plotters-solstice-0.1.1 (c (n "plotters-solstice") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plotters-backend") (r "^0.3") (d #t) (k 0)) (d (n "solstice-2d") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("WebGlRenderingContext"))) (d #t) (k 0)))) (h "1b3pmad1qzflad814iv96kbnqdis6r3c3v0pgciidcd59xi6vvfp")))

(define-public crate-plotters-solstice-0.2.0 (c (n "plotters-solstice") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plotters-backend") (r "^0.3") (d #t) (k 0)) (d (n "solstice-2d") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("WebGlRenderingContext"))) (d #t) (k 0)))) (h "0w01bib9hnglh4a20zg3c4yfd910245ddgzg11m0hmjfps7yg6s8")))

(define-public crate-plotters-solstice-0.2.1 (c (n "plotters-solstice") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "plotters-backend") (r "^0.3") (d #t) (k 0)) (d (n "solstice-2d") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("WebGlRenderingContext"))) (d #t) (k 0)))) (h "13c3n2hxhgmvzqr4lkinpkcarlj34f03739r2kckl5i618jyxbxj")))

