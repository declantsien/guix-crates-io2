(define-module (crates-io pl ot plotsweep) #:use-module (crates-io))

(define-public crate-plotsweep-0.1.0 (c (n "plotsweep") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "colorous") (r "^1.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "084157047y5ayd2h9w0vcg1pxl00ayr0ywckdn3yf4680fkqfw5l")))

