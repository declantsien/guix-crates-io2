(define-module (crates-io pl ot plotters-druid) #:use-module (crates-io))

(define-public crate-plotters-druid-0.1.0 (c (n "plotters-druid") (v "0.1.0") (d (list (d (n "druid") (r "^0.7.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)) (d (n "plotters-piet") (r "0.3.*") (d #t) (k 0)))) (h "1j2csfpngvflrfcpjwcmwpgv1mpwr96flh3zh547ygzvwdajmrw2")))

(define-public crate-plotters-druid-0.2.0 (c (n "plotters-druid") (v "0.2.0") (d (list (d (n "druid") (r "^0.7.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)) (d (n "plotters-piet") (r "^0.3.1") (d #t) (k 0)))) (h "00x0vq7mfpsv0nd4y4fhqpd03kczfl14w6zycxnq6x7x88jm1qk3")))

(define-public crate-plotters-druid-0.2.1 (c (n "plotters-druid") (v "0.2.1") (d (list (d (n "druid") (r "^0.8.2") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)) (d (n "plotters-piet") (r "^0.3.2") (d #t) (k 0)))) (h "1yzjsf7fdfgc0daidf2b22yr84563jq9pgl9vypd6x3d07g902nv") (y #t)))

(define-public crate-plotters-druid-0.3.0 (c (n "plotters-druid") (v "0.3.0") (d (list (d (n "druid") (r "^0.8.2") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)) (d (n "plotters-piet") (r "^0.3.2") (d #t) (k 0)))) (h "0g598gvk9y11b8fz093sm4xfwc9g3bgf7gfw4sfqsiqs9x6p2mv5")))

