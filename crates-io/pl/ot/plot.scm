(define-module (crates-io pl ot plot) #:use-module (crates-io))

(define-public crate-plot-0.1.0 (c (n "plot") (v "0.1.0") (d (list (d (n "clap") (r "^2.20.4") (d #t) (k 0)))) (h "15ana87qniv1ig8nm5zjffd8ikbasv09vmhjgqxyjcmhgr2y599k")))

(define-public crate-plot-0.2.0 (c (n "plot") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "plotlib") (r "^0.1.0") (d #t) (k 0)))) (h "18y3kmmpxcqk9j4wxw16n6zkcvhbqx3ampkr04zp5b2d6byaqs0n")))

