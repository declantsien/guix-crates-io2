(define-module (crates-io pl ot plotters-canvas) #:use-module (crates-io))

(define-public crate-plotters-canvas-0.1.0 (c (n "plotters-canvas") (v "0.1.0") (h "07i0z8wny4p261l3ndmp42xk209mribvj8a2wclg65cclpn3w5nj")))

(define-public crate-plotters-canvas-0.1.1 (c (n "plotters-canvas") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3.32") (d #t) (k 0)) (d (n "plotters-backend") (r "0.0.*") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.55") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.5") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.32") (f (quote ("Document" "DomRect" "Element" "HtmlElement" "Node" "Window" "HtmlCanvasElement" "CanvasRenderingContext2d"))) (d #t) (k 0)))) (h "1gc1vwm291iclkmsywhvhq65ig54yhnvl19212qlkz6ibv2vxfqp")))

(define-public crate-plotters-canvas-0.1.2 (c (n "plotters-canvas") (v "0.1.2") (d (list (d (n "js-sys") (r "^0.3.32") (d #t) (k 0)) (d (n "plotters-backend") (r "0.0.*") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.55") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.5") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.32") (f (quote ("Document" "DomRect" "Element" "HtmlElement" "Node" "Window" "HtmlCanvasElement" "CanvasRenderingContext2d"))) (d #t) (k 0)))) (h "15jxfbgd5y5a72m0sa13s2r4f3fdy6pwz8f02yz2rmj2r88fq8r2")))

(define-public crate-plotters-canvas-0.2.0 (c (n "plotters-canvas") (v "0.2.0") (d (list (d (n "js-sys") (r "^0.3.32") (d #t) (k 0)) (d (n "plotters") (r "0.2.*") (d #t) (k 2)) (d (n "plotters-backend") (r "0.0.*") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.55") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.5") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.32") (f (quote ("Document" "DomRect" "Element" "HtmlElement" "Node" "Window" "HtmlCanvasElement" "CanvasRenderingContext2d"))) (d #t) (k 0)))) (h "1js2gfbdiqi7jl21lygczpzbjr7hv5hp9mlphx56sknv3b0gzcpx")))

(define-public crate-plotters-canvas-0.3.0 (c (n "plotters-canvas") (v "0.3.0") (d (list (d (n "js-sys") (r "^0.3.32") (d #t) (k 0)) (d (n "plotters-backend") (r "0.3.*") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.55") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.5") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.32") (f (quote ("Document" "DomRect" "Element" "HtmlElement" "Node" "Window" "HtmlCanvasElement" "CanvasRenderingContext2d"))) (d #t) (k 0)))) (h "1knaiira030pknca19gcqppbsml5dzjp7gf1yr6zm0a5cnzq52j9")))

