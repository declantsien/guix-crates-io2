(define-module (crates-io pl ot plotly_kaleido) #:use-module (crates-io))

(define-public crate-plotly_kaleido-0.1.0 (c (n "plotly_kaleido") (v "0.1.0") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.6") (d #t) (k 1)))) (h "1nypjbikx9wy9g326rr78g43q9yx3k1sn571c2gbbnw93ks3vjpm")))

(define-public crate-plotly_kaleido-0.2.0 (c (n "plotly_kaleido") (v "0.2.0") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.6") (d #t) (k 1)) (d (n "zip") (r "^0.5.6") (d #t) (k 2)))) (h "1qmsds1zc0i3p6rjzzs7vr872rkzqrxh0v6l368456ghjdfck0yv")))

(define-public crate-plotly_kaleido-0.3.0 (c (n "plotly_kaleido") (v "0.3.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "directories") (r "^3.0.2") (d #t) (k 0)) (d (n "directories") (r "^3.0.2") (d #t) (k 1)) (d (n "dunce") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.13") (d #t) (k 1)) (d (n "zip") (r "^0.5.13") (d #t) (k 2)))) (h "1ndpqzrkj76s83jwx60k88h93fd165rkyjbmw6n8jq82r9r72clb")))

(define-public crate-plotly_kaleido-0.8.3 (c (n "plotly_kaleido") (v "0.8.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 1)) (d (n "dunce") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "zip") (r "^0.5.13") (d #t) (k 1)) (d (n "zip") (r "^0.5.13") (d #t) (k 2)))) (h "1a8jz2qv2as9bn4wqnxi19fd2bjfj9f2fxg9dsbk2hafaybwkc2h")))

(define-public crate-plotly_kaleido-0.8.4 (c (n "plotly_kaleido") (v "0.8.4") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 1)) (d (n "dunce") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "zip") (r "^0.5.13") (d #t) (k 1)) (d (n "zip") (r "^0.5.13") (d #t) (k 2)))) (h "0p38zwbi1n9gyxk5d0pydzdp9v1d6wp5qvcqkpzdp13q1p6x1f20")))

