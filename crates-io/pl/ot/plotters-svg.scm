(define-module (crates-io pl ot plotters-svg) #:use-module (crates-io))

(define-public crate-plotters-svg-0.1.0 (c (n "plotters-svg") (v "0.1.0") (h "0d0dv8af022ggxxsz4hbvdmmn1blq6d15z5afky3w12qib9dx63p")))

(define-public crate-plotters-svg-0.1.1 (c (n "plotters-svg") (v "0.1.1") (d (list (d (n "plotters-backend") (r "0.0.*") (d #t) (k 0)))) (h "0ykwa7m489lagv3gwfd1hbf4yy35cirm4f1nnm14sci3l650a4ab")))

(define-public crate-plotters-svg-0.2.0 (c (n "plotters-svg") (v "0.2.0") (d (list (d (n "plotters-backend") (r "0.2.*") (d #t) (k 0)))) (h "1jz5ngzrr55w2cgaj0c0rhrj4ixicpn83g3j12080ymhfyj4jzd5") (f (quote (("debug"))))))

(define-public crate-plotters-svg-0.3.0 (c (n "plotters-svg") (v "0.3.0") (d (list (d (n "plotters-backend") (r "0.3.*") (d #t) (k 0)))) (h "04fj393irdjb4mavcdwwk1bjnw7gqjp668415a24nq6r7gi052mk") (f (quote (("debug"))))))

(define-public crate-plotters-svg-0.3.1 (c (n "plotters-svg") (v "0.3.1") (d (list (d (n "plotters") (r "^0.3.0") (f (quote ("ttf"))) (k 2)) (d (n "plotters-backend") (r "0.3.*") (d #t) (k 0)))) (h "1aavi3i4jrwydw3i6x0zxs3i3c7gki7jlhg9agff35x5ixisj7sj") (f (quote (("debug"))))))

(define-public crate-plotters-svg-0.3.2 (c (n "plotters-svg") (v "0.3.2") (d (list (d (n "plotters-backend") (r "^0.3.3") (d #t) (k 0)))) (h "05cna9ca7d3abpxmqzg9swd20vsgk1adxsp05vrsy6rx68v8g4g0") (f (quote (("debug"))))))

(define-public crate-plotters-svg-0.3.3 (c (n "plotters-svg") (v "0.3.3") (d (list (d (n "image") (r "^0.24.2") (f (quote ("jpeg" "png" "bmp"))) (o #t) (k 0)) (d (n "plotters-backend") (r "^0.3") (d #t) (k 0)))) (h "0vx5wmm5mxip3fm4l67l3wcvv3jwph4c70zpd3kdmqdab4kiva7r") (f (quote (("debug") ("bitmap_encoder" "image"))))))

(define-public crate-plotters-svg-0.3.5 (c (n "plotters-svg") (v "0.3.5") (d (list (d (n "image") (r "^0.24.2") (f (quote ("jpeg" "png" "bmp"))) (o #t) (k 0)) (d (n "plotters-backend") (r "^0.3.5") (d #t) (k 0)))) (h "1axbw82frs5di4drbyzihr5j35wpy2a75hp3f49p186cjfcd7xiq") (f (quote (("debug") ("bitmap_encoder" "image"))))))

(define-public crate-plotters-svg-0.3.6 (c (n "plotters-svg") (v "0.3.6") (d (list (d (n "image") (r "^0.24.2") (f (quote ("jpeg" "png" "bmp"))) (o #t) (k 0)) (d (n "plotters-backend") (r "^0.3.6") (d #t) (k 0)))) (h "01g74kchmz4lyyv5wbzmfj2i7wi9db9bv122p08f1hyrly30dcw1") (f (quote (("debug") ("bitmap_encoder" "image"))))))

