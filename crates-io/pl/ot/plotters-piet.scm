(define-module (crates-io pl ot plotters-piet) #:use-module (crates-io))

(define-public crate-plotters-piet-0.3.0 (c (n "plotters-piet") (v "0.3.0") (d (list (d (n "piet-common") (r "^0.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)) (d (n "plotters-backend") (r "^0.3") (d #t) (k 0)))) (h "0j4f7955fjlk74l4qsciq1wkq5p6zxsyxjdf7kx7p1p46jxmpwis")))

(define-public crate-plotters-piet-0.3.1 (c (n "plotters-piet") (v "0.3.1") (d (list (d (n "piet-common") (r "^0.3") (d #t) (k 0)) (d (n "piet-common") (r "^0.3") (f (quote ("png"))) (d #t) (k 2)) (d (n "plotters") (r "^0.3") (d #t) (k 2)) (d (n "plotters-backend") (r "^0.3") (d #t) (k 0)))) (h "07cxgzfdkayrsjdsy0vx809igxc89s91acrw6mkq6rh3jr91dwqh")))

(define-public crate-plotters-piet-0.3.2 (c (n "plotters-piet") (v "0.3.2") (d (list (d (n "piet-common") (r "^0.6.1") (d #t) (k 0)) (d (n "plotters-backend") (r "^0.3") (d #t) (k 0)) (d (n "piet-common") (r "^0.6.1") (f (quote ("png"))) (d #t) (k 2)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "0ydxqw8ha3i90508livylk1m9dgbs1i3vnlvk29f5izfgbdznkir")))

