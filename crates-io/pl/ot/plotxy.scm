(define-module (crates-io pl ot plotxy) #:use-module (crates-io))

(define-public crate-plotxy-0.1.0 (c (n "plotxy") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "palette") (r "^0.6.1") (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)) (d (n "polars") (r "^0.25.1") (f (quote ("performant" "lazy"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "07w0a6fpi7h1vm83slkmjs9saz4mcgfmkn48ggyhmbk3l2q9py90")))

