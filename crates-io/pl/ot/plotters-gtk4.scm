(define-module (crates-io pl ot plotters-gtk4) #:use-module (crates-io))

(define-public crate-plotters-gtk4-0.1.0 (c (n "plotters-gtk4") (v "0.1.0") (d (list (d (n "gtk") (r "^0.7") (f (quote ("v4_14"))) (d #t) (k 0) (p "gtk4")) (d (n "pangocairo") (r "^0.18") (d #t) (k 0)) (d (n "plotters-backend") (r "^0.3") (d #t) (k 0)))) (h "10vjqszqzjkcc2f5zjjdpqjgg36pl2bylllyga17s760sgdxygyw")))

(define-public crate-plotters-gtk4-0.2.0 (c (n "plotters-gtk4") (v "0.2.0") (d (list (d (n "gtk") (r "^0.7") (f (quote ("v4_14"))) (d #t) (k 0) (p "gtk4")) (d (n "pangocairo") (r "^0.18") (d #t) (k 0)) (d (n "plotters-backend") (r "^0.3") (d #t) (k 0)))) (h "1lv7wlhinnlj3c66943j2n32yjyc9iclsr2z7ayillqhl55axxdz")))

(define-public crate-plotters-gtk4-0.3.0 (c (n "plotters-gtk4") (v "0.3.0") (d (list (d (n "gtk") (r "^0.7") (f (quote ("v4_14"))) (d #t) (k 0) (p "gtk4")) (d (n "pangocairo") (r "^0.18") (d #t) (k 0)) (d (n "plotters-backend") (r "^0.3") (d #t) (k 0)))) (h "0rcbcdwg69h74vz4fmiliclz80v2ysafjcikh42vn1jmnjidqrfc")))

(define-public crate-plotters-gtk4-0.3.1 (c (n "plotters-gtk4") (v "0.3.1") (d (list (d (n "gtk") (r "^0.7") (f (quote ("v4_14"))) (d #t) (k 0) (p "gtk4")) (d (n "pangocairo") (r "^0.18") (d #t) (k 0)) (d (n "plotters-backend") (r "^0.3") (d #t) (k 0)))) (h "0qh5knbgmbyk086jivn8xs6r4n9q4y7442sqszpmplcwaxkdb79p")))

(define-public crate-plotters-gtk4-0.3.2 (c (n "plotters-gtk4") (v "0.3.2") (d (list (d (n "gtk") (r "^0.7") (f (quote ("v4_14"))) (d #t) (k 0) (p "gtk4")) (d (n "pangocairo") (r "^0.18") (d #t) (k 0)) (d (n "plotters-backend") (r "^0.3") (d #t) (k 0)))) (h "1c7h9psf86c83iwav6iprwmsv8pb3kd7l96laxmq7nsc1kwlcskq")))

(define-public crate-plotters-gtk4-0.3.3 (c (n "plotters-gtk4") (v "0.3.3") (d (list (d (n "gtk") (r "^0.7") (f (quote ("v4_14"))) (d #t) (k 0) (p "gtk4")) (d (n "pangocairo") (r "^0.18") (d #t) (k 0)) (d (n "plotters-backend") (r "^0.3") (d #t) (k 0)))) (h "0cx5pmwx14ng9lgnkhhj2ms942vdmm14kibhcbv3g5xfkxgpj6sc")))

(define-public crate-plotters-gtk4-0.3.4 (c (n "plotters-gtk4") (v "0.3.4") (d (list (d (n "gtk") (r "^0.7") (f (quote ("v4_14"))) (d #t) (k 0) (p "gtk4")) (d (n "pangocairo") (r "^0.18") (d #t) (k 0)) (d (n "plotters-backend") (r "^0.3") (d #t) (k 0)))) (h "0p27ix5fhr74c4vxlf242l2dqvxwl4x3v3n3wkjvmb9d847iy23i")))

(define-public crate-plotters-gtk4-0.3.5 (c (n "plotters-gtk4") (v "0.3.5") (d (list (d (n "gtk") (r "^0.7") (f (quote ("v4_14"))) (d #t) (k 0) (p "gtk4")) (d (n "pangocairo") (r "^0.18") (d #t) (k 0)) (d (n "plotters-backend") (r "^0.3") (d #t) (k 0)))) (h "1g5zdwvn5k2axn013krx3sp7ngylzmcddxqhrs04l4mlx8dvwfl3")))

(define-public crate-plotters-gtk4-0.4.0 (c (n "plotters-gtk4") (v "0.4.0") (d (list (d (n "gtk") (r "^0.8") (f (quote ("v4_14"))) (d #t) (k 0) (p "gtk4")) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "pangocairo") (r "^0.19") (d #t) (k 0)) (d (n "plotters-backend") (r "^0.3") (d #t) (k 0)))) (h "07sqks4zyg8a9fyh3v93l94pw6likkxpl3g0kn3hwkzh234m1vl7")))

