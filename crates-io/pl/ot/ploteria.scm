(define-module (crates-io pl ot ploteria) #:use-module (crates-io))

(define-public crate-ploteria-0.1.0 (c (n "ploteria") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cast") (r "^0.2") (k 0)) (d (n "itertools") (r "^0.9") (k 0)) (d (n "itertools-num") (r "^0.1") (k 2)) (d (n "num-complex") (r "^0.3") (f (quote ("std"))) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "08ilcah1pdwdk7xc707793wkgbmayami62yz3b9hn77lm2vddlif")))

