(define-module (crates-io pl ot plotly_orca) #:use-module (crates-io))

(define-public crate-plotly_orca-0.1.0 (c (n "plotly_orca") (v "0.1.0") (h "18hqh9a3b3ash6gk561sj27kfxmyfnxi7pj2j81az67avid7jwmm") (y #t)))

(define-public crate-plotly_orca-0.2.1 (c (n "plotly_orca") (v "0.2.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0s4v3p3whflwfspc6dqiykwi33zksag30yyawvzmn25nb8l43vrr") (y #t)))

