(define-module (crates-io pl ot plotters-dioxus) #:use-module (crates-io))

(define-public crate-plotters-dioxus-0.1.0 (c (n "plotters-dioxus") (v "0.1.0") (d (list (d (n "dioxus") (r "^0.4.3") (d #t) (k 0)) (d (n "dioxus-desktop") (r "^0.4.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (k 0)) (d (n "plotters-backend") (r "^0.3") (d #t) (k 0)))) (h "1bamqcrf5s5bh8h92lhzqii0wm03nqx07bnynkvj2iyk8lah14lb")))

(define-public crate-plotters-dioxus-0.2.0 (c (n "plotters-dioxus") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "dioxus") (r "^0.4.3") (d #t) (k 0)) (d (n "dioxus-desktop") (r "^0.4.3") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 0)) (d (n "plotters-bitmap") (r "^0.3.3") (d #t) (k 0)))) (h "1l8ar1w6a6g15lgz3smgp240fdb6wbhsagc4z2kqghcvw4ra2wh6")))

(define-public crate-plotters-dioxus-0.2.1 (c (n "plotters-dioxus") (v "0.2.1") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "dioxus") (r "^0.4.3") (d #t) (k 0)) (d (n "dioxus-desktop") (r "^0.4.3") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 0)) (d (n "plotters-bitmap") (r "^0.3.3") (d #t) (k 0)))) (h "08f16miqmyl6cws0gi63g2vrm4vs2ahyvihfz51prx8ss2sxj809")))

(define-public crate-plotters-dioxus-0.2.2 (c (n "plotters-dioxus") (v "0.2.2") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "dioxus") (r "^0.4.3") (d #t) (k 0)) (d (n "dioxus-desktop") (r "^0.4.3") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 0)) (d (n "plotters-bitmap") (r "^0.3.3") (d #t) (k 0)))) (h "0hb2gz010ih0i40kv01km2l9wd08wki29yljslg5d00wxx30fjjm")))

