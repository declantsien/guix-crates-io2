(define-module (crates-io pl ot plotrs) #:use-module (crates-io))

(define-public crate-plotrs-0.1.3 (c (n "plotrs") (v "0.1.3") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^1.0.0") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "font-kit") (r "^0.11") (d #t) (k 0)) (d (n "image") (r "^0.24") (f (quote ("png"))) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "rusttype") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "12vms88ciw3qgz5fdvqym6fyyjf1jxyd6dvkfn6npgjxfki2b5k1") (r "1.60")))

