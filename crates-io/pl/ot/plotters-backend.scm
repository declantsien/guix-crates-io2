(define-module (crates-io pl ot plotters-backend) #:use-module (crates-io))

(define-public crate-plotters-backend-0.0.0-dummy (c (n "plotters-backend") (v "0.0.0-dummy") (h "19cvkd41kifbmximxaww0mcfr1yj6has43ln94g3ppwmdzj13w25")))

(define-public crate-plotters-backend-0.0.1 (c (n "plotters-backend") (v "0.0.1") (h "13dr4nkpcg8mq75c81kf8r32wpyglll9m7bh2ismkhqclc37v8hv")))

(define-public crate-plotters-backend-0.2.0 (c (n "plotters-backend") (v "0.2.0") (h "1s3rrsqd6qqflvnv162fww6kpcf9g8qansm61ynn04jxhkpbj7a1")))

(define-public crate-plotters-backend-0.2.1 (c (n "plotters-backend") (v "0.2.1") (h "025vhgwjxwb928n7y063a9by81qb1qq44s611b0p63r8cl5jh6bx")))

(define-public crate-plotters-backend-0.3.0 (c (n "plotters-backend") (v "0.3.0") (h "1425kzgyimxxq6n85y1wlhmr4y9vy16lxaiwfpga3cqwvk6zyzxh")))

(define-public crate-plotters-backend-0.3.1 (c (n "plotters-backend") (v "0.3.1") (h "0gdxivrhr7zfaqx8x3zhd1cwy9rc0pb202qhx8is7j45fh7y32zx") (y #t)))

(define-public crate-plotters-backend-0.3.2 (c (n "plotters-backend") (v "0.3.2") (h "075ccyz814q46dkr93zz7crj9mmyqgk0w6mmrpyz1sm0ilqig16q")))

(define-public crate-plotters-backend-0.3.3 (c (n "plotters-backend") (v "0.3.3") (h "180iyym8iysfvvgl2rzk6w3ar5sajg0nhbgnn0cl9qvkwxxfb28w") (y #t)))

(define-public crate-plotters-backend-0.3.4 (c (n "plotters-backend") (v "0.3.4") (h "0hl1x8dqrzsjw1vabyw48gzp7g6z8rlyjqjc4b0wvzl1cdhjhchr")))

(define-public crate-plotters-backend-0.3.5 (c (n "plotters-backend") (v "0.3.5") (h "02cn98gsj2i1bwrfsymifmyas1wn2gibdm9mk8w82x9s9n5n4xly")))

(define-public crate-plotters-backend-0.3.6 (c (n "plotters-backend") (v "0.3.6") (h "1dxdxdy31fkaqwp8bq7crbkn7kw7zs6i4mhwx80fjjk3qrifqk21")))

