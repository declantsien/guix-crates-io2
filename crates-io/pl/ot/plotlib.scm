(define-module (crates-io pl ot plotlib) #:use-module (crates-io))

(define-public crate-plotlib-0.1.0 (c (n "plotlib") (v "0.1.0") (h "0yxnh10p7syy8vxhmnp6fy0ljblm8x494a4qdhm0v4ay173dprvd")))

(define-public crate-plotlib-0.2.0 (c (n "plotlib") (v "0.2.0") (d (list (d (n "svg") (r "^0.5.5") (d #t) (k 0)))) (h "1571sh98ihxagzg0l54szg2sp395pr7yvf6zlp07l9k0w1wqj9dx")))

(define-public crate-plotlib-0.3.0 (c (n "plotlib") (v "0.3.0") (d (list (d (n "svg") (r "^0.5.10") (d #t) (k 0)))) (h "18ib13z1by28y54ldmf9s0ryfankcxilghrx5lz53nqjyi6xmylr")))

(define-public crate-plotlib-0.4.0 (c (n "plotlib") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "svg") (r "^0.5.12") (d #t) (k 0)))) (h "1xvhcvbbnvx1vkii5xk3dgdz0czykhrdn1aji6lw780p973hzp1j")))

(define-public crate-plotlib-0.5.0 (c (n "plotlib") (v "0.5.0") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "svg") (r "^0.7.1") (d #t) (k 0)))) (h "02c4p3iijx8f4c1069kgv96k59r76winvw6kxjncffdlm5q8i98s")))

(define-public crate-plotlib-0.5.1 (c (n "plotlib") (v "0.5.1") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "svg") (r "^0.7.1") (d #t) (k 0)))) (h "0hnjl32ja7q37qlxi145vk8hp2cb3i7pdizh4mk0z3bxk17i0qll")))

