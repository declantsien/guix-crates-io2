(define-module (crates-io pl ot plotters-cairo) #:use-module (crates-io))

(define-public crate-plotters-cairo-0.1.0 (c (n "plotters-cairo") (v "0.1.0") (d (list (d (n "cairo-rs") (r "^0.8.0") (f (quote ("ps"))) (d #t) (k 0)) (d (n "plotters-backend") (r "0.0.*") (d #t) (k 0)))) (h "02z9033jl87bvd1qnx52hnp5cfkydh2h5kcs00dwmnjiz3k910xp")))

(define-public crate-plotters-cairo-0.3.0 (c (n "plotters-cairo") (v "0.3.0") (d (list (d (n "cairo-rs") (r "^0.8.0") (f (quote ("ps"))) (d #t) (k 0)) (d (n "plotters-backend") (r "0.3.*") (d #t) (k 0)))) (h "1ppwrc60628i6jq315n0w894i0mnbl8bjvpl9jfnlf0q6yl8qqcq")))

(define-public crate-plotters-cairo-0.3.1 (c (n "plotters-cairo") (v "0.3.1") (d (list (d (n "cairo-rs") (r "^0.9.1") (f (quote ("ps"))) (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (k 2)) (d (n "plotters-backend") (r "^0.3.0") (d #t) (k 0)))) (h "0qc8wl8rlpcmfvix7y8ycavnfvvzaasn3zk7izpxvfrvw5064acj")))

(define-public crate-plotters-cairo-0.3.2 (c (n "plotters-cairo") (v "0.3.2") (d (list (d (n "cairo-rs") (r "^0.15.11") (k 0)) (d (n "cairo-rs") (r "^0.15.1") (f (quote ("ps"))) (k 2)) (d (n "plotters") (r "^0.3") (k 2)) (d (n "plotters-backend") (r "^0.3") (d #t) (k 0)))) (h "1nr5fdjav0frsdgl6clis3845nmmk8n0y8isj50lvbrm08a9ycmd")))

(define-public crate-plotters-cairo-0.4.0 (c (n "plotters-cairo") (v "0.4.0") (d (list (d (n "cairo-rs") (r "^0.17") (k 0)) (d (n "cairo-rs") (r "^0.17") (f (quote ("ps"))) (k 2)) (d (n "plotters") (r "^0.3.5") (k 2)) (d (n "plotters-backend") (r "^0.3.5") (d #t) (k 0)))) (h "1awwnhwk59k8laym9nml2qck7k9fzcfgxzsri3nn1cbqz79rl894")))

(define-public crate-plotters-cairo-0.5.0 (c (n "plotters-cairo") (v "0.5.0") (d (list (d (n "cairo-rs") (r "^0.18") (k 0)) (d (n "cairo-rs") (r "^0.18") (f (quote ("ps"))) (k 2)) (d (n "plotters") (r "^0.3.5") (k 2)) (d (n "plotters-backend") (r "^0.3.5") (d #t) (k 0)))) (h "0ny8sicxgs5356zjfw7396ivzxlfspr2v4qy36y4gi6f87ccawqs")))

(define-public crate-plotters-cairo-0.6.0 (c (n "plotters-cairo") (v "0.6.0") (d (list (d (n "cairo-rs") (r "^0.19") (k 0)) (d (n "cairo-rs") (r "^0.19") (f (quote ("ps"))) (k 2)) (d (n "plotters") (r "^0.3.5") (k 2)) (d (n "plotters-backend") (r "^0.3.5") (d #t) (k 0)))) (h "02842qy1b590rqzrl92znd27qk9wsz9pc6g3r22qh1y0a64mwbp1")))

