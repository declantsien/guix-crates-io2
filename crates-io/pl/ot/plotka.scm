(define-module (crates-io pl ot plotka) #:use-module (crates-io))

(define-public crate-plotka-0.1.0 (c (n "plotka") (v "0.1.0") (d (list (d (n "actix") (r "^0.7") (d #t) (k 0)) (d (n "actix-web") (r "^0.7") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "color-backtrace") (r "^0.1") (d #t) (k 0)) (d (n "csv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wmjsydds7ps3dv99wiypfh8l03sp0lg32q4z2x8rc31s7vddfjw") (y #t)))

