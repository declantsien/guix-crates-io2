(define-module (crates-io pl ot plotapi) #:use-module (crates-io))

(define-public crate-plotapi-0.1.0 (c (n "plotapi") (v "0.1.0") (d (list (d (n "ureq") (r "^1.5.5") (f (quote ("json"))) (d #t) (k 0)))) (h "159bf4d7jrd9m8qvk4xjihkadvhhcgjawbhcp17c541v3d0c525y")))

(define-public crate-plotapi-0.1.1 (c (n "plotapi") (v "0.1.1") (d (list (d (n "ureq") (r "^1.5.5") (f (quote ("json"))) (d #t) (k 0)))) (h "0mybkx60hcwrpvgibabm28jj4y8lxv2y1y4gaglhgblxhrqmdrxj")))

