(define-module (crates-io pl ot plotcap) #:use-module (crates-io))

(define-public crate-plotcap-0.1.0 (c (n "plotcap") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "byte-unit") (r "^4") (d #t) (k 0)) (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "humantime") (r "^2") (d #t) (k 0)) (d (n "pcap-parser") (r "^0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "08zb1m9jzbw3mljd83ymq6i09f1zjzm2q7zwgghv3a0czcvrrb0f")))

(define-public crate-plotcap-0.1.1 (c (n "plotcap") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "byte-unit") (r "^4") (d #t) (k 0)) (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "humantime") (r "^2") (d #t) (k 0)) (d (n "pcap-parser") (r "^0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0h0vxarvigqp15bxfhq2b1gbc7k4ba6862mmzlsk7j3gn0ayf6w8")))

(define-public crate-plotcap-0.1.2 (c (n "plotcap") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "byte-unit") (r "^4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc" "std" "clock"))) (k 0)) (d (n "clap") (r "^3") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "humantime") (r "^2") (d #t) (k 0)) (d (n "pcap-parser") (r "^0.14") (d #t) (k 0)))) (h "0d00fih7915zr2r94wh0pgs3sr22y9rdj8qq0lpy199l115kabrl")))

(define-public crate-plotcap-0.1.3 (c (n "plotcap") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "byte-unit") (r "^4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc" "std" "clock"))) (k 0)) (d (n "clap") (r "^4") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "humantime") (r "^2") (d #t) (k 0)) (d (n "pcap-parser") (r "^0.14") (d #t) (k 0)))) (h "0hf605ffasglkahn3ha871rfqhwh365ldn3jrnxi06g6p6mdlpak")))

