(define-module (crates-io pl ot plotters-fullerene) #:use-module (crates-io))

(define-public crate-plotters-fullerene-0.0.1 (c (n "plotters-fullerene") (v "0.0.1") (d (list (d (n "Fullerene") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 0)) (d (n "polyhedron-faces") (r "^0.3") (d #t) (k 0)))) (h "193q8gpmbn4pv154h24q8zk7iwihil166jca9hgir4fjgk0bgcil") (y #t)))

(define-public crate-plotters-fullerene-0.1.1 (c (n "plotters-fullerene") (v "0.1.1") (d (list (d (n "Fullerene") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 0)) (d (n "polyhedron-faces") (r "^0.3") (d #t) (k 0)))) (h "19m9jrf0b55n0n65x5129gmpf318ik20835vnx8n5nqvfqgbs5bl") (y #t)))

(define-public crate-plotters-fullerene-0.1.4 (c (n "plotters-fullerene") (v "0.1.4") (d (list (d (n "Fullerene") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 0)) (d (n "polyhedron-faces") (r "^0.4") (d #t) (k 0)))) (h "1rjyj0sd60vncn09dldixyrbyrrmf9rrc8vkbs7xw9862rn9cqqp")))

