(define-module (crates-io pl ot plotly_derive) #:use-module (crates-io))

(define-public crate-plotly_derive-0.8.0 (c (n "plotly_derive") (v "0.8.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0axk8s6mippmlrhg87hnxj8whzcdzkxvhsk0fw8ihq5nw3gkmgyr")))

(define-public crate-plotly_derive-0.8.1 (c (n "plotly_derive") (v "0.8.1") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jrxaavivlrgljmnk15v5zgj9mbgsjz65w4avrq4a1iiidmx471i")))

(define-public crate-plotly_derive-0.8.2 (c (n "plotly_derive") (v "0.8.2") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0j5piywcamsby38cmrw5czc6ar0c5in3hl8br0d5s61kj7xf941r")))

(define-public crate-plotly_derive-0.8.3 (c (n "plotly_derive") (v "0.8.3") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0l09vipa4lwl6d71va7n19bjb1hzkci5f00l7v57mz1jd9zz433d")))

(define-public crate-plotly_derive-0.8.4 (c (n "plotly_derive") (v "0.8.4") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qyy5rkfa6c5x66140rbc4ff0dsh9365d0c6xm4ilg68vcfc3z5j")))

