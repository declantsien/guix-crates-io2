(define-module (crates-io pl ot plotters-piston) #:use-module (crates-io))

(define-public crate-plotters-piston-0.3.0 (c (n "plotters-piston") (v "0.3.0") (d (list (d (n "piston_window") (r "^0.112.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.0") (f (quote ("ttf" "all_series"))) (k 2)) (d (n "plotters-backend") (r "^0.3.0") (d #t) (k 0)) (d (n "systemstat") (r "^0.1.5") (d #t) (k 2)))) (h "19y6vi6a6dr48ffrvp2979yvv9yl5h37gx86ix2xsgikjc7zvx85")))

