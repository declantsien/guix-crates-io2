(define-module (crates-io pl ag plagr) #:use-module (crates-io))

(define-public crate-plagr-0.1.0 (c (n "plagr") (v "0.1.0") (d (list (d (n "aes") (r "^0.7.4") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "block-modes") (r "^0.8.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rust-ini") (r "^0.17.0") (d #t) (k 0)))) (h "1a9myiqm1z94wqn8ds1lb0vjaph4i9fn9kncb3r95bj86hpf3ppa")))

