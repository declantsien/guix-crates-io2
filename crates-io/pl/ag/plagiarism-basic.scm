(define-module (crates-io pl ag plagiarism-basic) #:use-module (crates-io))

(define-public crate-plagiarism-basic-0.1.0 (c (n "plagiarism-basic") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strsim") (r "^0.9.3") (d #t) (k 0)))) (h "1p5c93lj03hmdgha447fwk5bhjyxn7h7c8qmbzsnphbyc40fciqm")))

(define-public crate-plagiarism-basic-0.1.1 (c (n "plagiarism-basic") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "strsim") (r "^0.9.3") (d #t) (k 0)))) (h "032274gqmnkdxz4b94cz7w48ir0hgni88azgfkrrpscp85r4ndkd")))

(define-public crate-plagiarism-basic-0.2.0 (c (n "plagiarism-basic") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "handlebars") (r "^2.0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.9.3") (d #t) (k 0)))) (h "0pnwhgviac4yjpfif62fqqasabx6ihh78pkifixca72wbkavwzrc")))

(define-public crate-plagiarism-basic-0.3.0 (c (n "plagiarism-basic") (v "0.3.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "gcollections") (r "^1.4.0") (d #t) (k 0)) (d (n "handlebars") (r "^2.0.2") (d #t) (k 0)) (d (n "intervallum") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.9.3") (d #t) (k 0)))) (h "0iv19fbi0bhpkqdb6n75cglirlgzvkkfviix5z729nqxfz360adk")))

(define-public crate-plagiarism-basic-0.4.0 (c (n "plagiarism-basic") (v "0.4.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "gcollections") (r "^1.4.0") (d #t) (k 0)) (d (n "handlebars") (r "^2.0.2") (d #t) (k 0)) (d (n "intervallum") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strsim") (r "^0.9.3") (d #t) (k 0)))) (h "0c7qw1kl1hrimkzqk6x25rsck6xgk05dj0sw4nicdfqyah79wfd0")))

(define-public crate-plagiarism-basic-0.5.1-unstable (c (n "plagiarism-basic") (v "0.5.1-unstable") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "plagiarismbasic_lib") (r "^0.5.1-unstable") (d #t) (k 0)))) (h "14wzw0781qmllafnjwplpi8my60par5q37lw1z7yj7yjv8x2gj10")))

(define-public crate-plagiarism-basic-0.5.2 (c (n "plagiarism-basic") (v "0.5.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "plagiarismbasic_lib") (r "^0.5.2") (d #t) (k 0)))) (h "00xbshxhyk36a7hj74dkz11mhjfdy18294m3yy3wvkf8rfyiakgz")))

(define-public crate-plagiarism-basic-0.6.1 (c (n "plagiarism-basic") (v "0.6.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "plagiarismbasic_lib") (r "^0.6.1") (d #t) (k 0)))) (h "0z4yj1l3n3nw93zcr39mrn0bh3jrklb24warkard3ngvfa289b2q")))

(define-public crate-plagiarism-basic-0.7.0 (c (n "plagiarism-basic") (v "0.7.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "plagiarismbasic_lib") (r "^0.7.0") (d #t) (k 0)))) (h "12ny776r6hkhr6wk7s1x6xwbs6wspr03yrc3x6yzlpq8gw91sba9")))

(define-public crate-plagiarism-basic-0.7.1 (c (n "plagiarism-basic") (v "0.7.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "plagiarismbasic_lib") (r "^0.7.1") (d #t) (k 0)))) (h "0gn8s83majlspf79swq4cx3z9k1m7xyn9rf2693aw44lga49xxc2")))

(define-public crate-plagiarism-basic-0.8.0 (c (n "plagiarism-basic") (v "0.8.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "plagiarismbasic_lib") (r "^0.8.0") (d #t) (k 0)))) (h "19pwcfgh6rni3d3r1fa2nagbkfxyy71fr1i04pd6byzxiyri6gpv")))

(define-public crate-plagiarism-basic-1.0.0 (c (n "plagiarism-basic") (v "1.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "plagiarismbasic_lib") (r "^1.0.0") (d #t) (k 0)))) (h "0b489dry50y4w19cmpyhks5hzf1wl2lfbbqfn5liwgwgwsqpv7vq")))

(define-public crate-plagiarism-basic-1.1.0 (c (n "plagiarism-basic") (v "1.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "plagiarismbasic_lib") (r "^1.1.0") (d #t) (k 0)))) (h "1gw99gr7w69gsbpqnjcyxp1i8pwcpcigzgcd8cs633j2xcbqsmbg")))

(define-public crate-plagiarism-basic-1.2.0 (c (n "plagiarism-basic") (v "1.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "plagiarismbasic_lib") (r "^1.2.0") (d #t) (k 0)))) (h "199qih8fk9zga0i6l2qj6pi5m7drbwpxbmnfarqx91clwj0gy0dk")))

