(define-module (crates-io pl ag plague) #:use-module (crates-io))

(define-public crate-plague-0.2.0 (c (n "plague") (v "0.2.0") (d (list (d (n "compiletest_rs") (r "^0.0.11") (d #t) (k 2)))) (h "197912kkii2k9hi07w90rqpjv0116fbmgykfbg71fdn8jr6pg2bb")))

(define-public crate-plague-0.3.0 (c (n "plague") (v "0.3.0") (d (list (d (n "compiletest_rs") (r "^0.0.11") (d #t) (k 2)))) (h "06zmxf7y86pflcc9mr1fna36l2vzwpsmd7i1pzsfz727cryvi4mp")))

(define-public crate-plague-0.4.0 (c (n "plague") (v "0.4.0") (d (list (d (n "compiletest_rs") (r "^0.0.11") (d #t) (k 2)))) (h "1580kmnhbp3llmw9kww4mc1p0wlh0gyr5wf16wsvw28ddcdpvgcp")))

(define-public crate-plague-0.5.0 (c (n "plague") (v "0.5.0") (d (list (d (n "compiletest_rs") (r "^0.0.11") (d #t) (k 2)))) (h "0p9y7m1rdw7pqvsyh85invmqxf102814lwz0xzp6lbf1g93r9c2w")))

(define-public crate-plague-0.5.1 (c (n "plague") (v "0.5.1") (d (list (d (n "compiletest_rs") (r "^0.0.11") (d #t) (k 2)))) (h "1xf7yc4lz4dyq79xwfbkf91rlnss2zpwvxfdp1s0gf63736i4i6g")))

(define-public crate-plague-0.5.2 (c (n "plague") (v "0.5.2") (d (list (d (n "compiletest_rs") (r "^0.0.11") (d #t) (k 2)))) (h "0hi8z6w3dvcqf75h6500hk5c7dsn6inbp220vq1x5jhn9ckzx4lm")))

(define-public crate-plague-0.5.3 (c (n "plague") (v "0.5.3") (d (list (d (n "compiletest_rs") (r "^0.0.11") (d #t) (k 2)))) (h "04664rdcy6ld0v3lx9nsr3byznbj3fbyzha47j7bnl2j2l0yykaa")))

(define-public crate-plague-0.6.0 (c (n "plague") (v "0.6.0") (d (list (d (n "compiletest_rs") (r "^0.2.0") (d #t) (k 2)))) (h "132dygikpb1sj5l5ajmwbqpsgappj669p4a07hshijvq8l01l12l")))

(define-public crate-plague-0.6.3 (c (n "plague") (v "0.6.3") (d (list (d (n "compiletest_rs") (r "^0.2.0") (d #t) (k 2)))) (h "0jrl60dnviqkj2ahpd2p44jw8jv23xf282hiy38qch0szliqk28m")))

