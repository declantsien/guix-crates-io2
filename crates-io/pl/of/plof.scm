(define-module (crates-io pl of plof) #:use-module (crates-io))

(define-public crate-plof-0.1.0 (c (n "plof") (v "0.1.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "http-body-util") (r "^0.1.0") (d #t) (k 0)))) (h "1sigb8nqhvbghc2jlhldxafbga4xfddxkgcf5imks7jldr0f51c3")))

