(define-module (crates-io pl yg plyg) #:use-module (crates-io))

(define-public crate-plyg-0.1.0 (c (n "plyg") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1vjmw1ghm84ys5k34igsxvxkddhn1cl7yq0vwnzzyq04p0dbry7z")))

(define-public crate-plyg-0.1.1 (c (n "plyg") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0nw5dhj0l655zjdy3snkmd9gbaqzhj5414kkf4468f3q43rfr3wl")))

