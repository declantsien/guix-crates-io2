(define-module (crates-io pl an plane-split) #:use-module (crates-io))

(define-public crate-plane-split-0.1.0 (c (n "plane-split") (v "0.1.0") (d (list (d (n "euclid") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (k 0)))) (h "1xgsbvmmbmws2fd996dajsgb9n7nxsvicpghhmqhv8fas6qvw16n")))

(define-public crate-plane-split-0.2.0 (c (n "plane-split") (v "0.2.0") (d (list (d (n "binary-space-partition") (r "^0.1") (d #t) (k 0)) (d (n "euclid") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (k 0)))) (h "1fv0xnagbg00casl12d77lb4ki7npag9b297c009r303lp3v4mdb")))

(define-public crate-plane-split-0.2.1 (c (n "plane-split") (v "0.2.1") (d (list (d (n "binary-space-partition") (r "^0.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (k 0)))) (h "0xarwhqm78zjmnr02kzgyjsfhngjdiy2rzdc4xb0jc3nwd01wpm0")))

(define-public crate-plane-split-0.2.2 (c (n "plane-split") (v "0.2.2") (d (list (d (n "binary-space-partition") (r "^0.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (k 0)))) (h "1v4sk8gy51rn2w5qxc4kappj2ak0bvqsfvlngh0z29dicl29rmb0")))

(define-public crate-plane-split-0.3.0 (c (n "plane-split") (v "0.3.0") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (k 0)))) (h "0fnkzm2ardppx22xa9x5pq3hf3vb81i5gpkv6k3dqa77wp4j8dlb")))

(define-public crate-plane-split-0.4.0 (c (n "plane-split") (v "0.4.0") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (k 0)))) (h "0m3dymydzqb9scj1hcaxhksydmmwrqmkh8adb3waj1xzfzpjjsam")))

(define-public crate-plane-split-0.4.1 (c (n "plane-split") (v "0.4.1") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.13") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (k 0)))) (h "0apb6lnirn4ilkxbgj8c5k36fjbqfndjzplw68cf5rw5xw5mn3gh")))

(define-public crate-plane-split-0.5.0 (c (n "plane-split") (v "0.5.0") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.14.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (k 0)))) (h "0426klc94kr43dh55dg9ax1wdpj66fgxdg62irigv20kpbli6k6s")))

(define-public crate-plane-split-0.6.0 (c (n "plane-split") (v "0.6.0") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.15") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (k 0)))) (h "09pslaf5hd722xynz1qibsnnclzh04r4l65nnrnmab55gjlh0y75")))

(define-public crate-plane-split-0.7.0 (c (n "plane-split") (v "0.7.0") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.16") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (k 0)))) (h "1dy1wf5238rk6zk55qirgas7bvsfq2dkwqbmhbcxqb9vab8vibfj")))

(define-public crate-plane-split-0.7.1 (c (n "plane-split") (v "0.7.1") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.16") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (k 0)))) (h "1f5fw1nps90nda6pdb74l09c4z7356ws95df11r8af9qgpfc4vgh")))

(define-public crate-plane-split-0.8.0 (c (n "plane-split") (v "0.8.0") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (k 0)))) (h "18hp9djzija2z4p11kb1vmv9y78y07jrd9czjsy3659s3vhmgib9")))

(define-public crate-plane-split-0.9.1 (c (n "plane-split") (v "0.9.1") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (k 0)))) (h "1bz40c5l8p954yzx5yag52giyy6aysl9r9p7vrh9b7agbd4bhybh")))

(define-public crate-plane-split-0.10.0 (c (n "plane-split") (v "0.10.0") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.18") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (k 0)))) (h "1321rx3xmynjrsrbkb2zf9rbcas94zapakvipxghi2ddmcm3h53f")))

(define-public crate-plane-split-0.11.0 (c (n "plane-split") (v "0.11.0") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.1.37") (k 0)))) (h "1j17v95dmdqwf29hmb54v459r376xhcdiwa3vg0p342vkby6hz8x")))

(define-public crate-plane-split-0.12.0 (c (n "plane-split") (v "0.12.0") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0rlbki84dfrgw6d3amq725sswbmv6nhdyj3rx5r8d0ng4mi7d87h")))

(define-public crate-plane-split-0.12.1 (c (n "plane-split") (v "0.12.1") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1fxixnb8gw06zjl3jsk8h7rdj95806jdg8p951lfnw0xwg4lyfpz")))

(define-public crate-plane-split-0.13.0 (c (n "plane-split") (v "0.13.0") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1f51h3drf0a9qsnslyl57wr3f165yng8dz6xplvi7zhmigrndmv4")))

(define-public crate-plane-split-0.13.1 (c (n "plane-split") (v "0.13.1") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "040lzasl01dbvhzh4lzv54045ky5vp8pqj2a0341mhwpf6zvj6nd")))

(define-public crate-plane-split-0.13.2 (c (n "plane-split") (v "0.13.2") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1nnyp5hdssw6g2frjff2kagyhdy7yclryzg86r4rq46jydqxnlnj")))

(define-public crate-plane-split-0.13.3 (c (n "plane-split") (v "0.13.3") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0sfr7r5k9iwnsnwqg2dd8bab2gikvjqvsmk8s2pjvg1vma29l7cv")))

(define-public crate-plane-split-0.13.4 (c (n "plane-split") (v "0.13.4") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0sggsazxgpx2nkp2xsa5ann0n0w1gi9ghnig6h1gyl5zsx9fmxb9")))

(define-public crate-plane-split-0.13.5 (c (n "plane-split") (v "0.13.5") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0lklwyll3azzw0k0k767yh1jbd6zvja7vicinxhkh07w75gic07l")))

(define-public crate-plane-split-0.13.6 (c (n "plane-split") (v "0.13.6") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0fij45qs8ws37b4bzsadfmixkq5qxbh7bysnwyrjka56vbr8qjxq")))

(define-public crate-plane-split-0.13.7 (c (n "plane-split") (v "0.13.7") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "11w3rv855091c10f7yiyg81754kbmj7n679ddnpcybsh8q2vnq4p")))

(define-public crate-plane-split-0.13.8 (c (n "plane-split") (v "0.13.8") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0r83g0ji7nv8hyhd0add4yrynv8k8g3bx8kwravqanlw7gc23ili")))

(define-public crate-plane-split-0.14.0 (c (n "plane-split") (v "0.14.0") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.20") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0nffpafixgijjms1i68qq089ibzw5qir9m2j4n8ipz5f6rs1r0li")))

(define-public crate-plane-split-0.14.1 (c (n "plane-split") (v "0.14.1") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.20") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0y4wkx2lw4fr5ffhl7d9nb0dxacvw4pb3f61vzw9mkgvhz41g8b8")))

(define-public crate-plane-split-0.13.9 (c (n "plane-split") (v "0.13.9") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1561a4xa64lv1a2rgbj4fw8y03rpli2iybc8i9qprl178v2ri1d4")))

(define-public crate-plane-split-0.15.0 (c (n "plane-split") (v "0.15.0") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.20") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0cyfpv6h2w4yi3si8kx72901msvqwgw9ynq3fkfv9x08d9j6mqgz")))

(define-public crate-plane-split-0.16.0 (c (n "plane-split") (v "0.16.0") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.21") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1a53xlqblixwgv3pkrbx7vkz153q3mh98jhidx10b50yspjsj69s")))

(define-public crate-plane-split-0.17.0 (c (n "plane-split") (v "0.17.0") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0m26frz7xzas82g3nqkl34vdd288n7vmkbcvvmwhf9mnr76ff492")))

(define-public crate-plane-split-0.17.1 (c (n "plane-split") (v "0.17.1") (d (list (d (n "binary-space-partition") (r "^0.1.2") (d #t) (k 0)) (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0qrz3c412wxikpqmn558xbvd9lvyxfnlgjpsw2v9gf26q5g0gxzk")))

(define-public crate-plane-split-0.18.0 (c (n "plane-split") (v "0.18.0") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.9") (d #t) (k 0)))) (h "0hqnrj5933ix8xiyj31z82lg1b470l5pjn72z3gfqacqcj17s7wc")))

