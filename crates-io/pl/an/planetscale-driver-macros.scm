(define-module (crates-io pl an planetscale-driver-macros) #:use-module (crates-io))

(define-public crate-planetscale-driver-macros-0.2.0 (c (n "planetscale-driver-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s3cla591whfir1ghnmfgfj3q5c5iklj3pdp2crz568vvi1f9y20")))

(define-public crate-planetscale-driver-macros-0.2.1 (c (n "planetscale-driver-macros") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mcqn1nswkn8y9gcvcxahamzx8m8y9hr3h828vhyy889bx8rc5ms") (y #t)))

(define-public crate-planetscale-driver-macros-0.2.2 (c (n "planetscale-driver-macros") (v "0.2.2") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "0iplxzsppcf0n9913wwjrkdmrrzqjaxwwa4h1ax98ysmfac97knv")))

