(define-module (crates-io pl an plan9_asm) #:use-module (crates-io))

(define-public crate-plan9_asm-0.1.0 (c (n "plan9_asm") (v "0.1.0") (d (list (d (n "insta") (r "^1.12.0") (d #t) (k 2)))) (h "0nrnpiy80lx6di1kyfaf18ah33wr7nnxn8gnz8ns2qvc776llzsg")))

(define-public crate-plan9_asm-0.1.1 (c (n "plan9_asm") (v "0.1.1") (d (list (d (n "insta") (r "^1.12.0") (d #t) (k 2)))) (h "1i997vs3cmx0695sfaj41pi2xgmdg2qck1i96ph210apims1wpky")))

(define-public crate-plan9_asm-0.1.2 (c (n "plan9_asm") (v "0.1.2") (d (list (d (n "insta") (r "^1.12.0") (d #t) (k 2)))) (h "1v36h2arcdfy93m2vgri8lrbp3hv54dhzyv636wrw1ss2sdl20y3")))

(define-public crate-plan9_asm-0.1.3 (c (n "plan9_asm") (v "0.1.3") (d (list (d (n "insta") (r "^1.12.0") (d #t) (k 2)))) (h "04midr81g65qajhqijvwv2zq60fi7nzv13clpn79h2m178nlmd99")))

(define-public crate-plan9_asm-0.1.4 (c (n "plan9_asm") (v "0.1.4") (d (list (d (n "insta") (r "^1.12.0") (d #t) (k 2)))) (h "0bcnpgp8vrjrjr2arl1v6b4qfrlxxhdmkc99khf8ydgmgx8kmvmq")))

(define-public crate-plan9_asm-0.1.5 (c (n "plan9_asm") (v "0.1.5") (d (list (d (n "insta") (r "^1.12.0") (d #t) (k 2)))) (h "06w34qzrah9c3bbppcn9z8dp45bc4728hnazm8s466x724k4nnkf")))

(define-public crate-plan9_asm-0.2.0 (c (n "plan9_asm") (v "0.2.0") (d (list (d (n "insta") (r "^1.12.0") (d #t) (k 2)))) (h "1pm0s12cvklbh8fhypk6n05naypbq4bsagp8gkdd1j7w18wwz6g6")))

(define-public crate-plan9_asm-0.3.0 (c (n "plan9_asm") (v "0.3.0") (d (list (d (n "insta") (r "^1.12.0") (d #t) (k 2)))) (h "0rwjq9wzibds963kw61lzr5qh1rmnps96h5flwppglvgmbwlip50")))

(define-public crate-plan9_asm-0.4.0 (c (n "plan9_asm") (v "0.4.0") (d (list (d (n "insta") (r "^1.12.0") (d #t) (k 2)))) (h "05xf2xqgi8ffbzssg3cyi062b2kj8k67ay2nqjxixwx699s08rxy")))

(define-public crate-plan9_asm-0.4.1 (c (n "plan9_asm") (v "0.4.1") (d (list (d (n "insta") (r "^1.12.0") (d #t) (k 2)))) (h "0yjv0v8ghzb2llnwdpns0hp5hbg26jaapsay1gfisc96wgax1n40")))

(define-public crate-plan9_asm-0.4.2 (c (n "plan9_asm") (v "0.4.2") (d (list (d (n "insta") (r "^1.12.0") (d #t) (k 2)))) (h "1vzywqf948ilw186cvq3sdjad1wmgdav5ginhzi1n6shpi6siz94")))

(define-public crate-plan9_asm-0.4.3 (c (n "plan9_asm") (v "0.4.3") (d (list (d (n "insta") (r "^1.12.0") (d #t) (k 2)))) (h "0bcx7zaqcli8yq1fk0sd45za2kafqqs37ba48qzpzs1q73ikb7x7")))

