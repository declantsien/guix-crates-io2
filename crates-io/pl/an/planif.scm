(define-module (crates-io pl an planif) #:use-module (crates-io))

(define-public crate-planif-0.0.0-alpha (c (n "planif") (v "0.0.0-alpha") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "windows") (r "^0.34.0") (f (quote ("alloc" "Data_Xml_Dom" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging" "Win32_System_TaskScheduler" "Win32_System_Com" "Win32_System_Ole"))) (d #t) (k 0)))) (h "1njv4p07ljg8dkg2yrlcy58dzswwlfxfynwp5i1gng5apa21ja1c")))

(define-public crate-planif-0.0.1 (c (n "planif") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "windows") (r "^0.34.0") (f (quote ("alloc" "Data_Xml_Dom" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging" "Win32_System_TaskScheduler" "Win32_System_Com" "Win32_System_Ole"))) (d #t) (k 0)))) (h "0m5hrkq4bg6zxw20d4xl57rh4s616vqid0qvxyghql7ja3gy1ddv")))

(define-public crate-planif-0.1.0 (c (n "planif") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "windows") (r "^0.34.0") (f (quote ("alloc" "Data_Xml_Dom" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging" "Win32_System_TaskScheduler" "Win32_System_Com" "Win32_System_Ole"))) (d #t) (k 0)))) (h "0ck0hvhvjyx4mmfc6vwnzdaqpsxfp8shcpa36h8ssa0lah6gw6q0")))

(define-public crate-planif-0.1.1 (c (n "planif") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "windows") (r "^0.34.0") (f (quote ("alloc" "Data_Xml_Dom" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging" "Win32_System_TaskScheduler" "Win32_System_Com" "Win32_System_Ole"))) (d #t) (k 0)))) (h "09zryshqh260rikn25mb6b447hq2gvl18nfhl4vk4ic9hbalkqj9")))

(define-public crate-planif-0.2.0 (c (n "planif") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "windows") (r "^0.34.0") (f (quote ("alloc" "Data_Xml_Dom" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging" "Win32_System_TaskScheduler" "Win32_System_Com" "Win32_System_Ole"))) (d #t) (k 0)))) (h "16847c5l8q65hyg55p2z8y499q4v8zak8cllf68sh109v1qbjkrb")))

(define-public crate-planif-0.2.1 (c (n "planif") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "windows") (r "^0.34.0") (f (quote ("alloc" "Data_Xml_Dom" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging" "Win32_System_TaskScheduler" "Win32_System_Com" "Win32_System_Ole"))) (d #t) (k 0)))) (h "04d20zp0fj3zxqb5ijpbxfwam08rf48mv21kwif4pk18p7f6lxbd")))

(define-public crate-planif-0.2.2 (c (n "planif") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "windows") (r "^0.48.0") (f (quote ("Data_Xml_Dom" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging" "Win32_System_TaskScheduler" "Win32_System_Com" "Win32_System_Ole"))) (d #t) (k 0)))) (h "1hzdrzkyjgr4wl7znk6jfcz65mad8azn7l8ykz489h17rp8nn12s")))

(define-public crate-planif-1.0.0 (c (n "planif") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "windows") (r "^0.48.0") (f (quote ("Data_Xml_Dom" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging" "Win32_System_TaskScheduler" "Win32_System_Com" "Win32_System_Ole"))) (d #t) (k 0)))) (h "08cb9rkxrmhqq92hgf46x3q2cwabkv85bmlk0nh6mh21yddfw0km")))

