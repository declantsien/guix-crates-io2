(define-module (crates-io pl an plantuml_encoding) #:use-module (crates-io))

(define-public crate-plantuml_encoding-0.1.0 (c (n "plantuml_encoding") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)))) (h "0z0kidl3nm5jrzizf363334g3lp50nslkr858sv4k84f4zcrrr7k")))

(define-public crate-plantuml_encoding-0.1.1 (c (n "plantuml_encoding") (v "0.1.1") (d (list (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)))) (h "0b2h7cmd4d2n8mz01lybpjd65qa3jfg5pw47qny6fi8spwdrni6j")))

(define-public crate-plantuml_encoding-0.1.3 (c (n "plantuml_encoding") (v "0.1.3") (d (list (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)))) (h "027q7292yf55m8sm9j3k1vffnimi0g3cjg1vx70by7pq314s4gk6")))

(define-public crate-plantuml_encoding-0.1.4 (c (n "plantuml_encoding") (v "0.1.4") (d (list (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)))) (h "0vlgl7jjg37hb1agq52spxad8dph3k8lad0gwlgmzfh9dxbp3kk4")))

(define-public crate-plantuml_encoding-1.0.0 (c (n "plantuml_encoding") (v "1.0.0") (d (list (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)))) (h "0a3mczml5w4lj489q75dyai2ck4vwdjwjf127l5hzl7726kw69qw")))

(define-public crate-plantuml_encoding-1.0.1 (c (n "plantuml_encoding") (v "1.0.1") (d (list (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)))) (h "14s3lk4g05xmmskx3an5dsa8x2a6w4las47xsn9yvjhxk23nrwxk")))

(define-public crate-plantuml_encoding-2.0.0 (c (n "plantuml_encoding") (v "2.0.0") (d (list (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)))) (h "165zy0qd9y74jkvgdmi9zn9bc1c4nvivy4c18j1rn5cm9q7mnvz8")))

(define-public crate-plantuml_encoding-2.0.1 (c (n "plantuml_encoding") (v "2.0.1") (d (list (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)))) (h "09pd8fjm68km4wa7kbmxb1bkdkgzgg9n5yrxmxlff4s8jmxrv3nv")))

(define-public crate-plantuml_encoding-2.0.2 (c (n "plantuml_encoding") (v "2.0.2") (d (list (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)))) (h "0g3n6a9phg3sl7dqbj6fsjxfnbrhhnq6w6ia1kggjpbfcjinjnpl")))

(define-public crate-plantuml_encoding-2.0.3 (c (n "plantuml_encoding") (v "2.0.3") (d (list (d (n "flate2") (r "^1.0.24") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)))) (h "1f2y8ds85565kkm2lnzdjqgvsbg4ywkfk2b0vlbcsgrlam6svys6")))

