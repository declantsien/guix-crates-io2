(define-module (crates-io pl an planturl) #:use-module (crates-io))

(define-public crate-planturl-0.4.0 (c (n "planturl") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.66") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "deflate") (r "^1.0.0") (o #t) (k 0)) (d (n "deflate") (r "^1") (k 2)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "deflate" "brotli"))) (o #t) (d #t) (k 0)))) (h "0xzrkl5j6frn7f3wfq3c24gwr6201mpasi7jyrrglhkx57i29aw8") (f (quote (("build-binary" "anyhow" "clap" "deflate" "reqwest"))))))

