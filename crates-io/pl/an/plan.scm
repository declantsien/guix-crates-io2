(define-module (crates-io pl an plan) #:use-module (crates-io))

(define-public crate-plan-0.1.0 (c (n "plan") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)))) (h "157lqjjpjd9138zgh2pqg8wcb9r8azf1qrnmz2bxk2nsywhdn8ka")))

(define-public crate-plan-0.1.1 (c (n "plan") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)))) (h "07hfp451gi7ijjly72ql7sjgdvdissd2nadwd190pp7djn9k3fkd")))

(define-public crate-plan-0.1.2 (c (n "plan") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)))) (h "02pfbv2fkxkcabmd8fpxdl2ccb9sm1r6r1sj61z8rd49xnfivm2m")))

(define-public crate-plan-0.1.3 (c (n "plan") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)))) (h "1mwksz1r6pilvc76y2rpcgxfnplsfc4ww481vzcdkdqws3gv2civ")))

(define-public crate-plan-0.1.4 (c (n "plan") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)))) (h "1dyn1fkqgjlady38p7c8mi11nrv1nc2ph72jsl4779swci7w7slv")))

(define-public crate-plan-1.0.0 (c (n "plan") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)))) (h "0crihssb895dgz590yds82bmbam28fgh2b2h9hbasfiyn9dmsx2c")))

(define-public crate-plan-1.0.1 (c (n "plan") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)))) (h "0qgr68bc15icp93rl07a57xql1r64rkp6ada5xyn1nszk9vyyl7k")))

