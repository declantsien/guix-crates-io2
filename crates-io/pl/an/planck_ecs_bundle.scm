(define-module (crates-io pl an planck_ecs_bundle) #:use-module (crates-io))

(define-public crate-planck_ecs_bundle-1.0.0 (c (n "planck_ecs_bundle") (v "1.0.0") (d (list (d (n "world_dispatcher") (r "^1.1.2") (d #t) (k 0)))) (h "0fnlyc8n9jbvgiqpbsg51flpl431i5phawwnlyhp389gvdjl7cx9")))

(define-public crate-planck_ecs_bundle-1.1.0 (c (n "planck_ecs_bundle") (v "1.1.0") (d (list (d (n "world_dispatcher") (r "^1.2.0") (d #t) (k 0)))) (h "1vg3hbqsnj0fz65xm4g1g79rn7255arm6jkz0rs8dis910j8gg7g")))

