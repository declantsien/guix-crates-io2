(define-module (crates-io pl an planus-translation) #:use-module (crates-io))

(define-public crate-planus-translation-0.4.0 (c (n "planus-translation") (v "0.4.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)) (d (n "codespan") (r "^0.11.1") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "lalrpop") (r "^0.20.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "planus-lexer") (r "^0.4.0") (d #t) (k 0)) (d (n "planus-types") (r "^0.4.0") (d #t) (k 0)) (d (n "string-interner") (r "^0.14.0") (d #t) (k 0)))) (h "12rra2nfs9l2gnx6icw1952z6rp4x5a87m98x8ilm12rm3z8zgd7") (r "1.64")))

