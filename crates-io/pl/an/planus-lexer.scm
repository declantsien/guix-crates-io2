(define-module (crates-io pl an planus-lexer) #:use-module (crates-io))

(define-public crate-planus-lexer-0.4.0 (c (n "planus-lexer") (v "0.4.0") (d (list (d (n "codespan") (r "^0.11.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)))) (h "1mmwdwbbzsjj3rsd7fndj2zx73p86d9mh8zk08gm890k5c401jvy") (r "1.64")))

