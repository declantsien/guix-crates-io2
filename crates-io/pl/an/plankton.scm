(define-module (crates-io pl an plankton) #:use-module (crates-io))

(define-public crate-plankton-0.1.0 (c (n "plankton") (v "0.1.0") (h "14j0km7wl50bqfhvq1x4bjfkb35slf1mqd5ykrkkixzmrdv052d1")))

(define-public crate-plankton-0.1.1 (c (n "plankton") (v "0.1.1") (h "0z82dmj9ndmmc5h6z640k4hzwxhxlyagwlxfkkwcbkfxhxin4lk3")))

