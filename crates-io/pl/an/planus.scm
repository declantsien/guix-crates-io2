(define-module (crates-io pl an planus) #:use-module (crates-io))

(define-public crate-planus-0.1.0 (c (n "planus") (v "0.1.0") (d (list (d (n "array-init-cursor") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0gdsgs65valqw0kv9hnm0kbzzzc9r65869b9f658y498b1zq1qaq") (r "1.57")))

(define-public crate-planus-0.2.0 (c (n "planus") (v "0.2.0") (d (list (d (n "array-init-cursor") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0ldiz74z65xjza6v3g482rbhkdbfbk2b26qgbxmd9jnnfkqvmzmz") (f (quote (("std") ("extra-validation") ("default" "std")))) (r "1.57")))

(define-public crate-planus-0.3.0 (c (n "planus") (v "0.3.0") (d (list (d (n "array-init-cursor") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1lk20simc1ikficbx0zm37chw2nsabfq3ghgyxfvhadx0vm6ccr0") (f (quote (("std") ("extra-validation") ("default" "std")))) (r "1.57")))

(define-public crate-planus-0.3.1 (c (n "planus") (v "0.3.1") (d (list (d (n "array-init-cursor") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "17x8mr175b9clg998xpi5z45f9fsspb0ncfnx2644bz817fr25pw") (f (quote (("std") ("extra-validation") ("default" "std")))) (r "1.57")))

(define-public crate-planus-0.4.0 (c (n "planus") (v "0.4.0") (d (list (d (n "array-init-cursor") (r "^0.2.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.2") (f (quote ("raw"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1xc2xwj8z6gmzbl8cff4s2pcrrbssf3smpbxrz8fhrd9zn4wbgic") (f (quote (("vtable-cache" "hashbrown") ("string-cache" "hashbrown") ("std") ("extra-validation") ("default" "std" "vtable-cache" "string-cache" "bytes-cache") ("bytes-cache" "hashbrown")))) (r "1.64")))

