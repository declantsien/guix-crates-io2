(define-module (crates-io pl an planus-inspector) #:use-module (crates-io))

(define-public crate-planus-inspector-0.4.0 (c (n "planus-inspector") (v "0.4.0") (d (list (d (n "clap") (r "^3.2.10") (f (quote ("derive" "deprecated"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "planus-buffer-inspection") (r "^0.4.0") (d #t) (k 0)) (d (n "planus-translation") (r "^0.4.0") (d #t) (k 0)) (d (n "planus-types") (r "^0.4.0") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1jy6k9ad8n2aqppkg0dywzx9fd14g39zikz50jgmaclqg1lqzqys") (r "1.64")))

