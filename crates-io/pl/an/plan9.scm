(define-module (crates-io pl an plan9) #:use-module (crates-io))

(define-public crate-plan9-0.1.0 (c (n "plan9") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lsp-types") (r "^0.74") (d #t) (k 0)) (d (n "nine") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "1r9rmagdi2m2sg0vrhw8ms90rjcshapfkwjiqfb4r21rw0f0cn58")))

(define-public crate-plan9-0.1.1 (c (n "plan9") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lsp-types") (r "^0.82") (d #t) (k 0)) (d (n "nine") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "1zskx8jjmkm587p32vh1r1j33ypln9jpfldpbqv4cwpvfnbyb100")))

