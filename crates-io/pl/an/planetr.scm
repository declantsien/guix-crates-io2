(define-module (crates-io pl an planetr) #:use-module (crates-io))

(define-public crate-planetr-0.1.0 (c (n "planetr") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0kdpcwlpndnfrrmqygzh5zalyjfv1h9aml4jn080zppd2i7mmiw4")))

(define-public crate-planetr-0.2.0 (c (n "planetr") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1qg22ycl6g2yyx0naq5ffl83isbj1v06s12q3w9xl2waza8xgyi5")))

