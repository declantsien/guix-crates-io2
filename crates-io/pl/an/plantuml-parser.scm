(define-module (crates-io pl an plantuml-parser) #:use-module (crates-io))

(define-public crate-plantuml-parser-0.1.0 (c (n "plantuml-parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("std"))) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0sd6myllidh2ga0m44w15c8bx40w3v486awfhn4py8zrj8zjsxnk") (y #t)))

(define-public crate-plantuml-parser-0.1.1 (c (n "plantuml-parser") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("std"))) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1qp8lyrl7giixb7vi95767aags0sfcqvc8hjnfxb87zff5mdxyk8")))

(define-public crate-plantuml-parser-0.1.2 (c (n "plantuml-parser") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("std"))) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1bgh40s520ywp0yazhs76pz4a4dqs29ahx3b0km81xh94j3cmgqi")))

(define-public crate-plantuml-parser-0.1.3 (c (n "plantuml-parser") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("std"))) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (k 0)))) (h "008xxl4qh13kh1vb19g8nlbfs88gd1v8c60r3hjczc48ln8g9cha")))

(define-public crate-plantuml-parser-0.1.4 (c (n "plantuml-parser") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("std"))) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (k 0)))) (h "1sdbch8ynyykcchrn4v3qlc87rriip4f5c6w1cz5db1y5l9c0ish")))

(define-public crate-plantuml-parser-0.1.5 (c (n "plantuml-parser") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("std"))) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (k 0)))) (h "1n7zkdmv5myijlp9v5z15q8aci0nw9q9vagk4j8qmr8z63ipl60c")))

(define-public crate-plantuml-parser-0.2.0 (c (n "plantuml-parser") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("std"))) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (k 0)))) (h "0yaci81v1x58bm07qagg1cajr10rsbi196l7v30y3hyxvxgi8mqc")))

(define-public crate-plantuml-parser-0.2.1 (c (n "plantuml-parser") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("std"))) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (k 0)))) (h "033igzv4wjw3837njqv2w003dzcwkmazyfbhjcfpwrlipb2nsw62")))

(define-public crate-plantuml-parser-0.3.0 (c (n "plantuml-parser") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("std"))) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (k 0)))) (h "1cpgwhix66faalcva8xn1xs5x3dm7a3xl8zbjr80s39fwa5l33d6")))

(define-public crate-plantuml-parser-0.4.0 (c (n "plantuml-parser") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("std"))) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "normalize-path") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (k 0)))) (h "149924als1a1q4w1dif7mxycdh0rk255hmdab439wznl9d7rji12")))

(define-public crate-plantuml-parser-0.5.0 (c (n "plantuml-parser") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("std"))) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "normalize-path") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (k 0)))) (h "0gmr0qcinkza83qikjlsq3kcaz46jnn130cgxir4rl09piajrz7k")))

(define-public crate-plantuml-parser-0.5.1 (c (n "plantuml-parser") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("std"))) (k 2)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "normalize-path") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (k 0)))) (h "1n8yd1y4bb93hg8rn8rwanivdgj51v7ypaawr23d6yp4ymkva2ii")))

