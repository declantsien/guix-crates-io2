(define-module (crates-io pl an plane-cli) #:use-module (crates-io))

(define-public crate-plane-cli-0.3.0 (c (n "plane-cli") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "async-nats") (r "^0.21.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "plane-core") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.36") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.15") (d #t) (k 0)))) (h "1pzj1fiyzav87n0arhhksxks3dvsncyij7fqpbb71zx47k58cpvj")))

