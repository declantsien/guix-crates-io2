(define-module (crates-io pl an planus-codegen) #:use-module (crates-io))

(define-public crate-planus-codegen-0.4.0 (c (n "planus-codegen") (v "0.4.0") (d (list (d (n "askama") (r "^0.11") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "planus-types") (r "^0.4.0") (d #t) (k 0)) (d (n "random_color") (r "^0.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "vec_map") (r "^0.8.2") (d #t) (k 0)))) (h "08f4849hxm7xk61zh5ffph46vw00sd1d68cgdmnx8qbc0q8vplaj") (r "1.64")))

