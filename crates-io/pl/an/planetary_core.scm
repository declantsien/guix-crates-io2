(define-module (crates-io pl an planetary_core) #:use-module (crates-io))

(define-public crate-planetary_core-0.1.0 (c (n "planetary_core") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "postcard") (r "^1.0.4") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (f (quote ("json"))) (d #t) (k 0)))) (h "1yc5a9n3ks9xacq1vck05c84gfq78mlzqc648l85czf570w8lj33")))

