(define-module (crates-io pl an planus-buffer-inspection) #:use-module (crates-io))

(define-public crate-planus-buffer-inspection-0.4.0 (c (n "planus-buffer-inspection") (v "0.4.0") (d (list (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "planus-types") (r "^0.4.0") (d #t) (k 0)) (d (n "rust-lapper") (r "^1.1.0") (d #t) (k 0)))) (h "0yzmsg3nisw595d1wrhw6yhrdc2ypszcfn41mcdvi6p5bxzrjc74") (r "1.64")))

