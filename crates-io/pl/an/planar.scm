(define-module (crates-io pl an planar) #:use-module (crates-io))

(define-public crate-planar-0.1.0 (c (n "planar") (v "0.1.0") (h "146n5bmi5kcln5v7f3sm59h08l5fnjhabxi3mcqsj735y6y4zj7n")))

(define-public crate-planar-0.1.1 (c (n "planar") (v "0.1.1") (h "18ggnmypq8jgc98zgakm65akwrfjyc9ax5i36jlfl17axfg1chxk")))

(define-public crate-planar-0.1.2 (c (n "planar") (v "0.1.2") (h "1z7zdc34qm8linmh5gcb016x3yi9vmdlpxbxb1rxj6rdgvq944yv")))

