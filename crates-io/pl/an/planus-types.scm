(define-module (crates-io pl an planus-types) #:use-module (crates-io))

(define-public crate-planus-types-0.4.0 (c (n "planus-types") (v "0.4.0") (d (list (d (n "codespan") (r "^0.11.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "lalrpop-util") (r "^0.20.0") (d #t) (k 0)) (d (n "planus-lexer") (r "^0.4.0") (d #t) (k 0)) (d (n "string-interner") (r "^0.14.0") (d #t) (k 0)))) (h "0571v32xi23p1af9s89algzf8g6r289gicvwad1pmk3r6pd6nc0h") (r "1.64")))

