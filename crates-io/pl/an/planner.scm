(define-module (crates-io pl an planner) #:use-module (crates-io))

(define-public crate-planner-0.1.0 (c (n "planner") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "17y13gm5kairr7d2zjnn9aca2jb38rpn7nxr8sy2nb22w4nfdlr5")))

(define-public crate-planner-0.1.1 (c (n "planner") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0qimiil6yaiwl6imlpcrf9psid7a18qx2hdjpjvcmp7vcc8nwxck")))

(define-public crate-planner-0.1.2 (c (n "planner") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1njg5gzkybh3b9aw0d4638g3569bf1jx061lx20s8h33yl3xdm8n")))

(define-public crate-planner-0.2.0 (c (n "planner") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1crdg2ngfxnh2gkgg43vq9nifkcmnbmd4lnvnz74xb25c1ipv64g")))

(define-public crate-planner-0.2.1 (c (n "planner") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "091qy0wqvvk1a7kn8mg98187ykycxfi95j08k1xga4sikan42y58")))

(define-public crate-planner-0.2.2 (c (n "planner") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "03bvy61sk4rbk0338qsi4cdspw3007iwxjqhfr2jgr25h5ylv1g6")))

(define-public crate-planner-0.3.0 (c (n "planner") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "comrak") (r "^0.10") (d #t) (k 0)) (d (n "dirs") (r "^3.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "07h8mxjwgjw3ln1q7srspl0iaipb1acllqm3scdj7zr7ing88frp")))

