(define-module (crates-io pl an plandustry) #:use-module (crates-io))

(define-public crate-plandustry-1.0.0 (c (n "plandustry") (v "1.0.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1lms52d308f9jw5hb8p3mrdnrpmlj7l30zv10jy3fjgfy6jrcvfa")))

(define-public crate-plandustry-1.0.1 (c (n "plandustry") (v "1.0.1") (d (list (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)))) (h "0zpsrqy7qcnhsm1cy103rsn19k3x9f0cq026f016z57gwpfdn06x")))

(define-public crate-plandustry-1.0.2 (c (n "plandustry") (v "1.0.2") (d (list (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (k 0)))) (h "1ww3i20shflg85k4xkdrz7a60bha247mla1pr2nk0y3xzb6w1z9h")))

