(define-module (crates-io pl an planetarium) #:use-module (crates-io))

(define-public crate-planetarium-0.1.0 (c (n "planetarium") (v "0.1.0") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.17") (o #t) (d #t) (k 0)))) (h "07csdwqpplyflrcw6zvn7b0px155wl7bi9dc1svvvva6ww43ndr6") (f (quote (("default" "png"))))))

(define-public crate-planetarium-0.1.1 (c (n "planetarium") (v "0.1.1") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.17") (o #t) (d #t) (k 0)))) (h "0byf23chlcsc2xv5vgkkwpjcxg3za5jif7ilvkdcwsd28d82xrc6") (f (quote (("default" "png"))))))

(define-public crate-planetarium-0.1.2 (c (n "planetarium") (v "0.1.2") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.17") (o #t) (d #t) (k 0)))) (h "0yz97fq2lbr8dk8pgyyawcqiardrfsjpfz9161yny72012ps1wgw") (f (quote (("default" "png"))))))

(define-public crate-planetarium-0.1.3 (c (n "planetarium") (v "0.1.3") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.17") (o #t) (d #t) (k 0)))) (h "0cdww15h9gr83gpplqp9sap4qqhvj1ryl88rap55nrvbyyyk70hr") (f (quote (("default" "png"))))))

(define-public crate-planetarium-0.1.4 (c (n "planetarium") (v "0.1.4") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.17") (o #t) (d #t) (k 0)))) (h "1qdnasb024l0yg67bj3brxlk00qcgnbhlnzmjqk3qlm01x722ayi") (f (quote (("default" "png"))))))

(define-public crate-planetarium-0.1.5 (c (n "planetarium") (v "0.1.5") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.17") (o #t) (d #t) (k 0)))) (h "0221s5fiaf6sn3f0bqlmhhqkvaa58wlh42hd1dlbpyn4dm32r5kz") (f (quote (("default" "png"))))))

(define-public crate-planetarium-0.1.6 (c (n "planetarium") (v "0.1.6") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.17.7") (o #t) (d #t) (k 0)))) (h "03yfka44vpg8kr29sl2hri2y48igld9sl6w76i15apymvspih13b") (f (quote (("default" "png"))))))

(define-public crate-planetarium-0.2.0 (c (n "planetarium") (v "0.2.0") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.17.7") (o #t) (d #t) (k 0)))) (h "0hkmf7i3r76xa86iv49m4sy1d9j6hgcjp3bapxxjghn5ryg88767") (f (quote (("default" "png"))))))

