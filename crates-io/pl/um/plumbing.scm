(define-module (crates-io pl um plumbing) #:use-module (crates-io))

(define-public crate-plumbing-0.9.0 (c (n "plumbing") (v "0.9.0") (d (list (d (n "futures") (r "^0.3.5") (f (quote ("alloc"))) (k 0)) (d (n "futures") (r "^0.3.5") (f (quote ("std"))) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-threaded" "macros" "rt-util"))) (k 2)))) (h "0dz9w44syvblkipd9lfqgyk73wp62f1qrl600wgn1w850ay98i8n")))

(define-public crate-plumbing-0.9.1 (c (n "plumbing") (v "0.9.1") (d (list (d (n "futures") (r "^0.3.5") (f (quote ("alloc"))) (k 0)) (d (n "futures") (r "^0.3.5") (f (quote ("std"))) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-threaded" "macros" "rt-util"))) (k 2)))) (h "0r7w8vfzzdarrsilmc6k6pzx0xxjma9amvrs6rxbvy9yi9y2cxma")))

(define-public crate-plumbing-0.9.2 (c (n "plumbing") (v "0.9.2") (d (list (d (n "futures") (r "^0.3.5") (f (quote ("alloc"))) (k 0)) (d (n "futures") (r "^0.3.5") (f (quote ("std"))) (k 2)) (d (n "pin-project") (r "^0.4.23") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-threaded" "macros" "rt-util"))) (k 2)))) (h "11npvm3i54ba0rppwr6f2k2ysiql84pw58kr5zblr7p24d0s2ldv")))

(define-public crate-plumbing-1.0.0 (c (n "plumbing") (v "1.0.0") (d (list (d (n "foreback") (r "^1.1.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (f (quote ("alloc"))) (k 0)) (d (n "futures") (r "^0.3.5") (f (quote ("std"))) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("rt-threaded" "macros" "rt-util"))) (k 2)))) (h "1pgl6cz3wf70vhcnn41hda0qdhsi7njdv72kfs1cm948alxzqdnf")))

