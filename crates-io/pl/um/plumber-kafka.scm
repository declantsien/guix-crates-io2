(define-module (crates-io pl um plumber-kafka) #:use-module (crates-io))

(define-public crate-plumber-kafka-0.1.0 (c (n "plumber-kafka") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "kafka") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 0)))) (h "09890h2136sby6vaqwfsknhpgiy8ks92smrfhg637a6xkdxivvls")))

