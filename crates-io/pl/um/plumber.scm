(define-module (crates-io pl um plumber) #:use-module (crates-io))

(define-public crate-plumber-0.0.1 (c (n "plumber") (v "0.0.1") (d (list (d (n "lazy_static") (r "^0.1.14") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0hfxdh6iz8wyzkiagpcivsfsfvh1szrbc48j6c3n053hg9sf6q6p")))

(define-public crate-plumber-0.0.2 (c (n "plumber") (v "0.0.2") (d (list (d (n "lazy_static") (r "^0.1.14") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1zyw2lh9sayay5rwggd8plivp5f61kfmgnvzyzbsx600csc8y91k")))

(define-public crate-plumber-0.0.3 (c (n "plumber") (v "0.0.3") (d (list (d (n "lazy_static") (r "^0.1.14") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1lif423r2p7ikksc42bi2wrwnpdjzxyyf7ifvgk20ly7x9f2kyyj")))

(define-public crate-plumber-0.0.4 (c (n "plumber") (v "0.0.4") (d (list (d (n "lazy_static") (r "^0.1.14") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "070j2gcv7l5q3lrnvf6crhdkczk15qg0y5s6x9w8s3n0ykv9liba")))

(define-public crate-plumber-0.0.5 (c (n "plumber") (v "0.0.5") (d (list (d (n "lazy_static") (r "^0.1.14") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0szllzs4gh29cm6hj6h959l7af3q5l5fqd2mw7fgmiv1c6kkrv2l")))

(define-public crate-plumber-0.0.6 (c (n "plumber") (v "0.0.6") (d (list (d (n "lazy_static") (r "^0.1.14") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "03bc35cgbb19g93z37jgz6zfhhc1zblhj153v14s0ycxvqcr1rva")))

(define-public crate-plumber-0.0.7 (c (n "plumber") (v "0.0.7") (d (list (d (n "lazy_static") (r "^0.1.14") (d #t) (k 0)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "10x0qidq008pyg0jphkdmizlpz35zb8rh11gyasgssp1q5gm85v4")))

