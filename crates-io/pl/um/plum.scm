(define-module (crates-io pl um plum) #:use-module (crates-io))

(define-public crate-plum-0.1.0 (c (n "plum") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)))) (h "0bf8x847670ljxchmcwvxak9ldagsdysirppps3mmnn64zs35m23")))

(define-public crate-plum-0.1.1 (c (n "plum") (v "0.1.1") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)))) (h "0ci3c58js1glc0rc4k294159kkh3x34arfkrkd7z7mk3r1slf31h")))

(define-public crate-plum-0.1.2 (c (n "plum") (v "0.1.2") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)))) (h "18wid1dwklc808pibvc0vqsjca3zl8crx6q2n7299gwaizcmyf9g")))

(define-public crate-plum-0.1.3 (c (n "plum") (v "0.1.3") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)))) (h "0p6ziazxngffb3dp48vr904ik7zgc93vra2chcm8pcnc8knvl7ba")))

(define-public crate-plum-0.1.4 (c (n "plum") (v "0.1.4") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)))) (h "1gkgz273rga284v5w9zb3ihr3q1krn3m00r2622kd9s7mags33ac")))

(define-public crate-plum-0.1.5 (c (n "plum") (v "0.1.5") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)))) (h "0yi1kjb2f38b6idqc5b2nksnjqlpx4vbc2w9g658rkg8fp3y5dyi")))

