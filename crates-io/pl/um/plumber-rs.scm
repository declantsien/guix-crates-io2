(define-module (crates-io pl um plumber-rs) #:use-module (crates-io))

(define-public crate-plumber-rs-0.1.0 (c (n "plumber-rs") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1s3lwxim6pmcaa9f06a6f7p2dwb7rmx4dzxz9vmxg4czbcwmlxkm")))

(define-public crate-plumber-rs-0.1.1 (c (n "plumber-rs") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0qqigc1r69mjzf6x45ygnknxhc1nadnc63xax4k5fdxjprlxd429")))

(define-public crate-plumber-rs-0.1.2 (c (n "plumber-rs") (v "0.1.2") (h "1kdblsxap41yvv3s1hyyc2xxbybjrpwfjqrzzmavhd6i2skw8dlp")))

