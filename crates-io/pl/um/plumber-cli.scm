(define-module (crates-io pl um plumber-cli) #:use-module (crates-io))

(define-public crate-plumber-cli-0.1.1 (c (n "plumber-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("signal"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "shlex") (r "^1.2.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 0)))) (h "1xlqqvwwnvb3khnbdl5z4jl12jag47rhsi4ax38brspgkrikv0vz")))

(define-public crate-plumber-cli-0.2.1 (c (n "plumber-cli") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("signal"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "shlex") (r "^1.2.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 0)))) (h "1axhm3g0ncshvbmy24ihkf42457jb9655yq4p6yd18crx07zlrz6")))

(define-public crate-plumber-cli-0.2.2 (c (n "plumber-cli") (v "0.2.2") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("signal"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "shlex") (r "^1.2.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.17") (d #t) (k 0)))) (h "15czrsns4lasg7aqd1n2520kbhiyy1rv4hn61alxpp0pma7fspx3")))

(define-public crate-plumber-cli-0.3.1 (c (n "plumber-cli") (v "0.3.1") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "shlex") (r "^1.2.0") (d #t) (k 0)))) (h "16va2qd9343gprp539c9631kbywm04r2wpc1kmf5pdfkqm342zmq")))

(define-public crate-plumber-cli-0.3.2 (c (n "plumber-cli") (v "0.3.2") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "shlex") (r "^1.2.0") (d #t) (k 0)))) (h "0jx2jnixid08hppaky31q1lafk4m83gv7p1b1wh8mxhvipw7dz1j")))

(define-public crate-plumber-cli-0.3.3 (c (n "plumber-cli") (v "0.3.3") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "shlex") (r "^1.2.0") (d #t) (k 0)))) (h "1pm4025zhbcc35fkf4dhjgkk4jlmkkk50ycx5dwpa0kf9f3y93zv")))

(define-public crate-plumber-cli-0.4.1 (c (n "plumber-cli") (v "0.4.1") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.1") (f (quote ("termination"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "shlex") (r "^1.2.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)))) (h "1q3i6ga3v9kx3jv8fw8k5jmwcly6vrfg049vlpdv0psy95c1rpzq")))

