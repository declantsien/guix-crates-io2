(define-module (crates-io pl um plumtree) #:use-module (crates-io))

(define-public crate-plumtree-0.0.1 (c (n "plumtree") (v "0.0.1") (h "19s25gcv22ilc0h1c7a3x70j9hk4warkv1anskxg67wd8488xbbl")))

(define-public crate-plumtree-0.0.2 (c (n "plumtree") (v "0.0.2") (h "1m6zbg0r926f2i8mmcgzg8508w32lx2s11l8bfjc5mc23dx4pxnk")))

(define-public crate-plumtree-0.0.3 (c (n "plumtree") (v "0.0.3") (h "1mxvnbp5q38423d4f7p784hlrl8zb88m4qgr366nqbws69xhaz5r")))

(define-public crate-plumtree-0.0.4 (c (n "plumtree") (v "0.0.4") (h "0qvj47yx0d6fwz6cvyzxqqqsszwsr46kqd3amwkv8vaiy8xvnx73")))

(define-public crate-plumtree-0.0.5 (c (n "plumtree") (v "0.0.5") (h "11xg5r35nh01fjblzzsy6mg6crd5vj9i9p8wl1bhya0041wlndgi")))

(define-public crate-plumtree-0.1.0 (c (n "plumtree") (v "0.1.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1i2shi8j8iqk7chrw0jjbrk91ya44pb0wxzaddgiz0z23ba8gp4b")))

(define-public crate-plumtree-0.1.1 (c (n "plumtree") (v "0.1.1") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0wdvw4jgw6iy4c3aq4jwjz6mhqg6pqrra27d5qkldn33wbx74vq7")))

