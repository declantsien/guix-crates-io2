(define-module (crates-io pl um plumbum) #:use-module (crates-io))

(define-public crate-plumbum-0.0.1 (c (n "plumbum") (v "0.0.1") (h "00c871z0nccsdbna7qdwv5m7dqn1a0rlj7sj3nxymh2vr52mwqav")))

(define-public crate-plumbum-0.0.2 (c (n "plumbum") (v "0.0.2") (h "0g28yp478f1nxr21zs6h2g2i5lnxw9z7a24r84w8yjmgrhp1w7f5")))

(define-public crate-plumbum-0.0.3 (c (n "plumbum") (v "0.0.3") (h "0dsiljq1g5pf5a73bpgj1mh5zarii4j025m3fkwz11mcpf1ma290")))

(define-public crate-plumbum-0.0.4 (c (n "plumbum") (v "0.0.4") (h "1gdwar8x4fg7kdw5n9binqlvqw37ddygg6smwxwbv5qs975nhwcp")))

(define-public crate-plumbum-0.0.5 (c (n "plumbum") (v "0.0.5") (h "102vasqxc82a7b0f9p4lkxzb51kchavf7iw34s1j779jjnpyadrn")))

(define-public crate-plumbum-0.0.6 (c (n "plumbum") (v "0.0.6") (h "1n412957b34g9qk5y3gn36vv6x3i9nvr0b10fpg48d0lg1jv2cjf")))

(define-public crate-plumbum-0.0.7 (c (n "plumbum") (v "0.0.7") (h "0xm8prh0n5h4ng8s2v6fsns02ni3x5j83s0pqp8mxy8bclpvwgya")))

(define-public crate-plumbum-0.0.8 (c (n "plumbum") (v "0.0.8") (h "1i6ainm2p961iz5xmx7cddrn8616cpwwqxxnml8fggic21xdk6l2")))

