(define-module (crates-io pl yp plyparse) #:use-module (crates-io))

(define-public crate-plyparse-0.1.0 (c (n "plyparse") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ply-rs") (r "^0.1.3") (d #t) (k 0)))) (h "1djg57r0a99bvaaz077jlg2wmqp4bvaiz0yygpzav9454dw2vswz")))

(define-public crate-plyparse-0.1.1 (c (n "plyparse") (v "0.1.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ply-rs") (r "^0.1.3") (d #t) (k 0)))) (h "0rh4f8r48rcdd2p1hhmxqgc30r3z09dz3innxgfsilrvap92g9ap")))

(define-public crate-plyparse-0.1.2 (c (n "plyparse") (v "0.1.2") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ply-rs") (r "^0.1.3") (d #t) (k 0)))) (h "06mxv3an3h1k694wm4mxfw5lvqw2vzd0799frm6rlxqmqcnwifdn")))

