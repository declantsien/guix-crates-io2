(define-module (crates-io pl on plonk-runtime-api) #:use-module (crates-io))

(define-public crate-plonk-runtime-api-0.1.0 (c (n "plonk-runtime-api") (v "0.1.0") (d (list (d (n "dusk-plonk") (r "^0.2") (k 0) (p "parity-plonk")) (d (n "sp-api") (r "^3.0") (k 0)))) (h "0ynx6jns0nnmrlvld98fzayhjg9gf4f43ylnd52f28cppfw2i2cb") (f (quote (("std" "sp-api/std") ("default" "std"))))))

