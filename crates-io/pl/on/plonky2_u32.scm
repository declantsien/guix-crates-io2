(define-module (crates-io pl on plonky2_u32) #:use-module (crates-io))

(define-public crate-plonky2_u32-0.1.0 (c (n "plonky2_u32") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (k 0)) (d (n "itertools") (r "^0.10.0") (k 0)) (d (n "num") (r "^0.4") (k 0)) (d (n "plonky2") (r "^0.1.1") (k 0)) (d (n "plonky2") (r "^0.1.1") (f (quote ("gate_testing"))) (k 2)) (d (n "rand") (r "^0.8.4") (f (quote ("getrandom"))) (k 2)))) (h "13v8820i15s4jj2y0x91l3ms04a79rxp638dljbypfcm5v7bzd70")))

