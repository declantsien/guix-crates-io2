(define-module (crates-io pl on plonky2_maybe_rayon) #:use-module (crates-io))

(define-public crate-plonky2_maybe_rayon-0.1.0 (c (n "plonky2_maybe_rayon") (v "0.1.0") (d (list (d (n "rayon") (r "^1.5.3") (o #t) (d #t) (k 0)))) (h "1i147r6wbvw8qqyvxdj28g8pjr7ny93m5d995fscsq622gdxqp35") (f (quote (("parallel" "rayon"))))))

(define-public crate-plonky2_maybe_rayon-0.1.1 (c (n "plonky2_maybe_rayon") (v "0.1.1") (d (list (d (n "rayon") (r "^1.5.3") (o #t) (d #t) (k 0)))) (h "0klr6xv9mi2p0clfn5bkjqdxqs1rvd5bg4ndjzc94klpvp5v0k8r") (f (quote (("parallel" "rayon"))))))

(define-public crate-plonky2_maybe_rayon-0.2.0 (c (n "plonky2_maybe_rayon") (v "0.2.0") (d (list (d (n "rayon") (r "^1.5.3") (o #t) (d #t) (k 0)))) (h "08x9pbm6pi0imwmahg3w7x6406xs2nwgmy6xww83x8dc1all9zwj") (f (quote (("parallel" "rayon"))))))

