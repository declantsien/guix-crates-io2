(define-module (crates-io pl on plonky2_ecdsa) #:use-module (crates-io))

(define-public crate-plonky2_ecdsa-0.1.0 (c (n "plonky2_ecdsa") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (k 0)) (d (n "itertools") (r "^0.10.0") (k 0)) (d (n "num") (r "^0.4.0") (k 0)) (d (n "plonky2") (r "^0.1.1") (k 0)) (d (n "plonky2_maybe_rayon") (r "^0.1.0") (k 0)) (d (n "plonky2_u32") (r "^0.1.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("getrandom"))) (k 2)))) (h "1vxaxrj77sywv8482pklvxr432w16d2lxwkdz2fzd5idw8sfgbyy") (f (quote (("parallel" "plonky2_maybe_rayon/parallel" "plonky2/parallel"))))))

