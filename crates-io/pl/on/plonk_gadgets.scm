(define-module (crates-io pl on plonk_gadgets) #:use-module (crates-io))

(define-public crate-plonk_gadgets-0.5.0 (c (n "plonk_gadgets") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "dusk-bytes") (r "^0.1") (d #t) (k 0)) (d (n "dusk-plonk") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "07rhhjpj9inyxlpjl2zq1gcp8sh3626h83icg9af88fb3smrn8pv")))

(define-public crate-plonk_gadgets-0.6.0-rc.0 (c (n "plonk_gadgets") (v "0.6.0-rc.0") (d (list (d (n "dusk-bytes") (r "^0.1") (d #t) (k 0)) (d (n "dusk-plonk") (r "^0.8.0-rc.1") (f (quote ("alloc"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0p7bs23zfprcp9bihyd2vkl4yhxq5frqby1b2002dxgrvrc89j1i") (f (quote (("std" "dusk-plonk/std"))))))

(define-public crate-plonk_gadgets-0.6.0 (c (n "plonk_gadgets") (v "0.6.0") (d (list (d (n "dusk-bytes") (r "^0.1") (d #t) (k 0)) (d (n "dusk-plonk") (r "^0.8") (f (quote ("alloc"))) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1z4gkklzl6jiwg7qcsvn565d6v0ii3f759kmqj9hlbpzhpwgj2zd") (f (quote (("std" "dusk-plonk/std"))))))

