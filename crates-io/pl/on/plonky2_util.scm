(define-module (crates-io pl on plonky2_util) #:use-module (crates-io))

(define-public crate-plonky2_util-0.1.0 (c (n "plonky2_util") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("getrandom"))) (k 2)))) (h "1j5mqn5zdfgkfq34jnxjihyqvlf6g2yqi93xwksqm56bm22ilzrs")))

(define-public crate-plonky2_util-0.1.1 (c (n "plonky2_util") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("getrandom"))) (k 2)))) (h "01kdpnclw8kz6d7rc2dphhklgafh416xvc1zwfk4hp5vlvif55jn")))

(define-public crate-plonky2_util-0.2.0 (c (n "plonky2_util") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("getrandom"))) (k 2)))) (h "0n92y6ckyp0qf1i30cr9bsd7hvfmvp66rxss0f1ix701ygskcqdi")))

