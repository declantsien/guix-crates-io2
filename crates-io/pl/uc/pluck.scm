(define-module (crates-io pl uc pluck) #:use-module (crates-io))

(define-public crate-pluck-0.1.0 (c (n "pluck") (v "0.1.0") (h "1rn44dzh7knqnz7nyds6ik20094wacrxmxs6f3fv7n3a595czsj1")))

(define-public crate-pluck-0.1.1 (c (n "pluck") (v "0.1.1") (h "088g4g0w79m0hnmg5wf4s30wi7sqkn195fzcpnfms7609kmdjl5p")))

