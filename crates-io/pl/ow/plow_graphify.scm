(define-module (crates-io pl ow plow_graphify) #:use-module (crates-io))

(define-public crate-plow_graphify-0.1.0 (c (n "plow_graphify") (v "0.1.0") (d (list (d (n "harriet") (r "^0.1") (d #t) (k 0)) (d (n "rdftk_core") (r "^0.3") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1hdn35pwmyl50zn5nbjldfjfpsyq109d86qdcs5fv1r20wylffrv")))

(define-public crate-plow_graphify-0.2.0 (c (n "plow_graphify") (v "0.2.0") (d (list (d (n "harriet") (r "^0.2") (d #t) (k 0)) (d (n "rdftk_core") (r "^0.3") (d #t) (k 0)) (d (n "rdftk_iri") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1cyrgb5fngz5pm1yhyyclzd14443x5jdifzz7b7hn9ay2k22vj50")))

(define-public crate-plow_graphify-0.2.1 (c (n "plow_graphify") (v "0.2.1") (d (list (d (n "field33_rdftk_core_temporary_fork") (r "^0.3") (d #t) (k 0)) (d (n "field33_rdftk_iri_temporary_fork") (r "^0.1") (d #t) (k 0)) (d (n "harriet") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ks9i3sbmrhyrnd6vhsnc1nn883c4bsvr7cp18b5f5m43p2ma6xk")))

(define-public crate-plow_graphify-0.2.2 (c (n "plow_graphify") (v "0.2.2") (d (list (d (n "field33_rdftk_core_temporary_fork") (r "^0.3") (d #t) (k 0)) (d (n "field33_rdftk_iri_temporary_fork") (r "^0.1") (d #t) (k 0)) (d (n "harriet") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "12wjpnq773wh98y7k0af7wvfwjgrdfjif6zw1qk3kxrw1zcjnfdz")))

(define-public crate-plow_graphify-0.2.3 (c (n "plow_graphify") (v "0.2.3") (d (list (d (n "field33_rdftk_core_temporary_fork") (r "^0.3") (d #t) (k 0)) (d (n "field33_rdftk_iri_temporary_fork") (r "^0.1") (d #t) (k 0)) (d (n "harriet") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0mbjpgwgi49lypns1p2yvlg2cgqzkcfnqn6r2j8c3ii31wdqdfww")))

