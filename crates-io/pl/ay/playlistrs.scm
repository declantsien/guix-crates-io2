(define-module (crates-io pl ay playlistrs) #:use-module (crates-io))

(define-public crate-playlistrs-0.2.0 (c (n "playlistrs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "4.*") (d #t) (k 0)) (d (n "partial_application") (r "^0.2.1") (d #t) (k 0)) (d (n "plist") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.20") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0nffm7zz1vn96z66vj6x9xyrxwyndxkrq0v5dvd5g4xy9ddxpk67")))

