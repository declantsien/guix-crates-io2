(define-module (crates-io pl ay playpen) #:use-module (crates-io))

(define-public crate-playpen-0.1.0 (c (n "playpen") (v "0.1.0") (d (list (d (n "hyper") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^0") (d #t) (k 0)) (d (n "serde_json") (r "^0") (d #t) (k 0)) (d (n "serde_macros") (r "^0") (d #t) (k 0)) (d (n "url") (r "^1") (f (quote ("query_encoding"))) (d #t) (k 0)))) (h "1glm22kfkcg81y0zw9k1vs9hvl69rvq8pqgnwwvq6pcc9v42cak6")))

