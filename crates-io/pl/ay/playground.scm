(define-module (crates-io pl ay playground) #:use-module (crates-io))

(define-public crate-playground-0.1.0 (c (n "playground") (v "0.1.0") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha1") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0frsaqy72f1cfvrx6k3wlw6010r8lmp6ac0k5fafi2ajh14rki7d") (y #t)))

