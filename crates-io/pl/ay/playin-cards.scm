(define-module (crates-io pl ay playin-cards) #:use-module (crates-io))

(define-public crate-playin-cards-0.1.0 (c (n "playin-cards") (v "0.1.0") (d (list (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1c1578dy4hi2c1wvmhv2ipgpvx9kwf92z8b8p9k74d6mvn255qi6") (f (quote (("rank-partialord"))))))

(define-public crate-playin-cards-0.1.1 (c (n "playin-cards") (v "0.1.1") (d (list (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1knvax4zsh8lsa9dc8fcp9ipm5vi6y6c2rkb51898yacdwpyajv6") (f (quote (("rank-partialord"))))))

(define-public crate-playin-cards-0.1.2 (c (n "playin-cards") (v "0.1.2") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1jjalrnrb1rq9h598clsgwnki0ch70xqyfc9rv4q3327d14rbk2m") (f (quote (("rank-partialord"))))))

