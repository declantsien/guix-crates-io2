(define-module (crates-io pl ay play_head) #:use-module (crates-io))

(define-public crate-play_head-0.0.1 (c (n "play_head") (v "0.0.1") (h "02s6arzcbg315krd5pwx5gfziv3qzkx0bsdblmwpa2d193nfa74q") (y #t)))

(define-public crate-play_head-0.0.2 (c (n "play_head") (v "0.0.2") (h "137bkv5x2w28qd2fll0jcgv4lflfh181qwy9710h5bdhp3g5cy0d") (y #t)))

