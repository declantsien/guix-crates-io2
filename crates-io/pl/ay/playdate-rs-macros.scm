(define-module (crates-io pl ay playdate-rs-macros) #:use-module (crates-io))

(define-public crate-playdate-rs-macros-0.0.2 (c (n "playdate-rs-macros") (v "0.0.2") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "0xdckr86srw215v7bqwq8c6s68kf1jlg2q14zqj6f351wa10j080")))

(define-public crate-playdate-rs-macros-0.0.3 (c (n "playdate-rs-macros") (v "0.0.3") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "0nc042v0kv3flaa9npmwm3km5d0n9zgk7fj4zr5bdrg6bvh3cxnq")))

(define-public crate-playdate-rs-macros-0.0.4 (c (n "playdate-rs-macros") (v "0.0.4") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "19nhv4wk8ca8vlw3rm6d157x9xgg2k3aia81p0lngklgxx2nss5v")))

(define-public crate-playdate-rs-macros-0.0.5 (c (n "playdate-rs-macros") (v "0.0.5") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "1baz2dvnffjkqnknj3b0k2nqsjf2j6gzva2c6y58nkkwm8wl5ycq")))

(define-public crate-playdate-rs-macros-0.0.6 (c (n "playdate-rs-macros") (v "0.0.6") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "1ycpnyfx46dni14ywi2jn56sd9904j7w5376j2j65gbraqvnc7mg")))

(define-public crate-playdate-rs-macros-0.0.7 (c (n "playdate-rs-macros") (v "0.0.7") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "1qi73l804lvs1rld23i9zhvpp6685556pjfldb6salvg9w7aqaav")))

(define-public crate-playdate-rs-macros-0.0.8 (c (n "playdate-rs-macros") (v "0.0.8") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "17vb11q28sjipff3vj3jagpd70cj4m9iwrh8f9l7g3nxa3crr8ix")))

(define-public crate-playdate-rs-macros-0.0.9 (c (n "playdate-rs-macros") (v "0.0.9") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "0zplnhf7lcakzi36zjxspzwmf3dk4qvkjn906siz7sz70zvqwpyb")))

(define-public crate-playdate-rs-macros-0.0.10 (c (n "playdate-rs-macros") (v "0.0.10") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "1b6n2mzcxq1wkwhw0kngkrrczhgl8rn94312j23rzk77hr0qwb1c")))

(define-public crate-playdate-rs-macros-0.0.11 (c (n "playdate-rs-macros") (v "0.0.11") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "0w2vqnafbvkpk2zg02r25js0y19yhfs6mjwc4dnanwaqsf8as71p")))

(define-public crate-playdate-rs-macros-0.0.12 (c (n "playdate-rs-macros") (v "0.0.12") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "1n7cqshk1zhrpv88m6mvvk64dzigb4dx5iqb4yqk367b50jf1qrh")))

(define-public crate-playdate-rs-macros-0.0.13 (c (n "playdate-rs-macros") (v "0.0.13") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "1glf99iwzs76ibr23djgvh9pmcdsckmsgwir03hsq78czgwbl612")))

(define-public crate-playdate-rs-macros-0.0.14 (c (n "playdate-rs-macros") (v "0.0.14") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "0fbb0ig2wj5mmj4n4vvw30zmbnc6kr0gc3c1z3rb5102ldcr02nl")))

(define-public crate-playdate-rs-macros-0.0.15 (c (n "playdate-rs-macros") (v "0.0.15") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "0l9lzrxg6srfdrml3wzm9pgpzcm3z5s2ll449kswcbylzfnh4k0c")))

