(define-module (crates-io pl ay playdate-scoreboards) #:use-module (crates-io))

(define-public crate-playdate-scoreboards-0.0.0 (c (n "playdate-scoreboards") (v "0.0.0") (h "0b8230gdiyyis162dzx01215nfgg228zhvahrrap2v60cn94bsah")))

(define-public crate-playdate-scoreboards-0.1.0 (c (n "playdate-scoreboards") (v "0.1.0") (d (list (d (n "erased_set") (r "^0.7.0") (d #t) (k 0)) (d (n "sys") (r "^0.2") (k 0) (p "playdate-sys")) (d (n "system") (r "^0.3") (f (quote ("try-trait-v2"))) (k 2) (p "playdate-system")))) (h "04akznwkffrnyhw4qgc15zc28y5rkcqsm0465l3d9srymjh7w310") (f (quote (("default" "sys/default") ("bindings-derive-debug" "sys/bindings-derive-debug") ("bindgen-static" "sys/bindgen-static") ("bindgen-runtime" "sys/bindgen-runtime"))))))

(define-public crate-playdate-scoreboards-0.1.1 (c (n "playdate-scoreboards") (v "0.1.1") (d (list (d (n "erased_set") (r "^0.8") (d #t) (k 0)) (d (n "sys") (r "^0.3") (k 0) (p "playdate-sys")) (d (n "system") (r "^0.3") (f (quote ("try-trait-v2"))) (k 2) (p "playdate-system")))) (h "0ip53rrsjmcbnblw4lniq4i52i0xcggx2qap77ncdkgix4war8d3") (f (quote (("default" "sys/default") ("bindings-derive-debug" "sys/bindings-derive-debug") ("bindgen-static" "sys/bindgen-static") ("bindgen-runtime" "sys/bindgen-runtime"))))))

