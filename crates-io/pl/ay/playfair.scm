(define-module (crates-io pl ay playfair) #:use-module (crates-io))

(define-public crate-playfair-0.0.1 (c (n "playfair") (v "0.0.1") (h "137i6k4cmlqvlsm7l2cjslg5b8n3zhd702spl51x0gh3v5ihkgdd") (r "1.65.0")))

(define-public crate-playfair-0.1.0 (c (n "playfair") (v "0.1.0") (h "0pmpvg8wlzsnqdbqh6y39jjvn6axx8g8647pv42fsx04w69yml3g") (r "1.65.0")))

(define-public crate-playfair-1.0.0 (c (n "playfair") (v "1.0.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "1qwab7xq406gh21f15rxwf0k9smn4yjqvi0gw91fqhygargfp7iv")))

