(define-module (crates-io pl ay playrs) #:use-module (crates-io))

(define-public crate-playrs-0.1.0 (c (n "playrs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0dgj1raxwvaa04iv650lchkya08gj7hh3rn37r7apnrn98hdv3xc")))

(define-public crate-playrs-0.2.0 (c (n "playrs") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "1vyh0lr2hswjgh0l1jzjka1yxjarns2q791zilxvg3ny7395m615")))

(define-public crate-playrs-0.3.0 (c (n "playrs") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "14hg6axc5ga4p1mlqx4gy9wqc7rcsdhlrbqdp6ixvj5b4ylfdgl0")))

