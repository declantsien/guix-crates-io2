(define-module (crates-io pl ay play-games-services) #:use-module (crates-io))

(define-public crate-play-games-services-0.1.9 (c (n "play-games-services") (v "0.1.9") (d (list (d (n "crossbow-android") (r "^0.1.9") (d #t) (k 0)))) (h "170z8yihfij87n2dwjynb4qisnpalnmksfjgzll71kh3ph164sf1")))

(define-public crate-play-games-services-0.2.0 (c (n "play-games-services") (v "0.2.0") (d (list (d (n "crossbow-android") (r "^0.2.0") (d #t) (k 0)))) (h "1hrx77l0c11kbnjjvhvhrwhp5wfcl5z2r1is7fp68kz5bx30hjii")))

(define-public crate-play-games-services-0.2.1 (c (n "play-games-services") (v "0.2.1") (d (list (d (n "crossbow-android") (r "^0.2.1") (d #t) (k 0)))) (h "0k5gkbp0ra2j7sgncqzqijlhczqg3qk1a2aakywsmsfdc21l2jbl")))

(define-public crate-play-games-services-0.2.2 (c (n "play-games-services") (v "0.2.2") (d (list (d (n "crossbow-android") (r "^0.2.2") (d #t) (k 0)))) (h "152ayh9vdfaswhwyf355dh09dpdws5g66d2i0gvxqaxqlrfwyyv3")))

(define-public crate-play-games-services-0.2.3 (c (n "play-games-services") (v "0.2.3") (d (list (d (n "crossbow-android") (r "^0.2.3") (d #t) (k 0)))) (h "0gpvfqc0x1vqd8haraw2m5f9d0g1dydvinsabhpqxmng0cvzasd2")))

