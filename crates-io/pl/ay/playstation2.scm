(define-module (crates-io pl ay playstation2) #:use-module (crates-io))

(define-public crate-playstation2-0.1.0 (c (n "playstation2") (v "0.1.0") (h "163dnbn8k5h4br2v6760q7q1b0bwwy07sy1j9m1nwgflwwxi0hrm") (y #t)))

(define-public crate-playstation2-0.1.1 (c (n "playstation2") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "02724495ij5nj5kz11gzjfxbpa6y49ldj43vp9l9rysyrff7lplm") (y #t)))

(define-public crate-playstation2-0.1.2 (c (n "playstation2") (v "0.1.2") (d (list (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0c2xni0diaqrf774vx6w8h1xsk2dl209yrqll94xa9dr2h6an5xy") (y #t)))

(define-public crate-playstation2-0.1.3 (c (n "playstation2") (v "0.1.3") (d (list (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "06qvm7y0v2nfpkbkr0pa1gn4j44bn7d0dii0cpmrxzsi8a7g0s5l")))

