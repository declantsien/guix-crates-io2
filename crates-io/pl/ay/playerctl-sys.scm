(define-module (crates-io pl ay playerctl-sys) #:use-module (crates-io))

(define-public crate-playerctl-sys-2.4.1 (c (n "playerctl-sys") (v "2.4.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "120mlr3xvhp19i696ry7lkdsls0cdkvhpgb5n1ibwrycd41qdgzf") (y #t)))

(define-public crate-playerctl-sys-2.4.2 (c (n "playerctl-sys") (v "2.4.2") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "0il0fyvkvnn764xlnq8hqrqqpbm4vdh7yxig729gpdixxg0jdq0v") (y #t)))

(define-public crate-playerctl-sys-2.4.3 (c (n "playerctl-sys") (v "2.4.3") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "1jm5h43smwmhwdlwyks68i72i01ydbfzrnfch3w740v8gq3dffg6") (y #t)))

(define-public crate-playerctl-sys-2.4.4 (c (n "playerctl-sys") (v "2.4.4") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "0b2iw2llc2b185g33ngij4xbqcicm26df4fhgy53azx400i95p2h") (y #t)))

(define-public crate-playerctl-sys-2.4.5 (c (n "playerctl-sys") (v "2.4.5") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "0zjybfa1ksk507w2klzxbc08z53j54dgg7pcaiy3ny4dkgfmiinp") (y #t)))

(define-public crate-playerctl-sys-0.1.0 (c (n "playerctl-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "meson") (r "^1.0.0") (d #t) (k 1)) (d (n "system-deps") (r "^6.0.2") (d #t) (k 1)))) (h "18j95xh4n0w2wbkjfx1m7nmp229dxwdx4mfj0mblb87cr7ajx31k") (y #t)))

(define-public crate-playerctl-sys-0.1.1 (c (n "playerctl-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "meson") (r "^1.0.0") (d #t) (k 1)) (d (n "system-deps") (r "^6.0.2") (d #t) (k 1)))) (h "1dbwzk0xbzg5x7iwg0phf1r8bqf1fs3v3kv6zqbi2sd9lk0gwinw") (y #t)))

(define-public crate-playerctl-sys-0.1.2 (c (n "playerctl-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "meson") (r "^1.0.0") (d #t) (k 1)) (d (n "system-deps") (r "^6.0.2") (d #t) (k 1)))) (h "066lq5yi9007p1is8ag3lvgv05dd4mjs0nqf9c503686akmlm5kk") (y #t)))

(define-public crate-playerctl-sys-0.1.3 (c (n "playerctl-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "meson") (r "^1.0.0") (d #t) (k 1)) (d (n "system-deps") (r "^6.0.2") (d #t) (k 1)))) (h "08vwabxmdb0l5anpqhgdkfkii331ccrk1fywx129mnriihj9wn3a") (y #t)))

(define-public crate-playerctl-sys-0.1.4 (c (n "playerctl-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "meson") (r "^1.0.0") (d #t) (k 1)) (d (n "system-deps") (r "^6.0.2") (d #t) (k 1)))) (h "1sf90jnxxwlgzz2jchzxq9qhcvsjjnmn83vw7whsk3aid5x59cig") (y #t)))

(define-public crate-playerctl-sys-0.1.5 (c (n "playerctl-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "glib-sys") (r "^0.15.10") (f (quote ("v2_72"))) (d #t) (k 0)) (d (n "meson") (r "^1.0.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)))) (h "154w7vwsvwhl9z6d6ms1ps8n4shgzy68z2yii5690l45028mv7j3") (l "playerctl")))

(define-public crate-playerctl-sys-0.1.6 (c (n "playerctl-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "glib-sys") (r "^0.15.10") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.15.10") (d #t) (k 0)) (d (n "meson") (r "^1.0.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)))) (h "18ar3lii3dp7h6xbxji6mxvpy7q53zxcsm8v4j75qnycx4h9vjrd") (l "playerctl")))

