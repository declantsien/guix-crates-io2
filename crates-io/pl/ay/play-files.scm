(define-module (crates-io pl ay play-files) #:use-module (crates-io))

(define-public crate-play-files-0.1.0 (c (n "play-files") (v "0.1.0") (d (list (d (n "arr_macro") (r "^0.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)))) (h "1kqirnxxyxmnbijp66i0yy7aiqkc0pr8v3bp7ddhsmrgkgkh6d24")))

