(define-module (crates-io pl ay playdate-rs-cli) #:use-module (crates-io))

(define-public crate-playdate-rs-cli-0.0.1 (c (n "playdate-rs-cli") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.17.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.23") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-cargo") (r "^0.11.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "minijinja") (r "^1.0.6") (d #t) (k 0)))) (h "0r4kijv3mmgisxw3n5c3zlvs4adcdiyrgmba00i2qkg3m1ghm8mn") (y #t)))

