(define-module (crates-io pl ay playdate-bindgen-cfg) #:use-module (crates-io))

(define-public crate-playdate-bindgen-cfg-0.1.0 (c (n "playdate-bindgen-cfg") (v "0.1.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("std" "env" "derive" "help"))) (o #t) (d #t) (k 0)))) (h "1pklg4jkfyzl3984l0yhnnr5sl42z4mfkdjbh08j713ns4hadrlh") (f (quote (("default"))))))

(define-public crate-playdate-bindgen-cfg-0.1.1 (c (n "playdate-bindgen-cfg") (v "0.1.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("std" "env" "derive" "help"))) (o #t) (d #t) (k 0)))) (h "0bdyls7xgjf9kap0ql2nvqm3pzw7v1r4ip9rmm39zrrm2nkry9lv") (f (quote (("default"))))))

(define-public crate-playdate-bindgen-cfg-0.1.2 (c (n "playdate-bindgen-cfg") (v "0.1.2") (d (list (d (n "clap") (r "^4.4") (f (quote ("std" "env" "derive" "help"))) (o #t) (d #t) (k 0)))) (h "1qwyk3qqyfr4dsf0jbr4azv2yd3v39swsvkzcd0pgpa0gl7fgxlc") (f (quote (("default"))))))

(define-public crate-playdate-bindgen-cfg-0.1.3 (c (n "playdate-bindgen-cfg") (v "0.1.3") (d (list (d (n "clap") (r "^4.5") (f (quote ("std" "env" "derive" "help"))) (o #t) (d #t) (k 0)))) (h "04g173hq0wrmjdsln2m4bqry6y7h2cwgz886g3qgi75xb04zfgvf") (f (quote (("default"))))))

(define-public crate-playdate-bindgen-cfg-0.1.4 (c (n "playdate-bindgen-cfg") (v "0.1.4") (d (list (d (n "clap") (r "^4.5") (f (quote ("std" "env" "derive" "help"))) (o #t) (d #t) (k 0)))) (h "1jpf3zcpy4nbb7mlifawjbqvl1k8ycbakn9yahmqhfi324wkx8ml") (f (quote (("default"))))))

(define-public crate-playdate-bindgen-cfg-0.1.5 (c (n "playdate-bindgen-cfg") (v "0.1.5") (d (list (d (n "clap") (r "^4.5") (f (quote ("std" "env" "derive" "help"))) (o #t) (d #t) (k 0)))) (h "0m4clahzhv6ryzb1zjkkm1ri7m4bg75rvpdgq7j0w3vapv7xkh7b") (f (quote (("default"))))))

(define-public crate-playdate-bindgen-cfg-0.1.6 (c (n "playdate-bindgen-cfg") (v "0.1.6") (d (list (d (n "clap") (r "^4.5") (f (quote ("std" "env" "derive" "help"))) (o #t) (d #t) (k 0)))) (h "0jbjf3grzv490jljd6rscbvxzidv6zi1whbv8nhsbmflhxd8cgw3") (f (quote (("default"))))))

