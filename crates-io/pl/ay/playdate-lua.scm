(define-module (crates-io pl ay playdate-lua) #:use-module (crates-io))

(define-public crate-playdate-lua-0.0.0 (c (n "playdate-lua") (v "0.0.0") (h "0rha8grq4vr21y33sd91zk4777fwkqlrfqpa7adhn2n9sfjv8y9m")))

(define-public crate-playdate-lua-0.1.0 (c (n "playdate-lua") (v "0.1.0") (d (list (d (n "sys") (r "^0.3") (k 0) (p "playdate-sys")) (d (n "system") (r "^0.3") (k 2) (p "playdate-system")))) (h "0ncln9ck3hqmcfgz6921znz7443351qpvrpgdl8wfch1kqldyq0n") (f (quote (("default" "sys/default") ("bindings-derive-debug" "sys/bindings-derive-debug") ("bindgen-static" "sys/bindgen-static") ("bindgen-runtime" "sys/bindgen-runtime"))))))

