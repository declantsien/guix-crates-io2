(define-module (crates-io pl ay play-billing) #:use-module (crates-io))

(define-public crate-play-billing-0.2.0 (c (n "play-billing") (v "0.2.0") (d (list (d (n "crossbow-android") (r "^0.2.0") (d #t) (k 0)))) (h "1npwqlgfsnj4vryrwdyk9rjqanwa87zggjxyvsqqa3s8nkbm9haz")))

(define-public crate-play-billing-0.2.1 (c (n "play-billing") (v "0.2.1") (d (list (d (n "crossbow-android") (r "^0.2.1") (d #t) (k 0)))) (h "0gj1ds31d5rxcq6mkffsrzdhcr5r59z2p911pnnknsc6p2pw2gs6")))

(define-public crate-play-billing-0.2.2 (c (n "play-billing") (v "0.2.2") (d (list (d (n "crossbow-android") (r "^0.2.2") (d #t) (k 0)))) (h "1cvjcxkxlszlq1j0dkxks0pv98030j6ikqkjg42zmx5caj1rj0k8")))

(define-public crate-play-billing-0.2.3 (c (n "play-billing") (v "0.2.3") (d (list (d (n "crossbow-android") (r "^0.2.3") (d #t) (k 0)))) (h "17iq8jqbpvl866lq2xracfdin0bifnbykfpmbndvk3q1m1rw8dnv")))

