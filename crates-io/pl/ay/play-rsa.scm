(define-module (crates-io pl ay play-rsa) #:use-module (crates-io))

(define-public crate-play-rsa-0.4.0 (c (n "play-rsa") (v "0.4.0") (d (list (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.6") (f (quote ("rand"))) (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "17spnz5gswskkgwhhzb1raxvd2jbqb24q3fgyj1hfs5qj3ip4kb2")))

(define-public crate-play-rsa-0.4.1 (c (n "play-rsa") (v "0.4.1") (d (list (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.6") (f (quote ("rand"))) (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "03gwh51658jz60fq78dvb2c88dhb52jgnrlri86f8373q2256z4h")))

