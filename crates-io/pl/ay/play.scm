(define-module (crates-io pl ay play) #:use-module (crates-io))

(define-public crate-play-0.0.1 (c (n "play") (v "0.0.1") (h "035s7zyfy0wan0kn7p9ms84mdh3gpay34n77d8y37ns28qxxi5ai")))

(define-public crate-play-0.0.2 (c (n "play") (v "0.0.2") (d (list (d (n "ao-sys") (r "^0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.0") (d #t) (k 0)))) (h "041fss4lix23vb40rmap98pl3kwwhfqh1vhlsfa7qhaql5yi7cz6")))

(define-public crate-play-0.0.3 (c (n "play") (v "0.0.3") (d (list (d (n "ao-sys") (r "^0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.0") (d #t) (k 0)))) (h "1i57g0hi15llhzgsljdsj8d6hs1v81fafk05q2zjffbh19fcdkx7")))

(define-public crate-play-0.1.0 (c (n "play") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.2") (d #t) (k 0)) (d (n "out123-sys") (r "^0.1") (d #t) (k 0)))) (h "1jnl6a6gq4swv1f13lhchx6vdr03ab9kbznibkw5z4fgr1wbvdla")))

(define-public crate-play-0.2.0 (c (n "play") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.3") (d #t) (k 0)) (d (n "out123-sys") (r "^0.2") (d #t) (k 0)))) (h "1ap1awgzbj68l27gwj1xlqfipkb5yj8srvqnibq9w2d83dkby5nn") (f (quote (("static" "mpg123-sys/static" "out123-sys/static"))))))

(define-public crate-play-0.2.1 (c (n "play") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.3") (d #t) (k 0)) (d (n "out123-sys") (r "^0.2") (d #t) (k 0)))) (h "18a97wdh9751jwha167ddhi7pvrcqz7l92257ng0272mm4brhmyh") (f (quote (("static" "mpg123-sys/static" "out123-sys/static"))))))

(define-public crate-play-0.2.2 (c (n "play") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.3") (d #t) (k 0)) (d (n "out123-sys") (r "^0.2") (d #t) (k 0)))) (h "1va1bxwf8rrnrngc07nmsffv3xfgggy4xa38ldy9bkcv22a432cf") (f (quote (("static" "mpg123-sys/static" "out123-sys/static"))))))

(define-public crate-play-0.3.0 (c (n "play") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.3") (d #t) (k 0)) (d (n "out123-sys") (r "^0.2") (d #t) (k 0)))) (h "06j8yfk1awxpjg1c3nz0lnkjfbkvmg0jami8dn4riqicqfjzm19b") (f (quote (("static" "mpg123-sys/static" "out123-sys/static"))))))

(define-public crate-play-0.4.0 (c (n "play") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.3") (d #t) (k 0)) (d (n "out123-sys") (r "^0.2") (d #t) (k 0)))) (h "1fj2d5z3ky9vj1asxhd0cy7m0a9d45g42z3kbsqvgfy2l7m7k712") (f (quote (("static" "mpg123-sys/static" "out123-sys/static"))))))

(define-public crate-play-0.4.1 (c (n "play") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.3") (d #t) (k 0)) (d (n "out123-sys") (r "^0.2") (d #t) (k 0)))) (h "1przm6l91b4vrdc0s2lmwfffr6pcf9a104x8cja62a7gspr1plm1") (f (quote (("static" "mpg123-sys/static" "out123-sys/static"))))))

(define-public crate-play-0.4.2 (c (n "play") (v "0.4.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.3") (d #t) (k 0)) (d (n "out123-sys") (r "^0.2") (d #t) (k 0)))) (h "16wbiv4jhia240ldx9bblwrwn1gklzmm2b5p6r9fa027lzqs36wp") (f (quote (("static" "mpg123-sys/static" "out123-sys/static"))))))

(define-public crate-play-0.5.0 (c (n "play") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.4") (d #t) (k 0)) (d (n "out123-sys") (r "^0.3") (d #t) (k 0)))) (h "0im4kwha2daz9v5yny5k9id8bwrixwzzwqbl8gs3v6bza59ffvsp") (f (quote (("static" "mpg123-sys/static" "out123-sys/static"))))))

(define-public crate-play-0.5.1 (c (n "play") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.4") (d #t) (k 0)) (d (n "out123-sys") (r "^0.3") (d #t) (k 0)))) (h "0m9df8rxmdvsc7xq11crq0bhaljx2piikb9nqq038g60fkanx0jv") (f (quote (("static" "mpg123-sys/static" "out123-sys/static"))))))

(define-public crate-play-0.5.2 (c (n "play") (v "0.5.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.5") (d #t) (k 0)) (d (n "out123-sys") (r "^0.4") (d #t) (k 0)))) (h "072h6iq0s3nxl3mk0pq9awl4gcwnssdcg20bcwdbcxiw9hvkz6cy") (f (quote (("static" "mpg123-sys/static" "out123-sys/static"))))))

(define-public crate-play-0.5.3 (c (n "play") (v "0.5.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mpg123-sys") (r "^0.6") (d #t) (k 0)) (d (n "out123-sys") (r "^0.5") (d #t) (k 0)))) (h "1w2gfi1gqclbp5c4vh7ilinmg3dpvkkjyi9gxblkk0aqaib0hw65") (f (quote (("static" "mpg123-sys/static" "out123-sys/static"))))))

