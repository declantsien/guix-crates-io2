(define-module (crates-io pl ay player) #:use-module (crates-io))

(define-public crate-player-0.1.0 (c (n "player") (v "0.1.0") (h "0ffmfrcmw20ya7ak4jskwvdwv43axq0apcvr45hyg8jy5msclinl")))

(define-public crate-player-0.1.1 (c (n "player") (v "0.1.1") (h "1vwycfh27r1vikicg43kf9dv20044mvip9ac0viwpj7y00lqi2rs")))

