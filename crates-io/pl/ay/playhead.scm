(define-module (crates-io pl ay playhead) #:use-module (crates-io))

(define-public crate-playhead-0.1.0 (c (n "playhead") (v "0.1.0") (h "1ghsf4fmpw11f8vyviyayk2c6vqjkl2sz03l4675slsmah1y04v4") (y #t)))

(define-public crate-playhead-0.0.3 (c (n "playhead") (v "0.0.3") (h "08b42zcm99wxiyj2mx7i78il114fc7vb1mprm7a7718rkdzwcmj1") (y #t)))

(define-public crate-playhead-0.2.0 (c (n "playhead") (v "0.2.0") (h "1z4387y12a90vshrakzw8h3493b6qhpzg262kpk3sznj8slm1p81")))

(define-public crate-playhead-0.2.1 (c (n "playhead") (v "0.2.1") (h "1s3n2ccnvnmy92gbski2gx7w0va9w744ir3d6cls6zh75yfhnw08")))

(define-public crate-playhead-0.2.2 (c (n "playhead") (v "0.2.2") (h "1kg85dpb6mkmdxqgigl7w36cmkqyzbhx51j1fqsl7c3c1gzr6k4d")))

