(define-module (crates-io pl ay play-2048) #:use-module (crates-io))

(define-public crate-play-2048-0.1.0 (c (n "play-2048") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "101p6kzna8y6xq5dq1hjd095h9fklwg9jyx8mpsj8alrpps3dpad")))

(define-public crate-play-2048-1.0.0 (c (n "play-2048") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1kzw0avmnjjnq4a2vmydym7q41f5acis9hsfw2p7zqk7ismsvfr5")))

