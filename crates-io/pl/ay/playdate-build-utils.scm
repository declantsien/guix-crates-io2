(define-module (crates-io pl ay playdate-build-utils) #:use-module (crates-io))

(define-public crate-playdate-build-utils-0.0.0 (c (n "playdate-build-utils") (v "0.0.0") (h "1r4w48qgzqgpqq46ia84s9j8s4ziriyr98ykz5ps6hl1gi4x3ris") (y #t)))

(define-public crate-playdate-build-utils-0.1.0 (c (n "playdate-build-utils") (v "0.1.0") (d (list (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1flswyakqyalsbyy7f8inz4102hmwxrk0ppvwqqc0jx5l9rxxykb") (f (quote (("default" "log" "cargo-message") ("cargo-message"))))))

(define-public crate-playdate-build-utils-0.1.1 (c (n "playdate-build-utils") (v "0.1.1") (d (list (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "035l5sdqh24ncszc2jaiz8cqiqnjvpy2irajsl5wz5pjpqxnhjd8") (f (quote (("default" "log" "cargo-message") ("cargo-message"))))))

(define-public crate-playdate-build-utils-0.1.2 (c (n "playdate-build-utils") (v "0.1.2") (d (list (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1invplkm6fjmscxvq7hpbii3d18h0h3jnqz5asa3fs04mncqqam2") (f (quote (("default" "log" "cargo-message") ("cargo-message"))))))

(define-public crate-playdate-build-utils-0.1.3 (c (n "playdate-build-utils") (v "0.1.3") (d (list (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "006fm3357jsypgm16wh1n3m2c2hp8k147snzq8p9al2rgmviyk9g") (f (quote (("default" "log" "cargo-message") ("cargo-message"))))))

(define-public crate-playdate-build-utils-0.2.0 (c (n "playdate-build-utils") (v "0.2.0") (d (list (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "windows-registry") (r "^0.1.0") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "16k9cn1jdx23qxq8wk021b838nb99234nqa3psjbzq60pmqfjn6j") (f (quote (("default" "log" "cargo-message") ("cargo-message"))))))

(define-public crate-playdate-build-utils-0.2.1 (c (n "playdate-build-utils") (v "0.2.1") (d (list (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "windows-registry") (r "^0.1.0") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0wsfazwmf414b7a6d8ql39zxlyabx1avaichix0d6i7ar847w43p") (f (quote (("default" "log" "cargo-message") ("cargo-message"))))))

(define-public crate-playdate-build-utils-0.3.0 (c (n "playdate-build-utils") (v "0.3.0") (d (list (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "windows-registry") (r "^0.1.0") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0sciazcjfdqq67ip79195pw2ks4xsai3dn9j887457qcs5c9znp4") (f (quote (("default" "log" "cargo-message") ("cargo-message"))))))

(define-public crate-playdate-build-utils-0.3.1 (c (n "playdate-build-utils") (v "0.3.1") (d (list (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "windows-registry") (r "^0.1.0") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1c9bdh8r4zdjikr68l03lc19np0987wlcdlw55z6j49wwj782imp") (f (quote (("default" "log" "cargo-message") ("cargo-message"))))))

(define-public crate-playdate-build-utils-0.3.2 (c (n "playdate-build-utils") (v "0.3.2") (d (list (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "windows-registry") (r "^0.1.0") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0qqjfyil3ry55g5aaa1mmcw55fms578lqw65d3hzvhil2dwnhynj") (f (quote (("default" "log" "cargo-message") ("cargo-message"))))))

