(define-module (crates-io pl ay play-core) #:use-module (crates-io))

(define-public crate-play-core-0.2.1 (c (n "play-core") (v "0.2.1") (d (list (d (n "crossbow-android") (r "^0.2.1") (d #t) (k 0)))) (h "12jsz8yi6v28ijjkkwc0b9hs93gps9719xglmnf7ll935vkhcph7")))

(define-public crate-play-core-0.2.2 (c (n "play-core") (v "0.2.2") (d (list (d (n "crossbow-android") (r "^0.2.2") (d #t) (k 0)))) (h "1x69lha9f0d4dcdc95y4j2chp6r59y9zzcf6gld820cgh0djzac5")))

(define-public crate-play-core-0.2.3 (c (n "play-core") (v "0.2.3") (d (list (d (n "crossbow-android") (r "^0.2.3") (d #t) (k 0)))) (h "04pmrrrndb5wy0plz1x4dwss5qjab79ycb1mzyj1djmq3b2c7rci")))

