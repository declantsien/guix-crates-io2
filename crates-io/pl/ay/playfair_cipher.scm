(define-module (crates-io pl ay playfair_cipher) #:use-module (crates-io))

(define-public crate-playfair_cipher-0.1.0 (c (n "playfair_cipher") (v "0.1.0") (h "1hxqwigdi5al74mqnmpnbbg0fx08yd245h20c658343wk9pqmkk2")))

(define-public crate-playfair_cipher-0.2.0 (c (n "playfair_cipher") (v "0.2.0") (h "0qpmx5yxb7yb9kyvvswj9mjjzkdnpfrffjl5aghs56qxa8bkbdwx")))

(define-public crate-playfair_cipher-0.2.1 (c (n "playfair_cipher") (v "0.2.1") (h "19n75lx85li9ii152js82rlkhan2has615pqfhs9ly7dgj7bql35")))

(define-public crate-playfair_cipher-0.2.2 (c (n "playfair_cipher") (v "0.2.2") (h "0b5drzasdypchxih0bcdvs29zd7wdcnn9g824sadc8r13i7b3c85")))

(define-public crate-playfair_cipher-0.2.3 (c (n "playfair_cipher") (v "0.2.3") (h "14z998a7x1j9vjssm8m594f8h99kagq4g25d4bgg7wd4g9k4yw2z")))

(define-public crate-playfair_cipher-0.3.0 (c (n "playfair_cipher") (v "0.3.0") (h "1b9y4y33hw5lh68shh2z1n1n1zl4wr5qcqn4zbsh3ayj7rdr1l9f")))

(define-public crate-playfair_cipher-0.3.1 (c (n "playfair_cipher") (v "0.3.1") (h "0wpwnlkr3pkdjjdwjgjjqfc2949zkvns4m92dc8f7b7dy6s1mxpp")))

(define-public crate-playfair_cipher-0.3.2 (c (n "playfair_cipher") (v "0.3.2") (h "1gl0qfh8bnss19q3fi4z83mgxffr3xjmxzxxm3zkmmjfs3xh46by")))

(define-public crate-playfair_cipher-0.3.3 (c (n "playfair_cipher") (v "0.3.3") (h "0nm1hncvzwwyxbq9dvh9rz1nba84mx5k2jvwzghh9w17sngg631i")))

(define-public crate-playfair_cipher-0.3.4 (c (n "playfair_cipher") (v "0.3.4") (h "0yxdwpflfhk0i78njmwl32s01yn6lwahcyk59q1qfkl0c910m1d2")))

(define-public crate-playfair_cipher-0.3.5 (c (n "playfair_cipher") (v "0.3.5") (h "0pzmyycff7bswdbgaip2w6k3xk4d6lhqq7d911g75s65jf42xw7h")))

