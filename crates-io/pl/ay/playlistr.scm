(define-module (crates-io pl ay playlistr) #:use-module (crates-io))

(define-public crate-playlistr-0.1.0 (c (n "playlistr") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)))) (h "1sf4mjaspppix3czw43nvmzhiqn1mp88kcm75maxbpgx625q7j8r")))

(define-public crate-playlistr-0.1.1 (c (n "playlistr") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)))) (h "02mq6n5g4s2mw8gb21x1j55qacxpryiaskpjwdqb50f1niqr4q77")))

