(define-module (crates-io pl ay playrsa) #:use-module (crates-io))

(define-public crate-playrsa-0.4.0 (c (n "playrsa") (v "0.4.0") (d (list (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.6") (f (quote ("rand"))) (d #t) (k 0)) (d (n "primal") (r "^0.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "0s43jr4bz9d036g3iix259zjsbk3csb8qvpzvnfwxs66lyciip0s") (y #t)))

