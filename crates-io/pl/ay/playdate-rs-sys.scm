(define-module (crates-io pl ay playdate-rs-sys) #:use-module (crates-io))

(define-public crate-playdate-rs-sys-0.0.1 (c (n "playdate-rs-sys") (v "0.0.1") (h "09l4smxmm8sk4zhas553j7606nr7a7smp3a8bz9mx875m129hns4")))

(define-public crate-playdate-rs-sys-0.0.2 (c (n "playdate-rs-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "home") (r "^0.5.5") (o #t) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 1)))) (h "07i9532c9wlxwx16cz7hkzlrrlvpn2p2w96v3hasvw9krml9waps") (f (quote (("generate" "bindgen" "home"))))))

(define-public crate-playdate-rs-sys-0.0.3 (c (n "playdate-rs-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "home") (r "^0.5.5") (o #t) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 1)))) (h "1jsl1iid2ybiwhv2kzj9lz2mqg2c4224z37brwph9lpspjhns6wh") (f (quote (("generate" "bindgen" "home"))))))

(define-public crate-playdate-rs-sys-0.0.4 (c (n "playdate-rs-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "home") (r "^0.5.5") (o #t) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 1)))) (h "1sxwg835bwiiw7fpjkp87i0hdf9rijz9pa0a9lmfsnqnkz9p6r5d") (f (quote (("generate" "bindgen" "home"))))))

(define-public crate-playdate-rs-sys-0.0.5 (c (n "playdate-rs-sys") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "home") (r "^0.5.5") (o #t) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 1)))) (h "1a4f0mgalg7al676h4lhrsmk8ddh6gnzqdzdwxz2pwz1wb7fhv9n") (f (quote (("generate" "bindgen" "home"))))))

(define-public crate-playdate-rs-sys-0.0.6 (c (n "playdate-rs-sys") (v "0.0.6") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "home") (r "^0.5.5") (o #t) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 1)))) (h "1h24132n79k3mji5djqbisl312w8lr5l3nnsirp1yji2fcgh3idl") (f (quote (("generate" "bindgen" "home"))))))

(define-public crate-playdate-rs-sys-0.0.7 (c (n "playdate-rs-sys") (v "0.0.7") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "home") (r "^0.5.5") (o #t) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 1)))) (h "1ydsc833dc6rbhwqdds80dyb405y5930f5gh6jw8qyvc0qjsn71p") (f (quote (("generate" "bindgen" "home"))))))

(define-public crate-playdate-rs-sys-0.0.8 (c (n "playdate-rs-sys") (v "0.0.8") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "home") (r "^0.5.5") (o #t) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 1)))) (h "0kxan7875csypdl9h8clgmgrqmnlwxlnn4cpb8i86wgyd1z3plbk") (f (quote (("generate" "bindgen" "home"))))))

(define-public crate-playdate-rs-sys-0.0.9 (c (n "playdate-rs-sys") (v "0.0.9") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "home") (r "^0.5.5") (o #t) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 1)))) (h "0y0yjzkyv6q7vp6m21ns319m8gx4ngccm4mmspn0y12a9vcqmf6n") (f (quote (("generate" "bindgen" "home"))))))

(define-public crate-playdate-rs-sys-0.0.10 (c (n "playdate-rs-sys") (v "0.0.10") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "home") (r "^0.5.5") (o #t) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 1)))) (h "04x837cqm0kbrg6sdmw635s0mxilc15r0n199qp6cihlaph5w03m") (f (quote (("generate" "bindgen" "home"))))))

(define-public crate-playdate-rs-sys-0.0.11 (c (n "playdate-rs-sys") (v "0.0.11") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "home") (r "^0.5.5") (o #t) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 1)))) (h "0gypsywhxq1l5kyyfxzwlbdbm49z6pa5vizg240vfyzv76i2r1xy") (f (quote (("generate" "bindgen" "home"))))))

(define-public crate-playdate-rs-sys-0.0.12 (c (n "playdate-rs-sys") (v "0.0.12") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "home") (r "^0.5.5") (o #t) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 1)))) (h "1kw5v082gp4cd5n5z8jkamwb5fr2z97ikhy7fyvwlq05xggh1fhv") (f (quote (("generate" "bindgen" "home"))))))

