(define-module (crates-io pl ic plic) #:use-module (crates-io))

(define-public crate-plic-0.0.0 (c (n "plic") (v "0.0.0") (h "0f93ksd0ca233i8sv4hzmwndwv708sakzsbbwnw7933wn98lfwlq") (f (quote (("primitive-id") ("default")))) (y #t)))

(define-public crate-plic-0.0.1 (c (n "plic") (v "0.0.1") (h "1mx7qmln2pbavmsxsp4pnbr3b20i113x2d2pagcpxlfvhgwgrn0x") (f (quote (("primitive-id") ("default")))) (y #t)))

(define-public crate-plic-0.0.2 (c (n "plic") (v "0.0.2") (h "0ylvgj36wiw9z37ynjyf4azagpcpd9yxzdv1l480wyyn66zhdmia") (f (quote (("primitive-id") ("default"))))))

