(define-module (crates-io pl io pliocomp) #:use-module (crates-io))

(define-public crate-pliocomp-0.1.0 (c (n "pliocomp") (v "0.1.0") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0x7swnnz4zm2mx6w5h0iaca13823k8dy6rvyzrnh0rnazs19azbs")))

(define-public crate-pliocomp-0.1.1 (c (n "pliocomp") (v "0.1.1") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0qdizmzf5yq4b4skj0f0hsww5dsbma5mhswmz5l0ay6qcagw6ycj")))

