(define-module (crates-io pl ug plugin-test-main) #:use-module (crates-io))

(define-public crate-plugin-test-main-0.1.0 (c (n "plugin-test-main") (v "0.1.0") (d (list (d (n "plugin-test-api") (r "^0.1") (d #t) (k 0)) (d (n "plugin-test-plugins") (r "^0.1") (d #t) (k 0)))) (h "16igd8fnh07i7mb231g7yys2899wifv84asjixvnwm9m748m3vzc")))

(define-public crate-plugin-test-main-0.1.1 (c (n "plugin-test-main") (v "0.1.1") (d (list (d (n "plugin-test-api") (r "^0.1") (d #t) (k 0)) (d (n "plugin-test-plugins") (r "^0.1") (d #t) (k 0)))) (h "091v0kz20n39wqi06xgvyi9yrz803xdzzi06pb4m9vflb3xv15wr")))

