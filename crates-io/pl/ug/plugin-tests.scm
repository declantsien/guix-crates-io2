(define-module (crates-io pl ug plugin-tests) #:use-module (crates-io))

(define-public crate-plugin-tests-0.6.1 (c (n "plugin-tests") (v "0.6.1") (d (list (d (n "forc-index") (r "^0.6.1") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.6.1") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "1lbzz1v0wdmbjvyr9dpwyxcxz63aixvxkxf84zc3lnks7npsjppp") (r "1.67.0")))

(define-public crate-plugin-tests-0.7.0 (c (n "plugin-tests") (v "0.7.0") (d (list (d (n "forc-index") (r "^0.7.0") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.7.0") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "03v3jqdil0kb5sbim1s21aaawmaia8d2viv28bcz8rixkh4yy9q3") (r "1.68.1")))

(define-public crate-plugin-tests-0.8.0 (c (n "plugin-tests") (v "0.8.0") (d (list (d (n "forc-index") (r "^0.8.0") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.8.0") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "1gjw98p301slsxkv8aqrav0k84b2lymzbp11af7jx24qr791gc1d") (r "1.68.1")))

(define-public crate-plugin-tests-0.9.0 (c (n "plugin-tests") (v "0.9.0") (d (list (d (n "forc-index") (r "^0.9.0") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.9.0") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "1k8a8z49yszsnm6nj1v8p7l2vlrp788lyajrip60w98z9siyz4mh") (r "1.68.1")))

(define-public crate-plugin-tests-0.10.0 (c (n "plugin-tests") (v "0.10.0") (d (list (d (n "forc-index") (r "^0.10.0") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.10.0") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "0wq2wdyiwnvhhblmdg8zas27dnw1rbrsyd3x8baa6n0f0ib4zcyj") (r "1.68.1")))

(define-public crate-plugin-tests-0.11.1 (c (n "plugin-tests") (v "0.11.1") (d (list (d (n "forc-index") (r "^0.11.1") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.11.1") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "18bxhidcynl9wlsfb2670k8m6ap3mzf0m349xgi0cl7nldcfw2rd") (r "1.68.1")))

(define-public crate-plugin-tests-0.11.2 (c (n "plugin-tests") (v "0.11.2") (d (list (d (n "forc-index") (r "^0.11.2") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.11.2") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "01y9iywfywm342s6bbr5ph3hdxa0n8wwapl8mb0xcpdmmns2jknp") (r "1.68.1")))

(define-public crate-plugin-tests-0.12.0 (c (n "plugin-tests") (v "0.12.0") (d (list (d (n "forc-index") (r "^0.12.0") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.12.0") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "0dxj7sxp5awfdvbllg4qlwsx7rvgr03gnihq7457wb1lmr5p8jvn") (r "1.69.0")))

(define-public crate-plugin-tests-0.12.1 (c (n "plugin-tests") (v "0.12.1") (d (list (d (n "forc-index") (r "^0.12.1") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.12.1") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "1asvqqr06w96g1kqlxg2kny5skqxka7yr0yvxx6hfhc76262552m") (r "1.69.0")))

(define-public crate-plugin-tests-0.13.0 (c (n "plugin-tests") (v "0.13.0") (d (list (d (n "forc-index") (r "^0.13.0") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.13.0") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "0v850m5ix04wsbv1a4y0zg61q5lw2jghaq5zgaliqjnlw5ns060c") (r "1.69.0")))

(define-public crate-plugin-tests-0.13.2 (c (n "plugin-tests") (v "0.13.2") (d (list (d (n "forc-index") (r "^0.13.2") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.13.2") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "0h7bdjxm1sp3q79px8sscwpqv7bzx5i7h6610whbq8fjzwq9pyhz") (r "1.69.0")))

(define-public crate-plugin-tests-0.14.0 (c (n "plugin-tests") (v "0.14.0") (d (list (d (n "forc-index") (r "^0.14.0") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.14.0") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "0711ik7b7bylps4nkrzj3pinxqy1c7a803ihr3hdnqg5fgzlhdf1") (r "1.69.0")))

(define-public crate-plugin-tests-0.15.0 (c (n "plugin-tests") (v "0.15.0") (d (list (d (n "forc-index") (r "^0.15.0") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.15.0") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "0knazdaglpfzharxcj1rd7gysyb0z3jv71s0m3sd3sh0ycvdblid") (r "1.69.0")))

(define-public crate-plugin-tests-0.15.1 (c (n "plugin-tests") (v "0.15.1") (d (list (d (n "forc-index") (r "^0.15.1") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.15.1") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "1q10bxr6d8wa5czydrb13spbg6lf02j1syr8wmad3x5vfq46m71f") (r "1.69.0")))

(define-public crate-plugin-tests-0.16.0 (c (n "plugin-tests") (v "0.16.0") (d (list (d (n "forc-index") (r "^0.16.0") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.16.0") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "0dmj81crbycxn7dq72l07vsjdwwx7p1fp3jqp9hlyh25hcscypxm") (r "1.69.0")))

(define-public crate-plugin-tests-0.16.1 (c (n "plugin-tests") (v "0.16.1") (d (list (d (n "forc-index") (r "^0.16.1") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.16.1") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "0791cpqwawl55y8yja3mqbxnrc4ahxnambjvwx1bba35wr484v6x") (r "1.69.0")))

(define-public crate-plugin-tests-0.16.2 (c (n "plugin-tests") (v "0.16.2") (d (list (d (n "forc-index") (r "^0.16.2") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.16.2") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "144cc8jimdqnbdcppvwirbzwiqzd3szyd02w3minys0nb7zpj589") (r "1.69.0")))

(define-public crate-plugin-tests-0.17.0 (c (n "plugin-tests") (v "0.17.0") (d (list (d (n "forc-index") (r "^0.17.0") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.17.0") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "1y9hmbwvgqqy0i8ga1xzhh8v1f4mda6w948dhjil074a0mpd800r") (r "1.69.0")))

(define-public crate-plugin-tests-0.17.1 (c (n "plugin-tests") (v "0.17.1") (d (list (d (n "forc-index") (r "^0.17.1") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.17.1") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "0aq669azs4836ykaarv1r2xcgb3dsxsz3bqjdxx8f2sif1gxycsq") (r "1.69.0")))

(define-public crate-plugin-tests-0.17.2 (c (n "plugin-tests") (v "0.17.2") (d (list (d (n "forc-index") (r "^0.17.2") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.17.2") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "0d3j2hmkh00q6srj0l406fb10a7qgyhln2b7qf0s6mm3jd3367jp") (r "1.69.0")))

(define-public crate-plugin-tests-0.17.3 (c (n "plugin-tests") (v "0.17.3") (d (list (d (n "forc-index") (r "^0.17.3") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.17.3") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "0hcxb4cvvl4ncxsqqjvf285n72j4b9wdf2rzaznpjszi4vgiyd6m") (r "1.69.0")))

(define-public crate-plugin-tests-0.18.0 (c (n "plugin-tests") (v "0.18.0") (d (list (d (n "forc-index") (r "^0.18.0") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.18.0") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "1wwhasj71465fi080sqdx7d4cas29l6jyncmjvfsy10yvp4xk9mr") (r "1.69.0")))

(define-public crate-plugin-tests-0.18.1 (c (n "plugin-tests") (v "0.18.1") (d (list (d (n "forc-index") (r "^0.18.1") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.18.1") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "0q955bb4rs2m8z0av0rh715gkayqqvdm4xd392d5gpgmrxslgg0i") (r "1.69.0")))

(define-public crate-plugin-tests-0.18.2 (c (n "plugin-tests") (v "0.18.2") (d (list (d (n "forc-index") (r "^0.18.2") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.18.2") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "1kf2df2y75sxh2pa9x5pnr5fwvs83prm89r81qsz3rmz20mni5ph") (r "1.69.0")))

(define-public crate-plugin-tests-0.18.3 (c (n "plugin-tests") (v "0.18.3") (d (list (d (n "forc-index") (r "^0.18.3") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.18.3") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "1ncdpfif6gffg93js3gwj0nsipczrba65kmr5lcgpgbcs8kg27f8") (r "1.69.0")))

(define-public crate-plugin-tests-0.18.4 (c (n "plugin-tests") (v "0.18.4") (d (list (d (n "forc-index") (r "^0.18.4") (d #t) (k 0)) (d (n "fuel-indexer-plugin") (r "^0.18.4") (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 0)))) (h "18gr14y227495y1mhwllkmxp767gdw2msr5pa2r9fbrp248a7mx1") (r "1.69.0")))

