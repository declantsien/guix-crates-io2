(define-module (crates-io pl ug plugin-runtime-codegen) #:use-module (crates-io))

(define-public crate-plugin-runtime-codegen-0.1.0 (c (n "plugin-runtime-codegen") (v "0.1.0") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full"))) (d #t) (k 0)))) (h "0hd2n5ixp89345kx5rajcs5nj0bfwsqfx9yabkk0pd3h1iyl5h0b")))

(define-public crate-plugin-runtime-codegen-0.1.1 (c (n "plugin-runtime-codegen") (v "0.1.1") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full"))) (d #t) (k 0)))) (h "0fz6zq2b83sad85gckkfwbwd8a720f70r8c43p6zxx7nwj8gpw1v")))

