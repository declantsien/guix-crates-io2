(define-module (crates-io pl ug pluginator) #:use-module (crates-io))

(define-public crate-pluginator-1.0.0 (c (n "pluginator") (v "1.0.0") (d (list (d (n "libloading") (r "^0.7.4") (d #t) (k 0)))) (h "1bhafyvnz468q957ikcys9zncb20irf8rlqjhs64hnwj6z41w074")))

(define-public crate-pluginator-1.0.1 (c (n "pluginator") (v "1.0.1") (d (list (d (n "libloading") (r "^0.7.4") (d #t) (k 0)))) (h "07b44bqg7x0wrvs900358cwgrra5gkjkrmjppwwxwikf9r5vipgv")))

