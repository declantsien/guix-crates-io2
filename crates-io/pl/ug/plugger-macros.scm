(define-module (crates-io pl ug plugger-macros) #:use-module (crates-io))

(define-public crate-plugger-macros-0.1.0 (c (n "plugger-macros") (v "0.1.0") (h "1lfhy9h2mb1867435jmaylcvb7jwmwvrhp0aapr5kmw65ayklhh0")))

(define-public crate-plugger-macros-0.1.1 (c (n "plugger-macros") (v "0.1.1") (h "13zcsfsrarsqhshb75wzwjdl54s326ik2nw3ikzl6bwwz2030mqi")))

(define-public crate-plugger-macros-0.1.2 (c (n "plugger-macros") (v "0.1.2") (h "094n90a2c7zygl7gqgfv6bjpksnrbnaxlpmrbw8ml03nz6g5mx6s")))

(define-public crate-plugger-macros-0.1.3 (c (n "plugger-macros") (v "0.1.3") (h "1mgcpc8zs4xzdhsn8fjrbc7amvr2c2l66iqiv9mmqkcvhyabr7ny")))

(define-public crate-plugger-macros-0.1.4 (c (n "plugger-macros") (v "0.1.4") (h "0v3vw2ch7vb6fww8pi9d2wwawi7h50fbbshzhyq59i2lvl2lhaxc")))

(define-public crate-plugger-macros-0.1.5 (c (n "plugger-macros") (v "0.1.5") (h "01syl8rkmja9kp8gxxqmqvz9cz0b1fpjda274qnd2vwjp4vcyl2k")))

(define-public crate-plugger-macros-0.2.0 (c (n "plugger-macros") (v "0.2.0") (d (list (d (n "plugger-core") (r "^0.2.0") (d #t) (k 0)) (d (n "plugger-ruby") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "11xshpgbiidb508yrppjc3x6v5lp15vvjzn7lfdwmcp23ka3bzvk") (f (quote (("ruby" "plugger-ruby") ("default" "ruby"))))))

