(define-module (crates-io pl ug plugger-ruby) #:use-module (crates-io))

(define-public crate-plugger-ruby-0.1.0 (c (n "plugger-ruby") (v "0.1.0") (d (list (d (n "plugger-core") (r "^0.1.1") (d #t) (k 0)) (d (n "plugger-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "rurust") (r "^0.1.0") (d #t) (k 0)))) (h "023hf4i3h6sw2k9pl4vzj76fiz199c77sly40s4v6nmf6r5b928j")))

(define-public crate-plugger-ruby-0.1.1 (c (n "plugger-ruby") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "plugger-core") (r "^0.1.3") (d #t) (k 0)) (d (n "plugger-macros") (r "^0.1.3") (d #t) (k 0)) (d (n "rurust") (r "^0.1.4") (d #t) (k 0)))) (h "0x71kfwgihj6q0za2ns45iidaa7c1rylv7p45s13dswqpzmdz0jg")))

(define-public crate-plugger-ruby-0.2.0 (c (n "plugger-ruby") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "plugger-core") (r "^0.2.0") (d #t) (k 0)) (d (n "rurust") (r "^0.1.6") (d #t) (k 0)))) (h "07y675cps3243fhbmbz4v45s56fmd4nghx9fb5iiclxvgcjjppbk")))

(define-public crate-plugger-ruby-0.3.0 (c (n "plugger-ruby") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "plugger-core") (r "^0.3.0") (d #t) (k 0)) (d (n "rurust") (r "^0.1.6") (d #t) (k 0)))) (h "10r9xi405z7cldm4rpidz7g02jhy28vmqdmqszazif61qhqjm1ks")))

