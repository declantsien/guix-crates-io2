(define-module (crates-io pl ug pluggy-rs) #:use-module (crates-io))

(define-public crate-pluggy-rs-0.1.0 (c (n "pluggy-rs") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0js9pg2hjc0kggd7dmarzdrhk6gn547qq54xba0yxqx1kgakfw05")))

