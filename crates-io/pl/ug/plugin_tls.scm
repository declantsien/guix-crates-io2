(define-module (crates-io pl ug plugin_tls) #:use-module (crates-io))

(define-public crate-plugin_tls-0.2.0 (c (n "plugin_tls") (v "0.2.0") (d (list (d (n "abi_stable") (r "^0.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (o #t) (d #t) (k 0)))) (h "0jlcnww3ik6lhyhh6j45wqr0g5v4kgddvvv8zw7929g53mfz2c95") (f (quote (("plugin") ("host" "parking_lot") ("default"))))))

(define-public crate-plugin_tls-0.2.1 (c (n "plugin_tls") (v "0.2.1") (d (list (d (n "abi_stable") (r "^0.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1hhx1w82mks56m37bwq2qdx7k0yz2iphv2sx020qnkkv0siyxpw7") (f (quote (("plugin") ("host" "parking_lot") ("default"))))))

(define-public crate-plugin_tls-0.2.2 (c (n "plugin_tls") (v "0.2.2") (d (list (d (n "abi_stable") (r "^0.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (o #t) (d #t) (k 0)))) (h "0kjr4n8b720b7dvmjar7klqbc9rcb1kxvxalw9w068200c930p5d") (f (quote (("plugin") ("host" "parking_lot") ("default"))))))

(define-public crate-plugin_tls-0.2.3 (c (n "plugin_tls") (v "0.2.3") (d (list (d (n "abi_stable") (r "^0.9") (d #t) (k 0)))) (h "1ydazaf5j9jrlhlhw6c8r4qsg0dpl61zmr1mn0nlr4nkjbpqcdsx") (f (quote (("plugin") ("host") ("default"))))))

(define-public crate-plugin_tls-0.3.0 (c (n "plugin_tls") (v "0.3.0") (d (list (d (n "abi_stable") (r "^0.9") (d #t) (k 0)))) (h "1gvnwkxhnci86l7wlz74fff1ilpany488jc92lsr1zk9c6k6g1ny") (f (quote (("plugin") ("host") ("default"))))))

(define-public crate-plugin_tls-0.4.0 (c (n "plugin_tls") (v "0.4.0") (d (list (d (n "abi_stable") (r "^0.9") (d #t) (k 0)))) (h "0p0i922cak732n7p3c70gkx38k6hj9j1057h0qq7c6vzrs6wl0xh") (f (quote (("plugin") ("host") ("default"))))))

(define-public crate-plugin_tls-0.4.1 (c (n "plugin_tls") (v "0.4.1") (d (list (d (n "abi_stable") (r ">=0.9") (d #t) (k 0)))) (h "05bn69ajlbaa919gj7yhv852lf9yylrw5mwqqq6di129mcn4vakw") (f (quote (("plugin") ("host") ("default"))))))

