(define-module (crates-io pl ug plugin_manager_lib) #:use-module (crates-io))

(define-public crate-plugin_manager_lib-0.1.1 (c (n "plugin_manager_lib") (v "0.1.1") (d (list (d (n "libloader") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lsrvz1m659385b97b38wimnxa9qzcl8nbap0ag6bvk8fg6iaml2")))

(define-public crate-plugin_manager_lib-0.1.2 (c (n "plugin_manager_lib") (v "0.1.2") (d (list (d (n "libloader") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "03mfczq298h3v0y6xyc617ciipyy6jk0hgf67kns44w9ja22nybz")))

(define-public crate-plugin_manager_lib-0.1.3 (c (n "plugin_manager_lib") (v "0.1.3") (d (list (d (n "libloader") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "16fi7nsgg1f6nliyp0gi1i19p7sfyqsa5cll2hfqbgzdjmy7rc4j")))

