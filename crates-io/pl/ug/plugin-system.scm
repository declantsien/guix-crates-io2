(define-module (crates-io pl ug plugin-system) #:use-module (crates-io))

(define-public crate-plugin-system-0.1.1 (c (n "plugin-system") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("full"))) (d #t) (k 0)))) (h "1rxy8rfx4v1lqpcyw42rjxxcf4721m4gxb0f7hjp60rnyc8mf2fy")))

