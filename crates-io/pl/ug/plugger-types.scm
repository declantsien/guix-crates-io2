(define-module (crates-io pl ug plugger-types) #:use-module (crates-io))

(define-public crate-plugger-types-0.1.0 (c (n "plugger-types") (v "0.1.0") (h "04rlji41pw8gxiyf6lnya5v0gzllqh9flrs1lnj1adbqsfy3a683")))

(define-public crate-plugger-types-0.1.1 (c (n "plugger-types") (v "0.1.1") (h "096byqqac7dclnvb0822bjx4sp2b3n87y8d3vdffnsw07fz8gp66")))

(define-public crate-plugger-types-0.1.2 (c (n "plugger-types") (v "0.1.2") (h "1xq10br8gga8k5czls0d72w1nyz1hlkxs7gl6rhnjf1646s9x7i8")))

(define-public crate-plugger-types-0.1.3 (c (n "plugger-types") (v "0.1.3") (h "03dfxmxcf38g28vasmdzl92ck3f95d7pmxi186vxby2zf06hl3fa")))

(define-public crate-plugger-types-0.1.5 (c (n "plugger-types") (v "0.1.5") (h "0hvs94ii229m5ivyn832gxni40f8zp6aamqyh6g5grc4s24xzyk5")))

