(define-module (crates-io pl ug plugin-runtime) #:use-module (crates-io))

(define-public crate-plugin-runtime-0.1.0 (c (n "plugin-runtime") (v "0.1.0") (d (list (d (n "plugin-runtime-codegen") (r "^0.1.0") (d #t) (k 0)) (d (n "state") (r "^0.4.1") (d #t) (k 0)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)))) (h "10irhmv69r9cwjvyr42rb2pnbx060kbppnsff7kiw2q059gr68r4")))

(define-public crate-plugin-runtime-0.1.1 (c (n "plugin-runtime") (v "0.1.1") (d (list (d (n "plugin-runtime-codegen") (r "^0.1.1") (d #t) (k 0)) (d (n "state") (r "^0.4.1") (d #t) (k 0)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)))) (h "0qgfhaclivwfyqci0lp1lz6a3cafga1liy745hjfd8dyvnjyxq2q")))

