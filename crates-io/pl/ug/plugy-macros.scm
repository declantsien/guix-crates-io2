(define-module (crates-io pl ug plugy-macros) #:use-module (crates-io))

(define-public crate-plugy-macros-0.1.0 (c (n "plugy-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1szlx0232qclzrhgzfvzygg4vh0ymzb1zr9i7gcfxa485f03sm5c")))

(define-public crate-plugy-macros-0.1.1 (c (n "plugy-macros") (v "0.1.1") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1m42ancw90jl3ajljdbv28s2ky2p4am440s2i0j30bv5h2iy4nz7")))

(define-public crate-plugy-macros-0.2.0 (c (n "plugy-macros") (v "0.2.0") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1c4xk6m9m76886xl3z2z2bw0adxzk8pl769n8vgzd5gw0whs3q8c")))

(define-public crate-plugy-macros-0.2.1 (c (n "plugy-macros") (v "0.2.1") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0qhmaiyvkpchrqx6s4pb45qcjviisairv2qq6skxgqijlsv71yy9")))

(define-public crate-plugy-macros-0.3.0 (c (n "plugy-macros") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1pcq9f2l910mzk5dlmszwdpzh8cpcj1571wayn8m2s6iwyi8qs46")))

(define-public crate-plugy-macros-0.3.1 (c (n "plugy-macros") (v "0.3.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1nrm70cq2s3r7wd1kls60nai833bfvp0hmp1vn6g24x0gpmvh8lq")))

