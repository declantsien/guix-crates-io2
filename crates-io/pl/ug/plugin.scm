(define-module (crates-io pl ug plugin) #:use-module (crates-io))

(define-public crate-plugin-0.0.0 (c (n "plugin") (v "0.0.0") (d (list (d (n "phantom") (r "*") (d #t) (k 0)) (d (n "typemap") (r "*") (d #t) (k 0)))) (h "0c05m5jm9vzc077p0pxx6zfhdzrzr81av20fs9j21dbp0ykpqr6z")))

(define-public crate-plugin-0.0.1 (c (n "plugin") (v "0.0.1") (d (list (d (n "phantom") (r "*") (d #t) (k 0)) (d (n "typemap") (r "*") (d #t) (k 0)))) (h "1vqy1bg6ifybjvig1lpphjkn1r0k09ygwy9756dafmgkwxk36axi")))

(define-public crate-plugin-0.0.2 (c (n "plugin") (v "0.0.2") (d (list (d (n "phantom") (r "*") (d #t) (k 0)) (d (n "typemap") (r "*") (d #t) (k 0)))) (h "0lp9nml2g8a694s163p00nz22pwf47garr6fdna4nbs5x22g8y94")))

(define-public crate-plugin-0.1.0 (c (n "plugin") (v "0.1.0") (d (list (d (n "phantom") (r "*") (d #t) (k 0)) (d (n "typemap") (r "*") (d #t) (k 0)))) (h "0130x3d92j5nn2q55mx33j8600kpkzwj0vpi4qpzfg41s5lzpgd6")))

(define-public crate-plugin-0.2.0 (c (n "plugin") (v "0.2.0") (d (list (d (n "phantom") (r "*") (d #t) (k 0)) (d (n "typemap") (r "*") (d #t) (k 0)))) (h "0y9v0kbnddpw8abkn0224fjzmwapvm4p2raw4alwwmp2f9arkhga")))

(define-public crate-plugin-0.2.1 (c (n "plugin") (v "0.2.1") (d (list (d (n "typemap") (r "*") (d #t) (k 0)) (d (n "void") (r "*") (d #t) (k 2)))) (h "1xsg1k1w5gdqyd6b3saq00afdp2vb2hblkziq36nwaxbghr3b82r")))

(define-public crate-plugin-0.2.2 (c (n "plugin") (v "0.2.2") (d (list (d (n "typemap") (r "*") (d #t) (k 0)) (d (n "void") (r "*") (d #t) (k 2)))) (h "08p6yhy62812r8cc3k0a61dg496njn2ch28fcdfbf1h4z6g51lvi")))

(define-public crate-plugin-0.2.3 (c (n "plugin") (v "0.2.3") (d (list (d (n "typemap") (r "*") (d #t) (k 0)) (d (n "void") (r "*") (d #t) (k 2)))) (h "1y5867mv43qcp8is0psaygb7ihm0cf2y8arii04f9f48qmxljgdc")))

(define-public crate-plugin-0.2.4 (c (n "plugin") (v "0.2.4") (d (list (d (n "typemap") (r "*") (d #t) (k 0)) (d (n "void") (r "*") (d #t) (k 2)))) (h "158r2hy8gxwzb6dzj6ib9vlnj5sah5hmrwmmiri01shl7a6i2lj0")))

(define-public crate-plugin-0.2.5 (c (n "plugin") (v "0.2.5") (d (list (d (n "typemap") (r "*") (d #t) (k 0)) (d (n "void") (r "*") (d #t) (k 2)))) (h "146hgz9nzkd2yyxv914xl9ppwjc4w52dsgv4jnmj7jp5zb69yfiw")))

(define-public crate-plugin-0.2.6 (c (n "plugin") (v "0.2.6") (d (list (d (n "typemap") (r "*") (d #t) (k 0)) (d (n "void") (r "*") (d #t) (k 2)))) (h "1q7nghkpvxxr168y2jnzh3w7qc9vfrby9n7ygy3xpj0bj71hsshs")))

