(define-module (crates-io pl ug plugin-test-api) #:use-module (crates-io))

(define-public crate-plugin-test-api-0.1.0 (c (n "plugin-test-api") (v "0.1.0") (h "007agzn5mm813awxx1c0zxvk84vrayii9dhh7abb1jmyq6z18hla")))

(define-public crate-plugin-test-api-0.1.1 (c (n "plugin-test-api") (v "0.1.1") (h "19ibapy5qc90yp67jkwwhyaxd6f6rf95bsr55djfdjlv07i2f5m1")))

(define-public crate-plugin-test-api-0.1.2 (c (n "plugin-test-api") (v "0.1.2") (h "0ps81mp2im37pgbb6n1ndpa488f1q40dvbwj7lyk5509y78lyb6x")))

