(define-module (crates-io pl ug plugkit) #:use-module (crates-io))

(define-public crate-plugkit-0.0.1 (c (n "plugkit") (v "0.0.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0770rsizxp3c7hx7bqr928an4asvvs0hah4fwjkhc9gcsrn1x0zs")))

(define-public crate-plugkit-0.0.2 (c (n "plugkit") (v "0.0.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fix7pww1j675zc3zg5azz3m7hzns2pln83g0wdafx2s7v321svm")))

(define-public crate-plugkit-0.0.3 (c (n "plugkit") (v "0.0.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jw8mhpm9ylaxzcvl7kmrcciqlxy5r0fb7jiy34108g7l5w32xdx")))

(define-public crate-plugkit-0.0.4 (c (n "plugkit") (v "0.0.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qak57z1315rnbv8jwc6a5fcb2k76zfi3a0jxjlfbwz8q5c5fiv3")))

(define-public crate-plugkit-0.0.5 (c (n "plugkit") (v "0.0.5") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jxrn3d22z0cw48g9wlmx8mmr7a6hv5ixlkvnb4d6y7xb93lfga8")))

(define-public crate-plugkit-0.0.6 (c (n "plugkit") (v "0.0.6") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15f7ayifdzm77hk3dmfckqc0kpjqimq5cmj9lkiaya5v5lif8rnp")))

(define-public crate-plugkit-0.0.7 (c (n "plugkit") (v "0.0.7") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gqa7p797sfpj1kjsgbg1j7kqaq7xdiaxi0s15d6qxb52zaqldgl")))

(define-public crate-plugkit-0.0.8 (c (n "plugkit") (v "0.0.8") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fcms74dfs178y2hc417ljpysi08ppnv900dic26cgfqv9r0d681")))

(define-public crate-plugkit-0.0.9 (c (n "plugkit") (v "0.0.9") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0dy2m52rnav8iwsr6l2f1ryf83yky7vc3avzkg724a0lrgfm819g")))

