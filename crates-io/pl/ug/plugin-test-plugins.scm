(define-module (crates-io pl ug plugin-test-plugins) #:use-module (crates-io))

(define-public crate-plugin-test-plugins-0.1.0 (c (n "plugin-test-plugins") (v "0.1.0") (d (list (d (n "plugin-test-api") (r "^0.1") (d #t) (k 0)))) (h "1k7z8ds0hlzaaxcflna1hpar8jjk57y76jlyfnb4vdd2dcvvxbvd")))

(define-public crate-plugin-test-plugins-0.1.1 (c (n "plugin-test-plugins") (v "0.1.1") (d (list (d (n "plugin-test-api") (r "^0.1") (d #t) (k 0)))) (h "0xx45j4yybaig29s43gv56a22wrbn6yvlca6y96fihqba56sp5d8")))

(define-public crate-plugin-test-plugins-0.1.2 (c (n "plugin-test-plugins") (v "0.1.2") (d (list (d (n "plugin-test-api") (r "^0.1") (d #t) (k 0)))) (h "1pasw8i2jkzvjayjvkgpikm4knqxhj0gsvd7v5i77pbhp2nfkpi5")))

