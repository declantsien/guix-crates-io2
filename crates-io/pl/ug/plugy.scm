(define-module (crates-io pl ug plugy) #:use-module (crates-io))

(define-public crate-plugy-0.1.0 (c (n "plugy") (v "0.1.0") (d (list (d (n "plugy-core") (r "^0.1.0") (d #t) (k 0)) (d (n "plugy-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "plugy-runtime") (r "^0.1.0") (d #t) (k 0)))) (h "1zlvhydjrc32blkny7pmg21lx3qschylznzza6hi3ay9xi9c4kbm")))

(define-public crate-plugy-0.1.1 (c (n "plugy") (v "0.1.1") (d (list (d (n "plugy-core") (r "^0.1.1") (d #t) (k 0)) (d (n "plugy-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "plugy-runtime") (r "^0.1.1") (d #t) (k 0)))) (h "1z1kgg3fqn5px05hkigfb4bfhby3g4ykaimzm9m691a7n3hkrvzr")))

(define-public crate-plugy-0.2.0 (c (n "plugy") (v "0.2.0") (d (list (d (n "plugy-core") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "plugy-macros") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "plugy-runtime") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1y9ba5a5jjgxw1ps1wsbjivbzkd6vaqd55zqj38akm9xhd3617kn") (f (quote (("runtime" "plugy-runtime") ("macros" "plugy-macros") ("default" "core" "macros") ("core" "plugy-core"))))))

(define-public crate-plugy-0.2.1 (c (n "plugy") (v "0.2.1") (d (list (d (n "plugy-core") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "plugy-macros") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "plugy-runtime") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "19zr0v0q1s6jj8lc3krrfy24k7dcrq3scfiqbf51bl3xay5i01am") (f (quote (("runtime" "plugy-runtime") ("macros" "plugy-macros") ("default" "core" "macros") ("core" "plugy-core"))))))

(define-public crate-plugy-0.3.0 (c (n "plugy") (v "0.3.0") (d (list (d (n "plugy-core") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "plugy-macros") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "plugy-runtime") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "0n36m90dyw5p3raapd5x1nggmflx6rfm61jkh5ri9w7a7bdh17ni") (f (quote (("runtime" "plugy-runtime") ("macros" "plugy-macros") ("default" "core" "macros") ("core" "plugy-core"))))))

(define-public crate-plugy-0.3.1 (c (n "plugy") (v "0.3.1") (d (list (d (n "plugy-core") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "plugy-macros") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "plugy-runtime") (r "^0.3.1") (o #t) (d #t) (k 0)))) (h "0zhr9liggh2whqjfbsblcw2bx8y53lsznnlvahcq8h7z78bi62fq") (f (quote (("runtime" "plugy-runtime") ("macros" "plugy-macros") ("default" "core" "macros") ("core" "plugy-core"))))))

