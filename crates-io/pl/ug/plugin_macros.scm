(define-module (crates-io pl ug plugin_macros) #:use-module (crates-io))

(define-public crate-plugin_macros-0.1.0 (c (n "plugin_macros") (v "0.1.0") (d (list (d (n "failure") (r "~0.1") (d #t) (k 0)) (d (n "fxhash") (r "~0.2") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "libloading") (r "~0.5") (d #t) (k 0)))) (h "044x9k0aidijal9w3j500p1hf92xfxks8x87q1cypa1l5l35ck6g") (f (quote (("swisstable" "hashbrown") ("nightly") ("fx" "fxhash") ("default" "swisstable"))))))

