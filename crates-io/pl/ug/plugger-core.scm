(define-module (crates-io pl ug plugger-core) #:use-module (crates-io))

(define-public crate-plugger-core-0.1.0 (c (n "plugger-core") (v "0.1.0") (d (list (d (n "plugger-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "plugger-types") (r "^0.1.0") (d #t) (k 0)))) (h "05yr34pyvyv5jplvrs8jm33d1bymylrpn23ja4pywr31jh4n2rgv")))

(define-public crate-plugger-core-0.1.1 (c (n "plugger-core") (v "0.1.1") (d (list (d (n "plugger-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "plugger-types") (r "^0.1.1") (d #t) (k 0)))) (h "1bqpb9a5fxkkgidcjfyj4skspbc6p8vka501z5fz76k0qfq5pk02")))

(define-public crate-plugger-core-0.1.2 (c (n "plugger-core") (v "0.1.2") (d (list (d (n "plugger-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "plugger-types") (r "^0.1.2") (d #t) (k 0)))) (h "1ls1kkin82ixdig551vrpmx0jzg161xq16lk5h2xh207hawaq4k3")))

(define-public crate-plugger-core-0.1.3 (c (n "plugger-core") (v "0.1.3") (d (list (d (n "plugger-macros") (r "^0.1.3") (d #t) (k 0)) (d (n "plugger-types") (r "^0.1.3") (d #t) (k 0)))) (h "08rnpvbwcb0aj1sgg1b8jqgsnh3dp21dbydvhk6624xcyfsdzb6r")))

(define-public crate-plugger-core-0.1.5 (c (n "plugger-core") (v "0.1.5") (d (list (d (n "plugger-macros") (r "^0.1.5") (d #t) (k 0)) (d (n "plugger-types") (r "^0.1.5") (d #t) (k 0)))) (h "08k4xvvl6rq3qq3xndczlpwkp271pmygkal4d5yxrk1zdydbz33s")))

(define-public crate-plugger-core-0.2.0 (c (n "plugger-core") (v "0.2.0") (h "0g09gwqbd1rv66ikz01zfb08mp95p1vx2qxbqhkh0ahyxy493r0a")))

(define-public crate-plugger-core-0.3.0 (c (n "plugger-core") (v "0.3.0") (h "04q0f9sfphrpn7ylfwhl44am2fc3baqhxy9cjkjn59ax4sq01i81")))

