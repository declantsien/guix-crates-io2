(define-module (crates-io pl t- plt-rs) #:use-module (crates-io))

(define-public crate-plt-rs-0.1.0 (c (n "plt-rs") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "proc-maps") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0p8c64b4152r8pja0nd1g6fs7qm872k2h65dcid31wrch9d73b1j")))

(define-public crate-plt-rs-0.2.0 (c (n "plt-rs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1x0ibbvjr2xcs1vrxsqb8xc2jrym8amslax6lnkqiyc0kb0wavzg")))

