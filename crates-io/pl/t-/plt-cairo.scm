(define-module (crates-io pl t- plt-cairo) #:use-module (crates-io))

(define-public crate-plt-cairo-0.1.0 (c (n "plt-cairo") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cairo-rs") (r "^0.16") (d #t) (k 0)) (d (n "draw") (r "^0.4.0") (d #t) (k 0) (p "plt-draw")) (d (n "png") (r "^0.17") (o #t) (d #t) (k 0)))) (h "0r04hk699k88max3hm8sxh68dqp9mf167mpqmiz109xa392158ja") (f (quote (("svg" "cairo-rs/svg") ("default" "png" "svg")))) (s 2) (e (quote (("png" "dep:png" "cairo-rs/png"))))))

