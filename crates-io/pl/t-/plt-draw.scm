(define-module (crates-io pl t- plt-draw) #:use-module (crates-io))

(define-public crate-plt-draw-0.1.0 (c (n "plt-draw") (v "0.1.0") (d (list (d (n "cairo-rs") (r "^0.15") (f (quote ("png" "svg"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 0)))) (h "0g3l17lz5y6mwvrq6hl7kr6bk09majiq2j6m8yq3galvy89brxjd")))

(define-public crate-plt-draw-0.2.0 (c (n "plt-draw") (v "0.2.0") (d (list (d (n "cairo-rs") (r "^0.15") (f (quote ("png" "svg"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 0)))) (h "00s9bhl19l3qf9cvrw2xg9334gdn16lkc4clw03jglcy59srn4x9")))

(define-public crate-plt-draw-0.3.0 (c (n "plt-draw") (v "0.3.0") (d (list (d (n "cairo-rs") (r "^0.15") (f (quote ("png" "svg"))) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 0)))) (h "1avxk3vkz9snkv8c9pr3la4pvw6nfq72dqh10xvbzjimbpy0hcid")))

(define-public crate-plt-draw-0.4.0 (c (n "plt-draw") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0s3by6sx9v0q9jqnqlds9748xqx7ky0sf1ffmr59yv0y325bpqvd")))

