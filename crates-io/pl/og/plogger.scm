(define-module (crates-io pl og plogger) #:use-module (crates-io))

(define-public crate-plogger-0.0.1 (c (n "plogger") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1p93qb22zk5rv4dwdmw3zgw80xzwi60bs27lyjwswnfv9x9z7dld")))

