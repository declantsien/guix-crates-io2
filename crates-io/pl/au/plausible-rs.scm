(define-module (crates-io pl au plausible-rs) #:use-module (crates-io))

(define-public crate-plausible-rs-0.1.0 (c (n "plausible-rs") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vzsxxpgid1kcclwvsbwv2by24ahjqpv08k6f81rz3qwyq85jrv0") (r "1.63")))

(define-public crate-plausible-rs-0.1.1 (c (n "plausible-rs") (v "0.1.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0z1mmi9f31w35kgnjfn07mm4fn1zzziy3f8lpai63d9j8nnj46p9") (r "1.63")))

(define-public crate-plausible-rs-0.1.2 (c (n "plausible-rs") (v "0.1.2") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1s8id9bfhqp787bh05w6jnbbwhpsavfbh5nv7mzyx2b7ac8mdc4l") (r "1.63")))

(define-public crate-plausible-rs-0.1.3 (c (n "plausible-rs") (v "0.1.3") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a10wsp4310i6s25gh16c6cfg9k035d2avc0rg1bh4pqbb9l58hf") (r "1.75")))

