(define-module (crates-io cy rc cyrconv) #:use-module (crates-io))

(define-public crate-cyrconv-0.3.0 (c (n "cyrconv") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "1dyx6dw46dag0kn2p6g71mbhyq2lpq4aqbnkdpxjc198qav81fi7") (y #t)))

(define-public crate-cyrconv-0.3.1 (c (n "cyrconv") (v "0.3.1") (d (list (d (n "serde") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "1b97x1mpxrwfxzmqli7nwdl4ipcd29lrphipv2y6sxqmap6dd43v")))

(define-public crate-cyrconv-0.3.2 (c (n "cyrconv") (v "0.3.2") (d (list (d (n "serde") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "06pkirh9xw1j2rbr59v0zrpzckhia9ayfgxbw5afgh7j9fpcy3ws")))

(define-public crate-cyrconv-0.3.3 (c (n "cyrconv") (v "0.3.3") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "1m02gccafg2v8arbgsq0754p7pg1cwdalcjzll8zn9b0mbwgfi9h")))

(define-public crate-cyrconv-0.4.0 (c (n "cyrconv") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0wnhihpkvn078xicfgg5yf6wabbnz3byajy6n091lcz4vz0kpq9p")))

(define-public crate-cyrconv-0.4.1 (c (n "cyrconv") (v "0.4.1") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.97") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0abzfz4fhamshy89264pxl28fdp6hxii0zwh476n9r4lhlx6lksx")))

