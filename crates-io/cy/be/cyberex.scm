(define-module (crates-io cy be cyberex) #:use-module (crates-io))

(define-public crate-cyberex-0.1.0 (c (n "cyberex") (v "0.1.0") (h "1czychidfxczamlh3585fnqx58fk77vb4pfbb2rnjsvcc73vslk7")))

(define-public crate-cyberex-0.1.1 (c (n "cyberex") (v "0.1.1") (h "07vajjha33n2zhylk15wrfij5jhmfvkd5d91awxk1cr8dxhmcnw5")))

(define-public crate-cyberex-0.1.2 (c (n "cyberex") (v "0.1.2") (h "0zll044mjaaczplywmgcvn94vm8y2cq2wixyb2v19pmrv4yjk3wq")))

(define-public crate-cyberex-0.1.3 (c (n "cyberex") (v "0.1.3") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "156nmlmxlpwzg7qnzkbvnjrik3v5m0w4piz5ai0gml61hk24xrz1") (f (quote (("full" "enable-async") ("enable-async" "tokio") ("default"))))))

(define-public crate-cyberex-0.1.4 (c (n "cyberex") (v "0.1.4") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "036yfvwcz10dk9149pp18q72dyg3wdf5jblx92jpdla3wfi6v925") (f (quote (("full" "enable-async") ("enable-async" "tokio") ("default"))))))

(define-public crate-cyberex-0.1.5 (c (n "cyberex") (v "0.1.5") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "11cqvcwnqbjgwsnn047xf6dr4vzwwdr3wj2jlf71f0l759wxf6r1") (f (quote (("full" "enable-async") ("enable-async" "tokio") ("default"))))))

(define-public crate-cyberex-0.1.6 (c (n "cyberex") (v "0.1.6") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0b9dghyjgdf7d39kcwhl0rg6g02g3f2n8avfk60pzhqkir0q6vbb") (f (quote (("full" "enable-async") ("enable-async" "tokio") ("default"))))))

(define-public crate-cyberex-0.1.7 (c (n "cyberex") (v "0.1.7") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1971yddhy6bgmy6pl59khwkr5qqpm0hshqrsi8cagrb0zsqjix4h") (f (quote (("full" "enable-async") ("enable-async" "tokio") ("default"))))))

(define-public crate-cyberex-0.1.8 (c (n "cyberex") (v "0.1.8") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0fx8723pxgmwi6vsq26cx1qdxc8n1mji0fm208l35ryrjy06g61i") (f (quote (("full" "enable-async") ("enable-async" "tokio") ("default"))))))

(define-public crate-cyberex-0.1.9 (c (n "cyberex") (v "0.1.9") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1wzb1anmpx1qb3h6w246svpsdxzwcsnqng2n9rakcqvbnrgiybzq") (f (quote (("full" "enable-async") ("enable-async" "tokio") ("default"))))))

(define-public crate-cyberex-0.1.11 (c (n "cyberex") (v "0.1.11") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0qsd3hdyn6nidqsrvwc6d4sv07xpqps7k0agx76avv61xffapz8z") (f (quote (("full" "enable-async") ("enable-async" "tokio") ("default"))))))

(define-public crate-cyberex-0.1.12 (c (n "cyberex") (v "0.1.12") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "08x5gvrva0l7q5avhhc56yqgbk38hzyfgb8h8kk2sfzbwbb8agcv") (f (quote (("full" "enable-async") ("enable-async" "tokio") ("default"))))))

(define-public crate-cyberex-0.1.13 (c (n "cyberex") (v "0.1.13") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "02l8nr28bnakkz8xa28b2g62vfijcnccx8qhdr1jz0r0hyflh1gh") (f (quote (("full" "enable-async") ("enable-async" "tokio") ("default"))))))

(define-public crate-cyberex-0.1.14 (c (n "cyberex") (v "0.1.14") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "106qa1an0m2qd1mgsbvcbpjcd231sykpqjcwvrrpcsxsyppc3vpk") (f (quote (("full" "enable-async") ("enable-async" "tokio") ("default"))))))

(define-public crate-cyberex-0.1.15 (c (n "cyberex") (v "0.1.15") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1056ikp0140fbban614bzk9ygqyc34yr5l5an2xys9z7zv0g600b") (f (quote (("full" "enable-async") ("enable-async" "tokio") ("default"))))))

(define-public crate-cyberex-0.1.16 (c (n "cyberex") (v "0.1.16") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "060wr8gnc1k38y3jf42n8nn7qq4b2xngivs0vkaa34spp2769wjk") (f (quote (("full" "enable-async") ("enable-async" "tokio") ("default"))))))

(define-public crate-cyberex-0.1.17 (c (n "cyberex") (v "0.1.17") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1rzi84b0slaf4inqixsjbxpz5qvlmp9xn34j1mhyr8hpx45aywsq") (f (quote (("full" "enable-async") ("enable-async" "tokio") ("default"))))))

(define-public crate-cyberex-0.1.18 (c (n "cyberex") (v "0.1.18") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "01iwjqhz704bwp41jnlm2d34nzwlrxipwf5l54m260mc3vng57hl") (f (quote (("full" "enable-async") ("enable-async" "tokio") ("default"))))))

(define-public crate-cyberex-0.1.19 (c (n "cyberex") (v "0.1.19") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "14zh50mhmp2yasrjb6whd0v9ys6yx5sj6f3xbrxblywdvxaxi49z") (f (quote (("full" "enable-async") ("enable-async" "tokio") ("default"))))))

(define-public crate-cyberex-0.1.20 (c (n "cyberex") (v "0.1.20") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0mnr0sya36a8a7d2jips7yvc16kv96fba9i3h87fyinzqx1zxkwj") (f (quote (("full" "enable-async") ("enable-async" "tokio") ("default"))))))

(define-public crate-cyberex-0.1.21 (c (n "cyberex") (v "0.1.21") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1w5pr9qx891czlclyrysfp6paniiayjmmv1ynicikp17906s55ba") (f (quote (("full" "enable-async") ("enable-async" "tokio") ("default"))))))

(define-public crate-cyberex-0.1.22 (c (n "cyberex") (v "0.1.22") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0pkqsr3izqcqng7l54sz5vq7w4q7ibsm766y437v9hhhc97pz671") (f (quote (("full" "enable-async") ("enable-async" "tokio") ("default"))))))

(define-public crate-cyberex-0.1.23 (c (n "cyberex") (v "0.1.23") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "08zv94146rjc38yslxgk60c9vif89vw0qcxdiqxw1k1ik8i12kpr") (f (quote (("full" "enable-async") ("enable-async" "tokio") ("default"))))))

(define-public crate-cyberex-0.1.25 (c (n "cyberex") (v "0.1.25") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1vpdnd7lqs16p95aqnhbmnfbpdgji317a6qz14y8g7hi74d67z49") (f (quote (("full" "enable-async") ("default")))) (s 2) (e (quote (("enable-async" "dep:tokio"))))))

(define-public crate-cyberex-0.1.26 (c (n "cyberex") (v "0.1.26") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "00qr2hhal8inaalx609gn6y4c5ccf9k05darx13y2a9i5d6qx4yz") (f (quote (("full" "enable-async") ("default")))) (s 2) (e (quote (("enable-async" "dep:tokio"))))))

(define-public crate-cyberex-0.1.27 (c (n "cyberex") (v "0.1.27") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1dpf8p2m576d9dsgczf1kxypd6nsjy3k8q6jra913j0i15b84j0z") (f (quote (("full" "enable-async") ("default")))) (s 2) (e (quote (("enable-async" "dep:tokio"))))))

(define-public crate-cyberex-0.1.28 (c (n "cyberex") (v "0.1.28") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1i8252z5ynlmp6p783sjawibdnmx15z3k4rlg6inlbmqi4df2v3w") (f (quote (("full" "enable-async") ("default")))) (s 2) (e (quote (("enable-async" "dep:tokio"))))))

(define-public crate-cyberex-0.2.1 (c (n "cyberex") (v "0.2.1") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "104kfibg5favm89a9ld9q5sbl3n7cnmxg672q3bs8rp028rqwkks") (f (quote (("full" "enable-async") ("default")))) (s 2) (e (quote (("enable-async" "dep:tokio"))))))

(define-public crate-cyberex-0.2.2 (c (n "cyberex") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "127bl69z6sjiz9pc16y0jahh4h6mnnhykdd27wdqr65chk7c0hxb") (f (quote (("full" "enable-async") ("default")))) (s 2) (e (quote (("enable-async" "dep:tokio"))))))

(define-public crate-cyberex-0.2.3 (c (n "cyberex") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "12wajp1ydx8iy1kw7abnv3xy59z2yi9f4zv4m61zmgcvy687869d") (f (quote (("full" "enable-async") ("default")))) (s 2) (e (quote (("enable-async" "dep:tokio"))))))

(define-public crate-cyberex-0.2.4 (c (n "cyberex") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1ciysx26wy2mxy1rhsxpalchjlhw2v9bd2bamqlqv4042qni3b57") (f (quote (("full" "enable-async") ("default")))) (s 2) (e (quote (("enable-async" "dep:tokio"))))))

(define-public crate-cyberex-0.3.1 (c (n "cyberex") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "12aacdjfbkkl8gpamgyvmjinv7b04vxm9kdglbyav8h62qhm8dv4") (f (quote (("full" "enable-async") ("default")))) (s 2) (e (quote (("enable-async" "dep:tokio"))))))

(define-public crate-cyberex-0.3.2 (c (n "cyberex") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1zd2scnzzl57hcq35j29srl2drhg8xv1bap8gigaz6vfpnp5iid6") (f (quote (("full" "enable-async") ("default")))) (s 2) (e (quote (("enable-async" "dep:tokio"))))))

(define-public crate-cyberex-0.3.3 (c (n "cyberex") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1nghk21qg18pyj9q8bm2h8wrfir3135wy0q0fg97a9w6ycxxgf2l") (f (quote (("full" "enable-async") ("default")))) (s 2) (e (quote (("enable-async" "dep:tokio"))))))

(define-public crate-cyberex-0.3.4 (c (n "cyberex") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0hzh0jvcy5njz1ljx48kzc0f6xlnrgvxyl035i7f39xg8bdbdb0f") (f (quote (("full" "enable-async") ("default")))) (s 2) (e (quote (("enable-async" "dep:tokio"))))))

(define-public crate-cyberex-0.3.5 (c (n "cyberex") (v "0.3.5") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1n0i5w2nyq6i17j4yanykf4pfnfrgc6lq5xg3891hy2fwxl3dlxh") (f (quote (("full" "enable-async") ("default")))) (s 2) (e (quote (("enable-async" "dep:tokio"))))))

(define-public crate-cyberex-0.3.6 (c (n "cyberex") (v "0.3.6") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "12wi114hj8wq9f68lj5nf2arkqjdgfpd1bzil4v9v1zk09703g7y") (f (quote (("full" "enable-async") ("default")))) (s 2) (e (quote (("enable-async" "dep:tokio"))))))

(define-public crate-cyberex-0.3.7 (c (n "cyberex") (v "0.3.7") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0bv26ygmb0lywk9rarp8s329szn06xr3472mpqhdgi2fbrawhaam") (f (quote (("full" "enable-async") ("default")))) (s 2) (e (quote (("enable-async" "dep:tokio"))))))

(define-public crate-cyberex-0.3.8 (c (n "cyberex") (v "0.3.8") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "0w0d4lxyagm3b34zy6cm1hxsvwa9363ykxvar6wvizqrygjj4jax") (f (quote (("full" "enable-async") ("default")))) (s 2) (e (quote (("enable-async" "dep:tokio"))))))

