(define-module (crates-io cy be cyber) #:use-module (crates-io))

(define-public crate-cyber-0.1.0 (c (n "cyber") (v "0.1.0") (h "08npl6c1sf2wcx374sbcpg0czp8f4slgjbj7f25nwa9nchc82vxy")))

(define-public crate-cyber-0.1.1 (c (n "cyber") (v "0.1.1") (d (list (d (n "bech32") (r "^0.7.2") (d #t) (k 0)) (d (n "hdwallet") (r "^0.2.5") (d #t) (k 0)) (d (n "ripemd160") (r "^0.9.1") (d #t) (k 0)) (d (n "secp256k1") (r "^0.17.2") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)) (d (n "subtle-encoding") (r "^0.5.1") (f (quote ("bech32-preview"))) (d #t) (k 0)) (d (n "tiny-bip39") (r "^0.7.3") (d #t) (k 0)))) (h "0gp58fgvpgj8212sxvjbpqr9mivmw9rafa435616qx3rdcyx3jis")))

(define-public crate-cyber-0.1.2 (c (n "cyber") (v "0.1.2") (d (list (d (n "bech32") (r "^0.7.2") (d #t) (k 0)) (d (n "hdwallet") (r "^0.2.5") (d #t) (k 0)) (d (n "ripemd160") (r "^0.9.1") (d #t) (k 0)) (d (n "secp256k1") (r "^0.17.2") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)) (d (n "subtle-encoding") (r "^0.5.1") (f (quote ("bech32-preview"))) (d #t) (k 0)) (d (n "tiny-bip39") (r "^0.7.3") (d #t) (k 0)))) (h "0gpg3basv5q5y0qg4r1sivh851m911npp8mw8zcky4gyd2g2lyya")))

(define-public crate-cyber-0.1.3 (c (n "cyber") (v "0.1.3") (d (list (d (n "bech32") (r "^0.7.2") (d #t) (k 0)) (d (n "hdwallet") (r "^0.2.5") (d #t) (k 0)) (d (n "ripemd160") (r "^0.9.1") (d #t) (k 0)) (d (n "secp256k1") (r "^0.17.2") (d #t) (k 0)) (d (n "sha2") (r "^0.9.1") (d #t) (k 0)) (d (n "subtle-encoding") (r "^0.5.1") (f (quote ("bech32-preview"))) (d #t) (k 0)) (d (n "tiny-bip39") (r "^0.7.3") (d #t) (k 0)))) (h "1a7xjrkq5b16sa7887n0qmss6hqjw9a5342vd360vk1bzzmvpb6n")))

(define-public crate-cyber-0.1.4 (c (n "cyber") (v "0.1.4") (d (list (d (n "bech32") (r "^0.9.0") (d #t) (k 0)) (d (n "hdwallet") (r "^0.3.1") (d #t) (k 0)) (d (n "ripemd") (r "^0.1.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "subtle-encoding") (r "^0.5.1") (f (quote ("bech32-preview"))) (d #t) (k 0)) (d (n "tiny-bip39") (r "^0.8.2") (d #t) (k 0)))) (h "0a89hda6v0x9h7qzhpg9dv4hyb0djkffva0q62sdhcww3siymik0")))

(define-public crate-cyber-1.0.0 (c (n "cyber") (v "1.0.0") (d (list (d (n "bech32") (r "^0.9.0") (d #t) (k 0)) (d (n "hdwallet") (r "^0.3.1") (d #t) (k 0)) (d (n "ripemd") (r "^0.1.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "subtle-encoding") (r "^0.5.1") (f (quote ("bech32-preview"))) (d #t) (k 0)) (d (n "tiny-bip39") (r "^0.8.2") (d #t) (k 0)))) (h "1bi0hfq5b2nq5ikndkcd2bpcwffxzsir0y9c7840yxqndzif7sq0")))

(define-public crate-cyber-1.0.1 (c (n "cyber") (v "1.0.1") (d (list (d (n "bech32") (r "^0.9.0") (d #t) (k 0)) (d (n "hdwallet") (r "^0.3.1") (d #t) (k 0)) (d (n "ripemd") (r "^0.1.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)) (d (n "subtle-encoding") (r "^0.5.1") (f (quote ("bech32-preview"))) (d #t) (k 0)) (d (n "tiny-bip39") (r "^0.8.2") (d #t) (k 0)))) (h "1m5dl0hb2sc8vfhkyp3zw18l426ck5fk1kwgsp9555cn8caq74q8")))

(define-public crate-cyber-1.0.2 (c (n "cyber") (v "1.0.2") (d (list (d (n "bech32") (r "^0.9") (d #t) (k 0)) (d (n "hdwallet") (r "^0.4") (d #t) (k 0)) (d (n "ripemd") (r "^0.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "subtle-encoding") (r "^0.5.1") (f (quote ("bech32-preview"))) (d #t) (k 0)) (d (n "tiny-bip39") (r "^1.0") (d #t) (k 0)))) (h "0sy03wr3bifag7761vgnrj058585hvdcmsbnd9qfm0q1mhcplw1q")))

