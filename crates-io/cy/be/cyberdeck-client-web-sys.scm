(define-module (crates-io cy be cyberdeck-client-web-sys) #:use-module (crates-io))

(define-public crate-cyberdeck-client-web-sys-0.1.0 (c (n "cyberdeck-client-web-sys") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.61") (d #t) (k 0)) (d (n "serde-wasm-bindgen") (r "^0.5.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.34") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.61") (f (quote ("RtcPeerConnection" "RtcSessionDescription" "RtcDataChannel" "Document" "Window" "Element" "RtcConfiguration" "Request" "RequestInit" "RequestMode" "Response" "RtcSessionDescriptionInit"))) (d #t) (k 0)))) (h "17b2lmj9blmidad3w5hwjwn2zn616kc6lbda8v7kzqmzhkq9hh7d")))

