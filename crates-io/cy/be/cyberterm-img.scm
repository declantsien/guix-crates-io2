(define-module (crates-io cy be cyberterm-img) #:use-module (crates-io))

(define-public crate-cyberterm-img-0.1.0 (c (n "cyberterm-img") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (d #t) (k 0)))) (h "0y1hxpcl86c69z56mamni7avanapgi6a42dmk24742wkfgdqyhqy")))

(define-public crate-cyberterm-img-0.1.1 (c (n "cyberterm-img") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (d #t) (k 0)) (d (n "mime") (r "^0.3.17") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0.4") (d #t) (k 0)))) (h "0k2711vwj0nzyhvr7wbbipa66x8nj7c1awjj1vl2km3b6ynd1gg4")))

