(define-module (crates-io cy be cyberrt-rs) #:use-module (crates-io))

(define-public crate-cyberrt-rs-0.1.0 (c (n "cyberrt-rs") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0.66") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.66") (d #t) (k 1)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.9.0") (d #t) (k 1)) (d (n "prost-types") (r "^0.9.0") (d #t) (k 0)))) (h "0049s45772c5rlnwykkbivrj0ld6ay2ri60di0mzrl5wha92iwm8")))

