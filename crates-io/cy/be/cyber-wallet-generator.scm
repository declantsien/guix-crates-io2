(define-module (crates-io cy be cyber-wallet-generator) #:use-module (crates-io))

(define-public crate-cyber-wallet-generator-0.1.0 (c (n "cyber-wallet-generator") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "cyber") (r "^0.1.3") (d #t) (k 0)))) (h "1ch12d34hhkylfl7m7z6gfc9055l30ykd6m9nyalcigylh2ybjjw")))

(define-public crate-cyber-wallet-generator-0.1.1 (c (n "cyber-wallet-generator") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "cyber") (r "^0.1.3") (d #t) (k 0)))) (h "0mbafa4zfz51jsjpwdpx1cbnhqls5bvifhyimr7c3bi4magmkd74")))

(define-public crate-cyber-wallet-generator-1.0.0 (c (n "cyber-wallet-generator") (v "1.0.0") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "cyber") (r "^1.0.1") (d #t) (k 0)))) (h "08p8xbv6yzqa9dx9hysxdd04zfwq8siw6jhp411zds4j08d6r22h")))

