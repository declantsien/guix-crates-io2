(define-module (crates-io cy be cybergarage) #:use-module (crates-io))

(define-public crate-cybergarage-1.0.0 (c (n "cybergarage") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ryz5s901p8n8smjl4fi4hix2j8q5lbm47bgq7j7f5fwr0v9blbc")))

(define-public crate-cybergarage-1.0.1 (c (n "cybergarage") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "19cqfpdl6d19cy49skgcv7lj6kydm7ffm78bjkrgzw6mx0cmdm16")))

(define-public crate-cybergarage-1.0.2 (c (n "cybergarage") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "06sw8jj2f064hhj27b0x09zw7z82n2i3qdkys37g0lz4ng929gpm")))

(define-public crate-cybergarage-1.1.0 (c (n "cybergarage") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)) (d (n "pnet") (r "^0.28.0") (d #t) (k 0)))) (h "1ai70611wkqwhj1wbj6jcy0ikhmvx316bbwpyaamv0p97dgw57gv")))

(define-public crate-cybergarage-1.1.1 (c (n "cybergarage") (v "1.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)) (d (n "pnet") (r "^0.28.0") (d #t) (k 0)))) (h "0i7amvcqcacrmz2hk0qsknsm7bshcw412593pavdn6gf02xss7kn")))

(define-public crate-cybergarage-1.1.2 (c (n "cybergarage") (v "1.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)) (d (n "pnet") (r "^0.28.0") (d #t) (k 0)))) (h "14xsld3ay18l0v7f32p6kzsr2fnj42nr9ab2j4a5sjv77fgyljda")))

(define-public crate-cybergarage-1.1.3 (c (n "cybergarage") (v "1.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)) (d (n "pnet") (r "^0.28.0") (d #t) (k 0)))) (h "0mk4452xzif0x6cgk6bvyj1v9a31022wbyaaanfc3ka5h6dqs4v8")))

(define-public crate-cybergarage-1.1.4 (c (n "cybergarage") (v "1.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)) (d (n "pnet") (r "^0.28.0") (d #t) (k 0)))) (h "0ibmck66rdbwli1lb601j0my2vzygw45m9vv2js74zpwg294w68a")))

(define-public crate-cybergarage-1.1.5 (c (n "cybergarage") (v "1.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)) (d (n "pnet") (r "^0.28.0") (d #t) (k 0)))) (h "0n4sccxlj93vj5vmr3md668znrn1a538129dj4ix1kjm1fkwk4v2")))

(define-public crate-cybergarage-1.1.6 (c (n "cybergarage") (v "1.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)) (d (n "pnet") (r "^0.28.0") (d #t) (k 0)))) (h "020bzwngs2fx5c31yfpcgwdr32ivhsx6xhm6ivxj1m9631dhza1m")))

