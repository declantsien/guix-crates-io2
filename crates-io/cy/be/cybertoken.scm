(define-module (crates-io cy be cybertoken) #:use-module (crates-io))

(define-public crate-cybertoken-1.0.0 (c (n "cybertoken") (v "1.0.0") (d (list (d (n "base-x") (r "^0.2.11") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1c2pjz5jx4j96bs94461qnnb77niawxfx99kq3vpw21zwvsc5pn0")))

(define-public crate-cybertoken-1.0.1 (c (n "cybertoken") (v "1.0.1") (d (list (d (n "base-x") (r "^0.2.11") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0r2zrnlmdpv3nzdqcayg7wqv6gplki4bvxn69xzavzxfv7nf8vyx")))

