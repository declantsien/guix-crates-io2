(define-module (crates-io cy ph cyphal-udp) #:use-module (crates-io))

(define-public crate-cyphal-udp-0.0.0 (c (n "cyphal-udp") (v "0.0.0") (h "1cg9yf7045cg9i7rj072j8i7hafj30wc1f5ywwsqhaldibskzllc")))

(define-public crate-cyphal-udp-0.0.8 (c (n "cyphal-udp") (v "0.0.8") (d (list (d (n "cyphal") (r "^0.0.8") (d #t) (k 0)))) (h "0clcws9j5dc73w24mbkxjlzvsghx3w7nya6q8ax63fni76z0r95r")))

(define-public crate-cyphal-udp-0.0.9 (c (n "cyphal-udp") (v "0.0.9") (d (list (d (n "cyphal") (r "^0.0.9") (d #t) (k 0)))) (h "1yn5iv1s93vg1h4zpirgn3sbqw0q3xsvqh0033v4dlxny8vz1jjj")))

