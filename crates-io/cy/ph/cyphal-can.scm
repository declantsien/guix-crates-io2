(define-module (crates-io cy ph cyphal-can) #:use-module (crates-io))

(define-public crate-cyphal-can-0.0.0 (c (n "cyphal-can") (v "0.0.0") (h "15hns90n5zm6if1sw7mjij270kkbpdn5j49fw2k3c31drzrlihid")))

(define-public crate-cyphal-can-0.0.6 (c (n "cyphal-can") (v "0.0.6") (d (list (d (n "crc") (r "^3.2.1") (d #t) (k 0)) (d (n "cyphal") (r "^0.0.6") (d #t) (k 0)))) (h "0a391cfy7ki6p05ngiialzdvakfk3w67xjq1ivkvzyw493a5nkif")))

(define-public crate-cyphal-can-0.0.7 (c (n "cyphal-can") (v "0.0.7") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "crc") (r "^3.2.1") (d #t) (k 0)) (d (n "cyphal") (r "^0.0.7") (d #t) (k 0)))) (h "1a8973fzrx8hkm0r6i1v4qkvq6rrxpbyn3bb6x77npgjf735ghkj")))

(define-public crate-cyphal-can-0.0.8 (c (n "cyphal-can") (v "0.0.8") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "crc") (r "^3.2.1") (d #t) (k 0)) (d (n "cyphal") (r "^0.0.8") (d #t) (k 0)))) (h "0pglgfky2kbpsqzma894mwnd31m46i5j0nlykg6d4ayckmxpcxzz")))

(define-public crate-cyphal-can-0.0.9 (c (n "cyphal-can") (v "0.0.9") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "crc") (r "^3.2.1") (d #t) (k 0)) (d (n "cyphal") (r "^0.0.9") (d #t) (k 0)))) (h "05ngnbl8vp68i134gwxz6p16cmngqasjpk0xvj21rjsy2f0cv07l")))

