(define-module (crates-io cy ph cyphal-socketcan) #:use-module (crates-io))

(define-public crate-cyphal-socketcan-0.0.0 (c (n "cyphal-socketcan") (v "0.0.0") (h "0326vh4qwcfn1vgcfrpwz2v9962np65h5lz03phcp9ccnn17i74f")))

(define-public crate-cyphal-socketcan-0.0.6 (c (n "cyphal-socketcan") (v "0.0.6") (d (list (d (n "cyphal") (r "^0.0.6") (d #t) (k 0)) (d (n "cyphal-can") (r "^0.0.6") (d #t) (k 0)) (d (n "embedded-can") (r "^0.4.1") (d #t) (k 0)) (d (n "socketcan") (r "^3.3.0") (d #t) (k 0)))) (h "1r7dbybzwz2sq0ahymdy08rnv8km7pkggwzvq623xn09hq28v73q")))

(define-public crate-cyphal-socketcan-0.0.7 (c (n "cyphal-socketcan") (v "0.0.7") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "cyphal") (r "^0.0.7") (d #t) (k 0)) (d (n "cyphal-can") (r "^0.0.7") (d #t) (k 0)) (d (n "embedded-can") (r "^0.4.1") (d #t) (k 0)) (d (n "socketcan") (r "^3.3.0") (f (quote ("async-std"))) (d #t) (k 0)))) (h "1pj9g60m0l4009ciggldwx93lz5v899yl2242mgni4mdfzcwlsxc")))

(define-public crate-cyphal-socketcan-0.0.8 (c (n "cyphal-socketcan") (v "0.0.8") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "cyphal") (r "^0.0.8") (d #t) (k 0)) (d (n "cyphal-can") (r "^0.0.8") (d #t) (k 0)) (d (n "embedded-can") (r "^0.4.1") (d #t) (k 0)) (d (n "socketcan") (r "^3.3.0") (f (quote ("async-std"))) (d #t) (k 0)))) (h "0b41z633qdcgl4hsp3r0xfvah1v0p0yq3p8zmc7wwymv6cnrkbm1")))

(define-public crate-cyphal-socketcan-0.0.9 (c (n "cyphal-socketcan") (v "0.0.9") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "cyphal") (r "^0.0.9") (d #t) (k 0)) (d (n "cyphal-can") (r "^0.0.9") (d #t) (k 0)) (d (n "embedded-can") (r "^0.4.1") (d #t) (k 0)) (d (n "socketcan") (r "^3.3.0") (f (quote ("async-std"))) (d #t) (k 0)))) (h "06m0m6vjbnrh29xafgjpivibq65j9nv7ac1xv0lsj4nadrriiydg")))

