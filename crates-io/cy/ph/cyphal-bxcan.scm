(define-module (crates-io cy ph cyphal-bxcan) #:use-module (crates-io))

(define-public crate-cyphal-bxcan-0.0.0 (c (n "cyphal-bxcan") (v "0.0.0") (h "1h5gza079ashnx7ahvr8bfw1hcmk31zgqb0gvx5w54lwrajzb867") (y #t)))

(define-public crate-cyphal-bxcan-0.0.6 (c (n "cyphal-bxcan") (v "0.0.6") (d (list (d (n "bxcan") (r "^0.7.0") (d #t) (k 0)) (d (n "cyphal") (r "^0.0.6") (d #t) (k 0)) (d (n "cyphal-can") (r "^0.0.6") (d #t) (k 0)))) (h "04zxvpr049xbdc5m8cz2afqq33zbqc1g6kzvnxq7f4rqhnmm71iy") (y #t)))

(define-public crate-cyphal-bxcan-0.0.7 (c (n "cyphal-bxcan") (v "0.0.7") (d (list (d (n "bxcan") (r "^0.7.0") (d #t) (k 0)) (d (n "cyphal") (r "^0.0.7") (d #t) (k 0)) (d (n "cyphal-can") (r "^0.0.7") (d #t) (k 0)))) (h "0r1sny9d435p7icq2xv7a7bwamk23473d1lzswifd0k3cr4igz6s") (y #t)))

(define-public crate-cyphal-bxcan-0.0.8 (c (n "cyphal-bxcan") (v "0.0.8") (d (list (d (n "bxcan") (r "^0.7.0") (d #t) (k 0)) (d (n "cyphal") (r "^0.0.8") (d #t) (k 0)) (d (n "cyphal-can") (r "^0.0.8") (d #t) (k 0)))) (h "1lfxjwqjacjrh2pp90xfp2nbfrbcic2kqsij4n64lsx6yknr1dnx") (y #t)))

(define-public crate-cyphal-bxcan-0.0.9 (c (n "cyphal-bxcan") (v "0.0.9") (d (list (d (n "bxcan") (r "^0.7.0") (d #t) (k 0)) (d (n "cyphal") (r "^0.0.9") (d #t) (k 0)) (d (n "cyphal-can") (r "^0.0.9") (d #t) (k 0)))) (h "1bgdx06awpvv9p6x20l2a0xq07s83pg2gkm6fwycrgglsmyl8z48") (y #t)))

