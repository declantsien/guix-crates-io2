(define-module (crates-io cy ph cyphal-fdcan) #:use-module (crates-io))

(define-public crate-cyphal-fdcan-0.0.0 (c (n "cyphal-fdcan") (v "0.0.0") (h "1lc2npbbv9s81fhbiwph7d2srfxswxhsymdf5rija2bn1lk4m44a") (y #t)))

(define-public crate-cyphal-fdcan-0.0.7 (c (n "cyphal-fdcan") (v "0.0.7") (d (list (d (n "cyphal") (r "^0.0.7") (d #t) (k 0)) (d (n "cyphal-can") (r "^0.0.7") (d #t) (k 0)) (d (n "fdcan") (r "^0.2.0") (d #t) (k 0)))) (h "0klll0bb1gpaxg1ffszc2anbcyxfyy01517div5z9j88wknj687d") (f (quote (("fdcan_h7" "fdcan/fdcan_h7") ("fdcan_g0_g4_l5" "fdcan/fdcan_g0_g4_l5") ("default" "fdcan_g0_g4_l5")))) (y #t)))

(define-public crate-cyphal-fdcan-0.0.8 (c (n "cyphal-fdcan") (v "0.0.8") (d (list (d (n "cyphal") (r "^0.0.8") (d #t) (k 0)) (d (n "cyphal-can") (r "^0.0.8") (d #t) (k 0)) (d (n "fdcan") (r "^0.2.0") (d #t) (k 0)))) (h "1gpvh9hfxhnvj451g92cgls7zmn5xvh7pwdrph1dw82b6i7bkc48") (f (quote (("fdcan_h7" "fdcan/fdcan_h7") ("fdcan_g0_g4_l5" "fdcan/fdcan_g0_g4_l5") ("default" "fdcan_g0_g4_l5")))) (y #t)))

(define-public crate-cyphal-fdcan-0.0.9 (c (n "cyphal-fdcan") (v "0.0.9") (d (list (d (n "cyphal") (r "^0.0.9") (d #t) (k 0)) (d (n "cyphal-can") (r "^0.0.9") (d #t) (k 0)) (d (n "fdcan") (r "^0.2.0") (d #t) (k 0)))) (h "0a1xfp58x4llpvs4r9yvs4wkckw5xb36g53yxbmzrndmwkq51s1w") (f (quote (("fdcan_h7" "fdcan/fdcan_h7") ("fdcan_g0_g4_l5" "fdcan/fdcan_g0_g4_l5") ("default" "fdcan_g0_g4_l5")))) (y #t)))

