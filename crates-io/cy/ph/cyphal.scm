(define-module (crates-io cy ph cyphal) #:use-module (crates-io))

(define-public crate-cyphal-0.0.1 (c (n "cyphal") (v "0.0.1") (h "1z2y31y6gk8r5ynpc6wpw044kx0irin6aciwk8acnjcwsxm18lzd")))

(define-public crate-cyphal-0.0.2 (c (n "cyphal") (v "0.0.2") (h "0mrwgn1phhf7yw9dpwviqaspihd9cy4ia7r6jg95lw4c76dd284g")))

(define-public crate-cyphal-0.0.3 (c (n "cyphal") (v "0.0.3") (d (list (d (n "embedded-can") (r "^0.4.1") (o #t) (k 0)) (d (n "socketcan") (r "^3.3.0") (o #t) (d #t) (k 0)))) (h "1vz110w8fw7avd55c3k28lyh2ddk46n8cq32svb2nv9w49s8hwjr") (f (quote (("crc")))) (s 2) (e (quote (("socketcan" "can" "dep:socketcan") ("can" "dep:embedded-can"))))))

(define-public crate-cyphal-0.0.4 (c (n "cyphal") (v "0.0.4") (d (list (d (n "embedded-can") (r "^0.4.1") (o #t) (k 0)) (d (n "socketcan") (r "^3.3.0") (o #t) (d #t) (k 0)))) (h "14g7j1psn76d765d8i57a4pxgjcwqnz2yly0rck5a401cckprn86") (f (quote (("crc")))) (s 2) (e (quote (("socketcan" "can" "dep:socketcan") ("can" "dep:embedded-can"))))))

(define-public crate-cyphal-0.0.5 (c (n "cyphal") (v "0.0.5") (d (list (d (n "crc") (r "^3.2.1") (o #t) (d #t) (k 0)) (d (n "embedded-can") (r "^0.4.1") (o #t) (k 0)) (d (n "socketcan") (r "^3.3.0") (o #t) (d #t) (k 0)))) (h "0cc9ai9xkq0jibmhmzyp1rhrrnm8cddd79wjx8c25skgfpknc8fh") (s 2) (e (quote (("socketcan" "dep:socketcan") ("canfd" "dep:embedded-can" "dep:crc") ("can" "dep:embedded-can" "dep:crc"))))))

(define-public crate-cyphal-0.0.6 (c (n "cyphal") (v "0.0.6") (h "1801zwi60qnm0f9ajl6bkpfxy4kfn94ph71cmbvp2am01n024krv")))

(define-public crate-cyphal-0.0.7 (c (n "cyphal") (v "0.0.7") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)))) (h "0jzbx67yy72zvfix1z5kkyir6vjw00pkiv911kbq6rx6hg2bg769")))

(define-public crate-cyphal-0.0.8 (c (n "cyphal") (v "0.0.8") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)))) (h "0zpbknj0b0vcqq7v5hr73l64334kav43nvd90p9aasvisylnzd1l")))

(define-public crate-cyphal-0.0.9 (c (n "cyphal") (v "0.0.9") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)))) (h "0811x277yg9pvsc97ycwp2wka1pv2z52qyjrmyrqgfb1axgww0s4")))

