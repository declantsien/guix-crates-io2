(define-module (crates-io cy ph cypher-dto-macros) #:use-module (crates-io))

(define-public crate-cypher-dto-macros-0.1.0 (c (n "cypher-dto-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0hpsfy611l3a2ij7ki41xb64ddlqdh423brac0i9brw4my5dh9ds") (f (quote (("serde" "serde/derive") ("default"))))))

(define-public crate-cypher-dto-macros-0.1.1 (c (n "cypher-dto-macros") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1r111rbi2cgivk54gbs2vsyq7zja4d1z3zvmzv22l4khx1f3ydlh") (f (quote (("serde" "serde/derive") ("default"))))))

(define-public crate-cypher-dto-macros-0.2.0 (c (n "cypher-dto-macros") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0ib4f22n6w4n478ska41hhsq31rjfnaq9d60f1nmb40dzmwwj5n8") (f (quote (("serde" "serde/derive") ("default"))))))

(define-public crate-cypher-dto-macros-0.3.0 (c (n "cypher-dto-macros") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1bph04aycddvd9rkvfiwi40v9amlm31lflwn8wjdc8hiw038mbz9") (f (quote (("serde" "serde/derive") ("default"))))))

(define-public crate-cypher-dto-macros-0.3.1 (c (n "cypher-dto-macros") (v "0.3.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "10wl9v2q0mhv2cdl1rnsdfjzgb9ayc1460606aka3cis4b3fci8s") (f (quote (("serde" "serde/derive") ("default"))))))

