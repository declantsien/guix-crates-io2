(define-module (crates-io cy ph cyphal-udpsocket) #:use-module (crates-io))

(define-public crate-cyphal-udpsocket-0.0.0 (c (n "cyphal-udpsocket") (v "0.0.0") (h "0l3aw33ygilj3wfwygcqjla90mzwivm5rpq5rlcm2fhqi753yhva")))

(define-public crate-cyphal-udpsocket-0.0.8 (c (n "cyphal-udpsocket") (v "0.0.8") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "cyphal") (r "^0.0.8") (d #t) (k 0)) (d (n "cyphal-udp") (r "^0.0.8") (d #t) (k 0)))) (h "009pdzrh0gb7bc5w1rd8di1fn16sxrv92n6j7wvsfr11g166y5cm")))

(define-public crate-cyphal-udpsocket-0.0.9 (c (n "cyphal-udpsocket") (v "0.0.9") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "cyphal") (r "^0.0.9") (d #t) (k 0)) (d (n "cyphal-udp") (r "^0.0.9") (d #t) (k 0)))) (h "0wqbmq6sq84qsa1zzva5zzb7vys0bqxw07di345gqgmq2j0kndm4")))

