(define-module (crates-io cy ph cypher_derive) #:use-module (crates-io))

(define-public crate-cypher_derive-0.1.1 (c (n "cypher_derive") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.7.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0ixy1rdgnm2rkf7i4gh6f9lsa4mv6fi3s9c133m4jy00yygajbv8") (y #t)))

(define-public crate-cypher_derive-0.1.2 (c (n "cypher_derive") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.7.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0is9rwwxgbp4bb2naznc52frmbsniz331cqx5hlrh0bp41pcbqhs") (y #t)))

(define-public crate-cypher_derive-0.1.3 (c (n "cypher_derive") (v "0.1.3") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.7.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0m9y8wa4ajgdhvcysjy6a3z9w8v4fvp4lwdrdcxx2p2bfkfk90s8") (y #t)))

(define-public crate-cypher_derive-0.1.4 (c (n "cypher_derive") (v "0.1.4") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.7.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0d5icanqh4zyd9czzsl330mps1xwf3nyikmqxn8c2mssi5405hkl") (y #t)))

(define-public crate-cypher_derive-0.1.5 (c (n "cypher_derive") (v "0.1.5") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.7.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0dnkzjqcnsi0wf0mjijqsfj4gp8a887kgvjgiqdx9h3wiky4mqd5")))

(define-public crate-cypher_derive-0.1.6 (c (n "cypher_derive") (v "0.1.6") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.7.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0f62p8579iajy7mdf2gip5qz6j7y0dhm6sni208lwa0pmvx6nkqz")))

