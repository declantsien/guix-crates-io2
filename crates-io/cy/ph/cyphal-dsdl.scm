(define-module (crates-io cy ph cyphal-dsdl) #:use-module (crates-io))

(define-public crate-cyphal-dsdl-0.0.0 (c (n "cyphal-dsdl") (v "0.0.0") (h "15w9x9a50savzyx69v5cv6xd8a8yg0c8jpn5jb5fasv9y0wx0b2n")))

(define-public crate-cyphal-dsdl-0.0.8 (c (n "cyphal-dsdl") (v "0.0.8") (d (list (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "1aff6kyllv26l67fw6pvsmrswz9ig9ngzy168kpmx0ph8ybz9n9i")))

(define-public crate-cyphal-dsdl-0.0.9 (c (n "cyphal-dsdl") (v "0.0.9") (d (list (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "0h0cs8psp00pxrrwv920z1q09qyb78f9hb9x1qg5vxzbagvlvkaw")))

