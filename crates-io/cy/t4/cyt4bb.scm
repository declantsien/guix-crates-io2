(define-module (crates-io cy t4 cyt4bb) #:use-module (crates-io))

(define-public crate-cyt4bb-0.0.1 (c (n "cyt4bb") (v "0.0.1") (d (list (d (n "cyt4bb_a") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "cyt4bb_b") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "0mfyi9yagxdipxpcap1455xinns2kxkvyr9yxgqgp84vhxl9h6q0") (f (quote (("rev_b" "cyt4bb_b") ("rev_a" "cyt4bb_a") ("default" "rev_b")))) (s 2) (e (quote (("rt" "cyt4bb_a?/rt" "cyt4bb_b?/rt") ("critical-section" "cyt4bb_a?/critical-section" "cyt4bb_b?/critical-section")))) (r "1.64")))

