(define-module (crates-io cy t4 cyt4dn_a) #:use-module (crates-io))

(define-public crate-cyt4dn_a-0.0.1 (c (n "cyt4dn_a") (v "0.0.1") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0rmx0x1y9p9d5svzn23p37mxjc730z0d79d2z1qj3afwlfsywciq") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.64")))

