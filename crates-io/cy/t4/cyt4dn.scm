(define-module (crates-io cy t4 cyt4dn) #:use-module (crates-io))

(define-public crate-cyt4dn-0.0.1 (c (n "cyt4dn") (v "0.0.1") (d (list (d (n "cyt4dn_a") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "cyt4dn_b") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "cyt4dn_c") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "0a1kgvw876yhkpzphv32phlyj16kb6rsv12yhfd9p8zr1lyrjqw4") (f (quote (("rev_c" "cyt4dn_c") ("rev_b" "cyt4dn_b") ("rev_a" "cyt4dn_a") ("default" "rev_c")))) (s 2) (e (quote (("rt" "cyt4dn_a?/rt" "cyt4dn_b?/rt" "cyt4dn_c?/rt") ("critical-section" "cyt4dn_a?/critical-section" "cyt4dn_b?/critical-section" "cyt4dn_c?/critical-section")))) (r "1.64")))

