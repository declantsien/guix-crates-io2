(define-module (crates-io cy t4 cyt4bb_a) #:use-module (crates-io))

(define-public crate-cyt4bb_a-0.0.1 (c (n "cyt4bb_a") (v "0.0.1") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1f2zm5m35vk2zjgvj9iixds5xl2pp1jyliq7487my4z4q4h8p7rq") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.64")))

