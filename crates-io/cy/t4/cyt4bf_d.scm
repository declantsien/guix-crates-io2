(define-module (crates-io cy t4 cyt4bf_d) #:use-module (crates-io))

(define-public crate-cyt4bf_d-0.0.1 (c (n "cyt4bf_d") (v "0.0.1") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "098g5igxsf3fz60pzk34sbzvdv67fp06wh4mrqd9m0p5zdwxcjx0") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.64")))

