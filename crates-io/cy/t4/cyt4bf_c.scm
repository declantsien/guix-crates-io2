(define-module (crates-io cy t4 cyt4bf_c) #:use-module (crates-io))

(define-public crate-cyt4bf_c-0.0.1 (c (n "cyt4bf_c") (v "0.0.1") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "19fyrp9m99ca2dan4kyp40d9iixa5sn17hafnvymvcfz61426885") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.64")))

