(define-module (crates-io cy t4 cyt4bf) #:use-module (crates-io))

(define-public crate-cyt4bf-0.0.1 (c (n "cyt4bf") (v "0.0.1") (d (list (d (n "cyt4bf_c") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "cyt4bf_d") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "1bp55zq5irvmxsanmhi0n5iacg391rkhdck7w7ajbn8fs26551xz") (f (quote (("rev_d" "cyt4bf_d") ("rev_c" "cyt4bf_c") ("default" "rev_d")))) (s 2) (e (quote (("rt" "cyt4bf_c?/rt" "cyt4bf_d?/rt") ("critical-section" "cyt4bf_c?/critical-section" "cyt4bf_d?/critical-section")))) (r "1.64")))

