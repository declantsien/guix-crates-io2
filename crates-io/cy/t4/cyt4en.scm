(define-module (crates-io cy t4 cyt4en) #:use-module (crates-io))

(define-public crate-cyt4en-0.0.1 (c (n "cyt4en") (v "0.0.1") (d (list (d (n "cyt4en_a") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "1dqw18774w27q4cf1kym7k0vmjcp1qjg9xj4skjwxbi1q0766b6s") (f (quote (("rev_a" "cyt4en_a") ("default" "rev_a")))) (s 2) (e (quote (("rt" "cyt4en_a?/rt") ("critical-section" "cyt4en_a?/critical-section")))) (r "1.64")))

