(define-module (crates-io cy w4 cyw43) #:use-module (crates-io))

(define-public crate-cyw43-0.0.0 (c (n "cyw43") (v "0.0.0") (h "1bcf6x4wxphv91qnjaf72vxnm06s82xwyfvbj6rgy9fj6ysgmnwa")))

(define-public crate-cyw43-0.1.0 (c (n "cyw43") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.0") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embassy-futures") (r "^0.1.0") (d #t) (k 0)) (d (n "embassy-net-driver-channel") (r "^0.2.0") (d #t) (k 0)) (d (n "embassy-sync") (r "^0.5.0") (d #t) (k 0)) (d (n "embassy-time") (r "^0.3.0") (d #t) (k 0)) (d (n "embedded-hal-1") (r "^1.0") (d #t) (k 0) (p "embedded-hal")) (d (n "futures") (r "^0.3.17") (f (quote ("async-await" "cfg-target-has-atomic" "unstable"))) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (k 0)))) (h "0phl4bbb5y7hlnbzx4v1jvxx13dlhn9i0sfrkc4bpzjqhxwyrmjr") (f (quote (("firmware-logs")))) (s 2) (e (quote (("log" "dep:log") ("defmt" "dep:defmt"))))))

