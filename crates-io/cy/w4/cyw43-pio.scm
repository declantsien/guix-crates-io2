(define-module (crates-io cy w4 cyw43-pio) #:use-module (crates-io))

(define-public crate-cyw43-pio-0.0.0 (c (n "cyw43-pio") (v "0.0.0") (h "0cvf0vnxplc522yfl165sxmz2j6hirm86qqv33bb6dapny9mf2wy")))

(define-public crate-cyw43-pio-0.1.0 (c (n "cyw43-pio") (v "0.1.0") (d (list (d (n "cyw43") (r "^0.1.0") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embassy-rp") (r "^0.1.0") (d #t) (k 0)) (d (n "fixed") (r "^1.23.1") (d #t) (k 0)) (d (n "pio") (r "^0.2.1") (d #t) (k 0)) (d (n "pio-proc") (r "^0.2") (d #t) (k 0)))) (h "03289qk4jdpn374bndb624r963x910fzpprzfjlf03j2x5pl8agl") (f (quote (("overclock"))))))

