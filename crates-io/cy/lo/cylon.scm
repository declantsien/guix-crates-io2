(define-module (crates-io cy lo cylon) #:use-module (crates-io))

(define-public crate-cylon-0.1.0 (c (n "cylon") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("async_futures"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0sxf0zfrb960lckbnngc6aj365zx69h67k6dk4vd7hqispcv8d8g")))

(define-public crate-cylon-0.1.1 (c (n "cylon") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("async_futures"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "097v7ghvb1g62wxa02ddl9znr3f7lijvpcmvbzg9bk1p23wl6a3m")))

(define-public crate-cylon-0.1.2 (c (n "cylon") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (f (quote ("async_futures"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1108dj0gyj00fc42l9dj54c3nzw0g19wqygqfwrn54p1i6i1bc9z") (f (quote (("crawl-delay"))))))

(define-public crate-cylon-0.1.3 (c (n "cylon") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (f (quote ("async_futures"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "08qqczjmbj07f05szykc9c7vx4yyxjiwvagwn3mp5gyd0jlw5dwr") (f (quote (("crawl-delay"))))))

(define-public crate-cylon-0.2.0 (c (n "cylon") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("async_futures"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1lvzdsp85ymhj28sh9688cwbyf7gwdnkkdm1vs4qaw2bcad8jkld") (f (quote (("crawl-delay"))))))

(define-public crate-cylon-0.3.0 (c (n "cylon") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (f (quote ("async_futures"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "00vnn0j1sn5qpnjycjk4kkypjz0nqbf404wndy04r4f5y2sv8751") (f (quote (("crawl-delay"))))))

(define-public crate-cylon-0.3.1 (c (n "cylon") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("async_futures"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0bbkjr8faxlkig5aidpvyknh44za5id44ww64z31fap9lnmh3k7a") (f (quote (("crawl-delay"))))))

(define-public crate-cylon-0.3.2 (c (n "cylon") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3") (f (quote ("async_futures"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0px6px6kc4gigq0kpyimq43d34xqg2kmzg2xd48cz403qm4yivh8") (f (quote (("crawl-delay"))))))

(define-public crate-cylon-0.3.3 (c (n "cylon") (v "0.3.3") (d (list (d (n "criterion") (r "^0.3") (f (quote ("async_futures"))) (d #t) (k 2)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "13gnwwwxy2xvg19rj4zjr4shim6z4s8r4xbyvk1gx1szhrm6n9qj") (f (quote (("crawl-delay"))))))

