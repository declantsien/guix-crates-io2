(define-module (crates-io cy lo cylon_ast) #:use-module (crates-io))

(define-public crate-cylon_ast-0.1.0 (c (n "cylon_ast") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0sajwqbcbmdg4gql6qs1xhprzjr0kz7v68fxbicccfmdlwa3638f")))

(define-public crate-cylon_ast-0.2.0 (c (n "cylon_ast") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "1w9cpimq10zadhi9bbjvk0q421l8g0l9ygd069l6830dgwnpbz6p")))

(define-public crate-cylon_ast-0.3.0 (c (n "cylon_ast") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0wjcw86gsa2y087h6xifd8m19fzax4h2nnczysr8a64wds1v2d3d") (y #t)))

(define-public crate-cylon_ast-0.3.1 (c (n "cylon_ast") (v "0.3.1") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0wkl66pl1myhd6f86h4c55i11v5q68mqfsyfcyhvplrcqbmbqqcf")))

(define-public crate-cylon_ast-0.3.2 (c (n "cylon_ast") (v "0.3.2") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0iig31kcamjghxf16x95whrb9m8k6hglvvgc5f9ly91ki7mm4rr4")))

(define-public crate-cylon_ast-0.4.0 (c (n "cylon_ast") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0a2vbx9lidxpcvjrqrsmbinqlhmj1naajfhzagcvgpq2i7l2q0nr")))

