(define-module (crates-io cy gv cygv) #:use-module (crates-io))

(define-public crate-cygv-0.0.1 (c (n "cygv") (v "0.0.1") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.1") (f (quote ("abi3-py38"))) (d #t) (k 0)) (d (n "rug") (r "^1.24.0") (d #t) (k 0)))) (h "055dnw3r31y1nj50vgggcy6v6rnd2r21gnxs7smx34wghfl6zbz3")))

(define-public crate-cygv-0.0.2 (c (n "cygv") (v "0.0.2") (d (list (d (n "itertools") (r "^0.13.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.21.1") (f (quote ("abi3-py38"))) (d #t) (k 0)) (d (n "rug") (r "^1.24.0") (d #t) (k 0)))) (h "028a5srg9bap27y3mayjwjadg5mfzr52jnpxbn0i1hyvc2vqlcdy")))

