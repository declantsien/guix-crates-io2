(define-module (crates-io cy t2 cyt2b9) #:use-module (crates-io))

(define-public crate-cyt2b9-0.0.1 (c (n "cyt2b9") (v "0.0.1") (d (list (d (n "cyt2b9_c") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "16dqbl7hl0n986ppvplcl3655pgixsdyxsk32dlm778iz0y0lrip") (f (quote (("rev_c" "cyt2b9_c") ("default" "rev_c")))) (s 2) (e (quote (("rt" "cyt2b9_c?/rt") ("critical-section" "cyt2b9_c?/critical-section")))) (r "1.64")))

