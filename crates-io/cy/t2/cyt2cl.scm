(define-module (crates-io cy t2 cyt2cl) #:use-module (crates-io))

(define-public crate-cyt2cl-0.0.1 (c (n "cyt2cl") (v "0.0.1") (d (list (d (n "cyt2cl_a") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "0hvlxc0mmkhc6djsizk91892gcsl0aidx6yv61yjvqyba68rk1p4") (f (quote (("rev_a" "cyt2cl_a") ("default" "rev_a")))) (s 2) (e (quote (("rt" "cyt2cl_a?/rt") ("critical-section" "cyt2cl_a?/critical-section")))) (r "1.64")))

