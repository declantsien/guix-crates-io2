(define-module (crates-io cy t2 cyt2b6) #:use-module (crates-io))

(define-public crate-cyt2b6-0.0.1 (c (n "cyt2b6") (v "0.0.1") (d (list (d (n "cyt2b6_d") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "1b52gwfxbw7hmq4jbwcdk6l6ppbz6amcqc7l7z208y203cd5hlj0") (f (quote (("rev_d" "cyt2b6_d") ("default" "rev_d")))) (s 2) (e (quote (("rt" "cyt2b6_d?/rt") ("critical-section" "cyt2b6_d?/critical-section")))) (r "1.64")))

