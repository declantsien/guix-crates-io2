(define-module (crates-io cy t2 cyt2bl_a) #:use-module (crates-io))

(define-public crate-cyt2bl_a-0.0.1 (c (n "cyt2bl_a") (v "0.0.1") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "08hhbr6dayfpc3jq5qhgyf5klldyjrm0fyggrv2sm6n2q9f3wals") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.64")))

