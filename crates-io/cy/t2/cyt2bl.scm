(define-module (crates-io cy t2 cyt2bl) #:use-module (crates-io))

(define-public crate-cyt2bl-0.0.1 (c (n "cyt2bl") (v "0.0.1") (d (list (d (n "cyt2bl_a") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "19zsq5bc6lcz0k66k967zjh83j4s514nglp0ff1h44ar6fksn8id") (f (quote (("rev_a" "cyt2bl_a") ("default" "rev_a")))) (s 2) (e (quote (("rt" "cyt2bl_a?/rt") ("critical-section" "cyt2bl_a?/critical-section")))) (r "1.64")))

