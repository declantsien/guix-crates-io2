(define-module (crates-io cy t2 cyt2b7) #:use-module (crates-io))

(define-public crate-cyt2b7-0.0.1 (c (n "cyt2b7") (v "0.0.1") (d (list (d (n "cyt2b7_d") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "05pcxgamzkqccwp758ym2zjqv7bf944l1vmp1hv2lvi31bsndng0") (f (quote (("rev_d" "cyt2b7_d") ("default" "rev_d")))) (s 2) (e (quote (("rt" "cyt2b7_d?/rt") ("critical-section" "cyt2b7_d?/critical-section")))) (r "1.64")))

