(define-module (crates-io cy t2 cyt2b7_d) #:use-module (crates-io))

(define-public crate-cyt2b7_d-0.0.1 (c (n "cyt2b7_d") (v "0.0.1") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0rs2s05gfccpbs660xi0lzgp3vcwqh8pk7sis88h1c8qw8k78m4k") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.64")))

