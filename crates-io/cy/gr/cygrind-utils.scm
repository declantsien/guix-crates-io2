(define-module (crates-io cy gr cygrind-utils) #:use-module (crates-io))

(define-public crate-cygrind-utils-0.1.0 (c (n "cygrind-utils") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "flo_curves") (r "^0.6.0") (d #t) (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "skia-safe") (r "^0.47.0") (f (quote ("gl"))) (o #t) (d #t) (k 0)))) (h "0zzy71c3vm3xvb0fziv8cy9ii20hgzynjlc1bgdl03yjkpp8igf3") (f (quote (("full" "draw2d") ("default")))) (s 2) (e (quote (("draw2d" "dep:skia-safe"))))))

(define-public crate-cygrind-utils-0.2.0 (c (n "cygrind-utils") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "flo_curves") (r "^0.6.0") (d #t) (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "skia-safe") (r "^0.47.0") (f (quote ("gl"))) (o #t) (d #t) (k 0)))) (h "1s14r9wzhf66sn580apq0ivw6bpc831i3qscf0n0dfaw4i1zrr70") (f (quote (("full" "draw2d") ("default")))) (s 2) (e (quote (("draw2d" "dep:skia-safe"))))))

(define-public crate-cygrind-utils-0.2.1 (c (n "cygrind-utils") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "flo_curves") (r "^0.6.0") (d #t) (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "skia-safe") (r "^0.47.0") (f (quote ("gl"))) (o #t) (d #t) (k 0)))) (h "06dm8qmjsdkpnjsvrb4criqvbgcmm3j5x63xn7c89p7ygn5483ws") (f (quote (("full" "draw2d") ("default")))) (s 2) (e (quote (("draw2d" "dep:skia-safe"))))))

