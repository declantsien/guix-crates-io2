(define-module (crates-io cy bo cyborgtime) #:use-module (crates-io))

(define-public crate-cyborgtime-2.1.1 (c (n "cyborgtime") (v "2.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "time") (r "^0.3") (f (quote ("formatting"))) (d #t) (k 2)))) (h "0fwi8gcsr0ph3adlpp8v16b7p4k9147f10spx51gxrqfzd1aczw1")))

