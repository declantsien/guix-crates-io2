(define-module (crates-io cy bo cyborg) #:use-module (crates-io))

(define-public crate-cyborg-0.5.1 (c (n "cyborg") (v "0.5.1") (d (list (d (n "docopt") (r "^0.6.50") (d #t) (k 0)) (d (n "docopt_macros") (r "^0.6.50") (d #t) (k 0)) (d (n "log") (r "^0.2.5") (d #t) (k 0)) (d (n "psutil") (r "^0.5.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.3") (d #t) (k 0)) (d (n "simple_logger") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.0") (d #t) (k 2)) (d (n "time") (r "^0.1.20") (d #t) (k 0)))) (h "1n95r4bfcb4h219pvxaxwq4rwpbfnpadvn441w85ap2h0dllbzi1")))

