(define-module (crates-io cy rl cyrly) #:use-module (crates-io))

(define-public crate-cyrly-0.1.0 (c (n "cyrly") (v "0.1.0") (d (list (d (n "ryu") (r "^1.0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9.19") (d #t) (k 2)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "1njqx0kf2ki9wbr3b8yman5war3cd5zhmq1lhcv0l4vcbn7lpblk") (f (quote (("std" "serde/std") ("default" "std")))) (r "1.62")))

