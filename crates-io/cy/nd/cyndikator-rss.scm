(define-module (crates-io cy nd cyndikator-rss) #:use-module (crates-io))

(define-public crate-cyndikator-rss-0.1.0 (c (n "cyndikator-rss") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "quick-xml") (r "^0.21.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (o #t) (d #t) (k 0)))) (h "162f5hvd01rn1vgic7a8hw9gk2xw84vd2cp1kig9xqiq1f9iiywx") (f (quote (("fetch" "reqwest" "url") ("default" "fetch"))))))

