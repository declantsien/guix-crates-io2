(define-module (crates-io cy nd cyndikator-atom) #:use-module (crates-io))

(define-public crate-cyndikator-atom-0.1.0 (c (n "cyndikator-atom") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.6") (d #t) (k 0)) (d (n "quick-xml") (r "^0.21.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "00f2rm4wwmbp0q9dly02pmflrvlqzlm2k646xwa5l9jdqfiag0fm")))

