(define-module (crates-io cy ni cynic-proc-macros) #:use-module (crates-io))

(define-public crate-cynic-proc-macros-0.2.0 (c (n "cynic-proc-macros") (v "0.2.0") (d (list (d (n "cynic-codegen") (r "^0.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "0ifivxa1kx3c6v2slnd18scmnj1nsmkksidrhrnanqbkxgblg8ll")))

(define-public crate-cynic-proc-macros-0.3.0 (c (n "cynic-proc-macros") (v "0.3.0") (d (list (d (n "cynic-codegen") (r "^0.3.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "1hkr56zlynmib6q61q3acqg0zzdajcd1z7g2vb1v2484rijcaqap")))

(define-public crate-cynic-proc-macros-0.4.0 (c (n "cynic-proc-macros") (v "0.4.0") (d (list (d (n "cynic-codegen") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "0n561049ail9hy0h5a7ww85c0wmv2cqwki4v18wgay1srpws4k3n")))

(define-public crate-cynic-proc-macros-0.5.0 (c (n "cynic-proc-macros") (v "0.5.0") (d (list (d (n "cynic-codegen") (r "^0.5.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "0vq2z0i3dx38r2wrqvd78pwfy9bwfyvplav4xa1117mqfk6psfnn")))

(define-public crate-cynic-proc-macros-0.6.0 (c (n "cynic-proc-macros") (v "0.6.0") (d (list (d (n "cynic-codegen") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "0r2fmyn745m8bmx8jlnvy7h6nzja8a54x90xpdbsxpk9xwlld72w")))

(define-public crate-cynic-proc-macros-0.7.0 (c (n "cynic-proc-macros") (v "0.7.0") (d (list (d (n "cynic-codegen") (r "^0.7.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "0kza21qgjyvkhh33c4njxjvjnmm30rnbxmf4d6aa10wj1nr63ic1")))

(define-public crate-cynic-proc-macros-0.8.0 (c (n "cynic-proc-macros") (v "0.8.0") (d (list (d (n "cynic-codegen") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "1cq3gijmp6rm0jz30x2fr94c1dv7al7f2p2lz8c6gwjq913ngk9a")))

(define-public crate-cynic-proc-macros-0.9.0 (c (n "cynic-proc-macros") (v "0.9.0") (d (list (d (n "cynic-codegen") (r "^0.9.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "1s53a59rkw6h83q9nsk13wz9ypd96q1qvrddzkksp1ywnb5f2qcj")))

(define-public crate-cynic-proc-macros-0.10.0 (c (n "cynic-proc-macros") (v "0.10.0") (d (list (d (n "cynic-codegen") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "1fqhc5z65dg6l8ixc1g45q1lm3czia50w0s9xvajrrj3bab08d5z")))

(define-public crate-cynic-proc-macros-0.10.1 (c (n "cynic-proc-macros") (v "0.10.1") (d (list (d (n "cynic-codegen") (r "^0.10.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "12jqwx6j9arrv94kjrc28srs774c5hqsmkzxwlr849i9h8xfhirc")))

(define-public crate-cynic-proc-macros-0.11.0 (c (n "cynic-proc-macros") (v "0.11.0") (d (list (d (n "cynic-codegen") (r "^0.11.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "1j78x1qg0gv8gjsc467b1zdji8imkv7mcy3cg17ww3fbdfq4karw")))

(define-public crate-cynic-proc-macros-0.11.1 (c (n "cynic-proc-macros") (v "0.11.1") (d (list (d (n "cynic-codegen") (r "^0.11.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "0vr21x6490v1lx4f2b5hzdpk0hcaazh39jwjdh8ra6l469j21d1v")))

(define-public crate-cynic-proc-macros-0.12.0 (c (n "cynic-proc-macros") (v "0.12.0") (d (list (d (n "cynic-codegen") (r "^0.12.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "0mk9jh6w9a2by81fygg2jd3jvzw94lg01byf8zkdksia75s2alm3")))

(define-public crate-cynic-proc-macros-0.12.1 (c (n "cynic-proc-macros") (v "0.12.1") (d (list (d (n "cynic-codegen") (r "^0.12.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "15pq79rjjwm2ph5im1s659s2zvj42an7v7c12wqk1wfmh0bc6248")))

(define-public crate-cynic-proc-macros-0.12.2 (c (n "cynic-proc-macros") (v "0.12.2") (d (list (d (n "cynic-codegen") (r "^0.12.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "0wwgc8kqjmj003hjfhbpc47in61wv5kzcxhhgmsgiz8m4rf059cf")))

(define-public crate-cynic-proc-macros-0.12.3 (c (n "cynic-proc-macros") (v "0.12.3") (d (list (d (n "cynic-codegen") (r "^0.12.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "1hjacpyjlyw42vqgik23sc6s76drwb4qjjlsjawqyyd2jp6xp19f")))

(define-public crate-cynic-proc-macros-0.13.0 (c (n "cynic-proc-macros") (v "0.13.0") (d (list (d (n "cynic-codegen") (r "^0.13.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "0i4xbhksh7s5lr502h4d4hhvi6jn1cnbkyscax13cib87vrynspp")))

(define-public crate-cynic-proc-macros-0.13.1 (c (n "cynic-proc-macros") (v "0.13.1") (d (list (d (n "cynic-codegen") (r "^0.13.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "0cfr1dp316746fbj649s4yrgbr88pq3457a225f6f95mxwg0q5fd")))

(define-public crate-cynic-proc-macros-0.14.0 (c (n "cynic-proc-macros") (v "0.14.0") (d (list (d (n "cynic-codegen") (r "^0.14.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "1ki2f3fxs8gca3cqzk82sdw8ivaazcmd1qbjw1cvd37s3yzgj97y")))

(define-public crate-cynic-proc-macros-0.14.1 (c (n "cynic-proc-macros") (v "0.14.1") (d (list (d (n "cynic-codegen") (r "^0.14.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "0qymmzdfwr6rdsg6p5yxahavx94ylc4xzn2akbj7vla0wq9nj09d")))

(define-public crate-cynic-proc-macros-0.15.0 (c (n "cynic-proc-macros") (v "0.15.0") (d (list (d (n "cynic-codegen") (r "^0.15.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "0fgiqwfp9x2w9cjmf4m7y555i1an70yqddypn720x1f8rxqw78mh")))

(define-public crate-cynic-proc-macros-0.15.1 (c (n "cynic-proc-macros") (v "0.15.1") (d (list (d (n "cynic-codegen") (r "^0.15.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (d #t) (k 0)))) (h "07fjnzlysl992n5v6si6yrmgdplp8v3hxhqwraamim6qsga58v89")))

(define-public crate-cynic-proc-macros-1.0.0 (c (n "cynic-proc-macros") (v "1.0.0") (d (list (d (n "cynic-codegen") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (d #t) (k 0)))) (h "1yc523m59m50379jwn5bwkxaww9ggmmhr73s6dpc1wvgdbsdxaqw")))

(define-public crate-cynic-proc-macros-2.0.0 (c (n "cynic-proc-macros") (v "2.0.0") (d (list (d (n "cynic-codegen") (r "^2.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.87") (d #t) (k 0)))) (h "1gaxl13kj2nmcx69qd2fb46anc3viyc65c33dm0jvymwn7iwyg8d")))

(define-public crate-cynic-proc-macros-2.0.1 (c (n "cynic-proc-macros") (v "2.0.1") (d (list (d (n "cynic-codegen") (r "^2.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.87") (d #t) (k 0)))) (h "0s6nilchpghpkd5fa51h5ysdr5769rs7z4xcp9vhpp4r31x1jrag")))

(define-public crate-cynic-proc-macros-2.1.0 (c (n "cynic-proc-macros") (v "2.1.0") (d (list (d (n "cynic-codegen") (r "^2.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.87") (d #t) (k 0)))) (h "01276ngzqg67gs7p9bfm6aj3ag6pbaqq7q097j8g4z9mri5pdykw")))

(define-public crate-cynic-proc-macros-2.2.0 (c (n "cynic-proc-macros") (v "2.2.0") (d (list (d (n "cynic-codegen") (r "^2.2.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.87") (d #t) (k 0)))) (h "19xvh6ds9as9si97zknfvz9x22pas497z2h8sj64kd1harjfs2gr")))

(define-public crate-cynic-proc-macros-2.2.1 (c (n "cynic-proc-macros") (v "2.2.1") (d (list (d (n "cynic-codegen") (r "^2.2.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.87") (d #t) (k 0)))) (h "0lapyvm1izg3xbnjr5swjkw9335jh4l92jya681np147djzwacma")))

(define-public crate-cynic-proc-macros-2.2.2 (c (n "cynic-proc-macros") (v "2.2.2") (d (list (d (n "cynic-codegen") (r "^2.2.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.87") (d #t) (k 0)))) (h "1nw00vbzga4faxsv9q8y6m3s3i3jj5kp2b5lf0p7rqvsbdgn0j69")))

(define-public crate-cynic-proc-macros-2.2.3 (c (n "cynic-proc-macros") (v "2.2.3") (d (list (d (n "cynic-codegen") (r "^2.2.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.87") (d #t) (k 0)))) (h "06zdk4rd003d1s42mhydgbsq349lp25y3hlj6jc2gydg3waa6v7i")))

(define-public crate-cynic-proc-macros-2.2.4 (c (n "cynic-proc-macros") (v "2.2.4") (d (list (d (n "cynic-codegen") (r "^2.2.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.87") (d #t) (k 0)))) (h "0n4308g47mhj4dlb0amz15xb9y6kma5awpqgl329gi80vrpbchb7")))

(define-public crate-cynic-proc-macros-2.2.5 (c (n "cynic-proc-macros") (v "2.2.5") (d (list (d (n "cynic-codegen") (r "^2.2.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.87") (d #t) (k 0)))) (h "0cmxa163a942yb6h1qnr9g11pmzjfn1iix3drsi2858zjci1lh7p")))

(define-public crate-cynic-proc-macros-2.2.6 (c (n "cynic-proc-macros") (v "2.2.6") (d (list (d (n "cynic-codegen") (r "^2.2.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.87") (d #t) (k 0)))) (h "0b8s36amchjb0555rlc48lsikslpiz6glw0wqhg4wbry1k7rkivh")))

(define-public crate-cynic-proc-macros-2.2.7 (c (n "cynic-proc-macros") (v "2.2.7") (d (list (d (n "cynic-codegen") (r "^2.2.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.87") (d #t) (k 0)))) (h "1x76azsi2mi8xw0a8vi59arwal5w941qyi6c8i5vq8hckmggwp2f")))

(define-public crate-cynic-proc-macros-2.2.8 (c (n "cynic-proc-macros") (v "2.2.8") (d (list (d (n "cynic-codegen") (r "^2.2.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.87") (d #t) (k 0)))) (h "0k8caazw1hxlshxrd4170r2pssxxjfflzhy5b074wdx5sx75qnda")))

(define-public crate-cynic-proc-macros-3.0.0-beta.1 (c (n "cynic-proc-macros") (v "3.0.0-beta.1") (d (list (d (n "cynic-codegen") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.87") (d #t) (k 0)))) (h "07ijsiqx4h8c21w80kzglw23rd1a7sxwjnbr8lj5bkhd27fl3wjs") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default"))))))

(define-public crate-cynic-proc-macros-3.0.0-beta.2 (c (n "cynic-proc-macros") (v "3.0.0-beta.2") (d (list (d (n "cynic-codegen") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.87") (d #t) (k 0)))) (h "17a9dxq87yrhzhxp7lr72a6cn77rsvchl039gynfdxi82qkrh85f") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default"))))))

(define-public crate-cynic-proc-macros-3.0.0-beta.3 (c (n "cynic-proc-macros") (v "3.0.0-beta.3") (d (list (d (n "cynic-codegen") (r "^3.0.0-beta.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.87") (d #t) (k 0)))) (h "1w25qizgp783fsyimwcksnbwi2wk8gzcwfl5580agvpslikwxlvg") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default"))))))

(define-public crate-cynic-proc-macros-3.0.0-beta.4 (c (n "cynic-proc-macros") (v "3.0.0-beta.4") (d (list (d (n "cynic-codegen") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.87") (d #t) (k 0)))) (h "17sm633sd3gr95l445549pkq2yv5g5bz5lbhmg65h63hzmsgk5yz") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default"))))))

(define-public crate-cynic-proc-macros-3.0.0 (c (n "cynic-proc-macros") (v "3.0.0") (d (list (d (n "cynic-codegen") (r "^3.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.87") (d #t) (k 0)))) (h "07f49srzw2an29spm9zj66n7s445s7gqszwywn95q5xniyagjjvi") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default"))))))

(define-public crate-cynic-proc-macros-3.0.1 (c (n "cynic-proc-macros") (v "3.0.1") (d (list (d (n "cynic-codegen") (r "^3.0.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.87") (d #t) (k 0)))) (h "0cylf7b74ncv4cdvq5vwn8d43abyp5s8k7ijf803m2k6zp114r3q") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default"))))))

(define-public crate-cynic-proc-macros-3.0.2 (c (n "cynic-proc-macros") (v "3.0.2") (d (list (d (n "cynic-codegen") (r "^3.0.2") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.87") (d #t) (k 0)))) (h "1v9lapqhvsq7s023izwh6hscfjh7r1hhalmmrglx0fzwfaaqkc62") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default"))))))

(define-public crate-cynic-proc-macros-3.1.0 (c (n "cynic-proc-macros") (v "3.1.0") (d (list (d (n "cynic-codegen") (r "^3.1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.87") (d #t) (k 0)))) (h "061g872zcrklkg8xxmma1z32ryrslc8kzsk85y22231zqdlaks6s") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default")))) (r "1.69")))

(define-public crate-cynic-proc-macros-3.1.1 (c (n "cynic-proc-macros") (v "3.1.1") (d (list (d (n "cynic-codegen") (r "^3.1.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.87") (d #t) (k 0)))) (h "1p0hrxb7n65bxm4r3jfmsnii8z3j19dnpz6q3m1kfv2cq63abjc1") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default")))) (r "1.69")))

(define-public crate-cynic-proc-macros-3.2.0 (c (n "cynic-proc-macros") (v "3.2.0") (d (list (d (n "cynic-codegen") (r "^3.2.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.87") (d #t) (k 0)))) (h "1hj9837zf7hp6cfyrj1nfjqhckv9qq4ipngl7x8v9ibvasqgkcpy") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default")))) (r "1.69")))

(define-public crate-cynic-proc-macros-3.2.1 (c (n "cynic-proc-macros") (v "3.2.1") (d (list (d (n "cynic-codegen") (r "^3.2.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.87") (d #t) (k 0)))) (h "1bdayv6v3c9ccfayabszgsi3pjv91gd862552dic5xry0y9fi6ib") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default")))) (r "1.69")))

(define-public crate-cynic-proc-macros-3.2.2 (c (n "cynic-proc-macros") (v "3.2.2") (d (list (d (n "cynic-codegen") (r "^3.2.2") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.87") (d #t) (k 0)))) (h "1wgbd4gjn9yswj895y2lsmcax5hncdbr9ib8427x9jspyfhkx4sa") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default")))) (r "1.69")))

(define-public crate-cynic-proc-macros-3.3.0 (c (n "cynic-proc-macros") (v "3.3.0") (d (list (d (n "cynic-codegen") (r "^3.3.0") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "05kginm0vsgl4dqpqsm1ch8nwn7gin7lax9cq64ann0xbnm8k42a") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default")))) (r "1.69")))

(define-public crate-cynic-proc-macros-3.3.1 (c (n "cynic-proc-macros") (v "3.3.1") (d (list (d (n "cynic-codegen") (r "^3.3.1") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0k6hj6lldrhgbbr3ih3kkvxn2w8vgxdbz2z388nadav02xqih7fg") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default")))) (r "1.69")))

(define-public crate-cynic-proc-macros-3.3.2 (c (n "cynic-proc-macros") (v "3.3.2") (d (list (d (n "cynic-codegen") (r "^3.3.2") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1aj82fg1rr00xn7yhagqfdmavc92ac8kazp3zv6q4x2bvvcaz4ki") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default")))) (r "1.69")))

(define-public crate-cynic-proc-macros-3.3.3 (c (n "cynic-proc-macros") (v "3.3.3") (d (list (d (n "cynic-codegen") (r "^3.3.3") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0pipq3fs6r0zq1vch5ly63p10fp3gl2xafw938rhaal14d8zxncp") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default")))) (r "1.69")))

(define-public crate-cynic-proc-macros-3.4.0 (c (n "cynic-proc-macros") (v "3.4.0") (d (list (d (n "cynic-codegen") (r "^3.4.0") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0yk7pmf98gr3dwc336jj43rl0r2ci67905sym2kh7z8ys56aaxqw") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default")))) (r "1.69")))

(define-public crate-cynic-proc-macros-3.4.1 (c (n "cynic-proc-macros") (v "3.4.1") (d (list (d (n "cynic-codegen") (r "^3.4.1") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0gv2w4fy4r5b7gr6wl90g0bi6k4vawkavh35z89ns7m2nymi4ywn") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default")))) (r "1.69")))

(define-public crate-cynic-proc-macros-3.4.2 (c (n "cynic-proc-macros") (v "3.4.2") (d (list (d (n "cynic-codegen") (r "^3.4.2") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1gfqkki8kyj5x5c74c6947c877lwjhwfks3mp33lg9jw52vdl7qw") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default")))) (r "1.69")))

(define-public crate-cynic-proc-macros-3.4.3 (c (n "cynic-proc-macros") (v "3.4.3") (d (list (d (n "cynic-codegen") (r "^3.4.3") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0qdxrhzkq2qkc5bg775nn49dmnb6f6z2xyxf3kqxlx3p5a3sb0gb") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default")))) (r "1.69")))

(define-public crate-cynic-proc-macros-3.5.0 (c (n "cynic-proc-macros") (v "3.5.0") (d (list (d (n "cynic-codegen") (r "^3.5.0") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "06pz8310ssxr0nsdmscar732pmyax07anq72ynadbyvbl3gdnsxl") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default")))) (r "1.69")))

(define-public crate-cynic-proc-macros-3.5.1 (c (n "cynic-proc-macros") (v "3.5.1") (d (list (d (n "cynic-codegen") (r "^3.5.1") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "112bk616m5nl78ngdzy1ific8gw96v9xf7r3ad6qddj7kw2r0j7z") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default")))) (r "1.69")))

(define-public crate-cynic-proc-macros-3.6.0 (c (n "cynic-proc-macros") (v "3.6.0") (d (list (d (n "cynic-codegen") (r "^3.6.0") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0bl37pav7c3hkr0yxsi316444y361fmkmidzjwpl8irscbj9jd4x") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default")))) (r "1.69")))

(define-public crate-cynic-proc-macros-3.6.1 (c (n "cynic-proc-macros") (v "3.6.1") (d (list (d (n "cynic-codegen") (r "^3.6.1") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1680bylj2sb58j6adg0lkn09azmqx7yxc2i8arrvaxf7isg1fqrs") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default")))) (r "1.69")))

(define-public crate-cynic-proc-macros-3.7.0 (c (n "cynic-proc-macros") (v "3.7.0") (d (list (d (n "cynic-codegen") (r "^3.7.0") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1bnfflzw2wdwdkpfrxm7pwh7dh7mmbxhqjnxrwj98rifmahql30s") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default")))) (r "1.72")))

(define-public crate-cynic-proc-macros-3.7.1 (c (n "cynic-proc-macros") (v "3.7.1") (d (list (d (n "cynic-codegen") (r "^3.7.1") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1k4anx0s82b2q8w9sz987hhzl1wqc0c72x1asndkc3h8pp73rnqw") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default")))) (r "1.72")))

(define-public crate-cynic-proc-macros-3.7.2 (c (n "cynic-proc-macros") (v "3.7.2") (d (list (d (n "cynic-codegen") (r "^3.7.2") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "09l4zji3igjjg35n2vcs5k2l6bvxg8kyqg0gfvfswmhdxkf1rjfq") (f (quote (("rkyv" "cynic-codegen/rkyv") ("default")))) (r "1.72")))

