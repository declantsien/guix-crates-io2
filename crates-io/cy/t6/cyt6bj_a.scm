(define-module (crates-io cy t6 cyt6bj_a) #:use-module (crates-io))

(define-public crate-cyt6bj_a-0.0.1 (c (n "cyt6bj_a") (v "0.0.1") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0sympwf6pj9ng4h6ksa7gcb9m1glvlgb13qjmn2rk61ck5kj3a5f") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.64")))

