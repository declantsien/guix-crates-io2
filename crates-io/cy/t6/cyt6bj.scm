(define-module (crates-io cy t6 cyt6bj) #:use-module (crates-io))

(define-public crate-cyt6bj-0.0.1 (c (n "cyt6bj") (v "0.0.1") (d (list (d (n "cyt6bj_a") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "07dlsd2dfkfb6dkkidalp1vq5rz76l4zdbvhb1j0ql67vhc1lw4m") (f (quote (("rev_a" "cyt6bj_a") ("default" "rev_a")))) (s 2) (e (quote (("rt" "cyt6bj_a?/rt") ("critical-section" "cyt6bj_a?/critical-section")))) (r "1.64")))

