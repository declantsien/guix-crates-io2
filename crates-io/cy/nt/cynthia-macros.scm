(define-module (crates-io cy nt cynthia-macros) #:use-module (crates-io))

(define-public crate-cynthia-macros-0.0.1 (c (n "cynthia-macros") (v "0.0.1") (d (list (d (n "cynthia") (r "^0.0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rqbdx0fd0jdqhqq36nny60sqbirppph0rsm1h6lq3jwkwv4m2xl")))

(define-public crate-cynthia-macros-0.0.2 (c (n "cynthia-macros") (v "0.0.2") (d (list (d (n "cynthia") (r "^0.0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0akjh07ckpg3nm1aa6iczdxk5h39458afj92hgnr00qp241fj5sn")))

(define-public crate-cynthia-macros-0.0.3 (c (n "cynthia-macros") (v "0.0.3") (d (list (d (n "cynthia") (r "^0.0.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0x5a3bhvhzaivvicbflyc2rp54rp7xqsvf6n63gwr3v5yrgh9xn6")))

