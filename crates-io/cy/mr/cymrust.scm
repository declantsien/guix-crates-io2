(define-module (crates-io cy mr cymrust) #:use-module (crates-io))

(define-public crate-cymrust-0.3.0 (c (n "cymrust") (v "0.3.0") (d (list (d (n "chrono") (r "^0.2.25") (d #t) (k 0)) (d (n "resolve") (r "^0.1.2") (d #t) (k 0)))) (h "069fhz6wi2m4l513m2f2kj58i7zs72j7kpq6yliij065in3rgp2b")))

(define-public crate-cymrust-0.3.1 (c (n "cymrust") (v "0.3.1") (d (list (d (n "chrono") (r "^0.2.25") (d #t) (k 0)) (d (n "resolve") (r "^0.1.2") (d #t) (k 0)))) (h "040p14axid0adl8a5p8983w5hg9gahc71pz517pn1q4rjckkpzj3")))

(define-public crate-cymrust-0.3.2 (c (n "cymrust") (v "0.3.2") (d (list (d (n "chrono") (r "^0.2.25") (d #t) (k 0)) (d (n "resolve") (r "^0.1.2") (d #t) (k 0)))) (h "0km78shdhrdr8qiaqayqx75gldbyx04dh2kfrvb319q54sw93ldf")))

(define-public crate-cymrust-0.3.3 (c (n "cymrust") (v "0.3.3") (d (list (d (n "chrono") (r "^0.3.0") (d #t) (k 0)) (d (n "resolve") (r "^0.1.2") (d #t) (k 0)))) (h "0cpvvymiwvpadfh4cyr14akfqfasq2h742a2ny9l0da7kzw6ssf6")))

(define-public crate-cymrust-0.3.4 (c (n "cymrust") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "resolve") (r "^0.1.2") (d #t) (k 0)))) (h "0p2s638kaa2kqq79w8m81ynp74ihxx1bl9h2fb22vlyas48am3h4")))

(define-public crate-cymrust-0.4.0 (c (n "cymrust") (v "0.4.0") (d (list (d (n "chrono") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "trust-dns-resolver") (r ">=0.19.0, <0.20.0") (d #t) (k 0)))) (h "0w759xqcylyr0kqsbj49qhr874jzgbq72afrb0bli7p5cm5lzhas")))

(define-public crate-cymrust-0.4.1 (c (n "cymrust") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.20") (d #t) (k 0)))) (h "05jz0vin7nkca245dhi8m4xwr9x28qfrhdz9bcw4kv6ixaawflnz")))

