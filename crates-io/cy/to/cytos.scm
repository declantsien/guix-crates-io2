(define-module (crates-io cy to cytos) #:use-module (crates-io))

(define-public crate-cytos-1.0.0 (c (n "cytos") (v "1.0.0") (d (list (d (n "bio") (r "^1.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0qy5sy9q3r60i40bpk5l2w8g53sq1wsvv89ychkbaflqgkphn71b") (r "1.70.0")))

(define-public crate-cytos-1.1.0 (c (n "cytos") (v "1.1.0") (d (list (d (n "bio") (r "^1.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1rx3bnak6lnbq5hvia5c1f6505pvq2r3h0svr4walhp891mwcjyl") (r "1.70.0")))

(define-public crate-cytos-1.2.0 (c (n "cytos") (v "1.2.0") (d (list (d (n "bio") (r "^1.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0nh7shkivk527lvdfl9wlrx7bz0nrw3sny9mn6m070x846byp942") (r "1.70.0")))

(define-public crate-cytos-1.2.1 (c (n "cytos") (v "1.2.1") (d (list (d (n "bio") (r "^1.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0bi5lkc9wvvgz6s2mdi5imaw7dsymqkc77f4kh14k7hgy423ylpa") (r "1.70.0")))

