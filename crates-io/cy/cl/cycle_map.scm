(define-module (crates-io cy cl cycle_map) #:use-module (crates-io))

(define-public crate-cycle_map-0.1.0 (c (n "cycle_map") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.12.0") (f (quote ("raw" "ahash"))) (k 0)))) (h "15h4ax3djqj79hgi36jlmk99qlawnbdmxn743m7xcr4iqm0zpp4a")))

(define-public crate-cycle_map-0.1.1 (c (n "cycle_map") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.12.0") (f (quote ("raw" "ahash"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1sp11nb351asgnk9n9x6l29xg60a3gc6727p0x59lj4icjf4in41")))

(define-public crate-cycle_map-0.2.0 (c (n "cycle_map") (v "0.2.0") (d (list (d (n "hashbrown") (r "^0.12.0") (f (quote ("raw" "ahash"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1bcjn3lv4jv396m47g2nzm9vhgrzw1d0kjixjfnwcw2r90rnjw01")))

