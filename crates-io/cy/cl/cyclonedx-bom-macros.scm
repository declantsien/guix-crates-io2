(define-module (crates-io cy cl cyclonedx-bom-macros) #:use-module (crates-io))

(define-public crate-cyclonedx-bom-macros-0.1.0 (c (n "cyclonedx-bom-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full" "fold"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "034qgq69axnbg3s0bygkchnhjv3qmavk8zli9wml2jzn3pr420y5") (r "1.70.0")))

