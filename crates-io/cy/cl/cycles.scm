(define-module (crates-io cy cl cycles) #:use-module (crates-io))

(define-public crate-cycles-0.1.0 (c (n "cycles") (v "0.1.0") (d (list (d (n "num-rational") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "1di91fmgc97mmmzqgb36hn2n1ba480f7k89jq89r5f9dcq8mhyla") (r "1.65.0")))

(define-public crate-cycles-0.1.1 (c (n "cycles") (v "0.1.1") (d (list (d (n "num-rational") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "0kl66l868l0hn1b4344l0mvn44dnayc0sk0n4jgwmvk9aml26jxi") (r "1.65.0")))

(define-public crate-cycles-0.1.2 (c (n "cycles") (v "0.1.2") (d (list (d (n "num-rational") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "0wd6yx5nxffy3swm4354apdc5arfmqqqqvwyn93z69pa7ljysbm5") (r "1.65.0")))

(define-public crate-cycles-0.1.3 (c (n "cycles") (v "0.1.3") (d (list (d (n "num-rational") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "0w4w6w4b2wh4pb0p195s7wl3ii9arcdlcdqkbcxg0617rpbcwvk0") (r "1.65.0")))

(define-public crate-cycles-0.2.0 (c (n "cycles") (v "0.2.0") (d (list (d (n "num-rational") (r "^0.4") (f (quote ("std"))) (k 0)))) (h "0glgaid654jghrf2b4ii3qjz7v6k6chamysd7f209rfiyrr08z4l") (r "1.65.0")))

