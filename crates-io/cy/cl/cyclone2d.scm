(define-module (crates-io cy cl cyclone2d) #:use-module (crates-io))

(define-public crate-cyclone2d-0.1.0 (c (n "cyclone2d") (v "0.1.0") (d (list (d (n "sdl2") (r "^0.32") (f (quote ("gfx"))) (k 2)) (d (n "vek2d") (r "^1.0") (d #t) (k 0)))) (h "0kbh1lwn2w95h9sp1vnd7dnww5fg6sksrw86snx7jdap0ir57fb2") (f (quote (("high_precision") ("default" "high_precision"))))))

(define-public crate-cyclone2d-0.1.1 (c (n "cyclone2d") (v "0.1.1") (d (list (d (n "sdl2") (r "^0.32") (f (quote ("gfx"))) (k 2)) (d (n "vek2d") (r "^1.0") (d #t) (k 0)))) (h "0mnv387px5k4b4f9wc2435x2r91vc3sz6rx6nmm7sdfl64lwgrim") (f (quote (("high_precision") ("default" "high_precision"))))))

(define-public crate-cyclone2d-0.1.2 (c (n "cyclone2d") (v "0.1.2") (d (list (d (n "sdl2") (r "^0.32") (f (quote ("gfx"))) (k 2)) (d (n "vek2d") (r "^1.0") (d #t) (k 0)))) (h "06bb6ggn38fp0igfjzq7lmxrg195afwsmic8s4q7nwcw7cmwj542") (f (quote (("high_precision") ("default" "high_precision"))))))

