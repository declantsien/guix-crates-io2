(define-module (crates-io cy cl cyclang-macros) #:use-module (crates-io))

(define-public crate-cyclang-macros-0.1.15 (c (n "cyclang-macros") (v "0.1.15") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1mbf0dqzar3k34fgpdnn30sxcnwadai0py2xmlg4zw22kr7nw712")))

(define-public crate-cyclang-macros-0.1.16 (c (n "cyclang-macros") (v "0.1.16") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0m47wnyzlm01q7ympmkzcdi7jfl2cp9lqylw5fvgjdvnz2a6x8iv")))

(define-public crate-cyclang-macros-0.1.17 (c (n "cyclang-macros") (v "0.1.17") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0ki6h3z8l57nixcyrk910dk6ajwlfpz2675cf4fz2hfq4m3n40w0")))

(define-public crate-cyclang-macros-0.1.18 (c (n "cyclang-macros") (v "0.1.18") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1m4hhv7fjxx77sixm59228z51amsrvsh1x548q6qifkydwja45lf")))

