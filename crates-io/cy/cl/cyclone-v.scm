(define-module (crates-io cy cl cyclone-v) #:use-module (crates-io))

(define-public crate-cyclone-v-0.1.0 (c (n "cyclone-v") (v "0.1.0") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "memoffset") (r "^0.9.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1fpwyypr27347r2z9fzlvdqrhbx5c9b6xfhjk574lf6dpycgfd7f") (f (quote (("std") ("default" "std"))))))

