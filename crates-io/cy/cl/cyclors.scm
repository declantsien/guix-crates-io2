(define-module (crates-io cy cl cyclors) #:use-module (crates-io))

(define-public crate-cyclors-0.1.0 (c (n "cyclors") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)))) (h "10gqhjj2yi4vkv28b76bsk329mpazxhqvjgz7jrg2jzgjp1g0in1")))

(define-public crate-cyclors-0.1.1 (c (n "cyclors") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)))) (h "1sdxqk6n6080linrhpnrrwzyxj9hkzij4gjll5pc3qihlim5y60q")))

(define-public crate-cyclors-0.1.2 (c (n "cyclors") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)))) (h "13g9nc6xij8bygjrxrf38ghlp7b1q53mff00jlfg08agn89lrzk5")))

(define-public crate-cyclors-0.1.3 (c (n "cyclors") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)))) (h "0y8d35vf724ddk4946x8rjxjv452ygmhypg73zdy4r7m1alpighl")))

(define-public crate-cyclors-0.1.4 (c (n "cyclors") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)))) (h "1372nj0yh5ai6l8bf4aipjrx4akcym6mbwylnwzvvnvy2l0fh7x8")))

(define-public crate-cyclors-0.1.5 (c (n "cyclors") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)))) (h "1z39m4ddsi2ymxsh0ig4dd0yirw9y2q9v5l49xnjnikycyvlwn52")))

(define-public crate-cyclors-0.1.6 (c (n "cyclors") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)))) (h "0zkjqf8lyh6bdfw526jby4ci6zki0zf3hkbzipmgdsxyjgwmzj9f")))

(define-public crate-cyclors-0.1.7 (c (n "cyclors") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)))) (h "00pc032q82lnyqkp9ghdw44a7680ka43y8x6b1vzwkjgf0j4mjzq")))

(define-public crate-cyclors-0.2.0 (c (n "cyclors") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "0b5hdrgwbb1fyvcnxjkp0mi4swx60sxcxwyw7m1mb3wn9yfxapad") (f (quote (("iceoryx"))))))

