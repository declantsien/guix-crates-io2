(define-module (crates-io cy cl cycler) #:use-module (crates-io))

(define-public crate-cycler-0.1.0 (c (n "cycler") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.11.1") (f (quote ("send_guard"))) (d #t) (k 0)))) (h "1v8zgw2byxcgc5iml0argyfnpaki0a8haxanyzs8ca0maipc9j4w") (f (quote (("unsafe_cleanup") ("default"))))))

(define-public crate-cycler-0.1.1 (c (n "cycler") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.11.1") (f (quote ("send_guard"))) (d #t) (k 0)))) (h "03smzp2wjhjchzbb40ir79nkghargwacvp851vfwfv093vcqsg88") (f (quote (("unsafe_cleanup") ("default" "unsafe_cleanup")))) (y #t)))

(define-public crate-cycler-0.1.2 (c (n "cycler") (v "0.1.2") (d (list (d (n "parking_lot") (r "^0.11.1") (f (quote ("send_guard"))) (d #t) (k 0)))) (h "0fnbfrhl1563mn0g5q6dai43f61aggj2q9q6h9s298avyhglag36") (f (quote (("unsafe_cleanup") ("default" "unsafe_cleanup"))))))

(define-public crate-cycler-0.1.3 (c (n "cycler") (v "0.1.3") (d (list (d (n "parking_lot") (r "^0.11.1") (f (quote ("send_guard"))) (d #t) (k 0)))) (h "0ly5b234agg7mskpbb4vchcxph221psjnx0csdy8c6q9di7cawyz") (f (quote (("unsafe_cleanup") ("default" "unsafe_cleanup"))))))

(define-public crate-cycler-0.2.0 (c (n "cycler") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.11.1") (f (quote ("send_guard"))) (d #t) (k 0)))) (h "02xq5yw0jw6l1g04544vc92spfxii0z04skvkmls6vj22kv8bvrg") (f (quote (("unsafe_cleanup" "allow_unsafe") ("default" "allow_unsafe" "unsafe_cleanup") ("allow_unsafe"))))))

(define-public crate-cycler-0.3.0 (c (n "cycler") (v "0.3.0") (d (list (d (n "parking_lot") (r "^0.11.1") (f (quote ("send_guard"))) (d #t) (k 0)))) (h "1662l81bsryrwlimb56cq2xjj1flm63fvhnmzfn5ynf55227493l") (f (quote (("unsafe_cleanup" "allow_unsafe") ("default" "allow_unsafe" "unsafe_cleanup" "atomic_cycler") ("atomic_cycler" "allow_unsafe") ("allow_unsafe"))))))

