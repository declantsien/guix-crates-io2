(define-module (crates-io cy cl cycle-sort) #:use-module (crates-io))

(define-public crate-cycle-sort-0.1.0 (c (n "cycle-sort") (v "0.1.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1mdd5wkcbhvacqp1ap7haz9ymplxq6na62ixgvfn209vamcpk6sq")))

(define-public crate-cycle-sort-0.1.1 (c (n "cycle-sort") (v "0.1.1") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "09s2zhwm2092z8xld642fqdwsycq9jk2kkcq2wwf1cj593i96c2p")))

(define-public crate-cycle-sort-0.2.0 (c (n "cycle-sort") (v "0.2.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1prn3ply38gxbzmxb60g2dwqgfy4aq5fii4q6izxl0azwm95wk3k")))

(define-public crate-cycle-sort-0.3.0 (c (n "cycle-sort") (v "0.3.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "07dk4h1h9gs1nkpbx5jw2i87zza1scy2xps1xm41rdh4k269j0qi")))

