(define-module (crates-io cy cl cycle-menu) #:use-module (crates-io))

(define-public crate-cycle-menu-0.1.0 (c (n "cycle-menu") (v "0.1.0") (h "080w01f72bw9bh1w99g6dm8li5kq8m42jq2dcfcknjwk6fng1wbg")))

(define-public crate-cycle-menu-0.2.0 (c (n "cycle-menu") (v "0.2.0") (h "0rli1774sqqxsx8dm2izs8gbqv8hzky6w57kpykc1invnlgpvhg2") (f (quote (("std") ("default" "std"))))))

(define-public crate-cycle-menu-0.3.0 (c (n "cycle-menu") (v "0.3.0") (h "06wim891i6g8dpf7hx2mkfddbv7ln51lb32blwg9x2j2pi4la2my") (f (quote (("std") ("default" "std"))))))

(define-public crate-cycle-menu-0.4.0 (c (n "cycle-menu") (v "0.4.0") (h "0pbfzwb24kgz3jp2wavfgixx91g6xz1cyd8a3w0h5n0wnf45ycxz") (f (quote (("std") ("default" "std"))))))

(define-public crate-cycle-menu-0.4.1 (c (n "cycle-menu") (v "0.4.1") (h "08kax0w2mma4bd7qhcxw8ijmxbv83yldh734wxrp5h9zvs7zazc8") (f (quote (("std") ("default" "std"))))))

(define-public crate-cycle-menu-0.4.2 (c (n "cycle-menu") (v "0.4.2") (h "005knjyv45kywdbvh4j9dvxipjjzrvl5aikhi46ihn7anvfsiy6v") (f (quote (("std") ("default" "std"))))))

