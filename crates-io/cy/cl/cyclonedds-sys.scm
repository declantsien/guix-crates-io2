(define-module (crates-io cy cl cyclonedds-sys) #:use-module (crates-io))

(define-public crate-cyclonedds-sys-0.1.0 (c (n "cyclonedds-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)) (d (n "bitmask") (r "^0.5") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "18cm2gjbxlz0nr79hxi1ffh4n8hdr3a62hx65j0y6n51f05hf9jc") (l "ddsc")))

(define-public crate-cyclonedds-sys-0.1.1 (c (n "cyclonedds-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)) (d (n "bitmask") (r "^0.5") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "1116fwmyp9kig68imhr0ckwnvd793p7f9ws6n3b61rxgabg6glcx") (l "ddsc")))

(define-public crate-cyclonedds-sys-0.1.2 (c (n "cyclonedds-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "bitmask") (r "^0.5") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "0bpp73s68cgr6zirvamj3gd7dmm632dzd53dv8dqnx90r30hbgg0") (l "ddsc")))

(define-public crate-cyclonedds-sys-0.1.3 (c (n "cyclonedds-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "bitmask") (r "^0.5") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "0srhywghqvqf3cfkd8idpnpvdja8fa52d7syj0ajkw6icw4x1xdi") (f (quote (("shm") ("default" "shm")))) (l "ddsc")))

(define-public crate-cyclonedds-sys-0.1.4 (c (n "cyclonedds-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "bitmask") (r "^0.5") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "1i90h7q44xg3cxg12hdlmw5r38alrcz454yp70615g7yb5n5jq28") (f (quote (("shm") ("default" "shm")))) (l "ddsc")))

(define-public crate-cyclonedds-sys-0.1.5 (c (n "cyclonedds-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "bitmask") (r "^0.5") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "1xv7p2wzj6x4amqvjdmlsd2sh87ys4zg5i5a3gxfmyn3kvinx998") (f (quote (("shm") ("default" "shm")))) (l "ddsc")))

(define-public crate-cyclonedds-sys-0.1.6 (c (n "cyclonedds-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "bitmask") (r "^0.5") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "0cy2zfz16dn6kcxb1s0nh12lxnc905vly56hkyyjvwvmcpmggmj6") (f (quote (("shm") ("default" "shm")))) (l "ddsc")))

(define-public crate-cyclonedds-sys-0.1.7 (c (n "cyclonedds-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "bitmask") (r "^0.5") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "0qmnrvsxfc9k9f02q36spvaiwq2wamm5ml8ds0vwdg483wm80i3p") (f (quote (("shm") ("default" "shm")))) (l "ddsc")))

(define-public crate-cyclonedds-sys-0.2.0 (c (n "cyclonedds-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "bitmask") (r "^0.5") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "1dd2wm19avbksr5axai9shky4fpviyhxhf20n3rhzdhgabgb6bpa") (f (quote (("shm") ("default" "shm")))) (l "ddsc")))

(define-public crate-cyclonedds-sys-0.2.1 (c (n "cyclonedds-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "bitmask") (r "^0.5") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "17999m8ls2jxp25xilrwsjkpcad7m0ad3l0ax1l60s28g9cv8f56") (f (quote (("shm") ("default" "shm")))) (l "ddsc")))

