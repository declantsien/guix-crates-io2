(define-module (crates-io cy cl cycle) #:use-module (crates-io))

(define-public crate-cycle-0.0.1 (c (n "cycle") (v "0.0.1") (h "19gvhian7bhf0r9v4dknxy447y8zllw2f02h0n9v4ls3nqrnkfl2")))

(define-public crate-cycle-0.0.2 (c (n "cycle") (v "0.0.2") (h "0azg7z27wfzf4p5giw5zwph7zcyqqlksdm3kzs9kc4dl0aysahv4")))

(define-public crate-cycle-0.0.3 (c (n "cycle") (v "0.0.3") (h "0x42h4xp4lpi987qz2if028k1c6rmlf6pxpny7xfdg6dh75pxavl")))

(define-public crate-cycle-0.0.4 (c (n "cycle") (v "0.0.4") (h "12azc62qzijn9qrg43i1cp284d6n13a65rdh0bk3v7sphpkhq496") (f (quote (("default" "cycle_lang" "cycle_plot") ("cycle_plot") ("cycle_lang"))))))

(define-public crate-cycle-0.1.0 (c (n "cycle") (v "0.1.0") (h "0ddznvm95xjwryk130xivaij7pz6favjhbqahz9v5k2cwbvlix9y") (f (quote (("default" "cycle_lang" "cycle_plot") ("cycle_plot") ("cycle_lang"))))))

(define-public crate-cycle-0.1.1 (c (n "cycle") (v "0.1.1") (h "148ynasxh3s07xig08hqdsgq83135ccba1mipwk21n41bglwlqb6") (f (quote (("default" "cycle_lang" "cycle_plot") ("cycle_plot") ("cycle_lang"))))))

(define-public crate-cycle-0.2.0 (c (n "cycle") (v "0.2.0") (h "0iiyii991jckkq0m9bm01q27bgc0wm5ahk68qg0j1x69vg845hnz") (f (quote (("default" "cycle_lang" "cycle_plot") ("cycle_plot") ("cycle_lang"))))))

(define-public crate-cycle-0.2.1 (c (n "cycle") (v "0.2.1") (h "1a43k851dkvl9l6505a4vccyjwssdaqmisq198rd2axw3v6xi8kw") (f (quote (("default" "cycle_lang" "cycle_plot") ("cycle_plot") ("cycle_lang"))))))

(define-public crate-cycle-0.3.0 (c (n "cycle") (v "0.3.0") (h "00lklnrvpfa4p3h42lz6zdbrl20jb1xrxjjlaz0y1lr577yjzwib") (f (quote (("default" "cycle_lang" "cycle_num" "cycle_plot") ("cycle_plot") ("cycle_num") ("cycle_lang"))))))

(define-public crate-cycle-0.4.0 (c (n "cycle") (v "0.4.0") (h "1gya5clna1m3yiiav8112wg2m9aw82mvz7hpklpnny8hs68mqlk3") (f (quote (("default" "cycle_lang" "cycle_num" "cycle_plot") ("cycle_plot") ("cycle_num") ("cycle_lang"))))))

(define-public crate-cycle-0.4.1 (c (n "cycle") (v "0.4.1") (h "0fdmhkalmy2992a7p121fybzda9g0abn6rasj30zd8gc5za1y84s") (f (quote (("default") ("cycle_plot") ("cycle_num") ("cycle_lang"))))))

