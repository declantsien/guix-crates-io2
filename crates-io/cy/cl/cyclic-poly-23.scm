(define-module (crates-io cy cl cyclic-poly-23) #:use-module (crates-io))

(define-public crate-cyclic-poly-23-0.1.0 (c (n "cyclic-poly-23") (v "0.1.0") (d (list (d (n "adler32") (r "^1") (d #t) (k 2)) (d (n "easybench") (r "^1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0wv0nvk7sh93xpqgmsg02fwaqdnjdh819a1h5f27b2qjhszmb20c")))

(define-public crate-cyclic-poly-23-0.2.0 (c (n "cyclic-poly-23") (v "0.2.0") (d (list (d (n "adler32") (r "^1") (d #t) (k 2)) (d (n "easybench") (r "^1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "04jq42bfw26zzqqlm75wnri9wglb134nkdnfwj3kpfj3k2x6j1hn")))

(define-public crate-cyclic-poly-23-0.3.0 (c (n "cyclic-poly-23") (v "0.3.0") (d (list (d (n "adler32") (r "^1") (d #t) (k 2)) (d (n "easybench") (r "^1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "151iwqka08kwnajdzx9lxch2wpwsvhc4gb3wh3iwjg7dm9w3x5q9") (f (quote (("std") ("default" "std" "num-traits/std"))))))

(define-public crate-cyclic-poly-23-0.3.1 (c (n "cyclic-poly-23") (v "0.3.1") (d (list (d (n "adler32") (r "^1") (d #t) (k 2)) (d (n "easybench") (r "^1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0cl90dzj7bb2s227chqc59hy874n0z4xdlvii3ssvsh55m75hdji") (f (quote (("std") ("default" "std" "num-traits/std"))))))

