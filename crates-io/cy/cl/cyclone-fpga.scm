(define-module (crates-io cy cl cyclone-fpga) #:use-module (crates-io))

(define-public crate-cyclone-fpga-0.1.0-pre (c (n "cyclone-fpga") (v "0.1.0-pre") (d (list (d (n "cyclone-f1-sys") (r "^0.1.0-pre") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "08ak2l0jx05dbl7fz21bdmyzhx5cgpyx5a1s5n2fj1cjv068brh8") (f (quote (("f1" "cyclone-f1-sys") ("default" "f1")))) (r "1.62")))

