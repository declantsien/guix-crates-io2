(define-module (crates-io cy cl cyclotron) #:use-module (crates-io))

(define-public crate-cyclotron-0.0.1 (c (n "cyclotron") (v "0.0.1") (h "0mxrmm7zg5cgwhswp72bi1a33hmhfrnaaky4lyb4w21mf7sw64il")))

(define-public crate-cyclotron-0.0.2 (c (n "cyclotron") (v "0.0.2") (h "0m75ilpc17xh94i8pjv2ckj1s0yv7v9i42afiscxlqhqxqc4fpa1")))

(define-public crate-cyclotron-0.0.3 (c (n "cyclotron") (v "0.0.3") (h "1a0mhcg7c3xrwcsd75jxssybgac7kz32dh62sh03x6f7xyaig6a4")))

