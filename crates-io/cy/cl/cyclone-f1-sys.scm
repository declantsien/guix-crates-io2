(define-module (crates-io cy cl cyclone-f1-sys) #:use-module (crates-io))

(define-public crate-cyclone-f1-sys-0.1.0-pre (c (n "cyclone-f1-sys") (v "0.1.0-pre") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "0yqjwicd84xdfrzd6zrxb0diiyif4rcr80cnyhp7dg56ig0160hw") (r "1.62")))

