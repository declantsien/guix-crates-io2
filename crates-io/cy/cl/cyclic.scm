(define-module (crates-io cy cl cyclic) #:use-module (crates-io))

(define-public crate-cyclic-0.1.0 (c (n "cyclic") (v "0.1.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-test" "run-cargo-clippy" "run-cargo-fmt"))) (k 2)))) (h "1b1sm7zi5qsaxbbj26aq1lc5abfa58ci8mm9ncwc2zxsb35a0i3l")))

(define-public crate-cyclic-0.1.1 (c (n "cyclic") (v "0.1.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-test" "run-cargo-clippy" "run-cargo-fmt"))) (k 2)))) (h "09j23gixky4r6jhcpv7g433mjd9fqvninikkhch88aflvf08prbd") (f (quote (("default") ("composite_order_division"))))))

