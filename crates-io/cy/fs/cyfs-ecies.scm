(define-module (crates-io cy fs cyfs-ecies) #:use-module (crates-io))

(define-public crate-cyfs-ecies-0.1.4 (c (n "cyfs-ecies") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "hkdf") (r "^0.8.0") (d #t) (k 0)) (d (n "libsecp256k1") (r "^0.3.5") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "0zd22j4wrvkm1rw2f6ks7cyfg1r83sp2wympjg07clxh7wgc5w8q")))

