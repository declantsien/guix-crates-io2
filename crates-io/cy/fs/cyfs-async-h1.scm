(define-module (crates-io cy fs cyfs-async-h1) #:use-module (crates-io))

(define-public crate-cyfs-async-h1-2.3.3 (c (n "cyfs-async-h1") (v "2.3.3") (d (list (d (n "async-channel") (r "^1.5.1") (d #t) (k 0)) (d (n "async-dup") (r "^1.2.2") (d #t) (k 0)) (d (n "async-std") (r "^1.7.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.8") (d #t) (k 0)) (d (n "http-types") (r "^2.9.0") (k 0)) (d (n "httparse") (r "^1.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.2") (d #t) (k 0)) (d (n "async-std") (r "^1.7.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "0hqwvym0nxybkxrwk7wzz26wxqrbwpjdwlglqlq4k9063bb677rg")))

