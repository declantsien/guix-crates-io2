(define-module (crates-io cy fs cyfs-base-derive) #:use-module (crates-io))

(define-public crate-cyfs-base-derive-0.1.0 (c (n "cyfs-base-derive") (v "0.1.0") (h "0yqdnrxaxlfkka7wphpcv9ykcpkyjr4kdy9n9hh6lb8mx8xlsgkn")))

(define-public crate-cyfs-base-derive-0.5.0 (c (n "cyfs-base-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1v6zqy92inwvnd12c8ir3xhxg8qwnrxskd7jg6anq3vrx430c5sd")))

