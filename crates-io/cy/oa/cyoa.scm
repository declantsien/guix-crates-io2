(define-module (crates-io cy oa cyoa) #:use-module (crates-io))

(define-public crate-cyoa-0.1.0 (c (n "cyoa") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0s6qrd3far09v6i4v5853khigcg2zpqicargqaz2i6xb3zbkd08r")))

(define-public crate-cyoa-0.1.2 (c (n "cyoa") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1m28b419yhxys9ndkff6qq7nv145bgx3h41wx1m943wqahnwhbfq")))

(define-public crate-cyoa-0.1.3 (c (n "cyoa") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0ig5y1dk06c8k8kk4sjiampcjdaqwgycjk1x6ndhb6wic3f4gq5y")))

