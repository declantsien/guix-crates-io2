(define-module (crates-io cy t3 cyt3dl_a) #:use-module (crates-io))

(define-public crate-cyt3dl_a-0.0.1 (c (n "cyt3dl_a") (v "0.0.1") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0pwcndd4913sbqg6ccpb8551k5p1p4a47h2bgxgwizvk83kf7hd7") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.64")))

