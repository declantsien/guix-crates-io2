(define-module (crates-io cy t3 cyt3dl) #:use-module (crates-io))

(define-public crate-cyt3dl-0.0.1 (c (n "cyt3dl") (v "0.0.1") (d (list (d (n "cyt3dl_a") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "cyt3dl_b") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "cyt3dl_c") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "15wnp603nlqqxa3nkczsiyiq8r3bx93cahw4ynfcd8l7ji2ryksg") (f (quote (("rev_c" "cyt3dl_c") ("rev_b" "cyt3dl_b") ("rev_a" "cyt3dl_a") ("default" "rev_c")))) (s 2) (e (quote (("rt" "cyt3dl_a?/rt" "cyt3dl_b?/rt" "cyt3dl_c?/rt") ("critical-section" "cyt3dl_a?/critical-section" "cyt3dl_b?/critical-section" "cyt3dl_c?/critical-section")))) (r "1.64")))

