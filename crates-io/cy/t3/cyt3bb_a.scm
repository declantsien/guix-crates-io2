(define-module (crates-io cy t3 cyt3bb_a) #:use-module (crates-io))

(define-public crate-cyt3bb_a-0.0.1 (c (n "cyt3bb_a") (v "0.0.1") (d (list (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0rrs5i2ixsb4syzmm9m2s4clk1m0wmsad2dzlm3cv2wzmvwndw1k") (f (quote (("rt" "cortex-m-rt/device")))) (r "1.64")))

