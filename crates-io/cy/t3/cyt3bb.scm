(define-module (crates-io cy t3 cyt3bb) #:use-module (crates-io))

(define-public crate-cyt3bb-0.0.1 (c (n "cyt3bb") (v "0.0.1") (d (list (d (n "cyt3bb_a") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "cyt3bb_b") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "1sam6rwl726v3856wx3zpyqla30z8v7wf2jm0ly0v3k1shfa2gnh") (f (quote (("rev_b" "cyt3bb_b") ("rev_a" "cyt3bb_a") ("default" "rev_b")))) (s 2) (e (quote (("rt" "cyt3bb_a?/rt" "cyt3bb_b?/rt") ("critical-section" "cyt3bb_a?/critical-section" "cyt3bb_b?/critical-section")))) (r "1.64")))

