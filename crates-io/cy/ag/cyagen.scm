(define-module (crates-io cy ag cyagen) #:use-module (crates-io))

(define-public crate-cyagen-0.1.0 (c (n "cyagen") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "01kgjp8845pahd50z2xxpwnv672l1fkfa6igig1k2agwkyj77837")))

(define-public crate-cyagen-0.1.1 (c (n "cyagen") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1vb53w0w0c8snaxs0f2911lam2wdl0yw041h8200n4wv8z1pdv0k")))

(define-public crate-cyagen-0.1.2 (c (n "cyagen") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1knj357kqz5qs80gzb06jh3c6b3vrbdnwpmn9s4gjsdk6cxh6gn0")))

(define-public crate-cyagen-0.1.3 (c (n "cyagen") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0rshd4ikff9nznfl95fhn4n4r094px4741isyhwcz6zb6yvsns04")))

(define-public crate-cyagen-0.1.4 (c (n "cyagen") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1wa4qw4mapiq7b3mfsb6rnwqm0ppq9cjmm0qd2dqi2ia8w4sgvk0")))

(define-public crate-cyagen-0.1.5 (c (n "cyagen") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "17iilg1yaz2c7na8j2rr84c5c22yyb8539p5ncxk8q4qw4j0r9pl")))

(define-public crate-cyagen-0.1.6 (c (n "cyagen") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0hccb1g6al41kvgdh775565x97mdbakcrhk909mabiy6pzvd4153")))

(define-public crate-cyagen-0.1.7 (c (n "cyagen") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1bmrzm6xq0kq3x2qsmkaczlwmhnkzc6pmz48zc839z9yj5hcnsc4")))

(define-public crate-cyagen-0.1.8 (c (n "cyagen") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1lg4kdkmw7yvvqhlwzjynmnlsss35xsf2ws4l3n0r56n44f29hy0")))

(define-public crate-cyagen-0.1.9 (c (n "cyagen") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tera") (r "^1.18.1") (k 0)))) (h "0041ggy4cipnqgkpakkldmxd1zwimrsm34ji00v1yvgvpghjpdnl")))

(define-public crate-cyagen-0.1.10 (c (n "cyagen") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tera") (r "^1.19.0") (d #t) (k 0)) (d (n "uuid") (r "^1.4.0") (f (quote ("v5" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "136izsg5ssj1ykzyv3h1srqa3qbf2q6iskr74wrwhlw1cbb28mlm")))

