(define-module (crates-io bs wp bswp) #:use-module (crates-io))

(define-public crate-bswp-0.1.0 (c (n "bswp") (v "0.1.0") (h "0c2cnw383jzg88w0ghbdk4sk6jpb57lq25hg5bvambibv7qw43kn")))

(define-public crate-bswp-1.0.0 (c (n "bswp") (v "1.0.0") (h "1ax1rrxr18z39skhn6k5yipiiap56gywgiknv56c8002y1rvi4iv")))

