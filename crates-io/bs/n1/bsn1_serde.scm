(define-module (crates-io bs n1 bsn1_serde) #:use-module (crates-io))

(define-public crate-bsn1_serde-0.1.0 (c (n "bsn1_serde") (v "0.1.0") (d (list (d (n "bsn1") (r "^2.0.0") (d #t) (k 0)) (d (n "bsn1_serde_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "121c214bd81dpzd5baq7kxnfnn45947zrwv6v3dl656rfdi0kx12")))

(define-public crate-bsn1_serde-0.2.0 (c (n "bsn1_serde") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "bsn1") (r "^3.0.0") (d #t) (k 0)) (d (n "bsn1_serde_macros") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "0gqx5pxgpxbadmxcj7fn4kb25mnzjf2g7lfa0w4jfblmbi5rax4g")))

