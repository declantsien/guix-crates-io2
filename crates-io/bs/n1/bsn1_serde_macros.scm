(define-module (crates-io bs n1 bsn1_serde_macros) #:use-module (crates-io))

(define-public crate-bsn1_serde_macros-0.1.0 (c (n "bsn1_serde_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.72") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "0zlnx7dx7izbi1rlxchlqj3rwy7f6pf68ap0pp923n3nl68s11fi")))

(define-public crate-bsn1_serde_macros-0.2.0 (c (n "bsn1_serde_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.72") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (d #t) (k 0)))) (h "11vl8dhzx9n85h4h8214fi4phzdl4bgzxi7xcc240l9c8jfw89xn")))

