(define-module (crates-io bs n1 bsn1) #:use-module (crates-io))

(define-public crate-bsn1-0.1.0 (c (n "bsn1") (v "0.1.0") (h "0vsnq6mqpqf57hgc5i97w57abjy0x40hfsqc7zz7j4yfahr4ga12")))

(define-public crate-bsn1-0.2.0 (c (n "bsn1") (v "0.2.0") (h "06jqfn0fgs7pxxn4qgr6wrhg2vyms81yd1pr4sk2fp9g9vq6flyi")))

(define-public crate-bsn1-0.3.0 (c (n "bsn1") (v "0.3.0") (h "142sxignqym8vcm1qrklisqy91y4f8xnq0vk0c65dgkzqys0jplb")))

(define-public crate-bsn1-0.3.1 (c (n "bsn1") (v "0.3.1") (h "0bsp85zzlpbsjrdifqq7pqw4zg40yr9hfhwbyv33rpbxkyc078b3")))

(define-public crate-bsn1-0.4.0 (c (n "bsn1") (v "0.4.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0ypw7b0r8b7hmc3a7zc68kvsyafqzzz0882wda6f031mxxp6b2pc")))

(define-public crate-bsn1-0.9.0 (c (n "bsn1") (v "0.9.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1qbgv9qhgy3h3y53byid8gl4r9dqms2wi7y7yif5h6qzdxc848rb")))

(define-public crate-bsn1-1.0.0 (c (n "bsn1") (v "1.0.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0l95i7dkj0plvgwjfdny639icxzkzdwisw651q8rsy4zi3hxigid")))

(define-public crate-bsn1-2.0.0 (c (n "bsn1") (v "2.0.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "0lmk72k9s43cac4s4wym5c6wnc5px9dimff9kj7vch7f1fqhp877")))

(define-public crate-bsn1-2.0.1 (c (n "bsn1") (v "2.0.1") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "0na27v3v3agaazpls9pjgfl9j75jy46j11vb1zd46957xfin1jhp")))

(define-public crate-bsn1-3.0.0 (c (n "bsn1") (v "3.0.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1ircdcfkjj456v82s645r9v1id7iv86ah0dkivqja7cqvfhivsgz")))

