(define-module (crates-io bs od bsod) #:use-module (crates-io))

(define-public crate-bsod-0.1.0 (c (n "bsod") (v "0.1.0") (d (list (d (n "windows") (r "^0.43.0") (f (quote ("Win32_Foundation" "Win32_System_LibraryLoader"))) (d #t) (k 0)))) (h "0hlyv2m8n4qfyqm5ggz7sgl8r27hi9aj8ql8l2wk85lfm9qd254l")))

(define-public crate-bsod-0.1.1 (c (n "bsod") (v "0.1.1") (d (list (d (n "windows") (r "^0.43.0") (f (quote ("Win32_Foundation" "Win32_System_LibraryLoader"))) (d #t) (k 0)))) (h "1nwwg1p51x0k2v2bawsm7n1n5dxpvm0wpsvi2vhhphiqlcfybhp8")))

