(define-module (crates-io bs #{72}# bs721-base) #:use-module (crates-io))

(define-public crate-bs721-base-0.1.0-alpha.1 (c (n "bs721-base") (v "0.1.0-alpha.1") (d (list (d (n "bs721") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.16.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "cw2") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0b13ham9z8kp0qksyqhzkphn6h3v4dfkw4zyrvvbyj96r9nmrvg2") (f (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

