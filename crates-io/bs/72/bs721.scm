(define-module (crates-io bs #{72}# bs721) #:use-module (crates-io))

(define-public crate-bs721-0.1.0-alpha.1 (c (n "bs721") (v "0.1.0-alpha.1") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (k 0)))) (h "0jrjmpmkwrchwrkjs5ghhphrdsz1yy43nk3y81g2zsnxvg0197qv")))

