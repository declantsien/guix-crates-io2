(define-module (crates-io bs p_ bsp_rs) #:use-module (crates-io))

(define-public crate-bsp_rs-0.1.0 (c (n "bsp_rs") (v "0.1.0") (d (list (d (n "binrw") (r "^0.12.0") (d #t) (k 0)) (d (n "com_goldsrc_formats") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (f (quote ("tga"))) (k 0)))) (h "14qnisxvc8f9aln8clj53zh9s25czqankr8b5k51f404jqis8jrd")))

