(define-module (crates-io bs a3 bsa3-hash) #:use-module (crates-io))

(define-public crate-bsa3-hash-1.0.0 (c (n "bsa3-hash") (v "1.0.0") (d (list (d (n "criterion") (r "^0.2") (k 2)))) (h "0731lvw937l05610ahvc5x075sjbdgrkv9nypd2dyxykvyn55691")))

(define-public crate-bsa3-hash-1.0.1 (c (n "bsa3-hash") (v "1.0.1") (d (list (d (n "criterion") (r "^0.2") (k 2)))) (h "1zks8bqq57696a90my3hask1qr15pdjhddiq17wbh6mma8g2gcql")))

(define-public crate-bsa3-hash-1.0.2 (c (n "bsa3-hash") (v "1.0.2") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)))) (h "0aipxwgmgci70jhrcrsaw8igy3lvxmnfp9giaq5ab8bwhnpdfrqm")))

(define-public crate-bsa3-hash-2.0.0 (c (n "bsa3-hash") (v "2.0.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)))) (h "02wckvb07hmhnmmqrw9vpdv9123i7npxpr9xilk3p264vbhsw4hj") (y #t)))

(define-public crate-bsa3-hash-3.0.0 (c (n "bsa3-hash") (v "3.0.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)))) (h "1dx0iqhkxjaha1kprhh8q5vd0522s1k43wpl0z2zdfcx8z3hhim5")))

