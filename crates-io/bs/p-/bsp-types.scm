(define-module (crates-io bs p- bsp-types) #:use-module (crates-io))

(define-public crate-bsp-types-0.1.0 (c (n "bsp-types") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "lsp-types") (r "^0.93.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.7") (d #t) (k 0)))) (h "1w2b31y2pxcn62zrc8ixdxchicryipvdy6q1ahlfhqlqpj1w0m0n")))

(define-public crate-bsp-types-0.1.1 (c (n "bsp-types") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "lsp-types") (r "^0.93.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.7") (d #t) (k 0)))) (h "1vcj5ii33hyrga6kpxk8mrz7n0naa2c85740fdwa61ssq5ks8gis")))

(define-public crate-bsp-types-0.1.2 (c (n "bsp-types") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "lsp-types") (r "^0.93.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.7") (d #t) (k 0)))) (h "1dp737gjvavq1apwpjwi1lyg47r2iwl3p6d0v45pa5mznyfjdpz0")))

(define-public crate-bsp-types-0.1.3 (c (n "bsp-types") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "lsp-types") (r "^0.93.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.7") (d #t) (k 0)))) (h "05n9lkfy07cx5vfnxs1xcg01iqxkvyc3vv28nlj1prmc88yb6dlf")))

