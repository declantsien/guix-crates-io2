(define-module (crates-io bs du bsdump) #:use-module (crates-io))

(define-public crate-bsdump-0.1.0 (c (n "bsdump") (v "0.1.0") (d (list (d (n "binread") (r "^2.1.0") (d #t) (k 0)) (d (n "brotli") (r "^3.3.0") (d #t) (k 0)) (d (n "bzip2") (r "^0.4.2") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "068imlw2znk9ad4z0sag3w98vpy6q44idzm86zh6aaxnssg23qcz")))

