(define-module (crates-io bs cc bsccontract-diff) #:use-module (crates-io))

(define-public crate-bsccontract-diff-0.1.0 (c (n "bsccontract-diff") (v "0.1.0") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "similar") (r "^2.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "web3") (r "^0.18.0") (d #t) (k 0)))) (h "0lbpgxh2bwlllma8c8rcdgxxpw0la1iwnams0ddkl8s8nwp3qsz9")))

