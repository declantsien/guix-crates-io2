(define-module (crates-io bs ab bsabin) #:use-module (crates-io))

(define-public crate-bsabin-0.1.0 (c (n "bsabin") (v "0.1.0") (d (list (d (n "bsalib") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)))) (h "1m220mnvjfyk81azmc1ljmy5dvrdi7nh2ssz62751d6hamvmmjz3")))

(define-public crate-bsabin-0.2.0 (c (n "bsabin") (v "0.2.0") (d (list (d (n "bsa") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)))) (h "14a08fh5mqc48hiiqwiajgnda0fjhxlb2rppxhq0r0sr9k7gz805")))

(define-public crate-bsabin-0.2.1 (c (n "bsabin") (v "0.2.1") (d (list (d (n "bsa") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)))) (h "02wrfh83vx7cgsk8c2ifkb9ypyj0r12cmpkk25pr6ai5710y98g5")))

