(define-module (crates-io bs pl bspline) #:use-module (crates-io))

(define-public crate-bspline-0.1.0 (c (n "bspline") (v "0.1.0") (d (list (d (n "image") (r "^0.3.14") (d #t) (k 2)))) (h "0ypa4cpbmxirdkjxadb1k3haxbjv01zy1h2f4m6wllk3wr4pr67y")))

(define-public crate-bspline-0.1.1 (c (n "bspline") (v "0.1.1") (d (list (d (n "image") (r "^0.3.14") (d #t) (k 2)))) (h "1rfhwh4zy26mvhlhasw6xvkm2sp3vkqq6m6npbwm8jjx9ravk4gw")))

(define-public crate-bspline-0.1.2 (c (n "bspline") (v "0.1.2") (d (list (d (n "image") (r "^0.3.14") (d #t) (k 2)))) (h "00y3ga2d7lqdf4cgxwx657l39f7xs3m345hz439x6gd54xz0pgmd")))

(define-public crate-bspline-0.1.3 (c (n "bspline") (v "0.1.3") (d (list (d (n "image") (r "^0.3.14") (d #t) (k 2)))) (h "1ak4ir0n60a2cpja1q9dfhwf6b1id7yhy9pl3czmhwd0l6wyjxmy")))

(define-public crate-bspline-0.1.4 (c (n "bspline") (v "0.1.4") (d (list (d (n "image") (r "^0.3.14") (d #t) (k 2)))) (h "02sh8ww2p0c4vlgdrdmvfamdvf7pdldcjivmdgli7jidkddprwnp")))

(define-public crate-bspline-0.2.0 (c (n "bspline") (v "0.2.0") (d (list (d (n "image") (r "^0.3.14") (d #t) (k 2)))) (h "0w73xh1b47zvvgs6ik77pidzh0jv98743fhlr181j895b49i11x0")))

(define-public crate-bspline-0.2.1 (c (n "bspline") (v "0.2.1") (d (list (d (n "image") (r "^0.4.0") (d #t) (k 2)))) (h "0i7clq5c2m3kkrbvxpg9pc58nsd02lkcj9y738scgh1p2j001jyp")))

(define-public crate-bspline-0.2.2 (c (n "bspline") (v "0.2.2") (d (list (d (n "image") (r "^0.4.0") (d #t) (k 2)))) (h "0ndsbjlmjsr8r4vj7xksl2jqjdq9jsa1nzcxfxnqsylzpdpqpwbl")))

(define-public crate-bspline-0.2.3 (c (n "bspline") (v "0.2.3") (d (list (d (n "image") (r "^0.21.2") (d #t) (k 2)))) (h "1yhn0xyvib3k9myx1i6m2mhdhvrm2sl2m9929yinn9dqdzwd82h6")))

(define-public crate-bspline-1.0.0 (c (n "bspline") (v "1.0.0") (d (list (d (n "image") (r "^0.21.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0kldin4qp45jc98hksspdl1pwy2cfqahvc2f0q8q5rd1b132qqa1")))

(define-public crate-bspline-1.1.0 (c (n "bspline") (v "1.1.0") (d (list (d (n "image") (r "^0.22") (d #t) (k 2)) (d (n "nalgebra") (r "^0.30") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "trait-set") (r "^0.2") (d #t) (k 0)))) (h "117fp2n477xqqlyq0s2paj79yx6nbsf663qlbcxzclzymvm86x5g") (f (quote (("nalgebra-support" "nalgebra"))))))

