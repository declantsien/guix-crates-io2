(define-module (crates-io bs pl bspl) #:use-module (crates-io))

(define-public crate-bspl-0.9.0 (c (n "bspl") (v "0.9.0") (d (list (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.4") (d #t) (k 0)))) (h "1hr2fwkcq9ql374bfmlf673mfa1ln99ddp2f1in4wnc81i6i27cj")))

(define-public crate-bspl-0.9.1 (c (n "bspl") (v "0.9.1") (d (list (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.4") (d #t) (k 0)))) (h "1dxhr42k59nrgzdyhnvkixf95bw08n484b7j1kaybi87fqa0sl8l")))

(define-public crate-bspl-0.9.2 (c (n "bspl") (v "0.9.2") (d (list (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.4") (d #t) (k 0)))) (h "0kd8l3qy0lzisylfj9g40kfsz1zpgbyswyf26h8p50w70mmyrkwr")))

(define-public crate-bspl-0.9.3 (c (n "bspl") (v "0.9.3") (d (list (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.4") (d #t) (k 0)))) (h "0dpjva842kdj5i4z6hvrr4f6gasyagcxfpxi7vdhcyc5wy6v4prq")))

(define-public crate-bspl-1.0.0 (c (n "bspl") (v "1.0.0") (d (list (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^2.1.4") (d #t) (k 0)))) (h "1xn493cixvq7mh7n4m7fqsldqh0ad2386ad1x8msffvmvn4vb75f")))

