(define-module (crates-io bs on bson-oid-timestamp) #:use-module (crates-io))

(define-public crate-bson-oid-timestamp-0.1.0 (c (n "bson-oid-timestamp") (v "0.1.0") (d (list (d (n "bson") (r "^2") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (k 2)))) (h "16lgx0vh3w7vs00gd22qmwsp9q1k9092sfmm2k2cykssx00xj007")))

