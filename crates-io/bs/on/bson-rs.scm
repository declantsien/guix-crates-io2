(define-module (crates-io bs on bson-rs) #:use-module (crates-io))

(define-public crate-bson-rs-0.0.1 (c (n "bson-rs") (v "0.0.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "chrono") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0fpcwsmjvrsjk635vzzky5ji0ih2srbra4g575n4b8zrrh36fv1b")))

(define-public crate-bson-rs-0.0.2 (c (n "bson-rs") (v "0.0.2") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "chrono") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0j7rw35j38a4yrxh5hzrk1dfwh6pi3067fg7lxhjry1if5cdbhn7")))

(define-public crate-bson-rs-0.0.3 (c (n "bson-rs") (v "0.0.3") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "chrono") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0121lfzw4z6ibjbkgdfpvlgsi34y7k4rdarbjfnjig1c5fjdnsvj")))

(define-public crate-bson-rs-0.1.0 (c (n "bson-rs") (v "0.1.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "chrono") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0j40rklyv4wnpx73ms1580rmvz8s2f65chkrc9jgb81a94wzqylc") (y #t)))

