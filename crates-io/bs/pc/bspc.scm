(define-module (crates-io bs pc bspc) #:use-module (crates-io))

(define-public crate-bspc-0.4.0 (c (n "bspc") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unix_socket") (r "^0.5") (d #t) (k 0)))) (h "178bb4dk46rr20ihdmgkqw8q572a1f76plyxsprirm6862cg91iw")))

(define-public crate-bspc-0.4.1 (c (n "bspc") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unix_socket") (r "^0.5") (d #t) (k 0)))) (h "0sxipvxkc15rli7awfja8krn3wjah19i3d9shxav6lwx46rz7wrk")))

(define-public crate-bspc-0.4.2 (c (n "bspc") (v "0.4.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unix_socket") (r "^0.5") (d #t) (k 0)))) (h "0inlh4sh03qb76nh30y47xvy2lcpqn9z160jd7n8ycza04484az9")))

(define-public crate-bspc-0.4.3 (c (n "bspc") (v "0.4.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "unix_socket") (r "^0.5") (d #t) (k 0)))) (h "19hi8mpqq3zl4dj5b112sx6ppfcl214d3p7jck5p9y5bwsy6vk0l")))

