(define-module (crates-io bs pc bspc-rs) #:use-module (crates-io))

(define-public crate-bspc-rs-0.1.0 (c (n "bspc-rs") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1vz46n2zlhzld53w2kvydyzf2pmpiybhsan25sccd377g1hzk7r7")))

(define-public crate-bspc-rs-0.1.1 (c (n "bspc-rs") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)))) (h "1an3srxn15l1ndhspnfgg0m4i837138mm8gl61mf9dprzfvc1lds")))

