(define-module (crates-io bs c- bsc-cli) #:use-module (crates-io))

(define-public crate-bsc-cli-0.2.0 (c (n "bsc-cli") (v "0.2.0") (d (list (d (n "bsc") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive" "env" "wrap_help"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "simple-eyre") (r "^0.3.1") (d #t) (k 0)))) (h "0mxvr3w9vz1kgf2dl98mlnqiz3fvmgmn7nq4l7d3jrhlrqs7vssg")))

