(define-module (crates-io bs cs bscscan_wrapper) #:use-module (crates-io))

(define-public crate-bscscan_wrapper-0.1.0 (c (n "bscscan_wrapper") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "14srma25yiacyajcbn7ylzp02m7zr3h6g837ahagxh5sws8kjjs6") (y #t)))

(define-public crate-bscscan_wrapper-0.1.1 (c (n "bscscan_wrapper") (v "0.1.1") (d (list (d (n "ethabi") (r "^13.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "111xjby7zjpbchgm0qp5nz33dhmy1h4wk01ll4fz2bn8h4lvyg1y") (y #t)))

(define-public crate-bscscan_wrapper-0.1.2 (c (n "bscscan_wrapper") (v "0.1.2") (d (list (d (n "actix-rt") (r "^1.1.1") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.10.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "web3") (r "^0.15.0") (d #t) (k 0)))) (h "1rscfxqvj3b1p6ck75swrma9d55jrkvw57zl9fcwv2780nyzaq71")))

