(define-module (crates-io bs ut bsutils) #:use-module (crates-io))

(define-public crate-bsutils-0.1.0 (c (n "bsutils") (v "0.1.0") (h "1ifjrz8ck6kdbf4lffyk3qhyskm12abdsg61nb2pszv0psbbc93q")))

(define-public crate-bsutils-0.1.1 (c (n "bsutils") (v "0.1.1") (h "1wkav98xb1cfmqikjbigqxhxlxx5r450wcqkw19jwqgrahw1h30i")))

(define-public crate-bsutils-0.1.2 (c (n "bsutils") (v "0.1.2") (h "1bck4z37yd8qcnnypkzd3hl6hvq5q0kjv5ks2v3w22skiynpy5nq")))

