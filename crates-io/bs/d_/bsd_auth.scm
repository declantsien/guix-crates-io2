(define-module (crates-io bs d_ bsd_auth) #:use-module (crates-io))

(define-public crate-bsd_auth-0.0.1 (c (n "bsd_auth") (v "0.0.1") (d (list (d (n "bsd_auth-sys") (r "^0.0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "13jm9rwpa2hh90dws90j9xscgpqi08x7r43pwm8fvp09l208m6hm")))

(define-public crate-bsd_auth-0.0.2 (c (n "bsd_auth") (v "0.0.2") (d (list (d (n "bsd_auth-sys") (r "^0.0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "0r58k1n73l06lc2hhg0k5lkay1125jf4ad00fmm5w5cdl482qz88")))

(define-public crate-bsd_auth-0.0.3 (c (n "bsd_auth") (v "0.0.3") (d (list (d (n "bsd_auth-sys") (r "^0.0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "1mzb47y5wl3l54njh8bbll5fri8jlf52fmr0l41g20kwhzlakxq9")))

(define-public crate-bsd_auth-0.1.0 (c (n "bsd_auth") (v "0.1.0") (d (list (d (n "bsd_auth-sys") (r "^0.0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "04bk9zlqgincx1hbd25hbwanzhgabafvx804q46fpjz6hjrzq862")))

(define-public crate-bsd_auth-0.2.0 (c (n "bsd_auth") (v "0.2.0") (d (list (d (n "bsd_auth-sys") (r "^0.0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "05hvc948zzf633yf0iwgc1i702kpv80pasg98b5g3srbwvkrpy7w")))

(define-public crate-bsd_auth-0.2.1 (c (n "bsd_auth") (v "0.2.1") (d (list (d (n "bsd_auth-sys") (r "^0.0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "152pwfvf74ar71zzd3fwlm4zqk8lbbvhdigazpd4j3zzm1yywcm5")))

(define-public crate-bsd_auth-0.2.2 (c (n "bsd_auth") (v "0.2.2") (d (list (d (n "bsd_auth-sys") (r "^0.0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "1p1kyzp6jk1qwd6w2gdfihb6y2m420jlvm6h566nwmag4qkrihb1")))

(define-public crate-bsd_auth-0.3.0 (c (n "bsd_auth") (v "0.3.0") (d (list (d (n "bsd_auth-sys") (r "^0.0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "1iw68ik85gy6vf1lk3rqw6qqn48krm9sram3nnry7l3zivairwwz")))

(define-public crate-bsd_auth-0.3.1 (c (n "bsd_auth") (v "0.3.1") (d (list (d (n "bsd_auth-sys") (r "^0.0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "06bkwcpq41dbadpvr05p6p7f8xpyrvg1s7yx296f7jkxjhf6nfji")))

