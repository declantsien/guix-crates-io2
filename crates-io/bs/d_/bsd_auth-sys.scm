(define-module (crates-io bs d_ bsd_auth-sys) #:use-module (crates-io))

(define-public crate-bsd_auth-sys-0.0.1 (c (n "bsd_auth-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)))) (h "12xdizh89gsx4036am0zh9iykrq7pcljxqi99djg9gwjv7ff28i1")))

(define-public crate-bsd_auth-sys-0.0.2 (c (n "bsd_auth-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)))) (h "1wyqwqlqw721ixjdi0l3j0g0acwxhh05h19r8ziws51jahad2x5i")))

(define-public crate-bsd_auth-sys-0.0.3 (c (n "bsd_auth-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)))) (h "1jiz7643a97zwkgzbwd4xqj9nc9ycwrr3hc8ak03kny0kc84vkky")))

(define-public crate-bsd_auth-sys-0.0.4 (c (n "bsd_auth-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)))) (h "0chskldlw42zss3zzf777drbx9zj65qpfkjhh07fwp9g3y3xvgc2")))

(define-public crate-bsd_auth-sys-0.0.5 (c (n "bsd_auth-sys") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)))) (h "1i7jii2f9cyw0mylpzn74qdnj4vrfjjwbyppwzzb38brs0rb933r")))

(define-public crate-bsd_auth-sys-0.0.6 (c (n "bsd_auth-sys") (v "0.0.6") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "0q02p3b6k94nwcna43wsp7igfiabpjvdk0ni05ly36hj7n84h9wh")))

(define-public crate-bsd_auth-sys-0.0.7 (c (n "bsd_auth-sys") (v "0.0.7") (d (list (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "1xwvggm55zww35is1vl10jlad0a8ckm2q5za3dxkw4zc9kziy2y9")))

