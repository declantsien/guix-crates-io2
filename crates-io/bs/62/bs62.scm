(define-module (crates-io bs #{62}# bs62) #:use-module (crates-io))

(define-public crate-bs62-0.1.0 (c (n "bs62") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0ny8y84l4w8v31d66qkrxdbq4bph9ry8ki9zdhpb37qhll152d0v")))

(define-public crate-bs62-0.1.1 (c (n "bs62") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0kv0fxw56g15z9wn5syv861i91i897n1w3aisn5jwic1c3xwfcwc")))

(define-public crate-bs62-0.1.2 (c (n "bs62") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "19kksi1aga8hx43y74kphlcck1ck1sdqlads3bhqy3qjrqgcymk4")))

(define-public crate-bs62-0.1.3 (c (n "bs62") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0pbsr16rdr2vzfy4mcf8l9fbmpl59p3cbykmz6f5ng4d2rj2a62j")))

(define-public crate-bs62-0.1.4 (c (n "bs62") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1g9ilny6mviam2l0l8fr2pp4nfpsz15bwjdsdjp582a3i9x0zpxg")))

