(define-module (crates-io bs ha bsharp_fset) #:use-module (crates-io))

(define-public crate-bsharp_fset-0.1.0 (c (n "bsharp_fset") (v "0.1.0") (d (list (d (n "parser") (r "^0.1.0") (d #t) (k 0) (p "bsharp_parser")) (d (n "tokens") (r "^0.1.0") (d #t) (k 0) (p "bsharp_tokens")))) (h "09jzxn3qk68789p4v1r7s7a3fjj5mlycfxd56am0w4wyzc1iznk1")))

(define-public crate-bsharp_fset-0.2.1 (c (n "bsharp_fset") (v "0.2.1") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "parser") (r "^0.2.0") (d #t) (k 0) (p "bsharp_parser")) (d (n "tokens") (r "^0.2.0") (d #t) (k 0) (p "bsharp_tokens")))) (h "1mgjphnsjfq1s0b4a4ngjcdmz9bi7varppw5y0sfg04mgvz4gi40")))

(define-public crate-bsharp_fset-0.2.2 (c (n "bsharp_fset") (v "0.2.2") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "parser") (r "^0.2.2") (d #t) (k 0) (p "bsharp_parser")) (d (n "tokens") (r "^0.2.2") (d #t) (k 0) (p "bsharp_tokens")))) (h "18cdsvk10nskdj8g5zghm2a89xgxrxxqm9jbdwvh08cs0y5a46cj")))

(define-public crate-bsharp_fset-0.2.3 (c (n "bsharp_fset") (v "0.2.3") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "parser") (r "^0.2.2") (d #t) (k 0) (p "bsharp_parser")) (d (n "tokens") (r "^0.2.2") (d #t) (k 0) (p "bsharp_tokens")))) (h "0jqsr2mzd9v7pv1y7fwkshxzak12fsbwsh0rbnbv3xkv76049dqm")))

