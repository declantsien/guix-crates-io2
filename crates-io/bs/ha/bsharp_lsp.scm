(define-module (crates-io bs ha bsharp_lsp) #:use-module (crates-io))

(define-public crate-bsharp_lsp-0.2.2 (c (n "bsharp_lsp") (v "0.2.2") (d (list (d (n "fset") (r "^0.2.2") (d #t) (k 0) (p "bsharp_fset")) (d (n "ir") (r "^0.2.2") (d #t) (k 0) (p "bsharp_ir")) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-lsp") (r "^0.20.0") (d #t) (k 0)))) (h "0n5v5rzbscp1db9akr5i1q7323knmdi9wsxzylxcbs49anly5qg1")))

(define-public crate-bsharp_lsp-0.2.3 (c (n "bsharp_lsp") (v "0.2.3") (d (list (d (n "fset") (r "^0.2.2") (d #t) (k 0) (p "bsharp_fset")) (d (n "ir") (r "^0.2.2") (d #t) (k 0) (p "bsharp_ir")) (d (n "tokens") (r "^0.2.2") (d #t) (k 0) (p "bsharp_tokens")) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-lsp") (r "^0.20.0") (d #t) (k 0)))) (h "0g2iizzf2dgjlv3jcpa7mild9ymgbxkdirwna6wabah02pxkxlh4")))

