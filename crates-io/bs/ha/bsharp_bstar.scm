(define-module (crates-io bs ha bsharp_bstar) #:use-module (crates-io))

(define-public crate-bsharp_bstar-0.2.2 (c (n "bsharp_bstar") (v "0.2.2") (d (list (d (n "ir") (r "^0.2.2") (d #t) (k 0) (p "bsharp_ir")))) (h "0c6i084bc0aahi40c43jplrjrwcg9izgmkwq6rs1534sax4ph0xd")))

(define-public crate-bsharp_bstar-0.2.3 (c (n "bsharp_bstar") (v "0.2.3") (d (list (d (n "ir") (r "^0.2.2") (d #t) (k 0) (p "bsharp_ir")))) (h "1mdwn0203m2klhljz0jyiafc9xsia0yrblp092l5r522hn8hmxd8")))

