(define-module (crates-io bs ha bsharp_tokens) #:use-module (crates-io))

(define-public crate-bsharp_tokens-0.1.0 (c (n "bsharp_tokens") (v "0.1.0") (h "0mh5ilp8hnkl78vgcifcws5isfcq6kh62lcw5dzsd50sdzr1h87a")))

(define-public crate-bsharp_tokens-0.2.1 (c (n "bsharp_tokens") (v "0.2.1") (h "0k9hdqmng73zdmjwncqq4swcffv8311p80j34qpbg7zdpmdjkx9g")))

(define-public crate-bsharp_tokens-0.2.2 (c (n "bsharp_tokens") (v "0.2.2") (h "1js02y8hmh555yfpx88jipfn8lzw9zp9w0hi1gmrp4s74h7rkk9n")))

(define-public crate-bsharp_tokens-0.2.3 (c (n "bsharp_tokens") (v "0.2.3") (h "13aj01rc4kfbqma51v6hnw4xsz61bpgxjfid3qb0i47gvx6d5xad")))

