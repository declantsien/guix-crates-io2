(define-module (crates-io bs ha bsharp_parser) #:use-module (crates-io))

(define-public crate-bsharp_parser-0.1.0 (c (n "bsharp_parser") (v "0.1.0") (d (list (d (n "tokens") (r "^0.1.0") (d #t) (k 0) (p "bsharp_tokens")))) (h "0pj6s0kb2ny17ph8zm19wpjzpfqh49fi6r1vrrxx0xgn9jmlkl1w")))

(define-public crate-bsharp_parser-0.2.1 (c (n "bsharp_parser") (v "0.2.1") (d (list (d (n "tokens") (r "^0.2.0") (d #t) (k 0) (p "bsharp_tokens")))) (h "1mq66yrrvy4dwm1zrvbxjnn8z5hzjmm71nwff4f4k1zbqa5blycc")))

(define-public crate-bsharp_parser-0.2.2 (c (n "bsharp_parser") (v "0.2.2") (d (list (d (n "tokens") (r "^0.2.2") (d #t) (k 0) (p "bsharp_tokens")))) (h "053sr4nbswfv5z1bfq9zc36w3y53849pq38md94j71by8bhxm7z5")))

(define-public crate-bsharp_parser-0.2.3 (c (n "bsharp_parser") (v "0.2.3") (d (list (d (n "tokens") (r "^0.2.2") (d #t) (k 0) (p "bsharp_tokens")))) (h "1bwj35pcl98aq0miahqprlhyv6cnxpii9shgwdz0q03aiclf73a5")))

