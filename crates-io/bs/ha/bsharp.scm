(define-module (crates-io bs ha bsharp) #:use-module (crates-io))

(define-public crate-bsharp-0.1.0 (c (n "bsharp") (v "0.1.0") (d (list (d (n "fset") (r "^0.1.0") (d #t) (k 0) (p "bsharp_fset")) (d (n "ir") (r "^0.1.0") (d #t) (k 0) (p "bsharp_ir")))) (h "1fj6zzcw49w1crnk2b2gz94sfllhqdmn4jbfxylfmqfq9wccij8w")))

(define-public crate-bsharp-0.2.1 (c (n "bsharp") (v "0.2.1") (d (list (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fset") (r "^0.2.0") (d #t) (k 0) (p "bsharp_fset")) (d (n "interp") (r "^0.2.0") (d #t) (k 0) (p "bsharp_interp")) (d (n "ir") (r "^0.2.0") (d #t) (k 0) (p "bsharp_ir")))) (h "0fgzki40cq4bgrpih09m6qb6ajf2zy192ngyips1z9l5sk8j02kx")))

(define-public crate-bsharp-0.2.2 (c (n "bsharp") (v "0.2.2") (d (list (d (n "bstar") (r "^0.2.2") (d #t) (k 0) (p "bsharp_bstar")) (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fset") (r "^0.2.2") (d #t) (k 0) (p "bsharp_fset")) (d (n "interp") (r "^0.2.2") (d #t) (k 0) (p "bsharp_interp")) (d (n "ir") (r "^0.2.2") (d #t) (k 0) (p "bsharp_ir")))) (h "18swwrb6wj3w3iak70rxjgvy18a8cc47lb6c3lmxgsjpszi6rc4n")))

(define-public crate-bsharp-0.2.3 (c (n "bsharp") (v "0.2.3") (d (list (d (n "bstar") (r "^0.2.2") (d #t) (k 0) (p "bsharp_bstar")) (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fset") (r "^0.2.2") (d #t) (k 0) (p "bsharp_fset")) (d (n "interp") (r "^0.2.2") (d #t) (k 0) (p "bsharp_interp")) (d (n "ir") (r "^0.2.2") (d #t) (k 0) (p "bsharp_ir")))) (h "18zblbll6m34hykxldhw271dmmlfdiycgvzsy6x9rrqr8wm0calb")))

