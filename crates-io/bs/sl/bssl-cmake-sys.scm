(define-module (crates-io bs sl bssl-cmake-sys) #:use-module (crates-io))

(define-public crate-bssl-cmake-sys-0.1.2403141 (c (n "bssl-cmake-sys") (v "0.1.2403141") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0k002fwqc1qhigdxbdh58wmn31ajjkpxzvpydkcynbi9nj73jny3") (l "bssl")))

(define-public crate-bssl-cmake-sys-0.1.2403142 (c (n "bssl-cmake-sys") (v "0.1.2403142") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "06drcs7dlmh3bi32xvxyag6842jqzlidba24cjnhxdwysirbl1kl") (l "bssl")))

(define-public crate-bssl-cmake-sys-0.1.2403180 (c (n "bssl-cmake-sys") (v "0.1.2403180") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1f2g9n35h0rmqjygqydjjwfxf5p0f78kg6xww1gnai72gnbkzv37") (l "bssl")))

(define-public crate-bssl-cmake-sys-0.1.2403260 (c (n "bssl-cmake-sys") (v "0.1.2403260") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0sgs6fjlm67n5xxnmpf89jqfq1sr47ghcycgn0ym3wzjs34av6pb") (l "bssl")))

(define-public crate-bssl-cmake-sys-0.1.2404010 (c (n "bssl-cmake-sys") (v "0.1.2404010") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0ad9drhavkny7ad00mjnl4y4l33kx63v1imc97fls88sp8cvczn1") (l "bssl")))

(define-public crate-bssl-cmake-sys-0.1.2404150 (c (n "bssl-cmake-sys") (v "0.1.2404150") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1dbkwidy8rhsgbv1mw2l7vhq8qph37gra37570jg5d6sx9dam9lj") (l "bssl")))

(define-public crate-bssl-cmake-sys-0.1.2404220 (c (n "bssl-cmake-sys") (v "0.1.2404220") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1awass99lhm3wlvh5rpj6kl5bzr2198q79lphfkfn0hixkr50ins") (l "bssl")))

(define-public crate-bssl-cmake-sys-0.1.2405060 (c (n "bssl-cmake-sys") (v "0.1.2405060") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1d8xvdlc6549c2svdsi7qmnjg8212y34fikbvr9hq2iq4s9j1bb8") (l "bssl")))

(define-public crate-bssl-cmake-sys-0.1.2405130 (c (n "bssl-cmake-sys") (v "0.1.2405130") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1zjw7nx956ic6rinljdi1k8gykadp41i30gxw7fxdm666vs30dp7") (l "bssl")))

(define-public crate-bssl-cmake-sys-0.1.2405200 (c (n "bssl-cmake-sys") (v "0.1.2405200") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "08daz24l1v963zi0d7b10j3vp9mjw5lgczslrdkrchn5pg5adabr") (l "bssl")))

(define-public crate-bssl-cmake-sys-0.1.2405270 (c (n "bssl-cmake-sys") (v "0.1.2405270") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "10h1zym1xs6r8m2ahlbp31vqssdnwcsj34xih9bwqlj2fbkjvjgg") (l "bssl")))

