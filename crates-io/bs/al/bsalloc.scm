(define-module (crates-io bs al bsalloc) #:use-module (crates-io))

(define-public crate-bsalloc-0.1.0 (c (n "bsalloc") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "mmap-alloc") (r "^0.1.0") (d #t) (k 0)))) (h "1zsnjv77rmkv561n48y9b0l7hg2a7c8jx4kkr0vyq4d1z4knivsf")))

