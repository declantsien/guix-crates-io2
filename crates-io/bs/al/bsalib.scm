(define-module (crates-io bs al bsalib) #:use-module (crates-io))

(define-public crate-bsalib-0.1.0 (c (n "bsalib") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "enumflags2") (r "^0.7.1") (d #t) (k 0)) (d (n "libflate") (r "^1.1.0") (d #t) (k 0)) (d (n "lz4") (r "^1.23.2") (d #t) (k 0)) (d (n "macro-attr-2018") (r "^1.1.1") (d #t) (k 0)) (d (n "newtype-derive-2018") (r "^0.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)))) (h "16vvhv0988h15qps0sllamhi92bbckm1lbcgm9vq1yv0x61giyca")))

