(define-module (crates-io bs at bsatoollib) #:use-module (crates-io))

(define-public crate-bsatoollib-0.1.0 (c (n "bsatoollib") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0n7cakd02i38n7smiihv3vzl26p2bbfiv2girhqfa25jx0x1qmcv")))

(define-public crate-bsatoollib-0.2.0 (c (n "bsatoollib") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0ddcf0mvlnxpzbhw9ys3fybnhlfcwlfymsczdphxdm0i3cfdw770")))

(define-public crate-bsatoollib-0.2.1 (c (n "bsatoollib") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1c8jrmyrbj04zal5zch75wlfk1d5m837qivmzdl60a9j7vchqgyh")))

