(define-module (crates-io bs #{58}# bs58) #:use-module (crates-io))

(define-public crate-bs58-0.1.2 (c (n "bs58") (v "0.1.2") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 2)))) (h "0wx7r79gfb4inhxr5vnk88lx32yynh6l8dk3mn9mah7afblj06rx")))

(define-public crate-bs58-0.1.3 (c (n "bs58") (v "0.1.3") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 2)))) (h "03jrx1ycgmp5jrxc71lqhzk85dyi581l355l93y6bh7pf0hsmwq9")))

(define-public crate-bs58-0.2.0 (c (n "bs58") (v "0.2.0") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 2)))) (h "17rdd54m33sqwzzhx72b29ldzc6r14jzlwdsplzl7mwq2n2s8vif")))

(define-public crate-bs58-0.2.1 (c (n "bs58") (v "0.2.1") (d (list (d (n "base58") (r "^0.1.0") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 2)))) (h "0iw088n0adc3b35x30ycz20fxhsnj7srchxyjfdnaydxd8bspffx")))

(define-public crate-bs58-0.2.2 (c (n "bs58") (v "0.2.2") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "base58") (r "^0.1.0") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 2)) (d (n "sha2") (r "^0.7.1") (o #t) (d #t) (k 0)))) (h "0hks9wfqgvd4v68kp1m0m9paspab9dkdi1473249kap7k3xrrrqd") (f (quote (("default") ("check" "sha2"))))))

(define-public crate-bs58-0.2.3 (c (n "bs58") (v "0.2.3") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "base58") (r "^0.1.0") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (o #t) (d #t) (k 0)))) (h "0qczyldfkkwf2vx8asmy2ll1r8r3ahzz1jg9hxsmili3a9b7g1p0") (f (quote (("default") ("check" "sha2"))))))

(define-public crate-bs58-0.2.4 (c (n "bs58") (v "0.2.4") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "base58") (r "^0.1.0") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (o #t) (d #t) (k 0)))) (h "1pd2ibrjfw6fw288xkdm9h9gfxp5pjxzwmz0l8yz8kgzcanl95hd") (f (quote (("default") ("check" "sha2"))))))

(define-public crate-bs58-0.2.5 (c (n "bs58") (v "0.2.5") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "base58") (r "^0.1.0") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (o #t) (d #t) (k 0)))) (h "125i962x0m0ggdif6ds51wfif2lypiicy469dj5j2l6rm6xycpn9") (f (quote (("default") ("check" "sha2"))))))

(define-public crate-bs58-0.3.0 (c (n "bs58") (v "0.3.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "base58") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (o #t) (k 0)))) (h "1bkd77ic7ybm5mhy5hyv4j0pxiyzvykl8gmfxnwsd7rzd8jwsw5i") (f (quote (("std" "alloc") ("default" "std") ("check" "sha2") ("alloc"))))))

(define-public crate-bs58-0.3.1 (c (n "bs58") (v "0.3.1") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "base58") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (o #t) (k 0)))) (h "1yw3q9fr44ya9k1kzljyq0awn3i2xyl18q7s5zh23qgri7a9qvj7") (f (quote (("std" "alloc") ("default" "std") ("check" "sha2") ("alloc"))))))

(define-public crate-bs58-0.4.0 (c (n "bs58") (v "0.4.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "base58") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 2)) (d (n "sha2") (r "^0.9.0") (o #t) (k 0)))) (h "1lr3vwzhhyica4y7rbkf26vr1h7vpjb1m6rml8zcqgw81c2y07vp") (f (quote (("std" "alloc") ("default" "std") ("check" "sha2") ("alloc"))))))

(define-public crate-bs58-0.5.0 (c (n "bs58") (v "0.5.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "base58") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 2)) (d (n "sha2") (r "^0.10") (o #t) (k 0)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)) (d (n "tinyvec") (r "^1.6.0") (f (quote ("grab_spare_slice"))) (o #t) (k 0)) (d (n "tinyvec") (r "^1.6.0") (f (quote ("rustc_1_55"))) (d #t) (k 2)))) (h "15kqgld75z03srq6nzsdgraakhvap5avgw364h352x0z6hv3ydgm") (f (quote (("default" "std") ("check" "sha2") ("cb58" "sha2")))) (s 2) (e (quote (("std" "alloc" "tinyvec?/std") ("alloc" "tinyvec?/alloc"))))))

(define-public crate-bs58-0.5.1 (c (n "bs58") (v "0.5.1") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)) (d (n "base58") (r "^0.1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rust-base58") (r "^0.0.4") (d #t) (k 2)) (d (n "sha2") (r "^0.10") (o #t) (k 0)) (d (n "smallvec") (r "^1") (o #t) (d #t) (k 0)) (d (n "tinyvec") (r "^1.6.0") (f (quote ("grab_spare_slice"))) (o #t) (k 0)) (d (n "tinyvec") (r "^1.6.0") (f (quote ("rustc_1_55"))) (d #t) (k 2)))) (h "1x3v51n5n2s3l0rgrsn142akdf331n2qsa75pscw71fi848vm25z") (f (quote (("default" "std") ("check" "sha2") ("cb58" "sha2")))) (s 2) (e (quote (("std" "alloc" "tinyvec?/std") ("alloc" "tinyvec?/alloc"))))))

