(define-module (crates-io bs #{58}# bs58-cli) #:use-module (crates-io))

(define-public crate-bs58-cli-0.1.0 (c (n "bs58-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "bs58") (r "^0.3.0") (d #t) (k 0)) (d (n "paw") (r "^1.0.0") (k 0)) (d (n "structopt") (r "^0.3.0") (f (quote ("paw" "color"))) (k 0)))) (h "1kzzx92ypz80q1q4kzc6zvkaghpccppf5kc5rxv0d8hdj3ap7i0r")))

(define-public crate-bs58-cli-0.1.1 (c (n "bs58-cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.26") (f (quote ("std"))) (k 0)) (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.0") (f (quote ("color"))) (k 0)))) (h "0bkln5pj6r48avnvhp7b4d6ipzj1wzbf20vgkx1gn6h127cy2v1n")))

(define-public crate-bs58-cli-0.1.2 (c (n "bs58-cli") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("std"))) (k 0)) (d (n "bs58") (r "^0.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("std" "derive" "color" "wrap_help" "error-context" "cargo" "suggestions" "usage"))) (k 0)))) (h "01qam6ry7ar4cqk1ya5sld28kk3rr4p4rdbfydh3f45bv51ydlv6")))

