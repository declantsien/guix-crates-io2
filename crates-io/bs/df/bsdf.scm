(define-module (crates-io bs df bsdf) #:use-module (crates-io))

(define-public crate-bsdf-0.1.0 (c (n "bsdf") (v "0.1.0") (d (list (d (n "fastrand") (r "^2.0.0") (d #t) (k 2)) (d (n "glam") (r "^0.24.1") (d #t) (k 0)))) (h "0vj7kfdd1b51vyxsy5qkj4zn6wlsq33w88sdyn1x6di2hl2nmhil") (f (quote (("rough-glass" "ggx") ("mix") ("lambert") ("ggx") ("emissive") ("disney" "ggx") ("default" "disney" "conductive" "emissive" "lambert" "mix" "rough-glass") ("conductive" "ggx")))) (y #t)))

(define-public crate-bsdf-0.1.1 (c (n "bsdf") (v "0.1.1") (d (list (d (n "fastrand") (r "^2.0.0") (d #t) (k 2)) (d (n "glam") (r "^0.24.1") (d #t) (k 0)) (d (n "png") (r "^0.17.10") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)))) (h "0n9rmd5pcsbzafcx11ns0fd3a8lj0si8d2qgax3jx1fs5y15mxw0") (f (quote (("rough-glass" "ggx") ("mix") ("lambert") ("ggx") ("emissive") ("disney" "ggx") ("default" "disney" "conductive" "emissive" "lambert" "mix" "rough-glass") ("conductive" "ggx"))))))

