(define-module (crates-io bs d- bsd-kvm-sys) #:use-module (crates-io))

(define-public crate-bsd-kvm-sys-0.1.0 (c (n "bsd-kvm-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "125kdrxdja3v966qlnh6rchcjyvrs13c0pg27chi764iyc3wzagw")))

(define-public crate-bsd-kvm-sys-0.2.0 (c (n "bsd-kvm-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0cn90d0kkfcs36v3sq3lpckyy0pdpdq0m7ihjlancripdn98yh35")))

