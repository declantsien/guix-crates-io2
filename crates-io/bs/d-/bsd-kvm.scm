(define-module (crates-io bs d- bsd-kvm) #:use-module (crates-io))

(define-public crate-bsd-kvm-0.1.0 (c (n "bsd-kvm") (v "0.1.0") (d (list (d (n "bsd-kvm-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08raimisskq2fbvbvr3l19wh8ygdclklbzfmgf1zdcb5l07lqcw8")))

(define-public crate-bsd-kvm-0.1.1 (c (n "bsd-kvm") (v "0.1.1") (d (list (d (n "bsd-kvm-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rm9cricb0qm7ppp0rvwpvv2ilh5igdcs1ssax9r4qgr5bnmd7cb")))

(define-public crate-bsd-kvm-0.1.2 (c (n "bsd-kvm") (v "0.1.2") (d (list (d (n "bsd-kvm-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1dqsbri9x6wk84gs76mj6cnrxhan9nn6jc5vjj1b256i210k8472")))

(define-public crate-bsd-kvm-0.1.3 (c (n "bsd-kvm") (v "0.1.3") (d (list (d (n "bsd-kvm-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "042m2hx8kvf02p8jp4liy697a746qh5hpl17mj11ll7k3g7bd0nk")))

(define-public crate-bsd-kvm-0.1.4 (c (n "bsd-kvm") (v "0.1.4") (d (list (d (n "bsd-kvm-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1nzdmxplqg6mwsxdvicc6m7zsphcppkd8z57akb5zypywb75lm7z")))

(define-public crate-bsd-kvm-0.1.5 (c (n "bsd-kvm") (v "0.1.5") (d (list (d (n "bsd-kvm-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.149") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gncwfwlx6mq47qc1siwaiqlsaccy7vsc1v39ybs4xvvn4lfpd4l")))

