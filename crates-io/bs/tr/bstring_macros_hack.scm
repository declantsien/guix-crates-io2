(define-module (crates-io bs tr bstring_macros_hack) #:use-module (crates-io))

(define-public crate-bstring_macros_hack-0.1.0 (c (n "bstring_macros_hack") (v "0.1.0") (d (list (d (n "bstring") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1ijzwmywvyk34hpjr0f3drc3wywarslm4iwzghspgnga39f1ssdw")))

