(define-module (crates-io bs tr bstring_macros) #:use-module (crates-io))

(define-public crate-bstring_macros-0.1.0 (c (n "bstring_macros") (v "0.1.0") (d (list (d (n "bstring") (r "^0.1") (d #t) (k 0)) (d (n "bstring_macros_hack") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.3") (d #t) (k 0)))) (h "136nj9h891qy7ydhcm19hylydk76hba40fnq54xzk87g87lzq4cv")))

