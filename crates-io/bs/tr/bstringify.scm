(define-module (crates-io bs tr bstringify) #:use-module (crates-io))

(define-public crate-bstringify-0.1.0 (c (n "bstringify") (v "0.1.0") (h "1dbsv0z4dynk41iab51hy1rhjpfnnll02h5n2gi0a6khlxfq64h2") (f (quote (("nightly") ("default"))))))

(define-public crate-bstringify-0.1.1 (c (n "bstringify") (v "0.1.1") (h "1mfsdq7q7jvhl9f4cyxf8f8s2xwwxh9c7jk3dvkdsh4yxwk0p4ry") (f (quote (("nightly") ("default"))))))

(define-public crate-bstringify-0.1.2 (c (n "bstringify") (v "0.1.2") (h "0r0z0yka5473yc5h605yw1kmzxd5f1s6p7jwhbi56aganiiraxmx") (f (quote (("nightly") ("default"))))))

