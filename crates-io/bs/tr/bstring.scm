(define-module (crates-io bs tr bstring) #:use-module (crates-io))

(define-public crate-bstring-0.1.0 (c (n "bstring") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "ref_slice") (r "^1.1") (d #t) (k 0)))) (h "1dl2q5r4vgqdincfgjgn2wlppj3d3plwxwc53hvx0j2nzzv5bnzy")))

