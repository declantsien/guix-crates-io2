(define-module (crates-io bs or bsor) #:use-module (crates-io))

(define-public crate-bsor-0.1.0 (c (n "bsor") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1wb3srdnd9ld1pmcwqzk2sc05nxyqb9d3y4q8d7f33rmahgfxhn2") (y #t)))

(define-public crate-bsor-0.1.1 (c (n "bsor") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1a8pk0jdr9mcx168hf6d52f5kzvq4qn7xppa4x6hm77x0l1avmjl")))

(define-public crate-bsor-0.2.0 (c (n "bsor") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1vdk7sgf323xrcm8hh4jwz7jnq993kgxazpibkjxiy6qs62md8gb")))

(define-public crate-bsor-0.2.1 (c (n "bsor") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "05y25cm1i2vamzpd7yiyqkhbk4s69hhz1482h2g4y7l9y0ppfkb3")))

(define-public crate-bsor-0.3.0 (c (n "bsor") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0xw7qwlg9vvwpgimjks7www9fbfkb6zfsf2yk6lsdnbzp1333253")))

