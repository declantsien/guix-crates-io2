(define-module (crates-io bs #{17}# bs1770) #:use-module (crates-io))

(define-public crate-bs1770-1.0.0 (c (n "bs1770") (v "1.0.0") (d (list (d (n "claxon") (r "^0.4.3") (d #t) (k 2)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.76") (d #t) (k 2)))) (h "0nvp2qxijmnii1fy35hg41bra07rrmvbhb7zdwj6dw2z40fccck3")))

