(define-module (crates-io bs co bscore-lib) #:use-module (crates-io))

(define-public crate-bscore-lib-0.1.0 (c (n "bscore-lib") (v "0.1.0") (d (list (d (n "bscore") (r "^0.1") (d #t) (k 0)))) (h "0x0niwmhmwsnkn4n5ymh9x8psv4r6na2xqyvjm4wim7i62xnd6zs") (y #t)))

(define-public crate-bscore-lib-1.0.0 (c (n "bscore-lib") (v "1.0.0") (d (list (d (n "bscore") (r "^1.0") (d #t) (k 0)))) (h "1516hxf06lfgv5hpqkm1l4s04vizd8gd6c2bskm69x39dslab33v") (y #t)))

(define-public crate-bscore-lib-1.0.1 (c (n "bscore-lib") (v "1.0.1") (d (list (d (n "bscore") (r "^1.0") (d #t) (k 0)))) (h "1aq4sjwnsxdimg275zish3iwhv1194fdmcrzrwbg2c524h5sd15r") (y #t)))

(define-public crate-bscore-lib-1.0.2 (c (n "bscore-lib") (v "1.0.2") (d (list (d (n "bscore") (r "^1.0") (d #t) (k 0)))) (h "0i3x39gg6yphvlbkahm6pcyck63r2a4r3c651l29pnziyi2i0rld")))

(define-public crate-bscore-lib-1.1.1 (c (n "bscore-lib") (v "1.1.1") (d (list (d (n "bscore") (r "^1.1") (d #t) (k 0)))) (h "1ygfyi5hpkmalzw8r7mm8bhqq91ar7ylpv2ayy7x4zdw7wzcqfhc")))

(define-public crate-bscore-lib-1.2.1 (c (n "bscore-lib") (v "1.2.1") (d (list (d (n "bscore") (r "^1.2") (d #t) (k 0)))) (h "141bp0lwhk1i6nzyl5spb6fcdhxg293jpkwvjzrpn0k4jfvbkn3v")))

