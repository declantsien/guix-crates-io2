(define-module (crates-io bs co bscore) #:use-module (crates-io))

(define-public crate-bscore-0.1.0 (c (n "bscore") (v "0.1.0") (h "0z4892fn27jyznnas8l80zgqpz9bkq45njyfdnkis7w1s9966yhz") (y #t)))

(define-public crate-bscore-0.1.1 (c (n "bscore") (v "0.1.1") (h "0xrdnck7zd4faawqv0zns02lgfvqc0nd18h9gvcdggkl4p7i3hb5") (y #t)))

(define-public crate-bscore-0.1.2 (c (n "bscore") (v "0.1.2") (h "1cf671ly28lbzllq1xkcn39imcbvcjig8jiq7g8ldvps7yd7sx4g") (y #t)))

(define-public crate-bscore-0.1.3 (c (n "bscore") (v "0.1.3") (h "1qv7ji0hpyq2fgqxqpj91rmjj374km8a9qqnwqi63a1fw5dpzsxv") (y #t)))

(define-public crate-bscore-1.0.0 (c (n "bscore") (v "1.0.0") (h "0jl81l7zbxav1p41pyx80ch2z3m84ka2j5z6svi0hyjls3vn067a") (y #t)))

(define-public crate-bscore-1.0.1 (c (n "bscore") (v "1.0.1") (h "0d4lqr977837bzbx6j2c04kd5z9q8s9ij8bl286nzmiscf6zhzmm")))

(define-public crate-bscore-1.1.1 (c (n "bscore") (v "1.1.1") (h "1fw5lqbn7c0fr5c25rkmljdx528anhxinhbcbvvssffd87xzs0yc")))

(define-public crate-bscore-1.2.1 (c (n "bscore") (v "1.2.1") (h "0s5pjnhqac4bq845lp3pj4ffx2b09gfklgz1pga4gbawscsb4p3s")))

