(define-module (crates-io bs et bset) #:use-module (crates-io))

(define-public crate-bset-0.1.0 (c (n "bset") (v "0.1.0") (d (list (d (n "byte_set") (r "^0.1.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "10fmd75f7acj850z81v1n7pqqcs0jaszj0vdxsiwhsivv7mg4ch8")))

