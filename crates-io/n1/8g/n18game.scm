(define-module (crates-io n1 #{8g}# n18game) #:use-module (crates-io))

(define-public crate-n18game-0.1.0 (c (n "n18game") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "n18catalogue") (r "^0.1.0") (d #t) (k 0)) (d (n "n18hex") (r "^0.1.0") (d #t) (k 0)) (d (n "n18map") (r "^0.1.0") (d #t) (k 0)) (d (n "n18route") (r "^0.1.0") (d #t) (k 0)) (d (n "n18tile") (r "^0.1.0") (d #t) (k 0)) (d (n "n18token") (r "^0.1.0") (d #t) (k 0)))) (h "1q0wnlfy08hihdhg42wb692z1aggv8k3lgdr7775qsrnj12ig2ml")))

