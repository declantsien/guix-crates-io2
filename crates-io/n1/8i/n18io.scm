(define-module (crates-io n1 #{8i}# n18io) #:use-module (crates-io))

(define-public crate-n18io-0.1.0 (c (n "n18io") (v "0.1.0") (d (list (d (n "n18catalogue") (r "^0.1.0") (d #t) (k 0)) (d (n "n18game") (r "^0.1.0") (d #t) (k 0)) (d (n "n18hex") (r "^0.1.0") (d #t) (k 0)) (d (n "n18map") (r "^0.1.0") (d #t) (k 0)) (d (n "n18route") (r "^0.1.0") (d #t) (k 0)) (d (n "n18tile") (r "^0.1.0") (d #t) (k 0)) (d (n "n18token") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1q7250dv5kraqvsj9xvvgs4jaxjadq72pangy5acbdgl7b6ig1mc")))

