(define-module (crates-io n1 #{8b}# n18brush) #:use-module (crates-io))

(define-public crate-n18brush-0.1.0 (c (n "n18brush") (v "0.1.0") (d (list (d (n "cairo-rs") (r "^0.14") (f (quote ("png" "pdf" "svg"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "n18hex") (r "^0.1.0") (d #t) (k 0)) (d (n "n18map") (r "^0.1.0") (d #t) (k 0)) (d (n "n18route") (r "^0.1.0") (d #t) (k 0)) (d (n "n18tile") (r "^0.1.0") (d #t) (k 0)) (d (n "n18token") (r "^0.1.0") (d #t) (k 0)))) (h "0b3hs04jc54vqp85hg4l9x00mx6d9vpiq9v1wywir4pgyzmjccsi")))

