(define-module (crates-io n1 #{8t}# n18token) #:use-module (crates-io))

(define-public crate-n18token-0.1.0 (c (n "n18token") (v "0.1.0") (d (list (d (n "cairo-rs") (r "^0.14") (f (quote ("png" "pdf" "svg"))) (d #t) (k 0)) (d (n "n18hex") (r "^0.1.0") (d #t) (k 0)))) (h "01yknzim67979gvwras5vd2m85hx8bh9x6imvzs1llzwbzdf1sdf")))

