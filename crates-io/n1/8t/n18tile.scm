(define-module (crates-io n1 #{8t}# n18tile) #:use-module (crates-io))

(define-public crate-n18tile-0.1.0 (c (n "n18tile") (v "0.1.0") (d (list (d (n "cairo-rs") (r "^0.14") (f (quote ("png" "pdf" "svg"))) (d #t) (k 0)) (d (n "n18hex") (r "^0.1.0") (d #t) (k 0)))) (h "12317h6v38rqq1vapdp0v1fyzwx8wj5jvvzh0sg96f7pbi0dny71")))

