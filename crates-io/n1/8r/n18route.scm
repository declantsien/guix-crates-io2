(define-module (crates-io n1 #{8r}# n18route) #:use-module (crates-io))

(define-public crate-n18route-0.1.0 (c (n "n18route") (v "0.1.0") (d (list (d (n "cairo-rs") (r "^0.14") (d #t) (k 2)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "n18hex") (r "^0.1.0") (d #t) (k 0)) (d (n "n18map") (r "^0.1.0") (d #t) (k 0)) (d (n "n18tile") (r "^0.1.0") (d #t) (k 0)) (d (n "n18token") (r "^0.1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1llz3ndf0xvlplyizn7hi6agh92bc0sqg68wkdkflsccpsvrl2nf")))

