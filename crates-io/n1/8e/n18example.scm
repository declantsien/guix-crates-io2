(define-module (crates-io n1 #{8e}# n18example) #:use-module (crates-io))

(define-public crate-n18example-0.1.0 (c (n "n18example") (v "0.1.0") (d (list (d (n "cairo-rs") (r "^0.14") (f (quote ("png" "pdf" "svg"))) (d #t) (k 0)) (d (n "n18brush") (r "^0.1.0") (d #t) (k 0)) (d (n "n18catalogue") (r "^0.1.0") (d #t) (k 0)) (d (n "n18game") (r "^0.1.0") (d #t) (k 0)) (d (n "n18hex") (r "^0.1.0") (d #t) (k 0)) (d (n "n18map") (r "^0.1.0") (d #t) (k 0)) (d (n "n18route") (r "^0.1.0") (d #t) (k 0)) (d (n "n18tile") (r "^0.1.0") (d #t) (k 0)) (d (n "n18token") (r "^0.1.0") (d #t) (k 0)))) (h "0zgp3c90iwkwb7ym327fj9kmww0qx1b1a4j2i5yl4asri5abjm4f")))

