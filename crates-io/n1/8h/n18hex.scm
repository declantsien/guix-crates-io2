(define-module (crates-io n1 #{8h}# n18hex) #:use-module (crates-io))

(define-public crate-n18hex-0.1.0 (c (n "n18hex") (v "0.1.0") (d (list (d (n "cairo-rs") (r "^0.14") (f (quote ("png" "pdf" "svg"))) (d #t) (k 0)) (d (n "pango") (r "^0.14") (d #t) (k 0)) (d (n "pangocairo") (r "^0.14") (d #t) (k 0)))) (h "1s9kvlb52plifakia12gjwzpbamb520zz0gpf9qcgm5x7ahxc4mv")))

