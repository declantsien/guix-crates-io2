(define-module (crates-io n1 #{8m}# n18map) #:use-module (crates-io))

(define-public crate-n18map-0.1.0 (c (n "n18map") (v "0.1.0") (d (list (d (n "cairo-rs") (r "^0.14") (f (quote ("png" "pdf" "svg"))) (d #t) (k 0)) (d (n "n18catalogue") (r "^0.1.0") (d #t) (k 0)) (d (n "n18hex") (r "^0.1.0") (d #t) (k 0)) (d (n "n18tile") (r "^0.1.0") (d #t) (k 0)) (d (n "n18token") (r "^0.1.0") (d #t) (k 0)))) (h "1m1vb9nzlax4gzxvfw28049s1fgs03qv2byf4pc1p24w0k1ic5d3")))

