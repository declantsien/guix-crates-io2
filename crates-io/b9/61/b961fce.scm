(define-module (crates-io b9 #{61}# b961fce) #:use-module (crates-io))

(define-public crate-b961fce-0.1.0 (c (n "b961fce") (v "0.1.0") (h "001zzysk545kfp1n267adqhd98rbfb6f09nfk40j37i65zhr7hnj")))

(define-public crate-b961fce-0.1.1 (c (n "b961fce") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)))) (h "0cry6fzk89gg361dp63fpql9nagvddrvns2rg72wis4lyvxcvsjk")))

(define-public crate-b961fce-0.1.2 (c (n "b961fce") (v "0.1.2") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)))) (h "0bk392i6zxqn6pl7q779qwbd2zl8zxdsc74chskp37wzrx0jr39q")))

(define-public crate-b961fce-0.1.3 (c (n "b961fce") (v "0.1.3") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)))) (h "0c51k34hv5plzfx6r2a3b57p97sdckh0s8jjvcild56vr34jz0iw")))

(define-public crate-b961fce-0.1.4 (c (n "b961fce") (v "0.1.4") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)))) (h "1ywcr3jnjvpcq8jnfql57sppb3wj189rxky3z5qwmlzqjjyjiqi5")))

(define-public crate-b961fce-0.1.5 (c (n "b961fce") (v "0.1.5") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)))) (h "18641qflrakzd2yvzzmxdgph13vkc31xn1bv8zgbmb0l6lgbskdb")))

(define-public crate-b961fce-0.1.6 (c (n "b961fce") (v "0.1.6") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)))) (h "03alab4cpbyk87mha6npf68a1dls2rwn69fafy01yni7q82m1arc")))

(define-public crate-b961fce-0.1.7 (c (n "b961fce") (v "0.1.7") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)))) (h "00vp3yh2ni9xxx0cwx63764sck23y7gyhlwbw19vk1pydhs9ia2m")))

(define-public crate-b961fce-0.1.8 (c (n "b961fce") (v "0.1.8") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)))) (h "07myhz9ps0bxs841zcz71vh6vgnqk5n8wshzr994ykkgfql29jg5")))

(define-public crate-b961fce-0.1.9 (c (n "b961fce") (v "0.1.9") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)))) (h "1kapm4dcsrmdgf59f0p2dnvqv8mfiyh5jwsh2qj1790v5fwrwalf")))

(define-public crate-b961fce-0.1.10 (c (n "b961fce") (v "0.1.10") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("ReadableStream"))) (d #t) (k 0)))) (h "1rr1z9dpjqrba3wyv2zh98j8aqvxk8bm58jnv4cy6fv0lw76daji")))

(define-public crate-b961fce-0.1.11 (c (n "b961fce") (v "0.1.11") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("ReadableStream"))) (d #t) (k 0)))) (h "1s8y1l0y9mqzv97cwkr9xr1mfm4zyyv351f4k9aippdh7vijpkvb")))

(define-public crate-b961fce-0.1.12 (c (n "b961fce") (v "0.1.12") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("ReadableStream"))) (d #t) (k 0)))) (h "1aw1llj225ymyxbhyrm9mjwc8bpv17gyj31rlc64hp356rcybzcr")))

