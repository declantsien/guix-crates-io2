(define-module (crates-io ce ta cetar) #:use-module (crates-io))

(define-public crate-cetar-0.1.0 (c (n "cetar") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "curl") (r "^0.4.46") (d #t) (k 0)))) (h "1zkwwh7vbv18ayb4almpqknip8cc144541llk5njj03qbs0am86y") (y #t)))

(define-public crate-cetar-0.1.1 (c (n "cetar") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "curl") (r "^0.4.46") (d #t) (k 0)))) (h "0wb10jad2a5sl11xxppwshn14hhdg397nb7pljzkpq1wngbdnwhs") (y #t)))

(define-public crate-cetar-0.1.3 (c (n "cetar") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "curl") (r "^0.4.46") (d #t) (k 0)))) (h "0df042kdksq5f9ybcdd6fl2hj098s34cj2hgc0sb5pnpbayw8wpz") (y #t)))

(define-public crate-cetar-0.1.4 (c (n "cetar") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "curl") (r "^0.4.46") (d #t) (k 0)))) (h "0yccnmmfbfcm059j0g12fgbnic05ddx15galiwf6c2517s5jg3x6") (y #t)))

(define-public crate-cetar-0.1.5 (c (n "cetar") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "curl") (r "^0.4.46") (d #t) (k 0)) (d (n "httpmock") (r "^0.7.0-rc.1") (d #t) (k 2)))) (h "0binshygs0lcam6969xa4izmfh73xijdlbn4cvscaymh5rx6zhr2") (y #t)))

(define-public crate-cetar-0.1.6 (c (n "cetar") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "curl") (r "^0.4.46") (d #t) (k 0)) (d (n "httpmock") (r "^0.7.0-rc.1") (d #t) (k 2)))) (h "1c98b7pgw2kqcmiz4pl3gsjcz5b362wdhkwhp54f3alqni0zk7m0")))

