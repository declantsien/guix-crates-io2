(define-module (crates-io ce rc cercis-html) #:use-module (crates-io))

(define-public crate-cercis-html-0.1.0 (c (n "cercis-html") (v "0.1.0") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)))) (h "065kp54w671w88km01397hidvqcdcl7p8sfwwada7h8xsi4zfqkd")))

(define-public crate-cercis-html-0.1.1 (c (n "cercis-html") (v "0.1.1") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)))) (h "0myswnj33gfpvy5cz5hki7rqr88qvc1nldfc5m8wgani1d034vx6")))

(define-public crate-cercis-html-0.1.2 (c (n "cercis-html") (v "0.1.2") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)))) (h "172hps73y8cwvb566w2744a3fwvl1lxd5cvay4h3v21s6p9zrhgh")))

(define-public crate-cercis-html-0.1.4 (c (n "cercis-html") (v "0.1.4") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)))) (h "1a1kfc6f1radiv188795kfh2bkf1qdx5yk9im96zrcvkcfa6g41h")))

(define-public crate-cercis-html-0.1.5 (c (n "cercis-html") (v "0.1.5") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)))) (h "0b0a578kiwh9s2gss9slmhcdx06qk8aryng3hkhx08pjpvhqkwqr")))

(define-public crate-cercis-html-0.1.7 (c (n "cercis-html") (v "0.1.7") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)))) (h "16brdibl6yr7ak0lcbkigx0bdsyv8f5pvii1mvdkaafcn94g24mk")))

(define-public crate-cercis-html-0.1.8 (c (n "cercis-html") (v "0.1.8") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)))) (h "0pmrlsqvyj54krs9k3dq0mfnbhycns7pls7ghllsgv036a7w46mk")))

(define-public crate-cercis-html-0.1.9 (c (n "cercis-html") (v "0.1.9") (d (list (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)))) (h "1cs7zx2yj0ndia9qswykfs50hm3clb1bian7cpda03w9jlpnb0il")))

(define-public crate-cercis-html-0.1.10 (c (n "cercis-html") (v "0.1.10") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)))) (h "1yp3zz01iwplv1vl44ksgdip4vbij6byhl27wrw8zifrgj699mdj")))

(define-public crate-cercis-html-0.2.0 (c (n "cercis-html") (v "0.2.0") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)))) (h "1g729gwygnz6ndyz4l2xqj46xdhzms192zwar3792cq56w4pxd91")))

(define-public crate-cercis-html-0.2.1 (c (n "cercis-html") (v "0.2.1") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)))) (h "1skzw8b6k8z9hlj3j4ddqcg7cdi54ly08mf3d14m7a376gip0l1h")))

(define-public crate-cercis-html-0.2.2 (c (n "cercis-html") (v "0.2.2") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)))) (h "0cjzs1xqhzqm1s9v1ivi6rz57mlq1mvaszly3kd6989j3l258z54")))

(define-public crate-cercis-html-0.2.3 (c (n "cercis-html") (v "0.2.3") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)))) (h "0m0rzc92yjk0j94a49jy5kdsabnidcjdzhxpsya12y1l05jarj5q")))

(define-public crate-cercis-html-0.3.0 (c (n "cercis-html") (v "0.3.0") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)))) (h "1ldvswdh3nkxprswbkmpx668bb0g06i25pavksfvzq8acb7188b8")))

(define-public crate-cercis-html-0.3.1 (c (n "cercis-html") (v "0.3.1") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)))) (h "083475chnxx1i3rddn7yajdrfhf5i607ljvgzsq77lb8gmqb7qxx")))

(define-public crate-cercis-html-1.0.0 (c (n "cercis-html") (v "1.0.0") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)))) (h "1484bdcw87i3fih6xakr6ygqsq23dc35ihdjxl0sm7aj1a5g6m5l")))

(define-public crate-cercis-html-1.0.1 (c (n "cercis-html") (v "1.0.1") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)))) (h "1y0al673px4vzlqfka5xc8r89p7k56klv7q414nhh4251cnvgkxd")))

(define-public crate-cercis-html-1.1.0 (c (n "cercis-html") (v "1.1.0") (d (list (d (n "html-escape") (r "^0.2") (d #t) (k 0)))) (h "0awh6r867x2vy0qd7yz4p40gv1bk6hqsx2djkd552k4994xpzbi4")))

