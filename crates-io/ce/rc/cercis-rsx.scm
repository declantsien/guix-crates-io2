(define-module (crates-io ce rc cercis-rsx) #:use-module (crates-io))

(define-public crate-cercis-rsx-0.1.2 (c (n "cercis-rsx") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "0vv1wfnnx5l13cmva59jdy3rp39j3vj0wxlx2vm8zg3w2222rqlb")))

(define-public crate-cercis-rsx-0.1.4 (c (n "cercis-rsx") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "11mi0b03fqjghwvfmhhvp8m14glc9rjwhgjb8wam9isiyknfid14")))

(define-public crate-cercis-rsx-0.1.5 (c (n "cercis-rsx") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "1lsd3dk7i3v8nbpk1yaf3d2dn35jgh8lpyh8q7hkf02q4xxfq1c4")))

(define-public crate-cercis-rsx-0.1.7 (c (n "cercis-rsx") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "03nddjblcmgbahhpphp5cplnr89faf4f8pci0r0cl9fgm677hnl7")))

(define-public crate-cercis-rsx-0.1.8 (c (n "cercis-rsx") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "0cw1h7vxms7yb8cyf8049vfvj10zd87n2n2pdx9r6c4yys7z23gs")))

(define-public crate-cercis-rsx-0.1.9 (c (n "cercis-rsx") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "06n8bwj24qp8jdccspx76a7nz3c4xyrqscaggrcdnmhxspsl5paq")))

(define-public crate-cercis-rsx-0.1.10 (c (n "cercis-rsx") (v "0.1.10") (d (list (d (n "cercis") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0wn7njprb0hwwiddbsbbnp7ca9zrixgbfjz697vnfpylqf0zmvli")))

(define-public crate-cercis-rsx-0.2.0 (c (n "cercis-rsx") (v "0.2.0") (d (list (d (n "cercis") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0ja9hvigy4ghr0b738k0s9ngjgjfymkw52jrgvzh8s8sqxy1mwwd")))

(define-public crate-cercis-rsx-0.2.1 (c (n "cercis-rsx") (v "0.2.1") (d (list (d (n "cercis") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "01dacjrbq9cx5z3b67ns3b0kmfad05ag6srk5hvy2zcihfwwxam9")))

(define-public crate-cercis-rsx-0.2.2 (c (n "cercis-rsx") (v "0.2.2") (d (list (d (n "cercis") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0w2khz5blw1pfj15a35kgd78y70i2svr2wpjcdwkp3l0mk8w2f88")))

(define-public crate-cercis-rsx-0.2.3 (c (n "cercis-rsx") (v "0.2.3") (d (list (d (n "cercis") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0cjx5vjrlgf190i8zijgc024y74xq7anbb2wg07b2lbi0xhkfq4k")))

(define-public crate-cercis-rsx-0.3.0 (c (n "cercis-rsx") (v "0.3.0") (d (list (d (n "cercis") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1wwcq9qdi5fhzf5lqhxwbs1v4458d81cdf6az28hax0qzswhjik0")))

(define-public crate-cercis-rsx-0.3.1 (c (n "cercis-rsx") (v "0.3.1") (d (list (d (n "cercis") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0m1rrll4mwl89y2lk4gbqmxm9hap4bspzlvcsnp8irq1idgxwh4d")))

(define-public crate-cercis-rsx-1.0.0 (c (n "cercis-rsx") (v "1.0.0") (d (list (d (n "cercis") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0aqizd9zhzr0mvh48gg52qdivb8409m5qvchdzmh6l7px977gsx8")))

(define-public crate-cercis-rsx-1.0.1 (c (n "cercis-rsx") (v "1.0.1") (d (list (d (n "cercis") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0n6m5wrd7g0xffraj0ikhhya2dwj4rp28ziyxzq1jmhc5cvv7nfc")))

(define-public crate-cercis-rsx-1.1.0 (c (n "cercis-rsx") (v "1.1.0") (d (list (d (n "cercis") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0f2syd2v7yjlj0igvd326q7x38kx54234r7yqs28bhgvxwghc5k4")))

