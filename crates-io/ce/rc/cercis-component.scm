(define-module (crates-io ce rc cercis-component) #:use-module (crates-io))

(define-public crate-cercis-component-0.1.2 (c (n "cercis-component") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0lxl16716yx2dmny3aqbpsghjk1ms0sr45vyrbxmpmbrwyv7y965")))

(define-public crate-cercis-component-0.1.4 (c (n "cercis-component") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "11h63hsy9xld5whv55pv7rcvxhsvaas5czcqpfwavih4xz7maq2c")))

(define-public crate-cercis-component-0.1.5 (c (n "cercis-component") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "00p3q6pdr4ngmwbq9b4nnykzqri58s500hds9rh992f7wj6ndjwn")))

(define-public crate-cercis-component-0.1.7 (c (n "cercis-component") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "08isi819vcc38144plwv7cr2f26qn0lrqmj6v57czbbqxz0qsa5i")))

(define-public crate-cercis-component-0.1.8 (c (n "cercis-component") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "169rcdh8wxgi1zvc6lqbibc7gvxj2q2nkr2dm85yjrlzpsr8mfkz")))

(define-public crate-cercis-component-0.1.9 (c (n "cercis-component") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0im6j1kk65wgcvxj345d57wj1w80jgg2iym7pmhqpm863vwmqxca")))

(define-public crate-cercis-component-0.1.10 (c (n "cercis-component") (v "0.1.10") (d (list (d (n "cercis") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0048pmkkh4wh7kb93f2djlzxx6lbvzgap8wkp328nnr2ryr7k3fk")))

(define-public crate-cercis-component-0.2.0 (c (n "cercis-component") (v "0.2.0") (d (list (d (n "cercis") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1m0zm1sjk1s82n6nmbfsazy00njlviq4jnxhgy56sj8qmck32dz7")))

(define-public crate-cercis-component-0.2.1 (c (n "cercis-component") (v "0.2.1") (d (list (d (n "cercis") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0h543a0y2s16v6kwk1wpnzlx2bs6j4q0bghvwyjrbn9rjsnm2aia")))

(define-public crate-cercis-component-0.2.2 (c (n "cercis-component") (v "0.2.2") (d (list (d (n "cercis") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "040k406i2a3zc47pnz4wx70w1j18nh7xv09rvqi2r6x7xs4aq0ql")))

(define-public crate-cercis-component-0.2.3 (c (n "cercis-component") (v "0.2.3") (d (list (d (n "cercis") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1557r5j4d99cpxr6hg6w68y4k9vdpcx41vs5wlql7bkihzs9zcpq")))

(define-public crate-cercis-component-0.3.0 (c (n "cercis-component") (v "0.3.0") (d (list (d (n "cercis") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "14sdipxh2z27f4r42xz1y392andidsg2cgx8i55r226kdkb3h3xk")))

(define-public crate-cercis-component-0.3.1 (c (n "cercis-component") (v "0.3.1") (d (list (d (n "cercis") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0mcyyylynm1dw0yjf6k231gp7ylafzsbb5qhgy8ahkf5w9kvvh6c")))

(define-public crate-cercis-component-1.0.0 (c (n "cercis-component") (v "1.0.0") (d (list (d (n "cercis") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "00qrf428g84gc0a4lana1hlahs2a5gzyxz1zmxk9ykw261s4xy81")))

(define-public crate-cercis-component-1.0.1 (c (n "cercis-component") (v "1.0.1") (d (list (d (n "cercis") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0rmq2nzgrliddvjn33gs812csllb0rbzvxpcag0y70l1wlaikg10")))

(define-public crate-cercis-component-1.1.0 (c (n "cercis-component") (v "1.1.0") (d (list (d (n "cercis") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "00f10nlipiibiih90kvb17i3sw88v5zhc9mcbyv174zikxhdgld2")))

