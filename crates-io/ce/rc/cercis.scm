(define-module (crates-io ce rc cercis) #:use-module (crates-io))

(define-public crate-cercis-0.1.2 (c (n "cercis") (v "0.1.2") (d (list (d (n "cercis-component") (r "^0.1.2") (d #t) (k 0)) (d (n "cercis-html") (r "^0.1.2") (d #t) (k 0)) (d (n "cercis-rsx") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "typed-builder") (r "^0.18.2") (d #t) (k 0)))) (h "0c4i9mii926hgfyxlaxjvbf3fkrfbizhc20lj8myfl3cf4hkmyyf")))

(define-public crate-cercis-0.1.3 (c (n "cercis") (v "0.1.3") (d (list (d (n "cercis-component") (r "^0.1") (d #t) (k 0)) (d (n "cercis-html") (r "^0.1") (d #t) (k 0)) (d (n "cercis-rsx") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "typed-builder") (r "^0.18.2") (d #t) (k 0)))) (h "1gsmnsnrgdk9h9gb0pkcwsv6ikzifn4lfvmvgby08kyph1xqijjv")))

(define-public crate-cercis-0.1.4 (c (n "cercis") (v "0.1.4") (d (list (d (n "cercis-component") (r "^0.1") (d #t) (k 0)) (d (n "cercis-html") (r "^0.1") (d #t) (k 0)) (d (n "cercis-rsx") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "typed-builder") (r "^0.18.2") (d #t) (k 0)))) (h "18ax0dx7d6kmmyjlqlvds7k5wn7kwbq9j6xak0bwi695ssyhd86h")))

(define-public crate-cercis-0.1.5 (c (n "cercis") (v "0.1.5") (d (list (d (n "cercis-component") (r "^0.1") (d #t) (k 0)) (d (n "cercis-html") (r "^0.1") (d #t) (k 0)) (d (n "cercis-rsx") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "typed-builder") (r "^0.18.2") (d #t) (k 0)))) (h "1vqhlsrvawf5x97370ng40qbw7xfic28jicknn9g1hwwaqbmykwa")))

(define-public crate-cercis-0.1.6 (c (n "cercis") (v "0.1.6") (d (list (d (n "cercis-component") (r "^0.1") (d #t) (k 0)) (d (n "cercis-html") (r "^0.1") (d #t) (k 0)) (d (n "cercis-rsx") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "typed-builder") (r "^0.18.2") (d #t) (k 0)))) (h "0hh3i272grn90dxwwm5nrp70p827dr0y41cvd741xg1d3y2g0f43")))

(define-public crate-cercis-0.1.7 (c (n "cercis") (v "0.1.7") (d (list (d (n "cercis-component") (r "^0.1") (d #t) (k 0)) (d (n "cercis-html") (r "^0.1") (d #t) (k 0)) (d (n "cercis-rsx") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "typed-builder") (r "^0.18.2") (d #t) (k 0)))) (h "1hq7y01fiaqanwq7xksqz0lnrzs56253lwz65pl107hhrk8rxhcf")))

(define-public crate-cercis-0.1.8 (c (n "cercis") (v "0.1.8") (d (list (d (n "cercis-component") (r "^0.1") (d #t) (k 0)) (d (n "cercis-html") (r "^0.1") (d #t) (k 0)) (d (n "cercis-rsx") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "typed-builder") (r "^0.18.2") (d #t) (k 0)))) (h "0v69rhzqjqwwz891fcbrhhf6dkqj332k0lflnl0d8wklcq0wj72i")))

(define-public crate-cercis-0.1.9 (c (n "cercis") (v "0.1.9") (d (list (d (n "cercis-component") (r "^0.1") (d #t) (k 0)) (d (n "cercis-html") (r "^0.1") (d #t) (k 0)) (d (n "cercis-rsx") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "typed-builder") (r "^0.18.2") (d #t) (k 0)))) (h "06zclmjbjqng65znm4ksrsxp1yrgmycjpxswzjyv8526yfikymyf")))

(define-public crate-cercis-0.1.10 (c (n "cercis") (v "0.1.10") (d (list (d (n "cercis-component") (r "^0.1") (d #t) (k 0)) (d (n "cercis-html") (r "^0.1") (d #t) (k 0)) (d (n "cercis-rsx") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "typed-builder") (r "^0.18.2") (d #t) (k 0)))) (h "10v5v3c6lv701j4ra4rj05b4c2xgqxa7fvmjw7lsaz712k80a9m6")))

(define-public crate-cercis-0.2.0 (c (n "cercis") (v "0.2.0") (d (list (d (n "cercis-component") (r "^0.1") (d #t) (k 0)) (d (n "cercis-html") (r "^0.1") (d #t) (k 0)) (d (n "cercis-rsx") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "typed-builder") (r "^0.18.2") (d #t) (k 0)))) (h "07s9ik0z4cbi2xhywf6yy8f54dkg0sm8ad379vxscjm29z47chfq")))

(define-public crate-cercis-0.2.1 (c (n "cercis") (v "0.2.1") (d (list (d (n "cercis-component") (r "^0.2") (d #t) (k 0)) (d (n "cercis-html") (r "^0.2") (d #t) (k 0)) (d (n "cercis-rsx") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "typed-builder") (r "^0.18.2") (d #t) (k 0)))) (h "0pg987nmna7hh4vcs6ngg6gzfjnfs1gpffz27q9hjmb39576522s")))

(define-public crate-cercis-0.2.2 (c (n "cercis") (v "0.2.2") (d (list (d (n "cercis-component") (r "^0.2") (d #t) (k 0)) (d (n "cercis-html") (r "^0.2") (d #t) (k 0)) (d (n "cercis-rsx") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "typed-builder") (r "^0.18.2") (d #t) (k 0)))) (h "1vrfqy7q7dnq47vsb41srld13gjz4an397j25pgbs98k44lf4yc1")))

(define-public crate-cercis-0.2.3 (c (n "cercis") (v "0.2.3") (d (list (d (n "cercis-component") (r "^0.2") (d #t) (k 0)) (d (n "cercis-html") (r "^0.2") (d #t) (k 0)) (d (n "cercis-rsx") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "typed-builder") (r "^0.18.2") (d #t) (k 0)))) (h "1pk9q2p200a70dm6cdzpr2zxcvhq7xl4fcyif2cy591gznjjgxsm")))

(define-public crate-cercis-0.3.0 (c (n "cercis") (v "0.3.0") (d (list (d (n "cercis-component") (r "^0.3") (d #t) (k 0)) (d (n "cercis-html") (r "^0.3") (d #t) (k 0)) (d (n "cercis-rsx") (r "^0.3") (d #t) (k 0)) (d (n "typed-builder") (r "^0.18.2") (d #t) (k 0)))) (h "0wq8xd0001l6yijsqivyhzywbfa4p91f75xggmfmqsis28aj9mzd")))

(define-public crate-cercis-0.3.1 (c (n "cercis") (v "0.3.1") (d (list (d (n "cercis-component") (r "^0.3") (d #t) (k 0)) (d (n "cercis-html") (r "^0.3") (d #t) (k 0)) (d (n "cercis-rsx") (r "^0.3") (d #t) (k 0)) (d (n "typed-builder") (r "^0.18.2") (d #t) (k 0)))) (h "050vq5c8f29pnljgcskkhm9qk27b517m6ig2vc0q15dha3i3pi2j")))

(define-public crate-cercis-1.0.0 (c (n "cercis") (v "1.0.0") (d (list (d (n "cercis-component") (r "^1") (d #t) (k 0)) (d (n "cercis-html") (r "^1") (d #t) (k 0)) (d (n "cercis-rsx") (r "^1") (d #t) (k 0)) (d (n "typed-builder") (r "^0.18.2") (d #t) (k 0)))) (h "1i2rmh055ym593jzmac7fr2xw3nccnsgh7vdzj06a2sx2sdqkzm3")))

(define-public crate-cercis-1.0.1 (c (n "cercis") (v "1.0.1") (d (list (d (n "cercis-component") (r "^1") (d #t) (k 0)) (d (n "cercis-html") (r "^1") (d #t) (k 0)) (d (n "cercis-rsx") (r "^1") (d #t) (k 0)) (d (n "typed-builder") (r "^0.18.2") (d #t) (k 0)))) (h "1ipbhh9769qwybplvwv5j19n4w0marrv346pw23j15pkkiwsn1fd")))

(define-public crate-cercis-1.1.0 (c (n "cercis") (v "1.1.0") (d (list (d (n "cercis-component") (r "^1") (d #t) (k 0)) (d (n "cercis-html") (r "^1") (d #t) (k 0)) (d (n "cercis-rsx") (r "^1") (d #t) (k 0)) (d (n "typed-builder") (r "^0.18.2") (d #t) (k 0)))) (h "1jphgizyy0bvc2mxvjnb9fdim70mmfmgpn17vj1mf80p6547wccy")))

