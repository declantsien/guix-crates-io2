(define-module (crates-io ce c1 cec1734-pac) #:use-module (crates-io))

(define-public crate-cec1734-pac-0.0.1 (c (n "cec1734-pac") (v "0.0.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1b35cir8glqizxs7p2cknwgjn5vnd49xqh4yl5kvvdq5774rfg16") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-cec1734-pac-0.0.22 (c (n "cec1734-pac") (v "0.0.22") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0rvfvfzjip8rfcdrrvpbm5wjwp3iy440lzzjgavjnxsjmws47i7r") (f (quote (("rt" "cortex-m-rt/device"))))))

