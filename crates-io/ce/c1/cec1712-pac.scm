(define-module (crates-io ce c1 cec1712-pac) #:use-module (crates-io))

(define-public crate-cec1712-pac-0.0.1 (c (n "cec1712-pac") (v "0.0.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0kzy3bzng2y9q4xwdl0jmlfxnx6gkdy9g1aijlgz8sacnfcmp5g5") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-cec1712-pac-0.0.2 (c (n "cec1712-pac") (v "0.0.2") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "00cdjhzad17gsfswj1cvyvxvmxsci7ijmhgzib22za826j3i25xj") (f (quote (("rt" "cortex-m-rt/device"))))))

