(define-module (crates-io ce c1 cec1736-pac) #:use-module (crates-io))

(define-public crate-cec1736-pac-0.0.1 (c (n "cec1736-pac") (v "0.0.1") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "12mf4ad9kcbzhbjmfrnl5djllwkjbbh8l5nv79dvc5xpq8khxxih") (f (quote (("rt" "cortex-m-rt/device")))) (y #t)))

(define-public crate-cec1736-pac-0.0.2 (c (n "cec1736-pac") (v "0.0.2") (d (list (d (n "bare-metal") (r "^1.0.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.7.6") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0bcq9vwd20sy0j9x8cb4k5zvc67g0i06hd72mp05p9yladh31cyb") (f (quote (("rt" "cortex-m-rt/device"))))))

