(define-module (crates-io ce a7 cea708-types) #:use-module (crates-io))

(define-public crate-cea708-types-0.0.1 (c (n "cea708-types") (v "0.0.1") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("std" "env-filter"))) (d #t) (k 0)))) (h "0wwmv7jmwy2n4jg9rvmc3q826akv2g96ggq0incqjkxwbzzyj9p6")))

(define-public crate-cea708-types-0.1.0 (c (n "cea708-types") (v "0.1.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("std" "env-filter"))) (d #t) (k 0)))) (h "0n69gjz20slkksyqcpwlfdgch65w5ik2rwpn3s3gpamjd9ryq8rs")))

(define-public crate-cea708-types-0.2.0 (c (n "cea708-types") (v "0.2.0") (d (list (d (n "muldiv") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("std" "env-filter"))) (d #t) (k 0)))) (h "0n5jr0456ay23vr7z8kq08yh3hdffxqhj3p7nh1wi7wicjs7gly6")))

(define-public crate-cea708-types-0.2.1 (c (n "cea708-types") (v "0.2.1") (d (list (d (n "muldiv") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("std" "env-filter"))) (d #t) (k 0)))) (h "0ywb7bij0zay8gg6v3jhg3lwlrd7jg0xbjgpr2z1ahziybwanypl")))

(define-public crate-cea708-types-0.3.0 (c (n "cea708-types") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "muldiv") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)))) (h "0sxn0323i2m9bknaq99as46hszhwipn68ny4fgflyyx94nqjckdb")))

(define-public crate-cea708-types-0.3.1 (c (n "cea708-types") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "muldiv") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ya4gbgvnziwl2i7s4vm5jy95xvbyq48fs3fkjhrmwdn7i4k6zzx") (r "1.63.0")))

(define-public crate-cea708-types-0.3.2 (c (n "cea708-types") (v "0.3.2") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "muldiv") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "09k4jjafazvq37b7vjny411h3y2yzsy8klbwdhayg0yfili2bf42") (r "1.63.0")))

