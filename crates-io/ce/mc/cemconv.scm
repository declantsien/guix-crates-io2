(define-module (crates-io ce mc cemconv) #:use-module (crates-io))

(define-public crate-cemconv-0.1.0 (c (n "cemconv") (v "0.1.0") (d (list (d (n "cem") (r "^0.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "structopt") (r "^0.1.6") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1.6") (d #t) (k 0)) (d (n "wavefront_obj") (r "^5.1.0") (d #t) (k 0)))) (h "1f7q406ljwf9b7mkmz86kv2ci4jqykkbsk4fhbyysfv6xzahkm9g")))

