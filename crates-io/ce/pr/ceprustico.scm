(define-module (crates-io ce pr ceprustico) #:use-module (crates-io))

(define-public crate-ceprustico-0.1.0 (c (n "ceprustico") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "03wi2f83c3mh72349pakbhddg1imzf8cziybici80lv15sj17x99")))

(define-public crate-ceprustico-0.1.1 (c (n "ceprustico") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0xq3rc6dh9sh8sacnwj17bv8ak8sm799haq1rq89jwj3a803my26")))

