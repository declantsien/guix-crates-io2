(define-module (crates-io ce ls celsium) #:use-module (crates-io))

(define-public crate-celsium-0.1.0 (c (n "celsium") (v "0.1.0") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "0zch277lj69kxny8d4qpi9crfahfb5bgb9qmbgiinhj04s6mr207")))

(define-public crate-celsium-0.1.1 (c (n "celsium") (v "0.1.1") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1pf3d0v69hdx4f70qpvjbisapxgnw8avlb7czdl9787fpdngmsyc")))

(define-public crate-celsium-0.1.2 (c (n "celsium") (v "0.1.2") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "0ykvvg8s1s83v9a49g78kvn1kk7bgqj3gds8yr0q2ryal69nnmc2")))

(define-public crate-celsium-0.1.3 (c (n "celsium") (v "0.1.3") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (d #t) (k 0)))) (h "18pmycak922khalxwx1r070ji90ramm5k2sffh2szdmg40cgkcpn")))

(define-public crate-celsium-0.1.4 (c (n "celsium") (v "0.1.4") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (d #t) (k 0)))) (h "00bqv7kjk5048fzwa507shfibsak66cz7syzpmv82zn64cz77ijj")))

(define-public crate-celsium-0.1.5 (c (n "celsium") (v "0.1.5") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.92") (d #t) (k 0)))) (h "00bkpmnbhk2zldkdvh10yb94hrrjmnysh83nbd1bcknzdg8cvs7z")))

