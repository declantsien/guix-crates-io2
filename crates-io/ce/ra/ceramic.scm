(define-module (crates-io ce ra ceramic) #:use-module (crates-io))

(define-public crate-ceramic-0.1.0 (c (n "ceramic") (v "0.1.0") (d (list (d (n "nix") (r "^0.7.0") (d #t) (k 0)) (d (n "stainless") (r "^0.1.10") (d #t) (k 2)))) (h "1xn2ncha2sm5bhllrnbv6m1krk10hvlg6m2sn11a2yj99p59i0vw")))

(define-public crate-ceramic-0.1.1 (c (n "ceramic") (v "0.1.1") (d (list (d (n "nix") (r "^0.7.0") (d #t) (k 0)))) (h "05js8k39gsdyjba5zdidgvbp9ym6mgdqfc78x59k6knaasnznqhl") (y #t)))

(define-public crate-ceramic-0.1.2 (c (n "ceramic") (v "0.1.2") (d (list (d (n "nix") (r "^0.7.0") (d #t) (k 0)))) (h "0p95jqa5ljb2bjxrzqygh1piai868awxh5yqj6ghfx5jjs8ww1xk")))

(define-public crate-ceramic-0.1.3 (c (n "ceramic") (v "0.1.3") (d (list (d (n "bincode") (r "^0.6.1") (d #t) (k 0)) (d (n "nix") (r "^0.7.0") (d #t) (k 0)) (d (n "rain") (r "^0.2.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1yx73dpb1nm47xq0l4ldlrl70nh9h86j7vb9nvkmrv2mjmlp6z4w")))

(define-public crate-ceramic-0.2.1 (c (n "ceramic") (v "0.2.1") (d (list (d (n "bincode") (r "^0.6.1") (d #t) (k 0)) (d (n "nix") (r "^0.7.0") (d #t) (k 0)) (d (n "rain") (r "^0.2.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "115nk048xi68ba056by3q25chpxp2lb7m3qc4g3xrilwyp5mhpvv")))

(define-public crate-ceramic-0.3.0 (c (n "ceramic") (v "0.3.0") (d (list (d (n "bincode") (r "^0.6.1") (d #t) (k 0)) (d (n "nix") (r "^0.7.0") (d #t) (k 0)) (d (n "rain") (r "^0.2.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1qgjsvldbj0n7fvsgzdaqn0hynalsibj6ils73xl3jq6x7ivjdhq")))

