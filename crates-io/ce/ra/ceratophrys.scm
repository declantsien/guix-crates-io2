(define-module (crates-io ce ra ceratophrys) #:use-module (crates-io))

(define-public crate-ceratophrys-0.0.1 (c (n "ceratophrys") (v "0.0.1") (d (list (d (n "gif") (r "^0.12.0") (d #t) (k 0)) (d (n "orfail") (r "^1.1.0") (d #t) (k 2)))) (h "0rs16g2vm5r579w8vnzjcdyydpq42wc7l3icsis14rmar98fbp2c")))

(define-public crate-ceratophrys-0.0.2 (c (n "ceratophrys") (v "0.0.2") (d (list (d (n "gif") (r "^0.12.0") (d #t) (k 0)) (d (n "orfail") (r "^1.1.0") (d #t) (k 2)))) (h "19f9czvzggaw0pychzhsh6qibgz84nj6kcjjsagm6ldhnmcj1hwn")))

(define-public crate-ceratophrys-0.0.3 (c (n "ceratophrys") (v "0.0.3") (d (list (d (n "gif") (r "^0.12.0") (d #t) (k 0)) (d (n "orfail") (r "^1.1.0") (d #t) (k 2)))) (h "13hckf1m0924ilnqc8m298aifbapri1zb1pf1mbfp1g6yvwc7yl8")))

