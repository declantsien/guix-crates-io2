(define-module (crates-io ce re ceres-solver-src) #:use-module (crates-io))

(define-public crate-ceres-solver-src-0.1.0+ceres2.1.0-eigen3.4.0 (c (n "ceres-solver-src") (v "0.1.0+ceres2.1.0-eigen3.4.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1506ca5rbnrzlz2hv282q8sjmvavvl97z1m7q4fyhncx4xjnxkhm") (l "ceres") (r "1.57.0")))

(define-public crate-ceres-solver-src-0.1.1-beta.0+ceres2.1.0-eigen3.4.0 (c (n "ceres-solver-src") (v "0.1.1-beta.0+ceres2.1.0-eigen3.4.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0xi84lqn95bqjbxy93c9albdbipcq19askiyy30s419nx27b51s8") (l "ceres") (r "1.57.0")))

(define-public crate-ceres-solver-src-0.1.1+ceres2.1.0-eigen3.4.0 (c (n "ceres-solver-src") (v "0.1.1+ceres2.1.0-eigen3.4.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0qhnrw5bzy3yy4fi872bns43v6vy9ga3l85y425vxsvpq6g8fg03") (l "ceres") (r "1.57.0")))

(define-public crate-ceres-solver-src-0.2.0-beta.0+ceres2.1.0-eigen3.4.0-glog0.6.0 (c (n "ceres-solver-src") (v "0.2.0-beta.0+ceres2.1.0-eigen3.4.0-glog0.6.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1f0arak1k8iipby4326icnbnz9zcxdp4kcjrlsyziy1jwmpbb50g") (l "ceres") (r "1.57.0")))

(define-public crate-ceres-solver-src-0.2.0+ceres2.1.0-eigen3.4.0-glog0.6.0 (c (n "ceres-solver-src") (v "0.2.0+ceres2.1.0-eigen3.4.0-glog0.6.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "04dlymb6d4qpi7mnblwm0y3s60awv612yqxg4xc1990glg1k0lb3") (l "ceres") (r "1.57.0")))

(define-public crate-ceres-solver-src-0.3.0+ceres2.2.0-eigen3.4.0-glog0.7.0 (c (n "ceres-solver-src") (v "0.3.0+ceres2.2.0-eigen3.4.0-glog0.7.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0m42l0x5qny6432viprw897qdpp9jga16b2l2bsqrm32zx3n0dvn") (l "ceres") (r "1.57.0")))

