(define-module (crates-io ce re cerebrum) #:use-module (crates-io))

(define-public crate-cerebrum-0.1.0 (c (n "cerebrum") (v "0.1.0") (h "0fm2rqr71aj4al5a8yjk61mjdga4cai0kb5w43qkapk6j1kwxlg0")))

(define-public crate-cerebrum-0.1.1 (c (n "cerebrum") (v "0.1.1") (h "0xbsc0w52hsad7ydkrqvbksfqi3rp97hzab5w0isi478p1g8mrxr")))

