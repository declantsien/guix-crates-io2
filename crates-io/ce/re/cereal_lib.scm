(define-module (crates-io ce re cereal_lib) #:use-module (crates-io))

(define-public crate-cereal_lib-1.0.0 (c (n "cereal_lib") (v "1.0.0") (d (list (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "statistical") (r "^1.0.0") (d #t) (k 0)))) (h "04g3j43cxxjyaysrmmx4mf9q5ap0gfaibfg5f5lpivxsibb8w8zi")))

(define-public crate-cereal_lib-2.1.0 (c (n "cereal_lib") (v "2.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "statistical") (r "^1.0.0") (d #t) (k 0)))) (h "1kba0vwa5fk01p2y5i0b04ma75s5mwqs8mwf6y1naf9gsg4y9jwz")))

(define-public crate-cereal_lib-2.1.1 (c (n "cereal_lib") (v "2.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "statistical") (r "^1.0.0") (d #t) (k 0)))) (h "0bcqdkyidx2hqj8nnvp3is81nmxxjsms9f6rvnqva3wpjsqws6ls")))

