(define-module (crates-io ce re ceres-std) #:use-module (crates-io))

(define-public crate-ceres-std-0.1.0 (c (n "ceres-std") (v "0.1.0") (h "05lwa088r26ijk69dy5r32pw3332z592z618srrl8wblx09j3qiy") (f (quote (("std") ("default"))))))

(define-public crate-ceres-std-0.2.0 (c (n "ceres-std") (v "0.2.0") (h "1iyq2nsjx1g2qi219s7hwn8d5ccbyjjb2kp35kn5axkschvk5nwp") (f (quote (("std") ("default"))))))

