(define-module (crates-io ce re cereal_macros) #:use-module (crates-io))

(define-public crate-cereal_macros-0.1.0 (c (n "cereal_macros") (v "0.1.0") (h "045fni3fx5i32ys237nwx5r3mrvd8l75bj6sji30dz0x5zvv6bfp")))

(define-public crate-cereal_macros-0.2.0 (c (n "cereal_macros") (v "0.2.0") (h "0wfn1cc52bi08w8am4n82qdrrbdslri1qfys3iq0pzf414z92klh")))

(define-public crate-cereal_macros-0.2.1 (c (n "cereal_macros") (v "0.2.1") (h "14qj5qfic6h5nhjdjpqxpg5j8fg2yrmp5z732k8fzwi01p9j0wn1")))

(define-public crate-cereal_macros-0.3.0 (c (n "cereal_macros") (v "0.3.0") (h "1dyblpf2n8wdd3x8qvv91gpfiwvayw78vxy8z4pszp693v6vbspj")))

(define-public crate-cereal_macros-0.3.1 (c (n "cereal_macros") (v "0.3.1") (h "0a4z74xdmhp1aj28z3bcj3ass7dnfld4g38qxm8jizidng143m1n")))

(define-public crate-cereal_macros-0.3.2 (c (n "cereal_macros") (v "0.3.2") (h "0j4sicabfw8p1bgp4g10lbqq78jwm6dg44v6ag1bkld9dnz867kd")))

(define-public crate-cereal_macros-0.3.3 (c (n "cereal_macros") (v "0.3.3") (h "1vn4ypxg3gavr7hkyh4683m6ry6pf9jxybmabmfz1l131n46nwmv")))

(define-public crate-cereal_macros-0.3.4 (c (n "cereal_macros") (v "0.3.4") (h "1cccxdckyjzqipq6xkiacv120sn0qkw3zmr5qv3svhvd7xvfw4xs")))

(define-public crate-cereal_macros-0.3.5 (c (n "cereal_macros") (v "0.3.5") (h "0iq0m6p6cjdgkaiyagnhq47apblbfqw55n75jyf34hp4ch9rnf4r")))

