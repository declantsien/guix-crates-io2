(define-module (crates-io ce re ceres-derive) #:use-module (crates-io))

(define-public crate-ceres-derive-0.1.0 (c (n "ceres-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19z0w6pbly90z610f18vbdams9fw4l260bs1myrwcn1mp0g91498")))

(define-public crate-ceres-derive-0.2.0 (c (n "ceres-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ivziv2bk16m14jh8685z6pf08bl2gyjmf6ycvrgw6crvnvrgf3x")))

