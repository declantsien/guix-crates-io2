(define-module (crates-io ce re cereal) #:use-module (crates-io))

(define-public crate-cereal-0.1.0 (c (n "cereal") (v "0.1.0") (d (list (d (n "cereal_macros") (r "^0.1.0") (d #t) (k 2)))) (h "0ylyyazxwac39sayras7c44kl41xl5l04id7jx1mkac3lql0aljh") (f (quote (("nightly"))))))

(define-public crate-cereal-0.1.1 (c (n "cereal") (v "0.1.1") (d (list (d (n "cereal_macros") (r "^0.1.0") (d #t) (k 2)))) (h "0nyk6b9x9r2c0zhvm8nrgm2kc93i3m7c49ravxiqhkq77n5gz7k9")))

(define-public crate-cereal-0.2.0 (c (n "cereal") (v "0.2.0") (d (list (d (n "cereal_macros") (r "^0.2.0") (d #t) (k 2)))) (h "0a4c0r4bzbb1g7qhnjqwhchmzw5aar5hvrkw5wibs8zb2xlr6bxh") (f (quote (("nightly"))))))

(define-public crate-cereal-0.2.1 (c (n "cereal") (v "0.2.1") (d (list (d (n "cereal_macros") (r "^0.2.0") (d #t) (k 2)))) (h "12i1c9zvcj20mrgj4h8mhybwdvc6771rgch3fw7k4wqi1csv2pn1") (f (quote (("nightly"))))))

(define-public crate-cereal-0.2.2 (c (n "cereal") (v "0.2.2") (d (list (d (n "cereal_macros") (r "^0.2.0") (d #t) (k 2)))) (h "1rw6arzwgdnbsnj4bvrcvjcfmqfwiqghsp6wk2nw93mbw1lmhzlc") (f (quote (("nightly"))))))

(define-public crate-cereal-0.3.0 (c (n "cereal") (v "0.3.0") (d (list (d (n "cereal_macros") (r "^0.3") (d #t) (k 2)))) (h "0wnkqc6jr7vldzr5x88lkylxnkli6h4cwx44zbdn6qdb2qwbw1i9") (f (quote (("nightly"))))))

(define-public crate-cereal-0.3.1 (c (n "cereal") (v "0.3.1") (d (list (d (n "cereal_macros") (r "^0.3") (d #t) (k 2)))) (h "16md5apsr64994q0z44ik72lwsp8zlj5gx1h6b7fwl8j7cwmia15") (f (quote (("nightly"))))))

