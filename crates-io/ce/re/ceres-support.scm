(define-module (crates-io ce re ceres-support) #:use-module (crates-io))

(define-public crate-ceres-support-0.2.0 (c (n "ceres-support") (v "0.2.0") (d (list (d (n "ceres-std") (r "^0") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (f (quote ("use_core"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "parity-wasm") (r "^0.41.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "08p8kdhqc6nlh3yklyb9sb0nasvxripvkmr9ng3mvwydn4gf3k91") (f (quote (("std" "ceres-std/std") ("default" "std"))))))

