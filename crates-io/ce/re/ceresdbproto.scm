(define-module (crates-io ce re ceresdbproto) #:use-module (crates-io))

(define-public crate-ceresdbproto-0.1.0 (c (n "ceresdbproto") (v "0.1.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "1f6fxf1l1qnic73gk4xl6i5yqhsw4bpj11mvi6q57nb5l7zi9wdm")))

(define-public crate-ceresdbproto-1.0.0 (c (n "ceresdbproto") (v "1.0.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "1w5m9klzwb4mc7cn4m5x4k75nxgjm69xpazj7namc944wmc4wyjn")))

(define-public crate-ceresdbproto-1.0.1 (c (n "ceresdbproto") (v "1.0.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "16jqq28hzyvfw2nl2gh526a7r22yiskjz17cr1g32b5jzvx6srcw")))

(define-public crate-ceresdbproto-1.0.2 (c (n "ceresdbproto") (v "1.0.2") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0s8yzf6jsarg2czhi3gs76lawv95b796gq70z377ssjvx9lf3q3n")))

(define-public crate-ceresdbproto-1.0.3 (c (n "ceresdbproto") (v "1.0.3") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0da71qmrh2l9rzqnb2c4bzk0cvmpl8hzkf0hjchkd8mb1h4iyva5")))

(define-public crate-ceresdbproto-1.0.4 (c (n "ceresdbproto") (v "1.0.4") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0sa8xrc8xs7cchy3kq2n69kg6s0qj9m3qq3iwq0ywkgq731d3m8q")))

(define-public crate-ceresdbproto-1.0.5 (c (n "ceresdbproto") (v "1.0.5") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0ykglqdv33hns8n1pwlhsmda8pkkqnv0py0fi3i2gc6j8sbwvzfb")))

(define-public crate-ceresdbproto-1.0.6 (c (n "ceresdbproto") (v "1.0.6") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0zld93h40xgcrszcgx4rcrw80wszs4fc0p3ngy733a5gx619w8l1")))

(define-public crate-ceresdbproto-1.0.7 (c (n "ceresdbproto") (v "1.0.7") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "1lq8igcw8c46b15p0mivq1i8x9j7ak42lailfzfj03rsp6arja7z")))

(define-public crate-ceresdbproto-1.0.8 (c (n "ceresdbproto") (v "1.0.8") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "1wixwyvpkglky2ngxmka4qzbwh46pg2db08y689qmn5cz4gz92zm")))

(define-public crate-ceresdbproto-1.0.9 (c (n "ceresdbproto") (v "1.0.9") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "1352x3dbby141mfz8dr18pdk2xh5smfabar6kc4b6cfrv54nx4i5")))

(define-public crate-ceresdbproto-1.0.10 (c (n "ceresdbproto") (v "1.0.10") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "1vlsai5icbkva25z01mx5pmmzk1ba3cl84vnkrl2ni9h5pbn32pa")))

(define-public crate-ceresdbproto-1.0.11 (c (n "ceresdbproto") (v "1.0.11") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "1hba95ccmq9571irxy88mjqkrlrwwl6s5bzh7n21wqsw14hp0fln")))

(define-public crate-ceresdbproto-1.0.12 (c (n "ceresdbproto") (v "1.0.12") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "04j3l0lwkkv9i0fc4dz3mglxr72b2xn0s6754dpz9bgzi2wkd3la")))

(define-public crate-ceresdbproto-1.0.13 (c (n "ceresdbproto") (v "1.0.13") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0976z7wb0hkivx724dhy3j2xs99gs19q0pq1yyfr05i9byhpc16p")))

(define-public crate-ceresdbproto-1.0.14 (c (n "ceresdbproto") (v "1.0.14") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "17an36r41am8ln65qawdvalsgk9jg6g1d6z5fb3rdq7v396rqgrr")))

(define-public crate-ceresdbproto-1.0.15 (c (n "ceresdbproto") (v "1.0.15") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "01yrnhgjb8c07667sp1smng804ak4kx7hyba636ccnf0z6q22dzx")))

(define-public crate-ceresdbproto-1.0.16 (c (n "ceresdbproto") (v "1.0.16") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0b6kn0v7y9wys9sjyffn1q6wrzq5pq4qv2qivrim4qii06ia1z1r")))

(define-public crate-ceresdbproto-1.0.17 (c (n "ceresdbproto") (v "1.0.17") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "1lgyppkx984mf1ydvw5qpifq4bnm0k6x4p73q8na56bb78ysmd0v")))

(define-public crate-ceresdbproto-1.0.18 (c (n "ceresdbproto") (v "1.0.18") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0xkjwm4cf7w9yfl9bn4mjp4l0vnzigifnzm7bdaf9kj0dvgrpjbz")))

(define-public crate-ceresdbproto-1.0.19 (c (n "ceresdbproto") (v "1.0.19") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "1sdrvmn6cqhf9n0xs8nvx9qvkdn8c29a8v98hcwfv8ayrr2s90fb")))

(define-public crate-ceresdbproto-1.0.20 (c (n "ceresdbproto") (v "1.0.20") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0bjxmfsxwq2hka5a0bgfpvifqh4x58b7jvskwwpysvy4lg10vk48")))

(define-public crate-ceresdbproto-1.0.21 (c (n "ceresdbproto") (v "1.0.21") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0jii7j1qrddrjr4p1h4qn30670ra0z9zrnmczhsmrcvn7x7w5rcw")))

(define-public crate-ceresdbproto-1.0.22 (c (n "ceresdbproto") (v "1.0.22") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "0vi37x6pj582gay9h52s363smp7qr14pv6s4lahkvzd5is79bgma")))

(define-public crate-ceresdbproto-1.0.23 (c (n "ceresdbproto") (v "1.0.23") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "protoc-bin-vendored") (r "^3.0.0") (d #t) (k 1)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "16fsf33x4j1qxp8x7qq6qfkzy872i3q6xpa29wkzjsmr1nll0ssn")))

