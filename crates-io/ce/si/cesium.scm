(define-module (crates-io ce si cesium) #:use-module (crates-io))

(define-public crate-cesium-0.1.0 (c (n "cesium") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "cursive") (r "^0.16.3") (d #t) (k 0)) (d (n "rust-utils") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "01dz67zjjn6wvn06cxvrj7vjbhjn70sqb5kzd6vrfiq7g3fdf1yy")))

(define-public crate-cesium-0.1.1 (c (n "cesium") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "cursive") (r "^0.16.3") (d #t) (k 0)) (d (n "rust-utils") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "077vbm4wygyby5yk4hqq86j3fjjb2sam75prf5q8ys2538wyalnw")))

(define-public crate-cesium-0.1.2 (c (n "cesium") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "cursive") (r "^0.16.3") (d #t) (k 0)) (d (n "rust-utils") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "056k5w6crf88km215pfa0qba8xgf0qshsicr4r19y7cwcy78rw4g")))

(define-public crate-cesium-0.2.0 (c (n "cesium") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "cursive") (r "^0.16.3") (f (quote ("toml"))) (d #t) (k 0)) (d (n "rust-utils") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0harcw4ngmfzsb2987kfqlqlr9w7fbvf1mspnc4x4r8pnc5p35pa")))

(define-public crate-cesium-0.2.1 (c (n "cesium") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "cursive") (r "^0.16.3") (f (quote ("toml"))) (d #t) (k 0)) (d (n "rust-utils") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0kc94bbm120fcp1ls5n9dg5ama5abzj24nxi6w7ksabr02x2yki3")))

