(define-module (crates-io ce si cesium-libmimalloc-sys) #:use-module (crates-io))

(define-public crate-cesium-libmimalloc-sys-2.1.2 (c (n "cesium-libmimalloc-sys") (v "2.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0l2vk3cj6y2b2s5krfmjnzlkq3nmb96ig8zg7b7f8if0g8i0926g") (f (quote (("secure") ("override") ("local_dynamic_tls") ("default") ("debug_in_debug") ("debug"))))))

