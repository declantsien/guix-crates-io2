(define-module (crates-io ce si cesium-allocator) #:use-module (crates-io))

(define-public crate-cesium-allocator-1.0.0 (c (n "cesium-allocator") (v "1.0.0") (d (list (d (n "cesium-libmimalloc-sys") (r "^2.1.2") (d #t) (k 0)))) (h "1qg3ma7qd98fqxanif0lwfia8yq20j4889q9rq0iy4qz6r30hy15")))

(define-public crate-cesium-allocator-1.0.1 (c (n "cesium-allocator") (v "1.0.1") (d (list (d (n "cesium-libmimalloc-sys") (r "^2.1.2") (d #t) (k 0)))) (h "1hm9amb8h75fp9yfvbl2nk9qy5f3wnymkdyw4qlpi4s5nqfkdp18")))

