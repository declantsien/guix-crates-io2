(define-module (crates-io ce l- cel-parser) #:use-module (crates-io))

(define-public crate-cel-parser-0.1.1 (c (n "cel-parser") (v "0.1.1") (d (list (d (n "lalrpop") (r "^0.19.1") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "081saihqv6a59p7f0gx5740f7c2vmvp2gb7yglprwlwvglb2jvpn")))

(define-public crate-cel-parser-0.1.3 (c (n "cel-parser") (v "0.1.3") (d (list (d (n "lalrpop") (r "^0.19.1") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "0lsb03ladylj8jz1wnq4l4xsb3hxjapip99gwyr3d4s05xpq0y27")))

(define-public crate-cel-parser-0.2.0 (c (n "cel-parser") (v "0.2.0") (d (list (d (n "lalrpop") (r "^0.19.1") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "1zjfbwjhpak5vsgcvnb9qwsn2hv72wrswifm2v519xmd98z4kvqx")))

(define-public crate-cel-parser-0.3.0 (c (n "cel-parser") (v "0.3.0") (d (list (d (n "lalrpop") (r "^0.19.1") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.1") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "0r06w8jxflx3mp3rrqi95m8mqxrm9b9hyj1q5449v1pz61w1gfv6")))

(define-public crate-cel-parser-0.4.0 (c (n "cel-parser") (v "0.4.0") (d (list (d (n "lalrpop") (r "^0.19.1") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.1") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "0xk75vdzb8qdzcj047v7wcci26nr7az95z1h87jsi6sciy16v02w") (y #t)))

(define-public crate-cel-parser-0.5.0 (c (n "cel-parser") (v "0.5.0") (d (list (d (n "lalrpop") (r "^0.19.1") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.1") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "0k8s38baqzv2i2n3998w39br76m8hi3bai83s3559zxzhvs8jhww")))

(define-public crate-cel-parser-0.5.1 (c (n "cel-parser") (v "0.5.1") (d (list (d (n "lalrpop") (r "^0.19.1") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.1") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "0mklryds8s5vaqrca8436g3ds4isc4n31v9gp3x7i5ra6b52f57m")))

(define-public crate-cel-parser-0.6.0 (c (n "cel-parser") (v "0.6.0") (d (list (d (n "lalrpop") (r "^0.19.1") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.1") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "17l7j1b3k8ib6yzj90ykyqsa5nvryb7w1j1j3hgmbg5b92hmpa5k")))

