(define-module (crates-io ce l- cel-rs) #:use-module (crates-io))

(define-public crate-cel-rs-0.1.0 (c (n "cel-rs") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.19.1") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.1") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)) (d (n "ordered_hash_map") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "1x0x0sg0qfa29nkz60sj8qljv5bi2xqcxlnywlnjk7cc4m4v3iha")))

(define-public crate-cel-rs-0.2.0 (c (n "cel-rs") (v "0.2.0") (d (list (d (n "lalrpop") (r "^0.19.1") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.1") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)) (d (n "ordered_hash_map") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "18h02cam57mycw1pjyixxkkzc2fbixjg4vyjyk9f6j0bynb73pkg")))

