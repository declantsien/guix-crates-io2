(define-module (crates-io ce l- cel-cli) #:use-module (crates-io))

(define-public crate-cel-cli-0.1.1 (c (n "cel-cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 0)) (d (n "cel-interpreter") (r "^0.1.1") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "rustyline") (r "^7.1.0") (d #t) (k 0)))) (h "0wxdn4cszns6fxplhl636p2fggh2d1xpxznfmapqw8pxwnv166pm")))

(define-public crate-cel-cli-0.2.0 (c (n "cel-cli") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 0)) (d (n "cel-interpreter") (r "^0.2.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "rustyline") (r "^7.1.0") (d #t) (k 0)))) (h "1dhxp21lzaxn6w4n8bgl73mhx9g4c4d0k0v85fqdbbn458dqsl52")))

