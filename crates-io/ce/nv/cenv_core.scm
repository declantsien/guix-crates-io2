(define-module (crates-io ce nv cenv_core) #:use-module (crates-io))

(define-public crate-cenv_core-0.1.0 (c (n "cenv_core") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1yb220qx66544a7rcrgnrl9h4snivjnpr0brcadrlxadhj5hawii")))

(define-public crate-cenv_core-0.2.0 (c (n "cenv_core") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0kh4zzgln0bfa749d5v2rhzazw9hvwsdxaiv6m9yqnzyac1x4ynm")))

(define-public crate-cenv_core-0.3.0 (c (n "cenv_core") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "044pq3if4diic8hm2an7gn77gj8pqlf0rqrqiaxdbk409jm9ga36")))

(define-public crate-cenv_core-0.3.1 (c (n "cenv_core") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)))) (h "0y79igwwdgl2y0an2n0sgssvrm5xns74v9fa0qix1hnyyz20v20l")))

(define-public crate-cenv_core-1.0.0 (c (n "cenv_core") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)))) (h "0drllh3vriyc247b8rkqvw87x8b01pyz0vqv83qfhmg5lf7krvdn")))

