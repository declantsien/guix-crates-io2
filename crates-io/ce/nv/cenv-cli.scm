(define-module (crates-io ce nv cenv-cli) #:use-module (crates-io))

(define-public crate-cenv-cli-0.1.0 (c (n "cenv-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "03m8dykdf1z217i46xj2hb9zlynx3hyhd08n1vvgj04sbs5lnps4")))

(define-public crate-cenv-cli-0.1.1 (c (n "cenv-cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "1paidn2r2jp7ky1qrbc43g88a08g6v85kycnmk6y31h0jh3gasxb")))

(define-public crate-cenv-cli-0.1.2 (c (n "cenv-cli") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jid0k3kzjr2j7rs6j19lpq2mhv7cb5q44wfqw6f3sdv9yn766a2")))

