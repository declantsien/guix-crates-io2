(define-module (crates-io ce ss cess) #:use-module (crates-io))

(define-public crate-cess-0.1.0 (c (n "cess") (v "0.1.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "yew") (r "^0.16.2") (d #t) (k 0)))) (h "09kv6r3prn7s4vbf1mazy0mv1b58ssil7a2kr4jbs0ml6rdiv0vd") (f (quote (("default" "console_error_panic_hook"))))))

