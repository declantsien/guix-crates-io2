(define-module (crates-io ce pl ceplugin) #:use-module (crates-io))

(define-public crate-ceplugin-0.5.0 (c (n "ceplugin") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "0zk3zsnhkvpb4i3mzzmd3k6b3wwx5zzv008gzmh2fxqykgyrppy2")))

(define-public crate-ceplugin-0.5.1 (c (n "ceplugin") (v "0.5.1") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "0rna1mcd7m1szmm5xxrnbk3l0gz9ipym4bmr9nkvzi4vdkab9m5b")))

(define-public crate-ceplugin-0.6.0 (c (n "ceplugin") (v "0.6.0") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (k 0)))) (h "10vmrpy9qvc2178n338ngg861f9byglzx7n3pj8a5p46ksr49y6m")))

