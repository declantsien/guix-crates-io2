(define-module (crates-io ce x_ cex_trading) #:use-module (crates-io))

(define-public crate-cex_trading-0.1.0 (c (n "cex_trading") (v "0.1.0") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "ring") (r "^0.14.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.93") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.15.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 2)))) (h "0adnnap5xzny6s9vxcni5wp9rm3chg8d7b8vkmd8ca0ghq3vmiqf") (y #t)))

(define-public crate-cex_trading-0.1.1 (c (n "cex_trading") (v "0.1.1") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "ring") (r "^0.14.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum_macros") (r "^0.15.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 2)))) (h "1prd83fhnwjgd3zkcv687z0fhz3m8j7499bcrk8zlslam64q2lx7") (y #t)))

