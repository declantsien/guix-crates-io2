(define-module (crates-io ce x_ cex_derive) #:use-module (crates-io))

(define-public crate-cex_derive-0.0.1 (c (n "cex_derive") (v "0.0.1") (h "071pq97fg20wxzv4b40x93fnxvkz79y237p0rj5bzjhviwgv0x4d")))

(define-public crate-cex_derive-0.1.0 (c (n "cex_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "19b158qi0s19jljknyvjdwdqlfdhp3069g3f9xwzd9q2hp3m67ni")))

(define-public crate-cex_derive-0.1.1 (c (n "cex_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "13z1kdxbs8wgqq293ly46x8b7l2ax69r1nxx4fwa6qs398kf8rwz")))

(define-public crate-cex_derive-0.2.1 (c (n "cex_derive") (v "0.2.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "1d0gx4jp24pzaaanj526gaz3ryvppx31xkpgb6c4i9fw81cyg9nd")))

(define-public crate-cex_derive-0.3.0-alpha (c (n "cex_derive") (v "0.3.0-alpha") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "109cpr4kkfirg9lhd8a5m3fm91hy69wlb8nqxql263ahfi8cl6vk")))

(define-public crate-cex_derive-0.3.0 (c (n "cex_derive") (v "0.3.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1w7p7i60gpjbvdxpr07qvgs0l8b25m5c6lwbb501059xv2f9rqnr")))

(define-public crate-cex_derive-0.4.0 (c (n "cex_derive") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)))) (h "0y8swaz2r96srr71pnd4vsgkfrjawrngaq9iv39wpfj1ha9nahvz")))

(define-public crate-cex_derive-0.5.0 (c (n "cex_derive") (v "0.5.0") (d (list (d (n "indexmap") (r "^1.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)))) (h "1iiinyb89iakhxa62b89l9xn307xfgggjhn9rsixlyl29i0cjzyz")))

(define-public crate-cex_derive-0.5.1 (c (n "cex_derive") (v "0.5.1") (d (list (d (n "indexmap") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)))) (h "1dar6r60y2xzqgwpf2rmg01khg550xzckni86hsppd2n46wafkcq")))

(define-public crate-cex_derive-0.5.2 (c (n "cex_derive") (v "0.5.2") (d (list (d (n "indexmap") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)))) (h "13ckdkbvgw5qz05av2nwj9bh14snz7ck7yk0k5rjwzbdcp6lhl2v")))

