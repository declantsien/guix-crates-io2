(define-module (crates-io ce su cesu8-str) #:use-module (crates-io))

(define-public crate-cesu8-str-1.0.0 (c (n "cesu8-str") (v "1.0.0") (h "06m107zkv3y3p64y2pz7bqqqc3kkdjygbkk9jdiz5qf18xda5vnh") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-cesu8-str-1.0.1 (c (n "cesu8-str") (v "1.0.1") (h "1g3z8m0and1yj97x0akmvvjqiwgwqa1k8ghsvc671ynha30qbv8v") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-cesu8-str-1.0.2 (c (n "cesu8-str") (v "1.0.2") (h "0vl8jpczfzywsif0frqy7bfgvc9y8jnl0b808kx13nvj1qcd5y7c") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-cesu8-str-1.1.0 (c (n "cesu8-str") (v "1.1.0") (h "01wlyr3pl2ml1k9r9ll8kf9bnh9788vs6h70pwaqz3ai074jkdxj") (f (quote (("default" "alloc") ("alloc"))))))

