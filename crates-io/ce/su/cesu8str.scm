(define-module (crates-io ce su cesu8str) #:use-module (crates-io))

(define-public crate-cesu8str-0.1.1 (c (n "cesu8str") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.24") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0s590l7jvs2560i8pl6m15dpg6c9nznfsl4rq0cnx9srfs6zn678") (f (quote (("validate-release") ("unstable") ("build-binary" "clap"))))))

(define-public crate-cesu8str-0.2.1 (c (n "cesu8str") (v "0.2.1") (d (list (d (n "clap") (r "^4.0.24") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 2)) (d (n "paste") (r "^1.0.11") (d #t) (k 2)) (d (n "rusty-fork") (r "^0.3.0") (d #t) (k 2)))) (h "0lx0hp92rszam6z2vd76n9bwwrbk701274jjy2iwk6lhwv0wnj7l") (f (quote (("validate-release") ("unstable") ("build-binary" "clap"))))))

(define-public crate-cesu8str-0.2.3 (c (n "cesu8str") (v "0.2.3") (d (list (d (n "clap") (r "^4.0.24") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 2)) (d (n "paste") (r "^1.0.11") (d #t) (k 2)) (d (n "rusty-fork") (r "^0.3.0") (d #t) (k 2)))) (h "1r9rvys55682rfw8mq0jf05rl9ms9dbr80cmm7fyf18jhiqax1nb") (f (quote (("validate-release") ("unstable") ("build-binary" "clap"))))))

(define-public crate-cesu8str-0.2.4 (c (n "cesu8str") (v "0.2.4") (d (list (d (n "clap") (r "^4.0.24") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 2)) (d (n "paste") (r "^1.0.11") (d #t) (k 2)) (d (n "rusty-fork") (r "^0.3.0") (d #t) (k 2)))) (h "1hg3jvp6s6sgyh9255xj5qivghllsb3dl4di4hx7gx629czp3jj1") (f (quote (("validate-release") ("unstable") ("build-binary" "clap"))))))

