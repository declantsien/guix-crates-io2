(define-module (crates-io ce su cesu8) #:use-module (crates-io))

(define-public crate-cesu8-0.0.1 (c (n "cesu8") (v "0.0.1") (h "1faiafsvfjznj5xq7d50xvbz2pb25nhlchx7813ch2s9fpq0y4gd")))

(define-public crate-cesu8-0.0.2 (c (n "cesu8") (v "0.0.2") (h "0pb49j6xvynwinz362vjgvpbp4msynli8i5bh28rdaq5kmnf397g")))

(define-public crate-cesu8-0.0.3 (c (n "cesu8") (v "0.0.3") (h "17djfhxif81ghc96kg7s1z1bkn4xwf4n88hgyqwabrmm3bfyrxac")))

(define-public crate-cesu8-0.0.4 (c (n "cesu8") (v "0.0.4") (h "1vmicn90jyjki1qwnwf30qr27vgj2c0gai66g3qayldfvplswwas")))

(define-public crate-cesu8-0.0.5 (c (n "cesu8") (v "0.0.5") (h "07nwmhxhhjbjaxlx61fxzfj35fqx2kzxakifvqqwgww1653bizyw")))

(define-public crate-cesu8-0.0.6 (c (n "cesu8") (v "0.0.6") (h "1af42jbk3xfhwgb5wny2fzgp1m6g0bvyzh10y5rkyg0hfm7qxdib")))

(define-public crate-cesu8-0.0.7 (c (n "cesu8") (v "0.0.7") (h "17rdcwahircz8lcs1sv3i40ijs70djrnsyaymqf42j3sp4wamspm")))

(define-public crate-cesu8-0.0.8 (c (n "cesu8") (v "0.0.8") (h "01hnqdzvz7lz1ma738hm18ck79myqcfwacw092wl43ig04s0zlsi")))

(define-public crate-cesu8-0.0.9 (c (n "cesu8") (v "0.0.9") (h "1q0yhq2msjn73xj42f665c1yp7qqbwimgfkbbhp4jz663xrnqvh2")))

(define-public crate-cesu8-1.0.0 (c (n "cesu8") (v "1.0.0") (h "09mykdyym4a4v9f6qg6blhlar89fq47d6zrkdwzw364mr4mki7cj") (f (quote (("unstable"))))))

(define-public crate-cesu8-1.1.0 (c (n "cesu8") (v "1.1.0") (h "0g6q58wa7khxrxcxgnqyi9s1z2cjywwwd3hzr5c55wskhx6s0hvd") (f (quote (("unstable"))))))

