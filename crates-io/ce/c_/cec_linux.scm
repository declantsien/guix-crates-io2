(define-module (crates-io ce c_ cec_linux) #:use-module (crates-io))

(define-public crate-cec_linux-0.1.0 (c (n "cec_linux") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "num_enum") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^1.34") (o #t) (d #t) (k 0)))) (h "0j547kylmpmc2hnqxs6fahdvid1amxyh1sr5xkg6lgvfzcgrh1s3") (f (quote (("tokio" "tokio/net") ("poll" "nix/poll"))))))

(define-public crate-cec_linux-0.1.1 (c (n "cec_linux") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "num_enum") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^1.34") (o #t) (d #t) (k 0)))) (h "0qdsg9y33j7gjnax9n533jqkb76k673w14cank8ss5cx5csk86z1") (f (quote (("tokio" "tokio/net") ("poll" "nix/poll"))))))

(define-public crate-cec_linux-0.2.0 (c (n "cec_linux") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "nix") (r "^0.28") (f (quote ("ioctl"))) (d #t) (k 0)) (d (n "num_enum") (r "^0.7") (d #t) (k 0)) (d (n "tokio") (r "^1.34") (o #t) (d #t) (k 0)))) (h "0ndv4ry5p7y0rd57sqys92xklh84d51w2wyca8g61753akg61f9l") (f (quote (("tokio" "tokio/net") ("poll" "nix/poll"))))))

