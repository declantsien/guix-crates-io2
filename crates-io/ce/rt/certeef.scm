(define-module (crates-io ce rt certeef) #:use-module (crates-io))

(define-public crate-certeef-0.1.0 (c (n "certeef") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1kg4q2iyjcirgl0cddcj7pv9wmwkbnkqb08mvxd7f50v3f0mhfcj")))

(define-public crate-certeef-0.1.1 (c (n "certeef") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wvmnhnncx3x89nfm7wwfmw62hkl3ic5nirq1w4bf2x43np39663")))

(define-public crate-certeef-0.1.2 (c (n "certeef") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "004afjdky91h3ll03j2gn8jwbg87hw9xvslgkg11v29p3fiwm77x")))

(define-public crate-certeef-0.1.3 (c (n "certeef") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hf4cvy0r0x2kq33q7gcsd383n8m52gsxsz97rlmnliq2i65lyg1")))

