(define-module (crates-io ce rt certinfo) #:use-module (crates-io))

(define-public crate-certinfo-0.1.0 (c (n "certinfo") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rustls") (r "^0.21.0") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "x509-parser") (r "^0.14.0") (d #t) (k 0)))) (h "1zpjxxknj9gkpnqwz9q18pqwmmbxhhcbgn8kdq7zpj29vah3yxw2")))

