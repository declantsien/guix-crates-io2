(define-module (crates-io ce rt cert-manager-crds) #:use-module (crates-io))

(define-public crate-cert-manager-crds-0.1.0 (c (n "cert-manager-crds") (v "0.1.0") (d (list (d (n "k8s-openapi") (r "^0.15.0") (f (quote ("v1_20"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1j4rg2c52b2p0vspq8qslblyp6clbi4jr024drr05nqhj9sq1k25")))

