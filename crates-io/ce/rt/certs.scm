(define-module (crates-io ce rt certs) #:use-module (crates-io))

(define-public crate-certs-0.1.0-alpha1 (c (n "certs") (v "0.1.0-alpha1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "x509-parser") (r "^0.15.0") (d #t) (k 0)))) (h "0x5z121hgqpaygc9yyaq2fnfc3wpy99ksk0wkvgfvn30hw75n3h8")))

(define-public crate-certs-0.1.0-alpha2 (c (n "certs") (v "0.1.0-alpha2") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "x509-parser") (r "^0.15.0") (d #t) (k 0)))) (h "19gfw569xsbw4nbyb1pg33wz2fp9hlv5aha1vvsvwnf4lg3xyr9s")))

