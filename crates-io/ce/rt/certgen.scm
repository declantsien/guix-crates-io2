(define-module (crates-io ce rt certgen) #:use-module (crates-io))

(define-public crate-certgen-0.1.0 (c (n "certgen") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)) (d (n "p12") (r "^0.1.0") (d #t) (k 0)) (d (n "rcgen") (r "^0.8.1") (f (quote ("x509-parser"))) (d #t) (k 0)))) (h "12fm6phl72jzy55fnrlwbi1by9z312fyfwp53q510rbhz04gi8ma") (f (quote (("default" "clap")))) (y #t)))

(define-public crate-certgen-0.1.1 (c (n "certgen") (v "0.1.1") (d (list (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)) (d (n "p12") (r "^0.1.2") (d #t) (k 0)) (d (n "pem") (r "^0.7.0") (d #t) (k 0)) (d (n "rcgen") (r "^0.8.1") (f (quote ("x509-parser"))) (d #t) (k 0)) (d (n "yasna") (r "^0.3.1") (f (quote ("bit-vec"))) (d #t) (k 0)))) (h "102ih618qcaz054lrvx3j58f09b9zzi8k8pisckfqk7fz1s7fgkk") (f (quote (("default" "clap")))) (y #t)))

(define-public crate-certgen-0.1.2 (c (n "certgen") (v "0.1.2") (d (list (d (n "bit-vec") (r "^0.6.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)) (d (n "p12") (r "^0.1.2") (d #t) (k 0)) (d (n "pem") (r "^0.7.0") (d #t) (k 0)) (d (n "rcgen") (r "^0.8.1") (f (quote ("x509-parser"))) (d #t) (k 0)) (d (n "yasna") (r "^0.3.1") (f (quote ("bit-vec"))) (d #t) (k 0)))) (h "08ryr86rdfp8y8a4r2wmhz8bl3ap2ka4vv4534cg3b2nlp4nr5zd") (f (quote (("default" "clap"))))))

(define-public crate-certgen-0.1.3 (c (n "certgen") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)) (d (n "p12") (r "^0.1.3") (d #t) (k 0)) (d (n "pem") (r "^0.7.0") (d #t) (k 0)) (d (n "rcgen") (r "^0.8.1") (f (quote ("x509-parser"))) (d #t) (k 0)))) (h "0m5kn16zifg92lqcajnnnslzyxlqrcih3gnharfxi82ww5f2g8ci") (f (quote (("default" "clap"))))))

