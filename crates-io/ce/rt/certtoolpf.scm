(define-module (crates-io ce rt certtoolpf) #:use-module (crates-io))

(define-public crate-certtoolpf-0.1.0 (c (n "certtoolpf") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "inquire") (r "^0.6.1") (d #t) (k 0)) (d (n "libepf") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1khpi7w73cwvgjc3lzcscviry74h7zmhm60j0mrmmivhxv4mvsnj")))

