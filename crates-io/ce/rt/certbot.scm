(define-module (crates-io ce rt certbot) #:use-module (crates-io))

(define-public crate-certbot-0.0.0 (c (n "certbot") (v "0.0.0") (h "1gj5d4anib6v7v0nqwj5p0smnybcmyx3v7y3nr45r86q87pj423a") (y #t)))

(define-public crate-certbot-0.1.0 (c (n "certbot") (v "0.1.0") (h "0ml726acq28p10mjpsgjqi432fg873nqmi3hw9ij2z3zc80an74c") (r "1.56")))

