(define-module (crates-io ce rt cert-gen) #:use-module (crates-io))

(define-public crate-cert-gen-0.1.0 (c (n "cert-gen") (v "0.1.0") (d (list (d (n "openssl") (r "^0.10.48") (d #t) (k 0)) (d (n "pem") (r "^2.0.0") (d #t) (k 0)) (d (n "rcgen") (r "^0.10.0") (f (quote ("pem" "x509-parser"))) (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "rustls") (r "^0.21.0") (d #t) (k 0)) (d (n "rustls-pemfile") (r "^1.0.2") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (d #t) (k 0)) (d (n "x509") (r "^0.2.0") (d #t) (k 0)))) (h "15ir3r2kj8k5qy4a9f73dfbznk436yazx4j753zbl7djyzrq1j57")))

