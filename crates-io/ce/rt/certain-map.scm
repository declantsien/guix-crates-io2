(define-module (crates-io ce rt certain-map) #:use-module (crates-io))

(define-public crate-certain-map-0.1.0 (c (n "certain-map") (v "0.1.0") (d (list (d (n "certain-map-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "service-async") (r "^0.1") (d #t) (k 0)))) (h "17f3nvbw9ydifib24ddcrbcg65393hj5ibnyxjmir5jpbyd7317v")))

(define-public crate-certain-map-0.2.0 (c (n "certain-map") (v "0.2.0") (d (list (d (n "certain-map-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "service-async") (r "^0.1") (d #t) (k 0)))) (h "0jn2mjz9r2aiswdyfhqx2sffghnzxipqkdwxdmfwf7rqjh1852mx")))

(define-public crate-certain-map-0.2.1 (c (n "certain-map") (v "0.2.1") (d (list (d (n "certain-map-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "service-async") (r "^0.1.4") (d #t) (k 0)))) (h "08aavbm1mqifhdxb8fv1zd8f4ivm2c2jpnqg8iqjnr650jhhz9b8")))

(define-public crate-certain-map-0.2.2 (c (n "certain-map") (v "0.2.2") (d (list (d (n "certain-map-macros") (r "^0.2.2") (d #t) (k 0)) (d (n "service-async") (r "^0.1.4") (d #t) (k 0)))) (h "07cplza9vfs2wasrpcnmhwym16gsfp03qy4p7ikfih00y7g4yskx")))

(define-public crate-certain-map-0.2.3 (c (n "certain-map") (v "0.2.3") (d (list (d (n "certain-map-macros") (r "^0.2.2") (d #t) (k 0)) (d (n "param") (r "^0.1.0") (d #t) (k 0)))) (h "07a6hzd1fgqb2d8swb8g91l9jjkr7dmpabfz4ar9sr7bp4z7kd3x")))

(define-public crate-certain-map-0.2.4 (c (n "certain-map") (v "0.2.4") (d (list (d (n "certain-map-macros") (r "^0.2.3") (d #t) (k 0)) (d (n "param") (r "^0.1.0") (d #t) (k 0)))) (h "1xhyfzdkskblspbsxaz08gzg4l9r4vni1ag6b86nq91hwgfczy7f")))

