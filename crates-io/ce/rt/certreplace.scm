(define-module (crates-io ce rt certreplace) #:use-module (crates-io))

(define-public crate-certreplace-0.1.0 (c (n "certreplace") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0qxnfc7ywra6rf73q3jcvmim9fsc94cdi1nyjhbkn6mx9dqg2bsi")))

(define-public crate-certreplace-0.1.1 (c (n "certreplace") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0x71jgc0gvnabkbsyzrbw7543dpyyn9y8vldjz2br81807gjy74f")))

(define-public crate-certreplace-0.1.2 (c (n "certreplace") (v "0.1.2") (d (list (d (n "jwalk") (r "^0.6.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "time") (r "^0.3.17") (f (quote ("formatting"))) (d #t) (k 0)))) (h "1bplq8qm4sybm7c37v2q47fnyypy6mx6wa6acs025ykppl4qh7an")))

(define-public crate-certreplace-0.1.3 (c (n "certreplace") (v "0.1.3") (d (list (d (n "jwalk") (r "^0.6.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.54") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)) (d (n "time") (r "^0.3.17") (f (quote ("formatting"))) (d #t) (k 0)))) (h "174yrzwicfmywjn47sfpkimnz9kki1pl236dvpvvlsncc8kxlpah")))

