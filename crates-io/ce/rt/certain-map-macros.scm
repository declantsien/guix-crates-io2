(define-module (crates-io ce rt certain-map-macros) #:use-module (crates-io))

(define-public crate-certain-map-macros-0.1.0 (c (n "certain-map-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0sskcvysrkfby7lgp5yyqq9w83lflgb6mkjcl0yxnfymshbxsnny")))

(define-public crate-certain-map-macros-0.2.0 (c (n "certain-map-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1avfnaczh9w9fyclfa9dagbf93bgq721hqys29kkir1wh7gkb4jd")))

(define-public crate-certain-map-macros-0.2.1 (c (n "certain-map-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wdvpsycqm5qh8kffd2gp2hp6ayiyib8ph6agm1z664w3fpk0v69")))

(define-public crate-certain-map-macros-0.2.2 (c (n "certain-map-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1s378pw33fi88g1nk3dg4vlgzc4a91yir7w9bbg7v21r1269bmz6")))

(define-public crate-certain-map-macros-0.2.3 (c (n "certain-map-macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0b703dxl07lsw6dg3w0hnch8srzpmwg7m30jmsdcscxg3ncygs47")))

