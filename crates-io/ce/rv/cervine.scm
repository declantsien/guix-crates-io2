(define-module (crates-io ce rv cervine) #:use-module (crates-io))

(define-public crate-cervine-0.0.1 (c (n "cervine") (v "0.0.1") (h "0z7s1nd1mgxgl3dl9zyggv0pmcrvj4lr6xryqk1arh7xlx6zr5xz")))

(define-public crate-cervine-0.0.2 (c (n "cervine") (v "0.0.2") (h "03bjfmfpnlxsqqi57ilxz3m956ksadpbhwzi10k0pwhgp6lmkcif")))

(define-public crate-cervine-0.0.3 (c (n "cervine") (v "0.0.3") (d (list (d (n "cargo-husky") (r "^1.5.0") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "git_info") (r "^0.1.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.115") (o #t) (k 0)) (d (n "smartstring") (r "^0.2.3") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "0znlc9zih93m4s1b975viqndjygi486iy2af8kg6q53ri96279v1")))

(define-public crate-cervine-0.0.4 (c (n "cervine") (v "0.0.4") (d (list (d (n "cargo-husky") (r "^1.5.0") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "git_info") (r "^0.1.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.115") (o #t) (k 0)) (d (n "smartstring") (r "^0.2.3") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "0finz94ab0hb3qm9znwl5627ylqq1x83iyff5yp5qi6qvfy4b2v4")))

(define-public crate-cervine-0.0.5 (c (n "cervine") (v "0.0.5") (d (list (d (n "cargo-husky") (r "^1.5.0") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "git_info") (r "^0.1.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.115") (o #t) (k 0)) (d (n "smartstring") (r "^0.2.3") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "0gk30n2h853lwkxr78r2hlgzq7hki7nyfphsc92mxm16vav5s8b8")))

(define-public crate-cervine-0.0.6 (c (n "cervine") (v "0.0.6") (d (list (d (n "cargo-husky") (r "^1.5.0") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "git_info") (r "^0.1.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.115") (o #t) (k 0)) (d (n "smartstring") (r "^0.2.3") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "1ddxqx3382k950yj2i83xxfv8cnm89xk4dpis9izq17g6jcbh3cz")))

