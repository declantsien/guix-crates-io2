(define-module (crates-io ce rv cervisia) #:use-module (crates-io))

(define-public crate-cervisia-0.0.1 (c (n "cervisia") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "closet") (r "^0.2.2") (d #t) (k 0)) (d (n "gio") (r "^0.2.0") (f (quote ("v2_40"))) (k 0)) (d (n "glib") (r "^0.3.1") (d #t) (k 0)) (d (n "gtk") (r "^0.2.0") (f (quote ("v3_10"))) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustix-bl") (r "^0.3") (d #t) (k 0)) (d (n "suffix-rs") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "07z04jnj0hk1k9yiykcrf3il6h62q9vydz8bk4mdbn24m8sk11rz")))

