(define-module (crates-io ce rv cervus) #:use-module (crates-io))

(define-public crate-cervus-0.1.0 (c (n "cervus") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^38.0") (d #t) (k 0)))) (h "0wijldnx63wb7bkfxzmnjxrjcay48dca72n399fhxdi7wfcch7fz")))

(define-public crate-cervus-0.2.0 (c (n "cervus") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^38.0") (d #t) (k 0)))) (h "1cdglc6l8a7lnf0c0klqkwq3m7l0ljbck0kz8hipsak5qkvwzkxr")))

(define-public crate-cervus-0.2.1 (c (n "cervus") (v "0.2.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^38.0") (d #t) (k 0)))) (h "183qx3ff06n9hfcrm1z1xcvxmv25r5za4av6vqzdi48036qd98y1")))

(define-public crate-cervus-0.2.2 (c (n "cervus") (v "0.2.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^38.0") (d #t) (k 0)))) (h "0dab12wm271agfxr58isgl23l0szkqmk0v5lnnlhsw3al8pb1nca")))

(define-public crate-cervus-0.2.3 (c (n "cervus") (v "0.2.3") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^38.0") (d #t) (k 0)))) (h "0425rmygagwn21qkx68bg0yz7qjmanqlbsjjbwx2irz7l4sr5l3y")))

(define-public crate-cervus-0.3.0 (c (n "cervus") (v "0.3.0") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^38.0") (d #t) (k 0)))) (h "063g2f9cirp4r3iq8spian1mbgrqmck0nix3yjka6zwmxjjyfvdy")))

(define-public crate-cervus-0.3.1 (c (n "cervus") (v "0.3.1") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^38.0") (d #t) (k 0)))) (h "1vkha75dwxv28pf8l8w6r0gxv321vrvwp38s7frasfd800zf7phi")))

(define-public crate-cervus-0.3.2 (c (n "cervus") (v "0.3.2") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^38.0") (d #t) (k 0)))) (h "0xqjyawkqh7c26dx1hs1dd209b2kam3cqwbz5whygj450h5zj99j")))

(define-public crate-cervus-0.3.3 (c (n "cervus") (v "0.3.3") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^38.0") (d #t) (k 0)))) (h "1qgpql6i1d4nvq8lvs20dpy7ky30b8p1iaiaaphz1zdcqmbyaqi5")))

(define-public crate-cervus-0.3.4 (c (n "cervus") (v "0.3.4") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^38.0") (d #t) (k 0)))) (h "0wd2szl9r53s5qz99vqrq4s8h6kgp9rmldb17bikpa941lybaz44")))

(define-public crate-cervus-0.3.5 (c (n "cervus") (v "0.3.5") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^38.0") (d #t) (k 0)))) (h "1rdhlldpqmc1dbzl8ss24b1gd95x6larjzw500mjq37ivzdvfqj9")))

(define-public crate-cervus-0.3.6 (c (n "cervus") (v "0.3.6") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^38.0") (d #t) (k 0)))) (h "0hwvnxfybqk0i3jw60l5r28sh4abyr3lnwfdassgglzsd6m7hv1a")))

(define-public crate-cervus-0.4.0 (c (n "cervus") (v "0.4.0") (d (list (d (n "base64") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "llvm-sys") (r "^38.0") (d #t) (k 0)))) (h "1fwh26k4jara3rr5x02bz6d0hvx166s68bylxqqfz7xrr083bmfk")))

(define-public crate-cervus-0.4.99 (c (n "cervus") (v "0.4.99") (h "0klrhab9ba4k3sf8d6nff3iykgmh2x3qy49vzy6n0px9k0r63sc1")))

