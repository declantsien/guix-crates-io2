(define-module (crates-io ce rv cervo-onnx) #:use-module (crates-io))

(define-public crate-cervo-onnx-0.1.0 (c (n "cervo-onnx") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo-core") (r "^0.1.0") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.16.7") (d #t) (k 0)) (d (n "tract-onnx") (r "^0.16.7") (d #t) (k 0)))) (h "0h4zrcva90gvmj7dj4z7iqd9c19xq6n9fcnqw6xmj93ml73f2243")))

(define-public crate-cervo-onnx-0.2.0 (c (n "cervo-onnx") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo-core") (r "^0.2.0") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.17.1") (d #t) (k 0)) (d (n "tract-onnx") (r "^0.17.1") (d #t) (k 0)))) (h "1r5dw7hnv0rgpc1ljg2c5lp1cafjs0gpbs4kd11vbgjzi49lfay6")))

(define-public crate-cervo-onnx-0.3.0 (c (n "cervo-onnx") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo-core") (r "^0.3.0") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.17.1") (d #t) (k 0)) (d (n "tract-onnx") (r "^0.17.1") (d #t) (k 0)))) (h "02mn7c0x2x8ddx9jzlwhsmmpsrn1w605xj56911sgcvwc518g7dh")))

(define-public crate-cervo-onnx-0.4.0 (c (n "cervo-onnx") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo-core") (r "^0.4.0") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.17.1") (d #t) (k 0)) (d (n "tract-onnx") (r "^0.17.1") (d #t) (k 0)))) (h "1iq2xshc1n3qzh6q3frbda3cqb2c2skfbl1sp5md6aci1j1kk6yp")))

(define-public crate-cervo-onnx-0.5.0-rc.0 (c (n "cervo-onnx") (v "0.5.0-rc.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo-core") (r "^0.5.0-rc.0") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.20") (d #t) (k 0)) (d (n "tract-onnx") (r "^0.20") (d #t) (k 0)))) (h "0gyrqdv5rh474k92jmjpfj9g8wm28l4b77z8vqsiyhli0psfxbsy")))

(define-public crate-cervo-onnx-0.5.0 (c (n "cervo-onnx") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo-core") (r "^0.5.0") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.20") (d #t) (k 0)) (d (n "tract-onnx") (r "^0.20") (d #t) (k 0)))) (h "1zv12rwndcaakfx06dlvnw8nb5qpcvh7nq9wgspzqgpvkfdqx2qa")))

(define-public crate-cervo-onnx-0.5.1 (c (n "cervo-onnx") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo-core") (r "^0.5.1") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.20") (d #t) (k 0)) (d (n "tract-onnx") (r "^0.20") (d #t) (k 0)))) (h "1haymcpp85z3wll06idw1rxq2yw3hknybdm1m2j9ln0mzsvw7zbb")))

(define-public crate-cervo-onnx-0.6.0 (c (n "cervo-onnx") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo-core") (r "^0.6.0") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.21") (d #t) (k 0)) (d (n "tract-onnx") (r "^0.21") (d #t) (k 0)))) (h "19pgp7vxnazvdv17h9cby54ywdgxpww2h3pnr0wpwi97f3wg02b9")))

