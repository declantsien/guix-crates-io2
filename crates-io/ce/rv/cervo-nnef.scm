(define-module (crates-io ce rv cervo-nnef) #:use-module (crates-io))

(define-public crate-cervo-nnef-0.1.0 (c (n "cervo-nnef") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo-core") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.10") (d #t) (k 0)) (d (n "tract-hir") (r "^0.16.7") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.16.7") (d #t) (k 0)) (d (n "tract-onnx") (r "^0.16.7") (d #t) (k 0)))) (h "1i4i4kzidarn4i42pld7h4zz3a4l9rh6rnk6z0rggzm5sd3a7zq4")))

(define-public crate-cervo-nnef-0.2.0 (c (n "cervo-nnef") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo-core") (r "^0.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tract-hir") (r "^0.17.1") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.17.1") (d #t) (k 0)) (d (n "tract-onnx") (r "^0.17.1") (d #t) (k 0)))) (h "0rbxdqwl06hmnmd1yr8xsv56nyd41p558lvg2n7fdigci31xj5ik")))

(define-public crate-cervo-nnef-0.3.0 (c (n "cervo-nnef") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo-core") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tract-hir") (r "^0.17.1") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.17.1") (d #t) (k 0)) (d (n "tract-onnx") (r "^0.17.1") (d #t) (k 0)))) (h "18qa80h97pcfzmkfqgmn6j8waq2fwp51infx3qm5vcl1c6jm6x0z")))

(define-public crate-cervo-nnef-0.4.0 (c (n "cervo-nnef") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo-core") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tract-hir") (r "^0.17.1") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.17.1") (d #t) (k 0)) (d (n "tract-onnx") (r "^0.17.1") (d #t) (k 0)))) (h "1v2xxmnxqsvd5hs85j1xk3m6kpg8xb24lrbjjgjymk956l9yipjv")))

(define-public crate-cervo-nnef-0.5.0-rc.0 (c (n "cervo-nnef") (v "0.5.0-rc.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo-core") (r "^0.5.0-rc.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tract-hir") (r "^0.20") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.20") (d #t) (k 0)) (d (n "tract-onnx") (r "^0.20") (d #t) (k 0)))) (h "1alaicy1v2v9101anhnq2h7m5dz5cnr2dnf6jdz18h9q837c6j9q")))

(define-public crate-cervo-nnef-0.5.0 (c (n "cervo-nnef") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo-core") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tract-hir") (r "^0.20") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.20") (d #t) (k 0)) (d (n "tract-onnx") (r "^0.20") (d #t) (k 0)))) (h "0wlijvsqvrzgxhlhnb85ad1i34wn2x9k2v6p8a9v9v43qj2ykxcx")))

(define-public crate-cervo-nnef-0.5.1 (c (n "cervo-nnef") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo-core") (r "^0.5.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tract-hir") (r "^0.20") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.20") (d #t) (k 0)) (d (n "tract-onnx") (r "^0.20") (d #t) (k 0)))) (h "08qrqh2p63gagvs3csvivqra7gg6rqic70nkh4y6s602jx91a7wa")))

(define-public crate-cervo-nnef-0.6.0 (c (n "cervo-nnef") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo-core") (r "^0.6.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "tract-hir") (r "^0.21") (d #t) (k 0)) (d (n "tract-nnef") (r "^0.21") (d #t) (k 0)) (d (n "tract-onnx") (r "^0.21") (d #t) (k 0)))) (h "0wil6wy44j3scchk8gdsq0wmv5b3wv031sii3dn1a73n9k7ssmvi")))

