(define-module (crates-io ce rv cervo-cli) #:use-module (crates-io))

(define-public crate-cervo-cli-0.5.0-rc.0 (c (n "cervo-cli") (v "0.5.0-rc.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo") (r "^0.5.0-rc.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.4") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("fmt"))) (d #t) (k 0)))) (h "04gz25jbb75zim9n4j85y2zyawciwnx2bs8zy3nmx5r878a8l8m1")))

(define-public crate-cervo-cli-0.5.0 (c (n "cervo-cli") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo") (r "^0.5.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.4") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("fmt"))) (d #t) (k 0)))) (h "1fvqjz6fhmrv8wdf7x2xrc51hr14817mb01rczlan8kzma264jxc")))

(define-public crate-cervo-cli-0.5.1 (c (n "cervo-cli") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.4") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("fmt"))) (d #t) (k 0)))) (h "0gynf1hwp4bnvhh30bb0nf46galw26pbqwicyz4jpa2qiqqv5kqa")))

(define-public crate-cervo-cli-0.6.0 (c (n "cervo-cli") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo") (r "^0.6.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.4") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("fmt"))) (d #t) (k 0)))) (h "04ykc6w8glbdlds1qfkr361iidcj3rdyxvy4liyq2c2bh8rfmpdg")))

