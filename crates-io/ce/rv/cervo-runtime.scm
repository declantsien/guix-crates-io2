(define-module (crates-io ce rv cervo-runtime) #:use-module (crates-io))

(define-public crate-cervo-runtime-0.3.0 (c (n "cervo-runtime") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "cervo-asset") (r "^0.3.0") (d #t) (k 2)) (d (n "cervo-core") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0kx8dbslwhpdijx5lrc2d9lnvzp3bix3mi9g8kh1a1laigk6kw03")))

(define-public crate-cervo-runtime-0.4.0 (c (n "cervo-runtime") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "cervo-asset") (r "^0.4.0") (d #t) (k 2)) (d (n "cervo-core") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03y9hzffw364km4yysrajvjy03pw5mxxn8pzwsv8xmpni9h8v3ll")))

(define-public crate-cervo-runtime-0.5.0-rc.0 (c (n "cervo-runtime") (v "0.5.0-rc.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "cervo-asset") (r "^0.5.0-rc.0") (d #t) (k 2)) (d (n "cervo-core") (r "^0.5.0-rc.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1y9zafpzkgwg55n7jgm1z07whgawz225zwqns3zk7fj1bjipbcjn") (f (quote (("threaded" "rayon") ("default"))))))

(define-public crate-cervo-runtime-0.5.0 (c (n "cervo-runtime") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "cervo-asset") (r "^0.5.0") (d #t) (k 2)) (d (n "cervo-core") (r "^0.5.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03nq2i79bamxln82xkwghh8av02702k5bimr2r2ijkgdafmmac1w") (f (quote (("threaded" "rayon") ("default"))))))

(define-public crate-cervo-runtime-0.5.1 (c (n "cervo-runtime") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "cervo-asset") (r "^0.5.1") (d #t) (k 2)) (d (n "cervo-core") (r "^0.5.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08ddpkz9wl3r906hr7fwgq648skw1yy1mb0whl3dn6b4fwpqw7lm") (f (quote (("threaded" "rayon") ("default"))))))

(define-public crate-cervo-runtime-0.6.0 (c (n "cervo-runtime") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "cervo-asset") (r "^0.6.0") (d #t) (k 2)) (d (n "cervo-core") (r "^0.6.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0iv06nr4hv82frpm61ygnqb295c54a3wzjps4mgwp6swh3p6rbn1") (f (quote (("threaded" "rayon") ("default"))))))

