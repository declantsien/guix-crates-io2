(define-module (crates-io ce rv cervo-asset) #:use-module (crates-io))

(define-public crate-cervo-asset-0.1.0 (c (n "cervo-asset") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo-core") (r "^0.1.0") (d #t) (k 0)) (d (n "cervo-nnef") (r "^0.1.0") (d #t) (k 0)) (d (n "cervo-onnx") (r "^0.1.0") (d #t) (k 0)))) (h "1s8hxbyrfmb073avck7j9k59ix5gmqq7shsbbb47dlxhip3w2pd6")))

(define-public crate-cervo-asset-0.2.0 (c (n "cervo-asset") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo-core") (r "^0.2.0") (d #t) (k 0)) (d (n "cervo-nnef") (r "^0.2.0") (d #t) (k 0)) (d (n "cervo-onnx") (r "^0.2.0") (d #t) (k 0)))) (h "0v9sr75krk3mawibrnsfgnfh7qanq0qykdkawd21j38s1c39z7z5")))

(define-public crate-cervo-asset-0.3.0 (c (n "cervo-asset") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo-core") (r "^0.3.0") (d #t) (k 0)) (d (n "cervo-nnef") (r "^0.3.0") (d #t) (k 0)) (d (n "cervo-onnx") (r "^0.3.0") (d #t) (k 0)))) (h "0ppfphflk9a8bvqyb4hw1rmd981qqvbb75ky4k3af529qni200qw")))

(define-public crate-cervo-asset-0.4.0 (c (n "cervo-asset") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo-core") (r "^0.4.0") (d #t) (k 0)) (d (n "cervo-nnef") (r "^0.4.0") (d #t) (k 0)) (d (n "cervo-onnx") (r "^0.4.0") (d #t) (k 0)))) (h "0hbi2wn1bgjjpnfpzqb17awhvjyk27qaav29rwwlc91dxr0w1fc8")))

(define-public crate-cervo-asset-0.5.0-rc.0 (c (n "cervo-asset") (v "0.5.0-rc.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo-core") (r "^0.5.0-rc.0") (d #t) (k 0)) (d (n "cervo-nnef") (r "^0.5.0-rc.0") (d #t) (k 0)) (d (n "cervo-onnx") (r "^0.5.0-rc.0") (d #t) (k 0)))) (h "0z2a7xpwpfj13g30y84h07zq80yg5zv3a1hh4nxnkcfp5b7rbkxl")))

(define-public crate-cervo-asset-0.5.0 (c (n "cervo-asset") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo-core") (r "^0.5.0") (d #t) (k 0)) (d (n "cervo-nnef") (r "^0.5.0") (d #t) (k 0)) (d (n "cervo-onnx") (r "^0.5.0") (d #t) (k 0)))) (h "0cc4s5xvsmzs41bjwdd5xpdpa3k3q9ikc4lmjwwqw9vbm2a5aarz")))

(define-public crate-cervo-asset-0.5.1 (c (n "cervo-asset") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo-core") (r "^0.5.1") (d #t) (k 0)) (d (n "cervo-nnef") (r "^0.5.1") (d #t) (k 0)) (d (n "cervo-onnx") (r "^0.5.1") (d #t) (k 0)))) (h "0h6kw6x21jvvzcsszr35lzdim6wc1ywmcqmqyrwpb51b588vd5f9")))

(define-public crate-cervo-asset-0.6.0 (c (n "cervo-asset") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "cervo-core") (r "^0.6.0") (d #t) (k 0)) (d (n "cervo-nnef") (r "^0.6.0") (d #t) (k 0)) (d (n "cervo-onnx") (r "^0.6.0") (d #t) (k 0)))) (h "1xgcqwy328wf0n00yc3569y6hmslis6p61cdxbpsij1qh69d73qs")))

