(define-module (crates-io ce rv cervo-core) #:use-module (crates-io))

(define-public crate-cervo-core-0.1.0 (c (n "cervo-core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "perchance") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "tract-core") (r "^0.16.7") (d #t) (k 0)) (d (n "tract-hir") (r "^0.16.7") (d #t) (k 0)))) (h "10vaxp9l0xy74zh88ywislhrrm4h393wqx0whb7zcc4qm3hg8f40")))

(define-public crate-cervo-core-0.2.0 (c (n "cervo-core") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "perchance") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "tract-core") (r "^0.17.1") (d #t) (k 0)) (d (n "tract-hir") (r "^0.17.1") (d #t) (k 0)))) (h "0ycillpb6y31pxc5nrc0jh53x3fal0ddrrm8y7zb55q7xbcxrb6h")))

(define-public crate-cervo-core-0.3.0 (c (n "cervo-core") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "perchance") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "tract-core") (r "^0.17.1") (d #t) (k 0)) (d (n "tract-hir") (r "^0.17.1") (d #t) (k 0)))) (h "0a77hai2wwdjmsb1x54ylbvmz6mh47cdrf2h9j8bx5p7pqvi9gwc")))

(define-public crate-cervo-core-0.4.0 (c (n "cervo-core") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "perchance") (r "^0.4") (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "tract-core") (r "^0.17.1") (d #t) (k 0)) (d (n "tract-hir") (r "^0.17.1") (d #t) (k 0)))) (h "035mdniisps6cdm6dpqr6jc088nzwdyy9akzwqnc4s8z8fmx0cw2")))

(define-public crate-cervo-core-0.5.0-rc.0 (c (n "cervo-core") (v "0.5.0-rc.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "perchance") (r "^0.4") (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "tract-core") (r "^0.20") (d #t) (k 0)) (d (n "tract-hir") (r "^0.20") (d #t) (k 0)))) (h "0hw07r1z5670bg5lajqfpswaqyjrwqdjwrbdakd6r2pc66g632iq")))

(define-public crate-cervo-core-0.5.0 (c (n "cervo-core") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "perchance") (r "^0.4") (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "tract-core") (r "^0.20") (d #t) (k 0)) (d (n "tract-hir") (r "^0.20") (d #t) (k 0)))) (h "167dkv121bkmmd9lmmvjcgpgqdcka2gaxri11hmlaw78s30zc8fs")))

(define-public crate-cervo-core-0.5.1 (c (n "cervo-core") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "perchance") (r "^0.4") (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "tract-core") (r "^0.20") (d #t) (k 0)) (d (n "tract-hir") (r "^0.20") (d #t) (k 0)))) (h "06v6rjsz5s543fjq7kmhk4hqjix43crbglpsmgjy5wbczdy28x0m")))

(define-public crate-cervo-core-0.6.0 (c (n "cervo-core") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "perchance") (r "^0.5") (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "tract-core") (r "^0.21") (d #t) (k 0)) (d (n "tract-hir") (r "^0.21") (d #t) (k 0)))) (h "0iwx1jpvhx4i8vnxramkjg12r6gvi6k05aym84pxilpcj34m9cxh")))

