(define-module (crates-io ce lt celtic-names) #:use-module (crates-io))

(define-public crate-celtic-names-1.0.0 (c (n "celtic-names") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dont_disappear") (r "^3.0.0") (d #t) (k 0)) (d (n "markov") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "ureq") (r "^2.0.2") (d #t) (k 0)))) (h "06912n5a99wbwb5v7cc73djai1n4kd7lbjpz2c7svilmi3pljq7v")))

(define-public crate-celtic-names-1.0.1 (c (n "celtic-names") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dont_disappear") (r "^3.0.0") (d #t) (k 0)) (d (n "markov") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "ureq") (r "^2.0.2") (d #t) (k 0)))) (h "1pmjhy045n19lwmk3azs6hx2c1832y5ny00xmawfq76fd1b688za")))

