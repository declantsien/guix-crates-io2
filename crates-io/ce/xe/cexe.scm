(define-module (crates-io ce xe cexe) #:use-module (crates-io))

(define-public crate-cexe-0.1.0 (c (n "cexe") (v "0.1.0") (h "0fx274b3ccf89hhnbhjq6rysgisd80921gla0vlqmxqb0gynkpmb")))

(define-public crate-cexe-0.1.1 (c (n "cexe") (v "0.1.1") (h "19hg4ifhrga30v82p1ska2f8669mqnangg3sisiq2bspy5f0iyfc")))

