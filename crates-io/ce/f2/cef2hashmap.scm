(define-module (crates-io ce f2 cef2hashmap) #:use-module (crates-io))

(define-public crate-cef2hashmap-0.1.1 (c (n "cef2hashmap") (v "0.1.1") (h "022dv20mhik69ps82vhw4yjg808a5jmg7df82cimv8h0pafs8vw3")))

(define-public crate-cef2hashmap-0.1.2 (c (n "cef2hashmap") (v "0.1.2") (h "1f6dn4l5g2nd3j224d1ji15ffpshqkvlaky52k82lk1z2d4127wn")))

(define-public crate-cef2hashmap-0.1.3 (c (n "cef2hashmap") (v "0.1.3") (h "0j7ns7fizlcwx90zbmcqjychan8fj06a05r537w2iyb1brk233vf")))

(define-public crate-cef2hashmap-0.1.4 (c (n "cef2hashmap") (v "0.1.4") (h "1hwxni1xigbmxp88cal7rrvd7cfszxwiipnimjyf220xivbiqsf2")))

