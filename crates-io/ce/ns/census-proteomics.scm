(define-module (crates-io ce ns census-proteomics) #:use-module (crates-io))

(define-public crate-census-proteomics-0.1.2 (c (n "census-proteomics") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1v2zzrylvygan5798yrqjib0npzn30z4c3ncgyzs5ykz6pyqs6w5") (f (quote (("serialization" "serde"))))))

(define-public crate-census-proteomics-0.1.3 (c (n "census-proteomics") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1rhij6r0mb5is238358k93czfrz3s794yih1w3xi2ypyx5zkxbbs") (f (quote (("serialization" "serde"))))))

(define-public crate-census-proteomics-0.1.4 (c (n "census-proteomics") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1d8y3d315b2h0il4fzn6fd1g1gffmpzncqqrlzlhh1lxdd570swl") (f (quote (("serialization" "serde"))))))

(define-public crate-census-proteomics-0.1.5 (c (n "census-proteomics") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "11isbb0r5y6w607i5ma9falb7wmhyks2dn4ldp61w2bxii6lzk1g") (f (quote (("serialization" "serde"))))))

(define-public crate-census-proteomics-0.1.6 (c (n "census-proteomics") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "02xk8b03iqh7yz44qndvicpvym58zc1pg1sdng76liwl3ajm1c4p") (f (quote (("serialization" "serde"))))))

(define-public crate-census-proteomics-0.1.7 (c (n "census-proteomics") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0pjjsqjpiqlldw8y7zpi7bys3iyfanyfh5xjv6apc6xjzx9hc1wk") (f (quote (("serialization" "serde"))))))

(define-public crate-census-proteomics-0.1.8 (c (n "census-proteomics") (v "0.1.8") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0srmyyfcgdrdh4rsgyhfig4fqxv17zv5kb3rh5gvln1w81ablc37") (f (quote (("serialization" "serde"))))))

(define-public crate-census-proteomics-0.2.1 (c (n "census-proteomics") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "12dnmllilns9h63kbv7lnbv554h3kd68dfbl0nbcy7s5qgq6jxqw") (f (quote (("serialization" "serde"))))))

(define-public crate-census-proteomics-0.3.0 (c (n "census-proteomics") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1c3gxxshdqhcqj85y4i41l6dsca38mhrm1bph2zd4s93n3h747an") (f (quote (("serialization" "serde"))))))

(define-public crate-census-proteomics-0.3.1 (c (n "census-proteomics") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0fv86cfpdczv35901kvg6xgmimaq8wskhqwz2752kzfd34j37zv3") (f (quote (("serialization" "serde"))))))

(define-public crate-census-proteomics-0.3.2 (c (n "census-proteomics") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0xj22zcf7nml85d6a4flyk3cn06y1hn8h7d14l1gydff9ah7d2hc") (f (quote (("serialization" "serde"))))))

(define-public crate-census-proteomics-0.3.3 (c (n "census-proteomics") (v "0.3.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0mpyxx4sxvap9850x3w98ljm6q3yrxrax7sksvkd2xnckjrqf5r4") (f (quote (("serialization" "serde"))))))

