(define-module (crates-io ce ns censys-search) #:use-module (crates-io))

(define-public crate-censys-search-0.1.0 (c (n "censys-search") (v "0.1.0") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "1cbdgd672wdw73wh492gyw9ab9ha2yw64bl5qjbwmic7k68kbjff")))

