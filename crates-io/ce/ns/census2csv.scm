(define-module (crates-io ce ns census2csv) #:use-module (crates-io))

(define-public crate-census2csv-0.1.0 (c (n "census2csv") (v "0.1.0") (d (list (d (n "census-proteomics") (r "^0.1.7") (f (quote ("serialization"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05pm4cvzhcl8z80a9j2sqk1x3fjnl8rdw7nylzdlxz3dpqdi74w0")))

(define-public crate-census2csv-0.1.1 (c (n "census2csv") (v "0.1.1") (d (list (d (n "census-proteomics") (r "^0.1.7") (f (quote ("serialization"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pcixjy3if6bg1r89jly13mnc64pxpvbrr1vrjck5jyl786c7jbh")))

(define-public crate-census2csv-0.1.2 (c (n "census2csv") (v "0.1.2") (d (list (d (n "census-proteomics") (r "^0.1.8") (f (quote ("serialization"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jlycvli0r00wg1ng6bl2khdyl261lkqd25ydrinzvwkl6jq8i3d")))

(define-public crate-census2csv-0.1.3 (c (n "census2csv") (v "0.1.3") (d (list (d (n "census-proteomics") (r "^0.1.8") (f (quote ("serialization"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r3w24srrrhwcb8c6h8436lzcxnq13m6lgwjw0wp2fqv6gs71mdd")))

(define-public crate-census2csv-0.1.4 (c (n "census2csv") (v "0.1.4") (d (list (d (n "census-proteomics") (r "^0.3.3") (f (quote ("serialization"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mzj8kdviqd4j3jqzi2qgla4ryhra1dap4wry8209f1q80yqa4sk")))

