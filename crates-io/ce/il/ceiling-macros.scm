(define-module (crates-io ce il ceiling-macros) #:use-module (crates-io))

(define-public crate-ceiling-macros-0.1.0 (c (n "ceiling-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "0xb8a065zgmk1608qi7y7fdc3hzcjhqnbjc5wa9kcq5nr52hymip")))

(define-public crate-ceiling-macros-0.1.1 (c (n "ceiling-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "0xhzcc1adijharxvqhmk5piwynw131b0mrwfdz27nr50zvggpdf7")))

(define-public crate-ceiling-macros-0.1.2 (c (n "ceiling-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "106rrqg35f0d8rgl04nmypvp286q6jfjs1kz8g4lmj8k573j0y73")))

(define-public crate-ceiling-macros-0.1.3 (c (n "ceiling-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "10gisdxjn1b7nm0rkbbwkavdzgibnfa0c5i220a8mr8zxhygg48z")))

