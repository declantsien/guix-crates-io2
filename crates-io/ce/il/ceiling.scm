(define-module (crates-io ce il ceiling) #:use-module (crates-io))

(define-public crate-ceiling-0.1.0 (c (n "ceiling") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "ceiling-macros") (r "^0.1") (d #t) (k 0)) (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "sero") (r "^0.1") (d #t) (k 0)))) (h "0nbsfg9l8bysxxc28lbz7zj1m2qbzpy3zr5a9dzvbm1mhlxv66d4") (f (quote (("default")))) (s 2) (e (quote (("async" "dep:async-trait"))))))

(define-public crate-ceiling-0.1.1 (c (n "ceiling") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "ceiling-macros") (r "^0.1") (d #t) (k 0)) (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "sero") (r "^0.1") (d #t) (k 0)))) (h "17mzqx4kd9rs40pmz1kzin0halw8m6hq3kjnrdb7xqn2danbppjd") (f (quote (("default")))) (s 2) (e (quote (("async" "dep:async-trait"))))))

(define-public crate-ceiling-0.1.2 (c (n "ceiling") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "ceiling-macros") (r "^0.1") (d #t) (k 0)) (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "sero") (r "^0.1") (d #t) (k 0)))) (h "0djn12g0wyz8prv8rcc182m3a0zqfx03n9v6m87hpnidfkz4ffqd") (f (quote (("default")))) (s 2) (e (quote (("async" "dep:async-trait"))))))

(define-public crate-ceiling-0.1.3 (c (n "ceiling") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "ceiling-macros") (r "^0.1") (d #t) (k 0)) (d (n "dashmap") (r "^5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "sero") (r "^0.1") (d #t) (k 0)))) (h "0biswj675dg355m6cwk2ijy988qsz7ssgmk1q715dprf5qjnqlbj") (f (quote (("default")))) (s 2) (e (quote (("async" "dep:async-trait"))))))

