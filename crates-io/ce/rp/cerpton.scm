(define-module (crates-io ce rp cerpton) #:use-module (crates-io))

(define-public crate-cerpton-0.1.0 (c (n "cerpton") (v "0.1.0") (d (list (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)))) (h "1hhl31w9br6yvxkjjj3sbdh8jyib0k2p4pmrk80zsav026fcchbx")))

(define-public crate-cerpton-0.1.1 (c (n "cerpton") (v "0.1.1") (d (list (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)))) (h "1knr17n83iid2yvmkx0az349bx6385y61k2n0hiy6s9bj9ay1b1h")))

