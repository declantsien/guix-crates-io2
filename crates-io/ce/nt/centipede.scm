(define-module (crates-io ce nt centipede) #:use-module (crates-io))

(define-public crate-centipede-0.3.0 (c (n "centipede") (v "0.3.0") (d (list (d (n "bulletproof-kzen") (r "^1.2") (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "curv-kzen") (r "^0.9") (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "0pplr8qhq52kk67mgbvy6672780046jfsqk16qqhb3z80z1d9ssy") (f (quote (("default" "curv-kzen/rust-gmp-kzen"))))))

(define-public crate-centipede-0.3.1 (c (n "centipede") (v "0.3.1") (d (list (d (n "bulletproof-kzen") (r "^1.2.1") (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "curv-kzen") (r "^0.10") (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "04m5j2vqr5y2vyzz8kc0q2q39yzx9lg9bgmsgjimhqrla6adnc3c") (f (quote (("default" "curv-kzen/rust-gmp-kzen"))))))

