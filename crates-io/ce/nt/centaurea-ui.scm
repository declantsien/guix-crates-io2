(define-module (crates-io ce nt centaurea-ui) #:use-module (crates-io))

(define-public crate-centaurea-ui-0.1.1 (c (n "centaurea-ui") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "11q8chcbj4aqn2fjhiyi60h4da7g7jhar0mmgp0rinhaymwxc989")))

(define-public crate-centaurea-ui-0.1.2 (c (n "centaurea-ui") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "14jbkwq7kqvdr9pqycqh8zijrrx18fsvyqn1xckhw3jni7wjgmqc")))

(define-public crate-centaurea-ui-0.2.0 (c (n "centaurea-ui") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0h26rv6kh2mpdhz5af9fyg1wlpyqhzrbnvdaq2kw0nydln5yz058")))

(define-public crate-centaurea-ui-0.3.0 (c (n "centaurea-ui") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "crossterm") (r "=0.20.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "11idr576q16m3lsml31832l3f64w40a5xrxm7r4j0qbz2vjz8b7l")))

