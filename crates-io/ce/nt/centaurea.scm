(define-module (crates-io ce nt centaurea) #:use-module (crates-io))

(define-public crate-centaurea-0.1.1 (c (n "centaurea") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "08gpf71rfmmcazqv5nd1ls7l3s7w7mapgshg22dfrr08nyv17k8f") (y #t)))

