(define-module (crates-io ce nt cents) #:use-module (crates-io))

(define-public crate-cents-0.1.0 (c (n "cents") (v "0.1.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "0rvjl791mkbq1jxyvr2vw093jlibr1ajhi0b3scqzd65c49vfh8w")))

(define-public crate-cents-0.1.1 (c (n "cents") (v "0.1.1") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "0100ks52bgzparkmjx1bz80rrdph39z2iv263l12i7z4krd87cfp")))

