(define-module (crates-io ce nt centichain_keypair) #:use-module (crates-io))

(define-public crate-centichain_keypair-1.0.0 (c (n "centichain_keypair") (v "1.0.0") (d (list (d (n "bip39") (r "^2.0.0") (d #t) (k 0)) (d (n "seed15") (r "^0.1.5") (d #t) (k 0)) (d (n "sp-core") (r "^27.0.0") (d #t) (k 0)))) (h "1xsvc1vw6g41p9clr859c7ikiqlyidv7j2n8crg82dw1mpcag7dj")))

(define-public crate-centichain_keypair-2.0.0 (c (n "centichain_keypair") (v "2.0.0") (d (list (d (n "bip39") (r "^2.0.0") (d #t) (k 0)) (d (n "seed15") (r "^0.1.5") (d #t) (k 0)) (d (n "sp-core") (r "^27.0.0") (d #t) (k 0)))) (h "1j8hj8k7xhv8ymsj4pnmcy246lrnrkwjfmn8vv0nbfmcm1ss1hw3")))

(define-public crate-centichain_keypair-2.1.0 (c (n "centichain_keypair") (v "2.1.0") (d (list (d (n "bip39") (r "^2.0.0") (d #t) (k 0)) (d (n "seed15") (r "^0.1.5") (d #t) (k 0)) (d (n "sp-core") (r "^27.0.0") (d #t) (k 0)))) (h "01a5l1r0pw11sm46w4ibv6pb59s0a90c3xk4c7vfnkan58c4sd4f")))

(define-public crate-centichain_keypair-2.1.1 (c (n "centichain_keypair") (v "2.1.1") (d (list (d (n "bip39") (r "^2.0.0") (d #t) (k 0)) (d (n "seed15") (r "^0.1.5") (d #t) (k 0)) (d (n "sp-core") (r "^32.0.0") (d #t) (k 0)))) (h "10wch48rdyjbn7lgk2dzdsj8jhdaddvqs408nxhj3v4rn9bfrlsb")))

(define-public crate-centichain_keypair-2.1.2 (c (n "centichain_keypair") (v "2.1.2") (d (list (d (n "bip39") (r "^2.0.0") (d #t) (k 0)) (d (n "seed15") (r "^0.1.5") (d #t) (k 0)) (d (n "sp-core") (r "^32.0.0") (d #t) (k 0)))) (h "1d942b5f7pygg8zn723ir4pvb1m4r2izff877gs30bdvy93vdilg")))

