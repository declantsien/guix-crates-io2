(define-module (crates-io ce dr cedrh) #:use-module (crates-io))

(define-public crate-cedrh-0.3.1 (c (n "cedrh") (v "0.3.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0ygq1b36kbxi6wqamfid2hk5j11yc05ijg7946ss6016l3d02f54")))

(define-public crate-cedrh-0.3.2 (c (n "cedrh") (v "0.3.2") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "03myrb86i6nisld31n79g0rkwgxdw49y8sldd6fv7mj0sw37cvfh")))

(define-public crate-cedrh-0.3.3 (c (n "cedrh") (v "0.3.3") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0ii05fh6kxkbpznmv3mmkh0cjrpca6x5qfkw8y98z90i7xdb89sf")))

(define-public crate-cedrh-0.3.4 (c (n "cedrh") (v "0.3.4") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0rh24mc2fzjby6fidwn1dp14vxmgvk5811ijq73pb6csphfpv00v")))

(define-public crate-cedrh-0.3.5 (c (n "cedrh") (v "0.3.5") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1955xhlv79d7r6zbvbjdz3633g8i13jl5v5rmddgsq4k7v89g8h9")))

(define-public crate-cedrh-0.3.6 (c (n "cedrh") (v "0.3.6") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1rdaqlmqypf57a9asrfxqj6g584rp11zk6mjjyngqsi2qy1rljgz")))

(define-public crate-cedrh-0.3.7 (c (n "cedrh") (v "0.3.7") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1gva6w18jq7vjsdvnaa178z5kv9w9d7kqylc80365zw0s1q3q7j1")))

(define-public crate-cedrh-0.3.8 (c (n "cedrh") (v "0.3.8") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0gsr18578z1snadhpzf071yybih5a3i169nxzgfw9phkjbm2nl2i")))

(define-public crate-cedrh-0.3.9 (c (n "cedrh") (v "0.3.9") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1fd7pik6bwbcgnv67w398nhqy51i4qjr7g0wa8n7rry12j17dqzs")))

(define-public crate-cedrh-0.4.0 (c (n "cedrh") (v "0.4.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1khgdxghpw37mmnny5470h3bpnasqbxm40i9c8p3wgky8xzm350m")))

(define-public crate-cedrh-0.4.1 (c (n "cedrh") (v "0.4.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0fl7bdkzn7j0y2rhivyzxqq6cax2f2vdb170v3kz3lffyasjiqmk")))

(define-public crate-cedrh-0.4.2 (c (n "cedrh") (v "0.4.2") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1f4syg0gcyvip6lcfyacbq4hhxg4k9jd57gbcpcyqnixhlkl5k5q")))

(define-public crate-cedrh-0.4.3 (c (n "cedrh") (v "0.4.3") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0d1kkkk3plm295axj2f5fpmr67p5jdgyxjdv7l3vf0rlw6gxahfl")))

(define-public crate-cedrh-0.4.4 (c (n "cedrh") (v "0.4.4") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "15wv6bc9c9fpadazpr2j08djh0isa062yhby7avkvp7b8zf1a20x")))

(define-public crate-cedrh-0.4.5 (c (n "cedrh") (v "0.4.5") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0rcrsnkm5ichvnnqwx3vi7r0yis73v107jfnx21h5z69pia1h7bz")))

(define-public crate-cedrh-0.4.6 (c (n "cedrh") (v "0.4.6") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "173ivqq3rdgr5fav4d369fnlmja0p2z35s4jy3527ni51jcln776")))

(define-public crate-cedrh-0.4.7 (c (n "cedrh") (v "0.4.7") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0l06sd2csna1qfkpaqfz8ck2p3l8p28v912jrxj07ba027f9nkgz")))

(define-public crate-cedrh-0.5.0 (c (n "cedrh") (v "0.5.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "05n54x2wwjnhnpyp590wd6ni49s179bshvlsgxphg4915sj0hz8j")))

(define-public crate-cedrh-0.5.1 (c (n "cedrh") (v "0.5.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "000gbckm2bwk7h17j1950159x706fxb9sxi2h4572pmv9ba4h45i")))

