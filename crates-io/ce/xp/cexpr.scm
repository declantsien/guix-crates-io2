(define-module (crates-io ce xp cexpr) #:use-module (crates-io))

(define-public crate-cexpr-0.1.0 (c (n "cexpr") (v "0.1.0") (d (list (d (n "clang-sys") (r "^0.7.0") (d #t) (k 2)) (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "nom") (r "^1") (f (quote ("regexp" "regexp_macros"))) (d #t) (k 0)) (d (n "regex") (r "^0.1.56") (d #t) (k 0)))) (h "19bm27abfydpak45cs7rmv9zdhjq76jcqjcn6fzl4b65dr3dybyi")))

(define-public crate-cexpr-0.1.1 (c (n "cexpr") (v "0.1.1") (d (list (d (n "clang-sys") (r "^0.7.0") (d #t) (k 2)) (d (n "nom") (r "^1") (f (quote ("regexp" "regexp_macros"))) (d #t) (k 0)))) (h "062i9ksj3l36wzq6cvpnfd2msh43f7l6a6ya53d9q63rcpdwj000")))

(define-public crate-cexpr-0.1.2 (c (n "cexpr") (v "0.1.2") (d (list (d (n "clang-sys") (r "^0.7.0") (d #t) (k 2)) (d (n "nom") (r "^1") (d #t) (k 0)))) (h "0jli55805lz6k4cqhk850jbihm0hx6wcahsc1z3l3dq02s8vh0yh")))

(define-public crate-cexpr-0.1.3 (c (n "cexpr") (v "0.1.3") (d (list (d (n "clang-sys") (r "^0.7.0") (d #t) (k 2)) (d (n "nom") (r "^1") (d #t) (k 0)))) (h "1fzf0wr2vs8zax9gz9mg4m48r770axxcczqnh48h1cnafpvg0qzz")))

(define-public crate-cexpr-0.2.0 (c (n "cexpr") (v "0.2.0") (d (list (d (n "clang-sys") (r "^0.11.0") (d #t) (k 2)) (d (n "nom") (r "^1") (d #t) (k 0)))) (h "0zfy0zpapj5jzq4j3ri0hih7ihl39vr65l7ws7wl3gpgi005yfir")))

(define-public crate-cexpr-0.2.1 (c (n "cexpr") (v "0.2.1") (d (list (d (n "clang-sys") (r "^0.11.0") (d #t) (k 2)) (d (n "nom") (r "^3") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "149p0h3d4bfq5xccyr12dywnkzhr45fkw4xz91pmfzf8g6z0x71w") (y #t)))

(define-public crate-cexpr-0.2.2 (c (n "cexpr") (v "0.2.2") (d (list (d (n "clang-sys") (r "^0.11.0") (d #t) (k 2)) (d (n "nom") (r "^3") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "04s3gdgqg75dg8iwwf5s41q2cippjx19jnahvxhpljgkdzgj3fyd")))

(define-public crate-cexpr-0.2.3 (c (n "cexpr") (v "0.2.3") (d (list (d (n "clang-sys") (r "^0.11.0") (d #t) (k 2)) (d (n "nom") (r "^3") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "0v1xa3758czmj8h97gh548mr8g0v13ixxvrlm1s79nb7jmgc9aj2")))

(define-public crate-cexpr-0.3.0 (c (n "cexpr") (v "0.3.0") (d (list (d (n "clang-sys") (r "^0.11.0") (d #t) (k 2)) (d (n "nom") (r "^4") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "0xq5ah4hyvhcpykspv8nrwcsw7whxx4qvmhqjkq5ccqjilzwxr0i")))

(define-public crate-cexpr-0.3.1 (c (n "cexpr") (v "0.3.1") (d (list (d (n "clang-sys") (r ">= 0.13.0, <= 0.26.0") (d #t) (k 2)) (d (n "nom") (r "^4") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "0m6cdgw0m3dwa5qzr2ay2556g3yix4qmrbd59wa9p5n7kw82vhd6")))

(define-public crate-cexpr-0.3.2 (c (n "cexpr") (v "0.3.2") (d (list (d (n "clang-sys") (r ">= 0.13.0, < 0.27.0") (d #t) (k 2)) (d (n "nom") (r "^4") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "15v4rw732k47dzg4nprv98rry4nq7pdc3zyvfk4pgwgnqhqgqngz")))

(define-public crate-cexpr-0.3.3 (c (n "cexpr") (v "0.3.3") (d (list (d (n "clang-sys") (r ">= 0.13.0, < 0.27.0") (d #t) (k 2)) (d (n "nom") (r "^4") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "16vfvgi21y711n2ha045dsw9if0wacsk91zwi7ygg0nax5mhih4g")))

(define-public crate-cexpr-0.3.4 (c (n "cexpr") (v "0.3.4") (d (list (d (n "clang-sys") (r ">= 0.13.0, < 0.28.0") (d #t) (k 2)) (d (n "nom") (r "^4") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "1nihn8h8wd1pb9jg1dhzcixfj6sfj6jfviyw6bnma6d9rwz6jkb4")))

(define-public crate-cexpr-0.3.5 (c (n "cexpr") (v "0.3.5") (d (list (d (n "clang-sys") (r ">= 0.13.0, < 0.29.0") (d #t) (k 2)) (d (n "nom") (r "^4") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "1by64ini3f058pwad3immx5cc12wr0m0kwgaxa8apzym03mj9ym7")))

(define-public crate-cexpr-0.3.6 (c (n "cexpr") (v "0.3.6") (d (list (d (n "clang-sys") (r ">= 0.13.0, < 0.29.0") (d #t) (k 2)) (d (n "nom") (r "^4") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "07fdfj4ff2974y33yixrb657riq9zl9b9h9lr0h7ridhhvxvbrgw")))

(define-public crate-cexpr-0.4.0 (c (n "cexpr") (v "0.4.0") (d (list (d (n "clang-sys") (r ">= 0.13.0, < 0.29.0") (d #t) (k 2)) (d (n "nom") (r "^5") (f (quote ("std"))) (k 0)))) (h "09qd1k1mrhcqfhqmsz4y1bya9gcs29si7y3w96pqkgid4y2dpbpl")))

(define-public crate-cexpr-0.5.0 (c (n "cexpr") (v "0.5.0") (d (list (d (n "clang-sys") (r ">=0.13.0, <0.29.0") (d #t) (k 2)) (d (n "nom") (r "^6") (f (quote ("std"))) (k 0)))) (h "12awiqgwqc1cnjr2b4lz139mdv3md0y13n0dxmv24b95g5v7ll6v")))

(define-public crate-cexpr-0.6.0 (c (n "cexpr") (v "0.6.0") (d (list (d (n "clang-sys") (r ">=0.13.0, <0.29.0") (d #t) (k 2)) (d (n "nom") (r "^7") (f (quote ("std"))) (k 0)))) (h "0rl77bwhs5p979ih4r0202cn5jrfsrbgrksp40lkfz5vk1x3ib3g")))

