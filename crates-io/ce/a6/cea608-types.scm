(define-module (crates-io ce a6 cea608-types) #:use-module (crates-io))

(define-public crate-cea608-types-0.0.1 (c (n "cea608-types") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1i2s9mwqi1yw3g76g030q0plsrgb45fhykcsszx7smzfmrl6add2") (r "1.65.0")))

(define-public crate-cea608-types-0.1.0 (c (n "cea608-types") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ydgj89p62761ipzaha2zs8dhjqkifq2z5bqysbzfvgc30kxr0mr") (r "1.65.0")))

(define-public crate-cea608-types-0.1.1 (c (n "cea608-types") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "11lzqplcdng4n2rxffli3n4b5ldbh68ap8vqs80rfblbnhv5zxrw") (r "1.65.0")))

