(define-module (crates-io ce nu cenum) #:use-module (crates-io))

(define-public crate-cenum-1.0.0 (c (n "cenum") (v "1.0.0") (d (list (d (n "cenum-derive") (r "^1.0.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "1fyyab8510rjjhgsggvybhvr6c53wmbw8xr8z8nspy6ckz88nqgs")))

(define-public crate-cenum-1.0.1 (c (n "cenum") (v "1.0.1") (d (list (d (n "cenum-derive") (r "^1.0.1") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "1a3fwf8dvy635jybnq9d75ndrlsr14p4zqhb6mwgzgvsxxiz4hj5")))

(define-public crate-cenum-1.0.2 (c (n "cenum") (v "1.0.2") (d (list (d (n "cenum-derive") (r "^1.0.2") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "1vhxp0rpnxh885i54shdxzg0bx91zwszib3plhhchyc9wvwvp9lf")))

(define-public crate-cenum-1.0.3 (c (n "cenum") (v "1.0.3") (d (list (d (n "cenum-derive") (r "^1.0.3") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "0044k1h20d4sn60srwz0ahrqrbjfw7l311y34sal3p6wlgx72bjl")))

(define-public crate-cenum-1.1.0 (c (n "cenum") (v "1.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0641jq4fd412a2d33q1qk9yj1f9csw15dmbfrg6mlna3dca35a30")))

(define-public crate-cenum-1.1.1 (c (n "cenum") (v "1.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0v1npbhamd0id0ym84ycds24dq9cdv3l8jxiq4xcxz1i6h33630g")))

