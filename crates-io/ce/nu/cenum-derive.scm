(define-module (crates-io ce nu cenum-derive) #:use-module (crates-io))

(define-public crate-cenum-derive-1.0.0 (c (n "cenum-derive") (v "1.0.0") (d (list (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1090i841rv7d9dqw194my41bba4a0dixzpgb98ys4dl6j2vs908s")))

(define-public crate-cenum-derive-1.0.1 (c (n "cenum-derive") (v "1.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "09gaq8lc6sw12dvshkgc75mrh0g5fw82ac3139wmji7h3q4d5abs")))

(define-public crate-cenum-derive-1.0.2 (c (n "cenum-derive") (v "1.0.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0jna5v5cjx0hh6izywg0dh8jvhls37zks4xdp3rw98kq8qp5rkdm")))

(define-public crate-cenum-derive-1.0.3 (c (n "cenum-derive") (v "1.0.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1n3q3k6rfliikwi3rdyqxh0p9xfg4m33yjjigzmigff6izzk7rck")))

