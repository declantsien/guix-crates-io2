(define-module (crates-io ce tk cetkaik_naive_representation) #:use-module (crates-io))

(define-public crate-cetkaik_naive_representation-1.0.0 (c (n "cetkaik_naive_representation") (v "1.0.0") (d (list (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)) (d (n "cetkaik_traits") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "04p666di6kwpgxjqgn27a1way0vfff6madz9dzw0s32h6s3rs225")))

(define-public crate-cetkaik_naive_representation-1.0.1 (c (n "cetkaik_naive_representation") (v "1.0.1") (d (list (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)) (d (n "cetkaik_traits") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pgzhlx6w1fs81dg0i0pdjz9ljk6rnhjxl1ijflvvlszmwm8p55v")))

(define-public crate-cetkaik_naive_representation-1.1.0 (c (n "cetkaik_naive_representation") (v "1.1.0") (d (list (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)) (d (n "cetkaik_traits") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "1d2x7pv3iysfpiqhryq2if97nwlwilnh3hb0qa7zrgpl48g0ycg0")))

(define-public crate-cetkaik_naive_representation-1.2.0 (c (n "cetkaik_naive_representation") (v "1.2.0") (d (list (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)) (d (n "cetkaik_traits") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "1axc64snh6xfzmxvpfn0bkcd8cbq3imag4pmi4vivakqpxv3s9hl")))

(define-public crate-cetkaik_naive_representation-1.3.0 (c (n "cetkaik_naive_representation") (v "1.3.0") (d (list (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)) (d (n "cetkaik_traits") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fn40xj7xhpnv8jffhyf430f9fccbfyfwnbd8hsyf8j41s0p0rgz")))

