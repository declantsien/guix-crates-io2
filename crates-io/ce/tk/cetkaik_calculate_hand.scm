(define-module (crates-io ce tk cetkaik_calculate_hand) #:use-module (crates-io))

(define-public crate-cetkaik_calculate_hand-0.1.0 (c (n "cetkaik_calculate_hand") (v "0.1.0") (d (list (d (n "cetkaik_core") (r "^0.1.18") (d #t) (k 0)) (d (n "multiset") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)))) (h "09w8acfwxzl05871yf13ign0c0n3i9ga3pc5am5zvdvnpkf7a8q3")))

(define-public crate-cetkaik_calculate_hand-0.1.1 (c (n "cetkaik_calculate_hand") (v "0.1.1") (d (list (d (n "cetkaik_core") (r "^0.1.19") (d #t) (k 0)) (d (n "multiset") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)))) (h "0hc7w88f309anpqrr5km1k8pylr1b125gz7allfr8v87axxy5jv7")))

(define-public crate-cetkaik_calculate_hand-0.1.2 (c (n "cetkaik_calculate_hand") (v "0.1.2") (d (list (d (n "cetkaik_core") (r "^0.2.0") (d #t) (k 0)) (d (n "multiset") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)))) (h "1q4rvyx2n4yliwm6nbzr33868iqf5kf2nfjk0qw0wbjg4iwcmxlg")))

(define-public crate-cetkaik_calculate_hand-0.1.3 (c (n "cetkaik_calculate_hand") (v "0.1.3") (d (list (d (n "cetkaik_core") (r "^0.2.2") (d #t) (k 0)) (d (n "multiset") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)))) (h "1nniq8yyf145fd47xpai8qyl1wvczzniv8f6nwn1a63xqfx4lszj")))

(define-public crate-cetkaik_calculate_hand-0.1.4 (c (n "cetkaik_calculate_hand") (v "0.1.4") (d (list (d (n "cetkaik_core") (r "^0.2.3") (d #t) (k 0)) (d (n "multiset") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "0iq2790gyv54zkm6jlhj63q001qylwggc0mlfinf1ym23wbxz3p3")))

(define-public crate-cetkaik_calculate_hand-0.1.5 (c (n "cetkaik_calculate_hand") (v "0.1.5") (d (list (d (n "cetkaik_core") (r "^0.3.0") (d #t) (k 0)) (d (n "multiset") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "0qm10jc9sd9zg6vfqsyypxm287mqyx82yvwrji95g7alwr4drpg0")))

(define-public crate-cetkaik_calculate_hand-0.1.6 (c (n "cetkaik_calculate_hand") (v "0.1.6") (d (list (d (n "cetkaik_core") (r "^0.3.1") (d #t) (k 0)) (d (n "multiset") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "02f8923l8y6z309ycil9xzyp5akmiawi9mryfqkhyrknldqs5nvy")))

(define-public crate-cetkaik_calculate_hand-0.1.7 (c (n "cetkaik_calculate_hand") (v "0.1.7") (d (list (d (n "cetkaik_core") (r "^0.3.3") (d #t) (k 0)) (d (n "multiset") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "0rfr1l53y1nzdgxww4kdw2qjiyjmdafzbn5pjq78f7ahdqljy04s")))

(define-public crate-cetkaik_calculate_hand-0.1.8 (c (n "cetkaik_calculate_hand") (v "0.1.8") (d (list (d (n "cetkaik_core") (r "^0.3.4") (d #t) (k 0)) (d (n "multiset") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)))) (h "095hg37q07hd4nn2swrnd8b332x1rkssxnlwbfig2411c31wr380")))

(define-public crate-cetkaik_calculate_hand-0.2.0 (c (n "cetkaik_calculate_hand") (v "0.2.0") (d (list (d (n "cetkaik_core") (r "^0.3.4") (d #t) (k 0)) (d (n "multiset") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)))) (h "0zx9nsp8dpbx0j1wl6srfn2h885iavrbj2jhpl57059j09gx64q8")))

(define-public crate-cetkaik_calculate_hand-0.3.0 (c (n "cetkaik_calculate_hand") (v "0.3.0") (d (list (d (n "cetkaik_core") (r "^0.3.4") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (d #t) (k 0)) (d (n "multiset") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)))) (h "1s0bhk3vgcgl8lz37znvkfif1vw69084bfswjjv8lbfzxmf47cx8")))

(define-public crate-cetkaik_calculate_hand-0.3.1 (c (n "cetkaik_calculate_hand") (v "0.3.1") (d (list (d (n "cetkaik_core") (r "^0.3.8") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (d #t) (k 0)) (d (n "multiset") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1pk9qdmmhmm553hb596ajwr7q31jpx2zp2ikkln4bq45f3fh5zmx")))

(define-public crate-cetkaik_calculate_hand-0.4.0 (c (n "cetkaik_calculate_hand") (v "0.4.0") (d (list (d (n "cetkaik_core") (r "^0.4.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "multiset") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.88") (d #t) (k 0)))) (h "0sasll7dglh7d1a37993fxvz18zmi265n7pp7j3ldakdn6sshf0h")))

(define-public crate-cetkaik_calculate_hand-0.5.0 (c (n "cetkaik_calculate_hand") (v "0.5.0") (d (list (d (n "cetkaik_core") (r "^0.5.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "multiset") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.88") (d #t) (k 0)))) (h "0vh61m1axlbc0q7ah55djwfww8a9i1fbw8ajd6vpvm6j9ylws77s")))

(define-public crate-cetkaik_calculate_hand-1.0.0 (c (n "cetkaik_calculate_hand") (v "1.0.0") (d (list (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13.1") (d #t) (k 0)) (d (n "multiset") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.88") (d #t) (k 0)))) (h "0942kiji5ya41sqnvc9vg0pdm1gvjh5mf0la45yamvwgl0pqh3m2")))

