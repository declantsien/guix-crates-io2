(define-module (crates-io ce tk cetkaik_traits) #:use-module (crates-io))

(define-public crate-cetkaik_traits-1.0.0 (c (n "cetkaik_traits") (v "1.0.0") (d (list (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)))) (h "17j7qqcwdf6f4ycawfy4gxha2xfaalpmxi8fjx619vhc1i5x159n")))

(define-public crate-cetkaik_traits-1.0.1 (c (n "cetkaik_traits") (v "1.0.1") (d (list (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)))) (h "1y9b1cnah7sm345hmpyi606764ggx6wjs0bfqg1wzca391xc4vka")))

(define-public crate-cetkaik_traits-1.1.0 (c (n "cetkaik_traits") (v "1.1.0") (d (list (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)))) (h "09rkgwphdqzgvr4bb514c4910gm1yvs0zfwkfnc2dywjgrq12fn0")))

(define-public crate-cetkaik_traits-1.1.1 (c (n "cetkaik_traits") (v "1.1.1") (d (list (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)))) (h "197vcgwlsam4g6cjpb63bb3spnxksj154bd2jzfkzm00jmbwv9id")))

(define-public crate-cetkaik_traits-1.2.0 (c (n "cetkaik_traits") (v "1.2.0") (d (list (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)))) (h "15pz6qxcv4hyjkzm647d3r72aq5lhpfrjijnaqcl7adgb2ylzazy")))

(define-public crate-cetkaik_traits-1.3.0 (c (n "cetkaik_traits") (v "1.3.0") (d (list (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)))) (h "0vlrmylas9lbhral97x376l0pqhrxq1xsm579jzxg5d7i65hvgp8")))

(define-public crate-cetkaik_traits-1.3.1 (c (n "cetkaik_traits") (v "1.3.1") (d (list (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)))) (h "01z6ayr1p2chm2y0hq1lawn823d47x5aikr6nx1038zfviccknra")))

