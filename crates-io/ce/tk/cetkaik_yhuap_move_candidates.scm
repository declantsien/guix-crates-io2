(define-module (crates-io ce tk cetkaik_yhuap_move_candidates) #:use-module (crates-io))

(define-public crate-cetkaik_yhuap_move_candidates-0.1.0 (c (n "cetkaik_yhuap_move_candidates") (v "0.1.0") (d (list (d (n "cetkaik_core") (r "^0.1.11") (d #t) (k 0)))) (h "0jypgc76cl1yf2v165j3z60hllazf7yynqzxvxs7j2kvb0lhplcp")))

(define-public crate-cetkaik_yhuap_move_candidates-0.1.1 (c (n "cetkaik_yhuap_move_candidates") (v "0.1.1") (d (list (d (n "cetkaik_core") (r "^0.2.0") (d #t) (k 0)))) (h "08pqx71yzcm023h7i07crpskj1nzbxmqcxj8z79lgsjbiix6i08a")))

(define-public crate-cetkaik_yhuap_move_candidates-0.1.2 (c (n "cetkaik_yhuap_move_candidates") (v "0.1.2") (d (list (d (n "cetkaik_core") (r "^0.2.1") (d #t) (k 0)))) (h "1x92sfc91gi6jxz84llrw1g5zzyjkjylw3kixk4m0lfhr3dklwgx")))

(define-public crate-cetkaik_yhuap_move_candidates-0.1.3 (c (n "cetkaik_yhuap_move_candidates") (v "0.1.3") (d (list (d (n "cetkaik_core") (r "^0.2.1") (d #t) (k 0)))) (h "1d8i711k8xgi3x7is6wmjnrcy0g0bdc852r2mq0d4aag97ikfdvd")))

(define-public crate-cetkaik_yhuap_move_candidates-0.1.4 (c (n "cetkaik_yhuap_move_candidates") (v "0.1.4") (d (list (d (n "cetkaik_core") (r "^0.2.2") (d #t) (k 0)))) (h "19jchcwzbnbgrr82n17k0bfj3m4x2i5lkfwg382ga8vnngjp1p06")))

(define-public crate-cetkaik_yhuap_move_candidates-0.2.0 (c (n "cetkaik_yhuap_move_candidates") (v "0.2.0") (d (list (d (n "cetkaik_core") (r "^0.2.2") (d #t) (k 0)))) (h "01sgp8ab2ljyl79wzhi8c020md6x87scncahw8s8yn7s2giny44b")))

(define-public crate-cetkaik_yhuap_move_candidates-0.2.1 (c (n "cetkaik_yhuap_move_candidates") (v "0.2.1") (d (list (d (n "cetkaik_core") (r "^0.2.3") (d #t) (k 0)))) (h "0cyq2xi1b0lqrn1qb91j9n82gqp5bx9zdclfdhvsd33ld9101zzq")))

(define-public crate-cetkaik_yhuap_move_candidates-0.2.2 (c (n "cetkaik_yhuap_move_candidates") (v "0.2.2") (d (list (d (n "cetkaik_core") (r "^0.3.0") (d #t) (k 0)))) (h "0anw39lzib2p8hj4m6k9nz130jjia26vkz84zwxvcggpl56yhgqd")))

(define-public crate-cetkaik_yhuap_move_candidates-0.2.3 (c (n "cetkaik_yhuap_move_candidates") (v "0.2.3") (d (list (d (n "cetkaik_core") (r "^0.3.1") (d #t) (k 0)))) (h "0q5479aazwh2mhigq1a4j2c1lzg2dwmmqcdlv5fxlqfsibsfcrsw")))

(define-public crate-cetkaik_yhuap_move_candidates-0.2.4 (c (n "cetkaik_yhuap_move_candidates") (v "0.2.4") (d (list (d (n "cetkaik_core") (r "^0.3.3") (d #t) (k 0)))) (h "0mv4n7jzg8rmkbn9l89fnzfw3hi325s854j425p749ajk33fkkg0")))

(define-public crate-cetkaik_yhuap_move_candidates-0.2.5 (c (n "cetkaik_yhuap_move_candidates") (v "0.2.5") (d (list (d (n "cetkaik_core") (r "^0.3.4") (d #t) (k 0)))) (h "1i8ynvz3grgs6zq07snvxsjx73kp9np6sa4860nrsbzv6l57cg2x")))

(define-public crate-cetkaik_yhuap_move_candidates-0.2.6 (c (n "cetkaik_yhuap_move_candidates") (v "0.2.6") (d (list (d (n "cetkaik_core") (r "^0.3.5") (d #t) (k 0)))) (h "171b9xm89fixf852106hv2308jdz8l39fjaq1k79f5imp24ff3sa")))

(define-public crate-cetkaik_yhuap_move_candidates-0.2.8 (c (n "cetkaik_yhuap_move_candidates") (v "0.2.8") (d (list (d (n "cetkaik_core") (r "^0.3.8") (d #t) (k 0)))) (h "1p07gs9by8632nxb1vw0wn0hq5fma8s0g2lm4fd4xi2009j0s0qh")))

(define-public crate-cetkaik_yhuap_move_candidates-0.2.9 (c (n "cetkaik_yhuap_move_candidates") (v "0.2.9") (d (list (d (n "cetkaik_core") (r "^0.3.8") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "08kzzvgb51bc6f6ra5fkd5sl8qv8fmv6yfvrj2gv84anhq2f1lhi")))

(define-public crate-cetkaik_yhuap_move_candidates-0.3.0 (c (n "cetkaik_yhuap_move_candidates") (v "0.3.0") (d (list (d (n "cetkaik_core") (r "^0.3.8") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "0hl42psiq9mymv387l6hz4pxh63wlidmwf3nnnsblibk312919jn")))

(define-public crate-cetkaik_yhuap_move_candidates-0.3.1 (c (n "cetkaik_yhuap_move_candidates") (v "0.3.1") (d (list (d (n "cetkaik_core") (r "^0.3.8") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "0p6sc49d5h9xgydnmgsi1hc2xrc513zshr74dvdf2jiha0147nyv")))

(define-public crate-cetkaik_yhuap_move_candidates-0.3.2 (c (n "cetkaik_yhuap_move_candidates") (v "0.3.2") (d (list (d (n "cetkaik_core") (r "^0.3.8") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "0hykjy0dydg55b8fx0bq5nbjdxyfgm1av9l8n7s6a4hl3g915r7g")))

(define-public crate-cetkaik_yhuap_move_candidates-0.3.3 (c (n "cetkaik_yhuap_move_candidates") (v "0.3.3") (d (list (d (n "cetkaik_core") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "0x22ng3wz4xlppzf13wagql13y0xy4bfj0k0i50z8zi73q5hf4ki")))

(define-public crate-cetkaik_yhuap_move_candidates-0.3.4 (c (n "cetkaik_yhuap_move_candidates") (v "0.3.4") (d (list (d (n "cetkaik_core") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "064d89swgdi0gb2wfxnds9vp8hbbf2yhhnjfyd4h3h62wpqimq4v")))

(define-public crate-cetkaik_yhuap_move_candidates-0.4.0 (c (n "cetkaik_yhuap_move_candidates") (v "0.4.0") (d (list (d (n "cetkaik_compact_representation") (r "^0.2.1") (d #t) (k 0)) (d (n "cetkaik_core") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "0v0fgpfgslgrx47bzqq6s4ljbsbxjrna7db2wwimxckkf230as1z")))

(define-public crate-cetkaik_yhuap_move_candidates-0.5.0 (c (n "cetkaik_yhuap_move_candidates") (v "0.5.0") (d (list (d (n "cetkaik_compact_representation") (r "^0.2.1") (d #t) (k 0)) (d (n "cetkaik_core") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "1yl3hbni9sn19fgpipm9j980y6rbhihiszwfaa1v9pr1l252nswy")))

(define-public crate-cetkaik_yhuap_move_candidates-0.5.1 (c (n "cetkaik_yhuap_move_candidates") (v "0.5.1") (d (list (d (n "cetkaik_compact_representation") (r "^0.2.7") (d #t) (k 0)) (d (n "cetkaik_core") (r "^0.4.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "15j2hfjppsfdk4g7k7zvjw31hl3pgirfkcsphx7ysnp2anhgni10")))

(define-public crate-cetkaik_yhuap_move_candidates-0.6.0 (c (n "cetkaik_yhuap_move_candidates") (v "0.6.0") (d (list (d (n "cetkaik_compact_representation") (r "^0.2.9") (d #t) (k 0)) (d (n "cetkaik_core") (r "^0.4.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "0blc6l8qz3xp9jig0qay9ickmvcbl31llf40vs4ww3mi0020ph5y")))

(define-public crate-cetkaik_yhuap_move_candidates-0.6.1 (c (n "cetkaik_yhuap_move_candidates") (v "0.6.1") (d (list (d (n "cetkaik_compact_representation") (r "^0.2.10") (d #t) (k 0)) (d (n "cetkaik_core") (r "^0.4.2") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "1h6kc4yikxmgsg23mjnjkzppczqbha5knl3f3aw5fghxhp7pgxnw")))

(define-public crate-cetkaik_yhuap_move_candidates-0.6.2 (c (n "cetkaik_yhuap_move_candidates") (v "0.6.2") (d (list (d (n "cetkaik_compact_representation") (r "^0.2.11") (d #t) (k 0)) (d (n "cetkaik_core") (r "^0.4.2") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "06if2m46yfph7s6ql6gn56jlap007a9crn0l7ij097mqd6lyx67f")))

(define-public crate-cetkaik_yhuap_move_candidates-0.6.3 (c (n "cetkaik_yhuap_move_candidates") (v "0.6.3") (d (list (d (n "cetkaik_compact_representation") (r "^0.2.11") (d #t) (k 0)) (d (n "cetkaik_core") (r "^0.4.2") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "17fp2ixprjmir2b4fdmgnxz2v135cnhdw4wjpy11xkb7djc5qarw")))

(define-public crate-cetkaik_yhuap_move_candidates-0.7.1 (c (n "cetkaik_yhuap_move_candidates") (v "0.7.1") (d (list (d (n "cetkaik_compact_representation") (r "^0.3.1") (d #t) (k 0)) (d (n "cetkaik_core") (r "^0.5.2") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "03hmxi7h9rcwypqpl2hsf8ba02nmxf6p7ymlagjf1dlym6j1b2pf")))

(define-public crate-cetkaik_yhuap_move_candidates-0.7.2 (c (n "cetkaik_yhuap_move_candidates") (v "0.7.2") (d (list (d (n "cetkaik_compact_representation") (r "^0.3.3") (d #t) (k 0)) (d (n "cetkaik_core") (r "^0.5.3") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "0i1a1kpss0nwq2bb3b3g8swzirfvhnadsmpx65ir0js7wgc054hk")))

(define-public crate-cetkaik_yhuap_move_candidates-0.7.3 (c (n "cetkaik_yhuap_move_candidates") (v "0.7.3") (d (list (d (n "cetkaik_compact_representation") (r "^0.3.4") (d #t) (k 0)) (d (n "cetkaik_core") (r "^0.5.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "1sh7vwj801vnsn1ab1nlr37dm2x7z2vciab4w2qfnd917fzblc5c")))

(define-public crate-cetkaik_yhuap_move_candidates-0.7.4 (c (n "cetkaik_yhuap_move_candidates") (v "0.7.4") (d (list (d (n "cetkaik_compact_representation") (r "^0.3.5") (d #t) (k 0)) (d (n "cetkaik_core") (r "^0.5.5") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "1n8msj20mlwbs4bsdaz1fdc8npz9xmfjw112w25sgwxng9k7vsi4")))

(define-public crate-cetkaik_yhuap_move_candidates-0.7.5 (c (n "cetkaik_yhuap_move_candidates") (v "0.7.5") (d (list (d (n "cetkaik_compact_representation") (r "^0.3.6") (d #t) (k 0)) (d (n "cetkaik_core") (r "^0.5.6") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "1nb4pwdyvz52ifn4112s9x5jc631f45bnklsw8xxzjgp2flr2nkw")))

(define-public crate-cetkaik_yhuap_move_candidates-1.0.0 (c (n "cetkaik_yhuap_move_candidates") (v "1.0.0") (d (list (d (n "cetkaik_compact_representation") (r "^1.0.0") (d #t) (k 2)) (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)) (d (n "cetkaik_naive_representation") (r "^1.0.0") (d #t) (k 2)) (d (n "cetkaik_traits") (r "^1.0.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "0jx9hd2l1gmwdqa03mq9x68fh0x6c1k4wfp186l3zl8g55xy55zk")))

(define-public crate-cetkaik_yhuap_move_candidates-1.2.0 (c (n "cetkaik_yhuap_move_candidates") (v "1.2.0") (d (list (d (n "cetkaik_compact_representation") (r "^1.2.0") (d #t) (k 2)) (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)) (d (n "cetkaik_naive_representation") (r "^1.2.0") (d #t) (k 2)) (d (n "cetkaik_traits") (r "^1.2.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "1n1wndsgnlcbsc9g5bkrq70kkc518yndhidrrf6sx2x0n9wnb100")))

(define-public crate-cetkaik_yhuap_move_candidates-1.3.0 (c (n "cetkaik_yhuap_move_candidates") (v "1.3.0") (d (list (d (n "cetkaik_compact_representation") (r "^1.2.0") (d #t) (k 2)) (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)) (d (n "cetkaik_naive_representation") (r "^1.2.0") (d #t) (k 2)) (d (n "cetkaik_traits") (r "^1.2.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "0lricjaygjgpjhlafl5m1ijcy2m1p94zfypn4mhg35x78fy1p6f0")))

(define-public crate-cetkaik_yhuap_move_candidates-1.3.1 (c (n "cetkaik_yhuap_move_candidates") (v "1.3.1") (d (list (d (n "cetkaik_compact_representation") (r "^1.5.0") (d #t) (k 2)) (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)) (d (n "cetkaik_naive_representation") (r "^1.3.0") (d #t) (k 2)) (d (n "cetkaik_traits") (r "^1.3.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (k 0)))) (h "0wnnnmplk8asjiza1asj6r67y9l48wgnxx3lcyflqs76s8y1jyy6")))

