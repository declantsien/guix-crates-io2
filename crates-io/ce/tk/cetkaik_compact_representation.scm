(define-module (crates-io ce tk cetkaik_compact_representation) #:use-module (crates-io))

(define-public crate-cetkaik_compact_representation-0.0.1 (c (n "cetkaik_compact_representation") (v "0.0.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "cetkaik_core") (r "^0.4.0") (d #t) (k 0)))) (h "0nyd8b8yb6n1pc5jp3ksfbiy578wdqjsy69bmxkiq0rgc0zf6b2s")))

(define-public crate-cetkaik_compact_representation-0.2.0 (c (n "cetkaik_compact_representation") (v "0.2.0") (d (list (d (n "cetkaik_core") (r "^0.4.0") (d #t) (k 0)))) (h "0s4wrpnni48p4hyj06cv40rcrxb09jlrs36nsdj7qfdqgb6ssnz3")))

(define-public crate-cetkaik_compact_representation-0.2.1 (c (n "cetkaik_compact_representation") (v "0.2.1") (d (list (d (n "cetkaik_core") (r "^0.4.0") (d #t) (k 0)))) (h "1lizp0rv8361s85zdj2y6x4zv36impxxj4vdyzmpwayxfqrv3nbs")))

(define-public crate-cetkaik_compact_representation-0.2.2 (c (n "cetkaik_compact_representation") (v "0.2.2") (d (list (d (n "cetkaik_core") (r "^0.4.0") (d #t) (k 0)))) (h "16c5qm9w7hz563syw81ix5nhwxk7qnrr7ppjxdfkr96h8z5k62qm")))

(define-public crate-cetkaik_compact_representation-0.2.3 (c (n "cetkaik_compact_representation") (v "0.2.3") (d (list (d (n "cetkaik_core") (r "^0.4.0") (d #t) (k 0)))) (h "0j71j3qg1cjhhhk7w8z3h73fga432pm6fykd4byk3z3b0cnsi8cq")))

(define-public crate-cetkaik_compact_representation-0.2.4 (c (n "cetkaik_compact_representation") (v "0.2.4") (d (list (d (n "cetkaik_core") (r "^0.4.0") (d #t) (k 0)))) (h "1p35svsbzmi0xx6qixbhz7xrf34nbckcqdlz6pr7j6npzfwqxh45")))

(define-public crate-cetkaik_compact_representation-0.2.5 (c (n "cetkaik_compact_representation") (v "0.2.5") (d (list (d (n "cetkaik_core") (r "^0.4.0") (d #t) (k 0)))) (h "1clvc48ki5lg24756f5inwrazmxz653xfk3as35ask98mvid3pib")))

(define-public crate-cetkaik_compact_representation-0.2.6 (c (n "cetkaik_compact_representation") (v "0.2.6") (d (list (d (n "cetkaik_core") (r "^0.4.1") (d #t) (k 0)))) (h "1zk5bjl47fyvj762sy5qjj0gzcbf7j4r9vjzdji0a5ph3fmpqz7n")))

(define-public crate-cetkaik_compact_representation-0.2.7 (c (n "cetkaik_compact_representation") (v "0.2.7") (d (list (d (n "cetkaik_core") (r "^0.4.1") (d #t) (k 0)))) (h "1vwdc46rk7mm62rspll87pjzga5cfs7j232xqyn9ygh08p7k5x29")))

(define-public crate-cetkaik_compact_representation-0.2.8 (c (n "cetkaik_compact_representation") (v "0.2.8") (d (list (d (n "cetkaik_core") (r "^0.4.1") (d #t) (k 0)))) (h "1sb52ivgl2n97c9p31g908dhvxvi1nqvdw66cimn0v6lmm5gkvrr")))

(define-public crate-cetkaik_compact_representation-0.2.9 (c (n "cetkaik_compact_representation") (v "0.2.9") (d (list (d (n "cetkaik_core") (r "^0.4.1") (d #t) (k 0)))) (h "06lrn7c93xsayvvr587srcf1hkw6pzbfxc8dljcbirb19kxr6j2n")))

(define-public crate-cetkaik_compact_representation-0.2.10 (c (n "cetkaik_compact_representation") (v "0.2.10") (d (list (d (n "cetkaik_core") (r "^0.4.1") (d #t) (k 0)))) (h "13w06bn51x0x7xwgn39hvwzad6r1g67fqlajzb5qjk8hnridd28m")))

(define-public crate-cetkaik_compact_representation-0.2.11 (c (n "cetkaik_compact_representation") (v "0.2.11") (d (list (d (n "cetkaik_core") (r "^0.4.1") (d #t) (k 0)))) (h "0d1iblg8yy554zal879r30k0ic9vdj6skn62gqq1vrnay1hrdja6")))

(define-public crate-cetkaik_compact_representation-0.3.0 (c (n "cetkaik_compact_representation") (v "0.3.0") (d (list (d (n "cetkaik_core") (r "^0.5.0") (d #t) (k 0)))) (h "0y436lg5qyh6z2fbznzsgcpnf80bb0fhkqszj7ndvsvvwwixg89f")))

(define-public crate-cetkaik_compact_representation-0.3.1 (c (n "cetkaik_compact_representation") (v "0.3.1") (d (list (d (n "cetkaik_core") (r "^0.5.2") (d #t) (k 0)))) (h "0pgwmrhzrb43wc3nskrbk3k8znmglw9a9m2q4519512mqk8yc51z")))

(define-public crate-cetkaik_compact_representation-0.3.2 (c (n "cetkaik_compact_representation") (v "0.3.2") (d (list (d (n "cetkaik_core") (r "^0.5.2") (d #t) (k 0)))) (h "1a2r2yxx128s5xipgmvl5nss925yb544wd9pffm463alrvzl6yqg")))

(define-public crate-cetkaik_compact_representation-0.3.3 (c (n "cetkaik_compact_representation") (v "0.3.3") (d (list (d (n "cetkaik_core") (r "^0.5.3") (d #t) (k 0)))) (h "15w5m9f6p1l7yy3hm6xqd2hrwszlr4k75bxxjxg6mymz1m1ydfcw")))

(define-public crate-cetkaik_compact_representation-0.3.4 (c (n "cetkaik_compact_representation") (v "0.3.4") (d (list (d (n "cetkaik_core") (r "^0.5.4") (d #t) (k 0)))) (h "0yl9v3j2fg67923ahhl2sla7vxyybf5f5r5a407m6dsiyklmyiwn")))

(define-public crate-cetkaik_compact_representation-0.3.5 (c (n "cetkaik_compact_representation") (v "0.3.5") (d (list (d (n "cetkaik_core") (r "^0.5.5") (d #t) (k 0)))) (h "0awj4c2zjj27mrizhfc7qdxy0kmbh50p2ivrzr45x9vr2rpfk717")))

(define-public crate-cetkaik_compact_representation-0.3.6 (c (n "cetkaik_compact_representation") (v "0.3.6") (d (list (d (n "cetkaik_core") (r "^0.5.6") (d #t) (k 0)))) (h "0xy9w5zh0yfkyj98h2ngx245s8nrpvdg503ssg8szdhirvy0aqx9")))

(define-public crate-cetkaik_compact_representation-1.0.0 (c (n "cetkaik_compact_representation") (v "1.0.0") (d (list (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)) (d (n "cetkaik_traits") (r "^1.0.0") (d #t) (k 0)))) (h "16n9q77gjny8r0202d6n2ylbwiy9mhbixylrxgs0jh55h48nv994")))

(define-public crate-cetkaik_compact_representation-1.0.1 (c (n "cetkaik_compact_representation") (v "1.0.1") (d (list (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)) (d (n "cetkaik_traits") (r "^1.0.0") (d #t) (k 0)))) (h "0rnv9wk57gw3shcabhlvzq9i39mmdhhav4dsbhv1r3mnyk7swzsa")))

(define-public crate-cetkaik_compact_representation-1.0.2 (c (n "cetkaik_compact_representation") (v "1.0.2") (d (list (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)) (d (n "cetkaik_traits") (r "^1.0.1") (d #t) (k 0)))) (h "1qryip7cvy0l4iwqg7blb6qsr83w9g09g2nj313gw649rn2lqn1q")))

(define-public crate-cetkaik_compact_representation-1.0.3 (c (n "cetkaik_compact_representation") (v "1.0.3") (d (list (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)) (d (n "cetkaik_traits") (r "^1.1.1") (d #t) (k 0)))) (h "0zjb1r69qya0r8m8m62vj2cbrh4kjr2pf0f01xxh5lfjc3p39zb0")))

(define-public crate-cetkaik_compact_representation-1.2.0 (c (n "cetkaik_compact_representation") (v "1.2.0") (d (list (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)) (d (n "cetkaik_traits") (r "^1.2.0") (d #t) (k 0)))) (h "0cyx5nkri2y75ximgddkyk1jmamygzbwk2pixfpg7lcivpv0fpgm")))

(define-public crate-cetkaik_compact_representation-1.3.0 (c (n "cetkaik_compact_representation") (v "1.3.0") (d (list (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)) (d (n "cetkaik_traits") (r "^1.2.0") (d #t) (k 0)))) (h "0bbn84sg6fb6c9rb7hdj69l6w1wh7ijz4fd8fiqkahdzbyjf9i43")))

(define-public crate-cetkaik_compact_representation-1.4.0 (c (n "cetkaik_compact_representation") (v "1.4.0") (d (list (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)) (d (n "cetkaik_traits") (r "^1.2.0") (d #t) (k 0)))) (h "1x0g9h6mfx2l05iy9b9pr42r3h5rz9pmrnrbm37m6avzp8fa1kii")))

(define-public crate-cetkaik_compact_representation-1.5.0 (c (n "cetkaik_compact_representation") (v "1.5.0") (d (list (d (n "cetkaik_fundamental") (r "^1.0.0") (d #t) (k 0)) (d (n "cetkaik_traits") (r "^1.3.1") (d #t) (k 0)))) (h "0z5zkyhd3acq491rpisfvjfcrfcwz0lkg60x3nf3zznj8x3zc51b")))

