(define-module (crates-io ce tk cetkaik_random_play) #:use-module (crates-io))

(define-public crate-cetkaik_random_play-0.1.0 (c (n "cetkaik_random_play") (v "0.1.0") (d (list (d (n "cetkaik_full_state_transition") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1577lmgh747xxbfbd1kyy43kksnz08cxifnrhbj12bbawn49hzjv")))

(define-public crate-cetkaik_random_play-0.1.1 (c (n "cetkaik_random_play") (v "0.1.1") (d (list (d (n "cetkaik_full_state_transition") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "0b17mgvwmmax294446vi2iai6mwd3fk2chgkydy5dkq3az6c6bmr")))

(define-public crate-cetkaik_random_play-0.1.2 (c (n "cetkaik_random_play") (v "0.1.2") (d (list (d (n "cetkaik_full_state_transition") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "04c0jsf030n6a9px41x1bzq35y9v726ibq94g6lfzhzgp22ap39n")))

(define-public crate-cetkaik_random_play-0.1.3 (c (n "cetkaik_random_play") (v "0.1.3") (d (list (d (n "cetkaik_full_state_transition") (r "^0.2.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "10byw4jjggbxyb5zi8n47rksxw3lzq4pfpwrv5cs5zci4gn3dkyv")))

(define-public crate-cetkaik_random_play-0.1.4 (c (n "cetkaik_random_play") (v "0.1.4") (d (list (d (n "cetkaik_full_state_transition") (r "^0.2.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)))) (h "1pp8axdazv3zpgcd22nmas2h8vv5f28pzdm959mk07wyf1s504fg")))

