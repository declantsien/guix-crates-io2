(define-module (crates-io ce e- cee-scape) #:use-module (crates-io))

(define-public crate-cee-scape-0.1.0 (c (n "cee-scape") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "expect-test") (r "^1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1w45hrfc3042bycr63bbsadkzy87933kgrfcqwg9k8xm4x5qvvr2") (f (quote (("test_c_integration") ("default" "test_c_integration"))))))

(define-public crate-cee-scape-0.1.1 (c (n "cee-scape") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "expect-test") (r "^1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0x1dll2rgi1zc2dvzr5im8nvshdc83gmnvql38k34ppkx79z54ly") (f (quote (("test_c_integration") ("default" "test_c_integration"))))))

(define-public crate-cee-scape-0.1.2 (c (n "cee-scape") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "expect-test") (r "^1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03lfzbhszrc8gza6si3sklbaihyag6zn0iqlxf9c8yk8hwq8h225") (f (quote (("test_c_integration") ("default" "test_c_integration"))))))

(define-public crate-cee-scape-0.1.3 (c (n "cee-scape") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "expect-test") (r "^1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0m7yxxks8yikkdwim4q773q9sl1d3gwag48kh0z6cxzp67mgsspz") (f (quote (("test_c_integration") ("default" "test_c_integration"))))))

(define-public crate-cee-scape-0.1.4 (c (n "cee-scape") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "expect-test") (r "^1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1crxdsm69ym2xh8lp0lgrmr71787hcdlf2g23lwfww2yvrfr9k7k") (f (quote (("test_c_integration") ("default" "test_c_integration"))))))

(define-public crate-cee-scape-0.1.5 (c (n "cee-scape") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "expect-test") (r "^1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bzdfhs43f8s6f77093k5p24cq9ns933h3n0r8ky0lx88nv24isc") (f (quote (("test_c_integration") ("default" "test_c_integration"))))))

(define-public crate-cee-scape-0.1.6 (c (n "cee-scape") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "expect-test") (r "^1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0a0avf8i95ximcyabjyrxp1rdxz5m2zv5vcpkfxjf05q1wr1iwfr") (f (quote (("test_c_integration") ("default" "test_c_integration"))))))

(define-public crate-cee-scape-0.2.0 (c (n "cee-scape") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "expect-test") (r "^1.4") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "008i57jgs0brzkd123cggrjh0vhjxaf0ikp9fygpg7qlaaqdyrsd") (f (quote (("use_c_to_interface_with_setjmp") ("test_c_integration") ("default" "test_c_integration"))))))

