(define-module (crates-io ce rk cerk_loader_file) #:use-module (crates-io))

(define-public crate-cerk_loader_file-0.1.0 (c (n "cerk_loader_file") (v "0.1.0") (d (list (d (n "anyhow") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "cerk") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "env_logger") (r ">=0.8.0, <0.9.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "1rji3qs43icr4k73nrm0f5jklci5v45c3bn21a7agnb3k0vx1gh5")))

(define-public crate-cerk_loader_file-0.2.0 (c (n "cerk_loader_file") (v "0.2.0") (d (list (d (n "anyhow") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "cerk") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "env_logger") (r ">=0.8.0, <0.9.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "0nh01vq55642avd3n7j8yc8qgwn6ph8adhq13inaqm2s84pwx3q2")))

(define-public crate-cerk_loader_file-0.2.1 (c (n "cerk_loader_file") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cerk") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ckw2iksnpd3gmfqqz1m7g2hrkwb6dp2lgs5jpz5wpjcglcbbfn7")))

(define-public crate-cerk_loader_file-0.2.6 (c (n "cerk_loader_file") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cerk") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lsqx1gl33c3yz48fsbs3cyjv4qzb499ncd9irl83q25yapcq0zv")))

(define-public crate-cerk_loader_file-0.2.7 (c (n "cerk_loader_file") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cerk") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cfgih110h7mwqk9plphzsq0vabddj0smdh2k35p6qs5zl1dj70g")))

(define-public crate-cerk_loader_file-0.2.8 (c (n "cerk_loader_file") (v "0.2.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cerk") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qh7hbqjsd3skdjkj3yd1g72q4y6hq50irnj6wrbfz8s46kc1ghy")))

