(define-module (crates-io ce rk cerk_runtime_threading) #:use-module (crates-io))

(define-public crate-cerk_runtime_threading-0.2.0 (c (n "cerk_runtime_threading") (v "0.2.0") (d (list (d (n "cerk") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 0)))) (h "1kdcw9611y6nqgjm4rbwn5lzv224g6br39bxp3b55wfb53c9b3hk")))

(define-public crate-cerk_runtime_threading-0.2.1 (c (n "cerk_runtime_threading") (v "0.2.1") (d (list (d (n "cerk") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 0)))) (h "16kyq18yky1sp9v0lgg88w06b93hx95mkylxwp0v5dihcygbhmq2")))

(define-public crate-cerk_runtime_threading-0.2.6 (c (n "cerk_runtime_threading") (v "0.2.6") (d (list (d (n "cerk") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "1smy9r5y9211zsv3zmxfncj345w1x363qnckqbjv5c78vp6ddhwy")))

(define-public crate-cerk_runtime_threading-0.2.7 (c (n "cerk_runtime_threading") (v "0.2.7") (d (list (d (n "cerk") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "0rmqlllys0b0mhddigxir0syfgh6p997wqg9gqh3wgimak41vaw2")))

(define-public crate-cerk_runtime_threading-0.2.8 (c (n "cerk_runtime_threading") (v "0.2.8") (d (list (d (n "cerk") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "05glqfi1yyij2fl9zb11nzkmhq3zvlgg8c15rzp6wds5bw6ymfi9")))

(define-public crate-cerk_runtime_threading-0.2.10 (c (n "cerk_runtime_threading") (v "0.2.10") (d (list (d (n "cerk") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "0v1aw4znhkwlrn2ig6ajxivfh9db3fk5y1mzx4i9zd3fpx72rb41")))

(define-public crate-cerk_runtime_threading-0.2.11 (c (n "cerk_runtime_threading") (v "0.2.11") (d (list (d (n "cerk") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "175cq9xf4169bkh5dimz1z3af4py4fqfm9ixd5b6qy4h9v3k534m")))

