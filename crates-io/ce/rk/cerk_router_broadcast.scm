(define-module (crates-io ce rk cerk_router_broadcast) #:use-module (crates-io))

(define-public crate-cerk_router_broadcast-0.2.0 (c (n "cerk_router_broadcast") (v "0.2.0") (d (list (d (n "cerk") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "cloudevents-sdk") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 0)))) (h "166f02avngq518rhb05x4rl1b9hf3d4ybfdd9jh3gqxidizqrxhc")))

(define-public crate-cerk_router_broadcast-0.2.1 (c (n "cerk_router_broadcast") (v "0.2.1") (d (list (d (n "cerk") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "cloudevents-sdk") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 0)))) (h "0f8ysakgpan5i2g7429w4jxnixxamq8f9dd19psc8ywd0d1cdyxd")))

(define-public crate-cerk_router_broadcast-0.2.6 (c (n "cerk_router_broadcast") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cerk") (r "^0.2") (d #t) (k 0)) (d (n "cloudevents-sdk") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "0f13jcxgpap8jz07hsjzd5v5al3yfwcw24l1yj8xg9193p04az3a")))

(define-public crate-cerk_router_broadcast-0.2.7 (c (n "cerk_router_broadcast") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cerk") (r "^0.2") (d #t) (k 0)) (d (n "cloudevents-sdk") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "1nm1mp437p6a1fnp4jrql1l82ivv6400s4f1wk8zy9xrdhhiv3d4")))

(define-public crate-cerk_router_broadcast-0.2.8 (c (n "cerk_router_broadcast") (v "0.2.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cerk") (r "^0.2") (d #t) (k 0)) (d (n "cloudevents-sdk") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "13ywr2wd8fncwn0gbhcbbxrg73zklzwm7nrwgb33045v2km62kpw")))

