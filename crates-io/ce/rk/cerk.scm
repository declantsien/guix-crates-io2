(define-module (crates-io ce rk cerk) #:use-module (crates-io))

(define-public crate-cerk-0.2.0 (c (n "cerk") (v "0.2.0") (d (list (d (n "anyhow") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "cloudevents-sdk") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "strum_macros") (r ">=0.19.4, <0.20.0") (d #t) (k 0)))) (h "0hbsa0zl7mdbnw3z5vq5w56p2x85vycj3r6zy6rgc427x8fw25yp")))

(define-public crate-cerk-0.2.1 (c (n "cerk") (v "0.2.1") (d (list (d (n "anyhow") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "cloudevents-sdk") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.117, <2.0.0") (d #t) (k 0)) (d (n "strum_macros") (r ">=0.19.4, <0.20.0") (d #t) (k 0)))) (h "1jax0wyf47l9971dvyx0xyjpc4mp3xjivkiphgmb4zk639y8lcqs")))

(define-public crate-cerk-0.2.2 (c (n "cerk") (v "0.2.2") (d (list (d (n "anyhow") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "cloudevents-sdk") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.117, <2.0.0") (d #t) (k 0)) (d (n "strum_macros") (r ">=0.19.4, <0.20.0") (d #t) (k 0)))) (h "1mj640q2lv6vpkd9bznff8a2qmpqflwq3js267v0hqriy42iimch")))

(define-public crate-cerk-0.2.3 (c (n "cerk") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cloudevents-sdk") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "strum_macros") (r "^0.19.4") (d #t) (k 0)))) (h "1d21l7lrzq72m5a9qf6185xap435wb8jzypgwnkn7abjd3vms486")))

(define-public crate-cerk-0.2.6 (c (n "cerk") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cloudevents-sdk") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "strum_macros") (r "^0.19.4") (d #t) (k 0)))) (h "0db212ijssgnjd17l4x0l2zfinjn529pr79wyibnwnsv2ij8dhq2")))

(define-public crate-cerk-0.2.7 (c (n "cerk") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cloudevents-sdk") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "strum_macros") (r "^0.19.4") (d #t) (k 0)))) (h "1rvisscccpmj4v55n55im37c5rb8ik3bs83cn9h0rv3hngwb73d1")))

(define-public crate-cerk-0.2.8 (c (n "cerk") (v "0.2.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cloudevents-sdk") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "strum_macros") (r "^0.19.4") (d #t) (k 0)))) (h "00mm48i4qf5xw1ba4aavldd7crgdkpci1prnipswc1pgmzxk3ajs")))

(define-public crate-cerk-0.2.10 (c (n "cerk") (v "0.2.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cloudevents-sdk") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "strum_macros") (r "^0.19.4") (d #t) (k 0)))) (h "1lya7lbbhlv4vdd2ji9da9i02a1jwj17g3gagwfjgq8jdzvbk8yf")))

(define-public crate-cerk-0.2.11 (c (n "cerk") (v "0.2.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cloudevents-sdk") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "strum_macros") (r "^0.19.4") (d #t) (k 0)))) (h "0z5kr3jj14ylx90dr6yqkqvr2m4lsp57kbw6y60ilx5gsf1iw050")))

