(define-module (crates-io ce ph ceph-rbd) #:use-module (crates-io))

(define-public crate-ceph-rbd-0.2.0 (c (n "ceph-rbd") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.36.0") (d #t) (k 1)) (d (n "ceph") (r "~0.4") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "nix") (r "~0.10") (d #t) (k 0)))) (h "1ijxxzap2zips1h3xpc1dw9nk5w58ipbykklc89jsiqkfxcqma8v")))

(define-public crate-ceph-rbd-0.2.1 (c (n "ceph-rbd") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.36.0") (d #t) (k 1)) (d (n "ceph") (r "~0.5") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "nix") (r "~0.10") (d #t) (k 0)))) (h "0wq4qbmb5dj30gismyq8k4svkzclr9vi7yjkkwm3x4z6ranv7a9z")))

(define-public crate-ceph-rbd-0.2.2 (c (n "ceph-rbd") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.36.0") (d #t) (k 1)) (d (n "ceph") (r "~0.5") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "nix") (r "~0.10") (d #t) (k 0)))) (h "1wm6m3fkh5kf209viynsnvn70p6v785bmd0xgw6f5ah20f74n8ys")))

(define-public crate-ceph-rbd-0.3.0 (c (n "ceph-rbd") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.36.0") (d #t) (k 1)) (d (n "ceph") (r "~1.0") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "nix") (r "~0.10") (d #t) (k 0)))) (h "1r04bwgm64p16ifmknr916g042ijwy5jgn7m986j8g8zajrghd10")))

(define-public crate-ceph-rbd-0.3.1 (c (n "ceph-rbd") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.36.0") (d #t) (k 1)) (d (n "ceph") (r "~1.0") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "nix") (r "~0.10") (d #t) (k 0)))) (h "0b507s01wv2whzilps7fpaj0znh8ipgmz3q6nf6niszjihw9z2dn")))

(define-public crate-ceph-rbd-0.3.2 (c (n "ceph-rbd") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.47.1") (d #t) (k 1)) (d (n "ceph") (r "~2.0") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "nix") (r "~0.10") (d #t) (k 0)))) (h "0lgw811k5mlnqqfbzy0wslx99lz2vbg0wjzahk94x0idyqwkbc7k")))

