(define-module (crates-io ce ph ceph-rust) #:use-module (crates-io))

(define-public crate-ceph-rust-0.1.0 (c (n "ceph-rust") (v "0.1.0") (h "02phzp5lzk0wcqjk1pqm5nlnhh11hab67hvzilyw4q2bli8lip60")))

(define-public crate-ceph-rust-0.1.1 (c (n "ceph-rust") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lsio") (r "^0.1") (d #t) (k 0)))) (h "19bl8453vwgsmhv34kqx26d8hh6fvfpg0ai20v7s142a0bzinldd")))

(define-public crate-ceph-rust-0.1.2 (c (n "ceph-rust") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lsio") (r "^0.1") (d #t) (k 0)))) (h "1phdnqy2izqac0bplr1ig9d0kfsj8b4vlmpi0qykgsnxvqfp0zfl")))

(define-public crate-ceph-rust-0.1.4 (c (n "ceph-rust") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lsio") (r "^0.1") (d #t) (k 0)))) (h "0c7lspxwsa0i3d7zchv4mrq2vxc940904rld6lc4q61kkxjdvxjp")))

(define-public crate-ceph-rust-0.1.5 (c (n "ceph-rust") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lsio") (r "^0.1") (d #t) (k 0)))) (h "1crd7x8hfxh8vf0v7b273z165p6cpfnq6h4v4x22dm6qb9p67ggi")))

(define-public crate-ceph-rust-0.1.6 (c (n "ceph-rust") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lsio") (r "^0.1") (d #t) (k 0)))) (h "0in6pmdqnkq1w94d1gncq4kpv0fl6ma7skjvq8lbj3xc12llwx8x")))

(define-public crate-ceph-rust-0.1.7 (c (n "ceph-rust") (v "0.1.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lsio") (r "^0.1") (d #t) (k 0)))) (h "1qhi2jwpagqnhn9cva8j135yxb76csnhk1vhaqx9rl39xc3lmp37")))

(define-public crate-ceph-rust-0.1.9 (c (n "ceph-rust") (v "0.1.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lsio") (r "^0.1") (d #t) (k 0)))) (h "0p4l9q1s397pcv89wwwf4033kh5lqa785ip57gqwblhvpffp0whw")))

(define-public crate-ceph-rust-0.1.10 (c (n "ceph-rust") (v "0.1.10") (d (list (d (n "bitflags") (r "~0.7") (d #t) (k 0)) (d (n "byteorder") (r "~0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "~1.2") (d #t) (k 0)) (d (n "uuid") (r "~0.3") (d #t) (k 0)))) (h "0wgig0r5rywpr2xg49w68sb9wzgxn4w2fx1jf7syj1msj8xlrnqf")))

(define-public crate-ceph-rust-0.1.12 (c (n "ceph-rust") (v "0.1.12") (d (list (d (n "bitflags") (r "~0.7") (d #t) (k 0)) (d (n "byteorder") (r "~0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "~1.2") (d #t) (k 0)) (d (n "uuid") (r "~0.3") (f (quote ("use_std"))) (d #t) (k 0)))) (h "1rnnq9nbsy3mjm400hcaixgrgcsr2sm12xf8qsjmg6x9wdx1h2fg")))

(define-public crate-ceph-rust-0.1.13 (c (n "ceph-rust") (v "0.1.13") (d (list (d (n "bitflags") (r "~0.7") (d #t) (k 0)) (d (n "byteorder") (r "~0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "~1.2") (d #t) (k 0)) (d (n "uuid") (r "~0.3") (f (quote ("use_std"))) (d #t) (k 0)))) (h "0p327b4ryndxljnb8arj30nimi9kmwrzqxp4ygpkrsq3z6ch6xqr")))

(define-public crate-ceph-rust-0.1.14 (c (n "ceph-rust") (v "0.1.14") (d (list (d (n "bitflags") (r "~0.7") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "~1.2") (d #t) (k 0)) (d (n "uuid") (r "~0.3") (f (quote ("use_std"))) (d #t) (k 0)))) (h "0p5hcj8f90brxbkfk6vq1kvnhab4xiyqp6ddwa0ds5mwzv5a4h0g")))

(define-public crate-ceph-rust-0.1.15 (c (n "ceph-rust") (v "0.1.15") (d (list (d (n "bitflags") (r "~0.7") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "~1.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)) (d (n "uuid") (r "~0.3") (f (quote ("use_std"))) (d #t) (k 0)))) (h "02l813mf4igh41rm2rk9wmwkyf6i8m6ys4hazz2r2bkd0cymv5gl")))

(define-public crate-ceph-rust-0.1.16 (c (n "ceph-rust") (v "0.1.16") (d (list (d (n "bitflags") (r "~0.7") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "~1.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)) (d (n "uuid") (r "~0.3") (f (quote ("use_std"))) (d #t) (k 0)))) (h "1fr2kw1jdp74kxapl3rp87mhim7lkd63dxim94r2nvqs8h1p6n26")))

(define-public crate-ceph-rust-0.1.17 (c (n "ceph-rust") (v "0.1.17") (d (list (d (n "bitflags") (r "~0.7") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "~1.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3") (d #t) (k 0)) (d (n "uuid") (r "~0.3") (f (quote ("use_std"))) (d #t) (k 0)))) (h "18rns715cs80kn33nxixzgl0mi9yrn39d5w9f2hh4mw8rjrajzvf")))

