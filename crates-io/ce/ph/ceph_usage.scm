(define-module (crates-io ce ph ceph_usage) #:use-module (crates-io))

(define-public crate-ceph_usage-0.1.0 (c (n "ceph_usage") (v "0.1.0") (d (list (d (n "ceph") (r "~0.4") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "14pppkpx28gz26bd6873ysl5v71spxwprry4n43yfgx7kmk3sq32")))

(define-public crate-ceph_usage-0.1.1 (c (n "ceph_usage") (v "0.1.1") (d (list (d (n "ceph") (r "~0.4") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "0haq7qkshific8xdzpdm7l98154902mss7cv16fb7kciqlh6fkv0")))

(define-public crate-ceph_usage-0.1.2 (c (n "ceph_usage") (v "0.1.2") (d (list (d (n "ceph") (r "~0.4") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "1ibmds8mv6k354irascxivj7falbjghw2hdkibzb6b2lvp4x2rir")))

(define-public crate-ceph_usage-0.1.3 (c (n "ceph_usage") (v "0.1.3") (d (list (d (n "ceph") (r "~0.4") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "0z89s8ydd0mimsk0an2h938pfgi9ivxpj736g0zv8jilgbfkvfv0")))

(define-public crate-ceph_usage-0.1.4 (c (n "ceph_usage") (v "0.1.4") (d (list (d (n "ceph") (r "~0.4") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "0n76kwqhd0w0d6ngis6svp7f3g5rc736z090v4l0rwph0jwwnfg4")))

(define-public crate-ceph_usage-0.1.5 (c (n "ceph_usage") (v "0.1.5") (d (list (d (n "ceph") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "1z08zfkjshd5rqmi0rm27nc3lwzyzabg8b7r3si6b3jmrg6g4nf8")))

(define-public crate-ceph_usage-0.1.6 (c (n "ceph_usage") (v "0.1.6") (d (list (d (n "ceph") (r "~0.4") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "01vyd4qjz3m62a1qjj4jkwb9nv9xpj066828sn9rfd9bh734vsnx")))

(define-public crate-ceph_usage-0.1.7 (c (n "ceph_usage") (v "0.1.7") (d (list (d (n "ceph") (r "~1.0") (d #t) (k 0)) (d (n "log") (r "~0.3") (d #t) (k 0)))) (h "12x9qp9qc23p95954zzys54c8ri7sbc7mrsshxwhxxhmbsiik3gl")))

(define-public crate-ceph_usage-0.1.8 (c (n "ceph_usage") (v "0.1.8") (d (list (d (n "ceph") (r "~1.0") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)))) (h "0ghrdfwxzlg475wg7zqj54gc51gpy10gvd0qvm15gzgg4gk9lr5p")))

(define-public crate-ceph_usage-0.1.9 (c (n "ceph_usage") (v "0.1.9") (d (list (d (n "ceph") (r "~2.0") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)))) (h "1kpg1g8qyc1ih0hnwgzdw9vlck3n5v30q6j9bw43mshfqb2igdxk")))

