(define-module (crates-io ce ph ceph-safe-disk) #:use-module (crates-io))

(define-public crate-ceph-safe-disk-0.1.1 (c (n "ceph-safe-disk") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "users") (r "^0.5") (d #t) (k 0)))) (h "1snjkgzwlb4jl5lw09y779zsz0jfkhnbkqiwg0ciklahgjnrgzgw")))

(define-public crate-ceph-safe-disk-0.1.2 (c (n "ceph-safe-disk") (v "0.1.2") (d (list (d (n "ansi_term") (r "~0.9") (d #t) (k 0)) (d (n "getopts") (r "~0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "~0.5") (d #t) (k 0)))) (h "1m5xl79d6ckbrwfh5rkk03k8mb6fxk4n098lg164va5v5fkhxir8")))

(define-public crate-ceph-safe-disk-0.1.3 (c (n "ceph-safe-disk") (v "0.1.3") (d (list (d (n "ansi_term") (r "~0.9") (d #t) (k 0)) (d (n "getopts") (r "~0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "~0.5") (d #t) (k 0)))) (h "07fk0km0c61n4krz2mw2jfajcb9xilvxvhrbsh3s21cpskr73i3s")))

(define-public crate-ceph-safe-disk-0.1.4 (c (n "ceph-safe-disk") (v "0.1.4") (d (list (d (n "ansi_term") (r "~0.11") (d #t) (k 0)) (d (n "getopts") (r "~0.2") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "users") (r "~0.8") (d #t) (k 0)))) (h "1nj8wjnrwy5p9zk0jnqazpwswf1m7lqpd5cgd0b1ld9vq8ai3hai")))

(define-public crate-ceph-safe-disk-0.1.5 (c (n "ceph-safe-disk") (v "0.1.5") (d (list (d (n "ansi_term") (r "~0.11") (d #t) (k 0)) (d (n "getopts") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "users") (r "~0.8") (d #t) (k 0)))) (h "1wzn9f1qb2yz6hd8dwbbq4q801fpd0gsy54lv7wpbafqbqxa1y56")))

(define-public crate-ceph-safe-disk-0.1.6 (c (n "ceph-safe-disk") (v "0.1.6") (d (list (d (n "ansi_term") (r "~0.11") (d #t) (k 0)) (d (n "getopts") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "users") (r "~0.8") (d #t) (k 0)))) (h "0gai6g8w0p5axa4jxxcdka3g162fan7d2pd84drf329mi9yp7y8p")))

