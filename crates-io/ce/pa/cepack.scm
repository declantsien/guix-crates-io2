(define-module (crates-io ce pa cepack) #:use-module (crates-io))

(define-public crate-cepack-0.1.0 (c (n "cepack") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)) (d (n "pelite") (r "^0.9") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)))) (h "0vjcfqlh1crx151f4h33g33wqvdjg6rddprvagk5a36lm6rb48mg")))

