(define-module (crates-io ce tu cetus-utils) #:use-module (crates-io))

(define-public crate-cetus-utils-0.1.0 (c (n "cetus-utils") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "inquire") (r "^0.5.2") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.26.1") (f (quote ("maths"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (f (quote ("color"))) (d #t) (k 0)))) (h "1p0d1i7v20a0mjh86dyicwx58m5gjz4ql9kcfjhsdqcany07jzsb")))

