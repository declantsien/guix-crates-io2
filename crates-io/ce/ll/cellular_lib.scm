(define-module (crates-io ce ll cellular_lib) #:use-module (crates-io))

(define-public crate-cellular_lib-0.2.0 (c (n "cellular_lib") (v "0.2.0") (h "1iziv80kf2zvhzqd6lhlcdi01z9dfb65sx3bkk83rgm53aifiwcj") (y #t)))

(define-public crate-cellular_lib-0.2.1 (c (n "cellular_lib") (v "0.2.1") (h "1w8m0k9j1w46m9wir0v09z0429rblcipz5f0779nhdf3r07c1g4j") (y #t)))

(define-public crate-cellular_lib-0.1.0 (c (n "cellular_lib") (v "0.1.0") (h "0nv7p0sngsa9znf45w70bml6g7l52r6bf8f4lbdn6z8kr7jgk41r")))

(define-public crate-cellular_lib-0.1.1 (c (n "cellular_lib") (v "0.1.1") (h "1zl9hg3233v16ng2rwqwqjrcjqkzd9avcl6v7qacq0cdgzymd3lf")))

