(define-module (crates-io ce ll cell-map-macro) #:use-module (crates-io))

(define-public crate-cell-map-macro-0.1.0 (c (n "cell-map-macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1b87l93rf2hk4x6w9595naw71c8bfzrxfgqs7cbriykwcx1m7672")))

(define-public crate-cell-map-macro-0.2.0 (c (n "cell-map-macro") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1g0wbr0fqwq53qjdk3a4bmm8s1a7k3666pmpyi1kh3j8f2vqg4qi")))

