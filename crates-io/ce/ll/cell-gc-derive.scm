(define-module (crates-io ce ll cell-gc-derive) #:use-module (crates-io))

(define-public crate-cell-gc-derive-0.2.1 (c (n "cell-gc-derive") (v "0.2.1") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (f (quote ("aster" "visit"))) (d #t) (k 0)))) (h "1v4xjhcwag5mp0vaxxhvafjmqbh3s886kalsga787jjd6i1c504m")))

