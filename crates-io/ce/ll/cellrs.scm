(define-module (crates-io ce ll cellrs) #:use-module (crates-io))

(define-public crate-cellrs-0.2.0 (c (n "cellrs") (v "0.2.0") (d (list (d (n "battery") (r "^0.7.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "02n5qyy0ysdy4dza9sf1qdfshs6xm7i43fvhwyhyp14grcjvlcra")))

(define-public crate-cellrs-0.2.1 (c (n "cellrs") (v "0.2.1") (d (list (d (n "battery") (r "^0.7.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "0qk5kbvyirl4a442gpr49szdyq6pzi2b2hbk19xw0j5npk5pfy4l")))

(define-public crate-cellrs-0.2.2 (c (n "cellrs") (v "0.2.2") (d (list (d (n "battery") (r "^0.7.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "0gs8h6a4lykgv000yq36s3dzjzjmj5pgl9k40wa1aj8igalqrsvx")))

(define-public crate-cellrs-0.2.3 (c (n "cellrs") (v "0.2.3") (d (list (d (n "battery") (r "^0.7.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "08qvx5l1y3c01fjm1dhamlhjnfn73x03bmhbql2h32f88scpviq8")))

