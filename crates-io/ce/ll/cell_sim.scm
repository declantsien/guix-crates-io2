(define-module (crates-io ce ll cell_sim) #:use-module (crates-io))

(define-public crate-cell_sim-0.0.1 (c (n "cell_sim") (v "0.0.1") (h "0qfq5asbw8rv0fv4iz5394n309nw50ai4858qz5gazgbv9k016bm")))

(define-public crate-cell_sim-0.0.2 (c (n "cell_sim") (v "0.0.2") (h "1xxmgkh9qapzdz3qd6l0fsp59h3fkvr1zisvrf8px421rqh84n86")))

(define-public crate-cell_sim-0.0.3 (c (n "cell_sim") (v "0.0.3") (d (list (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "19ra2bkdsj1npimv7wch2bfbv84jdlv7dg7m7c3vnv0nzbw20s99")))

(define-public crate-cell_sim-0.1.0 (c (n "cell_sim") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c1dyn6n035svc1c8cfw7fqw4lm2i925a3qicrrfjpwy0kjmzqn2")))

(define-public crate-cell_sim-0.2.0 (c (n "cell_sim") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "01s5143shah5zv8kil3kid4bh63yrmjnn4bpbjcsxhl72v1hmql7")))

(define-public crate-cell_sim-0.2.1 (c (n "cell_sim") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)))) (h "04qgbwqgcq87yr0gv0s7rhjnc25krs085n33iz48aqcny1yk96lj")))

