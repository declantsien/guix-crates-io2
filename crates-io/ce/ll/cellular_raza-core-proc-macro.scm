(define-module (crates-io ce ll cellular_raza-core-proc-macro) #:use-module (crates-io))

(define-public crate-cellular_raza-core-proc-macro-0.0.6 (c (n "cellular_raza-core-proc-macro") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ycys9lrsrl8j083nwi6jfya2cgii03275lhfdp9b3sv8890bi44")))

(define-public crate-cellular_raza-core-proc-macro-0.0.7 (c (n "cellular_raza-core-proc-macro") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02z821papslk8q24l8c550ydw4sqv6wxv8yzwpmh5r5jgxkdkh5m")))

(define-public crate-cellular_raza-core-proc-macro-0.0.8 (c (n "cellular_raza-core-proc-macro") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rji86ll3y2xdnbldw1avykgb07b6as00d5qi2jlx8w1dlq65989")))

