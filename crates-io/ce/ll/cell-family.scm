(define-module (crates-io ce ll cell-family) #:use-module (crates-io))

(define-public crate-cell-family-0.1.0 (c (n "cell-family") (v "0.1.0") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "056yv0sc3yjgc5im0b00nf9ka72ksrwrhkjjwclw9ziicd5d58n7") (f (quote (("std") ("nightly") ("default" "std")))) (r "1.61")))

