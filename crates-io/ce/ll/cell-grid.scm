(define-module (crates-io ce ll cell-grid) #:use-module (crates-io))

(define-public crate-cell-grid-0.1.0 (c (n "cell-grid") (v "0.1.0") (d (list (d (n "aline-v01") (r "^0.1.1") (o #t) (k 0) (p "aline")) (d (n "rstest") (r "^0.18.2") (k 2)))) (h "05hkxk30yflq4azcdc57ps6dwaxvmm4ak4nh16v380rmbvfqv9y4") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("aline-v01" "dep:aline-v01")))) (r "1.74")))

(define-public crate-cell-grid-0.1.1 (c (n "cell-grid") (v "0.1.1") (d (list (d (n "aline-v01") (r "^0.1.1") (o #t) (k 0) (p "aline")) (d (n "rstest") (r "^0.18.2") (k 2)))) (h "1sx7fagzqyy4592m4axmcfb7jqsswg438gqsb8pps72m48yhf83h") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("aline-v01" "dep:aline-v01")))) (r "1.74")))

