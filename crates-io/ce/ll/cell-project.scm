(define-module (crates-io ce ll cell-project) #:use-module (crates-io))

(define-public crate-cell-project-0.1.0 (c (n "cell-project") (v "0.1.0") (d (list (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0ma7p4yaqb59w11nfh0c0liydv9ikczkz69a8gmh5yd3fn0gc4vz") (f (quote (("nightly")))) (y #t)))

(define-public crate-cell-project-0.1.1 (c (n "cell-project") (v "0.1.1") (d (list (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0g6an0qidbilj93xpv34b9mskcllx844dnwqwn9hfchx32jzralw") (f (quote (("nightly")))) (y #t)))

(define-public crate-cell-project-0.1.2 (c (n "cell-project") (v "0.1.2") (d (list (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "150n825mcfp4zbkas5bv1xinp1qib7hjifghnzn91z8czmjlw2kn") (f (quote (("nightly")))) (y #t)))

(define-public crate-cell-project-0.1.3 (c (n "cell-project") (v "0.1.3") (d (list (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1a7rz8d7f9d1lhhq8m8pbx34vkajl4zmaf6di2jg5fswyjvrb2dw") (f (quote (("nightly")))) (y #t)))

(define-public crate-cell-project-0.1.4 (c (n "cell-project") (v "0.1.4") (d (list (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0ngvkmsd2znjappsgaivld4ysa08mqwcrhrlr47w2qpk36fn9ahx") (f (quote (("nightly"))))))

