(define-module (crates-io ce ll celly) #:use-module (crates-io))

(define-public crate-celly-0.4.5 (c (n "celly") (v "0.4.5") (d (list (d (n "bincode") (r "^0.5") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (d #t) (k 0)))) (h "074ihf1iy81071nslwfw41pqm8l9cb0vamrm9zfyzz2md8cl70k8")))

(define-public crate-celly-0.5.0 (c (n "celly") (v "0.5.0") (d (list (d (n "bincode") (r "^0.5") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (d #t) (k 0)))) (h "03fv64aacm73fyz6q46znqrkksiz2p26605s82ph19x21as8xkl9")))

(define-public crate-celly-0.6.0 (c (n "celly") (v "0.6.0") (d (list (d (n "bincode") (r "^0.5") (f (quote ("serde"))) (d #t) (k 2)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (d #t) (k 0)))) (h "02683bh9sxqkcrmqgpdh4wv3gfhzx35h0x6m95njbyi2zssn66zw")))

