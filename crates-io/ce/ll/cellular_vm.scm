(define-module (crates-io ce ll cellular_vm) #:use-module (crates-io))

(define-public crate-cellular_vm-0.2.0 (c (n "cellular_vm") (v "0.2.0") (d (list (d (n "cellular_lib") (r "^0.2.0") (d #t) (k 0)))) (h "1zxp2yahamdqh5dvccrba82bzgzdp20m00agci99fnn14y58p09v") (y #t)))

(define-public crate-cellular_vm-0.2.1 (c (n "cellular_vm") (v "0.2.1") (d (list (d (n "cellular_lib") (r "^0.2.1") (d #t) (k 0)))) (h "110ryrygfwh3p34cjcv5i3ih0w4nq11pafl3q1l578vzfyh4r51i") (y #t)))

