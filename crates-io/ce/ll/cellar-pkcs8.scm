(define-module (crates-io ce ll cellar-pkcs8) #:use-module (crates-io))

(define-public crate-cellar-pkcs8-0.1.0 (c (n "cellar-pkcs8") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "untrusted") (r "^0.7") (d #t) (k 0)))) (h "1xmpgxxqz5nkq33iqhmx3qgnvxidsb9fhh7b8z7bb11bpd1cs5fr")))

