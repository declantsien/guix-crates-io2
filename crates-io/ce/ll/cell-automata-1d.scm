(define-module (crates-io ce ll cell-automata-1d) #:use-module (crates-io))

(define-public crate-cell-automata-1d-0.1.0 (c (n "cell-automata-1d") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0pdblnhbx7lgzbfxf8zxc4im9nka49pjaswfd51fd7sly3r4lk24")))

(define-public crate-cell-automata-1d-0.1.1 (c (n "cell-automata-1d") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0qf6a1x130lgyipq5jh9xphb9vmnd27agx8rgh9qrv4dyxzgfkvs")))

(define-public crate-cell-automata-1d-0.1.2 (c (n "cell-automata-1d") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0g60k5w6i9ax1ykb23bj88f3xg0ridygi5cnh0hz5xz43w7nbckb")))

