(define-module (crates-io ce ll cell_reader) #:use-module (crates-io))

(define-public crate-cell_reader-0.2.0 (c (n "cell_reader") (v "0.2.0") (d (list (d (n "chemrust-core") (r "^0.2") (d #t) (k 0)) (d (n "chemrust-formats") (r "^0.2") (d #t) (k 0)) (d (n "chemrust-parser") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kdm0nx68c56374k1fzccmyd3l0ixfkry1mr81113jgaadnfsb4w")))

