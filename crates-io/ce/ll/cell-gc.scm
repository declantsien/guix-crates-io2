(define-module (crates-io ce ll cell-gc) #:use-module (crates-io))

(define-public crate-cell-gc-0.1.0 (c (n "cell-gc") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0") (d #t) (k 2)))) (h "0r044nblry7q8xlm7yiy3sxi338wsffvvcp5vg313zdxagxw4axq")))

(define-public crate-cell-gc-0.1.1 (c (n "cell-gc") (v "0.1.1") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0") (d #t) (k 2)))) (h "02m117xk270a63vk4rfnvsg88h7kd74f094sa7va9y174nzkprjr")))

(define-public crate-cell-gc-0.1.2 (c (n "cell-gc") (v "0.1.2") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0") (d #t) (k 2)))) (h "10mfjsymbfnwc421pyb1wi5ym7ycvlwh7zbbx1f2bfngj5s3m7hv")))

(define-public crate-cell-gc-0.1.3 (c (n "cell-gc") (v "0.1.3") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0") (d #t) (k 2)))) (h "0zb9h4m4v03b383zh6nnfydd4ldg22cfybqahvhrk487vvy3dbcp")))

(define-public crate-cell-gc-0.1.4 (c (n "cell-gc") (v "0.1.4") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0") (d #t) (k 2)))) (h "1bk8qxpm9x7d0v9zmhxv4rrdkbf0zs5d4v9aw7zb0bgq8f9a6qld")))

(define-public crate-cell-gc-0.1.5 (c (n "cell-gc") (v "0.1.5") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.0") (d #t) (k 2)))) (h "07rajrkzb5rhnz9hf83jk3d08m5w5kjbpibcc7k2jk6na25rii8g")))

(define-public crate-cell-gc-0.2.0 (c (n "cell-gc") (v "0.2.0") (d (list (d (n "bit-vec") (r "^0.4.3") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.1") (d #t) (k 2)))) (h "1x1jw6ndk6napczqhvij6nxrd5nx06pflhh2bqyycbp2m2cd15c8")))

(define-public crate-cell-gc-0.2.1 (c (n "cell-gc") (v "0.2.1") (d (list (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "cell-gc-derive") (r "^0.2.1") (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2") (d #t) (k 2)))) (h "0dnaavdnq9ag0cklrs0dr7pgig306j9ln1012287z3l8m4krf1z2")))

