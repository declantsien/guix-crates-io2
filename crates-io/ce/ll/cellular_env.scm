(define-module (crates-io ce ll cellular_env) #:use-module (crates-io))

(define-public crate-cellular_env-0.1.0 (c (n "cellular_env") (v "0.1.0") (d (list (d (n "cellular_lib") (r "^0.1.0") (d #t) (k 0)))) (h "1cs45il0vyxlwna7zkz6g7bxka0xfd3nqj1gkbrz9saqw3hs191j")))

(define-public crate-cellular_env-0.1.1 (c (n "cellular_env") (v "0.1.1") (d (list (d (n "cellular_lib") (r "^0.1.0") (d #t) (k 0)))) (h "0qdsq1s32mjbb6rwh09b91fis1lr0cvwn2hm036jslgs7n6wqvg0")))

