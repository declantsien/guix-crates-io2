(define-module (crates-io ce ll cellular_maps) #:use-module (crates-io))

(define-public crate-cellular_maps-0.0.1 (c (n "cellular_maps") (v "0.0.1") (h "1sz2ii9rb86m6wiw5gmi362vnrb81whks394h1pims0a7mpv2hrb")))

(define-public crate-cellular_maps-0.0.2 (c (n "cellular_maps") (v "0.0.2") (h "07jajv6j8amkfms9l3p3wkh4i9lphzx3jvcrb074i7bpn5yh7gdg")))

(define-public crate-cellular_maps-0.0.3 (c (n "cellular_maps") (v "0.0.3") (h "1w8lzly7s1dfpz0jxj31gp397h5s5ka0yxp14pybrf7g1krgzkrj")))

(define-public crate-cellular_maps-0.1.0 (c (n "cellular_maps") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.8") (d #t) (k 0)))) (h "0dh3fpw5awsr8mdlidy7h4cb1ayixh49a83mz85qal70j3hvs28s")))

(define-public crate-cellular_maps-0.1.1 (c (n "cellular_maps") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0sawfzcxx5yzr1m0n1kwcwwrmvjwyb2hi6mq38x8fj4vqcc4aym4")))

(define-public crate-cellular_maps-1.0.0 (c (n "cellular_maps") (v "1.0.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "1sy73lkyxc1d019vvngz2nq6c5d3j9c9769ikqdc3h3yypvpjz3f")))

(define-public crate-cellular_maps-1.0.1 (c (n "cellular_maps") (v "1.0.1") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "1wlm11cw2chbr9azxj6dpa5vha8qdicndqxb4dy97mkirwgjl4bg")))

(define-public crate-cellular_maps-1.1.0 (c (n "cellular_maps") (v "1.1.0") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "08a5nbf6hxkx9gbh17pin783nxrlyzlz70snancbfvd6wp8hkb29")))

