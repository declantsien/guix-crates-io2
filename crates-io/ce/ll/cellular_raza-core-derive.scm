(define-module (crates-io ce ll cellular_raza-core-derive) #:use-module (crates-io))

(define-public crate-cellular_raza-core-derive-0.0.4 (c (n "cellular_raza-core-derive") (v "0.0.4") (d (list (d (n "cellular_raza-concepts") (r "^0.0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "00v0fbwrv2gjr58aldm3l1m8hm6bc6ccd04d9pzpjpi9kjpv7b50") (y #t)))

(define-public crate-cellular_raza-core-derive-0.0.5 (c (n "cellular_raza-core-derive") (v "0.0.5") (d (list (d (n "cellular_raza-concepts") (r "^0.0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0g7l69fcfh84w5kn9iki1d0a94iv9r5qpdk83781gnfi1ykcd5b3")))

