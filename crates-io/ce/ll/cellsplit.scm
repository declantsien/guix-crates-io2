(define-module (crates-io ce ll cellsplit) #:use-module (crates-io))

(define-public crate-cellsplit-0.2.1 (c (n "cellsplit") (v "0.2.1") (d (list (d (n "clap") (r "^2.20.0") (f (quote ("color"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "slug") (r "^0.1") (d #t) (k 0)) (d (n "unborrow") (r "^0.3") (d #t) (k 0)))) (h "0h0p8lijxl8hx4cvp06ya48d407s9yd1cdvbzb3fjniqs1ykjxcw")))

