(define-module (crates-io ce ll cell_derive) #:use-module (crates-io))

(define-public crate-cell_derive-0.1.0 (c (n "cell_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0qr3d78z1frqam9h9363s3m5kkj0d35gmd9qf37fzi0nh55i2vb1")))

