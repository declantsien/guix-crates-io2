(define-module (crates-io ce ll cellopt) #:use-module (crates-io))

(define-public crate-cellopt-0.1.0 (c (n "cellopt") (v "0.1.0") (h "097vz1zbvgqlznhkyhd7l41rxy5dg6f80c1w74sn3njk1hi8j0fd")))

(define-public crate-cellopt-0.2.0 (c (n "cellopt") (v "0.2.0") (h "119qsgx1fxra9shniq9yhz3cc7pky1ar6qwznd29074dzm6s5jd8")))

(define-public crate-cellopt-0.3.0 (c (n "cellopt") (v "0.3.0") (h "08ib2rdm7pd79mhzdxnkizmax7wkl95kp34g7c9prpjkcq0mv81m")))

