(define-module (crates-io ce ll cellular_raza-concepts-derive) #:use-module (crates-io))

(define-public crate-cellular_raza-concepts-derive-0.0.4 (c (n "cellular_raza-concepts-derive") (v "0.0.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0210xfclsil9jzqghpv5d6mgix5118bibfliqbbhz1h5j0fg28dm") (y #t)))

(define-public crate-cellular_raza-concepts-derive-0.0.5 (c (n "cellular_raza-concepts-derive") (v "0.0.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0f08m0ziz7h8irr5jsx2v6hwh5qwkgw7x93dn0q99jqv6c69r5j4")))

(define-public crate-cellular_raza-concepts-derive-0.0.6 (c (n "cellular_raza-concepts-derive") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zbr4rrjw71l872ybg527d1jsz9g7arh8abn9n8b24s7n9yks0ip")))

(define-public crate-cellular_raza-concepts-derive-0.0.7 (c (n "cellular_raza-concepts-derive") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0apnznm321pivgn6g85b9bv154h59hfshq7zs6lsd2and7rn2i6h")))

(define-public crate-cellular_raza-concepts-derive-0.0.8 (c (n "cellular_raza-concepts-derive") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0izxxhw6bd0idb2yskl80zimw9g1qmkfcwzf979rmf1ba8id5apq")))

