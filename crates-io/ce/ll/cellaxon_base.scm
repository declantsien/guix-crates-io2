(define-module (crates-io ce ll cellaxon_base) #:use-module (crates-io))

(define-public crate-cellaxon_base-22.6.1 (c (n "cellaxon_base") (v "22.6.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0pc5vsxmnq1pdzfhpaq40pcg29w1aymki5mpknj6f39lvcnxd02v") (y #t)))

(define-public crate-cellaxon_base-22.6.2 (c (n "cellaxon_base") (v "22.6.2") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1ci5ahzx0x14l7r111lq3xyjq62qlrj95qrhpw5zxn43y2i90xwc") (y #t)))

(define-public crate-cellaxon_base-22.7.1 (c (n "cellaxon_base") (v "22.7.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "119qzll11i20knjx7xzajy41ak71v71nc17vhabqrihqlswp1fdj") (y #t)))

(define-public crate-cellaxon_base-22.9.1 (c (n "cellaxon_base") (v "22.9.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0d9ybccrwr23hxvhy933wklwp0lvf9fr1lg4ckqg6saxl0xh28jh") (y #t)))

(define-public crate-cellaxon_base-22.9.2 (c (n "cellaxon_base") (v "22.9.2") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0s8mmvb6yw937pj6jhgjwmpzlc4wxb8pcslav8q578plbjncqgjk") (y #t)))

(define-public crate-cellaxon_base-22.9.3 (c (n "cellaxon_base") (v "22.9.3") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1pcxqb8q73azhk0nhxphhz89xfhg97nvqpsdv3gwjxn2hmy8kfsd") (y #t)))

(define-public crate-cellaxon_base-22.10.1 (c (n "cellaxon_base") (v "22.10.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0plhv7szr2yp8il3n493l40rz91ir7vhgl6whf1ajf6gw7j3p1r7")))

