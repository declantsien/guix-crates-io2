(define-module (crates-io ce rm cermic) #:use-module (crates-io))

(define-public crate-cermic-1.0.0 (c (n "cermic") (v "1.0.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1i048x6sbwkdv2zgyybclv6clymbyph4gap5iq8vbk57z3i28hxa")))

