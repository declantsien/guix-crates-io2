(define-module (crates-io ce et ceetle_macros) #:use-module (crates-io))

(define-public crate-ceetle_macros-0.1.0 (c (n "ceetle_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("parsing"))) (d #t) (k 0)))) (h "18f8zr4bh396ix01vcvphb23zkwakaz6ff8zm1g5n61qw8fdg664")))

