(define-module (crates-io ce vi cevio) #:use-module (crates-io))

(define-public crate-cevio-0.1.0 (c (n "cevio") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_Com" "Win32_System_Ole"))) (d #t) (k 0)))) (h "04ryiqnfiqr1fnnqzf8x69wm2hs9ll1a34i9bxj4a2604wlqvig4")))

