(define-module (crates-io ce le celestia_generators) #:use-module (crates-io))

(define-public crate-celestia_generators-0.1.0 (c (n "celestia_generators") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "08cj640jjj4j4cghf7xw9jvvgz80d3vzqhxngairklkwhmx33h0g")))

(define-public crate-celestia_generators-0.1.1 (c (n "celestia_generators") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0a2hnp875dmc4dm6c3gjidnvnzagl4kk9ih4nk2jzapy6sdw9356")))

(define-public crate-celestia_generators-0.2.0 (c (n "celestia_generators") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.0") (d #t) (k 0)))) (h "18gzxh26sphgp807fyapkh40wisxxr2vciwv1g900k0sd3ly8a40")))

(define-public crate-celestia_generators-0.2.1 (c (n "celestia_generators") (v "0.2.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.0") (d #t) (k 0)))) (h "1mdf5hzp4ss1am8zhmm7h5awigd1nl8kmc1rmw2j3viimj871kkr")))

