(define-module (crates-io ce le celes) #:use-module (crates-io))

(define-public crate-celes-1.0.0 (c (n "celes") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1j9qzlrlc2s5gdwyirgqk3g68lvm32asamav0i23sb5v002fs3kd")))

(define-public crate-celes-1.0.1 (c (n "celes") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0i9mfh64w97r34ajjfrwzgf3jgx67h4b4w5crkg20551hc4dqaq3")))

(define-public crate-celes-1.0.2 (c (n "celes") (v "1.0.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0dg0plm0cbljpp7b8cm8qqpi71hqwcivfawcy8a2hgjjlhl9nsl9")))

(define-public crate-celes-1.0.3 (c (n "celes") (v "1.0.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0wp9a863hh72sm924iw4p07f6wsfd1m389ap796769wjydlzb5v6")))

(define-public crate-celes-1.0.4 (c (n "celes") (v "1.0.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0av4zm68mpijb0m5plh34ppcbc2vib19wjqv6wdl40mab4x4b671")))

(define-public crate-celes-1.0.5 (c (n "celes") (v "1.0.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0nbzv6vxgir3xbb4v8pmr6cw2lpyas2dqsghvlgwj9xy5vcxb0wc")))

(define-public crate-celes-1.0.6 (c (n "celes") (v "1.0.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "04zc3gpmv6w53f27rds5kli4393vpm57gmmkzn5mfgl1v6pywqca")))

(define-public crate-celes-2.0.0 (c (n "celes") (v "2.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0m1xk7dkqw9d42lif8gy5azdccgzhfkffc2k1mqdpz3np8jjh3lg")))

(define-public crate-celes-2.1.0 (c (n "celes") (v "2.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "043kyk2bn0rdr6bcwlkskzhp7a8bnwv8vjrqp9rfvbi3l7lk0w0c")))

(define-public crate-celes-2.2.0 (c (n "celes") (v "2.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0y618himhz5b82jjyxb9fp5dsmy8qmdmxrhl2zmf4hbs1ag682z3")))

(define-public crate-celes-2.3.0 (c (n "celes") (v "2.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "06p1xxsia26nwy5nkqppkbnxizkdg9fdabv9cry8kkiglcf1wpcq")))

(define-public crate-celes-2.4.0 (c (n "celes") (v "2.4.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0n5smjv84kznbr7h21y62dl36h9329lg19d9x34pqpcjfc9a5f9r")))

