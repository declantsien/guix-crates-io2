(define-module (crates-io ce le celeste_rs) #:use-module (crates-io))

(define-public crate-celeste_rs-0.1.0 (c (n "celeste_rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.31") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-aux") (r "^4.4") (d #t) (k 0)))) (h "0jncsfddv7bjgcvj4dk7yyzmhb4gnz0awycc4q492rb9h8j5s5pw")))

(define-public crate-celeste_rs-0.2.0 (c (n "celeste_rs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.31") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-aux") (r "^4.4") (d #t) (k 0)))) (h "0h0nzf5nq95ym1ylskj8i2w7qq0kqkawrbfqn3ihsidrrcsdb38s")))

(define-public crate-celeste_rs-0.2.1 (c (n "celeste_rs") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.31") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-aux") (r "^4.4") (d #t) (k 0)))) (h "02dah9122indgsgjbf78h0bj0fs8g1pb19syprr1vl9yx17a8z39")))

