(define-module (crates-io ce le celery-codegen) #:use-module (crates-io))

(define-public crate-celery-codegen-0.1.0-alpha.2 (c (n "celery-codegen") (v "0.1.0-alpha.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "00d9nn2par61prxq3dsb7d59y4qlrcp3l9b60hjb14lc3msai36s")))

(define-public crate-celery-codegen-0.1.0-alpha.3 (c (n "celery-codegen") (v "0.1.0-alpha.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1g2nswpyjv7wgr7pc59f8q3ph6nds3gaq0flhqn2ynm2dlxz8hwn")))

(define-public crate-celery-codegen-0.1.0-alpha.4 (c (n "celery-codegen") (v "0.1.0-alpha.4") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1g7cpsdjr1wzb8gpvrrmx0dnfdhsva06098ah3bbhnz0bmddmmfi")))

(define-public crate-celery-codegen-0.1.0-alpha.5 (c (n "celery-codegen") (v "0.1.0-alpha.5") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1k8r578v6pqwcqz98psha4i63hq0rjin2fg8bj8fn7vcfzfwg1dc")))

(define-public crate-celery-codegen-0.1.0-alpha.6 (c (n "celery-codegen") (v "0.1.0-alpha.6") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "11nynq54yd2i3p9m1y9dcghc8zamal88bgqr953ilpswl0wq7kzd")))

(define-public crate-celery-codegen-0.1.0-alpha.7 (c (n "celery-codegen") (v "0.1.0-alpha.7") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1qc8q8jhicjpjzjkppxdnixgzxlcv3ckb4npsy7f7ic025a6926c")))

(define-public crate-celery-codegen-0.1.0-alpha.8 (c (n "celery-codegen") (v "0.1.0-alpha.8") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0dilfmsmb8sp4vx0v2y4a7bcss5sanldar0n3wcj1hgzdh2hz39j")))

(define-public crate-celery-codegen-0.1.0 (c (n "celery-codegen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "10hh1fymx1fay2b209i3i8bagv9qnnig9g16skk6a6warz1fxslj")))

(define-public crate-celery-codegen-0.1.1-alpha.1 (c (n "celery-codegen") (v "0.1.1-alpha.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1ipp6hrf3hy5253d0g6709w27jcsf2mlc0hzv0syzicgs9fz7150")))

(define-public crate-celery-codegen-0.1.1-alpha.2 (c (n "celery-codegen") (v "0.1.1-alpha.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "19hdl3dd8x38f6f84zawfbbp8gyhqijwxaf56xj550hs7jzl2igz")))

(define-public crate-celery-codegen-0.1.1-alpha.3 (c (n "celery-codegen") (v "0.1.1-alpha.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0ymk35zv684aziycbjn1ndbflnjkdxg5mp8idnff6jfg31lsfacz")))

(define-public crate-celery-codegen-0.1.1 (c (n "celery-codegen") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1hn68alr5d3233nd44f9czwxbfs0sxj93zszxazympk7ibx53g5d")))

(define-public crate-celery-codegen-0.2.0-alpha.0 (c (n "celery-codegen") (v "0.2.0-alpha.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0rkxmmciv9jw4vkb8h5fkj0fi3j7c0shdqv6942qxj7kq9brgbsm")))

(define-public crate-celery-codegen-0.2.0-alpha.1 (c (n "celery-codegen") (v "0.2.0-alpha.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0pbhy67whjx2jq14n5plwm4r26w7i1xpaw4dkjq9d7fk9fzz5rwl")))

(define-public crate-celery-codegen-0.2.0-alpha.2 (c (n "celery-codegen") (v "0.2.0-alpha.2") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "124c7yvjc0flzy29y3npq34h5mg44ny2ghpahj28shqwjq99b0nr")))

(define-public crate-celery-codegen-0.2.0-alpha.3 (c (n "celery-codegen") (v "0.2.0-alpha.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "16a1dy128dpvkdhgwk3m8bfj698az1b29v891vqrsmpfknwlb8d6")))

(define-public crate-celery-codegen-0.2.0 (c (n "celery-codegen") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0d5nxsz5h5iz8m4wyhcxajdc70l71padilmsz9fkpn71z3hn0p4v")))

(define-public crate-celery-codegen-0.2.1 (c (n "celery-codegen") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "16qrkwhjqyadlriv8mg2z34r2iwv2k6w9j0d6jxa7gqgnhfrsaj4")))

(define-public crate-celery-codegen-0.2.2 (c (n "celery-codegen") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1b08vn4spgfbhyis0wj4ymwcb0qzfbg0ma8i1b74pky64gsnbls9")))

(define-public crate-celery-codegen-0.2.3 (c (n "celery-codegen") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0f6a6j5pf2r2xgmjrr1k2lsc5l4f43zwrc4ha0fz1vipkgvb18hj")))

(define-public crate-celery-codegen-0.2.4 (c (n "celery-codegen") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1mkv2cpj1974r51cz2yhbifsx4zg4s5544birv3hfbn9s6zxcjb7")))

(define-public crate-celery-codegen-0.2.5 (c (n "celery-codegen") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "059jl1sm554afwnd6lffkxsan6q4iy39891d1s3shqifldzb1rwd")))

(define-public crate-celery-codegen-0.2.6 (c (n "celery-codegen") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1agnlp0jzp3z2iz6xpy7d8rr7804246cf8wqdm71z9v8mickyy6l")))

(define-public crate-celery-codegen-0.3.0 (c (n "celery-codegen") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "164w71nyhvkwzkwlzc11ra8bzw7dc3fxpgqsdz1y9gns750zn9gc")))

(define-public crate-celery-codegen-0.3.1 (c (n "celery-codegen") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0qcs5j32ffh20xsi8kdn1x31hcsnabb7shlr1g0zw5sgks1yigb0")))

(define-public crate-celery-codegen-0.4.0-rc1 (c (n "celery-codegen") (v "0.4.0-rc1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1ahnfp7sbgvgp784mariggqvdl1zagn4c7mnjfilczidlbmzfgbb")))

(define-public crate-celery-codegen-0.4.0-rc2 (c (n "celery-codegen") (v "0.4.0-rc2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1m2b6k24d7mnbg78yi54g38xnwpwi64vp32wm6d6y0qjk5cnxbzv")))

(define-public crate-celery-codegen-0.4.0-rc3 (c (n "celery-codegen") (v "0.4.0-rc3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1zvdy9lxb9n8wzdbw7wn2j7cyh8v1z3fxxz7d2vba5dhrlkdf400")))

(define-public crate-celery-codegen-0.4.0-rc4 (c (n "celery-codegen") (v "0.4.0-rc4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1063iszd3qr5q7adz5x87s8gi189954b20mh3xpr83vinxb1y33j")))

(define-public crate-celery-codegen-0.4.0-rc5 (c (n "celery-codegen") (v "0.4.0-rc5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0lr9gwg4j75qqgq8afc8vsm6ylmgiyzd8q8qh7sw4igxjmnczvf5")))

(define-public crate-celery-codegen-0.4.0-rc6 (c (n "celery-codegen") (v "0.4.0-rc6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0fcg944wqv19jpcf16qakmwl8bzhhz01p6aara2cvf65yg54vya0")))

(define-public crate-celery-codegen-0.4.0-rc7 (c (n "celery-codegen") (v "0.4.0-rc7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1zq7xbpgrnkyvzvy0wl2wyg43yclm1p9lqvcmyz2mv7k6allfjh0")))

(define-public crate-celery-codegen-0.4.0-rc8 (c (n "celery-codegen") (v "0.4.0-rc8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1sp8sha5n9z5zwiip1c7ar2qsbxyj4dsc4wz3ahyqmvfs7vcby3a")))

(define-public crate-celery-codegen-0.4.0-rc9 (c (n "celery-codegen") (v "0.4.0-rc9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1jp6phmk7k242igy7ml3dl4y0dak6sgw1w16i8d6i2102g5ag03h")))

(define-public crate-celery-codegen-0.4.0-rc10 (c (n "celery-codegen") (v "0.4.0-rc10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "01yb94vdylczy127sh89mv7bwk2q43j5rzi3k9vqq2x6aiibfsln")))

(define-public crate-celery-codegen-0.4.0-rcn.11 (c (n "celery-codegen") (v "0.4.0-rcn.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0mm0awdjskmm8qxcgn1r3lwkn7yy5dy1aaar0fb556vhy6za807x")))

(define-public crate-celery-codegen-0.4.0 (c (n "celery-codegen") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0gdqa6ryw0xyy1j7fav8yn9k3pw0jildckzwzb2hxf6j7m6l1vpq")))

(define-public crate-celery-codegen-0.5.0 (c (n "celery-codegen") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "10cvv4rfgq97qjpj22l5ab2a4zqy1waj04z61m2bcs7nz3y825pj")))

(define-public crate-celery-codegen-0.5.1 (c (n "celery-codegen") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1sya0niq2f5wmz8ni839mr8kngmcv8snj7chg60xfn4gqj15fa56")))

(define-public crate-celery-codegen-0.5.2 (c (n "celery-codegen") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1qzx8jwzlhnnnps87xki4v2jz7axgd0c603434k5y629rdpi6wvx")))

(define-public crate-celery-codegen-0.5.3 (c (n "celery-codegen") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0i0mz77zb3c3cr3g6vzbi22pphic7r9fq7jlqm0r9nfm8pfg0ilm")))

(define-public crate-celery-codegen-0.5.4 (c (n "celery-codegen") (v "0.5.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0jds0nimax4zhxqam4563sj0j9042k2784wcl3ijsiywk3q7hdhl")))

(define-public crate-celery-codegen-0.5.5 (c (n "celery-codegen") (v "0.5.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "03kiy7y5n620g31kr8s8y8dh5vyqp3zabdgsck610cll80s4y3xc")))

