(define-module (crates-io ce le celeste_derive) #:use-module (crates-io))

(define-public crate-celeste_derive-1.0.0 (c (n "celeste_derive") (v "1.0.0") (d (list (d (n "celeste") (r "^1.0.1") (k 2)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)))) (h "01rcm73wcbq4spw7gd674amcs0pghm7g6ngs3ckk3l6kpqazj725")))

