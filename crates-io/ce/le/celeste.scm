(define-module (crates-io ce le celeste) #:use-module (crates-io))

(define-public crate-celeste-0.0.1 (c (n "celeste") (v "0.0.1") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)))) (h "1ybmaq163a1yq88mrgysxxjghmv01gc0xmrhvphyaawc44vc877y")))

(define-public crate-celeste-0.0.2 (c (n "celeste") (v "0.0.2") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)))) (h "11w9q97b3ca80mi5nqakfg45ash2vp9x7x45gagx9cknzzwqay2h")))

(define-public crate-celeste-0.0.3 (c (n "celeste") (v "0.0.3") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)) (d (n "nom-varint") (r "^0.1.0") (d #t) (k 0)))) (h "1jh4varblyq98ywm6f3v9zr6vxriqlss72gccsk27mjdyfc3jv25")))

(define-public crate-celeste-0.0.4 (c (n "celeste") (v "0.0.4") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)) (d (n "nom-varint") (r "^0.1.0") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.2.2") (d #t) (k 0)))) (h "04vpn7cv43r9b1ypxqmrzrb4krlxm821lxg5hi9zjwsizc9c0r85")))

(define-public crate-celeste-0.1.0 (c (n "celeste") (v "0.1.0") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)) (d (n "nom-varint") (r "^0.1.0") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.2.2") (d #t) (k 0)))) (h "1nd9k43fcfsr5n3x8jlwyr0jqgs6vhsfnf29rp3xvn09sjh7fy4h")))

(define-public crate-celeste-0.1.1 (c (n "celeste") (v "0.1.1") (d (list (d (n "nom") (r "^4.1") (d #t) (k 0)) (d (n "nom-varint") (r "^0.1.0") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.2.2") (d #t) (k 0)))) (h "075gh28w3awr39a5fqm1zbgl1y6pj2z755mwwa7kjlb8d6n6n88f")))

(define-public crate-celeste-1.0.0 (c (n "celeste") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (k 2)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "nom") (r "^4.1") (d #t) (k 0)) (d (n "nom-varint") (r "^0.1.0") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.2.2") (d #t) (k 0)))) (h "0vas50vb9vpb2r8j54hdhrhkjx386w3a8qsjgig6l3bbrw7c65fc")))

(define-public crate-celeste-1.0.1 (c (n "celeste") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (k 2)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "nom") (r "^4.1") (d #t) (k 0)) (d (n "nom-varint") (r "^0.1.0") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.2.2") (d #t) (k 0)))) (h "099p6dl3l4y7afmb6v1fn8jca9gl8a3k4079968ysyirm034l1pr")))

(define-public crate-celeste-2.0.0 (c (n "celeste") (v "2.0.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "celeste_derive") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (k 2)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "nom") (r "^4.1") (d #t) (k 0)) (d (n "nom-varint") (r "^0.1.0") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.2.2") (d #t) (k 0)))) (h "066w411z3im19hbbnrwxkcv98j0xr9l11z5cnwsdg3aah19x5h5b") (f (quote (("derive" "celeste_derive") ("default" "derive"))))))

(define-public crate-celeste-2.0.1 (c (n "celeste") (v "2.0.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "celeste_derive") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (k 2)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "nom") (r "^4.1") (d #t) (k 0)) (d (n "nom-varint") (r "^0.1.0") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.2.2") (d #t) (k 0)))) (h "0plgn3zh7xsfcd663css1ihlf0j58dk7fabmm9rkgzsdpacm9fhx") (f (quote (("derive" "celeste_derive") ("default" "derive"))))))

