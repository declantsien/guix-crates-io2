(define-module (crates-io ce le celestedebugrc) #:use-module (crates-io))

(define-public crate-celestedebugrc-0.1.0 (c (n "celestedebugrc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9") (k 0)))) (h "1ag33hgwflv81vgn15sni6xvb6mc58v69f88mj7kspki77ppgyhv")))

(define-public crate-celestedebugrc-0.2.0 (c (n "celestedebugrc") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9") (k 0)))) (h "1hd0nyvwvfcp4i4rbmvr5f9z461q57sky93c2qgd4vbr29q9ig91")))

(define-public crate-celestedebugrc-0.3.0 (c (n "celestedebugrc") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9") (k 0)))) (h "0dvjw8mlmiqj9p29pdzm8wvzvr85kahycw6lc54qp5dqimid2yps")))

(define-public crate-celestedebugrc-0.3.1 (c (n "celestedebugrc") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9") (k 0)))) (h "14i5csnshpivrik7q2s0rbf25rhh68k7j0c3y6j2dk5yl4h7f2s7")))

