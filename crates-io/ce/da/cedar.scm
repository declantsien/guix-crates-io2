(define-module (crates-io ce da cedar) #:use-module (crates-io))

(define-public crate-cedar-0.0.1 (c (n "cedar") (v "0.0.1") (d (list (d (n "cocoa") (r "^0.7") (d #t) (k 0)) (d (n "crossbeam") (r "^0.2.10") (d #t) (k 0)) (d (n "objc") (r "^0.2") (d #t) (k 0)))) (h "159b2fj3rzgr5gvxpwxrfa1dpd90x1fp38a6nkyprcfr5hsjm6ki")))

(define-public crate-cedar-0.1.0 (c (n "cedar") (v "0.1.0") (d (list (d (n "cocoa") (r "^0.7") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "core-graphics") (r "^0.6") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "crossbeam") (r "^0.2.10") (d #t) (k 0)) (d (n "gtk") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "objc") (r "^0.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "0r0k5c6fz7cib724zyrs972ivn2g47rljvdj3mfil3xr87hc4q8i") (f (quote (("gtk3" "gtk") ("default"))))))

(define-public crate-cedar-0.1.1 (c (n "cedar") (v "0.1.1") (d (list (d (n "cocoa") (r "^0.7") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "core-graphics") (r "^0.6") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "crossbeam") (r "^0.2.10") (d #t) (k 0)) (d (n "gtk") (r "^0.1.2") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "gtk") (r "^0.1.2") (o #t) (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "objc") (r "^0.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "1562ci29cqavym6823k64azvp5zfzmjdy1dja4hl4da23abnqfq9")))

