(define-module (crates-io ce da cedar-java-ffi) #:use-module (crates-io))

(define-public crate-cedar-java-ffi-3.1.0 (c (n "cedar-java-ffi") (v "3.1.0") (d (list (d (n "cedar-policy") (r "^3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "jni") (r "^0.21.0") (d #t) (k 0)) (d (n "jni_fn") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0sdkm4g183088dx2613567ipbvij7kspbrd80f78a2067qspccfj") (f (quote (("partial-eval" "cedar-policy/partial-eval")))) (y #t)))

(define-public crate-cedar-java-ffi-3.2.0 (c (n "cedar-java-ffi") (v "3.2.0") (d (list (d (n "cedar-policy") (r "^3.2") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "jni") (r "^0.21.0") (d #t) (k 0)) (d (n "jni_fn") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0q5sh3ivh7mrq1d9870vgyrd6471d5mcxkv9mf51hskn1bkvqfsr") (f (quote (("partial-eval" "cedar-policy/partial-eval")))) (y #t)))

