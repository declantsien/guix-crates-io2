(define-module (crates-io ce c- cec-alsa-sync) #:use-module (crates-io))

(define-public crate-cec-alsa-sync-1.0.0 (c (n "cec-alsa-sync") (v "1.0.0") (d (list (d (n "cec-rs") (r "^1.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0ficgmn0dyqs04bb505wmlyxly5cxmp6qp1br76d3crd2qf9dm4k")))

(define-public crate-cec-alsa-sync-1.0.1 (c (n "cec-alsa-sync") (v "1.0.1") (d (list (d (n "cec-rs") (r "^1.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1d57q0kk6227cjz3yr45hqywjpy8xsrqj6mqr03qffdrrfsjwqw3")))

(define-public crate-cec-alsa-sync-1.0.2 (c (n "cec-alsa-sync") (v "1.0.2") (d (list (d (n "cec-rs") (r "^1.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "05agw7vv6gggnw6k4mmdyp3d5h90msgciv14268qsagpfv8475ga")))

(define-public crate-cec-alsa-sync-1.0.3 (c (n "cec-alsa-sync") (v "1.0.3") (d (list (d (n "cec-rs") (r "^1.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1jz5jy5xprbdcf36lak0y1vi32s4y87rswcv7gz4y37mz5g9xss9")))

(define-public crate-cec-alsa-sync-1.0.4 (c (n "cec-alsa-sync") (v "1.0.4") (d (list (d (n "cec-rs") (r "^1.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "08aw09s2fy96lvjmxgri5kai17pgna5s6c0x3rlxsm1dq38j0gzw")))

(define-public crate-cec-alsa-sync-1.0.5 (c (n "cec-alsa-sync") (v "1.0.5") (d (list (d (n "cec-rs") (r "^1.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0xw40rg737d647jpprvj7slj9lli91v2gkzzkyan4hvhym3mr5i2")))

(define-public crate-cec-alsa-sync-1.0.6 (c (n "cec-alsa-sync") (v "1.0.6") (d (list (d (n "cec-rs") (r "^2.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "00cxsa184k6f8ibb9lasjd9n29wnvsa3m43nyphif5kanif7mvcf")))

(define-public crate-cec-alsa-sync-1.0.7 (c (n "cec-alsa-sync") (v "1.0.7") (d (list (d (n "cec-rs") (r "^2.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "15x5gsra0vgcmvws1n7fnkd6gbr99560hdgz9ri87pi8zgxpzqyq")))

(define-public crate-cec-alsa-sync-1.0.8 (c (n "cec-alsa-sync") (v "1.0.8") (d (list (d (n "cec-rs") (r "^2.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1sx9k0n6r0pdpq4fh2g79zzkrqxhk5f6icfydb5b2wkdygn4g0pw")))

(define-public crate-cec-alsa-sync-1.0.10 (c (n "cec-alsa-sync") (v "1.0.10") (d (list (d (n "cec-rs") (r "^2.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0cp8s6brbc9cxmj1s8qkwrv26lwv6drjfh3n1sl7343kvc5v0j3l")))

(define-public crate-cec-alsa-sync-1.0.11 (c (n "cec-alsa-sync") (v "1.0.11") (d (list (d (n "cec-rs") (r "^2.0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1jdgvvkfgbf6cb04gmsly39cwqw3xzirwmv4lb4m064x3rz2g8b0")))

(define-public crate-cec-alsa-sync-1.0.12 (c (n "cec-alsa-sync") (v "1.0.12") (d (list (d (n "cec-rs") (r "^2.0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0am27kk8lbldhn467nxxzbrsz0l672mgfs5n65yfqklxcy8a3gw7")))

(define-public crate-cec-alsa-sync-1.0.13 (c (n "cec-alsa-sync") (v "1.0.13") (d (list (d (n "cec-rs") (r "^2.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1lrmgyfqdq47kcb9qh07l0w6351mcs3jb8aci2j0g3h01jz2yy7y")))

(define-public crate-cec-alsa-sync-1.0.14 (c (n "cec-alsa-sync") (v "1.0.14") (d (list (d (n "cec-rs") (r "^2.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0nkfv1fbs9is2ki9zsk6c7p820f52g1yqmxxyk8i1rgspwi13nmy")))

(define-public crate-cec-alsa-sync-1.0.15 (c (n "cec-alsa-sync") (v "1.0.15") (d (list (d (n "cec-rs") (r "^2.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1siqfyy44z28lbsx6jqp0i1kwnxhr5rwhsqahb4h15kms7w1rq9f")))

(define-public crate-cec-alsa-sync-1.0.17 (c (n "cec-alsa-sync") (v "1.0.17") (d (list (d (n "cec-rs") (r "^2.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "18mvjchxyzxcdfwd51nbl6hv3lcw0j6srf3097zk48scp08mz0j5")))

(define-public crate-cec-alsa-sync-1.0.18 (c (n "cec-alsa-sync") (v "1.0.18") (d (list (d (n "cec-rs") (r "^2.2.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0r1i91vqzkydlr4q2w5nrksmsm4lmjyrd5aj3iwci5qy6435wyhk")))

(define-public crate-cec-alsa-sync-1.0.19 (c (n "cec-alsa-sync") (v "1.0.19") (d (list (d (n "cec-rs") (r "^2.2.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0jy6phvgfqy6y5m2dbqfy01jccn0xzmgw03lsiv7v48jj6n5gnq2")))

(define-public crate-cec-alsa-sync-1.2.1 (c (n "cec-alsa-sync") (v "1.2.1") (d (list (d (n "cec-rs") (r "^3.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1p70p00via25p9a30w3wn7x0765nm8nzdiwcxr3mgrcig5cvrgia")))

(define-public crate-cec-alsa-sync-2.0.0 (c (n "cec-alsa-sync") (v "2.0.0") (d (list (d (n "cec-rs") (r "^4.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "184rwnb1kq2friaqzwlggq9pphqkfmyzjhz5ljg4ccqzc7rhsrd0")))

(define-public crate-cec-alsa-sync-3.0.0 (c (n "cec-alsa-sync") (v "3.0.0") (d (list (d (n "cec-rs") (r "^6.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1vkb0r8xwg3582mz78dqrb8ajkak8cj9aajwa3r2nqcfwazmyglr")))

(define-public crate-cec-alsa-sync-3.0.1 (c (n "cec-alsa-sync") (v "3.0.1") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "cec-rs") (r "^6.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1dl78qwk35x5zla2lnj7fvqfp78d069nibm2czd5j2yagxzffl9p")))

(define-public crate-cec-alsa-sync-3.0.2 (c (n "cec-alsa-sync") (v "3.0.2") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "cec-rs") (r "^6.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "1dfxpqg6xqhjflvr1yymppmjk4m2vkigmx8yzgvvcf3jn6ndrj08")))

(define-public crate-cec-alsa-sync-3.0.3 (c (n "cec-alsa-sync") (v "3.0.3") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "cec-rs") (r "^6.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0yqw2jn5f9f38167dra50b698cdhak2lmmlfhyb1grci9xn034r5")))

