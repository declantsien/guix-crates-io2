(define-module (crates-io ce c- cec-rs) #:use-module (crates-io))

(define-public crate-cec-rs-0.1.0 (c (n "cec-rs") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "libcec-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1h0x9f4m9g2crfwinvzfqzrb4dahn273xmsjl2ag15qi76a7r2nh")))

(define-public crate-cec-rs-1.0.0 (c (n "cec-rs") (v "1.0.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "libcec-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "04w8lrhirjv7rf2cxczfwp6nf7fwhdml3c1vd6pzw60r22k8ll29")))

(define-public crate-cec-rs-1.1.0 (c (n "cec-rs") (v "1.1.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "libcec-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0a678ci77c9q2r6yin2q34mj6yxlfxxnisy9qwxjpzaa2zdg1fii")))

(define-public crate-cec-rs-1.1.1 (c (n "cec-rs") (v "1.1.1") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "libcec-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0igj2wwyzl0qgbngri4gfvd1pwj7h7yzwsxbgxkgxr0bm2fingvm")))

(define-public crate-cec-rs-2.0.0 (c (n "cec-rs") (v "2.0.0") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "enum-repr-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "libcec-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "03lwz2b8w1wzf4z9dq26i6cgszw91wzi1p3wwgb12v6rnrxkh4lj")))

(define-public crate-cec-rs-2.0.1 (c (n "cec-rs") (v "2.0.1") (d (list (d (n "arrayvec") (r "^0.5.1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "enum-repr-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "libcec-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "0k6cnrsm7ykqxxyhn6x1h2bjx62zrb4x3pvpsd4fsb3bxgf86nx2")))

(define-public crate-cec-rs-2.0.2 (c (n "cec-rs") (v "2.0.2") (d (list (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "enum-repr-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "libcec-sys") (r "^1.0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "10mlkhp4zcrjvxxykpvcszdhqr8wz813siq3yrp7p5a1jcrwm19a")))

(define-public crate-cec-rs-2.0.3 (c (n "cec-rs") (v "2.0.3") (d (list (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "enum-repr-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "libcec-sys") (r "^1.0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "03l6pwydhmnv88pfrw09g3qlzw10i7z9lybr5hd6zb7qgj9r4vqj")))

(define-public crate-cec-rs-2.1.0 (c (n "cec-rs") (v "2.1.0") (d (list (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "enum-repr-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "libcec-sys") (r "^1.0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "11vg48wig75rr4n0wxsq1rdzk135q5vwy8dji18zyzdz90r6c8i9")))

(define-public crate-cec-rs-2.2.0 (c (n "cec-rs") (v "2.2.0") (d (list (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "enum-repr-derive") (r "^0.1.4") (d #t) (k 0)) (d (n "libcec-sys") (r "^1.0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1cbc3b6bj0vf6pj7pvkfpp9im7r6hg77rc2j523vpny2ljfb9mw9")))

(define-public crate-cec-rs-2.2.1 (c (n "cec-rs") (v "2.2.1") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "enum-repr-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "libcec-sys") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1whi0qx7q34pdjrbiycrrgvr6fwpdr8hdf3wqf8nik2fqlrpkgfs")))

(define-public crate-cec-rs-2.2.2 (c (n "cec-rs") (v "2.2.2") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "enum-repr-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "libcec-sys") (r "^1.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0im3cd3bbh2hwcd81xkbx74yi20xr9p8f9k0sa4cqnb55jqhcaip")))

(define-public crate-cec-rs-3.0.0 (c (n "cec-rs") (v "3.0.0") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "enum-repr-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "libcec-sys") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1zdkp0zx9z1a4bl94qnifxvvc2ahkmw898n37ywwhb2ymnpcyl2k")))

(define-public crate-cec-rs-4.0.0 (c (n "cec-rs") (v "4.0.0") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "enum-repr-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "libcec-sys") (r "^3.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1vb5lhf7bcq43w4l5kn232cp7drffb9bnmlxf86qx3jvxq5cv8am")))

(define-public crate-cec-rs-5.0.0 (c (n "cec-rs") (v "5.0.0") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "enum-repr-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "libcec-sys") (r "^3.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "12g1b5ip28nn59v7w42gjlpzm2bxpa669laj7pp0zhl637gmvaa8")))

(define-public crate-cec-rs-6.0.0 (c (n "cec-rs") (v "6.0.0") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "enum-repr-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "libcec-sys") (r "^4.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "12vpryp9z901cvzwj0lc755bbag2wp4qnwh46i8kd6bc6sl80mng")))

(define-public crate-cec-rs-7.0.0 (c (n "cec-rs") (v "7.0.0") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "enum-repr-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "libcec-sys") (r "^4.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0wqr9bp53sjr88vnywvmcmc4hjdkyf0vhlzzf9kb63609418fz1h") (r "1.56.1")))

(define-public crate-cec-rs-7.1.0 (c (n "cec-rs") (v "7.1.0") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "enum-repr-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "libcec-sys") (r "^4.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "19wp8n9p3fy26yhkl0wfaryrc2w3hqq0rwww2aqfn3i3wnayqxrr") (r "1.56.1")))

(define-public crate-cec-rs-7.1.1 (c (n "cec-rs") (v "7.1.1") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.10.2") (d #t) (k 0)) (d (n "enum-repr-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "libcec-sys") (r "^4.0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "11p13cw1n54j0ybv7jj46jhvdpi38g7p7qrxf8b049sml2yw2ig1") (r "1.56.1")))

