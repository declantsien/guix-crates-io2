(define-module (crates-io ce c- cec-dpms) #:use-module (crates-io))

(define-public crate-cec-dpms-0.1.0 (c (n "cec-dpms") (v "0.1.0") (d (list (d (n "cec-rs") (r "^4.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.13") (d #t) (k 0)) (d (n "simplelog") (r "^0.11.2") (f (quote ("paris" "ansi_term"))) (d #t) (k 0)))) (h "14qm06r61lbdgbazdbs9kgnykrh5dlb98y71pxlkg40nl1ksv1bc")))

(define-public crate-cec-dpms-0.1.1 (c (n "cec-dpms") (v "0.1.1") (d (list (d (n "cec-rs") (r "^4.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.13") (d #t) (k 0)) (d (n "simplelog") (r "^0.11.2") (f (quote ("paris" "ansi_term"))) (d #t) (k 0)))) (h "1ffcahvc6avr72lxk67my0w6pc8ynmbypbpfsjn8rbg33apg9izc")))

