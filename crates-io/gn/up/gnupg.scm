(define-module (crates-io gn up gnupg) #:use-module (crates-io))

(define-public crate-gnupg-0.1.0 (c (n "gnupg") (v "0.1.0") (h "1rb7adjwcg2drivkw8acwbad8vy6908pxykc5f5mmihlhjmvyjly")))

(define-public crate-gnupg-0.2.0 (c (n "gnupg") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0.5") (d #t) (k 0)))) (h "1nvbq1pvg9fbkfy92hsfij9b0n1szh3j6rkdv28mzv1gw5a4in4y") (y #t)))

