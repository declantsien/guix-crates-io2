(define-module (crates-io gn up gnuplotter_core) #:use-module (crates-io))

(define-public crate-gnuplotter_core-0.1.6-alpha (c (n "gnuplotter_core") (v "0.1.6-alpha") (h "0xhxmd8gd7cxnqr9jq72kw4zzflhmfp66vl0crb4sq8ckvznp8am") (y #t)))

(define-public crate-gnuplotter_core-0.2.2-alpha (c (n "gnuplotter_core") (v "0.2.2-alpha") (d (list (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0x4qqy67998jksbinbl01xigi409kk209326s1ibj0lxxvskhzxn")))

(define-public crate-gnuplotter_core-0.2.3-alpha (c (n "gnuplotter_core") (v "0.2.3-alpha") (d (list (d (n "either") (r "^1.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0p5nsmghliwxxl1yinsydbh8nm77vqg65nbrxv3g28inq4zi1cvz")))

