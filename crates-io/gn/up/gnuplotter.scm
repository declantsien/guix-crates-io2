(define-module (crates-io gn up gnuplotter) #:use-module (crates-io))

(define-public crate-gnuplotter-0.1.0 (c (n "gnuplotter") (v "0.1.0") (h "0wcrxrh1c6k1b9lhmpd911gjrvinf9qydm7hc3gv87m6d7rdy90b") (y #t)))

(define-public crate-gnuplotter-0.1.2-alpha (c (n "gnuplotter") (v "0.1.2-alpha") (h "1isxc19f7270hh61dsgljx2wqnjz6dw44c13qklp6xzs4dl73n5c") (y #t)))

(define-public crate-gnuplotter-0.1.5-alpha (c (n "gnuplotter") (v "0.1.5-alpha") (h "09wv1y32hxvvnlij63hwsv44fjwdm6v4797rvjw9g71kmp1rxfx8") (y #t)))

(define-public crate-gnuplotter-0.1.6-alpha (c (n "gnuplotter") (v "0.1.6-alpha") (d (list (d (n "gnuplotter_core") (r "^0.1.6-alpha") (d #t) (k 0)) (d (n "gnuplotter_macros") (r "^0.1.6-alpha") (d #t) (k 0)))) (h "030grg5cw7mqhlb9dk5ngprb2w3gf8ss0c61myk0p3xpfn1rvn27") (y #t)))

(define-public crate-gnuplotter-0.2.2-alpha (c (n "gnuplotter") (v "0.2.2-alpha") (d (list (d (n "gnuplotter_core") (r "^0.2.2-alpha") (d #t) (k 0)) (d (n "gnuplotter_macros") (r "^0.2.2-alpha") (d #t) (k 0)))) (h "1afb22iwdrcr61j7a6vnvgc4k1vzrgjd2n2l24gl999dlc51ha7v")))

(define-public crate-gnuplotter-0.2.3-alpha (c (n "gnuplotter") (v "0.2.3-alpha") (d (list (d (n "gnuplotter_core") (r "^0.2.3-alpha") (d #t) (k 0)) (d (n "gnuplotter_macros") (r "^0.2.3-alpha") (d #t) (k 0)))) (h "0d7iil7xj72di4vjbbg9kyz3wz5yy55x7ddxclxj3d0z3d4zbs92")))

