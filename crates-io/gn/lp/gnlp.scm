(define-module (crates-io gn lp gnlp) #:use-module (crates-io))

(define-public crate-gnlp-0.1.0 (c (n "gnlp") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rust-bert") (r "^0.20.0") (d #t) (k 0)))) (h "0v65cgsywz1bxbkfm0yp9qgi19aim4cwn00gzkg5z880ggl8ywf2")))

