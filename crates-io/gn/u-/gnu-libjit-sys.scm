(define-module (crates-io gn u- gnu-libjit-sys) #:use-module (crates-io))

(define-public crate-gnu-libjit-sys-0.0.0 (c (n "gnu-libjit-sys") (v "0.0.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0s7wri9sc3bksz0z3bhsql1gb0zcjij4sgwylldxl0yffd1yhyaz")))

(define-public crate-gnu-libjit-sys-0.0.1 (c (n "gnu-libjit-sys") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "045czw6bsr1zlp8m87ajwxxkyf6110hxinc48k0pk4zr2a9gpk3l")))

(define-public crate-gnu-libjit-sys-0.0.2 (c (n "gnu-libjit-sys") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1hpmf71lwg9ifzvcns2ciwf6labjkw01vhsgmpd0gw7qm4m7qsa2")))

(define-public crate-gnu-libjit-sys-0.0.3 (c (n "gnu-libjit-sys") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1hnnbs6897n3677y83simb05ndsy929dpbc2f7gcq8q3aa8cx7h6")))

(define-public crate-gnu-libjit-sys-0.0.5 (c (n "gnu-libjit-sys") (v "0.0.5") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0nvvi4skzfcpc92ba7s9xaapws4a4kngjqvfvrgzxi8530sqzip6")))

(define-public crate-gnu-libjit-sys-0.0.6 (c (n "gnu-libjit-sys") (v "0.0.6") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "04i2pqx558qppyb84b529c2dgb8a09dlqb7csa6ljyk8gg7zg9di")))

(define-public crate-gnu-libjit-sys-0.0.7 (c (n "gnu-libjit-sys") (v "0.0.7") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "101ilvx6p7ml296by4y8wdp9d51kiax6wdfzaql7yzbfa56s309x")))

(define-public crate-gnu-libjit-sys-0.0.8 (c (n "gnu-libjit-sys") (v "0.0.8") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0svsdh8r9d18lvx9msl6z6ik3bajriizpf72mg5b3843cyrxbcqc")))

