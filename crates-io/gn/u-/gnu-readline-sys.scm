(define-module (crates-io gn u- gnu-readline-sys) #:use-module (crates-io))

(define-public crate-gnu-readline-sys-0.1.0 (c (n "gnu-readline-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "106j4jzbxxwsvwz2pzd3b4isnc7fdba8w15rsgrlqp31vbrbfxs4") (l "readline") (r "1.56")))

