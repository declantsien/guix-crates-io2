(define-module (crates-io gn u- gnu-libjit) #:use-module (crates-io))

(define-public crate-gnu-libjit-0.1.1 (c (n "gnu-libjit") (v "0.1.1") (d (list (d (n "gnu-libjit-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1mc1g7bb5wdwhyqc6717whar3brn7wccrxqij2ml4yjqny8a3l19")))

(define-public crate-gnu-libjit-0.2.0 (c (n "gnu-libjit") (v "0.2.0") (d (list (d (n "gnu-libjit-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zkgl2h1ygpkw77lpq221zdbvkzb7yh524lv27v9k8hirn23v5k6")))

(define-public crate-gnu-libjit-0.3.0 (c (n "gnu-libjit") (v "0.3.0") (d (list (d (n "gnu-libjit-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rj5jyjcd53rgzvb7s1zb4gizni6qx4f3r60z6xcxbpjbh9c131i")))

(define-public crate-gnu-libjit-0.4.0 (c (n "gnu-libjit") (v "0.4.0") (d (list (d (n "gnu-libjit-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "04zzp0yxmyz5b37p8zswi0v0manyp371vh7mwvyf6mf7nnah6srd")))

(define-public crate-gnu-libjit-0.5.0 (c (n "gnu-libjit") (v "0.5.0") (d (list (d (n "gnu-libjit-sys") (r "^0.0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fnjxbkshwcm716jf2yiv13f6hpgikb9112fgpxjqfrwdwkn5c6m")))

(define-public crate-gnu-libjit-0.6.0 (c (n "gnu-libjit") (v "0.6.0") (d (list (d (n "gnu-libjit-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rsr7dx004zr4iywir046bm0vlprkp8ppgvi6ankg4rvsj9vw00a")))

(define-public crate-gnu-libjit-0.7.0 (c (n "gnu-libjit") (v "0.7.0") (d (list (d (n "gnu-libjit-sys") (r "^0.0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wv8asv6kaphjvw09vsy3kdbg7shw7qa806chfaasp5m12sdm6sw")))

(define-public crate-gnu-libjit-0.8.0 (c (n "gnu-libjit") (v "0.8.0") (d (list (d (n "gnu-libjit-sys") (r "^0.0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1lza20bmlvz1f551jdklbwwcmpw49b4vcxam3gg47gk8nvazj2pj")))

(define-public crate-gnu-libjit-0.9.0 (c (n "gnu-libjit") (v "0.9.0") (d (list (d (n "gnu-libjit-sys") (r "^0.0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1y0y8lqkiw6hjrgahr20vcffs7983nxjn056is7ccpsznmvnaadh")))

