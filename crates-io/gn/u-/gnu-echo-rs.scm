(define-module (crates-io gn u- gnu-echo-rs) #:use-module (crates-io))

(define-public crate-gnu-echo-rs-0.1.0 (c (n "gnu-echo-rs") (v "0.1.0") (d (list (d (n "ascii_converter") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "10dm0sxybjy5yq7mmmpbkxkvqrxgmq1v9cn2wdw09fdcq7xp2zz8")))

