(define-module (crates-io gn u- gnu-ln) #:use-module (crates-io))

(define-public crate-gnu-ln-0.1.0 (c (n "gnu-ln") (v "0.1.0") (h "1d8gfgg1fj9dfk9l37xv84v7d35psv7l8q7az078cg6zbjkd900i")))

(define-public crate-gnu-ln-0.2.0 (c (n "gnu-ln") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "00j3xgnkknf8a6d78n82b783314mgxr3ja9pxhv500agcimjjq2n")))

