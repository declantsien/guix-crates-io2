(define-module (crates-io gn un gnunet) #:use-module (crates-io))

(define-public crate-gnunet-0.0.1 (c (n "gnunet") (v "0.0.1") (h "1aqh1z2ic3a5bnmgsdx6kpwdl03588qdy1ibnjx9yp8p1wi4mxfl")))

(define-public crate-gnunet-0.0.2 (c (n "gnunet") (v "0.0.2") (h "0lszv4brbhl79q7ghcnk1ilkdnq97xilchv3i2cl4svg4wwnysq5")))

(define-public crate-gnunet-0.0.3 (c (n "gnunet") (v "0.0.3") (h "1dc548q13w5bbjkssd2c90bk1xhbir89ksx7rpjsnjbisd35k3nn")))

(define-public crate-gnunet-0.0.4 (c (n "gnunet") (v "0.0.4") (h "1g0nimnp6zvpn5awarzg0ylq44alrpfxd4k0fs0sjncma5riys7d")))

(define-public crate-gnunet-0.0.5 (c (n "gnunet") (v "0.0.5") (h "04wcqcbj2gpnwb21mil8crh79aq9f13gs3g6avrxz0qhcjkb14rz")))

(define-public crate-gnunet-0.0.6 (c (n "gnunet") (v "0.0.6") (h "1jjsnldygghrv5vpfncg6mqw45h4gj7v5wx7ip56mg0jvprw9pkd")))

(define-public crate-gnunet-0.0.7 (c (n "gnunet") (v "0.0.7") (h "17sicq3ykz2mrpdlsfjrh4zw4vp9230bzxnn5fpcxh9dlcwaikbl")))

(define-public crate-gnunet-0.0.8 (c (n "gnunet") (v "0.0.8") (h "04kcxzzi5hxggy3f5pzzfm3w6wb16kf3p2csnkwklf0wclzvlbgy")))

(define-public crate-gnunet-0.0.9 (c (n "gnunet") (v "0.0.9") (h "1jw92p2j17a033sb0kp7hswfj8mpigbyvmrzdrblrp6mm8qqrcls")))

(define-public crate-gnunet-0.0.10 (c (n "gnunet") (v "0.0.10") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.0") (d #t) (k 0)))) (h "123nzcf57cfk2sc72flk5dd49lipc0aqikqyfvl6bgdrddf3072j")))

(define-public crate-gnunet-0.0.11 (c (n "gnunet") (v "0.0.11") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "error_def") (r "*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.0") (d #t) (k 0)))) (h "1bfixqf4fiz7rm9q7mq60spg0bfq16x0h0l89788xmarc5af6wdw")))

(define-public crate-gnunet-0.0.12 (c (n "gnunet") (v "0.0.12") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "error_def") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "unix_socket") (r "^0.3.0") (d #t) (k 0)))) (h "1adw967m6ldffa3qs3djshfcan1lmhrphcfdqbsa07g91bgvb2k2")))

(define-public crate-gnunet-0.0.13 (c (n "gnunet") (v "0.0.13") (d (list (d (n "byteorder") (r ">= 0.3.10") (d #t) (k 0)) (d (n "error_def") (r ">= 0.3.1") (d #t) (k 0)) (d (n "num") (r ">= 0.1.24") (d #t) (k 0)) (d (n "rand") (r ">= 0.3") (d #t) (k 0)) (d (n "rust-crypto") (r ">= 0.2.31") (d #t) (k 0)) (d (n "unix_socket") (r ">= 0.3.0") (d #t) (k 0)))) (h "0wbshb7785bfbpf68frp4x5jb6b9pvfyqv3lmr9z4qyfbrm3cibh")))

(define-public crate-gnunet-0.0.14 (c (n "gnunet") (v "0.0.14") (d (list (d (n "byteorder") (r ">= 0.3.10") (d #t) (k 0)) (d (n "error_def") (r ">= 0.3.1") (d #t) (k 0)) (d (n "num") (r ">= 0.1.24") (d #t) (k 0)) (d (n "rand") (r ">= 0.3") (d #t) (k 0)) (d (n "rust-crypto") (r ">= 0.2.31") (d #t) (k 0)) (d (n "unix_socket") (r ">= 0.3.0") (d #t) (k 0)))) (h "06xgbc1ymj7g3q05vq3laygrcmmz9i32i6czmfv1mlwxi0i2fz68")))

(define-public crate-gnunet-0.0.15 (c (n "gnunet") (v "0.0.15") (d (list (d (n "byteorder") (r ">= 0.3.10") (d #t) (k 0)) (d (n "error_def") (r ">= 0.3.6") (d #t) (k 0)) (d (n "num") (r ">= 0.1.24") (d #t) (k 0)) (d (n "rand") (r ">= 0.3") (d #t) (k 0)) (d (n "regex") (r ">= 0.1.8") (d #t) (k 0)) (d (n "regex_macros") (r ">= 0.1.8") (d #t) (k 0)) (d (n "rust-crypto") (r ">= 0.2.31") (d #t) (k 0)) (d (n "unix_socket") (r ">= 0.3.0") (d #t) (k 0)))) (h "0bvq7xiqwaq50hfcysqmxsf9r27cx3x77dqzgig2xsiwb6k6paxm")))

