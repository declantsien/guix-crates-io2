(define-module (crates-io gn un gnunet-rs) #:use-module (crates-io))

(define-public crate-gnunet-rs-0.0.0 (c (n "gnunet-rs") (v "0.0.0") (d (list (d (n "gnunet-sys") (r "^0.0") (d #t) (k 0)))) (h "0cdqz610sk3fmslm2hn6irik8knabq9nwia7wbl02lvd5b9ysi9r") (f (quote (("peerstore" "gnunet-sys/peerstore") ("fs" "gnunet-sys/fs"))))))

(define-public crate-gnunet-rs-0.0.1 (c (n "gnunet-rs") (v "0.0.1") (d (list (d (n "async-std") (r "^1.9") (d #t) (k 0)) (d (n "gnunet-sys") (r "^0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "19674bzhdbj0wpj2l44454m772m87mqmakmlxi8y0s65ngb2gdr4") (f (quote (("peerstore" "gnunet-sys/peerstore") ("fs" "gnunet-sys/fs") ("cadet" "gnunet-sys/cadet"))))))

(define-public crate-gnunet-rs-0.0.2 (c (n "gnunet-rs") (v "0.0.2") (d (list (d (n "async-std") (r "^1.9") (d #t) (k 0)) (d (n "gnunet-sys") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1ksgmx6q05fql611ygffyvhfxchjj609jkg0yn3ymg52ii2bafjk") (f (quote (("peerstore" "gnunet-sys/peerstore") ("fs" "gnunet-sys/fs") ("cadet" "gnunet-sys/cadet"))))))

