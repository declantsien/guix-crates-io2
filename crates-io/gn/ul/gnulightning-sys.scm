(define-module (crates-io gn ul gnulightning-sys) #:use-module (crates-io))

(define-public crate-gnulightning-sys-2.3.0 (c (n "gnulightning-sys") (v "2.3.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1pc32zsabvfjam3f981hdywclyj8biwzjj68qlppc495gx8r17fh")))

