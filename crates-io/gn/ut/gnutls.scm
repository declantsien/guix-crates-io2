(define-module (crates-io gn ut gnutls) #:use-module (crates-io))

(define-public crate-gnutls-0.1.0 (c (n "gnutls") (v "0.1.0") (d (list (d (n "gnutls-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)))) (h "1n400c5mmz1q7jqlnz1avdby907zq8w2j0vxh8gbxrvfjjp75884")))

(define-public crate-gnutls-0.1.1 (c (n "gnutls") (v "0.1.1") (d (list (d (n "gnutls-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)))) (h "00fka13ds67ks6vg3ayk28ckrjan7kl3jymyhmx6kyfzvsf3ilz3")))

(define-public crate-gnutls-0.1.2 (c (n "gnutls") (v "0.1.2") (d (list (d (n "gnutls-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)))) (h "1paqf6n11qdzigmadpiqqxawmvkrp7wpdkiqhl4imh8xd1vh4qm1")))

(define-public crate-gnutls-0.1.3 (c (n "gnutls") (v "0.1.3") (d (list (d (n "gnutls-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)))) (h "19vkqzanb8y5dqqz94f7i7d0xrqxsd6aprf145kg99jyxdqpl4rv")))

