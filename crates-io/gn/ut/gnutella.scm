(define-module (crates-io gn ut gnutella) #:use-module (crates-io))

(define-public crate-gnutella-0.1.0 (c (n "gnutella") (v "0.1.0") (h "1px9wsbh756jjlkdaiqpx246fdxy1hiv88fz0i29bg3l4jpiab6f")))

(define-public crate-gnutella-1.0.0 (c (n "gnutella") (v "1.0.0") (h "1k27xbk8g411sa2s1931gk07866gxnqfzwdvpkf8s5xg4agc5v47")))

