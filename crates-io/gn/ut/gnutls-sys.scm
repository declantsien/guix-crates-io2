(define-module (crates-io gn ut gnutls-sys) #:use-module (crates-io))

(define-public crate-gnutls-sys-0.1.0 (c (n "gnutls-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "0yjwxqw8bgsjmyjjdabyvkkqjfrxdzbrridilvgh07n5qk7vbcqb")))

(define-public crate-gnutls-sys-0.1.1 (c (n "gnutls-sys") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "127ghvc9dz1fx9i97jxh0jqms9rximqrj54dcsp8lxcnzf6mlx3k")))

(define-public crate-gnutls-sys-0.1.2 (c (n "gnutls-sys") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "1qz94534l6x78xd8gh8ri2cl2l40g53avj0irkpla8qayygxa1rm")))

