(define-module (crates-io gn al gnal_tsur) #:use-module (crates-io))

(define-public crate-gnal_tsur-0.1.0 (c (n "gnal_tsur") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18nz0335b80l7imcmk00n1cd1lrlkbvddqaymggarg68g2ic1dny")))

