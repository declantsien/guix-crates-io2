(define-module (crates-io gn or gnore) #:use-module (crates-io))

(define-public crate-gnore-0.1.0 (c (n "gnore") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "termprompt") (r "^0.7.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "05bzhr79ga755lhwmzshfgxhh6q73yhznv1n77yrjrpb1qj30v4a")))

