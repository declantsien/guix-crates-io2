(define-module (crates-io gn om gnome-egpu) #:use-module (crates-io))

(define-public crate-gnome-egpu-0.1.0 (c (n "gnome-egpu") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.7.1") (d #t) (k 0)) (d (n "glib") (r "^0.10.0") (d #t) (k 0)) (d (n "gudev") (r "^0.9.0") (d #t) (k 0)) (d (n "sudo") (r "^0.6.0") (d #t) (k 0)))) (h "1bxq6g3sna4fiwmnrzkn1d9xr8rphnmgxvfawm8mydmsj68b63rl")))

(define-public crate-gnome-egpu-0.1.1 (c (n "gnome-egpu") (v "0.1.1") (d (list (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "glib") (r "^0.10.0") (d #t) (k 0)) (d (n "gudev") (r "^0.9.0") (d #t) (k 0)) (d (n "sudo") (r "^0.6.0") (d #t) (k 0)))) (h "06b867m6b3i1g6rh0w3y1la8413dwqdw7ryzskfjf7snm92c4q8c")))

