(define-module (crates-io gn om gnome-randr) #:use-module (crates-io))

(define-public crate-gnome-randr-0.1.0 (c (n "gnome-randr") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "dbus") (r "^0.9.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "1gs1n0p0byimazb9hs1yv08iz8xbgqhh50faa39g4822mqjl5ww6")))

(define-public crate-gnome-randr-0.1.1 (c (n "gnome-randr") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "dbus") (r "^0.9.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "1xbgwpq6fjp4767fgi8rnsw5vy4mvj2b5xrzm3cd8sdyr78kb40r")))

