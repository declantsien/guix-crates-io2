(define-module (crates-io gn om gnome-desktop) #:use-module (crates-io))

(define-public crate-gnome-desktop-0.4.0 (c (n "gnome-desktop") (v "0.4.0") (d (list (d (n "ffi") (r "^0.4") (d #t) (k 0) (p "gnome-desktop-sys")) (d (n "gdk-pixbuf") (r "^0.16") (d #t) (k 0)) (d (n "gio") (r "^0.16") (d #t) (k 0)) (d (n "glib") (r "^0.16") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hn822ia1hycvdizqdpky8r3i8kljvs1d7y1wiz00ac7z409cnij") (f (quote (("default"))))))

(define-public crate-gnome-desktop-0.4.1 (c (n "gnome-desktop") (v "0.4.1") (d (list (d (n "ffi") (r "^0.4.1") (d #t) (k 0) (p "gnome-desktop-sys")) (d (n "gdesktop_enums") (r "^0.3.1") (d #t) (k 0) (p "gsettings-desktop-schemas")) (d (n "gdk-pixbuf") (r "^0.18") (d #t) (k 0)) (d (n "gio") (r "^0.18") (d #t) (k 0)) (d (n "glib") (r "^0.18") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "192vv5y092gfafbsdrqz09cmxibdz8n577n7gjr6pncapamj6s9c") (f (quote (("default")))) (y #t)))

(define-public crate-gnome-desktop-0.4.2 (c (n "gnome-desktop") (v "0.4.2") (d (list (d (n "ffi") (r "^0.4.1") (d #t) (k 0) (p "gnome-desktop-sys")) (d (n "gdesktop_enums") (r "^0.3.1") (d #t) (k 0) (p "gsettings-desktop-schemas")) (d (n "gdk-pixbuf") (r "^0.18") (d #t) (k 0)) (d (n "gio") (r "^0.18") (d #t) (k 0)) (d (n "glib") (r "^0.18") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0x767v4c8l6mg38afs3915y9394vk7131wgmn9ysl08hz2rhfwia") (f (quote (("default"))))))

