(define-module (crates-io gn ur gnurx-sys) #:use-module (crates-io))

(define-public crate-gnurx-sys-0.1.0 (c (n "gnurx-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "09l7m3q9f15q3g9gh29m317w290g3rnlg7dvy2y1gqcib39c5iz5") (l "gnurx")))

(define-public crate-gnurx-sys-0.1.1 (c (n "gnurx-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "02klcsc1yx8gbqf6w7244hqfcgrnq3l6ap41xyn7r5yvfm6djig3") (l "gnurx")))

(define-public crate-gnurx-sys-0.1.2 (c (n "gnurx-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "0zrj4zmh9a7cwyzr1jbs6nll41fksq247l2jq262nwapa42k16vj") (l "gnurx")))

(define-public crate-gnurx-sys-0.2.0 (c (n "gnurx-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "0jxavp4cnvvhsqrk71lafqcbf206bb8wv9kyc7k3bqy3s0bvgmvw") (l "gnurx")))

(define-public crate-gnurx-sys-0.2.1 (c (n "gnurx-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "1ivdrk9q7hh3zgjwl0i4s3m6nkmxvr0bwkrr37k5zfai14zxsn8s") (l "gnurx")))

(define-public crate-gnurx-sys-0.3.0 (c (n "gnurx-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "1ybdzqn6cmzymm7d4yjmjg314ragp7a0ardqbna8696hfmbx7ckr") (l "gnurx")))

(define-public crate-gnurx-sys-0.3.1 (c (n "gnurx-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "06nax9z11hn298v8b4pih4jbnqa1gwjbczna8r1p5ja13c9v4z2a") (l "gnurx")))

(define-public crate-gnurx-sys-0.3.2 (c (n "gnurx-sys") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "1pak8di1098lrrwxnlyfhjlk57fynkyhjksrvx8jqn9n226qnv5d") (l "gnurx")))

(define-public crate-gnurx-sys-0.3.3 (c (n "gnurx-sys") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.62") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "15wrqylfha89h3ba8bfa4h1zkblx0f9yb1xw9pvymixvcd20pqpj") (l "gnurx")))

(define-public crate-gnurx-sys-0.3.4 (c (n "gnurx-sys") (v "0.3.4") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "08br054k1xwkz4kjg8wy8limpdfv4ycz9zq3d7nskwpzf42v3h4z") (l "gnurx")))

(define-public crate-gnurx-sys-0.3.5 (c (n "gnurx-sys") (v "0.3.5") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "1ifh4xg9qnla1ci4j4wpdy1bjm1pvdgswhf4sn5ba64bwpkvcd58") (l "gnurx")))

(define-public crate-gnurx-sys-0.3.6 (c (n "gnurx-sys") (v "0.3.6") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "0jhcqi6fjz1w4zj9szpz659yzc0i48fxvxihnvw9z5ky9qc5y0mb") (l "gnurx")))

(define-public crate-gnurx-sys-0.3.7 (c (n "gnurx-sys") (v "0.3.7") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.4") (d #t) (k 1)))) (h "1gy826q5r6gvll4x1dsvf5bwb1wpaxn50fslhmpqj4g0pg6kbsq0") (l "gnurx")))

(define-public crate-gnurx-sys-0.3.8 (c (n "gnurx-sys") (v "0.3.8") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.5") (d #t) (k 1)))) (h "01wqyxpc1znza39ix85an9irhcfa3fkwqzcwbpry92m1ysjbpdy7") (l "gnurx")))

