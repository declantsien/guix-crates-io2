(define-module (crates-io gn ur gnuradio_client) #:use-module (crates-io))

(define-public crate-gnuradio_client-0.0.2 (c (n "gnuradio_client") (v "0.0.2") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.2") (d #t) (k 0)))) (h "0synjnpbwxzlksigifbj4mjiq42167kqa4yd98n0d1928fz3qrpy")))

