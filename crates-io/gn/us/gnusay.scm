(define-module (crates-io gn us gnusay) #:use-module (crates-io))

(define-public crate-gnusay-0.1.0 (c (n "gnusay") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1rfj8xn948r6q89jakhzraz7mmgz6mm9lwplbffci42br09cf5a8")))

(define-public crate-gnusay-0.1.1 (c (n "gnusay") (v "0.1.1") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)))) (h "0crdjfy4q396sshxwdg96vvm4vl0aq9wrh7kjsz54fn8vi88wmhi")))

(define-public crate-gnusay-0.2.0 (c (n "gnusay") (v "0.2.0") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)))) (h "119xq646r3k89xyf4xiyliqsd6mmragy4vayvn6rxjcy8sigij8m")))

(define-public crate-gnusay-0.3.0 (c (n "gnusay") (v "0.3.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nfyg06qppkgba99apn2klsxz6g5hxrvp09ng3kc3m65dqjv7lgz")))

