(define-module (crates-io gn aw gnaw) #:use-module (crates-io))

(define-public crate-gnaw-0.0.1 (c (n "gnaw") (v "0.0.1") (h "02fy7v50d6czyjw10f98nh7i4ar9gn2dm3mgqzqnjrh1xpx8y0hf")))

(define-public crate-gnaw-0.0.2 (c (n "gnaw") (v "0.0.2") (d (list (d (n "cargo-husky") (r "^1.5.0") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "git_info") (r "^0.1.2") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "12qd550vxg2ic77wv0k9gyky3w3f7fda1m13vj5hacfslpy8d5x7")))

