(define-module (crates-io gn os gnostr-sha256) #:use-module (crates-io))

(define-public crate-gnostr-sha256-0.0.3 (c (n "gnostr-sha256") (v "0.0.3") (d (list (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "sha256") (r "^1.2.2") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "07rp5l9ag8s49la9mi5i1apyih66ydx82kqi7n8ig1x2vnjsxiyr") (r "1.71.1")))

(define-public crate-gnostr-sha256-0.0.4 (c (n "gnostr-sha256") (v "0.0.4") (d (list (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "sha256") (r "^1.2.2") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "08qa15nwvipqq1p3xwj49x50502q9zggn3y65i735zybw6la6vf9") (r "1.71.1")))

(define-public crate-gnostr-sha256-0.0.5 (c (n "gnostr-sha256") (v "0.0.5") (d (list (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "sha256") (r "^1.2.2") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "0z2g3n8d1pfvx5hsg2m48n7vxlxag46g5fywzy3d9c87i6mjys9z") (r "1.71.1")))

