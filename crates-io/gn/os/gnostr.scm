(define-module (crates-io gn os gnostr) #:use-module (crates-io))

(define-public crate-gnostr-0.0.44 (c (n "gnostr") (v "0.0.44") (d (list (d (n "cc") (r "^1.0.98") (d #t) (k 0)) (d (n "cc") (r "^1.0.98") (f (quote ("libc"))) (d #t) (k 1)) (d (n "include_dir") (r "^0.7.3") (f (quote ("glob" "metadata"))) (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (f (quote ("glob" "metadata"))) (d #t) (k 1)) (d (n "markdown") (r "^1.0.0-alpha.17") (f (quote ("json" "log" "serde"))) (d #t) (k 0)) (d (n "markdown") (r "^1.0.0-alpha.17") (f (quote ("json" "log" "serde"))) (d #t) (k 1)))) (h "1nsbir2w31nmkc4l75c3iqw03jpl4mbypg2pjz36nbdwr6aglrnc") (r "1.76")))

