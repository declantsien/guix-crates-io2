(define-module (crates-io gn os gnostr-get-relays) #:use-module (crates-io))

(define-public crate-gnostr-get-relays-0.0.0 (c (n "gnostr-get-relays") (v "0.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mb44k84vldws308g9z804dkb3qdv5rkdazwfxcklsfdbbm2zqkw")))

(define-public crate-gnostr-get-relays-0.0.1 (c (n "gnostr-get-relays") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fb68hydhj7sb92047sykxvdshcffq418bnyrlbq4nzh4fylpwp3")))

(define-public crate-gnostr-get-relays-0.0.2 (c (n "gnostr-get-relays") (v "0.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "curl") (r "^0.4.46") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1i3rmp9ik3d37jg666xlvkwzhf4kcrsqfgkkf6pwc91xjxwszrpp")))

(define-public crate-gnostr-get-relays-0.0.3 (c (n "gnostr-get-relays") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "curl") (r "^0.4.46") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wr88fmaamkbvw8yx4a6bpf1lwkp0j8qqy76m91jjwvlrf9qn5b0")))

