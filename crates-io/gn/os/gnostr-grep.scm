(define-module (crates-io gn os gnostr-grep) #:use-module (crates-io))

(define-public crate-gnostr-grep-0.0.0 (c (n "gnostr-grep") (v "0.0.0") (d (list (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "028ycavhm6ymsal97h21rj2d87fi5f4p4n2yhj7lkx4xh719zbcz")))

(define-public crate-gnostr-grep-0.0.1 (c (n "gnostr-grep") (v "0.0.1") (d (list (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "0pdx94jnmhmvv489ws8rpjqak4jqfbbrl7ki8vck1yzymzkvivzr")))

(define-public crate-gnostr-grep-0.0.2 (c (n "gnostr-grep") (v "0.0.2") (d (list (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "19pp9cqscdh06fg444mrvzmc5xq22776hgs2sffhkqqwdqx38qfs")))

(define-public crate-gnostr-grep-0.0.3 (c (n "gnostr-grep") (v "0.0.3") (d (list (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "1c8jd4kw9r9qpvzvnqjv42wvmzsln2srmgbp8spass4hd7np4p29")))

(define-public crate-gnostr-grep-0.0.4 (c (n "gnostr-grep") (v "0.0.4") (d (list (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "0zf7y575dnhaam6298xdz6129jljwn784vwqibni3hg2apc6igwy")))

