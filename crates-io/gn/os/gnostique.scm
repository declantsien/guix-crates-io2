(define-module (crates-io gn os gnostique) #:use-module (crates-io))

(define-public crate-gnostique-0.1.0-alpha.1 (c (n "gnostique") (v "0.1.0-alpha.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "gtk") (r "^0.5.5") (f (quote ("v4_6"))) (d #t) (k 0) (p "gtk4")) (d (n "linkify") (r "^0.9.0") (d #t) (k 0)) (d (n "nostr-sdk") (r "^0.14.1") (d #t) (k 0)) (d (n "relm4") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1.24.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1z2c79vfaqhrzph1rfwag5hvfm261sn4m47gfhzsilrps22sml24")))

