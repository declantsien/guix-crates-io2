(define-module (crates-io gn os gnostr-command) #:use-module (crates-io))

(define-public crate-gnostr-command-0.0.2 (c (n "gnostr-command") (v "0.0.2") (d (list (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "io") (r "^0.0.2") (d #t) (k 0)) (d (n "nostr") (r "^0.24.0") (d #t) (k 0)) (d (n "nostr-sdk") (r "^0.24.0") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)) (d (n "use") (r "^0.0.0") (d #t) (k 0)))) (h "00qz1m3m7wssghcx4pkpdkdmlb50ix599kq5cw198r40qky1w7nv")))

(define-public crate-gnostr-command-0.0.3 (c (n "gnostr-command") (v "0.0.3") (d (list (d (n "chrono") (r "^0") (d #t) (k 0)) (d (n "io") (r "^0.0.2") (d #t) (k 0)) (d (n "nostr") (r "^0.24.0") (d #t) (k 0)) (d (n "nostr-sdk") (r "^0.24.0") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)) (d (n "use") (r "^0.0.0") (d #t) (k 0)))) (h "18h0543iafrvqnhc839m3x3scx50qn265k6nnqccsymcb7sfwbdc")))

