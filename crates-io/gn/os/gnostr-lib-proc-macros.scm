(define-module (crates-io gn os gnostr-lib-proc-macros) #:use-module (crates-io))

(define-public crate-gnostr-lib-proc-macros-0.0.25 (c (n "gnostr-lib-proc-macros") (v "0.0.25") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (d #t) (k 0)))) (h "1ix062lc9mgzanphcxsl427588lq6h5h2hfvc2knb9343z43hgqr") (r "1.71")))

