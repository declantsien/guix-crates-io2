(define-module (crates-io gn ud gnudb) #:use-module (crates-io))

(define-public crate-gnudb-0.1.0 (c (n "gnudb") (v "0.1.0") (d (list (d (n "discid") (r "^0.4") (d #t) (k 0)))) (h "12rx3mnw7nn44y977sg86sdgx282zpxw4sl2145mwid1ggj1m4pw")))

(define-public crate-gnudb-0.1.1 (c (n "gnudb") (v "0.1.1") (d (list (d (n "discid") (r "^0.4") (d #t) (k 0)))) (h "09mbnliwvf35zzsgp1c01bq034xjlwvpyf9qk8aih0lrmigwv8hv")))

(define-public crate-gnudb-0.1.2 (c (n "gnudb") (v "0.1.2") (d (list (d (n "discid") (r "^0.4") (d #t) (k 0)))) (h "007ywnc7gvac3ll1qv27rfkwsc05pl9kg8ja1h2hklcjd69x04ry")))

(define-public crate-gnudb-0.1.3 (c (n "gnudb") (v "0.1.3") (d (list (d (n "discid") (r "^0.4") (d #t) (k 0)))) (h "0bvs98x431g23hnfrryiv9mbzsmvjgzpk8nswhqx3agy63g9vv7h")))

(define-public crate-gnudb-0.1.4 (c (n "gnudb") (v "0.1.4") (d (list (d (n "discid") (r "^0.4") (d #t) (k 0)))) (h "16m41dd4ysaafgvijwzbkhw42ghc3c1d89k6m3k6yabfwcfhjd0n")))

(define-public crate-gnudb-0.2.1 (c (n "gnudb") (v "0.2.1") (d (list (d (n "async-std") (r "^1.11") (d #t) (k 0)) (d (n "discid") (r "^0.4") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "1qibnh4mrxrwwlz7zyr64n70mnx33yjvx2rpks3lnkaq2z2bc1n5")))

(define-public crate-gnudb-0.2.2 (c (n "gnudb") (v "0.2.2") (d (list (d (n "async-std") (r "^1.11") (d #t) (k 0)) (d (n "discid") (r "^0.4") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "171w2dj0dihq06q0jq3lxnvn66cxm8w15jhvk9a1kncxj6ilbxi4")))

