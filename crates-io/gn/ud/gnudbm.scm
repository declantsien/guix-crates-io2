(define-module (crates-io gn ud gnudbm) #:use-module (crates-io))

(define-public crate-gnudbm-0.1.0 (c (n "gnudbm") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0rwzy5jrf1sq6m7ac271d8wrs27ard9byj4vfjzamjvk40501r0h")))

(define-public crate-gnudbm-0.1.1 (c (n "gnudbm") (v "0.1.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1p9fngili6f1dwlwhqhqjb5yr698wgiw3g9ywqhsdwqi6f8dxh2c")))

(define-public crate-gnudbm-0.1.2 (c (n "gnudbm") (v "0.1.2") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "11cxp7nrhkai367gk4vfdhaw3b44i30pf7qjj571cibr5c3bigbl")))

(define-public crate-gnudbm-0.1.3 (c (n "gnudbm") (v "0.1.3") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1fv18q3qc74ny0izyl13y5p9nxch2ggj33p5ynzhsy8jr9m827ii")))

(define-public crate-gnudbm-0.2.0 (c (n "gnudbm") (v "0.2.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1l5pldm7kkvzja19lvjksf1za6b46s1msd4fc6b8iggahik46alq") (f (quote (("system-gdbm"))))))

(define-public crate-gnudbm-0.2.1 (c (n "gnudbm") (v "0.2.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1v5c7ykl97hs1zddfxq4jmm4nhrnrkymd14pghz2s85y40michkg") (f (quote (("system-gdbm"))))))

(define-public crate-gnudbm-0.2.3 (c (n "gnudbm") (v "0.2.3") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0i7pkswkdd2dm95qfdfr2yiid4bl4qjxac91xdkwbpg4l4jqg8gx") (f (quote (("system-gdbm"))))))

(define-public crate-gnudbm-0.2.4 (c (n "gnudbm") (v "0.2.4") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1fqvr3z8xphq43i3x02isycf3jnjimhpmcik7pq1mqr55zs64vdq") (f (quote (("system-gdbm"))))))

(define-public crate-gnudbm-0.2.5 (c (n "gnudbm") (v "0.2.5") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1mf8mixqbhwighpydq4rzkfmpnxyniq2673mz9cdxjgaaxif5rid") (f (quote (("system-gdbm"))))))

