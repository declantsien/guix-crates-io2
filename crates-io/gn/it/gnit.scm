(define-module (crates-io gn it gnit) #:use-module (crates-io))

(define-public crate-gnit-0.1.0 (c (n "gnit") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "git2") (r "^0.7.5") (d #t) (k 0)) (d (n "read_input") (r "^0.6.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2.13") (d #t) (k 0)))) (h "0niy73zwc5vkfjd8l23pmw298cvxaf40s6p4702fg63n554l56c4") (y #t)))

