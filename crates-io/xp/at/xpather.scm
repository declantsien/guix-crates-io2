(define-module (crates-io xp at xpather) #:use-module (crates-io))

(define-public crate-xpather-0.1.4 (c (n "xpather") (v "0.1.4") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "markup5ever") (r "^0.10") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0qjyj7v69hqgx68bhiczjci2rdgyqiyb514kqmhmhq93kggf1pv4")))

(define-public crate-xpather-0.2.0 (c (n "xpather") (v "0.2.0") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "markup5ever") (r "^0.10") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1kxq1j3dsrz66vvmixayjvyqdm90sza8hd22ay3sqx93f7k7xsi9")))

(define-public crate-xpather-0.3.0-beta.0 (c (n "xpather") (v "0.3.0-beta.0") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "markup5ever") (r "^0.10") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lvfwv8swilrkk6vzpvy8bajdv9ap31b19q4qzg6h6lci8r1nvil") (y #t)))

(define-public crate-xpather-0.3.0-beta.1 (c (n "xpather") (v "0.3.0-beta.1") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "markup5ever") (r "^0.10") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12vjxh6ffly6b9fhc4rrd66r4w0l7x16f27q6pm568783jh4df59") (y #t)))

(define-public crate-xpather-0.3.0-beta.2 (c (n "xpather") (v "0.3.0-beta.2") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "markup5ever") (r "^0.10") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01jg0igha9alwwvinkgi1krsd7h2cz31ls81p3x87xc4n08qqd33") (y #t)))

(define-public crate-xpather-0.3.0-beta.3 (c (n "xpather") (v "0.3.0-beta.3") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "markup5ever") (r "^0.10") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0drndc55vawsl7a9k8dksbrahx1pb89hzgqhs8d3qpp6i72haxn1") (y #t)))

(define-public crate-xpather-0.3.0-beta.4 (c (n "xpather") (v "0.3.0-beta.4") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "markup5ever") (r "^0.10") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0587imxgpmzdzg843lvgmh9ix50czlbq1vr7s7m90fbkw39gijm0") (y #t)))

(define-public crate-xpather-0.3.0-beta.5 (c (n "xpather") (v "0.3.0-beta.5") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "markup5ever") (r "^0.10") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bd7yd2bv117b6mz4yfnn6408qj8bkzf7qgq8n4ylr0vl1cicfy1") (y #t)))

(define-public crate-xpather-0.3.0-beta.6 (c (n "xpather") (v "0.3.0-beta.6") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "markup5ever") (r "^0.10") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0k0vnl68f7721f9xnlndvndssayp65wgkvk77yzjqf057ibmag68") (y #t)))

(define-public crate-xpather-0.3.0-beta.7 (c (n "xpather") (v "0.3.0-beta.7") (d (list (d (n "html5ever") (r "^0.25") (d #t) (k 0)) (d (n "markup5ever") (r "^0.10") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "107brw5m8nj4ammcb5n791vsxjyp3vyjzhdckzag1f4djc4ky4pl") (y #t)))

(define-public crate-xpather-0.3.0-beta.8 (c (n "xpather") (v "0.3.0-beta.8") (d (list (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "markup5ever") (r "^0.11") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "1zypxm31wsw3iwl7j7fkh8fm2hcirv7bm9jp4ma39f7gja53kw59")))

(define-public crate-xpather-0.3.0-beta.9 (c (n "xpather") (v "0.3.0-beta.9") (d (list (d (n "html5ever") (r "^0.26") (d #t) (k 0)) (d (n "markup5ever") (r "^0.11") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "0c626arf74aw3xlnd1m25s9j016yiik1cgfplh2zzfq39v31jps0")))

