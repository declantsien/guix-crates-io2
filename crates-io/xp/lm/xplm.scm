(define-module (crates-io xp lm xplm) #:use-module (crates-io))

(define-public crate-xplm-0.1.0 (c (n "xplm") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xplm-sys") (r "^0.1") (d #t) (k 0)))) (h "127z6qzrcb052d41mf0v7llkz1c1qd4faw6am4dnp676gw14g0ak")))

(define-public crate-xplm-0.1.1 (c (n "xplm") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xplm-sys") (r "^0.1") (d #t) (k 0)))) (h "19q2qx56yydi000sm1pxjcpqqy11cp39qr7vv755xgliyk76yl64")))

(define-public crate-xplm-0.1.3 (c (n "xplm") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xplm-sys") (r "^0.1") (d #t) (k 0)))) (h "04f1nc8am3gaph7pqlmwa95pxavmxzh201gfjlxn5j8d7b4d11pf")))

(define-public crate-xplm-0.1.5 (c (n "xplm") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xplm-sys") (r "^0.1") (d #t) (k 0)))) (h "0ryk2zq7cygslm979bvnr394hw1bbijmq6dqkyxv0hgv492dzar6")))

(define-public crate-xplm-0.1.6 (c (n "xplm") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xplm-sys") (r "^0.1") (d #t) (k 0)))) (h "08s6nn38icv94l61ri2i4jfmk3xrs1bmr5k8f1mnfr7s7cdpkrwp")))

(define-public crate-xplm-0.1.7 (c (n "xplm") (v "0.1.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xplm-sys") (r "^0.1") (d #t) (k 0)))) (h "0hy6mcwi5vdrmarqb2140739249yab5cjpl09swc6k5zbipq729z")))

(define-public crate-xplm-0.1.8 (c (n "xplm") (v "0.1.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xplm-sys") (r "^0.1") (d #t) (k 0)))) (h "1bb8lxq93zbv55wgq7yqpm2wb44x74cgfj7gmwcwi62kli6fg9nk")))

(define-public crate-xplm-0.1.9 (c (n "xplm") (v "0.1.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xplm-sys") (r "^0.1") (d #t) (k 0)))) (h "1glai6g5j5y4i08q4qy9ssc2b0218imspn7z4ia74a4wfl9j5kkm")))

(define-public crate-xplm-0.2.1 (c (n "xplm") (v "0.2.1") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "xplm-sys") (r "^0.2.1") (d #t) (k 0)))) (h "0ym5my7irj2v37rijji6jf1vf4cm78a2lfjqib2rqiq76gas3zrr")))

(define-public crate-xplm-0.2.2 (c (n "xplm") (v "0.2.2") (d (list (d (n "hfs_paths") (r "^0.1.0") (d #t) (t "cfg(all(target_os = \"macos\", not(feature = \"xplm210\")))") (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "xplm-sys") (r "^0.2.1") (k 0)))) (h "02rbqnqcg8ha204xkvad5bi2qavj17haxkd8248i5fhgiq38lkpg") (f (quote (("xplm210" "xplm-sys/xplm210") ("xplm200" "xplm-sys/xplm200") ("default" "xplm200" "xplm210"))))))

(define-public crate-xplm-0.3.1 (c (n "xplm") (v "0.3.1") (d (list (d (n "quick-error") (r "^1.2.3") (d #t) (k 0)) (d (n "xplm-sys") (r "^0.4.0") (d #t) (k 0)))) (h "09ah6wsdqqqhwzxvhf7wwx0accjv2g30fzp2mgmd5h8r0jl6vr1h")))

(define-public crate-xplm-0.4.0 (c (n "xplm") (v "0.4.0") (d (list (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "xplm-sys") (r "^0.5.0") (d #t) (k 0)))) (h "0vj268j5mmbpd5djzjhcqc7qp731pmi70hc92x3k64jpz3krx5x0")))

(define-public crate-xplm-0.4.1 (c (n "xplm") (v "0.4.1") (d (list (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "xplm-sys") (r "^0.5.0") (d #t) (k 0)))) (h "18jhfbjkjfp04bliq2gvxkcan2qsd3r3allzrlvc630z0jfldl3b")))

