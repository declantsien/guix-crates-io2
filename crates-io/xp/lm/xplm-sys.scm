(define-module (crates-io xp lm xplm-sys) #:use-module (crates-io))

(define-public crate-xplm-sys-0.1.0 (c (n "xplm-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0w08c81hg58zll7wvanshahl5sp1jdz7hf7kbnvgj19rd1cyaaqw")))

(define-public crate-xplm-sys-0.1.1 (c (n "xplm-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "167h2izj5bhhyjn1lri63bha97w8y89g98ws8dsdyd37h9rwc82d")))

(define-public crate-xplm-sys-0.1.2 (c (n "xplm-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0n5g6z6fbzmhlcddx6md3s08c1d6db7bbn50rjz156l0d9p8rz73")))

(define-public crate-xplm-sys-0.1.3 (c (n "xplm-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ni4dcswv9i6n1bk82zai9hxg2pkf395l8hvcqq6621sr1k0pzr2")))

(define-public crate-xplm-sys-0.1.4 (c (n "xplm-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "02d504bjrm2i15l6d2zvqd2jz6cvpp626qj5d0x85w99zanzapck")))

(define-public crate-xplm-sys-0.1.5 (c (n "xplm-sys") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0b8x9xxnkn2ykrvzqb8jlz0a1q6cny8d3nndh2m3p74121bphx49")))

(define-public crate-xplm-sys-0.2.0 (c (n "xplm-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.20.0") (d #t) (k 1)))) (h "18kna31rq030rgd0pf182b0hkcq7ms66jfd3lvzfvs3vr9b7g7gh")))

(define-public crate-xplm-sys-0.2.1 (c (n "xplm-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.20.0") (d #t) (k 1)))) (h "0fa369lxg5q1mbwlx6yz6ka3h1ww05bkc32xfr5rz4yx3ckcyakg")))

(define-public crate-xplm-sys-0.2.2 (c (n "xplm-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.20.0") (d #t) (k 1)))) (h "0ljvpjvg62d47wyq9rrk9z8pd9kr86zzknh8brvwkicrl0c003m1") (f (quote (("xplm210") ("xplm200") ("default" "xplm200" "xplm210"))))))

(define-public crate-xplm-sys-0.3.0 (c (n "xplm-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)))) (h "1ylshs00wjsraxxb0gsld13jmdayr4zxrvy2za1vqvf2cylikr5q") (f (quote (("xplm300") ("xplm210") ("xplm200"))))))

(define-public crate-xplm-sys-0.3.1 (c (n "xplm-sys") (v "0.3.1") (h "181g430jcsqs5nq240kmfjwahmbpnkibq01fqljdknc9j26xnczw")))

(define-public crate-xplm-sys-0.4.0 (c (n "xplm-sys") (v "0.4.0") (h "0xwn5kbfxd5cp616l565k8dys6x9n9c5gvwij0shhcxa7yr340d5")))

(define-public crate-xplm-sys-0.5.0 (c (n "xplm-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)))) (h "17jn4szb1n1ffhkq0j1a00bjql17ja23c3mvjcky5xd20n4d06p5")))

