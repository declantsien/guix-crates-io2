(define-module (crates-io xp d- xpd-common) #:use-module (crates-io))

(define-public crate-xpd-common-0.0.1 (c (n "xpd-common") (v "0.0.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1560mii39ydybciwh1p5bj6yphg5vc7dbjv6q7y8hsqvii2zfddx")))

(define-public crate-xpd-common-0.0.2 (c (n "xpd-common") (v "0.0.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xhd8k134giz4h5bkih0xphg9id1h366kbfckxfkm7iiqjwm21lh")))

(define-public crate-xpd-common-0.0.3 (c (n "xpd-common") (v "0.0.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ly5ygxnr8b74hvcfysr0xcms4p37k4nmjwkamljwclf9f3y34bp")))

(define-public crate-xpd-common-0.0.5 (c (n "xpd-common") (v "0.0.5") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "twilight-model") (r "^0.15") (d #t) (k 0)))) (h "0sr77n7kjk1lib3zxhqvpzj6jalb16am35zylyr7whl4pni2vij1")))

(define-public crate-xpd-common-0.0.6 (c (n "xpd-common") (v "0.0.6") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("signal" "macros"))) (d #t) (k 0)) (d (n "twilight-model") (r "^0.15") (d #t) (k 0)))) (h "086vaiv66pkidapa0x9d1sq85b8z6g7923agkszy0gczy8hf2w4h")))

