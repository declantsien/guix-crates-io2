(define-module (crates-io xp su xpsupport) #:use-module (crates-io))

(define-public crate-xpsupport-0.1.0 (c (n "xpsupport") (v "0.1.0") (d (list (d (n "gcc") (r "~0.3") (d #t) (k 1)))) (h "1j23wrri7kgiy3xlsrp1p8ggzfck7x01r010hx8hdsb256s9pf2g")))

(define-public crate-xpsupport-0.1.1 (c (n "xpsupport") (v "0.1.1") (d (list (d (n "gcc") (r "~0.3") (d #t) (k 1)))) (h "1issjwwg44x322w1728ai89bdv51013swkll3704qax1lswqaykn")))

(define-public crate-xpsupport-0.1.2 (c (n "xpsupport") (v "0.1.2") (d (list (d (n "gcc") (r "~0.3") (d #t) (k 1)))) (h "1kql9g6d1mpm9az1sfa7jhrriv8agq916w9n61n14wm35ycrs5jv")))

(define-public crate-xpsupport-0.1.3 (c (n "xpsupport") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3.45") (d #t) (k 1)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1ndy70bmzsdphxlzb2xqcg0yr2kn2hv43fcd0cc0i947kdznz2qx")))

(define-public crate-xpsupport-0.1.4 (c (n "xpsupport") (v "0.1.4") (d (list (d (n "gcc") (r "^0.3.45") (d #t) (k 1)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0bgqvr7bar4dsax6f16mabn1hi9k8llw56i446hxml7pxy09rm1p")))

(define-public crate-xpsupport-0.1.5 (c (n "xpsupport") (v "0.1.5") (d (list (d (n "gcc") (r "^0.3.45") (d #t) (k 1)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "15ljydxzb4qywradla2c9m1ddkdrdfkj938hlp2c024ghwdm0ldp")))

(define-public crate-xpsupport-0.2.0 (c (n "xpsupport") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.46") (d #t) (k 1)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)))) (h "0gkx64kjziz3sfvx08wlj6rgaqnv9w5bvb4k1v7wpfvq6rw0xhm1")))

(define-public crate-xpsupport-0.2.1 (c (n "xpsupport") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.46") (d #t) (k 1)) (d (n "rustc_version") (r "^0.2.3") (d #t) (k 1)))) (h "0q5qhwciw83dh7pnyy1070jwrl7hnk7ql21n71vab000py6bpd37")))

(define-public crate-xpsupport-0.2.2 (c (n "xpsupport") (v "0.2.2") (d (list (d (n "cc") (r "^1.0.46") (d #t) (k 1)))) (h "1yw5571v5i0cja0c2rjqi43cmakwq1w95a3n4rxqvg1g6ipxplcg")))

