(define-module (crates-io xp rs xprs) #:use-module (crates-io))

(define-public crate-xprs-0.0.1-beta1 (c (n "xprs") (v "0.0.1-beta1") (d (list (d (n "miette") (r "^5.10.0") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1zf29jl04dwnvwd5glw57y8vd26jsnynazh4ng34dzlf4w79cpsn") (f (quote (("pemdas") ("pejmdas") ("default" "pemdas" "compile-time-optimizations") ("compile-time-optimizations")))) (r "1.70.0")))

(define-public crate-xprs-0.0.1-beta2 (c (n "xprs") (v "0.0.1-beta2") (d (list (d (n "miette") (r "^5.10.0") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0l8srczcilzdwcagv452z496cqng4gjnfrzrxqqz518v5cj1nmlw") (f (quote (("pemdas") ("pejmdas") ("default" "pemdas" "compile-time-optimizations") ("compile-time-optimizations")))) (r "1.70.0")))

(define-public crate-xprs-0.0.1-beta3 (c (n "xprs") (v "0.0.1-beta3") (d (list (d (n "miette") (r "^5.10.0") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0mz51vimcsszb3j27lw6vd2b43vrk503lhdqyjahwc7dbgbyaqfl") (f (quote (("pemdas") ("pejmdas") ("default" "pemdas" "compile-time-optimizations") ("compile-time-optimizations")))) (r "1.70.0")))

(define-public crate-xprs-0.0.1 (c (n "xprs") (v "0.0.1") (d (list (d (n "miette") (r "^5.10.0") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1qhb2vmx45jcj62rvgz26fdvl6sp88dyysdiz07akyb5iagb2717") (f (quote (("pemdas") ("pejmdas") ("default" "pemdas" "compile-time-optimizations") ("compile-time-optimizations")))) (r "1.70.0")))

(define-public crate-xprs-0.0.2 (c (n "xprs") (v "0.0.2") (d (list (d (n "miette") (r "^5.10.0") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0lavd19jkaci3z485a3fcqzb8cjwziq1vzv6mf9rwf4mgy6yxd3f") (f (quote (("pemdas") ("pejmdas") ("default" "pemdas" "compile-time-optimizations") ("compile-time-optimizations")))) (r "1.70.0")))

(define-public crate-xprs-0.1.0 (c (n "xprs") (v "0.1.0") (d (list (d (n "miette") (r "^7.2.0") (f (quote ("fancy-no-backtrace"))) (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "10q5l0255pz3qckk9pw32hglr6pdy15y2zy5v5ijwygcjhfvnr8x") (f (quote (("pemdas") ("pejmdas") ("default" "pemdas" "compile-time-optimizations") ("compile-time-optimizations")))) (r "1.70.0")))

