(define-module (crates-io xp c- xpc-connection-sys) #:use-module (crates-io))

(define-public crate-xpc-connection-sys-0.1.0 (c (n "xpc-connection-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.48") (d #t) (k 1)))) (h "0k724b81v8f7g019zhladr4a7k9665v9gmgh42qhc5h6g5w6bbic")))

(define-public crate-xpc-connection-sys-0.1.1 (c (n "xpc-connection-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "0v2bw7a4874s6l0ax6niyg4rqpjkmxjmqsnf7m7j9l00lq1kzrz9")))

