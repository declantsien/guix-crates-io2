(define-module (crates-io xp c- xpc-connection) #:use-module (crates-io))

(define-public crate-xpc-connection-0.1.0 (c (n "xpc-connection") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.40.0") (d #t) (k 1)) (d (n "block") (r "^0.1.6") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.6.1") (d #t) (k 2)))) (h "1axwavrv408vkhynh0b7dcfxn9drngq9kcbdrg3c65y015m8cxw7")))

(define-public crate-xpc-connection-0.2.0 (c (n "xpc-connection") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.40.0") (d #t) (k 1)) (d (n "block") (r "^0.1.6") (d #t) (k 0)) (d (n "futures-preview") (r "^0.2.2") (d #t) (k 0)))) (h "0k316ynh7vgpkb71mv071y0kxnb6ky4dljx0sri768qgjcdzqn8i")))

(define-public crate-xpc-connection-0.2.1 (c (n "xpc-connection") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.40.0") (d #t) (k 1)) (d (n "block") (r "^0.1.6") (d #t) (k 0)) (d (n "futures-preview") (r "^0.2.2") (d #t) (k 0)))) (h "1qlhg4s0918aps41j65jdmdra87kdcmllakmcz6l7jlyfl9az3l1")))

(define-public crate-xpc-connection-0.2.2 (c (n "xpc-connection") (v "0.2.2") (d (list (d (n "block") (r "^0.1.6") (d #t) (k 0)) (d (n "futures-preview") (r "^0.2.2") (d #t) (k 0)) (d (n "xpc-connection-sys") (r "^0.1.0") (d #t) (k 0)))) (h "06z7h01s2wi3fklg0n41mhmvvx1h7s81ajyilbybch0v5mjiaal1")))

(define-public crate-xpc-connection-0.2.3 (c (n "xpc-connection") (v "0.2.3") (d (list (d (n "block") (r "^0.1.6") (d #t) (k 0)) (d (n "futures-preview") (r "^0.2.2") (d #t) (k 0)) (d (n "xpc-connection-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0w1iwkmcaliyik8r5k95j3sdg11v0migvxggkpc18ibprlyjbk5v")))

