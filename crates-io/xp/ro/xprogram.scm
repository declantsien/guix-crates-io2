(define-module (crates-io xp ro xprogram) #:use-module (crates-io))

(define-public crate-xprogram-0.1.0 (c (n "xprogram") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "solana-sdk") (r "^1.7.11") (d #t) (k 2)))) (h "0p6j15i35fzwmgn0w30hywfnm3jkijxw4x4jxzx65qywmc1id2b8") (f (quote (("no-entrypoint"))))))

