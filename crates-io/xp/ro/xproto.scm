(define-module (crates-io xp ro xproto) #:use-module (crates-io))

(define-public crate-xproto-0.1.0 (c (n "xproto") (v "0.1.0") (h "13q9aw5gc4kldxxzf3hj8dpbx6fw18x9izfdgc6x98ym37vv7wnb")))

(define-public crate-xproto-1.0.0 (c (n "xproto") (v "1.0.0") (h "0c4mihlsl8calj4d0rbip7p8z3lcd97z9kjlmfp495bnbqp6fbb2")))

(define-public crate-xproto-1.1.0 (c (n "xproto") (v "1.1.0") (d (list (d (n "protocol") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "protocol-derive") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0icxz58yv9hvsf4iifaj62shjxqj5zz9hxqfz6nprvx3mh3aq464") (f (quote (("protocol-support" "protocol" "protocol-derive") ("default" "protocol-support"))))))

(define-public crate-xproto-1.1.1 (c (n "xproto") (v "1.1.1") (d (list (d (n "protocol") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "protocol-derive") (r "^0.5") (o #t) (d #t) (k 0)))) (h "057pyw5hm830jgw4mdmd63vw3a1y0ds7vsyiagn6sy8jlc47379m") (f (quote (("protocol-support" "protocol" "protocol-derive") ("default" "protocol-support"))))))

(define-public crate-xproto-1.1.2 (c (n "xproto") (v "1.1.2") (d (list (d (n "protocol") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "protocol-derive") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1rbq17gk9gkbkn6qrg7qrzclalyab4ygfzbf8n9hzvalbmiiwzix") (f (quote (("protocol-support" "protocol" "protocol-derive") ("default" "protocol-support"))))))

(define-public crate-xproto-1.1.3 (c (n "xproto") (v "1.1.3") (d (list (d (n "protocol") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "protocol-derive") (r "^0.7") (o #t) (d #t) (k 0)))) (h "11jymlmgifz6yrybd4zqpp1lq44lx528jwrmjn3zw08g194f3xsi") (f (quote (("protocol-support" "protocol" "protocol-derive") ("default" "protocol-support"))))))

(define-public crate-xproto-1.1.4 (c (n "xproto") (v "1.1.4") (d (list (d (n "protocol") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "protocol-derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0ayb018d48bw3kzxkw2dh1zzjrx9hzmdq67rknysarlsd7xvzc7x") (f (quote (("protocol-support" "protocol" "protocol-derive") ("default" "protocol-support"))))))

(define-public crate-xproto-1.1.5 (c (n "xproto") (v "1.1.5") (d (list (d (n "protocol") (r "^3.0") (o #t) (d #t) (k 0)) (d (n "protocol-derive") (r "^3.0") (o #t) (d #t) (k 0)))) (h "1wrjimn500d2x88jy29i1q3ggdjh20qykx33kvq4n0avazkvg1dd") (f (quote (("protocol-support" "protocol" "protocol-derive") ("default" "protocol-support"))))))

(define-public crate-xproto-2.0.0 (c (n "xproto") (v "2.0.0") (d (list (d (n "protocol") (r "^3.1") (o #t) (d #t) (k 0)) (d (n "protocol-derive") (r "^3.1") (o #t) (d #t) (k 0)))) (h "1kp598wbl5hrdk7bxj1rzl221apf7218pgljz6zkq6rxvxg0zlv1") (f (quote (("protocol-support" "protocol" "protocol-derive") ("default" "protocol-support"))))))

(define-public crate-xproto-2.0.1 (c (n "xproto") (v "2.0.1") (d (list (d (n "protocol") (r "^3.1") (o #t) (d #t) (k 0)) (d (n "protocol-derive") (r "^3.1") (o #t) (d #t) (k 0)))) (h "0arn7spb8kjwz7bzvz0hlyrs4688dgf7rizyabnh6j11nbns9b36") (f (quote (("protocol-support" "protocol" "protocol-derive") ("default" "protocol-support"))))))

