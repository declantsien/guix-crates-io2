(define-module (crates-io xp ro xprompt) #:use-module (crates-io))

(define-public crate-xprompt-0.1.0 (c (n "xprompt") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "clap_derive") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "gethostname") (r "^0.2.1") (d #t) (k 0)) (d (n "git2") (r "^0.13.12") (k 0)))) (h "1isg082fdvd9f7nhznl8li7jgzr73qmnyizc353amlhzgpx5x8yj") (y #t)))

(define-public crate-xprompt-0.1.1 (c (n "xprompt") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "clap_derive") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.12") (k 0)))) (h "00kr04amnyw8p0r0vxi3qv55v8hdgbzgi80an27p6fxzgclzk199")))

(define-public crate-xprompt-0.2.0 (c (n "xprompt") (v "0.2.0") (d (list (d (n "ansi_term") (r ">=0.12.1, <0.13.0") (d #t) (k 0)) (d (n "clap") (r ">=3.0.0-beta.2, <4.0.0") (d #t) (k 0)) (d (n "clap_derive") (r ">=3.0.0-beta.2, <4.0.0") (d #t) (k 0)) (d (n "git2") (r ">=0.13.12, <0.14.0") (k 0)))) (h "00ms4mvx3v0g09dfajir5acqa0kah757qfmahlw6ds99nzz8cr2f")))

(define-public crate-xprompt-0.2.1 (c (n "xprompt") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "clap_derive") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "git2") (r "^0.13.12") (k 0)))) (h "0yadyjcsncpnqpvv2jz40k8nv19k6dhkq0i6i78q2n6sk3kjvd12")))

(define-public crate-xprompt-0.2.2 (c (n "xprompt") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "clap_derive") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "git2") (r "^0.13.12") (k 0)))) (h "0bz5xb4pz2zzn2mm488jw95awvqa14dmyvdni4p3is5hsxb819j0")))

(define-public crate-xprompt-0.2.3 (c (n "xprompt") (v "0.2.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "git2") (r "^0.13.12") (k 0)))) (h "1h99akjy6xfvm0yw2478qfz0db1qqr5awvy5bigkim7islhyfy3d")))

