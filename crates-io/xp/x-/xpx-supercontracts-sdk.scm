(define-module (crates-io xp x- xpx-supercontracts-sdk) #:use-module (crates-io))

(define-public crate-xpx-supercontracts-sdk-0.2.0 (c (n "xpx-supercontracts-sdk") (v "0.2.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11682m9gzwwkh3ijd1q51mq10s8zgngpdn737lqbmnm8jf4j2pr3")))

(define-public crate-xpx-supercontracts-sdk-0.2.1 (c (n "xpx-supercontracts-sdk") (v "0.2.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fjyj6rhngqkpvaskk554xb2vm10xsfn6s7hl69npwn2ddp82i22")))

(define-public crate-xpx-supercontracts-sdk-0.2.2 (c (n "xpx-supercontracts-sdk") (v "0.2.2") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nbq4r8yz84lzw5sjr5haw5bf7r2cr1zad1lyn6nzblnczxjxggq")))

