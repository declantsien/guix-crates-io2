(define-module (crates-io xp -x xp-xcm) #:use-module (crates-io))

(define-public crate-xp-xcm-0.3.4 (c (n "xp-xcm") (v "0.3.4") (d (list (d (n "codec") (r "^3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-std") (r "^4.0.0") (k 0)) (d (n "xcm") (r "^0") (k 0)) (d (n "xcm-executor") (r "^0") (o #t) (k 0)))) (h "13jca62xvfliard8lpvpr6z8dz042zkiqlccfb2arpxg250gr1s2") (f (quote (("std" "xcm/std" "codec/std" "sp-std/std") ("frame-std" "xcm-executor/std") ("frame" "xcm-executor") ("default" "std" "frame" "frame-std"))))))

(define-public crate-xp-xcm-0.3.5 (c (n "xp-xcm") (v "0.3.5") (d (list (d (n "codec") (r "^3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-std") (r "^4.0.0") (k 0)) (d (n "xcm") (r "^0") (k 0)) (d (n "xcm-executor") (r "^0") (o #t) (k 0)))) (h "1y8b4dyc58y0a49zp8x0s8plnd4anl6idsn48dlqjd96ddfkx36d") (f (quote (("std" "xcm/std" "codec/std" "sp-std/std") ("frame-std" "xcm-executor/std") ("frame" "xcm-executor") ("default" "std" "frame" "frame-std"))))))

(define-public crate-xp-xcm-0.3.6 (c (n "xp-xcm") (v "0.3.6") (d (list (d (n "codec") (r "^3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-std") (r "^4.0.0") (k 0)) (d (n "xcm") (r "^0") (k 0)) (d (n "xcm-executor") (r "^0") (o #t) (k 0)))) (h "1h5cgiwm3mmxcl5i8fnsw3siyhhwyqsam438sjks3843xqgx8ys6") (f (quote (("std" "xcm/std" "codec/std" "sp-std/std") ("frame-std" "xcm-executor/std") ("frame" "xcm-executor") ("default" "std" "frame" "frame-std"))))))

(define-public crate-xp-xcm-0.3.7 (c (n "xp-xcm") (v "0.3.7") (d (list (d (n "codec") (r "^3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "sp-std") (r "^4.0.0") (k 0)) (d (n "xcm") (r "^0") (k 0)) (d (n "xcm-executor") (r "^0") (o #t) (k 0)))) (h "13987cf3l4sd9dhyxwq8wa621cx9qaig69idc2zvp9w78fdiarn8") (f (quote (("std" "xcm/std" "codec/std" "sp-std/std") ("frame-std" "xcm-executor/std") ("frame" "xcm-executor") ("default" "std" "frame" "frame-std"))))))

