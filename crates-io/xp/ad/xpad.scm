(define-module (crates-io xp ad xpad) #:use-module (crates-io))

(define-public crate-xpad-0.1.0 (c (n "xpad") (v "0.1.0") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "0yblxvdinbwf33papqajf2g7mxdbwvzi75y4a0108fgnh2793js5")))

