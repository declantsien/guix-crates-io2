(define-module (crates-io xp la xplan) #:use-module (crates-io))

(define-public crate-xplan-0.1.0 (c (n "xplan") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.13") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.3") (d #t) (k 0)))) (h "0yadmkvj3f46zp5avyxabv4z2815da7433r1sj1r0aqbj6j82yjw")))

