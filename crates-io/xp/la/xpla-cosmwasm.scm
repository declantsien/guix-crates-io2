(define-module (crates-io xp la xpla-cosmwasm) #:use-module (crates-io))

(define-public crate-xpla-cosmwasm-0.1.0 (c (n "xpla-cosmwasm") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (f (quote ("stargate"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1fm2z5c57adzns6bgpi35vl3b25ayr98p7b9bj182hsg69q3m9in") (f (quote (("backtraces" "cosmwasm-std/backtraces"))))))

