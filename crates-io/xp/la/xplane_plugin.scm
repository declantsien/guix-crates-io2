(define-module (crates-io xp la xplane_plugin) #:use-module (crates-io))

(define-public crate-xplane_plugin-0.1.0 (c (n "xplane_plugin") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1d8k05ip12zdlif5s19gc9c2rvqy77kfp1f26dznd3xm0djs3a10")))

(define-public crate-xplane_plugin-0.1.1 (c (n "xplane_plugin") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1mz2rbm2fj66fz2y4z6znlj9yc4gpp4a9xddx6kckfmhfw5n46a2")))

