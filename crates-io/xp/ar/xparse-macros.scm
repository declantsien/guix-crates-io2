(define-module (crates-io xp ar xparse-macros) #:use-module (crates-io))

(define-public crate-xparse-macros-0.1.0 (c (n "xparse-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1p2xgq7xph310gl2n4gnqhvaxhs08kdixb3qpswhb3kwnqjf67p4") (f (quote (("async")))) (y #t)))

(define-public crate-xparse-macros-0.1.1 (c (n "xparse-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1203gvy0sxkjm3cv2pma8pp902xp8jrwzifid60ly0iliigi9mj5") (f (quote (("async")))) (y #t)))

(define-public crate-xparse-macros-0.1.2 (c (n "xparse-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0v27lf23gfqkg2pkiwga02bbrl78p1m4hdxdpvh2yyp2kjaz2ls7") (f (quote (("async")))) (y #t)))

(define-public crate-xparse-macros-0.1.3 (c (n "xparse-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1hsba55jibcjrm9mlh9yhsni0d4qxhqgkxwfikhc20ziq6l0qxw8") (f (quote (("async")))) (y #t)))

(define-public crate-xparse-macros-0.1.4 (c (n "xparse-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0bx3vjl4sny2dlfh05sp6flps11qz8yc24flbq9py4w6zvs6nq8p") (f (quote (("async")))) (y #t)))

(define-public crate-xparse-macros-0.1.5 (c (n "xparse-macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0awbgm335fbwphqma6716r6b66myhcxw2s7fwnh8kjhn49ahfaic") (f (quote (("async")))) (y #t)))

(define-public crate-xparse-macros-0.1.6 (c (n "xparse-macros") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0109724aj30wjqi321lhxfawig65c87rk6bcwpyyazp7m3znfnvx") (f (quote (("async")))) (y #t)))

(define-public crate-xparse-macros-0.1.7 (c (n "xparse-macros") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0fjgm64pfqvdmp9937pjgj7sqmc6pp88m9jxahjnba0zqgn8ad2n") (f (quote (("async"))))))

(define-public crate-xparse-macros-0.1.8 (c (n "xparse-macros") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0jlc48agkik0fny2kxc5r20ws4yc6ldnib5ns6f8wksd569riq29") (f (quote (("async"))))))

(define-public crate-xparse-macros-0.1.9 (c (n "xparse-macros") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0k1y048cz62xkixxz37s1qw4s6v20sw0lllzm1hb6cvgba7qj6cj") (f (quote (("async"))))))

(define-public crate-xparse-macros-0.1.10 (c (n "xparse-macros") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0l91bswl3r62id2dkrn3m4x3sv7ahrba60dzv9f19x3iqnqskc6f") (f (quote (("async"))))))

