(define-module (crates-io xp q2 xpq2) #:use-module (crates-io))

(define-public crate-xpq2-0.2.2 (c (n "xpq2") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^3.2") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "either") (r "^1.7") (d #t) (k 0)) (d (n "parquet") (r "^22.0") (d #t) (k 0)) (d (n "parquet_derive") (r "^22.0") (d #t) (k 2)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "streaming-stats") (r "^0.2") (d #t) (k 0)) (d (n "tabwriter") (r "^1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0rxmwy65zjp106lj2r9l8r9zhvw8nf8170kmhcw19i8jl5n1lhy7")))

