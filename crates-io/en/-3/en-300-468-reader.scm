(define-module (crates-io en #{-3}# en-300-468-reader) #:use-module (crates-io))

(define-public crate-en-300-468-reader-0.1.0 (c (n "en-300-468-reader") (v "0.1.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mpeg2ts-reader") (r "^0.13") (d #t) (k 0)))) (h "0p3h1iqg9qiy5dxigkkqn3fg3dvzv2c63pblibw50cmk0yll03by")))

(define-public crate-en-300-468-reader-0.2.0 (c (n "en-300-468-reader") (v "0.2.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mpeg2ts-reader") (r "^0.14") (d #t) (k 0)))) (h "0qlnwnr53agpz25d0s9brdry6mjl229bxbpm1nz2424q523cj7l7")))

(define-public crate-en-300-468-reader-0.3.0 (c (n "en-300-468-reader") (v "0.3.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mpeg2ts-reader") (r "^0.15") (d #t) (k 0)))) (h "10gik6a1pj322nnfykpzz90jq162pq9j57bhkjb01dr1dn5db94v")))

