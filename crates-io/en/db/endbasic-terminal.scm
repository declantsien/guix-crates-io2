(define-module (crates-io en db endbasic-terminal) #:use-module (crates-io))

(define-public crate-endbasic-terminal-0.8.0 (c (n "endbasic-terminal") (v "0.8.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "endbasic-std") (r "^0.8.0") (d #t) (k 0)))) (h "07zzd5kipmzk0hjfha99q6k3zscyxmy7067x1g61999q9qwghdz1")))

(define-public crate-endbasic-terminal-0.8.1 (c (n "endbasic-terminal") (v "0.8.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "endbasic-std") (r "^0.8.1") (d #t) (k 0)))) (h "1rps5lyqxgfwd8mgswh6ad6s3q88y7skbck212whbbndvjilmvi0")))

(define-public crate-endbasic-terminal-0.9.0 (c (n "endbasic-terminal") (v "0.9.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "endbasic-std") (r "^0.9.0") (d #t) (k 0)))) (h "03dk8g3wjr27svprsbanw3yxfjm9dx634spqcinh19avprsb4731")))

(define-public crate-endbasic-terminal-0.10.0 (c (n "endbasic-terminal") (v "0.10.0") (d (list (d (n "async-channel") (r "^1.5") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "endbasic-core") (r "^0.10.0") (d #t) (k 0)) (d (n "endbasic-std") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)))) (h "0b597j5v1q8xk9cx2hr069a8k52ac27dax8gkxq3bs4ls86dlais")))

