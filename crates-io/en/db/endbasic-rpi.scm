(define-module (crates-io en db endbasic-rpi) #:use-module (crates-io))

(define-public crate-endbasic-rpi-0.8.0 (c (n "endbasic-rpi") (v "0.8.0") (d (list (d (n "endbasic-std") (r "^0.8.0") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "1i74f7qqjb5vsz1481sn66vjwlicy4gg49vz48pzwnwbvzsvanxd")))

(define-public crate-endbasic-rpi-0.8.1 (c (n "endbasic-rpi") (v "0.8.1") (d (list (d (n "endbasic-std") (r "^0.8.1") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "0059hv1f33pxxhz322kk9i7phdxa1a2532n3qraiv91wacgxmx1r")))

(define-public crate-endbasic-rpi-0.9.0 (c (n "endbasic-rpi") (v "0.9.0") (d (list (d (n "endbasic-std") (r "^0.9.0") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "0sdfm4g87wfnbyw5x8mhyiyazv6n77gdlf8z1ds20vq6d1vn70df")))

(define-public crate-endbasic-rpi-0.10.0 (c (n "endbasic-rpi") (v "0.10.0") (d (list (d (n "endbasic-std") (r "^0.10.0") (d #t) (k 0)) (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "0mirlfhwzbq8wxwwmqrzzgrdj1mqjx0x0b8zap8dm1c2i9dq6q44")))

