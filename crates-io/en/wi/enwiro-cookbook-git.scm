(define-module (crates-io en wi enwiro-cookbook-git) #:use-module (crates-io))

(define-public crate-enwiro-cookbook-git-0.1.0 (c (n "enwiro-cookbook-git") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.6.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.196") (d #t) (k 0)))) (h "1yacbz2s0ylyxnh6qkx8n8sv19nxrnhqia1w1rpypla1p8fjrqsh")))

(define-public crate-enwiro-cookbook-git-0.1.1 (c (n "enwiro-cookbook-git") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.6.1") (d #t) (k 0)) (d (n "git2") (r "^0.18.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.197") (d #t) (k 0)))) (h "0zkc02lc3p2sm8jgim1d4q6d5rm79qf2i76j9d6pzc6nnnj37pnx")))

