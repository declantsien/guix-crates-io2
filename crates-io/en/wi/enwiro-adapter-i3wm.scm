(define-module (crates-io en wi enwiro-adapter-i3wm) #:use-module (crates-io))

(define-public crate-enwiro-adapter-i3wm-0.1.0 (c (n "enwiro-adapter-i3wm") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.9") (d #t) (k 0)) (d (n "i3ipc-types") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (d #t) (k 0)) (d (n "tokio-i3ipc") (r "^0.16.0") (d #t) (k 0)))) (h "16ri1yvbyqh6156pkjdgpsd4lsj8il2j4y84zzszrqvax9nhxszf")))

(define-public crate-enwiro-adapter-i3wm-0.1.1 (c (n "enwiro-adapter-i3wm") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.9") (d #t) (k 0)) (d (n "i3ipc-types") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (d #t) (k 0)) (d (n "tokio-i3ipc") (r "^0.16.0") (d #t) (k 0)))) (h "0xhvx9s1h0gggflfnk22awykxp4ibkld4ibdx78whj7i0bi7kihj")))

