(define-module (crates-io en sf ensf594-project-mmap) #:use-module (crates-io))

(define-public crate-ensf594-project-mmap-0.1.0 (c (n "ensf594-project-mmap") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive" "std" "color"))) (k 2)) (d (n "rand") (r "^0") (f (quote ("getrandom" "std" "std_rng"))) (k 2)))) (h "004r1f9aqa7j6flcdzkv9mb6p4zk7j02qvj46pxc1ka9652rnrcm")))

(define-public crate-ensf594-project-mmap-0.1.1 (c (n "ensf594-project-mmap") (v "0.1.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive" "std" "color"))) (k 2)) (d (n "rand") (r "^0") (f (quote ("getrandom" "std" "std_rng"))) (k 2)))) (h "0yxaska247586zdyk6a05y93ghg3m21mbj4vn4ap6g6isqg4k9nj")))

(define-public crate-ensf594-project-mmap-0.3.0 (c (n "ensf594-project-mmap") (v "0.3.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive" "std" "color"))) (k 2)) (d (n "rand") (r "^0") (f (quote ("getrandom" "std" "std_rng"))) (k 2)))) (h "0wh8k60jva2y5v8vbzwi8vxx4ahwnajpi9sihz1f3mp2mslmzrr7")))

(define-public crate-ensf594-project-mmap-0.4.0 (c (n "ensf594-project-mmap") (v "0.4.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive" "std" "color"))) (k 2)) (d (n "rand") (r "^0") (f (quote ("getrandom" "std" "std_rng"))) (k 2)))) (h "1gd6qcb4qvr6hmxalbamqm5kdsgy3cg0f4aqhck8did11f7gb763")))

(define-public crate-ensf594-project-mmap-0.5.0 (c (n "ensf594-project-mmap") (v "0.5.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive" "std" "color"))) (k 2)) (d (n "rand") (r "^0") (f (quote ("getrandom" "std" "std_rng"))) (k 2)))) (h "0hzb0bxcs43pb3pwb7scybiq21f1a989yxfjnzh246l6v634vv6l")))

