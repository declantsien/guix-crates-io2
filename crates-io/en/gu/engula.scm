(define-module (crates-io en gu engula) #:use-module (crates-io))

(define-public crate-engula-0.1.0 (c (n "engula") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "microunit") (r "^0.1.0") (d #t) (k 0)))) (h "1m6b1h1ny516w6rflppbqc7m7ncgzi8rfc7b61dfq7qdkk7sz6cd") (y #t)))

(define-public crate-engula-0.1.1 (c (n "engula") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "engula-supreme-unit") (r "^0.1.0") (d #t) (k 0)) (d (n "microunit") (r "^0.1.1") (d #t) (k 0)))) (h "1n9zn0r83gn2fd835xydadi4dh81pgw159xv1phllj5rbhk1wsfz") (y #t)))

(define-public crate-engula-0.2.0-alpha.0 (c (n "engula") (v "0.2.0-alpha.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "engula-journal") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "engula-kernel") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "engula-storage") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "hash-engine") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.6") (d #t) (k 0)))) (h "1070ai5wh0szxcwj5lgv0rld8jrsmwdfrcip3bdsmqq3739rbgaw") (y #t)))

(define-public crate-engula-0.2.0 (c (n "engula") (v "0.2.0") (d (list (d (n "clap") (r "=3.0.0-beta.5") (d #t) (k 0)) (d (n "engula-journal") (r "^0.2") (d #t) (k 0)) (d (n "engula-kernel") (r "^0.2") (d #t) (k 0)) (d (n "engula-storage") (r "^0.2") (d #t) (k 0)) (d (n "hash-engine") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.6") (d #t) (k 0)))) (h "02lfgccipx8amwh3qnfpflcxk77awyrixyj1bhb54id46687vnhs")))

(define-public crate-engula-0.3.0 (c (n "engula") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "engula-transactor") (r "^0.3") (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.8") (f (quote ("net"))) (d #t) (k 0)) (d (n "tonic") (r "^0.6") (d #t) (k 0)) (d (n "tracing") (r "^0.1.31") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.9") (d #t) (k 0)))) (h "0gm0177rr9m1ifc5vmd222di6d3qx5klnna7his4fn0dfg1785xn")))

