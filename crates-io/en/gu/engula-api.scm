(define-module (crates-io en gu engula-api) #:use-module (crates-io))

(define-public crate-engula-api-0.4.0 (c (n "engula-api") (v "0.4.0") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "prost-types") (r "^0.10.1") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "1ldrzvmfpxgvf0d03p8hi2l6ncill661rrgnd5kiqksldzrdhcxs")))

