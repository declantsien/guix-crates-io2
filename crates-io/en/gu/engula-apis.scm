(define-module (crates-io en gu engula-apis) #:use-module (crates-io))

(define-public crate-engula-apis-0.3.0 (c (n "engula-apis") (v "0.3.0") (d (list (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "tonic") (r "^0.6") (d #t) (k 0)) (d (n "tonic-build") (r "^0.6") (d #t) (k 1)))) (h "1h0kpx0psbq2mswnq8m6klckz95rb719kkibljwks6hnjjbsympp")))

