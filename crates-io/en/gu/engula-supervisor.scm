(define-module (crates-io en gu engula-supervisor) #:use-module (crates-io))

(define-public crate-engula-supervisor-0.3.0 (c (n "engula-supervisor") (v "0.3.0") (d (list (d (n "engula-apis") (r "^0.3") (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.6") (d #t) (k 0)) (d (n "tonic-build") (r "^0.6") (d #t) (k 1)))) (h "003m4v0k1pszr9g925mbzv12xk0askrgnlwk4w5wka0bsfmqvxh7")))

