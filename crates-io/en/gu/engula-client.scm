(define-module (crates-io en gu engula-client) #:use-module (crates-io))

(define-public crate-engula-client-0.3.0 (c (n "engula-client") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "engula-apis") (r "^0.3") (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.6") (d #t) (k 0)))) (h "1hjzc9p95l8x7g0jk00hadqwj7dha0k8sfsqjiym2x676ppr4nwq")))

(define-public crate-engula-client-0.4.0 (c (n "engula-client") (v "0.4.0") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "engula-api") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.8") (f (quote ("net"))) (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("std" "env-filter"))) (d #t) (k 2)))) (h "1jfr36wvz4ch1gfly25ipf4dbnnygj2hnlz4mv51c96ylhn7knfr")))

