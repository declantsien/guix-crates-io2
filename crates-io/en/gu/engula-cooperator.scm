(define-module (crates-io en gu engula-cooperator) #:use-module (crates-io))

(define-public crate-engula-cooperator-0.3.0 (c (n "engula-cooperator") (v "0.3.0") (d (list (d (n "engula-apis") (r "^0.3") (d #t) (k 0)) (d (n "engula-supervisor") (r "^0.3") (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.6") (d #t) (k 0)) (d (n "tonic-build") (r "^0.6") (d #t) (k 1)))) (h "1sg4ln9y5zdnwzsa36pmjx3k29yhbhy98pigd87p5614pq7iip5f")))

