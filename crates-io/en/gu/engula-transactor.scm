(define-module (crates-io en gu engula-transactor) #:use-module (crates-io))

(define-public crate-engula-transactor-0.3.0 (c (n "engula-transactor") (v "0.3.0") (d (list (d (n "engula-apis") (r "^0.3") (d #t) (k 0)) (d (n "engula-cooperator") (r "^0.3") (d #t) (k 0)) (d (n "engula-supervisor") (r "^0.3") (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.6") (d #t) (k 0)))) (h "1gghcqc2fdmvw6lpd62l3bsi9sp849qcx47hpff618q7aj58lbgc")))

