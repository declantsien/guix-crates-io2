(define-module (crates-io en si ensicoin_serializer) #:use-module (crates-io))

(define-public crate-ensicoin_serializer-0.1.0 (c (n "ensicoin_serializer") (v "0.1.0") (h "1skf9symmjm1nwx1bp07cmh3d59sj4b9ma6lw1cq3gd7mmpy86ld")))

(define-public crate-ensicoin_serializer-0.1.1 (c (n "ensicoin_serializer") (v "0.1.1") (h "0nrfjn4xpcxcifr3m6a0snjim32ma7jy7y0a9zzvdg5qf0m0pc91")))

(define-public crate-ensicoin_serializer-0.1.2 (c (n "ensicoin_serializer") (v "0.1.2") (h "1ykip8arxmy54miq4jf4wslpxilgp98ffld7srxi9sqa0pwf5gy4")))

(define-public crate-ensicoin_serializer-0.2.0 (c (n "ensicoin_serializer") (v "0.2.0") (h "1zchnv9pkfs7psnh8kawlxnj66cl4d32713zvc6mz7w4b190gnn1")))

(define-public crate-ensicoin_serializer-0.3.0 (c (n "ensicoin_serializer") (v "0.3.0") (h "1k7r5xk1cqsd2cm1cdsqc001k26kahyhh4cqmdyyrgswq4gj3dcn")))

(define-public crate-ensicoin_serializer-0.4.0 (c (n "ensicoin_serializer") (v "0.4.0") (h "172v10lpkkivdsp0c1q13q81m9flllgrpdlrfrrjjy028870ba56")))

(define-public crate-ensicoin_serializer-0.4.1 (c (n "ensicoin_serializer") (v "0.4.1") (h "1b8mici2b2b2c0ss7xq0c4kwkl4qwpl9ncir5y8ap81rkcrada0k")))

(define-public crate-ensicoin_serializer-0.4.2 (c (n "ensicoin_serializer") (v "0.4.2") (h "0fw327rnp7qgp7r8dl28i1ip1gg9rfk9w44qyhdfsz4xxd7p30ji")))

(define-public crate-ensicoin_serializer-0.4.3 (c (n "ensicoin_serializer") (v "0.4.3") (h "08a0pc5i8vgffyq88ssdfrr19ivz1nncqcmwa1l1pcy1r18q7y6q")))

(define-public crate-ensicoin_serializer-1.0.0 (c (n "ensicoin_serializer") (v "1.0.0") (d (list (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "0h518r5rmd6w6imlabqc1kv9zkivi8xwwlq15yxlgimnavhsygvc")))

(define-public crate-ensicoin_serializer-1.0.1 (c (n "ensicoin_serializer") (v "1.0.1") (d (list (d (n "ensicoin_serializer_derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "158kw6qm02b43larhkblrf6lwm5ymsr2vfspb4ydpkhksqz6sksj")))

(define-public crate-ensicoin_serializer-2.0.0 (c (n "ensicoin_serializer") (v "2.0.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "ensicoin_serializer_derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "1cm3qbyxhkg27lay824wg2ya2lkzhwn5xhxlx7qxpz7pdd65864r")))

(define-public crate-ensicoin_serializer-2.0.1 (c (n "ensicoin_serializer") (v "2.0.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "ensicoin_serializer_derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "0rqs4a0jwxmcxwji2fa88ldrxy4iya37xbj5vnk7f0mfrlrfgha0")))

(define-public crate-ensicoin_serializer-2.0.2 (c (n "ensicoin_serializer") (v "2.0.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "ensicoin_serializer_derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "0pjja692dbih9sc16n2rjsky7m2hv8v5l3lf8jkz2np81zqp94h1")))

(define-public crate-ensicoin_serializer-2.0.3 (c (n "ensicoin_serializer") (v "2.0.3") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "ensicoin_serializer_derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "1mg4s2c2x1q7v72f9l9mvi6h2q9vcbsxgz4ylg68rhh8wq02gfdw")))

(define-public crate-ensicoin_serializer-2.0.4 (c (n "ensicoin_serializer") (v "2.0.4") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "ensicoin_serializer_derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "1gkdb54qi9flpp1hqvap7wlmjvmm8nvq6k3663ip8vwlyz4rhd6x")))

(define-public crate-ensicoin_serializer-2.0.5 (c (n "ensicoin_serializer") (v "2.0.5") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "ensicoin_serializer_derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "1jr5qkprx2gpprg8rhcma0q8qlmhzx2jrvvg2xixh7rvap3g6w9c")))

(define-public crate-ensicoin_serializer-2.0.6 (c (n "ensicoin_serializer") (v "2.0.6") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "ensicoin_serializer_derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "0i8dd18l1ksq71vcffh8inhpz7b53zpfn9jmj9baq7mqhgbl69ck")))

(define-public crate-ensicoin_serializer-2.0.7 (c (n "ensicoin_serializer") (v "2.0.7") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "ensicoin_serializer_derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.10.0") (d #t) (k 0)))) (h "1by95ydyqwhz8y6smilfkdc8pg7fjjijkp7y24nz56m9y81vbmld")))

(define-public crate-ensicoin_serializer-2.0.8 (c (n "ensicoin_serializer") (v "2.0.8") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "ensicoin_serializer_derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.13.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.11.2") (d #t) (k 0)))) (h "1jlpyws11l1w0f9rf0i9zy8qsd1m1a327l6xjp1byd7nka66kz4y")))

(define-public crate-ensicoin_serializer-2.0.9 (c (n "ensicoin_serializer") (v "2.0.9") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "ensicoin_serializer_derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.12.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.11.2") (d #t) (k 0)))) (h "0p1dhbsh4fc7vi6q5xk24gaqhidjwgziy7y9jv7p3glyg5pr15h9")))

