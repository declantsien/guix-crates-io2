(define-module (crates-io en si ensicoin_serializer_derive) #:use-module (crates-io))

(define-public crate-ensicoin_serializer_derive-0.1.0 (c (n "ensicoin_serializer_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)))) (h "1ykzvdd28isb9qvn9d1mxpc8x2ya6226p0clwkqk4z1zsh8rakxk")))

(define-public crate-ensicoin_serializer_derive-0.2.0 (c (n "ensicoin_serializer_derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)))) (h "0bnrxsm37pldc7r6xvxwyk6lpwdbd1zvabq2c6i20j6x7ymshjbl")))

(define-public crate-ensicoin_serializer_derive-0.2.1 (c (n "ensicoin_serializer_derive") (v "0.2.1") (d (list (d (n "ensicoin_serializer") (r "^1.0") (d #t) (k 2)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)))) (h "0laqvjzl2klc3yn9xcwmvl760acsn0kmvbhbbbmskp3f74y5rx6z")))

(define-public crate-ensicoin_serializer_derive-0.2.2 (c (n "ensicoin_serializer_derive") (v "0.2.2") (d (list (d (n "ensicoin_serializer") (r "^1.0") (d #t) (k 2)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)))) (h "0qrx0v7vrf7yphxlv550i7h2fxg67d22sp49amkkjwky23j4jgf5")))

(define-public crate-ensicoin_serializer_derive-0.2.3 (c (n "ensicoin_serializer_derive") (v "0.2.3") (d (list (d (n "ensicoin_serializer") (r "^1.0") (d #t) (k 2)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)))) (h "0p23n8plvgf01n642dz8vhvzmg1gg70cg3i8zpr8p051cjyvxm03")))

(define-public crate-ensicoin_serializer_derive-0.2.4 (c (n "ensicoin_serializer_derive") (v "0.2.4") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 2)) (d (n "ensicoin_serializer") (r "^2.0") (d #t) (k 2)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)))) (h "00znyv3dljkwkn29zm5njmgxg7g2yny14apcx2rgv1lybixnqrf9")))

(define-public crate-ensicoin_serializer_derive-0.2.5 (c (n "ensicoin_serializer_derive") (v "0.2.5") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 2)) (d (n "ensicoin_serializer") (r "^2.0") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0vk2ny79r09m6p4pbyp9ws7px3kq885d7gys89mll8vrqh77zvck")))

(define-public crate-ensicoin_serializer_derive-0.2.6 (c (n "ensicoin_serializer_derive") (v "0.2.6") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 2)) (d (n "ensicoin_serializer") (r "^2.0") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "08gkga4jw0k8gzhwk1j47xbblmw4vqkzppwcix9gc1wf1f87ipa9")))

(define-public crate-ensicoin_serializer_derive-0.2.7 (c (n "ensicoin_serializer_derive") (v "0.2.7") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 2)) (d (n "ensicoin_serializer") (r "^2.0.7") (d #t) (k 2)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "0xvm2d35wg8ch8kxhfvs9nd39bqfv7hmlahncsjk1gm7lhwp5xrj")))

