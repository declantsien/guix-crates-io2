(define-module (crates-io en fo enforcer_backend) #:use-module (crates-io))

(define-public crate-enforcer_backend-0.1.0 (c (n "enforcer_backend") (v "0.1.0") (d (list (d (n "psutil") (r "^3.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1jxkyibc53fzfjy2336xglzdq3358zr5n0ns0b4vkzsvvzwq1rl1")))

(define-public crate-enforcer_backend-0.1.1 (c (n "enforcer_backend") (v "0.1.1") (d (list (d (n "psutil") (r "^3.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1zrhd8w6zg0gynmrs5b38ms5bkfx0q5jnszyyc6xxrrzd9jicscp")))

(define-public crate-enforcer_backend-0.1.2 (c (n "enforcer_backend") (v "0.1.2") (d (list (d (n "psutil") (r "^3.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0bj2fqjh0fqy16s3nailw6zcpikg71cnk8hkms9q3n1w3jygs99s")))

