(define-module (crates-io en zy enzyme) #:use-module (crates-io))

(define-public crate-enzyme-0.1.0 (c (n "enzyme") (v "0.1.0") (h "1ig130ky81s878907cgdfpm4kd2cn8ljqkxbfakq1x6xsdlnb5rj")))

(define-public crate-enzyme-0.2.0 (c (n "enzyme") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.58") (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (k 0)))) (h "0097lrs79r9w6hv4aj5nqr0hsbf9x74ygpdcvf07il2fm467xb7h")))

(define-public crate-enzyme-0.3.0 (c (n "enzyme") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.58") (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (k 0)))) (h "0vghff3jcly69dqcz2h3306v2zv2s7na66f6biqxf1c28ynpl75v")))

(define-public crate-enzyme-0.4.0 (c (n "enzyme") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.58") (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (k 0)))) (h "1gckjfcvpf5yzcylbjnq2fhh2dvh54h0s0s6md49yi69d1crvs9r")))

