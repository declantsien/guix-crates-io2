(define-module (crates-io en ha enhanced_enum) #:use-module (crates-io))

(define-public crate-enhanced_enum-0.1.0 (c (n "enhanced_enum") (v "0.1.0") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0jrbs3iw13kyrvip2kb8ipd1gd8iax40h2271mlqn1bfsbbq6fvw")))

(define-public crate-enhanced_enum-0.2.0 (c (n "enhanced_enum") (v "0.2.0") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "19jbvhjbpysdf9swwbsqd3y9gwxc0hyflmphb5qyw7dm4s0d34pg") (f (quote (("pyo3"))))))

(define-public crate-enhanced_enum-0.2.1 (c (n "enhanced_enum") (v "0.2.1") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0r7k22n6zgapbmlybzx39xkqi8lql9lpmn4nq56mkyjbpv9fk0sx") (f (quote (("pyo3"))))))

(define-public crate-enhanced_enum-0.2.2 (c (n "enhanced_enum") (v "0.2.2") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "1lhc30nbhdpkvxgjvrzlyggw8alyxw9afyiydbcpc0mvhmi85gzz") (f (quote (("pyo3"))))))

