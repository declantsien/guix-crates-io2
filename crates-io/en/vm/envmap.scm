(define-module (crates-io en vm envmap) #:use-module (crates-io))

(define-public crate-envmap-0.0.1 (c (n "envmap") (v "0.0.1") (h "0zxi6slnwa21a0s84nbjr8qhk3gpcmphbdwaiyhkg735wgzwnxy6")))

(define-public crate-envmap-0.0.2 (c (n "envmap") (v "0.0.2") (h "1ddzkq10qlsc6bvkw8sfk11h41y70i2n0dib4syfpc2jfhv3rkwg")))

