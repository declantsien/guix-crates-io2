(define-module (crates-io en vm envmnt) #:use-module (crates-io))

(define-public crate-envmnt-0.1.0 (c (n "envmnt") (v "0.1.0") (h "1mvm93nvcn5r6x6vgmbhd9pv1bs6sph1894fyd2pq29kavwlxih6")))

(define-public crate-envmnt-0.1.1 (c (n "envmnt") (v "0.1.1") (h "02s6607aqpj392lg2pdi3njj2l4y6plr2mpwrdkf1phsz6y6s3q4")))

(define-public crate-envmnt-0.2.0 (c (n "envmnt") (v "0.2.0") (h "06idhfnq38j3vx0j92z5vf3akdpq987zd05da3w8sj533qzipkpw")))

(define-public crate-envmnt-0.3.0 (c (n "envmnt") (v "0.3.0") (h "0n5y8768rar55ixrjbznlqgv1hy9mwx8g5lgfh3iy7d8a149xg41")))

(define-public crate-envmnt-0.3.1 (c (n "envmnt") (v "0.3.1") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)))) (h "0pjcljl3q26ggwcighw7755fvb6hxbbs3kmh9yffxm22syxssnjn")))

(define-public crate-envmnt-0.4.0 (c (n "envmnt") (v "0.4.0") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)))) (h "0iz1q5ssb6y5wyqq3s74671chcsj9wqsnjfhwvlrbjz9hd8642gj")))

(define-public crate-envmnt-0.4.1 (c (n "envmnt") (v "0.4.1") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)))) (h "1kx5x5agfv34hsnck64v33w5chb9cgwjm5xwgv3vzvjzkiph3nhy")))

(define-public crate-envmnt-0.5.0 (c (n "envmnt") (v "0.5.0") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)))) (h "0c5rifbsixc2xfgd0gkf65yglwmqx62zmpcmqgfnnw4p8xfc541p")))

(define-public crate-envmnt-0.6.0 (c (n "envmnt") (v "0.6.0") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)))) (h "12zkq3p999bypyxmjnpiqw9r3hmifb3bcikd7j3as1fdcbq01fyl")))

(define-public crate-envmnt-0.7.0 (c (n "envmnt") (v "0.7.0") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)))) (h "1vr5787sn701azi0ma5akpd2mw5abvgp3vkjxqck67wapiwwjq3p")))

(define-public crate-envmnt-0.7.1 (c (n "envmnt") (v "0.7.1") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)))) (h "1jlyifi4wpjdirp63mfmdyglj20z7sqd2fpvf7dkyb4zd8x82nfv")))

(define-public crate-envmnt-0.7.2 (c (n "envmnt") (v "0.7.2") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)))) (h "0wrlk12acjdgdabi5zbaxi0pg8xqycvp8g51w0mzbk2wblmwfkr3")))

(define-public crate-envmnt-0.7.3 (c (n "envmnt") (v "0.7.3") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)))) (h "1pz66advl6q052fjc6f4x3s5w01x5j7qp0rymjyn6ryi5ljgrvb7")))

(define-public crate-envmnt-0.7.4 (c (n "envmnt") (v "0.7.4") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)))) (h "0arzzrwi188p2ac31glari7fvfvb61w54razdh3qcwxz07xzvii4")))

(define-public crate-envmnt-0.7.5 (c (n "envmnt") (v "0.7.5") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)))) (h "0sdw6yyz85kvwqd2kbglhqxy70xarcizh3npknjwzf0hpzyxkk9r")))

(define-public crate-envmnt-0.8.0 (c (n "envmnt") (v "0.8.0") (d (list (d (n "indexmap") (r "^1.0") (d #t) (k 0)))) (h "0lkk5zyg5fh26vqc3w16v7lcpgsvvzck4373ls8pz8sc2cb1y46v")))

(define-public crate-envmnt-0.8.1 (c (n "envmnt") (v "0.8.1") (d (list (d (n "fsio") (r "^0.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.0") (d #t) (k 0)))) (h "0zzk2zi5hgk35hzx09ghma910cr98vf9vx3qh6k2bpzljavacyfp")))

(define-public crate-envmnt-0.8.2 (c (n "envmnt") (v "0.8.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "fsio") (r "^0.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)))) (h "1j132yacqb04790pb71x2z4hdqj7ir5z1mivggdi4zh0iiypxfjz")))

(define-public crate-envmnt-0.8.3 (c (n "envmnt") (v "0.8.3") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "fsio") (r "^0.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)))) (h "16ri54l14nii5bywhbhplg5a5ml1fqkrd50dchi08lxzmq6idb8d")))

(define-public crate-envmnt-0.8.4 (c (n "envmnt") (v "0.8.4") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "fsio") (r "^0.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)))) (h "0na01z737h9yn5y8qj3qjzipirnprgyprbv199632qbw53y2ilx2")))

(define-public crate-envmnt-0.9.0 (c (n "envmnt") (v "0.9.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "fsio") (r "^0.2") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)))) (h "07p1cah5df8kwah885dj8psypz1y65zpn8j3m5w1vr4nk4gcbynv")))

(define-public crate-envmnt-0.9.1 (c (n "envmnt") (v "0.9.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "fsio") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)))) (h "07jpfvhcy86n69csw5amxgzlpcvg1vzjv4y3vsccdyhj5y3dv5hg")))

(define-public crate-envmnt-0.10.0 (c (n "envmnt") (v "0.10.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "fsio") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)))) (h "1vkfg541lz4a78b6zwvs9f3aw22mmr21pk6zi0rrwv4ymp52zfwz")))

(define-public crate-envmnt-0.10.1 (c (n "envmnt") (v "0.10.1") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "fsio") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)))) (h "1jm1say9l2bn3sclphm6ryk9wyg67zznsb4x2ly34asi46yd5624")))

(define-public crate-envmnt-0.10.2 (c (n "envmnt") (v "0.10.2") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "fsio") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)))) (h "1p5z7ra8vbyqy43imncdh8zy754s2yh0q9n2sb5ngs8xnyb5dyvh")))

(define-public crate-envmnt-0.10.4 (c (n "envmnt") (v "0.10.4") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "fsio") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)))) (h "00am3348qp1v9cdckbsgn88m1n7kx6g7a8xwp34787l7p2i9jffp")))

