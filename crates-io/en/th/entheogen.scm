(define-module (crates-io en th entheogen) #:use-module (crates-io))

(define-public crate-entheogen-0.1.0 (c (n "entheogen") (v "0.1.0") (d (list (d (n "alga") (r "^0.8") (d #t) (k 0)) (d (n "cairo-rs") (r "^0.6") (f (quote ("png"))) (d #t) (k 0)) (d (n "nalgebra") (r "^0.17") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "palette") (r "^0.4") (d #t) (k 0)))) (h "0lj4z45m1q8s3rw7i6js45qyz9cbbfikmjsizbc38q1qkm5bn53n")))

