(define-module (crates-io en sc enschin) #:use-module (crates-io))

(define-public crate-enschin-0.0.1 (c (n "enschin") (v "0.0.1") (d (list (d (n "box2d") (r "^0.0.2") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "glfw") (r "^0.6.0") (d #t) (k 0)))) (h "0mv1gs1ry8nzlism41v2qf1pzvli6kp7pmv84q1cx8fp14n9jsqy") (y #t)))

(define-public crate-enschin-0.1.0 (c (n "enschin") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "enschin_proc") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "ndk-glue") (r "^0.5.0") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (d #t) (k 0)) (d (n "winit") (r "^0.26.1") (d #t) (k 0)))) (h "01kfp31k5xsqldnkkvcg02kphm3jg9yl7rarj5w1dnm3zgnjm4dj") (y #t)))

(define-public crate-enschin-0.1.1 (c (n "enschin") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4") (d #t) (k 0)) (d (n "enschin_proc") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "ndk-glue") (r "^0.5.0") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "wgpu") (r "^0.12") (d #t) (k 0)) (d (n "winit") (r "^0.26.1") (d #t) (k 0)))) (h "0zcri42svfc9ks3rcv7m1552xdwsqdc26vzd9myyi9hkpjhy43d6") (y #t)))

