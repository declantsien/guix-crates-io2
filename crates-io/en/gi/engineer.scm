(define-module (crates-io en gi engineer) #:use-module (crates-io))

(define-public crate-engineer-0.1.0 (c (n "engineer") (v "0.1.0") (d (list (d (n "engineer_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0mgqlr6rl8grr1mir9k8l5aansvrf7zwr90pq6crarc13jvq3xas")))

(define-public crate-engineer-0.1.1 (c (n "engineer") (v "0.1.1") (d (list (d (n "engineer_derive") (r "^0.1.1") (d #t) (k 0)))) (h "0108ahc0w8ynvbhm3n2v9xv3dgc4lhvcsr48dl0mf1v6idg3jk1h")))

(define-public crate-engineer-0.1.2 (c (n "engineer") (v "0.1.2") (d (list (d (n "engineer_derive") (r "^0.1.2") (d #t) (k 0)))) (h "1h3cydrqczd8d3sy6bvzjjgzwlijiqcih66m5q1x0dbk1yqwyr9j")))

(define-public crate-engineer-0.1.3 (c (n "engineer") (v "0.1.3") (d (list (d (n "engineer_derive") (r "^0.1.3") (d #t) (k 0)))) (h "1gy0dd5wkn0rarnvdk8icnh0n8zd2ydbhldgqfs6gycgrfwx6kkv")))

(define-public crate-engineer-0.1.4 (c (n "engineer") (v "0.1.4") (d (list (d (n "engineer_derive") (r "^0.1.4") (d #t) (k 0)))) (h "0sygwir89pby77ikln6q5v2blivs02p7zj3qf5pqdrjvxf8240xl")))

(define-public crate-engineer-0.1.5 (c (n "engineer") (v "0.1.5") (d (list (d (n "engineer_derive") (r "^0.1.5") (d #t) (k 0)))) (h "1m1npknd1m6d7f8272ddx13qc3k0av9axxmp447vnxi86kd9zwb3")))

(define-public crate-engineer-0.1.6 (c (n "engineer") (v "0.1.6") (d (list (d (n "engineer_derive") (r "^0.1.6") (d #t) (k 0)))) (h "0b1z35bl8j2gk776zpi7zam0alag0bnsa596g0mw4i12cq05hgwh")))

