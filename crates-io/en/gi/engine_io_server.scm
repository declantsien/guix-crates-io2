(define-module (crates-io en gi engine_io_server) #:use-module (crates-io))

(define-public crate-engine_io_server-0.1.0 (c (n "engine_io_server") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.40") (d #t) (k 0)) (d (n "bytes") (r "^0.5.6") (d #t) (k 0)) (d (n "engine_io_parser") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.55") (d #t) (k 0)) (d (n "strum") (r "^0.18.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.18.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "sync"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ncik01lm2h2aqqdax958nqpak9fcr1vm0g1jpk4qkqialgxyzfk")))

