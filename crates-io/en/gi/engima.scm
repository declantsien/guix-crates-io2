(define-module (crates-io en gi engima) #:use-module (crates-io))

(define-public crate-Engima-0.1.1 (c (n "Engima") (v "0.1.1") (h "00rhkb84aam6r4vcbrng5nw5zpy0dn57dymympswizyifz89k5dl")))

(define-public crate-Engima-0.2.2 (c (n "Engima") (v "0.2.2") (h "135b9kvyr0lwc12zlza3hw5sk0lyz402ml8pmgysy0micpr3mfq1") (y #t)))

(define-public crate-Engima-0.2.3 (c (n "Engima") (v "0.2.3") (h "1130g03nhizql4lqp9kvj14yaml1sd0n3i7gplakbl41mgh9px97")))

