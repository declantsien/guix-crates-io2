(define-module (crates-io en gi engine_io_warp) #:use-module (crates-io))

(define-public crate-engine_io_warp-0.1.0 (c (n "engine_io_warp") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.40") (d #t) (k 0)) (d (n "bytes") (r "^0.5.6") (d #t) (k 0)) (d (n "engine_io_parser") (r "^0.2.0") (d #t) (k 0)) (d (n "engine_io_server") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.2") (f (quote ("websocket"))) (d #t) (k 0)))) (h "1fylmg34mfdjr5hssr83hfcm01y3zd7fyz625aywassmh9qk8h71")))

