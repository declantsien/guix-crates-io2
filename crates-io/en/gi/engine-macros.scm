(define-module (crates-io en gi engine-macros) #:use-module (crates-io))

(define-public crate-engine-macros-0.1.0 (c (n "engine-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "09blqjzlx4xz9050rwbv7rinb1xzsijpm6cgsglvg6ffi82f26gs")))

(define-public crate-engine-macros-0.2.0 (c (n "engine-macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "0rvqr4vpc2y72pv1djykagrzxx9ih3df6mvbdzyk6bgpk7safhxy")))

(define-public crate-engine-macros-0.3.0 (c (n "engine-macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "0gj1lmns4bhcpxm29l05f66q3jv9zljnzdngq7a6v71yk5c2givy")))

