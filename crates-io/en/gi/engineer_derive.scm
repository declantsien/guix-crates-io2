(define-module (crates-io en gi engineer_derive) #:use-module (crates-io))

(define-public crate-engineer_derive-0.1.0 (c (n "engineer_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17k82j28czjd8ya2jc54fxrj4cg58fjlf0vvc1nrh0fqzys43314")))

(define-public crate-engineer_derive-0.1.1 (c (n "engineer_derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1sybmdnfzq1bdz2pk4xq468yc3pxj9r0lp21qqrwjdmwfs07xi2d")))

(define-public crate-engineer_derive-0.1.2 (c (n "engineer_derive") (v "0.1.2") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1am6hcfpi533pn6p4yn62jrvwv1991abidbc6gd4dgqpf9f7j983")))

(define-public crate-engineer_derive-0.1.3 (c (n "engineer_derive") (v "0.1.3") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1w4280v81ws155zyqyjaznxik347wkdns57f34gvm0zmkqhsvgfn")))

(define-public crate-engineer_derive-0.1.4 (c (n "engineer_derive") (v "0.1.4") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1fsbyvk1503y040rq27m2yp73rhr2jipq80x1sq9ry9vwnksk733")))

(define-public crate-engineer_derive-0.1.5 (c (n "engineer_derive") (v "0.1.5") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0yh5qfy95mxvag5v3cm0phzvikgm4a74f4fzgsd10vvyayzdrsxp")))

(define-public crate-engineer_derive-0.1.6 (c (n "engineer_derive") (v "0.1.6") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nvvdydwy2nb6n618dv3nzahr3vk89mbz1p3933zz799w7l89c23")))

