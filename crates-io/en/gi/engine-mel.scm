(define-module (crates-io en gi engine-mel) #:use-module (crates-io))

(define-public crate-engine-mel-0.6.0 (c (n "engine-mel") (v "0.6.0") (d (list (d (n "melodium-core") (r "^0.6.0") (d #t) (k 0)) (d (n "melodium-macro") (r "^0.6.0") (d #t) (k 0)))) (h "0w45cz1b0zk2giiz6dfyiihh2n7mfwc0f8an1z3p1gxkr5iyk2al") (r "1.60")))

(define-public crate-engine-mel-0.7.0-rc1 (c (n "engine-mel") (v "0.7.0-rc1") (d (list (d (n "melodium-core") (r "=0.7.0-rc1") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.7.0-rc1") (d #t) (k 0)))) (h "0p611vaby1l03hfrbk170bfmf8czgkwwk340b55zwj0mlzcah15z") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-engine-mel-0.7.0 (c (n "engine-mel") (v "0.7.0") (d (list (d (n "melodium-core") (r "=0.7.0") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.7.0") (d #t) (k 0)))) (h "1l0gh98rb0hc2rma0h358r07sij3jh5k9f6p8p63n1yi17srs5a6") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-engine-mel-0.7.1 (c (n "engine-mel") (v "0.7.1") (d (list (d (n "melodium-core") (r "^0.7.1") (d #t) (k 0)) (d (n "melodium-macro") (r "^0.7.1") (d #t) (k 0)))) (h "1scshr3v42cfjhvf3riwsys91kkhji74sl4z1j6s5gmd3qj6r0fi") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

