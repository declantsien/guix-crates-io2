(define-module (crates-io en gi engish) #:use-module (crates-io))

(define-public crate-engish-0.1.0 (c (n "engish") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("serde1"))) (d #t) (k 1)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)))) (h "0ja5l203gkylddyvvnwrwc8d2wmbg5f8rxzbs5xrl083khx1j94q") (f (quote (("words") ("nouns") ("default" "words" "nouns"))))))

