(define-module (crates-io en gi engine_io_parser) #:use-module (crates-io))

(define-public crate-engine_io_parser-0.1.0 (c (n "engine_io_parser") (v "0.1.0") (d (list (d (n "base64") (r "^0.12.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)))) (h "1w9sx3lfvyjdkyncc9p8gdsjlg8w1g7gkhhv3fchs189rg26a8nj")))

(define-public crate-engine_io_parser-0.2.0 (c (n "engine_io_parser") (v "0.2.0") (d (list (d (n "base64") (r "^0.12.2") (d #t) (k 0)) (d (n "bytes") (r "^0.5.6") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5.1.2") (d #t) (k 0)))) (h "0g35np6ri4j6wh008ch338jr4m1b4i06ry6jr980wbdbp9w0zahl")))

