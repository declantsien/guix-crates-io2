(define-module (crates-io en tw entwine) #:use-module (crates-io))

(define-public crate-entwine-0.1.0 (c (n "entwine") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1gx5slsx4hgk0mx16az4q42rpm2vayw3yj21dmbw2iwx0diq8cfz") (f (quote (("std") ("nightly") ("extended_random_tests") ("default" "std"))))))

