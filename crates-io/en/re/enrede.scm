(define-module (crates-io en re enrede) #:use-module (crates-io))

(define-public crate-enrede-0.1.0 (c (n "enrede") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "bytemuck") (r "^1.16") (f (quote ("derive" "must_cast"))) (d #t) (k 0)))) (h "16zl846fwn60jk5lq3m0r72jyqama15a228gvrxq8hb7iq4l81r2")))

(define-public crate-enrede-0.1.1 (c (n "enrede") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "bytemuck") (r "^1.16") (f (quote ("derive" "must_cast"))) (d #t) (k 0)))) (h "0xkc3nqzlsxv396x6q4kj0nhafaj916hr03aa9ldjl1zjpzhn3xh") (f (quote (("std" "alloc") ("defaults" "std") ("alloc"))))))

