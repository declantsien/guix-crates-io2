(define-module (crates-io en se ensemble_derive) #:use-module (crates-io))

(define-public crate-ensemble_derive-0.0.0 (c (n "ensemble_derive") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 2)) (d (n "deluxe") (r "^0.5.0") (d #t) (k 0)) (d (n "pluralizer") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "0hgars8rj4ljas2hlw1hs0jfdm92cpaigcmw062cpfcmp6vhf21s")))

(define-public crate-ensemble_derive-0.0.1 (c (n "ensemble_derive") (v "0.0.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "deluxe") (r "^0.5.0") (d #t) (k 0)) (d (n "pluralizer") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "046xg6fznxrln0qkbs7ay4is6mb6svy84xn9zd54i5d1g6w67yq6") (f (quote (("json"))))))

(define-public crate-ensemble_derive-0.0.2 (c (n "ensemble_derive") (v "0.0.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "deluxe") (r "^0.5.0") (d #t) (k 0)) (d (n "pluralizer") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "08m1djd7ixm3rg997j4dajfw14g58imyb4zi5nd9z2myz8qavjx9") (f (quote (("json"))))))

(define-public crate-ensemble_derive-0.0.3 (c (n "ensemble_derive") (v "0.0.3") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "deluxe") (r "^0.5.0") (d #t) (k 0)) (d (n "pluralizer") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "10zx4mvrkgcf2q78gi78jmdi7iv743z27p2k2zl3nwr5y7rixa34") (f (quote (("json"))))))

(define-public crate-ensemble_derive-0.0.4 (c (n "ensemble_derive") (v "0.0.4") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "deluxe") (r "^0.5.0") (d #t) (k 0)) (d (n "pluralizer") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "1dr0z8xn2wp06rrbq5wjjh3np26y29mi1h4j0m03r22s3qmxdqqz") (f (quote (("json"))))))

