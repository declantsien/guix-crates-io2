(define-module (crates-io en se enser) #:use-module (crates-io))

(define-public crate-enser-0.1.0 (c (n "enser") (v "0.1.0") (d (list (d (n "enser_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9.16") (d #t) (k 2)))) (h "1mvb7bqcbnp9145ifakysp2pp391ivcy98bid9im1zyf27w7k418")))

(define-public crate-enser-0.1.1 (c (n "enser") (v "0.1.1") (d (list (d (n "enser_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9.16") (d #t) (k 2)))) (h "1cm4gfp3mpp4hh03nwkl4mmhfijnmb8xpd7714ycpiv6p6xyf7yg")))

(define-public crate-enser-0.1.2 (c (n "enser") (v "0.1.2") (d (list (d (n "enser_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9.16") (d #t) (k 2)))) (h "07wbaq5n0pfjiiz8va41m97ln6qwqh6sf84vmcls27mpy59s2rl3")))

(define-public crate-enser-0.1.3 (c (n "enser") (v "0.1.3") (d (list (d (n "enser_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9.16") (d #t) (k 2)))) (h "03n0wkx06b37kbk2vq9byicmv0qm027jgc0hkwh99h69kkvprlwx")))

(define-public crate-enser-0.1.4 (c (n "enser") (v "0.1.4") (d (list (d (n "enser_derive") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 2)))) (h "1lvr9xz2wq3njrckgfllqgj37fy7k5i5fc4d2q6409msn4wmgg1l")))

