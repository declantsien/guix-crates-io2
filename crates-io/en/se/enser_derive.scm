(define-module (crates-io en se enser_derive) #:use-module (crates-io))

(define-public crate-enser_derive-0.1.0 (c (n "enser_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.7.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1nwsgq1vv2v7va1iz7q31p0k8q2gzykzybyg2qaxlwx2793l278c")))

(define-public crate-enser_derive-0.1.1 (c (n "enser_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.7.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "006kmyzb066dd19ndbvcxi0mhkprv3b02f7hkb3grrpamzihmd32")))

(define-public crate-enser_derive-0.1.2 (c (n "enser_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.7.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1hxc94m2bpvx6093r8qra3zw56ny1wwj2623m1kdb9c5ipfrhrxg")))

(define-public crate-enser_derive-0.1.3 (c (n "enser_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.7.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1z85z7fa4gilcmpjf2pigks2xdvihzm73lx0r4082qmrly7hm89p")))

(define-public crate-enser_derive-0.1.4 (c (n "enser_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.8.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1cfy6qnadhz51a1mr9khr6i6hjvfazrhcbhqfzbrzy575rwzxy4w")))

