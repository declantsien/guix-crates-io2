(define-module (crates-io en et enet-rs) #:use-module (crates-io))

(define-public crate-enet-rs-0.1.0 (c (n "enet-rs") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)))) (h "1avqx6i38pkbjdw8wdi0cw97w8jnrjc2flakjnsrcw6vcqwhib9x")))

(define-public crate-enet-rs-1.3.17 (c (n "enet-rs") (v "1.3.17") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)))) (h "1m2s8x25jaml9drg8mi3rbw1da5iw36mxs4l7vwvy359j4cdrw65")))

(define-public crate-enet-rs-1.3.171 (c (n "enet-rs") (v "1.3.171") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)))) (h "1f9pmb78q4pnz3nnzcbn83992q8rmqxbh2na1wygrgf1y8qnfxm8")))

