(define-module (crates-io en et enet-cs-sys) #:use-module (crates-io))

(define-public crate-enet-cs-sys-0.1.0 (c (n "enet-cs-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "11sbrvcvmn93kyix09rfrkpybkbhs0f6c90wd3y8vddxrkp1smwh")))

(define-public crate-enet-cs-sys-0.1.1 (c (n "enet-cs-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "0iay76p6ny0dyns4gyppsmnvh1vd9c34fvkdjpir1nl1gy0hc9kn")))

