(define-module (crates-io en et enet) #:use-module (crates-io))

(define-public crate-enet-0.1.0 (c (n "enet") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "enet-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "1igggqi0b967sfyqlqmblfvbbzh3gsqz5q4xav283l5h2mx145w2")))

(define-public crate-enet-0.1.1 (c (n "enet") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "enet-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "0cj7w84pcrx0d91gpyih83q9gx2yfsscxbbrdiaqg0i6h250yj7h")))

(define-public crate-enet-0.2.0 (c (n "enet") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "enet-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)))) (h "0wq68pnqaff7xhfhzp4vvp03874wycv7k60qfrlhqnkvl4s5imbf")))

(define-public crate-enet-0.2.1 (c (n "enet") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "enet-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "03rhw81y5x32iha78gj4nhmvyr6miszqsjhxwy4ls8lj5mqiwg8b")))

(define-public crate-enet-0.2.2 (c (n "enet") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "enet-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "1mip6xcwb7zqybwafnnlcsknk61wmk1770q6fh6snhl8ixvq3x5y")))

(define-public crate-enet-0.2.3 (c (n "enet") (v "0.2.3") (d (list (d (n "enet-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)))) (h "1138anghqv575npnx0hrga0f9p3mrwsw6z070gg2m6rwa0gfsda8")))

(define-public crate-enet-0.2.4 (c (n "enet") (v "0.2.4") (d (list (d (n "enet-sys") (r "^0.2.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "1h6ip5q7wzk3iw4c1j2r3ymkjxj5cw9wb0hxxhma8052w185mi9q")))

(define-public crate-enet-0.3.0 (c (n "enet") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 2)) (d (n "enet-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1b115axkv8hw2gp8l9pakmm58qvq9dm46qz02m6ija6bvvmq8p5x")))

