(define-module (crates-io en et enet-sys) #:use-module (crates-io))

(define-public crate-enet-sys-0.1.0 (c (n "enet-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "1pyygqmdl40yc73irn8wdq0jw5x4wx6l1a8vas7v9q0d8mnizhr6")))

(define-public crate-enet-sys-0.1.1 (c (n "enet-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "0lmgp4m5knjzvnkwflbv1vxhfy0wamqk395lmj549qddi4jps0ad")))

(define-public crate-enet-sys-0.1.2 (c (n "enet-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "12kgix8cwzadr5hzvzbn0swqis305qjj41682b3vad2nf0ipk6qh")))

(define-public crate-enet-sys-0.1.3 (c (n "enet-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "unsafe_unions") (r "^0.0.2") (d #t) (k 0)))) (h "158cssysckzvaiq1jrivz0lh2hglqs383f68iwbwvqf4w6yx3ngb")))

(define-public crate-enet-sys-0.1.4 (c (n "enet-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "unsafe_unions") (r "^0.0.2") (d #t) (k 0)))) (h "1dqr8vc06x6jfj3xm14hn2drw3g2vyzpz51wzx1bilbbdjbr5ksa")))

(define-public crate-enet-sys-0.2.0 (c (n "enet-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.35.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.29") (d #t) (k 1)))) (h "1jsvqff5wnahy23s9zg8kfxp5lk58bg3lsq57qpm73yq34483fnk")))

(define-public crate-enet-sys-0.2.1 (c (n "enet-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.35.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.29") (d #t) (k 1)))) (h "0zvqqwj37yz2j3r7h2y749jjgdipfiafifz80klbyfqfnrg5v3k0")))

(define-public crate-enet-sys-0.2.2 (c (n "enet-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.35.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.29") (d #t) (k 1)))) (h "1fvsqlq3w5rk0zlm33f8bhwd5jqi3w15qimgjpvzfjnfkp87khy2") (l "enet")))

(define-public crate-enet-sys-0.2.3 (c (n "enet-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.35.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "0h5x25iibcm8zfijxmnvpb073dhljakkdvanbf64n371l3x7hn60") (l "enet")))

(define-public crate-enet-sys-0.2.4 (c (n "enet-sys") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "1c9b5i5imczl5bwj73l7qr564ydymnx5d9l7fi69slk9amzrrd43") (y #t) (l "enet")))

(define-public crate-enet-sys-1.0.0 (c (n "enet-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "1a5l437106dfid945z9xkhf4drn272swgkc0wfdkpaccks4m4diq") (l "enet")))

(define-public crate-enet-sys-1.0.1 (c (n "enet-sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "17d9jjam0zxw4072446j0v92gg6djqmmy8v0xxih7531ylrpag2m") (l "enet")))

(define-public crate-enet-sys-1.0.2 (c (n "enet-sys") (v "1.0.2") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "054rx1k1h82g6wiw8cs5wmcbdxk8amw4x0fv8xdgxcsycawy99dy") (y #t) (l "enet")))

(define-public crate-enet-sys-1.0.3 (c (n "enet-sys") (v "1.0.3") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "152n06d1qf6lv47bingl8byj4q6bv4jalwj9mahyw18sd08rvswx") (l "enet")))

(define-public crate-enet-sys-1.0.4 (c (n "enet-sys") (v "1.0.4") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (d #t) (k 1)))) (h "0xzc5sndvndlxjqr9dczjnl30f7x0mpqf9jys1rhm8jbndf0yn74") (l "enet")))

