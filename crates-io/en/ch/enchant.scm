(define-module (crates-io en ch enchant) #:use-module (crates-io))

(define-public crate-enchant-0.1.0 (c (n "enchant") (v "0.1.0") (d (list (d (n "enchant-sys") (r "^0.1") (d #t) (k 0)))) (h "1jwq019p7h37xqs810zg3lsk6gf36k30g1v3dwv494q4viygj3w1")))

(define-public crate-enchant-0.1.1 (c (n "enchant") (v "0.1.1") (d (list (d (n "enchant-sys") (r "^0.1") (d #t) (k 0)))) (h "0jrx6aaqbpnknfdvickscyw1pkpiylq1kis2g496xy1512kpa2xf")))

(define-public crate-enchant-0.2.0 (c (n "enchant") (v "0.2.0") (d (list (d (n "enchant-sys") (r "^0.2") (d #t) (k 0)))) (h "1j9p6kjvdzdi7q2v6i1i0fhjrprjkpxnq95z4dsxwyycjyc4i44c")))

(define-public crate-enchant-0.3.0 (c (n "enchant") (v "0.3.0") (d (list (d (n "enchant-sys") (r "^0.2") (d #t) (k 0)))) (h "1ns4fr9hn8xdpaf78lk94kr90n0j65f0k6nqz463p4zmky7h3zfs")))

