(define-module (crates-io en ch enchant-sys) #:use-module (crates-io))

(define-public crate-enchant-sys-0.1.0 (c (n "enchant-sys") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0cd9g3gbnbzzglsqg6k18pn2943cnz18999gr8pjkabgb8r4nd9w")))

(define-public crate-enchant-sys-0.2.0 (c (n "enchant-sys") (v "0.2.0") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0n140gbb00g496y49jx3bzhp2pfgzcxa8k3wi3gw5kgwxdh35p0f") (l "enchant")))

(define-public crate-enchant-sys-0.2.1 (c (n "enchant-sys") (v "0.2.1") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0kx3kfjzybrs5z5fan0bwqi46wsi430kvwlqamlig8nqlvvmkbwp") (l "enchant")))

