(define-module (crates-io en di endinero) #:use-module (crates-io))

(define-public crate-endinero-0.1.1 (c (n "endinero") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1c7q0c7zji58av8lmqz51g8lgpz9x8dd0vl2fdnrmh6pzz7g7425") (r "1.65")))

(define-public crate-endinero-0.1.2 (c (n "endinero") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1dahzir897j1y9pd1r1mddx8lk868fgq7lgm30f026i2rzyybpal") (r "1.65")))

(define-public crate-endinero-0.1.3 (c (n "endinero") (v "0.1.3") (h "1jl0c326jrrv2p77sy3qyxk4j1y9c07r8ikadn2886j92mgxw20l") (r "1.65")))

(define-public crate-endinero-0.1.4 (c (n "endinero") (v "0.1.4") (h "1sy7m6pgn8s9776fd9d34byj490k0p2jl0x6c4l11yy4s9zi9ykd") (r "1.65")))

(define-public crate-endinero-0.1.5 (c (n "endinero") (v "0.1.5") (h "0zqqwyjrqh5nciflaz269xmwb53kmwkggliph6vr4vlx9wgpryff") (r "1.65")))

(define-public crate-endinero-0.1.6 (c (n "endinero") (v "0.1.6") (h "01a53qzjksracqp92fg7bprfriidd8xnvzblag13r9wfl1kzw5l0") (r "1.65")))

(define-public crate-endinero-0.1.7 (c (n "endinero") (v "0.1.7") (h "168g9l4ya2nld9mp8snwj2m54gvs21jr3vniyi1wb8r05gx36h2w") (r "1.65")))

