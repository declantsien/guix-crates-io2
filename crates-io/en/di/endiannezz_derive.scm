(define-module (crates-io en di endiannezz_derive) #:use-module (crates-io))

(define-public crate-endiannezz_derive-0.1.0 (c (n "endiannezz_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1icjhcz5y9qvb4cchin8n3lw87v1c99nnlpjfi67h70k90cmzhyk")))

(define-public crate-endiannezz_derive-0.1.1 (c (n "endiannezz_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0r2rj6yb8c2fhaiwvbavd2yni5gd4scymfrp6pvwxgmjl9123wg6")))

(define-public crate-endiannezz_derive-0.2.0 (c (n "endiannezz_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1dd3kycyls9wxfmzqcqzr5m57z46dzy5pdblan3lkrrl9xmj54lk")))

(define-public crate-endiannezz_derive-0.2.1 (c (n "endiannezz_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0dj0bzf853sq1ck6lya38pv3vv4g56ilnkwg0aflxdiqx272wqck")))

(define-public crate-endiannezz_derive-0.2.2 (c (n "endiannezz_derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "147wmyzwnzglhwhfilmcislgx06r5fbr6d3rmryzngbnvdx85krq")))

