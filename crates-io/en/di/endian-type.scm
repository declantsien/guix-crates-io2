(define-module (crates-io en di endian-type) #:use-module (crates-io))

(define-public crate-endian-type-0.1.0 (c (n "endian-type") (v "0.1.0") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1k8wvgkywcf7s03anpyi7wbhdir10sv78ig6mppk743d5ih37vjg")))

(define-public crate-endian-type-0.1.1 (c (n "endian-type") (v "0.1.1") (h "027laz6mn2wzv5yvpnygkr2r6plh7gs9xf5wlpa080akb9ypclm4")))

(define-public crate-endian-type-0.1.2 (c (n "endian-type") (v "0.1.2") (h "0bbh88zaig1jfqrm7w3gx0pz81kw2jakk3055vbgapw3dmk08ky3")))

(define-public crate-endian-type-0.2.0 (c (n "endian-type") (v "0.2.0") (h "1wk235wxf0kqwlbjp3racbl55jwzmh52fg8cbjf1lr93vbdhm6w6")))

