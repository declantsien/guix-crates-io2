(define-module (crates-io en di endian_codec_derive) #:use-module (crates-io))

(define-public crate-endian_codec_derive-0.1.0 (c (n "endian_codec_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1f9hpy7adj21da3lpfpwylbfkw584m1blwxbbpj749qa1f5f8aab")))

(define-public crate-endian_codec_derive-0.1.1 (c (n "endian_codec_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ga1zj8q8p8j2j3ccs9jqc9m8yzcgknhkwz6fnf9l8ba1jw7i1qp")))

