(define-module (crates-io en di endian-type-rs) #:use-module (crates-io))

(define-public crate-endian-type-rs-0.1.3 (c (n "endian-type-rs") (v "0.1.3") (d (list (d (n "num") (r "^0.2.1") (o #t) (k 0)))) (h "1vyjvwl0v9nw1jw2c10wr08akr30jvjxc1y9r3znhg0mnj50x77g") (f (quote (("std") ("default" "std"))))))

(define-public crate-endian-type-rs-0.1.4 (c (n "endian-type-rs") (v "0.1.4") (d (list (d (n "num") (r "^0.2.1") (o #t) (k 0)))) (h "0xjwfn536jwpqnf7w6xy5w9an1j0w8zxnx01zswi2074fmf9lhdn") (f (quote (("std") ("default" "std"))))))

