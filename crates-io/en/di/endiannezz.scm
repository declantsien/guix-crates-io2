(define-module (crates-io en di endiannezz) #:use-module (crates-io))

(define-public crate-endiannezz-0.1.0 (c (n "endiannezz") (v "0.1.0") (h "0rpq59pvkkc815848s248hazds20q81f4m1xbx900542c7bsgik4")))

(define-public crate-endiannezz-0.1.1 (c (n "endiannezz") (v "0.1.1") (h "0kjjx9cp4lqfzrqp7zmgzn562r85ps0bg8l7i3l9pvjh55pwzis7")))

(define-public crate-endiannezz-0.1.2 (c (n "endiannezz") (v "0.1.2") (h "17hybn3mmb6zkc8vlky6ncs0sszarf51pxr5p9i0qhz4b5pijd77")))

(define-public crate-endiannezz-0.1.3 (c (n "endiannezz") (v "0.1.3") (h "0vmi5mlilryppp00vff2ggjb9m6n9ff6dhxsyis26zn6mfbnzfjk")))

(define-public crate-endiannezz-0.2.0 (c (n "endiannezz") (v "0.2.0") (h "1pgwj0fs2jx72mgix4bccj02g194zjm1d8c1vypwpg8g517alywn")))

(define-public crate-endiannezz-0.3.0 (c (n "endiannezz") (v "0.3.0") (h "1m7q72swcjw416vlsl5zdddirm1im7ihsj11j9zbz5j3iy7yirv3")))

(define-public crate-endiannezz-0.4.0 (c (n "endiannezz") (v "0.4.0") (d (list (d (n "endiannezz_derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1bvq6acqclmz36g399wivy581zsxkjy6j1pg97hl7g3dsmj3ylh2") (f (quote (("inline_primitives") ("inline_io") ("derive" "endiannezz_derive") ("default" "inline_primitives" "derive"))))))

(define-public crate-endiannezz-0.4.1 (c (n "endiannezz") (v "0.4.1") (d (list (d (n "endiannezz_derive") (r "=0.1.1") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1l9cl2vr0r64rqsz1sl3g85kwqakqdviwvvlyl0dick666wdqfs1") (f (quote (("inline_primitives") ("inline_io") ("derive" "endiannezz_derive") ("default" "inline_primitives" "derive"))))))

(define-public crate-endiannezz-0.4.2 (c (n "endiannezz") (v "0.4.2") (d (list (d (n "endiannezz_derive") (r "=0.1.1") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0azxm7y396jl57z0dkzwi59pf8i806l1d67g42q1nx5m14a4s96r") (f (quote (("inline_primitives") ("inline_io") ("derive" "endiannezz_derive") ("default" "inline_primitives" "derive"))))))

(define-public crate-endiannezz-0.5.0 (c (n "endiannezz") (v "0.5.0") (d (list (d (n "automod") (r "^0.2") (d #t) (k 2)) (d (n "endiannezz_derive") (r "=0.1.1") (o #t) (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1kjb8wwqkkrabzmyl2bywc2b7fc7ny0mgwyywkag7im1lprinn2l") (f (quote (("inline_primitives") ("inline_io") ("derive" "endiannezz_derive") ("default" "inline_primitives" "derive")))) (y #t)))

(define-public crate-endiannezz-0.5.1 (c (n "endiannezz") (v "0.5.1") (d (list (d (n "automod") (r "^0.2") (d #t) (k 2)) (d (n "endiannezz_derive") (r "=0.2.0") (o #t) (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "05bpjd0vry57h1x2icpd6gfmr0acjmabb14ipmfwxfwfna4rik9g") (f (quote (("inline_primitives") ("inline_io") ("derive" "endiannezz_derive") ("default" "inline_primitives" "derive"))))))

(define-public crate-endiannezz-0.5.2 (c (n "endiannezz") (v "0.5.2") (d (list (d (n "automod") (r "^0.2") (d #t) (k 2)) (d (n "endiannezz_derive") (r "=0.2.0") (o #t) (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1k8gxl3ghy81vj49w6pqs4z3q9fk5h9cjxq3fvk7kxzw3hh4yky4") (f (quote (("inline_primitives") ("inline_io") ("derive" "endiannezz_derive") ("default" "inline_primitives" "derive"))))))

(define-public crate-endiannezz-0.6.0 (c (n "endiannezz") (v "0.6.0") (d (list (d (n "automod") (r "^0.2") (d #t) (k 2)) (d (n "endiannezz_derive") (r "=0.2.0") (o #t) (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "115n2n62hsl0xfy6w3aab10i1nlz4rp767zfybyzv000rgw3yc38") (f (quote (("inline_primitives") ("inline_io") ("derive" "endiannezz_derive") ("default" "inline_primitives" "derive"))))))

(define-public crate-endiannezz-0.6.1 (c (n "endiannezz") (v "0.6.1") (d (list (d (n "automod") (r "^0.2") (d #t) (k 2)) (d (n "endiannezz_derive") (r "=0.2.0") (o #t) (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1zj5kyyvgicjzd9c36vdssp6iisbgzrd3m6xml46i5qms6n79q4x") (f (quote (("inline_primitives") ("inline_io") ("derive" "endiannezz_derive") ("default" "inline_primitives" "derive"))))))

(define-public crate-endiannezz-0.6.2 (c (n "endiannezz") (v "0.6.2") (d (list (d (n "automod") (r "^0.2") (d #t) (k 2)) (d (n "endiannezz_derive") (r "=0.2.0") (o #t) (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "062hjkjaww7accl67m5vzqmcs34fm2rlrnf3qgvnlpv4r0hy4w3i") (f (quote (("inline_primitives") ("inline_io") ("derive" "endiannezz_derive") ("default" "inline_primitives" "derive"))))))

(define-public crate-endiannezz-0.6.3 (c (n "endiannezz") (v "0.6.3") (d (list (d (n "automod") (r "^0.2") (d #t) (k 2)) (d (n "endiannezz_derive") (r "=0.2.1") (o #t) (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1rmcj2fd5w9qa2j2543j8m5b6fsiw842jvplv3q256l6861ff44a") (f (quote (("inline_primitives") ("inline_io") ("derive" "endiannezz_derive") ("default" "inline_primitives" "derive"))))))

(define-public crate-endiannezz-0.6.4 (c (n "endiannezz") (v "0.6.4") (d (list (d (n "automod") (r "^0.2") (d #t) (k 2)) (d (n "endiannezz_derive") (r "=0.2.1") (o #t) (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0lnh9d26gm1amys241zfjbyrb6fph3h2vnap8427xn20la77d3ls") (f (quote (("unchecked_bool") ("inline_primitives") ("inline_io") ("derive" "endiannezz_derive") ("default" "inline_primitives" "derive"))))))

(define-public crate-endiannezz-0.6.5 (c (n "endiannezz") (v "0.6.5") (d (list (d (n "automod") (r "^0.2") (d #t) (k 2)) (d (n "endiannezz_derive") (r "=0.2.2") (o #t) (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1q6jphzf32avbp4wlps8dq8zb5gk97n1vprcc489ad688k7v7xy4") (f (quote (("unchecked_bool") ("inline_primitives") ("inline_io") ("derive" "endiannezz_derive") ("default" "inline_primitives" "derive"))))))

