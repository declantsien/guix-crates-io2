(define-module (crates-io en di endi) #:use-module (crates-io))

(define-public crate-endi-1.0.0 (c (n "endi") (v "1.0.0") (h "0lgaif02yymh7rwpqlykrzdpp5nw3gffmid6rj3fppgypy28zg5j") (f (quote (("std") ("default" "std")))) (r "1.60")))

(define-public crate-endi-1.0.1 (c (n "endi") (v "1.0.1") (h "1bqsm5dqq62qxs7zv889svjrv1pdmxy08dmcm14i8li7xiadqnl3") (f (quote (("std") ("default" "std")))) (r "1.60")))

(define-public crate-endi-1.0.2 (c (n "endi") (v "1.0.2") (h "0s4nmysrlc6d99m1180n4x326jwfb9fl3mh6jww284h6fcyxkw21") (f (quote (("std") ("default" "std")))) (r "1.60")))

(define-public crate-endi-1.1.0 (c (n "endi") (v "1.1.0") (h "1gxp388g2zzbncp3rdn60wxkr49xbhhx94nl9p4a6c41w4ma7n53") (f (quote (("std") ("default" "std")))) (r "1.60")))

