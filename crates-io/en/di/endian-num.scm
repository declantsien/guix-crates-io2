(define-module (crates-io en di endian-num) #:use-module (crates-io))

(define-public crate-endian-num-0.0.0 (c (n "endian-num") (v "0.0.0") (h "03py29dgm7jb1nbvgk3i5qdrwaxvlynwdc0pkmjlnw5ydldc62h2")))

(define-public crate-endian-num-0.1.0 (c (n "endian-num") (v "0.1.0") (d (list (d (n "bitflags") (r "^2") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytemuck_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7") (o #t) (k 0)) (d (n "zerocopy-derive") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1f9la5lvgysgwjsqyg7rcha3mrzjm5rxmah3sxq22gqj8y0mdw8d") (f (quote (("linux-types")))) (s 2) (e (quote (("zerocopy" "dep:zerocopy" "dep:zerocopy-derive") ("bytemuck" "dep:bytemuck" "dep:bytemuck_derive") ("bitflags" "dep:bitflags"))))))

(define-public crate-endian-num-0.1.1 (c (n "endian-num") (v "0.1.1") (d (list (d (n "bitflags") (r "^2") (o #t) (d #t) (k 0)) (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "bytemuck_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7") (o #t) (k 0)) (d (n "zerocopy-derive") (r "^0.7") (o #t) (d #t) (k 0)))) (h "053drg9q1mi0ajc973lmg3cnymd49j4n99bgvnxi1wcl42xlgn0s") (f (quote (("linux-types")))) (s 2) (e (quote (("zerocopy" "dep:zerocopy" "dep:zerocopy-derive") ("bytemuck" "dep:bytemuck" "dep:bytemuck_derive") ("bitflags" "dep:bitflags"))))))

