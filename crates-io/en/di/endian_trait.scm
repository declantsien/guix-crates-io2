(define-module (crates-io en di endian_trait) #:use-module (crates-io))

(define-public crate-endian_trait-0.1.0 (c (n "endian_trait") (v "0.1.0") (d (list (d (n "endian_trait_derive") (r "^0.1") (d #t) (k 0)))) (h "02sjn9xlkipwr28w468xvfz7mkdmb9jffmalgbq69777y58l6b5a")))

(define-public crate-endian_trait-0.1.1 (c (n "endian_trait") (v "0.1.1") (d (list (d (n "endian_trait_derive") (r "^0.1") (d #t) (k 0)))) (h "08mj5qqkr9fk101cfm9g08pvrp4l93awjkqnympr8j1b4lv18gmv")))

(define-public crate-endian_trait-0.2.0 (c (n "endian_trait") (v "0.2.0") (h "1ljzahanr7nnxy89d0n5an94rn0g9dmx6x5sglw13n2qsm4dxavh")))

(define-public crate-endian_trait-0.3.0 (c (n "endian_trait") (v "0.3.0") (h "0xppjhizaqainw9srz5ci5jin5dm325pq7byylqjpggfm462gk8c") (f (quote (("e128") ("arrays"))))))

(define-public crate-endian_trait-0.4.0 (c (n "endian_trait") (v "0.4.0") (d (list (d (n "endian_trait_derive") (r "^0.4.0") (d #t) (k 0)))) (h "0nhqngcb12j6fjy85f3w30n3x3b3y5djg4nm8smivmd1lbrjyqiy") (f (quote (("e128") ("arrays"))))))

(define-public crate-endian_trait-0.5.0 (c (n "endian_trait") (v "0.5.0") (d (list (d (n "endian_trait_derive") (r "^0.5.0") (d #t) (k 0)))) (h "0kly05d1rwl2kvd6c3yn3h2xfckq19kjl4bilgg53gsdgc6r5p0z") (f (quote (("e128") ("arrays"))))))

(define-public crate-endian_trait-0.6.0 (c (n "endian_trait") (v "0.6.0") (d (list (d (n "endian_trait_derive") (r "^0.6.0") (d #t) (k 0)))) (h "0ipxg6n63hipxsh54dwdnv67jb5ripmnx114f3z5dnrk5nb49j3p") (f (quote (("arrays"))))))

