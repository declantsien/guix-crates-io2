(define-module (crates-io en di endian_trait_derive) #:use-module (crates-io))

(define-public crate-endian_trait_derive-0.1.0 (c (n "endian_trait_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "03v6f7cnivw8sn7q389gs3mw1w7809ir4gi0dc5i36mhrlciaf4r")))

(define-public crate-endian_trait_derive-0.1.1 (c (n "endian_trait_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "10a235ycj2cxy7a51kirpb7xsv6a7j7gdz6f76q455zkrqx1rlbq")))

(define-public crate-endian_trait_derive-0.2.0 (c (n "endian_trait_derive") (v "0.2.0") (d (list (d (n "endian_trait") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "15n16jmgkdqpi70vmfkpvyhg9vbc2fsmywq0p0jpalf3pm2r9ayj")))

(define-public crate-endian_trait_derive-0.3.0 (c (n "endian_trait_derive") (v "0.3.0") (d (list (d (n "endian_trait") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1gl1l3isfhms2bl43d6ck7nicgzr3fykkq0h6bw8xcx26qzr9dh0")))

(define-public crate-endian_trait_derive-0.3.1 (c (n "endian_trait_derive") (v "0.3.1") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l73l4niwbly6kazxmc015wj103fiw4l2djhm9gy96qjd1gj2phv")))

(define-public crate-endian_trait_derive-0.3.2 (c (n "endian_trait_derive") (v "0.3.2") (d (list (d (n "endian_trait") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("derive"))) (d #t) (k 0)))) (h "13h7zcxdn60y6n58jnwd1hs7kz3ic6yxj04v43ab4i82768wyhbj")))

(define-public crate-endian_trait_derive-0.4.0 (c (n "endian_trait_derive") (v "0.4.0") (d (list (d (n "endian_trait") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0v8d7qjb7s3nlc1jbq277j43hhqpywh4ydlymvndscgb77m4fibw")))

(define-public crate-endian_trait_derive-0.5.0 (c (n "endian_trait_derive") (v "0.5.0") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0pgw6h5b61fjy36hnmvxglyk8j41z85xd4fy0wb6077gdhlmm2j1")))

(define-public crate-endian_trait_derive-0.6.0 (c (n "endian_trait_derive") (v "0.6.0") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "180wnv93433q8ykdy7yzqg0ac77cdp840nfl4xqr0sqagrrgyfm0")))

