(define-module (crates-io en di endian_codec) #:use-module (crates-io))

(define-public crate-endian_codec-0.1.0 (c (n "endian_codec") (v "0.1.0") (d (list (d (n "endian_codec_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1mnhcy1w9d1xky94kgx8grzry0dw6ilamy427mhs29r4d6hc3sw4") (f (quote (("derive" "endian_codec_derive") ("default" "derive"))))))

(define-public crate-endian_codec-0.1.1 (c (n "endian_codec") (v "0.1.1") (d (list (d (n "endian_codec_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0wrdcd14217yksms8wqvdp1m1w5f48c88z3f40bhafvyzf8cikn6") (f (quote (("derive" "endian_codec_derive") ("default" "derive"))))))

