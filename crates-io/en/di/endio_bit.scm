(define-module (crates-io en di endio_bit) #:use-module (crates-io))

(define-public crate-endio_bit-0.1.0 (c (n "endio_bit") (v "0.1.0") (h "0ymzkcb1267z6xiaphm914q5f8jwarpr6jm4gc71mqcs4cdcm7yx")))

(define-public crate-endio_bit-0.2.0 (c (n "endio_bit") (v "0.2.0") (h "0igm25005kjdvhnq5wkd6lp3bpcppdapwkmw83myjh7vm2bdkma1")))

