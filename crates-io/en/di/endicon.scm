(define-module (crates-io en di endicon) #:use-module (crates-io))

(define-public crate-endicon-1.0.0 (c (n "endicon") (v "1.0.0") (d (list (d (n "codicon") (r "^1.0.0") (d #t) (k 0)))) (h "11wha96r30s9xv892ppqhdzizmwhcz6az95rikzwl3d04nmzss73")))

(define-public crate-endicon-1.1.0 (c (n "endicon") (v "1.1.0") (d (list (d (n "codicon") (r "^1.0.0") (d #t) (k 0)))) (h "1a5s8wignrvywllc9sylf37qsvcc905jiqp117adr8ayq33rd641")))

(define-public crate-endicon-1.2.0 (c (n "endicon") (v "1.2.0") (d (list (d (n "codicon") (r "^1.0.0") (d #t) (k 0)))) (h "1dfwzhl9inx9zbs7c1gaxysm65sqfw00ncs2dc66qr7a858nvwkc")))

(define-public crate-endicon-2.0.0 (c (n "endicon") (v "2.0.0") (d (list (d (n "codicon") (r "^2.0.0") (d #t) (k 0)))) (h "1923yb83j0n974g72dg1fl12y722qxn991ww125mch4ql7krda1r")))

(define-public crate-endicon-3.0.0 (c (n "endicon") (v "3.0.0") (d (list (d (n "codicon") (r "^3.0") (d #t) (k 0)))) (h "0pgmlfgd2lf60p91hv7crfmf65nnd2ixzk9mbgnsyy2nzvalrdwx")))

