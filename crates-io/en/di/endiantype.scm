(define-module (crates-io en di endiantype) #:use-module (crates-io))

(define-public crate-endiantype-0.1.0 (c (n "endiantype") (v "0.1.0") (h "1ckvdgsgpjayvpcnx7nvfqhv625l8vh9n61lb5ypw52pmr835yqw")))

(define-public crate-endiantype-0.1.1 (c (n "endiantype") (v "0.1.1") (h "1i71k0gvg38vhv2zaklgn7ph58ah3an1w0ccxp7ymlvm1f6q44kb")))

(define-public crate-endiantype-0.1.2 (c (n "endiantype") (v "0.1.2") (h "09mq2913qxjdlwwdif8zs3lakwd2166zacp4wr1flrhhzifj6wnn") (f (quote (("std") ("default" "std"))))))

(define-public crate-endiantype-0.1.3 (c (n "endiantype") (v "0.1.3") (h "0f0xq056yhbapxzjk931wkk68j9cnh1a32hqs7c4r518fjhwk7k1") (f (quote (("std") ("default" "std"))))))

