(define-module (crates-io en di endianrw) #:use-module (crates-io))

(define-public crate-endianrw-0.2.0 (c (n "endianrw") (v "0.2.0") (h "09cqy1d97a7awg5b1d33ilj2v8hq8b7fpwiaw4b4xz4wqrix17cd") (y #t)))

(define-public crate-endianrw-0.2.1 (c (n "endianrw") (v "0.2.1") (h "08z3zgcyb5hx3jp1fsw5l1738xy17y5628mm890qf557lp48bqxc")))

(define-public crate-endianrw-0.2.2 (c (n "endianrw") (v "0.2.2") (h "1d6nss8dfdr68izrmrw7h80cdwzjbyj4lg16hdbb76kg9vbi36ls")))

