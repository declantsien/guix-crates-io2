(define-module (crates-io en di endianness) #:use-module (crates-io))

(define-public crate-endianness-0.1.0 (c (n "endianness") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0z49f5yad46bhfhzfbgkcz00kqzawvjm1c63zh5gwpnf7z6cibiv")))

(define-public crate-endianness-0.2.0 (c (n "endianness") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0mmj7lm8c8wlqsy8f9iwys7d3q62aprn4cfkvkc58vbfl73dsdm4")))

