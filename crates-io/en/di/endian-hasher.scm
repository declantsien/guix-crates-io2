(define-module (crates-io en di endian-hasher) #:use-module (crates-io))

(define-public crate-endian-hasher-0.1.0 (c (n "endian-hasher") (v "0.1.0") (h "1zbn8fa3y0hvm31gdi1rm976a7x09cifzh9l1nf3v1ykyp6hjalz") (y #t)))

(define-public crate-endian-hasher-0.1.1 (c (n "endian-hasher") (v "0.1.1") (h "0pabkdasfng6fv22f9xm106la5mxj61kkxngrifz93019jrz7scm")))

