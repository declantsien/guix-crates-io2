(define-module (crates-io en vs envshim) #:use-module (crates-io))

(define-public crate-envshim-0.1.0 (c (n "envshim") (v "0.1.0") (d (list (d (n "regex") (r "^1.9.4") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "12nz5x714zmwyh8yv3cxcy9dpingsh2h43jh1zwkg25l913ijz7n")))

(define-public crate-envshim-0.2.0 (c (n "envshim") (v "0.2.0") (d (list (d (n "regex") (r "^1.9.4") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "165grh4q3fmdzbr4mj7wvq8l71fl7mndz6mpb5rf78cf81a4qxj2")))

