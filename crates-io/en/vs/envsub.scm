(define-module (crates-io en vs envsub) #:use-module (crates-io))

(define-public crate-envsub-0.1.2 (c (n "envsub") (v "0.1.2") (d (list (d (n "getopts") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "regex") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "1sbicsy14inysn0jmxn3fz7gawsm290dypifpsjn7fq2x9rs3kww")))

(define-public crate-envsub-0.1.3 (c (n "envsub") (v "0.1.3") (d (list (d (n "getopts") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "regex") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "01grkzd1ib4xi51mf877x97g9wrg7gmbljflvw9bkr45dqb5jyjl")))

