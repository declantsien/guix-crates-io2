(define-module (crates-io en vs envsubst) #:use-module (crates-io))

(define-public crate-envsubst-0.0.1 (c (n "envsubst") (v "0.0.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1lxrbjw08bhcvfznpbkhvln5d25vlbcz10n9rimxxf9ilcq3nmf8")))

(define-public crate-envsubst-0.1.0 (c (n "envsubst") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "046zgi0aazb5ayrjs9anpcb71a91ri1m3k4lhna2x45zkfg11fwn")))

(define-public crate-envsubst-0.1.1 (c (n "envsubst") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0sirv9k1jzh7f69yrw9vwkq3j2l16lcmldhkx9b39j1lsnl1kkqk")))

(define-public crate-envsubst-0.2.0 (c (n "envsubst") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qj1akgkvj3lj0ja9jg2dqczvz6xwhbw1p8ikic8zy39ix3rxgw7")))

(define-public crate-envsubst-0.2.1 (c (n "envsubst") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hr6ahqqprid0cv9x5361ni9ahzi4izgqpbiwlli4kb7xvv2jbyg")))

