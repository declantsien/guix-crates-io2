(define-module (crates-io en vr envr) #:use-module (crates-io))

(define-public crate-envr-0.1.0 (c (n "envr") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fv0704bxx4gqr8nkjc00mp2jl0xvvnc8gssmj93zb0pd74phrsg")))

(define-public crate-envr-0.1.2 (c (n "envr") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wv533dlzk0n9pcbinkkz92nhsplx73y24v9c4a8g0x35sb6wmrz")))

