(define-module (crates-io en vr envrc) #:use-module (crates-io))

(define-public crate-envrc-0.2.0 (c (n "envrc") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "0h2x7j04vb8mbkf5394g3abf3x85hacld72kxlmpbhj04m9yhnr4")))

(define-public crate-envrc-0.3.0 (c (n "envrc") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "0ss20pn8vr7r8j1cc14h3m5r359clckrjwy664s4dmx5lg42450p")))

(define-public crate-envrc-0.4.0 (c (n "envrc") (v "0.4.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "145kr4rfx5p1v5h260vcwicii8kwfb69hbr0wsfca38r2ijk19fz")))

(define-public crate-envrc-0.5.0 (c (n "envrc") (v "0.5.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "04skppxn1cliji54rm8frzj0pq8m1vmwlqfbniwdkas52mlm5glk")))

