(define-module (crates-io en vr envro) #:use-module (crates-io))

(define-public crate-envro-0.1.0 (c (n "envro") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1srj1q9x157ixlp4hjwm0m5r2vcqqvs5bxc86j3679jr2fsxii2y")))

(define-public crate-envro-0.2.0 (c (n "envro") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1xszf707hmn6ls7c98pvbjj2i0f8mipp04qyxkm10m15prqdb5c8")))

(define-public crate-envro-0.3.0 (c (n "envro") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "176b36xixv1zpy28licwsvwc67i7pn8lbm519d2avz9j1v9j6r15")))

