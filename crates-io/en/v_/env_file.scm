(define-module (crates-io en v_ env_file) #:use-module (crates-io))

(define-public crate-env_file-0.1.0 (c (n "env_file") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0fkrgkyflqwvx95gncxwklgzx48938livic09aa9jyb0jp6mfh35")))

(define-public crate-env_file-0.1.1 (c (n "env_file") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0hlffckj2by5zq572nhlc46bwlkl09whafni3cb105c3mdxv6apz")))

(define-public crate-env_file-0.1.2 (c (n "env_file") (v "0.1.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1pv2mssp6zcpniwj9br0zf648cci39l1k40j8qy0zyj4g986cxk8")))

(define-public crate-env_file-0.1.3 (c (n "env_file") (v "0.1.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0fj3h1rwspn5jghjhr7iy9jnallf75rbbiis5kq1srz9x1h4p3q6")))

