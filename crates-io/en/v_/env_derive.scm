(define-module (crates-io en v_ env_derive) #:use-module (crates-io))

(define-public crate-env_derive-0.1.0 (c (n "env_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1z7l3vlzs51s97acab9xr2xhw9ahxqrjjas9h5kl26idpd9izffi")))

