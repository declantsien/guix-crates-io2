(define-module (crates-io en v_ env_dir) #:use-module (crates-io))

(define-public crate-env_dir-0.0.7 (c (n "env_dir") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "06gq9sp888db628bhalz2227wv32dr49kzv9r27x0jv63qwkxyxv") (f (quote (("default"))))))

