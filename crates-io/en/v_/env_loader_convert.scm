(define-module (crates-io en v_ env_loader_convert) #:use-module (crates-io))

(define-public crate-env_loader_convert-0.1.0 (c (n "env_loader_convert") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "05zhxbscyngdl1lmq3c1jjqz3sn0r0nd4adipvqph3r5hr6hgrkx") (y #t)))

(define-public crate-env_loader_convert-0.2.0 (c (n "env_loader_convert") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1gx42hvws4s360nycdsw7xk092spajvf62mx7q1jfsii10d8jyww")))

(define-public crate-env_loader_convert-0.2.1 (c (n "env_loader_convert") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1sj5qhrjr09fim0483v5gb6z0szspkjka44fxsy84ryx7q16sqq4")))

