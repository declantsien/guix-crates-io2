(define-module (crates-io en v_ env_utils) #:use-module (crates-io))

(define-public crate-env_utils-0.1.0 (c (n "env_utils") (v "0.1.0") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "tokio") (r "^1.10.0") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "12zm5l9736m37qc1ykbr77vl8kbr6hzgxa8zg65vlzaw5qmj1ycz") (f (quote (("testing" "tokio") ("default")))) (y #t)))

(define-public crate-env_utils-0.1.1 (c (n "env_utils") (v "0.1.1") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("test-util" "macros"))) (o #t) (d #t) (k 0)))) (h "1kmh1v7i846kwhn9jk7hx8xl3dhyn21wpcjhzwba4gdbkzlrsbj2") (f (quote (("testing" "tokio") ("default")))) (y #t)))

(define-public crate-env_utils-0.2.0 (c (n "env_utils") (v "0.2.0") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("test-util" "macros"))) (o #t) (d #t) (k 0)))) (h "1s3icamsxyby034kdpcach59a3mdrkw35nbfw8xvrg3wjdlzsf8k") (f (quote (("testing" "tokio") ("default")))) (y #t)))

(define-public crate-env_utils-0.3.0 (c (n "env_utils") (v "0.3.0") (d (list (d (n "dotenvy") (r "^0.15.7") (o #t) (d #t) (k 0)) (d (n "mockall") (r "^0.12.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("test-util" "macros"))) (d #t) (k 2)))) (h "14r3dzjc6xlq5zbwdkb1r1d642mvrb8q39rfyl8q3xxplzvadzg1") (f (quote (("default" "dotenvy")))) (s 2) (e (quote (("dotenvy" "dep:dotenvy"))))))

