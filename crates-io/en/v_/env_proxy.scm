(define-module (crates-io en v_ env_proxy) #:use-module (crates-io))

(define-public crate-env_proxy-0.1.0 (c (n "env_proxy") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "1id79mb1vnrhlr6wjy08zc712jc7vzd61159ydg6yc1kwp10bzil")))

(define-public crate-env_proxy-0.1.1 (c (n "env_proxy") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "1dxz1ww67zlj4an64amzwh81sd8bc8q2bfnr13vg1y88ihhdk47i")))

(define-public crate-env_proxy-0.2.0 (c (n "env_proxy") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "1bfvv0xc0x77sr24wvcxwadsjh8122w9s6axd54l7hmaswpbzp7c")))

(define-public crate-env_proxy-0.3.0 (c (n "env_proxy") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "1y2s2555q1gs7918gzsvw9742fzwih4sm5jc7pyd7l3pj2cr1gni")))

(define-public crate-env_proxy-0.3.1 (c (n "env_proxy") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "1r28r7g2gma64qj94fbf092j7s46hpx5vps6jp4a9h6b5xb9h1vh")))

(define-public crate-env_proxy-0.4.0 (c (n "env_proxy") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0m6l1ail9d01l7najin817zd3s2ymr2h74d22n8scc9pzw3dlcc7")))

(define-public crate-env_proxy-0.4.1 (c (n "env_proxy") (v "0.4.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1qabqhgybx1jzh6dmpx7kssciw312i8aa6al7fj0d12k32z1jl1s")))

