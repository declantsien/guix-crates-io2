(define-module (crates-io en v_ env_plus) #:use-module (crates-io))

(define-public crate-env_plus-0.1.0 (c (n "env_plus") (v "0.1.0") (h "1hf1jqmkmwakq60595v4kh8z9id3h21kdc87vzc2qb21csm1351y")))

(define-public crate-env_plus-0.1.1 (c (n "env_plus") (v "0.1.1") (h "0sh0jdfdnldmxy64c63qww1x154q0aiglmnvigvgssly4ydfvjfx")))

(define-public crate-env_plus-0.1.2 (c (n "env_plus") (v "0.1.2") (h "1yaz5p184f2nh1x5k9njzjn9ga0dz614jakmvz2pxhva5l36j3bh")))

