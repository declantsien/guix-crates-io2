(define-module (crates-io en v_ env_logger_successor) #:use-module (crates-io))

(define-public crate-env_logger_successor-0.9.1 (c (n "env_logger_successor") (v "0.9.1") (d (list (d (n "atty") (r "^0.2.5") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "humantime") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "regex") (r "^1.0.3") (f (quote ("std" "perf"))) (o #t) (k 0)) (d (n "termcolor") (r "^1.1.1") (o #t) (d #t) (k 0)))) (h "192j29hrxwp4x033d1mnpf08g3998s4xlqq5zwc9g85ddvcxmggn") (f (quote (("localtime" "chrono") ("default" "termcolor" "atty" "humantime" "regex"))))))

