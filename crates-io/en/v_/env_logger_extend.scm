(define-module (crates-io en v_ env_logger_extend) #:use-module (crates-io))

(define-public crate-env_logger_extend-0.1.0 (c (n "env_logger_extend") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "job_scheduler") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "16yky0q5wfgjl8gmgp090m1wzhri2fzix5yny064qps8zw1lqc60")))

(define-public crate-env_logger_extend-0.1.1 (c (n "env_logger_extend") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "job_scheduler") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "0pdw3ljk7j8hrxjvpixb7mqx7hw8a52b9qib9kv5k46vvir4k829")))

(define-public crate-env_logger_extend-0.1.2 (c (n "env_logger_extend") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "job_scheduler_ng") (r "^2.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "0rikvg7qhwcx5sbdrflfgbsxadd6jy0arhaqrxy4r90q29hwzmf9")))

(define-public crate-env_logger_extend-0.1.3 (c (n "env_logger_extend") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "job_scheduler_ng") (r "^2.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "1l1hh64x448liw326d6hi93lyz47q6djj49f75w41yqv1y4430fv")))

