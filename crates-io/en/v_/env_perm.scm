(define-module (crates-io en v_ env_perm) #:use-module (crates-io))

(define-public crate-env_perm-0.1.0 (c (n "env_perm") (v "0.1.0") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)))) (h "0h4wdls5pgn3nz105bdr2ml16bcz7hn47iz4w3bx3a04340hhp9k")))

(define-public crate-env_perm-0.1.1 (c (n "env_perm") (v "0.1.1") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)))) (h "0psxfhggw6ww9h2rm6y6c3d9mbqxrfnnh2dfx9kqdgi1f0m537xg")))

(define-public crate-env_perm-0.1.2 (c (n "env_perm") (v "0.1.2") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)))) (h "1m9b2jxllp4mqxff1mw8gsfd8vmbqf38vmqfhfd742c58m0mg66y")))

(define-public crate-env_perm-0.1.3 (c (n "env_perm") (v "0.1.3") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)))) (h "1v37fm9q7jl69ikyb2diapsdyx236x9mdv7m94chlyilf4gvpbnl")))

