(define-module (crates-io en v_ env_applier) #:use-module (crates-io))

(define-public crate-env_applier-0.1.0 (c (n "env_applier") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0vbr2p74mzbd948arf3bvin2ngyf13qx1gq368hjfl66a79dp8cr")))

(define-public crate-env_applier-0.1.1 (c (n "env_applier") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "12d86z695gf7xn90jfy5436ql4bi3awm7hbmzxxvcj5sjspk6lfa")))

(define-public crate-env_applier-0.1.2 (c (n "env_applier") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "18g61fargz7r851vv82vq2hl4s9fj2nw62gyds79dzjqb8xcq7fz")))

