(define-module (crates-io en v_ env_logger_timezone_fmt) #:use-module (crates-io))

(define-public crate-env_logger_timezone_fmt-0.1.0 (c (n "env_logger_timezone_fmt") (v "0.1.0") (d (list (d (n "anstyle") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1krip9las5194bx48r16j6zsjynsl49b0hbsfvdmvmcsk26r35zp")))

(define-public crate-env_logger_timezone_fmt-0.1.1 (c (n "env_logger_timezone_fmt") (v "0.1.1") (d (list (d (n "anstyle") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1s7ywbnaw5vkszw0ahhsz2kjyv2zypmana832hlvwxhikjw4j4kz")))

