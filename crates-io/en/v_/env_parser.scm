(define-module (crates-io en v_ env_parser) #:use-module (crates-io))

(define-public crate-env_parser-0.1.0 (c (n "env_parser") (v "0.1.0") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 2)))) (h "00v3955kcd50q02zah49viadd1f4mfwx6ygn1r1d982ksf8ml9nq") (f (quote (("to_lazy_static"))))))

(define-public crate-env_parser-0.1.1 (c (n "env_parser") (v "0.1.1") (d (list (d (n "indoc") (r ">=1.0.0, <2.0.0") (d #t) (k 2)))) (h "1h74vgs8dkdagmmqvzb8mzlxab8symwqf8x1w5nzjd7mqlipb0c8") (f (quote (("to_lazy_static"))))))

(define-public crate-env_parser-0.1.2 (c (n "env_parser") (v "0.1.2") (d (list (d (n "indoc") (r ">=1.0.0, <2.0.0") (d #t) (k 2)))) (h "0qsc1y612pd9n2pi7hgkbf56yfmc2gsqy4mxb41hfzkw4dylkhcv") (f (quote (("to_lazy_static"))))))

(define-public crate-env_parser-0.1.3 (c (n "env_parser") (v "0.1.3") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 2)))) (h "0c9yrldsq5whcidc7vrhwbjib58rmrakqypxaajbfh63zp354zgx") (f (quote (("to_lazy_static"))))))

(define-public crate-env_parser-0.1.4 (c (n "env_parser") (v "0.1.4") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 2)))) (h "03nvidk84zqw98p4bgp9r8p3xr0s6343iffvyyaccjh7mga23j49") (f (quote (("to_lazy_static"))))))

(define-public crate-env_parser-0.1.5 (c (n "env_parser") (v "0.1.5") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 2)))) (h "1dmcr3as6zz91pxjdxnkmcsh6gbl8cjblglahn8nkwki2m8hmqjm") (f (quote (("to_lazy_static"))))))

(define-public crate-env_parser-0.1.6 (c (n "env_parser") (v "0.1.6") (d (list (d (n "indoc") (r "^1.0") (d #t) (k 2)))) (h "0k77k80mn92vs2ac7mdk2phfj1h0g99an2l2iyjfd4lfmd6z7wn8") (f (quote (("to_lazy_static"))))))

(define-public crate-env_parser-0.1.7 (c (n "env_parser") (v "0.1.7") (d (list (d (n "indoc") (r "^2.0.4") (d #t) (k 2)))) (h "1aa9pr5mv1hx13paajw8ycm6k4xh96jnipb64p8isnaps4pkf7bd") (f (quote (("to_lazy_static"))))))

(define-public crate-env_parser-0.1.8 (c (n "env_parser") (v "0.1.8") (d (list (d (n "indoc") (r "^2.0.4") (d #t) (k 2)))) (h "0djv3d70ibkcmrih1nrnx8aaz1w060ii0s5x4h4jc5a30446w5w8") (f (quote (("to_lazy_static"))))))

(define-public crate-env_parser-0.1.9 (c (n "env_parser") (v "0.1.9") (d (list (d (n "indoc") (r "^2.0.4") (d #t) (k 2)))) (h "1a64ciivkbh9457i319gz4mvzy00fs2rzj0xzc8v2laj5zpyysz7") (f (quote (("to_lazy_static"))))))

(define-public crate-env_parser-0.1.10 (c (n "env_parser") (v "0.1.10") (d (list (d (n "indoc") (r "^2.0.4") (d #t) (k 2)))) (h "14s2nhhkzi4j5r3brgbhr6a5hvqrbvdfykq6i7945p4vvaj20ymp") (f (quote (("to_lazy_static"))))))

