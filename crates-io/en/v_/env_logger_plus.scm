(define-module (crates-io en v_ env_logger_plus) #:use-module (crates-io))

(define-public crate-env_logger_plus-0.10.0 (c (n "env_logger_plus") (v "0.10.0") (d (list (d (n "humantime") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "is-terminal") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (f (quote ("std"))) (d #t) (k 0)) (d (n "regex") (r "^1.0.3") (f (quote ("std" "perf"))) (o #t) (k 0)) (d (n "termcolor") (r "^1.1.1") (o #t) (d #t) (k 0)))) (h "0qhca1n7hg4bavxibpijfm4amasmclq22jjzcwh67ks2sb542xx5") (f (quote (("default" "auto-color" "humantime" "regex")))) (s 2) (e (quote (("regex" "dep:regex") ("humantime" "dep:humantime") ("color" "dep:termcolor") ("auto-color" "dep:is-terminal" "color")))) (r "1.60.0")))

