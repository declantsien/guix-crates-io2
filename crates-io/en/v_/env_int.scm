(define-module (crates-io en v_ env_int) #:use-module (crates-io))

(define-public crate-env_int-0.1.0 (c (n "env_int") (v "0.1.0") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0xg9nld9aqvsb0xjhdaghga5s902i4rdxcs5vq85h119kmp0qxsn")))

(define-public crate-env_int-0.2.0 (c (n "env_int") (v "0.2.0") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "03hhnhvrfqv2ns1kizfwcm5clnmxly1g181h7hna2mpgc75p1x3b")))

