(define-module (crates-io en v_ env_proc) #:use-module (crates-io))

(define-public crate-env_proc-0.1.0 (c (n "env_proc") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.92") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.92") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.43") (f (quote ("full"))) (d #t) (k 0)))) (h "02cx03jdb285xk1q2pq7d8skc296xxcnfxmrqwgvy4p1lnz57vw8")))

