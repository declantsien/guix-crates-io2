(define-module (crates-io en v_ env_assert) #:use-module (crates-io))

(define-public crate-env_assert-0.1.0 (c (n "env_assert") (v "0.1.0") (h "1xmahx8v1p59p5s3ssnfqjw7hqmmjbjbpc1l8xri54mr590yd9nc")))

(define-public crate-env_assert-0.1.1 (c (n "env_assert") (v "0.1.1") (h "1wgjf12kgq9219l27rj767sr68xcp3bip8nv05x619gq11wy07jc")))

(define-public crate-env_assert-0.1.3 (c (n "env_assert") (v "0.1.3") (h "1vd74xqp4hbx7ymnl9kjf4ghpvsfzd1dh0z1ghy9rwaplk985dm9")))

(define-public crate-env_assert-0.1.4 (c (n "env_assert") (v "0.1.4") (h "1vqpg57f0z404nnvv1qljvmfzvq2qrdk2rb9d823czyhcy306h2s")))

(define-public crate-env_assert-0.1.5 (c (n "env_assert") (v "0.1.5") (h "0v9jw8wlyw3dwakkk6yq8jhwi4fhl9jyyy1llxdy10vjc5rlky64")))

