(define-module (crates-io en v_ env_struct) #:use-module (crates-io))

(define-public crate-env_struct-0.1.0 (c (n "env_struct") (v "0.1.0") (h "1p8xlh3djnmbn815ifi6sa047a1ngiwa9bxsm1vjiq16gs1hvyd9")))

(define-public crate-env_struct-0.1.1 (c (n "env_struct") (v "0.1.1") (h "0yydy2rpfyi9am99llh78z3ac9v443bglfl81ajj1sf0rc60s2sj")))

(define-public crate-env_struct-0.1.2 (c (n "env_struct") (v "0.1.2") (h "0zva5vnlkpldkh0b7v4577zkh5w2q5qn5jf8knmb8z7mpvi37jiq")))

(define-public crate-env_struct-0.1.3 (c (n "env_struct") (v "0.1.3") (h "09zy7shxl47nbmr329xys8vw98k3rd3dblgw5q1is1d891606ai6")))

(define-public crate-env_struct-0.1.4 (c (n "env_struct") (v "0.1.4") (d (list (d (n "log") (r "^0.4.19") (o #t) (d #t) (k 0)))) (h "14qb04z9h2yfgds5zfh4imhafqg048j885l6nmkr9y0zcyv1ipf7") (f (quote (("logging" "log"))))))

