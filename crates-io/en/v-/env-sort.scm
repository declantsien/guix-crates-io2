(define-module (crates-io en v- env-sort) #:use-module (crates-io))

(define-public crate-env-sort-0.0.0 (c (n "env-sort") (v "0.0.0") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "find-target") (r "^0.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "winreg") (r "^0.10.1") (d #t) (k 0)))) (h "1gp73im0n9vz6dz15hskgsrsshni3wf03n0wscwqv0qjvjq32m9l") (f (quote (("default"))))))

(define-public crate-env-sort-0.1.0 (c (n "env-sort") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "find-target") (r "^0.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "winreg") (r "^0.10.1") (d #t) (k 0)) (d (n "winres") (r "^0.1.12") (d #t) (k 0)))) (h "1gdz8mpzcsgxjkd3n3z7wz7iqxdipc9ski2i0fjh4xvn2irccbi0") (f (quote (("default"))))))

(define-public crate-env-sort-0.1.1 (c (n "env-sort") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "find-target") (r "^0.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "winreg") (r "^0.10.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "winres") (r "^0.1.12") (d #t) (t "cfg(windows)") (k 1)))) (h "19zncp9yz1h0wbakq9796pwff9m9yjac80wmafzmssaqlfdvzlis") (f (quote (("default"))))))

