(define-module (crates-io en v- env-variables) #:use-module (crates-io))

(define-public crate-env-variables-0.1.0 (c (n "env-variables") (v "0.1.0") (h "0wqffx9p1pdwqhxgsnc4x626pg7ly7934pdw4wdz5wdcbfpfgya4")))

(define-public crate-env-variables-0.1.1 (c (n "env-variables") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)) (d (n "url") (r "^1.5") (d #t) (k 0)))) (h "19md05692ij6zp6kkbwvpy40xhsgdnl8kkwpcj9y4znby8p0s5j2")))

