(define-module (crates-io en v- env-url) #:use-module (crates-io))

(define-public crate-env-url-1.0.0 (c (n "env-url") (v "1.0.0") (d (list (d (n "derive-env-url") (r "^1.0.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1zla62h4mafng7y8kn3di1rawdw4knary7smsghzpnzax40gfwql")))

(define-public crate-env-url-1.0.1 (c (n "env-url") (v "1.0.1") (d (list (d (n "derive-env-url") (r "^1.0.1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1qg5c9xk2xxw736bqcawhhdzyn928kpn8j3g374k6bq818rdimyd")))

(define-public crate-env-url-1.0.2 (c (n "env-url") (v "1.0.2") (d (list (d (n "ctor") (r "^0.1.21") (d #t) (k 2)) (d (n "derive-env-url") (r "^1.0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0pjwkm8fm2c6rkxy5z87jqv74cl4scpjv1gpd1y0xf28hm142qc4") (y #t)))

(define-public crate-env-url-1.0.3 (c (n "env-url") (v "1.0.3") (d (list (d (n "ctor") (r "^0.1.21") (d #t) (k 2)) (d (n "derive-env-url") (r "^1.0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0s2wxnsdq3diljjw6awb1gsmlr0p97ipdn78m5sraq4f05bnm07i")))

(define-public crate-env-url-1.0.4 (c (n "env-url") (v "1.0.4") (d (list (d (n "ctor") (r "^0.1.21") (d #t) (k 2)) (d (n "derive-env-url") (r "^1.0.4") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "168h731irjv300pjk0wi8kvvm4cqnnsdzdrrn7jfcwy55cnl4vhp")))

(define-public crate-env-url-2.0.0 (c (n "env-url") (v "2.0.0") (d (list (d (n "ctor") (r "^0.1.21") (d #t) (k 2)) (d (n "derive-env-url") (r "^2.0.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1xvf3n5ixaclfqj2cgwld5zmgxmswa1grliyd1kf29ab3wy7640b") (y #t)))

(define-public crate-env-url-2.0.1 (c (n "env-url") (v "2.0.1") (d (list (d (n "ctor") (r "^0.1.21") (d #t) (k 2)) (d (n "derive-env-url") (r "^2.0.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1i6cjp4ak1xjq2hq1hkcgixq8qlwk352076va78k1p0dwzh03ny5") (y #t)))

(define-public crate-env-url-2.0.2 (c (n "env-url") (v "2.0.2") (d (list (d (n "ctor") (r "^0.1.21") (d #t) (k 2)) (d (n "derive-env-url") (r "^2.0.2") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1c5gf0151h90mxc9msdv0mw1wkkrj70d6ypc0aj0s6ynmylr4yxl") (y #t)))

(define-public crate-env-url-2.0.3 (c (n "env-url") (v "2.0.3") (d (list (d (n "ctor") (r "^0.1.22") (d #t) (k 2)) (d (n "derive-env-url") (r "^2.0.3") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "03y58rg0zq8arqrxc3ibavxplmjypn3lkcnadryrsaw1b9y6qvgm")))

(define-public crate-env-url-2.0.4 (c (n "env-url") (v "2.0.4") (d (list (d (n "ctor") (r "^0.2.0") (d #t) (k 2)) (d (n "derive-env-url") (r "^2.0.4") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "017c8kbsjnadzb4q0n0xjwk0aanpld22ss0r4iq5pg5ayrqflspy")))

(define-public crate-env-url-2.1.0 (c (n "env-url") (v "2.1.0") (d (list (d (n "ctor") (r "^0.2.8") (d #t) (k 2)) (d (n "derive-env-url") (r "^2.1.0") (d #t) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "1w971wnvsj51pk4ssfbf7i64n5g2in5cs3sdwc8bfgcb6l3yfq0d")))

