(define-module (crates-io en v- env-settings-utils) #:use-module (crates-io))

(define-public crate-env-settings-utils-0.1.0 (c (n "env-settings-utils") (v "0.1.0") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "dotenvy") (r "^0.15") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "1hc89x5za0kh27idx14i1af2znbymgy1r9kf9yvgyb2vrnh78m0f")))

(define-public crate-env-settings-utils-0.1.1 (c (n "env-settings-utils") (v "0.1.1") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "dotenvy") (r "^0.15") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "09lavarw43f8g07fsjnpx77nirwdrxqzd239k4d3kk19ky13c7d0")))

(define-public crate-env-settings-utils-0.1.2 (c (n "env-settings-utils") (v "0.1.2") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "dotenvy") (r "^0.15") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "1fr0mslacnafcl88kfjwpmw1x60v8nwp5x3qrmjf5wps8fcak2ch")))

(define-public crate-env-settings-utils-0.1.3 (c (n "env-settings-utils") (v "0.1.3") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "dotenvy") (r "^0.15") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "1lbwf3k1x5mnhx054wc4ibinkcrjj88hmrh5x61x8q2m2mx0vih6")))

(define-public crate-env-settings-utils-0.1.4 (c (n "env-settings-utils") (v "0.1.4") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "dotenvy") (r "^0.15") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "1zgvlaakfcl96w6mw6bh5jn7yr8n0xp8j1gjnzivmwr0najj9awq")))

(define-public crate-env-settings-utils-0.1.5 (c (n "env-settings-utils") (v "0.1.5") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "dotenvy") (r "^0.15") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "13rd923dkliak9sxccn1ldwl6k62wr6wfv3w2lis2fqy76g673bg")))

(define-public crate-env-settings-utils-0.1.6 (c (n "env-settings-utils") (v "0.1.6") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "dotenvy") (r "^0.15") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "095q6s9vyg6aijxs011xj80x602hp8wz1j9nhpqbrchflparlp7l")))

(define-public crate-env-settings-utils-0.1.7 (c (n "env-settings-utils") (v "0.1.7") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "dotenvy") (r "^0.15") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "1v3n2cxbbnvgfd4ipa5c3bf23szsxam8r61jxqscpinsj9227dm8")))

(define-public crate-env-settings-utils-0.1.8 (c (n "env-settings-utils") (v "0.1.8") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "dotenvy") (r "^0.15") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "16rjd0g73gpcqqqn064lxm40w108y8mavij2b4i7j7h3jnd22syg") (y #t)))

(define-public crate-env-settings-utils-0.1.9 (c (n "env-settings-utils") (v "0.1.9") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "dotenvy") (r "^0.15") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "0ms7rxsqgcapj9lglj9dwma6jq0n5y64gvyvp11im1cva1qzxs89")))

(define-public crate-env-settings-utils-0.1.10 (c (n "env-settings-utils") (v "0.1.10") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "dotenvy") (r "^0.15") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "149xpjzx3651cpxjnip5m6b4zn2q8qx9p4l9kk5i956b79ni6779")))

(define-public crate-env-settings-utils-0.1.11 (c (n "env-settings-utils") (v "0.1.11") (d (list (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "dotenvy") (r "^0.15") (d #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1yz4vqrlfqlxzbz3b8iy5yv0daw5gnagj92hgjvgirfxfl8j16ir")))

