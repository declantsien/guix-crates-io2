(define-module (crates-io en v- env-config) #:use-module (crates-io))

(define-public crate-env-config-0.1.0 (c (n "env-config") (v "0.1.0") (d (list (d (n "bae") (r "^0.1.6") (d #t) (k 0)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "doc-writer") (r "^0.1.0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fi57bkn1qzw673pqqgaxd21x7xkmxnhnfqmlpnhi4im9gbdl0p9")))

(define-public crate-env-config-0.2.0 (c (n "env-config") (v "0.2.0") (d (list (d (n "bae") (r "^0.1.6") (d #t) (k 0)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "doc-writer") (r "^0.1.0") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0k5979lrjvhyh5ql3f6z79jr10z6xi6lzn9bv61zpz7nxf0f32gz")))

