(define-module (crates-io en v- env-file-reader) #:use-module (crates-io))

(define-public crate-env-file-reader-0.1.0 (c (n "env-file-reader") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "02g8s57j0azj4sz3591akz8ivqndgcawbn6ysmj2v1xa9vcyn5bz")))

(define-public crate-env-file-reader-0.2.0 (c (n "env-file-reader") (v "0.2.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1cc799x8m5dkjfqvn86iplbihcyfsf561zdl51xrdc42yz1v02yh")))

(define-public crate-env-file-reader-0.3.0 (c (n "env-file-reader") (v "0.3.0") (d (list (d (n "lalrpop") (r "^0.19.7") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.7") (d #t) (k 0)) (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "0a1pvgnpc5ggxdkbbs2id0rp1j7h719j38hqqgshf6pxq35nva50")))

