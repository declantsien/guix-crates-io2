(define-module (crates-io en v- env-test-util) #:use-module (crates-io))

(define-public crate-env-test-util-0.1.0 (c (n "env-test-util") (v "0.1.0") (h "08z74pxy4267p61bva51y4vshzzn2pgnh3w77yxr02lbsr9q61ll")))

(define-public crate-env-test-util-1.0.0 (c (n "env-test-util") (v "1.0.0") (h "1psqywpcgqxr183y8nvr9kc902ax9zhbn7avc4c5k79spwj4ds26")))

(define-public crate-env-test-util-1.0.1 (c (n "env-test-util") (v "1.0.1") (h "0afyqjgzg52ix7bms3059lk9cgw73ma84zydphnycxzpmwpip9k2")))

