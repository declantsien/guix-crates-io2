(define-module (crates-io en v- env-inventory) #:use-module (crates-io))

(define-public crate-env-inventory-0.1.0 (c (n "env-inventory") (v "0.1.0") (d (list (d (n "inventory") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0df3cihjmvswf6iah7a4qhad8243whnrlzvzjv4zhpd7plh5gnpn") (y #t) (r "1.61")))

(define-public crate-env-inventory-0.1.1 (c (n "env-inventory") (v "0.1.1") (d (list (d (n "inventory") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1rq2aiyxz752ixy12lhmdgh3im7j1lv88nrj685mpcr4wky55lap") (y #t) (r "1.61")))

(define-public crate-env-inventory-0.2.0 (c (n "env-inventory") (v "0.2.0") (d (list (d (n "inventory") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0s12yw9ia4ighcydfihrnahk8jrv1srka9ssi2c7damvvc9lkxhs") (y #t) (r "1.61")))

(define-public crate-env-inventory-0.2.1 (c (n "env-inventory") (v "0.2.1") (d (list (d (n "inventory") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "160avnb945433vnvxymmkxcpdh3df5cq7q2s139r11gz4i489kb7") (r "1.61")))

(define-public crate-env-inventory-0.2.2 (c (n "env-inventory") (v "0.2.2") (d (list (d (n "inventory") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0bmmnlzq5k9xi7533xn6fj5knk5qfgc2yglaqk6pyrhsv8smjvda") (r "1.61")))

(define-public crate-env-inventory-0.2.3 (c (n "env-inventory") (v "0.2.3") (d (list (d (n "inventory") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "11rwb1v7ryjmr0b3088qzx89glhackf4brl9i01gpkcricv7a61b") (r "1.61")))

