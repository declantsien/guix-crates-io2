(define-module (crates-io en v- env-watcher) #:use-module (crates-io))

(define-public crate-env-watcher-0.1.0 (c (n "env-watcher") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "diff-struct") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.0.0") (d #t) (k 0)) (d (n "state") (r "^0.5.2") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1wimxhav131nhdj7a9c0z7m4nc9bh9cys8wrq4p9j4z7fkc5w32m") (f (quote (("derive" "state") ("default" "derive"))))))

