(define-module (crates-io en v- env-y) #:use-module (crates-io))

(define-public crate-env-y-0.1.0 (c (n "env-y") (v "0.1.0") (d (list (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "combine") (r "^4.6.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)))) (h "0s3nqkds3ccprlg60ca0842q470jw30gy9mrb2mv7nlg958niggf")))

