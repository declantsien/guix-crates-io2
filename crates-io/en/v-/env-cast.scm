(define-module (crates-io en v- env-cast) #:use-module (crates-io))

(define-public crate-env-cast-1.0.0 (c (n "env-cast") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "17pr6zrikn7xipac0kaajy2jkqdh4q8pxyy193a60h7jvy9whj3v")))

