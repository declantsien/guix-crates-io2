(define-module (crates-io en v- env-settings-derive) #:use-module (crates-io))

(define-public crate-env-settings-derive-0.1.0 (c (n "env-settings-derive") (v "0.1.0") (d (list (d (n "env-settings-utils") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "1ph7n2xh5cfl17vw0p5asjdgccqj8w66d5bb0xfjasy9p0p64hkv")))

(define-public crate-env-settings-derive-0.1.1 (c (n "env-settings-derive") (v "0.1.1") (d (list (d (n "env-settings-utils") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "07b8xgq332p20alp1dkkjjvgaffmjbwa99x2gnxlgvxxfydgac8b")))

(define-public crate-env-settings-derive-0.1.2 (c (n "env-settings-derive") (v "0.1.2") (d (list (d (n "env-settings-utils") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "19b87phs2y7vfgyb60zi7yv60nfzgipkbc8qfnzbyd040qp146ra")))

(define-public crate-env-settings-derive-0.1.3 (c (n "env-settings-derive") (v "0.1.3") (d (list (d (n "env-settings-utils") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "1aqniqwlmk1bzdid73vbwy35rcxcf5pi7vb4bray6rfc4k0arxaf")))

(define-public crate-env-settings-derive-0.1.4 (c (n "env-settings-derive") (v "0.1.4") (d (list (d (n "env-settings-utils") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "1j606pwy8p140fbcp3ql1z2krv1rdqsadymk67nvxiywmqb7ijnj")))

(define-public crate-env-settings-derive-0.1.5 (c (n "env-settings-derive") (v "0.1.5") (d (list (d (n "env-settings-utils") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "1v7xi7sfzdcf1ia2dyq32m2651wqgbbbn5hrpg2xfi39dj7mfjzq")))

(define-public crate-env-settings-derive-0.1.6 (c (n "env-settings-derive") (v "0.1.6") (d (list (d (n "env-settings-utils") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "1yxa98zsv1smmj2qly11z3avqr7hy4wdrnjmadhrk0bpb07v0rr4")))

(define-public crate-env-settings-derive-0.1.7 (c (n "env-settings-derive") (v "0.1.7") (d (list (d (n "env-settings-utils") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "1a93y0wzba8zgslhfknzabqyrhd809fxj6sis9ik3f4rmvci8sk7")))

(define-public crate-env-settings-derive-0.1.8 (c (n "env-settings-derive") (v "0.1.8") (d (list (d (n "env-settings-derive") (r "^0.1") (d #t) (k 0)) (d (n "env-settings-utils") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "07qv2sd84273a9597fnz4k8j2hp9vzi4xwh8hwfqi9d0iwha2xkz") (y #t)))

(define-public crate-env-settings-derive-0.1.9 (c (n "env-settings-derive") (v "0.1.9") (d (list (d (n "env-settings-utils") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "0i4alvsv12xyqvq3fhp7454jg6hmqbmdlv00dcq8yhgx69vb9la4")))

(define-public crate-env-settings-derive-0.1.10 (c (n "env-settings-derive") (v "0.1.10") (d (list (d (n "env-settings-utils") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "0pql4qmglmr1023wq4lrzrr23bg44ams80kfjz58wvysgxiky4cb")))

(define-public crate-env-settings-derive-0.1.11 (c (n "env-settings-derive") (v "0.1.11") (d (list (d (n "env-settings-utils") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 2)))) (h "00hflgc3cp5jl39vxqzs0xyy8d30vj8bd099w1vjfznl1y74aw36")))

