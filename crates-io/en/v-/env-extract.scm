(define-module (crates-io en v- env-extract) #:use-module (crates-io))

(define-public crate-env-extract-0.1.0 (c (n "env-extract") (v "0.1.0") (h "1i4af9fhq1jfivplmqki0xqhmmxxqwrrbnjh2cbzwykcsyl8zyp3")))

(define-public crate-env-extract-0.1.1 (c (n "env-extract") (v "0.1.1") (h "1i0kxzc7g3w2pvdydbdr1vr6ffhma1bf7d1hkc4ggcyk9hb0vidk")))

(define-public crate-env-extract-0.1.2 (c (n "env-extract") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1m5qkhmvhhmllpcwrmiwclm2sdfx7rcv225xsni6gy8lxannfyn8")))

(define-public crate-env-extract-0.1.21 (c (n "env-extract") (v "0.1.21") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0x24gwxpq2bhf609gbdhqv4mb3g4a749ck52qraks07cb9n5qm5q")))

(define-public crate-env-extract-0.1.22 (c (n "env-extract") (v "0.1.22") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "1spg50badsddd5w2bv121pmp3swx230x2d8l3yjn1r8545s86jvz")))

