(define-module (crates-io en v- env-var) #:use-module (crates-io))

(define-public crate-env-var-0.1.0 (c (n "env-var") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0xf2xpqs8lc7svb1zm7hq1rywajrkzvkfbminrlf3j5d9amd6g0r")))

(define-public crate-env-var-1.0.0 (c (n "env-var") (v "1.0.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "17128kqp01fcm9ary7nk8irn9ylvi5zc1jw1griv4dms7yd4qz8p")))

(define-public crate-env-var-1.0.1 (c (n "env-var") (v "1.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1xh4wr8d478bx9dgxw962pgk083r8dg8mi39vfzr9nw0p2qm7dfg")))

