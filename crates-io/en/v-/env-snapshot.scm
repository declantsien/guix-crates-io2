(define-module (crates-io en v- env-snapshot) #:use-module (crates-io))

(define-public crate-env-snapshot-0.1.0 (c (n "env-snapshot") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "flexbuffers") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "10z87s0v87rrvwzv90alg4hl2cs1jdby8pqhdcnv71k78aiifc7s")))

(define-public crate-env-snapshot-0.2.0 (c (n "env-snapshot") (v "0.2.0") (d (list (d (n "autojson") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "flexbuffers") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)))) (h "1nga5vjqh60svgqf4qj14l9wgmkacvl8ns98camwgbjkpmzfkxph")))

