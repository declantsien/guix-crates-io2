(define-module (crates-io en v- env-to-config-toml) #:use-module (crates-io))

(define-public crate-env-to-config-toml-0.1.0 (c (n "env-to-config-toml") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.2") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "env-file-reader") (r "^0.3.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "simple_logger") (r "^4.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "13cxx66dm082m1n98cn4kgc7zxdslxfkycjf3pqn3pwvbwlgmhml")))

