(define-module (crates-io en v- env-cli) #:use-module (crates-io))

(define-public crate-env-cli-1.0.0 (c (n "env-cli") (v "1.0.0") (d (list (d (n "arg") (r "^0.3") (d #t) (k 0)) (d (n "c-ffi") (r "^0.2") (k 0)))) (h "0x4v4q465vqgpvcgxjmzaqgw27cyrvhq50ic3vvq5qgs2z732740")))

(define-public crate-env-cli-1.0.1 (c (n "env-cli") (v "1.0.1") (d (list (d (n "c-ffi") (r "^0.2") (k 0)))) (h "11jbinc1hjv8z86xp7mn7h1axxbhjrqr70z3l9vb5kv66z1iszcy")))

(define-public crate-env-cli-1.0.2 (c (n "env-cli") (v "1.0.2") (d (list (d (n "c-ffi") (r "^0.4") (k 0)))) (h "1xmwc3whwcxqkr6dy5dw1qyz3cs2n71cpqyicirnbfg0z3bi7p37")))

