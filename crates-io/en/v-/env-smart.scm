(define-module (crates-io en v- env-smart) #:use-module (crates-io))

(define-public crate-env-smart-1.0.0-alpha.1 (c (n "env-smart") (v "1.0.0-alpha.1") (h "1zdxg6gzib9z5dqml3ma1l8217gqffd566786fjrbm2nx3b42zzq")))

(define-public crate-env-smart-1.0.0-alpha.2 (c (n "env-smart") (v "1.0.0-alpha.2") (h "08r75v4jn89p7y5wvjf1icyhssdf0yk1rbcikf1hi1m3qdn84amz")))

(define-public crate-env-smart-1.0.0 (c (n "env-smart") (v "1.0.0") (h "1l64123lsqy2hm07zaq88jn615l3pba4jxqqn7vwy1a1vl1r6kd6")))

(define-public crate-env-smart-1.0.1 (c (n "env-smart") (v "1.0.1") (h "0xsjgzdr1hxlvqgghvxrj5ixp9h9fxv3qihqpf7qc6iwcqfyx6z4")))

