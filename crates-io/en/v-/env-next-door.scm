(define-module (crates-io en v- env-next-door) #:use-module (crates-io))

(define-public crate-env-next-door-0.0.3 (c (n "env-next-door") (v "0.0.3") (d (list (d (n "dia-args") (r "^0.18") (d #t) (k 0)))) (h "0a5rkibkvigc40x8gm1r53qd6283azwxbbmhzz7601fza4aksydp")))

(define-public crate-env-next-door-0.1.0 (c (n "env-next-door") (v "0.1.0") (d (list (d (n "dia-args") (r "^0.19.1") (d #t) (k 0)))) (h "0569gixhi883wnx5b08nvg44j7bxiwrc3z2j8m7i4rwfl74p2jga")))

(define-public crate-env-next-door-0.2.0 (c (n "env-next-door") (v "0.2.0") (d (list (d (n "dia-args") (r "^0.23") (o #t) (d #t) (k 0)))) (h "05jbcdyvsndwgp4yzi1455i0sw4d4zplf4gwb69frwnkvdfmlad6") (f (quote (("bin" "dia-args"))))))

(define-public crate-env-next-door-0.3.0 (c (n "env-next-door") (v "0.3.0") (d (list (d (n "dia-args") (r "^0.24") (o #t) (d #t) (k 0)))) (h "00smjynyr3kjvsxbqgmv4dx4hb83awng6bn84x4mbpdvz6nqk6ag") (f (quote (("bin" "dia-args"))))))

(define-public crate-env-next-door-0.4.0 (c (n "env-next-door") (v "0.4.0") (d (list (d (n "dia-args") (r "^0.28") (o #t) (d #t) (k 0)))) (h "168alndpxclvvbpw869rrimz97mikcawyf4m481797pq8p71gpw4") (f (quote (("bin" "dia-args"))))))

(define-public crate-env-next-door-0.5.0 (c (n "env-next-door") (v "0.5.0") (d (list (d (n "dia-args") (r "^0.28") (o #t) (d #t) (k 0)))) (h "033h9809r27rsmdaz3b93l51i0mkfp12nsw03ahm5qch79w5naz2") (f (quote (("bin" "dia-args"))))))

(define-public crate-env-next-door-0.6.0 (c (n "env-next-door") (v "0.6.0") (d (list (d (n "dia-args") (r "^0.31") (o #t) (d #t) (k 0)))) (h "010hsglxjq7pmm7bk1a7sz5hj6abvfvykkdajxiyw969c6znqd3i") (f (quote (("bin" "dia-args"))))))

(define-public crate-env-next-door-0.6.1 (c (n "env-next-door") (v "0.6.1") (d (list (d (n "dia-args") (r "^0.31") (o #t) (d #t) (k 0)))) (h "1dl2815gplqf90kh270zbybz06s2abspg0win3j62zq8qz67dj0a") (f (quote (("bin" "dia-args"))))))

(define-public crate-env-next-door-0.7.0 (c (n "env-next-door") (v "0.7.0") (d (list (d (n "dia-args") (r "^0.46") (o #t) (d #t) (k 0)))) (h "0irj9vyf3km1ln3y8r2hxsl3vf1j45wasw2c2s044nbnq3y11ihb") (f (quote (("bin" "dia-args"))))))

(define-public crate-env-next-door-0.8.0 (c (n "env-next-door") (v "0.8.0") (d (list (d (n "dia-args") (r "^0.51") (o #t) (d #t) (k 0)))) (h "121byiyhshisqqkzid9ry3csk9ja4fhsn1i2wp46wmvxvkh5z2pl") (f (quote (("bin" "dia-args"))))))

(define-public crate-env-next-door-0.8.1 (c (n "env-next-door") (v "0.8.1") (d (list (d (n "dia-args") (r "^0.51") (o #t) (d #t) (k 0)))) (h "1jggzfw8k67azsqvf0lqy3dr3n1vgk83nd28siik9b3rprpyz8by") (f (quote (("bin" "dia-args"))))))

