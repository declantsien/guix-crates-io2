(define-module (crates-io en v- env-convert) #:use-module (crates-io))

(define-public crate-env-convert-0.1.0 (c (n "env-convert") (v "0.1.0") (h "1726v0yah0sh0a2i9r6zg48vqnbd9lwk97ic7w6jn43bflj9qvkp")))

(define-public crate-env-convert-0.1.1 (c (n "env-convert") (v "0.1.1") (h "198i13zgq8jp8fh15mllhhl7jlxc0zcmqh8w7817m13qmq86sbfz")))

