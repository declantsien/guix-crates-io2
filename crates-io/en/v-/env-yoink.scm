(define-module (crates-io en v- env-yoink) #:use-module (crates-io))

(define-public crate-env-yoink-0.1.0-beta.0 (c (n "env-yoink") (v "0.1.0-beta.0") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "grep") (r "^0.2.12") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1z5sqcczkl4y1dxrm6i6qza55b4la1ym1jaxvmqb2p6i4qwirw62")))

