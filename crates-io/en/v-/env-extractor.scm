(define-module (crates-io en v- env-extractor) #:use-module (crates-io))

(define-public crate-env-extractor-0.0.1 (c (n "env-extractor") (v "0.0.1") (h "07rja9dixkr5w34lwdjv2x2y9xvsh059qs89naxw1h26i6xklprx")))

(define-public crate-env-extractor-0.0.2 (c (n "env-extractor") (v "0.0.2") (h "0fcxpynsqjdvxhrgqjl52ksj3dkij068xyk6h6x37rmsc89kxkfq")))

(define-public crate-env-extractor-0.0.3 (c (n "env-extractor") (v "0.0.3") (h "1v35v1rj828xfpcnziv43j814wkc0iypsca12gv3an0lp9x5i8dv")))

(define-public crate-env-extractor-0.0.4 (c (n "env-extractor") (v "0.0.4") (h "0srai2mirizcis0b5p7bwxwlp6kkp0v0xzvwj9rm7g91q7wjdrry")))

(define-public crate-env-extractor-0.0.5 (c (n "env-extractor") (v "0.0.5") (h "1841cbw7z1c8kbvhq46wr9nqjsks0d7l669wp73hg0gklf7rw4c4")))

(define-public crate-env-extractor-0.0.6 (c (n "env-extractor") (v "0.0.6") (h "1w1yqnlhr3ni9jn07hfvrsxqqwap046s6lqgz2i8n2s50zxhlalh")))

(define-public crate-env-extractor-0.0.7 (c (n "env-extractor") (v "0.0.7") (h "0sb0943vb3m5l57h808v80vcp64ba4fxi0aaxb9yj07c99gg9laz")))

