(define-module (crates-io en v- env-loader) #:use-module (crates-io))

(define-public crate-env-loader-0.1.0 (c (n "env-loader") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)))) (h "0i30xxaan3ss8w7g4pylcapcs1qrkfrmqj3lyfy7vwhlq40lycgh") (y #t)))

(define-public crate-env-loader-0.2.0 (c (n "env-loader") (v "0.2.0") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "env_loader_convert") (r "^0.1.0") (d #t) (k 0)))) (h "1mn75ak2nr542j600ywq02b7b5w83r775fqy5ficq9kyyxpxii1i") (y #t)))

(define-public crate-env-loader-0.3.0 (c (n "env-loader") (v "0.3.0") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "env_loader_convert") (r "^0.2") (d #t) (k 0)))) (h "0vsaia2gadws7r74vxbs7f16q6s2ndc8sgplz9dbcq12m2mb3jl5") (y #t)))

(define-public crate-env-loader-0.3.1 (c (n "env-loader") (v "0.3.1") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "env_loader_convert") (r "^0.2") (d #t) (k 0)))) (h "1mma08mk2swsgx84k7blsny6d0vh1v9ydmfl78k96xanjs2aa1aw")))

(define-public crate-env-loader-0.3.2 (c (n "env-loader") (v "0.3.2") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "env_loader_convert") (r "^0.2.1") (d #t) (k 0)))) (h "1vaypigjqkbq6w3k9hvcpk8h2bjs290jw3hmsmh2gx653m88knzn")))

