(define-module (crates-io en v- env-libvpx-sys) #:use-module (crates-io))

(define-public crate-env-libvpx-sys-4.0.8 (c (n "env-libvpx-sys") (v "4.0.8") (d (list (d (n "bindgen") (r "^0.36.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)) (d (n "semver-parser") (r "^0.7.0") (d #t) (k 1)))) (h "0gq91q7l1lbgw7qj6fzjmfw1imbs8swv5cjn58qf9f0jn211xcc3") (f (quote (("generate" "bindgen") ("default")))) (l "vpx")))

(define-public crate-env-libvpx-sys-4.0.9 (c (n "env-libvpx-sys") (v "4.0.9") (d (list (d (n "bindgen") (r "^0.36.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)) (d (n "semver-parser") (r "^0.7.0") (d #t) (k 1)))) (h "1p8i1a6w1ga5p4blcf6p463inpq7mxllna633621fgvnxw8x8hlg") (f (quote (("generate" "bindgen") ("default")))) (l "vpx")))

(define-public crate-env-libvpx-sys-4.0.10 (c (n "env-libvpx-sys") (v "4.0.10") (d (list (d (n "bindgen") (r "^0.36.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)) (d (n "semver-parser") (r "^0.7.0") (d #t) (k 1)))) (h "113nyslk78r355kdbk1lqzk572zaaccjsc28c41nm4n9lx5l8msy") (f (quote (("generate" "bindgen") ("default")))) (l "vpx")))

(define-public crate-env-libvpx-sys-4.0.11 (c (n "env-libvpx-sys") (v "4.0.11") (d (list (d (n "bindgen") (r "^0.36.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)) (d (n "semver-parser") (r "^0.7.0") (d #t) (k 1)))) (h "1qwws5f8zlp8l1nazik2494xbm22mk13vv01w61zlqgkgycnxzdr") (f (quote (("generate" "bindgen") ("default")))) (l "vpx")))

(define-public crate-env-libvpx-sys-4.0.12 (c (n "env-libvpx-sys") (v "4.0.12") (d (list (d (n "bindgen") (r "^0.36.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)) (d (n "semver-parser") (r "^0.7.0") (d #t) (k 1)))) (h "131p8gsq1m9lvy6nvm6icd87yi14m3nkjlx6cb7iv6gpbzj1zbjv") (f (quote (("generate" "bindgen") ("default")))) (l "vpx")))

(define-public crate-env-libvpx-sys-4.0.13 (c (n "env-libvpx-sys") (v "4.0.13") (d (list (d (n "bindgen") (r "^0.36.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)) (d (n "semver-parser") (r "^0.7.0") (d #t) (k 1)))) (h "0l8f3l38q5xy058w22z6nmn4sn8pm5nq5mixaiab7ncs13iywx13") (f (quote (("generate" "bindgen") ("default")))) (l "vpx")))

(define-public crate-env-libvpx-sys-5.0.0 (c (n "env-libvpx-sys") (v "5.0.0") (d (list (d (n "bindgen") (r "^0.55.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)) (d (n "semver-parser") (r "^0.10.1") (d #t) (k 1)))) (h "0ihmzjq9ljwcj26bplc2ggi3rss55mg73b4zmf4n4q1jza6jic6p") (f (quote (("generate" "bindgen") ("default")))) (l "vpx")))

(define-public crate-env-libvpx-sys-5.1.0 (c (n "env-libvpx-sys") (v "5.1.0") (d (list (d (n "bindgen") (r "^0.55.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "0nk296jj2dbimp7wcsv3jmbd95ivy86q7q36x9bfz3w6bv41md0r") (f (quote (("generate" "bindgen") ("default")))) (l "vpx")))

(define-public crate-env-libvpx-sys-5.1.1 (c (n "env-libvpx-sys") (v "5.1.1") (d (list (d (n "bindgen") (r "^0.59.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "1vrlc7y4skbr6gp5hmh5bkjmw991mxxrkc4ij6gw085wmfxzfbcp") (f (quote (("generate" "bindgen") ("default")))) (l "vpx")))

(define-public crate-env-libvpx-sys-5.1.2 (c (n "env-libvpx-sys") (v "5.1.2") (d (list (d (n "bindgen") (r "^0.60.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "0c6jhgazl2w4mhydwgd32pjs6c2q7h2k66mwipggdnsd0n6wjgyb") (f (quote (("generate" "bindgen") ("default")))) (l "vpx")))

(define-public crate-env-libvpx-sys-5.1.3 (c (n "env-libvpx-sys") (v "5.1.3") (d (list (d (n "bindgen") (r "^0.68.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "0iq1f9qpvwkwkypbv6zxldkak2680gbsj6l2rh338002d9ixrv16") (f (quote (("generate" "bindgen") ("default")))) (l "vpx")))

