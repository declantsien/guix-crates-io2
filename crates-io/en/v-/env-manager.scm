(define-module (crates-io en v- env-manager) #:use-module (crates-io))

(define-public crate-env-manager-0.0.1 (c (n "env-manager") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pza7i5xmd1jkzm15ppipi8w48jlsilr9zcyg9r0ivwlxclypikx")))

