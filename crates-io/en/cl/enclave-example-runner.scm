(define-module (crates-io en cl enclave-example-runner) #:use-module (crates-io))

(define-public crate-enclave-example-runner-0.1.0 (c (n "enclave-example-runner") (v "0.1.0") (d (list (d (n "enclave-interface") (r "^0.1.0") (d #t) (k 0)) (d (n "sgx-isa") (r "^0.1.0") (d #t) (k 0)) (d (n "sgxs") (r "^0.4.0") (d #t) (k 0)))) (h "0wixr3qjc0scackqmkbz0wj31sc91mahxqhxn061mzd224kgc0jx")))

(define-public crate-enclave-example-runner-0.1.1 (c (n "enclave-example-runner") (v "0.1.1") (d (list (d (n "enclave-interface") (r "^0.1.1") (d #t) (k 0)) (d (n "sgx-isa") (r "^0.1.0") (d #t) (k 0)) (d (n "sgxs") (r "^0.5.0") (d #t) (k 0)))) (h "0p3wcbfb40apkgqs3jjff7hf5bzhnq1bys0qvk7b2nzrsb67995p")))

