(define-module (crates-io en cl enclose) #:use-module (crates-io))

(define-public crate-enclose-0.1.0 (c (n "enclose") (v "0.1.0") (h "0hy90vwpgxyn7jl8ak61w2v6hfaxih0lacf0p2hn87gcc7sffbap")))

(define-public crate-enclose-0.1.1 (c (n "enclose") (v "0.1.1") (h "1ib3lyj26n38i6gvjh5i4rf35akhdg0h91i9mq5gw2l210m9bw2g")))

(define-public crate-enclose-0.1.2 (c (n "enclose") (v "0.1.2") (h "1xn83al7yf0vxw35ly8g2wkrlycwx43z9np4kwh78hpacgm2gsny")))

(define-public crate-enclose-0.1.3 (c (n "enclose") (v "0.1.3") (h "1nia1nd8shyqm2cnb1bccqfylpdfdq8v070hjqj6dfmnhv5fy4hh")))

(define-public crate-enclose-0.1.4 (c (n "enclose") (v "0.1.4") (h "1z9q3nq1wilwxfrv278xvx72fa8gfgz3zlhaknya6mc9q9128hsy")))

(define-public crate-enclose-1.1.6 (c (n "enclose") (v "1.1.6") (h "0z4wn1rmnm9471v106wcjfapvzi1sifv56n94zj6gkvxaj02nxiv")))

(define-public crate-enclose-1.1.7 (c (n "enclose") (v "1.1.7") (h "1cdwi5avixa66nc449n9y52axmq1v74vilwb3mz03y9cg0m1yi2r")))

(define-public crate-enclose-1.1.8 (c (n "enclose") (v "1.1.8") (h "0mwk686pzbzabb8s52k4lyiy0qifnm4glbk6b819qvj2v99zamhh")))

(define-public crate-enclose-1.2.0 (c (n "enclose") (v "1.2.0") (h "16k3859qv6gaswag0rbk2d7nxqkkjpf2xwnqk80301280kwzdx5y")))

