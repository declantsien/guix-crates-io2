(define-module (crates-io en cl enclave-interface) #:use-module (crates-io))

(define-public crate-enclave-interface-0.1.0 (c (n "enclave-interface") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "sgx-isa") (r "^0.1.0") (d #t) (k 0)) (d (n "sgxs") (r "^0.4.0") (d #t) (k 0)))) (h "156nna8rarrb93n5bpcjgwk89bibh44g7hx57i2dp3ij9n7dxm0d")))

(define-public crate-enclave-interface-0.1.1 (c (n "enclave-interface") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.10") (d #t) (k 0)) (d (n "sgx-isa") (r "^0.1.0") (d #t) (k 0)) (d (n "sgxs") (r "^0.5.0") (d #t) (k 0)))) (h "01ph8pnprb1xmp31ksa4cvd31fcpnsfpr5xmz1n9g0xwg8cr7s9l")))

