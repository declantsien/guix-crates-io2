(define-module (crates-io en cl enclave-example) #:use-module (crates-io))

(define-public crate-enclave-example-0.1.0 (c (n "enclave-example") (v "0.1.0") (d (list (d (n "enclave") (r "= 0.1.0") (d #t) (k 0)) (d (n "enclave-interface") (r "= 0.1.0") (d #t) (k 0)) (d (n "sgx-isa") (r "^0.1.0") (d #t) (k 0)) (d (n "sgxs") (r "^0.4.0") (d #t) (k 0)))) (h "1gsgg3q5hl14sfqdx4b3xxyy4hz7kllz64lscl214ms4msqfyqzl") (f (quote (("enclave_debug_feature" "enclave/debug"))))))

(define-public crate-enclave-example-0.1.2 (c (n "enclave-example") (v "0.1.2") (d (list (d (n "enclave") (r "= 0.1.2") (d #t) (k 0)) (d (n "sgx-isa") (r "^0.1.0") (d #t) (k 0)))) (h "09m9i43kymwy21k515yyfyrqpc2k8a9i3d2xbmpwjgmmpag5pdrr") (f (quote (("enclave_debug" "enclave/debug"))))))

(define-public crate-enclave-example-0.1.3 (c (n "enclave-example") (v "0.1.3") (d (list (d (n "enclave") (r "= 0.1.3") (d #t) (k 0)) (d (n "sgx-isa") (r "^0.1.0") (d #t) (k 0)))) (h "1d323wjdcz83zrv19n1k4g7grpp3jkp8bxqr3841md27wji84lzs") (f (quote (("enclave_debug" "enclave/debug"))))))

(define-public crate-enclave-example-0.1.4 (c (n "enclave-example") (v "0.1.4") (d (list (d (n "enclave") (r "= 0.1.4") (d #t) (k 0)) (d (n "sgx-isa") (r "^0.1.0") (d #t) (k 0)))) (h "03wfsg5mimrswyjq3v3birrnmzh9prdz67rl72zgvp7w8wv5vmsg") (f (quote (("enclave_debug" "enclave/debug"))))))

