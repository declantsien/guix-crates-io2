(define-module (crates-io en st enstream) #:use-module (crates-io))

(define-public crate-enstream-0.1.0 (c (n "enstream") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3.21") (k 0)) (d (n "pin-project") (r "^1.0.10") (d #t) (k 0)) (d (n "pinned-aliasable") (r "^0.1.3") (d #t) (k 0)))) (h "03ngzbm8l8r6i7brwc15bs3g7lh7w7yxbmzxagwwrvwnjnqry5ki") (y #t)))

(define-public crate-enstream-0.2.0 (c (n "enstream") (v "0.2.0") (d (list (d (n "futures-util") (r "^0.3.21") (k 0)) (d (n "pin-project") (r "^1.0.10") (d #t) (k 0)) (d (n "pinned-aliasable") (r "^0.1.3") (d #t) (k 0)))) (h "1yb4q9a85014amj31apdhvqaxc7cvbjwvqb69a70g2fir9zj739b") (y #t)))

(define-public crate-enstream-0.2.1 (c (n "enstream") (v "0.2.1") (d (list (d (n "futures-util") (r "^0.3.21") (k 0)) (d (n "pin-project") (r "^1.0.10") (d #t) (k 0)) (d (n "pinned-aliasable") (r "^0.1.3") (d #t) (k 0)))) (h "0mq23j8zizx727qrb31l58b6q2v9s45gq1lqgj2dhdnc8s9pd500") (y #t)))

(define-public crate-enstream-0.3.0 (c (n "enstream") (v "0.3.0") (d (list (d (n "futures-core") (r "^0.3.21") (k 0)) (d (n "futures-util") (r "^0.3.21") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "pinned-aliasable") (r "^0.1.3") (d #t) (k 0)))) (h "07iib23da10x8mmv84lahncinw3hvvsgl7mkc5lvz1p4zdrjv7r7")))

