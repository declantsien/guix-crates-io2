(define-module (crates-io en am enamel) #:use-module (crates-io))

(define-public crate-enamel-0.1.0 (c (n "enamel") (v "0.1.0") (d (list (d (n "glium") (r "^0.13") (d #t) (k 0)) (d (n "glium_text") (r "^0.8") (d #t) (k 0)))) (h "0mspmzy70ckfdh0gcskph7sgyfxmj00qbnyrc506pzxgldfyvnhb")))

(define-public crate-enamel-0.2.0 (c (n "enamel") (v "0.2.0") (d (list (d (n "colorify") (r "^0.1") (d #t) (k 2)) (d (n "find_folder") (r "^0.3") (d #t) (k 0)) (d (n "glium") (r "^0.13") (d #t) (k 0)) (d (n "glium_text") (r "^0.8") (d #t) (k 0)))) (h "1bsris4fs5vn9sh4ydxdg0z7clllghh9d1z6gm4gbw939dq0fpv7")))

(define-public crate-enamel-0.3.0 (c (n "enamel") (v "0.3.0") (d (list (d (n "colorify") (r "^0.1") (d #t) (k 2)) (d (n "find_folder") (r "^0.3") (d #t) (k 0)) (d (n "glium") (r "^0.14") (d #t) (k 0)) (d (n "glium_text") (r "^0.9") (d #t) (k 0)))) (h "1js608rijrr7qaim5lwnlr1lsmgk2a2jqv56pr1nndansbn159lh")))

