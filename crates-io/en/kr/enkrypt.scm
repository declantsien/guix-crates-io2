(define-module (crates-io en kr enkrypt) #:use-module (crates-io))

(define-public crate-enkrypt-0.1.0 (c (n "enkrypt") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "0wj9xm9m0bkwry8cvjskk53h4kwybs8rpnfzzdnaa9rn8yb8vi0w")))

