(define-module (crates-io en al enalang_docgen) #:use-module (crates-io))

(define-public crate-enalang_docgen-0.5.2 (c (n "enalang_docgen") (v "0.5.2") (d (list (d (n "enalang_ir") (r "^0.5.2") (d #t) (k 0)) (d (n "flexstr") (r "^0.9.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "markdown") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2.1") (d #t) (k 0)))) (h "0gyzxqii38r2q5yf9ysw5c7cnmz8yynwja64ajd8xrr51jsyg2vx")))

(define-public crate-enalang_docgen-0.6.1 (c (n "enalang_docgen") (v "0.6.1") (d (list (d (n "enalang_ir") (r "^0.6.1") (d #t) (k 0)) (d (n "flexstr") (r "^0.9.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "markdown") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2.1") (d #t) (k 0)))) (h "1r53mw0ijbf12225l9anngqpkqj8ds4bi35ywwcp23w6pi8y4mmy")))

