(define-module (crates-io en al enalang_macro) #:use-module (crates-io))

(define-public crate-enalang_macro-0.6.1 (c (n "enalang_macro") (v "0.6.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "enalang_compiler") (r "^0.6.1") (d #t) (k 0)) (d (n "flexstr") (r "^0.9.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1d5x3kll1imldwig3zpnjl6n7y3mjin0dfy4lxki2jpf0c0mk0j7")))

