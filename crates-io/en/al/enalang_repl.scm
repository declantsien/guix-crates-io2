(define-module (crates-io en al enalang_repl) #:use-module (crates-io))

(define-public crate-enalang_repl-0.5.2 (c (n "enalang_repl") (v "0.5.2") (d (list (d (n "enalang_compiler") (r "^0.5.2") (d #t) (k 0)) (d (n "enalang_ir") (r "^0.5.2") (d #t) (k 0)) (d (n "enalang_vm") (r "^0.5.2") (d #t) (k 0)) (d (n "flexstr") (r "^0.9.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "15sklfhffanhs50b616nsjvbkc9w30jm75g34jx4ykh5dqnwix05")))

(define-public crate-enalang_repl-0.6.1 (c (n "enalang_repl") (v "0.6.1") (d (list (d (n "enalang_compiler") (r "^0.6.1") (d #t) (k 0)) (d (n "enalang_ir") (r "^0.6.1") (d #t) (k 0)) (d (n "enalang_vm") (r "^0.6.1") (d #t) (k 0)) (d (n "flexstr") (r "^0.9.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1srphgvmgrpav0ivs8dhzj0jz23rrbx6z97d0w0838dp2fx2f7bz")))

