(define-module (crates-io en al enalang_optimizer) #:use-module (crates-io))

(define-public crate-enalang_optimizer-0.3.3 (c (n "enalang_optimizer") (v "0.3.3") (h "1vynp960srjk0l84s9k2mv8cy2vd6hz5pk983bfpfpdyfpvbb4ac")))

(define-public crate-enalang_optimizer-0.3.4 (c (n "enalang_optimizer") (v "0.3.4") (h "0nzb5d3fd1aid1jhs81bka8yv4g0vmbavnvwlp3r584kzzd8riry")))

(define-public crate-enalang_optimizer-0.4.0 (c (n "enalang_optimizer") (v "0.4.0") (d (list (d (n "enalang_ir") (r "^0.4.0") (d #t) (k 0)) (d (n "enalang_vm") (r "^0.4.0") (d #t) (k 0)) (d (n "flexstr") (r "^0.9.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1cnrkzh9cfv17bn9bi78s8rq9fcjs2z6iln79n130sj32w6crbbz")))

(define-public crate-enalang_optimizer-0.5.0 (c (n "enalang_optimizer") (v "0.5.0") (d (list (d (n "enalang_ir") (r "^0.5.0") (d #t) (k 0)) (d (n "enalang_vm") (r "^0.5.0") (d #t) (k 0)) (d (n "flexstr") (r "^0.9.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1gpfgjzi6w5zcqb37rk3lrs69gqchljg17k1vapc0s0r7l5rnqkd") (y #t)))

(define-public crate-enalang_optimizer-0.5.2 (c (n "enalang_optimizer") (v "0.5.2") (d (list (d (n "enalang_ir") (r "^0.5.2") (d #t) (k 0)) (d (n "enalang_vm") (r "^0.5.2") (d #t) (k 0)) (d (n "flexstr") (r "^0.9.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0i5vmys1yljq0m2p084iirkxrq02pqx5qvzdn95l2zdd1y1jfchw")))

(define-public crate-enalang_optimizer-0.6.1 (c (n "enalang_optimizer") (v "0.6.1") (d (list (d (n "enalang_ir") (r "^0.6.1") (d #t) (k 0)) (d (n "enalang_vm") (r "^0.6.1") (d #t) (k 0)) (d (n "flexstr") (r "^0.9.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1dlwxc5q0r2nj5808d05wadiw6l8kk26z1cqqyzx1ym53awsnah4")))

