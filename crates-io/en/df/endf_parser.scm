(define-module (crates-io en df endf_parser) #:use-module (crates-io))

(define-public crate-endf_parser-0.1.0 (c (n "endf_parser") (v "0.1.0") (h "0davv56yn37ambghfqp8yp8r8b6mnzrrp46x5fqvxw2848hm652g")))

(define-public crate-endf_parser-0.1.1 (c (n "endf_parser") (v "0.1.1") (h "0iwwkrnlfjjn6aj62a1sbfj5jv911fl467pdydr2nvygsaz4s4mc")))

(define-public crate-endf_parser-0.2.0 (c (n "endf_parser") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 2)))) (h "1jnra8jzyvi768szj0qm9k26fs6vi79ha189g8zm50shcw91ki7y")))

