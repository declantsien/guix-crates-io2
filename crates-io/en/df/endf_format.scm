(define-module (crates-io en df endf_format) #:use-module (crates-io))

(define-public crate-endf_format-0.0.1 (c (n "endf_format") (v "0.0.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0jbjf16yl7h6xmn7yyzcln3440yiyayqplh2n822bjpz84sh0n9n")))

(define-public crate-endf_format-0.0.2 (c (n "endf_format") (v "0.0.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0r9sg1fngpzm7710sv8nknraz8v8xhy2lkag9bkvvyll91q326b9")))

(define-public crate-endf_format-0.0.3 (c (n "endf_format") (v "0.0.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0saizj869vqq8knd8y0g4nwykhzmdmci9zcmmqcq1imm4faczs0s")))

(define-public crate-endf_format-0.0.4 (c (n "endf_format") (v "0.0.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0fwpsz3q00l46svw1khgin4zs18nkx4jxsjgh12vn7j836ll11hq")))

