(define-module (crates-io en ar enarx-config) #:use-module (crates-io))

(define-public crate-enarx-config-0.1.0 (c (n "enarx-config") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.5.9") (k 2)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (k 0)))) (h "1iha488klnvk9s6cwncw36ynnqxbrsga8jhjlqlckjsy115gv7z2")))

(define-public crate-enarx-config-0.6.0 (c (n "enarx-config") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.5.9") (k 2)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (k 0)))) (h "0xx5nd8kx1m010p3lnf1v81kj5dpqjac3124c5614a8bh8bxizj3")))

(define-public crate-enarx-config-0.6.1 (c (n "enarx-config") (v "0.6.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.5.9") (k 2)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (k 0)))) (h "0rcky3wxx3qf37waiy6n26lwf7acy8p1apgbpica2afafqbd70mp")))

(define-public crate-enarx-config-0.6.2 (c (n "enarx-config") (v "0.6.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.5.9") (k 2)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (k 0)))) (h "1c04ajgnv9wralfcv8xy43qss86chshwpgrfyymfndmzspzd4796")))

(define-public crate-enarx-config-0.6.3 (c (n "enarx-config") (v "0.6.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.5.9") (k 2)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (k 0)))) (h "1wgcjci60qap3lyhcfl1ris6hdfzaci1r60ykrj2n3m8x1qmi84p")))

