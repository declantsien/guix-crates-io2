(define-module (crates-io en ar enarx-config-86d3ad9) #:use-module (crates-io))

(define-public crate-enarx-config-86d3ad9-0.6.0 (c (n "enarx-config-86d3ad9") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "toml") (r "^0.5.9") (k 2)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (k 0)))) (h "1fqsik1mnil3zvrixa6krxf99xrx7anzbfy49b9pni5jjixjk98p")))

