(define-module (crates-io en ce encedeus_js_runtime) #:use-module (crates-io))

(define-public crate-encedeus_js_runtime-0.1.0-alpha (c (n "encedeus_js_runtime") (v "0.1.0-alpha") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.23.6") (f (quote ("jpeg" "png"))) (o #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (o #t) (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "12vr1bgz8c3d9xmm4sa73rjwgrxdz75hj6q9k72mkkjd0ffj8lcm") (f (quote (("wasi_nn" "img") ("tensorflow" "img") ("img" "image" "imageproc") ("default") ("cjs"))))))

