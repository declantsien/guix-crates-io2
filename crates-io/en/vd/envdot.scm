(define-module (crates-io en vd envdot) #:use-module (crates-io))

(define-public crate-envdot-0.1.0 (c (n "envdot") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "1jjbjs1a9s7mabm19d9ggfxh88ym5gsnqk0ipwxwj1pivg6k0hsm")))

