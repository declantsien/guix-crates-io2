(define-module (crates-io en im enimda) #:use-module (crates-io))

(define-public crate-enimda-0.1.3 (c (n "enimda") (v "0.1.3") (d (list (d (n "image") (r "^0.10.4") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "03jjpmy9cbv9iimj3z9qs0mr7n06n29vks5xjsb45ra55y3npmhs")))

(define-public crate-enimda-0.1.4 (c (n "enimda") (v "0.1.4") (d (list (d (n "image") (r "^0.10.4") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "03h8swz31xdcxyp1d0rm9lrb75wdfrl6inrg81nhn2qhvpqhnw51")))

(define-public crate-enimda-0.1.5 (c (n "enimda") (v "0.1.5") (d (list (d (n "image") (r "^0.12.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0yn67ldxhb8wxm3njkdgp00bfdg6ksyx71jnf232zqkk0i2xcil6")))

(define-public crate-enimda-0.2.0 (c (n "enimda") (v "0.2.0") (d (list (d (n "gif") (r "^0.9.0") (d #t) (k 0)) (d (n "gif-dispose") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1jb7fvdnpsh1pcnwwflllw7736hd1bpmr9yfcyr7bqrbsb1p2kl6")))

(define-public crate-enimda-0.2.1 (c (n "enimda") (v "0.2.1") (d (list (d (n "gif") (r "^0.9.0") (d #t) (k 0)) (d (n "gif-dispose") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1651xqn9c580pmawc33s68yhz2pwx9cp586z2798z5vbqr359gpi")))

(define-public crate-enimda-0.3.0 (c (n "enimda") (v "0.3.0") (d (list (d (n "gif") (r "^0.9.0") (d #t) (k 0)) (d (n "gif-dispose") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "image-utils") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "02wc8bzvahz7kmbz9i9yd1rljffwa9d8959ws5s5axfk7wp6szdr") (y #t)))

(define-public crate-enimda-0.3.1 (c (n "enimda") (v "0.3.1") (d (list (d (n "gif") (r "^0.9.0") (d #t) (k 0)) (d (n "gif-dispose") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "image-utils") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0kg2l6rs011cwd4vxqhi7z598ahacc512nnb9x2x01fran7r2r0s")))

(define-public crate-enimda-0.3.2 (c (n "enimda") (v "0.3.2") (d (list (d (n "gif") (r "^0.9.0") (d #t) (k 0)) (d (n "gif-dispose") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "image-utils") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0ksj1dd5sfqzwdczzgx3qx78rfkihy28ik2byqj9sij7q9wyv718")))

(define-public crate-enimda-0.3.3 (c (n "enimda") (v "0.3.3") (d (list (d (n "gif") (r "^0.9.0") (d #t) (k 0)) (d (n "gif-dispose") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "image-utils") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1pnyc8ngk3mw6z605yzf09zvzhh4f4q8m1hpfydgn2fq0vk5vdhb")))

(define-public crate-enimda-0.3.4 (c (n "enimda") (v "0.3.4") (d (list (d (n "gif") (r "^0.9.0") (d #t) (k 0)) (d (n "gif-dispose") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "image-utils") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0pa3wfq5p2ajivfarysqar2hnzap209yi1qgvcx8l8byfld9vgql")))

(define-public crate-enimda-0.4.0 (c (n "enimda") (v "0.4.0") (d (list (d (n "gif") (r "^0.9.0") (d #t) (k 0)) (d (n "gif-dispose") (r "^1.0.1") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "image-utils") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "19mhmlv488yz8vc3vk2y92br6z36w0m838cx4rv127wvfzb88maj")))

(define-public crate-enimda-0.4.1 (c (n "enimda") (v "0.4.1") (d (list (d (n "gif") (r "^0.9.0") (d #t) (k 0)) (d (n "gif-dispose") (r "^1.0.1") (d #t) (k 0)) (d (n "image") (r "^0.12.3") (d #t) (k 0)) (d (n "image-utils") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1rz5rzs1nmks8g28536p3znpiwvmk3bzmgiayj684kp79c5vd66a")))

