(define-module (crates-io en c2 enc28j60) #:use-module (crates-io))

(define-public crate-enc28j60-0.1.0 (c (n "enc28j60") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.1") (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.1.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1r554wgi339qy43z5d27kn1ciann4644b0cmap3491ndicz16i9q")))

(define-public crate-enc28j60-0.2.0 (c (n "enc28j60") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.1") (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.0") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1gpwd8bs9w4kn8jwzidwi3pqar886480f0j1dkgmzpnvcyll8bxb")))

(define-public crate-enc28j60-0.2.1 (c (n "enc28j60") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.2.1") (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.0") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1q8684wwkcs91irhdchw9w8157w2sdz6003kill75lynqk2rvi28")))

