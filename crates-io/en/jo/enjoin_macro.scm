(define-module (crates-io en jo enjoin_macro) #:use-module (crates-io))

(define-public crate-enjoin_macro-0.1.0 (c (n "enjoin_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("visit" "visit-mut" "full"))) (d #t) (k 0)))) (h "1cl5jily0flmvsmp44xr7g047d04b7cw96y2aigc0ha52a68h1m7")))

(define-public crate-enjoin_macro-0.2.0 (c (n "enjoin_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("visit" "visit-mut" "full"))) (d #t) (k 0)))) (h "0fvygfkvwjksn7hp31nc4bpc39szc2f24zkixn1qhc7af0zd4b6f")))

