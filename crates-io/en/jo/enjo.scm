(define-module (crates-io en jo enjo) #:use-module (crates-io))

(define-public crate-enjo-0.1.0 (c (n "enjo") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "home") (r "^0.5.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "1j1bj0yz4pycdmrvqar9z24mkg1k4vxavwcz5n6pn961snh36frj")))

(define-public crate-enjo-0.1.1 (c (n "enjo") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.1") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "home") (r "^0.5.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "171b12xcllkb9iwwl0wjj6h7j3lxr4a7drxjp9xh1vaag03m3qv6")))

(define-public crate-enjo-0.2.0 (c (n "enjo") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.1") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "00nhyczciizbhr6dbnf3k39fbzrjq02qgi7zly3drplracpngi4l")))

