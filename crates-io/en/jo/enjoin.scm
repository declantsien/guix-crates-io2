(define-module (crates-io en jo enjoin) #:use-module (crates-io))

(define-public crate-enjoin-0.1.0 (c (n "enjoin") (v "0.1.0") (d (list (d (n "enjoin_macro") (r "^0.1") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (f (quote ("macro"))) (d #t) (k 2)))) (h "1mrxkjz411riw28g1016smvwgyvrqcy0bkzm4ff31fndkh2s3yxh")))

(define-public crate-enjoin-0.2.0 (c (n "enjoin") (v "0.2.0") (d (list (d (n "enjoin_macro") (r "^0.2") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (f (quote ("macro"))) (d #t) (k 2)))) (h "1h5xmf074z9c1wfjf4103fsz422k3sb963x5hfssln4l10bzwagy")))

