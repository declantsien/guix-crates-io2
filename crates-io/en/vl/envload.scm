(define-module (crates-io en vl envload) #:use-module (crates-io))

(define-public crate-envload-0.1.0 (c (n "envload") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "envload_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1czi4qpzfzycyvqskjajpprq6im073sy1mqsz0475kd6firy5p67")))

(define-public crate-envload-0.1.1 (c (n "envload") (v "0.1.1") (d (list (d (n "envload_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1agyp7j7dvqf9ffmm11l50i26a0aypw8rvcs6qksbz9790nnyy6n")))

