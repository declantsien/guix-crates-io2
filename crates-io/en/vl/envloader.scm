(define-module (crates-io en vl envloader) #:use-module (crates-io))

(define-public crate-envloader-0.1.0 (c (n "envloader") (v "0.1.0") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0xjm6xc9ap4mlpm8k65aa8x37q9v25ykxlim76z58f2i3kswwjpq")))

(define-public crate-envloader-0.1.1 (c (n "envloader") (v "0.1.1") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1fvy20fz8kl9f1a2q053zxkazbdd98kan9lqx7inw95jq1nws6fs")))

(define-public crate-envloader-0.1.2 (c (n "envloader") (v "0.1.2") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "045s8az8zcqw1w25vjmmnkvd90hhhbb6avxmqri5m7wrg6h29f4m")))

(define-public crate-envloader-0.1.3 (c (n "envloader") (v "0.1.3") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0hlixlbaqvjicjx5rkzhn0jj83bplyb6s9h0i2gn6sayk00m0wy6")))

