(define-module (crates-io en vl envload_derive) #:use-module (crates-io))

(define-public crate-envload_derive-0.1.0 (c (n "envload_derive") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13cl8z9nfh4bk780dhbngaz37wm8r36dmsab2dyaq8xdzcnqkmq5")))

(define-public crate-envload_derive-0.1.1 (c (n "envload_derive") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1w2nkc2sqjy11m96zfga2xp0pm6hkki710wc6vh8cxgjzyfhhn71")))

