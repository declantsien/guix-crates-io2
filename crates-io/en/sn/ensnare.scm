(define-module (crates-io en sn ensnare) #:use-module (crates-io))

(define-public crate-ensnare-0.1.0 (c (n "ensnare") (v "0.1.0") (h "1jmbbvbzgirb6r5vshnkb5yrbs15xgbxvq0ylqnw7r5vrdrjiklg") (y #t)))

(define-public crate-ensnare-0.0.1 (c (n "ensnare") (v "0.0.1") (h "1p4619ni35m8sn51g2wr5czzjx50w0gbwhvfscm7bvi2rhy3f8nd") (y #t)))

(define-public crate-ensnare-0.0.2-alpha.1 (c (n "ensnare") (v "0.0.2-alpha.1") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "getrandom") (r "^0.2.12") (d #t) (k 0)))) (h "0jhcydmgbv0274l5l6c32syzkynikxjfrn60igyx6jfk4yq05a11") (f (quote (("std" "getrandom/std") ("default" "std"))))))

