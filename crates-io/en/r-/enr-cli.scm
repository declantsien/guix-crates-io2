(define-module (crates-io en r- enr-cli) #:use-module (crates-io))

(define-public crate-enr-cli-0.1.0-alpha (c (n "enr-cli") (v "0.1.0-alpha") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "enr") (r "^0.1.0-alpha.2") (f (quote ("libp2p" "ed25519"))) (d #t) (k 0)))) (h "0hk059q7s2yabjdp66ynlanp07f52p9fi0s4v2n65akiwq6kl1bl")))

(define-public crate-enr-cli-0.1.0 (c (n "enr-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "enr") (r "^0.1.0") (f (quote ("ed25519"))) (d #t) (k 0)) (d (n "libp2p-core") (r "^0.19.1") (d #t) (k 0)))) (h "04yyn7d0f58rdvffxj9zy7km49q0y033c251px2kfz5xall6chrf")))

(define-public crate-enr-cli-0.1.1 (c (n "enr-cli") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "enr") (r "^0.1.1") (f (quote ("ed25519"))) (d #t) (k 0)) (d (n "libp2p-core") (r "^0.20.1") (d #t) (k 0)))) (h "0fb44iw3xyl7gbia8vcd61wxzdnp0hb07azisscbdv2n7i00pkii")))

(define-public crate-enr-cli-0.1.2 (c (n "enr-cli") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "enr") (r "^0.1.2") (f (quote ("ed25519"))) (d #t) (k 0)) (d (n "libp2p-core") (r "^0.20.1") (d #t) (k 0)))) (h "1kd49rf0jhz3zis8rpfysgylks9m72pmdp9n0x1kl1ihyaf26a5b")))

(define-public crate-enr-cli-0.2.0 (c (n "enr-cli") (v "0.2.0") (d (list (d (n "clap") (r ">=2.33.3, <3.0.0") (d #t) (k 0)) (d (n "enr") (r ">=0.4.0, <0.5.0") (f (quote ("ed25519"))) (d #t) (k 0)) (d (n "eth2_ssz") (r ">=0.1.2, <0.2.0") (d #t) (k 0)) (d (n "eth2_ssz_derive") (r ">=0.1.0, <0.2.0") (d #t) (k 0)) (d (n "hex") (r ">=0.4.2, <0.5.0") (d #t) (k 0)) (d (n "libp2p-core") (r ">=0.24.0, <0.25.0") (d #t) (k 0)))) (h "1hrsl99qj6zq63326hqqlp7fhs8if5s7n7cxraxbbdvy0i5v6nwj")))

(define-public crate-enr-cli-0.3.0 (c (n "enr-cli") (v "0.3.0") (d (list (d (n "clap") (r "^3.0.14") (d #t) (k 0)) (d (n "enr") (r "^0.5.1") (f (quote ("ed25519"))) (d #t) (k 0)) (d (n "eth2_ssz") (r "^0.4.0") (d #t) (k 0)) (d (n "eth2_ssz_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.31.1") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (d #t) (k 0)))) (h "0l3zj74985m6qfkr637izl93f1ar9czink7l1kr74xf0zmgc1w8b")))

(define-public crate-enr-cli-0.4.0 (c (n "enr-cli") (v "0.4.0") (d (list (d (n "clap") (r "^4.0.18") (d #t) (k 0)) (d (n "enr") (r "^0.7.0") (f (quote ("ed25519"))) (d #t) (k 0)) (d (n "eth2_ssz") (r "^0.4.0") (d #t) (k 0)) (d (n "eth2_ssz_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.36.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (d #t) (k 0)))) (h "1yxshswxi1lpwbm2ys42cs845lgagg24sxvkf26alr30c7wqjc12")))

(define-public crate-enr-cli-0.5.0 (c (n "enr-cli") (v "0.5.0") (d (list (d (n "clap") (r "^4.0.18") (d #t) (k 0)) (d (n "enr") (r "^0.8.1") (f (quote ("ed25519"))) (d #t) (k 0)) (d (n "eth2_ssz") (r "^0.4.0") (d #t) (k 0)) (d (n "eth2_ssz_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.36.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (d #t) (k 0)))) (h "1vqnac3n4v9m3hxd355ns000wyqq0q6spb0vgq61rv560ppci0ay")))

(define-public crate-enr-cli-0.5.1 (c (n "enr-cli") (v "0.5.1") (d (list (d (n "clap") (r "^4.0.18") (d #t) (k 0)) (d (n "enr") (r "^0.8.1") (f (quote ("ed25519"))) (d #t) (k 0)) (d (n "eth2_ssz") (r "^0.4.0") (d #t) (k 0)) (d (n "eth2_ssz_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.40.1") (d #t) (k 0)) (d (n "libp2p-identity") (r "^0.2.3") (f (quote ("ecdsa" "ed25519" "peerid" "secp256k1"))) (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (d #t) (k 0)))) (h "19qxm1sd1w7qhc6p38d7gak52xgd95yfhy8xjk41rl7wpbx1bxiz")))

