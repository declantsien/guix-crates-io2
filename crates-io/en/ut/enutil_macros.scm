(define-module (crates-io en ut enutil_macros) #:use-module (crates-io))

(define-public crate-enutil_macros-0.1.0 (c (n "enutil_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "0lh090ykcypfxm44v86a2bv9ih4plzlvhbqlnx28fnxwqr94jcxv")))

