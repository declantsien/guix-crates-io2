(define-module (crates-io en po enpow) #:use-module (crates-io))

(define-public crate-enpow-1.0.0 (c (n "enpow") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0vvminjgx5cg6vxbayb62vigb2ri11ax6jxjdpj5rzl8m9rcap6d")))

(define-public crate-enpow-1.0.1 (c (n "enpow") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0c12y04mm1a411igh9b1yz9v300g26437fx9j0c7c2hnzx6fgxvs")))

(define-public crate-enpow-1.0.2 (c (n "enpow") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "04bc9k83irwjir0iidp3hpg0amgbg10ls1j4h8yn4q3rsdxkrk3g")))

(define-public crate-enpow-2.0.0 (c (n "enpow") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1n2zdd4y0k5n4x898hmf6v3dabrd42swpl3dflmvm6yvdimd6xrj")))

(define-public crate-enpow-2.0.1 (c (n "enpow") (v "2.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0i77cz7vmkdp7li4lzyxx8bzd5i0r1vansjns4rhyppm2ad1sq7s") (r "1.58")))

(define-public crate-enpow-2.0.2 (c (n "enpow") (v "2.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1ykw49z2xjalw0w66b46lmar82z48r6ap5gdrm057apva2b5nwvr") (r "1.58")))

