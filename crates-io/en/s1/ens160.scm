(define-module (crates-io en s1 ens160) #:use-module (crates-io))

(define-public crate-ens160-0.1.1 (c (n "ens160") (v "0.1.1") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1h81g431qhma1g478ll44bs4xj3vm3z5hcwvxbdfszlvck2q8asl") (f (quote (("std") ("default" "std"))))))

(define-public crate-ens160-0.1.2 (c (n "ens160") (v "0.1.2") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1b6xygrcx8jhn72wzsc1j0ja8g0f3myx8asnxjd1lwknbw6gnpan") (f (quote (("std") ("default" "std"))))))

(define-public crate-ens160-0.2.0 (c (n "ens160") (v "0.2.0") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "0hp6jh6dddp3niz466q0yaj9sp7wdwgp2vs10fabn4wabvwgbyw8") (f (quote (("std") ("default" "std"))))))

(define-public crate-ens160-0.3.0 (c (n "ens160") (v "0.3.0") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0-alpha.10") (d #t) (k 0)))) (h "12qsrypn6wh98l7k2417mlh53avpp05mbkvkhqab90yv0wjipgfg") (f (quote (("std") ("default" "std"))))))

(define-public crate-ens160-0.4.0 (c (n "ens160") (v "0.4.0") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0-alpha.10") (d #t) (k 0)) (d (n "embedded-hal-async") (r "=1.0.0-rc.1") (o #t) (d #t) (k 0)))) (h "005aa47y86zii4k4xyzpfs8v2mpc7rwj7vi3zjvrv0hbj67vckf2") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("async" "dep:embedded-hal-async"))))))

(define-public crate-ens160-0.4.1 (c (n "ens160") (v "0.4.1") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0-rc.3") (d #t) (k 0)) (d (n "embedded-hal-async") (r "=1.0.0-rc.3") (o #t) (d #t) (k 0)))) (h "1gr1ifdzxr43xxccl50qb5f3ldb9rfr9p1bqdw8lsf0qkr9bply2") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("async" "dep:embedded-hal-async"))))))

(define-public crate-ens160-0.5.0 (c (n "ens160") (v "0.5.0") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0-rc.3") (d #t) (k 0)) (d (n "embedded-hal-async") (r "=1.0.0-rc.3") (o #t) (d #t) (k 0)) (d (n "maybe-async-cfg") (r "^0.2.3") (d #t) (k 0)))) (h "1cq4r5r8x7g40a7w8n33740rm5yv4mvfmw1h3vfiszlbmsxi2vcp") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("async" "dep:embedded-hal-async"))))))

(define-public crate-ens160-0.5.1 (c (n "ens160") (v "0.5.1") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0-rc.3") (d #t) (k 0)) (d (n "embedded-hal-async") (r "=1.0.0-rc.3") (o #t) (d #t) (k 0)) (d (n "maybe-async-cfg") (r "^0.2.3") (d #t) (k 0)))) (h "0qiqipplngk20czrl09wkfm4lh9qnx9clfvmaaqswsxcwdyk42rd") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("async" "dep:embedded-hal-async"))))))

(define-public crate-ens160-0.6.0 (c (n "ens160") (v "0.6.0") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "maybe-async-cfg") (r "^0.2.3") (d #t) (k 0)))) (h "0zkb5rf4cx1wbn42dbg3qk9r083bmmlsc148xa3w98b5xfj2xk83") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("async" "dep:embedded-hal-async"))))))

(define-public crate-ens160-0.6.1 (c (n "ens160") (v "0.6.1") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "maybe-async-cfg") (r "^0.2.3") (d #t) (k 0)))) (h "0c9qasmzwc1vwj961592syc77yrg3f189yi0839vlmcql3kw44vh") (f (quote (("std") ("default" "std")))) (s 2) (e (quote (("async" "dep:embedded-hal-async"))))))

