(define-module (crates-io en ti entity_table) #:use-module (crates-io))

(define-public crate-entity_table-0.1.0 (c (n "entity_table") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1llcf2v3plg07xnszcnlyl9vyqchjy0a1ynjb3xa0v19xyafcnhg")))

(define-public crate-entity_table-0.2.0 (c (n "entity_table") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0n175py65kp11718f0ahr72y3srln4nc01yk3b97426yylxmz0g7") (f (quote (("serialize" "serde"))))))

(define-public crate-entity_table-0.2.1 (c (n "entity_table") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "10q40s8jsd695z1ak9n3p69yv7znhrkaxrklf5y9sqqi0inqb9d6") (f (quote (("serialize" "serde"))))))

(define-public crate-entity_table-0.2.2 (c (n "entity_table") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0d1y5pviv0hxz32nrx6gdn03kcsvk1c0dvkrd983rj172zm66b69") (f (quote (("serialize" "serde"))))))

(define-public crate-entity_table-0.2.3 (c (n "entity_table") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1vf1dw6y15wmnzn0iaia26bvfyn8cqijrs0rba036hanpb70szih") (f (quote (("serialize" "serde"))))))

(define-public crate-entity_table-0.2.4 (c (n "entity_table") (v "0.2.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0v048yj4m95d8bxkq8w3hr04zrqm2v23ybj99qqd2dd8yhp770bp") (f (quote (("serialize" "serde"))))))

(define-public crate-entity_table-0.2.5 (c (n "entity_table") (v "0.2.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1fz9isr755rpx9rv1kd55wp4dx8rbrad77bnyn6pdj27q3nz7bfm") (f (quote (("serialize" "serde"))))))

(define-public crate-entity_table-0.2.6 (c (n "entity_table") (v "0.2.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0nm7dbgmpbrsfvvmgkxib3lbdk19yqhdwk7kcl4nwfmrpr3ibwv9") (f (quote (("serialize" "serde"))))))

(define-public crate-entity_table-0.2.7 (c (n "entity_table") (v "0.2.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0mzd86v5cryylvmaxr81801s6frjx9jymkf34rq2civ9n22gysrb") (f (quote (("serialize" "serde"))))))

(define-public crate-entity_table-0.2.8 (c (n "entity_table") (v "0.2.8") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0al3i9g0vmf819ikcqz5zdpn3pqbwvch54kww2s19mp7dsr1diiz") (f (quote (("serialize" "serde"))))))

(define-public crate-entity_table-0.2.9 (c (n "entity_table") (v "0.2.9") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0lgakmky9sszlzd7bl48vw6ngmq2ac9xm14bq6j00kk949v4wnn8") (f (quote (("serialize" "serde"))))))

(define-public crate-entity_table-0.2.10 (c (n "entity_table") (v "0.2.10") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "17k06l6q3msnbnz1pr2j0ca4wydgqdp2bj5fcc0sb19ynrbdsc1l") (f (quote (("serialize" "serde"))))))

