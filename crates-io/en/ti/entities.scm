(define-module (crates-io en ti entities) #:use-module (crates-io))

(define-public crate-entities-0.1.0 (c (n "entities") (v "0.1.0") (h "0a9bxhcyhw2cvlz7zwxzw9r5mnqj1zp2agh9mp0w9kwp6iip5m7f")))

(define-public crate-entities-0.2.0 (c (n "entities") (v "0.2.0") (h "05b09j6b5kx2yssg1dhj4v3i1wggi8n67ljnd9gq50xwqy46b24b")))

(define-public crate-entities-0.3.0 (c (n "entities") (v "0.3.0") (h "14h3k8lzr7vxzffdg9pp69010hkjj49sx6wk8lkyjvgzpx4dzk3f")))

(define-public crate-entities-1.0.0 (c (n "entities") (v "1.0.0") (h "1vv3zp3yl8ic4bwyvlnxr832qhgkphig64fg74n3130lrnbjwc1l")))

(define-public crate-entities-1.0.1 (c (n "entities") (v "1.0.1") (h "1jnpr0zvj97wm9pnh7fnl74rzaar39hhg65p03cm08bqqgj0lcmm")))

(define-public crate-entities-1.0.2-rc.1 (c (n "entities") (v "1.0.2-rc.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1h37g7j1ydlvzki66l8gbfb3xll8kknxc2dysx12vzqn2hjrmzx6") (r "1.13")))

