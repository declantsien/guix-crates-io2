(define-module (crates-io en ti entity_id_core) #:use-module (crates-io))

(define-public crate-entity_id_core-0.0.2 (c (n "entity_id_core") (v "0.0.2") (h "1hbw315qr8kbwzjnwpmndal4dlhijykhvhjg474sqp5vzgh8vr9h")))

(define-public crate-entity_id_core-0.0.3 (c (n "entity_id_core") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.53") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (o #t) (d #t) (k 0)))) (h "1bfd8rq3rgfkji2z3a67gc0rpsjvcq58gigiim6qmlspvj0p4j2i") (s 2) (e (quote (("uuid" "dep:uuid" "ulid/uuid"))))))

(define-public crate-entity_id_core-0.0.4 (c (n "entity_id_core") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.53") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (o #t) (d #t) (k 0)))) (h "0da4qbl80fqgp4y5acxjcb19mfcj0f8s07ljz4k7i87fhnrbh8y0") (s 2) (e (quote (("uuid" "dep:uuid" "ulid/uuid"))))))

