(define-module (crates-io en ti entity_store_helper) #:use-module (crates-io))

(define-public crate-entity_store_helper-0.1.0 (c (n "entity_store_helper") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.15") (d #t) (k 0)) (d (n "direction") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0dxvbp31qnp6bkn83qi4x8iz1p5dbg2ykyv1wicnckxhcr6xsv1v")))

(define-public crate-entity_store_helper-0.2.0 (c (n "entity_store_helper") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.15") (d #t) (k 0)) (d (n "direction") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1jcmnkws70rm5g6qxzzlml8rxgf5ij91hn5m8jvsfrbs52c0pl5q")))

(define-public crate-entity_store_helper-0.3.0 (c (n "entity_store_helper") (v "0.3.0") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "direction") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1dfhr22kmb6w74f6d5ihd5vk3bl4gax5dfvz97y7ja963wm0iscs")))

(define-public crate-entity_store_helper-0.4.0 (c (n "entity_store_helper") (v "0.4.0") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "direction") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1gv3rjzyrz3afl3gygd3fsnq0iwlcw128vkbibj9plkwmiyf3xa8")))

(define-public crate-entity_store_helper-0.4.1 (c (n "entity_store_helper") (v "0.4.1") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "direction") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0qjlnkr7x9wg8qq6w34kgidw1p9cyr2ag3j3y59llpl4k9mzzk2c")))

(define-public crate-entity_store_helper-0.5.0 (c (n "entity_store_helper") (v "0.5.0") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "direction") (r "^0.8") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0kny41y6jhxrspd27axhxfs351r3nx13lf81ma96am7bqhqqb79b")))

(define-public crate-entity_store_helper-0.6.0 (c (n "entity_store_helper") (v "0.6.0") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "direction") (r "^0.9") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "11yp4z6y79kracjzilf32dzb0n1wf38mv7vjdsz4d1ycz7wwvm6k")))

(define-public crate-entity_store_helper-0.7.0 (c (n "entity_store_helper") (v "0.7.0") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "direction") (r "^0.10") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0jjq609wzzz7xd54d6wij2k108c8whjfvf3ln0fd4iqj7zsbl88d")))

(define-public crate-entity_store_helper-0.9.0 (c (n "entity_store_helper") (v "0.9.0") (d (list (d (n "append") (r "^0.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "direction") (r "^0.13") (d #t) (k 0)) (d (n "grid_2d") (r "^0.7") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0cwb4744izlba1i564rg6vbr8jr6835y0y3y9c1h02i1m8v574v2")))

(define-public crate-entity_store_helper-0.10.0 (c (n "entity_store_helper") (v "0.10.0") (d (list (d (n "append") (r "^0.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "direction") (r "^0.14") (d #t) (k 0)) (d (n "grid_2d") (r "^0.7") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0zwca354jrd3ganc6q0rc7i661aac19fqgvb895bxgs6yjjwwin2")))

(define-public crate-entity_store_helper-0.11.0 (c (n "entity_store_helper") (v "0.11.0") (d (list (d (n "append") (r "^0.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "direction") (r "^0.14") (d #t) (k 0)) (d (n "grid_2d") (r "^0.7") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0in7khhp4r1nd7qv7zh2nn84l3x2kklgh59ammx5mcax5qn61m5k")))

(define-public crate-entity_store_helper-0.12.0 (c (n "entity_store_helper") (v "0.12.0") (d (list (d (n "append") (r "^0.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "direction") (r "^0.14") (d #t) (k 0)) (d (n "grid_2d") (r "^0.7") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "19gfqic6258n33644067kp23ws9110j5bwbjwd9azxp022b5mz5h")))

(define-public crate-entity_store_helper-0.13.0 (c (n "entity_store_helper") (v "0.13.0") (d (list (d (n "append") (r "^0.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "direction") (r "^0.14") (d #t) (k 0)) (d (n "grid_2d") (r "^0.7") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1y91g0pr3fjdyaqdlfshmprpv3279cxsqgl78c0ls1mngwczxnwr")))

(define-public crate-entity_store_helper-0.14.0 (c (n "entity_store_helper") (v "0.14.0") (d (list (d (n "direction") (r "^0.15") (d #t) (k 0)) (d (n "grid_2d") (r "^0.8") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1a49byqxd87j26dp5bwfwknwvba5x70wpi544k5f80bwfqrrys9y")))

(define-public crate-entity_store_helper-0.15.0 (c (n "entity_store_helper") (v "0.15.0") (d (list (d (n "direction") (r "^0.15") (d #t) (k 0)) (d (n "grid_2d") (r "^0.8") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0whn3cbv098l7ww6rpsaa9ndbkvcdykmxzciahin0vg0n398fg55")))

(define-public crate-entity_store_helper-0.16.0 (c (n "entity_store_helper") (v "0.16.0") (d (list (d (n "direction") (r "^0.17") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "grid_2d") (r "^0.11") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "1w7rv4qrgh88yzc9m0n2rwcx9y6v5nfa0wy14c7qw58ix0c05hgf")))

(define-public crate-entity_store_helper-0.16.1 (c (n "entity_store_helper") (v "0.16.1") (d (list (d (n "direction") (r "^0.17") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "grid_2d") (r "^0.12") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "1zgisw7fx41qdldayzx6b4l9icfgxwn3pz7wsnbwwgg7sib9h2q4")))

(define-public crate-entity_store_helper-0.17.0 (c (n "entity_store_helper") (v "0.17.0") (d (list (d (n "append") (r "^0.1") (d #t) (k 0)) (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "direction") (r "^0.17") (d #t) (k 0)) (d (n "grid_2d") (r "^0.12") (d #t) (k 0)) (d (n "num") (r "^0.1") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "06x4vc08jif2r03pq8i3zzln6bsdq0i0b80v057bfwc53n5xpna8")))

