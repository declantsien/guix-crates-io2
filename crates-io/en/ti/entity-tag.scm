(define-module (crates-io en ti entity-tag) #:use-module (crates-io))

(define-public crate-entity-tag-0.1.0 (c (n "entity-tag") (v "0.1.0") (h "0hlcr0rj607qi6z7igj5g3pvzbj623s1ljsllp3i3c2jbcq6gj77") (f (quote (("std") ("default" "std"))))))

(define-public crate-entity-tag-0.1.1 (c (n "entity-tag") (v "0.1.1") (h "0z274zsawpb3gmlgy5g24vbd4kakc920lkrkq5nvmlyyw9z8xxwk") (f (quote (("std") ("default" "std"))))))

(define-public crate-entity-tag-0.1.2 (c (n "entity-tag") (v "0.1.2") (d (list (d (n "base64") (r "^0.13") (f (quote ("alloc"))) (k 0)) (d (n "wyhash") (r "^0.5") (d #t) (k 0)))) (h "0ln8iaf9zpq08sng3r0ffjb3kr3qcx5sha85wal4s22yrv6mc4ph") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-entity-tag-0.1.3 (c (n "entity-tag") (v "0.1.3") (d (list (d (n "base64") (r "^0.13") (f (quote ("alloc"))) (k 0)) (d (n "wyhash") (r "^0.5") (d #t) (k 0)))) (h "1m8y6kaz4glblrgpqm5nclsyzspsixfnyvwp243vpjdamani8cmm") (f (quote (("std") ("default" "std"))))))

(define-public crate-entity-tag-0.1.4 (c (n "entity-tag") (v "0.1.4") (d (list (d (n "base64") (r "^0.13") (f (quote ("alloc"))) (k 0)) (d (n "wyhash") (r "^0.5") (d #t) (k 0)))) (h "07pmparpj8sjx5x374j508dzal2b3j8m71ng3dbkkb6pj46zf6jl") (f (quote (("std") ("default" "std"))))))

(define-public crate-entity-tag-0.1.5 (c (n "entity-tag") (v "0.1.5") (d (list (d (n "base64") (r "^0.13") (f (quote ("alloc"))) (k 0)) (d (n "wyhash") (r "^0.5") (d #t) (k 0)))) (h "1zsc8v9h7c52zifjylfii3j3ksv4hq5gw9886kgb9w7zimb4kkc2") (f (quote (("std") ("default" "std"))))))

(define-public crate-entity-tag-0.1.6 (c (n "entity-tag") (v "0.1.6") (d (list (d (n "ahash") (r "^0.7.6") (k 0)) (d (n "base64") (r "^0.13") (f (quote ("alloc"))) (k 0)))) (h "0r6zzdx2wbfnih4gdfnm4wr4llrb82ng7rxb0k6gdjpri7j5dv74") (f (quote (("std") ("default" "std"))))))

(define-public crate-entity-tag-0.1.7 (c (n "entity-tag") (v "0.1.7") (d (list (d (n "base64") (r "^0.21") (f (quote ("alloc"))) (k 0)) (d (n "highway") (r "^1") (k 0)))) (h "0wh81rdjyrqd09s5n6cj6g9kha2i913sz6xjfq9i9kdfzm4r06mm") (f (quote (("std" "base64/std" "highway/std") ("default" "std")))) (r "1.57")))

(define-public crate-entity-tag-0.1.8 (c (n "entity-tag") (v "0.1.8") (d (list (d (n "base64") (r "^0.21") (f (quote ("alloc"))) (k 0)) (d (n "highway") (r "^1") (k 0)))) (h "0y9bznz0vfcx8d45j0rpg6fzichnp6cgq9yvy2r2jkvykfkiax11") (f (quote (("std" "base64/std" "highway/std") ("default" "std")))) (r "1.56")))

