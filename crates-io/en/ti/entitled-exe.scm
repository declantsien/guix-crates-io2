(define-module (crates-io en ti entitled-exe) #:use-module (crates-io))

(define-public crate-entitled-exe-0.1.0 (c (n "entitled-exe") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.12") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "16247zwrqxpf2ifrb6ja6gi6sv7lgk9m2pgq0pyppb1ff0x962yd")))

(define-public crate-entitled-exe-0.2.0 (c (n "entitled-exe") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.12") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1y9c7i41l95jk1cc24sn83n1idm3v4c40wgr8v7p5w423qhbyaj0")))

(define-public crate-entitled-exe-0.2.1 (c (n "entitled-exe") (v "0.2.1") (d (list (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1bsb0pbkw57mvpqbyils6c077n2fyylgrnavkp057mqr3xi6rbq2")))

(define-public crate-entitled-exe-0.2.3 (c (n "entitled-exe") (v "0.2.3") (d (list (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1xvv4viq2k9ac1rpxbrn783kliri7i8xisrba9b552m8bb835c4s")))

(define-public crate-entitled-exe-0.3.0 (c (n "entitled-exe") (v "0.3.0") (d (list (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "0bw0kpybfnyjg88wh62a97mp89k5j74arlr02diwx5nc9yd7bnrs")))

(define-public crate-entitled-exe-0.3.1 (c (n "entitled-exe") (v "0.3.1") (d (list (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "02nx0lzrfbsd02hhfjg3gv4fp9m7vvwwx8fnly5bypp3z3wh9j2f")))

