(define-module (crates-io en ti entity_table_realtime) #:use-module (crates-io))

(define-public crate-entity_table_realtime-0.1.0 (c (n "entity_table_realtime") (v "0.1.0") (d (list (d (n "entity_table") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 2)))) (h "130lk96fal2pfl3is1c7sdm2ij28d3f4c28zd66pax3wq6004h9j") (f (quote (("serialize" "serde" "entity_table/serialize"))))))

(define-public crate-entity_table_realtime-0.1.1 (c (n "entity_table_realtime") (v "0.1.1") (d (list (d (n "entity_table") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 2)))) (h "0pxsl6wipdmwf5f9i3hmlnb8r0gv0gkx6ldxvmn86p6hhz9s0vax") (f (quote (("serialize" "serde" "entity_table/serialize"))))))

(define-public crate-entity_table_realtime-0.1.2 (c (n "entity_table_realtime") (v "0.1.2") (d (list (d (n "entity_table") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 2)))) (h "0cplar1sh66ipahsfzmxi9h743j5gzhaa83mzrx8gm9gy44grmxl") (f (quote (("serialize" "serde" "entity_table/serialize"))))))

(define-public crate-entity_table_realtime-0.2.0 (c (n "entity_table_realtime") (v "0.2.0") (d (list (d (n "entity_table") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 2)))) (h "09lsyarxacl15mpnk6wb3j5gsgpdp2qz66fm1nz5plpk9ja0m380") (f (quote (("serialize" "serde" "entity_table/serialize"))))))

