(define-module (crates-io en ti entity_id) #:use-module (crates-io))

(define-public crate-entity_id-0.0.1 (c (n "entity_id") (v "0.0.1") (d (list (d (n "entity_id_derive") (r "^0.0.1") (d #t) (k 0)))) (h "19nmaxzmyzxsh82cxbdcldjy68p4fb68kpvy5fi7shzc04vpd49d")))

(define-public crate-entity_id-0.0.2 (c (n "entity_id") (v "0.0.2") (d (list (d (n "entity_id_derive") (r "^0.0.2") (d #t) (k 0)))) (h "1nr2c01mdmm3gwf8z1r8fd9ah0ccqimfirvlrkr9r6808i11wmf5")))

(define-public crate-entity_id-0.0.3 (c (n "entity_id") (v "0.0.3") (d (list (d (n "entity_id_core") (r "^0.0.3") (d #t) (k 0)) (d (n "entity_id_derive") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (d #t) (k 2)) (d (n "uuid") (r "^1.3.0") (d #t) (k 2)))) (h "1c0j835xyqlxy1cnks2ixzv2y2qqisinajl6cx91gd91zkdjwnj2") (f (quote (("derive" "entity_id_derive") ("default" "derive")))) (s 2) (e (quote (("uuid" "entity_id_core/uuid" "entity_id_derive?/uuid"))))))

(define-public crate-entity_id-0.0.4 (c (n "entity_id") (v "0.0.4") (d (list (d (n "entity_id_core") (r "^0.0.4") (d #t) (k 0)) (d (n "entity_id_derive") (r "^0.0.4") (o #t) (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (d #t) (k 2)) (d (n "uuid") (r "^1.3.0") (d #t) (k 2)))) (h "17jfmdllc7y8vb6bmqgcwl11qv9w2pl07ds65aam491w77lcsyl7") (f (quote (("derive" "entity_id_derive") ("default" "derive")))) (s 2) (e (quote (("uuid" "entity_id_core/uuid" "entity_id_derive?/uuid"))))))

