(define-module (crates-io en ti entity_noop_macros) #:use-module (crates-io))

(define-public crate-entity_noop_macros-0.1.0 (c (n "entity_noop_macros") (v "0.1.0") (h "02d3k65gm0idxmh1liq89xyd0x25mymwnaqrmfym94m42gg7jij8")))

(define-public crate-entity_noop_macros-0.3.0 (c (n "entity_noop_macros") (v "0.3.0") (h "0ijw1yqig5zsi4firy53dnqkii4akiqgf3dbsv6hibsg9fm7y99d")))

(define-public crate-entity_noop_macros-0.3.1 (c (n "entity_noop_macros") (v "0.3.1") (h "0indyb6b7qv1mqxaik33rln6lizj0wx8akyib20m3zrfblr130s3")))

(define-public crate-entity_noop_macros-0.3.2 (c (n "entity_noop_macros") (v "0.3.2") (h "0hlixria93adsnzkg0am7xnnwa08a4mylpxpp5zgbc4l2k30s3cm")))

