(define-module (crates-io en ti entity-gym-derive) #:use-module (crates-io))

(define-public crate-entity-gym-derive-0.1.0 (c (n "entity-gym-derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1dkf2sc66g7sz8xlfs6si1ck4ihj83nyzrvlik67nz35rh81rppa")))

(define-public crate-entity-gym-derive-0.1.1 (c (n "entity-gym-derive") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "183z2xnma8cfp73ib8z4dhy8bskrdh4pyh8rbyjczqh3jr9wsly7")))

(define-public crate-entity-gym-derive-0.1.2 (c (n "entity-gym-derive") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1k3b90kh5xlbpf8vpbd0g9sr9hbrx7xi3cxrmvyr7b3slh6s71iw")))

(define-public crate-entity-gym-derive-0.1.3 (c (n "entity-gym-derive") (v "0.1.3") (d (list (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1imrady7vyv743r0pfrx0473q1w0sbig79hs2w1g3frils4w78lv")))

(define-public crate-entity-gym-derive-0.2.0 (c (n "entity-gym-derive") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qmc9qggsr59y0z08a9k6p7ghqkn50rww3ayc095k3nalq5ahqf4")))

