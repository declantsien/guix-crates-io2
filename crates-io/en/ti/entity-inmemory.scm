(define-module (crates-io en ti entity-inmemory) #:use-module (crates-io))

(define-public crate-entity-inmemory-0.3.0 (c (n "entity-inmemory") (v "0.3.0") (d (list (d (n "dyn-clone") (r "^1.0.3") (d #t) (k 0)) (d (n "entity") (r "=0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0qz7ii7afhyzdhc3azg0s1k7zf3cbfd733gwz7w5sbc4ckq855wc") (f (quote (("serde-1" "serde" "serde/rc" "entity/serde-1"))))))

(define-public crate-entity-inmemory-0.3.1 (c (n "entity-inmemory") (v "0.3.1") (d (list (d (n "dyn-clone") (r "^1.0.3") (d #t) (k 0)) (d (n "entity") (r "=0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0kpqj5r3gnba3xdnlmqfhbvzpnydglxqjl1g8gldw7a000d8vl65") (f (quote (("serde-1" "serde" "serde/rc" "entity/serde-1"))))))

(define-public crate-entity-inmemory-0.3.2 (c (n "entity-inmemory") (v "0.3.2") (d (list (d (n "dyn-clone") (r "^1.0.3") (d #t) (k 0)) (d (n "entity") (r "=0.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0hlk05g3c6jl9iabrp3zvbn5b02za95n3zs1k3qwbcka5r76w54d") (f (quote (("serde-1" "serde" "serde/rc" "entity/serde-1"))))))

