(define-module (crates-io en ti entity-async-graphql) #:use-module (crates-io))

(define-public crate-entity-async-graphql-0.3.0 (c (n "entity-async-graphql") (v "0.3.0") (d (list (d (n "async-graphql") (r "^2.7.4") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.11") (f (quote ("from" "into"))) (k 0)) (d (n "entity") (r "=0.3.0") (d #t) (k 0)) (d (n "entity-async-graphql-macros") (r "=0.3.0") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "0sr200qyjqa08adhlgxd2nq61saqn4n791c72vnd9jfxzlnn36qd") (f (quote (("macros" "entity-async-graphql-macros"))))))

(define-public crate-entity-async-graphql-0.3.1 (c (n "entity-async-graphql") (v "0.3.1") (d (list (d (n "async-graphql") (r "^2.7.4") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.11") (f (quote ("from" "into"))) (k 0)) (d (n "entity") (r "=0.3.1") (d #t) (k 0)) (d (n "entity-async-graphql-macros") (r "=0.3.1") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "0i1y7z8inp812gqjb644fg45jg0spdicc3cdpjx1yr94j9papszj") (f (quote (("macros" "entity-async-graphql-macros"))))))

(define-public crate-entity-async-graphql-0.3.2 (c (n "entity-async-graphql") (v "0.3.2") (d (list (d (n "async-graphql") (r "^2.7.4") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.11") (f (quote ("from" "into"))) (k 0)) (d (n "entity") (r "=0.3.2") (d #t) (k 0)) (d (n "entity-async-graphql-macros") (r "=0.3.2") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "1r7lizif6bsmv9c2kwy5d6vid3skg1j7davx5jybs77901zv7iz0") (f (quote (("macros" "entity-async-graphql-macros"))))))

