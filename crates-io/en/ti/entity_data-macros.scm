(define-module (crates-io en ti entity_data-macros) #:use-module (crates-io))

(define-public crate-entity_data-macros-0.1.0 (c (n "entity_data-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bm2cn1f24bjqa7brsy7864gim4n51flgg028vjahwa3wxg3lsd1")))

(define-public crate-entity_data-macros-1.1.0 (c (n "entity_data-macros") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0s10ijx1mxnk6gv79bc6fn2p54a86li4js749k8fka349cgjain1")))

(define-public crate-entity_data-macros-1.1.1 (c (n "entity_data-macros") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ady528kk652s8b8r05di4f7z1y90v7m5hl35qpp2j8m00l00xxz")))

(define-public crate-entity_data-macros-1.2.0 (c (n "entity_data-macros") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11pyq575qrxcl6fsn283aiy9ikpnnpq31sq37v98rf86cb4r6vrh")))

(define-public crate-entity_data-macros-1.3.0 (c (n "entity_data-macros") (v "1.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0dadic641sr6babrrz1lxh4029c65j5291cjbgx7whvhhmd1z63l")))

(define-public crate-entity_data-macros-1.5.0 (c (n "entity_data-macros") (v "1.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rj3vcrd4mrzgn166ich0bfpws33fc3vjydnv5m54wdsq1zqsy41")))

(define-public crate-entity_data-macros-1.5.1 (c (n "entity_data-macros") (v "1.5.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1dalj7p52lyxnyn2h8b27mxn25qlxs2xls537hzbx7nkivcgdx5y")))

(define-public crate-entity_data-macros-1.6.0 (c (n "entity_data-macros") (v "1.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1x2l6ass21jfdnx1dpfgjan90q8xhrbgrf53ixp08bi0xq93wljq")))

(define-public crate-entity_data-macros-1.6.1 (c (n "entity_data-macros") (v "1.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zs4hfnzmzxnwggsdfms3zbyly3gaqxl3bc2zwkaify1hjgn24jb")))

(define-public crate-entity_data-macros-1.6.2 (c (n "entity_data-macros") (v "1.6.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ag717ifhilwkcvzazh6flp6sx75isnqsc4qkk9i4h4h82lgyhv9")))

(define-public crate-entity_data-macros-1.6.3 (c (n "entity_data-macros") (v "1.6.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "151y5hfj0ipji6kppn5s5rzm4w26hnr1vdwbvwhvyx5mrrpk44ba")))

(define-public crate-entity_data-macros-1.7.0 (c (n "entity_data-macros") (v "1.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "100h72d50i413pl11sdy241d2hk1vrfw6r5padvyw8p80fp9p4r3")))

