(define-module (crates-io en ti entity-sled) #:use-module (crates-io))

(define-public crate-entity-sled-0.3.0 (c (n "entity-sled") (v "0.3.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "entity") (r "=0.3.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)))) (h "0knxghsw2nfw76xnjlskib3hqmjkby7m6w05dmm2s7r3h6ngrwgj")))

(define-public crate-entity-sled-0.3.1 (c (n "entity-sled") (v "0.3.1") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "entity") (r "=0.3.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)))) (h "1jmk012yqpgvrpflrlxx267ss9m8xjh9k2lxy6jfgwl4r2s9pqyd")))

(define-public crate-entity-sled-0.3.2 (c (n "entity-sled") (v "0.3.2") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "entity") (r "=0.3.2") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)))) (h "0iqypkw6zy5gb22xq7h71kcpham53vzn29ifrpsj9dwqc110qmpd")))

