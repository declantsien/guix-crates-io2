(define-module (crates-io en ti entity_rust) #:use-module (crates-io))

(define-public crate-entity_rust-0.0.1 (c (n "entity_rust") (v "0.0.1") (h "0ca33nl41sf35ivcd164wy8hnb9jpmw27j7ikmxvq4x6iyvym64z")))

(define-public crate-entity_rust-0.0.2 (c (n "entity_rust") (v "0.0.2") (d (list (d (n "lazy_static") (r ">= 0.1.15") (d #t) (k 0)) (d (n "shared-mutex") (r ">= 0.3.1") (d #t) (k 0)) (d (n "uuid") (r ">= 0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "09gbq9xh2gf86i0rr6wab9nmi1qyhnwx7dnz116wj2n9wvxh77i4")))

(define-public crate-entity_rust-0.0.3 (c (n "entity_rust") (v "0.0.3") (d (list (d (n "lazy_static") (r ">= 0.1.15") (d #t) (k 0)) (d (n "shared-mutex") (r ">= 0.3.1") (d #t) (k 0)) (d (n "uuid") (r ">= 0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0baw2z2dw4s7nw742faghhjs9hlm42qi4d14mm0vahq7ky4jndda")))

(define-public crate-entity_rust-0.0.4 (c (n "entity_rust") (v "0.0.4") (d (list (d (n "lazy_static") (r ">= 0.1.15") (d #t) (k 0)) (d (n "shared-mutex") (r ">= 0.3.1") (d #t) (k 0)) (d (n "uuid") (r ">= 0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1g02zxr248nq0jfsd5bxyg8my8vxsdndlgs24sijk881vq1am1j7")))

(define-public crate-entity_rust-0.0.5 (c (n "entity_rust") (v "0.0.5") (d (list (d (n "lazy_static") (r ">= 0.1.15") (d #t) (k 0)) (d (n "shared-mutex") (r ">= 0.3.1") (d #t) (k 0)) (d (n "uuid") (r ">= 0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1fwjq9p3gympb7dqfcpzcqah8bqh2zg61hpd0w2wzvg5rzsyc0xz")))

(define-public crate-entity_rust-0.0.6 (c (n "entity_rust") (v "0.0.6") (d (list (d (n "lazy_static") (r ">= 0.1.15") (d #t) (k 0)) (d (n "shared-mutex") (r ">= 0.3.1") (d #t) (k 0)) (d (n "uuid") (r ">= 0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1h5sjc96cwdiaxj19qkxl32cnxvi9xvw4yz4pjsv30vp1hnba5dl")))

(define-public crate-entity_rust-0.0.7 (c (n "entity_rust") (v "0.0.7") (d (list (d (n "lazy_static") (r ">= 0.1.15") (d #t) (k 0)) (d (n "shared-mutex") (r ">= 0.3.1") (d #t) (k 0)) (d (n "uuid") (r ">= 0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "00ni513vfvr934jr4iqdrx3fl6bcmi292g89lym18wampvxba9ll")))

(define-public crate-entity_rust-0.0.8 (c (n "entity_rust") (v "0.0.8") (d (list (d (n "lazy_static") (r ">= 0.1.15") (d #t) (k 0)) (d (n "shared-mutex") (r ">= 0.3.1") (d #t) (k 0)) (d (n "uuid") (r ">= 0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1qd05bimjhpv8mj65ighr89vlm62zqy1f7bwykpj2ivvi050rx44")))

