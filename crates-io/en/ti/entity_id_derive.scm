(define-module (crates-io en ti entity_id_derive) #:use-module (crates-io))

(define-public crate-entity_id_derive-0.0.1 (c (n "entity_id_derive") (v "0.0.1") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (f (quote ("uuid"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (d #t) (k 0)))) (h "030xn0v50m27gk18j3c756lbg6bh4dd98hf3bn8a41jvscmpjc05")))

(define-public crate-entity_id_derive-0.0.2 (c (n "entity_id_derive") (v "0.0.2") (d (list (d (n "entity_id_core") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.53") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (f (quote ("uuid"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (d #t) (k 0)))) (h "1cawxip5i93601yslm4y50p8w6swsssqkdx1x1bxfnrm5ym1bjxg")))

(define-public crate-entity_id_derive-0.0.3 (c (n "entity_id_derive") (v "0.0.3") (d (list (d (n "entity_id_core") (r "^0.0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.53") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (o #t) (d #t) (k 0)))) (h "0gpvk12vgdqn80zi8ajjk507zv248ayb2mirir1vw4swh2g3m0yf") (s 2) (e (quote (("uuid" "dep:uuid" "entity_id_core/uuid"))))))

(define-public crate-entity_id_derive-0.0.4 (c (n "entity_id_derive") (v "0.0.4") (d (list (d (n "entity_id_core") (r "^0.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.53") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (o #t) (d #t) (k 0)))) (h "0djlwvrvzahqiw3pkci90rikmbx8bg12q6x0iyvw2dzgqzw4nr9m") (s 2) (e (quote (("uuid" "dep:uuid" "entity_id_core/uuid"))))))

