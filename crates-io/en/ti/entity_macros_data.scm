(define-module (crates-io en ti entity_macros_data) #:use-module (crates-io))

(define-public crate-entity_macros_data-0.3.0 (c (n "entity_macros_data") (v "0.3.0") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "1y1dgk6b5kzb0g78pxlylvvqr97r18q9im0wk90dr5rj19dr3kc2")))

(define-public crate-entity_macros_data-0.3.1 (c (n "entity_macros_data") (v "0.3.1") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "0jyhz267c19114fdr17lm1pjwzqidmf8wndfaa56aa2dbk778r4c")))

(define-public crate-entity_macros_data-0.3.2 (c (n "entity_macros_data") (v "0.3.2") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "0xx23q98g3i357fazpbdqy67w23hlnlxb9nfpbiy23y41i7dnjki")))

