(define-module (crates-io en va envault-cipher) #:use-module (crates-io))

(define-public crate-envault-cipher-0.0.1 (c (n "envault-cipher") (v "0.0.1") (d (list (d (n "aes") (r "^0.8.1") (d #t) (k 0)) (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "cbc") (r "^0.1.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.12.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "1k4fsi29lc63djs32pv5y6a5cnxnhr3zv6n63h0b1bym7y17n987")))

