(define-module (crates-io en va envault) #:use-module (crates-io))

(define-public crate-envault-0.0.1 (c (n "envault") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "envault-processor") (r "^0.0.1") (d #t) (k 0)))) (h "0ib78pkxbm5q5y2c3m25gf7xvssvr0g9fdwjkispaqcwv3qd52dh")))

(define-public crate-envault-0.0.2 (c (n "envault") (v "0.0.2") (d (list (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "envault-processor") (r "^0.0.1") (d #t) (k 0)))) (h "03c3gm8z2lfinhciayyq183m8ics44slcadhqf5q5h7vidq35mf7")))

(define-public crate-envault-0.0.3 (c (n "envault") (v "0.0.3") (d (list (d (n "aes") (r "^0.8.1") (d #t) (k 0)) (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "cbc") (r "^0.1.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.12.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "1qf4c4sskyl9mr5yi7rhs1pr709dgkb6vvq1jn8kqhqlsi9x5qj2")))

