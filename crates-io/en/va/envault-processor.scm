(define-module (crates-io en va envault-processor) #:use-module (crates-io))

(define-public crate-envault-processor-0.0.1 (c (n "envault-processor") (v "0.0.1") (d (list (d (n "envault-cipher") (r "^0.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)))) (h "03z0jnjswp00lv6makmirwf0drcjyzpy5ff8h9hsfml5wb9cd1di")))

