(define-module (crates-io en va envar) #:use-module (crates-io))

(define-public crate-envar-0.1.0 (c (n "envar") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qygqz352y4ababflb4gl8adwcadbw3x1a1bz5mq2jfignvr70al")))

(define-public crate-envar-0.1.1 (c (n "envar") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07i7vhch1msayn7vs26ccqs9yzbdbdkqmgbb906x5jcmf96j8l21")))

