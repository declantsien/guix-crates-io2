(define-module (crates-io en ta entangled) #:use-module (crates-io))

(define-public crate-entangled-1.0.0 (c (n "entangled") (v "1.0.0") (d (list (d (n "async-channel") (r "^1.6") (d #t) (k 0)) (d (n "async-executor") (r "^1.4") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11") (d #t) (k 0)))) (h "0lv6z46dns5jvridyfvf7qndg4nmkl17im9mzcny5imfyk64s7kw")))

(define-public crate-entangled-1.1.0 (c (n "entangled") (v "1.1.0") (d (list (d (n "async-channel") (r "^1.6") (d #t) (k 0)) (d (n "async-executor") (r "^1.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures-lite") (r "^1.11") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 2)))) (h "0gbsivd8sssiadxj8rpbixbbhvhpjz0k2rb5mphch2gv9qh4an73")))

(define-public crate-entangled-1.2.0 (c (n "entangled") (v "1.2.0") (d (list (d (n "async-channel") (r "^1.6") (d #t) (k 0)) (d (n "async-executor") (r "^1.4") (d #t) (k 0)) (d (n "async-std") (r "^1.9") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures-lite") (r "^1.11") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 2)) (d (n "switchyard") (r "^0.2") (d #t) (k 2)))) (h "06sarjwwvp29l4v4f61skqhwrwqdmq7q1ci97yg9601qi3p74651")))

(define-public crate-entangled-1.3.0 (c (n "entangled") (v "1.3.0") (d (list (d (n "async-channel") (r "^1.6") (d #t) (k 0)) (d (n "async-executor") (r "^1.4") (d #t) (k 0)) (d (n "async-std") (r "^1.9") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures-lite") (r "^1.11") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 2)) (d (n "switchyard") (r "^0.2") (d #t) (k 2)))) (h "0a8dbdvyya82hmds66ncrs9hddjs0f4v2yb4b24vh88k1ic1p2m0")))

