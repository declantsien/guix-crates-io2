(define-module (crates-io en ta entab-cli) #:use-module (crates-io))

(define-public crate-entab-cli-0.1.0 (c (n "entab-cli") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "entab") (r "^0.1.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1mixh8cbfalw2rk6wjvy5i5wqag93yfnjpj0pqrnx87x47dh7byd") (f (quote (("mmap" "memmap") ("default" "mmap"))))))

(define-public crate-entab-cli-0.2.0 (c (n "entab-cli") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "entab") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0g3gy7fgzzrra1zkw55sy1ihhha3z0vw7nqy3r8mghn32yz2bsyw") (f (quote (("mmap" "memmap") ("default" "mmap"))))))

(define-public crate-entab-cli-0.2.1 (c (n "entab-cli") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "entab") (r "^0.2.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0gdq7lja8w4mncz2dlm9d1b7xr3wsx3s18xxjz6ixns28xi4x47b") (f (quote (("mmap" "memmap") ("default" "mmap"))))))

(define-public crate-entab-cli-0.2.2 (c (n "entab-cli") (v "0.2.2") (d (list (d (n "clap") (r "^3.0.0-beta") (d #t) (k 0)) (d (n "entab") (r "^0.2.2") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (o #t) (d #t) (k 0)))) (h "139jgprm3jc7z64kx67c58mpi4b650dvf8ac9f5fprm0wm1mqjwh") (f (quote (("mmap" "memmap") ("default" "mmap"))))))

