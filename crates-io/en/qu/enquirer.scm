(define-module (crates-io en qu enquirer) #:use-module (crates-io))

(define-public crate-enquirer-0.2.0 (c (n "enquirer") (v "0.2.0") (d (list (d (n "console") (r "^0.9.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)))) (h "0q7lk8vdf6hlpar04xx764n7gljkk7qhfjdkqn73d46h05n1047g")))

(define-public crate-enquirer-0.3.0 (c (n "enquirer") (v "0.3.0") (d (list (d (n "console") (r "^0.9.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)))) (h "1vhvrnmqs04ww2syfr2hwi6jrc06llin2x40sgc281jk9hvwmwrm")))

(define-public crate-enquirer-0.3.1 (c (n "enquirer") (v "0.3.1") (d (list (d (n "console") (r "^0.10.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.5.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)))) (h "0brql1yi3z7yjx12ij82mp9grwja0ld7aifj7q1bm1bwjp1xniv0")))

(define-public crate-enquirer-0.4.0 (c (n "enquirer") (v "0.4.0") (d (list (d (n "console") (r "^0.11.2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)))) (h "11zbznd4lpw51hrh85nv1sydjj8r4rbcjf2nvy0ks0vik7icjq4a")))

(define-public crate-enquirer-0.5.0 (c (n "enquirer") (v "0.5.0") (d (list (d (n "dialoguer") (r "^0.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)))) (h "0n94595zm160y452hnbmz5dflw4ma508pc0wynkmidxzm7h0zf9m")))

(define-public crate-enquirer-0.5.1 (c (n "enquirer") (v "0.5.1") (d (list (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)))) (h "0n6v0gzgprx6j8nsq6s9lpbvhizfjy8drhbzfwqxx828dj3fk41g")))

(define-public crate-enquirer-0.6.0 (c (n "enquirer") (v "0.6.0") (d (list (d (n "clap") (r "~3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)))) (h "0sjhivr1d1xr9mkjy60alm3xrd05q87j2b8rpwx7wrwbnlsg9d45")))

(define-public crate-enquirer-0.6.1 (c (n "enquirer") (v "0.6.1") (d (list (d (n "clap") (r "~3.1.0") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)))) (h "0r51b0i526jrndjwyrvamw1f3bqwdb7xhpvqghlgxr31vcli3li9")))

