(define-module (crates-io en qu enquote) #:use-module (crates-io))

(define-public crate-enquote-1.0.0 (c (n "enquote") (v "1.0.0") (h "0s2k14brn9cl1dp45h65srbfd7gqv4ma35xcwscah2qxds9rp3ys")))

(define-public crate-enquote-1.0.1 (c (n "enquote") (v "1.0.1") (h "1d39i6haq7vc0fn82jrzjjc9hpkg5q1ly0jf5388asyflsg6l3bs")))

(define-public crate-enquote-1.0.2 (c (n "enquote") (v "1.0.2") (h "121kljw0yrg6wvv8k6ddksvidfh649lf8gmpz4hlc2z7m4yy8fx0")))

(define-public crate-enquote-1.0.3 (c (n "enquote") (v "1.0.3") (h "0vm687r2wwgc3d3l2iqhag9wgkql6k93sdvjxvmfkdpksajpij1f")))

(define-public crate-enquote-1.1.0 (c (n "enquote") (v "1.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0clrjghlfkkb7sndabs5wch0fz2nif6nj4b117s8kqxx3nqnrhq6")))

