(define-module (crates-io en um enum-ref-macro) #:use-module (crates-io))

(define-public crate-enum-ref-macro-0.0.0 (c (n "enum-ref-macro") (v "0.0.0") (h "19vm420mlr26zpdfvvhsyzav8kjgywalx7x94lm05lyyfbrls10j")))

(define-public crate-enum-ref-macro-0.1.0 (c (n "enum-ref-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (d #t) (k 0)))) (h "0chik19kqcn4k3xfskabx0yqrlh0kdjz3v9pk4pzfq6i91jwp332")))

