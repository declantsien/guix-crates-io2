(define-module (crates-io en um enum_pipeline) #:use-module (crates-io))

(define-public crate-enum_pipeline-0.1.0 (c (n "enum_pipeline") (v "0.1.0") (d (list (d (n "enum_pipeline_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1cxi46pi5ddn6lrcl41g7fsrsfj9s28i6lv5bsb83ap78faa801s") (y #t)))

(define-public crate-enum_pipeline-2.0.0 (c (n "enum_pipeline") (v "2.0.0") (d (list (d (n "enum_pipeline_derive") (r "^2.0.0") (d #t) (k 0)))) (h "00iaw5r2k1l2f1bkcz4lihya08zkgcrcgw25brp4s72j2z4jh172")))

