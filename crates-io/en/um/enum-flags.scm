(define-module (crates-io en um enum-flags) #:use-module (crates-io))

(define-public crate-enum-flags-0.1.0 (c (n "enum-flags") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gw46674qqphfiqx3cb8lgbw9npf61islf2apm455a6kwi85ida1")))

(define-public crate-enum-flags-0.1.1 (c (n "enum-flags") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0g59vcs8z7zhqm68slsq9f1sdn291jhyv2cy927qyibacqzmrpxp") (y #t)))

(define-public crate-enum-flags-0.1.2 (c (n "enum-flags") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "185876173f2w5pyw1gy89zdp7n7bd4i91cj39719kfaryb5bg2zg") (y #t)))

(define-public crate-enum-flags-0.1.3 (c (n "enum-flags") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kyld5n7f3nyir1ba68wxj80f35q6mqf75pwncsv20zdp5lh91yb") (y #t)))

(define-public crate-enum-flags-0.1.4 (c (n "enum-flags") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08v9vrssnw49573x5z7ayg05brqsgby45z33lf3qn08qpsw3bqps") (y #t)))

(define-public crate-enum-flags-0.1.5 (c (n "enum-flags") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cs2314g7mv58c92nac0zlqpkyiwzrii1qc94wfvxblw191nnfmr") (y #t)))

(define-public crate-enum-flags-0.1.6 (c (n "enum-flags") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ban90nz5g9xm62ha8q6msajf7zpgalnwjgjad2530zf157lb17c") (y #t)))

(define-public crate-enum-flags-0.1.7 (c (n "enum-flags") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xi5zsc8w25dbmq1z37sd66d8qh3fjgqy6y9ixxj9llshqv5gg1a")))

(define-public crate-enum-flags-0.1.8 (c (n "enum-flags") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xgq809mvmypdm1rfrgjnbpaxbm9n05x4b50i2855xb1iqrd50in")))

(define-public crate-enum-flags-0.3.0-preview (c (n "enum-flags") (v "0.3.0-preview") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wkzamnlajljxp52fkxwvd7k2sacp9n09i01wk2cjnwpy9wg6zkg")))

(define-public crate-enum-flags-0.3.0-preview1 (c (n "enum-flags") (v "0.3.0-preview1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0f8db6bkylfhlzzi4sn9xw70qfybs0pd5qgsvigj1wv742gps4l3") (y #t)))

(define-public crate-enum-flags-0.3.0-preview2 (c (n "enum-flags") (v "0.3.0-preview2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ss0kbvqid6a73b6r3v5k6b2a9i9xw4cs1y27h93ghjlkvc7v8k0") (y #t)))

(define-public crate-enum-flags-0.3.0-preview3 (c (n "enum-flags") (v "0.3.0-preview3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "063smzvgg4zgfc2xiwmsldp80qy3g9i1nivl4x60jvi9nm1vidrb")))

(define-public crate-enum-flags-0.3.0 (c (n "enum-flags") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cq2c7w1cim5g26cs24ml766l45cy4j9ypdq0x2x2iaqmsa7z7g4")))

