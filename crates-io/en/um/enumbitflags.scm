(define-module (crates-io en um enumbitflags) #:use-module (crates-io))

(define-public crate-EnumBitFlags-1.0.0 (c (n "EnumBitFlags") (v "1.0.0") (h "1hz5w8vw2rb67zz9fd1xfsk66lrahbs0rigvwfjiixzjdb93yznm")))

(define-public crate-EnumBitFlags-1.0.1 (c (n "EnumBitFlags") (v "1.0.1") (h "0dcwg1qzv702xb87gv0x3md6viqmvrxjnmwh38q365hrx9m7ayv1")))

(define-public crate-EnumBitFlags-1.0.2 (c (n "EnumBitFlags") (v "1.0.2") (h "07018ip5w86my6fds50bwrwlbxiirzzqvd9mvgjyjg4dj0vrlksp")))

(define-public crate-EnumBitFlags-1.0.3 (c (n "EnumBitFlags") (v "1.0.3") (h "0mgsi49swg0ms4idnwy4iwwki1iwlvz765kqx0ks73j9hqmbsb7z")))

(define-public crate-EnumBitFlags-1.0.4 (c (n "EnumBitFlags") (v "1.0.4") (h "0xhq41f4r0z0pb8vnhakc7w7nb6by8kmhbhh20156wlbzp2d03np")))

(define-public crate-EnumBitFlags-1.0.5 (c (n "EnumBitFlags") (v "1.0.5") (h "1c2pk7vvhday389da3khg2frdpr24fm3jvqrrjhn68rwhqnh6bf2")))

(define-public crate-EnumBitFlags-1.0.6 (c (n "EnumBitFlags") (v "1.0.6") (h "0zsxl9fprwv8z0wjriazm5zgmgxf9sv8dsdma5f30cs48031b9pa")))

(define-public crate-EnumBitFlags-1.0.7 (c (n "EnumBitFlags") (v "1.0.7") (h "00g6rgfpshck84g84bbbncqgy9vq2q13masnih0m5cq3xyfggz1y")))

