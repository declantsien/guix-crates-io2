(define-module (crates-io en um enum_delegate_lib) #:use-module (crates-io))

(define-public crate-enum_delegate_lib-0.1.0 (c (n "enum_delegate_lib") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.101") (f (quote ("full"))) (d #t) (k 0)))) (h "1m06xkvls2ciba1laqz05bq9s6wwwrl3462wxmhrvpka1cr8b0vl")))

(define-public crate-enum_delegate_lib-0.2.0 (c (n "enum_delegate_lib") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.101") (f (quote ("full"))) (d #t) (k 0)))) (h "1zlhkjiras4yin0snicwaf9s08m38lm9w0qj02zac15k00w6q7rf")))

