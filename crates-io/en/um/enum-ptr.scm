(define-module (crates-io en um enum-ptr) #:use-module (crates-io))

(define-public crate-enum-ptr-0.1.0 (c (n "enum-ptr") (v "0.1.0") (d (list (d (n "enum-ptr-derive") (r "^0.1") (d #t) (k 0)))) (h "1601s87i33nkg2cc77sr18za3ba73bskw4na05q4ysi2qnxjgpf1") (f (quote (("std") ("default" "std"))))))

(define-public crate-enum-ptr-0.1.1 (c (n "enum-ptr") (v "0.1.1") (d (list (d (n "enum-ptr-derive") (r "^0.1") (d #t) (k 0)))) (h "0gzxjhvpa473cjvf4014jgxgsz0z98xs0m8ql5a4l9a9jdq3b3hc") (f (quote (("std") ("default" "std"))))))

(define-public crate-enum-ptr-0.1.2 (c (n "enum-ptr") (v "0.1.2") (d (list (d (n "enum-ptr-derive") (r "^0.1") (d #t) (k 0)))) (h "1fgjmj2p2vcn07mf5cx3pav2fp6hkhncqmfspk4nyj8lrydljwpx") (f (quote (("std") ("default" "std"))))))

(define-public crate-enum-ptr-0.1.3 (c (n "enum-ptr") (v "0.1.3") (d (list (d (n "enum-ptr-derive") (r "^0.1") (d #t) (k 0)))) (h "177gzglz1zgvkixil5gryffa7gzfsijc2hw4psb4jp6jsawmxxs3") (f (quote (("std") ("default" "std"))))))

(define-public crate-enum-ptr-0.1.4 (c (n "enum-ptr") (v "0.1.4") (d (list (d (n "enum-ptr-derive") (r "^0.1") (d #t) (k 0)))) (h "1ng5g6vqx1sc1418q62zxkfqi0ix3316mzzxahw33xq34jjnm2hp") (f (quote (("std") ("default" "std"))))))

(define-public crate-enum-ptr-0.1.5 (c (n "enum-ptr") (v "0.1.5") (d (list (d (n "enum-ptr-derive") (r "^0.1.5") (d #t) (k 0)))) (h "1kjd3ijg5m3x980a5yiyzf4s2lffgma3v6dad8jkbsv6vg298w52") (f (quote (("std") ("default" "std"))))))

(define-public crate-enum-ptr-0.1.6 (c (n "enum-ptr") (v "0.1.6") (d (list (d (n "enum-ptr-derive") (r "^0.1.6") (d #t) (k 0)))) (h "0xw0084679n0nlnd2ys7bsfd3cfnly5aw6jvx4ddm0lhnn5b8p65") (f (quote (("std") ("default" "std"))))))

(define-public crate-enum-ptr-0.1.7 (c (n "enum-ptr") (v "0.1.7") (d (list (d (n "enum-ptr-derive") (r "^0.1.7") (d #t) (k 0)))) (h "0xd0v6br0vqzp81zv21a4hy2k2s2jhi8jjm53d420m116bpn54g8") (f (quote (("std") ("default" "std"))))))

(define-public crate-enum-ptr-0.1.8 (c (n "enum-ptr") (v "0.1.8") (d (list (d (n "enum-ptr-derive") (r "^0.1.8") (d #t) (k 0)))) (h "1367s7ijwkrkbsy2fn18s6spyywm86n7kpwwcgw1d5aw2rzc1ym8") (f (quote (("std") ("default" "std"))))))

(define-public crate-enum-ptr-0.2.0-beta.0 (c (n "enum-ptr") (v "0.2.0-beta.0") (d (list (d (n "enum-ptr-derive") (r "^0.2.0-beta.0") (d #t) (k 0)))) (h "072ka4sqknxd5x8466fkik0jha8965rvryd4641qmgxj5dp6g05j") (f (quote (("default" "alloc") ("alloc"))))))

