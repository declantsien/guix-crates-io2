(define-module (crates-io en um enum-as-inner) #:use-module (crates-io))

(define-public crate-enum-as-inner-0.1.0 (c (n "enum-as-inner") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1f471ah92ljvrdpqh641d5547hz024047n216cnh83kq7kskhgxm")))

(define-public crate-enum-as-inner-0.2.0 (c (n "enum-as-inner") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1f79s87mx8qbfm018cryv4njppabr5r9ambgbnpbmhbag5d5phwl")))

(define-public crate-enum-as-inner-0.2.1 (c (n "enum-as-inner") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0zg3h7k3g1z7a9ayqy63sk302d4dg5g2h274ddv80mj4jxn2cn1x")))

(define-public crate-enum-as-inner-0.3.0 (c (n "enum-as-inner") (v "0.3.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bj914fh5b57j1ij66m021l9pdhvp1bnpwpahhl4qgsjprznq2lh")))

(define-public crate-enum-as-inner-0.3.1 (c (n "enum-as-inner") (v "0.3.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0azgmyzzkqy872y91xwk9gis4038sx2z0bssq7kx1vp5sz1h1sza")))

(define-public crate-enum-as-inner-0.3.2 (c (n "enum-as-inner") (v "0.3.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "177c4lknbm1aw11qh5lbcqck69ik3wn20m8xkl833lk1rgxgqjxw")))

(define-public crate-enum-as-inner-0.3.3 (c (n "enum-as-inner") (v "0.3.3") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "15gmpgywijda93lkq7hf2y53h66sqkhzabzbxich288xm6b00pvw")))

(define-public crate-enum-as-inner-0.3.4 (c (n "enum-as-inner") (v "0.3.4") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1m2l8zh0yd7i95qkyha86ca8m0bnhfimv38dr3n4p41yh6di03ap")))

(define-public crate-enum-as-inner-0.4.0 (c (n "enum-as-inner") (v "0.4.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wwvjmy2bvqqc3pyylsmlqpkswxrzsg40xva7z27szva8j0svk91")))

(define-public crate-enum-as-inner-0.5.0 (c (n "enum-as-inner") (v "0.5.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "10r2avbhrq45dgglx9bf6g83xb3ik94g0va4ydk6dbbpj1708bs3")))

(define-public crate-enum-as-inner-0.5.1 (c (n "enum-as-inner") (v "0.5.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05m1frlkgrikja714zxb97i00rhj36zlifiwiby7ymkx0jx0nwn9")))

(define-public crate-enum-as-inner-0.5.2 (c (n "enum-as-inner") (v "0.5.2") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0k0inrac5mbsjid1g6i7nvq5jii9m86h36yh85yvd2mcbw93hr9m") (y #t)))

(define-public crate-enum-as-inner-0.6.0 (c (n "enum-as-inner") (v "0.6.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0sjl5z0ycicpxg88qnn57m6sxi3ny9fl7b7vz0pb61bcjsvcpz2z")))

