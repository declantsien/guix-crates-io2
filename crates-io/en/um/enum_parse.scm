(define-module (crates-io en um enum_parse) #:use-module (crates-io))

(define-public crate-enum_parse-0.1.0 (c (n "enum_parse") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7") (f (quote ("derive"))) (d #t) (k 2)))) (h "1mr6yl576a9556zhsygln10izwjg3xcrn9i0p9zlc1rrahks8s5k")))

