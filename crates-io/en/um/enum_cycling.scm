(define-module (crates-io en um enum_cycling) #:use-module (crates-io))

(define-public crate-enum_cycling-0.1.0 (c (n "enum_cycling") (v "0.1.0") (d (list (d (n "enum_cycling_derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "enum_cycling_derive") (r "^0.1.0") (d #t) (k 2)))) (h "139rzf2gpml1wdzh75sk2n177k9r3gsjrsrjwkgp12x0xbah7hyk") (f (quote (("derive" "enum_cycling_derive"))))))

(define-public crate-enum_cycling-0.1.1 (c (n "enum_cycling") (v "0.1.1") (d (list (d (n "enum_cycling_derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "enum_cycling_derive") (r "^0.1.0") (d #t) (k 2)))) (h "0bjr6z0wxysy7s28c8skfki4pc3i4zn0p5l1fdgmgh0nixasczkn") (f (quote (("derive" "enum_cycling_derive"))))))

