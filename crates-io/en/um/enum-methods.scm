(define-module (crates-io en um enum-methods) #:use-module (crates-io))

(define-public crate-enum-methods-0.0.1 (c (n "enum-methods") (v "0.0.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "08h0qqjf39v8294f1grc2kgfpkb0156dh1viw7axxrcbv8wpkccb")))

(define-public crate-enum-methods-0.0.2 (c (n "enum-methods") (v "0.0.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1w91ap32r8wrsqidw4wz6h2z0p3i7v69aa8cm9fpqg7g2nmmg62s")))

(define-public crate-enum-methods-0.0.3 (c (n "enum-methods") (v "0.0.3") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "18vswqw92x68vhmf8wj3wp81xrad1mv4jqsd00vj0jmx1nqabd9g")))

(define-public crate-enum-methods-0.0.4 (c (n "enum-methods") (v "0.0.4") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "07ayx3qq1mfqgsl7yr9sfymabw1nw1gpp3bflyf9z9nshlbd3nbw")))

(define-public crate-enum-methods-0.0.5 (c (n "enum-methods") (v "0.0.5") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1s2ya62hfj8iz8b58i8yjv647wm300w79l1386yhzrwfddzgfxnq")))

(define-public crate-enum-methods-0.0.6 (c (n "enum-methods") (v "0.0.6") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1avmn3qv824wz7m49yd8z6dvbc604385kidbsz9mbd01g5sy6g8z")))

(define-public crate-enum-methods-0.0.7 (c (n "enum-methods") (v "0.0.7") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1bp4shq2i77y74na4bwv35vqfff1v5dys0y0cx8d1w2s1w669iql")))

(define-public crate-enum-methods-0.0.8 (c (n "enum-methods") (v "0.0.8") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "045v6af8jy9dfghxv1ppi7lyjizjbdmqszj6zkbddc2c5pdfg63p")))

