(define-module (crates-io en um enumflags) #:use-module (crates-io))

(define-public crate-enumflags-0.1.0 (c (n "enumflags") (v "0.1.0") (h "0iini2nl1vx72wb5y93al9hzndn45kz8icjvs06h47x0cl4kydg5")))

(define-public crate-enumflags-0.1.1 (c (n "enumflags") (v "0.1.1") (h "1z7cw2bssanbz2nmmlnxpbw8jmnh1fhm1llnn3r4ljccpzw2lsib")))

(define-public crate-enumflags-0.1.2 (c (n "enumflags") (v "0.1.2") (h "1hpkdqmiy4x4axhcx81w0v7dbaahdg4440da8lv8gpyqziqzf0xh")))

(define-public crate-enumflags-0.2.0 (c (n "enumflags") (v "0.2.0") (d (list (d (n "num") (r "^0.1.41") (d #t) (k 0)))) (h "1cfrl8sf210q1njnaa42rqc8skgpgikqs6ijlvkwffsinski7z9d") (f (quote (("nostd") ("default"))))))

(define-public crate-enumflags-0.2.1 (c (n "enumflags") (v "0.2.1") (d (list (d (n "num") (r "^0.1.41") (d #t) (k 0)))) (h "1fjnwp43pqf1g1f8x4m7pvhizr357wcsmw7g9yq8w51cv8wyzy08") (f (quote (("nostd") ("default"))))))

(define-public crate-enumflags-0.3.0 (c (n "enumflags") (v "0.3.0") (h "1v6m8ss9pl1q5ra5cym53z158fh5szip7qfvm40037djcydnqa6a") (f (quote (("nostd") ("default"))))))

(define-public crate-enumflags-0.4.1 (c (n "enumflags") (v "0.4.1") (h "0w33rcjn1r436w9a51nhd8acsy1j9piwh1mbal6xhilmdiylnsp1") (f (quote (("nostd") ("default"))))))

(define-public crate-enumflags-0.3.1 (c (n "enumflags") (v "0.3.1") (h "0iryilbqp8w0a5r5i0lklglgh160b55vfpzfh7pmp6bq2b8cp250") (f (quote (("nostd") ("default"))))))

(define-public crate-enumflags-0.3.2 (c (n "enumflags") (v "0.3.2") (h "0v2hv3avsm07y344554avzavab80zh4wjl1hwy2qg0b1s0aiy17m") (f (quote (("nostd") ("default"))))))

(define-public crate-enumflags-0.4.2 (c (n "enumflags") (v "0.4.2") (h "0afwkl7rk6z6fhdafhxz11xx14vgamd0y2cj2njf49h21rrwsyj1") (f (quote (("nostd") ("default"))))))

