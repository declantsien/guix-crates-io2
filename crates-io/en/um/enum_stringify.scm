(define-module (crates-io en um enum_stringify) #:use-module (crates-io))

(define-public crate-enum_stringify-0.1.0 (c (n "enum_stringify") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.32") (d #t) (k 0)))) (h "1pa8vnb8cz636xlb9y758ng447avbqa7mxp6nfq1xgpgxx79pcbw")))

(define-public crate-enum_stringify-0.2.0 (c (n "enum_stringify") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0.32") (d #t) (k 0)))) (h "018h2ryg2i4dvbfxwa3vmprx32izdvfyr0q7cj6dfblaywq0d0bw")))

(define-public crate-enum_stringify-0.3.0 (c (n "enum_stringify") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0.32") (d #t) (k 0)))) (h "0chfv12cvn8ahrrwmfniiv2kb0b9dg78d888v78karn9l937f60w")))

(define-public crate-enum_stringify-0.4.0 (c (n "enum_stringify") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.34") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0.45") (d #t) (k 0)))) (h "0cyiz9rwws4jiq042g1lyxrfc25rc5gbv8hza1cs841zamri2f7f")))

(define-public crate-enum_stringify-0.4.1 (c (n "enum_stringify") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.34") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0.45") (d #t) (k 0)))) (h "17hcgggjf8g0iksvlg2vdm2z2qcx222zq30y440rsff7khnwn9fs")))

(define-public crate-enum_stringify-0.4.2 (c (n "enum_stringify") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "051c6d7jniz587mnsvldh7nandwvlfbck6g8flr037xvm6spjv0m")))

