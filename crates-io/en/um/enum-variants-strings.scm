(define-module (crates-io en um enum-variants-strings) #:use-module (crates-io))

(define-public crate-enum-variants-strings-0.1.0 (c (n "enum-variants-strings") (v "0.1.0") (d (list (d (n "enum-variants-strings-derive") (r "^0.1.0") (d #t) (k 0)))) (h "10870h7ljz8bl73bylapgw210bkbzrvi2x3dkywvyzmsp3v48vy0")))

(define-public crate-enum-variants-strings-0.2.0 (c (n "enum-variants-strings") (v "0.2.0") (d (list (d (n "enum-variants-strings-derive") (r "^0.1.0") (d #t) (k 0)))) (h "11mdzygkmjzkyzyj7sqg3l1rlws0jy01zdmyl62czfip0v5va897")))

(define-public crate-enum-variants-strings-0.2.1 (c (n "enum-variants-strings") (v "0.2.1") (d (list (d (n "enum-variants-strings-derive") (r "^0.2.1") (d #t) (k 0)))) (h "00dj4m1g9yj07hb2jz7wbx7z198pmk2y56zm30xzaiz4v3vcixz7")))

(define-public crate-enum-variants-strings-0.2.2 (c (n "enum-variants-strings") (v "0.2.2") (d (list (d (n "enum-variants-strings-derive") (r "^0.2.2") (d #t) (k 0)))) (h "1zb56759xj4fnbdlw16pmy07rpppdvg4x1kkv8q1sgdw4sfp9vyc")))

(define-public crate-enum-variants-strings-0.2.3 (c (n "enum-variants-strings") (v "0.2.3") (d (list (d (n "enum-variants-strings-derive") (r "^0.2.4") (d #t) (k 0)))) (h "18alc1wv56mq3yh5piky85sd6p207992lmbl3xj7s02qxp7w33i0")))

(define-public crate-enum-variants-strings-0.3.0 (c (n "enum-variants-strings") (v "0.3.0") (d (list (d (n "enum-variants-strings-derive") (r "^0.3.0") (d #t) (k 0)))) (h "1z4wdpi2kxs3rhmk1g8d1fldvgqm0snwnfab9xmy7kqdcmrvhy5z")))

