(define-module (crates-io en um enum-display-derive) #:use-module (crates-io))

(define-public crate-enum-display-derive-0.1.0 (c (n "enum-display-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "071zb2gg63yybx20ivk5kby43dq18wjhdw80a376zz2b7jv6xxsk")))

(define-public crate-enum-display-derive-0.1.1 (c (n "enum-display-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1jhhhg2iqj0jdd43p4wbdgzsz1783bllw58sssaj494v59xz6vpi")))

