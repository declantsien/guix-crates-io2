(define-module (crates-io en um enum_set2) #:use-module (crates-io))

(define-public crate-enum_set2-0.1.0 (c (n "enum_set2") (v "0.1.0") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "enum_like") (r "^0.2") (d #t) (k 0)))) (h "1l14qr5awwnwdakq9hxqs64vzwqcp1y8lg88c9x1xqzds4vii2l8")))

(define-public crate-enum_set2-0.1.1 (c (n "enum_set2") (v "0.1.1") (d (list (d (n "bit-set") (r "^0.5") (d #t) (k 0)) (d (n "enum_like") (r "^0.2") (d #t) (k 0)))) (h "1wqh2bgy68gk6g7j79wwpkc9bp28n0cwx21h0n51i1gpj11mv308")))

