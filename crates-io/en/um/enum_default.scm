(define-module (crates-io en um enum_default) #:use-module (crates-io))

(define-public crate-enum_default-0.1.0 (c (n "enum_default") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1p7c3mmz3vp85m6jpach83qjsa03i9dclasiz1h5nbd8m9qwdq4d") (y #t)))

(define-public crate-enum_default-0.1.1 (c (n "enum_default") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1f9hsmpgwk8g45fmyj941ysq7r8ia293qrjd4611rik4zb7qif47") (y #t)))

(define-public crate-enum_default-0.1.2 (c (n "enum_default") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04gyygjdq8gpdrlsc77kcxb20lcvckbqx6paa9r325w50i0h9ik8") (y #t)))

(define-public crate-enum_default-0.2.2 (c (n "enum_default") (v "0.2.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "004h17b9ik0k3xxzzbhn2izzl9l1c14s5qzqnx35zpydqamswnd7") (y #t)))

(define-public crate-enum_default-0.2.3 (c (n "enum_default") (v "0.2.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "179r9ivg5sjan6n6bcxy6rfwvsfkxqm87dmbcn24b9bzgc27i5da") (y #t)))

(define-public crate-enum_default-0.2.4 (c (n "enum_default") (v "0.2.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rgxiv37kmkgm686ff4lzfl17lwh4ld8g4c0aipsbc3pncr0zrbf") (y #t)))

(define-public crate-enum_default-0.2.5 (c (n "enum_default") (v "0.2.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1k3jmiyaaxlmngl3rlisc6c7sm8nirxld0bnxl88pahcgv0qd8c0") (y #t)))

(define-public crate-enum_default-0.2.6 (c (n "enum_default") (v "0.2.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1hxgxpxms4glcwp6k50ibif7i2csd22hg9i9if8072cy96gzs0r0")))

