(define-module (crates-io en um enum_index_derive) #:use-module (crates-io))

(define-public crate-enum_index_derive-0.1.0 (c (n "enum_index_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "05ah5lrx2lkbviq06yib9cxw3aaznnqk0bfw4g126lr42x0q4wsa")))

(define-public crate-enum_index_derive-0.1.1 (c (n "enum_index_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "05k1vly2k54xhyddm1blj5imcgj6z022fslgij7wg08m7fr1yazg")))

(define-public crate-enum_index_derive-0.2.0 (c (n "enum_index_derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1r2fjimz56cq5cm9fnxf0nxwv7hldrawlg8ij1hz12slhn02rcla")))

