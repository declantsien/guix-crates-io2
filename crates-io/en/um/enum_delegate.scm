(define-module (crates-io en um enum_delegate) #:use-module (crates-io))

(define-public crate-enum_delegate-0.1.0 (c (n "enum_delegate") (v "0.1.0") (d (list (d (n "enum_delegate_lib") (r "^0.1.0") (d #t) (k 0)) (d (n "impls") (r "^1.0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.101") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.71") (d #t) (k 2)))) (h "124rwv2vgjfaay1jdfpi3kqkkrch2rdb4bbg8aq5n17vgpyhy4qn")))

(define-public crate-enum_delegate-0.2.0 (c (n "enum_delegate") (v "0.2.0") (d (list (d (n "enum_delegate_lib") (r "^0.2.0") (d #t) (k 0)) (d (n "impls") (r "^1.0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.101") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.71") (d #t) (k 2)))) (h "02nd2klkv33pir3n53zq2pljfhv8fc6r8dz0mx1s1jr223rpbsm8")))

