(define-module (crates-io en um enum-derived-macro) #:use-module (crates-io))

(define-public crate-enum-derived-macro-0.1.0 (c (n "enum-derived-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("derive"))) (d #t) (k 0)))) (h "08r15xq6v7wc6jqjzh6f9zhl3qmj6hfka5a24wyll63wbhw9s9f6")))

(define-public crate-enum-derived-macro-0.1.1 (c (n "enum-derived-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("derive"))) (d #t) (k 0)))) (h "170b7kz9kcfhmhbz9imagfdw07rbnm6bmjr62smga99a1l581zwn")))

(define-public crate-enum-derived-macro-0.2.0 (c (n "enum-derived-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "1f50nca0v65gqy7y7i60wsnjnblaggyj19zfj6pl3101785fzlzz")))

