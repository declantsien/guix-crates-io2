(define-module (crates-io en um enum-ptr-derive) #:use-module (crates-io))

(define-public crate-enum-ptr-derive-0.1.0 (c (n "enum-ptr-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1bxpszpxn6fixv5a38p3yngqk1992sfgklrcgzya2s0qvap849ip")))

(define-public crate-enum-ptr-derive-0.1.1 (c (n "enum-ptr-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0sndiiy4wwy0a2bjmmywxg8f9x0pggdmgh9fysmrcqdyhwiq5ljb")))

(define-public crate-enum-ptr-derive-0.1.2 (c (n "enum-ptr-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "14v4kvl4m6npvx3yh7maailzjjrzp4s9hhy8gm9116651ybkk336")))

(define-public crate-enum-ptr-derive-0.1.3 (c (n "enum-ptr-derive") (v "0.1.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1hzqnih497qd7dbjhlry4h3jr5ivvmx0lihb0z78gkvd0jrch833")))

(define-public crate-enum-ptr-derive-0.1.4 (c (n "enum-ptr-derive") (v "0.1.4") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1fils5qc3i9rkdkl5jjk9pww32sz3cqicmvf6q2vdvq24qqnz6ms")))

(define-public crate-enum-ptr-derive-0.1.5 (c (n "enum-ptr-derive") (v "0.1.5") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1xk9xqqkf3m53p8gyk255d5l9ky6zjbbcq85p0538fxxn7vhxwb4")))

(define-public crate-enum-ptr-derive-0.1.6 (c (n "enum-ptr-derive") (v "0.1.6") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1ca1j99v1xqazg7df7r50x5fp71c109d62q1m35id26lx99w7yza")))

(define-public crate-enum-ptr-derive-0.1.7 (c (n "enum-ptr-derive") (v "0.1.7") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1z7z5sb2lwyaqkagk5kks34mq4hipp7wj5q1zqqby2v5kbpyld3g")))

(define-public crate-enum-ptr-derive-0.1.8 (c (n "enum-ptr-derive") (v "0.1.8") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "175n3nwvlafpr4aandxjpymqhp78s7krxzhzxsldn92wrqc6j6ac")))

(define-public crate-enum-ptr-derive-0.2.0-beta.0 (c (n "enum-ptr-derive") (v "0.2.0-beta.0") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1xsnfbkxvbjrwgdf83szd6lv19wyfkfkdnfl2rxfb9khb1k08mpv")))

