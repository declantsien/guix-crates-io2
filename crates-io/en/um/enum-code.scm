(define-module (crates-io en um enum-code) #:use-module (crates-io))

(define-public crate-enum-code-0.1.0 (c (n "enum-code") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "09n78g2kqk44ll2cw11yj7s4ppfn34m958mr69xvahawks6dqcaj")))

(define-public crate-enum-code-0.1.1 (c (n "enum-code") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "044z4l9dbwix6ba9s6qsyhlzqhsn36915i7cmi4k0gvpdjhq5qws")))

