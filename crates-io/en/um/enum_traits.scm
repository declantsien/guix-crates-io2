(define-module (crates-io en um enum_traits) #:use-module (crates-io))

(define-public crate-enum_traits-0.1.0 (c (n "enum_traits") (v "0.1.0") (h "03npl7vnjrslm7sj8b95h7k1sy9dv5nbxbsjxixgd202mwn4p0bh")))

(define-public crate-enum_traits-0.1.1 (c (n "enum_traits") (v "0.1.1") (h "1zkxrdahcsk9zf0bxb32d0awxn549icxwpc085if2h64rkwk872d")))

(define-public crate-enum_traits-0.1.2 (c (n "enum_traits") (v "0.1.2") (h "1nrb3a79i3ghkmnaramqy5kg0bdbypkpk0qdv88n8z0vdbg63yyn")))

(define-public crate-enum_traits-0.2.0 (c (n "enum_traits") (v "0.2.0") (h "17vwpdm4hrjp6ykgxiicx6srzwvx3xcfk0fpjcdj57nal21aili8")))

(define-public crate-enum_traits-0.3.0 (c (n "enum_traits") (v "0.3.0") (h "1cshbqrcq9dwsa33a6p0r18112hhf7ab4lnsm6n66app1d64m38r")))

