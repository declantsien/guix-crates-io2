(define-module (crates-io en um enumflags2) #:use-module (crates-io))

(define-public crate-enumflags2-0.5.0 (c (n "enumflags2") (v "0.5.0") (d (list (d (n "enumflags2_derive") (r "^0.5.0") (d #t) (k 2)))) (h "1s35vfcdb876q7mhw1p1ijkg03vzh150xmwnl4822bg0ffk064w0") (f (quote (("nostd") ("default"))))))

(define-public crate-enumflags2-0.5.1 (c (n "enumflags2") (v "0.5.1") (d (list (d (n "enumflags2_derive") (r "^0.5.0") (d #t) (k 2)))) (h "1rcvbv730b2kr2m1gagwbch9dm2jqzjsrpk4aa4gkfp46jw3d6qv") (f (quote (("nostd") ("default"))))))

(define-public crate-enumflags2-0.5.2 (c (n "enumflags2") (v "0.5.2") (d (list (d (n "enumflags2_derive") (r "^0.5.0") (d #t) (k 2)))) (h "0qvmdw7w5j9pz92ljx8lvy74b554m7myg8m14r41qqa1s52a8c4c") (f (quote (("nostd") ("default"))))))

(define-public crate-enumflags2-0.6.0 (c (n "enumflags2") (v "0.6.0") (d (list (d (n "enumflags2_derive") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (k 0)))) (h "173sicnasij2yx4slm286d0i5vih6y0y8p061w2br4jjvf3919f7") (f (quote (("std"))))))

(define-public crate-enumflags2-0.6.1 (c (n "enumflags2") (v "0.6.1") (d (list (d (n "enumflags2_derive") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (k 0)))) (h "124fphzr0rwwc8zil5636ig0pxhryhfrl96dklgpd8k2wwn4f618") (f (quote (("std"))))))

(define-public crate-enumflags2-0.6.2 (c (n "enumflags2") (v "0.6.2") (d (list (d (n "enumflags2_derive") (r "^0.6.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (k 0)))) (h "1y3a94w2sfcgp3jrj5asqrnp3a16n08r7cns6aiqp55sha3iq4ik") (f (quote (("std") ("not_literal" "enumflags2_derive/not_literal"))))))

(define-public crate-enumflags2-0.6.3 (c (n "enumflags2") (v "0.6.3") (d (list (d (n "enumflags2_derive") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (k 0)))) (h "031v5a5dik3s7wcgalrjczygzir1hw0l8yaygssqahhrpx7543m8") (f (quote (("std") ("not_literal" "enumflags2_derive/not_literal"))))))

(define-public crate-enumflags2-0.6.4 (c (n "enumflags2") (v "0.6.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "enumflags2_derive") (r "= 0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (k 0)))) (h "182xd6cxxmadx1axnz6x73d12pzgwkc712zq2lxd4z1k48lxij43") (f (quote (("std") ("not_literal" "enumflags2_derive/not_literal"))))))

(define-public crate-enumflags2-0.7.0-preview1 (c (n "enumflags2") (v "0.7.0-preview1") (d (list (d (n "enumflags2_derive") (r "=0.7.0-preview1") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (k 0)))) (h "0flchjpb4v7wh9vm4jxnkc4vd926156wgwdi9bxikq73l1rjqhbp") (f (quote (("std"))))))

(define-public crate-enumflags2-0.7.0 (c (n "enumflags2") (v "0.7.0") (d (list (d (n "enumflags2_derive") (r "=0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (k 0)))) (h "1sjkvzhmwxihmh03mm1mwkqcms75fxdr1hixyvsa0m85hjp8w6gr") (f (quote (("std")))) (y #t)))

(define-public crate-enumflags2-0.7.1 (c (n "enumflags2") (v "0.7.1") (d (list (d (n "enumflags2_derive") (r "=0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (k 0)))) (h "0c8ibfjpvrfjrzkj5h7kr19i5b164cy7573fbwixvzs2srbj4rx8") (f (quote (("std")))) (y #t)))

(define-public crate-enumflags2-0.7.2 (c (n "enumflags2") (v "0.7.2") (d (list (d (n "enumflags2_derive") (r "=0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (k 0)))) (h "1znqwbb1qbab10kkvpyzb1q453wlw0c96wkw3iqjwkp5lkfszy8g") (f (quote (("std")))) (y #t) (r "1.41.1")))

(define-public crate-enumflags2-0.7.3 (c (n "enumflags2") (v "0.7.3") (d (list (d (n "enumflags2_derive") (r "=0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (k 0)))) (h "1vsdy8aw6cqm39z0akv7kaz0nfhdxzfvwg0qrw8l3y5kasq90p52") (f (quote (("std")))) (y #t) (r "1.41.1")))

(define-public crate-enumflags2-0.7.4 (c (n "enumflags2") (v "0.7.4") (d (list (d (n "enumflags2_derive") (r "=0.7.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (k 0)))) (h "1bljn1bnra5psq07pzpmcwj1sckxsxq60yqzbzccjllnqxyv6fhv") (f (quote (("std")))) (y #t) (r "1.41.1")))

(define-public crate-enumflags2-0.7.5 (c (n "enumflags2") (v "0.7.5") (d (list (d (n "enumflags2_derive") (r "=0.7.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (k 0)))) (h "1jswynhjjwsp6rfnyapbvhzxrf8lpfgr0f8mhd238f4m3g94qpg7") (f (quote (("std")))) (y #t) (r "1.41.1")))

(define-public crate-enumflags2-0.7.6 (c (n "enumflags2") (v "0.7.6") (d (list (d (n "enumflags2_derive") (r "=0.7.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (k 0)))) (h "0jxazzzl9bw8a568s1z7iapxhp1w2z9sjcq21hp7famvgzgyni00") (f (quote (("std")))) (y #t) (r "1.56")))

(define-public crate-enumflags2-0.7.7 (c (n "enumflags2") (v "0.7.7") (d (list (d (n "enumflags2_derive") (r "=0.7.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (k 0)))) (h "1lhvq084ylw3nvhgv1zyagavkj392zamydh5v6y352zn1l4zahf0") (f (quote (("std")))) (r "1.56")))

(define-public crate-enumflags2-0.7.8 (c (n "enumflags2") (v "0.7.8") (d (list (d (n "enumflags2_derive") (r "=0.7.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (k 0)))) (h "0f9r5i4fqj3vwzy2hm5pcl2cbnmz47w3mxkjxlxdkj900grv962r") (f (quote (("std")))) (r "1.56")))

(define-public crate-enumflags2-0.7.9 (c (n "enumflags2") (v "0.7.9") (d (list (d (n "enumflags2_derive") (r "=0.7.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (k 0)))) (h "13cfkn555q8v6rrbld8m2xjb14pnap9w1x5wv98hlpk7zgawjy1j") (f (quote (("std")))) (r "1.56")))

