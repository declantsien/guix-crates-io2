(define-module (crates-io en um enum_macro_gen) #:use-module (crates-io))

(define-public crate-enum_macro_gen-0.1.0 (c (n "enum_macro_gen") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19avjl2nygjcrcvi48b6aka7lk525cfxm4jhg9i8nfxcbvd4f91z") (f (quote (("all"))))))

(define-public crate-enum_macro_gen-0.1.1 (c (n "enum_macro_gen") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1sg40rsb4a4f9ry3p41q7z3nib6m88vciva3273l9hsgasl2jrpb") (f (quote (("all"))))))

