(define-module (crates-io en um enum-ordinalize) #:use-module (crates-io))

(define-public crate-enum-ordinalize-1.0.0 (c (n "enum-ordinalize") (v "1.0.0") (h "1hpb4lk383dip7cxvlsx4b0svvbpn18iyl0wnigx2xa8f2cipds5")))

(define-public crate-enum-ordinalize-1.0.1 (c (n "enum-ordinalize") (v "1.0.1") (h "0xz1z11z2ql8qy416czinr7vyqhl7zma5zr05563i0p75jj2rl8c")))

(define-public crate-enum-ordinalize-2.0.0 (c (n "enum-ordinalize") (v "2.0.0") (h "0axdz3c825m517bmi3rgaldpbvyc9zihl4qh3bwgmdgyvqxh8p0f")))

(define-public crate-enum-ordinalize-2.0.1 (c (n "enum-ordinalize") (v "2.0.1") (h "13cbfjgwgdm91h3nlsx3nmgbrvm24dj85kmjrf3rsizlwydqhgjl")))

(define-public crate-enum-ordinalize-2.0.2 (c (n "enum-ordinalize") (v "2.0.2") (h "1na9hnr4yj3dj4qq0cb8chy64wj3vxd1iqy0frvszl4mm6h733qy")))

(define-public crate-enum-ordinalize-2.0.3 (c (n "enum-ordinalize") (v "2.0.3") (h "1xzy8914c0dlb1yd3mlw22znl2izqd1v0iq03cwr06hdmwpwn5jz")))

(define-public crate-enum-ordinalize-2.0.4 (c (n "enum-ordinalize") (v "2.0.4") (h "0hqsaxkkk5r90wdhpyp3xxkncwmv1y2ay972k0ni4cn0zqg4xic6")))

(define-public crate-enum-ordinalize-2.0.5 (c (n "enum-ordinalize") (v "2.0.5") (h "1sbnx2sh7b5m0pc0qdp38ii1331lr4sfvw8hi18m1qzq4irrg99v")))

(define-public crate-enum-ordinalize-2.1.0 (c (n "enum-ordinalize") (v "2.1.0") (h "0ls75h52nxgr2aadjyhjvrmjwk2rx7bplydqp2scl6dcf77hbfrb")))

(define-public crate-enum-ordinalize-2.1.1 (c (n "enum-ordinalize") (v "2.1.1") (h "0pv800iszlm71gwm0gw7cnn2xadb2jny1has4lfsmb9nr04sfd0r")))

(define-public crate-enum-ordinalize-2.2.0 (c (n "enum-ordinalize") (v "2.2.0") (h "0wqmmdw13133nzbakzxj8q5d6bi6mdf5l3b0xkbq4v4jjgh88wz8")))

(define-public crate-enum-ordinalize-2.2.1 (c (n "enum-ordinalize") (v "2.2.1") (h "1c84xsj0cbibsi30ba6iw9azvibw9lgynmz46nr14bmh7ypk2n64")))

(define-public crate-enum-ordinalize-2.3.0 (c (n "enum-ordinalize") (v "2.3.0") (h "15i438zcaf3cm9s51dywpz5kc72j5vwqxn7k7h2bhrardkxn0sr6")))

(define-public crate-enum-ordinalize-2.4.0 (c (n "enum-ordinalize") (v "2.4.0") (h "123nqcpjhc0pxcf9qg1smrh0cgnaz3z0cdq2sswb6vbn6rg05g69")))

(define-public crate-enum-ordinalize-2.5.0 (c (n "enum-ordinalize") (v "2.5.0") (h "0gw4arn1bmdkxlylc719xvp4xcldl8fk842wgnjxwwxrjfvmgmv5") (f (quote (("nightly-test"))))))

(define-public crate-enum-ordinalize-2.5.1 (c (n "enum-ordinalize") (v "2.5.1") (h "1d42hd3lil1qi76lck7hsf3m6p91bafwkl3qw6il9n5kvc3rjvmh") (f (quote (("nightly-test"))))))

(define-public crate-enum-ordinalize-2.5.2 (c (n "enum-ordinalize") (v "2.5.2") (h "1hlzsf4csh7cfpzv7klsa1j712sl6qbwdryg8vgadssz0c1w9w0f") (f (quote (("nightly-test"))))))

(define-public crate-enum-ordinalize-3.0.0 (c (n "enum-ordinalize") (v "3.0.0") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "123lyfirz2b6k4j9nsll5v61qwdiy8kppnyd93svw18iwiw2nv3r") (f (quote (("nightly-test"))))))

(define-public crate-enum-ordinalize-3.0.1 (c (n "enum-ordinalize") (v "3.0.1") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0s9qd589lplqgm0j7brjwiz18zgwsq3mfdh8fzzkznkv88k4ac53") (f (quote (("nightly-test"))))))

(define-public crate-enum-ordinalize-3.1.0 (c (n "enum-ordinalize") (v "3.1.0") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1a8wy89ccai5z53fl135cpp81436mc954hi9ih84l5z2ig92lvyh") (f (quote (("nightly-test"))))))

(define-public crate-enum-ordinalize-3.1.1 (c (n "enum-ordinalize") (v "3.1.1") (d (list (d (n "if_rust_version") (r "^1.0.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1kv7jm3yjnbbkf44l7c43q2g9yq55xzy97qyshslh9j831sg74y7") (f (quote (("nightly-test"))))))

(define-public crate-enum-ordinalize-3.1.2 (c (n "enum-ordinalize") (v "3.1.2") (d (list (d (n "if_rust_version") (r "^1.0.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1b2n77jqxqk0ix7ickkik4bc2h9j2z5nss1n8najf9mcp0ay6dfw") (f (quote (("nightly-test"))))))

(define-public crate-enum-ordinalize-3.1.3 (c (n "enum-ordinalize") (v "3.1.3") (d (list (d (n "if_rust_version") (r "^1.0.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1ziv9phgyjl943293z35zlppayj7fqdhp06gk802pacdf1crx7q1") (f (quote (("nightly-test"))))))

(define-public crate-enum-ordinalize-3.1.4 (c (n "enum-ordinalize") (v "3.1.4") (d (list (d (n "if_rust_version") (r "^1.0.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0id1hk2bxk790yf4c8ynk0335bc6kgwxhn6zds7da85l8m1llm4r") (f (quote (("nightly-test"))))))

(define-public crate-enum-ordinalize-3.1.5 (c (n "enum-ordinalize") (v "3.1.5") (d (list (d (n "if_rust_version") (r "^1.0.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0rsf893pbc21nlg3cr2cgz2jlzz7h1ff7y7n06lhr8ga0hgb76c6") (f (quote (("nightly-test"))))))

(define-public crate-enum-ordinalize-3.1.6 (c (n "enum-ordinalize") (v "3.1.6") (d (list (d (n "if_rust_version") (r "^1.0.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1karwf1vbxqch4jm0g0liawg2slj6lbfpd21bagh3ills29gnv2w") (f (quote (("nightly-test"))))))

(define-public crate-enum-ordinalize-3.1.7 (c (n "enum-ordinalize") (v "3.1.7") (d (list (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0x4wyk8l4cbwvg37vz6sm8mvfm4fj1qs10q4ifi2m3ynllyi4ws6") (f (quote (("nightly-test"))))))

(define-public crate-enum-ordinalize-3.1.8 (c (n "enum-ordinalize") (v "3.1.8") (d (list (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "10zr1vsm8c3g3kcc8p2z12d84fzm3gfzx9nki3d6n8gxmpdf2xhn") (f (quote (("nightly-test"))))))

(define-public crate-enum-ordinalize-3.1.9 (c (n "enum-ordinalize") (v "3.1.9") (d (list (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "16z4p8bp21c8sv957iackb7smga2a0py7ffg3sb1cglx84wzylid") (f (quote (("nightly-test"))))))

(define-public crate-enum-ordinalize-3.1.10 (c (n "enum-ordinalize") (v "3.1.10") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1vsgl2i1gs95qjxgy8whqg5f0jpmni5n1ab6crddsq436yg6q5hb") (f (quote (("nightly-test"))))))

(define-public crate-enum-ordinalize-3.1.11 (c (n "enum-ordinalize") (v "3.1.11") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0v639grfkm9gvfmxrhd10w7jlk8q5bmdc1fxifd0g0z3zq7gqw11") (f (quote (("nightly-test"))))))

(define-public crate-enum-ordinalize-3.1.12 (c (n "enum-ordinalize") (v "3.1.12") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0sh9nny62ajd94dhpnw3n0z1kyrqlhbirjldlzzvgv25iggv2ax6") (f (quote (("nightly-test"))))))

(define-public crate-enum-ordinalize-3.1.13 (c (n "enum-ordinalize") (v "3.1.13") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1wajpwpp228xvrkzs1h6d7vq5ncr3dqc71s9yskwkvrwym96bxz4") (f (quote (("nightly-test")))) (r "1.56")))

(define-public crate-enum-ordinalize-3.1.15 (c (n "enum-ordinalize") (v "3.1.15") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1vj9iw2px39jaya7b8k0vdbrd8wjm55b3ix9n6jixzxv0qzzmw8v") (f (quote (("nightly-test")))) (r "1.56")))

(define-public crate-enum-ordinalize-4.0.0 (c (n "enum-ordinalize") (v "4.0.0") (d (list (d (n "enum-ordinalize-derive") (r "^4") (o #t) (d #t) (k 0)))) (h "08i0ahxpwy16nl3mr8gamq358vlw2cx35q7xym822znp6gar641c") (f (quote (("nightly-test") ("derive" "enum-ordinalize-derive") ("default" "derive")))) (r "1.56")))

(define-public crate-enum-ordinalize-4.1.0 (c (n "enum-ordinalize") (v "4.1.0") (d (list (d (n "enum-ordinalize-derive") (r "^4") (o #t) (d #t) (k 0)))) (h "08sqkf26sz3vx37gjfy8n5bbp4h8hw08dyqa6fcixsi0jgsbi8lq") (f (quote (("nightly-test") ("derive" "enum-ordinalize-derive") ("default" "derive")))) (r "1.56")))

(define-public crate-enum-ordinalize-4.1.1 (c (n "enum-ordinalize") (v "4.1.1") (d (list (d (n "enum-ordinalize-derive") (r "^4") (o #t) (d #t) (k 0)))) (h "0vdcynfaj7h7v9ip49j94x8wz29jkla82cnpxwm37f1n6xhvnrzi") (f (quote (("nightly-test") ("derive" "enum-ordinalize-derive") ("default" "derive")))) (r "1.56")))

(define-public crate-enum-ordinalize-4.1.2 (c (n "enum-ordinalize") (v "4.1.2") (d (list (d (n "enum-ordinalize-derive") (r "^4") (o #t) (d #t) (k 0)))) (h "0fssapd3dx6mmcq1k33017y5ify8ywcpsbnmqlb6yrn9kn6nybxv") (f (quote (("nightly-test") ("derive" "enum-ordinalize-derive") ("default" "derive")))) (r "1.56")))

(define-public crate-enum-ordinalize-4.2.0 (c (n "enum-ordinalize") (v "4.2.0") (d (list (d (n "enum-ordinalize-derive") (r "^4.2") (o #t) (k 0)))) (h "0p0rv4nrjc5wx0djk9yrbx6011s6wylsqkwfnqfcdribd9bfsrdk") (f (quote (("traits" "enum-ordinalize-derive/traits") ("nightly-test") ("derive" "enum-ordinalize-derive") ("default" "derive" "traits")))) (r "1.56")))

(define-public crate-enum-ordinalize-4.2.1 (c (n "enum-ordinalize") (v "4.2.1") (d (list (d (n "enum-ordinalize-derive") (r "^4.2.6") (o #t) (k 0)))) (h "0ydbzycxl0kais26lrk6i0riylw1ci4s065pky73vfvrnglbn9k9") (f (quote (("traits" "enum-ordinalize-derive/traits") ("nightly-test") ("derive" "enum-ordinalize-derive") ("default" "derive" "traits")))) (r "1.56")))

(define-public crate-enum-ordinalize-4.2.2 (c (n "enum-ordinalize") (v "4.2.2") (d (list (d (n "enum-ordinalize-derive") (r "^4.2.6") (o #t) (k 0)))) (h "0xrv32z2jafvdlh0qy2bj80kv8ykga8pygmy230wjabynb0rwdb5") (f (quote (("nightly-test") ("derive" "enum-ordinalize-derive") ("default" "derive" "traits")))) (y #t) (s 2) (e (quote (("traits" "enum-ordinalize-derive?/traits")))) (r "1.56")))

(define-public crate-enum-ordinalize-4.3.0 (c (n "enum-ordinalize") (v "4.3.0") (d (list (d (n "enum-ordinalize-derive") (r "^4.3") (o #t) (k 0)))) (h "1max64z9giii61qcwl56rndd7pakaylkaij5zqbbbvjl9vxdr87y") (f (quote (("nightly-test") ("default" "derive" "traits")))) (s 2) (e (quote (("traits" "enum-ordinalize-derive?/traits") ("derive" "dep:enum-ordinalize-derive")))) (r "1.60")))

