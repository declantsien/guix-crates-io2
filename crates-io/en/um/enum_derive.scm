(define-module (crates-io en um enum_derive) #:use-module (crates-io))

(define-public crate-enum_derive-0.1.1 (c (n "enum_derive") (v "0.1.1") (d (list (d (n "custom_derive") (r "^0.1.1") (d #t) (k 2)))) (h "138qbh9s4cxs2bfkga216w8x33iaasbjqyg7jh72sk0n62w78bzi")))

(define-public crate-enum_derive-0.1.2 (c (n "enum_derive") (v "0.1.2") (d (list (d (n "custom_derive") (r "^0.1.1") (d #t) (k 2)))) (h "0dn3gn3q7pyvphyxdci3phdkrrf69n5jgpdd7lixp24mnqmqrbva")))

(define-public crate-enum_derive-0.1.3 (c (n "enum_derive") (v "0.1.3") (d (list (d (n "custom_derive") (r "^0.1.1") (d #t) (k 2)))) (h "1z10avqzj89g1py50zvahxf729axsmjp5xz660039vwcsmjaz8sm")))

(define-public crate-enum_derive-0.1.7 (c (n "enum_derive") (v "0.1.7") (d (list (d (n "custom_derive") (r "^0.1.1") (d #t) (k 2)))) (h "05n8mdca3y2c8pvl1isncj52fa80wplvx28lxvwqmpzfr6lc4sj0") (f (quote (("std") ("default" "std"))))))

