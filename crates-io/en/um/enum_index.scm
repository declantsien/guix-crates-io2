(define-module (crates-io en um enum_index) #:use-module (crates-io))

(define-public crate-enum_index-0.1.0 (c (n "enum_index") (v "0.1.0") (d (list (d (n "enum_index_derive") (r "^0.1.0") (d #t) (k 2)))) (h "1xkavrbcacxgqf52l0a2sqyfsi09vwqa453j481gxbsz6pjkcdjk")))

(define-public crate-enum_index-0.1.1 (c (n "enum_index") (v "0.1.1") (d (list (d (n "enum_index_derive") (r "^0.1.1") (d #t) (k 2)))) (h "19fc1xc808l824qkisyvdlh6s0m1is8zircjcmk1l9wgyqrhfd8b")))

(define-public crate-enum_index-0.2.0 (c (n "enum_index") (v "0.2.0") (d (list (d (n "enum_index_derive") (r "^0.2.0") (d #t) (k 2)))) (h "1panfr0na7c9sam2x7bj2vz85fncxig1hdhc0s1vxrv2lpg2nlzm")))

