(define-module (crates-io en um enum_index_repr) #:use-module (crates-io))

(define-public crate-enum_index_repr-0.1.0 (c (n "enum_index_repr") (v "0.1.0") (h "1ax3s5q3db7gziklwnzjxgjfd32kr7j0rf33lj6vva0253qygzbx")))

(define-public crate-enum_index_repr-0.1.1 (c (n "enum_index_repr") (v "0.1.1") (h "0q9mfm2mi6m3gn1rqjx5va21pr90kg3m3lpkna9b2ac0ylr2jbld")))

(define-public crate-enum_index_repr-0.2.0 (c (n "enum_index_repr") (v "0.2.0") (d (list (d (n "enum_index_repr_derive") (r "^0.2") (d #t) (k 0)))) (h "1nni4cc6vfgsjj6lim9hlvnmsz62xgiz1jiqwyh23zj0911bgcak")))

(define-public crate-enum_index_repr-0.2.1 (c (n "enum_index_repr") (v "0.2.1") (d (list (d (n "enum_index_repr_derive") (r "^0.2") (d #t) (k 0)))) (h "18kgjxh0vlcdhyrkhh5nblpcc6qwp6h09hccrmkjm0psgdg4jw4d")))

