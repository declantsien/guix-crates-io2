(define-module (crates-io en um enum-extract-macro) #:use-module (crates-io))

(define-public crate-enum-extract-macro-0.1.0 (c (n "enum-extract-macro") (v "0.1.0") (d (list (d (n "enum-extract-error") (r "^0.1.0") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "1x7a02g00xs0gnwpy3x3ly9k35ihi3wz61dm9d7547zg8q610nb3")))

(define-public crate-enum-extract-macro-0.1.1 (c (n "enum-extract-macro") (v "0.1.1") (d (list (d (n "enum-extract-error") (r "^0.1.0") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.71") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.43") (f (quote ("full"))) (d #t) (k 0)))) (h "02ii28y670jawm6b2zgdlwch0d9f62p8jc1airv9chzsmdjnpnf5")))

