(define-module (crates-io en um enum-utility-macros) #:use-module (crates-io))

(define-public crate-enum-utility-macros-0.1.0 (c (n "enum-utility-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10166ccq78rp7234wbvc0nk4qy4wrdap9qcbjfwwriln0xdlrfia")))

