(define-module (crates-io en um enum2egui) #:use-module (crates-io))

(define-public crate-enum2egui-0.1.0 (c (n "enum2egui") (v "0.1.0") (d (list (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "enum2egui-derive") (r "^0.1.0") (d #t) (k 0)))) (h "11ca0vra21f2x3b1791pwibh0rc813ga6nl5fij9w7s9gzwh70fb")))

(define-public crate-enum2egui-0.1.1 (c (n "enum2egui") (v "0.1.1") (d (list (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "enum2egui-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0l5dscrhzkwnlbp547zz6q48nzx0qhyaxyjs9nlyh3q18rrbbzna")))

(define-public crate-enum2egui-0.1.2 (c (n "enum2egui") (v "0.1.2") (d (list (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "enum2egui-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0grdlpwjk3kmc7hmgibyx5nr6p4gpx8gjnc1id2k7ah12z5icr0i")))

(define-public crate-enum2egui-0.1.3 (c (n "enum2egui") (v "0.1.3") (d (list (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "enum2egui-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1g43ri3v7nw230g0mmj4fc0fnp2y8537bxm8492gvs2lsap246v4")))

(define-public crate-enum2egui-0.1.4 (c (n "enum2egui") (v "0.1.4") (d (list (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "enum2egui-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0zl6hi7k72865pj3h3k2zf6p557j4bq98gq6dq6g3kal36qlbq1k")))

(define-public crate-enum2egui-0.1.5 (c (n "enum2egui") (v "0.1.5") (d (list (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "enum2egui-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1vi02ajz4fayrqcjkb7351yd3wlif38bl901xl5yz4wmyxzrhykr")))

(define-public crate-enum2egui-0.1.6 (c (n "enum2egui") (v "0.1.6") (d (list (d (n "egui") (r "^0.23.0") (d #t) (k 0)) (d (n "enum2egui-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1xp12lp6ahmfyq3jc66gyyipdl8r5smz23cms33bpd5rbwdypkgf")))

(define-public crate-enum2egui-0.1.7 (c (n "enum2egui") (v "0.1.7") (d (list (d (n "egui") (r "^0.23.0") (d #t) (k 0)) (d (n "enum2egui-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.1") (o #t) (d #t) (k 0)))) (h "0qpmx41wr5ggr0sv1d6995smm0zic31ifv7jrn3ap8426zi3lbdf") (f (quote (("hashbrown_derive" "hashbrown") ("default"))))))

(define-public crate-enum2egui-0.1.8 (c (n "enum2egui") (v "0.1.8") (d (list (d (n "egui") (r "^0.24.1") (d #t) (k 0)) (d (n "enum2egui-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.1") (o #t) (d #t) (k 0)))) (h "04dgjgrxxaqnqh1vk5vvmz03sbibkm6xm63ya3hsx5sncysz6h09") (f (quote (("hashbrown_derive" "hashbrown") ("default"))))))

(define-public crate-enum2egui-0.2.0 (c (n "enum2egui") (v "0.2.0") (d (list (d (n "egui") (r "^0.26") (d #t) (k 0)) (d (n "enum2egui-derive") (r "^0.2.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.1") (o #t) (d #t) (k 0)))) (h "1c95p8ikpqwmi1pm1j5ywjw6y31nkfcmi60d99j5nwh03rzsy0z2") (f (quote (("hashbrown_derive" "hashbrown") ("default"))))))

