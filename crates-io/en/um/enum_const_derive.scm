(define-module (crates-io en um enum_const_derive) #:use-module (crates-io))

(define-public crate-enum_const_derive-0.1.0 (c (n "enum_const_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1zqrjjmhj1h8j1w8n86w6scx7m5fkq2g53s9valgll3nf3bwjb2m")))

(define-public crate-enum_const_derive-0.1.1 (c (n "enum_const_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kwpwra7vi20fhd3x915bgw3mzfvkpw18bhc4mp18gm4pihycz5h")))

