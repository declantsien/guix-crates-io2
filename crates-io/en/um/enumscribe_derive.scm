(define-module (crates-io en um enumscribe_derive) #:use-module (crates-io))

(define-public crate-enumscribe_derive-0.1.0 (c (n "enumscribe_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1b5nfwgl9khh0i77yvf6z3yxxzm496hiz4r6dwdljxbn84cgzj3h") (f (quote (("serde"))))))

(define-public crate-enumscribe_derive-0.1.1 (c (n "enumscribe_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qnwa4y6x8ma2s2j808whkbbb9jhx70apdg595vwd663cc4djvf1") (f (quote (("serde") ("default" "serde"))))))

(define-public crate-enumscribe_derive-0.1.2 (c (n "enumscribe_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zamyg52ax1f45jj4alv2g3mq70nsvfhqks26zcrj3gwap8zxxav") (f (quote (("serde") ("default" "serde"))))))

(define-public crate-enumscribe_derive-0.2.0 (c (n "enumscribe_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1k3zd0fsvmbmdmfwzninjkjzqhky83bkcgbwyba6q6blnrjjx9r6") (f (quote (("serde") ("default" "serde"))))))

(define-public crate-enumscribe_derive-0.2.1 (c (n "enumscribe_derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "09f9zj4q92sd25qhmwgsvjgq5bza7l8mmqmq4ibryxvhvnaj2gv5") (f (quote (("serde") ("default" "serde"))))))

(define-public crate-enumscribe_derive-0.3.0 (c (n "enumscribe_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1c3z0pan8kmzz4byqml9pvck9h5siy52250zdwwzni2r3q9lcxqk") (f (quote (("std") ("serde") ("default" "std" "serde"))))))

(define-public crate-enumscribe_derive-0.3.1 (c (n "enumscribe_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03w9ypdjg4x6gh1rsrr22h54xf04169lkdcvdm76s33rh6l2dsjc") (f (quote (("std") ("serde") ("default" "std" "serde"))))))

(define-public crate-enumscribe_derive-0.4.0 (c (n "enumscribe_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0wm04a31yyqpnbiv70zkw5pzwk9jrirlmqml5pmgkcisc9d3khd8") (f (quote (("std") ("serde") ("default" "std" "serde"))))))

