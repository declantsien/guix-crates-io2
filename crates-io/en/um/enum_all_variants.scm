(define-module (crates-io en um enum_all_variants) #:use-module (crates-io))

(define-public crate-enum_all_variants-0.1.0 (c (n "enum_all_variants") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "17frx6rb1wclg4hdsvs84jdql08vb195kpmh94xxdf0wsl3mwc6j")))

(define-public crate-enum_all_variants-0.2.0 (c (n "enum_all_variants") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "0caz9ba2qrxkwf242zsw68wfmgn0vjmh3a8dkv29bhl6d9b88qdl")))

