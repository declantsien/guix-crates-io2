(define-module (crates-io en um enum_like) #:use-module (crates-io))

(define-public crate-enum_like-0.1.0 (c (n "enum_like") (v "0.1.0") (h "1m1qnsspblyrypzpayx7rirfydrv3shzcbknnv1x8yvd0j835667")))

(define-public crate-enum_like-0.2.0 (c (n "enum_like") (v "0.2.0") (h "1l5xl0c588i2489n3p87yim8wix243pgyfyww1wsz53wp8iz0ir1")))

(define-public crate-enum_like-0.2.1 (c (n "enum_like") (v "0.2.1") (h "195yw41gmxfq94w8p212zal24hczaazgzjhj2r6pwxq4xqh8pvji")))

