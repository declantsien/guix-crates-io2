(define-module (crates-io en um enum_to_enum_derive) #:use-module (crates-io))

(define-public crate-enum_to_enum_derive-0.1.0 (c (n "enum_to_enum_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.74") (f (quote ("full" "extra-traits" "printing" "visit"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.25") (d #t) (k 2)))) (h "1ca00h74q5319g9zzqfhs3rsp31jn140gw9snazrl3lsw8q3b5ai")))

