(define-module (crates-io en um enum2str) #:use-module (crates-io))

(define-public crate-enum2str-0.1.0 (c (n "enum2str") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0v5sfa7jhnya5f1hvsqk26jgiv7mv5bbp459r08h74fc8w8h558c")))

(define-public crate-enum2str-0.1.1 (c (n "enum2str") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rs3c11qy0jd0w0hmhjpg4506sihnvh9w0kc7b3vn8szb5bk6rlp")))

(define-public crate-enum2str-0.1.2 (c (n "enum2str") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1fi612b5pd2ngvnkvs6rwnpkd200psyi0awxkc4jwmjzghlmmip8")))

(define-public crate-enum2str-0.1.3 (c (n "enum2str") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0c7saks7x96lck12wjfygzr3rzhnfx1371s6s720xv2blr7d7vjg")))

(define-public crate-enum2str-0.1.4 (c (n "enum2str") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1k3ixxj0xjh81fg3a866r3kbkx885mj9gwm5lspkzvxnm6f7xr1h")))

(define-public crate-enum2str-0.1.5 (c (n "enum2str") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qr5dbkrkv3f280h9kbv54d0fr738f5m04q2fc7kg7abhsrw729s")))

(define-public crate-enum2str-0.1.6 (c (n "enum2str") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1z0flsdd4p2cgxfk55gnh14zprry9dvi22q36ln27d08abgbzznn")))

(define-public crate-enum2str-0.1.7 (c (n "enum2str") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1hbvm55rn75qhmfafqq75qy58r88slppwzr0v4lcmc2pgrsy7gmh")))

(define-public crate-enum2str-0.1.8 (c (n "enum2str") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0xb9hr90jkm1k14y18fmx68mha5gzbpbl2ysy9j0z2f68l3ykx3c")))

(define-public crate-enum2str-0.1.9 (c (n "enum2str") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qk6w8pd2sya3vbv6s52cgqbfffbx3jnnazzr704i3zfywng2qya")))

(define-public crate-enum2str-0.1.10 (c (n "enum2str") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0snn9fg3x81jjym3jvmwxyg4dm8ycrwiz86jw4xkl8dazgrs35xq")))

