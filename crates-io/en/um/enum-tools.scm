(define-module (crates-io en um enum-tools) #:use-module (crates-io))

(define-public crate-enum-tools-0.5.0 (c (n "enum-tools") (v "0.5.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "remain") (r "^0.2.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (d #t) (k 0)))) (h "0vbar5gmawmwdq08d724mbcmrf1ybwxqmyq511dm5md8y7g34i03")))

(define-public crate-enum-tools-0.5.1 (c (n "enum-tools") (v "0.5.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01qx26001npz3djk03cdfkm372p5gy3m31h9wxi1bhwys4z4jin6")))

(define-public crate-enum-tools-0.5.2 (c (n "enum-tools") (v "0.5.2") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1si2cqsqpkc88lym82r5hy57icx2vklpxv4bh1ac80xxvqhychx4")))

(define-public crate-enum-tools-0.5.3 (c (n "enum-tools") (v "0.5.3") (d (list (d (n "proc-macro-error") (r "^1.0.4") (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "trybuild") (r "^1.0.85") (f (quote ("diff"))) (d #t) (k 2)))) (h "1qzzzyfhx6vjampyd6mgkrgkyygja2mb9pzcm9sacfzrc5661b3q") (r "1.58.1")))

