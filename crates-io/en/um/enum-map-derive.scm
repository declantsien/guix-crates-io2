(define-module (crates-io en um enum-map-derive) #:use-module (crates-io))

(define-public crate-enum-map-derive-0.1.0 (c (n "enum-map-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1008i5yahkp3v1gm7jpva45r4dzzw2gbzn8wikiciiws0driarm6")))

(define-public crate-enum-map-derive-0.1.1 (c (n "enum-map-derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1bm2sgc7ywargl4qxg8y3b1imm9cbhw9js0xpja1b8s02zlhxr6v")))

(define-public crate-enum-map-derive-0.1.2 (c (n "enum-map-derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0a633r19g1smpis7vjrnv2s4vjqx12prafrk2qwpc2kfpc95nc8w")))

(define-public crate-enum-map-derive-0.1.3 (c (n "enum-map-derive") (v "0.1.3") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1fhbfc6h9xqbfzsizvz2h0an41jpyi2vcbzqj9f9x0d8pk7g9d1j")))

(define-public crate-enum-map-derive-0.2.0 (c (n "enum-map-derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1cdb8s4isf9hn1xraqym29f6cbmpyc5flnk010w8rg5lhq74j0ks")))

(define-public crate-enum-map-derive-0.2.1 (c (n "enum-map-derive") (v "0.2.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "06ksphnm8qlyicd4c66qckklifsbp32fgms57h62gc48zzkaicjc")))

(define-public crate-enum-map-derive-0.2.2 (c (n "enum-map-derive") (v "0.2.2") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "07kw0q1ddvwzjj2mgsscl3hf5l015vnfwr18kjzd22cf2bjifxfn")))

(define-public crate-enum-map-derive-0.2.3 (c (n "enum-map-derive") (v "0.2.3") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1sp24r8flr1ifiv5glr5acf9jk4mgip99mb2949lgd81fk34gx53")))

(define-public crate-enum-map-derive-0.2.4 (c (n "enum-map-derive") (v "0.2.4") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0nf57nqw1sksjz5vkxf0mwq864wr02yy84s8jvkjsmyj3awig18w")))

(define-public crate-enum-map-derive-0.2.5 (c (n "enum-map-derive") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^0.3.1") (d #t) (k 0)) (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "syn") (r "^0.13.0") (d #t) (k 0)))) (h "026w635zf68dkmhm46bak5535dvmp7zkf95fw02nlyzh4k5hcy1l")))

(define-public crate-enum-map-derive-0.2.6 (c (n "enum-map-derive") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^0.4.2") (d #t) (k 0)) (d (n "quote") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^0.14.0") (d #t) (k 0)))) (h "1ymh62dflddn2vhkc497511wh51akr38b638h976r36g32myf431")))

(define-public crate-enum-map-derive-0.2.7 (c (n "enum-map-derive") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^0.4.2") (d #t) (k 0)) (d (n "quote") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^0.14.0") (d #t) (k 0)))) (h "13sag1i7f11yxmbrkmbi2n4nrnx4qvz0r9nwmg6p6abc52vp43c1")))

(define-public crate-enum-map-derive-0.2.8 (c (n "enum-map-derive") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^0.4.2") (d #t) (k 0)) (d (n "quote") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^0.14.0") (d #t) (k 0)))) (h "147mpn8mf1qwss7iwc587n75754v83rs7iw82jzivnpy8kygdr91")))

(define-public crate-enum-map-derive-0.2.9 (c (n "enum-map-derive") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^0.4.2") (d #t) (k 0)) (d (n "quote") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^0.14.0") (d #t) (k 0)))) (h "0flqaxxny9l5gcz91qnnn82q3grjrdgn9jkx713gw62xr029hb0j")))

(define-public crate-enum-map-derive-0.3.0 (c (n "enum-map-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.2") (d #t) (k 0)) (d (n "quote") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^0.14.0") (d #t) (k 0)))) (h "1rj5xkgn9iw59lyyhfj24jsjry2pca4lqhvv27y1pk84h20q7q0c")))

(define-public crate-enum-map-derive-0.4.0 (c (n "enum-map-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4.2") (d #t) (k 0)) (d (n "quote") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^0.14.0") (d #t) (k 0)))) (h "0rd38sk3jrbqxwxckvqz10sr2bma70wlc58mlmpppqjwa3pwh5gr")))

(define-public crate-enum-map-derive-0.2.10 (c (n "enum-map-derive") (v "0.2.10") (d (list (d (n "proc-macro2") (r "^0.4.2") (d #t) (k 0)) (d (n "quote") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^0.14.0") (d #t) (k 0)))) (h "1vwv2r2jn9sa4fg6pg5j7lszy5sy82b7l4v8ngpswnb8lwkjp3j0")))

(define-public crate-enum-map-derive-0.4.1 (c (n "enum-map-derive") (v "0.4.1") (d (list (d (n "enum-map") (r "^0.5.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.2") (d #t) (k 0)) (d (n "quote") (r "^0.6.0") (d #t) (k 0)) (d (n "syn") (r "^0.14.0") (d #t) (k 0)))) (h "0znh3kv0wj7hrjqrnsjdnz6dr7i261gic6wjvzzf4s18if56wgqm")))

(define-public crate-enum-map-derive-0.4.2 (c (n "enum-map-derive") (v "0.4.2") (d (list (d (n "enum-map") (r "^0.5.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.1") (d #t) (k 0)))) (h "1c0b853nyqsh7sdbaarv6lffvik0favzgbwqcm9kmwwk87zmxz3d")))

(define-public crate-enum-map-derive-0.4.3 (c (n "enum-map-derive") (v "0.4.3") (d (list (d (n "enum-map") (r "^0.5.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.1") (d #t) (k 0)))) (h "1mwscbnqdvnj74z6y8vxzbgaisgshxl6asgq7w85lbsknbgh2w75")))

(define-public crate-enum-map-derive-0.4.4 (c (n "enum-map-derive") (v "0.4.4") (d (list (d (n "enum-map") (r "^0.5.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.1") (d #t) (k 0)))) (h "1qzcvg3mlhdxnqxcia3jaljrbqs2zy38rs323y97cvm2s7v3ra6v")))

(define-public crate-enum-map-derive-0.4.5 (c (n "enum-map-derive") (v "0.4.5") (d (list (d (n "enum-map") (r "^0.5.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.1") (d #t) (k 0)))) (h "0lycndvksr8pfy8mh5a5jhaxgncfabqbvpw5g386mfx60qckzf6v")))

(define-public crate-enum-map-derive-0.4.6 (c (n "enum-map-derive") (v "0.4.6") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0mg43p1x90cz604zddk9qzss077v2id04qmmbpa1i7jc637m1i75")))

(define-public crate-enum-map-derive-0.5.0 (c (n "enum-map-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "10gahq36nx3vpzd7jj7qvz2gy6zp24v0vcbkv1l8j2hc1y9wvp7j")))

(define-public crate-enum-map-derive-0.6.0 (c (n "enum-map-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1ah7b71llknvwj031zsrqpxam5566hbc2i6vq7v4zqzn1ap8w9w4")))

(define-public crate-enum-map-derive-0.7.0 (c (n "enum-map-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "17i9r89kqnnd4r0qfs7np97qipkzxzy8ihxd4cd013whdb9c10l1")))

(define-public crate-enum-map-derive-0.8.0 (c (n "enum-map-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "01fs6hvdchg30srww4ky4bscm3vs1dsmfqp3qp78vwy6vq6plfx6")))

(define-public crate-enum-map-derive-0.9.0 (c (n "enum-map-derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1mi0ss2bly63x9rl4v5qr507gdgvpv153w7cj2kkcwm54m7cbl80") (r "1.61")))

(define-public crate-enum-map-derive-0.10.0 (c (n "enum-map-derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "09zwaz23bciicqyniqk6wbca942w19xr2z8n7cyghnndfqk5w159") (r "1.61")))

(define-public crate-enum-map-derive-0.11.0 (c (n "enum-map-derive") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1wjrbi6mydwawg37dnpy4k7y5limdbngg4xsqxcdf5k17dmsfk9a") (r "1.61")))

(define-public crate-enum-map-derive-0.12.0 (c (n "enum-map-derive") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0bvdy7vxxk1hrsls4bimdbm2plhb1d75z3hghvbx4whah04v8q45") (r "1.61")))

(define-public crate-enum-map-derive-0.13.0 (c (n "enum-map-derive") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1h2z7afr7h8glal4fk713bzawqj7zijg4b0gmhp5rpc3fn94vcfc") (r "1.61")))

(define-public crate-enum-map-derive-0.14.0 (c (n "enum-map-derive") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "09azx55dv8zjgmjb66f5vy0qfybsdx57gh838hf8c7dvwf4b5l04") (r "1.61")))

(define-public crate-enum-map-derive-1.0.0-0.gat.0 (c (n "enum-map-derive") (v "1.0.0-0.gat.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1pvxplamjaj89vcwhpa2w7h7llfxbjsjdpd6lq75s7wpclwrpij8") (r "1.61")))

(define-public crate-enum-map-derive-0.15.0 (c (n "enum-map-derive") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1f4c5yrh7zlas49qyvgrfbr669brz6fzi4qlxllqsd10wx3cscvr") (r "1.61")))

(define-public crate-enum-map-derive-1.0.0-beta.1 (c (n "enum-map-derive") (v "1.0.0-beta.1") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1srf7srxgd8g5g49wi4yc3fv4dwwshbqdsb1nvwg8p90rs8h0q24") (r "1.61")))

(define-public crate-enum-map-derive-0.16.0 (c (n "enum-map-derive") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1h4c813zbh250dh07ldzgikp5q7xfzjn84rsb744gbxys5rkvnxz") (y #t) (r "1.61")))

(define-public crate-enum-map-derive-0.17.0 (c (n "enum-map-derive") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1sv4mb343rsz4lc3rh7cyn0pdhf7fk18k1dgq8kfn5i5x7gwz0pj") (r "1.61")))

