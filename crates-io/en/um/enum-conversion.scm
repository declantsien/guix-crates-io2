(define-module (crates-io en um enum-conversion) #:use-module (crates-io))

(define-public crate-enum-conversion-0.0.1 (c (n "enum-conversion") (v "0.0.1") (d (list (d (n "enum-conversion-derive") (r "^0.0.1") (d #t) (k 0)) (d (n "enum-conversion-traits") (r "^0.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.40") (d #t) (k 2)))) (h "0d6c0p6zrfl96d0zd2nlq39nib3cnczhrnf37zmk59fafx1nv5z5")))

