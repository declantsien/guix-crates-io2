(define-module (crates-io en um enum-procs) #:use-module (crates-io))

(define-public crate-enum-procs-0.1.0 (c (n "enum-procs") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.59") (d #t) (k 0)))) (h "0kg9rd15y34hkkw5k61hsgmn4v4caw4bzv6xq04g7vhljwqicl7q")))

(define-public crate-enum-procs-0.1.1 (c (n "enum-procs") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.59") (d #t) (k 0)))) (h "1i88xlil86zz5fn5rwg5kcrq19sxad3rmjqg697yjx2lzjy09zad")))

(define-public crate-enum-procs-0.2.0 (c (n "enum-procs") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.59") (d #t) (k 0)))) (h "11i4dsg7w13b0h1d93h7hv06jzqjpnhcs6fck37n3ydvbjff5q6v")))

