(define-module (crates-io en um enum_str) #:use-module (crates-io))

(define-public crate-enum_str-0.1.0 (c (n "enum_str") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)))) (h "18kyv2f36i8kvkvsa21qr9kpmpqarv1xsaxlipr3rf35vpa3jlbz")))

(define-public crate-enum_str-0.1.1 (c (n "enum_str") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)))) (h "1ls33snhc7k2ckv0m05cv2ak4l5x907244fwsvbjajvlknwk12mb")))

(define-public crate-enum_str-0.1.2 (c (n "enum_str") (v "0.1.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)))) (h "0hw8lln10s2w6zwaq22ph9yrcf4y7kk66cscblfbkpl2dpz25334")))

