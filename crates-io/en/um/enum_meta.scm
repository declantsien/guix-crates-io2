(define-module (crates-io en um enum_meta) #:use-module (crates-io))

(define-public crate-enum_meta-0.1.0 (c (n "enum_meta") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "1fmabal973xjxb5xcc07s117srjil8klnj7y9wi0gjlqd52f3cv6")))

(define-public crate-enum_meta-0.2.0 (c (n "enum_meta") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "159cgz7h23g4rxh43jjwnpm0nhhlg3ifyz6q6rcsqsnsq7j89y92")))

(define-public crate-enum_meta-0.3.0 (c (n "enum_meta") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "0w1iiaxx10pd6p5i2yaj253r8nav9dppbg07d48kjvkd529kbdy3")))

(define-public crate-enum_meta-0.4.0 (c (n "enum_meta") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "0fx3xn9139acz3p1mv6r0d2aja3vjpysraxwhla0wbdni3cmkwjd")))

(define-public crate-enum_meta-0.5.0 (c (n "enum_meta") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "0lacx1v6za74s6qy7nxj2wx6hqdzm8j4fczlh2v9x46rmaysf6vr")))

(define-public crate-enum_meta-0.6.0 (c (n "enum_meta") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)))) (h "1g9gykb1f0y1514mr5qmvp3mlbg3i6v2nhmvcck1qls0jr46wxk6")))

