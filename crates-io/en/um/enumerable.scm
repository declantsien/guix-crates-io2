(define-module (crates-io en um enumerable) #:use-module (crates-io))

(define-public crate-enumerable-0.1.0 (c (n "enumerable") (v "0.1.0") (d (list (d (n "enumerable_derive") (r "=0.1.0") (d #t) (k 0)))) (h "14p159ykvs6yk643rw9i66i40lx41kj84dgslcp27ibz98m2vba6")))

(define-public crate-enumerable-0.2.0 (c (n "enumerable") (v "0.2.0") (d (list (d (n "enumerable_derive") (r "=0.2.0") (d #t) (k 0)))) (h "111dhb8dxzl6pl1sr4gajlr7jibwq7pgbd29zvzwk2z22vg1h4sk")))

