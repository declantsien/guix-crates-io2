(define-module (crates-io en um enum_downcast_derive) #:use-module (crates-io))

(define-public crate-enum_downcast_derive-0.1.0 (c (n "enum_downcast_derive") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jk3gvzvhjdh4bvykm1j8hv41l6gm1kwgxxd2c1s2snqj5zris50")))

