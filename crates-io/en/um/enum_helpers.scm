(define-module (crates-io en um enum_helpers) #:use-module (crates-io))

(define-public crate-enum_helpers-0.0.1 (c (n "enum_helpers") (v "0.0.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "1dgs53hxm3nfijwlacrn6jzj38bc0v260jrcxdf6azf5xn7l1kgp")))

(define-public crate-enum_helpers-0.0.2 (c (n "enum_helpers") (v "0.0.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "00syj5ygka6swkyfv8rc1rppij5lkir39qnshkajkfc91jn94ln4")))

(define-public crate-enum_helpers-0.0.3 (c (n "enum_helpers") (v "0.0.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "1win1l0la998yr2yr5q9dm4j29v1fm6ajdy60kh45q77q0wfmixb")))

(define-public crate-enum_helpers-0.0.4 (c (n "enum_helpers") (v "0.0.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("full"))) (d #t) (k 0)))) (h "0mvbyw8cxq1pngi3bmzab9dwnlsral4kzf98wjwkhkvxdkcw9a27")))

