(define-module (crates-io en um enum_index_repr_derive) #:use-module (crates-io))

(define-public crate-enum_index_repr_derive-0.2.0 (c (n "enum_index_repr_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "19ym0cq8i1qhwgb30p9dqdalc5b8a950xrpiikysnpmm5llb4zvz")))

