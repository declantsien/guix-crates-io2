(define-module (crates-io en um enum-derived) #:use-module (crates-io))

(define-public crate-enum-derived-0.1.0 (c (n "enum-derived") (v "0.1.0") (h "1m5sn43vhx3m9rpz6mxljr7cdb6ildz9pjaxvjxya830rlhcl6i9")))

(define-public crate-enum-derived-0.2.0 (c (n "enum-derived") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1gpdlpb6rna13ma9hs3800vjj2yzx101y52bflzcq2qv99jkiggg")))

(define-public crate-enum-derived-0.2.1 (c (n "enum-derived") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1ihrpbik21cj952lyxsq56xc1x7qmdnwija0zcjj0p44103qj2f4")))

(define-public crate-enum-derived-0.2.2 (c (n "enum-derived") (v "0.2.2") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "14288srbgqrq9rkvqnqzlfdvc7mdkhz2df9yy7an7wgv4y1wpv5f")))

(define-public crate-enum-derived-0.2.3 (c (n "enum-derived") (v "0.2.3") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "06aj12p95zxbwraqlzikdlk6j6gm3inic893whz417kjaziq935d")))

(define-public crate-enum-derived-0.2.5 (c (n "enum-derived") (v "0.2.5") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1h821zs73xwr41wbj2b06hn6d2kmwbqllza64hp1h9mxms9kijkx")))

(define-public crate-enum-derived-0.2.7 (c (n "enum-derived") (v "0.2.7") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0nfx74ram45c2qxib62x7jsd8wk8pfr690d09lzdv2i7iw8syrwd")))

(define-public crate-enum-derived-0.2.8 (c (n "enum-derived") (v "0.2.8") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0k4p9cwc8q5aa48igjbw0sgbns0lrkdmaznvn6qsbblgb5xr35vv")))

(define-public crate-enum-derived-0.3.0 (c (n "enum-derived") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1g6gr40whc1vld8lkz80bgzi0hl52yi7vh2cwf8inm67gyznnaax")))

(define-public crate-enum-derived-0.3.1 (c (n "enum-derived") (v "0.3.1") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1plfjmmr26k1wqkrc00mpw94qbl4wz1nqzqhlhn630i7yr4alp69")))

(define-public crate-enum-derived-0.4.0 (c (n "enum-derived") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0r84kz2c0vasjig3v1mx01frdz7znr6h2agc2scqd17bnm4bd8hm")))

(define-public crate-enum-derived-0.5.0 (c (n "enum-derived") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1kqrpiwba6sbczj812wq19xx0c3ml78py7wyvgfr7m49nk5fz38r")))

(define-public crate-enum-derived-0.5.1 (c (n "enum-derived") (v "0.5.1") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "03q83rk804m8ndy93jwln2jai5g3p21cicwwamhnjh7vpnj0qdx6")))

(define-public crate-enum-derived-0.5.2 (c (n "enum-derived") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1jm1fjcarnm7ssfbl172l8y3kl497ip1ghm37v9mivg2v3hh3akz")))

(define-public crate-enum-derived-0.5.3 (c (n "enum-derived") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1cac2jll8s6lfw3a9rdnnffjsr98qjy0ai8r5yk8ky45v76qaw9h")))

(define-public crate-enum-derived-0.6.0 (c (n "enum-derived") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "168wb2zwssykiswy18rhmzkknh85x72v7vxawanqj40v4cvmj5mf")))

(define-public crate-enum-derived-0.6.1 (c (n "enum-derived") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "05lp15m3nyd9by5pk2r19xknmnmhy6wdhbbdfvfgkwkl4w5y6rfi")))

(define-public crate-enum-derived-0.7.0 (c (n "enum-derived") (v "0.7.0") (d (list (d (n "enum-derived-macro") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "06sd7dfr31923l1zz8mx3kbnd0gfy1dpy7a4dhnhb85a7xlczwjf")))

(define-public crate-enum-derived-0.8.0 (c (n "enum-derived") (v "0.8.0") (d (list (d (n "enum-derived-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "00744bx0waw5azyw4q9sfpkg7zmw1p8ii3krjd952amdi8gmhn6p")))

(define-public crate-enum-derived-0.8.1 (c (n "enum-derived") (v "0.8.1") (d (list (d (n "enum-derived-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "14b39nh4wjy9mrr6skhmca3v1fvwj3qwdlbh88v45ikgrwry7q9k")))

(define-public crate-enum-derived-0.8.2 (c (n "enum-derived") (v "0.8.2") (d (list (d (n "enum-derived-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1bbmii2vilm984295xz7h48b0k14gdw12q0rvbpss9q7cwjg2iw7")))

