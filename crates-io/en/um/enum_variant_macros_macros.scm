(define-module (crates-io en um enum_variant_macros_macros) #:use-module (crates-io))

(define-public crate-enum_variant_macros_macros-0.1.0 (c (n "enum_variant_macros_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0fg15qdayn24gl23dp1bffsrljf0fj552qv1hjig9l7y5wp40w35")))

(define-public crate-enum_variant_macros_macros-0.2.0 (c (n "enum_variant_macros_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0ph4g6d5yjixlh34jkvidbpay8jyql60nhf5bg75ijz3r1yjyhk4")))

(define-public crate-enum_variant_macros_macros-0.3.0 (c (n "enum_variant_macros_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.54") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1mg2y34l36x7lr24ia2njp069y3cfikddgj4hfb6kjg84vk01x83")))

(define-public crate-enum_variant_macros_macros-0.3.1 (c (n "enum_variant_macros_macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1fw0p7yj0vvkx7462aksdi0bsnzff1czf3kg7d85y4v4pmvhq0ii")))

