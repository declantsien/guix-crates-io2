(define-module (crates-io en um enumn) #:use-module (crates-io))

(define-public crate-enumn-0.1.0 (c (n "enumn") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1mgq6cl658zrl7763lb476wb8cfd8qbgfcysqqzffhhwahz8lr6v")))

(define-public crate-enumn-0.1.1 (c (n "enumn") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "125khdrqgivin0vcqkqcg1bj0mqckfca7r16kgkgm6ab9aq7p78p")))

(define-public crate-enumn-0.1.2 (c (n "enumn") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1b62iabx3af8hac5cl6k9ll5rsm104q1kaqyn1fwrl5nrs232kk1")))

(define-public crate-enumn-0.1.3 (c (n "enumn") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1x7c55nja4qa69fzx18gikfhcr5qrbmg01axgj2s16h9sl9b2n2f")))

(define-public crate-enumn-0.1.4 (c (n "enumn") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0i7kzdx9ybn7j0rkf0xx5kx6n3ph4n54mdvxycghbgcq79vwhaq5") (r "1.31")))

(define-public crate-enumn-0.1.5 (c (n "enumn") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qkaw2liic16xnx3s76ybs9l5hl43n5qzmggz48z4b85b7x1m2q3") (r "1.31")))

(define-public crate-enumn-0.1.6 (c (n "enumn") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ck10am7scklcby0crbf4zd456gzbvkrk8mbfzamarbs0qxcp2z8") (r "1.31")))

(define-public crate-enumn-0.1.7 (c (n "enumn") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "123vrar6xznn7x5qnqn40yawldbzavj8nmbl2109nj2dw4rflh0r") (r "1.31")))

(define-public crate-enumn-0.1.8 (c (n "enumn") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0xkx9gmix7dbgjka5lawga4kly9im22316fjg1xwidrg0hcn60a8") (r "1.56")))

(define-public crate-enumn-0.1.9 (c (n "enumn") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0dvynk98v2cmi0yckgahkvksxv39h3lg6xqkl3klxzbhnlwdbr3g") (r "1.56")))

(define-public crate-enumn-0.1.10 (c (n "enumn") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "08l5hwx7x4m07i1cf8y2vksv51hr2fg77q8a0z9rkqsx1ybqm0y9") (r "1.56")))

(define-public crate-enumn-0.1.11 (c (n "enumn") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1id5n4wrnl2cqdl7jw91cnv6zqgs8xsdr12z2q8wi4n05pmw94xq") (r "1.56")))

(define-public crate-enumn-0.1.12 (c (n "enumn") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "0ard4yslzvax82k3gi38i7jcii7a605rz4fqpy34c6l03ppqrbf2") (r "1.56")))

(define-public crate-enumn-0.1.13 (c (n "enumn") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "0hnvrp440hwjfd4navbni2mhcjd63adxp8ryk6z3prw8d7yh1l3g") (r "1.56")))

