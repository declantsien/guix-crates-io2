(define-module (crates-io en um enum-ref) #:use-module (crates-io))

(define-public crate-enum-ref-0.0.0 (c (n "enum-ref") (v "0.0.0") (h "082b5iaadxravb6rx54z0pwnagv1ard722cwwirk9nr61w49lcd2")))

(define-public crate-enum-ref-0.1.0 (c (n "enum-ref") (v "0.1.0") (d (list (d (n "enum-ref-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.60") (f (quote ("diff"))) (d #t) (k 2)))) (h "1bpxp5rw5xj8m54qqg8zk0m3f2l9wc7jfqshikfibgp7ij7zh83i")))

