(define-module (crates-io en um enum-tags-macros) #:use-module (crates-io))

(define-public crate-enum-tags-macros-0.1.0 (c (n "enum-tags-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "19y2bd9qn40k3xp0r6fvpm028jv103ddg2mqcjj1ag2mgfz0c3x4")))

