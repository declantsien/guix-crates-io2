(define-module (crates-io en um enum-repr) #:use-module (crates-io))

(define-public crate-enum-repr-0.1.0 (c (n "enum-repr") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.8") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)))) (h "175fl4972l7k03xid2l15f556way5m393zh6vjhwzgvil3c7x9f9")))

(define-public crate-enum-repr-0.1.1 (c (n "enum-repr") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.8") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.4") (d #t) (k 0)))) (h "1h6dasdad6lwh6ah5gf797anfv2kcqcj40c6hbrl9gmfb7glpjbz")))

(define-public crate-enum-repr-0.1.2 (c (n "enum-repr") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.8") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.17") (d #t) (k 0)))) (h "0dzzk4l1chrc9886pa7lshgx8sjhvla0wyv22aiiig4v0z25c034")))

(define-public crate-enum-repr-0.2.0 (c (n "enum-repr") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.8") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.17") (f (quote ("full"))) (d #t) (k 0)))) (h "0icala600xi7yng4l72qvw4kx13sl3q6as7kl6j2z9sx9gidz038")))

(define-public crate-enum-repr-0.2.1 (c (n "enum-repr") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.8") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.17") (f (quote ("full"))) (d #t) (k 0)))) (h "1hfz5vg34qmk557aa1klj2w3w64i0yi9n1csnlmvcwz3iyb9lihl")))

(define-public crate-enum-repr-0.2.2 (c (n "enum-repr") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.8") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.17") (f (quote ("full"))) (d #t) (k 0)))) (h "05596d6cdd0wj46xyv1plxdl2i6f0r2jb8g0skm2zj1p7wvv83nd")))

(define-public crate-enum-repr-0.2.3 (c (n "enum-repr") (v "0.2.3") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.8") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.17") (f (quote ("full"))) (d #t) (k 0)))) (h "02iargpznq8rlryfj9xrzfn99d01ic49r0hg3m250claf0ykyv3l")))

(define-public crate-enum-repr-0.2.4 (c (n "enum-repr") (v "0.2.4") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.8") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.17") (f (quote ("full"))) (d #t) (k 0)))) (h "04n0397d7c5c4sknddyghlmryj8gjz30iyf0jvvan34fz47pmvra")))

(define-public crate-enum-repr-0.2.5 (c (n "enum-repr") (v "0.2.5") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14qnzb574gzvbmp8fa0ysqs1kbj1v02nj6jjsip1iw2j6fkgjma5")))

(define-public crate-enum-repr-0.2.6 (c (n "enum-repr") (v "0.2.6") (d (list (d (n "libc") (r "^0.2.42") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "060gjn3rjfy5pd5xlb7nz8smvc8p24gv3nhha2pg3am11yf0rlxs")))

