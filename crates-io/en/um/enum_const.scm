(define-module (crates-io en um enum_const) #:use-module (crates-io))

(define-public crate-enum_const-0.1.0 (c (n "enum_const") (v "0.1.0") (d (list (d (n "enum_const_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0b80pg01rzyn6n1sbqnfsshwh17ixz83jlfx8i28b3p78kdv3hgd")))

(define-public crate-enum_const-0.1.1 (c (n "enum_const") (v "0.1.1") (d (list (d (n "enum_const_derive") (r "^0.1.1") (d #t) (k 0)))) (h "0kz5zq25wvps0ryd8y39zfv247mr4kih3ic3ljxl3fx84rrihdx1")))

