(define-module (crates-io en um enum-kinds-macros) #:use-module (crates-io))

(define-public crate-enum-kinds-macros-0.1.0 (c (n "enum-kinds-macros") (v "0.1.0") (d (list (d (n "enum-kinds-traits") (r "^0.1.0") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "17caf7zdkzz3shmsg04srpq4fl7lvw2vcswgkbpvyyza6wr6blnl")))

(define-public crate-enum-kinds-macros-0.1.1 (c (n "enum-kinds-macros") (v "0.1.1") (d (list (d (n "enum-kinds-traits") (r "^0.1.0") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0zvz78f9v9ga26a5bm341d0jh7fa5jkkh6ak22y98i98s9b5lyac")))

(define-public crate-enum-kinds-macros-0.2.0 (c (n "enum-kinds-macros") (v "0.2.0") (d (list (d (n "enum-kinds-traits") (r "^0.1.1") (d #t) (k 2)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (d #t) (k 0)))) (h "131yccqw8qgg0ybj9icd6dr3r9sw23mr4swjz30ysrg6p6hdn399")))

(define-public crate-enum-kinds-macros-0.2.1 (c (n "enum-kinds-macros") (v "0.2.1") (d (list (d (n "enum-kinds-traits") (r "^0.1.2") (d #t) (k 2)) (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (d #t) (k 0)))) (h "1lnmzn1ajkvz9r3qvx5sky9qx6j457fqw5n7r27xnzkz6zyy14ws")))

