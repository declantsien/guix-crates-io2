(define-module (crates-io en um enum_properties) #:use-module (crates-io))

(define-public crate-enum_properties-0.1.0 (c (n "enum_properties") (v "0.1.0") (h "1ng7cf42qhaf91m4r5b2gnqfpxvawwbpfpcx4kdd5lrz118phlkm")))

(define-public crate-enum_properties-0.2.0 (c (n "enum_properties") (v "0.2.0") (h "06cjqni4a83qjcispyfzc6x2riym4sshv26c4irnw16k3mcqhasb")))

(define-public crate-enum_properties-0.2.1 (c (n "enum_properties") (v "0.2.1") (h "08r840ydmhv13iqfd6z6a509s2bqs72z14dj9rqq7ggph0h9bzbm")))

(define-public crate-enum_properties-0.3.0 (c (n "enum_properties") (v "0.3.0") (h "1123c79k4mhza55gwk5b3nqsnhn4f014qw8nxmjk93yqzg21i6dn")))

