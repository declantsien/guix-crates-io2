(define-module (crates-io en um enum-filter) #:use-module (crates-io))

(define-public crate-enum-filter-0.1.0 (c (n "enum-filter") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17mhjkcnlb07nxbhdpqk9wbxjdbh591sj3kdlsqvlr9c4wvrva5b")))

