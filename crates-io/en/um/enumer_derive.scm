(define-module (crates-io en um enumer_derive) #:use-module (crates-io))

(define-public crate-enumer_derive-0.1.0 (c (n "enumer_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "00b9n3nxar7h2r5vhqk2n79193rcaas0h7ml4pvm2mqdik3fal0n")))

(define-public crate-enumer_derive-0.1.1 (c (n "enumer_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "11nvyrswpxyf581p0h7f37qqc3jlywykrk1hlanks6cdl7m6l92v")))

