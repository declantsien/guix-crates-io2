(define-module (crates-io en um enumx_derive) #:use-module (crates-io))

(define-public crate-enumx_derive-0.0.1 (c (n "enumx_derive") (v "0.0.1") (h "0nx8ykzwilxfy31i4q9vz2y86518g6slwyd05blw8vj971cq33bp")))

(define-public crate-enumx_derive-0.1.0 (c (n "enumx_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "146gc28k3jw4ryhknl4isl24kcmlbswmdkxab59wf27pir6yv4ng")))

(define-public crate-enumx_derive-0.1.1 (c (n "enumx_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "0vy7zjv2n6rildsi5gscs7w4b67sjrw9zzmg8ynjzcplvkxyjv3z")))

(define-public crate-enumx_derive-0.2.0 (c (n "enumx_derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "1k2m3j3njnm6gi5av4xs6lr0dmy8v58dqkxsd4awmvcw2vglbd6j")))

(define-public crate-enumx_derive-0.2.1 (c (n "enumx_derive") (v "0.2.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)))) (h "00jy26zmc88qhznpq2932sd4zcdfn7x51bbn3k5wvg977rs3xqvs")))

(define-public crate-enumx_derive-0.3.0-alpha (c (n "enumx_derive") (v "0.3.0-alpha") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1k7q4drd2z262s0mw1g2vc1ynz9028mq1l4fkbm7jpfhi8m723v3")))

(define-public crate-enumx_derive-0.3.0 (c (n "enumx_derive") (v "0.3.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1xg3kzhkybw71crrijcs3fl0ywfky9gm1b3md72hcpa5slzydm77")))

(define-public crate-enumx_derive-0.3.1 (c (n "enumx_derive") (v "0.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)))) (h "0f8k8g3cl8cxpdsfyn87344qbimijksqyqivjybgywd07qyxqiad")))

(define-public crate-enumx_derive-0.4.0 (c (n "enumx_derive") (v "0.4.0") (d (list (d (n "indexmap") (r "^1.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1w10grwskwz8bdqnj5s10znjip8xyry3ql1qc9d52p02rkg86dy5")))

(define-public crate-enumx_derive-0.4.2 (c (n "enumx_derive") (v "0.4.2") (d (list (d (n "indexmap") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0jgbxd08zs7chh9hcq8kihhwxr6iprcdn6x3bfl6xaqyvppncpgs")))

