(define-module (crates-io en um enum_ext) #:use-module (crates-io))

(define-public crate-enum_ext-0.1.0 (c (n "enum_ext") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.62") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0h6i2rfixzzx1vwx9qzccv2ivvsjsjjbc92ijl5w0d10whgljb35") (y #t)))

(define-public crate-enum_ext-0.1.2 (c (n "enum_ext") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.62") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "090z2mncwylxrmf90xk8vka5lzqjl21f9mxipxmcq6fk6drq3c0g")))

(define-public crate-enum_ext-0.1.3 (c (n "enum_ext") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.64") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0js8k9lcc8pkq7vdkpd5ckcqw62rjhzfi3b7m2ix23baijnis92v")))

(define-public crate-enum_ext-0.2.0 (c (n "enum_ext") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.64") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1j0gi5p9dkjnfxz7zxcai3xri3if1kd9anz77wcy9fjv76v4g2b8")))

(define-public crate-enum_ext-0.2.1 (c (n "enum_ext") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.82") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.64") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "13amqxhqr500brp697qxmxbk7kzkga5ydp20ngpi9xr7fkdazk0l")))

(define-public crate-enum_ext-0.2.2 (c (n "enum_ext") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.84") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.66") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1vxswz922qpiwfyz38ii7zjjd13xp8qhbcdifnmpsx25v39ncqav")))

