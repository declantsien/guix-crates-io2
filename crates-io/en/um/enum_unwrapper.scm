(define-module (crates-io en um enum_unwrapper) #:use-module (crates-io))

(define-public crate-enum_unwrapper-0.1.0 (c (n "enum_unwrapper") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "187g5rnc3jlrw0hylb0m11qqajg8b1qf0f1dnysdwd2n802xqb5g") (y #t)))

(define-public crate-enum_unwrapper-0.1.1 (c (n "enum_unwrapper") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06vglrgw58fbc4pk1icbpqscffnvid1rvjvbchlnjlc3f3wqym53") (y #t)))

(define-public crate-enum_unwrapper-0.1.2 (c (n "enum_unwrapper") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0c2d8l875lwfybhfcnm1gbklj7lq1vcdz6rkp0hv818pvjbz9q3y")))

