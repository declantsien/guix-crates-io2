(define-module (crates-io en um enum-tryfrom-derive) #:use-module (crates-io))

(define-public crate-enum-tryfrom-derive-0.1.0 (c (n "enum-tryfrom-derive") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.136") (o #t) (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0rm5x4zkkvf34akq21bp22zj0z7dvbdx6np8xj3rvgx2n6is8qf6") (f (quote (("default"))))))

(define-public crate-enum-tryfrom-derive-0.1.1 (c (n "enum-tryfrom-derive") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.136") (o #t) (d #t) (k 0)) (d (n "enum-tryfrom") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "19l4byqrmn50c33q19017z2l5ycx9ivpj9ac6ii6bv59c95j4f1v") (f (quote (("default"))))))

(define-public crate-enum-tryfrom-derive-0.1.2 (c (n "enum-tryfrom-derive") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0.136") (o #t) (d #t) (k 0)) (d (n "enum-tryfrom") (r "^0.1") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0gq62hkrzb0mvd1cm3yqmjiazqqf0v2knrbijkkhj8s6gad13k4v") (f (quote (("default"))))))

(define-public crate-enum-tryfrom-derive-0.2.1 (c (n "enum-tryfrom-derive") (v "0.2.1") (d (list (d (n "enum-tryfrom") (r "^0.2") (d #t) (k 2)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "08khsll0gfff194wm9s6pv8hc6mj9prdqz6zf9dcx2jk8jp88f15") (f (quote (("default"))))))

