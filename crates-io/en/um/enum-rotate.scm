(define-module (crates-io en um enum-rotate) #:use-module (crates-io))

(define-public crate-enum-rotate-0.1.0 (c (n "enum-rotate") (v "0.1.0") (d (list (d (n "derive-enum-rotate") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.90") (d #t) (k 2)))) (h "0xksbnl48b8c0z229c9h29hv34k4lnqx37lhdf6nf8swbmjpai9l")))

(define-public crate-enum-rotate-0.1.1 (c (n "enum-rotate") (v "0.1.1") (d (list (d (n "derive-enum-rotate") (r "^0.1.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.90") (d #t) (k 2)))) (h "11fpz50q3sz307x4h2kbad34v303axvgwq7afgwjv8cr4575w11x")))

