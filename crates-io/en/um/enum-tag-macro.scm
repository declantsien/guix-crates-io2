(define-module (crates-io en um enum-tag-macro) #:use-module (crates-io))

(define-public crate-enum-tag-macro-0.1.0 (c (n "enum-tag-macro") (v "0.1.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (d #t) (k 0)))) (h "1zgj4f41r8410sidgk109kqinmhh8b8r1py9w7m1ngwja3kzg5jd")))

(define-public crate-enum-tag-macro-0.2.0 (c (n "enum-tag-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (d #t) (k 0)))) (h "0v02l3bc0fbm5v9jgw5mxx7nc8c36spxj0h1z2g2yagdq0macvrq")))

(define-public crate-enum-tag-macro-0.3.0 (c (n "enum-tag-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (d #t) (k 0)))) (h "0k2mpkyiyhijhw6k61jc35p2lng7ix3zas2qf1qjy90ywzw75k8w")))

