(define-module (crates-io en um enum-group) #:use-module (crates-io))

(define-public crate-enum-group-0.1.0 (c (n "enum-group") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "0jfzbcf8p4ww0sm7bj5dzkw7i65581z4d17mn96223h40di0jmcr")))

(define-public crate-enum-group-0.1.1 (c (n "enum-group") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "1kfqj3lawc0vivbymjgai8zsnybpbixsy5chbg1rgpp5vnz4payc")))

(define-public crate-enum-group-0.1.2 (c (n "enum-group") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing"))) (d #t) (k 0)))) (h "0641w9jy2vzwjagbmprxgdbkz47fqlpg80jmldvb7mw0xw76idmw")))

