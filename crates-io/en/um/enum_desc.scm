(define-module (crates-io en um enum_desc) #:use-module (crates-io))

(define-public crate-enum_desc-0.1.0 (c (n "enum_desc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("parsing" "extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0k0a8mch1zpb81lf7g5f2rf5f1faggl0mim286br3y0pzhvs59jp") (y #t)))

(define-public crate-enum_desc-0.1.1 (c (n "enum_desc") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("parsing" "extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "12zny1h5s31nbs4dp2q6dgmwjg6ma9cbrd7iq5xw6bidxicbx10q") (y #t)))

(define-public crate-enum_desc-0.1.3 (c (n "enum_desc") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("parsing" "extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "1sz51pmnmks5qapn4v3dz17rq92p766kyrn82cxdmkg6r4f4l0fk") (y #t)))

(define-public crate-enum_desc-0.1.4 (c (n "enum_desc") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("parsing" "extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0af6sj2gfr9awbf3ig10ypdci0fwdc6v6zpsq8xa7i25wrxsx90a") (y #t)))

(define-public crate-enum_desc-0.1.5 (c (n "enum_desc") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("parsing" "extra-traits" "full" "visit"))) (d #t) (k 0)))) (h "0y37jx8kznfjq4ibk0l7b3f717wkc0mrpp75limjg8p22jw5fdlk")))

