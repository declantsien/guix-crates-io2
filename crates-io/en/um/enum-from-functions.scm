(define-module (crates-io en um enum-from-functions) #:use-module (crates-io))

(define-public crate-enum-from-functions-0.0.0 (c (n "enum-from-functions") (v "0.0.0") (h "1yisaqhxlrb5zjnpkwxhbbgzdzfn78i551m49p79gcf5fmxp7hwq") (y #t)))

(define-public crate-enum-from-functions-0.1.0 (c (n "enum-from-functions") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0gs3zyqz7h3lsvylvxb2gm37zcyirwwv3h472hln8h0rgzb16qyq")))

(define-public crate-enum-from-functions-0.1.1 (c (n "enum-from-functions") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0p4g4iazbqrvc6mfpq0m8fq830wwgm7lcwxf76a3gqd2f73bngp9")))

(define-public crate-enum-from-functions-0.2.0 (c (n "enum-from-functions") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bh1rcylx9n9dw489hw7arnlrpvqhb2jg3skj03a40q65wx4gp4d")))

(define-public crate-enum-from-functions-0.3.0 (c (n "enum-from-functions") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.27") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mc95pw6k0ijbnh7a6zw17xvcl8jxnsw95qympicj2jxr4vl46js")))

