(define-module (crates-io en um enums_arena_defines) #:use-module (crates-io))

(define-public crate-enums_arena_defines-0.1.0 (c (n "enums_arena_defines") (v "0.1.0") (h "0p3pz5ibb53av8r8z3b0149ighrmarghicppm173c4ix9x3yizhv")))

(define-public crate-enums_arena_defines-0.1.2 (c (n "enums_arena_defines") (v "0.1.2") (h "17d651p8bwfr8jg0572lbj8ir738r08l2i4nz03vidnf85fj5qkw")))

