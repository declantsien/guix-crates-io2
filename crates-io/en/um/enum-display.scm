(define-module (crates-io en um enum-display) #:use-module (crates-io))

(define-public crate-enum-display-0.1.0 (c (n "enum-display") (v "0.1.0") (d (list (d (n "enum-display-derive") (r "^0.1.0") (d #t) (k 0)))) (h "167iy5ax2qv0p475y9ndq2868b7ndb22byc5zh4f2h3id5hyq1nf")))

(define-public crate-enum-display-0.1.1 (c (n "enum-display") (v "0.1.1") (d (list (d (n "enum-display-macro") (r "^0.1.1") (d #t) (k 0)))) (h "0f474kw2bwlm3lf4pvgk6219666jhzyidjf35y2vngj1491c9ysg")))

(define-public crate-enum-display-0.1.2 (c (n "enum-display") (v "0.1.2") (d (list (d (n "enum-display-macro") (r "^0.1.2") (d #t) (k 0)))) (h "0j77plp1wk9357xypdn65axb6s38y52428xd58h75ppqdlj1d2xb")))

(define-public crate-enum-display-0.1.3 (c (n "enum-display") (v "0.1.3") (d (list (d (n "enum-display-macro") (r "^0.1.3") (d #t) (k 0)))) (h "1qiz0k93l5vyxmhni9aa0nikg1js9qpjq3ks2yfrbladslrxzm4n")))

(define-public crate-enum-display-0.1.4 (c (n "enum-display") (v "0.1.4") (d (list (d (n "enum-display-macro") (r "^0.1.4") (d #t) (k 0)))) (h "093z856cdxxss44jpgch2mk51kfmgm1310pqka10a1ldbnr8n182")))

