(define-module (crates-io en um enum_cycling_derive) #:use-module (crates-io))

(define-public crate-enum_cycling_derive-0.1.0 (c (n "enum_cycling_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "0kpllyi0b98qbhii86c2lah1mqx6f75kqlr548ip2msl1gxzbc8s")))

(define-public crate-enum_cycling_derive-0.2.0 (c (n "enum_cycling_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "1s5j2cy4ns9vrx43cj52c8zfi7z379xcrpixh13c0q3cb13i9c6c")))

