(define-module (crates-io en um enum-utils-macros) #:use-module (crates-io))

(define-public crate-enum-utils-macros-0.1.0 (c (n "enum-utils-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "0bnzkpdsgnn7bxix2nw98kh9i0kza4q02srwlxs4bjcpsl3dajgl") (y #t)))

