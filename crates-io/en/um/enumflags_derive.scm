(define-module (crates-io en um enumflags_derive) #:use-module (crates-io))

(define-public crate-enumflags_derive-0.1.0 (c (n "enumflags_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "0vciw7rdbllcpk23ir875sp5hzpgagd8n9p5vcax5wf2mnf1rbnr")))

(define-public crate-enumflags_derive-0.1.1 (c (n "enumflags_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "176ip0wcjk51fvcb6a72a30c7xay4b5702ij2b7hjf47fc1d1jc6")))

(define-public crate-enumflags_derive-0.2.0 (c (n "enumflags_derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "1n3n6qm8ysl3argh2xqn92a18l87jiwpw00pp9cw9cggmxls0kh3")))

(define-public crate-enumflags_derive-0.2.1 (c (n "enumflags_derive") (v "0.2.1") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "1m6m5vds4kzma82ijg65i678rj52c9x8n5fm4df8761nky1fzn1k")))

(define-public crate-enumflags_derive-0.2.2 (c (n "enumflags_derive") (v "0.2.2") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "0ri4hgn46pf1412xb69vl821pg48qg8s3c4z1hdnssg71hp5vb26")))

(define-public crate-enumflags_derive-0.3.1 (c (n "enumflags_derive") (v "0.3.1") (d (list (d (n "quote") (r "^0.3.12") (d #t) (k 0)) (d (n "syn") (r "^0.11.4") (d #t) (k 0)))) (h "1sa93k200mgz1whd6cmi7j3djy5k9my7vgxgn942z6b2jivhdnvp") (f (quote (("nostd") ("default"))))))

(define-public crate-enumflags_derive-0.4.0 (c (n "enumflags_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12.5") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0qjc8lrljq7ycv2rxr4py7a26nv85myay5vyd4s5yr3q4c49cgpf") (f (quote (("nostd") ("default"))))))

(define-public crate-enumflags_derive-0.4.1 (c (n "enumflags_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12.5") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1459f7hjwwgfgkrg8z9q0cp1hh2237h426421kdpbp1rmzy4hrhf") (f (quote (("nostd") ("default"))))))

(define-public crate-enumflags_derive-0.4.2 (c (n "enumflags_derive") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12.5") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0lah4pkys87s6a8ccw5cg9y3xg262k8gk9gj1d738qxhhyp4dx0m") (f (quote (("nostd") ("default"))))))

