(define-module (crates-io en um enumeraties) #:use-module (crates-io))

(define-public crate-enumeraties-0.1.0 (c (n "enumeraties") (v "0.1.0") (d (list (d (n "enum-map") (r "^0.6") (d #t) (k 2)) (d (n "enum_properties") (r "^0.3.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "011h9jzg867zl07y7xjgi75iwl1gdvh6w0yd6gmlx1pm3c4w12z1") (f (quote (("default") ("bench"))))))

