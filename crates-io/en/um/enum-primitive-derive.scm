(define-module (crates-io en um enum-primitive-derive) #:use-module (crates-io))

(define-public crate-enum-primitive-derive-0.1.0 (c (n "enum-primitive-derive") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1y44lb899r7w4vn1p8j75vr92qsid5pgpihsqqd98vdj1wig3624")))

(define-public crate-enum-primitive-derive-0.1.1 (c (n "enum-primitive-derive") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1n8fqm70ph78pw63arail35cz7hv3bhg3v17sp8gy9wx4f9rzxzg")))

(define-public crate-enum-primitive-derive-0.1.2 (c (n "enum-primitive-derive") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1p8jg302b0sv2sw2y3r4cdgsz2zyrdx673f7r1j1hb661r90xfg2")))

(define-public crate-enum-primitive-derive-0.2.0 (c (n "enum-primitive-derive") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1rbmrfa551i8nv3i3f66c6mgan20xd6b7haaijjjkwp6vxb17chx") (y #t)))

(define-public crate-enum-primitive-derive-0.2.1 (c (n "enum-primitive-derive") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0h1k2izdl5lf2na8xnj7rgwjm8gawxc2p1w8j6ahifvyka7jhljz")))

(define-public crate-enum-primitive-derive-0.2.2 (c (n "enum-primitive-derive") (v "0.2.2") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "03ibjjx8dc4akpq8ck24qda5ix4jybz9jagfxykd0s6vxb2vjxf3")))

(define-public crate-enum-primitive-derive-0.3.0 (c (n "enum-primitive-derive") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0k6wcf58h5kh64yq5nfq71va53kaya0kzxwsjwbgwm2n2zd9axxs") (r "1.56")))

