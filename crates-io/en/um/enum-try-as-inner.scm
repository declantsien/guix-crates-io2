(define-module (crates-io en um enum-try-as-inner) #:use-module (crates-io))

(define-public crate-enum-try-as-inner-0.1.0 (c (n "enum-try-as-inner") (v "0.1.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "05ibh0fpbbbccp6v6agsfbfsvkp15nhv7ki8dsfghnmkbyplc258")))

(define-public crate-enum-try-as-inner-0.1.1 (c (n "enum-try-as-inner") (v "0.1.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "17in35lb25fxhrfbd1fd5pgz49nspnkdbsj4ld5b0f1kfp18yi0c")))

