(define-module (crates-io en um enum2repr) #:use-module (crates-io))

(define-public crate-enum2repr-0.1.9 (c (n "enum2repr") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xc5cbazazd0n44sj0ar5rvpv9110z924sxxz6bn5mrfhxjnm5ac") (y #t)))

(define-public crate-enum2repr-0.1.10 (c (n "enum2repr") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0glmv4544rvx5prb0l9lk3dqza6hhnkyfb105dcsy5rg71xybqr0") (y #t)))

(define-public crate-enum2repr-0.1.11 (c (n "enum2repr") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cjvcwg3vvmsvviw3yqyb5x6rj1q9s45ar7g5fpc0hxla5v8ywvg")))

(define-public crate-enum2repr-0.1.12 (c (n "enum2repr") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19garyqzc16bhq0prpw3fnh4vjvv1q3kjm8xj2dfq4bpf2jmbnf3")))

(define-public crate-enum2repr-0.1.13 (c (n "enum2repr") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18rw4gh8vh6krq4i2ix4cbd3nnzs554747xgxd4a1qckb0m68amx")))

(define-public crate-enum2repr-0.1.14 (c (n "enum2repr") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)))) (h "1xpdlgyi7rjhfyrlz354r6aw61sf16nbcllqyh5lw6aziadgvx1j")))

