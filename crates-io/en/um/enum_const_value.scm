(define-module (crates-io en um enum_const_value) #:use-module (crates-io))

(define-public crate-enum_const_value-0.1.0 (c (n "enum_const_value") (v "0.1.0") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0dxi4cwn77v4ch3lrhd0yrdipylz9sljim1w6y4sjy593ds9664q")))

(define-public crate-enum_const_value-0.2.0 (c (n "enum_const_value") (v "0.2.0") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0szxs51favy0mjq1d3rxp76632zdwb8hpkbw4wywm3b145asiwkd")))

(define-public crate-enum_const_value-0.3.0 (c (n "enum_const_value") (v "0.3.0") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qzf48rlfrcgf70780fbhmqf1n3fg6j59pk4l946ck6kps43sqwp")))

(define-public crate-enum_const_value-0.4.0 (c (n "enum_const_value") (v "0.4.0") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bswxqvka8cxlwrv5alwr32p4qb59mxmzqiiyzgvmfv445vycjpn")))

(define-public crate-enum_const_value-0.4.1 (c (n "enum_const_value") (v "0.4.1") (d (list (d (n "proc-macro2") (r "1.0.*") (d #t) (k 0)) (d (n "proc_macro2_helper") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "1.0.*") (d #t) (k 0)) (d (n "syn") (r "1.0.*") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fc3sba8ccjxn3vbbxfxmg5qlx7x41kpcmdn6khkngrh8dxsm0ns")))

(define-public crate-enum_const_value-0.4.2 (c (n "enum_const_value") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro2_helper") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0s2qj6c0n9l75854kx8rs18fwwzizn553wy8kky0sra44v3ygbp7")))

(define-public crate-enum_const_value-0.4.3 (c (n "enum_const_value") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc_macro2_helper") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17bsl87cf5zr3bh0gshad9a7lkbllw24zvxpw164k5ir3grlb73r")))

