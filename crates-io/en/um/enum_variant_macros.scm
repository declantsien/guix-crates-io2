(define-module (crates-io en um enum_variant_macros) #:use-module (crates-io))

(define-public crate-enum_variant_macros-0.1.0 (c (n "enum_variant_macros") (v "0.1.0") (d (list (d (n "enum_variant_macros_macros") (r "^0.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.22") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1rqlsfhizy2ph4g74c82a3vjp3awvss6x4bj6wwi8wk1qv59y61h")))

(define-public crate-enum_variant_macros-0.2.0 (c (n "enum_variant_macros") (v "0.2.0") (d (list (d (n "enum_variant_macros_macros") (r "^0.2.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0pzsjbl11ndj2vk5y3jz7nm66am0li0z7n60m8xb7rwwqqpbr3jw")))

(define-public crate-enum_variant_macros-0.3.0 (c (n "enum_variant_macros") (v "0.3.0") (d (list (d (n "enum_variant_macros_macros") (r "^0.3.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1w5sv8xy8svxjqzjl1p87da7537m668k4xng3cvi15b9is5lca62")))

