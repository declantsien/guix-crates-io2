(define-module (crates-io en um enum2contract) #:use-module (crates-io))

(define-public crate-enum2contract-0.1.0 (c (n "enum2contract") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "09b5pa9wlankgz746miid99il5gafmzjd9sy7a072dxlpz3pdlmy")))

(define-public crate-enum2contract-0.1.1 (c (n "enum2contract") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03jmnzvmn994lhp5hzh30i7imc33awsdlzp2bnljrwhpdsbsc40i")))

(define-public crate-enum2contract-0.1.2 (c (n "enum2contract") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (o #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1lykpghjihycidk6knkkp8s1jyi7by7rgs4y9n2w2axy2xscd854") (f (quote (("serde-derive" "serde") ("default"))))))

(define-public crate-enum2contract-0.1.3 (c (n "enum2contract") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.94") (f (quote ("alloc"))) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07sxd8yg829rdhh722rrz71jmdg4k8yvqch02x82y709japh6ffj")))

(define-public crate-enum2contract-0.1.4 (c (n "enum2contract") (v "0.1.4") (d (list (d (n "postcard") (r "^1.0.4") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.94") (f (quote ("alloc"))) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hrpbnlwazq6rlwg0bzqiqacn09800d1fq4a4in03dbwl5bl61m4")))

(define-public crate-enum2contract-0.1.5 (c (n "enum2contract") (v "0.1.5") (d (list (d (n "postcard") (r "^1.0.4") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.94") (f (quote ("alloc"))) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1gfw1wghngigsgg69kwx0335h5ffka3ha224f2zw2y6584dkvm76")))

(define-public crate-enum2contract-0.1.6 (c (n "enum2contract") (v "0.1.6") (d (list (d (n "postcard") (r "^1.0.4") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.94") (f (quote ("alloc"))) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0awx2s1igzgmj3jd9451vl1c5ndbns1r0l2y4cdrp3ykl7s23haz")))

