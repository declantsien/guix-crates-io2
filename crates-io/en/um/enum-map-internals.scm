(define-module (crates-io en um enum-map-internals) #:use-module (crates-io))

(define-public crate-enum-map-internals-0.1.0 (c (n "enum-map-internals") (v "0.1.0") (d (list (d (n "array-macro") (r "^1.0.0") (d #t) (k 0)) (d (n "enum-map") (r "^0.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.16") (o #t) (k 0)))) (h "1vcrprq7vpl4xryac5abilh4dp4ap1zxqq7gd469wcrb33dazm8s")))

(define-public crate-enum-map-internals-0.1.1 (c (n "enum-map-internals") (v "0.1.1") (d (list (d (n "array-macro") (r "^1.0.0") (d #t) (k 0)) (d (n "enum-map") (r "^0.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.16") (o #t) (k 0)))) (h "0vis62869l6jsqlvz4jvcwvwmhgc9jmp652zk4ca999xmfgjvir5")))

(define-public crate-enum-map-internals-0.1.2 (c (n "enum-map-internals") (v "0.1.2") (d (list (d (n "array-macro") (r "^1.0.0") (d #t) (k 0)) (d (n "enum-map") (r "^0.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.16") (o #t) (k 0)))) (h "1s637bc0fkg6ncm792q42k4gaagsy31yycj0x26a3ax77v7vmc1q")))

(define-public crate-enum-map-internals-0.1.3 (c (n "enum-map-internals") (v "0.1.3") (d (list (d (n "array-macro") (r "= 1.0.3") (d #t) (k 0)) (d (n "enum-map") (r "^0.5.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.16") (o #t) (k 0)))) (h "1fs6qdbbmlm3a9ynhajkhlkijkjcdcy00m1ydxjc27ldw9l6shyj") (y #t)))

(define-public crate-enum-map-internals-0.2.0 (c (n "enum-map-internals") (v "0.2.0") (d (list (d (n "array-macro") (r "^1.0.4") (d #t) (k 0)) (d (n "enum-map") (r "^0.6.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.16") (o #t) (k 0)))) (h "1fa0nsdiq4x16hd3in1vp1mv61i0vqplgjfhs6zcyis3y67575si")))

(define-public crate-enum-map-internals-0.2.1 (c (n "enum-map-internals") (v "0.2.1") (d (list (d (n "array-macro") (r "^1.0.4") (d #t) (k 0)) (d (n "enum-map") (r "^0.6.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.16") (o #t) (k 0)))) (h "0465nk6gr3m0xfrcb745h2vrvb7w598a3hmrvlv1na3mbijq2zwd")))

(define-public crate-enum-map-internals-0.2.2 (c (n "enum-map-internals") (v "0.2.2") (d (list (d (n "array-macro") (r "^1.0.4") (d #t) (k 0)) (d (n "enum-map") (r "^0.6.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.16") (o #t) (k 0)))) (h "079dbi282rryscm00ski9icz0cdhhkg9i69bfvjzcjr8qqmzdxqp")))

