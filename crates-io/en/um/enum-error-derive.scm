(define-module (crates-io en um enum-error-derive) #:use-module (crates-io))

(define-public crate-enum-error-derive-0.1.0 (c (n "enum-error-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.3") (d #t) (k 0)))) (h "0xays7qa76brg0qc23w8gvm4hchicl0z8f8hj4v9d3xy4z03pxr6")))

(define-public crate-enum-error-derive-0.1.1 (c (n "enum-error-derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.3") (d #t) (k 0)))) (h "1w045jqxxphlf5k5y20dmf1c2ylfsdkwifsgnpp9kfq6bjkgg7pi")))

