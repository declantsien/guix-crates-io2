(define-module (crates-io en um enum_to_u8_slice_derive) #:use-module (crates-io))

(define-public crate-enum_to_u8_slice_derive-0.1.0 (c (n "enum_to_u8_slice_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1avb25kmhclrhrsk47xnavix97j6js0gvlf9d84m4x3gnhsnxha8")))

(define-public crate-enum_to_u8_slice_derive-0.1.1 (c (n "enum_to_u8_slice_derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0hvzi74pnh5a7f4klrk0dz45l0mgcy5l3zajjhjsxzws28js4yc4")))

(define-public crate-enum_to_u8_slice_derive-0.2.0 (c (n "enum_to_u8_slice_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ab19saqm5vdhz15gshpj5xj2p6j9i3708mya25vdgyrmb7y1kpc")))

