(define-module (crates-io en um enumerare-macros) #:use-module (crates-io))

(define-public crate-enumerare-macros-0.1.0 (c (n "enumerare-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (d #t) (k 0)))) (h "0yvrc68ng8z94qcjnnmbk4fdlfxiaxk5dqfxrglljhhqszv859c8")))

