(define-module (crates-io en um enumorph) #:use-module (crates-io))

(define-public crate-enumorph-0.1.0 (c (n "enumorph") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "0kk99ls54liqd7jk39zgd113dkiwy108m76f2fgwbcajw96mg5n8")))

(define-public crate-enumorph-0.1.1 (c (n "enumorph") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "15jfhnaf92s1a9wqm055mbb5nbg5w4gxix2qyd5xyn0kvqi0ni8k")))

(define-public crate-enumorph-0.1.2 (c (n "enumorph") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.85") (d #t) (k 2)))) (h "093891sf7gjp3bkxbqsm1jczgsy6a0345yrkpz1iy2iw9482cgkd")))

