(define-module (crates-io en um enum2contract-derive) #:use-module (crates-io))

(define-public crate-enum2contract-derive-0.1.7 (c (n "enum2contract-derive") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0asl6cvwa425jrqqyaxr6a6pkdynq1cg0d2g5ykfmsbfzvm5k2z1") (f (quote (("json") ("binary"))))))

