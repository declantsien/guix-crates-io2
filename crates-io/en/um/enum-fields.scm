(define-module (crates-io en um enum-fields) #:use-module (crates-io))

(define-public crate-enum-fields-0.1.0 (c (n "enum-fields") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.58") (d #t) (k 0)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "0pr75l6c5m0gdnkp36bmz3jv9xfjjfnlzab562wa5f4zgrkkpayn")))

