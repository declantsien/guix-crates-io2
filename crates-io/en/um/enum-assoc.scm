(define-module (crates-io en um enum-assoc) #:use-module (crates-io))

(define-public crate-enum-assoc-0.1.0 (c (n "enum-assoc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18d6r7m91c3v03kd8baa4z93wy35fk1pzs0p5bkmqa148bjc0fpj")))

(define-public crate-enum-assoc-0.1.1 (c (n "enum-assoc") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1m8k9vs62m5fsq01kllwnwfvwmqszn5ckspc7wzfjc8yyac74qg9")))

(define-public crate-enum-assoc-0.1.2 (c (n "enum-assoc") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1xa5k4rdqlqj0a14h1z0c4sgxw2kj5d5mc422xf5pvws3qm4b36d")))

(define-public crate-enum-assoc-0.1.3 (c (n "enum-assoc") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cinq0mlbjr21l13n11zl95dwdyqhnrmhn8h2ddkf0wasbmbmfhp")))

(define-public crate-enum-assoc-0.1.4 (c (n "enum-assoc") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lms0p9mnd3yl436p0alikkxffw3k3hraxmwx2m5xghbx8cigi7g")))

(define-public crate-enum-assoc-0.1.5 (c (n "enum-assoc") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nwjawm63mi54kl3zh3iqs71x5qcy9yjgd1yrzhxpjb9pc4b66dr")))

(define-public crate-enum-assoc-0.1.6 (c (n "enum-assoc") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bs7qmggzi05rr2i4q8mhgzqcvdp1z77ylic4lh8p92zvbvwdqwk")))

(define-public crate-enum-assoc-0.1.7 (c (n "enum-assoc") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0823g69l76jhn4mnilb1z4fnmcgrdl24i9ymh5001zj4fas78wq7")))

(define-public crate-enum-assoc-0.1.8 (c (n "enum-assoc") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0svj1cj67bg9wg3r46dmwfgym7zrqwcv4p1hhmz2rlcbs5f04l0g")))

(define-public crate-enum-assoc-0.2.0 (c (n "enum-assoc") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18ydiybgi31xjcplvavjyyij8sniw1x6kria88v6ybix0q90sr4n") (y #t)))

(define-public crate-enum-assoc-0.2.1 (c (n "enum-assoc") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bxarbwxfs36iq8i5gv42qdcxdsm69lpl2p5rvnw0xj22yb8ki0n") (y #t)))

(define-public crate-enum-assoc-0.2.2 (c (n "enum-assoc") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06p97h9gchyrsjdhii86zm52i2n1ks776yy9gjndf3r2bfmvfdam")))

(define-public crate-enum-assoc-0.3.0 (c (n "enum-assoc") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fd1h2apfkw2sfcwlvpjrffclj40yy7brjkgq6hw5zdmzaj2y5kl")))

(define-public crate-enum-assoc-0.3.1 (c (n "enum-assoc") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18rwfadgkxq8d7hnmraya815ai0ik03j5qaiy9x16k1dy2shxn8r")))

(define-public crate-enum-assoc-0.3.2 (c (n "enum-assoc") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1l2mm1fdxjg68c4yw57gw1id8nn516rz0952nk3cz52cffi89y34")))

(define-public crate-enum-assoc-0.3.3 (c (n "enum-assoc") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16r282bj91d7b4js3gg9vqipa4i1dfysks7sipbringx2ppvcirr")))

(define-public crate-enum-assoc-0.3.4 (c (n "enum-assoc") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hdjjb26dx67yv259rfb95m4xm940ggsj249sisi2wyhyqskrhj1")))

(define-public crate-enum-assoc-0.4.0 (c (n "enum-assoc") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vd0h5628z6q4zfqiy0bls9mp9pc8ic73rz7nc4q0cc17l2lc3kf")))

(define-public crate-enum-assoc-1.0.0 (c (n "enum-assoc") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mfi3jscgxazvgwkk94bsqa18307s73ygaxqw4lnypxb7v7x4gkn")))

(define-public crate-enum-assoc-1.1.0 (c (n "enum-assoc") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01wxbfp6sy2c150f8xkp0q3238dim9lx702bbbf055bvsf4pn914")))

