(define-module (crates-io en um enum2pos) #:use-module (crates-io))

(define-public crate-enum2pos-0.1.0 (c (n "enum2pos") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01nh6nckra4cjzqs2s7pi0jyvrzcqwgcggvif8b5iy3zk5c29f6x")))

(define-public crate-enum2pos-0.1.1 (c (n "enum2pos") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bdxfj52cpjbx16sk8qrvdqmv5cg18klawyfwy34hfz4p4vhh9jc")))

