(define-module (crates-io en um enum-kinds-traits) #:use-module (crates-io))

(define-public crate-enum-kinds-traits-0.1.0 (c (n "enum-kinds-traits") (v "0.1.0") (h "0z8j3jqmv7incfaillvccdx0ax13r5fv578l2w3bayw0lj8c55fc")))

(define-public crate-enum-kinds-traits-0.1.1 (c (n "enum-kinds-traits") (v "0.1.1") (h "1inrwar69zr8wkk12yw5zbhhcwmf8lbaz1za0jxslxiwh4657k62")))

(define-public crate-enum-kinds-traits-0.1.2 (c (n "enum-kinds-traits") (v "0.1.2") (h "1f8hz853ic7mdimimcd77023vqsl63dg582y59zxks91ydvm8pjc")))

