(define-module (crates-io en um enum_to_str_derive) #:use-module (crates-io))

(define-public crate-enum_to_str_derive-0.1.0 (c (n "enum_to_str_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0anj8idazdzyj04skaib6v5p6xgsb3g8rz64cxyi6bivdlbqlnxa")))

(define-public crate-enum_to_str_derive-0.2.0 (c (n "enum_to_str_derive") (v "0.2.0") (d (list (d (n "proc_macro_helper") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0pr0jy4ckaq4isj3f2hdls2n31579560xcc0gf9q8bcqfv3s92cx")))

(define-public crate-enum_to_str_derive-0.2.1 (c (n "enum_to_str_derive") (v "0.2.1") (d (list (d (n "proc_macro_helper") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1v6mfl9nx01ah5z55f43l5mlacqhpqkzxzyw6rhg557qcmf619zf")))

(define-public crate-enum_to_str_derive-0.3.0 (c (n "enum_to_str_derive") (v "0.3.0") (d (list (d (n "proc_macro_helper") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1wm3fsny3hy60asll4y722yyfqzvbxhks38zpnbg6jr2lfv1c180")))

