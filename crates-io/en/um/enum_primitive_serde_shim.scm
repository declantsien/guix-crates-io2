(define-module (crates-io en um enum_primitive_serde_shim) #:use-module (crates-io))

(define-public crate-enum_primitive_serde_shim-0.2.0 (c (n "enum_primitive_serde_shim") (v "0.2.0") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1smbqjzdn718m9vmya6xb9m7gzplj0d4ffi2zfvjk0963hbxj41b")))

(define-public crate-enum_primitive_serde_shim-0.2.1 (c (n "enum_primitive_serde_shim") (v "0.2.1") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "178mzkl9y0a474jhw048dbvrkpmgp97mp94cym7sbz0wm0jh3sgb")))

(define-public crate-enum_primitive_serde_shim-0.2.2 (c (n "enum_primitive_serde_shim") (v "0.2.2") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1i8xv4k4l23qs2vafigmj1jzbm70sjalchxlg3gdks12b549yahx") (f (quote (("std") ("default" "std"))))))

(define-public crate-enum_primitive_serde_shim-0.2.3 (c (n "enum_primitive_serde_shim") (v "0.2.3") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "074g3w3zdq7da8snwg01xzj7bziv1h380mb2dcqv9fshrry1wqv8") (f (quote (("std") ("default" "std"))))))

