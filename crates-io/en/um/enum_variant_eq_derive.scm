(define-module (crates-io en um enum_variant_eq_derive) #:use-module (crates-io))

(define-public crate-enum_variant_eq_derive-0.1.0 (c (n "enum_variant_eq_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0iz51ykajnszpd59gaxkvihixc8x3xn3w9q94v7cd3z93k802vdd")))

(define-public crate-enum_variant_eq_derive-0.1.1 (c (n "enum_variant_eq_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nx98qvh8p3hjgc1z7h6nqc99pqqnrryzi3m9b1rgaxns6jf7b9j")))

(define-public crate-enum_variant_eq_derive-0.1.2 (c (n "enum_variant_eq_derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1gqzllf2bixlj09w7z5h47d2ih9cyhb7y5x6iaq8naakv2a67izb")))

