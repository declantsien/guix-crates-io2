(define-module (crates-io en um enumset_derive) #:use-module (crates-io))

(define-public crate-enumset_derive-0.1.0 (c (n "enumset_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (d #t) (k 0)))) (h "1829jqic6c14mcp32hdsx8kd229znv1601xwcmsplzy49m6wc7rw") (f (quote (("nightly" "proc-macro2/nightly"))))))

(define-public crate-enumset_derive-0.2.0 (c (n "enumset_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.21") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (d #t) (k 0)))) (h "125pmm7kby0axy3s4j7xlzbw3a7l3p3hj0vz6d7agpldmia0wc14") (f (quote (("nightly" "proc-macro2/nightly"))))))

(define-public crate-enumset_derive-0.3.0 (c (n "enumset_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.21") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (d #t) (k 0)))) (h "0dpbkjxzscfpj87grq9l2wga1w2w6fzgxzsr3hl7vjbv73dmq2zr") (f (quote (("serde") ("nightly" "proc-macro2/nightly"))))))

(define-public crate-enumset_derive-0.3.1 (c (n "enumset_derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^0.4.21") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (d #t) (k 0)))) (h "1wkpnjykkz63f5v8niaqnjil4y1p3qn49awc8qqsnr32y81213fp") (f (quote (("serde") ("nightly" "proc-macro2/nightly"))))))

(define-public crate-enumset_derive-0.3.2 (c (n "enumset_derive") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^0.4.21") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (d #t) (k 0)))) (h "08zp3bhw53ix1g5fqak9cgvgc2adixzjrlpxwsq35rb8rxvmqgpp") (f (quote (("serde") ("nightly" "proc-macro2/nightly"))))))

(define-public crate-enumset_derive-0.4.0 (c (n "enumset_derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.21") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (d #t) (k 0)))) (h "0jr89i4x99rckak2p9fhz5dyzz9lyzxf451adi94lalrda93pn81") (f (quote (("serde") ("nightly" "proc-macro2/nightly" "darling/diagnostics"))))))

(define-public crate-enumset_derive-0.4.1 (c (n "enumset_derive") (v "0.4.1") (d (list (d (n "darling") (r "^0.9.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.21") (d #t) (k 0)) (d (n "quote") (r "^0.6.9") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (d #t) (k 0)))) (h "1q4frlf8ajgclsllhx27c60rbfnlf32iczsx07va7kd6s1by3v5f") (f (quote (("serde") ("nightly" "proc-macro2/nightly" "darling/diagnostics"))))))

(define-public crate-enumset_derive-0.4.2 (c (n "enumset_derive") (v "0.4.2") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "17vc8v8wv1pn836d9f1785l01iwb4kc3dmqxsh708r3g0ldkg6hc") (f (quote (("serde") ("nightly" "proc-macro2/nightly" "darling/diagnostics"))))))

(define-public crate-enumset_derive-0.4.3 (c (n "enumset_derive") (v "0.4.3") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "163m8hdhm1ig7pqjk5frah323aihhp3ij6j881jfpgjbf78c515i") (f (quote (("serde") ("nightly" "proc-macro2/nightly" "darling/diagnostics"))))))

(define-public crate-enumset_derive-0.4.4 (c (n "enumset_derive") (v "0.4.4") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0qkar29h2dscvwly0cs60jiy6v6s27lz01pyw2wwxmf7zin7h6km") (f (quote (("serde") ("nightly" "proc-macro2/nightly" "darling/diagnostics"))))))

(define-public crate-enumset_derive-0.5.0 (c (n "enumset_derive") (v "0.5.0") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1b3mm2aq1vwlbnjhj3m015mj889ilfdszmv8yxf0r0kimhvg9gkl") (f (quote (("serde"))))))

(define-public crate-enumset_derive-0.5.1 (c (n "enumset_derive") (v "0.5.1") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1zwkhcpvs2svk1yrqi08acilibq18mpn1jksxyzxym0y9l5aph37") (f (quote (("serde"))))))

(define-public crate-enumset_derive-0.5.2 (c (n "enumset_derive") (v "0.5.2") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0dah3d67rhqqpp7b6s6c2m92qv02z29j2p8h3lpzhqi8klinx55k") (f (quote (("serde"))))))

(define-public crate-enumset_derive-0.5.3 (c (n "enumset_derive") (v "0.5.3") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0v3x42b1cnc6hj5mx8ga86vl71bqiny6xj4am9jn3sviwjy7k2hd") (f (quote (("serde"))))))

(define-public crate-enumset_derive-0.5.4 (c (n "enumset_derive") (v "0.5.4") (d (list (d (n "darling") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0iwx4raz55rqwdl8n6mb6866jzmh0hdggnh4vil8lg2hxkwm5771") (f (quote (("serde"))))))

(define-public crate-enumset_derive-0.5.5 (c (n "enumset_derive") (v "0.5.5") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1m7ccz9fcxsx3s1drj77psk62xfgjia0hp9lal3qhpb5ls514lb4") (f (quote (("serde"))))))

(define-public crate-enumset_derive-0.5.6 (c (n "enumset_derive") (v "0.5.6") (d (list (d (n "darling") (r "^0.13.0") (k 0)) (d (n "proc-macro-crate") (r "^1.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "18w5rmvq6bj0p9c7srfcwkna4by9cwqg02fb6h2a1csaz5qlhnaw") (f (quote (("serde")))) (y #t)))

(define-public crate-enumset_derive-0.5.7 (c (n "enumset_derive") (v "0.5.7") (d (list (d (n "darling") (r "^0.13.0") (k 0)) (d (n "proc-macro-crate") (r "^1.1.3") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0n00canivk9kgx7cg2p99iwcmyxi8fb6n77bg0a6pvjxkafpf0dj") (f (quote (("serde"))))))

(define-public crate-enumset_derive-0.6.0 (c (n "enumset_derive") (v "0.6.0") (d (list (d (n "darling") (r "^0.13.0") (k 0)) (d (n "proc-macro-crate") (r "^1.1.3") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0k1hhph1kjkw1p726fk9f4nxz3kgngm1grxypk7rr68xvkxs70za") (f (quote (("serde"))))))

(define-public crate-enumset_derive-0.6.1 (c (n "enumset_derive") (v "0.6.1") (d (list (d (n "darling") (r "^0.14") (k 0)) (d (n "proc-macro-crate") (r "^1") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1q5k98v0nbw9dw9gb51almh1b30n61is90wbm07vyyd2xd8vbrq3") (f (quote (("serde"))))))

(define-public crate-enumset_derive-0.7.0 (c (n "enumset_derive") (v "0.7.0") (d (list (d (n "darling") (r "^0.20") (k 0)) (d (n "proc-macro-crate") (r "^1") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1mix0fkq3029m660ljy2bnq9r49p8w3s7g5bn8mrlk3sy4pqbhhl") (f (quote (("serde"))))))

(define-public crate-enumset_derive-0.8.0 (c (n "enumset_derive") (v "0.8.0") (d (list (d (n "darling") (r "^0.20") (k 0)) (d (n "proc-macro-crate") (r "^1") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1qkbhijjdp9h7mb7pw8wxyljds5pzcab6mk08rld7pazb2b59cck") (f (quote (("serde"))))))

(define-public crate-enumset_derive-0.8.1 (c (n "enumset_derive") (v "0.8.1") (d (list (d (n "darling") (r "^0.20") (k 0)) (d (n "proc-macro-crate") (r "^1") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1bykfx8qm48payzbksna5vg1ddxbgc6a2jwn8j4g0w1dp1m6r2z0") (f (quote (("serde"))))))

