(define-module (crates-io en um enum-as-derive) #:use-module (crates-io))

(define-public crate-enum-as-derive-0.1.0 (c (n "enum-as-derive") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v1ybyimsligr3m10acdvljpyzjj0qhwih9gb9mnhakm9ilm54x0")))

(define-public crate-enum-as-derive-0.1.1 (c (n "enum-as-derive") (v "0.1.1") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.39") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c9zs018nhzrlgpd2q0v9yrawpl4q7pmyc8cs5ikk1dwdsczsxhz")))

