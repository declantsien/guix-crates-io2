(define-module (crates-io en um enumx) #:use-module (crates-io))

(define-public crate-enumx-0.0.1 (c (n "enumx") (v "0.0.1") (h "1mqf0lcr5fs9af1pg4z6pkh180qlcf2h6f42ifhnzncbm9j02bwg")))

(define-public crate-enumx-0.1.0 (c (n "enumx") (v "0.1.0") (d (list (d (n "enumx_derive") (r "^0.1") (d #t) (k 0)))) (h "106q3l1523j96jp76mb2g5sm5xy87yx8a59lbx59j8q1bgqkaslm")))

(define-public crate-enumx-0.1.1 (c (n "enumx") (v "0.1.1") (d (list (d (n "enumx_derive") (r "^0.1") (d #t) (k 0)))) (h "0czj7c45qcbp2xhvcgyq516cvmqg6p49wlhwzzc4kc7saamapc5f")))

(define-public crate-enumx-0.2.0 (c (n "enumx") (v "0.2.0") (d (list (d (n "enumx_derive") (r "^0.2") (d #t) (k 2)))) (h "0wzj8vv7cfdznin6wrvn52rslis373p05y6q7qly4sx6mhhy4nbx") (f (quote (("enum17_enum32"))))))

(define-public crate-enumx-0.2.1 (c (n "enumx") (v "0.2.1") (d (list (d (n "enumx_derive") (r "^0.2.1") (d #t) (k 2)))) (h "040sc0lc8a39pidh7ywd7xnza8ljrh0mjhcl1hwci4pjx7m1nqch") (f (quote (("enum32"))))))

(define-public crate-enumx-0.3.0-alpha (c (n "enumx") (v "0.3.0-alpha") (d (list (d (n "enumx_derive") (r "^0.3.0-alpha") (d #t) (k 2)))) (h "06sg1s7fgwmahxh80bq14v4fzqwkfby5lji43aa5qw7b0x1ljxz4")))

(define-public crate-enumx-0.3.0 (c (n "enumx") (v "0.3.0") (d (list (d (n "enumx_derive") (r "^0.3") (d #t) (k 2)))) (h "1bbkw0148bprk0mbbppb0v5g7ii8qrqhx32cir0lbl4kza9s17i5")))

(define-public crate-enumx-0.3.1 (c (n "enumx") (v "0.3.1") (d (list (d (n "enumx_derive") (r "^0.3.1") (d #t) (k 0)))) (h "1q8w1fq3fbmv4l6drrkmlricb59czs0dfjp75f2jl2m3wb14v841")))

(define-public crate-enumx-0.4.0 (c (n "enumx") (v "0.4.0") (d (list (d (n "enumx_derive") (r "^0.4") (d #t) (k 0)))) (h "06wsgrqw5kpf1xbacg25ywz2g333cyvf2xv519q8gsyhzfi4wcy1") (f (quote (("unstable") ("enum32") ("enum16") ("default" "enum16"))))))

(define-public crate-enumx-0.4.1 (c (n "enumx") (v "0.4.1") (d (list (d (n "enumx_derive") (r "^0.4") (d #t) (k 0)))) (h "0clg16karqjz6qpq8yjrjvpr5afm3ixr9nffbp7jh3m8f8qs85g0") (f (quote (("unstable") ("enum32") ("enum16") ("default" "enum16"))))))

(define-public crate-enumx-0.4.2 (c (n "enumx") (v "0.4.2") (d (list (d (n "enumx_derive") (r "^0.4.2") (d #t) (k 0)))) (h "1m7jfic6zqkmzqn86ivvc13n50cchsvcmy334k2j8r7876w3l9j8") (f (quote (("unstable") ("enum32") ("enum16") ("default" "enum16"))))))

(define-public crate-enumx-0.4.3 (c (n "enumx") (v "0.4.3") (d (list (d (n "enumx_derive") (r "^0.4.2") (d #t) (k 0)))) (h "0gv6wan5yb8ki2y062jd29fla7vh867ya5ddp31f4zsgn6z5m1rj") (f (quote (("unstable") ("enum32") ("enum16") ("default" "enum16"))))))

