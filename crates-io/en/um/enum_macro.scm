(define-module (crates-io en um enum_macro) #:use-module (crates-io))

(define-public crate-enum_macro-0.1.0 (c (n "enum_macro") (v "0.1.0") (h "18hq53qq19nj9yd7a9laa06zkpq1nb2xswmi0cphv28j8x3jw0b4")))

(define-public crate-enum_macro-0.1.1 (c (n "enum_macro") (v "0.1.1") (h "022llfqk04l6n82w5km365n9vmvha06xfl6f80zp9csdkvwmsw12")))

(define-public crate-enum_macro-0.1.2 (c (n "enum_macro") (v "0.1.2") (h "1vwwyyx9v9ja30by7hwn37ba78g0izpxv0w5gg57igd7q0jqdccv")))

(define-public crate-enum_macro-0.2.0 (c (n "enum_macro") (v "0.2.0") (h "0pvhy70xdq12c0l4q73r85ff6kjvdydvj92fcy481l28s9hcvghv")))

(define-public crate-enum_macro-0.3.0 (c (n "enum_macro") (v "0.3.0") (h "08304q7pzp6bkc6dqb1dcljmmlpdq1nqv7nspd8896qvll5hjc3q")))

(define-public crate-enum_macro-0.3.1 (c (n "enum_macro") (v "0.3.1") (h "1110rlpygvns8vxlszyx8izpbnzya8cp6fc2flndbsdgdld5qjn5")))

