(define-module (crates-io en um enum-tag) #:use-module (crates-io))

(define-public crate-enum-tag-0.1.0 (c (n "enum-tag") (v "0.1.0") (d (list (d (n "enum-tag-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1ki258g6rbbbmfvha5x8ra9530x4s0sws0dq4lfan2yds20yd7c6")))

(define-public crate-enum-tag-0.2.0 (c (n "enum-tag") (v "0.2.0") (d (list (d (n "enum-tag-macro") (r "^0.2.0") (d #t) (k 0)))) (h "1dk8am6cqrrwxwnhfblkma1v675851hgygz16nf3fw2ib1d8vl62")))

(define-public crate-enum-tag-0.3.0 (c (n "enum-tag") (v "0.3.0") (d (list (d (n "enum-tag-macro") (r "^0.3.0") (d #t) (k 0)))) (h "147kmdgnfksz055rdy4qj4n0ajw79rjh6zy2hl43y58i9hbcm5kn")))

