(define-module (crates-io en um enum_downcast) #:use-module (crates-io))

(define-public crate-enum_downcast-0.1.0 (c (n "enum_downcast") (v "0.1.0") (d (list (d (n "enum_dispatch") (r "^0.3") (d #t) (k 2)) (d (n "enum_downcast_derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 2)))) (h "0jlbrpffnrq6mgijii27gm6a3g14xags783ldzjlly8drdgh6cp5") (f (quote (("derive" "enum_downcast_derive") ("default"))))))

