(define-module (crates-io en um enum-ts) #:use-module (crates-io))

(define-public crate-enum-ts-0.1.0 (c (n "enum-ts") (v "0.1.0") (d (list (d (n "insta") (r "^1.5.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "028mlcs5ppf3m6lllp1s52f3pjdnwp9anxywnjp1fcyb36n03h61")))

(define-public crate-enum-ts-0.1.1 (c (n "enum-ts") (v "0.1.1") (d (list (d (n "insta") (r "^1.5.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "1c2mc3m4n9ggp3yvj54gmcnxv2wd77834dk3m661axm2hzi9kdgj")))

(define-public crate-enum-ts-0.1.2 (c (n "enum-ts") (v "0.1.2") (d (list (d (n "ignore") (r "^0.4.17") (d #t) (k 0)) (d (n "insta") (r "^1.5.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "0w2wk6z5vgrs8g7r7gh7jaznam7y359cgwyj2r0y1j5020wlbkjh")))

(define-public crate-enum-ts-0.1.4 (c (n "enum-ts") (v "0.1.4") (d (list (d (n "ignore") (r "^0.4.17") (d #t) (k 0)) (d (n "insta") (r "^1.5.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "1clnznd733l9h03h68vaiz30qgjshsk421d3cy0br4rflpd05wxb")))

(define-public crate-enum-ts-0.1.10 (c (n "enum-ts") (v "0.1.10") (d (list (d (n "ignore") (r "^0.4.17") (d #t) (k 0)) (d (n "insta") (r "^1.5.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "0kab1lbd31xc1qm9gk6f6dgj740wvs36m4yvxnfdvlwhqapycf2p")))

(define-public crate-enum-ts-0.2.2 (c (n "enum-ts") (v "0.2.2") (d (list (d (n "ignore") (r "^0.4.17") (d #t) (k 0)) (d (n "insta") (r "^1.5.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "1ybqj5c4bjkpg064gp59x95v7ggjf8xx14c33gl648wf8p4w4jin")))

(define-public crate-enum-ts-0.2.3 (c (n "enum-ts") (v "0.2.3") (d (list (d (n "ignore") (r "^0.4.17") (d #t) (k 0)) (d (n "insta") (r "^1.5.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "0a1afjh97afn2xv6kai6vdxfxqsgv16ixfix2wygy9yha7q6c78x")))

(define-public crate-enum-ts-0.2.4 (c (n "enum-ts") (v "0.2.4") (d (list (d (n "ignore") (r "^0.4.17") (d #t) (k 0)) (d (n "insta") (r "^1.5.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "13bvfynicgfqvakh8n91rs9dcg83g1mbjva04kr6mk2m2va0xc2x")))

(define-public crate-enum-ts-0.2.6 (c (n "enum-ts") (v "0.2.6") (d (list (d (n "ignore") (r "^0.4.17") (d #t) (k 0)) (d (n "insta") (r "^1.5.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "19s792kflz4mcjhi3ydyghjg70dzy2l2z1dfi46g5dh65hx4hp8v")))

