(define-module (crates-io en um enum-tryfrom) #:use-module (crates-io))

(define-public crate-enum-tryfrom-0.1.0 (c (n "enum-tryfrom") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.136") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "enum-tryfrom-derive") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "10cc7hfv98gf5if973m94gy8b6lx2qx5il2rysjpbsy3jsfn3sa1") (f (quote (("default") ("compiletest" "compiletest_rs"))))))

(define-public crate-enum-tryfrom-0.1.1 (c (n "enum-tryfrom") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.136") (o #t) (d #t) (k 0)) (d (n "compiletest_rs") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "enum-tryfrom-derive") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1fpqwb8ais99m6a31sbjjqi5j2jkvxlp7vcg7ky6nm79dcq03vrj") (f (quote (("default") ("compiletest" "compiletest_rs"))))))

(define-public crate-enum-tryfrom-0.1.2 (c (n "enum-tryfrom") (v "0.1.2") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "enum-tryfrom-derive") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1xplp4mb0x88j5yaqz2cdwlgmwad9hfqhr508rp295cz4isc31fh") (f (quote (("default"))))))

(define-public crate-enum-tryfrom-0.2.0 (c (n "enum-tryfrom") (v "0.2.0") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "enum-tryfrom-derive") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1a133ql7cyn5l96gr3922x4nhmnvzn2c8gyg4c7pfi4i86ihn10w") (f (quote (("std") ("default" "std"))))))

(define-public crate-enum-tryfrom-0.2.1 (c (n "enum-tryfrom") (v "0.2.1") (d (list (d (n "compiletest_rs") (r "^0.3") (d #t) (k 2)) (d (n "enum-tryfrom-derive") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "1faa5iy67r8afxk4yw88bjwngmk2pm4fhn0jl839fpkxghmvdj25") (f (quote (("std") ("default" "std"))))))

