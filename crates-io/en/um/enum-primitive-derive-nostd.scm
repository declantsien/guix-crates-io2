(define-module (crates-io en um enum-primitive-derive-nostd) #:use-module (crates-io))

(define-public crate-enum-primitive-derive-nostd-0.2.2 (c (n "enum-primitive-derive-nostd") (v "0.2.2") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1cq45gl6mw59g4q5m1581jhb7cxsvl513pg3w2yj8s1wyq3yhbsi")))

