(define-module (crates-io en um enum-iterator-derive) #:use-module (crates-io))

(define-public crate-enum-iterator-derive-0.1.0 (c (n "enum-iterator-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.5") (d #t) (k 0)))) (h "00z94p8y4ln6iix8gnmlb41ijckqamc4jwdsn88abcrm68jkch1p")))

(define-public crate-enum-iterator-derive-0.1.1 (c (n "enum-iterator-derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.5") (d #t) (k 0)))) (h "192p121dmxxp449g4cad6p0xi84ymfvnylmd6q3yzspbnqpx8rs9")))

(define-public crate-enum-iterator-derive-0.2.0 (c (n "enum-iterator-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4.6") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.14.2") (d #t) (k 0)))) (h "187hn28b8s5dzdfmg6kqa6dxvcm58ip5s4b79k7f80vs9jb2753i")))

(define-public crate-enum-iterator-derive-0.2.2 (c (n "enum-iterator-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^0.4.6") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0fpxfs01g6cfp3747xq6nn51jr7n89mpnwr4xaf2qb0wwb2340c7")))

(define-public crate-enum-iterator-derive-0.2.3 (c (n "enum-iterator-derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^0.4.6") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1n08zipj9g8rg8vfc22m84wvjzrli5y7p1kw99cw4jckbypycgi9")))

(define-public crate-enum-iterator-derive-0.3.0 (c (n "enum-iterator-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^0.4.26") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (d #t) (k 0)))) (h "0hdwjqzy7i7f2p5jcdapc3qaz5bz6y6rgmi66cr2lqpbbnwkq5pk")))

(define-public crate-enum-iterator-derive-0.3.1 (c (n "enum-iterator-derive") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1q7m9zzw1yc7l47sdjq2dvaqrirx4xs1d6vzxzrvlldhyk7d1y8x")))

(define-public crate-enum-iterator-derive-0.4.0 (c (n "enum-iterator-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "0dar9b2mz5ll6cx9fg2ylnygj07ckqanhr8i1n2pjdgr1la0xw2j")))

(define-public crate-enum-iterator-derive-0.5.0 (c (n "enum-iterator-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "1rmm7rj6snhp77pcid64sb9im7hrjg0ymvy2gnj6mn6bfaphcgzb")))

(define-public crate-enum-iterator-derive-0.6.0 (c (n "enum-iterator-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "01pc15d8l0ayrjv7xjjx1lxw2vypvlawcvc9ax7pdp60ywqsm50y")))

(define-public crate-enum-iterator-derive-0.7.0 (c (n "enum-iterator-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "0ndihb41kmi6pxc2bs097abxliw2pgnnw412lhdqfymjc1vw6d61")))

(define-public crate-enum-iterator-derive-0.8.0 (c (n "enum-iterator-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1knjgklkp6c1bnsxfkky1qmc51cj5isa655wkbakrxyrp2379x35")))

(define-public crate-enum-iterator-derive-0.8.1 (c (n "enum-iterator-derive") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1605h8n4q0mxgk87cf02728k5pmp906qa4qsd57b1w2rjfgnjn49")))

(define-public crate-enum-iterator-derive-1.0.0 (c (n "enum-iterator-derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "01ghzq85swn8x4k99x1bsarv84i7mbglrs4plfxdpi105m2mldrs")))

(define-public crate-enum-iterator-derive-1.0.1 (c (n "enum-iterator-derive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0lvh8b02c13mfzj3x89qrgfbjg8029a0zgn2m5iw5xj81i60xhb7")))

(define-public crate-enum-iterator-derive-1.0.2 (c (n "enum-iterator-derive") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "09fwp0fr1ikf82n0vp66mlqnlx2lypakr324j07qj884b5liwgxi")))

(define-public crate-enum-iterator-derive-1.1.0 (c (n "enum-iterator-derive") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1kirvrqgrb5a1d96mchf73nqr3j2rjlkx3zv5liq51x11ify93c2")))

(define-public crate-enum-iterator-derive-1.2.0 (c (n "enum-iterator-derive") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "01cxl62srl91qihwxn51s55dhj2qkp6frn648cfaxc7p7rv96prm")))

(define-public crate-enum-iterator-derive-1.2.1 (c (n "enum-iterator-derive") (v "1.2.1") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1jzxgfhz4i0hvnw5qzqhwscky8vsmxljv89g0navisacay4qbkzf")))

(define-public crate-enum-iterator-derive-1.3.0 (c (n "enum-iterator-derive") (v "1.3.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1f5zii1rb9mxl5w11crfl1lyn43a7h0jiia0fpk2imwbq9pc9k83")))

(define-public crate-enum-iterator-derive-1.3.1 (c (n "enum-iterator-derive") (v "1.3.1") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1xbwq77rs671zalsxi33svg7q44cqf96pbzhy56sqmrvsd9vp761")))

(define-public crate-enum-iterator-derive-1.4.0 (c (n "enum-iterator-derive") (v "1.4.0") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0nz6kz8jz2w1vy4y3r0mb8pa5nj3y77mdxdn3b38db322cf9kax1")))

