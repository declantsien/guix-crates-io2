(define-module (crates-io en um enum_variant_type) #:use-module (crates-io))

(define-public crate-enum_variant_type-0.1.0 (c (n "enum_variant_type") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.6.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("extra-traits" "visit"))) (d #t) (k 0)))) (h "0b9s2ibys2sxpf4bcqffpwl14m7fhjgflpb2jr2y4n18wa2ahfzh")))

(define-public crate-enum_variant_type-0.2.0 (c (n "enum_variant_type") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.7.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.13") (f (quote ("extra-traits" "visit"))) (d #t) (k 0)))) (h "092q9w1jwf8y5iyfnv8s7q2n2bci8qfqsrmarhck64kjk2kqbgfi")))

(define-public crate-enum_variant_type-0.2.1 (c (n "enum_variant_type") (v "0.2.1") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.7.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (f (quote ("extra-traits" "visit"))) (d #t) (k 0)))) (h "0104ym8iw8isas3hmqkc60c1b62hm2nm9yzhrfrwhjhbr2g6mzxn")))

(define-public crate-enum_variant_type-0.3.0 (c (n "enum_variant_type") (v "0.3.0") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.7.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("extra-traits" "visit"))) (d #t) (k 0)))) (h "07a6z32sq250ppw14ljbmgf8yhdrdsa31nlmyb8qfaqbk738qcpb")))

(define-public crate-enum_variant_type-0.3.1 (c (n "enum_variant_type") (v "0.3.1") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.34") (d #t) (k 0)) (d (n "proc_macro_roids") (r "^0.7.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("extra-traits" "visit"))) (d #t) (k 0)))) (h "0r5bd89qyyw2l5wbbym5gzrzkngqgp3wa4qykc1hcvz4gd2d1d7p")))

