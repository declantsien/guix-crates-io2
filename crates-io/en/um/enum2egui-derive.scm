(define-module (crates-io en um enum2egui-derive) #:use-module (crates-io))

(define-public crate-enum2egui-derive-0.1.0 (c (n "enum2egui-derive") (v "0.1.0") (d (list (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "14bsi8c64v0zdcdfhiiyl2zxzp6mc67dxab62hqhz7fq2s4dz9vg")))

(define-public crate-enum2egui-derive-0.2.0 (c (n "enum2egui-derive") (v "0.2.0") (d (list (d (n "egui") (r "^0.26") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1daa2kx3a74aakxpxwr4zj6fclsf52wkyl2552q8fi6vvcjm7jha")))

