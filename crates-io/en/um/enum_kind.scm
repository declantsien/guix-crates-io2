(define-module (crates-io en um enum_kind) #:use-module (crates-io))

(define-public crate-enum_kind-0.1.0 (c (n "enum_kind") (v "0.1.0") (d (list (d (n "pmutil") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^0.14.1") (f (quote ("full" "parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1ix21r5afqlmvvxxa3vj6sblgkh6ks186i2xk4hj8fx5gmmgan6g")))

(define-public crate-enum_kind-0.1.1 (c (n "enum_kind") (v "0.1.1") (d (list (d (n "pmutil") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^0.14.1") (f (quote ("full" "parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "09s17hiwifhikprnl12clmybxwj62ipxkhldjimzqm42kiacpllp")))

(define-public crate-enum_kind-0.1.2 (c (n "enum_kind") (v "0.1.2") (d (list (d (n "pmutil") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.4") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.1") (d #t) (k 0)) (d (n "syn") (r "^0.14.1") (f (quote ("full" "parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1ar9lhg3j336jw1i9rz5dxc99xs5lg7cmwcrl4flm3wgm6wqjm1v")))

(define-public crate-enum_kind-0.1.3 (c (n "enum_kind") (v "0.1.3") (d (list (d (n "pmutil") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1kiy1gppaplyvcsis1w6a7mgp4xyamdbllnarc06hirv70yp7lwg")))

(define-public crate-enum_kind-0.2.0 (c (n "enum_kind") (v "0.2.0") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1wwjmdd9h50l980dvgp7fcs0jagzb4289mbihkq52z8q6lz1amvf")))

(define-public crate-enum_kind-0.2.1 (c (n "enum_kind") (v "0.2.1") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.2") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "16gd5cb29lgv3n4nr5fkj7qg9f07h91alpkcjbq1xs2a6pd41fbq")))

(define-public crate-enum_kind-0.2.2 (c (n "enum_kind") (v "0.2.2") (d (list (d (n "pmutil") (r "^0.5.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "swc_macros_common") (r "^0.3.7") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1dlmai7vfy6w5rzk3gdfr8znamzb5igq2r4as9z8k7f5dr69b5cq")))

