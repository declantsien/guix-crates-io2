(define-module (crates-io en um enums_arena) #:use-module (crates-io))

(define-public crate-enums_arena-0.1.0 (c (n "enums_arena") (v "0.1.0") (d (list (d (n "enums_arena_derive") (r "^0.1") (d #t) (k 0)))) (h "1d3prgc1lmdlimnyy6zi02h2sn68hx88w0307xmrx9x4wcyk9i70")))

(define-public crate-enums_arena-0.1.1 (c (n "enums_arena") (v "0.1.1") (d (list (d (n "enums_arena_defines") (r "^0.1") (d #t) (k 0)) (d (n "enums_arena_derive") (r "^0.1") (d #t) (k 0)))) (h "01h34iwb70xfyc9ncj5hrahx11i49jrricp3iycny5fsyv0vig56") (y #t)))

(define-public crate-enums_arena-0.1.2 (c (n "enums_arena") (v "0.1.2") (d (list (d (n "enums_arena_defines") (r "^0.1") (d #t) (k 0)) (d (n "enums_arena_derive") (r "^0.1") (d #t) (k 0)))) (h "0987mwwyj1yj5pc60mib34s0xif7599msql2m7d9rwi9f1zmvd0j")))

(define-public crate-enums_arena-0.1.3 (c (n "enums_arena") (v "0.1.3") (d (list (d (n "enums_arena_defines") (r "^0.1") (d #t) (k 0)) (d (n "enums_arena_derive") (r "^0.1") (d #t) (k 0)))) (h "1w2cmkvnxna1f6yvvvpaq6f78daxfj63xff3rnl28nk07gk8gcdc")))

(define-public crate-enums_arena-0.1.4 (c (n "enums_arena") (v "0.1.4") (d (list (d (n "enums_arena_defines") (r "^0.1") (d #t) (k 0)) (d (n "enums_arena_derive") (r "^0.1.4") (d #t) (k 0)))) (h "1kdybha9qpakv3jj7kf738k6kfzmaqimnnl095rm2bwwzzsfakc1")))

