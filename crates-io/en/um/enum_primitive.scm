(define-module (crates-io en um enum_primitive) #:use-module (crates-io))

(define-public crate-enum_primitive-0.0.1 (c (n "enum_primitive") (v "0.0.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1pgmpa1ga1vps7ag6jhxslalyljbgndvhfmalrcz87jxb69cp2q1")))

(define-public crate-enum_primitive-0.0.2 (c (n "enum_primitive") (v "0.0.2") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "01pv73vfbxrzmf8qkmi5zip9kb4iz1y6f45rhr2y1dk78lyxh7ic")))

(define-public crate-enum_primitive-0.1.0 (c (n "enum_primitive") (v "0.1.0") (d (list (d (n "num") (r "*") (k 0)))) (h "08rw32r8bnmwlxngwkk350bafzkia3bagnppvmdpsk9ax5dzz7pp")))

(define-public crate-enum_primitive-0.1.1 (c (n "enum_primitive") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.1.32") (k 0)))) (h "100ip2p3w1rq0clca2ai5shhvpxfipnsjncj0f9ralad5w4m2idy")))

