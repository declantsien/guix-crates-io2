(define-module (crates-io en um enum_extract) #:use-module (crates-io))

(define-public crate-enum_extract-0.1.0 (c (n "enum_extract") (v "0.1.0") (h "1d8a3jd74ay9yjzfqnli1bha7aq14q8j8x00g5c3vx00869335lf")))

(define-public crate-enum_extract-0.1.1 (c (n "enum_extract") (v "0.1.1") (h "1lbrhz0rggad5phkzqbszx09hivylm0b7qa9qlkbdvlnhjg8ymqi")))

