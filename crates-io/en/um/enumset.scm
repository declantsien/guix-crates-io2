(define-module (crates-io en um enumset) #:use-module (crates-io))

(define-public crate-enumset-0.1.0 (c (n "enumset") (v "0.1.0") (h "0myzgn4z1glpf8sprh6nsn7xrrdw4ckilqdx59bqvm7mwjrr2ng6") (f (quote (("i128"))))))

(define-public crate-enumset-0.1.1 (c (n "enumset") (v "0.1.1") (h "0d50bprk1rnz52km3h0i3njj69lab8qhb929sgyp0b81h8rmprbc") (f (quote (("i128"))))))

(define-public crate-enumset-0.1.2 (c (n "enumset") (v "0.1.2") (h "07hgy693mz687czwvy8xszpf86ald9h4z9afbf60flvax2y35wyp") (f (quote (("i128"))))))

(define-public crate-enumset-0.2.0 (c (n "enumset") (v "0.2.0") (h "0mdyhwir490y92dd9rj54b5gfx9ly36sq2w6kgzaz0jsk3l1disx") (f (quote (("i128")))) (y #t)))

(define-public crate-enumset-0.2.1 (c (n "enumset") (v "0.2.1") (h "03w2n2f2n9pza4idgrmm6r4dgdidg8mkrddw0c6gicrbjcwvsfg0") (f (quote (("nightly"))))))

(define-public crate-enumset-0.3.0 (c (n "enumset") (v "0.3.0") (h "0zmdacv8fdk640s48x52sd36wrfnvb6spkyc1ll246ni6nmjm3jx") (f (quote (("nightly")))) (y #t)))

(define-public crate-enumset-0.3.1 (c (n "enumset") (v "0.3.1") (h "1mlziqmfsgsj20nm672y4xzf5kl1ss82akmj597x6prppnpk5kq4") (f (quote (("nightly")))) (y #t)))

(define-public crate-enumset-0.3.2 (c (n "enumset") (v "0.3.2") (h "1v2ms0gpharzdh8yw6r641bz0zf6i0p4zf2i51pva5vrgv83d08f") (f (quote (("nightly"))))))

(define-public crate-enumset-0.3.3 (c (n "enumset") (v "0.3.3") (h "08vk6c5inywgawryyyfql45r80lzxnq50yn7vdin8h188vqkxi6c") (f (quote (("nightly"))))))

(define-public crate-enumset-0.3.4 (c (n "enumset") (v "0.3.4") (h "18dg35nlgn12rar1w3cdlf582pd8vycssm2x1hlfawifcik2phpr") (f (quote (("nightly"))))))

(define-public crate-enumset-0.3.5 (c (n "enumset") (v "0.3.5") (h "0s0bh2sq6afd0fr3fn28nqiimdnqd4yw6iggys9x0fw1lhhw56ps") (f (quote (("nightly"))))))

(define-public crate-enumset-0.3.6 (c (n "enumset") (v "0.3.6") (h "1yg3s5xaqg58721bhpfc9pdn5599kb7c9xj6rv206lnqaxpvl7m0") (f (quote (("nightly"))))))

(define-public crate-enumset-0.3.7 (c (n "enumset") (v "0.3.7") (h "1hm997d4ckxry3j81fmg6szg9326lqrn9kmh1nb2a65p9s2qdw5g") (f (quote (("nightly")))) (y #t)))

(define-public crate-enumset-0.3.8 (c (n "enumset") (v "0.3.8") (h "0pbkbv0vfcrscr8y56wnmgkw02ldvkawl3vrjn31kzfd4a067k6v") (f (quote (("nightly"))))))

(define-public crate-enumset-0.3.9 (c (n "enumset") (v "0.3.9") (h "0a4n15hwf067p2gxxrmsvdw0mkm5nxky3081ksh2bcp9h7nk42m6") (f (quote (("nightly"))))))

(define-public crate-enumset-0.3.10 (c (n "enumset") (v "0.3.10") (h "1plmk2p6wd5rqjs4g23q9iwyclyj70whq9sdsn1im3frk7sxa1cm") (f (quote (("nightly"))))))

(define-public crate-enumset-0.3.11 (c (n "enumset") (v "0.3.11") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0h39rsxm0k87wyvsqx6ndfnil5vazpj9py23mrfl95nq6sp3q6c1") (f (quote (("nightly"))))))

(define-public crate-enumset-0.3.12 (c (n "enumset") (v "0.3.12") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0jwvbzh5ss41ya38k15bh89d29d9i2nf5nkw27f9340b0j1scr3c") (f (quote (("nightly"))))))

(define-public crate-enumset-0.3.13 (c (n "enumset") (v "0.3.13") (d (list (d (n "enumset_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "08ybkwj26wk9b41aaw0ikm28djiv3rsi3k9xmnjw7dx288vyiqgc") (f (quote (("nightly" "enumset_derive/nightly")))) (y #t)))

(define-public crate-enumset-0.3.14 (c (n "enumset") (v "0.3.14") (d (list (d (n "enumset_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "138x2ns12lwx7izrn1af8rmq9sa552g64k44rc4cr7g7cy7hq3j1") (f (quote (("nightly" "enumset_derive/nightly"))))))

(define-public crate-enumset-0.3.15 (c (n "enumset") (v "0.3.15") (d (list (d (n "bincode") (r "^1.0") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0hyr6xill76iy4kgznqhsb9hlbxbg6jjxd17apbn0jhm753y1i8f") (f (quote (("nightly" "enumset_derive/nightly"))))))

(define-public crate-enumset-0.3.16 (c (n "enumset") (v "0.3.16") (d (list (d (n "bincode") (r "^1.0") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "006ggvks0sdb57ipmzn7a13f8h9i0v1nxxfql4zj39v8zmvpgnjm") (f (quote (("nightly" "enumset_derive/nightly"))))))

(define-public crate-enumset-0.3.17 (c (n "enumset") (v "0.3.17") (d (list (d (n "bincode") (r "^1.0") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde2") (r "^1.0") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0k52kz336wysqqh2pbi3np5vfv9v5x81s58gjqfcmx39z9xm8rrd") (f (quote (("serde" "serde2" "enumset_derive/serde") ("nightly" "enumset_derive/nightly"))))))

(define-public crate-enumset-0.3.18 (c (n "enumset") (v "0.3.18") (d (list (d (n "bincode") (r "^1.0") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.3.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde2") (r "^1.0") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0cchcslykx4i50x4ax490fv4c7d6l0nyb2brj52hfymdxw8yj1yy") (f (quote (("serde" "serde2" "enumset_derive/serde") ("nightly" "enumset_derive/nightly"))))))

(define-public crate-enumset-0.3.19 (c (n "enumset") (v "0.3.19") (d (list (d (n "bincode") (r "^1.0") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.3.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde2") (r "^1.0") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0lmirnskk8z58ipzp30j4jyh398n148mc803lbpp39p6mbzmxga3") (f (quote (("serde" "serde2" "enumset_derive/serde") ("nightly" "enumset_derive/nightly"))))))

(define-public crate-enumset-0.4.0 (c (n "enumset") (v "0.4.0") (d (list (d (n "bincode") (r "^1.0") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde2") (r "^1.0.91") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "19xjy9c73f9jr6zn5cgxs36hvkvrzvivjalssskp0r9z2wpa5h6a") (f (quote (("serde" "serde2" "enumset_derive/serde") ("nightly" "enumset_derive/nightly"))))))

(define-public crate-enumset-0.4.1 (c (n "enumset") (v "0.4.1") (d (list (d (n "bincode") (r "^1.0") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde2") (r "^1.0.91") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "09nzjx949dxb2jpvfynhjj471pmbni24z5fvvflkd7vhrd2c1zby") (f (quote (("serde" "serde2" "enumset_derive/serde") ("nightly" "enumset_derive/nightly"))))))

(define-public crate-enumset-0.4.2 (c (n "enumset") (v "0.4.2") (d (list (d (n "bincode") (r "^1.0") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde2") (r "^1.0.91") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "0k556pzswrh0q5n4qxjaghkfapazpsbirszx5w9p4d2g3lk96hkv") (f (quote (("serde" "serde2" "enumset_derive/serde") ("nightly" "enumset_derive/nightly"))))))

(define-public crate-enumset-0.4.3 (c (n "enumset") (v "0.4.3") (d (list (d (n "bincode") (r "^1.0") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.4.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde2") (r "^1.0.91") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "0p9lx3rcck8ljal46k5gkjlfmwbcim6kwb5d22gcf6w16ai97gs1") (f (quote (("serde" "serde2" "enumset_derive/serde") ("nightly" "enumset_derive/nightly"))))))

(define-public crate-enumset-0.4.4 (c (n "enumset") (v "0.4.4") (d (list (d (n "bincode") (r "^1.0") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde2") (r "^1.0.91") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "12nwbs0b5w69c40vjyjvasxw5gzcy14c7givy4wcj77zyjp13f2p") (f (quote (("serde" "serde2" "enumset_derive/serde") ("nightly" "enumset_derive/nightly"))))))

(define-public crate-enumset-0.4.5 (c (n "enumset") (v "0.4.5") (d (list (d (n "bincode") (r "^1.0") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde2") (r "^1.0.91") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "0iqwmhiy454n70cjhi2vb1hlh0xxrz2yn8lvhzjmgirhcp5js64k") (f (quote (("serde" "serde2" "enumset_derive/serde") ("nightly" "enumset_derive/nightly"))))))

(define-public crate-enumset-1.0.0 (c (n "enumset") (v "1.0.0") (d (list (d (n "bincode") (r "^1.0") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "rustversion") (r "^1.0.2") (d #t) (k 2)) (d (n "serde2") (r "^1.0.91") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.24") (d #t) (k 2)))) (h "0hd29inm395rx70zgh2hjwzjdi72i3kdhzfm03cnlc9ljmswx49n") (f (quote (("serde" "serde2" "enumset_derive/serde"))))))

(define-public crate-enumset-1.0.1 (c (n "enumset") (v "1.0.1") (d (list (d (n "bincode") (r "^1.0") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.5.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "rustversion") (r "^1.0.2") (d #t) (k 2)) (d (n "serde2") (r "^1.0.91") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.24") (d #t) (k 2)))) (h "1lbb4msq15j3yb7c5943ihx7p66y29i7768xsipddv9g0si816lm") (f (quote (("serde" "serde2" "enumset_derive/serde"))))))

(define-public crate-enumset-1.0.2 (c (n "enumset") (v "1.0.2") (d (list (d (n "bincode") (r "^1.0") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.5.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.2") (d #t) (k 2)) (d (n "serde2") (r "^1.0.91") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.24") (d #t) (k 2)))) (h "0daivp8zrmq3s2j3v6hy7wgqnw2k2gqsgcszw17fnmsad4fsar9k") (f (quote (("serde" "serde2" "enumset_derive/serde"))))))

(define-public crate-enumset-1.0.3 (c (n "enumset") (v "1.0.3") (d (list (d (n "bincode") (r "^1.0") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.5.2") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.2") (d #t) (k 2)) (d (n "serde2") (r "^1.0.91") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.24") (d #t) (k 2)))) (h "136jxy87ss7mvj8zdnp4fpa47cn4qjf9gwla248zq4ps73zcv5rf") (f (quote (("serde" "serde2" "enumset_derive/serde"))))))

(define-public crate-enumset-1.0.4 (c (n "enumset") (v "1.0.4") (d (list (d (n "bincode") (r "^1.0") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.5.3") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.2") (d #t) (k 2)) (d (n "serde2") (r "^1.0.91") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.24") (d #t) (k 2)))) (h "1zyw2y95g53rayjpvarbli294lfbwn4xpyycmmn6jxkspv8nfqfg") (f (quote (("serde" "serde2" "enumset_derive/serde"))))))

(define-public crate-enumset-1.0.5 (c (n "enumset") (v "1.0.5") (d (list (d (n "bincode") (r "^1.0") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.5.4") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.2") (d #t) (k 2)) (d (n "serde2") (r "^1.0.91") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.24") (d #t) (k 2)))) (h "1hsx2v8dnszi0nn546myxrr50qb6s4ysi5vnc9r2g9hdss4k03n7") (f (quote (("serde" "serde2" "enumset_derive/serde"))))))

(define-public crate-enumset-1.0.6 (c (n "enumset") (v "1.0.6") (d (list (d (n "bincode") (r "^1.0") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.5.4") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.2") (d #t) (k 2)) (d (n "serde2") (r "^1.0.91") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.24") (d #t) (k 2)))) (h "1hxrdld1bmg42l3xy3zplnvm5l7gfb6fl47fy6mrk988czgrbmzv") (f (quote (("serde" "serde2" "enumset_derive/serde"))))))

(define-public crate-enumset-1.0.7 (c (n "enumset") (v "1.0.7") (d (list (d (n "bincode") (r "^1.0") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.5.5") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.2") (d #t) (k 2)) (d (n "serde2") (r "^1.0.91") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.24") (d #t) (k 2)))) (h "1c70w3fsb3gzxqmlh7jwz2z0vhy1nbd00l4f3c1ay0k1lffi4xky") (f (quote (("serde" "serde2" "enumset_derive/serde"))))))

(define-public crate-enumset-1.0.8 (c (n "enumset") (v "1.0.8") (d (list (d (n "bincode") (r "^1.0") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.5.5") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.2") (d #t) (k 2)) (d (n "serde2") (r "^1.0.91") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.24") (d #t) (k 2)))) (h "0b2vdggbsnvvw09czxaazbqcpa378fycf7fs3afz5dbgkb0x45k2") (f (quote (("serde" "serde2" "enumset_derive/serde"))))))

(define-public crate-enumset-1.0.9 (c (n "enumset") (v "1.0.9") (d (list (d (n "bincode") (r "^1.0") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.5.6") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.2") (d #t) (k 2)) (d (n "serde2") (r "^1.0.91") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.24") (d #t) (k 2)))) (h "1zmif8ziyf3m9sznx08ifqh5rmnnl6ada4d4jc0bma1ivdsaxw22") (f (quote (("serde" "serde2" "enumset_derive/serde")))) (y #t)))

(define-public crate-enumset-1.0.10 (c (n "enumset") (v "1.0.10") (d (list (d (n "bincode") (r "^1.0") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.5.7") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.2") (d #t) (k 2)) (d (n "serde2") (r "^1.0.91") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.24") (d #t) (k 2)))) (h "0mp7c0gal8bz6kip7ns4hdshd0y2sqb53k5j00gkqd1q3cqb5dp8") (f (quote (("std" "alloc" "enumset_derive/proc-macro-crate") ("serde" "serde2" "enumset_derive/serde") ("alloc"))))))

(define-public crate-enumset-1.0.11 (c (n "enumset") (v "1.0.11") (d (list (d (n "bincode") (r "^1.0") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.6.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.2") (d #t) (k 2)) (d (n "serde2") (r "^1.0.91") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.24") (d #t) (k 2)))) (h "0x70xzxs2sj0yn4sh99bwk8s51aswvyvf1ldm6kziwa89nrcv6a7") (f (quote (("std" "alloc" "enumset_derive/proc-macro-crate") ("serde" "serde2" "enumset_derive/serde") ("alloc"))))))

(define-public crate-enumset-1.0.12 (c (n "enumset") (v "1.0.12") (d (list (d (n "bincode") (r "^1") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.6.1") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "serde2") (r "^1") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0ly7n4f1cxlw38qy05dd8ayh3gvq6n3nq411rykg7dkal1hq1ghr") (f (quote (("std" "alloc" "enumset_derive/proc-macro-crate") ("serde" "serde2" "enumset_derive/serde") ("alloc"))))))

(define-public crate-enumset-1.0.13 (c (n "enumset") (v "1.0.13") (d (list (d (n "bincode") (r "^1") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.7.0") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "serde2") (r "^1") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1kxky09iddc6q83y3wlbasyz6v3b44wy7p4v9a5vv5yib93jbc2r") (f (quote (("std" "alloc" "enumset_derive/proc-macro-crate") ("serde" "serde2" "enumset_derive/serde") ("alloc"))))))

(define-public crate-enumset-1.1.0 (c (n "enumset") (v "1.1.0") (d (list (d (n "bincode") (r "^1") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.8.0") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "serde2") (r "^1") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1bhl9zy0981jwl5dzbzvi7svrrlj5ynwf2i0p4szrxixb4kbmfv5") (f (quote (("std" "alloc" "enumset_derive/proc-macro-crate") ("serde" "serde2" "enumset_derive/serde") ("alloc"))))))

(define-public crate-enumset-1.1.1 (c (n "enumset") (v "1.1.1") (d (list (d (n "bincode") (r "^1") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.8.0") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "serde2") (r "^1") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0bjvfbj9lr2rj1pwl8sb1a5r48aa2rz2m4srpcniz3wkhd3d9w97") (f (quote (("std" "alloc" "enumset_derive/proc-macro-crate") ("serde" "serde2" "enumset_derive/serde") ("alloc"))))))

(define-public crate-enumset-1.1.2 (c (n "enumset") (v "1.1.2") (d (list (d (n "bincode") (r "^1") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.8.0") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "serde2") (r "^1") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1fxv50qflzbvhb5g1lryhab31dlv5mg6gvc1xryhkphnkiqz2xg8") (f (quote (("std" "alloc" "enumset_derive/proc-macro-crate") ("serde" "serde2" "enumset_derive/serde") ("alloc"))))))

(define-public crate-enumset-1.1.3 (c (n "enumset") (v "1.1.3") (d (list (d (n "bincode") (r "^1") (f (quote ("i128"))) (d #t) (k 2)) (d (n "enumset_derive") (r "^0.8.0") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 2)) (d (n "serde2") (r "^1") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0z80d7v4fih563ysg8vny8kpspk3y340v7ncwmbzn4rc8skhsv12") (f (quote (("std" "alloc" "enumset_derive/proc-macro-crate") ("serde" "serde2" "enumset_derive/serde") ("alloc"))))))

