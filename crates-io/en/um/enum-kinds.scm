(define-module (crates-io en um enum-kinds) #:use-module (crates-io))

(define-public crate-enum-kinds-0.3.0 (c (n "enum-kinds") (v "0.3.0") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.13") (d #t) (k 0)))) (h "02n16n14kh0fq5sjvyllcrq7jw6pgw3gm3xhbc44nvq0k2pp3rh5") (f (quote (("no-stdlib") ("default"))))))

(define-public crate-enum-kinds-0.3.1 (c (n "enum-kinds") (v "0.3.1") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.10") (d #t) (k 2)) (d (n "syn") (r "^0.12.13") (d #t) (k 0)))) (h "0934hja13jclhqzbdmaichllh19d9w99rkg38k4j6s8bhrbakgmj") (f (quote (("no-stdlib") ("default"))))))

(define-public crate-enum-kinds-0.4.0 (c (n "enum-kinds") (v "0.4.0") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.10") (d #t) (k 2)) (d (n "syn") (r "^0.12.13") (d #t) (k 0)))) (h "1nphhvjynz6kz0jm9asw0pkc2xasbyy53i23zm3a57yldxbsjcii") (f (quote (("no-stdlib") ("default"))))))

(define-public crate-enum-kinds-0.4.1 (c (n "enum-kinds") (v "0.4.1") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.10") (d #t) (k 2)) (d (n "syn") (r "^0.12.13") (d #t) (k 0)))) (h "1df4ra7knqpnd2g50bixs389lg6hbb50858v0wcqr17a9lvirwnh") (f (quote (("no-stdlib") ("default"))))))

(define-public crate-enum-kinds-0.5.0 (c (n "enum-kinds") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.10") (d #t) (k 2)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "182rsi016zssa3xzicic1yy6z80z6whdzspjfqcpdv4azx0sa3n3") (f (quote (("no-stdlib") ("default"))))))

(define-public crate-enum-kinds-0.5.1 (c (n "enum-kinds") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.127") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 2)) (d (n "syn") (r "^1.0.74") (d #t) (k 0)))) (h "0qnlgzlsydnxsgcf2lkvqsrmdxignjkam1fsnfd4c7b8amls2h2f") (f (quote (("no-stdlib") ("default"))))))

