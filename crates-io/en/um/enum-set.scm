(define-module (crates-io en um enum-set) #:use-module (crates-io))

(define-public crate-enum-set-0.0.1 (c (n "enum-set") (v "0.0.1") (h "1vmcnlyd7ba7m1dqhaa1win6k8ivbkyj8g1k1vzlj51590p0v3h7")))

(define-public crate-enum-set-0.0.2 (c (n "enum-set") (v "0.0.2") (h "0cmb0j3avjg5r3w8rm28b0hwaq5yvnb3sgnx66405gd5pzs7av1c")))

(define-public crate-enum-set-0.0.3 (c (n "enum-set") (v "0.0.3") (h "16rr8sjvn9kqdjalhgx83a7fc7skx7r4fdslqzkcmh5g89ahi236")))

(define-public crate-enum-set-0.0.4 (c (n "enum-set") (v "0.0.4") (h "1h8z40srb45f9ias36i4p8rd852aj9bh7d303dc5s8cdsymcid2f")))

(define-public crate-enum-set-0.0.5 (c (n "enum-set") (v "0.0.5") (h "16g6gvr4xn6i9mv2rsk6v4hfg8w1bd93cbsksj633sgr652az5rc")))

(define-public crate-enum-set-0.0.6 (c (n "enum-set") (v "0.0.6") (h "1kraibg71snagskzivxlphnx4mdm6gas38p0cswr8k0h4m8zi5z9")))

(define-public crate-enum-set-0.0.7 (c (n "enum-set") (v "0.0.7") (h "1d81hr2m2s8g3pf8b6ikcppn7br55ylckh5zpcyr08nijcn0ckfs")))

(define-public crate-enum-set-0.0.8 (c (n "enum-set") (v "0.0.8") (h "1pv5cv7nr78df16l8bk6dhzqps9ifrrjh7cg2k9rs9vn2swvmwnv")))

