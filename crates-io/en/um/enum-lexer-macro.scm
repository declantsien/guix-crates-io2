(define-module (crates-io en um enum-lexer-macro) #:use-module (crates-io))

(define-public crate-enum-lexer-macro-0.1.0 (c (n "enum-lexer-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex-dfa-gen") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.36") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1b8x85an25vxw0q9lnyvbxh0qg73iqhsf0xl4xn91llnm23ib5dm")))

