(define-module (crates-io en um enumit) #:use-module (crates-io))

(define-public crate-enumit-0.0.0 (c (n "enumit") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1428q0y00z70pngrn0hf8iki6247plqk329x0f67q75nbl0gn92j")))

(define-public crate-enumit-0.1.0 (c (n "enumit") (v "0.1.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "08aknmkzz1lz1rjnw5320bszsrn1fmgva2d7i279mz0d1j6wsr9s")))

(define-public crate-enumit-0.0.2 (c (n "enumit") (v "0.0.2") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "1wh2gc2s1bh44w2kvjd9909gsidag9isah75s5vf9mbdjc5h50p8")))

(define-public crate-enumit-0.1.1 (c (n "enumit") (v "0.1.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "03lw57f5ncysyaqfpvmkk629kpk9sbfqwlfa12gc116vx0zrpflr")))

(define-public crate-enumit-0.1.2 (c (n "enumit") (v "0.1.2") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "1ww2951x6pcqpm5p6ir5s2qv2zrafi7c0306g2hri6n4jayy9k3j")))

