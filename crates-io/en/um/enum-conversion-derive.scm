(define-module (crates-io en um enum-conversion-derive) #:use-module (crates-io))

(define-public crate-enum-conversion-derive-0.0.1 (c (n "enum-conversion-derive") (v "0.0.1") (d (list (d (n "enum-conversion-traits") (r "^0.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tera") (r "^1") (d #t) (k 0)))) (h "06zzxpx9yi62kg7a2f1ikazn40bw7wd0gf0rbqrn8cjmphf03wjv")))

