(define-module (crates-io en um enumerable_derive) #:use-module (crates-io))

(define-public crate-enumerable_derive-0.1.0 (c (n "enumerable_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05y773gc7niah9yqigc0m45kvy97wxxaf1jl3vh33yrvv5bfsfsg")))

(define-public crate-enumerable_derive-0.2.0 (c (n "enumerable_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09r7cjzx27i64jva0z4p5v56yw66l7mancxfyrbdd4495005vhpy")))

