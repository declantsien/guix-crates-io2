(define-module (crates-io en um enumoid_derive) #:use-module (crates-io))

(define-public crate-enumoid_derive-0.1.0 (c (n "enumoid_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hhabkwk5zlazz4mfr2l7k9kf6yilg04wbmsl19wcdp7xa0qqwcw")))

(define-public crate-enumoid_derive-0.1.1 (c (n "enumoid_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17jrmwhy8palhiz61sklja27pcr66jz6qj81iar4y2h69jaj0rxk")))

(define-public crate-enumoid_derive-0.2.0 (c (n "enumoid_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0p7cp69lf6iyqvaj341apxl03h88ydsfmsn3518cmw421lxfszv4")))

(define-public crate-enumoid_derive-0.3.0 (c (n "enumoid_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0ypfn21hhxwdpsbs1br13av902i2ca16c3m87y48spjx3ay84y3z")))

(define-public crate-enumoid_derive-0.4.0 (c (n "enumoid_derive") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1xcgnrk2fiqx7yhlp3xjxwr0ka6s5kcv18wdwvyq62ws1flkx2hw")))

