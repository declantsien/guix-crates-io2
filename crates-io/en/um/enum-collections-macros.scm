(define-module (crates-io en um enum-collections-macros) #:use-module (crates-io))

(define-public crate-enum-collections-macros-0.1.0 (c (n "enum-collections-macros") (v "0.1.0") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12imna6kb67cl9rkn99sapd9yk61cbr52pcv7qxhrbrs6vwsaw9r")))

(define-public crate-enum-collections-macros-0.2.0 (c (n "enum-collections-macros") (v "0.2.0") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "176npp32jczd7pj18bv8yghqml4acdg0pfb4d7h7aabvr4hqsp9f")))

(define-public crate-enum-collections-macros-0.3.0 (c (n "enum-collections-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "00f8slbfpz1465wvpd4g6ns8h5l6cbfzfz696ikcj5cgbapnsww1")))

(define-public crate-enum-collections-macros-1.0.0 (c (n "enum-collections-macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0996nmsh7zgrx756l0aa85ljshz45xsh7j8gh73rib1bkck0bnc6") (f (quote (("variants") ("default"))))))

(define-public crate-enum-collections-macros-1.0.1 (c (n "enum-collections-macros") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0777dff41mxklqislilwfiv3wzjvjaq3gzwr8bvh75gad4nc0whs") (f (quote (("variants") ("default"))))))

