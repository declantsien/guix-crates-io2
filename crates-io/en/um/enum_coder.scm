(define-module (crates-io en um enum_coder) #:use-module (crates-io))

(define-public crate-enum_coder-0.0.0 (c (n "enum_coder") (v "0.0.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "fold" "full"))) (d #t) (k 0)))) (h "1fglphnvxx73vxayljz0s8jigp7q9r5iykg92lzf4a55h1w1jg51")))

(define-public crate-enum_coder-0.0.1 (c (n "enum_coder") (v "0.0.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits" "fold" "full"))) (d #t) (k 0)))) (h "0fwmnhxyzhdq8m7rbv3n60h104j0dcr8j8zj9z761fvq893l7kg2")))

