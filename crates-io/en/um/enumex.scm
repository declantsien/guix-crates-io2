(define-module (crates-io en um enumex) #:use-module (crates-io))

(define-public crate-enumex-0.1.0 (c (n "enumex") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "109dnvsbjgp2xs1gd8frhbzavkilb6k4rdgks94k3lia5kya5n1c")))

