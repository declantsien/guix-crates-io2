(define-module (crates-io en um enum_traits_macros) #:use-module (crates-io))

(define-public crate-enum_traits_macros-0.1.0 (c (n "enum_traits_macros") (v "0.1.0") (d (list (d (n "aster") (r "*") (k 0)) (d (n "quasi") (r "*") (k 0)) (d (n "quasi_codegen") (r "*") (k 1)) (d (n "quasi_macros") (r "*") (k 0)))) (h "0zm4hdc9y7zvqn35mrby0vg6iaiqxhzwjxg5xicqy7llbjwwjnxd")))

(define-public crate-enum_traits_macros-0.1.1 (c (n "enum_traits_macros") (v "0.1.1") (d (list (d (n "aster") (r "*") (k 0)) (d (n "quasi") (r "*") (k 0)) (d (n "quasi_codegen") (r "*") (k 1)) (d (n "quasi_macros") (r "*") (k 0)))) (h "02pfb7w611lrm3p30ksqzc1490dcl19lnll1aphd9yzidscm56ai")))

(define-public crate-enum_traits_macros-0.1.2 (c (n "enum_traits_macros") (v "0.1.2") (d (list (d (n "aster") (r "*") (k 0)) (d (n "quasi") (r "*") (k 0)) (d (n "quasi_codegen") (r "*") (k 1)) (d (n "quasi_macros") (r "*") (k 0)))) (h "1y49q09zapd8sy8kyhbwyxgir3lb9886rh5pss64j154xv7ngh30")))

(define-public crate-enum_traits_macros-0.1.3 (c (n "enum_traits_macros") (v "0.1.3") (d (list (d (n "aster") (r "0.*") (k 0)) (d (n "quasi") (r "0.*") (k 0)) (d (n "quasi_codegen") (r "0.*") (k 1)) (d (n "quasi_macros") (r "0.*") (k 0)))) (h "1ksn41jp0azqprs9kbkk7hakdv6q8fhf2vh9j2mxkr41ninzx2zr")))

(define-public crate-enum_traits_macros-1.0.0 (c (n "enum_traits_macros") (v "1.0.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "042px0m0d1vz9dh1sgyqpvksq29zcch4n8ay9d8sqyfhi93kchji")))

(define-public crate-enum_traits_macros-1.1.0 (c (n "enum_traits_macros") (v "1.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "05dck2fnaxjyngrwg79ypgb0sjmh4lzb8lf1msllbjckvp5wmkrg")))

(define-public crate-enum_traits_macros-1.2.0 (c (n "enum_traits_macros") (v "1.2.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "1x8lz7djm9qdj05lvfph04r90qzvsnq2xa3fspsch7ilqxbrkvyk")))

(define-public crate-enum_traits_macros-1.2.1 (c (n "enum_traits_macros") (v "1.2.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "17fvr30dnlaz20vw5yvrfnfg46h8pfkcv56ba61w5j1bcjypl554")))

(define-public crate-enum_traits_macros-2.0.0 (c (n "enum_traits_macros") (v "2.0.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.10") (d #t) (k 0)))) (h "0679g0f3rdgxk39zcpf2qjj960835hgmdqly84j3kzlnsk9fa34z")))

