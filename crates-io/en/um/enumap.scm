(define-module (crates-io en um enumap) #:use-module (crates-io))

(define-public crate-enumap-0.1.0 (c (n "enumap") (v "0.1.0") (h "0mr38brwdl01n0gkih0dmkpgyzplipsv557jwm0g4spr1isqyl9g")))

(define-public crate-enumap-0.2.0 (c (n "enumap") (v "0.2.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1ph0zwqlknzlq5cx6ygxd2hch1q7w7k47svb4j2pn7jc6s0rjlg5") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-enumap-0.3.0 (c (n "enumap") (v "0.3.0") (d (list (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "023il5as71fvanf4ybnqfj1pdjisl80nbc6rlm6m7viwixacd587") (s 2) (e (quote (("serde" "dep:serde"))))))

