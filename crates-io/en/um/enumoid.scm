(define-module (crates-io en um enumoid) #:use-module (crates-io))

(define-public crate-enumoid-0.1.0 (c (n "enumoid") (v "0.1.0") (d (list (d (n "enumoid_derive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1il42g02i8qhxl4hmzwqidbz4vch8z2ryl11gp6h01grfrlfich7") (f (quote (("default" "serde"))))))

(define-public crate-enumoid-0.1.1 (c (n "enumoid") (v "0.1.1") (d (list (d (n "enumoid_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0jplygzsigimw7sbxc9sf450fklqb2yi1wsskl6xg0kmq6xa87cd") (f (quote (("default" "serde"))))))

(define-public crate-enumoid-0.2.0 (c (n "enumoid") (v "0.2.0") (d (list (d (n "enumoid_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0acrd1jf0jvx5bsibl4n2dyvdrp12341869mh2yg78l96kvqznz2") (f (quote (("default" "serde"))))))

(define-public crate-enumoid-0.2.1 (c (n "enumoid") (v "0.2.1") (d (list (d (n "enumoid_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1j162w1pyrw8m86v9jq699q81v1qa7jqsf480ryj2109cnn614ac") (f (quote (("default" "serde"))))))

(define-public crate-enumoid-0.2.2 (c (n "enumoid") (v "0.2.2") (d (list (d (n "enumoid_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0skwlfmf7rknka789c9qz6zdzkijr1lcnzjfylpzgdsgbl1kpzjv") (f (quote (("default" "serde"))))))

(define-public crate-enumoid-0.2.3 (c (n "enumoid") (v "0.2.3") (d (list (d (n "enumoid_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1xdq3chcsynqd7fs62w8gggmm2kav4cm4n0g0y4rmimggb9l15w0") (f (quote (("default" "serde"))))))

(define-public crate-enumoid-0.3.0 (c (n "enumoid") (v "0.3.0") (d (list (d (n "enumoid_derive") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0jljk0djy5cdv3p4vb4g1d1zcnw2rs7ck2b9z225igycvxa6wbwg") (f (quote (("default" "serde"))))))

(define-public crate-enumoid-0.4.0 (c (n "enumoid") (v "0.4.0") (d (list (d (n "enumoid_derive") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "06bj3yk2wvn91j28r1n183gm7fvlzzii5jkm27zxii9wq14n2582") (f (quote (("default" "serde"))))))

