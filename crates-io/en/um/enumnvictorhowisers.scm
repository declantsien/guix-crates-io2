(define-module (crates-io en um enumnvictorhowisers) #:use-module (crates-io))

(define-public crate-enumnvictorhowisers-0.1.13 (c (n "enumnvictorhowisers") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "05xvgymcm6pjn3qbcgjj1xda90y4dj1prlgr5dsnccnvh47yg5zd") (r "1.56")))

