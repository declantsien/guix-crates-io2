(define-module (crates-io en um enumcapsulate-macros) #:use-module (crates-io))

(define-public crate-enumcapsulate-macros-0.1.0 (c (n "enumcapsulate-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.76") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "18ihxkg482ygmld493ihl600nv1r4k77anjywbqvwik2byqb6wym") (r "1.70.0")))

(define-public crate-enumcapsulate-macros-0.2.1 (c (n "enumcapsulate-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.81") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "0qx02h4ialx4fss4am87xaaj8w2hyn6vr4zjzcfz24hw12233xsg") (r "1.74.0")))

(define-public crate-enumcapsulate-macros-0.2.2 (c (n "enumcapsulate-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.81") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "1mg41xhgck2f59a9vsacsfq8kd4w6ah6yks9d5hl5avpvydcspw4") (r "1.74.0")))

(define-public crate-enumcapsulate-macros-0.3.0 (c (n "enumcapsulate-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (d #t) (k 0)))) (h "0vzzy6j2lm5pdnxc218h80bzzkmal45cbddhc5v6j7zfgayzqnh5") (r "1.74.0")))

