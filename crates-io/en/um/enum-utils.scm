(define-module (crates-io en um enum-utils) #:use-module (crates-io))

(define-public crate-enum-utils-0.1.0 (c (n "enum-utils") (v "0.1.0") (d (list (d (n "enum-utils-from-str") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.24.1") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (d #t) (k 0)))) (h "0pfyx2b6wakp5n2jc80534d1xh81jnxd1c7jnhhb7zl568sdki93")))

(define-public crate-enum-utils-0.1.1 (c (n "enum-utils") (v "0.1.1") (d (list (d (n "enum-utils-from-str") (r "^0.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.24.1") (d #t) (k 0)) (d (n "syn") (r "^0.15.33") (d #t) (k 0)))) (h "0dl8bf2gm7y3k8ds88zbzdivwfsfbh0vcyp1jgxpj649v5rfc6hz")))

(define-public crate-enum-utils-0.1.2 (c (n "enum-utils") (v "0.1.2") (d (list (d (n "enum-utils-from-str") (r "^0.1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive_internals") (r "^0.25") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "12irxl7w389cva0w0g47h7vdi6p31v99hczxr631sd8ddmqpycpd")))

