(define-module (crates-io en um enum-ordinalize-derive) #:use-module (crates-io))

(define-public crate-enum-ordinalize-derive-4.0.0 (c (n "enum-ordinalize-derive") (v "4.0.0") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1pp0b72y8g42kvqnv8dw1ayq5nq4y0lk6im1w8l0rq5kqw70vrd8") (r "1.56")))

(define-public crate-enum-ordinalize-derive-4.1.0 (c (n "enum-ordinalize-derive") (v "4.1.0") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1ym5vc077hhyiync5wdh56q8wvabr9g04va3b68dl3wmdqxq7s42") (r "1.56")))

(define-public crate-enum-ordinalize-derive-4.2.0 (c (n "enum-ordinalize-derive") (v "4.2.0") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0x2v2k39hrisc5v4pwbgh2jybvjdbgi9ywvdk6619fv0hq41ik1q") (f (quote (("traits") ("default" "traits")))) (r "1.56")))

(define-public crate-enum-ordinalize-derive-4.2.1 (c (n "enum-ordinalize-derive") (v "4.2.1") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "11sj1id3q1k6z92cwkib8sgbfl3ml22fj86cs9dpkf3w0qj2947k") (f (quote (("traits") ("default" "traits")))) (r "1.56")))

(define-public crate-enum-ordinalize-derive-4.2.2 (c (n "enum-ordinalize-derive") (v "4.2.2") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0kqkv3l6gdv44nm1ny9rv5v0chxny80knwyx303s2vsy1aj242fp") (f (quote (("traits") ("default" "traits")))) (r "1.56")))

(define-public crate-enum-ordinalize-derive-4.2.3 (c (n "enum-ordinalize-derive") (v "4.2.3") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1nfxsyxpflm4xx4zp0ay6ss45yrsv81warn5q61x65ad78ffrv6x") (f (quote (("traits") ("default" "traits")))) (r "1.56")))

(define-public crate-enum-ordinalize-derive-4.2.4 (c (n "enum-ordinalize-derive") (v "4.2.4") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "08w3q3lvn57c4j60sasj73k1yg50171rm8k8bqjfqgaaq2fgmhls") (f (quote (("traits") ("default" "traits")))) (r "1.56")))

(define-public crate-enum-ordinalize-derive-4.2.5 (c (n "enum-ordinalize-derive") (v "4.2.5") (d (list (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1ycb3jgbnckgvjdnx9g9yc7j5585aszihv7mp1kdlzi1kqk4xi7p") (f (quote (("traits") ("default" "traits")))) (r "1.56")))

(define-public crate-enum-ordinalize-derive-4.2.6 (c (n "enum-ordinalize-derive") (v "4.2.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0mknfcs766m0garm4bkj9l2knj2lg797r16r81h4x29d5b2c3drd") (f (quote (("traits") ("default" "traits")))) (r "1.56")))

(define-public crate-enum-ordinalize-derive-4.2.7 (c (n "enum-ordinalize-derive") (v "4.2.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0d2dgb86ds40jcwbrf1g1vkiiqq9jsfrf81m5czb3hg3dfxjc8qj") (f (quote (("traits") ("default" "traits")))) (r "1.56")))

(define-public crate-enum-ordinalize-derive-4.2.8 (c (n "enum-ordinalize-derive") (v "4.2.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "14whzacq4y0j7kcdjan1xrh85jz3pr6zqyy85nlbk4g3w0801ah5") (f (quote (("traits") ("default" "traits")))) (r "1.56")))

(define-public crate-enum-ordinalize-derive-4.2.9 (c (n "enum-ordinalize-derive") (v "4.2.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0aaqnfz3hs5rs1vg1nqh3bks5mgl4s3i2nhfqh062d3b27njr9hm") (f (quote (("traits") ("default")))) (r "1.56")))

(define-public crate-enum-ordinalize-derive-4.3.0 (c (n "enum-ordinalize-derive") (v "4.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "01adxmqpbjid8xx0bgnvmxfj4h5h2kfgc1ka016syj57n1r6sbg1") (f (quote (("traits") ("default")))) (r "1.60")))

(define-public crate-enum-ordinalize-derive-4.3.1 (c (n "enum-ordinalize-derive") (v "4.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1zy53fabazimwv5cl0366k834ybixzl84lxj9mfavbnlfn532a0d") (f (quote (("traits") ("default")))) (r "1.60")))

