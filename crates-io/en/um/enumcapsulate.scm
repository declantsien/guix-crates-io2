(define-module (crates-io en um enumcapsulate) #:use-module (crates-io))

(define-public crate-enumcapsulate-0.1.0 (c (n "enumcapsulate") (v "0.1.0") (d (list (d (n "enumcapsulate-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "tryexpand") (r "^0.7.0") (d #t) (k 2)))) (h "1bb1wvjxc3pvdqm50axyxig0kkya0vry6wb6z689dpb6pb2dwh8y") (f (quote (("derive" "macros") ("default" "derive")))) (s 2) (e (quote (("macros" "dep:enumcapsulate-macros")))) (r "1.70.0")))

(define-public crate-enumcapsulate-0.2.0 (c (n "enumcapsulate") (v "0.2.0") (d (list (d (n "enumcapsulate-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "tryexpand") (r "^0.8.0") (d #t) (k 2)))) (h "149hwkxs7cch6b41fw43maw6mrrp7vcdmm4cjpzm2nq1vzqmfps3") (f (quote (("derive" "macros") ("default" "derive")))) (s 2) (e (quote (("macros" "dep:enumcapsulate-macros")))) (r "1.74.0")))

(define-public crate-enumcapsulate-0.2.2 (c (n "enumcapsulate") (v "0.2.2") (d (list (d (n "enumcapsulate-macros") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "tryexpand") (r "^0.8.2") (d #t) (k 2)))) (h "04xpca5w4mx5zyjqwd8nr5w9gis5d19xnrjdjda6vcm07y9wwrai") (f (quote (("derive" "macros") ("default" "derive")))) (s 2) (e (quote (("macros" "dep:enumcapsulate-macros")))) (r "1.74.0")))

(define-public crate-enumcapsulate-0.3.0 (c (n "enumcapsulate") (v "0.3.0") (d (list (d (n "enumcapsulate-macros") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "tryexpand") (r "^0.8.4") (d #t) (k 2)))) (h "1dcx11jyrpj41s2bj34ifgrs4n98q7apzscjb8f5sw81n8vrzfy1") (f (quote (("derive" "macros") ("default" "derive")))) (s 2) (e (quote (("macros" "dep:enumcapsulate-macros")))) (r "1.74.0")))

