(define-module (crates-io en um enumber) #:use-module (crates-io))

(define-public crate-enumber-0.1.0 (c (n "enumber") (v "0.1.0") (d (list (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07gk8p9nnl87853m9abqwmcbyxy9bdarprma85lg5z6nk806kf2c")))

(define-public crate-enumber-0.2.0 (c (n "enumber") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1g8xx3vd98glc76n86na0rpc4r7bighn9g621bar2rhvv5p4xac6")))

(define-public crate-enumber-0.3.0 (c (n "enumber") (v "0.3.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06gd26x1fpxphkzlxnkmyjc7zalrr2vw88lz4yg23x6q62dv8dgs")))

(define-public crate-enumber-0.3.1 (c (n "enumber") (v "0.3.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1gxxqms1zpadyy1whq0ms1vwymz19mq197gfq53dhxnx14cig50f")))

