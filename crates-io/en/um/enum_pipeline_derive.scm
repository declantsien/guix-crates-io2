(define-module (crates-io en um enum_pipeline_derive) #:use-module (crates-io))

(define-public crate-enum_pipeline_derive-0.1.0 (c (n "enum_pipeline_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full"))) (d #t) (k 0)))) (h "1v4b56jbm3ipc0nc6qwb9qlkbp6jrml47cj616y2syjl9p0i4sy7") (y #t)))

(define-public crate-enum_pipeline_derive-1.0.0 (c (n "enum_pipeline_derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full"))) (d #t) (k 0)))) (h "0j46asjyzn4lzykwrmlxiw3sxds04fgiydj0lmafrfkcfpz546g7") (y #t)))

(define-public crate-enum_pipeline_derive-1.0.1 (c (n "enum_pipeline_derive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full"))) (d #t) (k 0)))) (h "09r5cisljlj6v4anf8ckbmgkabf91lfgzds11jcaalm0gyarvw06") (y #t)))

(define-public crate-enum_pipeline_derive-1.0.2 (c (n "enum_pipeline_derive") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full"))) (d #t) (k 0)))) (h "0208gjrj34cbi6ibfs2zxv9lcyx31nlhwk4qpc118mgql733lwxl") (y #t)))

(define-public crate-enum_pipeline_derive-2.0.0 (c (n "enum_pipeline_derive") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.80") (f (quote ("full"))) (d #t) (k 0)))) (h "067j1l25g83sfp0dk5913pf3vxrs7dsp3b4cz5mwldgz4zzdwi2i")))

