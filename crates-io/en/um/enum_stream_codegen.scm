(define-module (crates-io en um enum_stream_codegen) #:use-module (crates-io))

(define-public crate-enum_stream_codegen-0.0.0 (c (n "enum_stream_codegen") (v "0.0.0") (d (list (d (n "aster") (r "^0.16") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "panini_codegen") (r "^0.0") (d #t) (k 0)) (d (n "quasi") (r "^0.10") (d #t) (k 0)) (d (n "quasi_macros") (r "^0.10") (d #t) (k 0)))) (h "0rkng60f86b5kg7f8p7hr3cj0fi58w638dzhspkfdsqr4f1bv3b5")))

(define-public crate-enum_stream_codegen-0.1.0 (c (n "enum_stream_codegen") (v "0.1.0") (d (list (d (n "enum_coder") (r "^0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "panini_common") (r "^0.0") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "144p1rz4c1nh5rpwbd5s77fhj07yvszn652dj1dl5g02qwl0pcjl")))

