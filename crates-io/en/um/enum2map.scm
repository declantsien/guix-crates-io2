(define-module (crates-io en um enum2map) #:use-module (crates-io))

(define-public crate-enum2map-0.1.0 (c (n "enum2map") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "1rjwh0dv56fs564wxqhqaiz46kjhxqpvmm48hjny1c0mlqfnidsf")))

(define-public crate-enum2map-0.1.1 (c (n "enum2map") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "14a1a76gwl9h27z5ck0bgi5y3xkajs99zfdfaafl6hnf4ya7a5vh")))

