(define-module (crates-io en um enum_for_matches) #:use-module (crates-io))

(define-public crate-enum_for_matches-0.1.0 (c (n "enum_for_matches") (v "0.1.0") (h "020203pv05qyndswql9v9fv8ydxi5wycikhmc0wf5ybdk6xmgicq")))

(define-public crate-enum_for_matches-0.1.1 (c (n "enum_for_matches") (v "0.1.1") (h "0q62x6wvghaaa1cd1i7z0glf6va2ygibc4w4kf255swbf4h5g4zg")))

