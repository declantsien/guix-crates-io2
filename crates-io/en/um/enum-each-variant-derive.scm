(define-module (crates-io en um enum-each-variant-derive) #:use-module (crates-io))

(define-public crate-enum-each-variant-derive-0.1.0 (c (n "enum-each-variant-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.15") (d #t) (k 0)))) (h "17vj595inmrvi2fz2wqwcrb7v3p2ywajxg103rhjaqx12587rjwm")))

(define-public crate-enum-each-variant-derive-0.1.1 (c (n "enum-each-variant-derive") (v "0.1.1") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.14") (d #t) (k 0)))) (h "04kmfm2adhrphngkh8bc6i7b97pp3f40qv9wg61ajbfhikrh3344")))

(define-public crate-enum-each-variant-derive-0.1.2 (c (n "enum-each-variant-derive") (v "0.1.2") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.14") (d #t) (k 0)))) (h "1zfm3ni6m9x7r8wxvq5dv7cbcxrx1ymnrc96i9ipnrwznajwh26q")))

(define-public crate-enum-each-variant-derive-0.1.3 (c (n "enum-each-variant-derive") (v "0.1.3") (d (list (d (n "quote") (r "^0.4.2") (d #t) (k 0)) (d (n "syn") (r "^0.12.14") (d #t) (k 0)))) (h "0j549lf1h8k313cz5is1wy94ylmfvsfrzy3d5qqrm5y9aw238fna")))

