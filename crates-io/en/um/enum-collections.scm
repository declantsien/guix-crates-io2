(define-module (crates-io en um enum-collections) #:use-module (crates-io))

(define-public crate-enum-collections-0.1.0 (c (n "enum-collections") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "enum-collections-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "enum-map") (r "^2.4.2") (d #t) (k 2)))) (h "1qkwcln30bxa00xng289g3wflxk97sbz1p0r6ra235g2m12rgkm2")))

(define-public crate-enum-collections-0.2.0 (c (n "enum-collections") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "enum-collections-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "enum-map") (r "^2.4.2") (d #t) (k 2)))) (h "1r5b24yg4ycigj84vy3l47wanrxwikddz5703pmaihyhspbidrvg")))

(define-public crate-enum-collections-0.3.0 (c (n "enum-collections") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "enum-collections-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "enum-map") (r "^2.4.2") (d #t) (k 2)))) (h "1diyx29g026pz9qf61schbn549k0kvx91zw39gq98n0vyhiiv2ji")))

(define-public crate-enum-collections-0.4.0 (c (n "enum-collections") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "enum-collections-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "enum-map") (r "^2.4.2") (d #t) (k 2)))) (h "0yzs5b4j41rk4laavd3zzy6jxhmn0pzkswv4sr2yka10w7r5dvwk")))

(define-public crate-enum-collections-0.5.0 (c (n "enum-collections") (v "0.5.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "enum-collections-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "enum-map") (r "^2.4.2") (d #t) (k 2)))) (h "0r3gfvxn62i0qyqhmrp7zgmgzjq3rmrz6kcy9gr6dizb1gb63nwy")))

(define-public crate-enum-collections-0.6.0 (c (n "enum-collections") (v "0.6.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "enum-collections-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "enum-map") (r "^2.4.2") (d #t) (k 2)))) (h "1sjgk24pfxrq3vkz6lzx9wxl72z71172j8qd14l8ih1nx348djrk")))

(define-public crate-enum-collections-0.7.0 (c (n "enum-collections") (v "0.7.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "enum-collections-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "enum-map") (r "^2.4.2") (d #t) (k 2)))) (h "0wbyc13jkiarwzk0npnc8wx3qj7rp14idad9yvfdc5sd14m95abf")))

(define-public crate-enum-collections-0.8.0 (c (n "enum-collections") (v "0.8.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "enum-collections-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "enum-map") (r "^2.4.2") (d #t) (k 2)))) (h "1gvx6fn8czmmf30w86flj1c9x8pnbzrq8iwhqsfhf5pjdzh27qdi")))

(define-public crate-enum-collections-0.9.0 (c (n "enum-collections") (v "0.9.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "enum-collections-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "enum-map") (r "^2.4.2") (d #t) (k 2)))) (h "09pzf9ccls0w8852xayykcmijgb7i4mq0x2f5dfmpcskqzs8jjar")))

(define-public crate-enum-collections-0.10.0 (c (n "enum-collections") (v "0.10.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "enum-collections-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "enum-map") (r "^2.4.2") (d #t) (k 2)))) (h "077f220npbsdjzi8jqbgyn6fabz93xw678vd1cc0jjcdxambf32n") (f (quote (("eq") ("default" "debug" "eq") ("debug"))))))

(define-public crate-enum-collections-1.0.0 (c (n "enum-collections") (v "1.0.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "enum-collections-macros") (r "^1.0.0") (d #t) (k 0)))) (h "0zlnvrhb953akrg31zis11sxb4fvsy6c2y51kcznhlqhwxng0aj4") (f (quote (("variants" "enum-collections-macros/variants") ("eq") ("default" "debug" "eq") ("debug" "variants"))))))

(define-public crate-enum-collections-1.0.1 (c (n "enum-collections") (v "1.0.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "enum-collections-macros") (r "^1.0.0") (d #t) (k 0)))) (h "00s9x9mpvm1s6a3615a2rlziw0f23f2dqllx1wkg6g9wnlb7a7rr") (f (quote (("variants" "enum-collections-macros/variants") ("eq") ("default" "debug" "eq") ("debug" "variants"))))))

(define-public crate-enum-collections-1.1.0 (c (n "enum-collections") (v "1.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "enum-collections-macros") (r "^1.0.0") (d #t) (k 0)))) (h "1dm7i05wny0vs7i855kp7g7n3rxayxrdg3457sq6c01q5rfs8845") (f (quote (("variants" "enum-collections-macros/variants") ("eq") ("default" "debug" "eq") ("debug" "variants"))))))

(define-public crate-enum-collections-1.1.1 (c (n "enum-collections") (v "1.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "enum-collections-macros") (r "^1.0.0") (d #t) (k 0)))) (h "1vyhspc0rxpy70y5xai31fypcnjlvcr8y86bghlsrarwz6rkjf08") (f (quote (("variants" "enum-collections-macros/variants") ("eq") ("default" "debug" "eq") ("debug" "variants"))))))

(define-public crate-enum-collections-1.2.0 (c (n "enum-collections") (v "1.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "enum-collections-macros") (r "^1.0.0") (d #t) (k 0)))) (h "1cwsj96sk9rlqjfbisvjsnn3gnxd5y61735vvcsi2jk4fhgmbagg") (f (quote (("variants" "enum-collections-macros/variants") ("ext") ("eq") ("default" "debug" "eq") ("debug" "variants")))) (y #t)))

(define-public crate-enum-collections-1.3.0 (c (n "enum-collections") (v "1.3.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "enum-collections-macros") (r "^1.0.0") (d #t) (k 0)))) (h "18z2rfmb3i536i7zhaq3c3y0h730mlmfvpva39537y2qzpql4dxg") (f (quote (("variants" "enum-collections-macros/variants") ("ext") ("eq") ("default" "debug" "eq") ("debug" "variants")))) (y #t)))

(define-public crate-enum-collections-1.4.0 (c (n "enum-collections") (v "1.4.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "enum-collections-macros") (r "^1.0.0") (d #t) (k 0)))) (h "0xh8q4nfg9ig7hwqy19p410q7gdvfqgx7wrvv1vk447khl3bp45g") (f (quote (("variants" "enum-collections-macros/variants") ("ext") ("eq") ("default" "debug" "eq") ("debug" "variants")))) (y #t)))

(define-public crate-enum-collections-1.4.1 (c (n "enum-collections") (v "1.4.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "enum-collections-macros") (r "^1.0.1") (d #t) (k 0)))) (h "0k0wii43la17mknhkzlg3ybnkqpf21lp3zii8kpw51ram7xkrc3c") (f (quote (("variants" "enum-collections-macros/variants") ("ext") ("eq") ("default" "debug" "eq") ("debug" "variants"))))))

(define-public crate-enum-collections-1.5.0 (c (n "enum-collections") (v "1.5.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "enum-collections-macros") (r "^1.0.1") (d #t) (k 0)))) (h "0747gf2h3sdjijv02lc3s8fwwkpyjfk8wzqf032mzkgygbacmh7m") (f (quote (("variants" "enum-collections-macros/variants") ("ext") ("eq") ("default" "debug" "eq") ("debug" "variants"))))))

