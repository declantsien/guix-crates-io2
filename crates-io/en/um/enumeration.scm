(define-module (crates-io en um enumeration) #:use-module (crates-io))

(define-public crate-enumeration-0.1.0 (c (n "enumeration") (v "0.1.0") (h "06v5wjbcirjfaf4dh19jnn01mfkm4sk80k8vwqwrrlxsw21blc2c")))

(define-public crate-enumeration-1.0.0 (c (n "enumeration") (v "1.0.0") (h "0k8ss0j95hpnwg150dkggyr7l2ay937dq6vn2m9zkjfngrzfhclh")))

(define-public crate-enumeration-1.0.1 (c (n "enumeration") (v "1.0.1") (h "1w4wm8c772sv4nfrbnyyr757indnnqgjscjq8fwwcczccwmy63f8")))

(define-public crate-enumeration-1.0.2 (c (n "enumeration") (v "1.0.2") (h "0s8hp8kd3f7nkf0xij128csskq182zgwcmc7aizb6dxkb10nnw3q")))

(define-public crate-enumeration-1.1.0 (c (n "enumeration") (v "1.1.0") (h "17p1x1rv884r9wjmjf39fa0rf12dqral6bpyir87q8y1q1wip83y")))

(define-public crate-enumeration-1.2.0 (c (n "enumeration") (v "1.2.0") (h "0f0w0vqarr482c5mbih7g72dk2sxfm6vy03ab7xzvycpsnvk8fzy")))

(define-public crate-enumeration-1.2.1 (c (n "enumeration") (v "1.2.1") (h "0h4imqh7l6j1qpa4mgpz1i2x7p3i659skh8k9scghb0d6f4086xj")))

(define-public crate-enumeration-1.2.2 (c (n "enumeration") (v "1.2.2") (h "1sq9n60wpcgjzax0h0620pj6bagl31gs85pn7ixj0fsvnlddwlp0")))

