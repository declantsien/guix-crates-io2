(define-module (crates-io en um enum-display-macro) #:use-module (crates-io))

(define-public crate-enum-display-macro-0.1.0 (c (n "enum-display-macro") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.101") (f (quote ("full"))) (d #t) (k 0)))) (h "16pbawyrldybanm294mp99vjg3baj617al5rm2syycb4dkld40bx")))

(define-public crate-enum-display-macro-0.1.1 (c (n "enum-display-macro") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.101") (f (quote ("full"))) (d #t) (k 0)))) (h "1259pw95r9yfy62rjvr94y404jv3g8cf31skkzr0b7hb19fjw6pd")))

(define-public crate-enum-display-macro-0.1.2 (c (n "enum-display-macro") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.101") (f (quote ("full"))) (d #t) (k 0)))) (h "0l8h9c6ij3lamqnk2pjl14if09q0w5axqf2xhbr5yhmrmfrdafhm")))

(define-public crate-enum-display-macro-0.1.3 (c (n "enum-display-macro") (v "0.1.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.101") (f (quote ("full"))) (d #t) (k 0)))) (h "0y5k3hrp2aadc24z84ziyh643n7fbc31swnjn1p6gpky0hv3mkm0")))

(define-public crate-enum-display-macro-0.1.4 (c (n "enum-display-macro") (v "0.1.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.101") (f (quote ("full"))) (d #t) (k 0)))) (h "150dqvsdqsbd380wlz1ppnk4lm6zv3aarfzyclc1p5vvzvr2rgnl")))

