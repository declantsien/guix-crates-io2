(define-module (crates-io en um enum-tags) #:use-module (crates-io))

(define-public crate-enum-tags-0.1.0 (c (n "enum-tags") (v "0.1.0") (d (list (d (n "enum-tags-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "enum-tags-traits") (r "^0.1.0") (d #t) (k 0)))) (h "0jam7dzi3mlb0ywbxf79hh9bwyd9x4ajc2k9jzzc5fjqx2hibvw8")))

