(define-module (crates-io en um enum_to_string) #:use-module (crates-io))

(define-public crate-enum_to_string-0.1.0 (c (n "enum_to_string") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "1iyam6ssbpdwsjsn7vhwdsm25vl8hx3sxdgbfbf7z6y4riw0w56a")))

