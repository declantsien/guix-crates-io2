(define-module (crates-io en um enumflags2_derive) #:use-module (crates-io))

(define-public crate-enumflags2_derive-0.5.0 (c (n "enumflags2_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1qwgv2y21kly5nnr9yqjf9a98ssfdcnh4pfwbjsnq2fwzqymn0cy") (f (quote (("nostd") ("default"))))))

(define-public crate-enumflags2_derive-0.6.0 (c (n "enumflags2_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18j210zvrxibh66i076i1vfpdfiky89n8fbqqkz8g86pqjv317zg")))

(define-public crate-enumflags2_derive-0.6.1 (c (n "enumflags2_derive") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qfzghww5agwrr2f9sjr38fvx1sh0km07lspcv9klb7ix1irxgqh")))

(define-public crate-enumflags2_derive-0.6.2 (c (n "enumflags2_derive") (v "0.6.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "154qzfkq0inb17294gmmbjxqc7yxyln2ipa69bavhi1h472k9xpc") (f (quote (("not_literal"))))))

(define-public crate-enumflags2_derive-0.6.3 (c (n "enumflags2_derive") (v "0.6.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "00rgmakzk34aa7f7qn84h29m6vayfbmdxj9qnzpiwc50msnazn9f") (f (quote (("not_literal"))))))

(define-public crate-enumflags2_derive-0.6.4 (c (n "enumflags2_derive") (v "0.6.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1kkcwi4n76bi1c16ms00dyk4d393gdf29kpr4k9zsn5z7m7fjvll") (f (quote (("not_literal"))))))

(define-public crate-enumflags2_derive-0.7.0-preview1 (c (n "enumflags2_derive") (v "0.7.0-preview1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "09d1w2ch8c3pb0hyc9pi6h9dq75rs9lpsxdfmnxkhglw7vrggmbf")))

(define-public crate-enumflags2_derive-0.7.0 (c (n "enumflags2_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kypbpy5fff2sqz964pzw0vh48j0n9ydnbvrqbkqr0i719vnylik")))

(define-public crate-enumflags2_derive-0.7.2 (c (n "enumflags2_derive") (v "0.7.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kcsw59s6yqd9pm3b5s0hmk94iwy0c2f0qdcxvwymigcxxrfj5vc") (r "1.41.1")))

(define-public crate-enumflags2_derive-0.7.3 (c (n "enumflags2_derive") (v "0.7.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "194d20akzhyraj2dla4axcifyji6x5xwcp8jza26zaybjsacfkhl") (r "1.41.1")))

(define-public crate-enumflags2_derive-0.7.4 (c (n "enumflags2_derive") (v "0.7.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bnmyqv0x19b5lr4pqa28h1ks74gnak08qyly8cry9b8wk2w73gm") (r "1.41.1")))

(define-public crate-enumflags2_derive-0.7.6 (c (n "enumflags2_derive") (v "0.7.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09hcc0plcd54gbdkij6wkw095gwfr4brvmzmnikikzdxrln7fb4x") (r "1.56")))

(define-public crate-enumflags2_derive-0.7.7 (c (n "enumflags2_derive") (v "0.7.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0iapq76azgkqxby4v117a3jssa9rz7n18vla4i09grc3gngiz6jy") (r "1.56")))

(define-public crate-enumflags2_derive-0.7.8 (c (n "enumflags2_derive") (v "0.7.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0il2856z9qxzm11m6i9kr2jh75mfwmpcwlrr7qd4lp9mrl0jhppr") (r "1.56")))

(define-public crate-enumflags2_derive-0.7.9 (c (n "enumflags2_derive") (v "0.7.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1i1vjp2si8jq7cib97c26d3cysm0xip30fs5f84l46qv0xs54y2w") (r "1.56")))

