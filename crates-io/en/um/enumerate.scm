(define-module (crates-io en um enumerate) #:use-module (crates-io))

(define-public crate-enumerate-0.1.0 (c (n "enumerate") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.36") (f (quote ("full"))) (d #t) (k 0)))) (h "0c2k4iyavs39jlyarvgisr0s5vdyawabb8drfhjvnld6rc7y95y2")))

(define-public crate-enumerate-0.1.1 (c (n "enumerate") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.36") (f (quote ("full"))) (d #t) (k 0)))) (h "1pvshdpxl1w2xi3wv7fsiqqyxp5q6wqiy67115z3xmna76qbzdld")))

