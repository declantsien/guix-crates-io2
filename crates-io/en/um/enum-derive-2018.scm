(define-module (crates-io en um enum-derive-2018) #:use-module (crates-io))

(define-public crate-enum-derive-2018-0.2.0 (c (n "enum-derive-2018") (v "0.2.0") (d (list (d (n "macro-attr-2018") (r "^0.3.3") (d #t) (k 2)))) (h "1ji1xvn0v9jisjid1nyyj16b8zq5jcxi024bl77qviz25yzaiwn8") (f (quote (("std") ("default" "std"))))))

(define-public crate-enum-derive-2018-0.2.1 (c (n "enum-derive-2018") (v "0.2.1") (d (list (d (n "macro-attr-2018") (r "^0.3.3") (d #t) (k 2)))) (h "06q98lii3iqnsx9kz8vax3yh1z7cmypy294z4z5mcb3ypvj6cahr") (f (quote (("std") ("default" "std"))))))

(define-public crate-enum-derive-2018-0.3.0 (c (n "enum-derive-2018") (v "0.3.0") (d (list (d (n "macro-attr-2018") (r "^0.4.0") (d #t) (k 2)))) (h "007lk1i8fjv47n4jsg5s3yiy3npw2fq9iizasfhawybz2niz6mpb") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-enum-derive-2018-0.3.1 (c (n "enum-derive-2018") (v "0.3.1") (d (list (d (n "macro-attr-2018") (r "^0.4.0") (d #t) (k 2)))) (h "1z74r9gi1hpqr8vxm3wnay47f1wj8y3l26j0631qc4d3ffmx8jdx") (f (quote (("std") ("default" "std"))))))

(define-public crate-enum-derive-2018-1.0.0 (c (n "enum-derive-2018") (v "1.0.0") (d (list (d (n "macro-attr-2018") (r "^1.0.0") (d #t) (k 2)))) (h "0fpzqcs50845d6bqbpcwl3jp8wqjh9fxxflqxx4v8zlppr03978j") (f (quote (("std") ("default" "std"))))))

(define-public crate-enum-derive-2018-1.0.1 (c (n "enum-derive-2018") (v "1.0.1") (d (list (d (n "macro-attr-2018") (r "^1.0.0") (d #t) (k 2)))) (h "0w5ajnp5ypkj2h96jnf7m0hrzqs45a07nkv6qnrvxqc58hyq65wy") (f (quote (("std") ("default" "std"))))))

(define-public crate-enum-derive-2018-1.0.2 (c (n "enum-derive-2018") (v "1.0.2") (d (list (d (n "macro-attr-2018") (r "^1.1.1") (d #t) (k 2)))) (h "18hbg7q7sg0yfgkzbczdg2g462hgwndk7gdq9ifp9ca9wivym81i") (f (quote (("std") ("default" "std"))))))

(define-public crate-enum-derive-2018-2.0.0 (c (n "enum-derive-2018") (v "2.0.0") (d (list (d (n "macro-attr-2018") (r "^2.0.0") (d #t) (k 2)))) (h "074hm752rj41k5hp5zbkl2rckkdw6jbchiw0p8pzgg7wacb974jz") (f (quote (("std") ("default" "std")))) (r "1.60")))

(define-public crate-enum-derive-2018-2.0.1 (c (n "enum-derive-2018") (v "2.0.1") (d (list (d (n "macro-attr-2018") (r "^2.0.0") (d #t) (k 2)))) (h "0yz2sdab3hj4mwxjv2fczjhymphji1yihrn81hg4ynqc5vbsl4jc") (f (quote (("std") ("default" "std")))) (r "1.60")))

(define-public crate-enum-derive-2018-3.0.0 (c (n "enum-derive-2018") (v "3.0.0") (d (list (d (n "macro-attr-2018") (r "^2.0.0") (d #t) (k 2)))) (h "0d87flpnb8dv4zrwjpyca21n3vlpdpqwbh03vn1fp4za9vmjdi5b") (f (quote (("std") ("default" "std")))) (r "1.71")))

(define-public crate-enum-derive-2018-3.0.1 (c (n "enum-derive-2018") (v "3.0.1") (d (list (d (n "macro-attr-2018") (r "^3.0.0") (d #t) (k 2)))) (h "1zldpjd7qfskcx2bv4fvsjrj2crhfk7brmv1r29q55hhnnb1bnmx") (f (quote (("std") ("default" "std")))) (r "1.71")))

