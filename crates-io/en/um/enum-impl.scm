(define-module (crates-io en um enum-impl) #:use-module (crates-io))

(define-public crate-enum-impl-0.1.0 (c (n "enum-impl") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "13fy1zz3cqaf8wyihp5i2dwwvz0ihphzcwlvkip4cb1rcarx9a08")))

