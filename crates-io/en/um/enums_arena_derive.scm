(define-module (crates-io en um enums_arena_derive) #:use-module (crates-io))

(define-public crate-enums_arena_derive-0.1.0 (c (n "enums_arena_derive") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (d #t) (k 0)))) (h "0bv389hxvbj8hb2qv9xjr14n11890vfj515w1ffbahb16y3s72wi")))

(define-public crate-enums_arena_derive-0.1.1 (c (n "enums_arena_derive") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "enums_arena_defines") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (d #t) (k 0)))) (h "0szx3rn4rnlx2jdcr0f4qgj3vpqwx9ik944b8xi2x104y7zj316x")))

(define-public crate-enums_arena_derive-0.1.2 (c (n "enums_arena_derive") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "enums_arena_defines") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (d #t) (k 0)))) (h "1a3wh0fddxs3v74rcbv1bvi95dy60naanhg1pvl8hdwqr1pf5i0i")))

(define-public crate-enums_arena_derive-0.1.3 (c (n "enums_arena_derive") (v "0.1.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "enums_arena_defines") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (d #t) (k 0)))) (h "14h15bf1w74588y0g3rs1mf601c5ws39yhlfwvp2m3501i24y7aw")))

(define-public crate-enums_arena_derive-0.1.4 (c (n "enums_arena_derive") (v "0.1.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "enums_arena_defines") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (d #t) (k 0)))) (h "042g19v434kq2fxq2agb0l0dyhqz2pihkfp1gj5rjk5ni39kh9wg")))

