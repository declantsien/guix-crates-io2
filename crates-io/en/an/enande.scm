(define-module (crates-io en an enande) #:use-module (crates-io))

(define-public crate-enande-0.1.0 (c (n "enande") (v "0.1.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.19") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 2)))) (h "0xgf2r5jkgrp7v85lxzd8spk4nr8s1q3a2di4ig0d5sijv4pqc15") (y #t)))

(define-public crate-enande-0.1.1 (c (n "enande") (v "0.1.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.19") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 2)))) (h "136nnynrnkry6kpfzrb4vjx2i8cs0bxgp6swy8g88ryhppx662hp") (y #t)))

(define-public crate-enande-0.2.0 (c (n "enande") (v "0.2.0") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.19") (f (quote ("alloc"))) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.19") (d #t) (k 2)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 2)))) (h "1pswh7jnbn9kd1d6ry75sqz101vi75dfk717j0da31wiy0846v9p") (y #t)))

(define-public crate-enande-0.2.1 (c (n "enande") (v "0.2.1") (d (list (d (n "futures-preview") (r "^0.3.0-alpha.19") (f (quote ("alloc"))) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.19") (d #t) (k 2)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 2)))) (h "11bzk31m9kfj30s94cy5m8skcjp7r0nvr3z2fakazjccflzy1d7r") (y #t)))

