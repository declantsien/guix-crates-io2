(define-module (crates-io en vu envutil) #:use-module (crates-io))

(define-public crate-envutil-0.0.1 (c (n "envutil") (v "0.0.1") (h "1wl2swdvm8vhj8aknq5qxicrcggwrvaqms1zgslv5w8v01kn95zq") (y #t)))

(define-public crate-envutil-0.0.2 (c (n "envutil") (v "0.0.2") (h "1p0v0mi5k6wbxb34rw9i3pkl1d0zffkwwy6x6rwzsxir2a744av4")))

(define-public crate-envutil-0.0.3 (c (n "envutil") (v "0.0.3") (h "1511an1mvrm0lyw5mhhk8y5rlvl8d18qqkvdwda9jdahmsg64j3c")))

(define-public crate-envutil-0.0.4 (c (n "envutil") (v "0.0.4") (h "0kajyf8bs0cx0qh90a56h07qib93rljxnwkkhw2cp1xi5xzin3g9")))

