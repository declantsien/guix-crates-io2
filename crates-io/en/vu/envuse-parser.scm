(define-module (crates-io en vu envuse-parser) #:use-module (crates-io))

(define-public crate-envuse-parser-0.1.0-beta.0 (c (n "envuse-parser") (v "0.1.0-beta.0") (h "11qb1pvbicrfz3mg7wnwwbrn7v266p4r1xbp0cl2jfq3q1vgqviy")))

(define-public crate-envuse-parser-0.1.0-beta.1 (c (n "envuse-parser") (v "0.1.0-beta.1") (h "1z7xw8smvjnp7r4zkfhv6jvsjhiscnjv3m5dbgv9j3yh2pl19v1g")))

(define-public crate-envuse-parser-0.1.0-beta.2 (c (n "envuse-parser") (v "0.1.0-beta.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ziggvi3q9qlpd29gay7csgl38iximbdl7wkh9vinrbhjf8gnmvl")))

(define-public crate-envuse-parser-0.1.0-beta.3 (c (n "envuse-parser") (v "0.1.0-beta.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l9nshphzqqk31vi8rx2pf50a7n1dlbhw6g7g10lccbw3pqrn66w")))

(define-public crate-envuse-parser-0.2.0-beta.4 (c (n "envuse-parser") (v "0.2.0-beta.4") (d (list (d (n "insta") (r "^1.21.0") (f (quote ("yaml" "serde"))) (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "15ypd3rc014g5b9f48zhkpsa1v653mghq6vkr96ay2h8wii288s7")))

(define-public crate-envuse-parser-0.2.0-beta.5 (c (n "envuse-parser") (v "0.2.0-beta.5") (d (list (d (n "insta") (r "^1.21.0") (f (quote ("yaml" "serde"))) (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rhj2vscgbrih8mdw1xnsgcwcwsi1p50g7vmgsi3qd1ky4n6115f")))

(define-public crate-envuse-parser-0.4.0 (c (n "envuse-parser") (v "0.4.0") (d (list (d (n "insta") (r "^1.21.0") (f (quote ("yaml" "serde"))) (d #t) (k 2)) (d (n "js-sys") (r "^0.3.60") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)))) (h "0phk6wn27397fq39b0npjx5jhx6arxmw93gw75q3rckmpvqqkkjj") (f (quote (("with-js"))))))

(define-public crate-envuse-parser-0.5.1 (c (n "envuse-parser") (v "0.5.1") (d (list (d (n "insta") (r "^1.21.0") (f (quote ("yaml" "serde"))) (d #t) (k 2)) (d (n "js-sys") (r "^0.3.60") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)))) (h "0y186xy7198nibnzaf3f6qgx9jg136sbsr6scfq9lfdnnnbyhn2p") (f (quote (("with-js"))))))

(define-public crate-envuse-parser-0.6.0 (c (n "envuse-parser") (v "0.6.0") (d (list (d (n "insta") (r "^1.21.0") (f (quote ("yaml" "serde"))) (d #t) (k 2)) (d (n "js-sys") (r "^0.3.60") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)))) (h "0k8ax5q6fk6wsaj8di88xicakagp7kk1nxprdbmdsrcpm1f0175d") (f (quote (("with-js"))))))

(define-public crate-envuse-parser-0.7.0 (c (n "envuse-parser") (v "0.7.0") (d (list (d (n "insta") (r "^1.21.0") (f (quote ("yaml" "serde"))) (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l4psmkcy7yfv37hk4cypnz9fcff46aadm6painfkaxwl5rrqspn")))

(define-public crate-envuse-parser-0.8.0 (c (n "envuse-parser") (v "0.8.0") (d (list (d (n "insta") (r "^1.21.0") (f (quote ("yaml" "serde"))) (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "03mhcjaakkih9nq9a2qj7yldrvcnd1gzk5lnr9rmhiqapysv8yay")))

(define-public crate-envuse-parser-0.8.1 (c (n "envuse-parser") (v "0.8.1") (d (list (d (n "insta") (r "^1.21.0") (f (quote ("yaml" "serde"))) (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1r23wxv202d1sdpbmz62mwxv6hglm177dlsmbx9jdjynvpph17k1")))

(define-public crate-envuse-parser-0.8.2 (c (n "envuse-parser") (v "0.8.2") (d (list (d (n "insta") (r "^1.21.0") (f (quote ("yaml" "serde"))) (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lrhly6jhdd8mi6js3csndsr186q4xz0wkxf599d272cqpb6c0a5")))

(define-public crate-envuse-parser-0.9.0 (c (n "envuse-parser") (v "0.9.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "insta") (r "^1.21.0") (f (quote ("yaml" "serde"))) (d #t) (k 2)))) (h "0hzbhdg4bf1xq5hxhh250c6fa5qycw34lhswjmx4zf8kklxafd2j")))

(define-public crate-envuse-parser-0.9.1 (c (n "envuse-parser") (v "0.9.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "insta") (r "^1.21.0") (f (quote ("yaml" "serde"))) (d #t) (k 2)))) (h "1aia4yzm8kx418kl8yl00l2fxhb0v4pywkc8yly4gisdbb4dwhpl")))

