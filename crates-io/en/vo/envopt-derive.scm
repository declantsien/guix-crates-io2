(define-module (crates-io en vo envopt-derive) #:use-module (crates-io))

(define-public crate-envopt-derive-0.0.0 (c (n "envopt-derive") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1ngl1mrf5mxmq4x1ybfbkljgrg40qlcw4h8vb217zmcw1hz8fr06")))

