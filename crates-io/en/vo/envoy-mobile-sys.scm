(define-module (crates-io en vo envoy-mobile-sys) #:use-module (crates-io))

(define-public crate-envoy-mobile-sys-0.1.0 (c (n "envoy-mobile-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)))) (h "0iwp585m890bkn6cncga855vs759rx733gyimyhnlqskakrfrc78")))

(define-public crate-envoy-mobile-sys-0.1.1 (c (n "envoy-mobile-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)))) (h "1r3lnvnd8j5crwhq5g2fll5flm7n4lixl3jj6v9rbcgvkd0z2qap")))

(define-public crate-envoy-mobile-sys-0.1.2 (c (n "envoy-mobile-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)))) (h "0qbh9fv2bib24bczbbfb75kzw30d7pax8myml5g5x21rg8hi29bm") (l "envoy_mobile")))

