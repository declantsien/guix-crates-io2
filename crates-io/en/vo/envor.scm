(define-module (crates-io en vo envor) #:use-module (crates-io))

(define-public crate-envor-0.1.0 (c (n "envor") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0qqlhy9fd51l4kq4rw27b6s7gfk4jif2xwwb6053p96h1yh5hpgl")))

(define-public crate-envor-0.1.1 (c (n "envor") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "0ggw0cb4frjlrndyan8hlllnm0vc567fidvpgr7xc50g3s1diyan")))

(define-public crate-envor-0.1.3 (c (n "envor") (v "0.1.3") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)))) (h "02509x5dnf88glqby5a86sh6i9r16n5g31ax8j8vwll0h5rhw5a0")))

(define-public crate-envor-0.1.5 (c (n "envor") (v "0.1.5") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)))) (h "0fw0yls754xzvq0xj05q931lf8zjaflb1nxgwflmv7kz931f3pbf")))

