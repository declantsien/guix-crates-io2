(define-module (crates-io en vo envoption) #:use-module (crates-io))

(define-public crate-envoption-0.1.0 (c (n "envoption") (v "0.1.0") (h "1mr62aaxqnj5hvnb8nn1lzayk168ar8cbmszaps065y5lx3qhz19")))

(define-public crate-envoption-0.1.1 (c (n "envoption") (v "0.1.1") (h "1aq1vhp977qsbn5c3hv89gba985v7k9gyz4730nin9x51fj60bsl")))

(define-public crate-envoption-0.1.2 (c (n "envoption") (v "0.1.2") (h "18m6658fkjfmlyy7yz9fglyidfcgfxjvv532fpwih0blcj0lr9qw")))

(define-public crate-envoption-0.2.0 (c (n "envoption") (v "0.2.0") (h "16hml738qyw5ny0z4vycy4jmvj974kpb0yklyiwjinpbp78mz7pj")))

(define-public crate-envoption-0.2.1 (c (n "envoption") (v "0.2.1") (h "0daf5gilzq8lfv4nprlqmjxxfzr6rz76dsh9fh4sglss9yh003cs")))

