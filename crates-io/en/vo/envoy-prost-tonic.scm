(define-module (crates-io en vo envoy-prost-tonic) #:use-module (crates-io))

(define-public crate-envoy-prost-tonic-0.1.0 (c (n "envoy-prost-tonic") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "pbjson") (r "^0.5") (d #t) (k 0)) (d (n "pbjson-types") (r "^0.5") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tonic") (r "^0.8") (f (quote ("gzip"))) (d #t) (k 0)))) (h "0vb93dczcrrzrxw5i6b7i5x043lxf63m1yrp38y04yw2lf1piaky")))

