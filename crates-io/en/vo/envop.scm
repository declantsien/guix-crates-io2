(define-module (crates-io en vo envop) #:use-module (crates-io))

(define-public crate-envop-0.1.0 (c (n "envop") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.8") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "inquire") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1znsx0j9c94vpcdqxy25j0cjbzldd40g5kj6hn7nw22c4ghlahfz")))

(define-public crate-envop-1.0.0 (c (n "envop") (v "1.0.0") (d (list (d (n "argh") (r "^0.1.8") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "inquire") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "003plmhj9qa5mlc4w2vz06ffqqh4mql896vnajizh27qxfxcfvcp")))

(define-public crate-envop-1.0.4 (c (n "envop") (v "1.0.4") (d (list (d (n "argh") (r "^0.1.8") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "inquire") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0vwwxffzj81a3rzlgmj8ksl66pp99f3k4m6gcc3dpxxksra1bv8f")))

(define-public crate-envop-1.0.5 (c (n "envop") (v "1.0.5") (d (list (d (n "argh") (r "^0.1.8") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "inquire") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1vjxrfva3879rrlr1xfwyv95b4cs4kmlhcd96qzkc5nvr190bj3p")))

