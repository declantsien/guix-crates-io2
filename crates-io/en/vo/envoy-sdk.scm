(define-module (crates-io en vo envoy-sdk) #:use-module (crates-io))

(define-public crate-envoy-sdk-0.0.1 (c (n "envoy-sdk") (v "0.0.1") (d (list (d (n "proxy-wasm") (r "^0.1.0-alpha.2") (d #t) (k 0) (p "proxy-wasm-experimental")) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0npqgr1scfljl64gy1lgfjpc3g5ya1fmgqm60a84m5f1crv4gfj8")))

(define-public crate-envoy-sdk-0.0.2 (c (n "envoy-sdk") (v "0.0.2") (d (list (d (n "proxy-wasm") (r "^0.1.0-alpha.3") (d #t) (k 0) (p "proxy-wasm-experimental")) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1jw37mbjy2xq7nbj4gidap3xysf8bxc51pysvd1apl9m3lqqw8i8")))

(define-public crate-envoy-sdk-0.0.3 (c (n "envoy-sdk") (v "0.0.3") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "proxy-wasm") (r "^0.1.0-alpha.5") (d #t) (k 0) (p "proxy-wasm-experimental")) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0wbgrqkpdlc3kmbaj4mg1n0hdc8ww62sldvnnpcd8xa4z5qfmpd9") (f (quote (("default" "log")))) (y #t)))

(define-public crate-envoy-sdk-0.0.4 (c (n "envoy-sdk") (v "0.0.4") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "proxy-wasm") (r "^0.1.0-alpha.5") (d #t) (k 0) (p "proxy-wasm-experimental")) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "11lqy48lv3svih2zi2jh208b89va60dlgz0q5w1zr3ggxbyzvvp6") (f (quote (("default" "log")))) (y #t)))

(define-public crate-envoy-sdk-0.0.1-1 (c (n "envoy-sdk") (v "0.0.1-1") (d (list (d (n "proxy-wasm") (r "^0.0.2") (d #t) (k 0) (p "proxy-wasm-experimental")) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1hr3qzf9c5lb43ch16ya805iv6cy453zwayw9gdd7503da1hvvcw")))

(define-public crate-envoy-sdk-0.0.2-1 (c (n "envoy-sdk") (v "0.0.2-1") (d (list (d (n "proxy-wasm") (r "^0.0.3") (d #t) (k 0) (p "proxy-wasm-experimental")) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1wynzgllc8wn49np8n3xw7jqkqjbabx032d3li6mpv7ifqfppvbb")))

(define-public crate-envoy-sdk-0.0.5 (c (n "envoy-sdk") (v "0.0.5") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "proxy-wasm") (r "^0.0.5") (d #t) (k 0) (p "proxy-wasm-experimental")) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "05jqlagn8gqg13jvh07d6m9hbgagk34nq0b8d0p3295mngkzdqyr") (f (quote (("default" "log"))))))

(define-public crate-envoy-sdk-0.0.3-alpha1 (c (n "envoy-sdk") (v "0.0.3-alpha1") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "proxy-wasm") (r "^0.1.0-alpha.3") (d #t) (k 0) (p "proxy-wasm-experimental")) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "14h1gfnpybfl1yv4cgvb74w63x0hngw3qqmnzq200mvm3w68cp3a") (f (quote (("default" "log"))))))

(define-public crate-envoy-sdk-0.1.0 (c (n "envoy-sdk") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "proxy-wasm") (r "^0.0.7") (d #t) (k 0) (p "proxy-wasm-experimental")) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "103d7f3zk75g242lqv8iklkb3grc9ac30wwxy4r6xh249c7hxpc2") (f (quote (("default" "log"))))))

(define-public crate-envoy-sdk-0.2.0-alpha.1 (c (n "envoy-sdk") (v "0.2.0-alpha.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "proxy-wasm") (r "^0.0.8") (d #t) (k 0) (p "proxy-wasm-experimental")) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "02qa2i0phll4ci4fd769lih3adqk31ki4yf4j873vrcsrs0za4vv") (f (quote (("wee-alloc" "proxy-wasm/wee-alloc") ("default" "log"))))))

