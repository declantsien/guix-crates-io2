(define-module (crates-io en vo envoy-types) #:use-module (crates-io))

(define-public crate-envoy-types-0.1.0 (c (n "envoy-types") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 2)) (d (n "prost") (r "^0.11.9") (k 0)) (d (n "tonic") (r "^0.9.2") (f (quote ("codegen" "transport" "prost"))) (k 0)) (d (n "tonic-build") (r "^0.9.2") (f (quote ("prost" "cleanup-markdown"))) (k 2)))) (h "11ihgby6l24m2asjmijhpr06vag13rd5ny41p2ir34k2js4zjzkq")))

(define-public crate-envoy-types-0.1.1 (c (n "envoy-types") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 2)) (d (n "prost") (r "^0.11.9") (f (quote ("prost-derive"))) (k 0)) (d (n "tonic") (r "^0.9.2") (f (quote ("codegen" "transport" "prost"))) (k 0)) (d (n "tonic-build") (r "^0.9.2") (f (quote ("prost" "cleanup-markdown"))) (k 2)))) (h "0nazgab3bxrrlann8yf3a7pxhccam12xs82czdff5j014y69jflr")))

(define-public crate-envoy-types-0.2.0 (c (n "envoy-types") (v "0.2.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 2)) (d (n "prost") (r "^0.11.9") (f (quote ("prost-derive"))) (k 0)) (d (n "tonic") (r "^0.9.2") (f (quote ("codegen" "transport" "prost"))) (k 0)) (d (n "tonic-build") (r "^0.9.2") (f (quote ("prost" "cleanup-markdown"))) (k 2)))) (h "1614a302l0mffkv8q28dw8zrzhzxblwjbmibzij0d7qi2scz38kz")))

(define-public crate-envoy-types-0.3.0 (c (n "envoy-types") (v "0.3.0") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 2)) (d (n "prost") (r "^0.12.3") (f (quote ("prost-derive"))) (k 0)) (d (n "tonic") (r "^0.10.2") (f (quote ("codegen" "transport" "prost"))) (k 0)) (d (n "tonic-build") (r "^0.10.2") (f (quote ("prost" "cleanup-markdown"))) (k 2)))) (h "12faqjrg869vs15mj6gnjk1n7c5vk8zcyfgksimxx245n2yyyax4")))

(define-public crate-envoy-types-0.4.0 (c (n "envoy-types") (v "0.4.0") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "prost") (r "^0.12") (f (quote ("prost-derive"))) (k 0)) (d (n "tonic") (r "^0.11") (f (quote ("codegen" "transport" "prost"))) (k 0)) (d (n "tonic-build") (r "^0.11") (f (quote ("prost" "cleanup-markdown"))) (k 2)))) (h "05mfn7m1yf3b3c6xc5v5w3ljln9bgsi6hfl4s48xv2xyawfm3i0l")))

