(define-module (crates-io en vo envopts) #:use-module (crates-io))

(define-public crate-envopts-0.0.1 (c (n "envopts") (v "0.0.1") (h "0va33l6pbjli6pkr1b3z4z1rr8dy0lpghc6c3ys4sib3f8qz8mcr") (y #t)))

(define-public crate-envopts-0.1.0 (c (n "envopts") (v "0.1.0") (h "1n33a2b30w0d37lw4l1qmhknwapkzxh4qkpxvkfwyl3rp5v7a37g") (y #t)))

