(define-module (crates-io en vo envoy) #:use-module (crates-io))

(define-public crate-envoy-0.1.0 (c (n "envoy") (v "0.1.0") (h "0whkyf2ag54hjcxpgnf41ggz011h4y6vw0alj6kaba82mp66azvf")))

(define-public crate-envoy-0.1.1 (c (n "envoy") (v "0.1.1") (h "0kap3jijxl93l59ziiy24i2h5k7qj45dqnbq0158jwyzvxg7fix9")))

(define-public crate-envoy-0.1.2 (c (n "envoy") (v "0.1.2") (h "0vh6xnz8dw5lf02g02dxxgiwqhsk6r0g95p7cf44l3rc488xp2fi")))

(define-public crate-envoy-0.1.3 (c (n "envoy") (v "0.1.3") (h "0s57396r8hwl5clrxzj5fwngka5rkh7hcvzzgnmyfxx91hjbcd5v")))

