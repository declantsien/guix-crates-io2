(define-module (crates-io en vo envoy-ext-proc-stubs) #:use-module (crates-io))

(define-public crate-envoy-ext-proc-stubs-0.1.0 (c (n "envoy-ext-proc-stubs") (v "0.1.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "1y3qpby5avn020vy2qnic8y3rdpww4cibslg315h4d57h255k8s0")))

(define-public crate-envoy-ext-proc-stubs-0.1.1 (c (n "envoy-ext-proc-stubs") (v "0.1.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "tonic") (r "^0.8") (d #t) (k 0)) (d (n "tonic-build") (r "^0.8") (d #t) (k 1)))) (h "03j3w9qbpzgb3dz5jh3d1p15d7p2ff8k287hkx3dz8lw3a2rya35")))

