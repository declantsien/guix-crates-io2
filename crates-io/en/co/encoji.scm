(define-module (crates-io en co encoji) #:use-module (crates-io))

(define-public crate-encoji-0.1.0 (c (n "encoji") (v "0.1.0") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "serde") (r "^1.0") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0") (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "unicode-segmentation") (r "^1.1.0") (d #t) (k 0)))) (h "1rx5cjyn1585pyc60qaqzbq9g27dwzhg2dqaw7awmp5n14vs43wl")))

(define-public crate-encoji-0.1.1 (c (n "encoji") (v "0.1.1") (d (list (d (n "phf") (r "^0.7") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7") (d #t) (k 1)) (d (n "serde") (r "^1.0") (d #t) (k 1)) (d (n "serde_derive") (r "^1.0") (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "0wjdpa0a2f0j2x7qmm0yb0b6mf6wsrxnw9zsi01h0cadw19hi7j0")))

