(define-module (crates-io en co encoding-next-index-singlebyte) #:use-module (crates-io))

(define-public crate-encoding-next-index-singlebyte-1.20180106.0 (c (n "encoding-next-index-singlebyte") (v "1.20180106.0") (d (list (d (n "encoding-next_index_tests") (r "^0.1.5") (d #t) (k 2)))) (h "11csdp06sj6d6s25r6sbsckqjbja3gq8xmx7jksca70s4fn0nb70") (f (quote (("no-optimized-legacy-encoding"))))))

