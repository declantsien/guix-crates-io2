(define-module (crates-io en co encoding-index-tradchinese) #:use-module (crates-io))

(define-public crate-encoding-index-tradchinese-1.0.20140915 (c (n "encoding-index-tradchinese") (v "1.0.20140915") (d (list (d (n "encoding_index_tests") (r "^0.1.0") (d #t) (k 0)))) (h "12vf2bw4hjm1j6y1kiqplwfn3lqy0y5lb83smg1f3jyb2rk8rhkj")))

(define-public crate-encoding-index-tradchinese-1.0.20141106 (c (n "encoding-index-tradchinese") (v "1.0.20141106") (d (list (d (n "encoding_index_tests") (r "^0.1.1") (d #t) (k 0)))) (h "1nrbfbwb9740z4j4jb1x9d2vkrfn90qq37746mqgzgyiylv5n4a2")))

(define-public crate-encoding-index-tradchinese-1.0.20141219 (c (n "encoding-index-tradchinese") (v "1.0.20141219") (d (list (d (n "encoding_index_tests") (r "^0.1.3") (d #t) (k 0)))) (h "0nxgqs6xsgm2ismjmxh32h3pb8xbrklxwhiswbpl4mr4xpgh89ck")))

(define-public crate-encoding-index-tradchinese-1.20141219.0 (c (n "encoding-index-tradchinese") (v "1.20141219.0") (d (list (d (n "encoding_index_tests") (r "^0.1.3") (d #t) (k 0)))) (h "02hgkcsjjp224fkh7wgjfnp4vb02phdfxhr66nnwjgwlhdrrks79")))

(define-public crate-encoding-index-tradchinese-1.20141219.1 (c (n "encoding-index-tradchinese") (v "1.20141219.1") (d (list (d (n "encoding_index_tests") (r "^0.1.3") (d #t) (k 0)))) (h "1cgwxrcpwvyq3kl03rdp1jjmb2cvj6hp8dam69a7ckyza2y2p390")))

(define-public crate-encoding-index-tradchinese-1.20141219.2 (c (n "encoding-index-tradchinese") (v "1.20141219.2") (d (list (d (n "encoding_index_tests") (r "^0.1.3") (d #t) (k 0)))) (h "06wv581188ksgk1yylab4677jbrvy6ymcriyx6q5kqs2l7y76yjx")))

(define-public crate-encoding-index-tradchinese-1.20141219.3 (c (n "encoding-index-tradchinese") (v "1.20141219.3") (d (list (d (n "encoding_index_tests") (r "^0.1.3") (d #t) (k 0)))) (h "0swbm9rrx0akgask4804mfdqa4aywhwrp4a88k4w6nvk46pckkcr")))

(define-public crate-encoding-index-tradchinese-1.20141219.4 (c (n "encoding-index-tradchinese") (v "1.20141219.4") (d (list (d (n "encoding_index_tests") (r "^0.1.4") (d #t) (k 0)))) (h "1lr4qwzlg6c2crz5c2i0y6zngsr9i9jcgzlfzqi3gnlrz8cnkh37")))

(define-public crate-encoding-index-tradchinese-1.20141219.5 (c (n "encoding-index-tradchinese") (v "1.20141219.5") (d (list (d (n "encoding_index_tests") (r "^0.1.4") (d #t) (k 0)))) (h "060ci4iz6xfvzk38syfbjvs7pix5hch3mvxkksswmqwcd3aj03px")))

