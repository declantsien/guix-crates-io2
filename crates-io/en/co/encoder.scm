(define-module (crates-io en co encoder) #:use-module (crates-io))

(define-public crate-encoder-0.1.0 (c (n "encoder") (v "0.1.0") (d (list (d (n "encoder-itoa") (r "^1.0.10") (d #t) (k 0)) (d (n "encoder-ryu") (r "^1.0.16") (d #t) (k 0)))) (h "1cix2d5d48mv62170jyp0sf6p22fd2qz3alz65r6fvhvf0xq68as")))

(define-public crate-encoder-0.1.1 (c (n "encoder") (v "0.1.1") (d (list (d (n "encoder-ryu") (r "=1.0.16") (d #t) (k 0)) (d (n "simd-json") (r "=0.13.7") (d #t) (k 0)))) (h "100v7my59ch93jji62q58fk2wxfrkbrd2slyrinsrkayw2lcxjws")))

(define-public crate-encoder-0.1.2 (c (n "encoder") (v "0.1.2") (d (list (d (n "encoder-ryu") (r "=1.0.16") (d #t) (k 0)) (d (n "simd-json") (r "=0.13.7") (d #t) (k 0)))) (h "02w991p18f9i9pf425vlyihqy9mlfmgijayk89lvwich15z776i6")))

(define-public crate-encoder-0.2.0 (c (n "encoder") (v "0.2.0") (d (list (d (n "encoder-ryu") (r "=1.0.16") (d #t) (k 0)) (d (n "simd-json") (r "=0.13.7") (d #t) (k 0)))) (h "06nd5jvw8p3sckw78wcwwh9v9p7la08hpg1a3vw6pkprhqf0p65a")))

(define-public crate-encoder-0.2.1 (c (n "encoder") (v "0.2.1") (d (list (d (n "encoder-ryu") (r "=1.0.16") (d #t) (k 0)) (d (n "simd-json") (r "=0.13.7") (d #t) (k 0)))) (h "0xpph4n7469jvqv9i4yfni4agm7nc78s2rsgvxv555zr6771bbgl")))

(define-public crate-encoder-0.2.2 (c (n "encoder") (v "0.2.2") (d (list (d (n "criterion") (r "=0.5.1") (d #t) (k 2)) (d (n "encoder-ryu") (r "=1.0.16") (d #t) (k 0)) (d (n "indexmap") (r "=2.1.0") (d #t) (k 0)) (d (n "simd-json") (r "=0.13.7") (d #t) (k 0)))) (h "05lmbzk43i53l4iz14q1wqd2kdv2i6ll7nkhrpnvj2s2i4ljli06")))

(define-public crate-encoder-0.2.3 (c (n "encoder") (v "0.2.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "encoder-ryu") (r "^1.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.2") (d #t) (k 0)) (d (n "simd-json") (r "^0.13") (d #t) (k 0)))) (h "10f38c8i7bnm424gyry025kbz0zfa1djvaq8p4di27qy0ybnj71d")))

(define-public crate-encoder-0.2.4 (c (n "encoder") (v "0.2.4") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "encoder-ryu") (r "^1.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "simd-json") (r "^0.13") (d #t) (k 0)))) (h "11s98kv10mnz3m4vsay9azqg4pjmibzqs1pb8apcvvy6sn595xh3")))

