(define-module (crates-io en co encoding_rs_io) #:use-module (crates-io))

(define-public crate-encoding_rs_io-0.1.0 (c (n "encoding_rs_io") (v "0.1.0") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "12p4adin5bbawhdd3r7q7zsxm4hdfkgjn697jil6vjyzx6x0bkbv")))

(define-public crate-encoding_rs_io-0.1.1 (c (n "encoding_rs_io") (v "0.1.1") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "1i1217hvsv6ag28zqr730mn57cddv8gdds3hq0dyz5517dszw3xd")))

(define-public crate-encoding_rs_io-0.1.2 (c (n "encoding_rs_io") (v "0.1.2") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "1cy9yb5iy703rqj7pxjfk0al5mb1h3yx1mx2d4sjy5vf9mazy8pj")))

(define-public crate-encoding_rs_io-0.1.3 (c (n "encoding_rs_io") (v "0.1.3") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "1x8khwgklprl6m77znyrd9rm5qnpqs1dqi0knxba56rsnw56m3q9")))

(define-public crate-encoding_rs_io-0.1.4 (c (n "encoding_rs_io") (v "0.1.4") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "0d6hwjdsy6fzfc9gf04iknfyvpa0592hxrzx4bg7qg14b1hsb2bc")))

(define-public crate-encoding_rs_io-0.1.5 (c (n "encoding_rs_io") (v "0.1.5") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "1n58z9rd87dg94dvzl2knry4pl6g3sm5nmpghblmhp9gvfyg4kpr")))

(define-public crate-encoding_rs_io-0.1.6 (c (n "encoding_rs_io") (v "0.1.6") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "0b7k9p7inkrcanh7h6q4m278y05gmcwi8p5r43h7grzl5dxfw6cn")))

(define-public crate-encoding_rs_io-0.1.7 (c (n "encoding_rs_io") (v "0.1.7") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "10ra4l688cdadd8h1lsbahld1zbywnnqv68366mbhamn3xjwbhqw")))

