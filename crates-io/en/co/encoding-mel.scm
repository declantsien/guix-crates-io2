(define-module (crates-io en co encoding-mel) #:use-module (crates-io))

(define-public crate-encoding-mel-0.7.0-rc1 (c (n "encoding-mel") (v "0.7.0-rc1") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "melodium-core") (r "=0.7.0-rc1") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.7.0-rc1") (d #t) (k 0)))) (h "1jlcjc3y3pk0mjjz837gd33xkwqi95vzbpx9i9chc6iyza0z48dn") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-encoding-mel-0.7.0 (c (n "encoding-mel") (v "0.7.0") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "melodium-core") (r "=0.7.0") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.7.0") (d #t) (k 0)))) (h "033w77aidv4lnxig79svmgdd0pvb8bd1s2w3fvawgjzf9wcsfx3r") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-encoding-mel-0.7.1 (c (n "encoding-mel") (v "0.7.1") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "melodium-core") (r "^0.7.1") (d #t) (k 0)) (d (n "melodium-macro") (r "^0.7.1") (d #t) (k 0)))) (h "03ak10sp7xhlc5gbghjy6s4rqdr3d5fdf0izhan955sdjqa2wg09") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-encoding-mel-0.8.0-rc1 (c (n "encoding-mel") (v "0.8.0-rc1") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "melodium-core") (r "=0.8.0-rc1") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.8.0-rc1") (d #t) (k 0)))) (h "0xbn2s86a6lwh3gw0z9jgm3yp4m8lrn5j6lj0s0bnrqrs450505j") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-encoding-mel-0.8.0-rc2 (c (n "encoding-mel") (v "0.8.0-rc2") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "melodium-core") (r "=0.8.0-rc2") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.8.0-rc2") (d #t) (k 0)))) (h "0jskk156bpdgdn60czbjfpwc5rrym7w4sqj51b055ba74shy7l7z") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-encoding-mel-0.8.0-rc3 (c (n "encoding-mel") (v "0.8.0-rc3") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "melodium-core") (r "=0.8.0-rc3") (d #t) (k 0)) (d (n "melodium-macro") (r "=0.8.0-rc3") (d #t) (k 0)))) (h "0a343y02gix6smis7x2y7l5n3s8ivmnycph5hkz4fa2mk40v1d33") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

(define-public crate-encoding-mel-0.8.0 (c (n "encoding-mel") (v "0.8.0") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "melodium-core") (r "^0.8.0") (d #t) (k 0)) (d (n "melodium-macro") (r "^0.8.0") (d #t) (k 0)))) (h "0ygc6j8w3d78v3zqv7fm8qsld4884k0ykn3gra4j9wvnm1xab5ir") (f (quote (("real") ("plugin") ("mock")))) (r "1.60")))

