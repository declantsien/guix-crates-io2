(define-module (crates-io en co encodex) #:use-module (crates-io))

(define-public crate-encodex-0.0.1 (c (n "encodex") (v "0.0.1") (h "0x3c2rmkarpladdalvf45c12v2kfksjycnfs7c7hrm91m0rhk9v9") (f (quote (("dox")))) (y #t)))

(define-public crate-encodex-0.1.6 (c (n "encodex") (v "0.1.6") (h "0fxg70ja0vnhwi7qy67w9sxv4gz2v6qzcjj1lj1gg0qa3pdqm84v") (f (quote (("doc_tests")))) (y #t)))

(define-public crate-encodex-0.1.7 (c (n "encodex") (v "0.1.7") (h "1izh5hs75lrybkbq6fy9ywky5ja198z3mi7l4b9k8hknpbfv8dcd") (f (quote (("doc_tests")))) (y #t)))

(define-public crate-encodex-0.1.8 (c (n "encodex") (v "0.1.8") (h "153y22d45cxws7wjk1iqi745sbkmcda3v0hbkns9m9salwjx9c2d") (f (quote (("doc_tests"))))))

(define-public crate-encodex-0.1.9 (c (n "encodex") (v "0.1.9") (h "1g3mgq0ff633qrc8qs4l7j29kx9qhj02pnndg5yz9iwvnz9rg0gn") (f (quote (("sha2" "digest") ("doc_tests") ("digest") ("base_encodings") ("base64_url" "base_encodings") ("base64" "base_encodings") ("base32_hex" "base_encodings") ("base32" "base_encodings") ("base16" "base_encodings"))))))

(define-public crate-encodex-0.1.10 (c (n "encodex") (v "0.1.10") (h "0wbrv882hi32lmp4fcsjb4w4ak9j2bpn2zbx2wixzdly96paijq6") (f (quote (("ui") ("sha2" "digest") ("doc_tests") ("digest") ("code") ("base_encoding" "code") ("base64_url" "base_encoding") ("base64" "base_encoding") ("base32_hex" "base_encoding") ("base32" "base_encoding") ("base16" "base_encoding"))))))

(define-public crate-encodex-0.1.11 (c (n "encodex") (v "0.1.11") (h "1ps6cx74hrn112wxqgkyzyq954lys38rvj1v95r05ja7d0am707m") (f (quote (("ui") ("sha2" "digest") ("doc_tests") ("digest") ("code") ("base_encoding" "code") ("base64url" "base_encoding") ("base64" "base_encoding") ("base32hex" "base_encoding") ("base32" "base_encoding") ("base16" "base_encoding"))))))

(define-public crate-encodex-0.1.12 (c (n "encodex") (v "0.1.12") (h "1xcgnzfb0v0ca53v444h89dyky3bxvz2bqa4x5db5lmrj6gkh5dr") (f (quote (("ui") ("sha2" "digest") ("hex" "base_encoding") ("doc_tests") ("digest") ("code") ("base_encoding" "code") ("base64url" "base_encoding") ("base64" "base_encoding") ("base32hex" "base_encoding") ("base32" "base_encoding") ("base16" "base_encoding"))))))

