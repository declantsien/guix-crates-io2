(define-module (crates-io en co encoding-index-japanese) #:use-module (crates-io))

(define-public crate-encoding-index-japanese-1.0.20140915 (c (n "encoding-index-japanese") (v "1.0.20140915") (d (list (d (n "encoding_index_tests") (r "^0.1.0") (d #t) (k 0)))) (h "1klmxyplpj2gkh3yvwxllkvpbgjqsiihs5a5p2443sfmvbjhz2ln")))

(define-public crate-encoding-index-japanese-1.0.20141106 (c (n "encoding-index-japanese") (v "1.0.20141106") (d (list (d (n "encoding_index_tests") (r "^0.1.1") (d #t) (k 0)))) (h "15x2nq51x4s2fc59bv181lyhvsl4bfz53wrzzd35hi62q149r329")))

(define-public crate-encoding-index-japanese-1.0.20141219 (c (n "encoding-index-japanese") (v "1.0.20141219") (d (list (d (n "encoding_index_tests") (r "^0.1.2") (d #t) (k 0)))) (h "1s3fpcy0phinrjj26vb2qb35mmrdrmgccj58r7hyinbl00g1vzns")))

(define-public crate-encoding-index-japanese-1.0.20141220 (c (n "encoding-index-japanese") (v "1.0.20141220") (d (list (d (n "encoding_index_tests") (r "^0.1.3") (d #t) (k 0)))) (h "11xbbgwdjxhp1yxx3x7cl9hr53b72q0ddfzjkw71rgdv00a3xrry")))

(define-public crate-encoding-index-japanese-1.20141219.0 (c (n "encoding-index-japanese") (v "1.20141219.0") (d (list (d (n "encoding_index_tests") (r "^0.1.3") (d #t) (k 0)))) (h "1r2jqs5fv3mv96k0qspsxkpmi0sc6kssy59shak543i4y92cm6nx")))

(define-public crate-encoding-index-japanese-1.20141219.1 (c (n "encoding-index-japanese") (v "1.20141219.1") (d (list (d (n "encoding_index_tests") (r "^0.1.3") (d #t) (k 0)))) (h "1dx02np5ws5g5xm9yvpq9pzmyi999dgslxg0q2qnbx97zhqjzlda")))

(define-public crate-encoding-index-japanese-1.20141219.2 (c (n "encoding-index-japanese") (v "1.20141219.2") (d (list (d (n "encoding_index_tests") (r "^0.1.3") (d #t) (k 0)))) (h "0wb70da19lmyjgjssj6vjl0w659b7l8852zpnjwxb7qg42slylva")))

(define-public crate-encoding-index-japanese-1.20141219.3 (c (n "encoding-index-japanese") (v "1.20141219.3") (d (list (d (n "encoding_index_tests") (r "^0.1.3") (d #t) (k 0)))) (h "0grppmkbssrsav88i5wxxy43f8n1xqfnagc40hpbla393mdxrrc1")))

(define-public crate-encoding-index-japanese-1.20141219.4 (c (n "encoding-index-japanese") (v "1.20141219.4") (d (list (d (n "encoding_index_tests") (r "^0.1.4") (d #t) (k 0)))) (h "0qqa161m1q45w9rvpvfpqjsdp4q2la5272vv03rfgmqv0dh1ljgr")))

(define-public crate-encoding-index-japanese-1.20141219.5 (c (n "encoding-index-japanese") (v "1.20141219.5") (d (list (d (n "encoding_index_tests") (r "^0.1.4") (d #t) (k 0)))) (h "148c1lmd640p1d7fzk0nv7892mbyavvwddgqvcsm78798bzv5s04")))

