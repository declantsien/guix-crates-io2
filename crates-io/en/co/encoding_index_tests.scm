(define-module (crates-io en co encoding_index_tests) #:use-module (crates-io))

(define-public crate-encoding_index_tests-0.1.0 (c (n "encoding_index_tests") (v "0.1.0") (h "1jljkam0jz2jpcrgb1p91nyjb6hl197zcara6vjb8av631kpfcfg")))

(define-public crate-encoding_index_tests-0.1.1 (c (n "encoding_index_tests") (v "0.1.1") (h "08cs42jik01qzj07idi5yf91xfc9idn5f44n4696wv9h41yjm13v")))

(define-public crate-encoding_index_tests-0.1.2 (c (n "encoding_index_tests") (v "0.1.2") (h "0c01ydzz1kpz7m7w4j6gmm2l1kp527g20ipcm590ar1vfidy3qxz")))

(define-public crate-encoding_index_tests-0.1.3 (c (n "encoding_index_tests") (v "0.1.3") (h "1838ja34yfcx7yj9kgvl5dx6l7a245715dv7pvmpk8j4xh4r7nny")))

(define-public crate-encoding_index_tests-0.1.4 (c (n "encoding_index_tests") (v "0.1.4") (h "0s85y091gl17ixass49bzaivng7w8p82p6nyvz2r3my9w4mxhim2")))

