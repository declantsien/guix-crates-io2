(define-module (crates-io en co encoding-index-simpchinese) #:use-module (crates-io))

(define-public crate-encoding-index-simpchinese-1.0.20140915 (c (n "encoding-index-simpchinese") (v "1.0.20140915") (d (list (d (n "encoding_index_tests") (r "^0.1.0") (d #t) (k 0)))) (h "1d7yka34bb0zz5bbq6lfd945bnqgvixmlppzfxw98dj6kbxnx0jz")))

(define-public crate-encoding-index-simpchinese-1.0.20141106 (c (n "encoding-index-simpchinese") (v "1.0.20141106") (d (list (d (n "encoding_index_tests") (r "^0.1.1") (d #t) (k 0)))) (h "0s679r2mjq8r9mn08r2ljzffir9l6madqfj88hzpkbzb7szx6xry")))

(define-public crate-encoding-index-simpchinese-1.0.20141219 (c (n "encoding-index-simpchinese") (v "1.0.20141219") (d (list (d (n "encoding_index_tests") (r "^0.1.2") (d #t) (k 0)))) (h "12c5s7hpnqi94kblbvv2wqb95y1b7hgr3c2zmn1jmz6hvm43k8h5")))

(define-public crate-encoding-index-simpchinese-1.0.20141220 (c (n "encoding-index-simpchinese") (v "1.0.20141220") (d (list (d (n "encoding_index_tests") (r "^0.1.3") (d #t) (k 0)))) (h "19hfga4jks9r6jk3m69diwks96kgyfqc01jgsifm3l311719i19c")))

(define-public crate-encoding-index-simpchinese-1.20141219.0 (c (n "encoding-index-simpchinese") (v "1.20141219.0") (d (list (d (n "encoding_index_tests") (r "^0.1.3") (d #t) (k 0)))) (h "1r9xyl8f1qdby661j3rv6ph319d1af3a3mjz2jjhxnjvim0wnlvi")))

(define-public crate-encoding-index-simpchinese-1.20141219.1 (c (n "encoding-index-simpchinese") (v "1.20141219.1") (d (list (d (n "encoding_index_tests") (r "^0.1.3") (d #t) (k 0)))) (h "1idla000gspnh8sj5vdiwjiisxy3ff26917jx6flm63vxpnbp6pm")))

(define-public crate-encoding-index-simpchinese-1.20141219.2 (c (n "encoding-index-simpchinese") (v "1.20141219.2") (d (list (d (n "encoding_index_tests") (r "^0.1.3") (d #t) (k 0)))) (h "1jmjhbmlwll8f66n0krhf7k1s8scp64h9qh4ck4a19615knz8c39")))

(define-public crate-encoding-index-simpchinese-1.20141219.4 (c (n "encoding-index-simpchinese") (v "1.20141219.4") (d (list (d (n "encoding_index_tests") (r "^0.1.4") (d #t) (k 0)))) (h "06y005y3wvy6wq2jl8w6aripd3r896vb6740qdav0d382vnj39n8")))

(define-public crate-encoding-index-simpchinese-1.20141219.5 (c (n "encoding-index-simpchinese") (v "1.20141219.5") (d (list (d (n "encoding_index_tests") (r "^0.1.4") (d #t) (k 0)))) (h "1xria2i7mc5dqdrpqxasdbxv1qx46jjbm53if3y1i4cvj2a72ynq")))

