(define-module (crates-io en co encode_unicode) #:use-module (crates-io))

(define-public crate-encode_unicode-0.1.0 (c (n "encode_unicode") (v "0.1.0") (d (list (d (n "ascii") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "09wdn4v0l54y85bvvpjacs16bvy00cqwcxclq8qbycnr8sz09y3r")))

(define-public crate-encode_unicode-0.1.1 (c (n "encode_unicode") (v "0.1.1") (d (list (d (n "ascii") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "02b9p7hga4bmivc486yadqi8khasrpnrib5hqf6p11mlys3l15s5")))

(define-public crate-encode_unicode-0.1.2 (c (n "encode_unicode") (v "0.1.2") (d (list (d (n "ascii") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "1xbgpvvdk3x8whqy40ibpr7syzyrn5r4x0mpnn5ij8ancfvp120r")))

(define-public crate-encode_unicode-0.1.3 (c (n "encode_unicode") (v "0.1.3") (d (list (d (n "ascii") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "1km1gmmk0v7kf1wy52zb9mk4r8zd5fvr84l6j9kprvs1b0gmzmi8")))

(define-public crate-encode_unicode-0.2.0 (c (n "encode_unicode") (v "0.2.0") (d (list (d (n "ascii") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "1s7d115fn8nhrppf7gqsak01dv02va3qribq7iij4fivkcnkdapb") (f (quote (("no_std") ("ascii_no_std" "no_std" "ascii/no_std"))))))

(define-public crate-encode_unicode-0.3.0 (c (n "encode_unicode") (v "0.3.0") (d (list (d (n "ascii") (r "^0.8") (o #t) (k 0)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "1d90416rfm6r00hsr87v8cdk4cbyfgdgl3sha0wrq6r7dwbc29w9") (f (quote (("std") ("default" "std"))))))

(define-public crate-encode_unicode-0.3.1 (c (n "encode_unicode") (v "0.3.1") (d (list (d (n "ascii") (r "^0.8") (o #t) (k 0)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "0c4q52b9axs1a7zwdzbmdhz5clzr4w1lq4ic9w2wsb98s87fr260") (f (quote (("std") ("default" "std"))))))

(define-public crate-encode_unicode-0.3.2 (c (n "encode_unicode") (v "0.3.2") (d (list (d (n "ascii") (r "^0.8") (o #t) (k 0)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)))) (h "04m2cmf56dyi6drjjnz3higcdqnxcvbvc8xf0l6rk2yylrilknkx") (f (quote (("std") ("default" "std"))))))

(define-public crate-encode_unicode-0.3.3 (c (n "encode_unicode") (v "0.3.3") (d (list (d (n "ascii") (r "^0.8") (o #t) (k 0)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "1.0.*") (d #t) (t "cfg(unix)") (k 2)))) (h "1zi0ivjm2ywc1rzwfpkdjnzl1w7jmsnri6qjc4dzk7rzsdlax2g8") (f (quote (("std") ("default" "std"))))))

(define-public crate-encode_unicode-0.3.4 (c (n "encode_unicode") (v "0.3.4") (d (list (d (n "ascii") (r ">= 0.8, <= 0.10") (o #t) (k 0)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "1.0.*") (d #t) (t "cfg(unix)") (k 2)))) (h "1qbyx8ni0vlnjm4mqnw8k3w3xqlznfwj3b5lh9dg3im7sdl7524d") (f (quote (("std") ("default" "std"))))))

(define-public crate-encode_unicode-0.3.5 (c (n "encode_unicode") (v "0.3.5") (d (list (d (n "ascii") (r ">= 0.8, <= 0.10") (o #t) (k 0)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "1.0.*") (d #t) (t "cfg(unix)") (k 2)))) (h "1g8a8pixkxz6r927f4sc4r15qyc0szxdxb1732v8q7h0di4wkclh") (f (quote (("std") ("default" "std"))))))

(define-public crate-encode_unicode-0.3.6 (c (n "encode_unicode") (v "0.3.6") (d (list (d (n "ascii") (r ">= 0.8, < 2") (o #t) (k 0)) (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "1.0.*") (d #t) (t "cfg(unix)") (k 2)))) (h "07w3vzrhxh9lpjgsg2y5bwzfar2aq35mdznvcp3zjl0ssj7d4mx3") (f (quote (("std") ("default" "std"))))))

(define-public crate-encode_unicode-1.0.0 (c (n "encode_unicode") (v "1.0.0") (d (list (d (n "ascii") (r "^1.0.0") (o #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (t "cfg(unix)") (k 2)) (d (n "minreq") (r "^2.6") (f (quote ("https-native"))) (d #t) (k 2)))) (h "1h5j7j7byi289by63s3w4a8b3g6l5ccdrws7a67nn07vdxj77ail") (f (quote (("std") ("default" "std"))))))

