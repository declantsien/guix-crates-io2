(define-module (crates-io en co encoding-index-singlebyte) #:use-module (crates-io))

(define-public crate-encoding-index-singlebyte-1.0.20140915 (c (n "encoding-index-singlebyte") (v "1.0.20140915") (d (list (d (n "encoding_index_tests") (r "^0.1.0") (d #t) (k 0)))) (h "0rhh2df5irp6kqrvn9hy6kgi3wqpxanhngg7sxmihkwl67ab0hcf")))

(define-public crate-encoding-index-singlebyte-1.0.20141106 (c (n "encoding-index-singlebyte") (v "1.0.20141106") (d (list (d (n "encoding_index_tests") (r "^0.1.1") (d #t) (k 0)))) (h "079xf7vh4flj34cy9p1wapjlbjy8hakr6gsvqb4qf2x6qzb1vpbi")))

(define-public crate-encoding-index-singlebyte-1.0.20141219 (c (n "encoding-index-singlebyte") (v "1.0.20141219") (d (list (d (n "encoding_index_tests") (r "^0.1.3") (d #t) (k 0)))) (h "0xim2d942k6sna74kx3j888nxksdsi4p5nrah6jnxfcvqyhrd2b3")))

(define-public crate-encoding-index-singlebyte-1.20141219.0 (c (n "encoding-index-singlebyte") (v "1.20141219.0") (d (list (d (n "encoding_index_tests") (r "^0.1.3") (d #t) (k 0)))) (h "12hni7nqxfhlajrjqzah25bbc8gys9pbhbz2h44vkainwq1kr5ky")))

(define-public crate-encoding-index-singlebyte-1.20141219.1 (c (n "encoding-index-singlebyte") (v "1.20141219.1") (d (list (d (n "encoding_index_tests") (r "^0.1.3") (d #t) (k 0)))) (h "1lqijfa606a57zx3f92kkf89y4g7p1pvyfspdq202ad0b47nji01")))

(define-public crate-encoding-index-singlebyte-1.20141219.2 (c (n "encoding-index-singlebyte") (v "1.20141219.2") (d (list (d (n "encoding_index_tests") (r "^0.1.3") (d #t) (k 0)))) (h "0mshmyv03lfkzaf4xpxhbiky2p2ls98rl1dw7nv83qzfzawfn903")))

(define-public crate-encoding-index-singlebyte-1.20141219.4 (c (n "encoding-index-singlebyte") (v "1.20141219.4") (d (list (d (n "encoding_index_tests") (r "^0.1.4") (d #t) (k 0)))) (h "0qawz6h7c14mhcz6hmjgvaivxji691jz2s1xnlppwabhr70dd01d")))

(define-public crate-encoding-index-singlebyte-1.20141219.5 (c (n "encoding-index-singlebyte") (v "1.20141219.5") (d (list (d (n "encoding_index_tests") (r "^0.1.4") (d #t) (k 0)))) (h "0jp85bz2pprzvg9m95w4q0vibh67b6w3bx35lafay95jzyndal9k")))

