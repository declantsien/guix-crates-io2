(define-module (crates-io en co encodings) #:use-module (crates-io))

(define-public crate-encodings-0.0.0 (c (n "encodings") (v "0.0.0") (h "0q4lzajqpc8f2ikwzwb268vra7lp549hhdj1ghlzl09q7rz1hgb4")))

(define-public crate-encodings-0.0.1 (c (n "encodings") (v "0.0.1") (h "1si6apk3fyg4vi4zd3paw8r9ibxhyvhxndxdd3aqc55xh6xbk2nk")))

(define-public crate-encodings-0.1.0 (c (n "encodings") (v "0.1.0") (h "15pv9vjbxwvjz8qmmilkzvp9pc7s4dckhjijmfk960s7xppd63hw")))

