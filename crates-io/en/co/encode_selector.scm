(define-module (crates-io en co encode_selector) #:use-module (crates-io))

(define-public crate-encode_selector-0.3.3 (c (n "encode_selector") (v "0.3.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "onig") (r "^6.3.1") (d #t) (k 0)))) (h "08578q5cdcpi43a3dsnpdr82r6p1bgdxnwzv0xvd3637ffrni1ax")))

(define-public crate-encode_selector-0.4.0 (c (n "encode_selector") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "onig") (r "^6.3.1") (d #t) (k 0)))) (h "0pv89fr22fpkjjhysxf74808siinificqgqnmhbv9k6igmfla3z1")))

(define-public crate-encode_selector-0.4.1 (c (n "encode_selector") (v "0.4.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "onig") (r "^6.3.1") (d #t) (k 0)))) (h "1dqmghdnssvxyyky0z9gdnd8gj0i98ywljjqnymh066rbkf6ixsj")))

(define-public crate-encode_selector-0.4.2 (c (n "encode_selector") (v "0.4.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "onig") (r "^6.4.0") (d #t) (k 0)))) (h "1gwwmgxxi5cv4y55zhdldb0mlzxl1ki89q436pxn7ydnakvjqr69")))

