(define-module (crates-io en co encodingbufreader) #:use-module (crates-io))

(define-public crate-encodingbufreader-0.1.0 (c (n "encodingbufreader") (v "0.1.0") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)))) (h "03f1gi39z0y276d2kbcyn8qwha5r73hni5g1ywvin9di093rgdmr") (y #t)))

(define-public crate-encodingbufreader-0.1.1 (c (n "encodingbufreader") (v "0.1.1") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)))) (h "0ldk733iqk3hxcik8liwr88s0lz6i7389wxlspz0lahxs0bs7my1")))

