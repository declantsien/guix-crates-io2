(define-module (crates-io en co encoding-index-korean) #:use-module (crates-io))

(define-public crate-encoding-index-korean-1.0.20140915 (c (n "encoding-index-korean") (v "1.0.20140915") (d (list (d (n "encoding_index_tests") (r "^0.1.0") (d #t) (k 0)))) (h "09isx8jarhj17dhc033pyk6gp6yswpa69qipgiygzbyd06hr3wcp")))

(define-public crate-encoding-index-korean-1.1.20141106 (c (n "encoding-index-korean") (v "1.1.20141106") (d (list (d (n "encoding_index_tests") (r "^0.1.1") (d #t) (k 0)))) (h "1s7mp1nyqrljqibfy47fmwgyr3imscgfsdg6syah28fbcqckwhic")))

(define-public crate-encoding-index-korean-1.2.20141219 (c (n "encoding-index-korean") (v "1.2.20141219") (d (list (d (n "encoding_index_tests") (r "^0.1.1") (d #t) (k 0)))) (h "0xa6vfbfbz8yzhl6ggk3lpqhwf82yi0zi0jswk59i8y81akdvxc5")))

(define-public crate-encoding-index-korean-1.2.20141220 (c (n "encoding-index-korean") (v "1.2.20141220") (d (list (d (n "encoding_index_tests") (r "^0.1.3") (d #t) (k 0)))) (h "12lbam32w1asvss19dpi68i4yxdjdrwiik85g9i19wk54rcwfd7i")))

(define-public crate-encoding-index-korean-1.20141219.0 (c (n "encoding-index-korean") (v "1.20141219.0") (d (list (d (n "encoding_index_tests") (r "^0.1.3") (d #t) (k 0)))) (h "1ddggj3na9jc4a6qv5lrx84cl62x7p411x0klinplm7g310lc32d")))

(define-public crate-encoding-index-korean-1.20141219.1 (c (n "encoding-index-korean") (v "1.20141219.1") (d (list (d (n "encoding_index_tests") (r "^0.1.3") (d #t) (k 0)))) (h "17rm1jmdzp66li0hd8ichhbpjqmd30zrnd2nw48jsbgq7nbqw61q")))

(define-public crate-encoding-index-korean-1.20141219.2 (c (n "encoding-index-korean") (v "1.20141219.2") (d (list (d (n "encoding_index_tests") (r "^0.1.3") (d #t) (k 0)))) (h "0abgari0a3crimwfnq20klkbg56bfd233d4dzn35vhsnv4lyhxa4")))

(define-public crate-encoding-index-korean-1.20141219.4 (c (n "encoding-index-korean") (v "1.20141219.4") (d (list (d (n "encoding_index_tests") (r "^0.1.4") (d #t) (k 0)))) (h "1izbi5sqmijnbpcha3c3rvasyvjs2z60pa8cx14kxclwmaabymyy")))

(define-public crate-encoding-index-korean-1.20141219.5 (c (n "encoding-index-korean") (v "1.20141219.5") (d (list (d (n "encoding_index_tests") (r "^0.1.4") (d #t) (k 0)))) (h "10cxabp5ppygbq4y6y680856zl9zjvq7ahpiw8zj3fmwwsw3zhsd")))

