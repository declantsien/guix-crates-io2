(define-module (crates-io en co encon) #:use-module (crates-io))

(define-public crate-encon-0.0.1 (c (n "encon") (v "0.0.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.6") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "18xrgz923xzacbibk9q08yxmbjvjmpy19snv18g6r8y8i7xbcfb6")))

(define-public crate-encon-0.0.2 (c (n "encon") (v "0.0.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.6") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "0p1whvs1hfw1w6j7bkl5l86w2zbca7mkqvqsl1s46waliqgzfckz")))

(define-public crate-encon-0.0.3 (c (n "encon") (v "0.0.3") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.6") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "0d70mwhrfx17ml69dks709z3fayrri94pimpfdy2qxdrw8aanbx1")))

