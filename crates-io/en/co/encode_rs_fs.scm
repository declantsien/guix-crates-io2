(define-module (crates-io en co encode_rs_fs) #:use-module (crates-io))

(define-public crate-encode_rs_fs-0.1.0 (c (n "encode_rs_fs") (v "0.1.0") (d (list (d (n "encoding_rs") (r "^0.8.30") (d #t) (k 0)))) (h "0s346qdx4ykrvmwban6krs9gjhhbb63w71z32wvd2b8x8vc11gdz")))

(define-public crate-encode_rs_fs-0.1.1 (c (n "encode_rs_fs") (v "0.1.1") (d (list (d (n "encoding_rs") (r "^0.8.30") (d #t) (k 0)))) (h "1jvfs4ax44br0vdl1s4b2fwnj3jwwq9y997hfml34adxl0v2m5cp")))

(define-public crate-encode_rs_fs-0.1.3 (c (n "encode_rs_fs") (v "0.1.3") (d (list (d (n "encoding_rs") (r "^0.8.30") (d #t) (k 0)))) (h "00ba4cybq48hx2kmp3dacwjxwbwiwnpklpnfd02mnwc1x4f4hnhf")))

