(define-module (crates-io en co encoding-asn1) #:use-module (crates-io))

(define-public crate-encoding-asn1-0.1.0 (c (n "encoding-asn1") (v "0.1.0") (d (list (d (n "encoding-asn1-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17zd6214pbxgjhfhrshv2gy9w9p23lgkmyawvnz50xg0m25x8zvj")))

