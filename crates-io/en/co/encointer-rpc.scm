(define-module (crates-io en co encointer-rpc) #:use-module (crates-io))

(define-public crate-encointer-rpc-0.1.0 (c (n "encointer-rpc") (v "0.1.0") (d (list (d (n "jsonrpsee") (r "^0.16.2") (d #t) (k 0)) (d (n "jsonrpsee-core") (r "^0.16.2") (d #t) (k 0)) (d (n "jsonrpsee-types") (r "^0.16.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "00dwjv4bmkl0rjdwpspaa5j3a8apmrb8a2y5fsydpffrragykkc3")))

(define-public crate-encointer-rpc-2.0.0 (c (n "encointer-rpc") (v "2.0.0") (d (list (d (n "jsonrpsee") (r "^0.16.3") (d #t) (k 0)) (d (n "jsonrpsee-core") (r "^0.16.3") (d #t) (k 0)) (d (n "jsonrpsee-types") (r "^0.16.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1r0n7dx1z7baqz14yyd46qqpgadc1z94g6ymc97y553smklwh93k")))

(define-public crate-encointer-rpc-3.0.0 (c (n "encointer-rpc") (v "3.0.0") (d (list (d (n "jsonrpsee") (r "^0.16.3") (d #t) (k 0)) (d (n "jsonrpsee-core") (r "^0.16.3") (d #t) (k 0)) (d (n "jsonrpsee-types") (r "^0.16.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1nf8lrimnl2hf7m6vdykjkvbhx2sl46247wrxfpx03pm7nhh9a05")))

(define-public crate-encointer-rpc-3.0.1 (c (n "encointer-rpc") (v "3.0.1") (d (list (d (n "jsonrpsee") (r "^0.16.3") (d #t) (k 0)) (d (n "jsonrpsee-core") (r "^0.16.3") (d #t) (k 0)) (d (n "jsonrpsee-types") (r "^0.16.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0p4fg6fim585fzviy91naw9y4ihxz675ck3xqpzimfix26iy0166")))

(define-public crate-encointer-rpc-3.0.2 (c (n "encointer-rpc") (v "3.0.2") (d (list (d (n "jsonrpsee") (r "^0.16.3") (d #t) (k 0)) (d (n "jsonrpsee-core") (r "^0.16.3") (d #t) (k 0)) (d (n "jsonrpsee-types") (r "^0.16.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1cmvirsdmci9n20pmjvjafvj732831spg1b7djl1jkdb3wqj56zv")))

(define-public crate-encointer-rpc-5.0.0 (c (n "encointer-rpc") (v "5.0.0") (d (list (d (n "jsonrpsee") (r "^0.16.3") (f (quote ("client-core" "server" "macros"))) (d #t) (k 0)) (d (n "jsonrpsee-core") (r "^0.16.3") (d #t) (k 0)) (d (n "jsonrpsee-types") (r "^0.16.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0aqar05agr3hcvl4m7cwcxxg4w9rkhga0c9h9cc2bj24xyx0sg3w")))

(define-public crate-encointer-rpc-6.0.0 (c (n "encointer-rpc") (v "6.0.0") (d (list (d (n "jsonrpsee") (r "^0.20.3") (f (quote ("client-core" "server" "macros"))) (d #t) (k 0)) (d (n "jsonrpsee-core") (r "^0.20.3") (d #t) (k 0)) (d (n "jsonrpsee-types") (r "^0.20.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0pfw74jkixap7s4pybiflx52c1sdsj3xbbpc63b8k47bms6fa9zm")))

(define-public crate-encointer-rpc-7.0.0 (c (n "encointer-rpc") (v "7.0.0") (d (list (d (n "jsonrpsee") (r "^0.22.0") (f (quote ("client-core" "server" "macros"))) (d #t) (k 0)) (d (n "jsonrpsee-core") (r "^0.22.0") (d #t) (k 0)) (d (n "jsonrpsee-types") (r "^0.22.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0v3ni8s2d88j2xpdfnlb4gncmc95z7bnq1jsi4zbhz0g229d6cjc")))

(define-public crate-encointer-rpc-6.1.0 (c (n "encointer-rpc") (v "6.1.0") (d (list (d (n "jsonrpsee") (r "^0.20.3") (f (quote ("client-core" "server" "macros"))) (d #t) (k 0)) (d (n "jsonrpsee-core") (r "^0.20.3") (d #t) (k 0)) (d (n "jsonrpsee-types") (r "^0.20.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1q4if7c3y58vd49ax7nmwjzhi40c002c8k3jflp032fgmmal5pfj")))

(define-public crate-encointer-rpc-10.1.0 (c (n "encointer-rpc") (v "10.1.0") (d (list (d (n "jsonrpsee") (r "^0.22.0") (f (quote ("client-core" "server" "macros"))) (d #t) (k 0)) (d (n "jsonrpsee-core") (r "^0.22.0") (d #t) (k 0)) (d (n "jsonrpsee-types") (r "^0.22.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1wkwifashvfil6jzdgkm3viipih9yzk9dc7x7yja2zsmqxgr7a5d")))

