(define-module (crates-io en co encoding_literals) #:use-module (crates-io))

(define-public crate-encoding_literals-0.1.0 (c (n "encoding_literals") (v "0.1.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)))) (h "0020f56z4qr9gknf4wpdnimf44ybp738yl6p7wksmxb0q0b45mk2")))

(define-public crate-encoding_literals-0.1.1 (c (n "encoding_literals") (v "0.1.1") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)))) (h "0j76slxiqks8hfib3qckcbzal5m7a2dgr89bbcmscn535y6gamhw")))

(define-public crate-encoding_literals-0.1.2 (c (n "encoding_literals") (v "0.1.2") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0)))) (h "1gmwsz7fbv487aqwjpcm5m48rlad5q6n45h2n9azn3w7ckgcywh6")))

