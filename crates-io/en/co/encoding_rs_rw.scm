(define-module (crates-io en co encoding_rs_rw) #:use-module (crates-io))

(define-public crate-encoding_rs_rw-0.1.0 (c (n "encoding_rs_rw") (v "0.1.0") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "102xjpd8738drb1167wrs11n7ff2vw6a3fgm1v0gy0qg771wlsay") (y #t)))

(define-public crate-encoding_rs_rw-0.1.1 (c (n "encoding_rs_rw") (v "0.1.1") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "1xqqaf4zqahy50kqdrr9dlh89wlw9bd6qi2ghnsp1z3l85hlq9nr") (y #t)))

(define-public crate-encoding_rs_rw-0.1.2 (c (n "encoding_rs_rw") (v "0.1.2") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "0dfpak1w2qvb6djvjqnksjvhh5zbngwr5vh8p5lr2g3p18sj60qs") (f (quote (("unstable-handler")))) (y #t)))

(define-public crate-encoding_rs_rw-0.2.0 (c (n "encoding_rs_rw") (v "0.2.0") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "0cvygn76ivz9ffnbnbjnbbw6vjinimkmx6i5vfx8sspdvvpi8wps") (f (quote (("unstable-handler")))) (y #t)))

(define-public crate-encoding_rs_rw-0.2.1 (c (n "encoding_rs_rw") (v "0.2.1") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "0bgd41sbgcs6w4hilk9cjb3p5xlb626j3s8igk86jc33h7fd96yx") (f (quote (("unstable-handler")))) (y #t)))

(define-public crate-encoding_rs_rw-0.3.0-beta.0 (c (n "encoding_rs_rw") (v "0.3.0-beta.0") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "15042m4afnjiv87289viif1776hcznjlwkz5sf9v35qx46nnn1lh")))

(define-public crate-encoding_rs_rw-0.3.1 (c (n "encoding_rs_rw") (v "0.3.1") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "04ldiljqzjb787n0ab6dgbzcz6phpn80y9nmilzkl59l7qdlwybn")))

(define-public crate-encoding_rs_rw-0.3.2 (c (n "encoding_rs_rw") (v "0.3.2") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "1kc8yyrc32s3r9nn2s7ys7v019fciwii52hzzxg83wcahyfw2avs")))

(define-public crate-encoding_rs_rw-0.4.0 (c (n "encoding_rs_rw") (v "0.4.0") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "1n4pjmp49m2404gyvrq83r37sr4nikdr6s7bl5qc6pv4vwk8c1fi")))

(define-public crate-encoding_rs_rw-0.4.1 (c (n "encoding_rs_rw") (v "0.4.1") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "0xx1z57g5r00n25ka5h2rhfdkh45nqnxlfgapmm8arn74lnpaqg7")))

(define-public crate-encoding_rs_rw-0.4.2 (c (n "encoding_rs_rw") (v "0.4.2") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "03wkvyziyjk7j67560mi3mq7qky5si1q68scbm2dxzyfkpllwzsr")))

