(define-module (crates-io en co encoder-ryu) #:use-module (crates-io))

(define-public crate-encoder-ryu-1.0.16 (c (n "encoder-ryu") (v "1.0.16") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)))) (h "1vycx08xpqdxj9fh352ifvwifdz9kysj3zjbc22krxzm77fas9sy") (f (quote (("small")))) (r "1.36")))

