(define-module (crates-io en co encoded) #:use-module (crates-io))

(define-public crate-encoded-0.4.0 (c (n "encoded") (v "0.4.0") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "litrs") (r "^0.2.3") (d #t) (k 0)))) (h "03fvymlzz5ff4zbn0qql9clflgpg74c7nm9z24pj7ni6j6d0mby3")))

(define-public crate-encoded-0.4.1 (c (n "encoded") (v "0.4.1") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "litrs") (r "^0.2.3") (d #t) (k 0)))) (h "0xigy3829pcfrlpcd3iln49kl0dbir4mpqjd7gn712fwb2hvc01f")))

(define-public crate-encoded-0.4.2 (c (n "encoded") (v "0.4.2") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 0)) (d (n "litrs") (r "^0.2.3") (d #t) (k 0)))) (h "0m0k5w2j45yxcvkvahr2ajr6p0gyvr53pjrq0miqp0b5mdz8pwkg")))

