(define-module (crates-io en co enco) #:use-module (crates-io))

(define-public crate-enco-0.1.0 (c (n "enco") (v "0.1.0") (h "1r5gbzgjbd4510bas2s9knqxccmll8al5pvrlk0nz7lzdnbc1nk0")))

(define-public crate-enco-0.1.1 (c (n "enco") (v "0.1.1") (h "0na2ws7lmvplg2620nykk26f62kj1xn8sfgsrb2xgga8yvnm7w2n")))

(define-public crate-enco-0.1.2 (c (n "enco") (v "0.1.2") (h "1ildvmyjs1d94v3jwqv7w4fr8blh7qyllbgbqf2ix3n4rhalxmq8")))

(define-public crate-enco-0.1.3 (c (n "enco") (v "0.1.3") (h "0dcivd3k39r2a4x63g7nw8zydc48mnmfbc8x3qd131jlrw8xk38j")))

(define-public crate-enco-0.1.4 (c (n "enco") (v "0.1.4") (h "0g8x5sa3v73pqcn8na9y748sah0nj4l98kgbl4k0yigbsp2z1zjm")))

