(define-module (crates-io en co encoding8) #:use-module (crates-io))

(define-public crate-encoding8-0.1.0 (c (n "encoding8") (v "0.1.0") (h "0ydxrn9szd4y9chyf9mxj0n3i23ap17fqn5l8nbj5qpgmgzigp31")))

(define-public crate-encoding8-0.2.0 (c (n "encoding8") (v "0.2.0") (h "1i42pf88qdhz2fhmrivk1zijzy5pr6mclfpf2qzvqlknf9zicvaz")))

(define-public crate-encoding8-0.3.0 (c (n "encoding8") (v "0.3.0") (h "09hk83pqg7cci82pl77nn5lv7iw6iasdlnfski0nbq4vhw85qm08")))

(define-public crate-encoding8-0.3.1 (c (n "encoding8") (v "0.3.1") (h "0zvyl8amwqngdnv5k86imcyn0yms1q1y32h7xy6jp1kd0hrd0w6m")))

(define-public crate-encoding8-0.3.2 (c (n "encoding8") (v "0.3.2") (h "0imjaxp029xsh9rv0mwsn9rw3jjcz0paqp2mf9y4inmfqxay8i1i")))

