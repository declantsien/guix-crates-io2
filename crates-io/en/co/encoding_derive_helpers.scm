(define-module (crates-io en co encoding_derive_helpers) #:use-module (crates-io))

(define-public crate-encoding_derive_helpers-1.7.0 (c (n "encoding_derive_helpers") (v "1.7.0") (d (list (d (n "amplify") (r "^3.7.1") (f (quote ("proc_attr"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1cgcf5dim5lgh426aqsikhbk15wxabyqjwar0rvx1hcvyfw3a54i")))

(define-public crate-encoding_derive_helpers-1.7.1 (c (n "encoding_derive_helpers") (v "1.7.1") (d (list (d (n "amplify") (r "^3.7.1") (f (quote ("proc_attr"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0vykjzp87kcaxs1i23b4rkb73089lz51bdjp2firip1w6q6h8d4h")))

(define-public crate-encoding_derive_helpers-1.7.2 (c (n "encoding_derive_helpers") (v "1.7.2") (d (list (d (n "amplify") (r "^3.7.1") (f (quote ("proc_attr"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0v36n37spgh3nn825prmvrfh86vw3y6gdxg5yrvdkvk0xb92yjhb")))

(define-public crate-encoding_derive_helpers-1.7.4 (c (n "encoding_derive_helpers") (v "1.7.4") (d (list (d (n "amplify") (r "^3.7.1") (f (quote ("proc_attr"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1fxyfh8mi5lwrb6kk61jvbmdy7jaihj1xk3fbaa2d4dc2q4b5yca")))

(define-public crate-encoding_derive_helpers-1.7.5 (c (n "encoding_derive_helpers") (v "1.7.5") (d (list (d (n "amplify") (r "^3.7.1") (f (quote ("proc_attr"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0n5vfqyvbyli3hk3bv5sgwakbbz1x87d5mypzb8m86ghl88wb1fz")))

(define-public crate-encoding_derive_helpers-1.7.6-beta.1 (c (n "encoding_derive_helpers") (v "1.7.6-beta.1") (d (list (d (n "amplify") (r "^3.7.1") (f (quote ("proc_attr"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1w4zhlpp61hirnmcjl604xks2039z3njhq30njsjfv7p7j065l53")))

(define-public crate-encoding_derive_helpers-1.7.6 (c (n "encoding_derive_helpers") (v "1.7.6") (d (list (d (n "amplify") (r "^3.7.1") (f (quote ("proc_attr"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0mwhgrhdccad1jxisxrvssabhixgybjvcbcj2j3mrf37h2lahjyw")))

(define-public crate-encoding_derive_helpers-0.8.0 (c (n "encoding_derive_helpers") (v "0.8.0") (d (list (d (n "amplify") (r "^3.7.1") (f (quote ("proc_attr"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1msi5n8n1qpnjag0l7zszvbrg8yhq85izrdrzqbimdlbhna180yi") (r "1.59.0")))

(define-public crate-encoding_derive_helpers-0.8.1 (c (n "encoding_derive_helpers") (v "0.8.1") (d (list (d (n "amplify") (r "^3.7.1") (f (quote ("proc_attr"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1d3a4zyxp9caxsb0c8yw0iak95b2q7ix5yjknxrlnx2lk8yjk8nq") (r "1.59.0")))

(define-public crate-encoding_derive_helpers-2.0.0-alpha.1 (c (n "encoding_derive_helpers") (v "2.0.0-alpha.1") (d (list (d (n "amplify") (r "^3.13.0") (f (quote ("proc_attr"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1lik5372d3y8jff9brrrqqkm2fib2z621zsz6ihsslgig6alg2zq") (r "1.59.0")))

(define-public crate-encoding_derive_helpers-2.0.0-alpha.2 (c (n "encoding_derive_helpers") (v "2.0.0-alpha.2") (d (list (d (n "amplify") (r "^3.13.0") (f (quote ("proc_attr"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1fh8j6y0l713wf08bi5bj0wj4madgqd4b13zn6c3hzxwwjfvcz9x") (r "1.59.0")))

(define-public crate-encoding_derive_helpers-0.9.0 (c (n "encoding_derive_helpers") (v "0.9.0") (d (list (d (n "amplify") (r "^3.13.0") (f (quote ("proc_attr"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1jdq4b76xidczhdvpd94dcyq99b9jxs438srxsq43xn7xip2pm0c") (r "1.59.0")))

