(define-module (crates-io en co encode-webm-video-frames) #:use-module (crates-io))

(define-public crate-encode-webm-video-frames-0.4.4 (c (n "encode-webm-video-frames") (v "0.4.4") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "imgref") (r "^1.3.4") (d #t) (k 0)) (d (n "libvpx-native-sys") (r "^5") (d #t) (k 0)) (d (n "rgb") (r "^0.8") (d #t) (k 0)) (d (n "webm") (r "^1.0.0") (d #t) (k 0)))) (h "0gf3vn56ab0bwji83qcqb4fpd94n3vm6y07rgp8bxwgqn6if6mai")))

