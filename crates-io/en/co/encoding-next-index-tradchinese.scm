(define-module (crates-io en co encoding-next-index-tradchinese) #:use-module (crates-io))

(define-public crate-encoding-next-index-tradchinese-1.20180106.0 (c (n "encoding-next-index-tradchinese") (v "1.20180106.0") (d (list (d (n "encoding-next_index_tests") (r "^0.1.5") (d #t) (k 2)))) (h "1afbpp1j6031myk2zj94g08jgyg3dmlxnkm2nk255fjwbq6pxd8c") (f (quote (("no-optimized-legacy-encoding")))) (y #t)))

(define-public crate-encoding-next-index-tradchinese-1.20180106.1 (c (n "encoding-next-index-tradchinese") (v "1.20180106.1") (d (list (d (n "encoding-next_index_tests") (r "^0.1.5") (d #t) (k 2)))) (h "0p7ccmgnv2vnh55kid6c42zhwrn8hp98yyq3jlalz18z7ds3l5gf") (f (quote (("no-optimized-legacy-encoding"))))))

