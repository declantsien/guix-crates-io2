(define-module (crates-io en co encoded-words) #:use-module (crates-io))

(define-public crate-encoded-words-0.1.0 (c (n "encoded-words") (v "0.1.0") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.2") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.20") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "08qnw40prymy6niy4sm4lv4byjgb114dpcwpaa3vlq62dnmw9y2f")))

(define-public crate-encoded-words-0.2.0 (c (n "encoded-words") (v "0.2.0") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)) (d (n "charset") (r "^0.1.2") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.20") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (f (quote ("std"))) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "1vsb2n9pgyxirzaj8j9i5n1whmkzd62sjd2d8jwy5130gq8965lw")))

