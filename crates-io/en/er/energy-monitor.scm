(define-module (crates-io en er energy-monitor) #:use-module (crates-io))

(define-public crate-energy-monitor-0.1.0 (c (n "energy-monitor") (v "0.1.0") (h "1g59yfk8xl2yxrf3kqgdqdxh4xdcq8da9ls7s1mynz0nd1bs8qvm")))

(define-public crate-energy-monitor-0.1.1 (c (n "energy-monitor") (v "0.1.1") (h "1kmw3rl607igg6b39s6m2h4ff1g8rzfvwbrsnnsvjbznsdi67kp3")))

(define-public crate-energy-monitor-0.2.0 (c (n "energy-monitor") (v "0.2.0") (h "0p70shgjfhxw1vgmb07pr39ngvljv12z56il6vdn1k7ich32v1zy")))

(define-public crate-energy-monitor-0.2.1 (c (n "energy-monitor") (v "0.2.1") (h "01hbag0z8jwk8xs1rsk6k1cakyc10v74k6k6fc63ambswrgw34vh")))

