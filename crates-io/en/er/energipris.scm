(define-module (crates-io en er energipris) #:use-module (crates-io))

(define-public crate-energipris-0.1.0 (c (n "energipris") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rasciigraph") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1z7dw3s016d74wck8bkhs9kvh60x15bl7kn93rsji9nyhbjx65cg")))

