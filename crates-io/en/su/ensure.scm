(define-module (crates-io en su ensure) #:use-module (crates-io))

(define-public crate-ensure-0.1.0 (c (n "ensure") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.3.0") (d #t) (k 2)))) (h "1b9cgsjzj42djlsk9zvq0x0rwhgsmdrgxdvb6rdvajzs3yyr8hi3")))

(define-public crate-ensure-0.2.0 (c (n "ensure") (v "0.2.0") (h "0yyl5jzldbklw95fllmhj42sg64a0y1vpz77ywvcc1wa8494yz8l")))

(define-public crate-ensure-0.2.1 (c (n "ensure") (v "0.2.1") (h "0qqy0cb2gb25nd8v7s6n9s534g45d0c3qnlfppc2hs1ijkcbxq9q")))

(define-public crate-ensure-0.2.2 (c (n "ensure") (v "0.2.2") (h "02g44wsik20k3z2vfwnn6qp1wi9bdnijxbc3s2wsjhla52hqb8s5")))

(define-public crate-ensure-0.3.0 (c (n "ensure") (v "0.3.0") (h "1c09ncaahg2mx8as5lhs0qf3abc2gx1f1cl3n86kxfiyh0pnvc8m")))

(define-public crate-ensure-0.3.1 (c (n "ensure") (v "0.3.1") (h "0y64fyv4ng1ji8v1svh642cxrqzzhx2jyrbrl8vsi2rk9vhfi67n")))

