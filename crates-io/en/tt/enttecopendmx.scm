(define-module (crates-io en tt enttecopendmx) #:use-module (crates-io))

(define-public crate-enttecopendmx-0.1.0 (c (n "enttecopendmx") (v "0.1.0") (d (list (d (n "libftd2xx") (r "~0.31.0") (f (quote ("static"))) (d #t) (k 0)))) (h "1xmwxc1vkskga4v9115krrnyl965mj6b5phxbr14xyzizxgsr0hp")))

(define-public crate-enttecopendmx-0.1.1 (c (n "enttecopendmx") (v "0.1.1") (d (list (d (n "libftd2xx") (r "~0.31.0") (f (quote ("static"))) (d #t) (k 0)))) (h "1id0ia145cfy9cr2bkaaxrv9y5f5cln78mc1hf05rmdlcxhmm6s3")))

