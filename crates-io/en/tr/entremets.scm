(define-module (crates-io en tr entremets) #:use-module (crates-io))

(define-public crate-entremets-0.1.0-alpha.1 (c (n "entremets") (v "0.1.0-alpha.1") (h "18v2sh3fgp9zp0cq140fw9d7fld1l9x8dadyhna0fanphfk4mc8q")))

(define-public crate-entremets-0.1.0-alpha.2 (c (n "entremets") (v "0.1.0-alpha.2") (h "05qpqyfz0qshfdq2nhc4rzgnd2qk65v3xw1ls3bk0zvidkajdgj8")))

(define-public crate-entremets-0.1.0-alpha.3 (c (n "entremets") (v "0.1.0-alpha.3") (h "16kn0va8l5rnwr3c5fdq11q1a5r2ccj6pr24dc3s5xms1chjp3ck")))

(define-public crate-entremets-0.1.0-alpha.4 (c (n "entremets") (v "0.1.0-alpha.4") (h "05fhwzdk70iwclplh59613gyn89sc68mbs2b0mi8f5zkisjna3f5")))

(define-public crate-entremets-0.1.0-alpha.5 (c (n "entremets") (v "0.1.0-alpha.5") (h "1qyw3k6amvxkjpdq7pp63kqxp4p6ayjjjd4wzifc4pdv3dghi958")))

(define-public crate-entremets-0.1.0-alpha.6 (c (n "entremets") (v "0.1.0-alpha.6") (h "0ygs5490k38djswd8l6r5iv1znx73kkhllmkriq7c487vsh8jc0q")))

