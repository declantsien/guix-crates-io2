(define-module (crates-io en tr entro) #:use-module (crates-io))

(define-public crate-entro-0.0.1 (c (n "entro") (v "0.0.1") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "084h56nlv2hd0ndn5szv0agcy7amivlsklf8k4rvicf32kngli3d")))

(define-public crate-entro-0.0.2 (c (n "entro") (v "0.0.2") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "113xhzcikd4fsn7fjpakbdw8nj3v87yk9lnbhnm3snipahw116if")))

(define-public crate-entro-0.0.3 (c (n "entro") (v "0.0.3") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "043mfmr9m6y9azl3rb8x836hjxwhlwzp0r2q1v8ppbbqgb68ba5i")))

(define-public crate-entro-0.1.0 (c (n "entro") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "00g6i8a27j57wmdmy4zx6fni1zlxy8vnzbqjrb03021nc639175r")))

(define-public crate-entro-0.1.1 (c (n "entro") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jx7n9khrs22s2h9lx392snk1mwy8jql2pbzfh2fv9567ryc58ix")))

(define-public crate-entro-0.1.2 (c (n "entro") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nk1a0xxghnxng989yzl0xm0gwmw6588i4sngmswq0ryfmhx02hr")))

