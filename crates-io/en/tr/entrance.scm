(define-module (crates-io en tr entrance) #:use-module (crates-io))

(define-public crate-entrance-0.1.0 (c (n "entrance") (v "0.1.0") (d (list (d (n "entrance_derive") (r "^0.1") (d #t) (k 0)))) (h "1wfhmd521l5ihq98mcf1g28pc15cfwcam08rabckba12vvkbn3li")))

(define-public crate-entrance-0.2.0 (c (n "entrance") (v "0.2.0") (d (list (d (n "entrance_derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "16xx9svni52hmjqvihrks86r61qwnbn80fyvhqnrvxh2j9np7vfz") (f (quote (("derive" "entrance_derive") ("default" "derive"))))))

(define-public crate-entrance-0.3.0 (c (n "entrance") (v "0.3.0") (d (list (d (n "entrance_derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1lxk72rb5gbb2njsw8cxnwb5ky7sx3cb0dpwixcb9gi8mnrv44sf") (f (quote (("derive" "entrance_derive") ("default" "derive"))))))

