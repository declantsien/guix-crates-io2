(define-module (crates-io en tr entropy-lib) #:use-module (crates-io))

(define-public crate-entropy-lib-1.0.1 (c (n "entropy-lib") (v "1.0.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1l310iwpzdx3kxrzfz7cn56wqrrc6zvn77phl5m6ma4mfc4cak3m")))

(define-public crate-entropy-lib-1.0.2 (c (n "entropy-lib") (v "1.0.2") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0yj70i9hansdsca0dbfq4d03j0kjqzd8jczbf5iidnwcw8x2kllq")))

