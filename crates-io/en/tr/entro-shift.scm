(define-module (crates-io en tr entro-shift) #:use-module (crates-io))

(define-public crate-entro-shift-1.0.0 (c (n "entro-shift") (v "1.0.0") (h "1bs0dzgrn1z94p6pz21g0j4a42cm5gfvyng8b1n7w3a2lmx52s3z")))

(define-public crate-entro-shift-1.0.1 (c (n "entro-shift") (v "1.0.1") (h "1z5fk1dw6daklagasl1727v6ql4v5g9h8rxgjmb41ygmdjvgxwfs")))

(define-public crate-entro-shift-1.0.2 (c (n "entro-shift") (v "1.0.2") (h "1w5gha2himbfm355qga3q1w1ffqikfqqv4y013fxk673sih2n51z")))

(define-public crate-entro-shift-1.0.3 (c (n "entro-shift") (v "1.0.3") (h "1vj8g0j4d3jx0wkdny5kkqn4fdb9gnxgn3j7zqy07rz95c7dwqvj")))

(define-public crate-entro-shift-1.0.4 (c (n "entro-shift") (v "1.0.4") (h "0mrwh5pi131sxws1j3272i1w62d762v8yliklccmwcxpjhd062pc")))

(define-public crate-entro-shift-1.0.5 (c (n "entro-shift") (v "1.0.5") (h "0jqirvbra49nrnxrkfd060w0lj7q8ph7jysf6j394ab0f3s8qvqf")))

(define-public crate-entro-shift-1.0.6 (c (n "entro-shift") (v "1.0.6") (h "1m7wmrna2gzkqi9bpy0lxkmvzbql7mzx3fymm7san3r2ggklg15s")))

(define-public crate-entro-shift-1.0.7 (c (n "entro-shift") (v "1.0.7") (h "1ds9zv14bcrhgaybyjq1940i4sb6ii4ryc8y6jdyvfb9qg3kndcx")))

(define-public crate-entro-shift-1.1.0 (c (n "entro-shift") (v "1.1.0") (h "0xmvdanv1yi8lr09rw8p87r0v8d9b1y0h5n9b676wz52k7sv9gsp")))

