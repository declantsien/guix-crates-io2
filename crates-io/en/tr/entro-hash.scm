(define-module (crates-io en tr entro-hash) #:use-module (crates-io))

(define-public crate-entro-hash-1.0.0 (c (n "entro-hash") (v "1.0.0") (h "1hilaz6ndz247qw0aq1c93i9w4yndd487pca3ygh4n8hv95fr2sz")))

(define-public crate-entro-hash-1.0.1 (c (n "entro-hash") (v "1.0.1") (h "1kxvm5hzc077m9zm5sjx9r8sr8dmwpza52phichx95bs9s861fyx")))

(define-public crate-entro-hash-1.0.2 (c (n "entro-hash") (v "1.0.2") (h "0vqxnsg57dcy24iv7k9jhxxsy8ayb9869i4m5wvaq8pil22p5cad")))

(define-public crate-entro-hash-1.0.3 (c (n "entro-hash") (v "1.0.3") (h "0qqv9am7rrrzb53ax408rm5iymg21zlcpd1qcvxxxyy1bnka7cna")))

(define-public crate-entro-hash-1.0.4 (c (n "entro-hash") (v "1.0.4") (h "0cp1s4cmp180fywq8r8i5gc5yjl3m9kr97b5sbdl4i735g75fxn3")))

(define-public crate-entro-hash-1.0.5 (c (n "entro-hash") (v "1.0.5") (h "04vh059hjxh0nn44pvkacfbqz8q4f38lhq17c842rvlzy5nbs4wd")))

(define-public crate-entro-hash-1.0.6 (c (n "entro-hash") (v "1.0.6") (h "10xvjkq0dmfxvb27sp27svjm9wm5l9p8iib8k426lr468ynikam8")))

