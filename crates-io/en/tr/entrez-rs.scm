(define-module (crates-io en tr entrez-rs) #:use-module (crates-io))

(define-public crate-entrez-rs-0.1.0 (c (n "entrez-rs") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.20.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l7s156jpqp6sj98rcwbl1ady23vmma5rmplpky63liqrnkydmzr")))

(define-public crate-entrez-rs-0.1.1 (c (n "entrez-rs") (v "0.1.1") (d (list (d (n "quick-xml") (r "^0.20.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zxhmy3921k32b4qw0ai43wjwagc3r309cqca7idnpycsf778ag7")))

(define-public crate-entrez-rs-0.1.2-alpha (c (n "entrez-rs") (v "0.1.2-alpha") (d (list (d (n "quick-xml") (r "^0.20.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "roxmltree") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bsbdd40683s9iwml7cmh338wxknsxkr3g8fwqdfk6d6k6nzh6ad")))

(define-public crate-entrez-rs-0.1.2 (c (n "entrez-rs") (v "0.1.2") (d (list (d (n "quick-xml") (r "^0.20.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "roxmltree") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pfkz58869va7wl4cbqqkhy1g85pf48s7b6yyin7ls8cicnxahrz")))

(define-public crate-entrez-rs-0.1.3 (c (n "entrez-rs") (v "0.1.3") (d (list (d (n "quick-xml") (r "^0.20.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "roxmltree") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13kkhzsbypv707snwrhs44m9bfgx22rs7wx2w3s95w54zbwx9nqc")))

(define-public crate-entrez-rs-0.1.4 (c (n "entrez-rs") (v "0.1.4") (d (list (d (n "quick-xml") (r "^0.20.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "roxmltree") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n0ghhyr22zrj7jpvxsf3amx9dn7sns45hnk5j4ph9bzkd4ljak5")))

