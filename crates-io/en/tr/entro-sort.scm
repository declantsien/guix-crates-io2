(define-module (crates-io en tr entro-sort) #:use-module (crates-io))

(define-public crate-entro-sort-1.0.0 (c (n "entro-sort") (v "1.0.0") (h "12r4n2b4axd7vnx0c75m17bkr34svm20359j8i2xjd7y8gvcmcdx")))

(define-public crate-entro-sort-1.0.1 (c (n "entro-sort") (v "1.0.1") (h "02vcfwnkx8ylfpr1l2i3pj5xaczxbi1nmgy60p9nqh98fx9nikpc")))

(define-public crate-entro-sort-1.0.2 (c (n "entro-sort") (v "1.0.2") (h "1sjsnawsxj8dwqbg4ih9brbzb8ripvxlndg8a8csg5jpifj718yl")))

(define-public crate-entro-sort-1.0.3 (c (n "entro-sort") (v "1.0.3") (h "14h0nm423fshaxnid47npywalngjdicx67s94m4mwr4z4giza0m6")))

(define-public crate-entro-sort-1.0.4 (c (n "entro-sort") (v "1.0.4") (h "082v1rlanafwyirbxdak5fav1sb48lfz799s3f2xgj64ljzhym8k")))

