(define-module (crates-io en tr entropiter) #:use-module (crates-io))

(define-public crate-entropiter-0.1.0 (c (n "entropiter") (v "0.1.0") (h "12fk4xxa780f5vkhljv1ic2x30nqhj69rkbz17xljjs1an1v9vz7")))

(define-public crate-entropiter-0.2.0 (c (n "entropiter") (v "0.2.0") (h "10nd2scs919c2ac919y971q4ghdplz92gsgl87wdww3xxwhp0xsl")))

(define-public crate-entropiter-0.2.1 (c (n "entropiter") (v "0.2.1") (h "1vpx9cb7k0ign1rir9kgi8395qg5g2fzslj6msl35kgzprzfz8b6")))

