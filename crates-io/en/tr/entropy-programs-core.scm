(define-module (crates-io en tr entropy-programs-core) #:use-module (crates-io))

(define-public crate-entropy-programs-core-0.0.0 (c (n "entropy-programs-core") (v "0.0.0") (h "1cwq1q0mhx2ychlbh54rcm170cwkzv3n9mc9yzwbci309plmv3rz")))

(define-public crate-entropy-programs-core-0.9.0 (c (n "entropy-programs-core") (v "0.9.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("custom"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "wit-bindgen") (r "^0.7.0") (d #t) (k 0)) (d (n "witgen") (r "^0.15.0") (d #t) (k 0)))) (h "1frgms4xbijz23l112gg7sh7374vyv73nr1y6h83qqafqy3qaggr") (f (quote (("std" "serde/std") ("default" "std"))))))

(define-public crate-entropy-programs-core-0.10.0 (c (n "entropy-programs-core") (v "0.10.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("custom"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "wit-bindgen") (r "^0.7.0") (d #t) (k 0)) (d (n "witgen") (r "^0.15.0") (d #t) (k 0)))) (h "1f9cgzwss5fahlp389952ckl2krz8zxasr2kyvfz4g27s57d1df5") (f (quote (("std" "serde/std") ("default" "std"))))))

