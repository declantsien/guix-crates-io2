(define-module (crates-io en tr entropyscan-rs) #:use-module (crates-io))

(define-public crate-entropyscan-rs-0.1.0 (c (n "entropyscan-rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tabled") (r "^0.15.0") (d #t) (k 0)))) (h "0g7qkpw68jmp0kszgc229ajsh44il2mmg86hfnjivcf6na3vqfcy")))

