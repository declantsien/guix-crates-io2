(define-module (crates-io en tr entrance_derive) #:use-module (crates-io))

(define-public crate-entrance_derive-0.1.0 (c (n "entrance_derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0qip1grma0lna8hip6s498jqjflfhyxc41mbhl73mhik73l9npzl")))

(define-public crate-entrance_derive-0.2.0 (c (n "entrance_derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1j1p3w78f1qxnwvf4vwnhcw70l24ln83vr4dn68fnac1z4adywr7")))

(define-public crate-entrance_derive-0.3.0 (c (n "entrance_derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0c9nda3gmnqllm1ri1xxwn9j4jrrafpr8sp6cnwvszy52smj2bpc")))

