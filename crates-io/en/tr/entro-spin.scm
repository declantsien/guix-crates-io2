(define-module (crates-io en tr entro-spin) #:use-module (crates-io))

(define-public crate-entro-spin-1.0.0 (c (n "entro-spin") (v "1.0.0") (h "061da04sb96br7qd0w015z67k26kbj9icvp718czrhggjbgma8xz")))

(define-public crate-entro-spin-1.0.1 (c (n "entro-spin") (v "1.0.1") (h "1hw8aas4hlcyhp6avq8s9wzjdr6w7jilbg43z74dckx9i7ia28hh")))

(define-public crate-entro-spin-1.0.3 (c (n "entro-spin") (v "1.0.3") (h "1p5f8ljll4hqw9ajkx0as87wlbfdhv5nsiasbjs0i1vbd2zvdgxy")))

(define-public crate-entro-spin-1.0.4 (c (n "entro-spin") (v "1.0.4") (h "033lzjhxccspdsld5rw21qm6wixa8vazzkf49dap59w6h9r7d0mm")))

(define-public crate-entro-spin-1.0.5 (c (n "entro-spin") (v "1.0.5") (h "0klnpv7818bw4a2ak4gamx17clw2ivyb6a0l61xm6hh26d3m2d0h")))

(define-public crate-entro-spin-1.0.6 (c (n "entro-spin") (v "1.0.6") (h "0gjfnbp0k0ygw5364n49zhn4b3j823mz28v7zxz8cskw4yx7zbhn")))

(define-public crate-entro-spin-1.0.8 (c (n "entro-spin") (v "1.0.8") (h "10v0n73faywy94lfszvxyswwcw6wyv3mqir4vsvizirwkn1n4n8g")))

(define-public crate-entro-spin-1.1.0 (c (n "entro-spin") (v "1.1.0") (h "0m6zfv4y3v3lx9248z7rsccgc0f2dwbyphx28z7xnymy055c7cbn")))

(define-public crate-entro-spin-1.1.1 (c (n "entro-spin") (v "1.1.1") (h "1ikmd6m7nd14s3kq4kl89qjz5wnl5vgknd36vf527b93xgbd7bn3")))

(define-public crate-entro-spin-1.1.2 (c (n "entro-spin") (v "1.1.2") (h "1d472b8aswpxml439k7vdxkcm5awsiy6ca4dfd6dpz4higxc10bk")))

(define-public crate-entro-spin-1.1.3 (c (n "entro-spin") (v "1.1.3") (h "166ipv4yfvrx8x2fpw6rx0kl6f34dl1yhfilgxczxbwlpgdj92ik")))

