(define-module (crates-io en tr entrypoint_macros) #:use-module (crates-io))

(define-public crate-entrypoint_macros-0.1.0 (c (n "entrypoint_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0zrx9yf7jvb02y00vq378bczvi1p38j92q3v80y5vlr0y8z065bh")))

(define-public crate-entrypoint_macros-0.1.1 (c (n "entrypoint_macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "185flxlcyjzqq5h5i12lrg2d2k1ya1xvv1mxl07ipcl37l61zyd3")))

(define-public crate-entrypoint_macros-0.2.0 (c (n "entrypoint_macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1yrcw2jq1g7nbac1awjvjpgs332mjfi7c4g8nipnhii2q1rj10bw")))

