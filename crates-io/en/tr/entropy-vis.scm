(define-module (crates-io en tr entropy-vis) #:use-module (crates-io))

(define-public crate-entropy-vis-0.1.0 (c (n "entropy-vis") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "human-panic") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "owo-colors") (r "^3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "terminal_size") (r "^0.3") (d #t) (k 0)))) (h "1ff5243cavzww633qsd6kmdydg1x0slfp2b4qgfy3lh698ngyqs1")))

