(define-module (crates-io en tr entropy-macro) #:use-module (crates-io))

(define-public crate-entropy-macro-0.1.0 (c (n "entropy-macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1p2b67f44hjs0vw75k8zfgl1qndxspny42vsl45hara4ggsj6h3p")))

(define-public crate-entropy-macro-0.2.0 (c (n "entropy-macro") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "06g9gp1676317nfwzkca9p3hxz4jbrwjw564zdndqs9qpxvmi6fr")))

(define-public crate-entropy-macro-0.2.1 (c (n "entropy-macro") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1nqfsqlyrl1h2i8xnld8jgqppzqhmidsyqd44hlnisai1h5ri3q1")))

(define-public crate-entropy-macro-0.2.2 (c (n "entropy-macro") (v "0.2.2") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1dqmqqy8rrbzfcgf7d3lizw4zi3yww9d504iy6w7jkr1wzh6ihrq")))

(define-public crate-entropy-macro-0.3.1 (c (n "entropy-macro") (v "0.3.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "0w7b1xx3l4brckgziz21v1b7dibasknjyx3v2sapmh2hk5mpf8ck")))

(define-public crate-entropy-macro-0.3.2 (c (n "entropy-macro") (v "0.3.2") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1mvmwy7mygzpmdcllws60qza7g0plyx8k7h2gk502r0nn1wadblj")))

(define-public crate-entropy-macro-0.3.3 (c (n "entropy-macro") (v "0.3.3") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "11g3rqjyp3x2f2pj8r4s8sbyq1jnjxdyar1ygnkz8h8r7c6ssh54")))

