(define-module (crates-io en tr entrait_macros) #:use-module (crates-io))

(define-public crate-entrait_macros-0.1.0 (c (n "entrait_macros") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "mockall") (r "^0.11") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0i6h9q9s213az4zgvc3q9w2vik3q0nkv4klm1zxgj5q7xpl1f014")))

(define-public crate-entrait_macros-0.2.0 (c (n "entrait_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full"))) (d #t) (k 0)))) (h "05b3byzzvxw0kzkdv2xv5ip5435ar19jjaqg2j0s91qpsgxm02i6")))

(define-public crate-entrait_macros-0.2.1 (c (n "entrait_macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full"))) (d #t) (k 0)))) (h "1mmmg7vzq4vbb6hzwacj46m1xpn1s5zbkawvvf7wvrqvy8k4bwdc")))

(define-public crate-entrait_macros-0.3.0-beta.0 (c (n "entrait_macros") (v "0.3.0-beta.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full"))) (d #t) (k 0)))) (h "13sfn785ad57h99s67hx51bp6f4ppdwyg3ssz8bz52mg9y8skrdx")))

(define-public crate-entrait_macros-0.3.0 (c (n "entrait_macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full"))) (d #t) (k 0)))) (h "0cq8833vcqwbvqzxmpkhs7bd554jagz4ahk38v9wdqmpsfjfxyhj")))

(define-public crate-entrait_macros-0.3.1 (c (n "entrait_macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full"))) (d #t) (k 0)))) (h "1v7z83hw9z95alqvjamwf1yny474d5alhy77hmf5sn24j3mdhh24")))

(define-public crate-entrait_macros-0.3.2 (c (n "entrait_macros") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full"))) (d #t) (k 0)))) (h "05776ps825hj82bkc7viax9ipb2n7q382l49vn3yrd09bxqkzlb7")))

(define-public crate-entrait_macros-0.3.3 (c (n "entrait_macros") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full"))) (d #t) (k 0)))) (h "1sr5gr0rs5917mzmp0lr553qsi7255662ri53w0vp8r5rw8jp9jv")))

(define-public crate-entrait_macros-0.4.0-beta.0 (c (n "entrait_macros") (v "0.4.0-beta.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "02cxwb6x3c3h7byandv2kb2in5qkap2xgzi02lyvw33xzsrpkp9n")))

(define-public crate-entrait_macros-0.4.0-beta.1 (c (n "entrait_macros") (v "0.4.0-beta.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0rds0xhgcrdfcc46ic4x8n07y0fs8y0dr9pwb0vwknslxbbhskr6")))

(define-public crate-entrait_macros-0.4.0 (c (n "entrait_macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "10q074xm492cigxdqlw4w5893k3ib0x3qwa5jsa15nrnj6gxq772")))

(define-public crate-entrait_macros-0.4.1 (c (n "entrait_macros") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0jk5lcs6x2dfl2fa3jmpd3dvvaxk6naad53b0m1mq2wkbbznpfq0")))

(define-public crate-entrait_macros-0.4.2 (c (n "entrait_macros") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0r7s1kzvasdx2vn9wwbrrafrlmprwsm7arbgvi1vzn6qxi9jz10v")))

(define-public crate-entrait_macros-0.4.3 (c (n "entrait_macros") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0py9f7l6y56inchfnnca8nws8vrw1x87yd6gwxb2vy00y144b10f")))

(define-public crate-entrait_macros-0.4.4 (c (n "entrait_macros") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "08358kwx6w3kk9pcbxy3xlz8nw6pfsjax9yx80chfrg0ja0xyv7m")))

(define-public crate-entrait_macros-0.4.5 (c (n "entrait_macros") (v "0.4.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0b4z040lik3bqpbfgkadb32xcrzimdrhb7abdq7ahx67bpxd9vpy")))

(define-public crate-entrait_macros-0.4.6 (c (n "entrait_macros") (v "0.4.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1jn8ynnx7bm470kqf86jr2i3iyl6nq7lm68n41h1rc55h94czxf0")))

(define-public crate-entrait_macros-0.5.0-alpha.0 (c (n "entrait_macros") (v "0.5.0-alpha.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0i7x498b2x7f93l84di2cjjpv48m4b4rcdimcfbiaf7w3xwzifkn")))

(define-public crate-entrait_macros-0.5.0-alpha.1 (c (n "entrait_macros") (v "0.5.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0lmib6bjxlqwm3prfmk5rvgw0lwrmcldnszb7lnmwqf6iy0vpgbm")))

(define-public crate-entrait_macros-0.5.0 (c (n "entrait_macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "17408xg44xpn53ml1h2zj2267vq9ikwh92r11d3h1hmw6nn9bhwh")))

(define-public crate-entrait_macros-0.5.1 (c (n "entrait_macros") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "12xxlscfhzl9f3d7yp1ry0psrhpj6cg21p6p6ldk460ykymjivf6")))

(define-public crate-entrait_macros-0.5.2 (c (n "entrait_macros") (v "0.5.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.75") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "08lvpr7ja39yy0dbwlnymd9mm6vlci7br3x38a7cx4dhxyprync3")))

(define-public crate-entrait_macros-0.5.3 (c (n "entrait_macros") (v "0.5.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.8") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0i9nbq59y1vahvs2c4zgi0xhjm9fvaclmmgldrmvxbxnmib000vg") (r "1.60")))

(define-public crate-entrait_macros-0.6.0 (c (n "entrait_macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.8") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1b58v96q328lxjyqdh5qp3sqbdwy9cpp6ka9xvy14qp44633y910") (r "1.60")))

(define-public crate-entrait_macros-0.7.0 (c (n "entrait_macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.8") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0fbgs9957vxsy9ypqcqi1fxshjybahhpkbkj9ra570qhrv2j0jy8") (r "1.60")))

