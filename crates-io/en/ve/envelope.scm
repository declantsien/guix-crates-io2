(define-module (crates-io en ve envelope) #:use-module (crates-io))

(define-public crate-envelope-0.1.2 (c (n "envelope") (v "0.1.2") (d (list (d (n "rustc-serialize") (r "^0.2.12") (d #t) (k 0)))) (h "1h482lr47xmgppranf594jqny1fygip2pl7cdl2id95ag1617ikc")))

(define-public crate-envelope-0.1.3 (c (n "envelope") (v "0.1.3") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1x8n9cx6j1b3q3jm5kjbgdfdznqbjcgigqcml6hk97wf0b4h98q0")))

(define-public crate-envelope-0.2.0 (c (n "envelope") (v "0.2.0") (d (list (d (n "interpolation") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1p76r2ha7qqqvan3wj9qjryh9idyf2ny8vkj7bgz8a96lf3w18f5")))

(define-public crate-envelope-0.2.1 (c (n "envelope") (v "0.2.1") (d (list (d (n "interpolation") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1bhjnz9lgkgnchdh6fqbihcym5ypbh6rf8av13cbkx9j92r911v6")))

(define-public crate-envelope-0.2.2 (c (n "envelope") (v "0.2.2") (d (list (d (n "interpolation") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0r7b425s1vnmly4xv3nh8wxd70n7m3xrwd5rhp8fj9lrs4202f9b")))

(define-public crate-envelope-0.3.0 (c (n "envelope") (v "0.3.0") (d (list (d (n "interpolation") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0rirabirc19fqw95lxwd25cfhwr0q1882xiskz747s0838askcmf")))

(define-public crate-envelope-0.4.0 (c (n "envelope") (v "0.4.0") (d (list (d (n "interpolation") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1g8n8nc7zbxfd4akgxfzxdvq1y2d29z51wq3jh4cxmbmb6n4vvlm")))

(define-public crate-envelope-0.4.1 (c (n "envelope") (v "0.4.1") (d (list (d (n "interpolation") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0ag2d009i2q4xgfmyv1mgrbdnzldz6l5wjvab64qrkr18gklzcl5")))

(define-public crate-envelope-0.5.0 (c (n "envelope") (v "0.5.0") (d (list (d (n "interpolation") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1y60c2hw1i9lygqbmq73y4ngzbgvg9qa08mppnc8cc6x1fq8b91g")))

(define-public crate-envelope-0.5.1 (c (n "envelope") (v "0.5.1") (d (list (d (n "interpolation") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1pl43xy864dkgivimyib66f5wxy3zkcnaqhpk54c3s2897nsg7xq")))

(define-public crate-envelope-0.6.0 (c (n "envelope") (v "0.6.0") (d (list (d (n "interpolation") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "167g2dkf3kgfna20fyx9nispd1nf1c04y5jr5h3n6iijrvlmfiqc")))

(define-public crate-envelope-0.6.1 (c (n "envelope") (v "0.6.1") (d (list (d (n "interpolation") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "12gsxqvc1capcrpvrhvn9y3pdicgwslinw6qi3bv11s7i6jgjb7k")))

(define-public crate-envelope-0.6.2 (c (n "envelope") (v "0.6.2") (d (list (d (n "interpolation") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0j56mc2szg45vdwh5s9y70s4b2c861wgpz53hz07iakb8j56cn5l")))

(define-public crate-envelope-0.6.3 (c (n "envelope") (v "0.6.3") (d (list (d (n "interpolation") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1p1vplc343jjbav45aipiiydqfkga7g8zmd6lwzsc2y4wnb4hqv6")))

(define-public crate-envelope-0.6.4 (c (n "envelope") (v "0.6.4") (d (list (d (n "interpolation") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1hgbidw09nkx5i9grp896y7k4h9f9qx5r5cr5c2ysmkbjyyymbs0")))

(define-public crate-envelope-0.6.5 (c (n "envelope") (v "0.6.5") (d (list (d (n "interpolation") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0n5s926m61nhf6y8hwbzacysb2n6i7yy3m7nbcl95wls0r9r2wc9")))

(define-public crate-envelope-0.7.0 (c (n "envelope") (v "0.7.0") (d (list (d (n "interpolation") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0piqizswzrp0l476rq6wl478i1fgj3jjbdsvnjawnn02qak6g9bx")))

(define-public crate-envelope-0.8.0 (c (n "envelope") (v "0.8.0") (d (list (d (n "interpolation") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "1h2q5xcw0zsgi64i0whxbkgk0nk9lc3hj7klk21igzpppaghq0s3") (f (quote (("serde_serialization" "serde" "serde_json"))))))

(define-public crate-envelope-0.8.1 (c (n "envelope") (v "0.8.1") (d (list (d (n "interpolation") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "0vz4ycwn5k9qwxgnr95gvc53z4zphvwi14d2gjm9pid7pjga8683") (f (quote (("serde_serialization" "serde" "serde_json"))))))

