(define-module (crates-io en ve enve) #:use-module (crates-io))

(define-public crate-enve-0.0.0 (c (n "enve") (v "0.0.0") (h "1392fl68cnqzv1rgaxmjj2hlm6s86qgd6qc7rv0rdc6d6y4krdd2")))

(define-public crate-enve-0.1.0 (c (n "enve") (v "0.1.0") (d (list (d (n "estring") (r "^0.1") (d #t) (k 0)))) (h "1zwq4999814svxp20b8bmaha8y82sddjb8sbqw8zwlshcl2mwy0f") (f (quote (("vec" "estring/vec") ("number" "estring/number") ("default") ("bool" "estring/bool"))))))

(define-public crate-enve-0.2.0 (c (n "enve") (v "0.2.0") (d (list (d (n "estring") (r "^0.2") (d #t) (k 0)))) (h "1fdkpbbqygm7pdfv7gp71ncsa5shljgqkmll0vbm1sy7sjvyzwxm") (f (quote (("vec" "structs") ("structs" "estring/structs") ("number") ("low-level" "estring/low-level") ("default") ("bool")))) (r "1.59.0")))

(define-public crate-enve-0.3.0 (c (n "enve") (v "0.3.0") (d (list (d (n "estring") (r "^0.3") (d #t) (k 0)))) (h "097k1likcvykaxs59j5zvhh8mi71qr4g7pf47xjpshjz22hzvvvx") (f (quote (("vec" "structs") ("structs" "estring/structs") ("number") ("low-level" "estring/low-level") ("default") ("bool") ("aggs" "estring/aggs")))) (r "1.59.0")))

(define-public crate-enve-0.4.0 (c (n "enve") (v "0.4.0") (d (list (d (n "estring") (r "^0.3") (d #t) (k 0)))) (h "0abphvsgrfmpbicjxg278zjglsm9idnksqn2lyvnbdqs92dbx83y") (f (quote (("vec" "structs") ("structs" "estring/structs") ("number") ("low-level" "estring/low-level") ("default") ("bool") ("aggs" "estring/aggs")))) (r "1.59.0")))

