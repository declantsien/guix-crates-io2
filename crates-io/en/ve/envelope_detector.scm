(define-module (crates-io en ve envelope_detector) #:use-module (crates-io))

(define-public crate-envelope_detector-0.1.0 (c (n "envelope_detector") (v "0.1.0") (d (list (d (n "dsp-chain") (r "^0.9.0") (d #t) (k 2)) (d (n "time_calc") (r "^0.10.1") (d #t) (k 2)))) (h "0arz0zk73awpsrdqq7sw0z5smxb6fi9x8sp9p423vpiy24dqiidg")))

(define-public crate-envelope_detector-0.1.1 (c (n "envelope_detector") (v "0.1.1") (d (list (d (n "portaudio") (r "^0.6.1") (d #t) (k 2)) (d (n "time_calc") (r "^0.10.1") (d #t) (k 2)))) (h "0zxnlkddp00sy46mjrypqwk54bfdgx0rxp144s5gglxffnfswwd3")))

(define-public crate-envelope_detector-0.1.2 (c (n "envelope_detector") (v "0.1.2") (d (list (d (n "portaudio") (r "^0.6.2") (d #t) (k 2)) (d (n "time_calc") (r "^0.10.1") (d #t) (k 2)))) (h "1xyxhnzjr9il7b54ykbfr3n83kvyxbd0rwjghmp05fkxx9yfbdrc")))

(define-public crate-envelope_detector-0.2.0 (c (n "envelope_detector") (v "0.2.0") (d (list (d (n "portaudio") (r "^0.6.4") (d #t) (k 2)) (d (n "sample") (r "^0.6.0") (d #t) (k 0)) (d (n "time_calc") (r "^0.11.0") (d #t) (k 2)))) (h "1qzqv0hdlai61dpkxz59ja900spfsvd8gnaswd1h3pp7n0qwivkd")))

