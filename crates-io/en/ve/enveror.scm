(define-module (crates-io en ve enveror) #:use-module (crates-io))

(define-public crate-enveror-0.1.0 (c (n "enveror") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed-builder") (r "^0.18") (d #t) (k 0)))) (h "11fzy2n2inv0dg3pvccshd5wfadn30vdk2ilp8bf8i8dvhljawjk")))

(define-public crate-enveror-0.1.1 (c (n "enveror") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed-builder") (r "^0.18") (d #t) (k 0)))) (h "1l9wfn4ls6i0sk7jh7ybfzn0lywsqpgr6bz81j8066sd2fqgbzi0")))

(define-public crate-enveror-0.1.2 (c (n "enveror") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed-builder") (r "^0.18") (d #t) (k 0)))) (h "06s0ndcw9x7m5y7wcp9584ga50xp6p3xx47fxi5lig3h0f4vyzc9")))

(define-public crate-enveror-0.1.3 (c (n "enveror") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed-builder") (r "^0.18") (d #t) (k 0)))) (h "0kwmfalm6k7sp786bif383j6c0507a2jssf94f36vyz5x7fdyi5j")))

(define-public crate-enveror-0.1.4 (c (n "enveror") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed-builder") (r "^0.18") (d #t) (k 0)))) (h "1zllyk9mw9bq8lid9xygihq0332fcincsmzph7wigx6mama6gl3c")))

(define-public crate-enveror-0.1.5 (c (n "enveror") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed-builder") (r "^0.18") (d #t) (k 0)))) (h "1ybx37w2n5n9y5ihcz3wj9asw3r9pm48szw8ycxxmgp2wzk6bq94")))

(define-public crate-enveror-0.1.6 (c (n "enveror") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typed-builder") (r "^0.18") (d #t) (k 0)))) (h "1mi74nn9g2bni8zs8jw05ys4inmfhcl91xr3s6hafz219b1i2ppd")))

