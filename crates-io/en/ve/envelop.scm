(define-module (crates-io en ve envelop) #:use-module (crates-io))

(define-public crate-envelop-0.0.0 (c (n "envelop") (v "0.0.0") (h "1rv5qv9f0qkm1lk1rj969w8ql70iyi23zfa7584ac3xzdj4k6i1n")))

(define-public crate-envelop-0.0.1-alpha.1 (c (n "envelop") (v "0.0.1-alpha.1") (d (list (d (n "bytes") (r "^0.4.12") (f (quote ("serde"))) (d #t) (k 0)) (d (n "protobuf") (r "^2.7.0") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.7.0") (d #t) (k 1)) (d (n "ring") (r "^0.14.6") (d #t) (k 0)) (d (n "rust_swig") (r "^0.3.0") (d #t) (k 1)))) (h "01pcmlrvap32sln4bvjmjsf6m117f6z96g5cx2pgicyi20wmb9i7")))

(define-public crate-envelop-0.0.1-alpha.2 (c (n "envelop") (v "0.0.1-alpha.2") (d (list (d (n "bytes") (r "^0.4.12") (f (quote ("serde"))) (d #t) (k 0)) (d (n "protobuf") (r "^2.7.0") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.7.0") (d #t) (k 1)) (d (n "ring") (r "^0.14.6") (d #t) (k 0)) (d (n "rust_swig") (r "^0.3.0") (d #t) (k 1)))) (h "1fpid949g73rpbayv5db5mdmvxhx0czxr4s9w9bhjdql67mpfgfw")))

(define-public crate-envelop-0.0.1-alpha.4 (c (n "envelop") (v "0.0.1-alpha.4") (d (list (d (n "bytes") (r "^0.4.12") (f (quote ("serde"))) (d #t) (k 0)) (d (n "protobuf") (r "^2.7.0") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.7.0") (d #t) (k 1)) (d (n "ring") (r "^0.14.6") (d #t) (k 0)) (d (n "rust_swig") (r "^0.3.0") (d #t) (k 1)))) (h "13r0qwvxbaw825insx46mlvzbgqgw39fmcq6ciy091fbgmyvg81r")))

