(define-module (crates-io en by enby) #:use-module (crates-io))

(define-public crate-enby-0.1.0 (c (n "enby") (v "0.1.0") (h "149n1qyxlpf95s0jk18nva0ig2622jh4jdh9bdizk4cr6cjiydzr") (y #t)))

(define-public crate-enby-0.0.0 (c (n "enby") (v "0.0.0") (h "1jlnmb7z59q1jc16q9ihmh0kl19raldvhpbija2nrky42fnp86m7")))

