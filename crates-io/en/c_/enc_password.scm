(define-module (crates-io en c_ enc_password) #:use-module (crates-io))

(define-public crate-enc_password-0.1.0 (c (n "enc_password") (v "0.1.0") (d (list (d (n "aes-gcm") (r "^0.8.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.6") (d #t) (k 0)))) (h "0p8v58dc3cdggipq1ysvgnqgjq6g93bwf15nzn1dflwx08rnnl0k")))

(define-public crate-enc_password-0.1.1 (c (n "enc_password") (v "0.1.1") (d (list (d (n "aes-gcm") (r "^0.8.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.6") (d #t) (k 0)))) (h "1zwdgf82w9lfgraswxfir5x6vfmicq2sxjc16gxkik1b1n8m4sh5")))

(define-public crate-enc_password-0.1.2 (c (n "enc_password") (v "0.1.2") (d (list (d (n "aes-gcm") (r "^0.8.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.6") (d #t) (k 0)))) (h "18w1qdissjc9npw6kg4fyasp2i5xs8d11hw9jmf0jfqxikx0mkrz")))

