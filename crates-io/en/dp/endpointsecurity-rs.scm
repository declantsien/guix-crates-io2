(define-module (crates-io en dp endpointsecurity-rs) #:use-module (crates-io))

(define-public crate-endpointsecurity-rs-0.1.0 (c (n "endpointsecurity-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "block") (r "^0.1.6") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "1l7jjqdyyxfifmcgkga67kpqlygqymdrx89qlfybbdjr85nf6gf8")))

(define-public crate-endpointsecurity-rs-0.1.1 (c (n "endpointsecurity-rs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "block") (r "^0.1.6") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "0a6ppgmy0pd0cjzi1iqdbh8wfipjr8v74lsk86yvvfabz9fma299")))

