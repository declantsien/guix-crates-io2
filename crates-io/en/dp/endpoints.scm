(define-module (crates-io en dp endpoints) #:use-module (crates-io))

(define-public crate-endpoints-0.1.0 (c (n "endpoints") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11lvzsqi0jismnmqv5008l2xx0d4bpy0cp10dkv6vgpkfn77kzg3")))

(define-public crate-endpoints-0.2.0 (c (n "endpoints") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "19d8kk01xh4g4cz1vrp9mifa7gw430ibn2wj5z5w0n75nj62fkw4")))

(define-public crate-endpoints-0.3.0 (c (n "endpoints") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "147hqxpbmmz4wnzv51zcdilhwiil70349m2z8i3spaydd6hq3kg9")))

(define-public crate-endpoints-0.4.0 (c (n "endpoints") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z092vpv54pnjbr9390z1kcn1qfiwhxdjr85slc7x6hv8any41yz")))

(define-public crate-endpoints-0.5.0 (c (n "endpoints") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "1xkj7msrf1ld3aag875r5ygnzq1xh86r3rs4wdsi6jpwy3vxm9v4")))

(define-public crate-endpoints-0.5.1 (c (n "endpoints") (v "0.5.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "0k7g12cnaija5jx9f44ml26q83hp441fkfynfvsmjb385k2ckgm5")))

(define-public crate-endpoints-0.5.2 (c (n "endpoints") (v "0.5.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "11f1ha0sh30gd6w309lmk92vv2bc6ibz2klf986z94159x8yd70w")))

(define-public crate-endpoints-0.5.3 (c (n "endpoints") (v "0.5.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "0lhlm7r9ip4hqlcj7fw44kp6daxfd0m7c0lbi5bj1pwlb87xqn2m")))

(define-public crate-endpoints-0.6.0 (c (n "endpoints") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "0w5nyany3igxsx3dcqybf63ns0x4x25kqkjk12f91mnj8bppddwd")))

(define-public crate-endpoints-0.7.0 (c (n "endpoints") (v "0.7.0") (d (list (d (n "indexmap") (r "^2.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "00m30l7b6yrr2arx958nbn02sk3ckqg4k84kdrlgvr7qsxzayv24")))

(define-public crate-endpoints-0.7.1 (c (n "endpoints") (v "0.7.1") (d (list (d (n "indexmap") (r "^2.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "0fix0lbrl4snhxcihh0vahrgnw1bpzaw5qch40z1gm9q1bf27rka")))

(define-public crate-endpoints-0.8.0 (c (n "endpoints") (v "0.8.0") (d (list (d (n "indexmap") (r "^2.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "1mvmi8dryhnyrg59rwb5xq1i63jgagrgaqb17r9qkl7gr1lak2n3")))

