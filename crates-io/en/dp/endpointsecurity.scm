(define-module (crates-io en dp endpointsecurity) #:use-module (crates-io))

(define-public crate-endpointsecurity-0.1.0 (c (n "endpointsecurity") (v "0.1.0") (d (list (d (n "block") (r "^0.1.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0sghg0fhckv8l7jdqyg8k7zjdsiqlsi3qxn20s0k5lrjm5n8lch2")))

(define-public crate-endpointsecurity-0.1.1 (c (n "endpointsecurity") (v "0.1.1") (d (list (d (n "block") (r "^0.1.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1vzcrx33r9444k2pdn6bnyh43nyfl7f6f2jp8pcmvigsp4x3xdqv")))

(define-public crate-endpointsecurity-0.2.0 (c (n "endpointsecurity") (v "0.2.0") (d (list (d (n "block") (r "^0.1.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "03kx591xdkqz4pfar3fq856aq131xspj2vr32mrv30qar7kclsiv")))

(define-public crate-endpointsecurity-0.2.1 (c (n "endpointsecurity") (v "0.2.1") (d (list (d (n "block") (r "^0.1.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1mi8g6d1vqc6zacvgr4lsz9qcfdd0871gbx7li1r5bp80nkfbjjb")))

(define-public crate-endpointsecurity-0.3.0 (c (n "endpointsecurity") (v "0.3.0") (d (list (d (n "block") (r "^0.1.6") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "117kwd43d9cpxzy7klcprj6l5drisx0njpcrqj24yzickwxsxfy2")))

