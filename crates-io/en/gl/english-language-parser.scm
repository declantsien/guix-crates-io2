(define-module (crates-io en gl english-language-parser) #:use-module (crates-io))

(define-public crate-english-language-parser-0.2.0 (c (n "english-language-parser") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10ph8cs67wykvgw0m55jrkg1zmink31hkvvlmdn6gx53hs1rbg2l")))

(define-public crate-english-language-parser-0.3.0 (c (n "english-language-parser") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "peg") (r "^0.8.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01lsrf69xchqpda0gk2bscijz211xlkdbxmk1ixwc05mxgfzzhw8")))

