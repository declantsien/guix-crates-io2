(define-module (crates-io en gl english-numbers) #:use-module (crates-io))

(define-public crate-english-numbers-0.1.0 (c (n "english-numbers") (v "0.1.0") (h "0dss078xzq7fv3j1kvjsnv24chjwrr8pk3gbhj38sc4bmbwjz58n")))

(define-public crate-english-numbers-0.2.0 (c (n "english-numbers") (v "0.2.0") (h "0d1iy2721hf7l61w3m96vzizvvg9g30svclhcqqgapllfvjfz0wk")))

(define-public crate-english-numbers-0.3.0 (c (n "english-numbers") (v "0.3.0") (h "045nd5qf2hl5813dhvk4qjf2zl0h5ggb92yqvf1ij8cpjxjbd5m0")))

(define-public crate-english-numbers-0.3.1 (c (n "english-numbers") (v "0.3.1") (h "0pmybvzi7b69lsx1s5jwzk2q8wlq47g37h1wv38n0yrqj2ji53mq")))

(define-public crate-english-numbers-0.3.2 (c (n "english-numbers") (v "0.3.2") (h "1icpar37v3nk74mkbbc4gxmjf9k30xkfr07d82h65xvmw7bs6y4v")))

(define-public crate-english-numbers-0.3.3 (c (n "english-numbers") (v "0.3.3") (h "19hs1lm2rs2r29wyij2b144iksa5lhnfggjsnjcd8r1935p5sksf")))

