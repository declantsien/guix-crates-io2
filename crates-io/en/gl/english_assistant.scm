(define-module (crates-io en gl english_assistant) #:use-module (crates-io))

(define-public crate-english_assistant-0.1.1 (c (n "english_assistant") (v "0.1.1") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1g9inq0xfirc3xjl4d9cq40zszvk8pcdyfwgplk5k71izacw7bkh")))

