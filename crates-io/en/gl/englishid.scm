(define-module (crates-io en gl englishid) #:use-module (crates-io))

(define-public crate-englishid-0.1.0 (c (n "englishid") (v "0.1.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "194db9agi6mn1y9k54w2fvvpr6wc8axhsvpdn3l8sip7piscb78m") (y #t)))

(define-public crate-englishid-0.2.0 (c (n "englishid") (v "0.2.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0v84wjbd2vxrppv9x5xabdyw8lymjn4vymxy02blxkilh86w30f6") (y #t)))

(define-public crate-englishid-0.3.0 (c (n "englishid") (v "0.3.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "06cambhhv5khi7m31wp6a09hyj9mi43cvwwzb39m5lzghsk09szl")))

(define-public crate-englishid-0.3.1 (c (n "englishid") (v "0.3.1") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "17r5rydrffjg5kxjz79dcfv6y895iqngn2zqcrg63bmr1ag7knrn")))

