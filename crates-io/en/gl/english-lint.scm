(define-module (crates-io en gl english-lint) #:use-module (crates-io))

(define-public crate-english-lint-0.1.0 (c (n "english-lint") (v "0.1.0") (d (list (d (n "aho-corasick") (r "^0.5.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.66") (d #t) (k 0)))) (h "0hmkdjz9cr0267plgjymxhz6jk26rad0s493nb6cdvaylim4s6qg")))

(define-public crate-english-lint-0.1.1 (c (n "english-lint") (v "0.1.1") (d (list (d (n "aho-corasick") (r "^0.5.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.66") (d #t) (k 0)))) (h "1z5gi8rapa5m43ij2fkd17xmx3ynb1jw2z1qgvcvk6mcvx1fsmdr")))

