(define-module (crates-io en vt envtestkit) #:use-module (crates-io))

(define-public crate-envtestkit-1.0.0 (c (n "envtestkit") (v "1.0.0") (d (list (d (n "fake") (r "^2.2.3") (d #t) (k 2)) (d (n "serial_test") (r "^0.4.0") (d #t) (k 2)))) (h "0jmc9cgzhk08jyqwsj8biky44gl6ajrnk31w65zwffavbana14m6")))

(define-public crate-envtestkit-1.1.0 (c (n "envtestkit") (v "1.1.0") (d (list (d (n "fake") (r "^2.2.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^0.4.0") (d #t) (k 2)))) (h "1grf96crd87xy4ci0lh44jym0azfigylmqqfln8hf4ns78bqpml7") (f (quote (("lock" "lazy_static" "parking_lot") ("default" "lock"))))))

(define-public crate-envtestkit-1.1.1 (c (n "envtestkit") (v "1.1.1") (d (list (d (n "fake") (r "^2.2.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^0.4.0") (d #t) (k 2)))) (h "1k1j1dvib6dsnvqarplas5pv6gwka78mk311cfs6g296nq0q9sr0") (f (quote (("lock" "lazy_static" "parking_lot") ("default" "lock"))))))

(define-public crate-envtestkit-1.1.2 (c (n "envtestkit") (v "1.1.2") (d (list (d (n "fake") (r "^2.2.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "serial_test") (r "^0.4.0") (d #t) (k 2)))) (h "0bsl27xrpb22259qhk28rpqybqhsagsqzpw3m1v0wnwsx534b8af") (f (quote (("lock" "lazy_static" "parking_lot") ("default" "lock"))))))

