(define-module (crates-io en vt envtime) #:use-module (crates-io))

(define-public crate-envtime-0.0.1 (c (n "envtime") (v "0.0.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0zazxwjgkz2lvgdkdx2fcrxpdc6nvppns0lynjgpy07civ6c6kkd")))

(define-public crate-envtime-0.0.2 (c (n "envtime") (v "0.0.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1sgakxcwnlnysx51k8afmixdbjrbhzgypnicz8g10sjxszmy39d7")))

(define-public crate-envtime-0.0.3 (c (n "envtime") (v "0.0.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0s9yy0cc3ngy010b3nj64dfvyz904r7mww6vyf5da896jk1cc4i4")))

(define-public crate-envtime-0.0.4 (c (n "envtime") (v "0.0.4") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "1s98103wc289xjypaj1ab15qfbzfqjr9r8b2a86fx9zl66j4w2v2")))

