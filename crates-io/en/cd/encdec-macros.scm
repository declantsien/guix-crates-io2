(define-module (crates-io en cd encdec-macros) #:use-module (crates-io))

(define-public crate-encdec-macros-0.1.0 (c (n "encdec-macros") (v "0.1.0") (d (list (d (n "encdec-base") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "1hfwzx0xyybird2c971bvvfplinjh7285qd00l28n11bi6v966zl")))

(define-public crate-encdec-macros-0.3.0 (c (n "encdec-macros") (v "0.3.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "encdec-base") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "0gk8ggh0pbp1j92mkjzw0x3r0223572znypsyszyr92i982ghln4")))

(define-public crate-encdec-macros-0.4.0 (c (n "encdec-macros") (v "0.4.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "encdec-base") (r "^0.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "12ha4c99mvkdmvdh6i23n9pd8fg9qgcm9gf1a886zhcikxp36prc")))

(define-public crate-encdec-macros-0.5.0 (c (n "encdec-macros") (v "0.5.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "encdec-base") (r "^0.5.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "1vcm7r7l9gw3rmpw4d1zgw2frz83qfq715g283jnhmwvsxkhx90c")))

(define-public crate-encdec-macros-0.6.0 (c (n "encdec-macros") (v "0.6.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "encdec-base") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "12h8ydf1bkg4p088ckafb3k1x4v6c882zgp80873p8wpm42zlii2")))

(define-public crate-encdec-macros-0.6.1 (c (n "encdec-macros") (v "0.6.1") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "encdec-base") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "1rvillr4d14wlm9plqvw8s7yw3d5rrsbi9gazzxpcfr75nqgsgnk")))

(define-public crate-encdec-macros-0.6.2 (c (n "encdec-macros") (v "0.6.2") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "encdec-base") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "0hndncn97ylwjwm51ip7ny6yrxivp5s5xzmbvfgjk25jwmbgvffp")))

(define-public crate-encdec-macros-0.7.0 (c (n "encdec-macros") (v "0.7.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "encdec-base") (r "^0.7.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "0aa1mqqybny3lgs239ahxjgia4hnaq0j01inrnb5v296zvbwf014")))

(define-public crate-encdec-macros-0.7.1 (c (n "encdec-macros") (v "0.7.1") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "encdec-base") (r "^0.7.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "1ik9figzmqz1p3pri1i0fab9rn2gc3qs8nbdq54rvv3pnixs832q")))

(define-public crate-encdec-macros-0.8.0 (c (n "encdec-macros") (v "0.8.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "encdec-base") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "0dyb1hy04v4m9a0fh79yc351wlh20x7qksk3m0bx146n9l2hj76b")))

(define-public crate-encdec-macros-0.8.1 (c (n "encdec-macros") (v "0.8.1") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "encdec-base") (r "^0.8.0") (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "01pzmqdal1jzdinyfn31b0y874kdqs5jv9rbbdzwvi4sldgbv668")))

(define-public crate-encdec-macros-0.8.2 (c (n "encdec-macros") (v "0.8.2") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "encdec-base") (r "^0.8.0") (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "1r9980q6gvs720dn2xxb0b4haw0c4x94gzcz7yz39hpw2lc0hdyy")))

(define-public crate-encdec-macros-0.8.3 (c (n "encdec-macros") (v "0.8.3") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "encdec-base") (r "^0.8.0") (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "08hzd2ls2cg1ysn35fvmpsni69kv956dbz2gyrlvv05ad95r3sbz")))

(define-public crate-encdec-macros-0.9.0 (c (n "encdec-macros") (v "0.9.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "encdec-base") (r "^0.8.0") (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)))) (h "1vgf0xbjziqgr04qgqjlpsvgmd19b7367k28hnzm6sxf5a9rfm5i")))

