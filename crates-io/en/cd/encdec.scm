(define-module (crates-io en cd encdec) #:use-module (crates-io))

(define-public crate-encdec-0.1.0 (c (n "encdec") (v "0.1.0") (d (list (d (n "encdec-base") (r "^0.1") (d #t) (k 0)) (d (n "encdec-macros") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0s8svdxypmjrc6c2g4754ydaqg8kjmdzzyaplgx0syif3r1b6gk6") (f (quote (("heapless" "encdec-base/heapless"))))))

(define-public crate-encdec-0.1.1 (c (n "encdec") (v "0.1.1") (d (list (d (n "encdec-base") (r "^0.1") (d #t) (k 0)) (d (n "encdec-macros") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0vswh32jls3148rqiyw20wrv4pnccwsz263m97l636x11pw7k4f7") (f (quote (("heapless" "encdec-base/heapless"))))))

(define-public crate-encdec-0.2.0 (c (n "encdec") (v "0.2.0") (d (list (d (n "encdec-base") (r "^0.1") (d #t) (k 0)) (d (n "encdec-macros") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1qj2p7qaf2q16kygsmf24z73ncb7nbxpj9vsqqpg0fxhijwppprb") (f (quote (("heapless" "encdec-base/heapless"))))))

(define-public crate-encdec-0.3.0 (c (n "encdec") (v "0.3.0") (d (list (d (n "encdec-base") (r "^0.3") (d #t) (k 0)) (d (n "encdec-macros") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0srrf4l4bv10qzs36gkh7yial0zq0gw2vfki47dg7aya2mbd3rlf") (f (quote (("heapless" "encdec-base/heapless"))))))

(define-public crate-encdec-0.4.0 (c (n "encdec") (v "0.4.0") (d (list (d (n "encdec-base") (r "^0.4.0") (d #t) (k 0)) (d (n "encdec-macros") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0mmh8yq31124f90h3l3aarw1207ddjp49fxqsrq5zh7lygadhq54") (f (quote (("heapless" "encdec-base/heapless"))))))

(define-public crate-encdec-0.5.0 (c (n "encdec") (v "0.5.0") (d (list (d (n "encdec-base") (r "^0.5.0") (k 0)) (d (n "encdec-macros") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1vamd3kgrcdygzpq50xcfmvjpcqdpb776kaq8p5hwnni4ala0y81") (f (quote (("std" "encdec-base/std") ("heapless" "encdec-base/heapless"))))))

(define-public crate-encdec-0.6.0 (c (n "encdec") (v "0.6.0") (d (list (d (n "encdec-base") (r "^0.6.0") (k 0)) (d (n "encdec-macros") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0gfw0v8isbqvbriapm0mv1sp0gs6qqk9lmhvsj9shlnsdd8qcslw") (f (quote (("std" "encdec-base/std") ("heapless" "encdec-base/heapless") ("alloc" "encdec-base/alloc"))))))

(define-public crate-encdec-0.6.1 (c (n "encdec") (v "0.6.1") (d (list (d (n "encdec-base") (r "^0.6.0") (k 0)) (d (n "encdec-macros") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1l6pfjjn0nx2m61bpkvaass67vnwd2lz5zwz3il3hab8afrjghad") (f (quote (("std" "encdec-base/std") ("heapless" "encdec-base/heapless") ("alloc" "encdec-base/alloc"))))))

(define-public crate-encdec-0.6.2 (c (n "encdec") (v "0.6.2") (d (list (d (n "encdec-base") (r "^0.6.0") (k 0)) (d (n "encdec-macros") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1ni9aqdipl2vvfda3r2kw8n8sr21v14qamkw1hw8wrp4b0352lc8") (f (quote (("std" "encdec-base/std") ("heapless" "encdec-base/heapless") ("alloc" "encdec-base/alloc"))))))

(define-public crate-encdec-0.7.0 (c (n "encdec") (v "0.7.0") (d (list (d (n "encdec-base") (r "=0.7.0") (k 0)) (d (n "encdec-macros") (r "=0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1bc6rljbdwfj5ppdibxrgynfzcr3bpblfc3bcjaj3w9n5nw8c264") (f (quote (("std" "encdec-base/std") ("heapless" "encdec-base/heapless") ("default" "std" "alloc" "heapless") ("alloc" "encdec-base/alloc"))))))

(define-public crate-encdec-0.7.1 (c (n "encdec") (v "0.7.1") (d (list (d (n "encdec-base") (r "=0.7.1") (k 0)) (d (n "encdec-macros") (r "=0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0wwidgdw97jqbbaqm7yjd4a49ml09lfi1g6h8j32j2rvbpfdwnj7") (f (quote (("std" "encdec-base/std") ("heapless" "encdec-base/heapless") ("default" "std" "alloc" "heapless") ("alloc" "encdec-base/alloc"))))))

(define-public crate-encdec-0.8.0 (c (n "encdec") (v "0.8.0") (d (list (d (n "encdec-base") (r "=0.8.0") (k 0)) (d (n "encdec-macros") (r "=0.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1h8vwz5n4rff59mb97vsmdj95rd35hpj5wh82y2ppy2wyy7q3b8s") (f (quote (("std" "encdec-base/std") ("heapless" "encdec-base/heapless") ("default" "std" "alloc" "heapless") ("alloc" "encdec-base/alloc"))))))

(define-public crate-encdec-0.8.1 (c (n "encdec") (v "0.8.1") (d (list (d (n "encdec-base") (r "=0.8.1") (k 0)) (d (n "encdec-macros") (r "=0.8.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "10cr22nig6f4m61kf7vp2nwg90mdmsv7bmwickrcvjcypi5fg8nd") (f (quote (("std" "encdec-base/std") ("nightly") ("heapless" "encdec-base/heapless") ("default" "std" "alloc" "heapless") ("alloc" "encdec-base/alloc"))))))

(define-public crate-encdec-0.8.2 (c (n "encdec") (v "0.8.2") (d (list (d (n "encdec-base") (r "=0.8.2") (k 0)) (d (n "encdec-macros") (r "=0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "12y1lizk1n88di5w06csc3w0w1gzfmybij4ss75s2nvnqpmh5fzv") (f (quote (("std" "encdec-base/std") ("nightly") ("heapless" "encdec-base/heapless") ("default" "std" "alloc" "heapless") ("alloc" "encdec-base/alloc"))))))

(define-public crate-encdec-0.8.3 (c (n "encdec") (v "0.8.3") (d (list (d (n "encdec-base") (r "=0.8.2") (k 0)) (d (n "encdec-macros") (r "=0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1qfxrplkgff64q9gp1r1h5d4y09zm4bi52rmwx266zljs7d08hha") (f (quote (("std" "encdec-base/std") ("nightly") ("heapless" "encdec-base/heapless") ("default" "std" "alloc" "heapless") ("alloc" "encdec-base/alloc"))))))

(define-public crate-encdec-0.9.0 (c (n "encdec") (v "0.9.0") (d (list (d (n "encdec-base") (r "=0.9.0") (k 0)) (d (n "encdec-macros") (r "=0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "08d4gcr869mzkhm8vgv0xmdysw4k4cih8wv5ghgmb1da1ghr9pi5") (f (quote (("std" "encdec-base/std") ("nightly") ("heapless" "encdec-base/heapless") ("default" "std" "alloc" "heapless") ("alloc" "encdec-base/alloc"))))))

