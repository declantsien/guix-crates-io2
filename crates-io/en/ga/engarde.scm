(define-module (crates-io en ga engarde) #:use-module (crates-io))

(define-public crate-engarde-0.1.1 (c (n "engarde") (v "0.1.1") (d (list (d (n "syntect") (r "^4.5.0") (f (quote ("parsing" "regex-fancy" "assets" "html" "dump-load-rs"))) (o #t) (k 0)))) (h "1kv427mq2nsbq9xbchxrzrvs1fnp9ziwfn47yz51m3wi2xpg485z") (f (quote (("syntax" "syntect") ("default"))))))

(define-public crate-engarde-0.1.2 (c (n "engarde") (v "0.1.2") (d (list (d (n "syntect") (r "^5.0.0") (f (quote ("parsing" "regex-fancy" "default-syntaxes" "default-themes" "html" "dump-load" "yaml-load"))) (o #t) (k 0)))) (h "0vdnpxfz8qgw4751hyzshn6fq21jf3pnzjyjrqxi004h470jjdl7") (f (quote (("syntax" "syntect") ("default"))))))

(define-public crate-engarde-0.1.3 (c (n "engarde") (v "0.1.3") (d (list (d (n "syntect") (r "^5.0.0") (f (quote ("parsing" "regex-fancy" "default-syntaxes" "default-themes" "html" "dump-load" "yaml-load"))) (o #t) (k 0)))) (h "1w3nqb0grk0frna86i5xv958d00dxx0si8f7cqnbq0xgmkl5xr7m") (f (quote (("syntax" "syntect") ("default"))))))

(define-public crate-engarde-0.1.4 (c (n "engarde") (v "0.1.4") (d (list (d (n "syntect") (r "^5.0.0") (f (quote ("parsing" "regex-fancy" "default-syntaxes" "default-themes" "html" "dump-load" "yaml-load"))) (o #t) (k 0)))) (h "0mr114z66c6csm873nk3z8fli60xpqwywbjzrrlfc3viv5gwz8al") (f (quote (("syntax" "syntect") ("default"))))))

(define-public crate-engarde-0.1.5 (c (n "engarde") (v "0.1.5") (d (list (d (n "syntect") (r "^5.0.0") (f (quote ("parsing" "regex-fancy" "default-syntaxes" "default-themes" "html" "dump-load" "yaml-load"))) (o #t) (k 0)))) (h "0m2cnqwn19p3kmrz5zrd2rg33d8vqnb0s6bdhs2a2xkfizha32pr") (f (quote (("syntax" "syntect") ("default"))))))

(define-public crate-engarde-0.1.6 (c (n "engarde") (v "0.1.6") (d (list (d (n "syntect") (r "^5.0.0") (f (quote ("parsing" "regex-fancy" "default-syntaxes" "default-themes" "html" "dump-load" "yaml-load"))) (o #t) (k 0)))) (h "065z20wb1xnbsxsk7xcwzwy46wi2f7vrv2k26pby9hf9kxwpcwmi") (f (quote (("syntax" "syntect") ("default")))) (r "1.64.0")))

(define-public crate-engarde-0.1.7 (c (n "engarde") (v "0.1.7") (d (list (d (n "syntect") (r "^5.1.0") (f (quote ("parsing" "regex-fancy" "default-syntaxes" "default-themes" "html" "dump-load" "yaml-load"))) (o #t) (k 0)))) (h "1j1nqf5l30c33hd1pwyp0lxhmdpy8as5jqhqv7652fn4rg7g376z") (f (quote (("syntax" "syntect") ("default")))) (r "1.73")))

(define-public crate-engarde-0.1.8 (c (n "engarde") (v "0.1.8") (d (list (d (n "syntect") (r "^5.2.0") (f (quote ("parsing" "regex-fancy" "default-syntaxes" "default-themes" "html" "dump-load" "yaml-load"))) (o #t) (k 0)))) (h "0k51aav81hk5k4rir5g2zmbxxb8af8rig5925dhp0cbpvmm5q85l") (f (quote (("syntax" "syntect") ("default")))) (r "1.76")))

