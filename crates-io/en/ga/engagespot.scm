(define-module (crates-io en ga engagespot) #:use-module (crates-io))

(define-public crate-engagespot-0.1.0 (c (n "engagespot") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "tokio") (r "^1.25.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "09c7jsygbxvsb1ddxm4h02s8an4a71ishbh679z6mi0k5k1s9nxq")))

