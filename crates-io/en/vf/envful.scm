(define-module (crates-io en vf envful) #:use-module (crates-io))

(define-public crate-envful-0.1.0 (c (n "envful") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "1sihmb5ifrh36w03yr1gkjdgwmk88hfgxrp1ac8w0nr8bp3f7z1c")))

(define-public crate-envful-0.2.0 (c (n "envful") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "005kp8m51p7xf0d7p1ngp0s6np54iba9qmb69q8qvph410qnmhi9")))

(define-public crate-envful-0.4.1 (c (n "envful") (v "0.4.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "1xccb3njdxxi5fhkfhifvrakc0gzp8lx0lz3wslg0ajjvb402slf")))

(define-public crate-envful-0.4.2 (c (n "envful") (v "0.4.2") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "1wzirhf4icgb0325gj1qycyfk8ax7z2p0dc4r64c7hv3bvm2085i")))

(define-public crate-envful-0.4.3 (c (n "envful") (v "0.4.3") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "10sdzi7rvz229gk7hh7805zzg67qmaxpzc6va8xvwrqifk7mslhi")))

(define-public crate-envful-0.4.4 (c (n "envful") (v "0.4.4") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "0i7b8a51w2xs3b4qsg45n6d80av6k8vl1ngpcbh18kscplrb8b7h")))

(define-public crate-envful-0.4.15 (c (n "envful") (v "0.4.15") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "0xglqk5yrwwaxvxxbjx6l96yal2sc32d0brm7id003rq4frrm4nh")))

(define-public crate-envful-0.4.16 (c (n "envful") (v "0.4.16") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "08v70xhb4jdrcicw4hsla7fcw1zhkhy9k0xh75b884yi75bb558h")))

(define-public crate-envful-0.4.17 (c (n "envful") (v "0.4.17") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "1jmr2mvzycjs0l49f0h31gwwgsc7p62fmq6dqdp7j0xl7lv3xdn5")))

(define-public crate-envful-0.4.18 (c (n "envful") (v "0.4.18") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "0gn5vq1r2yrx0rv36r01mhz6n5ghjvhh4633z8h9q6x2hi4b9l0i")))

(define-public crate-envful-0.4.22 (c (n "envful") (v "0.4.22") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "0q5nd6inpjjc6lr8iv0qbvsc1v4drwsmyyzs09qa5pm4y5vzxdm4")))

(define-public crate-envful-0.4.21 (c (n "envful") (v "0.4.21") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "1ardzc67fpbrrd3y97ccqna08w4x6gs3al33x4bl82x25x80zakn")))

(define-public crate-envful-0.5.0 (c (n "envful") (v "0.5.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "1nlwr4pxcglgxa75xalcqkpib06sl5wwr6v7xal0yc9c260yk5ib")))

(define-public crate-envful-0.5.1 (c (n "envful") (v "0.5.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "08k7kg4zxmkdd4cjkm7fsdvg2k60g1lqp61b6g1d5cjifcycmn5k")))

(define-public crate-envful-0.5.2 (c (n "envful") (v "0.5.2") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "1459nkgxkf16rd1jya8dj15flgax7rmpkg9g24igq79xxz8wfcz7")))

(define-public crate-envful-0.5.3 (c (n "envful") (v "0.5.3") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "1zna2pi2fx27h85zfjvhlyxgjns3qs3nbg5magv85ga8mkl134j2")))

(define-public crate-envful-0.5.4 (c (n "envful") (v "0.5.4") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "157rihsfsz8yp4qyczv90gy1w8zrv56g2cpw5m9y2vx63ypk5025")))

(define-public crate-envful-0.5.5 (c (n "envful") (v "0.5.5") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "1v84ll1jm2qbnnm7rnfv8laf8w3lmjr3s1q0m5v1f576x60qds87")))

(define-public crate-envful-0.5.6 (c (n "envful") (v "0.5.6") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "0lbixc4xnfxysyf6cjk32zhmqk1bf7r9qh8x5gdzvgmxdd3jiwji")))

(define-public crate-envful-0.5.7 (c (n "envful") (v "0.5.7") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "which") (r "^4.2.4") (d #t) (k 0)))) (h "1gmmgg5mglmkcfipk9zbx1rldkdrinw1sf6psf3gyknv4k9gdcc7")))

(define-public crate-envful-0.5.8 (c (n "envful") (v "0.5.8") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "which") (r "^4.2.4") (d #t) (k 0)))) (h "03rh8av4dsg7wgr1payqg47biirlbqg082jrvdsm6198b37qgidx")))

(define-public crate-envful-0.5.9 (c (n "envful") (v "0.5.9") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "which") (r "^4.2.4") (d #t) (k 0)))) (h "16sy2qxx9cm67846spinjgw4w659001jrxy87ahx610svi3qinnm")))

(define-public crate-envful-0.5.10 (c (n "envful") (v "0.5.10") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "which") (r "^4.2.4") (d #t) (k 0)))) (h "087py8sgw5fxgklv9rq6y7gw9rhjyqr94i9qkbihmj4ai748pylg")))

(define-public crate-envful-0.5.11 (c (n "envful") (v "0.5.11") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "which") (r "^4.2.4") (d #t) (k 0)))) (h "03x5sdq8rhgix1hi9fg77fhgicvp6z0pyfhd3rl8j46m1jzmv2bx")))

(define-public crate-envful-0.5.12 (c (n "envful") (v "0.5.12") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "which") (r "^4.2.4") (d #t) (k 0)))) (h "09d6rmjryn0lr5iwk93690kf0fk8jhl1j8gab2jfw409gbrqbkb7")))

(define-public crate-envful-1.0.0 (c (n "envful") (v "1.0.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "which") (r "^4.2.4") (d #t) (k 0)))) (h "1m24mdym0fg571lz2xsgkny7lc7blxxsxhhfv24q9hq2d5gs39lq")))

(define-public crate-envful-1.0.1 (c (n "envful") (v "1.0.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "which") (r "^4.2.4") (d #t) (k 0)))) (h "072636lccpz1d97cahcbh80ghxnq1f4m0g0la0j2zn10p4cp3f46")))

(define-public crate-envful-1.0.2 (c (n "envful") (v "1.0.2") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "which") (r "^4.2.4") (d #t) (k 0)))) (h "0ap2clx8narz5v4hij29v0hlfzbcflc0pxbfmf1vrqvy7vn4b7w1")))

(define-public crate-envful-1.0.3 (c (n "envful") (v "1.0.3") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "which") (r "^4.2.4") (d #t) (k 0)))) (h "0f8q2iynbnbh77fxqs07kbr78s5kbk98pxzzc9d0zgwyf2amrw40")))

(define-public crate-envful-1.0.4 (c (n "envful") (v "1.0.4") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "which") (r "^4.2.4") (d #t) (k 0)))) (h "1mjqm0b2iczl1ifjbj3kla8hizfn53jl5y3x14ljzvfvfcqv685k")))

(define-public crate-envful-1.0.6 (c (n "envful") (v "1.0.6") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "which") (r "^4.2.4") (d #t) (k 0)))) (h "0k4vnghv8nl2bcgdgi3pkq952h8skgb6vfzf5g6c2ia174d7fn0d")))

(define-public crate-envful-1.0.7 (c (n "envful") (v "1.0.7") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "which") (r "^4.2.4") (d #t) (k 0)))) (h "0r7rjmiscr246kppf5nq6ys4r6ssx0sxf2811s37w6fa3vsqcccv")))

