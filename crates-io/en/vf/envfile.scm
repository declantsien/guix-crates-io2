(define-module (crates-io en vf envfile) #:use-module (crates-io))

(define-public crate-envfile-0.1.0 (c (n "envfile") (v "0.1.0") (d (list (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0vf89zkvzlj78dk66q5xdan6wmykqfb32rws6sfj8hnnjis5yys1")))

(define-public crate-envfile-0.2.0 (c (n "envfile") (v "0.2.0") (d (list (d (n "snailquote") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "0m3lxdvk5gldmq1fmyq0a8zhvvbzgg4xqhwk4x2ccr50alm8fw5h")))

(define-public crate-envfile-0.2.1 (c (n "envfile") (v "0.2.1") (d (list (d (n "snailquote") (r "^0.2.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)))) (h "13000gsmlvyk04g4fg721yn7r0bbbn4vhx94cx0klh7wf6fzxrsz")))

