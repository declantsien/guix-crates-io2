(define-module (crates-io en vf envfury) #:use-module (crates-io))

(define-public crate-envfury-0.0.0 (c (n "envfury") (v "0.0.0") (h "05kj3vq4phhism6ry5wc8m2la6g8az5203abx2xmggvmvmf3db6d")))

(define-public crate-envfury-0.1.0 (c (n "envfury") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "15r69y1wd4xzarkr46i8va4djl6gfd51k765k476gp7jmxrkfjla")))

(define-public crate-envfury-0.1.1 (c (n "envfury") (v "0.1.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03yz0zvnrvf7javq6cdw2b3q9jdkzhh6wypmiqxcvigjq75d7f4d")))

(define-public crate-envfury-0.2.0 (c (n "envfury") (v "0.2.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0vwv83hk8iw7n6sihvn9hdx9nj5b8z50shf5h99l7mp0bvcqjacr")))

