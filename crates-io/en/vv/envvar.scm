(define-module (crates-io en vv envvar) #:use-module (crates-io))

(define-public crate-envvar-0.1.0 (c (n "envvar") (v "0.1.0") (h "0ncininqvqwpqv0hhqc0n3g1pfq39m1rk7q0x8xzca70svhzp1mf") (y #t)))

(define-public crate-envvar-0.1.1 (c (n "envvar") (v "0.1.1") (h "0p3l9qcphmsmg48wjbx25idarjmd7i6sw50wnp24nkk37kixgslg")))

(define-public crate-envvar-0.1.2 (c (n "envvar") (v "0.1.2") (h "1j5j3ai0xq8crp962059mjqqd6cmkhdys257134j85lgpdyngbic")))

(define-public crate-envvar-0.1.3 (c (n "envvar") (v "0.1.3") (h "1wfyxcxrmznd5zxkka7744zvb314lw5gsmi9fn23l7kj01dpg1mx")))

