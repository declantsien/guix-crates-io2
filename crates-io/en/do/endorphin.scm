(define-module (crates-io en do endorphin) #:use-module (crates-io))

(define-public crate-endorphin-0.1.2 (c (n "endorphin") (v "0.1.2") (d (list (d (n "atomic") (r "^0.5.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (f (quote ("rayon" "raw"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (d #t) (k 0)))) (h "04rc12rzhrcbqa52v63yjwplmaglyf3m3gysmwsv55mp13zjwk11") (y #t)))

(define-public crate-endorphin-0.1.3 (c (n "endorphin") (v "0.1.3") (d (list (d (n "atomic") (r "^0.5.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (f (quote ("rayon" "raw"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (d #t) (k 0)))) (h "0x0bj0rdsds7vhbic5k9bilqll3ww33i52lqmi7qs3wqsna12wfz") (y #t)))

(define-public crate-endorphin-0.1.4 (c (n "endorphin") (v "0.1.4") (d (list (d (n "atomic") (r "^0.5.1") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (f (quote ("rayon" "raw"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)) (d (n "smallvec") (r "^1.7.0") (d #t) (k 0)))) (h "13cxgw6rp9w2zafmsd4hv8vl6ymrkl75jl9nbidr1z248qs9sm39") (y #t)))

(define-public crate-endorphin-0.1.5 (c (n "endorphin") (v "0.1.5") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (f (quote ("raw"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)))) (h "14zbw14ysp057yc8kznyaidz2x3nnvpaj2qkrs6rpvm1iahii0z2") (y #t)))

(define-public crate-endorphin-0.1.6 (c (n "endorphin") (v "0.1.6") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (f (quote ("raw"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)))) (h "1d7d1msmlw64zdyjk7dw94d2yni16ij6dn3l8pijx2qg6a66sr42") (y #t)))

(define-public crate-endorphin-0.1.7 (c (n "endorphin") (v "0.1.7") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (f (quote ("raw"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)))) (h "001m9wd42r8p6hlankck8kjvip10l2894snj5285k9w7cikyr48d") (y #t)))

(define-public crate-endorphin-0.1.8 (c (n "endorphin") (v "0.1.8") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (f (quote ("raw"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)))) (h "0kgcnz8nb7lwprzcjhmsf30mii9f6b30i98k4p3h8alva83jfx7m")))

(define-public crate-endorphin-0.1.9 (c (n "endorphin") (v "0.1.9") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.11.2") (f (quote ("raw"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.6") (d #t) (k 0)))) (h "1g9y2zl3d4xcy6dfxi9w3damcay000c45dl7j87c5qpy5v38gp72")))

