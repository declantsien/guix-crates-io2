(define-module (crates-io en ca encap_enum) #:use-module (crates-io))

(define-public crate-encap_enum-0.1.0 (c (n "encap_enum") (v "0.1.0") (h "12skz4lqz5g7bicpjcr1yjikmvq1xqk134zxs866z4n35xk8mmas") (y #t)))

(define-public crate-encap_enum-0.1.1 (c (n "encap_enum") (v "0.1.1") (h "0cmnagqirygvq5lcp7pn0hgyhi140wzdvp9h58p3x69ds1c1g116") (y #t)))

(define-public crate-encap_enum-0.1.2 (c (n "encap_enum") (v "0.1.2") (h "0fbcz3ms48x9ni50l4pkzyjav0r22nc1dmiz7ypvmza67sa8i123") (y #t)))

(define-public crate-encap_enum-0.1.3 (c (n "encap_enum") (v "0.1.3") (h "1qnwjp8zbkcr1mxm2k51gky0pr31pk1sfx19c5bpl84lx64kmq7b") (y #t)))

(define-public crate-encap_enum-0.1.4 (c (n "encap_enum") (v "0.1.4") (h "0i8g5j4zi7kza79fkvwwci1ak9adaqddbvgab6wvhqpy8mzcp8sq") (y #t)))

(define-public crate-encap_enum-0.1.5 (c (n "encap_enum") (v "0.1.5") (h "0brxbk5gpis3wsh094dwqby0xyz6bdrc43v5a5vnmyf1rmpyf70y") (y #t)))

(define-public crate-encap_enum-0.2.0 (c (n "encap_enum") (v "0.2.0") (h "011n92l2vxh8ih9l5d62bkny7272niygzbi1l57f5jnhsyphqiqa") (y #t)))

(define-public crate-encap_enum-0.2.1 (c (n "encap_enum") (v "0.2.1") (h "00ga8z0krlzdnk55p5ac0mjzij56xaf6dgpiysml9qk5s18qh0is") (y #t)))

(define-public crate-encap_enum-0.3.0 (c (n "encap_enum") (v "0.3.0") (h "0ssww4if0hw7ls45p1p9z38hi6z6p0c0jigr249098a2yzk4208l")))

(define-public crate-encap_enum-0.3.1 (c (n "encap_enum") (v "0.3.1") (h "0gqc97q4i1i6d817rd2f58lgy876vdfy0kj5ymmviff2vkrls7hh")))

