(define-module (crates-io en ca encase_derive_impl) #:use-module (crates-io))

(define-public crate-encase_derive_impl-0.2.0 (c (n "encase_derive_impl") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "0gs7qsvw1khcq2c3wbldvjjvcbsnmkwfhr88mvwidlkq3zw2q8rn")))

(define-public crate-encase_derive_impl-0.2.1 (c (n "encase_derive_impl") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "1hbx78x9mxjza4zri17fin7jv9kird7gx0bhlv4rga0yismdxrw6")))

(define-public crate-encase_derive_impl-0.3.0 (c (n "encase_derive_impl") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "1ky26a0xikhrkcdbshz5g4sh30dxkrq6z8f2s94gax6razj4sbqg")))

(define-public crate-encase_derive_impl-0.4.0 (c (n "encase_derive_impl") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "0gv66y9qvhplf0y8xivrczn90z5ga1ajpphdfgfpv2csifsam6dj")))

(define-public crate-encase_derive_impl-0.4.1 (c (n "encase_derive_impl") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "1vjpjqxxpmcylc0aydwcvivnq3y5hxnwr09va6bh5ss2x4wvc9zc")))

(define-public crate-encase_derive_impl-0.5.0 (c (n "encase_derive_impl") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "14qnpqla4fzz3bbwwkzymxli8d7vmh2nm1jh2bflqg4zjmc9sj5f")))

(define-public crate-encase_derive_impl-0.6.0 (c (n "encase_derive_impl") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0ham0b0zz2s4kixx264500ql15v34wg382nx4pp8w1y8kas6bg2x")))

(define-public crate-encase_derive_impl-0.6.1 (c (n "encase_derive_impl") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0z73j0dpglhyzixpk9kbnp08zjp5iznwz4gs8m519mhzhn7mdqiz")))

(define-public crate-encase_derive_impl-0.7.0 (c (n "encase_derive_impl") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.1") (d #t) (k 0)))) (h "1z7ghsx8f9fii2h0hxwfgb56j5mzqf1vbiz8i8xs3shking9m5cj")))

(define-public crate-encase_derive_impl-0.8.0 (c (n "encase_derive_impl") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.1") (d #t) (k 0)))) (h "0iwvxsck2dpyh50ygs5rflfvbdrc47z8fylhkwrlss23jyyxncgx")))

