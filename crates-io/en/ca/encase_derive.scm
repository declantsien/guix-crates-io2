(define-module (crates-io en ca encase_derive) #:use-module (crates-io))

(define-public crate-encase_derive-0.1.0 (c (n "encase_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "0spglqinmpxwqqvd6zl19ll0xf24fvd5xbi8ck597nn5v3xwqbq9")))

(define-public crate-encase_derive-0.1.1 (c (n "encase_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "1m82nn07aq8xvxs9iplaydig4kq4z4f4n4ls3rvagwwwsxyasszj")))

(define-public crate-encase_derive-0.1.2 (c (n "encase_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "0j08wagbi84495cb453087zz8ipi40cyhmslv0776nj4pzpqp4rr")))

(define-public crate-encase_derive-0.1.3 (c (n "encase_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.56") (d #t) (k 0)))) (h "1pkb38akkmggax9mlpp34nrhj5qq5i8zwzfz856pvr0k4yma6bih")))

(define-public crate-encase_derive-0.2.0 (c (n "encase_derive") (v "0.2.0") (d (list (d (n "encase_derive_impl") (r "=0.2.0") (d #t) (k 0)))) (h "0mqwxhczh71q3sfd27l1xqqg0l6vqrgmfj4f3xwkfyqjd9pgfvnl")))

(define-public crate-encase_derive-0.2.1 (c (n "encase_derive") (v "0.2.1") (d (list (d (n "encase_derive_impl") (r "=0.2.1") (d #t) (k 0)))) (h "04h60h0sjqh93miffqmzwc22xfy7mcgwf2j45s7d497s74ascapn")))

(define-public crate-encase_derive-0.3.0 (c (n "encase_derive") (v "0.3.0") (d (list (d (n "encase_derive_impl") (r "=0.3.0") (d #t) (k 0)))) (h "08lgazm4xif0rpmnm87aaqgi5miwjsjs6bdvy8lmycga5r0h5f7m")))

(define-public crate-encase_derive-0.4.0 (c (n "encase_derive") (v "0.4.0") (d (list (d (n "encase_derive_impl") (r "=0.4.0") (d #t) (k 0)))) (h "1lq526canhbrkc2vp9xqc6s88dc12vqnnrb4l6hc49i4l1zcs1nz")))

(define-public crate-encase_derive-0.4.1 (c (n "encase_derive") (v "0.4.1") (d (list (d (n "encase_derive_impl") (r "=0.4.1") (d #t) (k 0)))) (h "14lyawwq6v8wsh146g54pp97351jmsi639299x3kqs672jbkxafx")))

(define-public crate-encase_derive-0.5.0 (c (n "encase_derive") (v "0.5.0") (d (list (d (n "encase_derive_impl") (r "=0.5.0") (d #t) (k 0)))) (h "1jds7bnwhw2wf7x89w50sradx9b3bxlzlvv115cnz2qzxpgac7ag")))

(define-public crate-encase_derive-0.6.0 (c (n "encase_derive") (v "0.6.0") (d (list (d (n "encase_derive_impl") (r "=0.6.0") (d #t) (k 0)))) (h "0q8icwc4nrggbcbb3ld8ib9zqwnjkk5v37y7zq497vfllnw1q52l")))

(define-public crate-encase_derive-0.6.1 (c (n "encase_derive") (v "0.6.1") (d (list (d (n "encase_derive_impl") (r "=0.6.1") (d #t) (k 0)))) (h "03jaanhkz0mj5zl8mph3k00ngr3cq0z5fqbz176ggx6b13g0qlhf")))

(define-public crate-encase_derive-0.7.0 (c (n "encase_derive") (v "0.7.0") (d (list (d (n "encase_derive_impl") (r "=0.7.0") (d #t) (k 0)))) (h "0anp67afadnjz3bh35p2vjl205nq18ak3lmbq1nbm7niqx4i9kpl")))

(define-public crate-encase_derive-0.8.0 (c (n "encase_derive") (v "0.8.0") (d (list (d (n "encase_derive_impl") (r "=0.8.0") (d #t) (k 0)))) (h "046wrnlv4v7z73a09a06cbgzgqdjay4mj3wlnqnzxcdyngn9vq07")))

