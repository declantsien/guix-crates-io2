(define-module (crates-io en s- ens-rest-client) #:use-module (crates-io))

(define-public crate-ens-rest-client-0.1.0 (c (n "ens-rest-client") (v "0.1.0") (d (list (d (n "ethereum-types") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^2.3") (d #t) (k 0)))) (h "0g5ggfi1zf1lwixc3fdrd7clli9rhpf422giqm5a4pls2l4a8l84")))

