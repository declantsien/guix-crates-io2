(define-module (crates-io en de enderpy) #:use-module (crates-io))

(define-public crate-enderpy-0.1.0 (c (n "enderpy") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "enderpy_python_parser") (r "^0.1.0") (d #t) (k 0)) (d (n "enderpy_python_type_checker") (r "^0.1.0") (d #t) (k 0)))) (h "08mb0ji478q9i1g94iz44mwcznbnk3dim7a4a6mm5mymn153bk4p") (r "1.72.0")))

