(define-module (crates-io en de endecode) #:use-module (crates-io))

(define-public crate-endecode-0.1.0 (c (n "endecode") (v "0.1.0") (d (list (d (n "endecode-derive") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0hcaqhw1qiw338dng4mi517rph7b5x4lxzzj1na9762fpqin5c9k") (f (quote (("derive" "endecode-derive"))))))

(define-public crate-endecode-0.2.0 (c (n "endecode") (v "0.2.0") (d (list (d (n "endecode-derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0h6hdav2ca2z8mdp2cp9gnyy6apgrznz0j8pcbwawby18fql5zwn") (f (quote (("derive" "endecode-derive"))))))

