(define-module (crates-io en de enderpy_python_parser) #:use-module (crates-io))

(define-public crate-enderpy_python_parser-0.1.0 (c (n "enderpy_python_parser") (v "0.1.0") (d (list (d (n "insta") (r "^1.28.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "miette") (r "^5.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)) (d (n "unicode-id-start") (r "^1.0.3") (d #t) (k 0)))) (h "1ak7ws574l55fij3xx7r4xi3a4mf1syi67kvl899mw74pdfssdbn") (r "1.72.0")))

