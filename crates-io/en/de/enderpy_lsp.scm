(define-module (crates-io en de enderpy_lsp) #:use-module (crates-io))

(define-public crate-enderpy_lsp-0.1.0 (c (n "enderpy_lsp") (v "0.1.0") (d (list (d (n "dashmap") (r "^5.1.0") (d #t) (k 0)) (d (n "enderpy_python_parser") (r "^0.1.0") (d #t) (k 0)) (d (n "enderpy_python_type_checker") (r "^0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "im-rc") (r "^15.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-lsp") (r "^0.19.0") (f (quote ("proposed"))) (d #t) (k 0)))) (h "1bn1lmwmydjylh6gxnj2096h4v54pzi7w523sbbphrrswgwrw3il") (r "1.72.0")))

