(define-module (crates-io en de enderpy_python_type_checker) #:use-module (crates-io))

(define-public crate-enderpy_python_type_checker-0.1.0 (c (n "enderpy_python_type_checker") (v "0.1.0") (d (list (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "enderpy_python_parser") (r "^0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "insta") (r "^1.28.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "miette") (r "^5.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)))) (h "1vmiak1k7b61xz9633646vqaahiiq01yrsz1wbzjpmlf88gvlggr") (r "1.72.0")))

