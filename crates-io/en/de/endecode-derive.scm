(define-module (crates-io en de endecode-derive) #:use-module (crates-io))

(define-public crate-endecode-derive-0.1.0 (c (n "endecode-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "0a8kybqcyjfddc0g5p9h963rq4m8c087brfxjj1jz9azwdrgi6mc")))

(define-public crate-endecode-derive-0.2.0 (c (n "endecode-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)))) (h "1yzlqvrgcg1mv8s2ylgj7z0cw3s8wxw2mz204x5zgm8cmafld9mp")))

