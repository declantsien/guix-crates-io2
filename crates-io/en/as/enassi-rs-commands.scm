(define-module (crates-io en as enassi-rs-commands) #:use-module (crates-io))

(define-public crate-enassi-rs-commands-1.0.0 (c (n "enassi-rs-commands") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "enassi-rs-encrypt") (r "^1.0") (d #t) (k 0)) (d (n "enassi-rs-utils") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0i6mgnq3jqhdzg52wvimwbawjiy86vdhhkc164xvvzh00cyzjrr3")))

