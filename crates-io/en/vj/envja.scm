(define-module (crates-io en vj envja) #:use-module (crates-io))

(define-public crate-envja-0.1.0 (c (n "envja") (v "0.1.0") (d (list (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1l35m7n5k684f2ilvdd782idfi10kfw4v4wgxfpm9jx6k0ci58v2")))

(define-public crate-envja-0.2.0 (c (n "envja") (v "0.2.0") (d (list (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0q82n4a31r6x6s7xm5nc4fvsx44dx97bbv0cqjp87lp4s4g0pz5n")))

