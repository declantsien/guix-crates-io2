(define-module (crates-io en vj envja-cli) #:use-module (crates-io))

(define-public crate-envja-cli-0.1.0 (c (n "envja-cli") (v "0.1.0") (d (list (d (n "envja") (r "^0.1.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "02zxbazdq0x30hx3k12ifcf9ymr0lhjm1626s9vvv9x2mgfqkm7w")))

(define-public crate-envja-cli-0.2.0 (c (n "envja-cli") (v "0.2.0") (d (list (d (n "envja") (r "^0.2.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "09nnghl8nsryz7l389179d5b1gkwbw02z8i7r3h9gm658ya8m0yc")))

