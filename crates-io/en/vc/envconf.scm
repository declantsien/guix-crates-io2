(define-module (crates-io en vc envconf) #:use-module (crates-io))

(define-public crate-envconf-0.1.0 (c (n "envconf") (v "0.1.0") (d (list (d (n "envconf_derive") (r "^0.1") (d #t) (k 0)))) (h "140cw0vkd96355np3bnwkan1f18ipqpzyngsiisk31936y79yn47")))

(define-public crate-envconf-0.1.1 (c (n "envconf") (v "0.1.1") (d (list (d (n "envconf_derive") (r "^0.1") (d #t) (k 0)))) (h "0hm5kwr17b20nfp34rsdjgixa9asj320cdsjk7jv537nxa021wgs")))

