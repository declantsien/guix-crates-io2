(define-module (crates-io en vc envch) #:use-module (crates-io))

(define-public crate-envch-0.1.0 (c (n "envch") (v "0.1.0") (d (list (d (n "shellexpand") (r "^2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1wp4njs9x32i0yq3v3df44m793q0xqrklcx7m7dilbdgdr12scls")))

(define-public crate-envch-0.1.1 (c (n "envch") (v "0.1.1") (d (list (d (n "shellexpand") (r "^2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "09d8rr8vcna6xj7ylj8mlxw2mj14iyifdkcsbihg1kqz4klhjz2z")))

