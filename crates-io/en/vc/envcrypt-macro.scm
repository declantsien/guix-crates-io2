(define-module (crates-io en vc envcrypt-macro) #:use-module (crates-io))

(define-public crate-envcrypt-macro-0.1.0 (c (n "envcrypt-macro") (v "0.1.0") (d (list (d (n "magic-crypt") (r "^3.1.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0r860jpym1ql0l7b97v26bj341m6sfysbd3zp2ir1vkrklmdg4vf")))

(define-public crate-envcrypt-macro-0.2.0 (c (n "envcrypt-macro") (v "0.2.0") (d (list (d (n "magic-crypt") (r "^3.1.10") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1fgfxzqmv4sk67rcy3f2rdfpj8km77cjpnj0bxz7kbpayh24d7ds")))

(define-public crate-envcrypt-macro-0.2.1 (c (n "envcrypt-macro") (v "0.2.1") (d (list (d (n "magic-crypt") (r "^3.1.10") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "15861xga0lxxp1qbc468j573h83ii3p7dhwsfgazf88bjhl6rpr8")))

(define-public crate-envcrypt-macro-0.3.0 (c (n "envcrypt-macro") (v "0.3.0") (d (list (d (n "chacha20poly1305") (r "^0.10.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)))) (h "0a5vgaslyy6jfqa2ahcnha78f05l5x1g79bxg0w4wksya9368lbk") (y #t)))

(define-public crate-envcrypt-macro-0.3.1 (c (n "envcrypt-macro") (v "0.3.1") (d (list (d (n "chacha20poly1305") (r "^0.10.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)))) (h "07p5jh0glm3x8bzsyg6p2wwppvsgdkc3l5dqnx0flzi3fm3c8wi3") (y #t)))

(define-public crate-envcrypt-macro-0.3.2 (c (n "envcrypt-macro") (v "0.3.2") (d (list (d (n "chacha20poly1305") (r "^0.10.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)))) (h "0a2b93f87zwngw8v133a839rsblb5a8cwy9134f7698hlaav4mms") (y #t)))

(define-public crate-envcrypt-macro-0.4.0 (c (n "envcrypt-macro") (v "0.4.0") (d (list (d (n "chacha20poly1305") (r "^0.10.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)))) (h "1csyr321xl9wrk8z41fm18drcwp4344k8kaz5d42l3fnqhmhgbd4") (y #t)))

(define-public crate-envcrypt-macro-0.5.0 (c (n "envcrypt-macro") (v "0.5.0") (d (list (d (n "chacha20poly1305") (r "^0.10.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)))) (h "1ps2sk9wrrbllp4xc12233ghal2lklchp0bsfx48s2yl9m7n2cs1")))

