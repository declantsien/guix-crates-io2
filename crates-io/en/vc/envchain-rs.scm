(define-module (crates-io en vc envchain-rs) #:use-module (crates-io))

(define-public crate-envchain-rs-0.1.2 (c (n "envchain-rs") (v "0.1.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "rpassword") (r "^7") (d #t) (k 0)) (d (n "secret-service") (r "^3") (f (quote ("rt-tokio-crypto-rust"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1afdbaf0qk8s29688hl2kl808zpm83ndxqka2p2pbvpy1ac2f79b")))

(define-public crate-envchain-rs-0.1.3 (c (n "envchain-rs") (v "0.1.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "rpassword") (r "^7") (d #t) (k 0)) (d (n "secret-service") (r "^3") (f (quote ("rt-tokio-crypto-rust"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "10hp8b351bh9w6c3iwf27anh2r9lfy0y3jdm17pxw2g5768bg5gg")))

(define-public crate-envchain-rs-0.1.4 (c (n "envchain-rs") (v "0.1.4") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "rpassword") (r "^7") (d #t) (k 0)) (d (n "secret-service") (r "^3") (f (quote ("rt-tokio-crypto-rust"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1gh0cpwbkrzrv185dxdrz2py3z2q294h00mv2gc87vzhryyaylv5")))

(define-public crate-envchain-rs-0.1.5 (c (n "envchain-rs") (v "0.1.5") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "rpassword") (r "^7") (d #t) (k 0)) (d (n "secret-service") (r "^3") (f (quote ("rt-tokio-crypto-rust"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1pl1jfmc3qr1kd9ypimw6nzq4l6glfh9x450fdd9h3pfmc6rw7wg")))

