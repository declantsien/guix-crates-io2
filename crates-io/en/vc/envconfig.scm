(define-module (crates-io en vc envconfig) #:use-module (crates-io))

(define-public crate-envconfig-0.1.0 (c (n "envconfig") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "01l1i9s4bw555jc6wbf7b5gbjmy1wzwkips3jxp4s0f8knbdjw77")))

(define-public crate-envconfig-0.2.0 (c (n "envconfig") (v "0.2.0") (d (list (d (n "envconfig_derive") (r "^0.2.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "0k44kflp84y62h9wnqgkj948nqavd7a45vl9zbvpg70kjqirzw6y")))

(define-public crate-envconfig-0.3.0 (c (n "envconfig") (v "0.3.0") (d (list (d (n "envconfig_derive") (r "^0.3.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "17nrskarqz7icjrl09ydpiaplgc6wvcw9vgwdwybpvrzwa8jffxw")))

(define-public crate-envconfig-0.4.0 (c (n "envconfig") (v "0.4.0") (d (list (d (n "envconfig_derive") (r "^0.4.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "1a1j0b0v8x26iq1ywag4gbfywpszd3q1p2bd571ri52c2cp3jllg")))

(define-public crate-envconfig-0.5.0 (c (n "envconfig") (v "0.5.0") (d (list (d (n "envconfig_derive") (r "^0.5.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "0cbfbddz7ibikalsi0y2ahg9jiick390xbq3q9lqknr8admz8q59")))

(define-public crate-envconfig-0.5.1 (c (n "envconfig") (v "0.5.1") (d (list (d (n "envconfig_derive") (r "^0.5.1") (d #t) (k 2)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "0ibq27xb5l60fd7yffcm20v8q435qnbafgcin25rs4viygwr6hp8")))

(define-public crate-envconfig-0.6.0 (c (n "envconfig") (v "0.6.0") (d (list (d (n "envconfig_derive") (r "^0.6.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "0wb5d1d2c0ryc1a7y63pc77a8531nx9yy735w66z5rpd7mlq7b3w")))

(define-public crate-envconfig-0.7.0 (c (n "envconfig") (v "0.7.0") (d (list (d (n "envconfig_derive") (r "^0.7.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "1p3b6kvmh7g8gr05ba62lllj25ib768gj5270w44dkydqbcb76ia")))

(define-public crate-envconfig-0.8.0 (c (n "envconfig") (v "0.8.0") (d (list (d (n "envconfig_derive") (r "^0.8.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "0pcg22r1g7q6dp9s9bwpspql6pcsngs8rh7w9p3y7h60pp35niya")))

(define-public crate-envconfig-0.9.0 (c (n "envconfig") (v "0.9.0") (d (list (d (n "envconfig_derive") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "1swgvx4nl1il1waqwbcgakg56xwrw9lwf4vh0p9ilgps5a4f60g8")))

(define-public crate-envconfig-0.9.1 (c (n "envconfig") (v "0.9.1") (d (list (d (n "envconfig_derive") (r "^0.9.1") (d #t) (k 0)))) (h "1jmr39b9iw8zkrc33xsqywr0kcpi29fms5xb2nap02d91m3z03iw")))

(define-public crate-envconfig-0.10.0 (c (n "envconfig") (v "0.10.0") (d (list (d (n "envconfig_derive") (r "^0.10.0") (d #t) (k 0)))) (h "16xcpij5brxj4k5lf0x566zbzl3q9681cs7v3sdrsnpm45zcr0ga")))

