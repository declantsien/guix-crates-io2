(define-module (crates-io en vc envcrypt) #:use-module (crates-io))

(define-public crate-envcrypt-0.1.0 (c (n "envcrypt") (v "0.1.0") (d (list (d (n "envcrypt-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "magic-crypt") (r "^3.1.10") (d #t) (k 0)))) (h "0ny36m80b4z15gakgbqzrs7qb9ggsvx6ff7flh3s1m6hm0dizmjb")))

(define-public crate-envcrypt-0.2.0 (c (n "envcrypt") (v "0.2.0") (d (list (d (n "dotenvy") (r "^0.15.3") (d #t) (k 2)) (d (n "envcrypt-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "magic-crypt") (r "^3.1.10") (d #t) (k 0)))) (h "0jq2r26y6plqyaac6k9iv6c0mj4p159s2v8srrgr6qlgw0kidis4")))

(define-public crate-envcrypt-0.2.1 (c (n "envcrypt") (v "0.2.1") (d (list (d (n "dotenvy") (r "^0.15.3") (d #t) (k 2)) (d (n "envcrypt-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "magic-crypt") (r "^3.1.10") (d #t) (k 0)))) (h "1q8qmcr8qnq0zd1xqbdjd2imdjvv0r2i4878n4crndrsm06njy4w")))

(define-public crate-envcrypt-0.3.0 (c (n "envcrypt") (v "0.3.0") (d (list (d (n "chacha20poly1305") (r "^0.10.1") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.3") (d #t) (k 2)) (d (n "envcrypt-macro") (r "^0.3.0") (d #t) (k 0)))) (h "07x51rzzpfxh0hs8a064d360ggv88j33jp1jbszsxw3wsmx7y7pn") (y #t)))

(define-public crate-envcrypt-0.3.1 (c (n "envcrypt") (v "0.3.1") (d (list (d (n "chacha20poly1305") (r "^0.10.1") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.3") (d #t) (k 2)) (d (n "envcrypt-macro") (r "^0.3.0") (d #t) (k 0)))) (h "06ndpgjf79pwwygg7nm4c1g2r6pb1263lza997qh9by78b76n2r4") (y #t)))

(define-public crate-envcrypt-0.3.2 (c (n "envcrypt") (v "0.3.2") (d (list (d (n "chacha20poly1305") (r "^0.10.1") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.3") (d #t) (k 2)) (d (n "envcrypt-macro") (r "^0.3.0") (d #t) (k 0)))) (h "1s871ynl0qbnkh1pr3rv0ym4fdi90bb2bh9xcmvdflkf007i4wr6") (y #t)))

(define-public crate-envcrypt-0.4.0 (c (n "envcrypt") (v "0.4.0") (d (list (d (n "chacha20poly1305") (r "^0.10.1") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.3") (d #t) (k 2)) (d (n "envcrypt-macro") (r "^0.4.0") (d #t) (k 0)))) (h "0igldbhmkcagbffyv00j3lsjhrxk5jsrf9sldmwhlm3ambbv7avk") (y #t)))

(define-public crate-envcrypt-0.5.0 (c (n "envcrypt") (v "0.5.0") (d (list (d (n "chacha20poly1305") (r "^0.10.1") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.3") (d #t) (k 2)) (d (n "envcrypt-macro") (r "^0.5.0") (d #t) (k 0)))) (h "0zl50h0dc6r21cz40zlv2dly0zym027fvf24kmjyfa9qd49wvrg6")))

