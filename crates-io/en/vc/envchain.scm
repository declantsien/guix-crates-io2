(define-module (crates-io en vc envchain) #:use-module (crates-io))

(define-public crate-envchain-0.1.0 (c (n "envchain") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "rpassword") (r "^7") (d #t) (k 0)) (d (n "secret-service") (r "^3") (f (quote ("rt-tokio-crypto-rust"))) (d #t) (k 0)))) (h "0lb979a3qk6vyhskiynhvhsnvhjwg7l5ly3n4nw5ab286asrb3ll")))

