(define-module (crates-io en vc envcache) #:use-module (crates-io))

(define-public crate-envcache-0.1.0 (c (n "envcache") (v "0.1.0") (h "0qalfwpb8a0swhdlj80xcv2yk3sw38v1lqz41bb83y6xyzqvcdl9")))

(define-public crate-envcache-0.1.1 (c (n "envcache") (v "0.1.1") (h "0h9s2nl3lb8y3dwggbms02zrkrjb2hc8zkwn6h90wayvdg5j65rq")))

(define-public crate-envcache-0.1.2 (c (n "envcache") (v "0.1.2") (h "14681k3sxx4ijw2idzcfh0yilybpjacm7yak4hgisy0ry5acwzf6")))

(define-public crate-envcache-0.1.3 (c (n "envcache") (v "0.1.3") (h "0fvfib9nj3q092vb9q1gsiq60k3m3fcifzd77by39m6yavyv7hxn")))

