(define-module (crates-io en vc envctl) #:use-module (crates-io))

(define-public crate-envctl-1.0.0 (c (n "envctl") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "combine") (r "^4.6.6") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12h117rkvf6d8qjvywjk8j5rc2qzpa32izcxxm4jwagz5ixawyzk")))

(define-public crate-envctl-1.1.0 (c (n "envctl") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "combine") (r "^4.6.6") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03c3k7wjnmqs85p639mwfhis6gr1nx7nnnw063nirhbjsj3bdkrl")))

