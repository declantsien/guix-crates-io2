(define-module (crates-io en vc envconfig_derive) #:use-module (crates-io))

(define-public crate-envconfig_derive-0.2.0 (c (n "envconfig_derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0g9mnq6w8k30ppp1nm2yrn8n5zk2y277jx6ixi8ncvb7pbi3hc7s")))

(define-public crate-envconfig_derive-0.3.0 (c (n "envconfig_derive") (v "0.3.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1lq773f57zk4p20v7czb4ckp0nxvb5ygg1n563d2dhmshk9s2lxq")))

(define-public crate-envconfig_derive-0.4.0 (c (n "envconfig_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "166733qsxv8fgjz9jyp2rhf0l262h2ai8y6855v3iidrp0693vkx")))

(define-public crate-envconfig_derive-0.5.0 (c (n "envconfig_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "0i9f3953ngl8jsz4x25425vgfgvsnysf355jy99yapj3a6lppdmw")))

(define-public crate-envconfig_derive-0.5.1 (c (n "envconfig_derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "0a32i326d0ki32jmm21x0951ayz7b5r3c0pi3vwwsik2llswzxiq")))

(define-public crate-envconfig_derive-0.6.0 (c (n "envconfig_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "0x26a8fkpprqadf2zfxb5r8x3ziw17afn38szjxswa3pvvwh3a7h")))

(define-public crate-envconfig_derive-0.7.0 (c (n "envconfig_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^0.4.19") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.4") (d #t) (k 0)))) (h "0ma9z2g19rdk52gqvk83gkymw6f7bhkxm55kjz815llwjd4ll35r")))

(define-public crate-envconfig_derive-0.8.0 (c (n "envconfig_derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "05rgpdrv1n3hg6arqp96qzp7kli0ylk9rff5717v9k93l33wxdhb")))

(define-public crate-envconfig_derive-0.9.0 (c (n "envconfig_derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "0dr4jr42m98sq5905l5zsvngysgfx55zy5ijn087cqfb65d9j5wq")))

(define-public crate-envconfig_derive-0.9.1 (c (n "envconfig_derive") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "0h1lsxd8ybgx9rwi8wk8a56hdrpa3f8h29dw7mk2a3g7cq9m39rw")))

(define-public crate-envconfig_derive-0.10.0 (c (n "envconfig_derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "1gxr0kc2swhvix5w52wrjs7g20gsxdcggbyak98lajzqwmwa5z3x")))

