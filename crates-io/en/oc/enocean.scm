(define-module (crates-io en oc enocean) #:use-module (crates-io))

(define-public crate-enocean-0.1.0 (c (n "enocean") (v "0.1.0") (d (list (d (n "serialport") (r "^3.2.0") (d #t) (k 0)))) (h "1a9k6jam7g6n2a21f9mbpw4h9cyq417bq45wwh9i8kqmq4syy9jd")))

(define-public crate-enocean-0.2.0 (c (n "enocean") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "serialport") (r "^3.2.0") (d #t) (k 0)))) (h "0szi5x5wczcq92i0xnz1i2vrwj48zqzyn3r6frynkjar70d4j4c2")))

(define-public crate-enocean-0.2.1 (c (n "enocean") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "serialport") (r "^3.2.0") (d #t) (k 0)))) (h "1jhylpzbs3izb70ysf412jr7isy2g2k40iws80l8fz1wxki0im74")))

(define-public crate-enocean-0.2.2 (c (n "enocean") (v "0.2.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "serialport") (r "^3.2.0") (d #t) (k 0)))) (h "0s6fl0hvl69zfc6cmn33wi0b0xd6jzc3053p3jgb2lkrfs482amg")))

(define-public crate-enocean-0.2.3 (c (n "enocean") (v "0.2.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "serialport") (r "^3.2.0") (d #t) (k 0)))) (h "0nnq3rb8yzgsmv81bijnyj6vmjh5vlyfcm9a6378jbg0fbrgyjik")))

(define-public crate-enocean-0.2.4 (c (n "enocean") (v "0.2.4") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "serialport") (r "^3.2.0") (d #t) (k 0)))) (h "1w9ngcydd4jax8ml90dc0ny14cqkb35qb3hwqldj68b544zq06c4")))

(define-public crate-enocean-0.2.5 (c (n "enocean") (v "0.2.5") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "serialport") (r "^3.2.0") (d #t) (k 0)))) (h "18333hfslyavzadhqrnmp9ms43kmb1n1wvjna9b67x8whxcx16xx")))

(define-public crate-enocean-0.2.51 (c (n "enocean") (v "0.2.51") (d (list (d (n "serialport") (r "^3.2.0") (d #t) (k 0)))) (h "1yb0k2w8zrfll2fhpbiikjkmw38w9qc81c710wa45nb2l3swr47d")))

(define-public crate-enocean-0.2.52 (c (n "enocean") (v "0.2.52") (d (list (d (n "serialport") (r "^3.2.0") (d #t) (k 0)))) (h "181ngfrazkrqlxmhbi7qrv4ykcld0w4kjzwkmmixb5qyjavs8m8g")))

(define-public crate-enocean-0.2.53 (c (n "enocean") (v "0.2.53") (d (list (d (n "serialport") (r "^3.2.0") (d #t) (k 0)))) (h "00cf9nj4pm66zbirjprn1240maq1gaw6xa4c985lcd5wsxbg5bbq")))

(define-public crate-enocean-0.2.54 (c (n "enocean") (v "0.2.54") (d (list (d (n "serialport") (r "^3.2.0") (d #t) (k 0)))) (h "1rl5l5hbfnacmh42jqaszvqg76r1y83hgcig3h8iv8g328b9rxny")))

(define-public crate-enocean-0.2.56 (c (n "enocean") (v "0.2.56") (d (list (d (n "serialport") (r "^3.3.0") (d #t) (k 0)))) (h "03mg7305j5vgw8v8l97hng4zmwlc00kslkrzyb4ci9v15a079f46")))

(define-public crate-enocean-0.2.57 (c (n "enocean") (v "0.2.57") (d (list (d (n "serialport") (r ">=3.3.0, <4.0.0") (d #t) (k 0)))) (h "03jk9g3idf3my8r2598j5bpa221wx3ywnjdi7hgwig67bi5mn6ng")))

(define-public crate-enocean-0.2.58 (c (n "enocean") (v "0.2.58") (d (list (d (n "serialport") (r ">=3.3.0, <4.0.0") (d #t) (k 0)))) (h "09yb8134wywvdba6fvnhxblnspnw16kln2ayxxklirwjd8mnj0sg")))

