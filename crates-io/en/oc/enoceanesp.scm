(define-module (crates-io en oc enoceanesp) #:use-module (crates-io))

(define-public crate-enoceanesp-0.1.0 (c (n "enoceanesp") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.129") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nix") (r "^0.24.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1mnbwag66wk7qgcirh6ncnaynp12nhgx679wvx3f54d3jnanv2m2") (y #t)))

