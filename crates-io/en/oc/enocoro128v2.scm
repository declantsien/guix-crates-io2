(define-module (crates-io en oc enocoro128v2) #:use-module (crates-io))

(define-public crate-enocoro128v2-0.1.0 (c (n "enocoro128v2") (v "0.1.0") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (f (quote ("zeroize_derive"))) (k 0)))) (h "1d8wfrki8mvm9pcf9myimd58yy7vdacld7kr14maabj16380jf9x")))

(define-public crate-enocoro128v2-0.1.1 (c (n "enocoro128v2") (v "0.1.1") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (f (quote ("zeroize_derive"))) (k 0)))) (h "0sawbzfgw0ypazjrnjbhvqrqwsa3pj40rf71jyvmaard4a05qf55")))

(define-public crate-enocoro128v2-0.1.2 (c (n "enocoro128v2") (v "0.1.2") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (f (quote ("zeroize_derive"))) (k 0)))) (h "0k2hfaw3r2iwad3kk3ahbi6dgrn69nqxrag3mahgs56mf2y0wd7f")))

(define-public crate-enocoro128v2-0.1.3 (c (n "enocoro128v2") (v "0.1.3") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (f (quote ("zeroize_derive"))) (k 0)))) (h "17smdpbikscx6x5iagqc1kj75zpqp0qxjazq9s6kkq5c9ya61a8l")))

(define-public crate-enocoro128v2-0.1.4 (c (n "enocoro128v2") (v "0.1.4") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (f (quote ("zeroize_derive"))) (k 0)))) (h "0ya7pb8dpac2zgh4i2q945r1qx4hijz4wiz3yfbmyacfa4q83abf")))

(define-public crate-enocoro128v2-0.1.5 (c (n "enocoro128v2") (v "0.1.5") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.1.0") (f (quote ("zeroize_derive"))) (k 0)))) (h "1g42qrs16miz11fir1zps81xz4m4sqnfrjwbn7b5avmsayfqi62f")))

(define-public crate-enocoro128v2-0.1.6 (c (n "enocoro128v2") (v "0.1.6") (d (list (d (n "static_assertions") (r "^1") (d #t) (k 0)) (d (n "zeroize") (r "^1") (f (quote ("zeroize_derive"))) (k 0)))) (h "0nlbghh4i6wipwj5jmim465r7v7yybwvjc7n2hl66cr6nkb6wvy3")))

