(define-module (crates-io en vh envhub-pkgs) #:use-module (crates-io))

(define-public crate-envhub-pkgs-0.1.0 (c (n "envhub-pkgs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)))) (h "1ambnrv44s8iq4ks0mxin5fzk7y1fdmkvkki1zadracs2vmpd9ax")))

(define-public crate-envhub-pkgs-0.1.1 (c (n "envhub-pkgs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)))) (h "1xrik9vwd02bc8321a2xdb5l6ycnxmpdhyzrv75sbn6igdla9l5x")))

(define-public crate-envhub-pkgs-0.1.2 (c (n "envhub-pkgs") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)))) (h "1cc0igjdrp381gyg2hjq70y5f9giww6fxgbgdw81s82nhmcar8xp")))

(define-public crate-envhub-pkgs-0.1.3 (c (n "envhub-pkgs") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "1yi9cy05nq7bkcv09nyl3adq4chq9d3mfzrq2pkd5xpvyx3vpfhr")))

(define-public crate-envhub-pkgs-0.1.4 (c (n "envhub-pkgs") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "1fpyp0hyj5a8cdjfg2haqzvj6r0rdmqsh1b0xaghb4157j2xgj8d")))

(define-public crate-envhub-pkgs-0.1.5 (c (n "envhub-pkgs") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "06axqfijj0qz5941xfcj6vy9l69qx404nq2jdjw9iahama2k28n5")))

