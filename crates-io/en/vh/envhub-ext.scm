(define-module (crates-io en vh envhub-ext) #:use-module (crates-io))

(define-public crate-envhub-ext-0.1.0 (c (n "envhub-ext") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "envhub-types") (r "^0.2.0") (d #t) (k 0)))) (h "14ijr56sanbs7jh2s8mnym502fkqzgjl2ssmsmlfwsa1x16agsni")))

(define-public crate-envhub-ext-0.1.1 (c (n "envhub-ext") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "envhub-types") (r "^0.2.2") (d #t) (k 0)))) (h "1khpgilzc0yni1f72p5ksbbvk6zs3b6pvg6ig91s94hw3yhqnr7w")))

