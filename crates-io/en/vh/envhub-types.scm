(define-module (crates-io en vh envhub-types) #:use-module (crates-io))

(define-public crate-envhub-types-0.1.0 (c (n "envhub-types") (v "0.1.0") (d (list (d (n "hcl-rs") (r "^0.14.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "0xb1mdl39djz1r2cjhzlfsk583pxyi1abirz2xdkjb9hcssa6lmx")))

(define-public crate-envhub-types-0.2.0 (c (n "envhub-types") (v "0.2.0") (d (list (d (n "hcl-rs") (r "^0.14.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "1lpjk3d3hz87zvsij7c5qqq0piai7mbd73q5qrkzqzvnayvvgdy2")))

(define-public crate-envhub-types-0.2.1 (c (n "envhub-types") (v "0.2.1") (d (list (d (n "hcl-rs") (r "^0.14.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "1rvgkxhwygwh079w4br4qc37sc9bnhb6w8warvd0bmdfszl5sdqy")))

(define-public crate-envhub-types-0.2.2 (c (n "envhub-types") (v "0.2.2") (d (list (d (n "hcl-rs") (r "^0.14.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)))) (h "0fcqpj4627684sldansm98xb7krfkk5akk6h5shf7w67x1xcx5s9")))

