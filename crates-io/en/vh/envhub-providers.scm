(define-module (crates-io en vh envhub-providers) #:use-module (crates-io))

(define-public crate-envhub-providers-0.1.0 (c (n "envhub-providers") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "13nfz81kyh71ki4az0s64n3gm0vprsn6sxrhc87cs86cl1xx1xw6")))

(define-public crate-envhub-providers-0.2.0 (c (n "envhub-providers") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "envhub-pkgs") (r "^0.1.0") (d #t) (k 0)))) (h "13nx0vmsrflm5573d79hn92vq09i5925sypc8bzc8m41s8ibxa73")))

