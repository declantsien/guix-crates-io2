(define-module (crates-io en so enso-optics) #:use-module (crates-io))

(define-public crate-enso-optics-0.1.1 (c (n "enso-optics") (v "0.1.1") (d (list (d (n "enso-prelude") (r "^0.1.1") (d #t) (k 0)))) (h "0fa1in24hdh2avc6hlh9ndjda9328pblwrpcryw7xdi71kxkwh0g")))

(define-public crate-enso-optics-0.2.0 (c (n "enso-optics") (v "0.2.0") (d (list (d (n "enso-prelude") (r "^0.2.0") (d #t) (k 0)))) (h "00jqy4w5qjiz3lqbzg1vk1ad5q5raf5yi6nihmi7xi886i138jhz")))

