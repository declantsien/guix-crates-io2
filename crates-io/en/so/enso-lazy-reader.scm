(define-module (crates-io en so enso-lazy-reader) #:use-module (crates-io))

(define-public crate-enso-lazy-reader-0.1.0 (c (n "enso-lazy-reader") (v "0.1.0") (d (list (d (n "enso-prelude") (r "^0.1.3") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "060fqr36dkp4p2v0mcwaz4q9g1cn54bk5gw9a4qgyjbhacdb9sqd")))

(define-public crate-enso-lazy-reader-0.1.1 (c (n "enso-lazy-reader") (v "0.1.1") (d (list (d (n "enso-prelude") (r "^0.1.5") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "1z5kdn7zy4nwnpm8bxqr5kq0813rgmhxyxwyqkkxb1r39igpg5sr")))

(define-public crate-enso-lazy-reader-0.1.2 (c (n "enso-lazy-reader") (v "0.1.2") (d (list (d (n "enso-prelude") (r "^0.1.7") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "1vz7q3zcclaf3nblqk8zyj089gzwgyvjdgsbjs84za8kaic840rj")))

(define-public crate-enso-lazy-reader-0.2.0 (c (n "enso-lazy-reader") (v "0.2.0") (d (list (d (n "enso-prelude") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)))) (h "08r5fvcria20cyq6yczvkv8cw809xk5ilkzg5s3h68bkacmd533b")))

