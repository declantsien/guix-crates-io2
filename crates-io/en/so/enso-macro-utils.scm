(define-module (crates-io en so enso-macro-utils) #:use-module (crates-io))

(define-public crate-enso-macro-utils-0.1.0 (c (n "enso-macro-utils") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "1yldjsxnfcxmmgg2h4lj9gq2ir3kh7rh7x3q7v0z018iamq53mw7")))

(define-public crate-enso-macro-utils-0.1.1 (c (n "enso-macro-utils") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "1bnrqb60j1q5fz89rz71d1lb0smld0l0ck0zw0yyz4myhnchjvk1")))

(define-public crate-enso-macro-utils-0.2.0 (c (n "enso-macro-utils") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full" "visit"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "027z1rc35v6nvp55hj241vh3x083ks97h4xd8ch7hayqm6nc7r34")))

