(define-module (crates-io en so enso-automata) #:use-module (crates-io))

(define-public crate-enso-automata-0.1.1 (c (n "enso-automata") (v "0.1.1") (d (list (d (n "enso-prelude") (r "^0.1.1") (d #t) (k 0)))) (h "097zz9khlm7g7xcqz1z5v598lk5p0dij9zc4bx5lr7ga90h4739g")))

(define-public crate-enso-automata-0.1.2 (c (n "enso-automata") (v "0.1.2") (d (list (d (n "enso-prelude") (r "^0.1.3") (d #t) (k 0)))) (h "0by7vdbqfwq91hs51946hn2vb78kg2yl4z8pxcpxjz2r5hnssfi5")))

(define-public crate-enso-automata-0.1.3 (c (n "enso-automata") (v "0.1.3") (d (list (d (n "enso-prelude") (r "^0.1.4") (d #t) (k 0)))) (h "0fmbqhpvplparf26a9xvcfqs7gpisw7zmhdimcsp351xw3k8mjjy")))

(define-public crate-enso-automata-0.1.4 (c (n "enso-automata") (v "0.1.4") (d (list (d (n "enso-prelude") (r "^0.1.5") (d #t) (k 0)))) (h "0wzr44a2ksa08ki8qg7ygkk6vbc7fnd0dn9kbf856ndgbp2xwx4j")))

(define-public crate-enso-automata-0.1.5 (c (n "enso-automata") (v "0.1.5") (d (list (d (n "enso-prelude") (r "^0.1.7") (d #t) (k 0)))) (h "0sn6n2g2nl0qpzvhq7s15l5vgz36i0s3dq0wz9ylfriqhf0j9i51")))

(define-public crate-enso-automata-0.2.0 (c (n "enso-automata") (v "0.2.0") (d (list (d (n "enso-prelude") (r "^0.2.0") (d #t) (k 0)))) (h "0qfj89cwnaqwpcaj15ffxflikhl8z6ga40nzsb6kcba45xwjic7r")))

