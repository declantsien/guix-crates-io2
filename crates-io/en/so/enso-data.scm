(define-module (crates-io en so enso-data) #:use-module (crates-io))

(define-public crate-enso-data-0.1.1 (c (n "enso-data") (v "0.1.1") (d (list (d (n "enso-prelude") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dscfk6353wphhxwpyw079brg94wpn30fplqwfpibsgr4d5hpi7z")))

(define-public crate-enso-data-0.1.2 (c (n "enso-data") (v "0.1.2") (d (list (d (n "enso-prelude") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13jl6mv0yapmz2psdvzk99gfhzgd8pbgk593b9hp8w23whdd0cck")))

(define-public crate-enso-data-0.1.3 (c (n "enso-data") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "enso-prelude") (r "^0.1.8") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hkhhj7qdyz1jf4c8wgq3992y4my728zcmz8q9mjj5yb9ggr3i56")))

(define-public crate-enso-data-0.1.4 (c (n "enso-data") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "enso-prelude") (r "^0.1.8") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typenum") (r "^1.11.2") (d #t) (k 0)))) (h "15gvpn4jq17s85llyymr3jzfpk4rah23qdlwbsbiz8ivrh0psy50")))

(define-public crate-enso-data-0.1.5 (c (n "enso-data") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "enso-prelude") (r "^0.1.10") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typenum") (r "^1.11.2") (d #t) (k 0)))) (h "1pkkgsnx8alcnvh5n5j380cvsf0xzdgiszflcl948558s8xlpbsv")))

(define-public crate-enso-data-0.2.0 (c (n "enso-data") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "enso-prelude") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "typenum") (r "^1.11.2") (d #t) (k 0)))) (h "0nk2bajamch5md1vm8qnb3h265myfv5gxmig20dnf23f6r2a3nrr")))

