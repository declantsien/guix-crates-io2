(define-module (crates-io en so enso-shapely) #:use-module (crates-io))

(define-public crate-enso-shapely-0.1.0 (c (n "enso-shapely") (v "0.1.0") (d (list (d (n "derivative") (r "^1.0.3") (d #t) (k 0)) (d (n "enso-shapely-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "shrinkwraprs") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "16zhvmfn2rvlcqrix03knliw5ikv38n0bp6962lnp62zg3w1lx51") (f (quote (("default"))))))

(define-public crate-enso-shapely-0.1.1 (c (n "enso-shapely") (v "0.1.1") (d (list (d (n "derivative") (r "^1.0.3") (d #t) (k 0)) (d (n "enso-shapely-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "shrinkwraprs") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "0cmyrb1964iffx8mpbpnjj1zk9jin6s7nvr8304a4wbjy19q9dbw") (f (quote (("default"))))))

(define-public crate-enso-shapely-0.1.2 (c (n "enso-shapely") (v "0.1.2") (d (list (d (n "derivative") (r ">=1.0.3, <2.0.0") (d #t) (k 0)) (d (n "enso-shapely-macros") (r ">=0.1.1, <0.2.0") (d #t) (k 0)) (d (n "paste") (r ">=0.1.0, <0.2.0") (d #t) (k 0)) (d (n "shrinkwraprs") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r ">=0.2.0, <0.3.0") (d #t) (k 2)))) (h "0h3vmnlj2bpx0p554d97zhhprrmppw9aw42ssjlnavagb6lh25yl") (f (quote (("default"))))))

(define-public crate-enso-shapely-0.1.3 (c (n "enso-shapely") (v "0.1.3") (d (list (d (n "derivative") (r "^1.0.3") (d #t) (k 0)) (d (n "enso-shapely-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "shrinkwraprs") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "0qbzhmrj5ag8b0x8h0fw6q17n1j6liqz528f5nq7rhdcaziqi3rl") (f (quote (("default"))))))

(define-public crate-enso-shapely-0.1.4 (c (n "enso-shapely") (v "0.1.4") (d (list (d (n "derivative") (r "^1.0.3") (d #t) (k 0)) (d (n "enso-shapely-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "shrinkwraprs") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "1kdgmjmh8yq3mr17daix14mdi4z9a04frci0dxy379mfqj6qjypn") (f (quote (("default"))))))

(define-public crate-enso-shapely-0.2.0 (c (n "enso-shapely") (v "0.2.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "enso-shapely-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "shrinkwraprs") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "1n1wmydlfszj8kkx1sshqvicvhwnf4x82qjz6459ds38c1g366h6") (f (quote (("default"))))))

