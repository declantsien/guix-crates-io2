(define-module (crates-io en so enso-generics) #:use-module (crates-io))

(define-public crate-enso-generics-0.1.1 (c (n "enso-generics") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.21.1") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fwxfbn5xbhxypib1sia5g0rc1rd269321cjaassiypd21ngdxhb")))

(define-public crate-enso-generics-0.2.0 (c (n "enso-generics") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.21.1") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0if4ig7ljw866glahyj6vbwszg18ar216x8j6465bvb9h146hnil")))

