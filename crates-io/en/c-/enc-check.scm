(define-module (crates-io en c- enc-check) #:use-module (crates-io))

(define-public crate-enc-check-0.1.0 (c (n "enc-check") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tabled") (r "^0.14.0") (d #t) (k 0)))) (h "1x9k2dwaqfgly1m1chzmayq1vcd1ssiadv4r4dnq8hyj70kxcmba")))

(define-public crate-enc-check-0.1.1 (c (n "enc-check") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tabled") (r "^0.14.0") (d #t) (k 0)))) (h "052ly4sxifcqlgcv739gdg22f9jxqqcqqa08j5apcq7jjkcw4xma")))

