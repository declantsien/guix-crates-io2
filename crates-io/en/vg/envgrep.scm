(define-module (crates-io en vg envgrep) #:use-module (crates-io))

(define-public crate-envgrep-0.1.0 (c (n "envgrep") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0zxq8md7fix7s8q4yin10igrpbkrp60wm0g2xpwsc21jlcl1i6l6")))

