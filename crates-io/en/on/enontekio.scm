(define-module (crates-io en on enontekio) #:use-module (crates-io))

(define-public crate-enontekio-0.1.0 (c (n "enontekio") (v "0.1.0") (h "13j0qf53vlkz8d80vfyblnz7aingscdi0421q7ycm1vn1m7rg8v0")))

(define-public crate-enontekio-0.1.1 (c (n "enontekio") (v "0.1.1") (h "0rbwpjkdvi9pqh3fv6lg1lp715q8zi5zyfh94w4x0q5590cirzkb")))

(define-public crate-enontekio-0.1.2 (c (n "enontekio") (v "0.1.2") (h "1l5g0gr79rbj2vqns8fy2hwdcnbic7jlklgvb8pmpbj4bhk4q6qc")))

(define-public crate-enontekio-0.1.3 (c (n "enontekio") (v "0.1.3") (d (list (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)))) (h "0yaqx2l50qxzljy8m9l5h3gwzpx3a8fqcfslq4lhamiar5qax1qn") (y #t)))

(define-public crate-enontekio-0.2.0 (c (n "enontekio") (v "0.2.0") (d (list (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)))) (h "1b1iyygrr99w53csknm10bfjzfz59r6bkwwnyvzvl4lk0814fizj")))

