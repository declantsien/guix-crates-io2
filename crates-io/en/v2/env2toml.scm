(define-module (crates-io en v2 env2toml) #:use-module (crates-io))

(define-public crate-env2toml-0.1.0 (c (n "env2toml") (v "0.1.0") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.8.11") (d #t) (k 2)))) (h "05lw45y0462bk68k916cliwxvqig70majxx4rhnxbsdl7fmd0958")))

(define-public crate-env2toml-0.2.0 (c (n "env2toml") (v "0.2.0") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.11") (d #t) (k 0)))) (h "08a2sdjrlfz7jf2n8mc4hc64y8fin2hjs6shfvyn5xg6k85ic4f5")))

