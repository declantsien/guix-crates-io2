(define-module (crates-io en v2 env2) #:use-module (crates-io))

(define-public crate-env2-0.1.0 (c (n "env2") (v "0.1.0") (h "130l8n2zwvcmrysvn9mqg502xhscd4p6l7qlrlqz4dqwykwk2xah")))

(define-public crate-env2-0.2.0 (c (n "env2") (v "0.2.0") (h "05nzq4kmqrwgipdm82d06w1h8zkvvgq5w20krnl1lzi2nf8an0bw")))

(define-public crate-env2-0.3.0 (c (n "env2") (v "0.3.0") (h "1qhww0hclslifdgaaaf8shhcjrxywy5mmw637fpvwgbwflcq3rpx")))

(define-public crate-env2-0.3.1 (c (n "env2") (v "0.3.1") (h "039l4yxbayvmm6jih7g4ad84mjlifcawxqg190g0pnk0690n92av")))

(define-public crate-env2-0.3.2 (c (n "env2") (v "0.3.2") (h "06ycibclsp97ksv72l1scdjz55f6yzfm5pkk3c6csb60yagi6rrc")))

