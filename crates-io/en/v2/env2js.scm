(define-module (crates-io en v2 env2js) #:use-module (crates-io))

(define-public crate-env2js-1.0.1 (c (n "env2js") (v "1.0.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vd6b3g2pgf5x6zb6bqg98kxqga80nxblnhfha6bwlf7npk0dgv0")))

