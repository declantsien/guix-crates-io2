(define-module (crates-io en g- eng-units) #:use-module (crates-io))

(define-public crate-eng-units-0.1.0 (c (n "eng-units") (v "0.1.0") (h "1dpglr4lp4d0jyn4r3jl89da41jxzks69hdcj9cx66b056x90g8g") (y #t)))

(define-public crate-eng-units-0.1.1 (c (n "eng-units") (v "0.1.1") (h "0r8xnkngp2lbm2wygfwlamz8yb7zp48rzsdjh4whvi9ajkrb0scd") (y #t)))

(define-public crate-eng-units-0.1.2 (c (n "eng-units") (v "0.1.2") (h "184q82amd5m59kglipfxnj6gyf764y1naz94vangacm1y2vlk9i4") (y #t)))

(define-public crate-eng-units-0.1.3 (c (n "eng-units") (v "0.1.3") (h "05drc0zf1sdri5zl34qk3bb3i8sf00p83irwx1k93yfmi8dhf9gw") (y #t)))

(define-public crate-eng-units-0.1.4 (c (n "eng-units") (v "0.1.4") (h "1ck92v3qfv7as55qgm5j00qds3ggri5izvwpnk7bzv74gk02snyl") (y #t)))

(define-public crate-eng-units-0.1.5 (c (n "eng-units") (v "0.1.5") (h "0dppnb9bv1g0njm9z5z6nfcmzky7glqyk5y0pil0g3a5hd52jgf5") (y #t)))

(define-public crate-eng-units-0.1.6 (c (n "eng-units") (v "0.1.6") (h "15v2p6zrg914r6j6m56sg908kvkkaxgfgpxbri0s4pip8hxdwsdg") (y #t)))

(define-public crate-eng-units-0.1.7 (c (n "eng-units") (v "0.1.7") (h "1s9q9qm5vv5bbvffxbdaqvdvxp2k2jnmx7f65h2z877398w4r0nw") (y #t)))

(define-public crate-eng-units-0.1.8 (c (n "eng-units") (v "0.1.8") (h "1c5qsag7h8g77wg924h6mnblfy133w0w9jlybs8izfh6i50vxgqr") (y #t)))

(define-public crate-eng-units-0.1.9 (c (n "eng-units") (v "0.1.9") (h "1grpwli19isbm6wj6zaa1ni5fxld6r7s91s451g2nindxzjgr738") (y #t)))

(define-public crate-eng-units-0.1.10 (c (n "eng-units") (v "0.1.10") (h "02pchxap4qyy43kc5iv9m1bq3rmqvjw32f1lc7j9lp9yp0af32mq")))

