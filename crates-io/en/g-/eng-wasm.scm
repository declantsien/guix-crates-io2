(define-module (crates-io en g- eng-wasm) #:use-module (crates-io))

(define-public crate-eng-wasm-0.1.2 (c (n "eng-wasm") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "eng-pwasm-abi") (r "^0.2.1") (d #t) (k 0)) (d (n "ethabi") (r "^6.1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (f (quote ("derive"))) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4") (d #t) (k 0)))) (h "07lrdwm224qp87wxrym0jw0gvm8m1lg8abhdrn6ih0kf3lnmh2m4")))

(define-public crate-eng-wasm-0.1.3 (c (n "eng-wasm") (v "0.1.3") (d (list (d (n "eng-pwasm-abi") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wy1d584kjhg7yp9a1923hjpnppbgpcx1md32fr86adbqi5h87r8")))

(define-public crate-eng-wasm-0.1.4 (c (n "eng-wasm") (v "0.1.4") (d (list (d (n "eng-pwasm-abi") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17ap7iy6z08iqhpaihg70mdlafc29xd6bkwagrlw5mfasqvk35r3")))

(define-public crate-eng-wasm-0.1.5 (c (n "eng-wasm") (v "0.1.5") (d (list (d (n "eng-pwasm-abi") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0a88g4ym83pvzwfjldf6lwrj6a7b2maq572ia75w6kyhcn6k3q4d")))

(define-public crate-eng-wasm-0.1.6 (c (n "eng-wasm") (v "0.1.6") (d (list (d (n "eng-pwasm-abi") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lgyin65ga4acsghap4plidc1p8crvd96q5il7sw0yv4gaqlynbw")))

(define-public crate-eng-wasm-0.1.7 (c (n "eng-wasm") (v "0.1.7") (d (list (d (n "eng-pwasm-abi") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11dn0pk1m769lla1z0hwwvl6h3682c0ggbjji06zfx0441k51dmq")))

