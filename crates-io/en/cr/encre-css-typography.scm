(define-module (crates-io en cr encre-css-typography) #:use-module (crates-io))

(define-public crate-encre-css-typography-0.1.0 (c (n "encre-css-typography") (v "0.1.0") (d (list (d (n "encre-css") (r "^0.10.0") (d #t) (k 0)))) (h "0cqjad17vs3dxhg7czqx3i8g0625gdh0asfdvkw7hplkflfkshhq")))

(define-public crate-encre-css-typography-0.1.1 (c (n "encre-css-typography") (v "0.1.1") (d (list (d (n "encre-css") (r "^0.10.1") (d #t) (k 0)))) (h "05qbd409pklxrxqwncwyhcggyisplc0wyjhczi2wv81wm3z07q8f")))

(define-public crate-encre-css-typography-0.1.2 (c (n "encre-css-typography") (v "0.1.2") (d (list (d (n "encre-css") (r "^0.11.0") (d #t) (k 0)))) (h "0f0rfzbdl1mb585nbbr0hw2mizz25v3v2sivqc3qpm8jnab0kbx3")))

(define-public crate-encre-css-typography-0.1.3 (c (n "encre-css-typography") (v "0.1.3") (d (list (d (n "encre-css") (r "^0.12.0") (d #t) (k 0)))) (h "0yj4dxfs9zwah68nmz795j5lxzr9z75dyvskg4v8wrimr403vlsi")))

