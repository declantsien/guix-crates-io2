(define-module (crates-io en cr encryption_api_services_rust_crate) #:use-module (crates-io))

(define-public crate-encryption_api_services_rust_crate-0.1.0 (c (n "encryption_api_services_rust_crate") (v "0.1.0") (h "0x1p566qfxi9qv10i7820kddr00mqrf3l5yxxpg0lv4k8g7b3ga7")))

(define-public crate-encryption_api_services_rust_crate-0.1.1 (c (n "encryption_api_services_rust_crate") (v "0.1.1") (d (list (d (n "bcrypt") (r "^0.13.0") (d #t) (k 0)) (d (n "rsa") (r "^0.7.0") (d #t) (k 0)))) (h "1ql61bii2hxbnylcsmwqnpm6379zp5jzfbmfk6r7n9n22gnsaizi")))

