(define-module (crates-io en cr encryption-macros) #:use-module (crates-io))

(define-public crate-encryption-macros-0.1.0 (c (n "encryption-macros") (v "0.1.0") (d (list (d (n "encryption-macros-encryption") (r "^0.1") (d #t) (k 0)) (d (n "encryption-macros-key-generation") (r "^0.1") (d #t) (k 0)) (d (n "encryption-macros-utils") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "08i7hhf10jp0vwnj3nw5v6lb3k90zpp2qnfkl7rf3yihng5qqxlv") (y #t)))

(define-public crate-encryption-macros-0.1.1 (c (n "encryption-macros") (v "0.1.1") (d (list (d (n "encryption-macros-encryption") (r "^0.1.1") (d #t) (k 0)) (d (n "encryption-macros-key-generation") (r "^0.1") (d #t) (k 0)) (d (n "encryption-macros-utils") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "00ry8byhc69yi1if61rlaw063z9wq69nsrs172lnn9b6ipn1mhkp")))

