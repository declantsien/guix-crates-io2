(define-module (crates-io en cr encryptable) #:use-module (crates-io))

(define-public crate-encryptable-0.1.0 (c (n "encryptable") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "fernet") (r "^0.2.1") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "0rd54jp30y6mgsdv3413z2iv3gr61wzshdyjzkywvhdv4yz2hhmx")))

(define-public crate-encryptable-0.1.1 (c (n "encryptable") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "bincode") (r "^2.0.0-rc.3") (o #t) (d #t) (k 0)) (d (n "fernet") (r "^0.2.1") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "050i4p88p8wdw9xkcav0bgfd05l47v8xzvwqadvpz7l2pnb15qnc") (s 2) (e (quote (("serde" "dep:serde") ("bincode" "dep:bincode"))))))

