(define-module (crates-io en cr encrypto_sha256) #:use-module (crates-io))

(define-public crate-encrypto_sha256-0.1.0 (c (n "encrypto_sha256") (v "0.1.0") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "049x4n73hy2wj4lk7c5b9ncw2a048hcmfx28nlqgcwjqkwj5cdlr")))

(define-public crate-encrypto_sha256-0.1.1 (c (n "encrypto_sha256") (v "0.1.1") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "0r0j70ydf45b3rn3qq1z397g89m0a73km0dl0gcjzb77mrfzw6bq")))

