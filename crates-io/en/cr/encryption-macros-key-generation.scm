(define-module (crates-io en cr encryption-macros-key-generation) #:use-module (crates-io))

(define-public crate-encryption-macros-key-generation-0.1.0 (c (n "encryption-macros-key-generation") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0w41fjv6bxac1h3k8z19862g8f82sa8pjd3b111draix1rpm4b0i")))

