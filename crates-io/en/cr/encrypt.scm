(define-module (crates-io en cr encrypt) #:use-module (crates-io))

(define-public crate-encrypt-0.1.0 (c (n "encrypt") (v "0.1.0") (d (list (d (n "convert") (r "^0.2.0") (d #t) (k 0)) (d (n "digest") (r "^0.8.1") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)))) (h "1km2abpkkdqv7cpakzvx89ayikig89hdjvn8150xr3scak9giymg")))

(define-public crate-encrypt-0.1.1 (c (n "encrypt") (v "0.1.1") (d (list (d (n "convert") (r "^0.2.0") (d #t) (k 0)) (d (n "digest") (r "^0.8.1") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)))) (h "1pqb2f4nccjxrzq4kdhac8d436bzxxn077jbji1xdcak2g0g0wb3")))

(define-public crate-encrypt-0.1.2 (c (n "encrypt") (v "0.1.2") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)))) (h "1r130pzqvh1nc61jrflky1c81fn4ch9fka2r541g936mz5lnfmg1")))

(define-public crate-encrypt-0.1.3 (c (n "encrypt") (v "0.1.3") (d (list (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)))) (h "16bdlx7brj9j5kyv2dzdlkrja8pbwzwh0z7y6la1icl9k7xv3f3i")))

