(define-module (crates-io en cr encrust-core) #:use-module (crates-io))

(define-public crate-encrust-core-0.1.0 (c (n "encrust-core") (v "0.1.0") (d (list (d (n "chacha20") (r "^0.9.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h05w3qmbchpdvlknnkqrglmzbliv6pqy2z5pl67gibcggpww23l") (f (quote (("macros") ("default" "rand" "std" "macros") ("all" "rand" "std" "macros")))) (s 2) (e (quote (("std" "chacha20/std" "rand?/std" "zeroize/std") ("rand" "dep:rand"))))))

(define-public crate-encrust-core-0.1.1 (c (n "encrust-core") (v "0.1.1") (d (list (d (n "chacha20") (r "^0.9.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jdzbxn58qi4vqh7x3shrbdii23c66mhv4bzyqs5bbq7lj61is93") (f (quote (("macros") ("default" "rand" "std" "macros") ("all" "rand" "std" "macros")))) (s 2) (e (quote (("std" "chacha20/std" "rand?/std" "zeroize/std") ("rand" "dep:rand"))))))

