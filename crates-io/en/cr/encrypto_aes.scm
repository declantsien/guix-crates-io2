(define-module (crates-io en cr encrypto_aes) #:use-module (crates-io))

(define-public crate-encrypto_aes-0.1.0 (c (n "encrypto_aes") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("rand" "serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "1bf71b8z4mxzk3k7kllq7i5cmdg2d6d2qgwi5q7by5cald7c41ms")))

(define-public crate-encrypto_aes-0.2.0 (c (n "encrypto_aes") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("rand" "serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "0141rr328nq6swhmg7h58ggc38wmyvh95bqjfa3g19jhp3mzc0a1")))

(define-public crate-encrypto_aes-0.3.0 (c (n "encrypto_aes") (v "0.3.0") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "encrypto_rsa") (r "^3.1.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("rand" "serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "085kx6zsdazjfwg6i79iizjmb36g9smflf7iy95p86dw08zixmp7")))

