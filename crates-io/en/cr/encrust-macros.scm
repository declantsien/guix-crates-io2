(define-module (crates-io en cr encrust-macros) #:use-module (crates-io))

(define-public crate-encrust-macros-0.1.0 (c (n "encrust-macros") (v "0.1.0") (d (list (d (n "chacha20") (r "^0.9.1") (d #t) (k 0)) (d (n "encrust-core") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.33") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "zeroize") (r "^1.6.0") (d #t) (k 2)))) (h "0n38gdsm400snifnk81s5l95a1vfz3zm1y101ngwkmv02pga1d1q")))

(define-public crate-encrust-macros-0.1.1 (c (n "encrust-macros") (v "0.1.1") (d (list (d (n "chacha20") (r "^0.9.1") (d #t) (k 0)) (d (n "encrust-core") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.33") (f (quote ("derive" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "zeroize") (r "^1.6.0") (d #t) (k 2)))) (h "0p2gs6qpm7hl1va9mxhxv8g4wy0k03jmh2cszy771c2r0j2xpk5y")))

