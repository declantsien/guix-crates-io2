(define-module (crates-io en cr encrypto_rust) #:use-module (crates-io))

(define-public crate-encrypto_rust-0.1.0 (c (n "encrypto_rust") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("rand" "serde"))) (d #t) (k 0)) (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "0db91hzccyf4z8c0n3ffgza8r3aafsmr7kjvj6fkhywdv562l74a")))

(define-public crate-encrypto_rust-0.2.0 (c (n "encrypto_rust") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (k 0)) (d (n "num-bigint") (r "^0.4.3") (f (quote ("rand" "serde"))) (d #t) (k 0)) (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "19l9xxj5w6s7p6n6mkq5irvl0q0fjw1cp077rbbvrcrrl03jnddd")))

