(define-module (crates-io en cr encrypted-json-kv) #:use-module (crates-io))

(define-public crate-encrypted-json-kv-0.2.1 (c (n "encrypted-json-kv") (v "0.2.1") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)) (d (n "serde_test") (r "^1.0.110") (d #t) (k 2)) (d (n "sled") (r "^0.31.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0f6yybz4csqzp2qmsr74b2lhaap3j7mmg9664d2zzgfmmhp7xmiv")))

