(define-module (crates-io en cr encrypted_id_derive) #:use-module (crates-io))

(define-public crate-encrypted_id_derive-0.1.0 (c (n "encrypted_id_derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.10.2") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "0hzh8qbdk3q9vxabzhf7aq6rxfv6b1lglfdhnzh6jc0i7kpsjhiw")))

