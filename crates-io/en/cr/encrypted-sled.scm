(define-module (crates-io en cr encrypted-sled) #:use-module (crates-io))

(define-public crate-encrypted-sled-0.1.0 (c (n "encrypted-sled") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chacha20") (r "^0.7.2") (d #t) (k 2)) (d (n "cipher") (r "^0.3.0") (d #t) (k 0)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)))) (h "0a4pkc5gi9azlxnbm3p2zpjadp6w95psdl8qb2q16al3sla7wpxa")))

(define-public crate-encrypted-sled-0.1.1 (c (n "encrypted-sled") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chacha20") (r "^0.7.2") (d #t) (k 2)) (d (n "cipher") (r "^0.3.0") (d #t) (k 0)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)))) (h "1k9l4qkwrzs0q7ajb4iyvpj4kzzbq9cc1aj8ahs2an3pflxnb1k0")))

(define-public crate-encrypted-sled-0.1.2 (c (n "encrypted-sled") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chacha20") (r "^0.7.2") (d #t) (k 2)) (d (n "cipher") (r "^0.3.0") (d #t) (k 0)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)))) (h "0n9ci8cx5g7vdyw2xc8mkkkfpi7ljxgkbgmp82xzjxgq3iwxcqkv")))

(define-public crate-encrypted-sled-0.1.3 (c (n "encrypted-sled") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "chacha20") (r "^0.7.2") (d #t) (k 2)) (d (n "cipher") (r "^0.3.0") (d #t) (k 0)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)))) (h "13128w5xk7iy3caqqq9lhc6fb0a22mv54kgs1vbg3lv5bpwhk2qh")))

(define-public crate-encrypted-sled-0.2.0 (c (n "encrypted-sled") (v "0.2.0") (d (list (d (n "aead") (r "^0.4.3") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "blake3") (r "^1.1.0") (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (f (quote ("std"))) (o #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("std_rng"))) (d #t) (k 2)) (d (n "sled") (r "^0.34.6") (d #t) (k 0)))) (h "1zsrcz1ij71ymax7lgbg952mgnizrlkmakmzy0g7prcfz5aq0swc") (f (quote (("default" "rand"))))))

