(define-module (crates-io en cr encrypted_id) #:use-module (crates-io))

(define-public crate-encrypted_id-0.1.0 (c (n "encrypted_id") (v "0.1.0") (d (list (d (n "base64") (r "~0.10.1") (d #t) (k 0)) (d (n "byteorder") (r "~1.3.2") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "~1.3.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "16pkwb4z31v0f4y11rw9fzcafqbi0g6ig7j0yqnx8sqbpyq3s9cz")))

(define-public crate-encrypted_id-0.1.1 (c (n "encrypted_id") (v "0.1.1") (d (list (d (n "base64") (r "~0.10.1") (d #t) (k 0)) (d (n "byteorder") (r "~1.3.2") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "~1.3.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0jzzp1yw50wwrxal3x3gb71f327ihvms06y54cc1hpzlixacibhm")))

(define-public crate-encrypted_id-0.1.2 (c (n "encrypted_id") (v "0.1.2") (d (list (d (n "base64") (r "~0.10.1") (d #t) (k 0)) (d (n "byteorder") (r "~1.3.2") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "~1.3.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0n70aax8icp8q6vkf39qcdm3vhx9ssvjs26hw6m41ssgzyd5dvws")))

(define-public crate-encrypted_id-0.1.3 (c (n "encrypted_id") (v "0.1.3") (d (list (d (n "base64") (r "~0.10.1") (d #t) (k 0)) (d (n "byteorder") (r "~1.3.2") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "~1.3.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "173gn25k6s54jsmd5wbysas5vjpchlhmabid4ncwdc87g7p20msd")))

(define-public crate-encrypted_id-0.1.4 (c (n "encrypted_id") (v "0.1.4") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1aqhshxk4jd38y51glz7fdaypzdi5fwhr64s161li4x0yix00z8d")))

(define-public crate-encrypted_id-0.1.5 (c (n "encrypted_id") (v "0.1.5") (d (list (d (n "base64") (r "^0.12.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "11k0j89drsbpqggac9ji4xbspsjg28pdkl0x9614y6d224xb1i0g")))

