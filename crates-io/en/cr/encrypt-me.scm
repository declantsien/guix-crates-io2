(define-module (crates-io en cr encrypt-me) #:use-module (crates-io))

(define-public crate-encrypt-me-1.0.0 (c (n "encrypt-me") (v "1.0.0") (h "19j99xqn47a0kfrf1zka9q658qfsmmk9dbx1v6rk34kvm2hf44lx")))

(define-public crate-encrypt-me-1.1.0 (c (n "encrypt-me") (v "1.1.0") (h "0szpazxs79bafqjk26gi18h3v8ndly8iv7flyajdzhfdi4vf62s0")))

