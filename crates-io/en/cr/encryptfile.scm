(define-module (crates-io en cr encryptfile) #:use-module (crates-io))

(define-public crate-encryptfile-0.1.1 (c (n "encryptfile") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "0vg1113hmbyp8rmmh6lp73ggpc1gh35wlbsvvpgnnw0xpwa1haq4")))

(define-public crate-encryptfile-0.1.2 (c (n "encryptfile") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1vfw0iwzzxgyh7vixp54fs4lhpybn73vq7l2f26plcfcphbrddrp")))

(define-public crate-encryptfile-0.1.3 (c (n "encryptfile") (v "0.1.3") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "06bbfczm6d97kw95fh4y6gzldgv0yhj4y7frj1q5zfrg6mac9564")))

