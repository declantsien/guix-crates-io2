(define-module (crates-io en cr encrypt-remote-hook) #:use-module (crates-io))

(define-public crate-encrypt-remote-hook-0.1.0 (c (n "encrypt-remote-hook") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (f (quote ("tls"))) (d #t) (k 0)))) (h "0hki1qxy9zv14i2942rffsddm30synjam9hfh95vj2h9wk5i14pg")))

(define-public crate-encrypt-remote-hook-0.1.1 (c (n "encrypt-remote-hook") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (f (quote ("tls"))) (d #t) (k 0)))) (h "03s65wfbiwnw6k5vpwafv469z6lpvqa9pcfg47m7mx9z157vraf9")))

(define-public crate-encrypt-remote-hook-0.1.2 (c (n "encrypt-remote-hook") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (f (quote ("tls"))) (d #t) (k 0)))) (h "1gni6lx2fq45g4hbqfiasz6g0pj3gg6gbqvfgpzfy9qya4yavv5c")))

(define-public crate-encrypt-remote-hook-0.1.3 (c (n "encrypt-remote-hook") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (f (quote ("tls"))) (d #t) (k 0)))) (h "1gjh0xalk3h98lwdhivbwn62ddq40kjg8r97hkhaf4qyh1d96csq")))

(define-public crate-encrypt-remote-hook-0.2.0 (c (n "encrypt-remote-hook") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "dnsclient") (r "^0.1") (d #t) (k 0)) (d (n "dotenv-parser") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (f (quote ("tls"))) (d #t) (k 0)))) (h "05wdik6aymfp0b4annk6v9m5sy2m250ryc5d5lnyhsxisgpb08kj")))

