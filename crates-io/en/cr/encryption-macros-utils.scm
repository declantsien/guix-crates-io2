(define-module (crates-io en cr encryption-macros-utils) #:use-module (crates-io))

(define-public crate-encryption-macros-utils-0.1.0 (c (n "encryption-macros-utils") (v "0.1.0") (d (list (d (n "encryption-macros-key-generation") (r "^0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)))) (h "19sd7s7323s3sa9gf0nab4n92mr6sh4253hri8q6sa8mihvlm8i0")))

