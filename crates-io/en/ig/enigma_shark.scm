(define-module (crates-io en ig enigma_shark) #:use-module (crates-io))

(define-public crate-enigma_shark-0.1.0 (c (n "enigma_shark") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kawvkavr3a43ydj0dkvhhy2n0bs96dncma1c6khhvp3grqf3ifd")))

