(define-module (crates-io en ig enigma_machine) #:use-module (crates-io))

(define-public crate-enigma_machine-0.1.0 (c (n "enigma_machine") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "1yj72xl407w6iim16iqm1rgfyqnsjgcil37z0p2qlxiss6ipdc4x")))

(define-public crate-enigma_machine-0.1.1 (c (n "enigma_machine") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "1qfda5ac8im56c8hv2gqx4fi9fznah62kbxbqydz1rnjihaxzxwj")))

(define-public crate-enigma_machine-0.1.2 (c (n "enigma_machine") (v "0.1.2") (d (list (d (n "base64") (r "~0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "0v1b8pan64c4afigw94bfagrdsfx6iak11qplypq72cffcfzc75p")))

