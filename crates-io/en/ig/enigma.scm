(define-module (crates-io en ig enigma) #:use-module (crates-io))

(define-public crate-enigma-0.0.1-alpha (c (n "enigma") (v "0.0.1-alpha") (h "1af6p6dz7hbx3m70aprps1n4z6z6zfmkxkp4yqnxv238y5rn28wm")))

(define-public crate-enigma-0.0.2 (c (n "enigma") (v "0.0.2") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0mhw1w7vylx1zsyanisvk3z4hmzx3p34992nq5zgspdka1k3q6j0")))

