(define-module (crates-io en vi environment_template) #:use-module (crates-io))

(define-public crate-environment_template-0.1.0 (c (n "environment_template") (v "0.1.0") (d (list (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 2)))) (h "1r7im4098b1snfq9sa3kkh2yw01b7z79da6x87p69nmcwqxf98dh")))

