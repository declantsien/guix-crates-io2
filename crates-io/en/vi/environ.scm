(define-module (crates-io en vi environ) #:use-module (crates-io))

(define-public crate-environ-0.0.0 (c (n "environ") (v "0.0.0") (h "1acarwkpkh1swjj2ic33zycsqcwhpnmy7qi9kbj4mbwg6mbj5z8n")))

(define-public crate-environ-1.1.2 (c (n "environ") (v "1.1.2") (h "008kmfwa0q1k31p9cdk6jl94ifah4kllsglqlm25k6f8hv005yrb") (f (quote (("std") ("default" "std"))))))

(define-public crate-environ-1.1.3 (c (n "environ") (v "1.1.3") (h "12lnn4z2af2c830mv72nbk36iw0p0brbjxnf49fjh8fwqmqf51ni") (f (quote (("std") ("default" "std"))))))

