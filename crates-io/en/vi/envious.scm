(define-module (crates-io en vi envious) #:use-module (crates-io))

(define-public crate-envious-0.0.0 (c (n "envious") (v "0.0.0") (h "0xnbz9adlq3x7vp2fyr3pvbzsw90sr3gp1f4i0ndzbjldkghz5c5") (y #t)))

(define-public crate-envious-0.1.0 (c (n "envious") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0ydld5lgdvcnli8nmc2vb5y9akm3aj5xvcm8jhh2zmq7577lc0gy")))

(define-public crate-envious-0.1.1 (c (n "envious") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "08zg6kp1srfsjcqfs7y10hd4mpfbaja5spn6bs230yb83inq6qrs")))

(define-public crate-envious-0.2.0 (c (n "envious") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "temp-env") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "10rwyc2ydqlrdh3zffbliar16xp9y85mcsk8rm1s560k61115nn5")))

(define-public crate-envious-0.2.1 (c (n "envious") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "temp-env") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0pkr22mc8qn2fgzj7v7ki95wvj5f6qslg2ap4apczmhhh0j1ycgd")))

(define-public crate-envious-0.2.2 (c (n "envious") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "temp-env") (r "^0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1ivx51vspnw6blsfg1zsryigw886a0grj354jn0i723m8257hliy")))

