(define-module (crates-io en vi environment-sanity) #:use-module (crates-io))

(define-public crate-environment-sanity-0.0.1 (c (n "environment-sanity") (v "0.0.1") (d (list (d (n "memchr") (r "^1.0") (d #t) (k 0)))) (h "13k4iymsd1vkwb422nba8lgf77pn0kv7fpqz6w4zbvpz57vm37j3")))

(define-public crate-environment-sanity-0.0.2 (c (n "environment-sanity") (v "0.0.2") (d (list (d (n "memchr") (r "^1.0") (d #t) (k 0)))) (h "08hmjg6nlx0ysbjqpjisyqw6l27b6v6f2cdznpkxyqk3wkvql7l2")))

