(define-module (crates-io en vi envir_derive) #:use-module (crates-io))

(define-public crate-envir_derive-0.1.0 (c (n "envir_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0cakclsfnb0gb9w7lp3qc2r6vlqzgfx35hl1padgzcy49jsf1way")))

(define-public crate-envir_derive-0.2.0 (c (n "envir_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0l8s619xjzbx1amam1j5k664nji8qmnvpvdynh73g4jmrjc1gci6")))

(define-public crate-envir_derive-0.2.2 (c (n "envir_derive") (v "0.2.2") (d (list (d (n "envir") (r "^0.2") (f (quote ("serde"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0cd168ygpslgq3ywzljk41j414zd927syj5jqgz4b5piwxvfnxmp")))

