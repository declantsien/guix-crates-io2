(define-module (crates-io en vi envi) #:use-module (crates-io))

(define-public crate-envi-0.1.0 (c (n "envi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.7") (d #t) (k 2)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 0)) (d (n "serial_test") (r "^0.4.0") (d #t) (k 2)) (d (n "shell-escape") (r "^0.1.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "18062p2d9377g2jfx9c28j7xdxaas8gv4g5zzn8z3qd2dacv8vci")))

