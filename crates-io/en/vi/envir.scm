(define-module (crates-io en vi envir) #:use-module (crates-io))

(define-public crate-envir-0.1.0 (c (n "envir") (v "0.1.0") (d (list (d (n "envir_derive") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0y4b402c68a7a0908anlmbddkbaa3x272f72836v5ywyadhn637i") (f (quote (("extrapolation" "regex"))))))

(define-public crate-envir-0.2.0 (c (n "envir") (v "0.2.0") (d (list (d (n "dotenvy") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "envir_derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0nyvjar6xnxvl7yl8kwg11d9jqv4534f9n7yhajx3fbi01515r3z") (f (quote (("serde" "envir_derive") ("extrapolation" "regex" "serde") ("dotenv" "dotenvy") ("default" "dotenv"))))))

(define-public crate-envir-0.2.1 (c (n "envir") (v "0.2.1") (d (list (d (n "dotenvy") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "envir_derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0zl03m73ks932pg65vnpmssxrzq6kxn5sghz38nj7g2a0z0aq5s3") (f (quote (("serde" "envir_derive") ("extrapolation" "regex" "serde") ("dotenv" "dotenvy") ("default" "dotenv"))))))

(define-public crate-envir-0.2.2 (c (n "envir") (v "0.2.2") (d (list (d (n "dotenvy") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "envir_derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1az8n0cr6ry5xh2n6cs4a6aa73glbsq6pjzrv8k8kvbg9jpr8cpc") (f (quote (("serde" "envir_derive") ("extrapolation" "regex" "serde") ("dotenv" "dotenvy") ("default" "dotenv"))))))

