(define-module (crates-io en vi environment) #:use-module (crates-io))

(define-public crate-environment-0.1.0 (c (n "environment") (v "0.1.0") (h "0jpfq0d38lnbmxg1vnn5lf18c11aixsmd75jaqrrbsjxmqw24h97")))

(define-public crate-environment-0.1.1 (c (n "environment") (v "0.1.1") (h "1vh32mcxf3z8xaibwv751zj14d08nh7iwk1vqdj90rkq17i18jqz")))

