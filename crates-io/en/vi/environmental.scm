(define-module (crates-io en vi environmental) #:use-module (crates-io))

(define-public crate-environmental-1.0.0 (c (n "environmental") (v "1.0.0") (h "0j9wp65p0sdbs25wyq4p3fsqvz7wsnndni771bx5nsgawcjn0x6v") (f (quote (("std") ("default" "std"))))))

(define-public crate-environmental-1.0.1 (c (n "environmental") (v "1.0.1") (h "0b3y2zfdjrrcvf3ll4wxpwk1hl6fvmvzx6hwr4q8kpl0gdsn8x2w") (f (quote (("std") ("default" "std"))))))

(define-public crate-environmental-1.0.2 (c (n "environmental") (v "1.0.2") (h "1a1pg04lbrh33yxliddyllmlcf4ma4a5rqnhdfg07pl409x4dy1l") (f (quote (("std") ("default" "std"))))))

(define-public crate-environmental-1.1.0 (c (n "environmental") (v "1.1.0") (h "1b0l06n2lnhmxwrjpy87abcvikfaxwmpskbw6nhsg7582i6pchn8") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-environmental-1.1.1 (c (n "environmental") (v "1.1.1") (h "1i763vqvirixiznkr6qq9wyhhgdr962pkw2684f0mc0wlzbshsji") (f (quote (("std") ("default" "std"))))))

(define-public crate-environmental-1.1.2 (c (n "environmental") (v "1.1.2") (h "0zkd3j8pgl1ix67ij9n2gj7h2jz7x6y7aph2i23rizfzbmss2xk5") (f (quote (("std") ("default" "std"))))))

(define-public crate-environmental-1.1.3 (c (n "environmental") (v "1.1.3") (h "15s72bfcvb19nzc5aqn9m3hwg5cj5wx9kffrsyai2i11ms4ikfb8") (f (quote (("std") ("default" "std"))))))

(define-public crate-environmental-1.1.4 (c (n "environmental") (v "1.1.4") (h "0nxiw6j1kwjr7719h8aj1hv1p65nw3afar0wsn1hx1xai8195374") (f (quote (("std") ("default" "std"))))))

