(define-module (crates-io en rg enrgy) #:use-module (crates-io))

(define-public crate-enrgy-0.1.0 (c (n "enrgy") (v "0.1.0") (d (list (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "form_urlencoded") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "path-tree") (r "^0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "qstring") (r "^0.7") (d #t) (k 0)))) (h "1irx5aw78qj6sav89f5dh1xczmncqg7sxkw1lxscjpaa9n41vk9q")))

(define-public crate-enrgy-0.2.0 (c (n "enrgy") (v "0.2.0") (d (list (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "form_urlencoded") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "path-tree") (r "^0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)))) (h "1gan5kkxd003qm468arwmgsvzx5nk1xfm4dnwlw553gaknj32yd3")))

(define-public crate-enrgy-0.2.1 (c (n "enrgy") (v "0.2.1") (d (list (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "form_urlencoded") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "path-tree") (r "^0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)))) (h "14pv4q1zl5raqp5jzfq4zidm49321qs39yhg9x8spl4ay9ai20q1")))

(define-public crate-enrgy-0.2.2 (c (n "enrgy") (v "0.2.2") (d (list (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "form_urlencoded") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "path-tree") (r "^0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)))) (h "149rskblskdr6m4dyfryrynbymq3fy6irzh87rw9ap462hnl89k5")))

