(define-module (crates-io en te enter) #:use-module (crates-io))

(define-public crate-enter-0.0.1 (c (n "enter") (v "0.0.1") (h "0qlpanymqlflmzr7j8sqdq4xg5qn1rnbfgajjldph876dlz4bfmy") (y #t)))

(define-public crate-enter-0.0.2 (c (n "enter") (v "0.0.2") (h "1k22f3gyiikia9xmdpmkkm70h7nc2l1wvhhkmklfz8g9bp3s2l84") (y #t)))

(define-public crate-enter-0.0.3 (c (n "enter") (v "0.0.3") (h "00glv8dx5xx03ps5v16r16n7r5faw8q9vhygny59dymcfih7wkfk") (y #t)))

(define-public crate-enter-0.0.4 (c (n "enter") (v "0.0.4") (h "1zdnzk96kijgm6vvaxz5ambf644caqg7hwiwwps99smhjfbxzfji") (y #t)))

(define-public crate-enter-1.0.0 (c (n "enter") (v "1.0.0") (h "0nmwb6mpmmd4gf60503sfhzyk1aslf39dpzydfx0nd3zwn4dmpry")))

