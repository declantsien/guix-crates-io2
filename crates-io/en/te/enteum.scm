(define-module (crates-io en te enteum) #:use-module (crates-io))

(define-public crate-enteum-0.1.0 (c (n "enteum") (v "0.1.0") (h "0y5nfpfpbfdv1yakihc6jgjr4i5vcwspv2qg4bixjsiyqzywadx2")))

(define-public crate-enteum-0.1.1 (c (n "enteum") (v "0.1.1") (h "1f86m2axv2dmcwfg7f3cbbfjqqgvy2ipakk2mgn9ckwkd2qwg9i5")))

