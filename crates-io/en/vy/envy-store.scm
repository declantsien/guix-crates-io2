(define-module (crates-io en vy envy-store) #:use-module (crates-io))

(define-public crate-envy-store-0.1.0 (c (n "envy-store") (v "0.1.0") (d (list (d (n "envy") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "rusoto_ssm") (r "^0.34") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "05s5fw3cbxav3jws1a97vikfr0xr3b3cniayzq2kyi9a0w4mqskq")))

