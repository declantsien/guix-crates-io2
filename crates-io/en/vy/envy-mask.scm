(define-module (crates-io en vy envy-mask) #:use-module (crates-io))

(define-public crate-envy-mask-0.4.0 (c (n "envy-mask") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.13") (d #t) (k 0)) (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "05qcxrxlj4isl62wk6a37km7ksy15nync5wly1mhvqncw0hgzdxw")))

(define-public crate-envy-mask-0.4.1 (c (n "envy-mask") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.13") (d #t) (k 0)) (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "1fcbp0qhg2n5xcmdc0hh8cv1qb7fpp7jwk4vm97chid5a5619wg6")))

(define-public crate-envy-mask-0.4.2 (c (n "envy-mask") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.13") (d #t) (k 0)) (d (n "clap") (r "^4.4.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "0zc52paj29158fx428f08ypslxp1qcq7qb6sam4hfb5v4hzp5qgn")))

