(define-module (crates-io en vy envy-rs) #:use-module (crates-io))

(define-public crate-envy-rs-1.3.0 (c (n "envy-rs") (v "1.3.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.15") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)))) (h "03n4lj5sx6qsi5v9fa662niyy1fr964mzmgj6s8v64lrpwx22yp6")))

(define-public crate-envy-rs-1.3.1 (c (n "envy-rs") (v "1.3.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.15") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)))) (h "12gj24in8079k9kaczymjxx55mmvj32p0arlyj59fsssxhy8z7l7")))

