(define-module (crates-io en vy envy) #:use-module (crates-io))

(define-public crate-envy-0.1.0 (c (n "envy") (v "0.1.0") (d (list (d (n "serde") (r "^0.7") (d #t) (k 0)))) (h "0ksm5crms7v8hpqqj579q5a7wvmnzclx8x71qsshn3lypfmjd9nq")))

(define-public crate-envy-0.1.1 (c (n "envy") (v "0.1.1") (d (list (d (n "serde") (r "^0.7") (d #t) (k 0)))) (h "14pn404g8lxa5rqz30733f5rj6pnkzfzfxvjixi3mfg0zq2mhhad")))

(define-public crate-envy-0.1.2 (c (n "envy") (v "0.1.2") (d (list (d (n "serde") (r "^0.8") (d #t) (k 0)))) (h "0qpwdgj7g28h31v6qsknxckgslybijnm67yi0l3fjjxsx5f8iwk1")))

(define-public crate-envy-0.2.0 (c (n "envy") (v "0.2.0") (d (list (d (n "serde") (r "^0.9") (d #t) (k 0)))) (h "18dcaswyyxjmj018acri3a14z6bv561ij11d2cx39l2pw990fxhr")))

(define-public crate-envy-0.3.0 (c (n "envy") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1rfjvzs0v62dxkahkknms140s0vy2m1ggnn8r65jbirrr2j0byyp")))

(define-public crate-envy-0.3.1 (c (n "envy") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0raj5rr269lgfi9vckx8wqrlar27hr8ha1dkdp96z44s54cz8j94")))

(define-public crate-envy-0.3.2 (c (n "envy") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0mbrmk6rb5yxv3ai968js61zv5rvycslnrzxyyq1v6sll89fd9qr")))

(define-public crate-envy-0.3.3 (c (n "envy") (v "0.3.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1pv97zbkdv1a4d8cn5840nh1awmyy8mc7c7z3n35lyq9sl3l8ri0")))

(define-public crate-envy-0.4.0 (c (n "envy") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1d1lk8kfjxyb2k92ndaqzgkbcvdnsxmzal83qw0jmx0krxmq66r6")))

(define-public crate-envy-0.4.1 (c (n "envy") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1a9ihmsqd819p109wyl24qawqhm1kkkw5nq2g5ry6pxpsnms8f7r")))

(define-public crate-envy-0.4.2 (c (n "envy") (v "0.4.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "0rar459p7pl19v6pbx98q3hi2hxfl8q1ndxxw5d4zd9cgway0irz")))

