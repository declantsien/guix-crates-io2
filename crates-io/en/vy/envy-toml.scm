(define-module (crates-io en vy envy-toml) #:use-module (crates-io))

(define-public crate-envy-toml-0.1.0 (c (n "envy-toml") (v "0.1.0") (d (list (d (n "envy") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "01k9aj5gzva7dbw42ldbxirnxryyxbshk1ngim5w210fcgdd8dam")))

