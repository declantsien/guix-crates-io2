(define-module (crates-io k2 so k2so) #:use-module (crates-io))

(define-public crate-k2so-0.1.0 (c (n "k2so") (v "0.1.0") (h "10ld5zpzy328bksrlq00zyn9gj0pjn7s6iacbsk4xnsnvsmxi61v")))

(define-public crate-k2so-2.0.0 (c (n "k2so") (v "2.0.0") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "19b6ppv6422sq5qpfjlvyw2hq8aaw39a32xhjg6cz7mhcls3ykm8")))

(define-public crate-k2so-2.1.0 (c (n "k2so") (v "2.1.0") (d (list (d (n "clap") (r "~2.19.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "13wcjxsjdkk1bpmb9d9h0bhx7clvwiwa5ffqpf41pbxw9d6xra16")))

