(define-module (crates-io k2 on k2on-structs) #:use-module (crates-io))

(define-public crate-k2on-structs-0.1.0 (c (n "k2on-structs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gnn3di7vxx6030f9yq9pkj757q7kqcf2r645ln1zzy8hgrld75z")))

