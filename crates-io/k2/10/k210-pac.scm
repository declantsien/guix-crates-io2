(define-module (crates-io k2 #{10}# k210-pac) #:use-module (crates-io))

(define-public crate-k210-pac-0.1.0 (c (n "k210-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "riscv") (r "^0.5.3") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0sbplcjlzym8ldjlkr96zk9y9pjxpba0l2wfn5hc0hvrm2lap8yl") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-k210-pac-0.1.1 (c (n "k210-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.4") (d #t) (k 0)) (d (n "riscv") (r "^0.5.3") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1wr4azylap93gb0g3b6wyfy1vg8hgb864ca6w28qv3h3fzqiv22c") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-k210-pac-0.2.0 (c (n "k210-pac") (v "0.2.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "riscv") (r "^0.5.4") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "03p3vlqd3p4fy7l4pyk57w8340lh3b81yfrpjbl5hdc41sicvcb9") (f (quote (("rt" "riscv-rt"))))))

