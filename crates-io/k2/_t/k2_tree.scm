(define-module (crates-io k2 _t k2_tree) #:use-module (crates-io))

(define-public crate-k2_tree-0.1.0 (c (n "k2_tree") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0gw2qm27cd78nf1qijdqdgsi424fkc7i1aq5a73783fnvxisqbxf")))

(define-public crate-k2_tree-0.2.0 (c (n "k2_tree") (v "0.2.0") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0cijvkwjkbrx6cws053ys38h74g2qqarp2sbbwbic9n98dd149g3")))

(define-public crate-k2_tree-0.2.1 (c (n "k2_tree") (v "0.2.1") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1d58w31z1cfh3f6xn9gss22g51yrhyagk3hrwgpd4nb7vwqbb7w1")))

(define-public crate-k2_tree-0.2.2 (c (n "k2_tree") (v "0.2.2") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "170kgsrg62b1vfkm5mzj0cynvgr0q4v64xsrr6mcvilsw0g5v2x4")))

(define-public crate-k2_tree-0.3.0 (c (n "k2_tree") (v "0.3.0") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "19z3xgpiksdmrcrkgx0aqz2bkv9vaix11h8mcngq7vacvmga6fsf")))

(define-public crate-k2_tree-0.3.1 (c (n "k2_tree") (v "0.3.1") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "149swm5rdwlf993086ygx750spddvxscx7a0jcvj9d37zxj3bips")))

(define-public crate-k2_tree-0.3.2 (c (n "k2_tree") (v "0.3.2") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "117aj039bg3vmz59xvl4qn9qa3x0z9y47r7k29y6v6wdm8429sia")))

(define-public crate-k2_tree-0.4.0 (c (n "k2_tree") (v "0.4.0") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "094xdydh2bx3l6kxdmr449ziwkiwj04c8d369496l1s55cbf2699")))

(define-public crate-k2_tree-0.4.1 (c (n "k2_tree") (v "0.4.1") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0jk1xr3hzq6mm1k2i4abjdi54rywby4nbg9fdi37j522934k08kk")))

(define-public crate-k2_tree-0.4.2 (c (n "k2_tree") (v "0.4.2") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "13b9dydppph1x65gdnqb5bdqbx8brf7g7j4q00c41468bpcnpfc3")))

(define-public crate-k2_tree-0.4.3 (c (n "k2_tree") (v "0.4.3") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1wllgmzf9fhaj0viqx54pinjs3savvhj41w73xpfy14n3fnfzd2p")))

(define-public crate-k2_tree-0.5.0 (c (n "k2_tree") (v "0.5.0") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "0mq7vvc6g25ga7ks4yrmswbsvw44xg8ikz2sp6ai0kvdwsgx2cgf")))

(define-public crate-k2_tree-0.5.1 (c (n "k2_tree") (v "0.5.1") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "0qa4yrnzqq44jsvn9gdl08gx19s849gghh1bwb2h9nn7m8bmsf4p")))

(define-public crate-k2_tree-0.5.2 (c (n "k2_tree") (v "0.5.2") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "0r4d3m6q26wr3azc8crc7ir470i3f9nym3jvjys83xhbczaq9mry")))

(define-public crate-k2_tree-0.5.3 (c (n "k2_tree") (v "0.5.3") (d (list (d (n "bitvec") (r "^0.17.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "042m1hpb5hmm4idx2afm4ji2vc9i9j254k22i31byck9q5v7dby7")))

