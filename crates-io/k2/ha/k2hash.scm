(define-module (crates-io k2 ha k2hash) #:use-module (crates-io))

(define-public crate-k2hash-0.0.1 (c (n "k2hash") (v "0.0.1") (d (list (d (n "k2hash-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.18") (d #t) (k 0)))) (h "14g1bl09smk2ffb4j0cz629wk8ccarws88s5g5r2319dj8mqvfqk")))

