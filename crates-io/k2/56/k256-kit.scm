(define-module (crates-io k2 #{56}# k256-kit) #:use-module (crates-io))

(define-public crate-k256-kit-0.1.0 (c (n "k256-kit") (v "0.1.0") (d (list (d (n "ecies") (r "^0.2") (d #t) (k 0)) (d (n "libsecp256k1") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0w2lsj55klv5dy4p44311q7wmzlys0j1c2fw0rvlvkijq8k81rgj")))

