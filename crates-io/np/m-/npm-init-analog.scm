(define-module (crates-io np m- npm-init-analog) #:use-module (crates-io))

(define-public crate-npm-init-analog-1.0.0 (c (n "npm-init-analog") (v "1.0.0") (d (list (d (n "color-eyre") (r "^0.6.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "0n30jsq5whymmzdl8lw22knlbz0cxpk3vr41w430b3iqysp8c48v")))

(define-public crate-npm-init-analog-1.1.0 (c (n "npm-init-analog") (v "1.1.0") (d (list (d (n "color-eyre") (r "^0.6.1") (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1niwymck54wsc1rs1a0dvwvswhxjj8bx3wx9zah71h71729pkj80")))

