(define-module (crates-io np m- npm-registry) #:use-module (crates-io))

(define-public crate-npm-registry-0.1.0 (c (n "npm-registry") (v "0.1.0") (h "0ash3s5p38qgfrz5n2lfmcdjlhsli13b1djdyzxz8xry8npzx73v")))

(define-public crate-npm-registry-0.2.0 (c (n "npm-registry") (v "0.2.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)))) (h "1d2wahx62skrvps2ib5agrxwsffrc35k76fxx7837171blb8dyjg")))

