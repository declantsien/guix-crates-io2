(define-module (crates-io np m- npm-readme-client) #:use-module (crates-io))

(define-public crate-npm-readme-client-0.2.0 (c (n "npm-readme-client") (v "0.2.0") (d (list (d (n "dotenv") (r "^0.10.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)))) (h "0hppmkddlnjkhhh8plpw6kig05j9ndby8y8ccycgbwhjhy1gbj1n")))

(define-public crate-npm-readme-client-0.3.0 (c (n "npm-readme-client") (v "0.3.0") (d (list (d (n "dotenv") (r "^0.10.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)))) (h "1kyb0r94jsmjvszk2lx3rsbvzhh3vn8lxqh04zdi46yqh6pir15r")))

