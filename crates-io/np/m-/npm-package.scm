(define-module (crates-io np m- npm-package) #:use-module (crates-io))

(define-public crate-npm-package-0.1.0 (c (n "npm-package") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0yp84asxl9gvzax3m0hfjl5zbfm8sfjg7i5n32r59pmgmybh38zz")))

(define-public crate-npm-package-0.2.0 (c (n "npm-package") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1yi29m31mmp9qpqkp2z9w10rxigdl82bg0h3w4vibb39jd9q9q9f")))

