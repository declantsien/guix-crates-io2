(define-module (crates-io np m- npm-name) #:use-module (crates-io))

(define-public crate-npm-name-0.1.0 (c (n "npm-name") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.302") (d #t) (k 2)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "quicli") (r "^0.3.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)))) (h "0dqvqj91kqlfmlrgy9q3gkjnr9iw0l47qwm1csp1fr026vx0jp80")))

(define-public crate-npm-name-0.1.1 (c (n "npm-name") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.302") (d #t) (k 2)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "quicli") (r "^0.3.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)))) (h "0i7g3dw50gzm0m7mjgjg2nzgvca7k2mwnbwahn80vrfn5ma7b98b")))

