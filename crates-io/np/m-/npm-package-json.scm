(define-module (crates-io np m- npm-package-json) #:use-module (crates-io))

(define-public crate-npm-package-json-0.0.0 (c (n "npm-package-json") (v "0.0.0") (h "0zwvv2vx2222hw5zmbx9z8w2by70kn7a8ps5lyhdgm3f0g6vy1hr")))

(define-public crate-npm-package-json-0.1.0 (c (n "npm-package-json") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1xr8fxd6hrdkp5l7dgfv50s2iq25f1z3wvbwjgzd7309rk8vxgf2")))

(define-public crate-npm-package-json-0.1.1 (c (n "npm-package-json") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "04n3nc0bv0dms78g8l74idngxamzaln8rsh785v1avwjzn6a4gyl")))

(define-public crate-npm-package-json-0.1.2 (c (n "npm-package-json") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0py08q5vbw6493cn2kwpwzi80vryzcmc3q3vpkkg203mrpksh4w3")))

(define-public crate-npm-package-json-0.1.3 (c (n "npm-package-json") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1phzpfzx5xhmlvd5scg3v0n9a2rdmfwm4s8ibr6nbmhvfy4ky5nz")))

