(define-module (crates-io np x- npx-bin-helper) #:use-module (crates-io))

(define-public crate-npx-bin-helper-0.1.0 (c (n "npx-bin-helper") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)) (d (n "serial_test") (r "^0.9.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "xflags") (r "^0.3.1") (d #t) (k 0)))) (h "1ybw4i2wxy6wy5zb22ibz2mcm5mcisybfzfd33a978xhqcvrqvpi")))

(define-public crate-npx-bin-helper-0.2.0 (c (n "npx-bin-helper") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)) (d (n "serial_test") (r "^0.9.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "xflags") (r "^0.3.1") (d #t) (k 0)))) (h "0gscqxqry7qdi0sg43fp08wwzd4n2qbbk9s0gnkyzki9wjvl255w")))

