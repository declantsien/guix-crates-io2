(define-module (crates-io np hy nphysics) #:use-module (crates-io))

(define-public crate-nphysics-0.1.0 (c (n "nphysics") (v "0.1.0") (d (list (d (n "ncollide") (r "*") (d #t) (k 0)))) (h "1qknb70n7fx6iqpsnws5577rr3symi83z47vjqzjrhd06g49lmp8") (y #t)))

(define-public crate-nphysics-0.1.1 (c (n "nphysics") (v "0.1.1") (h "166jidvadzislp7iccbyywryj7fl5qny8v3pyzkibfbv4gxgwr77") (y #t)))

(define-public crate-nphysics-0.1.2 (c (n "nphysics") (v "0.1.2") (h "1slv20inpz425a99mx8g3hxiw0fjam580fsk383k3k6k6cd97pzi") (y #t)))

(define-public crate-nphysics-0.1.3 (c (n "nphysics") (v "0.1.3") (h "079kmvxhvz4smf6lrbcwfchqfp0b3v1ycjmycbd32ps2xm7qyx5d") (y #t)))

(define-public crate-nphysics-0.1.4 (c (n "nphysics") (v "0.1.4") (h "09xfsnsqhdh3snrr7ylddz5bl10ddr2nns7qn1fcxkfrws27r7ns") (y #t)))

