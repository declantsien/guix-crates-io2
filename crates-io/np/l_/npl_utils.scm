(define-module (crates-io np l_ npl_utils) #:use-module (crates-io))

(define-public crate-npl_utils-0.1.0 (c (n "npl_utils") (v "0.1.0") (d (list (d (n "radio_datetime_utils") (r "^0.3.0") (d #t) (k 0)))) (h "1vg8mybvk9r41jijs9rjwrz64f9fwm9vy182f580d4dyw73zh7vr")))

(define-public crate-npl_utils-0.2.0 (c (n "npl_utils") (v "0.2.0") (d (list (d (n "radio_datetime_utils") (r "^0.4.2") (d #t) (k 0)))) (h "1a0s6d8hyy2szrqpmhfc9dkdmk5493p1ylklp4xs06h7jl249811")))

(define-public crate-npl_utils-0.2.1 (c (n "npl_utils") (v "0.2.1") (d (list (d (n "radio_datetime_utils") (r "^0.4.2") (d #t) (k 0)))) (h "07hf8cwailym83lkpzj7107ajrvf33h6902lx3nf11ns2h70ncg3")))

(define-public crate-npl_utils-0.3.0 (c (n "npl_utils") (v "0.3.0") (d (list (d (n "radio_datetime_utils") (r "^0.5.0") (d #t) (k 0)))) (h "12bq1h2jvy9rhh9kgb9rz2f64krx3sqixyb9axja4cvkngjgls76")))

(define-public crate-npl_utils-0.3.1 (c (n "npl_utils") (v "0.3.1") (d (list (d (n "radio_datetime_utils") (r "^0.5.0") (d #t) (k 0)))) (h "1790496ib3jg06sdxbanvhfim0471aymv5m22ric6gq8w46rhj8c")))

