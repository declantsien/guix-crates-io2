(define-module (crates-io np ki npkill-rs) #:use-module (crates-io))

(define-public crate-npkill-rs-0.1.0 (c (n "npkill-rs") (v "0.1.0") (d (list (d (n "bytesize") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0kxmvys6s4xf9vzlryf2qhplriqqwz6llv39cs0901mf97h1f2gc")))

