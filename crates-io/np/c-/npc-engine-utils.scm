(define-module (crates-io np c- npc-engine-utils) #:use-module (crates-io))

(define-public crate-npc-engine-utils-0.1.0 (c (n "npc-engine-utils") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "npc-engine-core") (r "^0.1") (f (quote ("graphviz"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "12bhw2kjykn0bcid3dcyppzi2d1jka64vvxmffbpcr6rfqj9aykx")))

