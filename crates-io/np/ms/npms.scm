(define-module (crates-io np ms npms) #:use-module (crates-io))

(define-public crate-npms-0.1.0 (c (n "npms") (v "0.1.0") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "loa") (r "^0.1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0bdcqhdw5hw0nnp4s3wq5cxyjcymwiwpprgqc0xcychpvlmrd3g4")))

(define-public crate-npms-0.1.1 (c (n "npms") (v "0.1.1") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "loa") (r "^0.1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "rustls-tls" "rustls-tls-webpki-roots" "rustls-tls-manual-roots"))) (d #t) (k 0)))) (h "158qw8rswrf10spv88lj9a3756p4qvw7ni1wqn9a5ngyb0rk0fsk")))

(define-public crate-npms-0.1.2 (c (n "npms") (v "0.1.2") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "loa") (r "^0.1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "rustls-tls" "rustls-tls-webpki-roots" "rustls-tls-manual-roots"))) (d #t) (k 0)))) (h "09x3ixszapnhzs36fc04lgvil3xji36dg4yqcnj3k4m5ggipqmv9")))

