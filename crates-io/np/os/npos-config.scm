(define-module (crates-io np os npos-config) #:use-module (crates-io))

(define-public crate-npos-config-0.0.1 (c (n "npos-config") (v "0.0.1") (d (list (d (n "frame-election-provider-support") (r "^26.0.0") (k 0)) (d (n "frame-support") (r "^26.0.0") (d #t) (k 0)) (d (n "pallet-election-provider-multi-phase") (r "^25.0.0") (k 0)) (d (n "sp-npos-elections") (r "^24.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^29.0.0") (k 0)))) (h "1ccgdvqbhlp6zf6czbhw3msd94drz4bnndslg37wppr8mxd9izmy")))

(define-public crate-npos-config-0.0.2 (c (n "npos-config") (v "0.0.2") (d (list (d (n "frame-election-provider-support") (r "^27.0.0") (k 0)) (d (n "frame-support") (r "^27.0.0") (d #t) (k 0)) (d (n "pallet-election-provider-multi-phase") (r "^26.0.0") (k 0)) (d (n "sp-npos-elections") (r "^25.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^30.0.1") (k 0)))) (h "1641xd4h3m78vkb8rpfgmc4gld9mwi6v1d10h7rrbwgj9rh1dlq1")))

(define-public crate-npos-config-0.0.3 (c (n "npos-config") (v "0.0.3") (d (list (d (n "frame-election-provider-support") (r "^27.0.0") (k 0)) (d (n "frame-support") (r "^27.0.0") (d #t) (k 0)) (d (n "pallet-election-provider-multi-phase") (r "^26.0.0") (k 0)) (d (n "sp-npos-elections") (r "^25.0.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^30.0.1") (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ap256a00vnpndwdhvw96lycsh54bsifl8n29j6p7qv7fvfx4bd5")))

