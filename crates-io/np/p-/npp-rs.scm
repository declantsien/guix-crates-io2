(define-module (crates-io np p- npp-rs) #:use-module (crates-io))

(define-public crate-npp-rs-0.0.1 (c (n "npp-rs") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "cuda-runtime-sys") (r "^0.3.0-alpha.1") (d #t) (k 2)) (d (n "image") (r "^0.23.13") (d #t) (k 0)) (d (n "npp-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)) (d (n "rustacuda") (r "^0.1") (d #t) (k 0)) (d (n "rustacuda_core") (r "^0.1") (d #t) (k 0)) (d (n "rustacuda_derive") (r "^0.1") (d #t) (k 0)))) (h "181dw6w4achh4c0h089yvcpch3nii27j26awhjwkb8w0agbxlq4j")))

