(define-module (crates-io np nc npnc) #:use-module (crates-io))

(define-public crate-npnc-0.1.0 (c (n "npnc") (v "0.1.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "hazard") (r "^0.2.0") (d #t) (k 0)) (d (n "queuecheck") (r "^0.1.0") (d #t) (k 2)))) (h "1izvq2d3lprf98scn0vmydgs2wlzj35mviglgqp6p31kcxmwcz47") (f (quote (("valgrind"))))))

(define-public crate-npnc-0.1.1 (c (n "npnc") (v "0.1.1") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "hazard") (r "^0.2.0") (d #t) (k 0)) (d (n "queuecheck") (r "^0.1.0") (d #t) (k 2)))) (h "0widn43b8dnpsrw0pflj5nx9fagi8rkymjay0vfd7ka0z2rzpn9i") (f (quote (("valgrind"))))))

(define-public crate-npnc-0.2.0 (c (n "npnc") (v "0.2.0") (d (list (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "hazard") (r "^0.3.0") (d #t) (k 0)) (d (n "queuecheck") (r "^0.1.0") (d #t) (k 2)))) (h "09s7clknf313wsch2r43sipqnhl76kb8by6c8iac5s99b1knqv6x") (f (quote (("valgrind"))))))

(define-public crate-npnc-0.2.1 (c (n "npnc") (v "0.2.1") (d (list (d (n "hazard") (r "^0.3.0") (d #t) (k 0)) (d (n "queuecheck") (r "^0.1.0") (d #t) (k 2)))) (h "1mclrzq97p49d6lc251kd286f00j4vqsmr824l74mwn9529sy9b8") (f (quote (("valgrind"))))))

