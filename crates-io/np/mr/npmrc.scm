(define-module (crates-io np mr npmrc) #:use-module (crates-io))

(define-public crate-npmrc-0.1.0 (c (n "npmrc") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.186") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_ini") (r "^0.1.3") (d #t) (k 0)))) (h "13hf28awh9hj2ph0s5wvnfq03m8fj9vhiv5zqhsiggllqzmg5sxq")))

(define-public crate-npmrc-0.1.1 (c (n "npmrc") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.186") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_ini") (r "^0.1.3") (d #t) (k 0)))) (h "16qwqk1jmaib1a0s4695v8zji2mqc7nc7r2s5c346blgv3y9l04a")))

(define-public crate-npmrc-0.1.2 (c (n "npmrc") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0.186") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_ini") (r "^0.1.3") (d #t) (k 0)))) (h "064s46gpqxklcxy8i1izc6vd9k9y4z3r57bmc4m65rdd263841qd")))

(define-public crate-npmrc-0.2.0 (c (n "npmrc") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_ini") (r "^0.1.3") (d #t) (k 0)))) (h "0xfa83nscnnnnpyz3va8lmk02hzzfwkxy9wjsf42292zybbiqcf7")))

