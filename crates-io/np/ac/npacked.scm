(define-module (crates-io np ac npacked) #:use-module (crates-io))

(define-public crate-npacked-0.1.0 (c (n "npacked") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 2)) (d (n "lz4") (r "^1.10") (d #t) (k 0)))) (h "006wgj4ax90p827nii307blha9rwzsji9qa3n8lx6i88g9qklzij") (y #t)))

