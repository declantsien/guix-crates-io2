(define-module (crates-io np ch npchk) #:use-module (crates-io))

(define-public crate-npchk-0.1.0 (c (n "npchk") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "hyper") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.7.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.6") (d #t) (k 0)) (d (n "xmltree") (r "^0.6.1") (d #t) (k 0)))) (h "0907xkr8v4wk1gk8x690nnsrcw0nwm02k4znmncn7nkz7r1g5wpv")))

(define-public crate-npchk-0.1.1 (c (n "npchk") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "hyper") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.7.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.6") (d #t) (k 0)) (d (n "xmltree") (r "^0.6.1") (d #t) (k 0)))) (h "1ivpqnpjz5jh25dwlrv9mahyq4lhkbcknmkngpk425pd5mw8pm0a")))

(define-public crate-npchk-0.1.2 (c (n "npchk") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "hyper") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.7.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.6") (d #t) (k 0)) (d (n "xmltree") (r "^0.6.1") (d #t) (k 0)))) (h "0iqisangvz16ibkcm719pcx0q7lxfjcixays7lf3a28bz7wbpijm")))

(define-public crate-npchk-0.1.3 (c (n "npchk") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "hyper") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.7.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.6") (d #t) (k 0)) (d (n "xmltree") (r "^0.6.1") (d #t) (k 0)))) (h "0gqbw6ssygz9rxidmpa0fw9i2f911dbncdq7mi1dg07hsi05zn8j")))

(define-public crate-npchk-0.1.4 (c (n "npchk") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "hyper") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.7.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.6") (d #t) (k 0)) (d (n "xmltree") (r "^0.6.1") (d #t) (k 0)))) (h "0rxhrja8qr3y2ay3zm7fb6g501g0i6yi4nqfcr6xiq01ch1w699n")))

