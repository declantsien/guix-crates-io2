(define-module (crates-io np yz npyz-derive) #:use-module (crates-io))

(define-public crate-npyz-derive-0.5.0 (c (n "npyz-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0gpga0if47h7mrj275lz0gy3gg4jqhx7f3z5186xfy9fdm8zzmxf")))

(define-public crate-npyz-derive-0.6.0 (c (n "npyz-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0c0acf1xpn09fgc7lx91p65pn2602cq070jmk9mk12h5rm75w0ls")))

(define-public crate-npyz-derive-0.7.0 (c (n "npyz-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1vcsyqd5x7q01bccc2757lny0dqsw39km0gkn094p6ia5xnbv1d2")))

