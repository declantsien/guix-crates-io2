(define-module (crates-io np -l np-listener) #:use-module (crates-io))

(define-public crate-np-listener-0.1.0 (c (n "np-listener") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "default-net") (r "^0.17") (d #t) (k 0)) (d (n "pnet") (r "^0.34") (d #t) (k 0)))) (h "0kzg9m3nd2kgscy5r2lb0yk9y3ds62v79q29005nqwqabh63aam0")))

(define-public crate-np-listener-0.2.0 (c (n "np-listener") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "default-net") (r "^0.17") (d #t) (k 0)) (d (n "pnet") (r "^0.34") (d #t) (k 0)))) (h "1n8ci02sqxbzhj1xiixjr8f3cwhxwgp8irvlycj0lnlqzhg9v801")))

(define-public crate-np-listener-0.3.0 (c (n "np-listener") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "default-net") (r "^0.17") (d #t) (k 0)) (d (n "pnet") (r "^0.34") (d #t) (k 0)))) (h "184j9wwg2fgkv4h1xn30whzljbycglgar4azr2sppapkrqk1hhin")))

(define-public crate-np-listener-0.4.0 (c (n "np-listener") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "default-net") (r "^0.17") (d #t) (k 0)) (d (n "pnet") (r "^0.34") (d #t) (k 0)))) (h "06yjchj5l2kr2v103vlay73chna2qsgndqs680sasaxgq9vb68m5")))

(define-public crate-np-listener-0.5.0 (c (n "np-listener") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "default-net") (r "^0.17") (d #t) (k 0)) (d (n "pnet") (r "^0.34") (d #t) (k 0)))) (h "1k96w5fxn8lypjqcl97mk3k86rkxrsgncm9xjg5605z6kc2459ir")))

(define-public crate-np-listener-0.6.0 (c (n "np-listener") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "default-net") (r "^0.17") (d #t) (k 0)) (d (n "pnet") (r "^0.34") (d #t) (k 0)))) (h "0garh2qz9xfx3m3d6xkcfwpcrc24hz0yxi1cjbrr6x4p7m50fcic")))

(define-public crate-np-listener-0.7.0 (c (n "np-listener") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "default-net") (r "^0.17") (d #t) (k 0)) (d (n "pnet") (r "^0.34") (d #t) (k 0)))) (h "19q0zws03z0b2sy6yh9s7i4paqy8cv8hcr2kv2nlaqxgxs328fl8")))

