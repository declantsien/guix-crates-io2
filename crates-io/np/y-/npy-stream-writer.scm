(define-module (crates-io np y- npy-stream-writer) #:use-module (crates-io))

(define-public crate-npy-stream-writer-0.1.0 (c (n "npy-stream-writer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 2)))) (h "0v3kyav4zpbn6r21k2xwipzvddz1cbgg6zim5dgnlck7fk33r9gj")))

