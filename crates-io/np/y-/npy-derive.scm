(define-module (crates-io np y- npy-derive) #:use-module (crates-io))

(define-public crate-npy-derive-0.1.0 (c (n "npy-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0017fwwwp6pr171a548z83lnl200k68z2vchllcrq94r7x5q9475")))

(define-public crate-npy-derive-0.2.0 (c (n "npy-derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "042v5d6jqx5jfff592cfhihj2k9sqz1qdlmygfcimcaibd7rphx4")))

(define-public crate-npy-derive-0.2.1 (c (n "npy-derive") (v "0.2.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "18zdqip2a4a0vsg5wx6hfnijv4cys0zywq3fg7bqzppn6wwa00jy")))

(define-public crate-npy-derive-0.3.0 (c (n "npy-derive") (v "0.3.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0x5kflxq7g0cgfbgk76h5xpqwd8n8sr2jiip3c6xrwiih73an1sx")))

(define-public crate-npy-derive-0.3.1 (c (n "npy-derive") (v "0.3.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "0234fv2gjfqq550asjs8rjnjsd86rik2iq9r6mbbni65lxsyflkl")))

(define-public crate-npy-derive-0.4.0 (c (n "npy-derive") (v "0.4.0") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "14w6m80jn7s184m3nngchpb0as2kmjq4ll2riq8g5bjkd3gi61b9")))

