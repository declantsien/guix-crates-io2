(define-module (crates-io np ha nphard) #:use-module (crates-io))

(define-public crate-nphard-0.1.0 (c (n "nphard") (v "0.1.0") (h "0sq1806q0dsz4r1y4z94van7ss6gk9zyggmy4qn2wfz8myfgxgvs") (y #t)))

(define-public crate-nphard-0.1.1 (c (n "nphard") (v "0.1.1") (h "1i509d76mw21cby8v8qgrnv4gk6hz81dv53sax82wpxzmjzhm2dw")))

