(define-module (crates-io np m_ npm_time_machine) #:use-module (crates-io))

(define-public crate-npm_time_machine-1.0.0 (c (n "npm_time_machine") (v "1.0.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.17") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (f (quote ("std" "preserve_order"))) (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("macros" "parsing" "serde"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1znkhv1q1jkw0rigsvanjrgcqbpr9ilsm5msys2gj0vbhniw52wm")))

