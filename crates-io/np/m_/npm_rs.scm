(define-module (crates-io np m_ npm_rs) #:use-module (crates-io))

(define-public crate-npm_rs-0.1.0 (c (n "npm_rs") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "032iaa0xyc3q0661anc2pw9hmmyss6n35dl1922q157if48yfr3b")))

(define-public crate-npm_rs-0.1.1 (c (n "npm_rs") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "19v1blcgy5v9jsf0q1zpd3rgk9ip7xxk6sh85sf8g3q8am3qbsx8") (f (quote (("nightly"))))))

(define-public crate-npm_rs-0.1.2 (c (n "npm_rs") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "1i9l7qaygywvgaad5k8nv5l005p0h0wj487h5wzvxl3qqlpikdpc") (f (quote (("nightly"))))))

(define-public crate-npm_rs-0.2.0 (c (n "npm_rs") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "03mm8qmxhql4iqsar51nl45kqlwr2q25aj5kmck3ns9smf0qzrjl") (f (quote (("nightly")))) (y #t)))

(define-public crate-npm_rs-0.2.1 (c (n "npm_rs") (v "0.2.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "09188hdldyw4msr53w802fywqaq7f419jhvhxrgirzsqfi2b6r7n") (f (quote (("nightly"))))))

(define-public crate-npm_rs-1.0.0 (c (n "npm_rs") (v "1.0.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "0mbv0dv8zy57j0rqa06dgi5xr0sp8h4lmbzqxxq2amiwr93l6id1") (r "1.57")))

