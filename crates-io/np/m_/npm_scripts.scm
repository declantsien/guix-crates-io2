(define-module (crates-io np m_ npm_scripts) #:use-module (crates-io))

(define-public crate-npm_scripts-0.1.0 (c (n "npm_scripts") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zvk67jn855inbbwzgbq8cha81wg60hhypq28cki03smrygbx5ip")))

(define-public crate-npm_scripts-0.1.1 (c (n "npm_scripts") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02i18gl34jjqp3x5ki72ih1n1q6j5dx12cb3h32ibgqwprwia8v3")))

(define-public crate-npm_scripts-0.1.2 (c (n "npm_scripts") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cai3dnc8a8h227wdikpr0yinkkc5vkwkg7lzl5x75paicwh2f0r")))

(define-public crate-npm_scripts-0.2.0 (c (n "npm_scripts") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rgkkjld44w7iz4h8gqfxpaz8inp0j963fjasw5d2cg7vndw5r76")))

