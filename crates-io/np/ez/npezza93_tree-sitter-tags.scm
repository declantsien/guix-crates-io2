(define-module (crates-io np ez npezza93_tree-sitter-tags) #:use-module (crates-io))

(define-public crate-npezza93_tree-sitter-tags-0.20.2 (c (n "npezza93_tree-sitter-tags") (v "0.20.2") (d (list (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter") (r ">=0.20") (d #t) (k 0)))) (h "0aw6j1xyvp5b1a1p8yyw1jggfy3xv5i90h3l9svq79xblmyjmf6c")))

(define-public crate-npezza93_tree-sitter-tags-0.21.0 (c (n "npezza93_tree-sitter-tags") (v "0.21.0") (d (list (d (n "memchr") (r "^2.7.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tree-sitter") (r ">=0.20") (d #t) (k 0)))) (h "0vfpl1xkp3by9srr1jlcqldn3r1k17n7zl7an6871vwh4f6ab2hh") (r "1.70")))

(define-public crate-npezza93_tree-sitter-tags-0.21.1 (c (n "npezza93_tree-sitter-tags") (v "0.21.1") (d (list (d (n "memchr") (r "^2.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tree-sitter") (r ">=0.20") (d #t) (k 0)))) (h "0ir7fnr6ln7h7xy10hp7n3cv5irzsn1h14fqa9zmfs2palbbwl0q")))

