(define-module (crates-io np ez npezza93-tree-sitter-ruby) #:use-module (crates-io))

(define-public crate-npezza93-tree-sitter-ruby-0.20.2 (c (n "npezza93-tree-sitter-ruby") (v "0.20.2") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.22") (d #t) (k 0)))) (h "08d90r9ngnlla46v1f7p8qi7gbb6qlqrsbgd5kl93l8hifba12c9")))

