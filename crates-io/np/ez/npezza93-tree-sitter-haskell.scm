(define-module (crates-io np ez npezza93-tree-sitter-haskell) #:use-module (crates-io))

(define-public crate-npezza93-tree-sitter-haskell-0.14.1 (c (n "npezza93-tree-sitter-haskell") (v "0.14.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r "^0.20") (d #t) (k 0)))) (h "1yx7ds0qrvmsdnpl6ysbxpx2ix85p0wr8khq9fy9cbcniblnn14a")))

(define-public crate-npezza93-tree-sitter-haskell-0.15.1 (c (n "npezza93-tree-sitter-haskell") (v "0.15.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.22") (d #t) (k 0)))) (h "1c2l7lgf3skhhdl4815dm8dgpf795dk2qqi83knzpfipkb0dyi6d")))

