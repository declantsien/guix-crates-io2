(define-module (crates-io np ez npezza93-tree-sitter-swift) #:use-module (crates-io))

(define-public crate-npezza93-tree-sitter-swift-0.4.4 (c (n "npezza93-tree-sitter-swift") (v "0.4.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "tree-sitter") (r ">=0.22") (d #t) (k 0)))) (h "1j7j89nnyqklxd0c60x6rfn0gjs69ykczknf57l3q4nrm1k0kp37")))

