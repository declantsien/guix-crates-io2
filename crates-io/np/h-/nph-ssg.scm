(define-module (crates-io np h- nph-ssg) #:use-module (crates-io))

(define-public crate-nph-ssg-0.1.0 (c (n "nph-ssg") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.12") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8.0") (d #t) (k 0)))) (h "0p41dzacz3rdv7qnriqfd2awnz9a0clhrgwrbzhn9947h6ppkpcm")))

