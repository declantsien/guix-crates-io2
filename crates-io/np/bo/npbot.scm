(define-module (crates-io np bo npbot) #:use-module (crates-io))

(define-public crate-npbot-1.0.0 (c (n "npbot") (v "1.0.0") (d (list (d (n "dotenv") (r "^0.11.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "mammut") (r "^0.10.1") (d #t) (k 0)) (d (n "quicli") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.30") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.30") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_xml") (r "^0.9.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.5") (d #t) (k 0)) (d (n "url") (r "^1.7.0") (d #t) (k 0)))) (h "1c0cqcbsympziyr904kdyzhmfnmlj3pzkij3xsv7ngwn03zrzi2h")))

