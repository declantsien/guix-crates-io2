(define-module (crates-io np kl npkl) #:use-module (crates-io))

(define-public crate-npkl-1.0.0 (c (n "npkl") (v "1.0.0") (d (list (d (n "byte-unit") (r "^4.0.17") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0x4makqfd6im0crz5pkg944l5h352fk1pcswiy2dsn7zfrinifw3")))

(define-public crate-npkl-1.0.1 (c (n "npkl") (v "1.0.1") (d (list (d (n "byte-unit") (r "^4.0.19") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0a2haa6plrd08bywnlfz4c91jl268q812mq0b7wfhhirlsw6wcxb")))

(define-public crate-npkl-1.0.2 (c (n "npkl") (v "1.0.2") (d (list (d (n "byte-unit") (r "^5.1.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "00qfick0kb29c31wy1f5a3dcsbw4vkm787jx6iw2pf2whyvmdq9i")))

