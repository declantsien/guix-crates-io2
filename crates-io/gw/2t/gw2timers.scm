(define-module (crates-io gw #{2t}# gw2timers) #:use-module (crates-io))

(define-public crate-gw2timers-0.1.0 (c (n "gw2timers") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "12b8k2wr3aasrp6bcdhksghzvpcp54ixxl2w2nmymhhcjklzmqih")))

(define-public crate-gw2timers-0.2.0 (c (n "gw2timers") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "119rvafif5jfb4svvi51x3r61sfrssiyy6pfiidsi8wzmysn078k")))

(define-public crate-gw2timers-0.3.0 (c (n "gw2timers") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "1pmfffg55kgrnb6hzj9xpbkdkxl7rc6ak4rnxlc0il9j49myawf1")))

(define-public crate-gw2timers-0.3.1 (c (n "gw2timers") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "1q792hk0hy5c73d3fvmz8r01acdzfla862iclyf26vnf62181s61")))

(define-public crate-gw2timers-0.4.0 (c (n "gw2timers") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "14xg9ddl0i6cz457x7i0cvci76hg2mkki36w2a5f8amjp5bzjrzm")))

(define-public crate-gw2timers-0.4.1 (c (n "gw2timers") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "1dcalvrqcjm86kzmah81is2qn1mwfh7llkw3biacf920r6yvrd1m")))

