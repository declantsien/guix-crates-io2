(define-module (crates-io gw #{2l}# gw2lib-model) #:use-module (crates-io))

(define-public crate-gw2lib-model-1.0.0 (c (n "gw2lib-model") (v "1.0.0") (d (list (d (n "either") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "08jr6i4s8bw9psvr5vd4lks37hp411vcxmn42pxi23ijgh8smy2n")))

(define-public crate-gw2lib-model-1.0.1 (c (n "gw2lib-model") (v "1.0.1") (d (list (d (n "either") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v5x5yz5igslq570626fmgq3wj2yjiyjn8mh72rdqc4bc608q1f2")))

(define-public crate-gw2lib-model-1.0.2 (c (n "gw2lib-model") (v "1.0.2") (d (list (d (n "either") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "193fvhqrsxvqwyqrwkzgzz0dqnv0svzcz3g1rag60bq6d649jpmr")))

(define-public crate-gw2lib-model-1.1.0 (c (n "gw2lib-model") (v "1.1.0") (d (list (d (n "either") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c62g5mdrq3g9ifmlm4iha6q37fpx1sb0xsdcpasncw0nzvh34yk")))

(define-public crate-gw2lib-model-1.2.0 (c (n "gw2lib-model") (v "1.2.0") (d (list (d (n "either") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bjvwh4fcks7xz2sx12wq9cmd5l3xp49sip37g2apslm5r1440xs")))

(define-public crate-gw2lib-model-2.0.0 (c (n "gw2lib-model") (v "2.0.0") (d (list (d (n "either") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fg2qp147frwnh6v4fx6l1krv2a2i60kgyafb2vxls66vkx16zqm")))

(define-public crate-gw2lib-model-2.0.1 (c (n "gw2lib-model") (v "2.0.1") (d (list (d (n "either") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "1d0caan2wi5rkyv8ir8hd3n1c5pn3y5wb02y7vvzmhns5v3lx2w3")))

(define-public crate-gw2lib-model-2.0.2 (c (n "gw2lib-model") (v "2.0.2") (d (list (d (n "either") (r "^1.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.150") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qlj7yvj67lva8yiz2s1lza3a289zgq7vlsba57mlzwf12106fz5")))

(define-public crate-gw2lib-model-2.0.3 (c (n "gw2lib-model") (v "2.0.3") (d (list (d (n "either") (r "^1.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1i6c8jls5njig1laf79fspkzkjiqy7zy2q22v4sn4mmk120y6km5")))

(define-public crate-gw2lib-model-2.0.4 (c (n "gw2lib-model") (v "2.0.4") (d (list (d (n "either") (r "^1.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ja10m0sqc8fc57zlkkr4b159w7vg7d4xlx089ikyhnahz42ssqb")))

(define-public crate-gw2lib-model-2.0.5 (c (n "gw2lib-model") (v "2.0.5") (d (list (d (n "either") (r "^1.8.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.150") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "08jxsgz3h7gcdyfrp8103vi80knnz1dblnbxph8h0a4ajgjy6gkb")))

(define-public crate-gw2lib-model-2.0.6 (c (n "gw2lib-model") (v "2.0.6") (d (list (d (n "either") (r "^1.8.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0gh646ng118dl12svpf53s5kl33qzp2qhbdc21lrh2jz57gqr1cj")))

(define-public crate-gw2lib-model-2.1.0 (c (n "gw2lib-model") (v "2.1.0") (d (list (d (n "either") (r "^1.8.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0y0rwi2ncrbp9gpnky1kzb75d1aqiinbfabk9l2ac3w6y4shg7qv")))

(define-public crate-gw2lib-model-2.1.1 (c (n "gw2lib-model") (v "2.1.1") (d (list (d (n "either") (r "^1.8.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "1y8lvrrp4yvnagncqcyc2180p4dc1v6w92glar23ilm39c0d7q3d")))

(define-public crate-gw2lib-model-2.1.2 (c (n "gw2lib-model") (v "2.1.2") (d (list (d (n "either") (r "^1.8.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "02s1shbhp88hhw6a05mwipawidlhp7gk7dqjh3m0r3942br2bz3m")))

(define-public crate-gw2lib-model-2.1.3 (c (n "gw2lib-model") (v "2.1.3") (d (list (d (n "either") (r "^1.8.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0sbbl43p6x5wazg4rklfymhkyymnsf5g10wx7fd64gqrp53apvn5")))

(define-public crate-gw2lib-model-2.1.4 (c (n "gw2lib-model") (v "2.1.4") (d (list (d (n "either") (r "^1.8.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0852yjpgh1way555dnkk357rjjisjws569p0rlkkmj1idia9b516")))

(define-public crate-gw2lib-model-2.1.5 (c (n "gw2lib-model") (v "2.1.5") (d (list (d (n "either") (r "^1.8.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_tuple") (r "^0.5.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "16n4y56x435xabwsrss8vjcpxhrpissa2f79q247cqm2y3lyfrm7")))

