(define-module (crates-io gw #{2a}# gw2api) #:use-module (crates-io))

(define-public crate-gw2api-0.2.0 (c (n "gw2api") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "ureq") (r "^1.1.1") (f (quote ("json"))) (d #t) (k 0)))) (h "054nz0jlgdvrgca9sjg1rydcpj1p8wzj5d0960p5qlx131q9gpf0")))

