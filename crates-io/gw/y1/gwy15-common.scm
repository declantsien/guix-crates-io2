(define-module (crates-io gw y1 gwy15-common) #:use-module (crates-io))

(define-public crate-gwy15-common-0.1.0 (c (n "gwy15-common") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.3") (k 0)) (d (n "semver") (r "^1.0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1fzwwb209rj426a701dpdj3z850lxvzbk3xrmjxyc6lkkli2ixhc") (f (quote (("rustls" "reqwest/rustls-tls") ("native-tls" "reqwest/native-tls") ("default" "rustls"))))))

