(define-module (crates-io gw #{2_}# gw2_taco_parser) #:use-module (crates-io))

(define-public crate-gw2_taco_parser-0.1.0 (c (n "gw2_taco_parser") (v "0.1.0") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kqznljc1zyqfpdxal54c7cmm267c0wpyls4ad6k9h18z28yzvqd")))

(define-public crate-gw2_taco_parser-0.2.0 (c (n "gw2_taco_parser") (v "0.2.0") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jqvpm446dcvjinfc4zim6ny6g0dkr4mlgqnj3a5xmy44q2mb37q")))

(define-public crate-gw2_taco_parser-0.2.1 (c (n "gw2_taco_parser") (v "0.2.1") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cj7a1d486aa80b8hj6n6bjyax457r028zypw0pq7ha1mmbik62f")))

(define-public crate-gw2_taco_parser-0.3.0 (c (n "gw2_taco_parser") (v "0.3.0") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05y1q11y4x2q626xnmhdsxli7w70q4g8w8skdklcrmjlsrfxdp44")))

(define-public crate-gw2_taco_parser-0.4.0 (c (n "gw2_taco_parser") (v "0.4.0") (d (list (d (n "nom") (r "^7.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gidy4s2v80yl62652cx86pwy4zjiyqlkpny9jnfn1malzfypnvr")))

