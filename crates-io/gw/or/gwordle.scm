(define-module (crates-io gw or gwordle) #:use-module (crates-io))

(define-public crate-gwordle-0.1.0 (c (n "gwordle") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0vp1rxbv7npp6kb9j9jvqfk4kgm9vgzxwqkqy6bl9hxikycj2p2v")))

(define-public crate-gwordle-0.1.1 (c (n "gwordle") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0v7y3vg6if8s1fgjdwi8138dhck0a58pd50mfmhx67m12840vjc7")))

(define-public crate-gwordle-0.1.2 (c (n "gwordle") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0ngjmm7xhxzk02za14mkdm5pzn3az4lg1crbg7sw77wi52k78cqz")))

