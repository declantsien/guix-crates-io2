(define-module (crates-io gw _s gw_signal) #:use-module (crates-io))

(define-public crate-gw_signal-0.1.0 (c (n "gw_signal") (v "0.1.0") (d (list (d (n "more-asserts") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rustfft") (r "^6.2.0") (d #t) (k 0)))) (h "0czj1gvyzxq4arcdkr13i4vky1590dzwlwd3wyzr7dyqx9lcwcgm")))

(define-public crate-gw_signal-0.1.1 (c (n "gw_signal") (v "0.1.1") (d (list (d (n "more-asserts") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rustfft") (r "^6.2.0") (d #t) (k 0)))) (h "1kc5h05pbzaf1xf1mw5b1zi5ab85jmzv9jl6psb606gcpg9mj486")))

(define-public crate-gw_signal-0.1.2 (c (n "gw_signal") (v "0.1.2") (d (list (d (n "more-asserts") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rustfft") (r "^6.2.0") (d #t) (k 0)))) (h "19nf5in0hw5rdiizidx4d3041iw33lcysw06inbylsxg1yjkqz1j")))

(define-public crate-gw_signal-0.1.3 (c (n "gw_signal") (v "0.1.3") (d (list (d (n "more-asserts") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rustfft") (r "^6.2.0") (d #t) (k 0)))) (h "10zrdcn9jllvc3n43xl5v9i1i6iabgsr2bx252di5zr05sq0k14j")))

(define-public crate-gw_signal-0.1.4 (c (n "gw_signal") (v "0.1.4") (d (list (d (n "more-asserts") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rustfft") (r "^6.2.0") (d #t) (k 0)))) (h "0xi63svgidb4mzv0k94iy8sc0ld57g1xkbsw507bf0xg0fmym7hm")))

(define-public crate-gw_signal-0.1.5 (c (n "gw_signal") (v "0.1.5") (d (list (d (n "more-asserts") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rustfft") (r "^6.2.0") (d #t) (k 0)))) (h "1qli5833hnk3dbjd0j6imwxbzm4v7x5qp9r41n2s4dqpm03r3k6l")))

(define-public crate-gw_signal-0.1.6 (c (n "gw_signal") (v "0.1.6") (d (list (d (n "more-asserts") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rustfft") (r "^6.2.0") (d #t) (k 0)))) (h "1k9cdgy44a6mhhc848z55fx7lirfq4l0b5940ncbz7px6kaxpaa4")))

(define-public crate-gw_signal-0.1.7 (c (n "gw_signal") (v "0.1.7") (d (list (d (n "more-asserts") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rustfft") (r "^6.2.0") (d #t) (k 0)))) (h "1chr5arz58d28hd7f4zdr88xn75i0kiy54bj04jg5ab4nxvg79w3")))

(define-public crate-gw_signal-0.1.8 (c (n "gw_signal") (v "0.1.8") (d (list (d (n "more-asserts") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rustfft") (r "^6.2.0") (d #t) (k 0)))) (h "05q8rbgx8y1kswkqjd6aijv6v2q84xad7gn7xi4r8s51v0r3rsbz")))

(define-public crate-gw_signal-0.1.9-alpha.1 (c (n "gw_signal") (v "0.1.9-alpha.1") (d (list (d (n "more-asserts") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rustfft") (r "^6.2.0") (d #t) (k 0)))) (h "04sxwjzj12b2v00ll6y6bvvkcyfrflrlp46dhji6db024bxmh07j")))

(define-public crate-gw_signal-0.1.9 (c (n "gw_signal") (v "0.1.9") (d (list (d (n "more-asserts") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rustfft") (r "^6.2.0") (d #t) (k 0)))) (h "0zxsbjmw3ws71c9j9b8mx24il8v90pkxkdg11hcnahgaq1424j9i")))

(define-public crate-gw_signal-0.1.10-alpha.1 (c (n "gw_signal") (v "0.1.10-alpha.1") (d (list (d (n "more-asserts") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rustfft") (r "^6.2.0") (d #t) (k 0)))) (h "0ll9q37hnrndzi3zmzcj237ivq4yxnx4d44xlqpmzrsn90l2gf9j")))

(define-public crate-gw_signal-0.1.10-alpha.2 (c (n "gw_signal") (v "0.1.10-alpha.2") (d (list (d (n "more-asserts") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rustfft") (r "^6.2.0") (d #t) (k 0)))) (h "1z993drfysbxv17155cdxr66vwdnwmm46080rmc0cigpbw4na3qx")))

