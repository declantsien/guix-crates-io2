(define-module (crates-io gw -r gw-rust-programming-tutorial) #:use-module (crates-io))

(define-public crate-gw-rust-programming-tutorial-0.1.0 (c (n "gw-rust-programming-tutorial") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.65") (d #t) (k 0)) (d (n "stdext") (r "^0.3.1") (d #t) (k 0)))) (h "1mihjb2cy3jq3zi62glpgc6r0cjffwlfqhb4vjw6py3ir2rf0fp1")))

