(define-module (crates-io gw -d gw-dsl-parser) #:use-module (crates-io))

(define-public crate-gw-dsl-parser-0.1.0 (c (n "gw-dsl-parser") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sqlparser") (r "^0.36.0") (d #t) (k 0)))) (h "0xf7wdcd9s5gzzkq6ah53i143ssc3j625a3g0ac79df4xanmk0xy") (y #t)))

