(define-module (crates-io gw as gwasmi_core) #:use-module (crates-io))

(define-public crate-gwasmi_core-0.12.0 (c (n "gwasmi_core") (v "0.12.0") (d (list (d (n "downcast-rs") (r "^1.2") (k 0)) (d (n "libm") (r "^0.2.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 2)) (d (n "region") (r "^3.0.0") (o #t) (d #t) (k 0)))) (h "1kzx0q4zfvyfxsg75ybpxq7njizwg0hv9sswaa06qida1sd3si27") (f (quote (("virtual_memory" "region" "std") ("std" "num-traits/std" "downcast-rs/std") ("default" "std"))))))

