(define-module (crates-io gw as gwasm-dispatcher) #:use-module (crates-io))

(define-public crate-gwasm-dispatcher-0.1.0 (c (n "gwasm-dispatcher") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0vns5ayc22m49plqg8qczdnf11scl464zgnqi542daqiz04hq8xv")))

