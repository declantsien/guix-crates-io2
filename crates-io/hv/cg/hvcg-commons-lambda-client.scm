(define-module (crates-io hv cg hvcg-commons-lambda-client) #:use-module (crates-io))

(define-public crate-hvcg-commons-lambda-client-0.0.1 (c (n "hvcg-commons-lambda-client") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "hvcg_example_openapi_entity") (r "^1.0.0") (d #t) (k 2)) (d (n "rusoto_core") (r "^0.47.0") (d #t) (k 0)) (d (n "rusoto_lambda") (r "^0.47.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.6.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cavnzykjjl4i01kbismp41cks360lrbi3dml2drzjz537sq1b68")))

