(define-module (crates-io hv m- hvm-core) #:use-module (crates-io))

(define-public crate-hvm-core-0.1.0 (c (n "hvm-core") (v "0.1.0") (h "160vmg1q3asjr8f8khg99qhdp5inaxavd5lnihxzm95aik0x3gbf")))

(define-public crate-hvm-core-0.1.1 (c (n "hvm-core") (v "0.1.1") (h "05qwrwb0wv9c0iqyr9vbni2n2rxcc3wk5gsjinrl6jxdcx8nb8dr")))

(define-public crate-hvm-core-0.1.4 (c (n "hvm-core") (v "0.1.4") (h "11fvqj4v5wzg1ab2zgxbxmsd390bj7f377nyvzxd38rpg25dd9h0")))

(define-public crate-hvm-core-0.1.7 (c (n "hvm-core") (v "0.1.7") (h "1dz8mykwz0x52pmrr93hciiajp7fqr3aiqs0d2fsyvad70hqc86i")))

(define-public crate-hvm-core-0.1.9 (c (n "hvm-core") (v "0.1.9") (h "1k1ix30bab3wd5bhfqlvkw5yx6zlzryahgb1lzgwymvzzra6g60c")))

(define-public crate-hvm-core-0.1.11 (c (n "hvm-core") (v "0.1.11") (h "01fc3zmhjz3qjvnqa4ird58cvca14hk04mnhrg4x99ffwbl2hb9g")))

(define-public crate-hvm-core-0.1.12 (c (n "hvm-core") (v "0.1.12") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)))) (h "18j23kbim9j4jnbc0gj0ij09hbfa1984aa318gnjpc7m3rsqnfs4") (f (quote (("hvm_cli_options") ("default" "hvm_cli_options"))))))

(define-public crate-hvm-core-0.1.13 (c (n "hvm-core") (v "0.1.13") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)))) (h "03al7pzmcjyqp02z0lmkg1xbr8ij39ygl4pwwdik2gasn13yr0as") (f (quote (("hvm_cli_options") ("default" "hvm_cli_options"))))))

(define-public crate-hvm-core-0.1.14 (c (n "hvm-core") (v "0.1.14") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)))) (h "0anb3zhjia1ck3rxzr80bdxm20zgwr6ccijhqzj35ij6c245zcia") (f (quote (("hvm_cli_options") ("default" "hvm_cli_options"))))))

(define-public crate-hvm-core-0.1.18 (c (n "hvm-core") (v "0.1.18") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)))) (h "0zy2y1ipj231ym4nnjbkyix2wqps8yx3m771bz2f4nc82aq9jjk0") (f (quote (("hvm_cli_options") ("default" "hvm_cli_options"))))))

(define-public crate-hvm-core-0.2.0 (c (n "hvm-core") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)))) (h "1m53j7va6s30im4schn3i7r2xvszwd424bn0l509y99jz1g3xhzs") (f (quote (("hvm_cli_options") ("default" "hvm_cli_options"))))))

(define-public crate-hvm-core-0.1.19 (c (n "hvm-core") (v "0.1.19") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)))) (h "07ly64lj4ihyfh6bgvzrjqdfj7pvc55phwdl1spjadla5kd9kh0x") (f (quote (("hvm_cli_options") ("default" "hvm_cli_options"))))))

(define-public crate-hvm-core-0.1.20 (c (n "hvm-core") (v "0.1.20") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)))) (h "0x6yw0ph35x1mkwsxvlqcbl07gn9b24lcrjddbw67y4bi9pj39ci") (f (quote (("hvm_cli_options") ("default" "hvm_cli_options"))))))

(define-public crate-hvm-core-0.2.2 (c (n "hvm-core") (v "0.2.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)))) (h "05qphrkdq3i0nb1qmb4p2470asbk1pfich33rfv19drw32rmrj1d") (f (quote (("hvm_cli_options") ("default" "hvm_cli_options"))))))

(define-public crate-hvm-core-0.2.3 (c (n "hvm-core") (v "0.2.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)))) (h "0fyva6zia4wsdxqh1f4y196lw3gx106ynvipx42y6pxcrwa86nq8") (f (quote (("hvm_cli_options") ("default" "hvm_cli_options"))))))

(define-public crate-hvm-core-0.2.4 (c (n "hvm-core") (v "0.2.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)))) (h "1gi0mgjqy2svl0gm4chzlzxrdgskpiw955mznmcym4c80m3n9x42") (f (quote (("hvm_cli_options") ("default" "hvm_cli_options"))))))

(define-public crate-hvm-core-0.2.5 (c (n "hvm-core") (v "0.2.5") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)))) (h "1bly6b5b5wdwvc3f4vin428p55xqjrh6h0a6sj7mysf9rfkagpih") (f (quote (("hvm_cli_options") ("default" "hvm_cli_options"))))))

(define-public crate-hvm-core-0.2.6 (c (n "hvm-core") (v "0.2.6") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)))) (h "09b517m2n8hqb9pp7ncp6xcbzfvlhd5kg4iw64bp3s0z14g30z9i") (f (quote (("hvm_cli_options") ("default" "hvm_cli_options"))))))

(define-public crate-hvm-core-0.2.7 (c (n "hvm-core") (v "0.2.7") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)))) (h "0rd9nn4qvd4nb8cynj80f5b1f60ljxaqw4ia3fk863gls0f4m2sk") (f (quote (("hvm_cli_options") ("default" "hvm_cli_options"))))))

(define-public crate-hvm-core-0.2.8 (c (n "hvm-core") (v "0.2.8") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)))) (h "1adl45ja9mx28f13wwqckx1csjq92p07il8bz7dg5wgix3sc1zav") (f (quote (("hvm_cli_options") ("default" "hvm_cli_options"))))))

(define-public crate-hvm-core-0.2.9 (c (n "hvm-core") (v "0.2.9") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)))) (h "1xyaindmgsgyn8ms6aqap3ycffjzagyd1biacbh0w292zryqnf5f") (f (quote (("hvm_cli_options") ("default" "hvm_cli_options"))))))

(define-public crate-hvm-core-0.2.10 (c (n "hvm-core") (v "0.2.10") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)))) (h "074x4yh28yldx1z3g9y3fkq60db7hcfwgfbpihj08mamsd5fkcfz") (f (quote (("hvm_cli_options") ("default" "hvm_cli_options"))))))

(define-public crate-hvm-core-0.2.11 (c (n "hvm-core") (v "0.2.11") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)))) (h "13ik0590mms3p5d7anry7flmkgy0jmxmsygr1pfmwx7yzkg2672p") (f (quote (("hvm_cli_options") ("default" "hvm_cli_options"))))))

(define-public crate-hvm-core-0.2.12 (c (n "hvm-core") (v "0.2.12") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)))) (h "1psc8na24gifmdbhpiizf46pq8xkhspd7q506ma18gz4bz02rf0b") (f (quote (("hvm_cli_options") ("default" "hvm_cli_options"))))))

(define-public crate-hvm-core-0.2.14 (c (n "hvm-core") (v "0.2.14") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)))) (h "1xz1x5avhnq53avg87378jhwa7x8n4p25vzhqbg4riwajldxhfrl") (f (quote (("hvm_cli_options") ("default" "hvm_cli_options"))))))

(define-public crate-hvm-core-0.2.15 (c (n "hvm-core") (v "0.2.15") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)))) (h "0myhr2r1cy790fmjx2cmbif43vcw33pxkxrdkx8nr0bn30jxk41p") (f (quote (("lazy_mode") ("hvm_cli_options") ("default" "hvm_cli_options"))))))

(define-public crate-hvm-core-0.2.17 (c (n "hvm-core") (v "0.2.17") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)))) (h "027rbbsg8v9wg2szdg7mh2zi3fxf8dwv79j64ias95xs3yvas6gr") (f (quote (("lazy_mode") ("hvm_cli_options") ("default" "hvm_cli_options"))))))

(define-public crate-hvm-core-0.2.26 (c (n "hvm-core") (v "0.2.26") (d (list (d (n "TSPL") (r "^0.0.9") (d #t) (k 0)) (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.34.0") (f (quote ("glob"))) (d #t) (k 2)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)) (d (n "stacker") (r "^0.1.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "08zzljrpyknqhvjj9ilxc6hck4yrmy7v6vb4byqj6qrrvbnzdyzl") (f (quote (("trace") ("std") ("default" "cli" "_full_cli") ("_fuzz_no_free" "_fuzz") ("_fuzz" "std") ("_full_cli")))) (s 2) (e (quote (("cli" "std" "dep:clap"))))))

(define-public crate-hvm-core-0.3.0-hvm32.compat (c (n "hvm-core") (v "0.3.0-hvm32.compat") (d (list (d (n "TSPL") (r "^0.0.9") (d #t) (k 0)) (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.34.0") (f (quote ("glob"))) (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)) (d (n "stacker") (r "^0.1.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "14m6pz43g11bw0d7fsqsrqqpd7ka3jmilpj9ygwbhgszl03lwx29") (f (quote (("trace") ("std") ("default" "cli" "_full_cli") ("_fuzz_no_free" "_fuzz") ("_fuzz" "std") ("_full_cli")))) (s 2) (e (quote (("cli" "std" "dep:clap"))))))

(define-public crate-hvm-core-0.3.0-hvm32.compat.2 (c (n "hvm-core") (v "0.3.0-hvm32.compat.2") (d (list (d (n "TSPL") (r "^0.0.9") (d #t) (k 0)) (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.34.0") (f (quote ("glob"))) (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)) (d (n "stacker") (r "^0.1.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "00pbb76g7pgcs3bljhmqc7vms03w5mqqmh0gwfr9p6m2li8g5lcv") (f (quote (("trace") ("std") ("default" "cli" "_full_cli") ("_fuzz_no_free" "_fuzz") ("_fuzz" "std") ("_full_cli")))) (s 2) (e (quote (("cli" "std" "dep:clap"))))))

(define-public crate-hvm-core-0.3.0-hvm32.compat.3 (c (n "hvm-core") (v "0.3.0-hvm32.compat.3") (d (list (d (n "TSPL") (r "^0.0.9") (d #t) (k 0)) (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.34.0") (f (quote ("glob"))) (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)) (d (n "stacker") (r "^0.1.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0ynr5p7wws82wksmcdxz2k8z2pq82lmh34lp2acv4y8agq7api2d") (f (quote (("trace") ("std") ("default" "cli" "_full_cli") ("_fuzz_no_free" "_fuzz") ("_fuzz" "std") ("_full_cli")))) (s 2) (e (quote (("cli" "std" "dep:clap"))))))

(define-public crate-hvm-core-0.3.0-hvm32.compat.4 (c (n "hvm-core") (v "0.3.0-hvm32.compat.4") (d (list (d (n "TSPL") (r "^0.0.9") (d #t) (k 0)) (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.34.0") (f (quote ("glob"))) (d #t) (k 2)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)) (d (n "stacker") (r "^0.1.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1vk72gmq5hqr1bfhsyk7mjfw2dn5z0l9rvk0rk77rbr7g8p5px1l") (f (quote (("trace") ("std") ("default" "cli" "_full_cli") ("_fuzz_no_free" "_fuzz") ("_fuzz" "std") ("_full_cli")))) (s 2) (e (quote (("cli" "std" "dep:clap"))))))

