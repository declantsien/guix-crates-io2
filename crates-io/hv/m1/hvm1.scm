(define-module (crates-io hv m1 hvm1) #:use-module (crates-io))

(define-public crate-hvm1-1.0.13 (c (n "hvm1") (v "1.0.13") (d (list (d (n "HOPA") (r "^0.1.1") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.69") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen" "inaccurate"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.10") (d #t) (k 0)))) (h "10szv6alr7hs7jd3vvg9wpzjwhsxpwgwjj8p2vrpz6jk57fxfzns")))

(define-public crate-hvm1-1.0.14 (c (n "hvm1") (v "1.0.14") (d (list (d (n "HOPA") (r "^0.1.1") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.69") (d #t) (k 0)) (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "instant") (r "^0.1") (f (quote ("wasm-bindgen" "inaccurate"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.10") (d #t) (k 0)))) (h "05k8ixk79hcm3n6s6192hpjbfsl6j659y4acxjhb6p1pjygmvkqk")))

