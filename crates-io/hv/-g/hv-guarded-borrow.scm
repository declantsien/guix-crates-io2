(define-module (crates-io hv -g hv-guarded-borrow) #:use-module (crates-io))

(define-public crate-hv-guarded-borrow-0.1.0 (c (n "hv-guarded-borrow") (v "0.1.0") (d (list (d (n "hv-ecs") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1c3z2smc823f6y8hv86gqnn6sgcz2z9yzg2airvlamydwnznc5rp") (f (quote (("std") ("default"))))))

(define-public crate-hv-guarded-borrow-0.1.1 (c (n "hv-guarded-borrow") (v "0.1.1") (d (list (d (n "hv-ecs") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1vi1c6gpw669kwpi8h055bds7q6ms6hqjkhacyygh9jdjy8h9v36") (f (quote (("std") ("default"))))))

