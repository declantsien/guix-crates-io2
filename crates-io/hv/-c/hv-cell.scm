(define-module (crates-io hv -c hv-cell) #:use-module (crates-io))

(define-public crate-hv-cell-0.1.0 (c (n "hv-cell") (v "0.1.0") (d (list (d (n "hv-guarded-borrow") (r "^0.1.0") (d #t) (k 0)) (d (n "hv-lease-tracker") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0b84nf7yal0kik17yf3wqsj4gx06lhyzk6sa0p22jp1p77pgq1y3") (f (quote (("track-leases" "hv-lease-tracker"))))))

