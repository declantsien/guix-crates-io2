(define-module (crates-io hv ct hvctrl) #:use-module (crates-io))

(define-public crate-hvctrl-0.1.0 (c (n "hvctrl") (v "0.1.0") (d (list (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 2)) (d (n "windy") (r "^0.2.0") (d #t) (k 0)))) (h "127n1ghz567nmlfv6r50jrik5l3i7zqnm5pmx77wvxnz3rq4xq7n") (f (quote (("vmware" "vmrest" "vmrun") ("vmrun") ("vmrest" "reqwest") ("virtualbox" "vboxmanage") ("vboxmanage") ("hypervcmd") ("hyperv" "hypervcmd"))))))

