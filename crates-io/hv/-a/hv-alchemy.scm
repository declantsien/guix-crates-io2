(define-module (crates-io hv -a hv-alchemy) #:use-module (crates-io))

(define-public crate-hv-alchemy-0.1.0 (c (n "hv-alchemy") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.11.2") (d #t) (k 0)) (d (n "hv-atom") (r "^0.1.0") (d #t) (k 0)) (d (n "hv-cell") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "spin") (r "^0.9.2") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1v9rmzq0i64wvfdiklw9jnmb85g3zrf5jxshs12310cznn8w530b")))

