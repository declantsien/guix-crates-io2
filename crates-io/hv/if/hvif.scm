(define-module (crates-io hv if hvif) #:use-module (crates-io))

(define-public crate-hvif-0.1.0 (c (n "hvif") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cairo-rs") (r "^0.14") (o #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1m30agphc6lddvn7ilx2c4id36ck4y8fydm2sf0y989b0mv3496h")))

(define-public crate-hvif-0.1.1 (c (n "hvif") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "cairo-rs") (r "^0.14") (o #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "17ibl28lqbfhsrysnb0r4ivbicwwaxrlnnn6x8mxvpzck704hl69")))

