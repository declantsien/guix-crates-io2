(define-module (crates-io hv -l hv-lease-tracker) #:use-module (crates-io))

(define-public crate-hv-lease-tracker-0.1.0 (c (n "hv-lease-tracker") (v "0.1.0") (d (list (d (n "slab") (r "^0.4.5") (d #t) (k 0)) (d (n "spin") (r "^0.9.2") (d #t) (k 0)))) (h "1i2km21q0nwnb8qkx17dbpj08hzq6zjq1qz8i2c1brbzshgxaz9s")))

