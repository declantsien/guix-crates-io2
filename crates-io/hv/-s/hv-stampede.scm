(define-module (crates-io hv -s hv-stampede) #:use-module (crates-io))

(define-public crate-hv-stampede-0.1.0 (c (n "hv-stampede") (v "0.1.0") (d (list (d (n "bumpalo") (r "^3.8.0") (f (quote ("allocator_api" "boxed"))) (d #t) (k 0)) (d (n "spin") (r "^0.9.2") (d #t) (k 0)))) (h "1lsgfgc7wz2z2zij23dkahqlmpl2y6j9hn95647i6vv53n93cdak")))

(define-public crate-hv-stampede-0.2.0 (c (n "hv-stampede") (v "0.2.0") (d (list (d (n "bumpalo") (r "^3.8.0") (f (quote ("allocator_api" "boxed" "collections"))) (d #t) (k 0)) (d (n "spin") (r "^0.9.2") (d #t) (k 0)))) (h "0igs3mwffg4hc7cb2xibmcmwrnjh81jkqfypwxc9l5ywrmd0wlsd")))

(define-public crate-hv-stampede-0.2.1 (c (n "hv-stampede") (v "0.2.1") (d (list (d (n "bumpalo") (r "^3.8.0") (f (quote ("allocator_api" "boxed" "collections"))) (d #t) (k 0)) (d (n "spin") (r "^0.9.2") (d #t) (k 0)))) (h "06bibpxm8wl3mqxl05iam9y2ykrkw18wcmj4l9zlwg85k7z3h1px")))

