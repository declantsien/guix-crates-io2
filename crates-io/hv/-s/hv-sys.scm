(define-module (crates-io hv -s hv-sys) #:use-module (crates-io))

(define-public crate-hv-sys-0.1.0 (c (n "hv-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)))) (h "07danmcqwkz9fimalbmwxahxfd3r0zznd8kibhws5z2m982r32b5")))

(define-public crate-hv-sys-0.1.1 (c (n "hv-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)))) (h "01lfj67zyjmlg4l7cclv258g5djggnmrzjv1ji3sxhbnj163lcym")))

