(define-module (crates-io hv -e hv-ecs) #:use-module (crates-io))

(define-public crate-hv-ecs-0.1.0 (c (n "hv-ecs") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "hashbrown") (r "^0.11.0") (f (quote ("ahash" "inline-more"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (o #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_test") (r "^1.0.117") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.23") (d #t) (k 2)))) (h "11d4awki6a3z66j6r5r8fsv0kkr1bfk71fbmyd4gx27kmxy40z1h") (f (quote (("std") ("row-serialize" "serde") ("default" "std") ("column-serialize" "serde"))))))

