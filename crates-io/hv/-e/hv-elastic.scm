(define-module (crates-io hv -e hv-elastic) #:use-module (crates-io))

(define-public crate-hv-elastic-0.1.0 (c (n "hv-elastic") (v "0.1.0") (d (list (d (n "hv-cell") (r "^0.1.0") (d #t) (k 0)) (d (n "hv-ecs") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "hv-guarded-borrow") (r "^0.1.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "003h5bjkcji9bc6k5xsx07dxcy3vgcwpjybd2majb4675ww4f81d")))

(define-public crate-hv-elastic-0.2.0 (c (n "hv-elastic") (v "0.2.0") (d (list (d (n "hv-cell") (r "^0.1.0") (d #t) (k 0)) (d (n "hv-ecs") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "hv-guarded-borrow") (r "^0.1.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1p98yk0sjx3x0faznv3yy4sbmfwgvzm1ysjygajl351rzyi6q6y3")))

(define-public crate-hv-elastic-0.2.1 (c (n "hv-elastic") (v "0.2.1") (d (list (d (n "hv-cell") (r "^0.1.0") (d #t) (k 0)) (d (n "hv-ecs") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "hv-guarded-borrow") (r "^0.1.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1cg0pkggs047ngb30m1dbjan1xw0gx1n2j9l639kxxw9ddxmd5dd")))

(define-public crate-hv-elastic-0.3.0 (c (n "hv-elastic") (v "0.3.0") (d (list (d (n "hv-cell") (r "^0.1.0") (d #t) (k 0)) (d (n "hv-ecs") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "hv-guarded-borrow") (r "^0.1.1") (d #t) (k 0)) (d (n "hv-stampede") (r "^0.1.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0lsa261ypbcvg7j40s4i6ag2n6b58wd9mc3p14f61ja9dn6ypnj7")))

(define-public crate-hv-elastic-0.3.1 (c (n "hv-elastic") (v "0.3.1") (d (list (d (n "hv-cell") (r "^0.1.0") (d #t) (k 0)) (d (n "hv-ecs") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "hv-guarded-borrow") (r "^0.1.1") (d #t) (k 0)) (d (n "hv-stampede") (r "^0.1.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "19pdp3vpcnkrxidjh213f91bsbd19y051kyjq4jdwq1cznd9n0k2")))

(define-public crate-hv-elastic-0.4.0 (c (n "hv-elastic") (v "0.4.0") (d (list (d (n "hv-cell") (r "^0.1.0") (d #t) (k 0)) (d (n "hv-ecs") (r "^0.6.5") (o #t) (d #t) (k 0) (p "hecs")) (d (n "hv-guarded-borrow") (r "^0.1.1") (d #t) (k 0)) (d (n "hv-stampede") (r "^0.2.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (o #t) (d #t) (k 0)))) (h "09zrgyfjjm0l50mswvqk8n1iskm0zbyaxmw9dpzlr96c2ymzr19f") (f (quote (("std" "thiserror") ("default" "std"))))))

(define-public crate-hv-elastic-0.4.1 (c (n "hv-elastic") (v "0.4.1") (d (list (d (n "hv-cell") (r "^0.1.0") (d #t) (k 0)) (d (n "hv-ecs") (r "^0.6.5") (o #t) (d #t) (k 0) (p "hecs")) (d (n "hv-guarded-borrow") (r "^0.1.1") (d #t) (k 0)) (d (n "hv-stampede") (r "^0.2.1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (o #t) (d #t) (k 0)))) (h "1qsfg2c4fybb7x0dd197w9ykxp8ws46pkr2vg4nlbfc80m2jd7vm") (f (quote (("std" "thiserror") ("default" "std"))))))

