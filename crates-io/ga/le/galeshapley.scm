(define-module (crates-io ga le galeshapley) #:use-module (crates-io))

(define-public crate-galeshapley-0.0.2 (c (n "galeshapley") (v "0.0.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1vb31k4yzmgh90p94mfg09r1xys3rw9fm980716wg90yrs7g1y7d")))

(define-public crate-galeshapley-0.0.3 (c (n "galeshapley") (v "0.0.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1mdawn167i6q5hllkr6isz26hrxz2p45yampsz830n5gd7112r5q")))

(define-public crate-galeshapley-0.0.4 (c (n "galeshapley") (v "0.0.4") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0iydv8cil1yb7npkyw89112llhai7r6h8r5x9f8vxbdqylbi684r")))

