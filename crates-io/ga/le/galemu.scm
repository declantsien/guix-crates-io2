(define-module (crates-io ga le galemu) #:use-module (crates-io))

(define-public crate-galemu-0.2.0 (c (n "galemu") (v "0.2.0") (h "1ypsa1pyyw5g2bkxc5cm9wxjcpnwk01mxvgwg3x62jm78a0pf791")))

(define-public crate-galemu-0.2.1 (c (n "galemu") (v "0.2.1") (h "01b8xb11b3913zhwc13glgmgn237w9iwx3jy3sii2dn71wdgq7iq")))

(define-public crate-galemu-0.2.2 (c (n "galemu") (v "0.2.2") (h "0nvwcbl9vy03xfw6ch62sz8pc45vr9038lbfqryxxw1cycwp1bzc")))

(define-public crate-galemu-0.2.3 (c (n "galemu") (v "0.2.3") (h "0ab3xdfw6l1gl25srmvmhr6qfmhd487bkr9lw4wvb8j49wpl8lxm")))

