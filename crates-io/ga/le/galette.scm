(define-module (crates-io ga le galette) #:use-module (crates-io))

(define-public crate-galette-0.3.0 (c (n "galette") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1x891ba1s4x9inar82rw8bhkgrvkw31m952ayz712aqfpwqzgb2b")))

