(define-module (crates-io ga in gain-listener) #:use-module (crates-io))

(define-public crate-gain-listener-0.1.0 (c (n "gain-listener") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "flatbuffers") (r "^0.6.0") (d #t) (k 0)) (d (n "gain") (r "^0.3.0") (d #t) (k 0)) (d (n "httparse") (r "^1.3.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1mrn6s4s9j9g5a4i3qkkyb2v1qpynnnzminj5n8diq63wn4qvirx")))

(define-public crate-gain-listener-0.2.0 (c (n "gain-listener") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "flatbuffers") (r "^0.6.0") (d #t) (k 0)) (d (n "gain") (r "^0.4.0") (d #t) (k 0)) (d (n "httparse") (r "^1.3.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0bjfnjpx109p8g740x2x7ld9rpk6kzqm5vyj18pvjwwj0a51dcpq")))

(define-public crate-gain-listener-0.3.0 (c (n "gain-listener") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "flatbuffers") (r "^0.6.0") (d #t) (k 0)) (d (n "gain") (r "^0.4.0") (d #t) (k 0)) (d (n "httparse") (r "^1.3.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0kv2mypclkq0kndnlnkfj3qc27szirc5m4bd9l7g65icl8bh8ws6")))

(define-public crate-gain-listener-0.3.1 (c (n "gain-listener") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "flatbuffers") (r "^2.0.0") (d #t) (k 0)) (d (n "gain") (r "^0.4.0") (d #t) (k 0)) (d (n "httparse") (r "^1.3.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1n3hx0w161x5hfm85i2z51qhbz4946nif3m74nxn3q7psff4v6qs")))

