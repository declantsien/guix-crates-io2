(define-module (crates-io ga in gain-localhost) #:use-module (crates-io))

(define-public crate-gain-localhost-0.1.0 (c (n "gain-localhost") (v "0.1.0") (d (list (d (n "flatbuffers") (r "^0.6.0") (d #t) (k 0)) (d (n "gain") (r "^0.2.1") (d #t) (k 0)) (d (n "gain-lep") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1si88rzx86l46hh71a2f5f0x7jmr7lnf606gfm3pvgbfcqdmdag5") (f (quote (("lep" "gain-lep"))))))

(define-public crate-gain-localhost-0.1.1 (c (n "gain-localhost") (v "0.1.1") (d (list (d (n "flatbuffers") (r "^0.6.0") (d #t) (k 0)) (d (n "gain") (r ">=0.2.1, <1") (d #t) (k 0)) (d (n "gain-lep") (r ">=0.1.0, <1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1zplbvjmw9s8fm7kgjlx97i7kpv15lgdq2191sg9dn1zxd0cb3zl") (f (quote (("lep" "gain-lep"))))))

(define-public crate-gain-localhost-0.1.2 (c (n "gain-localhost") (v "0.1.2") (d (list (d (n "flatbuffers") (r "^0.6.0") (d #t) (k 0)) (d (n "gain") (r ">=0.4.0, <1") (d #t) (k 0)) (d (n "gain-lep") (r ">=0.1.2, <1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "18v0i6l902vi9lm3bmjs3s99dfw20q2lqry12rx6lzlnw5xlfd4p") (f (quote (("lep" "gain-lep"))))))

(define-public crate-gain-localhost-0.1.3 (c (n "gain-localhost") (v "0.1.3") (d (list (d (n "flatbuffers") (r "^2.0.0") (d #t) (k 0)) (d (n "gain") (r ">=0.4.0, <1") (d #t) (k 0)) (d (n "gain-lep") (r ">=0.1.2, <1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1p8l8xqdb9la0dik1simkb6cj66nvv26k5y606w94qh265cd2vkn") (f (quote (("lep" "gain-lep"))))))

