(define-module (crates-io ga in gain) #:use-module (crates-io))

(define-public crate-gain-0.1.0 (c (n "gain") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0.0") (d #t) (k 0)) (d (n "futures-await") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)))) (h "1yf7k5fv5kmiqkkbwbml1dx7a0lnmh7mxv20w6a0ljcxyk6313fs")))

(define-public crate-gain-0.2.0 (c (n "gain") (v "0.2.0") (d (list (d (n "async-task") (r "^1.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "072lwvw2k7j3bd4rcki8yfh2zi542f6w56r3zpr99s6rf90fmh5i")))

(define-public crate-gain-0.2.1 (c (n "gain") (v "0.2.1") (d (list (d (n "async-task") (r "^1.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0jmqng4q2gvqs223zjij22i1cvhbr9pln9wp74xp3qv2rb2jbs8r")))

(define-public crate-gain-0.3.0 (c (n "gain") (v "0.3.0") (d (list (d (n "async-task") (r "^1.3.0") (d #t) (k 0)) (d (n "futures-channel") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1016dsvj2w1m3s0ypdv7kil60vi4s57yj7x7b561vdhxnz2sjr3l")))

(define-public crate-gain-0.4.0 (c (n "gain") (v "0.4.0") (d (list (d (n "async-task") (r "^1.3.0") (d #t) (k 0)) (d (n "futures-channel") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0yk7ymqx1msfsn1zi2krhhl292jqy91yh5f0g0jx4mg62yfyjrgr")))

(define-public crate-gain-0.4.1 (c (n "gain") (v "0.4.1") (d (list (d (n "async-task") (r "^1.3.0") (d #t) (k 0)) (d (n "futures-channel") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1h1hx4ap61ivdx3a0784ln2w04ybdrqm1ifb4p3ayzbcrm9vv7cr")))

(define-public crate-gain-0.5.0 (c (n "gain") (v "0.5.0") (d (list (d (n "async-task") (r "^1.3.0") (d #t) (k 0)) (d (n "futures-channel") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1lcpanxvq8mdwjzka5ainl4xzc69zvac3qqm3hdfdnc312rpb9q9")))

