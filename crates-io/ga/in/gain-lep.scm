(define-module (crates-io ga in gain-lep) #:use-module (crates-io))

(define-public crate-gain-lep-0.1.0 (c (n "gain-lep") (v "0.1.0") (d (list (d (n "gain") (r "^0.2.1") (d #t) (k 0)) (d (n "lep") (r "^0.4.4") (d #t) (k 0)))) (h "07454r6xq4mlfw2n8235cm4ll7j67j4zazrs85wpsffnv91csgq7")))

(define-public crate-gain-lep-0.1.1 (c (n "gain-lep") (v "0.1.1") (d (list (d (n "gain") (r ">=0.3.0, <1") (d #t) (k 0)) (d (n "lep") (r "^0.4.4") (d #t) (k 0)))) (h "1hjfj48lb7c2fjjxy5p3p8mgk0cspyaafsdr661lpwfjn7ii4jw6")))

(define-public crate-gain-lep-0.1.2 (c (n "gain-lep") (v "0.1.2") (d (list (d (n "gain") (r ">=0.4.0, <1") (d #t) (k 0)) (d (n "lep") (r "^0.4.4") (d #t) (k 0)))) (h "1jqzhhsllsz81j0x2dki5jlps2j95x6378ch7sz1l4m935np4lkw")))

(define-public crate-gain-lep-0.1.3 (c (n "gain-lep") (v "0.1.3") (d (list (d (n "gain") (r ">=0.4.0, <1") (d #t) (k 0)) (d (n "lep") (r "^0.4.5") (d #t) (k 0)))) (h "1wnsx6pip7nlaq3qj9796q290lfb9n7ip1gki41f4pw0bcg94qn6")))

