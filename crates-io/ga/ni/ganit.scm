(define-module (crates-io ga ni ganit) #:use-module (crates-io))

(define-public crate-ganit-0.1.0 (c (n "ganit") (v "0.1.0") (h "0g9ag23arxg1p1b72jnnnv52bh5nsazlwam27qzsr7da3ryx1wdz")))

(define-public crate-ganit-0.1.1 (c (n "ganit") (v "0.1.1") (h "1clhaa9rnckgj7m5993i9ql8sqvb2v6cxr9w5hg7gbaba9y3lsd9")))

(define-public crate-ganit-0.1.2 (c (n "ganit") (v "0.1.2") (h "1f11qsbglxswh9y0my862a3smpmxhw1rpgnz43517vjm11n92il0")))

