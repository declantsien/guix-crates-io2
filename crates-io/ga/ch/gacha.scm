(define-module (crates-io ga ch gacha) #:use-module (crates-io))

(define-public crate-gacha-0.0.0 (c (n "gacha") (v "0.0.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0iciam07199n0jijhc2z1v8dyp53mv6i14b0n1jkrn85hrc49yaz") (f (quote (("default"))))))

(define-public crate-gacha-0.0.1 (c (n "gacha") (v "0.0.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1p0wcdk872j33m22ha0rmz366rr8pldqazwy391d9abvw2l72k90") (f (quote (("default"))))))

