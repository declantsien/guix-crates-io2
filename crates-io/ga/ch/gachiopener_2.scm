(define-module (crates-io ga ch gachiopener_2) #:use-module (crates-io))

(define-public crate-gachiopener_2-0.1.0 (c (n "gachiopener_2") (v "0.1.0") (d (list (d (n "open") (r "^4.0.0") (d #t) (k 0)))) (h "01fg1i03h7568lqw8mf98d7mq06m90315g792034y0nj4934m5zs")))

(define-public crate-gachiopener_2-0.2.0 (c (n "gachiopener_2") (v "0.2.0") (d (list (d (n "open") (r "^4.0.0") (d #t) (k 0)))) (h "1jwj2skgw4bxyhkvd3f387nxqmddq48c7k75f7yyfrf69abdh62l")))

