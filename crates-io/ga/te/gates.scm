(define-module (crates-io ga te gates) #:use-module (crates-io))

(define-public crate-gates-0.0.0 (c (n "gates") (v "0.0.0") (h "1l5gbhpzq1qczqich97xr22j34c7m5730pc4598hgm364vldgcrm")))

(define-public crate-gates-0.1.0 (c (n "gates") (v "0.1.0") (h "0hzxj6h868d2cm9z44cb2mdwdjrg1326hv2lf27wi0za6rmi35kj")))

