(define-module (crates-io ga te gate) #:use-module (crates-io))

(define-public crate-gate-0.1.0 (c (n "gate") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "gl") (r "^0.6.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "sdl2") (r "^0.29") (f (quote ("image" "mixer"))) (k 0)))) (h "0281ivs649xzd586jfmkqw71mg5lmx3rjq8b5vx87rvkc8ffv6nm")))

(define-public crate-gate-0.2.0 (c (n "gate") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "gl") (r "^0.6.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "sdl2") (r "^0.29") (f (quote ("image" "mixer"))) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0vkbmk40r7z2z0w84yaassypgfybwhlrkgj95z71wvin1v9x6lb6")))

(define-public crate-gate-0.2.1 (c (n "gate") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "gl") (r "^0.6.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "sdl2") (r "^0.29") (f (quote ("image" "mixer"))) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0iskmbbiwj2y01wn9hf14cscvigrca0l4v1zzfdk5fjdjv3gs1c4")))

(define-public crate-gate-0.3.0 (c (n "gate") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "gl") (r "^0.6.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "sdl2") (r "^0.29") (f (quote ("image" "mixer"))) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1707sp3rz6kbhyfkv17p2f093b9zz6kk5n2k83vm0x87pdm80bxm")))

(define-public crate-gate-0.4.0 (c (n "gate") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "gl") (r "^0.6.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "sdl2") (r "^0.29") (f (quote ("image" "mixer"))) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "15vpbasf861jkchd8ii1vkfhmpbq784asl7fiv1r2y5mqnzpizx5")))

(define-public crate-gate-0.5.0 (c (n "gate") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "gl") (r "^0.6.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "sdl2") (r "^0.29") (f (quote ("image" "mixer"))) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1s1l5i06c4bv6z6y5wpszxn4yk6dhpy2gkkz5x3lji87l9dpz5nq")))

(define-public crate-gate-0.6.0 (c (n "gate") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "gl") (r "^0.6.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "sdl2") (r "^0.29") (f (quote ("image" "mixer"))) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1ld5qrhfn67d6js034wiabj02iyc56d2109ayjnrzn7z34lbxd34")))

(define-public crate-gate-0.6.1 (c (n "gate") (v "0.6.1") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "gl") (r "^0.6.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "sdl2") (r "^0.29") (f (quote ("image" "mixer"))) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "010b6pvlyvk3in7299vyl138dql9lmhwvaw2pvwhq4zd91s2wawh")))

(define-public crate-gate-0.6.2 (c (n "gate") (v "0.6.2") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "gl") (r "^0.6.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "sdl2") (r "^0.29") (f (quote ("image" "mixer"))) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "17mdpn8bima8khlljy8ppnkr1pkbsyp4l45phll5aifdckyawm8l")))

(define-public crate-gate-0.6.3 (c (n "gate") (v "0.6.3") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "gl") (r "^0.6.1") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "sdl2") (r "^0.29") (f (quote ("image" "mixer"))) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0rcv8xvzvagvwvrdc039gv9xg6zh1w1hrsfnr8sz147p4v0009xk")))

