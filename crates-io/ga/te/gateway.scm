(define-module (crates-io ga te gateway) #:use-module (crates-io))

(define-public crate-gateway-0.0.0 (c (n "gateway") (v "0.0.0") (d (list (d (n "cpython") (r "^0.1") (d #t) (k 0)) (d (n "cpython-json") (r "^0.2") (k 0)) (d (n "crowbar") (r "^0.2") (d #t) (k 0)) (d (n "python3-sys") (r "^0.1.3") (f (quote ("python-3-4"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "066970qrq3z0x74ajhryq7jm9gxr8sb2csvkvp8rkxkpp64na456") (f (quote (("default" "cpython/python3-sys"))))))

