(define-module (crates-io ga te gatekeeper-core) #:use-module (crates-io))

(define-public crate-gatekeeper-core-0.1.0 (c (n "gatekeeper-core") (v "0.1.0") (d (list (d (n "apdu-core") (r "^0.3.0") (f (quote ("longer_payloads"))) (d #t) (k 0)) (d (n "freefare-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nfc-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "openssl") (r "^0.10.63") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0gzv6pabw1mhqd9h4p16k7cz8wihf6dfyidd9h1iwqf475hcdgdk")))

(define-public crate-gatekeeper-core-0.2.0 (c (n "gatekeeper-core") (v "0.2.0") (d (list (d (n "apdu-core") (r "^0.3.0") (f (quote ("longer_payloads"))) (d #t) (k 0)) (d (n "freefare-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nfc-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "openssl") (r "^0.10.63") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02888nqbnw5y3gz7q1gljv499af8vh3rmjyfziy7cm4y5d1nd25f")))

(define-public crate-gatekeeper-core-0.2.1 (c (n "gatekeeper-core") (v "0.2.1") (d (list (d (n "apdu-core") (r "^0.3.0") (f (quote ("longer_payloads"))) (d #t) (k 0)) (d (n "freefare-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nfc-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "openssl") (r "^0.10.63") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0bnqj4ldpach5zg7wvchncmnyhih08hbfywprypbxls7s45afc43")))

