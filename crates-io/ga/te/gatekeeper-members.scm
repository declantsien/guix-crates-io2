(define-module (crates-io ga te gatekeeper-members) #:use-module (crates-io))

(define-public crate-gatekeeper-members-0.1.0 (c (n "gatekeeper-members") (v "0.1.0") (d (list (d (n "libgatekeeper-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "008v2p5ip1gyshsry56igjbzvg8mi2kqgl3vc6g22bp6c1c66f9a")))

(define-public crate-gatekeeper-members-0.1.1 (c (n "gatekeeper-members") (v "0.1.1") (d (list (d (n "libgatekeeper-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "099sawflb179rvwizkgcl7dr6sgp17arzdhrzs1bchanm908mdfg")))

(define-public crate-gatekeeper-members-0.1.2 (c (n "gatekeeper-members") (v "0.1.2") (d (list (d (n "libgatekeeper-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "173nc17285yq1v3wrn75kyl9jfhhyk0ldbdrg1zkx55sjvkpw531")))

(define-public crate-gatekeeper-members-0.2.0 (c (n "gatekeeper-members") (v "0.2.0") (d (list (d (n "libgatekeeper-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1jvwxjr27zl3ah6ndm8k1d3hcrm7561gbm6ms1llndlqsa0sqm04")))

(define-public crate-gatekeeper-members-0.3.0 (c (n "gatekeeper-members") (v "0.3.0") (d (list (d (n "libgatekeeper-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1c80lkllmpfx52gq7mdi7y9j5b5q5v0hw6h6d926pblgi5x1dbqr")))

(define-public crate-gatekeeper-members-0.4.0 (c (n "gatekeeper-members") (v "0.4.0") (d (list (d (n "gatekeeper-core") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0ji6k7bx969gssby8aq7308yrpp8315ly5xp96dhzw1fmi7bwzh2")))

(define-public crate-gatekeeper-members-0.4.1 (c (n "gatekeeper-members") (v "0.4.1") (d (list (d (n "gatekeeper-core") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1j3qcpdkzfxnvvn84hw2x91fa1mjc3fxc2hv92kf5f848mkrjpxx")))

