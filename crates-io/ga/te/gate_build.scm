(define-module (crates-io ga te gate_build) #:use-module (crates-io))

(define-public crate-gate_build-0.1.0 (c (n "gate_build") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "1nhscjd6h2f1wcdrrx8l8vfs60vqqmbxxrjgylsmnhaqg3xk6wl8")))

(define-public crate-gate_build-0.2.0 (c (n "gate_build") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "1126wsf6cmr9146j931213fd99kwa6snly8wh56vsbv2h5a93vbm")))

(define-public crate-gate_build-0.2.1 (c (n "gate_build") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "0yifmld74c4ksfl45zqnjgi31f355pvs1zj3w4y1d785q6i153f1")))

(define-public crate-gate_build-0.3.0 (c (n "gate_build") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "048kla81zbfbkv77fz732my9n8pxcsq0ziaks4z555y3ip0a4hi3")))

(define-public crate-gate_build-0.4.0 (c (n "gate_build") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "14brgw53qrw09r5qq31lckgrqjy9cpax5yrs9ghmy2n104l2q4ha")))

(define-public crate-gate_build-0.5.0 (c (n "gate_build") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "0n2l0nr65d9r89rwhq723yj6g0591ak2q1pglmvjaqpk76pa74p2")))

(define-public crate-gate_build-0.6.0 (c (n "gate_build") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "0d3kbfs9034dmrjlygia7v2k8fxp3b7jcjafmlj96y005f46f81j")))

(define-public crate-gate_build-0.6.2 (c (n "gate_build") (v "0.6.2") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "1z1iih6f6lhsx27y351q9lnlrwlqil89vgzppa5irjarqiayc3qi")))

(define-public crate-gate_build-0.6.3 (c (n "gate_build") (v "0.6.3") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "0lzs0gssw5f7qyr4s7i7gggpab2zhpw1xg33scsahapgsj9sn1g1")))

