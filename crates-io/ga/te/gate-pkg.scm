(define-module (crates-io ga te gate-pkg) #:use-module (crates-io))

(define-public crate-gate-pkg-0.1.0 (c (n "gate-pkg") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "cw20") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "0bibygk9w2l4k2wxg8nk0016c7qmczvnnijy434jcl6afa967isz") (f (quote (("gate"))))))

