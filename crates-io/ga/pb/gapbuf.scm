(define-module (crates-io ga pb gapbuf) #:use-module (crates-io))

(define-public crate-gapbuf-0.1.0 (c (n "gapbuf") (v "0.1.0") (d (list (d (n "proptest") (r "^0.8.6") (d #t) (k 2)))) (h "1fqkh85lvgl37l2fnqbdqz9fl6794a7fl0xk3rwyl02xidi8k56g")))

(define-public crate-gapbuf-0.1.1 (c (n "gapbuf") (v "0.1.1") (d (list (d (n "proptest") (r "^0.8.6") (d #t) (k 2)))) (h "0s7jdxpk6h7pi37cgsi129g2brfi23cwn2hna9c9q9mfmi3mp35n")))

(define-public crate-gapbuf-0.1.2 (c (n "gapbuf") (v "0.1.2") (d (list (d (n "proptest") (r "^0.8.6") (d #t) (k 2)))) (h "1ia42w6hhlyzb1s7pbw0ai3ynm0rlr603f2zrf7mbd38r1njwgfa") (f (quote (("docs-rs"))))))

(define-public crate-gapbuf-0.1.3 (c (n "gapbuf") (v "0.1.3") (d (list (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "test-strategy") (r "^0.1.0") (d #t) (k 2)))) (h "03m42rql2b5ffix1p912b45354iwniavmm11hzkhdy58ib9a5q2k") (f (quote (("docs-rs"))))))

(define-public crate-gapbuf-0.1.4 (c (n "gapbuf") (v "0.1.4") (d (list (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "test-strategy") (r "^0.2.0") (d #t) (k 2)))) (h "14m5fzph8jc1aa797xlf4dscr9p9a9j2qkqhfnn29whycj9040ca") (f (quote (("docs-rs"))))))

