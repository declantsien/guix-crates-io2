(define-module (crates-io ga pb gapbuffer) #:use-module (crates-io))

(define-public crate-gapbuffer-0.1.0 (c (n "gapbuffer") (v "0.1.0") (h "0giilpfgpy9lhgdjhbwn9r8qzbqn555q1r2qayfkm7z26kr971sy")))

(define-public crate-gapbuffer-0.1.1 (c (n "gapbuffer") (v "0.1.1") (h "05s4mqk5v1p5kk7ai2s1nwm0bdch9026lngmwzfcqjhyy0zrp4iy")))

