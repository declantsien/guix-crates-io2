(define-module (crates-io ga ng ganganonline-parser-lib) #:use-module (crates-io))

(define-public crate-ganganonline-parser-lib-0.0.0 (c (n "ganganonline-parser-lib") (v "0.0.0") (d (list (d (n "pb-rs") (r "^0.8.2") (d #t) (k 1)) (d (n "quick-protobuf") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0b727zcld01v78vv59rwxcvh8400ps4lw2xgn2xwwppb5b9fz4n9") (f (quote (("no_cow") ("default"))))))

