(define-module (crates-io ga wi gawires-diff) #:use-module (crates-io))

(define-public crate-gawires-diff-1.0.0 (c (n "gawires-diff") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.3.4") (o #t) (d #t) (k 0)) (d (n "divsufsort") (r "^2.0.0") (d #t) (k 0)) (d (n "integer-encoding") (r "^2.0.0") (o #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "rayon") (r "^1.4.0") (d #t) (k 0)) (d (n "sacabase") (r "^2.0.0") (d #t) (k 0)) (d (n "sacapart") (r "^2.0.0") (d #t) (k 0)))) (h "0iviyzi2dpmfcsh2pvfcmxinw5zgjiw6x3dqh5k2bk2if64vnnly") (f (quote (("instructions") ("enc" "byteorder" "integer-encoding") ("default" "enc"))))))

