(define-module (crates-io ga wi gawires-patch) #:use-module (crates-io))

(define-public crate-gawires-patch-1.0.0 (c (n "gawires-patch") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "integer-encoding") (r "^2.0.0") (k 0)))) (h "01q4zvc73cmwzqcd3vrhk9nlsnky6b5njcvwz271yr3fzfjaljk8")))

