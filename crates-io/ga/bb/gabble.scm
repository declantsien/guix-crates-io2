(define-module (crates-io ga bb gabble) #:use-module (crates-io))

(define-public crate-gabble-0.1.0 (c (n "gabble") (v "0.1.0") (d (list (d (n "rand") (r "0.8.*") (d #t) (k 0)))) (h "1k4wjwmm2l9mx7wmv0k49yj8nggb58pgxb7ldnli9y4cm4w5al18")))

(define-public crate-gabble-0.1.1 (c (n "gabble") (v "0.1.1") (d (list (d (n "rand") (r "0.8.*") (d #t) (k 0)))) (h "12inwc67wnljymv7fkcrys0kvs0ssnl41bln1pgs2920qwxhff6v")))

