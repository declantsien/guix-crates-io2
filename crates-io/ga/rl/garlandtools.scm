(define-module (crates-io ga rl garlandtools) #:use-module (crates-io))

(define-public crate-garlandtools-0.1.0 (c (n "garlandtools") (v "0.1.0") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "13dj8xpfm5p4xh7pc1f3f166p3r4y1q09iyvk8csq5x8krylifm3")))

(define-public crate-garlandtools-0.1.1 (c (n "garlandtools") (v "0.1.1") (d (list (d (n "bytes") (r "^1.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1830kdcs1zdidab0v3l0n4a7pg8dcaws5axa602byxqiilqq0d6w")))

