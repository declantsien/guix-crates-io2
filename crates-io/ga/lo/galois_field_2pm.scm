(define-module (crates-io ga lo galois_field_2pm) #:use-module (crates-io))

(define-public crate-galois_field_2pm-0.1.0 (c (n "galois_field_2pm") (v "0.1.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0qzaybs6fxwrjphsspfy5g4adqy2wnq39fvs2wq1li41crykkyg2")))

