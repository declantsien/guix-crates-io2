(define-module (crates-io ga lo galois_field) #:use-module (crates-io))

(define-public crate-galois_field-0.1.0 (c (n "galois_field") (v "0.1.0") (h "19lk2bg9hn0v3pb9p7q93xzk150id09m26m9gn19n7r0d4a2sg4n")))

(define-public crate-galois_field-0.1.1 (c (n "galois_field") (v "0.1.1") (h "1kiww2kxgk52vm6qis2db9p82n6zyk44xlld08v1r5k3ggkqh9zx")))

(define-public crate-galois_field-0.1.2 (c (n "galois_field") (v "0.1.2") (h "0y1n3im7i549rwxl51r3p2y9zv9nm69x40y72g0v2ynqgxsd7jk4")))

(define-public crate-galois_field-0.1.3 (c (n "galois_field") (v "0.1.3") (h "0p53if4p9n06fhclfxvqci42appsz5fcra8h7nh8ix9hk2x7vzz1")))

(define-public crate-galois_field-0.1.4 (c (n "galois_field") (v "0.1.4") (h "1z9a40aavn8vklbm67v29d6v6r0bayppyjpl8hn0ahxacdrgqivb")))

(define-public crate-galois_field-0.1.5 (c (n "galois_field") (v "0.1.5") (h "1lf6xk9mgkz99whcbl92kxp6l7lvdqypk1152mvjz8xbc3pywcpw")))

(define-public crate-galois_field-0.1.6 (c (n "galois_field") (v "0.1.6") (h "05z7sl94r36hddc04fs2azkc8gwxyqq1ai6asapdpx0ymqi87m0g")))

(define-public crate-galois_field-0.1.7 (c (n "galois_field") (v "0.1.7") (h "10yc58za80hqnvrm9rb9w5k0000qsrq5cx5mab521xym2mycbaw8")))

(define-public crate-galois_field-0.1.8 (c (n "galois_field") (v "0.1.8") (h "1ai9w1r77ing24576xlz5zddlv3wnmb4z6n3d9ivpbrhvd9wzzjk")))

(define-public crate-galois_field-0.1.9 (c (n "galois_field") (v "0.1.9") (h "1zw1gi92h04vm21fk5hapkxbyy64fls8yhxrnhad425nv4m7g688")))

(define-public crate-galois_field-0.1.10 (c (n "galois_field") (v "0.1.10") (h "1lv9mxn8i0psidx3kzg3hqckq4h2q4h534bn3sacis2kdb3n1vh0")))

(define-public crate-galois_field-0.1.11 (c (n "galois_field") (v "0.1.11") (h "1hzqkrl8i9xp9asm29gsm220jlnwpm5wifzmr5qav2zf26vcv8m1")))

