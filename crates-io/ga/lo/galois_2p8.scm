(define-module (crates-io ga lo galois_2p8) #:use-module (crates-io))

(define-public crate-galois_2p8-0.1.0 (c (n "galois_2p8") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)))) (h "02gka0bwdb88nn3bcrw7zpn853m19iwypc7rs2c00fhgr3xzdsny") (f (quote (("simd"))))))

(define-public crate-galois_2p8-0.1.1 (c (n "galois_2p8") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)))) (h "008p4r4l31dqnk0fjm94ahy0q2c0ww4gs1ms8qgza563by318qcc") (f (quote (("simd"))))))

(define-public crate-galois_2p8-0.1.2 (c (n "galois_2p8") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 2)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)))) (h "143l2xmdxsqqj966722nv1zfncdm7yssixs07f3mc2qa18xskfml") (f (quote (("simd"))))))

