(define-module (crates-io ga ol gaol) #:use-module (crates-io))

(define-public crate-gaol-0.1.0 (c (n "gaol") (v "0.1.0") (h "11ly7mbs8gbwbvrq0x68hsz4ag5c550n8iczv2g64mrcibncxjff")))

(define-public crate-gaol-0.2.0 (c (n "gaol") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1zq7w7l6ghrndx1s0mrvzipkqjryfvm74ak4381v2n3v6z5kgi1m")))

(define-public crate-gaol-0.2.1 (c (n "gaol") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "08lwja6mah80s08nzpgr1fhvvfn7lslr78xwkskkjslngb55f686")))

