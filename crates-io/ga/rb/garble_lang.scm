(define-module (crates-io ga rb garble_lang) #:use-module (crates-io))

(define-public crate-garble_lang-0.1.0 (c (n "garble_lang") (v "0.1.0") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1yyc4dnbhnzyh1c6abzs265k3vg505lrzq1v1gbhml7s5pcgdjk7") (r "1.60.0")))

(define-public crate-garble_lang-0.1.1 (c (n "garble_lang") (v "0.1.1") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "034vbdmnwhr6dwcwwpbaky2qp6x36v9ix4m1b8wc2gqqysll3fqw") (r "1.60.0")))

(define-public crate-garble_lang-0.1.2 (c (n "garble_lang") (v "0.1.2") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1j85fvdrnkys44yxqpyn52a1j107hsaszzwv48ww2dcqiqi5i2zr") (r "1.60.0")))

(define-public crate-garble_lang-0.1.3 (c (n "garble_lang") (v "0.1.3") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1fc2lg2laglpw1jfz3wxmph2zm5r24swg03pp8djmk55ma3nflvw") (r "1.60.0")))

(define-public crate-garble_lang-0.1.4 (c (n "garble_lang") (v "0.1.4") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0fl8bnkv2bxd15vps3zz1bq1x527chbh0aj87xhi2ycisva0qvzq") (r "1.60.0")))

(define-public crate-garble_lang-0.1.6 (c (n "garble_lang") (v "0.1.6") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ry8x4jia72k3pzs67z5bd4j5hw0za7pf5d0mz2iqiy6mz2w3209") (f (quote (("bin" "clap")))) (r "1.60.0")))

(define-public crate-garble_lang-0.1.7 (c (n "garble_lang") (v "0.1.7") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0s1shm0cqndq3xpq7njfagwvm39d0dk11mavckrg66r3j7wb2ng4") (f (quote (("bin" "clap")))) (r "1.60.0")))

(define-public crate-garble_lang-0.1.8 (c (n "garble_lang") (v "0.1.8") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1dmyvvylrq4nfcblnlrf42s9xf99v8s2dj7pr0jxlpkwm4za1c1y") (f (quote (("bin" "clap")))) (r "1.60.0")))

(define-public crate-garble_lang-0.2.0 (c (n "garble_lang") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "08kchljqb0hblwxifhsyxcqfp5vh6ppjhabm9a8z8k61xzfwc9w5") (f (quote (("bin" "clap")))) (r "1.60.0")))

