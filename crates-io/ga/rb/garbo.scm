(define-module (crates-io ga rb garbo) #:use-module (crates-io))

(define-public crate-garbo-0.1.0 (c (n "garbo") (v "0.1.0") (d (list (d (n "bunch") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)))) (h "06sql49anylg75xfwv1gnhid40b5g3n51qp7rzmmpbr6qggjaj7d") (y #t)))

(define-public crate-garbo-0.2.0 (c (n "garbo") (v "0.2.0") (d (list (d (n "bunch") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)))) (h "06svwrkbwppq6b7axcr0f82sp2vzsf1w3pvhn5dzv01gdgqn14qb") (y #t)))

