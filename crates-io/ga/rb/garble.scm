(define-module (crates-io ga rb garble) #:use-module (crates-io))

(define-public crate-garble-0.0.1 (c (n "garble") (v "0.0.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0v4jivlf5bv7gaazgca1g84cimivyykq98ak76w74hfl3zpi8j3c") (f (quote (("simple" "rand") ("default" "simple"))))))

