(define-module (crates-io ga rs gars) #:use-module (crates-io))

(define-public crate-gars-0.0.3 (c (n "gars") (v "0.0.3") (d (list (d (n "google-authenticator") (r "^0.1.9") (d #t) (k 0)))) (h "02gn6x1m19vcvv74iviicnqa8dwqrppgvikcl2qf7485ngr2kcpk") (y #t)))

(define-public crate-gars-0.0.4 (c (n "gars") (v "0.0.4") (d (list (d (n "google-authenticator") (r "^0.1.9") (d #t) (k 0)))) (h "11gkwm964br34paq5q1hs4sbsz9zap9qiwbrsdwflx0jnvv29zd1")))

(define-public crate-gars-0.0.5 (c (n "gars") (v "0.0.5") (d (list (d (n "google-authenticator") (r ">=0.2.1, <0.3.0") (d #t) (k 0)))) (h "0064f6hjxqf6pp4djk3jky1k2xiv4b3wv4v52jn8hznnixrk4l35")))

(define-public crate-gars-0.1.0 (c (n "gars") (v "0.1.0") (d (list (d (n "google-authenticator") (r "^0.3.0") (d #t) (k 0)))) (h "1g374g6sap14mbacvcl3261hrfl05my65lviih377ncrshqj24kx")))

