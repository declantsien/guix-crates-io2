(define-module (crates-io ga nd gandi-client) #:use-module (crates-io))

(define-public crate-gandi-client-1.0.0-alpha.5 (c (n "gandi-client") (v "1.0.0-alpha.5") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "mockito") (r "^0.30") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0mrlz8pgyqd8cnxpq5z8702ya1ygsy15r380g39iycls7w7hvcf0") (y #t)))

(define-public crate-gandi-client-1.0.0-alpha.6 (c (n "gandi-client") (v "1.0.0-alpha.6") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "mockito") (r "^0.30") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0h09xikgqz1sc9z22lxw492k6rs15kjv6v3a3n8vhzl08z11blzm") (y #t)))

