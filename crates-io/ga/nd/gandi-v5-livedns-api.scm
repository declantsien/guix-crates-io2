(define-module (crates-io ga nd gandi-v5-livedns-api) #:use-module (crates-io))

(define-public crate-gandi-v5-livedns-api-0.1.0 (c (n "gandi-v5-livedns-api") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.12.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0w32psy71l9xymwpllppnjh4by9m57wlidnr8vj3hxxpfxlybl8l")))

