(define-module (crates-io ga nd gandalf) #:use-module (crates-io))

(define-public crate-gandalf-0.0.0 (c (n "gandalf") (v "0.0.0") (h "0p6idb01k7nxx7l38zj71ygdm00g5shvhwlvp4dg95n75hwmbf1s") (y #t)))

(define-public crate-gandalf-0.0.1 (c (n "gandalf") (v "0.0.1") (h "16211d3crq12xbkzrb2l6x73qskzymz1m99r4jvrnhmld8zvvhc5") (y #t)))

