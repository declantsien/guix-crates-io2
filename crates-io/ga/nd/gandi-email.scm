(define-module (crates-io ga nd gandi-email) #:use-module (crates-io))

(define-public crate-gandi-email-1.0.0 (c (n "gandi-email") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.9") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "171z3jg7iqcaq0jzwyq142y32ibcgwffrnrbypkcprvmiwv18vp6")))

(define-public crate-gandi-email-1.0.1 (c (n "gandi-email") (v "1.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.9") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0q73bbavm3qdj5y43qc7plnjghrvmgaz0lpyh02j54vl45b3v0df")))

