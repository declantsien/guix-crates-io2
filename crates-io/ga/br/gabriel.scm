(define-module (crates-io ga br gabriel) #:use-module (crates-io))

(define-public crate-gabriel-0.0.1 (c (n "gabriel") (v "0.0.1") (h "1y2z69gvpyh9rgbryk8rxprpi8fy7hq6g9k5hkh81ipwdg899y3c")))

(define-public crate-gabriel-0.0.2 (c (n "gabriel") (v "0.0.2") (h "1vk3rjjsd5cbq1q9y2zaxmsr54n5wffk3b9ka17cpc00q92sc1s0")))

(define-public crate-gabriel-0.0.3 (c (n "gabriel") (v "0.0.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0m82hpsl4y2m51jp0kd18rsbynq9v8nw9icqlv4znpa52n21n98i")))

