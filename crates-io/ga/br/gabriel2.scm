(define-module (crates-io ga br gabriel2) #:use-module (crates-io))

(define-public crate-gabriel2-0.0.1 (c (n "gabriel2") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0ssn6amyv3x7sgj9d59z7nkx7fmg9y3j3dh25959m248vq5rwdsc")))

(define-public crate-gabriel2-1.0.0 (c (n "gabriel2") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0j74l2ajllia13b50d2cnijgpmcyp85bn65fqxxw0cs4gqaa4ga5")))

(define-public crate-gabriel2-1.0.1 (c (n "gabriel2") (v "1.0.1") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0aavgi7i4ifkf1rh73zm6kyhi80bg4w66aczx4aaxig5p8ibff8m")))

(define-public crate-gabriel2-1.0.3 (c (n "gabriel2") (v "1.0.3") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1i2wadxqqagg2mvh0vzbb4lid597yppnxm8xwjzbd6ixkh2gq84d")))

(define-public crate-gabriel2-1.0.4 (c (n "gabriel2") (v "1.0.4") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fl8jk4j0j1fhs01m8wmi7msrlcia3iai5z8jnml7k41hhxnqv71")))

(define-public crate-gabriel2-1.0.5 (c (n "gabriel2") (v "1.0.5") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "075728ij9yn6ps9fhqcg4zi5nypqjxk0jymq6g83bl0xszf7gl2v")))

