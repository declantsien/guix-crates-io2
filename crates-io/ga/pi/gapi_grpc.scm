(define-module (crates-io ga pi gapi_grpc) #:use-module (crates-io))

(define-public crate-gapi_grpc-0.0.1 (c (n "gapi_grpc") (v "0.0.1") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)) (d (n "tonic") (r "^0.2") (f (quote ("tls" "tls-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.2") (d #t) (k 1)))) (h "0jynd4s5rggc5kix8kdc7pafkg75cxxbbbawxz3pblswl2mj259c")))

(define-public crate-gapi_grpc-0.0.2 (c (n "gapi_grpc") (v "0.0.2") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)) (d (n "tonic") (r "^0.2") (f (quote ("tls" "tls-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.2") (d #t) (k 1)))) (h "1sanf7wj7lixrkfkraqqs8mbqqww79ysq5ciywfswcprmggkbqii")))

(define-public crate-gapi_grpc-0.0.3 (c (n "gapi_grpc") (v "0.0.3") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)) (d (n "tonic") (r "^0.2") (f (quote ("tls" "tls-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.2") (d #t) (k 1)))) (h "1jq9qd631kn1pbgg1447hsjbzbhjmkz15ly7nyfz6ndb81kgx0j0")))

(define-public crate-gapi_grpc-0.0.4 (c (n "gapi_grpc") (v "0.0.4") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)) (d (n "tonic") (r "^0.2") (f (quote ("tls" "tls-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.2") (d #t) (k 1)))) (h "0dpdp5f61qvx43a2z0gzcfcv8282319iggdfs25m5kxw72gcpj55")))

(define-public crate-gapi_grpc-0.0.5 (c (n "gapi_grpc") (v "0.0.5") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)) (d (n "tonic") (r "^0.2") (f (quote ("tls" "tls-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.2") (d #t) (k 1)))) (h "1fvldxpah56nvlbfx9fwyqxxm3iyi36bylvhd168qmz36ldhfb4c")))

(define-public crate-gapi_grpc-0.0.6 (c (n "gapi_grpc") (v "0.0.6") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)) (d (n "tonic") (r "^0.2") (f (quote ("tls" "tls-roots"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.2") (d #t) (k 1)))) (h "154ic41sh4sd8qky7pxz5i095ndr45wvv27kqpaxj0sygam6hc6n")))

