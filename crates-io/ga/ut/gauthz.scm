(define-module (crates-io ga ut gauthz) #:use-module (crates-io))

(define-public crate-gauthz-0.1.0 (c (n "gauthz") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "medallion") (r "^2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "03b0zhw2p87rf8hy3yvv180hrdx54df4aq9hvycfxq4n1caxcsn8") (f (quote (("tls" "hyper-tls") ("default" "tls"))))))

