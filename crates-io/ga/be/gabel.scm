(define-module (crates-io ga be gabel) #:use-module (crates-io))

(define-public crate-gabel-0.0.0 (c (n "gabel") (v "0.0.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.4.4") (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2.15") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "temp-file") (r "^0.1.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "time") (r "^0.3.30") (f (quote ("macros" "formatting"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("time" "env-filter"))) (d #t) (k 0)))) (h "1m9v7qy4f8r9x1i4sjk70h3wg86mzkazdwjyvddpcg2ic3zilni1")))

