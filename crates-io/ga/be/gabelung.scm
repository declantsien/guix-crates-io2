(define-module (crates-io ga be gabelung) #:use-module (crates-io))

(define-public crate-gabelung-0.2.0 (c (n "gabelung") (v "0.2.0") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "1z9pr35xsq84ysq47wgysqvcj2210s8kf5idjpiiadsczgsyw9bb")))

(define-public crate-gabelung-0.2.1 (c (n "gabelung") (v "0.2.1") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "14bp1n4cj8nf827annpdfybbm5fj4550xnwc4h481fngyxifxj2x")))

(define-public crate-gabelung-0.2.2 (c (n "gabelung") (v "0.2.2") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "0ya2hsmz5672qybliv2d4d67jysprq6h1vrrq2z5sjsy7ld0h647")))

(define-public crate-gabelung-0.2.3 (c (n "gabelung") (v "0.2.3") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "1cf92vz7d8i2fhhzvwamg4srrmhfrg3jxfh123ham96vdlvzfybb")))

(define-public crate-gabelung-0.2.4 (c (n "gabelung") (v "0.2.4") (d (list (d (n "async-std") (r "^1.6.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "1lp6vh10wbzqrm4l41x9in4mal8y87qxkb3m5r5i9xxw90fbhqd3")))

