(define-module (crates-io ga rf garfsay) #:use-module (crates-io))

(define-public crate-garfsay-0.1.0 (c (n "garfsay") (v "0.1.0") (h "0lyzklss3ywfm2dagrhzg3zmpalgbsa1lp054xlzzgfrzrh6qyj3")))

(define-public crate-garfsay-0.1.1 (c (n "garfsay") (v "0.1.1") (h "0ic2bsmc2nfxrp0q51wxxfg9gz5llnwzwdy70k3n33an39djy028")))

