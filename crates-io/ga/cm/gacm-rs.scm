(define-module (crates-io ga cm gacm-rs) #:use-module (crates-io))

(define-public crate-gacm-rs-0.0.2 (c (n "gacm-rs") (v "0.0.2") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1r9r4pw91scg5cl1l9hrrj75qh9msfnd4m1gfm4k795cw0xf9s0m")))

(define-public crate-gacm-rs-0.0.3 (c (n "gacm-rs") (v "0.0.3") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorized") (r "^1.0.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)) (d (n "url") (r "^2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1aqwcrizv94lg7482myp109cqkpyj64fgz3wy08bzbi4qs2kkbp9")))

