(define-module (crates-io ga st gastimation) #:use-module (crates-io))

(define-public crate-gastimation-0.0.0 (c (n "gastimation") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colorize") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^1.8.0") (f (quote ("full"))) (d #t) (k 0)))) (h "166ykmcnnm4hfsvvqavamwv66vvir1dg0j4k7rghl0qr2pfjycz6")))

