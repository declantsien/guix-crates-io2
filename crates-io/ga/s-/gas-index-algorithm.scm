(define-module (crates-io ga s- gas-index-algorithm) #:use-module (crates-io))

(define-public crate-gas-index-algorithm-0.1.0 (c (n "gas-index-algorithm") (v "0.1.0") (d (list (d (n "micromath") (r "^2.0") (d #t) (k 0)))) (h "0l9rd22hjwmdxy5ryff6bqmlgagjw4vq4v3b15f08ixizva7ydxp")))

(define-public crate-gas-index-algorithm-0.1.1 (c (n "gas-index-algorithm") (v "0.1.1") (d (list (d (n "micromath") (r "^2.0") (d #t) (k 0)))) (h "1pgjbkhbmlk3711lqvh7czvm0vis1nfjljysyva5kg8i1vcqgv7p")))

(define-public crate-gas-index-algorithm-0.1.2 (c (n "gas-index-algorithm") (v "0.1.2") (d (list (d (n "micromath") (r "^2.0") (d #t) (k 0)))) (h "09b3xzbnrpqpxd8imfw0l5k79h13mgx9n0j0x574lb1wwqbdyxik")))

(define-public crate-gas-index-algorithm-0.1.3 (c (n "gas-index-algorithm") (v "0.1.3") (d (list (d (n "micromath") (r "^2.0") (d #t) (k 0)))) (h "0s0isgxib0xmaq1ra75107hyy2mf6rnxm1lvmb4rsizzx06hgrcq")))

