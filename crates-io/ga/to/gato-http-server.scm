(define-module (crates-io ga to gato-http-server) #:use-module (crates-io))

(define-public crate-gato-http-server-0.0.1 (c (n "gato-http-server") (v "0.0.1") (d (list (d (n "gato-core") (r "^0.5") (d #t) (k 0)))) (h "06gaccy5cy2c9w0jdjifni7ljn3vv8mdj9i04y2rbqqp2c8qhj6j")))

(define-public crate-gato-http-server-0.0.2 (c (n "gato-http-server") (v "0.0.2") (d (list (d (n "gato-core") (r "^0.5") (d #t) (k 0)))) (h "0cbnry4i5hzi2m3626h4n307b4cl6zqgpbxw36m3qkgxv1carw1j")))

(define-public crate-gato-http-server-0.0.3 (c (n "gato-http-server") (v "0.0.3") (d (list (d (n "gato-core") (r "^0.5") (d #t) (k 0)))) (h "0v47vd45n09n724nb4di6s6l3kf3x856nf7xxi2ncz1wg14gxxgi")))

(define-public crate-gato-http-server-0.0.4 (c (n "gato-http-server") (v "0.0.4") (d (list (d (n "gato-core") (r "^0.5") (d #t) (k 0)))) (h "08iynxjjl11xfan2izjnqiy8n5vizc3hrjw9jlhbnm33hdpcim00")))

