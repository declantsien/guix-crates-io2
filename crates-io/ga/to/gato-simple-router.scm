(define-module (crates-io ga to gato-simple-router) #:use-module (crates-io))

(define-public crate-gato-simple-router-0.1.0 (c (n "gato-simple-router") (v "0.1.0") (d (list (d (n "gato-core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qwklg9f8kzwznli5k557x2h032bz1wi2w16g87jgqrv2k9d0p64")))

(define-public crate-gato-simple-router-0.2.1 (c (n "gato-simple-router") (v "0.2.1") (d (list (d (n "gato-core") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0w0cpyx13np5cmwify769fslnc2gdsblj4wmjk7ni3bpwkf5qhip")))

(define-public crate-gato-simple-router-0.4.0 (c (n "gato-simple-router") (v "0.4.0") (d (list (d (n "gato-core") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "124sacl8ml1qy8ad8yvpvfxlm53cl5h4ckzvyjz53p9qnmkis363")))

(define-public crate-gato-simple-router-0.4.1 (c (n "gato-simple-router") (v "0.4.1") (d (list (d (n "gato-core") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01gwdn4vldgjfliczs00ykra1m71j1sr2lnjs6bm6ybbqxx93vqx")))

(define-public crate-gato-simple-router-0.4.2 (c (n "gato-simple-router") (v "0.4.2") (d (list (d (n "gato-core") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xdxqnkfql6k4y2j9fzgm2a06ycpb47hf8kmdzj3mak31qhpxyp8")))

(define-public crate-gato-simple-router-0.4.3 (c (n "gato-simple-router") (v "0.4.3") (d (list (d (n "gato-core") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yw8fbdxjprzsjdxq746hkwv5xz6ymz10cb8iwp9ff2ia88i0f02")))

(define-public crate-gato-simple-router-0.5.0 (c (n "gato-simple-router") (v "0.5.0") (d (list (d (n "gato-core") (r "^0.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06cyjl58daa19ak9q969i2xv8mx7w5mb4g5m22rmz3db0v7sqc73")))

(define-public crate-gato-simple-router-0.5.1 (c (n "gato-simple-router") (v "0.5.1") (d (list (d (n "gato-core") (r "^0.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11d6rg78x6m8hdbw3c337m5n7zkdzc6h98pm5h79d89c8ppd3r0x")))

(define-public crate-gato-simple-router-0.5.2 (c (n "gato-simple-router") (v "0.5.2") (d (list (d (n "gato-core") (r "^0.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19da1zlmzf7r1j23m07kaisrd0y69rqm8v1qb693cyswbds3bz2b")))

(define-public crate-gato-simple-router-0.5.3 (c (n "gato-simple-router") (v "0.5.3") (d (list (d (n "gato-core") (r "^0.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1h65gd39f2vr9gx5m0l1irb894kir7r335b0cgglpnkglk6y8xhs")))

(define-public crate-gato-simple-router-0.5.4 (c (n "gato-simple-router") (v "0.5.4") (d (list (d (n "gato-core") (r "^0.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16vkgpwcipwmnx9pjg6zrw3pzvygfpqzdq9ajwza9qqr3klvpx8z")))

