(define-module (crates-io ga to gato-core) #:use-module (crates-io))

(define-public crate-gato-core-0.1.0 (c (n "gato-core") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hvx20nkc85451g24x88blvfnyg41sqq21bgnsfnrcmyhxfl68d5")))

(define-public crate-gato-core-0.2.0 (c (n "gato-core") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1r6yimjs7m90b4dc8l1jq3a5x02hkajpakw7shj0jk7pc2d5p0xx")))

(define-public crate-gato-core-0.2.1 (c (n "gato-core") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0b1zbf01rhp2qyhzc3fs79asgar1n0c2hq6da24854jh0ajv0aak")))

(define-public crate-gato-core-0.2.2 (c (n "gato-core") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1k0a0l3h96jms93bj91i4s4hlga9s4y8q5gpm0sk20fa12cp269p")))

(define-public crate-gato-core-0.3.0 (c (n "gato-core") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cmsary8bvq4f283d06xhkr2a6lb0vx0yb50yn67czdwd6j515b4")))

(define-public crate-gato-core-0.4.0 (c (n "gato-core") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yxsgbnf1qlqf3fdsrily9piqv9f5vid7lnizzn6a0p9wyhskc9h")))

(define-public crate-gato-core-0.4.1 (c (n "gato-core") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sw4k2ck40n14vgwi8a2vahaqrdzn36f7fgmaap3hihll6xdhbfm")))

(define-public crate-gato-core-0.4.2 (c (n "gato-core") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dv07ivvli9p3z0k60kf4kb2z0hqlk56my7l5bs3nbsk9ix9vjjp")))

(define-public crate-gato-core-0.5.0 (c (n "gato-core") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m48w92anjkdnlb23i5w08hab3l0wkq2r9q8cs6n50105s4ncaz9")))

(define-public crate-gato-core-0.5.1 (c (n "gato-core") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nfxjyimiwzy0a1afk3fii1bk1my9inrn9kcrglw0c1akaz3br17")))

(define-public crate-gato-core-0.5.2 (c (n "gato-core") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jh3shklab1vjfjjqd1rdj3wjasz2jaxlr8mmfcyjw13izkj7zz8")))

(define-public crate-gato-core-0.5.3 (c (n "gato-core") (v "0.5.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qy44zmb58zziyfjq50xrsshfprx1ccyvifczaxhlawhjv6qf249")))

(define-public crate-gato-core-0.5.4 (c (n "gato-core") (v "0.5.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nbdisc90c28yi85l0mfahiib2mmffmq36xcsn4rzvvccarls07r")))

(define-public crate-gato-core-0.5.5 (c (n "gato-core") (v "0.5.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "102bvlyg967aaz27v1d892m41nzxkmsl82pjllr5f3x3hjlv9mqq")))

(define-public crate-gato-core-0.5.6 (c (n "gato-core") (v "0.5.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "141as46xjaa3zwyvnwngaiiv4s3pcgrqzax59wy3z8d9y9hg2v11")))

(define-public crate-gato-core-0.5.7 (c (n "gato-core") (v "0.5.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ax47pnhycmlws8zv6bj7g8k1k3pwccymhl8li3sn3i57614gxgf")))

