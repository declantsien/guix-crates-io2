(define-module (crates-io ga to gato-stdout-logger) #:use-module (crates-io))

(define-public crate-gato-stdout-logger-0.5.0 (c (n "gato-stdout-logger") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "gato-core") (r "^0.5.2") (d #t) (k 0)))) (h "1zhac7b139dfjp61z85zhm8vblssi8qdd8lhi9xv02gzvza786i2")))

