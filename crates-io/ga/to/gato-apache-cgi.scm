(define-module (crates-io ga to gato-apache-cgi) #:use-module (crates-io))

(define-public crate-gato-apache-cgi-0.1.0 (c (n "gato-apache-cgi") (v "0.1.0") (d (list (d (n "gato-core") (r "^0.1.0") (d #t) (k 0)))) (h "1bn1v6dd6grz7p1g1jm424czffd6vayqgjq30hw2vzy6fgxp0p42")))

(define-public crate-gato-apache-cgi-0.2.1 (c (n "gato-apache-cgi") (v "0.2.1") (d (list (d (n "gato-core") (r "^0.2.1") (d #t) (k 0)))) (h "0080qm9kfkaxb63x7his7i45l034z0lbcr43m4fhgv06fqp9rpy5")))

(define-public crate-gato-apache-cgi-0.4.0 (c (n "gato-apache-cgi") (v "0.4.0") (d (list (d (n "gato-core") (r "^0.4.0") (d #t) (k 0)))) (h "19lznkmdz9f47jcviqqa6f66fd9wh54vdrx1p8grbr6bys4frglq")))

(define-public crate-gato-apache-cgi-0.4.1 (c (n "gato-apache-cgi") (v "0.4.1") (d (list (d (n "gato-core") (r "^0.4.0") (d #t) (k 0)))) (h "08s99h4rryp8h2h8gb3f2637jhykzczibilrjjbg8mblrdnqfsng")))

(define-public crate-gato-apache-cgi-0.5.0 (c (n "gato-apache-cgi") (v "0.5.0") (d (list (d (n "gato-core") (r "^0.5") (d #t) (k 0)))) (h "1y1l5bw1xsav40y7sh9pcqrdwhcvyw1l2gyp6k6qvvz4rvi7h22y")))

