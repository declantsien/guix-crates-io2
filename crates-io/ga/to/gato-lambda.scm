(define-module (crates-io ga to gato-lambda) #:use-module (crates-io))

(define-public crate-gato-lambda-0.5.0 (c (n "gato-lambda") (v "0.5.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "gato-core") (r "^0.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fly509s62aa8ns8rlqsr1yyskq36qn6n6capwbisnzrwm14hp5n")))

(define-public crate-gato-lambda-0.5.1 (c (n "gato-lambda") (v "0.5.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "gato-core") (r "^0.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vrr6r0r7hqhbhwv3x5p4m9kj0lxqv5n8qcsfq00ym9ms62vbkk8")))

