(define-module (crates-io ga bc gabc-parser) #:use-module (crates-io))

(define-public crate-gabc-parser-0.1.0 (c (n "gabc-parser") (v "0.1.0") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "pest") (r "^1.0.6") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0mm3sx2a97zgqjayhamvpdgs9xg44hcx8kqhv2yafhp26c3fp13b")))

(define-public crate-gabc-parser-0.1.1 (c (n "gabc-parser") (v "0.1.1") (d (list (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "pest") (r "^1.0.6") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "128r067s0g2hirk6zkqmswbpjd7dwqlnifjxhwl40pjk5q2hff3g")))

