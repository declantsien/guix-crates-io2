(define-module (crates-io ga rg gargoyle-feed-monitor) #:use-module (crates-io))

(define-public crate-gargoyle-feed-monitor-0.1.0 (c (n "gargoyle-feed-monitor") (v "0.1.0") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "feed-rs") (r "^1.5.2") (d #t) (k 0)) (d (n "gargoyle") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0h8wcgyd88rvimv30rrknfx4j2kb60a5ll3vza2yab1s15q6kzra") (y #t)))

(define-public crate-gargoyle-feed-monitor-0.1.1 (c (n "gargoyle-feed-monitor") (v "0.1.1") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "feed-rs") (r "^1.5.2") (d #t) (k 0)) (d (n "gargoyle") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1985ccik4gmvsl3z79aqhh6ah0qmrc9i7nw24jqmizjhs1ifkkxl")))

(define-public crate-gargoyle-feed-monitor-0.1.2 (c (n "gargoyle-feed-monitor") (v "0.1.2") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "feed-rs") (r "^1.5.2") (d #t) (k 0)) (d (n "gargoyle") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking"))) (d #t) (k 0)))) (h "07yf7klyarzwfphzcjwpp41mmvjq7y03va9izcrhsdli1k519mb6") (y #t)))

(define-public crate-gargoyle-feed-monitor-0.1.3 (c (n "gargoyle-feed-monitor") (v "0.1.3") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "feed-rs") (r "^1.5.2") (d #t) (k 0)) (d (n "gargoyle") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0ifcbnk41mrl9qwwkpllcywnng1ydk65nrlyxkgibkndjh5kmc5z")))

