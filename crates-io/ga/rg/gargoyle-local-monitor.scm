(define-module (crates-io ga rg gargoyle-local-monitor) #:use-module (crates-io))

(define-public crate-gargoyle-local-monitor-0.1.0 (c (n "gargoyle-local-monitor") (v "0.1.0") (d (list (d (n "gargoyle") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.12") (d #t) (k 0)))) (h "0wdv2whjnz552k1qh0gh4xmlpqljgk3lgmfwsf6ygln9c8lc253m")))

