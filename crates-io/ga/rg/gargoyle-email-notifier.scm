(define-module (crates-io ga rg gargoyle-email-notifier) #:use-module (crates-io))

(define-public crate-gargoyle-email-notifier-0.1.0 (c (n "gargoyle-email-notifier") (v "0.1.0") (d (list (d (n "gargoyle") (r "^0.1.5") (d #t) (k 0)) (d (n "lettre") (r "^0.11.7") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "1m0sdh7v6ldcs32j19278vl0jr9g7p5n9clkjvnn5xfxhps6lhgs")))

