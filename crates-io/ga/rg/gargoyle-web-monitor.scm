(define-module (crates-io ga rg gargoyle-web-monitor) #:use-module (crates-io))

(define-public crate-gargoyle-web-monitor-0.1.0 (c (n "gargoyle-web-monitor") (v "0.1.0") (d (list (d (n "gargoyle") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0w91q1qiz7hxaiv6078qxw1gqrl59km94mmds69780anc54wf14m")))

(define-public crate-gargoyle-web-monitor-0.1.1 (c (n "gargoyle-web-monitor") (v "0.1.1") (d (list (d (n "gargoyle") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking"))) (d #t) (k 0)))) (h "1awjbiw66csq6apa694l2vchffl9dl7rcm2bmwj0889nn6dayv5a")))

