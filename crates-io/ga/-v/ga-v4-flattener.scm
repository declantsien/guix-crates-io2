(define-module (crates-io ga -v ga-v4-flattener) #:use-module (crates-io))

(define-public crate-ga-v4-flattener-0.1.0 (c (n "ga-v4-flattener") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "indoc") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f71jvyq2n3hqxmigwqxgdzl6a6y65bk02dy2bmwjwj5kc6b5vbn")))

(define-public crate-ga-v4-flattener-0.1.1 (c (n "ga-v4-flattener") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "indoc") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m8wqi22qj94qlhb4hk57wvpqidkfrjxydx11zlr8balinhfsbc7")))

(define-public crate-ga-v4-flattener-0.2.1 (c (n "ga-v4-flattener") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "indoc") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14aabc94ya2b86lph25j7gfma1c5byry4xgib26sa6w7in4gx2pf") (y #t)))

(define-public crate-ga-v4-flattener-0.3.0 (c (n "ga-v4-flattener") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "indoc") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vvh2rfysdbkid5rpd4p6j4d6hl03nxcqhv8x7py0bp3w2v7vq4w")))

(define-public crate-ga-v4-flattener-0.4.0 (c (n "ga-v4-flattener") (v "0.4.0") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "indoc") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1aw304v17sxn996shw7h2h23kl8nkvn4djclmiwl0i7b79s3c7z4")))

