(define-module (crates-io ga bo gaborator-sys) #:use-module (crates-io))

(define-public crate-gaborator-sys-0.1.0 (c (n "gaborator-sys") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "num-complex") (r "^0.3.1") (d #t) (k 2)))) (h "0g46aw1g8k435ivmhiig00h1wi6gvvy1118f1s786y6vwkqivr0p")))

(define-public crate-gaborator-sys-0.1.1 (c (n "gaborator-sys") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "num-complex") (r "^0.3.1") (d #t) (k 2)))) (h "02ngp6g145p3w723vhf2ngp9shp456pb62h39wwjwa2nvfcgrjiz")))

