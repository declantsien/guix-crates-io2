(define-module (crates-io ga bo gaborator) #:use-module (crates-io))

(define-public crate-gaborator-0.1.1 (c (n "gaborator") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "gaborator-sys") (r "=0.1.1") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "num-complex") (r "^0.3.1") (d #t) (k 2)))) (h "1lwwpi0p2mmb90hhcpp9lh7an3vzmgampx5dj9kq5ypkfm8h8xqd")))

