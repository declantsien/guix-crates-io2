(define-module (crates-io ga lb galbi) #:use-module (crates-io))

(define-public crate-galbi-0.0.0 (c (n "galbi") (v "0.0.0") (h "1l28g1vjx8qplwkd54dz3rk4gv62v1izvv3viy3adhk6rm73v2h8")))

(define-public crate-galbi-0.1.0 (c (n "galbi") (v "0.1.0") (h "1qakrphq2j9q1i4alx833d1gzn46vzbh0vjb2kc7ylv2rkpc3ilh")))

(define-public crate-galbi-0.2.0 (c (n "galbi") (v "0.2.0") (h "1v9an5w0iqqxy36srzh914hi1wrbxdnfvd61jawjcniakvfb7qbh")))

(define-public crate-galbi-0.2.1 (c (n "galbi") (v "0.2.1") (h "1akb5clr7bdacndyk4lv9nb4ianq32j7a38z15p39pnmzyiwwazf")))

(define-public crate-galbi-0.3.0 (c (n "galbi") (v "0.3.0") (h "14w5c5zlxpihr0p07yjgc1mkqkj5dg5xyc42rv6i72kqsy8fd94k")))

