(define-module (crates-io ga na ganache) #:use-module (crates-io))

(define-public crate-ganache-0.1.0 (c (n "ganache") (v "0.1.0") (d (list (d (n "downcast-rs") (r "^1.0.4") (d #t) (k 0)))) (h "0pq67jdz9alb9cfi2h0240yrhwin8lic5pd7rnjn1zml785jn2yg") (f (quote (("scalar_i32"))))))

(define-public crate-ganache-0.1.1 (c (n "ganache") (v "0.1.1") (d (list (d (n "downcast-rs") (r "^1.0.4") (d #t) (k 0)))) (h "1bcv9pw1lcv3qf4n8q196hhx1ylbnlys46v5860pbxz9hijapp3g") (f (quote (("scalar_i32"))))))

(define-public crate-ganache-0.1.2 (c (n "ganache") (v "0.1.2") (d (list (d (n "downcast-rs") (r "^1.0.4") (d #t) (k 0)))) (h "08xm0mp56zd2f46ys9gb1l6gnbf4xs4ymrg0adlhn1bqk66bj9ic") (f (quote (("scalar_i32"))))))

(define-public crate-ganache-0.1.3 (c (n "ganache") (v "0.1.3") (d (list (d (n "downcast-rs") (r "^1.0.4") (d #t) (k 0)))) (h "1k39i7p5q2lizn9qv68mi5l7b36zjvx9h4isvf3k8yk0g8rip5sz") (f (quote (("scalar_i32"))))))

(define-public crate-ganache-0.1.4 (c (n "ganache") (v "0.1.4") (d (list (d (n "downcast-rs") (r "^1.0.4") (d #t) (k 0)) (d (n "ggez") (r "^0.5.0-rc.2") (d #t) (k 2)))) (h "1pmwzyx9hm777vsgxhbgqyd40wg1l1z0mbqvchg8kjq68jzya1r5") (f (quote (("scalar_i32"))))))

(define-public crate-ganache-0.1.5 (c (n "ganache") (v "0.1.5") (d (list (d (n "downcast-rs") (r "^1.0.4") (d #t) (k 0)) (d (n "ggez") (r "^0.5.0-rc.2") (d #t) (k 2)))) (h "1x397q9d3lmv6fkp8mbnkqivdai34nbyfmpn3dkc7rnwldfaf8fx") (f (quote (("scalar_i32"))))))

(define-public crate-ganache-0.1.6 (c (n "ganache") (v "0.1.6") (d (list (d (n "downcast-rs") (r "^1.0.4") (d #t) (k 0)) (d (n "ggez") (r "^0.5.0-rc.2") (d #t) (k 2)))) (h "024dk9a2cv3g5as2iqigliwxv3a2yhzn4fzicafiqc3qwwjvwgr9") (f (quote (("scalar_i32"))))))

(define-public crate-ganache-0.1.7 (c (n "ganache") (v "0.1.7") (d (list (d (n "downcast-rs") (r "^1.0.4") (d #t) (k 0)) (d (n "ggez") (r "^0.5.0-rc.2") (d #t) (k 2)))) (h "14cjias8hkjhl79gh4w27i58ch670iifxx7s6df902ngz73y7ll2") (f (quote (("scalar_i32"))))))

