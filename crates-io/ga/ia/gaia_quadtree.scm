(define-module (crates-io ga ia gaia_quadtree) #:use-module (crates-io))

(define-public crate-gaia_quadtree-0.1.0 (c (n "gaia_quadtree") (v "0.1.0") (h "0nhq6qsd241q48qw5dysh7bddq7mk6ddxxk3k0190qmph9x8xkd7")))

(define-public crate-gaia_quadtree-0.1.1 (c (n "gaia_quadtree") (v "0.1.1") (h "0zfr6sb4ks1r0k8af47dyblqilhjgj0yv08a3jqg98kivz1h73ha")))

(define-public crate-gaia_quadtree-0.1.2 (c (n "gaia_quadtree") (v "0.1.2") (h "0a7ypc5lfwr2sf8dqf7fskjxbc3jx5j5isf435psg994fq9ykgkl")))

(define-public crate-gaia_quadtree-0.1.3 (c (n "gaia_quadtree") (v "0.1.3") (h "0zcd3qyiy59h1y4sdl14gng0l3lz0xw5n5qf2qm0cmwnkg8n46r3")))

(define-public crate-gaia_quadtree-0.1.4 (c (n "gaia_quadtree") (v "0.1.4") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1an71acxz7sm2pc5n22mwi5jsmc90xk0liwhh4l7gfxcwz6kip1p")))

(define-public crate-gaia_quadtree-0.1.5 (c (n "gaia_quadtree") (v "0.1.5") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1y9f5k2598k791gpibhqisqw1jg5lcsswp9sz5jvb8krr9ayj5jc")))

(define-public crate-gaia_quadtree-0.1.6 (c (n "gaia_quadtree") (v "0.1.6") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0sh2liwisvskhmpz5ba4c31318s9b1hdr1slv6wk4raydziwai2r")))

(define-public crate-gaia_quadtree-0.1.7 (c (n "gaia_quadtree") (v "0.1.7") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0cwf5jb3lbs6l1fyxf63l2l4v9sy3g8699cgxllkmhxnps0krwy3")))

