(define-module (crates-io ga ia gaia-stub) #:use-module (crates-io))

(define-public crate-gaia-stub-0.7.0-beta.2 (c (n "gaia-stub") (v "0.7.0-beta.2") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10") (d #t) (k 1)))) (h "19i8y50sjpgygba7zay65iidab6zrlfb03yzhhmmyd43y6cy810q")))

(define-public crate-gaia-stub-0.7.0-beta.3 (c (n "gaia-stub") (v "0.7.0-beta.3") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10") (d #t) (k 1)))) (h "09dvpir8asq80wxa108jkfzdnixilj6hpcllbqawvyglpas6h887")))

(define-public crate-gaia-stub-0.7.0-beta.5 (c (n "gaia-stub") (v "0.7.0-beta.5") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10") (d #t) (k 1)))) (h "1sv783mwywiywdynh75csgqqpf6l0h0swha96siy9dgnxnidbf7b")))

(define-public crate-gaia-stub-0.7.0 (c (n "gaia-stub") (v "0.7.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10") (d #t) (k 1)))) (h "13yrlds5sifp540b3iqssgfqsi2l4n2vr7kzl5574q3lnjba5rli")))

(define-public crate-gaia-stub-1.0.0 (c (n "gaia-stub") (v "1.0.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "prost-types") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.11") (d #t) (k 0)) (d (n "tonic-build") (r "^0.11") (d #t) (k 1)))) (h "1y4m4pqiqs4656gd6nwg2pbq39lcpsb2lf1bnkx64znhb8711k1b")))

