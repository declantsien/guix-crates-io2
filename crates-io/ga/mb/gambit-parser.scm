(define-module (crates-io ga mb gambit-parser) #:use-module (crates-io))

(define-public crate-gambit-parser-0.1.0 (c (n "gambit-parser") (v "0.1.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1ja27nwa3l35zw6nzxlymd14aqdjpvcqsd1bawbqcw4s803x3d03")))

(define-public crate-gambit-parser-0.2.0 (c (n "gambit-parser") (v "0.2.0") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0q2a6n3qk6wakdv53yisc3i3y2hq9vzbm0x3a3bgiqjjin28yr9s")))

