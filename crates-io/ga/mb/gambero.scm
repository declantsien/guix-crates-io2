(define-module (crates-io ga mb gambero) #:use-module (crates-io))

(define-public crate-gambero-0.1.0 (c (n "gambero") (v "0.1.0") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "async-tungstenite") (r "^0.13.0") (f (quote ("async-std-runtime" "async-tls"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "squalo") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1i95yp1w64820ka0l1s1cmdybmi9zflnavlrnpwjwrb3kx4ahglq")))

(define-public crate-gambero-0.1.1 (c (n "gambero") (v "0.1.1") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "async-tungstenite") (r "^0.13.0") (f (quote ("async-std-runtime" "async-tls"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "squalo") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "017a5z0b0h6qjm4209k2vfgj6504b76dx7lbrc6ybmf153fmr9mh")))

(define-public crate-gambero-0.1.2 (c (n "gambero") (v "0.1.2") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "async-tungstenite") (r "^0.13.0") (f (quote ("async-std-runtime" "async-tls"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "squalo") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bhl94k9rmd1zlvmnahbja7gj7q6p37l5nakwx38i2vqnm26lx2v")))

