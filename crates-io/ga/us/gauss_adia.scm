(define-module (crates-io ga us gauss_adia) #:use-module (crates-io))

(define-public crate-gauss_adia-0.1.0 (c (n "gauss_adia") (v "0.1.0") (h "1xxg61gnzh89pmr8ycf7lm8ba77yfhsrwzaw3ixxj9rsb26v7m9x")))

(define-public crate-gauss_adia-0.1.1 (c (n "gauss_adia") (v "0.1.1") (h "1lbznvzax4k9jvglssmb8m3k6pjf1qkcs7jdbxn9yvx5ij2g3bf9")))

