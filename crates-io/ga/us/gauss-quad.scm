(define-module (crates-io ga us gauss-quad) #:use-module (crates-io))

(define-public crate-gauss-quad-0.1.0 (c (n "gauss-quad") (v "0.1.0") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)))) (h "1g32z01f39l54yl3kalxyvi333vzkyrc28lpsvc4kvqlhq5ly2z8")))

(define-public crate-gauss-quad-0.1.1 (c (n "gauss-quad") (v "0.1.1") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)))) (h "0rjbipn55h1ii1frxi3x1s1msyawc4rwi588r33jban1llawzbb8")))

(define-public crate-gauss-quad-0.1.2 (c (n "gauss-quad") (v "0.1.2") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "statrs") (r "^0.11.0") (d #t) (k 0)))) (h "1ysdn8qj3hpkqbr2vbm9j13ah2n5fcr3i7xd2sd416dd0mp8wdr3")))

(define-public crate-gauss-quad-0.1.3 (c (n "gauss-quad") (v "0.1.3") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "statrs") (r "^0.11.0") (d #t) (k 0)))) (h "1pni1zyhdgxkrn0i1fcxrkzj6ly7c9r885xxd8dl0zxw53qls6gj")))

(define-public crate-gauss-quad-0.1.4 (c (n "gauss-quad") (v "0.1.4") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.18") (d #t) (k 0)) (d (n "statrs") (r "^0.11.0") (d #t) (k 0)))) (h "1gs86ga15l57nks1vw6pi8xfghh4za7xvp098b0q5a0md90dfhrb")))

(define-public crate-gauss-quad-0.1.5 (c (n "gauss-quad") (v "0.1.5") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.26") (d #t) (k 0)) (d (n "statrs") (r "^0.14") (d #t) (k 0)))) (h "1i6zgmvc682kgaaflzxaqdw8dsqm4zr6pidbr5kfirvk7hrh60nn")))

(define-public crate-gauss-quad-0.1.7 (c (n "gauss-quad") (v "0.1.7") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "nalgebra") (r "^0.30") (d #t) (k 0)))) (h "010kn9sam0ci1m1b80wid4jmmc1880rbd616xxfjpsk9r1n35d75")))

(define-public crate-gauss-quad-0.1.8 (c (n "gauss-quad") (v "0.1.8") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "nalgebra") (r "^0.30") (d #t) (k 0)))) (h "09krlpc7yb888xbjdg0f4dbpah7mqcjq89hm4c70qs8g97r3402a")))

