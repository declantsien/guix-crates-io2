(define-module (crates-io ga us gaussfilt) #:use-module (crates-io))

(define-public crate-gaussfilt-0.1.0 (c (n "gaussfilt") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "02r62iy3pxxr634wwmrvpkxdp2fdwin2qwr5r3fwn82wdmsbhsqy") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "libm"))))))

(define-public crate-gaussfilt-0.1.1 (c (n "gaussfilt") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "0a4sfh2n7di5f7p1j07xh6vvmc1izcgdky595gz5yqv5vvrfpbdl") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "libm"))))))

(define-public crate-gaussfilt-0.1.2 (c (n "gaussfilt") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "1fx9pv2i7v0adcdsflbq5h4zqfkk5bm4a8njnn68dpr7pjz2rxnl") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "libm"))))))

(define-public crate-gaussfilt-0.1.3 (c (n "gaussfilt") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "1hhvyr3mp0i0j1nlhahhv5rvijjwcqal65i1dmjay6lklia22r8c") (f (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "libm"))))))

