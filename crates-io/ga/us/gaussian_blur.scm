(define-module (crates-io ga us gaussian_blur) #:use-module (crates-io))

(define-public crate-gaussian_blur-0.1.0 (c (n "gaussian_blur") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)))) (h "1dvnb3385lnf1cx5i1db5gkfyiq18kcqa61ly081yhmb4v0fijy1")))

(define-public crate-gaussian_blur-0.1.1 (c (n "gaussian_blur") (v "0.1.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)))) (h "06s66lv3xkmgnahiwhq58xwrijmb6as10gb1h4i4rjxlcbpskv5d")))

