(define-module (crates-io ga us gauss_jordan_elimination) #:use-module (crates-io))

(define-public crate-gauss_jordan_elimination-0.1.0 (c (n "gauss_jordan_elimination") (v "0.1.0") (h "0dwz0k23yvczdm9bbk9n2hyljh1dhak123iadxlji8kadaf587nw")))

(define-public crate-gauss_jordan_elimination-0.2.0 (c (n "gauss_jordan_elimination") (v "0.2.0") (d (list (d (n "num") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "1as04h4imvz4hr47lm0flgwmnkqglnj6zsmb4gwx67c86d32glmi") (f (quote (("default")))) (s 2) (e (quote (("generic_calculation" "dep:num"))))))

