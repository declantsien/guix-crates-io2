(define-module (crates-io ga us gaussiant) #:use-module (crates-io))

(define-public crate-gaussiant-0.1.0 (c (n "gaussiant") (v "0.1.0") (d (list (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1bp578fddv4f3k9izj2h1j1yrrk94ks3ysq9x6qv4yjr5kl8zjvh")))

(define-public crate-gaussiant-0.1.1 (c (n "gaussiant") (v "0.1.1") (d (list (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1vvbi0wqq3pv72an2b8dymnds7hl78f1p70qakfhhnd4l4g1bzdk")))

(define-public crate-gaussiant-0.2.0 (c (n "gaussiant") (v "0.2.0") (d (list (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1xhdw0ll9p5wv7wfwk2fasxgksnp92z2lvaqny09v9dmvgyx8jxd")))

(define-public crate-gaussiant-0.3.0 (c (n "gaussiant") (v "0.3.0") (d (list (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "015rx5lwjz56kwffppx1j101fqbmxszi1kmj9dnazgpdn7fma6fz")))

(define-public crate-gaussiant-0.3.1 (c (n "gaussiant") (v "0.3.1") (d (list (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0691q3h149crch48yivmm6ay1rylpzwcd39xyys3d8dij1b6lwbb")))

(define-public crate-gaussiant-0.4.0 (c (n "gaussiant") (v "0.4.0") (d (list (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "13kcslrv1m3bjknixs7fay3ghz345axv3d1s60ya3lbmib9p2mpd")))

(define-public crate-gaussiant-0.5.0 (c (n "gaussiant") (v "0.5.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "primal") (r "^0.3.0") (d #t) (k 0)))) (h "0n5v3parzyv4f379fcvclkxwzq1n6wrs80zkpbb5ljz7b9h6pldx")))

(define-public crate-gaussiant-0.5.1 (c (n "gaussiant") (v "0.5.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "primal") (r "^0.3.0") (d #t) (k 0)))) (h "1h4yqrsk0f5f6sy3hrlhjwswyc7m17yrfpa6kz81y8fr6260llwy")))

(define-public crate-gaussiant-0.6.0 (c (n "gaussiant") (v "0.6.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "primal") (r "^0.3.0") (d #t) (k 0)))) (h "0g7sc8qizrkvs4v0vqfr8hn46vrcmwclvhqx21clm6prd4g8n4si")))

(define-public crate-gaussiant-0.7.0 (c (n "gaussiant") (v "0.7.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "primal") (r "^0.3.0") (d #t) (k 0)))) (h "1dqc5f7cynk7y2zcndaz9xnb1qfvxwj5bywik536d2c2mwv5l46j")))

(define-public crate-gaussiant-0.8.0 (c (n "gaussiant") (v "0.8.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "primal") (r "^0.3.0") (d #t) (k 0)))) (h "00yz9w3y16qih4j8y044w2s464kaf2bknljqf5n6w5z39l3ijba4")))

