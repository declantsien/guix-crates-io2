(define-module (crates-io ga rd garden-lang-parser) #:use-module (crates-io))

(define-public crate-garden-lang-parser-0.14.0 (c (n "garden-lang-parser") (v "0.14.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "line-numbers") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.1") (d #t) (k 0)))) (h "1k1wx76nxmrhz0jxlfvz0qc7s7h8qdzkq7akdhyrbxxj1azhd35n")))

