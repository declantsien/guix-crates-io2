(define-module (crates-io ga rd garde_derive) #:use-module (crates-io))

(define-public crate-garde_derive-0.1.0 (c (n "garde_derive") (v "0.1.0") (h "1abpjnd5m0akgphxxr60xw9pfn7fmp4cz9qg0f433gvv41y1jbpl")))

(define-public crate-garde_derive-0.3.0 (c (n "garde_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "00sd7562vh42llxwk709451pslq30b0zd3982mggnnzrxgshhymp") (f (quote (("default" "regex"))))))

(define-public crate-garde_derive-0.5.0 (c (n "garde_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "07kb27papl7hk1fnff5jdnf869nl1ljg5c5qsr1dx2bkawp1zygp") (f (quote (("default" "regex"))))))

(define-public crate-garde_derive-0.5.1 (c (n "garde_derive") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1i0kf13zr1rdrj42bk7q5cl43mg8bjspx7ag6fdlcl8wdx34l66s") (f (quote (("default" "regex"))))))

(define-public crate-garde_derive-0.7.0 (c (n "garde_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "11rw93lvmzwl58gmr7cicx80ffpb2nxrv45xpzv2m0djnixjlbqn") (f (quote (("default" "regex"))))))

(define-public crate-garde_derive-0.8.0 (c (n "garde_derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "069qwm6q2xrq0py6q53lhhxx9153gkk8z56l5bacwhlk8d4wkm7h") (f (quote (("default" "regex"))))))

(define-public crate-garde_derive-0.8.1 (c (n "garde_derive") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "06yym51kcy3mfh2bdi6xffqhbr4hbsifwx9ci6vi39gy3srndspx") (f (quote (("default" "regex"))))))

(define-public crate-garde_derive-0.9.0 (c (n "garde_derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "111l4pwvi8kgbflpxzzywbjyqzqbqf8b8qdwg64fxkh34qrr7scl") (f (quote (("default" "regex"))))))

(define-public crate-garde_derive-0.9.1 (c (n "garde_derive") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "00ghq36gr2gw9ww0y18xdkaxbym2gscqmz9xfq132dq50vbpwd3h") (f (quote (("default" "regex"))))))

(define-public crate-garde_derive-0.9.2 (c (n "garde_derive") (v "0.9.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0vfbmq597829bi7lq92l52kf9gm18lp3jagyxgr6rhk6rx826r0z") (f (quote (("default" "regex"))))))

(define-public crate-garde_derive-0.10.0 (c (n "garde_derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "13pijyaamzj9ybq8czmpygpvz2r5sd58l3ps5qipn4zpdhzv6a4l") (f (quote (("default" "regex"))))))

(define-public crate-garde_derive-0.11.0 (c (n "garde_derive") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0inq1sl2q7qgjzxzgywgpxaw02r7v1yalbna8832p47k330006cx") (f (quote (("default" "regex"))))))

(define-public crate-garde_derive-0.11.1 (c (n "garde_derive") (v "0.11.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0fzsdqqdccs22vxka22hdf73k37cfwphcg4l9gf5vqlgwz1j0ll4") (f (quote (("default" "regex"))))))

(define-public crate-garde_derive-0.11.2 (c (n "garde_derive") (v "0.11.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "18n64x73w6iflchawkjcd4dv58096zqzygp230vkmfrmw3ygg2by") (f (quote (("default" "regex"))))))

(define-public crate-garde_derive-0.12.0 (c (n "garde_derive") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "15mk1c52mdarvqf60afx26hiwiwd9vgad2j5ibspryn6hkc1xzzw") (f (quote (("default" "regex"))))))

(define-public crate-garde_derive-0.13.0 (c (n "garde_derive") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (o #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1j2grhy4dlj56wys93gf8g7l5syplykk3llb687n8ck7af83p92w") (f (quote (("default" "regex"))))))

(define-public crate-garde_derive-0.14.0 (c (n "garde_derive") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (o #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "03s2qczrb7l6dl2451g9656pl79kln5h852z9rsf0zbcw1kg97ax") (f (quote (("default" "regex"))))))

(define-public crate-garde_derive-0.14.1 (c (n "garde_derive") (v "0.14.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (o #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1kcsjcik0qja6zswxklp67gvynldx6kwbjxlbakrrkk6najxx6b6") (f (quote (("default" "regex"))))))

(define-public crate-garde_derive-0.15.0 (c (n "garde_derive") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (o #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1ngx3py747g5mzfp91dqyffrjsr7arvggcp17swl1fx2yjh3yrns") (f (quote (("default" "regex"))))))

(define-public crate-garde_derive-0.16.0 (c (n "garde_derive") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (o #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "07gkqzf9hwzancra74iarcmfwymxrpy7x91jfb7hpf2l4kf1gin3") (f (quote (("default" "regex"))))))

(define-public crate-garde_derive-0.16.1 (c (n "garde_derive") (v "0.16.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (o #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "04ll23d9a6g5mprzaqiyj4c40hkx5ryzgcy7ib7lz4fmwdq35biq") (f (quote (("default" "regex"))))))

(define-public crate-garde_derive-0.16.2 (c (n "garde_derive") (v "0.16.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (o #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "19jibhcmvacaljiks0zk3qij5zwvswim3phjcf18dkm9p2285wva") (f (quote (("default" "regex"))))))

(define-public crate-garde_derive-0.16.3 (c (n "garde_derive") (v "0.16.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (o #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1akqa1byj62814x9anzf591b64k2vcn7iypj21pivmixqw3d3bzy") (f (quote (("default" "regex"))))))

(define-public crate-garde_derive-0.17.0 (c (n "garde_derive") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (o #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0kxcjqgqnz2k4pacxb7ddh6agdxd13zq2n6irwk1kpp91fsfyhja") (f (quote (("default" "regex"))))))

(define-public crate-garde_derive-0.18.0 (c (n "garde_derive") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (o #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "derive"))) (d #t) (k 0)))) (h "0nrjk3wiq1vzhkrcfxwjdwi21c8gqagd8axpacaw8c2qa582dxlw") (s 2) (e (quote (("regex" "dep:regex"))))))

