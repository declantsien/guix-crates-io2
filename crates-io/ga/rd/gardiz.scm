(define-module (crates-io ga rd gardiz) #:use-module (crates-io))

(define-public crate-gardiz-0.1.0 (c (n "gardiz") (v "0.1.0") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "priority-queue") (r "^1.1") (d #t) (k 0)))) (h "132439iirg2jz50n7a3qskcw6wx0rb4p4h4k1jv3ywdmqaapm00i")))

(define-public crate-gardiz-0.1.1 (c (n "gardiz") (v "0.1.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "priority-queue") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0nh7fc5i7nn6dj6lhkcm3i06pd66sj7vzxylmnz4xi6lx6aa4f33") (f (quote (("impl-serde" "serde") ("default" "impl-serde"))))))

