(define-module (crates-io ga ea gaea) #:use-module (crates-io))

(define-public crate-gaea-0.3.0 (c (n "gaea") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.58") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "std-logger") (r "^0.3.3") (k 2)))) (h "0gw8s02ns2p8k557h0j0qp4qrqlmhccv6gb3ixmm4ijvdqxh4zh4") (f (quote (("user_space") ("std") ("nightly") ("disable_test_deadline") ("default" "std"))))))

(define-public crate-gaea-0.3.1 (c (n "gaea") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.58") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "std-logger") (r "^0.3.3") (k 2)))) (h "0m86rjpyb3kwf35lbiayf2v1glkw11k6i9xmx0l04qlhvm7j4sdh") (f (quote (("user_space") ("std") ("nightly") ("disable_test_deadline") ("default" "std"))))))

