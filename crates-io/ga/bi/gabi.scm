(define-module (crates-io ga bi gabi) #:use-module (crates-io))

(define-public crate-gabi-0.1.0 (c (n "gabi") (v "0.1.0") (h "0nr68207h7ijkclkngdip131lmzdrhj48w2h174z9hrwyr5kh7qh")))

(define-public crate-gabi-0.1.1 (c (n "gabi") (v "0.1.1") (h "0fs0k1pzvdypmr5ndv6w7aijdan95k3fm4r44v6psd0zqhg3x288")))

(define-public crate-gabi-0.2.0 (c (n "gabi") (v "0.2.0") (h "0szbqqdzwxzq71hmv6yzj6i62x1jr0kxh769858xj1hliwf4xhgf")))

(define-public crate-gabi-0.2.1 (c (n "gabi") (v "0.2.1") (h "10kf051pnc7ir4h6agyw11kwa5h9cvyzfnw7i4hj449lzg56iikn")))

(define-public crate-gabi-0.2.2 (c (n "gabi") (v "0.2.2") (h "0mqy4jxyaylnn7dxmzxpx1idql6f35jgl1xq03qyazfy516c6a86")))

(define-public crate-gabi-0.2.3 (c (n "gabi") (v "0.2.3") (h "0knqc1afd28jwz2i0ms5b4wd1vwx16lys3yaf8fx2k5xm4q91hnc")))

(define-public crate-gabi-0.2.4 (c (n "gabi") (v "0.2.4") (h "1aq64fmiyzwlfh4f749p2f3w5mwi578wwnx11pwjm2yjv5v5m1yz")))

(define-public crate-gabi-0.2.5 (c (n "gabi") (v "0.2.5") (h "19sjc95w9fdm2wzsz4rnzzbdi86rk307ncv0kibrcm3p4djxkx7z")))

(define-public crate-gabi-0.2.6 (c (n "gabi") (v "0.2.6") (h "17aycd9p4vjyjr70hcx9ymamb52dnm0sc5m32682r0bx3kvklx4y")))

