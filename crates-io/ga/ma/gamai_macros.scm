(define-module (crates-io ga ma gamai_macros) #:use-module (crates-io))

(define-public crate-gamai_macros-0.1.31 (c (n "gamai_macros") (v "0.1.31") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0hdb41868470lcxjbd2bfmi5j11zgi8wyvvr1jv4kjvqda4x2pqy")))

(define-public crate-gamai_macros-0.1.32 (c (n "gamai_macros") (v "0.1.32") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rstml") (r "^0.11.2") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "07arr7hgsvdzbh9qwxmy5h9rxwbfc7rb0d85b7ijwca17f39caci")))

(define-public crate-gamai_macros-0.1.33 (c (n "gamai_macros") (v "0.1.33") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rstml") (r "^0.11.2") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0cmxnng3vphx0qp8m2pqwn22fwq5jkxw9mp0ir9s2zi4vvksbj9d")))

(define-public crate-gamai_macros-0.1.34 (c (n "gamai_macros") (v "0.1.34") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rstml") (r "^0.11.2") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1cvwf8f8lzjlyqrv211mgdxfkcpifragcdy634hcmizsjam49bnk")))

(define-public crate-gamai_macros-0.1.35 (c (n "gamai_macros") (v "0.1.35") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rstml") (r "^0.11.2") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1dfhn5yjm2w8jq50wr4m6qjckbkz991jiyzb0swq9k2sl188dx6r")))

(define-public crate-gamai_macros-0.1.36 (c (n "gamai_macros") (v "0.1.36") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rstml") (r "^0.11.2") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1dcw4j3hj23dhgddhv67b73d7r0c4j5i7fr8c0k31nlf0wrf449w")))

(define-public crate-gamai_macros-0.1.37 (c (n "gamai_macros") (v "0.1.37") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rstml") (r "^0.11.2") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "094hlyx4g1nr0pjmg9fd6m1pc0j0ryy3r1kil9459b6bynms9h1r")))

(define-public crate-gamai_macros-0.1.38 (c (n "gamai_macros") (v "0.1.38") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0l2l5k44w5j93bv9w25bcfpnmdfmmnd6yzxzqr972gylj2pgv5yr")))

(define-public crate-gamai_macros-0.1.39 (c (n "gamai_macros") (v "0.1.39") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1r3w1n6yl6amh7sg2sm6s2p7y8dfrffg60lwi18z8xgdxxfnpc10")))

