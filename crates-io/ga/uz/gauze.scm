(define-module (crates-io ga uz gauze) #:use-module (crates-io))

(define-public crate-gauze-0.1.0 (c (n "gauze") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 0)))) (h "1i7dvysw3w80hqpcfbvw427zi27kch1apqb9r48m2b4d2zm6srsb")))

(define-public crate-gauze-0.1.1 (c (n "gauze") (v "0.1.1") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 0)))) (h "1ns0yki2a4lh1xv8xqm0vh4szj22x1izrq4rkb830ipq5gbk1skv")))

(define-public crate-gauze-0.1.2 (c (n "gauze") (v "0.1.2") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 0)))) (h "1wyav523j778fd86k43cr2f91rfx53389z7krgb4z037b2bvmzrl")))

(define-public crate-gauze-0.1.3 (c (n "gauze") (v "0.1.3") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 0)))) (h "1ic53lbhak3hpy31cjbh705x2falqdfs7cw7p0vj3bkcap6g7y2z")))

(define-public crate-gauze-0.1.4 (c (n "gauze") (v "0.1.4") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.52") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (d #t) (k 0)))) (h "18cqawd263g38gdw9w2cy9wj9fww4h3j1qrpqa9c4bmcfzkpj84h")))

