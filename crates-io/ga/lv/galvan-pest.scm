(define-module (crates-io ga lv galvan-pest) #:use-module (crates-io))

(define-public crate-galvan-pest-0.0.0-dev01 (c (n "galvan-pest") (v "0.0.0-dev01") (d (list (d (n "galvan-files") (r "^0.0.0-dev01") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)))) (h "16qzs7nhcv6jrd5jxhljm49r017ysnzybzwf2v64wiivpxc2mkkx") (f (quote (("exec" "galvan-files/exec"))))))

(define-public crate-galvan-pest-0.0.0-dev02 (c (n "galvan-pest") (v "0.0.0-dev02") (d (list (d (n "galvan-files") (r "^0.0.0-dev02") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)))) (h "132k04d6fb04h0b3is4nlh6ysmpknb41v6sf2h64103a7c7fhpn9") (f (quote (("exec" "galvan-files/exec"))))))

(define-public crate-galvan-pest-0.0.0-dev03 (c (n "galvan-pest") (v "0.0.0-dev03") (d (list (d (n "galvan-files") (r "^0.0.0-dev03") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)))) (h "0w92gxghc5258dxsxqd4v207p3dgh03w9xzvb8kjr5qiqcsij4fc") (f (quote (("exec" "galvan-files/exec"))))))

(define-public crate-galvan-pest-0.0.0-dev04 (c (n "galvan-pest") (v "0.0.0-dev04") (d (list (d (n "galvan-files") (r "^0.0.0-dev04") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)))) (h "0k9jm2kfypblyy2a36hhj93hqqh8xm6lwhqdd5pii196pn03gs3v") (f (quote (("exec" "galvan-files/exec"))))))

(define-public crate-galvan-pest-0.0.0-dev05 (c (n "galvan-pest") (v "0.0.0-dev05") (d (list (d (n "galvan-files") (r "^0.0.0-dev05") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)))) (h "1mc3phjq6hzpkzpfa5fpcn26am8xk2pd2a6x3awjlwz2ahj9mqaw") (f (quote (("exec" "galvan-files/exec"))))))

(define-public crate-galvan-pest-0.0.0-dev06 (c (n "galvan-pest") (v "0.0.0-dev06") (d (list (d (n "galvan-files") (r "^0.0.0-dev06") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)))) (h "17101i8ihq1bhnmr5xcp75ymxrwnh2wbbfmb98l0j50lbdlcz9j7") (f (quote (("exec" "galvan-files/exec"))))))

(define-public crate-galvan-pest-0.0.0-dev07 (c (n "galvan-pest") (v "0.0.0-dev07") (d (list (d (n "galvan-files") (r "^0.0.0-dev07") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)))) (h "0rf23qr8n4f9f6xw4hwhv2agpp7z2d54hw3km4dd9xw781n85r22") (f (quote (("exec" "galvan-files/exec"))))))

(define-public crate-galvan-pest-0.0.0-dev08 (c (n "galvan-pest") (v "0.0.0-dev08") (d (list (d (n "galvan-files") (r "^0.0.0-dev08") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)))) (h "0a24s4qkbq16diy0fmmglnvjr4skidz6ggv052j90a9h9n9208g3") (f (quote (("exec" "galvan-files/exec"))))))

(define-public crate-galvan-pest-0.0.0-dev09 (c (n "galvan-pest") (v "0.0.0-dev09") (d (list (d (n "galvan-files") (r "^0.0.0-dev09") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.5") (d #t) (k 0)))) (h "1q0s786wwb2lhgc1aw2b55cqq8xpw5fg0kzmn62qw18bgajmc40r") (f (quote (("exec" "galvan-files/exec"))))))

