(define-module (crates-io ga lv galvan-resolver) #:use-module (crates-io))

(define-public crate-galvan-resolver-0.0.0-dev03 (c (n "galvan-resolver") (v "0.0.0-dev03") (d (list (d (n "galvan-ast") (r "^0.0.0-dev03") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "16773fcsnhpi9psqy366nvn7nqci7pn07wn9phdb6wg8v2jhnz6z")))

(define-public crate-galvan-resolver-0.0.0-dev04 (c (n "galvan-resolver") (v "0.0.0-dev04") (d (list (d (n "galvan-ast") (r "^0.0.0-dev04") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0060wfakwa7kys1ai7p6nxcm49x63ap26v5hw9kdyqcqgxx1xgy9")))

(define-public crate-galvan-resolver-0.0.0-dev05 (c (n "galvan-resolver") (v "0.0.0-dev05") (d (list (d (n "galvan-ast") (r "^0.0.0-dev05") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "093p0r57vxdyrsmaamnwkl1pv5kn5s1977kzkxffdk5j6y4ikv83")))

(define-public crate-galvan-resolver-0.0.0-dev06 (c (n "galvan-resolver") (v "0.0.0-dev06") (d (list (d (n "galvan-ast") (r "^0.0.0-dev06") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "1bwmlwgbw33zl6m76f6fnf834nb2svrg4b2sh47hin4wnahi0glx")))

(define-public crate-galvan-resolver-0.0.0-dev07 (c (n "galvan-resolver") (v "0.0.0-dev07") (d (list (d (n "galvan-ast") (r "^0.0.0-dev07") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "13hnixb6iwl4yyvkz8ap0k9gnwz2iaqy1rlbl65pndag6l4ckfjs")))

(define-public crate-galvan-resolver-0.0.0-dev08 (c (n "galvan-resolver") (v "0.0.0-dev08") (d (list (d (n "galvan-ast") (r "^0.0.0-dev08") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "13hgy8qyhkwibbcdn8s7i04dljc334v55d312vkgxwid6f2qb1m7")))

(define-public crate-galvan-resolver-0.0.0-dev09 (c (n "galvan-resolver") (v "0.0.0-dev09") (d (list (d (n "galvan-ast") (r "^0.0.0-dev09") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)))) (h "0wc72d3gyxnjcg2dgwydhvwg9ncwhc3ksfc0ryfr887h2l11j6is")))

