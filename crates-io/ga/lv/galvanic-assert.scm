(define-module (crates-io ga lv galvanic-assert) #:use-module (crates-io))

(define-public crate-galvanic-assert-0.5.0 (c (n "galvanic-assert") (v "0.5.0") (h "1xj6b6zki19m5ygs00wscwlsr4bk7jn62mhqn487s7m6857q8ghv")))

(define-public crate-galvanic-assert-0.5.1 (c (n "galvanic-assert") (v "0.5.1") (h "1hj7rs4f3py40af5r25v2ary2qsmmr0zqachzbzsb5gdlp9j9p4z")))

(define-public crate-galvanic-assert-0.6.0 (c (n "galvanic-assert") (v "0.6.0") (h "0z98cqsrdfv2kaprylm1whb47m7raxfmn1v9dkz0f9mp3lzrrx5a")))

(define-public crate-galvanic-assert-0.7.0 (c (n "galvanic-assert") (v "0.7.0") (h "0lyjqzzg40bsvr4yjv1mmr4phgw1ga8zafg8ckpsc69qhv68aga8")))

(define-public crate-galvanic-assert-0.7.1 (c (n "galvanic-assert") (v "0.7.1") (h "1rymkih653klqyk55klcgr2m557afmaqy0s0bbmywp28v1dlbzdw")))

(define-public crate-galvanic-assert-0.8.0 (c (n "galvanic-assert") (v "0.8.0") (h "1hvl6cxl9b07kx7i40979id7l9za15rcdrclx4na0fg8sbgci8cw")))

(define-public crate-galvanic-assert-0.8.1 (c (n "galvanic-assert") (v "0.8.1") (h "1w98syki62a6qzgmjaqawcpdyn982vyp9pv5yk30g6bn97n3h5hl")))

(define-public crate-galvanic-assert-0.8.2 (c (n "galvanic-assert") (v "0.8.2") (h "1pmq3gg0km4qbd2c7scw8yj64n316qq9y3sk17nw411smzg5pr1s")))

(define-public crate-galvanic-assert-0.8.3 (c (n "galvanic-assert") (v "0.8.3") (h "0nabxsf5ycxw8dg3s9vxvq0700bvnyzjc0zwpwyiwkl1chzf410r")))

(define-public crate-galvanic-assert-0.8.4 (c (n "galvanic-assert") (v "0.8.4") (h "11ahs4m427v4daiyw01s71nzglckhf3qmsfnfrfxfx4apf4zzlvn") (y #t)))

(define-public crate-galvanic-assert-0.8.5 (c (n "galvanic-assert") (v "0.8.5") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0nnmr1bwv55wadsx3gbyqs46mjr9iqa3ps7ibq9izmzmyp3ca29g")))

(define-public crate-galvanic-assert-0.8.6 (c (n "galvanic-assert") (v "0.8.6") (h "18znddhg626kh0wv8nbp3szymd4rhdvsan488s488fsxi6mif2x9")))

(define-public crate-galvanic-assert-0.8.7 (c (n "galvanic-assert") (v "0.8.7") (h "0h5v0hk7idyzgpb9xqa2ji9zb9l4fjzd1j82hkqv8f4mh9n93yrs")))

