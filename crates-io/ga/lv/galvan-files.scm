(define-module (crates-io ga lv galvan-files) #:use-module (crates-io))

(define-public crate-galvan-files-0.0.0-dev01 (c (n "galvan-files") (v "0.0.0-dev01") (d (list (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (o #t) (d #t) (k 0)))) (h "04i5a5llz8c7pws85qw54chi2hzqgf3jkx553z6kcjij1fdmxfh3") (s 2) (e (quote (("exec" "dep:walkdir"))))))

(define-public crate-galvan-files-0.0.0-dev02 (c (n "galvan-files") (v "0.0.0-dev02") (d (list (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (o #t) (d #t) (k 0)))) (h "0cashawy32yr0wlfmbmvwlbixy7h264178myib58njfqfnrj97km") (s 2) (e (quote (("exec" "dep:walkdir"))))))

(define-public crate-galvan-files-0.0.0-dev03 (c (n "galvan-files") (v "0.0.0-dev03") (d (list (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (o #t) (d #t) (k 0)))) (h "0c1d6cxmcxdmpi3m5yfm0y66ppm65vl7lh9xdydmnb17y0ahj290") (s 2) (e (quote (("exec" "dep:walkdir"))))))

(define-public crate-galvan-files-0.0.0-dev04 (c (n "galvan-files") (v "0.0.0-dev04") (d (list (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (o #t) (d #t) (k 0)))) (h "1jrywx42nvfivihk9bw8jnwrqb3k0w25rh9sm65wl2yji3z7v3dp") (s 2) (e (quote (("exec" "dep:walkdir"))))))

(define-public crate-galvan-files-0.0.0-dev05 (c (n "galvan-files") (v "0.0.0-dev05") (d (list (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (o #t) (d #t) (k 0)))) (h "0fgcqih3549xihfhm949cnvspvkgrryvngkm7j36bswm9gxkvs62") (s 2) (e (quote (("exec" "dep:walkdir"))))))

(define-public crate-galvan-files-0.0.0-dev06 (c (n "galvan-files") (v "0.0.0-dev06") (d (list (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (o #t) (d #t) (k 0)))) (h "0yimba0kmyj3lkivlmqgzyzggqcgwy0pgvsb053hm4nqqg915chv") (s 2) (e (quote (("exec" "dep:walkdir"))))))

(define-public crate-galvan-files-0.0.0-dev07 (c (n "galvan-files") (v "0.0.0-dev07") (d (list (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (o #t) (d #t) (k 0)))) (h "0jsrsaxagxf7ss6d1b14mgl8ik8h1v60f5r3xax7irx6yw3w570v") (s 2) (e (quote (("exec" "dep:walkdir"))))))

(define-public crate-galvan-files-0.0.0-dev08 (c (n "galvan-files") (v "0.0.0-dev08") (d (list (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (o #t) (d #t) (k 0)))) (h "0yj9lkj310rmaxfz4ni37c7rsdyjk65yz3hxlr7ln29jm17xg46x") (s 2) (e (quote (("exec" "dep:walkdir"))))))

(define-public crate-galvan-files-0.0.0-dev09 (c (n "galvan-files") (v "0.0.0-dev09") (d (list (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (o #t) (d #t) (k 0)))) (h "0wvgvzqcwrrhnhajwmmbxjync4kglia110b3cpk36lk6f285mz92") (s 2) (e (quote (("exec" "dep:walkdir"))))))

