(define-module (crates-io ga lv galvanic-test) #:use-module (crates-io))

(define-public crate-galvanic-test-0.1.0 (c (n "galvanic-test") (v "0.1.0") (d (list (d (n "galvanic-mock") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1954gfbdn2jvfsq0980imgrlz24lky03jzbhad2841pvgqyrqxzw") (f (quote (("galvanic_mock_integration" "galvanic-mock"))))))

(define-public crate-galvanic-test-0.1.1 (c (n "galvanic-test") (v "0.1.1") (d (list (d (n "galvanic-mock") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0wj12dr6qc4azx0z7lb1z44qxawrrgy94yp0shmv6hz2z2zz09pk") (f (quote (("galvanic_mock_integration" "galvanic-mock"))))))

(define-public crate-galvanic-test-0.1.2 (c (n "galvanic-test") (v "0.1.2") (d (list (d (n "galvanic-mock") (r "^0.1") (o #t) (d #t) (k 0)))) (h "07l1l17cxxvm9m71s9awhcnjsr8ipjix2c0wqbcbfjzm40fx3ih2") (f (quote (("galvanic_mock_integration" "galvanic-mock"))))))

(define-public crate-galvanic-test-0.1.3 (c (n "galvanic-test") (v "0.1.3") (d (list (d (n "galvanic-mock") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0qmfnlyygl0hkaanb7cll86gbqk2dp1pkjf27q2cs9rwnbc7887d") (f (quote (("galvanic_mock_integration" "galvanic-mock"))))))

(define-public crate-galvanic-test-0.1.4 (c (n "galvanic-test") (v "0.1.4") (d (list (d (n "galvanic-mock") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1hz4zza7msz69bzbrn3ryf3c2z6jcmi9xygs78pm86h2cw9n4vcj") (f (quote (("galvanic_mock_integration" "galvanic-mock"))))))

(define-public crate-galvanic-test-0.1.5 (c (n "galvanic-test") (v "0.1.5") (d (list (d (n "galvanic-mock") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0zbxjwk3brcwnzzqfvhab6zqzlgbh7jgqxw1n7m4m6w0p8la5s2b") (f (quote (("galvanic_mock_integration" "galvanic-mock"))))))

(define-public crate-galvanic-test-0.2.0 (c (n "galvanic-test") (v "0.2.0") (d (list (d (n "galvanic-mock") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0ylh0mhdnp8g4q9y5m6dqvlhbqws77d3cw23088wv39q5wxwjvkf") (f (quote (("galvanic_mock_integration" "galvanic-mock"))))))

