(define-module (crates-io ga lv galvan-test-macro) #:use-module (crates-io))

(define-public crate-galvan-test-macro-0.0.0-dev01 (c (n "galvan-test-macro") (v "0.0.0-dev01") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bwlxcn0k88hgphd9j5fr1p5fib9iv7p0xwn89ahd6ixy6zp2akm")))

(define-public crate-galvan-test-macro-0.0.0-dev02 (c (n "galvan-test-macro") (v "0.0.0-dev02") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0x8y0mlja5y1szx278yij67g4rm6cw9h9gjzw0igjd7bp1kczw7n")))

(define-public crate-galvan-test-macro-0.0.0-dev03 (c (n "galvan-test-macro") (v "0.0.0-dev03") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1d49hrpm5k7af2j8fb96a74yhqjgnh68rzggc1x1liinmv1yb41v")))

(define-public crate-galvan-test-macro-0.0.0-dev04 (c (n "galvan-test-macro") (v "0.0.0-dev04") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0nf5lyvp6qvvf9akkwk7diczwv6b69i1j5vhzsw4rhfwbbgpmrbf")))

(define-public crate-galvan-test-macro-0.0.0-dev05 (c (n "galvan-test-macro") (v "0.0.0-dev05") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0cxagb95kxm732kgzfnsw3kb4v54lka1x3wrhwvjr4liqpkj3hsp")))

(define-public crate-galvan-test-macro-0.0.0-dev06 (c (n "galvan-test-macro") (v "0.0.0-dev06") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1lwj1a962jg16adyj554m7vrn8sf2kyyripajzaqgwmsgw313xrc")))

(define-public crate-galvan-test-macro-0.0.0-dev07 (c (n "galvan-test-macro") (v "0.0.0-dev07") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "05zhb9kqb2sjh3zfixgf6rji1684si4xz419z7z1w1016sjzkc15")))

(define-public crate-galvan-test-macro-0.0.0-dev08 (c (n "galvan-test-macro") (v "0.0.0-dev08") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0w8cc15b4x1n66ajywps1l2dvz69r48b6ax0b8wj1axf2bzzig92")))

(define-public crate-galvan-test-macro-0.0.0-dev09 (c (n "galvan-test-macro") (v "0.0.0-dev09") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1hkaks1zmw5wsckq372fpvy63pp7q8hzn82qj1iwir4k2xicx8hg")))

