(define-module (crates-io ga lv galvan) #:use-module (crates-io))

(define-public crate-galvan-0.0.0-dev01 (c (n "galvan") (v "0.0.0-dev01") (d (list (d (n "galvan-transpiler") (r "^0.0.0-dev01") (d #t) (k 0)))) (h "1g8bjn5cn2smf6idhnh04d2l40viha31afngwsnxvy8c3yzz0h1f") (f (quote (("build" "galvan-transpiler/exec"))))))

(define-public crate-galvan-0.0.0-dev02 (c (n "galvan") (v "0.0.0-dev02") (d (list (d (n "galvan-transpiler") (r "^0.0.0-dev02") (d #t) (k 0)))) (h "0b8d2959yyvmzcbyl6pml8ccc0l8pffqkm9h0an5gw5xrvhybs6z") (f (quote (("build" "galvan-transpiler/exec"))))))

(define-public crate-galvan-0.0.0-dev03 (c (n "galvan") (v "0.0.0-dev03") (d (list (d (n "galvan-transpiler") (r "^0.0.0-dev03") (d #t) (k 0)))) (h "10s0j9aigixks4569al526ns2pwmgqcdlsqyaqrrpw4cww7bbjhn") (f (quote (("build" "galvan-transpiler/exec"))))))

(define-public crate-galvan-0.0.0-dev04 (c (n "galvan") (v "0.0.0-dev04") (d (list (d (n "galvan-transpiler") (r "^0.0.0-dev04") (d #t) (k 0)))) (h "1p15mwdqqlbpnjiyad85caj4vsrqfn73ivbwv4mvilvah1lqmc9s") (f (quote (("build" "galvan-transpiler/exec"))))))

(define-public crate-galvan-0.0.0-dev05 (c (n "galvan") (v "0.0.0-dev05") (d (list (d (n "galvan-transpiler") (r "^0.0.0-dev05") (d #t) (k 0)))) (h "0mhirdnwl2d47h42q4wngmab51dicy6825bjgyqm65jimmwi0wmn") (f (quote (("build" "galvan-transpiler/exec"))))))

(define-public crate-galvan-0.0.0-dev06 (c (n "galvan") (v "0.0.0-dev06") (d (list (d (n "galvan-transpiler") (r "^0.0.0-dev06") (d #t) (k 0)))) (h "1hp64m7q9gg3wwzl7kskbqhhbghiwspn5g5pgxqq4bs391wjkzjv") (f (quote (("build" "galvan-transpiler/exec"))))))

(define-public crate-galvan-0.0.0-dev07 (c (n "galvan") (v "0.0.0-dev07") (d (list (d (n "galvan-transpiler") (r "^0.0.0-dev07") (d #t) (k 0)))) (h "1l83igin4xr7rggzvvkkynlqii54z5caj6djzzmgi7zgwnc60ysh") (f (quote (("build" "galvan-transpiler/exec"))))))

(define-public crate-galvan-0.0.0-dev08 (c (n "galvan") (v "0.0.0-dev08") (d (list (d (n "galvan-transpiler") (r "^0.0.0-dev08") (d #t) (k 0)))) (h "1bm1kjdds3q9d5dw94mlks430bpbwi6hadfdwbsb5jak1nyhsjb0") (f (quote (("build" "galvan-transpiler/exec"))))))

(define-public crate-galvan-0.0.0-dev09 (c (n "galvan") (v "0.0.0-dev09") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "galvan-transpiler") (r "^0.0.0-dev09") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)))) (h "09j5wdsjy0rvs0jzpizi9nlbv3ngy33l51l4m8pfprbzhxz4ai84") (f (quote (("build" "galvan-transpiler/exec"))))))

