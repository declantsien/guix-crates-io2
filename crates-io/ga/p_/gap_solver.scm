(define-module (crates-io ga p_ gap_solver) #:use-module (crates-io))

(define-public crate-gap_solver-0.1.0 (c (n "gap_solver") (v "0.1.0") (h "0g0xzkirvi2hswv9vv0cvc0hsw1filn7ymsp7xbc45mdb1dwdyx1")))

(define-public crate-gap_solver-0.2.0 (c (n "gap_solver") (v "0.2.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1pb4g4np7wrb4r8vb0iaiq3ijplx7v07a3nwl00hay182d2p0pp2")))

