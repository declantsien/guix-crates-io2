(define-module (crates-io ga p_ gap_query_interval_tree) #:use-module (crates-io))

(define-public crate-gap_query_interval_tree-0.0.1 (c (n "gap_query_interval_tree") (v "0.0.1") (d (list (d (n "discrete_range_map") (r "^0.4.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)))) (h "09g138sk7i4di7r8vwcqba8yh8362hn8b421hg1gsnspsz5jgdnq")))

(define-public crate-gap_query_interval_tree-0.1.0 (c (n "gap_query_interval_tree") (v "0.1.0") (d (list (d (n "discrete_range_map") (r "^0.5.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)))) (h "03gqhfnyhcqv5j6zhc1ah2431jp2wwvhin2fd19frxfnkhsxcsnn")))

(define-public crate-gap_query_interval_tree-0.2.0 (c (n "gap_query_interval_tree") (v "0.2.0") (d (list (d (n "discrete_range_map") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.12.0") (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("alloc"))) (k 0)))) (h "0xcwx92sdbk1pqs0q51qpamnwnki0sbmfn2d5zcx7hw7chridyds")))

