(define-module (crates-io ga t- gat-lending-iterator) #:use-module (crates-io))

(define-public crate-gat-lending-iterator-0.1.0 (c (n "gat-lending-iterator") (v "0.1.0") (h "1s0g5nn2dhcx9gg7x2aa5f47dqf2cg35cprbwnnajp9wdsivbwls")))

(define-public crate-gat-lending-iterator-0.1.1 (c (n "gat-lending-iterator") (v "0.1.1") (h "1177np61x3w8k1p085zi2m9ckz3y4s1gy2nzmd2b6ckn5lyis7q4") (r "1.65")))

(define-public crate-gat-lending-iterator-0.1.2 (c (n "gat-lending-iterator") (v "0.1.2") (h "0mnw2icb6d7ial7yxwk1krjx9bdhwk77x3lm69pp6xsiyim26s44") (r "1.65")))

(define-public crate-gat-lending-iterator-0.1.3 (c (n "gat-lending-iterator") (v "0.1.3") (h "1f9d5fkvwcdwdvkfqqh84c2n8kkfsrjjdsnw79zp5yxsgh6037c4") (r "1.65")))

(define-public crate-gat-lending-iterator-0.1.4 (c (n "gat-lending-iterator") (v "0.1.4") (h "1rj7gl36a2wzj1bd3jx5nw3jz5wq7zr6vvf60kvryf5s3c01mhqw") (r "1.65")))

(define-public crate-gat-lending-iterator-0.1.5 (c (n "gat-lending-iterator") (v "0.1.5") (h "1agx13403m08gk13sv8qhy9m4na97bm3lgpa1m0bdmdayawpncj2") (r "1.65")))

