(define-module (crates-io ga t- gat-std) #:use-module (crates-io))

(define-public crate-gat-std-0.1.0 (c (n "gat-std") (v "0.1.0") (d (list (d (n "gat-std-proc") (r "^0.1") (d #t) (k 0)))) (h "0py6jyzpklal1xdxqziz7sfjkk6wdaa9adhyjlpmb0482n34xkfr") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-gat-std-0.1.1 (c (n "gat-std") (v "0.1.1") (d (list (d (n "gat-std-proc") (r "^0.1") (d #t) (k 0)))) (h "14v4ldnzi8y8zkcj2qq7rj4af5ygk0s9iklflssxpcdgqzsfp3p0") (f (quote (("default" "alloc") ("alloc"))))))

