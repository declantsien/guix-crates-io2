(define-module (crates-io ga al gaal) #:use-module (crates-io))

(define-public crate-gaal-0.1.3 (c (n "gaal") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 0)) (d (n "synnax") (r "^0.1.5") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (f (quote ("crossterm"))) (d #t) (k 0)))) (h "0l7a4rijjwqsbi0xql1ia32xmk1i2kjwhhgjyzc0z9pk8x07jz6n")))

(define-public crate-gaal-0.1.5 (c (n "gaal") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 0)) (d (n "synnax") (r "^0.1.7") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (f (quote ("crossterm"))) (d #t) (k 0)))) (h "1dglngcwy7i9kcjhw52d4yinkkilvsn31vm51f2ignqzvzw11cd5")))

