(define-module (crates-io ga rm garminfit) #:use-module (crates-io))

(define-public crate-garminfit-0.1.0 (c (n "garminfit") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)))) (h "1x930pkzvr9j6i1i9rbj1q4frvz3m8332v1ajpdskaiw5z3gdsl4")))

