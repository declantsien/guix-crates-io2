(define-module (crates-io ga rn garnet) #:use-module (crates-io))

(define-public crate-garnet-0.0.0 (c (n "garnet") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.31") (d #t) (k 1)) (d (n "clap") (r "^2.27.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "fluent") (r "= 0.12.0") (d #t) (k 0)) (d (n "fluent-langneg") (r "= 0.13.0") (d #t) (k 0)) (d (n "unic-langid") (r "= 0.9.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("stringapiset" "winnls"))) (d #t) (k 0)))) (h "0hn6biifcwzpc01qynvizdyc448pkrdh0djwzzg0dnid52yvnpi3") (f (quote (("system_install"))))))

