(define-module (crates-io ga rn garnish_lang_traits) #:use-module (crates-io))

(define-public crate-garnish_lang_traits-0.0.2-alpha (c (n "garnish_lang_traits") (v "0.0.2-alpha") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0nc0433x9ngs34y1q6xgvi9x5w7n9491v5pjaavz0wjbmx5d25mr") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-garnish_lang_traits-0.0.3-alpha (c (n "garnish_lang_traits") (v "0.0.3-alpha") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0b6md8jk616s67jcv4fz9656dbrvs853iv5pp9ch2dmdybl6r8fj") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-garnish_lang_traits-0.0.4-alpha (c (n "garnish_lang_traits") (v "0.0.4-alpha") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "06zhb5sv3ha8xhi9sqlwpn18hv7rrafbf8aswqj8dbri1sba5pm7") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-garnish_lang_traits-0.0.5-alpha (c (n "garnish_lang_traits") (v "0.0.5-alpha") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0fj7zi3bfvyrh2rm24d5f4iipxyrn5fz5b6ibinn260p4q9m8s44") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-garnish_lang_traits-0.0.6-alpha (c (n "garnish_lang_traits") (v "0.0.6-alpha") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0kirxxsk7512ad94v503dx2kgk8kf560r5n3ajf2x23mryf9sscx") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-garnish_lang_traits-0.0.7-alpha (c (n "garnish_lang_traits") (v "0.0.7-alpha") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "045wk2vzy1aiwfxwaxl9dwily191jyzwadaflrp2xhv4nblhscxw") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-garnish_lang_traits-0.0.8-alpha (c (n "garnish_lang_traits") (v "0.0.8-alpha") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1zyw8yd6x4vp02iph4smpx4jx612mx5l6zb3qvakjh01rwpmixb8") (s 2) (e (quote (("serde" "dep:serde"))))))

