(define-module (crates-io ga rn garnish_lang_utilities) #:use-module (crates-io))

(define-public crate-garnish_lang_utilities-0.1.0 (c (n "garnish_lang_utilities") (v "0.1.0") (d (list (d (n "garnish_lang_compiler") (r "^0.0.2-alpha") (d #t) (k 0)) (d (n "garnish_lang_traits") (r "^0.0.2-alpha") (d #t) (k 0)))) (h "1b84iqz4mf62ranyvpy163wgnqkcf33xyk8885506n94xa6xrnpd")))

(define-public crate-garnish_lang_utilities-0.3.0 (c (n "garnish_lang_utilities") (v "0.3.0") (d (list (d (n "garnish_lang_compiler") (r "^0.0.4-alpha") (d #t) (k 0)) (d (n "garnish_lang_simple_data") (r "^0.0.4-alpha") (d #t) (k 2)) (d (n "garnish_lang_traits") (r "^0.0.4-alpha") (d #t) (k 0)))) (h "0b96m9whfgl9d77fnp87x4w5kk6zxxr39fs17sipalb4z9jkj9fv")))

(define-public crate-garnish_lang_utilities-0.4.0 (c (n "garnish_lang_utilities") (v "0.4.0") (d (list (d (n "garnish_lang_compiler") (r "^0.0.5-alpha") (d #t) (k 0)) (d (n "garnish_lang_simple_data") (r "^0.0.5-alpha") (d #t) (k 2)) (d (n "garnish_lang_traits") (r "^0.0.5-alpha") (d #t) (k 0)))) (h "01wzspd3mk9ml3jk7clzrd861cn3c0cg8nb73dqd2299f70q94fw")))

(define-public crate-garnish_lang_utilities-0.5.0 (c (n "garnish_lang_utilities") (v "0.5.0") (d (list (d (n "garnish_lang_compiler") (r "^0.0.6-alpha") (d #t) (k 0)) (d (n "garnish_lang_simple_data") (r "^0.0.6-alpha") (d #t) (k 2)) (d (n "garnish_lang_traits") (r "^0.0.6-alpha") (d #t) (k 0)))) (h "1ad061mbgqsjp400lr3fxbi0w13zbg2i7zljnraj6a6ijy7l2mha")))

