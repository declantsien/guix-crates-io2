(define-module (crates-io ga rn garnish_lang_runtime) #:use-module (crates-io))

(define-public crate-garnish_lang_runtime-0.0.1-alpha (c (n "garnish_lang_runtime") (v "0.0.1-alpha") (d (list (d (n "garnish_lang_common") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "garnish_lang_instruction_set_builder") (r "^0.0.1-alpha") (d #t) (k 2)))) (h "141p1s2pxai6dyijzgs4yh2g61b2n7yglxm33pm5nb1ixwyxy7a8")))

(define-public crate-garnish_lang_runtime-0.0.2-alpha (c (n "garnish_lang_runtime") (v "0.0.2-alpha") (d (list (d (n "garnish_lang_traits") (r "^0.0.2-alpha") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0agk1mlwv3kxjny42b4v9wq4zz1cpv0py6xs89d5a5s4pmfx3ybk") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-garnish_lang_runtime-0.0.3-alpha (c (n "garnish_lang_runtime") (v "0.0.3-alpha") (d (list (d (n "garnish_lang_traits") (r "^0.0.3-alpha") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1h14a7yk2mza4ffka9bnaj4if9l44p0fjdrqdq080lv3vf4bs9g1") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-garnish_lang_runtime-0.0.4-alpha (c (n "garnish_lang_runtime") (v "0.0.4-alpha") (d (list (d (n "garnish_lang_traits") (r "^0.0.4-alpha") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "12sczb25jl0xybaa1q9xhwlpcd6jxjb6iwmpjxpp17dfgkr3dgzr") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-garnish_lang_runtime-0.0.5-alpha (c (n "garnish_lang_runtime") (v "0.0.5-alpha") (d (list (d (n "garnish_lang_traits") (r "^0.0.5-alpha") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0dzb893f6k5gr1pckkivyvalnsx2zpgjl6zhvpawq11xw1z2rkdx") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-garnish_lang_runtime-0.0.6-alpha (c (n "garnish_lang_runtime") (v "0.0.6-alpha") (d (list (d (n "garnish_lang_traits") (r "^0.0.6-alpha") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0h0wanfk7slzx2nz3qa9wv841476niqvpzhc419359gasgbx2zym") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-garnish_lang_runtime-0.0.7-alpha (c (n "garnish_lang_runtime") (v "0.0.7-alpha") (d (list (d (n "garnish_lang_traits") (r "^0.0.7-alpha") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1yn593qr33jj9mi4m8y2jjx8mray775rhsdmrs652ddhlb9zgpsb") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-garnish_lang_runtime-0.0.8-alpha (c (n "garnish_lang_runtime") (v "0.0.8-alpha") (d (list (d (n "garnish_lang_traits") (r "^0.0.8-alpha") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1alihsqv1mrjk2gxr1kzlp8hagcn5ha7a32wm8f1micp33ifqyl0") (s 2) (e (quote (("serde" "dep:serde"))))))

