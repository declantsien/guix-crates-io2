(define-module (crates-io ga rn garnish_lang_common) #:use-module (crates-io))

(define-public crate-garnish_lang_common-0.0.1-alpha (c (n "garnish_lang_common") (v "0.0.1-alpha") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.3.0") (d #t) (k 0)))) (h "0bcj4xwg0x9cdca9xgfn5d7dcgpvws8rsrfqvhln0c896wz14yb1")))

