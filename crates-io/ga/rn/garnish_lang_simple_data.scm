(define-module (crates-io ga rn garnish_lang_simple_data) #:use-module (crates-io))

(define-public crate-garnish_lang_simple_data-0.0.2-alpha (c (n "garnish_lang_simple_data") (v "0.0.2-alpha") (d (list (d (n "garnish_lang_traits") (r "^0.0.2-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0vzdq9zsxq0d37l1bfnms1qk5s10zw8bjabgfdz9qx85pq1229s6") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-garnish_lang_simple_data-0.0.3-alpha (c (n "garnish_lang_simple_data") (v "0.0.3-alpha") (d (list (d (n "garnish_lang_traits") (r "^0.0.3-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ia8in13k1c8ww08hswy17k76imgx2id2ag8swvwf4h7si3j14ab") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-garnish_lang_simple_data-0.0.4-alpha (c (n "garnish_lang_simple_data") (v "0.0.4-alpha") (d (list (d (n "garnish_lang_traits") (r "^0.0.4-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0bi9gnyiry0g9iaggyiqz120qh3892ajpgayf8ipk07rfrmwmn7i") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-garnish_lang_simple_data-0.0.5-alpha (c (n "garnish_lang_simple_data") (v "0.0.5-alpha") (d (list (d (n "garnish_lang_traits") (r "^0.0.5-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "05rsj2z7r4gg9dqihz2qsyvk4x5fhj05vx0lk97wrqprdvfqy95v") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-garnish_lang_simple_data-0.0.6-alpha (c (n "garnish_lang_simple_data") (v "0.0.6-alpha") (d (list (d (n "garnish_lang_traits") (r "^0.0.6-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1n4sc68l7di50b95hf6hdslvy9ah4wxh2wz2c7dkpj412dla6ys7") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-garnish_lang_simple_data-0.0.7-alpha (c (n "garnish_lang_simple_data") (v "0.0.7-alpha") (d (list (d (n "garnish_lang_traits") (r "^0.0.7-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1cggvf6ylrfhnfhv23aqw9qph0fw6rj56q7ccsjw6g147gwkbgv5") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-garnish_lang_simple_data-0.0.8-alpha (c (n "garnish_lang_simple_data") (v "0.0.8-alpha") (d (list (d (n "garnish_lang_traits") (r "^0.0.8-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1lcs0wjqp4snvblmc112bylv3mvi1nhzcp6n6i9wwqgahsbchndb") (s 2) (e (quote (("serde" "dep:serde"))))))

