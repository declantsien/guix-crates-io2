(define-module (crates-io ga rn garnish_lang_annotations_collector) #:use-module (crates-io))

(define-public crate-garnish_lang_annotations_collector-0.2.0 (c (n "garnish_lang_annotations_collector") (v "0.2.0") (d (list (d (n "garnish_lang_compiler") (r "^0.0.2-alpha") (d #t) (k 0)) (d (n "garnish_lang_traits") (r "^0.0.2-alpha") (d #t) (k 0)))) (h "0dsb2g2bi71lp3c3gsmccmksaiqj83qlvxgrc5ybj6sdz8bqw68b")))

(define-public crate-garnish_lang_annotations_collector-0.3.0 (c (n "garnish_lang_annotations_collector") (v "0.3.0") (d (list (d (n "garnish_lang_compiler") (r "^0.0.4-alpha") (d #t) (k 0)) (d (n "garnish_lang_traits") (r "^0.0.4-alpha") (d #t) (k 0)))) (h "10jp4817d0mgj4nazg3j43fxlmykv10nwfv98pxml0h81s4glpny")))

(define-public crate-garnish_lang_annotations_collector-0.4.0 (c (n "garnish_lang_annotations_collector") (v "0.4.0") (d (list (d (n "garnish_lang_compiler") (r "^0.0.5-alpha") (d #t) (k 0)) (d (n "garnish_lang_traits") (r "^0.0.5-alpha") (d #t) (k 0)))) (h "1s1av93d3q9hb5r40azswll1wk10p5x4yvqc044g07asn6svx5w1")))

(define-public crate-garnish_lang_annotations_collector-0.5.0 (c (n "garnish_lang_annotations_collector") (v "0.5.0") (d (list (d (n "garnish_lang_compiler") (r "^0.0.6-alpha") (d #t) (k 0)) (d (n "garnish_lang_traits") (r "^0.0.6-alpha") (d #t) (k 0)))) (h "056jbki4m2dp4v3yjblyk8ndh9zv8cfxk075hyl8lz3z9s4f2abj")))

