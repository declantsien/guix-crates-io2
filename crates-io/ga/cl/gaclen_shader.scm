(define-module (crates-io ga cl gaclen_shader) #:use-module (crates-io))

(define-public crate-gaclen_shader-0.0.6 (c (n "gaclen_shader") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wsk3pd9dnch6air9jiix36nkxw9bcclqm8w5mivwz6bbvyl9dvv")))

(define-public crate-gaclen_shader-0.0.7 (c (n "gaclen_shader") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1h3jvb4p03h3ci0zcjwj1qrn6zy0ry6r4d2a650r5kmqn6c04z94")))

(define-public crate-gaclen_shader-0.0.8 (c (n "gaclen_shader") (v "0.0.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16wy7p33php88my97zv2jvmjki1nvh7namn164swrc3hp9yv14cx")))

(define-public crate-gaclen_shader-0.0.9 (c (n "gaclen_shader") (v "0.0.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vpccsvqh76l23x4azzl42rx4hadpnjv7wq6nm6akjwrqxpdznsp")))

(define-public crate-gaclen_shader-0.0.10 (c (n "gaclen_shader") (v "0.0.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "15s58qgk7gngispjyc5ipqwxprm579kq5siafhr26i63aaza1bjg")))

(define-public crate-gaclen_shader-0.0.11 (c (n "gaclen_shader") (v "0.0.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13b0j3kdj9j2wjzacjb93sjsfr4p2h4afsq4p4ny7b1ppy8427if")))

(define-public crate-gaclen_shader-0.0.12 (c (n "gaclen_shader") (v "0.0.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0k4ll8hd43c7z97b19b2jw9z5f0fk5xqjp5msk4p0cmgc1a6ay8d") (y #t)))

(define-public crate-gaclen_shader-0.0.13 (c (n "gaclen_shader") (v "0.0.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0fq8d60vx1jhhks32apq9p347pfgxfgarlaqky51xlv0anjvc79w")))

(define-public crate-gaclen_shader-0.0.14 (c (n "gaclen_shader") (v "0.0.14") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "shaderc") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08yhnk0rkjm4hpnfcx8x5dddc3hjlwx2vi8h6p3zww7wh2yaqkdg")))

