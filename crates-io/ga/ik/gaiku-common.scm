(define-module (crates-io ga ik gaiku-common) #:use-module (crates-io))

(define-public crate-gaiku-common-0.1.0 (c (n "gaiku-common") (v "0.1.0") (d (list (d (n "decorum") (r "^0.1.3") (d #t) (k 0)) (d (n "derive-new") (r "^0.5.6") (d #t) (k 0)) (d (n "derive_builder") (r "^0.7.1") (d #t) (k 0)) (d (n "getset") (r "^0.0.7") (d #t) (k 0)) (d (n "nalgebra") (r "^0.17.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "17vdlk4jkcrz421nsqvjnfbd2n181z8ibgw97r57xbi6zbx6xwav")))

