(define-module (crates-io ga ik gaiku-3d) #:use-module (crates-io))

(define-public crate-gaiku-3d-0.1.0 (c (n "gaiku-3d") (v "0.1.0") (d (list (d (n "gaiku-common") (r "^0.1.0") (d #t) (k 0)) (d (n "gox") (r "^0.2.0") (d #t) (k 0)) (d (n "obj-exporter") (r "^0.2.0") (d #t) (k 2)) (d (n "png") (r "^0.14.0") (d #t) (k 0)))) (h "1bksvdwcj1i0pk04ffdwqrig9zv51dnll2k7vfk1zv545addxcq8")))

