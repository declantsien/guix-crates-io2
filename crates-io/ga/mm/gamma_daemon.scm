(define-module (crates-io ga mm gamma_daemon) #:use-module (crates-io))

(define-public crate-gamma_daemon-0.1.0 (c (n "gamma_daemon") (v "0.1.0") (d (list (d (n "battery") (r "^0.7.8") (d #t) (k 0)) (d (n "bulbb") (r "^0.0.3") (d #t) (k 0)) (d (n "daemonize") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "0q0qwc8x4br1p7i9vizcha1ksq43vkklh6bdy84l2f2iihm7nkrp")))

(define-public crate-gamma_daemon-0.2.0 (c (n "gamma_daemon") (v "0.2.0") (d (list (d (n "battery") (r "^0.7.8") (d #t) (k 0)) (d (n "bulbb") (r "^0.0.3") (d #t) (k 0)) (d (n "daemonize") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1y5y7nnm5hyi1hnbl9mv33nj8h05vsr3gq70d3qf4y5aaz7xf5w8")))

(define-public crate-gamma_daemon-0.2.1 (c (n "gamma_daemon") (v "0.2.1") (d (list (d (n "battery") (r "^0.7.8") (d #t) (k 0)) (d (n "bulbb") (r "^0.0.3") (d #t) (k 0)) (d (n "daemonize") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0wikkxbk870hfcq5b6hfj4cqjcarnmr73rxanh28zwkd4l10gx2r")))

