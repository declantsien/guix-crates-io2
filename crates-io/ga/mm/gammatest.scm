(define-module (crates-io ga mm gammatest) #:use-module (crates-io))

(define-public crate-gammatest-0.1.0 (c (n "gammatest") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0l3q71d5m6ay2pxk86ckdyjdz2hpl2f2qqfqdbfwp8myqg7v0pjw")))

(define-public crate-gammatest-0.1.1 (c (n "gammatest") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1cnxwcpa20hfb9q6as2p83s6v0831naz0wp3sjx5gm5d65rxgfzq")))

(define-public crate-gammatest-0.1.2 (c (n "gammatest") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1qmn4bgxnr2abk5p5pyp81xpjsga56cjp0knkpihz20q1l5crjww")))

(define-public crate-gammatest-0.1.21 (c (n "gammatest") (v "0.1.21") (d (list (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0f24ggx1nayzhb0jkzqdm8k0h8xbysc5q38bch7w8m61ijhvvg88")))

