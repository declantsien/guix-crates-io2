(define-module (crates-io ga mm gamma) #:use-module (crates-io))

(define-public crate-gamma-0.1.1 (c (n "gamma") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.3.1") (d #t) (k 0)))) (h "04zblcqfcrh4zb7vdllfmcz3v8npirhscr2fha3ab8qapwfapbxb")))

(define-public crate-gamma-0.2.0 (c (n "gamma") (v "0.2.0") (h "1wdvrqblrgv9vcvz7vjzhv9klz14dj94zf1s7fz44zwljnr9zz92")))

(define-public crate-gamma-0.3.0 (c (n "gamma") (v "0.3.0") (h "1lyvsg084ms2niicd333h3f4g7liabaddq2xdhqrgbq411fdvhsm")))

(define-public crate-gamma-0.4.0 (c (n "gamma") (v "0.4.0") (h "1qqhc44i19nrbg9hcdvbw3z3sxgywryav3mm0ifw0pb57x7mhjar")))

(define-public crate-gamma-0.5.0 (c (n "gamma") (v "0.5.0") (d (list (d (n "indexmap") (r "^1.4.0") (d #t) (k 0)))) (h "1a2rkx809advzxf5a7iay96dwhcm7naddwaywlkd0x27hmaxdkli")))

(define-public crate-gamma-0.6.0 (c (n "gamma") (v "0.6.0") (h "0cpsf4zzhwnsv2p1nwsqhjkqv54zc18abk0yvv67nx5qcfxnk6kd")))

(define-public crate-gamma-0.6.1 (c (n "gamma") (v "0.6.1") (h "0815cgdrzxab7cpm4hqvpiqxglvsyb0s8ghv1s1cr5hnbfyi9h8y")))

(define-public crate-gamma-0.7.0 (c (n "gamma") (v "0.7.0") (h "0jh224z4qv9ayr3rl072bzxd212jn8l6vpfrfzz0jn0b8vciciv9")))

(define-public crate-gamma-0.8.0 (c (n "gamma") (v "0.8.0") (h "0wsbn7wh0abgh5pvjcinjvx81p0q4h70ihj09gvsbksma411a2rw")))

(define-public crate-gamma-0.8.1 (c (n "gamma") (v "0.8.1") (h "1p0k7ylfpnrg51j6migys528pjhwmsbhsm2nxxmhhqb7dgbs72jr")))

(define-public crate-gamma-0.9.0 (c (n "gamma") (v "0.9.0") (h "1lfvr7kwp1dsdwf3vbv2w1q9cj8vlgpf5zc8x0x7z7kv9ic7pwmq")))

