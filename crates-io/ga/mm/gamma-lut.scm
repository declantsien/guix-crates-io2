(define-module (crates-io ga mm gamma-lut) #:use-module (crates-io))

(define-public crate-gamma-lut-0.1.0 (c (n "gamma-lut") (v "0.1.0") (h "1yq9lg8qx3a62xnl8m4b2v7m4y9i6m6bbndim6rlspx9y8q6v007")))

(define-public crate-gamma-lut-0.1.1 (c (n "gamma-lut") (v "0.1.1") (h "1jxpikj1lbbf20jk0q2zc8i3qvdm9g0wyjzqq09gvpk2m5bvc5js")))

(define-public crate-gamma-lut-0.1.2 (c (n "gamma-lut") (v "0.1.2") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "0166drzs1jg5g5bx0msib242vrv9q466rv958rz9nbaq3fgb61gk")))

(define-public crate-gamma-lut-0.1.3 (c (n "gamma-lut") (v "0.1.3") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "09vx01k39290qkg80q3z74rv5s5khq44wf0z5isw0f0r2r2drg8k")))

(define-public crate-gamma-lut-0.1.4 (c (n "gamma-lut") (v "0.1.4") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "1kci9vcmy88w7waganp9pp5pwbrqppx85ymbhpzzm9r47l1bk9jc")))

(define-public crate-gamma-lut-0.1.5 (c (n "gamma-lut") (v "0.1.5") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "1wcsn9l0581p1pzcmphl7cv870x93bi8wcycxb53086lrsyaffy8")))

(define-public crate-gamma-lut-0.1.6 (c (n "gamma-lut") (v "0.1.6") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "0m9487kf4vpyb37m6p98skkwv4bps6ymrcnbjmc16xh7jggji1yd")))

(define-public crate-gamma-lut-0.2.0 (c (n "gamma-lut") (v "0.2.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "15xjfxqydfv7lrzyrs8dmpwanrq21pkvj7in4yw9dwikx7qjmxs1")))

(define-public crate-gamma-lut-0.2.1 (c (n "gamma-lut") (v "0.2.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1jcxspr34lh5x42ns7iyzycpz3gdvwm02i48p6cyz31n0d2hfrfx")))

(define-public crate-gamma-lut-0.2.2 (c (n "gamma-lut") (v "0.2.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1x91hwzq6f0rgqp6h0ddydxpdkah63kq39fx567khjr1xsrvw0f2")))

(define-public crate-gamma-lut-0.2.3 (c (n "gamma-lut") (v "0.2.3") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0mr8swk5dg60lnw4cph9nmbsqx3c3h6ac1r11hlpl1fkdaggw315")))

