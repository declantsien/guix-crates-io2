(define-module (crates-io ga ry gary-core) #:use-module (crates-io))

(define-public crate-gary-core-0.0.1 (c (n "gary-core") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "whoami") (r "^0.5.2") (d #t) (k 0)))) (h "1ly1dmdvsp3zbv7w97inhkhqq2jhxz85v8xklkkakw878d83fpsn")))

