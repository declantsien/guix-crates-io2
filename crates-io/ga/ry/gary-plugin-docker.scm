(define-module (crates-io ga ry gary-plugin-docker) #:use-module (crates-io))

(define-public crate-gary-plugin-docker-0.0.1 (c (n "gary-plugin-docker") (v "0.0.1") (d (list (d (n "bollard") (r "^0.3") (d #t) (k 0)) (d (n "core") (r "^0.0.1") (d #t) (k 0) (p "gary-core")) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "0w1ln81lsq25lhwn3fl3nxg7x86mkl4bl877yk1xz51g1kwhfnck")))

(define-public crate-gary-plugin-docker-0.0.2 (c (n "gary-plugin-docker") (v "0.0.2") (d (list (d (n "bollard") (r "^0.3") (d #t) (k 0)) (d (n "core") (r "^0.0.1") (d #t) (k 0) (p "gary-core")) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "0zylpd78n1ddcdq4xzcqpahj5dl6y9hmab1vzixgjaj1rri7gsjd")))

