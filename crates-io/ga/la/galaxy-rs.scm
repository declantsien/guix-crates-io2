(define-module (crates-io ga la galaxy-rs) #:use-module (crates-io))

(define-public crate-galaxy-rs-0.1.0 (c (n "galaxy-rs") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.19.1") (d #t) (k 0)))) (h "0grh3psjfg9d274y7z71fx9vnjdfp81ajs76bpq5bv4jf2j6iqrh")))

(define-public crate-galaxy-rs-0.1.1 (c (n "galaxy-rs") (v "0.1.1") (d (list (d (n "pyo3") (r "^0.19.1") (d #t) (k 0)))) (h "1wp1g6g0ys7iibylln4k8yr9zjlvmrcrb9cz68aszw9y2n4z9m8k")))

(define-public crate-galaxy-rs-0.1.2 (c (n "galaxy-rs") (v "0.1.2") (d (list (d (n "pyo3") (r "^0.19.1") (d #t) (k 0)))) (h "1ifwdaplzqsz4y96kd0s4gfldmh45cnr15a10lg2zcw3a4vjm54c")))

