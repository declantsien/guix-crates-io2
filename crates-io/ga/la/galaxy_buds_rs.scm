(define-module (crates-io ga la galaxy_buds_rs) #:use-module (crates-io))

(define-public crate-galaxy_buds_rs-0.2.1 (c (n "galaxy_buds_rs") (v "0.2.1") (d (list (d (n "async-std") (r "^1.8.0") (d #t) (k 0)) (d (n "bluetooth-serial-port-async") (r "^0.6.1") (d #t) (k 0)))) (h "1gj8akj1xcw342hzcnfcvlx7y6zfzygbzgwydmiyv040wc74ady4")))

(define-public crate-galaxy_buds_rs-0.2.2 (c (n "galaxy_buds_rs") (v "0.2.2") (d (list (d (n "async-std") (r "^1.8.0") (d #t) (k 0)) (d (n "bluetooth-serial-port-async") (r "^0.6.1") (d #t) (k 0)))) (h "02zl0wypqmhni3qg2b3sar6qrsxqqfkchkk1zsbm26f3kj92fnz4")))

(define-public crate-galaxy_buds_rs-0.2.3 (c (n "galaxy_buds_rs") (v "0.2.3") (d (list (d (n "async-std") (r "^1.8.0") (d #t) (k 0)) (d (n "bluetooth-serial-port-async") (r "^0.6.3") (d #t) (k 0)))) (h "1pgvnnczs5wa3f6mfqnsi9k9bd3j3gln8jk6n54rgha49i7mn26v")))

(define-public crate-galaxy_buds_rs-0.2.4 (c (n "galaxy_buds_rs") (v "0.2.4") (d (list (d (n "async-std") (r "^1.8.0") (d #t) (k 0)) (d (n "bluetooth-serial-port-async") (r "^0.6.3") (d #t) (k 0)))) (h "1f4viq2ccdgl6fbqcqb5xpiqkgzkc9kf9hhzki8gzfjg5gw084s2")))

(define-public crate-galaxy_buds_rs-0.2.5 (c (n "galaxy_buds_rs") (v "0.2.5") (d (list (d (n "async-std") (r "^1.8.0") (d #t) (k 0)) (d (n "bluetooth-serial-port-async") (r "^0.6.3") (d #t) (k 0)))) (h "1v6mbhc5qh9vmg7gfx3w7raffrrzjdh0mbyg4q2n1yxr6l2zzdfx")))

(define-public crate-galaxy_buds_rs-0.2.6 (c (n "galaxy_buds_rs") (v "0.2.6") (d (list (d (n "async-std") (r "^1.8.0") (d #t) (k 0)) (d (n "bluetooth-serial-port-async") (r "^0.6.3") (d #t) (k 0)))) (h "0mqcj7yp7jh7dljjyh0dk9f8zldyn1qf0hn3q2v2x6xbi2mzp2ma")))

(define-public crate-galaxy_buds_rs-0.2.7 (c (n "galaxy_buds_rs") (v "0.2.7") (d (list (d (n "async-std") (r "^1.8.0") (d #t) (k 0)) (d (n "bluetooth-serial-port-async") (r "^0.6.3") (d #t) (k 0)))) (h "086w9iz4qsh3mvl26i1wvi4rcxr5krw53hak19dlsnyqf2cz71mi")))

(define-public crate-galaxy_buds_rs-0.2.8 (c (n "galaxy_buds_rs") (v "0.2.8") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "bluetooth-serial-port-async") (r "^0.6.3") (d #t) (k 0)))) (h "1z64yyp7dxqlm6bmzyqv4ndw2ayj1516lqd6sgflpkmgda4jczak")))

(define-public crate-galaxy_buds_rs-0.2.9 (c (n "galaxy_buds_rs") (v "0.2.9") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "bluetooth-serial-port-async") (r "^0.6.3") (d #t) (k 0)))) (h "09hpl3wrlskdbp6r4nfvyjg9plhadmbvbbhx5xrs2n6gsww31wv4")))

(define-public crate-galaxy_buds_rs-0.2.9-1 (c (n "galaxy_buds_rs") (v "0.2.9-1") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "bluetooth-serial-port-async") (r "^0.6.3") (d #t) (k 0)))) (h "14lh85wdy81413v4iqyvj1k5cw927ffqknmksyrnpx7ilk5bbkws")))

(define-public crate-galaxy_buds_rs-0.2.10 (c (n "galaxy_buds_rs") (v "0.2.10") (d (list (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "bluetooth-serial-port-async") (r "^0.6.3") (d #t) (k 0)))) (h "1wlbxp79wssbcsy1aibq2lx5rir00g8xjdaawm1zgv41aymfh38l")))

