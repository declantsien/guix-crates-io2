(define-module (crates-io ga la galaxy_buds_live_rs) #:use-module (crates-io))

(define-public crate-galaxy_buds_live_rs-0.1.1 (c (n "galaxy_buds_live_rs") (v "0.1.1") (d (list (d (n "bluetooth-serial-port") (r "^0.6.0") (d #t) (k 0)))) (h "0p2r9fhlm4yk4p6s82877gy1lw95i60zgwvzgsrdm51llhbaw3n4")))

(define-public crate-galaxy_buds_live_rs-0.1.2 (c (n "galaxy_buds_live_rs") (v "0.1.2") (d (list (d (n "async-std") (r "^1.6.5") (d #t) (k 0)) (d (n "bluetooth-serial-port-async") (r "^0.6.0") (d #t) (k 0)))) (h "0khy72y5wvw98pnnkq4r4vnplkx52jq1w1h537kwpwq0mc8w0kvq")))

(define-public crate-galaxy_buds_live_rs-0.1.3 (c (n "galaxy_buds_live_rs") (v "0.1.3") (d (list (d (n "async-std") (r "^1.6.5") (d #t) (k 0)) (d (n "bluetooth-serial-port-async") (r "^0.6.0") (d #t) (k 0)))) (h "02qrlca2scgcbq01llqw8v2xql8afwkix40qyvqh5aqbsy048fkd")))

(define-public crate-galaxy_buds_live_rs-0.1.4 (c (n "galaxy_buds_live_rs") (v "0.1.4") (d (list (d (n "async-std") (r "^1.6.5") (d #t) (k 0)) (d (n "bluetooth-serial-port-async") (r "^0.6.0") (d #t) (k 0)))) (h "13gsqwcnhmg523qkpmh3c8rnl0mq39ky82dhn33r6rk4ig1b0k52")))

(define-public crate-galaxy_buds_live_rs-0.1.5 (c (n "galaxy_buds_live_rs") (v "0.1.5") (d (list (d (n "async-std") (r "^1.6.5") (d #t) (k 0)) (d (n "bluetooth-serial-port-async") (r "^0.6.0") (d #t) (k 0)))) (h "1xrjzqaqvxg4wsz3dshyrwjmf0v1h5di6h8jxwnm9gznf51f287q")))

(define-public crate-galaxy_buds_live_rs-0.1.6 (c (n "galaxy_buds_live_rs") (v "0.1.6") (d (list (d (n "async-std") (r "^1.6.5") (d #t) (k 0)) (d (n "bluetooth-serial-port-async") (r "^0.6.0") (d #t) (k 0)))) (h "096y4cqrhq4a4qm2003d12l9zqicadhsbxxddx6vvczqckdc0brr")))

(define-public crate-galaxy_buds_live_rs-0.2.0 (c (n "galaxy_buds_live_rs") (v "0.2.0") (d (list (d (n "async-std") (r "^1.7.0") (d #t) (k 0)) (d (n "bluetooth-serial-port-async") (r "^0.6.1") (d #t) (k 0)))) (h "1rl5320qqjxh3vkyidr9q7mjgbcs7hcqc06lc68ljn4xi5p9wkgp")))

