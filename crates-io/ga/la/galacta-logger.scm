(define-module (crates-io ga la galacta-logger) #:use-module (crates-io))

(define-public crate-galacta-logger-0.1.0 (c (n "galacta-logger") (v "0.1.0") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (f (quote ("std"))) (d #t) (k 0)))) (h "19a6y24hdxflv802bdhrcbf05g51d5h3xbiw72camrscdjb5ad5q")))

(define-public crate-galacta-logger-0.1.1 (c (n "galacta-logger") (v "0.1.1") (d (list (d (n "chrono") (r "0.4.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (f (quote ("std"))) (d #t) (k 0)))) (h "0kwq6j81lzhbfkgsypvb44aiiqv3cv8c6030n92j2dhz8alg4yx3")))

