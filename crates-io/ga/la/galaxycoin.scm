(define-module (crates-io ga la galaxycoin) #:use-module (crates-io))

(define-public crate-galaxycoin-0.0.4 (c (n "galaxycoin") (v "0.0.4") (d (list (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.0") (d #t) (k 0)))) (h "1qyi8cga9apbjx7z2926pd4dnhf1z3xi9x2d168g4c951n961645") (f (quote (("unstable")))) (y #t)))

(define-public crate-galaxycoin-0.0.5 (c (n "galaxycoin") (v "0.0.5") (d (list (d (n "digest") (r "^0.7.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.0") (d #t) (k 0)) (d (n "sha2") (r "^0.7.0") (d #t) (k 0)) (d (n "sha2-asm") (r "^0.3") (o #t) (d #t) (k 0)))) (h "17g9jy9zis9s8x40ddnqvw1ipgqd92h610ssba5g3swcy96z8ycr") (f (quote (("unstable") ("asm" "sha2-asm")))) (y #t)))

(define-public crate-galaxycoin-0.0.6 (c (n "galaxycoin") (v "0.0.6") (d (list (d (n "generic-array") (r "^0.9.0") (f (quote ("serde"))) (k 0)) (d (n "rayon") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "sha2") (r "^0.7.0") (d #t) (k 0)) (d (n "sha2-asm") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1giwi42cinrv58vck9di0kbkn6imcjmcx0mkqqjxpssp9qi8p3bi") (f (quote (("unstable") ("asm" "sha2-asm")))) (y #t)))

(define-public crate-galaxycoin-0.0.7 (c (n "galaxycoin") (v "0.0.7") (d (list (d (n "generic-array") (r "^0.9.0") (f (quote ("serde"))) (k 0)) (d (n "rayon") (r "^1.0.0") (d #t) (k 0)) (d (n "ring") (r "^0.13.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "sha2") (r "^0.7.0") (d #t) (k 0)) (d (n "sha2-asm") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "untrusted") (r "^0.6.2") (d #t) (k 0)))) (h "075xyy5lbhiqcy73qw08z9i2r478c04nd04q51b6yiac6ndwcfv3") (f (quote (("unstable") ("asm" "sha2-asm")))) (y #t)))

(define-public crate-galaxycoin-0.0.1 (c (n "galaxycoin") (v "0.0.1") (h "03gdiljnpncwx459v836lnr1dz7d5wjjjzwra4j2y4yk8xrlrp6l") (y #t)))

