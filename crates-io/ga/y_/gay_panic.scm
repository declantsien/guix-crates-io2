(define-module (crates-io ga y_ gay_panic) #:use-module (crates-io))

(define-public crate-gay_panic-0.1.0 (c (n "gay_panic") (v "0.1.0") (d (list (d (n "color_space") (r "^0.5.3") (d #t) (k 0)) (d (n "owo-colors") (r "~3.4.0") (d #t) (k 0)) (d (n "sashimi") (r "~0.1.1") (d #t) (k 0)))) (h "19ghbmj65r38i6z55g2manb5in7mnz6fcax68gqll4907s6pidz0")))

(define-public crate-gay_panic-1.0.0 (c (n "gay_panic") (v "1.0.0") (d (list (d (n "color_space") (r "^0.5.3") (d #t) (k 0)) (d (n "owo-colors") (r "~3.4.0") (d #t) (k 0)) (d (n "sashimi") (r "~0.1.1") (d #t) (k 0)))) (h "14dc2jydfsk29ham1bjp59q3vbi35j3xwf1zq0swn9c5lfvpzn1d") (r "1.65")))

(define-public crate-gay_panic-1.0.1 (c (n "gay_panic") (v "1.0.1") (d (list (d (n "color_space") (r "^0.5.3") (d #t) (k 0)) (d (n "owo-colors") (r "~3.4.0") (d #t) (k 0)) (d (n "sashimi") (r "~0.1.1") (d #t) (k 0)))) (h "0cg3nk74gq57r22g246fdx6k228ijw6z8ar2ckjvfmy1dkn75d8m") (r "1.65")))

