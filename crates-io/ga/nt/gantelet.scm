(define-module (crates-io ga nt gantelet) #:use-module (crates-io))

(define-public crate-gantelet-0.1.0 (c (n "gantelet") (v "0.1.0") (h "051asr2w6jk1vqhm6f5adiis28diqsmk1v16xkm834g0zc7mjdp9")))

(define-public crate-gantelet-0.1.1 (c (n "gantelet") (v "0.1.1") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "fasthash") (r "^0.4") (d #t) (k 0)))) (h "0m7bz4liwdvpk0x5ml35p7xwyypbfisyf6jfrpsr58ld4bakbnpy")))

(define-public crate-gantelet-0.1.2 (c (n "gantelet") (v "0.1.2") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "fasthash") (r "^0.4") (d #t) (k 0)))) (h "07bj44w8f5ssi9yyqbzjbiwyjismxrb0d6zgcpxya92d0lsgny31")))

(define-public crate-gantelet-0.3.0 (c (n "gantelet") (v "0.3.0") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "fasthash") (r "^0.4") (d #t) (k 0)) (d (n "indicatif") (r "^0") (d #t) (k 0)))) (h "0jvma7a8h5cpqq8iz64qwm9ihbixkysl7kb6qcq2r2yn4nj4aah7")))

(define-public crate-gantelet-0.3.1-alpha.2 (c (n "gantelet") (v "0.3.1-alpha.2") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "fasthash") (r "^0.4") (d #t) (k 0)) (d (n "indicatif") (r "^0") (d #t) (k 0)))) (h "13bqn3w1dxgvg7k58w2rwyshgx0xnmr4alfqhap4kv1yikmkr82h")))

(define-public crate-gantelet-0.3.1-alpha.3 (c (n "gantelet") (v "0.3.1-alpha.3") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "fasthash") (r "^0.4") (d #t) (k 0)) (d (n "indicatif") (r "^0") (d #t) (k 0)))) (h "01zsrsqgn3zalwjrnxzjgs0xsdgngvaixpkna3l1ppi8zcjssjzm")))

