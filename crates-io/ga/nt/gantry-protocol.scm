(define-module (crates-io ga nt gantry-protocol) #:use-module (crates-io))

(define-public crate-gantry-protocol-0.0.1 (c (n "gantry-protocol") (v "0.0.1") (d (list (d (n "bytes") (r "^0.5.4") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)) (d (n "prost-types") (r "^0.6.1") (d #t) (k 0)))) (h "020gqb9nv0yrznaa8c9n19p4gpqbmwx9vyvmrjdngs1skgxyv0pr")))

(define-public crate-gantry-protocol-0.0.3 (c (n "gantry-protocol") (v "0.0.3") (d (list (d (n "rmp-serde") (r "^0.14.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "182x5wci01xjnbika3pb0slpzvx3i5bakkcx203q3na7wx7jbi2p")))

(define-public crate-gantry-protocol-0.0.4 (c (n "gantry-protocol") (v "0.0.4") (d (list (d (n "rmp-serde") (r "^0.14.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "02n3h9j25q35532lr3xykgqgs262xvj9x0h9ab5wdf244pnf4kjz")))

(define-public crate-gantry-protocol-0.0.5 (c (n "gantry-protocol") (v "0.0.5") (d (list (d (n "rmp-serde") (r "^0.14.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "0rdfqi5w1s456aqppvnl8ln7hsk8fzf6v7151rnm440vhkn6fbhl")))

(define-public crate-gantry-protocol-0.0.6 (c (n "gantry-protocol") (v "0.0.6") (d (list (d (n "rmp-serde") (r "^0.14.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "1vhbggqnsykybr4317szvjzb6a9vh4ldc58x5xrwrj50blwqqmk8")))

(define-public crate-gantry-protocol-0.0.7 (c (n "gantry-protocol") (v "0.0.7") (d (list (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "1y64ylp5m6idvj0c2s8la9v6b7cxn6wqb3gs52p8qns6s8nw6s36")))

(define-public crate-gantry-protocol-0.0.8 (c (n "gantry-protocol") (v "0.0.8") (d (list (d (n "rmp-serde") (r "^0.14.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "09n2kn7q7vbazfn871isi9ham12v5s10w3wmsqp8l1dkw4plnrfm")))

