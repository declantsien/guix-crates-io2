(define-module (crates-io ga nt gantz-derive) #:use-module (crates-io))

(define-public crate-gantz-derive-0.1.0 (c (n "gantz-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1kbpk8phi2wwqw4ndlabv3izjf3rnmpvgffiiqk3rbxh5kyvm57j")))

