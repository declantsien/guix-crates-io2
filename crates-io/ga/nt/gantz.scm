(define-module (crates-io ga nt gantz) #:use-module (crates-io))

(define-public crate-gantz-0.1.0 (c (n "gantz") (v "0.1.0") (d (list (d (n "gantz-derive") (r "^0.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.4") (d #t) (k 0)))) (h "1d938j8v4b5va9j9g91yzkijcl2rf2khwjnxygn5x68f4vx8a1mk")))

