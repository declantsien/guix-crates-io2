(define-module (crates-io ga vl gavl) #:use-module (crates-io))

(define-public crate-gavl-0.1.0 (c (n "gavl") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "05dd7cf5i1fkq6l8mmd3kkz5i6iknaa9cvv5qi20ix465pgavrcz") (f (quote (("unchecked_mut") ("into_precompiled"))))))

(define-public crate-gavl-0.1.1 (c (n "gavl") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0qw08wk8rjljdsadzvkicjkibf1s5q5vdy8sza36z9ldhw2zbb7r") (f (quote (("unchecked_mut") ("into_precompiled"))))))

(define-public crate-gavl-0.1.2 (c (n "gavl") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "071hk1i9pm2grjyzlhxl8nh80my07rl6l9zawk3zckd8fmzipg7f") (f (quote (("unchecked_mut") ("into_precomputed"))))))

(define-public crate-gavl-0.1.3 (c (n "gavl") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1r10z200k8zmrd0bmmwmihgfffps7pyxj86sfzq4py8l51m0m6hw") (f (quote (("unchecked_mut") ("into_precomputed"))))))

(define-public crate-gavl-0.1.5 (c (n "gavl") (v "0.1.5") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "147g33kbhhsmpnnbjgm5k8pggrdmlih4c1ln0bjxnvsx0p0ngsks") (f (quote (("unchecked_mut") ("into_precomputed"))))))

