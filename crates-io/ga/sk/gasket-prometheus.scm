(define-module (crates-io ga sk gasket-prometheus) #:use-module (crates-io))

(define-public crate-gasket-prometheus-0.7.0 (c (n "gasket-prometheus") (v "0.7.0") (d (list (d (n "gasket") (r "^0.7.0") (d #t) (k 0)) (d (n "prometheus_exporter_base") (r "^1.4.0") (f (quote ("hyper_server"))) (d #t) (k 0)))) (h "0p1z6zs6fbdmfhs277z7cvagiybcp7p3xkl87ny1xlzb1jiz50l5")))

(define-public crate-gasket-prometheus-0.8.0 (c (n "gasket-prometheus") (v "0.8.0") (d (list (d (n "gasket") (r "^0.8.0") (d #t) (k 0)) (d (n "prometheus_exporter_base") (r "^1.4.0") (f (quote ("hyper_server"))) (d #t) (k 0)))) (h "07q4mbl2kb93nqa0a61d2xa8d7yqsy7n552lq721nz4wzi6jdkrw")))

