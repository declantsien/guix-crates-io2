(define-module (crates-io ga sk gask) #:use-module (crates-io))

(define-public crate-gask-0.1.0 (c (n "gask") (v "0.1.0") (h "1zcxblg37x15pjbdqd1h6m6ybalfnz92vqqvg8i75k83kfm3bdmc")))

(define-public crate-gask-0.1.1 (c (n "gask") (v "0.1.1") (h "0xyg79rzasrql4k936jni7nqbazw5pdl3srcj2cx0pc3kjsvwzmh")))

