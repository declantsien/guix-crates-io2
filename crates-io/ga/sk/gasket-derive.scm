(define-module (crates-io ga sk gasket-derive) #:use-module (crates-io))

(define-public crate-gasket-derive-0.4.0 (c (n "gasket-derive") (v "0.4.0") (d (list (d (n "gasket") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1dirzs3a6hkirwyvq7q6vr54fg961x3z2vkjlvrdl0j49wh9r0v4")))

(define-public crate-gasket-derive-0.5.0 (c (n "gasket-derive") (v "0.5.0") (d (list (d (n "gasket") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0saifb4dq4a0v9rgydhv3n3baalb45lxdp3bq9d506rbaz8aj56i")))

(define-public crate-gasket-derive-0.6.0 (c (n "gasket-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0f1i77ihlcnzcxi8lp2vf4ljf312b3xz1dsw1qmb4rxvl8ba3i8r")))

(define-public crate-gasket-derive-0.7.0 (c (n "gasket-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0nr4j73hcyqxh7g7cfrm9n5crrp974p4hwkis3fqyw4cqylhla4g")))

(define-public crate-gasket-derive-0.8.0 (c (n "gasket-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "1j19xy0dpnpdiq66lr87pp8n4v3apxwryx5rg18wb1f6lnfknvyi")))

