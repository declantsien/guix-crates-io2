(define-module (crates-io ga -s ga-scheduler) #:use-module (crates-io))

(define-public crate-ga-scheduler-0.1.0 (c (n "ga-scheduler") (v "0.1.0") (d (list (d (n "array-init") (r "^2.0.1") (d #t) (k 0)) (d (n "mockall") (r "^0.11.1") (d #t) (k 2)) (d (n "mockall_double") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ri0q4njjcavrmpynpn6sz7a00i6lib31wchraypw8c25l0yx3r3")))

(define-public crate-ga-scheduler-0.1.1 (c (n "ga-scheduler") (v "0.1.1") (d (list (d (n "array-init") (r "^2.0.1") (d #t) (k 0)) (d (n "mockall") (r "^0.11.1") (d #t) (k 2)) (d (n "mockall_double") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "14pakr0cjcgsj8sxxal94ryfly5y1b1qxgy1hmpdndxxckvi9w6j")))

