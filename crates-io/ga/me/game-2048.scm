(define-module (crates-io ga me game-2048) #:use-module (crates-io))

(define-public crate-game-2048-0.1.0 (c (n "game-2048") (v "0.1.0") (d (list (d (n "matrix_display") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.2") (d #t) (k 0)))) (h "0hd26qmahfbbf3dlgjggpzydbnlak9gj9fjcp1m4li4p1k8ip7g7")))

(define-public crate-game-2048-0.2.0 (c (n "game-2048") (v "0.2.0") (d (list (d (n "matrix_display") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.2") (d #t) (k 0)))) (h "0hifxdyb2y36939h90aqwbgy1ni2i6hw2z3y39nvhij1rnb87pvz")))

(define-public crate-game-2048-0.3.0 (c (n "game-2048") (v "0.3.0") (d (list (d (n "matrix_display") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.2") (d #t) (k 0)))) (h "14zqqfjvyqv9g01v352rf05dn7fpiyibmac1a8wrbcdvyls5n3r2")))

(define-public crate-game-2048-0.3.1 (c (n "game-2048") (v "0.3.1") (d (list (d (n "matrix_display") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.2") (d #t) (k 0)))) (h "1mzxyrj9g1g5g6s0j1f0wgm8swdj4v2d7slrknhl109bz7vgwdhc")))

(define-public crate-game-2048-0.3.2 (c (n "game-2048") (v "0.3.2") (d (list (d (n "matrix_display") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.2") (d #t) (k 0)))) (h "1kwh8n6kz5zqjf2rh3n9yb01bam0wkkaldqh7hyi93jx12d3hyyc")))

(define-public crate-game-2048-0.4.0 (c (n "game-2048") (v "0.4.0") (d (list (d (n "matrix_display") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.2") (d #t) (k 0)))) (h "1xz04qrca3b56qjgfi46y7kc1gnh1agzv3rmfjbi9ra9z8zizavk")))

(define-public crate-game-2048-0.5.0 (c (n "game-2048") (v "0.5.0") (d (list (d (n "matrix_display") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.2") (d #t) (k 0)))) (h "0zdhvpkip53b0xv6yhq79jh9gksds701f25czar2fd18pjw6nk3v")))

(define-public crate-game-2048-0.5.1 (c (n "game-2048") (v "0.5.1") (d (list (d (n "matrix_display") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.2") (d #t) (k 0)))) (h "10c9s87wnrkk36z9m96iyf1dzwqik5xxirwkil87l4ff3fn9hgyf")))

