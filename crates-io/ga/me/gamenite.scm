(define-module (crates-io ga me gamenite) #:use-module (crates-io))

(define-public crate-gamenite-0.1.0 (c (n "gamenite") (v "0.1.0") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "0kddpk7bpjy3ajw97b0v0wliaiach47brmd6j7b29lv1c32mzm0d")))

(define-public crate-gamenite-0.1.1 (c (n "gamenite") (v "0.1.1") (d (list (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "1yqpixirj1g3q0cip89252g3gh7b8n8ww0cwxv42qjll4sw89xmi")))

