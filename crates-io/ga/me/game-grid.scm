(define-module (crates-io ga me game-grid) #:use-module (crates-io))

(define-public crate-game-grid-0.1.0 (c (n "game-grid") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "derive") (r "^0.1.0") (d #t) (k 0)))) (h "0fpn8ixdg1dliaxwqr5w4ynmij5vhkbz8iy9mrv0zsg98l86bhdq") (s 2) (e (quote (("bevy-ivec2" "dep:bevy"))))))

(define-public crate-game-grid-0.1.1 (c (n "game-grid") (v "0.1.1") (d (list (d (n "bevy") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "derive") (r "^0.1.0") (d #t) (k 0)))) (h "03mjrn3p7d0c8lm38irprnri6nrxbzq9kiahvh7bsqg7j5advdmj") (s 2) (e (quote (("bevy-ivec2" "dep:bevy"))))))

(define-public crate-game-grid-0.1.2 (c (n "game-grid") (v "0.1.2") (d (list (d (n "bevy") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "derive") (r "^0.1.0") (d #t) (k 0)))) (h "0z4npqdznv6vljal502m867a5j2bhlhkgjhjw8w9n08g3sqy4gvj") (s 2) (e (quote (("bevy-ivec2" "dep:bevy"))))))

(define-public crate-game-grid-0.1.3 (c (n "game-grid") (v "0.1.3") (d (list (d (n "bevy") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "game-grid-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1mdj6hlb07baq38gaa97dyaxz7psggvj394hlxayywax7rlyz8gb") (s 2) (e (quote (("bevy-ivec2" "dep:bevy"))))))

