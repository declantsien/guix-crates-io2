(define-module (crates-io ga me gameboy_opengl) #:use-module (crates-io))

(define-public crate-gameboy_opengl-0.1.0 (c (n "gameboy_opengl") (v "0.1.0") (d (list (d (n "c_str_macro") (r "^1.0.2") (d #t) (k 0)) (d (n "gameboy_core") (r "^0.1.2") (d #t) (k 0)) (d (n "gl") (r "^0.7.0") (d #t) (k 0)) (d (n "glutin") (r "^0.11.0") (d #t) (k 0)))) (h "0q1yngh88mmd3m05y8crdsh291hhhnp7x2msx3zhixfd34fcq8lw")))

(define-public crate-gameboy_opengl-0.1.1 (c (n "gameboy_opengl") (v "0.1.1") (d (list (d (n "c_str_macro") (r "^1.0.2") (d #t) (k 0)) (d (n "gameboy_core") (r "^0.1.2") (d #t) (k 0)) (d (n "gl") (r "^0.7.0") (d #t) (k 0)) (d (n "glutin") (r "^0.11.0") (d #t) (k 0)))) (h "0m920l0cr3kbqkmsvl2i2c04kjzfnqrr43gadsfq01w0mmpvzn6l")))

(define-public crate-gameboy_opengl-0.1.2 (c (n "gameboy_opengl") (v "0.1.2") (d (list (d (n "c_str_macro") (r "^1.0.2") (d #t) (k 0)) (d (n "gameboy_core") (r "^0.1.4") (d #t) (k 0)) (d (n "gl") (r "^0.7.0") (d #t) (k 0)) (d (n "glutin") (r "^0.11.0") (d #t) (k 0)))) (h "0yq8px5k8jz3jvbxlra9jji9xz6j8syyyy62zdzvrljqnxn1z51r")))

(define-public crate-gameboy_opengl-0.1.3 (c (n "gameboy_opengl") (v "0.1.3") (d (list (d (n "c_str_macro") (r "^1.0.2") (d #t) (k 0)) (d (n "gameboy_core") (r "^0.1.9") (d #t) (k 0)) (d (n "gl") (r "^0.7.0") (d #t) (k 0)) (d (n "glutin") (r "^0.11.0") (d #t) (k 0)))) (h "0qnlwiml3b543g2jk3x655wcfgnnfcfshsbyap2sx6imrbhq5zli")))

(define-public crate-gameboy_opengl-0.1.4 (c (n "gameboy_opengl") (v "0.1.4") (d (list (d (n "c_str_macro") (r "^1.0") (d #t) (k 0)) (d (n "gameboy_core") (r "^0.2") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13") (d #t) (k 1)) (d (n "glutin") (r "^0.22.0-alpha3") (d #t) (k 0)))) (h "1c98ricbh766i0ja119rpl0kxa5hddw7dz4lzvwyblxfzbkpm7a5")))

(define-public crate-gameboy_opengl-0.1.5 (c (n "gameboy_opengl") (v "0.1.5") (d (list (d (n "c_str_macro") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "gameboy_core") (r "^0.2") (d #t) (k 0)) (d (n "gl_generator") (r "^0.13") (d #t) (k 1)) (d (n "glutin") (r "^0.22.0-alpha3") (d #t) (k 0)))) (h "0g9bnrv1pbbssma8qs047v2m623ccjnd3wabz1wnrpdn1irzl53z")))

(define-public crate-gameboy_opengl-0.1.6 (c (n "gameboy_opengl") (v "0.1.6") (d (list (d (n "c_str_macro") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "gameboy_core") (r "^0.2") (d #t) (k 0)) (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "glutin") (r "^0.22.0-alpha3") (d #t) (k 0)))) (h "1rjang81yz1d009x0g5is5gxjgc58csv3wv6k3qjbnyqhc7g7knf")))

(define-public crate-gameboy_opengl-0.1.7 (c (n "gameboy_opengl") (v "0.1.7") (d (list (d (n "c_str_macro") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "gameboy_core") (r "^0.2") (d #t) (k 0)) (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "glutin") (r "^0.22.0-alpha3") (d #t) (k 0)))) (h "0gkf1a5g9rvw4s928c2dn8g55wyj9n335nbbs6g60bv80rgaqp9p")))

(define-public crate-gameboy_opengl-0.1.8 (c (n "gameboy_opengl") (v "0.1.8") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "gameboy_core") (r "^0.2") (d #t) (k 0)) (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "glutin") (r "^0.22.0-alpha5") (d #t) (k 0)))) (h "08xx8f1kp1mjvz0wmzj6cfh26l8vvdiprx123wgswkcbgjy87n29")))

(define-public crate-gameboy_opengl-0.1.9 (c (n "gameboy_opengl") (v "0.1.9") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "gameboy_core") (r "^0.2") (d #t) (k 0)) (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "glutin") (r "^0.22.0-alpha5") (d #t) (k 0)))) (h "0fa023dcv6k8ai96rz7w21f68whciglyzlg8blmmzps28v5c369i")))

(define-public crate-gameboy_opengl-0.2.0 (c (n "gameboy_opengl") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "gameboy_core") (r "^0.2.6") (d #t) (k 0)) (d (n "gl_generator") (r "^0.14") (d #t) (k 1)) (d (n "glutin") (r "^0.22.0-alpha6") (d #t) (k 0)))) (h "05lk9a6s5j8inscp5fw6cmfm5byf0yj7scxb13lf81qy1ljlb8wa")))

(define-public crate-gameboy_opengl-0.2.1 (c (n "gameboy_opengl") (v "0.2.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "gameboy_core") (r "^0.2.7") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.33.0") (f (quote ("bundled" "static-link"))) (d #t) (k 0)))) (h "0drfinn90a9jxz56x5668xwjfjr1rpv2ppsndvzqsyk1kzkfxwjq")))

(define-public crate-gameboy_opengl-0.2.2 (c (n "gameboy_opengl") (v "0.2.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "gameboy_core") (r "^0.2.8") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.33.0") (f (quote ("bundled" "static-link"))) (d #t) (k 0)))) (h "09rdxi53qnbfpxcvrckd3qdcwbg4j8rslr231nfl8234511qyi4i")))

(define-public crate-gameboy_opengl-0.2.3 (c (n "gameboy_opengl") (v "0.2.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "gameboy_core") (r "^0.2.8") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.33.0") (f (quote ("bundled" "static-link"))) (d #t) (k 0)))) (h "1h14a64rxx58q39pfdzvijyzg9c638vbgr6b7kny7qswdp73dhzf")))

(define-public crate-gameboy_opengl-0.2.4 (c (n "gameboy_opengl") (v "0.2.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "gameboy_core") (r "^0.2.9") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.33.0") (f (quote ("bundled" "static-link"))) (d #t) (k 0)))) (h "0swpzlvdi7bywqkdjy7nmvwg2jhv8mwpwxzaf5whfsqgj7cw93av")))

(define-public crate-gameboy_opengl-0.2.5 (c (n "gameboy_opengl") (v "0.2.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "gameboy_core") (r "^0.3.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.33.0") (f (quote ("bundled" "static-link"))) (d #t) (k 0)))) (h "1jvhndh4b5jzyanv2iz5bd5cmswyrj9f8kagxrs1hynk1mcbrvy9")))

(define-public crate-gameboy_opengl-0.2.6 (c (n "gameboy_opengl") (v "0.2.6") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "gameboy_core") (r "^0.3.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.33.0") (f (quote ("bundled" "static-link"))) (d #t) (k 0)))) (h "0xw7fdg62dgkk9k7bixiwpfi8pisvifj6r9vlfgdgs7iw4c7skqq")))

(define-public crate-gameboy_opengl-0.2.7 (c (n "gameboy_opengl") (v "0.2.7") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "gameboy_core") (r "^0.3.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.33.0") (f (quote ("bundled" "static-link"))) (d #t) (k 0)))) (h "08ldgzlrzh70qd4ajlarmhmpbjwaya666j9nzyq5f54dp768p4lz")))

(define-public crate-gameboy_opengl-0.2.8 (c (n "gameboy_opengl") (v "0.2.8") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "directories") (r "^2.0") (d #t) (k 0)) (d (n "gameboy_core") (r "^0.3.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.0") (f (quote ("bundled" "static-link"))) (d #t) (k 0)))) (h "0mcq5n8mnj6vq7gr137026v9rj09pc10f0lpxy3cgi95ls5lzidb")))

