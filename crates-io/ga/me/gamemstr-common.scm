(define-module (crates-io ga me gamemstr-common) #:use-module (crates-io))

(define-public crate-gamemstr-common-0.1.0 (c (n "gamemstr-common") (v "0.1.0") (d (list (d (n "num-to-words") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0qbxc6m895wfag0nsgr9b5mvwsfs1cp7rd3fszvhrsvbagvnyiqx")))

(define-public crate-gamemstr-common-0.1.1 (c (n "gamemstr-common") (v "0.1.1") (d (list (d (n "diesel") (r "^2.0.3") (f (quote ("postgres"))) (o #t) (d #t) (k 0)) (d (n "num-to-words") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1z2l8386l44gf73jz1j2xfzsvpv2zg02bjjj60bywdsic30p95hl") (y #t) (s 2) (e (quote (("diesel" "dep:diesel"))))))

(define-public crate-gamemstr-common-0.2.0-alpha.1 (c (n "gamemstr-common") (v "0.2.0-alpha.1") (d (list (d (n "num-to-words") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1zrb12jxny0vkf72gsqrlphw0lqk1r89a7k9w4ya6njg99c27b17")))

(define-public crate-gamemstr-common-0.2.0-alpha.2 (c (n "gamemstr-common") (v "0.2.0-alpha.2") (d (list (d (n "num-to-words") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0pi5gl5d2cl6rk6da3bv380c2p5n5yx74cp63ljpki0ayf66jhxx")))

