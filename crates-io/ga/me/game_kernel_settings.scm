(define-module (crates-io ga me game_kernel_settings) #:use-module (crates-io))

(define-public crate-game_kernel_settings-0.1.0 (c (n "game_kernel_settings") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.72") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.72") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "15nld8r14alwpv7fi9q5fys43h8b7rzb9llm7nhzw7kca10v8qb7")))

