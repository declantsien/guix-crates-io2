(define-module (crates-io ga me games-rs) #:use-module (crates-io))

(define-public crate-games-rs-0.1.0 (c (n "games-rs") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "19c61wz34znqsxppaxxm1lmjq8m159qrb0v7wv4rcd4fid50763g")))

(define-public crate-games-rs-0.1.1 (c (n "games-rs") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "0w8rkmwbx0cb4vzfvlyh4s7q0cqx5w9j3n6ywz681zaingz7vh5m")))

(define-public crate-games-rs-0.1.2 (c (n "games-rs") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "05b0635ax100svfaxzysbjh4mgm5icmlfs846n18fzbxp9vqqfyf")))

(define-public crate-games-rs-0.1.3 (c (n "games-rs") (v "0.1.3") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.89") (d #t) (k 0)))) (h "1b89jqxykl6a7nkgdwqm2i7b3iaa115226g7xc72wddcckbm7cj3") (f (quote (("default") ("autosort"))))))

(define-public crate-games-rs-0.1.4 (c (n "games-rs") (v "0.1.4") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bkhv4ggkxb0b5zgn0y8vf8iwp4cl1gbjhywq592h5fbh020s6v9") (f (quote (("default") ("autosort"))))))

(define-public crate-games-rs-0.2.0 (c (n "games-rs") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "failure") (r "^0.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "08x1i537ixg33i0k9qz202nhprv3hl9g654c3637lvzv0rwza22f") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-games-rs-0.2.1 (c (n "games-rs") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "failure") (r "^0.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "00833nl8kygv7li9qsyzhivpy6j5yf31vaycxlqk1zxiqbgawxvp") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

(define-public crate-games-rs-0.3.0 (c (n "games-rs") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "105l6lg8akznkq3ffv1f1vzvhnkvj3g2hw10akq98hcqmmlrr7sb") (f (quote (("small_rng" "rand/small_rng") ("default"))))))

