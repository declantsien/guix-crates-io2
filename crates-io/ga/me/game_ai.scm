(define-module (crates-io ga me game_ai) #:use-module (crates-io))

(define-public crate-game_ai-0.1.31 (c (n "game_ai") (v "0.1.31") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bevy") (r "^0.11.0") (d #t) (k 0)) (d (n "game_ai_macros") (r "0.*") (d #t) (k 0)) (d (n "sweet") (r "0.*") (d #t) (k 2)))) (h "15m609mm9ambvrdd6nf7gp9s9zcwzzmlal55pm08sjqz2gv5k376") (y #t)))

(define-public crate-game_ai-0.1.32 (c (n "game_ai") (v "0.1.32") (h "04hh21d2h2l9b2slnkvwwf1zsy10a73kapqvwryilc36kqi2pcy0") (y #t)))

