(define-module (crates-io ga me gameboy-rom) #:use-module (crates-io))

(define-public crate-gameboy-rom-0.1.0 (c (n "gameboy-rom") (v "0.1.0") (d (list (d (n "nom") (r "^5.0.0-beta2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1na2vcwd44pvn8dk3vr03w7hdbbhbbyp0k9svz7b0kizr7gjddw4")))

(define-public crate-gameboy-rom-0.2.0 (c (n "gameboy-rom") (v "0.2.0") (d (list (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0gb8883gi4s40d8mcpb0sppfcv93v6vcmrz7a3zl3ngs70by2w6v")))

(define-public crate-gameboy-rom-0.2.1 (c (n "gameboy-rom") (v "0.2.1") (d (list (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "162qfdbnmz32v75kgs4jzdx96g75h2jh2xhqgqlmjfc66jhl9pid")))

(define-public crate-gameboy-rom-0.2.2 (c (n "gameboy-rom") (v "0.2.2") (d (list (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0dd9rw6dhf2dxfdcxix8hhc9730c43rrrcy63fxh5x0ijkip0qyr")))

(define-public crate-gameboy-rom-0.3.0 (c (n "gameboy-rom") (v "0.3.0") (d (list (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1zpvzw6l88b0pz8kplkval2zpxv25c0rn6a329lzyvws21sl9rmb")))

(define-public crate-gameboy-rom-0.4.0 (c (n "gameboy-rom") (v "0.4.0") (d (list (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0lyc7zxdhim85cciiywmncv8hg286psdcxr4m0wwk9s3250k649h")))

