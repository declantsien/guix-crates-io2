(define-module (crates-io ga me game_of_life_bevy) #:use-module (crates-io))

(define-public crate-game_of_life_bevy-0.1.0 (c (n "game_of_life_bevy") (v "0.1.0") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 0)))) (h "0yrna03vsd1s5rfpc6fivxjyfm14yxqxyj0srg2sa2yjha5b4pgc") (r "1.56.1")))

(define-public crate-game_of_life_bevy-0.1.1 (c (n "game_of_life_bevy") (v "0.1.1") (d (list (d (n "bevy") (r "^0.5.0") (d #t) (k 0)))) (h "0gksbhllbhf4vssbgjlrc46mqkvsvq3m0q7h94dkcx8krp2l3sii") (r "1.56.1")))

