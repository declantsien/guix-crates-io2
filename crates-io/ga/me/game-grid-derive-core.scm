(define-module (crates-io ga me game-grid-derive-core) #:use-module (crates-io))

(define-public crate-game-grid-derive-core-0.1.0 (c (n "game-grid-derive-core") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("extra-traits" "full" "fold"))) (d #t) (k 0)))) (h "15bj7k9h7grmlb9jjncfq38w1bg52zbxqv301yh79mmb2dh0hbal")))

