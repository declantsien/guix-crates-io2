(define-module (crates-io ga me gamesense) #:use-module (crates-io))

(define-public crate-gamesense-0.1.0 (c (n "gamesense") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.6") (d #t) (k 0)))) (h "039yping7z7pv0dvkacjsq127x5rpcspby27v3imrv02927jy3jn")))

(define-public crate-gamesense-0.1.1 (c (n "gamesense") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.6") (d #t) (k 0)))) (h "18zigmprwbj0fnhavk4xnrxxydjznjjjdpfx230kpnvfdws35hfh")))

(define-public crate-gamesense-0.1.2 (c (n "gamesense") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.6") (d #t) (k 0)))) (h "0cm8sz3gm61jlmqs4q6i714h474bfql5n72n6dkykrgba5k1r69l")))

