(define-module (crates-io ga me gameloop) #:use-module (crates-io))

(define-public crate-gameloop-0.1.0 (c (n "gameloop") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1pn5r9arrkr8x48j6j6i71rifbwqaz1hzaqvnkjkrs58j4xb0hja")))

(define-public crate-gameloop-0.1.1 (c (n "gameloop") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0j1g46xsi2hicni02k2prlb9wm4l0fd2525mjk4arm612xrilsbz")))

(define-public crate-gameloop-0.1.2 (c (n "gameloop") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "00bjzcnflxdmdmra6sr05715hw162zbv8shihjy1jabc0rkky1p3")))

(define-public crate-gameloop-0.2.0 (c (n "gameloop") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "15627idwdf58y54ccayyr1vbwn46whf0jzkgk5mz4sgaqp2y3aiv")))

