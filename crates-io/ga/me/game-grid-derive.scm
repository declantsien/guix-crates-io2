(define-module (crates-io ga me game-grid-derive) #:use-module (crates-io))

(define-public crate-game-grid-derive-0.1.0 (c (n "game-grid-derive") (v "0.1.0") (d (list (d (n "game-grid-derive-core") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("extra-traits" "full" "fold"))) (d #t) (k 0)))) (h "1v0ism9hh1n9sixnnqr0jy755f6yzanip9ijciyb91acx1f8pq4q")))

