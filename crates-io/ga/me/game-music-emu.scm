(define-module (crates-io ga me game-music-emu) #:use-module (crates-io))

(define-public crate-game-music-emu-0.2.0 (c (n "game-music-emu") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cpal") (r "^0.13.1") (d #t) (k 2)) (d (n "err-derive") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mk9xdmbs22q2kk5c14l1vp1542yi7hcz0nqa69wny35mpxx9hf1") (f (quote (("ym2612_emu_nuked") ("ym2612_emu_mame") ("vgm") ("spc") ("sap") ("nsfe") ("nsf") ("kss") ("hes") ("gym") ("gbs") ("default" "ay" "gbs" "gym" "hes" "kss" "nsf" "nsfe" "sap" "spc" "vgm" "ym2612_emu_nuked") ("ay"))))))

