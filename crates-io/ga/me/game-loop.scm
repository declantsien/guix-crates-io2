(define-module (crates-io ga me game-loop) #:use-module (crates-io))

(define-public crate-game-loop-0.1.0 (c (n "game-loop") (v "0.1.0") (h "1xdizvhyb7wsxsb39420qc2g6r059gg0ixy5r20q8h01knsf1d3h")))

(define-public crate-game-loop-0.2.0 (c (n "game-loop") (v "0.2.0") (h "10vydmvjxv33pyw5ingp862pgnrjiv39hk4v33n15ci3ad7imjjc")))

(define-public crate-game-loop-0.3.0 (c (n "game-loop") (v "0.3.0") (d (list (d (n "wasm-bindgen") (r "^0.2.56") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "web-sys") (r "^0.3.33") (f (quote ("Performance" "Window"))) (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "1i2v77ff9xbm44pcybfhkgy9sldb6q89g5svz6mk6rr3zljr3rmr")))

(define-public crate-game-loop-0.4.0 (c (n "game-loop") (v "0.4.0") (d (list (d (n "wasm-bindgen") (r "^0.2.56") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "web-sys") (r "^0.3.33") (f (quote ("Performance" "Window"))) (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "0b6867c5qh42d13p6q6j8kmmz6w6h0sybnas8kn4s8ga7kbs12xl")))

(define-public crate-game-loop-0.5.0 (c (n "game-loop") (v "0.5.0") (d (list (d (n "wasm-bindgen") (r "^0.2.56") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "web-sys") (r "^0.3.33") (f (quote ("Performance" "Window"))) (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "1x6ii32wjd7xp21fazli4i86w5gnfw6j1mnh99zag68snqr17rqa")))

(define-public crate-game-loop-0.6.0 (c (n "game-loop") (v "0.6.0") (d (list (d (n "wasm-bindgen") (r "^0.2.56") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "web-sys") (r "^0.3.33") (f (quote ("Performance" "Window"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "winit") (r "^0.21.0") (o #t) (d #t) (k 0)))) (h "0vha0jddzrxpmmis6c4xlf5i89b062z65qvy7y3xlyrkdwjydv63") (f (quote (("window" "winit"))))))

(define-public crate-game-loop-0.7.0 (c (n "game-loop") (v "0.7.0") (d (list (d (n "wasm-bindgen") (r "^0.2.56") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "web-sys") (r "^0.3.33") (f (quote ("Performance" "Window"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "winit") (r "^0.21.0") (o #t) (d #t) (k 0)))) (h "1x66wivralrrpl3s6a4n211x2mi54zrakycp591fhhp7115iqv6q") (f (quote (("window" "winit"))))))

(define-public crate-game-loop-0.7.1 (c (n "game-loop") (v "0.7.1") (d (list (d (n "wasm-bindgen") (r "^0.2.56") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "web-sys") (r "^0.3.33") (f (quote ("Performance" "Window"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "winit") (r "^0.22.2") (o #t) (d #t) (k 0)))) (h "1107w2x35jdfvr9icxm88p5i0wvx3yg9765cpxybdbp8ld20b5sq") (f (quote (("window" "winit"))))))

(define-public crate-game-loop-0.7.2 (c (n "game-loop") (v "0.7.2") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Performance" "Window"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "winit") (r "^0.24") (o #t) (d #t) (k 0)))) (h "144q2vdv16y47saqraf4pswmfc8znbyhmmzvm217qw5jjlzc9b7s") (f (quote (("window" "winit"))))))

(define-public crate-game-loop-0.7.3 (c (n "game-loop") (v "0.7.3") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Performance" "Window"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "winit") (r "^0.24") (o #t) (d #t) (k 0)))) (h "0chp2g6vg2xfv25sac9gnhcn8izil0bcawcsms3476ca3vxl9v0h") (f (quote (("window" "winit"))))))

(define-public crate-game-loop-0.8.0 (c (n "game-loop") (v "0.8.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Performance" "Window"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "winit") (r "^0.25") (o #t) (d #t) (k 0)))) (h "1gnihmr50i34jhijzzm003iadi7ph2chdzsbvyhlnmx0hqx0k367") (f (quote (("window" "winit"))))))

(define-public crate-game-loop-0.8.1 (c (n "game-loop") (v "0.8.1") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Performance" "Window"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "winit") (r "^0.26") (o #t) (d #t) (k 0)))) (h "1x408lp2zyqjfdq3dy3jyjvxi58l6lmjpq73lfgh8wfvi8hc9hl6") (f (quote (("window" "winit"))))))

(define-public crate-game-loop-0.8.2 (c (n "game-loop") (v "0.8.2") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Performance" "Window"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "winit") (r "^0.26") (o #t) (d #t) (k 0)))) (h "1913r4h4fkam66zmvrl9cq99a59b2k328krhg48dvfi7rsv007bf") (f (quote (("window" "winit"))))))

(define-public crate-game-loop-0.9.0 (c (n "game-loop") (v "0.9.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Performance" "Window"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "winit") (r "^0.26") (o #t) (d #t) (k 0)))) (h "1jrfgjmf1iqks8m11mavlsn8flzwcy66pw3panzgi8l4dqpbbhyd") (f (quote (("window" "winit"))))))

(define-public crate-game-loop-0.9.1 (c (n "game-loop") (v "0.9.1") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Performance" "Window"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "winit") (r "^0.26") (o #t) (d #t) (k 0)))) (h "00iwyxs2gax8a13k0fwf817xr8z92109blb3b8sbvr46izymjdm8") (f (quote (("window" "winit"))))))

(define-public crate-game-loop-0.10.0 (c (n "game-loop") (v "0.10.0") (d (list (d (n "tao") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Performance" "Window"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "winit") (r "^0.27") (o #t) (d #t) (k 0)))) (h "1r9cp5ymrshg0nqnlqrv1s5z4aaq8fvl9zamlac7kpkz7cj2vmpx")))

(define-public crate-game-loop-0.10.1 (c (n "game-loop") (v "0.10.1") (d (list (d (n "tao") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Performance" "Window"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "winit") (r "^0.27") (o #t) (d #t) (k 0)))) (h "1kmxalpahm6w2l69bip1rvi8zfi3z6bfiv2mpiry62k6hpzwlc92")))

(define-public crate-game-loop-0.10.2 (c (n "game-loop") (v "0.10.2") (d (list (d (n "tao") (r "^0.18") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Performance" "Window"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "winit") (r "^0.28") (o #t) (d #t) (k 0)))) (h "0dmwi24jl8wr1l9zlrk104x0d8nv1dvmd1wnfagynym7piysqvyr")))

(define-public crate-game-loop-1.0.0 (c (n "game-loop") (v "1.0.0") (d (list (d (n "tao") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Performance" "Window"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "winit") (r "^0.28") (o #t) (d #t) (k 0)))) (h "0l0zkhfmqjdmn6avfd3992ag2x5h7nvhvzjkdy9wc0i2g1vixfvh")))

(define-public crate-game-loop-1.0.1 (c (n "game-loop") (v "1.0.1") (d (list (d (n "tao") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Performance" "Window"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "winit") (r "^0.28") (o #t) (d #t) (k 0)))) (h "12jwx0997y0hi2b430h0rb2gg5v7iqx2vnmywz7i9ajlqr5qhmia")))

(define-public crate-game-loop-1.1.0 (c (n "game-loop") (v "1.1.0") (d (list (d (n "tao") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Performance" "Window"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "winit") (r "^0.29") (o #t) (d #t) (k 0)))) (h "0a001pgpcpj4ld7644axdxppvf509dalw5bgj845s24vi0qvl1ia")))

(define-public crate-game-loop-1.2.0 (c (n "game-loop") (v "1.2.0") (d (list (d (n "tao") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Performance" "Window"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "winit") (r "^0.30") (o #t) (d #t) (k 0)))) (h "105c4hxw2lrv3l25zryj2ip0cpcbvz1d3n8i6hhl6ydjy24m2mjz")))

