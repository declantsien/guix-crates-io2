(define-module (crates-io ga me gametime) #:use-module (crates-io))

(define-public crate-gametime-0.0.0 (c (n "gametime") (v "0.0.0") (h "0z3yan6p8nwvva766i1s9kczp7nqdy31bli5a6gjqn4cj0ydl1wh")))

(define-public crate-gametime-0.1.0 (c (n "gametime") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1r5jid1y14km9msv2an4dq0hw1d4crhlg1nj3cr3mdgx0pwrijj6") (f (quote (("std") ("global_reference" "std") ("default" "std"))))))

(define-public crate-gametime-0.1.1 (c (n "gametime") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "12s9898l398r7whl9df255jdhc67664lv1ank1hmwcb7am24gs62") (f (quote (("std") ("global_reference" "std") ("default" "std")))) (y #t)))

(define-public crate-gametime-0.1.2 (c (n "gametime") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "04j43196d6n39nrgh93s9r51n0vj6pgw8518nadvrwkl2ivan5v8") (f (quote (("std") ("global_reference" "std") ("default" "std"))))))

(define-public crate-gametime-0.1.3 (c (n "gametime") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "082qin1728px7fl9pb3g4705rq7nrrd5m6a863n0ahld7mfr6hc2") (f (quote (("std") ("global_reference" "std") ("default" "std"))))))

(define-public crate-gametime-0.1.4 (c (n "gametime") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1yl3gv2mr9lmwrpz714kp813dhdw4b7wjkczpdwcv1jahqfsnyrb") (f (quote (("std") ("global_reference" "std") ("default" "std"))))))

(define-public crate-gametime-0.1.5 (c (n "gametime") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1r3plyaw98v2p7lcbfwn31srbx7sf40ih0karhf041xibc88s5ng") (f (quote (("std") ("global_reference" "std") ("default" "std"))))))

(define-public crate-gametime-0.1.6 (c (n "gametime") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1sdh8fw4s4m8q9x7wpincjd2y7ch41l7k8x8byja4hid4cmi9dpz") (f (quote (("std") ("global_reference" "std") ("default" "std"))))))

(define-public crate-gametime-0.2.0 (c (n "gametime") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1hcj4f138866x2sklmkw35hsar12bskzha26k7133qdk2baw6nj1") (f (quote (("std") ("global_reference" "std") ("default" "std"))))))

(define-public crate-gametime-0.2.1 (c (n "gametime") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0w41yk5vzdys5fr2niby2zlmvd1f8818a9kdfnkp1m0q4zqcf9k7") (f (quote (("std") ("global_reference" "std") ("default" "std"))))))

(define-public crate-gametime-0.3.0 (c (n "gametime") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "192mw2ab1fnlv86lnl2b7x73rjypxjl3cpr8ghvnxshhpmdq47hk") (f (quote (("std") ("global_reference" "std") ("default" "std"))))))

(define-public crate-gametime-0.3.1 (c (n "gametime") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1p4mm66hsb7n6f95mbr857adnsfqbcr9z68j6269zjgqh1dz2f2x") (f (quote (("std") ("global_reference" "std") ("default" "std"))))))

(define-public crate-gametime-0.4.0 (c (n "gametime") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "11xh2knqczmcf5iiwqf12x3afrc3iszmjlj12b9jiwcnk2q8gqa0") (f (quote (("std") ("global_reference" "std") ("default" "std"))))))

(define-public crate-gametime-0.4.1 (c (n "gametime") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "01d6bayk9qnvskm731i6b5wklzmifw4j8z4drd7j9fpfwz73l8jc") (f (quote (("std") ("global_reference" "std") ("default" "std"))))))

