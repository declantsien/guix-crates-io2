(define-module (crates-io ga me game_chess_cui) #:use-module (crates-io))

(define-public crate-game_chess_cui-0.0.1 (c (n "game_chess_cui") (v "0.0.1") (d (list (d (n "game_chess_core") (r "~0.0") (d #t) (k 0)) (d (n "wca") (r "^0.1.0") (d #t) (k 0)))) (h "09694rmdkhrl29dqhyhnd9f35zkw6pkikjjg1m6pghx23kklzr2q")))

(define-public crate-game_chess_cui-0.0.2 (c (n "game_chess_cui") (v "0.0.2") (d (list (d (n "game_chess_core") (r "~0.0") (d #t) (k 0)) (d (n "wca") (r "^0.1.0") (d #t) (k 0)))) (h "01czc9fdvxgjclbwqsklqbqlhdf1r9sdbhf6fjy74r64kfgni8sd")))

