(define-module (crates-io ga me gamelib) #:use-module (crates-io))

(define-public crate-gamelib-0.1.0 (c (n "gamelib") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "raylib") (r "^3.7.0") (d #t) (k 0)))) (h "0746m3mrsqs6pw7jn0hckqswp954ixlsiyd2p7xm1rx8jmn3p84k")))

