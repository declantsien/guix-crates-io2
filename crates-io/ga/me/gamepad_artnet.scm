(define-module (crates-io ga me gamepad_artnet) #:use-module (crates-io))

(define-public crate-gamepad_artnet-0.1.0 (c (n "gamepad_artnet") (v "0.1.0") (d (list (d (n "artnet_protocol") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "gilrs") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)))) (h "04fm5yn7shmlcm3diz1clb9bysfpxgxzi4y0mbrqznn8w5ihphb3")))

(define-public crate-gamepad_artnet-0.1.1 (c (n "gamepad_artnet") (v "0.1.1") (d (list (d (n "artnet_protocol") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "gilrs") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)))) (h "1zs08hkwcbjv5kq9spdq07d32y5lrlcclj9r3hvaaj1kdkmil24c")))

(define-public crate-gamepad_artnet-0.1.2 (c (n "gamepad_artnet") (v "0.1.2") (d (list (d (n "artnet_protocol") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "gilrs") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)))) (h "0x8j9s9z1s7lgdq165y49h2rw8wywxwpqanljzkf5qvqmilyg4rf")))

