(define-module (crates-io ga me game_of_life_kata) #:use-module (crates-io))

(define-public crate-game_of_life_kata-0.1.0 (c (n "game_of_life_kata") (v "0.1.0") (d (list (d (n "text-colorizer") (r "^1.0.0") (d #t) (k 0)))) (h "1n5vk57xm7qj7dvnw4vpickgsw852sk7n08jgq1j8gvhv9xrxfbg")))

(define-public crate-game_of_life_kata-0.1.1 (c (n "game_of_life_kata") (v "0.1.1") (d (list (d (n "text-colorizer") (r "^1.0.0") (d #t) (k 0)))) (h "103262p6324bmwbxmfsxgzqkdji3s9awmvp7iwbz2idfiadwvh1i")))

