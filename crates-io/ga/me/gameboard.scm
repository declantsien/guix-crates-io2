(define-module (crates-io ga me gameboard) #:use-module (crates-io))

(define-public crate-gameboard-0.1.0 (c (n "gameboard") (v "0.1.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)))) (h "19lpbr4f1k68bygz2krk3xm18hdjpvjv6zw8b5v15hl0q57ly724")))

