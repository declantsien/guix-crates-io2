(define-module (crates-io ga me game-of-life) #:use-module (crates-io))

(define-public crate-game-of-life-0.1.0 (c (n "game-of-life") (v "0.1.0") (d (list (d (n "clap") (r "^2.29.2") (d #t) (k 0)) (d (n "piston") (r "^0.35.0") (d #t) (k 0)) (d (n "piston2d-graphics") (r "^0.24.0") (d #t) (k 0)) (d (n "piston2d-opengl_graphics") (r "^0.50.0") (d #t) (k 0)) (d (n "pistoncore-glutin_window") (r "^0.43.0") (d #t) (k 0)))) (h "1b27q00qq64b83ra7xx7p5x8kyqsmrq3vxn52vgq2pcrjl9461wv")))

