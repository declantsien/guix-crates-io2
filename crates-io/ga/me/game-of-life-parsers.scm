(define-module (crates-io ga me game-of-life-parsers) #:use-module (crates-io))

(define-public crate-game-of-life-parsers-0.1.0 (c (n "game-of-life-parsers") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "0p91ir7pxrdl86lr4wkrbw4zzx1c7xz0pagf7zrxyjpbrn0fahgy") (y #t)))

(define-public crate-game-of-life-parsers-0.1.1 (c (n "game-of-life-parsers") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "1g9nszs3l7y9b3plb5xm0mn5ns1b2fzi90lakag5clkiz4g3dfk3")))

(define-public crate-game-of-life-parsers-0.2.0 (c (n "game-of-life-parsers") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "1xbv0mn234npn505r1rrpchal8428246pmgam4zz8zsljyp2shg4")))

(define-public crate-game-of-life-parsers-1.0.0 (c (n "game-of-life-parsers") (v "1.0.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "19bky280p2zql99k7wb4lq7310g9351asgh1k9g5d49b3n09xmix")))

(define-public crate-game-of-life-parsers-2.0.0 (c (n "game-of-life-parsers") (v "2.0.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "io-test-util") (r "^0.1.0") (d #t) (k 2)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "1m9710fbs8n4b1mvrwqia7ldzldh75hbvfxbzzqr8mkqc2ahq2hc")))

(define-public crate-game-of-life-parsers-2.0.1 (c (n "game-of-life-parsers") (v "2.0.1") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "io-test-util") (r "^0.1.0") (d #t) (k 2)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)))) (h "03bwkhqsrb8n96lkxffi75f0n5q1653n8x6665llmcdm5n56aay2")))

