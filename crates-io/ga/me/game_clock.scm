(define-module (crates-io ga me game_clock) #:use-module (crates-io))

(define-public crate-game_clock-1.0.0 (c (n "game_clock") (v "1.0.0") (h "1hkpci1sdqr1i7f25mqr1wr5zbszmjbpngp9mai96mc93a7a5j7g")))

(define-public crate-game_clock-1.1.0 (c (n "game_clock") (v "1.1.0") (h "0lcgyg1s7crfn3vxwzdv23267hps7xarc8348s060w87n5z9dcqy")))

(define-public crate-game_clock-1.1.1 (c (n "game_clock") (v "1.1.1") (h "11385gxpp3qds1rw235vjk8wmqjvp61hd5hii9i8z2mymrg9d9fj")))

