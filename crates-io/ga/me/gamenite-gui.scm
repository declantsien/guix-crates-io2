(define-module (crates-io ga me gamenite-gui) #:use-module (crates-io))

(define-public crate-gamenite-gui-0.1.0 (c (n "gamenite-gui") (v "0.1.0") (d (list (d (n "eframe") (r "^0.18.0") (d #t) (k 0)) (d (n "gamenite") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.24.2") (d #t) (k 0)) (d (n "rfd") (r "^0.8.2") (d #t) (k 0)))) (h "152b6zfs2gjib6984d40sdrmd0lkk28z70fgmgkcmn5jp1cb2j3j")))

