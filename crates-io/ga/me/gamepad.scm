(define-module (crates-io ga me gamepad) #:use-module (crates-io))

(define-public crate-gamepad-0.1.0 (c (n "gamepad") (v "0.1.0") (h "0p97kz8b8dwgrq7w5mj8pj9mg1xybrl9wd4pi3rb4h2hiw9x960z")))

(define-public crate-gamepad-0.1.1 (c (n "gamepad") (v "0.1.1") (d (list (d (n "winapi") (r "0.3.*") (f (quote ("xinput" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0k41116j6jpxpagclx77flfl7xbyv3c17apbdzm0x15zfnz397lj")))

(define-public crate-gamepad-0.1.2 (c (n "gamepad") (v "0.1.2") (d (list (d (n "gilrs") (r "^0.8.1") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "0.3.*") (f (quote ("xinput" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0dn62yka9l74cx70s75z8nhbp3bnmclf8v0lis6dy2qrdlb50q90")))

(define-public crate-gamepad-0.1.3 (c (n "gamepad") (v "0.1.3") (d (list (d (n "gilrs") (r "^0.8.1") (d #t) (t "cfg(not(any(windows, android)))") (k 0)) (d (n "winapi") (r "0.3.*") (f (quote ("xinput" "winerror"))) (d #t) (t "cfg(windows)") (k 0)))) (h "15wz5s9a069py8aprs1lha4ia158hq3yvldvy3akc00cazbxbqbh")))

(define-public crate-gamepad-0.1.4 (c (n "gamepad") (v "0.1.4") (d (list (d (n "gilrs") (r "^0.8.1") (d #t) (t "cfg(not(any(android)))") (k 0)))) (h "04n8711qcxmm92lad7vxklhl8gdfdpjk7nppdgvf92y247ms2946")))

(define-public crate-gamepad-0.1.5 (c (n "gamepad") (v "0.1.5") (d (list (d (n "gilrs") (r "^0.8.1") (d #t) (t "cfg(not(any(android)))") (k 0)))) (h "1057r89xwjqj4pl4s1b47yxzhd2ch22rcg75xkryvb2j50n9jdyi")))

(define-public crate-gamepad-0.1.6 (c (n "gamepad") (v "0.1.6") (d (list (d (n "gilrs") (r "^0.8.1") (d #t) (t "cfg(not(any(android, target_family = \"wasm\")))") (k 0)))) (h "14jqa22si7jm10072vdc1aav0mii7mdql7nyx09p8pim67lmpmni")))

