(define-module (crates-io ga me game_chess) #:use-module (crates-io))

(define-public crate-game_chess-0.0.1 (c (n "game_chess") (v "0.0.1") (d (list (d (n "bevy") (r "^0.5") (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "bevy") (r "^0.5") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "bevy_webgl2") (r "^0.5") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "game_chess_core") (r "~0.0") (d #t) (k 0)) (d (n "tracing-wasm") (r "=0.2.0") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1a2skxg3fcg893id6lj8922xv7szqsnyqzr0ihzmhkw8bvhjkkib") (f (quote (("web" "bevy/bevy_gltf" "bevy/bevy_winit" "bevy/render" "bevy/png") ("desktop") ("default" "desktop"))))))

