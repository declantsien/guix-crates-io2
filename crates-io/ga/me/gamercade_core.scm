(define-module (crates-io ga me gamercade_core) #:use-module (crates-io))

(define-public crate-gamercade_core-0.1.0 (c (n "gamercade_core") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.4.1") (d #t) (k 0)))) (h "0vpq4bc63kzd7zjcqf26731pq4n510z3qp5f22k540d7xc8lgxyz")))

