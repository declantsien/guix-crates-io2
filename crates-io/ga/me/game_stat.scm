(define-module (crates-io ga me game_stat) #:use-module (crates-io))

(define-public crate-game_stat-0.1.0 (c (n "game_stat") (v "0.1.0") (h "0l67bck1ivn7xhpvg3f9fzs88j5djhpivbd97rfdlrlggk22addb")))

(define-public crate-game_stat-0.1.1 (c (n "game_stat") (v "0.1.1") (h "026lzh5n08ghkw2bfwxnxbfhav01s8aw27rawk4dlbdv34mwhc6n") (f (quote (("sync") ("default"))))))

(define-public crate-game_stat-0.2.0 (c (n "game_stat") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "tinyvec") (r "^1.6") (f (quote ("alloc" "rustc_1_55"))) (d #t) (k 0)))) (h "1szkjibvh7zsrdfccz61xxgnfmf0a255bcdbbd31dkyz549jxcjk") (f (quote (("sync") ("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-game_stat-0.2.1 (c (n "game_stat") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "tinyvec") (r "^1.6") (f (quote ("alloc" "rustc_1_55"))) (d #t) (k 0)))) (h "1skqi5b4rzqhhgmf6nfzj97v5zdmlyddl2zl0wm1kbs2wrb19hkp") (f (quote (("sync") ("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-game_stat-0.2.2 (c (n "game_stat") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "tinyvec") (r "^1.6") (f (quote ("alloc" "rustc_1_55"))) (d #t) (k 0)))) (h "1imf5fv1w4wq5v3yn9xm504zxka2ihhyra3zjim8xbg6q1sasxyb") (f (quote (("sync") ("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

