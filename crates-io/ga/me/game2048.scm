(define-module (crates-io ga me game2048) #:use-module (crates-io))

(define-public crate-game2048-0.0.1 (c (n "game2048") (v "0.0.1") (d (list (d (n "sdl2") (r "~0.0.3") (d #t) (k 0)) (d (n "sdl2_gfx") (r "~0.0.3") (d #t) (k 0)) (d (n "sdl2_ttf") (r "~0.0.3") (d #t) (k 0)))) (h "0ms7anf6rhs2nzjl0xyw2rs0iapzw609vbd0h3ygif1zg2x38z4d")))

(define-public crate-game2048-0.0.4 (c (n "game2048") (v "0.0.4") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.4") (d #t) (k 0)) (d (n "sdl2_gfx") (r "^0.4") (d #t) (k 0)) (d (n "sdl2_ttf") (r "^0.4") (d #t) (k 0)))) (h "1zfkdx6503rmdwgkyg8wyqc97wcavbcn5rbc03hm5z7ck2d3hvfy")))

(define-public crate-game2048-0.0.5 (c (n "game2048") (v "0.0.5") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.9") (d #t) (k 0)) (d (n "sdl2_gfx") (r "*") (d #t) (k 0)) (d (n "sdl2_ttf") (r "*") (d #t) (k 0)))) (h "0cgc19pc3lb79zjmsvcbc48gsbqc1wzxd1cxk3c4rfjkrvc6qqk9")))

