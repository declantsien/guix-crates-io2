(define-module (crates-io ga me game_ai_macros) #:use-module (crates-io))

(define-public crate-game_ai_macros-0.1.31 (c (n "game_ai_macros") (v "0.1.31") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0qgx5r52d0wf0q512k2i33hw6n1nb3lcn7wxm3s8lli3mj417s4g") (y #t)))

(define-public crate-game_ai_macros-0.1.32 (c (n "game_ai_macros") (v "0.1.32") (h "08706wysifr9cnw2jyaraxjs4bv7ml7wdjwh2vbk96ir848a60qk") (y #t)))

