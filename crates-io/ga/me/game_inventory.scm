(define-module (crates-io ga me game_inventory) #:use-module (crates-io))

(define-public crate-game_inventory-0.1.1 (c (n "game_inventory") (v "0.1.1") (h "1vf9wlwbg7ky41yg0q88c29qlgymjayzw7y6dmz1wdgn452qlry6")))

(define-public crate-game_inventory-0.1.2 (c (n "game_inventory") (v "0.1.2") (h "0p7r4q3mf6v8xcnkk3g7y1v1vsm2w0rh9b8dgsmn4v97nz35p0dy")))

(define-public crate-game_inventory-0.2.0 (c (n "game_inventory") (v "0.2.0") (h "17bkphn1l853dhaxb391j6p3s5kn6xia0h49kn0z21pp0rar4mzp")))

(define-public crate-game_inventory-0.2.1 (c (n "game_inventory") (v "0.2.1") (h "1m9jg6gg4nbahy5jmbaj9427lrrq4vja0lmkgykd5x2jvdprfcv2")))

