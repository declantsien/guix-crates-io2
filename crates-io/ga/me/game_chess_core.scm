(define-module (crates-io ga me game_chess_core) #:use-module (crates-io))

(define-public crate-game_chess_core-0.0.1 (c (n "game_chess_core") (v "0.0.1") (d (list (d (n "pleco") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00xnyn3hg68vsqy4zym3rijh5bkqi7d9ay0hd5c15n5s87w0ygsj")))

(define-public crate-game_chess_core-0.0.2 (c (n "game_chess_core") (v "0.0.2") (d (list (d (n "pleco") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ppam9r7ijdzsfbjxa02p5sf0j6jxh2pylalfc6w82c8c6v2dcxp")))

