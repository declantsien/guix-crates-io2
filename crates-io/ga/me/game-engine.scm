(define-module (crates-io ga me game-engine) #:use-module (crates-io))

(define-public crate-game-engine-0.1.0 (c (n "game-engine") (v "0.1.0") (h "1s2kym68fw9mmp6wrs56awx5pdq0bsiv6qx55bhpwkax5kmr61pl")))

(define-public crate-game-engine-0.0.0 (c (n "game-engine") (v "0.0.0") (h "0f215g7jffn7fkb2wk0lg2ikcnagq3va6b6vd12jqjig3b4y1fbm") (y #t)))

