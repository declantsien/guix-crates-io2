(define-module (crates-io ga me game_die) #:use-module (crates-io))

(define-public crate-game_die-0.0.1 (c (n "game_die") (v "0.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0mhn1n38a800mx5pbvam9k622zchzz0dp6zaz7b0wryx0z71yaxh") (f (quote (("history"))))))

(define-public crate-game_die-0.0.2 (c (n "game_die") (v "0.0.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "01bk6hqhma4rbgs9hvgw5cv08nqqp7v6z5x88w50gwyawzxhbpak") (f (quote (("history"))))))

