(define-module (crates-io ga me gameoflife_cli) #:use-module (crates-io))

(define-public crate-gameoflife_cli-0.1.0 (c (n "gameoflife_cli") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1d4npnkqfkyv1aax9w4dw393rk275q9rxwmqwra35dmccj6dvhyr") (y #t)))

(define-public crate-gameoflife_cli-0.2.0 (c (n "gameoflife_cli") (v "0.2.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1s6iw6k83c5q3m8fsyb1ra1dg1z5x74cfv1sbgij1f79l8q79v6v") (y #t)))

(define-public crate-gameoflife_cli-0.3.0 (c (n "gameoflife_cli") (v "0.3.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "0var6njhzvwzs8237knbsc36cvfi4f95pxjxnxyrybbgkww73wz4") (y #t)))

