(define-module (crates-io ga me gamesman-nova) #:use-module (crates-io))

(define-public crate-gamesman-nova-0.1.0 (c (n "gamesman-nova") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "strsim") (r "^0") (d #t) (k 0)))) (h "0a6mmdjr5d630ga5vfhz1h4ah9rns5xwijd9w9y7h9sbhy4ravca")))

(define-public crate-gamesman-nova-0.1.1 (c (n "gamesman-nova") (v "0.1.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "strsim") (r "^0") (d #t) (k 0)))) (h "1mgz1i7k2hn2906hlagg6blrcvsqvzgwqmr2a8ng1f9kkqmp9a6h")))

(define-public crate-gamesman-nova-0.1.2 (c (n "gamesman-nova") (v "0.1.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "strsim") (r "^0") (d #t) (k 0)))) (h "19bppqn4vi76lnax7683qzikmchbm73a52dfv68vaw43yd3f7rvn")))

(define-public crate-gamesman-nova-0.1.3 (c (n "gamesman-nova") (v "0.1.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "strsim") (r "^0") (d #t) (k 0)))) (h "0327gkys4hgh7nfj086idps4ddngnv00vch2g0wky5d6vmhzqrsa")))

(define-public crate-gamesman-nova-0.1.4 (c (n "gamesman-nova") (v "0.1.4") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0") (d #t) (k 0)) (d (n "num-traits") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0k1ssbdkrdsyz04hhn54zlbj5vary8ikgnbwjq8l3nlpa4a549fn")))

(define-public crate-gamesman-nova-0.1.5 (c (n "gamesman-nova") (v "0.1.5") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "exitcode") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0") (d #t) (k 0)) (d (n "num-traits") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "15axm7r8zqfcawq3wfssdhrw05sj1f1yj72b37r7ifd4ijj512rd")))

