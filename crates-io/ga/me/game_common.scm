(define-module (crates-io ga me game_common) #:use-module (crates-io))

(define-public crate-game_common-0.1.0 (c (n "game_common") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mbbj7lwix0zpvjgpsbqq7fccha7z7d6lfvkq982pn86m703sk6k")))

