(define-module (crates-io ga me gameoflife) #:use-module (crates-io))

(define-public crate-gameoflife-0.3.1 (c (n "gameoflife") (v "0.3.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1ncagwc4cvmfzn4dk1cazj0k29i9k0h7n4kh5sdc8cxap46fmim5")))

(define-public crate-gameoflife-1.0.0 (c (n "gameoflife") (v "1.0.0") (d (list (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "0gf1gw8zparv58ijsh11npkg6d2ja0n6p89m7xaddr19c9c21sjm")))

(define-public crate-gameoflife-1.0.1 (c (n "gameoflife") (v "1.0.1") (d (list (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1aa9z8il6hsmffwc2nxglmg01v4fgvmar49agsa4pljxf56s3f5f")))

(define-public crate-gameoflife-1.0.2 (c (n "gameoflife") (v "1.0.2") (d (list (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "17gpvxnwyh35ircbajd80lyw6yrlkrmh2r82givcf3pm4xhb08m3") (y #t)))

(define-public crate-gameoflife-1.0.3 (c (n "gameoflife") (v "1.0.3") (d (list (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "0i6c12nmcvsf2xnq9nj38q9mvlx61xdz0i8lbx9p7kd6dykkagx4")))

(define-public crate-gameoflife-1.1.0 (c (n "gameoflife") (v "1.1.0") (d (list (d (n "crossterm") (r "^0.18") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1xfbhgi92043k3qgikkl3ykxq6w8jlsmixk89f9wlm122crrmkq8")))

(define-public crate-gameoflife-1.1.1 (c (n "gameoflife") (v "1.1.1") (d (list (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "0swzybffcyasn49cgn0izhmlwnqy5b37r4ycj2ri3hk0snfl9ric")))

(define-public crate-gameoflife-1.2.0 (c (n "gameoflife") (v "1.2.0") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "0kpbnhxznjm976hvych0dpps4qschjmn6vln50vj755pl618b19g")))

