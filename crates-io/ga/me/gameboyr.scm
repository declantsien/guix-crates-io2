(define-module (crates-io ga me gameboyr) #:use-module (crates-io))

(define-public crate-gameboyr-0.1.0 (c (n "gameboyr") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "minifb") (r "^0.23.0") (d #t) (k 0)))) (h "09zxq8cfsx6d3q5kg6sd1pi34abgzrd9m66zf78xiv8p377g198r")))

(define-public crate-gameboyr-0.1.1 (c (n "gameboyr") (v "0.1.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "minifb") (r "^0.23.0") (d #t) (k 0)))) (h "1ww0i2k1bl6gvisl4rajc0phbp63vq677mr15w3sd14gn11g5xhq")))

(define-public crate-gameboyr-0.1.2 (c (n "gameboyr") (v "0.1.2") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "minifb") (r "^0.23.0") (d #t) (k 0)))) (h "07b7dp10dl6wrlahajq0r1qkcpdxd75g71rfixk6149ph81jh0kc")))

(define-public crate-gameboyr-0.2.1 (c (n "gameboyr") (v "0.2.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "minifb") (r "^0.23.0") (d #t) (k 0)))) (h "0w7y219dp8fsbsrm2sa7adxsayh4jhkh0i0p5y1919j451s9f4c0")))

(define-public crate-gameboyr-0.2.2 (c (n "gameboyr") (v "0.2.2") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "minifb") (r "^0.23.0") (d #t) (k 0)))) (h "125mvrvkz2xl21i6zbksblm9gk5z8pknmaprkvca06549ni3ikyh")))

(define-public crate-gameboyr-0.2.3 (c (n "gameboyr") (v "0.2.3") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "minifb") (r "^0.23.0") (d #t) (k 0)))) (h "02ymlk9lrq78qx4f3mgnq6g01d2r15njjpj1y1r1h1b3y7292n9r")))

(define-public crate-gameboyr-0.2.4 (c (n "gameboyr") (v "0.2.4") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "minifb") (r "^0.23.0") (d #t) (k 0)))) (h "05pnn4w8fvp96qs8qslbfv5kwq98ikfbm2qry0j4y9rl1i5xf45c")))

(define-public crate-gameboyr-0.2.5 (c (n "gameboyr") (v "0.2.5") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "minifb") (r "^0.23.0") (d #t) (k 0)))) (h "0gjccf47fp9z9jxpdbawkx9mbslmr4mjwplalsqn3fdkw9qw5ifv")))

(define-public crate-gameboyr-0.2.6 (c (n "gameboyr") (v "0.2.6") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "minifb") (r "^0.23.0") (d #t) (k 0)))) (h "06w30jsd4dc06flakx2759g0p7pjb49h651r9n1yhnwnpymsn72f")))

(define-public crate-gameboyr-0.2.7 (c (n "gameboyr") (v "0.2.7") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "minifb") (r "^0.23.0") (d #t) (k 0)))) (h "073wg9n3vdqzqr2sl3rxz0i93mirh76wdwgpb1ji6fh92hzgqncw")))

(define-public crate-gameboyr-0.3.0 (c (n "gameboyr") (v "0.3.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "minifb") (r "^0.23.0") (d #t) (k 0)))) (h "1wpf85rhrd75awv8sg3mj5v1qd20jr7swdmxa1b8m345dhb6p04f")))

(define-public crate-gameboyr-0.3.1 (c (n "gameboyr") (v "0.3.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "minifb") (r "^0.23.0") (d #t) (k 0)))) (h "1i161c66lns99c17qccvgi23aivzkfz5rsf71vwhzr6zdhq0wxgm")))

(define-public crate-gameboyr-0.4.0 (c (n "gameboyr") (v "0.4.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "blip_buf") (r "^0.1.4") (d #t) (k 0)) (d (n "cpal") (r "^0.15.2") (d #t) (k 0)) (d (n "minifb") (r "^0.24.0") (d #t) (k 0)))) (h "09hp5447r7vz3n6r0152k6n2fsl2pdh97khv2ihka5i6kvz90jmq")))

