(define-module (crates-io ga me game-metrics-macro) #:use-module (crates-io))

(define-public crate-game-metrics-macro-0.0.1 (c (n "game-metrics-macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nsvshw7arvn0wbb2027b8ii0rf04wfcp8n0fw9ch75h448g41k8")))

(define-public crate-game-metrics-macro-0.0.3 (c (n "game-metrics-macro") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0y5ln3fm9sa0ly2v6cvzh18rd7jcj7fhgv0bgvhc03qkaq90jnpl")))

(define-public crate-game-metrics-macro-0.0.4 (c (n "game-metrics-macro") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15h6nzzgw3lyyl76jn8s16kqw6i6mhwg3snpa5x4rj4gw57xw1sg") (f (quote (("disable") ("default"))))))

