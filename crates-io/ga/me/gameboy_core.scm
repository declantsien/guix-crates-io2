(define-module (crates-io ga me gameboy_core) #:use-module (crates-io))

(define-public crate-gameboy_core-0.1.0 (c (n "gameboy_core") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "0nqx14ha0igrh5ay1m5v9hkrln3hsp7scxsr8v5sand5nmix43hj")))

(define-public crate-gameboy_core-0.1.1 (c (n "gameboy_core") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "1yc4hzzp95yvwfr63rdcb6ihwhvpx330nd8k8crar2lr3nr1vflw")))

(define-public crate-gameboy_core-0.1.2 (c (n "gameboy_core") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "0rn8b2ssb09c2x8ikmy3mnzxl00a4m4naqmp1037l3zyqdh3q5q5")))

(define-public crate-gameboy_core-0.1.3 (c (n "gameboy_core") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "176yfd6v81p9zdmsshibbs5h5vsqm5sinc71yx8az87w2h511vnh")))

(define-public crate-gameboy_core-0.1.4 (c (n "gameboy_core") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "11mswm2kldr29m5swri3sd9692apyyz93zigndc851d3nfqg1p63")))

(define-public crate-gameboy_core-0.1.6 (c (n "gameboy_core") (v "0.1.6") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "0mh89b7a764fznf6w19nlma346c7xkd5n2v0mm82sr8f1ynazkqa")))

(define-public crate-gameboy_core-0.1.7 (c (n "gameboy_core") (v "0.1.7") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "0x05jqijlcq9b1zg8f8d4jjq900fjh7wgmhjhqgx4iswh68z3mrp")))

(define-public crate-gameboy_core-0.1.8 (c (n "gameboy_core") (v "0.1.8") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "000la9yng8xn4xhc6vn8ac9nr34bzw9j5rrwi3h1h7gmdclqgzss")))

(define-public crate-gameboy_core-0.1.9 (c (n "gameboy_core") (v "0.1.9") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "0n7fp5gp1zqxzmsvii2y5rhcrfk7r1wk7wk78lxvhd4qvs4xn061")))

(define-public crate-gameboy_core-0.1.10 (c (n "gameboy_core") (v "0.1.10") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "0z8fyal5pzgq0l9c96p87xwf80f3br77pb369rx5cn7qxyc4nr1a")))

(define-public crate-gameboy_core-0.1.11 (c (n "gameboy_core") (v "0.1.11") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "0yaqd7114s8y7vjmkz8fiz0iij5mswh6z5c7d696bw569fszcna0")))

(define-public crate-gameboy_core-0.1.12 (c (n "gameboy_core") (v "0.1.12") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "10k7mdz33ghphn4j0qwy8aac6zr650vn0yczw6pp74gbkqcf0bls")))

(define-public crate-gameboy_core-0.1.13 (c (n "gameboy_core") (v "0.1.13") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "1qh9yfa6sdfyry8iaba6vwvm1mq2f2b1i70apfp88m726pk1wx02")))

(define-public crate-gameboy_core-0.1.14 (c (n "gameboy_core") (v "0.1.14") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)))) (h "16pnla9lh69xnx1sf6cgxl8d66vc3zjh32yp0xxr5myyp5xirj16")))

(define-public crate-gameboy_core-0.2.0 (c (n "gameboy_core") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "0zkmmnrycizn9k3qq61b6d31y84m86lvbad3kch8jd04la57xva3")))

(define-public crate-gameboy_core-0.2.1 (c (n "gameboy_core") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "06r6z78c6ygv9xf0jrj3l1rm80rfvprwgcn2c6aghy3k9lbbahv8")))

(define-public crate-gameboy_core-0.2.2 (c (n "gameboy_core") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "036ksp3k1a1ibrp0c56i7sqm8bl8sfaqvgchpi4nvskkmhzl9kxz")))

(define-public crate-gameboy_core-0.2.3 (c (n "gameboy_core") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "1z1yrbbzy00ig6rlqp742zx1kcnjvq5152kccd9jr6waxjx54jdv")))

(define-public crate-gameboy_core-0.2.4 (c (n "gameboy_core") (v "0.2.4") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "1gjjkqsjid4d3g82w6hwmfm6l974fzhpv3v2fxmnx03mx3mg52xh")))

(define-public crate-gameboy_core-0.2.5 (c (n "gameboy_core") (v "0.2.5") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "10f0dkbix34kjbrbhgq7h0z1iahl6i9fhi9c9ydf6hppmpg40mh0")))

(define-public crate-gameboy_core-0.2.6 (c (n "gameboy_core") (v "0.2.6") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "0rfhr6h05h6bnprx85xf2yx69ygdj0njv6jy3rl3zpvdyq1l3kn5")))

(define-public crate-gameboy_core-0.2.7 (c (n "gameboy_core") (v "0.2.7") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "1w39hlxzcyap1hpclsqxiw8j9v658xglh1lfqk4r2zqbpxgy0fwh")))

(define-public crate-gameboy_core-0.2.8 (c (n "gameboy_core") (v "0.2.8") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "0wq0riwpzcjlr221yadg60m7dzmmc5cy78q7x6n8gijwnw62y6ls")))

(define-public crate-gameboy_core-0.2.9 (c (n "gameboy_core") (v "0.2.9") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "14i91m2pzdv78s4cwiv4lmryzll0icjj1x1xvgphz58wrdbp65xz")))

(define-public crate-gameboy_core-0.3.0 (c (n "gameboy_core") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "0p745p3y3afqmpr9dh18wh3vc5s259bmq3ny90kh7gm44a3whhcr")))

(define-public crate-gameboy_core-0.3.1 (c (n "gameboy_core") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "1kv4v3m3gga3hv6bgjnpi52fvqy93y08mpahvxiqgpw2m2ksipzh")))

(define-public crate-gameboy_core-0.3.2 (c (n "gameboy_core") (v "0.3.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "0zbvygwp6zn3jm2gvfr59dby1n52a45k9nsn7whyzlbvgrlj6l9l")))

(define-public crate-gameboy_core-0.3.3 (c (n "gameboy_core") (v "0.3.3") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)))) (h "0jzwk6i1c00snz4qxb4sv0ijc15l79sjdw4s516b6psvwim4gmp7")))

