(define-module (crates-io ga me game_kernel_utils) #:use-module (crates-io))

(define-public crate-game_kernel_utils-0.1.0 (c (n "game_kernel_utils") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.17.0") (d #t) (k 0)) (d (n "evmap") (r "^10.0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6.4") (d #t) (k 0)))) (h "0sxx2xdfcrcf503iqwvfx2xpjrb1xyjsr51ndf6mb0f367fnw3nw")))

