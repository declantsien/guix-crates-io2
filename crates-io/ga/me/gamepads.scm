(define-module (crates-io ga me gamepads) #:use-module (crates-io))

(define-public crate-gamepads-0.1.0 (c (n "gamepads") (v "0.1.0") (d (list (d (n "gilrs") (r "^0.10") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)))) (h "0vxd3zhvxs14l8fhwlf4h6lszvydzdsp3phr1r5zk7cc82xrnmkc")))

(define-public crate-gamepads-0.1.1 (c (n "gamepads") (v "0.1.1") (d (list (d (n "gilrs") (r "^0.10") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)))) (h "1fwq125dh1v032hrhdmdhzl04xyplmdcn1082vcya33fapysykw7") (f (quote (("macroquad") ("default"))))))

(define-public crate-gamepads-0.1.2 (c (n "gamepads") (v "0.1.2") (d (list (d (n "gilrs") (r "^0.10") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)))) (h "0iszla29xcdpvg6gjfh54zcycc610dqpbhx19bwpp6m43kgq0r76") (f (quote (("macroquad") ("default"))))))

(define-public crate-gamepads-0.1.3 (c (n "gamepads") (v "0.1.3") (d (list (d (n "gilrs") (r "^0.10") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "js-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Gamepad" "GamepadButton" "GamepadMappingType" "Window" "Navigator"))) (o #t) (d #t) (k 0)))) (h "0m0d27r2jf5x53i6zygqly9agp173di5yy8dzrac4jm2ci098q2d") (f (quote (("macroquad") ("default")))) (s 2) (e (quote (("wasm-bindgen" "dep:wasm-bindgen" "web-sys" "js-sys"))))))

(define-public crate-gamepads-0.1.4 (c (n "gamepads") (v "0.1.4") (d (list (d (n "gilrs") (r "^0.10") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "js-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Gamepad" "GamepadButton" "GamepadMappingType" "Window" "Navigator"))) (o #t) (d #t) (k 0)))) (h "1cma4k7jii16lw4m991il918cdr175hizs1cvygvyw41gxril2f5") (f (quote (("macroquad") ("default" "wasm-bindgen")))) (s 2) (e (quote (("wasm-bindgen" "dep:wasm-bindgen" "web-sys" "js-sys"))))))

(define-public crate-gamepads-0.1.5 (c (n "gamepads") (v "0.1.5") (d (list (d (n "gilrs") (r "^0.10") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "js-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Gamepad" "GamepadButton" "GamepadMappingType" "Window" "Navigator"))) (o #t) (d #t) (k 0)))) (h "1dqa0nwmlshjfnx8sv0vds72m7hxcag48vs60inx0bplaviv45m9") (f (quote (("macroquad") ("default" "wasm-bindgen")))) (s 2) (e (quote (("wasm-bindgen" "dep:wasm-bindgen" "web-sys" "js-sys"))))))

(define-public crate-gamepads-0.1.6 (c (n "gamepads") (v "0.1.6") (d (list (d (n "gilrs") (r "^0.10") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "js-sys") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Gamepad" "GamepadButton" "GamepadMappingType" "Window" "Navigator"))) (o #t) (d #t) (k 0)))) (h "1klx24mxnnbnlmdzbn1c321d5lzjhg67vg6d1127b6f3zsfd220j") (f (quote (("default" "wasm-bindgen")))) (s 2) (e (quote (("wasm-bindgen" "dep:wasm-bindgen" "web-sys" "js-sys"))))))

