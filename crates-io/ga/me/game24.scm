(define-module (crates-io ga me game24) #:use-module (crates-io))

(define-public crate-game24-0.1.0 (c (n "game24") (v "0.1.0") (d (list (d (n "convert-base") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0d6v1cirjavrqdif3x6srlhcsgfpwyqcnjwnag5giwqkgnm1sv43")))

(define-public crate-game24-0.2.0 (c (n "game24") (v "0.2.0") (h "1ikrinh4z96annw5sxyaqmrimc7syv69qvxh3ch8y6ai79mwlfn2")))

(define-public crate-game24-0.2.1 (c (n "game24") (v "0.2.1") (h "1wxr1cnzqvirc7cnqj12xcdycxv5scdsddnmbrmr09337fckdws0")))

(define-public crate-game24-0.3.0 (c (n "game24") (v "0.3.0") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "math-ast") (r "^0.2") (d #t) (k 0)))) (h "0dlf150q9ar85qnph6d8xhkri5al2fhsnllqw4lj1ym4riapqff4")))

(define-public crate-game24-0.3.1 (c (n "game24") (v "0.3.1") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "math-ast") (r "^0.2") (d #t) (k 0)))) (h "10ysgvd4b4wwx5w9q7likg5f47qcf4fnl839ylklj2q26hj8rqng")))

(define-public crate-game24-0.3.2 (c (n "game24") (v "0.3.2") (d (list (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "math-ast") (r "^0.2") (d #t) (k 0)))) (h "0ghn1j18fi9v0kncvji34wi28xhzisnb5dhzi5a3mkgakg76vlp8")))

