(define-module (crates-io ga me gamercade_rs) #:use-module (crates-io))

(define-public crate-gamercade_rs-0.1.0 (c (n "gamercade_rs") (v "0.1.0") (d (list (d (n "gamercade_core") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)))) (h "1l1w87jchjq94g66ikv8qdh1gaca86j3ivwlyvfqbl88bfnzirlc") (y #t)))

(define-public crate-gamercade_rs-0.0.1 (c (n "gamercade_rs") (v "0.0.1") (d (list (d (n "gamercade_core") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)))) (h "1sq5a2rlw1n3lazv7m0gynqs02zi8065clyqb8kcsi4pda5ywpp2")))

(define-public crate-gamercade_rs-0.1.1 (c (n "gamercade_rs") (v "0.1.1") (d (list (d (n "gamercade_core") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)))) (h "0ywp0b39grb5mlmz71x2siyc6hb8v3yvmznj5pl2zilcb650cz45")))

(define-public crate-gamercade_rs-0.1.2 (c (n "gamercade_rs") (v "0.1.2") (d (list (d (n "paste") (r "^1.0.8") (d #t) (k 0)))) (h "1gav176iw1px2lnk455bg340nj1c0n1rg3qkz0zjh2cs653ymgsx")))

