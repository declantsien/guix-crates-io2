(define-module (crates-io ga me game_template) #:use-module (crates-io))

(define-public crate-game_template-0.0.0 (c (n "game_template") (v "0.0.0") (h "1n5yfplrcjg705vhmdw164f213b9szgy6a97nxzvlqb5mrm2dnvb")))

(define-public crate-game_template-0.0.1 (c (n "game_template") (v "0.0.1") (h "1rzdccgl3fiaw9gcsibbk2ddw7x3lcp4yxglrpv61vcda74m7v96")))

