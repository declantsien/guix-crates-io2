(define-module (crates-io ga me gameai) #:use-module (crates-io))

(define-public crate-gameai-0.0.0 (c (n "gameai") (v "0.0.0") (d (list (d (n "clap") (r ">= 2.0.0") (d #t) (k 0)) (d (n "cpuprofiler") (r ">= 0.0.3") (d #t) (k 0)) (d (n "rand") (r ">= 0.3.0") (d #t) (k 0)) (d (n "rayon") (r ">= 0.8.0") (d #t) (k 0)))) (h "1z40jpv5d1yf60d7ah4f2bzjmd17wlsaiw59f2hi9jvnp6ggi1qk")))

(define-public crate-gameai-0.0.1 (c (n "gameai") (v "0.0.1") (d (list (d (n "clap") (r ">= 2.0.0") (d #t) (k 0)) (d (n "cpuprofiler") (r ">= 0.0.3") (d #t) (k 0)) (d (n "rand") (r ">= 0.3.0") (d #t) (k 0)) (d (n "rayon") (r ">= 0.8.0") (d #t) (k 0)))) (h "1528livv9a15612d2icmsrjmy72n3n4nf4s2wb6s0agqx6n118f5")))

(define-public crate-gameai-0.0.2 (c (n "gameai") (v "0.0.2") (d (list (d (n "clap") (r ">= 2.0.0") (d #t) (k 0)) (d (n "cpuprofiler") (r ">= 0.0.3") (d #t) (k 0)) (d (n "rand") (r ">= 0.3.0") (d #t) (k 0)) (d (n "rayon") (r ">= 0.8.0") (d #t) (k 0)))) (h "0rixcxrgjcym7kw3fv0yz7mjm8q27n2gi13xd0zh5kvvvz3b3xw6")))

