(define-module (crates-io ga me gameproxy) #:use-module (crates-io))

(define-public crate-gameproxy-0.1.0 (c (n "gameproxy") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1rf6kysz56rfayfl4l7vl4h3nf4ywx165xdby9qw5dxzb01sj7fi")))

(define-public crate-gameproxy-0.1.1 (c (n "gameproxy") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1zp938lw9i19n0jj6xv20ry8zxyn6alj94mzp20563bhdd2n3p5h")))

