(define-module (crates-io ga me gameloop-timing) #:use-module (crates-io))

(define-public crate-gameloop-timing-0.1.0 (c (n "gameloop-timing") (v "0.1.0") (h "1qhiwskwfz1sj5k2ci5vr4y8llfn4qw3fdy08d212qlxz3w7nris")))

(define-public crate-gameloop-timing-0.2.0 (c (n "gameloop-timing") (v "0.2.0") (h "0bdl8g3mi31r6mzs9a2l0629jc2lcgkziy5f1pxccmizpisqz04b")))

