(define-module (crates-io ga me game_kernel_ecs) #:use-module (crates-io))

(define-public crate-game_kernel_ecs-0.1.0 (c (n "game_kernel_ecs") (v "0.1.0") (d (list (d (n "crossbeam-utils") (r "^0.5.0") (d #t) (k 0)) (d (n "downcast-rs") (r "^1.1.1") (d #t) (k 0)) (d (n "evmap") (r "^9.0.1") (d #t) (k 0)) (d (n "game_kernel_settings") (r "^0.1.0") (d #t) (k 0)) (d (n "game_kernel_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "hibitset") (r "^0.6.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.6.4") (d #t) (k 0)) (d (n "paste") (r "^0.1.17") (d #t) (k 0)))) (h "0k32ygb7jzqivax788n01v8w9hdkprxc42b9l1aqk3j3fx6218j0")))

