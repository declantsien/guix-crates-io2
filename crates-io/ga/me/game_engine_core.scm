(define-module (crates-io ga me game_engine_core) #:use-module (crates-io))

(define-public crate-game_engine_core-1.0.0 (c (n "game_engine_core") (v "1.0.0") (d (list (d (n "game_clock") (r "^1.0.0") (d #t) (k 0)) (d (n "game_state_machine") (r "^1.0.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.0.0") (d #t) (k 0)))) (h "01rxxbvxwhfzjryjwxwgxp8m9zajkhanf41jg3a6nylid2dm3vh6")))

(define-public crate-game_engine_core-1.1.0 (c (n "game_engine_core") (v "1.1.0") (d (list (d (n "game_clock") (r "^1.1.0") (d #t) (k 0)) (d (n "game_state_machine") (r "^1.0.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.0.0") (d #t) (k 0)))) (h "1bz0qpzql92jwzyghy08i3yqpbn84sm9smkc5bviwmzbslr0v65g")))

(define-public crate-game_engine_core-1.1.1 (c (n "game_engine_core") (v "1.1.1") (d (list (d (n "game_clock") (r "^1.1.0") (d #t) (k 0)) (d (n "game_state_machine") (r "^1.0.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.0.0") (d #t) (k 0)))) (h "1syq37g2i251r9xaxb8rzdqx1m9bir1zxwd019882x9bwlkbca7y")))

(define-public crate-game_engine_core-1.1.2 (c (n "game_engine_core") (v "1.1.2") (d (list (d (n "game_clock") (r "^1.1.0") (d #t) (k 0)) (d (n "game_state_machine") (r "^1.0.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.0.0") (d #t) (k 0)))) (h "1hd35zfdbl6i2ngni2lmbhf0gq67dlk3bim74kc1wpixlfprcm02")))

