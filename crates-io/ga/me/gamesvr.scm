(define-module (crates-io ga me gamesvr) #:use-module (crates-io))

(define-public crate-gamesvr-0.1.0 (c (n "gamesvr") (v "0.1.0") (d (list (d (n "common") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0qlzppg1chcnxbacik65yfdlpa91xqc4igvpq4p35pxnv8zdb324")))

(define-public crate-gamesvr-0.1.1 (c (n "gamesvr") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "stormcommon") (r "^0.1.0") (d #t) (k 0)))) (h "1bfgr3xd8n7473l3m0jlq9l6ycz5dpqm4811f7xyqwrw36flcf9r")))

(define-public crate-gamesvr-0.1.2 (c (n "gamesvr") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "stormcommon") (r "^0.1.0") (d #t) (k 0)))) (h "06lydkqimcz1vlnhyy5kypg1yb1vcxh6ysx9703gfrvnqy1nsl2y")))

