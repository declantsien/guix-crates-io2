(define-module (crates-io ga un gaunt) #:use-module (crates-io))

(define-public crate-gaunt-0.0.0 (c (n "gaunt") (v "0.0.0") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)))) (h "1qarhzi9i3gd34w3pi4sh6g2a370wk8ccvag4a1bgjqbjjasha6l") (y #t)))

(define-public crate-gaunt-0.0.1 (c (n "gaunt") (v "0.0.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0rpckgbdmqhs2zbyqa3haqy9zq2196vxq39bw50dmqjv7609n7pf") (y #t)))

(define-public crate-gaunt-0.1.0 (c (n "gaunt") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "slog") (r "^2") (o #t) (d #t) (k 0)))) (h "1dlq68a88qqazvp3gc6pn8zmm7k8gh9kymyf2vm120ykzfsx6j03") (f (quote (("std" "alloc" "failure/std") ("logger" "slog" "std") ("default" "std") ("alloc")))) (y #t)))

