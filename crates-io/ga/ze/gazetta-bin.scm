(define-module (crates-io ga ze gazetta-bin) #:use-module (crates-io))

(define-public crate-gazetta-bin-0.1.1 (c (n "gazetta-bin") (v "0.1.1") (d (list (d (n "gazetta") (r "^0.1") (d #t) (k 0)) (d (n "horrorshow") (r "^0.5.4") (d #t) (k 0)))) (h "1sqxv602iv2hd376xybzj926fvj494819zg6ann4i4iijyl99zlc") (y #t)))

(define-public crate-gazetta-bin-0.1.2 (c (n "gazetta-bin") (v "0.1.2") (d (list (d (n "gazetta") (r "^0.1") (d #t) (k 0)) (d (n "horrorshow") (r "^0.5") (d #t) (k 0)))) (h "06ibnjly4df68gw27j63nbdshxzaknhwf7l50mg37qwwfqm3ydzj")))

(define-public crate-gazetta-bin-0.1.3 (c (n "gazetta-bin") (v "0.1.3") (d (list (d (n "gazetta") (r "^0.1") (d #t) (k 0)) (d (n "horrorshow") (r "^0.5") (d #t) (k 0)))) (h "15ip3x4ax4jfyn2y6w10rn82wqb130q6a081gl996sn1lrwjlid5")))

(define-public crate-gazetta-bin-0.1.4 (c (n "gazetta-bin") (v "0.1.4") (d (list (d (n "gazetta") (r "^0.1") (d #t) (k 0)) (d (n "horrorshow") (r "^0.5") (d #t) (k 0)))) (h "0bca446jvx4qdz7gcxqw1qzjf7z84m58pc1iagx1ykss6j3qqkkv")))

(define-public crate-gazetta-bin-0.1.5 (c (n "gazetta-bin") (v "0.1.5") (d (list (d (n "gazetta") (r "^0.1.2") (d #t) (k 0)) (d (n "gazetta") (r "^0.1.2") (d #t) (k 1)) (d (n "horrorshow") (r "^0.5") (d #t) (k 0)))) (h "17hczlzvq943ma62wh84kdznl22bz5zlbimqvfnc5zm5kqk8r29w")))

(define-public crate-gazetta-bin-0.1.6 (c (n "gazetta-bin") (v "0.1.6") (d (list (d (n "gazetta") (r "^0.1.2") (d #t) (k 0)) (d (n "gazetta") (r "^0.1.2") (d #t) (k 1)) (d (n "horrorshow") (r "^0.5") (d #t) (k 0)))) (h "17r2lnbjd1p31qgxj4v765hmycmv36pvkb3xrw0sgn39pm54dy53")))

(define-public crate-gazetta-bin-0.2.0 (c (n "gazetta-bin") (v "0.2.0") (d (list (d (n "gazetta") (r "^0.2") (d #t) (k 0)) (d (n "gazetta") (r "^0.2") (d #t) (k 1)) (d (n "horrorshow") (r "^0.6") (d #t) (k 0)))) (h "1xyi5bmfs3anr8nwc6iygw3s4k77n3glsi8xbpf3zshhgng96vy6")))

(define-public crate-gazetta-bin-0.2.1 (c (n "gazetta-bin") (v "0.2.1") (d (list (d (n "gazetta") (r "^0.2.1") (d #t) (k 0)) (d (n "gazetta") (r "^0.2.1") (d #t) (k 1)) (d (n "horrorshow") (r "^0.6") (d #t) (k 0)))) (h "0wlpjahffycy3i13mrvja6978lq9sx308mm9q5firka6pkv75dbi")))

(define-public crate-gazetta-bin-0.2.2 (c (n "gazetta-bin") (v "0.2.2") (d (list (d (n "gazetta") (r "^0.2.1") (d #t) (k 0)) (d (n "gazetta") (r "^0.2.1") (d #t) (k 1)) (d (n "horrorshow") (r "^0.6") (d #t) (k 0)))) (h "0ixp5amqsx1ny2xv4bpi22w6rrrp5w51n25aqfwfl670xw4axzn8")))

(define-public crate-gazetta-bin-0.3.0 (c (n "gazetta-bin") (v "0.3.0") (d (list (d (n "gazetta") (r "^0.3.0") (d #t) (k 0)) (d (n "gazetta") (r "^0.3.0") (d #t) (k 1)) (d (n "horrorshow") (r "^0.7") (d #t) (k 0)))) (h "1wk1winy31vq054182y6rizwzp60qbyl8yn72f0j4vv6349739p0")))

