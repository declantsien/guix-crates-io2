(define-module (crates-io ga ze gazebo_lint) #:use-module (crates-io))

(define-public crate-gazebo_lint-0.1.0 (c (n "gazebo_lint") (v "0.1.0") (d (list (d (n "smallvec") (r "^1.4") (d #t) (k 0)))) (h "1vll654md0j8qhdj4s63kqklyihhmhn6p5iaiwriff9qvnqh3vyw")))

(define-public crate-gazebo_lint-0.1.1 (c (n "gazebo_lint") (v "0.1.1") (d (list (d (n "smallvec") (r "^1.4") (d #t) (k 0)))) (h "138m4cvspkjc9hfc7h0sd2qcynajh6pnh8axdpc4bhvnralkd1a8")))

