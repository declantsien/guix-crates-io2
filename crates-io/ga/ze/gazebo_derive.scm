(define-module (crates-io ga ze gazebo_derive) #:use-module (crates-io))

(define-public crate-gazebo_derive-0.1.0 (c (n "gazebo_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (d #t) (k 0)))) (h "0n05m8ahp7d2czwnani71d9yk5s9bcf698grnfci7611srf7i79c")))

(define-public crate-gazebo_derive-0.1.1 (c (n "gazebo_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (d #t) (k 0)))) (h "0qrciyc20zd5pkvxldxwswznj6zsa2frzajhkrwyf2r98kvqnynn")))

(define-public crate-gazebo_derive-0.4.0 (c (n "gazebo_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "13alz23v8nsyj2nm7xnm6zsz595d0jw0jqbzray6ydfr9m3r0hsw")))

(define-public crate-gazebo_derive-0.4.1 (c (n "gazebo_derive") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1cdszxznrwhrbg5gv5x88xkrbmwfj5xnqpnc1a7fbbf8vx3cbjss")))

(define-public crate-gazebo_derive-0.5.0 (c (n "gazebo_derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0cm4dfzghly9qkg1172hb0iwzsy4axv7sj1p3dw5asp0k8kkh85a")))

(define-public crate-gazebo_derive-0.6.0 (c (n "gazebo_derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0723gi2g518bcs5dg0vgaxjm0yzwvkvsijwcmrvmawim2740lxdn")))

(define-public crate-gazebo_derive-0.7.0 (c (n "gazebo_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "018jqvy2bfyr9lgz2vzn944bql9aw1yhv4221qg612gla22jh4cp")))

(define-public crate-gazebo_derive-0.7.1 (c (n "gazebo_derive") (v "0.7.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1ccq2hxv396ym0kzxgsiapkvjanjmr7b787jkibkrf54h1kx2jxw")))

(define-public crate-gazebo_derive-0.8.0 (c (n "gazebo_derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.27") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0rvjxbypanzbw4fj70738k5rg0cfzdbrg7skghi9rnz10rrfkbx4")))

