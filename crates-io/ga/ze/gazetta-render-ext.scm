(define-module (crates-io ga ze gazetta-render-ext) #:use-module (crates-io))

(define-public crate-gazetta-render-ext-0.1.0 (c (n "gazetta-render-ext") (v "0.1.0") (d (list (d (n "gazetta-core") (r "^0.1") (d #t) (k 0)) (d (n "horrorshow") (r "^0.5") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.3") (d #t) (k 0)))) (h "12wal70wz7cmiiqs8pmwyq72hpzr30vd7d5dwc93f9rhb4lh9y63")))

(define-public crate-gazetta-render-ext-0.1.1 (c (n "gazetta-render-ext") (v "0.1.1") (d (list (d (n "gazetta-core") (r "^0.1") (d #t) (k 0)) (d (n "horrorshow") (r "^0.5") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)))) (h "06x7dnrqd84xacqvcmagic8vp1wlr5p4fpiml9yp5d7z2j9g4zl8")))

(define-public crate-gazetta-render-ext-0.1.2 (c (n "gazetta-render-ext") (v "0.1.2") (d (list (d (n "gazetta-core") (r "^0.1") (d #t) (k 0)) (d (n "horrorshow") (r "^0.5") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)))) (h "0f8m1z2g0lfqip9c8qkc50bfagpz4ifwhnwa4gnb7c226vgyv2la")))

(define-public crate-gazetta-render-ext-0.1.3 (c (n "gazetta-render-ext") (v "0.1.3") (d (list (d (n "gazetta-core") (r "^0.1") (d #t) (k 0)) (d (n "horrorshow") (r "^0.5") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)))) (h "0hz9swb1bb5xv4skjhqqj39gq92f4b1d5z3rxw9das0clrvin47v")))

(define-public crate-gazetta-render-ext-0.1.4 (c (n "gazetta-render-ext") (v "0.1.4") (d (list (d (n "gazetta-core") (r "^0.1") (d #t) (k 0)) (d (n "horrorshow") (r "^0.5") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)))) (h "16qs4pm073h658aqm91wrldr64yw44k2is16l6p6ykxrx7m9756f")))

(define-public crate-gazetta-render-ext-0.2.0 (c (n "gazetta-render-ext") (v "0.2.0") (d (list (d (n "gazetta-core") (r "^0.2") (d #t) (k 0)) (d (n "horrorshow") (r "^0.6") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)))) (h "0cncxdznv1080ysf0bb042jgz3w6xfs68v4rph7maph41wv358z4")))

(define-public crate-gazetta-render-ext-0.2.1 (c (n "gazetta-render-ext") (v "0.2.1") (d (list (d (n "gazetta-core") (r "^0.2.1") (d #t) (k 0)) (d (n "horrorshow") (r "^0.6") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1.2") (d #t) (k 0)))) (h "1smc2m36d9v3qaml9169sy97zg90jgvjnm0v01y76a4pz4viqpv2")))

(define-public crate-gazetta-render-ext-0.3.0 (c (n "gazetta-render-ext") (v "0.3.0") (d (list (d (n "gazetta-core") (r "^0.3.0") (d #t) (k 0)) (d (n "horrorshow") (r "^0.7") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.6.0") (d #t) (k 0)))) (h "027kia2mns8jzyaaywr50kbyxmx46pmakr6w8mm1p264mcp06h3z")))

