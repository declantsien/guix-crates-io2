(define-module (crates-io ga ze gazetta-cli) #:use-module (crates-io))

(define-public crate-gazetta-cli-0.1.0 (c (n "gazetta-cli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^1") (d #t) (k 0)) (d (n "gazetta-core") (r "^0.1") (d #t) (k 0)) (d (n "horrorshow") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "slug") (r "^0.1") (d #t) (k 0)))) (h "0akmf1q6lscmfsvf51g73958iza3xl3svl7xvxlpqnhqp1px3jcf")))

(define-public crate-gazetta-cli-0.1.1 (c (n "gazetta-cli") (v "0.1.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "gazetta-core") (r "^0.1") (d #t) (k 0)) (d (n "horrorshow") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "slug") (r "^0.1") (d #t) (k 0)))) (h "1xqa6ws5i7jn6l9sj8bf41pljg3v96ph2dbbpivfsih8d1j9wspa")))

(define-public crate-gazetta-cli-0.1.2 (c (n "gazetta-cli") (v "0.1.2") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "gazetta-core") (r "^0.1") (d #t) (k 0)) (d (n "horrorshow") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "slug") (r "^0.1") (d #t) (k 0)))) (h "1xjajphqrgdbiw75lcrwizpji9lkwwwj205jb7qfaan8jwf030ix")))

(define-public crate-gazetta-cli-0.1.3 (c (n "gazetta-cli") (v "0.1.3") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.9") (d #t) (k 0)) (d (n "gazetta-core") (r "^0.1") (d #t) (k 0)) (d (n "horrorshow") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "slug") (r "^0.1") (d #t) (k 0)))) (h "0khyba36zapfami4hf8sfdzc9l6kskcrxsxrclpsckx6smvyrxcm")))

(define-public crate-gazetta-cli-0.2.0 (c (n "gazetta-cli") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.9") (d #t) (k 0)) (d (n "gazetta-core") (r "^0.2") (d #t) (k 0)) (d (n "horrorshow") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "slug") (r "^0.1") (d #t) (k 0)))) (h "1i3msx3nnqxsdqdgff9p05nv9fvyz4rlvy969f8a95qcnz8d0d05")))

(define-public crate-gazetta-cli-0.2.1 (c (n "gazetta-cli") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.9") (d #t) (k 0)) (d (n "gazetta-core") (r "^0.2.1") (d #t) (k 0)) (d (n "horrorshow") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "slug") (r "^0.1") (d #t) (k 0)))) (h "1rxw8mfr2fpha685klb3m7n1ldmrnmvzh7ixb5g4m40ixpz2aw1b")))

(define-public crate-gazetta-cli-0.3.0 (c (n "gazetta-cli") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.9") (d #t) (k 0)) (d (n "gazetta-core") (r "^0.3.0") (d #t) (k 0)) (d (n "horrorshow") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "slug") (r "^0.1") (d #t) (k 0)))) (h "1i0a43fyvj9r07qipcqrqllm7vz9v1swdlh43plilcwdkxd273xg")))

