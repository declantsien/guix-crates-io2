(define-module (crates-io ga ze gazetta-model-ext) #:use-module (crates-io))

(define-public crate-gazetta-model-ext-0.1.0 (c (n "gazetta-model-ext") (v "0.1.0") (d (list (d (n "gazetta-core") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)))) (h "177vy9pz9p0i5f88qrgczqir1r97zzvipfc2g4h51jwxwsq9aqkk")))

(define-public crate-gazetta-model-ext-0.1.1 (c (n "gazetta-model-ext") (v "0.1.1") (d (list (d (n "gazetta-core") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "085aiyl33yh3hp11dwk2q50mh5z4dyzlh3gdrr2lf14hcrbj6syr")))

(define-public crate-gazetta-model-ext-0.2.0 (c (n "gazetta-model-ext") (v "0.2.0") (d (list (d (n "gazetta-core") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "0dbab8rdsv6x525bw5glrlikwiqzpgdm4qzs3pgp2kzsdy4yps0g")))

(define-public crate-gazetta-model-ext-0.2.1 (c (n "gazetta-model-ext") (v "0.2.1") (d (list (d (n "gazetta-core") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "01rpjmngrmrld7vzxafj8krhf4ranvndmp38jvlv9h5lm3p5wi2n")))

(define-public crate-gazetta-model-ext-0.3.0 (c (n "gazetta-model-ext") (v "0.3.0") (d (list (d (n "gazetta-core") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "0d4syiini7bgsghpcgc1if0ml1ayzgiam6caya5dwvc530ci63i3")))

