(define-module (crates-io ga ze gazetta) #:use-module (crates-io))

(define-public crate-gazetta-0.1.0 (c (n "gazetta") (v "0.1.0") (d (list (d (n "gazetta-cli") (r "^0.1") (d #t) (k 0)) (d (n "gazetta-core") (r "^0.1") (d #t) (k 0)) (d (n "gazetta-model-ext") (r "^0.1") (d #t) (k 0)) (d (n "gazetta-render-ext") (r "^0.1") (d #t) (k 0)))) (h "0p0qwnkxays5gnzjhda36a64qwcd976nasjkp21wbyavk3aggp9v")))

(define-public crate-gazetta-0.1.1 (c (n "gazetta") (v "0.1.1") (d (list (d (n "gazetta-cli") (r "^0.1") (d #t) (k 0)) (d (n "gazetta-core") (r "^0.1") (d #t) (k 0)) (d (n "gazetta-model-ext") (r "^0.1") (d #t) (k 0)) (d (n "gazetta-render-ext") (r "^0.1") (d #t) (k 0)))) (h "1mq20amh6pskav9ivcm36zvh9i21pv8886w1cr3a50yasfhd0zwg")))

(define-public crate-gazetta-0.1.2 (c (n "gazetta") (v "0.1.2") (d (list (d (n "gazetta-cli") (r "^0.1.3") (d #t) (k 0)) (d (n "gazetta-core") (r "^0.1") (d #t) (k 0)) (d (n "gazetta-model-ext") (r "^0.1") (d #t) (k 0)) (d (n "gazetta-render-ext") (r "^0.1") (d #t) (k 0)))) (h "0ivzscf9qyj4bvsmibzg9l2wgqdg02vzs1qrpjk62xv5cn0arbqc")))

(define-public crate-gazetta-0.1.3 (c (n "gazetta") (v "0.1.3") (d (list (d (n "gazetta-cli") (r "^0.1.3") (d #t) (k 0)) (d (n "gazetta-core") (r "^0.1") (d #t) (k 0)) (d (n "gazetta-model-ext") (r "^0.1") (d #t) (k 0)) (d (n "gazetta-render-ext") (r "^0.1") (d #t) (k 0)))) (h "0vdadx1zx2pq7ck245lnkbam41b00mqb3v0zp4rgp9f512slqkwz")))

(define-public crate-gazetta-0.2.0 (c (n "gazetta") (v "0.2.0") (d (list (d (n "gazetta-cli") (r "^0.2") (d #t) (k 0)) (d (n "gazetta-core") (r "^0.2") (d #t) (k 0)) (d (n "gazetta-model-ext") (r "^0.2") (d #t) (k 0)) (d (n "gazetta-render-ext") (r "^0.2") (d #t) (k 0)))) (h "0563yh5lxzrh87m6m6hj6lvgm6hd0rwhvz81fc58rzimhblagm4v")))

(define-public crate-gazetta-0.2.1 (c (n "gazetta") (v "0.2.1") (d (list (d (n "gazetta-cli") (r "^0.2.1") (d #t) (k 0)) (d (n "gazetta-core") (r "^0.2.1") (d #t) (k 0)) (d (n "gazetta-model-ext") (r "^0.2.1") (d #t) (k 0)) (d (n "gazetta-render-ext") (r "^0.2.1") (d #t) (k 0)))) (h "0bb62zwr6m35054f2ja57mfzq11arfydlb4hc4dl5k744yxlc849")))

(define-public crate-gazetta-0.3.0 (c (n "gazetta") (v "0.3.0") (d (list (d (n "gazetta-cli") (r "^0.3.0") (d #t) (k 0)) (d (n "gazetta-core") (r "^0.3.0") (d #t) (k 0)) (d (n "gazetta-model-ext") (r "^0.3.0") (d #t) (k 0)) (d (n "gazetta-render-ext") (r "^0.3.0") (d #t) (k 0)))) (h "05yh12x2dxmvp319f3x8acm02nigdf10isya4bxkdl5gfi18bzmp")))

