(define-module (crates-io ga ze gaze) #:use-module (crates-io))

(define-public crate-gaze-0.1.0 (c (n "gaze") (v "0.1.0") (h "1yf283pfh7pqyf118kmyp78v02vkq0j8bw9r4kcwb8f27m0dgmnh")))

(define-public crate-gaze-0.2.0 (c (n "gaze") (v "0.2.0") (d (list (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "0jkdfg0sj2gxg0f1qmm07470b5hi63wddvk3j6sziwdibifwkigk")))

(define-public crate-gaze-0.3.0 (c (n "gaze") (v "0.3.0") (d (list (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "1casva6qs0y416wqra40n6skiyd4w6xsxz62ccaf9nz1kplcmzkv")))

(define-public crate-gaze-0.4.0 (c (n "gaze") (v "0.4.0") (d (list (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "045c7lagk5mh85564w9j2n45bpj5p4wrdlyxbvqc0114pvd1jh6j")))

(define-public crate-gaze-0.5.0 (c (n "gaze") (v "0.5.0") (d (list (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0pwd39lmvgfsr89yvnj0xam6qihin3s9zyczcas0niyjnxs6r3sf")))

