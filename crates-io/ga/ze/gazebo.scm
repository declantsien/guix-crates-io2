(define-module (crates-io ga ze gazebo) #:use-module (crates-io))

(define-public crate-gazebo-0.1.0 (c (n "gazebo") (v "0.1.0") (d (list (d (n "gazebo_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0pdiipl1izph2blbpmbd4lwqs983kz9b00rcahz79pd5k95ny611") (f (quote (("str_pattern_extensions"))))))

(define-public crate-gazebo-0.2.0 (c (n "gazebo") (v "0.2.0") (d (list (d (n "gazebo_derive") (r "^0.1.1") (d #t) (k 0)))) (h "1s2njv6k3jbb99ifdhgbm1w9zw260p08rjkplvsi8835wii1gfb9") (f (quote (("str_pattern_extensions"))))))

(define-public crate-gazebo-0.2.1 (c (n "gazebo") (v "0.2.1") (d (list (d (n "gazebo_derive") (r "^0.1.1") (d #t) (k 0)))) (h "1pajj229kzr5z3v7mranhzsjspcgiijfvz3cb4ar767l192r3r04") (f (quote (("str_pattern_extensions"))))))

(define-public crate-gazebo-0.2.2 (c (n "gazebo") (v "0.2.2") (d (list (d (n "gazebo_derive") (r "^0.1.1") (d #t) (k 0)))) (h "0kxkbszhp6dwzr8w9yycl9ws839w6v0zijbrx7q08mkq3siawjnh") (f (quote (("str_pattern_extensions"))))))

(define-public crate-gazebo-0.3.0 (c (n "gazebo") (v "0.3.0") (d (list (d (n "gazebo_derive") (r "^0.1.1") (d #t) (k 0)))) (h "0in7m5b2c1bzmmxfg0k9rv1mw7g7f4ba9kx1f04d56yn57n9vbdd") (f (quote (("str_pattern_extensions"))))))

(define-public crate-gazebo-0.3.1 (c (n "gazebo") (v "0.3.1") (d (list (d (n "gazebo_derive") (r "^0.1.1") (d #t) (k 0)))) (h "0x5lhxm3fjmny9wpnww71zlsaga8mch65dxdcz20bgn38cdfx10a") (f (quote (("str_pattern_extensions"))))))

(define-public crate-gazebo-0.3.2 (c (n "gazebo") (v "0.3.2") (d (list (d (n "gazebo_derive") (r "^0.1.1") (d #t) (k 0)))) (h "1fhwsw6b115g6vz5bi4zn94rz6rmk63y99kmciimwij1sxhs994i") (f (quote (("str_pattern_extensions"))))))

(define-public crate-gazebo-0.3.3 (c (n "gazebo") (v "0.3.3") (d (list (d (n "gazebo_derive") (r "^0.1.1") (d #t) (k 0)))) (h "1v1kpn1fddskld0zw6vnfkzxbnnlqb0035znybasrck381rnh3ad") (f (quote (("str_pattern_extensions"))))))

(define-public crate-gazebo-0.4.0 (c (n "gazebo") (v "0.4.0") (d (list (d (n "gazebo_derive") (r "^0.4.0") (d #t) (k 0)))) (h "0i9fx4ga8gsj13ji5hricr9xkkc2b42dxc3w1x0j7vqmq02zaan3") (f (quote (("str_pattern_extensions"))))))

(define-public crate-gazebo-0.4.1 (c (n "gazebo") (v "0.4.1") (d (list (d (n "gazebo_derive") (r "^0.4.1") (d #t) (k 0)))) (h "1sqnfq0h3zx5wplvicb49aa5ki3wrz5rhp41vj47pdwrd6baz81n") (f (quote (("str_pattern_extensions"))))))

(define-public crate-gazebo-0.4.2 (c (n "gazebo") (v "0.4.2") (d (list (d (n "gazebo_derive") (r "^0.4.1") (d #t) (k 0)))) (h "1x4i3wk2w53xnnplzvm6b3qaphyrv29g3fcs6dvqhiqv3qxpclqm") (f (quote (("str_pattern_extensions"))))))

(define-public crate-gazebo-0.4.3 (c (n "gazebo") (v "0.4.3") (d (list (d (n "gazebo_derive") (r "^0.4.1") (d #t) (k 0)))) (h "136d4diy3n3hx2vj782bcq88r4csigjvhvcw3zdgr7nvw4ssk9gz") (f (quote (("str_pattern_extensions"))))))

(define-public crate-gazebo-0.4.4 (c (n "gazebo") (v "0.4.4") (d (list (d (n "gazebo_derive") (r "^0.4.1") (d #t) (k 0)))) (h "10hk8r5krbm1q0b42km1rfj3dd1b5h7vavrim23xy7nf4yfkb11z") (f (quote (("str_pattern_extensions"))))))

(define-public crate-gazebo-0.5.0 (c (n "gazebo") (v "0.5.0") (d (list (d (n "gazebo_derive") (r "^0.5.0") (d #t) (k 0)))) (h "0snhar8a5z725r0fqznc73klwprwzsavk2jrpdxr5wkqrdixy3rh") (f (quote (("str_pattern_extensions"))))))

(define-public crate-gazebo-0.6.0 (c (n "gazebo") (v "0.6.0") (d (list (d (n "gazebo_derive") (r "^0.6.0") (d #t) (k 0)))) (h "17s09bqixzhj89z2g7nq0cnj9gbixkwmbfl88nx4cn26xhg1pv5h") (f (quote (("str_pattern_extensions"))))))

(define-public crate-gazebo-0.7.0 (c (n "gazebo") (v "0.7.0") (d (list (d (n "gazebo_derive") (r "^0.7.0") (d #t) (k 0)))) (h "0556p48872dvmxi962mh4lfj2ffcn9n0xv4skr7hkkqda2fb7bi6") (f (quote (("str_pattern_extensions"))))))

(define-public crate-gazebo-0.7.1 (c (n "gazebo") (v "0.7.1") (d (list (d (n "gazebo_derive") (r "^0.7.1") (d #t) (k 0)))) (h "0jqj2fis8af15p35a7zsp7wprdrd32vv46giwlrn2hfsvalb8gmd") (f (quote (("str_pattern_extensions"))))))

(define-public crate-gazebo-0.8.0 (c (n "gazebo") (v "0.8.0") (d (list (d (n "gazebo_derive") (r "^0.8.0") (d #t) (k 0)))) (h "16p6hfmz30dk02mf4qnxydbbq3bjrlx27j73qq36pacaar58f4gl") (f (quote (("str_pattern_extensions"))))))

(define-public crate-gazebo-0.8.1 (c (n "gazebo") (v "0.8.1") (d (list (d (n "gazebo_derive") (r "^0.8.0") (d #t) (k 0)))) (h "05hlpww2vdgdvxi8311vf4swhwb1svmbdlmcdgwwad2drm0x5kyl") (f (quote (("str_pattern_extensions"))))))

