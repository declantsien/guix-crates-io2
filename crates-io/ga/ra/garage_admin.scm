(define-module (crates-io ga ra garage_admin) #:use-module (crates-io))

(define-public crate-garage_admin-0.7.0 (c (n "garage_admin") (v "0.7.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "garage_util") (r "^0.7.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "opentelemetry") (r "^0.17") (f (quote ("rt-tokio"))) (d #t) (k 0)) (d (n "opentelemetry-otlp") (r "^0.10") (d #t) (k 0)) (d (n "opentelemetry-prometheus") (r "^0.10") (d #t) (k 0)) (d (n "prometheus") (r "^0.13") (d #t) (k 0)) (d (n "tracing") (r "^0.1.30") (d #t) (k 0)))) (h "0dav5wflm4p87bpll2qc0isp9x705zs0i450g1cybp5qk68sidvr")))

