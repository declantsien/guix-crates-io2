(define-module (crates-io ga ra garando_errors) #:use-module (crates-io))

(define-public crate-garando_errors-0.1.0 (c (n "garando_errors") (v "0.1.0") (d (list (d (n "garando_pos") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "term") (r "^0.6") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0si5cmcy9fm9pdbxwaqjbvbyiw4gj5i2hkgyks024ngdmk25wj8q")))

