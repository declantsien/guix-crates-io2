(define-module (crates-io ga ra garando_syntax) #:use-module (crates-io))

(define-public crate-garando_syntax-0.1.0 (c (n "garando_syntax") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "garando_errors") (r "^0.1") (d #t) (k 0)) (d (n "garando_pos") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "1mpcmgjd630hgy2dx6bzfbx264cj2zilbm1qy1mrr6pjswq2hj0c")))

(define-public crate-garando_syntax-0.1.1 (c (n "garando_syntax") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "garando_errors") (r "^0.1") (d #t) (k 0)) (d (n "garando_pos") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2") (d #t) (k 0)))) (h "0ayk80px1kwkqc6an0nf2j3k7n1d4pnb3g0v4n6cfbyic4w3i2hx")))

