(define-module (crates-io ga ud gaudium) #:use-module (crates-io))

(define-public crate-gaudium-0.0.0 (c (n "gaudium") (v "0.0.0") (d (list (d (n "arrayvec") (r "0.4.*") (d #t) (k 0)) (d (n "kernel32-sys") (r "0.2.*") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "lazy_static") (r "0.2.*") (d #t) (k 0)) (d (n "nix") (r "0.8.*") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "user32-sys") (r "0.1.*") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winapi") (r "0.2.*") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "04ibfd8pi82n8sc19g3m23z21ppicw7x02bmp179kiiafzkd3lgx") (f (quote (("platform-facade"))))))

