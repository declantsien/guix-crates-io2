(define-module (crates-io ga tt gattii) #:use-module (crates-io))

(define-public crate-gattii-0.1.0 (c (n "gattii") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "glib") (r "^0.1") (d #t) (k 0)) (d (n "gtk") (r "^0.1") (d #t) (k 0)) (d (n "serialport") (r "^0.1") (d #t) (k 0)))) (h "181ivj81w3hznbckwm1l3xpg8cqbd17vljkk8ym954550pskbmic")))

(define-public crate-gattii-0.1.1 (c (n "gattii") (v "0.1.1") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "glib") (r "^0.1") (d #t) (k 0)) (d (n "gtk") (r "^0.1") (d #t) (k 0)) (d (n "serialport") (r "^0.2") (d #t) (k 0)))) (h "1878wjhh3bggp9gqixhqff8lswkqsh2zxmz5nf38x53lb8rbwlck")))

(define-public crate-gattii-0.2.0 (c (n "gattii") (v "0.2.0") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "glib") (r "^0.1") (d #t) (k 0)) (d (n "gtk") (r "^0.1") (f (quote ("v3_20"))) (d #t) (k 0)) (d (n "serialport") (r "^0.3") (d #t) (k 0)))) (h "06n0zsn2y9pn6w9d6847xv7y7c6hll0y3j3afah8nq6i22h5ydym")))

(define-public crate-gattii-0.9.0 (c (n "gattii") (v "0.9.0") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "glib") (r "^0.1") (d #t) (k 0)) (d (n "gtk") (r "^0.1") (f (quote ("v3_20"))) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serialport") (r "^0.3") (d #t) (k 0)))) (h "0r4ird07a39iz6ggx8h53iw8cs3q58m413dpviiadk4v0im71mc2")))

(define-public crate-gattii-0.10.0 (c (n "gattii") (v "0.10.0") (d (list (d (n "cairo-rs") (r "^0.1.3") (f (quote ("v1_12"))) (d #t) (k 0)) (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^2.22") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "gdk") (r "^0.5.3") (d #t) (k 0)) (d (n "glib") (r "^0.1.3") (d #t) (k 0)) (d (n "gtk") (r "^0.1.3") (f (quote ("v3_14"))) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serialport") (r "^1.0") (d #t) (k 0)))) (h "1k92wkd9r21gxjc9f8n9rs386xaxsawyfvcw0qxbzy93q0yl4nbr")))

(define-public crate-gattii-0.11.0 (c (n "gattii") (v "0.11.0") (d (list (d (n "cairo-rs") (r "^0.3") (f (quote ("v1_12"))) (d #t) (k 0)) (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "clap") (r "^2.22") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "gdk") (r "^0.7") (d #t) (k 0)) (d (n "glib") (r "^0.4") (d #t) (k 0)) (d (n "gtk") (r "^0.3") (f (quote ("v3_14"))) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serialport") (r "^2.1") (d #t) (k 0)))) (h "0ld0rk1628dyhfjc8wza3wsdb7hx9whym57v0n42ymkq3afjfhxi")))

(define-public crate-gattii-0.13.0 (c (n "gattii") (v "0.13.0") (d (list (d (n "cairo-rs") (r "^0.5") (f (quote ("v1_12"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.22") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "gdk") (r "^0.9") (d #t) (k 0)) (d (n "glib") (r "^0.6") (d #t) (k 0)) (d (n "gtk") (r "^0.5") (f (quote ("v3_14"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serialport") (r "^3.1") (d #t) (k 0)))) (h "0yd4bapk4pgd5r1n831cxkwpf7lnlrsadjka73znb4zxdppjciid")))

