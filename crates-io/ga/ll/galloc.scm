(define-module (crates-io ga ll galloc) #:use-module (crates-io))

(define-public crate-galloc-0.0.1 (c (n "galloc") (v "0.0.1") (h "0g5s49f6s4yg2dvq6ns0plkdyfg6lvrp045pk8hkyrflvk3p34gn") (y #t)))

(define-public crate-galloc-1.0.2-354d660.6 (c (n "galloc") (v "1.0.2-354d660.6") (d (list (d (n "gear-dlmalloc") (r "^0.1.4") (d #t) (k 0)))) (h "0dwhrkhn0skhfd5qz0rf52fcppxc6mnbzsn6180471w1vyvh0a7i") (f (quote (("debug" "gear-dlmalloc/debug"))))))

(define-public crate-galloc-1.0.2-354d660.7 (c (n "galloc") (v "1.0.2-354d660.7") (d (list (d (n "gear-dlmalloc") (r "^0.1.4") (d #t) (k 0)))) (h "1063kq1lc8f54yhf859pdm11ympzlkhjpw1jfxkh5srsfassv0qw") (f (quote (("debug" "gear-dlmalloc/debug"))))))

(define-public crate-galloc-1.0.2-354d660.8 (c (n "galloc") (v "1.0.2-354d660.8") (d (list (d (n "gear-dlmalloc") (r "^0.1.4") (d #t) (k 0)))) (h "1kl0f2xrhgcinynfsy841gwg0hj2illy5jkcwsj53n7x9gnxp3z0") (f (quote (("debug" "gear-dlmalloc/debug"))))))

(define-public crate-galloc-1.0.2-354d660.9 (c (n "galloc") (v "1.0.2-354d660.9") (d (list (d (n "gear-dlmalloc") (r "^0.1.4") (d #t) (k 0)))) (h "05ib1lg9m8lcyshg086mj6yf43vskdxkkr321w1ds8v55pp8j3g5") (f (quote (("debug" "gear-dlmalloc/debug"))))))

(define-public crate-galloc-1.0.2-354d660.10 (c (n "galloc") (v "1.0.2-354d660.10") (d (list (d (n "gear-dlmalloc") (r "^0.1.4") (d #t) (k 0)))) (h "0xbiraxfsfpmkdjy3yggrd92y1ds5g05nwmr94qf6i1rq9j4a71w") (f (quote (("debug" "gear-dlmalloc/debug"))))))

(define-public crate-galloc-1.0.2 (c (n "galloc") (v "1.0.2") (d (list (d (n "dlmalloc") (r "^0.1.4") (d #t) (k 0) (p "gear-dlmalloc")))) (h "10wyn7hbyyl7f1k0clkz66icbjp0lq71w3szsqp9z2x0wvyhhj5k") (f (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1.0.2-gtest-dev (c (n "galloc") (v "1.0.2-gtest-dev") (d (list (d (n "dlmalloc") (r "^0.1.4") (d #t) (k 0) (p "gear-dlmalloc")))) (h "1w52fa5jmbk8m9kwpbf7zqicmw0bqkdiip1m3aag79dzgvmp83wf") (f (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1.0.2-dev.0 (c (n "galloc") (v "1.0.2-dev.0") (d (list (d (n "dlmalloc") (r "^0.1.4") (d #t) (k 0) (p "gear-dlmalloc")))) (h "16yd8mnc52z9vcyjsiki7bqpxnmfdky4c68qhjbriir5zc16hksk") (f (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1.0.3 (c (n "galloc") (v "1.0.3") (d (list (d (n "dlmalloc") (r "^0.2.0") (d #t) (k 0) (p "gear-dlmalloc")))) (h "13mnalrrx9kdcp8m36scsdak67ml3p5mvhxq8rvvvnd3z8cfvc37") (f (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1.0.4-rc.0 (c (n "galloc") (v "1.0.4-rc.0") (d (list (d (n "dlmalloc") (r "^0.2.0") (d #t) (k 0) (p "gear-dlmalloc")))) (h "1y3x54bsiyqwspym6cwr231yh5bh8njvncyc93bnww17qncwnpia") (f (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1.0.4 (c (n "galloc") (v "1.0.4") (d (list (d (n "dlmalloc") (r "^0.2.0") (d #t) (k 0) (p "gear-dlmalloc")))) (h "05gil3dndb1r7i1k4jzb556ay36cvgn6w84d161q56bs63lhp0pz") (f (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1.0.5 (c (n "galloc") (v "1.0.5") (d (list (d (n "dlmalloc") (r "^0.1.4") (d #t) (k 0) (p "gear-dlmalloc")))) (h "1gvq7hdx3mgi5vnv31ryi09liqx35gs3i2pr0x1b90ivk7avk9bb") (f (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1.1.0 (c (n "galloc") (v "1.1.0") (d (list (d (n "dlmalloc") (r "^0.2.0") (d #t) (k 0) (p "gear-dlmalloc")))) (h "0iqrlwlvac2804m0ra4fyqiq6v91wfsbicgv9iidmfdi3rbnnwvk") (f (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1.1.1-rc.0 (c (n "galloc") (v "1.1.1-rc.0") (d (list (d (n "dlmalloc") (r "^0.2.0") (d #t) (k 0) (p "gear-dlmalloc")))) (h "0gca6sdb0yk6dyx15j9la3i56qk26bcjjlgwipnznq4acrqn28yr") (f (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1.1.1 (c (n "galloc") (v "1.1.1") (d (list (d (n "dlmalloc") (r "^0.2.0") (d #t) (k 0) (p "gear-dlmalloc")))) (h "0sx9a5n5mi9rb2qbw21vsjxz6js314sr4y1bmk6019h0kqbn1iig") (f (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1.2.0 (c (n "galloc") (v "1.2.0") (d (list (d (n "dlmalloc") (r "^0.2.0") (d #t) (k 0) (p "gear-dlmalloc")))) (h "0k2xhvgrns1pfjjmxdichc9bm7d7vlrzfwicckp7jvik0q7wj2q5") (f (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1.3.0 (c (n "galloc") (v "1.3.0") (d (list (d (n "dlmalloc") (r "^0.2.0") (d #t) (k 0) (p "gear-dlmalloc")))) (h "0537jbjv5mhknzz7fa82v5swg5l85cvma3673ms5m67kqljskp4r") (f (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1.2.0-pre1 (c (n "galloc") (v "1.2.0-pre1") (d (list (d (n "dlmalloc") (r "^0.2.0") (d #t) (k 0) (p "gear-dlmalloc")))) (h "037yygjgmxr2gk842sngw2vi2hfllhq7i34ppc8zmnr7krvkbxn2") (f (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1.2.0-pre.2 (c (n "galloc") (v "1.2.0-pre.2") (d (list (d (n "dlmalloc") (r "^0.2.0") (d #t) (k 0) (p "gear-dlmalloc")))) (h "1k67x4mx324mdcxnjjgdhw8w37nfpl1hf055r6qps8ncs2r325sk") (f (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1.3.1-pre.1 (c (n "galloc") (v "1.3.1-pre.1") (d (list (d (n "dlmalloc") (r "^0.2.0") (d #t) (k 0) (p "gear-dlmalloc")))) (h "09gn7qlcvb0pcddjghnk92afzix0x1d1gixyzf6g07a08kg5n104") (f (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1.3.1-pre.2 (c (n "galloc") (v "1.3.1-pre.2") (d (list (d (n "dlmalloc") (r "^0.2.0") (d #t) (k 0) (p "gear-dlmalloc")))) (h "1h5s5f9mmxlsxml5wwa1xm5mzx1prns0k6x8p30gxa7biv8pnb07") (f (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1.3.1-pre.3 (c (n "galloc") (v "1.3.1-pre.3") (d (list (d (n "dlmalloc") (r "^0.2.0") (d #t) (k 0) (p "gear-dlmalloc")))) (h "19sxb70dv3wdjqvci4llbw47nccy8achsxa9njlx850gciqpmj5x") (f (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1.2.0-pre.3 (c (n "galloc") (v "1.2.0-pre.3") (d (list (d (n "dlmalloc") (r "^0.2.0") (d #t) (k 0) (p "gear-dlmalloc")))) (h "14xibmn5270yrc5sq7ycajjcs7fd9a7khspsm5p0cbkrpq4m5kqw") (f (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1.2.1 (c (n "galloc") (v "1.2.1") (d (list (d (n "dlmalloc") (r "^0.2.0") (d #t) (k 0) (p "gear-dlmalloc")))) (h "0qx31x8p1kcnxn6a8648hphpilm64ylscq40j790l0iskr9w4fs4") (f (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1.3.0-pre.1 (c (n "galloc") (v "1.3.0-pre.1") (d (list (d (n "dlmalloc") (r "^0.2.0") (d #t) (k 0) (p "gear-dlmalloc")))) (h "1njsa485cz7cwvxvvvawzna4paz8zhzkqgdvrl4dqzd5aa3b77h1") (f (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1.3.1 (c (n "galloc") (v "1.3.1") (d (list (d (n "dlmalloc") (r "^0.2.0") (d #t) (k 0) (p "gear-dlmalloc")))) (h "1lgd67qnyawdflfvc7rqgr7md67sb1mnx4mf6mz4aw34ixiqrzjs") (f (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1.4.0 (c (n "galloc") (v "1.4.0") (d (list (d (n "dlmalloc") (r "^0.2.0") (d #t) (k 0) (p "gear-dlmalloc")))) (h "0gwdkybzdiw1bk6nwhf02zj10fdq34693qzc7s2anx4v2zqas6q5") (f (quote (("debug" "dlmalloc/debug"))))))

(define-public crate-galloc-1.4.1 (c (n "galloc") (v "1.4.1") (d (list (d (n "dlmalloc") (r "^0.2.0") (d #t) (k 0) (p "gear-dlmalloc")))) (h "0gxbh4pbprl4d4pff97d8b4qcwc0qazaqbg6jzzfb2kixsx5rh7f") (f (quote (("debug" "dlmalloc/debug"))))))

