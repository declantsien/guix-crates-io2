(define-module (crates-io ga ll gallium) #:use-module (crates-io))

(define-public crate-gallium-0.1.0 (c (n "gallium") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "handlebars") (r "^2.0.1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.5.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.11.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "1bvkad1cd5rm4abgp6gj71lcl9f2mmx4gj8glhswsj3ks0dcnicz")))

(define-public crate-gallium-0.1.1 (c (n "gallium") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "handlebars") (r "^2.0.1") (d #t) (k 0)) (d (n "notify") (r "^4.0.12") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.5.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.11.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "16gycfhicqryvik1l27si6h1d5bxmy5rhmyi4wkhpw4fmvd9ykd5")))

(define-public crate-gallium-0.1.2 (c (n "gallium") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "handlebars") (r "^2.0.1") (d #t) (k 0)) (d (n "notify") (r "^4.0.12") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.5.3") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)) (d (n "textwrap") (r "^0.11.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "01vhvafk10vj3c4nxpssc1ya7lw40ja1ax1rgzlr46l1hwpx7bqj")))

(define-public crate-gallium-0.1.3 (c (n "gallium") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "handlebars") (r "^2.0.1") (d #t) (k 0)) (d (n "notify") (r "^4.0.12") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.5.3") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)) (d (n "textwrap") (r "^0.11.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "1qq471pimzdpnpra4i63ci2h7q1dkyqkxf42wir2lchjkbwbnvc1")))

