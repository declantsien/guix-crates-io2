(define-module (crates-io ga ll gallop) #:use-module (crates-io))

(define-public crate-gallop-0.0.1 (c (n "gallop") (v "0.0.1") (h "0nr6h40shidaqq6xip0v78kpjcnqm65xksgbwaqg3flljzl68vaz")))

(define-public crate-gallop-0.0.2 (c (n "gallop") (v "0.0.2") (h "0dwgw7jvdgc2c2r4sn9cnbbpy1jwzc07jbfw4laxjcqhka95vs4x")))

(define-public crate-gallop-0.0.3 (c (n "gallop") (v "0.0.3") (h "0cbz4yklj8vpahiz3sh9dd6ckvk5i2jhd235yawdlifs0r1xza0r")))

(define-public crate-gallop-0.0.4 (c (n "gallop") (v "0.0.4") (h "0wikncy7ww5mzx7aqlpzsy9cb5zh7wak4fxbsgvca81qw3jlnc3b")))

(define-public crate-gallop-0.0.5 (c (n "gallop") (v "0.0.5") (h "1261294a5kkffwazabc1sk4d48jww2lcq7c98x73szkhmd37fmj3")))

(define-public crate-gallop-0.0.6 (c (n "gallop") (v "0.0.6") (h "11ab7zz4q5b1cxkv1pams3dz8wqsf9glb7kd0jrazprlsmcwy4iv")))

(define-public crate-gallop-1.0.1 (c (n "gallop") (v "1.0.1") (h "0ppysn5jni2i7jbfb0g0w0cv7a99zidgl4zm3qs8x2vdimmg5gpq")))

(define-public crate-gallop-1.0.2 (c (n "gallop") (v "1.0.2") (h "0gc6h7mljrm4pn9cayrksdy1biragi4xxhm1kprvrvwkkvdv2r38")))

(define-public crate-gallop-1.0.3 (c (n "gallop") (v "1.0.3") (h "1ffgy306cl719qcv4llv1k91z7pxj8bvs04hq7ybnz7jffr0qbsb")))

(define-public crate-gallop-1.0.4 (c (n "gallop") (v "1.0.4") (h "1gsyf2gp2psyxr64n1rhds94sxpd7pf87z1y2n8mz1pwc73r0mfh")))

(define-public crate-gallop-1.0.5 (c (n "gallop") (v "1.0.5") (h "16bha1v16vgmv8x33kcm0c5sp2avcd91dy3s3lr8h6mckinx9vaw")))

(define-public crate-gallop-1.0.6 (c (n "gallop") (v "1.0.6") (h "134m3pk1v5h95zpdvwf1blqrh8z3cxj9x1wqpz95yc3dmbaam4pr")))

(define-public crate-gallop-1.0.7 (c (n "gallop") (v "1.0.7") (h "1jdj20nvkb0lm11sbfi5nwgfdss9pfwfiyf139pdp8iapq69m59b")))

