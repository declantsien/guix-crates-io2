(define-module (crates-io ga ll galley) #:use-module (crates-io))

(define-public crate-galley-0.1.0 (c (n "galley") (v "0.1.0") (h "1fcs2nik9crwy4p3hhkfjwl881cmqj60q7gghc498r7kavrrvrng")))

(define-public crate-galley-0.1.1 (c (n "galley") (v "0.1.1") (h "1pfysczg02vwhpzah95slzg3nbr8ms35qv92l24p5nir2lwy9jpq")))

