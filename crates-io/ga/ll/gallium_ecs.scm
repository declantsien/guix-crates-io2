(define-module (crates-io ga ll gallium_ecs) #:use-module (crates-io))

(define-public crate-gallium_ecs-0.1.0 (c (n "gallium_ecs") (v "0.1.0") (d (list (d (n "ron") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "0njzhp38hxxfid4p0lfhr5n84w8lck2a48hflrhgp9k8gy0clkcl")))

(define-public crate-gallium_ecs-0.1.1 (c (n "gallium_ecs") (v "0.1.1") (d (list (d (n "ron") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "07jvqs8h5a8syfgd1l3bqlw20if9hhqsnrn2jn238iqiim48mxl1")))

(define-public crate-gallium_ecs-0.1.2 (c (n "gallium_ecs") (v "0.1.2") (d (list (d (n "ron") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "0d1r26m4xpd1rsbw3nwbl7iqis3bc061l2j16xba4bl0lfjn82bj")))

(define-public crate-gallium_ecs-0.2.0 (c (n "gallium_ecs") (v "0.2.0") (d (list (d (n "ron") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)))) (h "1qw5380sbwg72dnqbw00k9pjqy3vvfl2h9l10pmw8djjq3ks5ah8")))

