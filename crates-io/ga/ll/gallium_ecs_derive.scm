(define-module (crates-io ga ll gallium_ecs_derive) #:use-module (crates-io))

(define-public crate-gallium_ecs_derive-0.1.0 (c (n "gallium_ecs_derive") (v "0.1.0") (d (list (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "08nxx638hvc13hdfyh6xmqsg81s6fn5v1lj33jk66fj2d7wgk4ky")))

(define-public crate-gallium_ecs_derive-0.1.1 (c (n "gallium_ecs_derive") (v "0.1.1") (d (list (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1wdjadl4b1siywfjmcdx9z41qd51ly10xml8fkg58ndfrhy6hzx9")))

