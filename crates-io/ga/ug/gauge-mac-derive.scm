(define-module (crates-io ga ug gauge-mac-derive) #:use-module (crates-io))

(define-public crate-gauge-mac-derive-0.1.0 (c (n "gauge-mac-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "12536yy8nx9xgfyqxdml32c9l383vwgw9p5zhdbj2v2r2c15f14r")))

