(define-module (crates-io ga ug gauge-mac-tool) #:use-module (crates-io))

(define-public crate-gauge-mac-tool-0.1.0 (c (n "gauge-mac-tool") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "gauge-mac-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "i2cdev") (r "^0.5.1") (d #t) (k 0)))) (h "19y3glapljskhqhpwpbhlg1wz4ln1xxz0s6qzmdbj32r4zx6kabp")))

