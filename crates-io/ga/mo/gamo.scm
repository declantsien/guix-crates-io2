(define-module (crates-io ga mo gamo) #:use-module (crates-io))

(define-public crate-gamo-0.1.0 (c (n "gamo") (v "0.1.0") (h "0d6pskqm2v9wqb8671d9g5fss22680xp9csf8j14yyi1zj88jvxi")))

(define-public crate-gamo-0.2.0 (c (n "gamo") (v "0.2.0") (h "03x84y9hh0l9icwr93ksil0f7b1pl5rg7j651l7kpbr37w3n3qys")))

(define-public crate-gamo-0.3.0 (c (n "gamo") (v "0.3.0") (h "0v2b5ak7d8f8zsf5b2dywblk20shsq3z552q7rsyrl3jijyc86ww")))

