(define-module (crates-io ga bs gabs) #:use-module (crates-io))

(define-public crate-gabs-0.1.0 (c (n "gabs") (v "0.1.0") (d (list (d (n "comrak") (r "^0.18.0") (d #t) (k 0)))) (h "0ymnzld83ya4h8iqrzr7jja3alvfm2xvxds1icr4mj4fmm2knh67")))

(define-public crate-gabs-0.1.1 (c (n "gabs") (v "0.1.1") (d (list (d (n "comrak") (r "^0.18.0") (d #t) (k 0)))) (h "0w7117fmd62ff9mh45cvj1hr9divdfi5jjpspp3v3mxl4jn8yghc")))

(define-public crate-gabs-0.1.2 (c (n "gabs") (v "0.1.2") (d (list (d (n "comrak") (r "^0.18.0") (d #t) (k 0)))) (h "0v0239bldjgbx5j6r1yh9gmhh2i1l8lr0bypr64c04s7nr9qmr5q")))

(define-public crate-gabs-0.1.3 (c (n "gabs") (v "0.1.3") (d (list (d (n "comrak") (r "^0.18.0") (d #t) (k 0)))) (h "0afbgh877c9wkzqx43i3smkxi4f8n754kr14ywkqi20577q7g6s3")))

(define-public crate-gabs-0.1.4 (c (n "gabs") (v "0.1.4") (d (list (d (n "comrak") (r "^0.18.0") (d #t) (k 0)))) (h "0dkby0sj5c165kbm6nnrnx5p0vdjfd7yv66yhs0wgrzrda85qm4y")))

(define-public crate-gabs-0.1.5 (c (n "gabs") (v "0.1.5") (d (list (d (n "comrak") (r "^0.18.0") (d #t) (k 0)))) (h "1i6ks04ffq9c0gcm5qmhdqcc3b6s5vhxclnji5a539dn0fjw7h50")))

(define-public crate-gabs-0.1.6 (c (n "gabs") (v "0.1.6") (d (list (d (n "comrak") (r "^0.18.0") (d #t) (k 0)))) (h "1b6r1wx78iwgi1nqrffja425q760w5mg1hamk8llgajai23r12l9")))

(define-public crate-gabs-0.1.7 (c (n "gabs") (v "0.1.7") (d (list (d (n "comrak") (r "^0.18.0") (d #t) (k 0)))) (h "0qqznpd269ylidj59969mm7v9f28finldnwrjpjv3jjlrp9gpgya")))

