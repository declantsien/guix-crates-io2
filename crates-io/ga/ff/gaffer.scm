(define-module (crates-io ga ff gaffer) #:use-module (crates-io))

(define-public crate-gaffer-0.0.1 (c (n "gaffer") (v "0.0.1") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.4") (d #t) (k 2)) (d (n "futures") (r "^0.3.15") (f (quote ("executor"))) (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "1i8m44x2pcpv4662gkl2giwhskqjqgkdic0rzlxv0nlzymkjgjsd")))

(define-public crate-gaffer-0.1.0 (c (n "gaffer") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (f (quote ("executor"))) (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "0dbwxc5zjnnw5szd4dvjjh6kc7pdfw3yqpl8j17m4jsl0lfzwcab")))

(define-public crate-gaffer-0.2.0 (c (n "gaffer") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (f (quote ("executor"))) (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "1ni6rkmp9dz2agacxrqy3j80mvlcnpfv497rp7wfrz6psy93agnj")))

