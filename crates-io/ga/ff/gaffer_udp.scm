(define-module (crates-io ga ff gaffer_udp) #:use-module (crates-io))

(define-public crate-gaffer_udp-0.1.1 (c (n "gaffer_udp") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "cucumber") (r "^0.1.3") (d #t) (k 2)) (d (n "itertools") (r "^0.4.16") (d #t) (k 0)) (d (n "mio") (r "^0.5.1") (d #t) (k 0)))) (h "0nrz72iyy2rhds40ndzzyn9qmkxg868z10y12hw8kg08v7wrpgzg")))

(define-public crate-gaffer_udp-0.1.2 (c (n "gaffer_udp") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "cucumber") (r "^0.1.3") (d #t) (k 2)) (d (n "itertools") (r "^0.4.16") (d #t) (k 0)) (d (n "mio") (r "^0.5.1") (d #t) (k 0)))) (h "0x13asaw7dlgsyz6cf8kd68ncixacmwwixjn9jazp52xafv2h2hr")))

(define-public crate-gaffer_udp-0.1.3 (c (n "gaffer_udp") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "cucumber") (r "^0.1.3") (d #t) (k 2)) (d (n "itertools") (r "^0.5.8") (d #t) (k 0)) (d (n "mio") (r "^0.6.2") (d #t) (k 0)))) (h "0q12ysdxhs95irq9v25204m8xmf2h2vd1h6g8kk5nnswl3qm3iiw")))

(define-public crate-gaffer_udp-0.1.4 (c (n "gaffer_udp") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "cucumber") (r "^0.1.3") (d #t) (k 2)) (d (n "itertools") (r "^0.5.8") (d #t) (k 0)) (d (n "mio") (r "^0.6.2") (d #t) (k 0)))) (h "1fnd6knbc34qj1jqpskpymb01kxh3cyd9h6578hn6hdxhs1fk4pv")))

