(define-module (crates-io ga zb gazbuzz) #:use-module (crates-io))

(define-public crate-gazbuzz-0.1.0 (c (n "gazbuzz") (v "0.1.0") (h "14s8fbmgzywq0f1bcfdn01yra00sh6cxxv5ifsfld8v6ganidhwm")))

(define-public crate-gazbuzz-0.1.1 (c (n "gazbuzz") (v "0.1.1") (h "0vmy0flzr7bw51bbm9rfnq412w8parbgm7kx0i9hmdg01gh921ym")))

(define-public crate-gazbuzz-0.1.2 (c (n "gazbuzz") (v "0.1.2") (h "009qn0v61qgzm0f7kbgscclayvw14c0ab2qf2jjva46njdammbqi")))

(define-public crate-gazbuzz-0.1.3 (c (n "gazbuzz") (v "0.1.3") (h "0l2rxpjyr2sazdv894f2ryjp8h8nrbmypqiha831mnxwhfk3xnm8")))

(define-public crate-gazbuzz-0.1.4 (c (n "gazbuzz") (v "0.1.4") (h "0gbphiqx81vidhdy1lagqzx17n922m750zr6fhnih63kq6hgpaiz")))

(define-public crate-gazbuzz-0.1.5 (c (n "gazbuzz") (v "0.1.5") (h "02vx84c2k3ifmvbyca3fwxhh0ak57vf22lsg6lnf8kha4cppfmrq")))

