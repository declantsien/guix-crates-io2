(define-module (crates-io ga vi gaviota-sys) #:use-module (crates-io))

(define-public crate-gaviota-sys-0.1.0 (c (n "gaviota-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.36.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.15") (d #t) (k 1)))) (h "0ncx24axa2l1vikbppx7y6vncsvflj9h3g5fhq0aizadapc2dmfz")))

(define-public crate-gaviota-sys-0.1.1 (c (n "gaviota-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.37.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.15") (d #t) (k 1)))) (h "0xb1rs4vjp662impcs77xxk6l86pr8nn0midacar7jhrm03i4r9g")))

(define-public crate-gaviota-sys-0.1.2 (c (n "gaviota-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1i25ikn6ysgx7779wq42iqcdybsny1jh2snyn2v4ssxdlgch2grv")))

(define-public crate-gaviota-sys-0.1.3 (c (n "gaviota-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.43") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1kwf0sxmv0w93ffgvzn29f3znpl1f957ch8ljh466i7jmmh7yd2s")))

(define-public crate-gaviota-sys-0.1.4 (c (n "gaviota-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.44") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0865slr1nz33x4mr9w55nz6l30w81mig69xixv9yglbmp9h0m84i")))

(define-public crate-gaviota-sys-0.1.5 (c (n "gaviota-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.46") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1k434216vqm1r9g6lk3cmk2cn6idcf135h6lhs0dazclqr2ywy5y")))

(define-public crate-gaviota-sys-0.1.6 (c (n "gaviota-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.47") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "15zf1i4faq142x1v61p0j8ncb1fpkhkrha2rnh8kw0iswbcpxgiw")))

(define-public crate-gaviota-sys-0.1.7 (c (n "gaviota-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.48") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1z557zp9agcjmjxy6p6ly178sbsfl0l1xgsg6nag6c2dz23p9vc9")))

(define-public crate-gaviota-sys-0.1.8 (c (n "gaviota-sys") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1qb0gpfn8lf4i3kgzrajc3yn91qcs0zgkdwg8gd9b5wdxxsdipr3")))

(define-public crate-gaviota-sys-0.1.9 (c (n "gaviota-sys") (v "0.1.9") (d (list (d (n "bindgen") (r "^0.50") (f (quote ("logging"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "11jw1pn0vd40dr845chdi9yc3pzpylrq3ybgb4s2s88minwy2n1y")))

(define-public crate-gaviota-sys-0.1.10 (c (n "gaviota-sys") (v "0.1.10") (d (list (d (n "bindgen") (r "^0.51") (f (quote ("logging"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0m11csmvhmp8m7f8f66gwk5yhlc779czrk6dvh02gj9jakw7a6ri")))

(define-public crate-gaviota-sys-0.1.11 (c (n "gaviota-sys") (v "0.1.11") (d (list (d (n "bindgen") (r "^0.52") (f (quote ("logging" "runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "02vjhzdy2klkfcgimc5bdv6gcfh55xczb35cvy0b75r3jcy07zjh")))

(define-public crate-gaviota-sys-0.1.12 (c (n "gaviota-sys") (v "0.1.12") (d (list (d (n "bindgen") (r "^0.53") (f (quote ("logging" "runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1dmaipyyds1czb21xb9k4py0zqarcbpkl1kk05n1x0fmpc5hyjy1")))

(define-public crate-gaviota-sys-0.1.13 (c (n "gaviota-sys") (v "0.1.13") (d (list (d (n "bindgen") (r "^0.54") (f (quote ("logging" "runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "18b2lldzzc5vaml5gg64vpdjwr1si5mdpcvi440mkrs610saj8f2")))

(define-public crate-gaviota-sys-0.1.14 (c (n "gaviota-sys") (v "0.1.14") (d (list (d (n "bindgen") (r "^0.55") (f (quote ("logging" "runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1qy2xzj96p8wsm38fmccz69y56di9gmvg0pxrl3kfkcjmm7f492x")))

(define-public crate-gaviota-sys-0.1.15 (c (n "gaviota-sys") (v "0.1.15") (d (list (d (n "bindgen") (r ">=0.56.0, <0.57.0") (f (quote ("logging" "runtime"))) (k 1)) (d (n "cc") (r ">=1.0.0, <2.0.0") (d #t) (k 1)))) (h "1lrwa123200wfjp3v3ljrdbm1rwvzxbvim56rmpmnjw37xgx3wc1")))

(define-public crate-gaviota-sys-0.1.16 (c (n "gaviota-sys") (v "0.1.16") (d (list (d (n "bindgen") (r "^0.57") (f (quote ("logging" "runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1vpjq6qm78f889pif627sila7fni7dgjzhsvxqldm8gj3r8nvsa5")))

(define-public crate-gaviota-sys-0.1.17 (c (n "gaviota-sys") (v "0.1.17") (d (list (d (n "bindgen") (r "^0.58") (f (quote ("logging" "runtime"))) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0ihishpib7vxhlmyw4pidcdq3n494041sz1nmn4f0wbasawspl6k")))

(define-public crate-gaviota-sys-0.1.18 (c (n "gaviota-sys") (v "0.1.18") (d (list (d (n "bindgen") (r "^0.59") (f (quote ("logging" "runtime"))) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1wnms0qmli1svqpvgrv0w8643shjchblq56027jwfj73if2dj2j1")))

(define-public crate-gaviota-sys-0.1.19 (c (n "gaviota-sys") (v "0.1.19") (d (list (d (n "bindgen") (r "^0.60") (f (quote ("logging" "runtime"))) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0zin41jyl6cri3041gdv9fnp0g2xk0jid9bd12rggjik55h37iyw")))

(define-public crate-gaviota-sys-0.1.20 (c (n "gaviota-sys") (v "0.1.20") (d (list (d (n "bindgen") (r "^0.60") (f (quote ("logging" "runtime"))) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1aar31whh579q0w1syf7i12w1fkq8mqd2s0vdird7abfggvnsv3v") (l "gtb")))

(define-public crate-gaviota-sys-0.1.21 (c (n "gaviota-sys") (v "0.1.21") (d (list (d (n "bindgen") (r "^0.60") (f (quote ("logging" "runtime"))) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0zi1npd4fv2arbji482160cdrd0pb1jvhn2l7kcbq8avf1l3ly2h") (l "gtb")))

(define-public crate-gaviota-sys-0.1.22 (c (n "gaviota-sys") (v "0.1.22") (d (list (d (n "bindgen") (r "^0.63.0") (f (quote ("logging" "runtime"))) (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (k 1)))) (h "1q1fhj1fa7fbaha3d8bln8y6mk0c5ii8nprr1791yapgcamnmi6m") (l "gtb")))

(define-public crate-gaviota-sys-0.1.23 (c (n "gaviota-sys") (v "0.1.23") (d (list (d (n "bindgen") (r "^0.64.0") (f (quote ("logging" "runtime"))) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "1qp1m855f48n2ijx0ffsfijpc53nk1m0w2kd34g42xqzlm94b13s") (l "gtb")))

(define-public crate-gaviota-sys-0.1.24 (c (n "gaviota-sys") (v "0.1.24") (d (list (d (n "bindgen") (r "^0.65.1") (f (quote ("logging" "runtime"))) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "09f5m58pz4dgj3khglz9gy821pbpvnc26p9kb4dbyljhgdpyfzqv") (l "gtb")))

(define-public crate-gaviota-sys-0.1.25 (c (n "gaviota-sys") (v "0.1.25") (d (list (d (n "bindgen") (r "^0.66.1") (f (quote ("logging" "runtime"))) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "1yb6pahir6cjfh1sfk4piqwhzawdp4liqfj2nknczc9jjg4a1px9") (l "gtb")))

(define-public crate-gaviota-sys-0.1.26 (c (n "gaviota-sys") (v "0.1.26") (d (list (d (n "bindgen") (r "^0.69.2") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "1frhghch9h0yg9hpsqg5nm4jl81z8b6w12br5l9vx21asnxyv9gb") (l "gtb")))

