(define-module (crates-io ga p- gap-sys) #:use-module (crates-io))

(define-public crate-gap-sys-0.2.3 (c (n "gap-sys") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (f (quote ("experimental"))) (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "18xxqligfjsq1crcrrs8lph1jwg7my40is9phwpx866pbyhnqaxf")))

