(define-module (crates-io j_ he j_hello) #:use-module (crates-io))

(define-public crate-j_hello-0.1.0 (c (n "j_hello") (v "0.1.0") (h "158qir4b9jrkjbwhi64azibafkm9hlgllcg5xc8xmpynpg2v2116")))

(define-public crate-j_hello-0.1.1 (c (n "j_hello") (v "0.1.1") (h "1g75alp4mx1w3dw5jinm9sy1j0nbhyq9rm54qz5nbqkdksav80m3")))

(define-public crate-j_hello-0.1.10 (c (n "j_hello") (v "0.1.10") (h "1za1srlls5sg8kbv3pp4aw9dm388kby6imlfxinyw81pa5fm521x")))

