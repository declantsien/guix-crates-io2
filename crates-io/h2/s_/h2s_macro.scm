(define-module (crates-io h2 s_ h2s_macro) #:use-module (crates-io))

(define-public crate-h2s_macro-0.1.0 (c (n "h2s_macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "h2s_core") (r "=0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "1fm05zbbxw92w72wfi2r9zdqsx7dkj5yhr0g75fqsy6rkl81vhbk") (r "1.65")))

(define-public crate-h2s_macro-0.1.1 (c (n "h2s_macro") (v "0.1.1") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "h2s_core") (r "=0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "03xj6h2v2wvy2sxf4hdqy7srmf4a2v3h0j54a36apyd31vwvpjlb") (r "1.65")))

(define-public crate-h2s_macro-0.1.2 (c (n "h2s_macro") (v "0.1.2") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "h2s_core") (r "=0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "1g0h47kiadppigjx2yrwxjbdp50jwv60i0jg0v0k8prxbn9l2fz1") (r "1.65")))

(define-public crate-h2s_macro-0.1.3 (c (n "h2s_macro") (v "0.1.3") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "h2s_core") (r "=0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0dha0xnkjarxjfb1s7mwd6j3rhkw4wmzlaprswz4f8adrf06l5zl") (r "1.65")))

(define-public crate-h2s_macro-0.1.4 (c (n "h2s_macro") (v "0.1.4") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "h2s_core") (r "=0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0vq013hc57h36mx2s1hzdfs9b3d9waqa09jrj9xbjhhz9jighz8y") (r "1.65")))

(define-public crate-h2s_macro-0.1.5 (c (n "h2s_macro") (v "0.1.5") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "h2s_core") (r "^0.1.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0x5xc2lc98syiyg31i3n6hvdjnikx6hq299i847s99dq4hlg5xqb") (r "1.65")))

(define-public crate-h2s_macro-0.1.6 (c (n "h2s_macro") (v "0.1.6") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "h2s_core") (r "^0.1.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "1cnqgbgwij2kvc95zbv7vf0yja9g732as1y5zmdj6xilgck4pkq7") (r "1.65")))

(define-public crate-h2s_macro-0.17.0 (c (n "h2s_macro") (v "0.17.0") (d (list (d (n "darling") (r "^0.14.1") (d #t) (k 0)) (d (n "h2s_core") (r "^0.17.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "scraper") (r "^0.16.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (d #t) (k 0)))) (h "0c2zgxqxb96fq348m4sc1gz7z0by3ijckqnfmn45mmzmc67hlrsv") (r "1.65")))

(define-public crate-h2s_macro-0.18.0 (c (n "h2s_macro") (v "0.18.0") (d (list (d (n "darling") (r "^0.20.0") (d #t) (k 0)) (d (n "h2s_core") (r "^0.18.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "scraper") (r "^0.16.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (d #t) (k 0)))) (h "1pkp8h9649hipshk11kcx2b4y2h273fd2snw40c8idb3ghhqsh0p") (r "1.65")))

