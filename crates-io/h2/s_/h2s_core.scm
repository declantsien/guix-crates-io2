(define-module (crates-io h2 s_ h2s_core) #:use-module (crates-io))

(define-public crate-h2s_core-0.1.0 (c (n "h2s_core") (v "0.1.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)))) (h "1jnx4wl5i3p3gdxxdxf3wjyi6nncf7axdkxwnb49j97lz4jg22b5") (r "1.65")))

(define-public crate-h2s_core-0.1.1 (c (n "h2s_core") (v "0.1.1") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)))) (h "1x5k3f7ad1136d15qds8m0klxds5rd44f5d5jgrz6dfqflffdpj2") (r "1.65")))

(define-public crate-h2s_core-0.1.2 (c (n "h2s_core") (v "0.1.2") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)))) (h "1pz5vm7fyxhkwqivh6a74rf2rxkwpxxj4kjyphxrma3m198vn7kz") (r "1.65")))

(define-public crate-h2s_core-0.1.3 (c (n "h2s_core") (v "0.1.3") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 2)))) (h "1kzglk36ap43rs096cq17nq9y1aqcmqqhs12j2rqjm1r0589fczr") (r "1.65")))

(define-public crate-h2s_core-0.1.4 (c (n "h2s_core") (v "0.1.4") (h "1npdvpn9n041dhj3ny623y79d6wsigyin2a84kvx83j0xmq2mxmd") (r "1.65")))

(define-public crate-h2s_core-0.1.5 (c (n "h2s_core") (v "0.1.5") (h "1jqdlm0cc3qnf0rv18cjv12wak7p9lkyrbyycspzrrjc555anpyy") (r "1.65")))

(define-public crate-h2s_core-0.1.6 (c (n "h2s_core") (v "0.1.6") (h "1xpj87k88iwwsm65jr8d71dh1ygs98p4qygmm4llby95kk8x61k7") (r "1.65")))

(define-public crate-h2s_core-0.17.0 (c (n "h2s_core") (v "0.17.0") (h "0jznpknhc1fwlgh0hrid0kcpf3r4cn8d63bjm5arpgygrcgdh06z") (r "1.65")))

(define-public crate-h2s_core-0.18.0 (c (n "h2s_core") (v "0.18.0") (h "0yil8ynqwc7jcwp8axvf0nr0k6q3qr83kw07wcz3p75mdc0k1qrs") (r "1.65")))

