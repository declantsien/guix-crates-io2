(define-module (crates-io h2 #{56}# h256only) #:use-module (crates-io))

(define-public crate-h256only-0.4.0 (c (n "h256only") (v "0.4.0") (d (list (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0cjw9jxc6yrskfi0fymkp34khswbcxg0y6gp0m8qvbigg550q86w")))

(define-public crate-h256only-0.4.1 (c (n "h256only") (v "0.4.1") (d (list (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0pi2r7igaksm2y73r6gvzr8dlrdlm1sq4b8kwhqkcsd1wf43m413")))

