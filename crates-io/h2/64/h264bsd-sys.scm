(define-module (crates-io h2 #{64}# h264bsd-sys) #:use-module (crates-io))

(define-public crate-h264bsd-sys-0.1.0 (c (n "h264bsd-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1y6l21sl4fvh7fdsgwmwf51x839yfnxkq0qxcfl7ich3d88nqd2c")))

