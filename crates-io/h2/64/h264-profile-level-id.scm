(define-module (crates-io h2 #{64}# h264-profile-level-id) #:use-module (crates-io))

(define-public crate-h264-profile-level-id-0.0.0 (c (n "h264-profile-level-id") (v "0.0.0") (h "03jh2d6r5km2azx2ksxw5rmhsfprw6213zpq8nsq5wmcysiq8s5j")))

(define-public crate-h264-profile-level-id-0.1.0 (c (n "h264-profile-level-id") (v "0.1.0") (d (list (d (n "bitpattern") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0nk1clbmcygnsmv9682bxvnxjs0jbqx6y8jkbmi71m5l0whr8kap") (y #t)))

(define-public crate-h264-profile-level-id-0.1.1 (c (n "h264-profile-level-id") (v "0.1.1") (d (list (d (n "bitpattern") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "19dwpmxs5y49xyg8ly96v7idwgms53xhraadr7fvf52gzh1vqbp1")))

(define-public crate-h264-profile-level-id-0.2.0 (c (n "h264-profile-level-id") (v "0.2.0") (d (list (d (n "bitpattern") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0jd6fjpk8plglpk5m6q0ri6l70zrj446cf39n34n8yw4m7vdpl25")))

