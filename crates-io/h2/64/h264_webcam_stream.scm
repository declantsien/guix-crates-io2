(define-module (crates-io h2 #{64}# h264_webcam_stream) #:use-module (crates-io))

(define-public crate-h264_webcam_stream-0.1.0 (c (n "h264_webcam_stream") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 2)) (d (n "eyre") (r "^0.6.8") (d #t) (k 2)) (d (n "jpeg-decoder") (r "^0.3.0") (d #t) (k 0)) (d (n "openh264") (r "^0.3.0") (f (quote ("encoder" "decoder"))) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "v4l") (r "^0.13.1") (d #t) (k 0)))) (h "0qzll3f73k2pqgrjzi6wk9pjl9qpgsr2q66r4cvc7xk7jhdnvpjv")))

