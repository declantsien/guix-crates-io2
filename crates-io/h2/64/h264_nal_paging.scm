(define-module (crates-io h2 #{64}# h264_nal_paging) #:use-module (crates-io))

(define-public crate-h264_nal_paging-0.1.0 (c (n "h264_nal_paging") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)))) (h "1w3sjnwwd8h13b56fr9vxjy8775pzsn932qkvw5697jawz03v20k")))

(define-public crate-h264_nal_paging-0.1.1 (c (n "h264_nal_paging") (v "0.1.1") (d (list (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)))) (h "0j0yjnqsbbyv1z5rkxq36vh7fh1cz6k1c7jlzz8f1jxn8r2nfyws")))

(define-public crate-h264_nal_paging-0.1.2 (c (n "h264_nal_paging") (v "0.1.2") (d (list (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)))) (h "1y0fq80n1agkrddqlwa9zfn2ad8f35cwl12y1jm8qgsb48sxmxy4")))

(define-public crate-h264_nal_paging-0.1.3 (c (n "h264_nal_paging") (v "0.1.3") (d (list (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)))) (h "0m0j5a3pdzd6n5r8786dd9bhvgaflg7pg73h3vjvzy4jplma6j5m")))

(define-public crate-h264_nal_paging-0.1.4 (c (n "h264_nal_paging") (v "0.1.4") (d (list (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)))) (h "1iw7hq6vsk77r3x2ii8b52sbgjql4d0k7bm9w2cii7hsxb6r2s3k")))

(define-public crate-h264_nal_paging-0.1.5 (c (n "h264_nal_paging") (v "0.1.5") (d (list (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)))) (h "0dcriwdzczr0f6gcvdbad7hillv985x0fbhddqwmnm8n02833lyb") (f (quote (("test" "tokio/full"))))))

(define-public crate-h264_nal_paging-0.1.6 (c (n "h264_nal_paging") (v "0.1.6") (d (list (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)))) (h "13dv0vgz3550vb6ljar8g9f44yg3hp0l98wh4yw9ndx5gfn99iyf") (f (quote (("test" "tokio/full"))))))

(define-public crate-h264_nal_paging-0.1.7 (c (n "h264_nal_paging") (v "0.1.7") (d (list (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)))) (h "19679pjlhmpxwjzjxyv0wji0mzmm24il3bz4ahpbw5bbynvh7s9m") (f (quote (("test" "tokio/full"))))))

(define-public crate-h264_nal_paging-0.1.8 (c (n "h264_nal_paging") (v "0.1.8") (d (list (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)))) (h "03a73vhvr7cxl21kkxkqs7xv7yh42387g4yyh8yj3d643xqa25p2") (f (quote (("test" "tokio/full"))))))

(define-public crate-h264_nal_paging-0.1.9 (c (n "h264_nal_paging") (v "0.1.9") (d (list (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)))) (h "1d0bmsami2pk5kqkgfwcm1gxcb2bjv0rq3vgk7rhng8hyg78k2z7") (f (quote (("test" "tokio/full"))))))

(define-public crate-h264_nal_paging-0.1.10 (c (n "h264_nal_paging") (v "0.1.10") (d (list (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)))) (h "0qcgxi07wj8iyvcwljm8ppwlswqzsfmvixyy6fc6pdpivl3jpigs") (f (quote (("test" "tokio/full"))))))

(define-public crate-h264_nal_paging-0.1.11 (c (n "h264_nal_paging") (v "0.1.11") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)))) (h "000lxnlipm55fx9qygifk2bfc2xnjdrhsh5vqyh4hp8mpxmw265m") (f (quote (("test" "tokio/full"))))))

(define-public crate-h264_nal_paging-0.1.12 (c (n "h264_nal_paging") (v "0.1.12") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)))) (h "177i4c93kdar9pr9m7gkmkk69iam17qpa7qgdh68297gcyynyyq4") (f (quote (("test" "tokio/full"))))))

