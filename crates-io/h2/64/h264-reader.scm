(define-module (crates-io h2 #{64}# h264-reader) #:use-module (crates-io))

(define-public crate-h264-reader-0.2.0 (c (n "h264-reader") (v "0.2.0") (d (list (d (n "bitreader") (r "^0.3.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hex-literal") (r "^0.1.0") (d #t) (k 2)) (d (n "hex-slice") (r "^0.1.4") (d #t) (k 2)) (d (n "memchr") (r "^2.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)))) (h "0nmbhvzjk4m1xsk169kqcvcqfhcciqmws72ypgc7jbb8g0lvb5cz")))

(define-public crate-h264-reader-0.3.0 (c (n "h264-reader") (v "0.3.0") (d (list (d (n "bitreader") (r "^0.3.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hex-literal") (r "^0.1.0") (d #t) (k 2)) (d (n "hex-slice") (r "^0.1.4") (d #t) (k 2)) (d (n "memchr") (r "^2.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)))) (h "1pbk8hxkjz7xmvc254cq9z1f9ybjv4fnkyx1cw60h1c0ws4sxhc8")))

(define-public crate-h264-reader-0.4.0 (c (n "h264-reader") (v "0.4.0") (d (list (d (n "bitreader") (r "^0.3.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hex-literal") (r "^0.1.0") (d #t) (k 2)) (d (n "hex-slice") (r "^0.1.4") (d #t) (k 2)) (d (n "memchr") (r "^2.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)))) (h "0wx21gp80za90v7npsvd0bw32l2v3jl1rngakzhp3ljzawcq204w")))

(define-public crate-h264-reader-0.5.0 (c (n "h264-reader") (v "0.5.0") (d (list (d (n "bitreader") (r "^0.3.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.1") (d #t) (k 2)) (d (n "hex-slice") (r "^0.1.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memchr") (r "^2.1.1") (d #t) (k 0)) (d (n "rfc6381-codec") (r "^0.1") (d #t) (k 0)))) (h "188x15sb669pws0rn781yfl19xf8f4n1zgzi08cxb8zcpmlpdn68")))

(define-public crate-h264-reader-0.6.0 (c (n "h264-reader") (v "0.6.0") (d (list (d (n "bitstream-io") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)) (d (n "hex-slice") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memchr") (r "^2.1.1") (d #t) (k 0)) (d (n "rfc6381-codec") (r "^0.1") (d #t) (k 0)))) (h "0b1ww96c18hxpvmksl3nz8sfryv4grv22qvn3w1acx0v5y39bh53")))

(define-public crate-h264-reader-0.7.0 (c (n "h264-reader") (v "0.7.0") (d (list (d (n "bitstream-io") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "hex-literal") (r "^0.4.1") (d #t) (k 2)) (d (n "hex-slice") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memchr") (r "^2.1.1") (d #t) (k 0)) (d (n "rfc6381-codec") (r "^0.1") (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "1psh86y4zx7pqgz96isbdnpzlb79xkmijji56gy1rirc6b68s4dx")))

