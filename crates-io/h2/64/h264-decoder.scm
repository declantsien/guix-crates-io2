(define-module (crates-io h2 #{64}# h264-decoder) #:use-module (crates-io))

(define-public crate-h264-decoder-0.1.0 (c (n "h264-decoder") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "bytesio") (r "^0.2.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1rgljgrazivgbg718cv1a9rrmb0lpa15rljsnyfxm83m4b88qxyg")))

(define-public crate-h264-decoder-0.2.0 (c (n "h264-decoder") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "bytesio") (r "^0.3.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0sq3l1hrvijjc0k25rrjcfkxiaaxzihn2h55qlssgpjv7nkp4bg5")))

(define-public crate-h264-decoder-0.2.1 (c (n "h264-decoder") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "bytesio") (r "^0.3.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0wf11gl52a6shz0l6wdfkds8iwby6dwjvgvfp8iqsq0dkna47wy8")))

(define-public crate-h264-decoder-0.2.2 (c (n "h264-decoder") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "bytesio") (r "^0.3.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1vhayn94yr77ccady9fbv2m5g2lcyx9xrab0ignhq8dfn685xi4n")))

(define-public crate-h264-decoder-0.2.3 (c (n "h264-decoder") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "bytesio") (r "^0.3.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1h0ndwc8qjd5ah3pp622mkqmpwz5a7bknfnv5dv3rip3w21v701m")))

