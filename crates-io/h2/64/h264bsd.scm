(define-module (crates-io h2 #{64}# h264bsd) #:use-module (crates-io))

(define-public crate-h264bsd-0.1.0 (c (n "h264bsd") (v "0.1.0") (d (list (d (n "av-codec") (r "^0.3.0") (d #t) (k 0)) (d (n "av-data") (r "^0.4.1") (d #t) (k 0)) (d (n "h264bsd-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 2)))) (h "03arnpi50a6pmmf0slpyp2asi3vlxcw3law9icvscw53vf4dgcfg")))

(define-public crate-h264bsd-0.1.1 (c (n "h264bsd") (v "0.1.1") (d (list (d (n "av-codec") (r "^0.3.0") (d #t) (k 0)) (d (n "av-data") (r "^0.4.1") (d #t) (k 0)) (d (n "h264bsd-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 2)))) (h "0rxhmq043z000h8sr6hxb1hxj4y3bsm5vdzqx0fk7h8sln7j4gm5")))

