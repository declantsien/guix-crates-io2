(define-module (crates-io h2 sr h2sr) #:use-module (crates-io))

(define-public crate-h2sr-0.1.0 (c (n "h2sr") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "directories") (r "^3.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "ipnet") (r "^2.3") (d #t) (k 0)) (d (n "iprange") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-socks") (r "^0.5") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "14kqmykcr5ikbhlhxsjvk18figx5hy4x7zp753khjwfcjvlafdhy")))

