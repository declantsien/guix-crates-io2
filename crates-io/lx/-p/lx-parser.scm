(define-module (crates-io lx -p lx-parser) #:use-module (crates-io))

(define-public crate-lx-parser-0.1.0 (c (n "lx-parser") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0f7wlm00gjjr95b99bhg3vd5axk6pc23qj2i3gqccdmg8198srqr")))

