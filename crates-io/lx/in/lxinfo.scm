(define-module (crates-io lx in lxinfo) #:use-module (crates-io))

(define-public crate-lxinfo-0.1.2 (c (n "lxinfo") (v "0.1.2") (d (list (d (n "byte-unit") (r "^4.0.18") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "15fd6p5a5ry0bw5i6n2l2lf0cc8jgcxgdjkp0yz8d32if1apxvpg")))

