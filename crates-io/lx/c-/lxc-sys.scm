(define-module (crates-io lx c- lxc-sys) #:use-module (crates-io))

(define-public crate-lxc-sys-0.1.0 (c (n "lxc-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.36") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18yf8vp7kh6x5nwc4qn2hm1607n7bqfjbzixvnwi93dkgyykds6p")))

(define-public crate-lxc-sys-0.1.1 (c (n "lxc-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.36") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0i92w04i39k1ipazpmb47hzij7mw3nz2g2glm4g1ip92piynf7dg")))

(define-public crate-lxc-sys-0.2.0 (c (n "lxc-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18aranwvp1hibhq3v60ic8cjv42r552mi186sddd489c2hi2bbpi")))

(define-public crate-lxc-sys-0.2.1 (c (n "lxc-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "16kjspi7cpzlb04zilv651mmylxyd04i3vxd93l37ckhsa4c1mh2")))

(define-public crate-lxc-sys-0.3.0 (c (n "lxc-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1f7yy09ai2wgvm5mh759jy6k2rjqfnnk89f10abdb0w4r5x5n1il")))

(define-public crate-lxc-sys-0.4.0 (c (n "lxc-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05nr2y4dawfgaiabkm37y3wp0rqbkrv0w3sla4nh2y5zrr3kq7x3") (l "lxc")))

(define-public crate-lxc-sys-0.5.0 (c (n "lxc-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "172d7gl0lwq4xjix7ifv750za87bc7bfx2gvln20ibkj25v4y9aw") (l "lxc")))

