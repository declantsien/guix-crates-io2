(define-module (crates-io lx cr lxcrond) #:use-module (crates-io))

(define-public crate-lxcrond-0.2.0 (c (n "lxcrond") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "cmdline_words_parser") (r "^0.1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "log4rs") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1rzj95ppgxz6hn3i4mx6km5q74aamxhp9wdghbp3nv071nc8jyyf")))

(define-public crate-lxcrond-0.2.1 (c (n "lxcrond") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "cmdline_words_parser") (r "^0.1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "log4rs") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "04l97icvl2aaw4shh6797v5y1zbm5dw25cd4md8gcyc709k1d7xd")))

