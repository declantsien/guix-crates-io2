(define-module (crates-io bu na buna) #:use-module (crates-io))

(define-public crate-buna-0.1.1 (c (n "buna") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2.4") (f (quote ("macros"))) (d #t) (k 0)))) (h "02xj1031l4qw2j6j6w0d9ca5kvyj0hl10iv9cjy83mgnhichcn65")))

