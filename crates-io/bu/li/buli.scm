(define-module (crates-io bu li buli) #:use-module (crates-io))

(define-public crate-buli-0.1.0 (c (n "buli") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "08qmvhywhmm2izqw2jjaxlvchp5mjcvc8h2ajc29m3wdqw59pvzs")))

(define-public crate-buli-0.1.1 (c (n "buli") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json"))) (d #t) (k 0)))) (h "02k0jyh779w6fkl8kharv9a9q7xrhys94i4d22m421z21mq62xns")))

