(define-module (crates-io bu ff buffer_io) #:use-module (crates-io))

(define-public crate-buffer_io-1.0.0 (c (n "buffer_io") (v "1.0.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.6") (d #t) (k 0)))) (h "1h77hssi0km6yjcbs9zighb18kpn62gdxkhg76hvzcmzhckdkw2c")))

(define-public crate-buffer_io-1.0.1 (c (n "buffer_io") (v "1.0.1") (h "1w3h3kj2kr2fmjrh145rcz00dxi12bizdh4596qfga1yzvmvlnjv")))

(define-public crate-buffer_io-1.0.2 (c (n "buffer_io") (v "1.0.2") (d (list (d (n "err-derive") (r "^0.2.4") (d #t) (k 0)))) (h "1aq7q377l1pnlakhg4acx1k49m6pp60n12pgjhxv5myxrlgaicnz")))

(define-public crate-buffer_io-1.0.3 (c (n "buffer_io") (v "1.0.3") (d (list (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0gvc6dhq4b1yxxp0bdh8s9law32286s39diwf8m7mmj0xb9m6cz3")))

