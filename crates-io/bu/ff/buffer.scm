(define-module (crates-io bu ff buffer) #:use-module (crates-io))

(define-public crate-buffer-0.1.0 (c (n "buffer") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.3.16") (d #t) (k 0)) (d (n "mac") (r "^0.0.2") (d #t) (k 0)))) (h "1vvjygc9ms85iyf7w9q1sxdm3p297gx9qajk1zng15y0k77qghq0")))

(define-public crate-buffer-0.1.1 (c (n "buffer") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.3.16") (d #t) (k 0)) (d (n "mac") (r "^0.0.2") (d #t) (k 0)))) (h "0jxwkk8cwp4dhiizd0130wb7j4c4ld66vbz4yhvzcrd5l8c98hx4")))

(define-public crate-buffer-0.1.2 (c (n "buffer") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.3.7") (d #t) (k 0)) (d (n "mac") (r "^0.0.2") (d #t) (k 0)))) (h "00jz5dswdb1pwad3zgjh70mzprvmbvkgbwnmv71266d14n17z99z")))

(define-public crate-buffer-0.1.3 (c (n "buffer") (v "0.1.3") (d (list (d (n "arrayvec") (r "^0.3.7") (d #t) (k 0)) (d (n "mac") (r "^0.0.2") (d #t) (k 0)))) (h "0wzkvd57dy9yxmw3zv9q6j2n29g1zr9pdqm2agixw9lhzd3alkbl")))

(define-public crate-buffer-0.1.4 (c (n "buffer") (v "0.1.4") (d (list (d (n "arrayvec") (r "^0.3.7") (d #t) (k 0)) (d (n "mac") (r "^0.0.2") (d #t) (k 0)))) (h "00sh83rp3jmjmj9m61kpxs9z46fscdnyg8dz5bxfwlbyas29yrsc")))

(define-public crate-buffer-0.1.5 (c (n "buffer") (v "0.1.5") (d (list (d (n "arrayvec") (r "^0.3.7") (d #t) (k 0)) (d (n "mac") (r "^0.0.2") (d #t) (k 0)))) (h "1wa0b8kcx0b3z5mf6y8jwx61d7ml8qxcsdv38sb4hwr0d2x47vbh")))

(define-public crate-buffer-0.1.6 (c (n "buffer") (v "0.1.6") (d (list (d (n "arrayvec") (r "^0.3.7") (d #t) (k 0)) (d (n "mac") (r "^0.0.2") (d #t) (k 0)))) (h "0bbdx3041ggjhmxximxvzw95z8mg72jw96dgkfrwswya6sbfcvyj")))

(define-public crate-buffer-0.1.7 (c (n "buffer") (v "0.1.7") (d (list (d (n "arrayvec") (r "^0.3.7") (d #t) (k 0)) (d (n "mac") (r "^0.0.2") (d #t) (k 0)))) (h "0wy0nimj8gdjkcil6hqlp77hdhj2c0bcs55kxv3pyqi8rxk1z9hj")))

(define-public crate-buffer-0.1.8 (c (n "buffer") (v "0.1.8") (d (list (d (n "arrayvec") (r "^0.3.7") (d #t) (k 0)) (d (n "mac") (r "^0.0.2") (d #t) (k 0)))) (h "012k1cvfsi25yiwyv5pm94lnr9mf7ff6rxjljscn88ffrsf99wn2")))

(define-public crate-buffer-0.1.9 (c (n "buffer") (v "0.1.9") (d (list (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "mac") (r "^0.0.2") (d #t) (k 0)))) (h "0a6zj64mfvcvl3xwpygmmmfpnm68c6a1vfny1bj5pndm6a6j5dxa")))

