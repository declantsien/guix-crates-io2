(define-module (crates-io bu ff bufferring) #:use-module (crates-io))

(define-public crate-bufferring-0.0.1 (c (n "bufferring") (v "0.0.1") (h "12i71xywph0l7k9kyyj4xb9h2ql2gk9y4driana928sfl4y84d5a") (f (quote (("default" "alloc") ("alloc")))) (r "1.68")))

(define-public crate-bufferring-0.0.2 (c (n "bufferring") (v "0.0.2") (d (list (d (n "bit-vec") (r "^0.6.3") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^1.1") (f (quote ("std" "bit-set" "break-dead-code"))) (o #t) (k 0)))) (h "1f67549xqdiqmjlfc8bhdprm2nsl6z8pmi9pbk5wcqg1wbafzi9r") (f (quote (("default" "alloc") ("alloc")))) (s 2) (e (quote (("proptest" "dep:proptest" "dep:bit-vec")))) (r "1.68")))

