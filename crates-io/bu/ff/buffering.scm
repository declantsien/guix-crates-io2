(define-module (crates-io bu ff buffering) #:use-module (crates-io))

(define-public crate-buffering-0.1.0 (c (n "buffering") (v "0.1.0") (h "1s2zwsmg2qqfazysdsxa0yjpy0imm6gvb62awdi5qw8676ffqsyk")))

(define-public crate-buffering-0.2.0 (c (n "buffering") (v "0.2.0") (d (list (d (n "buffering_nocopy_macro") (r "^0.1.0") (d #t) (k 0)))) (h "1znqs4y8ka5zr9ni50330isz7xgcvk0lkraqpf8sqx7vi7a6ihj4")))

(define-public crate-buffering-0.3.0 (c (n "buffering") (v "0.3.0") (d (list (d (n "buffering_nocopy_macro") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0hlypq47mas99aahmq7llmqj9bg2vxb7kw26xqzrsknpb24jd1j9") (f (quote (("nocopy" "buffering_nocopy_macro") ("copy"))))))

(define-public crate-buffering-0.3.1 (c (n "buffering") (v "0.3.1") (d (list (d (n "buffering_nocopy_macro") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0yr23719nr9yx1g9qf2hrskbwz0kw323ypwdipc6wd0crl88rfmz") (f (quote (("nocopy" "buffering_nocopy_macro") ("copy"))))))

(define-public crate-buffering-0.3.2 (c (n "buffering") (v "0.3.2") (d (list (d (n "buffering_nocopy_macro") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 2)))) (h "07qy88sf1sr35ihjl2hrmfzlch0cl8w26f10mbj6wcjzdwy49gwa") (f (quote (("nocopy" "buffering_nocopy_macro") ("copy"))))))

(define-public crate-buffering-0.3.3 (c (n "buffering") (v "0.3.3") (d (list (d (n "buffering_nocopy_macro") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 2)))) (h "129bv50nysnf99qqf0y6100mij1vz6vlp15yzlqsv9phm1hgdbb8") (f (quote (("nocopy" "buffering_nocopy_macro") ("copy"))))))

(define-public crate-buffering-0.3.4 (c (n "buffering") (v "0.3.4") (d (list (d (n "buffering_nocopy_macro") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 2)))) (h "11cyq2nm569j028g93jxk00h8nfwc3l6n3hqw79w34na01p3lq97") (f (quote (("nocopy" "buffering_nocopy_macro") ("copy"))))))

(define-public crate-buffering-0.3.4-r1 (c (n "buffering") (v "0.3.4-r1") (d (list (d (n "buffering_nocopy_macro") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 2)))) (h "04dp2hnbkz578vwrvjxy8p4wqiab34d4ar7rdgmj6iys4kij15ac") (f (quote (("nocopy" "buffering_nocopy_macro") ("copy"))))))

(define-public crate-buffering-0.4.0 (c (n "buffering") (v "0.4.0") (d (list (d (n "buffering_nocopy_macro") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 2)))) (h "1x4nkwc3dx55s2ndfkvgdp8makqnp95vndn9q2bk6h8m7bs2y8c3") (f (quote (("nocopy" "buffering_nocopy_macro") ("copy")))) (y #t)))

(define-public crate-buffering-0.4.1 (c (n "buffering") (v "0.4.1") (d (list (d (n "buffering_nocopy_macro") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 2)))) (h "0h37mbdap5i23k98r5cl6fid0r0p9601yy91l0v29kymmkx8mxgf") (f (quote (("nocopy" "buffering_nocopy_macro") ("copy")))) (y #t)))

(define-public crate-buffering-0.4.2 (c (n "buffering") (v "0.4.2") (d (list (d (n "buffering_nocopy_macro") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 2)))) (h "1wmgy84j5scbndzx7mccz07z34n98fwnfah1zd5kzqjaif5iqk76") (f (quote (("nocopy" "buffering_nocopy_macro") ("copy"))))))

(define-public crate-buffering-0.5.0 (c (n "buffering") (v "0.5.0") (d (list (d (n "buffering_nocopy_macro") (r "^0.2.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 2)))) (h "0j31kgliy9v3wygkmwvc1liw37rvdwh5lsb2a1a0bha5rz0y3sm1")))

