(define-module (crates-io bu ff buffertk) #:use-module (crates-io))

(define-public crate-buffertk-0.1.0 (c (n "buffertk") (v "0.1.0") (h "12272wqy6w35yi1v3p3h52bk9hrha53x0j57mrybwa9vinw8wnvy")))

(define-public crate-buffertk-0.2.0 (c (n "buffertk") (v "0.2.0") (h "0jfxww9d9pbagafnv4chdgh3w1l2s2na7ssfna89ghz4nxb39ikd")))

(define-public crate-buffertk-0.3.0 (c (n "buffertk") (v "0.3.0") (h "0ssd22k8q8j8ldv3dg3raahzvjbrlg3ryj6yrxyhxyfw2nb410g4")))

(define-public crate-buffertk-0.4.0 (c (n "buffertk") (v "0.4.0") (h "0hqgmm7rd7xbq3j8xr5zbggg2dsjdwd8b8p49chiadzpiy8bd8rl")))

(define-public crate-buffertk-0.5.0 (c (n "buffertk") (v "0.5.0") (h "0391y94477wshw22njxxkk2kg9cgkl2jh5m0gac9gfqs7sfn4dz1") (f (quote (("default" "binaries") ("binaries"))))))

(define-public crate-buffertk-0.6.0 (c (n "buffertk") (v "0.6.0") (h "1i9a7phvpmn156xc2mm1d0r7s2803ir3qkf8ndfhlsg34cfxcqpi") (f (quote (("default" "binaries") ("binaries"))))))

