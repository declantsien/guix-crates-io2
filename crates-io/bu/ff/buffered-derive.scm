(define-module (crates-io bu ff buffered-derive) #:use-module (crates-io))

(define-public crate-buffered-derive-0.1.0 (c (n "buffered-derive") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "127kgmp8nm3cq11vp0z25cchq2k3jrg9y56jyb5kjwx96s7pv218")))

