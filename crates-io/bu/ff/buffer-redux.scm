(define-module (crates-io bu ff buffer-redux) #:use-module (crates-io))

(define-public crate-buffer-redux-1.0.0 (c (n "buffer-redux") (v "1.0.0") (d (list (d (n "memchr") (r "^2.0") (d #t) (k 0)) (d (n "safemem") (r "^0.3") (d #t) (k 0)) (d (n "slice-deque") (r "^0.3") (o #t) (d #t) (t "cfg(any(unix, windows))") (k 0)))) (h "1j90lnf4iz50px57xpxc6bx8hnmb6fyjm50nq6m8qn892nh6x26j") (f (quote (("default" "slice-deque"))))))

(define-public crate-buffer-redux-1.0.1 (c (n "buffer-redux") (v "1.0.1") (d (list (d (n "memchr") (r "^2.0") (d #t) (k 0)) (d (n "slice-deque") (r "^0.3") (o #t) (d #t) (t "cfg(any(unix, windows))") (k 0)))) (h "1xal51dkq53pz5f9rhyfy4a1knmhqfgadbg7s68j78g04bfqv7sc") (f (quote (("default" "slice-deque"))))))

