(define-module (crates-io bu ff buffer-trigger) #:use-module (crates-io))

(define-public crate-buffer-trigger-0.1.0 (c (n "buffer-trigger") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0j1902v4gw40ypdmd8q23a39chm5n3j3c834mz031qh9znnnzgnp")))

(define-public crate-buffer-trigger-0.2.0 (c (n "buffer-trigger") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0864phjl0yx3qwipia960vashkk75d5b169m6xjbbp7hc34djskz")))

(define-public crate-buffer-trigger-0.3.0 (c (n "buffer-trigger") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "09jqjspxim3nwvk4iy1hb1n5k4j5l26c0yq68kzglvf09lsq2zlx")))

(define-public crate-buffer-trigger-0.4.0 (c (n "buffer-trigger") (v "0.4.0") (d (list (d (n "async-std") (r "^1.6") (f (quote ("unstable" "attributes"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0syzyhaa0c0xacbwsgqn8g8h4r303174dyhb8k5y3hgqxdbfyjqy")))

(define-public crate-buffer-trigger-0.5.0 (c (n "buffer-trigger") (v "0.5.0") (d (list (d (n "async-std") (r "^1.6") (f (quote ("unstable" "attributes"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1jw2yd1pxlqvmv0qxqy7k1pnjg6iv54bx74al5jwxigsv2clh8ld")))

(define-public crate-buffer-trigger-0.6.0 (c (n "buffer-trigger") (v "0.6.0") (d (list (d (n "async-std") (r "^1.6") (f (quote ("unstable" "attributes"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "lifetime-thread") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "14i9n0f78kbmw5n2b54h2660zpas7iamgr0m03qxrmwsy49195iq")))

(define-public crate-buffer-trigger-0.7.0 (c (n "buffer-trigger") (v "0.7.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "lifetime-thread") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11wdcryha9wh3s1xhjcjpiha4wrnpikdxyj5w5v5cryajryy0p7d")))

