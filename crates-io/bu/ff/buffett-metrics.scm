(define-module (crates-io bu ff buffett-metrics) #:use-module (crates-io))

(define-public crate-buffett-metrics-0.1.0 (c (n "buffett-metrics") (v "0.1.0") (d (list (d (n "buffett-timing") (r "^0.1.1") (d #t) (k 0)) (d (n "influx_db_client") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "sys-info") (r "^0.5.6") (d #t) (k 0)))) (h "037j4w63r2mjpmvqvn6vg0df75frnsf3xfp83pflm13vq1ny3q7x")))

(define-public crate-buffett-metrics-0.1.1 (c (n "buffett-metrics") (v "0.1.1") (d (list (d (n "buffett-timing") (r "^0.1.1") (d #t) (k 0)) (d (n "influx_db_client") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "sys-info") (r "^0.5.6") (d #t) (k 0)))) (h "0d83753nrfask3250w6j7x6136n72hd385hb9g0caqcibhj4njr2")))

(define-public crate-buffett-metrics-0.1.2 (c (n "buffett-metrics") (v "0.1.2") (d (list (d (n "buffett-timing") (r "^0.1.1") (d #t) (k 0)) (d (n "influx_db_client") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "sys-info") (r "^0.5.6") (d #t) (k 0)))) (h "1v6yn3f3436g30dg81hkbdj915vzsr20wr3234sycvqxqjx88381")))

(define-public crate-buffett-metrics-0.1.3 (c (n "buffett-metrics") (v "0.1.3") (d (list (d (n "buffett-timing") (r "^0.1.1") (d #t) (k 0)) (d (n "influx_db_client") (r "^0.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "sys-info") (r "^0.5.6") (d #t) (k 0)))) (h "14hfrfwp8g4kxqvh19sdsd5qdjbxyvdrrsj9jvn3n127qc2ccm3k")))

