(define-module (crates-io bu ff buffett-budget) #:use-module (crates-io))

(define-public crate-buffett-budget-0.1.0 (c (n "buffett-budget") (v "0.1.0") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "buffett-interface") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)))) (h "0j0ahzr5z0wmmyc7p4hj9w38958dpk2rn6dllkckcwh6na4b5fl6") (y #t)))

(define-public crate-buffett-budget-0.1.1 (c (n "buffett-budget") (v "0.1.1") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "buffett-interface") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)))) (h "1iyfcbhhz39sg36plg80pw35m40rhwi4x55k5bcbb3xpiymr63gw")))

(define-public crate-buffett-budget-0.1.2 (c (n "buffett-budget") (v "0.1.2") (d (list (d (n "bincode") (r "^1.1.2") (d #t) (k 0)) (d (n "buffett-interface") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)))) (h "132afvx0pn77pbaqbcg0bkfcfmijd3mmhmv6dp76k4jfx12zm35l")))

