(define-module (crates-io bu ff buffered) #:use-module (crates-io))

(define-public crate-buffered-0.1.0 (c (n "buffered") (v "0.1.0") (d (list (d (n "buffered-derive") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0h0lsi0pgx1wkbmh937hlzb1v1qym9wrq1rdai2ada9lgvbb86wd")))

