(define-module (crates-io bu ff buffer_sv2) #:use-module (crates-io))

(define-public crate-buffer_sv2-0.1.0 (c (n "buffer_sv2") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "17cwkbw796pcp79w7aqfgwd4zvxaw8qrm0dbbdpnw6173dn7ya8b") (f (quote (("fuzz") ("debug"))))))

(define-public crate-buffer_sv2-0.1.1 (c (n "buffer_sv2") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.89") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "13bs1kifypjjbs3xbh5r04irxr5rl2b3am4afjzakd01k5gm5c0y") (f (quote (("with_serde" "serde") ("fuzz") ("debug"))))))

(define-public crate-buffer_sv2-0.1.3 (c (n "buffer_sv2") (v "0.1.3") (d (list (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.89") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "152yb8bz14zs8q6dv3x3ja5zm5mz9q9filjkma3ndzzz7b1n5b8l") (f (quote (("with_serde" "serde") ("fuzz") ("debug"))))))

(define-public crate-buffer_sv2-1.0.0 (c (n "buffer_sv2") (v "1.0.0") (d (list (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.89") (f (quote ("derive" "alloc"))) (o #t) (k 0)))) (h "1w30l4ba983r1qv95w5fc9bisi7nnmwjr06jf4azxgmvbg5d53xv") (f (quote (("with_serde" "serde") ("fuzz") ("debug"))))))

