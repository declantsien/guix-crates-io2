(define-module (crates-io bu ff buffer_display) #:use-module (crates-io))

(define-public crate-buffer_display-0.1.0 (c (n "buffer_display") (v "0.1.0") (d (list (d (n "vulkano") (r "^0.34.0") (d #t) (k 0)) (d (n "vulkano-macros") (r "^0.34.0") (d #t) (k 0)) (d (n "vulkano-shaders") (r "^0.34.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.34.0") (d #t) (k 0)) (d (n "winit") (r "^0.28.0") (d #t) (k 0)))) (h "0g0cwh2rlr1r0n0vw7sj4fj9riqh983dsp7cwjf9gchnj9q8psys")))

