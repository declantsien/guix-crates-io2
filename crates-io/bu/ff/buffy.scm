(define-module (crates-io bu ff buffy) #:use-module (crates-io))

(define-public crate-buffy-0.0.1 (c (n "buffy") (v "0.0.1") (d (list (d (n "crossterm") (r "^0.18.2") (d #t) (k 2)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "0cg7jxawd7xprmsxph2n944jbp5qfd8wbdkx3m8yylph8z800c9f")))

(define-public crate-buffy-0.0.2 (c (n "buffy") (v "0.0.2") (d (list (d (n "crossterm") (r "^0.18.2") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.18.2") (d #t) (k 2)))) (h "1i89wxazd94gh0v48f92sj6x122qr8pg5q663vlbpnzh2jjixbnb") (f (quote (("with_crossterm" "crossterm"))))))

