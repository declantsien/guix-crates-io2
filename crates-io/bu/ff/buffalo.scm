(define-module (crates-io bu ff buffalo) #:use-module (crates-io))

(define-public crate-buffalo-0.1.0 (c (n "buffalo") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "buffalo_macro") (r "^0.1") (d #t) (k 0)) (d (n "derive_deref") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1a5g6kksm1iba364nbaisjxc1bg2jgvwqwy711l56lv05n3ch9m7") (f (quote (("ndarray_0_15" "ndarray") ("bitvec_0_19" "bitvec"))))))

(define-public crate-buffalo-0.2.0 (c (n "buffalo") (v "0.2.0") (d (list (d (n "bitvec") (r "^0.22") (o #t) (d #t) (k 0)) (d (n "buffalo_macro") (r "^0.1") (d #t) (k 0)) (d (n "derive_deref") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1v6bczq9mkqpdlmb2q3sv60kw2ysck0iz7bi2f5p6rwq1qylkbxm") (f (quote (("ndarray_0_15" "ndarray") ("bitvec_0_22" "bitvec"))))))

(define-public crate-buffalo-0.3.0 (c (n "buffalo") (v "0.3.0") (d (list (d (n "bitvec") (r "^1") (o #t) (d #t) (k 0)) (d (n "buffalo_macro") (r "^0.1") (d #t) (k 0)) (d (n "derive_deref") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1vfs45fc51lv7qadj9y13z5m4nx72izj3q9fb0rk722637k4pb6g") (f (quote (("ndarray_0_15" "ndarray")))) (r "1.57")))

(define-public crate-buffalo-0.3.1 (c (n "buffalo") (v "0.3.1") (d (list (d (n "bitvec") (r "^1") (o #t) (d #t) (k 0)) (d (n "buffalo_macro") (r "^0.1") (d #t) (k 0)) (d (n "derive_deref") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "151b0i2w3wmr5fz9dk0ds91hnf4ic175shgphpx18gchd17fmg51") (f (quote (("ndarray_0_15" "ndarray")))) (r "1.57")))

(define-public crate-buffalo-0.4.0 (c (n "buffalo") (v "0.4.0") (d (list (d (n "bitvec") (r "^1") (o #t) (d #t) (k 0)) (d (n "buffalo_macro") (r "^0.1") (d #t) (k 0)) (d (n "derive_deref") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0ags9j6vg7fi0ar3370ran7pwmyvxjzabdvdihcz8xf5mv9r8zhb") (r "1.57")))

(define-public crate-buffalo-0.4.1 (c (n "buffalo") (v "0.4.1") (d (list (d (n "bitvec") (r "^1") (o #t) (d #t) (k 0)) (d (n "buffalo_macro") (r "^0.1") (d #t) (k 0)) (d (n "derive_deref") (r "^1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "13cviphzwj8zsb0abzwsliy2qizl6ajw5f8bpii71iyclk5xgs4h") (r "1.57")))

