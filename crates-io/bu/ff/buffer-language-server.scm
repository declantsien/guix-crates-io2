(define-module (crates-io bu ff buffer-language-server) #:use-module (crates-io))

(define-public crate-buffer-language-server-0.1.0 (c (n "buffer-language-server") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tower-lsp") (r "^0.19.0") (d #t) (k 0)))) (h "0acvzi8nxm6q0kdl3h9jfqgc0l8qzw0jiwfq903rrd0w8d51xdpi")))

