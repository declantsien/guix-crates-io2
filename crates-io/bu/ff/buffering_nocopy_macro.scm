(define-module (crates-io bu ff buffering_nocopy_macro) #:use-module (crates-io))

(define-public crate-buffering_nocopy_macro-0.1.0 (c (n "buffering_nocopy_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.4") (d #t) (k 0)) (d (n "syn") (r "^0.14.5") (d #t) (k 0)))) (h "0kp56p002jv0wd2wfimsfpp8sxxknfrwaqqpxgh0vk6r44kr9ik3")))

(define-public crate-buffering_nocopy_macro-0.1.1 (c (n "buffering_nocopy_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.9") (d #t) (k 0)) (d (n "quote") (r "^0.6.4") (d #t) (k 0)) (d (n "syn") (r "^0.14.5") (d #t) (k 0)))) (h "03b6wp4i3273jzw2nbnrnddlxyma3p465y2dszx6vc3ysq0fdlj7")))

(define-public crate-buffering_nocopy_macro-0.2.0 (c (n "buffering_nocopy_macro") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hb08bmj3r1ajfibyp8g0ay3xkxfa9px2yr64hssmx822mws5kp3") (y #t)))

(define-public crate-buffering_nocopy_macro-0.2.1 (c (n "buffering_nocopy_macro") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "17w596kjg6qc62sysrf1c62d1ljq4sad41s4173sgfvaldj9skid")))

(define-public crate-buffering_nocopy_macro-0.2.2 (c (n "buffering_nocopy_macro") (v "0.2.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0a8zs7s5z0f977g7zx95b8sf8gqa8iwh13rj639wk080d00l5kaa")))

