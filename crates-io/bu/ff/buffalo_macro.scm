(define-module (crates-io bu ff buffalo_macro) #:use-module (crates-io))

(define-public crate-buffalo_macro-0.1.0 (c (n "buffalo_macro") (v "0.1.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1n5ik2blp9g3v5py4wax800yc5f9wixaqmrgcwldzp2mngskyijg")))

