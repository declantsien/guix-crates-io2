(define-module (crates-io bu ff buffers) #:use-module (crates-io))

(define-public crate-buffers-0.0.0 (c (n "buffers") (v "0.0.0") (h "1x0qraykgc6xi5s18av89p635nr1qnw9kw28n9m7kvwd9rq0xz8k")))

(define-public crate-buffers-0.1.2 (c (n "buffers") (v "0.1.2") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)))) (h "0xr40wpz7f2ys8xxq9cy2ikyjs3jxy1wkmjpfsc8x8jycc0s357a")))

