(define-module (crates-io bu ff bufferpool) #:use-module (crates-io))

(define-public crate-bufferpool-0.1.1 (c (n "bufferpool") (v "0.1.1") (h "1lfj1bkhw5zx3jmd75xps8cncrqhzdhiv7a1ik228pav4xjr16s6")))

(define-public crate-bufferpool-0.1.2 (c (n "bufferpool") (v "0.1.2") (h "1y687vk5n03jdcdf22b24vr6m2x89hw1zh92135apqs39a7x1q1r")))

(define-public crate-bufferpool-0.1.3 (c (n "bufferpool") (v "0.1.3") (h "00brn5wfdg359giqmz0brgajsc5bblj84xvip0lizlxjpdk0iir4")))

(define-public crate-bufferpool-0.1.4 (c (n "bufferpool") (v "0.1.4") (h "0qf5r72xm453awzqh9lknl99d0jdiv53ldq7x9iq35h1h3d3dwfs")))

(define-public crate-bufferpool-0.1.5 (c (n "bufferpool") (v "0.1.5") (h "059wv8dvnpq9f332pxyvg8x063ffkymfzwd7izvjhaf45ajx9hai")))

(define-public crate-bufferpool-0.1.6 (c (n "bufferpool") (v "0.1.6") (h "0acmdmdhys9wqyldkldbkx48ivw0mw0jilzlqcif1n7vxpcvgdry")))

(define-public crate-bufferpool-0.1.7 (c (n "bufferpool") (v "0.1.7") (h "1pgd08v5826mihvg0q4p5zx43ygay88m383gkhpv3gc6xd5b2qnb")))

