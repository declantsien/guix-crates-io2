(define-module (crates-io bu ff buffett-interface) #:use-module (crates-io))

(define-public crate-buffett-interface-0.1.0 (c (n "buffett-interface") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "bs58") (r "^0.2.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (f (quote ("serde"))) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)))) (h "1z41cypsyvwi8p9ag1aaghzhyvnmqiqjzbvmvvsnjq4s371m9li8")))

