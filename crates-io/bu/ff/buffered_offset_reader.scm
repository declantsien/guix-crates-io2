(define-module (crates-io bu ff buffered_offset_reader) #:use-module (crates-io))

(define-public crate-buffered_offset_reader-0.1.0 (c (n "buffered_offset_reader") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1lip3p4hah919w43shsf45ai2ag8q5s72z5hl9j436r01gj1j2si")))

(define-public crate-buffered_offset_reader-0.1.1 (c (n "buffered_offset_reader") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0dn4h4g5i5f5k58ixd9mak9pwxdpqkaabxl50ag744v0yf7wdy49")))

(define-public crate-buffered_offset_reader-0.2.0 (c (n "buffered_offset_reader") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "08crap64psy55hvx0wxb2ffz3cibyvpph8m80kwmxplc59msidim")))

(define-public crate-buffered_offset_reader-0.3.0 (c (n "buffered_offset_reader") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "0dijqijw6jk8khgqjbjknyvxvn5j3vr25389djn7i2fkdqc1a1zd")))

(define-public crate-buffered_offset_reader-0.4.0 (c (n "buffered_offset_reader") (v "0.4.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "13ibnk32jq0jigrxz35f75ahhiyb8jgpraf332q18jaz5qh20919")))

(define-public crate-buffered_offset_reader-0.5.0 (c (n "buffered_offset_reader") (v "0.5.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1hd91sn81lwwc2f4wka506918nyvvkab754ichb78wbbb3r74iaa")))

(define-public crate-buffered_offset_reader-0.6.0 (c (n "buffered_offset_reader") (v "0.6.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)))) (h "1cf0ck6x1wd7g2h84mns6lva6j0m4f7gkhpwj8kaqj72l3br6rdb")))

