(define-module (crates-io bu ff buffered_logger) #:use-module (crates-io))

(define-public crate-buffered_logger-1.0.0 (c (n "buffered_logger") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)) (d (n "permissions") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1kz2i1x12djl1dymmxcp4hp69a30rx9hsriyxd83q1ggcq4gbph8")))

(define-public crate-buffered_logger-1.1.0 (c (n "buffered_logger") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1af2nw6xl30gb0sfhjag6vfj9xmrqcp6qhzi7l5zl29lh8vv3lg1")))

(define-public crate-buffered_logger-1.2.0 (c (n "buffered_logger") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "146hd6yzbyi2rdv9gck7by7s8w9pc3vwlak190r5xjlknvq4zw2y")))

