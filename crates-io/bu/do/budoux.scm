(define-module (crates-io bu do budoux) #:use-module (crates-io))

(define-public crate-budoux-0.0.1 (c (n "budoux") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1vf2qhh8p6qrk6idmc2rrssfrlligfhhdhvnzsjibkdlm8laa9w0")))

(define-public crate-budoux-0.0.2 (c (n "budoux") (v "0.0.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0dm8l32h447rsgrxvifpc5ramrblwm03ihc1bw8i7x6mgkfg8nhl")))

(define-public crate-budoux-0.0.3 (c (n "budoux") (v "0.0.3") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "178xdpvs3hjj7jjlbn4vvf3yhkai7pc8z6r1kj1ir2zcqkvylk75")))

(define-public crate-budoux-0.0.4 (c (n "budoux") (v "0.0.4") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "1bf2cnkhwx80ip9rsynq7g9cdpl70imiban8j2ajzzidc0bl0h9d")))

(define-public crate-budoux-0.1.0 (c (n "budoux") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "1n5m7hw8ls2lnq36x5m0zivj91rinsnx4acjp6bxlqn6rh9kpdmd")))

(define-public crate-budoux-0.1.1 (c (n "budoux") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "1h3zwvzdsazyr9vkvasdvb9rnll7z3gg6xwvcm2s4pfmmfnda23a")))

