(define-module (crates-io bu dg budget-tracker) #:use-module (crates-io))

(define-public crate-budget-tracker-0.1.0 (c (n "budget-tracker") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.3") (d #t) (k 0)))) (h "12jik8f3habfbc8ckpw6hzysqr5ds3b8pnf2lli5pmdz7yg472wb")))

(define-public crate-budget-tracker-0.2.0 (c (n "budget-tracker") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.3") (d #t) (k 0)))) (h "0377vc5qjffi0c44biirdw6pjrcbxa0bdh0a330nhy4qjsqiza8h")))

(define-public crate-budget-tracker-0.3.1 (c (n "budget-tracker") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x8anwgm9glgfik170a7mkzndadb0gxhs4qmg1svyzyqp33q51f5")))

(define-public crate-budget-tracker-0.4.0 (c (n "budget-tracker") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mc7pcmpkg6y3x3rklyjmbpqdld647g0fg8217hpj9vdb209irl5")))

(define-public crate-budget-tracker-0.5.1 (c (n "budget-tracker") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j9ji3v6lykp8lgl7kasrqddnj757m32h6a0kzprpkjvrqzcd4m0")))

(define-public crate-budget-tracker-0.5.3 (c (n "budget-tracker") (v "0.5.3") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.3") (d #t) (k 0)))) (h "1ii8ip5b1y0d9k8dhyad2fz99kcaad2mzz6h8x51f6mfb8779rsz")))

