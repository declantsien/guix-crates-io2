(define-module (crates-io bu dg budgetinvoice) #:use-module (crates-io))

(define-public crate-budgetinvoice-0.1.0 (c (n "budgetinvoice") (v "0.1.0") (h "03xv45nlc2p7d9ayf5pb2kbja0giivxwwc0sxlq31m8dzk0g8vqi")))

(define-public crate-budgetinvoice-0.1.1 (c (n "budgetinvoice") (v "0.1.1") (h "0hy9zxi3jq1i6hpxb6x0gkpgb4damj6jyhhk4s9xq3j9dhk6jvbj")))

(define-public crate-budgetinvoice-0.1.2 (c (n "budgetinvoice") (v "0.1.2") (h "1ng62c3x87gpnv6fk3r2vf06iywi5bq6mplyk57v1kvh3nhfbsvw")))

