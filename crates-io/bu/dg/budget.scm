(define-module (crates-io bu dg budget) #:use-module (crates-io))

(define-public crate-budget-0.3.1 (c (n "budget") (v "0.3.1") (d (list (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "1ii78jzvj8d47jv5dp7sz27x01s0wmcvwhpsqkradcm4y311bfmx")))

(define-public crate-budget-0.5.1 (c (n "budget") (v "0.5.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)) (d (n "tagsearch") (r "^0.13.0") (d #t) (k 0)))) (h "0vysbj4hff4xn3pj5zipdksh1y00pkfwp1anljw9mf02i1yb1frs")))

(define-public crate-budget-0.6.0 (c (n "budget") (v "0.6.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)) (d (n "tagsearch") (r "^0.13.0") (d #t) (k 0)))) (h "1p5d5fflig44a237h26s8v4gfpx5v08b2x8zhc36gq7n6s3rrp56")))

(define-public crate-budget-0.6.1 (c (n "budget") (v "0.6.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)) (d (n "tagsearch") (r "^0.13.0") (d #t) (k 0)))) (h "15i2blaab3fqzy3yk1393c71jdmsdgf3jkzdgnznns19jkp9phhs")))

(define-public crate-budget-0.7.0 (c (n "budget") (v "0.7.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)) (d (n "tagsearch") (r "^0.13.0") (d #t) (k 0)))) (h "0y5gia0b0hnn1w05bwi8pd16g35bcsabv9iv9rbqq5q1zsyri815")))

