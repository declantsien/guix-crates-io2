(define-module (crates-io bu sp buspirate) #:use-module (crates-io))

(define-public crate-buspirate-0.1.0 (c (n "buspirate") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "18h3b2cb2ai3qqj0z1gcgmywl09pw69sdmg3rihsfkqsv51j5m4g")))

