(define-module (crates-io bu ny bunyarrs) #:use-module (crates-io))

(define-public crate-bunyarrs-0.1.0 (c (n "bunyarrs") (v "0.1.0") (d (list (d (n "gethostname") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("formatting"))) (d #t) (k 0)))) (h "1vjjz2mw4pslvnzz8lafb6vkfmsr9zqfh90kkpbwsk25jij4l11y")))

(define-public crate-bunyarrs-0.2.0 (c (n "bunyarrs") (v "0.2.0") (d (list (d (n "gethostname") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("formatting"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)))) (h "0nk91s9lxhlj5dgk2niq94wwrcy0518jg378pr5nyln8rji2y6bk")))

