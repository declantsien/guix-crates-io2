(define-module (crates-io bu lu bulut) #:use-module (crates-io))

(define-public crate-bulut-0.2.0 (c (n "bulut") (v "0.2.0") (d (list (d (n "colored") (r "^1.6.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "0nxlfy8ahvs215xfyram9df51vqbzkqqfz7nbwfr93p8dff7lkba")))

(define-public crate-bulut-0.3.0 (c (n "bulut") (v "0.3.0") (d (list (d (n "colored") (r "^1.6.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "0c60292x7hv21gkhyx54856945y9pnbz9f6x85igs1w0avb45dll")))

