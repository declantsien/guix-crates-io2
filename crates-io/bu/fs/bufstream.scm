(define-module (crates-io bu fs bufstream) #:use-module (crates-io))

(define-public crate-bufstream-0.1.0 (c (n "bufstream") (v "0.1.0") (h "0msz4rn55rz5xi3lyn9ad0gg4zad5c6rc7bj4xd289h84qgz6y1a")))

(define-public crate-bufstream-0.1.1 (c (n "bufstream") (v "0.1.1") (h "1ci3bgnrdyg56b3k4bl7wqsdsgpa8m9dqdlf12bidmdfm3k3sjby")))

(define-public crate-bufstream-0.1.2 (c (n "bufstream") (v "0.1.2") (h "0x6h27md1fwabbhbycfldj0wklrpjr520z9p0cpzm60fzzidnj3v")))

(define-public crate-bufstream-0.1.3 (c (n "bufstream") (v "0.1.3") (d (list (d (n "futures") (r "^0.1.13") (o #t) (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0clf8q4ivw3rr24g11x703xh5fmsjw20vh2cfindxfbn3rqq5wzj") (f (quote (("tokio" "futures" "tokio-io") ("default"))))))

(define-public crate-bufstream-0.1.4 (c (n "bufstream") (v "0.1.4") (d (list (d (n "futures") (r "^0.1.13") (o #t) (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1j7f52rv73hd1crzrrfb9dr50ccmi3hb1ybd6s5dyg6jmllqkqs0") (f (quote (("tokio" "futures" "tokio-io") ("default"))))))

