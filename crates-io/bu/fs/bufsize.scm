(define-module (crates-io bu fs bufsize) #:use-module (crates-io))

(define-public crate-bufsize-0.4.0 (c (n "bufsize") (v "0.4.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "1vvq2jffy6q2z97w6wfmiw5qd5prfj8a786da9vrz2l0zima2cpw") (f (quote (("i128" "bytes/i128"))))))

(define-public crate-bufsize-0.5.0 (c (n "bufsize") (v "0.5.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)))) (h "1y33qjrl0c2gab8pbdyhi5jrs8yip5s1gyhbsfykdqcamsn248zj")))

(define-public crate-bufsize-0.5.1 (c (n "bufsize") (v "0.5.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)))) (h "1hbzdcmyaicsb5x49y794a37qgpbh5wmqhhl4fmjlzxr2pcy8pxv")))

(define-public crate-bufsize-0.6.0 (c (n "bufsize") (v "0.6.0") (d (list (d (n "bytes") (r "^0.6") (d #t) (k 0)))) (h "015bfjpqvj7zihalmfwgzh85vcr4fmxvxk0ap39g6s1y8y1jalnm")))

(define-public crate-bufsize-1.0.0 (c (n "bufsize") (v "1.0.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)))) (h "1mlcxjl6kcfy3qzppxsrfajq9sdgrmmsgdfawsi88c9js4vrp627")))

(define-public crate-bufsize-1.0.1 (c (n "bufsize") (v "1.0.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)))) (h "00fjcymh2ghrh3c50pams4fpasnr8k98lsxz05jj2h5b0k31ypby") (r "1.39")))

(define-public crate-bufsize-1.0.2 (c (n "bufsize") (v "1.0.2") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)))) (h "1kqq18356kyiyid38czb9gfadhbjnvnprj7p4laj7mxcm07qj7y8") (r "1.39")))

(define-public crate-bufsize-1.0.3 (c (n "bufsize") (v "1.0.3") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)))) (h "0lcxpndydscppgld0xnsncpdyc5v6jim3mvf7c9b5n1y0m54n7n8") (r "1.39")))

(define-public crate-bufsize-1.0.4 (c (n "bufsize") (v "1.0.4") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)))) (h "1zz19x2igdbxzg301dvw8y3167gdxzn48567cnba9lw11bapdk0c") (r "1.39")))

(define-public crate-bufsize-1.0.5 (c (n "bufsize") (v "1.0.5") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)))) (h "0adm1vi4r1sx4h93yx3l5vgmqx5gb70qfi1ryf3v43w0r0dwdfsi") (r "1.39")))

(define-public crate-bufsize-1.0.6 (c (n "bufsize") (v "1.0.6") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)))) (h "1s25ac260s37yqq8dks711qrd9nvvm0fglygrdlisbfix0dlyans") (r "1.39")))

(define-public crate-bufsize-1.0.7 (c (n "bufsize") (v "1.0.7") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)))) (h "0pff7dpr0m6lcscr1ggirl0bhrn7kkl3sg4p9nddk70052xayr3q") (r "1.39")))

