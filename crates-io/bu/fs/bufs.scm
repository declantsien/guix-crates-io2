(define-module (crates-io bu fs bufs) #:use-module (crates-io))

(define-public crate-bufs-0.0.0 (c (n "bufs") (v "0.0.0") (h "1dkfcrxcf5587r6gx3lcnd7vr0nj24mjqacbd0lpi7kg6swsphlb")))

(define-public crate-bufs-0.1.0 (c (n "bufs") (v "0.1.0") (d (list (d (n "bytes") (r "^1.6.0") (d #t) (k 0)))) (h "1k68y24syyk4rdmy7gxnfa24xbqpwnpcppwsanrba46grcck3v9g")))

