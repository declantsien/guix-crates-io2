(define-module (crates-io bu fs bufstream-fresh) #:use-module (crates-io))

(define-public crate-bufstream-fresh-0.3.0 (c (n "bufstream-fresh") (v "0.3.0") (h "1nsxfyb5q971dmhbmn30h1ibs9mszvbblfixa5i2d9djrw8n09gi")))

(define-public crate-bufstream-fresh-0.3.1 (c (n "bufstream-fresh") (v "0.3.1") (h "0pq3l8jxdwg8rb15p4q7ak31rbj65581ydvc17svdkhf8mfiwhvw")))

