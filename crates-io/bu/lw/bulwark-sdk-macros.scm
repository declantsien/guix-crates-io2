(define-module (crates-io bu lw bulwark-sdk-macros) #:use-module (crates-io))

(define-public crate-bulwark-sdk-macros-0.5.0 (c (n "bulwark-sdk-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "05nzi5irdwr5qm2q28vynmx7356sgfbbm9z187kschwp5as5xiqs")))

