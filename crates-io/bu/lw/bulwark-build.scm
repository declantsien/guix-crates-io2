(define-module (crates-io bu lw bulwark-build) #:use-module (crates-io))

(define-public crate-bulwark-build-0.5.0 (c (n "bulwark-build") (v "0.5.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "thiserror") (r "=1.0.43") (d #t) (k 0)) (d (n "wit-component") (r "^0.202.0") (d #t) (k 0)))) (h "06fs7bzkzfh48bcf5rv2nx417i67zrr7sgf1axwwv1jy4p1ky2yv")))

