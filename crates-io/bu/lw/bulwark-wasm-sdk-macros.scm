(define-module (crates-io bu lw bulwark-wasm-sdk-macros) #:use-module (crates-io))

(define-public crate-bulwark-wasm-sdk-macros-0.1.0 (c (n "bulwark-wasm-sdk-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1sivd3wznqzr3yc3lw6rl536hl5vgvhp36j2z4v0jlzfrkkpbrh4")))

(define-public crate-bulwark-wasm-sdk-macros-0.2.0 (c (n "bulwark-wasm-sdk-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "11y73a2r46c5cn6xmrivbq9mxm50xlmy781h6rwwkgymy0f0aci0")))

(define-public crate-bulwark-wasm-sdk-macros-0.3.0 (c (n "bulwark-wasm-sdk-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0d8a9xn0bja3hbcca7iinmsjgf8gdiskhwjkgy9n74av66ss8cqg")))

(define-public crate-bulwark-wasm-sdk-macros-0.4.0 (c (n "bulwark-wasm-sdk-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0lc34cwvaq7iispmqpi4gpjdlq8mja3h0gg8fwkwnzf62a7m1arx")))

(define-public crate-bulwark-wasm-sdk-macros-0.4.1 (c (n "bulwark-wasm-sdk-macros") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0zaz6l5snqjlhlhifc825sgaas54hkd1c2yay7b6nl8mkwydlw65")))

