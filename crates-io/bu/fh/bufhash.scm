(define-module (crates-io bu fh bufhash) #:use-module (crates-io))

(define-public crate-bufhash-0.1.0 (c (n "bufhash") (v "0.1.0") (h "1q7ff3mwn2f9mi0324brwmrbl0w40ridn9qda1367kyvr6ks4fms")))

(define-public crate-bufhash-0.1.1 (c (n "bufhash") (v "0.1.1") (h "1sz3rx9rc1cyiy0bd01r9mjcsl716f2y2ga7yp11yxwn10y60hv4")))

