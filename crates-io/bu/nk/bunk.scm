(define-module (crates-io bu nk bunk) #:use-module (crates-io))

(define-public crate-bunk-0.1.0 (c (n "bunk") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "include-bytes-plus") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16y9pfmvxrbpnpmajvadm2nr7a45aamkn26qnwbs830nwrwh9sja") (s 2) (e (quote (("serde" "dep:serde"))))))

