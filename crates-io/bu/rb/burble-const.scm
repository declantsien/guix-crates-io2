(define-module (crates-io bu rb burble-const) #:use-module (crates-io))

(define-public crate-burble-const-0.1.0 (c (n "burble-const") (v "0.1.0") (d (list (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.11") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "structbuf") (r "^0.3.4") (d #t) (k 0)))) (h "1ydz08vbka3k38dvksvcnnp1fs2r3akfq9ycmfihz9k2vrvx7y8f") (r "1.66")))

(define-public crate-burble-const-0.1.1 (c (n "burble-const") (v "0.1.1") (d (list (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.11") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "structbuf") (r "^0.3.4") (d #t) (k 0)))) (h "1vinkn3qwjg87g48bi3bfm2bxz5w49k71l1rhimm9hv7w3aj19mm") (r "1.65")))

(define-public crate-burble-const-0.1.2 (c (n "burble-const") (v "0.1.2") (d (list (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.11") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "structbuf") (r "^0.3.4") (d #t) (k 0)))) (h "0ra6c2g4ffh47rd9jqfrsbpm7p96x4xvpyscf2v00jrv6ym3y851") (r "1.65")))

(define-public crate-burble-const-0.2.0 (c (n "burble-const") (v "0.2.0") (d (list (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.11") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "structbuf") (r "^0.3.4") (d #t) (k 0)))) (h "1n63qqpss3j2av26n7xlwyiipm5daxccdkj3xmpvrr9gglx5yvaz") (r "1.65")))

(define-public crate-burble-const-0.2.1 (c (n "burble-const") (v "0.2.1") (d (list (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.11") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "structbuf") (r "^0.3.4") (d #t) (k 0)))) (h "0xfsl586mwy3zvz06m4akibfvpxdzq0xn2k08f8amhdrm6sp1hr7") (r "1.65")))

(define-public crate-burble-const-0.2.2 (c (n "burble-const") (v "0.2.2") (d (list (d (n "enum-iterator") (r "^1.4.0") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.11") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "structbuf") (r "^0.3.4") (d #t) (k 0)))) (h "0jkvlcyxp01h3sfmjc7h1yb40ycf520jkbr1383i7rpwkm38r2s5") (r "1.65")))

