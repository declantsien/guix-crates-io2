(define-module (crates-io bu rb burble-fs) #:use-module (crates-io))

(define-public crate-burble-fs-0.1.0 (c (n "burble-fs") (v "0.1.0") (d (list (d (n "burble") (r "^0.1.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0vqnp0ffa0mhwmlxmvmy4az8q2fb50jldrwxgwabydckqwlhj9vf") (r "1.66")))

(define-public crate-burble-fs-0.1.1 (c (n "burble-fs") (v "0.1.1") (d (list (d (n "burble") (r "^0.1.1") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1rx8li41w8b8yyvmwpzx9sdva013gbpxiiykw0a2xjmdfbzw8p2m") (r "1.65")))

(define-public crate-burble-fs-0.1.2 (c (n "burble-fs") (v "0.1.2") (d (list (d (n "burble") (r "^0.1.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0w5ryiay8ahl3bjl653y07wl5pkxyml7chxhf041w8fii4jnqbvh") (r "1.65")))

(define-public crate-burble-fs-0.2.0 (c (n "burble-fs") (v "0.2.0") (d (list (d (n "burble") (r "^0.2.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1i6m5r73pgpn5s3d56a0qmsp09h3zxs2ivsigvy1f6jvvf22fbzd") (r "1.65")))

(define-public crate-burble-fs-0.2.1 (c (n "burble-fs") (v "0.2.1") (d (list (d (n "burble") (r "^0.2.1") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1f412sl8n7f3v7hvy893wxaylb0kc1n54qhaflxlbpy04nbpcy3d") (r "1.65")))

(define-public crate-burble-fs-0.2.2 (c (n "burble-fs") (v "0.2.2") (d (list (d (n "burble") (r "^0.2.2") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "10k9h3n1w9pfly8awmjisk69adkp28wjzrfiqcn7d9hcn4hxgjb1") (r "1.65")))

