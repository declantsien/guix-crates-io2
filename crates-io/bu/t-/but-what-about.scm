(define-module (crates-io bu t- but-what-about) #:use-module (crates-io))

(define-public crate-but-what-about-0.1.0 (c (n "but-what-about") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.7.1") (o #t) (d #t) (k 0)))) (h "1r2ybfwn7x9qam1w7ipzfgj958dmbay8qc6l3slw68h8y75k1s25") (f (quote (("grapheme" "unicode-segmentation"))))))

