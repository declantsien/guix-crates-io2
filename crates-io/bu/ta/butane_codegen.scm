(define-module (crates-io bu ta butane_codegen) #:use-module (crates-io))

(define-public crate-butane_codegen-0.1.0 (c (n "butane_codegen") (v "0.1.0") (d (list (d (n "butane_core") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1dh35g9lnax4cpn1sdjr0mf2x04nkw6ysg4nd6kmszwgnhd8mv45") (f (quote (("datetime"))))))

(define-public crate-butane_codegen-0.2.0 (c (n "butane_codegen") (v "0.2.0") (d (list (d (n "butane_core") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0vakpkfncm5xpjy4yz2dg9799g5gqi38njv925xdp2jm9i7dfbxc") (f (quote (("datetime"))))))

(define-public crate-butane_codegen-0.2.1 (c (n "butane_codegen") (v "0.2.1") (d (list (d (n "butane_core") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "02jcnxkb1c5b239j847i8wc452msqagfd9p2jmxpay8c5hiqmqzq") (f (quote (("datetime"))))))

(define-public crate-butane_codegen-0.3.0 (c (n "butane_codegen") (v "0.3.0") (d (list (d (n "butane_core") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1ccxviw0yrx5xv03a7j88d74mlb79sfyl7kck29pias5b4qadhl3") (f (quote (("datetime"))))))

(define-public crate-butane_codegen-0.3.1 (c (n "butane_codegen") (v "0.3.1") (d (list (d (n "butane_core") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "02n6m71nxagpi5bakj9fsnhvlp60hc4pfkp2p4lsylnz8qjaaw49") (f (quote (("datetime"))))))

(define-public crate-butane_codegen-0.4.0 (c (n "butane_codegen") (v "0.4.0") (d (list (d (n "butane_core") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (o #t) (d #t) (k 0)))) (h "114rsv4r9xwa6v28y0y9l39l7r0xb4viibd2fgy75fhyigj3fwv2") (f (quote (("datetime"))))))

(define-public crate-butane_codegen-0.5.0 (c (n "butane_codegen") (v "0.5.0") (d (list (d (n "butane_core") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "uuid") (r "^1.2") (o #t) (d #t) (k 0)))) (h "011lv165wl8y67pda900cnq6crxxzmgzd66s9aj835cfy9xs226d") (f (quote (("datetime"))))))

(define-public crate-butane_codegen-0.6.0 (c (n "butane_codegen") (v "0.6.0") (d (list (d (n "butane_core") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.2") (o #t) (d #t) (k 0)))) (h "11w7rradmm6s1cmy8b3dily6h8s3b67w6pad3yipvskrc0yj18l3") (f (quote (("json" "butane_core/json") ("datetime"))))))

(define-public crate-butane_codegen-0.6.1 (c (n "butane_codegen") (v "0.6.1") (d (list (d (n "butane_core") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1x9sxjipr534navh4k3yg0xlv5zmabkxfq2j0bzd98rckgphxdhv") (f (quote (("uuid" "butane_core/uuid") ("json" "butane_core/json") ("datetime" "butane_core/datetime"))))))

