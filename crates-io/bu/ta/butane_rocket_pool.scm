(define-module (crates-io bu ta butane_rocket_pool) #:use-module (crates-io))

(define-public crate-butane_rocket_pool-0.5.0 (c (n "butane_rocket_pool") (v "0.5.0") (d (list (d (n "butane") (r "^0.4.1") (f (quote ("r2d2"))) (d #t) (k 0)) (d (n "r2d2") (r "^0.8.9") (d #t) (k 0)) (d (n "rocket_sync_db_pools") (r "^0.1.0-rc.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1nx92dsx6h7lm7wk12g4dxqbk77qmzv8l6d0ljyqriwrm155gd57")))

(define-public crate-butane_rocket_pool-0.5.1 (c (n "butane_rocket_pool") (v "0.5.1") (d (list (d (n "butane") (r "^0.5.1") (f (quote ("r2d2"))) (d #t) (k 0)) (d (n "r2d2") (r "^0.8.10") (d #t) (k 0)) (d (n "rocket_sync_db_pools") (r "^0.1.0-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mdwmw4sg85ji3bma2adb228vbi2cx6blgb27fp1ypcxz19rpdhy")))

(define-public crate-butane_rocket_pool-0.5.2 (c (n "butane_rocket_pool") (v "0.5.2") (d (list (d (n "butane") (r "^0.5.1") (f (quote ("r2d2"))) (d #t) (k 0)) (d (n "r2d2") (r "^0.8.10") (d #t) (k 0)) (d (n "rocket_sync_db_pools") (r "^0.1.0-rc.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jb99723qrdy0g8fwyanz7fx7wkwl5ajsarn1ws6zbz2bxn1kv96")))

(define-public crate-butane_rocket_pool-0.5.3 (c (n "butane_rocket_pool") (v "0.5.3") (d (list (d (n "butane") (r "^0.6.1") (f (quote ("r2d2"))) (d #t) (k 0)) (d (n "r2d2") (r "^0.8.10") (d #t) (k 0)) (d (n "rocket_sync_db_pools") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q8nciwk2f3qg2xzq3w0v5vshblzcfzjrrc3risb6cdlpmvcgyq4")))

