(define-module (crates-io bu sm busmust) #:use-module (crates-io))

(define-public crate-busmust-0.1.0 (c (n "busmust") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "busmust-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)))) (h "109j7a5wzr8x8bycdwdpkix6pks138ygpnn25y44mb6f4kycy8jh")))

(define-public crate-busmust-0.1.1 (c (n "busmust") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "busmust-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)))) (h "1mffyiiwn43g0cq34vvy2cypyqymdrnagjyf6b94hma8crakjacg")))

(define-public crate-busmust-0.1.2 (c (n "busmust") (v "0.1.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "busmust-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (d #t) (k 2)))) (h "1jzkphmxy1d95mlb8bfkxlcpi0pm0ddnngx9grjbfnxymxsa44g1")))

(define-public crate-busmust-0.1.3 (c (n "busmust") (v "0.1.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "busmust-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (d #t) (k 2)))) (h "0ayi2phjl2h4286ibadycrll8waq2b8qrb8gjdf7vd5n75gjllz0")))

