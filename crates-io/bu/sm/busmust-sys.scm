(define-module (crates-io bu sm busmust-sys) #:use-module (crates-io))

(define-public crate-busmust-sys-0.1.0 (c (n "busmust-sys") (v "0.1.0") (d (list (d (n "bitfield-struct") (r "^0.3.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "1mzwkqmywlf69xsz15gwd02pb7pmcxb4whsnk5brl1py0whaqcg5")))

(define-public crate-busmust-sys-0.1.1 (c (n "busmust-sys") (v "0.1.1") (d (list (d (n "bitfield-struct") (r "^0.3.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "1f4l8gpnq5ns6k0sw120kznaysyccmir4hf3afjg4m7ilnvxbp8v")))

(define-public crate-busmust-sys-0.1.2 (c (n "busmust-sys") (v "0.1.2") (d (list (d (n "bitfield-struct") (r "^0.3.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "1lbg6vq0ni7vahnpq1y6c0f150s9kbn1hml9hi5dyrdfl3avss4r")))

(define-public crate-busmust-sys-0.1.3 (c (n "busmust-sys") (v "0.1.3") (d (list (d (n "bitfield-struct") (r "^0.3.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "08mnd0vm53cxryfj3q9yhpmj52mc43na4kysbhfj29kwab263kh0")))

