(define-module (crates-io bu ch buchberger) #:use-module (crates-io))

(define-public crate-buchberger-0.1.0 (c (n "buchberger") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.2") (d #t) (k 0)))) (h "14vp8v2xr7h4pmnl6jzdkz7f80drgaspm3y2h43ljgs5bsql8gg8")))

