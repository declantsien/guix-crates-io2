(define-module (crates-io bu ck bucket_queue) #:use-module (crates-io))

(define-public crate-bucket_queue-1.0.0 (c (n "bucket_queue") (v "1.0.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1k49g5g9yjk81b25s27jnrfrw3pjnwxvxhyw2bjrw77cpigl1dih")))

(define-public crate-bucket_queue-2.0.0 (c (n "bucket_queue") (v "2.0.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0qsl5gk16vhacnc6gjiryqsphiv6ksjf404rvmf6hl0psj7gb7l9")))

