(define-module (crates-io bu ck bucket-limiter) #:use-module (crates-io))

(define-public crate-bucket-limiter-0.1.0 (c (n "bucket-limiter") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1rciy4h909b0dc5jv4ry29q0pppkh06pcwyv8h29pwqkywcmbmrk")))

(define-public crate-bucket-limiter-0.1.1 (c (n "bucket-limiter") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "1qm8wb2y0qaj80sw8l3pwwbn6vnnajnl3d89rvac0zfv86mz0105")))

(define-public crate-bucket-limiter-0.2.0 (c (n "bucket-limiter") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "redis") (r "^0.8.0") (d #t) (k 0)))) (h "06g3ylkd85vsk5nnl2mkr5vzbgpqm0s24w41yzj8yi9amba1qlz2")))

