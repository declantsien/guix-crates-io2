(define-module (crates-io bu ck buck2) #:use-module (crates-io))

(define-public crate-buck2-0.1.0 (c (n "buck2") (v "0.1.0") (h "15w1c7l81l23g44i6sixhlkp6lj8383w62cpiid9xq9zqiak8ymm")))

(define-public crate-buck2-0.1.1 (c (n "buck2") (v "0.1.1") (h "1185ha6m346vyrcrr67dp57bvk3vrpnkzdb8m9scsmfcwcmkfhry")))

