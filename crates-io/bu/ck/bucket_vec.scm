(define-module (crates-io bu ck bucket_vec) #:use-module (crates-io))

(define-public crate-bucket_vec-0.1.0 (c (n "bucket_vec") (v "0.1.0") (d (list (d (n "libm") (r "^0.2") (k 0)))) (h "0dypjjxazrsy8gcsln628yv39z8x30fhkz1p06hy534wjiqp3lbr") (f (quote (("std") ("default" "std"))))))

(define-public crate-bucket_vec-0.1.1 (c (n "bucket_vec") (v "0.1.1") (d (list (d (n "libm") (r "^0.2") (k 0)))) (h "0hf4wwvx88aa9xfiyyyfz1msmph95k3b5yr0k25qi1jqpr1bkxff") (f (quote (("std") ("default" "std"))))))

(define-public crate-bucket_vec-0.2.0 (c (n "bucket_vec") (v "0.2.0") (d (list (d (n "libm") (r "^0.2") (k 0)) (d (n "paste") (r "^0.1.6") (d #t) (k 2)))) (h "0q0b87cvcf0r0p60b1f6wb604f5wg015cm76j0yz3q8nj70zz72x") (f (quote (("std") ("default" "std"))))))

(define-public crate-bucket_vec-0.3.0 (c (n "bucket_vec") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "libm") (r "^0.2") (k 0)) (d (n "paste") (r "^0.1.6") (d #t) (k 2)))) (h "1zksa2ywilz2j2c1708zgsy38l91cbmrx9s3cp3nf1zl8r7v177r") (f (quote (("std") ("nightly" "criterion/real_blackbox") ("default" "std"))))))

(define-public crate-bucket_vec-0.3.1 (c (n "bucket_vec") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "libm") (r "^0.2") (k 0)) (d (n "paste") (r "^0.1.6") (d #t) (k 2)))) (h "0bzab6zf9akcgjsza0mb81n46abybq9jsk1imzi4ad5ysv9dslg9") (f (quote (("std") ("nightly" "criterion/real_blackbox") ("default" "std"))))))

(define-public crate-bucket_vec-0.4.0 (c (n "bucket_vec") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "libm") (r "^0.2") (k 0)) (d (n "paste") (r "^0.1.6") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0xjmi1arbr8hicwm2c25jzzcv8cl72iqzkhppd7anh2lvl24mpbv") (f (quote (("std") ("nightly" "criterion/real_blackbox") ("default" "std"))))))

(define-public crate-bucket_vec-0.5.0 (c (n "bucket_vec") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "libm") (r "^0.2") (k 0)) (d (n "paste") (r "^0.1.6") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1gl5d5h28vjk1q40kmps5h3w7ffh4y3ypf46bpa4znj2mw23fisy") (f (quote (("std") ("nightly" "criterion/real_blackbox") ("default" "std"))))))

(define-public crate-bucket_vec-0.6.0 (c (n "bucket_vec") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "libm") (r "^0.2") (k 0)) (d (n "paste") (r "^0.1.6") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0damm53v3nvbkzz7g6n3dgmdv771ffvn253lyv9z5chaplghyikj") (f (quote (("std") ("nightly" "criterion/real_blackbox") ("default" "std"))))))

(define-public crate-bucket_vec-0.7.0 (c (n "bucket_vec") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "libm") (r "^0.2") (k 0)) (d (n "paste") (r "^0.1.6") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "scale") (r "^1.1") (o #t) (k 0) (p "parity-scale-codec")))) (h "1pmmhl84b44gq8afiwdflcic8xrq676waxqxddm0w3c4xdgmdgrb") (f (quote (("std" "scale/std") ("scale-1" "scale") ("nightly" "criterion/real_blackbox") ("default" "std" "scale-1"))))))

(define-public crate-bucket_vec-0.7.1 (c (n "bucket_vec") (v "0.7.1") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "libm") (r "^0.2") (k 0)) (d (n "paste") (r "^0.1.6") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "scale") (r "^1.1") (o #t) (k 0) (p "parity-scale-codec")))) (h "0lppd5lknrly2x768bggzcb8xmpf6lh4qwyvkdb60m25l9a1iv2b") (f (quote (("std" "scale/std") ("scale-1" "scale") ("nightly" "criterion/real_blackbox") ("default" "std" "scale-1"))))))

(define-public crate-bucket_vec-0.8.0 (c (n "bucket_vec") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "libm") (r "^0.2") (k 0)) (d (n "paste") (r "^0.1.6") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "scale") (r "^1.1") (o #t) (k 0) (p "parity-scale-codec")))) (h "07d753gsza85s2kcag73xwlagz0cmimpvf5im2ni2aybm6rn8ji1") (f (quote (("std" "scale/std") ("scale-1" "scale") ("nightly" "criterion/real_blackbox") ("default" "std" "scale-1"))))))

