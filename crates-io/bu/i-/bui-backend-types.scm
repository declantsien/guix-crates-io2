(define-module (crates-io bu i- bui-backend-types) #:use-module (crates-io))

(define-public crate-bui-backend-types-0.7.0 (c (n "bui-backend-types") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1pcckgim7khcgnz2ifqn4xbkdl136pkkh0p8cdvk2p2pvbxy1ww5")))

(define-public crate-bui-backend-types-0.8.0 (c (n "bui-backend-types") (v "0.8.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde"))) (d #t) (k 0)))) (h "03g9xi10cxikyy8dj09cwidj1b8ai8ihfr8mkbhipm2px4i85zmg") (f (quote (("uuid-v4" "uuid/v4") ("default"))))))

