(define-module (crates-io bu i- bui-backend-codegen) #:use-module (crates-io))

(define-public crate-bui-backend-codegen-0.1.0 (c (n "bui-backend-codegen") (v "0.1.0") (d (list (d (n "includedir_codegen") (r "^0.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "030x3z5zirc4vhsc9pcm9jbkcdv21mar1wcpsl0cqr76gqk3wbq7") (f (quote (("serve_files") ("default" "bundle_files") ("bundle_files"))))))

(define-public crate-bui-backend-codegen-0.1.1 (c (n "bui-backend-codegen") (v "0.1.1") (d (list (d (n "includedir_codegen") (r "^0.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "0lhz8jk2c8hf8pnrzahiff7a0pj6xizy7rkmksks2h4crf32xia4") (f (quote (("serve_files") ("default" "bundle_files") ("bundle_files"))))))

(define-public crate-bui-backend-codegen-0.1.2 (c (n "bui-backend-codegen") (v "0.1.2") (d (list (d (n "includedir_codegen") (r "^0.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "158lyj4j3mckgx6dnbqpkaf79ad2hhb5hlyv35j8v23adnmpxk11") (f (quote (("serve_files") ("default" "bundle_files") ("bundle_files"))))))

(define-public crate-bui-backend-codegen-0.1.3 (c (n "bui-backend-codegen") (v "0.1.3") (d (list (d (n "includedir_codegen") (r "^0.2.0") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "1fqj034m3h09riclmv2v6yaf0g9wgf5xhsjy03ng68fygdwhyj46") (f (quote (("serve_files") ("default" "bundle_files") ("bundle_files"))))))

(define-public crate-bui-backend-codegen-0.8.0 (c (n "bui-backend-codegen") (v "0.8.0") (d (list (d (n "includedir_codegen") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.2.5") (o #t) (d #t) (k 0)))) (h "1k40g2jdhqaq5bsp2wbc8q8al6q297zb169x5cqa4fn7fcj4maf1") (f (quote (("serve_files") ("default" "bundle_files") ("bundle_files" "walkdir" "includedir_codegen"))))))

(define-public crate-bui-backend-codegen-0.9.0 (c (n "bui-backend-codegen") (v "0.9.0") (d (list (d (n "includedir_codegen") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.2.5") (o #t) (d #t) (k 0)))) (h "13i48f8hn4d2njkx8mqnfljdgwkvmpa73sv70a7xl33splhj0vx1") (f (quote (("serve_files") ("default" "bundle_files") ("bundle_files" "walkdir" "includedir_codegen"))))))

(define-public crate-bui-backend-codegen-0.9.1 (c (n "bui-backend-codegen") (v "0.9.1") (d (list (d (n "includedir_codegen") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.2.5") (o #t) (d #t) (k 0)))) (h "012vd7z2wml9hgi7dj7r1yzvvzhhapms6qkqwqyakf5gnaqzrx1m") (f (quote (("serve_files") ("default" "bundle_files") ("bundle_files" "walkdir" "includedir_codegen"))))))

