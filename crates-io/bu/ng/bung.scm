(define-module (crates-io bu ng bung) #:use-module (crates-io))

(define-public crate-bung-0.1.0 (c (n "bung") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rmp") (r "^0.8.11") (d #t) (k 0)) (d (n "rmpv") (r "^1.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.159") (d #t) (k 2)))) (h "0lr97jp43axg7rgcgai30wlh906m164nk9i09wxnq4hz6clwz2ys") (r "1.56.0")))

