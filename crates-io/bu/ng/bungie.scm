(define-module (crates-io bu ng bungie) #:use-module (crates-io))

(define-public crate-bungie-0.1.0 (c (n "bungie") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.22") (d #t) (k 0)))) (h "1c6wfm04r97fm7zbwzjz90zcddgy7wilxa6ng1z67b222i5j3c3s")))

(define-public crate-bungie-0.1.1 (c (n "bungie") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.13.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11.22") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.68") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.68") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.22") (d #t) (k 0)))) (h "1q2314pr7fnqp7y8kcnlbfhcmxgdnhqg0wlxgzhm5claraiphgg6")))

(define-public crate-bungie-0.1.2 (c (n "bungie") (v "0.1.2") (d (list (d (n "dotenv") (r "^0.13.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11.22") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.68") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.68") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.22") (d #t) (k 0)))) (h "14dvrkkzni0dyfdkaw816zmc3liis86g2msni1w66sbvdyngjdbd")))

(define-public crate-bungie-0.2.0 (c (n "bungie") (v "0.2.0") (d (list (d (n "dotenv") (r "^0.13.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11.22") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.68") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.68") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.22") (d #t) (k 0)))) (h "16v89vi5sl1jhjgh90gqm41a3kwjibsryc8xib4jmh7l3jwlv4zc")))

