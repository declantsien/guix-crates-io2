(define-module (crates-io bu sa busan) #:use-module (crates-io))

(define-public crate-busan-0.1.0 (c (n "busan") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "0mmxmqdd4pfb1bwmq5xikq8k19jnpcd6f80zx236443l6wig5bcm")))

(define-public crate-busan-0.1.1 (c (n "busan") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "09gr8phld7mpfirgc3bhblq599qg9kzsbanizp231f0ag79aiw65")))

(define-public crate-busan-0.1.2 (c (n "busan") (v "0.1.2") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "1186yg6qpif27il14rqv5gqh4ggq2yzw9v0rz555a8xz0jv434dy")))

(define-public crate-busan-0.1.3 (c (n "busan") (v "0.1.3") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "0xc6pl1wk1cpxhx21yk1yx5ny2wr62v5z8pn5amxr7wgcakw13lz")))

(define-public crate-busan-0.2.0 (c (n "busan") (v "0.2.0") (d (list (d (n "busan-derive") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "0yncnxl8sakcz6nymkp3l3ws5lij45kwf9xxiahi0sbm1a0vzhrw")))

(define-public crate-busan-0.2.1 (c (n "busan") (v "0.2.1") (d (list (d (n "busan-derive") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "00kagp31gb35xlkr1scndqha83555vky6q1559mk994rcd118plm")))

