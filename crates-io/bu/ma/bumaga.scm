(define-module (crates-io bu ma bumaga) #:use-module (crates-io))

(define-public crate-bumaga-0.1.0 (c (n "bumaga") (v "0.1.0") (d (list (d (n "css-color-parser") (r "^0.1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "skia-safe") (r "^0.49.0") (f (quote ("shaper"))) (d #t) (k 0)) (d (n "stretch") (r "^0.3.2") (d #t) (k 0)))) (h "0qnq1hxjvayxlsp5410aghvjnbix02cnjkhp54rinwa1qwpa75x9")))

