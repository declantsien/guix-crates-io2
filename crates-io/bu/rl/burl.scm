(define-module (crates-io bu rl burl) #:use-module (crates-io))

(define-public crate-burl-0.1.1 (c (n "burl") (v "0.1.1") (d (list (d (n "aok") (r "^0.1.3") (d #t) (k 0)) (d (n "aok") (r "^0.1.3") (d #t) (k 2)) (d (n "base-x") (r "^0.2.11") (d #t) (k 0)) (d (n "intbin") (r "^0.1.2") (d #t) (k 2)) (d (n "loginit") (r "^0.1.10") (d #t) (k 2)) (d (n "static_init") (r "^1.0.3") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 2)))) (h "0czd1i35iq6vjf8a3sv1k6h4wlx64jx0rdx4qriprwpv8bmszsrh")))

