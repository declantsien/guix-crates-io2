(define-module (crates-io bu gh bughouse) #:use-module (crates-io))

(define-public crate-bughouse-0.0.1 (c (n "bughouse") (v "0.0.1") (d (list (d (n "chess") (r "^3.2.0") (d #t) (k 0)))) (h "1vs0l5mlndpy4hnj1qf6d36w03wghy79bvlmg2f9lnn9awr4nln3")))

(define-public crate-bughouse-0.0.2 (c (n "bughouse") (v "0.0.2") (d (list (d (n "chess") (r "^3.2.0") (d #t) (k 0)))) (h "0riycwwpaj14q127bk0fz43hlyskfm1wggy59177y8pc4ak2bflq")))

(define-public crate-bughouse-0.0.3 (c (n "bughouse") (v "0.0.3") (d (list (d (n "chess") (r "^3.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1xfbaiid066sys0a4q4lnjqf4slssk9qy8b8jrfk9kc66kqgcnzw")))

(define-public crate-bughouse-0.0.4 (c (n "bughouse") (v "0.0.4") (d (list (d (n "chess") (r "^3.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1igm61hic140fa0a4f6770dd554l5wc5bas4q72m8v7wgm62bwnx")))

(define-public crate-bughouse-0.0.5 (c (n "bughouse") (v "0.0.5") (d (list (d (n "chess") (r "^3.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1msdxk3344l0fnzp4wwn07lahm9dvka9919zvr69x2sql9w8h6n3")))

(define-public crate-bughouse-0.0.6 (c (n "bughouse") (v "0.0.6") (d (list (d (n "chess") (r "^3.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0c6f1j246ibj74r7c565dylpal20sdri4iqp80a403gw6r05sjcf")))

(define-public crate-bughouse-0.0.7 (c (n "bughouse") (v "0.0.7") (d (list (d (n "chess") (r "^3.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bjami7lvyv50ixdnqvkm3zxclikkqns8w0wcmz0iy5mvsc6hlwx")))

(define-public crate-bughouse-0.0.8 (c (n "bughouse") (v "0.0.8") (d (list (d (n "chess") (r "^3.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1l99n2idaagqrsbnkjq1h3vndzz6m4jzy9n08zgfmqd9hyyp419q")))

(define-public crate-bughouse-0.0.9 (c (n "bughouse") (v "0.0.9") (d (list (d (n "chess") (r "^3.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0im1isjk4a52sid7gicll2ysy0gskb17049iy8sir210r8wy5ycz")))

(define-public crate-bughouse-0.0.10 (c (n "bughouse") (v "0.0.10") (d (list (d (n "chess") (r "^3.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "065x3331zhg0hqyq46mbylvh5k0vlraljw89iv011xcdvnwacfal")))

(define-public crate-bughouse-0.0.11 (c (n "bughouse") (v "0.0.11") (d (list (d (n "chess") (r "^3.2.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1iaw878rdgg2s5zyrd9nvxbry4zs5ymzkr3yc4cndf9al2v8vcis")))

