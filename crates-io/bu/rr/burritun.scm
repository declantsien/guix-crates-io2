(define-module (crates-io bu rr burritun) #:use-module (crates-io))

(define-public crate-burritun-0.1.0 (c (n "burritun") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^3") (d #t) (k 0)) (d (n "pktparse") (r "^0.2.2") (d #t) (k 0)) (d (n "pnet") (r "^0.21") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tun-tap") (r "^0.1.1") (d #t) (k 0)))) (h "14fkqqg9f5yi5s7gf3nmbr6yq72qd1717r99pfg8bw0hrhn9inqb")))

(define-public crate-burritun-0.1.1 (c (n "burritun") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pktparse") (r "^0.3") (d #t) (k 0)) (d (n "pnet") (r "^0.21") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tun-tap") (r "^0.1.1") (d #t) (k 0)))) (h "0sm0l68740285i5m60z8i73qwc06gdyyyb69685hilly1rs0fn8m")))

