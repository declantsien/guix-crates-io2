(define-module (crates-io bu rr burrtype_internal) #:use-module (crates-io))

(define-public crate-burrtype_internal-0.2.0 (c (n "burrtype_internal") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13g5rzq5vkcmqq9iqspliisl2h7vbaywrk2d3bz8s2shpcrfl95g") (f (quote (("docs") ("default"))))))

(define-public crate-burrtype_internal-0.3.0 (c (n "burrtype_internal") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1r2ba6fwykzck0ckl0hyp8ws8wwk6kgl1p6kgq5iyyjvrfyrhadn") (f (quote (("docs") ("default"))))))

(define-public crate-burrtype_internal-0.4.0 (c (n "burrtype_internal") (v "0.4.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09a13rhqcrng5dmncq0s421jgr8djnsc10js8h913lai827ll9y1") (f (quote (("docs") ("default"))))))

(define-public crate-burrtype_internal-0.5.0 (c (n "burrtype_internal") (v "0.5.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1nvfcdxx50k520s5r3gdhh2px4ba16a9v1prwlvnyh091dp9l4vv") (f (quote (("docs") ("default"))))))

