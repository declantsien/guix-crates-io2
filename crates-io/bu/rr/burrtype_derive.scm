(define-module (crates-io bu rr burrtype_derive) #:use-module (crates-io))

(define-public crate-burrtype_derive-0.1.0 (c (n "burrtype_derive") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "00ysz4mxy8i0vn0csp68z57cq7piwl0kvbxsw7rggvwqgxp19q2b")))

(define-public crate-burrtype_derive-0.2.0 (c (n "burrtype_derive") (v "0.2.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "burrtype_internal") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "printing"))) (d #t) (k 0)))) (h "009vlpj5da483dpx18qlql6hcr82cvg8r7cqvjkm78s5q847824s") (f (quote (("docs" "burrtype_internal/docs") ("default") ("auto_register"))))))

(define-public crate-burrtype_derive-0.3.0 (c (n "burrtype_derive") (v "0.3.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "burrtype_internal") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "printing"))) (d #t) (k 0)))) (h "1sjc3d73rwrhmgxqmxhvcgsxvdiaaxs3m8d7lw57fmvbgg7k5pj3") (f (quote (("docs" "burrtype_internal/docs") ("default") ("auto_register"))))))

(define-public crate-burrtype_derive-0.4.0 (c (n "burrtype_derive") (v "0.4.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "burrtype_internal") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "printing"))) (d #t) (k 0)))) (h "1jrfvs4jlggfckv6dhblqa3v3y2jm253svcbmgwkrw11rgx5g7v0") (f (quote (("serde_compat") ("docs" "burrtype_internal/docs") ("default") ("auto_register"))))))

(define-public crate-burrtype_derive-0.5.0 (c (n "burrtype_derive") (v "0.5.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "burrtype_internal") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "printing"))) (d #t) (k 0)))) (h "1wv1b9q06sc31ykbxssmf46yi56z2gb10ypg6x848sl27dpg5snw") (f (quote (("serde_compat") ("docs" "burrtype_internal/docs") ("default") ("auto_register"))))))

