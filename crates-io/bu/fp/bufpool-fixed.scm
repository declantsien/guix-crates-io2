(define-module (crates-io bu fp bufpool-fixed) #:use-module (crates-io))

(define-public crate-bufpool-fixed-0.1.0 (c (n "bufpool-fixed") (v "0.1.0") (d (list (d (n "off64") (r "^0.6.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0g397rkhpzpnr9nvavjx8iss51v08fnrwi8yxw58xgzx9c6784ii")))

(define-public crate-bufpool-fixed-0.2.0 (c (n "bufpool-fixed") (v "0.2.0") (d (list (d (n "off64") (r "^0.6.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "08qrnjh4r59f10ww3g2hfckjbvvd64fa72gx9fw4fkvcfbzdabw2")))

