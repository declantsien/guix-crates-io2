(define-module (crates-io bu fp bufpool) #:use-module (crates-io))

(define-public crate-bufpool-0.1.0 (c (n "bufpool") (v "0.1.0") (d (list (d (n "off64") (r "^0.6.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1l496b9mdbfw5irds8g3d1jjflb2kjclwpdd1s8d0hg3g4ng0zq5")))

(define-public crate-bufpool-0.1.1 (c (n "bufpool") (v "0.1.1") (d (list (d (n "off64") (r "^0.6.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1fcwz0jn1bgz3m4kkn2bfa0mn8l5pshr8mj18gbnjb46hffdw2z4")))

(define-public crate-bufpool-0.1.2 (c (n "bufpool") (v "0.1.2") (d (list (d (n "off64") (r "^0.6.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0n55lrl4cvsqnrvlzniiqayja18k065d6cj8bphcjqqyp4qgr2hv")))

(define-public crate-bufpool-0.2.0 (c (n "bufpool") (v "0.2.0") (d (list (d (n "off64") (r "^0.6.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0filfabz8bgs97c9ri6ih8bmy8rbgvmafifwg8mpk1jixjyfwgfl")))

(define-public crate-bufpool-0.2.1 (c (n "bufpool") (v "0.2.1") (d (list (d (n "off64") (r "^0.6.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "15rpidxbpgxcw3ilyw2kfr3avajvy131520ggb5di1620482hfdl")))

(define-public crate-bufpool-0.2.3 (c (n "bufpool") (v "0.2.3") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.142") (d #t) (k 2)) (d (n "off64") (r "^0.6.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1jng1islaha72z1ml0hvarnhij83d6nz5islp3qfnbmy4hishjbp")))

(define-public crate-bufpool-0.2.4 (c (n "bufpool") (v "0.2.4") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.142") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0gb373g52qc9nq4hnmsb4d67wmbfsah5xvn5bjxp1zsv7kdb3kx3") (f (quote (("no-pool"))))))

