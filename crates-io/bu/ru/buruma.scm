(define-module (crates-io bu ru buruma) #:use-module (crates-io))

(define-public crate-buruma-0.1.0 (c (n "buruma") (v "0.1.0") (d (list (d (n "bytes") (r "^0.6.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.3.6") (f (quote ("full"))) (d #t) (k 0)))) (h "0wylq3f5y6jvnjbjd0ky9ndb9x5wnwl6nv5bid38412qbz16r2zw")))

