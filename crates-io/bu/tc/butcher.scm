(define-module (crates-io bu tc butcher) #:use-module (crates-io))

(define-public crate-butcher-0.1.0 (c (n "butcher") (v "0.1.0") (h "134p5vkp2iyjvasbfnqs9xd1q0i7df0yqaf2kh44dp7vs9sfanyh")))

(define-public crate-butcher-0.1.1 (c (n "butcher") (v "0.1.1") (h "0fnsy6gdsn8hlpq2djhzz19x852l7sd78pp0nqi9cx2r2n807ad3")))

(define-public crate-butcher-0.2.0 (c (n "butcher") (v "0.2.0") (d (list (d (n "butcher_proc_macro") (r "^0.2") (d #t) (k 0)))) (h "12d3fvyc2y0b72j8qks0x2v4g38i11hgxrzzrb1249l6k613py89")))

(define-public crate-butcher-0.2.1 (c (n "butcher") (v "0.2.1") (d (list (d (n "butcher_proc_macro") (r "^0.2") (d #t) (k 0)))) (h "1wr3yf08wpcyh82a0ql06myyk70b2jnwg4imw9h05kk78za1cl9n")))

(define-public crate-butcher-0.3.0 (c (n "butcher") (v "0.3.0") (d (list (d (n "butcher_proc_macro") (r "^0.3") (d #t) (k 0)))) (h "1fyiaasfghvhn7gah65ahj0mrg6r2q83jln4dpf7p6s715m3pr10")))

(define-public crate-butcher-0.4.0 (c (n "butcher") (v "0.4.0") (d (list (d (n "butcher_proc_macro") (r "^0.4") (d #t) (k 0)))) (h "18l5v5p7v1yrj2b4nr86xr4zzz7lbh5wqb9x0f1h8kycfpfvf43b")))

(define-public crate-butcher-0.5.0 (c (n "butcher") (v "0.5.0") (d (list (d (n "butcher_proc_macro") (r "^0.5") (d #t) (k 0)))) (h "12nwj3mhy4pls5k8l832rxhqsqirwjg6h7ddv4xd16njlq7bqn8i")))

