(define-module (crates-io bu nq bunq) #:use-module (crates-io))

(define-public crate-bunq-0.1.0 (c (n "bunq") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "isahc") (r "^0.9.8") (f (quote ("json"))) (d #t) (k 0)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "0p96sx4ff99gfj185jysnd8qp2amjskixhaha6zr1yd01bi7xrph")))

