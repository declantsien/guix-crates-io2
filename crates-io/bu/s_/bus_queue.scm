(define-module (crates-io bu s_ bus_queue) #:use-module (crates-io))

(define-public crate-bus_queue-0.1.0 (c (n "bus_queue") (v "0.1.0") (d (list (d (n "arc-swap") (r "^0.3.6") (d #t) (k 0)))) (h "1x6gf35cs4ii20hv60wky93fac14mh8j8fqhjr7857hkkfqngv4v")))

(define-public crate-bus_queue-0.1.1 (c (n "bus_queue") (v "0.1.1") (d (list (d (n "arc-swap") (r "^0.3.6") (d #t) (k 0)))) (h "0phwg351l12lyrs8i4c9qfk90shx1czdc3c30db2p93glin7hsra")))

(define-public crate-bus_queue-0.1.2 (c (n "bus_queue") (v "0.1.2") (d (list (d (n "arc-swap") (r "^0.3.6") (d #t) (k 0)))) (h "1cyicpwq11cz24i4sdxf83imhpfwg6489rlwyhdjgkv6s6r2lacx")))

(define-public crate-bus_queue-0.1.3 (c (n "bus_queue") (v "0.1.3") (d (list (d (n "arc-swap") (r "^0.3.6") (d #t) (k 0)))) (h "0bvd2lm35ajcfqlfqlwsqh93sipljgq4mb47v1gd0sbr9ys21z5n")))

(define-public crate-bus_queue-0.1.4 (c (n "bus_queue") (v "0.1.4") (d (list (d (n "arc-swap") (r "^0.3.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1035qnq5g82g6072k6ali5m2xqg49c6c5ij0k2l3fa0xvg61899z") (f (quote (("default" "async") ("async-example" "tokio") ("async" "futures"))))))

(define-public crate-bus_queue-0.2.0 (c (n "bus_queue") (v "0.2.0") (d (list (d (n "arc-swap") (r "^0.3.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0chnsvp2z6s5lb61ga3q2cqficnmqhlm9ds22gvfwc42sqg7j5sy") (f (quote (("default" "async") ("async-example" "tokio") ("async" "futures"))))))

(define-public crate-bus_queue-0.2.1 (c (n "bus_queue") (v "0.2.1") (d (list (d (n "arc-swap") (r "^0.3.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1bxb5p22g12czs551vwq66l9qrjz5lad3z8bchljdliaa2sfyj4y") (f (quote (("default" "async") ("async-example" "tokio") ("async" "futures"))))))

(define-public crate-bus_queue-0.2.2 (c (n "bus_queue") (v "0.2.2") (d (list (d (n "arc-swap") (r "^0.3.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0lgaqih4pgvmcjbrvil5dhzf8i5v16gvn83nv7icgz62azdg583i") (f (quote (("default" "async") ("async-example" "tokio") ("async" "futures"))))))

(define-public crate-bus_queue-0.2.3 (c (n "bus_queue") (v "0.2.3") (d (list (d (n "arc-swap") (r "^0.3.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (o #t) (d #t) (k 0)))) (h "17klaxkcgbrlaxhqsab06rbd736wc92fag1xrl4svl5g34nkr4zx") (f (quote (("default" "async") ("async-example" "tokio-core") ("async" "futures"))))))

(define-public crate-bus_queue-0.2.4 (c (n "bus_queue") (v "0.2.4") (d (list (d (n "arc-swap") (r "^0.3.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0dw3wwwz6vddrih3vn3ka3gyk4sbqcf23g0fhal3zjqzdkc8apzz") (f (quote (("default" "async") ("async-example" "tokio-core") ("async" "futures"))))))

(define-public crate-bus_queue-0.3.1 (c (n "bus_queue") (v "0.3.1") (d (list (d (n "arc-swap") (r "^0.3.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0ycycr9djvxrwfjpzs7as3sx905hg6ywk6i2bag9nmxq7hscmca4") (f (quote (("default" "async") ("async-example" "tokio-core") ("async" "futures"))))))

(define-public crate-bus_queue-0.3.2 (c (n "bus_queue") (v "0.3.2") (d (list (d (n "arc-swap") (r "^0.3.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (o #t) (d #t) (k 0)))) (h "15wa6f0ar1pijphbzzvzhjx2zsivndihbf5h460n4pr051hw46bf") (f (quote (("default" "async") ("async-example" "tokio-core") ("async" "futures"))))))

(define-public crate-bus_queue-0.3.3 (c (n "bus_queue") (v "0.3.3") (d (list (d (n "arc-swap") (r "^0.3.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1hqzs9b6awjznvdapqydp4pjix9j8gppq6jq6436ar2az8mmrmxk") (f (quote (("default" "async") ("async-example" "tokio") ("async" "futures"))))))

(define-public crate-bus_queue-0.3.4 (c (n "bus_queue") (v "0.3.4") (d (list (d (n "arc-swap") (r "^0.3.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.1") (o #t) (d #t) (k 0)))) (h "04zav212nlbp1kfq7g6p4v5qqaxy129q872vzzjmc0bnpvhbdxax") (f (quote (("default" "async") ("async-example" "tokio") ("async" "futures"))))))

(define-public crate-bus_queue-0.3.5 (c (n "bus_queue") (v "0.3.5") (d (list (d (n "arc-swap") (r "^0.3.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.1") (o #t) (d #t) (k 0)))) (h "01psq06crczqb967jyfxmn72bb58pv1l71425iqi6xy7k36dp4zh") (f (quote (("default" "async") ("async-example" "tokio") ("async" "futures"))))))

(define-public crate-bus_queue-0.3.6 (c (n "bus_queue") (v "0.3.6") (d (list (d (n "arc-swap") (r "^0.3.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0hhd203bjylrjsm8xmb84jgxf78dwc677gr3ig1p9jfsgm10iblk") (f (quote (("default" "async") ("async-example" "tokio") ("async" "futures"))))))

(define-public crate-bus_queue-0.3.7 (c (n "bus_queue") (v "0.3.7") (d (list (d (n "arc-swap") (r "^0.3.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1x795jy4rqihhwanjb8jmfnnypy49sw771dn3jr8z9ax5ww613w7") (f (quote (("default" "async") ("async-example" "tokio") ("async" "futures"))))))

(define-public crate-bus_queue-0.3.8 (c (n "bus_queue") (v "0.3.8") (d (list (d (n "arc-swap") (r "^0.3.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.1") (o #t) (d #t) (k 0)))) (h "11k23j2kw4h7ji4j3whlr3d817i2jxk5lzflvs3hkhq4mafjq6sc") (f (quote (("default" "async") ("async-example" "tokio") ("async" "futures"))))))

(define-public crate-bus_queue-0.3.9 (c (n "bus_queue") (v "0.3.9") (d (list (d (n "arc-swap") (r "^0.3.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1sglaiyd7qvid450r844ssipvdsvxbh902zw2pfds8b3fipip5nr") (f (quote (("default" "async") ("async-example" "tokio") ("async" "futures"))))))

(define-public crate-bus_queue-0.3.10 (c (n "bus_queue") (v "0.3.10") (d (list (d (n "arc-swap") (r "^0.3.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0snlz6lmb5vbwfxv0gpbd2jq79dhm266nbz4cczgvsqp99s6w7pq") (f (quote (("default" "async") ("async-example" "tokio") ("async" "futures"))))))

(define-public crate-bus_queue-0.4.0 (c (n "bus_queue") (v "0.4.0") (d (list (d (n "arc-swap") (r "^0.3.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lockfree") (r "^0.5.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (o #t) (d #t) (k 0)))) (h "02zjifhgrq2zni1igbjqjw9sj8zx87glv595n8yj37dp2xcqxn8x") (f (quote (("default" "async") ("async-example" "tokio") ("async" "futures"))))))

(define-public crate-bus_queue-0.4.1 (c (n "bus_queue") (v "0.4.1") (d (list (d (n "arc-swap") (r "^0.3.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lockfree") (r "^0.5.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1z7iayv3dvbb6aiwrz1ax4blgfj93bljjrnrmyfsp9vzn7hgkgh2") (f (quote (("default" "async") ("async-example" "tokio") ("async" "futures"))))))

(define-public crate-bus_queue-0.5.0 (c (n "bus_queue") (v "0.5.0") (d (list (d (n "arc-swap") (r "^0.4.6") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)))) (h "18pikq51mw27f5m2q2h6322lq4lvv2qa3qp5jr9qdg9yz65jn6ml")))

(define-public crate-bus_queue-0.5.1 (c (n "bus_queue") (v "0.5.1") (d (list (d (n "arc-swap") (r "^0.4.6") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)))) (h "01ryyqiv9400hb3dclacxzf57sv1dgbiqci8sx1i7f6s6r1pr629")))

(define-public crate-bus_queue-0.5.3 (c (n "bus_queue") (v "0.5.3") (d (list (d (n "arc-swap") (r "^0.4.6") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (f (quote ("thread-pool"))) (d #t) (k 2)) (d (n "futures-core") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-test") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0va94f0ia9im0jjd0cv244cd02zg4arilwma34h21l2lq17f763y") (f (quote (("rwlock-export") ("rwlock") ("default" "arcswap" "rwlock") ("arcswap" "arc-swap"))))))

