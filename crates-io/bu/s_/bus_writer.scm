(define-module (crates-io bu s_ bus_writer) #:use-module (crates-io))

(define-public crate-bus_writer-0.1.0 (c (n "bus_writer") (v "0.1.0") (d (list (d (n "bus") (r "^2.0.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.3") (d #t) (k 0)))) (h "1vn7a18zkxkg0fndlkyanhpgpi0bhwqvv2l1hr24dmh1hqnx5sjz")))

(define-public crate-bus_writer-0.1.1 (c (n "bus_writer") (v "0.1.1") (d (list (d (n "bus") (r "^2.0.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.3") (d #t) (k 0)))) (h "0dz8rckm81wfxl9aga1bg3xqjbjsx1lkgzd1w6yf1a3v7302l95w")))

(define-public crate-bus_writer-0.1.2 (c (n "bus_writer") (v "0.1.2") (d (list (d (n "bus") (r "^2.0.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.3") (d #t) (k 0)))) (h "0ybx1c4ba06jlmpjyv2z74n1905g62915frlznqfl7wihilgrbva")))

