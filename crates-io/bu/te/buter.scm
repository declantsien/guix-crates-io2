(define-module (crates-io bu te buter) #:use-module (crates-io))

(define-public crate-buter-0.1.0 (c (n "buter") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)))) (h "0sc9h37yvxy4xi08bgw0vzl4ma25xm36lcsbzgp0bgfrz5gnxii8")))

(define-public crate-buter-1.0.0 (c (n "buter") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)) (d (n "leak_slice") (r "^0.2.0") (d #t) (k 0)) (d (n "lock_api") (r "^0.4.7") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1qmr9ll62sjjlkvs5lnkhdy4wvzwrqs86c26n6cibw9vq7g8qsl0")))

(define-public crate-buter-1.1.0 (c (n "buter") (v "1.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)) (d (n "leak_slice") (r "^0.2.0") (d #t) (k 0)) (d (n "lock_api") (r "^0.4.7") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1f894m7h3n9yfssadcc8z49jak93dipaaz6q91vfj61dfvbwcw02")))

(define-public crate-buter-1.1.1 (c (n "buter") (v "1.1.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0jr551ihh6q3yxg0hmq7v6g2iwla438wm6jm8bcq69w0abprc410")))

(define-public crate-buter-1.1.2 (c (n "buter") (v "1.1.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "08yk6aqkphmar0lh8qm7zk9fazv3fbmvv3cvia4y0pwwxgnvl1cc")))

(define-public crate-buter-1.2.2 (c (n "buter") (v "1.2.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0b9mbmxwwdqjfyw2fh9zwv17fjdlrzpbv2jbkd57aw4dal2xapbs")))

(define-public crate-buter-1.2.3 (c (n "buter") (v "1.2.3") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0va7jy9pk7inxc2c7n2pahfbkx1kck7c9x9z8prfbcglfcrz58ar")))

(define-public crate-buter-1.2.4 (c (n "buter") (v "1.2.4") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "0nrlqjz69jl8i41rpspm253pif8xsx80q01xxwwjn38pifkd38yk")))

