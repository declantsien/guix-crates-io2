(define-module (crates-io bu i_ bui_basic) #:use-module (crates-io))

(define-public crate-bui_basic-0.0.1 (c (n "bui_basic") (v "0.0.1") (d (list (d (n "bui") (r "^0.0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "jni") (r "^0.19") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "winit") (r "^0.26") (d #t) (k 2)))) (h "06lc7l378v02z80wilxksn0i5234skgckmn2f21w5a5z5y2jgzi9") (y #t)))

