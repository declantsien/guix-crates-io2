(define-module (crates-io bu sb busbus) #:use-module (crates-io))

(define-public crate-busbus-0.1.0 (c (n "busbus") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)))) (h "0knc9immwaxzzmlw2zlydkpwswazjfs5saxz5dlj9rqawrii3xlc")))

