(define-module (crates-io bu ll bullettrain) #:use-module (crates-io))

(define-public crate-bullettrain-0.1.0 (c (n "bullettrain") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "0a5hx7qnvkrb19ni68v1sggg4z2vs7w9mwpnds3wanhn4h1n62g1")))

(define-public crate-bullettrain-0.2.0 (c (n "bullettrain") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "17ml2a0dmx3d8fypyv8ydnp5v7iqbcgiqf8h6mrihcvvlm2dg5gy")))

