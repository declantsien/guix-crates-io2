(define-module (crates-io bu ll bulletformat) #:use-module (crates-io))

(define-public crate-bulletformat-0.1.0 (c (n "bulletformat") (v "0.1.0") (h "1l4g74zb0asnharjilfvhz90gq5l4a41zm86x0r9y2d3ja4b5lcp")))

(define-public crate-bulletformat-0.1.1 (c (n "bulletformat") (v "0.1.1") (h "1fdwd81xjiwfh8xx43p04sz0n1z1awsyf5dqs4jlmsr1z82f1x5v")))

(define-public crate-bulletformat-0.2.0 (c (n "bulletformat") (v "0.2.0") (h "1ibw9iwd9xlfppdy0p0l6jk13nv0k0nk9rks6scz5n809hdl3gg0")))

(define-public crate-bulletformat-0.3.0 (c (n "bulletformat") (v "0.3.0") (h "1f66mgri6sk7i4i3wqj405v27v2nm0x6l6m0xpm5hazqnlichqnn")))

(define-public crate-bulletformat-1.0.0 (c (n "bulletformat") (v "1.0.0") (h "08wvs0gd0q6qbhzrng6rkf8vh7wbph26vihl9dv3akji99m4f9rj")))

(define-public crate-bulletformat-1.1.0 (c (n "bulletformat") (v "1.1.0") (h "0i5xldcfbizigvhbc6c9xqw8srw6w8ws7dpqifwsyn7574pgxbk4")))

(define-public crate-bulletformat-1.2.0 (c (n "bulletformat") (v "1.2.0") (h "1y90w4p2gbah898x3fr826jakrcfgm0qgj9k47dwy46264vrw3y7")))

(define-public crate-bulletformat-1.3.0 (c (n "bulletformat") (v "1.3.0") (h "18j3ddjjbjnrzhmkr92ckjqnrkc38c43mpp6sm15siq7k95xk3in")))

(define-public crate-bulletformat-1.4.0 (c (n "bulletformat") (v "1.4.0") (h "171b2d9x0q5w2i79594xhr48rkbfc2gqngbibfv0r6v2kigyqasn")))

(define-public crate-bulletformat-1.5.0 (c (n "bulletformat") (v "1.5.0") (h "0m4bshy0q4mxg7bz5arslwcszi3zh4pi547n5d1id9bacf381cfg")))

(define-public crate-bulletformat-1.6.0 (c (n "bulletformat") (v "1.6.0") (h "1pj2ppnikks89rk53hw7n9il0cky3mj97kdpj4q4y6flyjp20lp2")))

