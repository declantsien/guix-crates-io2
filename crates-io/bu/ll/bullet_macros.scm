(define-module (crates-io bu ll bullet_macros) #:use-module (crates-io))

(define-public crate-bullet_macros-0.1.0 (c (n "bullet_macros") (v "0.1.0") (d (list (d (n "bullet_core") (r "^0.1.0") (d #t) (k 0)) (d (n "simd") (r "^0.2.0") (d #t) (k 0)))) (h "0ds51ibx5g4r9d99ysv0ccnm8v7y3ycccgvqbf9vyvkxgvbz084v")))

