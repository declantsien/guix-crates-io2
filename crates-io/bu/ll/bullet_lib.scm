(define-module (crates-io bu ll bullet_lib) #:use-module (crates-io))

(define-public crate-bullet_lib-1.0.0 (c (n "bullet_lib") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.68.1") (o #t) (d #t) (k 1)) (d (n "bulletformat") (r "^1.0.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (o #t) (d #t) (k 1)))) (h "14df2rl47xgikzzqhlczddf6w4f7pbm9rfyvvacbgqyji9669b93") (f (quote (("cuda" "bindgen" "cc"))))))

