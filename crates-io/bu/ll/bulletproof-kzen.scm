(define-module (crates-io bu ll bulletproof-kzen) #:use-module (crates-io))

(define-public crate-bulletproof-kzen-1.2.0 (c (n "bulletproof-kzen") (v "1.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "curv-kzen") (r "^0.9") (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "0y2w8rl6crcvq9lfx1raagr8172393j7qhjggy7aj8m3mxxd8yq2") (f (quote (("default" "curv-kzen/rust-gmp-kzen"))))))

(define-public crate-bulletproof-kzen-1.2.1 (c (n "bulletproof-kzen") (v "1.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "curv-kzen") (r "^0.10") (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "1wq0d626rdj6ab5id4j47i4ql6nqrjpklgfkyfq89yids73d66lf") (f (quote (("default" "curv-kzen/rust-gmp-kzen"))))))

