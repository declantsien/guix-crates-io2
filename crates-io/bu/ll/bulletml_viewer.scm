(define-module (crates-io bu ll bulletml_viewer) #:use-module (crates-io))

(define-public crate-bulletml_viewer-0.1.0 (c (n "bulletml_viewer") (v "0.1.0") (d (list (d (n "bulletml") (r "^0.2") (d #t) (k 0)) (d (n "piston_window") (r "^0.107") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "138li39x33ylbxnyfcqhxvfasgry5gligw8z8x65vyn5g4w8c1rb")))

