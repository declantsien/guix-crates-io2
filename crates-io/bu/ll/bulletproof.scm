(define-module (crates-io bu ll bulletproof) #:use-module (crates-io))

(define-public crate-bulletproof-0.1.0 (c (n "bulletproof") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1c5viwpdqpizb3hdm2c6w3s9a9jd7xk4zr2lv10v6b1r2f4wdz39")))

(define-public crate-bulletproof-0.1.1 (c (n "bulletproof") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01smvbllivc5l83vgqyly0vp9r18fcbz6i5w757xpsqf5v3fjs6j")))

(define-public crate-bulletproof-0.2.0 (c (n "bulletproof") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1f51q3hk6aagfg31q6xdjmfm22cggkh73wxflzdjx2q4lizrn9jv")))

