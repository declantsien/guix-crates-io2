(define-module (crates-io bu ll bulletml_viewer_rs) #:use-module (crates-io))

(define-public crate-bulletml_viewer_rs-0.1.0 (c (n "bulletml_viewer_rs") (v "0.1.0") (d (list (d (n "bulletml") (r "^0.2") (d #t) (k 0)) (d (n "piston_window") (r "^0.107") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1nmj4h34vkl1qnjki20qclmx3lv1qdj72hg646nmab6c8462k3n4") (y #t)))

