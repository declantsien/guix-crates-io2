(define-module (crates-io bu ll bulletml) #:use-module (crates-io))

(define-public crate-bulletml-0.1.0 (c (n "bulletml") (v "0.1.0") (d (list (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "fasteval") (r "^0.2") (d #t) (k 0)) (d (n "indextree") (r "^3.0") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "roxmltree") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rxl6x44zmsgd0s8v5mrf08zjwjfmkk48fmp25dc9p1hm1rxbiw5") (f (quote (("backtrace"))))))

(define-public crate-bulletml-0.2.0 (c (n "bulletml") (v "0.2.0") (d (list (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "fasteval") (r "^0.2") (d #t) (k 0)) (d (n "indextree") (r "^4.0") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "roxmltree") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01virgxqvxdwgr3g3wah1rmcp2mmx6bla6q7iakaf3d1zl88ciwf") (f (quote (("backtrace"))))))

(define-public crate-bulletml-0.2.1 (c (n "bulletml") (v "0.2.1") (d (list (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "fasteval") (r "^0.2") (d #t) (k 0)) (d (n "indextree") (r "^4.0") (d #t) (k 0)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "roxmltree") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zm3hish52c7hj7a8f55j4ia8m02p78j9808hd7vjg97cxvv5pkf") (f (quote (("backtrace"))))))

