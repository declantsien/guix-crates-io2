(define-module (crates-io bu ll bullshit-rs) #:use-module (crates-io))

(define-public crate-bullshit-rs-0.1.0 (c (n "bullshit-rs") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1mvsckga5qw5w6650bp9fq9wdbxx1llrh3ifyfhqi11gmhd26g7n")))

(define-public crate-bullshit-rs-0.2.0 (c (n "bullshit-rs") (v "0.2.0") (d (list (d (n "js-sys") (r "^0.3.55") (o #t) (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (o #t) (k 0)))) (h "06nvc3y0x4ivp2qmxi5bvrp4r7lnkckbya1rzvnlf6i9m1x7hh2p") (f (quote (("js" "rand/std_rng" "js-sys") ("default" "rand/std" "rand/std_rng"))))))

