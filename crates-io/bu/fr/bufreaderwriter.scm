(define-module (crates-io bu fr bufreaderwriter) #:use-module (crates-io))

(define-public crate-bufreaderwriter-0.1.0 (c (n "bufreaderwriter") (v "0.1.0") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0zk0bw7rphxvbpivcxaavn82jcnd71gjkkc4a43lsxph8bd02i9f")))

(define-public crate-bufreaderwriter-0.1.1 (c (n "bufreaderwriter") (v "0.1.1") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0wrrhx0l65xxsp1pzc61lrxb8h9hf8r23zg8xg47hjkh15hc7ydj")))

(define-public crate-bufreaderwriter-0.1.2 (c (n "bufreaderwriter") (v "0.1.2") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1cfkigil1kmal49hl1c1giks7bzbjg1gyl29fz936nch189d4cy6")))

(define-public crate-bufreaderwriter-0.2.0 (c (n "bufreaderwriter") (v "0.2.0") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1zb6vzs7rb16ji7w3bp4543fm83jbrsd0hh2m2gx6brbqs19fip5") (y #t)))

(define-public crate-bufreaderwriter-0.2.1 (c (n "bufreaderwriter") (v "0.2.1") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1ilwlv3v214isgl59if5wwswigwcy6gksq0m5jy9c5rfaq0zpcpq") (y #t)))

(define-public crate-bufreaderwriter-0.2.2 (c (n "bufreaderwriter") (v "0.2.2") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0pq9pwbhdh1cf4qjvyw5x8120q9xkm98lnc6zqc835jp18cxp89m")))

(define-public crate-bufreaderwriter-0.2.3 (c (n "bufreaderwriter") (v "0.2.3") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "13l7imqj5byr813r1g1wp3d8d50rmf1mzg5c00hpfpm193nnan7p")))

(define-public crate-bufreaderwriter-0.2.4 (c (n "bufreaderwriter") (v "0.2.4") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "17m4a2c3ixlyiw7yx8lkg8qxf4sd9i5xvwkvh3xxj9p6rnbsvrgc")))

