(define-module (crates-io bu fr bufr-dump) #:use-module (crates-io))

(define-public crate-bufr-dump-0.1.0 (c (n "bufr-dump") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "bufr") (r "^0.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1l3kdd6cbgv5817vgv4rpvw3h3f97brch6kgqg8ivv5s3356bbdq")))

(define-public crate-bufr-dump-0.1.1 (c (n "bufr-dump") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "bufr") (r "^0.1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0z8v57463v9w0qayg8pjaay8gwb1nw37wkl8lf68lyj9hjpc1mwy")))

