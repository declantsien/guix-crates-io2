(define-module (crates-io bu fr bufrng) #:use-module (crates-io))

(define-public crate-bufrng-1.0.0 (c (n "bufrng") (v "1.0.0") (d (list (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)))) (h "03n36vfnpd04sk68x9am7vgwjgipl9k2kdzsy64479vlqfav0kj1")))

(define-public crate-bufrng-1.0.1 (c (n "bufrng") (v "1.0.1") (d (list (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)))) (h "01a5c5vcddk51hhxvfa4pia3hq8s8cs85g3jrh91gaf35a4s8asi")))

(define-public crate-bufrng-1.0.2 (c (n "bufrng") (v "1.0.2") (d (list (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)))) (h "1ikl86s774l9zfvflch34zqxb93h4hlfrpchnbvj98b97jmy0v5i")))

