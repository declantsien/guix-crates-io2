(define-module (crates-io bu fr bufr) #:use-module (crates-io))

(define-public crate-bufr-0.0.1 (c (n "bufr") (v "0.0.1") (d (list (d (n "bitreader") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "getset") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ikbqpw1ss05ab0v05f8piadbcvk7p75axda4vads2iiqhj9g3d3")))

(define-public crate-bufr-0.1.1 (c (n "bufr") (v "0.1.1") (d (list (d (n "bitreader") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "getset") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1v9kx80x6gi3160qk2b553yl2n44ln55bv87phib1b24hydazgp6")))

