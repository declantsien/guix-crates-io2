(define-module (crates-io bu n- bun-cli) #:use-module (crates-io))

(define-public crate-bun-cli-0.1.0 (c (n "bun-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "inquire") (r "^0.7.4") (d #t) (k 0)))) (h "19fxd0dvqhyfyvq9daiw5yiik2fg6781a2b64r1689c6b3jn0ki4")))

(define-public crate-bun-cli-0.2.0 (c (n "bun-cli") (v "0.2.0") (h "0zji6askb6lbjg2wwgrgqjdhkwl2b8id8yhx7057553z15a0fp1j")))

(define-public crate-bun-cli-0.3.0 (c (n "bun-cli") (v "0.3.0") (h "1rr1vlkis1w2aygk11i49s62644x26sflypc9kcvy41dn6bkihpw")))

(define-public crate-bun-cli-0.3.1 (c (n "bun-cli") (v "0.3.1") (h "1q3jdngjhmc392dccj3xkffiiji5fnp3b31281pr7kslibd8pa3b")))

