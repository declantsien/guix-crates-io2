(define-module (crates-io bu rs burst) #:use-module (crates-io))

(define-public crate-burst-0.0.1 (c (n "burst") (v "0.0.1") (h "1962yi36qwxh4kp0nwc5ab4srq8wq8lmm6qj6x917kw1vb7jxgcq")))

(define-public crate-burst-0.0.2 (c (n "burst") (v "0.0.2") (h "0ybvs8pjdz3zg3qwj9kn48im8maixmizzahgwdp8ap2zm6zlkvd3")))

(define-public crate-burst-0.0.3 (c (n "burst") (v "0.0.3") (h "1aslgfr520g6r5zlbv1708m2vkzv39j6sr9p4xsl9l8ni4xgf9i1")))

