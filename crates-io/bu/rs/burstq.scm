(define-module (crates-io bu rs burstq) #:use-module (crates-io))

(define-public crate-burstq-0.1.0 (c (n "burstq") (v "0.1.0") (d (list (d (n "async-event") (r "^0.1.0") (d #t) (k 0)) (d (n "core_affinity") (r "^0.8") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "flume") (r "^0.11") (d #t) (k 2)) (d (n "loom") (r "^0.7") (f (quote ("futures"))) (d #t) (t "cfg(loom)") (k 0)) (d (n "pollster") (r "^0.3") (d #t) (k 2)))) (h "09sc664xn95hpjj2c6vq07qnxkj9i8pc8a9kvyn2ighvsb3sgrbw") (f (quote (("std") ("default" "std") ("alloc"))))))

