(define-module (crates-io bu rn burnt-glue) #:use-module (crates-io))

(define-public crate-burnt-glue-0.1.2 (c (n "burnt-glue") (v "0.1.2") (d (list (d (n "cosmwasm-std") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01lmw33xxa297fddx8hf78s8561a8myrz08xmz1gn9zkiaj6wr9p")))

(define-public crate-burnt-glue-0.1.3 (c (n "burnt-glue") (v "0.1.3") (d (list (d (n "cosmwasm-std") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "06645gm1kriyzbfqaqlq7k36bl9322l1jv8i00jj1xafvmk81y0n")))

(define-public crate-burnt-glue-0.1.4 (c (n "burnt-glue") (v "0.1.4") (d (list (d (n "cosmwasm-std") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16j77xv7nrk8m5w51c9vkij3l9lbz81iqxdrc3gafa9hgw3vk9ix")))

(define-public crate-burnt-glue-0.2.0 (c (n "burnt-glue") (v "0.2.0") (d (list (d (n "cosmwasm-std") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rfp5wd61h3l17shcnnm0p8391ldzh4y868japd7wjprl6wvffig")))

(define-public crate-burnt-glue-0.2.1 (c (n "burnt-glue") (v "0.2.1") (d (list (d (n "cosmwasm-std") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0l5ijd262bn198bvji6ipk1mvgn6gk8991lns976rb5wmbj9gxna")))

(define-public crate-burnt-glue-0.2.2 (c (n "burnt-glue") (v "0.2.2") (d (list (d (n "cosmwasm-std") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0mkx6vn750vvz5h5i4s1fqkap82y0pm43q6nwby67c4wgrqwkcm5")))

