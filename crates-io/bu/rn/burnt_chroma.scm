(define-module (crates-io bu rn burnt_chroma) #:use-module (crates-io))

(define-public crate-burnt_chroma-0.1.0 (c (n "burnt_chroma") (v "0.1.0") (d (list (d (n "arbitrary") (r "^1.3.0") (d #t) (k 2)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "mockall") (r "^0.11.4") (d #t) (k 2)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.29.1") (d #t) (k 0)))) (h "0fk8nsrjcrr610b8phizv9g1byy5b8gib9yzib0g8p9xsxlskp4y") (f (quote (("debug_assertions"))))))

