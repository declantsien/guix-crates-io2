(define-module (crates-io bu rn burn-tensor-testgen) #:use-module (crates-io))

(define-public crate-burn-tensor-testgen-0.3.0 (c (n "burn-tensor-testgen") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0y5vsq44dpb2cq5fa7wgmwb5xw0fqw8xnh3ahii6flc9srn0rb32")))

(define-public crate-burn-tensor-testgen-0.4.0 (c (n "burn-tensor-testgen") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "048ijzn6jsjafa7r2509gzyda61cx1i06a2m6zxqiywq3vdp9wy7")))

(define-public crate-burn-tensor-testgen-0.5.0 (c (n "burn-tensor-testgen") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0c4zx4qx0gnm3dakh76yna7xz14lv22m6vvh5nnxxp37k82jcnrs")))

(define-public crate-burn-tensor-testgen-0.6.0 (c (n "burn-tensor-testgen") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "09ds37iyr5hxa2cni121plkhk22a2xkxb9gmcjq6v4qgdb4535rl")))

(define-public crate-burn-tensor-testgen-0.7.0 (c (n "burn-tensor-testgen") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "190dr71ik9ckzbx7gcrbq3dfzdjd8lvlnsgvzb3b3whzw1gkkvk1")))

(define-public crate-burn-tensor-testgen-0.8.0 (c (n "burn-tensor-testgen") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0092wrgkmc2cfmqrdg55vamkpd2l90f18yi1npb9qrqnj0x8w82f")))

(define-public crate-burn-tensor-testgen-0.9.0 (c (n "burn-tensor-testgen") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08agl4ipaqf94xyr5dl98bcms9lv9y1bvr8a1y8p0549jifgnbpw")))

(define-public crate-burn-tensor-testgen-0.10.0 (c (n "burn-tensor-testgen") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07srzg08cj476cqyl57v23x0lkpd8qxkrmkkr07g5g1qww0a0bgr")))

(define-public crate-burn-tensor-testgen-0.11.0 (c (n "burn-tensor-testgen") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "0331jh58zjqcfm9m083pygr7pha13jbk5sm4i8zj187nw24h1xqd")))

(define-public crate-burn-tensor-testgen-0.11.1 (c (n "burn-tensor-testgen") (v "0.11.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "11rzccqpsljlawa6i3vnvzklgjknfpl64pp1mrhlx5xbn2dafmd5")))

(define-public crate-burn-tensor-testgen-0.12.0 (c (n "burn-tensor-testgen") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "1diqwk6j9zhnb362kfl41xihi1g762idh3vkiy1v8jdk1mffzhwx")))

(define-public crate-burn-tensor-testgen-0.12.1 (c (n "burn-tensor-testgen") (v "0.12.1") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "0i2wsr4bk8g5044qjj4xj96dxbf5j1g6llccwd39rlp9k4938gvl")))

(define-public crate-burn-tensor-testgen-0.13.0 (c (n "burn-tensor-testgen") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "1djg9krrv8291hpw908vbrl3q8wsxnfck3z3z2ah4lj3xvzqdq2a")))

(define-public crate-burn-tensor-testgen-0.13.1 (c (n "burn-tensor-testgen") (v "0.13.1") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "1065fm0f16gn8ldb2xdxcw019xjb2y826f6w54gpvm1alqb4pww8")))

(define-public crate-burn-tensor-testgen-0.13.2 (c (n "burn-tensor-testgen") (v "0.13.2") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)))) (h "06183q9n6xyjgqid47ah8kdlx15w006acp5wdk5cjdl7v8gmxx96")))

