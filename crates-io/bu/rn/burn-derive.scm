(define-module (crates-io bu rn burn-derive) #:use-module (crates-io))

(define-public crate-burn-derive-0.1.0 (c (n "burn-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06j3qp4ap6vmq02d2h8n4ysbjfyz653r7q7szyk8f1cij4npjn44")))

(define-public crate-burn-derive-0.2.3 (c (n "burn-derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1r97fxw5njv2pdsmw68i1v7d5k1libn1jb0izvscay7fqndz4wi6")))

(define-public crate-burn-derive-0.3.0 (c (n "burn-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13ndgh2f6ysh21yhb45a583bwvb5ni4pn5vzr0g30bv9r3wx5crc")))

(define-public crate-burn-derive-0.4.0 (c (n "burn-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "1y7z4691q5hnbjsakaady190v8kz4cyf6cc6az7grn764igb4smm")))

(define-public crate-burn-derive-0.5.0 (c (n "burn-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "0b89yxnnf6y74w7mngw9g23wf4sy10vssrcahalk8a30g155ybmm")))

(define-public crate-burn-derive-0.6.0 (c (n "burn-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "16b5iljww8qcqwckvkxm9wjfa9ahs51039jmr8s4f32q5kn516m7")))

(define-public crate-burn-derive-0.7.0 (c (n "burn-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (d #t) (k 0)))) (h "05ikn4bdvb560vibghdhwqkxl00p2jhbwzkylprjck1qgn57vb6m")))

(define-public crate-burn-derive-0.8.0 (c (n "burn-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jk97qpkpzzwadfhlic25lbprzfr5qiggiccvdsi6vngy85igfc6")))

(define-public crate-burn-derive-0.9.0 (c (n "burn-derive") (v "0.9.0") (d (list (d (n "derive-new") (r "^0.5.9") (k 0)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "008i0zvhxjk9sza1bsa88l3b1a8kgg034r0c760dmj37zscnr7k0")))

(define-public crate-burn-derive-0.10.0 (c (n "burn-derive") (v "0.10.0") (d (list (d (n "derive-new") (r "^0.5.9") (k 0)) (d (n "proc-macro2") (r "^1.0.68") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1dllkb2dhpj9ci3j6mzvfq4i2s6i79kahydh6xdwdwdld80dc9h7")))

(define-public crate-burn-derive-0.11.0 (c (n "burn-derive") (v "0.11.0") (d (list (d (n "derive-new") (r "^0.5.9") (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gnaygv02f71smifsyrcva4nmahmrm5lmhk74wl0mqv4sx7dki22")))

(define-public crate-burn-derive-0.11.1 (c (n "burn-derive") (v "0.11.1") (d (list (d (n "derive-new") (r "^0.5.9") (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1z6q89h351r35gz3zb3cxvwk8624alv8kvd5fa9b89d5d7ziai6l")))

(define-public crate-burn-derive-0.12.0 (c (n "burn-derive") (v "0.12.0") (d (list (d (n "derive-new") (r "^0.6.0") (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cjmzl4ym6bm68zwl0d3cwzy948qnscfxyaj8kd66nnsljkar6x4")))

(define-public crate-burn-derive-0.12.1 (c (n "burn-derive") (v "0.12.1") (d (list (d (n "derive-new") (r "^0.6.0") (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0yybrmvfsakh5h4vcpp0mlxk4nhynzd4wdqw37ra7fijbzg4kwbh")))

(define-public crate-burn-derive-0.13.0 (c (n "burn-derive") (v "0.13.0") (d (list (d (n "derive-new") (r "^0.6.0") (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13jyr2lmdk5idbvhjb4flpd075bvlx1wa42ffl76xm2p9q6hw3r7")))

(define-public crate-burn-derive-0.13.1 (c (n "burn-derive") (v "0.13.1") (d (list (d (n "derive-new") (r "^0.6.0") (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rbas4b8lnsqr2p22qkbypk02irhj4hys2f7fhd1z0jl85zpxgvx")))

(define-public crate-burn-derive-0.13.2 (c (n "burn-derive") (v "0.13.2") (d (list (d (n "derive-new") (r "^0.6.0") (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cfrg7pq4n60fif0d4xkbs0dq4ab7mfa33ffsgzm8w0w2wnaq646")))

