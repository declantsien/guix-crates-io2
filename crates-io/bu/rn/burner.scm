(define-module (crates-io bu rn burner) #:use-module (crates-io))

(define-public crate-burner-0.1.0 (c (n "burner") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "013kqgn2y1kbmhy60p8x2wm71ni83mlp0v6hda83hk6j8jf3hmb8")))

(define-public crate-burner-0.1.1 (c (n "burner") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0w2hilgcapagr9rhybwz3zpa7wf3arigd8q7fy11cwd9zihgyk1s")))

(define-public crate-burner-0.1.2 (c (n "burner") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hfvwi8r7bj9xlr74a0i7c0frv9gw8gbi6x2xw9haxaa3d859f1d")))

(define-public crate-burner-0.1.3 (c (n "burner") (v "0.1.3") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gkyxzjwhgax2wpjaj5harpwkz0vkqcpmgsbkivxjzv0wfcwxpc6")))

(define-public crate-burner-0.1.4 (c (n "burner") (v "0.1.4") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jl2p30vglffj6q2yazfq347pgn85xgszsb31d9qilab5lz61dh0")))

(define-public crate-burner-0.1.5 (c (n "burner") (v "0.1.5") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18r4dm699i09y5syg2kh2kiwlmgi42s9c8sw2rr4lngppz62ksml")))

(define-public crate-burner-0.1.6 (c (n "burner") (v "0.1.6") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00gk35n9fsidfq3vsm0i7kqhakgq5i15v6a253n89dp2z0g0r3hf")))

(define-public crate-burner-0.1.7 (c (n "burner") (v "0.1.7") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01bvv8c6hi4vm6f94zrjxrslcv9ya9i6jc9lx7zrhfpkfidfk60n")))

(define-public crate-burner-0.1.8 (c (n "burner") (v "0.1.8") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12zz6v0l4l90h6gwgmzxb9a5xxn4w8jp83q1rvllc90gd017rz36")))

(define-public crate-burner-0.1.9 (c (n "burner") (v "0.1.9") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16fgcnllwmkwxdfq2q60jwha5k9agjncbjj3j1dgp4jwbi6rs9c3")))

(define-public crate-burner-0.2.0 (c (n "burner") (v "0.2.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19vzbrnds76gi7q4ydw3y9i7kp9d78pw5zgah9b64yar7k1p22f3")))

(define-public crate-burner-0.2.1 (c (n "burner") (v "0.2.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0dmgska03dzdy6gacxd94b67f9zck5vf9pq9kwvsr06wjhwn7cm9")))

(define-public crate-burner-0.2.2 (c (n "burner") (v "0.2.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17za0m5aq7pjhimxxlabhvfqdap4485m7g1lnzapbwffcr0c3aki")))

(define-public crate-burner-0.2.3 (c (n "burner") (v "0.2.3") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jnmk1a23ribqqh2vffy0j9z9i79amh8hhcvm6pj2qj4zk170f78")))

