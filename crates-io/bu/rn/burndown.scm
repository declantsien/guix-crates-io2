(define-module (crates-io bu rn burndown) #:use-module (crates-io))

(define-public crate-burndown-0.1.0 (c (n "burndown") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("formatting" "parsing"))) (d #t) (k 0)))) (h "0ca9v2b1pdhm22wpyxcvwkdckvs8mg3wqlf47s0gbnfcp5zy4gpl") (y #t)))

