(define-module (crates-io bu rn burn_operation) #:use-module (crates-io))

(define-public crate-burn_operation-0.1.0 (c (n "burn_operation") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jwalk") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 0)))) (h "1qzhraqfp052p808d4452yv1wvx7ymzry6vrhiyyhs4dxd48alz3")))

