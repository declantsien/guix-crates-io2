(define-module (crates-io bu rp burpsuite-kit) #:use-module (crates-io))

(define-public crate-burpsuite-kit-0.1.0 (c (n "burpsuite-kit") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "http") (r "^0.2") (k 0)) (d (n "once_cell") (r "^1.5") (f (quote ("std"))) (k 0)) (d (n "quick-xml") (r "^0.20") (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (k 0)))) (h "174xl7gqlc57yy4lyrdwahk1d7iwz8iycc3f16ny67ssz14j8aib")))

(define-public crate-burpsuite-kit-0.1.1 (c (n "burpsuite-kit") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "http") (r "^0.2") (k 0)) (d (n "once_cell") (r "^1.5") (f (quote ("std"))) (k 0)) (d (n "quick-xml") (r "^0.20") (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (k 0)))) (h "1drdk481xvhjgcr6yxhc1iw4y30srw7v7iigdpjszckp8rgk2bn9")))

(define-public crate-burpsuite-kit-0.1.2 (c (n "burpsuite-kit") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "http") (r "^0.2") (k 0)) (d (n "once_cell") (r "^1.5") (f (quote ("std"))) (k 0)) (d (n "quick-xml") (r "^0.20") (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (k 0)))) (h "05x7msja5hg9dw0kbqi3pf5byvcizp7lchn6vn2w90hqcx5h2fhb")))

(define-public crate-burpsuite-kit-0.1.3 (c (n "burpsuite-kit") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "http") (r "^0.2") (k 0)) (d (n "once_cell") (r "^1.5") (f (quote ("std"))) (k 0)) (d (n "quick-xml") (r "^0.20") (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (k 0)))) (h "0bwzjg83ilzlc1rq25q6jhalkc6wy88wpbfqk9a457b1q5in5gsp")))

(define-public crate-burpsuite-kit-0.1.4 (c (n "burpsuite-kit") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "http") (r "^0.2") (k 0)) (d (n "once_cell") (r "^1.7") (f (quote ("std"))) (k 0)) (d (n "quick-xml") (r "^0.22") (k 0)) (d (n "strum") (r "^0.20") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "0c94p0px9znj3sk3bsxl8ms5i8a3pl1cw21qmap0hdyzqqlb2y5k")))

(define-public crate-burpsuite-kit-0.2.0 (c (n "burpsuite-kit") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "http") (r "^0.2") (k 0)) (d (n "once_cell") (r "^1.8") (f (quote ("std"))) (k 0)) (d (n "quick-xml") (r "^0.22") (k 0)) (d (n "strum") (r "^0.23") (f (quote ("std" "derive"))) (k 0)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "15mxc302c79sbyi66hnb04h7xk2xa450qm0pwrv28mrnhcyy67jh")))

(define-public crate-burpsuite-kit-0.3.0 (c (n "burpsuite-kit") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "http") (r "^0.2") (k 0)) (d (n "once_cell") (r "^1") (f (quote ("std"))) (k 0)) (d (n "quick-xml") (r "^0.23") (k 0)) (d (n "strum") (r "^0.24") (f (quote ("std" "derive"))) (k 0)) (d (n "thiserror") (r "^1") (k 0)))) (h "03n26nwqq0ypxzsqqdbxws4zca3jaiwdq8ijkx7w9cmvcgrlvxal")))

(define-public crate-burpsuite-kit-0.4.0 (c (n "burpsuite-kit") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "http") (r "^0.2") (k 0)) (d (n "once_cell") (r "^1") (f (quote ("std"))) (k 0)) (d (n "quick-xml") (r "^0.27") (k 0)) (d (n "strum") (r "^0.24") (f (quote ("std" "derive"))) (k 0)) (d (n "thiserror") (r "^1") (k 0)))) (h "0k3w9wcsk08pfj9wq6hjjysbkj56z1wvqc7ywhirvyhciwzk0fjz")))

