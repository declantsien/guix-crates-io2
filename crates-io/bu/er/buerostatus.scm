(define-module (crates-io bu er buerostatus) #:use-module (crates-io))

(define-public crate-buerostatus-0.1.0 (c (n "buerostatus") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9.6") (d #t) (k 0)))) (h "1s0g20h5sz5161dkzzwj06v2l4pj94fj0izfydj1iazcvkk3m57i")))

(define-public crate-buerostatus-0.1.1 (c (n "buerostatus") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.3.0") (d #t) (k 0)))) (h "0qrywaaf4bnzz7j7g4v7fpqrdn934zvrdwsvkcizjcn2aygsa792")))

