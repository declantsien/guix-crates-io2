(define-module (crates-io bu f- buf-min) #:use-module (crates-io))

(define-public crate-buf-min-0.0.1 (c (n "buf-min") (v "0.0.1") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0an3ndwpgldnps42y6c19h7czqkkmn5h4q11zq46vc80wk3vmwv0") (f (quote (("bytes-buf" "bytes"))))))

(define-public crate-buf-min-0.0.2 (c (n "buf-min") (v "0.0.2") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1ifcz0g0czzj9xybkkjb189qqbj5csshfzigbg61lxagzhp30fp5") (f (quote (("bytes-buf" "bytes"))))))

(define-public crate-buf-min-0.1.0 (c (n "buf-min") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0zw9qay0drvx9gr5lb8pr9h24b7nspfdb1rpf4khvgxm4f6bajjc") (f (quote (("bytes-buf" "bytes"))))))

(define-public crate-buf-min-0.1.1 (c (n "buf-min") (v "0.1.1") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0)))) (h "194wvzn0np8y5v34q6iksa5p4l06y1qsc8kazvg7qz6hm9lp1bmn") (f (quote (("bytes-buf" "bytes"))))))

(define-public crate-buf-min-0.1.2 (c (n "buf-min") (v "0.1.2") (d (list (d (n "bytes") (r "^0.6") (o #t) (d #t) (k 0)))) (h "17g5hmyj0yjz3hgrbd8l1xzdwmd49cggqy7lirg3ng8z50r9c2qj") (f (quote (("bytes-buf" "bytes")))) (y #t)))

(define-public crate-buf-min-0.2.0 (c (n "buf-min") (v "0.2.0") (d (list (d (n "bytes") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0ja3xbyv9267k3xh5qaajzf1737bj3bjpbn9qvbl3yyhc57707l8") (f (quote (("bytes-buf" "bytes"))))))

(define-public crate-buf-min-0.3.0 (c (n "buf-min") (v "0.3.0") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0) (p "bytes")) (d (n "bytes-tokio3") (r "^0.6") (o #t) (d #t) (k 0) (p "bytes")))) (h "09fp5ac8b2zsavn23pj8rlw98xgsny43n8hkjjjqsn8xxv8f9zn1") (f (quote (("bytes-buf-tokio3" "bytes-tokio3") ("bytes-buf" "bytes"))))))

(define-public crate-buf-min-0.3.1 (c (n "buf-min") (v "0.3.1") (d (list (d (n "bytes") (r "^0.5") (o #t) (d #t) (k 0) (p "bytes")) (d (n "bytes-tokio3") (r "^0.6") (o #t) (d #t) (k 0) (p "bytes")))) (h "09gdcsdmyyd8j8349m32bq0hm8c2faprr655cimvd70xaq8nypax") (f (quote (("bytes-buf-tokio3" "bytes-tokio3") ("bytes-buf" "bytes"))))))

(define-public crate-buf-min-0.4.0 (c (n "buf-min") (v "0.4.0") (d (list (d (n "bytes-tokio2") (r "^0.5") (o #t) (d #t) (k 0) (p "bytes")) (d (n "bytes-tokio3") (r "^0.6") (o #t) (d #t) (k 0) (p "bytes")))) (h "10za6b9lkd9h0wz8qpghyckd66c0wl07sxhq0nrnppbbylfal5zs") (f (quote (("bytes-buf-tokio3" "bytes-tokio3") ("bytes-buf-tokio2" "bytes-tokio2"))))))

(define-public crate-buf-min-0.5.0 (c (n "buf-min") (v "0.5.0") (d (list (d (n "bytes-tokio2") (r "^0.5") (o #t) (d #t) (k 0) (p "bytes")) (d (n "bytes-tokio3") (r "^1.0") (o #t) (d #t) (k 0) (p "bytes")))) (h "0rg54sc4z6p3kcbb208v8xc0vrhdwh2w8558qc43crh8z7a0qaij") (f (quote (("bytes-buf-tokio3" "bytes-tokio3") ("bytes-buf-tokio2" "bytes-tokio2"))))))

(define-public crate-buf-min-0.6.0 (c (n "buf-min") (v "0.6.0") (d (list (d (n "bytes-tokio2") (r "^0.5") (o #t) (d #t) (k 0) (p "bytes")) (d (n "bytes-tokio3") (r "^1.0") (o #t) (d #t) (k 0) (p "bytes")))) (h "111rrxgsqnqnl2gcp2x8amwj76kzb2sxh6rl1q62a6xj934gyk5n") (f (quote (("bytes-buf-tokio3" "bytes-tokio3") ("bytes-buf-tokio2" "bytes-tokio2"))))))

(define-public crate-buf-min-0.6.1 (c (n "buf-min") (v "0.6.1") (d (list (d (n "bytes-tokio2") (r "^0.5") (o #t) (d #t) (k 0) (p "bytes")) (d (n "bytes-tokio3") (r "^1.0") (o #t) (d #t) (k 0) (p "bytes")) (d (n "ntex-bytes") (r "^0.1") (o #t) (d #t) (k 0)))) (h "13cfqwmajwdy41p6plldz6x9fizxxys6ppxgsbh99yz2ky51qlzl") (f (quote (("ntex" "ntex-bytes") ("bytes-buf-tokio3" "bytes-tokio3") ("bytes-buf-tokio2" "bytes-tokio2"))))))

(define-public crate-buf-min-0.7.0 (c (n "buf-min") (v "0.7.0") (d (list (d (n "bytes") (r "^1.2") (o #t) (d #t) (k 0) (p "bytes")) (d (n "ntex-bytes") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1hyr2yicp4xlpada6ndwdr3nmh0dykhqg9zqp9hg99b8bazxd8rl")))

(define-public crate-buf-min-0.7.1 (c (n "buf-min") (v "0.7.1") (d (list (d (n "bytes") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "ntex-bytes") (r "^0.1") (o #t) (d #t) (k 0)))) (h "146wb3p4n62jbka6ym7sagziydcbixwhamw0cknl49w4ys66km92")))

