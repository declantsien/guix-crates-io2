(define-module (crates-io bu f- buf-rand) #:use-module (crates-io))

(define-public crate-buf-rand-0.1.0 (c (n "buf-rand") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0yp2sxjw7rf3ssmvb5188zzcwcyp2m4dbxds6plmsk9ckv8c3dcd")))

(define-public crate-buf-rand-0.1.1 (c (n "buf-rand") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "042kvyn35afkywcrk8ms0siyyha8197fiz48qid8hgvs9n2bn2pr")))

(define-public crate-buf-rand-0.1.2 (c (n "buf-rand") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1ipj9svh7bzrc71sc2bm96sz6v5zs6p4vllj9qrf6vyy23lygkar")))

(define-public crate-buf-rand-0.1.3 (c (n "buf-rand") (v "0.1.3") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0laxb30dcbclzbn78iwkr5yvmj2xjbas1mr6dfqk45b7nnn499ca")))

