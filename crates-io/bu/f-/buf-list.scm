(define-module (crates-io bu f- buf-list) #:use-module (crates-io))

(define-public crate-buf-list-0.1.0 (c (n "buf-list") (v "0.1.0") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("io-std" "io-util" "macros" "rt"))) (d #t) (k 2)))) (h "1jxps0v3mjh9gxnhm5flhp0a52ar1lqwsf3nddpda2wmy9k95mbp") (r "1.39")))

(define-public crate-buf-list-0.1.1 (c (n "buf-list") (v "0.1.1") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("io-std" "io-util" "macros" "rt"))) (d #t) (k 2)))) (h "1kwfhavmarws3y06njgs57iwiqvjg07g546s7ywa244v424s6al4") (r "1.39")))

(define-public crate-buf-list-0.1.2 (c (n "buf-list") (v "0.1.2") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("io-std" "io-util" "macros" "rt"))) (d #t) (k 2)))) (h "0af54sdj4wvqwsf47h0zi68qgqhm1fj1lqbc1d8akiq6c73n7vj9") (r "1.39")))

(define-public crate-buf-list-0.1.3 (c (n "buf-list") (v "0.1.3") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("io-std" "io-util" "macros" "rt"))) (d #t) (k 2)))) (h "10sj4qfm5hccdglrmq0a1f0q2cdr1czs7n188kkk6xx8dami7cny") (r "1.39")))

(define-public crate-buf-list-1.0.0 (c (n "buf-list") (v "1.0.0") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 2)) (d (n "tokio") (r "^1.0.0") (f (quote ("io-std" "io-util" "macros" "rt"))) (d #t) (k 2)))) (h "0byhvhgv2s8f8x4b0qwkjhrflnfnlhfzgvvmsqkvzw41qsqayla8") (r "1.39")))

(define-public crate-buf-list-1.0.1 (c (n "buf-list") (v "1.0.1") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (d #t) (k 2)) (d (n "tokio") (r "^1.0.0") (f (quote ("io-std" "io-util" "macros" "rt"))) (d #t) (k 2)))) (h "0j1c9sxmh2d4dvdi0zbvrchh1ggv9g5z36l5aqrl0iyv4xdy8qpl") (r "1.39")))

(define-public crate-buf-list-1.0.2 (c (n "buf-list") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 2)) (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "dummy-waker") (r "^1.1.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.25") (d #t) (k 2)) (d (n "futures-io-03") (r "^0.3.25") (o #t) (d #t) (k 0) (p "futures-io")) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "test-strategy") (r "^0.3.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0.0") (f (quote ("io-std"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("io-std" "io-util" "macros" "rt"))) (d #t) (k 2)))) (h "053ym9pkip1qsyzfzy7af65kadg351xbydq5s8aayv67hi9bhm1z") (f (quote (("tokio1" "tokio") ("futures03" "futures-io-03")))) (r "1.39")))

(define-public crate-buf-list-1.0.3 (c (n "buf-list") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 2)) (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "dummy-waker") (r "^1.1.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.25") (d #t) (k 2)) (d (n "futures-io-03") (r "^0.3.25") (o #t) (d #t) (k 0) (p "futures-io")) (d (n "proptest") (r "^1.1.0") (d #t) (k 2)) (d (n "test-strategy") (r "^0.3.0") (d #t) (k 2)) (d (n "tokio") (r "^1.0.0") (f (quote ("io-std"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("io-std" "io-util" "macros" "rt"))) (d #t) (k 2)))) (h "0flwfh22y6jx9p5lipyl185m1jz8rqgfn0nq7qm3wjlrbmld2szm") (f (quote (("tokio1" "tokio") ("futures03" "futures-io-03")))) (r "1.39")))

