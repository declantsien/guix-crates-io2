(define-module (crates-io bu f- buf-read-ext) #:use-module (crates-io))

(define-public crate-buf-read-ext-0.1.0 (c (n "buf-read-ext") (v "0.1.0") (h "0cvzcjc7ksd7nni347cf4zhxfcj2gp1w3yrf50yydns78qv9q2bj")))

(define-public crate-buf-read-ext-0.2.0 (c (n "buf-read-ext") (v "0.2.0") (h "0b6y0jdz3dqz0gb8s00xaqfd35s803i36z1j987vaby3z9r40f6c")))

(define-public crate-buf-read-ext-0.2.1 (c (n "buf-read-ext") (v "0.2.1") (h "062wzyr7bfrlyzvad545ygbhnmg519frgvy4sr0cvaj5xbgf33cx")))

(define-public crate-buf-read-ext-0.3.0 (c (n "buf-read-ext") (v "0.3.0") (d (list (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1aqrlk1abbjaxxz0mdqm9qzqym8dlklwr8r55ba643vsdkpknaxx") (f (quote (("default" "async") ("async" "futures" "tokio-io"))))))

(define-public crate-buf-read-ext-0.4.0 (c (n "buf-read-ext") (v "0.4.0") (h "0g57zyil58pdsfsbm9vf576abfzr0mp99b6g9vg69g2v9v272b1f")))

