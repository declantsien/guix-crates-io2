(define-module (crates-io bu f- buf-ref-reader) #:use-module (crates-io))

(define-public crate-buf-ref-reader-0.2.0 (c (n "buf-ref-reader") (v "0.2.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 2)) (d (n "memchr") (r "^2") (f (quote ("libc"))) (k 0)))) (h "0wgfrr3dvrjx8pwkwsf9avcb9qaj3j7y2c7m7fxfl4f3jdhx0sp0")))

(define-public crate-buf-ref-reader-0.3.0 (c (n "buf-ref-reader") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "copy_in_place") (r "^0.2") (d #t) (k 0)) (d (n "fnv") (r "^1") (d #t) (k 2)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "memchr") (r "^2") (f (quote ("libc"))) (k 2)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "slice-deque") (r "^0.1") (d #t) (k 0)))) (h "1fsrj8r7nfdwilv5b09d8pn3bgacls8628zf45k7h1fnnqcyngzn")))

