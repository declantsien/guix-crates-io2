(define-module (crates-io bu f- buf-trait) #:use-module (crates-io))

(define-public crate-buf-trait-0.4.0 (c (n "buf-trait") (v "0.4.0") (h "1l8pmxr532wv9vp0pxnrdlpr975xmnlvx5gj1z4azvzdbz796px8")))

(define-public crate-buf-trait-0.4.1 (c (n "buf-trait") (v "0.4.1") (d (list (d (n "zerocopy") (r "^0.7") (d #t) (k 0)))) (h "1d0pxqvynln4p58n7ajl95fk0x772xvgxjpsqgb77h78f33szsi1")))

