(define-module (crates-io bu zz buzzec-strip-dag-node) #:use-module (crates-io))

(define-public crate-buzzec-strip-dag-node-1.0.0 (c (n "buzzec-strip-dag-node") (v "1.0.0") (d (list (d (n "assert_cmd") (r "^1.0.4") (d #t) (k 2)) (d (n "bimap") (r "^0.6.1") (d #t) (k 0)) (d (n "predicates") (r "^1.0.8") (d #t) (k 2)) (d (n "slotmap") (r "^1.0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0fg2wwjbvblw820fhcdppm93qlxfl18z6zyhxxa00pznfnpm7463")))

