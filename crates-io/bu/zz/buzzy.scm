(define-module (crates-io bu zz buzzy) #:use-module (crates-io))

(define-public crate-buzzy-0.1.0 (c (n "buzzy") (v "0.1.0") (h "1b93gb9w94jl0m9w9ci8804drc0ja3q5j74nvpblbqzkrxjhbcyj")))

(define-public crate-buzzy-0.1.1 (c (n "buzzy") (v "0.1.1") (h "19ck071fpqkw3nfh2icbq2npjn9m5npb35dk4wb0nlsmvfrzra5x")))

(define-public crate-buzzy-0.1.2 (c (n "buzzy") (v "0.1.2") (h "0jsa06xry12bkf9dvmhnn0w97in96sh6bljqknvz32yscw740q8x")))

(define-public crate-buzzy-0.2.0 (c (n "buzzy") (v "0.2.0") (d (list (d (n "bevy") (r "^0.5") (f (quote ("dynamic"))) (d #t) (k 0)) (d (n "bevy_egui") (r "^0.4") (d #t) (k 0)))) (h "00figr6vyfx9phjy0lk6z9v72p2zvysa19hvl6v606m4635rk370")))

(define-public crate-buzzy-0.2.1 (c (n "buzzy") (v "0.2.1") (d (list (d (n "bevy") (r "^0.5") (d #t) (k 0)) (d (n "bevy_egui") (r "^0.4") (d #t) (k 0)))) (h "03251n8pvpwv16577nja45p8vm4rscznir7hp1dv55191z8v6b3j")))

