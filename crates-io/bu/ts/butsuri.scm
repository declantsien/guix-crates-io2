(define-module (crates-io bu ts butsuri) #:use-module (crates-io))

(define-public crate-butsuri-0.1.0 (c (n "butsuri") (v "0.1.0") (d (list (d (n "bevy") (r "^0.7.0") (d #t) (k 0)) (d (n "bevy_prototype_lyon") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1fj58v2a05i60jx74x8bbry973z3rfa73abk6ln464j1x15i6wa3")))

