(define-module (crates-io bu ld buldak) #:use-module (crates-io))

(define-public crate-buldak-0.0.0 (c (n "buldak") (v "0.0.0") (h "096cwmfk9r8jn8myvyhrshfzq6sq6g3rx0118jg1m31fv8y4xgwy")))

(define-public crate-buldak-0.0.1 (c (n "buldak") (v "0.0.1") (h "00p8sj97sa33rrdcm6nd3mlrd9nw7aajy078njqb4911i2s5n17m")))

(define-public crate-buldak-0.0.2 (c (n "buldak") (v "0.0.2") (h "1vl4v1kl8w0gsm9x6mjh8npingai0glp376a5nmhv1jrk06drr7g")))

(define-public crate-buldak-0.0.3 (c (n "buldak") (v "0.0.3") (h "0hp3hk4c6y1dg3f1yhjjh77bj72grnzgq1djahk047ic9j2y7ivh")))

(define-public crate-buldak-0.0.4 (c (n "buldak") (v "0.0.4") (h "0674zi0aj4ic0f6zkdpaazd4yxyvyjvsmxjnzdkglgbd4fqb2755")))

(define-public crate-buldak-0.0.5 (c (n "buldak") (v "0.0.5") (h "0ry6f2aqx6bbbxs43q9agrag4n274zc6zpx83k1yvm7f2y23a35x")))

(define-public crate-buldak-0.1.0 (c (n "buldak") (v "0.1.0") (h "1x4468w22sw8h6in704dx2anv5i73jdv7s4mv9c3hcy0k0sj8v5q")))

(define-public crate-buldak-0.1.1 (c (n "buldak") (v "0.1.1") (h "1r64zx31li3n98w9z1d0irx3zrdk06m60rh8v18fnbq4c2waj4il")))

(define-public crate-buldak-0.2.0 (c (n "buldak") (v "0.2.0") (h "1dnw3xmzjzksadv43a84y6q7wyzznpnz8fnw88009bpnrpah3ihk")))

(define-public crate-buldak-0.3.0 (c (n "buldak") (v "0.3.0") (h "0nd3d7qa44lys9ywlm504dkw0vckynq1wak0cwnn3q4x3rsx4804")))

(define-public crate-buldak-0.4.0 (c (n "buldak") (v "0.4.0") (h "0gzcaz346qw4nkha50plpykgcbdwn7cbhyrblhdsvx283lsjmqcn")))

(define-public crate-buldak-0.5.0 (c (n "buldak") (v "0.5.0") (h "1ghq671a3nk6jwv935c7cyx977lasyshzb2kk2z5ph85imd4vzk8")))

(define-public crate-buldak-0.6.0 (c (n "buldak") (v "0.6.0") (h "0xjzbmwdqp9lpj9pkfil1sc29na0b3m1qss8g6wjl356ahchky7r")))

(define-public crate-buldak-0.6.1 (c (n "buldak") (v "0.6.1") (h "0ngg4psfix3x84p5rhi8gkgci6g332qyzc4nsmc33qpms31k6vkx")))

(define-public crate-buldak-0.7.0 (c (n "buldak") (v "0.7.0") (h "1r0w5gjbwsbcr22av37yzqybb8adx2mgy1j9vxgdf122sb98cx6q")))

(define-public crate-buldak-0.8.0 (c (n "buldak") (v "0.8.0") (h "0j18jj30yyvw2m4nbqgiyhkpm9ny6j9gibl5pyljynia6dmdz2a7")))

(define-public crate-buldak-0.9.0 (c (n "buldak") (v "0.9.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1dryfh5bvkx75195vdp2a8cjfhgprpaxld67kc9cdgs01g53ia0k")))

(define-public crate-buldak-0.9.1 (c (n "buldak") (v "0.9.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0cykyn3sni2bsnh84mh20dd7b5anh0651vddzadgnklxal60fi4x")))

(define-public crate-buldak-0.10.0 (c (n "buldak") (v "0.10.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0sr0h0j0d6dgfqa6i71n956p0sq121iwwvlm2zkbj8lfwrw19ndc")))

(define-public crate-buldak-0.11.0 (c (n "buldak") (v "0.11.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "164lpa5qr83pfn1h8wx7vk6d0341imkyyhw7vl4nb7n8i8wn5s2m")))

(define-public crate-buldak-0.11.1 (c (n "buldak") (v "0.11.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "19v4hffvwil5npnbsizp4a7r5inmbfpn6sdf6fbfallsmg8vlphk")))

(define-public crate-buldak-0.11.2 (c (n "buldak") (v "0.11.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "19z8k5n3mvdypad6g5g18ii9fhlp080pp4s5xsl9mmbfxqlkai6a")))

(define-public crate-buldak-0.12.0 (c (n "buldak") (v "0.12.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "03kgdzbjdqgkm8syb96cp1l5bn01q52wmf63yr2bk8majamhj5f2")))

(define-public crate-buldak-0.12.1 (c (n "buldak") (v "0.12.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "17j2zsv0gmwk9hbqacfs35im7hys76xf8pcmy8lwd9rwj5xxjyiy")))

(define-public crate-buldak-0.12.2 (c (n "buldak") (v "0.12.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1y0dd1h38cirvmg7yvddm1ah4flhzx65ckykqszsqam1y9h8m46y")))

(define-public crate-buldak-0.13.0 (c (n "buldak") (v "0.13.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "19jjyfmnzr9zip8vgddcc4in7lw3pzaiksr2hc8yyaf0ahf16b39")))

(define-public crate-buldak-0.14.0 (c (n "buldak") (v "0.14.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0sw6yis32glsskayphsj4s23mhhsrspbmxng6185flz3y4xq0619")))

(define-public crate-buldak-0.14.1 (c (n "buldak") (v "0.14.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "12n6q3rc7c43b72z9rhyf5gyljqhhag2k0i6c7182zjf693wf3xa")))

(define-public crate-buldak-0.15.0 (c (n "buldak") (v "0.15.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "022l3caifac50dpqp4lyy65kllgipsn4hld7knw81yblg9aa37b2")))

(define-public crate-buldak-0.16.0 (c (n "buldak") (v "0.16.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0vn0rs4c5b4jl78xrscv66yw56zxx7jflli0kq72c307r3y81c0c")))

(define-public crate-buldak-0.17.0 (c (n "buldak") (v "0.17.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "16q2mr25yj8vnma9dsgvj4fs0bgxqf3gck44n2fxdqb9qg9nrf1a")))

(define-public crate-buldak-0.18.0 (c (n "buldak") (v "0.18.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1ziq2zg28bdc58mp7bsardlyki09xfxvgz75mq5qcc5i5s7j6jcj")))

(define-public crate-buldak-0.19.0 (c (n "buldak") (v "0.19.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "18ynq6ph7hzgrm60sg9xyfbwicisj6dg20hwfywj6jhs578azyvb")))

(define-public crate-buldak-0.20.0 (c (n "buldak") (v "0.20.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0r05b5iqh4lgmh7q25yrqdx06qfbmsjppavdnciai3ix097dqar5")))

(define-public crate-buldak-0.20.1 (c (n "buldak") (v "0.20.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0s8ynria5prybrs1l8866cjgj28ry6bjzyxsfqv7dpgn0gza3z52")))

(define-public crate-buldak-0.21.0 (c (n "buldak") (v "0.21.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0wl9hjv0pk0jf4pdmmakpn8n6snx9ywq68bvqpz1ppkb268ca1rw")))

(define-public crate-buldak-0.22.0 (c (n "buldak") (v "0.22.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0nj2ry46r4jq16pkiyg8q2fx4ycj2x7xcslw751n7w19k9a41m4g")))

(define-public crate-buldak-0.23.0 (c (n "buldak") (v "0.23.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0qpc6s6k07lmvxz440bakg4ky1bmabqbdsa2pnyan0bh90amirgg")))

(define-public crate-buldak-0.23.1 (c (n "buldak") (v "0.23.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0xk9a7hjnf58yfm43zxxb343vwp8v7aijvziahm9v9fniy2dvmi0")))

(define-public crate-buldak-0.24.0 (c (n "buldak") (v "0.24.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0pycminj3kvpbrbg9xrk7lldkqgms0asqsz015wpnl92l3sch66v")))

(define-public crate-buldak-0.25.0 (c (n "buldak") (v "0.25.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0vsaay482yw23bl2pglpvif934zwmz6ghkwxw59jjz9sma4w70qb")))

(define-public crate-buldak-0.26.0 (c (n "buldak") (v "0.26.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0k1hrgmwfzqrybabq05nhbp7h4c9197yz4pk1cs8ra0230k0x5jy")))

(define-public crate-buldak-0.27.0 (c (n "buldak") (v "0.27.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1c2xn0yqq62jq2bgm7rica8ndj5fa1g7p9a9sl5s247pwyjhzvb6")))

(define-public crate-buldak-0.27.1 (c (n "buldak") (v "0.27.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1p00i4p4ix812y810vng4i2a5g8a3v0j1j5pw9xljrpa4wkxq6g2")))

(define-public crate-buldak-0.28.0 (c (n "buldak") (v "0.28.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1nwygb4ihsh8p4yab7hrgfgdfrp7w7i59ypl8hfbazrmwxdmv19z")))

(define-public crate-buldak-0.28.1 (c (n "buldak") (v "0.28.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0wf58bl60q3s8sgzc0yln68g45hxfbrhc9y555xzhn0ff95c2s26")))

