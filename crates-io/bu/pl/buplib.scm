(define-module (crates-io bu pl buplib) #:use-module (crates-io))

(define-public crate-buplib-1.0.0 (c (n "buplib") (v "1.0.0") (d (list (d (n "rodio") (r "^0.17.3") (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "09j4lmcszvrsbkggq25i51406zb73bf5134z2in3idz441czd1dl")))

(define-public crate-buplib-2.0.0 (c (n "buplib") (v "2.0.0") (d (list (d (n "rodio") (r "^0.17.3") (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1ziqqgaiarjfcx9n2q9skln6qpyq8bbmmqp7qxam5ass47jiwsfr") (y #t)))

(define-public crate-buplib-2.0.1 (c (n "buplib") (v "2.0.1") (d (list (d (n "rodio") (r "^0.17.3") (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "08g4mz2baay2s2vazff8nxwv95mfvf10i00ackqzv3i1vd9nj3ra")))

(define-public crate-buplib-3.0.0 (c (n "buplib") (v "3.0.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 2)) (d (n "rodio") (r "^0.17.3") (k 0)))) (h "0q6rpvwn03j2fhivqdkbqil918lfgkfjrgvxnjkvjl635sq1k5l7")))

(define-public crate-buplib-3.1.0 (c (n "buplib") (v "3.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 2)) (d (n "rodio") (r "^0.17.3") (k 0)))) (h "1m9z04vqwngsrd33j58fb0fc28li9nb1x6kw67h7wlf1msiswis7") (f (quote (("mut"))))))

(define-public crate-buplib-3.2.0 (c (n "buplib") (v "3.2.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 2)) (d (n "rodio") (r "^0.17.3") (k 0)))) (h "0pxnc3ypamkypw4j8kcqq30cyrpbva52j7rdk6iap6m7kp7mbdcl") (f (quote (("mut") ("future" "mut"))))))

(define-public crate-buplib-3.2.1 (c (n "buplib") (v "3.2.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 2)) (d (n "rodio") (r "^0.17.3") (k 0)))) (h "0gb27mkh313h3ckgcq64zl9vfy8xp25gbfgc83b1cyddgbyqc09k") (f (quote (("mut") ("future" "mut"))))))

(define-public crate-buplib-3.3.0 (c (n "buplib") (v "3.3.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 2)) (d (n "rodio") (r "^0.17.3") (k 0)))) (h "1vljhrxx88ya5j74zld7ig233iwz7ilgbg0j96xddlm745cqmg4v") (f (quote (("mut") ("future" "mut"))))))

