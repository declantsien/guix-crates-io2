(define-module (crates-io bu rd burden) #:use-module (crates-io))

(define-public crate-burden-0.1.0 (c (n "burden") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)))) (h "08q5pbfs0wxaw6x1psvx561w38llgcc695f7dci5bcl7jvpvpvk2")))

(define-public crate-burden-0.1.1 (c (n "burden") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)))) (h "0s9minxlhl46lr350s7sl93hx3r92y3nhjym7a55sj28cbnf46mk")))

(define-public crate-burden-0.2.0 (c (n "burden") (v "0.2.0") (d (list (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "12i0big8wxkzcsjsvf4a7ancchqcb4dvzdq7ynbrs6w804n9ac5l")))

(define-public crate-burden-0.2.1 (c (n "burden") (v "0.2.1") (d (list (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)))) (h "1c1gm3zl2ppaxdw44vbv8qr8ykngkd8l0b9i43nw0r1f65ngnk19")))

