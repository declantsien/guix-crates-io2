(define-module (crates-io bu gl bugle) #:use-module (crates-io))

(define-public crate-bugle-0.0.1 (c (n "bugle") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (f (quote ("compat" "thread-pool"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14hjqx7yrhphzkgd3gyikqlyfqcmh8m6yvghnkslfkcz378gw98g")))

