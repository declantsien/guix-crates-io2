(define-module (crates-io bu mb bumblebee) #:use-module (crates-io))

(define-public crate-bumblebee-0.1.0 (c (n "bumblebee") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "typetag") (r "^0.1.3") (d #t) (k 0)))) (h "0p27vh60ym4vv5asg7xkw315k018jd491c8ff6jn84hi791q3nn0")))

(define-public crate-bumblebee-0.1.1 (c (n "bumblebee") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "typetag") (r "^0.1.3") (d #t) (k 0)))) (h "0rabk3mfpzamzq6i7l5zifjlg4lqr2i59bc959yxlkzd6jkrk79c")))

