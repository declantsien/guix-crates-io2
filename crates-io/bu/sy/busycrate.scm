(define-module (crates-io bu sy busycrate) #:use-module (crates-io))

(define-public crate-busycrate-0.1.0 (c (n "busycrate") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)))) (h "06097ifzfknba4w481hcsavl8r1p5lq0ddy7h2fb2l4kfw0d1hqa")))

