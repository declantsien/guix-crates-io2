(define-module (crates-io bu il build_uuid) #:use-module (crates-io))

(define-public crate-build_uuid-0.3.0 (c (n "build_uuid") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "twox-hash") (r "^1.6.3") (k 0)) (d (n "uuid") (r "^1.2.1") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.33") (d #t) (k 2)))) (h "0wpisw94kgcc8gb1cmkbf8x2z7b494h7lr8zn43i74v4vi7s63sl") (l "build_uuid")))

