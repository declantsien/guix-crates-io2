(define-module (crates-io bu il build_cfg) #:use-module (crates-io))

(define-public crate-build_cfg-0.1.0 (c (n "build_cfg") (v "0.1.0") (d (list (d (n "build_cfg_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "build_cfg_shared") (r "^0.1.0") (d #t) (k 0)))) (h "06pyhh6pgvpxbg7v46kkdjfxkv542d9asn7shax04z1i0aalxk8j")))

(define-public crate-build_cfg-1.1.0 (c (n "build_cfg") (v "1.1.0") (d (list (d (n "build_cfg_macros") (r "^1.1.0") (d #t) (k 0)) (d (n "build_cfg_shared") (r "^1.1.0") (d #t) (k 0)))) (h "0bsizjxkqr3p2f3cli9pq2lbd5pxybwp4vinf1g4p4g3jva2jlzr")))

