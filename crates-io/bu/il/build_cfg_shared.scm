(define-module (crates-io bu il build_cfg_shared) #:use-module (crates-io))

(define-public crate-build_cfg_shared-0.1.0 (c (n "build_cfg_shared") (v "0.1.0") (d (list (d (n "derive_more") (r "^0") (d #t) (k 0)))) (h "1glwm7n8bzlxszasziknpf5s2jlqai5ik7zg8681fffknj9qsaa2")))

(define-public crate-build_cfg_shared-1.0.0 (c (n "build_cfg_shared") (v "1.0.0") (d (list (d (n "derive_more") (r "^0") (d #t) (k 0)))) (h "01pzsckh95mrdrgmhal01m3v01r1yc22cmwvk06iwqnfmwgskq76")))

(define-public crate-build_cfg_shared-1.1.0 (c (n "build_cfg_shared") (v "1.1.0") (d (list (d (n "derive_more") (r "^0") (d #t) (k 0)))) (h "0y2dzd6my6xfqw6axia5x85g52cqj4pc9ikkhd6dbxfrw9xdmxn9")))

