(define-module (crates-io bu il buildxargs) #:use-module (crates-io))

(define-public crate-buildxargs-1.2.0 (c (n "buildxargs") (v "1.2.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "assert_fs") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "predicates") (r "^3") (d #t) (k 2)) (d (n "shlex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1axyz8i28d2zivjzhk996xa156rlrg2m2fz1rvdhh3pfpll54139")))

