(define-module (crates-io bu il builder-rs) #:use-module (crates-io))

(define-public crate-builder-rs-0.1.0 (c (n "builder-rs") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.89") (d #t) (k 2)))) (h "1azxhwbd23c71lnmkfxbvakl3avaahklw9x4szql3f3nn263r4lm")))

