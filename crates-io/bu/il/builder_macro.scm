(define-module (crates-io bu il builder_macro) #:use-module (crates-io))

(define-public crate-builder_macro-0.1.0 (c (n "builder_macro") (v "0.1.0") (h "0q6bmc1lbafjzl1x5glb1qi5z9694l7i226hz8isp0pclnn95p2a")))

(define-public crate-builder_macro-0.1.1 (c (n "builder_macro") (v "0.1.1") (h "1csv36fv720yzi7vdm8qdnjzhin0jn9a1ac642gfjvwm24pifha2")))

(define-public crate-builder_macro-0.2.0 (c (n "builder_macro") (v "0.2.0") (h "0mgrky2fwws7yfg4a5q48k7jrf7fgy02v38l96nhvlx4254h5qyr")))

(define-public crate-builder_macro-0.3.0 (c (n "builder_macro") (v "0.3.0") (h "0bf7vfhm63dc7w4nhd06y9jwaygy0g687vlbifmm8zps11w3yicj")))

(define-public crate-builder_macro-0.4.0 (c (n "builder_macro") (v "0.4.0") (h "0ld66521ldnl1si22x50ws47y1bqckyj10lx44q4yrfp8pyxfjif")))

(define-public crate-builder_macro-0.5.0 (c (n "builder_macro") (v "0.5.0") (h "0pky1pvpcyy5y54vc4dhrla5b6lby7ysx86azm2nq7sxcz4ynci6")))

(define-public crate-builder_macro-0.5.1 (c (n "builder_macro") (v "0.5.1") (h "1iq4jrym7d880mkskxm8cj3vqkvzap5c9q5814bc8zs5jlznhq9l")))

