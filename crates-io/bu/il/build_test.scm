(define-module (crates-io bu il build_test) #:use-module (crates-io))

(define-public crate-build_test-0.1.0 (c (n "build_test") (v "0.1.0") (h "0bazgzy83bl7k5595i3nyzx1qj3c4ahsvnncd8xsk8ssdwjxl9m6") (y #t)))

(define-public crate-build_test-0.1.1 (c (n "build_test") (v "0.1.1") (h "1p5943kd06awj2p41hriafcwiz7k8wbf30yl80q9i15gmgr7grdy") (y #t)))

(define-public crate-build_test-0.1.2 (c (n "build_test") (v "0.1.2") (h "0fy47032fi6mdbxqz1ibhz7iigr2z4lq1q7y85j986jsna6aa1ar") (y #t)))

