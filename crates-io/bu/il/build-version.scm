(define-module (crates-io bu il build-version) #:use-module (crates-io))

(define-public crate-build-version-0.1.0 (c (n "build-version") (v "0.1.0") (d (list (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)))) (h "0h32198b1a4yyxqapbc7sn95x94qc0vvhfivq1n802arhlcm0yz0")))

(define-public crate-build-version-0.1.1 (c (n "build-version") (v "0.1.1") (d (list (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)))) (h "1a2bwih4la2v33dflrzdfqm7mrsvxhwzkpz6s6as17kakqinnjqw")))

