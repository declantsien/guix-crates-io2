(define-module (crates-io bu il build_stamp) #:use-module (crates-io))

(define-public crate-build_stamp-1.0.0 (c (n "build_stamp") (v "1.0.0") (h "19ns87lrza5l8c5f7179lq91qqpym8v068gxiyfqplrl98h41bvf")))

(define-public crate-build_stamp-1.0.2 (c (n "build_stamp") (v "1.0.2") (h "0b0vydybn5qq6skcffwwi02cpqmkjql1iamzhndrz71g7rvax532")))

