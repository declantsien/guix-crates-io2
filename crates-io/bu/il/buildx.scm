(define-module (crates-io bu il buildx) #:use-module (crates-io))

(define-public crate-buildx-0.1.0 (c (n "buildx") (v "0.1.0") (h "1fn29lm15wp652cdbgri9wyin2kx3dmsv37x24f322jjfzrbayrp")))

(define-public crate-buildx-0.1.1 (c (n "buildx") (v "0.1.1") (h "1x14k7l6h2dl56cihfbb626yna9vldnynf0m2v41rra6avhiza7i")))

