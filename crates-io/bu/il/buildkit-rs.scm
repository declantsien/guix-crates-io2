(define-module (crates-io bu il buildkit-rs) #:use-module (crates-io))

(define-public crate-buildkit-rs-0.1.0 (c (n "buildkit-rs") (v "0.1.0") (d (list (d (n "buildkit-rs-llb") (r "^0.1.0") (d #t) (k 0)) (d (n "buildkit-rs-proto") (r "^0.1.0") (d #t) (k 0)) (d (n "buildkit-rs-reference") (r "^0.1.0") (d #t) (k 0)) (d (n "buildkit-rs-util") (r "^0.1.0") (d #t) (k 0)))) (h "1jf81nizm06mik105cc45j5d441z2ghblp23drlljxzry027ch1y")))

