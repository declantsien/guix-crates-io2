(define-module (crates-io bu il buildrs_playground) #:use-module (crates-io))

(define-public crate-buildrs_playground-0.0.1 (c (n "buildrs_playground") (v "0.0.1") (d (list (d (n "autocfg") (r "^0.1") (d #t) (k 1)))) (h "1kmhcjs7m240igsx23mwbchvmpxd2k45l7600qdhf4vl52a8spic")))

(define-public crate-buildrs_playground-0.0.2 (c (n "buildrs_playground") (v "0.0.2") (d (list (d (n "autocfg") (r "^0.1") (d #t) (k 1)))) (h "14w5x6jnc63byjs696nv4ai4j90dzad0f4zbirnbbdzdhh1k69hr")))

