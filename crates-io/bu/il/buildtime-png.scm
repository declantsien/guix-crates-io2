(define-module (crates-io bu il buildtime-png) #:use-module (crates-io))

(define-public crate-buildtime-png-0.1.0 (c (n "buildtime-png") (v "0.1.0") (d (list (d (n "png") (r "^0.14.1") (k 0)))) (h "1d96may36cvd7d1j3k5xw0ikz138iyfqzap73cziaqlw58lazml0")))

(define-public crate-buildtime-png-0.1.1 (c (n "buildtime-png") (v "0.1.1") (d (list (d (n "png") (r "^0.14.1") (k 0)))) (h "00ns37z477h1bqr8farrfqx6fby050y742rmr345038majznqaah")))

(define-public crate-buildtime-png-0.1.2 (c (n "buildtime-png") (v "0.1.2") (d (list (d (n "png") (r "^0.14.1") (k 0)))) (h "1ka47jgpr84a1n072vr1yik9rskznlw63ksdpz4p52n30xw3yhwd")))

(define-public crate-buildtime-png-0.1.3 (c (n "buildtime-png") (v "0.1.3") (d (list (d (n "png") (r "^0.14.1") (k 0)))) (h "09whg7z212fch10ybv0b4ad8yk2w0cmhcwla51xa0yrklnmsqhjc")))

(define-public crate-buildtime-png-0.1.4 (c (n "buildtime-png") (v "0.1.4") (d (list (d (n "png") (r "^0.14.1") (k 0)))) (h "1sv98lkmbx60yffzshlmmv62041dj2qf5y6n3xv34c9as3czv3pd")))

