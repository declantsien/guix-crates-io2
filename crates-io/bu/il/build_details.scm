(define-module (crates-io bu il build_details) #:use-module (crates-io))

(define-public crate-build_details-0.1.0 (c (n "build_details") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.22") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "01n9lgbihwsd428b8bd3r6w2p6gwiqsnz3vf75gffhhfdhikg7ax")))

(define-public crate-build_details-0.1.1 (c (n "build_details") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.22") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "0ilyl4xf9gppc7fkr4xy9y4l3blhmqqmyxf73x5fgz482akzv2bj")))

(define-public crate-build_details-0.1.2 (c (n "build_details") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.22") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 2)))) (h "0k3vx77bk9qg7dy40barrmfb5bk2d7dv68ask3xf60fvllimzha7")))

