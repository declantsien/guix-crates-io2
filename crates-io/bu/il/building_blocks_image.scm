(define-module (crates-io bu il building_blocks_image) #:use-module (crates-io))

(define-public crate-building_blocks_image-0.1.0 (c (n "building_blocks_image") (v "0.1.0") (d (list (d (n "building_blocks_core") (r "^0.1") (d #t) (k 0)) (d (n "building_blocks_storage") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)))) (h "0g6wph5vys2i2v7za1056qrxl49zxsvdx8hgziqbsg9r7xgbkvlv")))

(define-public crate-building_blocks_image-0.2.0 (c (n "building_blocks_image") (v "0.2.0") (d (list (d (n "building_blocks_core") (r "^0.2") (d #t) (k 0)) (d (n "building_blocks_storage") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)))) (h "158x9gpfq9kcmf6n34h250609w041x37mwmgmh6a6x1pnab5xhj4")))

(define-public crate-building_blocks_image-0.3.0 (c (n "building_blocks_image") (v "0.3.0") (d (list (d (n "building_blocks_core") (r "^0.3") (d #t) (k 0)) (d (n "building_blocks_storage") (r "^0.3") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)))) (h "125x72c7ycs1vdgx49h3v2q1g7acglsyhri9m7kp1cg2yi1xy38h")))

(define-public crate-building_blocks_image-0.4.0 (c (n "building_blocks_image") (v "0.4.0") (d (list (d (n "building_blocks_core") (r "^0.4") (d #t) (k 0)) (d (n "building_blocks_storage") (r "^0.4") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)))) (h "1cylb94q6pns3qi92n7ih640jcn1pbv8cyyvjym581mqlp3kccs2")))

(define-public crate-building_blocks_image-0.4.3 (c (n "building_blocks_image") (v "0.4.3") (d (list (d (n "building_blocks_core") (r "^0.4.3") (k 0)) (d (n "building_blocks_storage") (r "^0.4.3") (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)))) (h "08cj349szm10df6x19arw0kvzqck3glfpcwm206h8541jyxiydbg")))

