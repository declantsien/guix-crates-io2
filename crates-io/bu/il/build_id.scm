(define-module (crates-io bu il build_id) #:use-module (crates-io))

(define-public crate-build_id-0.1.0 (c (n "build_id") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "twox-hash") (r "^1.1") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("const_fn"))) (d #t) (k 0)))) (h "1f1lap0mka5s5gjsjgd11fc2xq0il7syr0s20a7n0cf3brrhy71s")))

(define-public crate-build_id-0.1.1 (c (n "build_id") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "proc_self") (r "^0.1") (d #t) (k 0)) (d (n "twox-hash") (r "^1.1") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("const_fn"))) (d #t) (k 0)))) (h "003ilv7hk0biaabpx8x4pcc1kv76hzr3yjybjncksn9230dq73s9")))

(define-public crate-build_id-0.1.2 (c (n "build_id") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "proc_self") (r "^0.1") (d #t) (k 0)) (d (n "twox-hash") (r "^1.1") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("const_fn"))) (d #t) (k 0)))) (h "1grqi142x544pyla6nj063v6bhx1y7whik461q38f10ivp871srl")))

(define-public crate-build_id-0.1.3 (c (n "build_id") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "palaver") (r "^0.2") (d #t) (k 0)) (d (n "twox-hash") (r "^1.1") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("const_fn"))) (d #t) (k 0)))) (h "12l1wybvc2k7cj9544l5zbl4mqv7c6y3v9hcgvvnmkaqxyc68f1j")))

(define-public crate-build_id-0.1.4 (c (n "build_id") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "palaver") (r "^0.2") (d #t) (k 0)) (d (n "twox-hash") (r "^1.1") (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("const_fn"))) (d #t) (k 0)))) (h "1r6jv47l9ygg36ykk0d4hxi6pcvlwcwvcmfh727m0q9ymjikd27k")))

(define-public crate-build_id-0.1.5 (c (n "build_id") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "palaver") (r "^0.2") (d #t) (k 0)) (d (n "twox-hash") (r "^1.1") (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("const_fn"))) (d #t) (k 0)))) (h "02hxp128f2251myd0h1qysjby2kqxnhdmpmfrva4b4ssgxycksxx")))

(define-public crate-build_id-0.2.0 (c (n "build_id") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.2") (d #t) (k 0)) (d (n "palaver") (r "^0.2") (d #t) (k 0)) (d (n "twox-hash") (r "^1.1") (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "04j0ijbvdbjhxnlm08ig2yjh5aj2xr4jizhwylaqq8j2mk3mfici")))

(define-public crate-build_id-0.2.1 (c (n "build_id") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.2") (d #t) (k 0)) (d (n "palaver") (r "^0.2") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "twox-hash") (r "^1.1") (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "1wysfxq01s1yc039h9cym52wvx7rpy3pmy7wqdlj4kcbbmwvdpn6") (l "build_id")))

