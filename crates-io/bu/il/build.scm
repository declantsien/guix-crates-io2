(define-module (crates-io bu il build) #:use-module (crates-io))

(define-public crate-build-0.0.1 (c (n "build") (v "0.0.1") (d (list (d (n "rustc-serialize") (r "^0.3.21") (d #t) (k 0)))) (h "11p71zvjqw62l166vfzj69qc61lpjv3q4b7j0b6ndqm2qn1cy3pf")))

(define-public crate-build-0.0.2 (c (n "build") (v "0.0.2") (d (list (d (n "rustc-serialize") (r "^0.3.21") (d #t) (k 0)))) (h "181jr1g861byn76if631ns59ibf7siz833p0yabp596nwvfgvazr")))

