(define-module (crates-io bu il buildless) #:use-module (crates-io))

(define-public crate-buildless-0.0.1 (c (n "buildless") (v "0.0.1") (h "1gwdklssk3jscy7hnfn7sdwqqmi5bmdjrs6wx26il0dswzkf949g") (r "1.56")))

(define-public crate-buildless-0.0.1-alpha0 (c (n "buildless") (v "0.0.1-alpha0") (h "1cy0aj5fa6dr0jv5fcq1xbm6rxa6nc36p85lb7bq7dl419ajv6hd") (r "1.56")))

