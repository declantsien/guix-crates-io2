(define-module (crates-io bu il build-target) #:use-module (crates-io))

(define-public crate-build-target-0.1.0 (c (n "build-target") (v "0.1.0") (h "121irh2ja66qhw34hx2g463rxjzc5vcqq58nvlwzx660dd8sjzdx")))

(define-public crate-build-target-0.1.1 (c (n "build-target") (v "0.1.1") (h "1r6crk1ywzq6zq3fwdc8kznaziy5j1vfpnqjyy6sli7q7vlpz2md")))

(define-public crate-build-target-0.2.0 (c (n "build-target") (v "0.2.0") (h "0dqipsqdsq9vsjgkgk6jgbyxrd1vifkyg2ld31kbjnp28f65v0l3")))

(define-public crate-build-target-0.2.1 (c (n "build-target") (v "0.2.1") (h "02r2gvci3xam5ymn2y3p6lwc32qx7nryp7z1yblcvrnpflvzw7ir")))

(define-public crate-build-target-0.3.0 (c (n "build-target") (v "0.3.0") (h "155wb3lacaacj9p8i75wyacfbp1xbrajrah1sq5k25fqsd527142")))

(define-public crate-build-target-0.3.1 (c (n "build-target") (v "0.3.1") (h "19ai76yhqavk7haajcldkmvdq312yz0s2n9cb000r5y0ad7wlywx")))

(define-public crate-build-target-0.4.0 (c (n "build-target") (v "0.4.0") (h "0aw3jmw6srp7l4ph75pvp39af9vnhai5cd3rpayrzamvmfxk68c3")))

