(define-module (crates-io bu il builtin_interfaces) #:use-module (crates-io))

(define-public crate-builtin_interfaces-1.2.1 (c (n "builtin_interfaces") (v "1.2.1") (d (list (d (n "rosidl_runtime_rs") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "0hym320c7mk73ivkx4fnf1akq3xvxf2arkh9d2lp9z6s37ijc0n3") (y #t) (s 2) (e (quote (("serde" "dep:serde" "dep:serde-big-array" "rosidl_runtime_rs/serde"))))))

