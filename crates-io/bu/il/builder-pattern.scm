(define-module (crates-io bu il builder-pattern) #:use-module (crates-io))

(define-public crate-builder-pattern-0.1.0 (c (n "builder-pattern") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1yrxmpbpcqf9vni50vxd42xidiqpb832s6awm0nmyp1v4in4qzz2")))

(define-public crate-builder-pattern-0.1.1 (c (n "builder-pattern") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03xf1fqm7a1kf740mqpia8ghz7z6631gd8ks3r9i8w41lrwdplhx")))

(define-public crate-builder-pattern-0.1.2 (c (n "builder-pattern") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0sg47s3npmryabm4sphi6x9196jqcpx8jfnh1s10d6vfjmx6mrxh")))

(define-public crate-builder-pattern-0.1.3 (c (n "builder-pattern") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1f9si93z95vlgmkk5p47w4vj6wc7r5bsxlnlr2lawysf361a385h")))

(define-public crate-builder-pattern-0.2.0 (c (n "builder-pattern") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12xb0m2vnbkkymp4bx08h4ddwx92gizjdyg4f9j71i3fgjddvca4")))

(define-public crate-builder-pattern-0.2.1 (c (n "builder-pattern") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1s2s3rwfhdwp4v3rhkkan4nry494pf48fr41k8awak7nrkv1wh8h")))

(define-public crate-builder-pattern-0.2.2 (c (n "builder-pattern") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1n1cv0zgq8pn8dchgqp9q4fqlkhc3rqyi81zcbvgzp8kfzqf6rmx")))

(define-public crate-builder-pattern-0.3.0 (c (n "builder-pattern") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "15d7fhmfhffnskqgmivbf44nb7s853i7sq46hifcw9bs58yv1xh6")))

(define-public crate-builder-pattern-0.3.1 (c (n "builder-pattern") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08hav53yssxpnl7hjxlqnnjflb88ra1wrjn9wxvrs032k3ibfph5")))

(define-public crate-builder-pattern-0.3.2 (c (n "builder-pattern") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0n8jk2n5m34bxqzwxlklpgqmr26gz8jax1fmsp58ab2vpr3pmh2s")))

(define-public crate-builder-pattern-0.3.3 (c (n "builder-pattern") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1cvbgfppx7wgn8jvkhbslk8gp17jq3p0sf1m351hxxjsybcqajpr")))

(define-public crate-builder-pattern-0.3.4 (c (n "builder-pattern") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1raka2fmmabg7dx58h0fcvl576v6qqjfqsx2c8ws038lfi562h84")))

(define-public crate-builder-pattern-0.3.5 (c (n "builder-pattern") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0iggnj38h6vxm88zagpchsaxvi7la68kqd19k5dwd7853zfjymqg")))

(define-public crate-builder-pattern-0.3.6 (c (n "builder-pattern") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02fzxs3d39dkpkblcbk13n3s9fvzll805z80889c8qbyxiq2r1xv")))

(define-public crate-builder-pattern-0.3.7 (c (n "builder-pattern") (v "0.3.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 2)))) (h "15dk4slfng1d779bwyb43qqw76wgrl6pj4hbl7kd1f12nvwkd9fb")))

(define-public crate-builder-pattern-0.3.8 (c (n "builder-pattern") (v "0.3.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 2)))) (h "01if7rjzcnwn02rlm8m1hwwpkq4jm22zf373d76jbpil12kyxqbi")))

(define-public crate-builder-pattern-0.3.9 (c (n "builder-pattern") (v "0.3.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 2)))) (h "01z5gasdra5rd3gjn06wnw8x7i5lapgl9rhx6f22gqc7zhccl8x7")))

(define-public crate-builder-pattern-0.3.10 (c (n "builder-pattern") (v "0.3.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 2)))) (h "0f80ybzjvjw6j8w8k45mlvrc6gc0fgww8icmn7hl4fdcag9mkw9m")))

(define-public crate-builder-pattern-0.3.11 (c (n "builder-pattern") (v "0.3.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 2)))) (h "04lklfjc3hckkx0vd8ibffvnrhcmzvdvi7q11l8m17l3azbhbm2c")))

(define-public crate-builder-pattern-0.4.0 (c (n "builder-pattern") (v "0.4.0") (d (list (d (n "builder-pattern-macro") (r "^0.4.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 2)))) (h "1s71ml5lfdc6ma85v7g1m62by7x3vnznhf7k6bc5bjk1bxx7f2b2") (f (quote (("future" "futures") ("default" "future"))))))

(define-public crate-builder-pattern-0.4.1 (c (n "builder-pattern") (v "0.4.1") (d (list (d (n "builder-pattern-macro") (r "^0.4.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 2)))) (h "1g71bx85kg8fa0hhn4dya9pkx4mzpwdr796707sr9cdzg7qk2nx4") (f (quote (("future" "futures") ("default" "future"))))))

(define-public crate-builder-pattern-0.4.2 (c (n "builder-pattern") (v "0.4.2") (d (list (d (n "builder-pattern-macro") (r "^0.4.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 2)))) (h "1rfniqbjc039z1hxfaxh7zww1ag76dg50mlzh7fiizlf7nwpclyq") (f (quote (("future" "futures") ("default" "future"))))))

