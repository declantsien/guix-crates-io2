(define-module (crates-io bu il buildkite-cli) #:use-module (crates-io))

(define-public crate-buildkite-cli-0.1.0 (c (n "buildkite-cli") (v "0.1.0") (d (list (d (n "buildkite") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pbkjnmy5k53b73xcbdr96v0hvd83facriadm3wvganj119w7irq")))

