(define-module (crates-io bu il builder-rust) #:use-module (crates-io))

(define-public crate-builder-rust-0.1.0 (c (n "builder-rust") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.32") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.2.0") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1.23") (d #t) (k 0)))) (h "05rii86gwy7z31nw9kk0wwdf06kkxpdrrznpgsf2w59qjdmc42hd") (y #t)))

(define-public crate-builder-rust-0.2.0 (c (n "builder-rust") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "ramhorns") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.32") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.2.0") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1.23") (d #t) (k 0)))) (h "06awd15ix2icvnd4ji4m1knkn6xs2mr92mhrnwzj4g8nw9cy4hjj") (y #t)))

