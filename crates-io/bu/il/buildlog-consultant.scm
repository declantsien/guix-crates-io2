(define-module (crates-io bu il buildlog-consultant) #:use-module (crates-io))

(define-public crate-buildlog-consultant-0.0.33 (c (n "buildlog-consultant") (v "0.0.33") (d (list (d (n "pyo3") (r "^0.18") (f (quote ("auto-initialize"))) (d #t) (k 0)))) (h "00573f0nfyz8kj3086bc3xjr10n5xs50lm2xbxlj6z1k17zzrwd5")))

(define-public crate-buildlog-consultant-0.0.35 (c (n "buildlog-consultant") (v "0.0.35") (d (list (d (n "inventory") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pyo3") (r "^0.19") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "shlex") (r "^1") (d #t) (k 0)))) (h "03gnnzrvlh344yxkhw60hcmhd3gpmd89ibv2p3rl604ga1kcaps0")))

