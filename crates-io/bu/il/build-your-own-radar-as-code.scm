(define-module (crates-io bu il build-your-own-radar-as-code) #:use-module (crates-io))

(define-public crate-build-your-own-radar-as-code-0.1.0 (c (n "build-your-own-radar-as-code") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0i48sm13m9dm5q3gy1sny3cm2k07ydsqq9w1ykvw92phrw26xsh0")))

(define-public crate-build-your-own-radar-as-code-0.2.0 (c (n "build-your-own-radar-as-code") (v "0.2.0") (d (list (d (n "csv") (r "^1.1.1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "037jwmkmv79xc307lsrkhgpapay549wzgp0775vpcq42nj2ws87m")))

