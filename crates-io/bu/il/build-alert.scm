(define-module (crates-io bu il build-alert) #:use-module (crates-io))

(define-public crate-build-alert-0.1.0 (c (n "build-alert") (v "0.1.0") (d (list (d (n "syn") (r "^2") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "12zxi4fjw5kya9wg41chiq1qbw5pv7qip2d50llc6wqiak10wnyx")))

(define-public crate-build-alert-0.1.1 (c (n "build-alert") (v "0.1.1") (d (list (d (n "syn") (r "^2") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "1fhsdhnfp3snb6xi4l4awyaqbdhn159m3bp526n64d7g3524lszk")))

(define-public crate-build-alert-0.1.2 (c (n "build-alert") (v "0.1.2") (d (list (d (n "syn") (r "^2") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)))) (h "1zcva66mpyracgzz8f02vr05kr9dj7g9bmgzavg80cm7pird5ncx")))

(define-public crate-build-alert-0.1.3 (c (n "build-alert") (v "0.1.3") (d (list (d (n "syn") (r "^2.0.23") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "11d968vb0cpm0fadxn0vl7fqm06anxdhg1nr6v41p0w74s4yqaa8")))

(define-public crate-build-alert-0.1.4 (c (n "build-alert") (v "0.1.4") (d (list (d (n "syn") (r "^2.0.23") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1shmnqpjnn60s93yxsbbvw3z77gd2czf19vk0zqq113rh6cyszpj")))

(define-public crate-build-alert-0.1.5 (c (n "build-alert") (v "0.1.5") (d (list (d (n "syn") (r "^2.0.23") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "1srr5sj183zwmc8svpr2z4ghl6x1a29zrlib1vdm03zhnrj57y9x")))

(define-public crate-build-alert-0.1.6 (c (n "build-alert") (v "0.1.6") (d (list (d (n "syn") (r "^2.0.46") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)))) (h "11c36xndly4l23cd60qy8kk2w64ndyvck3i5syai5dl7mzdd67vp")))

