(define-module (crates-io bu il build_script) #:use-module (crates-io))

(define-public crate-build_script-0.1.0 (c (n "build_script") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "12waingiqfij08cjf9zssqk8si8q4rm3fadbfv42h6i1kkqr6vw4")))

(define-public crate-build_script-0.1.1 (c (n "build_script") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1z941446c89m04ic92qkn2l5zj1vz2msnq9kbqs06597bvbqbiq9")))

(define-public crate-build_script-0.1.2 (c (n "build_script") (v "0.1.2") (d (list (d (n "gag") (r "^0.1.10") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)))) (h "0hfnzbmld4qk350j22c7dk8wmpryhx9ss9sy5jdj28flwsd5b1s4")))

(define-public crate-build_script-0.1.3 (c (n "build_script") (v "0.1.3") (d (list (d (n "gag") (r "^0.1.10") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)))) (h "08ajvm7081fm1b9ic18qk20cywhs1jd8cs763f9q2yzyx3anl6p7")))

(define-public crate-build_script-0.1.4 (c (n "build_script") (v "0.1.4") (d (list (d (n "gag") (r "^0.1.10") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)))) (h "0lsq3jb5vy8k0hsvdv6rqpzmyfm1zgh072gr8l4ckydc42yz9xwf")))

(define-public crate-build_script-0.2.0 (c (n "build_script") (v "0.2.0") (d (list (d (n "gag") (r "^0.1.10") (d #t) (k 2)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)))) (h "0qmap2xkp2cbiqdhbjys4nvc4k23rlv1wbijza6q6fb9gb0a6g0n")))

