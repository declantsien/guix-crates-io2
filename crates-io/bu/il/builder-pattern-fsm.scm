(define-module (crates-io bu il builder-pattern-fsm) #:use-module (crates-io))

(define-public crate-builder-pattern-fsm-0.0.1 (c (n "builder-pattern-fsm") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "syn") (r "^2.0.37") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "10bm89b6w1lsx4k7nbq36w51kladzcbzms64jdj61xdr0q83f8j0")))

