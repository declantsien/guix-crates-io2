(define-module (crates-io bu il build-utils) #:use-module (crates-io))

(define-public crate-build-utils-0.1.0 (c (n "build-utils") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0s6i8d2l7flhm3dh2bi6pxrrqmsic5jzndc89g7gw1a1qbid6dfw")))

