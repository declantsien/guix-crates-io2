(define-module (crates-io bu il build-deftly) #:use-module (crates-io))

(define-public crate-build-deftly-0.1.0 (c (n "build-deftly") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "derive-deftly") (r "^0.10.3") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)))) (h "110f3jsqj46283jb5ax6f0276yw27zlh0fgyvqy729lwj8gad291")))

