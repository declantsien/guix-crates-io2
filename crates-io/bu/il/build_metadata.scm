(define-module (crates-io bu il build_metadata) #:use-module (crates-io))

(define-public crate-build_metadata-0.1.0 (c (n "build_metadata") (v "0.1.0") (d (list (d (n "git2") (r "^0.4") (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1z0pzcsjzcmmc8cp666spfxzk1bjz7gnbxq159ciw63ahf9fjf53")))

