(define-module (crates-io bu il building_blocks_vox) #:use-module (crates-io))

(define-public crate-building_blocks_vox-0.1.0 (c (n "building_blocks_vox") (v "0.1.0") (d (list (d (n "building_blocks_core") (r "^0.1") (d #t) (k 0)) (d (n "building_blocks_storage") (r "^0.1") (d #t) (k 0)) (d (n "dot_vox") (r "^4.1") (d #t) (k 0)))) (h "1jgr5kqaf9bbxnxzw7rlhijybx6s1j46jn2kgjy8cg1m735d1lsl")))

(define-public crate-building_blocks_vox-0.2.0 (c (n "building_blocks_vox") (v "0.2.0") (d (list (d (n "building_blocks_core") (r "^0.2") (d #t) (k 0)) (d (n "building_blocks_storage") (r "^0.2") (d #t) (k 0)) (d (n "dot_vox") (r "^4.1") (d #t) (k 0)))) (h "0sndrr72lwwjzhymirdqndffrjq9adzzgjqimr0jsswp7hmhi6md")))

(define-public crate-building_blocks_vox-0.3.0 (c (n "building_blocks_vox") (v "0.3.0") (d (list (d (n "building_blocks_core") (r "^0.3") (d #t) (k 0)) (d (n "building_blocks_storage") (r "^0.3") (d #t) (k 0)) (d (n "dot_vox") (r "^4.1") (d #t) (k 0)))) (h "1mnagski66ksf46dpzai9869nhia3ihsk79bifsia89yh0lw9ng8")))

(define-public crate-building_blocks_vox-0.4.0 (c (n "building_blocks_vox") (v "0.4.0") (d (list (d (n "building_blocks_core") (r "^0.4") (d #t) (k 0)) (d (n "building_blocks_storage") (r "^0.4") (d #t) (k 0)) (d (n "dot_vox") (r "^4.1") (d #t) (k 0)))) (h "034b6zqlm7d5bvqpsaaxw16kbq0m5g1ga2p1l912kyxgmiafixnz")))

