(define-module (crates-io bu il build-helper) #:use-module (crates-io))

(define-public crate-build-helper-0.1.0 (c (n "build-helper") (v "0.1.0") (d (list (d (n "semver") (r "^0.6.0") (d #t) (k 0)))) (h "1ijllgzxs9kal4i4dfnbf9yqdbbqhl1cd4s9i5mda9agb9clp1qh") (f (quote (("nightly") ("default"))))))

(define-public crate-build-helper-0.1.1 (c (n "build-helper") (v "0.1.1") (d (list (d (n "semver") (r "^0.6.0") (d #t) (k 0)))) (h "0pxddbizzd3mvg3777hlg92la5qacjs8734cjk79ajgsycdikkmx") (f (quote (("nightly") ("default"))))))

