(define-module (crates-io bu il buildkit-rs-proto) #:use-module (crates-io))

(define-public crate-buildkit-rs-proto-0.1.0 (c (n "buildkit-rs-proto") (v "0.1.0") (d (list (d (n "prost") (r "^0.11.8") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.8") (d #t) (k 0)) (d (n "tonic") (r "^0.8.3") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.8") (d #t) (k 1)) (d (n "tonic-build") (r "^0.8.4") (d #t) (k 1)))) (h "0c6a2d1gij6l9fzfpgpdjpalrlij0nmjjzg30r1s1xhbxw5i2c7c")))

