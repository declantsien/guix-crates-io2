(define-module (crates-io bu il buildkite) #:use-module (crates-io))

(define-public crate-buildkite-0.1.0 (c (n "buildkite") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0081fjd6f018q5y18avmxkv66s03lbwq7z4qq4wldhy3kc15gzzk")))

