(define-module (crates-io bu il build_epoch) #:use-module (crates-io))

(define-public crate-build_epoch-0.0.1 (c (n "build_epoch") (v "0.0.1") (d (list (d (n "lazy_static") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1dnkhyv72qfj3rrpwi7y9zh66hs0c9dzva9b4gh27pyj8cg703d1")))

(define-public crate-build_epoch-0.1.0 (c (n "build_epoch") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)))) (h "1kc5jh5dnjvmx1qsmf5lxvf2mm9xbf1xw5qwq04yn6ghqz84skn9")))

