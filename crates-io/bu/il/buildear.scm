(define-module (crates-io bu il buildear) #:use-module (crates-io))

(define-public crate-BuildEAR-3.1.0 (c (n "BuildEAR") (v "3.1.0") (d (list (d (n "clap") (r "~2.32.0") (k 0)) (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "0iav55kfdi7m0hvfqwj436i36rl2hkw2k5q6jxfliwf597iwjdd2") (y #t)))

