(define-module (crates-io bu il builder-pattern-macro) #:use-module (crates-io))

(define-public crate-builder-pattern-macro-0.4.0 (c (n "builder-pattern-macro") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wpia46jl72h0bvdwdwna6ynqyifsz3z7j28lbfhy3qflck7qfl2")))

(define-public crate-builder-pattern-macro-0.4.1 (c (n "builder-pattern-macro") (v "0.4.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0529bhc5igdq09q5zy9fygizsw5z38j1lz82d90nrgygwn1x9qif")))

(define-public crate-builder-pattern-macro-0.4.2 (c (n "builder-pattern-macro") (v "0.4.2") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1x7mw4yjvf57m7bpv9cc73yals79isrzy1zq2g8qi5dki3pj9mj7")))

