(define-module (crates-io bu il build-plan) #:use-module (crates-io))

(define-public crate-build-plan-0.1.0 (c (n "build-plan") (v "0.1.0") (d (list (d (n "semver") (r "^0.9.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.55") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.17") (d #t) (k 0)))) (h "1lkg5vjnw9zy6kc9mp9szb4s6i4zgrjiahmc92gqz15d02q7jf9s")))

(define-public crate-build-plan-0.1.1 (c (n "build-plan") (v "0.1.1") (d (list (d (n "semver") (r "^0.9.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.55") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.17") (d #t) (k 0)))) (h "00izk0dl5l5z9p993kj0rjrjh231hzg6ywk7lswiwbs9nyjk8p4k")))

