(define-module (crates-io bu il build-parallel) #:use-module (crates-io))

(define-public crate-build-parallel-0.1.0 (c (n "build-parallel") (v "0.1.0") (d (list (d (n "crossbeam-utils") (r "^0.7.0") (d #t) (k 0)) (d (n "jobserver") (r "^0.1.19") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)))) (h "08s19mr7yqdlkkyl6xnvv7d1prnxgbl755605jlfzkl07cwdp020")))

(define-public crate-build-parallel-0.1.1 (c (n "build-parallel") (v "0.1.1") (d (list (d (n "crossbeam-utils") (r "^0.7.0") (d #t) (k 0)) (d (n "jobserver") (r "^0.1.19") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "0xyy6x4v8gd24vammb6syinyk5dm6rgj1caga5hfd10qiha8npsb")))

(define-public crate-build-parallel-0.1.2 (c (n "build-parallel") (v "0.1.2") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "jobserver") (r "^0.1.19") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)))) (h "1fyjfylll3rh5lcg7y1psc2zqil0c6rhki98wlb7c5j0nyfzzqxq")))

