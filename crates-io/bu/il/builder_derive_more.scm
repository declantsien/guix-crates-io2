(define-module (crates-io bu il builder_derive_more) #:use-module (crates-io))

(define-public crate-builder_derive_more-0.1.0 (c (n "builder_derive_more") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "125fqm44ynz7sr7lhbcfkc9q1bylzx44wpzqnicvyccjn6dz3lpw") (r "1.64")))

