(define-module (crates-io bu il build-data) #:use-module (crates-io))

(define-public crate-build-data-0.1.0 (c (n "build-data") (v "0.1.0") (d (list (d (n "build-data-writer") (r "^0") (d #t) (k 2)))) (h "1nb6d2mii80x1adpbl2mlzyrm6z8c0i2sz0ry12i0br2lzwwppbr") (y #t)))

(define-public crate-build-data-0.1.1 (c (n "build-data") (v "0.1.1") (d (list (d (n "build-data-writer") (r "^0") (d #t) (k 2)))) (h "0kj5055cvb9qqp56kc2b81dabkhfg8jvj3q8xl9vxmc9m9j7ib01") (y #t)))

(define-public crate-build-data-0.1.2 (c (n "build-data") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "rusty-fork") (r "^0.3.0") (d #t) (k 2)) (d (n "safe-lock") (r "^0.1.3") (d #t) (k 0)) (d (n "safe-regex") (r "^0.2.3") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "1l30wxnnk4wbaqb67sjrzmdcp9nhjqi9nxqv8fq8y3msanlwhgwr")))

(define-public crate-build-data-0.1.3 (c (n "build-data") (v "0.1.3") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "rusty-fork") (r "^0.3.0") (d #t) (k 2)) (d (n "safe-lock") (r "^0.1.3") (d #t) (k 0)) (d (n "safe-regex") (r "^0.2.3") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "143d0ah3nk5pbcf50fqwhqd9glq03icab6z2gjnaqydnmbvzk50s")))

(define-public crate-build-data-0.1.4 (c (n "build-data") (v "0.1.4") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "rusty-fork") (r "^0.3.0") (d #t) (k 2)) (d (n "safe-lock") (r "^0.1.3") (d #t) (k 0)) (d (n "safe-regex") (r "^0.2.3") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "0xl7g4bd1rmx9mmsqhgs0qlbcc57579dficaba5bfbbb853krj2a")))

(define-public crate-build-data-0.1.5 (c (n "build-data") (v "0.1.5") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "rusty-fork") (r "^0.3.0") (d #t) (k 2)) (d (n "safe-lock") (r "^0.1.3") (d #t) (k 0)) (d (n "safe-regex") (r "^0.2.3") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)))) (h "1h5kknpc9rz8y75fsc68zpvjv4va9cqm1lfjiwy9fz5b5i78ilxf")))

(define-public crate-build-data-0.2.0 (c (n "build-data") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "safe-lock") (r "^0.1") (d #t) (k 2)) (d (n "safe-regex") (r "^0.3") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)))) (h "02qjinwy67nifp33iw3yj7qv5mmnizcbsd5z0ayxx491dnphvfiq") (y #t)))

(define-public crate-build-data-0.2.1 (c (n "build-data") (v "0.2.1") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (k 0)) (d (n "safe-lock") (r "^0.1") (d #t) (k 2)) (d (n "safe-regex") (r "^0.3") (d #t) (k 0)) (d (n "speculoos") (r "^0.11.0") (d #t) (k 2)))) (h "1r3wkfrw6lsryq594sbsz82iqjd9bj51lxf2yk1z68wwrv70z8pd")))

