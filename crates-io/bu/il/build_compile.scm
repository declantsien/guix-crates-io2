(define-module (crates-io bu il build_compile) #:use-module (crates-io))

(define-public crate-build_compile-0.1.0 (c (n "build_compile") (v "0.1.0") (h "0pfa1snmzx9phf4hkwmw16ja5rm0hqbrj9bch5ych4s7rkqqldwm")))

(define-public crate-build_compile-0.2.0 (c (n "build_compile") (v "0.2.0") (h "0f3h7jmcnwjqzcskr0w0klskf2c4vgf0nnxsggn3w56mfj84wgnh")))

