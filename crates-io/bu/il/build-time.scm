(define-module (crates-io bu il build-time) #:use-module (crates-io))

(define-public crate-build-time-0.1.0 (c (n "build-time") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0spzlf3zfkw568aq5mhnwjrmrcz05w5jwqdav9pm8zym2adh3d5w")))

(define-public crate-build-time-0.1.1 (c (n "build-time") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0h1a7dqvq4rqkh1kiibi5csxhgrvvvbwmjp2s1xiik75alscq0qr")))

(define-public crate-build-time-0.1.2 (c (n "build-time") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.20") (d #t) (k 0)) (d (n "once_cell") (r "^1.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "11lpz7y9sd7b7qbvgsy913qcdz595r921mnharr00k6dzg7zq5mm")))

(define-public crate-build-time-0.1.3 (c (n "build-time") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.20") (f (quote ("clock"))) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing"))) (d #t) (k 0)))) (h "10nk1lqb5mlmwxv0300pylf9bg7mmwhb8s3r9gbvzdr9zhcrq8gi")))

