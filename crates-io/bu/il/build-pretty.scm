(define-module (crates-io bu il build-pretty) #:use-module (crates-io))

(define-public crate-build-pretty-1.0.0 (c (n "build-pretty") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "retrieve") (r "^1.1.1") (d #t) (k 0)) (d (n "rust-i18n") (r "^0.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "14k4f91pcarmkyqm0p1q6r5vxwpfgdfpdpiycqw1bsjc9nfz3p2v")))

