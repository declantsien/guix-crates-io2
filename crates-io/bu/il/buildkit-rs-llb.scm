(define-module (crates-io bu il buildkit-rs-llb) #:use-module (crates-io))

(define-public crate-buildkit-rs-llb-0.1.0 (c (n "buildkit-rs-llb") (v "0.1.0") (d (list (d (n "buildkit-rs-proto") (r "^0.1.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "shlex") (r "^1.1.0") (d #t) (k 0)))) (h "14rlzqmci2dz7c5kn4vsfk5bm0pamxsakr4fflhyl7b5h9imzg2r")))

