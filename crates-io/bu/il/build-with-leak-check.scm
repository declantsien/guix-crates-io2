(define-module (crates-io bu il build-with-leak-check) #:use-module (crates-io))

(define-public crate-build-with-leak-check-0.1.0 (c (n "build-with-leak-check") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (d #t) (k 0)))) (h "03p1pqw225gvbs4ig9yifzqim5mwnmlyzsv3sglyjvl0w3x88q78")))

