(define-module (crates-io bu il build-trie) #:use-module (crates-io))

(define-public crate-build-trie-0.1.0 (c (n "build-trie") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1a1ds0gmn14c38k02kzk1rfg2yvgvmvkdpdv89vaslc1xn3m2y4d")))

(define-public crate-build-trie-0.1.1 (c (n "build-trie") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08scbiwn1y7q3m82lnvk13p8zsddqzr7wmbzfm1isnq3y1b6xn6w")))

