(define-module (crates-io bu il build-env) #:use-module (crates-io))

(define-public crate-build-env-0.0.1 (c (n "build-env") (v "0.0.1") (h "17sirascm7c63714piy31x4mnwwzx1v87jnm86q2s3qgn2lxs8ib")))

(define-public crate-build-env-0.0.2 (c (n "build-env") (v "0.0.2") (h "11panzbkpjz2fkh815yqwqqyfi9hgz40x2x9x3y8gsacdri94167")))

(define-public crate-build-env-0.1.0 (c (n "build-env") (v "0.1.0") (h "1qpyqhxfrs50lansfrn62rgmiv6ld0ggnjlah3scndbmnyik868z")))

(define-public crate-build-env-0.2.0 (c (n "build-env") (v "0.2.0") (h "17ng4f5jlwhaikjd7h1811yn55wm2xbggjiy8aavcm7r70cz6s70")))

(define-public crate-build-env-0.3.0 (c (n "build-env") (v "0.3.0") (h "09jrylcimngnnb2a4kmzf9y5iflgr775cchm3kpp89ibxx39iy3c")))

(define-public crate-build-env-0.3.1 (c (n "build-env") (v "0.3.1") (h "09az61wrd609vvixvhzsj49b8vny9qzl101zxzwip881x1paq8hm")))

