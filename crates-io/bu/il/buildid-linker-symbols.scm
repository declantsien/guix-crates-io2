(define-module (crates-io bu il buildid-linker-symbols) #:use-module (crates-io))

(define-public crate-buildid-linker-symbols-1.0.0 (c (n "buildid-linker-symbols") (v "1.0.0") (h "1z2614n0vkxplwx1f425znabfhfhivgmyzc6jz8b6cha86qqld5p")))

(define-public crate-buildid-linker-symbols-1.0.1 (c (n "buildid-linker-symbols") (v "1.0.1") (h "18c2xf64ldgbf983pwzkcfklq13r1g312s6q8zgdxp97lj1fxb07")))

