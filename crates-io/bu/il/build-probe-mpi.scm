(define-module (crates-io bu il build-probe-mpi) #:use-module (crates-io))

(define-public crate-build-probe-mpi-0.1.0 (c (n "build-probe-mpi") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 0)))) (h "14sxsl5awj9i3hi7vis1209b6lzr21jmyf53psdgx35c4gd6g563")))

(define-public crate-build-probe-mpi-0.1.1 (c (n "build-probe-mpi") (v "0.1.1") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 0)))) (h "125dxmiqfblxbm1s49a7k1j8232arm0qrxm5jky2z1glrp1dvmgl")))

(define-public crate-build-probe-mpi-0.1.2 (c (n "build-probe-mpi") (v "0.1.2") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (t "cfg(unix)") (k 0)))) (h "0az6flg2yy21px3sv3a0rddnl6f7dk9cqbi1f6psm15yx6vdkx9y")))

(define-public crate-build-probe-mpi-0.1.3 (c (n "build-probe-mpi") (v "0.1.3") (d (list (d (n "pkg-config") (r "^0.3.27") (d #t) (t "cfg(unix)") (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "1k787flyzfxks455z077bn5a0q10fnkx0ldc74gjwbyz8s1wf1k6") (r "1.65")))

(define-public crate-build-probe-mpi-0.1.4 (c (n "build-probe-mpi") (v "0.1.4") (d (list (d (n "pkg-config") (r "^0.3.30") (d #t) (t "cfg(unix)") (k 0)) (d (n "shell-words") (r "^1.1.0") (d #t) (k 0)))) (h "1zpqnxi76qlp4lv671wxj7b2p3wcwsda0fqqqwwf7q7nw9nzld1j") (r "1.65")))

