(define-module (crates-io bu il build-wrap) #:use-module (crates-io))

(define-public crate-build-wrap-0.1.0 (c (n "build-wrap") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "cargo_metadata") (r "^0.18") (d #t) (k 2)) (d (n "once_cell") (r "^1.19") (d #t) (k 2)) (d (n "tempfile") (r "^3.9") (d #t) (k 0)))) (h "0akc7xz606iqbnq6m12kw9lcfril7vi0fzphdmbv7ypqcm98rwb9")))

(define-public crate-build-wrap-0.1.1 (c (n "build-wrap") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "cargo_metadata") (r "^0.18") (d #t) (k 2)) (d (n "ctor") (r "^0.2") (d #t) (k 2)) (d (n "once_cell") (r "^1.19") (d #t) (k 2)) (d (n "tempfile") (r "^3.10") (d #t) (k 0)))) (h "0mz3wcdcxl4257rc2bwbwipazmqgdz4p6zb4xhkjlj4rm93qwsgz")))

(define-public crate-build-wrap-0.2.0 (c (n "build-wrap") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "cargo_metadata") (r "^0.18") (d #t) (k 2)) (d (n "ctor") (r "^0.2") (d #t) (k 2)) (d (n "once_cell") (r "^1.19") (d #t) (k 2)) (d (n "tempfile") (r "^3.10") (d #t) (k 0)))) (h "0h3bq56mx962vk79m65bfk4gjcpblbwb01nw5s5g0alc2n1q66lf")))

(define-public crate-build-wrap-0.2.1 (c (n "build-wrap") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "cargo_metadata") (r "^0.18") (d #t) (k 2)) (d (n "ctor") (r "^0.2") (d #t) (k 2)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "tempfile") (r "^3.10") (d #t) (k 0)))) (h "10r286i9c25g36cma4jpkqhm85j5y1n7dr3vpldalwpv3ph4d7nf")))

(define-public crate-build-wrap-0.3.0 (c (n "build-wrap") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "cargo_metadata") (r "^0.18") (d #t) (k 2)) (d (n "ctor") (r "^0.2") (d #t) (k 2)) (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 2)) (d (n "tempfile") (r "^3.10") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1r0fa7ck012464hkjz6xv38kdbr63cs047fxc0n0h7x5zqnffimr")))

