(define-module (crates-io bu il buildable) #:use-module (crates-io))

(define-public crate-buildable-0.0.1 (c (n "buildable") (v "0.0.1") (h "0k2l88g0a7g95ipmxs2vpgz63si1wsqaq1czgzwa6jzagnrwaq66")))

(define-public crate-buildable-0.0.2 (c (n "buildable") (v "0.0.2") (h "15bgp82lc5fh68g50bl3i4accl1f8m0fwhiby9kzrf8nd4h3hki8")))

(define-public crate-buildable-0.0.3 (c (n "buildable") (v "0.0.3") (h "0lsnqzm95j13vca2hivp2k1ffww9fkhiavqwc3jp865vnas4dsxc")))

(define-public crate-buildable-0.0.4 (c (n "buildable") (v "0.0.4") (h "1fzwskk5g77diqxarnl3mzbymvaa3s9wj1675vcfs88l3dr0z7mh")))

(define-public crate-buildable-0.0.5 (c (n "buildable") (v "0.0.5") (h "0jrk59qk5wr10aah7x4hfp4iac596sws3qcb27s5n1q22mf8cp2h")))

