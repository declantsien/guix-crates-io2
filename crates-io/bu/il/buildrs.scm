(define-module (crates-io bu il buildrs) #:use-module (crates-io))

(define-public crate-buildrs-0.1.0 (c (n "buildrs") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "02l2956rva8hwliwjjby3irhjqwi4py4cayhi4pvmbmk5hmb7ccp")))

(define-public crate-buildrs-0.1.1 (c (n "buildrs") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "0wa7rqycj766j2gvfmp7dg7w9rljk2b8md490b5p6n81mjn8j6nd")))

