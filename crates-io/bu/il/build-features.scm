(define-module (crates-io bu il build-features) #:use-module (crates-io))

(define-public crate-build-features-0.1.0 (c (n "build-features") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)))) (h "07kfy1y349ghw7lpix3lld63dmc09068x7dn3bxn9h7jw5d19zyc") (y #t)))

(define-public crate-build-features-0.1.1 (c (n "build-features") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)))) (h "1j07qaisdrg57far5jf6wrb39h3imrqy0zxlqy23fxihgl9rkg2b") (y #t)))

(define-public crate-build-features-0.1.2 (c (n "build-features") (v "0.1.2") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)))) (h "0zj3sk7ffnmjmkwpw6dhnhlgbjvhh0zv8lyq25x3ag40z26kyyj4") (y #t)))

(define-public crate-build-features-0.1.3 (c (n "build-features") (v "0.1.3") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)))) (h "1cfpwgiiz8qac4my9i0wdaqxzbwf9pdn8kjp9xhycpjfdm56s0j5") (y #t)))

(define-public crate-build-features-0.1.4 (c (n "build-features") (v "0.1.4") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)))) (h "0a2adyrjr7557k4nrwcphbc956vpbs79gh4dkf56ffl44z7sgscx") (y #t)))

(define-public crate-build-features-0.1.5 (c (n "build-features") (v "0.1.5") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)))) (h "0bh7j8ak6f1wzjlfbj4lqgvxszx00n50ffibwsj4wqxfxlijid22") (y #t)))

(define-public crate-build-features-0.1.6 (c (n "build-features") (v "0.1.6") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)))) (h "1yd7s7b5hfkkxs5f4bxwvrpd89vmxdqh8p609sy6ag0zdjfh1xwp") (y #t)))

(define-public crate-build-features-0.1.7 (c (n "build-features") (v "0.1.7") (d (list (d (n "cargo_metadata") (r "^0.14.1") (d #t) (k 0)))) (h "0gq7kix1dmdy2szm6rn5a543y625vb58yx1q81h3icm72z5w2pa7")))

