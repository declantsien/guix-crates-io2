(define-module (crates-io bu il build_const) #:use-module (crates-io))

(define-public crate-build_const-0.1.0 (c (n "build_const") (v "0.1.0") (h "11wczvvcdccahym3qvy0jd754sc1g0mmrmgqsqaa7c84ziaxfkp9")))

(define-public crate-build_const-0.1.1 (c (n "build_const") (v "0.1.1") (h "00al3nbrwhsy670xjya8caz9ivwkjya1mi6r3l3g5y187x61q9l5")))

(define-public crate-build_const-0.2.0 (c (n "build_const") (v "0.2.0") (h "1jdr1kx4wdhazzxnzys0fg98pnxn6cnw50vbczkyplk2br7wh3g9") (f (quote (("std") ("default" "std"))))))

(define-public crate-build_const-0.2.1 (c (n "build_const") (v "0.2.1") (h "0faz882spx9474cszay2djmb0lghbwq51qayabcar1s7g4r2l29r") (f (quote (("std") ("default" "std"))))))

(define-public crate-build_const-0.2.2 (c (n "build_const") (v "0.2.2") (h "1dryhsf4vfi1plljgv069sgfr8m1rsg04qy76x36kh6swqsl5bml") (f (quote (("std") ("default" "std"))))))

