(define-module (crates-io bu il builder_m4cro) #:use-module (crates-io))

(define-public crate-builder_m4cro-0.1.0 (c (n "builder_m4cro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1b1ghqky63zh0pvsh1bmqrk34xsij7s9aafmlb6dm7sn5x28x10v")))

(define-public crate-builder_m4cro-0.2.0 (c (n "builder_m4cro") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1rw8sb928kdfhdd7d2cdnyq73hgdzdgcf03lhm0kmszlwd95sv67")))

(define-public crate-builder_m4cro-0.3.0 (c (n "builder_m4cro") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "003wj2vhr59gp6hgvr01cs3pw8idl5y3304lxhc335j2m47zvjkg")))

(define-public crate-builder_m4cro-0.3.1 (c (n "builder_m4cro") (v "0.3.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0gazy111r3hwa6r74klvlkpzl17lj5vg6xi62hssqwhp7zsqavlm")))

