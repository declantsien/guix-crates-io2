(define-module (crates-io bu il buildinfy) #:use-module (crates-io))

(define-public crate-buildinfy-0.1.0 (c (n "buildinfy") (v "0.1.0") (h "0y09pav5bj6gk08v1r9ywvqgwsfkjz7l2cfjzi4sdlm8f9bj6mqh")))

(define-public crate-buildinfy-0.1.1 (c (n "buildinfy") (v "0.1.1") (h "1iwlgharnqf7rqys4q23ammlz40s0v5969kmczccn8x56shlqc33")))

(define-public crate-buildinfy-0.1.2 (c (n "buildinfy") (v "0.1.2") (h "0x0j70pb7rs3jw6ckjvzns4l7ba66427v9db0pmv7wf3s68ndbgr")))

(define-public crate-buildinfy-0.1.3 (c (n "buildinfy") (v "0.1.3") (h "0yrzdifnb6rr9bb9f0h212vp6jsyhrv77p2ygmsaxbwgj60n3k3q")))

(define-public crate-buildinfy-0.1.4 (c (n "buildinfy") (v "0.1.4") (h "05nvd9magc6fzm4v78lsbsmvcjismnrlgddgz71l8x89gyzqh2j3")))

