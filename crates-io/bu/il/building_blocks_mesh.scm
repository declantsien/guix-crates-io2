(define-module (crates-io bu il building_blocks_mesh) #:use-module (crates-io))

(define-public crate-building_blocks_mesh-0.1.0 (c (n "building_blocks_mesh") (v "0.1.0") (d (list (d (n "building_blocks_core") (r "^0.1") (d #t) (k 0)) (d (n "building_blocks_storage") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)))) (h "094ixw7sjkyns3idx16rggrycr3iz3s0ksq4nd4mz1r6dxy4nr5i")))

(define-public crate-building_blocks_mesh-0.2.0 (c (n "building_blocks_mesh") (v "0.2.0") (d (list (d (n "building_blocks_core") (r "^0.2") (d #t) (k 0)) (d (n "building_blocks_storage") (r "^0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)))) (h "1s1hgbar0asvzjcilddi72mq7gwknq53smrb7v3a2yc0hs0zlapj")))

(define-public crate-building_blocks_mesh-0.3.0 (c (n "building_blocks_mesh") (v "0.3.0") (d (list (d (n "building_blocks_core") (r "^0.3") (d #t) (k 0)) (d (n "building_blocks_storage") (r "^0.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)))) (h "1bx9rxm32nm19payk99fi9l7hdqy8ghx3bxdbagridr9qkqqc2iv")))

(define-public crate-building_blocks_mesh-0.4.0 (c (n "building_blocks_mesh") (v "0.4.0") (d (list (d (n "building_blocks_core") (r "^0.4") (d #t) (k 0)) (d (n "building_blocks_storage") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)))) (h "1gq70bjfhpkshwbnl2bna3xrdbl1akq3gj9s9gw6k68r2fn1f2mp")))

(define-public crate-building_blocks_mesh-0.4.3 (c (n "building_blocks_mesh") (v "0.4.3") (d (list (d (n "building_blocks_core") (r "^0.4.3") (k 0)) (d (n "building_blocks_storage") (r "^0.4.3") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)))) (h "1frcd6wrvyas3v13qaz5j0a60klslfnw5v1bav5vjaf96jqm2j40")))

(define-public crate-building_blocks_mesh-0.5.0 (c (n "building_blocks_mesh") (v "0.5.0") (d (list (d (n "building_blocks_core") (r "^0.5.0") (k 0)) (d (n "building_blocks_storage") (r "^0.5.0") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)))) (h "0ylp85vhfypf324ywwrwsg9kkmxb4x4hb17rksj4vwa8db3qm3kv")))

(define-public crate-building_blocks_mesh-0.6.0 (c (n "building_blocks_mesh") (v "0.6.0") (d (list (d (n "building_blocks_core") (r "^0.6.0") (k 0)) (d (n "building_blocks_storage") (r "^0.6.0") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0q0x17x7xm8fnvbawgv0mb6rfbp0851gcjdhwh9v4mi6wxbzhini")))

(define-public crate-building_blocks_mesh-0.7.0 (c (n "building_blocks_mesh") (v "0.7.0") (d (list (d (n "building_blocks_core") (r "^0.7.0") (k 0)) (d (n "building_blocks_storage") (r "^0.7.0") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "06bk8a5j30c0rf3s6rrb1b39dmcdnfwv7jrhjhx9mks5f00712fv")))

(define-public crate-building_blocks_mesh-0.7.1 (c (n "building_blocks_mesh") (v "0.7.1") (d (list (d (n "building_blocks_core") (r "^0.7.1") (k 0)) (d (n "building_blocks_storage") (r "^0.7.1") (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1sqs3jpyzn4am5ygrjq1wh266ypvpyfar5qj4zzaskj6hf0aqgnh")))

