(define-module (crates-io bu il builder_derive) #:use-module (crates-io))

(define-public crate-builder_derive-0.0.1 (c (n "builder_derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.49") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14bnbij991vbxbijjcpabvj6qwsx3zsjqw7kdr6lr392idvi5hmj")))

