(define-module (crates-io bu il build-data-writer) #:use-module (crates-io))

(define-public crate-build-data-writer-0.1.0 (c (n "build-data-writer") (v "0.1.0") (h "07zp7wkvbql93r55kaiy4inkah64p0c0f5p2b4y0clhxp7wi37nq") (y #t)))

(define-public crate-build-data-writer-0.1.1 (c (n "build-data-writer") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "build-data") (r "^0") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "safe-lock") (r "^0.1.3") (d #t) (k 2)) (d (n "safe-regex") (r "^0.2.3") (d #t) (k 2)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)) (d (n "temp-dir") (r "^0.1.10") (d #t) (k 2)) (d (n "temp-file") (r "^0.1.5") (d #t) (k 2)))) (h "03rfqrz0z1l9fj6mk7ia41p2zks9v7plvw69naknwdd3ycb0zikj") (y #t)))

(define-public crate-build-data-writer-0.1.2 (c (n "build-data-writer") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "build-data") (r "^0") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "safe-lock") (r "^0.1.3") (d #t) (k 2)) (d (n "safe-regex") (r "^0.2.3") (d #t) (k 2)) (d (n "spectral") (r "^0.6.0") (d #t) (k 2)) (d (n "temp-dir") (r "^0.1.10") (d #t) (k 2)) (d (n "temp-file") (r "^0.1.5") (d #t) (k 2)))) (h "0npyfpv6bva4rw5pnbf9qw6jyqfsrl9ip92nrd63x8lfpg58vdjl") (y #t)))

