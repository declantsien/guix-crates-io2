(define-module (crates-io bu il building_blocks_procgen) #:use-module (crates-io))

(define-public crate-building_blocks_procgen-0.1.0 (c (n "building_blocks_procgen") (v "0.1.0") (d (list (d (n "building_blocks_core") (r "^0.1") (d #t) (k 0)) (d (n "building_blocks_storage") (r "^0.1") (d #t) (k 0)))) (h "120qfaa1ib9l77cdfxk8sc5g926srch7mnqrr9zlxzb4bpasx2xr")))

(define-public crate-building_blocks_procgen-0.2.0 (c (n "building_blocks_procgen") (v "0.2.0") (d (list (d (n "building_blocks_core") (r "^0.2") (d #t) (k 0)) (d (n "building_blocks_storage") (r "^0.2") (d #t) (k 0)))) (h "0ciqlqcc428fvsflh9pqzspcsv1c9rfsn9mjyl8rx1dv2ivzcfrp")))

(define-public crate-building_blocks_procgen-0.3.0 (c (n "building_blocks_procgen") (v "0.3.0") (d (list (d (n "building_blocks_core") (r "^0.3") (d #t) (k 0)))) (h "0hpm0m8xh0qlp3a5hm7hhnjchgmyrmyjzk1i5692n0va3wsxkmak")))

(define-public crate-building_blocks_procgen-0.4.0 (c (n "building_blocks_procgen") (v "0.4.0") (d (list (d (n "building_blocks_core") (r "^0.4") (d #t) (k 0)))) (h "077p78zzwqxxdbf37rk363as8pvzb5z5hq4cfskmr7v9310pifan")))

