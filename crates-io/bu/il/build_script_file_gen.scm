(define-module (crates-io bu il build_script_file_gen) #:use-module (crates-io))

(define-public crate-build_script_file_gen-0.1.0 (c (n "build_script_file_gen") (v "0.1.0") (h "04kdcqnakg7ws73bc3ap8dg3l7wjvgbz0ddv88w3gjbgi49d22yw")))

(define-public crate-build_script_file_gen-0.2.0 (c (n "build_script_file_gen") (v "0.2.0") (h "0h6fw86nm7ix8p8zlh5f73qbzjs7lkc7qbjf831vfnw8121631vr")))

(define-public crate-build_script_file_gen-0.3.0 (c (n "build_script_file_gen") (v "0.3.0") (h "075j4kwf2mbq21b97xr8y2hcl1m1plrwr2jrh6kphinaalmiyc2k")))

(define-public crate-build_script_file_gen-0.4.0 (c (n "build_script_file_gen") (v "0.4.0") (h "1f6ykmshc31mp5pc7s86x0vbxb1gzimmbfhinaq2qkpgbs0y7pin")))

(define-public crate-build_script_file_gen-0.5.0 (c (n "build_script_file_gen") (v "0.5.0") (h "03qdikz2sk57qnm5437lcxzbfh7aipa5pna13rbzfv09wz1ygnxl")))

(define-public crate-build_script_file_gen-0.6.0 (c (n "build_script_file_gen") (v "0.6.0") (h "0z07195nbqnqnq031jrsa2hkv6ri1cxa9xlrr1d0ilwgbj06vvn1")))

(define-public crate-build_script_file_gen-0.6.1 (c (n "build_script_file_gen") (v "0.6.1") (h "1yisbzjqp5an4cicwcskra0hs7hh33dn5x8k78h6yxs56mxhxk62")))

