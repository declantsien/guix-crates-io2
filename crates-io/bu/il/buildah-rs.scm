(define-module (crates-io bu il buildah-rs) #:use-module (crates-io))

(define-public crate-buildah-rs-0.1.0 (c (n "buildah-rs") (v "0.1.0") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0vcy5jjvi58hf01fkg6vfdf0hfhw88c6ldb1vz2jyn1xgmjq03gp")))

(define-public crate-buildah-rs-0.1.1 (c (n "buildah-rs") (v "0.1.1") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0qgx89367qyz4j0bj8cgxyfzjn4xq0iracmws3sz3fk23sv5ry8k")))

(define-public crate-buildah-rs-0.2.0 (c (n "buildah-rs") (v "0.2.0") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "01zqhws4dbxnrn8a6v2yzxii0rhizvqyavy678j4fv981sj92mbl")))

(define-public crate-buildah-rs-0.3.0 (c (n "buildah-rs") (v "0.3.0") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1hdfxzdgyrvmzigl68hld4cy6dv6b0slf3h6w0x2wjdxyc5h30az")))

