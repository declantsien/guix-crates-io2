(define-module (crates-io bu il build_cfg_macros) #:use-module (crates-io))

(define-public crate-build_cfg_macros-0.1.0 (c (n "build_cfg_macros") (v "0.1.0") (d (list (d (n "build_cfg_shared") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10c0rg23l1q1nci5pkqb2z62hh6hms6b3dvkmy175g454kbyq3js")))

(define-public crate-build_cfg_macros-1.0.0 (c (n "build_cfg_macros") (v "1.0.0") (d (list (d (n "build_cfg_shared") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06fpi51glgihnxmxq89hhjlkiz72404hnnfa025yqpzdnz8yz8jp")))

(define-public crate-build_cfg_macros-1.1.0 (c (n "build_cfg_macros") (v "1.1.0") (d (list (d (n "build_cfg_shared") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xhlr4s3mg599ka1ay5s2v4x0cqg52ab5ms9iij2q5b1ih4f02i7")))

