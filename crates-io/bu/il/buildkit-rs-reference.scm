(define-module (crates-io bu il buildkit-rs-reference) #:use-module (crates-io))

(define-public crate-buildkit-rs-reference-0.1.0 (c (n "buildkit-rs-reference") (v "0.1.0") (d (list (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1f4768g121s1c6zk903chlllz0q7gl9lh3km6aazcmb8ifkk1vnb")))

