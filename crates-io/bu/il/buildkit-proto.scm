(define-module (crates-io bu il buildkit-proto) #:use-module (crates-io))

(define-public crate-buildkit-proto-0.1.0 (c (n "buildkit-proto") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "prost") (r "^0.5") (d #t) (k 0)) (d (n "prost-types") (r "^0.5") (d #t) (k 0)) (d (n "tower-grpc") (r "^0.1") (d #t) (k 0)) (d (n "tower-grpc-build") (r "^0.1") (d #t) (k 1)))) (h "1vcgsdivqj0z9w08avbmnp4hxlbrpsgm0c1ibi7km3ka05jj7lwh")))

(define-public crate-buildkit-proto-0.2.0 (c (n "buildkit-proto") (v "0.2.0") (d (list (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-types") (r "^0.6") (d #t) (k 0)) (d (n "tonic") (r "^0.1") (d #t) (k 0)) (d (n "tonic-build") (r "^0.1") (f (quote ("transport"))) (k 1)))) (h "05g8y7p54wkfx72jrdi2wbylr3g17gjpfa1z1i5vbwpvb9r65q9d")))

