(define-module (crates-io bu il build-context) #:use-module (crates-io))

(define-public crate-build-context-0.1.0 (c (n "build-context") (v "0.1.0") (h "1cj00f4795midqf3yam0rgypj5d578vrqrm8s80wh5nq91vhfqh4") (r "1.45")))

(define-public crate-build-context-0.1.1 (c (n "build-context") (v "0.1.1") (h "15jnnh6vk7ifc98j08rv8v9yapzaciz7awdk4q2lrna868dnzhjh") (r "1.45")))

(define-public crate-build-context-0.1.2 (c (n "build-context") (v "0.1.2") (h "1fizasfbqjaxk3fr532f8r2gxia5xb6synzxwf8ivvsyilchkk2k") (r "1.31")))

