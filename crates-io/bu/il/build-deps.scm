(define-module (crates-io bu il build-deps) #:use-module (crates-io))

(define-public crate-build-deps-0.1.0 (c (n "build-deps") (v "0.1.0") (d (list (d (n "build-helper") (r "^0.1.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)))) (h "027p9zlzb6spjhm9si34qzgsvpa5vlxn4q7hx2q4qxjz9f4qcdyl") (y #t)))

(define-public crate-build-deps-0.1.1 (c (n "build-deps") (v "0.1.1") (d (list (d (n "build-helper") (r "^0.1.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)))) (h "0hwf5p226cjib10ihxw736knqcnw78pnkfr9yp1r27xc6x1z0civ")))

(define-public crate-build-deps-0.1.2 (c (n "build-deps") (v "0.1.2") (d (list (d (n "build-helper") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)))) (h "0mnn6kg2f0xzr46q482lhxlk78fy3fq1gn83s3zhsr9c99ki62fv")))

(define-public crate-build-deps-0.1.3 (c (n "build-deps") (v "0.1.3") (d (list (d (n "build-helper") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)))) (h "1midzmf2320r1md9rs27csvpys3lqywxpx6kl8faa1mlhvablyb5")))

(define-public crate-build-deps-0.1.4 (c (n "build-deps") (v "0.1.4") (d (list (d (n "glob") (r "^0.3") (d #t) (k 0)))) (h "0hvym3c328r57rfszrzqhiw6hij4a8nw6lrm7r7ww608jrl49wb4")))

