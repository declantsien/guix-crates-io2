(define-module (crates-io bu il build-rs) #:use-module (crates-io))

(define-public crate-build-rs-0.1.0 (c (n "build-rs") (v "0.1.0") (h "0szqsg0hk6g3jfwj0ykikmllv30ia8vcp27pi64l0isk3gj60phs")))

(define-public crate-build-rs-0.1.2 (c (n "build-rs") (v "0.1.2") (h "1lkxq4q9l3gg5jdsfca58rjq5x8647c0v2hv23czi6cccriqf2xh")))

