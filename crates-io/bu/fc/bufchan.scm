(define-module (crates-io bu fc bufchan) #:use-module (crates-io))

(define-public crate-bufchan-0.1.0 (c (n "bufchan") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8.2") (d #t) (k 2)) (d (n "divan") (r "^0.1") (d #t) (k 2)) (d (n "fastrand") (r "^2") (d #t) (k 2)) (d (n "flume") (r "^0.11.0") (d #t) (k 2)) (d (n "kanal") (r "^0.1.0-pre8") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)))) (h "0i0h7syqb1l8yq3cjas0d8x9xipkb284wb3yk35g1hj4lypgwhks")))

