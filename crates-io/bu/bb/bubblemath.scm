(define-module (crates-io bu bb bubblemath) #:use-module (crates-io))

(define-public crate-bubblemath-0.1.0 (c (n "bubblemath") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "032lgm2lkj9vywsdf9b62x1zcmsc9scqqxblbs1k9s0bbpjqpcb7")))

(define-public crate-bubblemath-0.1.1 (c (n "bubblemath") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0p16cdajgw4pffr7wrmnj1xj9892fqjd1p2pa3njijh3d3jwyva0")))

(define-public crate-bubblemath-0.1.2 (c (n "bubblemath") (v "0.1.2") (d (list (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0cc8rqlyraa1f52risaj4d8sh71zlpq7530whz3aryxhkx2hc0kc")))

