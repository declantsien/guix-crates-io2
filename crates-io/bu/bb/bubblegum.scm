(define-module (crates-io bu bb bubblegum) #:use-module (crates-io))

(define-public crate-bubblegum-1.0.0 (c (n "bubblegum") (v "1.0.0") (d (list (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "dot_parse") (r "^0.2.0") (d #t) (k 0)) (d (n "httparse") (r "^1.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2") (d #t) (k 0)) (d (n "queues") (r "^1.1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "tungstenite") (r "^0.17.2") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0c9j5ka5w2z13hdffni601wl6mz2q64d34d6jljqbb13ysq94mdv")))

(define-public crate-bubblegum-1.0.1 (c (n "bubblegum") (v "1.0.1") (d (list (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "dot_parse") (r "^0.2.0") (d #t) (k 0)) (d (n "httparse") (r "^1.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2") (d #t) (k 0)) (d (n "queues") (r "^1.1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)) (d (n "tungstenite") (r "^0.17.2") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1w012hy9agwf1vsj44dli53hpca5k3ayild083285nf8c6c64kwv")))

