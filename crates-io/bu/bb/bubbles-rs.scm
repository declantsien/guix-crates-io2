(define-module (crates-io bu bb bubbles-rs) #:use-module (crates-io))

(define-public crate-bubbles-rs-0.1.0 (c (n "bubbles-rs") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "fast-srgb8") (r "^1.0.0") (d #t) (k 0)) (d (n "palette") (r "^0.6.1") (d #t) (k 0)))) (h "1wdzzg93xnpxvd7nnf6zxfacjig2ij1s61nll8izncgwrv4vm21m")))

(define-public crate-bubbles-rs-0.2.0 (c (n "bubbles-rs") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "fast-srgb8") (r "^1.0.0") (d #t) (k 0)) (d (n "palette") (r "^0.6.1") (d #t) (k 0)))) (h "028sx9aray8mi44v88jmb1c07fa7l4j7wqjjx6720zln659py1zr")))

