(define-module (crates-io bu bb bubblebabble) #:use-module (crates-io))

(define-public crate-bubblebabble-0.1.0 (c (n "bubblebabble") (v "0.1.0") (h "06y080n82mp8iclnkq9vicijsk1ysyd4xkgxh1whlkbnixdyv6hh")))

(define-public crate-bubblebabble-0.1.1 (c (n "bubblebabble") (v "0.1.1") (h "0k4snfn6hv7dvk44yh23cxr65izsmd2i4bzjx3nzjaxxiqr7djmn")))

(define-public crate-bubblebabble-0.1.2 (c (n "bubblebabble") (v "0.1.2") (h "0sxnhhghnxwh5pg95dfpw1pc4dprihxzj832anzsixq5a4mkxaas")))

