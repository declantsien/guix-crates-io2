(define-module (crates-io bu bb bubblers) #:use-module (crates-io))

(define-public crate-bubblers-0.1.0 (c (n "bubblers") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "predicates") (r "^3.1.0") (d #t) (k 2)) (d (n "rustubble") (r "^0.1.3") (d #t) (k 0)))) (h "07n4xivwb98swxqrwa3g4ms594awzif04ia7496hbrq6r136xy8q")))

