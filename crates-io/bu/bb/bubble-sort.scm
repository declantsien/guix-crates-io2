(define-module (crates-io bu bb bubble-sort) #:use-module (crates-io))

(define-public crate-bubble-sort-0.1.0 (c (n "bubble-sort") (v "0.1.0") (h "0mp1l1a14clh3h639xwjrp9725qf8vk5y8d4zidmcf8c3gyq04dr")))

(define-public crate-bubble-sort-0.2.0 (c (n "bubble-sort") (v "0.2.0") (h "0jadzda5vlksv69jrs2j9yxl2i0lhlfqks0h66fkkarqg8pbf4s6")))

