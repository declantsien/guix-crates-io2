(define-module (crates-io bu mp bump2version) #:use-module (crates-io))

(define-public crate-bump2version-0.1.0 (c (n "bump2version") (v "0.1.0") (h "0khlldwyqgp0473jdkcxld11mijmi97mga75n5zh1ywx15shljgq")))

(define-public crate-bump2version-0.1.1 (c (n "bump2version") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0zmmp2x2h42b4zvp47qr3dr16jbpbs1i12wqg8j9r600g1vi6m7x")))

(define-public crate-bump2version-0.1.2 (c (n "bump2version") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1n0xzr4rqx3ri8h7avd9bcrgmhy4x8b2gx2732wl2pqlxc6vclqm")))

(define-public crate-bump2version-0.1.3 (c (n "bump2version") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "055g32z34qssmfcr27lrxflfwlr3x5hvj8i25y0xdda4lg7scivv")))

