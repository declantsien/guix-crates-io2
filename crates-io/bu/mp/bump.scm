(define-module (crates-io bu mp bump) #:use-module (crates-io))

(define-public crate-bump-0.0.0 (c (n "bump") (v "0.0.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "filesystem") (r "^0.4.0") (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0c17bqaf7fxbkfrn1l957xk6lj2n31gq23njfnd6cabiwiw5a0px")))

