(define-module (crates-io bu mp bump-into) #:use-module (crates-io))

(define-public crate-bump-into-0.1.0 (c (n "bump-into") (v "0.1.0") (h "13rwba87b4h85rkpy0582p1l5r92x3m4xp9qxc8d1yn528ssw21m")))

(define-public crate-bump-into-0.1.1 (c (n "bump-into") (v "0.1.1") (h "0mih47zvgal4nw4myjf2rvnvl5d7r5509w86qrs57slnmdbnn3m3")))

(define-public crate-bump-into-0.1.2 (c (n "bump-into") (v "0.1.2") (h "0xckw9cgjsbgxqqdh48ffs9q7f1110kydyj1gy4i2pfhkcnsp50x")))

(define-public crate-bump-into-0.1.3 (c (n "bump-into") (v "0.1.3") (h "1b2q6m43flv5xw3n252zqiqnz91b4zi93kh0xcssifamp6kzff3g")))

(define-public crate-bump-into-0.1.4 (c (n "bump-into") (v "0.1.4") (h "1q1c8rplcsn4m27zslrb0zac7f09z5lv675hgmml1rnk8b9lkry4")))

(define-public crate-bump-into-0.2.0 (c (n "bump-into") (v "0.2.0") (h "0rqxn4dwiyh9q6afsb5kbhivyp95rkil38vn0ga5iql7ikjc069j")))

(define-public crate-bump-into-0.3.0 (c (n "bump-into") (v "0.3.0") (h "1qd57pqhqrlnmpz9gk22ny254nndi1bc59iwxrcfwr085lyi3spp")))

(define-public crate-bump-into-0.4.0 (c (n "bump-into") (v "0.4.0") (h "15516587h4ryqaqy34asfmxfzirrgwrqsvnk73x3g9hcbv7plizp")))

(define-public crate-bump-into-0.4.1 (c (n "bump-into") (v "0.4.1") (h "0y6pvs37k65hyn9x4zq2j9zcqfnxwhnkia43ig6cch0qgpycph68")))

(define-public crate-bump-into-0.4.2 (c (n "bump-into") (v "0.4.2") (h "1w1ah966k9z4wv74xyrsql6s8igdcg187jis7blhywbr4w17fsn1")))

(define-public crate-bump-into-0.4.3 (c (n "bump-into") (v "0.4.3") (h "0sklmm5xf6h89lnr40xw3cinsq80j2wygscrv10kms26qxqhqirr")))

(define-public crate-bump-into-0.4.4 (c (n "bump-into") (v "0.4.4") (h "1hs9lliglnrkl3cx3n7sdhajk9riq2g4ywq6gq4piihb4zwiq4jh")))

(define-public crate-bump-into-0.5.0 (c (n "bump-into") (v "0.5.0") (h "1bm3ba423l1z66drc7rg8pjfbnbqyibgzbrj5n225cx5ipy4nl7k")))

(define-public crate-bump-into-0.5.1 (c (n "bump-into") (v "0.5.1") (h "0ifvfkalvxm40g5z0r0lzkzxc4j61bjpfwh7yzdixlgpg32vf2a9")))

(define-public crate-bump-into-0.5.2 (c (n "bump-into") (v "0.5.2") (h "1bz1ncsvscfdklqm64asypxzcnk6v8xjfwfc62mqg4jnd97vrggx")))

(define-public crate-bump-into-0.6.0 (c (n "bump-into") (v "0.6.0") (h "09a1kajy6nimvb4gcniza90r36ri89v5xlvayz9ygs0hdrq4786c")))

(define-public crate-bump-into-0.7.0 (c (n "bump-into") (v "0.7.0") (h "09yc5cbzbg9qsn8xr5z11k5bcsrgmm8m4r4p9x33g7141h1khpi4")))

(define-public crate-bump-into-0.7.1 (c (n "bump-into") (v "0.7.1") (h "1n2d9wb6xy2wbymqsgfrkisl7a2fgrwjri3m0g9mxij1dl9bmgs7")))

(define-public crate-bump-into-0.7.2 (c (n "bump-into") (v "0.7.2") (h "0rg075i9mrcfppwb10lrli7wby8w97vkzqgjarn65b2idrmq09k9")))

(define-public crate-bump-into-0.8.0 (c (n "bump-into") (v "0.8.0") (h "1k3gf0ynca9hqrz4z7xfvjvrhrswnvv4hkrzgymb4h0zdi59l1qb") (r "1.44")))

(define-public crate-bump-into-0.8.1 (c (n "bump-into") (v "0.8.1") (h "0n2ifckpb3yfjli1j5aww2mvq8kvmxz6h9kl2bp9899rj9g5c5ri") (r "1.44")))

(define-public crate-bump-into-0.8.2 (c (n "bump-into") (v "0.8.2") (h "0wfcyx1ccsa4n8i1qgp8q8b36zl4qipyghnlzf7jwhlrw63q7ziv") (r "1.44")))

(define-public crate-bump-into-0.8.3 (c (n "bump-into") (v "0.8.3") (h "0j08f396g014h2bw4cn8fqkh9fp9carli6j7i36r60g47zlhlq5z") (r "1.44")))

