(define-module (crates-io bu mp bumpslab) #:use-module (crates-io))

(define-public crate-bumpslab-0.1.0 (c (n "bumpslab") (v "0.1.0") (d (list (d (n "bumpalo") (r "^3.11.1") (d #t) (k 0)))) (h "0a30lqvb7cnj6gafxwwipy827yfigb7is96fm83k0vvjyhh4ap6k")))

(define-public crate-bumpslab-0.2.0 (c (n "bumpslab") (v "0.2.0") (d (list (d (n "bumpalo") (r "^3.11.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "07lnvm2ly2vhzp5hqb4lsyqcmp2rc7a29yjrsxk9h2xmfp41cn3y")))

