(define-module (crates-io bu mp bump-allocator) #:use-module (crates-io))

(define-public crate-bump-allocator-0.1.0 (c (n "bump-allocator") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "0iskiwah469r7awa7zd6zsas9gmfpj4dcnw4628dcmd89y4d7cnr")))

(define-public crate-bump-allocator-0.1.1 (c (n "bump-allocator") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "0dwrbsqmnq5347g0bxipw59ww6zsnag7kp9n6f4zppjxmzakb7f0")))

(define-public crate-bump-allocator-0.1.2 (c (n "bump-allocator") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "1ckfz6s2mfsqq8fvxhfpffwih0acvk5m5jlb84lws54q6y9vzvdj")))

