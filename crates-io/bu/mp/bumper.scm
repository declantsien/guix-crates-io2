(define-module (crates-io bu mp bumper) #:use-module (crates-io))

(define-public crate-bumper-0.5.0 (c (n "bumper") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "cargo-edit") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pandoc") (r "^0.8.4") (d #t) (k 0)) (d (n "pandoc_ast") (r "^0.8.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.0") (d #t) (k 0)))) (h "02zfps798frqis5k1mk293d87941249pkc5s5cb2mlfs3ypjamyn")))

