(define-module (crates-io bu mp bumpalo-herd) #:use-module (crates-io))

(define-public crate-bumpalo-herd-0.1.0 (c (n "bumpalo-herd") (v "0.1.0") (d (list (d (n "bumpalo") (r "~3") (d #t) (k 0)) (d (n "bumpalo") (r "~3") (f (quote ("collections"))) (d #t) (k 2)) (d (n "crossbeam-utils") (r "~0.7") (d #t) (k 2)) (d (n "num_cpus") (r "~1") (d #t) (k 2)) (d (n "rayon") (r "~1") (d #t) (k 2)) (d (n "version-sync") (r "~0.8") (d #t) (k 2)))) (h "1zfrh5h3gkgx39i3qg8i301vg89ic96qpm25vsji7h307ys2j347")))

(define-public crate-bumpalo-herd-0.1.1 (c (n "bumpalo-herd") (v "0.1.1") (d (list (d (n "bumpalo") (r "~3") (d #t) (k 0)) (d (n "bumpalo") (r "~3") (f (quote ("collections"))) (d #t) (k 2)) (d (n "crossbeam-utils") (r "~0.7") (d #t) (k 2)) (d (n "num_cpus") (r "~1") (d #t) (k 2)) (d (n "rayon") (r "~1") (d #t) (k 2)) (d (n "version-sync") (r "~0.8") (d #t) (k 2)))) (h "0p4grkkx3wnhp5hf7g3f9vcd65r41pld1ha3gssbcbzpbk2hh218")))

(define-public crate-bumpalo-herd-0.1.2 (c (n "bumpalo-herd") (v "0.1.2") (d (list (d (n "bumpalo") (r "~3") (d #t) (k 0)) (d (n "bumpalo") (r "~3") (f (quote ("collections"))) (d #t) (k 2)) (d (n "crossbeam-utils") (r "~0.7") (d #t) (k 2)) (d (n "num_cpus") (r "~1") (d #t) (k 2)) (d (n "rayon") (r "~1") (d #t) (k 2)))) (h "15bw2cdwhg5gmi0ra8hh79rj14bcjr2w1zhnrhbr6ifk0bpbf6n5")))

