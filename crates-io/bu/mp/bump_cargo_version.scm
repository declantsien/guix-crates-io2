(define-module (crates-io bu mp bump_cargo_version) #:use-module (crates-io))

(define-public crate-bump_cargo_version-0.1.0 (c (n "bump_cargo_version") (v "0.1.0") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "tsu") (r "^1.0.1") (d #t) (k 0)))) (h "08rbz4253b9sdgwwdajzr4877vlhqqddhdysr0fn00mm139ib97w")))

(define-public crate-bump_cargo_version-0.1.1 (c (n "bump_cargo_version") (v "0.1.1") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (f (quote ("indexmap"))) (d #t) (k 0)) (d (n "tsu") (r "^1.0.1") (d #t) (k 0)))) (h "11nd62rga09l6jk4s0c4j9rla45iq78sagjigc62d5n7v1s39izn")))

(define-public crate-bump_cargo_version-0.1.2 (c (n "bump_cargo_version") (v "0.1.2") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "tsu") (r "^1.0.1") (d #t) (k 0)))) (h "0a3j22caxh8zn6jr5ja2b7z6ksg3605y22lid5wfbjg4nqvmns5f")))

(define-public crate-bump_cargo_version-0.1.3 (c (n "bump_cargo_version") (v "0.1.3") (d (list (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "tsu") (r "^1.0.1") (d #t) (k 0)))) (h "0bj5yjw3b7492vgwqghwpwf01mvalvz8r8lkzv69vda24i5vj7mr")))

