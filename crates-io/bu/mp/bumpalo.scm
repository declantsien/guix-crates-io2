(define-module (crates-io bu mp bumpalo) #:use-module (crates-io))

(define-public crate-bumpalo-1.0.0 (c (n "bumpalo") (v "1.0.0") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)))) (h "07xvq52hl5jha1yhp7bz6hyf5m930shr205d5cghrsl7c4zizfr2") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-bumpalo-1.0.1 (c (n "bumpalo") (v "1.0.1") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)))) (h "0s2gxncj79fg41s0jvc6ljw8jqrnygww2nr52h9w7z1pi595nqzh") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-bumpalo-1.0.2 (c (n "bumpalo") (v "1.0.2") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)))) (h "1vn6jlidgffrvkax9ymfq81x6vf1985avj8qb4wyd5h78y198889") (f (quote (("std") ("default" "std"))))))

(define-public crate-bumpalo-1.1.0 (c (n "bumpalo") (v "1.1.0") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)))) (h "0i4nkvv25bbp0asgs212iqm6qf9i53rn1rkp34j0pimnf1gcnkzw") (f (quote (("std") ("default" "collections" "std") ("collections" "std"))))))

(define-public crate-bumpalo-1.2.0 (c (n "bumpalo") (v "1.2.0") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)))) (h "1xrgifsfl668b8jfr53b3ia255vjcdwivciwgv006pqrlkbjpniq") (f (quote (("std") ("default" "collections" "std") ("collections" "std"))))))

(define-public crate-bumpalo-2.0.0 (c (n "bumpalo") (v "2.0.0") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)))) (h "0rb183fkp89mlr5zbxrh75ji8lxxk5b7ddqav2qfkj9c3dzxr8mz") (f (quote (("std") ("default" "collections" "std") ("collections" "std"))))))

(define-public crate-bumpalo-2.1.0 (c (n "bumpalo") (v "2.1.0") (d (list (d (n "criterion") (r "^0.2.5") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)))) (h "12nmfpw76vb7xjq5ligpq2sxfikb905yz8syymq76dqa4h4j3vr1") (f (quote (("std") ("default" "collections" "std") ("collections" "std"))))))

(define-public crate-bumpalo-2.2.0 (c (n "bumpalo") (v "2.2.0") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)))) (h "1771qls27dgq3nsc98cssmazymd8sdim7lwni4ndqs5x8vcw6i72") (f (quote (("std") ("default" "collections" "std") ("collections" "std")))) (y #t)))

(define-public crate-bumpalo-2.2.1 (c (n "bumpalo") (v "2.2.1") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)))) (h "1np7yq5yd1q2wa4gk23i16bq8vzr01xvsal1zp3j7l8pbzi9k810") (f (quote (("std") ("default" "collections" "std") ("collections" "std")))) (y #t)))

(define-public crate-bumpalo-2.2.2 (c (n "bumpalo") (v "2.2.2") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)))) (h "0vn512shcyp687y2w5v4nxq3jbyyhjyv3fwxpnsnkgb9r1hpgg90") (f (quote (("std") ("default" "collections" "std") ("collections" "std"))))))

(define-public crate-bumpalo-2.3.0 (c (n "bumpalo") (v "2.3.0") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)))) (h "1i3kcqc42mbimrf94mgf9hbbqv78vylfnz3q23wf5c9mm4mvr9pn") (f (quote (("std") ("default" "collections" "std") ("collections" "std"))))))

(define-public crate-bumpalo-2.4.0 (c (n "bumpalo") (v "2.4.0") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)))) (h "0mswkzspwii1f4c7wzv4synnyxhb2g64dvzik8wh2dzrixllaaia") (f (quote (("std") ("default" "collections" "std") ("collections" "std"))))))

(define-public crate-bumpalo-2.4.1 (c (n "bumpalo") (v "2.4.1") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)))) (h "07z77b1x6c4gaa7n9znjr76mr69q114h412fcd20a2a8w05p4fa6") (f (quote (("std") ("default" "collections" "std") ("collections" "std"))))))

(define-public crate-bumpalo-2.4.2 (c (n "bumpalo") (v "2.4.2") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)))) (h "1md5qidv47b8rmaccjmz5f7wf6p3hxdja99lab8ljy5m3d04k7r9") (f (quote (("std") ("default" "collections" "std") ("collections" "std"))))))

(define-public crate-bumpalo-2.4.3 (c (n "bumpalo") (v "2.4.3") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)))) (h "19bsqc7bvw1ynzv0rwgrrnrn6q4inkjn6ycbh4k9a6z0v2ps7p44") (f (quote (("std") ("default" "collections" "std") ("collections" "std"))))))

(define-public crate-bumpalo-2.5.0 (c (n "bumpalo") (v "2.5.0") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)))) (h "018b5calz3895v04shk9bn7i73r4zf8yf7p1dqg92s3xya13vm1c") (f (quote (("std") ("default" "collections" "std") ("collections" "std"))))))

(define-public crate-bumpalo-2.6.0 (c (n "bumpalo") (v "2.6.0") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)))) (h "020psxs5dlm0gdbs83rx2rcavibdshdr0fpzk3mmw65zq8ppz05d") (f (quote (("std") ("default" "collections" "std") ("collections" "std"))))))

(define-public crate-bumpalo-3.0.0 (c (n "bumpalo") (v "3.0.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)))) (h "0pf3plah9s9bafxznafwrbpakl9mb8jbqdib68840x03yr166qy2") (f (quote (("default") ("collections")))) (y #t)))

(define-public crate-bumpalo-3.1.0 (c (n "bumpalo") (v "3.1.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)))) (h "1jkvwg41r3y3b6rf2mw78qb31g2w3xxvjshghmgl7gay8r9pww7n") (f (quote (("default") ("collections")))) (y #t)))

(define-public crate-bumpalo-3.1.1 (c (n "bumpalo") (v "3.1.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)))) (h "171nyj1bvi9b7g46lg1ravysqwsndlc9x8wswfsfsflaimx5dqlg") (f (quote (("default") ("collections")))) (y #t)))

(define-public crate-bumpalo-3.1.2 (c (n "bumpalo") (v "3.1.2") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)))) (h "1x1j6lgaidayfvg7yh9ysdpfjlaiqx60y4vvg1rmy2nw3n607f2z") (f (quote (("default") ("collections")))) (y #t)))

(define-public crate-bumpalo-3.2.0 (c (n "bumpalo") (v "3.2.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)))) (h "0hpp4wfcn04gnl1ji4a80b85xwknsci81xqyllq174gq9z0rsd8z") (f (quote (("default") ("collections")))) (y #t)))

(define-public crate-bumpalo-3.2.1 (c (n "bumpalo") (v "3.2.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)))) (h "11silgpsnfv6ir7j2nh7a69564f92vq20k9ha7zcbynpiav9vbhj") (f (quote (("default") ("collections"))))))

(define-public crate-bumpalo-3.3.0 (c (n "bumpalo") (v "3.3.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1xkqmd4id9qxw4n43chg1im0ynzxy2jx3lanb9w1yjp27v9g2mjk") (f (quote (("default") ("collections"))))))

(define-public crate-bumpalo-3.4.0 (c (n "bumpalo") (v "3.4.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "082qig1vxlklb9qwkg3j4hpfxb4b5blm59ln21njfc2p01zhi31f") (f (quote (("default") ("collections") ("boxed"))))))

(define-public crate-bumpalo-3.5.0 (c (n "bumpalo") (v "3.5.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0n8dfj0caarpq409anz4d62idqgz9nlvc1q3pshkj93hiilacyph") (f (quote (("default") ("collections") ("boxed") ("allocator_api"))))))

(define-public crate-bumpalo-3.6.0 (c (n "bumpalo") (v "3.6.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1nfm0nhd7pz4z16v9fcs52zs37j1sc6vixhnj0fp4ja3y5p5k7h9") (f (quote (("default") ("collections") ("boxed") ("allocator_api"))))))

(define-public crate-bumpalo-3.6.1 (c (n "bumpalo") (v "3.6.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1gndni6ng6z1v14lq5zgm1k2y9320w4bc2ijzgyz9qwx9f56nfb3") (f (quote (("default") ("collections") ("boxed") ("allocator_api"))))))

(define-public crate-bumpalo-3.7.0 (c (n "bumpalo") (v "3.7.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0ccn025n0x1gc0ijjlabin2xs7dkx5yfagkskr93yw9c06pyfncw") (f (quote (("default") ("collections") ("boxed") ("allocator_api"))))))

(define-public crate-bumpalo-3.7.1 (c (n "bumpalo") (v "3.7.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0f55q7915m5dx5cgn5cli72z0gk18nf2757rd63lky4ypzvngpyr") (f (quote (("default") ("collections") ("boxed") ("allocator_api"))))))

(define-public crate-bumpalo-3.8.0 (c (n "bumpalo") (v "3.8.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0v5vck40wmz95d48gq0z3al32vy1yic8fihjkb0sfh4h7862c7lg") (f (quote (("default") ("collections") ("boxed") ("allocator_api"))))))

(define-public crate-bumpalo-3.9.0 (c (n "bumpalo") (v "3.9.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "182v3y0wbvmnncfzwaij54f4j2hbwf0ysm11265bh3rb5yfqqhzy") (f (quote (("default") ("collections") ("boxed") ("allocator_api"))))))

(define-public crate-bumpalo-3.9.1 (c (n "bumpalo") (v "3.9.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1688dv6s0cbj72p9lmll8a02a85dzxvdw2is7pji490zmd35m954") (f (quote (("default") ("collections") ("boxed") ("allocator_api"))))))

(define-public crate-bumpalo-3.10.0 (c (n "bumpalo") (v "3.10.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1qrx6sg13yxljk1yr705j5wg34iiy3531by1hqrpiihl8qhvvk1p") (f (quote (("default") ("collections") ("boxed") ("allocator_api"))))))

(define-public crate-bumpalo-3.11.0 (c (n "bumpalo") (v "3.11.0") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0pcfy89hpmixns4qqd4swbhsnvn3mkah0w229wijq3fj30hq5bf1") (f (quote (("default") ("collections") ("boxed") ("allocator_api"))))))

(define-public crate-bumpalo-3.11.1 (c (n "bumpalo") (v "3.11.1") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1fl072w8wia496byc2h6ck2159sir2jjrb8niwq8h4916r8njbsp") (f (quote (("default") ("collections") ("boxed") ("allocator_api"))))))

(define-public crate-bumpalo-3.12.0 (c (n "bumpalo") (v "3.12.0") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0damxqdgqqzp3zyfwvbrg5hzx39kqgxnxl3yyq3kk4ald0jiw9hd") (f (quote (("default") ("collections") ("boxed") ("allocator_api"))))))

(define-public crate-bumpalo-3.12.1 (c (n "bumpalo") (v "3.12.1") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1j7hjszs00lgl1ddwg4369f4jh87cbpf1m3xzczz751n0scy274v") (f (quote (("default") ("collections") ("boxed") ("allocator_api")))) (y #t)))

(define-public crate-bumpalo-3.12.2 (c (n "bumpalo") (v "3.12.2") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0fy6fkz4x58jdzm6r07ym264bnfpq1gc210vvl90rzzck17djviw") (f (quote (("default") ("collections") ("boxed") ("allocator_api")))) (r "1.60.0")))

(define-public crate-bumpalo-3.13.0 (c (n "bumpalo") (v "3.13.0") (d (list (d (n "allocator-api2") (r "^0.2.8") (o #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1h9zmxb9d14m2sx34daz88fsjw1lx7d5mhaqbldwqgl8xzdc7qm3") (f (quote (("default") ("collections") ("boxed") ("allocator_api")))) (r "1.60.0")))

(define-public crate-bumpalo-3.14.0 (c (n "bumpalo") (v "3.14.0") (d (list (d (n "allocator-api2") (r "^0.2.8") (o #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1v4arnv9kwk54v5d0qqpv4vyw2sgr660nk0w3apzixi1cm3yfc3z") (f (quote (("std") ("default") ("collections") ("boxed") ("allocator_api")))) (r "1.60.0")))

(define-public crate-bumpalo-3.15.0 (c (n "bumpalo") (v "3.15.0") (d (list (d (n "allocator-api2") (r "^0.2.8") (o #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "07vd0xsx7p4kyq3lfz73vmx5xw3388vjlqb3nbch38iw5d69jank") (f (quote (("std") ("default") ("collections") ("boxed") ("allocator_api")))) (y #t) (r "1.65.0")))

(define-public crate-bumpalo-3.15.1 (c (n "bumpalo") (v "3.15.1") (d (list (d (n "allocator-api2") (r "^0.2.8") (o #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1dr5kxavcy9dcqv608qmpd2g0xr5z5xbsdwv0vrwpz3qr8cxcr67") (f (quote (("std") ("default") ("collections") ("boxed") ("allocator_api")))) (r "1.73.0")))

(define-public crate-bumpalo-3.15.2 (c (n "bumpalo") (v "3.15.2") (d (list (d (n "allocator-api2") (r "^0.2.8") (o #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0c7i2m28wqriaq1kcjj7dim0yxp8n5mydjssl3dh2igff9vvxcd3") (f (quote (("std") ("default") ("collections") ("boxed") ("allocator_api")))) (r "1.73.0")))

(define-public crate-bumpalo-3.15.3 (c (n "bumpalo") (v "3.15.3") (d (list (d (n "allocator-api2") (r "^0.2.8") (o #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0nsm985rzdcnx8lmy9px1179f8yc8jarg5n8aw8jldmvf6m898cf") (f (quote (("std") ("default") ("collections") ("boxed") ("allocator_api")))) (r "1.73.0")))

(define-public crate-bumpalo-3.15.4 (c (n "bumpalo") (v "3.15.4") (d (list (d (n "allocator-api2") (r "^0.2.8") (o #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1ahfhgw2lzlgv5j0h07z8mkdnk4kvl2grf8dkb32dm4zsjfrpxkz") (f (quote (("std") ("default") ("collections") ("boxed") ("allocator_api")))) (r "1.73.0")))

(define-public crate-bumpalo-3.16.0 (c (n "bumpalo") (v "3.16.0") (d (list (d (n "allocator-api2") (r "^0.2.8") (o #t) (k 0)) (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.171") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 2)))) (h "0b015qb4knwanbdlp1x48pkb4pm57b8gidbhhhxr900q2wb6fabr") (f (quote (("std") ("default") ("collections") ("boxed") ("allocator_api")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.73.0")))

