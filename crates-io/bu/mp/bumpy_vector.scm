(define-module (crates-io bu mp bumpy_vector) #:use-module (crates-io))

(define-public crate-bumpy_vector-0.0.0 (c (n "bumpy_vector") (v "0.0.0") (d (list (d (n "pretty_assertions") (r "~0.6.1") (d #t) (k 2)) (d (n "ron") (r "~0.5.1") (d #t) (k 0)) (d (n "serde") (r "~1.0.110") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jff4zwm41xdbbv5hw6z5asdlcklk50xns0yl4gy4bw4cicdqspb")))

(define-public crate-bumpy_vector-0.0.1 (c (n "bumpy_vector") (v "0.0.1") (d (list (d (n "pretty_assertions") (r "~0.6.1") (d #t) (k 2)) (d (n "ron") (r "~0.5.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1.0.110") (o #t) (d #t) (k 0)))) (h "05z6wn1wycqp886xs38l22hi3vd39gk0dvk9vm6h3sfinrnqgi4l") (f (quote (("serialize" "serde" "ron"))))))

(define-public crate-bumpy_vector-0.0.2 (c (n "bumpy_vector") (v "0.0.2") (d (list (d (n "pretty_assertions") (r "~0.6.1") (d #t) (k 2)) (d (n "ron") (r "~0.5.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1.0.110") (o #t) (d #t) (k 0)))) (h "114jvjqlr65ihz65pmvh5cc86i8q97qfpibxiqvxpswr7pm6xikm") (f (quote (("serialize" "serde" "ron"))))))

(define-public crate-bumpy_vector-0.0.3 (c (n "bumpy_vector") (v "0.0.3") (d (list (d (n "pretty_assertions") (r "~0.6.1") (d #t) (k 2)) (d (n "ron") (r "~0.5.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1.0.110") (o #t) (d #t) (k 0)))) (h "0qfv2lidqy7sc65mvaz6f8xb83ld3x4968vdr4xvsiali44c3gv4") (f (quote (("serialize" "serde" "ron"))))))

(define-public crate-bumpy_vector-0.0.4 (c (n "bumpy_vector") (v "0.0.4") (d (list (d (n "pretty_assertions") (r "~0.6.1") (d #t) (k 2)) (d (n "ron") (r "~0.5.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "~1.0.110") (o #t) (d #t) (k 0)))) (h "0kzm7xbyy5s5lq482d2vpci2fpg0xbz163hyngg746jg86gaa764") (f (quote (("serialize" "serde" "ron"))))))

