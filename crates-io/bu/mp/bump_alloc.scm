(define-module (crates-io bu mp bump_alloc) #:use-module (crates-io))

(define-public crate-bump_alloc-0.1.0 (c (n "bump_alloc") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.46") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("minwindef" "winnt"))) (d #t) (k 0)))) (h "0dwbml07azma23aa2wc5l3sgckxj5ja4vnkm3cymj27b3biwwwh2")))

