(define-module (crates-io bu mp bump-api) #:use-module (crates-io))

(define-public crate-bump-api-0.1.0 (c (n "bump-api") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0bchxv0zzwbcdj2n8sswfy5b7hjwmwxzm1nb872iws51mx99qfyz")))

(define-public crate-bump-api-2.0.0 (c (n "bump-api") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "httpclient") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0cff17zaxz0p9d3hxv5msw051zbrx41s1wrj9m72rc4d5mkcmr97")))

