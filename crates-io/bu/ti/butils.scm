(define-module (crates-io bu ti butils) #:use-module (crates-io))

(define-public crate-butils-0.0.1 (c (n "butils") (v "0.0.1") (h "17yh49lcaw3wyc8vc7djajfk85m2lpin83hvqxwpidna3hr7gh0d")))

(define-public crate-butils-0.1.0 (c (n "butils") (v "0.1.0") (h "0sgbddp1dalg63j8sib9xhhmr9yxc0jdnfrwgkajx3bmq468hppw")))

