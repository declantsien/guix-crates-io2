(define-module (crates-io bu tl butlerd) #:use-module (crates-io))

(define-public crate-butlerd-0.1.0 (c (n "butlerd") (v "0.1.0") (d (list (d (n "hyper") (r "^0.12.10") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.78") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.78") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)))) (h "1fqcmk3y57yy6x37s0z019j20mfcy9xylr63vk7mc2pyrhdlv1d5")))

(define-public crate-butlerd-0.1.1 (c (n "butlerd") (v "0.1.1") (d (list (d (n "hyper") (r "^0.12.10") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.78") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.78") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)))) (h "1b6gkcc2pwwvi62h7s79wpvbi0rpy8vvsx6vxfhi4h7aj1ds5lkg")))

(define-public crate-butlerd-0.1.2 (c (n "butlerd") (v "0.1.2") (d (list (d (n "hyper") (r "^0.12.10") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.78") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.78") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.27") (d #t) (k 0)))) (h "028acmgb8bpymapl155v5s83r3kvqr21mr8mhb4s3q39dgmrjn09")))

