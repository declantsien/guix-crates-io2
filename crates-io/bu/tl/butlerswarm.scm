(define-module (crates-io bu tl butlerswarm) #:use-module (crates-io))

(define-public crate-butlerswarm-0.8.0 (c (n "butlerswarm") (v "0.8.0") (d (list (d (n "serde") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.132") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1mi3s9x9lmprb6l5zg2y8xbccc5wcavx0mx1jy6qpnl6idaz7fnb")))

(define-public crate-butlerswarm-0.9.0 (c (n "butlerswarm") (v "0.9.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.132") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0sn7j90yszfb6cv57a74kn3nzc2afy4vhs9mwl02kdvgq07rwkxb")))

(define-public crate-butlerswarm-0.9.1 (c (n "butlerswarm") (v "0.9.1") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.132") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "11lzwyv1s8p036rilani517hi10xyl1v3gbg3wbhycjrjmxzwvpf")))

