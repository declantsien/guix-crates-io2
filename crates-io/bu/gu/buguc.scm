(define-module (crates-io bu gu buguc) #:use-module (crates-io))

(define-public crate-buguc-0.1.0 (c (n "buguc") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lib-rv32-asm") (r "^0.2.0") (d #t) (k 0)))) (h "0cxmb7lr2v7ixi3kmvx2friidsnsr2xns47bxfaa3baa080vh6pd")))

(define-public crate-buguc-0.2.0 (c (n "buguc") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)))) (h "0c01q0ybzglxg5c0abrpbwp0wvd5w09j4dh2abq93x206fjgx22g")))

(define-public crate-buguc-0.2.1 (c (n "buguc") (v "0.2.1") (h "03ssi1cc680srk82pm8frwkj3767vhq2vd55q7w30a5ma3qsa5mj")))

(define-public crate-buguc-0.3.0 (c (n "buguc") (v "0.3.0") (h "1xq9gph7hqc7sb5nn8407qgv794pvn4cgb9q237s0ls71w59x4cx")))

