(define-module (crates-io bu jo bujo) #:use-module (crates-io))

(define-public crate-bujo-0.1.0 (c (n "bujo") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete_command") (r "^0.5.1") (f (quote ("carapace"))) (d #t) (k 0)) (d (n "clap_derive") (r "^4.5.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "time") (r "^0.3.34") (f (quote ("serde" "local-offset" "serde-well-known"))) (d #t) (k 0)))) (h "0h86ahca54vr6b3dvpd6j5zhjfv283rmmxwy9iwpd1jcv1fbypj9")))

