(define-module (crates-io bu tt buttplug_derive) #:use-module (crates-io))

(define-public crate-buttplug_derive-0.0.1 (c (n "buttplug_derive") (v "0.0.1") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "09vaqypp2d4pczfxch36jqcilkjpssm3qh14m1bsr1hvd9rjc9nr")))

(define-public crate-buttplug_derive-0.1.0 (c (n "buttplug_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.14") (d #t) (k 0)))) (h "1acpagrff5vykhrwhhix87zlr330wh15vy0pzgvygmnmf2zrqkbw")))

(define-public crate-buttplug_derive-0.2.0 (c (n "buttplug_derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.16") (d #t) (k 0)))) (h "1ras62yk3p3ilxxjprcw1zrz25vqwwsr2pzr74w31y90skykng38")))

(define-public crate-buttplug_derive-0.3.0 (c (n "buttplug_derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "0alclqdk63qxch9pl7hlizyc3n1nr9v5ckwpy9k8g5szwzlfbnvg")))

(define-public crate-buttplug_derive-0.4.0 (c (n "buttplug_derive") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "0iig8hgq0r15wcjc9xq0dmffw9aqjx0i6xvqg0m7bvl281hx9kgv")))

(define-public crate-buttplug_derive-0.5.0 (c (n "buttplug_derive") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "02n9jj9551xdsgh5pq6d7ch5mxjkyylv0jyygdr5a0zpvxjgk1g5")))

(define-public crate-buttplug_derive-0.6.0 (c (n "buttplug_derive") (v "0.6.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (d #t) (k 0)))) (h "02yjppr4k40l4aj5d4jxs2kzg0a1ccxj605zrp1f7z0wpz5j4dsn")))

(define-public crate-buttplug_derive-0.6.1 (c (n "buttplug_derive") (v "0.6.1") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (d #t) (k 0)))) (h "13c4fm2c3cwnynnq10zfswmyfv32nmfifv02qf6b79xb23p41zhz")))

(define-public crate-buttplug_derive-0.6.2 (c (n "buttplug_derive") (v "0.6.2") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (d #t) (k 0)))) (h "1hkni8frx65vlgszda77aqgf0hxcqsz62qyp5cqraj5y1ncw7hyp")))

(define-public crate-buttplug_derive-0.7.0 (c (n "buttplug_derive") (v "0.7.0") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.58") (d #t) (k 0)))) (h "1lifmik17pp0cah9jkqccz0qa62cgc5x5gp3dal3y846bv3g9bq3")))

(define-public crate-buttplug_derive-0.8.0 (c (n "buttplug_derive") (v "0.8.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "19b3nslj0gf8fbkr15virr13x7zbk8h9hzg5wjwjak0j2i6nglyi")))

