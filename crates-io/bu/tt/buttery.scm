(define-module (crates-io bu tt buttery) #:use-module (crates-io))

(define-public crate-buttery-0.1.0 (c (n "buttery") (v "0.1.0") (d (list (d (n "glam") (r "^0.22.0") (d #t) (k 0)))) (h "17vkpylwgcfj515z2xp0lzdsm0yd409ds0mj64x6xizwi3q9h885")))

(define-public crate-buttery-0.1.1 (c (n "buttery") (v "0.1.1") (d (list (d (n "glam") (r "^0.22.0") (d #t) (k 0)))) (h "1liyxnk9sgqd1kd1fp1v50gc3cy2yvhh9riv1pgwhmmr0lm5grcl")))

(define-public crate-buttery-0.2.0 (c (n "buttery") (v "0.2.0") (d (list (d (n "glam") (r "^0.24.0") (d #t) (k 0)))) (h "1zjf91diwawjil3yah9wbk7sc518mpv5hp0svlbaydy6r3p6vq7i")))

