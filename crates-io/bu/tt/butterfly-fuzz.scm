(define-module (crates-io bu tt butterfly-fuzz) #:use-module (crates-io))

(define-public crate-butterfly-fuzz-0.1.0 (c (n "butterfly-fuzz") (v "0.1.0") (d (list (d (n "libafl") (r "^0.8") (d #t) (k 0)) (d (n "pcap") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1fflk28c41wjazvsji18bq9qjxyb0ds881phygzlbs4v789aq4s1")))

(define-public crate-butterfly-fuzz-0.1.1 (c (n "butterfly-fuzz") (v "0.1.1") (d (list (d (n "libafl") (r "^0.8") (d #t) (k 0)) (d (n "pcap") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1pmlvqcdw9rnczflrmzi9gpnqr7k7grgn54jys2r9myj93ilznds") (f (quote (("docs-rs" "pcap/docs-rs") ("default"))))))

(define-public crate-butterfly-fuzz-0.2.0 (c (n "butterfly-fuzz") (v "0.2.0") (d (list (d (n "libafl") (r "^0.8") (d #t) (k 0)) (d (n "pcap") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1ibmj7klyzyffrxc4d79wvlw2ns4jgxap8lp93641n51cw77kydp") (f (quote (("docs-rs" "pcap/docs-rs") ("default"))))))

(define-public crate-butterfly-fuzz-0.2.1 (c (n "butterfly-fuzz") (v "0.2.1") (d (list (d (n "ahash") (r "^0.7") (d #t) (k 0)) (d (n "libafl") (r "^0.8") (d #t) (k 0)) (d (n "pcap") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0hiydc1b5mkk9b20rj541bjga57g3hi1fdmxvlw7wh2gl0p8mq43") (f (quote (("graphviz") ("docs-rs" "pcap/docs-rs") ("default"))))))

(define-public crate-butterfly-fuzz-0.2.2 (c (n "butterfly-fuzz") (v "0.2.2") (d (list (d (n "ahash") (r "^0.7") (d #t) (k 0)) (d (n "libafl") (r "^0.8") (d #t) (k 0)) (d (n "pcap") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "13iv0qqi1dmxpl2hsl2qxrfd7paja4qvfgjwkxy0r51ai4fllnay") (f (quote (("safe_only") ("graphviz") ("docs-rs" "pcap/docs-rs") ("default"))))))

