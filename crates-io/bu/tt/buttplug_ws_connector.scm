(define-module (crates-io bu tt buttplug_ws_connector) #:use-module (crates-io))

(define-public crate-buttplug_ws_connector-0.0.1 (c (n "buttplug_ws_connector") (v "0.0.1") (d (list (d (n "async-std") (r "^0.99.11") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.17") (d #t) (k 0)) (d (n "buttplug") (r "^0.0.2-beta.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "futures-preview") (r "^0.3.0-alpha.19") (f (quote ("async-await"))) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ws") (r "^0.9.1") (d #t) (k 0)))) (h "081jr7nzfyq4x0x6vyhgs34id3ymjk1mrr407x5f168akk6572vw") (y #t)))

(define-public crate-buttplug_ws_connector-0.0.2 (c (n "buttplug_ws_connector") (v "0.0.2") (h "0b2h3hxm7i88civymwi42k68hl2kavaxxcwm2041yy4c0lk5c5mn") (y #t)))

(define-public crate-buttplug_ws_connector-0.0.3 (c (n "buttplug_ws_connector") (v "0.0.3") (h "1fk0gsrjlnjjjvxwdz3vn74rflwcb48sav1h483dwwg4r6p01kfx") (y #t)))

(define-public crate-buttplug_ws_connector-0.0.4 (c (n "buttplug_ws_connector") (v "0.0.4") (h "0wip5553fx9ybf5d43vv8mxd0lpnsw6mql08ycfsliaa5gzhp2x8") (y #t)))

