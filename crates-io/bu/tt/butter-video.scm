(define-module (crates-io bu tt butter-video) #:use-module (crates-io))

(define-public crate-butter-video-0.1.0 (c (n "butter-video") (v "0.1.0") (d (list (d (n "av-metrics-decoders") (r "^0.1.7") (f (quote ("ffmpeg"))) (k 0)) (d (n "average") (r "^0.13.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.0") (d #t) (k 0)) (d (n "image") (r "^0.24") (f (quote ("png"))) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "yuv") (r "^0.1.4") (d #t) (k 0)))) (h "1g57jgm1yg5zj6kxhbb1b1fvv762prldwgzhf1mbfv14ag4vc561")))

