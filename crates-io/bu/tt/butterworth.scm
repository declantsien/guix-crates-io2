(define-module (crates-io bu tt butterworth) #:use-module (crates-io))

(define-public crate-butterworth-0.0.1 (c (n "butterworth") (v "0.0.1") (h "07h59h4qf91jhcx3kbcbn4w30px8dckzfhqw4m0idfla4a1wi50s")))

(define-public crate-butterworth-0.1.0 (c (n "butterworth") (v "0.1.0") (d (list (d (n "num-complex") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0hf69iihx2n35c429wxylmk6w19gk0kvx1nh0kxgkzjmppwvgfsw")))

