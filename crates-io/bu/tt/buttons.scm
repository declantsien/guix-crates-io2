(define-module (crates-io bu tt buttons) #:use-module (crates-io))

(define-public crate-buttons-0.1.0 (c (n "buttons") (v "0.1.0") (d (list (d (n "winit") (r ">= 0.6.0") (o #t) (d #t) (k 0)))) (h "1fbn59hbg7qxr76r1cfb3k2w07nhwq4rd9vbs2azhki9chivbl6f") (f (quote (("winit-support" "winit") ("default"))))))

(define-public crate-buttons-0.1.1 (c (n "buttons") (v "0.1.1") (d (list (d (n "winit") (r ">= 0.6.0, < 0.20.0") (o #t) (d #t) (k 0)))) (h "09dlp5japzdb5ydh7s73jmxkx8irasxzrbz6jd57byajh39l3qyl") (f (quote (("winit-support" "winit") ("default"))))))

(define-public crate-buttons-0.2.0 (c (n "buttons") (v "0.2.0") (d (list (d (n "winit") (r "~0.21.0") (o #t) (d #t) (k 0)))) (h "16kmddqxip8vb0f1bpfg9xlbglbds69lq5qcg2z3cpvj8db8la5x") (f (quote (("winit-support" "winit") ("default"))))))

(define-public crate-buttons-0.3.0 (c (n "buttons") (v "0.3.0") (d (list (d (n "winit") (r "~0.23.0") (o #t) (d #t) (k 0)))) (h "1k6cs4p81q1ahb0vpsln5nci23nchdds4qafywyhj0cc5413z4ac") (f (quote (("winit-support" "winit") ("default")))) (y #t)))

(define-public crate-buttons-0.3.1 (c (n "buttons") (v "0.3.1") (d (list (d (n "winit") (r "~0.24.0") (o #t) (d #t) (k 0)))) (h "107k2svliwp74jsgf89rhs5w8w31v4j141z1x15r4ijzg0asddmn") (f (quote (("winit-support" "winit") ("default"))))))

(define-public crate-buttons-0.4.0 (c (n "buttons") (v "0.4.0") (d (list (d (n "winit") (r "~0.26.0") (o #t) (d #t) (k 0)) (d (n "winit") (r "~0.26.0") (d #t) (k 2)))) (h "1jiqfhz6h2zxlkcqp56m4d5i2ww8s941cp9n280iymmjbia8fazg") (f (quote (("default")))) (s 2) (e (quote (("winit" "dep:winit"))))))

(define-public crate-buttons-0.5.0 (c (n "buttons") (v "0.5.0") (d (list (d (n "winit") (r "^0.27") (o #t) (d #t) (k 0)) (d (n "winit") (r "^0.27") (d #t) (k 2)))) (h "0xp8w3mrnwl8h4i63bjsj8iwn327m6cmkgh9j9yp56w024lm1p7z") (f (quote (("default" "winit")))) (s 2) (e (quote (("winit" "dep:winit"))))))

