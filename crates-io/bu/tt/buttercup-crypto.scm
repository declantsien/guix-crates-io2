(define-module (crates-io bu tt buttercup-crypto) #:use-module (crates-io))

(define-public crate-buttercup-crypto-0.1.0 (c (n "buttercup-crypto") (v "0.1.0") (d (list (d (n "rust-crypto-wasm") (r "^0.2.36") (d #t) (k 0)))) (h "1cs5ybic831cplx3ldyijzcs4q2v24ym70rjr6cal05a2f8gvs0w")))

(define-public crate-buttercup-crypto-0.2.0 (c (n "buttercup-crypto") (v "0.2.0") (d (list (d (n "rust-crypto-wasm") (r "^0.3") (d #t) (k 0)))) (h "0173dm5jhasvacs3yxwk7ikjj2b0wmsxcajlc2c8r7xqjyifxmgm")))

(define-public crate-buttercup-crypto-0.3.0 (c (n "buttercup-crypto") (v "0.3.0") (d (list (d (n "aes-soft") (r "^0.1.0") (d #t) (k 0)) (d (n "base64") (r "^0.5") (d #t) (k 0)) (d (n "block-modes") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "hmac") (r "^0.6.2") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.1") (d #t) (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)))) (h "0snzxlil3n9rgbns8cynblflcdw7fpszbjih5ma2fz1bpx76zsz3")))

(define-public crate-buttercup-crypto-0.4.0 (c (n "buttercup-crypto") (v "0.4.0") (d (list (d (n "aes-soft") (r "^0.1.0") (d #t) (k 0)) (d (n "base64") (r "^0.5") (d #t) (k 0)) (d (n "block-modes") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "hmac") (r "^0.6.2") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.1") (d #t) (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)))) (h "197i87ikk1a2m6s5m6j5fx58bqs8ikfbrd5k36nk4pvpknn31axj")))

(define-public crate-buttercup-crypto-0.4.1 (c (n "buttercup-crypto") (v "0.4.1") (d (list (d (n "aes-soft") (r "^0.1.0") (d #t) (k 0)) (d (n "base64") (r "^0.5") (d #t) (k 0)) (d (n "block-modes") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "hmac") (r "^0.6.2") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.1") (d #t) (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)))) (h "0h91yxjvlxrzp8q0w6dmrvaadkmag0ai0f42h8256x5fn4a3jrqg")))

(define-public crate-buttercup-crypto-0.4.2 (c (n "buttercup-crypto") (v "0.4.2") (d (list (d (n "aes-soft") (r "^0.1.0") (d #t) (k 0)) (d (n "base64") (r "^0.5") (d #t) (k 0)) (d (n "block-modes") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "hmac") (r "^0.6.2") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.1") (d #t) (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)))) (h "1dhk7z870l3lm7qni4p68hx72lid1zsjxajj463qs0sr0l0s51xf")))

