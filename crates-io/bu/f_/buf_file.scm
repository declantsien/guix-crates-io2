(define-module (crates-io bu f_ buf_file) #:use-module (crates-io))

(define-public crate-buf_file-0.1.0 (c (n "buf_file") (v "0.1.0") (d (list (d (n "raw_serde") (r "^0.1.4") (d #t) (k 0)))) (h "1ps7zg1b85vfs59ml6mv84v4hzl55pskn34lld471jhkasx4xwd8") (y #t)))

(define-public crate-buf_file-0.1.1 (c (n "buf_file") (v "0.1.1") (d (list (d (n "raw_serde") (r "^0.1.4") (d #t) (k 0)))) (h "1f64860xyanrpygx28198nkilc737lwb9p5pajr8a0f5xyv23vjx") (y #t)))

(define-public crate-buf_file-0.2.1 (c (n "buf_file") (v "0.2.1") (h "0answ6z26jbhrsbjz89ly7iw427qxy2m6qd7px6y3z9926w789b3") (y #t)))

(define-public crate-buf_file-0.2.2 (c (n "buf_file") (v "0.2.2") (h "1iv9s2bfmjg4jz8mld52fbb0z1mrr998s3jmh6hpp06mhfaprjhv") (y #t)))

(define-public crate-buf_file-0.2.3 (c (n "buf_file") (v "0.2.3") (h "1mf0qzndmn0l5hws1r821nfm2vqw0xix55vvivl68nd10ihdp1ff") (y #t)))

(define-public crate-buf_file-0.2.4 (c (n "buf_file") (v "0.2.4") (h "18f2l5jbvb3nhkzwdrlwv6nz285ahi4c2ckj61f0lhy7s72hjnzl") (y #t)))

(define-public crate-buf_file-0.2.5 (c (n "buf_file") (v "0.2.5") (h "0fvmqr63m8g515kgk64xxpyylnjvlxhkxzzn6akd2b9mfqb89d6r") (y #t)))

(define-public crate-buf_file-0.2.6 (c (n "buf_file") (v "0.2.6") (h "19891qcm0gb23isyjhnvazci92bnw7hz73imym38s599y5gjcbvy") (y #t)))

(define-public crate-buf_file-0.2.7 (c (n "buf_file") (v "0.2.7") (h "0gqq3lcm7asq50cfr5fgnknnqqlqkwhsyh9pv44ppf9x6a2f06y6") (y #t)))

(define-public crate-buf_file-0.2.8 (c (n "buf_file") (v "0.2.8") (h "0fssqv52kxlyrmmsp00rmmr23vv8qngg4h308h2v0873m9p2n4yb") (y #t)))

(define-public crate-buf_file-0.2.9 (c (n "buf_file") (v "0.2.9") (h "0c2hxrfdr2884szxkynxjnwzmybjfj3z74rmjhpsl8lw5fxvakb1") (y #t)))

