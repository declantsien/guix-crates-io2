(define-module (crates-io bu f_ buf_stream) #:use-module (crates-io))

(define-public crate-buf_stream-0.2.0 (c (n "buf_stream") (v "0.2.0") (d (list (d (n "futures") (r "^0.1.13") (o #t) (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "05hgvh7wah38rk1l8n5vvb4c7lxsinxh1yiyqcahpgz3cpwcd49x") (f (quote (("tokio" "futures" "tokio-io") ("default"))))))

