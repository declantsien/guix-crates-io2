(define-module (crates-io bu f_ buf_stream_reader) #:use-module (crates-io))

(define-public crate-buf_stream_reader-0.1.0 (c (n "buf_stream_reader") (v "0.1.0") (h "19lp54a0sz3qa7jdcmc5wmfwhljjh7n9ds4l82z80hrcvfx5x6fq")))

(define-public crate-buf_stream_reader-0.1.1 (c (n "buf_stream_reader") (v "0.1.1") (h "0h0z26882b9arvrwsg1km4k8g0z8knyzyjac5wz70c30r4q4dl04")))

(define-public crate-buf_stream_reader-0.2.0 (c (n "buf_stream_reader") (v "0.2.0") (h "14l7ynkwkx5ikb1icp3qgl7527804kpvmckp2vapsm23wsvb83lh")))

