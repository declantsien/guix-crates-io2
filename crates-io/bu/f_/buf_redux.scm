(define-module (crates-io bu f_ buf_redux) #:use-module (crates-io))

(define-public crate-buf_redux-0.1.0 (c (n "buf_redux") (v "0.1.0") (h "0m57iisw5wj8m52680cmlc1p3152h6rzv4i82i1y5nh54qn40r59")))

(define-public crate-buf_redux-0.1.1 (c (n "buf_redux") (v "0.1.1") (h "0mqvijmfzvhh6n741nsgy70v7h40ndfbw6cgylalfj83c554rrs3") (y #t)))

(define-public crate-buf_redux-0.1.2 (c (n "buf_redux") (v "0.1.2") (h "10vrg2bwaazqlh3ncy5irqdjfzbn7yyvxfvmnbp09r8jym2gskgy")))

(define-public crate-buf_redux-0.1.3 (c (n "buf_redux") (v "0.1.3") (h "16chpd90zm92pk1cig3qkvbsxi22d72f2rzq1zw5i2y66ncvs5di")))

(define-public crate-buf_redux-0.2.0 (c (n "buf_redux") (v "0.2.0") (h "1sn29r8b4c6f5dba4mmskxnk63im5calqplw2c0csfaalgp2d4n7")))

(define-public crate-buf_redux-0.3.0 (c (n "buf_redux") (v "0.3.0") (h "0zgbaqwzl9533shciy2zdnly0gd1n060vla2f9j41jzmp4crs6w6") (f (quote (("nightly"))))))

(define-public crate-buf_redux-0.5.0 (c (n "buf_redux") (v "0.5.0") (d (list (d (n "safemem") (r "^0.1") (d #t) (k 0)))) (h "0jng7l78w2v2jv46zy2m00zvjcjjrb93sg6dswp28aksaz434hmx") (f (quote (("nightly"))))))

(define-public crate-buf_redux-0.5.1 (c (n "buf_redux") (v "0.5.1") (d (list (d (n "safemem") (r "^0.1") (d #t) (k 0)))) (h "1jsa0p0cdwj3ss0gxkl64ljkykmx4hi4h7xny628xskdndb27dz3") (f (quote (("nightly"))))))

(define-public crate-buf_redux-0.5.2 (c (n "buf_redux") (v "0.5.2") (d (list (d (n "safemem") (r "^0.1") (d #t) (k 0)))) (h "07dzj906dknzw73ki1x73jjdga74ybf2i13dc6jj7azgysiap6kb") (f (quote (("nightly"))))))

(define-public crate-buf_redux-0.5.3 (c (n "buf_redux") (v "0.5.3") (d (list (d (n "safemem") (r "^0.1") (d #t) (k 0)))) (h "0wajkh5n7vih5djdxfzj5qqn0hjv0sar0m1kmvqg1g7lr0hwaqis") (f (quote (("nightly"))))))

(define-public crate-buf_redux-0.5.4 (c (n "buf_redux") (v "0.5.4") (d (list (d (n "safemem") (r "^0.1") (d #t) (k 0)))) (h "0j8igbpxb9y96yzam4fryb0kwx7690rx3l0w9vh5rfqn3wdc546k") (f (quote (("nightly"))))))

(define-public crate-buf_redux-0.6.0 (c (n "buf_redux") (v "0.6.0") (d (list (d (n "memchr") (r "^0.1") (d #t) (k 0)) (d (n "safemem") (r "^0.1") (d #t) (k 0)))) (h "0jm8x8bd6i051flcf2dyzr2z2kphax7lwwrdfspzqx0qlqazfpij") (f (quote (("nightly"))))))

(define-public crate-buf_redux-0.6.1 (c (n "buf_redux") (v "0.6.1") (d (list (d (n "memchr") (r "^0.1") (d #t) (k 0)) (d (n "safemem") (r "^0.1") (d #t) (k 0)))) (h "0paqa586np6mxyg98cmv32q574i1kvv270bf7d417fiiq4s7cjg1") (f (quote (("nightly"))))))

(define-public crate-buf_redux-0.6.2 (c (n "buf_redux") (v "0.6.2") (d (list (d (n "memchr") (r "^0.1") (d #t) (k 0)) (d (n "safemem") (r "^0.1") (d #t) (k 0)))) (h "0c41wq8cbfzrj8xxxbd7mn0rc9hq36afgrj62w634rav0a198raz") (f (quote (("nightly"))))))

(define-public crate-buf_redux-0.6.3 (c (n "buf_redux") (v "0.6.3") (d (list (d (n "memchr") (r "^1.0") (d #t) (k 0)) (d (n "safemem") (r "^0.2") (d #t) (k 0)))) (h "0jgrn1qqlkrxd299kd8j9n2ps3g5xj1pi2dmbyq1dy4z6539c9xr") (f (quote (("nightly"))))))

(define-public crate-buf_redux-0.7.0 (c (n "buf_redux") (v "0.7.0") (d (list (d (n "memchr") (r "^2.0") (d #t) (k 0)) (d (n "safemem") (r "^0.2") (d #t) (k 0)) (d (n "slice-deque") (r "^0.1.8") (o #t) (d #t) (t "cfg(any(unix, windows))") (k 0)))) (h "0nnmhp30nq3ypljgr5ph6036ay5dnadiyqkcl7qnsk6qh7d9xs6r") (f (quote (("nightly" "slice-deque/unstable") ("default" "slice-deque"))))))

(define-public crate-buf_redux-0.7.1 (c (n "buf_redux") (v "0.7.1") (d (list (d (n "memchr") (r "^2.0") (d #t) (k 0)) (d (n "safemem") (r "^0.2") (d #t) (k 0)) (d (n "slice-deque") (r "^0.1.8") (o #t) (d #t) (t "cfg(any(unix, windows))") (k 0)))) (h "1ah19a947mc449bicp80nfjx781rc703i05pjisrdkn94rx6iii0") (f (quote (("nightly" "slice-deque/unstable") ("default" "slice-deque"))))))

(define-public crate-buf_redux-0.8.1 (c (n "buf_redux") (v "0.8.1") (d (list (d (n "memchr") (r "^2.0") (d #t) (k 0)) (d (n "safemem") (r "^0.2") (d #t) (k 0)) (d (n "slice-deque") (r "^0.1") (o #t) (d #t) (t "cfg(any(unix, windows))") (k 0)))) (h "1ja92zpcvsqgqqxy77a3j4ilmb1inwpn4wbp8pw3ylmzmdkmrwkj") (f (quote (("nightly" "slice-deque/unstable") ("default" "slice-deque"))))))

(define-public crate-buf_redux-0.8.2 (c (n "buf_redux") (v "0.8.2") (d (list (d (n "memchr") (r "^2.0") (d #t) (k 0)) (d (n "safemem") (r "^0.2") (d #t) (k 0)) (d (n "slice-deque") (r "^0.2") (o #t) (d #t) (t "cfg(any(unix, windows))") (k 0)))) (h "0llpb7kb7qv3pqcjkm7xx9krjsn83njxd0r0xgrwamx8a0v0nsyb") (f (quote (("nightly" "slice-deque/unstable") ("default" "slice-deque"))))))

(define-public crate-buf_redux-0.8.3 (c (n "buf_redux") (v "0.8.3") (d (list (d (n "memchr") (r "^2.0") (d #t) (k 0)) (d (n "safemem") (r "^0.3") (d #t) (k 0)) (d (n "slice-deque") (r "^0.2") (o #t) (d #t) (t "cfg(any(unix, windows))") (k 0)))) (h "12n3vvj58dx4p4vskylcvq912cwcc2w35v83k5vm54w6s6jjhy1m") (f (quote (("nightly" "slice-deque/unstable") ("default" "slice-deque"))))))

(define-public crate-buf_redux-0.8.4 (c (n "buf_redux") (v "0.8.4") (d (list (d (n "memchr") (r "^2.0") (d #t) (k 0)) (d (n "safemem") (r "^0.3") (d #t) (k 0)) (d (n "slice-deque") (r "^0.2") (o #t) (d #t) (t "cfg(any(unix, windows))") (k 0)))) (h "0zy0p4vd2kjk7z5m4m8kdwhs5cmx1z02n7v36njhgfs8fs4aclxr") (f (quote (("nightly" "slice-deque/unstable") ("default" "slice-deque"))))))

