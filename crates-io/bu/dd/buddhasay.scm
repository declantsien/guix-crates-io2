(define-module (crates-io bu dd buddhasay) #:use-module (crates-io))

(define-public crate-buddhasay-0.1.0 (c (n "buddhasay") (v "0.1.0") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "06pcpfbc9d1r0wp1fsc0ssrmrbisssjj1n2plnmk4yywx8h1scf7")))

(define-public crate-buddhasay-0.1.1 (c (n "buddhasay") (v "0.1.1") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "0z4hr04zr4w9085bfd42zyag0px67m5vmbbsh45svybypdg5c8fq")))

(define-public crate-buddhasay-0.2.0 (c (n "buddhasay") (v "0.2.0") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "0xl9d6xypx8jsm64rqaaic6isay8am1v4arfclkww5g7b5wv4aja")))

(define-public crate-buddhasay-0.3.0 (c (n "buddhasay") (v "0.3.0") (d (list (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "1h2xafjw43rsqrd19mx34658nj9vyv62ynagq2sv9i8y2fh3g247")))

(define-public crate-buddhasay-0.4.0 (c (n "buddhasay") (v "0.4.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1s94ky75fg16iik3cv6v7mmsz5gx3kyq31waqvali8fadkndbjd9")))

(define-public crate-buddhasay-0.6.0 (c (n "buddhasay") (v "0.6.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "13xh83izalynjfq161vdngp4va60zawqd2a3cbkpxwama7r26pwd")))

(define-public crate-buddhasay-0.6.1 (c (n "buddhasay") (v "0.6.1") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1pvy379gpjxbawqhdwd9v5i5g3nhrscn19wh219cl4glgg9p22az")))

(define-public crate-buddhasay-0.6.2 (c (n "buddhasay") (v "0.6.2") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0fk29kaadd0ihc4g8lrri8rhcm5ba7q6gq0ldrd6jljbgnf3n1ca")))

(define-public crate-buddhasay-0.6.3 (c (n "buddhasay") (v "0.6.3") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0igd5di3h66xxqqahj2fql1xw7l0qqmggacx63l5gn9zfpkipyf5")))

