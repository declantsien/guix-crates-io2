(define-module (crates-io bu dd buddy-alloc) #:use-module (crates-io))

(define-public crate-buddy-alloc-0.1.0-pre.1 (c (n "buddy-alloc") (v "0.1.0-pre.1") (h "1bhz9hzq0cmbkrmhvcj5i88lb7iw9wll3b73g9218081679j1xpi")))

(define-public crate-buddy-alloc-0.1.0-pre.2 (c (n "buddy-alloc") (v "0.1.0-pre.2") (h "07dckhx6p0wx9jv4j18adb6iaar168b13s3wac4f8vmkrvy3k36z")))

(define-public crate-buddy-alloc-0.1.0 (c (n "buddy-alloc") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1bazr2j6s92k5wbvivks14b6kh34bl0297cp2bsmmf5qz2k9pbjw") (y #t)))

(define-public crate-buddy-alloc-0.1.1 (c (n "buddy-alloc") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1wmzsj1d65nxrgvfnpv52gnb8ar2an6k1bn8zlvjibr3j1ahv7sf") (y #t)))

(define-public crate-buddy-alloc-0.1.2 (c (n "buddy-alloc") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1jq716dhglbkhg5j3fv4x6ja3yppfz65w15ffg1n326b6fxhn8hn") (y #t)))

(define-public crate-buddy-alloc-0.2.0 (c (n "buddy-alloc") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0ik3palp8gxlhas23lp8694ly6qp08fhhbbkkgxyx1wq04i15pgd") (y #t)))

(define-public crate-buddy-alloc-0.3.0 (c (n "buddy-alloc") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "03lyj4nkvidasgnhd37l7q2i2i21vk1ksklh13sdv7yzkym9xfpg") (y #t)))

(define-public crate-buddy-alloc-0.4.0 (c (n "buddy-alloc") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "14az9i03g7z5mdz11x9y81sq84nrwp7hyfsfnrw29gqwcc6vp1as") (y #t)))

(define-public crate-buddy-alloc-0.4.1 (c (n "buddy-alloc") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1m9z7wbiblh3fi2wg5510g59ddsmj85b9pmmwa2xn1k4k0wg7ycg")))

(define-public crate-buddy-alloc-0.4.2 (c (n "buddy-alloc") (v "0.4.2") (d (list (d (n "compiler_builtins") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "029dri5jaaa08d3dq6isbq3acvp9j3741ga12sjsc3fg175s8h1j") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins/rustc-dep-of-std") ("default"))))))

(define-public crate-buddy-alloc-0.5.0 (c (n "buddy-alloc") (v "0.5.0") (d (list (d (n "compiler_builtins") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1v5a0vq8drbq14hrhakz6yxra1l80pnrha8nbqfy706zhdindan6") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins/rustc-dep-of-std") ("default"))))))

(define-public crate-buddy-alloc-0.5.1 (c (n "buddy-alloc") (v "0.5.1") (d (list (d (n "compiler_builtins") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "15dkp8ja06cfhzqvkccn6g0q63ym4ll8h907grd5v2ba9ak2s38z") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins/rustc-dep-of-std") ("default"))))))

