(define-module (crates-io bu dd buddy-system) #:use-module (crates-io))

(define-public crate-buddy-system-0.1.0 (c (n "buddy-system") (v "0.1.0") (d (list (d (n "generational-arena") (r "^0.2.9") (d #t) (k 0)))) (h "1jfr6lhq0b9r7kqqdmxcpcwm2y81pklz62j2z56g57is9yk4f7qw")))

(define-public crate-buddy-system-0.2.0 (c (n "buddy-system") (v "0.2.0") (d (list (d (n "generational-arena") (r "^0.2.9") (d #t) (k 0)))) (h "1c34hkcaml9gpz2h947bdin036kn07p6hsl75zidl9yr4ylkyp3f")))

