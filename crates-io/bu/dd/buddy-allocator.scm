(define-module (crates-io bu dd buddy-allocator) #:use-module (crates-io))

(define-public crate-buddy-allocator-0.1.0 (c (n "buddy-allocator") (v "0.1.0") (d (list (d (n "alloc-wg") (r "^0.8") (d #t) (k 0)))) (h "0r1saj6szs7lxyy0q8c8mrk4x5zj1y8wc4jxa969xzapg23x8nys")))

(define-public crate-buddy-allocator-0.1.1 (c (n "buddy-allocator") (v "0.1.1") (d (list (d (n "alloc-wg") (r "^0.8") (k 0)))) (h "1lni3fsqn67ywxls6nalz1z1cclyak10zwsaw08vqn3vfhbdbrf4") (f (quote (("std" "alloc-wg/std") ("default" "std"))))))

(define-public crate-buddy-allocator-0.2.0 (c (n "buddy-allocator") (v "0.2.0") (d (list (d (n "alloc-wg") (r "^0.8") (k 0)))) (h "0vi4amcfx6anag8cndp3hcpwcy6zmxgqh7833dsknlny4xg89cxk") (f (quote (("std" "alloc-wg/std") ("default" "std"))))))

(define-public crate-buddy-allocator-0.2.1 (c (n "buddy-allocator") (v "0.2.1") (d (list (d (n "alloc-wg") (r "^0.8") (k 0)))) (h "0mnmg658m7qvx021bl5bvhym13as6qnxslm42p0k49q3j95q8xgs") (f (quote (("std" "alloc-wg/std") ("default" "std"))))))

(define-public crate-buddy-allocator-0.2.2 (c (n "buddy-allocator") (v "0.2.2") (d (list (d (n "alloc-wg") (r "^0.8") (k 0)))) (h "1ms4xfwchzkrc7bs0mmv5a8c5145l723x9b49fpii71rzjmlr5ps") (f (quote (("std" "alloc-wg/std") ("default" "std"))))))

(define-public crate-buddy-allocator-0.3.0 (c (n "buddy-allocator") (v "0.3.0") (d (list (d (n "alloc-wg") (r "^0.9") (k 0)))) (h "10iifwbi1giy0a0iz9hdnqvbw3avsa9l0g968kli27m6sk8d5k3c") (f (quote (("std" "alloc-wg/std") ("default" "std"))))))

(define-public crate-buddy-allocator-0.3.1 (c (n "buddy-allocator") (v "0.3.1") (d (list (d (n "alloc-wg") (r "^0.9") (k 0)))) (h "0z4kzcq016kx6d8n497vrsa098xs3bmm824dqgg8zl6gr51f1jy7") (f (quote (("std" "alloc-wg/std") ("default" "std"))))))

