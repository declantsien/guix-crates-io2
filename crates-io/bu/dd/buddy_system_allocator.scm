(define-module (crates-io bu dd buddy_system_allocator) #:use-module (crates-io))

(define-public crate-buddy_system_allocator-0.1.0 (c (n "buddy_system_allocator") (v "0.1.0") (d (list (d (n "spin") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0mglzi53rpjmp2wa7icc2hwfg6q424j1h6j93fis2qjlj6a6w8xf") (f (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.1.1 (c (n "buddy_system_allocator") (v "0.1.1") (d (list (d (n "spin") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0bahq4x3wsbxz779dppzsqiiqpc6b1nzkrr3wlsy6h4rmdqmp2gp") (f (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.1.2 (c (n "buddy_system_allocator") (v "0.1.2") (d (list (d (n "spin") (r "^0.5") (o #t) (d #t) (k 0)))) (h "15f5dhmx4lg1ihjkzcjhklwcafnngyv7aqwdk4my7mi7wbqjin1f") (f (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.2.0 (c (n "buddy_system_allocator") (v "0.2.0") (d (list (d (n "spin") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0cgvaxf18hz1xl0ab0gig5m6g287cbqxbrq3f2w1dd43qfp6q421") (f (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.2.1 (c (n "buddy_system_allocator") (v "0.2.1") (d (list (d (n "spin") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1xl3p6zbdbsscha8p1vfjjglahg4sb65l40cpmpi28pypwab2z4q") (f (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.3.0 (c (n "buddy_system_allocator") (v "0.3.0") (d (list (d (n "spin") (r "^0.5") (o #t) (d #t) (k 0)))) (h "04434a6sn3kinimjk3hhrxi8lhm06smh5hhpwv176b0cfndlg5ls") (f (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.3.1 (c (n "buddy_system_allocator") (v "0.3.1") (d (list (d (n "spin") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0p2756xzq074l6gpv37g7m6l8irpwamb157c2q96h06ismdd9cb4") (f (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.3.2 (c (n "buddy_system_allocator") (v "0.3.2") (d (list (d (n "spin") (r "^0.5") (o #t) (d #t) (k 0)))) (h "02p8rldgxnc8h99w89xicmkszdfmwibk2vr273wqy6hhkaa2nw3r") (f (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.3.3 (c (n "buddy_system_allocator") (v "0.3.3") (d (list (d (n "spin") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1s82dz4pf38cby1mbd8q0r6irrpkkdr9p18a9y63yxdiwgap2wbc") (f (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.3.4 (c (n "buddy_system_allocator") (v "0.3.4") (d (list (d (n "spin") (r "^0.5") (o #t) (d #t) (k 0)))) (h "14dfbbiy9rc447gszgm7z1xc3iyw82awnhsb5zds1zh778ac19ll") (f (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.3.5 (c (n "buddy_system_allocator") (v "0.3.5") (d (list (d (n "spin") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0vs208ah5qk1747hxv35ck99xmpjcxdxg0826xwfx2b5appibnjr") (f (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.3.6 (c (n "buddy_system_allocator") (v "0.3.6") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "spin") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0aphl9zh2dz603fmkc14765pkh0jiy1z2zs4dvv04prhz155bafj") (f (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.3.7 (c (n "buddy_system_allocator") (v "0.3.7") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "spin") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0n707cvfz2i2c2f64g4v8v241rbwkffbw146fknd0san3qnj3j9z") (f (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.3.8 (c (n "buddy_system_allocator") (v "0.3.8") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "spin") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1jk337zk2qqa4zjxrcm59c0snyzikxiy8cp14b42h0l1kal2l1q9") (f (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.3.9 (c (n "buddy_system_allocator") (v "0.3.9") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "spin") (r "^0.5") (o #t) (d #t) (k 0)))) (h "13q19nhk1q06nrk9l8vha6ym9y55lambdmgan523mqc3inakp5cy") (f (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.4.0 (c (n "buddy_system_allocator") (v "0.4.0") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "spin") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0sx31311sj1x6h5vmsivjl184rrkj9zqbga6wbgpl7sgc0i4s5j2") (f (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.5.0 (c (n "buddy_system_allocator") (v "0.5.0") (d (list (d (n "spin") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1bv1h6m8iv5kq0spgiqgbivks5ya8mfp34bcw2splh78zzhri3w7") (f (quote (("use_spin" "spin") ("default" "use_spin") ("const_fn"))))))

(define-public crate-buddy_system_allocator-0.6.0 (c (n "buddy_system_allocator") (v "0.6.0") (d (list (d (n "spin") (r ">=0.7.0, <0.8.0") (o #t) (d #t) (k 0)))) (h "0c5qzj5nnaahhdffjgs6wxx18v1wg5w6a8yi1fp4cnqh1rv5xs5l") (f (quote (("use_spin" "spin") ("default" "use_spin") ("const_fn"))))))

(define-public crate-buddy_system_allocator-0.7.0 (c (n "buddy_system_allocator") (v "0.7.0") (d (list (d (n "spin") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0r15da3fl1g1czqk223f6cy4rf2433zydzlzplmx0nhbgyfkkasj") (f (quote (("use_spin" "spin") ("default" "use_spin") ("const_fn"))))))

(define-public crate-buddy_system_allocator-0.8.0 (c (n "buddy_system_allocator") (v "0.8.0") (d (list (d (n "spin") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0j4apcx159gq65l2ns39g68nk44yvpi6mzwf2pk6q91cy32klw2m") (f (quote (("use_spin" "spin") ("default" "use_spin") ("const_fn"))))))

(define-public crate-buddy_system_allocator-0.9.0 (c (n "buddy_system_allocator") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ctor") (r "^0.1.23") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "spin") (r "^0.9.3") (o #t) (d #t) (k 0)))) (h "0acnyb8xiq5mx05hf1rlbb46p97d00649aacr9iid7hcdddkdya3") (f (quote (("use_spin" "spin") ("default" "use_spin") ("const_fn"))))))

(define-public crate-buddy_system_allocator-0.9.1 (c (n "buddy_system_allocator") (v "0.9.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "ctor") (r "^0.2.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "spin") (r "^0.9.8") (o #t) (d #t) (k 0)))) (h "1dc93fjy3zh142irl4zpb370l5hz6s4z83kxbv3i4wyimn65fkfl") (f (quote (("use_spin" "spin") ("default" "use_spin") ("const_fn"))))))

