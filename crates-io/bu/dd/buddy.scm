(define-module (crates-io bu dd buddy) #:use-module (crates-io))

(define-public crate-buddy-0.0.1 (c (n "buddy") (v "0.0.1") (h "1pmqkw1x2rmvjwvrlyk5f9pri6lc3awqx2l8z54i8w85bx3y2w4c") (y #t)))

(define-public crate-buddy-0.0.2 (c (n "buddy") (v "0.0.2") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 0)) (d (n "toml") (r "^0.7.2") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0as3v37mjxhn3871b2r3ncj1c9mac9453xp6db3a8rjw8zinhns2")))

