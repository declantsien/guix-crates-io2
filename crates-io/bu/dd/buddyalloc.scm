(define-module (crates-io bu dd buddyalloc) #:use-module (crates-io))

(define-public crate-buddyalloc-0.1.4 (c (n "buddyalloc") (v "0.1.4") (h "1rrb5rca5z0qakx5nl60aih9bnlmjnz9rv5zs30sg8j03p1h1f1m")))

(define-public crate-buddyalloc-0.1.5 (c (n "buddyalloc") (v "0.1.5") (h "0cav6bgvvxki0n9l9sw0wivg04jvmq76awnbfn0awbs2w9sdkpm5")))

