(define-module (crates-io bu dd buddies) #:use-module (crates-io))

(define-public crate-buddies-0.0.1 (c (n "buddies") (v "0.0.1") (d (list (d (n "bitvec") (r "^0.11") (k 0)))) (h "019vsrpzihdk4w2zn4ps7bbligp1ln2908phzvy4gwhm09rf60bl") (f (quote (("default"))))))

(define-public crate-buddies-0.0.2 (c (n "buddies") (v "0.0.2") (d (list (d (n "bitvec") (r "^0.11") (k 0)))) (h "0m8k9k7bvyq5b6x6han0axy9pinplyy875ygxbahxcv48rq9kyrq") (f (quote (("default"))))))

(define-public crate-buddies-0.0.3 (c (n "buddies") (v "0.0.3") (d (list (d (n "bitvec") (r "^0.11") (k 0)))) (h "1q4p5322s7pm8xlvi3myx0vnmkrbq8sygm5fry2bmk30l5ym4aqj") (f (quote (("default"))))))

