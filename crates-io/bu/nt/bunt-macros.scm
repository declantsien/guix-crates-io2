(define-module (crates-io bu nt bunt-macros) #:use-module (crates-io))

(define-public crate-bunt-macros-0.1.0 (c (n "bunt-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04s9n40kcdcdkn96p347fr8p12j0ay3jvk0c2iqj52nkbw8k7sg4")))

(define-public crate-bunt-macros-0.2.0 (c (n "bunt-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.1") (d #t) (k 0)))) (h "13hcaa6ab62an6cajs5s4f4lv4z0072qg1cmmfc6pkmdwwdw8vh9")))

(define-public crate-bunt-macros-0.2.2 (c (n "bunt-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.1") (d #t) (k 0)))) (h "1kq9fxwxy8bh2y191xsm00828n1i1givk4lpsaw6dazj1law1dvc")))

(define-public crate-bunt-macros-0.2.3 (c (n "bunt-macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.1") (d #t) (k 0)))) (h "0bji27nfifv2ydwjjvqjms14ap330qafp70bzi6pns5ghhsx7c59")))

(define-public crate-bunt-macros-0.2.4 (c (n "bunt-macros") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.1") (d #t) (k 0)))) (h "16v9dx2kz9jqkljyj8b5636v0shn7l4f4yj0d26kz4avpbjvbrrj")))

(define-public crate-bunt-macros-0.2.5 (c (n "bunt-macros") (v "0.2.5") (d (list (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.1") (d #t) (k 0)))) (h "1056014hjn7ysscdqyjfa1xcvwiqv4x35h8dlx0ghilbpcdy66hq")))

(define-public crate-bunt-macros-0.2.8 (c (n "bunt-macros") (v "0.2.8") (d (list (d (n "litrs") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.1") (d #t) (k 0)))) (h "1f2ay0i4b61k10gyzl4334ih2ph1myjp1v3llp2fmd5377cqki4a")))

