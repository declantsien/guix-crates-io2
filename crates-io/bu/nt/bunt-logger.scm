(define-module (crates-io bu nt bunt-logger) #:use-module (crates-io))

(define-public crate-bunt-logger-0.1.0 (c (n "bunt-logger") (v "0.1.0") (d (list (d (n "bunt") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "1d66rs9r58vdv1yzjd4hw1yahvy1dipkknwv6nlsw2ifcammx9xf")))

(define-public crate-bunt-logger-0.1.1 (c (n "bunt-logger") (v "0.1.1") (d (list (d (n "bunt") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "1zmmlfjdzw8m035wz7qlc2ypxyd9n8n1nqjlblxbsk0ndn1crc86")))

