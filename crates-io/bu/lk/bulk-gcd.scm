(define-module (crates-io bu lk bulk-gcd) #:use-module (crates-io))

(define-public crate-bulk-gcd-0.1.0 (c (n "bulk-gcd") (v "0.1.0") (d (list (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "rug") (r "^1.2.2") (f (quote ("integer"))) (k 0)))) (h "1wy1mzdr9hq0563c7qlnvyz0hv83mc7vambf7w2ajgnh8a11vkh8")))

(define-public crate-bulk-gcd-0.1.1 (c (n "bulk-gcd") (v "0.1.1") (d (list (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "rug") (r "^1.2.2") (f (quote ("integer"))) (k 0)))) (h "16afvk77dz9qz06yfr0klr945wrca3m39sgzr43jbn9kwnjkgpxm")))

(define-public crate-bulk-gcd-0.1.2 (c (n "bulk-gcd") (v "0.1.2") (d (list (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "rug") (r "^1.2.2") (f (quote ("integer"))) (k 0)))) (h "10jvw50nza10451072h90crlfy06kvdnan63ijfimkiq3x74yqz7")))

(define-public crate-bulk-gcd-0.1.3 (c (n "bulk-gcd") (v "0.1.3") (d (list (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "rug") (r "^1.2.2") (f (quote ("integer"))) (k 0)))) (h "0dab1mncys2zjcw95pa365cyzpl14fwmr5dw2mp4s1q894c2v9gf")))

(define-public crate-bulk-gcd-1.0.0 (c (n "bulk-gcd") (v "1.0.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "rug") (r "^1.2.2") (f (quote ("integer"))) (k 0)))) (h "0p1krscimifmj6szqpw41zybhiynzrb7rx09scqfw2g4636jcal0")))

(define-public crate-bulk-gcd-1.0.1 (c (n "bulk-gcd") (v "1.0.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "rug") (r "^1.2.2") (f (quote ("integer"))) (k 0)))) (h "1q2gh6ciqw51iqmd2ksx1r45ml51hy2v7rjh25m8fqxr3q5i3k3z")))

(define-public crate-bulk-gcd-1.0.2 (c (n "bulk-gcd") (v "1.0.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "rug") (r "^1.2.2") (f (quote ("integer"))) (k 0)))) (h "1lmymy3357pjc8fp4kmxq4v6n8n7m70zrwl4wc6v19xfmprjdrni")))

(define-public crate-bulk-gcd-1.0.4 (c (n "bulk-gcd") (v "1.0.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "rug") (r "^1.2.2") (f (quote ("integer"))) (k 0)))) (h "01iry7ma65lr8f0rqc6q4w9dqvwxri0hhypprs9399sdyc039yqb")))

(define-public crate-bulk-gcd-1.0.5 (c (n "bulk-gcd") (v "1.0.5") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "rug") (r "^1.2.2") (f (quote ("integer"))) (k 0)))) (h "1l47rrfz6pixs8galj102z68lamfg4l5njybh9d5znbw6k0qcdni")))

(define-public crate-bulk-gcd-1.0.6 (c (n "bulk-gcd") (v "1.0.6") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "rug") (r "^1.2.2") (f (quote ("integer"))) (k 0)))) (h "0hawgll6i019izklx8nvpyshs4zqc965igfk7zj163h2cpxsaz9j")))

(define-public crate-bulk-gcd-1.0.7 (c (n "bulk-gcd") (v "1.0.7") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "rug") (r "^1.2.2") (f (quote ("integer"))) (k 0)))) (h "1gld04rj5i5f2pfbk73fwg9qwmya07w5xbinmmfwshg8ljwbx1fj")))

(define-public crate-bulk-gcd-2.0.0 (c (n "bulk-gcd") (v "2.0.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "rug") (r "^1.2.2") (f (quote ("integer"))) (k 0)))) (h "0mqr0g3k5c45d263bxq7ra5dp7s7mafal25havgzpzifh5qnp0wn")))

(define-public crate-bulk-gcd-2.1.0 (c (n "bulk-gcd") (v "2.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "rug") (r "^1.2.2") (f (quote ("integer"))) (k 0)))) (h "02km8jf7cizcd1fnssy2wpv2p5ab9i4pyfcyv58yiiyyxdvgj4cp")))

(define-public crate-bulk-gcd-2.1.1 (c (n "bulk-gcd") (v "2.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "rug") (r "^1.2.2") (f (quote ("integer"))) (k 0)))) (h "1my6hwy2cfls6vbhw3nb4g2slzkc43db3g8f8yal2zffihrgnh47")))

(define-public crate-bulk-gcd-2.1.2 (c (n "bulk-gcd") (v "2.1.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "rug") (r "^1.2.2") (f (quote ("integer"))) (k 0)))) (h "14cvwdwdbhlsrdffxm470k5vp1vi2j2kx794ksm8hhv6cy0h9yx7")))

(define-public crate-bulk-gcd-2.2.0 (c (n "bulk-gcd") (v "2.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "rug") (r "^1.2.2") (f (quote ("integer"))) (k 0)))) (h "18dix2j895pibf40nvf69znadi6v7jlvq10y4s0xms2w9wk438dl")))

