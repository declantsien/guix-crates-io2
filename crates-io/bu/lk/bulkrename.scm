(define-module (crates-io bu lk bulkrename) #:use-module (crates-io))

(define-public crate-bulkrename-0.1.2 (c (n "bulkrename") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.77") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0mpjkgirb6x9fmsr6i52cpgfxlvqzmbrg3lbmj489rjjqqk47sgx")))

