(define-module (crates-io bu lk bulk_allocator) #:use-module (crates-io))

(define-public crate-bulk_allocator-0.1.0 (c (n "bulk_allocator") (v "0.1.0") (h "0n7fw8yzykdfrv7mn5wjfqa8fx5gpcv7q3k1havh98d8pnafy1nj")))

(define-public crate-bulk_allocator-0.2.0 (c (n "bulk_allocator") (v "0.2.0") (h "077vhmh04z42dc4dipizj66w8jsmwa6kj0v68sbnpd0mc88662mw")))

(define-public crate-bulk_allocator-0.3.0 (c (n "bulk_allocator") (v "0.3.0") (d (list (d (n "gharial") (r "^0.3") (d #t) (k 2)))) (h "02z9jqm3w2f6kgldfx5s7kh17jdmafdjx67szfcyjnfpxsr5yfg8")))

(define-public crate-bulk_allocator-0.4.0 (c (n "bulk_allocator") (v "0.4.0") (d (list (d (n "gharial") (r "^0.3") (d #t) (k 2)))) (h "1rr08kh0f7w73arkvbcn3dvq3255nlv519p2k9zspm70qv25a1yj")))

(define-public crate-bulk_allocator-0.4.1 (c (n "bulk_allocator") (v "0.4.1") (d (list (d (n "gharial") (r "^0.3") (d #t) (k 2)))) (h "1jpk4pda88ix6vyq0azyhxjbd8m7988vdhpafn5hfd323a7kcyb1")))

(define-public crate-bulk_allocator-0.5.1 (c (n "bulk_allocator") (v "0.5.1") (d (list (d (n "gharial") (r "^0.3") (d #t) (k 2)))) (h "1fvvdazac35ccijazlclxjzbikmscx5b6jfkhbjh9bcgsdbyma70") (y #t)))

(define-public crate-bulk_allocator-0.5.2 (c (n "bulk_allocator") (v "0.5.2") (d (list (d (n "gharial") (r "^0.3") (d #t) (k 2)))) (h "1hawz4s5kpawivf11lzwrrawn7b5xlblvzrz6w4la8vvfby0yyw3")))

