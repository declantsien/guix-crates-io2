(define-module (crates-io bu si business_central) #:use-module (crates-io))

(define-public crate-business_central-0.1.0 (c (n "business_central") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1pw9066lr1wf94sjvificaxrnh38cj2kh46gkg8j27nxvw1hdybf")))

