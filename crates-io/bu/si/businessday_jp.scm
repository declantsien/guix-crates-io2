(define-module (crates-io bu si businessday_jp) #:use-module (crates-io))

(define-public crate-businessday_jp-0.1.0 (c (n "businessday_jp") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "holiday_jp") (r "^0.1.2") (d #t) (k 0)))) (h "09bas3y8aqzsgpdyf9h5a34jmmf2qz9p8k8xs9gy1xi67w5sma0v")))

(define-public crate-businessday_jp-0.1.1 (c (n "businessday_jp") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "holiday_jp") (r "^0.1.2") (d #t) (k 0)))) (h "1r66a9j44lc3szfb7fbhw88kgjlkf0jdwrps56jyflkla1kjb1sg")))

