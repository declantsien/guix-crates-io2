(define-module (crates-io bu nn bunnyhop) #:use-module (crates-io))

(define-public crate-bunnyhop-0.2.0 (c (n "bunnyhop") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "press-btn-continue") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "serial_test") (r "^1.0.0") (d #t) (k 0)) (d (n "sqlite") (r "^0.30.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.10") (d #t) (k 0)))) (h "1kg635mrfs3j418vim1nxvd3l5k67pknng3lxf95bvdfmfzixwn0")))

