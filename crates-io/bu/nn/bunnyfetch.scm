(define-module (crates-io bu nn bunnyfetch) #:use-module (crates-io))

(define-public crate-bunnyfetch-0.1.2 (c (n "bunnyfetch") (v "0.1.2") (h "11hz9kksya7pqjqg16zj9zvmnzkk2kbndmb3n97zn0prc8cnz2df")))

(define-public crate-bunnyfetch-0.1.3 (c (n "bunnyfetch") (v "0.1.3") (h "0vlbcr0xx3y16pbwwdasqhyz7cl8vpis38ljwsjpyj16wh785rq4")))

