(define-module (crates-io bu nn bunny) #:use-module (crates-io))

(define-public crate-bunny-0.1.0 (c (n "bunny") (v "0.1.0") (h "1dd6pss2b3gvg602li6aad2b51yz3c4zsy28xqmaaahwr9pam63p")))

(define-public crate-bunny-0.1.1 (c (n "bunny") (v "0.1.1") (h "1b7gy95dyrbjdz1lkqcmn3xab24gssahh4haygw28iv6zxgrr3l9")))

