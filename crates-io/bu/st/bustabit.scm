(define-module (crates-io bu st bustabit) #:use-module (crates-io))

(define-public crate-bustabit-0.1.0 (c (n "bustabit") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "hmac") (r "^0.7.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "16kvjqgfbqyf61ynwh3pck3szfc8kdkqkg7x8kxxbnin6ib4lqij")))

(define-public crate-bustabit-0.2.0 (c (n "bustabit") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.1") (d #t) (k 0)) (d (n "hmac") (r "^0.7.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1cwg3jasl5sngm93alk2qy475h2s3zgpy81g1i0yzkjijc6b6493")))

(define-public crate-bustabit-0.2.1 (c (n "bustabit") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "criterion") (r "^0.2.9") (d #t) (k 2)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "hmac") (r "^0.7.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0cys2283g4ciay8zifgjwd6cy5z3lfiinbm5f000dzijjnwy7cwf")))

