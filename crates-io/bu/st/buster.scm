(define-module (crates-io bu st buster) #:use-module (crates-io))

(define-public crate-buster-0.1.0 (c (n "buster") (v "0.1.0") (d (list (d (n "geo") (r "^0.17.1") (d #t) (k 0)) (d (n "openaip") (r "^0.2") (d #t) (k 0)) (d (n "simconnect") (r "^0.1.4") (d #t) (k 0)) (d (n "tts") (r "^0.16.0") (d #t) (k 0)))) (h "0zpd57y94gy3gqyv14f9qn4nrvfa5c77c33fdx41rd9h89l4r186")))

