(define-module (crates-io bu st bustubx-cli) #:use-module (crates-io))

(define-public crate-bustubx-cli-0.2.0 (c (n "bustubx-cli") (v "0.2.0") (d (list (d (n "bustubx") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "1qaygfinjcmfb6s03lf1vhvh8zybjpp9v7wzlv6rvp0l7fry8mhd")))

(define-public crate-bustubx-cli-0.3.0 (c (n "bustubx-cli") (v "0.3.0") (d (list (d (n "bustubx") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)))) (h "1visxsac7sb6q526bbqx8fwnkz34x1a419azva5ycaq1g5mlq9yz")))

