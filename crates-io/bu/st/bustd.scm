(define-module (crates-io bu st bustd) #:use-module (crates-io))

(define-public crate-bustd-0.1.1 (c (n "bustd") (v "0.1.1") (d (list (d (n "argh") (r "^0.1.5") (d #t) (k 0)) (d (n "cc") (r "^1.0.68") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "daemonize") (r "^0.4.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.97") (d #t) (k 0)) (d (n "libc") (r "^0.2.97") (d #t) (k 1)) (d (n "procfs") (r "^0.9.1") (d #t) (k 2)))) (h "0byz7ml2vcdgmdxw1kmdbja83541w2pwcri82kfc4fdi8fr3100j") (f (quote (("glob-ignore" "glob"))))))

