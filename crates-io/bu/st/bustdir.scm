(define-module (crates-io bu st bustdir) #:use-module (crates-io))

(define-public crate-bustdir-0.1.0 (c (n "bustdir") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "askama") (r "^0.12") (o #t) (k 0)) (d (n "blake3") (r "^1") (f (quote ("neon"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "tera") (r "^1") (o #t) (k 0)))) (h "05ky9dwyf8q64zcaamacbpf5wx57cf3izbvipnczj2l0y5xgh12y") (f (quote (("default")))) (s 2) (e (quote (("tera" "dep:tera" "get_or_random") ("get_or_random" "dep:rand") ("askama" "dep:askama" "get_or_random"))))))

(define-public crate-bustdir-0.1.1 (c (n "bustdir") (v "0.1.1") (d (list (d (n "ahash") (r "^0.8") (d #t) (k 0)) (d (n "askama") (r "^0.12") (o #t) (k 0)) (d (n "blake3") (r "^1") (f (quote ("neon"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "tera") (r "^1") (o #t) (k 0)))) (h "17kg6xshg77w6ghfldhx7w2f37d6g2l7c1jvpwpwb189xj8na5q3") (f (quote (("default")))) (s 2) (e (quote (("tera" "dep:tera" "get_or_random") ("get_or_random" "dep:rand") ("askama" "dep:askama" "get_or_random"))))))

