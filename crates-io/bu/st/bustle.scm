(define-module (crates-io bu st bustle) #:use-module (crates-io))

(define-public crate-bustle-0.1.0 (c (n "bustle") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)))) (h "0lib2hw96jf1a2q0ym5p5xlwq7di6r6g4q4w9g30xvp3vccqfyiz")))

(define-public crate-bustle-0.2.0 (c (n "bustle") (v "0.2.0") (d (list (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)))) (h "1gh4200vf97b0s9fyyyr23my1hm8sj909wif67y6k7bkxs509vbb")))

(define-public crate-bustle-0.3.0 (c (n "bustle") (v "0.3.0") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)))) (h "0504580ik3jfkndjzbbqb0hk3im65vx86059xfw8jam6bvbn9jcv")))

(define-public crate-bustle-0.3.1 (c (n "bustle") (v "0.3.1") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)))) (h "13zkryn3b26apf31nk9r8k5a0pk0lgvwv0yn3cln83xx2cmkb5n2")))

(define-public crate-bustle-0.3.2 (c (n "bustle") (v "0.3.2") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)))) (h "0cvbg41hwi7wygdkp2g4076zphqjvld9v58yy6xy9zq915ry6yf3")))

(define-public crate-bustle-0.4.0 (c (n "bustle") (v "0.4.0") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)))) (h "1qx63h8hfhxnw31kwqmrgng6ys3x2n4rsjky4b3mn7kv34wnva36")))

(define-public crate-bustle-0.4.1 (c (n "bustle") (v "0.4.1") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)))) (h "1f0gwv4fg05ns7ym7fm6gyvk5d87w9p9170cvi95xhj6g8r8r9gi")))

(define-public crate-bustle-0.4.2 (c (n "bustle") (v "0.4.2") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)))) (h "0pwhap1prbmfbmz0i04q0pka0lch5dsbapkb95zfda8fdzil57y3")))

(define-public crate-bustle-0.4.3 (c (n "bustle") (v "0.4.3") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "tracing-subscriber") (r "^0.2.16") (d #t) (k 2)))) (h "10h7kni9jf7bxy1dwfyz3wmvjvgrg4056grins1gf9wq13cy7856")))

(define-public crate-bustle-0.4.4 (c (n "bustle") (v "0.4.4") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "scopeguard") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "1ff8m08w57njvgx6hb7gxxv574gwy6qip6raqdrp6k26d9wr7775")))

(define-public crate-bustle-0.5.0 (c (n "bustle") (v "0.5.0") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "scopeguard") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "18yxxyy1csss61fbwk83clphg9hrlgrkwkdarl1jpwaj9zq3accr")))

(define-public crate-bustle-0.5.1 (c (n "bustle") (v "0.5.1") (d (list (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "scopeguard") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.8") (d #t) (k 2)))) (h "1jjzcnrzn088r83c6h22msrk7hxzdp3dwiq4bnfsqf8d9x7dhaq8")))

