(define-module (crates-io bu gr bugreport) #:use-module (crates-io))

(define-public crate-bugreport-0.1.0 (c (n "bugreport") (v "0.1.0") (d (list (d (n "snailquote") (r "^0.3") (d #t) (k 0)) (d (n "sys-info") (r "^0.7") (d #t) (k 0)))) (h "01gq5y034fgz1hm8kx12apgcyx91snfzm0bvb7d98fyb1yy6hhl2")))

(define-public crate-bugreport-0.2.0 (c (n "bugreport") (v "0.2.0") (d (list (d (n "snailquote") (r "^0.3") (d #t) (k 0)) (d (n "sys-info") (r "^0.7") (d #t) (k 0)))) (h "07vnsnh247qz8rj7srb70f9y7xrnqjri5y5n4bg4rymxnkm338vg")))

(define-public crate-bugreport-0.2.1 (c (n "bugreport") (v "0.2.1") (d (list (d (n "snailquote") (r "^0.3") (d #t) (k 0)) (d (n "sys-info") (r "^0.7") (d #t) (k 0)))) (h "0qlnwqjjxfhnnizxi728262wgl6ndhm0nffhrwpx5r09ngkbqc9p")))

(define-public crate-bugreport-0.3.0 (c (n "bugreport") (v "0.3.0") (d (list (d (n "snailquote") (r "^0.3") (d #t) (k 0)) (d (n "sys-info") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1n5f1nkqbc5yf9bckjap49pwnqnvdczm6x9y23caaghpgw0n4rqi") (f (quote (("format_plaintext") ("format_markdown") ("default" "collector_operating_system" "format_markdown") ("collector_operating_system" "sys-info"))))))

(define-public crate-bugreport-0.4.0 (c (n "bugreport") (v "0.4.0") (d (list (d (n "git-version") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1") (d #t) (k 0)) (d (n "sys-info") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0gday6f2brqgzl12a0vf7fx1hh1mim6gdjxc6dfwk9v4i19pxsd0") (f (quote (("git_hash" "git-version") ("format_plaintext") ("format_markdown") ("default" "collector_operating_system" "git_hash" "format_markdown") ("collector_operating_system" "sys-info"))))))

(define-public crate-bugreport-0.4.1 (c (n "bugreport") (v "0.4.1") (d (list (d (n "git-version") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "shell-escape") (r "^0.1") (d #t) (k 0)) (d (n "sys-info") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0k20nbl226kni905sy3rvizl7k4h28971143qdlzwfznnjrb8500") (f (quote (("git_hash" "git-version") ("format_plaintext") ("format_markdown") ("default" "collector_operating_system" "git_hash" "format_markdown") ("collector_operating_system" "sys-info"))))))

(define-public crate-bugreport-0.5.0 (c (n "bugreport") (v "0.5.0") (d (list (d (n "git-version") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.1.0") (d #t) (k 2)) (d (n "shell-escape") (r "^0.1") (d #t) (k 0)) (d (n "sys-info") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1jcq9ywxyd7fw1ing8ixna0cfcs5lrviyvx6h6080ir532w20lak") (f (quote (("git_hash" "git-version") ("format_plaintext") ("format_markdown") ("default" "collector_operating_system" "git_hash" "format_markdown") ("collector_operating_system" "sys-info"))))))

