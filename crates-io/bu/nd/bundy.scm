(define-module (crates-io bu nd bundy) #:use-module (crates-io))

(define-public crate-bundy-0.1.0 (c (n "bundy") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bmy3czpdv0cvvn92pjyv25dnzv3pm1fz1l0z2pgrqjd5fpc38dc")))

(define-public crate-bundy-0.1.1 (c (n "bundy") (v "0.1.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01x1sipscg71phj83kvxrc84rdkvy5b83i75r9ak3ndx8asm2zcy")))

(define-public crate-bundy-0.1.2 (c (n "bundy") (v "0.1.2") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m50b9xq7ipyllqz98kkm9vibd05rb4q0nq8ps531a65w6cxn3fp")))

