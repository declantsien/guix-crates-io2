(define-module (crates-io bu nd bundle-derive) #:use-module (crates-io))

(define-public crate-bundle-derive-0.1.0 (c (n "bundle-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cqxh064945zgb8nwwf3am8s3crxv1wsh6nmzkhvvbib0hzh45yq")))

(define-public crate-bundle-derive-0.2.0 (c (n "bundle-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rzdcabxr54hskhiq1vhcz9mq9lz4s76dyvd0bkhpq6n3jv041va")))

(define-public crate-bundle-derive-0.3.0 (c (n "bundle-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1fk6w7w4risxw5kk9hcfzjga85wd1jbgypz04qb181hp11z55snm")))

(define-public crate-bundle-derive-0.4.0 (c (n "bundle-derive") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1fnbmi6lyg0sphjlra7mivw5pcj1al7jlha0ccnfdr5kln060fzx")))

