(define-module (crates-io bu nd bundle-lua) #:use-module (crates-io))

(define-public crate-bundle-lua-0.1.0 (c (n "bundle-lua") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("cargo"))) (d #t) (k 0)))) (h "0j1cm1q0idfxh52ix5hr1pdwni134ifp8vrx1p8haa4as0ni4cmb")))

(define-public crate-bundle-lua-0.2.0 (c (n "bundle-lua") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("cargo"))) (d #t) (k 0)))) (h "0i96a7g59b554zhwsh43pr57x2ya8sqd4q64w6z3xpgmsmagxdbb")))

(define-public crate-bundle-lua-0.3.0 (c (n "bundle-lua") (v "0.3.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("cargo"))) (d #t) (k 0)))) (h "1g1z6n2v9qw7j23v8a4ga08544x3dav41vdx6r5l9vsgshd2w9vr")))

(define-public crate-bundle-lua-0.4.0 (c (n "bundle-lua") (v "0.4.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("cargo"))) (d #t) (k 0)))) (h "1f4zzsnyjb3crs660n5j3hhs1lbcgmf204y8zpsyki6r1d3bvzxw")))

(define-public crate-bundle-lua-0.4.1 (c (n "bundle-lua") (v "0.4.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("cargo"))) (d #t) (k 0)))) (h "0zrdxcnay7cvsl1c748gplhyfmxkydza1n131jc7qljzvl6hf65f")))

(define-public crate-bundle-lua-0.5.0 (c (n "bundle-lua") (v "0.5.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "164441aa0w41iczxx46sbrf808wijpd03f935hsa8kwksvbic2d1")))

(define-public crate-bundle-lua-0.5.1 (c (n "bundle-lua") (v "0.5.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1w3pxcahxpwy8mgv08vd659qs44ra8sklibp7a8nahlrczwprk97")))

(define-public crate-bundle-lua-0.5.2 (c (n "bundle-lua") (v "0.5.2") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1pxd0i43igq0p25qqb8dm92p2jsjgn8v62mpv19ax6lynnf4myqa")))

