(define-module (crates-io bu nd bundle_bundle) #:use-module (crates-io))

(define-public crate-bundle_bundle-0.1.0 (c (n "bundle_bundle") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9") (k 0)))) (h "04b95d08iqp1y8kn9z922x4cqa27dnkxd58fjqdpfdfk3shy5x75")))

(define-public crate-bundle_bundle-0.1.1 (c (n "bundle_bundle") (v "0.1.1") (d (list (d (n "bevy") (r "^0.9") (k 0)) (d (n "bevy") (r "^0.9") (d #t) (k 2)))) (h "06d5k0znv65wwr7r54q0sglbv10skfx651pva9dzfgx6raglci9p")))

