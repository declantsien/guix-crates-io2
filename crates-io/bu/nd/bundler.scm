(define-module (crates-io bu nd bundler) #:use-module (crates-io))

(define-public crate-bundler-0.1.0 (c (n "bundler") (v "0.1.0") (d (list (d (n "goldenfile") (r "^0.5.1") (d #t) (k 2)) (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "1sz6v3rfm4pnlgpy9knxz71h51ar459h885x5fgs9nb3jqqcz9sy")))

(define-public crate-bundler-0.1.1 (c (n "bundler") (v "0.1.1") (d (list (d (n "goldenfile") (r "^0.5.1") (d #t) (k 2)) (d (n "quote") (r "^0.5.1") (d #t) (k 0)) (d (n "rustfmt") (r "^0.10.0") (d #t) (k 0)) (d (n "syn") (r "^0.13.1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "toml") (r "^0.4.6") (d #t) (k 0)))) (h "15rha53829vljsnjxhkj2y1vh754blccann0iznhf3yzk96s2pqd")))

