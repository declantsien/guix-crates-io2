(define-module (crates-io bu nd bundlebee) #:use-module (crates-io))

(define-public crate-bundlebee-0.1.0 (c (n "bundlebee") (v "0.1.0") (h "1d0yqhj7ip2bmiar8xcql10gf4x6x61hbh2ip88wmckcxcf69wjb")))

(define-public crate-bundlebee-0.1.1 (c (n "bundlebee") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fcgb3jr5zpzxsl6bk5sm5ynr46ayznk5vvm9wva5li2f419hh0q")))

(define-public crate-bundlebee-0.1.2 (c (n "bundlebee") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0j9ac8254jjf0kng64bdpjsil94pkspyzljx4mx2jzxih1v2l57z")))

