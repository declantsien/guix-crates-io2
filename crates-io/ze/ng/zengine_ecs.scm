(define-module (crates-io ze ng zengine_ecs) #:use-module (crates-io))

(define-public crate-zengine_ecs-0.0.1 (c (n "zengine_ecs") (v "0.0.1") (d (list (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "zengine_macro") (r "^0.0.1") (d #t) (k 0)))) (h "10w4nglqs03r1v4l5bjs3xkz4fr6yha6gi2mcwb8qmqqx406qmhs")))

(define-public crate-zengine_ecs-0.0.2 (c (n "zengine_ecs") (v "0.0.2") (d (list (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "zengine_macro") (r "^0.0.2") (d #t) (k 0)))) (h "1nnjx29snchrrwd0y85w32kiq31xq862syb3f6x4y8sy5zgyjzgj")))

(define-public crate-zengine_ecs-0.1.0 (c (n "zengine_ecs") (v "0.1.0") (d (list (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "zengine_macro") (r "^0.1.0") (d #t) (k 0)))) (h "133xwd5fkmjf02gpvcz7mh4gjb4ysx6bc79q02k1as008cfyrr07")))

(define-public crate-zengine_ecs-0.1.1 (c (n "zengine_ecs") (v "0.1.1") (d (list (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "zengine_macro") (r "^0.1.1") (d #t) (k 0)))) (h "1y4dnxqfrxrin3pdqvwldwp79z5pmghb8nfw9zc9s4zc05hivcff")))

(define-public crate-zengine_ecs-0.1.2 (c (n "zengine_ecs") (v "0.1.2") (d (list (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "zengine_macro") (r "^0.1.2") (d #t) (k 0)))) (h "0lmq018rbkidgy7yaf6m04h40878kimjzs1wsa7rq807mv07svrc")))

