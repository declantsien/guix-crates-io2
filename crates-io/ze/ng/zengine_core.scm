(define-module (crates-io ze ng zengine_core) #:use-module (crates-io))

(define-public crate-zengine_core-0.0.1 (c (n "zengine_core") (v "0.0.1") (d (list (d (n "glam") (r "^0.21.3") (d #t) (k 0)) (d (n "instant") (r "^0.1.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "zengine_ecs") (r "^0.0.1") (d #t) (k 0)) (d (n "zengine_engine") (r "^0.0.1") (d #t) (k 0)) (d (n "zengine_macro") (r "^0.0.1") (d #t) (k 0)))) (h "083f8rvpmcpc4nyc6hz3hcvvrahgkbqjl36rzc35dwgw7g0n9650")))

(define-public crate-zengine_core-0.1.0 (c (n "zengine_core") (v "0.1.0") (d (list (d (n "glam") (r "^0.21.3") (d #t) (k 0)) (d (n "instant") (r "^0.1.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "zengine_ecs") (r "^0.1.0") (d #t) (k 0)) (d (n "zengine_engine") (r "^0.1.0") (d #t) (k 0)) (d (n "zengine_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0a8sa329nl1i25rpjf7ass6wrdsvliwrzpz7hj4cfmihl5xp36ib")))

(define-public crate-zengine_core-0.1.1 (c (n "zengine_core") (v "0.1.1") (d (list (d (n "glam") (r "^0.21.3") (d #t) (k 0)) (d (n "instant") (r "^0.1.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "zengine_ecs") (r "^0.1.1") (d #t) (k 0)) (d (n "zengine_engine") (r "^0.1.1") (d #t) (k 0)) (d (n "zengine_macro") (r "^0.1.1") (d #t) (k 0)))) (h "0p123q7vzg8b9sic9l3c4wzlqbs4kz4snxj5fpxlg7pxz0whkpzb")))

(define-public crate-zengine_core-0.1.2 (c (n "zengine_core") (v "0.1.2") (d (list (d (n "glam") (r "^0.21.3") (d #t) (k 0)) (d (n "instant") (r "^0.1.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "zengine_ecs") (r "^0.1.2") (d #t) (k 0)) (d (n "zengine_engine") (r "^0.1.2") (d #t) (k 0)) (d (n "zengine_macro") (r "^0.1.2") (d #t) (k 0)))) (h "1wvii22zppbgd0vn276k52myfmwwwjwdd2lra6m1gdrp0fr3aw6p")))

