(define-module (crates-io ze ng zengine_gamepad) #:use-module (crates-io))

(define-public crate-zengine_gamepad-0.0.1 (c (n "zengine_gamepad") (v "0.0.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "gilrs") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "zengine_ecs") (r "^0.0.1") (d #t) (k 0)) (d (n "zengine_engine") (r "^0.0.1") (d #t) (k 0)) (d (n "zengine_input") (r "^0.0.1") (d #t) (k 0)) (d (n "zengine_macro") (r "^0.0.1") (d #t) (k 0)))) (h "1c369z5b473wxn79v934xjabi0f6jak9mlnvzsv31an4mpghxbx1")))

(define-public crate-zengine_gamepad-0.1.0 (c (n "zengine_gamepad") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "gilrs") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "zengine_ecs") (r "^0.1.0") (d #t) (k 0)) (d (n "zengine_engine") (r "^0.1.0") (d #t) (k 0)) (d (n "zengine_input") (r "^0.1.0") (d #t) (k 0)) (d (n "zengine_macro") (r "^0.1.0") (d #t) (k 0)))) (h "08swdbbhyymy8rdc1wll7mvinclxw4alvr6hjabf271g66bwpd1j")))

(define-public crate-zengine_gamepad-0.1.1 (c (n "zengine_gamepad") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "gilrs") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "zengine_ecs") (r "^0.1.1") (d #t) (k 0)) (d (n "zengine_engine") (r "^0.1.1") (d #t) (k 0)) (d (n "zengine_input") (r "^0.1.1") (d #t) (k 0)) (d (n "zengine_macro") (r "^0.1.1") (d #t) (k 0)))) (h "1crj27ffa5xd9wbwqj8m366wjfndrlghb4rbaz2qsv7jydn20lwk")))

(define-public crate-zengine_gamepad-0.1.2 (c (n "zengine_gamepad") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "gilrs") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "zengine_ecs") (r "^0.1.2") (d #t) (k 0)) (d (n "zengine_engine") (r "^0.1.2") (d #t) (k 0)) (d (n "zengine_input") (r "^0.1.2") (d #t) (k 0)) (d (n "zengine_macro") (r "^0.1.2") (d #t) (k 0)))) (h "1xlgvwn3v935yxnvyhhi765y8q5rd3wi8rh3wml7x157raja9kzr")))

