(define-module (crates-io ze ng zengine_macro) #:use-module (crates-io))

(define-public crate-zengine_macro-0.0.1 (c (n "zengine_macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "073bdalj05fqa2v72fycnhf0yzqkmim4vjd12mm5zlrzr72zbd1l")))

(define-public crate-zengine_macro-0.0.2 (c (n "zengine_macro") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1d059pzrj5hhiwpgg1wqrv0p9068v46kd0ad7rkx1ggg18la841h")))

(define-public crate-zengine_macro-0.1.0 (c (n "zengine_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1750sqrkrb6j9k039m6h09wp3n8hiw74l60hnyc8bnq5sjm5w7iv")))

(define-public crate-zengine_macro-0.1.1 (c (n "zengine_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "08p4qj4h79yg8y9nqxc613fbsmnkfjcjl0mpnmbn6b8jkrr7xn9x")))

(define-public crate-zengine_macro-0.1.2 (c (n "zengine_macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1a3ss7h7fyzdh40aiinhxhfip8x3mmvaw088igxccxskdvg8d26m")))

