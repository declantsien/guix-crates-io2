(define-module (crates-io ze ng zengine_physics) #:use-module (crates-io))

(define-public crate-zengine_physics-0.0.1 (c (n "zengine_physics") (v "0.0.1") (d (list (d (n "glam") (r "^0.21.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "zengine_core") (r "^0.0.1") (d #t) (k 0)) (d (n "zengine_ecs") (r "^0.0.1") (d #t) (k 0)) (d (n "zengine_macro") (r "^0.0.1") (d #t) (k 0)))) (h "1lvz6s69xqqjfns2yphbwvjdxr3vdb8zvby9xvyn9wb6kjsdihz1")))

(define-public crate-zengine_physics-0.1.0 (c (n "zengine_physics") (v "0.1.0") (d (list (d (n "glam") (r "^0.21.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "zengine_core") (r "^0.1.0") (d #t) (k 0)) (d (n "zengine_ecs") (r "^0.1.0") (d #t) (k 0)) (d (n "zengine_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0sk8wxm8fypwsh5ahcxcg01xzln6ip0k9xf61773ds98zpqqgg7a")))

(define-public crate-zengine_physics-0.1.1 (c (n "zengine_physics") (v "0.1.1") (d (list (d (n "glam") (r "^0.21.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "zengine_core") (r "^0.1.1") (d #t) (k 0)) (d (n "zengine_ecs") (r "^0.1.1") (d #t) (k 0)) (d (n "zengine_macro") (r "^0.1.1") (d #t) (k 0)))) (h "15l6q9ncpac0086pgcmgh2jxmwfzjh77216na7kwxvlshg426dl7")))

(define-public crate-zengine_physics-0.1.2 (c (n "zengine_physics") (v "0.1.2") (d (list (d (n "glam") (r "^0.21.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "zengine_core") (r "^0.1.2") (d #t) (k 0)) (d (n "zengine_ecs") (r "^0.1.2") (d #t) (k 0)) (d (n "zengine_macro") (r "^0.1.2") (d #t) (k 0)))) (h "1dnn3c9md1nlznw3lgyyzfmpf6g729g4lz73g8livbj7p8dq7ma6")))

