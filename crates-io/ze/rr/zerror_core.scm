(define-module (crates-io ze rr zerror_core) #:use-module (crates-io))

(define-public crate-zerror_core-0.2.0 (c (n "zerror_core") (v "0.2.0") (d (list (d (n "biometrics") (r "^0.1") (d #t) (k 0)) (d (n "buffertk") (r "^0.2") (d #t) (k 0)) (d (n "prototk") (r "^0.2") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.2") (d #t) (k 0)) (d (n "zerror") (r "^0.1") (d #t) (k 0)))) (h "1nqdbbwdbgx4p0vmjn6ls5nmikfjp2536pcr13afxx42rj0h26b8")))

(define-public crate-zerror_core-0.2.1 (c (n "zerror_core") (v "0.2.1") (d (list (d (n "biometrics") (r "^0.2") (d #t) (k 0)) (d (n "buffertk") (r "^0.3") (d #t) (k 0)) (d (n "prototk") (r "^0.2") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.2") (d #t) (k 0)) (d (n "zerror") (r "^0.1") (d #t) (k 0)))) (h "11qqpvn3j3acgc7n39m9dkvsx4v7vxs38hk800syb3b8i2f2hv3x")))

(define-public crate-zerror_core-0.2.2 (c (n "zerror_core") (v "0.2.2") (d (list (d (n "biometrics") (r "^0.3") (d #t) (k 0)) (d (n "buffertk") (r "^0.3") (d #t) (k 0)) (d (n "prototk") (r "^0.2") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.3") (d #t) (k 0)) (d (n "zerror") (r "^0.1") (d #t) (k 0)))) (h "1a92z5hfrz3g98h2zsq3hn9y6hd86jrpvkq42wl2i0ryp4ag17bg")))

(define-public crate-zerror_core-0.3.0 (c (n "zerror_core") (v "0.3.0") (d (list (d (n "biometrics") (r "^0.4") (d #t) (k 0)) (d (n "buffertk") (r "^0.4") (d #t) (k 0)) (d (n "prototk") (r "^0.4") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.4") (d #t) (k 0)) (d (n "tatl") (r "^0.3") (d #t) (k 0)) (d (n "zerror") (r "^0.2") (d #t) (k 0)) (d (n "zerror_derive") (r "^0.1") (d #t) (k 0)))) (h "0j3gphk3i0v562fs42dpmdwy9y9y02364dl0cf8ma4mdqfnb6nhf")))

(define-public crate-zerror_core-0.4.0 (c (n "zerror_core") (v "0.4.0") (d (list (d (n "biometrics") (r "^0.5") (d #t) (k 0)) (d (n "buffertk") (r "^0.5") (d #t) (k 0)) (d (n "prototk") (r "^0.5") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.5") (d #t) (k 0)) (d (n "tatl") (r "^0.4") (d #t) (k 0)) (d (n "zerror") (r "^0.3") (d #t) (k 0)) (d (n "zerror_derive") (r "^0.2") (d #t) (k 0)))) (h "1c5qjqbrmyi4hqlmn0d8kcw5zvl12j0n17ks2db0v45mmigdl4zk")))

(define-public crate-zerror_core-0.5.0 (c (n "zerror_core") (v "0.5.0") (d (list (d (n "biometrics") (r "^0.6") (d #t) (k 0)) (d (n "buffertk") (r "^0.6") (d #t) (k 0)) (d (n "prototk") (r "^0.6") (d #t) (k 0)) (d (n "prototk_derive") (r "^0.6") (d #t) (k 0)) (d (n "tatl") (r "^0.5") (d #t) (k 0)) (d (n "zerror") (r "^0.4") (d #t) (k 0)) (d (n "zerror_derive") (r "^0.3") (d #t) (k 0)))) (h "1kmfg8f1x684h4p4ifp9dqhv2djjrximsivx78hvbin63ml30fm6")))

