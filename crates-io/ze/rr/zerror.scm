(define-module (crates-io ze rr zerror) #:use-module (crates-io))

(define-public crate-zerror-0.1.0 (c (n "zerror") (v "0.1.0") (h "1m45jx8jygjmkg5rl44lj58s0k2b9v23cxmn127j5vy7xkjgv4pw")))

(define-public crate-zerror-0.1.1 (c (n "zerror") (v "0.1.1") (h "10yv8i4ilbgva79m544qmccw7hrl3p1pm8x9pvwhj9gh3n36dlhb")))

(define-public crate-zerror-0.2.0 (c (n "zerror") (v "0.2.0") (h "1f9877jay5csh798jhk4cz6h93rrh4bdqjichcxw21bg8pcgnxwk")))

(define-public crate-zerror-0.3.0 (c (n "zerror") (v "0.3.0") (h "13hb2csjpcsbc1bfgsnyr3338fdvsrj1m29dvh778psfphndyxvv")))

(define-public crate-zerror-0.4.0 (c (n "zerror") (v "0.4.0") (h "0zjqw5dydiws2bv6gadhw8bhywbynixbba8msi0njnshr3amwhn0")))

