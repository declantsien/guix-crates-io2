(define-module (crates-io ze rr zerror_derive) #:use-module (crates-io))

(define-public crate-zerror_derive-0.1.0 (c (n "zerror_derive") (v "0.1.0") (d (list (d (n "derive_util") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qdb74gnhaw60bnyjva0vni5y0q1npvf36x2na221l4lrhlk5kb5")))

(define-public crate-zerror_derive-0.2.0 (c (n "zerror_derive") (v "0.2.0") (d (list (d (n "derive_util") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0v9245ndq7xj56wvhgfbrpymkm0ss3biwr49nln0pralh59kjns2")))

(define-public crate-zerror_derive-0.3.0 (c (n "zerror_derive") (v "0.3.0") (d (list (d (n "derive_util") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1i0bbsd11fz27hfg3vjagsqph374ghr3w0v127bln83lyi88zjsy")))

