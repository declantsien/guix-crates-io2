(define-module (crates-io ze d_ zed_extension_api) #:use-module (crates-io))

(define-public crate-zed_extension_api-0.0.1 (c (n "zed_extension_api") (v "0.0.1") (d (list (d (n "wit-bindgen") (r "^0.18") (d #t) (k 0)))) (h "0klm7k4kjs0bnvkcqa3bxy6lmyqcpz4xk4pxs00yg9kmk7qsj211")))

(define-public crate-zed_extension_api-0.0.3 (c (n "zed_extension_api") (v "0.0.3") (d (list (d (n "wit-bindgen") (r "^0.22") (d #t) (k 0)))) (h "1zihvag9g4blwqg6qy26qbiz32gvk0ifj7hjiqh0wf1pba55lfzl")))

(define-public crate-zed_extension_api-0.0.4 (c (n "zed_extension_api") (v "0.0.4") (d (list (d (n "wit-bindgen") (r "^0.22") (d #t) (k 0)))) (h "0fwmgl0620xp4lz2vvd2aa4qvx1nzg5wvp10bfrmxfsj86nirifm")))

(define-public crate-zed_extension_api-0.0.5 (c (n "zed_extension_api") (v "0.0.5") (d (list (d (n "wit-bindgen") (r "^0.22") (d #t) (k 0)))) (h "18clsfgrzfp4zmk0w1pxdk1grrpx6mmj76pg6lb5k01a617axx55")))

(define-public crate-zed_extension_api-0.0.6 (c (n "zed_extension_api") (v "0.0.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.22") (d #t) (k 0)))) (h "0ng4x5gkdc0pqha71ahz6lm32rc3whg0dsyvx4n2vsrzxb58pjkp")))

