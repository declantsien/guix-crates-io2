(define-module (crates-io ze d_ zed_script) #:use-module (crates-io))

(define-public crate-zed_script-0.1.0 (c (n "zed_script") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.58") (d #t) (k 0)) (d (n "serial_test") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1yk9564jmr6dkpfpva0n1608vy015vqvc5q2i77igb7s4xqwz90m")))

(define-public crate-zed_script-0.1.1 (c (n "zed_script") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.58") (d #t) (k 0)) (d (n "serial_test") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0qdp5iv1jw6s4vzxrwdkp0rb5y4hih8d4c4dn6i1q11k6qahmg2k")))

