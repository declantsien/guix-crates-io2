(define-module (crates-io ze ni zenity) #:use-module (crates-io))

(define-public crate-zenity-1.0.0 (c (n "zenity") (v "1.0.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "unicode-icons") (r "^1.0.0") (d #t) (k 2)))) (h "0pw9yvfbmpv022qkb9qmxfsirqmd6w0lnq125a36rcfshxjaxcbq")))

(define-public crate-zenity-1.0.1 (c (n "zenity") (v "1.0.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "unicode-icons") (r "^1.0.0") (d #t) (k 2)))) (h "030g4vi9al88w9ymlxqkcl0p25667ifailf6jkvd5yf53nr2vc6r")))

(define-public crate-zenity-1.0.3 (c (n "zenity") (v "1.0.3") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "unicode-icons") (r "^1.0.0") (d #t) (k 2)))) (h "1yl05zbd8r8mghx3v0vha5sm6nvl3ydrpwy8n4m010iakqc53i0r")))

(define-public crate-zenity-1.0.4 (c (n "zenity") (v "1.0.4") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "unicode-icons") (r "^1.0.1") (d #t) (k 2)))) (h "0cg97vqmx56085rh83xdyw84pc4zqxim1j9ql4p8cnfk0ya7wcn3")))

(define-public crate-zenity-1.0.6 (c (n "zenity") (v "1.0.6") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "unicode-icons") (r "^1.0.1") (d #t) (k 2)))) (h "06hcacvj2brxq6ijjw8vgx665i63g5558wv24saqdp4f33brz69q")))

(define-public crate-zenity-1.1.1 (c (n "zenity") (v "1.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "unicode-icons") (r "^1.0.1") (d #t) (k 2)) (d (n "utils_arteii_rs") (r "^0.1.0") (d #t) (k 0)))) (h "0qcsj2k4389b3accv0j141dfz9ma3yq2sm13d6fg52p9b9l5w73b")))

(define-public crate-zenity-1.2.0 (c (n "zenity") (v "1.2.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "unicode-icons") (r "^2.1.2") (f (quote ("symbols"))) (d #t) (k 2)) (d (n "utils_arteii_rs") (r "^0.1.0") (d #t) (k 0)))) (h "00lxvv1rcqqwi765cxzwf3rjc8bsi9z8k7vk3qncyydwq5a1r9cz")))

(define-public crate-zenity-1.2.1 (c (n "zenity") (v "1.2.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "unicode-icons") (r "^2.1.2") (f (quote ("symbols"))) (d #t) (k 2)) (d (n "utils_arteii_rs") (r "^0.1.0") (d #t) (k 0)))) (h "1wz88hx64lkgqqfcggi02g46ir1al915m98mj0g2lni2q08wp27m")))

(define-public crate-zenity-1.3.0 (c (n "zenity") (v "1.3.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "supports-color") (r "^3.0.0") (d #t) (k 0)) (d (n "unicode-icons") (r "^2.1.2") (f (quote ("symbols"))) (d #t) (k 2)) (d (n "utils_arteii_rs") (r "^0.1.0") (d #t) (k 0)))) (h "1mq7v5zjwrizd2hynv17j20gpn3zgn6w8baqhy3gq0z2249k7zfl")))

(define-public crate-zenity-1.3.1 (c (n "zenity") (v "1.3.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "supports-color") (r "^3.0.0") (d #t) (k 0)) (d (n "unicode-icons") (r "^2.1.2") (f (quote ("symbols"))) (k 2)) (d (n "utils_arteii_rs") (r "^0.1.0") (d #t) (k 0)))) (h "05zm6ca32ap5nq7kg4vrmmv6wdxd35xnlxv4d6bxwvniiiqbk4mg") (y #t)))

(define-public crate-zenity-1.3.2 (c (n "zenity") (v "1.3.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "supports-color") (r "^3.0.0") (d #t) (k 0)) (d (n "unicode-icons") (r "^2.1.2") (f (quote ("symbols"))) (k 2)))) (h "05imgy4ic6qqh92a3iyav13fnmymbkqlrcaj45458jjy5b85260l")))

(define-public crate-zenity-1.4.0 (c (n "zenity") (v "1.4.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "supports-color") (r "^3.0.0") (d #t) (k 0)) (d (n "unicode-icons") (r "^2.1.2") (f (quote ("symbols"))) (k 2)))) (h "0gyh6acz301zyv3pl6y7i5yg1y8z5ah5mikxzhlbgqb9338jh93l")))

(define-public crate-zenity-2.0.0 (c (n "zenity") (v "2.0.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "supports-color") (r "^3.0.0") (d #t) (k 0)) (d (n "unicode-icons") (r "^2.1.2") (f (quote ("symbols"))) (k 2)))) (h "13dhg319m11lfkl5xvf1fdn6y3r4hblya4bw41ladj1f6y1f5jss")))

(define-public crate-zenity-2.0.1 (c (n "zenity") (v "2.0.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "supports-color") (r "^3.0.0") (d #t) (k 0)) (d (n "unicode-icons") (r "^2.1.2") (f (quote ("symbols"))) (k 2)))) (h "1llymffckcvb0h309kwfr206li9ry1iylv3s8x65lxlakl8sj1nk")))

(define-public crate-zenity-2.0.3 (c (n "zenity") (v "2.0.3") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "supports-color") (r "^3.0.0") (d #t) (k 0)) (d (n "unicode-icons") (r "^2.1.2") (f (quote ("symbols"))) (k 2)))) (h "09l7mmh0qypasknng4pv5si8k98s7b6zq3syj44qs2g2pnmkf071")))

(define-public crate-zenity-3.0.0 (c (n "zenity") (v "3.0.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "supports-color") (r "^3.0.0") (d #t) (k 0)) (d (n "unicode-icons") (r "^2.1.2") (f (quote ("symbols"))) (k 2)))) (h "1gnxigbinnxw4673chwcyspvlrbgl0y3gl05ccack7fr42krpiir")))

(define-public crate-zenity-3.0.1 (c (n "zenity") (v "3.0.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "supports-color") (r "^3.0.0") (d #t) (k 0)) (d (n "unicode-icons") (r "^2.1.2") (f (quote ("symbols"))) (k 2)))) (h "1q8x4gcvi3cgrhy0sw8xnhhn8l9s0snm5kx3210c1l6m01aqvgkp")))

(define-public crate-zenity-3.1.0 (c (n "zenity") (v "3.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "supports-color") (r "^3.0.0") (d #t) (k 0)) (d (n "unicode-icons") (r "^2.1.2") (f (quote ("symbols"))) (k 2)))) (h "0zrfdwb6ld4pmdmj6imb834skcffl3qw1mgwxzbka76l9cdvacil")))

(define-public crate-zenity-3.2.0 (c (n "zenity") (v "3.2.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "supports-color") (r "^3.0.0") (d #t) (k 0)) (d (n "unicode-icons") (r "^2.1.2") (f (quote ("symbols"))) (k 2)))) (h "16l3s4h22kk3gi3z6z3ljkdy71npia8fxvd9rkw9nrwadw022bjs")))

(define-public crate-zenity-3.2.1 (c (n "zenity") (v "3.2.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "supports-color") (r "^3.0.0") (d #t) (k 0)) (d (n "unicode-icons") (r "^2.1.2") (f (quote ("symbols"))) (k 2)))) (h "1m5h6jcii8n0azbk81ih6mzpkxv5bwp1kmj3yjr54jbfzi0z19pw")))

(define-public crate-zenity-3.2.2 (c (n "zenity") (v "3.2.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "supports-color") (r "^3.0.0") (d #t) (k 0)) (d (n "unicode-icons") (r "^2.1.2") (f (quote ("symbols"))) (k 2)))) (h "197zhcklqrdq25ylf86s7qw6r1fp9ayw41jp2zfhnj3dxkmmdf5b")))

(define-public crate-zenity-3.2.3 (c (n "zenity") (v "3.2.3") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "supports-color") (r "^3.0.0") (d #t) (k 0)) (d (n "unicode-icons") (r "^2.1.2") (f (quote ("symbols"))) (k 2)))) (h "1v3y7i2mmh9b123lg2z2ah5z3v6b96s3amwi2fkz3zd7ypdyl02d") (f (quote (("spinner") ("progressbar") ("menu") ("default" "spinner" "progressbar" "menu"))))))

(define-public crate-zenity-3.3.0 (c (n "zenity") (v "3.3.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "supports-color") (r "^3.0.0") (d #t) (k 0)) (d (n "unicode-icons") (r "^2.1.2") (f (quote ("symbols"))) (k 2)))) (h "05fiwsij21i30af5zc3n7mm6xqyajg50s33s6dzzg8c78j7p97c5") (f (quote (("spinner") ("progressbar") ("menu") ("default" "spinner" "progressbar" "menu"))))))

(define-public crate-zenity-3.4.0 (c (n "zenity") (v "3.4.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "supports-color") (r "^3.0.0") (d #t) (k 0)) (d (n "unicode-icons") (r "^2.1.2") (f (quote ("symbols"))) (k 2)))) (h "1xh9cd92b6dkxv6bqcdhr11px15hi86y725g3sagxy368wakqmzg") (f (quote (("spinner") ("progressbar") ("menu") ("default" "spinner" "progressbar" "menu"))))))

(define-public crate-zenity-3.4.1 (c (n "zenity") (v "3.4.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "supports-color") (r "^3.0.0") (d #t) (k 0)) (d (n "unicode-icons") (r "^2.1.2") (f (quote ("symbols"))) (k 2)))) (h "0xqm33fxkk7wsllx05qjm2shb8yxzbk1jm2s5qsgjimlr70knd2v") (f (quote (("spinner") ("progressbar") ("menu") ("default" "spinner" "progressbar" "menu"))))))

(define-public crate-zenity-3.5.0 (c (n "zenity") (v "3.5.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (f (quote ("std"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "supports-color") (r "^3.0.0") (d #t) (k 0)) (d (n "unicode-icons") (r "^2.1.2") (f (quote ("symbols"))) (k 2)))) (h "09695098azdfam29yqb3rsrhp7203jjs5chmf63dk6xyizln8axs") (f (quote (("spinner") ("progressbar") ("menu" "spinner") ("log") ("default" "spinner" "progressbar" "menu" "log"))))))

