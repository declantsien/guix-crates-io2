(define-module (crates-io ze ni zenith) #:use-module (crates-io))

(define-public crate-zenith-0.0.1 (c (n "zenith") (v "0.0.1") (h "0immxhgw5bhs9f8fdy4pbg8apmrr6iqm3r53r8l2vgf6idyjpr8d")))

(define-public crate-zenith-0.0.2 (c (n "zenith") (v "0.0.2") (h "1309gxw08fkln0d8dbk89zaw5s6w71qw4pkjkn6phbh6z4isd75s")))

