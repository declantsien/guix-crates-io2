(define-module (crates-io ze ni zenity-dialog) #:use-module (crates-io))

(define-public crate-zenity-dialog-0.1.0 (c (n "zenity-dialog") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "11bb0rdz7z7g0bm2xf1pri3dv1ncb9px4qfx25lc7n1p1i5hfz20")))

(define-public crate-zenity-dialog-0.1.1 (c (n "zenity-dialog") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "0j7i92lq0bnaan71v137bilwa8k35y3iq68lfmx5b89i9l35azmh")))

(define-public crate-zenity-dialog-0.2.1 (c (n "zenity-dialog") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "0imxhr15k5wrmb2q10v2yxdidqz0767pc5b8laxhqxrpg37343nz")))

(define-public crate-zenity-dialog-0.2.2 (c (n "zenity-dialog") (v "0.2.2") (d (list (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "1wwyi2nqqij86jjrbngb2zf9x9wb13d6ygdr5wbwk00f2djdwrc6")))

