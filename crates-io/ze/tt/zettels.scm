(define-module (crates-io ze tt zettels) #:use-module (crates-io))

(define-public crate-zettels-0.1.0 (c (n "zettels") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "libzettels") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simplelog") (r "^0.7.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "1jd0r2crd7nd6v4wlnd1rlq3mcxsq01gr3jmc3ljm573n1rv9blv")))

(define-public crate-zettels-0.1.1 (c (n "zettels") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "libzettels") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simplelog") (r "^0.7.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "113f56y0j5qsqcay6w2f85xg139fv2vsf9yqbb87z9md19rra771")))

(define-public crate-zettels-0.2.0 (c (n "zettels") (v "0.2.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "libzettels") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simplelog") (r "^0.7.6") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "1w1pafxazxkhm0v8h4g82wfg2jin28c9kq6my0rpj1whxx77y629")))

(define-public crate-zettels-0.3.0 (c (n "zettels") (v "0.3.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "libzettels") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "simplelog") (r "^0.11.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)))) (h "0vz6v2700a03q7hnj5rl2z2y4qgxwkmi2106liriiv15dcwk864l")))

