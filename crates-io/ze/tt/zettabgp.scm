(define-module (crates-io ze tt zettabgp) #:use-module (crates-io))

(define-public crate-zettabgp-0.1.0 (c (n "zettabgp") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "0liqciv8yay233c7g5ncdh2jn5vjs5klxvlkq15jcygd8rpnp66z") (f (quote (("serialization" "serde") ("default" "serialization"))))))

(define-public crate-zettabgp-0.1.1 (c (n "zettabgp") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "18j23hc96smw0kp6yngqx8dxn787g35fiays2bqyl4ssravy4pfz") (f (quote (("serialization" "serde") ("default" "serialization"))))))

(define-public crate-zettabgp-0.1.2 (c (n "zettabgp") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "172r4fs7qwrxd8gm4yjmcvrklj29ma4qy1vfywyw7nzxfwwg02x3") (f (quote (("serialization" "serde") ("default" "serialization"))))))

(define-public crate-zettabgp-0.1.3 (c (n "zettabgp") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "0k7hqdji4mxhm6dwddf7ijkf237ay3f5wnyrddpf72ln8jbf68sj") (f (quote (("serialization" "serde") ("default" "serialization"))))))

(define-public crate-zettabgp-0.1.4 (c (n "zettabgp") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "1ab7fk7awidgdz03cqpcsv5nzzjipiixvc45xqj62jggm8lwarzm") (f (quote (("serialization" "serde") ("default" "serialization"))))))

(define-public crate-zettabgp-0.1.5 (c (n "zettabgp") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "1lbypma93rzkzz8ncvrddbp69k4qyzlwxz4swx1gd6spnrp3f58k") (f (quote (("serialization" "serde") ("default" "serialization"))))))

(define-public crate-zettabgp-0.2.0 (c (n "zettabgp") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "0bg9cp88pih1kziawgi1apb94aywqnxjv0076bh9jd4n29rfq4x7") (f (quote (("serialization" "serde") ("default" "serialization"))))))

(define-public crate-zettabgp-0.2.1 (c (n "zettabgp") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.125") (o #t) (d #t) (k 0)))) (h "1ypl9y9m73fyc1y6jixrjqrx93adibh3zb5sldn83gnsl7mf3y40") (f (quote (("serialization" "serde") ("default" "serialization"))))))

(define-public crate-zettabgp-0.3.0 (c (n "zettabgp") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1rvs5pnpaj58mx99n07b17wxhvl1lfg6k947ai387lfb8m2ppgw0") (f (quote (("serialization" "serde") ("default" "serialization")))) (y #t)))

(define-public crate-zettabgp-0.3.1 (c (n "zettabgp") (v "0.3.1") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0fgcsxnfd1av0ds3h9b5p9rkvdz04crgb6vhabkj35gnm2yhn32k") (f (quote (("serialization" "serde") ("default" "serialization"))))))

(define-public crate-zettabgp-0.3.2 (c (n "zettabgp") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1pgkm07x0bvlf7x4a2ggbwnz2amz9y0rcllbc3mx0n4vk0v6dl8s") (f (quote (("serialization" "serde") ("default" "serialization"))))))

(define-public crate-zettabgp-0.3.3 (c (n "zettabgp") (v "0.3.3") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0rzycv8j5wdk5lcqrqxssqqh2bpbj9aivc52d6ynx8bvdp1gsbnp") (f (quote (("serialization" "serde") ("default" "serialization"))))))

(define-public crate-zettabgp-0.3.4 (c (n "zettabgp") (v "0.3.4") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "198c99s4mb2caqz29x41731lkii24s9wpdzp3cqwb61dgq4057a5") (f (quote (("serialization" "serde") ("default" "serialization"))))))

