(define-module (crates-io ze er zeerust) #:use-module (crates-io))

(define-public crate-zeerust-0.1.0 (c (n "zeerust") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)))) (h "1wjvv63yy6vr6nhpz7b9wp6qwzjgqmmxw4x5fjaam9cyhdzigavb")))

(define-public crate-zeerust-0.1.1 (c (n "zeerust") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)))) (h "0fr9gv79i4xv99lz87k3bdyb6ajrgqvyyfc5mrwnip4ijkdln10d")))

(define-public crate-zeerust-0.2.0 (c (n "zeerust") (v "0.2.0") (d (list (d (n "enum-display-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)))) (h "0jah0bl71n9nwfd75sgicvnav033137y5923d45lmnprkji0zk5i")))

(define-public crate-zeerust-0.2.1 (c (n "zeerust") (v "0.2.1") (d (list (d (n "enum-display-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "stderrlog") (r "^0.4") (d #t) (k 0)))) (h "0nx24myhq0vcq4rzysv7ll3ahjq6gmm383ambk7dgwm239dgdfn8")))

