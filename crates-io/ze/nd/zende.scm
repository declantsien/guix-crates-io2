(define-module (crates-io ze nd zende) #:use-module (crates-io))

(define-public crate-zende-0.1.0 (c (n "zende") (v "0.1.0") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "016rkk8vhkvd3b6h8kc4asr5r0nxggwj2c6hy6an0qwlpqc33mff")))

(define-public crate-zende-0.1.3 (c (n "zende") (v "0.1.3") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0cvq3kjsf8dyp6fszs39d9lmv9zqdx0rl8417bq907fi8cmdfzzm")))

(define-public crate-zende-0.1.5 (c (n "zende") (v "0.1.5") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "064ls0wcz0mncb8kxb2m8ss5j76xq5x5ahngcqh2w19yk8pqfmiz")))

(define-public crate-zende-0.1.6 (c (n "zende") (v "0.1.6") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1043qnlsb0r7cw8f1rwh2q8vya0rkiv2cxbjhg48vmqi3vs2a11k")))

(define-public crate-zende-0.1.8 (c (n "zende") (v "0.1.8") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0nahxag9372cpzpcc97y0g0jcjz10hwml49nl4sncxx25axx98r3")))

