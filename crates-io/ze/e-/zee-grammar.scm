(define-module (crates-io ze e- zee-grammar) #:use-module (crates-io))

(define-public crate-zee-grammar-0.1.0 (c (n "zee-grammar") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.6.3") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.6.3") (d #t) (k 1)))) (h "0j0g19vdlzq6wykd1y85g8qg8j8d8w6d68mglrslg4a1mwzikibm")))

(define-public crate-zee-grammar-0.1.2 (c (n "zee-grammar") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.6.3") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.6.3") (d #t) (k 1)))) (h "02k99gx4bgq0mxqyd23b6h4p65rhr5v858qk7ffxsqqzddp603mg")))

(define-public crate-zee-grammar-0.2.0 (c (n "zee-grammar") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.6") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.6") (d #t) (k 1)))) (h "0x79q923hlx41z9a7hs31zm6fj770ajxiq02k4b51nnw1p2fdrlw") (r "1.59")))

(define-public crate-zee-grammar-0.2.1 (c (n "zee-grammar") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.6") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.6") (d #t) (k 1)))) (h "0i77wrw40y023qppl3myz191y75snrzh0x497iip0lfp5brdz6gk") (r "1.59")))

(define-public crate-zee-grammar-0.2.2 (c (n "zee-grammar") (v "0.2.2") (d (list (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.6") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.6") (d #t) (k 1)))) (h "1xdad5zydmw27riad9xd6dcb4xzqf3mfn0bras5sgj9ipxbpzyar") (r "1.59")))

(define-public crate-zee-grammar-0.2.3 (c (n "zee-grammar") (v "0.2.3") (d (list (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.6") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.6") (d #t) (k 1)))) (h "1rviajpri8jzrj5kgxn04znw5qr0cvc9mrs4g4nmbv32gyqdrcjq") (r "1.59")))

(define-public crate-zee-grammar-0.3.0 (c (n "zee-grammar") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "libloading") (r "^0.7.3") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.6") (d #t) (k 0)))) (h "0v37nhdw25rrxnvhjwiqj6ggydv4h82gwvwcjgw7x51ffhkkxzik") (r "1.59")))

