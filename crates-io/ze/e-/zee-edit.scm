(define-module (crates-io ze e- zee-edit) #:use-module (crates-io))

(define-public crate-zee-edit-0.1.0 (c (n "zee-edit") (v "0.1.0") (d (list (d (n "euclid") (r "^0.22.7") (d #t) (k 0)) (d (n "ropey") (r "^1.4.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1xg5rgysm81v3za6f7kwpx8rcp4d4y4ssk1yhqzqln7cw6jarlzf") (r "1.59")))

