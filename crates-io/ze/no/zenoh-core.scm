(define-module (crates-io ze no zenoh-core) #:use-module (crates-io))

(define-public crate-zenoh-core-0.6.0-beta.1 (c (n "zenoh-core") (v "0.6.0-beta.1") (d (list (d (n "anyhow") (r "^1.0.65") (f (quote ("std"))) (k 0)) (d (n "async-std") (r "=1.12.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "zenoh-macros") (r "^0.6.0-beta.1") (d #t) (k 0)))) (h "016i7kywb979hv7sfx0lzcgz94h9h1s0badwd1ylc5q4jhyd9fxs") (r "1.62.1")))

(define-public crate-zenoh-core-0.7.0-rc (c (n "zenoh-core") (v "0.7.0-rc") (d (list (d (n "anyhow") (r "^1.0.66") (f (quote ("std"))) (k 0)) (d (n "async-std") (r "=1.12.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "zenoh-macros") (r "^0.7.0-rc") (d #t) (k 0)))) (h "0pk052bbjsdsgvk0vk4j55fxz42gqgmzak2dnxfmb0rziwama3qv") (r "1.62.1")))

(define-public crate-zenoh-core-0.7.1-rc (c (n "zenoh-core") (v "0.7.1-rc") (d (list (d (n "async-std") (r "=1.12.0") (f (quote ("default"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "zenoh-result") (r "^0.7.1-rc") (k 0)))) (h "1gq1i7hy9gdziy6f5af77frvby6d8z4y7lb21jchpiijl45fizq8") (f (quote (("std") ("default" "std")))) (y #t) (r "1.65.0")))

(define-public crate-zenoh-core-0.7.2-rc (c (n "zenoh-core") (v "0.7.2-rc") (d (list (d (n "async-std") (r "=1.12.0") (f (quote ("default"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "zenoh-result") (r "^0.7.2-rc") (k 0)))) (h "09spb3zdvn12siciffix496da32dj6wyp4b68jwfr8hnlpgf43k8") (f (quote (("std") ("default" "std")))) (r "1.65.0")))

(define-public crate-zenoh-core-0.10.0-rc (c (n "zenoh-core") (v "0.10.0-rc") (d (list (d (n "async-std") (r "=1.12.0") (f (quote ("default"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "zenoh-result") (r "^0.10.0-rc") (k 0)))) (h "17d5hfpgyr9i1lagxrfjwa4d27f9qvxndishbs5shs18m8dgimq2") (f (quote (("std") ("default" "std")))) (r "1.66.1")))

(define-public crate-zenoh-core-0.10.1-rc (c (n "zenoh-core") (v "0.10.1-rc") (d (list (d (n "async-std") (r "=1.12.0") (f (quote ("default"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "zenoh-result") (r "^0.10.1-rc") (k 0)))) (h "0jiybzpnc1zdd26gl3ndhj3v43abvrid6n0rbg39460k1d494zs4") (f (quote (("std") ("default" "std")))) (r "1.66.1")))

(define-public crate-zenoh-core-0.11.0-rc.1 (c (n "zenoh-core") (v "0.11.0-rc.1") (d (list (d (n "async-global-executor") (r "^2.3.1") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt"))) (k 0)) (d (n "zenoh-result") (r "^0.11.0-rc.1") (k 0)) (d (n "zenoh-runtime") (r "^0.11.0-rc.1") (d #t) (k 0)))) (h "1pk3l24arw9p3sdab52fzs88ia6ych0yw3wp57d5j2g2x8mxhv5h") (f (quote (("std") ("default" "std")))) (r "1.66.1")))

(define-public crate-zenoh-core-0.11.0-rc.2 (c (n "zenoh-core") (v "0.11.0-rc.2") (d (list (d (n "async-global-executor") (r "^2.3.1") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt"))) (k 0)) (d (n "zenoh-result") (r "^0.11.0-rc.2") (k 0)) (d (n "zenoh-runtime") (r "^0.11.0-rc.2") (d #t) (k 0)))) (h "0bz9l7192c337wdqszl726jrylij999xvrxvw6j6s8wavybxj4d6") (f (quote (("std") ("default" "std")))) (r "1.66.1")))

(define-public crate-zenoh-core-0.11.0-rc.3 (c (n "zenoh-core") (v "0.11.0-rc.3") (d (list (d (n "async-global-executor") (r "^2.3.1") (f (quote ("tokio"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt"))) (k 0)) (d (n "zenoh-result") (r "^0.11.0-rc.3") (k 0)) (d (n "zenoh-runtime") (r "^0.11.0-rc.3") (d #t) (k 0)))) (h "14ng4cb98vg56l05h0n1mj2fmw0v8w8z3ahfr7wp8ha0jyap6j6g") (f (quote (("std") ("default" "std")))) (r "1.72.0")))

