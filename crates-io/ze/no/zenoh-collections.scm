(define-module (crates-io ze no zenoh-collections) #:use-module (crates-io))

(define-public crate-zenoh-collections-0.6.0-beta.1 (c (n "zenoh-collections") (v "0.6.0-beta.1") (d (list (d (n "async-std") (r "=1.12.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.57") (d #t) (k 0)) (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "zenoh-core") (r "^0.6.0-beta.1") (d #t) (k 0)) (d (n "zenoh-sync") (r "^0.6.0-beta.1") (d #t) (k 0)))) (h "0617riw0v2gphvwq0y2iv2bsg23i65igl5ywxd093gs1ivdz9q05") (r "1.62.1")))

(define-public crate-zenoh-collections-0.7.0-rc (c (n "zenoh-collections") (v "0.7.0-rc") (d (list (d (n "async-std") (r "=1.12.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.59") (d #t) (k 0)) (d (n "flume") (r "^0.10.14") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "zenoh-core") (r "^0.7.0-rc") (d #t) (k 0)) (d (n "zenoh-sync") (r "^0.7.0-rc") (d #t) (k 0)))) (h "0i9qganq2bzz3chalyg1ykbx425pzkkzmyvysxjzg6iczxx6s98y") (r "1.62.1")))

(define-public crate-zenoh-collections-0.7.1-rc (c (n "zenoh-collections") (v "0.7.1-rc") (h "1vjzdhzpsc27hzsl7g1p8zhywws2v7pd5105nlz6nfcnl3528hv4") (f (quote (("std") ("default" "std")))) (y #t) (r "1.65.0")))

(define-public crate-zenoh-collections-0.7.2-rc (c (n "zenoh-collections") (v "0.7.2-rc") (h "19zi4vw47cggbz0mi5c9yhs7l49q24q4xb648h557jj8yw7sgcrw") (f (quote (("std") ("default" "std")))) (r "1.65.0")))

(define-public crate-zenoh-collections-0.10.0-rc (c (n "zenoh-collections") (v "0.10.0-rc") (h "0f5fzm1xd8dj80qvfd8q53fa4caniibjndh6a2xy05wlnwbpw7r7") (f (quote (("std") ("default" "std")))) (r "1.66.1")))

(define-public crate-zenoh-collections-0.10.1-rc (c (n "zenoh-collections") (v "0.10.1-rc") (h "0hajy949i4lfkgrfy3prm464hv3d9hhiqbbjx608y7srixw5i733") (f (quote (("std") ("default" "std")))) (r "1.66.1")))

(define-public crate-zenoh-collections-0.11.0-rc.1 (c (n "zenoh-collections") (v "0.11.0-rc.1") (h "0mc7dgwkvxwsyil77swxkj2yqk0hhkrp865v2wzfdr2mz1d2ym1d") (f (quote (("std") ("default" "std")))) (r "1.66.1")))

(define-public crate-zenoh-collections-0.11.0-rc.2 (c (n "zenoh-collections") (v "0.11.0-rc.2") (h "1khn1m69cl0gs6mpc05s8vr9d3qr5fwdz122lwcsc6cz5qv8phqk") (f (quote (("std") ("default" "std")))) (r "1.66.1")))

(define-public crate-zenoh-collections-0.11.0-rc.3 (c (n "zenoh-collections") (v "0.11.0-rc.3") (h "0v00kqd5ia1w9dw14rqyhnpp0rxvc6cpixqcn97v8nha5dm2gjxp") (f (quote (("std") ("default" "std")))) (r "1.72.0")))

