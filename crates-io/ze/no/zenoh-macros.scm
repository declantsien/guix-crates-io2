(define-module (crates-io ze no zenoh-macros) #:use-module (crates-io))

(define-public crate-zenoh-macros-0.0.0 (c (n "zenoh-macros") (v "0.0.0") (h "0av9w5qw7qf5hf6rvsb1w0mn6lwmhil9hv9mdw0y0f7dffg8cmj5") (y #t)))

(define-public crate-zenoh-macros-0.6.0-beta.1 (c (n "zenoh-macros") (v "0.6.0-beta.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.101") (f (quote ("full"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)))) (h "0hg17ljq29d42nkvd7i2rvmmnf431mjzmfqqg34snn348mcl0ws0") (r "1.62.1")))

(define-public crate-zenoh-macros-0.7.0-rc (c (n "zenoh-macros") (v "0.7.0-rc") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)))) (h "1s4x2sf0yj8m4rq9a7ikpkzx8zm38dnf8ki0ra67g409285w56is") (r "1.62.1")))

(define-public crate-zenoh-macros-0.7.1-rc (c (n "zenoh-macros") (v "0.7.1-rc") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)) (d (n "zenoh-keyexpr") (r "^0.7.1-rc") (f (quote ("std"))) (k 0)))) (h "1jqkmac3c80abirxmg9jv3ll35s5vag3crmglllgb9iaxk3zwh0z") (y #t) (r "1.65.0")))

(define-public crate-zenoh-macros-0.7.2-rc (c (n "zenoh-macros") (v "0.7.2-rc") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)) (d (n "zenoh-keyexpr") (r "^0.7.2-rc") (f (quote ("std"))) (k 0)))) (h "1d9fcqxv099qzv0fsaishw8abj8mxq6r7ly31z5yrcjiya04m7j3") (r "1.65.0")))

(define-public crate-zenoh-macros-0.10.0-rc (c (n "zenoh-macros") (v "0.10.0-rc") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)) (d (n "zenoh-keyexpr") (r "^0.10.0-rc") (f (quote ("std"))) (k 0)))) (h "1d7gzbia72krx9gbv9gip736cxqc4yxqzn1z1hjbwkns13fcb2lh") (r "1.66.1")))

(define-public crate-zenoh-macros-0.10.1-rc (c (n "zenoh-macros") (v "0.10.1-rc") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)) (d (n "zenoh-keyexpr") (r "^0.10.1-rc") (f (quote ("std"))) (k 0)))) (h "1y64xcn2c8kvy3pw3gmz18wzscxrwxydy70s6xwflvajw5hxn9i8") (r "1.66.1")))

(define-public crate-zenoh-macros-0.11.0-rc.1 (c (n "zenoh-macros") (v "0.11.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "zenoh-keyexpr") (r "^0.11.0-rc.1") (f (quote ("std"))) (k 0)))) (h "01sgpmd08byzdkj63wn5byq9jxmsxylcnnqaaqsb7m6ba3fc8dnp") (r "1.66.1")))

(define-public crate-zenoh-macros-0.11.0-rc.2 (c (n "zenoh-macros") (v "0.11.0-rc.2") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "zenoh-keyexpr") (r "^0.11.0-rc.2") (f (quote ("std"))) (k 0)))) (h "0d4lvax84f2l5il989z6hiw280li7zk2apzqkfjxjxiwr7zjk9pj") (r "1.66.1")))

(define-public crate-zenoh-macros-0.11.0-rc.3 (c (n "zenoh-macros") (v "0.11.0-rc.3") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "zenoh-keyexpr") (r "^0.11.0-rc.3") (f (quote ("std"))) (k 0)))) (h "1pgfqp1jdm4ky0865n8z2blai20415bgzmyq2fsq8b21fbd05jq9") (r "1.72.0")))

