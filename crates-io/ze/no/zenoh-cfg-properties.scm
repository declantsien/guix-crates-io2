(define-module (crates-io ze no zenoh-cfg-properties) #:use-module (crates-io))

(define-public crate-zenoh-cfg-properties-0.6.0-beta.1 (c (n "zenoh-cfg-properties") (v "0.6.0-beta.1") (d (list (d (n "zenoh-core") (r "^0.6.0-beta.1") (d #t) (k 0)) (d (n "zenoh-macros") (r "^0.6.0-beta.1") (d #t) (k 0)))) (h "01d4013q31z1fi6pfjw62l3780amrcs14fdf4qvgy8z64nq3p78a") (r "1.62.1")))

(define-public crate-zenoh-cfg-properties-0.7.0-rc (c (n "zenoh-cfg-properties") (v "0.7.0-rc") (d (list (d (n "zenoh-core") (r "^0.7.0-rc") (d #t) (k 0)) (d (n "zenoh-macros") (r "^0.7.0-rc") (d #t) (k 0)))) (h "0vh0k0m4a1gm8xc31362gbn6h5ghi603wcw9gpv686xzji8kjqx9") (r "1.62.1")))

(define-public crate-zenoh-cfg-properties-0.7.1-rc (c (n "zenoh-cfg-properties") (v "0.7.1-rc") (d (list (d (n "zenoh-result") (r "^0.7.1-rc") (f (quote ("default"))) (k 0)))) (h "0h6kr6gbcj9m8r3vy33j73dvasxs05xz13jr8ykbg9853bhgb10s") (y #t) (r "1.65.0")))

(define-public crate-zenoh-cfg-properties-0.7.2-rc (c (n "zenoh-cfg-properties") (v "0.7.2-rc") (d (list (d (n "zenoh-result") (r "^0.7.2-rc") (f (quote ("default"))) (k 0)))) (h "1yd08zfa61pmf2f3ds65qqlga0x7ppfkpddc6qfnvcnad6iavlq8") (r "1.65.0")))

