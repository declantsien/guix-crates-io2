(define-module (crates-io ze no zenoh-buffers) #:use-module (crates-io))

(define-public crate-zenoh-buffers-0.6.0-beta.1 (c (n "zenoh-buffers") (v "0.6.0-beta.1") (d (list (d (n "async-std") (r "=1.12.0") (k 0)) (d (n "bincode") (r "^1.3.3") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (o #t) (d #t) (k 0)) (d (n "shared_memory") (r "=0.12.4") (o #t) (d #t) (k 0)) (d (n "zenoh-collections") (r "^0.6.0-beta.1") (d #t) (k 0)) (d (n "zenoh-core") (r "^0.6.0-beta.1") (d #t) (k 0)))) (h "0008r6b932zpiiw62mvv1lx39gbkx9y6zmgchmnvjapiyhgj67gi") (f (quote (("shared-memory" "shared_memory" "serde" "log" "bincode")))) (r "1.62.1")))

(define-public crate-zenoh-buffers-0.7.0-rc (c (n "zenoh-buffers") (v "0.7.0-rc") (d (list (d (n "async-std") (r "=1.12.0") (k 0)) (d (n "bincode") (r "^1.3.3") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (o #t) (d #t) (k 0)) (d (n "shared_memory") (r "=0.12.4") (o #t) (d #t) (k 0)) (d (n "zenoh-collections") (r "^0.7.0-rc") (d #t) (k 0)) (d (n "zenoh-core") (r "^0.7.0-rc") (d #t) (k 0)))) (h "0236gay9zi3gd4bwxzqcnrqddd61zkyalgs8d7y56g4d4bqm8k94") (f (quote (("shared-memory" "shared_memory" "serde" "log" "bincode")))) (r "1.62.1")))

(define-public crate-zenoh-buffers-0.7.1-rc (c (n "zenoh-buffers") (v "0.7.1-rc") (d (list (d (n "rand") (r "^0.8.5") (o #t) (k 0)) (d (n "zenoh-collections") (r "^0.7.1-rc") (k 0)))) (h "0lvfzw1dqp5v88bfnjx8kdi5rjbw2fds9gh4awcwixxxmbacv392") (f (quote (("test" "rand")))) (y #t) (r "1.65.0")))

(define-public crate-zenoh-buffers-0.7.2-rc (c (n "zenoh-buffers") (v "0.7.2-rc") (d (list (d (n "rand") (r "^0.8.5") (o #t) (k 0)) (d (n "zenoh-collections") (r "^0.7.2-rc") (k 0)))) (h "0jf5sfj47vlcbdfdynxg4banxdxjrpxyh69jhh1333f1iz121a72") (f (quote (("test" "rand")))) (r "1.65.0")))

(define-public crate-zenoh-buffers-0.10.0-rc (c (n "zenoh-buffers") (v "0.10.0-rc") (d (list (d (n "rand") (r "^0.8.5") (o #t) (k 0)) (d (n "zenoh-collections") (r "^0.10.0-rc") (k 0)))) (h "0yr0mvphh4rp2340j7p4b5rs96zxc8fmg5j8rylk23axhy4wrv12") (f (quote (("test" "rand") ("std") ("shared-memory") ("default" "std")))) (r "1.66.1")))

(define-public crate-zenoh-buffers-0.10.1-rc (c (n "zenoh-buffers") (v "0.10.1-rc") (d (list (d (n "rand") (r "^0.8.5") (o #t) (k 0)) (d (n "zenoh-collections") (r "^0.10.1-rc") (k 0)))) (h "172x81sbz114haqj9f682rsjyriypqnfgzb2y1gdnah5w0gjdqh7") (f (quote (("test" "rand") ("std") ("shared-memory") ("default" "std")))) (r "1.66.1")))

(define-public crate-zenoh-buffers-0.11.0-rc.1 (c (n "zenoh-buffers") (v "0.11.0-rc.1") (d (list (d (n "rand") (r "^0.8.5") (o #t) (k 0)) (d (n "zenoh-collections") (r "^0.11.0-rc.1") (k 0)))) (h "0k21mcpb4n8xwcbymzjshl9n873xm47y859if84ciy3qql2mn9m9") (f (quote (("test" "rand") ("std") ("shared-memory") ("default" "std")))) (r "1.66.1")))

(define-public crate-zenoh-buffers-0.11.0-rc.2 (c (n "zenoh-buffers") (v "0.11.0-rc.2") (d (list (d (n "rand") (r "^0.8.5") (o #t) (k 0)) (d (n "zenoh-collections") (r "^0.11.0-rc.2") (k 0)))) (h "1jp64bka3nyrswpajgy4gjija6rx0m7gk8130y67lchxwaik4m4l") (f (quote (("test" "rand") ("std") ("shared-memory") ("default" "std")))) (r "1.66.1")))

(define-public crate-zenoh-buffers-0.11.0-rc.3 (c (n "zenoh-buffers") (v "0.11.0-rc.3") (d (list (d (n "rand") (r "^0.8.5") (o #t) (k 0)) (d (n "zenoh-collections") (r "^0.11.0-rc.3") (k 0)))) (h "1zn77n1z3dwgxaiwrsh0ls2wvq3p41yf05hcy2nacl4im35v971l") (f (quote (("test" "rand") ("std") ("shared-memory") ("default" "std")))) (r "1.72.0")))

