(define-module (crates-io ze no zenode) #:use-module (crates-io))

(define-public crate-zenode-0.1.0 (c (n "zenode") (v "0.1.0") (d (list (d (n "gql_client") (r "^1.0.6") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "p2panda-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "08pczzky0fw5a1471zx023pk5b3b8zf38drd4dvcq127hjkqsi6l")))

(define-public crate-zenode-0.2.0 (c (n "zenode") (v "0.2.0") (d (list (d (n "gql_client") (r "^1.0.6") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "p2panda-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0qgmdz27kbywp3isay4cyndx4a581vnkjm3nsq7kn9l8818x270c")))

(define-public crate-zenode-0.3.0 (c (n "zenode") (v "0.3.0") (d (list (d (n "gql_client") (r "^1.0.6") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "p2panda-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1xzxm1b11f6w6lkq6l7g4mx5jd5rfvq90jdb2aiwgmy9fybwvjkx")))

(define-public crate-zenode-0.3.4 (c (n "zenode") (v "0.3.4") (d (list (d (n "gql_client") (r "^1.0.6") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "p2panda-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1xr9yfrs3hxz8nva21lgqsfwrhdgv0ska67szx61qkaq43y00glv")))

