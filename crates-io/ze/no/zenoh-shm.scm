(define-module (crates-io ze no zenoh-shm) #:use-module (crates-io))

(define-public crate-zenoh-shm-0.0.0 (c (n "zenoh-shm") (v "0.0.0") (h "0j4ziw3b7qjydap9rrdli2jsgxav1y3rl1ama10iipa9wqdp5w2s") (y #t)))

(define-public crate-zenoh-shm-0.7.1-rc (c (n "zenoh-shm") (v "0.7.1-rc") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive" "default"))) (k 0)) (d (n "shared_memory") (r "^0.12.4") (d #t) (k 0)) (d (n "zenoh-buffers") (r "^0.7.1-rc") (d #t) (k 0)) (d (n "zenoh-result") (r "^0.7.1-rc") (k 0)))) (h "1r5vxlf88h4npzh6wvp64w0jbhnp69h6p7k8ziq1vjj1g53ffc2z") (y #t) (r "1.65.0")))

(define-public crate-zenoh-shm-0.7.2-rc (c (n "zenoh-shm") (v "0.7.2-rc") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive" "default"))) (k 0)) (d (n "shared_memory") (r "^0.12.4") (d #t) (k 0)) (d (n "zenoh-buffers") (r "^0.7.2-rc") (d #t) (k 0)) (d (n "zenoh-result") (r "^0.7.2-rc") (k 0)))) (h "12w8cn95lybkvf5904np9kfhlv36pqp9k64n74a208l4nl3s4ms8") (r "1.65.0")))

(define-public crate-zenoh-shm-0.10.0-rc (c (n "zenoh-shm") (v "0.10.0-rc") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive" "default"))) (k 0)) (d (n "shared_memory") (r "^0.12.4") (d #t) (k 0)) (d (n "zenoh-buffers") (r "^0.10.0-rc") (k 0)) (d (n "zenoh-result") (r "^0.10.0-rc") (k 0)))) (h "0nndvh64f3q2ll6x15h44lhydamgsdil73d16xf1fhcqgf2qidwm") (r "1.66.1")))

(define-public crate-zenoh-shm-0.10.1-rc (c (n "zenoh-shm") (v "0.10.1-rc") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive" "default"))) (k 0)) (d (n "shared_memory") (r "^0.12.4") (d #t) (k 0)) (d (n "zenoh-buffers") (r "^0.10.1-rc") (k 0)) (d (n "zenoh-result") (r "^0.10.1-rc") (k 0)))) (h "0ri2pzz47677wr5cwdzpq6r7y0dz2w0sfr6ksznlh0sf9wa006c4") (r "1.66.1")))

(define-public crate-zenoh-shm-0.11.0-rc.1 (c (n "zenoh-shm") (v "0.11.0-rc.1") (d (list (d (n "serde") (r "^1.0.154") (f (quote ("derive" "default"))) (k 0)) (d (n "shared_memory") (r "^0.12.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zenoh-buffers") (r "^0.11.0-rc.1") (k 0)) (d (n "zenoh-result") (r "^0.11.0-rc.1") (k 0)))) (h "0znkd5acng20kifr21j1wpqpb083f6irdbffhffiifga1d9hhv1v") (r "1.66.1")))

(define-public crate-zenoh-shm-0.11.0-rc.2 (c (n "zenoh-shm") (v "0.11.0-rc.2") (d (list (d (n "serde") (r "^1.0.154") (f (quote ("derive" "default"))) (k 0)) (d (n "shared_memory") (r "^0.12.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zenoh-buffers") (r "^0.11.0-rc.2") (k 0)) (d (n "zenoh-result") (r "^0.11.0-rc.2") (k 0)))) (h "0lm97bv5a955fm70mi17s717366w07kw7w66297d1508h0xgaqa4") (r "1.66.1")))

(define-public crate-zenoh-shm-0.11.0-rc.3 (c (n "zenoh-shm") (v "0.11.0-rc.3") (d (list (d (n "serde") (r "^1.0.154") (f (quote ("derive" "default"))) (k 0)) (d (n "shared_memory") (r "^0.12.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "zenoh-buffers") (r "^0.11.0-rc.3") (k 0)) (d (n "zenoh-result") (r "^0.11.0-rc.3") (k 0)))) (h "04rjiw3f5v304wf0260fc3jnnclnqmyh3l5hrrajy1nfwl0vh2a7") (r "1.72.0")))

