(define-module (crates-io ze no zenoh-ros-type) #:use-module (crates-io))

(define-public crate-zenoh-ros-type-0.1.0 (c (n "zenoh-ros-type") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)))) (h "19w1f6ahr491a7prq12lpfq6qy2ni57xrqvkqrr2hc933hb2g7wk")))

(define-public crate-zenoh-ros-type-0.2.0 (c (n "zenoh-ros-type") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)))) (h "0kagf0gncmdwxvfrfym7gvrg2da13jj62vp7ajwzyzplwqn5n71c")))

(define-public crate-zenoh-ros-type-0.2.1 (c (n "zenoh-ros-type") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)))) (h "13r556p9nsqpkspm6p97d6rzv3ikzpm47dnnlcl17jcampydf2p6")))

(define-public crate-zenoh-ros-type-0.3.0 (c (n "zenoh-ros-type") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)))) (h "0idjb3xkizcp40ychdvrm058aprcd1phm48x5qa148mlqgv3dvdv")))

(define-public crate-zenoh-ros-type-0.3.1 (c (n "zenoh-ros-type") (v "0.3.1") (d (list (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)))) (h "1jxqb6mm613gibnfdl33b3v3km4wzi9fas9cxprqnnvc3rcl0xhh")))

(define-public crate-zenoh-ros-type-0.3.2 (c (n "zenoh-ros-type") (v "0.3.2") (d (list (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)))) (h "1jijavk2fh3x1wv2p1gayvpbpp2cgria670ddh8qswpdfkjh1bry")))

(define-public crate-zenoh-ros-type-0.3.3 (c (n "zenoh-ros-type") (v "0.3.3") (d (list (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)))) (h "1zvlqdzzfkfgpkfir4gh3bzqnblicz4fczq84gwhf71vsymicj84")))

