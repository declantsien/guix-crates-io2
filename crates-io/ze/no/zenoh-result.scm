(define-module (crates-io ze no zenoh-result) #:use-module (crates-io))

(define-public crate-zenoh-result-0.0.0 (c (n "zenoh-result") (v "0.0.0") (h "0n0znwxlwsjw4k2r5lx4jbkaiyvdja05ilhinv9abbvrxmkkgwc7") (y #t)))

(define-public crate-zenoh-result-0.7.1-rc (c (n "zenoh-result") (v "0.7.1-rc") (d (list (d (n "anyhow") (r "^1.0.69") (k 0)))) (h "09a4mr3p0i38zx75hjr3xcjsb7ip0r9s7i1f75dpc4rchz2svd5i") (f (quote (("std" "anyhow/std") ("default" "std")))) (y #t) (r "1.65.0")))

(define-public crate-zenoh-result-0.7.2-rc (c (n "zenoh-result") (v "0.7.2-rc") (d (list (d (n "anyhow") (r "^1.0.69") (k 0)))) (h "1h1qc14zjkwavfwy1xibz9xh8cwqn3vm2wq9k49calcfrbk13bj2") (f (quote (("std" "anyhow/std") ("default" "std")))) (r "1.65.0")))

(define-public crate-zenoh-result-0.10.0-rc (c (n "zenoh-result") (v "0.10.0-rc") (d (list (d (n "anyhow") (r "^1.0.69") (k 0)))) (h "0nwpbcynh4i5ixxk9njgz0fcr945pdzzdf9zlkrpll1h36z5q6dy") (f (quote (("std" "anyhow/std") ("default" "std")))) (r "1.66.1")))

(define-public crate-zenoh-result-0.10.1-rc (c (n "zenoh-result") (v "0.10.1-rc") (d (list (d (n "anyhow") (r "^1.0.69") (k 0)))) (h "0n7970i6sqr9iq3z4m7apaq7yxcfyfd3h6ljcbf9dgqkvh4m4frx") (f (quote (("std" "anyhow/std") ("default" "std")))) (r "1.66.1")))

(define-public crate-zenoh-result-0.11.0-rc.1 (c (n "zenoh-result") (v "0.11.0-rc.1") (d (list (d (n "anyhow") (r "^1.0.69") (k 0)))) (h "09y0jxw93fhy7kvwacpfz5z7ym3f4b6sj0k0541nlkgx4y22la93") (f (quote (("std" "anyhow/std") ("default" "std")))) (r "1.66.1")))

(define-public crate-zenoh-result-0.11.0-rc.2 (c (n "zenoh-result") (v "0.11.0-rc.2") (d (list (d (n "anyhow") (r "^1.0.69") (k 0)))) (h "1xgmk5dgp6clj8plpkg7by2qm3fh097w4rz7w85pxpjljwfsyajw") (f (quote (("std" "anyhow/std") ("default" "std")))) (r "1.66.1")))

(define-public crate-zenoh-result-0.11.0-rc.3 (c (n "zenoh-result") (v "0.11.0-rc.3") (d (list (d (n "anyhow") (r "^1.0.69") (k 0)))) (h "1z9gj2jd89fgf2hkwvl9b5aahgz0mdzln94ahafbyqpxgpahqmyg") (f (quote (("std" "anyhow/std") ("default" "std")))) (r "1.72.0")))

