(define-module (crates-io ze no zeno) #:use-module (crates-io))

(define-public crate-zeno-0.1.0 (c (n "zeno") (v "0.1.0") (h "0nccn8nr1pbjx9iwk1lnr0a3sp6dha2n9gk5q7977cag9la7zkn5")))

(define-public crate-zeno-0.1.1 (c (n "zeno") (v "0.1.1") (h "1y0345vbx1vcfv9girqcw25wjir49as4pqfjpxqg59zcj84szx8q")))

(define-public crate-zeno-0.1.2 (c (n "zeno") (v "0.1.2") (h "0sbwiq6wdayv3zmafyjm36p6y6apvh38jvl5bi57ji3zi9ba1kbd")))

(define-public crate-zeno-0.2.0 (c (n "zeno") (v "0.2.0") (h "1j7n0pcipkh2rpwv7b6npc3dzgq5s4izklxhjfbr6zx6sirqaw7a")))

(define-public crate-zeno-0.2.1 (c (n "zeno") (v "0.2.1") (h "13lxgvj4kh77jgyxdnbz9scq282i9iq9g589853263091bngwqfd") (f (quote (("eval") ("default" "eval"))))))

(define-public crate-zeno-0.2.2 (c (n "zeno") (v "0.2.2") (h "0087hr8fzqx4fswj4bl2dvnlyhd21pgp1m8393fkx95kr44vl461") (f (quote (("eval") ("default" "eval"))))))

(define-public crate-zeno-0.2.3 (c (n "zeno") (v "0.2.3") (h "15z6l48wv4bhsrdvjxmnr3jxjmahkrz8qjg78n9gsrmrvghgh5fx") (f (quote (("eval") ("default" "eval"))))))

(define-public crate-zeno-0.3.0 (c (n "zeno") (v "0.3.0") (d (list (d (n "libm") (r "^0.2.7") (o #t) (k 0)))) (h "0blidjwc09cpj3g4ssvrs6vhaf65jgk90ciyhg57anck7mvkkizj") (f (quote (("std") ("eval") ("default" "eval" "std"))))))

(define-public crate-zeno-0.3.1 (c (n "zeno") (v "0.3.1") (d (list (d (n "libm") (r "^0.2.7") (o #t) (k 0)))) (h "0yvn3k9bizlfrlgl02z0piw8jl02aahzxv13zvcq85xnfximyfpa") (f (quote (("std") ("eval") ("default" "eval" "std")))) (s 2) (e (quote (("libm" "dep:libm"))))))

