(define-module (crates-io ze nr zenroom) #:use-module (crates-io))

(define-public crate-zenroom-0.3.0 (c (n "zenroom") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 2)))) (h "0mww6i80j7qj8slfhfnv8cj68p9g7sdr87fz8qdsnkj9szkpsb42")))

(define-public crate-zenroom-0.3.0-test (c (n "zenroom") (v "0.3.0-test") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 2)))) (h "110x57y39v4afgqvr1gijpb2wq3mhll50s2ywl181dz5af4snzvr")))

(define-public crate-zenroom-0.3.1 (c (n "zenroom") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 2)))) (h "0vk2pic8z43siqwxj1z2faw5w66ivklvdvvg3779v3qk7qw5ibac")))

(define-public crate-zenroom-0.3.2 (c (n "zenroom") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 2)))) (h "1pd76d745aw04zp4sns345dpny435k3gcxcgj0wh227c8qs4cxa1")))

(define-public crate-zenroom-0.3.3 (c (n "zenroom") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 2)))) (h "1nib4rl9660qv04g5jaa0pr2li6iqb225gqckbhyiagprj4x307i")))

