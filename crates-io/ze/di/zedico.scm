(define-module (crates-io ze di zedico) #:use-module (crates-io))

(define-public crate-zedico-0.1.0 (c (n "zedico") (v "0.1.0") (h "1imym241ds1kc1qi74jwgifknp5liml8x30jcck54gfggppkjz0s")))

(define-public crate-zedico-0.1.1 (c (n "zedico") (v "0.1.1") (h "0j316x5iaybrprwxfzd8x7m1aj0hc5xnlz4hv2a8bv460q2wdy8r")))

