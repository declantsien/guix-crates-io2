(define-module (crates-io ze di zedis) #:use-module (crates-io))

(define-public crate-zedis-0.1.101 (c (n "zedis") (v "0.1.101") (d (list (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "sled") (r "^0.28.0") (d #t) (k 0)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "0ldd3qp82ij3npjpccgkjva29m7w869fgk07v332nhf9iyycvfr5")))

(define-public crate-zedis-0.1.102 (c (n "zedis") (v "0.1.102") (d (list (d (n "actix-web") (r "^1.0.0") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.0") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sled") (r "^0.28.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.4") (d #t) (k 0)) (d (n "web-view") (r "^0.4.5") (d #t) (k 0)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "1zmya8fqmms3a97dnn4x69i048r0bm9zix2zvcbmds9jaxg2jrz3")))

