(define-module (crates-io ze di zedis-cli) #:use-module (crates-io))

(define-public crate-zedis-cli-0.1.0 (c (n "zedis-cli") (v "0.1.0") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.3") (d #t) (k 0)) (d (n "zmq") (r "^0.9") (d #t) (k 0)))) (h "174a15887hcxfzmzf5gmgrabyymdi0w24qijwqv95fb68rbjcl4q")))

