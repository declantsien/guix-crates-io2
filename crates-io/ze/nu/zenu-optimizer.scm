(define-module (crates-io ze nu zenu-optimizer) #:use-module (crates-io))

(define-public crate-zenu-optimizer-0.1.0 (c (n "zenu-optimizer") (v "0.1.0") (d (list (d (n "zenu-autograd") (r "^0.1.0") (d #t) (k 0)) (d (n "zenu-layer") (r "^0.1.0") (d #t) (k 0)) (d (n "zenu-matrix") (r "^0.1.0") (d #t) (k 0)))) (h "05aj1mvjqc1n7pb6b39310szsp0d5zqnw8s205mcdjflamin3lkj")))

