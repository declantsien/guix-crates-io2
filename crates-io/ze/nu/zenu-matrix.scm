(define-module (crates-io ze nu zenu-matrix) #:use-module (crates-io))

(define-public crate-zenu-matrix-0.1.0 (c (n "zenu-matrix") (v "0.1.0") (d (list (d (n "cblas") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (f (quote ("use_std"))) (k 2)) (d (n "openblas-src") (r "^0.10.8") (f (quote ("system" "cblas"))) (d #t) (k 0)))) (h "1pawcia7310grrqnl0qhi59dw3zmx8ihay84rhf1vyvzj431f6xy")))

(define-public crate-zenu-matrix-0.1.1 (c (n "zenu-matrix") (v "0.1.1") (d (list (d (n "cblas") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.10.0") (f (quote ("use_std"))) (k 2)) (d (n "openblas-src") (r "^0.10.8") (f (quote ("system" "cblas"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)))) (h "0xj8wcdiv44hhw1fq6fdb2pjdhzxg0zsdxwqi0qr09fj2b6nk90i")))

