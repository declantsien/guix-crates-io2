(define-module (crates-io ze nu zenu-autograd) #:use-module (crates-io))

(define-public crate-zenu-autograd-0.1.0 (c (n "zenu-autograd") (v "0.1.0") (d (list (d (n "zenu-matrix") (r "^0.1.0") (d #t) (k 0)))) (h "0r6myj4xka74cbpw9zp8jmwb5kil846fx0i20057shg7mhqha8q4")))

(define-public crate-zenu-autograd-0.1.1 (c (n "zenu-autograd") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "zenu-matrix") (r "^0.1.0") (d #t) (k 0)))) (h "1cgk5yx8gl5c48fk10p1rl71z42b60v5xdh0ry9dfis85y3hbjc0")))

