(define-module (crates-io ze nu zenu) #:use-module (crates-io))

(define-public crate-zenu-0.1.0 (c (n "zenu") (v "0.1.0") (d (list (d (n "zenu-matrix") (r "^0.1.0") (d #t) (k 0)))) (h "15vsyl45h4z0aw8g73ln4vx7pkn00wf6kzqsj0jdcslmf9cvys8d")))

(define-public crate-zenu-0.1.1 (c (n "zenu") (v "0.1.1") (d (list (d (n "flate2") (r "^1.0") (f (quote ("zlib"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zenu-autograd") (r "^0.1.0") (d #t) (k 0)) (d (n "zenu-layer") (r "^0.1.0") (d #t) (k 0)) (d (n "zenu-matrix") (r "^0.1.0") (d #t) (k 0)) (d (n "zenu-optimizer") (r "^0.1.0") (d #t) (k 0)))) (h "0yanjrhpixynlzs4cb3wd6iwv4nkilfrw6pnhzczkh8b88r3sbf3")))

