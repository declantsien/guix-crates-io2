(define-module (crates-io ze n_ zen_template) #:use-module (crates-io))

(define-public crate-zen_template-0.1.0 (c (n "zen_template") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "simple_logger") (r "^4") (d #t) (k 2)))) (h "0j82mghgnwf3zxgnm7whsfx4fx46wxxbw7zzx057yx57l0xmz8ga") (f (quote (("yaml" "serde" "serde_yaml") ("json" "serde" "serde_json") ("default") ("all" "json" "yaml"))))))

