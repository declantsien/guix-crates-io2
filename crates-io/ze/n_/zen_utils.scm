(define-module (crates-io ze n_ zen_utils) #:use-module (crates-io))

(define-public crate-zen_utils-0.1.0 (c (n "zen_utils") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "1zpgs3fnmr08bkc0a4vshxzk2n2dsqapz580yd7qwyclagn55g9h")))

(define-public crate-zen_utils-0.1.1 (c (n "zen_utils") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "0hqifwpv44n21ma94p8dhkvd9x1rvg7gnxcmxpzpz06cjw7m11fd")))

(define-public crate-zen_utils-0.1.2 (c (n "zen_utils") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "07h0xr917mwk9w877hk0kpgg5xidyqsgqqnvvvqxablkcvmaldq3")))

(define-public crate-zen_utils-0.1.3 (c (n "zen_utils") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "0d8pip7fhgi3rc4zvfd1983qlzhnh0nrp3241pc5210wf00fmhmk")))

