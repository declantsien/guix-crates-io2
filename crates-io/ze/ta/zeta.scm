(define-module (crates-io ze ta zeta) #:use-module (crates-io))

(define-public crate-zeta-0.1.0 (c (n "zeta") (v "0.1.0") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "19594fgc7dyqsgbyxw5nw28iv7h626z8jq263099rksisxjkfix1") (y #t)))

(define-public crate-zeta-0.1.1 (c (n "zeta") (v "0.1.1") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1nc0khqy7qi5jdf2ylv22klnygipk0x79dvr7k6q1srbir3qrrkv") (y #t)))

(define-public crate-zeta-0.1.2 (c (n "zeta") (v "0.1.2") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0hh3xhacc06bkrk8w8jpkjnm753p9yqblhglkkfhb3lqdvjs2wvg") (y #t)))

(define-public crate-zeta-0.1.3 (c (n "zeta") (v "0.1.3") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "08qlyza56rfd1ysh7d9xdfjrfgba5zn3q7qnl8q07yjdc4xhmzx3") (y #t)))

(define-public crate-zeta-0.1.4 (c (n "zeta") (v "0.1.4") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "09xcngssb4yvgphi8hf0q4876lxban1dq17f25bpnm08j02zf3g4") (y #t)))

(define-public crate-zeta-0.1.5 (c (n "zeta") (v "0.1.5") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0ansh0fh4yz10sw1nza4hy689cdp9yf9cgwzsy8qw6s6wy8pqdi2") (y #t)))

(define-public crate-zeta-0.1.6 (c (n "zeta") (v "0.1.6") (d (list (d (n "cc") (r "^1.0.45") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1lqaf3m884v4wbj8x1j2yckd2p7p1n3gyfwvgn74mqmp7bl3k8fk") (y #t)))

