(define-module (crates-io ze ta zeta-abi) #:use-module (crates-io))

(define-public crate-zeta-abi-0.1.0 (c (n "zeta-abi") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.26.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4.0") (d #t) (k 0)) (d (n "chainlink_solana") (r "^1.0.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.13.5") (d #t) (k 0)))) (h "0in5pjcclh0g7zgkvpm4dyb4gdlk7g3a5njifr2l7jq5qfyjcgrd") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("mainnet") ("default" "no-entrypoint" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-zeta-abi-0.1.1 (c (n "zeta-abi") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.26.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.4.0") (d #t) (k 0)) (d (n "chainlink_solana") (r "^1.0.0") (d #t) (k 0)) (d (n "solana-program") (r "^1.14.16") (d #t) (k 0)))) (h "1miw2f5kswha2ls67r66ssg0xh2c5bn67j5qgsp4cg6f3yvqzzcp") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("mainnet") ("default" "no-entrypoint" "cpi") ("cpi" "no-entrypoint"))))))

