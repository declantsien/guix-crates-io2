(define-module (crates-io ze lf zelf) #:use-module (crates-io))

(define-public crate-zelf-0.0.1 (c (n "zelf") (v "0.0.1") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 2)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)))) (h "1raxbbi2m4zmina4nwh7jy62zs6fh684rm89pgiw3fr5wmcmzkpw") (y #t)))

(define-public crate-zelf-0.1.0 (c (n "zelf") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 2)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 2)))) (h "10lhnkz42ngv5m1ciq373krv3644l9sgfi584ax1ifqp0ll8i1hf")))

