(define-module (crates-io ze ld zelda) #:use-module (crates-io))

(define-public crate-zelda-0.1.0 (c (n "zelda") (v "0.1.0") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1d8f5bqi0jnrihrg3b21qs5frp8pgvmpsdj5hxdpicwksrhg8sxg")))

(define-public crate-zelda-0.1.1 (c (n "zelda") (v "0.1.1") (d (list (d (n "bincode") (r "^1.2.0") (d #t) (k 0)) (d (n "chashmap") (r "^2.2.2") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14x2pv1m8v6kj092w3crrw8cn7an0y8p8ds4gm2hi77wzb9d7qj2")))

