(define-module (crates-io ze rn zernike) #:use-module (crates-io))

(define-public crate-zernike-0.1.0 (c (n "zernike") (v "0.1.0") (d (list (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "0bhibsxh7268q9rwqxk4yz5z2ql99vbbr1z3avc6n0lbc6z6d136")))

(define-public crate-zernike-0.1.1 (c (n "zernike") (v "0.1.1") (d (list (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "0z32diinh4jd2gcpjhbh5320hsh778mpvjwbh7hlz1ypqhxxnq64")))

(define-public crate-zernike-0.2.0 (c (n "zernike") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.14.0") (d #t) (k 2)) (d (n "ndarray-npy") (r "^0.7.1") (d #t) (k 2)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "0znsdndf4wwicn42vb2x1jmd584p7171yrkq76bls78pwvpnmb7k")))

(define-public crate-zernike-0.2.1 (c (n "zernike") (v "0.2.1") (d (list (d (n "ndarray") (r "^0.14.0") (d #t) (k 2)) (d (n "ndarray-npy") (r "^0.7.1") (d #t) (k 2)) (d (n "plotters") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "1jdc78ks77h50lfs2nynxnlp5vpnn0xbjxh2n9mwrd8sz8il3ihb")))

