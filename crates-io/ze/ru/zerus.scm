(define-module (crates-io ze ru zerus) #:use-module (crates-io))

(define-public crate-zerus-0.2.0 (c (n "zerus") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "rustls-tls"))) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "16i94z32zja8zrq5b0igpfjx0pf2q7bkngd9wlpfd5gjsfmnnd39")))

(define-public crate-zerus-0.3.0 (c (n "zerus") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "guppy") (r "^0.15.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "rustls-tls"))) (k 0)))) (h "1wajgv8zpjzc02bdmwwyf1g2s9fbqvxfcn5kmhqgpv2sy6qirhig")))

(define-public crate-zerus-0.4.0 (c (n "zerus") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "guppy") (r "^0.15.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking" "rustls-tls"))) (k 0)))) (h "1fyp1cf1irsfnj881fd9an9f3jii87lz2p7d4ffvkykly18cj6yz")))

(define-public crate-zerus-0.5.0 (c (n "zerus") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "guppy") (r "^0.17.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "rustls-tls"))) (k 0)))) (h "0wl0s3yixjwpkqnf1vrn7xqhxg2ns3xdva8pzy5mfnvjm8cfw72h")))

(define-public crate-zerus-0.6.0 (c (n "zerus") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^4.5.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "guppy") (r "^0.17.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "rustls-tls"))) (k 0)))) (h "0haa1ckdkbcw2rdji0drbbxj2ny0k9d867j9ig79rd6vlyikg9f0")))

(define-public crate-zerus-0.7.0 (c (n "zerus") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.18.3") (f (quote ("vendored-libgit2" "vendored-openssl"))) (d #t) (k 0)) (d (n "guppy") (r "^0.17.5") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.3") (f (quote ("blocking" "rustls-tls" "http2"))) (k 0)))) (h "17ila213rj2j1dqmb80ga122rmjhgfbpa9illkq157z91ka3v0ad")))

