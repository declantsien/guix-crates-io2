(define-module (crates-io ze ke zeke) #:use-module (crates-io))

(define-public crate-zeke-0.1.0 (c (n "zeke") (v "0.1.0") (h "0jsw81b657m84vz13p15q34kvljd1dkxyisx7xnhgrgqjk5i31mf")))

(define-public crate-zeke-0.1.1 (c (n "zeke") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03gzxdfagpccxsbz18fi2fck82bsw63k2xwk0spkij70a9pbakm3")))

(define-public crate-zeke-0.1.2 (c (n "zeke") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ai8fwgab7gn1vry2rqpf7v1yd94j5jc8j188q8aypx371yfr58h")))

