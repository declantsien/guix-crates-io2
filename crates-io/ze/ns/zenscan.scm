(define-module (crates-io ze ns zenscan) #:use-module (crates-io))

(define-public crate-zenscan-0.1.0 (c (n "zenscan") (v "0.1.0") (d (list (d (n "clap") (r "^2.27.0") (d #t) (k 0)))) (h "1l20asamdkxqysaqfzwvfpjj6msv31qmsr4n4lz0p3rymmmg4v89")))

(define-public crate-zenscan-0.1.1 (c (n "zenscan") (v "0.1.1") (d (list (d (n "clap") (r "^2.27.0") (d #t) (k 0)))) (h "0rhxixhhg0q29bbdn7x6p6rkzr46maafw489icc2lysl3hyry8x1")))

(define-public crate-zenscan-0.1.2 (c (n "zenscan") (v "0.1.2") (d (list (d (n "clap") (r "^2.27.0") (d #t) (k 0)))) (h "0za14khwhak1c8ha6fgdd380cwh55cfxsi5ijxjgf334x2fdqza3")))

(define-public crate-zenscan-0.1.3 (c (n "zenscan") (v "0.1.3") (d (list (d (n "better-panic") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^2.27.0") (d #t) (k 0)) (d (n "dns-lookup") (r "^1.0.3") (d #t) (k 0)) (d (n "pnet") (r "^0.26.0") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)))) (h "1q6npgvzrv50s5rmjcqr68nmq5q04crk478wckmv782q615hnqiv")))

