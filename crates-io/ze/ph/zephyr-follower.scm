(define-module (crates-io ze ph zephyr-follower) #:use-module (crates-io))

(define-public crate-zephyr-follower-0.1.0 (c (n "zephyr-follower") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jyxy5g2jnw0psclddpja6ij13fva7nnbfz07n283hb9yhv1i938")))

(define-public crate-zephyr-follower-0.1.1 (c (n "zephyr-follower") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1h8a91p63yh1y61vpmgr0z13kbfsqywjyxh6yba1v5brjd53mb00")))

