(define-module (crates-io ze ph zephyr-config) #:use-module (crates-io))

(define-public crate-zephyr-config-0.1.0 (c (n "zephyr-config") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)))) (h "14gl6rh4r093i4l85gqhb8gc06llvpcg4n0ch9daxlhgksv4kisi")))

(define-public crate-zephyr-config-0.1.1 (c (n "zephyr-config") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)))) (h "007xbr3416abv22iqzqkgml4xqj1szdggr0qjb1s6gn3x9mr9h58")))

