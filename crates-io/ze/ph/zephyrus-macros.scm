(define-module (crates-io ze ph zephyrus-macros) #:use-module (crates-io))

(define-public crate-zephyrus-macros-0.2.1 (c (n "zephyrus-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1xwvq45kk7a4h5kbnnp89cz1ldr6s24hdx627xy6vb78a19z31cq")))

(define-public crate-zephyrus-macros-0.2.2 (c (n "zephyrus-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1jywbdd5sd1271g0vm8mbi0p3y9iapg81b388zlw0yq0jn170sks")))

(define-public crate-zephyrus-macros-0.2.3 (c (n "zephyrus-macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0spspp70kirpy0kl1kkbj6i431r4kq5saqcdvj6m8lsjiib6a99y")))

(define-public crate-zephyrus-macros-0.2.5 (c (n "zephyrus-macros") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "16j6244hkgfyh99psnf6hz5vgfwn39lm8kklv3lyl0zaxp03c72r")))

(define-public crate-zephyrus-macros-0.2.8 (c (n "zephyrus-macros") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1p577ma4sb2r0hc06sgvhas7znz231haprjk7s3c23x8jxxj3pak")))

(define-public crate-zephyrus-macros-0.3.0 (c (n "zephyrus-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1m81if00ciclxi6lp6nhca8qp3prsx2pdwvp9348ppi7312lswz3")))

(define-public crate-zephyrus-macros-0.3.1 (c (n "zephyrus-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "043582n49i957a9n05d4687qxv08rwqxzs5jnz9cfksns3api21q")))

(define-public crate-zephyrus-macros-0.3.2 (c (n "zephyrus-macros") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "12cjchjdz4ja6hdixldlqx6m6wgx413bbq55lkzs4g6w12yrbdi4")))

(define-public crate-zephyrus-macros-0.4.0 (c (n "zephyrus-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0bq4w4cddwdcrcnpd6cb31f35mlzb2xwg3rwnvg96fbsd26gd31b")))

(define-public crate-zephyrus-macros-0.5.0 (c (n "zephyrus-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0niaw5g7npm2c733gsak2dns1c4c5drh7zhpynd3agql0ba8jsjs")))

(define-public crate-zephyrus-macros-0.6.0 (c (n "zephyrus-macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1fvwfgf0dzdj7r2gk91gryhxc64an54bfv291fr4dzjv83lgdrjn")))

(define-public crate-zephyrus-macros-0.8.0 (c (n "zephyrus-macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0si1s07cgdd8zhrqxjjrp72r8iz5v3k7l3877jkcp18v2q88hask")))

(define-public crate-zephyrus-macros-0.9.0 (c (n "zephyrus-macros") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "1vmci9r9q1vfnkhfj23y58wmxhmw6vh0dgr5rdkd3qyggxc27ny1")))

(define-public crate-zephyrus-macros-0.10.0 (c (n "zephyrus-macros") (v "0.10.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0f82zkr3mrc4sqyakvz23w7vbp5dqckqran0bvmkn5qssijxryml")))

(define-public crate-zephyrus-macros-0.11.0 (c (n "zephyrus-macros") (v "0.11.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0ssks27dnbp0yg7wfsm9j51vvbx90v5i8lg1ihvf5zdcclld1jjq")))

