(define-module (crates-io ze nv zenv) #:use-module (crates-io))

(define-public crate-zenv-0.1.0 (c (n "zenv") (v "0.1.0") (d (list (d (n "pico-args") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "113hlil3b51shcw7p2vmpfcq1lj3ijvzp2qnxx20jph5sbhnrd7d") (f (quote (("cli" "pico-args"))))))

(define-public crate-zenv-0.2.1 (c (n "zenv") (v "0.2.1") (d (list (d (n "pico-args") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "1vsq8wkrwr05h3ipl6pw7w6iqzlgzfv3b105bn1sabcj58wk3bcp") (f (quote (("cli" "pico-args"))))))

(define-public crate-zenv-0.3.0 (c (n "zenv") (v "0.3.0") (d (list (d (n "pico-args") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "0fvq1na5h09vznmcrjfrlfrgdsjk9c3cm6s61di9z2c90myvcmdj") (f (quote (("cli" "pico-args"))))))

(define-public crate-zenv-0.4.0 (c (n "zenv") (v "0.4.0") (d (list (d (n "pico-args") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "0xbjrzw7mvpyhs316kalfw0458rwxnj8rrn9i2v317mk4spm6s3y") (f (quote (("cli" "pico-args"))))))

(define-public crate-zenv-0.5.0 (c (n "zenv") (v "0.5.0") (d (list (d (n "pico-args") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "1f97pkibgaldshc09f6dri893swrkxsgw89pgfij0v2wdw6r6rxz") (f (quote (("cli" "pico-args"))))))

(define-public crate-zenv-0.6.0 (c (n "zenv") (v "0.6.0") (d (list (d (n "pico-args") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "0vixpy21ha1m474sg7k0h6c7vv1lzrwliyzwpbjyx7lxig36cilh") (f (quote (("cli" "pico-args"))))))

(define-public crate-zenv-0.7.1 (c (n "zenv") (v "0.7.1") (d (list (d (n "pico-args") (r "^0.4.2") (o #t) (d #t) (k 0)))) (h "0rqg2xfrjbdgrhw8vapnkfzy6nzxkbvh7c1s8gzs5qpkpsswr851") (f (quote (("cli" "pico-args"))))))

(define-public crate-zenv-0.8.0 (c (n "zenv") (v "0.8.0") (d (list (d (n "lexopt") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "01v0k164aqi7ws3cd5184bbsyja6a43jjs49pwnl8jgya42815lp") (f (quote (("cli" "lexopt"))))))

