(define-module (crates-io ze ti zetik-tailwind) #:use-module (crates-io))

(define-public crate-zetik-tailwind-0.1.0 (c (n "zetik-tailwind") (v "0.1.0") (h "0w40z4pirkc4z5qm8w91p3mv64jk10axwssnngm473yhw2ij3rcq")))

(define-public crate-zetik-tailwind-0.1.1 (c (n "zetik-tailwind") (v "0.1.1") (h "1j66a8pr1nahwl84c97c1rkq0j2nk2bsyk5k88vh4ggi9wxhmaaw")))

(define-public crate-zetik-tailwind-0.1.2 (c (n "zetik-tailwind") (v "0.1.2") (h "077z7lkr1sld8ghjwzf3vbdn1har9mdvs1whl4wz67k1a52hjl67")))

(define-public crate-zetik-tailwind-0.1.3 (c (n "zetik-tailwind") (v "0.1.3") (h "0jyc17i2wf8i4xqnlad54lqp7z3dwk3hwpvzawsp1hsvqzx9ssww")))

