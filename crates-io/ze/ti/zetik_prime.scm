(define-module (crates-io ze ti zetik_prime) #:use-module (crates-io))

(define-public crate-zetik_prime-0.1.0 (c (n "zetik_prime") (v "0.1.0") (h "1kdi9a86afgdfravr11x51w9m0d30id58jldpaccgjalwr40lqpx")))

(define-public crate-zetik_prime-0.1.1 (c (n "zetik_prime") (v "0.1.1") (h "0kfwyxa6gmsq4h30d9w07i1p7snljpsgin93lyvjhzs055qr52d2")))

(define-public crate-zetik_prime-0.2.0 (c (n "zetik_prime") (v "0.2.0") (h "1casfcj4ncjvqk8j097c5iiyg7rx8xx4mzrvwa30ydilxfl15z8q")))

(define-public crate-zetik_prime-0.2.1 (c (n "zetik_prime") (v "0.2.1") (h "17r0ggy7116vdndgf6xbcv5yxf9894pjsssbnl20qzn3hvi707h6")))

