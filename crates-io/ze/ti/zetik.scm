(define-module (crates-io ze ti zetik) #:use-module (crates-io))

(define-public crate-zetik-0.0.1 (c (n "zetik") (v "0.0.1") (h "1i8v9fdh5b6qx0b4s9h4hwz23c6qlm5asgkn7fpb0kdrglnqpqix")))

(define-public crate-zetik-0.0.2 (c (n "zetik") (v "0.0.2") (d (list (d (n "num-integer") (r "^0.1") (d #t) (k 0)))) (h "1sa6fc1s69j3w98yc1w9rlqhw3abkswpcnpzi84m23xc5w8557x9")))

(define-public crate-zetik-0.0.3 (c (n "zetik") (v "0.0.3") (d (list (d (n "num-integer") (r "^0.1") (d #t) (k 0)))) (h "012n87i5870793i1195jif77cbg23xz0n6d0c3xiilcsz7pq7l9s")))

(define-public crate-zetik-0.0.4 (c (n "zetik") (v "0.0.4") (d (list (d (n "num-integer") (r "^0.1") (d #t) (k 0)))) (h "1i0mjql13cgpwmwdbsn3ix1v1rdlcaqcaik7iag34kynpqhajd0x")))

(define-public crate-zetik-0.0.5-fen (c (n "zetik") (v "0.0.5-fen") (d (list (d (n "num-integer") (r "^0.1") (d #t) (k 0)))) (h "0rds8hgg8ib8nhlc9x0610rjasx8xnvbll5p2342xvy3sq271qxf")))

(define-public crate-zetik-0.0.5 (c (n "zetik") (v "0.0.5") (d (list (d (n "num-integer") (r "^0.1") (d #t) (k 0)))) (h "0zacrpgbz1vpr6ps74c210dgs0x61gm5hfxkrk50ilyv6920xkvv")))

(define-public crate-zetik-0.0.6 (c (n "zetik") (v "0.0.6") (d (list (d (n "num-integer") (r "^0.1") (d #t) (k 0)))) (h "0lwpvhhqshsrpkz8a4c2ga67wz1xkaj0dzcw210yamg0aph2j6h7")))

(define-public crate-zetik-0.0.7 (c (n "zetik") (v "0.0.7") (h "0yn7ligln9s7gh6m3mzf28l1vshh7phgdzmqxbjp80qgj36q0ixb")))

(define-public crate-zetik-0.0.8 (c (n "zetik") (v "0.0.8") (h "1qbgxj7y4db51sm903nqkwjaiq0rvasi1x17vrr24z1vpd5jnh10")))

(define-public crate-zetik-0.0.8-walter (c (n "zetik") (v "0.0.8-walter") (h "1l2sb5s3kbx9463w0ml7hzl14m4wjl8b7bficw3pb6g6v12fww0b")))

(define-public crate-zetik-0.0.9-jesse (c (n "zetik") (v "0.0.9-jesse") (h "1cjva2w9m0l07wnwa3divqvps4a468l1aa52jacxs7w712xsiwqb")))

(define-public crate-zetik-0.0.9 (c (n "zetik") (v "0.0.9") (h "0g9jp7p2pbyw9gixl8h4ps4cpfl0gr8awnqsy4h2b6szxyy5f9j0")))

(define-public crate-zetik-0.0.10 (c (n "zetik") (v "0.0.10") (h "04hni6fk4vnqaqv1qr17bb3w93wspck4pd4q7m5w8aiw63z9d2mb")))

(define-public crate-zetik-0.0.11 (c (n "zetik") (v "0.0.11") (h "0hgr5i5na0355w0b5ka81ghkyy2diddnk4ch1yg1j8flns45b7f5")))

(define-public crate-zetik-0.0.12 (c (n "zetik") (v "0.0.12") (h "1czk4mqjl4j5l36hp2rlmvlmak67130l18qfifm2nbdlsdw4cyfd")))

(define-public crate-zetik-0.0.13 (c (n "zetik") (v "0.0.13") (h "1dqhn9rvh1mzjcaimfxh6ijwy6y34cd6179wa03nxilajbcxf65b")))

