(define-module (crates-io ze dm zedmq) #:use-module (crates-io))

(define-public crate-zedmq-0.1.0 (c (n "zedmq") (v "0.1.0") (h "07424crhl7qxi3g8j12dkgrmzwq1lackwdhhxy8msbs50877jnk4")))

(define-public crate-zedmq-0.2.0 (c (n "zedmq") (v "0.2.0") (h "0q1y66r43h4awj36z0fxk79hb8d3q8pmlqpv6ap90dhb74c3rgvy")))

(define-public crate-zedmq-0.2.1 (c (n "zedmq") (v "0.2.1") (h "15hklc8kfs7ld63h59fnbdywphv8zsvgkjijcjvsmx5w74khq8mg")))

(define-public crate-zedmq-0.3.0 (c (n "zedmq") (v "0.3.0") (h "16fxnzz437xkgnybfj11aan0jxfzgz5sszzfz7hfb5m8ai8dr7ch")))

(define-public crate-zedmq-0.4.0 (c (n "zedmq") (v "0.4.0") (d (list (d (n "zmq") (r ">=0.9.2, <0.10.0") (d #t) (k 2)))) (h "0wmfryiibc1q1485mhjnzgh24ycydx90dglymqg9z9nf7nd39dbr")))

(define-public crate-zedmq-0.5.0 (c (n "zedmq") (v "0.5.0") (d (list (d (n "zmq") (r "^0.9.2") (d #t) (k 2)))) (h "09a1nkl662wj5azmjdmqmlhy97idbha3k2pkf9asm8p999z0f63h")))

(define-public crate-zedmq-0.6.0 (c (n "zedmq") (v "0.6.0") (d (list (d (n "zmq") (r "^0.9.2") (d #t) (k 2)))) (h "0g1799h15a6gvbg465zn0l69jq177p9djcgf0ahdlfb7cfkmdd9p")))

(define-public crate-zedmq-0.7.0 (c (n "zedmq") (v "0.7.0") (d (list (d (n "zmq") (r "^0.9.2") (d #t) (k 2)))) (h "06myxkaxyl7gwaqg6n5fzan63xhf9zaipyzb6br9l5g1vk74ji6x")))

