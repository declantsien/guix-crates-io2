(define-module (crates-io ze nn zenn) #:use-module (crates-io))

(define-public crate-zenn-0.1.0 (c (n "zenn") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.30.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1w7f2xqxpbs4appsn1wvldgbdfnsg5lf66x9xklpzqq9km51dwfl")))

