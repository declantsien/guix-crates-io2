(define-module (crates-io ze us zeus-fm) #:use-module (crates-io))

(define-public crate-zeus-fm-0.1.0 (c (n "zeus-fm") (v "0.1.0") (d (list (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.10") (d #t) (k 0)))) (h "0jz66ni7vpw37c22lkqj5zbzpx9rblryy62gvxgi0li2jy4pps88")))

(define-public crate-zeus-fm-0.1.1 (c (n "zeus-fm") (v "0.1.1") (d (list (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.10") (d #t) (k 0)))) (h "1p659majyba5zpxqr0y366m8g0skhb7b6zdl4nsd2sjrlnfm1wr1")))

(define-public crate-zeus-fm-0.1.2 (c (n "zeus-fm") (v "0.1.2") (d (list (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.10") (d #t) (k 0)))) (h "05b6fz6jbn69vzd51g8bximdm97as21xr6764c2z9q2w42mipjqb")))

