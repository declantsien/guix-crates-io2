(define-module (crates-io ze nt zentime-rs-timer) #:use-module (crates-io))

(define-public crate-zentime-rs-timer-0.3.4 (c (n "zentime-rs-timer") (v "0.3.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "rodio") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1q38i0q4jvhydi103nd5szyakavv7ah65mpxs43p01inlzavmg83")))

(define-public crate-zentime-rs-timer-0.4.0 (c (n "zentime-rs-timer") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0spfpp1zzn1gdqsn127rf7rdgnfh8iyn33vsc200jh0cxz37dqya")))

(define-public crate-zentime-rs-timer-0.5.0 (c (n "zentime-rs-timer") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jhbm6ffd17mqncp0jz5icxbk5hfvybvpxp1gzc2ws57fjkywrln")))

(define-public crate-zentime-rs-timer-0.6.0 (c (n "zentime-rs-timer") (v "0.6.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vj6mpccmwvm694v3gvpni8d66wmh900bnpybwd7idk6k0vfhdd3")))

(define-public crate-zentime-rs-timer-0.7.0 (c (n "zentime-rs-timer") (v "0.7.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1d0i3x7wq794mxz807hq3nk1j4sqcwz60gcgj80nzc55qwkblmzs")))

(define-public crate-zentime-rs-timer-0.8.0 (c (n "zentime-rs-timer") (v "0.8.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "11jxb285mkn312jpfn0zf6dpi0dicz9rpxfaj2rk857fr06d92mh")))

(define-public crate-zentime-rs-timer-0.8.1 (c (n "zentime-rs-timer") (v "0.8.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0z0ph0iza0fkl2dsxgg93h81y8wzld3ba5f1xng6a3p72sq2kkqf")))

(define-public crate-zentime-rs-timer-0.9.0 (c (n "zentime-rs-timer") (v "0.9.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "14ngipsysjpvj2w9z2jpri1k9rm1b7dikh66hsqccvskhgkqnyy6")))

(define-public crate-zentime-rs-timer-0.10.0 (c (n "zentime-rs-timer") (v "0.10.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "09s7cwsx9gn7hs0aqjybcwghjra9hvbfxrr45xq7x44h8xash0wr")))

(define-public crate-zentime-rs-timer-0.11.0 (c (n "zentime-rs-timer") (v "0.11.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1kk0fqk3b56xkya4g8770ylcvjz2hppz6z4dvwkxgzimsqc6496d")))

(define-public crate-zentime-rs-timer-0.12.0 (c (n "zentime-rs-timer") (v "0.12.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1fdrdmv27my41rpql198zyas718lydvnyjl7ib92a09nwmjjgi6p")))

(define-public crate-zentime-rs-timer-0.13.0 (c (n "zentime-rs-timer") (v "0.13.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0d0ggxhpy3q4i4r6x1vklcsp6mrdxq5c5v0hlxhdajmnhd1p0fby")))

