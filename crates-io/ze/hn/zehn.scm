(define-module (crates-io ze hn zehn) #:use-module (crates-io))

(define-public crate-zehn-0.1.0 (c (n "zehn") (v "0.1.0") (h "0p47rcchi2ibdv22xinjdac2qmglvd546whrsgda2v7q8ly9a68k")))

(define-public crate-zehn-0.1.1 (c (n "zehn") (v "0.1.1") (d (list (d (n "indexmap") (r "^2.2.5") (d #t) (k 0)))) (h "11mh9mi76yc78p6k4v6cmr76sbczqj9lrz9fkgq68mznyi3fd9s4")))

