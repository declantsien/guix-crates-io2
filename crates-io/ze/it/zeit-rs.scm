(define-module (crates-io ze it zeit-rs) #:use-module (crates-io))

(define-public crate-zeit-rs-0.1.0 (c (n "zeit-rs") (v "0.1.0") (h "0mjk8f8nkbm2nj5ggdjhdy2ld9wn40z5j1gcypn531fkin7bj0nb")))

(define-public crate-zeit-rs-0.1.1 (c (n "zeit-rs") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "082kvjbas8x3axflk5cdbapw999wqk63yd1jx8ayfk1zjih7vp01")))

(define-public crate-zeit-rs-0.1.2 (c (n "zeit-rs") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "log4rs") (r "^0.5") (d #t) (k 0)) (d (n "toml") (r "^0.2") (d #t) (k 0)))) (h "1xbqw52zald1pkm2hz1dqxvm0wald2piw60cvwvv386mrrm4yg9s")))

