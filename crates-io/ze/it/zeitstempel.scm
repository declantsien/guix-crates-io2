(define-module (crates-io ze it zeitstempel) #:use-module (crates-io))

(define-public crate-zeitstempel-0.0.0 (c (n "zeitstempel") (v "0.0.0") (h "15hhywnl3ld0gashp9k5ghn9kcnljqnd99x8dy53z8fd7hzz384l")))

(define-public crate-zeitstempel-0.1.0 (c (n "zeitstempel") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\", target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "078zlsb1hmi9kgjg7sx6i8bnxkh2sb1v03653j6wia57syd7z0zj") (f (quote (("win10plus"))))))

(define-public crate-zeitstempel-0.1.1 (c (n "zeitstempel") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\", target_os = \"macos\", target_os = \"ios\"))") (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1y16x2li3217nbqxcknxby20l6652wwqsdjr9wvlxlhflfv3xspf") (f (quote (("win10plus"))))))

