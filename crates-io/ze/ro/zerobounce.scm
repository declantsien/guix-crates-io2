(define-module (crates-io ze ro zerobounce) #:use-module (crates-io))

(define-public crate-zerobounce-1.0.0 (c (n "zerobounce") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "13l2c4j0xad0ia980wzj9kqi37iswyhwbbgzbzmgpqgfnr7zpmrs")))

