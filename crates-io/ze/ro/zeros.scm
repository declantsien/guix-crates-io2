(define-module (crates-io ze ro zeros) #:use-module (crates-io))

(define-public crate-zeros-0.0.1 (c (n "zeros") (v "0.0.1") (h "05a6y57rvhw4fwmdcy5wl7mj8yl3br42d305dvli1vkn5q7q07xy")))

(define-public crate-zeros-0.1.0 (c (n "zeros") (v "0.1.0") (h "0ly9mfpp89yv5hyxl2vdx60ak73xlr3c4mrfmny082044al6lc72")))

(define-public crate-zeros-0.2.0 (c (n "zeros") (v "0.2.0") (h "1awgvb1qaj5h33ayn5igi4r38ji0drs1vxd61gf1251m5dmivmxh")))

(define-public crate-zeros-0.3.0 (c (n "zeros") (v "0.3.0") (h "03fb548njazxl19rj72yvpf4rr1p0p0528pa1dwfzhb33myprj0k")))

(define-public crate-zeros-0.4.0 (c (n "zeros") (v "0.4.0") (d (list (d (n "tiny-keccak") (r "^1") (d #t) (k 2)))) (h "1njf06fpad59ybcz360p7zj8rdr7hbnmnih8lr6xjd9028lxpwz4")))

(define-public crate-zeros-1.0.0 (c (n "zeros") (v "1.0.0") (d (list (d (n "tiny-keccak") (r "^1") (d #t) (k 2)))) (h "1n6f4v3i4bvpdqn6dyyv83jwfbq73k70bjv0s5a7k2p2k2px3cmz")))

(define-public crate-zeros-2.0.0 (c (n "zeros") (v "2.0.0") (d (list (d (n "tiny-keccak") (r "^1") (d #t) (k 2)))) (h "1nzrlgqnj83pzc7n10wgvs41q25paxshc3mg09wcm9ar5w10as0f")))

(define-public crate-zeros-2.1.0 (c (n "zeros") (v "2.1.0") (d (list (d (n "tiny-keccak") (r "^1") (d #t) (k 2)))) (h "1a8yhhhx4y3vsyn9j1ld21irqp63wfgswwzvdimkq03l38mjrn8b")))

(define-public crate-zeros-2.2.0 (c (n "zeros") (v "2.2.0") (d (list (d (n "kib") (r "^2") (d #t) (k 2)) (d (n "tiny-keccak") (r "^1") (d #t) (k 2)))) (h "1n8w9kyakkkinicqdsg88nap1jglrd1m63dj7fzs3jphqmjmw6zp")))

(define-public crate-zeros-2.3.0 (c (n "zeros") (v "2.3.0") (d (list (d (n "kib") (r "^2") (d #t) (k 2)) (d (n "tiny-keccak") (r "^1") (d #t) (k 2)))) (h "10kqwspb6bjssfwkcy2hv5bmqazjq7rbqvisj0nlm9jsq9vk7cjf")))

(define-public crate-zeros-2.4.0 (c (n "zeros") (v "2.4.0") (d (list (d (n "kib") (r "^2") (d #t) (k 2)) (d (n "tiny-keccak") (r "^1") (d #t) (k 2)))) (h "0hmx52qbjhk59zrj1j08xisvbxvb3dw23csxqcmi13v9lz4zjpng")))

(define-public crate-zeros-2.5.0 (c (n "zeros") (v "2.5.0") (d (list (d (n "kib") (r "^2") (d #t) (k 2)) (d (n "tiny-keccak") (r "^1") (d #t) (k 2)))) (h "1bpnrhs516kjck98pmgb5wlnar7zdjr92ydlikbhzhqcwg0xadnx")))

(define-public crate-zeros-2.6.0 (c (n "zeros") (v "2.6.0") (d (list (d (n "kib") (r "^2") (d #t) (k 2)) (d (n "tiny-keccak") (r "^1") (d #t) (k 2)))) (h "104mvj73cy7rzgi1710k8h8dv5y3744jv2kk7yxzjlp3db8wxa4j")))

(define-public crate-zeros-3.0.0 (c (n "zeros") (v "3.0.0") (d (list (d (n "kib") (r "^2") (d #t) (k 2)) (d (n "tiny-keccak") (r "^1") (d #t) (k 2)))) (h "0vqf2hy31pkzwjfi2fg0x4vslbv9c7z574gzh00akykaqmrqnpaj")))

(define-public crate-zeros-4.0.0 (c (n "zeros") (v "4.0.0") (d (list (d (n "kib") (r "^2") (d #t) (k 2)) (d (n "tiny-keccak") (r "^1") (d #t) (k 2)))) (h "0k80qvsdr9hxvg9jixd45afzmprjslraqph63xy03vbjsfszim8d")))

(define-public crate-zeros-5.0.0 (c (n "zeros") (v "5.0.0") (d (list (d (n "kib") (r "^2") (d #t) (k 2)) (d (n "tiny-keccak") (r "^1") (d #t) (k 2)))) (h "0kfwkcl1pg2136k09s808hr654pvpx9zwqcf3zsvwf20w7768h78")))

(define-public crate-zeros-5.1.0 (c (n "zeros") (v "5.1.0") (d (list (d (n "kib") (r "^2") (d #t) (k 2)) (d (n "tiny-keccak") (r "^1") (d #t) (k 2)))) (h "170m27ahnwpbi4lbdz08z5dncvrbaaj4v3bnpk47wb7knv7rhmgg")))

(define-public crate-zeros-5.2.0 (c (n "zeros") (v "5.2.0") (d (list (d (n "kib") (r "^2") (d #t) (k 2)) (d (n "tiny-keccak") (r "^1") (d #t) (k 2)))) (h "0g4lsvslvj3vca3w8ryhb09n9cz3fdgbfdk1zbyq7s0cryvgw1jr")))

(define-public crate-zeros-6.0.0 (c (n "zeros") (v "6.0.0") (d (list (d (n "kib") (r "^3") (d #t) (k 2)) (d (n "tiny-keccak") (r "^1") (d #t) (k 2)))) (h "1vvqya5rsmlmva9svh0n2091nkhvc06jbsbvfi4ac9jr85ki9dm6") (f (quote (("std"))))))

(define-public crate-zeros-6.1.0 (c (n "zeros") (v "6.1.0") (d (list (d (n "kib") (r "^3") (d #t) (k 2)) (d (n "tiny-keccak") (r "^1") (d #t) (k 2)))) (h "0ghkrb3b03mzllhdj292kp7zhxz8jk17qb1ivns36982ibrn7zm8") (f (quote (("std"))))))

(define-public crate-zeros-7.0.0 (c (n "zeros") (v "7.0.0") (d (list (d (n "kib") (r "^3") (d #t) (k 2)) (d (n "tiny-keccak") (r "^1") (d #t) (k 2)))) (h "0pmjmkkcssjn6lpbw1jlrzfrhlc35lxxwfyxpsz0djxpdqqd51gb") (f (quote (("std"))))))

(define-public crate-zeros-7.1.0 (c (n "zeros") (v "7.1.0") (d (list (d (n "kib") (r "^3") (d #t) (k 2)) (d (n "tiny-keccak") (r "^1") (d #t) (k 2)))) (h "0rivp96jljclxazcdpqny10am1agiwv32hcyc0frfpq8mxb58fql") (f (quote (("std"))))))

(define-public crate-zeros-7.1.1 (c (n "zeros") (v "7.1.1") (d (list (d (n "kib") (r "^3") (d #t) (k 2)) (d (n "tiny-keccak") (r "^1") (d #t) (k 2)))) (h "0bqwkjrfh2x7jyjza9811nyksc82dm0jmjc8iiwprzs2z0wl3q5a") (f (quote (("std"))))))

(define-public crate-zeros-7.2.0 (c (n "zeros") (v "7.2.0") (d (list (d (n "kib") (r "^3") (d #t) (k 2)) (d (n "tiny-keccak") (r "^1") (d #t) (k 2)))) (h "03ldnw7xwqn95xwq3f88l1g0zdhgp2wppksxk2af86y8hifl9466") (f (quote (("std"))))))

(define-public crate-zeros-8.0.0 (c (n "zeros") (v "8.0.0") (h "0khaavk9bsfbs9lcyxmk507x8l4l7qp052mb053ly8wldmxv63qy") (f (quote (("std"))))))

(define-public crate-zeros-9.0.0 (c (n "zeros") (v "9.0.0") (h "0d9x8pakr7gc1jd1kbmnjah92gv3ryd3xdlmkl9qvx88fb5c79ir") (f (quote (("std"))))))

(define-public crate-zeros-9.1.0 (c (n "zeros") (v "9.1.0") (h "1nkc5xax3xj22nb5j5v5hh8kkh6ni0ji068q0g06m24ns4611540") (f (quote (("std"))))))

(define-public crate-zeros-10.0.0 (c (n "zeros") (v "10.0.0") (h "1chf2ax009fnv9bxf4c12zn9nw2pq1mvcfwazakfzh2q1ng2yr8y") (f (quote (("std"))))))

(define-public crate-zeros-10.0.1 (c (n "zeros") (v "10.0.1") (h "0np1vl6x6hsi142a3f0l9ivhh49mq10lcg2725wz38qcczk10x09") (f (quote (("std"))))))

(define-public crate-zeros-10.0.2 (c (n "zeros") (v "10.0.2") (h "1nmqrprvg7chvwhj4lfcyps19cz5y0wlhfr6x2r3zhi506xd4k57") (f (quote (("std"))))))

(define-public crate-zeros-11.0.0 (c (n "zeros") (v "11.0.0") (h "0jgnhfchcfhd0145h0xcvpxzczpg7h7widv2jhm0ww2p49zk0gp2") (f (quote (("std"))))))

(define-public crate-zeros-11.1.0 (c (n "zeros") (v "11.1.0") (h "1r6awda446hqgxhqnw4q34nd0c5asvpyfdsxbsndri5hi0h76k0d") (f (quote (("std"))))))

(define-public crate-zeros-11.2.0 (c (n "zeros") (v "11.2.0") (h "0biicxbd1mwaap6wcgxqwfg1qrpzsfjrrspapswpcs75j01gqis2") (f (quote (("std"))))))

(define-public crate-zeros-11.2.1 (c (n "zeros") (v "11.2.1") (h "0kq2cw8j2ixl6jacjnyb7bg4zma714fw2ybnfdk6g9msxvchyk67") (f (quote (("std"))))))

(define-public crate-zeros-11.3.0 (c (n "zeros") (v "11.3.0") (h "130n68p5hsr843cnn82h9pxv9drawvdwjxxz0i4f03l8qlq0lxm2") (f (quote (("std"))))))

(define-public crate-zeros-12.0.0 (c (n "zeros") (v "12.0.0") (h "11gngj1j0yr6ynz1v17w1qxlvc217l9b15aig4fgk7w9ngh99nif") (f (quote (("std"))))))

(define-public crate-zeros-12.1.0 (c (n "zeros") (v "12.1.0") (h "1yikdjvl2rw6hlc6kk6fjhksk8zi385spwisjk64gzyk1r2xxjhq") (f (quote (("std"))))))

(define-public crate-zeros-12.2.0 (c (n "zeros") (v "12.2.0") (h "0kmaqh79cn07y0kh1az4kgr654v4hn1x6cswk8v0jkjn22v718b8") (f (quote (("std"))))))

(define-public crate-zeros-12.2.1 (c (n "zeros") (v "12.2.1") (h "0mj45lv2qh654j0lf61ks1z62rnzv99yxa9qg0qw4lg54izccali") (f (quote (("std"))))))

(define-public crate-zeros-12.2.2 (c (n "zeros") (v "12.2.2") (h "0rcnlnz14l4k79xcyw757vidj7pbdj3jvspfbfj05hsp0b53mr62") (f (quote (("std"))))))

(define-public crate-zeros-13.0.0 (c (n "zeros") (v "13.0.0") (h "0xrsa3040ni4mqbh8svgja7qg2ibxiinnh09svwcjvpkrcd4zagx") (f (quote (("std"))))))

(define-public crate-zeros-13.1.0 (c (n "zeros") (v "13.1.0") (h "12iqh3absbwd8p16x4dg048q59fgbdh6ib5f47f5kn3943cr3s6i") (f (quote (("std"))))))

(define-public crate-zeros-13.2.0 (c (n "zeros") (v "13.2.0") (d (list (d (n "openssl") (r ">=0.10, <0.11") (f (quote ("vendored"))) (d #t) (k 2)))) (h "0g1brrdpy264pg297v6d7z6wmz1ixa476n6ydswhxk8andznc1bc") (f (quote (("std") ("simd"))))))

(define-public crate-zeros-13.2.1 (c (n "zeros") (v "13.2.1") (d (list (d (n "openssl") (r ">=0.10, <0.11") (f (quote ("vendored"))) (d #t) (k 2)))) (h "1dzhk7lw87i7j25rsw4rck5m2g09gvqqwh2v4aphkqfm5zpan8rb") (f (quote (("std") ("simd"))))))

(define-public crate-zeros-13.2.2 (c (n "zeros") (v "13.2.2") (d (list (d (n "openssl") (r ">=0.10, <0.11") (f (quote ("vendored"))) (d #t) (k 2)))) (h "1mnx60zf24179d2xfkz1ahiizdwd20mwx6fk6wp4fr48drh9mqiq") (f (quote (("std") ("simd"))))))

(define-public crate-zeros-13.3.0 (c (n "zeros") (v "13.3.0") (d (list (d (n "kib") (r ">=7.0.1, <8") (d #t) (k 2)) (d (n "openssl") (r ">=0.10, <0.11") (f (quote ("vendored"))) (d #t) (k 2)))) (h "018aw15kskk2amf73cpbgncia4maij59m90v0ailf5n5cwm3n0wv") (f (quote (("std") ("simd"))))))

(define-public crate-zeros-13.3.1 (c (n "zeros") (v "13.3.1") (d (list (d (n "kib") (r ">=7.0.1, <8") (d #t) (k 2)) (d (n "openssl") (r ">=0.10, <0.11") (f (quote ("vendored"))) (d #t) (k 2)))) (h "11xb71y8pia5bk7k2vp50c2mhvliw07df4bxpxgsraniiplqjsfm") (f (quote (("std") ("simd"))))))

(define-public crate-zeros-14.0.0 (c (n "zeros") (v "14.0.0") (d (list (d (n "kib") (r ">=7.0.1, <8") (d #t) (k 2)) (d (n "openssl") (r ">=0.10, <0.11") (f (quote ("vendored"))) (d #t) (k 2)))) (h "0ifr9lig66cvc79jd4rnc7spb8r0835pr6p5037vnns0gcjdygch") (f (quote (("std") ("simd"))))))

(define-public crate-zeros-14.0.1 (c (n "zeros") (v "14.0.1") (d (list (d (n "kib") (r ">=7.0.1, <8") (d #t) (k 2)) (d (n "openssl") (r ">=0.10, <0.11") (f (quote ("vendored"))) (d #t) (k 2)))) (h "0q5y7l3scxzfdfc2zyn3p9rym28rwq2izhgpwf7va9hw35a162cf") (f (quote (("std") ("simd"))))))

(define-public crate-zeros-15.0.0 (c (n "zeros") (v "15.0.0") (d (list (d (n "kib") (r ">=7.0.1, <8") (d #t) (k 2)) (d (n "openssl") (r ">=0.10, <0.11") (f (quote ("vendored"))) (d #t) (k 2)))) (h "1bfj1alaa01yfznwaf7xqysscfgz8p25h10h1vj4am78zh4f2vdi") (f (quote (("std") ("simd"))))))

(define-public crate-zeros-15.0.1 (c (n "zeros") (v "15.0.1") (d (list (d (n "kib") (r ">=7.0.1, <8") (d #t) (k 2)) (d (n "openssl") (r ">=0.10, <0.11") (f (quote ("vendored"))) (d #t) (k 2)))) (h "1pp9pgyp6168ih5qv43hkcjy52sd0cbvssm01hvwn6smzp85jd1s") (f (quote (("std") ("simd"))))))

(define-public crate-zeros-15.0.2 (c (n "zeros") (v "15.0.2") (d (list (d (n "kib") (r ">=7.0.1, <8") (d #t) (k 2)) (d (n "openssl") (r ">=0.10, <0.11") (f (quote ("vendored"))) (d #t) (k 2)))) (h "0gi2l9rfciab926ri8xln7hgg4n1pxchw5c3kndsy28zfdqprxwi") (f (quote (("std") ("simd"))))))

(define-public crate-zeros-15.0.3 (c (n "zeros") (v "15.0.3") (d (list (d (n "kib") (r ">=7.0.1, <8") (d #t) (k 2)) (d (n "openssl") (r ">=0.10, <0.11") (f (quote ("vendored"))) (d #t) (k 2)))) (h "1xmb7xx9bj0hs2pwri3nvcz1h7qy12jgv3znkywsgkka71v2xsqv") (f (quote (("std") ("simd"))))))

(define-public crate-zeros-15.0.4 (c (n "zeros") (v "15.0.4") (d (list (d (n "kib") (r ">=7.0.1, <8") (d #t) (k 2)) (d (n "openssl") (r ">=0.10, <0.11") (f (quote ("vendored"))) (d #t) (k 2)))) (h "0iwyksy5i3rfysmvsn8cv5yk4gdv4vqm23bbsw0qj088nc1c0hg2") (f (quote (("std") ("simd"))))))

(define-public crate-zeros-16.0.0 (c (n "zeros") (v "16.0.0") (d (list (d (n "kib") (r ">=7.0.1, <8") (d #t) (k 2)) (d (n "openssl") (r ">=0.10, <0.11") (f (quote ("vendored"))) (d #t) (k 2)))) (h "1ss43ivlhshhcbzb3p9ppl0py6h5bfl6rqg76qdm39d3drzccjdq") (f (quote (("std") ("simd"))))))

(define-public crate-zeros-16.0.1 (c (n "zeros") (v "16.0.1") (d (list (d (n "kib") (r ">=7.0.1, <8") (d #t) (k 2)) (d (n "openssl") (r ">=0.10, <0.11") (f (quote ("vendored"))) (d #t) (k 2)))) (h "1x4m6mxxh9i2jbf9njakkbdf76qn3v24sf96iximqr7zbbpqvi6y") (f (quote (("std") ("simd"))))))

(define-public crate-zeros-16.0.2 (c (n "zeros") (v "16.0.2") (d (list (d (n "kib") (r ">=7.0.1, <8") (d #t) (k 2)) (d (n "openssl") (r ">=0.10, <0.11") (f (quote ("vendored"))) (d #t) (k 2)))) (h "13g99q9mvh7xvf8hkg4lzp7iqhjd2vvzj08kyhp6j1kc171l6syw") (f (quote (("std") ("simd"))))))

(define-public crate-zeros-16.0.3 (c (n "zeros") (v "16.0.3") (d (list (d (n "kib") (r ">=7.0.1, <8") (d #t) (k 2)) (d (n "openssl") (r ">=0.10, <0.11") (f (quote ("vendored"))) (d #t) (k 2)))) (h "18m2swbfly41i3iq3ahs7yfc66p5fk6vcb70aycbfmz72rcdpi25") (f (quote (("std") ("simd"))))))

(define-public crate-zeros-16.0.4 (c (n "zeros") (v "16.0.4") (d (list (d (n "kib") (r ">=7.0.1, <8") (d #t) (k 2)) (d (n "openssl") (r ">=0.10, <0.11") (f (quote ("vendored"))) (d #t) (k 2)))) (h "12qnik9wyxcg3j6rdfhkajw9jny3pppm5dnyxy1qf6rwwa89vqws") (f (quote (("std") ("simd"))))))

