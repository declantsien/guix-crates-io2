(define-module (crates-io ze ro zerostash-fuse) #:use-module (crates-io))

(define-public crate-zerostash-fuse-0.7.0 (c (n "zerostash-fuse") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "fuse_mt") (r "^0.6.0") (d #t) (k 0)) (d (n "infinitree") (r "^0.10.4") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("user"))) (k 0)) (d (n "scc") (r "^1.8.3") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt" "time" "signal" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "zerostash-files") (r "^0.7.0") (d #t) (k 0)))) (h "0yb90mzkhl2y49il2jbj2az286gvf52fqyxky72cfbmk3y9sbdzr")))

