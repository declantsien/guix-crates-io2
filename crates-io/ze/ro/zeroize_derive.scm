(define-module (crates-io ze ro zeroize_derive) #:use-module (crates-io))

(define-public crate-zeroize_derive-0.1.0 (c (n "zeroize_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "10fjdinf6ql7g400q8p9bgbmkmpxbyb7n0nh76c98682ha879w5k")))

(define-public crate-zeroize_derive-0.7.0 (c (n "zeroize_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "1jr2jfmgknxdq5z47z8z9gdfdrpl4imlbankncavdds07ca6yccx")))

(define-public crate-zeroize_derive-0.8.0 (c (n "zeroize_derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "0cb4m6p7a29y19pawajzvlhqpamc3j4pm1717yfgz6wn1mk4pb4x") (y #t)))

(define-public crate-zeroize_derive-0.9.0 (c (n "zeroize_derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "1g4dswi7sdwlxj92pj4r28xysfkdpplscsyj0rkbk8xw9fg4dldg") (y #t)))

(define-public crate-zeroize_derive-0.9.3 (c (n "zeroize_derive") (v "0.9.3") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "0gk4j5snqaizk5z7slhmzb105vvq3zgsrc4b52an1wri1syic1h8") (y #t)))

(define-public crate-zeroize_derive-0.10.0 (c (n "zeroize_derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1bnwh691i31ap3fwfmf9gbzvj1mvcv4b0l22dy60fhzwd6ffb77w") (y #t)))

(define-public crate-zeroize_derive-1.0.0-pre (c (n "zeroize_derive") (v "1.0.0-pre") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1k97gmczw11xa18llayhrvy1b7vxhbsmjflhkpmaqrn18d5nrwxi") (y #t)))

(define-public crate-zeroize_derive-1.0.0 (c (n "zeroize_derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "18lc9xq9dwvmv81y3bqnw20974nbrs7d20rljb1inz7wd7n1w9fy") (y #t)))

(define-public crate-zeroize_derive-1.0.1 (c (n "zeroize_derive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "05jx6j2c078f41ik0xgc5lb0z7yjfhgg76x43akanql8n7fnkwy3") (y #t)))

(define-public crate-zeroize_derive-1.1.0 (c (n "zeroize_derive") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1cfsssf3jrkzhly58ln410j0jjx1mk5rmgw673rb5smspqqf3hd2") (y #t)))

(define-public crate-zeroize_derive-1.2.0 (c (n "zeroize_derive") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1iz09bgfl8383468z0qyxxd488565fp7748piyq258sim0j21zxx") (y #t)))

(define-public crate-zeroize_derive-1.1.1 (c (n "zeroize_derive") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0mvizabr5v4gjqhr39gj43cnhhmlx0mgb4gahhbl9xgqq5hnbdaf") (y #t)))

(define-public crate-zeroize_derive-1.2.1 (c (n "zeroize_derive") (v "1.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "05w56qbd4smiph02g82gd8bi0pmv2rb56aqalanvrsw3z060z3hm") (y #t)))

(define-public crate-zeroize_derive-1.2.2 (c (n "zeroize_derive") (v "1.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0wqznp1wdgp9dgb026b9ss3kbxhp3y20rs0zbmnwd27c4cbsbwb5") (y #t)))

(define-public crate-zeroize_derive-1.3.0-pre (c (n "zeroize_derive") (v "1.3.0-pre") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "0iblk91b7q0fxkwn9ydghf5f95cqdd4aqfha1skygs7b230548a2") (y #t)))

(define-public crate-zeroize_derive-1.3.0 (c (n "zeroize_derive") (v "1.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1k649zdsq1fz626myxgxcmn7kp9ql78p94hilmxbqm5qkab7c1m9") (y #t)))

(define-public crate-zeroize_derive-1.3.1 (c (n "zeroize_derive") (v "1.3.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12") (d #t) (k 0)))) (h "1nzdqyryjnqcrqz0vhddpkd8sybhn0bd8rbd6l33rdhhxwzz3s41") (y #t)))

(define-public crate-zeroize_derive-1.3.2 (c (n "zeroize_derive") (v "1.3.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.2") (d #t) (k 0)))) (h "05rs3m2bckzcfv52d7qphicic65l4m1gqjswsa0gdm6s85v1i3rz")))

(define-public crate-zeroize_derive-1.3.3 (c (n "zeroize_derive") (v "1.3.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.2") (d #t) (k 0)))) (h "0v6kkgkw8sxddx2608in3xx8ijbbyj5xb5b6741j1sjh7v5hggs4")))

(define-public crate-zeroize_derive-1.4.0 (c (n "zeroize_derive") (v "1.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hbly6rdvmbjysy144g9v1n49imknf1cjjlc5399yvdjp200a2ap") (r "1.56")))

(define-public crate-zeroize_derive-1.4.1 (c (n "zeroize_derive") (v "1.4.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0d69c8lfy3bkk70mhah3hxg79ffdjn2wnq8xlyy50sr1wmrq0n15") (r "1.56")))

(define-public crate-zeroize_derive-1.4.2 (c (n "zeroize_derive") (v "1.4.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "0sczjlqjdmrp3wn62g7mw6p438c9j4jgp2f9zamd56991mdycdnf") (r "1.56")))

