(define-module (crates-io ze ro zerodrop) #:use-module (crates-io))

(define-public crate-zerodrop-0.1.0 (c (n "zerodrop") (v "0.1.0") (d (list (d (n "consistenttime") (r "^0.2") (d #t) (k 0)))) (h "0b6zwfg9mjl8lqj17d721pgqgkrp313msgqll86g74x2hqbck8mg") (y #t)))

(define-public crate-zerodrop-0.1.1 (c (n "zerodrop") (v "0.1.1") (d (list (d (n "consistenttime") (r "^0.2") (d #t) (k 0)))) (h "04zl0h53mbi0lg7v353j042wbd4rzkrd5xam3irp35dfqr163z4k") (y #t)))

(define-public crate-zerodrop-0.1.2 (c (n "zerodrop") (v "0.1.2") (d (list (d (n "consistenttime") (r "^0.2") (d #t) (k 0)))) (h "15x9k1p2kn8fx6pwssi8ck9cfmykxhlkmrm3by7ff5z4789r19pm") (y #t)))

(define-public crate-zerodrop-0.1.3 (c (n "zerodrop") (v "0.1.3") (d (list (d (n "consistenttime") (r "^0.2") (d #t) (k 0)))) (h "1j0dab9rzcmfhnmgxb5z024hh7al2hllxkzrad78lnr98biimd27") (y #t)))

(define-public crate-zerodrop-0.1.4 (c (n "zerodrop") (v "0.1.4") (d (list (d (n "consistenttime") (r "^0.2") (d #t) (k 0)))) (h "0qrnc4ffzqazwhdllkb7qdi2gikdi8y8i2bq5x3ypnl39g74vx36")))

