(define-module (crates-io ze ro zero_sum) #:use-module (crates-io))

(define-public crate-zero_sum-0.1.0 (c (n "zero_sum") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1narplqwa39pkp8k3b4rs4rkmwj13fb48gdpl558pmd3gqp68fgv")))

(define-public crate-zero_sum-0.2.0 (c (n "zero_sum") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "16rki6a5i08b6an7pqdpg5frbf2cd7asrhihkz9yd75rlcgrzvnm")))

(define-public crate-zero_sum-1.0.0 (c (n "zero_sum") (v "1.0.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1b38rkx99bj7hyrfwgif8rpy1pj9za83n8j69fqrm7hb5j1fi2xf") (f (quote (("with_tic_tac_toe") ("with_tak" "lazy_static" "rand") ("with_all" "with_tak" "with_tic_tac_toe"))))))

(define-public crate-zero_sum-1.1.0 (c (n "zero_sum") (v "1.1.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)))) (h "19q5qi8xsibgs94yfn2252svc2aby4qlwd6ffcnw6y289n9kiync") (f (quote (("with_tic_tac_toe") ("with_tak" "lazy_static" "rand") ("with_all" "with_tak" "with_tic_tac_toe"))))))

(define-public crate-zero_sum-1.1.1 (c (n "zero_sum") (v "1.1.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)))) (h "18gii20ihzsqr1y0xwm3ryqrgd8ckkc052jmp88gryl3dslz0fk9") (f (quote (("with_tic_tac_toe") ("with_tak" "lazy_static" "rand") ("with_all" "with_tak" "with_tic_tac_toe"))))))

(define-public crate-zero_sum-1.1.2 (c (n "zero_sum") (v "1.1.2") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1cpbpyqkjxqlkw2dn4nlzl4c9d07cp5g50fp5rlphxdjaaawkfld") (f (quote (("with_tic_tac_toe") ("with_tak" "lazy_static" "rand") ("with_all" "with_tak" "with_tic_tac_toe"))))))

(define-public crate-zero_sum-1.2.0 (c (n "zero_sum") (v "1.2.0") (d (list (d (n "blas") (r "^0.15.3") (o #t) (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rusqlite") (r "^0.10") (o #t) (d #t) (k 0)))) (h "07v345jb1c0qb9pcj47ja9kfih6cdyg41cppay9aghqac2vbvqqd") (f (quote (("with_tic_tac_toe") ("with_tak_ann" "with_tak" "blas" "rusqlite") ("with_tak" "lazy_static" "rand") ("with_all" "with_tak" "with_tak_ann" "with_tic_tac_toe"))))))

