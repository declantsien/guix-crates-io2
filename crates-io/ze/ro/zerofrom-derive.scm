(define-module (crates-io ze ro zerofrom-derive) #:use-module (crates-io))

(define-public crate-zerofrom-derive-0.1.0 (c (n "zerofrom-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "zerovec") (r "^0.6") (f (quote ("yoke"))) (d #t) (k 2)))) (h "06iwc9yy6y301fgrs3h7nd8g7ac6f8lr2zqlcql97hb2c1yz91c7")))

(define-public crate-zerofrom-derive-0.1.1 (c (n "zerofrom-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "zerofrom") (r "^0.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "zerovec") (r "^0.9") (f (quote ("yoke"))) (d #t) (k 2)))) (h "0ag8jp2ral62494vca8j3fdxfmyl7h1ixl0yki0d5nwxvmmai2if")))

(define-public crate-zerofrom-derive-0.1.2 (c (n "zerofrom-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("derive" "fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)))) (h "16f4l6sk5h121p2k3bl110jjzp4x8iqhgg16wpyfmf6lyz0ygsml")))

(define-public crate-zerofrom-derive-0.1.3 (c (n "zerofrom-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("derive" "fold" "visit"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13") (d #t) (k 0)) (d (n "zerofrom") (r "^0.1.1") (f (quote ("derive"))) (k 2)) (d (n "zerovec") (r "^0.9.4") (f (quote ("yoke"))) (k 2)))) (h "1hqq5xw5a55623313p2gs9scbn24kqhvgrn2wvr75lvi0i8lg9p6")))

(define-public crate-zerofrom-derive-0.1.4 (c (n "zerofrom-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.61") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.21") (f (quote ("fold"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "19b31rrs2ry1lrq5mpdqjzgg65va51fgvwghxnf6da3ycfiv99qf")))

