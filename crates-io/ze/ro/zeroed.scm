(define-module (crates-io ze ro zeroed) #:use-module (crates-io))

(define-public crate-zeroed-1.0.0 (c (n "zeroed") (v "1.0.0") (h "1k1lz1adz1kkv4vkxwcy1ndj295n58py80jwgspnhlrvm9ky3rkz") (y #t)))

(define-public crate-zeroed-2.0.0 (c (n "zeroed") (v "2.0.0") (h "0bn89dwqh30w2bnn8h8jbkq20lkw4yq3jvkl69krpx4177n3vp4z") (y #t)))

(define-public crate-zeroed-3.0.0 (c (n "zeroed") (v "3.0.0") (h "1dklmb3wp22gd4hl6xph4mjrrc64ign2642fsm6hwcvazxgjb0hs") (y #t)))

(define-public crate-zeroed-4.0.0 (c (n "zeroed") (v "4.0.0") (h "04qabcahlrnd13a5v0wf6a7djvylgzr4dj5w0ivbxp7ycxqf9fnk") (y #t)))

(define-public crate-zeroed-5.0.0 (c (n "zeroed") (v "5.0.0") (h "1h459my6s3i1hxd7f3mb1j69gp0vwcg9llsv3vr9zsr3pc3h385n") (y #t)))

