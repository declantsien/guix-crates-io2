(define-module (crates-io ze ro zero_pass_backend_derive) #:use-module (crates-io))

(define-public crate-zero_pass_backend_derive-0.1.0 (c (n "zero_pass_backend_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1khbay9bxz46fkvsnmyqmwr5q0xdmycxcfkngml8q8n1pmkk97n9")))

(define-public crate-zero_pass_backend_derive-0.1.1 (c (n "zero_pass_backend_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1bcr1fq52m69qq924c3m1j8p1dvmp0nrjggcq8ks1qjgjw39id55")))

(define-public crate-zero_pass_backend_derive-0.1.2 (c (n "zero_pass_backend_derive") (v "0.1.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10hx9kh8sx8f8gjxhdj1f5cmj9g2chmvw830pl6r2dnmwjprhfwf")))

(define-public crate-zero_pass_backend_derive-0.1.3 (c (n "zero_pass_backend_derive") (v "0.1.3") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0y22yym4slkf1jqfsgx6kvv8sakqnh2hdr1pkqwykm98pk86r95p")))

