(define-module (crates-io ze ro zerogc-derive) #:use-module (crates-io))

(define-public crate-zerogc-derive-0.1.0 (c (n "zerogc-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0dbpz5zpa2jpaqyqfgic3hrrmglwm7fh095d5zw06gyrgrczy1wd")))

(define-public crate-zerogc-derive-0.1.1 (c (n "zerogc-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1jip8sf62zl7cbya7rmmkbid9wnz42d6w4aayd12lhc4yazgh266")))

(define-public crate-zerogc-derive-0.1.3 (c (n "zerogc-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.55") (d #t) (k 0)) (d (n "zerogc") (r "^0.1.3") (d #t) (k 2)))) (h "1qxa5by08xqd2jiz7dmx3086qmwpdz1njsbqqcislcggj3rg4hc9")))

(define-public crate-zerogc-derive-0.1.4 (c (n "zerogc-derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.55") (d #t) (k 0)) (d (n "zerogc") (r "^0.1.3") (d #t) (k 2)))) (h "1gmbmljyaxm2mj5slny5i7yxjlrzbwd7mby1fgq1lh5dvxyzbav4")))

(define-public crate-zerogc-derive-0.2.0-alpha.1 (c (n "zerogc-derive") (v "0.2.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.55") (d #t) (k 0)) (d (n "zerogc") (r "^0.2.0-alpha.1") (d #t) (k 2)))) (h "011b0rjab4l8rix025zfqgkkzvmpyry4xpj1lldm4czvj5gb3218")))

(define-public crate-zerogc-derive-0.2.0-alpha.2 (c (n "zerogc-derive") (v "0.2.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "zerogc") (r "^0.2.0-alpha.2") (d #t) (k 2)))) (h "1iy2i9al0806aigbavvvvxrfcpwcck5w7cgzzxq18jf5z7pcs3fk")))

(define-public crate-zerogc-derive-0.2.0-alpha.3 (c (n "zerogc-derive") (v "0.2.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "zerogc") (r "^0.2.0-alpha.3") (d #t) (k 2)))) (h "193zbhg9v3nmck5zgwpig5xhxnbvb91i6fr8x0gdybjjmx5gcy1p")))

(define-public crate-zerogc-derive-0.2.0-alpha.4 (c (n "zerogc-derive") (v "0.2.0-alpha.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "zerogc") (r "^0.2.0-alpha.4") (d #t) (k 2)))) (h "0wqx4r9cpidjn2nil23mv7p8z0d39yszxx53035cl0vqwppzrjxl")))

(define-public crate-zerogc-derive-0.2.0-alpha.5 (c (n "zerogc-derive") (v "0.2.0-alpha.5") (d (list (d (n "proc-macro-kwargs") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.55") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "zerogc") (r "^0.2.0-alpha.5") (d #t) (k 2)))) (h "0g2hxy97lc0jdw1kq3cm2m55scvg5rvlf3ynvq85rxyqa5f005l1")))

(define-public crate-zerogc-derive-0.2.0-alpha.6 (c (n "zerogc-derive") (v "0.2.0-alpha.6") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "proc-macro-kwargs") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0.55") (f (quote ("full" "extra-traits" "visit" "fold"))) (d #t) (k 0)) (d (n "zerogc") (r "^0.2.0-alpha.6") (f (quote ("serde1"))) (d #t) (k 2)))) (h "0wnvjbs4ph9xlnfmqw9r1ipkfd1i5azaiyhfkqyq779b96hfhxdh") (f (quote (("__serde-internal"))))))

