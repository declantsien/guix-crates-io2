(define-module (crates-io ze ro zerodmg-emulator) #:use-module (crates-io))

(define-public crate-zerodmg-emulator-0.0.1 (c (n "zerodmg-emulator") (v "0.0.1") (h "0gs0k4fiqbj12i5c433101sndd8dd2np2rhxxgkizjzk9br21dhr") (y #t)))

(define-public crate-zerodmg-emulator-0.1.0 (c (n "zerodmg-emulator") (v "0.1.0") (d (list (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "zerodmg-utils") (r "^0.1.0") (d #t) (k 0)))) (h "14v74yjnh159if2bvny6kip07zgwhldx2aqwrlbx6ls7gmr1yggz") (y #t)))

(define-public crate-zerodmg-emulator-0.1.3 (c (n "zerodmg-emulator") (v "0.1.3") (d (list (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "zerodmg-utils") (r "^0.1.3") (d #t) (k 0)))) (h "1ibxfskwcn1n9fiaq5vx0l3rxmdc7jk4zjzam5bj97jygblbyp1g") (y #t)))

(define-public crate-zerodmg-emulator-0.1.4 (c (n "zerodmg-emulator") (v "0.1.4") (d (list (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "zerodmg-utils") (r "^0.1.4") (d #t) (k 0)))) (h "007kmmzh9w9rjr5czvly0yga7jakmgiz5y40i57a35h4gzwqybp1") (y #t)))

(define-public crate-zerodmg-emulator-0.1.5 (c (n "zerodmg-emulator") (v "0.1.5") (d (list (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "zerodmg-utils") (r "^0.1.5") (d #t) (k 0)))) (h "1i3sd7yll10nssn1hf2f1hkm3ca53wkfsdng8wfv76wdb02z7ji4") (y #t)))

(define-public crate-zerodmg-emulator-0.1.6 (c (n "zerodmg-emulator") (v "0.1.6") (d (list (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "zerodmg-utils") (r "^0.1.6") (d #t) (k 0)))) (h "1vgsk0v0xws0mf0cwabdds5nvvpcajz8mf9m0gi11h4xifpqz5b2") (y #t)))

(define-public crate-zerodmg-emulator-0.1.8 (c (n "zerodmg-emulator") (v "0.1.8") (d (list (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "zerodmg-utils") (r "^0.1.8") (d #t) (k 0)))) (h "0l0mix4ps9dnhls7dzg8s5c4rwnyaw2sdrpa38v7nddxrxk70wqf") (y #t)))

(define-public crate-zerodmg-emulator-0.1.9 (c (n "zerodmg-emulator") (v "0.1.9") (d (list (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "zerodmg-codes") (r "^0.1.9") (d #t) (k 0)) (d (n "zerodmg-utils") (r "^0.1.9") (d #t) (k 0)))) (h "0cfj85mm6hj5h7zlzk8bnsybb9vgzwj1n66k69wi9k44gmjxx6zw") (y #t)))

(define-public crate-zerodmg-emulator-0.1.10-zerodmg-emulator (c (n "zerodmg-emulator") (v "0.1.10-zerodmg-emulator") (h "1bx5rdh8hi40p141v8vbf7hgmgavwy66i4fanxbi1wz49mrmh8pw") (y #t)))

(define-public crate-zerodmg-emulator-0.0.0-- (c (n "zerodmg-emulator") (v "0.0.0--") (h "0v77xn8h6zb3hzmx5hk36cxb7l0sk6rk7j8xm5492rv7z0r4cwm9") (y #t)))

