(define-module (crates-io ze ro zeroize_alloc) #:use-module (crates-io))

(define-public crate-zeroize_alloc-0.1.0 (c (n "zeroize_alloc") (v "0.1.0") (h "0zrq2rfin1k9sg3v2rwzsnf3vkl94fsy8lgq09kdjpq5qqylm2q2") (f (quote (("leaky"))))))

(define-public crate-zeroize_alloc-0.1.1 (c (n "zeroize_alloc") (v "0.1.1") (h "18xl3ms2f9937ds5iqcly7x4xqqd2g5bw3pjqmmgqpjxdi21mddn") (f (quote (("leaky"))))))

(define-public crate-zeroize_alloc-0.1.2 (c (n "zeroize_alloc") (v "0.1.2") (h "1ifg555gajkd81smr7hdcwx4gjqxw95cq6gp8c8dssl3cd3xrqzb") (f (quote (("leaky"))))))

