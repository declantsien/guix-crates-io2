(define-module (crates-io ze ro zero) #:use-module (crates-io))

(define-public crate-zero-0.1.0 (c (n "zero") (v "0.1.0") (h "10l7zp4f7dz2mlqhp6dc5vfc4348383y2jidggr7zlwgqj2abmmp")))

(define-public crate-zero-0.1.1 (c (n "zero") (v "0.1.1") (h "14zfcjl6605w6rqycxg4h05fq4wgs225ia4fkfc3w0j5l3n5lp04")))

(define-public crate-zero-0.1.2 (c (n "zero") (v "0.1.2") (h "1ic2vv2xs5m29hpk7ny9rs6zp31d012p0n4p4ab88n00nakch6sz")))

(define-public crate-zero-0.1.3 (c (n "zero") (v "0.1.3") (h "113pa9jj40x6bvxsw582ca9np7d53qkb2b6cavfyczya6k61pqig")))

