(define-module (crates-io ze ro zeronet_address) #:use-module (crates-io))

(define-public crate-zeronet_address-0.1.0 (c (n "zeronet_address") (v "0.1.0") (d (list (d (n "serde") (r "~1.0.133") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.74") (d #t) (k 0)) (d (n "sha2") (r "~0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "~1.0.30") (d #t) (k 0)))) (h "05qr6hg9s4mlkp5pgi3hj6sjv4z8qsff8317a2fg4b288wj6szy5")))

(define-public crate-zeronet_address-0.1.1 (c (n "zeronet_address") (v "0.1.1") (d (list (d (n "serde") (r "~1.0.133") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.74") (d #t) (k 0)) (d (n "sha2") (r "~0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "~1.0.30") (d #t) (k 0)))) (h "0acgbh1z7sy5aznwg5hlkigx2mra45lhryxjfzgrlhvi7saixd63")))

