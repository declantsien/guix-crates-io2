(define-module (crates-io ze ro zero85) #:use-module (crates-io))

(define-public crate-zero85-0.1.5 (c (n "zero85") (v "0.1.5") (d (list (d (n "quickcheck") (r "^0.2.24") (d #t) (k 2)))) (h "0bf3yksa7z6b9b1bxmjh3akrk57z0fcsm2frgzjzms9pcdxwhr0d")))

(define-public crate-zero85-0.2.0 (c (n "zero85") (v "0.2.0") (d (list (d (n "quickcheck") (r "0.2.*") (d #t) (k 2)))) (h "0dsy1k4kkllphl7lscnx9wzg5vi3d9dj6ib75x2qrn80n86a31v6")))

