(define-module (crates-io ze ro zero-pairing) #:use-module (crates-io))

(define-public crate-zero-pairing-0.2.0 (c (n "zero-pairing") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (k 2)) (d (n "rand_xorshift") (r "^0.3") (k 2)) (d (n "zero-bls12-381") (r "^0.2.0") (k 0)) (d (n "zero-crypto") (r "^0.2.1") (k 0)))) (h "0gi8vnq73w82m8sdvwkh4chbzllqkmgnr8h5di2l17121pspx09v") (y #t)))

(define-public crate-zero-pairing-0.2.2 (c (n "zero-pairing") (v "0.2.2") (d (list (d (n "rand") (r "^0.8") (k 2)) (d (n "rand_xorshift") (r "^0.3") (k 2)) (d (n "zero-bls12-381") (r "^0.2.2") (k 0)) (d (n "zero-crypto") (r "^0.2.2") (k 0)))) (h "0nlxhl3m3w4x55k71zks94yv0ikwrnmhngnd2rp1f9dvnd79hfz3") (y #t)))

(define-public crate-zero-pairing-0.2.3 (c (n "zero-pairing") (v "0.2.3") (d (list (d (n "rand") (r "^0.8") (k 2)) (d (n "rand_xorshift") (r "^0.3") (k 2)) (d (n "zero-bls12-381") (r "^0.2.2") (k 0)) (d (n "zero-crypto") (r "^0.2.2") (k 0)))) (h "0x3gzl39l43nxr5dr07zac4kmas1b7va2wlh6xwha01jch22lba6") (y #t)))

(define-public crate-zero-pairing-0.2.5 (c (n "zero-pairing") (v "0.2.5") (d (list (d (n "rand") (r "^0.8") (k 2)) (d (n "rand_xorshift") (r "^0.2.0") (k 2) (p "fullcodec_rand_xorshift")) (d (n "zero-bls12-381") (r "^0.2.5") (k 0)) (d (n "zero-crypto") (r "^0.2.5") (k 0)))) (h "09rbfrrjjf4vixmc82iyrxqlxkkxygz5h92jmnszcxn2zm5zrsl2") (y #t)))

(define-public crate-zero-pairing-0.1.10 (c (n "zero-pairing") (v "0.1.10") (d (list (d (n "rand") (r "^0.8") (k 2)) (d (n "rand_xorshift") (r "^0.3") (k 2)) (d (n "zero-bls12-381") (r "^0.1.10") (k 0)) (d (n "zero-crypto") (r "^0.1.10") (k 0)))) (h "1lvlybxkw938cqnhfs6f3a8g78qbaw6j7rjnzhfnzb3x74gzfp77")))

(define-public crate-zero-pairing-0.1.11 (c (n "zero-pairing") (v "0.1.11") (d (list (d (n "parity-scale-codec") (r "^2.1.0") (f (quote ("derive"))) (k 0)) (d (n "rand_core") (r "^0.6.4") (f (quote ("getrandom"))) (d #t) (k 2)) (d (n "zero-bls12-381") (r "^0.1.11") (k 0)) (d (n "zero-crypto") (r "^0.1.11") (k 0)) (d (n "zero-jubjub") (r "^0.1.11") (k 0)))) (h "026p2j7jmlmg7bpy2i5xi66h9p5i0vjmdz6327qayxr061wd8bxw")))

