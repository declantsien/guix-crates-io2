(define-module (crates-io ze ro zero_pass_backend) #:use-module (crates-io))

(define-public crate-zero_pass_backend-0.0.1 (c (n "zero_pass_backend") (v "0.0.1") (h "1dl052qsgy5frq08iiqqiddxknxfjpzy25b6hg91b5vly8wa799y")))

(define-public crate-zero_pass_backend-0.0.2 (c (n "zero_pass_backend") (v "0.0.2") (h "091hn6cda8r4ycj0b66kvxchgd13945mqk6b6cqad47y9m3xwdqz")))

(define-public crate-zero_pass_backend-0.0.3 (c (n "zero_pass_backend") (v "0.0.3") (h "1jf86gavwlksg8qw0kv16bniaabfdjgd2vbgn5zghra58lhglfbn")))

(define-public crate-zero_pass_backend-0.0.4 (c (n "zero_pass_backend") (v "0.0.4") (h "09dblgs1qh8al2ka2ghcj3xnnmpfrf91i3gkihc6c706icvf7dki")))

(define-public crate-zero_pass_backend-0.0.5 (c (n "zero_pass_backend") (v "0.0.5") (h "063p2zjvp32pr6f582b95i9ijdggqygys5sv53xni5n5mhp3qpvq")))

(define-public crate-zero_pass_backend-0.0.6 (c (n "zero_pass_backend") (v "0.0.6") (h "1y86ik43300h9kjfk97bwxjnxik2nz0dhs8q0ygdlmsbb241ksay")))

(define-public crate-zero_pass_backend-0.0.7 (c (n "zero_pass_backend") (v "0.0.7") (h "19ldi7fw65jgxy3ip8bsx8ypnqd6x7bgdz1pcbkllw985l463y1b")))

(define-public crate-zero_pass_backend-0.1.0 (c (n "zero_pass_backend") (v "0.1.0") (h "0lpv8yisc7sssaivvwk7m6qvj2k9vjhcdn79zmf5y6pik2gjpdj6")))

(define-public crate-zero_pass_backend-0.1.1 (c (n "zero_pass_backend") (v "0.1.1") (h "1qzjb7i6bc9660483iqlsxpmjz1b34a3mszlw9kv637kfc1h58mv")))

(define-public crate-zero_pass_backend-0.2.0 (c (n "zero_pass_backend") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0km4l8gb2491fxjlkapbz6an1jp608a9xkmnv0w7j4xhdnfwkszp")))

(define-public crate-zero_pass_backend-0.2.1 (c (n "zero_pass_backend") (v "0.2.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0z66m1r576mj3l9pihqhqkc3h6kqbpfqz1iah0qimaqlwqp654f0")))

(define-public crate-zero_pass_backend-0.2.2 (c (n "zero_pass_backend") (v "0.2.2") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "02a6hdpmda2ggz1vxx802dhz69xfkv4z1alppyclrflkiymppjsh")))

(define-public crate-zero_pass_backend-0.2.3 (c (n "zero_pass_backend") (v "0.2.3") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "18nbzva69gabrbvwj5divnay69g3drsn0wqijcpgsn72dgsziy6s")))

(define-public crate-zero_pass_backend-0.2.4 (c (n "zero_pass_backend") (v "0.2.4") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0lj5dshszysnf54ygij4xy2m1jymd4y4n0wg9lkpb7b36y1amdc4") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-zero_pass_backend-0.2.6 (c (n "zero_pass_backend") (v "0.2.6") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "zero_pass_backend_derive") (r "^0.1.1") (d #t) (k 0)))) (h "0332rva99mw6n7aa5jflwnsx5pkxz65m96vyjia3gzdc0935g5ik") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-zero_pass_backend-0.3.0 (c (n "zero_pass_backend") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.79") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "zero_pass_backend_derive") (r "^0.1.1") (d #t) (k 0)))) (h "1d0ykvq58dyb3m71jkx868g94sdpv3l9szzbfgpqyfgw7ykmz231") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-zero_pass_backend-0.3.1 (c (n "zero_pass_backend") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1.79") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "zero_pass_backend_derive") (r "^0.1.2") (d #t) (k 0)))) (h "03k2cfgi4hb2gnzywpxb7sqs658hzgi47d4mm418ql44y679qlpj") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-zero_pass_backend-0.3.2 (c (n "zero_pass_backend") (v "0.3.2") (d (list (d (n "async-trait") (r "^0.1.79") (d #t) (k 0)) (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "zero_pass_backend_derive") (r "^0.1.2") (d #t) (k 0)))) (h "0rvj4y020sviiaj6l4laynpxmwjfkb7xcx0fr8iahshwr8cspvvp") (s 2) (e (quote (("serde" "dep:serde"))))))

