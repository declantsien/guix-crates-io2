(define-module (crates-io ze ro zeroconf-macros) #:use-module (crates-io))

(define-public crate-zeroconf-macros-0.1.0 (c (n "zeroconf-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)))) (h "0gfj4i28jldfv1vradpwzcn3qwsqqcfar8wb56p2pmiizhvga3gv")))

(define-public crate-zeroconf-macros-0.1.1 (c (n "zeroconf-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)))) (h "03rm8ja00i90mjjspk48ll68f2lpm4v3q0l8xam4q5c06aaqa52h")))

(define-public crate-zeroconf-macros-0.1.2 (c (n "zeroconf-macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)))) (h "0mavd2aiw6fb26d158ssw3dl94gbz27bhf0rn7sd7w9pfc8i2vnr")))

(define-public crate-zeroconf-macros-0.1.3 (c (n "zeroconf-macros") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "11ix4i2z6b84jnd88knp7gzq333jxgf8fsc8bq225zk10hxjdsc2")))

(define-public crate-zeroconf-macros-0.1.4 (c (n "zeroconf-macros") (v "0.1.4") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (d #t) (k 0)))) (h "1nyw985hiz2qrmflpsq5446f7nspmfhxz0zbmvrb37af9s1c8973")))

