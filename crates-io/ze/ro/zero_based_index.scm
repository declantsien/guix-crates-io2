(define-module (crates-io ze ro zero_based_index) #:use-module (crates-io))

(define-public crate-zero_based_index-0.1.0 (c (n "zero_based_index") (v "0.1.0") (d (list (d (n "include_display_mode_tex") (r "^0.1.0") (d #t) (k 0)))) (h "0zg93sb8n6drx4faswa0l9m83hxp756vx3074012im0x7gjl3gf3") (f (quote (("unchecked_math")))) (r "1.59.0")))

(define-public crate-zero_based_index-0.1.1 (c (n "zero_based_index") (v "0.1.1") (d (list (d (n "include_display_mode_tex") (r "^0.1.0") (d #t) (k 0)))) (h "1mfzf268wcy59q82wvjxzdl3w5smypbz496rz7g9rgmikpk7k8w7") (f (quote (("unchecked_math")))) (r "1.59.0")))

(define-public crate-zero_based_index-0.2.1 (c (n "zero_based_index") (v "0.2.1") (d (list (d (n "include_display_mode_tex") (r "^0.1.0") (d #t) (k 0)))) (h "1gl1pyrqajpy1jqxxsggxzyq0yl3z6pir0nydgnjrq30vw11v16z") (f (quote (("unchecked_math")))) (r "1.59.0")))

(define-public crate-zero_based_index-0.2.2 (c (n "zero_based_index") (v "0.2.2") (d (list (d (n "const_fn") (r "^0.4.9") (o #t) (d #t) (k 0)) (d (n "include_display_mode_tex") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "0488cchhbrw303ymn8p0gibfgqhzx324zhp7al75zcmphscf6q6f") (f (quote (("unchecked_math") ("nightly" "unchecked_math" "const_inherent_unchecked_arith") ("doc" "include_display_mode_tex") ("const_inherent_unchecked_arith" "const_fn")))) (r "1.59.0")))

