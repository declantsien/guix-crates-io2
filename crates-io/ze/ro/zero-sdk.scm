(define-module (crates-io ze ro zero-sdk) #:use-module (crates-io))

(define-public crate-zero-sdk-0.1.0 (c (n "zero-sdk") (v "0.1.0") (d (list (d (n "httpmock") (r "^0.6.6") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json" "charset"))) (d #t) (k 0)))) (h "1c9s1z9qclh3xwcqcxj3hcr0s5rrvfwg0kk2x7bxdw0g48szks62")))

(define-public crate-zero-sdk-0.1.1 (c (n "zero-sdk") (v "0.1.1") (d (list (d (n "httpmock") (r "^0.6.6") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json" "charset"))) (d #t) (k 0)))) (h "1yp6xa18jc7jwinc95pn1fkq9g7c3v32y64rng3wl64nixlp1j5l")))

(define-public crate-zero-sdk-0.1.2 (c (n "zero-sdk") (v "0.1.2") (d (list (d (n "httpmock") (r "^0.6.6") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json" "charset"))) (d #t) (k 0)))) (h "077p4rmr3hpi1hcf9x2fw7627xgn4z3bzzfa2lj7jsvixdza4204")))

(define-public crate-zero-sdk-0.1.3 (c (n "zero-sdk") (v "0.1.3") (d (list (d (n "httpmock") (r "^0.6.6") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json" "charset"))) (d #t) (k 0)))) (h "0p2fs9n1r9jiszccsj87ykkc00qv40vlidfpflqqh823jf18a1rn")))

(define-public crate-zero-sdk-0.1.4 (c (n "zero-sdk") (v "0.1.4") (d (list (d (n "httpmock") (r "^0.6.6") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json" "charset"))) (d #t) (k 0)))) (h "1qj57zqiyygis8gyfac46d2j4izag3p0fbmmsbhpaj19j36djpyw")))

(define-public crate-zero-sdk-0.2.0 (c (n "zero-sdk") (v "0.2.0") (d (list (d (n "httpmock") (r "^0.6.6") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("json" "charset"))) (d #t) (k 0)))) (h "0mqq3pf82yppgrkxq8ccgid8k70yaay9r3p3dhciw4ixagsrp79b")))

