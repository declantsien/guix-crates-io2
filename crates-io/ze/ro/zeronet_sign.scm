(define-module (crates-io ze ro zeronet_sign) #:use-module (crates-io))

(define-public crate-zeronet_sign-0.1.9 (c (n "zeronet_sign") (v "0.1.9") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zeronet_cryptography") (r "^0.1.9") (d #t) (k 0)))) (h "1all1fiw97f1fyp8x8qbd5clrx1dan9i3zyhgm7b4ldm3nxazw3w")))

