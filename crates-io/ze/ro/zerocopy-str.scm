(define-module (crates-io ze ro zerocopy-str) #:use-module (crates-io))

(define-public crate-zerocopy-str-0.1.1 (c (n "zerocopy-str") (v "0.1.1") (d (list (d (n "borsh") (r "^0.10.3") (d #t) (k 2)) (d (n "shadow-nft-common") (r "^0.1.1") (d #t) (k 0)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)))) (h "1mvvpazw54b3y8cnndc6rbfixs9zdc8547wm947cqphss7021wf6")))

