(define-module (crates-io ze ro zerodmg-utils) #:use-module (crates-io))

(define-public crate-zerodmg-utils-0.0.1 (c (n "zerodmg-utils") (v "0.0.1") (h "0ncj8bddkvw10z0yqxfmsrh1wgsiy5v3dfv8y6mhwsw9varwp09x") (y #t)))

(define-public crate-zerodmg-utils-0.1.0 (c (n "zerodmg-utils") (v "0.1.0") (d (list (d (n "image") (r "^0.19.0") (d #t) (k 0)))) (h "158z0bpc6xns2hikj7kh4nvqygy6gg80xnb342cxq9aqckan0644") (y #t)))

(define-public crate-zerodmg-utils-0.1.1 (c (n "zerodmg-utils") (v "0.1.1") (d (list (d (n "image") (r "^0.19.0") (d #t) (k 0)))) (h "0m9yfz2dxz94x35zdp9hmz3k01iva9djxvp4i5q4ngj3pqx1vp47") (y #t)))

(define-public crate-zerodmg-utils-0.1.2 (c (n "zerodmg-utils") (v "0.1.2") (d (list (d (n "image") (r "^0.19.0") (d #t) (k 0)))) (h "1jcc0y39pylvcshc17y4q0vn4dcagn2bqbrm5whh8bmsi1r8m7lr") (y #t)))

(define-public crate-zerodmg-utils-0.1.3 (c (n "zerodmg-utils") (v "0.1.3") (d (list (d (n "image") (r "^0.19.0") (d #t) (k 0)))) (h "0x7f72pc8fbywbgs4p0c4laqk8rib6cy3qd5rwy6b65wpsyjl9fq") (y #t)))

(define-public crate-zerodmg-utils-0.1.4 (c (n "zerodmg-utils") (v "0.1.4") (d (list (d (n "image") (r "^0.19.0") (d #t) (k 0)))) (h "0df9z5ca2sd19nk1hpn3531h091zx8b8783c9685qa6cdvs8rvd6") (y #t)))

(define-public crate-zerodmg-utils-0.1.5 (c (n "zerodmg-utils") (v "0.1.5") (d (list (d (n "image") (r "^0.19.0") (d #t) (k 0)))) (h "1fl90ph5cgw676d8s1rp84x8fmmjb05alw9di9a6h1cw6zxhdmmw") (y #t)))

(define-public crate-zerodmg-utils-0.1.6 (c (n "zerodmg-utils") (v "0.1.6") (d (list (d (n "image") (r "^0.19.0") (d #t) (k 0)))) (h "0kzbh673apgg3cjmks21dh7k0krfsvz6ps23ykn3c9ksc95zvkg5") (y #t)))

(define-public crate-zerodmg-utils-0.1.7 (c (n "zerodmg-utils") (v "0.1.7") (d (list (d (n "image") (r "^0.19.0") (d #t) (k 0)))) (h "1qrb2nqpbcyc2xzsrsky93lv7nq94k8c330c62gr08p68687h15b") (y #t)))

(define-public crate-zerodmg-utils-0.1.8 (c (n "zerodmg-utils") (v "0.1.8") (d (list (d (n "image") (r "^0.19.0") (d #t) (k 0)))) (h "08xy8adbjsc889kyfjlpn1r72yhzg4hfbhjrrrnq1dg5sx7h4mij") (y #t)))

(define-public crate-zerodmg-utils-0.1.9 (c (n "zerodmg-utils") (v "0.1.9") (d (list (d (n "image") (r "^0.19.0") (d #t) (k 0)))) (h "039xdagdnjp1qib3lc30h7xpp0hm6101lxv12r77h2c7ciz5vqmr") (y #t)))

(define-public crate-zerodmg-utils-0.1.10-zerodmg-utils (c (n "zerodmg-utils") (v "0.1.10-zerodmg-utils") (h "0kv6ccmsn6qws7h9vx466wcr289kfaa3c56wb1nqjc2qxgpjas0y") (y #t)))

(define-public crate-zerodmg-utils-0.0.0-- (c (n "zerodmg-utils") (v "0.0.0--") (h "054lgva7x470r81aq87zypbazgjk4bsdms940znjhnmvw9qmywgh") (y #t)))

