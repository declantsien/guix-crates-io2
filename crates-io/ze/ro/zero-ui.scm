(define-module (crates-io ze ro zero-ui) #:use-module (crates-io))

(define-public crate-zero-ui-0.1.0 (c (n "zero-ui") (v "0.1.0") (h "0apb5fah75062fshkajv6l7vgwgb0cd0l81bx3n29pbxa1607p56")))

(define-public crate-zero-ui-0.1.1 (c (n "zero-ui") (v "0.1.1") (d (list (d (n "leptos") (r "^0.5.2") (f (quote ("csr" "nightly"))) (d #t) (k 0)) (d (n "leptos-use") (r "^0.8.2") (d #t) (k 0)))) (h "103kwslhvb3zg3k9hfsqan80w1k361zfc9lacax0wlwlf59wq2h0") (f (quote (("ssr" "leptos/ssr" "leptos-use/ssr") ("hydrate" "leptos/hydrate") ("csr" "leptos/csr"))))))

(define-public crate-zero-ui-0.1.2 (c (n "zero-ui") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "leptos") (r "^0.5.2") (f (quote ("csr" "nightly"))) (d #t) (k 0)) (d (n "leptos-use") (r "^0.8.2") (d #t) (k 0)) (d (n "leptos_icons") (r "^0.1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "17igfagbx77f6yxmgcsm6wz21vzk1c0ggxg6vbi4422jvs8d4s04") (f (quote (("ssr" "leptos/ssr" "leptos-use/ssr") ("hydrate" "leptos/hydrate") ("csr" "leptos/csr"))))))

(define-public crate-zero-ui-0.1.3 (c (n "zero-ui") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "icondata_core") (r "^0.0.2") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "leptos") (r "^0.5.2") (f (quote ("csr" "nightly"))) (d #t) (k 0)) (d (n "leptos-use") (r "^0.8.2") (d #t) (k 0)) (d (n "leptos_icons") (r "^0.1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1jmdnpf5vcjp0v9c05vfaqlk6ravwcmbnhc38j0dcx8k9iavy1zk") (f (quote (("ssr" "leptos/ssr" "leptos-use/ssr") ("hydrate" "leptos/hydrate") ("csr" "leptos/csr"))))))

