(define-module (crates-io ze ro zero-crypto) #:use-module (crates-io))

(define-public crate-zero-crypto-0.1.0 (c (n "zero-crypto") (v "0.1.0") (h "0clr5m0cy3q4qi5kwrmbk15fafkgmv6ym2v4vh6qqkxhd1v993bm") (y #t)))

(define-public crate-zero-crypto-0.1.1 (c (n "zero-crypto") (v "0.1.1") (d (list (d (n "parity-scale-codec") (r "^2") (f (quote ("derive"))) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (k 2)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "rand_xorshift") (r "^0.3") (k 2)) (d (n "sp-std") (r "^3.0.0") (k 0)))) (h "0lkc74h8h4x1s8rgpxqhcf09msk50hm4w8q6fblm1v7kb4swxkyw") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-zero-crypto-0.1.2 (c (n "zero-crypto") (v "0.1.2") (d (list (d (n "parity-scale-codec") (r "^2") (f (quote ("derive"))) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (k 2)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "rand_xorshift") (r "^0.3") (k 2)) (d (n "sp-std") (r "^3.0.0") (k 0)))) (h "0ardvk9wn13w3gz8aqnbbr549qlwyy1xqrmk52c5xkwljhf1j3g1") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-zero-crypto-0.2.0 (c (n "zero-crypto") (v "0.2.0") (d (list (d (n "parity-scale-codec") (r "^2") (f (quote ("derive"))) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (k 2)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "rand_xorshift") (r "^0.3") (k 2)) (d (n "sp-std") (r "^3.0.0") (k 0)) (d (n "subtle") (r "^2.0") (k 0)))) (h "06jvwn831ngcb25mhxry9n0yv8snf9gv47sxbjvzbvfg4djij81p") (y #t)))

(define-public crate-zero-crypto-0.2.1 (c (n "zero-crypto") (v "0.2.1") (d (list (d (n "parity-scale-codec") (r "^2") (f (quote ("derive"))) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (k 2)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "rand_xorshift") (r "^0.3") (k 2)) (d (n "sp-std") (r "^3.0.0") (k 0)) (d (n "subtle") (r "^2.0") (k 0)))) (h "1yk9mv5w7y1cz03ch0zwf55bkyfcanw1d7k9fqi2rsmdd0w1ry6q") (y #t)))

(define-public crate-zero-crypto-0.2.2 (c (n "zero-crypto") (v "0.2.2") (d (list (d (n "parity-scale-codec") (r "^2") (f (quote ("derive"))) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (k 2)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "rand_xorshift") (r "^0.3") (k 2)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (k 2)) (d (n "sp-std") (r "^3.0.0") (k 0)) (d (n "subtle") (r "^2.0") (k 0)) (d (n "zero-bls12-381") (r "^0.2.0") (k 2)))) (h "0d2rw3wgbs8yma2zf2x3ihw1qjcfhbmckamgs5w6l8f8hpp2i3nk") (y #t)))

(define-public crate-zero-crypto-0.2.3 (c (n "zero-crypto") (v "0.2.3") (d (list (d (n "parity-scale-codec") (r "^2") (f (quote ("derive"))) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (k 2)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "rand_xorshift") (r "^0.3") (k 2)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (k 2)) (d (n "sp-std") (r "^3.0.0") (k 0)) (d (n "subtle") (r "^2.0") (k 0)) (d (n "zero-bls12-381") (r "^0.2.0") (k 2)))) (h "1pvsp71gjnyv79ydf5nrf468307nny10r0hzgrvsjl0sf50fx15l") (y #t)))

(define-public crate-zero-crypto-0.2.5 (c (n "zero-crypto") (v "0.2.5") (d (list (d (n "parity-scale-codec") (r "^2") (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (k 2)) (d (n "rand_core") (r "^0.6.4") (k 0)) (d (n "rand_xorshift") (r "^0.3") (k 2)) (d (n "sp-std") (r "^3.0.0") (k 0)))) (h "1kdflyip3in2hlbi02v5jn619wnm4hvl7cdqlvl1hbq3f5giy4i2") (y #t)))

(define-public crate-zero-crypto-0.1.10 (c (n "zero-crypto") (v "0.1.10") (d (list (d (n "parity-scale-codec") (r "^2") (f (quote ("derive"))) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (k 2)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "rand_xorshift") (r "^0.3") (k 2)) (d (n "sp-std") (r "^3.0.0") (k 0)))) (h "1i5v2m6hnvfkld6mkq601vnp8h1w87ckr9pa5axxvdghxfg657jj")))

(define-public crate-zero-crypto-0.1.11 (c (n "zero-crypto") (v "0.1.11") (d (list (d (n "dusk-bytes") (r "^0.1") (d #t) (k 0)) (d (n "parity-scale-codec") (r "^2") (f (quote ("derive"))) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.4") (k 0)) (d (n "sp-std") (r "^3.0.0") (k 0)) (d (n "subtle") (r "^2.0") (k 0)))) (h "1s2r44pjdd21zp2n2lqsnzra87j41j7n3s16n300c2r3276s759k") (f (quote (("std" "rand_core/getrandom") ("default" "std"))))))

