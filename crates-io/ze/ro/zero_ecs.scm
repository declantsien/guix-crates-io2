(define-module (crates-io ze ro zero_ecs) #:use-module (crates-io))

(define-public crate-zero_ecs-0.1.1 (c (n "zero_ecs") (v "0.1.1") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.1.0") (d #t) (k 0)))) (h "121bzgfjvpb4r9vjlc6h150dfj5znvmpiiqqds1k62vqzsfz2dk5")))

(define-public crate-zero_ecs-0.1.2 (c (n "zero_ecs") (v "0.1.2") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.1.2") (d #t) (k 0)))) (h "1zc7mndjj9dlim4hx1nkykq13sgj1ax7yaaq8l3nfkq38vblrcqx")))

(define-public crate-zero_ecs-0.1.3-beta.1 (c (n "zero_ecs") (v "0.1.3-beta.1") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.1.3-beta.1") (d #t) (k 0)))) (h "1rwpq6c53zds8ljad16kysglrl2xds7fdql3bq0g9d972my43j32")))

(define-public crate-zero_ecs-0.1.3-beta.2 (c (n "zero_ecs") (v "0.1.3-beta.2") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.1.3-beta.2") (d #t) (k 0)))) (h "1p6ywy53fz4xrfbzyxl7c4b45dp0mr1bsl7hbpkb1k86b51rss4w")))

(define-public crate-zero_ecs-0.2.0 (c (n "zero_ecs") (v "0.2.0") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.0") (d #t) (k 0)))) (h "1xxnf2yjys69wb8nprjmlkksy228n93629cqxkxjrif4k3051rqk")))

(define-public crate-zero_ecs-0.2.1 (c (n "zero_ecs") (v "0.2.1") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.1") (d #t) (k 0)))) (h "1qqv9w27fz7kxgpdnx92d4dvz42l27jj3s04bmyxw1ajsckysv55")))

(define-public crate-zero_ecs-0.2.2 (c (n "zero_ecs") (v "0.2.2") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.2") (d #t) (k 0)))) (h "0mpybm0dw560qddws1f5zhm70mpxlmwqh0hlfivs9szpnm2kanax")))

(define-public crate-zero_ecs-0.2.3 (c (n "zero_ecs") (v "0.2.3") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.3") (d #t) (k 0)))) (h "1cc2r9rk6yl7qbcb1k1xqaynd6lwb3q50w6f51xrn0666j1mh2mw")))

(define-public crate-zero_ecs-0.2.4 (c (n "zero_ecs") (v "0.2.4") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.4") (d #t) (k 0)))) (h "0iyxqjbxyiwi7v3sk4byipz1vmzbifsmxfpv46l8c5f8a2mvq5kr")))

(define-public crate-zero_ecs-0.2.5 (c (n "zero_ecs") (v "0.2.5") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.5") (d #t) (k 0)))) (h "0xbwpb3siaib8vw5ycm5r4258kl5fhpn9j95w6r0ig6cks0xj0bn")))

(define-public crate-zero_ecs-0.2.8 (c (n "zero_ecs") (v "0.2.8") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.8") (d #t) (k 0)))) (h "1hwgpf5j7lk4fvalf7xcj2cfxq5kv6hixra04425p0rh1wbn82k0")))

(define-public crate-zero_ecs-0.2.10 (c (n "zero_ecs") (v "0.2.10") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.10") (d #t) (k 0)))) (h "1vrqk53f4m09km1ci2hxanijc4ns4p4h7n43nrl65fchzqk56764")))

(define-public crate-zero_ecs-0.2.11 (c (n "zero_ecs") (v "0.2.11") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.11") (d #t) (k 0)))) (h "0y8pm479l66kk79fw39hrn3vwrj8db34yb8w29q6sxxf2ikrjkin")))

(define-public crate-zero_ecs-0.2.12 (c (n "zero_ecs") (v "0.2.12") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.12") (d #t) (k 0)))) (h "1nia4m737w7195r3w9qpkaraacfv93qir2wc3by0a1d5lk2qj0h8")))

(define-public crate-zero_ecs-0.2.13 (c (n "zero_ecs") (v "0.2.13") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.13") (d #t) (k 0)))) (h "1xln4v2i2sqiiywfbl4km8gffdrkc3djsxxg4hjv0qx5vagagyd9")))

(define-public crate-zero_ecs-0.2.14 (c (n "zero_ecs") (v "0.2.14") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.14") (d #t) (k 0)))) (h "1mi5aw9252gwwxq8yaks6zqrz0f3icmnz66vzm4kr0jqzwmiwi9l")))

(define-public crate-zero_ecs-0.2.15 (c (n "zero_ecs") (v "0.2.15") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.15") (d #t) (k 0)))) (h "11x87zczygfj2rgp0xb2f58vca0cm5mcc0yzw1av1dm2618nf2v7")))

(define-public crate-zero_ecs-0.2.17 (c (n "zero_ecs") (v "0.2.17") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.17") (d #t) (k 0)))) (h "0rzikdgvg9xs92bg5lky38vvf7kg7fxlnd6wnvn8vbdjpw1y5cqw")))

(define-public crate-zero_ecs-0.2.18 (c (n "zero_ecs") (v "0.2.18") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.18") (d #t) (k 0)))) (h "1d9wmasd3kz3hji0ymw15s4vpjqkjmwbkc9vllyhci9jgaw7ksc2")))

(define-public crate-zero_ecs-0.2.19 (c (n "zero_ecs") (v "0.2.19") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.19") (d #t) (k 0)))) (h "0whsa43kag5b89kl9ia322jvsqrbk2sc8zc0vmvfalbq1xwzhg6w")))

(define-public crate-zero_ecs-0.2.20 (c (n "zero_ecs") (v "0.2.20") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.20") (d #t) (k 0)))) (h "084bb3pgahl7c7ghl401hcj8az8fmjcz1nj4kg90d071w9kx9jz6")))

(define-public crate-zero_ecs-0.2.21 (c (n "zero_ecs") (v "0.2.21") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.21") (d #t) (k 0)))) (h "06s2d2z3nbn1kzfw2zx3nf0v6bvprrsdgf8ia9kdk3fyq3cip77l")))

(define-public crate-zero_ecs-0.2.22 (c (n "zero_ecs") (v "0.2.22") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.22") (d #t) (k 0)))) (h "175sqm8ckw9c0h2vmsn42b2k2pq478wwgxgm48ngzkhn80yz935f")))

