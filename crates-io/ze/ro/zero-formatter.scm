(define-module (crates-io ze ro zero-formatter) #:use-module (crates-io))

(define-public crate-zero-formatter-0.1.0 (c (n "zero-formatter") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.2") (d #t) (k 0)))) (h "0yn8xz6fmy4j7b0axrb4kxjs4i5icfnglhdmyh14sf1q40aywdb8")))

