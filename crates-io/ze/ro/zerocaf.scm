(define-module (crates-io ze ro zerocaf) #:use-module (crates-io))

(define-public crate-zerocaf-0.1.0 (c (n "zerocaf") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "curve25519-dalek") (r "^1.1.3") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "subtle") (r "^2") (k 0)))) (h "13bzwl9c4hvmmidfilh2cy66ma9y2hs3qjxhn2d8gzyr84za6bm2") (f (quote (("u64_backend") ("nightly" "subtle/nightly") ("default" "u64_backend"))))))

(define-public crate-zerocaf-0.1.1 (c (n "zerocaf") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "curve25519-dalek") (r "^1.1.3") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "subtle") (r "^2") (k 0)))) (h "131ak2cb097sbbrpa23zb3hpydqcjskldpshk3gqh3vjj2n5l8c0") (f (quote (("u64_backend") ("nightly" "subtle/nightly") ("default" "u64_backend"))))))

(define-public crate-zerocaf-0.2.0 (c (n "zerocaf") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "curve25519-dalek") (r "^1.1.3") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)) (d (n "subtle") (r "^2") (k 0)))) (h "1rvlchrvw5nqifnimpf89m3cs4aycx46k9n0zk3ihqa6x32msjgd") (f (quote (("u64_backend") ("nightly" "subtle/nightly") ("default" "u64_backend"))))))

