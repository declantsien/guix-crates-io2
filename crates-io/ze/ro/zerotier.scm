(define-module (crates-io ze ro zerotier) #:use-module (crates-io))

(define-public crate-zerotier-0.1.0 (c (n "zerotier") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.0-pre.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "salsa20") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.89") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "x25519-dalek") (r "^0.5.2") (d #t) (k 0)))) (h "05s51rj3gqbdwizgai3f6lxnxw3rh89dgraq3ky4ndx9y2hrpxy9")))

