(define-module (crates-io ze ro zeronet_sign_derive) #:use-module (crates-io))

(define-public crate-zeronet_sign_derive-0.1.9 (c (n "zeronet_sign_derive") (v "0.1.9") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "derive" "extra-traits"))) (d #t) (k 0)) (d (n "zeronet_cryptography") (r "^0.1.9") (d #t) (k 0)) (d (n "zeronet_sign") (r "^0.1.9") (d #t) (k 0)))) (h "04gw89bb5bq78kinx3smy3lc63v03wszhwp6af8basj5r5750bm3")))

