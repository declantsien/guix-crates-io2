(define-module (crates-io ze ro zero_v_gen) #:use-module (crates-io))

(define-public crate-zero_v_gen-0.1.0 (c (n "zero_v_gen") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "1pwlvxvzq64qzi0y6r69lajfx4dzs0xfamfl4bdyj9n51rj14117")))

