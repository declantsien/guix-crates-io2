(define-module (crates-io ze ro zeroize) #:use-module (crates-io))

(define-public crate-zeroize-0.1.0 (c (n "zeroize") (v "0.1.0") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 1)))) (h "1gsmrbx0l4g7bv86r5la7ys3yrgwmj9i76k4afvdcwlaj1r9bbdz") (f (quote (("windows" "cc") ("nightly") ("linux-backport" "cc") ("default" "linux-backport" "windows")))) (y #t)))

(define-public crate-zeroize-0.1.1 (c (n "zeroize") (v "0.1.1") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 1)))) (h "1zja7q4ikk51f6k1wcpgg1j5k51kiprfwcnsbj6g4k49ldl0vycd") (f (quote (("windows" "cc") ("nightly") ("linux-backport" "cc") ("default" "linux-backport" "windows")))) (y #t)))

(define-public crate-zeroize-0.1.2 (c (n "zeroize") (v "0.1.2") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 1)))) (h "0xnxw668jgwmy9dpkhrjqyxifpj9zdy16ldic29l37h525izfy6m") (f (quote (("windows" "cc") ("nightly") ("linux-backport" "cc") ("default" "linux-backport" "windows")))) (y #t)))

(define-public crate-zeroize-0.2.0 (c (n "zeroize") (v "0.2.0") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 1)))) (h "1hqgxgs35ipm44r2fz3fpwn2z6ykbrgjlsjk33h648113b2nqssm") (f (quote (("windows" "cc") ("nightly") ("linux-backport" "cc") ("default" "linux-backport" "windows")))) (y #t)))

(define-public crate-zeroize-0.3.0 (c (n "zeroize") (v "0.3.0") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 1)))) (h "0k83wflir9pjy49070hd4bhwxjsanzjhl660xivhnk4flb5wxfp0") (f (quote (("windows" "cc") ("nightly") ("linux-backport" "cc") ("default" "linux-backport" "windows")))) (y #t)))

(define-public crate-zeroize-0.4.0 (c (n "zeroize") (v "0.4.0") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 1)))) (h "084z5l46mc3s6ig1kk7cw6wsn2shm6raah4jmv47i539j8rs172d") (f (quote (("windows" "cc") ("std") ("nightly") ("linux-backport" "cc") ("default" "linux-backport" "std" "windows")))) (y #t)))

(define-public crate-zeroize-0.4.1 (c (n "zeroize") (v "0.4.1") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 1)) (d (n "semver") (r "^0.9") (o #t) (d #t) (k 1)))) (h "1gg4226v866cahx4l4nb1vm6xyqxcz0w4xyigjarlmswnkpxb6gy") (f (quote (("windows" "cc") ("std") ("nightly") ("linux-backport" "cc" "semver") ("default" "linux-backport" "std" "windows")))) (y #t)))

(define-public crate-zeroize-0.4.2 (c (n "zeroize") (v "0.4.2") (d (list (d (n "cc") (r "^1") (o #t) (d #t) (k 1)) (d (n "semver") (r "^0.9") (o #t) (d #t) (k 1)))) (h "0fbwsinb9yp6n7gh7h9b24mycwk7vkqcp6rmssi8pwnykpngzpfp") (f (quote (("windows" "cc") ("std") ("nightly") ("linux-backport" "cc" "semver") ("default" "linux-backport" "std" "windows")))) (y #t)))

(define-public crate-zeroize-0.5.0 (c (n "zeroize") (v "0.5.0") (h "1cymhr085y2vip6b8i1ypb1k9ss0ym7j3lyk1m70nvvnpb1n26fy") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-zeroize-0.5.1 (c (n "zeroize") (v "0.5.1") (h "1pnvn7vj899y1nr3bhv0avh383q8cx32idq751649l855jniaqpk") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-zeroize-0.5.2 (c (n "zeroize") (v "0.5.2") (h "057a8s180bygnc9s6mar1n7bnqvm5fjrh23fxxib5crgxrpfppwd") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc")))) (y #t)))

(define-public crate-zeroize-0.6.0 (c (n "zeroize") (v "0.6.0") (d (list (d (n "zeroize_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "15gcjnscyma8jzxwm80kkyxaqbxbx7gn4kk1265m7bxnb2w07176") (f (quote (("std" "alloc") ("nightly") ("default" "std" "zeroize_derive") ("alloc")))) (y #t)))

(define-public crate-zeroize-0.7.0 (c (n "zeroize") (v "0.7.0") (d (list (d (n "zeroize_derive") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0yiinz5ynahrxjsy5phmk8kxs2ggk3r8brc6z8xp7rxc5y9fqgnw") (f (quote (("std" "alloc") ("default" "std" "zeroize_derive") ("alloc")))) (y #t)))

(define-public crate-zeroize-0.8.0 (c (n "zeroize") (v "0.8.0") (d (list (d (n "zeroize_derive") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0rlgzix26ccvg1rmzmh7vng6jh2vzs20v5b0ljqfrn4i5dbnq2mn") (f (quote (("std" "alloc") ("default" "std" "zeroize_derive") ("alloc")))) (y #t)))

(define-public crate-zeroize-0.9.0 (c (n "zeroize") (v "0.9.0") (d (list (d (n "zeroize_derive") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0nhs149y01mnw1ig273j4r0vy49nq7jrrjns8f64xrzkzhn02dg1") (f (quote (("std" "alloc") ("default" "std" "zeroize_derive") ("alloc")))) (y #t)))

(define-public crate-zeroize-0.9.1 (c (n "zeroize") (v "0.9.1") (d (list (d (n "zeroize_derive") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0hvh6a5x30vz33bvrwv6lly7w3y3hdq09gs2dgi9g51fqaps8bjy") (f (quote (("std" "alloc") ("default" "std" "zeroize_derive") ("alloc")))) (y #t)))

(define-public crate-zeroize-0.9.2 (c (n "zeroize") (v "0.9.2") (d (list (d (n "zeroize_derive") (r "^0.9") (o #t) (d #t) (k 0)))) (h "04zjwh7mg84zf9ijdsbgqrmycs6jmm39ql74p30lk8xm0dn96xs1") (f (quote (("std" "alloc") ("default" "std" "zeroize_derive") ("alloc")))) (y #t)))

(define-public crate-zeroize-0.9.3 (c (n "zeroize") (v "0.9.3") (d (list (d (n "zeroize_derive") (r "^0.9") (o #t) (d #t) (k 0)))) (h "11iz5cgjlcrdc5smigs6krmmk9gbnaiaajy9aidwzr0k1l0nmbs5") (f (quote (("std" "alloc") ("default" "std" "zeroize_derive") ("alloc")))) (y #t)))

(define-public crate-zeroize-0.10.0 (c (n "zeroize") (v "0.10.0") (d (list (d (n "zeroize_derive") (r "^0.10") (o #t) (d #t) (k 0)))) (h "046a203bz1kg7fz9jsdi8jglivbg7zjr6b8wq03qkhzkpf78advc") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-zeroize-0.10.1 (c (n "zeroize") (v "0.10.1") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "zeroize_derive") (r "^0.10") (o #t) (d #t) (k 0)))) (h "081iq1maxy4q3p66ii21kx2aaiz24nsvp8pvcsqzfc36lrzli420") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-zeroize-1.0.0-pre (c (n "zeroize") (v "1.0.0-pre") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "zeroize_derive") (r "^1.0.0-pre") (o #t) (d #t) (k 0)))) (h "1xzdfn8wj1x7gs8ssh9mymjrf928k11z76xd7ns8vygaw4k62hsm") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-zeroize-1.0.0 (c (n "zeroize") (v "1.0.0") (d (list (d (n "bytes") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "zeroize_derive") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1lv2aiqn8bh34dr50cjf4sp9lmqvz2ixii2pqf283lganpcpkjfd") (f (quote (("default" "alloc") ("bytes-preview" "bytes") ("alloc"))))))

(define-public crate-zeroize-1.1.0 (c (n "zeroize") (v "1.1.0") (d (list (d (n "zeroize_derive") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1f5547q8l8bpi16yy6lix2gl9rf1qz45lj06bq7wjk525gnw5fiw") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-zeroize-1.1.1 (c (n "zeroize") (v "1.1.1") (d (list (d (n "zeroize_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "12m2ypry9vbh6j89gy2fbill72xrjkmr214bb39x5nvaarr3kwq5") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-zeroize-1.2.0 (c (n "zeroize") (v "1.2.0") (d (list (d (n "zeroize_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "0dhwwh63cnishw5jvvf9li2lscin6jq7srs19p50szrmvny79ac1") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-zeroize-1.3.0 (c (n "zeroize") (v "1.3.0") (d (list (d (n "zeroize_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "1z8yix823b6lz878qwg6bvwhg3lb0cbw3c9yij9p8mbv7zdzfmj7") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-zeroize-1.4.0 (c (n "zeroize") (v "1.4.0") (d (list (d (n "zeroize_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "061d5bq9657izmcyasysh8n5k56pv5nalx4b6afqfb6b6w9ydbzf") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-zeroize-1.4.1 (c (n "zeroize") (v "1.4.1") (d (list (d (n "zeroize_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "1kd0v8bg208xj5agkihb0nlzbig1aba7sy3lf4vsxxqmc22b0z9p") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-zeroize-1.4.2 (c (n "zeroize") (v "1.4.2") (d (list (d (n "zeroize_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "0w79swbr48k2hd2ckkfg0lkvz3554yn7yji6j2kym3bn2f2v0s5z") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-zeroize-1.4.3 (c (n "zeroize") (v "1.4.3") (d (list (d (n "zeroize_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "068nvl3n5hk6lfn5y24grf2c7anzzqfzjjccscq3md7rqp79v3fn") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-zeroize-1.5.0-pre (c (n "zeroize") (v "1.5.0-pre") (d (list (d (n "zeroize_derive") (r "=1.3.0-pre") (o #t) (d #t) (k 0)))) (h "0f7nld8r4ifwm9yyhx5z1q5c8v5xw7s8zmkljbd98g8sn5x348dp") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-zeroize-1.5.0 (c (n "zeroize") (v "1.5.0") (d (list (d (n "zeroize_derive") (r "^1.3") (o #t) (d #t) (k 0)))) (h "1nq8zq5h7ad6kahdk0fxw4fdm0ibycj061jngxqkqchw67n2l8nc") (f (quote (("derive" "zeroize_derive") ("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-zeroize-1.5.1 (c (n "zeroize") (v "1.5.1") (d (list (d (n "zeroize_derive") (r "^1.3") (o #t) (d #t) (k 0)))) (h "06dpq9fakzbsvj99aaxsj2dy9863f59qkig94zkhpn88pr4wfqj0") (f (quote (("derive" "zeroize_derive") ("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-zeroize-1.5.2 (c (n "zeroize") (v "1.5.2") (d (list (d (n "zeroize_derive") (r "^1.3") (o #t) (d #t) (k 0)))) (h "01k0xmqd7l3yqzdfii7c5gxvdrb6m7bgi8l5q87f17n3cc08g23w") (f (quote (("derive" "zeroize_derive") ("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-zeroize-1.5.3 (c (n "zeroize") (v "1.5.3") (d (list (d (n "zeroize_derive") (r "^1.3") (o #t) (d #t) (k 0)))) (h "0276kspsl58z8pk9z497inyqwpvvlldgkxn8rwgkl3plw9c4fd2h") (f (quote (("derive" "zeroize_derive") ("default" "alloc") ("alloc"))))))

(define-public crate-zeroize-1.5.4 (c (n "zeroize") (v "1.5.4") (d (list (d (n "zeroize_derive") (r "^1.3") (o #t) (d #t) (k 0)))) (h "05q3ynz820xxpx0zqcf28mgx7bpzak2x9qcwhq52hgzxia5p5dby") (f (quote (("derive" "zeroize_derive") ("default" "alloc") ("alloc") ("aarch64"))))))

(define-public crate-zeroize-1.5.5 (c (n "zeroize") (v "1.5.5") (d (list (d (n "zeroize_derive") (r "^1.3") (o #t) (d #t) (k 0)))) (h "01yzgi5n5skl7l8wypbpzw8r6s6azhxyn824w79g5chns03khscl") (f (quote (("std" "alloc") ("derive" "zeroize_derive") ("default" "alloc") ("alloc") ("aarch64"))))))

(define-public crate-zeroize-1.5.6 (c (n "zeroize") (v "1.5.6") (d (list (d (n "zeroize_derive") (r "^1.3") (o #t) (d #t) (k 0)))) (h "0hj4df4grjm405mqpyz0nixm8zn33vydw6ra7xn6llfqzyn7id90") (f (quote (("std" "alloc") ("derive" "zeroize_derive") ("default" "alloc") ("alloc") ("aarch64"))))))

(define-public crate-zeroize-1.5.7 (c (n "zeroize") (v "1.5.7") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "zeroize_derive") (r "^1.3") (o #t) (d #t) (k 0)))) (h "17ql9c1qhh5kw5aas72swwicnr701alhmhnrfmr9wrkg1jyvb563") (f (quote (("std" "alloc") ("derive" "zeroize_derive") ("default" "alloc") ("alloc") ("aarch64"))))))

(define-public crate-zeroize-1.6.0 (c (n "zeroize") (v "1.6.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "zeroize_derive") (r "^1.3") (o #t) (d #t) (k 0)))) (h "1ndar43r58zbmasjhrhgas168vxb4i0rwbkcnszhjybwpbqmc29a") (f (quote (("std" "alloc") ("derive" "zeroize_derive") ("default" "alloc") ("alloc") ("aarch64")))) (r "1.56")))

(define-public crate-zeroize-1.6.1 (c (n "zeroize") (v "1.6.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "zeroize_derive") (r "^1.3") (o #t) (d #t) (k 0)))) (h "12qasj2jl4bp1xksa3m4j9par22vqav2jihgh21vaaf9rxp998qj") (f (quote (("std" "alloc") ("derive" "zeroize_derive") ("default" "alloc") ("alloc") ("aarch64")))) (y #t) (r "1.60")))

(define-public crate-zeroize-1.7.0 (c (n "zeroize") (v "1.7.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "zeroize_derive") (r "^1.3") (o #t) (d #t) (k 0)))) (h "0bfvby7k9pdp6623p98yz2irqnamcyzpn7zh20nqmdn68b0lwnsj") (f (quote (("std" "alloc") ("derive" "zeroize_derive") ("default" "alloc") ("alloc") ("aarch64")))) (r "1.60")))

(define-public crate-zeroize-1.8.0 (c (n "zeroize") (v "1.8.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "zeroize_derive") (r "^1.3") (o #t) (d #t) (k 0)))) (h "1vsrhciz0dj1fl6wyhjwaq5zh5h1703hsz5qlqq23yabcak1yf33") (f (quote (("std" "alloc") ("derive" "zeroize_derive") ("default" "alloc") ("alloc") ("aarch64")))) (y #t) (r "1.72")))

(define-public crate-zeroize-1.8.1 (c (n "zeroize") (v "1.8.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "zeroize_derive") (r "^1.3") (o #t) (d #t) (k 0)))) (h "1pjdrmjwmszpxfd7r860jx54cyk94qk59x13sc307cvr5256glyf") (f (quote (("std" "alloc") ("simd") ("derive" "zeroize_derive") ("default" "alloc") ("alloc") ("aarch64")))) (r "1.60")))

