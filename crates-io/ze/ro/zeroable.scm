(define-module (crates-io ze ro zeroable) #:use-module (crates-io))

(define-public crate-zeroable-0.1.0 (c (n "zeroable") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.0") (d #t) (k 0)) (d (n "zeroable_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1146cmkixyvl4nmpgjjfhr6zndaiqsiwpm4nzan8blqncclasgw9") (f (quote (("testing") ("print_type") ("nightly_testing" "testing"))))))

(define-public crate-zeroable-0.1.1 (c (n "zeroable") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.0") (d #t) (k 0)) (d (n "zeroable_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1ymngqzg89j1na03j757m2mbv0y872krdhxlkxzl5i6hias4k2r0") (f (quote (("testing") ("print_type") ("nightly_testing" "testing"))))))

(define-public crate-zeroable-0.1.2 (c (n "zeroable") (v "0.1.2") (d (list (d (n "bytemuck") (r "^1.0") (d #t) (k 0)) (d (n "zeroable_derive") (r "^0.1.0") (d #t) (k 0)))) (h "040k4fk76l3qangxp5fwi5i2gdqyik3ss1y2ap4gikhhhq39000w") (f (quote (("testing") ("print_type") ("nightly_testing" "nightly_docs" "testing") ("nightly_docs"))))))

(define-public crate-zeroable-0.2.0 (c (n "zeroable") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.0") (d #t) (k 0)) (d (n "zeroable_derive") (r "^0.2.0") (d #t) (k 0)))) (h "1dpc5j19n5a45mp604dbs4gpkyk9zh67ksx1r03px4q0cb47nshv") (f (quote (("testing") ("print_type") ("nightly_testing" "nightly_docs" "testing") ("nightly_docs" "bytemuck/extern_crate_alloc"))))))

