(define-module (crates-io ze ro zerobuf) #:use-module (crates-io))

(define-public crate-zerobuf-0.0.1 (c (n "zerobuf") (v "0.0.1") (h "0a44jc4divb0gl06xhahc4scgdr6zddfja7cb3xwly85vf12a4zl")))

(define-public crate-zerobuf-0.0.2 (c (n "zerobuf") (v "0.0.2") (h "0c4q5nw7njy6jiw2np91alp1fq57spsax0nk28p38ypp0i33jm1i")))

(define-public crate-zerobuf-0.0.3 (c (n "zerobuf") (v "0.0.3") (h "0ykswpgl24hdab45vixhficm5f17dvgm1qhzy1ypfd6pl36n01d9")))

