(define-module (crates-io ze ro zerodmg-codes) #:use-module (crates-io))

(define-public crate-zerodmg-codes-0.1.7 (c (n "zerodmg-codes") (v "0.1.7") (d (list (d (n "zerodmg-utils") (r "^0.1.7") (d #t) (k 0)))) (h "0w7nfj05isa1pkijsiklhmdca02n7bi4m57cpgriy0gn49rrxcr3") (y #t)))

(define-public crate-zerodmg-codes-0.1.8 (c (n "zerodmg-codes") (v "0.1.8") (d (list (d (n "zerodmg-utils") (r "^0.1.8") (d #t) (k 0)))) (h "05l1zcn6ma1bgvp7p1nrspqf1846v9jlihhlqrzn06px0v8l8i6a") (y #t)))

(define-public crate-zerodmg-codes-0.1.9 (c (n "zerodmg-codes") (v "0.1.9") (d (list (d (n "derive_more") (r "^0.11.0") (d #t) (k 0)) (d (n "zerodmg-utils") (r "^0.1.9") (d #t) (k 0)))) (h "1ccv99mgid8ry1jx1z10xl6dqih4p7373hx3h46i1fdz2a6w8y1c") (y #t)))

(define-public crate-zerodmg-codes-0.1.10-zerodmg-codes (c (n "zerodmg-codes") (v "0.1.10-zerodmg-codes") (h "102jvhp29agicvmgkgww4dgwmax0srxzvc5c94kf6rda01l96d9v") (y #t)))

(define-public crate-zerodmg-codes-0.0.0-- (c (n "zerodmg-codes") (v "0.0.0--") (h "114fiy2aw1fgik70cfvw0zlk2lvi11493rzhc6imz1nh1hdz3ff8") (y #t)))

