(define-module (crates-io ze ro zero_ecs_build) #:use-module (crates-io))

(define-public crate-zero_ecs_build-0.1.1 (c (n "zero_ecs_build") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.1.0") (d #t) (k 0)))) (h "07nc8cna0nmh2vzswnrb2ssyc9xarkid3r2kig8s6nrrfwr3lygf")))

(define-public crate-zero_ecs_build-0.1.2 (c (n "zero_ecs_build") (v "0.1.2") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.1.2") (d #t) (k 0)))) (h "0wqyfxy23hzm6glf9pzmkks854z8s4sxjpp57dwia2fg00xyd0n1")))

(define-public crate-zero_ecs_build-0.1.3-beta.1 (c (n "zero_ecs_build") (v "0.1.3-beta.1") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.1.3-beta.1") (d #t) (k 0)))) (h "1s0xfbaqjbcbpc2japv5l7gjjr7n3zjw1g1pwbvnyymdpiljrxyi")))

(define-public crate-zero_ecs_build-0.1.3-beta.2 (c (n "zero_ecs_build") (v "0.1.3-beta.2") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.1.3-beta.2") (d #t) (k 0)))) (h "1a0dajwzd1yx98ja2shvgd1v4ws9dls6zizbyfwaqam02y2hwsq1")))

(define-public crate-zero_ecs_build-0.2.0 (c (n "zero_ecs_build") (v "0.2.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.0") (d #t) (k 0)))) (h "03p4y7i8pm5g6cygmf4rb2i03bwcmykyr8lvfz68l8hqgkqimpzz")))

(define-public crate-zero_ecs_build-0.2.1 (c (n "zero_ecs_build") (v "0.2.1") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.1") (d #t) (k 0)))) (h "1xxp2w8far0k4y5lf1iyamj3bk5v995ikg9yl59rlih6m2fk46fj")))

(define-public crate-zero_ecs_build-0.2.2 (c (n "zero_ecs_build") (v "0.2.2") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.2") (d #t) (k 0)))) (h "1lmb4j57rr0pnwp941pznyinbkasp0r8g6m2jjafyiiyjmdmj9ra")))

(define-public crate-zero_ecs_build-0.2.3 (c (n "zero_ecs_build") (v "0.2.3") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.3") (d #t) (k 0)))) (h "0jjk767ys3q7fbqfd24rprx7ljczi5bdx4g9n7wjh9hn5265hdg1")))

(define-public crate-zero_ecs_build-0.2.4 (c (n "zero_ecs_build") (v "0.2.4") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.4") (d #t) (k 0)))) (h "0irv1ip8s7qd5gfqgyb9n6k4j44x47hhn1fbhxagrn7kivzvxwyf")))

(define-public crate-zero_ecs_build-0.2.5 (c (n "zero_ecs_build") (v "0.2.5") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.5") (d #t) (k 0)))) (h "06kl3fh2ap0dxl4jqr01sh5zbz7d2742dhrf7p5qm9v8a67vwn04")))

(define-public crate-zero_ecs_build-0.2.8 (c (n "zero_ecs_build") (v "0.2.8") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.8") (d #t) (k 0)))) (h "0dypjrciinpvrcicqqdlf8gb2c6lkq9mpfvqf4draw97c9mwr264")))

(define-public crate-zero_ecs_build-0.2.10 (c (n "zero_ecs_build") (v "0.2.10") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.10") (d #t) (k 0)))) (h "1j50gavvfdplqh18v1b2nz5m22b90x5m99frag6pj9fslhzl9yz7")))

(define-public crate-zero_ecs_build-0.2.11 (c (n "zero_ecs_build") (v "0.2.11") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.11") (d #t) (k 0)))) (h "13188b25cflb6mbzm937z7jx7q78cb8rng39a6lki0smc6157ysi")))

(define-public crate-zero_ecs_build-0.2.12 (c (n "zero_ecs_build") (v "0.2.12") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.12") (d #t) (k 0)))) (h "1qlx3c27d75h4xvsdjc2z41i02j0bbvgyny6jmn40lic90j36vx7")))

(define-public crate-zero_ecs_build-0.2.13 (c (n "zero_ecs_build") (v "0.2.13") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.13") (d #t) (k 0)))) (h "0f3q17rjszrgkrpqalik59dyb53hchdd8wv137i77xzpvqa8996c")))

(define-public crate-zero_ecs_build-0.2.14 (c (n "zero_ecs_build") (v "0.2.14") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.14") (d #t) (k 0)))) (h "03fmrrkdac8khv0yqn75975vf490a64m3xskg3dh5amf5nbljvm8")))

(define-public crate-zero_ecs_build-0.2.15 (c (n "zero_ecs_build") (v "0.2.15") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.15") (d #t) (k 0)))) (h "0s2w5lj0535h3zs71xjqi4nh1sn8sjlbw0dkyanjdy04yxwx42rb")))

(define-public crate-zero_ecs_build-0.2.17 (c (n "zero_ecs_build") (v "0.2.17") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.17") (d #t) (k 0)))) (h "0v8iyzj5brkvx78kqi1vi5ka7kh2k75icbvb8cgi7a5dhc1bn5ja")))

(define-public crate-zero_ecs_build-0.2.18 (c (n "zero_ecs_build") (v "0.2.18") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.18") (d #t) (k 0)))) (h "08pfkkvnjsilc6p602wvylqbfmm6mf6510br4435vm21lfsm1wkr")))

(define-public crate-zero_ecs_build-0.2.19 (c (n "zero_ecs_build") (v "0.2.19") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.19") (d #t) (k 0)))) (h "0vn9bkyzjsylhv2znnk68a7mfqal76vljlpn1ym74gm1l8nnzw8k")))

(define-public crate-zero_ecs_build-0.2.20 (c (n "zero_ecs_build") (v "0.2.20") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.20") (d #t) (k 0)))) (h "178mgrjyybpmx88iz3av4z2mfh7z6zcmd9fak6a3h4davjsd3qzz")))

(define-public crate-zero_ecs_build-0.2.21 (c (n "zero_ecs_build") (v "0.2.21") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.21") (d #t) (k 0)))) (h "0v8wfk8ann2xvjpi16xnr2bnpm8v29pkczsnr1ibprvrp041xxy4")))

(define-public crate-zero_ecs_build-0.2.22 (c (n "zero_ecs_build") (v "0.2.22") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.51") (f (quote ("full"))) (d #t) (k 0)) (d (n "zero_ecs_macros") (r "^0.2.22") (d #t) (k 0)))) (h "13y7ybrpdqf58ky8z4ia01za71h74m55b1jhxc45jnjspns1mdyy")))

