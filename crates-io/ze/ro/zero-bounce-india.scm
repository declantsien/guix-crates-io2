(define-module (crates-io ze ro zero-bounce-india) #:use-module (crates-io))

(define-public crate-zero-bounce-india-1.0.0 (c (n "zero-bounce-india") (v "1.0.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "mockito") (r "^1.0.2") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.16") (f (quote ("blocking" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "0jasbb16s7ajsjz0hzk3ms2ksrd7qc2g14prdk9xq2ngw1h1ilas")))

(define-public crate-zero-bounce-india-1.1.0 (c (n "zero-bounce-india") (v "1.1.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "mockito") (r "^1.0.2") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.16") (f (quote ("blocking" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "007gvnxj50vb56bh1fc85d08g3gfn04h72y27rrsj8mf3yrlq8j7")))

