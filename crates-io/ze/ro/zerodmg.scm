(define-module (crates-io ze ro zerodmg) #:use-module (crates-io))

(define-public crate-zerodmg-0.0.1 (c (n "zerodmg") (v "0.0.1") (h "1njmqmdh8xfqw3001haqzhid6j32ny9argjg8h5dakfx99mxgg2p") (y #t)))

(define-public crate-zerodmg-0.1.3 (c (n "zerodmg") (v "0.1.3") (d (list (d (n "futures") (r "0.1.*") (d #t) (k 0)) (d (n "hyper") (r "0.11.*") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 0)) (d (n "zerodmg-emulator") (r "^0.1.3") (d #t) (k 0)) (d (n "zerodmg-utils") (r "^0.1.3") (d #t) (k 0)))) (h "07mfrq4ppk8l00ms9hrcqgd18f0ppvnjj6xn85q792nrsarp1k3s") (y #t)))

(define-public crate-zerodmg-0.1.4 (c (n "zerodmg") (v "0.1.4") (d (list (d (n "futures") (r "0.1.*") (d #t) (k 0)) (d (n "hyper") (r "0.11.*") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 0)) (d (n "zerodmg-emulator") (r "^0.1.4") (d #t) (k 0)) (d (n "zerodmg-utils") (r "^0.1.4") (d #t) (k 0)))) (h "09d6jwaaj2f5z9siijbjrsv23sq005q0pcz91mv4pd2klzshb9rf") (y #t)))

(define-public crate-zerodmg-0.1.5 (c (n "zerodmg") (v "0.1.5") (d (list (d (n "futures") (r "0.1.*") (d #t) (k 0)) (d (n "hyper") (r "0.11.*") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 0)) (d (n "zerodmg-emulator") (r "^0.1.5") (d #t) (k 0)) (d (n "zerodmg-utils") (r "^0.1.5") (d #t) (k 0)))) (h "1l57z6ds5c2iygzvfw3zmbgippm5fqnwk12w7rylkb435y3pzpl0") (y #t)))

(define-public crate-zerodmg-0.1.6 (c (n "zerodmg") (v "0.1.6") (d (list (d (n "futures") (r "0.1.*") (d #t) (k 0)) (d (n "hyper") (r "0.11.*") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 0)) (d (n "zerodmg-emulator") (r "^0.1.6") (d #t) (k 0)) (d (n "zerodmg-utils") (r "^0.1.6") (d #t) (k 0)))) (h "0rflvzgbad78cga0md759zbdi21b99bxs4f98lrwam6j940yc0ja") (y #t)))

(define-public crate-zerodmg-0.1.8 (c (n "zerodmg") (v "0.1.8") (d (list (d (n "futures") (r "0.1.*") (d #t) (k 0)) (d (n "hyper") (r "0.11.*") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 0)) (d (n "zerodmg-codes") (r "^0.1.8") (d #t) (k 0)) (d (n "zerodmg-emulator") (r "^0.1.8") (d #t) (k 0)) (d (n "zerodmg-utils") (r "^0.1.8") (d #t) (k 0)))) (h "1icwdwk111k0kfak0ayjk1fdjsxv9n94xhb337bzypd75dg128y7") (y #t)))

(define-public crate-zerodmg-0.1.9 (c (n "zerodmg") (v "0.1.9") (d (list (d (n "futures") (r "0.1.*") (d #t) (k 0)) (d (n "hyper") (r "0.11.*") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 0)) (d (n "zerodmg-codes") (r "^0.1.9") (d #t) (k 0)) (d (n "zerodmg-emulator") (r "^0.1.9") (d #t) (k 0)) (d (n "zerodmg-utils") (r "^0.1.9") (d #t) (k 0)))) (h "0x1ggi0aw1jl5zhww2nyln1n560f6rnvj1qf4xqzhilsirkqjpfy") (y #t)))

(define-public crate-zerodmg-0.1.10-zerodmg (c (n "zerodmg") (v "0.1.10-zerodmg") (h "05srn3h0m7dad0jwrr6ss5lash948drrcv02drwp0jpql1fg5744") (y #t)))

(define-public crate-zerodmg-0.0.0-- (c (n "zerodmg") (v "0.0.0--") (h "08r0a23vzxn69b8pcbxvl42gqbvparg0bs5qlgfzbs131pxxwvx2") (y #t)))

