(define-module (crates-io ze ro zerofrom) #:use-module (crates-io))

(define-public crate-zerofrom-0.1.0 (c (n "zerofrom") (v "0.1.0") (d (list (d (n "zerofrom-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1164kwyy3id6h11sym7dv7fnb4wamjjqz8dcpn855j51gz67imdf") (f (quote (("derive" "zerofrom-derive") ("default" "alloc") ("alloc"))))))

(define-public crate-zerofrom-0.1.1 (c (n "zerofrom") (v "0.1.1") (d (list (d (n "zerofrom-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0m6y2kbzqq24ny8m611lrw556c361xxcx6davsvh8azprigkbsbr") (f (quote (("derive" "zerofrom-derive") ("default" "alloc") ("alloc"))))))

(define-public crate-zerofrom-0.1.2 (c (n "zerofrom") (v "0.1.2") (d (list (d (n "zerofrom-derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "18mylrn6zii17vcpzka94pxxlwn16vk23kpwbmhjgpji69ndfm6z") (f (quote (("default" "alloc") ("alloc")))) (s 2) (e (quote (("derive" "dep:zerofrom-derive"))))))

(define-public crate-zerofrom-0.1.3 (c (n "zerofrom") (v "0.1.3") (d (list (d (n "zerofrom-derive") (r "^0.1.1") (o #t) (k 0)))) (h "1dq5dmls0gdlbxgzvh56754k0wq7ch60flbq97g9mcf0qla0hnv5") (f (quote (("default" "alloc") ("alloc")))) (s 2) (e (quote (("derive" "dep:zerofrom-derive")))) (r "1.66")))

(define-public crate-zerofrom-0.1.4 (c (n "zerofrom") (v "0.1.4") (d (list (d (n "zerofrom-derive") (r "^0.1.3") (o #t) (k 0)))) (h "0mdbjd7vmbix2ynxbrbrrli47a5yrpfx05hi99wf1l4pwwf13v4i") (f (quote (("default" "alloc") ("alloc")))) (s 2) (e (quote (("derive" "dep:zerofrom-derive")))) (r "1.67")))

