(define-module (crates-io ze ro zeronet_cryptography) #:use-module (crates-io))

(define-public crate-zeronet_cryptography-0.1.8 (c (n "zeronet_cryptography") (v "0.1.8") (d (list (d (n "base64") (r "~0.12.0") (d #t) (k 0)) (d (n "basex-rs") (r "~0.1.0") (d #t) (k 0)) (d (n "bitcoin") (r "~0.25") (d #t) (k 0)) (d (n "ripemd160") (r "~0.9.0") (d #t) (k 0)) (d (n "secp256k1") (r "~0.19.0") (f (quote ("recovery" "rand" "std" "rand-std"))) (d #t) (k 0)) (d (n "sha2") (r "~0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "~1.0") (d #t) (k 0)))) (h "12k27nx4cgpv1crk9d2y4czmcwnhdrzafvlljffzzabiv6k77w4h")))

(define-public crate-zeronet_cryptography-0.1.9 (c (n "zeronet_cryptography") (v "0.1.9") (d (list (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "basex-rs") (r "~0.1.0") (d #t) (k 0)) (d (n "bitcoin") (r "~0.27") (d #t) (k 0)) (d (n "ripemd") (r "~0.1") (d #t) (k 0)) (d (n "secp256k1") (r "~0.20") (f (quote ("recovery" "rand" "std" "rand-std"))) (d #t) (k 0)) (d (n "sha2") (r "~0.10") (d #t) (k 0)) (d (n "thiserror") (r "~1.0") (d #t) (k 0)))) (h "0f13jcgrn5acwqqb0h8yrwgj34q04ssa8pzy1kna8d2cj2dk8x70")))

