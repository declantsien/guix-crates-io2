(define-module (crates-io ze ro zero-network) #:use-module (crates-io))

(define-public crate-zero-network-0.1.0 (c (n "zero-network") (v "0.1.0") (h "1xd0bm00q6jwni6fk46calzgkm8xbjbms1mkmjrxdw7lahy7fsps")))

(define-public crate-zero-network-0.1.10 (c (n "zero-network") (v "0.1.10") (d (list (d (n "zero-bls12-381") (r "^0.1.10") (k 0)) (d (n "zero-crypto") (r "^0.1.10") (k 0)) (d (n "zero-elgamal") (r "^0.1.10") (k 0)) (d (n "zero-jubjub") (r "^0.1.10") (k 0)) (d (n "zero-kzg") (r "^0.1.10") (k 0)) (d (n "zero-pairing") (r "^0.1.10") (k 0)) (d (n "zero-plonk") (r "^0.1.10") (k 0)) (d (n "codec") (r "^2.0.0") (f (quote ("derive"))) (k 2) (p "parity-scale-codec")) (d (n "frame-support") (r "^3.0.0") (k 2)) (d (n "frame-system") (r "^3.0.0") (k 2)) (d (n "rand") (r "^0.8") (k 2)) (d (n "rand_core") (r "^0.6") (k 2)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (k 2)) (d (n "sp-core") (r "^3.0.0") (k 2)) (d (n "sp-io") (r "^3.0.0") (k 2)) (d (n "sp-runtime") (r "^3.0.0") (k 2)))) (h "09j91cbp4glkb698l0rsjbdzv9b5jg8rl3f560rlsc8hk527g08g")))

