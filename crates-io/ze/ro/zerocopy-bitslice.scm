(define-module (crates-io ze ro zerocopy-bitslice) #:use-module (crates-io))

(define-public crate-zerocopy-bitslice-0.1.1 (c (n "zerocopy-bitslice") (v "0.1.1") (d (list (d (n "sha2") (r "^0.10.6") (o #t) (d #t) (k 0)) (d (n "shadow-nft-common") (r "^0.1.1") (d #t) (k 0)))) (h "0xyy19qprm2a59c76hanycgpx3ljhgizakfraqj5ijx6pmpw16ba") (f (quote (("std") ("default" "choose-random-zero" "std") ("choose-random-zero" "std" "sha2"))))))

