(define-module (crates-io ze ro zeromq-src) #:use-module (crates-io))

(define-public crate-zeromq-src-0.1.0 (c (n "zeromq-src") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "06knjx6699kb452ln1vfcrfln8rhz9v0b1q8aayx7dm4f5ma0fp5") (y #t)))

(define-public crate-zeromq-src-0.1.1 (c (n "zeromq-src") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "017ysgay4nvfn144ybyl441a1xml6q42shsmz0xl6r9r064mxsd5") (y #t)))

(define-public crate-zeromq-src-0.1.2 (c (n "zeromq-src") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "196bb9arcgmfzsz83258wa1xgfi76g2dj1laarivjjmp36z4rkvk") (y #t)))

(define-public crate-zeromq-src-0.1.3+4.2.1 (c (n "zeromq-src") (v "0.1.3+4.2.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "1vv6z7qsggb4g8dy6s2d04dhl0w533dcqrrpcp8zw0i9p5g3h5aj") (y #t)))

(define-public crate-zeromq-src-0.1.4+4.2.1 (c (n "zeromq-src") (v "0.1.4+4.2.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "0pw8ac9rxi7kjks1lbj5wbnixz7l47pk31v46a8a45kb4dn4p3bs") (y #t)))

(define-public crate-zeromq-src-0.1.4+4.3.1 (c (n "zeromq-src") (v "0.1.4+4.3.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "1jini50prlwsv0nl8qss3w9jy1rbv5646zzj4w897sgqmk5wv5n4") (y #t)))

(define-public crate-zeromq-src-0.1.4-preview+4.3.2 (c (n "zeromq-src") (v "0.1.4-preview+4.3.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "12v2v6qvwb6ckv788w2m34fmbrzhqzhxsrhdha8xvh4qmw929grv")))

(define-public crate-zeromq-src-0.1.5+4.3.1 (c (n "zeromq-src") (v "0.1.5+4.3.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "0izynm8rrgk5y730q9fcm8vz044k9dlfchvpyyxz0x79qxcaq2sb") (y #t)))

(define-public crate-zeromq-src-0.1.5-preview+4.3.2 (c (n "zeromq-src") (v "0.1.5-preview+4.3.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "0plj3n2ihxpjwyvh36c30lmw3l8dvxxj16j6nqahqgp130qi5mai")))

(define-public crate-zeromq-src-0.1.6+4.3.1 (c (n "zeromq-src") (v "0.1.6+4.3.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "1ih1ril1z42nrwp57nm55yf36i0dqrizakl6ydj3ykldsn2cjnwg") (y #t)))

(define-public crate-zeromq-src-0.1.6-preview+4.3.2 (c (n "zeromq-src") (v "0.1.6-preview+4.3.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "081493vij15xx3pwjr85az38mdp7s2afkfdlx8m23p9nilxhyci0") (y #t)))

(define-public crate-zeromq-src-0.1.6-preview.1+4.3.2 (c (n "zeromq-src") (v "0.1.6-preview.1+4.3.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "0jj1c0kkdwj56sni8mp34wd23157d982b1wnd2n17q3w8ag28dzw") (y #t)))

(define-public crate-zeromq-src-0.1.7+4.3.2 (c (n "zeromq-src") (v "0.1.7+4.3.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "03a3hasw6ii5vyqgy2fywcm95k1js2c9pihpc7amwnqn8xvcah90")))

(define-public crate-zeromq-src-0.1.8+4.3.2 (c (n "zeromq-src") (v "0.1.8+4.3.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "01f8mpbzx8h4bf49n2g3zdlsmznlimq9q7nr8vin7jigg24ra7ja")))

(define-public crate-zeromq-src-0.1.9+4.3.2 (c (n "zeromq-src") (v "0.1.9+4.3.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "0akj6c8jzszzvjfixnq64f253nqb60w0nwa6r7sl9pa86ai6m4qx")))

(define-public crate-zeromq-src-0.1.10+4.3.2 (c (n "zeromq-src") (v "0.1.10+4.3.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 0)))) (h "1wd2xwc6l7ixwxy3vdjxxhif2yhqp91n0dg44bzcyzw1cv9k74fz")))

(define-public crate-zeromq-src-0.2.0+4.3.4 (c (n "zeromq-src") (v "0.2.0+4.3.4") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "dircpy") (r "^0.3.8") (d #t) (k 0)))) (h "1vcg65jsrbkwdnd1y6fkvnv3r9gspkvpbskm8b8i7r7xr67m0qhd")))

(define-public crate-zeromq-src-0.2.1+4.3.4 (c (n "zeromq-src") (v "0.2.1+4.3.4") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "dircpy") (r "^0.3.8") (d #t) (k 0)))) (h "10xdgkb098993vfsiv1hnfwjvv19knk6plb9lqhq1nxl55lhiq4c")))

(define-public crate-zeromq-src-0.2.2+4.3.4 (c (n "zeromq-src") (v "0.2.2+4.3.4") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "dircpy") (r "^0.3.8") (d #t) (k 0)))) (h "16vqcpdvwngk87n86f54yky4sggcvl9aw9jynxaphjgvfki3avd9")))

(define-public crate-zeromq-src-0.2.3+4.3.4 (c (n "zeromq-src") (v "0.2.3+4.3.4") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "dircpy") (r "^0.3.8") (d #t) (k 0)))) (h "0r08fc36z5mrx7038a9i8ym0nvqn3nm78s6yn6ifn2alsfvbh46y") (y #t)))

(define-public crate-zeromq-src-0.2.4+4.3.4 (c (n "zeromq-src") (v "0.2.4+4.3.4") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "dircpy") (r "^0.3.8") (d #t) (k 0)))) (h "1zj9b2ygh1iriynh8zqp5rp75yapw0ns73s0vx2z5dvxcpylw9w8")))

(define-public crate-zeromq-src-0.2.5+4.3.4 (c (n "zeromq-src") (v "0.2.5+4.3.4") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "dircpy") (r "^0.3.8") (d #t) (k 0)))) (h "0r1m3by3m3v3x2malngqzmkb887p1drcrnx9qmyh8gbmkw8siajk")))

(define-public crate-zeromq-src-0.2.6+4.3.4 (c (n "zeromq-src") (v "0.2.6+4.3.4") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "dircpy") (r "^0.3.8") (d #t) (k 0)))) (h "02qnyiq40l5kbhk06dz83ap468jz03qvmd6zs1g5sdkh29vhn4pw")))

(define-public crate-zeromq-src-0.3.0+4.3.5 (c (n "zeromq-src") (v "0.3.0+4.3.5") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "dircpy") (r "^0.3.8") (d #t) (k 0)))) (h "19anghn8f49k0rfkk27p4bma44njyqgaifl5xv5670ckklh5vpx7")))

(define-public crate-zeromq-src-0.3.1+4.3.5 (c (n "zeromq-src") (v "0.3.1+4.3.5") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "dircpy") (r "^0.3.8") (d #t) (k 0)))) (h "12a2vils9hgb74m4wh9k0gbvdgj8nvw88r1sa9z04lhxr4ksyh56")))

(define-public crate-zeromq-src-0.3.2+4.3.5 (c (n "zeromq-src") (v "0.3.2+4.3.5") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 0)) (d (n "dircpy") (r "^0.3.8") (d #t) (k 0)))) (h "038gbs864hwzf1j0cgpbd6imv15hddynrh9mpr59hh2gd2w57xwq")))

