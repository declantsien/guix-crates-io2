(define-module (crates-io ze ll zellij-tile) #:use-module (crates-io))

(define-public crate-zellij-tile-0.5.0 (c (n "zellij-tile") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "119icg80yxhgg5x55jgpszjlmciq1ym1j4phzcfjw9j9q66qz4nj")))

(define-public crate-zellij-tile-0.6.0 (c (n "zellij-tile") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "1938h1yxc1f1kn000i61m97lxp46scjj26gvhs79d4xjj73la335")))

(define-public crate-zellij-tile-1.0.0 (c (n "zellij-tile") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "12v2aidhz9v3mvfjmag84x54zglgvfg7li3nlyn0fyclcppm1dqc") (y #t)))

(define-public crate-zellij-tile-1.1.0 (c (n "zellij-tile") (v "1.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "161d7r6rywvj06xfmwdw8wqhbv1i6cnbmh7aky5hr924iipja45z") (y #t)))

(define-public crate-zellij-tile-1.2.0 (c (n "zellij-tile") (v "1.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "0z3mimxfdg2nshcxsc9qlfb432lfwb1zhwg8hb8fm5x4rr8ybi7n") (y #t)))

(define-public crate-zellij-tile-0.7.0 (c (n "zellij-tile") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "05m3w0hvpf1ijd79cng141hdaly6hbaj87nwhzarsix7xyqj5gkp")))

(define-public crate-zellij-tile-0.8.0 (c (n "zellij-tile") (v "0.8.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "0bzdixvvb3k51037hblj4bsxpw0mm6xqk4lcx4sslshnffb4l5xz")))

(define-public crate-zellij-tile-0.11.0 (c (n "zellij-tile") (v "0.11.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "19p0rb4a5bblfcxq6lf8p3n2hg7vwcf81dj2hs4jqm5hik3kbjgq")))

(define-public crate-zellij-tile-0.12.0 (c (n "zellij-tile") (v "0.12.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "0glj98w2fr9haj1b02zf4y63gnbgaphx2rjp53pxp2ipr87mwf44")))

(define-public crate-zellij-tile-0.12.1 (c (n "zellij-tile") (v "0.12.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "0svfx27z9jn80q2vd2fkjrj3v1nw110c8ckljbkgy3l9g311gfzp")))

(define-public crate-zellij-tile-0.13.0 (c (n "zellij-tile") (v "0.13.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "08j0p6gy4ghgn89pnsiq0ifzdg1kyp94ksr1hdzsnp6vjprsiy6y")))

(define-public crate-zellij-tile-0.14.0 (c (n "zellij-tile") (v "0.14.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "1arambkrfwgl42ryss7p6d7ld2lffr3f96p075jm9n3qhnqww3s6")))

(define-public crate-zellij-tile-0.15.0 (c (n "zellij-tile") (v "0.15.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "11nar5s8d68bhki68jnb0lza6fwl68p4zw57n9fyqsjljfcfg7sk")))

(define-public crate-zellij-tile-0.16.0 (c (n "zellij-tile") (v "0.16.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "094dl76a8q9xd13wszksw1cq4b08dba9xqnkpzl8xfahk68a1i8k")))

(define-public crate-zellij-tile-0.17.0 (c (n "zellij-tile") (v "0.17.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "1a0cv1gbgz3hqdpm9pfshzi6akaank50b2i2d1675byvqbp14v57")))

(define-public crate-zellij-tile-0.18.0 (c (n "zellij-tile") (v "0.18.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "00gcxjrccv5h64iclwdyn7cs68la87qh5ssjyh1wz9wrz63irwk6")))

(define-public crate-zellij-tile-0.18.1 (c (n "zellij-tile") (v "0.18.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "04skrp3krigs34nzmigpcil865j63wxj5qhl8724vx824qar7g57")))

(define-public crate-zellij-tile-0.19.0 (c (n "zellij-tile") (v "0.19.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "000r44kdqki7z727mv5ld9yigi90ghhipw1cnfd8s10mrmszcqr0")))

(define-public crate-zellij-tile-0.20.0 (c (n "zellij-tile") (v "0.20.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "0jsianhkljwqm40zypm3vjb8rlc9yx84qdf0aqgkhq8vjwf5y9hh")))

(define-public crate-zellij-tile-0.20.1 (c (n "zellij-tile") (v "0.20.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "080wnm3xm8kyibf8r3vnfy23lsr1ks4rw43d4755dwhvp0vlyakp")))

(define-public crate-zellij-tile-0.21.0 (c (n "zellij-tile") (v "0.21.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "19l2grwcigl0j61xgkaqh0yglxf5sqwbya5sfyvjqnz6iipsz4y9")))

(define-public crate-zellij-tile-0.22.0 (c (n "zellij-tile") (v "0.22.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "1l1bslsh10r91ywihc0hhf69g05jl02wwnm1ywc9zz0cc676zabm")))

(define-public crate-zellij-tile-0.22.1 (c (n "zellij-tile") (v "0.22.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "1p8vyq488v4930l380gblbij717yl1pk65q65ig1dfrnz7x5vrqp")))

(define-public crate-zellij-tile-0.23.0 (c (n "zellij-tile") (v "0.23.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "021k766bcmyjaa6mzijzzsi16i2pgz5qyycbgva7j4kp1rllsc6p")))

(define-public crate-zellij-tile-0.24.0 (c (n "zellij-tile") (v "0.24.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "09kw9bgh3mnymgfml6p219fb3pg8r03xm985g31bqcnzss4j05b2")))

(define-public crate-zellij-tile-0.25.0 (c (n "zellij-tile") (v "0.25.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "0ya3fb1zmk41qa17zafy484csgvk62zwagv5w5krbj3brvbp80c5")))

(define-public crate-zellij-tile-0.26.0 (c (n "zellij-tile") (v "0.26.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "02nff77vci9rxkrkkmylilswgc4kqb158djcfa56pzqwb6qajccq")))

(define-public crate-zellij-tile-0.26.1 (c (n "zellij-tile") (v "0.26.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "1sqwqnrk3iy44b5ihzri6ymj324rw0rswizrg7l7g4f4y4lqj44n")))

(define-public crate-zellij-tile-0.27.0 (c (n "zellij-tile") (v "0.27.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "1iah1kdyawja6g826lwx6mb81aym8lv5g4gidrx5p3035yad8ggn")))

(define-public crate-zellij-tile-0.28.0 (c (n "zellij-tile") (v "0.28.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "1aifcblalpsvs81d67l7abqrlagl8m6xayh2bdy0kx078m2a9a7z")))

(define-public crate-zellij-tile-0.28.1 (c (n "zellij-tile") (v "0.28.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "0hj08mwckrzv9ykxzl1nx3vjmw6321mxafa52x5b0d2nfpjgwkd8")))

(define-public crate-zellij-tile-0.29.0 (c (n "zellij-tile") (v "0.29.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "02zfm3da4id856libxmffj2gswxmlahkrcadqv9fj0x8m2n2f8dg")))

(define-public crate-zellij-tile-0.29.1 (c (n "zellij-tile") (v "0.29.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "0wbpi5xqwkfzzxg27a8c1iqb616nrfnk1hix69wx9asasv7ngrql")))

(define-public crate-zellij-tile-0.30.0 (c (n "zellij-tile") (v "0.30.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)))) (h "1n07z102x4q3n7jlaa3kr8ymj76z7c4b4zh1ckjlkk9n9mhgjji0")))

(define-public crate-zellij-tile-0.31.4 (c (n "zellij-tile") (v "0.31.4") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)) (d (n "zellij-utils") (r "^0.31.4") (d #t) (k 0)))) (h "0jgs7anxdhwhz0zaz3if6dz6lsza9bx1r0iwwj02x7v3w6015ql2")))

(define-public crate-zellij-tile-0.32.0 (c (n "zellij-tile") (v "0.32.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)) (d (n "zellij-utils") (r "^0.32.0") (d #t) (k 0)))) (h "1lzfn9kf985h6156fxsyi41dyly9s989z8ksz5zd10xcy86xlizm")))

(define-public crate-zellij-tile-0.34.0 (c (n "zellij-tile") (v "0.34.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)) (d (n "zellij-utils") (r "^0.34.0") (d #t) (k 0)))) (h "0pzvkndndakfy3fh110v1rscj3gammwaq9a0ld6hv19f21rgzrf3")))

(define-public crate-zellij-tile-0.34.3 (c (n "zellij-tile") (v "0.34.3") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)) (d (n "zellij-utils") (r "^0.34.3") (d #t) (k 0)))) (h "0byiwf96hyrwfxph3h29cmsjyvpw814j6m7x0z93rsaxi4cfmdg2")))

(define-public crate-zellij-tile-0.34.4 (c (n "zellij-tile") (v "0.34.4") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)) (d (n "zellij-utils") (r "^0.34.4") (d #t) (k 0)))) (h "0h1z7mfcc3jqy3id9s0sh95z4fa849gqiz8qbrm76s1p34akbipz")))

(define-public crate-zellij-tile-0.35.0 (c (n "zellij-tile") (v "0.35.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)) (d (n "zellij-utils") (r "^0.35.0") (d #t) (k 0)))) (h "0aspqvphca3n72bb957ngb2hkzjic7xa455d824vwpcq1b0c5av3")))

(define-public crate-zellij-tile-0.35.1 (c (n "zellij-tile") (v "0.35.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)) (d (n "zellij-utils") (r "^0.35.1") (d #t) (k 0)))) (h "1xk6vj8nr85fjqlg4mras6fihkbb8v0l901hcqbmmlwqxn1wabvf")))

(define-public crate-zellij-tile-0.35.2 (c (n "zellij-tile") (v "0.35.2") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)) (d (n "zellij-utils") (r "^0.35.2") (d #t) (k 0)))) (h "0nsjpsnbjdbb4h4218x5zn6li2s057djczh8q1mayhbp14dh9rwp")))

(define-public crate-zellij-tile-0.36.0 (c (n "zellij-tile") (v "0.36.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)) (d (n "zellij-utils") (r "^0.36.0") (d #t) (k 0)))) (h "0npv6g497wngami8w4s54x2vxx9qx84lq8k9qgzisqbh6jvq2aqf")))

(define-public crate-zellij-tile-0.37.0 (c (n "zellij-tile") (v "0.37.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)) (d (n "zellij-utils") (r "^0.37.0") (d #t) (k 0)))) (h "00yajdbv4wljg3sy46i7b2vnvgzkr7hi7f8v257vjg7vn159fabj")))

(define-public crate-zellij-tile-0.37.1 (c (n "zellij-tile") (v "0.37.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)) (d (n "zellij-utils") (r "^0.37.1") (d #t) (k 0)))) (h "17iy3ll4cc4bspz5xsl79iags1pw8cifi3sbq7190rk1zrhy1dgm")))

(define-public crate-zellij-tile-0.37.2 (c (n "zellij-tile") (v "0.37.2") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)) (d (n "zellij-utils") (r "^0.37.2") (d #t) (k 0)))) (h "1l205b7067p7j7h29anrp31av3n6cmbh1zdwzcfaiwl30vhr6z4b")))

(define-public crate-zellij-tile-0.38.0 (c (n "zellij-tile") (v "0.38.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)) (d (n "zellij-utils") (r "^0.38.0") (d #t) (k 0)))) (h "18dwd38z753m74vhy3yma19jm9rj9h09jab79g19svsd5pcahzh7")))

(define-public crate-zellij-tile-0.38.1 (c (n "zellij-tile") (v "0.38.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)) (d (n "zellij-utils") (r "^0.38.1") (d #t) (k 0)))) (h "0px1ji3myprrz0m4qjc233x7c9ckc2r64bdq5szs5s18m001gdb8")))

(define-public crate-zellij-tile-0.38.2 (c (n "zellij-tile") (v "0.38.2") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)) (d (n "zellij-utils") (r "^0.38.2") (d #t) (k 0)))) (h "18qviif45biknvjgwbc1s32yl5abrmznjfx78ip1rpd6yyz5n913")))

(define-public crate-zellij-tile-0.39.0 (c (n "zellij-tile") (v "0.39.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)) (d (n "zellij-utils") (r "^0.39.0") (d #t) (k 0)))) (h "1i2hn6z2v1zvcbs0590ji250flda7l0fa9zcxkmh3bmfninfmkza")))

(define-public crate-zellij-tile-0.39.1 (c (n "zellij-tile") (v "0.39.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)) (d (n "zellij-utils") (r "^0.39.1") (d #t) (k 0)))) (h "1lilmnsalbgyvjw6gr515c4pcgq8nks2z2qf9h0cz2svzzrp01i3")))

(define-public crate-zellij-tile-0.39.2 (c (n "zellij-tile") (v "0.39.2") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)) (d (n "zellij-utils") (r "^0.39.2") (d #t) (k 0)))) (h "1k4hd9n3ba0262gf0bx6y3jl6ksrw6gzjh172rg17lr54ixy1rdy")))

(define-public crate-zellij-tile-0.40.0 (c (n "zellij-tile") (v "0.40.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)) (d (n "zellij-utils") (r "^0.40.0") (d #t) (k 0)))) (h "1gajhnfxj3dnzfsvss15vf3vv3hxkcp0f360g8wf8w97nsc12isq")))

(define-public crate-zellij-tile-0.40.1 (c (n "zellij-tile") (v "0.40.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.0") (d #t) (k 0)) (d (n "zellij-utils") (r "^0.40.1") (d #t) (k 0)))) (h "0yrp84a9zh35blb6zhkclxwqzvij9va8xfpdnd1sh4kz4xcp9ka3")))

