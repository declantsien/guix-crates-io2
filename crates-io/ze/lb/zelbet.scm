(define-module (crates-io ze lb zelbet) #:use-module (crates-io))

(define-public crate-zelbet-0.0.0 (c (n "zelbet") (v "0.0.0") (h "0car9i8qplnyv47zrbqs6l19jrnvm492laap20nmss05s4m1rwpk") (y #t)))

(define-public crate-zelbet-0.0.0-next (c (n "zelbet") (v "0.0.0-next") (h "0wb0nqcmsz86rdc5gbiyq37hi7j8c2p91imkq57k1r4cvxarp7l8")))

