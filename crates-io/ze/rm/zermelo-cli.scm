(define-module (crates-io ze rm zermelo-cli) #:use-module (crates-io))

(define-public crate-zermelo-cli-0.1.0 (c (n "zermelo-cli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y8yjyfhhil8jfpg4lni0pmpybyh79dxyj6khvhlyqvy0vxl8ni8")))

(define-public crate-zermelo-cli-0.1.1 (c (n "zermelo-cli") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "15w504vcw686gj7fsrzzwld2lvbhqhz116cmi824aqvvqlz4pj48")))

(define-public crate-zermelo-cli-0.2.0 (c (n "zermelo-cli") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qyzfaldhw97qxzflg95mkxwxp250ljcv8synjgpwdgnvv8x30f7")))

(define-public crate-zermelo-cli-0.2.3 (c (n "zermelo-cli") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dw34jzv05f853dcqw1qs0zdy350xmlghsq1dn1cam6xyp4lpy36")))

