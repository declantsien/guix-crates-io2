(define-module (crates-io ze rm zermelo) #:use-module (crates-io))

(define-public crate-zermelo-0.1.0 (c (n "zermelo") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "00p38pxcl9613349k8hp929fbdzva7j7sp6xihmvikfc0ln8qrb7")))

(define-public crate-zermelo-0.1.1 (c (n "zermelo") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "11c8dn8s7gmwda89xb6sgv41npjy4zsp61w324hcb9nnzb2sygf9")))

(define-public crate-zermelo-0.1.2 (c (n "zermelo") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "1i6y0cpf8v43s455b53667jdw6f1dvw9q6rgig6hn2j3p72nnqv1")))

(define-public crate-zermelo-0.1.3 (c (n "zermelo") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "0m38v3zl39630ssryfanb8nr5vg55184mcripi3blc8b817ykpj7")))

(define-public crate-zermelo-0.1.4 (c (n "zermelo") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "0090k6gw8bzdzsk868cd7994nj18169zay8lnyvggcf21xpylpfk")))

(define-public crate-zermelo-0.1.5 (c (n "zermelo") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)))) (h "1gjz1lnic4z2kdm4vlwnrah5nq40kl2yib7isb2p63qzp94wxdrh")))

(define-public crate-zermelo-0.1.6 (c (n "zermelo") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)))) (h "11ywyr9n5wr0m4qikndvzd8sdvxccza1qc7prxlzm6z81xl3nj9l")))

(define-public crate-zermelo-0.1.7 (c (n "zermelo") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)))) (h "0zgxswr6ir56l0khw493fs3x3wpambnx7v1sviv10mdyhg6pwn7j") (y #t)))

(define-public crate-zermelo-0.1.8 (c (n "zermelo") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)))) (h "1vap5ff07bh16gy97qgc9x0v8clqgila2zrhprg0ibr6hf7asimh")))

(define-public crate-zermelo-0.2.0 (c (n "zermelo") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)))) (h "1alrdc1fjshhkvsbnan84aamd11qzx78xlmsnfh4z0x623jav7j1")))

(define-public crate-zermelo-0.2.1 (c (n "zermelo") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)))) (h "12zdyz1i5hmw6ajsbssnwxyi0202kwl6bkg1ya25b75midjjic84")))

(define-public crate-zermelo-0.3.0 (c (n "zermelo") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)))) (h "159xg03fqrx51l4s0ax32hxq4cfyz95mmdz8d3p6kr9x1fil4bcq")))

(define-public crate-zermelo-0.3.1 (c (n "zermelo") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)))) (h "0jc30nh8qh126v77c85d57bkp8bgk2ngmw49n235nyg44fqlvm5v")))

(define-public crate-zermelo-0.4.0 (c (n "zermelo") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)))) (h "1hp0lxrvrc8jjdsdv1bafazvdc4dncpzh09mw6i32146sgbhczfr")))

(define-public crate-zermelo-0.4.1 (c (n "zermelo") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)))) (h "1rcrbmmy01zi8ds4nkv2zm11zlknc0r2b8sdwrjlbcf1gckm085i")))

