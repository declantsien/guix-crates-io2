(define-module (crates-io ze te zetelverdeling) #:use-module (crates-io))

(define-public crate-zetelverdeling-0.1.0 (c (n "zetelverdeling") (v "0.1.0") (h "0c09minq687f6rxliw76bfsxgsc8q3zgn0riskvafdgra3z7qmzg") (y #t)))

(define-public crate-zetelverdeling-0.2.0 (c (n "zetelverdeling") (v "0.2.0") (h "130cxjvmh3fjlf7l7lizxz9g4xall4qfydjfgjvclzvmxls9x2b3")))

(define-public crate-zetelverdeling-0.2.1 (c (n "zetelverdeling") (v "0.2.1") (h "1sbl645da4kfpyhi3m877dlq88vr0mpg7v3phjgr2kd1a08743yz")))

(define-public crate-zetelverdeling-0.2.2 (c (n "zetelverdeling") (v "0.2.2") (h "16prfv1nrksjz311hjr44kwl0q4vns2ljjplq8vq36b81l9q8fkp")))

