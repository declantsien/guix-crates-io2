(define-module (crates-io ze n- zen-tmpl) #:use-module (crates-io))

(define-public crate-zen-tmpl-0.20.1 (c (n "zen-tmpl") (v "0.20.1") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "zen-expression") (r "^0.20.1") (d #t) (k 0)))) (h "03ksrcrwwdk9r76wxa30qmaq9b1hjzxciykrm5cxsq4ygam5k51y")))

(define-public crate-zen-tmpl-0.21.0 (c (n "zen-tmpl") (v "0.21.0") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "zen-expression") (r "^0.21.0") (d #t) (k 0)))) (h "02rxyd5ik7rmay76kn6a27cyr6mbml6z6n244ry1qrj2shdyadmp")))

(define-public crate-zen-tmpl-0.22.0 (c (n "zen-tmpl") (v "0.22.0") (d (list (d (n "itertools") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "zen-expression") (r "^0.22.0") (d #t) (k 0)))) (h "0135ypmlmyjp91a81va54s7lpldx8z5ilnzhzwv7c3pkfy7pkxr0")))

