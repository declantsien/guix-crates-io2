(define-module (crates-io ze n- zen-colour) #:use-module (crates-io))

(define-public crate-zen-colour-1.0.0 (c (n "zen-colour") (v "1.0.0") (h "0livkb7di9i8fzjw6w4gh2744cxbdrch3q9n5hrvmyriaakv8dlh")))

(define-public crate-zen-colour-1.0.1 (c (n "zen-colour") (v "1.0.1") (h "0rb367bnnqr3midxpwlzr5aacmkm38f8pcdakkpjpv2xvzlcbnrj")))

(define-public crate-zen-colour-1.1.0 (c (n "zen-colour") (v "1.1.0") (h "051sdk92sjnlrnkclwkzng1nnkh7zd7jx7lyiv6296lv9p2n1mkr")))

(define-public crate-zen-colour-1.1.1 (c (n "zen-colour") (v "1.1.1") (h "1cny27svchj4lkj9hwbws4d8gbbarbkaj7ycp9hvcv4g4w0mr8wm")))

(define-public crate-zen-colour-1.1.2 (c (n "zen-colour") (v "1.1.2") (h "10j94ifk80gx8q6h9fi7xgb2xwbbrxmiq9rv2jl9a54p2l5f42z2")))

