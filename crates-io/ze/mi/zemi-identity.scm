(define-module (crates-io ze mi zemi-identity) #:use-module (crates-io))

(define-public crate-zemi-identity-0.1.0 (c (n "zemi-identity") (v "0.1.0") (d (list (d (n "base64-url") (r "^1.4.13") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "rust-argon2") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0f7yyggrvwl33m8m0iyvnd1vxzabqzys1rj7slkbv5fnh4aqjvym")))

(define-public crate-zemi-identity-0.1.1 (c (n "zemi-identity") (v "0.1.1") (d (list (d (n "base64-url") (r "^1.4.13") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (d #t) (k 0)) (d (n "rust-argon2") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "109hg8hzk6pw1rph0wkc2vanp2hw601vva9fjjn2akjb0zb0q58h")))

