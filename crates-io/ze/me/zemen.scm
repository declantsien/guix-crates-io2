(define-module (crates-io ze me zemen) #:use-module (crates-io))

(define-public crate-zemen-0.1.0 (c (n "zemen") (v "0.1.0") (d (list (d (n "time") (r "^0.3.17") (d #t) (k 0)))) (h "08ivc15cnq71ibdjin7s8xxlqvjym7ffg8nkgsdy6rsciirkzkpr") (y #t) (r "1.66.0")))

(define-public crate-zemen-0.1.1 (c (n "zemen") (v "0.1.1") (d (list (d (n "time") (r "^0.3.17") (d #t) (k 0)))) (h "0rfvbsynm5f0bvf7ha173ws7l9knk840m575zlh5v6aazwzk4lw6") (r "1.66.0")))

(define-public crate-zemen-0.1.2 (c (n "zemen") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "time") (r "^0.3.17") (d #t) (k 0)))) (h "08zh6bqr1b5gqn3ckaqc2p69iczjb50f5djnkbq5l086p1j65zvh") (r "1.66.0")))

(define-public crate-zemen-0.1.3 (c (n "zemen") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "time") (r "^0.3.17") (d #t) (k 0)))) (h "0six33q518kzwj31gn4bx02r3kpbzfd344b7h48xw9s1kh4ng98j") (r "1.66.0")))

(define-public crate-zemen-0.1.4 (c (n "zemen") (v "0.1.4") (d (list (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (d #t) (k 0)))) (h "1bn28m8nw5jp4yamhqs05lrx0gnx70pfyzijb1f8z4jdblgdb300") (r "1.75.0")))

