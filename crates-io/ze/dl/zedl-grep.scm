(define-module (crates-io ze dl zedl-grep) #:use-module (crates-io))

(define-public crate-zedl-grep-0.1.0 (c (n "zedl-grep") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "rust_search") (r "^2.0.0") (d #t) (k 0)))) (h "0r37b6wjj1miayk5q8d4w4cwbs679bwsakf20f4i1x6gy1lz2glf")))

(define-public crate-zedl-grep-0.1.1 (c (n "zedl-grep") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "rust_search") (r "^2.0.0") (d #t) (k 0)))) (h "1caw4v055r190dfa5baj9ksyh9cjna37z8wkk2wgmiw668g90klh")))

(define-public crate-zedl-grep-0.1.2 (c (n "zedl-grep") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "rust_search") (r "^2.0.0") (d #t) (k 0)))) (h "0gnkppd2pd6khjhvnfbl8rrm6y7f6afvaalj4fic16zigga11ypw")))

