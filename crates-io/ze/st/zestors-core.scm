(define-module (crates-io ze st zestors-core) #:use-module (crates-io))

(define-public crate-zestors-core-0.0.1 (c (n "zestors-core") (v "0.0.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tiny-actor") (r "^0.2") (f (quote ("internals"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pm3bmvnmgjn6g30ch23z3d62r49ciq6wrj6m7s71q8f6jmn0s5p")))

