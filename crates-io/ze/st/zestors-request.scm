(define-module (crates-io ze st zestors-request) #:use-module (crates-io))

(define-public crate-zestors-request-0.0.1 (c (n "zestors-request") (v "0.0.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "zestors-core") (r "^0.0.1") (d #t) (k 0)))) (h "1273smnpxfaik452zsd0hj22ax1gbin0w55g4y9bg046y91fbpjd")))

