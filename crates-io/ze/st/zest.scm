(define-module (crates-io ze st zest) #:use-module (crates-io))

(define-public crate-zest-0.0.1 (c (n "zest") (v "0.0.1") (h "0x7bf02gyk43jljqjx4aa69lbywlqrzf16mdkp5p6dxx0s9mwcjg")))

(define-public crate-zest-0.0.2 (c (n "zest") (v "0.0.2") (h "06wlx8f5wdc180arnx7x2x6wsrky5saa2lay93w3xlfsns65q7kh")))

