(define-module (crates-io ze st zestors-codegen) #:use-module (crates-io))

(define-public crate-zestors-codegen-0.0.1 (c (n "zestors-codegen") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1mv07zvvcgilddykq6bq0gzwrdj7w8q2lv4380cawzxcpn3fh21k")))

(define-public crate-zestors-codegen-0.0.2 (c (n "zestors-codegen") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pjhvp06ppx8w1b10wivb00mdz96l9gzld8lx03b60arfvn8xr3c")))

(define-public crate-zestors-codegen-0.0.3 (c (n "zestors-codegen") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "002i8qa0aa7fxi5idbssz5gdqp2jr67mj2lzzr2gc3ld2y7dvriv")))

(define-public crate-zestors-codegen-0.1.0 (c (n "zestors-codegen") (v "0.1.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)))) (h "0wfmf2w2w5wm507wdjywrf8abgrx9gdj4ygpzfzhfnjnc6cxks75")))

(define-public crate-zestors-codegen-0.1.1 (c (n "zestors-codegen") (v "0.1.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16fbgjmjhq84yhy9wz504pcymy08wl0h1w4p04fhp0q56daxblb8")))

