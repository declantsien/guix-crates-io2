(define-module (crates-io ze st zest4096) #:use-module (crates-io))

(define-public crate-zest4096-0.1.0 (c (n "zest4096") (v "0.1.0") (h "0dqpr25zqzrrzmfbksc6qvn26z1j3a0hgyq0rr9cys11x5pw0c5r")))

(define-public crate-zest4096-0.1.1 (c (n "zest4096") (v "0.1.1") (h "1pzxkmpc1vb0xvd3b73chhpdjpb3b2cq7vcsflgk7bgxk73gga1r")))

(define-public crate-zest4096-0.1.2 (c (n "zest4096") (v "0.1.2") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0gb53vv8nbamnhgn9s03hfiy521v6vhh93n9df34qjxx7v2pw8m5")))

(define-public crate-zest4096-0.1.3 (c (n "zest4096") (v "0.1.3") (d (list (d (n "num") (r "^0.4") (k 0)))) (h "16ppd3zxrnak33rvbip6gv3szbk485d0j58x3l88r50qpfgrhdbr")))

(define-public crate-zest4096-0.1.4 (c (n "zest4096") (v "0.1.4") (d (list (d (n "num") (r "^0.4") (k 0)))) (h "1nlz46a2hywg408z649fawjmlwrz1m0dp8ypjplm96v6c5dbjygv")))

(define-public crate-zest4096-0.1.5 (c (n "zest4096") (v "0.1.5") (d (list (d (n "num") (r "^0.4") (k 0)))) (h "0jzj60sz3yryz5by4cl5jkr353vdpq8fqpqiyrlpy7d65l8y0fqn") (y #t)))

(define-public crate-zest4096-0.1.6 (c (n "zest4096") (v "0.1.6") (d (list (d (n "num") (r "^0.4") (k 0)))) (h "1dlcra2mbdrla311bzxcplllgnf0wdr7rmls96qiz9qgvqmldyfk") (y #t)))

(define-public crate-zest4096-0.1.7 (c (n "zest4096") (v "0.1.7") (d (list (d (n "num") (r "^0.4") (k 0)))) (h "1bdiakbaamwnzv8809gflmi5g9lbfhp9pn48k7c3hs10c6h898jw") (y #t)))

(define-public crate-zest4096-0.1.8 (c (n "zest4096") (v "0.1.8") (d (list (d (n "num") (r "^0.4") (k 0)))) (h "1d85lshxkrr40ldzp44213rzcrhbdg5cck98aww1pfsw0s0zana0")))

(define-public crate-zest4096-0.1.9 (c (n "zest4096") (v "0.1.9") (d (list (d (n "num") (r "^0.4") (k 0)))) (h "02w0ybwwzrn4096bbwyr4a3jfcj0kw20mr4myq9ih9jzx4iy6p6j")))

(define-public crate-zest4096-0.1.10 (c (n "zest4096") (v "0.1.10") (d (list (d (n "num") (r "^0.4") (k 0)))) (h "1qw9bic12g1d7ivmvpyjq8s1iw3xbjz5ckb5hhjix7fhqaqzyg31")))

(define-public crate-zest4096-0.1.11 (c (n "zest4096") (v "0.1.11") (d (list (d (n "num") (r "^0.4.0") (k 0)))) (h "157mw02p6vafah0005rja1bz84wwy5csdhs5431x03r0y3q06nyp")))

(define-public crate-zest4096-0.1.12 (c (n "zest4096") (v "0.1.12") (h "0dlz5qvw5m8wpp5gfdnr5d2n9aw4ydbwkbzlhid0nbyx4xkaqgcf")))

(define-public crate-zest4096-0.1.13 (c (n "zest4096") (v "0.1.13") (h "0xlj1rcqhd7ymy110md88ssl9q2w2m4lbbsfj9jw8ww4d0av32c9")))

(define-public crate-zest4096-0.1.14 (c (n "zest4096") (v "0.1.14") (h "1myskyn56pc5z77gd413k5d0k8sl2bsdgvp7hpcnri6kxk227kn7")))

(define-public crate-zest4096-0.1.15 (c (n "zest4096") (v "0.1.15") (h "1if2yi19f20l1h091963ikz8qairm7r1lg852b1j47x216qm1chk")))

(define-public crate-zest4096-0.1.16 (c (n "zest4096") (v "0.1.16") (h "135bw2bz2y6h3dii7dqw1qzzwr8ykyckadmd54xyf2ipx4als76z")))

(define-public crate-zest4096-0.1.17 (c (n "zest4096") (v "0.1.17") (h "1klcxqgxpiawjsp3rapji0wdl3fs8nryx5123g13bslnv8fl0ff9")))

(define-public crate-zest4096-0.1.18 (c (n "zest4096") (v "0.1.18") (h "0sg0dsc25zf8cm72gqmi6kcshlhpkyr8qx687in8fznkvs6bnwwa")))

(define-public crate-zest4096-0.1.19 (c (n "zest4096") (v "0.1.19") (h "0vnm1gc7rkv6084man6q9ab99mzwpmag9k1mk2ky8wipk9vxvlsw")))

(define-public crate-zest4096-0.1.20 (c (n "zest4096") (v "0.1.20") (h "0zqwxn4z08k2hr4nzcgq0bnmvph2df4ghpxij6q6v7rm6ialma0s")))

(define-public crate-zest4096-0.1.21 (c (n "zest4096") (v "0.1.21") (h "00xg9r5qb8bgizfmllnhkkmxikwfd74s7rv8sd8ikrah8d9g15sv")))

(define-public crate-zest4096-0.1.22 (c (n "zest4096") (v "0.1.22") (h "1hxz01y8dij57hmf3blb2f9pb7b8xg3q9q1iqj264iv3ns3gkmzw")))

(define-public crate-zest4096-0.1.23 (c (n "zest4096") (v "0.1.23") (h "1c2282qi30a49cypb52jhn93mp9mzrkyf02580h5v0bk49276n1b")))

(define-public crate-zest4096-0.1.24 (c (n "zest4096") (v "0.1.24") (h "1kic0q7g2l0rhq18y556bw3c16hn0vpsbafxhydw1j5i98dp7563")))

(define-public crate-zest4096-0.1.25 (c (n "zest4096") (v "0.1.25") (h "0qlrg186z4v2fxhw8bj5yk6k58pyymwrhq2zvw3jkksj24g05v9x")))

(define-public crate-zest4096-0.1.26 (c (n "zest4096") (v "0.1.26") (h "0pgyz504c5fw9pvrq4h5k1s11l5qdhyzf8cwvghhc48328dsjvlf")))

(define-public crate-zest4096-0.1.27 (c (n "zest4096") (v "0.1.27") (h "05qmn3jyack9f7rmbjzzcn9xyfrns5wkly1wm491vp9gk7ihs1b8")))

