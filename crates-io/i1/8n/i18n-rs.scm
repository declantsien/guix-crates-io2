(define-module (crates-io i1 #{8n}# i18n-rs) #:use-module (crates-io))

(define-public crate-i18n-rs-0.0.1 (c (n "i18n-rs") (v "0.0.1") (d (list (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "1siy5f2s60061vazn2hmlfxjln1nq0w017d1jny0zzmdg15k9g2p")))

(define-public crate-i18n-rs-0.1.0 (c (n "i18n-rs") (v "0.1.0") (d (list (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "15kwb763c81xkpfzwn9lyqd9j3n9dhrlvsyw8dl6zrhfir9l938f")))

