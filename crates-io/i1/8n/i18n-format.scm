(define-module (crates-io i1 #{8n}# i18n-format) #:use-module (crates-io))

(define-public crate-i18n-format-0.1.0 (c (n "i18n-format") (v "0.1.0") (d (list (d (n "gettext-rs") (r "^0.7.0") (f (quote ("gettext-system"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1bnnkg076cri0c6sblkhgvs38hsxiqzh7dhdm8fxphzcdi5abyms")))

(define-public crate-i18n-format-0.2.0 (c (n "i18n-format") (v "0.2.0") (d (list (d (n "gettext-rs") (r "^0.7.0") (f (quote ("gettext-system"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "182fz88817vhn26h7dcln03vdkrdammnznh9wgq0711fzvfccjgj")))

