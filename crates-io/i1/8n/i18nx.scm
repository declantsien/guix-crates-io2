(define-module (crates-io i1 #{8n}# i18nx) #:use-module (crates-io))

(define-public crate-i18nx-0.1.0 (c (n "i18nx") (v "0.1.0") (d (list (d (n "formatx") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0d0fkznl49qdlzj1nkgl4mjfhilxhg3xcavbj3cxi9zil1li84fb")))

