(define-module (crates-io i1 #{8n}# i18n_provider-rizzen-yazston) #:use-module (crates-io))

(define-public crate-i18n_provider-rizzen-yazston-0.6.1 (c (n "i18n_provider-rizzen-yazston") (v "0.6.1") (d (list (d (n "i18n_utility-rizzen-yazston") (r "^0.6.1") (d #t) (k 0)))) (h "17nkaxr8yd2i0nyvk38gd2srbq4qwdvdzy5b5w9rld32xjmr7nga") (r "1.70.0")))

(define-public crate-i18n_provider-rizzen-yazston-0.7.0 (c (n "i18n_provider-rizzen-yazston") (v "0.7.0") (d (list (d (n "i18n_utility-rizzen-yazston") (r "^0.7.0") (d #t) (k 0)))) (h "05ghjxzj47zik1yz5b8yskfivz6922zsz9jpbx2dgdmmh3iznh4g") (f (quote (("sync" "i18n_utility-rizzen-yazston/sync")))) (r "1.70.0")))

(define-public crate-i18n_provider-rizzen-yazston-0.8.0 (c (n "i18n_provider-rizzen-yazston") (v "0.8.0") (d (list (d (n "i18n_utility-rizzen-yazston") (r "^0.8.0") (d #t) (k 0)))) (h "1kds9qf0gk5axsywf0iwhv8195szyqgyyxdaiycnhvqgddx7266k") (f (quote (("sync" "i18n_utility-rizzen-yazston/sync")))) (r "1.70.0")))

(define-public crate-i18n_provider-rizzen-yazston-0.9.0 (c (n "i18n_provider-rizzen-yazston") (v "0.9.0") (d (list (d (n "i18n_utility-rizzen-yazston") (r "^0.9.0") (k 0)))) (h "0yi6d7g59knfgb588n53myk0i07wbbc7avixjsiy287r5bgzvs8x") (f (quote (("sync" "i18n_utility-rizzen-yazston/sync")))) (r "1.70.0")))

(define-public crate-i18n_provider-rizzen-yazston-0.9.1 (c (n "i18n_provider-rizzen-yazston") (v "0.9.1") (d (list (d (n "i18n_utility-rizzen-yazston") (r "^0.9.1") (k 0)))) (h "1iygn37zgm26iv1wjx9g7z89166b51617gxgyknl805a12p610ry") (f (quote (("sync" "i18n_utility-rizzen-yazston/sync")))) (r "1.70.0")))

