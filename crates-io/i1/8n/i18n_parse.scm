(define-module (crates-io i1 #{8n}# i18n_parse) #:use-module (crates-io))

(define-public crate-i18n_parse-0.1.1 (c (n "i18n_parse") (v "0.1.1") (d (list (d (n "aok") (r "^0.1.11") (d #t) (k 0)) (d (n "aok") (r "^0.1.11") (d #t) (k 2)) (d (n "loginit") (r "^0.1.10") (d #t) (k 2)) (d (n "static_init") (r "^1.0.3") (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (f (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 2)))) (h "0shv8wkdj1d4dkyh6x9wfip5appbjkiwk6rf2rpsm4k6iwjdamxj")))

