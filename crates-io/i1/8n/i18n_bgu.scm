(define-module (crates-io i1 #{8n}# i18n_bgu) #:use-module (crates-io))

(define-public crate-i18n_bgu-0.1.1 (c (n "i18n_bgu") (v "0.1.1") (d (list (d (n "aok") (r "^0.1.3") (d #t) (k 0)) (d (n "bgu") (r "^0.1.15") (d #t) (k 0)))) (h "1axlipwhzm23iyahifj30f7s8z51d59xm1gzag9i2lq0c7k4gxgl")))

(define-public crate-i18n_bgu-0.1.2 (c (n "i18n_bgu") (v "0.1.2") (d (list (d (n "aok") (r "^0.1.3") (d #t) (k 0)) (d (n "bgu") (r "^0.1.16") (d #t) (k 0)))) (h "09p2km67yqxb0z1ivwpb4hzvkhwwzj7y3wd5357nzgwf7jw95ddf")))

(define-public crate-i18n_bgu-0.1.3 (c (n "i18n_bgu") (v "0.1.3") (d (list (d (n "aok") (r "^0.1.11") (d #t) (k 0)) (d (n "bgu") (r "^0.1.19") (d #t) (k 0)) (d (n "const-str") (r "^0.5.7") (d #t) (k 0)))) (h "0h3ynq6afybgzpgj6xpnkyq19g6hznlalj6lnynsaj5mcc29m899")))

