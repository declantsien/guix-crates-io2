(define-module (crates-io i1 #{8n}# i18n_codegen) #:use-module (crates-io))

(define-public crate-i18n_codegen-0.1.0 (c (n "i18n_codegen") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.14") (d #t) (k 2)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)))) (h "1ra9axw30mz2dqx797jzgkibn26y8rj8fbd2q63l1ganm3nbf7l9")))

(define-public crate-i18n_codegen-0.1.1 (c (n "i18n_codegen") (v "0.1.1") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.14") (d #t) (k 2)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)))) (h "0msszix2cdbdwl7rczsglca3sysnck7jcsdywir0x5hhl50m0qhx")))

