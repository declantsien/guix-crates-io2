(define-module (crates-io i1 #{8n}# i18n-again-support) #:use-module (crates-io))

(define-public crate-i18n-again-support-0.0.0 (c (n "i18n-again-support") (v "0.0.0") (h "1521432g4vlg450rqbk59hzkkggwybij0pc9jb9w8l4mvcjjnlsd")))

(define-public crate-i18n-again-support-0.0.1 (c (n "i18n-again-support") (v "0.0.1") (d (list (d (n "cargo_toml") (r "^0.15") (d #t) (k 0)) (d (n "fs-err") (r "^2.9") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "postcard") (r "^1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "0kvm2v7njz81di030isfav99a948drxykarhqp8v472q8wrw1pd7")))

