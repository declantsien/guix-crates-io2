(define-module (crates-io i1 #{8n}# i18n_utility-rizzen-yazston) #:use-module (crates-io))

(define-public crate-i18n_utility-rizzen-yazston-0.6.1 (c (n "i18n_utility-rizzen-yazston") (v "0.6.1") (d (list (d (n "icu_locid") (r "^1.2.0") (d #t) (k 0)))) (h "1zsil76pjb8fkli64vmlbpin5ndd39fn7xw6qkdc4w1zsc9nag9l") (r "1.70.0")))

(define-public crate-i18n_utility-rizzen-yazston-0.7.0 (c (n "i18n_utility-rizzen-yazston") (v "0.7.0") (d (list (d (n "icu_locid") (r "^1.3.2") (d #t) (k 0)))) (h "15h6fdm44cpaaj1kbn3r9id57115mahjvc4s28f79cvmdqnpc9bn") (f (quote (("sync")))) (r "1.70.0")))

(define-public crate-i18n_utility-rizzen-yazston-0.8.0 (c (n "i18n_utility-rizzen-yazston") (v "0.8.0") (d (list (d (n "fixed_decimal") (r "^0.5.4") (f (quote ("ryu"))) (d #t) (k 0)) (d (n "icu_calendar") (r "^1.4.0") (d #t) (k 0)) (d (n "icu_locid") (r "^1.4.0") (d #t) (k 0)))) (h "02ramm03xh3wklr8llljhgi7arcnxbzf73x2cgmqy9p673878adf") (f (quote (("sync")))) (r "1.70.0")))

(define-public crate-i18n_utility-rizzen-yazston-0.9.0 (c (n "i18n_utility-rizzen-yazston") (v "0.9.0") (d (list (d (n "fixed_decimal") (r "^0.5.4") (f (quote ("ryu"))) (k 0)) (d (n "icu_calendar") (r "^1.4.0") (k 0)) (d (n "icu_locid") (r "^1.4.0") (k 0)))) (h "0r75yf2h2rscm8qmnj8ycy1wf6sysrlprnbw8c9l294cncmf0jk5") (f (quote (("sync") ("icu_extended")))) (r "1.70.0")))

(define-public crate-i18n_utility-rizzen-yazston-0.9.1 (c (n "i18n_utility-rizzen-yazston") (v "0.9.1") (d (list (d (n "fixed_decimal") (r "^0.5.4") (f (quote ("ryu"))) (k 0)) (d (n "icu_calendar") (r "^1.4.0") (k 0)) (d (n "icu_locid") (r "^1.4.0") (k 0)))) (h "0c0ygxb9n44323wq1i2qq6i0lr72waxw0rsjmwx09w1lx42vdfpa") (f (quote (("sync") ("icu_extended")))) (r "1.70.0")))

