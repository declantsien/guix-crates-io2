(define-module (crates-io i1 #{8n}# i18n-again-extract) #:use-module (crates-io))

(define-public crate-i18n-again-extract-0.0.1 (c (n "i18n-again-extract") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fs-err") (r "^2.9") (d #t) (k 0)) (d (n "i18n-again-support") (r "^0.0.1") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1499drwfp985w19ln3kpj7gy76cjsanwxb8q4ysimczvlrls5ryj")))

