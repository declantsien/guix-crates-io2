(define-module (crates-io i1 #{8n}# i18n-again-macro) #:use-module (crates-io))

(define-public crate-i18n-again-macro-0.0.1 (c (n "i18n-again-macro") (v "0.0.1") (d (list (d (n "fs-err") (r "^2.9") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "i18n-again-support") (r "^0.0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0f62kymqc80hhmy5xmglb1k9md0010bvifjpysjc424djz9fxzl9")))

