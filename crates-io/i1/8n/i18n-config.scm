(define-module (crates-io i1 #{8n}# i18n-config) #:use-module (crates-io))

(define-public crate-i18n-config-0.1.0 (c (n "i18n-config") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1pgdss32yxwg005y7na5rrsancw6bhn1wrc2iqw3kl1grivyipnj")))

(define-public crate-i18n-config-0.1.1 (c (n "i18n-config") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0w921bvfy5nig5a6b1kh01s0y9wayzq9i2pslp3g2l43d26d3018")))

(define-public crate-i18n-config-0.1.2 (c (n "i18n-config") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0rqinxzzajh1mhqykipkhlqnlq38wha243i5xzj4iy5qv89ldsgw")))

(define-public crate-i18n-config-0.2.0 (c (n "i18n-config") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "17xjzyc44av2v54vfvl99njb99cfyv8v8qlkq3lng8xwlvnlmifb")))

(define-public crate-i18n-config-0.2.1 (c (n "i18n-config") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0nlyh6gs6pppwa60lqdynwqq8qlw1d88z3il54zjh71kizy6rc2c")))

(define-public crate-i18n-config-0.2.2 (c (n "i18n-config") (v "0.2.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1b6gh8fgx4w08bnzqhg67jnnmsbmy32k4184lacg2y8dl5rb1i5v")))

(define-public crate-i18n-config-0.3.0 (c (n "i18n-config") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0wwzxz22hf63b9sldq79g5giy2bz84fgisa1k96ag9bpb7qjwa2b")))

(define-public crate-i18n-config-0.4.0 (c (n "i18n-config") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (f (quote ("serde"))) (d #t) (k 0)))) (h "0l42iv3pc5vppld62gp1ncabyq7376qhn8yvqr336bljk9n8yvaw")))

(define-public crate-i18n-config-0.4.1 (c (n "i18n-config") (v "0.4.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (f (quote ("serde"))) (d #t) (k 0)))) (h "1kprz7kiirdwg480c51cl2fczazpqgmdz89srypgvb7szs4aiqnb")))

(define-public crate-i18n-config-0.4.2 (c (n "i18n-config") (v "0.4.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (f (quote ("serde"))) (d #t) (k 0)))) (h "10yr0k8vy2s60yzajn659yscb3ch0ya6ywyqrgrm3yxb8g6zyamn")))

(define-public crate-i18n-config-0.4.3 (c (n "i18n-config") (v "0.4.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (f (quote ("serde"))) (d #t) (k 0)))) (h "1py8ddiyh3fqnr4lcdl078zz5w8cbsdnk0dw74bh2hv5xv7977rx")))

(define-public crate-i18n-config-0.4.4 (c (n "i18n-config") (v "0.4.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (f (quote ("serde"))) (d #t) (k 0)))) (h "1mb8lxp3ba3ipzm1nfkr8k0a749m2922r9kf3qmjyvmdmm60i1xr")))

(define-public crate-i18n-config-0.4.5 (c (n "i18n-config") (v "0.4.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (f (quote ("serde"))) (d #t) (k 0)))) (h "1nfvrzrglladisdrws26nr1d715qsafs66phl2cvph9md9ng34b6")))

(define-public crate-i18n-config-0.4.6 (c (n "i18n-config") (v "0.4.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)) (d (n "unic-langid") (r "^0.9") (f (quote ("serde"))) (d #t) (k 0)))) (h "0h3ndvkk4ws2jgkrk6wnbpc6l7xmnlr1ycxr49dzs8dwik2f770c")))

