(define-module (crates-io i1 #{8e}# i18e) #:use-module (crates-io))

(define-public crate-i18e-0.1.1 (c (n "i18e") (v "0.1.1") (d (list (d (n "aok") (r "^0.1.3") (d #t) (k 2)) (d (n "loginit") (r "^0.1.10") (d #t) (k 2)) (d (n "static_init") (r "^1.0.3") (d #t) (k 2)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "04g4h3sdja4brnw1rfxxncrkrkmm3j387m996ygpsmnf9x71x68y")))

