(define-module (crates-io ko mg komga) #:use-module (crates-io))

(define-public crate-komga-1.9.2 (c (n "komga") (v "1.9.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1d682hq7lsa4clm8l49jk8ixfm8zfsnnikibbsnjwc3c1ydra5m4")))

