(define-module (crates-io ko rg korg-syro-sys) #:use-module (crates-io))

(define-public crate-korg-syro-sys-0.1.0 (c (n "korg-syro-sys") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "wav") (r "^0.4.1") (d #t) (k 2)))) (h "0rp1lw5qsry68acdmdjn4rcdca7j2hlciqr5yrjq56klnld9hl2k")))

(define-public crate-korg-syro-sys-0.2.0 (c (n "korg-syro-sys") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.62") (d #t) (k 1)) (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "wav") (r "^0.4.1") (d #t) (k 2)))) (h "0myq7gdwrc7faybnbm0i53mjf52q16zanlmz1cj7nwyndxqq7hz7")))

