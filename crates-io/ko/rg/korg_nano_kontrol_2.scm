(define-module (crates-io ko rg korg_nano_kontrol_2) #:use-module (crates-io))

(define-public crate-korg_nano_kontrol_2-0.1.0 (c (n "korg_nano_kontrol_2") (v "0.1.0") (d (list (d (n "midir") (r "^0.5") (d #t) (k 0)))) (h "0jfpx1ml3vbaf0gn870wpk6i12z5xlhs19xk4c1gdsvlxykx47pk")))

(define-public crate-korg_nano_kontrol_2-0.1.1 (c (n "korg_nano_kontrol_2") (v "0.1.1") (d (list (d (n "midir") (r "^0.5") (d #t) (k 0)))) (h "077xz5pgkacc1zd652jkxfdsmqlh9w7wf5sbky16d6ajvr2v0ak4")))

