(define-module (crates-io ko rt korten) #:use-module (crates-io))

(define-public crate-korten-0.0.1 (c (n "korten") (v "0.0.1") (d (list (d (n "rustix") (r "^0.38.21") (f (quote ("mm"))) (k 0)) (d (n "scudo") (r "^0.1.3") (d #t) (k 2)))) (h "0ks2pkjq4878qg0g36j5qaad8dl2yr0rr0c4qkkf9r5crfsa0ddd")))

