(define-module (crates-io ko ko kokoro-dynamic-plugin) #:use-module (crates-io))

(define-public crate-kokoro-dynamic-plugin-0.0.1 (c (n "kokoro-dynamic-plugin") (v "0.0.1") (d (list (d (n "kokoro-core") (r "^0.0.1") (d #t) (k 0)) (d (n "kokoro-default-impl") (r "^0.0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)))) (h "15ds9rywan2bfbcdlnmzh98qd3q1dw2yjmx2ddvsnq4hv6q28zb5") (y #t)))

(define-public crate-kokoro-dynamic-plugin-0.0.2 (c (n "kokoro-dynamic-plugin") (v "0.0.2") (d (list (d (n "kokoro-core") (r "^0.0.2") (d #t) (k 0)) (d (n "kokoro-default-impl") (r "^0.0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0hi7nlljg0q32jksf881g18fjvz3xv6xwpd171clrs5y58qk20x8") (y #t)))

(define-public crate-kokoro-dynamic-plugin-0.0.3 (c (n "kokoro-dynamic-plugin") (v "0.0.3") (d (list (d (n "kokoro-core") (r "^0.0.3") (d #t) (k 0)) (d (n "kokoro-default-impl") (r "^0.0.3") (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1li56g5n4xqzlxr19w3faah9q6z26kl0m0cgrrsi0f3j1bhbpk39")))

(define-public crate-kokoro-dynamic-plugin-0.0.4 (c (n "kokoro-dynamic-plugin") (v "0.0.4") (d (list (d (n "kokoro-core") (r "^0.0.4") (d #t) (k 0)) (d (n "kokoro-default-impl") (r "^0.0.4") (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "0ynsgily0q010ygw8p16025l2alm56sjj106k64rrg4c2xrwdz97")))

(define-public crate-kokoro-dynamic-plugin-0.0.5 (c (n "kokoro-dynamic-plugin") (v "0.0.5") (d (list (d (n "kokoro-core") (r "^0.0.5") (d #t) (k 0)) (d (n "kokoro-default-impl") (r "^0.0.5") (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "158phdk172xbvdfs7qm6vn7aivlz5sflrq280j57ys2mmr3skhsx")))

(define-public crate-kokoro-dynamic-plugin-0.0.6 (c (n "kokoro-dynamic-plugin") (v "0.0.6") (d (list (d (n "kokoro-core") (r "^0.0.6") (d #t) (k 0)) (d (n "kokoro-default-impl") (r "^0.0.6") (d #t) (k 0)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "00j0cz9yk3zp1r21xwvgsxmw4v071dl0gqh691pxh5l2k4cq11wr")))

