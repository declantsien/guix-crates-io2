(define-module (crates-io ko ko kokoro-macros) #:use-module (crates-io))

(define-public crate-kokoro-macros-0.0.1 (c (n "kokoro-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "08npashridja7w3nyalbpjkgj34g0nllbilw5jdxw4bxvjv6k764") (y #t)))

(define-public crate-kokoro-macros-0.0.2 (c (n "kokoro-macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0imrjknqqc35vck2rjzqp3plpshf8xb7z6k39p08ffx8fcm44ca5") (y #t)))

(define-public crate-kokoro-macros-0.0.3 (c (n "kokoro-macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1nki5w3h0nb51f3v1ha0gwll3irkzipv4bm5dl9167rdni0sxz31")))

(define-public crate-kokoro-macros-0.0.4 (c (n "kokoro-macros") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0ygyxdhbmgvwbyaxn2wzrwk27h7pkqk36333g1h32vvx0hpj60y3")))

(define-public crate-kokoro-macros-0.0.5 (c (n "kokoro-macros") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "02x25rl48yw3mglmj9hrfb1jjf0n4ipq38jzlyw6i8z4ds64qyx8")))

(define-public crate-kokoro-macros-0.0.6 (c (n "kokoro-macros") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1qghnrqyp51b2q9c79fq1salqcidasmpqs7d5gljs866yx3ay4z1")))

