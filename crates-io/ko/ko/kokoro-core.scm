(define-module (crates-io ko ko kokoro-core) #:use-module (crates-io))

(define-public crate-kokoro-core-0.0.1 (c (n "kokoro-core") (v "0.0.1") (d (list (d (n "dashmap") (r "^5.5.3") (f (quote ("rayon" "inline"))) (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "1p8pkj5ca96dsjyyqa2y21rs33sampczjd1n68wxnjhs8dcp911c") (f (quote (("nightly")))) (y #t)))

(define-public crate-kokoro-core-0.0.2 (c (n "kokoro-core") (v "0.0.2") (d (list (d (n "dashmap") (r "^5.5.3") (f (quote ("rayon" "inline"))) (d #t) (k 0)) (d (n "either") (r "^1.10.0") (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "1kvix6gakw8pwy97k429kmjgi5xgzjyzwgxdbzqbxnm7ijr8yllp") (f (quote (("nightly")))) (y #t)))

(define-public crate-kokoro-core-0.0.3 (c (n "kokoro-core") (v "0.0.3") (d (list (d (n "dashmap") (r "^5.5.3") (f (quote ("rayon" "inline"))) (d #t) (k 0)) (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "1mbhrkspizmz4r1z8q1abf24pncyzph57yamy93pha9rlria1lxn") (f (quote (("nightly"))))))

(define-public crate-kokoro-core-0.0.4 (c (n "kokoro-core") (v "0.0.4") (d (list (d (n "dashmap") (r "^5.5.3") (f (quote ("rayon" "inline"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "0na6h0k0szg2aj56wfsg83d6h7v6d2ylqw2l65sa69azq4aiiq6x") (f (quote (("nightly"))))))

(define-public crate-kokoro-core-0.0.5 (c (n "kokoro-core") (v "0.0.5") (d (list (d (n "dashmap") (r "^5.5.3") (f (quote ("rayon" "inline"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "1v9dl2qj329ynfb29ils0376k9d0z8aw5zydlaqwnhzi6fwi3w03") (f (quote (("nightly"))))))

(define-public crate-kokoro-core-0.0.6 (c (n "kokoro-core") (v "0.0.6") (d (list (d (n "dashmap") (r "^5.5.3") (f (quote ("rayon" "inline"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)))) (h "0aajy5pgwiz6wjvxfr1s8lzq65aq86s8ccymymhw8s3avfdxikp4") (f (quote (("nightly"))))))

