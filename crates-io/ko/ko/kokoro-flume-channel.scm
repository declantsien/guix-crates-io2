(define-module (crates-io ko ko kokoro-flume-channel) #:use-module (crates-io))

(define-public crate-kokoro-flume-channel-0.0.2 (c (n "kokoro-flume-channel") (v "0.0.2") (d (list (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "kokoro-core") (r "^0.0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1ghr5djxlw4a43a0p6zqlfxvjj41pcfqic91v5mfdhdasdmxbhab")))

(define-public crate-kokoro-flume-channel-0.0.3 (c (n "kokoro-flume-channel") (v "0.0.3") (d (list (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "kokoro-core") (r "^0.0.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "03xdk6xg5pci65r9bzp6067bwhpl8pc6i8kavhzznrg3km2gv95c")))

(define-public crate-kokoro-flume-channel-0.0.4 (c (n "kokoro-flume-channel") (v "0.0.4") (d (list (d (n "flume") (r "^0.11.0") (d #t) (k 0)) (d (n "kokoro-core") (r "^0.0.6") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0lww08vm7vb00bg5kiiq0zjf7ja6nbyxhy1gzp0qvhqc2rc6njh5")))

