(define-module (crates-io ko ko kokoro-default-impl) #:use-module (crates-io))

(define-public crate-kokoro-default-impl-0.0.1 (c (n "kokoro-default-impl") (v "0.0.1") (d (list (d (n "kokoro-core") (r "^0.0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "03r48bl510svzp4b1ajkg26zj18704czw5sc6sn2a5qh3is5fxs9") (y #t)))

(define-public crate-kokoro-default-impl-0.0.2 (c (n "kokoro-default-impl") (v "0.0.2") (d (list (d (n "kokoro-core") (r "^0.0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rgk33xqfi7p0y7yzw7ngb1pzz798gxipiq69f9fh4ri2dxxq2sg") (y #t)))

(define-public crate-kokoro-default-impl-0.0.3 (c (n "kokoro-default-impl") (v "0.0.3") (d (list (d (n "kokoro-core") (r "^0.0.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0pr1zs0nsvrzvql0mdrznbf5ys9ywj22kjfrdr33l820wj5n7rim")))

(define-public crate-kokoro-default-impl-0.0.4 (c (n "kokoro-default-impl") (v "0.0.4") (d (list (d (n "kokoro-core") (r "^0.0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0m9vd46iapyq619rchzqpxlq1w7d2fkg6w28b0vp43h8lfvwgqy4")))

(define-public crate-kokoro-default-impl-0.0.5 (c (n "kokoro-default-impl") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "kokoro-core") (r "^0.0.5") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0460q1qz7y1wlhnrnq3xxmg8pi1wcypglfnbkb7ai076g2dhj74a")))

(define-public crate-kokoro-default-impl-0.0.6 (c (n "kokoro-default-impl") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "kokoro-core") (r "^0.0.6") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1im7awwip031hm2gdlms475rjn88dfm7zs0q823qr9hqbkh18raq")))

