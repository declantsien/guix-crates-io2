(define-module (crates-io ko ry koryto) #:use-module (crates-io))

(define-public crate-koryto-0.0.1 (c (n "koryto") (v "0.0.1") (h "03k2ib1bbmmr0hqz19ga4x3p8f8lz9x3qrm13xhqx9h7klaqgx0z")))

(define-public crate-koryto-0.0.2 (c (n "koryto") (v "0.0.2") (h "1pbrblcy79m2yk2f5lhg38hrm9sv48wxvzwapnissl1c86w1bdlz")))

(define-public crate-koryto-0.1.0 (c (n "koryto") (v "0.1.0") (h "1iwcma39mxwgfk80w55nramigbka82vlg4481kmzkdjfnfwk2629")))

(define-public crate-koryto-0.1.1 (c (n "koryto") (v "0.1.1") (d (list (d (n "thunderdome") (r "^0.6.1") (d #t) (k 0)))) (h "07rs4039ikca7zxfnbdsy386lnm8kkzcdlpwxm4vn2wjs4vymfhy")))

