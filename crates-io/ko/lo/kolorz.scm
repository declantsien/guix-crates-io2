(define-module (crates-io ko lo kolorz) #:use-module (crates-io))

(define-public crate-kolorz-0.1.0 (c (n "kolorz") (v "0.1.0") (h "1wdxw2qhmb0qc5vz0gvlj1bcc69412hlz7n5zwj96kxsrw8ypbm9")))

(define-public crate-kolorz-0.2.0 (c (n "kolorz") (v "0.2.0") (h "1m24gjphnzjwlmbxcz39k1j9h15ky1qf2350bdmgwmb35airbiqi")))

(define-public crate-kolorz-0.3.0 (c (n "kolorz") (v "0.3.0") (h "06jjwsbbr3j20jhzx5gxdqbxn01zgf4s9l0y14hbdibawk6xc7hm")))

(define-public crate-kolorz-0.4.0 (c (n "kolorz") (v "0.4.0") (h "1jdd90dwxs7a78fv6pfwy84m7q6fxc88aym37fsvglnh7pnnsh2y")))

(define-public crate-kolorz-0.5.0 (c (n "kolorz") (v "0.5.0") (h "0kysf4hz1vb7g32wq88vy0wyxj1h3f6czh5xfk2vnzaicsv2qn5c")))

(define-public crate-kolorz-0.6.0 (c (n "kolorz") (v "0.6.0") (h "0r5srjs1nksqkiv16cq4jn1x89sv82dig0701ynlxigzkvn1pmwl")))

(define-public crate-kolorz-0.6.1 (c (n "kolorz") (v "0.6.1") (h "1k92wbkkf820af98q59glzhwmmbqq1pb9amgqbsj92lp2ifci9h9")))

(define-public crate-kolorz-0.7.0 (c (n "kolorz") (v "0.7.0") (h "1iag22m67wkwh0vgdk2jx8r3sccfbz41wcqf0aw9rxl33m50p95c")))

(define-public crate-kolorz-0.8.0 (c (n "kolorz") (v "0.8.0") (h "15dbjg6gh7n3pw20g2zvr4vmfwrfqbdn6h4p1dhn2masg7156gin")))

(define-public crate-kolorz-0.9.0 (c (n "kolorz") (v "0.9.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1xd0rhcgba3551yarkirg09xxi20z2nx991dvbaiwp6s1pk7qpi2")))

(define-public crate-kolorz-0.10.0 (c (n "kolorz") (v "0.10.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1hmghcfpmvkdqw7j7ar4vpf5h3w32z44k7hmc0phmblajxl42n2q")))

