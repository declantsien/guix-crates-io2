(define-module (crates-io ko lo kolorwheel) #:use-module (crates-io))

(define-public crate-kolorwheel-1.1.0 (c (n "kolorwheel") (v "1.1.0") (d (list (d (n "all_asserts") (r "^2.3.1") (d #t) (k 2)) (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "eframe") (r "^0.23.0") (d #t) (k 2)) (d (n "egui") (r "^0.23.0") (d #t) (k 2)))) (h "1za3mg198cpgg4czpxy16fk4f2n7cr53i3zd7rb78283j3d79bfk")))

(define-public crate-kolorwheel-1.1.1 (c (n "kolorwheel") (v "1.1.1") (d (list (d (n "all_asserts") (r "^2.3.1") (d #t) (k 2)) (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "eframe") (r "^0.23.0") (d #t) (k 2)) (d (n "egui") (r "^0.23.0") (d #t) (k 2)))) (h "1cpxci3s6bql52dmdqdk0riy8vk2pz44ijgslhnxzaay81iwmydg")))

