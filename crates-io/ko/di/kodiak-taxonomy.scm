(define-module (crates-io ko di kodiak-taxonomy) #:use-module (crates-io))

(define-public crate-kodiak-taxonomy-0.1.0 (c (n "kodiak-taxonomy") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "1jhal1i45bkx0k4s08wl15xdv9v5lanwphp13pa5iki5n0azgmda")))

(define-public crate-kodiak-taxonomy-0.2.0 (c (n "kodiak-taxonomy") (v "0.2.0") (d (list (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "0skz1c7x0m0c6jq3rz2srxynfai3m7gaq746cp8v9wwny8kkwmlk")))

