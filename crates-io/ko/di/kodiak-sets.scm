(define-module (crates-io ko di kodiak-sets) #:use-module (crates-io))

(define-public crate-kodiak-sets-0.1.0 (c (n "kodiak-sets") (v "0.1.0") (d (list (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)))) (h "1ikqj2swk4ri311qpv3zz4rbswb0pgnjcc6azls24wg62fhpsxx5")))

(define-public crate-kodiak-sets-0.2.0 (c (n "kodiak-sets") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.178") (o #t) (k 0)))) (h "009n9h75ynwnhvdw8s040nl9lhfihdisj7vi7aw5yplnbsys6rs0") (f (quote (("std") ("serde-derive" "serde/derive" "serde/std") ("default" "std"))))))

