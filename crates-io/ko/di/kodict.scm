(define-module (crates-io ko di kodict) #:use-module (crates-io))

(define-public crate-kodict-0.1.0 (c (n "kodict") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)))) (h "0yj89yry3xv47r4907khzxxf2pbmg9mv7dma1sjhkry371dvgz6s")))

(define-public crate-kodict-0.1.1 (c (n "kodict") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)))) (h "1clq7gaj4i3iw1nqwfhis36xq1ad1jgnrhc440x899bdbw9jhq0i")))

(define-public crate-kodict-0.1.2 (c (n "kodict") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)))) (h "05mxr8cc0ry8zf24d0b7fyhkfqz2bbvq1yki8ik5c8h2v9xvpn3d")))

(define-public crate-kodict-0.1.3 (c (n "kodict") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "118j9sx5zby4p02fppysz6q2775zd8k6h5yqll247kid4ji6nwcb")))

(define-public crate-kodict-0.2.0 (c (n "kodict") (v "0.2.0") (d (list (d (n "calamine") (r "^0.15") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "1cjgfclij0084a7cv7va81cs7dxzf3yka15ab2mzsr2y7paiv1j7")))

(define-public crate-kodict-0.2.1 (c (n "kodict") (v "0.2.1") (d (list (d (n "calamine") (r "^0.15") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "0rbk6qipxb4mxdpqi6hnni4icxcnam0j3ymi7jvs8xh0yydxrvh0")))

