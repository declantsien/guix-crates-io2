(define-module (crates-io ko al koala) #:use-module (crates-io))

(define-public crate-koala-0.1.0 (c (n "koala") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0b8cw20x0jirmzbbkq6xs1xj4266swlbqa0mq4z4yv8bdj6vnn8h")))

(define-public crate-koala-0.1.1 (c (n "koala") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0lx4gkjjgk39s4qay5pwp1yrm5ywdihp60g9shaqjghrzynhbrkp")))

(define-public crate-koala-0.1.2 (c (n "koala") (v "0.1.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "04pm7mij5bp2kyqwl92smg57g3zxplvsk0zbmnbxk31s3wmcw40w")))

(define-public crate-koala-0.1.3 (c (n "koala") (v "0.1.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1a4k5jy9j1q0xil73w9l10gibwcgww6d89pad19hkad0pyjn0vkp")))

(define-public crate-koala-0.1.4 (c (n "koala") (v "0.1.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1lacnkwfavrg8si6k9cjijz8ch8xbj6pp7j69h78v6bfl65myick")))

(define-public crate-koala-0.1.5 (c (n "koala") (v "0.1.5") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0z9k9x44vy8ipk4mb6hcnw7p58y56av16gk47fffr8jrj55sd26i")))

