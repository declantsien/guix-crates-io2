(define-module (crates-io ko un kount) #:use-module (crates-io))

(define-public crate-kount-0.1.0 (c (n "kount") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.8.2") (f (quote ("macros" "sync" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.19") (f (quote ("env-filter" "fmt" "ansi"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.1") (d #t) (k 0)) (d (n "winit") (r "^0.25.0") (d #t) (k 0)))) (h "13yj4w4zrcnn70a5mxg1qwkbclvzc9mgw9cccaaldwmmlbfrjrjx")))

