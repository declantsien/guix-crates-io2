(define-module (crates-io ko mi komihash) #:use-module (crates-io))

(define-public crate-komihash-0.2.0 (c (n "komihash") (v "0.2.0") (h "0k33a7qn97fn7p68wdmfjapc593kx15sadw7rv2qvx8ml5j0m7bk")))

(define-public crate-komihash-0.2.1 (c (n "komihash") (v "0.2.1") (h "0lrgrpdrn5ns2hmz5lqli1dpzz2nsans879vdwxyyvj40mspp08h") (y #t)))

(define-public crate-komihash-0.2.2 (c (n "komihash") (v "0.2.2") (h "0lfbgwsc5swimvj2whldl9yimvmwlnqb6iaq5j40caqrjpin56g2") (y #t)))

(define-public crate-komihash-0.2.3 (c (n "komihash") (v "0.2.3") (h "1v13nnzwz8dxrbc8nkp1bhjnlzybl8xl2ca27iz9zrncf7ayki7q")))

(define-public crate-komihash-0.3.0 (c (n "komihash") (v "0.3.0") (h "0hgyqq12lgnhhwh2pma2m947i1his3xzigk326kshr3pwan9053p")))

(define-public crate-komihash-0.4.0 (c (n "komihash") (v "0.4.0") (h "1z9q1vr5f0ncx8v0kb0binrdgscp716i85p1ksv0cdqxvmg3x43m")))

(define-public crate-komihash-0.4.1 (c (n "komihash") (v "0.4.1") (h "123nf3q2ddqz8hj3llim43rw68kh34k0ljj5hlnarwbds5xlhcr8")))

