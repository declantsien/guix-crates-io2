(define-module (crates-io ko mi komi-hash) #:use-module (crates-io))

(define-public crate-komi-hash-0.1.0 (c (n "komi-hash") (v "0.1.0") (h "0dl06minkpl4znlxi3cr03xz5hmfg2rf6n2zdgxb80fl3gy8k46h") (y #t)))

(define-public crate-komi-hash-0.1.1 (c (n "komi-hash") (v "0.1.1") (h "0ajzssfgvq4d0wh9q9bk96ml45s2rpafv1p05xy4qyidp5ifxsq9") (y #t)))

(define-public crate-komi-hash-0.1.2 (c (n "komi-hash") (v "0.1.2") (h "1mlypyfriz15xxspxjvy527kjs80k2niniwzz1yw82rxx0kcrzq2") (y #t)))

(define-public crate-komi-hash-0.1.3 (c (n "komi-hash") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "pcg") (r "^4.1.0") (d #t) (k 2)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 2)))) (h "14j8f4yqdqqw7w842if4pz25jb2pfrz2phhk643cxaix6jgm8alx") (y #t)))

(define-public crate-komi-hash-0.1.4 (c (n "komi-hash") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "pcg") (r "^4.1.0") (d #t) (k 2)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 2)))) (h "0ynim6sb9v8whwl965p5h1m9flm5cqfayrw9fj3s0dvgympq9fz3") (y #t)))

