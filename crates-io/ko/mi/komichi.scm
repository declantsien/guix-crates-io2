(define-module (crates-io ko mi komichi) #:use-module (crates-io))

(define-public crate-komichi-0.1.0 (c (n "komichi") (v "0.1.0") (h "0v913yv968hmfnz1frlzlh8srz2hmr4yvzihfnhg2070zys62dvc") (y #t)))

(define-public crate-komichi-0.2.0 (c (n "komichi") (v "0.2.0") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "dirs-sys") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)) (d (n "whoami") (r "^1.5.1") (d #t) (k 0)))) (h "0qdj2b1hwlw0hdax208il224d1wa9sqdqz75j42i4d3m6vj2lag1") (y #t)))

(define-public crate-komichi-1.0.0 (c (n "komichi") (v "1.0.0") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dirs-sys") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)))) (h "0vwzn0y5mmzlv4slrkbrkx64yh02s7gmr69ig6z1hg36a5i5rl2s") (y #t)))

(define-public crate-komichi-1.0.1 (c (n "komichi") (v "1.0.1") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dirs-sys") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)))) (h "19m7i0mxlyhzqafdy00c5db2f0y1w6v91n51v2ilvsc3a7wnlfky") (y #t)))

(define-public crate-komichi-1.0.2 (c (n "komichi") (v "1.0.2") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dirs-sys") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)))) (h "1xddnl0fx2c0nzgr3098fnvp2k7g20j77kd31wjk5dlsy8ahxgi7")))

(define-public crate-komichi-1.0.3 (c (n "komichi") (v "1.0.3") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dirs-sys") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)))) (h "0cqz6dg68rncjcfcshpm8z8pjihnvvpv8nsqivy0cmq1xsrkq8ka")))

