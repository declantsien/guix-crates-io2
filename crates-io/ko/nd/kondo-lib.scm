(define-module (crates-io ko nd kondo-lib) #:use-module (crates-io))

(define-public crate-kondo-lib-0.1.0 (c (n "kondo-lib") (v "0.1.0") (d (list (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "08190b20ycyaks3akgnm45xz3sykzcr35xjj9028wfmfl9yla4xz")))

(define-public crate-kondo-lib-0.2.0 (c (n "kondo-lib") (v "0.2.0") (d (list (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1cyc9q7xs951djfzixc1p1gwh7l8arfw1ski6n9q07bpnslj72cq")))

(define-public crate-kondo-lib-0.6.0 (c (n "kondo-lib") (v "0.6.0") (d (list (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0ihkp7i7pqdssjv3yclqa7b5x21vhn9j5a8gbvdxx8jhcn3y421l")))

(define-public crate-kondo-lib-0.7.0 (c (n "kondo-lib") (v "0.7.0") (d (list (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "101yrl40p9rnk7zkvhdvlmv713iscmlp687zjsgpb3vfg3y98ksi")))

(define-public crate-kondo-lib-0.8.0 (c (n "kondo-lib") (v "0.8.0") (d (list (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "09z22b98sympcmh9afljyzxwh5dw8n93jv000kidp6xin5p1rld8")))

