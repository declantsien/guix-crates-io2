(define-module (crates-io ko nd kondo-ui) #:use-module (crates-io))

(define-public crate-kondo-ui-0.1.0 (c (n "kondo-ui") (v "0.1.0") (d (list (d (n "druid") (r "^0.5.0") (d #t) (k 0)) (d (n "kondo-lib") (r "^0.1") (d #t) (k 0)))) (h "039bfkmnacm0fpp6mkxqwn5wxdz3gw2jrvhrpf8j9i6qrkp501mk")))

(define-public crate-kondo-ui-0.2.0 (c (n "kondo-ui") (v "0.2.0") (d (list (d (n "druid") (r "^0.7") (d #t) (k 0)) (d (n "kondo-lib") (r "^0.2") (d #t) (k 0)))) (h "0mzxpa3k8c7wdds6lpjwbaaa9xjydjrhqilk5ma83crv4fcgvn3c")))

(define-public crate-kondo-ui-0.6.0 (c (n "kondo-ui") (v "0.6.0") (d (list (d (n "druid") (r "^0.7") (d #t) (k 0)) (d (n "kondo-lib") (r "^0.6") (d #t) (k 0)))) (h "000vily2pm7aabskhlqh091qinq9iz2d5m492abcinw6am1dqz26")))

(define-public crate-kondo-ui-0.7.0 (c (n "kondo-ui") (v "0.7.0") (d (list (d (n "druid") (r "^0.7") (d #t) (k 0)) (d (n "kondo-lib") (r "^0.7") (d #t) (k 0)))) (h "1p8b3wbkbhgwlqybjz24cx6d5qfarhx8qvfyld2357p181n1rpk2")))

(define-public crate-kondo-ui-0.8.0 (c (n "kondo-ui") (v "0.8.0") (d (list (d (n "druid") (r "^0.7") (d #t) (k 0)) (d (n "kondo-lib") (r "^0.8") (d #t) (k 0)))) (h "04fzws04p1pj9cly2vb0kzh5kd10jggsgaarv6mkfl92764if0ac")))

