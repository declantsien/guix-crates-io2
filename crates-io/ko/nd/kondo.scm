(define-module (crates-io ko nd kondo) #:use-module (crates-io))

(define-public crate-kondo-0.1.0 (c (n "kondo") (v "0.1.0") (d (list (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0jm3qvqgh2ncxzb2184zgsacqyrjfr3mbz8kf1r05cns2z3khsk1")))

(define-public crate-kondo-0.2.0 (c (n "kondo") (v "0.2.0") (d (list (d (n "structopt") (r "^0.3.9") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0q7d3x6hd6ahl1xrf4y8p9msgh2nvq258s28n3pax3dx8lwpbyvf")))

(define-public crate-kondo-0.3.0 (c (n "kondo") (v "0.3.0") (d (list (d (n "kondo-lib") (r "^0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0n93v69nry0xjwb8xba50dsgwbmpy23v7x375k2zrsjdgrc8hr95")))

(define-public crate-kondo-0.4.0 (c (n "kondo") (v "0.4.0") (d (list (d (n "kondo-lib") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "16gsyz6qx4fj1j0z2lin78iji7d76mvi3ijv5wy79aggyv92dd6f")))

(define-public crate-kondo-0.6.0 (c (n "kondo") (v "0.6.0") (d (list (d (n "kondo-lib") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0x1q5yg62a1lxgzdh1pgmrpyzdd31kfl6gfnfs77s4f0zjj4y4gm")))

(define-public crate-kondo-0.7.0 (c (n "kondo") (v "0.7.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "kondo-lib") (r "^0.7") (d #t) (k 0)))) (h "0rh19x41llygw8svb76njjkx5qzb39mxqw9a06zvf736vb4a2j4b")))

(define-public crate-kondo-0.8.0 (c (n "kondo") (v "0.8.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4") (d #t) (k 0)) (d (n "kondo-lib") (r "^0.8") (d #t) (k 0)))) (h "1f5krilx0p7wb1nfwyir9rskcdmkzcw8wr9z42r88hdvpyas14yr")))

