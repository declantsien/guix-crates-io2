(define-module (crates-io ko ok kooka_lib_messenger) #:use-module (crates-io))

(define-public crate-kooka_lib_messenger-0.0.1 (c (n "kooka_lib_messenger") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.5.12") (d #t) (k 0)) (d (n "nats") (r "^0.3.1") (d #t) (k 0)))) (h "08kg1parnblc3wy2xabjpak7cq1az9yi3k9rv97wx5h8a7j9qxq4")))

(define-public crate-kooka_lib_messenger-0.0.2 (c (n "kooka_lib_messenger") (v "0.0.2") (d (list (d (n "env_logger") (r "^0.5.12") (d #t) (k 0)) (d (n "nats") (r "^0.2.0") (d #t) (k 0)))) (h "1wlrasjgdd1w4ysq380agk2d5897ns3imq107g4d4kn7amsqzwwx")))

(define-public crate-kooka_lib_messenger-0.0.3 (c (n "kooka_lib_messenger") (v "0.0.3") (d (list (d (n "env_logger") (r "^0.5.12") (d #t) (k 0)) (d (n "nats") (r "^0.3.1") (d #t) (k 0)))) (h "1dv5ia8hnylx0g09dqnb2ixmj60jxlr3bsza8c3wijm2d0nsdhjs")))

(define-public crate-kooka_lib_messenger-0.0.4 (c (n "kooka_lib_messenger") (v "0.0.4") (d (list (d (n "env_logger") (r "^0.5.12") (d #t) (k 0)) (d (n "nats") (r "^0.3.1") (d #t) (k 0)))) (h "16z95498m63837x32jnzf4a0jq9ixzhf4j4pqj1wplxi1saihr7j")))

(define-public crate-kooka_lib_messenger-0.0.5 (c (n "kooka_lib_messenger") (v "0.0.5") (d (list (d (n "env_logger") (r "^0.5.12") (d #t) (k 0)) (d (n "nats") (r "^0.3.1") (d #t) (k 0)))) (h "089zz3mz8wm3d2kw41s24ds0h76mkmnidvmkx4firv68b5srz80z")))

(define-public crate-kooka_lib_messenger-0.0.6 (c (n "kooka_lib_messenger") (v "0.0.6") (d (list (d (n "env_logger") (r "^0.5.12") (d #t) (k 0)) (d (n "nats") (r "^0.3.1") (d #t) (k 0)))) (h "0flv4kr17ikzl0wjxnbgskk4vl099aahx2nhi5v2m8pblbynh0zs")))

(define-public crate-kooka_lib_messenger-0.0.7 (c (n "kooka_lib_messenger") (v "0.0.7") (d (list (d (n "env_logger") (r "^0.5.12") (d #t) (k 0)) (d (n "nats") (r "^0.3.1") (d #t) (k 0)))) (h "1i5dwas8myrz9nriwf5z1w7w6vcw7w7x4x3q75k0bq8yd9i4glkh")))

(define-public crate-kooka_lib_messenger-0.0.8 (c (n "kooka_lib_messenger") (v "0.0.8") (d (list (d (n "env_logger") (r "^0.5.12") (d #t) (k 0)) (d (n "nats") (r "^0.3.1") (d #t) (k 0)))) (h "1xk0b93qcm28mkxr44wipjnhqsnl5lqx27ys26iknrdkx2xjpm09")))

(define-public crate-kooka_lib_messenger-0.0.9 (c (n "kooka_lib_messenger") (v "0.0.9") (d (list (d (n "env_logger") (r "^0.5.12") (d #t) (k 0)) (d (n "nats") (r "^0.3.1") (d #t) (k 0)))) (h "1f8vr8rbag70pn29hrlc3yv3wvi4k9jask4m5k7ims32ibss85q6")))

(define-public crate-kooka_lib_messenger-0.1.0 (c (n "kooka_lib_messenger") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "nats") (r "^0.3.1") (d #t) (k 0)))) (h "1sm6zvs8yf2cryqfnnv61hw2ybq19656h718fy8inayal3xgg57h")))

(define-public crate-kooka_lib_messenger-0.1.1 (c (n "kooka_lib_messenger") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)) (d (n "nats") (r "^0.3.2") (d #t) (k 0)))) (h "0hmzl1p3m06ng1f9b2l5r7g6lmssagwm765d79wb9dmksm06gy17")))

(define-public crate-kooka_lib_messenger-0.1.2 (c (n "kooka_lib_messenger") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)) (d (n "nats") (r "^0.3.2") (d #t) (k 0)))) (h "1spbwij5blzwzl0xkjghkisygbfv1cg4h4igjmn3x4bnb70ggxsv")))

