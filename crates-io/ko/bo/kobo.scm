(define-module (crates-io ko bo kobo) #:use-module (crates-io))

(define-public crate-kobo-0.1.0 (c (n "kobo") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "evdev-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1l4dcn9151a44ql62fxlb36yal7ca4xj6grwx04qbcp87a5vjzdq")))

(define-public crate-kobo-0.2.0 (c (n "kobo") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "evdev-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0mkqfvrrzrcw3m1ky6675jbs5z3rx0wpm2mfcxqqmwx7lslrz6s8")))

(define-public crate-kobo-0.3.0 (c (n "kobo") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "evdev-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0b3q51damdfrmrxfjq998r03kf96r053fw5wbx5rqn6zazhqsnw1")))

(define-public crate-kobo-0.3.1 (c (n "kobo") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "evdev-rs") (r "^0.5.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "073np2w8dsg57h6q00kcmfqifsc50g1z3ygwndpkm9x12vdmzb1r")))

