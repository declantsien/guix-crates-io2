(define-module (crates-io ko bo kobold_macros) #:use-module (crates-io))

(define-public crate-kobold_macros-0.0.0 (c (n "kobold_macros") (v "0.0.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rpafwdzwdz48w98dpa0g3whcgnqyl1ww2l1zhkn3hc7y0iscs15")))

(define-public crate-kobold_macros-0.1.0 (c (n "kobold_macros") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.1") (d #t) (k 0)) (d (n "beef") (r "^0.5.1") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "02zphryih9hj5xvgrxhjkmmrx0qsqcwlljclc1pb1b3rq32rfhk1")))

(define-public crate-kobold_macros-0.2.0 (c (n "kobold_macros") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "beef") (r "^0.5.2") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "19gbz30wd675lz4msk8mrvh5gvgjk216q8sz4fb5pxl9l0wm6b98")))

(define-public crate-kobold_macros-0.3.0 (c (n "kobold_macros") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "beef") (r "^0.5.2") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0as01ib8ml02gjlp9ab3v13navyjq08pry0ihy8z862v7r5xgnzn")))

(define-public crate-kobold_macros-0.4.0 (c (n "kobold_macros") (v "0.4.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "beef") (r "^0.5.2") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)))) (h "1pjz54qh1463mss4bdx19qp08vq3kkmvqbxnmibaa8ch3v5ld34g")))

(define-public crate-kobold_macros-0.4.1 (c (n "kobold_macros") (v "0.4.1") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "beef") (r "^0.5.2") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)))) (h "18vqxbfksm0hrgc55m9psyf28xw2yp2jzyc5085v4jclgh7gnxld")))

(define-public crate-kobold_macros-0.5.0 (c (n "kobold_macros") (v "0.5.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "beef") (r "^0.5.2") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 2)))) (h "1nd6c13jw8zij0yjgckzrky76cqdvvrgdg5v0xg1knz787wqsxal")))

(define-public crate-kobold_macros-0.6.0 (c (n "kobold_macros") (v "0.6.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "beef") (r "^0.5.2") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 2)))) (h "0qmccv8i7kp3f346mqw69ca8dvi8n88k9257rawfykh1r6var294")))

(define-public crate-kobold_macros-0.7.0 (c (n "kobold_macros") (v "0.7.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "beef") (r "^0.5.2") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 2)))) (h "0cshygl200pf47nklflwydpjfm2ygibyyxshbn1mdzk6jmvnmckg")))

(define-public crate-kobold_macros-0.7.1 (c (n "kobold_macros") (v "0.7.1") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "beef") (r "^0.5.2") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 2)))) (h "0j2lss2p4khcjyb60y1hyw7xa7kdj4kcqfgzq7i2vcrnkg1sl5ni")))

(define-public crate-kobold_macros-0.8.0 (c (n "kobold_macros") (v "0.8.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "beef") (r "^0.5.2") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 2)))) (h "0hi905jc4p0cjklirzphwm03xlrk7rnc92798krbcjwix3rqp4jl")))

(define-public crate-kobold_macros-0.9.0 (c (n "kobold_macros") (v "0.9.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "beef") (r "^0.5.2") (d #t) (k 0)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 2)))) (h "1l6q1hxydffplvl7n5675h7yp9ag4jxsnci2hkx2nddprgylk7ba")))

