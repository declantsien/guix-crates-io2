(define-module (crates-io ko bo kobold-tool) #:use-module (crates-io))

(define-public crate-kobold-tool-0.1.0 (c (n "kobold-tool") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "12vbamxsjmadq7h9zjjm99794jqmmcp0cp816dm9f17h2yd2y0k1")))

(define-public crate-kobold-tool-0.1.1 (c (n "kobold-tool") (v "0.1.1") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "14s2grx0yzmn529rmh0icpsqrbvl3ka39x9nzy5fl4w0g7jfx1ah")))

