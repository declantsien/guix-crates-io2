(define-module (crates-io ko bo kobold_qr) #:use-module (crates-io))

(define-public crate-kobold_qr-0.5.0 (c (n "kobold_qr") (v "0.5.0") (d (list (d (n "fast_qr") (r "^0.8.4") (d #t) (k 0)) (d (n "kobold") (r "^0.5.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("CanvasRenderingContext2d" "HtmlCanvasElement"))) (d #t) (k 0)))) (h "0yzml71wpmpfiyh3p7d1y2ryqa1waapsl0cwgy9rfh617xis7mn6")))

(define-public crate-kobold_qr-0.6.0 (c (n "kobold_qr") (v "0.6.0") (d (list (d (n "fast_qr") (r "^0.8.5") (d #t) (k 0)) (d (n "kobold") (r "^0.6.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("CanvasRenderingContext2d" "HtmlCanvasElement"))) (d #t) (k 0)))) (h "1mylm9i963y8v78md1q3a5pq9d077b5b3hh3cl779axfmcghdjmq")))

(define-public crate-kobold_qr-0.7.0 (c (n "kobold_qr") (v "0.7.0") (d (list (d (n "fast_qr") (r "^0.8.5") (d #t) (k 0)) (d (n "kobold") (r "^0.7.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("CanvasRenderingContext2d" "HtmlCanvasElement"))) (d #t) (k 0)))) (h "0403q7gb7jh9n9i088s1i0ivgiza6v31fxkp4jij04dr46b8d44r")))

(define-public crate-kobold_qr-0.7.1 (c (n "kobold_qr") (v "0.7.1") (d (list (d (n "fast_qr") (r "^0.8.5") (d #t) (k 0)) (d (n "kobold") (r "^0.7.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("CanvasRenderingContext2d" "HtmlCanvasElement"))) (d #t) (k 0)))) (h "1ld5700nmc3pldgl6bhmspppr5g4swql5qb83n3218xa74zzqdqz")))

(define-public crate-kobold_qr-0.8.0 (c (n "kobold_qr") (v "0.8.0") (d (list (d (n "fast_qr") (r "^0.8.5") (d #t) (k 0)) (d (n "kobold") (r "^0.8.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("CanvasRenderingContext2d" "HtmlCanvasElement"))) (d #t) (k 0)))) (h "13d2h0n27zkia3c53xybmij0842n9ismaj4lznxms55yakyh9v8a")))

(define-public crate-kobold_qr-0.9.0 (c (n "kobold_qr") (v "0.9.0") (d (list (d (n "fast_qr") (r "^0.8.5") (d #t) (k 0)) (d (n "kobold") (r "^0.9.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("CanvasRenderingContext2d" "HtmlCanvasElement"))) (d #t) (k 0)))) (h "1a4sfl5k5s657q6pxh0iwx97qgrdaz4nn3a6rnz68j0gz8z9dipf")))

