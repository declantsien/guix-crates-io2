(define-module (crates-io ko bo kobolt) #:use-module (crates-io))

(define-public crate-kobolt-0.1.0 (c (n "kobolt") (v "0.1.0") (h "0x5ya58v4gchzhqabl8k3c1plcl7551s9z2s6gwidnbj3mqz7pmr")))

(define-public crate-kobolt-0.1.1 (c (n "kobolt") (v "0.1.1") (h "0k85hnwd25yzq4bix5m1qaap24p849bmma71sn2ppzii3ra1nl8m")))

(define-public crate-kobolt-0.1.11 (c (n "kobolt") (v "0.1.11") (h "10idajndlfk7knxjk996r0va2h8yhwriqkq03yq9vh1rs8nzf0j0")))

