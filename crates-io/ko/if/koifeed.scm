(define-module (crates-io ko if koifeed) #:use-module (crates-io))

(define-public crate-koifeed-1.0.0 (c (n "koifeed") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1p8mg0m17wshimd7pdx11784im48fhh2rbv0i2yhjvb8zlhy6884")))

