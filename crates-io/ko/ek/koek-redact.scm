(define-module (crates-io ko ek koek-redact) #:use-module (crates-io))

(define-public crate-koek-redact-0.1.0 (c (n "koek-redact") (v "0.1.0") (h "091015n0ikp9mlvxmxapirnkmgk35fcsqr10hk6a2ayymbknj694")))

(define-public crate-koek-redact-0.2.0 (c (n "koek-redact") (v "0.2.0") (h "0dw7qid0fqfsm2l0yi2habvpm6c18idp15bp693f5ha65zdzjdvj")))

