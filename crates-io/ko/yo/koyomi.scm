(define-module (crates-io ko yo koyomi) #:use-module (crates-io))

(define-public crate-koyomi-0.1.0 (c (n "koyomi") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.1") (d #t) (k 0)))) (h "011mqj8801qjc4y1kkgrrkfjwl2qf7ckibrm8fr48099frgsd0j1")))

(define-public crate-koyomi-0.2.0 (c (n "koyomi") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.1") (d #t) (k 0)))) (h "0xzgjavgd54axfmfkb8dcbswjz1wsg1qybgqwsqzh981scdz85r9")))

(define-public crate-koyomi-0.3.0 (c (n "koyomi") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1k25gc017dlp95vklm97r8mc4qgiklzzlp18xj89va3yb47nadgn")))

(define-public crate-koyomi-0.4.0 (c (n "koyomi") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "12f8svhd3irv4i5fj7npl8vz8d2q1i4g447hn10sq5syia0da03s")))

