(define-module (crates-io ko yo koyomi-rs) #:use-module (crates-io))

(define-public crate-koyomi-rs-0.1.0 (c (n "koyomi-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "rstest") (r "^0.16.0") (d #t) (k 2)))) (h "13hjwib19fng1b0qvsy2k9gwwwlaw2jnnx5vxhydfgh2fqi5hana")))

