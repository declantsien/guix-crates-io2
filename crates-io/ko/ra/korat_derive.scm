(define-module (crates-io ko ra korat_derive) #:use-module (crates-io))

(define-public crate-korat_derive-0.1.0 (c (n "korat_derive") (v "0.1.0") (d (list (d (n "korat") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r ">= 0.3.15") (d #t) (k 0)) (d (n "rusoto_core") (r ">= 0.25.0") (d #t) (k 2)) (d (n "rusoto_dynamodb") (r ">= 0.25.0") (d #t) (k 0)) (d (n "syn") (r ">= 0.11.11") (d #t) (k 0)))) (h "1v66xvlsa3zk29zbw0qmf49j4wjp39bmrhvykf7iincp9xbil3n2")))

(define-public crate-korat_derive-0.1.1 (c (n "korat_derive") (v "0.1.1") (d (list (d (n "korat") (r "^0.1.1") (d #t) (k 0)) (d (n "quote") (r ">= 0.3.15") (d #t) (k 0)) (d (n "rusoto_core") (r ">= 0.25.0") (d #t) (k 2)) (d (n "rusoto_dynamodb") (r ">= 0.25.0") (d #t) (k 0)) (d (n "syn") (r ">= 0.11.11") (d #t) (k 0)))) (h "1bb1jlh47x3z79b01f6rgh77lhp4z2pzhfgzwz914vy2pf6lv92d")))

(define-public crate-korat_derive-0.1.2 (c (n "korat_derive") (v "0.1.2") (d (list (d (n "korat") (r "^0.1.2") (d #t) (k 0)) (d (n "quote") (r ">= 0.3.15") (d #t) (k 0)) (d (n "rusoto_core") (r ">= 0.25.0") (d #t) (k 2)) (d (n "rusoto_dynamodb") (r ">= 0.25.0") (d #t) (k 0)) (d (n "syn") (r ">= 0.11.11") (d #t) (k 0)))) (h "111zp8yfvi9wm367b7wmcvn7kajjjbayqqv8kp4b77qgav753vqx")))

(define-public crate-korat_derive-0.1.3 (c (n "korat_derive") (v "0.1.3") (d (list (d (n "korat") (r "^0.1.2") (d #t) (k 0)) (d (n "quote") (r ">= 0.3.15") (d #t) (k 0)) (d (n "rusoto_core") (r "^0") (d #t) (k 2)) (d (n "rusoto_dynamodb") (r "^0") (d #t) (k 0)) (d (n "syn") (r ">= 0.11.11") (d #t) (k 0)))) (h "0364ij4wmkpjnw24hmb65q4xsppw859388wf498xm5rgiv7ks9rg")))

