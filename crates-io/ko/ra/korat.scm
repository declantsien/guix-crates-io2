(define-module (crates-io ko ra korat) #:use-module (crates-io))

(define-public crate-korat-0.1.0 (c (n "korat") (v "0.1.0") (d (list (d (n "quick-error") (r ">= 1.2.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r ">= 0.25.0") (d #t) (k 0)))) (h "1pqkxry6vbvdwab8akdb35bkqmnpsjhlqi9xliwn62hap4vaniy6")))

(define-public crate-korat-0.1.1 (c (n "korat") (v "0.1.1") (d (list (d (n "quick-error") (r ">= 1.2.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r ">= 0.25.0") (d #t) (k 0)))) (h "05wqp0nh2qsvpkl5ak6afzx8nxinl7z0glyzyzznraymgrw3fynx")))

(define-public crate-korat-0.1.2 (c (n "korat") (v "0.1.2") (d (list (d (n "quick-error") (r ">= 1.2.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r ">= 0.25.0") (d #t) (k 0)))) (h "1mxqnp9b5rmc8wz1zvi1193vapcgm0lmjdxjvr62kbxcs0dn6zdq")))

(define-public crate-korat-0.1.3 (c (n "korat") (v "0.1.3") (d (list (d (n "quick-error") (r ">= 1.2.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r "^0") (d #t) (k 0)))) (h "0ckqck3nn5dhj9b6w47fdirflxln1zvjzd8ckbrjhp7hfzrv5x74")))

(define-public crate-korat-0.2.0 (c (n "korat") (v "0.2.0") (d (list (d (n "quick-error") (r ">= 1.2.0") (d #t) (k 0)) (d (n "rusoto_dynamodb") (r "^0") (d #t) (k 0)))) (h "1rf42dv4zandlidi232wiip787qi94af9lsy2vg17g3h5xd29g50")))

