(define-module (crates-io ko ns konst_kernel) #:use-module (crates-io))

(define-public crate-konst_kernel-0.0.0 (c (n "konst_kernel") (v "0.0.0") (d (list (d (n "arrayvec") (r "^0.7") (k 2)))) (h "1jglsf9kq11njnqwaqnkbi57sx355ii9yx40qgck9c73pdm8cywz") (f (quote (("rust_1_64") ("docsrs") ("debug")))) (r "1.57.0")))

(define-public crate-konst_kernel-0.3.0 (c (n "konst_kernel") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.7") (k 2)))) (h "0pkxc21fn2sl36wvn3hhs9a27wc36s86lvx8cfdbhzk59fckhbl2") (f (quote (("rust_1_64") ("nightly_mut_refs") ("mut_refs") ("iter") ("docsrs") ("debug") ("alloc") ("__for_konst" "rust_1_64")))) (r "1.57.0")))

(define-public crate-konst_kernel-0.3.4 (c (n "konst_kernel") (v "0.3.4") (d (list (d (n "arrayvec") (r "^0.7") (k 2)))) (h "12cvsldjwkhm7g1zny5ns0m4wry6xz3a5aq9m5igwb9rahj6hwbp") (f (quote (("rust_1_64") ("nightly_mut_refs") ("mut_refs") ("iter") ("docsrs") ("debug") ("alloc") ("__for_konst" "rust_1_64")))) (r "1.57.0")))

(define-public crate-konst_kernel-0.3.5 (c (n "konst_kernel") (v "0.3.5") (d (list (d (n "arrayvec") (r "^0.7") (k 2)) (d (n "typewit") (r "^1.1") (d #t) (k 0)))) (h "0v8lm310wiz4n8dysg2w74y3lgkmvjyi5mwyz2fk1rr2c0kapljm") (f (quote (("rust_1_64" "typewit/rust_1_61") ("nightly_mut_refs") ("mut_refs") ("iter") ("docsrs") ("debug") ("alloc") ("__for_konst" "rust_1_64")))) (r "1.57.0")))

(define-public crate-konst_kernel-0.3.6 (c (n "konst_kernel") (v "0.3.6") (d (list (d (n "arrayvec") (r "^0.7") (k 2)) (d (n "typewit") (r "^1.5") (d #t) (k 0)))) (h "1dfqjpdh7hvddgfx1rxjabr0wnl6q9vv0xzba7ajgw1rvhz16xik") (f (quote (("rust_1_64" "typewit/rust_1_61") ("nightly_mut_refs") ("mut_refs") ("iter") ("docsrs") ("debug") ("alloc") ("__for_konst" "rust_1_64")))) (r "1.57.0")))

(define-public crate-konst_kernel-0.3.8 (c (n "konst_kernel") (v "0.3.8") (d (list (d (n "arrayvec") (r "^0.7") (k 2)) (d (n "typewit") (r "^1.5") (d #t) (k 0)))) (h "140pxwbaqzchks1a962d8m99dx7b5glbhffgh6520vkb6y6fmins") (f (quote (("rust_1_64" "typewit/rust_1_61") ("nightly_mut_refs") ("mut_refs") ("iter") ("docsrs") ("debug") ("alloc") ("__for_konst" "rust_1_64")))) (r "1.57.0")))

(define-public crate-konst_kernel-0.3.9 (c (n "konst_kernel") (v "0.3.9") (d (list (d (n "arrayvec") (r "^0.7") (k 2)) (d (n "typewit") (r "^1.5") (d #t) (k 0)))) (h "0ayv7r84zh2wg99j97ka9gqmpa393j70hmppmpb0y8hr2xd4a2my") (f (quote (("rust_1_64" "typewit/rust_1_61") ("nightly_mut_refs") ("mut_refs") ("iter") ("docsrs") ("debug") ("alloc") ("__for_konst" "rust_1_64")))) (r "1.57.0")))

