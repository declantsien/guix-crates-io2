(define-module (crates-io ko ns konst_proc_macros) #:use-module (crates-io))

(define-public crate-konst_proc_macros-0.1.0 (c (n "konst_proc_macros") (v "0.1.0") (h "0cf9ihx7q7i3blwxhchs2wa7cv07zw9ll6dcmk1096hnpm7q5py1")))

(define-public crate-konst_proc_macros-0.2.0 (c (n "konst_proc_macros") (v "0.2.0") (h "15xcba6dqilnc6nj8rqq0izfxj5cqhplb7aa1h0bd3f3783b6vnh")))

(define-public crate-konst_proc_macros-0.2.11 (c (n "konst_proc_macros") (v "0.2.11") (h "0dxp8mdh3q9d044ql203way4fgbc50n3j3pi2j1x2snlcaa10klq")))

(define-public crate-konst_proc_macros-0.3.0 (c (n "konst_proc_macros") (v "0.3.0") (h "0hv07c8b0v8rlcwkf2n1fv46d5ldk8md344c5c6dc2ayqcfsna2f") (r "1.65.0")))

