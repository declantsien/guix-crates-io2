(define-module (crates-io ko ns konst_macro_rules) #:use-module (crates-io))

(define-public crate-konst_macro_rules-0.2.0 (c (n "konst_macro_rules") (v "0.2.0") (h "196dvf2zhbkbfm6fl0mz1f9nfjnrd4yhg742s8xr2cmzk0jpl172")))

(define-public crate-konst_macro_rules-0.2.3 (c (n "konst_macro_rules") (v "0.2.3") (h "1ica1xd2mgkgkwpwsy3513szr435zryb9m2xk4d2yj9z209b3yjx") (f (quote (("deref_raw_in_fn") ("const_generics"))))))

(define-public crate-konst_macro_rules-0.2.4 (c (n "konst_macro_rules") (v "0.2.4") (h "08ykxq2b5pgszja3jk65cccbbxha0brgp71rbdcg9s6yz9r3n2pr") (f (quote (("deref_raw_in_fn") ("const_generics"))))))

(define-public crate-konst_macro_rules-0.2.5 (c (n "konst_macro_rules") (v "0.2.5") (h "0cnwvmk83lnwla7c4kxgnr75haxj1zklg20qwnljq72458f2i853") (f (quote (("deref_raw_in_fn") ("const_generics"))))))

(define-public crate-konst_macro_rules-0.2.7 (c (n "konst_macro_rules") (v "0.2.7") (h "136x6c2flg3rwpw0dzlg38264s6yi7qbpkyiz2jnxyq50s7ndr42") (f (quote (("nightly_mut_refs" "mut_refs") ("mut_refs" "deref_raw_in_fn") ("deref_raw_in_fn") ("const_generics"))))))

(define-public crate-konst_macro_rules-0.2.9 (c (n "konst_macro_rules") (v "0.2.9") (h "1315kychsbak3a9bmlcs3y0cvynkccklp41bl44c9rppv7ls8lpc") (f (quote (("nightly_mut_refs" "mut_refs") ("mut_refs" "deref_raw_in_fn") ("deref_raw_in_fn") ("const_generics"))))))

(define-public crate-konst_macro_rules-0.2.10 (c (n "konst_macro_rules") (v "0.2.10") (h "157q5fsayk3ihgs33napwd146k9ld97driniv2gqya3hz8srr2k7") (f (quote (("rust_1_56") ("rust_1_55") ("nightly_mut_refs") ("mut_refs") ("deref_raw_in_fn") ("const_generics"))))))

(define-public crate-konst_macro_rules-0.2.11 (c (n "konst_macro_rules") (v "0.2.11") (h "1x9z78jn6rmb51d2i6913lf1zf0cq18kj303n1jh6k6k21dxavvl") (f (quote (("rust_1_56") ("rust_1_55") ("nightly_mut_refs") ("mut_refs") ("deref_raw_in_fn") ("const_generics"))))))

(define-public crate-konst_macro_rules-0.2.12 (c (n "konst_macro_rules") (v "0.2.12") (h "0gd44pkpw38jqnhcg43lypxmalljhhj4jlrz8fqkbk7qn9nfl4rq") (f (quote (("rust_1_56") ("rust_1_55") ("nightly_mut_refs") ("mut_refs") ("deref_raw_in_fn") ("const_generics"))))))

(define-public crate-konst_macro_rules-0.2.13 (c (n "konst_macro_rules") (v "0.2.13") (h "1wq8rw5anl4l7bkfh1f9pgl59jca3mra7c7xf2yc0icpigzjkqv6") (f (quote (("rust_1_56") ("rust_1_55") ("nightly_mut_refs") ("mut_refs") ("deref_raw_in_fn") ("const_generics"))))))

(define-public crate-konst_macro_rules-0.2.14 (c (n "konst_macro_rules") (v "0.2.14") (h "05ahyq3ln0pyqzy1gp46wakq0bvqajyy0lc0290xsmm7bbyq6hsn") (f (quote (("rust_1_56") ("rust_1_55") ("nightly_mut_refs") ("mut_refs") ("deref_raw_in_fn") ("const_generics"))))))

(define-public crate-konst_macro_rules-0.2.19 (c (n "konst_macro_rules") (v "0.2.19") (h "0dswja0dqcww4x3fwjnirc0azv2n6cazn8yv0kddksd8awzkz4x4") (f (quote (("rust_1_61") ("rust_1_57") ("rust_1_56") ("rust_1_55") ("rust_1_51") ("nightly_mut_refs") ("mut_refs") ("deref_raw_in_fn"))))))

