(define-module (crates-io ko ns konstruct) #:use-module (crates-io))

(define-public crate-konstruct-0.1.0 (c (n "konstruct") (v "0.1.0") (d (list (d (n "frunk_core") (r "^0.2") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "http") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "07v76l63x8w4cclxrc2brzhr6yahyycdijv5xbz9p5f5z44n4j7j") (f (quote (("strict") ("default")))) (y #t)))

