(define-module (crates-io ko na kona-mpt) #:use-module (crates-io))

(define-public crate-kona-mpt-0.0.1 (c (n "kona-mpt") (v "0.0.1") (d (list (d (n "alloy-primitives") (r "^0.7.0") (f (quote ("rlp"))) (k 0)) (d (n "alloy-rlp") (r "^0.3.4") (k 0)) (d (n "alloy-trie") (r "^0.3.1") (k 0)) (d (n "anyhow") (r "^1.0.79") (k 0)) (d (n "reqwest") (r "^0.12") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 2)))) (h "1ar7s05xcmgdz3c3fwpqdga7chflxffkzhagq8vmxngvfk4gl2y0")))

