(define-module (crates-io ko na kona-common-proc) #:use-module (crates-io))

(define-public crate-kona-common-proc-0.0.1 (c (n "kona-common-proc") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.79") (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "kona-common") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1b8dp6q427gdikyxydf656d8j8my70a16krdavhimxk6cy641376")))

