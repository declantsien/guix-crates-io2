(define-module (crates-io ko na kona-preimage) #:use-module (crates-io))

(define-public crate-kona-preimage-0.0.1 (c (n "kona-preimage") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.79") (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "kona-common") (r "^0.0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.0") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0smf8bjzp8w7ziym17p9f6l72aa8kaph2683n3s3faplv2lwxif9")))

