(define-module (crates-io ko na kona-common) #:use-module (crates-io))

(define-public crate-kona-common-0.0.1 (c (n "kona-common") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.79") (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.10.5") (d #t) (k 0)))) (h "1gs5b5bkw0mcjmr9y32rbc2w1k3qb69v9ghj8zr899dggzlgkkmx")))

