(define-module (crates-io ko lb kolben) #:use-module (crates-io))

(define-public crate-kolben-0.0.1 (c (n "kolben") (v "0.0.1") (d (list (d (n "postcard-cobs") (r "^0.1.5-pre") (d #t) (k 0)) (d (n "rcobs") (r "^0.1.1") (d #t) (k 0)) (d (n "rzcobs") (r "^0.1.1") (d #t) (k 0)))) (h "15ryh6yqlmwyg8cfzgg349xcl6gqj8y0dqfwxi3wgqi6ww0gy3a9")))

(define-public crate-kolben-0.0.2 (c (n "kolben") (v "0.0.2") (d (list (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "postcard-cobs") (r "^0.1.5-pre") (k 0)) (d (n "rcobs") (r "^0.1.1") (k 0)) (d (n "rzcobs") (r "^0.1.1") (k 0)))) (h "0qk76vkv6dq1z6xsybyybrw65j3k7j4fkbl7dwhqckykr6n5bp8r") (f (quote (("use-std" "postcard-cobs/use_std" "rcobs/std" "rzcobs/std") ("default" "use-std"))))))

(define-public crate-kolben-0.0.3 (c (n "kolben") (v "0.0.3") (d (list (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "postcard-cobs") (r "^0.1.5-pre") (k 0)) (d (n "rcobs") (r "^0.1.1") (k 0)) (d (n "rzcobs") (r "^0.1.1") (k 0)))) (h "1nnrjw7vmfcn1djw19ws9f7zlj3xmmfscnixkgvypnmpbc5w440z") (f (quote (("use-std" "postcard-cobs/use_std" "rcobs/std" "rzcobs/std") ("default" "use-std"))))))

