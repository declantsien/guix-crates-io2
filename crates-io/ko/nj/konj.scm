(define-module (crates-io ko nj konj) #:use-module (crates-io))

(define-public crate-konj-0.1.0 (c (n "konj") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "indexmap") (r "^1.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0xzykrc19dfz3nbgdv7f1py5d0fjvwghm087w46sa3l9c70f531j")))

