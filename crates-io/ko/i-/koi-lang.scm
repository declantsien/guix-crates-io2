(define-module (crates-io ko i- koi-lang) #:use-module (crates-io))

(define-public crate-koi-lang-0.1.0 (c (n "koi-lang") (v "0.1.0") (h "19fasmh7q7c0x90wyi1z1zmscl69szl3w7wzkggmv0ak022lr2kz")))

(define-public crate-koi-lang-0.1.2 (c (n "koi-lang") (v "0.1.2") (h "0ij5ac9w63mklwnvhjn66zpq8bjzn18rgj1lb4z74v757acm397r")))

(define-public crate-koi-lang-0.1.3 (c (n "koi-lang") (v "0.1.3") (h "1qm15dg6ghv3243v1nndfhwk2x2q6jmh798ap098jks105i36wsi")))

