(define-module (crates-io ko i- koi-lexer) #:use-module (crates-io))

(define-public crate-koi-lexer-0.1.0 (c (n "koi-lexer") (v "0.1.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "1030brrdv86zh7bdkxvjqa5d913mr42bhhm826gl4wyw1373y6cn")))

(define-public crate-koi-lexer-0.2.0 (c (n "koi-lexer") (v "0.2.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "0nis8aicf3245rr70vqgxy3qdp6ci8kchy5pxhsk9ww0dj666ngb")))

(define-public crate-koi-lexer-0.3.0-alpha (c (n "koi-lexer") (v "0.3.0-alpha") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "0inzavimx4yh5s3wsabb8xrs9001vh3798lhc0r0jchvvzcff028")))

(define-public crate-koi-lexer-0.3.0-alpha.1 (c (n "koi-lexer") (v "0.3.0-alpha.1") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "0nw01pg11zn0z7r8agpwinjk3d7jzsz730x02zyldcwcswmsw8ig")))

(define-public crate-koi-lexer-0.3.0-alpha.2 (c (n "koi-lexer") (v "0.3.0-alpha.2") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "0xmpnbapwxw4m7slyhhm8hl89nkd68q8iqycnh76dp2py2jpn30j")))

(define-public crate-koi-lexer-0.3.0-alpha.3 (c (n "koi-lexer") (v "0.3.0-alpha.3") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "0dif7ymaaibvip9c62fpjysp0zqv4skl1l09czh0pi1d9rjf4k79")))

(define-public crate-koi-lexer-0.3.0-alpha.4 (c (n "koi-lexer") (v "0.3.0-alpha.4") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 0)))) (h "1x4phr1wj9hb0imdfa36phj7gfa4jldm1qw4p4z0s3hanmkg6776")))

