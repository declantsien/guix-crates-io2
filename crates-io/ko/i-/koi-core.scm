(define-module (crates-io ko i- koi-core) #:use-module (crates-io))

(define-public crate-koi-core-0.1.0-alpha.2 (c (n "koi-core") (v "0.1.0-alpha.2") (d (list (d (n "error-chain") (r "^0.11") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "1f8xmzmqhcip0vispn4xx8wchkviq625l00gvfhl0vw6lb307fcp") (y #t)))

(define-public crate-koi-core-0.1.0-alpha.3 (c (n "koi-core") (v "0.1.0-alpha.3") (d (list (d (n "error-chain") (r "^0.11") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "1pg3gmlqbqkz39pmdwjrw4054kz3i489ardfvjq0gkl20gygy9y2") (y #t)))

(define-public crate-koi-core-0.1.0 (c (n "koi-core") (v "0.1.0") (h "1jr5bdj3873yvb7d98vf6m341zfdvyp1d3aha7fv970g1wfhjj4b") (y #t)))

