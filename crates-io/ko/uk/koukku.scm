(define-module (crates-io ko uk koukku) #:use-module (crates-io))

(define-public crate-koukku-0.1.0 (c (n "koukku") (v "0.1.0") (d (list (d (n "clap") (r "^2.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.7.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.7.5") (d #t) (k 0)) (d (n "rust-ini") (r "^0.9.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)))) (h "09csgl1ygzhasi4nnx03nszc3m1l9vvw585ik797kyjg8qbbc0b3")))

(define-public crate-koukku-0.1.1 (c (n "koukku") (v "0.1.1") (d (list (d (n "clap") (r "^2.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.7.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "openssl") (r "^0.7.5") (d #t) (k 0)) (d (n "rust-ini") (r "^0.9.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)))) (h "17ya07l3xhdg67nvl1lyr1sxkbn24ssmlxwq8v91hiyq2wy1kjrh")))

