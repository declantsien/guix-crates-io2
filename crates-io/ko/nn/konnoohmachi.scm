(define-module (crates-io ko nn konnoohmachi) #:use-module (crates-io))

(define-public crate-konnoohmachi-0.1.4 (c (n "konnoohmachi") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.0") (d #t) (k 2)) (d (n "itertools-num") (r "^0.1.1") (d #t) (k 2)) (d (n "pyo3") (r "^0.15.0") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)))) (h "0f5752jclzxxx1yfgnlc0qcakqg9f9inx9hdsplclimvwwkhl6hy")))

(define-public crate-konnoohmachi-0.1.5 (c (n "konnoohmachi") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.0") (d #t) (k 2)) (d (n "itertools-num") (r "^0.1.1") (d #t) (k 2)) (d (n "pyo3") (r "^0.15.0") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)))) (h "1z8iczcynb3nqc59cn98s1y7h5hfmxjvf1xxra5a745gws4q2plr")))

(define-public crate-konnoohmachi-0.2.0 (c (n "konnoohmachi") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "numpy") (r "^0.15") (d #t) (k 0)) (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "000xzpl1sknglk3c4y97sdkpk32cqcpzkrxjqhhb1jvvip9dx3b0")))

(define-public crate-konnoohmachi-1.0.0 (c (n "konnoohmachi") (v "1.0.0") (d (list (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "numpy") (r "^0.15") (d #t) (k 0)) (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "1klwzdxyd7l7bprcdw1dwfxcwycnhjsak640p6k29lvdy2anspz1")))

