(define-module (crates-io ko ib koibumi-node-sync) #:use-module (crates-io))

(define-public crate-koibumi-node-sync-0.0.0 (c (n "koibumi-node-sync") (v "0.0.0") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "koibumi-core") (r "^0.0.8") (d #t) (k 0)) (d (n "koibumi-net") (r "^0.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "koibumi-socks-sync") (r "^0.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)) (d (n "rusqlite") (r "^0.24.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)))) (h "1klaapx48jq2x262r6mdgbavm6g4n51v5p3s5fh9lpaz5gi8cir3")))

