(define-module (crates-io ko ib koibumi-common-sync) #:use-module (crates-io))

(define-public crate-koibumi-common-sync-0.0.0 (c (n "koibumi-common-sync") (v "0.0.0") (d (list (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "koibumi-box-sync") (r "^0.0.0") (d #t) (k 0)) (d (n "koibumi-node-sync") (r "^0.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "rusqlite") (r "^0.24.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0w81yk1c1fxzhac60054myx6hclb131bqjgl0hig1icflc95smb0")))

