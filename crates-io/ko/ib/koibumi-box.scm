(define-module (crates-io ko ib koibumi-box) #:use-module (crates-io))

(define-public crate-koibumi-box-0.0.0 (c (n "koibumi-box") (v "0.0.0") (d (list (d (n "koibumi-core") (r "^0.0.4") (d #t) (k 0)) (d (n "koibumi-node") (r "^0.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "sqlx") (r "^0.4.0-beta.1") (f (quote ("sqlite"))) (d #t) (k 0)))) (h "0yd1p2i8n23gpcb68iskzkm4f1jiip8lszxg8h14c56a6mr0wxcr")))

(define-public crate-koibumi-box-0.0.1 (c (n "koibumi-box") (v "0.0.1") (d (list (d (n "koibumi-core") (r "^0.0.5") (d #t) (k 0)) (d (n "koibumi-node") (r "^0.0.5") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "sqlx") (r "^0.4.0-beta.1") (f (quote ("sqlite"))) (d #t) (k 0)))) (h "15z86mwvkyqq45pxgbxzwh5wk9wgsg30n4dyaj7insk1k0xlmyc5")))

(define-public crate-koibumi-box-0.0.2 (c (n "koibumi-box") (v "0.0.2") (d (list (d (n "koibumi-core") (r "^0.0.6") (d #t) (k 0)) (d (n "koibumi-node") (r "^0.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "sqlx") (r "^0.4.0-beta.1") (f (quote ("macros" "runtime-async-std" "sqlite"))) (k 0)))) (h "0dwff3zbsjfa1zqwrbjkq43qg1kln3ah8vvrafvwghm8wg84rm9h")))

(define-public crate-koibumi-box-0.0.3 (c (n "koibumi-box") (v "0.0.3") (d (list (d (n "koibumi-core") (r "^0.0.7") (d #t) (k 0)) (d (n "koibumi-node") (r "^0.0.7") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "sqlx") (r "^0.4.0-beta.1") (f (quote ("macros" "runtime-async-std" "sqlite"))) (k 0)))) (h "1vrn9qpyhykm3xwj93yhy2a6aqdw95bi7j8jhz8c577bdawdx7wl")))

(define-public crate-koibumi-box-0.0.4 (c (n "koibumi-box") (v "0.0.4") (d (list (d (n "koibumi-core") (r "^0.0.8") (d #t) (k 0)) (d (n "koibumi-node") (r "^0.0.8") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "sqlx") (r "^0.4.1") (f (quote ("runtime-async-std-native-tls" "sqlite"))) (d #t) (k 0)))) (h "1zzx5d9wx2s2ki9l7j02hccy4xv0kpl7in2bdk65wq5rnq307gsb")))

