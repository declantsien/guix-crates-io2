(define-module (crates-io ko ib koibumi-daemon-sync) #:use-module (crates-io))

(define-public crate-koibumi-daemon-sync-0.0.0 (c (n "koibumi-daemon-sync") (v "0.0.0") (d (list (d (n "ctrlc") (r "^3.1.4") (f (quote ("termination"))) (o #t) (d #t) (k 0)) (d (n "koibumi-box-sync") (r "^0.0.0") (d #t) (k 0)) (d (n "koibumi-common-sync") (r "^0.0.0") (d #t) (k 0)) (d (n "koibumi-core") (r "^0.0.8") (d #t) (k 0)) (d (n "koibumi-node-sync") (r "^0.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "021pqqhqhfrhik3dbafq1rabyp05hq9zcpbbagi9kw9mv253yiaf") (f (quote (("default"))))))

