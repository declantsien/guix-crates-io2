(define-module (crates-io ko ib koibumi-base32) #:use-module (crates-io))

(define-public crate-koibumi-base32-0.0.0 (c (n "koibumi-base32") (v "0.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0wdjgls8p54mzfcv2nlyha47kxly6nvzm46si3r8b73z7bf10bhk")))

(define-public crate-koibumi-base32-0.0.1 (c (n "koibumi-base32") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "10mpvjvlc9yal3jjr98zyrsn9nra0lp2dlqqxlpmcnnkvcag91xn")))

(define-public crate-koibumi-base32-0.0.2 (c (n "koibumi-base32") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1frb232m6rw50ly8ijhsbllcqfif5kcma1l6hrgm5g22c3c3il94")))

