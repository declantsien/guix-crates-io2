(define-module (crates-io ko ib koibumi-base58) #:use-module (crates-io))

(define-public crate-koibumi-base58-0.0.0 (c (n "koibumi-base58") (v "0.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.0") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.43") (k 0)) (d (n "num-traits") (r "^0.2.12") (k 0)))) (h "1v880d26h9vc05aj181ghsk1qdj73rwhv0n516590fm6xwgchymy")))

(define-public crate-koibumi-base58-0.0.1 (c (n "koibumi-base58") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.0") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.43") (k 0)) (d (n "num-traits") (r "^0.2.12") (k 0)))) (h "1mi40c5w25vmmk5g2y02zlnrmm36q5glm7mdhdr66pcky4sp0dng")))

