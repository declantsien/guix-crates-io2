(define-module (crates-io ko ib koibumi-socks) #:use-module (crates-io))

(define-public crate-koibumi-socks-0.0.0 (c (n "koibumi-socks") (v "0.0.0") (d (list (d (n "async-std") (r "^1.5.0") (d #t) (k 0)))) (h "157kf4q06p55sgnwm4k5j7m37ahi8k8z4g94wr5j2khgwa8jc240")))

(define-public crate-koibumi-socks-0.0.1 (c (n "koibumi-socks") (v "0.0.1") (d (list (d (n "async-std") (r "^1.5.0") (d #t) (k 0)))) (h "1ik43xwh1l9h04zyiixax6crs9hy1gcw8qr3y7yjmkir8kc3llqx")))

(define-public crate-koibumi-socks-0.0.2 (c (n "koibumi-socks") (v "0.0.2") (d (list (d (n "async-std") (r "^1.5.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)))) (h "0z74vv66nndrkdm6wp1krkn6cjpfpcjymjpfw4wfzl4w0mhvbnyr")))

(define-public crate-koibumi-socks-0.0.3 (c (n "koibumi-socks") (v "0.0.3") (d (list (d (n "async-std") (r "^1.5.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "koibumi-socks-core") (r "^0.0.0") (d #t) (k 0)))) (h "1pqv0znvr5aqngd78q53ghwjlnprsvhaffwxn8ym7m9qkjjggp2b")))

(define-public crate-koibumi-socks-0.0.4 (c (n "koibumi-socks") (v "0.0.4") (d (list (d (n "async-std") (r "^1.5.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "koibumi-socks-core") (r "^0.0.1") (d #t) (k 0)))) (h "0bjpmkwgkvvnwr0pilibjn61i0r2qzc3w4z5hh3px77nnzjag720")))

(define-public crate-koibumi-socks-0.0.5 (c (n "koibumi-socks") (v "0.0.5") (d (list (d (n "async-std") (r "^1.5.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "koibumi-net") (r "^0.0.0") (d #t) (k 0)))) (h "1icg6y3lda6fg16asb9czwcyvjs9j51w85lycyqik7xv7sxf6qd3")))

