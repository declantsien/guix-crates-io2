(define-module (crates-io ko to koto_regex) #:use-module (crates-io))

(define-public crate-koto_regex-0.13.0 (c (n "koto_regex") (v "0.13.0") (d (list (d (n "koto_runtime") (r "^0.13.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0p5f5bh244sb0yj4zjsqi2qk7axylslm5lzqy434xvch995aw89f")))

(define-public crate-koto_regex-0.14.0 (c (n "koto_regex") (v "0.14.0") (d (list (d (n "koto_runtime") (r "^0.14.0") (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0d1qyzrfnmbam0rx2lsia0ps5ddh6wq1x9zgp23fh2vwmvn2ff4x") (f (quote (("rc" "koto_runtime/rc") ("default" "arc") ("arc" "koto_runtime/arc"))))))

