(define-module (crates-io ko to koto_toml) #:use-module (crates-io))

(define-public crate-koto_toml-0.1.0 (c (n "koto_toml") (v "0.1.0") (d (list (d (n "koto_runtime") (r "^0.1.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "00f1z4jwqz936x3mnrqgaalswavmffwb2bd2js920zydz0pfypns")))

(define-public crate-koto_toml-0.2.0 (c (n "koto_toml") (v "0.2.0") (d (list (d (n "koto_runtime") (r "^0.2.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.2.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1m4g03vwmci2v2730p29bjdqn7gfl3l7vngvfi8im63lbh3jvkr6")))

(define-public crate-koto_toml-0.3.0 (c (n "koto_toml") (v "0.3.0") (d (list (d (n "koto_runtime") (r "^0.3.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0cdvpjrkmyvvwgw87731h8l7f635djgr0g3yh2lqvqqrlfnja33j")))

(define-public crate-koto_toml-0.4.0 (c (n "koto_toml") (v "0.4.0") (d (list (d (n "koto_runtime") (r "^0.4.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.4.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1brgna9p7ybsbs70v28b8y122x96gq9294b51kqrmx7x5z9j8hwh")))

(define-public crate-koto_toml-0.5.0 (c (n "koto_toml") (v "0.5.0") (d (list (d (n "koto_runtime") (r "^0.5.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.5.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "05ak451rczzrkxh291s9r53vjc0imxzbsklcli2zblrw269l3fic")))

(define-public crate-koto_toml-0.6.0 (c (n "koto_toml") (v "0.6.0") (d (list (d (n "koto_runtime") (r "^0.6.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1gvgwhi6lwdbw26a20z59al9mlijz0cqa0m02lnldixfzdm1kqly")))

(define-public crate-koto_toml-0.7.0 (c (n "koto_toml") (v "0.7.0") (d (list (d (n "koto_runtime") (r "^0.7.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.7.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "16fd67apwwgv1pww7rdc1p7b80n6p4dph58zrqwpn0px16lgd2gk")))

(define-public crate-koto_toml-0.8.0 (c (n "koto_toml") (v "0.8.0") (d (list (d (n "koto_runtime") (r "^0.8.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.8.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "1y261qa94jbga131ajp10zx5barfplky8rvqisqyr39gxxsj8vyz")))

(define-public crate-koto_toml-0.8.1 (c (n "koto_toml") (v "0.8.1") (d (list (d (n "koto_runtime") (r "^0.8.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.8.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0013fcni6b038k3f23spy2krw18jg2r6dpb7aaxjzgwvmlml9rq2")))

(define-public crate-koto_toml-0.9.0 (c (n "koto_toml") (v "0.9.0") (d (list (d (n "koto_runtime") (r "^0.9.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.9.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "15p1nmwzx6zwxhkra0md9jk6prc1rhanxrwc51cgzlnz2psm8xlq")))

(define-public crate-koto_toml-0.9.1 (c (n "koto_toml") (v "0.9.1") (d (list (d (n "koto_runtime") (r "^0.9.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.9.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "07xxfa9rq970zy7rvr3yp9q1mx4nanc0wy8y5l8bm84av134sp9q")))

(define-public crate-koto_toml-0.10.0 (c (n "koto_toml") (v "0.10.0") (d (list (d (n "koto_runtime") (r "^0.10.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.10.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0janpwa7143dq2jmwvimby10cqxa2di9hqg07y62l7371gak5hfl")))

(define-public crate-koto_toml-0.11.0 (c (n "koto_toml") (v "0.11.0") (d (list (d (n "koto_runtime") (r "^0.11.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.11.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "03888kvq3k6wbc5aw0fpsr99k6shsvgxx7n43hvcblff60r3q9d9")))

(define-public crate-koto_toml-0.12.0 (c (n "koto_toml") (v "0.12.0") (d (list (d (n "koto_runtime") (r "^0.12.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.12.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "006g6hb7gvg0y08lzg5rmf7cigrnkg1i8hxd6rk5g02shcwyb9b5")))

(define-public crate-koto_toml-0.13.0 (c (n "koto_toml") (v "0.13.0") (d (list (d (n "koto_runtime") (r "^0.13.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.13.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0k8nr7gxsw5s4p86gi84g9z687g4f4gx1y316v0bb4bws09bpafi")))

(define-public crate-koto_toml-0.14.0 (c (n "koto_toml") (v "0.14.0") (d (list (d (n "koto_runtime") (r "^0.14.0") (k 0)) (d (n "koto_serialize") (r "^0.14.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "16vlx2dlrhg68yg2vnjp3zvx70npcpwmyrq8cj3h2avvcpcyf9a2") (f (quote (("rc" "koto_runtime/rc") ("default" "arc") ("arc" "koto_runtime/arc"))))))

