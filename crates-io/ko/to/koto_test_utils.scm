(define-module (crates-io ko to koto_test_utils) #:use-module (crates-io))

(define-public crate-koto_test_utils-0.14.0 (c (n "koto_test_utils") (v "0.14.0") (d (list (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "koto_bytecode") (r "^0.14.0") (k 0)) (d (n "koto_runtime") (r "^0.14.0") (k 0)) (d (n "pulldown-cmark") (r "^0.9.1") (k 0)))) (h "0zwxrqwr1l2kp693xik3pc6wh454fiik6hdbcwjj8j5ddpzbf2xl") (f (quote (("rc" "koto_runtime/rc") ("default" "arc") ("arc" "koto_runtime/arc"))))))

