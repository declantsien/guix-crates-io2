(define-module (crates-io ko to koto_bytecode) #:use-module (crates-io))

(define-public crate-koto_bytecode-0.1.0 (c (n "koto_bytecode") (v "0.1.0") (d (list (d (n "koto_parser") (r "^0.1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "1wjgbgpyimj9jh2djq75zf65gcfskw3009hcb12ls60g8x91lfz6")))

(define-public crate-koto_bytecode-0.2.0 (c (n "koto_bytecode") (v "0.2.0") (d (list (d (n "koto_parser") (r "^0.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "1n3cync8j7fpd9xrjn0g8fb9mdyzkwz5wy8vd0si31kkrghgdx24")))

(define-public crate-koto_bytecode-0.3.0 (c (n "koto_bytecode") (v "0.3.0") (d (list (d (n "koto_parser") (r "^0.3.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "0r6mmia573na27y72f7my30sc92sgn9micjwv40syqjz636d1h29")))

(define-public crate-koto_bytecode-0.4.0 (c (n "koto_bytecode") (v "0.4.0") (d (list (d (n "koto_parser") (r "^0.4.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "13mdfhfg69jxqfl315x777s0gssky7f485gjq133hmyi73sqnp8v")))

(define-public crate-koto_bytecode-0.5.0 (c (n "koto_bytecode") (v "0.5.0") (d (list (d (n "koto_parser") (r "^0.5.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "0k2cvz8vnv97qqjs3n7l6acpjqzcaqjk0jl13y71gvqwdkj2gx4g")))

(define-public crate-koto_bytecode-0.6.0 (c (n "koto_bytecode") (v "0.6.0") (d (list (d (n "koto_parser") (r "^0.6.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "1pszss3l64mcbhyf079ik72c9ifzn6caawb1ql3jw3xyadm3mn3p")))

(define-public crate-koto_bytecode-0.7.0 (c (n "koto_bytecode") (v "0.7.0") (d (list (d (n "koto_parser") (r "^0.7.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "18431yfbgvjfzhf2lk7w7wjgfybp3nxsykgv4j2lmzdbrvh894y9")))

(define-public crate-koto_bytecode-0.8.0 (c (n "koto_bytecode") (v "0.8.0") (d (list (d (n "koto_parser") (r "^0.8.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "0jfdqlj2dssqlni65srr6m96gl7ypxiind9yl86di8anypgkvqzw")))

(define-public crate-koto_bytecode-0.8.1 (c (n "koto_bytecode") (v "0.8.1") (d (list (d (n "koto_parser") (r "^0.8.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "1d6zrc7xcn9rgqiv7wqx8dljc4lyk1r18yx9vhm1anl9lchcbgfq")))

(define-public crate-koto_bytecode-0.9.0 (c (n "koto_bytecode") (v "0.9.0") (d (list (d (n "dunce") (r "^1.0.2") (d #t) (k 0)) (d (n "koto_parser") (r "^0.9.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "0xj5x8p47m77aqphp49vhnj13rc5wjdw6c8h19y7nl3vchlyccql")))

(define-public crate-koto_bytecode-0.9.1 (c (n "koto_bytecode") (v "0.9.1") (d (list (d (n "dunce") (r "^1.0.2") (d #t) (k 0)) (d (n "koto_parser") (r "^0.9.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "1z24bhpf4dln4905k8dywcjxdyrd9pl7s3dspfr8xn4v758xc59k")))

(define-public crate-koto_bytecode-0.10.0 (c (n "koto_bytecode") (v "0.10.0") (d (list (d (n "dunce") (r "^1.0.2") (d #t) (k 0)) (d (n "koto_parser") (r "^0.10.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "0plak5hwdmjxwnnsnrsayhgw9hr77zn4x0b09g8znb0817p6q87c")))

(define-public crate-koto_bytecode-0.11.0 (c (n "koto_bytecode") (v "0.11.0") (d (list (d (n "dunce") (r "^1.0.2") (d #t) (k 0)) (d (n "koto_parser") (r "^0.11.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.8.0") (f (quote ("const_generics" "union"))) (d #t) (k 0)))) (h "090hs6hjnbs2sm03q61rmdczz1xs0ngggn0bqlyxsf234w3582qv") (r "1.58.1")))

(define-public crate-koto_bytecode-0.12.0 (c (n "koto_bytecode") (v "0.12.0") (d (list (d (n "dunce") (r "^1.0.2") (d #t) (k 0)) (d (n "koto_memory") (r "^0.12.0") (d #t) (k 0)) (d (n "koto_parser") (r "^0.12.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.11.1") (f (quote ("const_generics" "union"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1a03iryqqz9l3hsfg53n69x7jk6200flajazcb4aad3gkxdl6vrd") (f (quote (("default"))))))

(define-public crate-koto_bytecode-0.13.0 (c (n "koto_bytecode") (v "0.13.0") (d (list (d (n "dunce") (r "^1.0.2") (d #t) (k 0)) (d (n "koto_memory") (r "^0.13.0") (k 0)) (d (n "koto_parser") (r "^0.13.0") (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.11.1") (f (quote ("const_generics" "union"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "066a0csaywbsck1q50b8dhwjjwc2zyypgiv927bf7838yfa142c7") (f (quote (("rc" "koto_memory/rc") ("default" "arc") ("arc" "koto_memory/arc"))))))

(define-public crate-koto_bytecode-0.14.0 (c (n "koto_bytecode") (v "0.14.0") (d (list (d (n "dunce") (r "^1.0.2") (d #t) (k 0)) (d (n "koto_memory") (r "^0.14.0") (k 0)) (d (n "koto_parser") (r "^0.14.0") (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.11.1") (f (quote ("const_generics" "union"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "197kl971pyf7251740x8804lswn46nzqdwc3chzh2915n87pb568") (f (quote (("rc" "koto_memory/rc") ("default" "arc") ("arc" "koto_memory/arc"))))))

