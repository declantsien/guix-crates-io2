(define-module (crates-io ko to koto_yaml) #:use-module (crates-io))

(define-public crate-koto_yaml-0.9.0 (c (n "koto_yaml") (v "0.9.0") (d (list (d (n "koto_runtime") (r "^0.9.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (f (quote ("preserve_order" "std"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.20") (d #t) (k 0)))) (h "16x8m2jpl5bixar0h80grwskdysjjfdk16hs4yx5l7lzp0pyyp23")))

(define-public crate-koto_yaml-0.9.1 (c (n "koto_yaml") (v "0.9.1") (d (list (d (n "koto_runtime") (r "^0.9.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (f (quote ("preserve_order" "std"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.20") (d #t) (k 0)))) (h "1n6xpyfmklh3vqciyrhhv9cxcg6azbzs7c395qjklil7h8sg8q09")))

(define-public crate-koto_yaml-0.10.0 (c (n "koto_yaml") (v "0.10.0") (d (list (d (n "koto_runtime") (r "^0.10.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.10.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (f (quote ("preserve_order" "std"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.20") (d #t) (k 0)))) (h "0irhasdcsmsbs736cxklsggljwbyn94gvc6djaslzjwmj258gfl4")))

(define-public crate-koto_yaml-0.11.0 (c (n "koto_yaml") (v "0.11.0") (d (list (d (n "koto_runtime") (r "^0.11.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.11.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (f (quote ("preserve_order" "std"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.20") (d #t) (k 0)))) (h "17ci1pi7jjr9nn9wfsmzpaxyp0mkdpnqj5myhyw8ar22z5k3621z")))

(define-public crate-koto_yaml-0.12.0 (c (n "koto_yaml") (v "0.12.0") (d (list (d (n "koto_runtime") (r "^0.12.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.12.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.20") (d #t) (k 0)))) (h "1b262f6bg18rgh1jzn373imvhnji1bw2fp0l8ljy34b30qvhq7hc")))

(define-public crate-koto_yaml-0.13.0 (c (n "koto_yaml") (v "0.13.0") (d (list (d (n "koto_runtime") (r "^0.13.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.13.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.20") (d #t) (k 0)))) (h "0vl7r0yz1r20fv14sip7rn8ja80h24cz02hzhmy2x5pzav5z42g1")))

(define-public crate-koto_yaml-0.14.0 (c (n "koto_yaml") (v "0.14.0") (d (list (d (n "koto_runtime") (r "^0.14.0") (k 0)) (d (n "koto_serialize") (r "^0.14.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.20") (d #t) (k 0)))) (h "0llz823z1zwzizwdvxycys63rxckjsbcfgmfdjpcf3lczqjhz74a") (f (quote (("rc" "koto_runtime/rc") ("default" "arc") ("arc" "koto_runtime/arc"))))))

