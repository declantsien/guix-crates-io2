(define-module (crates-io ko to koto_parser) #:use-module (crates-io))

(define-public crate-koto_parser-0.1.0 (c (n "koto_parser") (v "0.1.0") (d (list (d (n "koto_lexer") (r "^0.1.0") (d #t) (k 0)))) (h "06glzxj1smx5z6rxn95hgjliz8bva23l79qai2aq7ir9vryd1jr7") (f (quote (("panic_on_parser_error") ("default"))))))

(define-public crate-koto_parser-0.2.0 (c (n "koto_parser") (v "0.2.0") (d (list (d (n "koto_lexer") (r "^0.2.0") (d #t) (k 0)))) (h "0fh9zxrr14zvwapbgr1q5vva2vy50ah1cfz31nwyhblgby2dp0lg") (f (quote (("panic_on_parser_error") ("default"))))))

(define-public crate-koto_parser-0.3.0 (c (n "koto_parser") (v "0.3.0") (d (list (d (n "koto_lexer") (r "^0.3.0") (d #t) (k 0)))) (h "03r8a9l4ya0zz4xq6r4vz3fkrf7axc86c0y9g6dh48bf2j0pkhkh") (f (quote (("panic_on_parser_error") ("default"))))))

(define-public crate-koto_parser-0.4.0 (c (n "koto_parser") (v "0.4.0") (d (list (d (n "koto_lexer") (r "^0.4.0") (d #t) (k 0)))) (h "1a6awgcq0wcyiwh0nzx9qydbzmnvw8qh9gw0s0w09n3l2ww5nnd3") (f (quote (("panic_on_parser_error") ("default"))))))

(define-public crate-koto_parser-0.5.0 (c (n "koto_parser") (v "0.5.0") (d (list (d (n "koto_lexer") (r "^0.5.0") (d #t) (k 0)))) (h "19ama6ravv0gw1ad75mpmla9c3nd86vj8hwkf6w9lcv48q6ipm0d") (f (quote (("panic_on_parser_error") ("default"))))))

(define-public crate-koto_parser-0.6.0 (c (n "koto_parser") (v "0.6.0") (d (list (d (n "koto_lexer") (r "^0.6.0") (d #t) (k 0)))) (h "0h6hxfsni7989di19dh84c4k77pnzz2jjrnd9z8mphsqdawpfpav") (f (quote (("panic_on_parser_error") ("default"))))))

(define-public crate-koto_parser-0.7.0 (c (n "koto_parser") (v "0.7.0") (d (list (d (n "koto_lexer") (r "^0.7.0") (d #t) (k 0)))) (h "1indy62znxffbjwsmi059vzy83apdinx89pswmlyj8mzjvp3yrib") (f (quote (("panic_on_parser_error") ("default"))))))

(define-public crate-koto_parser-0.8.0 (c (n "koto_parser") (v "0.8.0") (d (list (d (n "koto_lexer") (r "^0.8.0") (d #t) (k 0)))) (h "0k95f69174rfqr4zj3bjlkyrhh6f1n0ikyh53qz4xfp64311h2qs") (f (quote (("panic_on_parser_error") ("default"))))))

(define-public crate-koto_parser-0.8.1 (c (n "koto_parser") (v "0.8.1") (d (list (d (n "koto_lexer") (r "^0.8.0") (d #t) (k 0)))) (h "0nync6538yp6vmwk8h72a1q1h6ggv2fqzikndvd4q0xwmxb9qlhw") (f (quote (("panic_on_parser_error") ("default"))))))

(define-public crate-koto_parser-0.9.0 (c (n "koto_parser") (v "0.9.0") (d (list (d (n "koto_lexer") (r "^0.9.0") (d #t) (k 0)))) (h "1gwws6vd4ss5xwirp39gis45ix9dwcy5n71h9w42xrs2hap5ggfd") (f (quote (("panic_on_parser_error") ("default"))))))

(define-public crate-koto_parser-0.9.1 (c (n "koto_parser") (v "0.9.1") (d (list (d (n "koto_lexer") (r "^0.9.0") (d #t) (k 0)))) (h "0gl8962kj5xj5bvvz2iwd72hdq7zbxq0akhhwkndxap67ppzldkl") (f (quote (("panic_on_parser_error") ("default"))))))

(define-public crate-koto_parser-0.10.0 (c (n "koto_parser") (v "0.10.0") (d (list (d (n "koto_lexer") (r "^0.10.0") (d #t) (k 0)))) (h "0hkcvfsnzqm086mzs1fkmdah009gi98c3sck17p4ff2g2ajb68wl") (f (quote (("panic_on_parser_error") ("default"))))))

(define-public crate-koto_parser-0.11.0 (c (n "koto_parser") (v "0.11.0") (d (list (d (n "koto_lexer") (r "^0.11.0") (d #t) (k 0)))) (h "0c1k395rp1iih3dig0xi9hpwfcgz5kxvd0rsfdn6bnjl8459g86i") (f (quote (("panic_on_parser_error") ("default")))) (r "1.58.1")))

(define-public crate-koto_parser-0.12.0 (c (n "koto_parser") (v "0.12.0") (d (list (d (n "koto_lexer") (r "^0.12.0") (d #t) (k 0)) (d (n "koto_memory") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1b34mghsc3h021igvsbrnhq419jhcfivszbp4hg3b775nv8jyvnz") (f (quote (("panic_on_parser_error") ("default"))))))

(define-public crate-koto_parser-0.13.0 (c (n "koto_parser") (v "0.13.0") (d (list (d (n "koto_lexer") (r "^0.13.0") (d #t) (k 0)) (d (n "koto_memory") (r "^0.13.0") (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.0") (d #t) (k 0)))) (h "0jqipkbcvar7h003y0wqa2dcq6bfh399sjydky377m09zvs7gm42") (f (quote (("rc" "koto_memory/rc") ("panic_on_parser_error") ("default" "arc") ("arc" "koto_memory/arc"))))))

(define-public crate-koto_parser-0.14.0 (c (n "koto_parser") (v "0.14.0") (d (list (d (n "koto_lexer") (r "^0.14.0") (d #t) (k 0)) (d (n "koto_memory") (r "^0.14.0") (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.0") (d #t) (k 0)))) (h "1843ml8s2d7576kl1h32xrczyq95wjwa5p3jrp4nfr6f0zcmwjls") (f (quote (("rc" "koto_memory/rc") ("panic_on_parser_error") ("default" "arc") ("arc" "koto_memory/arc"))))))

