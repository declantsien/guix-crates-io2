(define-module (crates-io ko to koto_derive) #:use-module (crates-io))

(define-public crate-koto_derive-0.13.0 (c (n "koto_derive") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full"))) (d #t) (k 0)))) (h "0gpidz7qjh1ls191v8jgsnznf9rs9kvyc33jd3gbbnwpwvdb0k8q")))

(define-public crate-koto_derive-0.14.0 (c (n "koto_derive") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full"))) (d #t) (k 0)))) (h "17gmb4r5shvc92qn1f0hv82c78kzhg2ha1kggfc83wkajm7cgwh1")))

