(define-module (crates-io ko to koto_tempfile) #:use-module (crates-io))

(define-public crate-koto_tempfile-0.1.0 (c (n "koto_tempfile") (v "0.1.0") (d (list (d (n "koto_runtime") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "04a2k9j4yfjp37ih9n4ahr3gl8hzk5lx6792pl10qd2i7s4la86h")))

(define-public crate-koto_tempfile-0.2.0 (c (n "koto_tempfile") (v "0.2.0") (d (list (d (n "koto_runtime") (r "^0.2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "0j9xn8k0hdyyb2zf31980br73jx4zyf5xx97pnnf6qax7pgfdm9v")))

(define-public crate-koto_tempfile-0.3.0 (c (n "koto_tempfile") (v "0.3.0") (d (list (d (n "koto_runtime") (r "^0.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "0dnmdx07w5463skavq2118iv7zarcxw2qsnd98xcdk27wwg342gl")))

(define-public crate-koto_tempfile-0.4.0 (c (n "koto_tempfile") (v "0.4.0") (d (list (d (n "koto_runtime") (r "^0.4.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "0sacs7mls0rpygdpl1g72ak27pirm57bbbadqaxnx82134s0rn9m")))

(define-public crate-koto_tempfile-0.5.0 (c (n "koto_tempfile") (v "0.5.0") (d (list (d (n "koto_runtime") (r "^0.5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "16v7jp14kby27d6p2ydknj3dar47953hjszmkc815vvdbbx2syvz")))

(define-public crate-koto_tempfile-0.6.0 (c (n "koto_tempfile") (v "0.6.0") (d (list (d (n "koto_runtime") (r "^0.6.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "0pfsas9x1lic5ycnwsnnyifzwsyys7hfxrh2srasaid3r2mnfpiy")))

(define-public crate-koto_tempfile-0.7.0 (c (n "koto_tempfile") (v "0.7.0") (d (list (d (n "koto_runtime") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "1lvd56b6lb0a5rbdgiba61d5z3aqhd5y9f2r49mnck7sl422pk8y")))

(define-public crate-koto_tempfile-0.8.0 (c (n "koto_tempfile") (v "0.8.0") (d (list (d (n "koto_runtime") (r "^0.8.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "0h8s2r91h3dlp8zgrnl9ynj1fv2slzbmqhm15y4z3w70j2xfc3f4")))

(define-public crate-koto_tempfile-0.8.1 (c (n "koto_tempfile") (v "0.8.1") (d (list (d (n "koto_runtime") (r "^0.8.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "1ffsb61ap0rrk2ksbirh5b82hbhljdrkjcswl94n3h6y3pxhk710")))

(define-public crate-koto_tempfile-0.9.0 (c (n "koto_tempfile") (v "0.9.0") (d (list (d (n "koto_runtime") (r "^0.9.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "18w8jcvs9fyl2y43ifhxnqlp1s4c2rnis901h7s5nyg301k2ilfi")))

(define-public crate-koto_tempfile-0.9.1 (c (n "koto_tempfile") (v "0.9.1") (d (list (d (n "koto_runtime") (r "^0.9.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "0knyry5vj6klg45f39dnflps0h5c7kn5nyv3csr39f5wccg48jp6")))

(define-public crate-koto_tempfile-0.10.0 (c (n "koto_tempfile") (v "0.10.0") (d (list (d (n "koto_runtime") (r "^0.10.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "19hv94bc2l4n0vw2j828x0s6x44hnhid6y6cgq2ls0nrra9dhfrl")))

(define-public crate-koto_tempfile-0.11.0 (c (n "koto_tempfile") (v "0.11.0") (d (list (d (n "koto_runtime") (r "^0.11.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "1xilw5x9hhpj2pd3jsdsl1ikm2rs4mc2cycvdsbacg21jdw72dgd")))

(define-public crate-koto_tempfile-0.12.0 (c (n "koto_tempfile") (v "0.12.0") (d (list (d (n "koto_runtime") (r "^0.12.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "1yz8pi4jb7hdfncj5l64336n4j9ssjs03ippm1r5csxaalvz3ilh")))

(define-public crate-koto_tempfile-0.13.0 (c (n "koto_tempfile") (v "0.13.0") (d (list (d (n "koto_runtime") (r "^0.13.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "07ccyk3j84ywhbs4dh21rlc3jhdh9r5bw2mj49i6f759fhm1bagd")))

(define-public crate-koto_tempfile-0.14.0 (c (n "koto_tempfile") (v "0.14.0") (d (list (d (n "koto_runtime") (r "^0.14.0") (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "148kg7pii52f78qsbxdqvb2lnzcsc72qrprc85a349knnh929vx5") (f (quote (("rc" "koto_runtime/rc") ("default" "arc") ("arc" "koto_runtime/arc"))))))

