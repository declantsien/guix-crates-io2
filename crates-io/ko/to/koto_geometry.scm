(define-module (crates-io ko to koto_geometry) #:use-module (crates-io))

(define-public crate-koto_geometry-0.12.0 (c (n "koto_geometry") (v "0.12.0") (d (list (d (n "koto_runtime") (r "^0.12.0") (d #t) (k 0)) (d (n "nannou_core") (r "^0.18.0") (d #t) (k 0)))) (h "05304zprcp7yfbx79n3rni29xq02aj9974qbzdmf6gpzx1xdi5zp")))

(define-public crate-koto_geometry-0.13.0 (c (n "koto_geometry") (v "0.13.0") (d (list (d (n "koto_runtime") (r "^0.13.0") (d #t) (k 0)) (d (n "nannou_core") (r "^0.18.0") (d #t) (k 0)))) (h "06q21z7yaxcyw2g8jil6z5871cjrg5gxh786bq45w16siryj3v91")))

(define-public crate-koto_geometry-0.14.0 (c (n "koto_geometry") (v "0.14.0") (d (list (d (n "koto_runtime") (r "^0.14.0") (k 0)) (d (n "nannou_core") (r "^0.18.0") (d #t) (k 0)))) (h "1qkznf056fa8l5yzp0mnazyi2am9k7m2ncy2vrbcidb339x9yq5x") (f (quote (("rc" "koto_runtime/rc") ("default" "arc") ("arc" "koto_runtime/arc"))))))

