(define-module (crates-io ko to koto_color) #:use-module (crates-io))

(define-public crate-koto_color-0.12.0 (c (n "koto_color") (v "0.12.0") (d (list (d (n "koto_runtime") (r "^0.12.0") (d #t) (k 0)) (d (n "palette") (r "^0.7.2") (d #t) (k 0)))) (h "0789qcwpl35f5vpb6ibgpicn1bgi53v036y1v9sjcb6q8gwn3wll")))

(define-public crate-koto_color-0.13.0 (c (n "koto_color") (v "0.13.0") (d (list (d (n "koto_runtime") (r "^0.13.0") (d #t) (k 0)) (d (n "palette") (r "^0.7.2") (d #t) (k 0)))) (h "0h9pkj4afppz23wxyil0cjddab83k6vybhrkqljd24cima873ryr")))

(define-public crate-koto_color-0.14.0 (c (n "koto_color") (v "0.14.0") (d (list (d (n "koto_runtime") (r "^0.14.0") (k 0)) (d (n "palette") (r "^0.7.2") (d #t) (k 0)))) (h "01j6xw8n4yj47xzqd0vg0ipnjm210v1zgq8zysac5yqxfwbwmmyf") (f (quote (("rc" "koto_runtime/rc") ("default" "arc") ("arc" "koto_runtime/arc"))))))

