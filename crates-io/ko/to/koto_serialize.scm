(define-module (crates-io ko to koto_serialize) #:use-module (crates-io))

(define-public crate-koto_serialize-0.1.0 (c (n "koto_serialize") (v "0.1.0") (d (list (d (n "koto_runtime") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)))) (h "06z4ql1nzbhhb15mx41jj9jl86jq611mwvhdzibyxdm2cb1gja40")))

(define-public crate-koto_serialize-0.2.0 (c (n "koto_serialize") (v "0.2.0") (d (list (d (n "koto_runtime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)))) (h "0bs6qqfsir4jwbmz4dk1155fb6w3pvzl1nm184df276my7pz4zpw")))

(define-public crate-koto_serialize-0.3.0 (c (n "koto_serialize") (v "0.3.0") (d (list (d (n "koto_runtime") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)))) (h "1c9pwigp7xc95r3p7zvhkga01xaqq7dw0lz3bchf38i3sbrkq90r")))

(define-public crate-koto_serialize-0.4.0 (c (n "koto_serialize") (v "0.4.0") (d (list (d (n "koto_runtime") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)))) (h "0h2s064nwprdgk01jli6l03qk6ywmr7ramfwv6q8dgdqg6acy2zj")))

(define-public crate-koto_serialize-0.5.0 (c (n "koto_serialize") (v "0.5.0") (d (list (d (n "koto_runtime") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)))) (h "1y3bwkxw4w2379n7cr1qkmf0zhnx8slh8gbzkc1dwss6vqdyv885")))

(define-public crate-koto_serialize-0.6.0 (c (n "koto_serialize") (v "0.6.0") (d (list (d (n "koto_runtime") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)))) (h "1948z05m7wi4indvprj65h98ssas1w0qyyyn2gcl2jdpvm4ic42w")))

(define-public crate-koto_serialize-0.7.0 (c (n "koto_serialize") (v "0.7.0") (d (list (d (n "koto_runtime") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)))) (h "0a6y2r7j41vji86ir5c01y79ab10z19am8and6b29aan72lr0n6s")))

(define-public crate-koto_serialize-0.8.0 (c (n "koto_serialize") (v "0.8.0") (d (list (d (n "koto_runtime") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)))) (h "0f6lf9x7ynbqjpv5hlbnzxv0gymxkm6kp2chkmmdxxh25fihillh")))

(define-public crate-koto_serialize-0.8.1 (c (n "koto_serialize") (v "0.8.1") (d (list (d (n "koto_runtime") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)))) (h "1nfqx62flacc1l6m7df9fcbgkzk35lcgibnyl1d9x8g7ndlwmi7n")))

(define-public crate-koto_serialize-0.9.0 (c (n "koto_serialize") (v "0.9.0") (d (list (d (n "koto_runtime") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)))) (h "0yh1b4f7j5amj09b8rky5dwlbiz48nwgw795mjrhyp2bpa94kq4h")))

(define-public crate-koto_serialize-0.9.1 (c (n "koto_serialize") (v "0.9.1") (d (list (d (n "koto_runtime") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)))) (h "1mf05g8dfyzxcmzrnbcdl18bw3fcb76njs3p2p5ik6cwm4vq55vi")))

(define-public crate-koto_serialize-0.10.0 (c (n "koto_serialize") (v "0.10.0") (d (list (d (n "koto_runtime") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)))) (h "01n9jy910g7mamk86bv34jdnwdr9dxpxgysgvd9gpvqj9lpag3xb")))

(define-public crate-koto_serialize-0.11.0 (c (n "koto_serialize") (v "0.11.0") (d (list (d (n "koto_runtime") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)))) (h "0vgbj2pj4d0ff6k1vsfd7jb4bgcpdqc4fh72iif7sfa1rzif1pxb")))

(define-public crate-koto_serialize-0.12.0 (c (n "koto_serialize") (v "0.12.0") (d (list (d (n "koto_runtime") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)))) (h "1ji23q4k4r2rv7q5yv32if2rax44vd0hgf0irrly6qkra9p26gby")))

(define-public crate-koto_serialize-0.13.0 (c (n "koto_serialize") (v "0.13.0") (d (list (d (n "koto_runtime") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)))) (h "0z2ini2rskcm58gngj0wchdrqjnxb85szwhcm9dq7sr8slj0m0y6")))

(define-public crate-koto_serialize-0.14.0 (c (n "koto_serialize") (v "0.14.0") (d (list (d (n "koto_runtime") (r "^0.14.0") (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)))) (h "0x5x4lwfcrd9y9k1cv1fi8i7h3hqpil7p1k3nxwjxmzapdvjyniy") (f (quote (("rc" "koto_runtime/rc") ("default" "arc") ("arc" "koto_runtime/arc"))))))

