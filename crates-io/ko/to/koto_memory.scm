(define-module (crates-io ko to koto_memory) #:use-module (crates-io))

(define-public crate-koto_memory-0.12.0 (c (n "koto_memory") (v "0.12.0") (h "04k1j8grgyrnhb603nn9vgs3sczjq6qaqcb0dfn9y9b0sg9rs5k1")))

(define-public crate-koto_memory-0.13.0 (c (n "koto_memory") (v "0.13.0") (d (list (d (n "parking_lot") (r "^0.12.1") (o #t) (d #t) (k 0)))) (h "0chn1ljp9hfw5jdp4wx0kf50f5hhwc21n0bk0zjw1sfl4y60q8ry") (f (quote (("rc") ("default" "arc") ("arc" "parking_lot"))))))

(define-public crate-koto_memory-0.14.0 (c (n "koto_memory") (v "0.14.0") (d (list (d (n "parking_lot") (r "^0.12.1") (o #t) (d #t) (k 0)))) (h "15ilx06szzc9v73jsllm767p42qswd3wi7knnk3340y4damsrfd8") (f (quote (("rc") ("default" "arc") ("arc" "parking_lot"))))))

