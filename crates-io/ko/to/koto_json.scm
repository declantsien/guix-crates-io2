(define-module (crates-io ko to koto_json) #:use-module (crates-io))

(define-public crate-koto_json-0.1.0 (c (n "koto_json") (v "0.1.0") (d (list (d (n "koto_runtime") (r "^0.1.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (f (quote ("preserve_order" "std"))) (d #t) (k 0)))) (h "1sdszbvphfic0pqa5q37y9adhbfm92hr8pbh1lj4fj493mpa0dgw")))

(define-public crate-koto_json-0.2.0 (c (n "koto_json") (v "0.2.0") (d (list (d (n "koto_runtime") (r "^0.2.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (f (quote ("preserve_order" "std"))) (d #t) (k 0)))) (h "0nda9z1cgahpwq5gv8vygbyqdy48pf1638wb5rc06jmsldg3z575")))

(define-public crate-koto_json-0.3.0 (c (n "koto_json") (v "0.3.0") (d (list (d (n "koto_runtime") (r "^0.3.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (f (quote ("preserve_order" "std"))) (d #t) (k 0)))) (h "0ab0zhw5yb0051pqfli91j0rdw1vbgd540wz0rzgz593z4cv1iv1")))

(define-public crate-koto_json-0.4.0 (c (n "koto_json") (v "0.4.0") (d (list (d (n "koto_runtime") (r "^0.4.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (f (quote ("preserve_order" "std"))) (d #t) (k 0)))) (h "00vpj157lq8vz73q2ki5zin2zr5kxynws20sivzbdqxnb1q4nixw")))

(define-public crate-koto_json-0.5.0 (c (n "koto_json") (v "0.5.0") (d (list (d (n "koto_runtime") (r "^0.5.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.5.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (f (quote ("preserve_order" "std"))) (d #t) (k 0)))) (h "0n6fz1kgdrzp59yky4c7349w0mrkif3ir2z79sd2873ns1yhwj89")))

(define-public crate-koto_json-0.6.0 (c (n "koto_json") (v "0.6.0") (d (list (d (n "koto_runtime") (r "^0.6.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (f (quote ("preserve_order" "std"))) (d #t) (k 0)))) (h "16z6fimwny234s7czazx0cb6mp46q6gp3097dxwrkr6v6zfcpml4")))

(define-public crate-koto_json-0.7.0 (c (n "koto_json") (v "0.7.0") (d (list (d (n "koto_runtime") (r "^0.7.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (f (quote ("preserve_order" "std"))) (d #t) (k 0)))) (h "0fzh5c4vcf60mhajxhczbbcn65lr8c7awqx0aahdhpibjwwd60i8")))

(define-public crate-koto_json-0.8.0 (c (n "koto_json") (v "0.8.0") (d (list (d (n "koto_runtime") (r "^0.8.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (f (quote ("preserve_order" "std"))) (d #t) (k 0)))) (h "1gfw2ji903814fca2d0pzldyx06dchsfskhi69bg3rj6m1lr6mrs")))

(define-public crate-koto_json-0.8.1 (c (n "koto_json") (v "0.8.1") (d (list (d (n "koto_runtime") (r "^0.8.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (f (quote ("preserve_order" "std"))) (d #t) (k 0)))) (h "1kak601jbiqclwwn4lcbphf8522ipizkzc385ndbwrin03an6xlw")))

(define-public crate-koto_json-0.9.0 (c (n "koto_json") (v "0.9.0") (d (list (d (n "koto_runtime") (r "^0.9.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (f (quote ("preserve_order" "std"))) (d #t) (k 0)))) (h "1n8jfwzdjhji76ig5cbxxaq65m97mh1dgz3pi4mcsh4ms64gfbrj")))

(define-public crate-koto_json-0.9.1 (c (n "koto_json") (v "0.9.1") (d (list (d (n "koto_runtime") (r "^0.9.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (f (quote ("preserve_order" "std"))) (d #t) (k 0)))) (h "17h4w50bppakkb07m5jkmxbc6g0k6p0ilc9advjckfg2nf29k6l3")))

(define-public crate-koto_json-0.10.0 (c (n "koto_json") (v "0.10.0") (d (list (d (n "koto_runtime") (r "^0.10.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.10.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (f (quote ("preserve_order" "std"))) (d #t) (k 0)))) (h "0ybxdgc7k13g00b8cnaf98yl2379pjrxwyin1c55wlqh2iwimzmi")))

(define-public crate-koto_json-0.11.0 (c (n "koto_json") (v "0.11.0") (d (list (d (n "koto_runtime") (r "^0.11.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.11.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (f (quote ("preserve_order" "std"))) (d #t) (k 0)))) (h "1r3ihw70v2xg0kp96zrdqc4j7lag9qzwxb700y41zgy2rq584gdc")))

(define-public crate-koto_json-0.12.0 (c (n "koto_json") (v "0.12.0") (d (list (d (n "koto_runtime") (r "^0.12.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.12.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (f (quote ("preserve_order" "std"))) (d #t) (k 0)))) (h "0pwrlvxyyra67w8b7iwas59mqym90i91192w14awwqj8bn40xpp1")))

(define-public crate-koto_json-0.13.0 (c (n "koto_json") (v "0.13.0") (d (list (d (n "koto_runtime") (r "^0.13.0") (d #t) (k 0)) (d (n "koto_serialize") (r "^0.13.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (f (quote ("preserve_order" "std"))) (d #t) (k 0)))) (h "0jv6zkmmg87akhw6c1m7gsnysxjw92x29azxvw4i3p9nc5bd6nap")))

(define-public crate-koto_json-0.14.0 (c (n "koto_json") (v "0.14.0") (d (list (d (n "koto_runtime") (r "^0.14.0") (k 0)) (d (n "koto_serialize") (r "^0.14.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (f (quote ("preserve_order" "std"))) (d #t) (k 0)))) (h "1n8j1ldpjgiplzvriqp5ngxbcqkx3djmncz051gfj014mw30q99f") (f (quote (("rc" "koto_runtime/rc") ("default" "arc") ("arc" "koto_runtime/arc"))))))

