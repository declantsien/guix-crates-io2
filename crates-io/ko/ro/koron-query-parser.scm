(define-module (crates-io ko ro koron-query-parser) #:use-module (crates-io))

(define-public crate-koron-query-parser-1.0.0 (c (n "koron-query-parser") (v "1.0.0") (d (list (d (n "sqlparser") (r "^0.41.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1ypn86cfl4gzzzh8lr4crqjvn58vryblmascs2hhwparnhaiakq5")))

