(define-module (crates-io ko sh kosh) #:use-module (crates-io))

(define-public crate-kosh-0.1.0 (c (n "kosh") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.26") (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)))) (h "17rxcgr80y6205sfz1aka7q42xki9rdngn0hi9hhcvnbvy99llbc")))

(define-public crate-kosh-0.1.1 (c (n "kosh") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.26") (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.46.0") (f (quote ("Win32_System_Console"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0fzrdpwdx5kqx200a4knb499wfwnb2rsw2xajc56q24fp0fzf8gn")))

