(define-module (crates-io ko mp komple-framework-types) #:use-module (crates-io))

(define-public crate-komple-framework-types-0.1.0-alpha (c (n "komple-framework-types") (v "0.1.0-alpha") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)))) (h "10m22rk0zpvxdm6ms0pvw3hnla885j7bl0zs7vrzjm62i5gf62m2") (y #t)))

(define-public crate-komple-framework-types-1.0.0-beta (c (n "komple-framework-types") (v "1.0.0-beta") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)))) (h "02w9zbaxhrp3073k45l9f0ji4k5ja9n4gglq3bzskigvldck2yvp")))

(define-public crate-komple-framework-types-1.0.1-beta (c (n "komple-framework-types") (v "1.0.1-beta") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)))) (h "1zan2r2zj6a42b85mj9lhczzz3l8i3ra1lgqxdlrcn87h7zy3w2c")))

(define-public crate-komple-framework-types-1.1.0-beta (c (n "komple-framework-types") (v "1.1.0-beta") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)))) (h "1fkyzawyrd79wa57mx5na7fzxfvlgfmdwi5l9gcdbg0ds2ij8wxj")))

(define-public crate-komple-framework-types-1.1.1-beta (c (n "komple-framework-types") (v "1.1.1-beta") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)))) (h "1klrycrwn2cwkfvfyla8agzpakbhqi6kxbkqxmfr5d23gynfk9hr")))

