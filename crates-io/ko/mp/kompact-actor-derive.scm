(define-module (crates-io ko mp kompact-actor-derive) #:use-module (crates-io))

(define-public crate-kompact-actor-derive-0.4.0 (c (n "kompact-actor-derive") (v "0.4.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "105pignakl3f74062ax8g557y14p6qbkm64rc2d37lb2qjgqkf50")))

(define-public crate-kompact-actor-derive-0.5.0 (c (n "kompact-actor-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01icjr7n6640q2qspfspd2nn9xb4b4ddc08vbkpk34642xrqzx10")))

(define-public crate-kompact-actor-derive-0.6.0 (c (n "kompact-actor-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ckqzipr78d63nyd3gismhq2sn09xyjqakbpalq41rykg9wfaqcc")))

(define-public crate-kompact-actor-derive-0.7.0 (c (n "kompact-actor-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qh157qsn9b667hcg4m1w5djisgs46gf58xgdlim3i0vq5j5h2ib")))

(define-public crate-kompact-actor-derive-0.8.0 (c (n "kompact-actor-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1mm7slzamw70hjrvh122g2fbbfqwwd1zh9j1d13xbb2lbzrgj24p")))

(define-public crate-kompact-actor-derive-0.8.1 (c (n "kompact-actor-derive") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07vgv1jrnw6mawfwb9555in25qgwkkbnbifw0ywmgni8xfygl8w6")))

(define-public crate-kompact-actor-derive-0.9.0 (c (n "kompact-actor-derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ji9byflw9dln9zxq19s319xmh8hqwqa9ip51z710hwgdnhx6lmy")))

(define-public crate-kompact-actor-derive-0.10.0 (c (n "kompact-actor-derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ignhkl1qzanllb2p6k736ryw3zla9iwlcw9jz22wwdcnd3bxq37")))

(define-public crate-kompact-actor-derive-0.10.1 (c (n "kompact-actor-derive") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "05sh31kp70pclcljr0c5790c122vsrjxm6mdnfa37f0d9iss273j")))

(define-public crate-kompact-actor-derive-0.11.0 (c (n "kompact-actor-derive") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cc0mv14klc31shbvv047n0rjlwr84nlh2i302mkb5r4g5vlrn0q")))

(define-public crate-kompact-actor-derive-0.11.1 (c (n "kompact-actor-derive") (v "0.11.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1d462yxxhpl12zh4mnyywc9nw1r8cgfaj3ivnyp0vycbx6yi8164")))

(define-public crate-kompact-actor-derive-0.11.2 (c (n "kompact-actor-derive") (v "0.11.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mviqniyl4m3yn80m28lv3215ccsr9cr6nkk064b2jzndpdh5wn8")))

