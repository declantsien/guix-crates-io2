(define-module (crates-io ko mp kompact-component-derive) #:use-module (crates-io))

(define-public crate-kompact-component-derive-0.4.0 (c (n "kompact-component-derive") (v "0.4.0") (d (list (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "15blky3k508pdi06w4mpa25xa7mlxxafgcw58h1qnbrw6gzkhca8")))

(define-public crate-kompact-component-derive-0.5.0 (c (n "kompact-component-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qwplk6cip7d7zqqr0kwnakhg4p0b7bzjynmcr2zdhdamp833d05")))

(define-public crate-kompact-component-derive-0.6.0 (c (n "kompact-component-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19vs39p39ln3n5zz6w5b6bwsbdjzia9gz5g4vcafz22rmw70h3s4")))

(define-public crate-kompact-component-derive-0.7.0 (c (n "kompact-component-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11hykxnd4j4i2b1iw2y5k6cz8vlwnz58cm4aqhn1yy4wmlwkqmdc")))

(define-public crate-kompact-component-derive-0.8.0 (c (n "kompact-component-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1nk1vcgck2bxxdmi80d3qxlqdvsj4vf8b9i6r67kv2b8b79sblip")))

(define-public crate-kompact-component-derive-0.8.1 (c (n "kompact-component-derive") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hq7fw1i5i8hva74b4yh0q5141idnw8mw9kf4pi2fvmhriynv39f")))

(define-public crate-kompact-component-derive-0.9.0 (c (n "kompact-component-derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jkiyrwpg61yciyjjqbs3z2dx7lmlm0samv6s4wfrnk8lxki4z2f")))

(define-public crate-kompact-component-derive-0.10.0 (c (n "kompact-component-derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fw43n4cl5yscigkmxyw0imiwpjhmd5n9spvf81mrbsv50lfyfyz")))

(define-public crate-kompact-component-derive-0.10.1 (c (n "kompact-component-derive") (v "0.10.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1n66z01msn37gis864c5lbcj9l0rf9fg9bai4hk3a0anypsf6k6p")))

(define-public crate-kompact-component-derive-0.11.0 (c (n "kompact-component-derive") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "141qlafssbrizzc8a0fnl9xpa9hyfh2qhzq92ihxw02q5jkr5dwf")))

(define-public crate-kompact-component-derive-0.11.1 (c (n "kompact-component-derive") (v "0.11.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0kw1lamw2wzk6zkl6zsq26kdzy6xay4ci5iqjk18j7i0kzs67895")))

(define-public crate-kompact-component-derive-0.11.2 (c (n "kompact-component-derive") (v "0.11.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1sn34h3xgg3l28s1lkfidhg531aml0n9micnixnp0j52vylpg9bc")))

