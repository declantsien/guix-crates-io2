(define-module (crates-io ko nt kontrolluppgift_macros) #:use-module (crates-io))

(define-public crate-kontrolluppgift_macros-0.1.0 (c (n "kontrolluppgift_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1g6g4kxz2biavd8d51g1xvmxip3xjg5c4fzxyn7zim7clj1pzwhf")))

(define-public crate-kontrolluppgift_macros-0.2.0 (c (n "kontrolluppgift_macros") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "02f9p95q7sg05y35rnf2lr5x4jr8q3km3xbjz4ks49bq5f6b2lxl")))

(define-public crate-kontrolluppgift_macros-0.3.0 (c (n "kontrolluppgift_macros") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "12l2vq8nxrngvkgzbdzqr65affqvca2gc3yca258bzix2cl0n921")))

(define-public crate-kontrolluppgift_macros-0.4.0 (c (n "kontrolluppgift_macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1mx7468hd4vhbxqxggq4iscfq5v5g8k6sgpr540ihnq8i54z8dah")))

