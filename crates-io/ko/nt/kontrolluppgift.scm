(define-module (crates-io ko nt kontrolluppgift) #:use-module (crates-io))

(define-public crate-kontrolluppgift-0.1.0 (c (n "kontrolluppgift") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 2)))) (h "19ii044dydpjc3c3pprikzx4q9j063f3pfa6f8kc4vk85sxg7iz6")))

(define-public crate-kontrolluppgift-0.2.0 (c (n "kontrolluppgift") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 2)))) (h "003m8fmbwlppf47vsk29c8v87c3fgc7z1lla708gvq4rvmrd75rz")))

(define-public crate-kontrolluppgift-0.3.0 (c (n "kontrolluppgift") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 2)))) (h "0z46jq1iafrv1hh83ywddar4yfh92qzqmk8bicd4f3vdf2d6360z")))

(define-public crate-kontrolluppgift-0.4.0 (c (n "kontrolluppgift") (v "0.4.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 2)))) (h "128czd77w6r6m3w2w4pkg0dla926dhisf1vl6d2md5r1kcl0kaw2")))

(define-public crate-kontrolluppgift-0.5.0 (c (n "kontrolluppgift") (v "0.5.0") (d (list (d (n "quick-xml") (r "^0.28.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "16rfkvcw7b8gv76dxp2v3d57mz3l9pzx25nj09in0bs042a6gw48")))

(define-public crate-kontrolluppgift-0.6.0 (c (n "kontrolluppgift") (v "0.6.0") (d (list (d (n "quick-xml") (r "^0.28.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zz9v7absjgqzng6x274lacyv5wi2fa3s3n426n5cdq3n8m7napz")))

(define-public crate-kontrolluppgift-0.7.0 (c (n "kontrolluppgift") (v "0.7.0") (d (list (d (n "quick-xml") (r "^0.28.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "16l0fh6xs52p0fchvck5b1fz6lm4c39yqmqm67i9i49jzyhfv4rk")))

(define-public crate-kontrolluppgift-0.8.0 (c (n "kontrolluppgift") (v "0.8.0") (d (list (d (n "quick-xml") (r "^0.28.0") (d #t) (k 0)))) (h "1mn101kd1z5q6hvz6jh9dgq4lhrwhvd4gjf9afwjfcfi23vzng4i")))

(define-public crate-kontrolluppgift-0.9.0 (c (n "kontrolluppgift") (v "0.9.0") (d (list (d (n "quick-xml") (r "^0.28.0") (d #t) (k 0)))) (h "03qp4dkx3d663p9cirnyg5a48iigwrh900y4kj7bzjzdv71vpymm")))

(define-public crate-kontrolluppgift-0.9.1 (c (n "kontrolluppgift") (v "0.9.1") (d (list (d (n "quick-xml") (r "^0.28.0") (d #t) (k 0)))) (h "17xgq472awag506kjixh4qw55im2k12ai3xhg84gd6n6lavci309")))

(define-public crate-kontrolluppgift-0.10.0 (c (n "kontrolluppgift") (v "0.10.0") (d (list (d (n "quick-xml") (r "^0.28.0") (d #t) (k 0)))) (h "05knbil2y757rwf1npl82h6298da3wpn6pmccn0zpsfqc1x9m6b8")))

(define-public crate-kontrolluppgift-0.11.0 (c (n "kontrolluppgift") (v "0.11.0") (d (list (d (n "quick-xml") (r "^0.28.0") (d #t) (k 0)))) (h "045gna1mqyj9ihhlfd5arrp96fj793grrz2027gi2q77hs3k92pl")))

(define-public crate-kontrolluppgift-0.12.0 (c (n "kontrolluppgift") (v "0.12.0") (d (list (d (n "quick-xml") (r "^0.28.0") (d #t) (k 0)))) (h "073wd2ihc3jq8rb3rp9abc6994v89bjdh20azq3ckg562pb9wbgw")))

(define-public crate-kontrolluppgift-0.13.0 (c (n "kontrolluppgift") (v "0.13.0") (d (list (d (n "quick-xml") (r "^0.28.0") (d #t) (k 0)))) (h "0n159b1s355pw3k741apwzh28r7r78qxxwvlxn9fqlsjqnbilhnh")))

(define-public crate-kontrolluppgift-0.13.1 (c (n "kontrolluppgift") (v "0.13.1") (d (list (d (n "kontrolluppgift_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28.0") (d #t) (k 0)))) (h "12q4wyxd509swas55rz2wnvxmkdl4g4ya2vwv7flzj2m5i55kn77")))

(define-public crate-kontrolluppgift-0.13.2 (c (n "kontrolluppgift") (v "0.13.2") (d (list (d (n "kontrolluppgift_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28.0") (d #t) (k 0)))) (h "13gyhwsrvf74lprkgykgn1zfzhnbf2vrf0p1mz9qzjcfj0w0k1zz")))

(define-public crate-kontrolluppgift-0.14.0 (c (n "kontrolluppgift") (v "0.14.0") (d (list (d (n "kontrolluppgift_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28.0") (d #t) (k 0)))) (h "1mbkncnjz5f2l8zyy0km3xll6f8ciy0ijqdzrxlj4lrzhiahidig")))

(define-public crate-kontrolluppgift-0.15.0 (c (n "kontrolluppgift") (v "0.15.0") (d (list (d (n "kontrolluppgift_macros") (r "^0.2.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28.0") (d #t) (k 0)))) (h "1axhfk0ncab9d3smdc7zmqg8v55g43lk10ank3isk5qvrqf6fc1w")))

(define-public crate-kontrolluppgift-0.16.0 (c (n "kontrolluppgift") (v "0.16.0") (d (list (d (n "kontrolluppgift_macros") (r "^0.2.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28.0") (d #t) (k 0)))) (h "0gbqf7xk6wh97dhf0fln58if4fdwh8yccr5ykji54gpddmcn68l1")))

(define-public crate-kontrolluppgift-0.17.0 (c (n "kontrolluppgift") (v "0.17.0") (d (list (d (n "kontrolluppgift_macros") (r "^0.3.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28.0") (d #t) (k 0)))) (h "0cwqzppmw6hiw09ifqz395qgnd8dxz9xrq8z0jg21dgkwd80ay8v")))

(define-public crate-kontrolluppgift-0.18.0 (c (n "kontrolluppgift") (v "0.18.0") (d (list (d (n "kontrolluppgift_macros") (r "^0.3.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "126pvvx9a0z6jcg9l7mh6y0cyd9jx5mpqkfrkzqygw247hyslnq7")))

(define-public crate-kontrolluppgift-0.19.0 (c (n "kontrolluppgift") (v "0.19.0") (d (list (d (n "kontrolluppgift_macros") (r "^0.3.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.29.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "00pz8hpawf07riinw6qs7dcjxiwv6qwcil09vp5v4my3jnm928ch")))

(define-public crate-kontrolluppgift-0.20.0 (c (n "kontrolluppgift") (v "0.20.0") (d (list (d (n "kontrolluppgift_macros") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.30.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "time") (r "^0.3.25") (f (quote ("parsing" "formatting"))) (d #t) (k 0)))) (h "0w0vwmdga7fw1zvpjgq9vsvvrhrs6rbfhxqbcgfr7jc98i22h6yv")))

(define-public crate-kontrolluppgift-0.21.0 (c (n "kontrolluppgift") (v "0.21.0") (d (list (d (n "kontrolluppgift_macros") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "quick-xml") (r "^0.30.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "time") (r "^0.3.25") (f (quote ("parsing" "formatting"))) (d #t) (k 0)))) (h "1myac2v1n3m3n9brf0kr0l9xydjgalqqa4r3waajh89967wfk231")))

