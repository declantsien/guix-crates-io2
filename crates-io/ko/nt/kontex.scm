(define-module (crates-io ko nt kontex) #:use-module (crates-io))

(define-public crate-kontex-0.1.0 (c (n "kontex") (v "0.1.0") (h "0f3fihxr3qr3msf7x2qj24w573kq3m89csfnmn6m6dgv42a3spib")))

(define-public crate-kontex-0.2.0 (c (n "kontex") (v "0.2.0") (h "1358bh7zcp1y2a026gmm8sp2vzg6q9gw4p7sp9wzs0l7jfz4jlk5")))

