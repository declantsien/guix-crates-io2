(define-module (crates-io ko nt kontrak-sqlite) #:use-module (crates-io))

(define-public crate-kontrak-sqlite-0.1.2 (c (n "kontrak-sqlite") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.12") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "00ynm22w8kgpyrwav3mihlwjxpy87z28924y44npfyhin5psp6j1") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-kontrak-sqlite-0.1.3 (c (n "kontrak-sqlite") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.12") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "15xbyw2iwr4hqv0k6kiacql6svjl467rdvdg727dwmvb0fjzxpdf") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-kontrak-sqlite-0.1.4 (c (n "kontrak-sqlite") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sqlite3-sys") (r "^0.12") (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "01k2gp1sp1nrshvam5a074an83gq3fhvr3yf3rqvl8yd5aiv6axl") (f (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

