(define-module (crates-io ko np konpeito) #:use-module (crates-io))

(define-public crate-konpeito-0.1.0 (c (n "konpeito") (v "0.1.0") (d (list (d (n "aead") (r "^0.5.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.16") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (f (quote ("clap" "cli"))) (d #t) (k 0)) (d (n "ellipse") (r "^0.2.0") (d #t) (k 0)) (d (n "magic-crypt") (r "^3.1.10") (d #t) (k 0)) (d (n "osshkeys") (r "^0.6.2") (d #t) (k 0)) (d (n "passwords") (r "^3.1.9") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.2") (d #t) (k 0)) (d (n "sled") (r "^0.34.7") (f (quote ("compression"))) (d #t) (k 0)) (d (n "tabwriter") (r "^1.2.1") (d #t) (k 0)))) (h "0l5q20f57cnvd10igfr56yvqs691mfhj8w9lypawv1isz3pglw3z")))

(define-public crate-konpeito-0.2.0 (c (n "konpeito") (v "0.2.0") (d (list (d (n "base64") (r "^0.22.1") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "keyring") (r "^2.3.3") (d #t) (k 0)))) (h "1aaddxc1x5z9vcldn0m9svn2ksg7hilb8h5qz2ca54k9gcyc3qml")))

(define-public crate-konpeito-0.2.1 (c (n "konpeito") (v "0.2.1") (d (list (d (n "base64") (r "^0.22.1") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "keyring") (r "^2.3.3") (d #t) (k 0)))) (h "07ixfv29x6jc8j7hq0yaplc8ca01ijjp6911hvkp106mqlfxbqj2")))

