(define-module (crates-io ko mm kommandozeilen_argumente_derive) #:use-module (crates-io))

(define-public crate-kommandozeilen_argumente_derive-0.1.0 (c (n "kommandozeilen_argumente_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full" "parsing" "proc-macro"))) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "19grpqizwarxycvd77k8n4rh2ca4gnbni2y6mz8flvfnfn64gr3k")))

(define-public crate-kommandozeilen_argumente_derive-0.1.2 (c (n "kommandozeilen_argumente_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full" "parsing" "proc-macro"))) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "093bxfclwp3wpmwl6wsqrf6ydm146kbbcj2h0jrvhpvi14fzyxmw") (y #t)))

(define-public crate-kommandozeilen_argumente_derive-0.1.3 (c (n "kommandozeilen_argumente_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full" "parsing" "proc-macro"))) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "1zr16n62h3cvj8s15c7fi5znlaflzvz2nzlfpz3y2cyg77kx1931")))

(define-public crate-kommandozeilen_argumente_derive-0.2.0 (c (n "kommandozeilen_argumente_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.89") (f (quote ("derive" "parsing" "proc-macro"))) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "12d7ni4xzz9hipvhnd3n8g2qidirm6js4n7hjv9mj01bk4mcsy2q")))

