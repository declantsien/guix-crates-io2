(define-module (crates-io ko mo komodo_airdrop) #:use-module (crates-io))

(define-public crate-komodo_airdrop-0.0.1 (c (n "komodo_airdrop") (v "0.0.1") (h "0g9v7p13919l05fmjigc01f8p3vxwsqq4ijpfdqqc6bbsy8y48h6")))

(define-public crate-komodo_airdrop-0.0.2 (c (n "komodo_airdrop") (v "0.0.2") (d (list (d (n "derive_more") (r "^0.14") (d #t) (k 0)) (d (n "komodo_rpc_client") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09cbkwgdrf088nn5x9swkbar4pbdvd5ray6i833k4lckji4av8p0")))

