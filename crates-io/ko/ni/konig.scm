(define-module (crates-io ko ni konig) #:use-module (crates-io))

(define-public crate-konig-0.1.0 (c (n "konig") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0ra7l60bz2z18kyn4bhb2iqrfpj5x04wynsp45si6hf9v8z0wzhd")))

(define-public crate-konig-0.1.1 (c (n "konig") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0a830h2q5ay6d3d5c62a1vklvrcmlfa8xqvxpz2zdr8hg7c4741q")))

(define-public crate-konig-0.1.2 (c (n "konig") (v "0.1.2") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nonmax") (r "^0.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0i8zcizvjkbbyxk2vy2zpx3ypd7xl3d6lfncq4d0j9ki89z1hbhm")))

(define-public crate-konig-0.1.3 (c (n "konig") (v "0.1.3") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nonmax") (r "^0.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1yhr4257hh47zh9r9vzcq4b869izw9js6lprvsmvr0b4bwzqb22j")))

(define-public crate-konig-0.1.4 (c (n "konig") (v "0.1.4") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nonmax") (r "^0.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "12dslm4vqy1cs2mddcvqrq0vqimhwaiddrmlv2sa5jplbmw46vk6")))

(define-public crate-konig-0.1.5 (c (n "konig") (v "0.1.5") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "nonmax") (r "^0.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0ry3438aziqqipksvn6hl7fqdzkwfn4zd1xf2dr72xf5n8dwh2jr")))

