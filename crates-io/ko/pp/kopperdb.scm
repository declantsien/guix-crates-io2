(define-module (crates-io ko pp kopperdb) #:use-module (crates-io))

(define-public crate-kopperdb-0.1.0 (c (n "kopperdb") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0g6qk4gxjbww9d1b7waklsxl0p70h9whx2rpfl9rqqrr6lzc1sp8")))

