(define-module (crates-io ko pt koption_macros) #:use-module (crates-io))

(define-public crate-koption_macros-0.1.0 (c (n "koption_macros") (v "0.1.0") (h "0j56ixj51z8k9v70pn8hlv6xwn0c69i708dpip0v023d2imbir1r")))

(define-public crate-koption_macros-0.1.1 (c (n "koption_macros") (v "0.1.1") (h "16gzzp5ya4wzm7avm0jwwdqp1s2il3n6q8pl6fp9ya6v2v3a7j77")))

