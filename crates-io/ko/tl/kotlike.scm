(define-module (crates-io ko tl kotlike) #:use-module (crates-io))

(define-public crate-kotlike-0.1.0 (c (n "kotlike") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("fold" "full"))) (d #t) (k 0)))) (h "1fx20n1vx393p3j0f77cn1k52l04v3zjm10n3wd0l1mc0xm5kaan") (r "1.70.0")))

