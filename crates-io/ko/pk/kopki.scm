(define-module (crates-io ko pk kopki) #:use-module (crates-io))

(define-public crate-kopki-0.1.1 (c (n "kopki") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.14.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.10") (d #t) (k 0)))) (h "0b7avp9qd252fml7k46bl72jbbs8hpk243c7xhrplg29dhf43k0h") (r "1.56")))

(define-public crate-kopki-0.1.2 (c (n "kopki") (v "0.1.2") (d (list (d (n "bytemuck") (r "^1.14.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.10") (d #t) (k 0)))) (h "1d2j1w17dccnga5z841bkjk9cgmz0jglj440xvhhxc0956iz3y0m") (r "1.56")))

(define-public crate-kopki-0.2.2 (c (n "kopki") (v "0.2.2") (d (list (d (n "bytemuck") (r "^1.14.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.10") (d #t) (k 0)))) (h "050d6gmd6hcpqyrw6rxvq1jgg4szchm43fxknyvvc0x02cc8lsg5") (r "1.56")))

(define-public crate-kopki-0.2.3 (c (n "kopki") (v "0.2.3") (d (list (d (n "bytemuck") (r "^1.14.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.10") (d #t) (k 0)))) (h "1yprl3s8rdzsf9qhcqzk59609wb0h766qrdccz829kn49draps7r") (r "1.56")))

(define-public crate-kopki-0.2.4 (c (n "kopki") (v "0.2.4") (d (list (d (n "bytemuck") (r "^1.14.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.10") (d #t) (k 0)))) (h "0lcinavsars9r6i2zvpxh5f5y2kym5rnxwzkbvvhysxw1s7zmblx") (r "1.56")))

(define-public crate-kopki-0.3.4 (c (n "kopki") (v "0.3.4") (d (list (d (n "bytemuck") (r "^1.14.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.10") (d #t) (k 0)))) (h "15g83lll18y5w42j9lvcqa5z461pcxd3vidcx2vgrqz6aqw3x5ln") (r "1.56")))

(define-public crate-kopki-0.3.5 (c (n "kopki") (v "0.3.5") (d (list (d (n "bytemuck") (r "^1.14.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "pollster") (r "^0.3.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.19.1") (d #t) (k 0)) (d (n "winit") (r "^0.29.10") (d #t) (k 0)))) (h "15ddawsjmxh74fnhk05dasdp6jkvjhz74fib7w3zpsygdial9a74") (r "1.56")))

