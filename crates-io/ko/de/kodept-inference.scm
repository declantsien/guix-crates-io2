(define-module (crates-io ko de kodept-inference) #:use-module (crates-io))

(define-public crate-kodept-inference-0.1.1 (c (n "kodept-inference") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99") (f (quote ("display" "into" "from" "deref" "deref_mut" "constructor" "try_into" "is_variant"))) (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "nonempty-collections") (r "^0.1") (d #t) (k 0)))) (h "11i7ssqawgmsvzvrcjbgw86n3q63fkhw7g9dbp9c2cfz8127hwn6") (r "1.74.0")))

(define-public crate-kodept-inference-0.1.2 (c (n "kodept-inference") (v "0.1.2") (d (list (d (n "derive_more") (r "^0.99") (f (quote ("display" "into" "from" "deref" "deref_mut" "constructor" "try_into" "is_variant"))) (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "nonempty-collections") (r "^0.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1xpvq89f4pn902jln4kdh6n8n8hsaijr1ds0m0b9kyp5rkv1wkzj") (r "1.74.0")))

(define-public crate-kodept-inference-0.2.0 (c (n "kodept-inference") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99") (f (quote ("display" "into" "from" "deref" "deref_mut" "constructor" "try_into" "is_variant"))) (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)) (d (n "nonempty-collections") (r "^0.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "16vzk0zsrdjxvdhrxw77vdh3waxq2azmyv938f6dfphxaka4l9gh") (r "1.74.0")))

(define-public crate-kodept-inference-0.2.1 (c (n "kodept-inference") (v "0.2.1") (d (list (d (n "derive_more") (r "^0.99") (f (quote ("display" "into" "from" "deref" "deref_mut" "constructor" "try_into" "is_variant"))) (d #t) (k 0)) (d (n "itertools") (r "^0.13") (d #t) (k 0)) (d (n "nonempty-collections") (r "^0.2") (d #t) (k 0)) (d (n "rstest") (r "^0.19.0") (d #t) (k 2)))) (h "02yi479wxdycr8bg683yn1nvh11qh2i3bahk5jnysj1bss0qqxwa") (r "1.74.0")))

