(define-module (crates-io ko de kodec) #:use-module (crates-io))

(define-public crate-kodec-0.1.0 (c (n "kodec") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (o #t) (d #t) (k 0)))) (h "006rql64y275l51bjaakxhvnl4d4cgs5f55h85fi9zj0jxddh9gw") (f (quote (("json" "serde_json") ("default") ("binary" "bincode"))))))

