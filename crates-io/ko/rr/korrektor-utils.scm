(define-module (crates-io ko rr korrektor-utils) #:use-module (crates-io))

(define-public crate-korrektor-utils-0.1.0 (c (n "korrektor-utils") (v "0.1.0") (d (list (d (n "pcre") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "11kafrq5n3n8bvxl06nkraqnm2hb0wp9z6g1qabyghff4c36ds2y")))

(define-public crate-korrektor-utils-0.1.1 (c (n "korrektor-utils") (v "0.1.1") (d (list (d (n "pcre") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "00s0bn9l7h9b7f7kx2c4aq5kadclzr5pw11y3xgs15269xh0p27r")))

(define-public crate-korrektor-utils-0.1.2 (c (n "korrektor-utils") (v "0.1.2") (d (list (d (n "pcre") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1z1f3s5naisxm15qk0g45m29rk77c61fv05xg9d6xi0m2s6kkjvp")))

