(define-module (crates-io ko rr korrektor) #:use-module (crates-io))

(define-public crate-korrektor-0.1.1 (c (n "korrektor") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "pcre") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0l183qi5dv3lv53fbwm43l5yixmb7wa8vxbpkr34kyli5kj72kda")))

(define-public crate-korrektor-0.2.1 (c (n "korrektor") (v "0.2.1") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "pcre") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "19fvsnxkd37lxcl2r7bi7zdk87q27h2gvp8dhxxi50yd3v0qav8w")))

(define-public crate-korrektor-0.2.2 (c (n "korrektor") (v "0.2.2") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "pcre") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0dmrbl04jbfm9jag0lwx3a0k25aldyzssk2864ijncx74k312d6g")))

(define-public crate-korrektor-0.2.3 (c (n "korrektor") (v "0.2.3") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "pcre") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1lvhzy9wdk3dwc80y0j3xy9fbfly77gqp838wl8fjf6583ywwbg1")))

(define-public crate-korrektor-0.2.4 (c (n "korrektor") (v "0.2.4") (d (list (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "korrektor-utils") (r "^0.1.2") (d #t) (k 0)) (d (n "pcre") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "041l554676iflizfdcrjmfmnjmr188na2r2l75xnk2i590bl2f7i") (y #t)))

(define-public crate-korrektor-0.3.0 (c (n "korrektor") (v "0.3.0") (d (list (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "korrektor-utils") (r "^0.1.2") (d #t) (k 0)) (d (n "pcre") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "03158md8ah9fkzwkwnmrs24jixfcaykv4pwbi77a8xs3zxvap382")))

(define-public crate-korrektor-0.3.1 (c (n "korrektor") (v "0.3.1") (d (list (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "korrektor-utils") (r "^0.1.2") (d #t) (k 0)) (d (n "pcre") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10r54m47jx13m5dmxf53h14d533s382xcl5ii0nigzh4rg9yrqdk")))

