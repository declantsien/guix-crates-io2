(define-module (crates-io ko da kodama) #:use-module (crates-io))

(define-public crate-kodama-0.1.0 (c (n "kodama") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (k 2)))) (h "0jiwivabfs256y3gdwlcxh8lcjgfxqfngzy9xrapawzcz457d6d1")))

(define-public crate-kodama-0.1.1 (c (n "kodama") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (k 2)))) (h "069p02s4bbnqf8yy1nd4b026ndps40pzgbgxbhfh2kplvqndk795")))

(define-public crate-kodama-0.2.0 (c (n "kodama") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (k 2)))) (h "1jzhyjr04b8099rivzwa5vs933kr5nardz9y7lz3c0g7m3sdgk22")))

(define-public crate-kodama-0.2.1 (c (n "kodama") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (k 2)))) (h "12ppgxlfgshb5hr1ywz4whfhbmqxdx7cyviz57yc0y5h469pr76j")))

(define-public crate-kodama-0.2.2 (c (n "kodama") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "00qjqhjz4dfz39pqk4nwgxlkmjslipa3qbdlwc9q85ylnzhkhh57")))

(define-public crate-kodama-0.2.3 (c (n "kodama") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "14chipddp2w5s5638aawzva5kyddr5yxnll1wffg9ys43akz6i3s")))

(define-public crate-kodama-0.3.0 (c (n "kodama") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "10mgknzw6vnhn9a42dfhb609rwn5cwk2652wj91ch0s7xp7gws7q")))

