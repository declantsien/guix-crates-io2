(define-module (crates-io ko da kodama-capi) #:use-module (crates-io))

(define-public crate-kodama-capi-0.1.0 (c (n "kodama-capi") (v "0.1.0") (d (list (d (n "kodama") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03xxk0xx1ng3i6q57rz1d7c7503cs93hvv794kc03yzz72hiz6n4")))

(define-public crate-kodama-capi-0.1.1 (c (n "kodama-capi") (v "0.1.1") (d (list (d (n "kodama") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1acc0cmspyfchmjm6gyq20drqifb1m1ajjf50cq26afjdrprwcqv")))

(define-public crate-kodama-capi-0.1.2 (c (n "kodama-capi") (v "0.1.2") (d (list (d (n "kodama") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0y1gk4cgbbw3an5pkgh1mjpjy6pg1msrp0wpxmih06x57mxqwsid")))

(define-public crate-kodama-capi-0.1.3 (c (n "kodama-capi") (v "0.1.3") (d (list (d (n "kodama") (r "^0.2.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ak06lg8qbfxknc38ji0dw8fl8bzlcvrn3lmpisv0qvlh3zd1k29")))

(define-public crate-kodama-capi-0.2.0 (c (n "kodama-capi") (v "0.2.0") (d (list (d (n "kodama") (r "^0.3.0") (d #t) (k 0)))) (h "0ybp32q8yxnb37dmbplwaz78c5m4gdjvifqi6c2m4xx3j1r6q8ih")))

