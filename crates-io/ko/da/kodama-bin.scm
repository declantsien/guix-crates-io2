(define-module (crates-io ko da kodama-bin) #:use-module (crates-io))

(define-public crate-kodama-bin-0.1.0 (c (n "kodama-bin") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.4") (d #t) (k 0)) (d (n "kodama") (r "^0.1.0") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1wjyvdimj4dg38yl30wf7hn9hyci5lh17l3qj0ay5c75i029af7l")))

(define-public crate-kodama-bin-0.1.1 (c (n "kodama-bin") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "kodama") (r "^0.1.1") (d #t) (k 0)) (d (n "rayon") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "05jxng5sxpxg4pg8w0v1amp8lmrdkw3b57cixig91hpf9nrcpaxs")))

(define-public crate-kodama-bin-0.1.2 (c (n "kodama-bin") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "kodama") (r "^0.2.2") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0gm1fv6x6x8mpb4avaiqimykjyydvdxyv0xbimdmaym7wj1d5ibj")))

