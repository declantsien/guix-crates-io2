(define-module (crates-io ko ci kociemba) #:use-module (crates-io))

(define-public crate-kociemba-0.5.0 (c (n "kociemba") (v "0.5.0") (d (list (d (n "bincode") (r "^2.0.0-rc") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "spinners") (r "^4.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "1kfn9vb604408iwqaymaq6gmv3vmm6fids7ky9zbh6fymr3cgpl1") (y #t)))

(define-public crate-kociemba-0.5.1 (c (n "kociemba") (v "0.5.1") (d (list (d (n "bincode") (r "^2.0.0-rc") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "spinners") (r "^4.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "03g4aqn3gl4pia5rz3krr0w6vfik6xalj6pk3xsmzk1gdrr4bhgf")))

(define-public crate-kociemba-0.5.2 (c (n "kociemba") (v "0.5.2") (d (list (d (n "bincode") (r "^2.0.0-rc") (f (quote ("serde"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "tower-http") (r "^0.5.2") (d #t) (k 0)))) (h "1ywld1i97zfgl8acv7pwsqgav2ga9fxhrgf7v44h3d4jf5v5dial")))

