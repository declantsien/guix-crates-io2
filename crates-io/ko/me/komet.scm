(define-module (crates-io ko me komet) #:use-module (crates-io))

(define-public crate-komet-0.0.1 (c (n "komet") (v "0.0.1") (h "1ymx851x16n072x9cwn8xms8cvbyjjgp8mqiz7bbn6fqcc5szk5r")))

(define-public crate-komet-0.0.2 (c (n "komet") (v "0.0.2") (h "0qxc2kbywb7ksdjfgjvqyf3zj6gggpmkc7r9gnz1admmnvvrf4vq")))

(define-public crate-komet-0.0.3 (c (n "komet") (v "0.0.3") (h "0ki1wvba62n4hm8nd9dzg2ib57xic7z0vcngc3vflbqrqfpzdsf6")))

