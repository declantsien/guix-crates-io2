(define-module (crates-io ko op koopa) #:use-module (crates-io))

(define-public crate-koopa-0.0.1 (c (n "koopa") (v "0.0.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "intrusive-collections") (r "^0.9") (d #t) (k 0)) (d (n "phf") (r "^0.9") (f (quote ("macros"))) (d #t) (k 0)))) (h "1hfvms4a5bml0j1fajsyahm8glq85csm07n5jcfdk8nflmywxw0h") (f (quote (("no-front-logger"))))))

(define-public crate-koopa-0.0.3 (c (n "koopa") (v "0.0.3") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "key-node-list") (r "^0.0.3") (d #t) (k 0)))) (h "1pnahmdczmxnzfbhr6jjswsn9gyj5nwig9pm71b1lggxs5hn7471") (f (quote (("no-front-logger"))))))

(define-public crate-koopa-0.0.4 (c (n "koopa") (v "0.0.4") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "key-node-list") (r "^0.0.4") (d #t) (k 0)))) (h "0kxb7asrikd646gbvfs9nmzi8c9hl84nkk089dybbw94w3rpchbv") (f (quote (("no-front-logger"))))))

(define-public crate-koopa-0.0.5 (c (n "koopa") (v "0.0.5") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "key-node-list") (r "^0.0.4") (d #t) (k 0)))) (h "1bxchsn9r9n0hxnlk4qdzgp5jlxm65371n6ly0kah8ld7s2x85bq") (f (quote (("no-front-logger"))))))

(define-public crate-koopa-0.0.6 (c (n "koopa") (v "0.0.6") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "key-node-list") (r "^0.0.5") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 2)))) (h "0npd1c4ynw5xii960xs1bjccgz5izjqvkf93gbcrzdnciyls79wg") (f (quote (("no-front-logger"))))))

(define-public crate-koopa-0.0.7 (c (n "koopa") (v "0.0.7") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "key-node-list") (r "^0.0.5") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 2)))) (h "0nv0k2jrdj4c3nqfdrzq640hkwb8xnxbvjvz747w84k3yqhi814m") (f (quote (("no-front-logger"))))))

