(define-module (crates-io ko ka kokai) #:use-module (crates-io))

(define-public crate-kokai-0.1.0 (c (n "kokai") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git2") (r "^0.13") (f (quote ("vendored-openssl"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1i03jim5xaw01m001r74pszvlfqjj7gwk9c3cnmd2bv7pv9swmk9")))

(define-public crate-kokai-0.2.0 (c (n "kokai") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git2") (r "^0.13") (f (quote ("vendored-openssl"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0c85pvl5gb19ra5c1hwjhappyz6rzkdknarwjmnhhadx1lbjcxvp")))

(define-public crate-kokai-0.3.0 (c (n "kokai") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git2") (r "^0.13") (f (quote ("vendored-openssl"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1h3jlzcxjlf2z0dd9sr3csdnvjpmmsl7qdz2dr76fwvls8d1q03x")))

(define-public crate-kokai-0.3.1 (c (n "kokai") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "git2") (r "^0.13") (f (quote ("vendored-openssl"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "11z1gamvb6578w39n0x1lscirq3g779ixvwjhh4p6l6rqs571zks")))

