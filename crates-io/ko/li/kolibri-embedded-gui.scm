(define-module (crates-io ko li kolibri-embedded-gui) #:use-module (crates-io))

(define-public crate-kolibri-embedded-gui-0.0.0-alpha.1 (c (n "kolibri-embedded-gui") (v "0.0.0-alpha.1") (d (list (d (n "embedded-graphics") (r "^0.8") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.5.0") (d #t) (k 2)) (d (n "embedded-iconoir") (r "^0.2.2") (f (quote ("all-resolutions"))) (d #t) (k 0)) (d (n "heapless") (r "^0.7") (f (quote ("serde" "ufmt-impl"))) (d #t) (k 0)))) (h "0xxyndwnjx23xq15jl3p1xv9zwbk6x00b5hin6d6qnrqndh7lsid")))

