(define-module (crates-io ko nf konfiguration) #:use-module (crates-io))

(define-public crate-konfiguration-0.1.0 (c (n "konfiguration") (v "0.1.0") (d (list (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03i2kqb8a0fmf4sw10swnfq5bmf7z1mj92i837kjd94jvfhi0163") (r "1.60")))

(define-public crate-konfiguration-0.2.0 (c (n "konfiguration") (v "0.2.0") (d (list (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bpi8ga9y3vhfxjlb90fw88xk41v1vwrs4090g10f52njnlm8xbi") (r "1.60")))

(define-public crate-konfiguration-0.2.1 (c (n "konfiguration") (v "0.2.1") (d (list (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0aix09l75n1fyb98z7idvx7488icfbxli7ym94j2rm6izwsjnxip") (r "1.60")))

(define-public crate-konfiguration-0.2.2 (c (n "konfiguration") (v "0.2.2") (d (list (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13ff12liihzapkq652hk4mwsgihqm6pac9cszjjz0qa3j9p47f9r") (r "1.60")))

(define-public crate-konfiguration-1.0.0 (c (n "konfiguration") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde-untagged") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.0") (d #t) (k 0)))) (h "0l8w56l9ch106va0il9v3wzzgpx2yaipbp5iqiliqzb9k4nbhi8x") (r "1.60")))

(define-public crate-konfiguration-1.0.1 (c (n "konfiguration") (v "1.0.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde-untagged") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.0") (d #t) (k 0)))) (h "164qlm8rpkxr2c0c0xdhpv9pb3l44gb6zvkakfyq20q1z6mifig8") (r "1.66.1")))

(define-public crate-konfiguration-1.1.0 (c (n "konfiguration") (v "1.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde-untagged") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.0") (d #t) (k 0)))) (h "0lakbfpp6l84ivvpphg64kn8njb10c0wfkf72lf5l3nd48nizl1v") (r "1.66.1")))

(define-public crate-konfiguration-1.2.0 (c (n "konfiguration") (v "1.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde-untagged") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.0") (d #t) (k 0)))) (h "14i0vjx0hk4vc7f8r9hb8ppv6fbxl5x17vnx9xhircqqa0hn2nw1") (r "1.66.1")))

