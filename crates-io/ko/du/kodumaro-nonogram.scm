(define-module (crates-io ko du kodumaro-nonogram) #:use-module (crates-io))

(define-public crate-kodumaro-nonogram-3.0.0 (c (n "kodumaro-nonogram") (v "3.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fastrand") (r "^1.9") (d #t) (k 0)) (d (n "resolution") (r "^0.1") (d #t) (k 0)) (d (n "rscenes") (r "^1.2") (d #t) (k 0)) (d (n "static_init") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1ag2qgiwapz137j2wc8kfxav3a06gy33z8sacfq8vfsqbw7phxvk")))

(define-public crate-kodumaro-nonogram-3.0.1 (c (n "kodumaro-nonogram") (v "3.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fastrand") (r "^1.9") (d #t) (k 0)) (d (n "resolution") (r "^0.1") (d #t) (k 0)) (d (n "rscenes") (r "^1.2") (d #t) (k 0)) (d (n "static_init") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0h5857ab8hggqgmf5s57ag0cnmv7shyd9k03i8sqa8m1qpcjaw6g")))

(define-public crate-kodumaro-nonogram-3.0.2 (c (n "kodumaro-nonogram") (v "3.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fastrand") (r "^1.9") (d #t) (k 0)) (d (n "resolution") (r "^0.1") (d #t) (k 0)) (d (n "rscenes") (r "^1.2") (d #t) (k 0)) (d (n "static_init") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0pmivqz6ws8mhh5pm28nidiq9m0c3kdbqmrmh8lglfm5n3w2klf2")))

(define-public crate-kodumaro-nonogram-3.0.3 (c (n "kodumaro-nonogram") (v "3.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fastrand") (r "^1.9") (d #t) (k 0)) (d (n "resolution") (r "^0.1") (d #t) (k 0)) (d (n "rscenes") (r "^1.2") (d #t) (k 0)) (d (n "static_init") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1cbz3mn6f4yi7k4ynssjmdfc3zkwg9l48i4y1g54apkyk1a57vjp")))

(define-public crate-kodumaro-nonogram-3.0.4 (c (n "kodumaro-nonogram") (v "3.0.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "fastrand") (r "^2.0") (d #t) (k 0)) (d (n "resolution") (r "^0.1") (d #t) (k 0)) (d (n "rscenes") (r "^1.4") (f (quote ("eyre"))) (k 0)) (d (n "static_init") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0y1zyap3639ga48gc7393hmpcwbg4gyx1q41fyj2nnjraq0bw9s9")))

(define-public crate-kodumaro-nonogram-3.0.5 (c (n "kodumaro-nonogram") (v "3.0.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "fastrand") (r "^2.0") (d #t) (k 0)) (d (n "resolution") (r "^0.1") (d #t) (k 0)) (d (n "rscenes") (r "^1.4") (f (quote ("eyre"))) (k 0)) (d (n "static_init") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "13p6w5a1jm55h9yrwyslcf9svldywp6i0njyv7rdrj08m9xkwgz3")))

(define-public crate-kodumaro-nonogram-3.0.6 (c (n "kodumaro-nonogram") (v "3.0.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "fastrand") (r "^2.0") (d #t) (k 0)) (d (n "resolution") (r "^0.1") (d #t) (k 0)) (d (n "rscenes") (r "^1.4") (f (quote ("eyre"))) (k 0)) (d (n "static_init") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0z68nqhsaa3mzjqydibw6iv2007as2q1i0fx1d973394bfms7r2r")))

(define-public crate-kodumaro-nonogram-3.0.7 (c (n "kodumaro-nonogram") (v "3.0.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "fastrand") (r "^2.0") (d #t) (k 0)) (d (n "resolution") (r "^0.1") (d #t) (k 0)) (d (n "rscenes") (r "^1.4") (f (quote ("eyre"))) (k 0)) (d (n "static_init") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1wr99k22ahvjzbrpm242v8ivr25xv7yjzbqzmhxy17malgwsln7l")))

(define-public crate-kodumaro-nonogram-4.0.0 (c (n "kodumaro-nonogram") (v "4.0.0") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 0)) (d (n "rscenes") (r "^2.0") (f (quote ("fake-fs" "storage"))) (d #t) (k 0)))) (h "00c96xfw2zp0fqjpp6flf1dknk8g0vdirgdcm0kmh3jhinrkv5lc")))

(define-public crate-kodumaro-nonogram-4.0.1 (c (n "kodumaro-nonogram") (v "4.0.1") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 0)) (d (n "rscenes") (r "^2.0") (f (quote ("fake-fs" "storage"))) (d #t) (k 0)))) (h "0v3gnfz99dgpmqfvrddzwv4k3wiq2bhx1f9ija3fimwqqhwslykr")))

(define-public crate-kodumaro-nonogram-4.0.2 (c (n "kodumaro-nonogram") (v "4.0.2") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 0)) (d (n "rscenes") (r "^2.0") (f (quote ("fake-fs" "storage"))) (d #t) (k 0)))) (h "0s3hqnyasrp2xpf88lzjvc2gic4h3vyvplrxps4hbrhj68xjdjvk")))

(define-public crate-kodumaro-nonogram-4.0.3 (c (n "kodumaro-nonogram") (v "4.0.3") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 0)) (d (n "rscenes") (r "^2.0") (f (quote ("fake-fs" "storage"))) (d #t) (k 0)))) (h "09bp3hmcbg22xp01x4h9mmhcz2c5cnyq5rf5wagghpbr8ja1y7vl")))

(define-public crate-kodumaro-nonogram-4.0.4 (c (n "kodumaro-nonogram") (v "4.0.4") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 0)) (d (n "rscenes") (r "^2.0") (f (quote ("fake-fs" "storage"))) (d #t) (k 0)))) (h "1wh30riifvl9lwrqbznjhlxx3p1iyk59k49d4zn72ycw3bg0glxi")))

(define-public crate-kodumaro-nonogram-4.0.5 (c (n "kodumaro-nonogram") (v "4.0.5") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 0)) (d (n "rscenes") (r "^2.0") (f (quote ("storage"))) (d #t) (k 0)))) (h "0x28cn79ji6z803r6312xlrh9pi18wdhxq78a7azmrg17p0y3gv8")))

(define-public crate-kodumaro-nonogram-4.0.6 (c (n "kodumaro-nonogram") (v "4.0.6") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 0)) (d (n "rscenes") (r "^2.0") (f (quote ("storage"))) (d #t) (k 0)))) (h "0pbh0m715xy1rllqbmlgbs9jwrdgn7wkk36r1lpc7m5kziw7zxz8")))

(define-public crate-kodumaro-nonogram-4.0.7 (c (n "kodumaro-nonogram") (v "4.0.7") (d (list (d (n "fastrand") (r "^2.0") (d #t) (k 0)) (d (n "rscenes") (r "^2.0") (f (quote ("storage"))) (d #t) (k 0)))) (h "1hgdg1kcw3sfrjr0q2zs9k4dwli1v7zb5jh7pdcv7y3fjb8swnc2")))

