(define-module (crates-io ko du kodumaro-clock) #:use-module (crates-io))

(define-public crate-kodumaro-clock-1.0.1 (c (n "kodumaro-clock") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "raylib") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13hc90kw4fqjd18fsvyaka3qzjnalk57varhllqa9iqkmhb52hdk")))

(define-public crate-kodumaro-clock-1.0.2 (c (n "kodumaro-clock") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "raylib") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "18pihhn9nn3r5x3hqy6fwinc281532jnc7gpr1wpdnfcw58v32vz")))

(define-public crate-kodumaro-clock-1.0.3 (c (n "kodumaro-clock") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "raylib") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0gxc0z6x4466p204wli58kpzxa5ll7c1q8zkz7sri8l97idrhdlf")))

(define-public crate-kodumaro-clock-1.0.4 (c (n "kodumaro-clock") (v "1.0.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "raylib") (r "^3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0040jswk0bqpk6iqv0hqx0z2dg00wjfsz2pssvgkrmy6s5yqr06y")))

