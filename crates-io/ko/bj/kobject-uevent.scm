(define-module (crates-io ko bj kobject-uevent) #:use-module (crates-io))

(define-public crate-kobject-uevent-0.1.0 (c (n "kobject-uevent") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "netlink-sys") (r "^0.7") (d #t) (k 2)))) (h "038n4c06pk9v1y4xfp005887snf2vsha7ns8aw1m3gky3hxfr0n1")))

(define-public crate-kobject-uevent-0.1.1 (c (n "kobject-uevent") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "netlink-sys") (r "^0.8.3") (d #t) (k 2)))) (h "1aqwnsqx3p28v3p7ykqyzqd4sn9hc52amy3jb55xranrg4lynlh5")))

