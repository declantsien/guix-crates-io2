(define-module (crates-io ko mb kombi) #:use-module (crates-io))

(define-public crate-kombi-0.1.0 (c (n "kombi") (v "0.1.0") (h "007cjw19g00c082iq08cb4csni0jf8695f5bf6csq9r7pk183lpa")))

(define-public crate-kombi-0.1.1 (c (n "kombi") (v "0.1.1") (h "0zlxzs1cp6a4iq90n1p1dwfqy131fyl4n28vsylji3dim42a64zx")))

(define-public crate-kombi-0.1.2 (c (n "kombi") (v "0.1.2") (h "0iymxb0v4ix6zyvmwc7i92lnkpdw94z1zdnjdiq7ncv5vqwndp4q")))

(define-public crate-kombi-0.1.3 (c (n "kombi") (v "0.1.3") (h "1fzq2kw8sgjj61r3fwr1z9f3ydmijixdpjy74fqvipxiyg5wyyik")))

(define-public crate-kombi-0.2.0 (c (n "kombi") (v "0.2.0") (h "0sfn07b9j6mrivzdyqasjw2vv49mlv2xcm00vmr9kmzdqjq9203v")))

(define-public crate-kombi-0.2.1 (c (n "kombi") (v "0.2.1") (h "129wbs7k42r52vg1n65z057mqi9wbz5559czvmniivc7d58ygkj9")))

(define-public crate-kombi-0.2.2 (c (n "kombi") (v "0.2.2") (h "0vzc088hdj3imdyz6ag91gwz3f7x21rcnar4nbba10bq1f4545bq")))

