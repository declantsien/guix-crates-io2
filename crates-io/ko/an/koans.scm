(define-module (crates-io ko an koans) #:use-module (crates-io))

(define-public crate-koans-0.2.0 (c (n "koans") (v "0.2.0") (h "0k391l01vvf53wi7pkc1p0gbrdmyz0yyr8p4mb2cal2pdw3vnkhv") (y #t)))

(define-public crate-koans-0.3.0 (c (n "koans") (v "0.3.0") (h "02vsswic8zgzm95fa1pyij2y4xqwanckfpp1vbydvzdw31qn9xk5") (y #t)))

