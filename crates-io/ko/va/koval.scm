(define-module (crates-io ko va koval) #:use-module (crates-io))

(define-public crate-koval-0.1.0 (c (n "koval") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "04cv93drls86n0j7fwi81bdnk6iypi9z1brscak3h8ckifmh520q")))

(define-public crate-koval-0.2.0 (c (n "koval") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "12y9nihy1lazxk49c8h333mynpw9ifclqjx2jbzswh97s4g144n1")))

(define-public crate-koval-0.3.0 (c (n "koval") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1vdhrqhmg2svw4rb107kngh079bfqfyns6b30wm7623095pn9xcr")))

(define-public crate-koval-0.4.0 (c (n "koval") (v "0.4.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1w58j7vwl37nwmfawii5jagh4l3qm0z756dhw7whqr0a2d7pn8a5")))

(define-public crate-koval-0.5.0 (c (n "koval") (v "0.5.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "190gxhichigwqaqnjjffi6g50by7nh5xzaz8f6h2qgjasiaxk8wy")))

(define-public crate-koval-0.5.1 (c (n "koval") (v "0.5.1") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1i45nayjg4d84g0nm9kxga0w8jwhcmy591b30fr74f7yb9lw2xz9")))

