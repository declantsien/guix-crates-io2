(define-module (crates-io ko la kola) #:use-module (crates-io))

(define-public crate-kola-0.1.0 (c (n "kola") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "ropey") (r "^1.6.1") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt-multi-thread" "macros" "io-std"))) (d #t) (k 0)) (d (n "tower-lsp") (r "^0.20.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.22.6") (d #t) (k 0)) (d (n "tree-sitter-kotlin") (r "^0.3.6") (d #t) (k 0)))) (h "1rbwkw1br6h5p4zvw7mhwa6afxm5i4da18b1f3qmriq659scczjf")))

