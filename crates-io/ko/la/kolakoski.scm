(define-module (crates-io ko la kolakoski) #:use-module (crates-io))

(define-public crate-kolakoski-0.1.0 (c (n "kolakoski") (v "0.1.0") (d (list (d (n "num") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "1vhhr7scxi949wc951i6az236b8j5vswz663957ds5kl45w9ggr9") (f (quote (("num-traits" "num") ("default"))))))

(define-public crate-kolakoski-1.0.0 (c (n "kolakoski") (v "1.0.0") (d (list (d (n "num") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "0xb4q1sj8xj1dzydsdyy4v9si4flaq38mlkp00ymlgkj7arj52ym") (f (quote (("num-traits" "num") ("default"))))))

(define-public crate-kolakoski-1.0.1 (c (n "kolakoski") (v "1.0.1") (d (list (d (n "num") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "0q96csdhgw2r6shqsh9gb9mjp5c2vgqzjxcn31gmxim44gmdhc5j") (f (quote (("num-traits" "num") ("default"))))))

(define-public crate-kolakoski-2.0.0 (c (n "kolakoski") (v "2.0.0") (d (list (d (n "num") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "0vglyyp5jnyqx1759b01pyb4flhw38x3a67f5i4ncnw7a3lz7qn6") (f (quote (("num-traits" "num") ("default"))))))

