(define-module (crates-io ko re korean-lunar-calendar) #:use-module (crates-io))

(define-public crate-korean-lunar-calendar-0.1.0 (c (n "korean-lunar-calendar") (v "0.1.0") (d (list (d (n "chrono") (r "~0") (d #t) (k 0)) (d (n "lazy_static") (r "~1") (d #t) (k 0)))) (h "1yp5lh1hk9wd00mzkp82ijf9pkpwgwl4d0ny4bbimgpnjbr8laj6")))

(define-public crate-korean-lunar-calendar-1.0.0 (c (n "korean-lunar-calendar") (v "1.0.0") (d (list (d (n "chrono") (r "~0") (d #t) (k 0)) (d (n "lazy_static") (r "~1") (d #t) (k 0)))) (h "0q193zvp5f82dqd1g34vxg36vwxggzryzfb43nlzmvl32j3w1dm2")))

