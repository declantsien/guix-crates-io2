(define-module (crates-io ko re koreanbots) #:use-module (crates-io))

(define-public crate-koreanbots-2.0.0 (c (n "koreanbots") (v "2.0.0") (d (list (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1phglsw4fc57sf6h9lp0nhjpb0bqmnn2kavzb1cn5hrglyz5zxb0")))

(define-public crate-koreanbots-2.0.1 (c (n "koreanbots") (v "2.0.1") (d (list (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0407bs6vrkcg0gqac4fcfxw1w0dldii7vmsc66dbnl75pyd5bmqs")))

(define-public crate-koreanbots-2.0.2 (c (n "koreanbots") (v "2.0.2") (d (list (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1i31r9dzw2mgs2vaifc1kdxxjwaajw2mq4lm7ykaacyq6jiv1nn6")))

(define-public crate-koreanbots-2.0.3 (c (n "koreanbots") (v "2.0.3") (d (list (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "02xjd77bmbym0gidw6yrgaf708rwd7i9vd5a8pymm836h7viyhmq")))

