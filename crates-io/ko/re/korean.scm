(define-module (crates-io ko re korean) #:use-module (crates-io))

(define-public crate-korean-0.1.0 (c (n "korean") (v "0.1.0") (d (list (d (n "proptest") (r "^0.9") (d #t) (k 2)))) (h "009fdf7ab1sq1ydwc96gn3rpr9c5cmxq9c4sainv7snpgp2nglpg")))

(define-public crate-korean-0.2.0 (c (n "korean") (v "0.2.0") (d (list (d (n "proptest") (r "^0.9") (d #t) (k 2)))) (h "1wmngg1s67d0nqgxrhqbal8ilj1bhj1s1z36rjxhzb8c6b2vp4h2")))

(define-public crate-korean-0.3.0 (c (n "korean") (v "0.3.0") (h "0nbvdwhrd18qnpy9hyqa2rwn0rmb6219rb8xwij84rx04vca0bar")))

(define-public crate-korean-0.3.1 (c (n "korean") (v "0.3.1") (h "18ix22ppdb9pyxp850bamawpbixzx0qm4bqfsffl7rx2a73hmlc2")))

