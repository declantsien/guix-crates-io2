(define-module (crates-io ko re korero) #:use-module (crates-io))

(define-public crate-korero-0.1.0 (c (n "korero") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0j8l1izsq0f422l2pdnw9gqyj08c77fm8j2b00nhzdfdhbcncdsg")))

(define-public crate-korero-0.1.1 (c (n "korero") (v "0.1.1") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "0bjb0j3hsl9427hdw56p3rniks0ia2qdrwlsijx7a3b5zh9pg42g")))

(define-public crate-korero-0.1.11 (c (n "korero") (v "0.1.11") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "08fz7s1h5xkjp43yaascsyn90rrg67ghxjh4d1rbd9s2sjlk4rdp")))

(define-public crate-korero-0.1.12 (c (n "korero") (v "0.1.12") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1bhnwzb5yqhjdaf3flyy99l2xn2f2q7g2swfzgxxhj5l04khdfgw")))

