(define-module (crates-io ko re korean_regex) #:use-module (crates-io))

(define-public crate-korean_regex-0.1.0 (c (n "korean_regex") (v "0.1.0") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "15c3krp5f435fk1qg3cxmjrilsv09rf328291afzdpkg8dc1a9rm")))

(define-public crate-korean_regex-0.2.0 (c (n "korean_regex") (v "0.2.0") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "067bwidz9a58ivc1agiyirnyg62jy11mf755cry1ddzmw5khmmcv")))

(define-public crate-korean_regex-0.2.1 (c (n "korean_regex") (v "0.2.1") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0a93rzkj3s4xl2jb190fhz333x7l4r3kn97w6dhyyih8ywqppdqz")))

(define-public crate-korean_regex-0.3.0 (c (n "korean_regex") (v "0.3.0") (d (list (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1n504mnil9wl7yw0j1r3w0y0ca0bsdn5f66bwhhn2mz6aj0jmmak")))

