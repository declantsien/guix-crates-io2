(define-module (crates-io ko re koreanbots-rs) #:use-module (crates-io))

(define-public crate-koreanbots-rs-0.1.0 (c (n "koreanbots-rs") (v "0.1.0") (d (list (d (n "ureq") (r "^1.5.1") (f (quote ("json"))) (d #t) (k 0)))) (h "075rjfmfw29ddyzxs8209qf4g9s80h3z0gds437l1z0yma9x9s46") (y #t)))

(define-public crate-koreanbots-rs-0.1.1 (c (n "koreanbots-rs") (v "0.1.1") (d (list (d (n "ureq") (r "^1.5.1") (f (quote ("json"))) (d #t) (k 0)))) (h "0lafd70q5yyrnwbsl4x80qzgl10lrr6y4w28q6c5q257098j4lya")))

(define-public crate-koreanbots-rs-0.2.0 (c (n "koreanbots-rs") (v "0.2.0") (d (list (d (n "ureq") (r "^1.5.1") (f (quote ("json"))) (d #t) (k 0)))) (h "1ccsrllkx77namjp0d4p1ag1g21zqry3j7zacqfgkbbv8n8qr7lf")))

(define-public crate-koreanbots-rs-0.2.1 (c (n "koreanbots-rs") (v "0.2.1") (d (list (d (n "ureq") (r "^1.5.1") (f (quote ("json"))) (d #t) (k 0)))) (h "1snzcqd7dj9463pgvy2am4lld4wzm1wwbzcfcv8nlcz0fbdwhk65")))

