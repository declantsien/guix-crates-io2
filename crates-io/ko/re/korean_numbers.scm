(define-module (crates-io ko re korean_numbers) #:use-module (crates-io))

(define-public crate-korean_numbers-0.4.0 (c (n "korean_numbers") (v "0.4.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "1z2c8c2yzx09hl6pz11hslys1rrn3nizmr24z5iq1i70a0fpbh57")))

(define-public crate-korean_numbers-0.4.1 (c (n "korean_numbers") (v "0.4.1") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "1z0s4xz3pwcxw50xhx7ggsxrz50ip24f2zlsg9v133p6c1hkdzyz")))

(define-public crate-korean_numbers-0.4.2 (c (n "korean_numbers") (v "0.4.2") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "0i2nfxazl2n94lvxkrg17zv2inl8rbc0ngq211kjyq9183r3wc9s")))

(define-public crate-korean_numbers-0.5.0 (c (n "korean_numbers") (v "0.5.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "0a7420m69fpnfzqx6yxapr6vqwgnva8c16vilwr9id4lpa4rha6r")))

(define-public crate-korean_numbers-0.5.1 (c (n "korean_numbers") (v "0.5.1") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "17aixyg0jwlyrpxdq0fv6cshi1jji7yx1jyp5084kakdpc86p83y")))

(define-public crate-korean_numbers-0.5.2 (c (n "korean_numbers") (v "0.5.2") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "1bxn24dscf579h1r4qzwm2mnwbvxm2zdlsx5rwppdi1xbg5c1qk0")))

(define-public crate-korean_numbers-0.5.3 (c (n "korean_numbers") (v "0.5.3") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "11vqyy869zn19galiafb23rdaamvn28nq9yxy40vcakfhjgjyqsk")))

(define-public crate-korean_numbers-0.5.5 (c (n "korean_numbers") (v "0.5.5") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "12jdl0cj26bc1hq6a69r0vx02329sav0npd9h57ighscms8qky1v")))

(define-public crate-korean_numbers-0.6.1 (c (n "korean_numbers") (v "0.6.1") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "18vskczxpj16nn5d7kms685ga50m2fk4srm4wxa59w8qalvrfvmw")))

(define-public crate-korean_numbers-0.6.2 (c (n "korean_numbers") (v "0.6.2") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0vygq7ggxg1b4c55qkm5rqsz79lk8n7qmnva7dnzhsa64gx6b1pi")))

(define-public crate-korean_numbers-0.6.4 (c (n "korean_numbers") (v "0.6.4") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1iyss24hc3ppxdlkr2h3c8wszydp0k84zkf4c08xr58jpqbq2rr5")))

(define-public crate-korean_numbers-0.6.5 (c (n "korean_numbers") (v "0.6.5") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0bvvvlcs5p863wxhz1q7zjk85k8zjw3k2g4fh9v5dp928rvg6b6i")))

(define-public crate-korean_numbers-0.6.6 (c (n "korean_numbers") (v "0.6.6") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "16lc4v47bcafn60j4l7kwsbnb49j3n4lnash7hgyc13w6amgs7aj")))

(define-public crate-korean_numbers-0.6.7 (c (n "korean_numbers") (v "0.6.7") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1ai2w475r5iwwg4wh0ipv58mpaxkbdwfwwdpyp4cnl8f3p2yax1b")))

