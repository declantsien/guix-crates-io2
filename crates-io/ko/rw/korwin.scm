(define-module (crates-io ko rw korwin) #:use-module (crates-io))

(define-public crate-korwin-0.1.0 (c (n "korwin") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1sw25a8ix312ymh6rxrwrpgqj6158vl0p53yah0ff27bwhgynkf1") (y #t)))

(define-public crate-korwin-0.2.0 (c (n "korwin") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1p68z8sar3papi5pxzbdsi83ma7j95dzsv0cgf5dpa6kxxdqlfxd") (y #t)))

(define-public crate-korwin-0.3.0 (c (n "korwin") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1nbwgawnlipri5zbnnvawc95d8f6gr0f33sm3ngp51h0zwmn95wz") (y #t)))

(define-public crate-korwin-0.4.0 (c (n "korwin") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "15ncahvbcvmgalbzsns2x1cyg7543si9vd9jc82xlxvm5mld0cp4")))

