(define-module (crates-io m- bu m-bus-parser-cli) #:use-module (crates-io))

(define-public crate-m-bus-parser-cli-0.0.1 (c (n "m-bus-parser-cli") (v "0.0.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "m-bus-parser") (r "^0.0.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)))) (h "04402hmiavp715ppsk874ip7msqsf7krk9qyjfywphfhazw01z77")))

