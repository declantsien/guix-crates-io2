(define-module (crates-io b6 #{4-}# b64-ct) #:use-module (crates-io))

(define-public crate-b64-ct-0.1.0 (c (n "b64-ct") (v "0.1.0") (d (list (d (n "paste") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "13pdz5qid5dm9makjnn8smlq19jxfcrk5z8ff291x28yx5rb31rv") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-b64-ct-0.1.1 (c (n "b64-ct") (v "0.1.1") (d (list (d (n "paste") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1hwd7ahnxbk3lbyy8p070gr870fgjby29q3gxzy8g9nwpzp8s7mc") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-b64-ct-0.1.2 (c (n "b64-ct") (v "0.1.2") (d (list (d (n "paste") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1286fgra0dg9scpn5qhg6wwl05nw0m1yyx0psdnl8d72jmd1w9ij") (f (quote (("std") ("nightly") ("default" "std"))))))

