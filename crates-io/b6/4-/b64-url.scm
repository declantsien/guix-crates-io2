(define-module (crates-io b6 #{4-}# b64-url) #:use-module (crates-io))

(define-public crate-b64-url-0.1.0 (c (n "b64-url") (v "0.1.0") (h "0vli7d0q7k6zwa5cpikpkrh75bm2r396rmm56ni93gr343qxv750")))

(define-public crate-b64-url-0.2.0 (c (n "b64-url") (v "0.2.0") (h "0lh21r8v3rklsk2sa97jv55k3cxz019ccwlj9z8yslwjlkyizwk5")))

(define-public crate-b64-url-0.3.0 (c (n "b64-url") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1jwx4c6ck3595pn1khfld4rwmjd77j2wfnjadqmql3w1bkpb2ihw")))

(define-public crate-b64-url-0.4.0 (c (n "b64-url") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "143z0w07x3738fsmjrbjac586g9zwwkrcrqd5lnzw3p3pi03nplj")))

(define-public crate-b64-url-0.5.0 (c (n "b64-url") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0v6c3ddf9mkpdmzd44rmrqvydn0f2bfx66d089dv12sb1xmjahis")))

