(define-module (crates-io b6 #{4-}# b64-rs) #:use-module (crates-io))

(define-public crate-b64-rs-1.0.0 (c (n "b64-rs") (v "1.0.0") (h "0isi389ncgs9ipyf8djk6rqjqgv276nzwsldh6ycz5kdbqh42ggq") (y #t)))

(define-public crate-b64-rs-1.0.1 (c (n "b64-rs") (v "1.0.1") (h "0vizf9fk4lwh9q824bvxzd85a5x00314lk3pfmv84avacbpay0m9") (y #t)))

(define-public crate-b64-rs-1.0.2 (c (n "b64-rs") (v "1.0.2") (h "0775zbxh0drj5w257kdv0rxp1clrnm8ba5cmdc350bfl50gxd303")))

(define-public crate-b64-rs-1.0.3 (c (n "b64-rs") (v "1.0.3") (h "19xr7k3i67a3lmk11snbsfj09wsnwybjh48h0qix5ln0psmjjyji")))

