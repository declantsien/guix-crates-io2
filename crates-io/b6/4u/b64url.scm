(define-module (crates-io b6 #{4u}# b64url) #:use-module (crates-io))

(define-public crate-b64url-0.1.0 (c (n "b64url") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "1jpissxw6nw1bzk6kfb83acy2y76jwsgc80gvrnalypqddh0ia3g")))

(define-public crate-b64url-0.1.1 (c (n "b64url") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "08n6hkm9fvrncl6i4cvd283yj2i2wzjvxaf79cbb8hbricjvjwp2")))

