(define-module (crates-io b6 #{4c}# b64ct) #:use-module (crates-io))

(define-public crate-b64ct-0.1.0 (c (n "b64ct") (v "0.1.0") (h "0cj0wgbbis9hnklmrsdsiiky82x3n2a5ajiw7bqd6pridp1sv4x2") (f (quote (("std" "alloc") ("alloc")))) (y #t)))

(define-public crate-b64ct-0.2.0 (c (n "b64ct") (v "0.2.0") (h "0vknwvmqfm9cn4d36gwg30dnkb1dpipyrgnrq0vmz4p9z6rz8kaj") (f (quote (("std" "alloc") ("alloc")))) (y #t)))

