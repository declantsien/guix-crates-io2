(define-module (crates-io rv em rvemu) #:use-module (crates-io))

(define-public crate-rvemu-0.0.1 (c (n "rvemu") (v "0.0.1") (d (list (d (n "num-bigint") (r "^0.2.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "1v947b6f32lfp2cj2g8xss9fq079qwy96hvbmasl6528yzxw6nba")))

(define-public crate-rvemu-0.0.2 (c (n "rvemu") (v "0.0.2") (d (list (d (n "num-bigint") (r "^0.2.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "1i41l2lkqgx3aivcj0hd44h4c10ncj94gvf0f8vn6byd7ylv8yfx")))

(define-public crate-rvemu-0.0.3 (c (n "rvemu") (v "0.0.3") (d (list (d (n "num-bigint") (r "^0.2.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)))) (h "1f9rb6fw3vrag089ix3i2f6sa8n4jr5hd5b9hxvghw41pwq457xa")))

(define-public crate-rvemu-0.0.4 (c (n "rvemu") (v "0.0.4") (h "0jrl5l3pv71y5435cc132zpyscwsdaxf2ldc6mqnlsa1p5rfdmdh")))

(define-public crate-rvemu-0.0.5 (c (n "rvemu") (v "0.0.5") (d (list (d (n "wasm-bindgen") (r "^0.2.59") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3.36") (f (quote ("Document" "Element" "HtmlElement" "Node" "Window"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1bpkmvdg9dpr5zy78f7gwf16g2lcvs30dr8g71c3sfq6iqgwgv4f")))

(define-public crate-rvemu-0.0.6 (c (n "rvemu") (v "0.0.6") (d (list (d (n "wasm-bindgen") (r "^0.2.59") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3.36") (f (quote ("Document" "Element" "HtmlElement" "Node" "Window"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1ji2yzijzsx7ib1czvv5sw0iv6dg3kaqhiwgh1hnckczmjc3phh1")))

(define-public crate-rvemu-0.0.7 (c (n "rvemu") (v "0.0.7") (d (list (d (n "wasm-bindgen") (r "^0.2.59") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3.36") (f (quote ("Document" "Element" "HtmlElement" "Node" "Window"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1xyvds8lqyvc26307bca2v5z935s2nmbjyf4v2alr2hkkqn9h5cj")))

(define-public crate-rvemu-0.0.8 (c (n "rvemu") (v "0.0.8") (d (list (d (n "wasm-bindgen") (r "^0.2.59") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3.36") (f (quote ("Document" "Element" "HtmlElement" "Node" "Window"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0dkmsfd5svk6z77js3fnamnkyd30vwgakrznf25kaqy0qf7fhcsv")))

(define-public crate-rvemu-0.0.9 (c (n "rvemu") (v "0.0.9") (d (list (d (n "wasm-bindgen") (r "^0.2.59") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3.36") (f (quote ("Document" "Element" "HtmlElement" "Node" "Window"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "16sahvr9bbvhk993pnnjl8lwcf46l7zxkrr0fz6jsvihzcpm89gm")))

(define-public crate-rvemu-0.0.10 (c (n "rvemu") (v "0.0.10") (d (list (d (n "js-sys") (r "^0.3.36") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2.59") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.9") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3.36") (f (quote ("Document" "Element" "HtmlElement" "Node" "Window"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0xxqqxbqdrz8az9kzir5z290k2kv8axlhyfwrj2lg1i3xyfzqd89")))

(define-public crate-rvemu-0.0.11 (c (n "rvemu") (v "0.0.11") (d (list (d (n "js-sys") (r "^0.3.50") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2.73") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.23") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3.50") (f (quote ("Document" "Element" "HtmlElement" "Node" "Window"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0avk1s2zm2gf5ibxrj771fl0c7ykvmzrl6clym8b0yg66w0jvkz2")))

