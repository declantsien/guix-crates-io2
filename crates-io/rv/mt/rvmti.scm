(define-module (crates-io rv mt rvmti) #:use-module (crates-io))

(define-public crate-rvmti-0.1.0 (c (n "rvmti") (v "0.1.0") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)) (d (n "jvmti-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0a3w8xvnbv1mr0fdc7y0biqpzg0csjc2qn4ca3n5kz0k13nc84w1") (y #t)))

(define-public crate-rvmti-0.1.1 (c (n "rvmti") (v "0.1.1") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)))) (h "0r25nzccpzj1sw6rlg4b54z3rvb3s8snsh72y8a8f5lmz0zlgg06") (y #t)))

(define-public crate-rvmti-0.1.2 (c (n "rvmti") (v "0.1.2") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)))) (h "090jgz5id26456nr3jl6jxmih0hvi7vb86hsg3cl76ksxzfd9493") (y #t)))

(define-public crate-rvmti-0.1.3 (c (n "rvmti") (v "0.1.3") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)))) (h "0n5whs90n45pwyzq0a04a2r29bdnrnqkv45sp2814fcjq1rkgrwc") (y #t)))

(define-public crate-rvmti-0.1.4 (c (n "rvmti") (v "0.1.4") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)))) (h "0jglj0ingm2qysznhcxgfhhajsl5219kr1g6j7s7wvc15w5zsc7j") (y #t)))

(define-public crate-rvmti-0.1.5 (c (n "rvmti") (v "0.1.5") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)))) (h "1dy1bbk3i19zkhq6i66rb2ibyfxajl32kk0n822wvpd9il29idav") (y #t)))

(define-public crate-rvmti-0.1.6 (c (n "rvmti") (v "0.1.6") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)))) (h "0pkzhiwld2fa47lzzb3fms2raiblfiabr6rwkjdca40bw1avpf83") (y #t)))

(define-public crate-rvmti-0.1.7 (c (n "rvmti") (v "0.1.7") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)))) (h "0njvq7jwzg2wkfni1ys8qlm7xp9dcq5hmkn50cbhy5zbnwxgizxs") (y #t)))

(define-public crate-rvmti-0.1.8 (c (n "rvmti") (v "0.1.8") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)))) (h "1wmc8sdm0kcb62va3clr9a4nycl916vs91ax69zckm8jm5ma2b4l") (y #t)))

(define-public crate-rvmti-0.1.9 (c (n "rvmti") (v "0.1.9") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)))) (h "1r6i2hwvfq3nky3kh2liwnmp9ig9jwpxsb5w9rvfvd7hvfm9985v") (y #t)))

(define-public crate-rvmti-0.2.0 (c (n "rvmti") (v "0.2.0") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)))) (h "09v9amlmxq9d9wb8hixpywdcj9bf21w824lsjcyqlqwshq8y5f9m") (y #t)))

(define-public crate-rvmti-0.2.1 (c (n "rvmti") (v "0.2.1") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)))) (h "0bqhz6zqizwx7fmf9pcih83zgiplqx951rdvqs5w6dkrc57b2lkk") (y #t)))

(define-public crate-rvmti-0.2.2 (c (n "rvmti") (v "0.2.2") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)))) (h "0gzsaq06mzfihd1g72j8gx0jl8l3ax2wnlhgljhdcrl049vjal9x") (y #t)))

(define-public crate-rvmti-0.2.3 (c (n "rvmti") (v "0.2.3") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)))) (h "0z88jxihx11fhdrcya2cn1qdzj8avxwnzwfv9l38cqimxzm378xj") (y #t)))

(define-public crate-rvmti-0.2.4 (c (n "rvmti") (v "0.2.4") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)))) (h "0xlds4fa17a1ndslm74dc5bg9ks9k8hy7677yffp8bdvijizv959") (y #t)))

(define-public crate-rvmti-0.2.5 (c (n "rvmti") (v "0.2.5") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)))) (h "18kfwav4gpi80wlksgy609xdl75bs6mpr6dqyway95kajcjfng86") (y #t)))

(define-public crate-rvmti-0.2.6 (c (n "rvmti") (v "0.2.6") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)))) (h "0h42744ms23h3z9ky7l8p45mkqz4kicnrqxp9vqv0f57rlhv0vml") (y #t)))

(define-public crate-rvmti-0.2.7 (c (n "rvmti") (v "0.2.7") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)))) (h "16xr352jfp79lzj69f79lvnv79hpf0bg2x3bnsnvacicqc887swl") (y #t)))

(define-public crate-rvmti-0.2.8 (c (n "rvmti") (v "0.2.8") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)))) (h "0lg9p6rf5kw51b5zbdg3ip3mjjczwy8q5d4lx1961a9b93j8i7g0") (y #t)))

(define-public crate-rvmti-0.2.9 (c (n "rvmti") (v "0.2.9") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)))) (h "04jf4x0yp98qkz0rizq1v8i8ginwnz06q1lcys4hmxs3mqzxy231") (y #t)))

(define-public crate-rvmti-0.3.0 (c (n "rvmti") (v "0.3.0") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)))) (h "1pws3a9ipcv0vj7vwb5sw1d9xyicnwbmwp55p9qah9ac1bha931g")))

