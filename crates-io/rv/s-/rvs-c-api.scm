(define-module (crates-io rv s- rvs-c-api) #:use-module (crates-io))

(define-public crate-rvs-c-api-0.2.0 (c (n "rvs-c-api") (v "0.2.0") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rvs") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0yd0m5bpd3hnk75bdsjjp8sxfz93swr19bblhv3x6sp9glf5r2sq")))

(define-public crate-rvs-c-api-0.3.0 (c (n "rvs-c-api") (v "0.3.0") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rvs") (r "^0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0cd8zm821m6zk72agrgmw30g12nwhlz9wvhfq2izgzbkaz4qsyhb")))

(define-public crate-rvs-c-api-0.4.0 (c (n "rvs-c-api") (v "0.4.0") (d (list (d (n "difference") (r "^2.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rvs") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0zvv1x16ia2bqlsxk10ccg4dabmykn1aqx9rywshwpzzkyzlpzjd")))

(define-public crate-rvs-c-api-0.4.1 (c (n "rvs-c-api") (v "0.4.1") (d (list (d (n "difference") (r "^2.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rvs") (r "^0.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0wh8xpk44cd5h7bys5s9bxrdg11iyk6jdxp390v6i2g9v92vzm9q")))

(define-public crate-rvs-c-api-0.5.0 (c (n "rvs-c-api") (v "0.5.0") (d (list (d (n "difference") (r ">=2.0.0, <3.0.0") (d #t) (k 2)) (d (n "libc") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "rvs") (r ">=0.5.0, <0.6.0") (d #t) (k 0)) (d (n "tempfile") (r ">=3.0.0, <4.0.0") (d #t) (k 2)))) (h "0j5al7zq3kgr9xbd0k3wnwmwmcxjgslgiygwcc0cn7iapqsmb7s1")))

