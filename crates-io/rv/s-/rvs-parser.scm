(define-module (crates-io rv s- rvs-parser) #:use-module (crates-io))

(define-public crate-rvs-parser-0.1.0 (c (n "rvs-parser") (v "0.1.0") (d (list (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "1pysihl46h5xfkqhlj75sqf604r948mv2kb3lsadd53dfs5zd6dp")))

(define-public crate-rvs-parser-0.2.0 (c (n "rvs-parser") (v "0.2.0") (d (list (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "0dvzr8ydg8xw9fcgf9l684aapzwn36d1y3q5x7zhb5az237n074g")))

(define-public crate-rvs-parser-0.3.0 (c (n "rvs-parser") (v "0.3.0") (d (list (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "1p9q1xxn2csacdq0cr5xh8rmlcpb6yxdfbgm5ixghl90rlrmnqh0")))

(define-public crate-rvs-parser-0.4.0 (c (n "rvs-parser") (v "0.4.0") (d (list (d (n "peg") (r "^0.5") (d #t) (k 1)))) (h "10xp4fgyqpdd92w03iym9h63ddj2l8lvg3xfnjjnr8zf2vh4byif")))

(define-public crate-rvs-parser-0.5.0 (c (n "rvs-parser") (v "0.5.0") (d (list (d (n "peg") (r ">=0.6.0, <0.7.0") (d #t) (k 0)))) (h "10zdr1m0g84g3mcz3b6ygsyd0nbrk6iig36shq1z2n1cr2y34nx2")))

