(define-module (crates-io rv s- rvs-repl) #:use-module (crates-io))

(define-public crate-rvs-repl-0.2.0 (c (n "rvs-repl") (v "0.2.0") (d (list (d (n "rvs") (r "^0.2") (d #t) (k 0)))) (h "18a98fkpp291fxg2qi881jwdlrkkv59gf7yaym7v0l8mc3hw94mk")))

(define-public crate-rvs-repl-0.3.0 (c (n "rvs-repl") (v "0.3.0") (d (list (d (n "rvs") (r "^0.3") (d #t) (k 0)))) (h "0jywvhfz98i6wrdi6jy9g4gb2plwl6k4p86vdf1ybsrqvyp3nlic")))

(define-public crate-rvs-repl-0.4.0 (c (n "rvs-repl") (v "0.4.0") (d (list (d (n "rvs") (r "^0.4") (d #t) (k 0)))) (h "10hzbnwca15rhd3ij5vh948asp7xgiaawq672c22sl3r2xxgr8d2")))

(define-public crate-rvs-repl-0.4.1 (c (n "rvs-repl") (v "0.4.1") (d (list (d (n "rvs") (r "^0.4") (d #t) (k 0)))) (h "06njy99k2cv6rxczy6f2mlyaisl7f8clj6xxzqgmpbmny3mfvc5m")))

(define-public crate-rvs-repl-0.5.0 (c (n "rvs-repl") (v "0.5.0") (d (list (d (n "rvs") (r ">=0.5.0, <0.6.0") (d #t) (k 0)))) (h "1zyw55d9lw60q549r9pwl0v41yygwz9b433shdwizvzc9ffpk40q")))

