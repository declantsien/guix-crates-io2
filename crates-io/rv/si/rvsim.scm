(define-module (crates-io rv si rvsim) #:use-module (crates-io))

(define-public crate-rvsim-0.1.0 (c (n "rvsim") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1vxrhbz6s70gq619sw4wl8zm4w4cd8p5xqyj4w6g596ciwmv8h7s") (f (quote (("serialize" "serde" "serde_derive"))))))

(define-public crate-rvsim-0.2.0 (c (n "rvsim") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "13b4z715xb3layhhc83j9nhydn9nv8cdb1f6jpbf33l3jg33ch7q") (f (quote (("serialize" "serde" "serde_derive") ("rv32c"))))))

(define-public crate-rvsim-0.2.1 (c (n "rvsim") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "004rq25m28771dy94917b1yp6gqdcsl1l4rj0pxwyhvsargf7lsk") (f (quote (("serialize" "serde" "serde_derive") ("rv32fd") ("rv32c") ("default" "rv32fd"))))))

(define-public crate-rvsim-0.2.2 (c (n "rvsim") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "117wnilvzs783asy764ak8271dgq6rc7impjixqhyna50wh8a3lm") (f (quote (("serialize" "serde" "serde_derive") ("rv32fd") ("rv32c") ("default" "rv32fd"))))))

