(define-module (crates-io rv k_ rvk_methods) #:use-module (crates-io))

(define-public crate-rvk_methods-0.1.0 (c (n "rvk_methods") (v "0.1.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "rvk") (r "^0.23.0") (d #t) (k 0)) (d (n "rvk_objects") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1fw7ii3kzfqrdng37jza008104zyhyknnlqd9gkfybwlwdxjhdn9")))

