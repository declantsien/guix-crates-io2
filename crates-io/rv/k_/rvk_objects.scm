(define-module (crates-io rv k_ rvk_objects) #:use-module (crates-io))

(define-public crate-rvk_objects-0.1.0 (c (n "rvk_objects") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0a1w54gbw2w1p6dlvxmgw1wpv2i6q3w6vrzqd8dhzpykk8zz4iyw")))

(define-public crate-rvk_objects-0.2.0 (c (n "rvk_objects") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1whd1k17ip9ibnic57129whi1gclsnw4bdn2kw38xv7530rqzz9x")))

