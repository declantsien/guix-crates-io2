(define-module (crates-io rv m- rvm-lib) #:use-module (crates-io))

(define-public crate-rvm-lib-0.0.0 (c (n "rvm-lib") (v "0.0.0") (h "1iqcc7zg06n4vagzpk23jmknfwn465v3ikk8jkjyrqzc9rh9iabm")))

(define-public crate-rvm-lib-0.0.1 (c (n "rvm-lib") (v "0.0.1") (h "0sja2g649ckmcf1fky5z022w8fm8ipz49vxg8hva4qfhysanc8s9")))

