(define-module (crates-io rv m_ rvm_opcode) #:use-module (crates-io))

(define-public crate-rvm_opcode-0.1.0 (c (n "rvm_opcode") (v "0.1.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "10ks05pfygzzwvkmd3aibx2f5gmrfwpy3ad3l1x6f0spi9a1n07j")))

