(define-module (crates-io rv ue rvue) #:use-module (crates-io))

(define-public crate-rvue-0.1.0 (c (n "rvue") (v "0.1.0") (d (list (d (n "chrono") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "xml-rs") (r "^0.4.0") (d #t) (k 0)))) (h "0mis1mhn6q6x3vzkl58rasvilvxxynlqsmkqzh9s1cmwrbd1fk04") (f (quote (("serde-serialize" "serde" "serde_derive"))))))

