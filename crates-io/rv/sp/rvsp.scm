(define-module (crates-io rv sp rvsp) #:use-module (crates-io))

(define-public crate-rvsp-0.1.0 (c (n "rvsp") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "const_format") (r "^0.2.30") (d #t) (k 0)) (d (n "realfft") (r "^3.1.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "1iqcc7dldasi55h3ydarfb2pfw4za9v56srz1sc763i670yw37pi")))

(define-public crate-rvsp-0.1.1 (c (n "rvsp") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "const_format") (r "^0.2.30") (d #t) (k 0)) (d (n "realfft") (r "^3.1.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "0ka1vw2v5hg297a84813gj1q1rdznnv2ny3i0492zmrhibf81fdv")))

(define-public crate-rvsp-0.1.2 (c (n "rvsp") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "const_format") (r "^0.2.30") (d #t) (k 0)) (d (n "realfft") (r "^3.1.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "0pfp63al04ik9rrlqphwncdb98ljg7by8m0bh3pmbarpkyprxx52")))

