(define-module (crates-io rv tf rvtf) #:use-module (crates-io))

(define-public crate-rvtf-1.0.0 (c (n "rvtf") (v "1.0.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "vtf") (r "^0.1.5") (d #t) (k 0)) (d (n "vtflib") (r "^0.2.1") (f (quote ("static"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "18znjd5p0x011h07jr3jz684wfisf2ixfqr09mljmgvgaz4fhgq7")))

