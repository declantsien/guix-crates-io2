(define-module (crates-io rv v- rvv-encode) #:use-module (crates-io))

(define-public crate-rvv-encode-0.1.0 (c (n "rvv-encode") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1ifvb1yisw779waaz93j51rcdnh2cvm8559f4ga4syx5i3l06y82")))

(define-public crate-rvv-encode-0.1.1 (c (n "rvv-encode") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0i2azl5j5m3grgc319z4xhldh71rzizhbbx7wy8bwvmjg0ai6kar")))

(define-public crate-rvv-encode-0.1.2 (c (n "rvv-encode") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "15hqwxm7p0jkpscf23137ky2fzb29sxqa1fi3ksl2z88a98d3jg7")))

(define-public crate-rvv-encode-0.1.3 (c (n "rvv-encode") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1dx93zfla7wgkw4p24js7a7hqvf1s1kn99hmc7rc43m73abx9n14")))

(define-public crate-rvv-encode-0.1.4 (c (n "rvv-encode") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1psgqlg261y1dr9ydcqhysfcybnf6izq3zzr2jzxfs6rknyzzhvg") (y #t)))

(define-public crate-rvv-encode-0.1.5 (c (n "rvv-encode") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0ng5frww7azywpr0yj37w2ymynycss6q37s6wbr6csdv6cwja7hj")))

(define-public crate-rvv-encode-0.1.6 (c (n "rvv-encode") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1qfp7bry6zc9rbdgysbbkba3k41ivcsqhgard265k73h26vnjw9k")))

(define-public crate-rvv-encode-0.1.7 (c (n "rvv-encode") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1ix5szllg170irsvpm2yfcylagcvhh7c8xq69k059i20wba8rnj1") (y #t)))

(define-public crate-rvv-encode-0.1.8 (c (n "rvv-encode") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "181p3si88n98manxxfhjfmpazkgx1sxjj9zalkg9k9y8wqp4bv3g")))

(define-public crate-rvv-encode-0.1.9 (c (n "rvv-encode") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1wrjfy3yy0xy5dgddcs8sxrgz63m0q7cn980cfs4r6svaq9wz7iy")))

(define-public crate-rvv-encode-0.1.10 (c (n "rvv-encode") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1n18ps6spd73sccpl6iwq7dhqfshkvw1nlrw5iayzpw51v978xq5")))

(define-public crate-rvv-encode-0.1.11 (c (n "rvv-encode") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1caz8ky8w1q5mv8zqah7nrlh933wbi7w82nqsdbqnhcz1d5h3py9")))

(define-public crate-rvv-encode-0.1.12 (c (n "rvv-encode") (v "0.1.12") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1mw4c4m95prxjqhqwnm07lm9kq1fbc0xdazdakx8zvrk2ggskb7m")))

(define-public crate-rvv-encode-0.1.13 (c (n "rvv-encode") (v "0.1.13") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0vfhhvcmgdc8g2jmdlqx2n9157agfymgyn08jg0c3d4ndskhdpcl")))

(define-public crate-rvv-encode-0.2.0 (c (n "rvv-encode") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0xdwmwgp3wyj53bixk5yx85g43qfxy1dj1ijvrha0p43vdk6y32c")))

(define-public crate-rvv-encode-0.2.1 (c (n "rvv-encode") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1ffw5mzfih6ahzvcha0xhfwyvabm2r1c0zpbf0c5975w4mn3hdfc")))

