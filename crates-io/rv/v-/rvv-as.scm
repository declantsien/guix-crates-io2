(define-module (crates-io rv v- rvv-as) #:use-module (crates-io))

(define-public crate-rvv-as-0.1.0 (c (n "rvv-as") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rvv-encode") (r "^0.1.0") (d #t) (k 0)))) (h "15gwqyh2mlykijyqa2f309jhrag4ljw1ad63jfy91l1jkrfb7c74")))

(define-public crate-rvv-as-0.1.1 (c (n "rvv-as") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rvv-encode") (r "^0.1.1") (d #t) (k 0)))) (h "1hr1ch7dymx63bd516b691kps5acyav9hkqmvjbjdwbw74zxgwp5")))

(define-public crate-rvv-as-0.1.2 (c (n "rvv-as") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rvv-encode") (r "^0.1.2") (d #t) (k 0)))) (h "05mdaf9ml11qz81si7pvz1vqpkl19w5wgq8z3n7z9q1mw47m56fy")))

(define-public crate-rvv-as-0.1.3 (c (n "rvv-as") (v "0.1.3") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rvv-encode") (r "^0.1.3") (d #t) (k 0)))) (h "079r6810vnzqjj00n6576hhq2snrrxj72m56hjjk327pdhbi7hl9")))

(define-public crate-rvv-as-0.1.5 (c (n "rvv-as") (v "0.1.5") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rvv-encode") (r "^0.1.5") (d #t) (k 0)))) (h "070741bnvjgr241icwq1m24r6qbfyn4kiml3796x77d646w4jhri")))

(define-public crate-rvv-as-0.1.6 (c (n "rvv-as") (v "0.1.6") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rvv-encode") (r "^0.1.6") (d #t) (k 0)))) (h "0y86s4p1fwkhm8ylyq76ck2r5aliv1w9da0758nmig52hr0zg5yk")))

(define-public crate-rvv-as-0.1.7 (c (n "rvv-as") (v "0.1.7") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rvv-encode") (r "^0.1.7") (d #t) (k 0)))) (h "0p30ihbzannzfhj8hknkak89w6p9h0v0jy3p9w532yws94p7a9ac") (y #t)))

(define-public crate-rvv-as-0.1.8 (c (n "rvv-as") (v "0.1.8") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rvv-encode") (r "^0.1.8") (d #t) (k 0)))) (h "1l0f9wb7fxw43h2xxkv12mifks7ay2paahkc850r625gv5dbbrw3")))

(define-public crate-rvv-as-0.1.9 (c (n "rvv-as") (v "0.1.9") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rvv-encode") (r "^0.1.9") (d #t) (k 0)))) (h "11s437v7grj31q1jn6z9q5hr08y8ddy6cl6wbamf8dj2rsw7kjja")))

(define-public crate-rvv-as-0.1.10 (c (n "rvv-as") (v "0.1.10") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rvv-encode") (r "^0.1.10") (d #t) (k 0)))) (h "1yk5d7h24idhhsgrjagdcdlrd37d4dzdz2np03xd0wwy52z2w65n")))

(define-public crate-rvv-as-0.1.11 (c (n "rvv-as") (v "0.1.11") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rvv-encode") (r "^0.1.11") (d #t) (k 0)))) (h "00ls3b3s3vqrdjs0dh3jy2d9ksbsznxnv0hrvw20n0i6gs66wi9k")))

(define-public crate-rvv-as-0.1.12 (c (n "rvv-as") (v "0.1.12") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rvv-encode") (r "^0.1.11") (d #t) (k 0)))) (h "0cc1hxsq2dz1n0gqvijjsyfxqzy3bajgh4irr5yxrgxcy62lddmq")))

(define-public crate-rvv-as-0.1.13 (c (n "rvv-as") (v "0.1.13") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rvv-encode") (r "^0.1.13") (d #t) (k 0)))) (h "1b3wiv9pgcyrlnzs7a47mzwqh5vh2kj8b4b611bcgwbrcvcha9sb")))

(define-public crate-rvv-as-0.2.0 (c (n "rvv-as") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rvv-encode") (r "^0.2.0") (d #t) (k 0)))) (h "0w2jnh8g8b1rrm4d9r55mldz4h45lc3ibwlzlljh6a5paisa5mnb")))

(define-public crate-rvv-as-0.2.1 (c (n "rvv-as") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rvv-encode") (r "^0.2.1") (d #t) (k 0)))) (h "12f6hpx42a5v85zxqp1b868a46021r12q9cgnp7rrvdk27hxlc86")))

