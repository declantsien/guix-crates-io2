(define-module (crates-io rv im rvimage-domain) #:use-module (crates-io))

(define-public crate-rvimage-domain-0.4.1 (c (n "rvimage-domain") (v "0.4.1") (d (list (d (n "image") (r "^0.25.0") (d #t) (k 0)) (d (n "imageproc") (r "~0.24") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10fcgfwlfck96rzzdhp180l1sr0crr0j204hfq768dl0v3a8n6ch")))

