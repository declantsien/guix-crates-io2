(define-module (crates-io rv im rvim) #:use-module (crates-io))

(define-public crate-rvim-0.0.2 (c (n "rvim") (v "0.0.2") (d (list (d (n "man") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "0fqca6x4vy2hsj9bgajnla6ajznk8a8ncr441iqjl5j8vkyxrwss") (f (quote (("build_deps" "man"))))))

(define-public crate-rvim-0.0.4 (c (n "rvim") (v "0.0.4") (d (list (d (n "man") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "0q3fc7qcb7j5an0gvmgfky7qpc7sa6vr3sj3r7zc5zx2x0fmsdwq") (f (quote (("build_deps" "man"))))))

(define-public crate-rvim-0.0.5 (c (n "rvim") (v "0.0.5") (d (list (d (n "man") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "02n2bpl3fvqr5s572ln5x9kbgafd0kdk40rn23xfqavgfzz6vz16") (f (quote (("build_deps" "man"))))))

(define-public crate-rvim-0.0.3 (c (n "rvim") (v "0.0.3") (d (list (d (n "man") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "1zjki198n5ps1r90niji9wfwg3gc21008p7ddfgf2z9gyzzglfmd") (f (quote (("build_deps" "man"))))))

(define-public crate-rvim-0.0.6 (c (n "rvim") (v "0.0.6") (d (list (d (n "man") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "1ck58mz0ykalfl7j589irfb712cclrgfihahcpmpq5srh73qg65s") (f (quote (("build_deps" "man"))))))

(define-public crate-rvim-0.0.7 (c (n "rvim") (v "0.0.7") (d (list (d (n "man") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "1yxbdwvqdq2vmcgj94rz7h8q5ls5n54h0pp90l3h9r246mri6pa7") (f (quote (("build_deps" "man"))))))

(define-public crate-rvim-0.0.8 (c (n "rvim") (v "0.0.8") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "man") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "17l9fkwzha0gx90ajvivnnm9s8z440hjvlrpwxj6gnr9imvgiakl") (f (quote (("build_deps" "man"))))))

