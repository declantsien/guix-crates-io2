(define-module (crates-io rv pk rvpk) #:use-module (crates-io))

(define-public crate-rvpk-1.1.0 (c (n "rvpk") (v "1.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "cntr-fuse") (r "^0.4") (o #t) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "daemonize") (r "^0.4.1") (o #t) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libc") (r "^0.2.83") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "17f2bwhr58k4pc86s1dbywrzvrkdkl1s014w7f58mqjkizd465gv") (f (quote (("fuse" "cntr-fuse" "daemonize") ("default" "fuse"))))))

