(define-module (crates-io rv #{32}# rv32m1_ri5cy-hal) #:use-module (crates-io))

(define-public crate-rv32m1_ri5cy-hal-0.0.1 (c (n "rv32m1_ri5cy-hal") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "riscv") (r "^0.5.4") (d #t) (k 0)) (d (n "rv32m1_ri5cy-pac") (r "^0.1.1") (d #t) (k 0)))) (h "1ij1rjrpmsvwwiwvr5p14wblfjqv6krfzpwxica8dxsk06sqkfps")))

