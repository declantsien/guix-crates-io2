(define-module (crates-io rv #{32}# rv32m1_ri5cy-pac) #:use-module (crates-io))

(define-public crate-rv32m1_ri5cy-pac-0.1.0 (c (n "rv32m1_ri5cy-pac") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "riscv") (r "^0.5.4") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0vkarb5nj09vwbvm634vn5ayf84kakhfiyzhnbasn2f524baiz2s") (f (quote (("rt" "riscv-rt"))))))

(define-public crate-rv32m1_ri5cy-pac-0.1.1 (c (n "rv32m1_ri5cy-pac") (v "0.1.1") (d (list (d (n "bare-metal") (r "^0.2.5") (d #t) (k 0)) (d (n "riscv") (r "^0.5.4") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0mxjxrgjvv5syhhhgnp6bidgmqb69bypk01ipghbzzqrm9lakb7z") (f (quote (("rt" "riscv-rt"))))))

