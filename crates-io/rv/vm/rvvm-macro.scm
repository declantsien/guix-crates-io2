(define-module (crates-io rv vm rvvm-macro) #:use-module (crates-io))

(define-public crate-rvvm-macro-0.1.0 (c (n "rvvm-macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)))) (h "093x9g60n2j640glgmljb6bxlyd8jk70xhyaazwlkj8a2anzvxw6")))

