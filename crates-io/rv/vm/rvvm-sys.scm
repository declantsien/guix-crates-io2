(define-module (crates-io rv vm rvvm-sys) #:use-module (crates-io))

(define-public crate-rvvm-sys-0.1.0 (c (n "rvvm-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "0pilws11amy2jq9gzvzz7jmgxbwpkksmwrdz0ak16pfj237rslr5")))

(define-public crate-rvvm-sys-0.2.0 (c (n "rvvm-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)))) (h "02y6261r55k9lfdb6rg2q4iyzaccyfsd1aq1j2djc1d5ixcdlgvs") (f (quote (("embeddable") ("default" "embeddable"))))))

(define-public crate-rvvm-sys-0.2.1 (c (n "rvvm-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)))) (h "1ax7g4yy2f1kwyp5481mid5gmdkw086crf1c7rg0imx1dyga1z60") (f (quote (("embeddable") ("default" "embeddable"))))))

(define-public crate-rvvm-sys-0.2.3 (c (n "rvvm-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "1ybn4c9kqhb7p15hy0msqb92vkcq3hsnjzq22azj32g7hyjngdwc") (f (quote (("embeddable") ("default" "embeddable"))))))

(define-public crate-rvvm-sys-0.2.4 (c (n "rvvm-sys") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "1wv1aiblwk2yrh6scd5hxf1qrkwjfzy8pjpi7khvg9w8daxwbqg5") (f (quote (("embeddable") ("default" "embeddable"))))))

(define-public crate-rvvm-sys-1.0.1 (c (n "rvvm-sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)))) (h "0j8hdq4l0102m51jgl1qjq85iilvi1bhknp3jpn7x59xpzqjdmkn")))

(define-public crate-rvvm-sys-1.1.0 (c (n "rvvm-sys") (v "1.1.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)))) (h "0z5a0vqbsr0z9xba2m9kakm42br2jlwqgyva9jikfs9z55hwzr4f") (f (quote (("dynamic") ("default"))))))

(define-public crate-rvvm-sys-1.1.1 (c (n "rvvm-sys") (v "1.1.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)))) (h "0chx2if51kqhfx38gy1dbzk4r9zcbbkxb81n5hhdwsry95h2xrns") (f (quote (("dynamic") ("default"))))))

(define-public crate-rvvm-sys-1.1.2 (c (n "rvvm-sys") (v "1.1.2") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)))) (h "0r69mrbhms7i2lqf2nhqa31arzbff97ii1n4xb24fnficl9wphmh") (f (quote (("dynamic") ("default"))))))

