(define-module (crates-io mc ai mcai-docker) #:use-module (crates-io))

(define-public crate-mcai-docker-0.1.0 (c (n "mcai-docker") (v "0.1.0") (d (list (d (n "async-std") (r "^1.11.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "bollard") (r "^0.12.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)))) (h "0jskfz9acb60z48c0nz1hxajyxq0h4rmp2b59f2ijad7acs8gywx")))

(define-public crate-mcai-docker-0.1.1 (c (n "mcai-docker") (v "0.1.1") (d (list (d (n "async-std") (r "^1.11.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "bollard") (r "^0.12.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)))) (h "0rwh4nkavq730djagbgxny7ciqilwbpw0d1chcsxmmz9slglgrid")))

(define-public crate-mcai-docker-0.2.0 (c (n "mcai-docker") (v "0.2.0") (d (list (d (n "async-std") (r "^1.11.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "bollard") (r "^0.12.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)))) (h "1y0ikcyjdbvzs9510f0w6b00zij9a64rcprlm57ndwqri97lf4b8")))

(define-public crate-mcai-docker-0.2.1 (c (n "mcai-docker") (v "0.2.1") (d (list (d (n "async-std") (r "^1.11.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "bollard") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)))) (h "0imvlldvbdc8q7lq71db74sfhl3aqkk1rd08d77wc0yy37ba7zk8")))

(define-public crate-mcai-docker-0.2.2 (c (n "mcai-docker") (v "0.2.2") (d (list (d (n "async-io") (r "^1.8.0") (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "bollard") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)))) (h "1lvcr6a3yydb8rwrj574fx6r719bp490zhi7wx20412d9s219awq")))

