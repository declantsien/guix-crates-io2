(define-module (crates-io mc ai mcai_build) #:use-module (crates-io))

(define-public crate-mcai_build-0.1.1 (c (n "mcai_build") (v "0.1.1") (d (list (d (n "cargo_toml") (r "^0.11.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0vhkg3qybrs602j3wz8pig7pp34zbiv4s3hbnxwxvvx7c4nfd0jf")))

(define-public crate-mcai_build-0.1.2 (c (n "mcai_build") (v "0.1.2") (d (list (d (n "cargo_toml") (r "^0.11.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1l4ccpzzdfs24c1kbzv77iyqmwylc2xfm4ivnxmvvgj81d7101gn")))

(define-public crate-mcai_build-0.2.0 (c (n "mcai_build") (v "0.2.0") (d (list (d (n "cargo_toml") (r "^0.11.5") (d #t) (k 0)) (d (n "mcai-license") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0ni4abr8s5ywmd0dmr9x28nra8mf7x9dgvyi97c2wwmqd4f0gkhk")))

