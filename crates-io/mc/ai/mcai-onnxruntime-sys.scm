(define-module (crates-io mc ai mcai-onnxruntime-sys) #:use-module (crates-io))

(define-public crate-mcai-onnxruntime-sys-0.0.14 (c (n "mcai-onnxruntime-sys") (v "0.0.14") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.1") (d #t) (k 1)) (d (n "zip") (r "^0.5") (d #t) (k 1)))) (h "03s4zqk50sfl0hzg8ch3hyg46mxnm0xrd8cwvfd1jp798zvdqmx8") (f (quote (("generate-bindings" "bindgen") ("disable-sys-build-script") ("default"))))))

(define-public crate-mcai-onnxruntime-sys-0.0.15 (c (n "mcai-onnxruntime-sys") (v "0.0.15") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.1") (d #t) (k 1)) (d (n "zip") (r "^0.5") (d #t) (k 1)))) (h "1dk0ld4vzpy4h7r8sgas6jm1n9vl3jviwkjhvrvqjhmzlq0mva7r") (f (quote (("generate-bindings" "bindgen") ("disable-sys-build-script") ("default"))))))

