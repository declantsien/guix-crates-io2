(define-module (crates-io mc ai mcai-backend) #:use-module (crates-io))

(define-public crate-mcai-backend-0.0.1 (c (n "mcai-backend") (v "0.0.1") (d (list (d (n "built") (r "^0.4") (d #t) (k 1)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "term-table") (r "^1.3.1") (d #t) (k 0)))) (h "05w5giics6n6rv0pjlhyb46ckdf8q76w7yhxb3p62ji1g4pl4jdc")))

