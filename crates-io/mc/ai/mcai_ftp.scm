(define-module (crates-io mc ai mcai_ftp) #:use-module (crates-io))

(define-public crate-mcai_ftp-3.0.1 (c (n "mcai_ftp") (v "3.0.1") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.25") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1mi0dlh2hylrnn7p1vg0ihn32jrxxdh4m460xpjr3086lvjp0dbq") (f (quote (("secure" "openssl") ("debug_print"))))))

