(define-module (crates-io mc ai mcai-graph) #:use-module (crates-io))

(define-public crate-mcai-graph-0.1.0 (c (n "mcai-graph") (v "0.1.0") (d (list (d (n "mcai-types") (r "^0.1.0") (d #t) (k 0)))) (h "1k06s06kv95cdcah6hcr5l9sxhd8ivq7v5v1dhxly5qjh09ah3nq")))

(define-public crate-mcai-graph-0.2.0 (c (n "mcai-graph") (v "0.2.0") (d (list (d (n "mcai-types") (r "^0.1.0") (d #t) (k 0)))) (h "051mhkjj13p7rd54rh28dv0yn7sfa6mh3b606jp1az17rbwlivcj")))

(define-public crate-mcai-graph-0.3.0 (c (n "mcai-graph") (v "0.3.0") (d (list (d (n "mcai-types") (r "^0.1.0") (d #t) (k 0)))) (h "0hbab9jqja137jp79gcvqw7zdd7a7yq8217xz09xnwplvig2lq4y")))

