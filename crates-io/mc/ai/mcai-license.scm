(define-module (crates-io mc ai mcai-license) #:use-module (crates-io))

(define-public crate-mcai-license-0.1.0 (c (n "mcai-license") (v "0.1.0") (d (list (d (n "license") (r "^2.0") (f (quote ("offline"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0ca2ygd6x9ggrm85fb7d3s49i6li3lfn1qsjg5rzq2gpsyyw2cwa")))

