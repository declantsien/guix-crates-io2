(define-module (crates-io mc ai mcai-types) #:use-module (crates-io))

(define-public crate-mcai-types-0.1.0 (c (n "mcai-types") (v "0.1.0") (d (list (d (n "schemars") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b7ad4n5x6mz27k16zd82gz4pykhnlg21dp344vsph6c99i594vg")))

