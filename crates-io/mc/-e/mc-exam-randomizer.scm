(define-module (crates-io mc -e mc-exam-randomizer) #:use-module (crates-io))

(define-public crate-mc-exam-randomizer-0.1.0 (c (n "mc-exam-randomizer") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "fake") (r "^2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0pbmi3milq6rdpdgac6aqhyr7akb45rdf1jy5w1wga6xhsn051yl")))

(define-public crate-mc-exam-randomizer-0.2.0 (c (n "mc-exam-randomizer") (v "0.2.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vip7y4wm3vflcnnhx49lyn8mnmdijh3zrh3z0925gadwh99w45i")))

(define-public crate-mc-exam-randomizer-0.2.1 (c (n "mc-exam-randomizer") (v "0.2.1") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0nllgx8ndbb5rx65gsw5s5zjn75803w7qr691v52s00mvn7d99ln")))

(define-public crate-mc-exam-randomizer-0.3.0 (c (n "mc-exam-randomizer") (v "0.3.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0dk7gzg1ldk5a1kj5zwh1kslkcihb53cs5gn1cr06g1zyjvncw7f")))

(define-public crate-mc-exam-randomizer-0.3.1 (c (n "mc-exam-randomizer") (v "0.3.1") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1s5l16z20z1p8mjnplzfi8yhygkinw2jrzzzvw1pvdncpb1biz9s")))

(define-public crate-mc-exam-randomizer-0.3.2 (c (n "mc-exam-randomizer") (v "0.3.2") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hbwn06v91r4chmb9lcd5ks98cng0ymhqmxmxa4w6agd2d3zlz56")))

(define-public crate-mc-exam-randomizer-0.3.3 (c (n "mc-exam-randomizer") (v "0.3.3") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0550zyyxm6ah9dvcd8xf0g70dqyyfwdiax9j5p2b1bac40s5nwwd")))

(define-public crate-mc-exam-randomizer-0.3.4 (c (n "mc-exam-randomizer") (v "0.3.4") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01vp4r5lil1afqb3bvks42cyk2g052v99n3qib98n40p618b90fp")))

(define-public crate-mc-exam-randomizer-0.3.5 (c (n "mc-exam-randomizer") (v "0.3.5") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zrahpanmyg6mny0blqp56fwvy97qkf6lalikkdlyss31v4fwp32")))

(define-public crate-mc-exam-randomizer-0.3.6 (c (n "mc-exam-randomizer") (v "0.3.6") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bxmz66nlkmybqnw2kaz1sl153362mvag12n8p1y43ap7rngkzgd")))

(define-public crate-mc-exam-randomizer-0.3.7 (c (n "mc-exam-randomizer") (v "0.3.7") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "jetscii") (r "^0.5.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ah64qap6x7jdfqng26x64l9cg9g21p5d5rq4f2qa5pdmqx2m2y3")))

