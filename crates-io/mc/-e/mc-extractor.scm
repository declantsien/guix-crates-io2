(define-module (crates-io mc -e mc-extractor) #:use-module (crates-io))

(define-public crate-mc-extractor-0.1.0 (c (n "mc-extractor") (v "0.1.0") (h "15fdlzf7771vfkh39im9wvyi1v4m4j7p2f445nwva023n150gj7r")))

(define-public crate-mc-extractor-0.2.0 (c (n "mc-extractor") (v "0.2.0") (d (list (d (n "indicatif") (r "^0.16.2") (f (quote ("improved_unicode"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "1q9723703rb3qmzaqdikqlkgdxs6pygjjihfq2q0dix4yr9qcgha")))

