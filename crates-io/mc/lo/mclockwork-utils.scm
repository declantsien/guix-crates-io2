(define-module (crates-io mc lo mclockwork-utils) #:use-module (crates-io))

(define-public crate-mclockwork-utils-2.0.19 (c (n "mclockwork-utils") (v "2.0.19") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "base64") (r "~0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "static-pubkey") (r "^1.0.3") (d #t) (k 0)))) (h "010iyj69cd6r9zakjjy86pqbkkh07i2q7xwrv2n7zvl0bcc9wkgx")))

