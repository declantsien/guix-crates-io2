(define-module (crates-io mc a- mca-parser) #:use-module (crates-io))

(define-public crate-mca-parser-0.0.1 (c (n "mca-parser") (v "0.0.1") (h "1dhlv79dfyh054dfq0vprwqyvb79ibj7zgw9vsz2faf7z68a4svs")))

(define-public crate-mca-parser-0.0.2 (c (n "mca-parser") (v "0.0.2") (h "0vhvcg05gbvylp1vjf9xh8kb37wxryfpjzmi1vyd8h8mc77qp6l7")))

(define-public crate-mca-parser-0.0.3 (c (n "mca-parser") (v "0.0.3") (d (list (d (n "fastnbt") (r "^2.3.2") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)))) (h "1sgf9p0z0ys8nzgg8drm42i5s3ynniipvva1968dzdxn2mmlbnkr")))

(define-public crate-mca-parser-0.1.0 (c (n "mca-parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "fastnbt") (r "^2.3.2") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)))) (h "0m6c4bjbfs6wblqxky9ig2700y1pc906kqj29hm68fypnb4grlc6")))

(define-public crate-mca-parser-0.2.0 (c (n "mca-parser") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "fastnbt") (r "^2.3.2") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)))) (h "0i1f088yks6n2y7kgi1damb6jfd3a1k0acv6b9xvfky5rmmy45x4")))

(define-public crate-mca-parser-1.0.0 (c (n "mca-parser") (v "1.0.0") (d (list (d (n "fastnbt") (r "^2.3.2") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)))) (h "1xpf7ri1rjp4dvc0divr2wzx0lp2rvmqs6rdhqgcb6w0czr1c02q")))

(define-public crate-mca-parser-1.0.1 (c (n "mca-parser") (v "1.0.1") (d (list (d (n "fastnbt") (r "^2.3.2") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)))) (h "1yj8d8jrvz8dwfhdhg5cf370sqsz8v8xz75r77rnb5gjmpwr5sgq")))

(define-public crate-mca-parser-1.0.2 (c (n "mca-parser") (v "1.0.2") (d (list (d (n "fastnbt") (r "^2.3.2") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)))) (h "15w600y4z304qk73l3917ibzjd1nazkv86csp113mgfw5v406v3h")))

