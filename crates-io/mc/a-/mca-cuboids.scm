(define-module (crates-io mc a- mca-cuboids) #:use-module (crates-io))

(define-public crate-mca-cuboids-0.1.0 (c (n "mca-cuboids") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fastanvil") (r "^0.26.0") (d #t) (k 0)) (d (n "fastnbt") (r "^2.2.0") (d #t) (k 0)) (d (n "pprof") (r "^0.10.0") (f (quote ("criterion" "flamegraph"))) (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)))) (h "0rrqdi9iza7nkyls7y4nzfkbydn09nd0n8dih8hyvnkvg7x22j56")))

(define-public crate-mca-cuboids-0.2.0 (c (n "mca-cuboids") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fastanvil") (r "^0.26.0") (d #t) (k 0)) (d (n "fastnbt") (r "^2.2.0") (d #t) (k 0)) (d (n "pprof") (r "^0.10.0") (f (quote ("criterion" "flamegraph"))) (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)))) (h "06dwvn80j5djg6xqmnwl8yggdsbffibw8lrkwhhcg7nnaf91qd16")))

