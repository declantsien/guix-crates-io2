(define-module (crates-io mc pa mcpat) #:use-module (crates-io))

(define-public crate-mcpat-0.0.1 (c (n "mcpat") (v "0.0.1") (h "0zc8h76pcnpd0pwqzsvc1rcmcp3kg9s93037fvngpar1brcr6nfw")))

(define-public crate-mcpat-0.1.0 (c (n "mcpat") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "mcpat-sys") (r "*") (d #t) (k 0)))) (h "0518l6k0z7j4m1qq696rl7kadf4hzjxx8hxy84ws1havx8yc2v67")))

(define-public crate-mcpat-0.1.1 (c (n "mcpat") (v "0.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "mcpat-sys") (r "*") (d #t) (k 0)))) (h "0123q4axcvnvq2mvy1mylrg7slr8325qygb0bhvhbbxg2y13adrf")))

(define-public crate-mcpat-0.1.2 (c (n "mcpat") (v "0.1.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "mcpat-sys") (r "*") (d #t) (k 0)))) (h "0wx5bzgw4hnnqiv8xckrdxx4320g618ygws7cff208mxybs3z52n")))

(define-public crate-mcpat-0.1.3 (c (n "mcpat") (v "0.1.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "mcpat-sys") (r "*") (d #t) (k 0)))) (h "0xzm8dh0pn86iym015kmgql62l0hifhdsxhzb0xihagcbnvf3nl1")))

(define-public crate-mcpat-0.1.4 (c (n "mcpat") (v "0.1.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "mcpat-sys") (r "*") (d #t) (k 0)))) (h "04n0wbkq358f2gf594a2vncyvl4j06ji98ldz3wjqyqj29qrfyr0")))

(define-public crate-mcpat-0.1.5 (c (n "mcpat") (v "0.1.5") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "mcpat-sys") (r "*") (d #t) (k 0)))) (h "18rrwp752cmalkx7143021vakliphlcr4kwqrm4nnhlnyzr8lxgc")))

(define-public crate-mcpat-0.1.6 (c (n "mcpat") (v "0.1.6") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "mcpat-sys") (r "*") (d #t) (k 0)))) (h "1k9s8d1h2ghp3mqnbqfg4y60sqb1w04rk7cm5vbqa6bf2qrx7b4g")))

(define-public crate-mcpat-0.2.0 (c (n "mcpat") (v "0.2.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "mcpat-sys") (r "*") (d #t) (k 0)))) (h "0mzcidfax2cp16lzwj5mrg5q0g9za1rmfi0p6j5c376fh40mlm8h")))

(define-public crate-mcpat-0.3.0 (c (n "mcpat") (v "0.3.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "mcpat-sys") (r "*") (d #t) (k 0)))) (h "106p7qahimkm4qpjwzfqv843ilfm0cb939npaq1ami28126yj1x2") (f (quote (("cache" "mcpat-sys/cache"))))))

(define-public crate-mcpat-0.4.0 (c (n "mcpat") (v "0.4.0") (d (list (d (n "hiredis") (r "*") (o #t) (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "mcpat-sys") (r "*") (d #t) (k 0)))) (h "021vbprgmp5ba1rwkrjjz54kk4pgjxbjhhwr8n9d48zmpbq666l2") (f (quote (("caching" "mcpat-sys/caching" "hiredis"))))))

(define-public crate-mcpat-0.5.0 (c (n "mcpat") (v "0.5.0") (d (list (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "hiredis") (r "*") (o #t) (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "mcpat-sys") (r "*") (d #t) (k 0)))) (h "0jq2i31didcf63vdrkpbm12yrjl3s0vcn79f0ndrnv9k4p1mvjsl") (f (quote (("caching" "mcpat-sys/caching" "hiredis"))))))

(define-public crate-mcpat-0.6.0 (c (n "mcpat") (v "0.6.0") (d (list (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "hiredis") (r "*") (o #t) (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "mcpat-sys") (r "*") (d #t) (k 0)))) (h "0l94g5qymj2vq8jhqiq6kp2d8798cj1ylh32c28n5pjpcqx7r1j4") (f (quote (("caching" "mcpat-sys/caching" "hiredis"))))))

(define-public crate-mcpat-0.7.0 (c (n "mcpat") (v "0.7.0") (d (list (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "hiredis") (r "*") (o #t) (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "mcpat-sys") (r "*") (d #t) (k 0)))) (h "1i08i8lmv7rcjyh4vilfrz8xycbi2587aj8a0y82fwia4xvm6fgr") (f (quote (("caching" "mcpat-sys/caching" "hiredis"))))))

(define-public crate-mcpat-0.8.0 (c (n "mcpat") (v "0.8.0") (d (list (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "hiredis") (r "*") (o #t) (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "mcpat-sys") (r "*") (d #t) (k 0)))) (h "1ra80sic0blaxncg0zzvynx1py0p448vjnvkrhl1g8dmjf69g1vm") (f (quote (("caching" "mcpat-sys/caching" "hiredis"))))))

(define-public crate-mcpat-0.8.1 (c (n "mcpat") (v "0.8.1") (d (list (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "hiredis") (r "*") (o #t) (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "mcpat-sys") (r "*") (d #t) (k 0)))) (h "00vx8cnasbqnbbm6vcv89ccfp74xn20jd9n1yjx10hbykib3ppzv") (f (quote (("caching" "mcpat-sys/caching" "hiredis"))))))

(define-public crate-mcpat-0.8.2 (c (n "mcpat") (v "0.8.2") (d (list (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "hiredis") (r "*") (o #t) (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "mcpat-sys") (r "*") (d #t) (k 0)))) (h "0gl6f32ha7x1763sx2lf6qam3j4y34980645hgzab233ypz81qrq") (f (quote (("caching" "mcpat-sys/caching" "hiredis"))))))

(define-public crate-mcpat-0.8.3 (c (n "mcpat") (v "0.8.3") (d (list (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "hiredis") (r "*") (o #t) (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "mcpat-sys") (r "*") (d #t) (k 0)))) (h "08g7rivfrw0i0qp0kj0vvzy3bddd9z8as8lzplw0hs3v2sx8gxd4") (f (quote (("caching" "mcpat-sys/caching" "hiredis"))))))

(define-public crate-mcpat-0.9.0 (c (n "mcpat") (v "0.9.0") (d (list (d (n "fixture") (r "^0.3") (d #t) (k 2)) (d (n "hiredis") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mcpat-sys") (r "^0.8") (d #t) (k 0)))) (h "11cbnq5j2lqj3mpb7k1222idc4aiy9izkzvf6wnw75swiv00a8y2") (f (quote (("caching" "mcpat-sys/caching" "hiredis"))))))

