(define-module (crates-io mc ke mckernant1-tools) #:use-module (crates-io))

(define-public crate-mckernant1-tools-0.0.1 (c (n "mckernant1-tools") (v "0.0.1") (h "1626cb2bl2bk0405xv5cwypskv3pvna3vwc331s8p3d5989fp41v")))

(define-public crate-mckernant1-tools-0.0.2 (c (n "mckernant1-tools") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (o #t) (d #t) (k 0)))) (h "15i9xzk4r2n8cjpdh9fr1f3i4kh6wkybplmhkcx6mvyy0808vzc3") (f (quote (("time" "chrono") ("channels" "crossbeam" "time")))) (r "1.59.0")))

(define-public crate-mckernant1-tools-0.0.3 (c (n "mckernant1-tools") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (o #t) (d #t) (k 0)))) (h "1cmnywia189hb413hn367b67qxxcp4cgsma7qys3mzfj5s4fx5hs") (f (quote (("time" "chrono") ("channels" "crossbeam" "time")))) (r "1.59.0")))

(define-public crate-mckernant1-tools-0.0.4 (c (n "mckernant1-tools") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)))) (h "0c06gi3jmynvb14jyizd93xs0v6iff507p4fpywrhk4ddm7fdc70") (f (quote (("time" "chrono") ("channels" "crossbeam" "time")))) (r "1.59.0")))

(define-public crate-mckernant1-tools-0.0.5 (c (n "mckernant1-tools") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (o #t) (d #t) (k 0)))) (h "1ci2jbrmf3ibq3z18snc1bjn9nymvxskl79w6rj1q5hjw8sh8r4c") (f (quote (("time" "chrono") ("channels" "crossbeam" "time") ("async" "tokio")))) (r "1.59.0")))

(define-public crate-mckernant1-tools-0.0.6 (c (n "mckernant1-tools") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (o #t) (d #t) (k 0)))) (h "1f00jnwbj1ll1g118c0vv6dh1l7vijcaf5qhphhc5a2plrz1j262") (f (quote (("time" "chrono") ("channels" "crossbeam" "chrono") ("async" "tokio")))) (r "1.59.0")))

