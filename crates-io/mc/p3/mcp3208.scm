(define-module (crates-io mc p3 mcp3208) #:use-module (crates-io))

(define-public crate-mcp3208-0.1.0 (c (n "mcp3208") (v "0.1.0") (d (list (d (n "spidev") (r "^0.4.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0xb1azn9krn4b47snir4rnb32w820hkikpwsq9h8ymxnbwzr0p3v")))

