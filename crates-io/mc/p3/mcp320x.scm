(define-module (crates-io mc p3 mcp320x) #:use-module (crates-io))

(define-public crate-mcp320x-0.1.0 (c (n "mcp320x") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "0gm5bq792jji3wpwl08vjmjy5didn6k69nm4722877g253mkbxiy")))

(define-public crate-mcp320x-0.1.1 (c (n "mcp320x") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1ww09bg89xbcvlm4g5l06ih58ynx2mqjcsgnqh9lq7jcdk38bycm")))

