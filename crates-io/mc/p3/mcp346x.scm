(define-module (crates-io mc p3 mcp346x) #:use-module (crates-io))

(define-public crate-mcp346x-0.1.0 (c (n "mcp346x") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "measurements") (r "^0.11") (d #t) (k 0)))) (h "15q1l56bgav9vhq9k7nq4c14srbgsbwzh1x90yxdw023nxihipr3")))

