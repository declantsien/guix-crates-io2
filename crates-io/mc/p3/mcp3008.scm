(define-module (crates-io mc p3 mcp3008) #:use-module (crates-io))

(define-public crate-mcp3008-1.0.0 (c (n "mcp3008") (v "1.0.0") (d (list (d (n "spidev") (r "^0.3.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1ck1a5crcfqxrw50px835xwv5jyp3fwg1plvdz0prvwslp4igxy5")))

