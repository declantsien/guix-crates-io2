(define-module (crates-io mc p3 mcp3425) #:use-module (crates-io))

(define-public crate-mcp3425-0.1.0 (c (n "mcp3425") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.1.1") (d #t) (k 2)))) (h "103d7gxsp4mxc8yzlbcllb4czqydss07ghirsqkvl6a8f4phy8iz")))

(define-public crate-mcp3425-0.1.1 (c (n "mcp3425") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.1.1") (d #t) (k 2)))) (h "03bbfsghldlh3nkd8qcjgs4h90spx3jyni4xn969cxp2knx3cwxa")))

(define-public crate-mcp3425-0.2.0 (c (n "mcp3425") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.1.1") (d #t) (k 2)) (d (n "measurements") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0gfwn7scp57v6r8k4j7hgs8dqs8p1vflchp6fsdi5amvh9nz2qfs") (f (quote (("default"))))))

(define-public crate-mcp3425-0.2.1 (c (n "mcp3425") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.1.1") (d #t) (k 2)) (d (n "measurements") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1r8kdwba8b9hnk261vb5py9d66j48d5cix0hsbasmfnrgm9dc1sq") (f (quote (("default"))))))

(define-public crate-mcp3425-0.3.0 (c (n "mcp3425") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "measurements") (r "^0.10") (o #t) (d #t) (k 0)))) (h "0ryjpzplvdy5xjjk1zgi525993hsx13v6by4csj1x6qlwkpg61zw") (f (quote (("default"))))))

(define-public crate-mcp3425-1.0.0 (c (n "mcp3425") (v "1.0.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "measurements") (r "^0.11") (o #t) (k 0)))) (h "1rbmlibf3v3gfilwsjww58nmpqcnfcyj3qi63vmfdsa4hn3g58hr") (f (quote (("quad_channel") ("dual_channel") ("default"))))))

(define-public crate-mcp3425-1.1.0 (c (n "mcp3425") (v "1.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (f (quote ("eh0"))) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "measurements") (r "^0.11") (o #t) (k 0)) (d (n "rstest") (r "^0.18") (d #t) (k 2)))) (h "04cip1k8w8m7i701i5s22n2bx44i0rvx61z1k4xrnyz8myrdh3l5") (f (quote (("quad_channel") ("dual_channel") ("default"))))))

