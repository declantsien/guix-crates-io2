(define-module (crates-io mc o- mco-gen) #:use-module (crates-io))

(define-public crate-mco-gen-0.1.0 (c (n "mco-gen") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "winapi") (r "^0.3") (f (quote ("memoryapi" "sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0iggzvsli13hvadpz0j9lml3l0nlqvns8308arkax1f27874c672")))

(define-public crate-mco-gen-0.1.1 (c (n "mco-gen") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)) (d (n "winapi") (r "^0.3") (f (quote ("memoryapi" "sysinfoapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "16r2wiiq8p14n1h44hzw6klni76pf5nq6myx8la3hl0wwp6h50lj")))

