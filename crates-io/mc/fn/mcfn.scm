(define-module (crates-io mc fn mcfn) #:use-module (crates-io))

(define-public crate-mcfn-0.0.1 (c (n "mcfn") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "termstatus") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1y9rkzx4k1bc7qrjfslxx98ji7wz7ya7dydlc9nfyfwa6if56k2c") (y #t)))

