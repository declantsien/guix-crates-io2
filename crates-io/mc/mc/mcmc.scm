(define-module (crates-io mc mc mcmc) #:use-module (crates-io))

(define-public crate-mcmc-0.0.1 (c (n "mcmc") (v "0.0.1") (h "1c96glg21fbpj0h4x77f6ckkk68k95m6jcwdbfx4bzr868fbm93k")))

(define-public crate-mcmc-0.1.3 (c (n "mcmc") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "arima") (r "^0.2.0") (d #t) (k 0)))) (h "19bg2d3mrcla97i0j7y78g59cxjm673vgml0xcw1hg9ahacp9qxz")))

