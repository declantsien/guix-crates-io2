(define-module (crates-io mc #{17}# mc173) #:use-module (crates-io))

(define-public crate-mc173-0.1.0 (c (n "mc173") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.24") (d #t) (k 0)) (d (n "indexmap") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "09wpgfwq8pxs71gcq24pfqfkk3cd2wdgs2k96v8nnqnxpgw6pc44")))

(define-public crate-mc173-0.2.0 (c (n "mc173") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.24") (d #t) (k 0)) (d (n "indexmap") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1s393snb9acm7f705b9pf86caab0hr1wgn3dk7h5z18l0nqn65xz")))

