(define-module (crates-io mc x- mcx-pac) #:use-module (crates-io))

(define-public crate-mcx-pac-0.0.1 (c (n "mcx-pac") (v "0.0.1") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "panic-halt") (r "^0") (d #t) (k 2)) (d (n "ral-registers") (r "^0.1.3") (d #t) (k 0)))) (h "1prk8a1lz7d350kb008gjmgx2psjxvb7kfvqfrnl28pdj7p4ipv6") (f (quote (("rt" "cortex-m-rt/device") ("mcxn947") ("mcxn946") ("mcxn547") ("mcxn546") ("default" "rt"))))))

