(define-module (crates-io mc pr mcproto-rs) #:use-module (crates-io))

(define-public crate-mcproto-rs-0.2.0 (c (n "mcproto-rs") (v "0.2.0") (d (list (d (n "base64") (r "^0.12.3") (f (quote ("alloc"))) (k 0)) (d (n "flate2") (r "^1.0.17") (d #t) (k 2)) (d (n "rand") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive" "alloc"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)))) (h "1avpjxx0hipd16n1hs1yqjcnpnaa220pdd62j0pi8y416y977nlm") (f (quote (("v1_16_3") ("v1_15_2") ("std" "rand") ("gat") ("default" "std" "bench" "v1_15_2" "v1_16_3") ("bench"))))))

