(define-module (crates-io mc cs mccs-db) #:use-module (crates-io))

(define-public crate-mccs-db-0.0.1 (c (n "mccs-db") (v "0.0.1") (d (list (d (n "mccs") (r "^0.0.1") (d #t) (k 0)) (d (n "mccs-caps") (r "^0.0.1") (d #t) (k 2)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.33") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.33") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "1n56wh2371j6mw5fqwcaysysfx9bmnwc4vlpls8zlbx62z59hyd1")))

(define-public crate-mccs-db-0.0.2 (c (n "mccs-db") (v "0.0.2") (d (list (d (n "mccs") (r "~0.0.1") (d #t) (k 0)) (d (n "mccs-caps") (r "~0.0.1") (d #t) (k 2)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.33") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.33") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "1rvnrjxl3xa4a2a9ndclv72i5jhyk93s3szs3f9qjgxahlamw7qs")))

(define-public crate-mccs-db-0.1.0 (c (n "mccs-db") (v "0.1.0") (d (list (d (n "mccs") (r "^0.1.0") (d #t) (k 0)) (d (n "mccs-caps") (r "^0.1.0") (d #t) (k 2)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.33") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.33") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "0s6dpvhcdccl7d8kapxq9a1835n41c4h2c6ks8gy6f6y8zx7q32p")))

(define-public crate-mccs-db-0.1.1 (c (n "mccs-db") (v "0.1.1") (d (list (d (n "mccs") (r "^0.1.0") (d #t) (k 0)) (d (n "mccs-caps") (r "^0.1.0") (d #t) (k 2)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.33") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.33") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "0ipwwgd1yhqhvj3l26qvlirx05vxqfhnkkigh67bnpmjcidcnd3i")))

(define-public crate-mccs-db-0.1.2 (c (n "mccs-db") (v "0.1.2") (d (list (d (n "mccs") (r "^0.1.0") (d #t) (k 0)) (d (n "mccs-caps") (r "^0.1.0") (d #t) (k 2)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.33") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.33") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7.3") (d #t) (k 0)))) (h "1l16h39ww750d3mlzk1q6fqmqshhr6vgms31qh42j7p1w6xnywlr")))

(define-public crate-mccs-db-0.1.3 (c (n "mccs-db") (v "0.1.3") (d (list (d (n "mccs") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)) (d (n "mccs-caps") (r "^0.1") (d #t) (k 2)))) (h "0f4xyilm4szclvh4vzqx85qvqn0hvjxwpf61xj5r38d137zainiw") (f (quote (("test-nom-errors" "nom/verbose-errors"))))))

(define-public crate-mccs-db-0.2.0 (c (n "mccs-db") (v "0.2.0") (d (list (d (n "mccs") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "mccs-caps") (r "^0.2") (d #t) (k 2)))) (h "1ibxb0mlg96c9mpnz5adsx58alx1sgn4bfwyx20ys9hfw597jh1z")))

