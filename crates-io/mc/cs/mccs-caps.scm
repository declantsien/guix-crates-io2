(define-module (crates-io mc cs mccs-caps) #:use-module (crates-io))

(define-public crate-mccs-caps-0.0.1 (c (n "mccs-caps") (v "0.0.1") (d (list (d (n "mccs") (r "^0.0.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)))) (h "13yrb2by04had1czg1w5sclp0zn1snhrw5aplzr6fnf0fi02p6g1")))

(define-public crate-mccs-caps-0.0.2 (c (n "mccs-caps") (v "0.0.2") (d (list (d (n "mccs") (r "~0.0.1") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)))) (h "1n9p3llb2pbck8hzw4zb48yj4la03h5317m167i6r81q32valvik")))

(define-public crate-mccs-caps-0.1.0 (c (n "mccs-caps") (v "0.1.0") (d (list (d (n "mccs") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^3.2.1") (d #t) (k 0)))) (h "1kp3rcngwwl39lhk03hshx9qqyinywqpwyc12wzkzidf8l9nnxnr")))

(define-public crate-mccs-caps-0.1.3 (c (n "mccs-caps") (v "0.1.3") (d (list (d (n "mccs") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^3") (d #t) (k 0)))) (h "1560krp2swrjd1lp5a3pzbzd0cccp0mbwxm2rxlpkc1v3b863fcf") (f (quote (("test-nom-errors" "nom/verbose-errors"))))))

(define-public crate-mccs-caps-0.2.0 (c (n "mccs-caps") (v "0.2.0") (d (list (d (n "mccs") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "124jw2fcdv25zc5c2sp7b4jwqp166iq1bczblz310pgipsfrqwj7")))

