(define-module (crates-io mc cs mccs) #:use-module (crates-io))

(define-public crate-mccs-0.0.1 (c (n "mccs") (v "0.0.1") (d (list (d (n "void") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "1bzwpg9dki8x1np6kxgckybgh5q6p065dr42hqa8cmpwwb062wss") (f (quote (("default" "void"))))))

(define-public crate-mccs-0.0.2 (c (n "mccs") (v "0.0.2") (d (list (d (n "void") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "08z96mq3k6ja4kri0z6987wv260j9m7jk0wzfda48kqr6k1wpp03") (f (quote (("default" "void"))))))

(define-public crate-mccs-0.1.0 (c (n "mccs") (v "0.1.0") (d (list (d (n "void") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "08l41y7bsj2h1mln21d6xml7lrkyk9wlc6jmskh4348plinnqdkl") (f (quote (("default" "void"))))))

(define-public crate-mccs-0.1.3 (c (n "mccs") (v "0.1.3") (d (list (d (n "void") (r "^1") (o #t) (d #t) (k 0)))) (h "1kc8345bgacw5bl9hrbcxwz4z4m1vaf6n2k6icaysbylvsrxd430") (f (quote (("default" "void"))))))

(define-public crate-mccs-0.2.0 (c (n "mccs") (v "0.2.0") (h "0q1k6bwpzmni0aimfbxzhk752g4chmgnn1wa57wdb9y9jx9anqra")))

