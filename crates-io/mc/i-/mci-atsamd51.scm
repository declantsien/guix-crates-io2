(define-module (crates-io mc i- mci-atsamd51) #:use-module (crates-io))

(define-public crate-mci-atsamd51-0.1.0 (c (n "mci-atsamd51") (v "0.1.0") (d (list (d (n "atsamd-hal") (r "~0.8") (o #t) (k 0)) (d (n "bit_field") (r "~0.10") (d #t) (k 0)) (d (n "cortex-m-rt") (r "~0.6") (o #t) (d #t) (k 0)) (d (n "embedded-error") (r "^0.3") (d #t) (k 0)) (d (n "mci") (r "^0.1") (d #t) (k 0)))) (h "1kxz9n7qjys3i1800xfc83w5in7xkcjmmphpac2ax4p1s1ddk0mc") (f (quote (("unproven" "atsamd-hal/unproven") ("sdio" "mci/sdio") ("rt" "cortex-m-rt" "atsamd-hal/samd51j20a-rt") ("mmc" "mci/mmc") ("default" "atsamd51j20a" "sdio" "mmc") ("atsamd51j20a" "atsamd51" "atsamd-hal/samd51j20a") ("atsamd51" "rt" "atsamd-hal" "atsamd-hal/samd51" "unproven"))))))

