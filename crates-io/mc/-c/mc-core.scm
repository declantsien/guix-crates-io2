(define-module (crates-io mc -c mc-core) #:use-module (crates-io))

(define-public crate-mc-core-0.1.0 (c (n "mc-core") (v "0.1.0") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "hecs") (r "^0.6") (d #t) (k 0)) (d (n "named-binary-tag") (r "^0.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "14yjwc9ibkk1pay7r4gvk7py3sy5qvnqla02v01pknczvh614n5b")))

(define-public crate-mc-core-0.1.1 (c (n "mc-core") (v "0.1.1") (d (list (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "hecs") (r "^0.6") (d #t) (k 0)) (d (n "named-binary-tag") (r "^0.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0jhfnallma0x9ypgf5pff1br4ywgnr13cgljb0f66yk81qsal4r1")))

