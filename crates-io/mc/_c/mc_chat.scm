(define-module (crates-io mc _c mc_chat) #:use-module (crates-io))

(define-public crate-mc_chat-0.1.0 (c (n "mc_chat") (v "0.1.0") (d (list (d (n "erased-serde") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fy4xpdp8y7sqvjcl7l33q3fghw12ad3y94bzl3c19c99wxb91ih") (f (quote (("use-serde" "serde" "erased-serde") ("default" "use-serde"))))))

(define-public crate-mc_chat-0.3.0 (c (n "mc_chat") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0bxvvkxdk9srjkck74d99pwkvmq37dsigr8a1s0p9rd532h506fw")))

