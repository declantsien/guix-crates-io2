(define-module (crates-io mc dl mcdl) #:use-module (crates-io))

(define-public crate-mcdl-0.0.1 (c (n "mcdl") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha-1") (r "^0.9.8") (d #t) (k 0)) (d (n "tokio") (r "^1.6.1") (f (quote ("full"))) (d #t) (k 0)))) (h "14a4g9qkgn57906947r188vshf6ild0a93rjjky1a1ki7ymx4csi")))

(define-public crate-mcdl-0.0.2 (c (n "mcdl") (v "0.0.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha-1") (r "^0.9.8") (d #t) (k 0)) (d (n "ureq") (r "^2.3.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0zn12r6w5icagv335rdrn0ia79ydcz6qn6qp1awq6wxsn9s4s683")))

