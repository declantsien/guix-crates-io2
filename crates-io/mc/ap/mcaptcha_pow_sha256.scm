(define-module (crates-io mc ap mcaptcha_pow_sha256) #:use-module (crates-io))

(define-public crate-mcaptcha_pow_sha256-0.4.0 (c (n "mcaptcha_pow_sha256") (v "0.4.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (f (quote ("serde" "num-bigint"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0ldxsi9v1xjvjzf7mjd1aqdaf4r0l744jiq68y7zp8pcrj9zhxys")))

(define-public crate-mcaptcha_pow_sha256-0.5.0 (c (n "mcaptcha_pow_sha256") (v "0.5.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (f (quote ("serde" "num-bigint"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)))) (h "0idsz0ix4z7akpgkn7499jxg07xk17rk901f0aag4gkz7d3hgc48") (f (quote (("incremental") ("default"))))))

