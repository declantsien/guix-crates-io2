(define-module (crates-io mc ap mcaptcha-api-rs) #:use-module (crates-io))

(define-public crate-mcaptcha-api-rs-0.1.0 (c (n "mcaptcha-api-rs") (v "0.1.0") (d (list (d (n "actix-rt") (r "^2.9.0") (d #t) (k 2)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json" "gzip" "native-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.194") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "16izadv2kpbn6ycbdqcsb45q2sjbhmnw0jj8cqxg52fxxmizjy3r")))

