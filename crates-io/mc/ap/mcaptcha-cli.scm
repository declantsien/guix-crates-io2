(define-module (crates-io mc ap mcaptcha-cli) #:use-module (crates-io))

(define-public crate-mcaptcha-cli-0.2.0 (c (n "mcaptcha-cli") (v "0.2.0") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mcaptcha_pow_sha256") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "gzip" "native-tls-vendored"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1.33") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "url") (r "^2.4.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0k5vhkk608lsajsgdbsn0jx7wl7yy29x40x0b29qmx4fy86lmdxm")))

