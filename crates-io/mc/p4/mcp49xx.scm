(define-module (crates-io mc p4 mcp49xx) #:use-module (crates-io))

(define-public crate-mcp49xx-0.1.0 (c (n "mcp49xx") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "184fc03afs9xi0mr9xrxsnzjw1cd9ab448bh6bxipgij56wsg76r")))

(define-public crate-mcp49xx-0.2.0 (c (n "mcp49xx") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "1jrn9qk1b7igh536b6s7ixvkpnjp5r0sndai3hjlja3lf1kvcngn")))

(define-public crate-mcp49xx-0.3.0 (c (n "mcp49xx") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "11pmq32w540aysnm8a46xylpxk8c0sj4v8s89vxn1c1lhvvwq33l")))

