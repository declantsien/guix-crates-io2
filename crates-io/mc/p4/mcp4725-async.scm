(define-module (crates-io mc p4 mcp4725-async) #:use-module (crates-io))

(define-public crate-mcp4725-async-0.1.0 (c (n "mcp4725-async") (v "0.1.0") (d (list (d (n "defmt-03") (r "^0.3") (o #t) (d #t) (k 0) (p "defmt")) (d (n "embedded-hal-async") (r "^1.0.0") (d #t) (k 0)) (d (n "futures-lite") (r "^2.3.0") (d #t) (k 2)))) (h "1w6ni7sfz837yi4jsf2imxxhrim6v27mbl34msaja20a66xjfdp4") (s 2) (e (quote (("defmt-03" "dep:defmt-03"))))))

