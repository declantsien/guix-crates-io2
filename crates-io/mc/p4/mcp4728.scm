(define-module (crates-io mc p4 mcp4728) #:use-module (crates-io))

(define-public crate-mcp4728-0.1.0 (c (n "mcp4728") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (t "cfg(target_os = \"linux\")") (k 2)) (d (n "num_enum") (r "^0.5.7") (k 0)))) (h "1x2x2vflyihp0vib4c55vljsgs88j2xkp73f6lf8fm76rbnmyini")))

