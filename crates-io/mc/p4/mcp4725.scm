(define-module (crates-io mc p4 mcp4725) #:use-module (crates-io))

(define-public crate-mcp4725-0.1.0 (c (n "mcp4725") (v "0.1.0") (d (list (d (n "cortex-m-rt") (r "^0.6.7") (d #t) (k 2)) (d (n "cortex-m-semihosting") (r "^0.3.2") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "nb") (r "^0.1") (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.5.1") (d #t) (k 2)) (d (n "stm32f1xx-hal") (r "^0.3.0") (f (quote ("rt" "stm32f103"))) (d #t) (k 2)))) (h "0x0rc203954yg370cyrkmhq2yj6j2r4dw4zi40nnrk3ykskxzz9r")))

(define-public crate-mcp4725-0.1.1 (c (n "mcp4725") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "18b5xkvfs8akpgkdbxqw4gm3wnzzjlxa9rsqarkhnsxnc7k9dsf3")))

(define-public crate-mcp4725-0.1.2 (c (n "mcp4725") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1vic8l9j8ab0v37bq4nkpkvjsn6bbn15fr4ald53c8hf6yh9pskg")))

(define-public crate-mcp4725-0.2.0 (c (n "mcp4725") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "0qmml5f7bpgvxlw07m3s68kbl7sqnjp0bzzhlf23srzgw9wqy18p")))

(define-public crate-mcp4725-0.2.1 (c (n "mcp4725") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1bvalkgiy34pjl28apcsb635j2db7n5kbflayq36m4cr4gry0z71")))

(define-public crate-mcp4725-0.2.2 (c (n "mcp4725") (v "0.2.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "0w5knk17lnmcvjnf4cvqd9lmvadwa62k4grdplwwwsl65qckv6zw")))

(define-public crate-mcp4725-0.3.0 (c (n "mcp4725") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.1") (d #t) (k 2)))) (h "131bxip2jwkfckz1x0xsdl4nb7vwb70cyizr80m46cx30c5zs961")))

(define-public crate-mcp4725-0.3.1 (c (n "mcp4725") (v "0.3.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.1") (d #t) (k 2)))) (h "1rb4yvbp0g8x8g3l5j6agrf7h3vszsyb3hhila1lpm4v8lqpjj10")))

(define-public crate-mcp4725-0.4.0 (c (n "mcp4725") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)))) (h "03b0wsssd5mykhj7dc318cvwnc112i7jfw4961ifxrynp29cbqb6")))

(define-public crate-mcp4725-0.4.1 (c (n "mcp4725") (v "0.4.1") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)))) (h "1zqvkmdfk2yswiwdia8hlgh7qmjaq8411x4a5xvir6rdcm5fgycb")))

(define-public crate-mcp4725-0.4.2 (c (n "mcp4725") (v "0.4.2") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "1d32inhk95n23s192ch9vn3x5llc6fkfyshhaxvf9f4p8fhrkrfw")))

