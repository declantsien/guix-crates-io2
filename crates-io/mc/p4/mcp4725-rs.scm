(define-module (crates-io mc p4 mcp4725-rs) #:use-module (crates-io))

(define-public crate-mcp4725-rs-0.1.0 (c (n "mcp4725-rs") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "1i502rqrlwrwmfrsymzgq0zz2w59rgmfxivx6fmxqkqpsh4mv8m4")))

(define-public crate-mcp4725-rs-0.1.1 (c (n "mcp4725-rs") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "0wqvr4wfi70dx7d67h7ng27bkam6gldy3f5rik7d995ny1az7k43")))

