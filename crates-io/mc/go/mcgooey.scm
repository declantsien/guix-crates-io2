(define-module (crates-io mc go mcgooey) #:use-module (crates-io))

(define-public crate-mcgooey-0.1.0 (c (n "mcgooey") (v "0.1.0") (d (list (d (n "auto_impl") (r "^0.5.0") (d #t) (k 0)) (d (n "macroquad") (r "^0.3.16") (d #t) (k 0)))) (h "0635amb10kb68gaw72d5nlj8pszvkmx8h10hpi9lbq69xhib3zkq") (f (quote (("debug_draw")))) (y #t)))

(define-public crate-mcgooey-0.1.1 (c (n "mcgooey") (v "0.1.1") (d (list (d (n "auto_impl") (r "^0.5.0") (d #t) (k 0)) (d (n "macroquad") (r "^0.3.13") (d #t) (k 0)))) (h "002k75yil8agg1kpzif7fngi1ig5fyv3vr0iz55mc267ayhr2qz7") (f (quote (("debug_draw"))))))

(define-public crate-mcgooey-0.1.2 (c (n "mcgooey") (v "0.1.2") (d (list (d (n "auto_impl") (r "^0.5.0") (d #t) (k 0)) (d (n "macroquad") (r "=0.3.13") (d #t) (k 0)))) (h "1v68cj7k3xsjvzf948zsy5jzi7g38f9gdc1wy8l14mbnxbi6bvbv") (f (quote (("debug_draw"))))))

(define-public crate-mcgooey-0.1.3 (c (n "mcgooey") (v "0.1.3") (d (list (d (n "auto_impl") (r "^0.5.0") (d #t) (k 0)) (d (n "macroquad") (r "=0.3.13") (d #t) (k 0)))) (h "1k34c4kq9wz6gawzby1wyjhliklv9nbfhrs3yj6nywk2i3g4pkn6") (f (quote (("debug_draw"))))))

(define-public crate-mcgooey-0.1.4 (c (n "mcgooey") (v "0.1.4") (d (list (d (n "auto_impl") (r "^0.5.0") (d #t) (k 0)) (d (n "macroquad") (r "=0.3.13") (d #t) (k 0)))) (h "0jmk3ipmxc8cb0baimyfm6dwqn9z1iffz8pl9pkh3y7vy19swpa0") (f (quote (("debug_draw"))))))

