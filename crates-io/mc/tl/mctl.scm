(define-module (crates-io mc tl mctl) #:use-module (crates-io))

(define-public crate-mctl-0.1.0 (c (n "mctl") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 1)) (d (n "derive_more") (r "^0.99.13") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (d #t) (k 0)) (d (n "mpris") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "xdg") (r "^2.2.0") (d #t) (k 0)))) (h "02gl8qrk8j1rhzrkdr2v83n9lhif1138rhsj9lx35xml8hvlawzx")))

(define-public crate-mctl-0.1.3 (c (n "mctl") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 1)) (d (n "derive_more") (r "^0.99.13") (d #t) (k 0)) (d (n "mpd") (r "^0.0.12") (d #t) (k 0)) (d (n "mpris") (r "^1.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "xdg") (r "^2.2.0") (d #t) (k 0)))) (h "1kw2irpvszmhd1zxjn9havwc0lcy0dyzf8m9ssnhjv1sfcr7w41z")))

