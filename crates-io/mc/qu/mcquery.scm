(define-module (crates-io mc qu mcquery) #:use-module (crates-io))

(define-public crate-mcquery-1.0.0 (c (n "mcquery") (v "1.0.0") (d (list (d (n "async-minecraft-ping") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0yfrj64vwamkdz04zss0lfnnsfwgsvyym9z3n6bc30kfpyg0nswv")))

(define-public crate-mcquery-1.0.1 (c (n "mcquery") (v "1.0.1") (d (list (d (n "async-minecraft-ping") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "mc-legacy-formatting") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1sjmalw71kh5byqc2d580j1p7bgplqb7plkj2qdnk2d9asfb0976")))

(define-public crate-mcquery-1.1.0 (c (n "mcquery") (v "1.1.0") (d (list (d (n "async-minecraft-ping") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "mc-legacy-formatting") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("macros" "rt"))) (d #t) (k 0)))) (h "0dr111cdjd09x536klc2ps8053fara04kfp2dqf8b98dch4rxps4")))

(define-public crate-mcquery-1.1.1 (c (n "mcquery") (v "1.1.1") (d (list (d (n "async-minecraft-ping") (r "^0.8") (d #t) (k 0)) (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "mc-legacy-formatting") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("macros" "rt"))) (d #t) (k 0)))) (h "06d77hdzhbj6fhbspaavmg3k8w3vj7i0scmf4gm2spxmy0kidqay")))

