(define-module (crates-io mc mp mcmp) #:use-module (crates-io))

(define-public crate-mcmp-0.1.2 (c (n "mcmp") (v "0.1.2") (h "0q4dywqrdrnsnipnmc5q8f04l8p5bgbim24rqf6bnrlcs7825r1c") (y #t)))

(define-public crate-mcmp-0.1.3 (c (n "mcmp") (v "0.1.3") (h "10nr0whzyxiygv6d0m3il5pjk7kixdvx3j1fdl89ryd1yi19wilr") (y #t)))

(define-public crate-mcmp-0.1.4 (c (n "mcmp") (v "0.1.4") (h "18pw2igz4nw0sn4sikhfpvxgyvkd0i38q20il4csvs0q8srqwfjq") (y #t)))

