(define-module (crates-io mc be mcbe-lan-advertizer) #:use-module (crates-io))

(define-public crate-mcbe-lan-advertizer-0.1.0 (c (n "mcbe-lan-advertizer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1spm0ndnw02m2c2q1d0fly17byv3qhrdr78iqpn4nr5a4bwj5d4c")))

(define-public crate-mcbe-lan-advertizer-0.1.1 (c (n "mcbe-lan-advertizer") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10ayjfyp04hfmma56mj2ygzfkwmwpnrdfw95la94v969g93p8q6j")))

