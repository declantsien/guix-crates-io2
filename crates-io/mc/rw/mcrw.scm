(define-module (crates-io mc rw mcrw) #:use-module (crates-io))

(define-public crate-mcrw-0.4.0 (c (n "mcrw") (v "0.4.0") (d (list (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)))) (h "1iijvmlpi5qwvwf7xwjswyz42a3l610h3q7dbgdxsfx6s5fzf74r")))

