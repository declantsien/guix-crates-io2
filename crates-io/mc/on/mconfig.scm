(define-module (crates-io mc on mconfig) #:use-module (crates-io))

(define-public crate-mconfig-0.1.0 (c (n "mconfig") (v "0.1.0") (d (list (d (n "futures") (r "^0") (d #t) (k 0)) (d (n "mongodb") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^2") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "0kj1ksgszrnvnrjbpmlam2l7x4l1pw201qzhii11gzk2lbs3wbj0")))

(define-public crate-mconfig-0.2.0 (c (n "mconfig") (v "0.2.0") (d (list (d (n "futures") (r "^0") (d #t) (k 0)) (d (n "mongodb") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^2") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "19r9d8ffv92pagn633lyg5l96rmig4dsgmq1rlwhr9rc2wr2f9cw")))

(define-public crate-mconfig-0.2.1 (c (n "mconfig") (v "0.2.1") (d (list (d (n "futures") (r "^0") (d #t) (k 0)) (d (n "mongodb") (r "^2") (d #t) (k 0)) (d (n "qrt-log-utils") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^2") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)))) (h "06x116sxvrwzxmssy3b73l7373p9bg5fbb868hkrwfyf0xcmipp1")))

