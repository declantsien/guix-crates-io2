(define-module (crates-io mc _a mc_auth) #:use-module (crates-io))

(define-public crate-mc_auth-0.1.0 (c (n "mc_auth") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0d32lg0zag8na4d10hbvs28mza4dzx37x3da04y5zjk6wchn9alw")))

