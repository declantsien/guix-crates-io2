(define-module (crates-io mc -v mc-vanilla) #:use-module (crates-io))

(define-public crate-mc-vanilla-0.1.0 (c (n "mc-vanilla") (v "0.1.0") (d (list (d (n "mc-core") (r "^0.1.0") (d #t) (k 0)) (d (n "named-binary-tag") (r "^0.6") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "1x462q6p3vd629hqd781w4ikhznzjaz34kfapdzndhaxigixlqjx")))

(define-public crate-mc-vanilla-0.1.1 (c (n "mc-vanilla") (v "0.1.1") (d (list (d (n "mc-core") (r "^0.1.1") (d #t) (k 0)) (d (n "named-binary-tag") (r "^0.6") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "07ch8bi0jchb7jf4h0bi8h6a9agps6rdyw2wz8wk4gimf8rm0brx")))

