(define-module (crates-io mc -v mc-varint) #:use-module (crates-io))

(define-public crate-mc-varint-0.1.0 (c (n "mc-varint") (v "0.1.0") (h "0ww3gsga0am6wl3mp7zd09rz220khmbz0y106zi9f0bhvis61rq1")))

(define-public crate-mc-varint-0.1.1 (c (n "mc-varint") (v "0.1.1") (h "02dvys27qk2x3c2rqh172sd8xr1i8nyilpsa192my79p9q6xa1v4")))

