(define-module (crates-io mc to mctools) #:use-module (crates-io))

(define-public crate-mctools-0.0.0 (c (n "mctools") (v "0.0.0") (h "0f9d8xl1yyr8k78j2037x9vrj3dy8ncjhwzc7nir2c6mr5mgwq7j")))

(define-public crate-mctools-0.1.0 (c (n "mctools") (v "0.1.0") (d (list (d (n "png") (r "^0.17.13") (d #t) (k 0)))) (h "0zyr5288wxaqg31h6b6iy7jxbhghgmvq2kkh7a5c00dg203pfk8l")))

(define-public crate-mctools-0.1.1 (c (n "mctools") (v "0.1.1") (d (list (d (n "png") (r "^0.17.13") (d #t) (k 0)))) (h "1abz2nwsz42qyai7bldjx8sfvxz0m1hm7brrpx6cnmda6x6xv7r2")))

