(define-module (crates-io mc -o mc-oblivious-traits) #:use-module (crates-io))

(define-public crate-mc-oblivious-traits-1.0.0 (c (n "mc-oblivious-traits") (v "1.0.0") (d (list (d (n "aligned-cmov") (r "^1") (d #t) (k 0)) (d (n "balanced-tree-index") (r "^1") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (k 0)))) (h "1vf366zdgfbr4v88amlqx7a70wzsdhncmjn5sk9fbzlnn8jmbbi9") (f (quote (("no_asm_insecure" "aligned-cmov/no_asm_insecure"))))))

(define-public crate-mc-oblivious-traits-2.0.0 (c (n "mc-oblivious-traits") (v "2.0.0") (d (list (d (n "aligned-cmov") (r "^2") (d #t) (k 0)) (d (n "balanced-tree-index") (r "^2") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)))) (h "1l133xkj2y60icmri1r2bl2mahvns97ff9098rs6j38v7c1r8rb9") (f (quote (("no_asm_insecure" "aligned-cmov/no_asm_insecure"))))))

(define-public crate-mc-oblivious-traits-2.2.0 (c (n "mc-oblivious-traits") (v "2.2.0") (d (list (d (n "aligned-cmov") (r "^2.2") (d #t) (k 0)) (d (n "balanced-tree-index") (r "^2.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)))) (h "0j5qband5kdkmxzda6yy3cm0dnvk3b22373lq3khrnk5zlhv50ij") (f (quote (("no_asm_insecure" "aligned-cmov/no_asm_insecure"))))))

(define-public crate-mc-oblivious-traits-2.3.0 (c (n "mc-oblivious-traits") (v "2.3.0") (d (list (d (n "aligned-cmov") (r "^2.3") (d #t) (k 0)) (d (n "balanced-tree-index") (r "^2.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)))) (h "09z1slmkkzaawbbzay798crm9c4myn10hkcm607v8p7q50wvz82n") (f (quote (("no_asm_insecure" "aligned-cmov/no_asm_insecure"))))))

