(define-module (crates-io mc -o mc-oblivious-ram) #:use-module (crates-io))

(define-public crate-mc-oblivious-ram-1.0.0 (c (n "mc-oblivious-ram") (v "1.0.0") (d (list (d (n "aligned-cmov") (r "^1") (d #t) (k 0)) (d (n "balanced-tree-index") (r "^1") (d #t) (k 0)) (d (n "mc-oblivious-traits") (r "^1") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (k 0)))) (h "063jr0h8gfr115ma45z2xzhxh207y7js4yb16rw8fwjl3fc0ma47") (f (quote (("no_asm_insecure" "aligned-cmov/no_asm_insecure"))))))

(define-public crate-mc-oblivious-ram-2.0.0 (c (n "mc-oblivious-ram") (v "2.0.0") (d (list (d (n "aligned-cmov") (r "^2") (d #t) (k 0)) (d (n "balanced-tree-index") (r "^2") (d #t) (k 0)) (d (n "mc-oblivious-traits") (r "^2") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)))) (h "0yfp6bjib7pnygw09azf717hhlqxdz5k2r8imwk8rmglhhdpc5ak") (f (quote (("no_asm_insecure" "aligned-cmov/no_asm_insecure"))))))

(define-public crate-mc-oblivious-ram-2.2.0 (c (n "mc-oblivious-ram") (v "2.2.0") (d (list (d (n "aligned-cmov") (r "^2.2") (d #t) (k 0)) (d (n "balanced-tree-index") (r "^2.2") (d #t) (k 0)) (d (n "mc-oblivious-traits") (r "^2.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)))) (h "05wdx4znfcs5rwa9hl223gabww0cv5kqpr9s3mps8xga228nl1c5") (f (quote (("no_asm_insecure" "aligned-cmov/no_asm_insecure"))))))

(define-public crate-mc-oblivious-ram-2.3.0 (c (n "mc-oblivious-ram") (v "2.3.0") (d (list (d (n "aligned-cmov") (r "^2.3") (d #t) (k 0)) (d (n "balanced-tree-index") (r "^2.3") (d #t) (k 0)) (d (n "mc-oblivious-traits") (r "^2.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "1p5asdh387lwqpsyv4lf9dva33w5i4dm6y50x8ir3wf9cjsr3cw6") (f (quote (("no_asm_insecure" "aligned-cmov/no_asm_insecure"))))))

