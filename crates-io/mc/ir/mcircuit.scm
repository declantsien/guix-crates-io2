(define-module (crates-io mc ir mcircuit) #:use-module (crates-io))

(define-public crate-mcircuit-0.1.0 (c (n "mcircuit") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "variant_count") (r "^1.1") (d #t) (k 0)))) (h "01nq67jmxjh5q0xfip7ya6nc1lr255nx4f7kqk846x6lwh6wiq2q")))

(define-public crate-mcircuit-0.1.1 (c (n "mcircuit") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "variant_count") (r "^1.1") (d #t) (k 0)))) (h "1ik7k2g4nahzxrpcndz7y7nk50i1i57mxmhpcghdbh3x58kc7mix")))

(define-public crate-mcircuit-0.1.2 (c (n "mcircuit") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "variant_count") (r "^1.1") (d #t) (k 0)))) (h "0r1sckfw876wy8wjrqnr9vxl21xly2lfnlciw74grg8k69p1n7fj")))

(define-public crate-mcircuit-0.1.3 (c (n "mcircuit") (v "0.1.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "variant_count") (r "^1.1") (d #t) (k 0)))) (h "1yzx0mrajc3amg21cvx2di5ggvc4qb5bklly5cms651rkb5j2hwb")))

(define-public crate-mcircuit-0.1.4 (c (n "mcircuit") (v "0.1.4") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lexpr") (r "^0.2.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "variant_count") (r "^1.1") (d #t) (k 0)))) (h "0jifcdjbklkzhrb27m20l0yv4kjn6k1crjwabgnvwhz4raxwd94j")))

(define-public crate-mcircuit-0.1.5 (c (n "mcircuit") (v "0.1.5") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lexpr") (r "^0.2.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "variant_count") (r "^1.1") (d #t) (k 0)))) (h "0ff0y62m01xzcrmrib9y9r0wf73vp20v8snb09zv9nljqxsl5nvx")))

(define-public crate-mcircuit-0.1.6 (c (n "mcircuit") (v "0.1.6") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lexpr") (r "^0.2.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "variant_count") (r "^1.1") (d #t) (k 0)))) (h "0mr26670br2cx1zs07wv50hmg5fq920x8nn001nkbdg8s9sggw92")))

(define-public crate-mcircuit-0.1.7 (c (n "mcircuit") (v "0.1.7") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lexpr") (r "^0.2.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "variant_count") (r "^1.1") (d #t) (k 0)))) (h "06pnbl4xwpy99y3nggzn8qyq7i69js3jx6f8g9n0daw18m6sj7m3")))

(define-public crate-mcircuit-0.1.8 (c (n "mcircuit") (v "0.1.8") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lexpr") (r "^0.2.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "variant_count") (r "^1.1") (d #t) (k 0)))) (h "0dpzg10vrrs06ff64bzbmsja4qhpyfkcdbq33vgzbkrlgk3vs5i2")))

(define-public crate-mcircuit-0.1.9 (c (n "mcircuit") (v "0.1.9") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lexpr") (r "^0.2.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "variant_count") (r "^1.1") (d #t) (k 0)))) (h "17g7plxrpzbpdkbahcg1c5lckmgi8wd73xvcyqawzm2y41z98x2w")))

(define-public crate-mcircuit-0.1.10 (c (n "mcircuit") (v "0.1.10") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "lexpr") (r "^0.2.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "variant_count") (r "^1.1") (d #t) (k 0)))) (h "1b0ix28z6wjpjr18zywxvjf896vh4fwb0d9vmsvk7z2nynr4xkgq")))

