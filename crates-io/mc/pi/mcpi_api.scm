(define-module (crates-io mc pi mcpi_api) #:use-module (crates-io))

(define-public crate-mcpi_api-0.1.0 (c (n "mcpi_api") (v "0.1.0") (h "0pwskr52xkyr7a1wylw1nxf5ihy2zi0jxlh0vl9zq9gdmwyghi6s")))

(define-public crate-mcpi_api-0.1.1 (c (n "mcpi_api") (v "0.1.1") (h "0l2lqws9nqsvf47sknvxjla0hdw27lishd0fixkqd71f38xnz9c7")))

(define-public crate-mcpi_api-0.2.1 (c (n "mcpi_api") (v "0.2.1") (h "16ywqvmdg90l7j011mxgbg8685bg7r8i651w46ls4r61vh83hfp2")))

(define-public crate-mcpi_api-0.2.2 (c (n "mcpi_api") (v "0.2.2") (h "11pdiy1axvfza8gpjr31i7xknllmyrb27630vbpzafhf288j62vz")))

