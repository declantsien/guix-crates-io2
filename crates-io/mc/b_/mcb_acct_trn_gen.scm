(define-module (crates-io mc b_ mcb_acct_trn_gen) #:use-module (crates-io))

(define-public crate-mcb_acct_trn_gen-5.0.0 (c (n "mcb_acct_trn_gen") (v "5.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1s74dcz0k34rx22y0fphvq2v2cfniaa0w2fwfj81s4qgg40y3my7")))

