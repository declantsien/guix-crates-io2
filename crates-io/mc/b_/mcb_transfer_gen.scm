(define-module (crates-io mc b_ mcb_transfer_gen) #:use-module (crates-io))

(define-public crate-mcb_transfer_gen-5.0.0 (c (n "mcb_transfer_gen") (v "5.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1yvhj38f79zqzhmvw49ylxixcarfvh3p3vwlf53x3m2q2lzk9qi7")))

