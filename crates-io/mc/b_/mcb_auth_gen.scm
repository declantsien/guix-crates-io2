(define-module (crates-io mc b_ mcb_auth_gen) #:use-module (crates-io))

(define-public crate-mcb_auth_gen-5.0.0 (c (n "mcb_auth_gen") (v "5.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0qb7xf1c7q8w5phbk35b0jji98np8xx2wb14hk1rjn80li8b096d")))

