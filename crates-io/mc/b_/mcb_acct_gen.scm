(define-module (crates-io mc b_ mcb_acct_gen) #:use-module (crates-io))

(define-public crate-mcb_acct_gen-5.0.0-beta.90 (c (n "mcb_acct_gen") (v "5.0.0-beta.90") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "144mg1bp8rmw6j9mixpbbdkw95yv4y5lpr611zlym12zm30wk1p9")))

(define-public crate-mcb_acct_gen-5.0.0 (c (n "mcb_acct_gen") (v "5.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1mkz5gc4sac9wf2m2cvdlldigdi848k1rqgf4hbg11g4177bd8ql")))

