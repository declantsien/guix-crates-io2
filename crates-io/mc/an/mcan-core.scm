(define-module (crates-io mc an mcan-core) #:use-module (crates-io))

(define-public crate-mcan-core-0.1.0 (c (n "mcan-core") (v "0.1.0") (d (list (d (n "fugit") (r "^0.3") (d #t) (k 0)))) (h "1gzl5256kr06prhznsmn24pxzpb61niyclqy4wh0n774wvanw8cs")))

(define-public crate-mcan-core-0.1.1 (c (n "mcan-core") (v "0.1.1") (d (list (d (n "fugit") (r "^0.3") (d #t) (k 0)))) (h "1p4vzivcwkn8h1ny5am7bgvlzlh1fhijhj5d7zg5b8smphyxvbbh")))

(define-public crate-mcan-core-0.2.0 (c (n "mcan-core") (v "0.2.0") (d (list (d (n "fugit") (r "^0.3") (d #t) (k 0)))) (h "05nbmah1sknkhrjixxk5b7q06985zhyf85fgah0bnkgyprh49mdm")))

(define-public crate-mcan-core-0.2.1 (c (n "mcan-core") (v "0.2.1") (d (list (d (n "fugit") (r "^0.3") (d #t) (k 0)))) (h "1ac121nblpnjgqddcgy3vd6gv5c71rc294myqqc128p2c2wvq571")))

(define-public crate-mcan-core-0.2.2 (c (n "mcan-core") (v "0.2.2") (d (list (d (n "fugit") (r "^0.3") (d #t) (k 0)))) (h "1jppd4p8fqy90vq7fhrvywg6r4ff7h83fvc5rijj97i6ym7ywgcb")))

