(define-module (crates-io mc an mcan) #:use-module (crates-io))

(define-public crate-mcan-0.1.0 (c (n "mcan") (v "0.1.0") (d (list (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1g2xvkhrr7ld21ni7zj36vwiy35s5p7lbdakccrb0rd2373f8w1i") (y #t)))

(define-public crate-mcan-0.2.0 (c (n "mcan") (v "0.2.0") (d (list (d (n "bitfield") (r "^0.14") (d #t) (k 0)) (d (n "embedded-can") (r "^0.4") (d #t) (k 0)) (d (n "fugit") (r "^0.3.5") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "mcan-core") (r "^0.2") (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "1kxgrwqpw3qzp72ih0ndjvh6mqk6iqfy5bf5l2jyawzbq63nplny")))

(define-public crate-mcan-0.3.0 (c (n "mcan") (v "0.3.0") (d (list (d (n "bitfield") (r "^0.14") (d #t) (k 0)) (d (n "embedded-can") (r "^0.4") (d #t) (k 0)) (d (n "fugit") (r "^0.3.5") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "mcan-core") (r "^0.2") (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0brgipwaaidaxh9qanj3vimsjbk2hw6xqa1qx2zh9f32ym4avc8x")))

(define-public crate-mcan-0.4.0 (c (n "mcan") (v "0.4.0") (d (list (d (n "bitfield") (r "^0.14") (d #t) (k 0)) (d (n "embedded-can") (r "^0.4") (d #t) (k 0)) (d (n "fugit") (r "^0.3.5") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "mcan-core") (r "^0.2") (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0a5ifvxy2gyxqv45c47s7mxfz80v70cyglkgk07z68xlwi1g80h3")))

(define-public crate-mcan-0.5.0 (c (n "mcan") (v "0.5.0") (d (list (d (n "bitfield") (r "^0.14") (d #t) (k 0)) (d (n "embedded-can") (r "^0.4") (d #t) (k 0)) (d (n "fugit") (r "^0.3.5") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "mcan-core") (r "^0.2") (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0innmk8l3v8j5364rkhywq0mrfxgrvvgr18hrlv4kb1xp92lf74k")))

