(define-module (crates-io mc p2 mcp25625) #:use-module (crates-io))

(define-public crate-mcp25625-0.1.0 (c (n "mcp25625") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "vhrdcan") (r "^0.1.0") (d #t) (k 0)))) (h "0gykr3s1mgjl115a3qk75d3m8im9ylq004ya51qcms4zwllv0hgy")))

