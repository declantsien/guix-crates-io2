(define-module (crates-io mc p2 mcp25xx) #:use-module (crates-io))

(define-public crate-mcp25xx-0.1.0 (c (n "mcp25xx") (v "0.1.0") (d (list (d (n "embedded-can") (r "^0.3.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "1r82n8v3kynizix56mnn0wj71222a9jn2994mjx6h9k65lksswfx") (f (quote (("mcp25625") ("mcp2515"))))))

