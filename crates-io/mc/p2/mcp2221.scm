(define-module (crates-io mc p2 mcp2221) #:use-module (crates-io))

(define-public crate-mcp2221-0.1.0 (c (n "mcp2221") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "rusb") (r "^0.8.1") (d #t) (k 0)))) (h "10hx61jxkhh2xfqr0qqfk8d4il5wr63s9hhq5k7lm8zhkcnihay8")))

(define-public crate-mcp2221-0.1.1 (c (n "mcp2221") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "rusb") (r "^0.8.1") (d #t) (k 0)))) (h "1ds966gx5j0m8sq6imnhax6h6fq6gx1in7sg0pf8z1h4pwhamznl")))

