(define-module (crates-io mc p2 mcp23017) #:use-module (crates-io))

(define-public crate-mcp23017-0.1.0 (c (n "mcp23017") (v "0.1.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.13.2") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "0q7b81pw1jkmvb8aqrjwn3nilafk6fkva20hnhcj87196svpqbbq")))

(define-public crate-mcp23017-0.1.1 (c (n "mcp23017") (v "0.1.1") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.13.2") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)))) (h "1pgl1a2k9rjyxp1wqvjddf68kqpybghzag1zbhb2ddkl99458j3z")))

(define-public crate-mcp23017-1.0.0 (c (n "mcp23017") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "0b71nqcc00m5i22x025k22yxv9m6npfdq3f0jpx1qgz74xkgscic")))

