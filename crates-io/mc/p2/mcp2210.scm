(define-module (crates-io mc p2 mcp2210) #:use-module (crates-io))

(define-public crate-mcp2210-0.0.1 (c (n "mcp2210") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libudev") (r "^0.2") (d #t) (k 0)))) (h "1n45bczi47b66dqbqkyvxshjp9apaav8gc67sr1v5rdi2260vqbb")))

(define-public crate-mcp2210-0.1.0 (c (n "mcp2210") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libudev") (r "^0.2") (d #t) (k 0)))) (h "0779ddsym95cb9v3ak750kk1pgkvajrpcyaq4cmpv9h2bnr4w9kh")))

(define-public crate-mcp2210-0.2.0 (c (n "mcp2210") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "hidapi") (r "^2.4.1") (d #t) (k 0)))) (h "1ikz3010mb7bw8arcysnvgg652zvxsqmhgdxlmd2iasx3vn9m76p") (r "1.63")))

