(define-module (crates-io mc p2 mcp2515) #:use-module (crates-io))

(define-public crate-mcp2515-0.1.0 (c (n "mcp2515") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "16c39lx7m2qywd8ndmbxgpw6xh1rplh6q1a2yivffx4bd9vm0kp3")))

(define-public crate-mcp2515-0.1.1 (c (n "mcp2515") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "012y38ydl050y1c176snn0kig54gvya0hvb0nfhh25nwk3w5rbix")))

(define-public crate-mcp2515-0.2.0 (c (n "mcp2515") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "1isqcn3s8d3xcilj620qf9km1sl5xg2ilf4rxny4i3dlhbprn36x")))

(define-public crate-mcp2515-0.2.1 (c (n "mcp2515") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "defmt") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "14qrpada0md3mjp9bii47pcr2bh6sy0y14gh0ysw15sd1jbpar4v")))

(define-public crate-mcp2515-0.2.2 (c (n "mcp2515") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "defmt") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "ufmt") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "void") (r "^1.0.2") (k 0)))) (h "1zx3s9ja6aicgw69rx1r08v9pzyyd455nq350a7ns17av1ik5lnm")))

