(define-module (crates-io mc p2 mcp23x17) #:use-module (crates-io))

(define-public crate-mcp23x17-0.1.0 (c (n "mcp23x17") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.2") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (t "cfg(linux)") (k 2)))) (h "1m1ypw1zyw0fzai7ljmj332w2lfvqfysdfnahw9wf8xlmnikphnn")))

(define-public crate-mcp23x17-0.2.0 (c (n "mcp23x17") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.2") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)))) (h "0qlqmm8g7dzjjibrav2545snv07ih4d6h5kd4n7z9cnxljyxd9j4")))

