(define-module (crates-io mc p2 mcp23s17) #:use-module (crates-io))

(define-public crate-mcp23s17-0.1.0 (c (n "mcp23s17") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "0bqilwhh6pd6hf9sjy584pc1n7kc8hncn2y87r1487fpp1cksyj0")))

