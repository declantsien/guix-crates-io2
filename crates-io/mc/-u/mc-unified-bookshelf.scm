(define-module (crates-io mc -u mc-unified-bookshelf) #:use-module (crates-io))

(define-public crate-mc-unified-bookshelf-0.1.0 (c (n "mc-unified-bookshelf") (v "0.1.0") (d (list (d (n "residua-mutf8") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1l8m26bvrk11a24sr8x71zh3bfd94szd0b7qnlk0iyy7xqdy9fgh") (y #t)))

(define-public crate-mc-unified-bookshelf-0.1.1 (c (n "mc-unified-bookshelf") (v "0.1.1") (d (list (d (n "residua-mutf8") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "15mzf7ddxhdrfj4523dbv424vsp2ycz5nqljpn29rsnlxd4lmc9w") (y #t)))

(define-public crate-mc-unified-bookshelf-0.1.2 (c (n "mc-unified-bookshelf") (v "0.1.2") (d (list (d (n "residua-mutf8") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1hh3zb7lwss0500161hhr3r8f7106j4bx1d68mjrf968y4ci4670")))

(define-public crate-mc-unified-bookshelf-0.2.0 (c (n "mc-unified-bookshelf") (v "0.2.0") (d (list (d (n "flate2") (r "^1") (d #t) (k 2)) (d (n "residua-mutf8") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1ishkxa9x6pii6p0hb3zqhdps1a6z5l6zw6bp8l2s86w2ma5lmvq") (f (quote (("je-region" "je" "nbt") ("je") ("dev-be-legacy-alpha" "be") ("default" "nbt") ("be")))) (s 2) (e (quote (("nbt" "dep:residua-mutf8"))))))

(define-public crate-mc-unified-bookshelf-0.3.0 (c (n "mc-unified-bookshelf") (v "0.3.0") (d (list (d (n "flate2") (r "^1") (d #t) (k 2)) (d (n "residua-mutf8") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "16ly78dbqp2bgj90s0j2g29ccmxm3m7q2ahdgnvn8fwdii9xh7c7") (f (quote (("je-region" "je" "nbt") ("je") ("dev-be-legacy-alpha" "be") ("default" "nbt") ("be")))) (s 2) (e (quote (("nbt" "dep:residua-mutf8"))))))

(define-public crate-mc-unified-bookshelf-0.4.0 (c (n "mc-unified-bookshelf") (v "0.4.0") (d (list (d (n "flate2") (r "^1") (d #t) (k 2)) (d (n "residua-mutf8") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1il5vmy656fscnfvbnmm8ymgh89h476mr1hhmrj2ipy3lv8gb7x6") (f (quote (("je-region" "je" "nbt") ("je") ("dev-be-legacy-alpha" "be") ("default" "nbt") ("be")))) (s 2) (e (quote (("nbt" "dep:residua-mutf8"))))))

