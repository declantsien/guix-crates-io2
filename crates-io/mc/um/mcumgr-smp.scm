(define-module (crates-io mc um mcumgr-smp) #:use-module (crates-io))

(define-public crate-mcumgr-smp-0.6.0 (c (n "mcumgr-smp") (v "0.6.0") (d (list (d (n "base64") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "ciborium") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "crc") (r "^3.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "041bzsmlq1hjk64fihbls9qyfhzcybdn0cnfv4q4m8svaizhczgf") (f (quote (("serial" "base64" "crc") ("payload-cbor" "serde" "serde_bytes" "ciborium") ("default" "serial" "payload-cbor"))))))

