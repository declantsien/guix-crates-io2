(define-module (crates-io mc ua mcuat) #:use-module (crates-io))

(define-public crate-mcuat-0.1.0 (c (n "mcuat") (v "0.1.0") (d (list (d (n "serialport") (r "^4.2.0") (d #t) (k 0)))) (h "1lchlajfzjnnb0ndr04q166f47zbyf1sycqdx7xc28xk1dm826z6")))

(define-public crate-mcuat-0.1.1 (c (n "mcuat") (v "0.1.1") (d (list (d (n "serialport") (r "^4.2.0") (d #t) (k 0)))) (h "1l5r3sh2iyfs9l8cfp0049nximdhxyj3fijq1r498pcbpwgv0w2f")))

(define-public crate-mcuat-0.1.2 (c (n "mcuat") (v "0.1.2") (d (list (d (n "serial-rs") (r "^0.2.1") (d #t) (k 0)) (d (n "serialport") (r "^4.2.0") (d #t) (k 0)))) (h "1rz1y1dhb7yi10c30vcq5vmznx1lz70sv0giy51lh96h0sfp65bh")))

