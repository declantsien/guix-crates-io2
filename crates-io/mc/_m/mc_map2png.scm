(define-module (crates-io mc _m mc_map2png) #:use-module (crates-io))

(define-public crate-mc_map2png-0.1.0 (c (n "mc_map2png") (v "0.1.0") (d (list (d (n "fastnbt") (r "^2") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0gq8zcjawp7axwcpilzig0x0piz4q396vp2x2fjrz0jsqqb09m7z")))

