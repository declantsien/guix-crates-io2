(define-module (crates-io mc of mcoffin-option-ext) #:use-module (crates-io))

(define-public crate-mcoffin-option-ext-0.1.0 (c (n "mcoffin-option-ext") (v "0.1.0") (h "0j8b7xxvi61daq2m1r5rwnliwhffd5ca5szf3amckh4vmwa3cwcp") (f (quote (("result") ("default" "result"))))))

(define-public crate-mcoffin-option-ext-0.2.0 (c (n "mcoffin-option-ext") (v "0.2.0") (h "0c6gi7sbbp3ybacm05jay9wr4k7xcdiqa5kp4mszymr7392pa1jv") (f (quote (("result") ("default" "result"))))))

