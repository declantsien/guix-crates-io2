(define-module (crates-io mc at mcatool) #:use-module (crates-io))

(define-public crate-mcatool-0.2.0 (c (n "mcatool") (v "0.2.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("cargo" "std" "color"))) (k 0)) (d (n "fastnbt") (r "^2.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("rust_backend"))) (k 0)) (d (n "fs_extra") (r "^1.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18fsy9rj6zvqkvr96cagww5i98z8903zfyj43718yqjzbxrh8c60")))

(define-public crate-mcatool-0.2.1 (c (n "mcatool") (v "0.2.1") (d (list (d (n "clap") (r "^3.1") (f (quote ("cargo" "std" "color"))) (k 0)) (d (n "fastnbt") (r "^2.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("rust_backend"))) (k 0)) (d (n "fs_extra") (r "^1.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m9mplbjs5cyf684h13d4gmx640grwr0jk5bcdqwi8i00hbizpgs")))

