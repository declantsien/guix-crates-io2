(define-module (crates-io mc _b mc_bootstrap) #:use-module (crates-io))

(define-public crate-mc_bootstrap-0.1.1 (c (n "mc_bootstrap") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "07707rcmdhcvgrdzvf479xbf2s47x6gh9rh1y096np3i6z92yhbr")))

