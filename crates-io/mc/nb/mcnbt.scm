(define-module (crates-io mc nb mcnbt) #:use-module (crates-io))

(define-public crate-mcnbt-0.1.0 (c (n "mcnbt") (v "0.1.0") (d (list (d (n "residua-mutf8") (r "^2.0.0") (d #t) (k 0)))) (h "1psw16pdgj6cqxgy3ijk9vs5q2vxkj78mv8nb3mp7akl16krlqw8")))

(define-public crate-mcnbt-0.2.0 (c (n "mcnbt") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "residua-mutf8") (r "^2.0.0") (d #t) (k 0)))) (h "1vhzbr9f52z342a43bpwh33gpmnfpj6zfb8lba8ifrdm9f6pca2c")))

(define-public crate-mcnbt-1.0.0 (c (n "mcnbt") (v "1.0.0") (d (list (d (n "nom") (r "^7.1.3") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "residua-mutf8") (r "^2.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0bfsgjfigybs6plmgi9r35g37b16hk5qp6vicp7lq7qrqqznf79g") (f (quote (("read" "nom") ("default" "read"))))))

(define-public crate-mcnbt-1.1.0 (c (n "mcnbt") (v "1.1.0") (d (list (d (n "clap") (r "^4.4.7") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "residua-mutf8") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (f (quote ("preserve_order"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "07a3mjcjiyrdz36v4hj8yz4qwbf6b6zp8slkvxzj3aiyc6sjfz17") (f (quote (("read" "nom") ("default" "read") ("cli" "clap")))) (s 2) (e (quote (("serde" "dep:serde"))))))

