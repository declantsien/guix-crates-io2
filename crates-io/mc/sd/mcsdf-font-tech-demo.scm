(define-module (crates-io mc sd mcsdf-font-tech-demo) #:use-module (crates-io))

(define-public crate-mcsdf-font-tech-demo-1.0.0 (c (n "mcsdf-font-tech-demo") (v "1.0.0") (d (list (d (n "glium") (r "^0.22.0") (d #t) (k 0)) (d (n "mcsdf") (r "^0.1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1j61sbq23jq60y6rwfxgsql50angwbc8k5bry3rhg7lzg268byqy")))

(define-public crate-mcsdf-font-tech-demo-1.0.1 (c (n "mcsdf-font-tech-demo") (v "1.0.1") (d (list (d (n "glium") (r "^0.30.0") (d #t) (k 0)) (d (n "mcsdf") (r "^0.1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0nawphg3fmvx4zp0pd62yxn5j95y0564c9g9kr7sa7lndfwmz5jz")))

