(define-module (crates-io mc sd mcsdf) #:use-module (crates-io))

(define-public crate-mcsdf-0.1.0 (c (n "mcsdf") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)) (d (n "rusttype") (r "^0.7.3") (d #t) (k 0)))) (h "00f16i46kl5fjqrrqif549krdynbm43356cmai55gpwj6fq2kvm5")))

