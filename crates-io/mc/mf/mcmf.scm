(define-module (crates-io mc mf mcmf) #:use-module (crates-io))

(define-public crate-mcmf-1.0.0 (c (n "mcmf") (v "1.0.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0fkri1c48c81ma5fm5x8p96r9mhln24qy1lsddbi8fw0lyrhi434")))

(define-public crate-mcmf-1.1.0 (c (n "mcmf") (v "1.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1bnn4573qjfm9zxsj9gw7dwfl4img16i65a66h1k4azabb1484lc")))

(define-public crate-mcmf-2.0.0 (c (n "mcmf") (v "2.0.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "14wd6xn6lpinpc3va1klzd3z01qzq7hx0zad2h3dwhbs33jqkclc")))

