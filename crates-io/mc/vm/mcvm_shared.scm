(define-module (crates-io mc vm mcvm_shared) #:use-module (crates-io))

(define-public crate-mcvm_shared-0.0.2 (c (n "mcvm_shared") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "17g71q6m2d2gpwamk6xyza2100bj44f8yq1dy4iakqjs1i6fak5p")))

(define-public crate-mcvm_shared-0.0.3 (c (n "mcvm_shared") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c113bx22yyvfi1hanz1isf3snrq1ll9n5y4wd6nf3rnznm40j1k")))

(define-public crate-mcvm_shared-0.0.4 (c (n "mcvm_shared") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0h75f8d89b25nf5dcy58yjy222cjmpj44125zy1xyz0az65yc95v")))

(define-public crate-mcvm_shared-0.0.5 (c (n "mcvm_shared") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fnm50piaa00ks3dkignx025z044nwzrdb07qfh57a3zv7lydxz3")))

(define-public crate-mcvm_shared-0.0.6 (c (n "mcvm_shared") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ny4qq5sgzknczdf9qg2vsyykmy1xnkw2x0fap1kifwmind6j033")))

(define-public crate-mcvm_shared-0.1.0 (c (n "mcvm_shared") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "current_locale") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ym3m3csvb5l7f3if4rkl7bgag83q9wd25qll5rdy9rk65a48xbi")))

(define-public crate-mcvm_shared-0.1.1 (c (n "mcvm_shared") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "current_locale") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l7c2fr2qx42aavbkp12q9nfjkf5d4hwhivyjb75nqb4phmpzp5g")))

(define-public crate-mcvm_shared-0.2.0 (c (n "mcvm_shared") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "current_locale") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0g8kpyhi7fg6pyf0mpsj6k5lmbpajh24rqnabqblgm1j7dd9z54a")))

(define-public crate-mcvm_shared-0.3.0 (c (n "mcvm_shared") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "current_locale") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1801s1mvvaggr3948jx6i5m03kv0xd958dwhns784f8g7g6wjjcy")))

(define-public crate-mcvm_shared-0.4.0 (c (n "mcvm_shared") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "current_locale") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1577kgs97j2fv7n60gdddbx9pd9z8s4gz45djb6ss8adbwwd7rj2")))

(define-public crate-mcvm_shared-0.5.0 (c (n "mcvm_shared") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "current_locale") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "04g6wx5faz6s9p0axxgh5p4mf6cv5wlw70zhrr3qxkw79ddgw21f")))

(define-public crate-mcvm_shared-0.5.1 (c (n "mcvm_shared") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "current_locale") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "031fhzx6fvyxy3665191bj7krc0c39w5xs3w6h0xdwvlasqyz25q")))

(define-public crate-mcvm_shared-0.6.0 (c (n "mcvm_shared") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "current_locale") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "11va6a4bpkgyc0i90dykdxj63q62zgsbp54gayl2718prc9sciv9")))

(define-public crate-mcvm_shared-0.6.1 (c (n "mcvm_shared") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "current_locale") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "16h1s2bbch9k06hikh5vc4zir8r21gl3cp2pa3dacxbljry8byq2")))

(define-public crate-mcvm_shared-0.7.0 (c (n "mcvm_shared") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "current_locale") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1krkrs71fl9hnkrybw539wmwvwgfwysr6g9l8a196j98sbknrikd")))

(define-public crate-mcvm_shared-0.8.0 (c (n "mcvm_shared") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "current_locale") (r "^0.1.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "0k7sba45zb48ifpf870zk12y05s9s0f5df41hqligl0fmjn4mn0r")))

(define-public crate-mcvm_shared-0.9.0 (c (n "mcvm_shared") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "current_locale") (r "^0.1.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1c8grn05izdmg6n3z5az1blk7wixfwjfn2s8w49g9rhws19nz90l") (s 2) (e (quote (("schema" "dep:schemars"))))))

(define-public crate-mcvm_shared-0.10.0 (c (n "mcvm_shared") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "cfg-match") (r "^0.2.1") (d #t) (k 0)) (d (n "current_locale") (r "^0.1.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "02i9ml1dd7x42nckf1wh4xmydn8v8padhphsdfcxlyd94zsvdycn") (s 2) (e (quote (("schema" "dep:schemars"))))))

(define-public crate-mcvm_shared-0.10.1 (c (n "mcvm_shared") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "cfg-match") (r "^0.2.1") (d #t) (k 0)) (d (n "current_locale") (r "^0.1.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "11al0hilzcsdrghfn5skdyp7pg6kq8g9fv0lxw2xnjx8yglllihf") (s 2) (e (quote (("schema" "dep:schemars"))))))

(define-public crate-mcvm_shared-0.11.0 (c (n "mcvm_shared") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "cfg-match") (r "^0.2.1") (d #t) (k 0)) (d (n "current_locale") (r "^0.1.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "17lrqlm4yrxpshiv2j4qbhfhd2zcibndy7hj9r1ma22vrhghsks9") (s 2) (e (quote (("schema" "dep:schemars"))))))

(define-public crate-mcvm_shared-0.12.0 (c (n "mcvm_shared") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "cfg-match") (r "^0.2.1") (d #t) (k 0)) (d (n "current_locale") (r "^0.1.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "1bmdnz1137h684jk7y9gw1m0ddidrjpzayg0lx51l4mhg7flaaz5") (s 2) (e (quote (("schema" "dep:schemars"))))))

(define-public crate-mcvm_shared-0.13.0 (c (n "mcvm_shared") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "cfg-match") (r "^0.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.20") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "sys-locale") (r "^0.3.1") (d #t) (k 0)))) (h "0yk0q06l603l02aq9gbams7z41zhym00f7smn6sd00mn0rvvnc84") (s 2) (e (quote (("schema" "dep:schemars")))) (r "1.77.0")))

