(define-module (crates-io mc vm mcvm_parse) #:use-module (crates-io))

(define-public crate-mcvm_parse-0.0.4 (c (n "mcvm_parse") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "color-print") (r "^0.3.4") (d #t) (k 0)) (d (n "mcvm_shared") (r "^0.0.2") (d #t) (k 0)))) (h "1qvld7f07q8343qx9alggz0i2dz2pj6jl0ab3wqpy5wvaf0s42ff")))

(define-public crate-mcvm_parse-0.0.5 (c (n "mcvm_parse") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "color-print") (r "^0.3.4") (d #t) (k 0)) (d (n "mcvm_shared") (r "^0.0.3") (d #t) (k 0)))) (h "1cjqci6drl265gws72islmvgi7jc8b1lfj1ag0srl2bkvp4y1n27")))

(define-public crate-mcvm_parse-0.0.6 (c (n "mcvm_parse") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "color-print") (r "^0.3.4") (d #t) (k 0)) (d (n "mcvm_shared") (r "^0.0.3") (d #t) (k 0)))) (h "0fahcscs3vg7icspga5yc006i5yban21agyybgvy0zjwy85b6859")))

(define-public crate-mcvm_parse-0.0.7 (c (n "mcvm_parse") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "color-print") (r "^0.3.4") (d #t) (k 0)) (d (n "mcvm_shared") (r "^0.0.4") (d #t) (k 0)))) (h "1wkzr04pp6sbz7ppw0b2i9g1gqynvx6pwq8bgfaqi47l5jndd2ca")))

(define-public crate-mcvm_parse-0.0.8 (c (n "mcvm_parse") (v "0.0.8") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "color-print") (r "^0.3.4") (d #t) (k 0)) (d (n "mcvm_shared") (r "^0.0.5") (d #t) (k 0)))) (h "01vnykr77w9zx9w73q4zz97kh968kfigmqzqx67qkcysql8jpp7v")))

(define-public crate-mcvm_parse-0.0.9 (c (n "mcvm_parse") (v "0.0.9") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "color-print") (r "^0.3.4") (d #t) (k 0)) (d (n "mcvm_shared") (r "^0.0.6") (d #t) (k 0)))) (h "1k5smv6anjk71b4i9798f23qhwvyc0qdgl2k0kiis0h95dyqxlky")))

(define-public crate-mcvm_parse-0.1.0 (c (n "mcvm_parse") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "color-print") (r "^0.3.4") (d #t) (k 0)) (d (n "mcvm_shared") (r "^0.1.0") (d #t) (k 0)))) (h "1d4889icj1z9i12wz9qk6f2bkqhdvj83phpdvacmvglj218hhwdi")))

(define-public crate-mcvm_parse-0.1.1 (c (n "mcvm_parse") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "color-print") (r "^0.3.4") (d #t) (k 0)) (d (n "mcvm_shared") (r "^0.1.1") (d #t) (k 0)))) (h "14f576gfprsl260145652rpklfigvc9an78f12ama5k7jvvlgn2a")))

(define-public crate-mcvm_parse-0.2.0 (c (n "mcvm_parse") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "color-print") (r "^0.3.4") (d #t) (k 0)) (d (n "mcvm_shared") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cdi4vlpw5sny0apyqsivpj2dwbdzm35cqynipnnfa2br9703nm9")))

(define-public crate-mcvm_parse-0.3.0 (c (n "mcvm_parse") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "color-print") (r "^0.3.4") (d #t) (k 0)) (d (n "mcvm_shared") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "08zvf6j31nrwlnpma7wbhqxsy19wny1ys0vy1zh2pla178z9fkjf")))

(define-public crate-mcvm_parse-0.4.0 (c (n "mcvm_parse") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "color-print") (r "^0.3.4") (d #t) (k 0)) (d (n "mcvm_shared") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qaxbifjb9mznjafnqkclh0bqynn68wgbg10y79aqp50v3wa7kq9")))

(define-public crate-mcvm_parse-0.5.0 (c (n "mcvm_parse") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "mcvm_shared") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f3v2fk96kqg8z94gzlah642xsdvr9dp5zxxvf103gslqpx4xjzj")))

(define-public crate-mcvm_parse-0.5.1 (c (n "mcvm_parse") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "mcvm_shared") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sp60zmx68kd233xp77jxqk8c9nh8yz297667lhfjfvkalfjzz1y")))

(define-public crate-mcvm_parse-0.6.0 (c (n "mcvm_parse") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "mcvm_shared") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "17zg29w87kzbn15m50qv1hdhzig94hqd7a2zki9q46rwx71gmh6d")))

(define-public crate-mcvm_parse-0.7.0 (c (n "mcvm_parse") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "mcvm_shared") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dygl193i7lpbdkgy7f72khajsla9syx9586y5dnblnqz3bvwg82")))

(define-public crate-mcvm_parse-0.8.0 (c (n "mcvm_parse") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "mcvm_shared") (r "^0.8.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zi7f8xx9pn05sf5q31rr37bpy18giwp89bmqyz79sgvby7n8ya6")))

(define-public crate-mcvm_parse-0.9.0 (c (n "mcvm_parse") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "mcvm_shared") (r "^0.9.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w42phva8vayiv4h9qgfxplc1rxg0f230ar93fdsxx271wlyndzf") (s 2) (e (quote (("schema" "dep:schemars" "mcvm_shared/schema"))))))

(define-public crate-mcvm_parse-0.10.0 (c (n "mcvm_parse") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "mcvm_shared") (r "^0.10.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xz6zd2jgfx0q2ba0zklkpvbyc5q7qmyjp0z6dgjgd7pd1pr3ffr") (s 2) (e (quote (("schema" "dep:schemars" "mcvm_shared/schema"))))))

(define-public crate-mcvm_parse-0.11.0 (c (n "mcvm_parse") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "mcvm_shared") (r "^0.10.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vzaqnlp4fc7x1z4hz1z4y81rxm5n46wa9d8xi52ikclg3xamrly") (s 2) (e (quote (("schema" "dep:schemars" "mcvm_shared/schema"))))))

(define-public crate-mcvm_parse-0.12.0 (c (n "mcvm_parse") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "mcvm_shared") (r "^0.11.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "15ygdsa9y57nwsrmkwqby0cz1fg9w6a5n41y0jjx7cijx02phfhx") (s 2) (e (quote (("schema" "dep:schemars" "mcvm_shared/schema"))))))

(define-public crate-mcvm_parse-0.13.0 (c (n "mcvm_parse") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "mcvm_shared") (r "^0.12.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pbi4n2ngp6agslmh6nv8jk3rnhrsmn3kdh3nzcq2d7lg30ribgr") (s 2) (e (quote (("schema" "dep:schemars" "mcvm_shared/schema"))))))

(define-public crate-mcvm_parse-0.14.0 (c (n "mcvm_parse") (v "0.14.0") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "mcvm_shared") (r "^0.13.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.20") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "01x5zrv0sswsiycc91nzgcb41szll1ls8p4b19j5i88s0y8dinp4") (s 2) (e (quote (("schema" "dep:schemars" "mcvm_shared/schema")))) (r "1.77.0")))

