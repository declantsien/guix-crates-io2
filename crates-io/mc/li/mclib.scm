(define-module (crates-io mc li mclib) #:use-module (crates-io))

(define-public crate-mclib-0.0.1 (c (n "mclib") (v "0.0.1") (d (list (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "mclib-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (f (quote ("v4" "fast-rng" "macro-diagnostics"))) (d #t) (k 0)))) (h "0a954k3fkdqsl5584idmdixd89ny5c820mwb0mr59n60qrdw2y84")))

