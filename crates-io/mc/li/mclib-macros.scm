(define-module (crates-io mc li mclib-macros) #:use-module (crates-io))

(define-public crate-mclib-macros-0.0.1 (c (n "mclib-macros") (v "0.0.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1bp1341h6nb5b4rrxc8grlskqwakaza3ahmn2rb5whq6yvgxw7js")))

