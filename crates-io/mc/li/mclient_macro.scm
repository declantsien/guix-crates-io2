(define-module (crates-io mc li mclient_macro) #:use-module (crates-io))

(define-public crate-mclient_macro-0.1.2 (c (n "mclient_macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mkgigkhshm02i8rggggx8k7rlz6g8a33b3bmpz5mx0g0v5p989y")))

(define-public crate-mclient_macro-0.1.3 (c (n "mclient_macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lh1cf9qwv88bdad9lpkhs6arvq32rw5glmqmba3jrbgr633yfsh")))

(define-public crate-mclient_macro-0.1.4 (c (n "mclient_macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dyv2jw8n8p1czm7z4g9ric9qaq64cr4jmxybf8dzz7n2kfq5azp")))

