(define-module (crates-io mc pe mcpe_query) #:use-module (crates-io))

(define-public crate-mcpe_query-0.1.0 (c (n "mcpe_query") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1dlvpgrqmrvwq8pcjldihb17jk9s822d3y9mfg16pl0w1dq97xz5")))

(define-public crate-mcpe_query-0.1.1 (c (n "mcpe_query") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "05lc3aqpakxrgw0dx9db6zqrmfxf7cav7q3zw6dsyl8mq7ngqg3h")))

(define-public crate-mcpe_query-0.1.2 (c (n "mcpe_query") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0dd4qqlvfz17gdxhvfygz8yqwdiq1sclaabsgr8l4g2zd8a4mzcq")))

