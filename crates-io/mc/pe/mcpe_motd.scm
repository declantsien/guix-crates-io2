(define-module (crates-io mc pe mcpe_motd) #:use-module (crates-io))

(define-public crate-mcpe_motd-0.1.0 (c (n "mcpe_motd") (v "0.1.0") (h "0h3aaqapb1010wdmji859zcgziyrxzsz1vn5m61im72207kjx5cj")))

(define-public crate-mcpe_motd-1.0.0 (c (n "mcpe_motd") (v "1.0.0") (h "0n4jirhai4gma5dfsx7jmkl7495spig2hvd0gcp45w0rlpdlp549")))

