(define-module (crates-io mc -l mc-launchermeta) #:use-module (crates-io))

(define-public crate-mc-launchermeta-0.1.0 (c (n "mc-launchermeta") (v "0.1.0") (d (list (d (n "http-client") (r "^6.5") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1l9za3f92pffa0pqi7xyb7i6m2xv61crrm7bz7jndshnq9qqkvsv") (r "1.56.0")))

(define-public crate-mc-launchermeta-0.1.1 (c (n "mc-launchermeta") (v "0.1.1") (d (list (d (n "http-client") (r "^6.5") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12g3hd3yzifap3m392bv6n5yrcj2r68bckapaj85x5xica8n23as") (r "1.56.0")))

