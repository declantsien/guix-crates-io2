(define-module (crates-io mc sc mcschem) #:use-module (crates-io))

(define-public crate-mcschem-0.1.0 (c (n "mcschem") (v "0.1.0") (d (list (d (n "quartz_nbt") (r "^0.2.8") (d #t) (k 0)))) (h "10f71n6rvh0zkxmqav6karmm12cyn6nw5vmzpp6ll8h6yns58dzw") (r "1.71.0")))

(define-public crate-mcschem-0.1.1 (c (n "mcschem") (v "0.1.1") (d (list (d (n "quartz_nbt") (r "^0.2.8") (d #t) (k 0)))) (h "1ryv86acm3f3xxxafxbpw61i02is4bvgyfzx2fw9619s8jsgr8s0") (r "1.71.0")))

