(define-module (crates-io mc _d mc_downloader) #:use-module (crates-io))

(define-public crate-mc_downloader-0.1.0 (c (n "mc_downloader") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "chksum") (r "^0.1.0-rc5") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "pbr") (r "^1.0.4") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("rt-multi-thread" "time" "rt"))) (d #t) (k 0)))) (h "0v1r2jql7lay8w29bkzxrdmwp8bpy86ycaipxin2da6l9n5rk9n3")))

