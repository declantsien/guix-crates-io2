(define-module (crates-io mc -s mc-sgx-dcap-quoteverify-types) #:use-module (crates-io))

(define-public crate-mc-sgx-dcap-quoteverify-types-0.2.0 (c (n "mc-sgx-dcap-quoteverify-types") (v "0.2.0") (d (list (d (n "mc-sgx-core-types") (r "=0.2.0") (d #t) (k 0)) (d (n "mc-sgx-dcap-quoteverify-sys-types") (r "=0.2.0") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "07z014d7zs1q7p3m7d74d4six7q255sd4i7pmf41lh6n40dq69y0") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-quoteverify-types-0.2.1 (c (n "mc-sgx-dcap-quoteverify-types") (v "0.2.1") (d (list (d (n "mc-sgx-core-types") (r "=0.2.1") (d #t) (k 0)) (d (n "mc-sgx-dcap-quoteverify-sys-types") (r "=0.2.1") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "05kdi4ddkzfxbrhamwii9z5lx3kd4wppw8nvszhg3356bmmm4maa") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-quoteverify-types-0.3.0 (c (n "mc-sgx-dcap-quoteverify-types") (v "0.3.0") (d (list (d (n "mc-sgx-core-types") (r "=0.3.0") (d #t) (k 0)) (d (n "mc-sgx-dcap-quoteverify-sys-types") (r "=0.3.0") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "0gn06q07h6jqr3n3yidzqwzzamihshq4sdsvjqbi0rkkh3xwxxqm") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-quoteverify-types-0.4.0 (c (n "mc-sgx-dcap-quoteverify-types") (v "0.4.0") (d (list (d (n "mc-sgx-core-types") (r "=0.4.0") (d #t) (k 0)) (d (n "mc-sgx-dcap-quoteverify-sys-types") (r "=0.4.0") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "0jhbx26q3ha09m2sch0jmhzw8vvwlwcrqrkag7r5vk91jrw2g3pw") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-quoteverify-types-0.4.1 (c (n "mc-sgx-dcap-quoteverify-types") (v "0.4.1") (d (list (d (n "mc-sgx-core-types") (r "=0.4.1") (d #t) (k 0)) (d (n "mc-sgx-dcap-quoteverify-sys-types") (r "=0.4.1") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "1dycx9marsvpfd8f3fny52wds798d1y7mc8s220hi8r42c5b1jyz") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-quoteverify-types-0.4.2 (c (n "mc-sgx-dcap-quoteverify-types") (v "0.4.2") (d (list (d (n "mc-sgx-core-types") (r "=0.4.2") (d #t) (k 0)) (d (n "mc-sgx-dcap-quoteverify-sys-types") (r "=0.4.2") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "11bich0pl8rhqcrzzhswrsh11ml3zyfdh00cpsiips9dx58w49bp") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-quoteverify-types-0.5.0 (c (n "mc-sgx-dcap-quoteverify-types") (v "0.5.0") (d (list (d (n "mc-sgx-core-types") (r "=0.5.0") (d #t) (k 0)) (d (n "mc-sgx-dcap-quoteverify-sys-types") (r "=0.5.0") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "1ba2fj1m8c3lvjc5kqj8nd28f8742z4nwhvnmrkdpazj0ymnbyw3") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-quoteverify-types-0.6.0 (c (n "mc-sgx-dcap-quoteverify-types") (v "0.6.0") (d (list (d (n "mc-sgx-core-types") (r "=0.6.0") (d #t) (k 0)) (d (n "mc-sgx-dcap-quoteverify-sys-types") (r "=0.6.0") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "0shhi7jvk5r7yflfqcsbcz6mqs3zm5b1lv3029ai8n8w3mjpygg3") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-quoteverify-types-0.6.1 (c (n "mc-sgx-dcap-quoteverify-types") (v "0.6.1") (d (list (d (n "mc-sgx-core-types") (r "=0.6.1") (d #t) (k 0)) (d (n "mc-sgx-dcap-quoteverify-sys-types") (r "=0.6.1") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "12qv7viwf3akpg05w49fi6sy5d7nk0gfp5f1bb4m00l3bklsxkgg") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-quoteverify-types-0.7.0 (c (n "mc-sgx-dcap-quoteverify-types") (v "0.7.0") (d (list (d (n "mc-sgx-core-types") (r "=0.7.0") (d #t) (k 0)) (d (n "mc-sgx-dcap-quoteverify-sys-types") (r "=0.7.0") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "0zgsh8hvifvyp51y9dh7ach515j7d1m7j21wlnc4nzf2b5dfdrw7") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-quoteverify-types-0.7.1 (c (n "mc-sgx-dcap-quoteverify-types") (v "0.7.1") (d (list (d (n "mc-sgx-core-types") (r "=0.7.1") (d #t) (k 0)) (d (n "mc-sgx-dcap-quoteverify-sys-types") (r "=0.7.1") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "1n9cs1zsw973q6djhshpxch9f6ymm7qn9idfjbsj74pa2z6a9i4y") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-quoteverify-types-0.7.2 (c (n "mc-sgx-dcap-quoteverify-types") (v "0.7.2") (d (list (d (n "mc-sgx-core-types") (r "=0.7.2") (d #t) (k 0)) (d (n "mc-sgx-dcap-quoteverify-sys-types") (r "=0.7.2") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "17ljsxvi74n6qxnbxhgqn3q7q37wnpy4f2sf3wzvv7kzzc2rf5f3") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-quoteverify-types-0.7.3 (c (n "mc-sgx-dcap-quoteverify-types") (v "0.7.3") (d (list (d (n "mc-sgx-core-types") (r "=0.7.3") (d #t) (k 0)) (d (n "mc-sgx-dcap-quoteverify-sys-types") (r "=0.7.3") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "0xz5cjvfn3khnbzgnblqk7a2ncln6cdh8dccp63q55762i0q1513") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-quoteverify-types-0.7.4 (c (n "mc-sgx-dcap-quoteverify-types") (v "0.7.4") (d (list (d (n "mc-sgx-core-types") (r "=0.7.4") (d #t) (k 0)) (d (n "mc-sgx-dcap-quoteverify-sys-types") (r "=0.7.4") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "1sbdxymr25r4hqibfhxqbsp74dnj5nasxhyzkg2a93n3xkiqd5rw") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-quoteverify-types-0.7.5 (c (n "mc-sgx-dcap-quoteverify-types") (v "0.7.5") (d (list (d (n "mc-sgx-core-types") (r "=0.7.5") (d #t) (k 0)) (d (n "mc-sgx-dcap-quoteverify-sys-types") (r "=0.7.5") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "1kyi3pdnla29a40jhdwsc0bgpjc9j3vp238ggjs0hj9f57365z48") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-quoteverify-types-0.8.0 (c (n "mc-sgx-dcap-quoteverify-types") (v "0.8.0") (d (list (d (n "mc-sgx-core-types") (r "=0.8.0") (d #t) (k 0)) (d (n "mc-sgx-dcap-quoteverify-sys-types") (r "=0.8.0") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "0sn1ppn99xcd5mvvr6xp9c5jp5n5vx7x8z70752110wn40fl5w20") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-quoteverify-types-0.9.0 (c (n "mc-sgx-dcap-quoteverify-types") (v "0.9.0") (d (list (d (n "mc-sgx-core-types") (r "=0.9.0") (d #t) (k 0)) (d (n "mc-sgx-dcap-quoteverify-sys-types") (r "=0.9.0") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "12wd8zdnmn5xsjncj5pdjypq7saa93606p0z7sqbq4pkb6h9p582") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-quoteverify-types-0.10.0 (c (n "mc-sgx-dcap-quoteverify-types") (v "0.10.0") (d (list (d (n "mc-sgx-core-types") (r "=0.10.0") (d #t) (k 0)) (d (n "mc-sgx-dcap-quoteverify-sys-types") (r "=0.10.0") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "0cqj9ch2mpmsq1h20n86sycd64dwlq4g8rqsp921bfaskmc11w38") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-quoteverify-types-0.10.1 (c (n "mc-sgx-dcap-quoteverify-types") (v "0.10.1") (d (list (d (n "mc-sgx-core-types") (r "=0.10.1") (d #t) (k 0)) (d (n "mc-sgx-dcap-quoteverify-sys-types") (r "=0.10.1") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "084pa63mqnlmiy2116vkly5gh2k33pfjny76bkvskb18rs9ma9sf") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-quoteverify-types-0.11.0 (c (n "mc-sgx-dcap-quoteverify-types") (v "0.11.0") (d (list (d (n "mc-sgx-core-types") (r "=0.11.0") (d #t) (k 0)) (d (n "mc-sgx-dcap-quoteverify-sys-types") (r "=0.11.0") (d #t) (k 0)) (d (n "yare") (r "^2") (d #t) (k 2)))) (h "0qnp0gqdk8r5n8vnlsv9y2fmirqiw8fx96h494wcr138lvaqw82j") (r "1.62.1")))

