(define-module (crates-io mc -s mc-sgx-core-sys-types) #:use-module (crates-io))

(define-public crate-mc-sgx-core-sys-types-0.1.0 (c (n "mc-sgx-core-sys-types") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.1.0") (d #t) (k 1)))) (h "193vmbxfcj7y6bw30kmai442gmkz9694ppfsvak1ibhd7gcq411r") (r "1.62.1")))

(define-public crate-mc-sgx-core-sys-types-0.2.0 (c (n "mc-sgx-core-sys-types") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.2.0") (d #t) (k 1)))) (h "1dv9jb0sqqwq5ydmqnhs9pq9a139dkw3z8f2mfhxwzgbka9f67rj") (r "1.62.1")))

(define-public crate-mc-sgx-core-sys-types-0.2.1 (c (n "mc-sgx-core-sys-types") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.2.1") (d #t) (k 1)))) (h "0vk99ynrhmzsmfawf31lgbflggi9qs2r10p4pqdznvfmk0h458dq") (r "1.62.1")))

(define-public crate-mc-sgx-core-sys-types-0.3.0 (c (n "mc-sgx-core-sys-types") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.3.0") (d #t) (k 1)))) (h "04hskra5nl82id6bnidf9aiv0zcisww99mc8lk1i28vs1ssbv6ha") (r "1.62.1")))

(define-public crate-mc-sgx-core-sys-types-0.4.0 (c (n "mc-sgx-core-sys-types") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.4.0") (d #t) (k 1)))) (h "0jy6iazdkaiy6bvhvg2xh411kafyzmhqjv5hdpnnz5wx61g0m6k0") (r "1.62.1")))

(define-public crate-mc-sgx-core-sys-types-0.4.1 (c (n "mc-sgx-core-sys-types") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.4.1") (d #t) (k 1)))) (h "1xngnmfxvya656gk62bc1lcfh91h8d3sasmk8q2cmxnpg0k3l2af") (r "1.62.1")))

(define-public crate-mc-sgx-core-sys-types-0.4.2 (c (n "mc-sgx-core-sys-types") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.4.2") (d #t) (k 1)))) (h "0c3h11mj4200r7nxfgk7mr6730k649r2kr7whdprq6d09fsgg5y9") (r "1.62.1")))

(define-public crate-mc-sgx-core-sys-types-0.5.0 (c (n "mc-sgx-core-sys-types") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.5.0") (d #t) (k 1)))) (h "16r3l9kmq80dgdr5vn3g8j0ylnzwwq9hajhb8qhm1ilnjw1pjrrk") (r "1.62.1")))

(define-public crate-mc-sgx-core-sys-types-0.6.0 (c (n "mc-sgx-core-sys-types") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.6.0") (d #t) (k 1)))) (h "1nfn48n6xf3ac904nknkj3v5r18dckkdhz0cafxnkvvwlgzfky2q") (r "1.62.1")))

(define-public crate-mc-sgx-core-sys-types-0.6.1 (c (n "mc-sgx-core-sys-types") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.6.1") (d #t) (k 1)))) (h "122z6z9ck011cly0fclwzralkwyd57pzhmzvanhmw0627gcnzxqb") (r "1.62.1")))

(define-public crate-mc-sgx-core-sys-types-0.7.0 (c (n "mc-sgx-core-sys-types") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (k 0)))) (h "150bggigrh9na0sqqan4wcd7ach11gm3iccxq1dxd5y4181wwxx6") (r "1.62.1")))

(define-public crate-mc-sgx-core-sys-types-0.7.1 (c (n "mc-sgx-core-sys-types") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.1") (d #t) (k 1)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (k 0)))) (h "14f8ixkvz3fprwr1q6jbvg1s7dbkicar3rhs40ywzj8bmjcsvjds") (r "1.62.1")))

(define-public crate-mc-sgx-core-sys-types-0.7.2 (c (n "mc-sgx-core-sys-types") (v "0.7.2") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.2") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_with") (r "^3.1") (f (quote ("macros"))) (k 0)))) (h "1x9sjqcglsl17q8539fm1r9ravz2ghnk14hypqg0jkgvdmnhzviz") (r "1.62.1")))

(define-public crate-mc-sgx-core-sys-types-0.7.3 (c (n "mc-sgx-core-sys-types") (v "0.7.3") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.3") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_with") (r "^3.2") (f (quote ("macros"))) (k 0)))) (h "0fpq8r7ymwrkh6fglkk7vhzx5n2zsplgyl2spk31ll9cwnxl3zhc") (r "1.62.1")))

(define-public crate-mc-sgx-core-sys-types-0.7.4 (c (n "mc-sgx-core-sys-types") (v "0.7.4") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.4") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_with") (r "^3.2") (f (quote ("macros"))) (k 0)))) (h "1ahkhn09aa92y6xmw62jzv5h26cd827n1v0bsab6nk3mxh5n80w5") (r "1.62.1")))

(define-public crate-mc-sgx-core-sys-types-0.7.5 (c (n "mc-sgx-core-sys-types") (v "0.7.5") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.5") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_with") (r "^3.2") (f (quote ("macros"))) (k 0)))) (h "18w6yynh9kp6c9vdbcvdq7sb0ydnzyl4p9329rj0q3fg1vqp9sb1") (r "1.62.1")))

(define-public crate-mc-sgx-core-sys-types-0.8.0 (c (n "mc-sgx-core-sys-types") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.8.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_with") (r "^3.2") (f (quote ("macros"))) (k 0)))) (h "0yxy7xyq6cdjy3s21zqji6wracndx8q1s9ag5gj09s91s1m4ccjh") (r "1.62.1")))

(define-public crate-mc-sgx-core-sys-types-0.9.0 (c (n "mc-sgx-core-sys-types") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.9.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_with") (r "^3.2") (f (quote ("macros"))) (k 0)))) (h "1w3cnj0vmqsng5kp1lcgn0r1xbpf33mfzqjz5lvzrx15sdy77qy1") (r "1.62.1")))

(define-public crate-mc-sgx-core-sys-types-0.10.0 (c (n "mc-sgx-core-sys-types") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.10.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_with") (r "^3.2") (f (quote ("macros"))) (k 0)))) (h "0g7cwxajdmqp7nlgy3l1f4px3rcczkkj7ib6fnapyb7cl9cssr7f") (r "1.62.1")))

(define-public crate-mc-sgx-core-sys-types-0.10.1 (c (n "mc-sgx-core-sys-types") (v "0.10.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.10.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_with") (r "^3.2") (f (quote ("macros"))) (k 0)))) (h "0v2xy8w59y6bjdwxlwjq11acq174y4wyg5438qglwcxxjcxmamjv") (r "1.62.1")))

(define-public crate-mc-sgx-core-sys-types-0.11.0 (c (n "mc-sgx-core-sys-types") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.11.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_with") (r "^3.2") (f (quote ("macros"))) (k 0)))) (h "10gmrw137wi2855p6qzrg7zh8h89syyki9gs7vk5aa0g7lr36p0w") (r "1.62.1")))

