(define-module (crates-io mc -s mc-sgx-tcrypto-sys-types) #:use-module (crates-io))

(define-public crate-mc-sgx-tcrypto-sys-types-0.1.0 (c (n "mc-sgx-tcrypto-sys-types") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.1.0") (d #t) (k 1)))) (h "02c095c9p62pnk8zmamh59gbycsjzzfwx6flfpl4zhnq9a2k70am") (r "1.62.1")))

(define-public crate-mc-sgx-tcrypto-sys-types-0.2.0 (c (n "mc-sgx-tcrypto-sys-types") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.2.0") (d #t) (k 1)))) (h "1ba8l9if5ylzjnnbp5mm6ll4yd3lzvrybh9a1amqncyn66a25rqa") (r "1.62.1")))

(define-public crate-mc-sgx-tcrypto-sys-types-0.2.1 (c (n "mc-sgx-tcrypto-sys-types") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.2.1") (d #t) (k 1)))) (h "1wzbkid1cgmx502f4csgfdxmhkdbyw5nnwva7m3xkiq36rayy8jx") (r "1.62.1")))

(define-public crate-mc-sgx-tcrypto-sys-types-0.3.0 (c (n "mc-sgx-tcrypto-sys-types") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.3.0") (d #t) (k 1)))) (h "1ixih77x41yyah8g9vgl6l6h7rj96dd3ynv4sxypr70h0hnb0hp5") (r "1.62.1")))

(define-public crate-mc-sgx-tcrypto-sys-types-0.4.0 (c (n "mc-sgx-tcrypto-sys-types") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.4.0") (d #t) (k 1)))) (h "0bzaj10wpylam6ppccgzf26glcmvpx4iad8va2zbi15vfp395cd2") (r "1.62.1")))

(define-public crate-mc-sgx-tcrypto-sys-types-0.4.1 (c (n "mc-sgx-tcrypto-sys-types") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.4.1") (d #t) (k 1)))) (h "1g08safx9hvfs3sl9kpl5dfid112i292vplwa5i4anyyhwmzannz") (r "1.62.1")))

(define-public crate-mc-sgx-tcrypto-sys-types-0.4.2 (c (n "mc-sgx-tcrypto-sys-types") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.4.2") (d #t) (k 1)))) (h "0rxql2365bkyqp1z4013592awz7z3wvv1wyc2qxnb2f01339isgl") (r "1.62.1")))

(define-public crate-mc-sgx-tcrypto-sys-types-0.5.0 (c (n "mc-sgx-tcrypto-sys-types") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.5.0") (d #t) (k 1)))) (h "1c7si6ym5l6mc5h0xfb7vxj0j98l68nqzrl9diqrfrbrz8ija89i") (r "1.62.1")))

(define-public crate-mc-sgx-tcrypto-sys-types-0.6.0 (c (n "mc-sgx-tcrypto-sys-types") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.6.0") (d #t) (k 1)))) (h "1p26zs36rjpjrgc0kxg4yl9qmmqca93nrlis4dk3gkjjhcvrmq16") (r "1.62.1")))

(define-public crate-mc-sgx-tcrypto-sys-types-0.6.1 (c (n "mc-sgx-tcrypto-sys-types") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.6.1") (d #t) (k 1)))) (h "0jmzh6z7lnzjwzqmh0f41i8v74pfg1qcdimf1s80pcvhsx8bgmaf") (r "1.62.1")))

(define-public crate-mc-sgx-tcrypto-sys-types-0.7.0 (c (n "mc-sgx-tcrypto-sys-types") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.0") (d #t) (k 1)))) (h "1rb407c1rp93r1vbkmn34zg1ic4vzrrva97nw8w9bbs41y2xf2vb") (r "1.62.1")))

(define-public crate-mc-sgx-tcrypto-sys-types-0.7.1 (c (n "mc-sgx-tcrypto-sys-types") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.1") (d #t) (k 1)))) (h "0lc9fbsr6rmkz0yg5jmrhd2p33fhkgf25hnaiyzjw8nx32klnysb") (r "1.62.1")))

(define-public crate-mc-sgx-tcrypto-sys-types-0.7.2 (c (n "mc-sgx-tcrypto-sys-types") (v "0.7.2") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.2") (d #t) (k 1)))) (h "0d2q16dc7l0wm4jr50vyw6dri0czz6x2pnyrnndmsmymxp0iy3yr") (r "1.62.1")))

(define-public crate-mc-sgx-tcrypto-sys-types-0.7.3 (c (n "mc-sgx-tcrypto-sys-types") (v "0.7.3") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.3") (d #t) (k 1)))) (h "03dwgzhx49kzmn7l1jiz9729cqai8pnp38gl84p4mjqm24n5k9xc") (r "1.62.1")))

(define-public crate-mc-sgx-tcrypto-sys-types-0.7.4 (c (n "mc-sgx-tcrypto-sys-types") (v "0.7.4") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.4") (d #t) (k 1)))) (h "09rh5kcmlwfzx7mjjk27l008x44jfmvi8fq6193kbdddkvcnam5x") (r "1.62.1")))

(define-public crate-mc-sgx-tcrypto-sys-types-0.7.5 (c (n "mc-sgx-tcrypto-sys-types") (v "0.7.5") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.5") (d #t) (k 1)))) (h "1vppdcxc3ry6qlj1qysqkzg83wf4d6c3hvmmzr69ci637ynrmwqp") (r "1.62.1")))

(define-public crate-mc-sgx-tcrypto-sys-types-0.8.0 (c (n "mc-sgx-tcrypto-sys-types") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.8.0") (d #t) (k 1)))) (h "0fbclr6f3d09zygkzwq095xnh6051mbgbg599j7v3cy0w2xh56bn") (r "1.62.1")))

(define-public crate-mc-sgx-tcrypto-sys-types-0.9.0 (c (n "mc-sgx-tcrypto-sys-types") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.9.0") (d #t) (k 1)))) (h "0lda1fj9yz6hlxb8a00b2sbhzd8g9x9wcyn7id4zanmn3xwjjs9b") (r "1.62.1")))

(define-public crate-mc-sgx-tcrypto-sys-types-0.10.0 (c (n "mc-sgx-tcrypto-sys-types") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.10.0") (d #t) (k 1)))) (h "0vrj8nx1syl2n8afazzq4m092chj4i55aglhb1fyzzczrljk346d") (r "1.62.1")))

(define-public crate-mc-sgx-tcrypto-sys-types-0.10.1 (c (n "mc-sgx-tcrypto-sys-types") (v "0.10.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.10.1") (d #t) (k 1)))) (h "1pivdbh646cikkzz1zfjrqljk43ydqq3rf905cn679ibsirfncmk") (r "1.62.1")))

(define-public crate-mc-sgx-tcrypto-sys-types-0.11.0 (c (n "mc-sgx-tcrypto-sys-types") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.11.0") (d #t) (k 1)))) (h "1n9qri6jq3qy1rl6sl6k96amzdfra5lbih330p3am15pm729ql1w") (r "1.62.1")))

