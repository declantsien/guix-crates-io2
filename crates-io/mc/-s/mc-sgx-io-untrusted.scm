(define-module (crates-io mc -s mc-sgx-io-untrusted) #:use-module (crates-io))

(define-public crate-mc-sgx-io-untrusted-0.1.0 (c (n "mc-sgx-io-untrusted") (v "0.1.0") (d (list (d (n "mc-sgx-urts") (r "^0.4.0") (d #t) (k 0)) (d (n "mc-sgx-util") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "serial_test") (r "^1.0.0") (d #t) (k 2)))) (h "0hk2g0w50vs3vim90h1p31ak3wrhlvnv1dav39md8zw2drw0ci82") (f (quote (("sim" "mc-sgx-urts/sim") ("default")))) (r "1.62.1")))

