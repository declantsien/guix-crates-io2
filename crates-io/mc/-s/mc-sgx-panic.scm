(define-module (crates-io mc -s mc-sgx-panic) #:use-module (crates-io))

(define-public crate-mc-sgx-panic-0.1.0 (c (n "mc-sgx-panic") (v "0.1.0") (d (list (d (n "mc-sgx-io") (r "=0.1.0") (o #t) (d #t) (k 0)) (d (n "mc-sgx-panic-sys") (r "=0.1.0") (d #t) (k 0)) (d (n "mc-sgx-sync") (r "=0.1.0") (o #t) (d #t) (k 0)))) (h "0sy896pk9w3sbn9i4dc2czk1h22k7ndqdgwm1fmpjjxvbc7m5hbi") (s 2) (e (quote (("log" "dep:mc-sgx-io" "dep:mc-sgx-sync")))) (r "1.62.1")))

