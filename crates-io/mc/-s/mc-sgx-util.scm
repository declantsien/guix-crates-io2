(define-module (crates-io mc -s mc-sgx-util) #:use-module (crates-io))

(define-public crate-mc-sgx-util-0.2.0 (c (n "mc-sgx-util") (v "0.2.0") (h "02l92vd4idmypx9srbl702my3l3cacdw4fbcda5vigc031ksc4s3") (r "1.62.1")))

(define-public crate-mc-sgx-util-0.2.1 (c (n "mc-sgx-util") (v "0.2.1") (h "1a7i5ww9w6kax27yyl3qnx3fbiq0df9gpjx4yrlnfprrfz9fdl36") (r "1.62.1")))

(define-public crate-mc-sgx-util-0.3.0 (c (n "mc-sgx-util") (v "0.3.0") (h "0zijaqnlyzvh4kfhfg20n38vgq181y5pm1np4j13krkby55cxjwr") (r "1.62.1")))

(define-public crate-mc-sgx-util-0.4.0 (c (n "mc-sgx-util") (v "0.4.0") (h "05zhdlph3vanj4hrpybhm63hlfn5qkjgcw4vyhf9ak4rsgfypd3c") (r "1.62.1")))

(define-public crate-mc-sgx-util-0.4.1 (c (n "mc-sgx-util") (v "0.4.1") (h "1cz0f14jw4hxg26cvfz2s05bnb1chlwzm5nyq067iy09hs488sbj") (r "1.62.1")))

(define-public crate-mc-sgx-util-0.4.2 (c (n "mc-sgx-util") (v "0.4.2") (h "1gwjygznmd12wcnqja3nf5clf8vxsx7v5ssyv12gx9r9bppv9qzp") (r "1.62.1")))

(define-public crate-mc-sgx-util-0.5.0 (c (n "mc-sgx-util") (v "0.5.0") (h "023x7qpj3w6xvn31x4cxz5458zj2c3dzbd802hj9ffw2ry5b382m") (r "1.62.1")))

(define-public crate-mc-sgx-util-0.6.0 (c (n "mc-sgx-util") (v "0.6.0") (h "124c8rx01v3yagm0ijjalq8mzhn3lxhsh6zl1jnz5qwsq1ia0y39") (r "1.62.1")))

(define-public crate-mc-sgx-util-0.6.1 (c (n "mc-sgx-util") (v "0.6.1") (h "1jbyhza4jgvdqkq3jw8vfhf5slzjhc0sxc16avn2jgx7s9k2p2nj") (r "1.62.1")))

(define-public crate-mc-sgx-util-0.7.0 (c (n "mc-sgx-util") (v "0.7.0") (h "0m942gyj9zmblbk82w4hr53q7pnhmm5nxm17pfyzcg93zambdqjb") (r "1.62.1")))

(define-public crate-mc-sgx-util-0.7.1 (c (n "mc-sgx-util") (v "0.7.1") (h "19fdc76549l5g4xcrihrisaa631fwbwf4hmbqmrilv57m436ysvk") (r "1.62.1")))

(define-public crate-mc-sgx-util-0.7.2 (c (n "mc-sgx-util") (v "0.7.2") (h "1689vqdfl1p4wpbqm55ym97ilj30lkb6agr3sccngjqk7bzir267") (r "1.62.1")))

(define-public crate-mc-sgx-util-0.7.3 (c (n "mc-sgx-util") (v "0.7.3") (h "1f1hd48lwbzs30mazccfkw4rkcxaccivs4bny0ijy3d0g03n8f7y") (r "1.62.1")))

(define-public crate-mc-sgx-util-0.7.4 (c (n "mc-sgx-util") (v "0.7.4") (h "1z9naqbcvah2p4yghsgh9cp5rrmxn0zzmfwhz71p01zq3d6ay04n") (r "1.62.1")))

(define-public crate-mc-sgx-util-0.7.5 (c (n "mc-sgx-util") (v "0.7.5") (h "10xkj5c8ikg7xbcmxhvgvj880bad4f2isx2fiwq2badm7fa3z49z") (r "1.62.1")))

(define-public crate-mc-sgx-util-0.8.0 (c (n "mc-sgx-util") (v "0.8.0") (h "0ixqrqbn9v0rvvx1h1p4wn4lb24nli11a79p23rw29qbw53mpnzl") (r "1.62.1")))

(define-public crate-mc-sgx-util-0.9.0 (c (n "mc-sgx-util") (v "0.9.0") (h "17hpdr1ch6izpn200ddyv2h7kc5bdmjpicvmm964zh2921msgyia") (r "1.62.1")))

(define-public crate-mc-sgx-util-0.10.0 (c (n "mc-sgx-util") (v "0.10.0") (h "121z7wg2xl48ld2i379ka8q51n18mziwy4f2h513vfzj55j2d8pc") (r "1.62.1")))

(define-public crate-mc-sgx-util-0.10.1 (c (n "mc-sgx-util") (v "0.10.1") (h "1c7mwlaxhdzpcz4awl24kixgp77y2hij6naag6126j3xapfpp8bh") (r "1.62.1")))

(define-public crate-mc-sgx-util-0.11.0 (c (n "mc-sgx-util") (v "0.11.0") (h "1dzz7x5lb3ifsv4pl5xfbxfm4pddg30chwd1n7230dg3qfz3ghmk") (r "1.62.1")))

