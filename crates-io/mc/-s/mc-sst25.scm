(define-module (crates-io mc -s mc-sst25) #:use-module (crates-io))

(define-public crate-mc-sst25-0.1.0 (c (n "mc-sst25") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "mockall") (r "^0.11.3") (d #t) (k 2)))) (h "0avgi3jwl9jxbms9hgycjnx0dks8l7gba306q8gkrb9sh80bi33s") (f (quote (("strict") ("example") ("default" "example"))))))

(define-public crate-mc-sst25-0.1.1 (c (n "mc-sst25") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "mockall") (r "^0.11.3") (d #t) (k 2)))) (h "1qr9nmfj1pq40ygh8lngkq8gi08j8yxlphh72zwr69s916dd5l6l") (f (quote (("strict") ("example") ("default" "example"))))))

(define-public crate-mc-sst25-0.1.2 (c (n "mc-sst25") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "mockall") (r "^0.11.3") (d #t) (k 2)))) (h "0p87a5v4z8jpajyslxpfkiwsafp1kwq93wwf85143zmg5adyscwy") (f (quote (("strict") ("example") ("default" "example"))))))

(define-public crate-mc-sst25-0.1.3 (c (n "mc-sst25") (v "0.1.3") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "mockall") (r "^0.11.3") (d #t) (k 2)))) (h "0n6iirkajcf2f1j23jf5ypckqwl6vplf80pvmhk411clyq2zppsc") (f (quote (("strict") ("example") ("default" "example"))))))

