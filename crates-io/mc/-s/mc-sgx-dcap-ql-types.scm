(define-module (crates-io mc -s mc-sgx-dcap-ql-types) #:use-module (crates-io))

(define-public crate-mc-sgx-dcap-ql-types-0.2.0 (c (n "mc-sgx-dcap-ql-types") (v "0.2.0") (d (list (d (n "mc-sgx-core-types") (r "=0.2.0") (d #t) (k 0)) (d (n "mc-sgx-dcap-ql-sys-types") (r "=0.2.0") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "0x6pm8pny9rxk20ifh9mvgj8idq3b4w7xkxxxvr9a85v7za5ixn1") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-types-0.2.1 (c (n "mc-sgx-dcap-ql-types") (v "0.2.1") (d (list (d (n "mc-sgx-core-types") (r "=0.2.1") (d #t) (k 0)) (d (n "mc-sgx-dcap-ql-sys-types") (r "=0.2.1") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "0dval7zvpqay1nmzymsh6bzrg82ny2qx6gjs9cf9jyfhxyswss34") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-types-0.3.0 (c (n "mc-sgx-dcap-ql-types") (v "0.3.0") (d (list (d (n "mc-sgx-core-types") (r "=0.3.0") (d #t) (k 0)) (d (n "mc-sgx-dcap-ql-sys-types") (r "=0.3.0") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "12jwqa66glgi8cw3zfidlzdg63amip0swcfsk2qrkfrj8v1lc1ir") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-types-0.4.0 (c (n "mc-sgx-dcap-ql-types") (v "0.4.0") (d (list (d (n "mc-sgx-core-types") (r "=0.4.0") (d #t) (k 0)) (d (n "mc-sgx-dcap-ql-sys-types") (r "=0.4.0") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "078g9wnqpx8gngzdy0ky10mgza1rpsv4b8cc0b172kbxzvqd30hm") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-types-0.4.1 (c (n "mc-sgx-dcap-ql-types") (v "0.4.1") (d (list (d (n "mc-sgx-core-types") (r "=0.4.1") (d #t) (k 0)) (d (n "mc-sgx-dcap-ql-sys-types") (r "=0.4.1") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "15w5hq93ks36kida2986d8y7wrz5n6rlmcz6nnlc3c51ibns5gcq") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-types-0.4.2 (c (n "mc-sgx-dcap-ql-types") (v "0.4.2") (d (list (d (n "mc-sgx-core-types") (r "=0.4.2") (d #t) (k 0)) (d (n "mc-sgx-dcap-ql-sys-types") (r "=0.4.2") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "1594v2k1wrdg0rm5m0xkgqn35xc45l3fv88zb0nwvwi3c084cr40") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-types-0.5.0 (c (n "mc-sgx-dcap-ql-types") (v "0.5.0") (d (list (d (n "mc-sgx-core-types") (r "=0.5.0") (d #t) (k 0)) (d (n "mc-sgx-dcap-ql-sys-types") (r "=0.5.0") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "1ay4sv5hwjd1c98v97205ys3sn6s1zr8akknzqr1waiq5zpgjasf") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-types-0.6.0 (c (n "mc-sgx-dcap-ql-types") (v "0.6.0") (d (list (d (n "mc-sgx-core-types") (r "=0.6.0") (d #t) (k 0)) (d (n "mc-sgx-dcap-ql-sys-types") (r "=0.6.0") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "19gkdp875jgb64phi4lsjm5kwzh3cbvl2x6z0qafbknin84d3apz") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-types-0.6.1 (c (n "mc-sgx-dcap-ql-types") (v "0.6.1") (d (list (d (n "mc-sgx-core-types") (r "=0.6.1") (d #t) (k 0)) (d (n "mc-sgx-dcap-ql-sys-types") (r "=0.6.1") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "0fd1ziz6kvf3sqxj3yfdn5b9ybq50wqryknpzp5di2xxis180is3") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-types-0.7.0 (c (n "mc-sgx-dcap-ql-types") (v "0.7.0") (d (list (d (n "mc-sgx-core-types") (r "=0.7.0") (d #t) (k 0)) (d (n "mc-sgx-dcap-ql-sys-types") (r "=0.7.0") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "0f8xvndrzhpfrvxy0a1lnbiscrzhly59ld4pfpkng14s29jllh13") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-types-0.7.1 (c (n "mc-sgx-dcap-ql-types") (v "0.7.1") (d (list (d (n "mc-sgx-core-types") (r "=0.7.1") (d #t) (k 0)) (d (n "mc-sgx-dcap-ql-sys-types") (r "=0.7.1") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "00p1k7553zzcamvpv2w5n18g5iw79b7294bykrl848h54mg3c7sw") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-types-0.7.2 (c (n "mc-sgx-dcap-ql-types") (v "0.7.2") (d (list (d (n "mc-sgx-core-types") (r "=0.7.2") (d #t) (k 0)) (d (n "mc-sgx-dcap-ql-sys-types") (r "=0.7.2") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "0iiwd0zpi8zfb25izi7z0kpmcria9gp5x8r4hbhsip83ifdf4cvn") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-types-0.7.3 (c (n "mc-sgx-dcap-ql-types") (v "0.7.3") (d (list (d (n "mc-sgx-core-types") (r "=0.7.3") (d #t) (k 0)) (d (n "mc-sgx-dcap-ql-sys-types") (r "=0.7.3") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "0kq2vqhif3asb3bhf87cnv0qw7x8dksci1ybq9d401mr4yxaxzdx") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-types-0.7.4 (c (n "mc-sgx-dcap-ql-types") (v "0.7.4") (d (list (d (n "mc-sgx-core-types") (r "=0.7.4") (d #t) (k 0)) (d (n "mc-sgx-dcap-ql-sys-types") (r "=0.7.4") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "1x3g0jmq4ap4ijv94m22p1v3bnzccb3smq9dg69hlvg1icwswjfw") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-types-0.7.5 (c (n "mc-sgx-dcap-ql-types") (v "0.7.5") (d (list (d (n "mc-sgx-core-types") (r "=0.7.5") (d #t) (k 0)) (d (n "mc-sgx-dcap-ql-sys-types") (r "=0.7.5") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "1gqx9l59b21z5jnqphj4ymd9j51j8yr7z42jpcg1nhrkwkl0n7hp") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-types-0.8.0 (c (n "mc-sgx-dcap-ql-types") (v "0.8.0") (d (list (d (n "mc-sgx-core-types") (r "=0.8.0") (d #t) (k 0)) (d (n "mc-sgx-dcap-ql-sys-types") (r "=0.8.0") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "08lc4zigqdyja52bx6s3brpa67axh4h95n4rmcsi89cq2zwz1vi8") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-types-0.9.0 (c (n "mc-sgx-dcap-ql-types") (v "0.9.0") (d (list (d (n "mc-sgx-core-types") (r "=0.9.0") (d #t) (k 0)) (d (n "mc-sgx-dcap-ql-sys-types") (r "=0.9.0") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "1vfybg08rj8nv1wq6hhaym5km5f9vvxvalnlp9ykhpqp6irzdm9p") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-types-0.10.0 (c (n "mc-sgx-dcap-ql-types") (v "0.10.0") (d (list (d (n "mc-sgx-core-types") (r "=0.10.0") (d #t) (k 0)) (d (n "mc-sgx-dcap-ql-sys-types") (r "=0.10.0") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "00nskf4m6h1qrndy4qh8kad8hr1y0f19k4q0awyd76isr9rc4h84") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-types-0.10.1 (c (n "mc-sgx-dcap-ql-types") (v "0.10.1") (d (list (d (n "mc-sgx-core-types") (r "=0.10.1") (d #t) (k 0)) (d (n "mc-sgx-dcap-ql-sys-types") (r "=0.10.1") (d #t) (k 0)) (d (n "yare") (r "^1.0.2") (d #t) (k 2)))) (h "1wflglb1kp3rqq7ablsvr94qv2y1h10ib1lrq5wjw4qdw4001nf8") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-types-0.11.0 (c (n "mc-sgx-dcap-ql-types") (v "0.11.0") (d (list (d (n "mc-sgx-core-types") (r "=0.11.0") (d #t) (k 0)) (d (n "mc-sgx-dcap-ql-sys-types") (r "=0.11.0") (d #t) (k 0)) (d (n "yare") (r "^2") (d #t) (k 2)))) (h "078aa3v8lbmg3z2pwmzm0agysan6rmnk00bb86zlyi4z1zryamb0") (r "1.62.1")))

