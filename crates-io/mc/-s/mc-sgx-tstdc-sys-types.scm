(define-module (crates-io mc -s mc-sgx-tstdc-sys-types) #:use-module (crates-io))

(define-public crate-mc-sgx-tstdc-sys-types-0.1.0 (c (n "mc-sgx-tstdc-sys-types") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.1.0") (d #t) (k 1)))) (h "1wxjkfg0kcsiwbqnd0ymf393psr63x7kjjkr8kafifw63wrrdsz4") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-sys-types-0.2.0 (c (n "mc-sgx-tstdc-sys-types") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.2.0") (d #t) (k 1)))) (h "07cjnf2kwq3g9yfmr5rpksy01acnkz4s0fpf804szlnczwagv3ls") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-sys-types-0.2.1 (c (n "mc-sgx-tstdc-sys-types") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.2.1") (d #t) (k 1)))) (h "1d2qg61ns3wq510zknqgpx3m457az5n9r415r6pphfn5vmr0iaaq") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-sys-types-0.3.0 (c (n "mc-sgx-tstdc-sys-types") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.3.0") (d #t) (k 1)))) (h "1xn82n0inwxdvzknkcjgqlng11vlj9mwikw2c5zkzf4xavb07dv3") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-sys-types-0.4.0 (c (n "mc-sgx-tstdc-sys-types") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.4.0") (d #t) (k 1)))) (h "1pzx5aayndqaaiy3vz42isfi8sgi30ihaqd2vkgb9rn2y1i8anwh") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-sys-types-0.4.1 (c (n "mc-sgx-tstdc-sys-types") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.4.1") (d #t) (k 1)))) (h "0qrwrwkr5mmhmc9bzh9mf9cnbxb59n4y76k0pcshlv9v55n42ffq") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-sys-types-0.4.2 (c (n "mc-sgx-tstdc-sys-types") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.4.2") (d #t) (k 1)))) (h "0ibr2l77nxkbzphx5rk2mjldii5miv41sx2y0i9lffrwqh9v1vnz") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-sys-types-0.5.0 (c (n "mc-sgx-tstdc-sys-types") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.5.0") (d #t) (k 1)))) (h "1mkb4qivdhpinbd810snzc6899lp89hx8n2qzkxrafg2m9gzwa56") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-sys-types-0.6.0 (c (n "mc-sgx-tstdc-sys-types") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.6.0") (d #t) (k 1)))) (h "15w9w765h5hrbdnb1h07rhl24slp71gnp6hymnvfyhpzljig2njr") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-sys-types-0.6.1 (c (n "mc-sgx-tstdc-sys-types") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.6.1") (d #t) (k 1)))) (h "0rlqwfdsvg63ppm6y8zss0idbx04k43hymf6myvyc0pj20g5g3qq") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-sys-types-0.7.0 (c (n "mc-sgx-tstdc-sys-types") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.0") (d #t) (k 1)))) (h "1g6sn7lm0d5rgcb9g98x85f5xm00rbfz5bnm749240dfllwcwczi") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-sys-types-0.7.1 (c (n "mc-sgx-tstdc-sys-types") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.1") (d #t) (k 1)))) (h "1zigwn4ddmfanh2idrswgc74p3l1ympzi27bnjppmiv9l1hv7fb7") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-sys-types-0.7.2 (c (n "mc-sgx-tstdc-sys-types") (v "0.7.2") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.2") (d #t) (k 1)))) (h "01c1zmc7d07r0gcksrzc1r17x4y0j8agi2h263gr2xqan2bwi587") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-sys-types-0.7.3 (c (n "mc-sgx-tstdc-sys-types") (v "0.7.3") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.3") (d #t) (k 1)))) (h "13bnly8awxb4f5af88gyzss9nsl58dl1hkyx7p97mq4d4mrdsqs7") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-sys-types-0.7.4 (c (n "mc-sgx-tstdc-sys-types") (v "0.7.4") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.4") (d #t) (k 1)))) (h "1b45gakfb1mb2y9hna5sq0qn0zgrb4a0z2ah4c6z71yay7h2k0k5") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-sys-types-0.7.5 (c (n "mc-sgx-tstdc-sys-types") (v "0.7.5") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.5") (d #t) (k 1)))) (h "0sr2w515as6ii1w4b6bqypbj8m7y5hk07chd3i05lkmjqc08483l") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-sys-types-0.8.0 (c (n "mc-sgx-tstdc-sys-types") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.8.0") (d #t) (k 1)))) (h "0xsz9sydh2mj0w80h3hh49nlwkpkvnzskkjcwk3n5axdvf2kda3g") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-sys-types-0.9.0 (c (n "mc-sgx-tstdc-sys-types") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.9.0") (d #t) (k 1)))) (h "0n7517k1jwl9xms0q4sml8rbcgjiybd82ckql7pa9fn89hmggpgs") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-sys-types-0.10.0 (c (n "mc-sgx-tstdc-sys-types") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.10.0") (d #t) (k 1)))) (h "19x5dqr7vz5jhxjysq1j9lx5fm64iqi8r5b58ni7m0scj4s123w2") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-sys-types-0.10.1 (c (n "mc-sgx-tstdc-sys-types") (v "0.10.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.10.1") (d #t) (k 1)))) (h "0kdky3fdxid863gggpi2hbhabq7m0fhm9q6pran5xf445sxw8b5g") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-sys-types-0.11.0 (c (n "mc-sgx-tstdc-sys-types") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.11.0") (d #t) (k 1)))) (h "108vywigvxm9cw86ajffx72j7nkgyp07cw6z9jxxmssr30b7mx49") (r "1.62.1")))

