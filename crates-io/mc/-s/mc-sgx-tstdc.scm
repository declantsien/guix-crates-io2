(define-module (crates-io mc -s mc-sgx-tstdc) #:use-module (crates-io))

(define-public crate-mc-sgx-tstdc-0.4.1 (c (n "mc-sgx-tstdc") (v "0.4.1") (d (list (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "libc") (r "^0.2.139") (k 0)) (d (n "mc-sgx-tstdc-sys") (r "^0.4.1-beta.0") (d #t) (k 0)) (d (n "mc-sgx-tstdc-sys-types") (r "^0.4.1-beta.0") (d #t) (k 0)))) (h "1pvqfvck1mcfdk3fvsm2cqp9crfpclbbk3z4lkd1r5cgkln5mqh7") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-0.4.2 (c (n "mc-sgx-tstdc") (v "0.4.2") (d (list (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "libc") (r "^0.2.139") (k 0)) (d (n "mc-sgx-tstdc-sys") (r "^0.4.2-beta.0") (d #t) (k 0)) (d (n "mc-sgx-tstdc-sys-types") (r "^0.4.2-beta.0") (d #t) (k 0)))) (h "01la3xlcshpa7igl8b3gfdm80whk6z06jay4v8843nf5dlz6gcf6") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-0.5.0 (c (n "mc-sgx-tstdc") (v "0.5.0") (d (list (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "libc") (r "^0.2.139") (k 0)) (d (n "mc-sgx-tstdc-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "mc-sgx-tstdc-sys-types") (r "^0.5.0") (d #t) (k 0)))) (h "1314q1p89x7hipxpv2ay2rffx9zbxr74ij0yqb0f3mnlva8y9vv0") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-0.6.0 (c (n "mc-sgx-tstdc") (v "0.6.0") (d (list (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "libc") (r "^0.2.139") (k 0)) (d (n "mc-sgx-tstdc-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "mc-sgx-tstdc-sys-types") (r "^0.6.0") (d #t) (k 0)))) (h "0n6l2nlzhhz96x8r8g0mfh3isbbnjsav2096fkrcaaykw0il3q87") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-0.6.1 (c (n "mc-sgx-tstdc") (v "0.6.1") (d (list (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "libc") (r "^0.2.139") (k 0)) (d (n "mc-sgx-tstdc-sys") (r "^0.6.1") (d #t) (k 0)) (d (n "mc-sgx-tstdc-sys-types") (r "^0.6.1") (d #t) (k 0)))) (h "0hhb6h1k30k4hpmcrz1hyy66szghkvi6lh8sjn2iinka0p9hc1ja") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-0.7.0 (c (n "mc-sgx-tstdc") (v "0.7.0") (d (list (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "libc") (r "^0.2.139") (k 0)) (d (n "mc-sgx-tstdc-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "mc-sgx-tstdc-sys-types") (r "^0.7.0") (d #t) (k 0)))) (h "07cx6mczbp6b2j0rmm6f2yg4q3nv465xgk9q70gj9smckf7jzq3z") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-0.7.1 (c (n "mc-sgx-tstdc") (v "0.7.1") (d (list (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "libc") (r "^0.2.139") (k 0)) (d (n "mc-sgx-tstdc-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "mc-sgx-tstdc-sys-types") (r "^0.7.0") (d #t) (k 0)))) (h "1qqf6m731casjjnc7p1vs6ngi7lw1aa3r84c7fvxz2yhvr0z49j2") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-0.7.2 (c (n "mc-sgx-tstdc") (v "0.7.2") (d (list (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "libc") (r "^0.2.139") (k 0)) (d (n "mc-sgx-tstdc-sys") (r "=0.7.2") (d #t) (k 0)) (d (n "mc-sgx-tstdc-sys-types") (r "=0.7.2") (d #t) (k 0)))) (h "0x1dgb1m7if4ngx5y499xp46s9a9yyp9jfwfzwk93z05hxjxbaxj") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-0.7.3 (c (n "mc-sgx-tstdc") (v "0.7.3") (d (list (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "libc") (r "^0.2.139") (k 0)) (d (n "mc-sgx-tstdc-sys") (r "=0.7.3") (d #t) (k 0)) (d (n "mc-sgx-tstdc-sys-types") (r "=0.7.3") (d #t) (k 0)))) (h "00ppdf52jdh9v67h6cwdpxssx4xgkxn2dfk8s4jmllbjpzg006w3") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-0.7.4 (c (n "mc-sgx-tstdc") (v "0.7.4") (d (list (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "libc") (r "^0.2.139") (k 0)) (d (n "mc-sgx-tstdc-sys") (r "=0.7.4") (d #t) (k 0)) (d (n "mc-sgx-tstdc-sys-types") (r "=0.7.4") (d #t) (k 0)))) (h "1hnsdvdb7fqbwh2wczkdax079gdndz7n2cgmg6csq064nwfw29lx") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-0.7.5 (c (n "mc-sgx-tstdc") (v "0.7.5") (d (list (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "libc") (r "^0.2.139") (k 0)) (d (n "mc-sgx-tstdc-sys") (r "=0.7.5") (d #t) (k 0)) (d (n "mc-sgx-tstdc-sys-types") (r "=0.7.5") (d #t) (k 0)))) (h "1k6fhsic3wcr9nmy0bddjhjf9qmaqmp3fv8c77ny6zbnlfl7lppk") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-0.8.0 (c (n "mc-sgx-tstdc") (v "0.8.0") (d (list (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "libc") (r "^0.2.139") (k 0)) (d (n "mc-sgx-tstdc-sys") (r "=0.8.0") (d #t) (k 0)) (d (n "mc-sgx-tstdc-sys-types") (r "=0.8.0") (d #t) (k 0)))) (h "0f12abgdycy6fffkn6g082zgzf8fgps3gy1ha96d2bfgyc2pgnka") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-0.9.0 (c (n "mc-sgx-tstdc") (v "0.9.0") (d (list (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "libc") (r "^0.2.139") (k 0)) (d (n "mc-sgx-tstdc-sys") (r "=0.9.0") (d #t) (k 0)) (d (n "mc-sgx-tstdc-sys-types") (r "=0.9.0") (d #t) (k 0)))) (h "0s4xaj809464p3sj3vawibpcshkp0g11a336ms4x9j9d1gl2296g") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-0.10.0 (c (n "mc-sgx-tstdc") (v "0.10.0") (d (list (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "libc") (r "^0.2.139") (k 0)) (d (n "mc-sgx-tstdc-sys") (r "=0.10.0") (d #t) (k 0)) (d (n "mc-sgx-tstdc-sys-types") (r "=0.10.0") (d #t) (k 0)))) (h "0npr8bpgq06grzmzc15d7hzz60vhzlfx97nihn5ip3y98hwhhpil") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-0.10.1 (c (n "mc-sgx-tstdc") (v "0.10.1") (d (list (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "libc") (r "^0.2.139") (k 0)) (d (n "mc-sgx-tstdc-sys") (r "=0.10.1") (d #t) (k 0)) (d (n "mc-sgx-tstdc-sys-types") (r "=0.10.1") (d #t) (k 0)))) (h "0lsk940yymkp6ajvvvwxyyb3n4plf6jdqz2zm1371cbv61qgqz97") (r "1.62.1")))

(define-public crate-mc-sgx-tstdc-0.11.0 (c (n "mc-sgx-tstdc") (v "0.11.0") (d (list (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "libc") (r "^0.2.139") (k 0)) (d (n "mc-sgx-tstdc-sys") (r "=0.11.0") (d #t) (k 0)) (d (n "mc-sgx-tstdc-sys-types") (r "=0.11.0") (d #t) (k 0)))) (h "0ax0zmmx6cfmv0g6zij8iq928f7yix9hzixmc32wdp0n2bf7rydl") (r "1.62.1")))

