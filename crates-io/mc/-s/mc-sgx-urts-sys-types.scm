(define-module (crates-io mc -s mc-sgx-urts-sys-types) #:use-module (crates-io))

(define-public crate-mc-sgx-urts-sys-types-0.1.0 (c (n "mc-sgx-urts-sys-types") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.1.0") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.1.0") (d #t) (k 0)))) (h "1cdkil3vrh2hajx4mhvnxy65kdyvi4zs1bpm4hn4lxa4v3ckkn6y") (r "1.62.1")))

(define-public crate-mc-sgx-urts-sys-types-0.2.0 (c (n "mc-sgx-urts-sys-types") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.2.0") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.2.0") (d #t) (k 0)))) (h "04a71h7rjfjbs62fbf2ap1l484rnd7i1pfqxqwpmpzk1b20fdmfb") (r "1.62.1")))

(define-public crate-mc-sgx-urts-sys-types-0.2.1 (c (n "mc-sgx-urts-sys-types") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.2.1") (d #t) (k 0)))) (h "0wngsvg40ix3z7jyrjy0sxf8mspw36z625g33hdsr0rikvcm63b0") (r "1.62.1")))

(define-public crate-mc-sgx-urts-sys-types-0.3.0 (c (n "mc-sgx-urts-sys-types") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.3.0") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.3.0") (d #t) (k 0)))) (h "1dbx2mll1fdkq9kqihqzlly4cf0acx6mxjvkjklzi1nq340ycxb9") (r "1.62.1")))

(define-public crate-mc-sgx-urts-sys-types-0.4.0 (c (n "mc-sgx-urts-sys-types") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.4.0") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.4.0") (d #t) (k 0)))) (h "0b282p15q3387zmgigmbna0qi6aql06a27m93d7q46fxyj7ni83c") (r "1.62.1")))

(define-public crate-mc-sgx-urts-sys-types-0.4.1 (c (n "mc-sgx-urts-sys-types") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.4.1") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.4.1") (d #t) (k 0)))) (h "0r641i0y6gr41571dklrb1s0l3j94br1bjqs0xkj08n99c60n5w5") (r "1.62.1")))

(define-public crate-mc-sgx-urts-sys-types-0.4.2 (c (n "mc-sgx-urts-sys-types") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.4.2") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.4.2") (d #t) (k 0)))) (h "19c8fcwrdd6lsxd95128r58dvpqympn5ss4xrxxkh31a4nqjkkkx") (r "1.62.1")))

(define-public crate-mc-sgx-urts-sys-types-0.5.0 (c (n "mc-sgx-urts-sys-types") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.5.0") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.5.0") (d #t) (k 0)))) (h "1p5mwvckzb4xqnz3dymwhcznl3jhz0z2075g7zsa64d37aqqsaja") (r "1.62.1")))

(define-public crate-mc-sgx-urts-sys-types-0.6.0 (c (n "mc-sgx-urts-sys-types") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.6.0") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.6.0") (d #t) (k 0)))) (h "1gp7c61c2bgg1hvb77kg0q6r3p0cia32h1z0jp610iazn3lmz15a") (r "1.62.1")))

(define-public crate-mc-sgx-urts-sys-types-0.6.1 (c (n "mc-sgx-urts-sys-types") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.6.1") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.6.1") (d #t) (k 0)))) (h "1qi4i8887k26nmkki1k3533w9qmjj3zvwyi5ar76535aqny7azf7") (r "1.62.1")))

(define-public crate-mc-sgx-urts-sys-types-0.7.0 (c (n "mc-sgx-urts-sys-types") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.0") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.7.0") (d #t) (k 0)))) (h "1wd3nhfg4xmkryxiiprk265x7iwp1h8hb7k5q8yhp271l6mw4mw7") (r "1.62.1")))

(define-public crate-mc-sgx-urts-sys-types-0.7.1 (c (n "mc-sgx-urts-sys-types") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.1") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.7.1") (d #t) (k 0)))) (h "11cjq3miyr492x4fhczzbi86gm6irqzb0cpbz0qfpqc8qd4myb1d") (r "1.62.1")))

(define-public crate-mc-sgx-urts-sys-types-0.7.2 (c (n "mc-sgx-urts-sys-types") (v "0.7.2") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.2") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.7.2") (d #t) (k 0)))) (h "1aq63irggb2arb681m6qrmsfm1hyrd1n3pyb8fsi9hhyhxdns01n") (r "1.62.1")))

(define-public crate-mc-sgx-urts-sys-types-0.7.3 (c (n "mc-sgx-urts-sys-types") (v "0.7.3") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.3") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.7.3") (d #t) (k 0)))) (h "10n2spzfjiq8lx76pgqpi02s814v7dm7wy7b08jb8i3q7g20ppaf") (r "1.62.1")))

(define-public crate-mc-sgx-urts-sys-types-0.7.4 (c (n "mc-sgx-urts-sys-types") (v "0.7.4") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.4") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.7.4") (d #t) (k 0)))) (h "1w9xc3syb2l2x5j0pnxxs46va7jmhd84dfwghvj0dbgm42mzw282") (r "1.62.1")))

(define-public crate-mc-sgx-urts-sys-types-0.7.5 (c (n "mc-sgx-urts-sys-types") (v "0.7.5") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.5") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.7.5") (d #t) (k 0)))) (h "1v7hll70314gmz2xivrib3fwnn4dydliy5h9p6s7xqr89mhg1ify") (r "1.62.1")))

(define-public crate-mc-sgx-urts-sys-types-0.8.0 (c (n "mc-sgx-urts-sys-types") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.8.0") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.8.0") (d #t) (k 0)))) (h "1ilfgyav720vya4pc92ggdr6wy68fiyfcwv3flifvr19mba7067p") (r "1.62.1")))

(define-public crate-mc-sgx-urts-sys-types-0.9.0 (c (n "mc-sgx-urts-sys-types") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.9.0") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.9.0") (d #t) (k 0)))) (h "0hkdv2ijgx0hbiyr3wxifyfnv3wli3751pk0qn4sgkh3pc2whfir") (r "1.62.1")))

(define-public crate-mc-sgx-urts-sys-types-0.10.0 (c (n "mc-sgx-urts-sys-types") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.10.0") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.10.0") (d #t) (k 0)))) (h "1zqjglikjybdrh8wamn4xpmak8y7csmpwv8fwa85qjg3kfniz0n3") (r "1.62.1")))

(define-public crate-mc-sgx-urts-sys-types-0.10.1 (c (n "mc-sgx-urts-sys-types") (v "0.10.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.10.1") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.10.1") (d #t) (k 0)))) (h "1ci3v3cq4z0q9d2xjgxivxdzi0g1afb81nnz9qgml32snzhpilhl") (r "1.62.1")))

(define-public crate-mc-sgx-urts-sys-types-0.11.0 (c (n "mc-sgx-urts-sys-types") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.11.0") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.11.0") (d #t) (k 0)))) (h "0mb3zjal99blnqndnvajbdv2k9zi0hf2zr0wahdgizy2z02mcgky") (r "1.62.1")))

