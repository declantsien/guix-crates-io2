(define-module (crates-io mc -s mc-sgx-dcap-sys-types) #:use-module (crates-io))

(define-public crate-mc-sgx-dcap-sys-types-0.1.0 (c (n "mc-sgx-dcap-sys-types") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.1.0") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.1.0") (d #t) (k 0)))) (h "1hwjxgcvs7c7s2050mbpb6vnkmvjs91a6wsj6r7zc83iaygcjffw") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-sys-types-0.2.0 (c (n "mc-sgx-dcap-sys-types") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.2.0") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.2.0") (d #t) (k 0)))) (h "01xfrskgdp4hq52xvp64ganrvdnj43z38475gp1q2bcnlmhfhwcq") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-sys-types-0.2.1 (c (n "mc-sgx-dcap-sys-types") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.2.1") (d #t) (k 0)))) (h "1ba6wf6vmla3isqk0wb3c9bpplzyyw45y6njkksf9x56fxdnsqlg") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-sys-types-0.3.0 (c (n "mc-sgx-dcap-sys-types") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.3.0") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.3.0") (d #t) (k 0)))) (h "0w6qaiphjckc6sapqvvyvvmkmapxclcpnbflxs1mhqabg4x3947s") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-sys-types-0.4.0 (c (n "mc-sgx-dcap-sys-types") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.4.0") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.4.0") (d #t) (k 0)))) (h "1p9fb89czkap88iz2c9q0vv3bfnzqp3qay411dah9iahhhjnrgbg") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-sys-types-0.4.1 (c (n "mc-sgx-dcap-sys-types") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.4.1") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.4.1") (d #t) (k 0)))) (h "1dkyr3ik3pfz0h1nncc9anm9milyv98nya4q4lbxicf9as4dyx8n") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-sys-types-0.4.2 (c (n "mc-sgx-dcap-sys-types") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.4.2") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.4.2") (d #t) (k 0)))) (h "0zwirjsga6i4v48radly5iakxzxk7ri6azs963g6g4d7a23sjx49") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-sys-types-0.5.0 (c (n "mc-sgx-dcap-sys-types") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.5.0") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.5.0") (d #t) (k 0)))) (h "06nq3hpfh5jq4989yfh2g0wpzk2gyx37hh9hmgsz9g77d9b4599x") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-sys-types-0.6.0 (c (n "mc-sgx-dcap-sys-types") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.6.0") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.6.0") (d #t) (k 0)))) (h "1c2rmc91wlwshxh6dpga5n6fnfimpm8iaby7whlcsqzj8vmgxfhs") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-sys-types-0.6.1 (c (n "mc-sgx-dcap-sys-types") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.6.1") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.6.1") (d #t) (k 0)))) (h "1n64ydwyh7hap7id59qnrraiksfb1s4yhyrllx986dm8bz5s2mvi") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-sys-types-0.7.0 (c (n "mc-sgx-dcap-sys-types") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.0") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.7.0") (d #t) (k 0)))) (h "1majymbgkrf3qzzxk1s2rnpfizzvs89cbh8k9788kkqy45gnjjs3") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-sys-types-0.7.1 (c (n "mc-sgx-dcap-sys-types") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.1") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.7.1") (d #t) (k 0)))) (h "1g4ay66zqnj9n64bgrs86003a5pxbama5adyf2m0a56gdr3smlpd") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-sys-types-0.7.2 (c (n "mc-sgx-dcap-sys-types") (v "0.7.2") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.2") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.7.2") (d #t) (k 0)))) (h "1ra4494108brv7gajbyx6jg9f5wc3g3zw3ry20mywz9nll8ijh52") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-sys-types-0.7.3 (c (n "mc-sgx-dcap-sys-types") (v "0.7.3") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.3") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.7.3") (d #t) (k 0)))) (h "1jja6npmjijf76g2493c4fl9j4pfzi4iq2aa1fw4jy981kx4bq6n") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-sys-types-0.7.4 (c (n "mc-sgx-dcap-sys-types") (v "0.7.4") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.4") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.7.4") (d #t) (k 0)))) (h "0mwd4mr97x1s8vxb53xxqv1xi5c3zsmaasinc1y2d5pryyjycksi") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-sys-types-0.7.5 (c (n "mc-sgx-dcap-sys-types") (v "0.7.5") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.5") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.7.5") (d #t) (k 0)))) (h "0r0v55l6wmi7mrskcgaqlwnpzrsjwymyrbjrp246mq65kcck3i7k") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-sys-types-0.8.0 (c (n "mc-sgx-dcap-sys-types") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.8.0") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.8.0") (d #t) (k 0)))) (h "0q0q2vz8rs0dfj3dj3gn5kg7yc00dxhmfnsjyd6xbv6ikcz6d66g") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-sys-types-0.9.0 (c (n "mc-sgx-dcap-sys-types") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.9.0") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.9.0") (d #t) (k 0)))) (h "1y43g39raf8bnjqacdb6rxbbjgb38g3m2il9bf6vzi7b8p8p7jsr") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-sys-types-0.10.0 (c (n "mc-sgx-dcap-sys-types") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.10.0") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.10.0") (d #t) (k 0)))) (h "1dvarw0qc7pw4pznairgbynz3ch78djrgv865jgczp8ssd2h27y8") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-sys-types-0.10.1 (c (n "mc-sgx-dcap-sys-types") (v "0.10.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.10.1") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.10.1") (d #t) (k 0)))) (h "1s88rbnx44vvns2hnb5msbsaf8fcby7by3lmm1gw4z84khidssny") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-sys-types-0.11.0 (c (n "mc-sgx-dcap-sys-types") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.11.0") (d #t) (k 1)) (d (n "mc-sgx-core-sys-types") (r "=0.11.0") (d #t) (k 0)))) (h "12bynhfm63f9338r0avkcx3wl15w1abpk4gvwqni8p3a64xfklrd") (r "1.62.1")))

