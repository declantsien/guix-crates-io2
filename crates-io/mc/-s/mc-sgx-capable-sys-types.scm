(define-module (crates-io mc -s mc-sgx-capable-sys-types) #:use-module (crates-io))

(define-public crate-mc-sgx-capable-sys-types-0.1.0 (c (n "mc-sgx-capable-sys-types") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.1.0") (d #t) (k 1)))) (h "18mgh3xz8jk4mapwnf2n4calgb3hf8nrxamxhgfb6qqns8nrjlhf") (r "1.62.1")))

(define-public crate-mc-sgx-capable-sys-types-0.2.0 (c (n "mc-sgx-capable-sys-types") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.2.0") (d #t) (k 1)))) (h "0fi0mjjdiag7y358v42k8pdwsckjldx1zkpjq0a2g4nfn28hz7l1") (r "1.62.1")))

(define-public crate-mc-sgx-capable-sys-types-0.2.1 (c (n "mc-sgx-capable-sys-types") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.2.1") (d #t) (k 1)))) (h "19dd862i6mcr9lysg6y7rdvbmsk61aaagyn26ngikh6q33gdmvzs") (r "1.62.1")))

(define-public crate-mc-sgx-capable-sys-types-0.3.0 (c (n "mc-sgx-capable-sys-types") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.3.0") (d #t) (k 1)))) (h "0qyv48nv07fcndsk39lffill9r3disl241qmhnszdlb4vaii8cz4") (r "1.62.1")))

(define-public crate-mc-sgx-capable-sys-types-0.4.0 (c (n "mc-sgx-capable-sys-types") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.4.0") (d #t) (k 1)))) (h "0rsgzd5ikwzz6j2swbxmgzkkxq8hk5bm6i816rwxdh4v62j2j3w8") (r "1.62.1")))

(define-public crate-mc-sgx-capable-sys-types-0.4.1 (c (n "mc-sgx-capable-sys-types") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.4.1") (d #t) (k 1)))) (h "0nbvb8dfdwqi2cbybs4rxb1zlkr7xi8y3b6br87zwf5a4nz3i39x") (r "1.62.1")))

(define-public crate-mc-sgx-capable-sys-types-0.4.2 (c (n "mc-sgx-capable-sys-types") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.4.2") (d #t) (k 1)))) (h "1ixpcfshhcy5jmxv484adbwirmvvnc3sq6zaw86s3mplyd8ldb2s") (r "1.62.1")))

(define-public crate-mc-sgx-capable-sys-types-0.5.0 (c (n "mc-sgx-capable-sys-types") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.5.0") (d #t) (k 1)))) (h "0mw9356szlx1nmzzgj407icvfbvnjpvs2mls07sfs2m48fh3f18c") (r "1.62.1")))

(define-public crate-mc-sgx-capable-sys-types-0.6.0 (c (n "mc-sgx-capable-sys-types") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.6.0") (d #t) (k 1)))) (h "1grplayapglin54g03x92i64yn19ymyd1c9v2f6ksy07dmz85jzh") (r "1.62.1")))

(define-public crate-mc-sgx-capable-sys-types-0.6.1 (c (n "mc-sgx-capable-sys-types") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.6.1") (d #t) (k 1)))) (h "1v6d3kz758pdqzccwg7q2hvc2r23fyv27r53klvrqh5byi279ivb") (r "1.62.1")))

(define-public crate-mc-sgx-capable-sys-types-0.7.0 (c (n "mc-sgx-capable-sys-types") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.0") (d #t) (k 1)))) (h "1r8v21vkzkvri6l4kkz8s43hbmwrfv0q6n1xvsq9dz6kv84cfz2w") (r "1.62.1")))

(define-public crate-mc-sgx-capable-sys-types-0.7.1 (c (n "mc-sgx-capable-sys-types") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.1") (d #t) (k 1)))) (h "1banl8b3xsp847wkxxg6i786v5jx83fac2md03ydv13lpk7x480j") (r "1.62.1")))

(define-public crate-mc-sgx-capable-sys-types-0.7.2 (c (n "mc-sgx-capable-sys-types") (v "0.7.2") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.2") (d #t) (k 1)))) (h "1vx7gz8n13ilm1jcv183idh0kms1cxv9c8gsfzmiyr0amblm6ng5") (r "1.62.1")))

(define-public crate-mc-sgx-capable-sys-types-0.7.3 (c (n "mc-sgx-capable-sys-types") (v "0.7.3") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.3") (d #t) (k 1)))) (h "1cgd698044l8bzr3b1v0ifzmv4qqfwpsp51fil0zjx27j749aqz1") (r "1.62.1")))

(define-public crate-mc-sgx-capable-sys-types-0.7.4 (c (n "mc-sgx-capable-sys-types") (v "0.7.4") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.4") (d #t) (k 1)))) (h "0zab10dgcbxvnpmyjv3v4m3vhq9ch0zin7cp5yn3jl880gcirrja") (r "1.62.1")))

(define-public crate-mc-sgx-capable-sys-types-0.7.5 (c (n "mc-sgx-capable-sys-types") (v "0.7.5") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.5") (d #t) (k 1)))) (h "0axh8bw6iz8s4lr03pkjqprh9y1g5qhdc7v6c30czx245y9aw8d1") (r "1.62.1")))

(define-public crate-mc-sgx-capable-sys-types-0.8.0 (c (n "mc-sgx-capable-sys-types") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.8.0") (d #t) (k 1)))) (h "0z520z8c0l6mf9m8w5dmrf9n0p716pfhqd7a5c2b762nkzp4k2jj") (r "1.62.1")))

(define-public crate-mc-sgx-capable-sys-types-0.9.0 (c (n "mc-sgx-capable-sys-types") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.9.0") (d #t) (k 1)))) (h "1z04j88bfij73qxy90s70vqz6cw189iwlijb5jjbiy8zfss9nswj") (r "1.62.1")))

(define-public crate-mc-sgx-capable-sys-types-0.10.0 (c (n "mc-sgx-capable-sys-types") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.10.0") (d #t) (k 1)))) (h "0k37bvszny6z8vbrbyikfh91abddidj05vnx6cf7l0z2gixc7vvm") (r "1.62.1")))

(define-public crate-mc-sgx-capable-sys-types-0.10.1 (c (n "mc-sgx-capable-sys-types") (v "0.10.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.10.1") (d #t) (k 1)))) (h "1ngzrl94ivy060mn1b7ww81anbycfmx1sqra2bpzpg11l2bkvf8x") (r "1.62.1")))

(define-public crate-mc-sgx-capable-sys-types-0.11.0 (c (n "mc-sgx-capable-sys-types") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.11.0") (d #t) (k 1)))) (h "192f3i63r0363n00w92mmxz2fb93hqf8xjvml39hxdmrnmdg8kz5") (r "1.62.1")))

