(define-module (crates-io mc -s mc-sgx-io) #:use-module (crates-io))

(define-public crate-mc-sgx-io-0.1.0 (c (n "mc-sgx-io") (v "0.1.0") (d (list (d (n "mc-sgx-core-sys-types") (r "^0.4.0") (d #t) (k 0)) (d (n "mc-sgx-core-types") (r "^0.4.0") (d #t) (k 0)) (d (n "mc-sgx-util") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 2)) (d (n "serial_test") (r "^1.0.0") (d #t) (k 2)) (d (n "yare") (r "^1.0.1") (d #t) (k 2)))) (h "1ykccc3zfbg15lpk547izhdkhpc76fsgnw89rvvz0s4rx9qplyb8") (r "1.62.1")))

