(define-module (crates-io mc -s mc-sgx-alloc) #:use-module (crates-io))

(define-public crate-mc-sgx-alloc-0.1.0 (c (n "mc-sgx-alloc") (v "0.1.0") (d (list (d (n "mc-sgx-tservice-sys") (r "^0.4.0") (d #t) (k 0)))) (h "0xfhg37k8gdkay9xvj3ysssy1b248ag44ccx23q2aalr2cnwwg8l") (r "1.62.1")))

