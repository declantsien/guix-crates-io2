(define-module (crates-io mc -s mc-server-pinger) #:use-module (crates-io))

(define-public crate-mc-server-pinger-0.1.0-alpha.1 (c (n "mc-server-pinger") (v "0.1.0-alpha.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "craftping") (r "^0.2.1") (f (quote ("async-tokio"))) (d #t) (k 0)) (d (n "peg") (r "^0.6.3") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("rt" "time" "macros"))) (d #t) (k 0)))) (h "1psccr3lnb0m069cxbkn2r2pxdiqa1kyzjj0c7acs42nkx2hyrc1")))

(define-public crate-mc-server-pinger-0.1.0-alpha.2 (c (n "mc-server-pinger") (v "0.1.0-alpha.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "craftping") (r "^0.2.1") (f (quote ("async-tokio"))) (d #t) (k 0)) (d (n "peg") (r "^0.6.3") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("rt" "time" "macros"))) (d #t) (k 0)))) (h "1x2ylbn3vglb0kqr8wgwwschcvmlpk61i59pgj0c416yjybmj58y")))

(define-public crate-mc-server-pinger-0.1.0-alpha.3 (c (n "mc-server-pinger") (v "0.1.0-alpha.3") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "craftping") (r "^0.2.1") (f (quote ("async-tokio"))) (d #t) (k 0)) (d (n "peg") (r "^0.6.3") (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("rt" "time" "macros"))) (d #t) (k 0)))) (h "1glx83p72a3rcfnypqv7by8sb973kpnwqs1s5dlfx0a3fwisc0y4")))

(define-public crate-mc-server-pinger-0.1.0-alpha.4 (c (n "mc-server-pinger") (v "0.1.0-alpha.4") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "craftping") (r "^0.2.1") (f (quote ("async-tokio"))) (d #t) (k 0)) (d (n "peg") (r "^0.6.3") (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("rt" "time" "macros"))) (d #t) (k 0)))) (h "16syj5494a38ab5mw74vhja4q2hs49qbd54d83amir4k4fmmp3kk")))

(define-public crate-mc-server-pinger-0.1.0-alpha.5 (c (n "mc-server-pinger") (v "0.1.0-alpha.5") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "craftping") (r "^0.3.0") (f (quote ("async-tokio"))) (d #t) (k 0)) (d (n "peg") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1.13.1") (f (quote ("rt" "net" "time" "macros"))) (d #t) (k 0)))) (h "1vc5zci27j0lm0k0wn4rp557xaal4za20whn54bs0bijg66xm91b")))

(define-public crate-mc-server-pinger-0.1.0-alpha.6 (c (n "mc-server-pinger") (v "0.1.0-alpha.6") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "craftping") (r "^0.3.0") (f (quote ("async-tokio"))) (d #t) (k 0)) (d (n "peg") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^1.13.1") (f (quote ("rt" "net" "time" "macros"))) (d #t) (k 0)))) (h "08sklr0qgd51bnr1xyk3b1gwf51qv9na25j06lqljhlgyyl2wk1k")))

