(define-module (crates-io mc -s mc-sgx-dcap-ql-sys-types) #:use-module (crates-io))

(define-public crate-mc-sgx-dcap-ql-sys-types-0.1.0 (c (n "mc-sgx-dcap-ql-sys-types") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.1.0") (d #t) (k 1)))) (h "1k498c0md61ipxv6ds7c44ncddra799dzy43r2wrwfck1vrh0n9w") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-sys-types-0.2.0 (c (n "mc-sgx-dcap-ql-sys-types") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.2.0") (d #t) (k 1)))) (h "18h7nwsnxjnbb41zpsax2nwssq63xskidsaqc964570ar8iddxlh") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-sys-types-0.2.1 (c (n "mc-sgx-dcap-ql-sys-types") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.2.1") (d #t) (k 1)))) (h "0fnl72fny1qbnapa5vwnz06qis99a9h0ajgbclq53nl99gll12md") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-sys-types-0.3.0 (c (n "mc-sgx-dcap-ql-sys-types") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.3.0") (d #t) (k 1)))) (h "0abq3i9gnskx54ib2p6l4zllr1qxm8xyyir0mjgs7f4rbwwy7k62") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-sys-types-0.4.0 (c (n "mc-sgx-dcap-ql-sys-types") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.4.0") (d #t) (k 1)))) (h "08azdcl1sg1h3imgx33jjmj9gsw70f85z8vf2ah8pnxd7yqnv27m") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-sys-types-0.4.1 (c (n "mc-sgx-dcap-ql-sys-types") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.4.1") (d #t) (k 1)))) (h "0c7hmnn8dfrdhhbvpk32j2y34gni7mnv7w0k7k4afsfm4wnlnbpr") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-sys-types-0.4.2 (c (n "mc-sgx-dcap-ql-sys-types") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.4.2") (d #t) (k 1)))) (h "038mgjiz1w7cfbjkkpqywyphq6ifddvvl13dhpblg7fdvyqk5xnq") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-sys-types-0.5.0 (c (n "mc-sgx-dcap-ql-sys-types") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.5.0") (d #t) (k 1)))) (h "16x7mzammw7iirkff8104vzv5400vnzy6ljrx777cz8ksbjb1x9q") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-sys-types-0.6.0 (c (n "mc-sgx-dcap-ql-sys-types") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.6.0") (d #t) (k 1)))) (h "0haiwpm7akj9mmjdkzgnwq8ap62gk2zi2hh85yfxy6xn3p0xihdr") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-sys-types-0.6.1 (c (n "mc-sgx-dcap-ql-sys-types") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.6.1") (d #t) (k 1)))) (h "1hwv4r5vs1qz9rn1r8jcw1ryzbraz903sragri3l0f9q3v55fdqi") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-sys-types-0.7.0 (c (n "mc-sgx-dcap-ql-sys-types") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.0") (d #t) (k 1)))) (h "1ssa6jl78i535sv3h5x3sbm9ibghn0bq302sha0fan2a3mwvya13") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-sys-types-0.7.1 (c (n "mc-sgx-dcap-ql-sys-types") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.1") (d #t) (k 1)))) (h "1k7vhkb9g3z7rh00n5axf09s5xwrprmdp5frpg0ysjc4a2ncbxqr") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-sys-types-0.7.2 (c (n "mc-sgx-dcap-ql-sys-types") (v "0.7.2") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.2") (d #t) (k 1)))) (h "0cbm1aj9sw2ladk1cjz8vs0plmlkfx1xa78k72wyxv2019lsh20w") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-sys-types-0.7.3 (c (n "mc-sgx-dcap-ql-sys-types") (v "0.7.3") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.3") (d #t) (k 1)))) (h "038h5m20dv66x6hp2w441606n123pahih5bvjqahkggdv9v4j5cq") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-sys-types-0.7.4 (c (n "mc-sgx-dcap-ql-sys-types") (v "0.7.4") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.4") (d #t) (k 1)))) (h "0yjyvpsvngcplqc21b826cd4m8mxmzcx0fm6xisyrfla1f2gy0f2") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-sys-types-0.7.5 (c (n "mc-sgx-dcap-ql-sys-types") (v "0.7.5") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.7.5") (d #t) (k 1)))) (h "10vqxv13p5s4k1ryam7v73ghf5z2l850rhv0cb0h9f49pyzkxfbh") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-sys-types-0.8.0 (c (n "mc-sgx-dcap-ql-sys-types") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.8.0") (d #t) (k 1)))) (h "17rxyrqklqh89in0is50fbb990f7gia96mlzjhwvshqwg609mh96") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-sys-types-0.9.0 (c (n "mc-sgx-dcap-ql-sys-types") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.9.0") (d #t) (k 1)))) (h "0fsgrvvzna68pji2a7k81428ks17w6br2n1mq3dxnc03gk9ka4av") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-sys-types-0.10.0 (c (n "mc-sgx-dcap-ql-sys-types") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.10.0") (d #t) (k 1)))) (h "0r17ndsk90ryg899smz0ay6851vmccxb5bf4lxlbc2hj99b23dw2") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-sys-types-0.10.1 (c (n "mc-sgx-dcap-ql-sys-types") (v "0.10.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.10.1") (d #t) (k 1)))) (h "0myw9j9m9c6i60r380frh0vf6kjqbyas8q6i60db3cavs5vajcx6") (r "1.62.1")))

(define-public crate-mc-sgx-dcap-ql-sys-types-0.11.0 (c (n "mc-sgx-dcap-ql-sys-types") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 1)) (d (n "mc-sgx-core-build") (r "=0.11.0") (d #t) (k 1)))) (h "17kx3sk2nxah4l29vpyvdayqgflnw84b5jyjxrm562c0srpmli8p") (r "1.62.1")))

