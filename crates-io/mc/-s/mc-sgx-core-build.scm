(define-module (crates-io mc -s mc-sgx-core-build) #:use-module (crates-io))

(define-public crate-mc-sgx-core-build-0.1.0 (c (n "mc-sgx-core-build") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 0)))) (h "0lg1nkp5v9m7bc79kz5qi9mdsn7vmhn129ji350ycipwpaj8czjl") (r "1.62.1")))

(define-public crate-mc-sgx-core-build-0.2.0 (c (n "mc-sgx-core-build") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 0)))) (h "1hjyz0w6bdpjmpvpzi4vbsf3q0f9vbbv7g7f9n9zrlhn9r1scx4k") (r "1.62.1")))

(define-public crate-mc-sgx-core-build-0.2.1 (c (n "mc-sgx-core-build") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 0)))) (h "00jhxm6wpmdp9glb9kpzzjcq3y0g6pzb2apiphqjyap39iz4hk1b") (r "1.62.1")))

(define-public crate-mc-sgx-core-build-0.3.0 (c (n "mc-sgx-core-build") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 0)))) (h "1fdajs5kj2gb47gcz16zpyrb3hxax5m6b0cxzpf5h7x2aqbwg422") (r "1.62.1")))

(define-public crate-mc-sgx-core-build-0.4.0 (c (n "mc-sgx-core-build") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 0)))) (h "00gkmycmclr2ph98j3wdv5ly3zxvzpb63lyd5lk93g632pkcrg7d") (r "1.62.1")))

(define-public crate-mc-sgx-core-build-0.4.1 (c (n "mc-sgx-core-build") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 0)))) (h "0rprai3zb1an0axnmds6jwblmxa5apnlcr1s7v87lcq58gbh6m8k") (r "1.62.1")))

(define-public crate-mc-sgx-core-build-0.4.2 (c (n "mc-sgx-core-build") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 0)))) (h "1mwb0l40v2192hbaqzkbq93rc3y3ki03m7q3yg2s3i0gicswijim") (r "1.62.1")))

(define-public crate-mc-sgx-core-build-0.5.0 (c (n "mc-sgx-core-build") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 0)))) (h "1lf87vvn6m1hnaxg285nyghcpy1lsrcahai7ci9vp9f5ah0iyd2p") (r "1.62.1")))

(define-public crate-mc-sgx-core-build-0.6.0 (c (n "mc-sgx-core-build") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 0)))) (h "1prr6rzd3qnaf021k2vkd21xraa4d57qxyqx5bxc00sl05h41zps") (r "1.62.1")))

(define-public crate-mc-sgx-core-build-0.6.1 (c (n "mc-sgx-core-build") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 0)))) (h "00damgql1dfza4qvcskyzp5681lwlb1pg6nixhwdis5v06fahaz1") (r "1.62.1")))

(define-public crate-mc-sgx-core-build-0.7.0 (c (n "mc-sgx-core-build") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 0)))) (h "0l7f15irwnpywbdpna12wswidqqgsrd0icws936530ff4976b10p") (r "1.62.1")))

(define-public crate-mc-sgx-core-build-0.7.1 (c (n "mc-sgx-core-build") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 0)))) (h "1jljlg06mvdnypqv6y28y77mjlvqnxgipsb6yiy06qz8v02f1752") (r "1.62.1")))

(define-public crate-mc-sgx-core-build-0.7.2 (c (n "mc-sgx-core-build") (v "0.7.2") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 0)))) (h "0c9g81071kr3mw114nd7506qx8n2kqh4vk9vqayxc70rl6nrw62b") (r "1.62.1")))

(define-public crate-mc-sgx-core-build-0.7.3 (c (n "mc-sgx-core-build") (v "0.7.3") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 0)))) (h "04jyhakshvmn0nwdbg6dabr6rp1mkblg1v8a5wdardb0iph05095") (r "1.62.1")))

(define-public crate-mc-sgx-core-build-0.7.4 (c (n "mc-sgx-core-build") (v "0.7.4") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 0)))) (h "1b14gb85flpc245qkbbjql2km8ziqy9f2sa81x8xq0hzfmp6ij1z") (r "1.62.1")))

(define-public crate-mc-sgx-core-build-0.7.5 (c (n "mc-sgx-core-build") (v "0.7.5") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 0)))) (h "0qfiabwjhbblq5fbmgxi7q7zb4ds82b0vmi567yjmwk5d3wv8b0c") (r "1.62.1")))

(define-public crate-mc-sgx-core-build-0.8.0 (c (n "mc-sgx-core-build") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 0)))) (h "07fxif667p1r96gilcp1nc0hvg3qd8wjm1d3l7df7kcvdfxyaj5r") (r "1.62.1")))

(define-public crate-mc-sgx-core-build-0.9.0 (c (n "mc-sgx-core-build") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 0)))) (h "172nil2732wiwi6gmpg51mxigilqflsgdpps9hd717lmhymm4751") (r "1.62.1")))

(define-public crate-mc-sgx-core-build-0.10.0 (c (n "mc-sgx-core-build") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 0)))) (h "1gw6vjp03al40w5cjr93jg3ppc6mpqrb7apz13if11a52njvshg6") (r "1.62.1")))

(define-public crate-mc-sgx-core-build-0.10.1 (c (n "mc-sgx-core-build") (v "0.10.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 0)))) (h "14qh1i732ziqfsngvnxqhra83l4l5hrm1121d9y2b7i7sd36xwn2") (r "1.62.1")))

(define-public crate-mc-sgx-core-build-0.11.0 (c (n "mc-sgx-core-build") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 0)) (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 0)))) (h "0pcsi1ciq2rhaw4bm98wwrvqs6pj5ln73v5l4jcw7pvn1rbhlhll") (r "1.62.1")))

