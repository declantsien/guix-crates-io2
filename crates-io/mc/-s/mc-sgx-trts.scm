(define-module (crates-io mc -s mc-sgx-trts) #:use-module (crates-io))

(define-public crate-mc-sgx-trts-0.2.0 (c (n "mc-sgx-trts") (v "0.2.0") (d (list (d (n "mc-sgx-trts-sys") (r "=0.2.0") (d #t) (k 0)))) (h "10gw5fanm263390h7lxdmfvsi546gpnf3b0dprgmhmcds053n8nr") (r "1.62.1")))

(define-public crate-mc-sgx-trts-0.2.1 (c (n "mc-sgx-trts") (v "0.2.1") (d (list (d (n "mc-sgx-trts-sys") (r "=0.2.1") (d #t) (k 0)))) (h "1ajd5kzzhdv3dhcgnvkzqn9z8b22r22wmhmaa4q8k7jwr1z8p0h1") (r "1.62.1")))

(define-public crate-mc-sgx-trts-0.3.0 (c (n "mc-sgx-trts") (v "0.3.0") (d (list (d (n "mc-sgx-trts-sys") (r "=0.3.0") (d #t) (k 0)))) (h "1b4jiclphdwydxm2sf11a2zhij2fpbzqnvcnfaxsyk1q4v41is3i") (r "1.62.1")))

(define-public crate-mc-sgx-trts-0.4.0 (c (n "mc-sgx-trts") (v "0.4.0") (d (list (d (n "mc-sgx-trts-sys") (r "=0.4.0") (d #t) (k 0)))) (h "08c7ypn1msik7l5z17y4dkf47rcw0q6410x1ksn6gz5qs2xm4kmh") (r "1.62.1")))

(define-public crate-mc-sgx-trts-0.4.1 (c (n "mc-sgx-trts") (v "0.4.1") (d (list (d (n "mc-sgx-trts-sys") (r "=0.4.1") (d #t) (k 0)))) (h "02bxd629ymscahn2ff22isn1crbj46f4qyg2mn9frspx9zv0w4ls") (r "1.62.1")))

(define-public crate-mc-sgx-trts-0.4.2 (c (n "mc-sgx-trts") (v "0.4.2") (d (list (d (n "mc-sgx-trts-sys") (r "=0.4.2") (d #t) (k 0)))) (h "1azpi8jj07zdb5g4y1wxc2rcarir0d94d9hqbvsc7mnb5pyrrmmq") (r "1.62.1")))

(define-public crate-mc-sgx-trts-0.5.0 (c (n "mc-sgx-trts") (v "0.5.0") (d (list (d (n "mc-sgx-trts-sys") (r "=0.5.0") (d #t) (k 0)))) (h "1fc8mn0ldhifyb6nzg78n2wyc347x5k91fba6kh0h3h1dsjn3r50") (r "1.62.1")))

(define-public crate-mc-sgx-trts-0.6.0 (c (n "mc-sgx-trts") (v "0.6.0") (d (list (d (n "mc-sgx-trts-sys") (r "=0.6.0") (d #t) (k 0)))) (h "029wzbzrzpil0ssp70crgi3hlsp7lrpm17g97z9w488g07pi7gr3") (r "1.62.1")))

(define-public crate-mc-sgx-trts-0.6.1 (c (n "mc-sgx-trts") (v "0.6.1") (d (list (d (n "mc-sgx-trts-sys") (r "=0.6.1") (d #t) (k 0)))) (h "0bs9g5zrqhz9w5ibcim2wi5yfblddd7l6nfaykm6vwdhfpvms2cb") (r "1.62.1")))

(define-public crate-mc-sgx-trts-0.7.0 (c (n "mc-sgx-trts") (v "0.7.0") (d (list (d (n "mc-sgx-trts-sys") (r "=0.7.0") (d #t) (k 0)))) (h "0grx0r1l65sda488a3shal00psv9jbl65sbml2cvlfzpbw8cf39i") (r "1.62.1")))

(define-public crate-mc-sgx-trts-0.7.1 (c (n "mc-sgx-trts") (v "0.7.1") (d (list (d (n "mc-sgx-trts-sys") (r "=0.7.1") (d #t) (k 0)))) (h "05fy9q39y1052hml2w1gg6r0999khbc8m7aaqfhvin9agdf83n2w") (r "1.62.1")))

(define-public crate-mc-sgx-trts-0.7.2 (c (n "mc-sgx-trts") (v "0.7.2") (d (list (d (n "mc-sgx-trts-sys") (r "=0.7.2") (d #t) (k 0)))) (h "0hlhy43gz4llqxnrd8r2gyry5fjyhkpf3bzhspd60l94k3q93ssg") (r "1.62.1")))

(define-public crate-mc-sgx-trts-0.7.3 (c (n "mc-sgx-trts") (v "0.7.3") (d (list (d (n "mc-sgx-trts-sys") (r "=0.7.3") (d #t) (k 0)))) (h "1a4pp9f8x19igijlnfj6m3ffff3ivsp8jskrpjzm9hbfi9jvnk7d") (r "1.62.1")))

(define-public crate-mc-sgx-trts-0.7.4 (c (n "mc-sgx-trts") (v "0.7.4") (d (list (d (n "mc-sgx-trts-sys") (r "=0.7.4") (d #t) (k 0)))) (h "1h6mg1qmkcmvy95xhgsikd2c0kkj054bmf2rv2k9zm8pzmdd8ahk") (r "1.62.1")))

(define-public crate-mc-sgx-trts-0.7.5 (c (n "mc-sgx-trts") (v "0.7.5") (d (list (d (n "mc-sgx-trts-sys") (r "=0.7.5") (d #t) (k 0)))) (h "1z2vpi8rcan7001ykrhi7aj9561369jxqxy6ypgr53m8fr7g20gf") (r "1.62.1")))

(define-public crate-mc-sgx-trts-0.8.0 (c (n "mc-sgx-trts") (v "0.8.0") (d (list (d (n "mc-sgx-trts-sys") (r "=0.8.0") (d #t) (k 0)))) (h "06c2a16df1b8anwrim3rjv0w8g7zsbpmgazrwcbpsbwa63ikhbkv") (r "1.62.1")))

(define-public crate-mc-sgx-trts-0.9.0 (c (n "mc-sgx-trts") (v "0.9.0") (d (list (d (n "mc-sgx-trts-sys") (r "=0.9.0") (d #t) (k 0)))) (h "05y3zjjzrkvvcmb8d6cskg5gsjzkcslbpcn9vvbgqjws0h0swd89") (r "1.62.1")))

(define-public crate-mc-sgx-trts-0.10.0 (c (n "mc-sgx-trts") (v "0.10.0") (d (list (d (n "mc-sgx-trts-sys") (r "=0.10.0") (d #t) (k 0)))) (h "1fwrqk1rkmv7f4v4wh2ci1mgblylasv533ahcqj17cjjjjw1n51h") (r "1.62.1")))

(define-public crate-mc-sgx-trts-0.10.1 (c (n "mc-sgx-trts") (v "0.10.1") (d (list (d (n "mc-sgx-trts-sys") (r "=0.10.1") (d #t) (k 0)))) (h "06hv9mm6r23h65zp9bdhsl5kcjhsqs20jn43nnimvnxy4b1d5k8z") (r "1.62.1")))

(define-public crate-mc-sgx-trts-0.11.0 (c (n "mc-sgx-trts") (v "0.11.0") (d (list (d (n "mc-sgx-trts-sys") (r "=0.11.0") (d #t) (k 0)))) (h "0lgrdgin3mqr648yavs6faphaw7c41illj0fb9l705fd9a4f23qs") (r "1.62.1")))

