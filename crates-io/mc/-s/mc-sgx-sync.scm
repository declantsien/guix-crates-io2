(define-module (crates-io mc -s mc-sgx-sync) #:use-module (crates-io))

(define-public crate-mc-sgx-sync-0.1.0 (c (n "mc-sgx-sync") (v "0.1.0") (d (list (d (n "mc-sgx-panic-sys") (r "=0.1.0") (d #t) (k 0)) (d (n "mc-sgx-tstdc") (r "^0.4.1") (d #t) (k 0)))) (h "0zszz0l5zwnak2g5niwcij4y438dfgmq1kdiq1ad54a0z4rj7qd1") (r "1.62.1")))

