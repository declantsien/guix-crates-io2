(define-module (crates-io mc -n mc-network-data-types) #:use-module (crates-io))

(define-public crate-mc-network-data-types-0.1.0 (c (n "mc-network-data-types") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)))) (h "04zb7d6b69axwa69sza1gi69y6gjsh132mpakjkwa7cj2dhag5ky")))

(define-public crate-mc-network-data-types-0.1.1 (c (n "mc-network-data-types") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)))) (h "1r0fv2x64dcc47agi8ijqvjc9qznss8ilv4hrafyammh6s00rwy7")))

(define-public crate-mc-network-data-types-0.1.2 (c (n "mc-network-data-types") (v "0.1.2") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)))) (h "15fxgn79cxmckhkv3p3ddqwf4xaskcicxnbrmi1ayq7f62d6labv")))

(define-public crate-mc-network-data-types-0.1.3 (c (n "mc-network-data-types") (v "0.1.3") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)))) (h "1q6plv4qwmpg6m40yr0kcpbjaf92q6dw4ms9kxysg95rnp3qn42i")))

(define-public crate-mc-network-data-types-0.1.4 (c (n "mc-network-data-types") (v "0.1.4") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)))) (h "1zd1486bgy76h6qvih0q849vrwmhjipl8vj6i8jb50hxx8ibpm37")))

