(define-module (crates-io mc sl mcslock) #:use-module (crates-io))

(define-public crate-mcslock-0.1.0 (c (n "mcslock") (v "0.1.0") (d (list (d (n "lock_api") (r "^0.4") (o #t) (k 0)) (d (n "loom") (r "^0.7") (d #t) (t "cfg(loom)") (k 2)))) (h "0kv1rl48rcwqd39db2walzvx918xqx6yyxkrnd4jn889k1d27ps1") (f (quote (("yield") ("thread_local") ("barging")))) (s 2) (e (quote (("lock_api" "barging" "dep:lock_api")))) (r "1.65.0")))

(define-public crate-mcslock-0.1.1 (c (n "mcslock") (v "0.1.1") (d (list (d (n "lock_api") (r "^0.4") (o #t) (k 0)) (d (n "loom") (r "^0.7") (d #t) (t "cfg(loom)") (k 2)))) (h "1fpb35856csm5wpml2iwkcr393mjlgig7qxm45gnbpi715q1hic0") (f (quote (("yield") ("thread_local") ("barging")))) (s 2) (e (quote (("lock_api" "barging" "dep:lock_api")))) (r "1.65.0")))

(define-public crate-mcslock-0.1.2 (c (n "mcslock") (v "0.1.2") (d (list (d (n "lock_api") (r "^0.4") (o #t) (k 0)) (d (n "loom") (r "^0.7") (d #t) (t "cfg(loom)") (k 2)))) (h "0spafvkf33byb331wy9mlamhkiqfxnfp28sn1iqih1nyxyvzmssd") (f (quote (("yield") ("thread_local") ("barging")))) (s 2) (e (quote (("lock_api" "barging" "dep:lock_api")))) (r "1.65.0")))

(define-public crate-mcslock-0.2.0 (c (n "mcslock") (v "0.2.0") (d (list (d (n "lock_api") (r "^0.4") (o #t) (k 0)) (d (n "loom") (r "^0.7") (d #t) (t "cfg(loom)") (k 2)))) (h "1r957nrq1wvwz69vl4zdpvwfshsbp1plyab0bgw9zwmj40vjx4rv") (f (quote (("yield") ("thread_local") ("barging")))) (s 2) (e (quote (("lock_api" "barging" "dep:lock_api")))) (r "1.65.0")))

