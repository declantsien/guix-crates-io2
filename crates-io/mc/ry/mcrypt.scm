(define-module (crates-io mc ry mcrypt) #:use-module (crates-io))

(define-public crate-mcrypt-0.1.0 (c (n "mcrypt") (v "0.1.0") (d (list (d (n "git-version") (r "^0.3.5") (d #t) (k 0)) (d (n "build-data") (r "^0.1.3") (d #t) (k 1)))) (h "1ih3n95hgrwvz7404xhpdpnrx7gzc0m0a1sk6rwrgn40vy9qs64s") (y #t)))

(define-public crate-mcrypt-0.1.1 (c (n "mcrypt") (v "0.1.1") (d (list (d (n "build-data") (r "^0.1.3") (d #t) (k 1)))) (h "0xy1lqg7pwwac79r45r7lgpnz50fhkahlknkixd00yryrviim27l") (y #t)))

(define-public crate-mcrypt-0.1.2 (c (n "mcrypt") (v "0.1.2") (d (list (d (n "build-data") (r "^0.1.3") (d #t) (k 1)))) (h "1qf4008x2aaixyhnqmj9nyijafrl1j5xby298l9mx9k1rky18wp9") (y #t)))

(define-public crate-mcrypt-0.1.3 (c (n "mcrypt") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 1)) (d (n "vergen") (r "^7.5.0") (d #t) (k 1)))) (h "1f50wha45gkr63anbifp0hk88smzwi6zb92ng9zdfc27gx5vi73r") (y #t)))

(define-public crate-mcrypt-0.1.4 (c (n "mcrypt") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 1)) (d (n "vergen") (r "^7.5.0") (f (quote ("build" "rustc"))) (k 1)))) (h "03bvnhb1pd0ahrc0xpawbaj3br676yvhzkhyg4wfrkblxa9x2gqf")))

