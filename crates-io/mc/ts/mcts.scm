(define-module (crates-io mc ts mcts) #:use-module (crates-io))

(define-public crate-mcts-0.1.0 (c (n "mcts") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1icrsbd3wq6nv5iilgaphmg248r3z5cln568wcr1miqawszh8cjg")))

(define-public crate-mcts-0.2.0 (c (n "mcts") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0y17ilkbj7696z1bjkg276gh2l1mma14wdd22n4m2d7dh020wjng")))

(define-public crate-mcts-0.3.0 (c (n "mcts") (v "0.3.0") (d (list (d (n "crossbeam") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1qas2g3i9mfpwj41iqhcyrfzjzs2ax2mcd5xryh829w0iqh28bbx")))

