(define-module (crates-io mc -b mc-build-rs) #:use-module (crates-io))

(define-public crate-mc-build-rs-0.1.0 (c (n "mc-build-rs") (v "0.1.0") (d (list (d (n "cargo-emit") (r "^0.2.1") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (k 0)) (d (n "fluent-asserter") (r "^0.1.9") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "mockall") (r "^0.11.3") (d #t) (k 2)) (d (n "mockall_double") (r "^0.3.0") (d #t) (k 2)) (d (n "temp-env") (r "^0.3.2") (d #t) (k 2)) (d (n "url") (r "^2.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "10jjggq7yz5l73d3pvkszpxpnmyw3h6q9xf2zh654f7803w1v3iv") (r "1.68")))

