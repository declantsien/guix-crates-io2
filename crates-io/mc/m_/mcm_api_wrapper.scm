(define-module (crates-io mc m_ mcm_api_wrapper) #:use-module (crates-io))

(define-public crate-mcm_api_wrapper-1.0.0 (c (n "mcm_api_wrapper") (v "1.0.0") (d (list (d (n "derive-getters") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "serde_qs") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (d #t) (k 0)))) (h "0127byxlvzz1yw3maa2mw0nqjvq2v0bb3a6n3558w3g5fwv27ykq") (y #t)))

(define-public crate-mcm_api_wrapper-1.0.1 (c (n "mcm_api_wrapper") (v "1.0.1") (d (list (d (n "derive-getters") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "serde_qs") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (d #t) (k 0)))) (h "0fqiy2byjnpnhwii2rah7hkqds196sd3fy5rdrmp087vkch9g5jz") (y #t)))

