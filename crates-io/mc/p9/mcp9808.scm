(define-module (crates-io mc p9 mcp9808) #:use-module (crates-io))

(define-public crate-mcp9808-0.1.0 (c (n "mcp9808") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.9.0") (d #t) (k 0)) (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.1.2") (d #t) (k 0)))) (h "024fb9rx4zhgw3nbdm4ymmczl10chy7d4dmdknkk4ipgbs4z2i6h")))

(define-public crate-mcp9808-0.2.0 (c (n "mcp9808") (v "0.2.0") (d (list (d (n "bit_field") (r "^0.10.2") (d #t) (k 0)) (d (n "cast") (r "^0.3.0") (k 0)) (d (n "embedded-hal") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "1npnm4liffcldrqg71brw5k8gyihm17swrc24j1fm54cqz8k9kg5") (f (quote (("with_floating_point") ("no_floating_point") ("default" "with_floating_point"))))))

(define-public crate-mcp9808-0.3.0 (c (n "mcp9808") (v "0.3.0") (d (list (d (n "bit_field") (r "^0.10.2") (d #t) (k 0)) (d (n "cast") (r "^0.3.0") (k 0)) (d (n "embedded-hal") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "1l8xdsfmcrhqkggc3y42i3lpgijyb384w8kvzdlwkwkjnmr80af8") (f (quote (("with_floating_point") ("no_floating_point") ("default" "with_floating_point"))))))

(define-public crate-mcp9808-0.4.0 (c (n "mcp9808") (v "0.4.0") (d (list (d (n "bit_field") (r "^0.10.2") (d #t) (k 0)) (d (n "cast") (r "^0.3.0") (k 0)) (d (n "embedded-hal") (r "^1.0.0-rc.1") (d #t) (k 0)))) (h "13q684pjvas91bsd6x3dn1h02wymnzv8qw7h60b7ni052pdb41c9") (f (quote (("with_floating_point") ("no_floating_point") ("default" "with_floating_point"))))))

