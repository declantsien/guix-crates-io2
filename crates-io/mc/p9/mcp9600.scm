(define-module (crates-io mc p9 mcp9600) #:use-module (crates-io))

(define-public crate-mcp9600-0.1.0 (c (n "mcp9600") (v "0.1.0") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "bitvec") (r "~1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1w9s4pz58n257d3fc7r67bik23cghr3f4qj503cka26hwbjxcn37")))

(define-public crate-mcp9600-0.1.1 (c (n "mcp9600") (v "0.1.1") (d (list (d (n "bitvec") (r "~1.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1acbgwv1v2ba6z44xpggpcs1nbf90lr154vqs7yl9d7chwvis5d9")))

