(define-module (crates-io mc _s mc_service) #:use-module (crates-io))

(define-public crate-mc_service-0.1.0 (c (n "mc_service") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0sjafa9mim5xqig0wh94xcq4i91ym4bsdbp33692i3cd943wh6sq")))

