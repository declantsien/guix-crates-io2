(define-module (crates-io mc _s mc_sim) #:use-module (crates-io))

(define-public crate-mc_sim-0.1.0 (c (n "mc_sim") (v "0.1.0") (d (list (d (n "cached") (r "^0.22.0") (d #t) (k 0)) (d (n "csv") (r "^1.1.5") (d #t) (k 2)) (d (n "fraction") (r "^0.8.0") (d #t) (k 0)) (d (n "humantime") (r "^2.0.1") (d #t) (k 0)) (d (n "quick-error") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.118") (d #t) (k 0)) (d (n "statrs") (r "^0.13.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 2)))) (h "028d4wbs4439j7pk9rxsnds7qgjmpwp5ns9ban5488jc4mi5bx5d")))

