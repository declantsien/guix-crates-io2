(define-module (crates-io mc in mcinterface) #:use-module (crates-io))

(define-public crate-mcinterface-0.2.0 (c (n "mcinterface") (v "0.2.0") (h "18igx8nyiv86sd8cac117px3xycs7iwy0gfi4a3p3hix8knpwf74") (f (quote (("fmt") ("default" "fmt"))))))

(define-public crate-mcinterface-0.3.0 (c (n "mcinterface") (v "0.3.0") (h "0fddj7fw3n2rcf5jn51bgwn979bj8jizani80sqfdb0h09hbrwl5") (f (quote (("fmt") ("default" "fmt"))))))

(define-public crate-mcinterface-0.3.1 (c (n "mcinterface") (v "0.3.1") (h "0g7s2di29l8pv0hwqaf29jwci305qggimbpsgjgq38fsdllw872q") (f (quote (("fmt") ("default" "fmt"))))))

(define-public crate-mcinterface-0.3.2 (c (n "mcinterface") (v "0.3.2") (h "1ahqljg15hcj64j57qkmmsfzia0yka0hq0cfdzqbz8pn0i5hx19w") (f (quote (("fmt") ("default" "fmt"))))))

