(define-module (crates-io mc xn mcxn947-pac) #:use-module (crates-io))

(define-public crate-mcxn947-pac-0.0.1 (c (n "mcxn947-pac") (v "0.0.1") (d (list (d (n "cortex-m") (r ">=0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1zggcyz96ifmw1ryhxxza34j896vjvr7yybwbq3ijvg3g3hzljjc") (f (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-mcxn947-pac-0.0.2 (c (n "mcxn947-pac") (v "0.0.2") (d (list (d (n "cortex-m") (r ">=0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1k1b2qc79mpp7lp97i9hgapldpq39kapybjwwx41axwbj4an2i6r") (f (quote (("rt" "cortex-m-rt/device") ("default" "rt" "critical-section"))))))

(define-public crate-mcxn947-pac-0.0.3 (c (n "mcxn947-pac") (v "0.0.3") (d (list (d (n "cortex-m") (r ">=0.7.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r ">=0.7.3") (o #t) (d #t) (k 0)) (d (n "critical-section") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0724vrlsg4mv86xjiw1z7jkg170ww2vbs9rjn5h1vmvg7x9k651x") (f (quote (("rt" "cortex-m-rt/device") ("default" "rt" "critical-section"))))))

