(define-module (crates-io mc co mccolors-rust) #:use-module (crates-io))

(define-public crate-mccolors-rust-0.1.0 (c (n "mccolors-rust") (v "0.1.0") (h "1jpicmmxipnhkb88hf2vw024pgr7ynkdklxqqnsqsw2csl1r0m3n")))

(define-public crate-mccolors-rust-0.1.1 (c (n "mccolors-rust") (v "0.1.1") (h "1j5jfw0p54gd7r7mji9yj4dyk8737yi947asfyms9zlxnfpqc0cp")))

(define-public crate-mccolors-rust-0.1.2 (c (n "mccolors-rust") (v "0.1.2") (h "1dsni67r6s3gxh9p7p9r0brhbrnf4nfbsl0ba7nlcmdp7crwvd0f")))

(define-public crate-mccolors-rust-0.1.3 (c (n "mccolors-rust") (v "0.1.3") (h "1yif9sk0dpr92gmg7gkgy0jrvjl3r4aaxvs8fbqm359pw0ss8j0q")))

