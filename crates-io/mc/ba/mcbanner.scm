(define-module (crates-io mc ba mcbanner) #:use-module (crates-io))

(define-public crate-mcbanner-0.1.0 (c (n "mcbanner") (v "0.1.0") (d (list (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "rust-embed") (r "^8.2.0") (d #t) (k 0)))) (h "02ghdi7fsg774hirpyq7435m0w97qfyxjd27jwx60v1x0rzi5wci") (y #t)))

(define-public crate-mcbanner-0.1.1 (c (n "mcbanner") (v "0.1.1") (d (list (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "rust-embed") (r "^8.2.0") (d #t) (k 0)))) (h "1xygii4jpf3l0fxkx6hpjg5bcad77725vjysqb7ayxh9kxljg60y")))

