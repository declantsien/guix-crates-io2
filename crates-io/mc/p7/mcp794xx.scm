(define-module (crates-io mc p7 mcp794xx) #:use-module (crates-io))

(define-public crate-mcp794xx-0.1.0 (c (n "mcp794xx") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.4") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.2") (d #t) (k 2)) (d (n "rtcc") (r "^0.1") (d #t) (k 0)))) (h "1mbn1kfs2aarrc5l8nmak43pag5whmi636api9lak5kzskrdgf72")))

(define-public crate-mcp794xx-0.2.0 (c (n "mcp794xx") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "rtcc") (r "^0.2") (d #t) (k 0)))) (h "0r4md9ha01dhq0gx5gz0k5wjs2h6iai1nkd6p3bny76bz7bf448l")))

(define-public crate-mcp794xx-0.2.1 (c (n "mcp794xx") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "rtcc") (r "^0.2") (d #t) (k 0)))) (h "1gvchpmdk4sl8qaw09d4vj4vcgmq06iiw56p5ykcjfaavzj59hp7")))

(define-public crate-mcp794xx-0.3.0 (c (n "mcp794xx") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)) (d (n "rtcc") (r "^0.3") (d #t) (k 0)))) (h "0kvn4k6jr8bqrfv3p70bzwqjny2lc258lj6559w50ijjga3vagfz")))

