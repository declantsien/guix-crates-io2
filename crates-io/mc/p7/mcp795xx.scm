(define-module (crates-io mc p7 mcp795xx) #:use-module (crates-io))

(define-public crate-mcp795xx-0.1.0 (c (n "mcp795xx") (v "0.1.0") (d (list (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "0w8kshjfjxa55pn11rar1m6bmlg4vxxm4v0l92101w2xcfimrb3p")))

(define-public crate-mcp795xx-0.1.1 (c (n "mcp795xx") (v "0.1.1") (d (list (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1rcx3g05gfbg6a4y542dsnl0pm27nvcya8cx2rd0zsagc0fwbbdd")))

(define-public crate-mcp795xx-0.2.0 (c (n "mcp795xx") (v "0.2.0") (d (list (d (n "bitfield") (r "^0.13.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.10") (o #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "11a9v018bn2jbkdadal7p8qyc37s91k360clcxbr5gi997c7jjcl") (f (quote (("default" "chrono"))))))

