(define-module (crates-io mc l_ mcl_rust) #:use-module (crates-io))

(define-public crate-mcl_rust-0.0.1 (c (n "mcl_rust") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1rqf0kj11v4sf48vz45yn5gbgl21083d1jyrp00j7kb6jydrrddp")))

(define-public crate-mcl_rust-1.0.1 (c (n "mcl_rust") (v "1.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "09n28yikla5cmzxj1c3fzfgi44v8h2jbs5z1pjjrckgn6qqbm04j")))

(define-public crate-mcl_rust-1.0.2 (c (n "mcl_rust") (v "1.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "05h5v7fypa183f2r3lg6sajk5ixj85v71rvx7jn0hsigfwyjw1fl")))

