(define-module (crates-io mc l_ mcl_sched) #:use-module (crates-io))

(define-public crate-mcl_sched-0.1.0-rc (c (n "mcl_sched") (v "0.1.0-rc") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmcl-sys") (r "^0.1.2-rc") (d #t) (k 0)))) (h "1pp2i6ia9xbydh9251d7kqvpmg3qrfy5ayz0va68c9lnl4abldlh")))

(define-public crate-mcl_sched-0.1.0-rc2 (c (n "mcl_sched") (v "0.1.0-rc2") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmcl-sys") (r "^0.1.2-rc2") (d #t) (k 0)))) (h "0ikhbkw827cwbf94cqq61s3l6hw9gi21vw5fzds700mbpr4dxvaw") (f (quote (("shared_mem" "libmcl-sys/shared_mem") ("pocl_extensions" "shared_mem" "libmcl-sys/pocl_extensions") ("mcl_debug" "libmcl-sys/mcl_debug") ("docs-rs" "libmcl-sys/docs-rs"))))))

(define-public crate-mcl_sched-0.1.0-rc3 (c (n "mcl_sched") (v "0.1.0-rc3") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmcl-sys") (r "^0.1.2-rc3") (d #t) (k 0)))) (h "0r0w08spc1jns7cvd29rwxnj3mz9igk6sfm4hn7cl9nrki2vxk7y") (f (quote (("shared_mem" "libmcl-sys/shared_mem") ("pocl_extensions" "shared_mem" "libmcl-sys/pocl_extensions") ("mcl_debug" "libmcl-sys/mcl_debug") ("docs-rs" "libmcl-sys/docs-rs"))))))

(define-public crate-mcl_sched-0.1.0 (c (n "mcl_sched") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libmcl-sys") (r "^0.1.2") (d #t) (k 0)))) (h "04h1vnfyjnw8d1450gyqla7ih13hx82pan15w2pzfwibp1n1bp8z") (f (quote (("shared_mem" "libmcl-sys/shared_mem") ("pocl_extensions" "shared_mem" "libmcl-sys/pocl_extensions") ("mcl_debug" "libmcl-sys/mcl_debug") ("docs-rs" "libmcl-sys/docs-rs"))))))

