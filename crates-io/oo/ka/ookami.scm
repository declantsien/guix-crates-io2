(define-module (crates-io oo ka ookami) #:use-module (crates-io))

(define-public crate-ookami-0.1.0 (c (n "ookami") (v "0.1.0") (d (list (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)))) (h "0ryryqxqcg45dvbm11fws3a84bx1xl8kcyzpvcd74jsr1d4jpgvx")))

(define-public crate-ookami-0.2.0 (c (n "ookami") (v "0.2.0") (d (list (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)))) (h "1kha1fnqbxdpi0l5kgcicvnjqya23yy091xmmb169igl7k19p59x")))

