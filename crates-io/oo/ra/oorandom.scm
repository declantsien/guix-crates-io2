(define-module (crates-io oo ra oorandom) #:use-module (crates-io))

(define-public crate-oorandom-0.1.0 (c (n "oorandom") (v "0.1.0") (h "1f9ba3jv6599kgk27p4llkv617zkd0s1mpi10qg8l96p3fzbbwx3")))

(define-public crate-oorandom-9.3.0 (c (n "oorandom") (v "9.3.0") (d (list (d (n "randomize") (r "^3.0.0-rc.2") (d #t) (k 2)))) (h "14dchijxmrhk3fg0wgyrpc2cgrsd91lnka8iz5k1xcsd6sgvi552")))

(define-public crate-oorandom-9.3.1 (c (n "oorandom") (v "9.3.1") (d (list (d (n "randomize") (r "^3.0.0-rc.2") (d #t) (k 2)))) (h "1gbb9140p1006i19fp1cjys0mck404y99zcn47mkc12670n1v6y9")))

(define-public crate-oorandom-9.3.2 (c (n "oorandom") (v "9.3.2") (d (list (d (n "randomize") (r "^3.0.0-rc.2") (d #t) (k 2)))) (h "18i0dhznnlv8l3gyiqw1ih48sh3dkflidw7s7df0yssbk6snvlzn")))

(define-public crate-oorandom-9.3.3 (c (n "oorandom") (v "9.3.3") (d (list (d (n "randomize") (r "^3.0.0-rc.2") (d #t) (k 2)))) (h "1hk6qcp6k5sqx9zh19y5hjmxyxcagv7jhlipflzmdzcnpvzdm9if")))

(define-public crate-oorandom-9.3.4 (c (n "oorandom") (v "9.3.4") (d (list (d (n "randomize") (r "^3.0.0-rc.2") (d #t) (k 2)))) (h "1ql14gk13c3a4mcqd8zln3a8ndr8hpy12ar37fchzpw0qfi6h950")))

(define-public crate-oorandom-9.5.9 (c (n "oorandom") (v "9.5.9") (d (list (d (n "rand_core") (r "^0.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 2)) (d (n "randomize") (r "^3.0.0-rc.2") (d #t) (k 2)))) (h "1vhv65lwakvphkggagdlfaydrfzdmm29gbi9j5s75dqmx6772zgn")))

(define-public crate-oorandom-10.0.0 (c (n "oorandom") (v "10.0.0") (d (list (d (n "rand_core") (r "^0.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 2)) (d (n "randomize") (r "^3.0.0") (d #t) (k 2)))) (h "073w1117glzj50rqrnpzkyn3s63wmw6lq590rg52bqmj870wrfiv")))

(define-public crate-oorandom-10.0.1 (c (n "oorandom") (v "10.0.1") (d (list (d (n "rand_core") (r "^0.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 2)) (d (n "randomize") (r "^3.0.0") (d #t) (k 2)))) (h "1zzqj53v28mm69wrb25yjddjwz17w9dijb10wgsv2iiab8h4f1da")))

(define-public crate-oorandom-11.0.0 (c (n "oorandom") (v "11.0.0") (d (list (d (n "rand_core") (r "^0.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 2)) (d (n "randomize") (r "^3.0.0") (d #t) (k 2)))) (h "1n2108s1y1rsjf06x5kiylbyqb7wls97sfpz5fn2z2fyzhks8kdx")))

(define-public crate-oorandom-11.0.1 (c (n "oorandom") (v "11.0.1") (d (list (d (n "rand_core") (r "^0.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 2)) (d (n "random-fast-rng") (r "^0.1") (d #t) (k 2)) (d (n "randomize") (r "^3.0.0") (d #t) (k 2)))) (h "0ji0xjfqp8bgvzancjdjs2sphnhn4nppghqagcwxqz5lihr2mqq9")))

(define-public crate-oorandom-11.1.0 (c (n "oorandom") (v "11.1.0") (d (list (d (n "rand_core") (r "^0.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 2)) (d (n "random-fast-rng") (r "^0.1") (d #t) (k 2)) (d (n "randomize") (r "^3.0.0") (d #t) (k 2)))) (h "01clxfnz1zwg4maynvbgj09wlkj5m3c8kjqfrp3sqp59qb4wgkpb")))

(define-public crate-oorandom-11.1.1 (c (n "oorandom") (v "11.1.1") (d (list (d (n "rand_core") (r "^0.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 2)) (d (n "random-fast-rng") (r "^0.1") (d #t) (k 2)) (d (n "randomize") (r "^3.0.0") (d #t) (k 2)))) (h "0n33kc6bci43762knisghpi23alasj2ckqp43ccn0zrwqddk5bwl")))

(define-public crate-oorandom-11.1.2 (c (n "oorandom") (v "11.1.2") (d (list (d (n "rand_core") (r "^0.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 2)) (d (n "random-fast-rng") (r "^0.1") (d #t) (k 2)) (d (n "randomize") (r "^3.0.0") (d #t) (k 2)))) (h "0b0bmi76bfywdwllnrpc2ksmvcw05ykqbnz4jbm0i811h2ywww51")))

(define-public crate-oorandom-11.1.3 (c (n "oorandom") (v "11.1.3") (d (list (d (n "rand_core") (r "^0.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 2)) (d (n "random-fast-rng") (r "^0.1") (d #t) (k 2)) (d (n "randomize") (r "^3.0.0") (d #t) (k 2)))) (h "0xdm4vd89aiwnrk1xjwzklnchjqvib4klcihlc2bsd4x50mbrc8a")))

