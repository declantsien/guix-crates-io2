(define-module (crates-io oo p- oop-macro) #:use-module (crates-io))

(define-public crate-oop-macro-0.0.0-alpha.0 (c (n "oop-macro") (v "0.0.0-alpha.0") (h "1bs7mnmwdkc7wskhyrw7ik7psd55rw5b2vhh90rwgac0r8vlvf32")))

(define-public crate-oop-macro-0.0.1 (c (n "oop-macro") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("default" "full"))) (d #t) (k 0)))) (h "1krxqk3kvzych4gnsd2sjd0c4m6g3sk5z4rpgphy3yli69i1z4z1")))

(define-public crate-oop-macro-0.0.2 (c (n "oop-macro") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("default" "full"))) (d #t) (k 0)))) (h "115z26zqqkjk0c03cgs0mcnafrq8cijv1xqnjwm8bgl8rwyh94z4")))

