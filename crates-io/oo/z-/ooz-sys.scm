(define-module (crates-io oo z- ooz-sys) #:use-module (crates-io))

(define-public crate-ooz-sys-0.1.0 (c (n "ooz-sys") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (d #t) (k 1)))) (h "13ijvvk1xm6hw6pb0kvlsgrsapddm9kj8p3g7fz2d0r0hgfsr7qq") (y #t)))

(define-public crate-ooz-sys-0.2.0 (c (n "ooz-sys") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 1)) (d (n "cc") (r "^1.0.72") (d #t) (k 1)))) (h "0247ypkd04b6zr2bz71f7hafdnnrwch8sabflr89gj7zzpna7843") (y #t)))

