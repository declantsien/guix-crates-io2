(define-module (crates-io oo tp ootp) #:use-module (crates-io))

(define-public crate-ootp-0.0.1 (c (n "ootp") (v "0.0.1") (d (list (d (n "hmac-sha1") (r "~0.1.3") (d #t) (k 0)))) (h "1k7wah3102pw7mdjh7zm6ilm15k4yzhda93v33jaiwqd94bp2pl6")))

(define-public crate-ootp-0.0.2 (c (n "ootp") (v "0.0.2") (d (list (d (n "base32") (r "^0.4") (d #t) (k 2)) (d (n "hmac-sha1") (r "^0.1") (d #t) (k 0)))) (h "1shjrhqwl5sks27ha906625j4msdlwqd63hvh9ysw11w6vn52l45")))

(define-public crate-ootp-0.0.3 (c (n "ootp") (v "0.0.3") (d (list (d (n "base32") (r "^0.4") (d #t) (k 2)) (d (n "hmac-sha1") (r "^0.1") (d #t) (k 0)))) (h "14izl4285ldknb2aprr0h846kzm3xyqfa9gxwn374569y7xnn39j")))

(define-public crate-ootp-0.0.4 (c (n "ootp") (v "0.0.4") (d (list (d (n "hmac-sha1") (r "^0.1") (d #t) (k 0)))) (h "0iniasbhbspkkkl7qlbpv4kpzh5chgj1mx58kwv4xxc0qpzyfa7f")))

(define-public crate-ootp-0.0.5 (c (n "ootp") (v "0.0.5") (d (list (d (n "hmac-sha1") (r "^0.1") (d #t) (k 0)))) (h "1iwq38vyw7gc0b99303kj3c7d4j6lcsag5wmh02v1n96rvgi7wpa")))

(define-public crate-ootp-0.0.6 (c (n "ootp") (v "0.0.6") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hmac-sha1") (r "^0.1") (d #t) (k 0)))) (h "01l9q6la7p8jqybx0wvyc1qihf3gmg7slxg4awhh5axpsbarf67a")))

(define-public crate-ootp-0.0.7 (c (n "ootp") (v "0.0.7") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hmac-sha") (r "^0.4") (d #t) (k 0)))) (h "0dd6rzww96qk5qvn4l3gwk3h0b7z5zxs456bava4zz420zq0x8vp")))

(define-public crate-ootp-0.1.0 (c (n "ootp") (v "0.1.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "hmac-sha") (r "^0.4") (d #t) (k 0)))) (h "18q85l2ss43yr97w3avdw7cafc7lj8if2gvdkypr4zdpyzldagkm")))

(define-public crate-ootp-0.1.1 (c (n "ootp") (v "0.1.1") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hmac-sha") (r "^0.5.0") (d #t) (k 0)))) (h "0946y0zndzj2bvcnj5j9gy5m631vsn25871y0yj81yqz3aw9lsj5")))

