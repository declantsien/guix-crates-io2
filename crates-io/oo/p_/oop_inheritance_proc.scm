(define-module (crates-io oo p_ oop_inheritance_proc) #:use-module (crates-io))

(define-public crate-oop_inheritance_proc-1.0.0 (c (n "oop_inheritance_proc") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1l6qf2zrwgg6am5pmhfl99hlr5ag20sha812p0ydzxascqdpw2cc")))

