(define-module (crates-io oo ps oops) #:use-module (crates-io))

(define-public crate-oops-0.1.0 (c (n "oops") (v "0.1.0") (h "1a34z02l5ihpib8dc83jlx3sd6fhrn084alc9a7syab4rfvdg2p2")))

(define-public crate-oops-0.2.0 (c (n "oops") (v "0.2.0") (h "01ia9phvja4fkr9dbkb5q04kdzln7pf2bxrkfsnr6rdld4jv01ba")))

