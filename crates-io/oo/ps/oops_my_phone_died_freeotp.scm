(define-module (crates-io oo ps oops_my_phone_died_freeotp) #:use-module (crates-io))

(define-public crate-oops_my_phone_died_freeotp-1.0.0 (c (n "oops_my_phone_died_freeotp") (v "1.0.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "totp-lite") (r "^1.0.3") (d #t) (k 0)))) (h "0kibnx1w0v6az3pssby7n4ay1s5kbgj8db5713ik4a79icb1xwv3")))

(define-public crate-oops_my_phone_died_freeotp-1.0.1 (c (n "oops_my_phone_died_freeotp") (v "1.0.1") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "totp-lite") (r "^1.0.3") (d #t) (k 0)))) (h "1amsnzl2p2zsy8w0v12p67c5yhaq22qjab0hy9lhf4j3qq5ayl23")))

