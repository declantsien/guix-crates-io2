(define-module (crates-io oo zi oozie) #:use-module (crates-io))

(define-public crate-oozie-0.1.0 (c (n "oozie") (v "0.1.0") (d (list (d (n "csv") (r "^0.15.0") (d #t) (k 0)))) (h "10zs36ajj3sq2v6vbm4msjgj429yrv6i63g21zb5ldfi0a17r1zl")))

(define-public crate-oozie-0.1.1 (c (n "oozie") (v "0.1.1") (d (list (d (n "csv") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.10.0") (d #t) (k 0)))) (h "129m0qfpvviq2kslfwvnidghczszmqffw7k7wgrw535d0kdwmc55")))

(define-public crate-oozie-0.1.2 (c (n "oozie") (v "0.1.2") (d (list (d (n "csv") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "0vs6g5kxpraj755lrrgj0l3pgvz6w977xrj93h7r92sbrfspxwlh")))

