(define-module (crates-io oo fs oofs) #:use-module (crates-io))

(define-public crate-oofs-0.1.2 (c (n "oofs") (v "0.1.2") (d (list (d (n "oofs_derive") (r "=0.1.2") (d #t) (k 0)))) (h "0b5jgap58fivm8lhin06znhlbk06m5hp6chiga532jwdcal12zx2") (f (quote (("display_owned_release") ("display_owned_disabled"))))))

(define-public crate-oofs-0.1.3 (c (n "oofs") (v "0.1.3") (d (list (d (n "oofs_derive") (r "=0.1.3") (d #t) (k 0)))) (h "00idr81falqnsnbvqhfr94zc9kk2yjnq5z2qa3n5px9x3fky9k3w") (f (quote (("display_owned_release") ("display_owned_disabled"))))))

(define-public crate-oofs-0.1.4 (c (n "oofs") (v "0.1.4") (d (list (d (n "oofs_derive") (r "=0.1.4") (d #t) (k 0)))) (h "0vl1q5klp78mnz3k3gklrvyl387aijb8bzmpniigrh5k327iq65k") (f (quote (("display_owned_release") ("display_owned_disabled"))))))

(define-public crate-oofs-0.1.5 (c (n "oofs") (v "0.1.5") (d (list (d (n "oofs_derive") (r "=0.1.4") (d #t) (k 0)))) (h "0zbn2k6ha8kgl28nbvpq1p11sgyq09rybkq5p2a34pmxqrvng2x9") (f (quote (("display_owned_release") ("display_owned_disabled"))))))

(define-public crate-oofs-0.1.6 (c (n "oofs") (v "0.1.6") (d (list (d (n "oofs_derive") (r "^0.1.5") (d #t) (k 0)))) (h "0nxd4ycsis5yjlnkqh2chympcafc5nsrykzgyhyij68yw9apf8ll") (f (quote (("display_owned_release") ("display_owned_disabled"))))))

(define-public crate-oofs-0.1.7 (c (n "oofs") (v "0.1.7") (d (list (d (n "oofs_derive") (r "=0.1.6") (d #t) (k 0)))) (h "0jqpqps1vxdjp1nyk6abx3s8nvfw7h2yv96mb4a51dq5sycjbb03") (f (quote (("display_owned_release") ("display_owned_disabled"))))))

(define-public crate-oofs-0.1.8 (c (n "oofs") (v "0.1.8") (d (list (d (n "oofs_derive") (r "=0.1.8") (d #t) (k 0)))) (h "1z2abp4rxbjw0pjm9zpfqpcmmzp9fp5bpzmbb7fkca6grqd554lh") (f (quote (("location") ("default" "location") ("debug_strategy_full") ("debug_strategy_disabled"))))))

(define-public crate-oofs-0.1.9 (c (n "oofs") (v "0.1.9") (d (list (d (n "oofs_derive") (r "=0.1.9") (d #t) (k 0)))) (h "13v5s42c7pv8iyk3n21g1mi01j4n7gmqjbfvhflsdjr6l41kh4k0") (f (quote (("location") ("default" "location") ("debug_strategy_full") ("debug_strategy_disabled"))))))

(define-public crate-oofs-0.1.11 (c (n "oofs") (v "0.1.11") (d (list (d (n "oofs_derive") (r "=0.1.11") (d #t) (k 0)))) (h "0dcpm9b9f8krg1zkph13hw527sp5cw1p59crzcwndx4i0grnhlqh") (f (quote (("location") ("default" "location") ("debug_strategy_full") ("debug_strategy_disabled"))))))

(define-public crate-oofs-0.1.13 (c (n "oofs") (v "0.1.13") (d (list (d (n "oofs_derive") (r "=0.1.13") (d #t) (k 0)))) (h "1h751i54jnnl13a15amx2pbfd5cgwr5nhm3yyp80cav6b2cjzr70") (f (quote (("location") ("default" "location") ("debug_strategy_full") ("debug_strategy_disabled"))))))

(define-public crate-oofs-0.1.14 (c (n "oofs") (v "0.1.14") (d (list (d (n "oofs_derive") (r "=0.1.14") (d #t) (k 0)))) (h "01rqjcb186h8v960h9bcfyfwr6g0zhj2yhdgw7lj4cp7fhddn30n") (f (quote (("location") ("default" "location") ("debug_strategy_full") ("debug_strategy_disabled"))))))

(define-public crate-oofs-0.1.15 (c (n "oofs") (v "0.1.15") (d (list (d (n "oofs_derive") (r "=0.1.15") (d #t) (k 0)))) (h "114b4jindb511cxl9vm3aqw9piirviiw4jghvjgnc8zqcpgvdjn2") (f (quote (("location") ("default" "location") ("debug_strategy_full") ("debug_strategy_disabled"))))))

(define-public crate-oofs-0.2.0 (c (n "oofs") (v "0.2.0") (d (list (d (n "oofs_derive") (r "=0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18b5c6zsh6m8g5896ayfwrxpzm85gwc542wmq4mqgikwwxm47wqy") (f (quote (("location") ("default" "location") ("debug_non_copyable_full") ("debug_non_copyable_disabled"))))))

(define-public crate-oofs-0.2.2 (c (n "oofs") (v "0.2.2") (d (list (d (n "oofs_derive") (r "=0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0h8g85c81rkqwqp8j5kl048k308q9liwsk64yar8rbll7qik3n67") (f (quote (("location") ("default" "location") ("debug_non_copyable_full") ("debug_non_copyable_disabled"))))))

(define-public crate-oofs-0.2.3 (c (n "oofs") (v "0.2.3") (d (list (d (n "oofs_derive") (r "=0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fn4l89cwpdng8jbj1qs1jwn0nwsaks8bkbbffwiq7lx5hlv3fmm") (f (quote (("location") ("default" "location") ("debug_non_copyable_full") ("debug_non_copyable_disabled"))))))

