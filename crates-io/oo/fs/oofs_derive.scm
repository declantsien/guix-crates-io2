(define-module (crates-io oo fs oofs_derive) #:use-module (crates-io))

(define-public crate-oofs_derive-0.1.2 (c (n "oofs_derive") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0zcjl1l3ggblzsy32y1i2i69w2crm2njq254ildng51nqx6hdi3m")))

(define-public crate-oofs_derive-0.1.3 (c (n "oofs_derive") (v "0.1.3") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1s7ij1hirp7pgq12686y0s3vzcizplcdqhvr5kirjdv8b5h95m14")))

(define-public crate-oofs_derive-0.1.4 (c (n "oofs_derive") (v "0.1.4") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "02ysaphp9kh85v7c3gnbryxxd7mi1ra9als8zz70nizj7rmrklps")))

(define-public crate-oofs_derive-0.1.6 (c (n "oofs_derive") (v "0.1.6") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "1v5lbsirif2xdmqz11jz319iisgm9k2a31403ifg8qvnndj4pkal")))

(define-public crate-oofs_derive-0.1.8 (c (n "oofs_derive") (v "0.1.8") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0czk57bi84s1fkp5dqjkfh1agr3zavkr903wr925vgi0b2qahwk0")))

(define-public crate-oofs_derive-0.1.9 (c (n "oofs_derive") (v "0.1.9") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0n0m6rzdss7kvd65bka70v7hsqzfq2lgcw8fg1l546ya8dmr6gqy")))

(define-public crate-oofs_derive-0.1.11 (c (n "oofs_derive") (v "0.1.11") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "155bjzh6pp5zigzsn6zjn1daypya2gaxy9vcyiscgvgqhyz09yhd")))

(define-public crate-oofs_derive-0.1.13 (c (n "oofs_derive") (v "0.1.13") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17y337ahi1ffnzcfykg5xmmpcd9ipr4agv7dgzyg1p50m1kqmbs2")))

(define-public crate-oofs_derive-0.1.14 (c (n "oofs_derive") (v "0.1.14") (d (list (d (n "oofs") (r "=0.1.13") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fv4mgmzv37vji9yxz7a6h5c81q3640qxh33nfdxdhl2328ag1xn")))

(define-public crate-oofs_derive-0.1.15 (c (n "oofs_derive") (v "0.1.15") (d (list (d (n "oofs") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14ppqpmvl68ilpz6f7w5g6d1miq2fja5rk8vfsys34qahy1hpmsm")))

(define-public crate-oofs_derive-0.2.0 (c (n "oofs_derive") (v "0.2.0") (d (list (d (n "oofs") (r "^0.1") (d #t) (k 2)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1aqk2sgkw72jpqyn00habpmi8rp6qjrizf7yzz9vnl6nrj2a5527")))

(define-public crate-oofs_derive-0.2.2 (c (n "oofs_derive") (v "0.2.2") (d (list (d (n "oofs") (r "^0.1") (d #t) (k 2)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0dhld0zn7lp3jl1bk5jki3ksy0yfrzjkbqnswivsk5rmx6b24mb6")))

(define-public crate-oofs_derive-0.2.3 (c (n "oofs_derive") (v "0.2.3") (d (list (d (n "oofs") (r "^0.2") (d #t) (k 2)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1wifn7f69lls0d0brqcywkc0jn0swjdn0ljr9m1q1xj99gyakbg1")))

