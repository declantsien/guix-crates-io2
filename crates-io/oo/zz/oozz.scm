(define-module (crates-io oo zz oozz) #:use-module (crates-io))

(define-public crate-oozz-0.3.1 (c (n "oozz") (v "0.3.1") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.9") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "167bgqwdlfklzib9wphkj2qgswpqmiv7qlfpkb4mpncfrp9brihi")))

(define-public crate-oozz-0.3.2 (c (n "oozz") (v "0.3.2") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.9") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "06gd4lsyv17cj5jql0gp9bg6s11k6p2dp44f09q8vmhl2mdyi1x4")))

(define-public crate-oozz-0.3.3 (c (n "oozz") (v "0.3.3") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.9") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "0yhjkpp7la3b5980qi3qar39pnqns0kzfsxjilcs87ldk64gpsya")))

(define-public crate-oozz-0.3.4 (c (n "oozz") (v "0.3.4") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.9") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "13k111bcxkz55psv00hq5jy4s6wgr6gvx5kijl4bb6i6s0fjfwna")))

(define-public crate-oozz-0.3.5 (c (n "oozz") (v "0.3.5") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.9") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "1pc8vcgcgdphqcbmpadyl3g2j8l9ja7lmlfxvr1mvv54xa3pwgwq")))

(define-public crate-oozz-0.4.0 (c (n "oozz") (v "0.4.0") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.9") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "0w40z615ajwrv3nm64n7zjz8s44fa0x4617xfw1g18f4m7mb9iaf")))

(define-public crate-oozz-0.4.1 (c (n "oozz") (v "0.4.1") (d (list (d (n "clap") (r "^2.26.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.9") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)))) (h "1asyyfilgpj1j1gavisdjdbyyprk4hzbfhxxjlmwc5xgr7l0bd5h")))

