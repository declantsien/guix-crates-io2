(define-module (crates-io oo dl oodle-sys) #:use-module (crates-io))

(define-public crate-oodle-sys-0.1.0 (c (n "oodle-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69") (f (quote ("runtime" "which-rustfmt"))) (o #t) (k 1)))) (h "0r57qg0sg5gazwk5h12prd6wcgjlr9rcvk1ffgc2m7ldhwpa3a2z") (l "oo2corelinux64")))

(define-public crate-oodle-sys-0.2.0 (c (n "oodle-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69") (f (quote ("runtime" "which-rustfmt"))) (o #t) (k 1)))) (h "1k5qg8a8fdmwm2f4wr6qn7g62kh00d4a117ysx844iv35s0mq95p") (l "oo2corelinux64")))

