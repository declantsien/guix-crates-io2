(define-module (crates-io oo dl oodle-safe) #:use-module (crates-io))

(define-public crate-oodle-safe-0.1.0 (c (n "oodle-safe") (v "0.1.0") (d (list (d (n "oodle-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0pq1c6p175vdc3m6rg2480c6rkfmdpp4gmgvgkapgg95q29vrk1d") (f (quote (("bindgen" "oodle-sys/bindgen"))))))

(define-public crate-oodle-safe-0.2.0 (c (n "oodle-safe") (v "0.2.0") (d (list (d (n "oodle-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1fb85gkl05zf5hn6ljrd6afxh1rjslp56svbrrljjm01rzazsk92") (f (quote (("bindgen" "oodle-sys/bindgen"))))))

