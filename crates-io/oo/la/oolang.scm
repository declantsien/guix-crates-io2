(define-module (crates-io oo la oolang) #:use-module (crates-io))

(define-public crate-OOLANG-0.1.0 (c (n "OOLANG") (v "0.1.0") (d (list (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "1hpsiklwl6ir54jcbipbpk6sdz5hqpvvl4ff45rik53pxwh1j8iv") (y #t) (r "1.58")))

(define-public crate-OOLANG-0.1.1 (c (n "OOLANG") (v "0.1.1") (d (list (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "1nf6cnn8bkp46fz4mxc0fhbz0bjs1m06alrg2rm9p1xr5acvsfn7") (r "1.58")))

