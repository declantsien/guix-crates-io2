(define-module (crates-io oo hi oohid) #:use-module (crates-io))

(define-public crate-oohid-0.3.0 (c (n "oohid") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4" "macro-diagnostics"))) (d #t) (k 0)))) (h "151s6y9klnz5rcv894spjq7v0pmy99zr9im80qdxhmbfckpcbz1x")))

(define-public crate-oohid-0.3.1 (c (n "oohid") (v "0.3.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4" "macro-diagnostics"))) (d #t) (k 0)))) (h "1yf30f9hbp92aa1lc6ysib7wlcnykkqrs1zr4cjxrkdyd8y1pnb1")))

(define-public crate-oohid-0.3.2 (c (n "oohid") (v "0.3.2") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4" "macro-diagnostics"))) (d #t) (k 0)))) (h "13yjc4jcmiarvx2r2c3c7bknwkzhps82n1lbb68gd2wgfz45vjqq")))

(define-public crate-oohid-0.3.3 (c (n "oohid") (v "0.3.3") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4" "macro-diagnostics"))) (d #t) (k 0)))) (h "0nwqbfcz68dyyc9yw4a3bb8ij2wcq8kwpgqqlxc4jcam2hxymdkf")))

(define-public crate-oohid-0.3.4 (c (n "oohid") (v "0.3.4") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4" "macro-diagnostics"))) (d #t) (k 0)))) (h "14az9j29f0aa17ysb8k4ndylkqz7rlhxvb9r9qk8hh3nbqg1mx50")))

(define-public crate-oohid-0.3.5 (c (n "oohid") (v "0.3.5") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4" "macro-diagnostics"))) (d #t) (k 0)))) (h "0c6jg3qyr6gidwxdkmlgg26idq5dc71h8pd553wiqz2n2h2ay0sd")))

(define-public crate-oohid-0.3.6 (c (n "oohid") (v "0.3.6") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4" "macro-diagnostics"))) (d #t) (k 0)))) (h "1c10niivhh6l7zzz166zg2s8133s68wfy4hgb7h5zhp164g5x3fc")))

(define-public crate-oohid-0.3.7 (c (n "oohid") (v "0.3.7") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4" "macro-diagnostics"))) (d #t) (k 0)))) (h "0b200apw04a4gkr9fak3flihb0hqag7cc9r5gcwk3fwl8xcqszbn")))

(define-public crate-oohid-0.3.8 (c (n "oohid") (v "0.3.8") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4" "macro-diagnostics"))) (d #t) (k 0)))) (h "177nfqh12rknqv0fllgii6f5hxavhnqfik4w457hjly7r3p5k5j3")))

(define-public crate-oohid-0.3.9 (c (n "oohid") (v "0.3.9") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4" "macro-diagnostics"))) (d #t) (k 0)))) (h "0ivpfca7pigpcnyiqk7h0qmh0pw1dfrz5pgz5k7cxkq8kdrzhzlr")))

(define-public crate-oohid-0.3.10 (c (n "oohid") (v "0.3.10") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1xwxfjchdw022735n3icjmmhklx10g9yb7qdgvs222h5l98zgzjq")))

(define-public crate-oohid-0.3.11 (c (n "oohid") (v "0.3.11") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "17lpl1z8b3ad2406la9qs7z13hpjqcxzi3qziqvi76d5kfh6796q")))

