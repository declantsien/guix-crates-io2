(define-module (crates-io oo zl oozle) #:use-module (crates-io))

(define-public crate-oozle-0.1.0 (c (n "oozle") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0lv64kwlr6f0529lxj5vzwpygn4n79q6ik6yw5bg00ihgqxmxxw1")))

(define-public crate-oozle-0.1.1 (c (n "oozle") (v "0.1.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1sh9yyfgmixd1irybk2r3dal16wbp0a598i4z9v4crlcwk2jgd2n")))

(define-public crate-oozle-0.1.2 (c (n "oozle") (v "0.1.2") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1vyryb8m0nqp2nn78byvw44msjx9s1gz2jknn3yfxwxbrmyn6x7k")))

(define-public crate-oozle-0.1.3 (c (n "oozle") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1p7nib2hnb52f8i66d924w8kw4js31d40sy6n9plgj5cxs3jrpb4")))

(define-public crate-oozle-0.1.4 (c (n "oozle") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "00nrvkl6zi7v6hdbvcyskbybfyaqkm6lqlw30p88wvr5nbzznv7m")))

(define-public crate-oozle-0.1.5 (c (n "oozle") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0bi81n53s0spyaspjgighxi23q44n6bvdbiv4zdcjmid6a5fayzw")))

