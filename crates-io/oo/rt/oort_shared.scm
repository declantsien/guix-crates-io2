(define-module (crates-io oo rt oort_shared) #:use-module (crates-io))

(define-public crate-oort_shared-0.1.0 (c (n "oort_shared") (v "0.1.0") (h "1s2f6cp2hzzb41z63yzkyldy4wf8bshcab3bs8z8y3zxymcgibwh")))

(define-public crate-oort_shared-0.2.0 (c (n "oort_shared") (v "0.2.0") (h "0nkyp2p3c29xv8snlzlx7zj1iadxxnjrjvzbf3m7vgc46wi1ia9f")))

(define-public crate-oort_shared-0.2.1 (c (n "oort_shared") (v "0.2.1") (h "06mp6k8ppdr5k00nbbw89rrqmib9kjq4bfrla5mb2s38pf2w9vci")))

(define-public crate-oort_shared-0.2.6 (c (n "oort_shared") (v "0.2.6") (h "1wdq9srj0f64hpif5kz78p6k1bsjb624gi38lc7pkgjq4a9878gs")))

(define-public crate-oort_shared-0.3.0 (c (n "oort_shared") (v "0.3.0") (h "13pxvlxniq2slpknd2m4jpkdkkkmxy1b8n4qh9jz86zzq4lxcqrr")))

(define-public crate-oort_shared-0.3.1 (c (n "oort_shared") (v "0.3.1") (h "0kbm5bg15gixj0g7vzkw7xiicp3bgg4gp469xp47nq89aspcys2q")))

(define-public crate-oort_shared-0.4.0 (c (n "oort_shared") (v "0.4.0") (h "0rbyqdcn3bsffmvk0f52mxixc7bnz6qwzz2gix22jghkcry9agrj")))

(define-public crate-oort_shared-0.4.1 (c (n "oort_shared") (v "0.4.1") (h "1bg6ll7s3alzn99lyqlzc0iw82x7y2gp1dbg5fapdlncjsvzhi6g")))

(define-public crate-oort_shared-0.4.2 (c (n "oort_shared") (v "0.4.2") (h "1h3fhmqfirxr2kp53kjhsxkm9ilg46yvhibq566r7494ikcp2x2v")))

(define-public crate-oort_shared-0.4.3 (c (n "oort_shared") (v "0.4.3") (h "1q1srx96sgfp21kmgm08a78z0psfrdyindd7abvmg721dmwrk461")))

(define-public crate-oort_shared-0.5.0 (c (n "oort_shared") (v "0.5.0") (h "13w9ack1csrssm081gq9dnqirzql4ggnz8bxzgcwwzp814hhl55r")))

(define-public crate-oort_shared-0.5.1 (c (n "oort_shared") (v "0.5.1") (h "0kc5wdql99l0vbkm8fpp3980g08mf57pb84wfim5y5vb1v1q304s")))

(define-public crate-oort_shared-0.5.2 (c (n "oort_shared") (v "0.5.2") (h "0p0wd8y0zvbi157w0zgsp30acr7kcas13iq7zsbb80v0mj5h41bq")))

(define-public crate-oort_shared-0.6.0 (c (n "oort_shared") (v "0.6.0") (h "05cp95bg3zdkwy27q5sqy3ykq24ycmq2h14rg3yxwcf8cgic34r3")))

(define-public crate-oort_shared-0.6.1 (c (n "oort_shared") (v "0.6.1") (h "0cavaps083v7rb7sq517hh5dy61qj8ra0a2pl62dff30y5mlx2ha")))

