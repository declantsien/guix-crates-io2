(define-module (crates-io oo rt oort-vm) #:use-module (crates-io))

(define-public crate-oort-vm-0.1.0 (c (n "oort-vm") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "logos") (r "^0.14.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)))) (h "1ycgv1qffksnpb7zhl7syzp0fa8sj10948aggz5kxhgb1ll5z9lb")))

(define-public crate-oort-vm-1.0.0 (c (n "oort-vm") (v "1.0.0") (d (list (d (n "c-emit") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "logos") (r "^0.14.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)))) (h "1achxfj8d7sigp24rn7ff2mkbsx0bdppg9nrycp9in2347jcjqhg")))

