(define-module (crates-io oo rt oort_api) #:use-module (crates-io))

(define-public crate-oort_api-0.1.0 (c (n "oort_api") (v "0.1.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "oort_shared") (r "^0.1.0") (d #t) (k 0)))) (h "0i5hdws2vgvi02as0cdhyp78bynfimy20ww1124fkx30gh76dajq")))

(define-public crate-oort_api-0.2.0 (c (n "oort_api") (v "0.2.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "oort_shared") (r "^0.2.0") (d #t) (k 0)))) (h "0344lbcgx55md7jdfwl7cyxs623iiz1yzzq1x7h9h4ji2wyw5glv")))

(define-public crate-oort_api-0.2.1 (c (n "oort_api") (v "0.2.1") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "oort_shared") (r "^0.2.0") (d #t) (k 0)))) (h "0wwy2pqbmki9g8x1hgir1lgf6bkfw6nyks4sygz4y5rl7xd754cd")))

(define-public crate-oort_api-0.2.6 (c (n "oort_api") (v "0.2.6") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "oort_shared") (r "^0.2.0") (d #t) (k 0)))) (h "0q4kmr18dcag5ixsssjkdng5fpbn16sb3d5mhjs9qcdfmrxdmgm5")))

(define-public crate-oort_api-0.3.0 (c (n "oort_api") (v "0.3.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "oort_shared") (r "^0.3.0") (d #t) (k 0)))) (h "15l1aipfsh53a32529w6zzrmkqnydrxpqqkz0jzz366i70a0aj0q")))

(define-public crate-oort_api-0.3.1 (c (n "oort_api") (v "0.3.1") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "oort_shared") (r "^0.3.0") (d #t) (k 0)))) (h "1sqfvijckbhjnw55a632lxpg1sd3rx8j5vpzphhy192ig2g6f218")))

(define-public crate-oort_api-0.4.0 (c (n "oort_api") (v "0.4.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "oort_shared") (r "^0.4.0") (d #t) (k 0)))) (h "0r3gyk55g11zz2i1f1vxc7c7q585v7kvgpn5qaigw4qvnqydak54")))

(define-public crate-oort_api-0.4.2 (c (n "oort_api") (v "0.4.2") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "oort_shared") (r "^0.4.0") (d #t) (k 0)))) (h "158hfaz26cb2dng04fldsck2xgz93jvn3wkq2kszg0nf9mwvjdz1")))

(define-public crate-oort_api-0.4.3 (c (n "oort_api") (v "0.4.3") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "oort_shared") (r "^0.4.0") (d #t) (k 0)))) (h "1d5s3xvdbc7vqynskrfs11r366qk3h13f2h241hxm586wcdjrgks")))

(define-public crate-oort_api-0.5.0 (c (n "oort_api") (v "0.5.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "oort_shared") (r "^0.5.0") (d #t) (k 0)))) (h "0vk0ab3cp03sparlbxjshf1hl1f3a8ksnkyhia0qm7n0x8py223y")))

(define-public crate-oort_api-0.5.1 (c (n "oort_api") (v "0.5.1") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "oort_shared") (r "^0.5.0") (d #t) (k 0)))) (h "1r8zq5svq3gi5jpy8kaazcjv44r79l513dz292m4hj3qsihr7pkw")))

(define-public crate-oort_api-0.5.2 (c (n "oort_api") (v "0.5.2") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "oort_shared") (r "^0.5.0") (d #t) (k 0)))) (h "03mbbw15v4pxfhawppdw7daz6jd3kx27130g881xm88n8sb6b4sh")))

(define-public crate-oort_api-0.6.0 (c (n "oort_api") (v "0.6.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "oort_shared") (r "^0.6.0") (d #t) (k 0)))) (h "00gc03x2izx7zfdc1yhygrgnk65fqd8ndzal85ylcvkav1vh14z4")))

(define-public crate-oort_api-0.6.1 (c (n "oort_api") (v "0.6.1") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "oort_shared") (r "^0.6.0") (d #t) (k 0)))) (h "0496k184dqdrwkm90jn0zi9snzmj9hb1ixbqjh1bmgzjdiakadpp")))

(define-public crate-oort_api-0.9.0 (c (n "oort_api") (v "0.9.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)))) (h "0pq9jc4ql2hc5r37bm3nkilya1jl6skkdjlbjvv470yd8w6kkxbi")))

(define-public crate-oort_api-0.11.1 (c (n "oort_api") (v "0.11.1") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)))) (h "05z21i7kcxcczrpwzqff1kjb9r7zwwzf2q2imjlp5d1rldrszwg1")))

(define-public crate-oort_api-0.11.2 (c (n "oort_api") (v "0.11.2") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)))) (h "1s37r353aaxaccfbpk7kv3djz6gi0izdyvmvbm7fr6g0f5y83l2l")))

(define-public crate-oort_api-0.12.0 (c (n "oort_api") (v "0.12.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)))) (h "1xacsz6xxmx8xh3nb4h8kg9j5c0s26qmi8yaryrfxh4nkngcq7v5")))

(define-public crate-oort_api-0.12.1 (c (n "oort_api") (v "0.12.1") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)))) (h "0azczm686j6v5x4aiccpzl6krww4lrbg9494v7782p78hwr740yf")))

(define-public crate-oort_api-0.13.0 (c (n "oort_api") (v "0.13.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)))) (h "1pxclrrfgnzrjqvgwybh3ajgkkskshvl68mq7bplcf02mchxrjpy")))

(define-public crate-oort_api-0.13.1 (c (n "oort_api") (v "0.13.1") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)))) (h "1mjzsaz6sf4b7n1bi6ydv0jw689m9c9qz0if3ikl2ml67wvhdp6d")))

(define-public crate-oort_api-0.14.0 (c (n "oort_api") (v "0.14.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)))) (h "0g0gbaah4rsrqhj523i2qp81qx34a7l0jxqlnzq215ln9xlzd9b5")))

(define-public crate-oort_api-0.15.0 (c (n "oort_api") (v "0.15.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)))) (h "1ckkccqx1kgpfn2dxa9z1d5h41mhbcr45vr42wdxdyba53xqixdy")))

(define-public crate-oort_api-0.17.0 (c (n "oort_api") (v "0.17.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)))) (h "1rg7rv28bggqh58w9m7nrgj83d5dbdh7ish00y89m6zn06q1q81p")))

(define-public crate-oort_api-0.18.1 (c (n "oort_api") (v "0.18.1") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)))) (h "1s6kv6i7v9hby8qpplri1rf9rzzz19by43if0pr5q7py65gf507w")))

(define-public crate-oort_api-0.19.0 (c (n "oort_api") (v "0.19.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)))) (h "0wkfxbd7yba6nv78mpqxmi44waf6ax12c1j39jy8qw1w3bgvd5c2")))

(define-public crate-oort_api-0.19.2 (c (n "oort_api") (v "0.19.2") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)))) (h "1ahnpn8kw9dbc42x856f3lsyj41lmmgsxcblbwray8n3xzrirndp")))

(define-public crate-oort_api-0.20.0 (c (n "oort_api") (v "0.20.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)))) (h "117dinff21awqrhgllzx4x9q3wpv5xw6c73pyfwpl9429agm40cm")))

(define-public crate-oort_api-0.21.0 (c (n "oort_api") (v "0.21.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0asvhrrxr844jlv2g19n7pvkkrlgyzd6pn5v10igrxhglbshyqzm")))

(define-public crate-oort_api-0.22.0 (c (n "oort_api") (v "0.22.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1jzyxsrhqrcgapfazaiw3blm5nlildvnkcggs9p90g60gpglili4")))

(define-public crate-oort_api-0.22.1 (c (n "oort_api") (v "0.22.1") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1n8akqj0h0a60125zqdkw5y7gc6l9m1asxf110fff93p7rjdpg5s")))

(define-public crate-oort_api-0.23.1 (c (n "oort_api") (v "0.23.1") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "165xmbgir7x67mmpan9rwxs52lzfdz1j0q5x1kjbm6lnk5pl1a7w")))

(define-public crate-oort_api-0.23.2 (c (n "oort_api") (v "0.23.2") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "12752laqz9p6vk2j7dx6csv5wb1l4pga3ifyngwh5c17g4k0k251")))

(define-public crate-oort_api-0.24.0 (c (n "oort_api") (v "0.24.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0zpqv5y8jzb0xlnwij4p5n0ck8mcn26gy3ydf31zj18di10h6sxh")))

(define-public crate-oort_api-0.25.0 (c (n "oort_api") (v "0.25.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "17mfjkjm1c047dfhvmb9qqhkpybx8zgcayaphdhsn87lcamfzp4l")))

(define-public crate-oort_api-0.26.0 (c (n "oort_api") (v "0.26.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1aam15zzm4dwxbvwvzbdc9ff74hhznjqyswcvj1dgm6fnyk7cw1m")))

(define-public crate-oort_api-0.27.0 (c (n "oort_api") (v "0.27.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0jjlghfcdg2x5pns346zidl89gpzzvc6qrm0fdm3wdmynny6pi3v")))

(define-public crate-oort_api-0.29.0 (c (n "oort_api") (v "0.29.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0vkif9gqg0yx1n99fpygx3i62m909pdq83bxcp361bm5m8w3qck2")))

(define-public crate-oort_api-0.30.0 (c (n "oort_api") (v "0.30.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0s3d6imxw0a0m7s80d7smfhz675vi2gwnl5m9dbgbjq67afsmds3")))

(define-public crate-oort_api-0.31.0 (c (n "oort_api") (v "0.31.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0wvdxhd3i4z1rbxh6l3hsywlzfpn4i9a7pfbm071ygv93b42xvbb")))

(define-public crate-oort_api-0.31.1 (c (n "oort_api") (v "0.31.1") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0gfis5sf66d5s2kl5ywkc2clpx03ndbdk9c94kfb7imv2mvrdmpi")))

(define-public crate-oort_api-0.31.2 (c (n "oort_api") (v "0.31.2") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "01dy1qdigp1hxwrlin02x47994z6kfrwd11j3cri4zm97zc5ijah")))

(define-public crate-oort_api-0.31.3 (c (n "oort_api") (v "0.31.3") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1b6g7aqyampqkwc0dpm197qf62cfaxa6zlyvy1inajv75s078h8f")))

(define-public crate-oort_api-0.32.1 (c (n "oort_api") (v "0.32.1") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "14554ifq9n57vr882xk2bjk4bz9w2125gabx2l864057r19izrdc")))

(define-public crate-oort_api-0.33.0 (c (n "oort_api") (v "0.33.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0ikc28z6hdd7z5spcdx3skn7njfd1fkdq0v7wr5nq7jx7qiggp15")))

(define-public crate-oort_api-0.34.0 (c (n "oort_api") (v "0.34.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0ya222g8d3n0zgdcpsqak1dwn1m72cjlypyhqp4ap0gc4mqhq6hb")))

(define-public crate-oort_api-0.34.1 (c (n "oort_api") (v "0.34.1") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0ygfan4nrzlks7j2bx7w6vh8hchp37fqlpnkdhjwcn1rqrd1nm8a")))

(define-public crate-oort_api-0.34.2 (c (n "oort_api") (v "0.34.2") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1l2gmk3gj3v5icwfxb4x1802cffljcvw1j8yazzc2ladl8mkdd39")))

(define-public crate-oort_api-0.35.0 (c (n "oort_api") (v "0.35.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0bscy7phl7m9yc91xppzdpm1wc1ckdndb9k36y52g0mipynf6npv")))

(define-public crate-oort_api-0.36.0 (c (n "oort_api") (v "0.36.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0ispfgcfhp1ls4ihywn05rx01260rfxf7xbmb3wdlbaw1arnzrnq")))

(define-public crate-oort_api-0.36.1 (c (n "oort_api") (v "0.36.1") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "130lz3d4j0kl3j76nwkvq1l4lnzpjrprmjmim4npfzvcaz1f3sgk")))

(define-public crate-oort_api-0.37.0 (c (n "oort_api") (v "0.37.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "10kk6imxaf0lphz8cmiym2glbz2vzwa6rcbcpg8srcmlki32jqz4")))

(define-public crate-oort_api-0.38.0 (c (n "oort_api") (v "0.38.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "10p51jdqg2yx01wclpvxw5jwjh18vmlmpdnx5h3piwgnajy7kzfv")))

(define-public crate-oort_api-0.38.1 (c (n "oort_api") (v "0.38.1") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1hy6pxi2mahplx49g15d5f5di58g8zdjrdvq3p2f81mpi45jhfx7")))

(define-public crate-oort_api-0.38.2 (c (n "oort_api") (v "0.38.2") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "18zgblwxw4yipafx69c7gyhavl43as2rxwqphig1waw3hv4zbrba")))

(define-public crate-oort_api-0.39.0 (c (n "oort_api") (v "0.39.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1w074cwzky0gpc1g3ihpamzchanqsg0ng0267qcsf4a04mad9vsp")))

(define-public crate-oort_api-0.39.1 (c (n "oort_api") (v "0.39.1") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1y6pjz7ikhvky2v9gvr1g976h9s4l9dy2npk1y0zs6733s0rvyzj")))

(define-public crate-oort_api-0.39.2 (c (n "oort_api") (v "0.39.2") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1m2k6bxpzw0l8fvix0988kz783i92z8wv65234ri0ykpbsphyz71")))

(define-public crate-oort_api-0.40.0 (c (n "oort_api") (v "0.40.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "01kqv5k3xlk6c91cwwrq687gh5q2iav5qw4i1cds1zwp84nrp5yw")))

(define-public crate-oort_api-0.41.0 (c (n "oort_api") (v "0.41.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1q2nb045v4cnm060iqcr7dd96lws50zmg87lyamjv9x67wgxslh9")))

(define-public crate-oort_api-0.42.0 (c (n "oort_api") (v "0.42.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "032jzg1vrvmh0sf79g4ii6m5lhls851xs6jib243501q92ahv8py")))

(define-public crate-oort_api-0.42.1 (c (n "oort_api") (v "0.42.1") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0ijx9xbbi8fxk6nzqxmy47p511gagss0ira8i7w4l4j1w52jqc3n")))

(define-public crate-oort_api-0.42.2 (c (n "oort_api") (v "0.42.2") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1qbhswpk6h32rg55c4aalq8zjmklw98f5shxh19ghziq6p5qq6d0")))

(define-public crate-oort_api-0.43.0 (c (n "oort_api") (v "0.43.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "02d0vc651n5hlns42vybzlwv4v4n1614y5w22y5vgapyyajisgqh")))

(define-public crate-oort_api-0.43.2 (c (n "oort_api") (v "0.43.2") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "17a94jlpshfbgqhly3b1hqqhsrjf5hh77z4msb17p2d1dixxi2x6")))

(define-public crate-oort_api-0.44.0 (c (n "oort_api") (v "0.44.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "199gbls25vg19dpng276y2ma63kvbsy9zm71a4gv3qpkr16myq2q")))

(define-public crate-oort_api-0.45.0 (c (n "oort_api") (v "0.45.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1ccaj9wh5zdgmw9s8fzlpq4y1r5d38b6nz120vc5l3skk6flban6")))

(define-public crate-oort_api-0.45.1 (c (n "oort_api") (v "0.45.1") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "18l4d823wz50ddf4rbvhsqa8jpvc28iis7061wrc11f1dg83xv07")))

(define-public crate-oort_api-0.46.0 (c (n "oort_api") (v "0.46.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "11nsy0p3qgi61380cklc567zv411lyda4dmlznmpcp3j2vvx84mm")))

(define-public crate-oort_api-0.46.1 (c (n "oort_api") (v "0.46.1") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "13xjn6flf9p9c1w2ygvrckn0h1z9slaf16fh9ibqn8y859xzl5gb")))

(define-public crate-oort_api-0.46.2 (c (n "oort_api") (v "0.46.2") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0r25g3a3hxwk0a66p8ik0yl1ypmb6r4n9wdl15l3zdmnsnxdy14r")))

(define-public crate-oort_api-0.47.0 (c (n "oort_api") (v "0.47.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0i72vkabdrvyjbaw2g7ns0hvqkh44zhj5rrc9cqvcbydi6x0c81p")))

(define-public crate-oort_api-0.48.0 (c (n "oort_api") (v "0.48.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1v1cwfy61zxjj1asc7z8x8ndchqmfanpdagypywk9k37qcs191nj")))

(define-public crate-oort_api-0.49.0 (c (n "oort_api") (v "0.49.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "09byfv5mrpkk62yrgpm1in4hkgw6kjmfcjy2r3zlqmk7anhamics")))

(define-public crate-oort_api-0.50.0 (c (n "oort_api") (v "0.50.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0zvqyw79zx59q9llfs4lzxnf44sivdzb5633diafc48n8svj4mw7")))

(define-public crate-oort_api-0.51.0 (c (n "oort_api") (v "0.51.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1k1vzq9zmj4nxbni77mgn1xi2rr17rcxiy6h9jyqdi5x2vhk3zqp")))

(define-public crate-oort_api-0.52.0 (c (n "oort_api") (v "0.52.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0j0i6lnpvvbv0wf8niqhpaazhzf8gs0chiz12ja47c88s8djk1yz")))

(define-public crate-oort_api-0.53.0 (c (n "oort_api") (v "0.53.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "114sflhs2z7lcrarfs34z9lf26p2hdzf6p50gn7nqyi389q4bb80")))

(define-public crate-oort_api-0.54.0 (c (n "oort_api") (v "0.54.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "12nycmsgvm0g923jyi0qj4w70bkvxxpx62ikzsv5fd7k3qr53z1y")))

(define-public crate-oort_api-0.54.1 (c (n "oort_api") (v "0.54.1") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1j4s4s2f9r4qnh06vg2jg7xdjsqmbw721j5naymyq5a504w3x9cd")))

(define-public crate-oort_api-0.54.2 (c (n "oort_api") (v "0.54.2") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1wnb5smhqdp5xd52rwrihn1w0bpdr9s85mf9kqlm5p5079bs6x50")))

(define-public crate-oort_api-0.55.0 (c (n "oort_api") (v "0.55.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1smpgxab0paf81wih96rb9vs4zwl5aw3d9hlmh3khaxq380vn5hk")))

(define-public crate-oort_api-0.55.1 (c (n "oort_api") (v "0.55.1") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1wbixb22wj8x3zlgvx2xx1xz6wdxav5yg3216b4v79bg1fx4jg9h")))

(define-public crate-oort_api-0.56.0 (c (n "oort_api") (v "0.56.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "12cp143z8ybiw3hb2ig79blkyr8w9wgsqklr45nfy6cq89shnxah")))

(define-public crate-oort_api-0.57.0 (c (n "oort_api") (v "0.57.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1ji52vbrdj3q0bicki8kwbmj2k4f9ca48b923plm04c7z5295pcc")))

(define-public crate-oort_api-0.57.1 (c (n "oort_api") (v "0.57.1") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0iya26lrv74pw6csdm25xz87yml5gvzzpwwsxhkcsgz7ksznxr2h")))

(define-public crate-oort_api-0.57.2 (c (n "oort_api") (v "0.57.2") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0502qryl8ikglq8226spz8rfm5fy6mi05z49r7872kr2hw77dzz4")))

(define-public crate-oort_api-0.57.3 (c (n "oort_api") (v "0.57.3") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1nnxh9334cqsdigd8p4j2gndn76lfibg8bp3gk1zvrmzyhbxq60q")))

(define-public crate-oort_api-0.58.0 (c (n "oort_api") (v "0.58.0") (d (list (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0inybial2w2c29lg2zni9agdwrrsgzvng8rai3gcgn38a0827fq5")))

(define-public crate-oort_api-0.59.0 (c (n "oort_api") (v "0.59.0") (d (list (d (n "maths-rs") (r "^0.2.4") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0nq3r8d6n2cldkjxqwd0k2s5d8x5hxwawbq6ab7lwcqhs27kj8ls")))

(define-public crate-oort_api-0.60.0 (c (n "oort_api") (v "0.60.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.4") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0p5yvv9m9882v9x0jfpx6kddzpk221vhx2kjdyp2g2gzxbwc1ixc")))

(define-public crate-oort_api-0.61.0 (c (n "oort_api") (v "0.61.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.4") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0g26grxla6lx7nllb5v77v7d7ffprkgbwwxac9k5c7jabx0hrlbh")))

(define-public crate-oort_api-0.61.1 (c (n "oort_api") (v "0.61.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.4") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1fyidwxqihazfz7mj39vwmc4hhj15gb8smmr2sgb4b9syb2f5a45")))

(define-public crate-oort_api-0.62.0 (c (n "oort_api") (v "0.62.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.4") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1yja91lwwiwhr8cl5csyn5h20wdmm7gj6habn3zqc1jwyy0m0fv5")))

(define-public crate-oort_api-0.63.0 (c (n "oort_api") (v "0.63.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.4") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1laj9a3r4ls0sr2wyg01b53i2gq8gvjblnxz2kzcvk9gd6gd49gp")))

(define-public crate-oort_api-0.64.0 (c (n "oort_api") (v "0.64.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0mvzcjl4bdkfcdg1nqinyv709bg5iin026ggp7rw77la8cwsbqwb")))

(define-public crate-oort_api-0.64.1 (c (n "oort_api") (v "0.64.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0na2yp6p0gldy5vprhvd6zabdw8vkr3kf3073ycjds0j6d4bjadf")))

(define-public crate-oort_api-0.64.2 (c (n "oort_api") (v "0.64.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0qxr9jx7517hxxh5mwnjakki62dhhj5pq47sjmkdyf2dyzcbc3yz")))

(define-public crate-oort_api-0.64.3 (c (n "oort_api") (v "0.64.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1zgn5jf3bffbkrmsyg76in992in19k3bfxf0hhxswxnjbgd54npd")))

(define-public crate-oort_api-0.65.0 (c (n "oort_api") (v "0.65.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1slhqc9dgnzi3hilnch4pb1s1a6rrcpjj2brhpi62dwlsi4fkkiv")))

(define-public crate-oort_api-0.65.1 (c (n "oort_api") (v "0.65.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1ckk5bg7f7myq0jj0j1ydlm5c5g6iashv92nml3y39j0196ziyng")))

(define-public crate-oort_api-0.66.0 (c (n "oort_api") (v "0.66.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1vagkb79wh1n6la4c6mam6zv5x5cll6pm9ifcs52gb56chps1phm")))

(define-public crate-oort_api-0.66.1 (c (n "oort_api") (v "0.66.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1nvlnhzykndp2yxss0mkjn62w8f2ghiqglgagm27lc3w5k6nhfqa")))

(define-public crate-oort_api-0.67.0 (c (n "oort_api") (v "0.67.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0hsahi5b3zxw12y8q83afa89p3zs5h29czgj15vj5ayn4z7rzhz5")))

(define-public crate-oort_api-0.67.1 (c (n "oort_api") (v "0.67.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1ir295j4vmnmhjzk2z9ar6ap766cvx4zhkj53rjxknbks2c4kbs5")))

(define-public crate-oort_api-0.67.2 (c (n "oort_api") (v "0.67.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1skm870f5hcps9ganhp4czc8gn1ik60mafcqxbz2m58ddl3xx8hi")))

(define-public crate-oort_api-0.67.3 (c (n "oort_api") (v "0.67.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "13xirvhi4swf2j8mll2l06kg572pacpaj242vjirvlsjg88wzyn2")))

(define-public crate-oort_api-0.67.4 (c (n "oort_api") (v "0.67.4") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0vm9q2mxq85067lc3w86n70ygvwgsnz0dx2v08phvrp5bwn9l2id")))

(define-public crate-oort_api-0.68.0 (c (n "oort_api") (v "0.68.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1vqpif7fhp6i9pg4vqz6sc90j3bbmnhjp7gp915amrgdmg480zh2")))

(define-public crate-oort_api-0.68.1 (c (n "oort_api") (v "0.68.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "031nm0pbbdyd3lvirbsprivv21x85hchkmqlriv81c1iby9yzcpq")))

(define-public crate-oort_api-0.68.2 (c (n "oort_api") (v "0.68.2") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0w9idrfv2higwjy6dc4zizsy0b7xyzl151zif9fhj7q8g8dzakjk")))

(define-public crate-oort_api-0.69.0 (c (n "oort_api") (v "0.69.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0234xbrl3j52hyqxvpmmbkd8j39ybzkkqrr4mwxzibn1h75p7bky")))

(define-public crate-oort_api-0.70.0 (c (n "oort_api") (v "0.70.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1chcbi65h520i6p4mnbqafhab8znpasgsf366sd28ymwxlhk9is5")))

(define-public crate-oort_api-0.71.0 (c (n "oort_api") (v "0.71.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1gqwsrb5wm46pafxq5hi32rac4rq587f3w8wlylmpb082igvqqrl")))

(define-public crate-oort_api-0.72.0 (c (n "oort_api") (v "0.72.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "00r8lvggdj6ir33kfb4zvcza3qyfrl2zfnvgkriav21rzwimhpkf")))

(define-public crate-oort_api-0.73.0 (c (n "oort_api") (v "0.73.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1fyav1fqbyaqmy46d11p7lnfrlnl82i5j8g63ql6m4zni297x2cs")))

(define-public crate-oort_api-0.74.0 (c (n "oort_api") (v "0.74.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0zzy4850xi69xlb7n98431s07xixkz424q045chpsj82pr1pnwp1")))

(define-public crate-oort_api-0.74.1 (c (n "oort_api") (v "0.74.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "04xagmghmg09bdb1scr7gwv7fk9qz2i8xqjzwchbz831k8dlgpx7")))

(define-public crate-oort_api-0.75.0 (c (n "oort_api") (v "0.75.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "17npxvmbml7lrsjvh8fz9g81nq8210y3ms0ps2fd2sgvdadkbx56")))

(define-public crate-oort_api-0.76.0 (c (n "oort_api") (v "0.76.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1v1iibm8ymfmnchll51h3gbamgcs74x527k3r16zq32hpb4r6655")))

(define-public crate-oort_api-0.76.1 (c (n "oort_api") (v "0.76.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0b5k8q9w6cw24zb6b6s09n3lv941p89kslxk16hg2w27njah2n70")))

(define-public crate-oort_api-0.76.2 (c (n "oort_api") (v "0.76.2") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0jw3rqps28kp4lqr5c4vzc4i6jgxpiadqgynyr6yfdka5zdk0a8i")))

(define-public crate-oort_api-0.76.3 (c (n "oort_api") (v "0.76.3") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1jm3mnhbcw10xmi30y11rl60v46snr3wcp2qfqrmv12s3gmd1lfi")))

(define-public crate-oort_api-0.77.0 (c (n "oort_api") (v "0.77.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1hkkr8xgrw5llqzwb5gm0rmf6cqzlswj4bdm3i316v3411i68rjw")))

(define-public crate-oort_api-0.78.0 (c (n "oort_api") (v "0.78.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1f7w0xy1v35j0g4p3nnyrh6lk110g5nc4r0d92dnmck5mwf993v7")))

(define-public crate-oort_api-0.78.1 (c (n "oort_api") (v "0.78.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1iajvx0b53m2qrmg2avrcihmlgnhgibj8fcjw1463ndakxqfhllf")))

(define-public crate-oort_api-0.78.2 (c (n "oort_api") (v "0.78.2") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0yaihhlbamjva036i6m8iamlznrd74sw7l037ync05qs3a6gqfb0")))

(define-public crate-oort_api-0.79.0 (c (n "oort_api") (v "0.79.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1p5d8i28mwzcfwxmgwrazknddi5s22rskjy81qx4lqrdd5zqy7ym")))

(define-public crate-oort_api-0.79.1 (c (n "oort_api") (v "0.79.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1spfrqlirk1ika6max53a7jq322iygjkid9bbfxcz93h2r3gfl0a")))

(define-public crate-oort_api-0.79.2 (c (n "oort_api") (v "0.79.2") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "08xfqa3rs1g0a4jzg4z0088a6m5frqy1din32xh8wjs99q1y8gdy")))

(define-public crate-oort_api-0.79.3 (c (n "oort_api") (v "0.79.3") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "maths-rs") (r "^0.2.5") (d #t) (k 0)) (d (n "oorandom") (r "^11.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0zpa0zj1abmxrgydywqsgqp48ghxf307zqfljpzl2xqsrmdvgqls")))

