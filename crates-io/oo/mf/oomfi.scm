(define-module (crates-io oo mf oomfi) #:use-module (crates-io))

(define-public crate-oomfi-0.1.0 (c (n "oomfi") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)))) (h "1kkvwnxnkf4ygfxd9kpy3mmhmylyh68amaxpw8frf5fdspc99imi") (y #t)))

(define-public crate-oomfi-0.1.1 (c (n "oomfi") (v "0.1.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)))) (h "0x15xxs5p0b1731zq0p6yahy0njapzciq2nm3375c18gsgf09dxi")))

(define-public crate-oomfi-0.1.2 (c (n "oomfi") (v "0.1.2") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)))) (h "1xqrw79bb4mrrlq9pqgdy7h3v02c8kw8nh4xkpxqwn2r9g6j6sjj")))

