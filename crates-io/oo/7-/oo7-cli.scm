(define-module (crates-io oo #{7-}# oo7-cli) #:use-module (crates-io))

(define-public crate-oo7-cli-0.3.0 (c (n "oo7-cli") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("alloc" "clock"))) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "oo7") (r "^0.3") (d #t) (k 0)) (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("macros" "rt-multi-thread"))) (k 0)))) (h "1hq2kg862d5gamw4aj3cbwxdf9dizryz7l9150cm52kflmygabnf") (r "1.75")))

