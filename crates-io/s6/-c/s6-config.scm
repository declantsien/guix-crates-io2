(define-module (crates-io s6 -c s6-config) #:use-module (crates-io))

(define-public crate-s6-config-0.1.0 (c (n "s6-config") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1ihcjywpfkvdha384iyqajrhfln48775ips2ysnrsmsmk5c74rsx")))

(define-public crate-s6-config-0.1.1 (c (n "s6-config") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1ck7qigylzkplxhpwayffcwqwwd0wr1nqgz9pqm97w9snz6ips43")))

(define-public crate-s6-config-0.1.2 (c (n "s6-config") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "06bn8vkv5zg0dx13jxmqw3k4grh8w7dfkywkp9yzx2x9gm1flirr")))

(define-public crate-s6-config-0.1.3 (c (n "s6-config") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indoc") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "1z6jxqlx6w18mjqidnmjbn0aifcxf9ipwd367ymh4a60z55wcgq8")))

