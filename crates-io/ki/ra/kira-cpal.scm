(define-module (crates-io ki ra kira-cpal) #:use-module (crates-io))

(define-public crate-kira-cpal-0.1.0-beta.0 (c (n "kira-cpal") (v "0.1.0-beta.0") (d (list (d (n "assert_no_alloc") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "cpal") (r "^0.13.3") (d #t) (k 0)) (d (n "kira") (r "^0.6.0-beta.0") (d #t) (k 0)) (d (n "ringbuf") (r "^0.2.3") (d #t) (k 0)))) (h "12y37hcv4yi3ks053sfj2nbp4kshn0xg9qgjqqxmw53n1sll0yvp")))

