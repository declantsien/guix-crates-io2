(define-module (crates-io ki ra kira-loaders) #:use-module (crates-io))

(define-public crate-kira-loaders-0.1.0-beta.0 (c (n "kira-loaders") (v "0.1.0-beta.0") (d (list (d (n "kira") (r "^0.6.0-beta.0") (d #t) (k 0)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 0)) (d (n "symphonia") (r "^0.4.0") (k 0)))) (h "08yd8fk2aqncw24lwjbzzipszncj0a0w4ls4xxci3d86xml64hli") (f (quote (("wav" "symphonia/wav" "symphonia/pcm") ("ogg" "symphonia/ogg" "symphonia/vorbis") ("mp3" "symphonia/mp3") ("flac" "symphonia/flac") ("default" "mp3" "ogg" "flac" "wav"))))))

(define-public crate-kira-loaders-0.1.0-beta.1 (c (n "kira-loaders") (v "0.1.0-beta.1") (d (list (d (n "kira") (r "^0.6.0-beta.2") (d #t) (k 0)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 0)) (d (n "symphonia") (r "^0.4.0") (k 0)))) (h "02m6f0r0rylkrsnj56djxs5i44l6g0sqgfa892g27sdgg3savjxb") (f (quote (("wav" "symphonia/wav" "symphonia/pcm") ("ogg" "symphonia/ogg" "symphonia/vorbis") ("mp3" "symphonia/mp3") ("flac" "symphonia/flac") ("default" "mp3" "ogg" "flac" "wav"))))))

(define-public crate-kira-loaders-0.1.0-beta.2 (c (n "kira-loaders") (v "0.1.0-beta.2") (d (list (d (n "kira") (r "^0.6.0-beta.2") (d #t) (k 0)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 0)) (d (n "symphonia") (r "^0.4.0") (k 0)))) (h "08k037ygks45i428abrxk8k4f65ky6q2bi1i6fk9xb5153csijc2") (f (quote (("wav" "symphonia/wav" "symphonia/pcm") ("ogg" "symphonia/ogg" "symphonia/vorbis") ("mp3" "symphonia/mp3") ("flac" "symphonia/flac") ("default" "mp3" "ogg" "flac" "wav"))))))

(define-public crate-kira-loaders-0.1.0-beta.3 (c (n "kira-loaders") (v "0.1.0-beta.3") (d (list (d (n "kira") (r "^0.6.0-beta.2") (d #t) (k 0)) (d (n "ringbuf") (r "^0.2.6") (d #t) (k 0)) (d (n "symphonia") (r "^0.4.0") (k 0)))) (h "0aa7kqsq67lmlc94r4kchpkzlsyfk457j9r8dw3a14dkg82p6p8h") (f (quote (("wav" "symphonia/wav" "symphonia/pcm") ("ogg" "symphonia/ogg" "symphonia/vorbis") ("mp3" "symphonia/mp3") ("flac" "symphonia/flac") ("default" "mp3" "ogg" "flac" "wav"))))))

