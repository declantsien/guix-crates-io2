(define-module (crates-io ki mo kimono) #:use-module (crates-io))

(define-public crate-kimono-0.0.0 (c (n "kimono") (v "0.0.0") (d (list (d (n "stretch") (r "^0.3.2") (d #t) (k 0)))) (h "14smxg5j4h0k7jds5257c13a0r9lb2zjnyjgngh8bkj3wpvcj3qk")))

(define-public crate-kimono-0.0.1 (c (n "kimono") (v "0.0.1") (d (list (d (n "ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1mj7c30ddc1n7p1hvssi0j9r65ppa10605vjafxdmam80w8g3d38")))

(define-public crate-kimono-0.0.2 (c (n "kimono") (v "0.0.2") (d (list (d (n "ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1frm38m1jh88r8560grl4f5s4z89nypidknaqpzcrd1zqap1h8ap")))

(define-public crate-kimono-0.0.3 (c (n "kimono") (v "0.0.3") (d (list (d (n "ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0yhflbgbjc35y5asw0lzrcmrqb84r8va041sp6jfc2r2c11a38wi")))

(define-public crate-kimono-0.0.4 (c (n "kimono") (v "0.0.4") (d (list (d (n "ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "13a6dyfk3i6fc7ya6cclgg8rhf5cisshamk0fz96lldbgi69j7v9")))

(define-public crate-kimono-0.0.5 (c (n "kimono") (v "0.0.5") (d (list (d (n "ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0xjvqk8pmfrqqy7b4fy4w8nfq23dvp79r5czs58nv7gxhgxb7rag")))

(define-public crate-kimono-0.0.6 (c (n "kimono") (v "0.0.6") (d (list (d (n "ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0vcz54wyiqf70q4rjj73mhccnk4qlg8pfm6mlbb85r65fz1bvvwz")))

(define-public crate-kimono-0.0.7 (c (n "kimono") (v "0.0.7") (d (list (d (n "ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0138qgf3w1aww6x9ni7kr4356wqi2zd1bfv5z5issaz71m87rpc0")))

(define-public crate-kimono-0.1.0 (c (n "kimono") (v "0.1.0") (d (list (d (n "ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0jxdix1cmqzc63llvina6k4i9aycwpdrd0a7wrvnhjd3z0s80d08")))

(define-public crate-kimono-0.1.1 (c (n "kimono") (v "0.1.1") (d (list (d (n "ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0hgfxdaz2pnjfg6gk9dk1rdlrdhhjxz7pdazcwdmbh01l1pj75gl")))

(define-public crate-kimono-0.1.2 (c (n "kimono") (v "0.1.2") (d (list (d (n "ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0nqyb9xkc7jvm1whv43kyl5lwv2dzmws68d0s7gwlql0d7h0qz8c")))

(define-public crate-kimono-0.1.3 (c (n "kimono") (v "0.1.3") (d (list (d (n "ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ukiyoe") (r "^0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0jhk68dfx21s57d3sji7yr7kjzlmbv36s0h4kdqs3xw716i915y2")))

