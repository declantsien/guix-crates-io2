(define-module (crates-io ki ri kiri) #:use-module (crates-io))

(define-public crate-kiri-0.1.0 (c (n "kiri") (v "0.1.0") (d (list (d (n "evdev") (r "^0.12.1") (d #t) (k 0)) (d (n "evdev-keys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.138") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "0pb6dnlj4j89i1zfws2kc2mjlrv4k3jyjgdlgc9n1z90hgb0rw72")))

(define-public crate-kiri-0.2.0 (c (n "kiri") (v "0.2.0") (d (list (d (n "evdev") (r "^0.12.1") (d #t) (k 0)) (d (n "evdev-keys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.138") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "130q5s6s44jw7dhzqc2iv0zz5vrw583921dmj30sqwyiaf9abcan")))

(define-public crate-kiri-0.3.0 (c (n "kiri") (v "0.3.0") (d (list (d (n "evdev") (r "^0.12.1") (d #t) (k 0)) (d (n "evdev-keys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.138") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "1s9d000gl002kzwhgk31chfanmcjjvzds838y8jdwqxbxfagzzcp")))

