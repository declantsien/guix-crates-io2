(define-module (crates-io ki ti kitin) #:use-module (crates-io))

(define-public crate-kitin-0.1.0 (c (n "kitin") (v "0.1.0") (h "1ybrx260r1787jpmflrmvz3dnhwndm5sd98xwpr2645zgpmb5f4c")))

(define-public crate-kitin-0.1.1 (c (n "kitin") (v "0.1.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sf3c1mlbhh3silvllwlv25wiksfng70mrlnr2c3f8smw3chbh3v")))

(define-public crate-kitin-0.1.2 (c (n "kitin") (v "0.1.2") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.25") (d #t) (k 0)))) (h "0fzjbn7lirkd9jqgnl0d1js7pg82k9ppqmmlngacfak8ar1vbps5")))

