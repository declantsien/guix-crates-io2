(define-module (crates-io ki ll killjoy-notifier-logfile) #:use-module (crates-io))

(define-public crate-killjoy-notifier-logfile-0.1.0 (c (n "killjoy-notifier-logfile") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "csv") (r "^1.1.1") (d #t) (k 0)) (d (n "dbus") (r "^0.6.5") (d #t) (k 0)) (d (n "xdg") (r "^2.2.0") (d #t) (k 0)))) (h "0q0djcgydx2gkyvd6s9xdyg8bhsgw06hv5r0z7kvkxi4kb7gmv8d")))

(define-public crate-killjoy-notifier-logfile-0.1.1 (c (n "killjoy-notifier-logfile") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "csv") (r "^1.1.1") (d #t) (k 0)) (d (n "dbus") (r "^0.6.5") (d #t) (k 0)) (d (n "xdg") (r "^2.2.0") (d #t) (k 0)))) (h "0y487mqagfndgm1caim638h5cjm2dip60jqdhvn0ax65cn0ndpjn")))

(define-public crate-killjoy-notifier-logfile-0.2.0 (c (n "killjoy-notifier-logfile") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "csv") (r "^1.1.1") (d #t) (k 0)) (d (n "dbus") (r "^0.7.1") (d #t) (k 0)) (d (n "xdg") (r "^2.2.0") (d #t) (k 0)))) (h "0nd09riyfbrgrqz9li1ig8gmd9ck8xacrry73hr8lw2zal2jich3")))

