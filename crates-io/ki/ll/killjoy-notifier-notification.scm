(define-module (crates-io ki ll killjoy-notifier-notification) #:use-module (crates-io))

(define-public crate-killjoy-notifier-notification-0.1.0 (c (n "killjoy-notifier-notification") (v "0.1.0") (d (list (d (n "dbus") (r "^0.6.5") (d #t) (k 0)))) (h "1v1avdw8ls0i8idylbfj6jmmfbh60lgsi4zr8j7bbf8jb1c3k6r6")))

(define-public crate-killjoy-notifier-notification-0.1.1 (c (n "killjoy-notifier-notification") (v "0.1.1") (d (list (d (n "dbus") (r "^0.6.5") (d #t) (k 0)))) (h "002zlvqky6rynifqh55a4cdxw4fvmymq4bd22jfslxfwxlsvpmqi")))

(define-public crate-killjoy-notifier-notification-0.2.0 (c (n "killjoy-notifier-notification") (v "0.2.0") (d (list (d (n "dbus") (r "^0.7.1") (d #t) (k 0)))) (h "1l9py8hjzxnm5xdff1vpyf9cpp8ax4x1ds9x2mgrnjabkib09gzj")))

