(define-module (crates-io ki je kijetesantakaluotokieni) #:use-module (crates-io))

(define-public crate-kijetesantakaluotokieni-1.0.0 (c (n "kijetesantakaluotokieni") (v "1.0.0") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "voca_rs") (r "^1.14.0") (d #t) (k 0)))) (h "13fm9gp7xxl2isnb3cqc8xhhfvbdmq7hbnhc136x29icdkv5n1ki")))

(define-public crate-kijetesantakaluotokieni-1.0.1 (c (n "kijetesantakaluotokieni") (v "1.0.1") (d (list (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "voca_rs") (r "^1.14.0") (d #t) (k 0)))) (h "0qwhqfnmijfbq45lc02drrhn5n3p9z43s96fiqz8piq1n3knpx5d")))

