(define-module (crates-io ki no kinode_process_lib) #:use-module (crates-io))

(define-public crate-kinode_process_lib-0.0.0-reserved (c (n "kinode_process_lib") (v "0.0.0-reserved") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "ethers-core") (r "^2.0.11") (o #t) (d #t) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.16.0") (d #t) (k 0)))) (h "0ylkl7qw0xlmx8zmawqvsjskwk80ipsl13185fjnp22cvjlrfwwn") (f (quote (("eth" "ethers-core"))))))

