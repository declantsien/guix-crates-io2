(define-module (crates-io ki gn kignore) #:use-module (crates-io))

(define-public crate-kignore-0.1.0 (c (n "kignore") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "gitignore_inner") (r "^0.1.0") (d #t) (k 0)))) (h "0miwfc9kfbnf0lvihwq238si10zjdqa1npvq1wwd2nkpchzvhr8z")))

(define-public crate-kignore-0.1.1 (c (n "kignore") (v "0.1.1") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "gitignore_inner") (r "^0.1.0") (d #t) (k 0)))) (h "09mgvbm66xq051jsw9k54safxnfm4h1mkcanc9lhpnx5hybvxrnx")))

