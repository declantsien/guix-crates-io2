(define-module (crates-io ki pp kippschalter) #:use-module (crates-io))

(define-public crate-kippschalter-0.1.0 (c (n "kippschalter") (v "0.1.0") (d (list (d (n "i3ipc") (r "^0.10.1") (f (quote ("i3-4-14"))) (d #t) (k 0)))) (h "19wd4vyg492kr6ivl39i8kd3r51ci08vx67pwfiviyhnbhc0arjh") (y #t)))

(define-public crate-kippschalter-0.2.1 (c (n "kippschalter") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "i3ipc") (r "^0.10.1") (f (quote ("i3-4-14"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0k1cm65llbbilc2i8l6v1fxlp3d7k0k5lrkqfb6bmiqmgzgx20kj") (y #t)))

