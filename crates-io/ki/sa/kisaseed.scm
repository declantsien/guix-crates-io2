(define-module (crates-io ki sa kisaseed) #:use-module (crates-io))

(define-public crate-kisaseed-0.1.0 (c (n "kisaseed") (v "0.1.0") (d (list (d (n "cipher") (r "^0.4.4") (d #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "13nyyna4z8i9s0hi57id3mxvw58jr7q1sng1pvfyk7a9zf75wsca") (s 2) (e (quote (("zeroize" "dep:zeroize")))) (r "1.56")))

(define-public crate-kisaseed-0.1.1 (c (n "kisaseed") (v "0.1.1") (d (list (d (n "cipher") (r "^0.4.4") (d #t) (k 0)) (d (n "cipher") (r "^0.4.4") (f (quote ("dev"))) (d #t) (k 2)))) (h "0l5zfhsynxg67mvmnw5h908nbgzq813w4vx7nbiiin2bp062nx79") (f (quote (("zeroize" "cipher/zeroize")))) (r "1.56")))

(define-public crate-kisaseed-0.1.2 (c (n "kisaseed") (v "0.1.2") (d (list (d (n "cipher") (r "^0.4.4") (d #t) (k 0)) (d (n "cipher") (r "^0.4.4") (f (quote ("dev"))) (d #t) (k 2)))) (h "1x1vb4qxjf26iy0h7mrbc92s62wkj65lgycwvv6ywm53w3a303vh") (f (quote (("zeroize" "cipher/zeroize")))) (r "1.56")))

(define-public crate-kisaseed-0.1.3 (c (n "kisaseed") (v "0.1.3") (d (list (d (n "cipher") (r "^0.4.4") (d #t) (k 0)) (d (n "cipher") (r "^0.4.4") (f (quote ("dev"))) (d #t) (k 2)))) (h "1pscsknsrvwn2z7gvlg1sqs4p5mkppbmbf0lcspg9d5cizfrn6wm") (f (quote (("zeroize" "cipher/zeroize")))) (r "1.56")))

