(define-module (crates-io ki nc kincaid) #:use-module (crates-io))

(define-public crate-kincaid-0.1.0 (c (n "kincaid") (v "0.1.0") (h "1pr4l5c2fsp2j68wn85ixfh40nmmvciyxjgsh7cprkrq5ph06wwc")))

(define-public crate-kincaid-0.2.0 (c (n "kincaid") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "12qz69nj7ffvm8yb68742jssfv6wy9dnahaa12473kw9kpibag3m")))

(define-public crate-kincaid-0.2.2 (c (n "kincaid") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0asq6qq9dm49hcmlnsh3g556cpp1jyjab9km39qwdb0r2hhzwxq2")))

(define-public crate-kincaid-0.2.4 (c (n "kincaid") (v "0.2.4") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "0231kvi96vp1nb8mr5b0jwr4x4znfbcdbd0n3pd28ldic5n3lzc2") (f (quote (("default" "console_error_panic_hook"))))))

