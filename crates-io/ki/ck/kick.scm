(define-module (crates-io ki ck kick) #:use-module (crates-io))

(define-public crate-kick-0.0.0 (c (n "kick") (v "0.0.0") (h "0w20mwq04pnq1wwayzhd37ixig7x24kh8vr4s10006i99prf4906")))

(define-public crate-kick-0.0.1 (c (n "kick") (v "0.0.1") (h "1jjvff9ciza30hpqpixqgf6nyk2k8i9mfxxpkchnqj5gc125wyca")))

(define-public crate-kick-0.0.2 (c (n "kick") (v "0.0.2") (h "088nvsrfg6nq9apvk378jcggvpryrxmjsvhz69hzwdbb3p61lx3j") (f (quote (("nightly"))))))

(define-public crate-kick-0.0.3 (c (n "kick") (v "0.0.3") (h "0mizg6jani0hpf23p8g05zml345ymgma3hw6qyvd92yydx6gckjm") (f (quote (("nightly"))))))

(define-public crate-kick-0.0.4 (c (n "kick") (v "0.0.4") (h "0h9mdqc9izbl5k7mrhxhc8a0r6xk2sgchckd2g2ps7gkghcpzfpw") (f (quote (("nightly"))))))

(define-public crate-kick-0.1.0 (c (n "kick") (v "0.1.0") (h "1zkjxdi2h4fqn11a2kq20z2rq0h51jrr8db2y0nngzp4i8vmjfcs") (f (quote (("nightly"))))))

(define-public crate-kick-0.1.2 (c (n "kick") (v "0.1.2") (h "14yfy6jcki39nr8h3pd6gsxvkj7cvmhp606p3609dsddl4jh8m4g") (f (quote (("nightly"))))))

