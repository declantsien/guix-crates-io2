(define-module (crates-io ki st kistro) #:use-module (crates-io))

(define-public crate-kistro-0.1.0 (c (n "kistro") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ivv9zj2dpy2nl4grrsc4r6rnvgi11h4alid4jdkg5p3qffqa6zk")))

(define-public crate-kistro-0.1.1 (c (n "kistro") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)))) (h "14kyd41xfpy0db7brrkvn4dnfkdkyjlfr8pxcymh14jq5lvfixnr")))

