(define-module (crates-io ki fu kifuwarabe_tic_tac_toe) #:use-module (crates-io))

(define-public crate-kifuwarabe_tic_tac_toe-0.1.0 (c (n "kifuwarabe_tic_tac_toe") (v "0.1.0") (d (list (d (n "casual_logger") (r "^0.4.7") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0vwkcgwibr3s9zibr46mlsbli6cls2gl8k7z80fqjhxrc98j8a0g")))

(define-public crate-kifuwarabe_tic_tac_toe-0.1.1 (c (n "kifuwarabe_tic_tac_toe") (v "0.1.1") (d (list (d (n "casual_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "12r288x3r3lx0vj7mdaxnlazhf4x0ic2ps3i112wis64zxrblzr7")))

(define-public crate-kifuwarabe_tic_tac_toe-0.1.2 (c (n "kifuwarabe_tic_tac_toe") (v "0.1.2") (d (list (d (n "casual_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1h8gwa35xaid29id0y4qvwaij3zh1dnjnirid5c5k08znyajspdj")))

(define-public crate-kifuwarabe_tic_tac_toe-0.1.3 (c (n "kifuwarabe_tic_tac_toe") (v "0.1.3") (d (list (d (n "casual_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0azxniwbjwb56k35j5afyb3cbf6cz5szfyplhzbs9zxx6l5phidi")))

(define-public crate-kifuwarabe_tic_tac_toe-0.1.4 (c (n "kifuwarabe_tic_tac_toe") (v "0.1.4") (d (list (d (n "casual_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0yg1iyi5yd7vkcfb3k9xdvrrwy2c13frmy4f9a2s4mbnwmx7w5pn")))

(define-public crate-kifuwarabe_tic_tac_toe-0.1.5 (c (n "kifuwarabe_tic_tac_toe") (v "0.1.5") (d (list (d (n "casual_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "15p216pm346mpacqncb696fznyvy17ffx8s90vy6m9wmz61bg9bb")))

(define-public crate-kifuwarabe_tic_tac_toe-0.1.6 (c (n "kifuwarabe_tic_tac_toe") (v "0.1.6") (d (list (d (n "casual_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "02qxj8a66bbvg4zms3maf9hjc2cvj1xk8x2dri3sfgfl0kgbwyd1")))

(define-public crate-kifuwarabe_tic_tac_toe-0.2.0 (c (n "kifuwarabe_tic_tac_toe") (v "0.2.0") (d (list (d (n "casual_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1j682qdkc6llkzjli9g984i7mn95cd1vr7437xbxjh3a8r2lvfh8")))

(define-public crate-kifuwarabe_tic_tac_toe-0.2.1 (c (n "kifuwarabe_tic_tac_toe") (v "0.2.1") (d (list (d (n "casual_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "15h3g7llx72wq1349jlzjf6501ywghzdwlarncl3qr4wgjabkjgh")))

(define-public crate-kifuwarabe_tic_tac_toe-0.2.2 (c (n "kifuwarabe_tic_tac_toe") (v "0.2.2") (d (list (d (n "casual_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0m5d5vx2f22al1lnxq4nmrzkpr2v4a0srivr878dzdv035m5sdxm")))

(define-public crate-kifuwarabe_tic_tac_toe-0.2.3 (c (n "kifuwarabe_tic_tac_toe") (v "0.2.3") (d (list (d (n "casual_logger") (r "^0.5.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0ncfwrz0gwmcwac729l0dvbknj2wig15ygq3y6y1k6kwcf88lw3i")))

(define-public crate-kifuwarabe_tic_tac_toe-0.2.4 (c (n "kifuwarabe_tic_tac_toe") (v "0.2.4") (d (list (d (n "casual_logger") (r "^0.6.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "17h2fm8cc9zr26v5zbb5j2cggv108vh5i9pl7nhaihg1mzy52v7w")))

