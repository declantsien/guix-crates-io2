(define-module (crates-io ki la kilac) #:use-module (crates-io))

(define-public crate-kilac-0.1.0 (c (n "kilac") (v "0.1.0") (h "1f890rcc8c4cbh6i2nbg5xbvirdkksglbcv3wyqammmnp0srzrnq")))

(define-public crate-kilac-0.1.1 (c (n "kilac") (v "0.1.1") (h "1r9cjz2flbql7s8kb5mbrc099l7vss1cbmfh906sjzlpkjbnyyai")))

(define-public crate-kilac-0.1.2 (c (n "kilac") (v "0.1.2") (d (list (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)))) (h "11315c5fqm01mpysdbl7gwh744gpb59y2v7sz5glxxzic8b7ivbz")))

(define-public crate-kilac-0.1.3 (c (n "kilac") (v "0.1.3") (d (list (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)))) (h "00ccqlsbi0vpdmn9mjq8d81lfjxfvxgnnqqbfih6hc6av1npddbl")))

(define-public crate-kilac-0.2.0 (c (n "kilac") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.5.0") (d #t) (k 2)))) (h "0jhw8wymnphp23m9wjg73055vqmaqsqy7ysabsj9pnqamw7x85a1")))

