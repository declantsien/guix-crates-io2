(define-module (crates-io ki fm kifmm-fftw-sys) #:use-module (crates-io))

(define-public crate-kifmm-fftw-sys-0.1.0 (c (n "kifmm-fftw-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.90") (d #t) (k 1)) (d (n "kifmm-fftw-src") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.5") (d #t) (k 0)))) (h "0zygrivhwkamkya41fwppzynv48zkar1gvv68rrwqrdpsvjk8v1d")))

