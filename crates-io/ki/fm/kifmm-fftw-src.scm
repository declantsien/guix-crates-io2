(define-module (crates-io ki fm kifmm-fftw-src) #:use-module (crates-io))

(define-public crate-kifmm-fftw-src-0.1.0 (c (n "kifmm-fftw-src") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.90") (d #t) (k 1)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)) (d (n "ftp") (r "^3.0.1") (d #t) (k 1)) (d (n "zip") (r "^1.1.1") (d #t) (k 1)))) (h "19y8ri9v5zqhdk0bi31r2lq5i0maylhs847k4ikrq8kpg87yjczx") (l "fftw3")))

