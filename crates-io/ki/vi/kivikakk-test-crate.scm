(define-module (crates-io ki vi kivikakk-test-crate) #:use-module (crates-io))

(define-public crate-kivikakk-test-crate-0.1.0 (c (n "kivikakk-test-crate") (v "0.1.0") (h "1ysjxya1v9nq3mzybq6hq091nc6ljbgg7r69q5d1brg15z5v4zxl")))

(define-public crate-kivikakk-test-crate-0.1.1 (c (n "kivikakk-test-crate") (v "0.1.1") (h "139wpyzkrdv7m9j3zadil715h5f06vb687ajs6db63ap474y77xi")))

