(define-module (crates-io ki tt kitten) #:use-module (crates-io))

(define-public crate-kitten-0.1.0 (c (n "kitten") (v "0.1.0") (h "0k1i5n1i9bn5xdsvzpi22372lyxjbj1d6cvn1cm1g59n0c3495py")))

(define-public crate-kitten-0.1.1 (c (n "kitten") (v "0.1.1") (h "08625yd5llihk4xdk4m5n2zgnr0dj2ydbndmpm0mmlyihmxflna7")))

(define-public crate-kitten-0.1.3 (c (n "kitten") (v "0.1.3") (h "08j62sxni0s422kd766irqg3mwn73zcjrw23rs54hd1nv8nd6jr4")))

(define-public crate-kitten-0.2.0 (c (n "kitten") (v "0.2.0") (h "09wciy3xr59rpww8b7571mdffikrsa03m4vgvlampxgl4d70hz79")))

(define-public crate-kitten-0.3.0 (c (n "kitten") (v "0.3.0") (h "0n3sag26lcbgy640xixv3zxdnq7fp52b1llg5v71z7i2j9wi2qvl")))

(define-public crate-kitten-0.4.0 (c (n "kitten") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0fpzp390d52nwxd7dbs2py39g0l71xb1hw04kwkiy413az6d6m34") (y #t)))

(define-public crate-kitten-0.5.0 (c (n "kitten") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1.73") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19bc6n4x7lg5dsjqh4i4zn5hy4cm69givla9xh9wg48f8y90zmvz")))

