(define-module (crates-io ki tt kittycad-unit-conversion-derive) #:use-module (crates-io))

(define-public crate-kittycad-unit-conversion-derive-0.1.0 (c (n "kittycad-unit-conversion-derive") (v "0.1.0") (d (list (d (n "inflections") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.60") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("derive" "fold"))) (d #t) (k 0)))) (h "0f47q0zxzrxlj8nz0dchg600j8wqn4whb45349kwxvf1j9mc80bh")))

