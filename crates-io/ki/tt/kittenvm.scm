(define-module (crates-io ki tt kittenvm) #:use-module (crates-io))

(define-public crate-KittenVM-1.0.0-1 (c (n "KittenVM") (v "1.0.0-1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1bpl0mp8rljvvy1a3sqx7cricd9rlj2c0xslj6q0s6izjjyb5w8c") (y #t)))

(define-public crate-KittenVM-1.0.0-2 (c (n "KittenVM") (v "1.0.0-2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "03jj4nx3ch63ldb0l1ymjrpjmri4fymxjycwkfvwpd96088r7lyz") (y #t)))

(define-public crate-KittenVM-1.0.0-3 (c (n "KittenVM") (v "1.0.0-3") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "15w9p7ddmzabh6pi66k979h2c9z3jbqizc6i7vyn5m06zqbnzza9") (y #t)))

(define-public crate-KittenVM-1.0.0-4 (c (n "KittenVM") (v "1.0.0-4") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "007z6k2kf6fvlvq6s4p6hhdj9lyz0mnnhbva1m0b9sqd257r9ish") (y #t)))

