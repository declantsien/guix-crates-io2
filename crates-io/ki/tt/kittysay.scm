(define-module (crates-io ki tt kittysay) #:use-module (crates-io))

(define-public crate-kittysay-0.1.0 (c (n "kittysay") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-stdin") (r "^0.4.0") (d #t) (k 0)))) (h "189dc8lf8dakla8l3isflyrfx6hkmhcy7pra75144ad7j268dfj4")))

(define-public crate-kittysay-0.2.0 (c (n "kittysay") (v "0.2.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-stdin") (r "^0.4.0") (d #t) (k 0)))) (h "07xm738w4a1491c3nghpax6rj1ajpp3qdv26h88dm8b9ikc7pi2b")))

(define-public crate-kittysay-0.3.0 (c (n "kittysay") (v "0.3.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-stdin") (r "^0.4.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "10dw8jc3c8xibzswrcfz5a10kvx9i221brcc55a6bkin2fgis6cn")))

(define-public crate-kittysay-0.4.0 (c (n "kittysay") (v "0.4.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-stdin") (r "^0.4.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1ycb2rbiaknsqxkm7ylz893la8hrq9pgrfkxplcdysfkv1y28cjr")))

(define-public crate-kittysay-0.5.0 (c (n "kittysay") (v "0.5.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-stdin") (r "^0.4.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "0nhw1b0af9pyqmn3hk1ramw24zsvalyrqis35ylbvkcqa1azsa9q")))

(define-public crate-kittysay-0.6.0 (c (n "kittysay") (v "0.6.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-stdin") (r "^0.4.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "1p4kajmrgik2fcgid3phaahc49gpr3z53wvn2p8lw110civwbxv1")))

