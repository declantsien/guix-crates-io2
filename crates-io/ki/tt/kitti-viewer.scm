(define-module (crates-io ki tt kitti-viewer) #:use-module (crates-io))

(define-public crate-kitti-viewer-0.1.2 (c (n "kitti-viewer") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (f (quote ("suggestions" "color"))) (d #t) (k 0)) (d (n "kiss3d") (r "^0.24.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21") (d #t) (k 0)))) (h "0glvkyaz8lvaxf9hxvg02v05npgnwsb6yji5mnhbkq6sc88whpf7")))

