(define-module (crates-io ki tt kitti-dataset-nalgebra) #:use-module (crates-io))

(define-public crate-kitti-dataset-nalgebra-0.3.0 (c (n "kitti-dataset-nalgebra") (v "0.3.0") (d (list (d (n "kitti-dataset") (r "^0.3.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.5") (d #t) (k 0)) (d (n "slice-of-array") (r "^0.3.2") (d #t) (k 0)))) (h "0gw4j820n3ql32b8qr4x51xmkjwz3alkql95hzk36ni5a32b6ilb")))

