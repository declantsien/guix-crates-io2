(define-module (crates-io ki tt kittyfb) #:use-module (crates-io))

(define-public crate-kittyfb-0.1.0 (c (n "kittyfb") (v "0.1.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "03vg43kpqfnsyx5lv981y9na18398x3cjj9f9gwnj8042gr6szyi")))

(define-public crate-kittyfb-0.1.1 (c (n "kittyfb") (v "0.1.1") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "15p4y9j3y8yi151rwq4nrvcalij4vkbs24v0byrq5qs6dcnb6xkw")))

(define-public crate-kittyfb-0.1.2 (c (n "kittyfb") (v "0.1.2") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1hfi3q7rf5ilbv5qh6vpjd2k64bs73r9pv10026757cdq6ky7lfb")))

