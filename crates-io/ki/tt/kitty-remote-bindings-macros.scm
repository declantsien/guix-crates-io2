(define-module (crates-io ki tt kitty-remote-bindings-macros) #:use-module (crates-io))

(define-public crate-kitty-remote-bindings-macros-0.1.0 (c (n "kitty-remote-bindings-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "kitty-remote-bindings-core") (r "^0.1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1j3jg9ldz2g7h8srb24ikl8aiw1kbkixxwhrxrjzz0sglwvlf9qr")))

(define-public crate-kitty-remote-bindings-macros-0.1.1 (c (n "kitty-remote-bindings-macros") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "kitty-remote-bindings-core") (r "^0.1.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0xkfqb5cnpvyjv5maijdq2gig8zkvkfp0mhvdj8yp404qw9hrndw")))

