(define-module (crates-io ki tt kittycad-execution-plan-traits) #:use-module (crates-io))

(define-public crate-kittycad-execution-plan-traits-0.1.0 (c (n "kittycad-execution-plan-traits") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0saj3sz9yl1vd4z09ndvcf1hn4qs5h2dy79pc2j37z4zlh274p53") (r "1.74")))

(define-public crate-kittycad-execution-plan-traits-0.1.1 (c (n "kittycad-execution-plan-traits") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1p7dvcv5f9vhk09yfxnfh397si4k1v9gxgg57mz38dyi3ql2gvpp") (r "1.74")))

(define-public crate-kittycad-execution-plan-traits-0.1.2 (c (n "kittycad-execution-plan-traits") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "03lwfabsgfsgd1csr79pd0r0432k7p39y0khwzhpfhiwzpcmm7m4") (r "1.74")))

(define-public crate-kittycad-execution-plan-traits-0.1.3 (c (n "kittycad-execution-plan-traits") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0wsj8v81q6281kb7dwfapk4vnpfy3x39jc95i8c83x7cmip431hl") (r "1.74")))

(define-public crate-kittycad-execution-plan-traits-0.1.4 (c (n "kittycad-execution-plan-traits") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1bqk485pjca9hw4vyd7adngkbrasw610a4whay8vfyqkky0d4kls") (r "1.74")))

(define-public crate-kittycad-execution-plan-traits-0.1.5 (c (n "kittycad-execution-plan-traits") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1irdnphqb7d6s1lgnhlvnia7yhhcmdy7yqg9iycnswlbm665bp19") (r "1.74")))

(define-public crate-kittycad-execution-plan-traits-0.1.6 (c (n "kittycad-execution-plan-traits") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1bx225fc7w9i20j6s71248kf81qka511hn41jxkbp5bxhgvr7y6n") (r "1.74")))

(define-public crate-kittycad-execution-plan-traits-0.1.7 (c (n "kittycad-execution-plan-traits") (v "0.1.7") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1w3kjiq6f6mkai5zh7dinqchcvklk5lzcmaxlchi1qccskyv1n29") (r "1.74")))

(define-public crate-kittycad-execution-plan-traits-0.1.8 (c (n "kittycad-execution-plan-traits") (v "0.1.8") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "01v8igkwx37khpjqb134c2ryylhmrqw1p4p79r0c66pj79drx3nf") (r "1.74")))

(define-public crate-kittycad-execution-plan-traits-0.1.9 (c (n "kittycad-execution-plan-traits") (v "0.1.9") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "06i5d7x3kyinjrn9chm2prfrym11cf7h6z4vhyc5xg39bkrwvxyf") (r "1.74")))

(define-public crate-kittycad-execution-plan-traits-0.1.10 (c (n "kittycad-execution-plan-traits") (v "0.1.10") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "198b52ifn49la52lxwambbzgsi6vwzzw0qqb2kmrg5mmazyqxv53") (r "1.74")))

(define-public crate-kittycad-execution-plan-traits-0.1.11 (c (n "kittycad-execution-plan-traits") (v "0.1.11") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0gnl2flyagapfj4pnbrbyjkl4k3kxkrfarh00yxqz2f5nbg6953i") (r "1.74")))

(define-public crate-kittycad-execution-plan-traits-0.1.12 (c (n "kittycad-execution-plan-traits") (v "0.1.12") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "16b29x4x5iphhqrx19glisa6jh1wb1424l1krn86yzb8y91d81qr") (r "1.74")))

(define-public crate-kittycad-execution-plan-traits-0.1.13 (c (n "kittycad-execution-plan-traits") (v "0.1.13") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1msv3hvh7xdpwajpsxi9wiii388zfqwswim0b6mkx8c3kdl6a3r0") (r "1.74")))

(define-public crate-kittycad-execution-plan-traits-0.1.14 (c (n "kittycad-execution-plan-traits") (v "0.1.14") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "03kfxn1k13nb0abja95wfk8m934hb5awz9z415cq5ycivc24jb9x") (r "1.74")))

(define-public crate-kittycad-execution-plan-traits-0.1.15 (c (n "kittycad-execution-plan-traits") (v "0.1.15") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0l9if9jlbaklw9bv8vdrsg9qicr7g8ixpd37mbrqxsl04xzb8g0j") (r "1.74")))

