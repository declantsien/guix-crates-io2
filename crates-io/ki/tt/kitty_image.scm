(define-module (crates-io ki tt kitty_image) #:use-module (crates-io))

(define-public crate-kitty_image-0.1.0 (c (n "kitty_image") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (o #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 2)))) (h "0djpqfm8gs6x3mlm9lxgrqw8pv186n51vvzcjy55xdq1x3hxigjr")))

