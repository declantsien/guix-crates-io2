(define-module (crates-io ki tt kitty_pool) #:use-module (crates-io))

(define-public crate-kitty_pool-0.1.0 (c (n "kitty_pool") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "getset") (r "^0.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (d #t) (k 0)) (d (n "tokio-async-await") (r "^0.1.7") (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "05fligjdsb1ravbzjacr1y89309gxk2i8p8b138my1jf9vhvzfd5") (f (quote (("unstable")))) (y #t)))

(define-public crate-kitty_pool-0.1.1 (c (n "kitty_pool") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "getset") (r "^0.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (d #t) (k 0)) (d (n "tokio-async-await") (r "^0.1.7") (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1ixf1d0hayk0f41l649jpjns2a8s43712r3akvflv86glfhyizfd") (f (quote (("unstable")))) (y #t)))

(define-public crate-kitty_pool-0.2.0 (c (n "kitty_pool") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "getset") (r "^0.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0rmvnvdi45pw1lx00j7fva4qaxh943zh0a6hmwafzzkgs2j66a2f") (f (quote (("unstable")))) (y #t)))

(define-public crate-kitty_pool-0.3.0 (c (n "kitty_pool") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.24") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.4") (d #t) (k 0)) (d (n "getset") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "uuid") (r "^0.8.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1a41x0m8l6my8l3g7l7mbzzswicvrnms00a7g3rzs704w74h2p1w") (f (quote (("unstable"))))))

