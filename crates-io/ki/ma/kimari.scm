(define-module (crates-io ki ma kimari) #:use-module (crates-io))

(define-public crate-kimari-0.1.0 (c (n "kimari") (v "0.1.0") (d (list (d (n "kimari-derive") (r "=0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.19") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "059rlyhvj1f0c1lz9bbaljkik0rhqcgyl8ka10iim4z1g24pay74") (r "1.68.0")))

