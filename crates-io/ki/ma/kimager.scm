(define-module (crates-io ki ma kimager) #:use-module (crates-io))

(define-public crate-kimager-0.1.0 (c (n "kimager") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bimap") (r "^0.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.11.0") (f (quote ("v1_20"))) (k 0)) (d (n "kube") (r "^0.52.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.4") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "16viqlq8yx8kdgcxvjs15pgjghj78aixkkdvkxma22av6pr2hh1x")))

