(define-module (crates-io ki ma kimari-derive) #:use-module (crates-io))

(define-public crate-kimari-derive-0.1.0 (c (n "kimari-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bk4mb8pp5dl801dampfikas8hhia72v9g8gs8wzh8rlnz8kr2zm") (r "1.68.0")))

