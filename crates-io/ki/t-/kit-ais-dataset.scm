(define-module (crates-io ki t- kit-ais-dataset) #:use-module (crates-io))

(define-public crate-kit-ais-dataset-0.1.0 (c (n "kit-ais-dataset") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "noisy_float") (r "^0.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5.0") (d #t) (k 0)))) (h "1xmkyazbf8grcm9rdfjb8aj1qn285p3vazbfdyz7hkyr60cizmv5")))

