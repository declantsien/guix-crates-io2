(define-module (crates-io ki ot kioto) #:use-module (crates-io))

(define-public crate-kioto-0.0.0 (c (n "kioto") (v "0.0.0") (h "0b1sqcilg23hikkm89barjya1n3a2y5974sybj4fxds2nr1in9lw")))

(define-public crate-kioto-0.0.1 (c (n "kioto") (v "0.0.1") (h "1r2lvqwkqm728k8n940c1g4d5gy5g6yzr25x6s5vwhq9b50zgc0s")))

