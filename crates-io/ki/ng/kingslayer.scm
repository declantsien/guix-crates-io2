(define-module (crates-io ki ng kingslayer) #:use-module (crates-io))

(define-public crate-kingslayer-0.1.0 (c (n "kingslayer") (v "0.1.0") (d (list (d (n "rand") (r "^0.6") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12fgjk1v0k75n3jsmijx3rvyc2370gjyyvzjv3b232pddxxf7k4s")))

(define-public crate-kingslayer-0.2.0 (c (n "kingslayer") (v "0.2.0") (d (list (d (n "rand") (r "^0.6") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r2z9f319vqr3q08hsghvw6s9mgqv0pgbfwwhmhgzr6c9whvg1fc")))

(define-public crate-kingslayer-0.2.1 (c (n "kingslayer") (v "0.2.1") (d (list (d (n "rand") (r "^0.6") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13w4navcbhhj0d68xp0g82hid4k6vdd03wnx64408jnq7q6hi4cr")))

(define-public crate-kingslayer-0.4.0 (c (n "kingslayer") (v "0.4.0") (d (list (d (n "rand") (r "^0.6") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10svlxg9as35y7vip70dslh1jqj50gzal5sybnq8z239b3hxq0h4")))

(define-public crate-kingslayer-0.4.1 (c (n "kingslayer") (v "0.4.1") (d (list (d (n "rand") (r "^0.6") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hqws001iz5v42r4wq7d95vs4cxlpcn47v6hmbvva39afj4wc05q")))

(define-public crate-kingslayer-0.4.2 (c (n "kingslayer") (v "0.4.2") (d (list (d (n "rand") (r "^0.6") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19pc2ncydnp7xllmmzdydfvylxmqilsa88h79mblkzrl6jb8vbh9")))

(define-public crate-kingslayer-0.4.3 (c (n "kingslayer") (v "0.4.3") (d (list (d (n "rand") (r "^0.7") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "rayon") (r "^1.2") (d #t) (k 0)) (d (n "ron") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0c5ijjzf28ak13yvknc9igxn6vwjr06jdbapaakh2wmc5vsx0wcg")))

(define-public crate-kingslayer-0.5.0 (c (n "kingslayer") (v "0.5.0") (d (list (d (n "rand") (r "^0.7") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "ron") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f0k3kv8z2sc0l2kaf6723ns6sy9aynvc42fxhlsiz18vk9mmkqp") (y #t)))

(define-public crate-kingslayer-0.5.1 (c (n "kingslayer") (v "0.5.1") (d (list (d (n "rand") (r "^0.7") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "ron") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1w32rd4905z5ysaybk9l2aga1iwpk07x86k86x209p62ka4hq5lm")))

(define-public crate-kingslayer-0.5.2 (c (n "kingslayer") (v "0.5.2") (d (list (d (n "rand") (r "^0.7") (f (quote ("wasm-bindgen"))) (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "ron") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "03jk8rqp3qdjnq9vg0zb33pgd1sixh4inlqnz5mfzsp3i3qj87xa")))

(define-public crate-kingslayer-0.5.3 (c (n "kingslayer") (v "0.5.3") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1r5q054crwkhzh4ks2n86m8n2hpv18nmgrxxr38gg48pklbjnzrp")))

(define-public crate-kingslayer-0.5.4 (c (n "kingslayer") (v "0.5.4") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0779zqz84aa8nq1mbdg6x5v4cav76fi4ldljbl7g3kjy5zdabdvv")))

(define-public crate-kingslayer-0.5.5 (c (n "kingslayer") (v "0.5.5") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "00z4hgz6jbjqflkl3zn9r1x7j3vg70h47cgjk7g9jkj3v5c9jqxb")))

