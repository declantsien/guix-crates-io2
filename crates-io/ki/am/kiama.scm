(define-module (crates-io ki am kiama) #:use-module (crates-io))

(define-public crate-kiama-0.1.0 (c (n "kiama") (v "0.1.0") (h "0x91wk152j6nwvk6bczy1z92s891cvlrnjm0ybs73amnxblvjcvk") (y #t) (r "1.77.0")))

(define-public crate-kiama-0.0.0 (c (n "kiama") (v "0.0.0") (h "1572b72jmnbp3af7ixwgrzjscg7xn1wgvv4gyx8nmccdk24sgz2r") (r "1.77.0")))

