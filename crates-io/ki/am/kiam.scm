(define-module (crates-io ki am kiam) #:use-module (crates-io))

(define-public crate-kiam-0.1.0 (c (n "kiam") (v "0.1.0") (h "1qjwx6zggvfrw27qh4cq16grcq28p1agi2i9r8msrcwigaq35v2g")))

(define-public crate-kiam-0.1.1 (c (n "kiam") (v "0.1.1") (h "1jzx71gn8p9c9hi23wy9mlqq96gcdprl456xvvlya6p4hljc63yb")))

