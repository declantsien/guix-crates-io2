(define-module (crates-io ki tk kitkat) #:use-module (crates-io))

(define-public crate-kitkat-0.1.0 (c (n "kitkat") (v "0.1.0") (h "12j0f3hvvnja59ijjm7f3rx4ig9zjyp7xdv3wg54vfc290byg0nx") (y #t)))

(define-public crate-kitkat-0.1.1 (c (n "kitkat") (v "0.1.1") (h "1b7m4j1f7j9r7xmm8ggxyh5d4ifiar0v8gr5k1xkh80znbhw09wa") (y #t)))

(define-public crate-kitkat-1.0.0 (c (n "kitkat") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "minifb") (r "^0.19.1") (d #t) (k 0)))) (h "1dii0sjn3993k7ba39s173l1l4rbxhzi126wrnw6yla1fsryc8m2")))

(define-public crate-kitkat-1.1.0 (c (n "kitkat") (v "1.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "minifb") (r "^0.19.1") (d #t) (k 0)))) (h "06rrajx9dfchi0r17k2lyn69qck5x9fckdy0g8w375lpynm109sd")))

(define-public crate-kitkat-1.1.1 (c (n "kitkat") (v "1.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "minifb") (r "^0.19.1") (d #t) (k 0)))) (h "0lyhgi23sncazizg6g1xp5gg2pypk56s3w72vqr5hgzfqmh85svh")))

(define-public crate-kitkat-1.1.2 (c (n "kitkat") (v "1.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "minifb") (r "^0.19") (d #t) (k 0)))) (h "1r7vkils10lfvia3g5vnd58qffval61vc5j59fyxwdjq28ik4vps")))

(define-public crate-kitkat-1.1.3 (c (n "kitkat") (v "1.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "minifb") (r "^0.19") (d #t) (k 0)))) (h "0qsg7bavg14whhws6wgjj9wchkia7x6b8wvhm1siz2x35hc0q1wb")))

