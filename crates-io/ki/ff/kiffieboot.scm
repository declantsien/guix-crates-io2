(define-module (crates-io ki ff kiffieboot) #:use-module (crates-io))

(define-public crate-kiffieboot-0.1.0 (c (n "kiffieboot") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 2)) (d (n "mips-mcu") (r "^0.3.0") (d #t) (k 0)) (d (n "pic32-hal") (r "^0.10.0") (f (quote ("pic32mx2xxfxxxb" "usb-device"))) (d #t) (k 2)) (d (n "pic32mx2xx") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "pic32mx470") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "usb-device") (r "^0.2.9") (d #t) (k 2)) (d (n "usbd-dfu-rt") (r "^0.3.1") (o #t) (d #t) (k 0)))) (h "0i59jybr78d0cc6zr1nyp9fmaq47fziwng5kpp09hsj19c36pz2g") (f (quote (("pic32mx470" "pic32mx470/pic32mx47xfxxxl") ("pic32mx2x4" "pic32mx2xx/pic32mx2x4fxxxd") ("pic32mx2x0" "pic32mx2xx/pic32mx2xxfxxxb") ("default" "usbd-dfu-rt"))))))

