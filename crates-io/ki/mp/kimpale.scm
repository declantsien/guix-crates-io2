(define-module (crates-io ki mp kimpale) #:use-module (crates-io))

(define-public crate-kimpale-0.1.0 (c (n "kimpale") (v "0.1.0") (h "10vimkhvc9gxd28rk71xr0f38jd1cascx1s43c0rindsaxfpx6j9") (y #t)))

(define-public crate-kimpale-0.1.1 (c (n "kimpale") (v "0.1.1") (h "1v7ic117idzaf00yhss4acl78wcy93qk46b2gb2wdn1k1cxr9b8j") (y #t)))

