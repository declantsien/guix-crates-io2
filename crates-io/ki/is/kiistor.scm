(define-module (crates-io ki is kiistor) #:use-module (crates-io))

(define-public crate-kiistor-0.1.7 (c (n "kiistor") (v "0.1.7") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.3") (f (quote ("runtime-tokio-rustls" "postgres"))) (d #t) (k 0)) (d (n "tabled") (r "^0.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "087kl3kjplhpbz1vfbiniwb9ql698jf21papxh0lmpsy76zhjbjg")))

(define-public crate-kiistor-0.1.8 (c (n "kiistor") (v "0.1.8") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.3") (f (quote ("runtime-tokio-rustls" "postgres"))) (d #t) (k 0)) (d (n "tabled") (r "^0.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1pxkcwvgq97p7sbp49hp2947hxfvpb3pq74rjn9mja6im20rqsjp")))

(define-public crate-kiistor-0.1.10 (c (n "kiistor") (v "0.1.10") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.3") (f (quote ("runtime-tokio-rustls" "postgres"))) (d #t) (k 0)) (d (n "tabled") (r "^0.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1a0nrwbkg2b8n5x62222jwd4jcv040ks10r72hakq25049kq78dl")))

