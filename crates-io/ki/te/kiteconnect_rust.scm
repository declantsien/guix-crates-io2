(define-module (crates-io ki te kiteconnect_rust) #:use-module (crates-io))

(define-public crate-kiteconnect_rust-0.1.0 (c (n "kiteconnect_rust") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "hyper") (r "^0.11.6") (d #t) (k 0)) (d (n "mockito") (r "^0.9.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k40w2dshywf6k1d8ij1lh5x8gm33ncqzmidq8x57xva9dnpwwpj") (y #t)))

(define-public crate-kiteconnect_rust-0.1.1 (c (n "kiteconnect_rust") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "hyper") (r "^0.11.6") (d #t) (k 0)) (d (n "mockito") (r "^0.9.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "196lq7p5wrwgdrfavw8bnxxbpmb1807dr7kzbiz0l4iwmkv1lc8v") (y #t)))

