(define-module (crates-io ki tc kitchen_sink) #:use-module (crates-io))

(define-public crate-kitchen_sink-0.1.0 (c (n "kitchen_sink") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)))) (h "1zm619q057s7aa83ccfh3mf33x9p80fm20icsrdvww1a8w43kcfd") (y #t)))

(define-public crate-kitchen_sink-0.1.1 (c (n "kitchen_sink") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "1lg5m76spclpyg3pc776mmff103jggjbixhx5d5aa8xdgs1vf96l")))

(define-public crate-kitchen_sink-0.2.0 (c (n "kitchen_sink") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "fastrand") (r "^1.5.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "06rz7nnmyi02fscc76nk539c5iqzrk04pzgv7c2c2w53zn8ca2cc")))

(define-public crate-kitchen_sink-0.3.0 (c (n "kitchen_sink") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "fastrand") (r "^1.5.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "0ik160xqzs3rgpx53gh3rz9klx8i88yi0n27zwmiaqfysil33v8v")))

