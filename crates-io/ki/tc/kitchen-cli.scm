(define-module (crates-io ki tc kitchen-cli) #:use-module (crates-io))

(define-public crate-kitchen-cli-0.2.0 (c (n "kitchen-cli") (v "0.2.0") (d (list (d (n "question") (r "^0.2.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0apg8mfi370qnpbm4b020ihmny62dnayf5x8f5sklk7d3d5s7zdm")))

