(define-module (crates-io ki tc kitchen) #:use-module (crates-io))

(define-public crate-kitchen-0.1.0 (c (n "kitchen") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0szjk68ghh035qswya89lm4mgjysf560dkmgzjkgmkv5scrrvm6i")))

(define-public crate-kitchen-0.1.1 (c (n "kitchen") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "1w4f23z6qzqqa4z2b0qz1h37f5550d3hwy8yc97xkrsbmcazc1hy")))

(define-public crate-kitchen-0.2.0 (c (n "kitchen") (v "0.2.0") (d (list (d (n "question") (r "^0.2.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "1dcjn7hm4rqj4c18favp567fcbrsn2x4gsrnhijk34vfm1qrnjib")))

