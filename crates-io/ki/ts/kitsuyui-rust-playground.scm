(define-module (crates-io ki ts kitsuyui-rust-playground) #:use-module (crates-io))

(define-public crate-kitsuyui-rust-playground-0.1.0 (c (n "kitsuyui-rust-playground") (v "0.1.0") (h "0d0w3l6p0bman03n66fj3pbd6i0g4ld6ckb45jxz3n4jhavqpbfh")))

(define-public crate-kitsuyui-rust-playground-0.1.2 (c (n "kitsuyui-rust-playground") (v "0.1.2") (h "15i5w3pkscim8gdqjpczmaa3cd2gcypjw0xjq9nn1pgi30ipjk06")))

(define-public crate-kitsuyui-rust-playground-0.1.3 (c (n "kitsuyui-rust-playground") (v "0.1.3") (h "04s8qxxdmb77f04lxcynkrnlr2xs5yvcz484v74lxdzra3js7qsc")))

(define-public crate-kitsuyui-rust-playground-0.1.5 (c (n "kitsuyui-rust-playground") (v "0.1.5") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (d #t) (k 0)))) (h "0a48vs5nr24by5f48l4rlifycsbsn3d4vrsmvwc37j2rk894r4f1")))

(define-public crate-kitsuyui-rust-playground-0.1.6 (c (n "kitsuyui-rust-playground") (v "0.1.6") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.45") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (d #t) (k 0)))) (h "1m80jbg3my7zhnwix1jpnznn3vgs2jiy0q6lhjdhi10rdglycbxf")))

(define-public crate-kitsuyui-rust-playground-0.1.7 (c (n "kitsuyui-rust-playground") (v "0.1.7") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.57") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (d #t) (k 0)))) (h "09x78cl69ywq2yzggh5gprcc3hvv3j1kbvb9y3vi20kniirhmj3a")))

(define-public crate-kitsuyui-rust-playground-0.1.8 (c (n "kitsuyui-rust-playground") (v "0.1.8") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.57") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "openssl-probe") (r "^0.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (d #t) (k 0)))) (h "1za30jcsak9m977wrgddhm2hrw6ca83fxd2mks7d20ffs3bl9png")))

