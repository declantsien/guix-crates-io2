(define-module (crates-io ki ts kitsune) #:use-module (crates-io))

(define-public crate-kitsune-0.1.0 (c (n "kitsune") (v "0.1.0") (d (list (d (n "console") (r "^0.7.7") (d #t) (k 0)) (d (n "indicatif") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "0a7j53q6yvk2w0q4ajvbd65w4zzblyza45am5p2i5blrvd1d7hyk")))

