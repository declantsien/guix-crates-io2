(define-module (crates-io ki ts kitsuyui-rust-playground-lib) #:use-module (crates-io))

(define-public crate-kitsuyui-rust-playground-lib-0.1.5 (c (n "kitsuyui-rust-playground-lib") (v "0.1.5") (h "0v5ggw9mf0m2f1c5a6vy7z07zp8jrn81409y91hdpr9hajy0pz8k")))

(define-public crate-kitsuyui-rust-playground-lib-0.1.6 (c (n "kitsuyui-rust-playground-lib") (v "0.1.6") (h "1zlz6x2wfx4fl54pc2lrqzbm34nc9yy0dkszzbq1vyysj07q7s2w")))

(define-public crate-kitsuyui-rust-playground-lib-0.1.7 (c (n "kitsuyui-rust-playground-lib") (v "0.1.7") (h "07dyhvs0ppn0c36bx7fwf4wdxv0xa61gl85g1q4849qh9yjjg94y")))

(define-public crate-kitsuyui-rust-playground-lib-0.1.8 (c (n "kitsuyui-rust-playground-lib") (v "0.1.8") (h "0l58n7ph9mrpxh5aaagvjjvv9dqpqyyy7rxq91r5af0f20nmiwss")))

