(define-module (crates-io ki ts kitsu-rs) #:use-module (crates-io))

(define-public crate-kitsu-rs-0.1.0 (c (n "kitsu-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1") (d #t) (k 0)))) (h "0cgqcri6a4cx1hfngir7siwypxbinw7y3vk5hp1jg36cwp6y6ksq")))

