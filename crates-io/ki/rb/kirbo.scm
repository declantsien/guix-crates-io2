(define-module (crates-io ki rb kirbo) #:use-module (crates-io))

(define-public crate-kirbo-0.0.1 (c (n "kirbo") (v "0.0.1") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.14") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0jvjbs4pr1v7v9l4dbxfrgr8qcv2zjdy7nrajf2f2c3hflsnw1g5")))

