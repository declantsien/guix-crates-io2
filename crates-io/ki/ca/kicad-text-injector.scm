(define-module (crates-io ki ca kicad-text-injector) #:use-module (crates-io))

(define-public crate-kicad-text-injector-0.2.2 (c (n "kicad-text-injector") (v "0.2.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "dict") (r "^0.1.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "repvar") (r "^0.1.17") (d #t) (k 0)))) (h "0mlv5dk5xjzrxncpn6jyij6vl8pl8s0hrm5yxgmas31skvfcx95m")))

