(define-module (crates-io ki ca kicad_parse_gen) #:use-module (crates-io))

(define-public crate-kicad_parse_gen-0.6.1 (c (n "kicad_parse_gen") (v "0.6.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "0.3.*") (d #t) (k 0)))) (h "1kkanfhcg2f8znlyz1x7jxpv3mhvbxshh00c10hychfjszzapfl2")))

(define-public crate-kicad_parse_gen-0.7.0 (c (n "kicad_parse_gen") (v "0.7.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "0.4.*") (d #t) (k 0)))) (h "1mwrgj36xvcfw49kfvwmc2vvsf5y38p92148jmk99q5jwq2ds5zc")))

(define-public crate-kicad_parse_gen-0.7.1 (c (n "kicad_parse_gen") (v "0.7.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "0.4.*") (d #t) (k 0)))) (h "1d67n8nnd28cgjm83zrq155755vwkbd3n6586nnhgxwy7bzdj7h7")))

(define-public crate-kicad_parse_gen-0.7.2 (c (n "kicad_parse_gen") (v "0.7.2") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "0.4.*") (d #t) (k 0)))) (h "0h36ggkz1nb7cback8pv066n3hm20bhpy8naq10vx4y21iasv1ac")))

(define-public crate-kicad_parse_gen-0.7.3 (c (n "kicad_parse_gen") (v "0.7.3") (d (list (d (n "symbolic_expressions") (r "0.4.*") (d #t) (k 0)))) (h "0dww9d84k0lq0py9nkpr16vkgz5a81533iyqq6smk65m5hv6jqzn")))

(define-public crate-kicad_parse_gen-1.0.1 (c (n "kicad_parse_gen") (v "1.0.1") (d (list (d (n "symbolic_expressions") (r "1.0.*") (d #t) (k 0)))) (h "1aicf5cck86g2xmhydivglfkgapmd8f8knnkfx81jax83ggxc7sm")))

(define-public crate-kicad_parse_gen-1.0.2 (c (n "kicad_parse_gen") (v "1.0.2") (d (list (d (n "symbolic_expressions") (r "1.0.*") (d #t) (k 0)))) (h "0lr4a8vvb7wqksblnpci33y7jzir3r5bkwi11q7ynr4bs6dpzhyr")))

(define-public crate-kicad_parse_gen-1.0.3 (c (n "kicad_parse_gen") (v "1.0.3") (d (list (d (n "symbolic_expressions") (r "1.0.*") (d #t) (k 0)))) (h "0rw79610izwqpm3zqxf401dgqa9j0502swzsk2ff1qq4kknc8k8w")))

(define-public crate-kicad_parse_gen-1.0.4 (c (n "kicad_parse_gen") (v "1.0.4") (d (list (d (n "symbolic_expressions") (r "1.0.*") (d #t) (k 0)))) (h "0aahl6m56v6zs729zc3w85x6l91vvry32pv43i6w4pjx09f75cs6")))

(define-public crate-kicad_parse_gen-1.0.5 (c (n "kicad_parse_gen") (v "1.0.5") (d (list (d (n "symbolic_expressions") (r "1.0.*") (d #t) (k 0)))) (h "1469lwxzmfl8n4gamzww90qayy1ishki1plqxa8byjgbc7gdma3z")))

(define-public crate-kicad_parse_gen-1.0.6 (c (n "kicad_parse_gen") (v "1.0.6") (d (list (d (n "symbolic_expressions") (r "1.0.*") (d #t) (k 0)))) (h "1pcrn7a7p6b51hvzfyq4skfg6g97x8cf73ccd5433kzjq0n6zjdr")))

(define-public crate-kicad_parse_gen-1.0.7 (c (n "kicad_parse_gen") (v "1.0.7") (d (list (d (n "symbolic_expressions") (r "1.0.*") (d #t) (k 0)))) (h "1davwh70h3kyifg0b3ap4324y4ghynirs2xy7c586d2d2n5qpp5s")))

(define-public crate-kicad_parse_gen-3.0.4 (c (n "kicad_parse_gen") (v "3.0.4") (d (list (d (n "difference") (r "^0.4") (d #t) (k 2)) (d (n "error-chain") (r "^0.8") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.0.7") (d #t) (k 0)))) (h "1l71919zwzxy5b7xyvaqfcimdfgy95i4v3fa2xz8ygshf3vg1zml")))

(define-public crate-kicad_parse_gen-3.0.5 (c (n "kicad_parse_gen") (v "3.0.5") (d (list (d (n "difference") (r "^0.4") (d #t) (k 2)) (d (n "error-chain") (r "^0.8") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.0.7") (d #t) (k 0)))) (h "1g69i7lni4mrw9zg5v7w4m0ri52dkm222nvlkcdxib0fa87im644")))

(define-public crate-kicad_parse_gen-3.0.6 (c (n "kicad_parse_gen") (v "3.0.6") (d (list (d (n "difference") (r "^0.4") (d #t) (k 2)) (d (n "error-chain") (r "^0.8") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.0.7") (d #t) (k 0)))) (h "0gijpjpmhmam4d7zk2jm81r0my1s5vm9mwi5ji7ig048wcn6mbrx")))

(define-public crate-kicad_parse_gen-3.0.7 (c (n "kicad_parse_gen") (v "3.0.7") (d (list (d (n "difference") (r "^0.4") (d #t) (k 2)) (d (n "error-chain") (r "^0.8") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.0.7") (d #t) (k 0)))) (h "0g85pfqwkj9pgl38lb1c723slwg8hsqwaf2k0jhl8gdxg0bvyl50")))

(define-public crate-kicad_parse_gen-4.0.0 (c (n "kicad_parse_gen") (v "4.0.0") (d (list (d (n "difference") (r "^0.4") (d #t) (k 2)) (d (n "error-chain") (r "^0.8") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.0.7") (d #t) (k 0)))) (h "0zkmrbfybw6rnf4r6zhvk9m8lb4wc2qbqr9kwys722y2xniraadr")))

(define-public crate-kicad_parse_gen-4.1.0 (c (n "kicad_parse_gen") (v "4.1.0") (d (list (d (n "difference") (r "^0.4") (d #t) (k 2)) (d (n "error-chain") (r "^0.8") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.0.7") (d #t) (k 0)))) (h "1a4d54d57cn0w3ba90xf614vylzbaz7v8gjbi6w3r4gay99mrjqa")))

(define-public crate-kicad_parse_gen-4.1.1 (c (n "kicad_parse_gen") (v "4.1.1") (d (list (d (n "difference") (r "^0.4") (d #t) (k 2)) (d (n "error-chain") (r "^0.8") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.0.7") (d #t) (k 0)))) (h "167d3ywd5d67b9k40mpdl30y0rqnna7ryyw42y3lfmqaikd52a0d")))

(define-public crate-kicad_parse_gen-4.1.2 (c (n "kicad_parse_gen") (v "4.1.2") (d (list (d (n "difference") (r "^0.4") (d #t) (k 2)) (d (n "error-chain") (r "^0.8") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.0.7") (d #t) (k 0)))) (h "0wgnqsa85yjlh7yxkkx8dsprm3p08w1lm1xys7kv41php7ci5ysk")))

(define-public crate-kicad_parse_gen-4.1.3 (c (n "kicad_parse_gen") (v "4.1.3") (d (list (d (n "difference") (r "^0.4") (d #t) (k 2)) (d (n "error-chain") (r "^0.8") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.0.7") (d #t) (k 0)))) (h "1dmk66x9yk4cz63anniw3vb37fmy6w1238m4irwabdijjhg4f65f")))

(define-public crate-kicad_parse_gen-4.1.4 (c (n "kicad_parse_gen") (v "4.1.4") (d (list (d (n "difference") (r "^0.4") (d #t) (k 2)) (d (n "error-chain") (r "^0.8") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.0.7") (d #t) (k 0)))) (h "0hl0bwczz2j8jzjp82gihp20rd12irwn1sx5nqn91pfbsf2z6ifq")))

(define-public crate-kicad_parse_gen-4.1.5 (c (n "kicad_parse_gen") (v "4.1.5") (d (list (d (n "difference") (r "^0.4") (d #t) (k 2)) (d (n "error-chain") (r "^0.8") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.0.7") (d #t) (k 0)))) (h "10hsqfhil5mfpmgx3ja5zyvx6zmcf788h9a1n0wqwkykvc8mr7dh")))

(define-public crate-kicad_parse_gen-4.1.6 (c (n "kicad_parse_gen") (v "4.1.6") (d (list (d (n "difference") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.0.7") (d #t) (k 0)))) (h "0im4lh6bnrn8n7bjfd23iq7v9a5gzlkzvn20xnhimcz92yjjf1xp")))

(define-public crate-kicad_parse_gen-4.1.7 (c (n "kicad_parse_gen") (v "4.1.7") (d (list (d (n "difference") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.0.7") (d #t) (k 0)))) (h "0m7jjpkmkp7q0g1ymsppd2b1qb5d79vlhqz93imaa69q49gs3bkl")))

(define-public crate-kicad_parse_gen-4.1.8 (c (n "kicad_parse_gen") (v "4.1.8") (d (list (d (n "difference") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "0plkkinha4iajcr7gamcybj44rz5qmd9g8y8k0pq57rwr8k0idkg")))

(define-public crate-kicad_parse_gen-4.2.0 (c (n "kicad_parse_gen") (v "4.2.0") (d (list (d (n "difference") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "1jz93hkrblcxf53m9bzmjpkwx1shy6mw8cqp2nswhilfy9gx5b0j")))

(define-public crate-kicad_parse_gen-4.2.1 (c (n "kicad_parse_gen") (v "4.2.1") (d (list (d (n "difference") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "067m6fd49ahzzrw0q3nnrvk0mhqyq5ij040fmm1bm809k9315ba4")))

(define-public crate-kicad_parse_gen-4.2.2 (c (n "kicad_parse_gen") (v "4.2.2") (d (list (d (n "difference") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "092v7clgmvqm0lhl880aq9hc43233ymkl0rii6mw48xxanyrg7hw")))

(define-public crate-kicad_parse_gen-4.3.0 (c (n "kicad_parse_gen") (v "4.3.0") (d (list (d (n "difference") (r "^0.4") (d #t) (k 2)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "0kx11lsahwn4694bw50vhfcimw6k0v1ysi1dhv8i4a266lqnnpl0")))

(define-public crate-kicad_parse_gen-4.3.1 (c (n "kicad_parse_gen") (v "4.3.1") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "0kj0qs0b5kb4j3ibv27535gx1hq1jlbbsqmjml2x2q18b5pl6780")))

(define-public crate-kicad_parse_gen-4.3.2 (c (n "kicad_parse_gen") (v "4.3.2") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "0xn7y1nxd0khz3253qrl09755qj01723k5i0myiq1d604ay71nr0")))

(define-public crate-kicad_parse_gen-4.4.0 (c (n "kicad_parse_gen") (v "4.4.0") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "13hia6p6j919jn5j2dbgwj4w1z5k4ds7skik0gsqhjnk5ys5l38h")))

(define-public crate-kicad_parse_gen-4.4.2 (c (n "kicad_parse_gen") (v "4.4.2") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "15i4pj3d5156jj4g1ixzx3nai539bszz63wj8z32i3m251dnngnw")))

(define-public crate-kicad_parse_gen-4.5.0 (c (n "kicad_parse_gen") (v "4.5.0") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "1mklxzf9kjglkgvrbmhwsf94li0asgzng7khk53w9i2kkwdzpqz7")))

(define-public crate-kicad_parse_gen-4.5.1 (c (n "kicad_parse_gen") (v "4.5.1") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "0ar9h3dhl1xmq824sv1pq09x3934if3g61j8jpap9xl7p4dgg2nn")))

(define-public crate-kicad_parse_gen-4.5.2 (c (n "kicad_parse_gen") (v "4.5.2") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "0mpnf75p3ylz1gsf89svz3dq3dcli3r18sn1n5plrd8lfba6anhj")))

(define-public crate-kicad_parse_gen-4.5.3 (c (n "kicad_parse_gen") (v "4.5.3") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "1yridhr4zlgwi1a7b4jfiwzbv4z25xr8fjj0njsnm2wx5qs3w51a")))

(define-public crate-kicad_parse_gen-4.5.4 (c (n "kicad_parse_gen") (v "4.5.4") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "1jdzgzaa0a3qlgrrynlahpc3aslc76lqzxcfiax943ra93jfd5lv")))

(define-public crate-kicad_parse_gen-4.5.5 (c (n "kicad_parse_gen") (v "4.5.5") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "0cqmvav04615pibx3nr5xwffsb0kva259465vn0x3n37wmxa0r9q")))

(define-public crate-kicad_parse_gen-4.5.6 (c (n "kicad_parse_gen") (v "4.5.6") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "0gc2w49gz0wj59pc7fww0wc5v8yjm2g0mwl8cdw9xssl0hpkzq7g")))

(define-public crate-kicad_parse_gen-4.6.0 (c (n "kicad_parse_gen") (v "4.6.0") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "0ll9zyjsivdnffs364vs6g6p13w2d08db1kgkk2jv9dc13h28x70")))

(define-public crate-kicad_parse_gen-4.6.1 (c (n "kicad_parse_gen") (v "4.6.1") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "1q4w6hd1dd3zbc2l4gnjsjb0pjb3hmxxhhk9aikz0dy03yk1l6s8")))

(define-public crate-kicad_parse_gen-4.7.0 (c (n "kicad_parse_gen") (v "4.7.0") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "01bvvl4wn5hxdxqsasv1xj31cn1780plas8rv9kjjh2fpqbx8lx4")))

(define-public crate-kicad_parse_gen-4.7.1 (c (n "kicad_parse_gen") (v "4.7.1") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "0cdssgrvx24pclr6l44prjhw194j8nb3qf22s5kdaz05nq2pcch3")))

(define-public crate-kicad_parse_gen-4.7.2 (c (n "kicad_parse_gen") (v "4.7.2") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "1b2sr6cgrd7lr1sw55aw23jickkprdz2a8syiahyv1lxq6c6fbl5")))

(define-public crate-kicad_parse_gen-4.7.4 (c (n "kicad_parse_gen") (v "4.7.4") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "19bi86fpxraci0qxj0arycq0if01fn7qglsxji4d72200gc1m9gx")))

(define-public crate-kicad_parse_gen-4.7.5 (c (n "kicad_parse_gen") (v "4.7.5") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "05xwqhyp9a9y9fqfhq6aszd5r2nvb074b411lraldrkn4vhjrgid")))

(define-public crate-kicad_parse_gen-4.7.6 (c (n "kicad_parse_gen") (v "4.7.6") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "1b7m5h61ad43iravsnhgzhaas0181b543d6lvjlsyxm1sg7wam72")))

(define-public crate-kicad_parse_gen-5.0.0 (c (n "kicad_parse_gen") (v "5.0.0") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "1vpcqjz5vg4zi2sf4hz82c7xihzll1arizwlbkgm8ar8xcyh75fh")))

(define-public crate-kicad_parse_gen-5.0.1 (c (n "kicad_parse_gen") (v "5.0.1") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "1sws1jii5wix0w7vm9qy66707lvp6vvawl1v7pkfx73cwhzm10df")))

(define-public crate-kicad_parse_gen-5.0.2 (c (n "kicad_parse_gen") (v "5.0.2") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "0xg4pk8ik2wdn3nhv5a0npx64h3hc47id5m9xcr1qndcr2jqgls3")))

(define-public crate-kicad_parse_gen-5.0.3 (c (n "kicad_parse_gen") (v "5.0.3") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "14bwn4a6kfkv387x73j4xdkqdw8qqdha2i08ixa4p98gvrlsszvm")))

(define-public crate-kicad_parse_gen-5.0.4 (c (n "kicad_parse_gen") (v "5.0.4") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "1fwyfqf2jh8xj0ixxl7qpjynz0ixi6q2vilz8n5ad9c4lm55wfkz")))

(define-public crate-kicad_parse_gen-5.0.5 (c (n "kicad_parse_gen") (v "5.0.5") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "0cwwcn5fl1ssrxhgf5slh8666n366sivg6ymlj78kmhvizsvhvc6")))

(define-public crate-kicad_parse_gen-5.0.6 (c (n "kicad_parse_gen") (v "5.0.6") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "0gb1sgh571djzsfcxss3h769afkq4my2igk1y9hscy6vcc4gmi5b")))

(define-public crate-kicad_parse_gen-5.0.7 (c (n "kicad_parse_gen") (v "5.0.7") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "1nm79ih2vz6xciwfw2xn044cp5wkn5m9jb6znh4w0cxjs67c5fj7")))

(define-public crate-kicad_parse_gen-5.0.8 (c (n "kicad_parse_gen") (v "5.0.8") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "0sg0vp81zb3niyffndbb2vd1xr3djw4vps6hz52r7fmm5k1p8mpi")))

(define-public crate-kicad_parse_gen-5.0.9 (c (n "kicad_parse_gen") (v "5.0.9") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "05i613bmp5sfcscalqql4yb6sxa6v6x4xgyrz8934c4d3k90c62l")))

(define-public crate-kicad_parse_gen-5.0.10 (c (n "kicad_parse_gen") (v "5.0.10") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "0ybmmxan82yjwdy04jy6ipn4w3gpk3zrk09vc9w7q65gkpdjr83h")))

(define-public crate-kicad_parse_gen-5.0.11 (c (n "kicad_parse_gen") (v "5.0.11") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "0c34cpqfpbi2155b7gm6w30c9s0az1fba5dllrs2falsl22iwb48")))

(define-public crate-kicad_parse_gen-5.0.12 (c (n "kicad_parse_gen") (v "5.0.12") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "1r1ck06291qnmkzvr82k1gh5nbf1sfypw2ffyq4amly1fr0nkv90")))

(define-public crate-kicad_parse_gen-5.0.13 (c (n "kicad_parse_gen") (v "5.0.13") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "117igmr7hza5xfkr1r1626w8g3q8m78nawbl0498j0g7cin2d5sq")))

(define-public crate-kicad_parse_gen-5.0.14 (c (n "kicad_parse_gen") (v "5.0.14") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "1l9szar1ipijvbw7b17ajjbfrhfqc4p7ikd3rhsdcv703v1gj1jf")))

(define-public crate-kicad_parse_gen-5.0.15 (c (n "kicad_parse_gen") (v "5.0.15") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "0cy4033l4n6blk6ki1svjrqvbdm2y61pn424grf4pil0nl3hvh4s")))

(define-public crate-kicad_parse_gen-5.0.16 (c (n "kicad_parse_gen") (v "5.0.16") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "1xjzdyq3dy7w5vsmy8r4hyrpa2fa1w90pyp0z8j8hvb8svw3hbpj")))

(define-public crate-kicad_parse_gen-5.0.17 (c (n "kicad_parse_gen") (v "5.0.17") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "191592cny1nccvpgg0pxsdh6rlv6fhsqk9bhk640is9n0algimhh")))

(define-public crate-kicad_parse_gen-5.0.18 (c (n "kicad_parse_gen") (v "5.0.18") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "0q3bcg2qmrc2rp232q5wx62rq7nlpn9013lpln26idihh3myr46c")))

(define-public crate-kicad_parse_gen-5.0.19 (c (n "kicad_parse_gen") (v "5.0.19") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "0qmqx08ln6jqrvaadcvryjmc42696myr2pj27gd6ywmsda7xlif8")))

(define-public crate-kicad_parse_gen-6.0.0 (c (n "kicad_parse_gen") (v "6.0.0") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "0zgg8mfjc17gbkkkdava0jf88xbpk54am0sa01s5s1s50120a4vp")))

(define-public crate-kicad_parse_gen-6.0.1 (c (n "kicad_parse_gen") (v "6.0.1") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "0rlxvj0zfhrzvgq1n5ms4a3i35y9nk03y8zrhc42bfvbrwirfzps")))

(define-public crate-kicad_parse_gen-6.0.2 (c (n "kicad_parse_gen") (v "6.0.2") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "~4.1.7") (d #t) (k 0)))) (h "0vjr5mvib9sybhki2g2wjw86rm1am59d4mc9mnh9x4r5qvfnwnyc")))

(define-public crate-kicad_parse_gen-7.0.1 (c (n "kicad_parse_gen") (v "7.0.1") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "^5.0") (d #t) (k 0)))) (h "1i474vnbdib1k8p39d5gxgfm3lhy8wvi3sjgk2naadd5jy69vnrf")))

(define-public crate-kicad_parse_gen-7.0.2 (c (n "kicad_parse_gen") (v "7.0.2") (d (list (d (n "difference") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "symbolic_expressions") (r "^5.0") (d #t) (k 0)))) (h "0h396gs8xh1g8490yc1nqxcnk076xsz5zgg310gwgr6rzrv35ymc")))

