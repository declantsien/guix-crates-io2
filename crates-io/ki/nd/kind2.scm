(define-module (crates-io ki nd kind2) #:use-module (crates-io))

(define-public crate-kind2-0.1.0 (c (n "kind2") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hvm") (r "^0.1.20") (d #t) (k 0)) (d (n "stacker") (r "^0.1") (d #t) (k 0)))) (h "013s9z5n8z71ar277yl57fbxsvwk3ga9qaisf1sf5a67dn9c3qa8")))

(define-public crate-kind2-0.1.3 (c (n "kind2") (v "0.1.3") (d (list (d (n "clap") (r "^3.1.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hvm") (r "^0.1.24") (d #t) (k 0)) (d (n "stacker") (r "^0.1") (d #t) (k 0)))) (h "1qacy4d21s13gbmzvcvsn0g515kkhjzwv0d3nifiq6f64cp9qgmf")))

(define-public crate-kind2-0.1.4 (c (n "kind2") (v "0.1.4") (d (list (d (n "clap") (r "^3.1.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hvm") (r "^0.1.24") (d #t) (k 0)) (d (n "stacker") (r "^0.1") (d #t) (k 0)))) (h "1b6w3pah7q3h7nxw2ni24cp22n579y17c6yizp1m0hmf6ij9bxkf")))

(define-public crate-kind2-0.1.5 (c (n "kind2") (v "0.1.5") (d (list (d (n "clap") (r "^3.1.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hvm") (r "^0.1.24") (d #t) (k 0)) (d (n "stacker") (r "^0.1") (d #t) (k 0)))) (h "03j46h79daiv4si1mx6rari2javdq23s64hg49jc0nck51rb4x6d")))

(define-public crate-kind2-0.2.0 (c (n "kind2") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.48") (d #t) (k 0)))) (h "1acrszn5v3n6jykhmw8px9mchzisvagshk7qw3alfqbdjdbn5ki8")))

(define-public crate-kind2-0.2.3 (c (n "kind2") (v "0.2.3") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.48") (d #t) (k 0)))) (h "1xfc70fs31cvvgxw3s1iczydjqgad9hhlgi2q2q1cmcq602rw4p7")))

(define-public crate-kind2-0.2.4 (c (n "kind2") (v "0.2.4") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.48") (d #t) (k 0)))) (h "0qyfpssv9pbky708nkgrrydz6038yh5vbcyq2pj4a66d37ph8mmx")))

(define-public crate-kind2-0.2.5 (c (n "kind2") (v "0.2.5") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.48") (d #t) (k 0)))) (h "0b4p4crvv0r4s3hrnvwx6kshqpxn4p0mdl0jjag0c5k3bqqrrkyw")))

(define-public crate-kind2-0.2.6 (c (n "kind2") (v "0.2.6") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.48") (d #t) (k 0)))) (h "0pgqsqzy2cd84vclbs1qqig96zn5ggyjk01d4w51yr4xf67l78l6")))

(define-public crate-kind2-0.2.10 (c (n "kind2") (v "0.2.10") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.48") (d #t) (k 0)))) (h "0xcfj7wk97dglhc2llc4294fz1k9iwkx036dn8nn3lgx41i3gxx4")))

(define-public crate-kind2-0.2.11 (c (n "kind2") (v "0.2.11") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.48") (d #t) (k 0)))) (h "1n4gsp1qlblbnkd7v6i9vgp6fs0gls6xjpwl0mcwk2ir6nj6cibz")))

(define-public crate-kind2-0.2.12 (c (n "kind2") (v "0.2.12") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.48") (d #t) (k 0)))) (h "1vpbz1gm1p2z59r9n5wrsn0z869kd1dlflf99mw6g32z828y9svd")))

(define-public crate-kind2-0.2.13 (c (n "kind2") (v "0.2.13") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.48") (d #t) (k 0)))) (h "0w92bnxl8512a002lw4v0l9407sby3jipg4d55ldwpx4fcfs1cs7")))

(define-public crate-kind2-0.2.14 (c (n "kind2") (v "0.2.14") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.48") (d #t) (k 0)))) (h "1bvrbl1083bs6ry9zvzksvbmvzrc34ads55v99y0v31n46fcx6zj")))

(define-public crate-kind2-0.2.16 (c (n "kind2") (v "0.2.16") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.48") (d #t) (k 0)))) (h "1xg7ay20c1awqvsi1bxg4j1l67z8idbm0hyzla7kklg7qnhl846m")))

(define-public crate-kind2-0.2.19 (c (n "kind2") (v "0.2.19") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.48") (d #t) (k 0)))) (h "0qk849xnc1iikyraky8496b55lcsn5mrs3v74pgkg563vi6k39ai")))

(define-public crate-kind2-0.2.20 (c (n "kind2") (v "0.2.20") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.49") (d #t) (k 0)))) (h "0mchfmzrxid605x0f0mljz2fm7ph6pa5y4ifcrmb9icxi4w8x00d")))

(define-public crate-kind2-0.2.21 (c (n "kind2") (v "0.2.21") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.52") (d #t) (k 0)))) (h "10by4qyi2574djaarvk9k2cp61xwjwrbrn6gyr9p2x98xajk7nxb")))

(define-public crate-kind2-0.2.22 (c (n "kind2") (v "0.2.22") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.52") (d #t) (k 0)))) (h "1dhcj7v9mkc5x4jkl2a4mh8qg31w1nacy7vfwv4l7psjfzmfyha0")))

(define-public crate-kind2-0.2.23 (c (n "kind2") (v "0.2.23") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.52") (d #t) (k 0)))) (h "1hf48z1bicdkaxgzhnyc6sivyns6i9jj4fivyd42xfqa1jz2b7cl")))

(define-public crate-kind2-0.2.24 (c (n "kind2") (v "0.2.24") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.52") (d #t) (k 0)))) (h "1xmfd1m8wvxbj365zghww4pjqmmavc4qffbl6kqf0323msydf03w")))

(define-public crate-kind2-0.2.25 (c (n "kind2") (v "0.2.25") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.52") (d #t) (k 0)))) (h "18115mibmq464mn96131dm4f0bz4nsc48rw07wrl9yxr3acvvn2w")))

(define-public crate-kind2-0.2.26 (c (n "kind2") (v "0.2.26") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.52") (d #t) (k 0)))) (h "1i32sxmv4qbgafwymvl2h0dh80lnj2cz859k2c8afg3immig0l4w")))

(define-public crate-kind2-0.2.27 (c (n "kind2") (v "0.2.27") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.59") (d #t) (k 0)))) (h "05mhqg2vhj3h9ay85m8n90x4j95p2agkdby9ra5q022cxs9vphiw")))

(define-public crate-kind2-0.2.28 (c (n "kind2") (v "0.2.28") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.59") (d #t) (k 0)))) (h "1ypsmlq9w4s9wglaixbrrnv2rrdvpqxl7v96lmpb3rzcyk8c4bq6")))

(define-public crate-kind2-0.2.29 (c (n "kind2") (v "0.2.29") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.59") (d #t) (k 0)))) (h "0hzr3g4k4na8qw199sq9bkm97wh66g1wm4p0wyaqfr25zvywbi5z")))

(define-public crate-kind2-0.2.30 (c (n "kind2") (v "0.2.30") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.59") (d #t) (k 0)))) (h "0kplp5q2fk206di9hl7b9k2yf3w03648s5kaj6ff2lsq2x77w862")))

(define-public crate-kind2-0.2.31 (c (n "kind2") (v "0.2.31") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.61") (d #t) (k 0)))) (h "1b8gcgcknk4dc8p1pn7qj49v9kjjvywi82ppbg43y6kxk8qny2n6")))

(define-public crate-kind2-0.2.32 (c (n "kind2") (v "0.2.32") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.62") (d #t) (k 0)))) (h "1maa2687jl3dnyfy27bpk79527mqnajzl2xm61fci1qhy5s2py5g")))

(define-public crate-kind2-0.2.37 (c (n "kind2") (v "0.2.37") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.65") (d #t) (k 0)))) (h "018v03g0janbydxpd5gzxl9d5bla1n9addybi3r131c1ihp56snj")))

(define-public crate-kind2-0.2.41 (c (n "kind2") (v "0.2.41") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.69") (d #t) (k 0)))) (h "022lzn8drfbb1rhrxizxcv09xpsclbigi7qjcd8x962mn27qdvy1")))

(define-public crate-kind2-0.2.45 (c (n "kind2") (v "0.2.45") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.69") (d #t) (k 0)))) (h "0fm952isms335ml9i9gbql8m0dq9h2vnky76qcgk6zvljbi6z5fn")))

(define-public crate-kind2-0.2.48 (c (n "kind2") (v "0.2.48") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.70") (d #t) (k 0)))) (h "1s4cam9x5a7fm8j9gvrsqa82lw10wwd4867x394w49jz5x67728q")))

(define-public crate-kind2-0.2.49 (c (n "kind2") (v "0.2.49") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.70") (d #t) (k 0)))) (h "061nwxa2kd21nzn84p6lg1b9cd12va98d57paydw9xgrv3077dbh")))

(define-public crate-kind2-0.2.51 (c (n "kind2") (v "0.2.51") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.70") (d #t) (k 0)))) (h "1h2i0ga2kbdwmj25pr6iia1iaaryf8nrhk5da2zcax01njnjg7ln")))

(define-public crate-kind2-0.2.53 (c (n "kind2") (v "0.2.53") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.70") (d #t) (k 0)))) (h "09sv4d94jmvma67qawpxq53xzf3lmz54a784r3nbs6d52idvb20n")))

(define-public crate-kind2-0.2.54 (c (n "kind2") (v "0.2.54") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.70") (d #t) (k 0)))) (h "1d2s3946h4k88gci0mk3zbbcdysikaywi0j68lhvwr93wqvsp7rg")))

(define-public crate-kind2-0.2.55 (c (n "kind2") (v "0.2.55") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.72") (d #t) (k 0)))) (h "020c7php14jy837m1spl6261089rvz064ydmpwhzwnpclylkm0lh")))

(define-public crate-kind2-0.2.56 (c (n "kind2") (v "0.2.56") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.73") (d #t) (k 0)))) (h "171ivi7sjni0mf59w383hm9k904cx4lpx42gvjq0v7id5b661fmk")))

(define-public crate-kind2-0.2.57 (c (n "kind2") (v "0.2.57") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.73") (d #t) (k 0)))) (h "0bfz74ky60zn3gfvwryg4x7k6w8yici0al6bxz6vdiw91hfja7v5")))

(define-public crate-kind2-0.2.59 (c (n "kind2") (v "0.2.59") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.73") (d #t) (k 0)))) (h "0454fvh4pxyvavcj9vva3qrqjwnk8k9hafa83rn4j956hp2acb2y")))

(define-public crate-kind2-0.2.60 (c (n "kind2") (v "0.2.60") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.75") (d #t) (k 0)))) (h "13bf52ja05sbykqqcc4rpjvssgar76a6r4lzjsj7p0d6sl5s79z2")))

(define-public crate-kind2-0.2.61 (c (n "kind2") (v "0.2.61") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.75") (d #t) (k 0)))) (h "18l7865xfibw62wg8pk3jn4s6h3aw4jh2c89lnklqpn11xpqlj0c")))

(define-public crate-kind2-0.2.62 (c (n "kind2") (v "0.2.62") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.75") (d #t) (k 0)))) (h "09z9n8vxwhnx2y3q2kpbyw2kx37v1i4ibzzga0hspg350pjl6q3p")))

(define-public crate-kind2-0.2.63 (c (n "kind2") (v "0.2.63") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.75") (d #t) (k 0)))) (h "1lh1v5yhg08ssb31yb2hqvw8gacaiqpb1p08si6vanji0r7vn106")))

(define-public crate-kind2-0.2.64 (c (n "kind2") (v "0.2.64") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.75") (d #t) (k 0)))) (h "0yc4pxsmg9gnrycyqryxpw6y2phnmvxdy7m8al9braz987dy5pj6")))

(define-public crate-kind2-0.2.66 (c (n "kind2") (v "0.2.66") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.80") (d #t) (k 0)))) (h "1bqxr6abw3ry661391pmda9m4lydxaipkbxcndfdmj6fs9zg007i")))

(define-public crate-kind2-0.2.67 (c (n "kind2") (v "0.2.67") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.80") (d #t) (k 0)))) (h "0584nfa271z8i38jjcllj8ajmny0h0wmhd56p7a6jwzhhg5slj8g")))

(define-public crate-kind2-0.2.68 (c (n "kind2") (v "0.2.68") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.80") (d #t) (k 0)))) (h "0ya7gxmsgdlv7map8gmhymd31wjnp1jbg8rw18lyyarb69vyjqas")))

(define-public crate-kind2-0.2.69 (c (n "kind2") (v "0.2.69") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.80") (d #t) (k 0)))) (h "0ndrp0facgj6a9ay67snpm4afnxb58cailm28k445v0iwybivs9v")))

(define-public crate-kind2-0.2.70 (c (n "kind2") (v "0.2.70") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.80") (d #t) (k 0)))) (h "1rbw5mfm6h6wkn4kk6nmgh9s48i45nvfx8jpcqbn3r91mlcr6fzs")))

(define-public crate-kind2-0.2.71 (c (n "kind2") (v "0.2.71") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.80") (d #t) (k 0)))) (h "01xrq492ppn1wd0w6kn77z4ii74n7kmay4ywpl492g4ymvkzb219")))

(define-public crate-kind2-0.2.72 (c (n "kind2") (v "0.2.72") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.81") (d #t) (k 0)))) (h "19r5v4x1hh7c3fbkkzh50rn4209nyiz8055wpd79ydhvbv8mpbx1")))

(define-public crate-kind2-0.2.73 (c (n "kind2") (v "0.2.73") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.81") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zz0jk4yyhj1zvdcjh9f6khk64hdv6xps2ps4lp3hfgrwr6vd04s")))

(define-public crate-kind2-0.2.74 (c (n "kind2") (v "0.2.74") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.81") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0b5bi55ncn95z2jn6ycgslmjsd0570c9sf5g4wi3w37w59bq4q6i")))

(define-public crate-kind2-0.2.75 (c (n "kind2") (v "0.2.75") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.81") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rg5z4i7agp575l4cc55i0d2v95i16w686sr7isj6gixkny3y6d9")))

(define-public crate-kind2-0.2.76 (c (n "kind2") (v "0.2.76") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.81") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "07px4xgvr01jli29x6dsvnrz9vghblg6q28rwl2q6hfbd9k1j2lx")))

(define-public crate-kind2-0.2.77 (c (n "kind2") (v "0.2.77") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.81") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "19y63g79sfr65da0hw7iv6l69rcwkgk03xkm2dvymzpzb26kfwf9")))

(define-public crate-kind2-0.2.78 (c (n "kind2") (v "0.2.78") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.81") (d #t) (k 0)) (d (n "ntest") (r "^0.8.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "0cmd6pfis2vi3k1xjwram91y9glk3j83h44kvy6811hsyd4g8yxw")))

(define-public crate-kind2-0.2.79 (c (n "kind2") (v "0.2.79") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "highlight_error") (r "^0.1.1") (d #t) (k 0)) (d (n "hvm") (r "^0.1.81") (d #t) (k 0)) (d (n "ntest") (r "^0.8.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "0a1mm5a5klrnxghviy9cyi6i8xhls7d5scx9arxpl16743nbj1vp")))

(define-public crate-kind2-0.3.6 (c (n "kind2") (v "0.3.6") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "kind-checker") (r "^0.1.0") (d #t) (k 0)) (d (n "kind-driver") (r "^0.1.0") (d #t) (k 0)) (d (n "kind-report") (r "^0.1.0") (d #t) (k 0)))) (h "1qnrnn1gyc9m09kj1js611b8sai5wyqdchm9q5v8372jiks8mxs9")))

(define-public crate-kind2-0.3.7 (c (n "kind2") (v "0.3.7") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "kind-checker") (r "^0.1.0") (d #t) (k 0)) (d (n "kind-driver") (r "^0.1.0") (d #t) (k 0)) (d (n "kind-report") (r "^0.1.0") (d #t) (k 0)))) (h "14fv494j7v9kypfywwgzigkc2abzqbh9dgzjbri3wzf9lsypq5jn")))

(define-public crate-kind2-0.3.8 (c (n "kind2") (v "0.3.8") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "kind-checker") (r "^0.1.1") (d #t) (k 0)) (d (n "kind-driver") (r "^0.1.1") (d #t) (k 0)) (d (n "kind-query") (r "^0.1.1") (d #t) (k 0)) (d (n "kind-report") (r "^0.1.1") (d #t) (k 0)))) (h "1zjavmc051m5iai49a9qrh67hx83zb6f7lwr3iyja9c01lpws6vf")))

(define-public crate-kind2-0.3.9 (c (n "kind2") (v "0.3.9") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "kind-checker") (r "^0.1.2") (d #t) (k 0)) (d (n "kind-driver") (r "^0.1.2") (d #t) (k 0)) (d (n "kind-query") (r "^0.1.2") (d #t) (k 0)) (d (n "kind-report") (r "^0.1.2") (d #t) (k 0)))) (h "0f4l1m6bch7hpsf5qfcxgcdcdg9cmlqz284nrb530w4pcgpqpij7")))

(define-public crate-kind2-0.3.10 (c (n "kind2") (v "0.3.10") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "kind-checker") (r "^0.1.3") (d #t) (k 0)) (d (n "kind-driver") (r "^0.1.3") (d #t) (k 0)) (d (n "kind-query") (r "^0.1.3") (d #t) (k 0)) (d (n "kind-report") (r "^0.1.3") (d #t) (k 0)))) (h "0738a1ggxh9706idgiy84562sbwdwv7q4zaknl8hh9cp2b777nj4")))

