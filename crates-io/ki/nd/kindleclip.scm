(define-module (crates-io ki nd kindleclip) #:use-module (crates-io))

(define-public crate-kindleclip-0.2.0 (c (n "kindleclip") (v "0.2.0") (h "1sn4rcb7yhpd2k80nml8iij22g0agqjifbpfh9xam2sw7cfgaqf4")))

(define-public crate-kindleclip-0.2.1 (c (n "kindleclip") (v "0.2.1") (h "0r22p751r7ayscx3mshgjny0vlscyv4mhvlpwly8gmxxkal1530w")))

(define-public crate-kindleclip-0.3.0 (c (n "kindleclip") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.0") (d #t) (k 0)))) (h "0f5hrv1vhsjwlzhs83xspz01jivfnsgq31c4jm2fah093sa1pivq")))

(define-public crate-kindleclip-0.4.0 (c (n "kindleclip") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "06mrs9pcks9a7vq07bfk4xc02y0n2kg7cay3jsqd8wdijhigw8vb")))

(define-public crate-kindleclip-0.6.0 (c (n "kindleclip") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1mb5d8i3vmkrca0zayz9qav9qp8gvnprp31i7x18p2zjccah6hzs")))

(define-public crate-kindleclip-0.7.0 (c (n "kindleclip") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "036pyj93mwrxxzjb85ppp32fxikwbldrxnr30636i8d51v2n7rqp")))

