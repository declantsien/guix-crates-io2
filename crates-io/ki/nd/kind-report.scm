(define-module (crates-io ki nd kind-report) #:use-module (crates-io))

(define-public crate-kind-report-0.1.0 (c (n "kind-report") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "kind-span") (r "^0.1.0") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "termsize") (r "^0.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "0dga6prxrlya32hcq06p7n5gixba4gmfph4z1pw16zqm4ss2qs8l")))

(define-public crate-kind-report-0.1.1 (c (n "kind-report") (v "0.1.1") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "kind-span") (r "^0.1.1") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "termsize") (r "^0.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "1lqdnczxnc5qpm0vprdd57vrhizj58did2wglq0gjfpq1i3kjrba")))

(define-public crate-kind-report-0.1.2 (c (n "kind-report") (v "0.1.2") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "kind-span") (r "^0.1.2") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "termsize") (r "^0.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "0nb4sqqdyap3c5lqjfa3ywgdg63zigbc24wfhkz35yi8d0kmanr2")))

(define-public crate-kind-report-0.1.3 (c (n "kind-report") (v "0.1.3") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "kind-span") (r "^0.1.3") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "refl") (r "^0.2.1") (d #t) (k 0)) (d (n "termsize") (r "^0.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "1gn6j8z7f96gkjcqm6fqrcg5hnkbxjfnp59q1jw0xm0qqw3mf54m")))

