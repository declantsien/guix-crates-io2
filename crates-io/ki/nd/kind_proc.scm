(define-module (crates-io ki nd kind_proc) #:use-module (crates-io))

(define-public crate-kind_proc-0.1.0 (c (n "kind_proc") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1znj8a1q8k1x7kzjy3dwkqzq7niv36f19cm5pdwblibxmbipwws8") (r "1.65")))

(define-public crate-kind_proc-0.2.0 (c (n "kind_proc") (v "0.2.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "10zacsx4sx55zrdj82xpnazs9lzhdydccqn35k2mi83l8w1d243b") (r "1.65")))

(define-public crate-kind_proc-0.3.0 (c (n "kind_proc") (v "0.3.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1lnjh6qylvq3l3w8l9qwy6y7dr1vrln6gq7i0p88q0phia6588rr") (r "1.65")))

(define-public crate-kind_proc-1.0.0 (c (n "kind_proc") (v "1.0.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "16728hfg9bm4r8lhxzhvrp3c7glq0y6l3padsdpnqj07gf1142b7") (r "1.65")))

