(define-module (crates-io ki nd kindle_highlights) #:use-module (crates-io))

(define-public crate-kindle_highlights-0.3.2 (c (n "kindle_highlights") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.24") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0rv5xqm86kjvb0x5nnip8c5fkzsizw2yjdvj4dv7ikp6aqbvrmjm")))

(define-public crate-kindle_highlights-0.3.3-dev (c (n "kindle_highlights") (v "0.3.3-dev") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.24") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0jdpix0vpd59kjdzg6yklx20c7sjapsv115ix4710ihgsp5ps25r")))

