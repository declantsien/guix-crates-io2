(define-module (crates-io ki nd kindelia_ws) #:use-module (crates-io))

(define-public crate-kindelia_ws-0.1.7 (c (n "kindelia_ws") (v "0.1.7") (d (list (d (n "futures-util") (r "^0.3.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1ps5crb339frzjhh9p7a0km7ck0n15qn34d62y3ri08pg6hygg0p")))

