(define-module (crates-io ki nd kind-config) #:use-module (crates-io))

(define-public crate-kind-config-0.1.0 (c (n "kind-config") (v "0.1.0") (h "04dpxlgh72df7640qwndaxddbp30djx24zcyzbg3l8l1jwqjxwg8")))

(define-public crate-kind-config-0.2.0 (c (n "kind-config") (v "0.2.0") (d (list (d (n "hdf5") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "1f2avfnw29wnr6jhibjnh06a0i2qdh2scwdxfal0fvjzvn1gvinz")))

(define-public crate-kind-config-0.2.1 (c (n "kind-config") (v "0.2.1") (d (list (d (n "hdf5") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "1b6hxpnajlidzngndy0yfhx0c5kp2dp4bkfwm5ayz963d033swba")))

(define-public crate-kind-config-0.2.2 (c (n "kind-config") (v "0.2.2") (d (list (d (n "hdf5") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "1q7d885nycqdrnn1g8grxvvy1fmg52nz5s1rqw6vqnz8szfzgglj")))

(define-public crate-kind-config-0.2.3 (c (n "kind-config") (v "0.2.3") (d (list (d (n "hdf5") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "0c2kq7a2s9smi2z9sb12db88rbpdkxqqckn45h61gg885wln6xpj")))

(define-public crate-kind-config-0.2.4 (c (n "kind-config") (v "0.2.4") (d (list (d (n "hdf5") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "1cr0rxh7l0wb9zign85wrf0gjj92daq6c26c9bbnq71di5bsq1gh")))

(define-public crate-kind-config-0.2.5 (c (n "kind-config") (v "0.2.5") (d (list (d (n "hdf5") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "09j8bji6fdw3w4p8d99b7nzkq4p40pxanvc23yjnq5bw5c1vg5vj")))

(define-public crate-kind-config-0.2.6 (c (n "kind-config") (v "0.2.6") (d (list (d (n "hdf5") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "1sxngpzic13cd6sl4cyaj3z3kfvmjcnqwwj3y6dhfqzqc8r93iay")))

(define-public crate-kind-config-0.2.7 (c (n "kind-config") (v "0.2.7") (d (list (d (n "hdf5") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "0hifzl0mwni04ipaw4vdsrzzrs4gwf32fw9iazw2h41lx3h7l521")))

