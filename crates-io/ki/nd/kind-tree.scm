(define-module (crates-io ki nd kind-tree) #:use-module (crates-io))

(define-public crate-kind-tree-0.1.0 (c (n "kind-tree") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "kind-span") (r "^0.1.0") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.6") (d #t) (k 0)))) (h "0jym6ar3kdmi2kb3zd85v3qp50wxzl4ax1id62jd86kgr9bq4y8v")))

(define-public crate-kind-tree-0.1.1 (c (n "kind-tree") (v "0.1.1") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "hvm") (r "^1.0.3") (d #t) (k 0)) (d (n "kind-span") (r "^0.1.1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.6") (d #t) (k 0)))) (h "1jpdzxsz3k84lhi2s6l61spjfjg00412lbzwqhgwxl81szc476rk")))

(define-public crate-kind-tree-0.1.2 (c (n "kind-tree") (v "0.1.2") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "hvm") (r "^1.0.6") (d #t) (k 0)) (d (n "kind-span") (r "^0.1.2") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.6") (d #t) (k 0)))) (h "0j737x5ikznk6m4394waxwfb222wibdp8as5ykhi4wkvx0daz00s")))

(define-public crate-kind-tree-0.1.3 (c (n "kind-tree") (v "0.1.3") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "hvm") (r "^1.0.8") (d #t) (k 0)) (d (n "kind-span") (r "^0.1.3") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.6") (d #t) (k 0)))) (h "1l106wn8fyxy5hnx9wiwd0836r117cg06sgl021hc8savpmj1rnb")))

