(define-module (crates-io ki nd kindle_formats) #:use-module (crates-io))

(define-public crate-kindle_formats-0.1.0 (c (n "kindle_formats") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "09gxmkly2fcf95im0k5wm8cakd4nf2dpd52h61ir6rxg4sb96p3p") (f (quote (("krds") ("default" "krds"))))))

(define-public crate-kindle_formats-0.1.1 (c (n "kindle_formats") (v "0.1.1") (d (list (d (n "linked-hash-map") (r "^0.5.6") (f (quote ("serde_impl"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xj0rjkg6xhr6b1rhk4024i0ifgb3qdsw7x1r4dnxni7sg3whm44") (f (quote (("krds") ("default" "krds")))) (s 2) (e (quote (("linked_hash_maps" "dep:linked-hash-map"))))))

(define-public crate-kindle_formats-0.1.2 (c (n "kindle_formats") (v "0.1.2") (d (list (d (n "linked-hash-map") (r "^0.5.6") (f (quote ("serde_impl"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wy8jzb2ds307wsjxxiwm00hazxgjjnxkv8wfha5lz2nwdacb34d") (f (quote (("krds") ("default" "krds")))) (s 2) (e (quote (("linked_hash_maps" "dep:linked-hash-map"))))))

(define-public crate-kindle_formats-0.1.3 (c (n "kindle_formats") (v "0.1.3") (d (list (d (n "linked-hash-map") (r "^0.5.6") (f (quote ("serde_impl"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "11mx9f55m90s9s98y5dxsw1bxn3wv6s0w9m07mhwrc5yf32k90yj") (f (quote (("krds") ("default" "krds")))) (s 2) (e (quote (("linked_hash_maps" "dep:linked-hash-map"))))))

