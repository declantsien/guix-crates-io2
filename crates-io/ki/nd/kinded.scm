(define-module (crates-io ki nd kinded) #:use-module (crates-io))

(define-public crate-kinded-0.0.1 (c (n "kinded") (v "0.0.1") (d (list (d (n "kinded_macros") (r "^0.1.0") (d #t) (k 0)))) (h "006nx40plkhvpkn2d9kqny2i5ah1himp98vyw1hhrlj3q6rvaazs")))

(define-public crate-kinded-0.0.2 (c (n "kinded") (v "0.0.2") (d (list (d (n "kinded_macros") (r "^0.0.2") (d #t) (k 0)))) (h "14j4qlrsq7ap27kss17h52ljf40jcnli1j22fnlxhfrigyd188dm")))

(define-public crate-kinded-0.0.3 (c (n "kinded") (v "0.0.3") (d (list (d (n "kinded_macros") (r "^0.0.3") (d #t) (k 0)))) (h "117gd2szrwxn6kbp4k6jp3s9q0dg6r7yy63dkynpk3m16gkcfs87")))

(define-public crate-kinded-0.1.1 (c (n "kinded") (v "0.1.1") (d (list (d (n "kinded_macros") (r "^0.1.1") (d #t) (k 0)))) (h "1rjfv0q3ywjn8wkv56076lb3hgk5j4ll7c2jrliqi1xg4vrzi97y")))

(define-public crate-kinded-0.2.0 (c (n "kinded") (v "0.2.0") (d (list (d (n "kinded_macros") (r "^0.2.0") (d #t) (k 0)))) (h "0g027frivhnrqv41crfi63zsawzrg9c78grnmlpvm525pb71802w")))

(define-public crate-kinded-0.3.0 (c (n "kinded") (v "0.3.0") (d (list (d (n "kinded_macros") (r "^0.3.0") (d #t) (k 0)))) (h "0k7kc8jy5qxflg80r12zvpc34ir1h98i3xz9y0chnri3yjrdnjyf")))

