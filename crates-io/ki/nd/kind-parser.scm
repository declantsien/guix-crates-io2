(define-module (crates-io ki nd kind-parser) #:use-module (crates-io))

(define-public crate-kind-parser-0.1.0 (c (n "kind-parser") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "kind-report") (r "^0.1.0") (d #t) (k 0)) (d (n "kind-span") (r "^0.1.0") (d #t) (k 0)) (d (n "kind-tree") (r "^0.1.0") (d #t) (k 0)))) (h "17wqlk6kk0a6k3y2xma92spi2n783g7813924qdgr0szbblmwc5k")))

(define-public crate-kind-parser-0.1.1 (c (n "kind-parser") (v "0.1.1") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "kind-report") (r "^0.1.1") (d #t) (k 0)) (d (n "kind-span") (r "^0.1.1") (d #t) (k 0)) (d (n "kind-tree") (r "^0.1.1") (d #t) (k 0)))) (h "1nmlwbjnd034a6565ak69nkplcp9im7qw047knqzhkf44a8x097k")))

(define-public crate-kind-parser-0.1.2 (c (n "kind-parser") (v "0.1.2") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "kind-report") (r "^0.1.2") (d #t) (k 0)) (d (n "kind-span") (r "^0.1.2") (d #t) (k 0)) (d (n "kind-tree") (r "^0.1.2") (d #t) (k 0)))) (h "1sxidic2vckqx3dknrmz0gqna2mrc7g3kihiq5ffy2nns1w64rmj")))

(define-public crate-kind-parser-0.1.3 (c (n "kind-parser") (v "0.1.3") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "kind-report") (r "^0.1.3") (d #t) (k 0)) (d (n "kind-span") (r "^0.1.3") (d #t) (k 0)) (d (n "kind-tree") (r "^0.1.3") (d #t) (k 0)))) (h "13pzpnlgsfh14jk6l2pydhpaf32fdk63i1l32vfy4izh5gin47af")))

