(define-module (crates-io ki nd kindness) #:use-module (crates-io))

(define-public crate-kindness-0.2.0 (c (n "kindness") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "insta") (r "^1.21.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "1ry6izmwp2lc3rwzmf32sxgj36i7v0ms8bq65qh4iknkn5xbjin0")))

(define-public crate-kindness-0.3.0 (c (n "kindness") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "insta") (r "^1.21.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "13wvgmqnzw916jb3xh66l47vzr282laqs2j7p5jxfp3kizrnq1j0")))

(define-public crate-kindness-0.4.0 (c (n "kindness") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "insta") (r "^1.21.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "0x44am9kvd5m1cqvc32r9n02633960xa11zbc7bz3sn5cvr6xgm3") (f (quote (("std"))))))

