(define-module (crates-io ki nd kind-derive) #:use-module (crates-io))

(define-public crate-kind-derive-0.1.0 (c (n "kind-derive") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "im-rc") (r "^15.1.0") (d #t) (k 0)) (d (n "kind-report") (r "^0.1.0") (d #t) (k 0)) (d (n "kind-span") (r "^0.1.0") (d #t) (k 0)) (d (n "kind-tree") (r "^0.1.0") (d #t) (k 0)))) (h "0jh3wlr8xl419x3hcv4c8ibpw41wbki26h9cj4j91wrqbmwnbf86")))

(define-public crate-kind-derive-0.1.1 (c (n "kind-derive") (v "0.1.1") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "im-rc") (r "^15.1.0") (d #t) (k 0)) (d (n "kind-report") (r "^0.1.1") (d #t) (k 0)) (d (n "kind-span") (r "^0.1.1") (d #t) (k 0)) (d (n "kind-tree") (r "^0.1.1") (d #t) (k 0)))) (h "17lbw6spzvizkjlh38l7l9363f20askcsb85zsa3nm11cj3pfbfj")))

(define-public crate-kind-derive-0.1.2 (c (n "kind-derive") (v "0.1.2") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "im-rc") (r "^15.1.0") (d #t) (k 0)) (d (n "kind-report") (r "^0.1.2") (d #t) (k 0)) (d (n "kind-span") (r "^0.1.2") (d #t) (k 0)) (d (n "kind-tree") (r "^0.1.2") (d #t) (k 0)))) (h "14m6c94lm16hwq3np8z5qz3jh146hhn5shlk5vn9d2403vxwqs6s")))

(define-public crate-kind-derive-0.1.3 (c (n "kind-derive") (v "0.1.3") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "im-rc") (r "^15.1.0") (d #t) (k 0)) (d (n "kind-report") (r "^0.1.3") (d #t) (k 0)) (d (n "kind-span") (r "^0.1.3") (d #t) (k 0)) (d (n "kind-tree") (r "^0.1.3") (d #t) (k 0)))) (h "1zf7wjf9vn718pzrcj1cg4w61vnvli40dwwgwlxjl36ws888617n")))

