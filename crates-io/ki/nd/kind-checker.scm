(define-module (crates-io ki nd kind-checker) #:use-module (crates-io))

(define-public crate-kind-checker-0.1.0 (c (n "kind-checker") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "hvm") (r "^1.0.0") (d #t) (k 0)) (d (n "im-rc") (r "^15.1.0") (d #t) (k 0)) (d (n "kind-report") (r "^0.1.0") (d #t) (k 0)) (d (n "kind-span") (r "^0.1.0") (d #t) (k 0)) (d (n "kind-tree") (r "^0.1.0") (d #t) (k 0)))) (h "1fv82r7vihp9si6c56xz7nfdpyjl2lqdjra8q36zxbj1bpxxml44")))

(define-public crate-kind-checker-0.1.1 (c (n "kind-checker") (v "0.1.1") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "hvm") (r "^1.0.3") (d #t) (k 0)) (d (n "im-rc") (r "^15.1.0") (d #t) (k 0)) (d (n "kind-report") (r "^0.1.1") (d #t) (k 0)) (d (n "kind-span") (r "^0.1.1") (d #t) (k 0)) (d (n "kind-tree") (r "^0.1.1") (d #t) (k 0)))) (h "04irlda9z4nv4lwmgbvhmnfhj14lyw2vqjax2n707g9f40yx64qz")))

(define-public crate-kind-checker-0.1.2 (c (n "kind-checker") (v "0.1.2") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "hvm") (r "^1.0.6") (d #t) (k 0)) (d (n "im-rc") (r "^15.1.0") (d #t) (k 0)) (d (n "kind-report") (r "^0.1.2") (d #t) (k 0)) (d (n "kind-span") (r "^0.1.2") (d #t) (k 0)) (d (n "kind-tree") (r "^0.1.2") (d #t) (k 0)))) (h "1lif188fra4fbcdkiqxkbfj7yzdmzpklszdhsn71ybyqwkdbfm38")))

(define-public crate-kind-checker-0.1.3 (c (n "kind-checker") (v "0.1.3") (d (list (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "hvm") (r "^1.0.8") (d #t) (k 0)) (d (n "im-rc") (r "^15.1.0") (d #t) (k 0)) (d (n "kind-report") (r "^0.1.3") (d #t) (k 0)) (d (n "kind-span") (r "^0.1.3") (d #t) (k 0)) (d (n "kind-tree") (r "^0.1.3") (d #t) (k 0)))) (h "129bkd7iv2zmr73fwsr4nmglfqx7cazy60c5vhfiixbl4ww9i2mh")))

