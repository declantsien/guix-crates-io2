(define-module (crates-io ki nd kindelia_server) #:use-module (crates-io))

(define-public crate-kindelia_server-0.1.7 (c (n "kindelia_server") (v "0.1.7") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "kindelia_common") (r "^0.1.7") (d #t) (k 0)) (d (n "kindelia_core") (r "^0.1.7") (d #t) (k 0)) (d (n "kindelia_lang") (r "^0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.9") (f (quote ("net"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1kshb2333j45cr9jdi0413wf45l4plsrj8nagd5cymi1zkw0qbi5")))

