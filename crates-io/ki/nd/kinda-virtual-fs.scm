(define-module (crates-io ki nd kinda-virtual-fs) #:use-module (crates-io))

(define-public crate-kinda-virtual-fs-0.1.0 (c (n "kinda-virtual-fs") (v "0.1.0") (h "1ywsdmnrina3gadyflkwsbpr5cwhq0qgr6d871k01fbc2fczyhk3") (y #t)))

(define-public crate-kinda-virtual-fs-0.1.1 (c (n "kinda-virtual-fs") (v "0.1.1") (h "17kccqzhkdffwgg7y8l8jr9ykwh95777gm151xjva5vrq6m2b4ya")))

