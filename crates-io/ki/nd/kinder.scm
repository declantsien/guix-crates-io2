(define-module (crates-io ki nd kinder) #:use-module (crates-io))

(define-public crate-kinder-0.1.0 (c (n "kinder") (v "0.1.0") (h "027v2l34xs6ybdxpf61hp3iqhykr3vvy7i1iizmky5v4kndzfhgp")))

(define-public crate-kinder-0.1.1 (c (n "kinder") (v "0.1.1") (h "12pzqr2sk2xbxrz79fljpzc9zsyk7f98bdj4mq6cxcdb5q4m9p8a")))

(define-public crate-kinder-0.1.2 (c (n "kinder") (v "0.1.2") (h "16rynairlfj3p8ss9zz3zajv1v6bl0zg8dg5kah8nac3frs3qbq2")))

(define-public crate-kinder-0.1.3 (c (n "kinder") (v "0.1.3") (h "1i28w6npk9rpx1cqnacy7ic95x54cshazz55i4jak1rrmn4nl13i")))

