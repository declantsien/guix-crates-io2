(define-module (crates-io ki nd kind-target-hvm) #:use-module (crates-io))

(define-public crate-kind-target-hvm-0.1.0 (c (n "kind-target-hvm") (v "0.1.0") (d (list (d (n "hvm") (r "^1.0.0") (d #t) (k 0)) (d (n "kind-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "kind-report") (r "^0.1.0") (d #t) (k 0)) (d (n "kind-span") (r "^0.1.0") (d #t) (k 0)) (d (n "kind-tree") (r "^0.1.0") (d #t) (k 0)))) (h "0m4n1x1kcvjyqr9mgfdv2hcj3kj8hblh9qyxphm0187s5xynax1c")))

(define-public crate-kind-target-hvm-0.1.1 (c (n "kind-target-hvm") (v "0.1.1") (d (list (d (n "hvm") (r "^1.0.3") (d #t) (k 0)) (d (n "kind-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "kind-report") (r "^0.1.1") (d #t) (k 0)) (d (n "kind-span") (r "^0.1.1") (d #t) (k 0)) (d (n "kind-tree") (r "^0.1.1") (d #t) (k 0)))) (h "0p7b18nn2qr44nj10hqyjrc42i10a7xd7k383106h6klrp744q75")))

(define-public crate-kind-target-hvm-0.1.2 (c (n "kind-target-hvm") (v "0.1.2") (d (list (d (n "hvm") (r "^1.0.6") (d #t) (k 0)) (d (n "kind-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "kind-report") (r "^0.1.2") (d #t) (k 0)) (d (n "kind-span") (r "^0.1.2") (d #t) (k 0)) (d (n "kind-tree") (r "^0.1.2") (d #t) (k 0)))) (h "07la54n7fh97apllq4ja0v5naqnnjhz99dvrkkp991qi2sxja1pn")))

(define-public crate-kind-target-hvm-0.1.3 (c (n "kind-target-hvm") (v "0.1.3") (d (list (d (n "hvm") (r "^1.0.8") (d #t) (k 0)) (d (n "kind-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "kind-report") (r "^0.1.3") (d #t) (k 0)) (d (n "kind-span") (r "^0.1.3") (d #t) (k 0)) (d (n "kind-tree") (r "^0.1.3") (d #t) (k 0)))) (h "0fg9s7dnqqnqdk0sdpvrcx4dmfnr8fd6cq23zjk5hy0mlh00xvh6")))

