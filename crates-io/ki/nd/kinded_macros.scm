(define-module (crates-io ki nd kinded_macros) #:use-module (crates-io))

(define-public crate-kinded_macros-0.1.0 (c (n "kinded_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "14zj71vw59iish0p5ga189ls5k30aazpzj0mi0xcqycwq77cbn45")))

(define-public crate-kinded_macros-0.0.2 (c (n "kinded_macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1l6n2iaw3imp26257yw2fc3v4p6q6k7q5zqi8l6l1qkd5hz163rg")))

(define-public crate-kinded_macros-0.0.3 (c (n "kinded_macros") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1dxp7pvp1vk3i5s1pw1nmwd6sp5a2rs74ckb2sbj6bsy8aqsw7yi")))

(define-public crate-kinded_macros-0.1.1 (c (n "kinded_macros") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0q1lq514485616zxsnfsksavfwvp3pfhml7xglcjmdf2p6718ghq")))

(define-public crate-kinded_macros-0.2.0 (c (n "kinded_macros") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "09nqh394z3qp2xx991bq36d3jq31m60i6vwm5da885rcyiprrfby")))

(define-public crate-kinded_macros-0.3.0 (c (n "kinded_macros") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "13982c6nrvym7kk2g151cjcxnbx5l9nn0vrxmifz8cnbbpf4sfx1")))

