(define-module (crates-io ki nt kintone-rs) #:use-module (crates-io))

(define-public crate-kintone-rs-0.1.0 (c (n "kintone-rs") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0f25c1m93zh8axq8qbnd4pl1w82h6kqbss1l990ja2clfpqdzsx2")))

(define-public crate-kintone-rs-0.2.0 (c (n "kintone-rs") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1w9lqv6fsk3p7ak746anwha8zy7c1y8j8maarpgvjlbw8c04x8nv")))

(define-public crate-kintone-rs-0.3.0 (c (n "kintone-rs") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1dxjk83hsi05amsqq110nyhr9y3f7mxybzhr2dy84nxfpn215542")))

(define-public crate-kintone-rs-0.3.1 (c (n "kintone-rs") (v "0.3.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1mk01r47k6bn6vwwwzjdv4idv3z2r307g6g235pdl808m6qggsl4")))

(define-public crate-kintone-rs-0.3.2 (c (n "kintone-rs") (v "0.3.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0p4v0wi36ysmkdxkvqjy0h9r5dd8dk0adl1bf6pii4y0gmyni0xg")))

