(define-module (crates-io ki nt kinto_http) #:use-module (crates-io))

(define-public crate-kinto_http-0.1.0 (c (n "kinto_http") (v "0.1.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0qgydx48hznm7pagmhgkd23vwgyfhf1b66xygp0m9wp5zddsg35s")))

