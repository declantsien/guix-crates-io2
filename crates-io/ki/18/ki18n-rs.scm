(define-module (crates-io ki #{18}# ki18n-rs) #:use-module (crates-io))

(define-public crate-ki18n-rs-0.1.0 (c (n "ki18n-rs") (v "0.1.0") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "qmetaobject") (r "^0.2") (d #t) (k 0)) (d (n "qttypes") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 1)))) (h "1n8g3hl4ry03bx5javnq3ak5hhszg5c1l9i3s4ylfk76v6phzii3") (y #t) (l "KF5I18n")))

(define-public crate-ki18n-rs-0.1.1 (c (n "ki18n-rs") (v "0.1.1") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "qmetaobject") (r "^0.2") (d #t) (k 0)) (d (n "qttypes") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 1)))) (h "08sgdz52c5y9f2vks3s54has10zw6pzp3na4z8kd26xijxbyjas6") (y #t) (l "KF5I18n")))

(define-public crate-ki18n-rs-1.0.0 (c (n "ki18n-rs") (v "1.0.0") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "qmetaobject") (r "^0.2") (d #t) (k 0)) (d (n "qttypes") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 1)))) (h "1mk4m8djdwvwjz94q5ayxd51rsxsqk9nsnzyf20y8c2yrf278pja") (y #t) (l "KF5I18n")))

(define-public crate-ki18n-rs-1.0.1 (c (n "ki18n-rs") (v "1.0.1") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "qmetaobject") (r "^0.2") (d #t) (k 0)) (d (n "qttypes") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 1)))) (h "1ypzcpwsspvz96d1cay8h828748g6sv9cfc4zq53brs7l0x1ckjd") (y #t) (l "KF5I18n")))

(define-public crate-ki18n-rs-1.0.2 (c (n "ki18n-rs") (v "1.0.2") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (f (quote ("docs-only"))) (d #t) (k 1)) (d (n "qmetaobject") (r "^0.2") (d #t) (k 0)) (d (n "qttypes") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 1)))) (h "01czn0cnmsh618zdwkvrnk6cgibrb1md8s3bklrx3706q9cckk3h") (y #t) (l "KF5I18n")))

(define-public crate-ki18n-rs-0.1.2 (c (n "ki18n-rs") (v "0.1.2") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "qmetaobject") (r "^0.2") (d #t) (k 0)) (d (n "qttypes") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 1)))) (h "1nimc2zdnnpcbfifcbdx2zp5g3skp3l1r1la9x584vds67aibq8z") (y #t) (l "KF5I18n")))

(define-public crate-ki18n-rs-0.1.3 (c (n "ki18n-rs") (v "0.1.3") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "qmetaobject") (r "^0.2") (d #t) (k 0)) (d (n "qttypes") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 1)))) (h "1mg3qmwjyxl2p32jn9mni236kpwb8g472d7ijzkczdvjzymk39ar") (y #t) (l "KF5I18n")))

(define-public crate-ki18n-rs-1.0.3 (c (n "ki18n-rs") (v "1.0.3") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "qmetaobject") (r "^0.2") (d #t) (k 0)) (d (n "qttypes") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 1)))) (h "1asgn751kp8by2awaqzw2xy7i2072ydpa1dg10zrvq9m7fryswly") (y #t) (l "KF5I18n")))

