(define-module (crates-io ki ib kiibohd-core-ffi) #:use-module (crates-io))

(define-public crate-kiibohd-core-ffi-0.1.0 (c (n "kiibohd-core-ffi") (v "0.1.0") (d (list (d (n "c_utf8") (r "^0.1") (k 0)) (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "kiibohd-hall-effect") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "kiibohd-hid-io-ffi") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "kiibohd-keyscanning") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "kiibohd-usb") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "pkg-version") (r "^1.0") (d #t) (k 0)))) (h "1ca78awpmklnz1r8h4rdqrkmwkpcjka9dwk87aad1gjbgydhg844") (f (quote (("std") ("lib") ("default" "kiibohd-hall-effect" "kiibohd-hid-io-ffi" "kiibohd-keyscanning") ("capi"))))))

