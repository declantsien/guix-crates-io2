(define-module (crates-io ki ib kiibohd-hid-io-ffi) #:use-module (crates-io))

(define-public crate-kiibohd-hid-io-ffi-0.1.0 (c (n "kiibohd-hid-io-ffi") (v "0.1.0") (d (list (d (n "c_utf8") (r "^0.1") (k 0)) (d (n "cstr_core") (r "^0.2") (k 0)) (d (n "heapless") (r "^0.7.10") (d #t) (k 0)) (d (n "hid-io-protocol") (r "^0.1.0") (f (quote ("device"))) (k 0)) (d (n "pkg-version") (r "^1.0") (d #t) (k 0)))) (h "1bvplcxm55g92vkj0dfdslk83kyjbhf632zxpfkysxayc80f674x")))

