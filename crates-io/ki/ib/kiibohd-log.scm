(define-module (crates-io ki ib kiibohd-log) #:use-module (crates-io))

(define-public crate-kiibohd-log-0.1.0 (c (n "kiibohd-log") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7.1") (d #t) (k 0)) (d (n "cortex-m-semihosting") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "rtt-target") (r "^0.3.1") (o #t) (k 0)) (d (n "rtt-target") (r "^0.3.1") (k 2)))) (h "112l411dlzzgb8l1c13hmz82qn533n1l3q34y92b79b5n05cx7ib") (f (quote (("semihosting" "cortex-m-semihosting") ("rtt" "rtt-target/cortex-m"))))))

