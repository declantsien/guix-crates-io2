(define-module (crates-io ki k_ kik_sync_service) #:use-module (crates-io))

(define-public crate-kik_sync_service-0.7.0 (c (n "kik_sync_service") (v "0.7.0") (h "1w7g6ldbz3bar3gcn1fvmyqxqp8x8py2ca1dxl2nvvmndigfyr4v") (y #t)))

(define-public crate-kik_sync_service-0.7.1 (c (n "kik_sync_service") (v "0.7.1") (h "1y6ahr6b8x01h08d7c690hbcr07wr7srnab64f00vffcig8nrr3d") (y #t)))

(define-public crate-kik_sync_service-0.7.2 (c (n "kik_sync_service") (v "0.7.2") (h "1rg3x6agh4jnmdrbc9kvfy7lznjyljcpz6aaj65s5qwgpf6b48x7") (y #t)))

(define-public crate-kik_sync_service-0.7.3 (c (n "kik_sync_service") (v "0.7.3") (h "18wjx0xmdxiwyp30ghndn5rdpisnm3fl7g137mjsjp4pg68mw0g2")))

