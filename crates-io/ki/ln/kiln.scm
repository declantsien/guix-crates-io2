(define-module (crates-io ki ln kiln) #:use-module (crates-io))

(define-public crate-kiln-0.1.0 (c (n "kiln") (v "0.1.0") (h "0lvnkhrwqhvvka5p1blp99qjs5bri9xn40w56b59cwmyz9xxdfvg")))

(define-public crate-kiln-0.1.1 (c (n "kiln") (v "0.1.1") (h "0q0di4686hxab944fz3g6rdib263jwp1rrap287mgrmhh2qwkmd6")))

(define-public crate-kiln-0.1.2 (c (n "kiln") (v "0.1.2") (h "1bn37icqhsz7iaw57frvg4bxabqbf7i83fywlnbs1bi5jqfjwa7i")))

(define-public crate-kiln-0.1.3 (c (n "kiln") (v "0.1.3") (d (list (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "04x9idpwn84n8aj1hdp87k036j71358naqjjmdpyijhrchg1zdf5")))

(define-public crate-kiln-0.2.0 (c (n "kiln") (v "0.2.0") (d (list (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1mx757aahz7c2la9bw1kifyl63197gasgfr2ss2jsgylbw70qd6a")))

