(define-module (crates-io ki wi kiwi-ecs) #:use-module (crates-io))

(define-public crate-kiwi-ecs-1.0.0 (c (n "kiwi-ecs") (v "1.0.0") (d (list (d (n "kiwi-internal-macros") (r "^1.0.0") (d #t) (k 0)) (d (n "kiwi-macros") (r "^1.0.0") (d #t) (k 0)))) (h "12zyyy7f62mf41dgqis02sha93c8ppc53gf81yzjkj30qbs1knj4")))

(define-public crate-kiwi-ecs-1.0.1 (c (n "kiwi-ecs") (v "1.0.1") (d (list (d (n "kiwi-internal-macros") (r "^1.0.0") (d #t) (k 0)) (d (n "kiwi-macros") (r "^1.0.1") (d #t) (k 0)))) (h "13vpcnm9f0g2m7lx1vmr9wrac6wwkyb1dkx9753m1fkqa7h8ivr5")))

(define-public crate-kiwi-ecs-1.0.2 (c (n "kiwi-ecs") (v "1.0.2") (d (list (d (n "kiwi-internal-macros") (r "^1.0.0") (d #t) (k 0)) (d (n "kiwi-macros") (r "^1.0.2") (d #t) (k 0)))) (h "169aq7rfrlf6id8w00zf826i48mra0cjv25pknmaqiblg0n9jbvd")))

(define-public crate-kiwi-ecs-1.0.3 (c (n "kiwi-ecs") (v "1.0.3") (d (list (d (n "kiwi-internal-macros") (r "^1.0.1") (d #t) (k 0)) (d (n "kiwi-macros") (r "^1.0.2") (d #t) (k 0)))) (h "0g8xb3lf5nq2jafv4lqmp7036fm9hbp5s2c76ll5nwh4sm0d33cx")))

(define-public crate-kiwi-ecs-1.0.4 (c (n "kiwi-ecs") (v "1.0.4") (d (list (d (n "kiwi-internal-macros") (r "^1.0.2") (d #t) (k 0)) (d (n "kiwi-macros") (r "^1.0.3") (d #t) (k 0)))) (h "04jlajf3nwcgz75xar4393mc61jrwzzza9a1pnhvs6jggdilsimb") (y #t)))

(define-public crate-kiwi-ecs-1.1.0 (c (n "kiwi-ecs") (v "1.1.0") (d (list (d (n "kiwi-internal-macros") (r "^1.0.2") (d #t) (k 0)) (d (n "kiwi-macros") (r "^1.0.3") (d #t) (k 0)))) (h "16gcd9vx68gwidzahsk0kmkcwb34krffysw01zcaklczmkvh34r1") (y #t)))

(define-public crate-kiwi-ecs-1.1.1 (c (n "kiwi-ecs") (v "1.1.1") (d (list (d (n "kiwi-internal-macros") (r "^1.0.3") (d #t) (k 0)) (d (n "kiwi-macros") (r "^1.0.3") (d #t) (k 0)))) (h "1xk8c5683pn5xixcmiaif0snqavv8hlgvw89i3qks7ha2hm185rw") (y #t)))

(define-public crate-kiwi-ecs-1.1.2 (c (n "kiwi-ecs") (v "1.1.2") (d (list (d (n "kiwi-internal-macros") (r "^1.0.3") (d #t) (k 0)) (d (n "kiwi-macros") (r "^1.0.3") (d #t) (k 0)))) (h "09liqz2wg367y3i0xi3kyys21rlsvw8d1js5icrq9h266ihp6cgn")))

(define-public crate-kiwi-ecs-1.2.0 (c (n "kiwi-ecs") (v "1.2.0") (d (list (d (n "kiwi-internal-macros") (r "^1.0.3") (d #t) (k 0)) (d (n "kiwi-macros") (r "^1.0.4") (d #t) (k 0)))) (h "1zylc13dxl9bhdcafwj0808zw94sgxi293xr6b4mldd4pr57s52v")))

(define-public crate-kiwi-ecs-1.2.1 (c (n "kiwi-ecs") (v "1.2.1") (d (list (d (n "kiwi-internal-macros") (r "^1.0.4") (d #t) (k 0)) (d (n "kiwi-macros") (r "^1.0.4") (d #t) (k 0)))) (h "19hl5nvwl6a8sv6n4hwy5xlsmik6fva4h45k3d0f1pdzrimy2107")))

(define-public crate-kiwi-ecs-1.2.2 (c (n "kiwi-ecs") (v "1.2.2") (d (list (d (n "kiwi-internal-macros") (r "^1.0.4") (d #t) (k 0)) (d (n "kiwi-macros") (r "^1.0.5") (d #t) (k 0)))) (h "1snlswjgsf6233pj53nvcmiz612r2gynb2kfc9832nbkz7virvgx")))

(define-public crate-kiwi-ecs-1.2.3 (c (n "kiwi-ecs") (v "1.2.3") (d (list (d (n "kiwi-internal-macros") (r "^1.0.5") (d #t) (k 0)) (d (n "kiwi-macros") (r "^1.0.5") (d #t) (k 0)))) (h "0l11vfsdrbplnykv13mvbn8hb4xi4z4sjna67afjfnlz518zkgzx")))

(define-public crate-kiwi-ecs-1.3.0 (c (n "kiwi-ecs") (v "1.3.0") (d (list (d (n "kiwi-internal-macros") (r "^1.0.5") (d #t) (k 0)) (d (n "kiwi-macros") (r "^1.0.6") (d #t) (k 0)))) (h "12h77lrb7wjch5rhxbmvr3myqb0msgb01n4w8wgddjpz3syz14rc") (f (quote (("try" "kiwi-macros/try")))) (y #t)))

(define-public crate-kiwi-ecs-1.3.1 (c (n "kiwi-ecs") (v "1.3.1") (d (list (d (n "kiwi-internal-macros") (r "^1.0.5") (d #t) (k 0)) (d (n "kiwi-macros") (r "^1.0.7") (d #t) (k 0)))) (h "0g6s0fsmfymyc6lx62dhpnnmqdgg6wzyij3zzk7wnzgrwml77r44") (f (quote (("try" "kiwi-macros/try"))))))

(define-public crate-kiwi-ecs-1.4.0 (c (n "kiwi-ecs") (v "1.4.0") (d (list (d (n "kiwi-internal-macros") (r "^1.0.5") (d #t) (k 0)) (d (n "kiwi-macros") (r "^1.0.6") (d #t) (k 0)))) (h "0bk8s2j65cflzapfbhi0hpyyn5g8vjz0lih1ilbzdn1pc1p5r5cd") (f (quote (("try" "kiwi-macros/try")))) (y #t)))

(define-public crate-kiwi-ecs-1.4.1 (c (n "kiwi-ecs") (v "1.4.1") (d (list (d (n "kiwi-internal-macros") (r "^1.0.7") (d #t) (k 0)) (d (n "kiwi-macros") (r "^1.0.7") (d #t) (k 0)))) (h "1i8wmlwqphk690sikj3sygpir11lsdl0lb2ag74l8h038i4wms1f") (f (quote (("try" "kiwi-macros/try"))))))

