(define-module (crates-io ki wi kiwi-sdk) #:use-module (crates-io))

(define-public crate-kiwi-sdk-0.1.1-alpha.4 (c (n "kiwi-sdk") (v "0.1.1-alpha.4") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "kiwi-macro") (r "~0.1.1-alpha.4") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.17.0") (d #t) (k 0)))) (h "0r1xliazi0359k9j2vnai2qbgqzfx0r3f6c6jxfc3dwmzq8s6v2v") (r "1.73.0")))

(define-public crate-kiwi-sdk-0.1.1-alpha.5 (c (n "kiwi-sdk") (v "0.1.1-alpha.5") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "kiwi-macro") (r "~0.1.1-alpha.5") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.17.0") (d #t) (k 0)))) (h "09cd8qnc04nd6irkwz7cayyrf7qsn33x4bigcikwhx5c60bfhwkv") (r "1.73.0")))

(define-public crate-kiwi-sdk-0.1.1-alpha.6 (c (n "kiwi-sdk") (v "0.1.1-alpha.6") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "kiwi-macro") (r "~0.1.1-alpha.6") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.17.0") (d #t) (k 0)))) (h "0lm4pvys9fyw20qxjh2dzd433791zjv956qzx1mv6inhkqqby6iz") (r "1.73.0")))

(define-public crate-kiwi-sdk-0.1.1-alpha.7 (c (n "kiwi-sdk") (v "0.1.1-alpha.7") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "kiwi-macro") (r "~0.1.1-alpha.7") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.17.0") (d #t) (k 0)))) (h "1v962qw3r7aszzazd9yaw09861l194hdzh3nawpmbci9y08wnn16") (r "1.73.0")))

(define-public crate-kiwi-sdk-0.1.1 (c (n "kiwi-sdk") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "kiwi-macro") (r "~0.1.1") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.21.0") (d #t) (k 0)))) (h "186dgis1rb8xydsiy962lv43dcyfd94p0rf7i3rfwh2q0w64km8a") (r "1.73.0")))

