(define-module (crates-io ki wi kiwi-macro) #:use-module (crates-io))

(define-public crate-kiwi-macro-0.1.1-alpha.3 (c (n "kiwi-macro") (v "0.1.1-alpha.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.17.0") (d #t) (k 0)))) (h "0lwc914zjb4msj9kkz2k1q0kzpp3rjqc5kprk72zvdg6wmah7j1r") (r "1.73.0")))

(define-public crate-kiwi-macro-0.1.1-alpha.4 (c (n "kiwi-macro") (v "0.1.1-alpha.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.17.0") (d #t) (k 0)))) (h "0ai2hwhprww4cnagnpik5jj40cfql9q2ky92ny6138kr32rq6b35") (r "1.73.0")))

(define-public crate-kiwi-macro-0.1.1-alpha.5 (c (n "kiwi-macro") (v "0.1.1-alpha.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.17.0") (d #t) (k 0)))) (h "12s7s5y0b0g784nzy4qxn94kv6zhvsjcy4rldc14kr6vyh04y7kf") (r "1.73.0")))

(define-public crate-kiwi-macro-0.1.1-alpha.6 (c (n "kiwi-macro") (v "0.1.1-alpha.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.17.0") (d #t) (k 0)))) (h "14jw7rhii5xi7z1qh7j4igkwcmm5vsrxglsxavcwgs87zf7rlc2f") (r "1.73.0")))

(define-public crate-kiwi-macro-0.1.1-alpha.7 (c (n "kiwi-macro") (v "0.1.1-alpha.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04v840x7d6p91lkf2cf1bdswmn91fawyrmcpzp7fgxqynz7vi7hn") (r "1.73.0")))

(define-public crate-kiwi-macro-0.1.1 (c (n "kiwi-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vbw9b3w0czz6bnqkm41jq4if25h6bz5sdahq47ppzygywlgvrjk") (r "1.73.0")))

