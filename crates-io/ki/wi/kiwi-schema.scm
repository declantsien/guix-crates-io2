(define-module (crates-io ki wi kiwi-schema) #:use-module (crates-io))

(define-public crate-kiwi-schema-0.0.1 (c (n "kiwi-schema") (v "0.0.1") (h "0iwzvqig9ph25h4qsjdshn0mzvv5427jkp5i9s79a67rl9n93lha")))

(define-public crate-kiwi-schema-0.0.2 (c (n "kiwi-schema") (v "0.0.2") (h "187f2gzcs29p0bdcwfihji0fnv7jpklnd8jzpb8mnbn1rmdqjd4r")))

(define-public crate-kiwi-schema-0.0.3 (c (n "kiwi-schema") (v "0.0.3") (h "02l4c8l1ad4i0bc8mvy6sqj7hfmrjc99mj4jpsy9jbhdgv8ma27i")))

(define-public crate-kiwi-schema-0.0.4 (c (n "kiwi-schema") (v "0.0.4") (h "0k7pl0d3wkm7ixjn7yqsgj33hdv86afnm7sczxwarixgjfb3g92b")))

(define-public crate-kiwi-schema-0.0.5 (c (n "kiwi-schema") (v "0.0.5") (h "0kg4jxwlyjdhwjgvi8nf4qk84a8gdfhrl2vg2bwmniqiynb6f56m")))

(define-public crate-kiwi-schema-0.0.6 (c (n "kiwi-schema") (v "0.0.6") (h "1ad49q9hsz8qfnc6k15y2r2006a02zgyf28n90fyz80lh9xk4d32")))

(define-public crate-kiwi-schema-0.0.7 (c (n "kiwi-schema") (v "0.0.7") (h "0zbh9akwbjqhigx9w606fm57526kkgawcvz799h4fm9i9yv2lapd")))

(define-public crate-kiwi-schema-0.0.8 (c (n "kiwi-schema") (v "0.0.8") (h "1apxc51ahsk830ayjp587sniaqi0wds7qv55hbia84s1wh45x6f0")))

(define-public crate-kiwi-schema-0.1.0 (c (n "kiwi-schema") (v "0.1.0") (h "1i182lwmm8aqghy7bam2ls03m3zj6f9p19q33pz5q6g9mnn0hdr7")))

(define-public crate-kiwi-schema-0.1.1 (c (n "kiwi-schema") (v "0.1.1") (h "097rhbf8q6kflsd7na5zvpbzabmxr6bksawkr878rbckamqgs0py")))

(define-public crate-kiwi-schema-0.1.2 (c (n "kiwi-schema") (v "0.1.2") (h "0dpihwx03lbzxc0xlnnfkjbj5lvrin394dalf07h9rljwnfdbmjb")))

(define-public crate-kiwi-schema-0.2.0 (c (n "kiwi-schema") (v "0.2.0") (h "0nspxyikdlankx47111pnyc3damk9m7v1mhrpdaxnj9cvxd9c97j")))

(define-public crate-kiwi-schema-0.2.1 (c (n "kiwi-schema") (v "0.2.1") (h "153ba3fa3psd2imrjfrbq76j8liwkjzfphbhg4kqlvlyq7mk2nyl")))

