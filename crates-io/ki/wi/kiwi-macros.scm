(define-module (crates-io ki wi kiwi-macros) #:use-module (crates-io))

(define-public crate-kiwi-macros-1.0.0 (c (n "kiwi-macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "0nn8imaqb3wkig9bd50bfn2bxp6lckgijlwrb8x8c0qrcc43dp7y")))

(define-public crate-kiwi-macros-1.0.1 (c (n "kiwi-macros") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "0a9b60pvmwighrgig74wqydb2rhvibnwjpsmz7c0yjwcyiyjlifw")))

(define-public crate-kiwi-macros-1.0.2 (c (n "kiwi-macros") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "0h3qxkkp2as7r07swxk1512bsivyk89a139slf7wvkwg702i2vhy")))

(define-public crate-kiwi-macros-1.0.3 (c (n "kiwi-macros") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "1dxh6dvbl78lw0mkmlp14zawjnz4m9gy3iynhyjws7r66q3cib9l")))

(define-public crate-kiwi-macros-1.0.4 (c (n "kiwi-macros") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "0p9b8djhhhbzwis4qjl0pb3isa1hfi1q0pglc9dhvvyp8wsdp4wk")))

(define-public crate-kiwi-macros-1.0.5 (c (n "kiwi-macros") (v "1.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "06nshfaa1w8a9991a6jigz48ij6nia0wijd9k44q5s89r6df2myr")))

(define-public crate-kiwi-macros-1.0.6 (c (n "kiwi-macros") (v "1.0.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "15pbanhfaxlwmj2yq1vgniyvinlx46c0y942hppyl1884cc9h51c") (f (quote (("try")))) (y #t)))

(define-public crate-kiwi-macros-1.0.7 (c (n "kiwi-macros") (v "1.0.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)))) (h "14yjnfrsz1bmk4cy6b7qracip4nkx99r6gnzsi4pn4zgk8kvlsla") (f (quote (("try"))))))

