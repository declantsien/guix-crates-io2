(define-module (crates-io ki wi kiwi-internal-macros) #:use-module (crates-io))

(define-public crate-kiwi-internal-macros-1.0.0 (c (n "kiwi-internal-macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0c30slwyczx99p0flsl7dz9zydr7kw7n0qx1chzbbnxrrmskgxhc")))

(define-public crate-kiwi-internal-macros-1.0.1 (c (n "kiwi-internal-macros") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1a9izq14xmvladyyl51jpkr342f5rlv4181328h2a90zd0z1znhp")))

(define-public crate-kiwi-internal-macros-1.0.2 (c (n "kiwi-internal-macros") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1y8pwvgs93kwsmmshw7vr9hvr832ss7q2v4cbrqg5n5m2y5gvpsm")))

(define-public crate-kiwi-internal-macros-1.0.3 (c (n "kiwi-internal-macros") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14gyvia6fis67q5sckqsgy4kcwsldh9ihcrvwnkvmpg2mh2j2p77")))

(define-public crate-kiwi-internal-macros-1.0.4 (c (n "kiwi-internal-macros") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0h6bq8yl8xvjb53vaz01pm08g1xs7zgyskh20m05a0bnqvvppj7f")))

(define-public crate-kiwi-internal-macros-1.0.5 (c (n "kiwi-internal-macros") (v "1.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0dhax780h6plzz59qka9s04xnrkrs71hxqh83pia7yhpy7ad1hm9")))

(define-public crate-kiwi-internal-macros-1.0.7 (c (n "kiwi-internal-macros") (v "1.0.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11kzrn5whg8d2wgikpjdv5c09z85bmbgk0niw6d3im4wpjv0zmi3")))

