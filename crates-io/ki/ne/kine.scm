(define-module (crates-io ki ne kine) #:use-module (crates-io))

(define-public crate-kine-0.1.0 (c (n "kine") (v "0.1.0") (d (list (d (n "kine-core") (r "^0.1.0") (d #t) (k 0)) (d (n "kine-icu") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "kine-core") (r "^0.1.0") (f (quote ("tz-system-provider-builtin-iers" "tz-utc-provider-builtin-iers"))) (d #t) (k 2)))) (h "1ig6wb012gnsdywzynj05054qxsjagpk60b4b7zvpikc56wsn6r5") (s 2) (e (quote (("icu" "dep:kine-icu"))))))

(define-public crate-kine-0.1.1 (c (n "kine") (v "0.1.1") (d (list (d (n "kine-core") (r "^0.1.0") (d #t) (k 0)) (d (n "kine-icu") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "kine-core") (r "^0.1.0") (f (quote ("tz-system-provider-builtin-iers" "tz-utc-provider-builtin-iers"))) (d #t) (k 2)))) (h "0fh7fyv3rlm5a0brikqy6ngdkb8rfyvnzzakcczq370fwndk4vhc") (s 2) (e (quote (("icu" "dep:kine-icu"))))))

(define-public crate-kine-0.1.2 (c (n "kine") (v "0.1.2") (d (list (d (n "kine-core") (r "^0.1.0") (d #t) (k 0)) (d (n "kine-icu") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "kine-core") (r "^0.1.0") (f (quote ("tz-system-provider-builtin-iers" "tz-utc-provider-builtin-iers"))) (d #t) (k 2)))) (h "1dlv57d9c68z1ljwjymcngmb4ynxszklrzjfwhd7r49hwmczgz8k") (s 2) (e (quote (("icu" "dep:kine-icu"))))))

(define-public crate-kine-0.1.3 (c (n "kine") (v "0.1.3") (d (list (d (n "kine-core") (r "^0.1.0") (d #t) (k 0)) (d (n "kine-icu") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "kine-core") (r "^0.1.0") (f (quote ("tz-system-provider-builtin-iers" "tz-utc-provider-builtin-iers"))) (d #t) (k 2)))) (h "1kdbkdy6xnxlr809biljvsjvy7bbk8xndgv27ykrjrx0k571aiap") (f (quote (("tz-utc-provider-builtin-iers" "kine-core/tz-utc-provider-builtin-iers") ("tz-system-provider-builtin-iers" "kine-core/tz-system-provider-builtin-iers")))) (s 2) (e (quote (("icu" "dep:kine-icu"))))))

