(define-module (crates-io ki ne kinetic-protobuf) #:use-module (crates-io))

(define-public crate-kinetic-protobuf-0.1.0 (c (n "kinetic-protobuf") (v "0.1.0") (d (list (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)))) (h "1b73plr026cy3sfbcrmskzsqg0acfx2f7kdqgzr9jk776ddwz33n")))

(define-public crate-kinetic-protobuf-0.1.1 (c (n "kinetic-protobuf") (v "0.1.1") (d (list (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)))) (h "19y8wpx5fcapx63h9pcb25wv8zgmd8q639l875cc2imyvdjf2mbd")))

(define-public crate-kinetic-protobuf-0.1.2 (c (n "kinetic-protobuf") (v "0.1.2") (d (list (d (n "protobuf") (r "^1.0.24") (d #t) (k 0)))) (h "1lzqw3csgisb8sbns9mzwxhzb81lqknsmgwsdi1a5a65vz9zkcb3")))

