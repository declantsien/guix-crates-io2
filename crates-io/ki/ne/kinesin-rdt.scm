(define-module (crates-io ki ne kinesin-rdt) #:use-module (crates-io))

(define-public crate-kinesin-rdt-0.1.0 (c (n "kinesin-rdt") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-error") (r "^0.2.0") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "02dhqcv25kg5f1xvszbcidfmwsbcx0iqd7hv22v85pni0z7182zj")))

(define-public crate-kinesin-rdt-0.1.1 (c (n "kinesin-rdt") (v "0.1.1") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-error") (r "^0.2.0") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.17") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "1rx588qywxbivza02cmjmjqfjmd5mi9h0hk023b60mx708fhld74")))

