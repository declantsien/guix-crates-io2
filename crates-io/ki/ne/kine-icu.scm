(define-module (crates-io ki ne kine-icu) #:use-module (crates-io))

(define-public crate-kine-icu-0.1.0 (c (n "kine-icu") (v "0.1.0") (d (list (d (n "bolero") (r "^0.8.0") (d #t) (k 0)) (d (n "icu_calendar") (r "^1.1") (d #t) (k 0)) (d (n "kine-core") (r "^0.1.0") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "kine-core") (r "^0.1.0") (f (quote ("tz-system-provider-builtin-iers" "tz-utc-provider-builtin-iers"))) (d #t) (k 2)))) (h "0s5lv70h1nxc173bjq263csn7icj88lyn8pqwp9zj3xifb7d2ly9")))

(define-public crate-kine-icu-0.1.1 (c (n "kine-icu") (v "0.1.1") (d (list (d (n "bolero") (r "^0.8.0") (d #t) (k 0)) (d (n "icu_calendar") (r "^1.1") (d #t) (k 0)) (d (n "kine-core") (r "^0.1.0") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "kine-core") (r "^0.1.0") (f (quote ("tz-system-provider-builtin-iers" "tz-utc-provider-builtin-iers"))) (d #t) (k 2)))) (h "059v48hja6dg0zdp86s2061mgfmz27zjdp114fa7ndqdgj740f1c")))

(define-public crate-kine-icu-0.1.2 (c (n "kine-icu") (v "0.1.2") (d (list (d (n "bolero") (r "^0.8.0") (d #t) (k 0)) (d (n "icu_calendar") (r "^1.1") (d #t) (k 0)) (d (n "kine-core") (r "^0.1.0") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "kine-core") (r "^0.1.0") (f (quote ("tz-system-provider-builtin-iers" "tz-utc-provider-builtin-iers"))) (d #t) (k 2)))) (h "1cw2j1g1cwnvqbvxk4vq7kcz8x2p14d2qs77aa7n41yp3l8d657h")))

