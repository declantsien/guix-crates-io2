(define-module (crates-io ki ne kinect) #:use-module (crates-io))

(define-public crate-kinect-0.0.0 (c (n "kinect") (v "0.0.0") (d (list (d (n "expectest") (r "^0.10") (d #t) (k 2)))) (h "0drl3jjaiqwak85ldyr0m9vmp2vjrxrw33hwf6daah1x59whavzl")))

(define-public crate-kinect-0.0.1 (c (n "kinect") (v "0.0.1") (d (list (d (n "expectest") (r "^0.10") (d #t) (k 2)) (d (n "k4a-sys-temp") (r "^0.2.1") (d #t) (k 0)))) (h "0qcxhn8msb3gd9ka0gssl6ml6kxg7452n9wpg0a2bs2hkn096spz")))

(define-public crate-kinect-0.0.2 (c (n "kinect") (v "0.0.2") (d (list (d (n "k4a-sys-temp") (r "^0.2.1") (d #t) (k 0)))) (h "10gg8ysb0a2cs4np97iw3ap5x6nf3wfpgmj265k435n7mqyax96i")))

(define-public crate-kinect-0.0.3 (c (n "kinect") (v "0.0.3") (d (list (d (n "k4a-sys-temp") (r "^0.2.1") (d #t) (k 0)))) (h "1l7qcdyk7q1pfrb2cbj4lcb27xcg8c1zxzd4238wqvh6giphl6m9")))

(define-public crate-kinect-0.0.4 (c (n "kinect") (v "0.0.4") (d (list (d (n "k4a-sys-temp") (r "^0.2.3") (d #t) (k 0)))) (h "1rfggdpg907jglhyl7nx1v4jb7f1lqwv60kprx9bzsa9ipfzc7x2")))

(define-public crate-kinect-0.0.5 (c (n "kinect") (v "0.0.5") (d (list (d (n "k4a-sys-temp") (r "^0.2.3") (d #t) (k 0)))) (h "0h7wnh2d2zgycbkyqbzl406s5j5bkrq6hvq9f8bbfvyjrycwx1gi")))

(define-public crate-kinect-0.0.6 (c (n "kinect") (v "0.0.6") (d (list (d (n "k4a-sys-temp") (r "^0.2.3") (d #t) (k 0)))) (h "0kbbjcl7xzyzg5knm2pslzn8rrpb9zdqsygqr8k9bypg3mvsizra")))

