(define-module (crates-io ki ne kinetic-rust) #:use-module (crates-io))

(define-public crate-kinetic-rust-0.0.7 (c (n "kinetic-rust") (v "0.0.7") (d (list (d (n "docopt_macros") (r "*") (d #t) (k 0)) (d (n "protobuf") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)))) (h "104kabj90g95yycqj6h5hzcdnnlim3qnm3jzj3z8wxfrkim9fi9g")))

(define-public crate-kinetic-rust-0.0.13 (c (n "kinetic-rust") (v "0.0.13") (d (list (d (n "docopt_macros") (r "*") (d #t) (k 0)) (d (n "protobuf") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)))) (h "051whccsw0wqwar2bvzrp8zf2pnp5am53fryn0asni1pf9yknfjz")))

(define-public crate-kinetic-rust-0.0.16 (c (n "kinetic-rust") (v "0.0.16") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "protobuf") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "term") (r "*") (d #t) (k 0)))) (h "1hd1vihkj2gimm661hq6la1510qps8685paln24pyd2709srjfvw")))

