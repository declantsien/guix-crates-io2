(define-module (crates-io ki ne kinesis) #:use-module (crates-io))

(define-public crate-kinesis-0.0.1 (c (n "kinesis") (v "0.0.1") (d (list (d (n "console_error_panic_hook") (r "^0.1.7") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("Document" "Element" "HtmlElement" "Node" "EventTarget" "Window" "MouseEvent" "console" "DomStringMap" "Text"))) (d #t) (k 0)))) (h "18mgygi12g5kj71njzz5n8z1ijsg7y4i285r1s1idysm6434fb80")))

