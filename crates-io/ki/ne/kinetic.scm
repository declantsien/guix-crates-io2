(define-module (crates-io ki ne kinetic) #:use-module (crates-io))

(define-public crate-kinetic-0.1.0 (c (n "kinetic") (v "0.1.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "protobuf") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "shell") (r "*") (d #t) (k 0)) (d (n "term") (r "*") (d #t) (k 0)))) (h "0qk5k81p5dgzcjxkczxzcsd2yf4psc5w3w9ha21qy1wcwmyl5yc6")))

(define-public crate-kinetic-0.1.1 (c (n "kinetic") (v "0.1.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "protobuf") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "shell") (r "*") (d #t) (k 0)) (d (n "term") (r "*") (d #t) (k 0)))) (h "1my9vcf1kdzmrn67a8fcjza702d967al9bcv7w5kncfgiicpf1q9")))

