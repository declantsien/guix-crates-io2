(define-module (crates-io ki ne kinetic-engine) #:use-module (crates-io))

(define-public crate-kinetic-engine-0.1.0 (c (n "kinetic-engine") (v "0.1.0") (h "0z6frwhi2wb656mn0kgkri2yqrli2kdybs7mndmz3x69xs7fk367") (y #t)))

(define-public crate-kinetic-engine-0.1.1 (c (n "kinetic-engine") (v "0.1.1") (h "0k1kg5q9nxwdjaa4v9509lnaj5fgmnq8xnw06xa7hzxibfawnnyv") (y #t)))

