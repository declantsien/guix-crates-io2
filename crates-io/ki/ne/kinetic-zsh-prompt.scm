(define-module (crates-io ki ne kinetic-zsh-prompt) #:use-module (crates-io))

(define-public crate-kinetic-zsh-prompt-0.1.0 (c (n "kinetic-zsh-prompt") (v "0.1.0") (h "1hh7whk0fhx7an5vaanv4z1ffy4ws3fii9gi09ig06xp6ji60kl1")))

(define-public crate-kinetic-zsh-prompt-0.1.1 (c (n "kinetic-zsh-prompt") (v "0.1.1") (h "0xah3pshqm9d66w9v673437jgipx1p4mc50y3nc4nj1dhgp38613")))

