(define-module (crates-io ki ne kinesin) #:use-module (crates-io))

(define-public crate-kinesin-0.1.0 (c (n "kinesin") (v "0.1.0") (d (list (d (n "kinesin-rdt") (r "^0.1.0") (d #t) (k 0)))) (h "0ijspdikqi7c7skgc690cgp87hc6v43605mbyjw0f414p2sr9zsv")))

(define-public crate-kinesin-0.1.1 (c (n "kinesin") (v "0.1.1") (d (list (d (n "kinesin-rdt") (r "^0.1.0") (d #t) (k 0)))) (h "1iwslb6znqkh54fjwcc2ln0z6yv8n4zzv0nl43aabdzsqlg1y1sy")))

