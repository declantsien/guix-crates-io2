(define-module (crates-io ki ne kine-core) #:use-module (crates-io))

(define-public crate-kine-core-0.1.0 (c (n "kine-core") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "bolero") (r "^0.8.0") (d #t) (k 2)))) (h "1vnybrk5c4mnnwdqf6njv6ybcadm8lg8vl5vqw51d4bfmviwgmxj") (f (quote (("tz-utc-provider-builtin-iers") ("tz-system-provider-builtin-iers") ("std") ("default" "std"))))))

(define-public crate-kine-core-0.1.1 (c (n "kine-core") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "bolero") (r "^0.8.0") (d #t) (k 2)))) (h "0fk81lfim7jlc83dig4r36qmj24m2szva9jrj4jrc41ndbwvcb5r") (f (quote (("tz-utc-provider-builtin-iers") ("tz-system-provider-builtin-iers") ("std") ("default" "std"))))))

