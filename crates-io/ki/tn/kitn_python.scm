(define-module (crates-io ki tn kitn_python) #:use-module (crates-io))

(define-public crate-kitn_python-0.0.0 (c (n "kitn_python") (v "0.0.0") (d (list (d (n "kitn") (r "^0.0.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.20") (f (quote ("abi3-py37" "extension-module"))) (d #t) (k 0)))) (h "1xfibq5nl76c50ywaqw78yp403yd681c1a493gi1f47sjf87q2ym")))

(define-public crate-kitn_python-0.0.1 (c (n "kitn_python") (v "0.0.1") (h "1lailw8v0vcmq0zm4mz63iwsaw68k61cflwfd89ynxd06ijfj948")))

