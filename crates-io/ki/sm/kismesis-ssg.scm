(define-module (crates-io ki sm kismesis-ssg) #:use-module (crates-io))

(define-public crate-kismesis-ssg-0.1.4 (c (n "kismesis-ssg") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "kismesis") (r "^0.1.11") (f (quote ("reporting"))) (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)))) (h "0pff4fnfvya15mr6zs4njv91zfsmmbvrgh5qbh3xl1nmkgm8iq6i") (f (quote (("plugins" "kismesis/plugins") ("default" "plugins"))))))

(define-public crate-kismesis-ssg-0.1.5 (c (n "kismesis-ssg") (v "0.1.5") (d (list (d (n "clap") (r "^4.4.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "kismesis") (r "^0.1.12") (f (quote ("reporting"))) (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)))) (h "0yp6ln7w4z9pffgrfvg6hgp5krl251qccsdsa1m711hf81bmsi1s") (f (quote (("plugins" "kismesis/plugins") ("default" "plugins"))))))

(define-public crate-kismesis-ssg-0.1.6 (c (n "kismesis-ssg") (v "0.1.6") (d (list (d (n "clap") (r "^4.4.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "kismesis") (r "^0.1.13") (f (quote ("reporting"))) (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)))) (h "170hh5c9gjngzvrk0fn23aaxnxqap0h2j4j40mrb5knmgbxrqv4k") (f (quote (("plugins" "kismesis/plugins") ("default" "plugins"))))))

(define-public crate-kismesis-ssg-0.1.7 (c (n "kismesis-ssg") (v "0.1.7") (d (list (d (n "clap") (r "^4.4.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "kismesis") (r "^0.1.14") (f (quote ("reporting"))) (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)))) (h "1lqz5z6bs82q5x223bpfizc09w3famsa0lhbfisd02vanzpgcv5s") (f (quote (("plugins" "kismesis/plugins") ("default" "plugins"))))))

(define-public crate-kismesis-ssg-0.1.8 (c (n "kismesis-ssg") (v "0.1.8") (d (list (d (n "clap") (r "^4.4.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "kismesis") (r "^0.1.15") (f (quote ("reporting"))) (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)))) (h "1yzn6d5g0m83fyx2ycsicks37spl2q221y9lkffirxnrnpf2wgdy") (f (quote (("plugins" "kismesis/plugins") ("default" "plugins"))))))

(define-public crate-kismesis-ssg-0.1.9 (c (n "kismesis-ssg") (v "0.1.9") (d (list (d (n "clap") (r "^4.4.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "kismesis") (r "^0.1.16") (f (quote ("reporting"))) (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)))) (h "1mkamrkk7xi32wwy6ynsyzlxnvs4y1rvw4k89ry8wd1l0088k1bz") (f (quote (("plugins" "kismesis/plugins") ("default" "plugins"))))))

(define-public crate-kismesis-ssg-0.1.10 (c (n "kismesis-ssg") (v "0.1.10") (d (list (d (n "clap") (r "^4.4.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "kismesis") (r "^0.1.17") (f (quote ("reporting"))) (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)))) (h "0lr2qjyjzvraxb2fb5wrrgmwp8dphp4r879gffz5d19v1jnnxmvb") (f (quote (("plugins" "kismesis/plugins") ("default" "plugins"))))))

(define-public crate-kismesis-ssg-0.2.1 (c (n "kismesis-ssg") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "kismesis") (r "^0.2.1") (f (quote ("reporting"))) (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)))) (h "004n7iqmi3rbvwap92ppcppprgdaqj9lbn8k5g91ayqcvjpqacb0") (f (quote (("plugins" "kismesis/plugins") ("default" "plugins"))))))

