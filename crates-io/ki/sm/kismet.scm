(define-module (crates-io ki sm kismet) #:use-module (crates-io))

(define-public crate-kismet-0.1.0 (c (n "kismet") (v "0.1.0") (d (list (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "logos-derive") (r "^0.12.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.2") (d #t) (k 0)) (d (n "overload") (r "^0.1.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "1asnkxb59ydqknxn69jc8wibv5xm788f30mr6mm5jhf4zi1gdqyb")))

