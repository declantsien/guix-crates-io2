(define-module (crates-io ki sm kismet_cli) #:use-module (crates-io))

(define-public crate-kismet_cli-0.1.0 (c (n "kismet_cli") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "kismet") (r "^0.1.0") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)))) (h "11gfs7h2ygmsydbq7ggg0k9sbc6ln1bp3ccqswwk4gq3iykbgfdi")))

