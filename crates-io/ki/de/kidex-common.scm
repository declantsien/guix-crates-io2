(define-module (crates-io ki de kidex-common) #:use-module (crates-io))

(define-public crate-kidex-common-0.1.0 (c (n "kidex-common") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (o #t) (d #t) (k 0)))) (h "0fg5jx533k1whkx1wfnlhysplzqqw1qc6743arx29cx0kv9n3lfr") (s 2) (e (quote (("util" "dep:serde_json"))))))

