(define-module (crates-io ki bi kibi) #:use-module (crates-io))

(define-public crate-kibi-0.1.0 (c (n "kibi") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.16") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1") (d #t) (k 0)))) (h "0g51a3l3lahghnqjayqr5vlfp52j3wpzkq4j9ywn2yn2w61vpar2") (y #t)))

(define-public crate-kibi-0.1.1 (c (n "kibi") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1") (d #t) (k 0)))) (h "15af9bvb1libsrz8q71kxf6g3df1is11jj58wa6i2h14nlwmr489")))

(define-public crate-kibi-0.1.2 (c (n "kibi") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.13") (d #t) (k 0)) (d (n "tokei") (r "^10.1.1") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "06hxl9vfzlwjl6yk67cwsmxf6pfhvm37k514gv7cwaak3hlmv7hb")))

(define-public crate-kibi-0.2.0 (c (n "kibi") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.69") (d #t) (t "cfg(unix)") (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "serial_test") (r "^0.4.0") (d #t) (k 2)) (d (n "signal-hook") (r "^0.1.13") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("wincon"))) (t "cfg(windows)") (k 0)) (d (n "winapi-util") (r "^0.1.4") (d #t) (t "cfg(windows)") (k 0)))) (h "1wwfcp8ch4khijqscq8y3fs3nxkrwdzpnhnjv8n3yqvy1zlhiwx3")))

(define-public crate-kibi-0.2.1 (c (n "kibi") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.79") (d #t) (t "cfg(unix)") (k 0)) (d (n "serial_test") (r "^0.5.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("wincon"))) (t "cfg(windows)") (k 0)) (d (n "winapi-util") (r "^0.1.4") (d #t) (t "cfg(windows)") (k 0)))) (h "148xqhgddvzcfrggn6dgvixbz5k4kklazga68w8975mn6lvxfanb")))

(define-public crate-kibi-0.2.2 (c (n "kibi") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.86") (d #t) (t "cfg(unix)") (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("wincon"))) (t "cfg(windows)") (k 0)) (d (n "winapi-util") (r "^0.1.4") (d #t) (t "cfg(windows)") (k 0)))) (h "1w2i6js9c60yfcvkj412msfz352ns8dhjl23hv4ciyghxz2chmrh")))

