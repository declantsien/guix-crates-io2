(define-module (crates-io ki ki kiki) #:use-module (crates-io))

(define-public crate-kiki-1.0.0 (c (n "kiki") (v "1.0.0") (d (list (d (n "insta") (r "^1.26.0") (d #t) (k 2)) (d (n "lalrpop") (r "^0.19.7") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1nmibcqab7qkbn6bz5v8z1w7674vj7sa84gv8l0wiqq737lg7vvw") (y #t)))

(define-public crate-kiki-2.0.0 (c (n "kiki") (v "2.0.0") (d (list (d (n "insta") (r "^1.26.0") (d #t) (k 2)) (d (n "lalrpop") (r "^0.19.7") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "sha256") (r "^1.1.4") (d #t) (k 0)))) (h "14ibinjyrzyvga9p8abh9lw8sqqk0f96s2gjdmwblva11bcxbw9x") (y #t)))

(define-public crate-kiki-3.0.0 (c (n "kiki") (v "3.0.0") (d (list (d (n "insta") (r "^1.26.0") (d #t) (k 2)) (d (n "kiki") (r "^2.0.0") (d #t) (k 1)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sha256") (r "^1.1.4") (d #t) (k 0)) (d (n "sha256") (r "^1.1.4") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 1)))) (h "0ans52ggnrwyq6r0axyi5gr7h6d8b9q5k2j55kl7fjfxvr1rcnhj") (y #t)))

(define-public crate-kiki-3.0.1 (c (n "kiki") (v "3.0.1") (d (list (d (n "insta") (r "^1.26.0") (d #t) (k 2)) (d (n "kiki") (r "^2.0.0") (d #t) (k 1)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sha256") (r "^1.1.4") (d #t) (k 0)) (d (n "sha256") (r "^1.1.4") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 1)))) (h "12x02cnsh9njj1rw0j3fhf94l44lshbdz947984vl60k60wyjzjb") (y #t)))

(define-public crate-kiki-4.0.0 (c (n "kiki") (v "4.0.0") (d (list (d (n "insta") (r "^1.26.0") (d #t) (k 2)) (d (n "kiki") (r "^3.0.1") (d #t) (k 1)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sha256") (r "^1.1.4") (d #t) (k 0)) (d (n "sha256") (r "^1.1.4") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 1)))) (h "19yckhv634j7di3w4li0n2kbn752nlsv345pn4x63xn04g910559") (y #t)))

(define-public crate-kiki-5.0.0 (c (n "kiki") (v "5.0.0") (d (list (d (n "insta") (r "^1.26.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sha256") (r "^1.1.4") (d #t) (k 0)) (d (n "sha256") (r "^1.1.4") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 1)))) (h "0yj98dsjdz776k9ii5aa680axmldfjhcp3xczvw8s9yc5bvlncdv")))

(define-public crate-kiki-6.0.0 (c (n "kiki") (v "6.0.0") (d (list (d (n "insta") (r "^1.26.0") (d #t) (k 2)) (d (n "kiki") (r "^5.0.0") (d #t) (k 1)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sha256") (r "^1.1.4") (d #t) (k 0)) (d (n "sha256") (r "^1.1.4") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 1)))) (h "1abzg1avifwavg87mvxdwq9dcby6mj6ri2y8bsn89iqwj5j19il0")))

(define-public crate-kiki-6.0.1 (c (n "kiki") (v "6.0.1") (d (list (d (n "insta") (r "^1.26.0") (d #t) (k 2)) (d (n "kiki") (r "^5.0.0") (d #t) (k 1)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sha256") (r "^1.1.4") (d #t) (k 0)) (d (n "sha256") (r "^1.1.4") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 1)))) (h "1vgfyy4m9xvb78y9yzw8i8g84nvkh2iacnq9gpd08fv6jgc16l45")))

(define-public crate-kiki-6.0.2 (c (n "kiki") (v "6.0.2") (d (list (d (n "insta") (r "^1.26.0") (d #t) (k 2)) (d (n "kiki") (r "^5.0.0") (d #t) (k 1)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sha256") (r "^1.1.4") (d #t) (k 0)) (d (n "sha256") (r "^1.1.4") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 1)))) (h "0shld6f5ya2ajkqmbzzi41v3x0jcyd6k0wwzgjrgg99pn8210rqi")))

(define-public crate-kiki-6.1.0 (c (n "kiki") (v "6.1.0") (d (list (d (n "insta") (r "^1.26.0") (d #t) (k 2)) (d (n "kiki") (r "^5") (d #t) (k 1)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sha256") (r "^1.1.4") (d #t) (k 0)) (d (n "sha256") (r "^1") (d #t) (k 1)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "1i5nylj3y1krb8kkny6qf8iyzk6vi9ngvd9jm3w5fln5jff4lc77")))

(define-public crate-kiki-6.1.1 (c (n "kiki") (v "6.1.1") (d (list (d (n "insta") (r "^1.26.0") (d #t) (k 2)) (d (n "kiki") (r "^5") (d #t) (k 1)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sha256") (r "^1.1.4") (d #t) (k 0)) (d (n "sha256") (r "^1") (d #t) (k 1)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "1gkpiz114vbxqw17m4crpci6m0q6qggiwjpq709g02ma55b4ckg9")))

(define-public crate-kiki-7.0.0 (c (n "kiki") (v "7.0.0") (d (list (d (n "insta") (r "^1.26.0") (d #t) (k 2)) (d (n "kiki") (r "^6") (d #t) (k 1)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "sha256") (r "^1.1.4") (d #t) (k 0)) (d (n "sha256") (r "^1") (d #t) (k 1)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "052xjdk2rdam19pd1h49kkz928jjrl871jc2sw5xlb6c2nfhlb8v")))

