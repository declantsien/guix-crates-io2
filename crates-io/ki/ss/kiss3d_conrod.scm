(define-module (crates-io ki ss kiss3d_conrod) #:use-module (crates-io))

(define-public crate-kiss3d_conrod-0.63.0 (c (n "kiss3d_conrod") (v "0.63.0") (d (list (d (n "conrod_derive") (r "^0.63") (d #t) (k 0)) (d (n "daggy") (r "^0.5") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "instant") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.24") (d #t) (k 0)) (d (n "rusttype") (r "^0.7") (f (quote ("gpu_cache"))) (d #t) (k 0)))) (h "11g1a6qcs8nf1pik1src2jnxia2x7z44rwr3pyjndv4dbs6l75q8") (f (quote (("wasm-bindgen" "instant/wasm-bindgen") ("stdweb" "instant/stdweb"))))))

(define-public crate-kiss3d_conrod-0.64.0 (c (n "kiss3d_conrod") (v "0.64.0") (d (list (d (n "conrod_derive") (r "^0.63") (d #t) (k 0)) (d (n "daggy") (r "^0.5") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "instant") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "pistoncore-input") (r "^0.24") (d #t) (k 0)) (d (n "rusttype") (r "^0.8") (f (quote ("gpu_cache"))) (d #t) (k 0)))) (h "16slgh5ypl645p38xzkqss331mv5rnzxkfcj92rghyncp1vcyl62") (f (quote (("wasm-bindgen" "instant/wasm-bindgen") ("stdweb" "instant/stdweb"))))))

