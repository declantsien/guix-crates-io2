(define-module (crates-io ki ss kissa-core-console) #:use-module (crates-io))

(define-public crate-kissa-core-console-0.1.0 (c (n "kissa-core-console") (v "0.1.0") (d (list (d (n "mlua") (r "^0.9.0-rc.3") (f (quote ("lua54" "parking_lot"))) (d #t) (k 0)))) (h "1g04281cs7565i9f8a8m4cqjgn3liah97dkr522h5cczfyyay7d8") (y #t)))

(define-public crate-kissa-core-console-0.1.1 (c (n "kissa-core-console") (v "0.1.1") (d (list (d (n "mlua") (r "^0.9.0-rc.3") (f (quote ("lua54" "parking_lot"))) (d #t) (k 0)))) (h "1mfw6pz9cs0cjy5b4rwhid9gc6pf0wq5x5szi42vsll7rs1w7sri") (y #t)))

(define-public crate-kissa-core-console-0.1.2 (c (n "kissa-core-console") (v "0.1.2") (d (list (d (n "mlua") (r "^0.9.0-rc.3") (f (quote ("lua54" "parking_lot"))) (d #t) (k 0)))) (h "05ab25cv798mznnz6gm4bnbrifiqmnyhv94sm2sb64qp219ji1ms") (y #t)))

(define-public crate-kissa-core-console-0.1.3 (c (n "kissa-core-console") (v "0.1.3") (d (list (d (n "mlua") (r "^0.9.0-rc.3") (f (quote ("lua54" "parking_lot"))) (d #t) (k 0)))) (h "052ismd9qplnc73jiblcn2js9nsxfkpn8gpblmpqvvkrrp9a0hhb") (y #t)))

(define-public crate-kissa-core-console-0.1.4 (c (n "kissa-core-console") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "mlua") (r "^0.9.0-rc.3") (f (quote ("lua54" "parking_lot"))) (d #t) (k 0)))) (h "139wgizl70djn4c1r0fxi0pdvzi0ik7shkhjd3rmw7knp27h7b2v") (y #t)))

