(define-module (crates-io ki ss kiss-ntpd) #:use-module (crates-io))

(define-public crate-kiss-ntpd-1.0.0 (c (n "kiss-ntpd") (v "1.0.0") (h "1gb0rvsjmhjkr4j3c6kv6w2g97z0mh70df0kmb8bfd5rg7ms9w38")))

(define-public crate-kiss-ntpd-2.0.0 (c (n "kiss-ntpd") (v "2.0.0") (h "1cia07nnhslhykw3irfi4rj9j034wa8km9h93j1hj3b70jaa4bxx")))

