(define-module (crates-io ki ss kiss-icp-ops-sophus) #:use-module (crates-io))

(define-public crate-kiss-icp-ops-sophus-0.1.0 (c (n "kiss-icp-ops-sophus") (v "0.1.0") (d (list (d (n "kiss-icp-ops-core") (r "^0.1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)))) (h "1yf8y8b9mbsm123l2306aq9qspq13wxhba33ixr7zwprzah18iq1") (r "1.62")))

