(define-module (crates-io ki ss kiss-icp-ops-eigen) #:use-module (crates-io))

(define-public crate-kiss-icp-ops-eigen-0.1.0 (c (n "kiss-icp-ops-eigen") (v "0.1.0") (d (list (d (n "kiss-icp-ops-core") (r "^0.1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)))) (h "02l99akv5g2zymi4ix5i0bpl3d21fs6ly82my0r28nh3rlgbrc5p") (r "1.62")))

