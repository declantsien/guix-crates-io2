(define-module (crates-io ki ss kiss_dc) #:use-module (crates-io))

(define-public crate-kiss_dc-0.0.5 (c (n "kiss_dc") (v "0.0.5") (d (list (d (n "tungstenite") (r "^0.11.1") (d #t) (k 0)))) (h "1jxg993n2qgc56yg0fipjgk08a6xdy0kr0syaj2zzp0hvdaxd9sc")))

(define-public crate-kiss_dc-0.0.6 (c (n "kiss_dc") (v "0.0.6") (d (list (d (n "tungstenite") (r "^0.11.1") (d #t) (k 0)))) (h "1xi6pnnfkpgnsz557xrz1xm9jq6zfj5i7ql0r4lz6r6m5923cy84")))

(define-public crate-kiss_dc-0.0.7 (c (n "kiss_dc") (v "0.0.7") (d (list (d (n "tungstenite") (r "^0.11.1") (d #t) (k 0)))) (h "074djmd7kpw9ihdd6nafvhvgmac9i14nkg7gaspmc3wb677pc8w1")))

(define-public crate-kiss_dc-0.0.8 (c (n "kiss_dc") (v "0.0.8") (d (list (d (n "tungstenite") (r "^0.11.1") (d #t) (k 0)))) (h "06njyrlyfvq1y0vj31rg3abaxg5pw0p18bl7wk4b88dbgrv6j9md")))

(define-public crate-kiss_dc-0.0.9 (c (n "kiss_dc") (v "0.0.9") (d (list (d (n "tungstenite") (r "^0.11.1") (d #t) (k 0)))) (h "15pmvrw7niafy8axx9pw9n11yk7phh1avh2rbs1wrc89hab5fjbh")))

(define-public crate-kiss_dc-0.0.10 (c (n "kiss_dc") (v "0.0.10") (d (list (d (n "tungstenite") (r "^0.11.1") (d #t) (k 0)))) (h "18fx78zidgqxm71whl6l9jf55213yp2cx06n8ajx95yygpgklp37")))

(define-public crate-kiss_dc-0.0.11 (c (n "kiss_dc") (v "0.0.11") (d (list (d (n "tungstenite") (r "^0.11.1") (d #t) (k 0)))) (h "1rng8w78cg1pi1prpf28mmndw06acnw0z0z0hl9x0qrnhqvvdxvf")))

(define-public crate-kiss_dc-0.0.12 (c (n "kiss_dc") (v "0.0.12") (d (list (d (n "tungstenite") (r "^0.11.1") (d #t) (k 0)))) (h "09by317qxjca9i343cck7sjxjnxrbay1bn6kqs7zcn72hhg50iyx")))

