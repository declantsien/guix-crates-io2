(define-module (crates-io ki ss kiss-icp-ops) #:use-module (crates-io))

(define-public crate-kiss-icp-ops-0.1.0 (c (n "kiss-icp-ops") (v "0.1.0") (d (list (d (n "kiss-icp-ops-core") (r "^0.1.0") (d #t) (k 0)) (d (n "kiss-icp-ops-eigen") (r "^0.1.0") (d #t) (k 0)) (d (n "kiss-icp-ops-sophus") (r "^0.1.0") (d #t) (k 0)))) (h "1n24cl1qzj52pc6czl9x784nxwyxrrd0340pi8in46k26zay3083") (r "1.62")))

