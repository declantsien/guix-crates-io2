(define-module (crates-io ki ss kissfft-sys) #:use-module (crates-io))

(define-public crate-kissfft-sys-0.1.0 (c (n "kissfft-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.3") (d #t) (k 0)))) (h "06rnhrcfqsqmrsaf7lrd9n7w46krwdkmshx1l76x85374ycp5j06")))

