(define-module (crates-io ki ss kiss-icp-ops-core) #:use-module (crates-io))

(define-public crate-kiss-icp-ops-core-0.1.0 (c (n "kiss-icp-ops-core") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.32") (d #t) (k 0)))) (h "1nr86j7i4r9ika1i0c52a30mm59qf9fhsdbf4f8090phr0iv9a4j") (r "1.62")))

