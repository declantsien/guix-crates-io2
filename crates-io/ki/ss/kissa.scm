(define-module (crates-io ki ss kissa) #:use-module (crates-io))

(define-public crate-kissa-0.0.1 (c (n "kissa") (v "0.0.1") (d (list (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)))) (h "0n3a6zagiiqgjhcqm9jvna96zawsvz40spdif36vf71m1bkmfyc2") (y #t)))

(define-public crate-kissa-0.0.2 (c (n "kissa") (v "0.0.2") (d (list (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)))) (h "1pyywgrpxiaif8w08cibl5fnvvjqwqwihv5xc2b6d5xdr4gz2l34") (y #t)))

