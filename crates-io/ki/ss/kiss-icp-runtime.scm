(define-module (crates-io ki ss kiss-icp-runtime) #:use-module (crates-io))

(define-public crate-kiss-icp-runtime-0.1.0 (c (n "kiss-icp-runtime") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "hwlocality") (r "^1.0.0-alpha.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)))) (h "03mh4v521whkwcrr22mxlklql55sy9sjjj5q7p2qxcqrx7x6sgmf") (f (quote (("numa" "hwlocality" "rand") ("default" "numa")))) (r "1.62")))

