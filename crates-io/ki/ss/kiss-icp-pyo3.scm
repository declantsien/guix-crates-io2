(define-module (crates-io ki ss kiss-icp-pyo3) #:use-module (crates-io))

(define-public crate-kiss-icp-pyo3-0.1.0 (c (n "kiss-icp-pyo3") (v "0.1.0") (d (list (d (n "kiss-icp-core") (r "^0.1.0") (d #t) (k 0)) (d (n "numpy") (r "^0.20") (f (quote ("nalgebra"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.20") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)))) (h "01y6a1fxmlms8mcnfzivsx5q3hicrnp40mw9qxblrfp33byknlxw") (f (quote (("nightly" "kiss-icp-core/nightly" "pyo3/nightly") ("default")))) (r "1.62")))

