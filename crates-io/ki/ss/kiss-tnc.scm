(define-module (crates-io ki ss kiss-tnc) #:use-module (crates-io))

(define-public crate-kiss-tnc-0.1.0 (c (n "kiss-tnc") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zb6n03xgrbfq7gih44nf3m8i00w04xnh54nlapng7a08j8fxz3d")))

(define-public crate-kiss-tnc-0.1.1 (c (n "kiss-tnc") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02xip7rwfflcpsja4dpfmgi2330x3h8jdf0xqjrfrq7b52f8dipc")))

(define-public crate-kiss-tnc-0.1.2 (c (n "kiss-tnc") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "019b0idwwys0h2ks0rib48hglm61ym1rnf1rk36k8yph7haycf7b")))

(define-public crate-kiss-tnc-0.1.3 (c (n "kiss-tnc") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0v1fgclasa6d0a61zfn11rw26wliz9qvwlwvnxlv89rj381iaggb")))

(define-public crate-kiss-tnc-0.2.0 (c (n "kiss-tnc") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util" "macros" "rt" "sync"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "01f252szk0fqd0qap90qx1m4nvfp9bzd2qdqpr8ai2ls82l92jyx")))

(define-public crate-kiss-tnc-0.2.1 (c (n "kiss-tnc") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util" "macros" "rt" "sync"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "04g837mvpblid74921hsnfrykc4ixgm79a5z8jd3xim3sa39nq2h")))

(define-public crate-kiss-tnc-0.2.2 (c (n "kiss-tnc") (v "0.2.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util" "macros" "rt" "sync"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "01kcas43hbk8ajhb92p0x3bf4w9sa1kx5bsy5drcccam4ywh8hp8")))

