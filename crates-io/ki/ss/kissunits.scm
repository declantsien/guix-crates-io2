(define-module (crates-io ki ss kissunits) #:use-module (crates-io))

(define-public crate-kissunits-0.0.0 (c (n "kissunits") (v "0.0.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0z93z6hxbz86gilpc3kc2nz0w956grs2lm6qsm92cv94ygzkbkda")))

(define-public crate-kissunits-1.0.0 (c (n "kissunits") (v "1.0.0") (h "0zx41i3pfp9lbjsqpzlqfwmi8j7asix3zv5hnvy98k4jzbxipwdk")))

(define-public crate-kissunits-1.0.1 (c (n "kissunits") (v "1.0.1") (h "0nkyiivvxvpjk1xs6gnm7n7pqsncl93wy1a3d99fs1sxs75l4348")))

(define-public crate-kissunits-1.0.2 (c (n "kissunits") (v "1.0.2") (h "1jb0wak1gvwin88qkm1g1vc4qyj60zvfw11zgjb6z9rr2hq7zkkb")))

(define-public crate-kissunits-1.0.3 (c (n "kissunits") (v "1.0.3") (h "0jq6q861s8nzz46csivk3iv9iwjmmzpxpa2dg17fsgl138is6b52")))

(define-public crate-kissunits-1.1.0 (c (n "kissunits") (v "1.1.0") (h "01c9pgypl4g20ppkyv39fbhj7272pa4967bslp63ppn46l201n6m")))

(define-public crate-kissunits-2.0.0 (c (n "kissunits") (v "2.0.0") (h "0znrj4122cwpklis7fb3jzs7a07bcaivvklm76a9sqym5n9dg8hd")))

