(define-module (crates-io ki ss kiss-ftpd) #:use-module (crates-io))

(define-public crate-kiss-ftpd-1.0.0 (c (n "kiss-ftpd") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "libunftp") (r "^0.17") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)) (d (n "unftp-sbe-fs") (r "^0.1") (d #t) (k 0)))) (h "1rsz669lpl80z7zqm7f5p1gx0krjpz7pgpyq4a3i77ip1qlacfqf") (f (quote (("default" "env_logger"))))))

