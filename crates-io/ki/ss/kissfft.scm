(define-module (crates-io ki ss kissfft) #:use-module (crates-io))

(define-public crate-kissfft-0.0.1 (c (n "kissfft") (v "0.0.1") (h "0viaqg779gfg99kf5hg15a35jmc1ryr3q4qqp6srkyprxisq86mq") (y #t)))

(define-public crate-kissfft-0.0.2 (c (n "kissfft") (v "0.0.2") (h "1d5hwgxaa21m22z7mxyhbyl7xd5zk87ahm7dfli2pl7hw249knyz") (y #t)))

(define-public crate-kissfft-0.0.3 (c (n "kissfft") (v "0.0.3") (h "1f28h25cp9127zqxqq1h4fs4x4sv5f2zyvgyvfd5zjwca6i3y0ay") (y #t)))

(define-public crate-kissfft-0.0.4 (c (n "kissfft") (v "0.0.4") (h "1vjclw6jzg15frb66cq3h76phi2s9h8m53q0mzpmk630h2bmysg5") (y #t)))

(define-public crate-kissfft-0.0.5 (c (n "kissfft") (v "0.0.5") (h "0kp87gnhkf5aj69sn6aif6i7bllr38cvl9hwj6lsfbdfd3kjaidw") (y #t)))

(define-public crate-kissfft-0.0.6 (c (n "kissfft") (v "0.0.6") (h "17wi7kslg87fc13y0sbb8g75a1bp6g0yjmw59j3y6qvbqxkg9phn") (y #t)))

(define-public crate-kissfft-0.0.7 (c (n "kissfft") (v "0.0.7") (h "02yralkdsm7vgmrnn25awd603nhsd84059nb3a538h7cnvm45j2m") (y #t)))

(define-public crate-kissfft-0.0.8 (c (n "kissfft") (v "0.0.8") (h "11ll1zq6b8dcnnpk0y7zmiggz17d3xpid3cvskhdfgxws7xpln09") (y #t)))

