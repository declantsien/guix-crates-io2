(define-module (crates-io ki ss kiss_xml) #:use-module (crates-io))

(define-public crate-kiss_xml-0.0.0 (c (n "kiss_xml") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "dyn-clone") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1cq7xhsicra7fxlylkwi83dlp0hj9d63lbgfviin5daw5mf5bgcn")))

