(define-module (crates-io ki ss kiss3d-trackball) #:use-module (crates-io))

(define-public crate-kiss3d-trackball-0.1.0 (c (n "kiss3d-trackball") (v "0.1.0") (d (list (d (n "kiss3d") (r "^0.30") (d #t) (k 0)) (d (n "nalgebra") (r "^0.25") (d #t) (k 0)) (d (n "trackball") (r "^0.2") (d #t) (k 0)))) (h "08l45bgyhl1r3nkiplyzy5f1n9sxc1bwfkfh4m2g5bp0whr16055") (f (quote (("cc" "trackball/cc"))))))

(define-public crate-kiss3d-trackball-0.1.1 (c (n "kiss3d-trackball") (v "0.1.1") (d (list (d (n "kiss3d") (r "^0.30") (d #t) (k 0)) (d (n "nalgebra") (r "^0.25") (d #t) (k 0)) (d (n "trackball") (r "^0.2") (d #t) (k 0)))) (h "1b7pg52d06bliqv95xwkyycy3dkbcjgr3r8asfvywa16zsvvfbwq") (f (quote (("cc" "trackball/cc"))))))

(define-public crate-kiss3d-trackball-0.2.0 (c (n "kiss3d-trackball") (v "0.2.0") (d (list (d (n "kiss3d") (r "^0.31") (d #t) (k 0)) (d (n "nalgebra") (r "^0.26") (d #t) (k 0)) (d (n "trackball") (r "^0.3") (d #t) (k 0)))) (h "1c0995z491mggcl9s4hp1nmvwr2ry6z3cbjd7wks451439p5avm3") (f (quote (("cc" "trackball/cc"))))))

(define-public crate-kiss3d-trackball-0.3.0 (c (n "kiss3d-trackball") (v "0.3.0") (d (list (d (n "kiss3d") (r "^0.31") (d #t) (k 0)) (d (n "nalgebra") (r "^0.26") (d #t) (k 0)) (d (n "trackball") (r "^0.5") (d #t) (k 0)))) (h "01s8hf45cdkk0kkxqkzxmad5qf2a6d6hzq5dcq69fg5x3jp7yyar") (f (quote (("cc" "trackball/cc"))))))

(define-public crate-kiss3d-trackball-0.3.1 (c (n "kiss3d-trackball") (v "0.3.1") (d (list (d (n "kiss3d") (r "^0.31") (d #t) (k 0)) (d (n "nalgebra") (r "^0.26") (f (quote ("libm"))) (k 0)) (d (n "trackball") (r "^0.6") (d #t) (k 0)))) (h "0y4c3yaihaf6gawkn8bdx7avr0440qwj82g94zjzcj37p8j735mm") (f (quote (("cc" "trackball/cc"))))))

(define-public crate-kiss3d-trackball-0.4.0 (c (n "kiss3d-trackball") (v "0.4.0") (d (list (d (n "kiss3d") (r "^0.32") (d #t) (k 0)) (d (n "nalgebra") (r "^0.29") (f (quote ("libm"))) (k 0)) (d (n "trackball") (r "^0.7") (d #t) (k 0)))) (h "11d148ks4ii6jqj3ybmf5cpx3djm2dckrzr4lci6268a4nipw2zw") (f (quote (("cc" "trackball/cc"))))))

(define-public crate-kiss3d-trackball-0.5.0 (c (n "kiss3d-trackball") (v "0.5.0") (d (list (d (n "kiss3d") (r "^0.34") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30") (f (quote ("libm"))) (k 0)) (d (n "trackball") (r "^0.8") (d #t) (k 0)))) (h "0vxmzvxz7bf649njkwqg87gq1l0131wbnn1hirilxdkz41vax73g") (f (quote (("cc" "trackball/cc"))))))

(define-public crate-kiss3d-trackball-0.6.0 (c (n "kiss3d-trackball") (v "0.6.0") (d (list (d (n "kiss3d") (r "^0.34") (d #t) (k 0)) (d (n "trackball") (r "^0.8") (d #t) (k 0)))) (h "1yyqy274azlp0a27p7w8sm1gj5081qxbgb3vvgcirhjz6g1b16hn") (f (quote (("cc" "trackball/cc"))))))

(define-public crate-kiss3d-trackball-0.6.1 (c (n "kiss3d-trackball") (v "0.6.1") (d (list (d (n "kiss3d") (r "^0.34") (d #t) (k 0)) (d (n "trackball") (r "^0.8") (d #t) (k 0)))) (h "1csr0b82hh83qgx7q3pi9hhfrccq6ay2566k68hw1lp5jy9baqi8") (f (quote (("cc" "trackball/cc"))))))

