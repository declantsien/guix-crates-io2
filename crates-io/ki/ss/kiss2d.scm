(define-module (crates-io ki ss kiss2d) #:use-module (crates-io))

(define-public crate-kiss2d-0.1.0 (c (n "kiss2d") (v "0.1.0") (d (list (d (n "bresenham") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.20") (k 0)) (d (n "minifb") (r "^0.11") (d #t) (k 0)) (d (n "rusttype") (r "^0.7") (d #t) (k 0)))) (h "0jx9qfv6dw94f1cc0v1b7k29nrc6mq44anzal4pg351ya6vixm2y")))

(define-public crate-kiss2d-0.1.1 (c (n "kiss2d") (v "0.1.1") (d (list (d (n "image") (r "^0.20") (k 0)) (d (n "minifb") (r "^0.11") (d #t) (k 0)) (d (n "rusttype") (r "^0.7") (d #t) (k 0)))) (h "0wb2jxshvrp1flfljc5cznzbjz7xsvw01g2r7v309inmswvq26zs")))

(define-public crate-kiss2d-0.1.2 (c (n "kiss2d") (v "0.1.2") (d (list (d (n "image") (r "^0.20") (k 0)) (d (n "minifb") (r "^0.11") (d #t) (k 0)) (d (n "rusttype") (r "^0.7") (d #t) (k 0)))) (h "1jrkana07xz7k2gs23fzsrrfapg2m0fyqdyd58w44zn9pk2yfgqn")))

(define-public crate-kiss2d-0.1.3 (c (n "kiss2d") (v "0.1.3") (d (list (d (n "image") (r "^0.20") (k 0)) (d (n "minifb") (r "^0.11") (d #t) (k 0)) (d (n "rusttype") (r "^0.7") (d #t) (k 0)))) (h "1bcw9wx8b68l5sfzifch64v1695iimm9xcifg9q2iji30z526j0m")))

(define-public crate-kiss2d-0.1.4 (c (n "kiss2d") (v "0.1.4") (d (list (d (n "minifb") (r "^0.11") (d #t) (k 0)) (d (n "rusttype") (r "^0.7") (d #t) (k 0)))) (h "1pm5rnf81j3vyl0mp326jipnlv3y45pki2h1yn0awg2hya93xnz0")))

(define-public crate-kiss2d-0.1.5 (c (n "kiss2d") (v "0.1.5") (d (list (d (n "minifb") (r "^0.11") (d #t) (k 0)) (d (n "rusttype") (r "^0.7") (d #t) (k 0)))) (h "0qmqc1q44s2bkvqw7kmwdyfjaf2qfbllky5f7q22brj0km746vd1")))

(define-public crate-kiss2d-0.1.6 (c (n "kiss2d") (v "0.1.6") (d (list (d (n "minifb") (r "^0.11") (d #t) (k 0)) (d (n "rusttype") (r "^0.7") (d #t) (k 0)))) (h "0p90s2x39hbqspd3dy1mpx75aa9004kxip2rc2pv2jqgaqy3a7m1")))

