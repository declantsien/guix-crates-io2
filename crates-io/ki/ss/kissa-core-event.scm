(define-module (crates-io ki ss kissa-core-event) #:use-module (crates-io))

(define-public crate-kissa-core-event-0.1.0 (c (n "kissa-core-event") (v "0.1.0") (d (list (d (n "mlua") (r "^0.9.0-rc.3") (f (quote ("lua54" "parking_lot"))) (d #t) (k 0)))) (h "1985n72gvz9w0rylwbi732i72zn5qcl02wn1db6rxds4rgkbhs6j") (y #t)))

(define-public crate-kissa-core-event-0.1.1 (c (n "kissa-core-event") (v "0.1.1") (d (list (d (n "mlua") (r "^0.9.0-rc.3") (f (quote ("lua54" "parking_lot"))) (d #t) (k 0)))) (h "007dcm9sdvq46bdjzp5w50d8i7mqxzcqdl2i4x1zbp67rw5gmmbr") (y #t)))

(define-public crate-kissa-core-event-0.1.2 (c (n "kissa-core-event") (v "0.1.2") (d (list (d (n "mlua") (r "^0.9.0-rc.3") (f (quote ("lua54" "parking_lot"))) (d #t) (k 0)))) (h "1ki4gl0pxya8jvl66i1mkbnckc8d6432z7h6r1d2asq1kimmgysc") (y #t)))

(define-public crate-kissa-core-event-0.1.3 (c (n "kissa-core-event") (v "0.1.3") (d (list (d (n "mlua") (r "^0.9.0-rc.3") (f (quote ("lua54" "parking_lot"))) (d #t) (k 0)))) (h "0iqaamgpnfnm4qk9j4w8j6j5lrsl6x54c1wyjavgq9qiyhyyl8rc") (y #t)))

