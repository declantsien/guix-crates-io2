(define-module (crates-io bm p8 bmp8bit) #:use-module (crates-io))

(define-public crate-bmp8bit-0.1.0 (c (n "bmp8bit") (v "0.1.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "itertools") (r "^0.12") (d #t) (k 0)))) (h "1gyv5bx62qmiws6s6v6y3dglsnkh0jqfjcci8q8k3b4nd8spzcin")))

