(define-module (crates-io bm s- bms-sm) #:use-module (crates-io))

(define-public crate-bms-sm-0.1.0 (c (n "bms-sm") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "windows") (r "^0.54") (f (quote ("Win32_System_Memory"))) (d #t) (k 0)))) (h "089pvf9frmhmj6c1xw54cqf7daw1wlk5sp2kscyb0alwdppwj74p")))

(define-public crate-bms-sm-0.1.1 (c (n "bms-sm") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "windows") (r "^0.54") (f (quote ("Win32_System_Memory"))) (d #t) (k 0)))) (h "0vn6h10zqwpklh11yynbjlrwic7cwcjbdr45cj49jsvbz4vhd2gg")))

(define-public crate-bms-sm-0.1.2 (c (n "bms-sm") (v "0.1.2") (d (list (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "windows") (r "^0.54") (f (quote ("Win32_System_Memory"))) (d #t) (k 0)))) (h "1ha1pm52llxnikidcmw6d0q5gndmdiy9v3im0rqwqpp6zrs6isrq")))

