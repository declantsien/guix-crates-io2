(define-module (crates-io bm s- bms-rs) #:use-module (crates-io))

(define-public crate-bms-rs-0.1.0 (c (n "bms-rs") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)))) (h "0148qlaw4hcmf7ixkw0vvxskskd0s56brg205dbdvvlx7fnbspwj")))

(define-public crate-bms-rs-0.1.1 (c (n "bms-rs") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)))) (h "19hlhq4dag4ywvrlaqrwn1hx9qb15wg096qs0gr1zhw1fp6whi9g")))

(define-public crate-bms-rs-0.1.2 (c (n "bms-rs") (v "0.1.2") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)))) (h "0pxkqpmxv0b5vnp11l9dg4cq5m5a1nad7c52syl6qpvg4548jfmg")))

(define-public crate-bms-rs-0.1.3 (c (n "bms-rs") (v "0.1.3") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)))) (h "1caq08hwi19mqc7ghcpskd6skjnw28yqhh5q6qr2h599pqc6agap")))

(define-public crate-bms-rs-0.1.4 (c (n "bms-rs") (v "0.1.4") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)))) (h "1yxa98fjhqwhjpzg758da8xsgr717pjsxdhy59qy6i7awrg53l5h")))

(define-public crate-bms-rs-0.1.5 (c (n "bms-rs") (v "0.1.5") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)))) (h "0xqawcf1maq9vixfr4r3f46976drc585d2cipdp7mlv15xy3dh5w")))

(define-public crate-bms-rs-0.1.6 (c (n "bms-rs") (v "0.1.6") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)))) (h "12sagf9qncn1pg6gg4s6g04p2s0c2r5ip46r3sx89j9056786axf")))

(define-public crate-bms-rs-0.1.7 (c (n "bms-rs") (v "0.1.7") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02x3qacaw045d0nldshspbk3aps9qjncjh4a0w0v3v3jddl8fw80") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-bms-rs-0.1.8 (c (n "bms-rs") (v "0.1.8") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1d4pf103yc6sdjgpkkvv7rsj9yak8adwlh43wafjfc1z3ark7xb2") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-bms-rs-0.2.0 (c (n "bms-rs") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04bqz913j9jmbpq10z53xd068zycngr7ikipii98lbskrg6p3ir4") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-bms-rs-0.3.0 (c (n "bms-rs") (v "0.3.0") (d (list (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "190cyk34vxi0mckq1bvv95wdlaqlv01idh5nwkb12i305prw8h6z") (f (quote (("default" "bmson")))) (s 2) (e (quote (("serde" "dep:serde") ("bmson" "dep:serde" "serde_json"))))))

(define-public crate-bms-rs-0.4.0 (c (n "bms-rs") (v "0.4.0") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1np4rzw09nv26rvxl9pa94yxw7vrzhzb2rh73hnjs9764lmmll5s") (f (quote (("default" "bmson")))) (s 2) (e (quote (("serde" "dep:serde") ("bmson" "dep:serde" "serde_json"))))))

(define-public crate-bms-rs-0.4.1 (c (n "bms-rs") (v "0.4.1") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0xhcrfvjx0mml25d3paq3gjgl12n4i2hsm2hf179659m50ypirsr") (f (quote (("default" "bmson")))) (s 2) (e (quote (("serde" "dep:serde") ("bmson" "dep:serde" "serde_json"))))))

(define-public crate-bms-rs-0.4.2 (c (n "bms-rs") (v "0.4.2") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1xl19ms3267x7bpgglw5c6kkml5pcn0y948x54sgqhrzn4ypgfrb") (f (quote (("default" "bmson")))) (s 2) (e (quote (("serde" "dep:serde") ("bmson" "dep:serde" "serde_json"))))))

(define-public crate-bms-rs-0.4.3 (c (n "bms-rs") (v "0.4.3") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "18n202dlwanigsxrd1vpp533z4555bk7j1rj6bapz6zvbr1rrmzi") (f (quote (("default" "bmson")))) (s 2) (e (quote (("serde" "dep:serde") ("bmson" "dep:serde" "serde_json"))))))

(define-public crate-bms-rs-0.4.4 (c (n "bms-rs") (v "0.4.4") (d (list (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1093948w3j48ffvrl8mrdq4kq9wgvq5q5bz9120xdajhxwxqn31k") (f (quote (("default" "bmson")))) (s 2) (e (quote (("serde" "dep:serde") ("bmson" "dep:serde" "serde_json"))))))

