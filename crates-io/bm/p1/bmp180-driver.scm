(define-module (crates-io bm p1 bmp180-driver) #:use-module (crates-io))

(define-public crate-bmp180-driver-0.1.0 (c (n "bmp180-driver") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "00kfg30y4303l37f0p9x2jmzfa47wjk4lridzbvm2bnfxaw06kg6") (r "1.71")))

(define-public crate-bmp180-driver-0.1.1 (c (n "bmp180-driver") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)))) (h "0wpk8bfkgm4ppkd65i0wsl9s0kq5ywfvy3wffgdxxl6lbi9qwabc") (r "1.71")))

