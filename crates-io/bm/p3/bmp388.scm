(define-module (crates-io bm p3 bmp388) #:use-module (crates-io))

(define-public crate-bmp388-0.0.1 (c (n "bmp388") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "0zx7qj0xfqhxlxv578g0x6jy74b3gnw9xhn6a5safp395p6fg3y0") (y #t)))

(define-public crate-bmp388-0.0.2 (c (n "bmp388") (v "0.0.2") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "1ly3075b0g98wyw8jw1da3gn9aqg2y7gh1q55kqqhbsynx6ipr4k") (y #t)))

(define-public crate-bmp388-0.0.3 (c (n "bmp388") (v "0.0.3") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "089sxllrg9yjh941capy2jcsr4gc2vm04kfz5wzp4rxgcq5nyw9i") (y #t)))

(define-public crate-bmp388-0.1.0 (c (n "bmp388") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "05255ihqmh9xlpssjizglpgqkjl9ba86sflfwnaznnjrndpp5915")))

