(define-module (crates-io bm ff bmff) #:use-module (crates-io))

(define-public crate-bmff-0.1.0 (c (n "bmff") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fixed") (r "^1.23") (d #t) (k 0)))) (h "1apikni3pn36ldasxyj8rnq3wrsrw7s9ijakx3490m3wxhmq87h9") (r "1.66")))

(define-public crate-bmff-0.1.1 (c (n "bmff") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fixed") (r "^1.23") (d #t) (k 0)))) (h "0d2h46979gvdhca10lg7z4k56zackk4ll7dlnhd3v8jldwky8wwk") (r "1.66")))

