(define-module (crates-io bm ap bmap) #:use-module (crates-io))

(define-public crate-bmap-0.1.0 (c (n "bmap") (v "0.1.0") (h "15fr709wlqkpv7z8g7x0spqi0zhg9n82p2sjn362jzf4vsf3l4wh")))

(define-public crate-bmap-0.2.0 (c (n "bmap") (v "0.2.0") (h "1b5m0x5xwcx16m8p8n0900ysbahjb3wwkib0wqry6gbgqnvhjp7h")))

(define-public crate-bmap-0.2.1 (c (n "bmap") (v "0.2.1") (h "140bsm38jk5r44dhzn0x15afvcds7qqx6pzw5fza32lmshspklm6")))

(define-public crate-bmap-0.2.2 (c (n "bmap") (v "0.2.2") (h "0ckp104771px5hw4ifiawdj98qwb3mgfxccz5ikdqxn37y559gs4")))

