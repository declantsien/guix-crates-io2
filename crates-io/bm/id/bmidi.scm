(define-module (crates-io bm id bmidi) #:use-module (crates-io))

(define-public crate-bmidi-0.0.1 (c (n "bmidi") (v "0.0.1") (d (list (d (n "clap") (r "*") (d #t) (k 2)) (d (n "dsp-chain") (r "^0.9.0") (d #t) (k 2)) (d (n "pitch_calc") (r "^0.9.7") (d #t) (k 0)) (d (n "synth") (r "^0.9.0") (d #t) (k 2)) (d (n "time_calc") (r "^0.10.0") (d #t) (k 2)))) (h "0374x7cwqhshz3fkfhbd5ygf98d2wzmsfjrq9yirjgch4qkkh3aq")))

(define-public crate-bmidi-0.0.2 (c (n "bmidi") (v "0.0.2") (d (list (d (n "pitch_calc") (r "^0.11.0") (d #t) (k 0)))) (h "0jxq9rvw9lg0swf3a2h4mbqrrc6syr52abir19flv3vs245x36v0")))

(define-public crate-bmidi-0.0.3 (c (n "bmidi") (v "0.0.3") (d (list (d (n "pitch_calc") (r "^0.12.0") (d #t) (k 0)))) (h "0drk847rv37imw5b4rfpa2nawbl5vcl8pgyxsn5zmgr43ny5jy8x")))

