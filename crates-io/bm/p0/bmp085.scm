(define-module (crates-io bm p0 bmp085) #:use-module (crates-io))

(define-public crate-bmp085-0.1.0 (c (n "bmp085") (v "0.1.0") (d (list (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)))) (h "1l86rvl66f8ic6zrmg7mqnny5d9r8mz46amisj4sxahlifiq5pgx")))

(define-public crate-bmp085-0.1.1 (c (n "bmp085") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 2)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "1fyki81g1r23bz164ki9b78594x39kyhmyap4sssmvqa5ifkkr26")))

(define-public crate-bmp085-0.1.2 (c (n "bmp085") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 2)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)))) (h "1w84dl4ad5yf3436ljp1gdvd8x1iw0bi8ihjga2dldnc6jiq7c4q")))

