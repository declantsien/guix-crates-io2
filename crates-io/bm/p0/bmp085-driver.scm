(define-module (crates-io bm p0 bmp085-driver) #:use-module (crates-io))

(define-public crate-bmp085-driver-0.1.0 (c (n "bmp085-driver") (v "0.1.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "generic-array") (r "^0.11.0") (d #t) (k 0)) (d (n "hal") (r "^0.2.0") (d #t) (k 0) (p "embedded-hal")) (d (n "libm") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "0dxqf5rcmf5flw6357wgi7q19wbzzpyf9vgm0whwzcflkwkqz65w") (f (quote (("default" "libm"))))))

(define-public crate-bmp085-driver-0.1.1-alpha.0 (c (n "bmp085-driver") (v "0.1.1-alpha.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "generic-array") (r "^0.11.0") (d #t) (k 0)) (d (n "hal") (r "^0.2.0") (d #t) (k 0) (p "embedded-hal")) (d (n "libm") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "0pmsrrqy9la4r8w1k8jn02n69grpaakykp26103g2wr3pmgm87jh") (f (quote (("default" "libm"))))))

(define-public crate-bmp085-driver-0.1.1 (c (n "bmp085-driver") (v "0.1.1") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "generic-array") (r "^0.11.0") (d #t) (k 0)) (d (n "hal") (r "^0.2.0") (d #t) (k 0) (p "embedded-hal")) (d (n "libm") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "14ivg0dxz0x7k4bd79dis3gpd6hscdlzl6yh85hv9b3cf6ajsqnz") (f (quote (("default" "libm"))))))

(define-public crate-bmp085-driver-0.1.2 (c (n "bmp085-driver") (v "0.1.2") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "generic-array") (r "^0.11.0") (d #t) (k 0)) (d (n "hal") (r "^0.2.0") (d #t) (k 0) (p "embedded-hal")) (d (n "libm") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "0k7sljfka1d4ira3qhkfw80kcw69knjz54rg27hkw0gpi0f4b2nm") (f (quote (("default" "libm"))))))

(define-public crate-bmp085-driver-0.1.3 (c (n "bmp085-driver") (v "0.1.3") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "generic-array") (r "^0.11.0") (d #t) (k 0)) (d (n "hal") (r "^0.2.0") (d #t) (k 0) (p "embedded-hal")) (d (n "libm") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "07n71h8cc4ns49szzhkncd9dlcb7g9lh7nrnb5nvg6n8s76bl5xi") (f (quote (("default" "libm"))))))

(define-public crate-bmp085-driver-0.1.4 (c (n "bmp085-driver") (v "0.1.4") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "generic-array") (r "^0.11.0") (d #t) (k 0)) (d (n "hal") (r "^0.2.0") (d #t) (k 0) (p "embedded-hal")) (d (n "libm") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "0fj4npnhyfga219s0ssvgzmdpqvdrm9dvx0069jjnrcscydfpbqh") (f (quote (("default" "libm"))))))

