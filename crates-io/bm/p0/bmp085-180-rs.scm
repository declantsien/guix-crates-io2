(define-module (crates-io bm p0 bmp085-180-rs) #:use-module (crates-io))

(define-public crate-bmp085-180-rs-0.1.0 (c (n "bmp085-180-rs") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "libm") (r "^0.2.8") (d #t) (k 0)))) (h "1550bbr3l4f0ix5dngbsxyybc8vpj4dh0qxbw4pajgqnyyq0vf4c")))

(define-public crate-bmp085-180-rs-0.2.0 (c (n "bmp085-180-rs") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10.0") (f (quote ("eh1"))) (k 2)) (d (n "libm") (r "^0.2.8") (d #t) (k 0)))) (h "134a061j0fh9if6gi04zjhr70vrkicpw0ibq17kqpbc32nlav4zb")))

