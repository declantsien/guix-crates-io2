(define-module (crates-io bm e2 bme280-rs) #:use-module (crates-io))

(define-public crate-bme280-rs-0.1.0 (c (n "bme280-rs") (v "0.1.0") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1rb03ib9c8h9zwji3zf6iksx7b3wfkmmnxcisa8gl6pv5rdx2fjl")))

(define-public crate-bme280-rs-0.2.0 (c (n "bme280-rs") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^1") (o #t) (k 0)) (d (n "embedded-hal-async") (r "^1") (o #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10") (f (quote ("eh1" "embedded-hal-async"))) (k 2)) (d (n "log") (r "^0.4") (k 0)) (d (n "uom") (r "^0.33") (f (quote ("f32" "si"))) (o #t) (k 0)))) (h "11xadk07qqy7wynlbyzxav29zn6rkpklblf5aazl1ycdmgwsfkvp") (f (quote (("default" "blocking" "async")))) (s 2) (e (quote (("uom" "dep:uom") ("blocking" "dep:embedded-hal") ("async" "dep:embedded-hal-async")))) (r "1.75")))

