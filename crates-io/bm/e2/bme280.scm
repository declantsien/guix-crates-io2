(define-module (crates-io bm e2 bme280) #:use-module (crates-io))

(define-public crate-bme280-0.1.0 (c (n "bme280") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.0") (d #t) (k 2)))) (h "1rxr6vyh7lzq4xijdpzdjk7d08j75l5r0jjfxi64mhz5i5ifqv68")))

(define-public crate-bme280-0.1.1 (c (n "bme280") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.0") (d #t) (k 2)))) (h "0lml8nlky6bag70is1k7ghvn2nca2064fn9xqzia96qw950w6s6i")))

(define-public crate-bme280-0.1.2 (c (n "bme280") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.2.0") (d #t) (k 2)))) (h "13b7cvm9czkz6xyw4m97qwgpymjw8dbxpqwkzyszmyis56vpd06l")))

(define-public crate-bme280-0.2.0 (c (n "bme280") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qrggzcyfydg43fjhz9s880jg34l0r8w68iipzwapx1kmlc0sh7j")))

(define-public crate-bme280-0.2.1 (c (n "bme280") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1x33cfvn44y16xm5vpnsgzx0xj4yivhsh6qz5ip760sn59zkb4xw")))

(define-public crate-bme280-0.3.0 (c (n "bme280") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0cm4bc2js7fp8lgkgc3cx2n4qi16sd6xhqazla8hqk98ybm3kh9j")))

(define-public crate-bme280-0.4.1 (c (n "bme280") (v "0.4.1") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.7.0") (d #t) (k 2)) (d (n "cortex-m-rtic") (r "^1.0.0") (d #t) (k 2)) (d (n "defmt") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "defmt-rtt") (r "^0.3.0") (d #t) (k 2)) (d (n "embedded-hal") (r "^1.0.0-alpha.7") (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "stm32f4xx-hal") (r "^0.12.0") (f (quote ("stm32f411"))) (d #t) (k 2)))) (h "1blr0w16n9hlgggzlnkx0ix2v3xp800yfmc8czvfh0vrrrcyr85g") (f (quote (("with_defmt" "defmt"))))))

(define-public crate-bme280-0.4.4 (c (n "bme280") (v "0.4.4") (d (list (d (n "cortex-m") (r "^0.7.3") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.7.0") (d #t) (k 2)) (d (n "cortex-m-rtic") (r "^1.0.0") (d #t) (k 2)) (d (n "defmt") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "defmt-rtt") (r "^0.3.0") (d #t) (k 2)) (d (n "derive_more") (r "^0.99.17") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "=1.0.0-alpha.7") (d #t) (k 0)) (d (n "panic-semihosting") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "stm32f4xx-hal") (r "^0.12.0") (f (quote ("stm32f411"))) (d #t) (k 2)))) (h "19cxdkhvnid3jlhzmzj0dx5qgkqi4cy07cmpazzg0vbnn9fxhlix") (f (quote (("with_std" "derive_more") ("with_defmt" "defmt"))))))

(define-public crate-bme280-0.5.0 (c (n "bme280") (v "0.5.0") (d (list (d (n "defmt") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "maybe-async-cfg") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1cbcms5wn2y83qsva72b2q31hk8dcjd9985kjqlc9pilpd4fd0qq") (f (quote (("with_std" "derive_more") ("with_defmt" "defmt") ("sync") ("default" "sync") ("async" "embedded-hal-async")))) (y #t)))

(define-public crate-bme280-0.5.1 (c (n "bme280") (v "0.5.1") (d (list (d (n "defmt") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "maybe-async-cfg") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "07yg2hmg9jjq9cmck9n103ccvkad55sh3q5hwpyidwr3j4dwi6hn") (f (quote (("with_std" "derive_more") ("with_defmt" "defmt") ("sync") ("default" "sync") ("async" "embedded-hal-async"))))))

