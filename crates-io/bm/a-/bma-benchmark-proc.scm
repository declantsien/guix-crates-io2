(define-module (crates-io bm a- bma-benchmark-proc) #:use-module (crates-io))

(define-public crate-bma-benchmark-proc-0.0.1 (c (n "bma-benchmark-proc") (v "0.0.1") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full"))) (d #t) (k 0)))) (h "1x7b3hq1f3avrh0lh3f5msm72ldm5ibj4f644332aszf6xwqw1f5")))

(define-public crate-bma-benchmark-proc-0.0.11 (c (n "bma-benchmark-proc") (v "0.0.11") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full"))) (d #t) (k 0)))) (h "0w91xylfzvwv2x2sbk56nf4wsv29444119rdw8cwy1zs3d05blf4")))

(define-public crate-bma-benchmark-proc-0.0.12 (c (n "bma-benchmark-proc") (v "0.0.12") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full"))) (d #t) (k 0)))) (h "1r1j8dc0x8mj21ras2vbmw3dcbl78kf1h03jabgpzhlhx3knrxrl")))

(define-public crate-bma-benchmark-proc-0.0.13 (c (n "bma-benchmark-proc") (v "0.0.13") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full"))) (d #t) (k 0)))) (h "1fs2i5vvrpjbrgvq8zdr1f29rv7diw9960f0gnqj3blvz2frd7dw")))

(define-public crate-bma-benchmark-proc-0.0.14 (c (n "bma-benchmark-proc") (v "0.0.14") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full"))) (d #t) (k 0)))) (h "0xb3qbjg95fraw8grjw34gz69prz3zhj6w1wqiiwyjns2g6lavps")))

(define-public crate-bma-benchmark-proc-0.0.15 (c (n "bma-benchmark-proc") (v "0.0.15") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full"))) (d #t) (k 0)))) (h "0z3mscvwi1zlfhbkcp01azdk3qzrl3pbdjrrjv8l38jrw3y1n00s")))

(define-public crate-bma-benchmark-proc-0.0.16 (c (n "bma-benchmark-proc") (v "0.0.16") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full"))) (d #t) (k 0)))) (h "08dx27nd0grbijrg581ia67cxmgwzww0rr3hk5wngl1x5bd3qhrv")))

(define-public crate-bma-benchmark-proc-0.0.17 (c (n "bma-benchmark-proc") (v "0.0.17") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full"))) (d #t) (k 0)))) (h "0kygf1zh0ndp37myjf6fakvi2nypflxwxsx831kspslrzndp5sgb")))

(define-public crate-bma-benchmark-proc-0.0.18 (c (n "bma-benchmark-proc") (v "0.0.18") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full"))) (d #t) (k 0)))) (h "0kcrasw9m3wwc5jqnpjdz0ffmyf5xwgxa056jd78bv27r5dlrmvy")))

