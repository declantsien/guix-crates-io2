(define-module (crates-io bm a- bma-jrpc-derive) #:use-module (crates-io))

(define-public crate-bma-jrpc-derive-0.1.0 (c (n "bma-jrpc-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "071i5125bcin6n8jyn29hv1c3wan1fcidil643vnv48phmbj6zl4")))

(define-public crate-bma-jrpc-derive-0.1.1 (c (n "bma-jrpc-derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0656qyj3h4w1h7wdlbpbb4544fllpm3jps3zzy8gm271yrkawlqy")))

