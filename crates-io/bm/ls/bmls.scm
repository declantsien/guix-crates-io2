(define-module (crates-io bm ls bmls) #:use-module (crates-io))

(define-public crate-bmls-1.0.0 (c (n "bmls") (v "1.0.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "matrixmultiply") (r "^0.3.8") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1ghkrbjv108vpqvdq12qdbp5qlp7x2f8r9qr4lv3scg56ifhr72q")))

(define-public crate-bmls-1.1.1 (c (n "bmls") (v "1.1.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "matrixmultiply") (r "^0.3.8") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1iz1d79wjpkfjhmzla50235gp5cri3w4pbh9c2g8yg6gvmfaahd8") (f (quote (("ndarray") ("default"))))))

