(define-module (crates-io bm i2 bmi270) #:use-module (crates-io))

(define-public crate-bmi270-0.1.0 (c (n "bmi270") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 0)))) (h "1lz4dsyrfdyj9m7fhi62zml6k07sj6jsr9n76rqlrykmrrvcq0vv")))

(define-public crate-bmi270-0.1.1 (c (n "bmi270") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.2") (d #t) (k 0)))) (h "0b2gfiq74r7gbsj86c69rlzc4xz1qmb86dpv1y6kkj50hmqjdzvh")))

