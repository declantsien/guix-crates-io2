(define-module (crates-io bm k_ bmk_linux) #:use-module (crates-io))

(define-public crate-bmk_linux-0.2.0 (c (n "bmk_linux") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "0ix5s4gbx9r1qn5bn9z7wg8pd11xcd7v5whg2wx1s6y0sdf63ngl")))

(define-public crate-bmk_linux-0.2.1 (c (n "bmk_linux") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "15ahy4mf60aszc5xflaifj5q4b55hj0ks7lqpl901mm4jgcfssyi")))

(define-public crate-bmk_linux-0.2.2 (c (n "bmk_linux") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "1h170hhg9px4dn5n91i4q91nc7fm35003w8cg3mnngkmfj7rnznx")))

