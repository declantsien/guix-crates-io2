(define-module (crates-io bm p- bmp-client) #:use-module (crates-io))

(define-public crate-bmp-client-0.1.0 (c (n "bmp-client") (v "0.1.0") (h "0kn7iml79m44kba350aha0h056z91rb9xfb7sjnc396lv7q05mb0")))

(define-public crate-bmp-client-0.1.1 (c (n "bmp-client") (v "0.1.1") (d (list (d (n "bmp-protocol") (r "=0.1.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("tcp"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("net" "rt-core" "macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0cb4n73mjmig5hwc9370mndfxx7niy0991ai61nd94yxnrik7j5s")))

