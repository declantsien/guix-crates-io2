(define-module (crates-io bm p- bmp-rust) #:use-module (crates-io))

(define-public crate-bmp-rust-0.1.0 (c (n "bmp-rust") (v "0.1.0") (h "1qqxg4pfm81c8yj5iv36w9296nqba86qxjxbqs6inzmx6p01bk6h")))

(define-public crate-bmp-rust-0.1.1 (c (n "bmp-rust") (v "0.1.1") (h "16m33symxx1p4app5nzmp5cf3wzzfbf3sfffarz8m8k1v93q1wlh")))

(define-public crate-bmp-rust-0.1.2 (c (n "bmp-rust") (v "0.1.2") (h "15i3p7hjaxn82wlyxp33cnr9yyikd497qb44jfmj68wxk9i9yxyy")))

(define-public crate-bmp-rust-0.2.0 (c (n "bmp-rust") (v "0.2.0") (h "1bzvqv42mrpvchahzrxh1xfxfnwblgaqjzdx2xhqfjspgf29xqkm")))

(define-public crate-bmp-rust-0.2.1 (c (n "bmp-rust") (v "0.2.1") (h "1vhp9pqkv5xhzc2va39y0lq7w2k1w1vkynh9k8xm0kay6dqi23ml")))

(define-public crate-bmp-rust-0.2.2 (c (n "bmp-rust") (v "0.2.2") (h "1vm4rlgnkj8kp5p2nr6f314r6gchh1sihbi1qrq5ap183gylm1ms") (y #t)))

(define-public crate-bmp-rust-0.2.3 (c (n "bmp-rust") (v "0.2.3") (h "0ajdvnpvhn6zlw4rw07ipb75x4779q4q0ndd71cjq6gs5j62rh2p") (y #t)))

(define-public crate-bmp-rust-0.2.4 (c (n "bmp-rust") (v "0.2.4") (h "0qkzr2hdjwvlz3j1mmlf504czc19sg8y3db31bi46liwizm992g1")))

(define-public crate-bmp-rust-0.2.5 (c (n "bmp-rust") (v "0.2.5") (h "0sw2k9ag4d1aq19gr9rbbxrvvx8y8jmsvnjhlm31ffp6d9jks7zg")))

(define-public crate-bmp-rust-0.2.6 (c (n "bmp-rust") (v "0.2.6") (h "0vfm9xh0y3nkka07kpskrx4maxxyswhz9dmjyjckc0iy9yimwgac")))

(define-public crate-bmp-rust-0.3.0 (c (n "bmp-rust") (v "0.3.0") (h "1yarhsd1rb9lig7sl46x9vc4ygi5qr51yfbawhx6wis39r46wscy")))

(define-public crate-bmp-rust-0.3.1 (c (n "bmp-rust") (v "0.3.1") (h "1ins2pv7xyiqvg6vjrjxm84ciadf50x7lz01qn6c5wlii788sgcs")))

(define-public crate-bmp-rust-0.3.2 (c (n "bmp-rust") (v "0.3.2") (h "0dd2fza3zpbxxw36mh69bifk4j3kv39xsabnj0xsapq5zh3fpays")))

(define-public crate-bmp-rust-0.3.3 (c (n "bmp-rust") (v "0.3.3") (h "0hwrbv49i50j8qhyjrbbks5sbs4x0wiyj24c174ccqkga5c1dcbp")))

(define-public crate-bmp-rust-0.3.4 (c (n "bmp-rust") (v "0.3.4") (h "08qckl9jg6c5y82km5y4395mmcvk1znqqdw3qmvv7n1lnsl0cd88")))

(define-public crate-bmp-rust-0.4.0 (c (n "bmp-rust") (v "0.4.0") (h "1va1q5ms05glwi06q9zcibrgczsynl4jiw2z1g9ppqz5hmgh7wr0")))

(define-public crate-bmp-rust-0.4.1 (c (n "bmp-rust") (v "0.4.1") (h "190gidhr9850hf5llchmkgvh4nnd3x7rngg4hizs12cr0ylrbcvk")))

