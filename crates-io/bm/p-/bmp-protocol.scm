(define-module (crates-io bm p- bmp-protocol) #:use-module (crates-io))

(define-public crate-bmp-protocol-0.1.0 (c (n "bmp-protocol") (v "0.1.0") (h "1l21w1vqd4f74k8k6hsgy1x5gm2p3zizlndlpiz05z2k27kzhip1")))

(define-public crate-bmp-protocol-0.1.3 (c (n "bmp-protocol") (v "0.1.3") (d (list (d (n "bgp-rs") (r "=0.6") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (f (quote ("i128"))) (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "hashbrown") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("tcp"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "net" "rt-core" "macros"))) (d #t) (k 2)) (d (n "tokio-util") (r "^0.3") (f (quote ("codec"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1kmfkj52phrjhn5905ri1yk3z9gawbc0r81a7bryfpv7yfkj0b07")))

