(define-module (crates-io bm p- bmp-monochrome) #:use-module (crates-io))

(define-public crate-bmp-monochrome-0.1.0 (c (n "bmp-monochrome") (v "0.1.0") (h "14sz5243979q2809r6fi5q3v8j1mx44gjbr3ni68cy0xkbbbzlax")))

(define-public crate-bmp-monochrome-0.2.0 (c (n "bmp-monochrome") (v "0.2.0") (h "1jw6f4m2gdcnyrr6d6iacvjz16lp8h482a19rhb4di69a0dqa1jp")))

(define-public crate-bmp-monochrome-0.3.0 (c (n "bmp-monochrome") (v "0.3.0") (h "06h87yjqf4481ixqc9r8kpc0cdsg7pr7y5pg2rvl7y1jn89xxrmf")))

(define-public crate-bmp-monochrome-0.4.0 (c (n "bmp-monochrome") (v "0.4.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0s0qhw0vdjvw2acx4zx7yxhs8y46l8jxgjwz504pv246vcrxf8ax")))

(define-public crate-bmp-monochrome-0.6.0 (c (n "bmp-monochrome") (v "0.6.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "19d3mppjyiwlslhmhypsavhx9snqxh68n3ab9q96mmr8hxm2hq0f")))

(define-public crate-bmp-monochrome-0.7.0 (c (n "bmp-monochrome") (v "0.7.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "00vf1jknm8fc2nc8r0sqk8yvych4zjc3w2vzm0krf2l47ab2kcrd")))

(define-public crate-bmp-monochrome-0.8.0 (c (n "bmp-monochrome") (v "0.8.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1i52qb7nh71lr42iyf7kfzaar9d6hndpahfraxbypdasfmgq618y")))

(define-public crate-bmp-monochrome-0.9.0 (c (n "bmp-monochrome") (v "0.9.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1pkmc487nwhgg0nhiwrymqkvhikca16754hppq62a553yv5yswhi")))

(define-public crate-bmp-monochrome-0.10.0 (c (n "bmp-monochrome") (v "0.10.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1iy5snzfw2s58vki1scghw85jp0v6d8i7pk53886r7bvsv0n41qa")))

(define-public crate-bmp-monochrome-0.11.0 (c (n "bmp-monochrome") (v "0.11.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1gy9yzh919095fx9shdx1fvrr8p1yr28a5hcmdgnc3mjzxbspa0d")))

(define-public crate-bmp-monochrome-0.12.0 (c (n "bmp-monochrome") (v "0.12.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0gqh2m6n9icd56bk90an9d2yc5irkd5nky56mb4pa1y9pj99pdif")))

(define-public crate-bmp-monochrome-0.13.0 (c (n "bmp-monochrome") (v "0.13.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1xh3m3cxdnfsmxhxjfhrmc2id8r246v0g53x0ynnny7f448lhcm9")))

(define-public crate-bmp-monochrome-0.14.0 (c (n "bmp-monochrome") (v "0.14.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0aadyjjpdsjv4nzkh5m3dyz3hh7dih1bc2zwndy5yy1911ykihv7") (y #t)))

(define-public crate-bmp-monochrome-0.15.0 (c (n "bmp-monochrome") (v "0.15.0") (d (list (d (n "arbitrary") (r "^0.4.7") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0n123rmda6ma560l09mdz16ldvqrjj7qa4i22n0s8mg1gdbb2jhb") (f (quote (("fuzz" "arbitrary"))))))

(define-public crate-bmp-monochrome-0.16.0 (c (n "bmp-monochrome") (v "0.16.0") (d (list (d (n "arbitrary") (r "^0.4.7") (f (quote ("derive"))) (o #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1zc24r6lcvwnawldh2ramblfrzaxq4ppzpf19041j2qzwp23mdvm") (f (quote (("fuzz" "arbitrary"))))))

(define-public crate-bmp-monochrome-0.17.0 (c (n "bmp-monochrome") (v "0.17.0") (d (list (d (n "arbitrary") (r "^0.4.7") (f (quote ("derive"))) (o #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "image") (r "^0.23.10") (f (quote ("bmp"))) (o #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1nxrkfqlaapgnvbavhmyrcs1i9hsl397rrd4f6z68wk6hg6iikiz") (f (quote (("fuzz" "arbitrary" "image"))))))

(define-public crate-bmp-monochrome-1.0.0 (c (n "bmp-monochrome") (v "1.0.0") (d (list (d (n "arbitrary") (r "^0.4.7") (f (quote ("derive"))) (o #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "image") (r "^0.23.10") (f (quote ("bmp"))) (o #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1hc0mlmy26jpvx34rw896ki60wkb6c2finnsfs3zvzfin5bwpmi0") (f (quote (("fuzz" "arbitrary" "image"))))))

(define-public crate-bmp-monochrome-1.1.0 (c (n "bmp-monochrome") (v "1.1.0") (d (list (d (n "arbitrary") (r "^0.4.7") (f (quote ("derive"))) (o #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "image") (r "^0.23.10") (f (quote ("bmp"))) (o #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0rv50kszwmjg3ricw3j6924n44bclmfdhbnwfrsdw7vnscmhi6l2") (f (quote (("fuzz" "arbitrary" "image"))))))

