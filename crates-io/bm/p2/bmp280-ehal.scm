(define-module (crates-io bm p2 bmp280-ehal) #:use-module (crates-io))

(define-public crate-bmp280-ehal-0.0.1 (c (n "bmp280-ehal") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "19mmclzs0rhcqakjy40b2a9pfwsc4mcxfma8b3jp9mlrrg7zjmrs")))

(define-public crate-bmp280-ehal-0.0.2 (c (n "bmp280-ehal") (v "0.0.2") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "03rlf0ny42bjyjmq7rxf5f4ccq66lbdclrps35ys3r61fvq3b67d")))

(define-public crate-bmp280-ehal-0.0.3 (c (n "bmp280-ehal") (v "0.0.3") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "1i7iac540fi4ba2wx7xag1j9wpaj9jbyijyk0rzmqdp2d8dyrhnp")))

(define-public crate-bmp280-ehal-0.0.5 (c (n "bmp280-ehal") (v "0.0.5") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "1k4mzjc70wjzydj222y4z981kqb4kd6l00jygp8cz3s7gq9rhb2y")))

(define-public crate-bmp280-ehal-0.0.6 (c (n "bmp280-ehal") (v "0.0.6") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "0xybn2x6j4vp8n1fcxl5a6q3dfp35ii0gzxpdvvnysn9pfpfz16z")))

