(define-module (crates-io bm p2 bmp280-rs) #:use-module (crates-io))

(define-public crate-bmp280-rs-0.1.0 (c (n "bmp280-rs") (v "0.1.0") (d (list (d (n "defmt") (r "^0.1.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)) (d (n "snafu") (r "^0.6.10") (k 0)))) (h "1lhca6a14v9ikk5xw05i5zh0ysy1lmxzd1na2qjp321l8p2kbx22")))

(define-public crate-bmp280-rs-0.1.1 (c (n "bmp280-rs") (v "0.1.1") (d (list (d (n "defmt") (r "^0.1.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)) (d (n "snafu") (r "^0.6.10") (k 0)))) (h "0byqlqcd0m37ig5sblbk47ib3lb68q24kcvs3jr53ywd4skwqp2b")))

(define-public crate-bmp280-rs-0.1.2 (c (n "bmp280-rs") (v "0.1.2") (d (list (d (n "defmt") (r "^0.3.5") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)) (d (n "snafu") (r "^0.6.10") (k 0)))) (h "1aqnds55h806kmnba3x3bkyp2mqgz74hzxwhxclbaiqk91gw3abw")))

