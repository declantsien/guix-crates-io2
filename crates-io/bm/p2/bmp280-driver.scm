(define-module (crates-io bm p2 bmp280-driver) #:use-module (crates-io))

(define-public crate-bmp280-driver-0.0.6 (c (n "bmp280-driver") (v "0.0.6") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "1wf2sb1rbbr8rzj42fwv4dnsbx4qx71rzsc7535k1xqsqlgpm2bs")))

(define-public crate-bmp280-driver-0.0.7 (c (n "bmp280-driver") (v "0.0.7") (d (list (d (n "embedded-hal") (r "^0.2.1") (d #t) (k 0)))) (h "0wxkrdymsmabd1gnggl0xz5ygb4g7mynxmv1s59hck47bmkyn0v8")))

