(define-module (crates-io bm p2 bmp280) #:use-module (crates-io))

(define-public crate-bmp280-0.1.0 (c (n "bmp280") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.0") (d #t) (k 0)))) (h "19nwnzgr8ldmqz5wx86i5iqcyphpdvxpnf4qgn8b6wjnqdxzpj7d")))

(define-public crate-bmp280-0.1.1 (c (n "bmp280") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.0") (d #t) (k 0)))) (h "1fi4pxwqyyar3026qlswl77mfx676qwn8lqmacww7qlisyypy3qn")))

(define-public crate-bmp280-0.1.2 (c (n "bmp280") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.0") (d #t) (k 0)))) (h "0j8wj1jbh3yab89a154k60nmys53ly0x1jj8sdfb17nka4yrfxg6")))

(define-public crate-bmp280-0.2.1 (c (n "bmp280") (v "0.2.1") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.0") (d #t) (k 0)))) (h "0k53g5b6idvcy0lpvda10iaz51yfhf5byw0a2075mdx8ssgfga2s")))

(define-public crate-bmp280-0.3.0 (c (n "bmp280") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.0") (d #t) (k 0)))) (h "1bph209ldmf9jypr2kmv1qadwsnlkphl00lby121314bj4hgsj30")))

(define-public crate-bmp280-0.4.0 (c (n "bmp280") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "i2cdev") (r "^0.4.4") (d #t) (k 0)))) (h "07pab46s2y0icyr6q4s92f6lv0gw09ckm0hvgp4kkqc4nav9kp5a")))

