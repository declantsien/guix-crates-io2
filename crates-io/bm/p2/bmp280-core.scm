(define-module (crates-io bm p2 bmp280-core) #:use-module (crates-io))

(define-public crate-bmp280-core-0.1.0 (c (n "bmp280-core") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "1d778xr5ch2zffr88a1hsqysx5y1lr5lb0d4z01405ka358i4jp1")))

(define-public crate-bmp280-core-0.1.1 (c (n "bmp280-core") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "0scigqxmic5h3wbika5grkp0mhjs76y4lx9hrvcyja3mcmx3a3r4")))

(define-public crate-bmp280-core-0.1.2 (c (n "bmp280-core") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "14nq66abd65jpckq80vpgwb8bmkgkrbg9n4qr37p0m1pqv8gcprl")))

(define-public crate-bmp280-core-0.2.0 (c (n "bmp280-core") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "18xz4nfdr4746b64ll8fjjkgqd37yhi8pf8zar5w902856b38158")))

