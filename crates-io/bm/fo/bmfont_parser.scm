(define-module (crates-io bm fo bmfont_parser) #:use-module (crates-io))

(define-public crate-bmfont_parser-0.1.0 (c (n "bmfont_parser") (v "0.1.0") (h "1dj7ld5vwfq0jlpzv01kxxhm0v1wkgxlly9mpw10ps9bjcl79d4w")))

(define-public crate-bmfont_parser-0.1.1 (c (n "bmfont_parser") (v "0.1.1") (h "04lrcxs0sq68gfrfqskf9kq12vabpzwi4hxihs9q1xcp0fbzsavj")))

(define-public crate-bmfont_parser-0.2.0 (c (n "bmfont_parser") (v "0.2.0") (h "135fywg97wfb3gi87wkwgs78nyi98y08w0wgq99czmy9qb36mfky")))

