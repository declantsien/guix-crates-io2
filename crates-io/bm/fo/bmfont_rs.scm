(define-module (crates-io bm fo bmfont_rs) #:use-module (crates-io))

(define-public crate-bmfont_rs-0.1.0 (c (n "bmfont_rs") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "roxmltree") (r "^0.14.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 2)))) (h "18nqckkg6ma6k95b990cjgqic6parlvm70akgqyhmkckwkc0kmbh") (f (quote (("xml" "roxmltree") ("serde_boolint"))))))

(define-public crate-bmfont_rs-0.2.0 (c (n "bmfont_rs") (v "0.2.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 2)) (d (n "roxmltree") (r "^0.14.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.67") (d #t) (k 2)))) (h "19ikhqwhlx84hr33wlh0a6wriqwwj9kq15b4ywc3lfdnn4h6ffml") (f (quote (("xml" "roxmltree") ("serde_boolint"))))))

