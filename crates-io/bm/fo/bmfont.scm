(define-module (crates-io bm fo bmfont) #:use-module (crates-io))

(define-public crate-bmfont-0.1.0 (c (n "bmfont") (v "0.1.0") (d (list (d (n "glium") (r "*") (d #t) (k 2)) (d (n "image") (r "*") (d #t) (k 2)))) (h "07cal793h6hignzaykr0my31xkrhyca6l2yj0m6l0bwmnjbvby22")))

(define-public crate-bmfont-0.2.0 (c (n "bmfont") (v "0.2.0") (d (list (d (n "glium") (r "^0.13") (d #t) (k 2)) (d (n "image") (r "^0.6") (d #t) (k 2)))) (h "0xfh8fyv0kahkkp3b56pcrg9pazw0x3633qcaw81mx3limx0sh5g")))

(define-public crate-bmfont-0.2.1 (c (n "bmfont") (v "0.2.1") (d (list (d (n "glium") (r "^0.13") (d #t) (k 2)) (d (n "image") (r "^0.6") (d #t) (k 2)))) (h "0xgc7lzif4czvn3i9m76929109ij2kk774nbyn1jbns0dxknymhc")))

(define-public crate-bmfont-0.3.1 (c (n "bmfont") (v "0.3.1") (d (list (d (n "glium") (r "^0.16.0") (d #t) (k 2)) (d (n "image") (r "^0.10.3") (d #t) (k 2)))) (h "1r3i8p4r62x2vvsdbndjs7dmcjyjsl5df2w2rrzc5blw8vxzihwc")))

(define-public crate-bmfont-0.3.2 (c (n "bmfont") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "glium") (r "^0.16.0") (d #t) (k 2)) (d (n "image") (r "^0.10.3") (d #t) (k 2)))) (h "010pwy0aydfwy7847sv9i1q9vmp2fw4wxh798vxll8dh5k7g4gxz") (f (quote (("parse-error") ("default" "parse-error"))))))

(define-public crate-bmfont-0.3.3 (c (n "bmfont") (v "0.3.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "glium") (r "^0.16.0") (d #t) (k 2)) (d (n "image") (r "^0.10.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "18z31rcpmnw861cnv15301xf7a6bjf2kfw0yvj5xmvk4v9wc2r3l") (f (quote (("parse-error") ("default" "parse-error"))))))

