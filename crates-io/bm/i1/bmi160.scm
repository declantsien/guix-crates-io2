(define-module (crates-io bm i1 bmi160) #:use-module (crates-io))

(define-public crate-bmi160-0.1.0 (c (n "bmi160") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "1kmckwnkq9h5hs9mlxvnjfr95amlpkz5iwh33ywpsbp6cm09v2vn")))

(define-public crate-bmi160-0.1.1 (c (n "bmi160") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "0f5krdxfmha6xx7sfffrs0pnvki959v41b2rwf9mgzsd9smy9l4d")))

(define-public crate-bmi160-0.1.2 (c (n "bmi160") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9") (d #t) (k 2)) (d (n "linux-embedded-hal") (r "^0.3") (d #t) (k 2)))) (h "0zj8yh2z4gp8z7q7cv3p41c4ixi8b7sfv8h6sbn82l25vi08a650")))

(define-public crate-bmi160-1.0.0 (c (n "bmi160") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10.0") (f (quote ("eh1"))) (k 2)) (d (n "linux-embedded-hal") (r "^0.4.0") (d #t) (k 2)))) (h "1gzd7nllh9p8z37y81y3nlb8vrm6dyg63iwzr1x6fk1hxs9b0q0j")))

(define-public crate-bmi160-1.1.0 (c (n "bmi160") (v "1.1.0") (d (list (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.10.0") (f (quote ("eh1"))) (k 2)) (d (n "linux-embedded-hal") (r "^0.4.0") (d #t) (k 2)))) (h "1an2w8x8jr9sbdfcjn7d85yjsjqqrqs31mcc6rvjzqs4lp285w88")))

