(define-module (crates-io bm d- bmd-speededitor) #:use-module (crates-io))

(define-public crate-bmd-speededitor-0.1.4 (c (n "bmd-speededitor") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hidapi") (r "^1.4.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0czvavr918f8xkxpzxgbv0iqcgn9h7yxprndr9d59rjrljzc3mdp")))

(define-public crate-bmd-speededitor-0.2.0 (c (n "bmd-speededitor") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hidapi") (r "^1.4.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0p4jrzq62n099ldl59fgvap2ffz97k4kj00ffhf2r4rjn0l2qwnv")))

(define-public crate-bmd-speededitor-0.2.3 (c (n "bmd-speededitor") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "hidapi") (r "^1.4.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "08l48f6qk2mhdxib6fn1mm7z5w906x7nbh6hswjnvvgsncldy1gq")))

