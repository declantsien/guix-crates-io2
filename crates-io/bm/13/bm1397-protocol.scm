(define-module (crates-io bm #{13}# bm1397-protocol) #:use-module (crates-io))

(define-public crate-bm1397-protocol-0.1.0 (c (n "bm1397-protocol") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "crc-any") (r "^2.4.3") (k 0)) (d (n "defmt") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "1karj9xmgd1scz4wmnvwal6pw77g4ymdnvf1lh0avn4wa35qm58q") (s 2) (e (quote (("defmt" "dep:defmt"))))))

(define-public crate-bm1397-protocol-0.2.0 (c (n "bm1397-protocol") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "crc-any") (r "^2.4.3") (k 0)) (d (n "defmt") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "08bk126678jmvsnrpahwc5wm7l9cxbw08lqak3z30yanlm6fw5rq") (s 2) (e (quote (("defmt" "dep:defmt"))))))

