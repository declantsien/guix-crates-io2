(define-module (crates-io bm f- bmf-parse) #:use-module (crates-io))

(define-public crate-bmf-parse-0.1.0 (c (n "bmf-parse") (v "0.1.0") (d (list (d (n "bstringify") (r "^0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0v3dcj3nc108fs8vh6xvfwsw3n7i2dvbhbdy75fjmxkiwkly6qnr") (y #t)))

(define-public crate-bmf-parse-0.1.1 (c (n "bmf-parse") (v "0.1.1") (d (list (d (n "bstringify") (r "^0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1y5ipcsvg8yipb5zfb5i6sc4ilb5ql9wb5x9vl6pm93rv9a4x3kv")))

(define-public crate-bmf-parse-0.1.2 (c (n "bmf-parse") (v "0.1.2") (d (list (d (n "bstringify") (r "^0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "15yjk7387jl9cpgz5lqkhcd8qyvfjsd1ax56qbbm89xv8sspnbab")))

(define-public crate-bmf-parse-0.1.3 (c (n "bmf-parse") (v "0.1.3") (d (list (d (n "bstringify") (r "^0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "17lgbyv3bwlwk57ffax8wngj8d5lqmi0w213bppzkmhw5fi5iqva")))

(define-public crate-bmf-parse-0.1.4 (c (n "bmf-parse") (v "0.1.4") (d (list (d (n "bstringify") (r "^0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1jkp9v56yqx05fmhgccdcksqvbcqbi2vfmkr4sfa45mqgp9n6xqn")))

(define-public crate-bmf-parse-0.1.5 (c (n "bmf-parse") (v "0.1.5") (d (list (d (n "bstringify") (r "^0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1dpajmaicl37makpxf43im4ivc709mdaswbjq2lxnghqkqlkf68w")))

(define-public crate-bmf-parse-0.1.6 (c (n "bmf-parse") (v "0.1.6") (d (list (d (n "bstringify") (r "^0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1db5b213l5sanhm9mss2xls196nxrqnmwnpsg3f4n9qxqnfc9rgw")))

(define-public crate-bmf-parse-0.1.7 (c (n "bmf-parse") (v "0.1.7") (d (list (d (n "bstringify") (r "^0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0kawxrgzykyss02zj0kg21apif1ga6mbx8w9h31slp73gnicgnl9")))

