(define-module (crates-io bm a4 bma400) #:use-module (crates-io))

(define-public crate-bma400-0.1.0 (c (n "bma400") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9.0") (d #t) (k 2)))) (h "1vkpp884b1sq9kf0pzsm1nwfidg13pv0yyiyszy23jq1dll9cnv0") (f (quote (("spi") ("i2c-default" "i2c") ("i2c-alt" "i2c") ("i2c") ("float"))))))

(define-public crate-bma400-0.1.1 (c (n "bma400") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9.0") (d #t) (k 2)))) (h "1zvsii6kgi62vrz6qfmj6c21s1mh8sqh1lywpjdsjr1fy0a0gdq0") (f (quote (("spi") ("i2c-default" "i2c") ("i2c-alt" "i2c") ("i2c") ("float"))))))

(define-public crate-bma400-0.2.0 (c (n "bma400") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.9.0") (d #t) (k 2)))) (h "0j0lxsmalx1d1bbg0c436i1qicccx94i7wblivc00s3cz10f45xq") (f (quote (("spi") ("i2c-default" "i2c") ("i2c-alt" "i2c") ("i2c") ("float"))))))

