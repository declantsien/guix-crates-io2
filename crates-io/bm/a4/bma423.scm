(define-module (crates-io bm a4 bma423) #:use-module (crates-io))

(define-public crate-bma423-0.0.1 (c (n "bma423") (v "0.0.1") (d (list (d (n "accelerometer") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "bitmask-enum") (r "^1.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (k 0)))) (h "1hwz27n0pc0i33vwjs696rv08cm0g6pcrl9a4zp080v33vcrawsl") (f (quote (("default" "accel") ("accel" "accelerometer"))))))

(define-public crate-bma423-0.0.2 (c (n "bma423") (v "0.0.2") (d (list (d (n "accelerometer") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "bitmask-enum") (r "^1.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (k 0)))) (h "1knds7d37xbkyij4jpkwibjdw43lbbrm3vcjapxdiy4nnbn56m4i") (f (quote (("default" "accel") ("accel" "accelerometer"))))))

(define-public crate-bma423-0.0.3 (c (n "bma423") (v "0.0.3") (d (list (d (n "accelerometer") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "bitmask-enum") (r "^1.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (k 0)))) (h "12h1kr6yx4cs2n02rzhhn8gh3l9lvqwad212mahvgdl4pqrh2a3n") (f (quote (("default" "accel") ("accel" "accelerometer"))))))

