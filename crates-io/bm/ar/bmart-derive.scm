(define-module (crates-io bm ar bmart-derive) #:use-module (crates-io))

(define-public crate-bmart-derive-0.1.0 (c (n "bmart-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full"))) (d #t) (k 0)))) (h "1cj608q1v5qnmvs4qgkg15d5y2qn61r95vx921ca1pz1fcykyzq6")))

(define-public crate-bmart-derive-0.1.1 (c (n "bmart-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04wbhqhy0y54d7q4wmrzrkapcdl3bhy1a7apgca5gsrwrs0wxc26")))

(define-public crate-bmart-derive-0.1.2 (c (n "bmart-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fks4g507f2cc4l9q8s7igm93kmvg3yq8lqmmlr8h7kdbicc4dzl")))

(define-public crate-bmart-derive-0.1.3 (c (n "bmart-derive") (v "0.1.3") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lz81h0pxjk932akwkpbybb0kjgp1j19lki3r2d1z80z0b2465s0")))

