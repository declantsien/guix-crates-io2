(define-module (crates-io bm ar bmark) #:use-module (crates-io))

(define-public crate-bmark-0.1.0 (c (n "bmark") (v "0.1.0") (h "0p5fh72swik126vjfkjjyf1hy8y09ppyxmbzfd86iimqlnn1b0qk") (y #t)))

(define-public crate-bmark-0.1.1 (c (n "bmark") (v "0.1.1") (d (list (d (n "directories") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "06v235d6l584ykh2lviip2jqrrhkgaa4mplq2whl2mwipgg1jnkc")))

(define-public crate-bmark-0.1.2 (c (n "bmark") (v "0.1.2") (d (list (d (n "directories") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "06m8r9dbm89fa4czqn3qqc6g66z87j92v1mlsyb1666pfdkf45pw")))

(define-public crate-bmark-0.1.3 (c (n "bmark") (v "0.1.3") (d (list (d (n "directories") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "00imz08mv2pwmy9v1c53pap9fmfbnqara6r2srssjjvbxb3q77n1")))

(define-public crate-bmark-0.1.4 (c (n "bmark") (v "0.1.4") (d (list (d (n "directories") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "1lm78lldwl9xzidad2vvgs85149xxcl66fl30ackwbrxp83ydn39")))

(define-public crate-bmark-0.1.5 (c (n "bmark") (v "0.1.5") (d (list (d (n "directories") (r "^5.0.0") (d #t) (k 0)) (d (n "fuzzy_finder") (r "^0.3.2") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8.1") (d #t) (k 0)) (d (n "rofi") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "00iwkfl6qba3f8c8ljmp5f399nlph0zqm6b0qvpvyvyc0wxchicp")))

