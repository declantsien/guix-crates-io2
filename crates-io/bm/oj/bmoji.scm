(define-module (crates-io bm oj bmoji) #:use-module (crates-io))

(define-public crate-bmoji-0.1.0 (c (n "bmoji") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "emojis") (r "^0.3") (d #t) (k 0)))) (h "1kkbvx36ga9mrvz7172779wzw1hf41w6hw2k76nxgmrgzpaq4y4a")))

(define-public crate-bmoji-0.2.0 (c (n "bmoji") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "emojis") (r "^0.3") (d #t) (k 0)))) (h "0xfh4jm5j9m32qqy1yvqiz487fcrifdp49iqf4a8h1xysj1ngp4j")))

(define-public crate-bmoji-0.2.1 (c (n "bmoji") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "emojis") (r "^0.3") (d #t) (k 0)))) (h "0lqdzdrkgf7f7jgmjab5f79pzpdcnzvzjsy9plbvivjw2cmib8rh")))

