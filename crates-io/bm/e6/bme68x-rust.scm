(define-module (crates-io bm e6 bme68x-rust) #:use-module (crates-io))

(define-public crate-bme68x-rust-0.1.0 (c (n "bme68x-rust") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0i2nq32gm464nf4g6rq1ah0s2359n3jafdb7rwir215jri5ccf3w")))

(define-public crate-bme68x-rust-0.1.1 (c (n "bme68x-rust") (v "0.1.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ikm77x065hrcvxi3bbnifvl2pavg9xf0rmaajfnp4q77brzdxqp")))

(define-public crate-bme68x-rust-0.1.2 (c (n "bme68x-rust") (v "0.1.2") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19n5rj4y18h5f5arnwn6gvln5cqayg71gl3xam4xb23y628d9047")))

(define-public crate-bme68x-rust-0.1.3 (c (n "bme68x-rust") (v "0.1.3") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 2)))) (h "1ai7g8wrqbkpabpv952rlqz4vj8778lxnwlnlhljxpj6rcbafp45")))

