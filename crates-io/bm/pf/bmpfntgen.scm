(define-module (crates-io bm pf bmpfntgen) #:use-module (crates-io))

(define-public crate-bmpfntgen-0.1.0 (c (n "bmpfntgen") (v "0.1.0") (d (list (d (n "image") (r "^0.22.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1p0cahyn7wi7rbfr87vvxn43h8qafgc8gq7djr2kmmgwh0gvz7sf")))

