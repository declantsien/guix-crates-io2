(define-module (crates-io bm os bmos_client) #:use-module (crates-io))

(define-public crate-bmos_client-1.0.0 (c (n "bmos_client") (v "1.0.0") (d (list (d (n "json_minimal") (r "^0.1.3") (d #t) (k 0)))) (h "00fcj4w81ywbn2x4amlg1ha9kipdfr3lwd3mjzdzccid938xdydx")))

(define-public crate-bmos_client-1.0.1 (c (n "bmos_client") (v "1.0.1") (d (list (d (n "json_minimal") (r "^0.1.3") (d #t) (k 0)))) (h "0mi0z2jib9ycbs4cjw41bnimqla2l2k4i93s492p1w0csfs0awny")))

