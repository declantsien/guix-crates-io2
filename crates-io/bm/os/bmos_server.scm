(define-module (crates-io bm os bmos_server) #:use-module (crates-io))

(define-public crate-bmos_server-1.0.0 (c (n "bmos_server") (v "1.0.0") (d (list (d (n "openweathermap") (r "^0.2.4") (d #t) (k 0)) (d (n "random-number") (r "^0.1.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.5") (f (quote ("image" "ttf"))) (k 0)) (d (n "soloud") (r "^0.4.0") (f (quote ("openal"))) (k 0)))) (h "1895a9gjvab2x5ma962d4r5gljii96cdb39gxsbrzz7wvq5kb3y4")))

(define-public crate-bmos_server-1.0.1 (c (n "bmos_server") (v "1.0.1") (d (list (d (n "openweathermap") (r "^0.2.4") (d #t) (k 0)) (d (n "random-number") (r "^0.1.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.5") (f (quote ("image" "ttf"))) (k 0)) (d (n "soloud") (r "^0.4.0") (f (quote ("openal"))) (k 0)))) (h "0jx13q1j11440qfb6x98b1va4alj9lz60nl2ccgalvpgknlxbr92")))

(define-public crate-bmos_server-1.0.2 (c (n "bmos_server") (v "1.0.2") (d (list (d (n "openweathermap") (r "^0.2.4") (d #t) (k 0)) (d (n "random-number") (r "^0.1.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.5") (f (quote ("image" "ttf"))) (k 0)) (d (n "soloud") (r "^0.4.0") (f (quote ("openal"))) (k 0)))) (h "0j8fmbzkdb3flwvhhdxgkjx2hd0d0626bnwjhb3hpbjddyb5yc3w")))

(define-public crate-bmos_server-1.0.3 (c (n "bmos_server") (v "1.0.3") (d (list (d (n "openweathermap") (r "^0.2.4") (d #t) (k 0)) (d (n "random-number") (r "^0.1.6") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.5") (f (quote ("image" "ttf"))) (k 0)) (d (n "soloud") (r "^0.4.0") (f (quote ("openal"))) (k 0)))) (h "1r1rl51fza0d1dqzhsh9b6kj0baxaws5vpfdz0r5xyvx5k00lky3")))

