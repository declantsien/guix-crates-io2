(define-module (crates-io bm -s bm-ssz) #:use-module (crates-io))

(define-public crate-bm-ssz-0.2.0 (c (n "bm-ssz") (v "0.2.0") (d (list (d (n "bm") (r "^0.5") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)) (d (n "primitive-types") (r "^0.4") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 2)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "0hi5k9b0dfxl3js43lpsxqjsl8r65zibzsm6cqfssiynjvyjy1zw")))

