(define-module (crates-io k- lo k-lock) #:use-module (crates-io))

(define-public crate-k-lock-0.1.1 (c (n "k-lock") (v "0.1.1") (d (list (d (n "atomic-wait") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (d #t) (k 2)))) (h "0rdzphrb5mi1c2jg9g5vsd0bimzchsbxwz0kj1pqmwvr90ap86ky")))

(define-public crate-k-lock-0.1.2 (c (n "k-lock") (v "0.1.2") (d (list (d (n "atomic-wait") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (d #t) (k 2)) (d (n "readme-rustdocifier") (r "^0.1") (d #t) (k 1)))) (h "1c72ixqsmwssg80jxnd08009njg3z1dbyr39mlcrhs4hfbj8l1fn")))

(define-public crate-k-lock-0.1.3 (c (n "k-lock") (v "0.1.3") (d (list (d (n "atomic-wait") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (d #t) (k 2)) (d (n "readme-rustdocifier") (r "^0.1") (d #t) (k 1)))) (h "094s6ip4xhykbwg8qizihfd8z0sfcqzfrixzd19pxi4kji2y9m2p")))

(define-public crate-k-lock-0.1.4 (c (n "k-lock") (v "0.1.4") (d (list (d (n "atomic-wait") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (d #t) (k 2)) (d (n "readme-rustdocifier") (r "^0.1") (d #t) (k 1)))) (h "0yhr4hhy1j46110dgj4m1gg6g4gmzdkq39wi7dby8hsw4b5vd1bq")))

(define-public crate-k-lock-0.1.5 (c (n "k-lock") (v "0.1.5") (d (list (d (n "atomic-wait") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (d #t) (k 2)) (d (n "readme-rustdocifier") (r "^0.1") (d #t) (k 1)))) (h "0y168lxkp6kqhclplxk0x2af2zz167jgfw4c6xpqf4f55g5h67l6")))

(define-public crate-k-lock-0.2.0 (c (n "k-lock") (v "0.2.0") (d (list (d (n "atomic-wait") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (d #t) (k 2)) (d (n "readme-rustdocifier") (r "^0.1") (d #t) (k 1)))) (h "110l4y7j1k6g3nz59hbfaq79q9fmfzs5667iylc6yxqm4a4m5lbk")))

(define-public crate-k-lock-0.2.1 (c (n "k-lock") (v "0.2.1") (d (list (d (n "atomic-wait") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (d #t) (k 2)) (d (n "readme-rustdocifier") (r "^0.1") (d #t) (k 1)))) (h "1cmir246ajzcxccgq4hb8q1aj03d1kg10i166g0f4kmkmwm6qnxf")))

(define-public crate-k-lock-0.2.2 (c (n "k-lock") (v "0.2.2") (d (list (d (n "atomic-wait") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (d #t) (k 2)) (d (n "readme-rustdocifier") (r "^0.1") (d #t) (k 1)))) (h "1ji1ni876wm6mczqrp5glmvjj71irjcc00lx6vw4jkrgv8la22am")))

