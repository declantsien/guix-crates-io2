(define-module (crates-io k- li k-lighter) #:use-module (crates-io))

(define-public crate-k-lighter-1.0.0 (c (n "k-lighter") (v "1.0.0") (d (list (d (n "clap") (r "^4.3.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "configparser") (r "^3.0.2") (d #t) (k 0)))) (h "0p5g96h38772mgp2qv627nrjnsw1w64dzf1qfr45zwivpncjs27p")))

(define-public crate-k-lighter-1.0.1 (c (n "k-lighter") (v "1.0.1") (d (list (d (n "clap") (r "^4.3.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "configparser") (r "^3.0.2") (d #t) (k 0)))) (h "1ps4lcdizvcx2qj5pl2xl10hffh1lfav0dz1vpd5sn10qdlnbsbb")))

(define-public crate-k-lighter-1.0.2 (c (n "k-lighter") (v "1.0.2") (d (list (d (n "clap") (r "^4.3.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "configparser") (r "^3.0.2") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "0l5757xasj1g4y8j3j6c7fr8lxpdqf0jky6vnp2hj9np1xnilp1n")))

(define-public crate-k-lighter-1.0.3 (c (n "k-lighter") (v "1.0.3") (d (list (d (n "clap") (r "^4.3.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "configparser") (r "^3.0.2") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "0b6bnqmv2md9ndcz1nzg055846rsvf5srrvzvj1z48k83v2i4sp6")))

(define-public crate-k-lighter-1.0.4 (c (n "k-lighter") (v "1.0.4") (d (list (d (n "clap") (r "^4.3.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "configparser") (r "^3.0.2") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "09wc8f98dpw2qxdn7rwxnl8w2k0hy1lllf81id3z274m3xsijqyb")))

