(define-module (crates-io k- on k-onfig-derive) #:use-module (crates-io))

(define-public crate-k-onfig-derive-0.0.1 (c (n "k-onfig-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (d #t) (k 0)))) (h "15z3cypc1vdcagydkp72szywdg80xx70c8vai7ixc1r0bgigv6c4")))

