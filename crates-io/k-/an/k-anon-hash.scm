(define-module (crates-io k- an k-anon-hash) #:use-module (crates-io))

(define-public crate-k-anon-hash-0.1.0 (c (n "k-anon-hash") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "10mmbsf350fb7hdlfw7xm53ncv6nf2b11vcy22knbs0f6b961pvr")))

(define-public crate-k-anon-hash-0.1.1 (c (n "k-anon-hash") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "0hppq25vkly5w6r6bmjqmrn5pb585fikk61zpv9923ibxscidqdi")))

(define-public crate-k-anon-hash-0.1.2 (c (n "k-anon-hash") (v "0.1.2") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "12v86ddnpgwf08chk623ipigg7mjgxqwqjdcd7wly4rwbb3jvxsf")))

(define-public crate-k-anon-hash-0.1.3 (c (n "k-anon-hash") (v "0.1.3") (d (list (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "1n05yf0wk1bvm3n4zjapl5jrc0gfdqj1glwj6xff9q0bjnmx15gn")))

