(define-module (crates-io cs vd csvdimreduce) #:use-module (crates-io))

(define-public crate-csvdimreduce-0.1.0 (c (n "csvdimreduce") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.74") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "interpolation") (r "^0.2.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "number_range") (r "^0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "trimothy") (r "^0.1.8") (d #t) (k 0)) (d (n "xflags") (r "^0.3.1") (d #t) (k 0)))) (h "1svwlp9l3kzl7d1fwh0cajr8w8026xvbmsz2razf6k2gsn9a2j74")))

