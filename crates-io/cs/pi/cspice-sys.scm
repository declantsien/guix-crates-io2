(define-module (crates-io cs pi cspice-sys) #:use-module (crates-io))

(define-public crate-cspice-sys-0.0.1 (c (n "cspice-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.56.0") (o #t) (d #t) (k 1)))) (h "0mjisl42y0r3w6xirngph0k51xs35cgjx2kizk3adrz8k9za5cbd") (f (quote (("generate" "bindgen")))) (l "cspice")))

(define-public crate-cspice-sys-1.0.0 (c (n "cspice-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "1lcdzfil75wg6kxpm8vaw22p4gapn9zqld6mbp6pa368yh9iwhxz")))

(define-public crate-cspice-sys-1.0.1 (c (n "cspice-sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "0hd9zp1mqa6xwsdrmjmvdf8hwdg7shx6wiismqsv33fclbvv37hf") (y #t)))

(define-public crate-cspice-sys-1.0.2 (c (n "cspice-sys") (v "1.0.2") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "06fz98vk50mqd5c19p71jsfm2mfk0nd14m9n7rzh30i8kz30k0iq") (y #t)))

(define-public crate-cspice-sys-1.0.3 (c (n "cspice-sys") (v "1.0.3") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "1181fq2zi7d964ks2cigh4hsigy9ap6s8hfza9cgzd6q7xrc70wp")))

(define-public crate-cspice-sys-1.0.4 (c (n "cspice-sys") (v "1.0.4") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking"))) (o #t) (d #t) (k 1)))) (h "01127lljyj935knxkd7p726cl58kfkq6ki9ic3xmwnp5wgqqlqgq") (s 2) (e (quote (("downloadcspice" "dep:reqwest"))))))

