(define-module (crates-io cs pi cspice) #:use-module (crates-io))

(define-public crate-cspice-0.0.1 (c (n "cspice") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "cspice-sys") (r "^1.0.3") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_plain") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1ziyfcdwa4f9sas79v57g1pf15iwx11hs6ymc8kdhx4x8hyrxycl")))

(define-public crate-cspice-0.1.0 (c (n "cspice") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (o #t) (d #t) (k 0)) (d (n "cspice-sys") (r "^1.0.4") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_plain") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0q8akc8a277xcdmnwz9n7cpn5h7iv0shc4z27991z398msjbxx85")))

