(define-module (crates-io cs vs csvsc) #:use-module (crates-io))

(define-public crate-csvsc-0.1.0 (c (n "csvsc") (v "0.1.0") (d (list (d (n "csv") (r "^1.0.5") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)))) (h "0shah7v54l1pi9rfxz7j4rnz0l836w10968apajy0nvwfk8ldy75")))

(define-public crate-csvsc-1.0.0 (c (n "csvsc") (v "1.0.0") (d (list (d (n "csv") (r "^1.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)))) (h "1bghpka9924g7n5b19nnfmxkfylpqgs6vy6rg51zy3sqfmk6xmrq")))

(define-public crate-csvsc-1.1.0 (c (n "csvsc") (v "1.1.0") (d (list (d (n "csv") (r "^1.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)))) (h "03ifd0mrmg72b5rckl5pma8in32p4ngziss2328nki842n1dmxp3")))

(define-public crate-csvsc-1.1.1 (c (n "csvsc") (v "1.1.1") (d (list (d (n "csv") (r "^1.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)))) (h "1rbrclb0kakr3m7nybl2chp1njiih9q8ph8ik0wv0mv29m1sl3zm")))

(define-public crate-csvsc-1.2.0 (c (n "csvsc") (v "1.2.0") (d (list (d (n "csv") (r "^1.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)))) (h "0q1knzjbbfvd93rndwwrcpwlvbqprqsfwcdia1nal15wwaj04fgn")))

(define-public crate-csvsc-1.3.0 (c (n "csvsc") (v "1.3.0") (d (list (d (n "csv") (r "^1.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)))) (h "1c6j833mp2knyf415gndq1w7c6fx1gwr6cy6pkb0017kx6pqbrs6")))

(define-public crate-csvsc-1.4.0 (c (n "csvsc") (v "1.4.0") (d (list (d (n "csv") (r "^1.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)))) (h "11lmqngry0v228bhm6fsw4rlhkafmbnwal8sxn05r7cqmz0bm530")))

(define-public crate-csvsc-2.0.0 (c (n "csvsc") (v "2.0.0") (d (list (d (n "csv") (r "^1.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)))) (h "0mipvs9qq2c0cb0w9dszdz0200mzl3261bkby761vjg76sd2djv9")))

(define-public crate-csvsc-2.0.1 (c (n "csvsc") (v "2.0.1") (d (list (d (n "csv") (r "^1.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)))) (h "0p2qfvvifmpk93s9ql95j25mdnv8a37zpc25ixw1g336lfg3fra1")))

(define-public crate-csvsc-2.1.0 (c (n "csvsc") (v "2.1.0") (d (list (d (n "csv") (r "^1.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)))) (h "0g9jximv8lyy064kawjca6yvh9gxn3q35l9n32fw48xi3avfbsb1")))

(define-public crate-csvsc-2.1.1 (c (n "csvsc") (v "2.1.1") (d (list (d (n "csv") (r "^1.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)))) (h "01yi3zhyip1p6ajy8147vngqwlny0s05bjak2q5ap12snlhxp2c0")))

(define-public crate-csvsc-2.2.0 (c (n "csvsc") (v "2.2.0") (d (list (d (n "csv") (r "^1.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)))) (h "0w61m2ncyd488kxpw5n5i0kkasqxziy6a6qapkdkgc6frizsjlr1")))

(define-public crate-csvsc-2.2.1 (c (n "csvsc") (v "2.2.1") (d (list (d (n "csv") (r "^1.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)))) (h "0slh81avzggxd1hxxlagp6171vqn5lv2sm3bipzb52bfs27g99gd")))

