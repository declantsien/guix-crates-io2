(define-module (crates-io cs vs csvstore) #:use-module (crates-io))

(define-public crate-csvstore-0.1.0 (c (n "csvstore") (v "0.1.0") (d (list (d (n "csv") (r "^0.15") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "walkdir") (r "^1") (d #t) (k 0)))) (h "12c416anxc34bg637yliyxqvprqh4i923glvcv2g267rcrnfr8j1")))

