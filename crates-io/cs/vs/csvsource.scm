(define-module (crates-io cs vs csvsource) #:use-module (crates-io))

(define-public crate-csvsource-0.6.0 (c (n "csvsource") (v "0.6.0") (d (list (d (n "clap") (r "^3.0.7") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2.1") (d #t) (k 0)))) (h "0dmbmypiy3lzbnj3bkqri3d262mwr2xamlcnk6y8b12819kz818b")))

(define-public crate-csvsource-0.7.0 (c (n "csvsource") (v "0.7.0") (d (list (d (n "clap") (r "^3.0.7") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tinytemplate") (r "^1.2.1") (d #t) (k 0)))) (h "0v45q23qaap74qgkych6i6xxybba57f7m93ldv733cmp869bm8vp")))

