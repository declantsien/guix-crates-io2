(define-module (crates-io cs vs csvstream) #:use-module (crates-io))

(define-public crate-csvstream-0.1.0 (c (n "csvstream") (v "0.1.0") (h "134fzbf770nqi0cxcwlb1ywfbkf0841salqrpl37hj8z97xl4zpa")))

(define-public crate-csvstream-0.2.0 (c (n "csvstream") (v "0.2.0") (h "0bkcizc1aw52jcpjw5mfz383yyfry36h72nlalz853fa892z3p1w")))

