(define-module (crates-io cs i_ csi_parser) #:use-module (crates-io))

(define-public crate-csi_parser-0.1.0 (c (n "csi_parser") (v "0.1.0") (h "11j4dv9whg5clsivvf0bx0bi0mkg5cp5m7y4n8b7x58l7w56xsld") (y #t)))

(define-public crate-csi_parser-0.1.1 (c (n "csi_parser") (v "0.1.1") (h "0qpssnchiib8mq1w7w0vqvmn5hfsf76qyl1cr74hdhf3jpm784n6") (f (quote (("std") ("no_std") ("default" "std")))) (y #t)))

(define-public crate-csi_parser-0.1.2 (c (n "csi_parser") (v "0.1.2") (h "07i876arfirb8dcmz4rlmp8xj572i17zd952hlslc1l2qa5lqmal") (f (quote (("std") ("no_std") ("default" "std")))) (y #t)))

(define-public crate-csi_parser-0.1.3 (c (n "csi_parser") (v "0.1.3") (h "067ahm8c1mr2ji24kv29fxnhmxzpand31j11z79bkq555mfm7z2c") (f (quote (("std") ("no_std") ("default" "std"))))))

