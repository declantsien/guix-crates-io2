(define-module (crates-io cs ql csql) #:use-module (crates-io))

(define-public crate-csql-1.0.0 (c (n "csql") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "eframe") (r "^0.21.0") (d #t) (k 0)) (d (n "egui") (r "^0.21.0") (d #t) (k 0)) (d (n "egui_extras") (r "^0.21.0") (d #t) (k 0)) (d (n "grid") (r "^0.10.0") (d #t) (k 0)) (d (n "if_chain") (r "^1.0.2") (d #t) (k 0)) (d (n "rfd") (r "^0.11.1") (d #t) (k 0)) (d (n "sqlx") (r "^0.6.2") (f (quote ("runtime-tokio-native-tls" "mysql"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jlapx8yrjpx9jg7yswfv4bmpj181prrymwsn17apxa3s169dqwc")))

