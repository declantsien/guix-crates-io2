(define-module (crates-io cs de csdeps) #:use-module (crates-io))

(define-public crate-csdeps-0.1.1 (c (n "csdeps") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0nza6bcimpjz53iz94qd2gjz4nv8i5h208k275x7md5k7llgg6m9")))

(define-public crate-csdeps-0.1.2 (c (n "csdeps") (v "0.1.2") (d (list (d (n "indicatif") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0wbh1p53fifbnxrk5gqjygbjp086c3yrq2s448mlcn2ibpp0732x")))

