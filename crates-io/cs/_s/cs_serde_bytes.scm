(define-module (crates-io cs _s cs_serde_bytes) #:use-module (crates-io))

(define-public crate-cs_serde_bytes-0.12.0 (c (n "cs_serde_bytes") (v "0.12.0") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0xcnwbg5cag5x8i5qhs5nvg4zj8r4si322691kcsa18hjcg5xjpb") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-cs_serde_bytes-0.12.1 (c (n "cs_serde_bytes") (v "0.12.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0v0cibasxhw1kcl8bsjqsfsi9hik4al9yvw4xnn0wdbi4s0b9ddp") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-cs_serde_bytes-0.12.2 (c (n "cs_serde_bytes") (v "0.12.2") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "148xbdipq94m02455r11bvq7anh653f6ic160mai90plmgfp7ijz") (f (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

