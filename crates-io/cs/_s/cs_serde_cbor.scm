(define-module (crates-io cs _s cs_serde_cbor) #:use-module (crates-io))

(define-public crate-cs_serde_cbor-0.12.0 (c (n "cs_serde_cbor") (v "0.12.0") (d (list (d (n "half") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.14") (k 0)) (d (n "serde_derive") (r "^1.0.14") (k 2)))) (h "1ia2wmn3njlddy99bfisp57y7qbjn2j958j34a9v7gb6am17fyyb") (f (quote (("unsealed_read_write") ("tags") ("std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

