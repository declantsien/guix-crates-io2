(define-module (crates-io cs sp cssparser-macros) #:use-module (crates-io))

(define-public crate-cssparser-macros-0.1.0 (c (n "cssparser-macros") (v "0.1.0") (d (list (d (n "phf_codegen") (r "^0.7") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "1s6ya3bs6zpypx7h26alpk3p8w6xlz57zyi4hiy7fl0ayi918pm8")))

(define-public crate-cssparser-macros-0.2.0 (c (n "cssparser-macros") (v "0.2.0") (d (list (d (n "phf_codegen") (r "^0.7") (d #t) (k 0)) (d (n "procedural-masquerade") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.14") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (f (quote ("full"))) (d #t) (k 0)))) (h "15bb8kj9cqvwzw9ws11y5px410iclq04w86vq0iyig5xw1fl3w5q")))

(define-public crate-cssparser-macros-0.3.0 (c (n "cssparser-macros") (v "0.3.0") (d (list (d (n "phf_codegen") (r "^0.7") (d #t) (k 0)) (d (n "procedural-masquerade") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.14") (d #t) (k 0)) (d (n "syn") (r "^0.11.8") (f (quote ("full"))) (d #t) (k 0)))) (h "1pzn69q1km1nd84fhkjvbwyfnycc0a94406xx9sm5fsjmz2dx6h7")))

(define-public crate-cssparser-macros-0.3.2 (c (n "cssparser-macros") (v "0.3.2") (d (list (d (n "phf_codegen") (r "^0.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.2") (d #t) (k 0)) (d (n "procedural-masquerade") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1l6lsiybdvd26x2j3wzhyqfghw1dbgrbflg3dq10xjnj5vw19rww")))

(define-public crate-cssparser-macros-0.3.3 (c (n "cssparser-macros") (v "0.3.3") (d (list (d (n "phf_codegen") (r "^0.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.3") (d #t) (k 0)) (d (n "procedural-masquerade") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01bqfwad183mqh9k70yks9fakcnxbfgh2qpdd6sxxgwdw4x3i9gk")))

(define-public crate-cssparser-macros-0.3.4 (c (n "cssparser-macros") (v "0.3.4") (d (list (d (n "phf_codegen") (r "^0.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "procedural-masquerade") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11fpicrjrx8cdrpg3xl16h4fxj08bj3rd0q270phhjhqch6lvdkj")))

(define-public crate-cssparser-macros-0.3.5 (c (n "cssparser-macros") (v "0.3.5") (d (list (d (n "phf_codegen") (r "^0.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "procedural-masquerade") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.12") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pi74izzlg26wgzfp7vl3yl3xkl4xgykfsx3qsddngwqkcnkhvmi")))

(define-public crate-cssparser-macros-0.3.6 (c (n "cssparser-macros") (v "0.3.6") (d (list (d (n "phf_codegen") (r "^0.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "procedural-masquerade") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1vqyc5xm8a4va92vs1nn0cc46c930l2n21gccijnc5y7hx7cicav")))

(define-public crate-cssparser-macros-0.4.0 (c (n "cssparser-macros") (v "0.4.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0v0p05ccdjy9m1dpisv2z3d28g6sg9vn3dxzzb0n9dgn60i1yrqi")))

(define-public crate-cssparser-macros-0.5.0 (c (n "cssparser-macros") (v "0.5.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13dnsay1rdgrr08j8glvr1322dwn2cbxr8p18jgqnw09c43vchkb")))

(define-public crate-cssparser-macros-0.6.0 (c (n "cssparser-macros") (v "0.6.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vp13g4blyjvhg3j4r9b7vrwhnfi1y2fmhv8hxgficpjazg7bbnz")))

(define-public crate-cssparser-macros-0.6.1 (c (n "cssparser-macros") (v "0.6.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cfkzj60avrnskdmaf7f8zw6pp3di4ylplk455zrzaf19ax8id8k")))

