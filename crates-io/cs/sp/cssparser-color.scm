(define-module (crates-io cs sp cssparser-color) #:use-module (crates-io))

(define-public crate-cssparser-color-0.1.0 (c (n "cssparser-color") (v "0.1.0") (d (list (d (n "cssparser") (r "^0.33") (d #t) (k 0)) (d (n "difference") (r "^2.0") (d #t) (k 2)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "17qcjsrph1ywcdsx1ipqgmzaas4dbbir5djjmzbqjnfqc6d0jv2m")))

