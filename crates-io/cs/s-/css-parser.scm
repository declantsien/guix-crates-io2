(define-module (crates-io cs s- css-parser) #:use-module (crates-io))

(define-public crate-css-parser-0.1.0 (c (n "css-parser") (v "0.1.0") (d (list (d (n "console") (r "^0.14.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "tokenizer-lib") (r "^0.4.1") (d #t) (k 0)))) (h "1wjsk459lm1niw4v0d1rszrzjbkba3cscsh15rnyjylra2nxq5hs")))

(define-public crate-css-parser-0.2.0 (c (n "css-parser") (v "0.2.0") (d (list (d (n "argh") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11.1") (o #t) (d #t) (k 0)) (d (n "console") (r "^0.14.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "source-map") (r "^0.5.0") (d #t) (k 0)) (d (n "tokenizer-lib") (r "^1.2.0") (d #t) (k 0)))) (h "1g3kr46wipia24qrdx21128ncqglf48w1i0rsa69wkw7ma169114") (f (quote (("cli" "argh" "base64" "codespan-reporting"))))))

