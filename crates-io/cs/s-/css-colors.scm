(define-module (crates-io cs s- css-colors) #:use-module (crates-io))

(define-public crate-css-colors-0.0.1 (c (n "css-colors") (v "0.0.1") (h "1mvwh2hqmk1z7qw48mln0pfmfiwrrfanj3azn1y4wwh76cgb0v6k")))

(define-public crate-css-colors-0.0.2 (c (n "css-colors") (v "0.0.2") (h "1bil83fnbg2549c374gfvxzzrif4zv2gbd7zg6bcqklxahlzjs67")))

(define-public crate-css-colors-0.1.0 (c (n "css-colors") (v "0.1.0") (h "197nnfbasrka5s7fz7nvbqz1wc3nkiyarvjvhsrk511qmj2dy8pi")))

(define-public crate-css-colors-1.0.0 (c (n "css-colors") (v "1.0.0") (h "0wmm7syp6281zw1nvmq57paicglpd8qa7f9fcfx5brf2xv3lyjz2")))

(define-public crate-css-colors-1.0.1 (c (n "css-colors") (v "1.0.1") (h "0dljfdw4p54drjy9a5m6h5qnvz8lkdllxfkln0vk9wh8azybphi2")))

