(define-module (crates-io cs s- css-color) #:use-module (crates-io))

(define-public crate-css-color-0.1.0 (c (n "css-color") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "lexical") (r "^2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0hriwd5rxva4xkdgqfv3frlil3rm29nrrjj3v0llqq4bz5s7zq39") (f (quote (("bench"))))))

(define-public crate-css-color-0.1.1 (c (n "css-color") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "lexical") (r "^2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "10m2mig2chd491sm0a82jzs7gc3sxklcabbf9kyjc078yi6bqm52") (f (quote (("bench"))))))

(define-public crate-css-color-0.2.0 (c (n "css-color") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0z335lazyiwvi30iscb5fhvcckmii0616rwbf2xcsly39bxvsq7h") (f (quote (("bench"))))))

(define-public crate-css-color-0.2.1 (c (n "css-color") (v "0.2.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1isi8g3hcp9k73dk34xknwhdj9a86f75fiq36q5qjvxvaqjnx7qv") (f (quote (("bench"))))))

(define-public crate-css-color-0.2.2 (c (n "css-color") (v "0.2.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0p16i6a93grxa33prf6iivk43pnxvg67kh5h984nm4p9q6i23fzc") (f (quote (("bench"))))))

(define-public crate-css-color-0.2.3 (c (n "css-color") (v "0.2.3") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1vabyj6hc7cdwz6pan6mwv5kvmnvf28q834zkbq8csndw4nknmb6") (f (quote (("bench"))))))

(define-public crate-css-color-0.2.4 (c (n "css-color") (v "0.2.4") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "01ndh1k250xb607v6myqy143i90dn7ca36cjfc5y06q6nfmdc3av") (f (quote (("bench"))))))

(define-public crate-css-color-0.2.5 (c (n "css-color") (v "0.2.5") (d (list (d (n "once_cell") (r "^1.17.0") (d #t) (k 2)) (d (n "regex") (r "^1.7.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1d3yyy7037zprmlj55sn6vf00dg0vlnxl65q7hd16mn84iacc0fi") (f (quote (("bench"))))))

(define-public crate-css-color-0.2.6 (c (n "css-color") (v "0.2.6") (d (list (d (n "once_cell") (r "^1.17.0") (d #t) (k 2)) (d (n "regex") (r "^1.7.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1w9d1gdjg45057vj6w0gwz8x8x36rz371j4fvkgj4v85qrzvd50b") (f (quote (("bench"))))))

(define-public crate-css-color-0.2.7 (c (n "css-color") (v "0.2.7") (d (list (d (n "once_cell") (r "^1.17.0") (d #t) (k 2)) (d (n "regex") (r "^1.7.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "15sh3ch73s780wz2b8kca5knldvf2rwwxr3s1p2lzp1s5zalcfn4") (f (quote (("bench"))))))

(define-public crate-css-color-0.2.8 (c (n "css-color") (v "0.2.8") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0fjli3irfaqw7a8a85snrmmz55ky3zqcvikp3m8cwy7x37kymaj2") (f (quote (("bench"))))))

