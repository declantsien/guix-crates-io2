(define-module (crates-io cs s- css-rs-macro) #:use-module (crates-io))

(define-public crate-css-rs-macro-0.1.0 (c (n "css-rs-macro") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)))) (h "0wjq4z7s1303rvi86gf97kzpm237j744q3hbfi976ydydrzmkmq0")))

