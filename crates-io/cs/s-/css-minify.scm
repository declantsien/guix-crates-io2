(define-module (crates-io cs s- css-minify) #:use-module (crates-io))

(define-public crate-css-minify-0.1.0 (c (n "css-minify") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)))) (h "0ghqr35jy1f8a1h3y73l6cwmpxxizd4r6rzg8fkwkgz1xzdz7fvq")))

(define-public crate-css-minify-0.1.1 (c (n "css-minify") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)))) (h "02wkrgspf9jvfq06gd7svj94bdifxhr495yhfx0nnxyrvk783apd")))

(define-public crate-css-minify-0.2.0 (c (n "css-minify") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)))) (h "0wy5f9rhx2p72g1vjnvr546f55bx0m7b545zjmzc7ajnma0rzk6f")))

(define-public crate-css-minify-0.2.1 (c (n "css-minify") (v "0.2.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)))) (h "09dklqdn4iv8biq7jq27xgylr187zn6m4x7ybad7ic2nj16y9f0y")))

(define-public crate-css-minify-0.2.2 (c (n "css-minify") (v "0.2.2") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)))) (h "1ay9625mbdndn4zsvllfjsbdhcd9acgh5wwm65mzk6kw7dg1hav9")))

(define-public crate-css-minify-0.3.0 (c (n "css-minify") (v "0.3.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "02fimyxs3z8ykwy1igkmhvjrf9h7mxk533pva692ddlr9pyj6mki")))

(define-public crate-css-minify-0.3.1 (c (n "css-minify") (v "0.3.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1gaj0nb8v21ldcg6xwl5xp1imq7v3cja649v122s5m7q34nnwk47")))

