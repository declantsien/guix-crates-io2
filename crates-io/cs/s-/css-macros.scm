(define-module (crates-io cs s- css-macros) #:use-module (crates-io))

(define-public crate-css-macros-0.1.0 (c (n "css-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0pq9awhcypa0h5gjqxjywxxbrcid5mmx5ad461jbj2pnhbpdzdry")))

