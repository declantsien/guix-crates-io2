(define-module (crates-io cs s- css-image) #:use-module (crates-io))

(define-public crate-css-image-0.1.0 (c (n "css-image") (v "0.1.0") (d (list (d (n "cairo-rs") (r "^0.19.2") (f (quote ("png"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "0b2gcilhfqq1rgszhjh787lz1cw35zxxz53iyr0z3wwx31265iww")))

(define-public crate-css-image-0.1.1 (c (n "css-image") (v "0.1.1") (d (list (d (n "cairo-rs") (r "^0.19.2") (f (quote ("png"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "0sp0rdrp5qr0506a88xxp5qc3k4sg51m74yv6pwbivq0pbgcmrnp")))

(define-public crate-css-image-0.1.2 (c (n "css-image") (v "0.1.2") (d (list (d (n "cairo-rs") (r "^0.19.2") (f (quote ("png"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "1lpc1kfaalvly325a345lnxpzb1f2p6ks8h5fxkaybk8bb9vmf9n")))

(define-public crate-css-image-0.1.3 (c (n "css-image") (v "0.1.3") (d (list (d (n "cairo-rs") (r "^0.19.2") (f (quote ("png"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "074q2ria8jkl4npdxpb2jwkmc2as5ykmf8vagrdjdfbcjdq7gzm1")))

(define-public crate-css-image-0.2.0 (c (n "css-image") (v "0.2.0") (d (list (d (n "cairo-rs") (r "^0.19.2") (f (quote ("png"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "0n1hq83xym5znilqkrf2ccln9099vahh3jmqnbwvn212hp31gp0d")))

(define-public crate-css-image-0.3.0 (c (n "css-image") (v "0.3.0") (d (list (d (n "cairo-rs") (r "^0.19.2") (f (quote ("png"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "1vz6i3v0d37rr0a1kls6xzb8890ma5jx3axf10zln660bggx1mvm")))

(define-public crate-css-image-0.4.0 (c (n "css-image") (v "0.4.0") (d (list (d (n "cairo-rs") (r "^0.19.2") (f (quote ("png"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "0hqc85hnnxjww6slg2xnyj6gdn9r9i1gknmpgvzymavljqs03hd1")))

(define-public crate-css-image-0.4.1 (c (n "css-image") (v "0.4.1") (d (list (d (n "cairo-rs") (r "^0.19.2") (f (quote ("png"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "0h13skrynbhdrb3r8y3xgh6qsnicgyk0qyqydrjd29mm0qzhw5wk")))

(define-public crate-css-image-0.4.2 (c (n "css-image") (v "0.4.2") (d (list (d (n "cairo-rs") (r "^0.19.2") (f (quote ("png"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "0si400nm5fklh6pi3jbcljwwqcp2iw2wjy393lmr7siwrddkzshs")))

(define-public crate-css-image-0.4.3 (c (n "css-image") (v "0.4.3") (d (list (d (n "cairo-rs") (r "^0.19.2") (f (quote ("png"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "0jck8fn2ki7rwg4nsw13s1bmgb5lkz9ssc25wpgccxnj7rlfkxpd")))

