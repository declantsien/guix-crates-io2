(define-module (crates-io cs s- css-style) #:use-module (crates-io))

(define-public crate-css-style-0.8.0 (c (n "css-style") (v "0.8.0") (d (list (d (n "derive_more") (r "^0.99.13") (d #t) (k 0)) (d (n "derive_rich") (r "^0.4.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "palette") (r "^0.5.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "0d4qvxh1ppn9r7c010nq3abhvh2lsavpniw7y80a72n0270fwgc2")))

(define-public crate-css-style-0.9.0 (c (n "css-style") (v "0.9.0") (d (list (d (n "derive_more") (r "^0.99.13") (d #t) (k 0)) (d (n "derive_rich") (r "^0.4.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "palette") (r "^0.5.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)))) (h "1wy1jm129smshil2d7dgmg5aqlj1b3nq5kayi52cqcbdsd6695bf")))

(define-public crate-css-style-0.10.0 (c (n "css-style") (v "0.10.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "derive_rich") (r "^0.4.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "palette") (r "^0.6.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)))) (h "17kfv5d6prdsgzfw6ybr4n2gjkg830y5xr91ngfhg5vfwbqja55s")))

(define-public crate-css-style-0.11.0 (c (n "css-style") (v "0.11.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "derive_rich") (r "^0.4.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "palette") (r "^0.6.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)))) (h "1v3jabrrzwwsx6lp7r9pn6ls67zf5h0yhl0cfqslm8fx1jb8sq7v")))

(define-public crate-css-style-0.11.1 (c (n "css-style") (v "0.11.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "derive_rich") (r "^0.4.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "palette") (r "^0.6.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)))) (h "1xw89wz1bl9wkp48ifihmlpzm7dp82y5iwlnrbdmbb4mi8dkcnnk")))

(define-public crate-css-style-0.12.1 (c (n "css-style") (v "0.12.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "derive_rich") (r "^0.4.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "palette") (r "^0.6.0") (f (quote ("named" "named_from_str"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)))) (h "1jm1yxhx3f72kfxp4pj87qm6w7yyn4gj33wqmq6yjcpgvh3avw3i")))

(define-public crate-css-style-0.13.0 (c (n "css-style") (v "0.13.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "derive_rich") (r "^0.4.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "palette") (r "^0.6.0") (f (quote ("named" "named_from_str"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)))) (h "0d5h7ybd0fi7dnbd8w95bqzcx207g938lsbzqf6qiyaj0699jv06")))

(define-public crate-css-style-0.13.1 (c (n "css-style") (v "0.13.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "derive_rich") (r "^0.4.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "palette") (r "^0.6.0") (f (quote ("named" "named_from_str"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)))) (h "0znnz21fcbr9vd58d5yh3m11ydhahf83q8mqsrymbz7r9xsw6c6p")))

(define-public crate-css-style-0.14.0 (c (n "css-style") (v "0.14.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "derive_rich") (r "^0.4.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.1") (d #t) (k 0)) (d (n "palette") (r "^0.6.0") (f (quote ("named" "named_from_str"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)))) (h "1rwfxz5jm0l7j9fvmwi86c7lnn5gn8nmgawqd2yhzw0hg1cpqa1w")))

(define-public crate-css-style-0.14.1 (c (n "css-style") (v "0.14.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "derive_rich") (r "^0.4.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.1") (d #t) (k 0)) (d (n "palette") (r "^0.6.0") (f (quote ("named" "named_from_str"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.7") (d #t) (k 0)))) (h "1j6jdv4wl51626yzpcdb8r322blvi7bml1q6xfzvvwdm8wwyynzg")))

