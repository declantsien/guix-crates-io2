(define-module (crates-io cs s- css-compare) #:use-module (crates-io))

(define-public crate-css-compare-0.1.0 (c (n "css-compare") (v "0.1.0") (d (list (d (n "lightningcss") (r "^1.0.0-alpha.44") (f (quote ("grid"))) (k 0)) (d (n "similar-asserts") (r "^1.4") (d #t) (k 2)))) (h "0sv7wdz107218y1h1pg24xqzcnpxb42q988w11kk2i2w3v71a79v")))

(define-public crate-css-compare-0.1.1 (c (n "css-compare") (v "0.1.1") (d (list (d (n "lightningcss") (r "^1.0.0-alpha.46") (f (quote ("grid"))) (k 0)) (d (n "similar-asserts") (r "^1.4") (d #t) (k 2)))) (h "0jdwvn6d5a138d36xfzrvvw3xqw9xwafgy2z9h0grsq8rpg51jix")))

(define-public crate-css-compare-0.1.2 (c (n "css-compare") (v "0.1.2") (d (list (d (n "lightningcss") (r "^1.0.0-alpha.46") (f (quote ("grid"))) (k 0)) (d (n "similar-asserts") (r "^1.4") (d #t) (k 2)))) (h "19f758kfgk4qhqc461gs1gddnfk799xpx3hlc9ysi4ldd8zih865")))

