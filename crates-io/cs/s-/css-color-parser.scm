(define-module (crates-io cs s- css-color-parser) #:use-module (crates-io))

(define-public crate-css-color-parser-0.1.0 (c (n "css-color-parser") (v "0.1.0") (h "0f2riy8g5lxlcgf3c3dl18zbs27f81v1373crnc2zq4xpvqq2z53")))

(define-public crate-css-color-parser-0.1.1 (c (n "css-color-parser") (v "0.1.1") (h "0bcmkibjwjkiym9waxj87g70708ykay2nc7r8xxs4whslg9hx3qc")))

(define-public crate-css-color-parser-0.1.2 (c (n "css-color-parser") (v "0.1.2") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)))) (h "07aqswgwc5fjfzqmwyrhib45r6l8r6bbalayaxpdrrlpxzknrjww")))

