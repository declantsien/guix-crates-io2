(define-module (crates-io cs s- css-purify) #:use-module (crates-io))

(define-public crate-css-purify-0.0.0 (c (n "css-purify") (v "0.0.0") (d (list (d (n "html5ever_ext") (r "^0.21.2") (d #t) (k 0)))) (h "1mapj1b3wyxxk0zmg4bmm7cm8whs34p2d1ay6c72qarxys8ymxrm")))

(define-public crate-css-purify-0.0.1 (c (n "css-purify") (v "0.0.1") (d (list (d (n "html5ever_ext") (r "^0.21.6") (d #t) (k 0)))) (h "1x6sc12zj3ig3b7hnzdmidl2z4z9s4sgak2w8scppaplb30xy3b8")))

(define-public crate-css-purify-0.0.2 (c (n "css-purify") (v "0.0.2") (d (list (d (n "html5ever_ext") (r "^0.21.7") (d #t) (k 0)))) (h "0ia7cwrl4sbwvzg3q6r3yhim05h6b4pn3i5yr92i0p9pdhkh20i9")))

(define-public crate-css-purify-0.0.3 (c (n "css-purify") (v "0.0.3") (d (list (d (n "html5ever_ext") (r "^0.21.8") (d #t) (k 0)))) (h "1xc7b6dwxqr97x39m30v46say2w1a9zd45niplwx55yipchaafgy")))

(define-public crate-css-purify-0.0.4 (c (n "css-purify") (v "0.0.4") (d (list (d (n "html5ever_ext") (r "^0.21.9") (d #t) (k 0)))) (h "0zvyncy66nr4kd9jj98pnan798il56d9ys5qxch3zwhpb976i09l")))

(define-public crate-css-purify-0.0.5 (c (n "css-purify") (v "0.0.5") (d (list (d (n "html5ever_ext") (r "^0.21.10") (d #t) (k 0)))) (h "11r0iiflrp88hn4iwdgwj6v6gkwas05s0vhfapk3s4j3ywi3xsq6")))

(define-public crate-css-purify-0.0.6 (c (n "css-purify") (v "0.0.6") (d (list (d (n "html5ever_ext") (r "^0.21.11") (d #t) (k 0)))) (h "11xhw9dnp74ywjdlxcz752c0pnvm4p5lxwx3ydrv793b4ndcrl04")))

(define-public crate-css-purify-0.0.7 (c (n "css-purify") (v "0.0.7") (d (list (d (n "html5ever_ext") (r "^0.21.12") (d #t) (k 0)))) (h "0h22zh4fsvkhk66kj25l8qxmhvha45w52s9cggv3yr8hppax2hpp")))

(define-public crate-css-purify-0.0.8 (c (n "css-purify") (v "0.0.8") (d (list (d (n "html5ever_ext") (r "^0.21.13") (d #t) (k 0)))) (h "1jlrw8x894lmqxpl835ll96cpz6srkpjhqs0gqm785f1dib1hmsz")))

(define-public crate-css-purify-0.0.9 (c (n "css-purify") (v "0.0.9") (d (list (d (n "html5ever_ext") (r "^0.21.14") (d #t) (k 0)))) (h "1wbq7ikj8m4vwpgj4n9kw0jcqf8zv46f0iam1cz83g731vgz1wrk")))

(define-public crate-css-purify-0.0.10 (c (n "css-purify") (v "0.0.10") (d (list (d (n "html5ever_ext") (r "^0.21.15") (d #t) (k 0)))) (h "05qfzbd6p2gw4ig49amvm1v9khzvhw37dcly1njrwk1bmdk237s3")))

(define-public crate-css-purify-0.0.11 (c (n "css-purify") (v "0.0.11") (d (list (d (n "html5ever_ext") (r "^0.21.16") (d #t) (k 0)))) (h "0jafkg5g3ff9y55kysj26nydzm6kvsqxbvcmgyry5wx7y1h28jv1")))

(define-public crate-css-purify-0.0.12 (c (n "css-purify") (v "0.0.12") (d (list (d (n "html5ever_ext") (r "^0.21.17") (d #t) (k 0)))) (h "04sfjsr252mq8in9zh4bxcnrs1rlx66db77jxw57qq9l3g34jqkb")))

(define-public crate-css-purify-0.0.13 (c (n "css-purify") (v "0.0.13") (d (list (d (n "html5ever_ext") (r "^0.21.18") (d #t) (k 0)))) (h "150605cn95r45l6l2n2v4dsl5lyyb2qci7fkm4jqxlqa1x4n9sra")))

(define-public crate-css-purify-0.0.14 (c (n "css-purify") (v "0.0.14") (d (list (d (n "html5ever_ext") (r "^0.21.19") (d #t) (k 0)))) (h "05d6nh7kyyj8nk3zq8wqdkn88rglajsznhlsv6q1d05s8c7vmi4b")))

(define-public crate-css-purify-0.0.15 (c (n "css-purify") (v "0.0.15") (d (list (d (n "html5ever_ext") (r "^0.21.20") (d #t) (k 0)))) (h "0mppyn42a3mnrl2kzs510qhihrzrrw6lhnvhc2s5l872kpyljd3n")))

