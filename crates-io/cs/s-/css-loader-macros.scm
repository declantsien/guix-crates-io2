(define-module (crates-io cs s- css-loader-macros) #:use-module (crates-io))

(define-public crate-css-loader-macros-0.0.1 (c (n "css-loader-macros") (v "0.0.1") (d (list (d (n "cssparser") (r "^0.29") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rsass") (r "^0.25") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "05ini293sn7x8xjqwwgs30qdr6vgnkn2w161fnplc1j8dhyb7nzz")))

