(define-module (crates-io cs s- css-autoprefix) #:use-module (crates-io))

(define-public crate-css-autoprefix-0.0.0 (c (n "css-autoprefix") (v "0.0.0") (d (list (d (n "caniuse-serde") (r "^0.0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "css") (r "^0.0.16") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)))) (h "00fl1n1ahkmzi9lhnqdvwdspcj5f6g77n3cz9njy9k60j4sp38fp")))

(define-public crate-css-autoprefix-0.0.1 (c (n "css-autoprefix") (v "0.0.1") (d (list (d (n "caniuse-serde") (r "^0.0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "css") (r "^0.0.18") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)))) (h "07qa9rqprbgni3ga8mqnif15fy4qx5qjm0z1hinbwzr4kpj0nmbk")))

(define-public crate-css-autoprefix-0.0.2 (c (n "css-autoprefix") (v "0.0.2") (d (list (d (n "caniuse-serde") (r "^0.0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "css") (r "^0.0.22") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)))) (h "17gj305gyr5c4l07f0h81r6g0jcyb7cqwi3ygcjqg1ix8rrbrkj0")))

(define-public crate-css-autoprefix-0.0.3 (c (n "css-autoprefix") (v "0.0.3") (d (list (d (n "caniuse-serde") (r "^0.0.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "css") (r "^0.0.22") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)))) (h "1fg8llgs9n2nqagf20qnz2izjjz2dp3hbv1d2g6v1pna3g1xn82y")))

(define-public crate-css-autoprefix-0.0.4 (c (n "css-autoprefix") (v "0.0.4") (d (list (d (n "caniuse-serde") (r "^0.0.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "css") (r "^0.0.22") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)))) (h "0cvnsxp2lafm300mb4f04f3kj1d2msfipgnlgm7m6yggjvcxg2nf")))

(define-public crate-css-autoprefix-0.0.5 (c (n "css-autoprefix") (v "0.0.5") (d (list (d (n "caniuse-serde") (r "^0.0.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "css") (r "^0.0.22") (d #t) (k 0)) (d (n "maplit") (r "^0.1") (d #t) (k 0)))) (h "0vfqzf5jxspywz3x06fyh3qgr8asa5dag3rfa06r7if2xgbp6ijh")))

