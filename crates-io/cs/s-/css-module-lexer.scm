(define-module (crates-io cs s- css-module-lexer) #:use-module (crates-io))

(define-public crate-css-module-lexer-0.0.1 (c (n "css-module-lexer") (v "0.0.1") (d (list (d (n "indoc") (r "^2") (d #t) (k 2)))) (h "1g7mhr0lr8b4vwh0fxzywpri9v0g4wsjkzzkd340fz4nq31dsyf5")))

(define-public crate-css-module-lexer-0.0.2 (c (n "css-module-lexer") (v "0.0.2") (d (list (d (n "codspeed-criterion-compat") (r "^2") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "smallvec") (r "^1.13") (d #t) (k 0)))) (h "0l9yax5192wpls4myvdx0m862kd71mcy73jdgdl8v9r1dkbw4y54")))

(define-public crate-css-module-lexer-0.0.3 (c (n "css-module-lexer") (v "0.0.3") (d (list (d (n "codspeed-criterion-compat") (r "^2") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 2)) (d (n "similar-asserts") (r "^1") (d #t) (k 2)) (d (n "smallvec") (r "^1.13") (d #t) (k 0)))) (h "0cz3m81y9rrd6rk478d6g2z990qph2m5ax2i6fxcihs12801nxj7")))

(define-public crate-css-module-lexer-0.0.4 (c (n "css-module-lexer") (v "0.0.4") (d (list (d (n "codspeed-criterion-compat") (r "^2") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 2)) (d (n "similar-asserts") (r "^1") (d #t) (k 2)) (d (n "smallvec") (r "^1.13") (d #t) (k 0)))) (h "1vyxkwjm96s4ws8zbas0bwna6rmgqjzzxwh5vy77kg0cyh422agy")))

(define-public crate-css-module-lexer-0.0.5 (c (n "css-module-lexer") (v "0.0.5") (d (list (d (n "codspeed-criterion-compat") (r "^2") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 2)) (d (n "similar-asserts") (r "^1") (d #t) (k 2)) (d (n "smallvec") (r "^1.13") (d #t) (k 0)))) (h "1h5mc352347sks4b7xc2ax5qxfrq7b7qr1chg0w6fjsw6vhdav86")))

(define-public crate-css-module-lexer-0.0.6 (c (n "css-module-lexer") (v "0.0.6") (d (list (d (n "codspeed-criterion-compat") (r "^2") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 2)) (d (n "similar-asserts") (r "^1") (d #t) (k 2)) (d (n "smallvec") (r "^1.13") (d #t) (k 0)))) (h "02fyxzh87raswi9d9h6xbay5pmrkbzj1dmdz5kzvxsk8r9k0ya4j")))

(define-public crate-css-module-lexer-0.0.7 (c (n "css-module-lexer") (v "0.0.7") (d (list (d (n "codspeed-criterion-compat") (r "^2") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 2)) (d (n "similar-asserts") (r "^1") (d #t) (k 2)) (d (n "smallvec") (r "^1.13") (d #t) (k 0)))) (h "1xfhh2fn5wq1bzk9nvgzpd3zp7m56xv7wbnljvq17dknvsx06f7r")))

(define-public crate-css-module-lexer-0.0.8 (c (n "css-module-lexer") (v "0.0.8") (d (list (d (n "codspeed-criterion-compat") (r "^2") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 2)) (d (n "similar-asserts") (r "^1") (d #t) (k 2)) (d (n "smallvec") (r "^1.13") (d #t) (k 0)))) (h "1qcylvqlf77nxkj06idqaw8jd74a42jlwhw8cpk3j2vmwalrmy73")))

(define-public crate-css-module-lexer-0.0.9 (c (n "css-module-lexer") (v "0.0.9") (d (list (d (n "codspeed-criterion-compat") (r "^2") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 2)) (d (n "similar-asserts") (r "^1") (d #t) (k 2)) (d (n "smallvec") (r "^1.13") (d #t) (k 0)))) (h "0cfv5gy51z0yw5h7wr0vfnih9fc7m9aas8dri00cvsm24jps4mzq")))

(define-public crate-css-module-lexer-0.0.10 (c (n "css-module-lexer") (v "0.0.10") (d (list (d (n "codspeed-criterion-compat") (r "^2") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "indoc") (r "^2") (d #t) (k 2)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 2)) (d (n "similar-asserts") (r "^1") (d #t) (k 2)) (d (n "smallvec") (r "^1.13") (d #t) (k 0)))) (h "19fk0vqdc30y0yh3716amfsi9mq41cy10arhg7q21f35x4mm40af")))

