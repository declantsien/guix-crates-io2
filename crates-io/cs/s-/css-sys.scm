(define-module (crates-io cs s- css-sys) #:use-module (crates-io))

(define-public crate-css-sys-0.0.0 (c (n "css-sys") (v "0.0.0") (h "0l981i3f4d73jkkr42mgxiv0zmx24zpkx9nj7s60pvxvsaa94y46")))

(define-public crate-css-sys-0.1.1 (c (n "css-sys") (v "0.1.1") (h "10hcr5hxg40kyz4qrqhwvb15qsc17phq5pj1pch580rm4zidir0i")))

(define-public crate-css-sys-0.1.2 (c (n "css-sys") (v "0.1.2") (h "0y4c4kz11rwgplrn8qs3bnqxy37kva5yfb3j5lm5q4v7f736acx5")))

(define-public crate-css-sys-0.1.3 (c (n "css-sys") (v "0.1.3") (h "0d3zrbhqldsr1wvfbq9yp1ilrx6842nzdk06g7cjzl6sr7gb8fia")))

