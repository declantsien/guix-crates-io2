(define-module (crates-io cs s- css-loader) #:use-module (crates-io))

(define-public crate-css-loader-0.0.1 (c (n "css-loader") (v "0.0.1") (d (list (d (n "css-loader-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "Document" "HtmlHeadElement"))) (d #t) (k 0)))) (h "1697qa0bavxjr528i73mr52qsynvkwhyscgphj66mi23cvacvvx8")))

