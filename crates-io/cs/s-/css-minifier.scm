(define-module (crates-io cs s- css-minifier) #:use-module (crates-io))

(define-public crate-css-minifier-0.1.0 (c (n "css-minifier") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "css-minify") (r "^0.1") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)))) (h "0nxwm1g0cmns97j2ryq4126bhc487q7nd6si2k2s91h9sr2720la")))

(define-public crate-css-minifier-0.1.1 (c (n "css-minifier") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "css-minify") (r "^0.1") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)))) (h "09rl49lfxblkfrjadhvcizihnif6hrjsacklgyjx639cif4m1clc")))

(define-public crate-css-minifier-0.2.0 (c (n "css-minifier") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "css-minify") (r "^0.2") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)))) (h "0f8w8wxrfp13xm67rnhkpy13gbqcy5mx3bkhr0bczh1x270msh3c")))

(define-public crate-css-minifier-0.2.1 (c (n "css-minifier") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "css-minify") (r "^0.2") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)))) (h "1a9dgs938lhbbqa0s8p0dmy6rvc92m5yl0qgs6av5qf1z8v6gga4")))

(define-public crate-css-minifier-0.3.0 (c (n "css-minifier") (v "0.3.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "css-minify") (r "^0.3") (d #t) (k 0)) (d (n "indoc") (r "^1.0") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1") (d #t) (k 0)))) (h "0xbnzvzng9xy8xnybbvsky7h5y7fhgcvp31siwbdhcq9lsw22bqd")))

