(define-module (crates-io cs v_ csv_utility) #:use-module (crates-io))

(define-public crate-csv_utility-0.1.0 (c (n "csv_utility") (v "0.1.0") (d (list (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "csv") (r "^1.1.4") (d #t) (k 0)) (d (n "dialoguer") (r "^0.7.1") (d #t) (k 0)) (d (n "seahorse") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0w8af4p8jd6n33kdrrhi27j0yf1719i63mrkvbc2wh4wy48473vi")))

(define-public crate-csv_utility-0.1.1 (c (n "csv_utility") (v "0.1.1") (d (list (d (n "csv") (r "^1.1.5") (d #t) (k 0)) (d (n "dialoguer") (r "^0.7.1") (d #t) (k 0)) (d (n "seahorse") (r "^1.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wsiiaj2q0pn8x0gv6v0fhzdvbdvcpgyqwjp4d07ac45yjr2fnn3")))

