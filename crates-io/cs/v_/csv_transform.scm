(define-module (crates-io cs v_ csv_transform) #:use-module (crates-io))

(define-public crate-csv_transform-0.1.0 (c (n "csv_transform") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "sqlx") (r "^0.6") (f (quote ("runtime-tokio-native-tls" "sqlite" "offline"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 0)))) (h "1lvn86fq1nybppf17q9y1hxpma1j216wwc5g151azbammyrwyr96")))

