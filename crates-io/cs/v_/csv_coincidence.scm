(define-module (crates-io cs v_ csv_coincidence) #:use-module (crates-io))

(define-public crate-csv_coincidence-0.1.0 (c (n "csv_coincidence") (v "0.1.0") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "149x9rhxm4z8kkxfmx49cv6v23d69sm9yjjr2hhjqbcf1xaq68lj") (y #t)))

(define-public crate-csv_coincidence-0.1.1 (c (n "csv_coincidence") (v "0.1.1") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1ac1mzxp55vlhkqzpn0m8mk3rn3f6dglkbj1yl2skjahz1cjc8dv")))

