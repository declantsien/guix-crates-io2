(define-module (crates-io cs v_ csv_ledger) #:use-module (crates-io))

(define-public crate-csv_ledger-0.1.0 (c (n "csv_ledger") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0a2nxsqb94bd0zgyc4azy7z3qssk557x95qbfd0jvq8kq2vkgi9n") (f (quote (("test_args") ("default"))))))

(define-public crate-csv_ledger-0.1.1 (c (n "csv_ledger") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv_ledger_lib") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0nzaq5rk6ns8lkrakcwza32s2nkvj93a7qh5wibwhs90qh42dzcq") (f (quote (("test_args") ("default"))))))

(define-public crate-csv_ledger-0.1.2 (c (n "csv_ledger") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv_ledger_lib") (r "^0.1.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1z30fssb49xk6li63fcxdqy36pvafr9ahmyknzrk5wbfwp13g40f") (f (quote (("test_args") ("default"))))))

