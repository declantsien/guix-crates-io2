(define-module (crates-io cs v_ csv_stacker) #:use-module (crates-io))

(define-public crate-csv_stacker-0.1.0 (c (n "csv_stacker") (v "0.1.0") (d (list (d (n "polars") (r "^0.25.1") (f (quote ("lazy"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1apvcsrgpdfw9bc9psfrydbyyi9llmvm6mk34c9ny1chpl955rvb")))

(define-public crate-csv_stacker-0.1.1 (c (n "csv_stacker") (v "0.1.1") (d (list (d (n "polars") (r "^0.25.1") (f (quote ("lazy"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "12mwkr4v3ay8q79knq748sj4w7c0j1gnc3271i6834bmx68v0n20")))

(define-public crate-csv_stacker-0.1.2 (c (n "csv_stacker") (v "0.1.2") (d (list (d (n "polars") (r "^0.25.1") (f (quote ("lazy"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1hhgsg36g61ic9yw5dd4zw38646idap2ffyq6mqhx0liz7x4mn07")))

(define-public crate-csv_stacker-0.1.3 (c (n "csv_stacker") (v "0.1.3") (d (list (d (n "polars") (r "^0.25.1") (f (quote ("lazy"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1wbfspj4jprskafv2q99chwamxd2v4bgvh6l46hm76jyxjyyidvk")))

