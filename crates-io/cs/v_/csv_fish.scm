(define-module (crates-io cs v_ csv_fish) #:use-module (crates-io))

(define-public crate-csv_fish-0.2.0 (c (n "csv_fish") (v "0.2.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "csv") (r "^1.0") (d #t) (k 0)) (d (n "fishers_exact") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.16.0") (d #t) (k 0)))) (h "03p4nw3b5c4ppnsmhnswynbf1wawld1ffankb69l9l4i4rl5mbax")))

(define-public crate-csv_fish-0.2.2 (c (n "csv_fish") (v "0.2.2") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "csv") (r "^1.0") (d #t) (k 0)) (d (n "fishers_exact") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.16.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "01xa6ay7z5rkfvls9pphr1wihgrn6fsf2kjr125xa1p6dyl8y288")))

(define-public crate-csv_fish-0.2.3 (c (n "csv_fish") (v "0.2.3") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "csv") (r "^1.0") (d #t) (k 0)) (d (n "fishers_exact") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.16.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1ajhhcnmcdkiw9kavfx0p7bxpr5yrpr8yj0ggbwlazkymrsdf6kp")))

(define-public crate-csv_fish-0.2.4 (c (n "csv_fish") (v "0.2.4") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "csv") (r "^1.0") (d #t) (k 0)) (d (n "fishers_exact") (r "^1.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.16.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "1kqmmlvll8n384v9sklzljr0hd1askxd0jlmgg8sdfxgfjs2sfyq")))

