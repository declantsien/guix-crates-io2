(define-module (crates-io cs v_ csv_ledger_lib) #:use-module (crates-io))

(define-public crate-csv_ledger_lib-0.1.0 (c (n "csv_ledger_lib") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "0jpfkwzbjyzgprrzrv2zwhiyg8d43q3hl1rs4zs7x6hcsx5h8x0q")))

(define-public crate-csv_ledger_lib-0.1.1 (c (n "csv_ledger_lib") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "1sai220x915nn3r6whd4xq2hzxg6935mnahq71w0vryvl7ayxq5s")))

