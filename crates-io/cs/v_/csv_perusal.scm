(define-module (crates-io cs v_ csv_perusal) #:use-module (crates-io))

(define-public crate-csv_perusal-0.5.0 (c (n "csv_perusal") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)))) (h "0wnpk2z0qkxplq5sjsbapp60283vsjdgx71xjnh8k616fhv9dfvv")))

(define-public crate-csv_perusal-0.5.1 (c (n "csv_perusal") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)))) (h "0m269yk9b8mx9wjgds6z1c27vhaixk2b4jwifb76bp0jmv42fk57")))

(define-public crate-csv_perusal-0.5.2 (c (n "csv_perusal") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)))) (h "1n6240p7s8algfakwm00282gwnx4scmwwvxnn8wbyrybcw5an77m")))

(define-public crate-csv_perusal-0.6.0 (c (n "csv_perusal") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)))) (h "0xqal1mkwy4insl5ciq5ry214cyal9325kpn60bpkxbsh513glz3")))

(define-public crate-csv_perusal-0.6.1 (c (n "csv_perusal") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xjryhdx5xa79f18js9j894p8il11gfp4a6swlc1qf5ramlsc8yw")))

(define-public crate-csv_perusal-0.6.2 (c (n "csv_perusal") (v "0.6.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0krm7kxfcag5wbah50z2n0dwimfhssvbmay07kafmqv4gjq2li89")))

(define-public crate-csv_perusal-0.6.5 (c (n "csv_perusal") (v "0.6.5") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "memchr") (r "^2") (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xpagxf66h0r47l3nz8fmllh1nn1zpbqbxwkkm7w8vp56s80sz3p")))

(define-public crate-csv_perusal-0.7.0 (c (n "csv_perusal") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "memchr") (r "^2") (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "03bxcz58wkh3cr8lqpicpjmpksn6pzmf7n0jcsb6nbyqdwh8kn4p")))

(define-public crate-csv_perusal-0.7.1 (c (n "csv_perusal") (v "0.7.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "memchr") (r "^2") (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "rayon") (r "^1.9.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "13nncd47bwnzf1z5azwcdznaqviq4918ch2bynspn4w3vwwlyj14")))

