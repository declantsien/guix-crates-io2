(define-module (crates-io cs v_ csv_to_table) #:use-module (crates-io))

(define-public crate-csv_to_table-0.2.0 (c (n "csv_to_table") (v "0.2.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "tabled") (r "^0.11") (f (quote ("std"))) (k 0)))) (h "1is55bky3smsxw10i3cf36njnciq1yka6zj2acc944vz6b6rff8s") (f (quote (("color" "tabled/color"))))))

(define-public crate-csv_to_table-0.3.0 (c (n "csv_to_table") (v "0.3.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "tabled") (r "^0.12") (f (quote ("std"))) (k 0)))) (h "11kyxdjpl45yhqwfcb9ars27pfw8z1pv33w941d8hy7x873mszby") (f (quote (("color" "tabled/color"))))))

(define-public crate-csv_to_table-0.4.0 (c (n "csv_to_table") (v "0.4.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "tabled") (r "^0.15") (f (quote ("std"))) (k 0)))) (h "0x03brxn8b583pacx93ws5a793p80nq3mp1a3r19n8dihcraid3q") (f (quote (("macros" "tabled/macros") ("derive" "tabled/derive") ("ansi" "tabled/ansi"))))))

