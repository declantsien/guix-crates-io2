(define-module (crates-io cs v_ csv_reader) #:use-module (crates-io))

(define-public crate-csv_reader-0.1.0 (c (n "csv_reader") (v "0.1.0") (h "13d533z8bsvhw20fi8brpqx4i5vhmfjh29vbxkrsprfmcamqyy0k")))

(define-public crate-csv_reader-0.1.1 (c (n "csv_reader") (v "0.1.1") (h "0rkp99g5sd1q8kp35arbqicgzl4g5qkvcxnld589psllikp8rwbx")))

