(define-module (crates-io cs v_ csv_db) #:use-module (crates-io))

(define-public crate-csv_db-0.1.0 (c (n "csv_db") (v "0.1.0") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bvc4abzrpxpasrm0hgq7sygmhwq5l9fmkh7kagi8drxyhi0cyaz")))

(define-public crate-csv_db-0.1.1 (c (n "csv_db") (v "0.1.1") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kxcqrxs7azchrklp4rw1mgsi05wxi6k4nsg2n2zzzhj4splylfy")))

(define-public crate-csv_db-0.1.2 (c (n "csv_db") (v "0.1.2") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gfwsf14dw9s9884yw3m9g747g10x9ip6bffssanal6g3w3j0l6h")))

(define-public crate-csv_db-0.1.3 (c (n "csv_db") (v "0.1.3") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pkbmmwbadv5a7f7dfwdfx193b9k4776ba163iwqnadyngbbgkpm")))

(define-public crate-csv_db-0.2.0 (c (n "csv_db") (v "0.2.0") (d (list (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ff7vyg01bd3ylazcpvz26nk5j3xanyv86cfk5h5rwdn6h68h967")))

