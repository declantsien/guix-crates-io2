(define-module (crates-io cs #{45}# cs453getters) #:use-module (crates-io))

(define-public crate-cs453getters-0.1.0 (c (n "cs453getters") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (d #t) (k 0)))) (h "0gs5q378p4i5kr4qz5494k70i3b949a20dascn0hvpwa8mqggr1l")))

