(define-module (crates-io cs si cssifier) #:use-module (crates-io))

(define-public crate-cssifier-0.1.0 (c (n "cssifier") (v "0.1.0") (d (list (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "1adwxkdhrs0pr2918j72m9vmpsvs939j6zm9bgpnvc1by9wjiqm5")))

(define-public crate-cssifier-0.1.1 (c (n "cssifier") (v "0.1.1") (d (list (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "0l3spd9wcwzyy59gbzsc577nfa9p19irilm80vrwcapajzl09ikw")))

(define-public crate-cssifier-0.1.2 (c (n "cssifier") (v "0.1.2") (d (list (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "0d1ik872897fvnkyc8sjw0yhrbnjwfx16bsc19k6b8yjm3vrwdv5")))

(define-public crate-cssifier-0.1.3 (c (n "cssifier") (v "0.1.3") (d (list (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "0srmjkjs25q3jw62lvf7fc2j5gdk1z7ggk2ncbnrh0y4h2kb96fa")))

