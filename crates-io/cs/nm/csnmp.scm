(define-module (crates-io cs nm csnmp) #:use-module (crates-io))

(define-public crate-csnmp-0.1.0 (c (n "csnmp") (v "0.1.0") (d (list (d (n "from-to-repr") (r "^0.1") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("net" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1m3544caq8jrcm153nxvxv53pk228b0cjsv6pn0isvfg75kmdp59")))

(define-public crate-csnmp-0.2.0 (c (n "csnmp") (v "0.2.0") (d (list (d (n "from-to-repr") (r "^0.1") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("net" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1v7jss8achdv0ssvp5xv77dp0d544daqqf6gwxi8p81s8pb3rbf4")))

(define-public crate-csnmp-0.2.1 (c (n "csnmp") (v "0.2.1") (d (list (d (n "from-to-repr") (r "^0.1") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("net" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0crx1hphgwn5s6zcpvnkqh5nsm4valrj9yq7w67mam84qaa5cj9y")))

(define-public crate-csnmp-0.2.2 (c (n "csnmp") (v "0.2.2") (d (list (d (n "from-to-repr") (r "^0.1") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("net" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0355krvqgnd9q5h7mwjzsa2sxfmhmhm7j5z7p8mi2g09l2f856lv")))

(define-public crate-csnmp-0.2.3 (c (n "csnmp") (v "0.2.3") (d (list (d (n "from-to-repr") (r "^0.1") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("net" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1ykd372wy3yvhqd6hbapbzqj1c03hr53kd3bkfzk1s88p73vi4n2")))

(define-public crate-csnmp-0.3.0 (c (n "csnmp") (v "0.3.0") (d (list (d (n "from-to-repr") (r "^0.1") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("net" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "06n0nvf1nyafqv88m9nj5pyvcr8i4z7h3b2dj8j4hl0vy1q9d16j")))

(define-public crate-csnmp-0.3.1 (c (n "csnmp") (v "0.3.1") (d (list (d (n "from-to-repr") (r "^0.1") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("net" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "19yj8340jwmani45128ac7v0hcav36h89flqzf2s8c63nfgmq0h5")))

(define-public crate-csnmp-0.3.2 (c (n "csnmp") (v "0.3.2") (d (list (d (n "from-to-repr") (r "^0.1") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("net" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1r7rxp5cnapwl3f5snjl70r8rwxq88yphhi98h73pvlsx30vwjid")))

(define-public crate-csnmp-0.3.3 (c (n "csnmp") (v "0.3.3") (d (list (d (n "from-to-repr") (r "^0.1") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("net" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "00q9mxbhw0dpqqdh0ck8r9dksd5m99wvpmawlnirycbn2l4ak431")))

(define-public crate-csnmp-0.3.4 (c (n "csnmp") (v "0.3.4") (d (list (d (n "from-to-repr") (r "^0.1") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("net" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "15vq3fskj6zxds2qhlbg768ws8ka0h4nf35llnlg61sjgaicbz8q")))

(define-public crate-csnmp-0.3.5 (c (n "csnmp") (v "0.3.5") (d (list (d (n "derivative") (r "^2.2") (d #t) (k 0)) (d (n "from-to-repr") (r "^0.1") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("net" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1zbdvgfvnmxk68d7yxh2ykchfbq8ix7m74c8wm1s8kn1d1jf20l5")))

(define-public crate-csnmp-0.4.0 (c (n "csnmp") (v "0.4.0") (d (list (d (n "derivative") (r "^2.2") (d #t) (k 0)) (d (n "dhat") (r "^0.3") (d #t) (k 2)) (d (n "from-to-repr") (r "^0.1") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("net" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "06d9k3ripsr0hpwqvava52di1iy1lgsikgjqxns27ra2cr4l87v7")))

(define-public crate-csnmp-0.5.0 (c (n "csnmp") (v "0.5.0") (d (list (d (n "derivative") (r "^2.2") (d #t) (k 0)) (d (n "dhat") (r "^0.3") (d #t) (k 2)) (d (n "from-to-repr") (r "^0.1") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("net" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "036glm019jwyja1zdp9f61mhpcz4ci4k075fbdvz0wgwx2hpzf0y")))

(define-public crate-csnmp-0.6.0 (c (n "csnmp") (v "0.6.0") (d (list (d (n "derivative") (r "^2.2") (d #t) (k 0)) (d (n "dhat") (r "^0.3") (d #t) (k 2)) (d (n "from-to-repr") (r "^0.2") (d #t) (k 0)) (d (n "simple_asn1") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("net" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("full"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)))) (h "1rm4sdwqllxkbpdby9p055008c57y41khp17j09c7k4yiax8vnih")))

