(define-module (crates-io cs ve csvenum) #:use-module (crates-io))

(define-public crate-csvenum-0.1.0 (c (n "csvenum") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1k2ynlgqvaafspal6s0bs32jdklhqw0s1xa7gx51k5lw7khbzbyp")))

(define-public crate-csvenum-0.1.1 (c (n "csvenum") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1lg83zjifi2355kmichcnwrzalrqd34rd3nraizawd04d1yakp2s")))

(define-public crate-csvenum-0.1.2 (c (n "csvenum") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "07j6046rm9yphmymjs2j4cm9w8k580lm0nfw8frjnh8zqr8rv42q")))

(define-public crate-csvenum-0.1.3 (c (n "csvenum") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1f9fi081ir8jd94rxyfmvbfg8jxnx9jg41bf0jb6bmabvg80ns4j")))

(define-public crate-csvenum-0.1.4 (c (n "csvenum") (v "0.1.4") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1h52jliahfkfp4racvirqapkslmgzfhz4w62pswjcsckh06k2364")))

