(define-module (crates-io cs vl csvlm) #:use-module (crates-io))

(define-public crate-csvlm-0.1.1 (c (n "csvlm") (v "0.1.1") (h "0piszcwbbyl4fvj2vw9w0ymh25cq1sf3lzsl7igy9rflaqcvjkpg")))

(define-public crate-csvlm-0.1.2 (c (n "csvlm") (v "0.1.2") (h "0x2phhjfwdrjhkg0ryg8sqqi7lihbqhs5hpj4nfwfvxsfvv343w5")))

(define-public crate-csvlm-0.1.3 (c (n "csvlm") (v "0.1.3") (h "04ak3zi223zzxkrrl9l4s6zf8chshh0jln7r9hs0035qzxclwxdx")))

(define-public crate-csvlm-0.1.4 (c (n "csvlm") (v "0.1.4") (h "1lwqdxc46jhvpjgaphgjgkp69kjm46ykb90b44ji6cl1yhaz5v7m")))

