(define-module (crates-io cs vl csvll) #:use-module (crates-io))

(define-public crate-csvll-0.1.5 (c (n "csvll") (v "0.1.5") (h "1fcdylc4g09his7f409hsav3pf63sb5zcyydrx8s4pdn6ifvh1hi")))

(define-public crate-csvll-0.1.6 (c (n "csvll") (v "0.1.6") (h "10bc3si8vdn70zh9qzvgrw7jvc8fwgxvj8mwykslmgzyxjm0c4xi")))

