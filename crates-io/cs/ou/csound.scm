(define-module (crates-io cs ou csound) #:use-module (crates-io))

(define-public crate-csound-0.1.0 (c (n "csound") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "csound-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0myww3z0bbd6nq9fnzl069sczlxmzmq53vlx9169c5fxj11zmsl6")))

(define-public crate-csound-0.1.1 (c (n "csound") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "csound-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09dk432d54z28w2mbgvzrchl9sl8l81d2yfaz27d0f6j5cgacvzl")))

(define-public crate-csound-0.1.2 (c (n "csound") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "csound-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cj1fsc0d3scj7jmrw67gi2nzslsaw1dynlq23sqz1964pdnrkrc")))

(define-public crate-csound-0.1.3 (c (n "csound") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "csound-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1r9n4q7a1aj87rp2lnb2qk5ajw2i4pnmp01232dpc95m7c478c7i")))

(define-public crate-csound-0.1.4 (c (n "csound") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "csound-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pf2ic4rkfgw1r11ncgv6kzmnr0svm4hv9bq3dc7z2ankrnzc59y")))

(define-public crate-csound-0.1.5 (c (n "csound") (v "0.1.5") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "csound-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ml88zm106j0hj1np85d3zck7dwgdb913v163x5ycv77zbxl266c")))

(define-public crate-csound-0.1.6 (c (n "csound") (v "0.1.6") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "csound-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)))) (h "1i19g4hhp3xm9ckvavf64flbdjznc282ny20wf5n3xk3p2fj29p6")))

(define-public crate-csound-0.1.7 (c (n "csound") (v "0.1.7") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "csound-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)))) (h "0ii3zppp4md7b4qbb82llxbbxnfl8zn296szf95drk1qkbrqyjhr")))

(define-public crate-csound-0.1.8 (c (n "csound") (v "0.1.8") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0) (p "bitflags")) (d (n "csound_sys") (r "^0.1.2") (d #t) (k 0) (p "csound-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0) (p "libc")) (d (n "rand") (r "^0.6.4") (d #t) (k 2) (p "rand")) (d (n "va_list") (r "^0.1.3") (d #t) (k 0) (p "va_list")))) (h "0zfm90ilp0bm364vx3bcr0d11nm0i4hr680zvg5c5xb5fdfh8jcd")))

