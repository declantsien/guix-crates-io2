(define-module (crates-io cs ou csound-sys) #:use-module (crates-io))

(define-public crate-csound-sys-0.1.0 (c (n "csound-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "1g9sq3zypxwjmz140xpjgp4rz0n8mhg6plivm128963kqmzg5s68") (f (quote (("dynamic" "pkg-config") ("default" "dynamic")))) (l "csound64")))

(define-public crate-csound-sys-0.1.1 (c (n "csound-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0albx8z0md3sy9ngjd1rjyll0msfkl4k9fx4ik8qdp9bgbd2xx91") (l "csound64")))

(define-public crate-csound-sys-0.1.2 (c (n "csound-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0) (p "libc")) (d (n "va_list") (r "^0.1.3") (d #t) (k 0) (p "va_list")))) (h "0fgyphfq9pf5fkmiddr3nvap5wg9gmfcqrlys9j1n4gdx9liicc6") (l "csound64")))

