(define-module (crates-io cs s_ css_math) #:use-module (crates-io))

(define-public crate-css_math-1.0.0 (c (n "css_math") (v "1.0.0") (d (list (d (n "matches") (r "^0.1.10") (d #t) (k 2)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)))) (h "1pca03py80xhzb2ms4jsqn4738sj6gcdkisiha70v4jsbpagv28a")))

