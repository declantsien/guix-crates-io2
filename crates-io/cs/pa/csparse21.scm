(define-module (crates-io cs pa csparse21) #:use-module (crates-io))

(define-public crate-csparse21-0.2.2 (c (n "csparse21") (v "0.2.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.4") (d #t) (k 0)))) (h "04j2mi1vb23vhxq8wm75vna40s2kawmmw21550w795x987z5wxwc")))

(define-public crate-csparse21-0.2.3 (c (n "csparse21") (v "0.2.3") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.4") (d #t) (k 0)))) (h "09nfvrfjsaxmdv55a7y36abkpc6cjvxmclrjnkhamzq7xfqlcc98")))

