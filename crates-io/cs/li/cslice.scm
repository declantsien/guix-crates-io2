(define-module (crates-io cs li cslice) #:use-module (crates-io))

(define-public crate-cslice-0.1.0 (c (n "cslice") (v "0.1.0") (h "1qgv73ijannbxh5plxhhn4zx5fa5j4sq4101lfxaxgaahsjz9fp8")))

(define-public crate-cslice-0.1.1 (c (n "cslice") (v "0.1.1") (h "0hdlbiawnbdm3n1x62v55w4xkmsg9xkzv07vjpi13w4nhsqjk3w7")))

(define-public crate-cslice-0.2.0 (c (n "cslice") (v "0.2.0") (h "0h2gy0b6c9szs5sxn9ch7v4826221barrq72yjqh40jna17p2z39")))

(define-public crate-cslice-0.3.0 (c (n "cslice") (v "0.3.0") (h "02myjqpkk0k4p0kjlcn1lyb8kl2b4z9yckcrch7b3r07c4qbg30g")))

