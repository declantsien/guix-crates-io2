(define-module (crates-io cs pg cspgen) #:use-module (crates-io))

(define-public crate-cspgen-0.1.0 (c (n "cspgen") (v "0.1.0") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "headless_chrome") (r "^0.9.0") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1ivfjw7f35mmlmzmfaz11lipfpdbywp6rkmvwn84gzszx884n0p7")))

(define-public crate-cspgen-0.2.0 (c (n "cspgen") (v "0.2.0") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "headless_chrome") (r "^0.9.0") (d #t) (k 0)) (d (n "merge") (r "^0.1.0") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "15q4apv1xh75x323z4x0z16imh4nr9xjsddny1xb5skv0l7ayph6")))

(define-public crate-cspgen-0.2.1 (c (n "cspgen") (v "0.2.1") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "headless_chrome") (r "^0.9.0") (d #t) (k 0)) (d (n "merge") (r "^0.1.0") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1a8m9961m2crr9fw9jrsmlqr5da51854y9bkp673jwjbsmil15ha")))

(define-public crate-cspgen-0.2.2 (c (n "cspgen") (v "0.2.2") (d (list (d (n "clap") (r "^2") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "headless_chrome") (r "^0.9.0") (d #t) (k 0)) (d (n "merge") (r "^0.1.0") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "13mk78mka7fw259brhaf03vcm717d7wrn39jk0sbvs3mxy0wcp2l")))

(define-public crate-cspgen-0.2.3 (c (n "cspgen") (v "0.2.3") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "headless_chrome") (r "^0.9.0") (d #t) (k 0)) (d (n "merge") (r "^0.1.0") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "108w9iw3038clyj85r1p09jmki8dwf0ya9zx1vp9s77173ymq43x")))

(define-public crate-cspgen-0.2.4 (c (n "cspgen") (v "0.2.4") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "headless_chrome") (r "^0.9.0") (d #t) (k 0)) (d (n "merge") (r "^0.1.0") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1cj5blxv2ibzw7bzwc87lx0n1wy1gjzd6x7anff512zjh6is7ppd")))

(define-public crate-cspgen-0.2.5 (c (n "cspgen") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "headless_chrome") (r "^1.0.2") (d #t) (k 0)) (d (n "merge") (r "^0.1.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1i2ng0wll43qy9imx1z3nrvv4ymhik9vj6kgj35hh2ryv7dqw2di")))

(define-public crate-cspgen-0.2.6 (c (n "cspgen") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "headless_chrome") (r "^1.0.2") (d #t) (k 0)) (d (n "merge") (r "^0.1.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0rp3b63bybkn3nd1bcas5zhlkbqhpv3p6j7h3nkji7zcx5mmq68k")))

(define-public crate-cspgen-0.2.7 (c (n "cspgen") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "headless_chrome") (r "^1.0.2") (d #t) (k 0)) (d (n "merge") (r "^0.1.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "11c6ibw23is1dxl4rz7sy00alaf12papwscn5p2z2bsb6cidn6nn")))

