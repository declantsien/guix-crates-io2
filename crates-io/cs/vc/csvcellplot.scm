(define-module (crates-io cs vc csvcellplot) #:use-module (crates-io))

(define-public crate-csvcellplot-0.1.0 (c (n "csvcellplot") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "csv") (r "^1.2.0") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (f (quote ("png"))) (k 0)) (d (n "imageproc") (r "^0.23.0") (k 0)) (d (n "lerp") (r "^0.4.0") (d #t) (k 0)) (d (n "num-integer") (r "^0.1.45") (d #t) (k 0)) (d (n "palette") (r "^0.6.1") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (d #t) (k 0)))) (h "1fv59pjf3ivi4cmygbmpzqbpv2xbrahwq1iwicqqhh781jp902yy")))

