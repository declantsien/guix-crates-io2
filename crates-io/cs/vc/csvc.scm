(define-module (crates-io cs vc csvc) #:use-module (crates-io))

(define-public crate-csvc-0.1.0 (c (n "csvc") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (k 0)))) (h "1d7y3iijmp5zjcjyka8rzg9mdznz9ziy4vyzscs9qqzbm1aw8dzy")))

(define-public crate-csvc-0.2.0 (c (n "csvc") (v "0.2.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (k 0)) (d (n "zstd") (r "^0.4.14") (d #t) (k 0)))) (h "1bljfpisazj6gi39srmgk6r0m87p6hp1g3cypzvw4295pvn3ki4c")))

