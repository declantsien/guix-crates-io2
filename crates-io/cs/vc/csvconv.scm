(define-module (crates-io cs vc csvconv) #:use-module (crates-io))

(define-public crate-csvconv-0.1.0 (c (n "csvconv") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "11hs4fy81c27q006946gycwa9wid5g68l9cryfgnikxrvbzsg02c")))

