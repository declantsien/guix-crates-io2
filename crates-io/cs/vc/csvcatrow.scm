(define-module (crates-io cs vc csvcatrow) #:use-module (crates-io))

(define-public crate-csvcatrow-0.1.0 (c (n "csvcatrow") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "1w67x7hrggdskmyz7bxha7vqqi4javnlih42wgvpsrfn7762crgv")))

