(define-module (crates-io cs vp csvpsql) #:use-module (crates-io))

(define-public crate-csvpsql-0.1.0 (c (n "csvpsql") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "dtparse") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0jq5d39m8ynvwrswa0b7y6gm72sh382igqibp04y9hidi0s1vqs4")))

(define-public crate-csvpsql-0.1.1 (c (n "csvpsql") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "dtparse") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1n3xj4sj311mbcm1ajpjb9glnv2kwvvjamxgvlhfxl4gjixscxig")))

(define-public crate-csvpsql-0.1.2 (c (n "csvpsql") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "dtparse") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1mds3fwzb6533d69bggcdjq1s0n9kd6g5h0pmr99vs5s7lj0rd52")))

(define-public crate-csvpsql-0.1.3 (c (n "csvpsql") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "dtparse") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1xy4xgplz3swv81xahkihhsx7k2xw4blvhb88d0sb51fbjg3i74s")))

(define-public crate-csvpsql-0.1.4 (c (n "csvpsql") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "dtparse") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1ywgzk6n0fhxpi7g9jznzxzsvgsar97kycbh5br096212m09lmxc")))

(define-public crate-csvpsql-0.1.5 (c (n "csvpsql") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "dtparse") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0dy3q7g62p4kr7jpzrzc1gm4b524zcfg6l5xl39ldfcbj9f1jh1n")))

