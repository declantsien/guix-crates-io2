(define-module (crates-io cs s3 css3) #:use-module (crates-io))

(define-public crate-css3-0.1.0 (c (n "css3") (v "0.1.0") (h "1xxndrkcj5vzmryw2amkf287kl1nlxh51ddxf1d95gdwr5iy4dy8")))

(define-public crate-css3-0.1.1 (c (n "css3") (v "0.1.1") (h "0pnk2szvazr4h102kr1f8dz1ljaq0i2c1lndxf8a4cgsbga53f7s")))

