(define-module (crates-io cs s3 css3-selector) #:use-module (crates-io))

(define-public crate-css3-selector-0.1.0 (c (n "css3-selector") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)))) (h "0xccwj23f4w5w1nbp4bkc2d6yyq3s43yrb2mpbqn1ax4mqq7hb0n")))

(define-public crate-css3-selector-0.1.1 (c (n "css3-selector") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1x1s4y8bgapj8pchg8plczrc12mn1y0iwzxgjbds577cbnlnpyqb")))

(define-public crate-css3-selector-0.1.2 (c (n "css3-selector") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "063nj3mm28dfdry6bqmaw756wd9d7dmmyh05h9jh86s364haq6rv")))

(define-public crate-css3-selector-0.1.3 (c (n "css3-selector") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1gy8s88bzkvkk7hi4xirvf4k4r41gi171v9gixd3s4qdl6jhz9xn")))

