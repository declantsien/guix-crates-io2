(define-module (crates-io cs -c cs-cli) #:use-module (crates-io))

(define-public crate-cs-cli-3.0.1 (c (n "cs-cli") (v "3.0.1") (d (list (d (n "chainseeker") (r "^3.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.6") (f (quote ("rt" "macros"))) (d #t) (k 0)))) (h "1c2xdfpc8sdbyjv564g6zlwpnbs7z7ybwnvq25c9iyfsll4q6h14")))

