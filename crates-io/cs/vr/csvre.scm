(define-module (crates-io cs vr csvre) #:use-module (crates-io))

(define-public crate-csvre-0.1.0 (c (n "csvre") (v "0.1.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "docopt") (r "^1.0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "01cixr2qd6vmk1lix1bjfk96kh96zgzgp6mzidx31csh0fsqa4s4")))

