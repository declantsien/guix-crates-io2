(define-module (crates-io cs vr csvrow) #:use-module (crates-io))

(define-public crate-csvrow-0.1.0 (c (n "csvrow") (v "0.1.0") (h "1ww6bwb4ij65hahdwd1xv4m0a90l3m3dbjxik2fcixrgw7c6kwha")))

(define-public crate-csvrow-0.1.1 (c (n "csvrow") (v "0.1.1") (h "0ks47mk81xrxigshbfavrb6h7n95khnn9gq0qsgsn1g3andw5a3i")))

(define-public crate-csvrow-0.2.1 (c (n "csvrow") (v "0.2.1") (h "01fbfh0ywimvlwdsljmrf1bbka76h819sd56fnyygvjj3k0gpp5l")))

