(define-module (crates-io cs vr csvr) #:use-module (crates-io))

(define-public crate-csvr-0.1.0 (c (n "csvr") (v "0.1.0") (h "0v60kjdfp62c0yavasd7cv13bia30sgy4g341ryh3m9d0k6z35ac") (y #t)))

(define-public crate-csvr-0.1.1 (c (n "csvr") (v "0.1.1") (h "04viax469qwnzkqbiqy8kky6mcpx5rlkb7v3ykw43l0cdgg5rf2r")))

