(define-module (crates-io cs vr csvroll) #:use-module (crates-io))

(define-public crate-csvroll-0.1.0 (c (n "csvroll") (v "0.1.0") (d (list (d (n "rollbuf") (r "^0.1.0") (d #t) (k 0)) (d (n "x86intrin") (r "^0.4.3") (d #t) (k 0)))) (h "1lphmvswxgmkf2bv07fazw14xy3c95qp00zp6a6745y48v4mv2zv")))

