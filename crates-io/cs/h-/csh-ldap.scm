(define-module (crates-io cs h- csh-ldap) #:use-module (crates-io))

(define-public crate-csh-ldap-0.1.0 (c (n "csh-ldap") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ldap3") (r "^0.10.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.21.1") (d #t) (k 0)))) (h "1vjgcppzlkh7sp1fr3fzqgzq6khiafhb19sp5jd757cbihl1rkr3")))

(define-public crate-csh-ldap-0.2.0 (c (n "csh-ldap") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ldap3") (r "^0.10.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.21.1") (d #t) (k 0)))) (h "0dngblw19avr59n54z5ir3jha11jqb6aykskzmv4yzpp7k9nc448")))

(define-public crate-csh-ldap-0.2.1 (c (n "csh-ldap") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.56") (d #t) (k 0)) (d (n "deadpool") (r "^0.9.5") (f (quote ("async-trait" "managed"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ldap3") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.21.2") (d #t) (k 0)))) (h "01i3hkapnmz2lzygzv6mlgdw1iknrk09r6kqn73jfxhx88cblzav")))

