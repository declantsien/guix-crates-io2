(define-module (crates-io cs pl csplit) #:use-module (crates-io))

(define-public crate-csplit-0.1.0 (c (n "csplit") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0xzlp2yy8dmhmc2pb858ij56xfva0638wx6n7bjv8h4dj7j0220z")))

