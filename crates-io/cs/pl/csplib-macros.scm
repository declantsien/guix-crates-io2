(define-module (crates-io cs pl csplib-macros) #:use-module (crates-io))

(define-public crate-csplib-macros-0.1.2 (c (n "csplib-macros") (v "0.1.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0q5xxv4x52xnb3y4097gzfj958zv8qn8qlnqiddpmcg6d80wivcq")))

