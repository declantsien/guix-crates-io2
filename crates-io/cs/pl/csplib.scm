(define-module (crates-io cs pl csplib) #:use-module (crates-io))

(define-public crate-csplib-0.1.0 (c (n "csplib") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1272jxv9m3cvbspf0hzkd1vwji1g5fwc11jb30fwdawnz01g2yn1")))

(define-public crate-csplib-0.1.1 (c (n "csplib") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1zhg7q0wpwvwpwvzf1jkks2lw1bdxs1ipa99r2blssrilnan8b7n")))

(define-public crate-csplib-0.1.2 (c (n "csplib") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "csplib-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0mj5m5n2i3rbqjaa3pc4d42c5rsjsnjas2cf0cp8k92r95afzxcm")))

