(define-module (crates-io cs te cstea) #:use-module (crates-io))

(define-public crate-cstea-1.0.0 (c (n "cstea") (v "1.0.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "rettle") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "08a0mk3wyr5g0scrmxadh1fcdflysk5hx5wqy3h2yczdvdyq2jmk")))

(define-public crate-cstea-1.0.1 (c (n "cstea") (v "1.0.1") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "rettle") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xsx9pzzsl4y346wsb9wi1crb0nd40sls06aqsdnq532bqbykrn8")))

(define-public crate-cstea-2.0.0 (c (n "cstea") (v "2.0.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "rettle") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yvkmcbd78fd4n8xlwgpl6kjl6aw8vqfbm1pf0mmny72qx5p3cfb")))

(define-public crate-cstea-2.0.1 (c (n "cstea") (v "2.0.1") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "rettle") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0972jdcgd6pgh5n845jf3f80pnl51drnlh823ldwxpkyc9s12x8z")))

