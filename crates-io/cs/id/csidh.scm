(define-module (crates-io cs id csidh) #:use-module (crates-io))

(define-public crate-csidh-0.1.0 (c (n "csidh") (v "0.1.0") (d (list (d (n "const-primes") (r "^0.4") (d #t) (k 0)) (d (n "crypto-bigint") (r "^0.6.0-pre.12") (k 0)) (d (n "oorandom") (r "^11") (d #t) (k 0)))) (h "1238mr3qazwf0rj4xi2057gy1281ri94n60p9rby4p6pq0q5b9dz") (f (quote (("no_cm_velu") ("no_cm_p_plus_1_over_4") ("no_cm_order") ("no_cm" "no_cm_velu" "no_cm_p_plus_1_over_4" "no_cm_order")))) (r "1.76")))

(define-public crate-csidh-0.2.0 (c (n "csidh") (v "0.2.0") (d (list (d (n "crypto-bigint") (r "^0.6.0-pre.12") (k 0)) (d (n "oorandom") (r "^11") (d #t) (k 0)))) (h "1yspyxy1l7wkllfsf57ysjy07hc3z9g15l21ykccr00a2453iybb") (r "1.76")))

