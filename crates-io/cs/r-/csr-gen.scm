(define-module (crates-io cs r- csr-gen) #:use-module (crates-io))

(define-public crate-csr-gen-0.2.0 (c (n "csr-gen") (v "0.2.0") (d (list (d (n "clap") (r "^2.21.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.3.1") (d #t) (k 0)))) (h "0n72h064iylisaz4qw8ri60dg8ljdal9z0r5bqqph6apwspqmhj0")))

