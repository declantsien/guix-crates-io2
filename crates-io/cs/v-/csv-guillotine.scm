(define-module (crates-io cs v- csv-guillotine) #:use-module (crates-io))

(define-public crate-csv-guillotine-0.1.0 (c (n "csv-guillotine") (v "0.1.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)))) (h "10591lq2qmp3gmqgv66x9h5vy7dy6lkywkdacifzfrdczcags2zl")))

(define-public crate-csv-guillotine-0.1.1 (c (n "csv-guillotine") (v "0.1.1") (d (list (d (n "csv") (r "^1") (d #t) (k 0)))) (h "07iwzah7hml70h8cc5fnqkn64p43nyfmimqkppkng19wnvwx32gr")))

(define-public crate-csv-guillotine-0.2.0 (c (n "csv-guillotine") (v "0.2.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)))) (h "0vax5wb3n7p6ly76w577mm6l4bqi05q8kh2s564kqdmvxfjw7qyq")))

(define-public crate-csv-guillotine-0.3.0 (c (n "csv-guillotine") (v "0.3.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)))) (h "0a8lf6frbzj7wid34sbqpw48lwn9jgbl8gp96vgsdqmpfyyjhn9v")))

(define-public crate-csv-guillotine-0.3.1 (c (n "csv-guillotine") (v "0.3.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)))) (h "1bxnslrwf4afa5gah2pf2bdhb4w5kz0a8nm70n28jyq9yv4qvqm8")))

(define-public crate-csv-guillotine-0.3.2 (c (n "csv-guillotine") (v "0.3.2") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)))) (h "07vj4f0895gw19yv9s7hhwda9mjgqs952fcka295zd5v8vrv23z5")))

(define-public crate-csv-guillotine-0.3.3 (c (n "csv-guillotine") (v "0.3.3") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)))) (h "0cjzjmsaw5gi6yp7sm0dng7w66xydnwbpqadix46rs4zmjql9lgl")))

(define-public crate-csv-guillotine-0.3.4 (c (n "csv-guillotine") (v "0.3.4") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)))) (h "0kl0vyhmh2p0s58w6xq2v0xa91agxgxcbszg39xzvvp3ry6yw5f4")))

(define-public crate-csv-guillotine-0.3.5 (c (n "csv-guillotine") (v "0.3.5") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)))) (h "1cmbk2vvzxcxpx7qrck3xh6xf1hl9anq8wmzd5k9k29mdbx7vlg3")))

