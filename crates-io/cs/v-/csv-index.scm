(define-module (crates-io cs v- csv-index) #:use-module (crates-io))

(define-public crate-csv-index-0.1.0 (c (n "csv-index") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.1") (d #t) (k 0)) (d (n "csv-core") (r "^0.1.0") (d #t) (k 0)))) (h "0q2ca2kz8687pxbp0lwjr3g2pjkdh3i53y1c9f2a808s2pdwq1bl")))

(define-public crate-csv-index-0.1.1 (c (n "csv-index") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.2") (d #t) (k 0)) (d (n "csv-core") (r "^0.1.1") (d #t) (k 0)))) (h "0alycqbmb0jyvkzddgzby5id3b6jisd9v2g47nq8kcq66jf4y76x")))

(define-public crate-csv-index-0.1.2 (c (n "csv-index") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.3") (d #t) (k 0)) (d (n "csv-core") (r "^0.1.2") (d #t) (k 0)))) (h "0ivcl0bra7rs7v6pwj1rmblkd8qdgqzm9nhigrk6pir98ryq9zi2")))

(define-public crate-csv-index-0.1.3 (c (n "csv-index") (v "0.1.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.4") (d #t) (k 0)))) (h "1zgkw77sf0n20gki2az965y33q2cn3icqnzg64a9b3abn03wx8yy")))

(define-public crate-csv-index-0.1.4 (c (n "csv-index") (v "0.1.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)))) (h "1awk07fpyjf7320233zf0m52sy3zl4yhkkdz5n1f18vn9pl5jmrw")))

(define-public crate-csv-index-0.1.5 (c (n "csv-index") (v "0.1.5") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)))) (h "1imzr3dyfysfxggzb20q379crk6id6j9fq0zzm1hv7bg07pvw9vv")))

(define-public crate-csv-index-0.1.6 (c (n "csv-index") (v "0.1.6") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "csv") (r "^1.1.0") (d #t) (k 0)))) (h "01048y84y0bakqm0x4y1svjv6lzc753b9q598xp7xgcqrdgi6x7j")))

