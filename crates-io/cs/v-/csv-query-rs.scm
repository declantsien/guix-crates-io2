(define-module (crates-io cs v- csv-query-rs) #:use-module (crates-io))

(define-public crate-csv-query-rs-0.2.1 (c (n "csv-query-rs") (v "0.2.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "csv") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)))) (h "025lw16l59kmq1bnq9zl2m2wbr5wi60qkr3x2kmbl9qk22vdcsg4")))

(define-public crate-csv-query-rs-0.2.2 (c (n "csv-query-rs") (v "0.2.2") (h "1hzx1sny3462m9c40ibn7hvb084pzylvzsn04d86skdhry2wgmqs")))

