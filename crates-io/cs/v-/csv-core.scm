(define-module (crates-io cs v- csv-core) #:use-module (crates-io))

(define-public crate-csv-core-0.1.0 (c (n "csv-core") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.3") (k 2)) (d (n "memchr") (r "^1") (k 0)))) (h "172i8hnlw0k8g5p7s2j56p72kpswk9w56rynz50p66fxjhqan4ba")))

(define-public crate-csv-core-0.1.1 (c (n "csv-core") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.3") (k 2)) (d (n "memchr") (r "^1") (k 0)))) (h "030cskqjdv906y638hajgqikad6fzaipi9j4vphp4ywihlr3mya3")))

(define-public crate-csv-core-0.1.2 (c (n "csv-core") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.3") (k 2)) (d (n "memchr") (r "^1") (k 0)))) (h "0lwvms3f5gf22dzl45z5047f5dsxbd0scjp3nidmabxixym1jjgd")))

(define-public crate-csv-core-0.1.3 (c (n "csv-core") (v "0.1.3") (d (list (d (n "arrayvec") (r "^0.3") (k 2)) (d (n "memchr") (r "^1") (k 0)))) (h "0v384ahrc16dxxz6yr12rp6lxqmsswrb1iawcw22v9fr46zvl7xf")))

(define-public crate-csv-core-0.1.4 (c (n "csv-core") (v "0.1.4") (d (list (d (n "arrayvec") (r "^0.3") (k 2)) (d (n "memchr") (r "^2") (k 0)))) (h "01a16w5yqprb9bmjz5yq8rsicdncr3nif4zgfr18p93vdzcfdn2d") (f (quote (("libc" "memchr/libc") ("default" "libc"))))))

(define-public crate-csv-core-0.1.5 (c (n "csv-core") (v "0.1.5") (d (list (d (n "arrayvec") (r "^0.4") (k 2)) (d (n "memchr") (r "^2") (k 0)))) (h "0r9xymqixn5hdkjhvb0wzyivfcnvq0dkhyphs7kzzrip5zvdwp7s") (f (quote (("libc" "memchr/libc") ("default" "libc"))))))

(define-public crate-csv-core-0.1.6 (c (n "csv-core") (v "0.1.6") (d (list (d (n "arrayvec") (r "^0.4") (k 2)) (d (n "memchr") (r "^2") (k 0)))) (h "0k5zs0x0qmmn27pa5kcg86lg84s29491fw5sh3zswxswnavasp4v") (f (quote (("libc" "memchr/libc") ("default" "libc"))))))

(define-public crate-csv-core-0.1.7 (c (n "csv-core") (v "0.1.7") (d (list (d (n "arrayvec") (r "^0.4") (k 2)) (d (n "memchr") (r "^2") (k 0)))) (h "0val9xx7rc54bykg6a6dqih854imaqmwkwjq0dxwdysz4psbwsq7") (f (quote (("libc" "memchr/libc") ("default"))))))

(define-public crate-csv-core-0.1.8 (c (n "csv-core") (v "0.1.8") (d (list (d (n "arrayvec") (r "^0.5") (k 2)) (d (n "memchr") (r "^2") (k 0)))) (h "0v9pd65a1h5v9pyinmfkbz75z61zrlxq82vgj9b227plxz0a9vzk") (f (quote (("libc" "memchr/libc") ("default"))))))

(define-public crate-csv-core-0.1.9 (c (n "csv-core") (v "0.1.9") (d (list (d (n "arrayvec") (r "^0.5") (k 2)) (d (n "memchr") (r "^2") (k 0)))) (h "0bl3046c68s3w845sgpp6jk2kncpd754q1w9jvrrk4w2fcqf4aya") (f (quote (("libc" "memchr/libc") ("default"))))))

(define-public crate-csv-core-0.1.10 (c (n "csv-core") (v "0.1.10") (d (list (d (n "arrayvec") (r "^0.5") (k 2)) (d (n "memchr") (r "^2") (k 0)))) (h "145wcc3560v1kmysqqspvddppiysr2rifqzy4nnlh3r6kxanc91b") (f (quote (("libc" "memchr/libc") ("default"))))))

(define-public crate-csv-core-0.1.11 (c (n "csv-core") (v "0.1.11") (d (list (d (n "arrayvec") (r "^0.5") (k 2)) (d (n "memchr") (r "^2") (k 0)))) (h "0w7s7qa60xb054rqddpyg53xq2b29sf3rbhcl8sbdx02g4yjpyjy") (f (quote (("libc" "memchr/libc") ("default"))))))

