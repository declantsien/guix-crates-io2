(define-module (crates-io cs v- csv-partial-cache) #:use-module (crates-io))

(define-public crate-csv-partial-cache-0.1.0 (c (n "csv-partial-cache") (v "0.1.0") (d (list (d (n "csv-line") (r "^0.1") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "07s1yr4ylyvqzn4av34zrfcvq812r3s60rawd0qw0k40i0i87c8r")))

(define-public crate-csv-partial-cache-0.2.0 (c (n "csv-partial-cache") (v "0.2.0") (d (list (d (n "csv-line") (r "^0.1") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0r2wxhjahdacwc2433s92rb61h94xqm6iigi3s5vz1naw7nf8c09") (f (quote (("default" "cache")))) (s 2) (e (quote (("cache" "dep:serde_json"))))))

