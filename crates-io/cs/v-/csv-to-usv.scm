(define-module (crates-io cs v- csv-to-usv) #:use-module (crates-io))

(define-public crate-csv-to-usv-1.0.0 (c (n "csv-to-usv") (v "1.0.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "usv") (r ">=0.5") (d #t) (k 0)))) (h "0z50fpz97kxl3s28z6jyqf44wvbklj62984b06fd0skg0r969jaw")))

(define-public crate-csv-to-usv-1.1.0 (c (n "csv-to-usv") (v "1.1.0") (d (list (d (n "criterion") (r ">=0.5") (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "rand") (r ">=0.8") (d #t) (k 2)) (d (n "usv") (r ">=0.5") (d #t) (k 0)))) (h "192528csnvs0ikg5jsfdg1yxws8wi934cqz6fjmp8vabq4xz7dnr")))

(define-public crate-csv-to-usv-1.1.1 (c (n "csv-to-usv") (v "1.1.1") (d (list (d (n "criterion") (r ">=0.5") (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "rand") (r ">=0.8") (d #t) (k 2)) (d (n "usv") (r ">=0.5") (d #t) (k 0)))) (h "1pbh5zgn42r9g1rfsjyf84jn43a59icpjhnvrvr29g3cafirydyb")))

(define-public crate-csv-to-usv-1.1.2 (c (n "csv-to-usv") (v "1.1.2") (d (list (d (n "criterion") (r ">=0.5") (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "rand") (r ">=0.8") (d #t) (k 2)) (d (n "usv") (r ">=0.5") (d #t) (k 0)))) (h "190rn8v449lyhw4zcqcivyg2829kmqld630678qfk3wxgnja5lv3")))

(define-public crate-csv-to-usv-1.1.3 (c (n "csv-to-usv") (v "1.1.3") (d (list (d (n "criterion") (r ">=0.5") (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "rand") (r ">=0.8") (d #t) (k 2)) (d (n "usv") (r "=0.6") (d #t) (k 0)))) (h "0qsvsc3kppqb0zjif77s40z1yzg8gq2b6v3p03c3jw6d47wvib5c")))

(define-public crate-csv-to-usv-1.1.4 (c (n "csv-to-usv") (v "1.1.4") (d (list (d (n "criterion") (r ">=0.5") (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "rand") (r ">=0.8") (d #t) (k 2)) (d (n "usv") (r "=0.6") (d #t) (k 0)))) (h "1hpxrgmvs5ki73cz1wy55k632iwb43m5a2hkc7bcy60zg49yp0is")))

(define-public crate-csv-to-usv-1.2.0 (c (n "csv-to-usv") (v "1.2.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "criterion") (r ">=0.5") (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r ">=0.8") (d #t) (k 2)) (d (n "usv") (r "=0.6") (d #t) (k 0)))) (h "170bsrm42gdhn9v5w3idb3rjxsqx8kc69x5rs2gs7lczd6pby9jf")))

(define-public crate-csv-to-usv-1.3.0 (c (n "csv-to-usv") (v "1.3.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "criterion") (r ">=0.5") (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r ">=0.8") (d #t) (k 2)) (d (n "usv") (r "^0.9.0") (d #t) (k 0)))) (h "0csi9vpj92vad8qn2p2gi9nln9kz4b6n6lmk6fh8nx6dsfj49rlh")))

(define-public crate-csv-to-usv-1.4.0 (c (n "csv-to-usv") (v "1.4.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "criterion") (r ">=0.5") (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r ">=0.8") (d #t) (k 2)) (d (n "usv") (r "^0.13.2") (d #t) (k 0)))) (h "0nbrg9k86szbrl6b4ndyhpagrkgi3w17mlspk9lbnydvy4nikvn6")))

(define-public crate-csv-to-usv-1.4.1 (c (n "csv-to-usv") (v "1.4.1") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "criterion") (r ">=0.5") (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r ">=0.8") (d #t) (k 2)) (d (n "usv") (r "^0.13.2") (d #t) (k 0)))) (h "1s1ym2cjpjlvk1hw0w52rnzhrwlszagcm30pp7sjn7hgwjg17giw")))

(define-public crate-csv-to-usv-1.5.0 (c (n "csv-to-usv") (v "1.5.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "criterion") (r ">=0.5") (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r ">=0.8") (d #t) (k 2)) (d (n "usv") (r "^0.13.12") (d #t) (k 0)))) (h "0pc2fmm5nil3757x22gvrhihgycwh67s3aqb60pnglx9dwry3dn9")))

(define-public crate-csv-to-usv-1.5.1 (c (n "csv-to-usv") (v "1.5.1") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "criterion") (r ">=0.5") (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r ">=0.8") (d #t) (k 2)) (d (n "usv") (r "^0.15") (d #t) (k 0)))) (h "1j19jwlbd49hg8sbyvmbbyh22apsp2clvjqnlibl66ns430b8w8w")))

(define-public crate-csv-to-usv-1.5.2 (c (n "csv-to-usv") (v "1.5.2") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "criterion") (r ">=0.5") (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r ">=0.8") (d #t) (k 2)) (d (n "usv") (r "^0.17.1") (d #t) (k 0)))) (h "0h7rkj99m12x0n3p1zi2iavz3x4dg47v7xapm1a7c3frfkz68mam")))

(define-public crate-csv-to-usv-1.5.3 (c (n "csv-to-usv") (v "1.5.3") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "criterion") (r ">=0.5") (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r ">=0.8") (d #t) (k 2)) (d (n "usv") (r "^0.19.1") (d #t) (k 0)))) (h "0kdy8c011akmvnyplvz0ggia6w6q64cy1kmbacg1j9xz6b1bbw9f")))

(define-public crate-csv-to-usv-1.5.4 (c (n "csv-to-usv") (v "1.5.4") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "criterion") (r ">=0.5") (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)) (d (n "rand") (r ">=0.8") (d #t) (k 2)) (d (n "usv") (r "^0.19.1") (d #t) (k 0)))) (h "1q3bbkjavjy9v5id7q42z4ik2rrx4w63hpa9s10bsf16z627jz1p")))

