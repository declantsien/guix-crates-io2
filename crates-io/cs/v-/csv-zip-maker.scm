(define-module (crates-io cs v- csv-zip-maker) #:use-module (crates-io))

(define-public crate-csv-zip-maker-0.1.0 (c (n "csv-zip-maker") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "1sir1q2j2kgl97qpxk7rrsivqc023g8g2riypl31xn8xbgqgsbky")))

(define-public crate-csv-zip-maker-0.1.1 (c (n "csv-zip-maker") (v "0.1.1") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "0z24q3yn44as6i7bdm6qccnrs6m56477c3xnzv82k30f8rnlmxy9")))

(define-public crate-csv-zip-maker-0.1.2 (c (n "csv-zip-maker") (v "0.1.2") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "1p5m9b3fwvlgrqc8zyy9fk0f3a3fw48qb5j7rxlgdlsgrrq7lq6h")))

(define-public crate-csv-zip-maker-0.2.0 (c (n "csv-zip-maker") (v "0.2.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "03nq3manpd2b7lhb9zap1h5miwp97y0wn7wgx14azqpd4kkp16gz")))

(define-public crate-csv-zip-maker-0.2.1 (c (n "csv-zip-maker") (v "0.2.1") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "0l7k8pfz457hja3wmg51rn6rnmlcvlpbpfsjq83jvmil94hq6b9r")))

(define-public crate-csv-zip-maker-0.2.2 (c (n "csv-zip-maker") (v "0.2.2") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (d #t) (k 0)))) (h "14dzlzwx63pak82lygij40ng0vw8zq660nvgssl82w33qazy3dlc")))

(define-public crate-csv-zip-maker-0.2.3 (c (n "csv-zip-maker") (v "0.2.3") (d (list (d (n "csv") (r "^1.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.10") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (f (quote ("deflate"))) (k 0)))) (h "1gpbw8xpaxmhiyp921r7xw9k78q82chzg4mw1sd6zc1b803i8hig")))

(define-public crate-csv-zip-maker-0.3.0 (c (n "csv-zip-maker") (v "0.3.0") (d (list (d (n "csv") (r "^1.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.10") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (f (quote ("deflate"))) (k 0)))) (h "15ijdjzzk4vxja3zbq9bhqd0jz8ifqvap1skksp5h10ra200hf80")))

(define-public crate-csv-zip-maker-0.3.1 (c (n "csv-zip-maker") (v "0.3.1") (d (list (d (n "csv") (r "^1.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.10") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (f (quote ("deflate"))) (k 0)))) (h "14cn2xfryfn1wayp1smgpbg1hz0534zc4ix02a1gaclzkjrm0fxa")))

(define-public crate-csv-zip-maker-0.4.0 (c (n "csv-zip-maker") (v "0.4.0") (d (list (d (n "csv") (r "^1.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.10") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "time") (r "^0.3.7") (f (quote ("std"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (f (quote ("deflate" "time"))) (k 0)))) (h "1yqp3awwnw273xr0z77mrr2x5aayszsbwqa6zgf366q2m1b0b6jn")))

