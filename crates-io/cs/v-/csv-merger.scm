(define-module (crates-io cs v- csv-merger) #:use-module (crates-io))

(define-public crate-csv-merger-0.1.0 (c (n "csv-merger") (v "0.1.0") (d (list (d (n "csv") (r "^1.2.1") (d #t) (k 0)))) (h "0z31313h7gi93vplgv6n512mlfbxv4f27dagpi766wzbw9743d27")))

(define-public crate-csv-merger-0.1.1 (c (n "csv-merger") (v "0.1.1") (d (list (d (n "csv") (r "^1.2.1") (d #t) (k 0)))) (h "055yqdm5y4n35iv4x5vz1iwdrgp8997paa7rynzpvl9245fklmam")))

