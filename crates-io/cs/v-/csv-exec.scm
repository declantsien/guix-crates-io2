(define-module (crates-io cs v- csv-exec) #:use-module (crates-io))

(define-public crate-csv-exec-0.1.0 (c (n "csv-exec") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "shell-words") (r "^0.1") (d #t) (k 0)))) (h "0zib7lnaj3xmfxw17y3jy3cns85c9qphr71ikmrrqqr1qly9d9kr")))

(define-public crate-csv-exec-0.2.0 (c (n "csv-exec") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "shell-words") (r "^0.1") (d #t) (k 0)))) (h "0x6z6gz8ir1qyk53lhk75qgb6gyx4zjss4d4p8y9w1ndznkq0pf6")))

(define-public crate-csv-exec-0.3.0 (c (n "csv-exec") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "assert_cmd") (r "^0.12") (d #t) (k 2)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "shell-words") (r "^0.1") (d #t) (k 0)))) (h "1f2h2ychc5vd5ypvc09jr9r9pwvi7hnzls4dxxprqpw3fy6zvaij")))

