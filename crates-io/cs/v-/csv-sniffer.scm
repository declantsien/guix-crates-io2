(define-module (crates-io cs v- csv-sniffer) #:use-module (crates-io))

(define-public crate-csv-sniffer-0.1.0 (c (n "csv-sniffer") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "csv-core") (r "^0.1.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1famhpiwvfjiwlhilxxmm2qqih3vij34ff5dxdhi448r8ngnshai")))

(define-public crate-csv-sniffer-0.1.1 (c (n "csv-sniffer") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "csv-core") (r "^0.1.4") (d #t) (k 0)) (d (n "memchr") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0fbz65c31rgm2406xldbp77k8k0p4ik17dkcbm80l9xvchhrb3kv")))

(define-public crate-csv-sniffer-0.2.0 (c (n "csv-sniffer") (v "0.2.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "csv-core") (r "^0.1") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0nic75rnf73sh5kcg69p1lmb4v431i2w05r1626pb241rnsnqpqd")))

(define-public crate-csv-sniffer-0.3.0 (c (n "csv-sniffer") (v "0.3.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "csv-core") (r "^0.1") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1rwa09h9z5yy32gcp6a43v17n95lqglhm4f2j1xmdvvpc0j4ry6h")))

(define-public crate-csv-sniffer-0.3.1 (c (n "csv-sniffer") (v "0.3.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "csv-core") (r "^0.1") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0705g1alpvs5ja71nw93ldicr9i2ri431q0sbkai094m7fljmqx1")))

