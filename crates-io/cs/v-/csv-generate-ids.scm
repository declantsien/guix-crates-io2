(define-module (crates-io cs v- csv-generate-ids) #:use-module (crates-io))

(define-public crate-csv-generate-ids-0.1.1 (c (n "csv-generate-ids") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0jymqr0p829dzq10qy2q0r9n3ph928ha4asp37wz0pfxc0shjqbx")))

(define-public crate-csv-generate-ids-0.1.2 (c (n "csv-generate-ids") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1vkhp0jx7s8g8v3nj7jcn4hyvqcq1ncpfn8c0ghi028sji5cvb86")))

