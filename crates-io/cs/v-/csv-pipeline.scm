(define-module (crates-io cs v- csv-pipeline) #:use-module (crates-io))

(define-public crate-csv-pipeline-0.1.0 (c (n "csv-pipeline") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "1y35yf3qnhw00zvkq56xcfxry2agknh50p6yfg9fz9wp2rk3fdrm")))

(define-public crate-csv-pipeline-0.2.0 (c (n "csv-pipeline") (v "0.2.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "1vks0xlaxbnyplgi3bqh80zf6adwkmxxpvs6bd27fcl6hp4q7hx5")))

(define-public crate-csv-pipeline-0.3.0 (c (n "csv-pipeline") (v "0.3.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "1icagwhbzw75ask9nq9rym7r38l495d3sdgn497fpazgdndlp4ih")))

(define-public crate-csv-pipeline-0.3.1 (c (n "csv-pipeline") (v "0.3.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "1bckhbybb3dkmm00320fhlzsd3iafpdlhzcypv58ncckgiq0z8ms")))

(define-public crate-csv-pipeline-0.4.0 (c (n "csv-pipeline") (v "0.4.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)))) (h "1fbxqzdiqbl8xmlsxyfirh9bm2fwammzzxvwdkc31vzldsr6951c")))

