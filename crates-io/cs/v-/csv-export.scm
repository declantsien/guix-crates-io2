(define-module (crates-io cs v- csv-export) #:use-module (crates-io))

(define-public crate-csv-export-0.1.0 (c (n "csv-export") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "minijinja") (r "^0.15.0") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "164n2qfz9g6jgnw1hjkxjjybz2ccpjpp4zpnb38hq7njmmfwgn1j")))

(define-public crate-csv-export-0.1.1 (c (n "csv-export") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "minijinja") (r "^0.15.0") (d #t) (k 0)) (d (n "predicates") (r "^2.1") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "139vk12kxv5h3dw3lvrgjz4k07qbapp9c9c446zb04yb6rw1qcc5")))

