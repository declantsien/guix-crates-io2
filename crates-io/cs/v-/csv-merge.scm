(define-module (crates-io cs v- csv-merge) #:use-module (crates-io))

(define-public crate-csv-merge-0.1.0 (c (n "csv-merge") (v "0.1.0") (d (list (d (n "assert_cli") (r "^0.6.3") (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 0)))) (h "1gd4020rcsdv494rw34gdry97rf11gpjk0l450fik07s51049ybg") (y #t)))

(define-public crate-csv-merge-0.1.1 (c (n "csv-merge") (v "0.1.1") (d (list (d (n "assert_cli") (r "^0.6.3") (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 0)))) (h "09pix57cz4xpqc6x2a8lw7ddw3zk7hhy3a2rk1zai27mrq1i1bm5")))

(define-public crate-csv-merge-0.1.2 (c (n "csv-merge") (v "0.1.2") (d (list (d (n "assert_cli") (r "^0.6.3") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "galvanic-test") (r "^0.2.0") (d #t) (k 0)))) (h "0va4j3cbl7lhvvfr323k325fkk43364fkc5pjkqn5q5k9i8332gj")))

