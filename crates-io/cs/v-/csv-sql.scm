(define-module (crates-io cs v- csv-sql) #:use-module (crates-io))

(define-public crate-csv-sql-0.1.0 (c (n "csv-sql") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)))) (h "0d5j7xpk937ashc5jw6aqriqwisrdzn0jkd221gnf9irn1sc8mvn")))

(define-public crate-csv-sql-0.1.1 (c (n "csv-sql") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)))) (h "1gfqb5dvvjvwmxbxyn569g9k805yfg75nzfnjm1a0zrr0r8l2xci")))

(define-public crate-csv-sql-0.1.2 (c (n "csv-sql") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)))) (h "11zi9rd6hgm1nc857c2sis5xw6a80d0gg0b8v97hzxsl4h7smbsx")))

(define-public crate-csv-sql-0.2.0 (c (n "csv-sql") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "1fkv87ih7vvkkq7nayz9l64jz4mj351wnk2c08kqjxas6bkzxyd5")))

(define-public crate-csv-sql-0.3.0 (c (n "csv-sql") (v "0.3.0") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "15i1lr9a5ss5agmaslmv17146cc5ap1pvzvcwp9dkwavanhxcg98")))

(define-public crate-csv-sql-0.4.0 (c (n "csv-sql") (v "0.4.0") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "0wcz4p0ppwjc5x4sljnzd98nbfis33jl4ngnc8l6ciylf7jq0kzp")))

