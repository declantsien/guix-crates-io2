(define-module (crates-io cs v- csv-parser) #:use-module (crates-io))

(define-public crate-csv-parser-0.1.0 (c (n "csv-parser") (v "0.1.0") (h "01w7i51iq57i5d6b1f874bvscwv6kmy8n2v12x4919d7kqgywy5v")))

(define-public crate-csv-parser-0.1.1 (c (n "csv-parser") (v "0.1.1") (h "1m18skgj8icrylvxg4dd3szvq7y738ir59s1rkzc49j1h9mrdhvd")))

