(define-module (crates-io cs v- csv-line) #:use-module (crates-io))

(define-public crate-csv-line-0.1.0 (c (n "csv-line") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "csv-core") (r "^0.1") (d #t) (k 2)) (d (n "quick-csv") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0xhqiavwcqvnzrrxf2s0ckbh3wc8z4xqc0f4bbib4cx2nk5id8zz")))

(define-public crate-csv-line-0.1.1 (c (n "csv-line") (v "0.1.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "csv-core") (r "^0.1") (d #t) (k 2)) (d (n "quick-csv") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1sn8qnql86q85qxpz9lripzd03xkhz4izxzcrwknm1zgzxxgfplz")))

(define-public crate-csv-line-0.2.0 (c (n "csv-line") (v "0.2.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "csv-core") (r "^0.1") (d #t) (k 2)) (d (n "quick-csv") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "028jq7jmchalvnzqdiv0lqpmdgqinfahsqfvlqxzw06j8ym5ffv9")))

