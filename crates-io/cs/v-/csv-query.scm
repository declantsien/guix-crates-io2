(define-module (crates-io cs v- csv-query) #:use-module (crates-io))

(define-public crate-csv-query-0.3.0 (c (n "csv-query") (v "0.3.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "csv") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)))) (h "1qd6spzfm41i1kf9l51b8k7csvda3sks4c1zam163iz79b7nssdk")))

(define-public crate-csv-query-0.3.1 (c (n "csv-query") (v "0.3.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "csv") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)))) (h "0a69wzyzvsfj616xsbjihwkkshcbznyisgl370nh9vy7dpypw7ss") (f (quote (("sqlite_bundled" "rusqlite/bundled"))))))

(define-public crate-csv-query-0.4.0 (c (n "csv-query") (v "0.4.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "csv") (r "^1.0.0") (d #t) (k 0)) (d (n "directories") (r "^1.0.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.14.0") (d #t) (k 0)) (d (n "rustyline") (r "^2.0.0") (o #t) (d #t) (k 0)))) (h "0ys1j8fk6cyhnp6y1y534lgv0nc1llh1cp4fmjxw6y4psfwqy1dh") (f (quote (("sqlite_bundled" "rusqlite/bundled") ("interactive" "rustyline"))))))

(define-public crate-csv-query-0.4.1 (c (n "csv-query") (v "0.4.1") (d (list (d (n "csv") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.14.0") (d #t) (k 0)))) (h "0w1i60inxvgslks04i12zjvhf98yaw55xvkjbis0visgmr4595x8") (f (quote (("sqlite_bundled" "rusqlite/bundled"))))))

(define-public crate-csv-query-0.5.0 (c (n "csv-query") (v "0.5.0") (d (list (d (n "csv") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.14.0") (f (quote ("backup"))) (d #t) (k 0)))) (h "1pp06d3s9ddc3fdz01gakxk2xs6zwj81lrb9p13snidli8chcp18") (f (quote (("sqlite_bundled" "rusqlite/bundled"))))))

