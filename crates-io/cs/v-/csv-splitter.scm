(define-module (crates-io cs v- csv-splitter) #:use-module (crates-io))

(define-public crate-csv-splitter-0.1.0 (c (n "csv-splitter") (v "0.1.0") (d (list (d (n "args") (r "^2.2.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "09pig2f6lxq13fqxwwr3siq7hvi6rvn289pb6alsm7bwj0971rvy")))

(define-public crate-csv-splitter-0.1.1 (c (n "csv-splitter") (v "0.1.1") (d (list (d (n "args") (r "^2.2.0") (d #t) (k 0)) (d (n "chardet") (r "^0.2.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.33") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "0pz5i1x6wzd4r4prjnbridxc8d6zf3k6c0kb71rpg18g4zjbabg5")))

(define-public crate-csv-splitter-0.2.1 (c (n "csv-splitter") (v "0.2.1") (d (list (d (n "args") (r "^2.2.0") (d #t) (k 0)) (d (n "chardet") (r "^0.2.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.33") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "lapin") (r "^2.3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0n54l6hvy2bvivcrabgp64w1fq346wl7l51bgjfni6i7394hj5bv")))

