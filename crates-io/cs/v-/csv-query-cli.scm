(define-module (crates-io cs v- csv-query-cli) #:use-module (crates-io))

(define-public crate-csv-query-cli-0.4.1 (c (n "csv-query-cli") (v "0.4.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "csv-query") (r "^0.4.1") (d #t) (k 0)) (d (n "directories") (r "^1.0.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "rustyline") (r "^2.0.0") (d #t) (k 0)))) (h "14pnlnnrsr6mm141zzx46pnnqq49v0l0cpnrmizr6fvqsjp6yrlc") (f (quote (("sqlite_bundled" "csv-query/sqlite_bundled"))))))

