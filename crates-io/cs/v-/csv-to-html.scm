(define-module (crates-io cs v- csv-to-html) #:use-module (crates-io))

(define-public crate-csv-to-html-0.4.6 (c (n "csv-to-html") (v "0.4.6") (d (list (d (n "build_html") (r "^2.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)))) (h "1yb0pbvq5crf2xl99d6xndvkzhs6lfr8y3rxfya6ii87mbhspy8g")))

(define-public crate-csv-to-html-0.5.8 (c (n "csv-to-html") (v "0.5.8") (d (list (d (n "build_html") (r "^2.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)))) (h "0346s8lnvps2ihdq37mb20g36bii50scwq92mg0jzvg25dl8ik08")))

(define-public crate-csv-to-html-0.5.10 (c (n "csv-to-html") (v "0.5.10") (d (list (d (n "build_html") (r "^2.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)))) (h "1518kl5v60kdr7b6j2x2blwpcsv7qyh2nk53l4zhrycmhyi0vnh2")))

