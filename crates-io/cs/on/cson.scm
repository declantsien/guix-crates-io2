(define-module (crates-io cs on cson) #:use-module (crates-io))

(define-public crate-cson-0.1.0 (c (n "cson") (v "0.1.0") (h "0ajsy8yvilv3lqr7dx0zkclg7c7ca8h0d22920asn013l3dwh6zj")))

(define-public crate-cson-0.1.1 (c (n "cson") (v "0.1.1") (h "014hki7xmwjwgd0s9mlcr4kgplj87xlkc2a6d7wyjpvv03r5gja6")))

(define-public crate-cson-0.1.2 (c (n "cson") (v "0.1.2") (h "15pvgggb5id3qj7sf871dblh3zh7i8d7fg54bigqmh3kkbfwnhns")))

(define-public crate-cson-0.1.3 (c (n "cson") (v "0.1.3") (h "01lf756zkgrjb8y9c27lx64iqwh329w8mymp4scyldhhgabcbyl4")))

(define-public crate-cson-0.1.4 (c (n "cson") (v "0.1.4") (h "1gdphm41zq3lb1fjplgry72n3xp9d806ixq3brckp88hp0hr2wj7")))

(define-public crate-cson-0.1.5 (c (n "cson") (v "0.1.5") (h "1hwml7ahk6k1zg8rrsi8plyz2m3dbb40713a87fyjh3ad9nqi3nj")))

(define-public crate-cson-0.1.6 (c (n "cson") (v "0.1.6") (h "0wl59arkklhbfnwm7xqgxyr6bb53kxg5qpxjfc792hrfd93d2qq3")))

(define-public crate-cson-0.1.7 (c (n "cson") (v "0.1.7") (h "153fhz8phrvmvwdns3vl95wcbl1mgp8ndllj9f0cm974y7493q45")))

(define-public crate-cson-0.1.8 (c (n "cson") (v "0.1.8") (d (list (d (n "rustc-serialize") (r "^0.2.4") (d #t) (k 0)))) (h "0s3vczgg5ry2x7f632bkcpsr3mxy3nrnk2hgb61z6fzxxrp4bc7r")))

(define-public crate-cson-0.1.9 (c (n "cson") (v "0.1.9") (d (list (d (n "rustc-serialize") (r "^0.2.6") (d #t) (k 0)))) (h "09mfljbj1rmxqf8pppcqi4fyd5k9s64hkhiw4xzb95nc7rqci4mm")))

(define-public crate-cson-0.1.10 (c (n "cson") (v "0.1.10") (d (list (d (n "rustc-serialize") (r "^0.2.7") (d #t) (k 0)))) (h "024zl478ibv4lhaf5qrsi803iaiqbqgg0bzb13qgr5x8j6pigbbr")))

(define-public crate-cson-0.1.11 (c (n "cson") (v "0.1.11") (d (list (d (n "rustc-serialize") (r "^0.2.9") (d #t) (k 0)))) (h "07d3jlghpir8gfian933qlh06pz378vdx9daglbj97kvxb0k7y8v")))

(define-public crate-cson-0.1.12 (c (n "cson") (v "0.1.12") (d (list (d (n "rustc-serialize") (r "^0.2.10") (d #t) (k 0)))) (h "0d1gp8hxjwwq6kqhmci6gakk371axrcw5s0x5kbkb24wjd76011i")))

(define-public crate-cson-0.1.13 (c (n "cson") (v "0.1.13") (d (list (d (n "rustc-serialize") (r "^0.2.10") (d #t) (k 0)))) (h "0dbmfp4x56h7vfgf8aq7yixsl31hn6ip5jqrvg0p7c96dmzynhm6")))

(define-public crate-cson-0.1.14 (c (n "cson") (v "0.1.14") (d (list (d (n "rustc-serialize") (r "^0.2.11") (d #t) (k 0)))) (h "08nmzcml8pag0mpb1xpk65fgbx9vil7hm96353j0rb9wq2vf092z")))

(define-public crate-cson-0.1.15 (c (n "cson") (v "0.1.15") (d (list (d (n "rustc-serialize") (r "^0.2.12") (d #t) (k 0)))) (h "0sk0bifxd95905kqk1wk2inpm9s5rn507l10rsq6kxjgibyrpa04")))

(define-public crate-cson-0.1.16 (c (n "cson") (v "0.1.16") (d (list (d (n "rustc-serialize") (r "^0.2.15") (d #t) (k 0)))) (h "084dx19l8in1p6fkxi6c7zy34m01pcvbf2gpqyfz0sdi5h9k8fva")))

