(define-module (crates-io cs bu csbubble_sort) #:use-module (crates-io))

(define-public crate-csbubble_sort-0.1.0 (c (n "csbubble_sort") (v "0.1.0") (h "0m8gkcwdngp0z12zkv01piw4nc8kc4wzivl044x0p4sbg2snxhsi") (y #t)))

(define-public crate-csbubble_sort-0.1.1 (c (n "csbubble_sort") (v "0.1.1") (h "0466ih68vg04g0r3738csyn5ks9hwjp6xdw4rkx3vipnzcmdnyq2")))

