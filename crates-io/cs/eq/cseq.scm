(define-module (crates-io cs eq cseq) #:use-module (crates-io))

(define-public crate-cseq-0.1.0 (c (n "cseq") (v "0.1.0") (d (list (d (n "binout") (r "^0.2") (d #t) (k 0)) (d (n "bitm") (r "^0.3") (d #t) (k 0)) (d (n "dyn_size_of") (r "^0.4") (d #t) (k 0)))) (h "1vjdjvlz5fqdbf8d42ba3jzzim3wb6bqry7hv17lwpgd1i5nj695")))

(define-public crate-cseq-0.1.1 (c (n "cseq") (v "0.1.1") (d (list (d (n "binout") (r "^0.2") (d #t) (k 0)) (d (n "bitm") (r "^0.3") (d #t) (k 0)) (d (n "dyn_size_of") (r "^0.4") (d #t) (k 0)))) (h "1gd3bisx99071xkilcabznrbvva4lndvwshshh349fzqn6kx9rmc")))

(define-public crate-cseq-0.1.2 (c (n "cseq") (v "0.1.2") (d (list (d (n "binout") (r "^0.2") (d #t) (k 0)) (d (n "bitm") (r "^0.4") (d #t) (k 0)) (d (n "dyn_size_of") (r "^0.4") (d #t) (k 0)))) (h "10wcssbwr40mma8i5q5zpi77izp1k587bkwhykk9m1jpyl9q1axh")))

