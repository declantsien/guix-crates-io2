(define-module (crates-io cs ha csharpbindgen) #:use-module (crates-io))

(define-public crate-csharpbindgen-0.1.0 (c (n "csharpbindgen") (v "0.1.0") (d (list (d (n "insta") (r "^0.8.1") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07ga8643w36x3d07ys8bhlj2n83kgk277b5cnz9i9g61lxgdmjhq")))

