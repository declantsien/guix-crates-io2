(define-module (crates-io cs ha csharp_binder) #:use-module (crates-io))

(define-public crate-csharp_binder-0.1.0 (c (n "csharp_binder") (v "0.1.0") (d (list (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1585yv3lbw5hrkiq93mfqkwyqan1qv1s3q5maikhsj7xh5fpgbk6")))

(define-public crate-csharp_binder-0.1.1 (c (n "csharp_binder") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "03pzd04cnrqimrjf4d907m36vsqjrjm1j0p27wfs8j74hbmy7m8q")))

(define-public crate-csharp_binder-0.1.2 (c (n "csharp_binder") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1ly5wpnbn45hrj191szpyqzxr7vz74iaw39sdasrwkw53px03kxp")))

(define-public crate-csharp_binder-0.1.3 (c (n "csharp_binder") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0yxxvagvjfnafz2m5rb9w0x3yjidch50bs78qf53nag7i2zphsl9")))

(define-public crate-csharp_binder-0.1.4 (c (n "csharp_binder") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "07shb7077b62dwx7rkyh61v6xjf6fd5yhqzcim9jm92y4yh5k28x")))

(define-public crate-csharp_binder-0.1.5 (c (n "csharp_binder") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0wbd4lc87av7a7jx80mbk6af22nwzkbfq4v0rn3xyy0270ivi54s")))

(define-public crate-csharp_binder-0.1.6 (c (n "csharp_binder") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0wxfimygj43jrrd4h5ik6szgmichpzjkz15ymzfsg56k2id8kbn8")))

(define-public crate-csharp_binder-0.1.7 (c (n "csharp_binder") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "15h3d9r285wxjmsmr2z3awpz0n7cnqmn8wq7ywz2skh5i477gf3f")))

(define-public crate-csharp_binder-0.2.0 (c (n "csharp_binder") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0liby09igkcq1kzdngicv62cj4xhaicngw8f8hl59jy4rszjp5f8")))

(define-public crate-csharp_binder-0.2.1 (c (n "csharp_binder") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "09hh0zcv450gmakiwjd4n8kzcrf5b913zggg921aj9cyz3ick7pl")))

(define-public crate-csharp_binder-0.2.2 (c (n "csharp_binder") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "00habsl1x3qyz2jzkycvcmjj126yfhjc5mg5wk26aqy4zakghlh3")))

(define-public crate-csharp_binder-0.3.0 (c (n "csharp_binder") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0s5z9gk045zpx0h01q6xwp0pd41ng7nv96i8yvd8qw4h6324mx63")))

(define-public crate-csharp_binder-0.3.1 (c (n "csharp_binder") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "1vf94fvlqw3ly6q3aabvvfrp3s9qqg0nmrpil246ls235pzqw7cq")))

