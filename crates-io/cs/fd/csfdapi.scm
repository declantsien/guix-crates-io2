(define-module (crates-io cs fd csfdapi) #:use-module (crates-io))

(define-public crate-csfdapi-0.1.0 (c (n "csfdapi") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11.22") (d #t) (k 0)) (d (n "oauth1") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "select") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)) (d (n "web-view") (r "^0.2") (d #t) (k 0)))) (h "0hq90a0i6x2ixl8lcy23r1lrhmdi5d5vkm5zxsg7hhfjl40hwpp0")))

