(define-module (crates-io cs v2 csv2json) #:use-module (crates-io))

(define-public crate-csv2json-0.1.0 (c (n "csv2json") (v "0.1.0") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "csv") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wchn86skn73sy7hpxd1c6f6falbnml41fp8wjxzvzqmcvzzgyff")))

(define-public crate-csv2json-0.1.1 (c (n "csv2json") (v "0.1.1") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "csv") (r "^0.15") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1m76ayx4jsmiy9cfl71sm4gqmw93fkppamz168i9f9x3b84pbync")))

(define-public crate-csv2json-0.2.0 (c (n "csv2json") (v "0.2.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "csv") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)))) (h "0xxrhlrqmc8j877c8vsnx20pbc10n8jkivp97xql096cc7f6xpzx")))

(define-public crate-csv2json-0.3.0 (c (n "csv2json") (v "0.3.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "csv") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)))) (h "1ww0a49fy59gg2js8jnn2gl1qcdpmaw3a818y1vwd15dy53rcm2h")))

(define-public crate-csv2json-0.3.1 (c (n "csv2json") (v "0.3.1") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "csv") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)))) (h "1xdgmpkciz8x0xzjmkwr2f5ip12hxkvq46blqd8m4gpswnhkzbr7")))

