(define-module (crates-io cs v2 csv2json-easy) #:use-module (crates-io))

(define-public crate-csv2json-easy-0.2.1 (c (n "csv2json-easy") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "1kqlg4fi7npwckv4fcisf88zxsggpyrrqbyl56b9qh2jv6knzcfh")))

