(define-module (crates-io cs v2 csv2json-lite) #:use-module (crates-io))

(define-public crate-csv2json-lite-0.1.0 (c (n "csv2json-lite") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.1") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "08ws6kkmfs89srh382c0bjdid037pm3clgrdff75v1khjmmmvvq2")))

