(define-module (crates-io cs v2 csv2ndjson-lite) #:use-module (crates-io))

(define-public crate-csv2ndjson-lite-0.1.0 (c (n "csv2ndjson-lite") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.5") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "00zy6n2xnxpxkz41f97xhd4yjj8z7azhcgm9dp2ykkqqmqa8liz0")))

(define-public crate-csv2ndjson-lite-0.1.1 (c (n "csv2ndjson-lite") (v "0.1.1") (d (list (d (n "csv") (r "^1.1.5") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1hanj6xazr19ixyw1x4kdqy6np1773n6bvxql2hn28m7hjg1p57r")))

(define-public crate-csv2ndjson-lite-0.2.0 (c (n "csv2ndjson-lite") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "csv") (r "^1.1.5") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "016kl012zdc0jd4fzj4vs24mvfbgm9x46rj4cfnpm8xrmppmc39r")))

