(define-module (crates-io cs v2 csv2xls) #:use-module (crates-io))

(define-public crate-csv2xls-0.1.0 (c (n "csv2xls") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "xlsxwriter") (r "^0.3") (d #t) (k 0)))) (h "0q126n7v4j0snc5rhh4hmgndlfvrxp0p5d7439ph3wamqlxgz1x9")))

