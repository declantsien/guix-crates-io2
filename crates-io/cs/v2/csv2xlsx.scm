(define-module (crates-io cs v2 csv2xlsx) #:use-module (crates-io))

(define-public crate-csv2xlsx-0.1.0 (c (n "csv2xlsx") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "simple_excel_writer") (r "^0.2.0") (d #t) (k 0)) (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "05rqkdkz34j0w9mkxmqrawygssj0h53zx6irik02ywkc3dwwm3ni")))

(define-public crate-csv2xlsx-0.1.1 (c (n "csv2xlsx") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "simple_excel_writer") (r "^0.2.0") (d #t) (k 0)) (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "1mkkbq1iw7awkqmsgsn6x23w0wx1l04ia1xv0bsk2ypr6ark2mpi")))

(define-public crate-csv2xlsx-0.2.0 (c (n "csv2xlsx") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "simple_excel_writer") (r "^0.2.0") (d #t) (k 0)) (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "02nd6c1asn1l219sabwccqvpdsf8k9d17rv3k8pc122nyvs3lcll")))

(define-public crate-csv2xlsx-0.3.0 (c (n "csv2xlsx") (v "0.3.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "simple_excel_writer") (r "^0.2.0") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "1a3574j46wabjpamln23shk03h3m8vkzddh979mnh1b68868k761")))

