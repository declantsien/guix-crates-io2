(define-module (crates-io cs v2 csv2arrow) #:use-module (crates-io))

(define-public crate-csv2arrow-0.1.0 (c (n "csv2arrow") (v "0.1.0") (d (list (d (n "arrow") (r "^3.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0msb8yg4skxxbsiz2raksq46lhz39zjbdrf63l9cdbncln4in1bf")))

(define-public crate-csv2arrow-0.1.1 (c (n "csv2arrow") (v "0.1.1") (d (list (d (n "arrow") (r "^3.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00yac0jhyj6irwz1ffbdmylqzwi42z3rphlwf46af94cfkf9crja")))

(define-public crate-csv2arrow-0.1.2 (c (n "csv2arrow") (v "0.1.2") (d (list (d (n "arrow") (r "^3.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09jk123168l11xc9v1djpdag4y9nsy8yfzdmkjpzb7mfn4agas68")))

(define-public crate-csv2arrow-0.1.3 (c (n "csv2arrow") (v "0.1.3") (d (list (d (n "arrow") (r "^3.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x18lgsrj5smrlmqvscjjw9b36ayy653gdfzvbkvn1xa317kqc05")))

(define-public crate-csv2arrow-0.2.0 (c (n "csv2arrow") (v "0.2.0") (d (list (d (n "arrow") (r "^3.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lj0n5rvw0d8fjkb6qd60nqr3c53ix8w1nngawch6jjl5y53gvfz")))

(define-public crate-csv2arrow-0.2.1 (c (n "csv2arrow") (v "0.2.1") (d (list (d (n "arrow") (r "^3.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1c2a80j413y4r9gxlkg0r86x4af1hrlpkly1zlaaamfzl2791ph1")))

(define-public crate-csv2arrow-0.3.0 (c (n "csv2arrow") (v "0.3.0") (d (list (d (n "arrow") (r "^4.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1j8b46vf1pqad4vpwfxjzfw4ajbnh16p2lhaws800s3qgxlnfqjh")))

(define-public crate-csv2arrow-0.3.1 (c (n "csv2arrow") (v "0.3.1") (d (list (d (n "arrow") (r "^9.1.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1c4ncn5aa0xffj0d3rpj8z8hc585niprf9vbyk3hhgzn489l7f6j")))

(define-public crate-csv2arrow-0.4.0 (c (n "csv2arrow") (v "0.4.0") (d (list (d (n "arrow") (r "^13.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "16dm59j8dgi4fm89fczkbg7wjkvahfdhcg8kv2j8hf55xc5is3yi")))

(define-public crate-csv2arrow-0.4.1 (c (n "csv2arrow") (v "0.4.1") (d (list (d (n "arrow") (r "^21.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1ly567nc96yxzxa13lcfdrz2r8sj6nvr84wyn9nsd3rjnsixjxqp")))

(define-public crate-csv2arrow-0.5.0 (c (n "csv2arrow") (v "0.5.0") (d (list (d (n "arrow") (r "^23.0.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "19f9vdm9j9fr2sgf987qwks91k5cl63pr3n51rq4f5z3r3waq0vp")))

(define-public crate-csv2arrow-0.6.0 (c (n "csv2arrow") (v "0.6.0") (d (list (d (n "arrow") (r "^23.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1qisk775vzn0xy7gdw0s0w2rz0zac6hp3qa60rkmh28pnak1n3zf")))

(define-public crate-csv2arrow-0.6.1 (c (n "csv2arrow") (v "0.6.1") (d (list (d (n "arrow") (r "^30.0.1") (d #t) (k 0)) (d (n "arrow-schema") (r "^30.0.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1f47kh2avkfspkpiv4z1nbcpgs7j367855a35vw8kgfx0zifyhi0")))

(define-public crate-csv2arrow-0.7.0 (c (n "csv2arrow") (v "0.7.0") (d (list (d (n "arrow") (r "^32.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^32.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1jriaik806rp6cf0vacwnxi8hv1dfdzwp44s6maxz7lxqgjhka5a")))

(define-public crate-csv2arrow-0.7.1 (c (n "csv2arrow") (v "0.7.1") (d (list (d (n "arrow") (r "^32.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^32.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "135mrnmpgpl083vr5pj1075xi0qj5lir79zxph0wzzkxv5psqc6m")))

(define-public crate-csv2arrow-0.7.2 (c (n "csv2arrow") (v "0.7.2") (d (list (d (n "arrow") (r "^32.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^32.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1jip5av6vbp5xrji86wpxnl5g4lhdc30qmb8n90bjwh8pqcf9rgy")))

(define-public crate-csv2arrow-0.8.0 (c (n "csv2arrow") (v "0.8.0") (d (list (d (n "arrow") (r "^34.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^34.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1rd9valym412pjw55493rhh16ri79wirsmbzmmnn72ilajgp8rx0")))

(define-public crate-csv2arrow-0.8.1 (c (n "csv2arrow") (v "0.8.1") (d (list (d (n "arrow") (r "^34.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^34.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "007w5llr8i8xmisq5hrhg53pl4am1jq8hf2n9n3id1ynmf6wxvpm")))

(define-public crate-csv2arrow-0.10.0 (c (n "csv2arrow") (v "0.10.0") (d (list (d (n "arrow") (r "^36.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^36.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.10.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)))) (h "1vckr99cqr30rd6bc3if5f8mx813bxfkrh91x0qi2fna54xngrng")))

(define-public crate-csv2arrow-0.11.0 (c (n "csv2arrow") (v "0.11.0") (d (list (d (n "arrow") (r "^40.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^40.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1jx3nz5x5rszcd5k88zsfb1lzp9gzhkmz8ykqvpsx7i0xsgr0rwd")))

(define-public crate-csv2arrow-0.12.0 (c (n "csv2arrow") (v "0.12.0") (d (list (d (n "arrow") (r "^44.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^44.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "0yhspbyyyqmsf9ghkcg3fli9l2s7jhk7f3jr5b6vxwlg45zbibfg")))

(define-public crate-csv2arrow-0.12.1 (c (n "csv2arrow") (v "0.12.1") (d (list (d (n "arrow") (r "^44.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^44.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "1a4yirpgf50zfy9swsk4jxfpdrh9ixcs04bk6fxqsd2z95nnk9rx")))

(define-public crate-csv2arrow-0.13.0 (c (n "csv2arrow") (v "0.13.0") (d (list (d (n "arrow") (r "^46.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^46.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "19bm6ig2p7v8wsqg9nqi1kc5m0z83yrj8bjrlkfkmklkw9c54v1q")))

(define-public crate-csv2arrow-0.14.0 (c (n "csv2arrow") (v "0.14.0") (d (list (d (n "arrow") (r "^47.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^47.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.14.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0jh4klylg815isj5amnwrv1hsh7ilikwv5cz6ddykfgizzy9cxg1")))

(define-public crate-csv2arrow-0.14.1 (c (n "csv2arrow") (v "0.14.1") (d (list (d (n "arrow") (r "^47.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^47.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.14.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1hzdlsjdz4a081nr96qw86p5p7swsy4z74j6xg4r55d9qacw37lk")))

(define-public crate-csv2arrow-0.17.0 (c (n "csv2arrow") (v "0.17.0") (d (list (d (n "arrow") (r "^49.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^49.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.17.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "06k6js94v73q12pfbvqi9icgr1v3jsw2mvfsfx0h96bsrw0bqb4l")))

(define-public crate-csv2arrow-0.17.1 (c (n "csv2arrow") (v "0.17.1") (d (list (d (n "arrow") (r "^49.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^49.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.17.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1y0k7wj75mif41lszsiawgfj0j310gxblb0x6ipj3a7a1l9l0cgk")))

(define-public crate-csv2arrow-0.17.2 (c (n "csv2arrow") (v "0.17.2") (d (list (d (n "arrow") (r "^49.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^49.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.17.2") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0nc4dpxhcngrfz4fffin2qhjz05z1fxpw7pbkh7n20gvw104q8ia")))

(define-public crate-csv2arrow-0.17.3 (c (n "csv2arrow") (v "0.17.3") (d (list (d (n "arrow") (r "^49.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^49.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.17.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "06jngr9ylw1a8dh6ncqy15yfxxs985hkjcnjn7v6m5ibhmdbqv57")))

(define-public crate-csv2arrow-0.17.4 (c (n "csv2arrow") (v "0.17.4") (d (list (d (n "arrow") (r "^49.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^49.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.17.4") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1fl3jcmxarp4qsaispv0wsf9k5ingbmj7j4jvjiyy3bk9iswnzsb")))

(define-public crate-csv2arrow-0.17.5 (c (n "csv2arrow") (v "0.17.5") (d (list (d (n "arrow") (r "^49.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^49.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.17.5") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "01fqsb6zxcbzkh9lklv6h8gd1cacrc3cq1pmshh92lf2j20qs2ns")))

(define-public crate-csv2arrow-0.17.7 (c (n "csv2arrow") (v "0.17.7") (d (list (d (n "arrow") (r "^49.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^49.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.17.7") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "1b779ymv9jaa0xjzs52k3n8lfgwls0x26q9mjwghx3v56fp1mhkr")))

(define-public crate-csv2arrow-0.17.8 (c (n "csv2arrow") (v "0.17.8") (d (list (d (n "arrow") (r "^49.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^49.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.17.8") (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)))) (h "0xip5nmxch08khrafsgar3lqppjgs4rqizk9cpy1whq8jmn8hmln")))

(define-public crate-csv2arrow-0.17.9 (c (n "csv2arrow") (v "0.17.9") (d (list (d (n "arrow") (r "^50.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^50.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.17.9") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "1db0mlahyb17nfjai24pjgc0hbl9s9d63xv8ww20ill42chpryb0")))

(define-public crate-csv2arrow-0.17.10 (c (n "csv2arrow") (v "0.17.10") (d (list (d (n "arrow") (r "^50.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^50.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.17.10") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "126y0zv3hkimrg0lfhigbib89hmgkc718hskpi91p60gg0hfy49s")))

(define-public crate-csv2arrow-0.18.0 (c (n "csv2arrow") (v "0.18.0") (d (list (d (n "arrow") (r "^51.0.0") (d #t) (k 0)) (d (n "arrow-schema") (r "^51.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "arrow-tools") (r "^0.18.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "1yj2skawdx2sbx4ycrh3ccny735ibdv1imz2kvrq6xlw6qqr78lm")))

