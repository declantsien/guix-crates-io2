(define-module (crates-io cs v2 csv2struct) #:use-module (crates-io))

(define-public crate-csv2struct-0.1.0 (c (n "csv2struct") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 0)))) (h "0gk82dpxmk65zmcs3wv8yili9nnn5pw5jlawbg6vpmc6sl2406mn")))

