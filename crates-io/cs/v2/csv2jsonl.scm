(define-module (crates-io cs v2 csv2jsonl) #:use-module (crates-io))

(define-public crate-csv2jsonl-0.3.0 (c (n "csv2jsonl") (v "0.3.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "encoding_rs_io") (r "^0.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.7") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "180qnq2mg3vx1vfnj09axip21pfnhdx5fnzjksyf4i1j2yr9z1s5")))

