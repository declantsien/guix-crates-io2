(define-module (crates-io cs cl csclib) #:use-module (crates-io))

(define-public crate-csclib-1.0.0 (c (n "csclib") (v "1.0.0") (h "0cqg1dg01ph1530xqsv6280di8yx9lrxjmjinpcjls4b36p61k03") (y #t)))

(define-public crate-csclib-1.0.1 (c (n "csclib") (v "1.0.1") (h "0l3i4rqanbjwvs3id15h7lnxqy0xjn91qmjr16ffzlwr09mr235v")))

