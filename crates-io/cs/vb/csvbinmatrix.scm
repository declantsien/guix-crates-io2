(define-module (crates-io cs vb csvbinmatrix) #:use-module (crates-io))

(define-public crate-csvbinmatrix-0.1.1 (c (n "csvbinmatrix") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "rstest") (r "^0.19") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0sbaypkayl4mz8m0gnwx1g27f9vls403yx88ikcic8809lin1lbk")))

(define-public crate-csvbinmatrix-0.2.0 (c (n "csvbinmatrix") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "rstest") (r "^0.19") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "16pj3lzzii6dmx5mkq24l1pdlmzsvbli1ss1s45jiyf0dg5mhnki")))

(define-public crate-csvbinmatrix-0.2.1 (c (n "csvbinmatrix") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "rstest") (r "^0.19") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "04xd4d1xpxmak3792q16m1hd5r7bivr4030j7p7synhxf2h55xk2")))

(define-public crate-csvbinmatrix-0.3.0 (c (n "csvbinmatrix") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "rstest") (r "^0.19") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "07c3chc08yvx346bfip4w28alxkb9i6xqh114qb47cygwl4iiicp")))

(define-public crate-csvbinmatrix-0.4.0 (c (n "csvbinmatrix") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "rstest") (r "^0.19") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "08sic50rwqn6y33khq7w6w6jxcjlz2hxm4wziddmwawqs8gkf276") (r "1.60.0")))

(define-public crate-csvbinmatrix-0.5.0 (c (n "csvbinmatrix") (v "0.5.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "rstest") (r "^0.19") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0bbi43yh3d2mx75q81d4hi2avmg6v161lcw3pjnr60nbm4g2sxh2") (r "1.60.0")))

(define-public crate-csvbinmatrix-0.5.1 (c (n "csvbinmatrix") (v "0.5.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "rstest") (r "^0.19") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "046vf8d83yrlz280m51qsf325pr24jx6dgn7hfq4xz9srv71m2rm") (r "1.60.0")))

(define-public crate-csvbinmatrix-0.6.0 (c (n "csvbinmatrix") (v "0.6.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "rstest") (r "^0.19") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1s2n5ixpl9k1k19hqcmpwqkg7zkg0g7j4vi1gijlis21nprx4fi3") (r "1.60.0")))

(define-public crate-csvbinmatrix-0.7.0 (c (n "csvbinmatrix") (v "0.7.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "rstest") (r "^0.19") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05qj2bzflqigxnxjjx9qqm1xd846ikvh90cn3158xj3mcv57yvma") (r "1.60.0")))

