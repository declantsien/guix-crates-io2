(define-module (crates-io cs pr cspretty) #:use-module (crates-io))

(define-public crate-cspretty-0.1.0 (c (n "cspretty") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "182slzlcd925489zwabzj8x1fzxbpf5pp6vc6b32s6h9zg7bha3q")))

(define-public crate-cspretty-0.1.1 (c (n "cspretty") (v "0.1.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0in2svhwkvn2lxyzznsbwy865d3mh7jk2np62ldzaidmyyyqv926")))

(define-public crate-cspretty-0.1.2 (c (n "cspretty") (v "0.1.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0jprnlw1aw6kgzz1qr26nhzmvrkrvrl7widwl9bjalm32b7xrgwd")))

(define-public crate-cspretty-0.1.3 (c (n "cspretty") (v "0.1.3") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1dygrc5j4r7m87h1l80cl45qvx681f36ijawrqndzx049jz017y8")))

