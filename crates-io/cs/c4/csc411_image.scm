(define-module (crates-io cs c4 csc411_image) #:use-module (crates-io))

(define-public crate-csc411_image-0.1.0 (c (n "csc411_image") (v "0.1.0") (d (list (d (n "image") (r "^0.23.0") (d #t) (k 0)))) (h "08i22w847n4hx32my4hba0sg99rahzxh3qdpmanriw5bab1kl0i8")))

(define-public crate-csc411_image-0.2.0 (c (n "csc411_image") (v "0.2.0") (d (list (d (n "image") (r "^0.23.0") (d #t) (k 0)))) (h "1n1g7p4mcjz0fd3mvdg6azg1kzirq2zxy5g78c5ssq3rgn1vd6ic")))

(define-public crate-csc411_image-0.2.1 (c (n "csc411_image") (v "0.2.1") (d (list (d (n "image") (r "^0.23.0") (d #t) (k 0)))) (h "17dlam0xq9a20xd2061hf7xsrjl9ih5wfpfgwxdxfcczd00dr5mk")))

(define-public crate-csc411_image-0.2.2 (c (n "csc411_image") (v "0.2.2") (d (list (d (n "image") (r "^0.23.0") (d #t) (k 0)))) (h "0wnswc2qij3vhdl45ds724dylxx8wala0mrm7ijbgppyjx1pmfgj")))

(define-public crate-csc411_image-0.2.3 (c (n "csc411_image") (v "0.2.3") (d (list (d (n "image") (r "^0.23.0") (d #t) (k 0)))) (h "1m35cxza553mfs42gnykid8csyn5zs5w57c8xx47bfxr40r4wcvk")))

(define-public crate-csc411_image-0.3.0 (c (n "csc411_image") (v "0.3.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "0bnn24s3jq5176h3x4jmr6p6rx8g1289m7v5l1h2hf9jly5ry218")))

(define-public crate-csc411_image-0.3.1 (c (n "csc411_image") (v "0.3.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "17c93sk1kzhfqlz8apx31y9ywja5pdvfjb30f2i7g74xaifb6spp")))

(define-public crate-csc411_image-0.3.2 (c (n "csc411_image") (v "0.3.2") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "194sq1rdfcp211czf4067sli2z2dg9317l2p176lqqgib9x6yfbm")))

(define-public crate-csc411_image-0.3.3 (c (n "csc411_image") (v "0.3.3") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "17wsmzbvnzzqp4hfi1c3cl9cplw97rxpgb16n1qwmd7hhqzd9rfa")))

(define-public crate-csc411_image-0.3.4 (c (n "csc411_image") (v "0.3.4") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "0ng7z4hx6k8vc9v71b9gwn30yhma4phkqf5xr3x1ncwi06qih8vv")))

(define-public crate-csc411_image-0.4.0 (c (n "csc411_image") (v "0.4.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)))) (h "1j5j38dlg4rc21pdnw7mygs05zdyqmn41nmhkn7jx32jfx0yn61c")))

(define-public crate-csc411_image-0.5.0 (c (n "csc411_image") (v "0.5.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)))) (h "0z8i2xxp5sz28ajn1pz1bjvgd8wy3asfwxkw5vzh3563p3xsqnsi")))

(define-public crate-csc411_image-0.5.1 (c (n "csc411_image") (v "0.5.1") (d (list (d (n "image") (r "^0.24.7") (d #t) (k 0)))) (h "184j09wi1wkzzsxqwgsv0m4ah1lmz70xg6gv9xdz92g887h6wzwr")))

(define-public crate-csc411_image-0.5.2 (c (n "csc411_image") (v "0.5.2") (d (list (d (n "image") (r "=0.24.7") (d #t) (k 0)))) (h "1iqi2wr7i0gbr5gnxsb0s0i7pp2zgz8slrgxk7cdivm8s1agak6v")))

