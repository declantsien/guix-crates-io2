(define-module (crates-io cs c4 csc411_rpegio) #:use-module (crates-io))

(define-public crate-csc411_rpegio-0.1.0 (c (n "csc411_rpegio") (v "0.1.0") (h "1inas32l1ji0kg28p2n1i7gl4lv0299qfapp5l5szxgbqf21fmzh")))

(define-public crate-csc411_rpegio-0.2.0 (c (n "csc411_rpegio") (v "0.2.0") (h "13kkb4r2ddaf8z09b21w53aiymdn8iahllfm2020vs57lj34ilnh")))

(define-public crate-csc411_rpegio-0.3.0 (c (n "csc411_rpegio") (v "0.3.0") (h "1fj5x5zndg10y8rffmrv0d4ffy932razlvf1y81mbg3l1jjsyv8m")))

(define-public crate-csc411_rpegio-0.3.1 (c (n "csc411_rpegio") (v "0.3.1") (h "1pq1h347ps588vbhcp9viv69bkpds08nc639siijkvwvkc72sx6g")))

(define-public crate-csc411_rpegio-0.4.0 (c (n "csc411_rpegio") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0ssg0iq9k7vpwsiq7cs0r5acvkapdddwgwnkp0p1gzfkq698ixn3")))

