(define-module (crates-io cs rs csrs) #:use-module (crates-io))

(define-public crate-csrs-0.1.0 (c (n "csrs") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "00mypd7j3zvad4azmk9481v4wn20n1jkniplm374jpx824fswjds")))

(define-public crate-csrs-0.1.1-dev (c (n "csrs") (v "0.1.1-dev") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1sb11qiklprwbwd3f8fikxxcy2k6amk9aaa3nmsgxr366lqr91f3")))

(define-public crate-csrs-0.1.2-dev (c (n "csrs") (v "0.1.2-dev") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "0lc02asfzw8zx9ryi3q09phps4kx0k2bxgv6dz5xvnlr0ivqds21")))

