(define-module (crates-io cs d- csd-rs) #:use-module (crates-io))

(define-public crate-csd-rs-0.1.0 (c (n "csd-rs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0w6abwwkzlsgxsxn9h9bfdixsj4sc7awbwsr2wcw2daj1awkkm1k")))

