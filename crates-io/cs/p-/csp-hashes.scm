(define-module (crates-io cs p- csp-hashes) #:use-module (crates-io))

(define-public crate-csp-hashes-0.0.1 (c (n "csp-hashes") (v "0.0.1") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "scraper") (r "^0.16.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "01lrgnkwm2zn86iad0jyfq3ai6l5jd5i01ag5frljrnqbd4x87rb")))

(define-public crate-csp-hashes-0.0.2 (c (n "csp-hashes") (v "0.0.2") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "scraper") (r "^0.16.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1yszzskhc7mlv1aj2lgss41s551254d43bx6cwmqf17i7482bjva")))

(define-public crate-csp-hashes-0.0.3 (c (n "csp-hashes") (v "0.0.3") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "scraper") (r "^0.16.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "1cds1h4i7bfqg69zb4ihn2q8j2r85plfp5wnz06djxzww6igy4np")))

(define-public crate-csp-hashes-0.0.4 (c (n "csp-hashes") (v "0.0.4") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "scraper") (r "^0.16.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "0wmks52ach6j35cfiqhynypmhmnli3hwnsg4fdjdky2i63kzg9gq")))

