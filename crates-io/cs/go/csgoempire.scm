(define-module (crates-io cs go csgoempire) #:use-module (crates-io))

(define-public crate-csgoempire-0.1.0 (c (n "csgoempire") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1b6fd8z0mmbdp9qx2iixdgi0yjjj2ppbsx7izmk7p4nhczh4zx8x")))

