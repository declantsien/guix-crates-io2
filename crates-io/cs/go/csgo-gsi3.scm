(define-module (crates-io cs go csgo-gsi3) #:use-module (crates-io))

(define-public crate-csgo-gsi3-0.3.6 (c (n "csgo-gsi3") (v "0.3.6") (h "076g7yr89a787f8hmjv4yl5gaz40khw80fzxdlzj12ihr13b3azy") (y #t)))

(define-public crate-csgo-gsi3-0.3.7 (c (n "csgo-gsi3") (v "0.3.7") (h "0m3fkx1mn0bmskzwmpylxw30y0hbrlxxc45j6p2fqw851ld6shcm")))

