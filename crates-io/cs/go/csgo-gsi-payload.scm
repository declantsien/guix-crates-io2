(define-module (crates-io cs go csgo-gsi-payload) #:use-module (crates-io))

(define-public crate-csgo-gsi-payload-0.1.0 (c (n "csgo-gsi-payload") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_with") (r "^2.1.0") (d #t) (k 0)))) (h "03yir8iz1b7af1hfvzr6b7q9wi11i000mjddr6gbzqc4p8yrwdqd")))

(define-public crate-csgo-gsi-payload-0.1.1 (c (n "csgo-gsi-payload") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "serde_with") (r "^2.1.0") (d #t) (k 0)))) (h "0z7fcmfkvkrp8nyfrskp5jvhzmxmbwcj3a7xyz2xza792fzz9bm0")))

