(define-module (crates-io cs #{50}# cs50) #:use-module (crates-io))

(define-public crate-cs50-1.0.0 (c (n "cs50") (v "1.0.0") (h "04hg19k2h7cn427wlz5q50hkl5ch586j3z8bl36vvxqd50cxz8mm")))

(define-public crate-cs50-1.0.1 (c (n "cs50") (v "1.0.1") (h "1l5281qxqvanbkyjpra9901zvk0wgys2lj7aliayc8hhajrmzqrm")))

