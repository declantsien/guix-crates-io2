(define-module (crates-io cs in csinsertion_sort) #:use-module (crates-io))

(define-public crate-csinsertion_sort-0.1.0 (c (n "csinsertion_sort") (v "0.1.0") (h "1b9qzqbp01pwf73g8cj3yqv86220j60r38yk3n6872m3469vc8s0") (y #t)))

(define-public crate-csinsertion_sort-0.1.1 (c (n "csinsertion_sort") (v "0.1.1") (h "0mf9sshi9dxmjzidfwpp51dfkrrhi6dirycb528h834pxjlm1yfm") (y #t)))

(define-public crate-csinsertion_sort-0.1.3 (c (n "csinsertion_sort") (v "0.1.3") (h "1kh4449dlx4gdjasi13vs6vwf4xg4mglw0m93vzfwwhd23jy35xx")))

