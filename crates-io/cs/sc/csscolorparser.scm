(define-module (crates-io cs sc csscolorparser) #:use-module (crates-io))

(define-public crate-csscolorparser-0.1.0 (c (n "csscolorparser") (v "0.1.0") (h "1l7sb4b33j2yl3ckxn7zh3ab5grv45skan8cvdwxvhd90nk4v5mp")))

(define-public crate-csscolorparser-0.2.0 (c (n "csscolorparser") (v "0.2.0") (d (list (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "0gasqghwn46gygziw9g56yz3cdhdm30r0kqyx12yqmklknys14bm")))

(define-public crate-csscolorparser-0.3.0 (c (n "csscolorparser") (v "0.3.0") (d (list (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "1fp10nc52nbr85za6m08qvlf1hrqg4nmmd3dp28fzlhaziqb7mk1")))

(define-public crate-csscolorparser-0.4.0 (c (n "csscolorparser") (v "0.4.0") (d (list (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "0f76ar47xkcirflxmhz0xsg34yy2gpqbk5h0dw3m6vbdn8apnzdl")))

(define-public crate-csscolorparser-0.5.0 (c (n "csscolorparser") (v "0.5.0") (d (list (d (n "cint") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.25") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1ghzs8i852slblyh6c52z17wc6pl50005y5rspim69gk7vckpyxj") (f (quote (("rust-rgb" "rgb") ("named-colors" "phf") ("default" "named-colors"))))))

(define-public crate-csscolorparser-0.6.0 (c (n "csscolorparser") (v "0.6.0") (d (list (d (n "cint") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "lab") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.32") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.137") (d #t) (k 2)))) (h "0gh5v9i2mq5dd6ivqr6lb8061fd0ikzcvdgbksw99qsk2nqpa544") (f (quote (("rust-rgb" "rgb") ("named-colors" "phf") ("default" "named-colors"))))))

(define-public crate-csscolorparser-0.6.1 (c (n "csscolorparser") (v "0.6.1") (d (list (d (n "cint") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "lab") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.33") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.139") (d #t) (k 2)))) (h "1by8lb9mi2wa2qcqzhpx7lab6k6hf5a1xs3fai7wmvd79dv2baw0") (f (quote (("rust-rgb" "rgb") ("named-colors" "phf") ("default" "named-colors"))))))

(define-public crate-csscolorparser-0.6.2 (c (n "csscolorparser") (v "0.6.2") (d (list (d (n "cint") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "lab") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.11.0") (f (quote ("macros"))) (o #t) (d #t) (k 0)) (d (n "rgb") (r "^0.8.33") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.139") (d #t) (k 2)))) (h "1gxh11hajx96mf5sd0az6mfsxdryfqvcfcphny3yfbfscqq7sapb") (f (quote (("rust-rgb" "rgb") ("named-colors" "phf") ("default" "named-colors"))))))

