(define-module (crates-io cs -d cs-datetime-parse) #:use-module (crates-io))

(define-public crate-cs-datetime-parse-1.0.0 (c (n "cs-datetime-parse") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "time") (r "^0.3.34") (f (quote ("macros" "local-offset"))) (d #t) (k 0)))) (h "0vwc1wi29z3qjh76llc9vdaqd0g0a1801zhs8sj0vs2wmk9hrv8b") (s 2) (e (quote (("serde" "dep:serde" "time/serde"))))))

(define-public crate-cs-datetime-parse-1.1.0 (c (n "cs-datetime-parse") (v "1.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "time") (r "^0.3.34") (f (quote ("macros" "local-offset"))) (d #t) (k 0)))) (h "1042gmsl2d3wr0xbj75iq6hjin73df2whg6sd5369p8bh0rz5xmb") (s 2) (e (quote (("serde" "dep:serde" "time/serde"))))))

