(define-module (crates-io cs ta csta) #:use-module (crates-io))

(define-public crate-csta-0.1.0 (c (n "csta") (v "0.1.0") (d (list (d (n "plotters") (r "^0.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0db4k7yj7l40lnvrrixxvbv5074f2pgz2k98vfc6fzkfngjxw1hq")))

(define-public crate-csta-0.1.1 (c (n "csta") (v "0.1.1") (d (list (d (n "plotters") (r "^0.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "098yjranpgg1f8hcl0m6qd72n4crsl0f7ab5bdl7ds7xp7rnlka2")))

(define-public crate-csta-0.2.0 (c (n "csta") (v "0.2.0") (d (list (d (n "plotters") (r "^0.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zj183qs9z6zb6cj0a71rga0yndb2gn7l33gwhbj8dh3z19dp4cm")))

(define-public crate-csta-0.3.0 (c (n "csta") (v "0.3.0") (d (list (d (n "plotters") (r "^0.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ga5xfs3vqfs7i4vb5mqa86i5v269y53hi3qqzwxb5kl8lgqkzli")))

(define-public crate-csta-0.4.0 (c (n "csta") (v "0.4.0") (d (list (d (n "plotters") (r "^0.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1flqmqrws51hqly3j63nkwkj5wchcbc569zkh7w7ghnwwg8aln1z")))

(define-public crate-csta-0.5.0 (c (n "csta") (v "0.5.0") (d (list (d (n "piston_window") (r "^0.112") (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)) (d (n "plotters-piston") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1dip136f7vgdbaiqckggsd3jp3rj03qpizblmrvvwbdmd76imcrd")))

(define-public crate-csta-0.6.0 (c (n "csta") (v "0.6.0") (d (list (d (n "plotters") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0xsdhs9q8nxpfwl4m0mwg56jdqb9mgb08pgy1jmgcy5sakg0v3bm")))

(define-public crate-csta-0.7.0 (c (n "csta") (v "0.7.0") (d (list (d (n "plotters") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_arrays") (r "^0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18i2zqj2bgrimb2p64c6b67qli8azz2l1n2qvkbcbjrh53w3ll1l")))

(define-public crate-csta-1.0.0 (c (n "csta") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0qx10wxgc941sdghcm560vr8xpad8kgs5wr5vr7mfjqq3f434ww2") (f (quote (("default"))))))

(define-public crate-csta-1.0.1 (c (n "csta") (v "1.0.1") (d (list (d (n "csta_derive") (r "=1.0.0") (d #t) (t "cfg(any())") (k 0)) (d (n "csta_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0dcvy4as3k94niq9lgpvfi938i0hmps6ifsy7fmb1j88p4q3m10r") (f (quote (("derive" "csta_derive") ("default"))))))

