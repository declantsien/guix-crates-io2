(define-module (crates-io cs ta cstars) #:use-module (crates-io))

(define-public crate-cstars-0.1.0 (c (n "cstars") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0.0") (d #t) (k 0)) (d (n "lib_cstars") (r "^0.1.0") (d #t) (k 0)))) (h "197nshf1yh35fvlxksj0fvnagcmi5jnwrz4fp3ypj9afdsslzf08")))

