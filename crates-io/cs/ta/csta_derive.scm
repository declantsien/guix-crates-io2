(define-module (crates-io cs ta csta_derive) #:use-module (crates-io))

(define-public crate-csta_derive-1.0.0 (c (n "csta_derive") (v "1.0.0") (d (list (d (n "csta") (r "^1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0syflpcpmla62i1zs6rk14fc9kjwvcl24a0ix0f6qpnk7kcmgrz6") (f (quote (("default"))))))

