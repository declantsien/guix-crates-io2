(define-module (crates-io cs f_ csf_benchmark) #:use-module (crates-io))

(define-public crate-csf_benchmark-0.1.0 (c (n "csf_benchmark") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csf") (r "^0.1") (d #t) (k 0)) (d (n "fsum") (r "^0.1") (d #t) (k 0)) (d (n "ph") (r "^0.7") (f (quote ("wyhash"))) (d #t) (k 0)))) (h "14k15d0h3r7dj11kczgvayp624g5plbx3wv9m44681912pafl1k3")))

(define-public crate-csf_benchmark-0.1.1 (c (n "csf_benchmark") (v "0.1.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csf") (r "^0.1") (d #t) (k 0)) (d (n "ph") (r "^0.7") (f (quote ("wyhash"))) (d #t) (k 0)))) (h "1fqlbrwll5mi4zmf601qlg94lsy8sf40gjfxg2khfx3f7k2l8g10")))

(define-public crate-csf_benchmark-0.1.2 (c (n "csf_benchmark") (v "0.1.2") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csf") (r "^0.1") (d #t) (k 0)) (d (n "ph") (r "^0.7") (f (quote ("wyhash"))) (d #t) (k 0)))) (h "0k2lmaj7l120rcyhknbmilf7lxd0q053kll93fdijfzccvbn1ydg")))

(define-public crate-csf_benchmark-0.1.3 (c (n "csf_benchmark") (v "0.1.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csf") (r "^0.1") (d #t) (k 0)) (d (n "ph") (r "^0.8") (f (quote ("wyhash"))) (d #t) (k 0)))) (h "06k1pdkfz7j1dqdh1rw3wx4g19j9hw8j0snabl1zp9dj434x4337")))

(define-public crate-csf_benchmark-0.1.4 (c (n "csf_benchmark") (v "0.1.4") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csf") (r "^0.1") (d #t) (k 0)) (d (n "ph") (r "^0.8") (f (quote ("wyhash"))) (d #t) (k 0)))) (h "1jslw71whkhh6vrk2pxrvx5pzpkwgqdf0n3v4nvlldqlqy5lmmll")))

(define-public crate-csf_benchmark-0.1.5 (c (n "csf_benchmark") (v "0.1.5") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csf") (r "^0.1") (d #t) (k 0)) (d (n "ph") (r "^0.8") (f (quote ("wyhash"))) (d #t) (k 0)))) (h "0k6vjgq9ivr6l5z8r07nqiphkgj2j84f0wwx1qhkq4iid5nf7xy2")))

