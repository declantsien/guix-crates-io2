(define-module (crates-io cs bi csbindgen) #:use-module (crates-io))

(define-public crate-csbindgen-0.1.0 (c (n "csbindgen") (v "0.1.0") (d (list (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "092kzbw6pr5b9ybj275p2pjjmb8v09vi9xfmyiiy5a7z0xwfyvc4")))

(define-public crate-csbindgen-0.1.1 (c (n "csbindgen") (v "0.1.1") (d (list (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1qbv71dcf2mmi1hw3772sxcs02phqldbyiapdfxdlv4a5ypwwfv3")))

(define-public crate-csbindgen-0.1.2 (c (n "csbindgen") (v "0.1.2") (d (list (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "12w85zwicjfdmif83d34dr5xdkdybpg2cdgdbkiq7d0645dsd7yw")))

(define-public crate-csbindgen-1.0.0 (c (n "csbindgen") (v "1.0.0") (d (list (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0lbksrk6br85jxs4g9xp76h08yzdffixglsfwiam2wisxp4j9gyx")))

(define-public crate-csbindgen-1.1.0 (c (n "csbindgen") (v "1.1.0") (d (list (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "09dih52wpbrbizhvnnw6bf5zipjfg1j5gf09m561wzk1mlhqji4g")))

(define-public crate-csbindgen-1.2.0 (c (n "csbindgen") (v "1.2.0") (d (list (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1fmrhcixbvaqs0rrzbbbvw3g8adjqzyyxiadx6r63v5jpqlg9n3x")))

(define-public crate-csbindgen-1.3.0 (c (n "csbindgen") (v "1.3.0") (d (list (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1qd0pnghpim1vss0nkw0gpks63d98crnqnfhas33b1j9ryxqi5wc")))

(define-public crate-csbindgen-1.4.0 (c (n "csbindgen") (v "1.4.0") (d (list (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0bml5c03gpnjvnkc5h6p2nd7crrnq54fyv2sabmgkyzax5lg75s6")))

(define-public crate-csbindgen-1.5.0 (c (n "csbindgen") (v "1.5.0") (d (list (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "080hpis1j6c7198jgr1mlx5j7gzw0xy59drwvczlylm0g6sllffm")))

(define-public crate-csbindgen-1.6.0 (c (n "csbindgen") (v "1.6.0") (d (list (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1xrcz83srp6vqc35zsi6k6ikndscb63ha4rds2aylcb590c3siiq")))

(define-public crate-csbindgen-1.7.0 (c (n "csbindgen") (v "1.7.0") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0ikzzhwwv8y9hd4nix73yi2rjf81dlni2d55nky688q868dd7zpj")))

(define-public crate-csbindgen-1.7.1 (c (n "csbindgen") (v "1.7.1") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1l4z1afdi5hfv6kaav1cqk81n3zjc50rqp3khb9s1jfvsm6ig42h")))

(define-public crate-csbindgen-1.7.2 (c (n "csbindgen") (v "1.7.2") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1n2v45rrh9n1g52kci4zwaagplvckp771yd3fdcrm2fkl679zjhn")))

(define-public crate-csbindgen-1.7.3 (c (n "csbindgen") (v "1.7.3") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "15l10l2w85fv5qbvqanf82i3d0241wc7j00iqs9x10lk2fksdpy0")))

(define-public crate-csbindgen-1.7.4 (c (n "csbindgen") (v "1.7.4") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "1v71vqd9abjmsvarhlisg8g9030558jnjxdgzl7dq56dpgfdghqc")))

(define-public crate-csbindgen-1.7.5 (c (n "csbindgen") (v "1.7.5") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "097y8waq725hbiad28i3bsl1g4g4aqrv8439dqc15vb66nx3jlib")))

(define-public crate-csbindgen-1.8.0 (c (n "csbindgen") (v "1.8.0") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0i7mmnavhccjmlzwv6mq636ys332npxx7r36xygpfi6jila8jhzc")))

(define-public crate-csbindgen-1.8.1 (c (n "csbindgen") (v "1.8.1") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0l53h8abhsmwfw4k7k3a3j357kb0y1vx34fiz7b3i22mbnni2y2a")))

(define-public crate-csbindgen-1.8.2 (c (n "csbindgen") (v "1.8.2") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "0qa9j5jrfqkfc0ma1gghlsqd991rifwq580i50nfj9f7iyqv2iw7")))

(define-public crate-csbindgen-1.8.3 (c (n "csbindgen") (v "1.8.3") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "09wvh3zlqinvrdi8qyimvyqpx6s8faqf44kml59lgpkaw992fbyz")))

(define-public crate-csbindgen-1.9.0 (c (n "csbindgen") (v "1.9.0") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "11dkszhpmi0r01i5cz4x77601p4m02an1bha1z91kq9mmrnp4j68")))

(define-public crate-csbindgen-1.9.1 (c (n "csbindgen") (v "1.9.1") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "173sfqjibiwrj1qqjd428sjxhlqw8dk1rqxxdjaydq1mdxjynw6g")))

