(define-module (crates-io cs bi csbinding_generator) #:use-module (crates-io))

(define-public crate-csbinding_generator-0.5.0 (c (n "csbinding_generator") (v "0.5.0") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csharp_binder") (r "^0.3.1") (d #t) (k 0)))) (h "1djbl3gdba22gb88af9831y6nqxaapdj8m5gy4jx1h4dmlz369l6")))

