(define-module (crates-io cs vt csvtosql-core) #:use-module (crates-io))

(define-public crate-csvtosql-core-0.1.0 (c (n "csvtosql-core") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "0dd2mrnv7gv0djxlp5jr2j3f1ih6rvqh51dpmfsd43xfgzr44sxj")))

(define-public crate-csvtosql-core-0.2.0 (c (n "csvtosql-core") (v "0.2.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "09fla8bjcn6ngj0g50nsml31fr7g6bhynrzkcj2235mlhnh207ki")))

(define-public crate-csvtosql-core-0.2.1 (c (n "csvtosql-core") (v "0.2.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)))) (h "0g4826vil01kqqi6k2654h3dg1w4cd2srw49gawanb56fhjwkli7")))

