(define-module (crates-io cs vt csvtool) #:use-module (crates-io))

(define-public crate-csvtool-0.0.1 (c (n "csvtool") (v "0.0.1") (h "1pvcvl73gch0sax1d4q7mkh4ipmwhhb4wi7k2ckv4ydxrw7z3vl2")))

(define-public crate-csvtool-0.0.2 (c (n "csvtool") (v "0.0.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "1gbcbc3adp3lip6n8qrdg0z2gf5822mvcj0d1kcncjs59xhwpf6a")))

