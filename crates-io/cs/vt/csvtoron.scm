(define-module (crates-io cs vt csvtoron) #:use-module (crates-io))

(define-public crate-csvtoron-0.2.0 (c (n "csvtoron") (v "0.2.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jwpywgzschq5l19zpsxz5zfbf5mab93lcwapcam8jk2fh0pzzvd")))

