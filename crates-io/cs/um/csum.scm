(define-module (crates-io cs um csum) #:use-module (crates-io))

(define-public crate-csum-0.1.0 (c (n "csum") (v "0.1.0") (d (list (d (n "checksums") (r "=0.9.1") (d #t) (k 0)) (d (n "clap") (r "=3.2.22") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "08l2j4gq6aadpdmpvjgvk3zxbv1pgh4mnc2v5rs0jsnfm81zmbfc") (r "1.63")))

(define-public crate-csum-0.1.1 (c (n "csum") (v "0.1.1") (d (list (d (n "checksums") (r "=0.9.1") (d #t) (k 0)) (d (n "clap") (r "=3.2.22") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "11fh2vzjx2lwmkixf8yvc5hmnnh4pv5shfg6n8w5m974shmndmlk") (r "1.63.0")))

(define-public crate-csum-0.2.0 (c (n "csum") (v "0.2.0") (d (list (d (n "checksums") (r "=0.9.1") (d #t) (k 0)) (d (n "clap") (r "=3.2.22") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "00xw534r5kjspkbjgpig5yxx0551q6jgkn73bb4kj0cmj8whx8fy") (r "1.63.0")))

(define-public crate-csum-0.2.1 (c (n "csum") (v "0.2.1") (d (list (d (n "checksums") (r "=0.9.1") (d #t) (k 0)) (d (n "clap") (r "=3.2.22") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1496pvnh80ma65msllmfkxxx1wiacnb9ga9dw6gim9rrkb1fgibw") (r "1.63.0")))

