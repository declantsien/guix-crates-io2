(define-module (crates-io cs ap csaparser) #:use-module (crates-io))

(define-public crate-csaparser-0.3.0 (c (n "csaparser") (v "0.3.0") (d (list (d (n "usiagent") (r "^0.6.1") (d #t) (k 0)))) (h "1dxdyfwi0rij70fj79yczyfz9hvhijpxsvmh7772304yhvpp90ss")))

(define-public crate-csaparser-0.3.1 (c (n "csaparser") (v "0.3.1") (d (list (d (n "usiagent") (r "^0.6.3") (d #t) (k 0)))) (h "0p715b9xvjfk82bljrkngrrs6gc7mvfjajpqvq54m3wda17c0lrf")))

