(define-module (crates-io cs et cset-derive) #:use-module (crates-io))

(define-public crate-cset-derive-0.1.0 (c (n "cset-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1jfh7sxfzn5qmjkswljgyhf0jqrbasws12378m9yawg69d5al5z9")))

(define-public crate-cset-derive-0.1.1 (c (n "cset-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0kj576ad7najsa9xqaq7hhdl8m8qpwiykcdqx1ki8x19xzi99dh1")))

