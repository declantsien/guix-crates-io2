(define-module (crates-io cs et cset) #:use-module (crates-io))

(define-public crate-cset-0.1.0 (c (n "cset") (v "0.1.0") (d (list (d (n "cset-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1hfy31jwsgs8v4375y399fxrrvphb3000ajgbqz0a160dcy7mhnj")))

(define-public crate-cset-0.1.1 (c (n "cset") (v "0.1.1") (d (list (d (n "cset-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0z3wvxhhvilr5j7v2g4lbrrdzkj4a44a6qcz4bylsznlyvr0hdcg")))

