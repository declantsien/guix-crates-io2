(define-module (crates-io cs tr cstream) #:use-module (crates-io))

(define-public crate-cstream-0.1.0 (c (n "cstream") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.154") (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "1398wplz67gj6mmz1yd3sjnqw4f9am1r0d08y7dylqsn0kjci4k6") (f (quote (("std" "alloc" "libc/std") ("default" "std") ("alloc"))))))

(define-public crate-cstream-0.1.1 (c (n "cstream") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.154") (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "186hlfij72zzlqprv9dv9g1b5jqfjz10vhvccgjdskriwx6rzrfg") (f (quote (("std" "alloc" "libc/std") ("default" "std") ("alloc"))))))

(define-public crate-cstream-0.1.2 (c (n "cstream") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.154") (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "0spk9b0q22pjj98wk4j9z2x9qgsk51av9zc2nibrv7dsl22lz65x") (f (quote (("std" "alloc" "libc/std") ("default" "std") ("alloc"))))))

(define-public crate-cstream-0.1.3 (c (n "cstream") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.154") (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "070v3xd22zkm8ccg3fldr113fgsf4nikvn7v8p2gib79yclqacp7") (f (quote (("std" "alloc" "libc/std") ("default" "std") ("alloc"))))))

(define-public crate-cstream-0.1.4 (c (n "cstream") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.154") (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "1bjvfb5mglnh43ky8xk7d8196jd3184224x47ki4hvvhnndzaz6h") (f (quote (("std" "alloc" "libc/std") ("default" "std") ("alloc"))))))

