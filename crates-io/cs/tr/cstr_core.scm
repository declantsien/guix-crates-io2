(define-module (crates-io cs tr cstr_core) #:use-module (crates-io))

(define-public crate-cstr_core-0.1.0 (c (n "cstr_core") (v "0.1.0") (d (list (d (n "cty") (r "^0.1") (d #t) (k 0)) (d (n "memchr") (r "^1.0") (k 0)))) (h "0a71q41nk8pnp0yz6s391qxxslgwf95ng7rijpznrcz70qj8habq") (f (quote (("alloc"))))))

(define-public crate-cstr_core-0.1.1 (c (n "cstr_core") (v "0.1.1") (d (list (d (n "cty") (r "^0.1") (d #t) (k 0)) (d (n "memchr") (r "^2.0") (k 0)))) (h "1idsqx7390gz84wmycp2wzxfiz1zl9ix4yrb1cx61rh4aaan5zz9") (f (quote (("alloc"))))))

(define-public crate-cstr_core-0.1.2 (c (n "cstr_core") (v "0.1.2") (d (list (d (n "cty") (r "^0.1") (d #t) (k 0)) (d (n "memchr") (r "^2.0") (k 0)))) (h "0db048qhwi6zgsbxjjmrkiirgnqfj63yvl143mi4is2pxrc73gkf") (f (quote (("alloc"))))))

(define-public crate-cstr_core-0.2.0 (c (n "cstr_core") (v "0.2.0") (d (list (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "memchr") (r "^2.3.3") (k 0)))) (h "11dsj23h0igkbfdgjpr9yml54783dqciw7yvy41qg5h3bx5m2n47") (f (quote (("use_libc" "memchr/libc") ("default" "arc") ("arc") ("alloc"))))))

(define-public crate-cstr_core-0.2.1 (c (n "cstr_core") (v "0.2.1") (d (list (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "memchr") (r "^2.3.3") (k 0)))) (h "1dw6hx1mz1fzbswjk2nwqpnvzda6y7nx7796yjqa7kl225gzzygd") (f (quote (("use_libc" "memchr/libc") ("default" "arc" "alloc") ("arc") ("alloc"))))))

(define-public crate-cstr_core-0.2.2 (c (n "cstr_core") (v "0.2.2") (d (list (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "memchr") (r "^2.3.3") (k 0)))) (h "1vv53g51swqplk5qnw31dxmhnhl0ldi4c2wrpl3i8hlvq0pzbg96") (f (quote (("use_libc" "memchr/libc") ("nightly") ("default" "arc" "alloc") ("arc") ("alloc"))))))

(define-public crate-cstr_core-0.2.3 (c (n "cstr_core") (v "0.2.3") (d (list (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "memchr") (r "^2.3.3") (k 0)))) (h "1fymp34mv7bix8m35pw3qs30b7879wmaym03ihfbzdl84plwa1r8") (f (quote (("use_libc" "memchr/libc") ("std") ("nightly") ("default" "arc" "alloc") ("arc") ("alloc"))))))

(define-public crate-cstr_core-0.2.4 (c (n "cstr_core") (v "0.2.4") (d (list (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "memchr") (r "^2.3.3") (k 0)))) (h "1ly1r301kxfa1dz0gqh34pry7ry4mw3g00ss3mkkdrz1x7psjywi") (f (quote (("use_libc" "memchr/libc") ("std") ("nightly") ("default" "arc" "alloc") ("arc") ("alloc"))))))

(define-public crate-cstr_core-0.2.5 (c (n "cstr_core") (v "0.2.5") (d (list (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "memchr") (r "^2.3.3") (k 0)))) (h "027vl2w6r71sfsg7b72j6nfljfhzblma8ss8746snqy0fg12hj34") (f (quote (("use_libc" "memchr/libc") ("std") ("nightly") ("default" "arc" "alloc") ("arc") ("alloc"))))))

(define-public crate-cstr_core-0.2.6 (c (n "cstr_core") (v "0.2.6") (d (list (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "memchr") (r "^2.3.3") (k 0)))) (h "0mh9lyzc2iyz2wzzhj1jhxyy3384wg19s8db1ka35a6w9wp7966x") (f (quote (("use_libc" "memchr/libc") ("std") ("nightly") ("default" "arc" "alloc") ("arc") ("alloc"))))))

