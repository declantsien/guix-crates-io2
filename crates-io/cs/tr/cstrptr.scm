(define-module (crates-io cs tr cstrptr) #:use-module (crates-io))

(define-public crate-cstrptr-0.1.0 (c (n "cstrptr") (v "0.1.0") (d (list (d (n "cty") (r "^0.2.0") (d #t) (k 0)) (d (n "memchr") (r "^2.2.1") (o #t) (k 0)))) (h "0lmygqs4bgkilm982an3dp73iifmxc70mclgnmhsvky45s3w83vv") (f (quote (("unstable") ("std") ("default" "memchr") ("alloc"))))))

(define-public crate-cstrptr-0.1.1 (c (n "cstrptr") (v "0.1.1") (d (list (d (n "cty") (r "^0.2.0") (d #t) (k 0)) (d (n "memchr") (r "^2.2.1") (o #t) (k 0)))) (h "0j064c0wy9p147xifl3gg94jrwa42a6cqzrkkw99amb1wpyp71gx") (f (quote (("unstable") ("std") ("default" "memchr") ("alloc"))))))

(define-public crate-cstrptr-0.1.2 (c (n "cstrptr") (v "0.1.2") (d (list (d (n "cty") (r "^0.2.0") (d #t) (k 0)) (d (n "memchr") (r "^2.2.1") (o #t) (k 0)))) (h "0ildjjvam9z1mg3ax9rka58dphxbw7xdrv3mkxc19dva7bsg1r6r") (f (quote (("unstable") ("std") ("default" "memchr") ("alloc"))))))

(define-public crate-cstrptr-0.1.3 (c (n "cstrptr") (v "0.1.3") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.2") (o #t) (k 0)))) (h "10lbrrxdzmbjvm11ijb35q7rr9m0ggfip998q7nlghwz3l5xypr1") (f (quote (("unstable") ("std") ("default" "memchr") ("alloc"))))))

