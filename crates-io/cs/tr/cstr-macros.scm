(define-module (crates-io cs tr cstr-macros) #:use-module (crates-io))

(define-public crate-cstr-macros-0.1.0 (c (n "cstr-macros") (v "0.1.0") (d (list (d (n "procedural-masquerade") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "153pnhhybl708iswb5gw3vp62vjn3k0ifhkndr8v75iyjj8qkdck")))

(define-public crate-cstr-macros-0.1.1 (c (n "cstr-macros") (v "0.1.1") (d (list (d (n "procedural-masquerade") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 2)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "1b102ci9pjjpwrrmrjclvy3miq770w28p466w5kqps9sjhx5shmk")))

(define-public crate-cstr-macros-0.1.2 (c (n "cstr-macros") (v "0.1.2") (d (list (d (n "procedural-masquerade") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 2)) (d (n "syn") (r "^0.12") (f (quote ("derive" "parsing"))) (k 0)))) (h "0y1xjfry6xsvwd8xy6pdqlysm6b9dy024s1i317nz8qy7lh1dwzr")))

(define-public crate-cstr-macros-0.1.3 (c (n "cstr-macros") (v "0.1.3") (d (list (d (n "procedural-masquerade") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 2)) (d (n "syn") (r "^0.13") (f (quote ("derive" "parsing"))) (k 0)))) (h "1a148g1ma83060fkvmjmjgkbdq3aslx2pvmn5zrimv6khdyc2wh4")))

(define-public crate-cstr-macros-0.1.4 (c (n "cstr-macros") (v "0.1.4") (d (list (d (n "procedural-masquerade") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("derive" "parsing"))) (k 0)))) (h "0y7da14izlxijfr2vkjxb9dvzj2wywavlk6f4cdzicz9xpb0qw2y")))

(define-public crate-cstr-macros-0.1.5 (c (n "cstr-macros") (v "0.1.5") (d (list (d (n "procedural-masquerade") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("derive" "parsing"))) (k 0)))) (h "0j1x9cvsd744h568lm7fwwshyzdq9hwsbnsysycgqwy7gs2ds4hg")))

(define-public crate-cstr-macros-0.1.6 (c (n "cstr-macros") (v "0.1.6") (d (list (d (n "procedural-masquerade") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("derive" "parsing"))) (k 0)))) (h "0fkax9qz9vphcm8s1f9zaw2qsfz6kiq9byq789ifys47ymghwryd")))

