(define-module (crates-io cs tr cstr) #:use-module (crates-io))

(define-public crate-cstr-0.1.0 (c (n "cstr") (v "0.1.0") (d (list (d (n "cstr-macros") (r "^0.1") (d #t) (k 0)) (d (n "procedural-masquerade") (r "^0.1") (d #t) (k 0)))) (h "134xl92inhygjppl8d42v8gdl53rk6yfklvdxvhmncar9w7wrm5a")))

(define-public crate-cstr-0.1.1 (c (n "cstr") (v "0.1.1") (d (list (d (n "cstr-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "procedural-masquerade") (r "^0.1") (d #t) (k 0)))) (h "1zpamqi0ih842ij2sm2ngp47jrnv40hchpx17ygk0ilf815hmhgs")))

(define-public crate-cstr-0.1.2 (c (n "cstr") (v "0.1.2") (d (list (d (n "cstr-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "procedural-masquerade") (r "^0.1") (d #t) (k 0)))) (h "0p331r6qzk25lhd7zwamqh4b5q37vqgdqczpk2g33y8szni62h5q")))

(define-public crate-cstr-0.1.3 (c (n "cstr") (v "0.1.3") (d (list (d (n "cstr-macros") (r "^0.1.2") (d #t) (k 0)) (d (n "procedural-masquerade") (r "^0.1") (d #t) (k 0)))) (h "1sm65magw5kl70cgcbfgghygqifd2hdn1xgp3jp7wr693pdpnmdn")))

(define-public crate-cstr-0.1.4 (c (n "cstr") (v "0.1.4") (d (list (d (n "cstr-macros") (r "^0.1.3") (d #t) (k 0)) (d (n "procedural-masquerade") (r "^0.1") (d #t) (k 0)))) (h "0lnj8zm217p1hp2g7zni6ny3v34dsn4h0vhhg7s0njq8xa676sxw")))

(define-public crate-cstr-0.1.5 (c (n "cstr") (v "0.1.5") (d (list (d (n "cstr-macros") (r "^0.1.4") (d #t) (k 0)) (d (n "procedural-masquerade") (r "^0.1") (d #t) (k 0)))) (h "0w9a624ba96wm7hql51jzgk9r9487nm7y9akpd5yi9n0r5914s7f")))

(define-public crate-cstr-0.1.6 (c (n "cstr") (v "0.1.6") (d (list (d (n "cstr-macros") (r "^0.1.4") (d #t) (k 0)) (d (n "procedural-masquerade") (r "^0.1") (d #t) (k 0)))) (h "1v7pb4a4rdwm38m3chkwng0fa8v61cfbs34crz8zkkx5i1vv7cdp")))

(define-public crate-cstr-0.1.7 (c (n "cstr") (v "0.1.7") (d (list (d (n "cstr-macros") (r "^0.1.5") (d #t) (k 0)) (d (n "procedural-masquerade") (r "^0.1") (d #t) (k 0)))) (h "0q0gphn1nxa1xwa92akhggspqvry8wvkkrnfsivy1mzcsj7a1xqr")))

(define-public crate-cstr-0.2.0 (c (n "cstr") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "printing"))) (k 0)) (d (n "trybuild") (r "^1.0.30") (d #t) (k 2)))) (h "1r1j2828d0mfy586ihy6sv65jmh6rd2zzmw29kpvj5iavzvjybr1")))

(define-public crate-cstr-0.2.1 (c (n "cstr") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "printing"))) (k 0)) (d (n "trybuild") (r "^1.0.30") (d #t) (k 2)))) (h "024bkj308552kzgvwcpmbvbr919im2h5614q4fr4847gsgdmrjws")))

(define-public crate-cstr-0.2.2 (c (n "cstr") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "printing"))) (k 0)) (d (n "trybuild") (r "^1.0.30") (d #t) (k 2)))) (h "076cx21n9f19c65ffj7nly90rnn2a4j7s5y6jvrjgj3lj8yykjni")))

(define-public crate-cstr-0.2.3 (c (n "cstr") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing"))) (k 0)) (d (n "trybuild") (r "^1.0.30") (d #t) (k 2)))) (h "1g5cdaxidlj05gba12dydzw19nca7mv1x4ryigfq95ihw7lfjrwl")))

(define-public crate-cstr-0.2.4 (c (n "cstr") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.30") (d #t) (k 2)))) (h "07gniygq6jhbbzp2d7kjnqvx7h3ag90p9vq99r06504dmwzhb6nm")))

(define-public crate-cstr-0.2.5 (c (n "cstr") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.18") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.30") (d #t) (k 2)))) (h "0f3fwsr74v3h4c9rgp77x5q68k3c1s88b9vyc03kja4mdsg8yz85")))

(define-public crate-cstr-0.2.6 (c (n "cstr") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.30") (d #t) (k 2)))) (h "0nwp4kzr2dsxl32hx3yr0iffk3q2i2v49x0jn4zlzlc7875wq6pj")))

(define-public crate-cstr-0.2.7 (c (n "cstr") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.30") (d #t) (k 2)))) (h "0jm2fwqbaj27nz3isbw5x9f8p1r14qg42l03sgl11bbvz49whpf3")))

(define-public crate-cstr-0.2.8 (c (n "cstr") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.30") (d #t) (k 2)))) (h "06wl8x732dvn31w0hwbgsgf6jlc3q56s1a0xf6b5icx3fvbkj6n1")))

(define-public crate-cstr-0.2.9 (c (n "cstr") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.30") (d #t) (k 2)))) (h "1n3z76f83dflbla9abrbcf484ypsyn1qkshischggbyw6qv6v17j")))

(define-public crate-cstr-0.2.10 (c (n "cstr") (v "0.2.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.30") (d #t) (k 2)))) (h "1ssd8ac8n5ixk0rf8n9g3bzndxk8v4slhkbnzlh7zdp46b8hs3x6")))

(define-public crate-cstr-0.2.11 (c (n "cstr") (v "0.2.11") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.30") (d #t) (k 2)))) (h "02yrs43l5cdm26vf3pl6xxyd4zfd6hhs4l49czip2ckd7b1riaca") (r "1.64")))

(define-public crate-cstr-0.2.12 (c (n "cstr") (v "0.2.12") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.30") (d #t) (k 2)))) (h "0dj6ll9ry27kn4k0vvhlvbhn9dyyr9haxnd06bxaqnmfr01kjlk8") (r "1.64")))

