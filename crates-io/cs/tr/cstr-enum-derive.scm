(define-module (crates-io cs tr cstr-enum-derive) #:use-module (crates-io))

(define-public crate-cstr-enum-derive-0.1.0 (c (n "cstr-enum-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "printing" "parsing" "derive"))) (d #t) (k 0)))) (h "0khl64g7j7mxb2zmyv0al3vcw454b4l4p151wha3lj21q7h0hlpa")))

