(define-module (crates-io cs tr cstr-argument) #:use-module (crates-io))

(define-public crate-cstr-argument-0.0.1 (c (n "cstr-argument") (v "0.0.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "memchr") (r "^1") (d #t) (k 0)))) (h "1ivzzcx9xxhxnmww944hk6jma34qd9minfqif5zpfi14syy58ma9") (f (quote (("nightly"))))))

(define-public crate-cstr-argument-0.0.2 (c (n "cstr-argument") (v "0.0.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "memchr") (r "^1") (d #t) (k 0)))) (h "0n5nlxdiz77silxb83hd099wbaibvxq8li4kgzrrschrnyj70iai") (f (quote (("nightly"))))))

(define-public crate-cstr-argument-0.1.0 (c (n "cstr-argument") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)))) (h "1vfd3b88xnccqb9ydcs2g63bcsmj75m82ngbrgjmaja6pm0vkyi2") (f (quote (("nightly"))))))

(define-public crate-cstr-argument-0.1.1 (c (n "cstr-argument") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "memchr") (r "^2") (k 0)))) (h "1fyclwygkwp2yrz87n2vvf6l262b3pwrwxga9lx7q362cy04xg90") (f (quote (("nightly"))))))

(define-public crate-cstr-argument-0.1.2 (c (n "cstr-argument") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "memchr") (r "^2") (k 0)))) (h "0h7bi8sfvwq11r40pjvs3bqqmwva25dw7rasjp73niwscn79rgdn") (f (quote (("nightly"))))))

