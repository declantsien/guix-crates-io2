(define-module (crates-io cs tr cstring-interop) #:use-module (crates-io))

(define-public crate-cstring-interop-0.1.0 (c (n "cstring-interop") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "rcstring") (r "^0.2.1") (d #t) (k 0)))) (h "0sn5rn5i87f5ja33i96jdnhfig5nf9ywwas1qyr606p61n2i9b4s")))

