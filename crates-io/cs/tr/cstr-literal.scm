(define-module (crates-io cs tr cstr-literal) #:use-module (crates-io))

(define-public crate-cstr-literal-0.1.0 (c (n "cstr-literal") (v "0.1.0") (d (list (d (n "const_format") (r "^0.2.31") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (f (quote ("diff"))) (d #t) (k 2)))) (h "0r0n88zynr812xa55ajadx19hv8sh6pyhsdg66skngx3fxb7pln7") (r "1.64.0")))

