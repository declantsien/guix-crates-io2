(define-module (crates-io cs tr cstring) #:use-module (crates-io))

(define-public crate-cstring-0.0.1 (c (n "cstring") (v "0.0.1") (h "128hzmw8znpschcxbxw14ggamrq1vdq4q5hg8q1ydq6nrqrivspx")))

(define-public crate-cstring-0.0.2 (c (n "cstring") (v "0.0.2") (h "0dnfsisnx3xx6aqsni0z3w8pd2lc6zqgpyrb0pr8p6mqjahv2y7m")))

(define-public crate-cstring-0.0.3 (c (n "cstring") (v "0.0.3") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.0") (k 0)))) (h "00y26p0fljr3yd5vlbrwlwy7mnij48r1rvgv5l7yz6jzb4yx5xpl") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-cstring-0.0.4 (c (n "cstring") (v "0.0.4") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.0") (k 0)))) (h "17x272vxjsmzf4kcw9p054ldbxyzxyhjy4cpr5w8pfpqh56fk7gm") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-cstring-0.0.5 (c (n "cstring") (v "0.0.5") (d (list (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "memchr") (r "^2.3.4") (k 0)))) (h "1wsnkd5w9kspr9a310g13d006r2k6b2wlm95jxrjsh52c6mffsva") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-cstring-0.1.0 (c (n "cstring") (v "0.1.0") (d (list (d (n "cty") (r ">=0.2.1, <0.3.0") (d #t) (k 0)) (d (n "memchr") (r ">=2.3.4, <3.0.0") (k 0)))) (h "15jy3grw5g9s4r7gig19axx1phy7585fra3fwq7nfv02h2dhpfjf") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-cstring-0.1.1 (c (n "cstring") (v "0.1.1") (d (list (d (n "cty") (r ">=0.2.1, <0.3.0") (d #t) (k 0)) (d (n "memchr") (r ">=2.3.4, <3.0.0") (k 0)))) (h "0g5dbr12wr7n3cnp67rgiws8gprm38x3riwiisz4kcvdbhfa54j5") (f (quote (("default" "alloc") ("alloc"))))))

