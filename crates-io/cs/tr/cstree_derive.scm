(define-module (crates-io cs tr cstree_derive) #:use-module (crates-io))

(define-public crate-cstree_derive-0.12.0-rc.0 (c (n "cstree_derive") (v "0.12.0-rc.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (d #t) (k 0)))) (h "0ycxzgxjb08kaj6nbwmr6vqgzy42hk7brjk8569dmx7hjiyc95c7") (r "1.68")))

(define-public crate-cstree_derive-0.12.0 (c (n "cstree_derive") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (d #t) (k 0)))) (h "1bqvjlbyj2x2wsy2q74n6xnrxb33qi8j9sbzfmwfrv70xn5w79hd") (r "1.68")))

