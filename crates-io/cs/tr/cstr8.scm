(define-module (crates-io cs tr cstr8) #:use-module (crates-io))

(define-public crate-cstr8-0.1.0 (c (n "cstr8") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0pqk5jlyih014xbrnrzcplxcdf2avqw8f489x4250pm3waik049f") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.59.0")))

(define-public crate-cstr8-0.1.1 (c (n "cstr8") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "05bqw7lcqwc3fmkcyiv1yx4hq7jjbz7x7rpd3pr95xywxssb9q10") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.59.0")))

(define-public crate-cstr8-0.1.2 (c (n "cstr8") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1rj3s1mhqhn22lnq9i6zdcj91fgx4ykivj63dlhhhcrmavvcn30v") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.69.0")))

(define-public crate-cstr8-0.1.3 (c (n "cstr8") (v "0.1.3") (h "1wkhw06rlj50n5r607i8szb76l58j8ch02z14ljnisayvqds8xsf") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.72.0")))

(define-public crate-cstr8-0.1.4 (c (n "cstr8") (v "0.1.4") (h "04ylb2bbxr0w7d15glvsdx7ymi280x64mcmgnjgb8y46pq1znrlv") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.72.0")))

