(define-module (crates-io cs vq csvql) #:use-module (crates-io))

(define-public crate-csvql-0.1.0 (c (n "csvql") (v "0.1.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "00inrp5a6bxxyyfhmb083yw958miy5r9vjkjzgq8f38f1yrvqq0l")))

(define-public crate-csvql-0.1.1 (c (n "csvql") (v "0.1.1") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)))) (h "16d7c210plx4p83ca013qjnyx73wpc07wva9y11kbmfrn200lchg")))

