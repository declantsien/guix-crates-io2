(define-module (crates-io cs vq csvquery) #:use-module (crates-io))

(define-public crate-csvquery-0.1.0 (c (n "csvquery") (v "0.1.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "lru") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "19lxw952ma1c9vfqvh5xya2wg2vq857fx7vhzx6k67anl9dyf041")))

(define-public crate-csvquery-0.2.0 (c (n "csvquery") (v "0.2.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0q11gm6jhxswpr1g025706n1v7s8sgla1lf640hkqqg7z542145z")))

(define-public crate-csvquery-0.3.0 (c (n "csvquery") (v "0.3.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0fdn873zjlg2kvrqvmm4vd691d8rjj83m47jbvn0rar766baxsji")))

(define-public crate-csvquery-0.3.1 (c (n "csvquery") (v "0.3.1") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "047g87md2l0g1qspgssgqgq96gy0ldshr416cwrzqihwsmckg2gf")))

