(define-module (crates-io cs fm csfml-window-sys) #:use-module (crates-io))

(define-public crate-csfml-window-sys-0.1.0 (c (n "csfml-window-sys") (v "0.1.0") (d (list (d (n "csfml-system-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "sfml-types") (r "^0.1.0") (d #t) (k 0)))) (h "0rm4q8lfq9c0y81y15z6d0wxilf17yj59vc99wlj7xrjw7c2s16a")))

(define-public crate-csfml-window-sys-0.1.1 (c (n "csfml-window-sys") (v "0.1.1") (d (list (d (n "csfml-system-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "sfml-types") (r "^0.1.0") (d #t) (k 0)))) (h "1kzvqg8wn3r5d2hb733dh93kixk2mdcpjsyn5bkag73ld4drckzp")))

(define-public crate-csfml-window-sys-0.2.0 (c (n "csfml-window-sys") (v "0.2.0") (d (list (d (n "csfml-system-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "sfml-build") (r "^0.1.0") (d #t) (k 1)))) (h "0ja5sf3jqx56zadn9gsxpb03xfnzaialibksbd1dpgmjxmzjd3nj")))

(define-public crate-csfml-window-sys-0.4.0 (c (n "csfml-window-sys") (v "0.4.0") (d (list (d (n "csfml-system-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "sfml-build") (r "^0.2.0") (d #t) (k 1)))) (h "065n3ifbn8b0xslrkc6v8xcb30ap3d667vqcjvh2myd982c0vzc6")))

(define-public crate-csfml-window-sys-0.5.0 (c (n "csfml-window-sys") (v "0.5.0") (d (list (d (n "csfml-system-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "sfml-build") (r "^0.3.0") (d #t) (k 1)))) (h "1lqxqhbzmvgyiyycjf5pbbic6ic1f5ariknfgizicik6186h138x") (l "csfml-window")))

(define-public crate-csfml-window-sys-0.6.0 (c (n "csfml-window-sys") (v "0.6.0") (d (list (d (n "csfml-system-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "sfml-build") (r "^0.4.0") (d #t) (k 1)))) (h "05qs6pmggglrlgbaw80xg2awc82hpxdz403775ykgzdanv17cw72") (l "csfml-window")))

