(define-module (crates-io cs fm csfml-system-sys) #:use-module (crates-io))

(define-public crate-csfml-system-sys-0.1.0 (c (n "csfml-system-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.2") (d #t) (k 0)))) (h "0v90fyf40hvr1kyc6xa6paphk65n186mi1k20sg598mwjagk04zn")))

(define-public crate-csfml-system-sys-0.2.0 (c (n "csfml-system-sys") (v "0.2.0") (d (list (d (n "sfml-build") (r "^0.1.0") (d #t) (k 1)))) (h "0g3gzpgz7irlk9adjb9pfmr2yalk15n3yc317q4s20yac864d9bn")))

(define-public crate-csfml-system-sys-0.2.1 (c (n "csfml-system-sys") (v "0.2.1") (d (list (d (n "sfml-build") (r "^0.1.0") (d #t) (k 1)))) (h "1ifiawxp8cs96akx6x00ikqr6kgs25rsf6jm9fz2bf33nhppwf4f")))

(define-public crate-csfml-system-sys-0.4.0 (c (n "csfml-system-sys") (v "0.4.0") (d (list (d (n "sfml-build") (r "^0.2.0") (d #t) (k 1)))) (h "1zd1xxa4xrz0y3v6r3al49xvs7qh8ig5yyf05wnmkmsi0sn6js12")))

(define-public crate-csfml-system-sys-0.5.0 (c (n "csfml-system-sys") (v "0.5.0") (d (list (d (n "sfml-build") (r "^0.3.0") (d #t) (k 1)))) (h "0pg3mirhg1lb330q8ynskicgv9hspdmp165p9vanh1bzvb1fzghf") (l "csfml-system")))

(define-public crate-csfml-system-sys-0.6.0 (c (n "csfml-system-sys") (v "0.6.0") (d (list (d (n "sfml-build") (r "^0.4.0") (d #t) (k 1)))) (h "1byg6z04f2ss9k9847d7s5an96jc16hbkbda0cb4hw6fkv5vf6a0") (l "csfml-system")))

