(define-module (crates-io cs fm csfml-audio-sys) #:use-module (crates-io))

(define-public crate-csfml-audio-sys-0.1.0 (c (n "csfml-audio-sys") (v "0.1.0") (d (list (d (n "csfml-system-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "sfml-types") (r "^0.1.0") (d #t) (k 0)))) (h "1l3bm9bxqfbqmlsm7a3y33j6b5az64gnc5vwllby7jvh4hxi8gph")))

(define-public crate-csfml-audio-sys-0.2.0 (c (n "csfml-audio-sys") (v "0.2.0") (d (list (d (n "csfml-system-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "sfml-build") (r "^0.1.0") (d #t) (k 1)))) (h "1i943399jcn24brhk5xh7lsm5inxx7f399qyb2ik5pwyf7qvw70j")))

(define-public crate-csfml-audio-sys-0.4.0 (c (n "csfml-audio-sys") (v "0.4.0") (d (list (d (n "csfml-system-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "sfml-build") (r "^0.2.0") (d #t) (k 1)))) (h "190s0qidhndc5mj781vv99c97932gvzaqqmz9f5ysv8fs3ympxf3")))

(define-public crate-csfml-audio-sys-0.5.0 (c (n "csfml-audio-sys") (v "0.5.0") (d (list (d (n "csfml-system-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "sfml-build") (r "^0.3.0") (d #t) (k 1)))) (h "0hl2iabrpys2apd7n82i72x8q8is5r1ln98a6dfd9m66cfvd6a5r") (l "csfml-audio")))

(define-public crate-csfml-audio-sys-0.6.0 (c (n "csfml-audio-sys") (v "0.6.0") (d (list (d (n "csfml-system-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "sfml-build") (r "^0.4.0") (d #t) (k 1)))) (h "15nsg4kz0r3a4b59cw5hivkklskb5gfl095qy3gq5igjnqjhvqpl") (l "csfml-audio")))

