(define-module (crates-io cs fm csfml-network-sys) #:use-module (crates-io))

(define-public crate-csfml-network-sys-0.1.0 (c (n "csfml-network-sys") (v "0.1.0") (d (list (d (n "csfml-system-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "sfml-types") (r "^0.1.0") (d #t) (k 0)))) (h "03ll0fzpxm9cr9qpg6bpyalp84jrhjnq9qwf8yv4ajvk4hll64dx")))

(define-public crate-csfml-network-sys-0.2.0 (c (n "csfml-network-sys") (v "0.2.0") (d (list (d (n "csfml-system-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "sfml-build") (r "^0.1.0") (d #t) (k 1)))) (h "1vxgcnac0w2ybxy1p9vgchz6lgl1ndj7xgni470yg29bzgbp6x83")))

