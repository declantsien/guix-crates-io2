(define-module (crates-io cs he csheap) #:use-module (crates-io))

(define-public crate-csheap-0.1.0 (c (n "csheap") (v "0.1.0") (h "1wzgf1a10bv89f3m3ggvvd805xzqw5f394lpqh5qf0bnm726rzx8") (y #t)))

(define-public crate-csheap-0.1.1 (c (n "csheap") (v "0.1.1") (h "0a5m8hhs7rw3h67fx31qbv4bzwgx569a6wal7kz9xas4xfbz5wza") (y #t)))

(define-public crate-csheap-0.1.3 (c (n "csheap") (v "0.1.3") (h "061awmyil83403ysz1y9jy3nrjp6h4ch69wj0819dzpyc0gw0kk5") (y #t)))

(define-public crate-csheap-0.1.4 (c (n "csheap") (v "0.1.4") (h "09m8hm9hpy74nklma4df9rgcdj9azvq86x41f0ganczs8c5hzbp2") (y #t)))

(define-public crate-csheap-0.1.5 (c (n "csheap") (v "0.1.5") (h "1im2kkxc7bsrbxkl7r7qfbwp5saynjh0qkrn6kr2sivxxshc12m9") (y #t)))

(define-public crate-csheap-0.1.6 (c (n "csheap") (v "0.1.6") (h "01nhbmk8dsv90sb1p2pdvi85nqvj341dxn7j6hi7w3vwz379g33p") (y #t)))

(define-public crate-csheap-0.1.7 (c (n "csheap") (v "0.1.7") (h "0q7lbg71z76cfknj7kq7193138nn3pk01vhfj03qc4ppk1rnk2sk") (y #t)))

(define-public crate-csheap-0.1.8 (c (n "csheap") (v "0.1.8") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0rq391a80bfzramlr1i67qgi5kk4h23yl9nhyaycqf0gss3wg1az") (y #t)))

(define-public crate-csheap-0.1.9 (c (n "csheap") (v "0.1.9") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1i21kdvyhxyfcs53qirvl307b5c6xh7hx75k0amrvj2w68n17ydv") (y #t)))

(define-public crate-csheap-0.1.10 (c (n "csheap") (v "0.1.10") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "16jbvbf4rf900593ln7k2846qlhbvhkirxrv2agjl5jl9093bngx") (y #t)))

(define-public crate-csheap-0.1.11 (c (n "csheap") (v "0.1.11") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1wllj696gn3am868v7kjabp98l4lxklz41mwpri3v9x7951am7cs") (y #t)))

(define-public crate-csheap-0.1.12 (c (n "csheap") (v "0.1.12") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1mccjq8q2w0x6mwdsdl74aa0gp0ci826ns79k3qc1jimw0ils09a")))

