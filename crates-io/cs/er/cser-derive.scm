(define-module (crates-io cs er cser-derive) #:use-module (crates-io))

(define-public crate-cser-derive-0.1.0 (c (n "cser-derive") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "18snwqz5yvjgxypihi60k8sna5gzgq5ynd110plfwwzbdrhcffhj")))

