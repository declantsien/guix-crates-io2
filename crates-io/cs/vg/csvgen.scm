(define-module (crates-io cs vg csvgen) #:use-module (crates-io))

(define-public crate-csvgen-0.1.0 (c (n "csvgen") (v "0.1.0") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.0-pre.1") (d #t) (k 0)) (d (n "time") (r "^0.1.39") (d #t) (k 0)))) (h "079jaxii7rm4vzb1c0bh4lnp53q0s6j5xyjrbp4c4gy1pdjk06cm")))

(define-public crate-csvgen-0.1.1 (c (n "csvgen") (v "0.1.1") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.0-pre.1") (d #t) (k 0)))) (h "0r4ir036hhvklgjiqrdfbxqx3x2x3y5dbay0f6zsf4h9ziacsr7y")))

