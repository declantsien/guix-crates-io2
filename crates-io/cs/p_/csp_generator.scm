(define-module (crates-io cs p_ csp_generator) #:use-module (crates-io))

(define-public crate-csp_generator-0.1.0-alpha (c (n "csp_generator") (v "0.1.0-alpha") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fapvh2s209v4jz4ii00kf9n5m1frz40sw0zrgibmxd079xailhl")))

(define-public crate-csp_generator-0.1.0-beta (c (n "csp_generator") (v "0.1.0-beta") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ljx11fm6571cshkkdvfijhmk4p7zbniy36q1dfgvjbrljnh5smk")))

(define-public crate-csp_generator-0.1.0-beta.1 (c (n "csp_generator") (v "0.1.0-beta.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lm89zjsl6sk53g5fc7i7vbydc7gsv9949cibkk9avpb6yj8mh1y")))

(define-public crate-csp_generator-0.1.0-rc (c (n "csp_generator") (v "0.1.0-rc") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rj0h63swgdw5zrf4x0yza0rkvjifm1v5cim73dxq730dvwiak8p")))

(define-public crate-csp_generator-0.1.0-rc.1 (c (n "csp_generator") (v "0.1.0-rc.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rf06rzd0asw9xln49l0y3vvp0ljn03dj6ncykl6n4r0hlh7knr4")))

(define-public crate-csp_generator-0.2.0-beta (c (n "csp_generator") (v "0.2.0-beta") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1880mya6s97zzjvnpp8ngza30z1h32apd2kdp1lb0g7kkn458dd9")))

(define-public crate-csp_generator-0.2.0-beta.1 (c (n "csp_generator") (v "0.2.0-beta.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0n8s18027isvivd249cpkgz4n9x32w6pdbq1mkxn2ckqiv0ixzs7")))

(define-public crate-csp_generator-0.2.0-beta.2 (c (n "csp_generator") (v "0.2.0-beta.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0a0n4h0am56rxiwyyxvzy9lrayv4v0czy26sdyizswd8gyc0sx8p")))

(define-public crate-csp_generator-0.2.0-beta.3 (c (n "csp_generator") (v "0.2.0-beta.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hqxj762giw1vrl3j7nrlbb73xxk96cjm8s2xj2vy8gjvv0jx02c")))

(define-public crate-csp_generator-0.2.0-rc (c (n "csp_generator") (v "0.2.0-rc") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0h2vmls5wp83mc2q1yz47ij6sp83mq418i6xhilcdqix2v3cjgji")))

