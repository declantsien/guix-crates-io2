(define-module (crates-io cs ea csearch_app) #:use-module (crates-io))

(define-public crate-csearch_app-1.0.0 (c (n "csearch_app") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.8") (d #t) (k 0)))) (h "00vg8jfay31lbk8gkcs3rng4np63693f4l7vx3rias80rlmrs60d")))

