(define-module (crates-io cs ea csearch) #:use-module (crates-io))

(define-public crate-csearch-1.0.0 (c (n "csearch") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.1.13") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "nameof") (r "^1.2.2") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.8") (d #t) (k 0)))) (h "05drz5ny81nh26gaqlhb93mnzkpiwwz3kl6ny7zbm3hvnw5kha1n")))

(define-public crate-csearch-1.1.0 (c (n "csearch") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.1.13") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.1.5") (d #t) (k 0)) (d (n "csearch_app") (r "^1.0.0") (d #t) (k 0)) (d (n "nameof") (r "^1.2.2") (d #t) (k 0)))) (h "0y5qhv4jgrcg323w7vwnrzrdwkd3yjy9xj5nbsga7mlqyx073shd") (y #t)))

(define-public crate-csearch-1.1.1 (c (n "csearch") (v "1.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.1.13") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.1.5") (d #t) (k 0)) (d (n "csearch_app") (r "^1.0.0") (d #t) (k 0)) (d (n "nameof") (r "^1.2.2") (d #t) (k 0)))) (h "1ijhny9zl4dsn6g2l7jiwrgqjbrngvsfxqy6qvmxiaw178s1wx8r")))

(define-public crate-csearch-1.1.2 (c (n "csearch") (v "1.1.2") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.1.13") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.1.5") (d #t) (k 0)) (d (n "csearch_app") (r "^1.0.0") (d #t) (k 0)) (d (n "nameof") (r "^1.2.2") (d #t) (k 0)))) (h "0gfxs10q9g9lqa9ykzmihyjnxygvhjfa56ysk5sgwbzv2pbn05h9")))

