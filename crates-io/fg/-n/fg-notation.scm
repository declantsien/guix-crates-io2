(define-module (crates-io fg -n fg-notation) #:use-module (crates-io))

(define-public crate-fg-notation-0.1.0 (c (n "fg-notation") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.59") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "01w71smc06ydkby3svx5vy8binv5f9y85nsnr6w1lv6bqafs3csa")))

