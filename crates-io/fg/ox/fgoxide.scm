(define-module (crates-io fg ox fgoxide) #:use-module (crates-io))

(define-public crate-fgoxide-0.1.1 (c (n "fgoxide") (v "0.1.1") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0mjy2jbfqvhbs4mjnn8h6q0c4qxk6ygx0ipkzc36iz6s4sf72c2f")))

(define-public crate-fgoxide-0.1.2 (c (n "fgoxide") (v "0.1.2") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1yclrlp863dlr1i3g4aavdra8zi1rjl0z3hxn060lxxp3n2s18wk")))

(define-public crate-fgoxide-0.1.3 (c (n "fgoxide") (v "0.1.3") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "15y22dn5xm7qn9zgnbnawwmf5d541as6w69nn6jssmkbzg2c4wfm")))

(define-public crate-fgoxide-0.1.4 (c (n "fgoxide") (v "0.1.4") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1dn6100j798c02axqqwgfd3zfy2sayqi3ixgsmr293cpwmbblxw8")))

(define-public crate-fgoxide-0.2.0 (c (n "fgoxide") (v "0.2.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "19ckqpar2vx0alb92yk4m3ynsbvbyyxzijhcmiccrrnmlppdx3r7")))

(define-public crate-fgoxide-0.3.0 (c (n "fgoxide") (v "0.3.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "06az302pwsqdwdz9hl48inghmgah7g53s0dimi0w7i9bhh2p2any")))

(define-public crate-fgoxide-0.4.0 (c (n "fgoxide") (v "0.4.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0br9dzpwa08795y9nkish00x5i6hdca4smvyhaxw7glr01085n0w") (r "1.58.1")))

