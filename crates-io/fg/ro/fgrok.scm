(define-module (crates-io fg ro fgrok) #:use-module (crates-io))

(define-public crate-fgrok-1.1.0 (c (n "fgrok") (v "1.1.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "onig") (r "^5.0") (d #t) (k 0)))) (h "1dapgg9a7n4sh75rz3kjpw82r6ycsqa3iiipm9jg0n0n084s0fx9")))

