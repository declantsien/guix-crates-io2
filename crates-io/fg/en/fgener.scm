(define-module (crates-io fg en fgener) #:use-module (crates-io))

(define-public crate-fgener-0.1.0 (c (n "fgener") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "09qljgvrjmlbkvx2irj71cbzjs4g78g80sc4pcdhpzw9jxzwvf0z")))

(define-public crate-fgener-0.1.1 (c (n "fgener") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1vxbr1wcq07p18mys1s690na5g506qhgzny17gc8wc90ssq74ari")))

