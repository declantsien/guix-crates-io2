(define-module (crates-io fg en fgen) #:use-module (crates-io))

(define-public crate-fgen-0.1.0 (c (n "fgen") (v "0.1.0") (d (list (d (n "cbindgen") (r "^0.6.0") (o #t) (d #t) (k 1)) (d (n "env_logger") (r "^0.5.10") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.42") (o #t) (d #t) (k 1)) (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.6") (d #t) (k 2)) (d (n "tera") (r "^0.11.8") (d #t) (k 0)))) (h "1f1kaigxyx0n7v105ghmdcx3ls41w569srkp3czzwr4i79m5kinh") (f (quote (("default") ("cc" "cbindgen" "libc"))))))

