(define-module (crates-io lc #{3-}# lc3-rs) #:use-module (crates-io))

(define-public crate-lc3-rs-0.1.0 (c (n "lc3-rs") (v "0.1.0") (d (list (d (n "rust-embed") (r "^4.3") (d #t) (k 0)))) (h "17p636qv3z9z5c68hh1fi0pwv52hy6dcg0bky3jkqv4swmf5xq1l") (f (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

(define-public crate-lc3-rs-0.1.1 (c (n "lc3-rs") (v "0.1.1") (h "0wslirvxvincdgr59fjvv4crmfh2g3rijfyclv7frcpjfy04p90r") (f (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

(define-public crate-lc3-rs-0.1.2 (c (n "lc3-rs") (v "0.1.2") (h "17jg1zi4y450xj5jb7d7a3x09p204673md1sfxzj8bm0q2kgabky") (f (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

(define-public crate-lc3-rs-0.1.3 (c (n "lc3-rs") (v "0.1.3") (h "0rscz7c7z75p73m9rjcdpg1pxjjcvrhwj7c3xx7zr76h8fm7lnb3") (f (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

(define-public crate-lc3-rs-0.2.0 (c (n "lc3-rs") (v "0.2.0") (h "1n0w0jn5pl5i4fk1v1rhzy7vvm4sy41sscghraqi0k4ingsj9pxr") (f (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

(define-public crate-lc3-rs-0.3.0 (c (n "lc3-rs") (v "0.3.0") (h "1gzcmgnxh0q4vdwyk0cd214gsssgr5259f8zja39gniii6q21n1y") (f (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

(define-public crate-lc3-rs-0.3.1 (c (n "lc3-rs") (v "0.3.1") (h "00bnlkx00zns6907jzk7gyfzvklr1gc6drjbfc666qk7w14md9c7") (f (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

(define-public crate-lc3-rs-0.4.0 (c (n "lc3-rs") (v "0.4.0") (h "13c0rq3n51bpf9shxyv2mimqyrnbf7drdrh2bqsc1mqinqss00sc") (f (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

(define-public crate-lc3-rs-0.4.1 (c (n "lc3-rs") (v "0.4.1") (h "1aclrp4f89dawni3hfnawnisad1x7hwrjzk65ng727j1gldxf1b0") (f (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

(define-public crate-lc3-rs-0.4.2 (c (n "lc3-rs") (v "0.4.2") (h "1dy7ncbzhip20j175ksvsya5x9zpm1804l739a192j88w87pyc0r") (f (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

(define-public crate-lc3-rs-0.4.3 (c (n "lc3-rs") (v "0.4.3") (h "0dn7gb6faab7v8lyw3y97wiy29x6bzlyahzlcn5i55035fb86alg") (f (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

(define-public crate-lc3-rs-0.5.0 (c (n "lc3-rs") (v "0.5.0") (h "03w8jnk3rfs4q3kz3a1d6k60pjjh5p00jb14k1v5iicsxqhybgfd") (f (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

(define-public crate-lc3-rs-0.6.0 (c (n "lc3-rs") (v "0.6.0") (h "0cddqhj99yvz17ln166wy6v1zxwhcm1w2bbjj96mcqjm41bmfmcc") (f (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

