(define-module (crates-io lc #{3-}# lc3-ensemble) #:use-module (crates-io))

(define-public crate-lc3-ensemble-0.1.0 (c (n "lc3-ensemble") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.12") (d #t) (k 0)) (d (n "logos") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0i36li2i9flhvxi7b0klqx0bc5x125cy7068nz6b3mm9p58lszip")))

(define-public crate-lc3-ensemble-0.2.0 (c (n "lc3-ensemble") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5.12") (d #t) (k 0)) (d (n "logos") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1vpf4lih71apzva84x39w5jp0js8p232f10k63kqxwdy78lggcgr")))

(define-public crate-lc3-ensemble-0.2.1 (c (n "lc3-ensemble") (v "0.2.1") (d (list (d (n "crossbeam-channel") (r "^0.5.12") (d #t) (k 0)) (d (n "logos") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02kbiykp9iljll6f79h3hsqdi7vpms72ci3fp1xsagcz62wmh2j3") (r "1.70")))

(define-public crate-lc3-ensemble-0.3.0 (c (n "lc3-ensemble") (v "0.3.0") (d (list (d (n "crossbeam-channel") (r "^0.5.12") (d #t) (k 0)) (d (n "logos") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "08zgs6ziia0jqh2vc7za2qc1ymgilwrh3yd2wr1m3lzxlvn39242") (r "1.70")))

(define-public crate-lc3-ensemble-0.4.0 (c (n "lc3-ensemble") (v "0.4.0") (d (list (d (n "crossbeam-channel") (r "^0.5.12") (d #t) (k 0)) (d (n "logos") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.7") (d #t) (k 0)))) (h "16bvpnrs92ax4shyqvvghd9644394dp9bhjv14yjqakjnrfx6w29") (r "1.70")))

(define-public crate-lc3-ensemble-0.4.1 (c (n "lc3-ensemble") (v "0.4.1") (d (list (d (n "crossbeam-channel") (r "^0.5.12") (d #t) (k 0)) (d (n "logos") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.7") (d #t) (k 0)))) (h "1is1f4f5jxc8hk8g8zncb6mblip943p0char1f6y268f77flbs8w") (r "1.70")))

(define-public crate-lc3-ensemble-0.4.2 (c (n "lc3-ensemble") (v "0.4.2") (d (list (d (n "crossbeam-channel") (r "^0.5.12") (d #t) (k 0)) (d (n "logos") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1yf89q4chyhr57msml7clj85w9cwim4r7vgqnsb74x1rkfy05j5b") (r "1.70")))

