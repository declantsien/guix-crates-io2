(define-module (crates-io lc #{3-}# lc3-macros) #:use-module (crates-io))

(define-public crate-lc3-macros-0.1.0-alpha0 (c (n "lc3-macros") (v "0.1.0-alpha0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "0lvs4hxb1az4j41bwjcz55wby1nprz7r55gn1jc6188zrlda55rp")))

