(define-module (crates-io lc #{3-}# lc3-codec) #:use-module (crates-io))

(define-public crate-lc3-codec-0.1.0 (c (n "lc3-codec") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0.0") (k 0)) (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "fast-math") (r "^0.1.1") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (k 0)) (d (n "itertools") (r "^0.10.3") (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "log") (r "^0.4") (k 2)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "simple_logger") (r "^1.11.0") (k 2)))) (h "0y6il9l2l3sjj6cp895jjwwplg3d2sin8ll9jgb2iyyds6q1a9ys")))

(define-public crate-lc3-codec-0.2.0 (c (n "lc3-codec") (v "0.2.0") (d (list (d (n "bitvec") (r "^1.0.0") (k 0)) (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "fast-math") (r "^0.1.1") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (k 0)) (d (n "itertools") (r "^0.10.3") (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "log") (r "^0.4") (k 2)) (d (n "num-traits") (r "^0.2.14") (f (quote ("libm"))) (k 0)) (d (n "simple_logger") (r "^1.11.0") (k 2)))) (h "1x7j8yxz670cprsvynczljwrgr6qk9l8gng0455qccyznq4v40ds") (f (quote (("default" "alloc") ("alloc"))))))

