(define-module (crates-io lc #{3-}# lc3-isa) #:use-module (crates-io))

(define-public crate-lc3-isa-0.1.0-alpha0 (c (n "lc3-isa") (v "0.1.0-alpha0") (d (list (d (n "arbitrary") (r "^0.4.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 2)) (d (n "lc3-macros") (r "^0.1.0-alpha0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1f50jkgqa6pmf16jkbp8pvligffmnb3i92207wfs4qhj9v5dgrfq") (f (quote (("strict") ("no_std") ("nightly-const") ("default" "no_std"))))))

