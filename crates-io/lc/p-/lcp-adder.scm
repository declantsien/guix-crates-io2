(define-module (crates-io lc p- lcp-adder) #:use-module (crates-io))

(define-public crate-lcp-adder-0.1.0 (c (n "lcp-adder") (v "0.1.0") (d (list (d (n "lcp-add-one") (r "^0.1.0") (d #t) (k 0)) (d (n "random") (r "^0.12.2") (d #t) (k 0)))) (h "11z3cipcj89405vdv42fn72mqwx6rn87mwyl62sqzgdx23ac52zx")))

