(define-module (crates-io lc d1 lcd1602-diver) #:use-module (crates-io))

(define-public crate-lcd1602-diver-0.1.0 (c (n "lcd1602-diver") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1ccvprcv8qhnlyqaswb192rjz7dllz3vg41bz9d8s1w9cq2wz00k")))

(define-public crate-lcd1602-diver-0.1.1 (c (n "lcd1602-diver") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1zjqvpdw7gaj9mc42nyv58ysyidh7a8djb8ibl40yajd3wf8ldl4")))

