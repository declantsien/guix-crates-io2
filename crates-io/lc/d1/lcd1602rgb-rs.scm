(define-module (crates-io lc d1 lcd1602rgb-rs) #:use-module (crates-io))

(define-public crate-lcd1602rgb-rs-0.1.0 (c (n "lcd1602rgb-rs") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "rp-pico") (r "^0.5.0") (d #t) (k 0)))) (h "0x0bwzc71lw3cqdfz5f25npx0kndhf8f6k2siirnr78ip9xprr8x")))

(define-public crate-lcd1602rgb-rs-0.1.1 (c (n "lcd1602rgb-rs") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "0b42nb0m6gcgndm8qi3j95f5rb0fnjcx1wci9rx47v0iwszn3w23")))

(define-public crate-lcd1602rgb-rs-0.2.0 (c (n "lcd1602rgb-rs") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "0nimsd83cbsgr31qnf0sx1f037d08p6n9sv51r22v4zf3n0b4y19")))

