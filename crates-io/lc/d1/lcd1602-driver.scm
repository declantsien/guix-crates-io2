(define-module (crates-io lc d1 lcd1602-driver) #:use-module (crates-io))

(define-public crate-lcd1602-driver-0.1.0 (c (n "lcd1602-driver") (v "0.1.0") (d (list (d (n "embedded-hal") (r "0.2.*") (f (quote ("unproven"))) (d #t) (k 0)))) (h "166wcajy81khwhgaqv8wbh18cwaz5m7r7raf4wxr3g0rbh88f9mc")))

(define-public crate-lcd1602-driver-0.2.0 (c (n "lcd1602-driver") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^1") (d #t) (k 0)))) (h "0ac9dbaz58a9vn7ir7m66i51vpn1lz899dgh3d9lgjx6pb155dnd")))

