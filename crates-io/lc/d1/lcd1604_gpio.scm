(define-module (crates-io lc d1 lcd1604_gpio) #:use-module (crates-io))

(define-public crate-lcd1604_gpio-1.0.0 (c (n "lcd1604_gpio") (v "1.0.0") (d (list (d (n "rppal") (r "^0.16.0") (d #t) (k 0)))) (h "1yfxsx5dhp4xnwqqd7xqwkxakhy7vqvd473yr9rq0df46kvn17ld")))

(define-public crate-lcd1604_gpio-1.0.1 (c (n "lcd1604_gpio") (v "1.0.1") (d (list (d (n "rppal") (r "^0.16.0") (d #t) (k 0)))) (h "1rxq2aiz540c4d6s5zz441lra9fpvyf7h9l28f8a71wfis1nc8p6")))

