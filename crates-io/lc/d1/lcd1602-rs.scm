(define-module (crates-io lc d1 lcd1602-rs) #:use-module (crates-io))

(define-public crate-lcd1602-rs-0.1.0 (c (n "lcd1602-rs") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "1hfn66sn70dxrrg6yrn0pp9makfdrz9qq8fjqz8ibi73ffhikish")))

