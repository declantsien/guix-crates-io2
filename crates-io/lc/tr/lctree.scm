(define-module (crates-io lc tr lctree) #:use-module (crates-io))

(define-public crate-lctree-0.1.0 (c (n "lctree") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_derive2") (r "^0.1.21") (d #t) (k 2)))) (h "1h8g6zdvqd412yai0lk0nknp0b6kb12kdj9nrxd4nr14q7v7czgn") (r "1.66")))

(define-public crate-lctree-0.1.1 (c (n "lctree") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_derive2") (r "^0.1.21") (d #t) (k 2)))) (h "0bhid1nnjwkqyawqvcxzhg7g5a3x9s3pybjcbzm98mp0faj2d9nl") (r "1.66")))

(define-public crate-lctree-0.1.2 (c (n "lctree") (v "0.1.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_derive2") (r "^0.1.21") (d #t) (k 2)))) (h "116s2zhypkkc51mx0z493x1q0rg1zz7ds88p9y8hahvrhn0b87zs") (r "1.63")))

(define-public crate-lctree-0.1.3 (c (n "lctree") (v "0.1.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_derive2") (r "^0.1.21") (d #t) (k 2)))) (h "024p8zdbn5ka4gbkb3m2g1i6846r8261giqc0k62bwnyzw7wfkcm") (r "1.63")))

(define-public crate-lctree-0.2.0 (c (n "lctree") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_derive2") (r "^0.1.21") (d #t) (k 2)))) (h "1m8m4a2yih3ipyyav5dzfl17nmq21mnn8b2nlibz1xb0l1lq4gdc") (r "1.63")))

(define-public crate-lctree-0.2.1 (c (n "lctree") (v "0.2.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_derive2") (r "^0.1.21") (d #t) (k 2)))) (h "1dnd92yfqpqx2s53bdzds9qlybpvhx9z8ldv51p2ahziyxaq429a") (r "1.63")))

(define-public crate-lctree-0.2.2 (c (n "lctree") (v "0.2.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_derive2") (r "^0.1.21") (d #t) (k 2)))) (h "12xpfh6mgyjlhx0xfqbd2x90xd1hzkgwnvizijjpf0l1q95srllp") (r "1.63")))

(define-public crate-lctree-0.2.3 (c (n "lctree") (v "0.2.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_derive2") (r "^0.1.21") (d #t) (k 2)))) (h "1dnksbnbr74wriqxrdnmqkxlgkq57dyfsflmnccqvz305izgygri") (r "1.63")))

(define-public crate-lctree-0.2.4 (c (n "lctree") (v "0.2.4") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_derive2") (r "^0.1.21") (d #t) (k 2)))) (h "1ax8i2qmg0l938453ynkdq2kp6frgmj3p29b9n9bkssp07jz6rx6") (r "1.63")))

(define-public crate-lctree-0.3.0 (c (n "lctree") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_derive2") (r "^0.1.21") (d #t) (k 2)))) (h "0vcgyimw4lzycvpybyd42ipfjwfng1i5jvmaibdxw30mdg0rwvg4") (r "1.63")))

(define-public crate-lctree-0.3.1 (c (n "lctree") (v "0.3.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_derive2") (r "^0.1.21") (d #t) (k 2)))) (h "1gr0ab06a9nwvb3g6aaz7jabffd21s90rnjcv4am3j5g110jgzrw") (r "1.63")))

(define-public crate-lctree-0.3.2 (c (n "lctree") (v "0.3.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_derive2") (r "^0.1.21") (d #t) (k 2)))) (h "09g3zmpmwrhfafvlwcds1yzyqgxhffig0h2sm2yf7mrim39sicam") (r "1.63")))

(define-public crate-lctree-0.3.3 (c (n "lctree") (v "0.3.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_derive2") (r "^0.1.21") (d #t) (k 2)))) (h "1qys8bvrzd1x9r0h3ix09i168vdj0r3k2sp0p1qfmkpl5d326bfw") (r "1.63")))

