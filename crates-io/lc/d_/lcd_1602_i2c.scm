(define-module (crates-io lc d_ lcd_1602_i2c) #:use-module (crates-io))

(define-public crate-lcd_1602_i2c-0.2.0 (c (n "lcd_1602_i2c") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "16fh350a17pb5pns1amvh4gks2vh9g697i04sbp3fyzn3dnfp2kx")))

(define-public crate-lcd_1602_i2c-0.2.1 (c (n "lcd_1602_i2c") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)))) (h "0nx7lagl5fmwab5kkxihnbd0ayyz1wxys7vsiyy5rs9r2q1swqf0")))

(define-public crate-lcd_1602_i2c-0.3.0 (c (n "lcd_1602_i2c") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.7.2") (d #t) (k 2)))) (h "123251cnlb1jijmj7i8libsad70iig4hvxz4yjw7279ic173spyr")))

