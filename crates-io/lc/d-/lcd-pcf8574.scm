(define-module (crates-io lc d- lcd-pcf8574) #:use-module (crates-io))

(define-public crate-lcd-pcf8574-0.1.2 (c (n "lcd-pcf8574") (v "0.1.2") (d (list (d (n "i2cdev") (r "^0.5.0") (d #t) (k 0)) (d (n "lcd") (r "^0.4.1") (d #t) (k 0)))) (h "0za8vyg2xxdhgjfyk6aqm35006gsdlijkxxzzg843na4hs4ry7dq")))

(define-public crate-lcd-pcf8574-0.2.0 (c (n "lcd-pcf8574") (v "0.2.0") (d (list (d (n "i2cdev") (r "^0.5.0") (d #t) (k 0)) (d (n "lcd") (r "^0.4.1") (d #t) (k 0)))) (h "0qfkw0pwvql3nq8l4a3xyvd0ls2hh6y21i1kfn6nm0x4ksr3mddj")))

