(define-module (crates-io lc d- lcd-lcm1602-i2c) #:use-module (crates-io))

(define-public crate-lcd-lcm1602-i2c-0.1.0 (c (n "lcd-lcm1602-i2c") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "ufmt") (r "^0.1.0") (d #t) (k 0)) (d (n "ufmt-write") (r "^0.1.0") (d #t) (k 0)))) (h "158hcmamnw9759ydycxnf9mrkk14bkj442l8rvdbzy569xwky4kw")))

