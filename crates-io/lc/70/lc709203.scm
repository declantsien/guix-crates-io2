(define-module (crates-io lc #{70}# lc709203) #:use-module (crates-io))

(define-public crate-lc709203-0.1.0 (c (n "lc709203") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1a6jj61yhn94isncw0j43kg4j0gzcy1ifprhj1j7y4hyj951b378") (f (quote (("default" "log"))))))

(define-public crate-lc709203-0.2.0 (c (n "lc709203") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "035p73hfky6bcywzrcxfybd0apqn111rny6i3bmj2mncq799l96r") (f (quote (("default" "log"))))))

(define-public crate-lc709203-0.2.1 (c (n "lc709203") (v "0.2.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "03p5p1d821vzivdagk6rjlbacikpws2n6k5w454vdcrips3fyx7w") (f (quote (("default" "log"))))))

