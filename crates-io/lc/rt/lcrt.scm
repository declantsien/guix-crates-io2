(define-module (crates-io lc rt lcrt) #:use-module (crates-io))

(define-public crate-lcrt-0.1.0 (c (n "lcrt") (v "0.1.0") (h "128z12fprjhvclk2w807dpad1npk0nblkm22kn0cziy7dzdliwpr")))

(define-public crate-lcrt-0.1.1 (c (n "lcrt") (v "0.1.1") (d (list (d (n "cool_asserts") (r "^2.0.3") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (o #t) (d #t) (k 0)))) (h "10ssgb172476cq25vdb27n6w3m0mii0mvap3hm1f9p3mxiijz0ir") (f (quote (("default") ("all" "testing")))) (s 2) (e (quote (("testing" "dep:cool_asserts" "dep:pretty_assertions"))))))

(define-public crate-lcrt-0.1.2 (c (n "lcrt") (v "0.1.2") (d (list (d (n "cool_asserts") (r "^2.0.3") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (o #t) (d #t) (k 0)))) (h "1vrgvyrjqlwwn0s8qymp63227dhakkc7jc5rrmrxh7bmmfm6afbm") (f (quote (("default") ("all" "testing")))) (s 2) (e (quote (("testing" "dep:cool_asserts" "dep:pretty_assertions"))))))

