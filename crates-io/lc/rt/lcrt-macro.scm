(define-module (crates-io lc rt lcrt-macro) #:use-module (crates-io))

(define-public crate-lcrt-macro-0.1.0 (c (n "lcrt-macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.14.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "prettyplease") (r "^0.1.23") (d #t) (k 2)))) (h "1rzzcbpgs8zb06x7zrv1hq9463jqry885zjf4daf1nl1yvl0l9dz")))

