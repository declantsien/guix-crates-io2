(define-module (crates-io lc -r lc-render) #:use-module (crates-io))

(define-public crate-lc-render-0.1.0 (c (n "lc-render") (v "0.1.0") (h "0iv724swzgvlz2gf5l27amx76r369gl4a5vhi7xkiy5jhp0kr2sg")))

(define-public crate-lc-render-0.2.0 (c (n "lc-render") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "svg") (r "^0.9.1") (d #t) (k 0)))) (h "1b1whyb7mjvb79vcjqf64x3mblavwgbcqhiayzrnvjn77v6sw5rf")))

(define-public crate-lc-render-0.2.1 (c (n "lc-render") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "svg") (r "^0.9.1") (d #t) (k 0)))) (h "0pccxbfnpbbvhb0shqwc7n0kcvzpvgrdlvyjki1sn1rr943girh3")))

(define-public crate-lc-render-0.2.2 (c (n "lc-render") (v "0.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "svg") (r "^0.9.1") (d #t) (k 0)))) (h "0spz4660cgk6p9jxxvaj8621hangqys0fncc08dmfxik3mxywg5z")))

(define-public crate-lc-render-0.2.3 (c (n "lc-render") (v "0.2.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "svg") (r "^0.9.1") (d #t) (k 0)))) (h "0y3413m9k2wkg40kh6l3p9j0j8la171wrh0sq2qf7d4ggva4ja2l")))

