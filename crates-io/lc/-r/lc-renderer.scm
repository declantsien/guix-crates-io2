(define-module (crates-io lc -r lc-renderer) #:use-module (crates-io))

(define-public crate-lc-renderer-0.0.1 (c (n "lc-renderer") (v "0.0.1") (h "0y4jcysg3zwb27yd56byhkfckhhvywjcmzvi86jb61fy762kv12n")))

(define-public crate-lc-renderer-0.1.0 (c (n "lc-renderer") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lc-render") (r "^0.2.3") (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-types") (r "^0.7") (d #t) (k 0)) (d (n "slog") (r "^2.7") (d #t) (k 0)) (d (n "slog-async") (r "^2.5") (d #t) (k 0)) (d (n "slog-json") (r "^2.3") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.4") (d #t) (k 0)) (d (n "tonic-build") (r "^0.4") (d #t) (k 1)) (d (n "tonic-health") (r "^0.3.1") (d #t) (k 0)))) (h "0war3ri5riga2l1m3nhlxxvhdyr9df98hnxzvv8fahyr47by8x68")))

(define-public crate-lc-renderer-0.2.0 (c (n "lc-renderer") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lc-render") (r "^0.2.3") (d #t) (k 0)) (d (n "prost") (r "^0.7") (d #t) (k 0)) (d (n "prost-types") (r "^0.7") (d #t) (k 0)) (d (n "slog") (r "^2.7") (d #t) (k 0)) (d (n "slog-async") (r "^2.5") (d #t) (k 0)) (d (n "slog-json") (r "^2.3") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread" "signal"))) (d #t) (k 0)) (d (n "tonic") (r "^0.4") (d #t) (k 0)) (d (n "tonic-build") (r "^0.4") (d #t) (k 1)) (d (n "tonic-health") (r "^0.3.1") (d #t) (k 0)))) (h "1200ha7m1arffpig04dnbs6avim43ifr89bix9w1mmzr3dq04mis")))

