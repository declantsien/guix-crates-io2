(define-module (crates-io lc m_ lcm_gen) #:use-module (crates-io))

(define-public crate-lcm_gen-0.1.0 (c (n "lcm_gen") (v "0.1.0") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)))) (h "091d068jvdpk3f7y932frg9ziqylw7rwiz0abjcpngn6hjpcvb4f")))

(define-public crate-lcm_gen-0.1.1 (c (n "lcm_gen") (v "0.1.1") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)))) (h "0m8kw9kvfl7k3clp1mpsiph50b6djfhvh8yrb15ndlvsk4jnmvsq")))

