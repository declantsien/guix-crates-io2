(define-module (crates-io lc g- lcg-tools) #:use-module (crates-io))

(define-public crate-lcg-tools-0.1.0 (c (n "lcg-tools") (v "0.1.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.1") (d #t) (k 0)))) (h "0198g0a3snashgylyhaglfbw0b2ga1vbihm8jgli066j3sgcpg4d")))

