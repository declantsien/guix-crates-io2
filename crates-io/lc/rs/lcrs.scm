(define-module (crates-io lc rs lcrs) #:use-module (crates-io))

(define-public crate-lcrs-0.1.0 (c (n "lcrs") (v "0.1.0") (h "1f24ha2jjagsh1lghs6l8a5ljgn62dcl99lhary9m24j6a9gd5bb")))

(define-public crate-lcrs-0.1.1 (c (n "lcrs") (v "0.1.1") (h "0095ilfsj82r9kzvr838449y15nnkgyh4pd2q8x9gaxi3sn1ymw0")))

