(define-module (crates-io lc s- lcs-png-diff-rs) #:use-module (crates-io))

(define-public crate-lcs-png-diff-rs-0.1.0 (c (n "lcs-png-diff-rs") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.24.2") (k 0)) (d (n "rusty_pool") (r "^0.7.0") (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0sj315jpkganvpm5z1wy7f39g2mp0ixqa9mram7s5c2b0j6qd081") (f (quote (("default" "binary") ("binary" "all_image_formats") ("all_image_formats" "image/png")))) (y #t)))

