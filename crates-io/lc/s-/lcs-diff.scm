(define-module (crates-io lc s- lcs-diff) #:use-module (crates-io))

(define-public crate-lcs-diff-0.1.0 (c (n "lcs-diff") (v "0.1.0") (h "1d1yabr4xy6hxshw63zxj8aprg3p5n0ybhi3jpg7bcq8fz8kp824")))

(define-public crate-lcs-diff-0.1.1 (c (n "lcs-diff") (v "0.1.1") (d (list (d (n "speculate") (r "^0.0.25") (d #t) (k 2)))) (h "1vglzj70422mxkv65lq108x84lx7g5fzbqd6l6srxy6fk5qflwn1")))

