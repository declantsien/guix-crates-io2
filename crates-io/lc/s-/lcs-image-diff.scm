(define-module (crates-io lc s- lcs-image-diff) #:use-module (crates-io))

(define-public crate-lcs-image-diff-0.1.0 (c (n "lcs-image-diff") (v "0.1.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "lcs-diff") (r "^0.1.1") (d #t) (k 0)))) (h "1bsb0p6pfprljp3pm2fgck9b24saz4a3slhs5pz0kv4ryj2j8m5g")))

(define-public crate-lcs-image-diff-0.1.1 (c (n "lcs-image-diff") (v "0.1.1") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "lcs-diff") (r "^0.1.1") (d #t) (k 0)))) (h "1i2fjvfrvs7h5ddv2mzflwmww7vmx6x47g1in8y6a8s7mffzbcg3")))

(define-public crate-lcs-image-diff-0.1.2 (c (n "lcs-image-diff") (v "0.1.2") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "lcs-diff") (r "^0.1.1") (d #t) (k 0)))) (h "1n0qinw2hyw1933mamz6z9qd9sgcq4wpp10ck36dba06f54cn8ni")))

(define-public crate-lcs-image-diff-0.1.3 (c (n "lcs-image-diff") (v "0.1.3") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "lcs-diff") (r "^0.1.1") (d #t) (k 0)))) (h "1r6l15h7i11d6saz5r45ndk7s9qii732yh0cnf4c30asaq3f97rf")))

(define-public crate-lcs-image-diff-0.1.4 (c (n "lcs-image-diff") (v "0.1.4") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "lcs-diff") (r "^0.1.1") (d #t) (k 0)))) (h "1rq7397gg01707q9bb6vax84qfgc0s4w3jcmm7186bprnhzhpl4k")))

(define-public crate-lcs-image-diff-0.1.5 (c (n "lcs-image-diff") (v "0.1.5") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.8") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "lcs-diff") (r "^0.1.1") (d #t) (k 0)))) (h "0zqd9cw8z2smkjfxj9klmcd8mfs6rbi46gpnhbcyzl20qz86sfpl")))

(define-public crate-lcs-image-diff-0.1.6 (c (n "lcs-image-diff") (v "0.1.6") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.8") (d #t) (k 0)) (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "lcs-diff") (r "^0.1.1") (d #t) (k 0)))) (h "1izmxz275wm52jmzrsnmsxgyrlvq4lfd0iiq9m1fhxbzzj0873bv")))

(define-public crate-lcs-image-diff-0.1.7 (c (n "lcs-image-diff") (v "0.1.7") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.18") (o #t) (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.20") (k 0)) (d (n "lcs-diff") (r "^0.1.1") (d #t) (k 0)))) (h "0xw1kva5rkncjc0hx4pij61hzayyhjxpnvcrmg7zyp56a2mxg8m8") (f (quote (("default" "binary") ("binary" "futures" "futures-cpupool" "all_image_formats") ("all_image_formats" "image/gif_codec" "image/jpeg" "image/ico" "image/png_codec" "image/pnm" "image/tga" "image/tiff" "image/webp" "image/bmp" "image/hdr" "image/dxt" "image/jpeg_rayon"))))))

