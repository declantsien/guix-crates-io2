(define-module (crates-io lc ms lcms2) #:use-module (crates-io))

(define-public crate-lcms2-0.1.0 (c (n "lcms2") (v "0.1.0") (d (list (d (n "lcms2-sys") (r "^0.4") (d #t) (k 0)))) (h "0z5pscg0wxh0bmd2vblchmsnlf2xvswkdsg7rj0d36n3k6ml2mrs") (y #t)))

(define-public crate-lcms2-0.2.0 (c (n "lcms2") (v "0.2.0") (d (list (d (n "lcms2-sys") (r "^0.4") (d #t) (k 0)))) (h "12ypzpfskjbx4wicifp3n802fia59pvcm8vdb3wq24p8g8rsrywx") (y #t)))

(define-public crate-lcms2-0.2.1 (c (n "lcms2") (v "0.2.1") (d (list (d (n "lcms2-sys") (r "^0.4") (d #t) (k 0)))) (h "0zdq2bhi4v63klikccd0mn0aj5ifhmx0x5jq0ykhn1mz44rzsvr8") (y #t)))

(define-public crate-lcms2-1.0.0 (c (n "lcms2") (v "1.0.0") (d (list (d (n "lcms2-sys") (r "^0.4") (d #t) (k 0)))) (h "1782b05wb94i5rl29bvn7ygiyprmlnydmm1jg6d8q8q3cz3f4w0d") (y #t)))

(define-public crate-lcms2-1.1.0 (c (n "lcms2") (v "1.1.0") (d (list (d (n "lcms2-sys") (r "^0.5") (d #t) (k 0)))) (h "1vgqs72rll9q0lxgm15yr7jm9rcdnbybxah9v0c4g135kxzaflpv") (y #t)))

(define-public crate-lcms2-1.1.1 (c (n "lcms2") (v "1.1.1") (d (list (d (n "lcms2-sys") (r "^1.0.0") (d #t) (k 0)))) (h "04hrr6kmr3qj900lnz50xyjgfyb92vqrm17vj11cqmh1m0ync1kb") (y #t)))

(define-public crate-lcms2-1.1.2 (c (n "lcms2") (v "1.1.2") (d (list (d (n "lcms2-sys") (r "^1.0.0") (d #t) (k 0)))) (h "11hw4wvnbm55478mpnlp2ypxwy14a09qn65yx12l4647fv58zq0f") (y #t)))

(define-public crate-lcms2-2.0.0 (c (n "lcms2") (v "2.0.0") (d (list (d (n "lcms2-sys") (r "^1.0.2") (d #t) (k 0)))) (h "023592wqx71ld3x4a3p98n5iwslkyy2b44i2wyrzajhrh98v5lzf") (y #t)))

(define-public crate-lcms2-3.0.0 (c (n "lcms2") (v "3.0.0") (d (list (d (n "foreign-types") (r "^0.2.0") (d #t) (k 0)) (d (n "lcms2-sys") (r "^2.0.0") (d #t) (k 0)))) (h "08l3yjipmdilrik7y8ydmw03kylhfji8aqmz6f5dnq2ijimscb09") (y #t)))

(define-public crate-lcms2-3.1.0 (c (n "lcms2") (v "3.1.0") (d (list (d (n "foreign-types") (r "^0.2.0") (d #t) (k 0)) (d (n "lcms2-sys") (r "^2.1.0") (d #t) (k 0)))) (h "1imfabpf32cg4y1nrx9ikpfjnas5q701515cyra181axwnmil31c") (y #t)))

(define-public crate-lcms2-3.2.0 (c (n "lcms2") (v "3.2.0") (d (list (d (n "foreign-types") (r "^0.2.0") (d #t) (k 0)) (d (n "lcms2-sys") (r "^2.1.1") (d #t) (k 0)))) (h "1kjbdyw6k78g9a2s2xalrjygkspwqb165zyx250l823fw8v8d4y9") (y #t)))

(define-public crate-lcms2-4.0.0 (c (n "lcms2") (v "4.0.0") (d (list (d (n "foreign-types") (r "^0.2.0") (d #t) (k 0)) (d (n "lcms2-sys") (r "^2.3.0") (d #t) (k 0)))) (h "0f13c079q1h3601d2zzbkgfi6bnr46gsv60m48nrh3y8ijils57a") (y #t)))

(define-public crate-lcms2-4.1.0 (c (n "lcms2") (v "4.1.0") (d (list (d (n "foreign-types") (r "^0.2.0") (d #t) (k 0)) (d (n "lcms2-sys") (r "^2.3.1") (d #t) (k 0)))) (h "1ki3bin52z8a4jxccf0ncrv4z1dq4049il1qpp43hygb6ah0qx7y") (y #t)))

(define-public crate-lcms2-4.2.0 (c (n "lcms2") (v "4.2.0") (d (list (d (n "foreign-types") (r "^0.2.0") (d #t) (k 0)) (d (n "lcms2-sys") (r "^2.4.4") (d #t) (k 0)))) (h "0kk8y2ghpmvimv7y6mc4n1jrrk0fhma7ry9y1byan73vqlap3b7m") (f (quote (("static" "lcms2-sys/static")))) (y #t)))

(define-public crate-lcms2-4.2.1 (c (n "lcms2") (v "4.2.1") (d (list (d (n "foreign-types") (r "^0.3.0") (d #t) (k 0)) (d (n "lcms2-sys") (r "^2.4.4") (d #t) (k 0)))) (h "16mg1n3mclxmy0b4bmwrpn040gkm9289dc618hpf053d94jyg9vx") (f (quote (("static" "lcms2-sys/static"))))))

(define-public crate-lcms2-5.0.0 (c (n "lcms2") (v "5.0.0") (d (list (d (n "foreign-types") (r "^0.3.0") (d #t) (k 0)) (d (n "lcms2-sys") (r "^2.4.4") (d #t) (k 0)))) (h "1mbarmh5h8zcfvbk5gp1f6n4jxldpf8wl572pyrc7s3l9a9p5n9d") (f (quote (("static" "lcms2-sys/static")))) (y #t)))

(define-public crate-lcms2-5.0.1 (c (n "lcms2") (v "5.0.1") (d (list (d (n "foreign-types") (r "^0.3.0") (d #t) (k 0)) (d (n "lcms2-sys") (r "^2.4.7") (d #t) (k 0)))) (h "0xphvzipzm3rz870xg4h7y907492d185q888ivpzxvhgmmfb7r0r") (f (quote (("static" "lcms2-sys/static")))) (y #t)))

(define-public crate-lcms2-5.1.0 (c (n "lcms2") (v "5.1.0") (d (list (d (n "foreign-types") (r "^0.4.0") (d #t) (k 0)) (d (n "lcms2-sys") (r "^2.4.7") (d #t) (k 0)))) (h "0xrjfxp25acbhfad28kbmjsyb9qdp47sdhcs8py342hhphd4nabj") (f (quote (("static" "lcms2-sys/static"))))))

(define-public crate-lcms2-5.1.1 (c (n "lcms2") (v "5.1.1") (d (list (d (n "foreign-types") (r "^0.4.0") (d #t) (k 0)) (d (n "lcms2-sys") (r "^2.4.7") (d #t) (k 0)))) (h "1pj0w9065k8qay5fd5nn7frgiyp3xgn24v5mwhza49945b8vzrf1") (f (quote (("static" "lcms2-sys/static"))))))

(define-public crate-lcms2-5.1.2 (c (n "lcms2") (v "5.1.2") (d (list (d (n "foreign-types") (r "^0.4.0") (d #t) (k 0)) (d (n "lcms2-sys") (r "^3.1.0") (d #t) (k 0)))) (h "118w8q7n4ms5wha5jwvhilb81cqki4i1wfvqaif7xcyavljvgrsy") (f (quote (("static" "lcms2-sys/static"))))))

(define-public crate-lcms2-5.1.3 (c (n "lcms2") (v "5.1.3") (d (list (d (n "foreign-types") (r "^0.4.0") (d #t) (k 0)) (d (n "lcms2-sys") (r "^3.1.0") (d #t) (k 0)))) (h "1ywkw2hqcyx9q7v6z4dihwdf8zl1a7cpqdd5lmaj2fs8w1cbvvkx") (f (quote (("static" "lcms2-sys/static"))))))

(define-public crate-lcms2-5.1.4 (c (n "lcms2") (v "5.1.4") (d (list (d (n "foreign-types") (r "^0.4.0") (d #t) (k 0)) (d (n "lcms2-sys") (r "^3.1.1") (d #t) (k 0)))) (h "0ia2x9s2fd2d7k9h386lm8cqpv4y9rk7vsjb2nhzssjqm3mdnrz5") (f (quote (("static" "lcms2-sys/static"))))))

(define-public crate-lcms2-5.1.5 (c (n "lcms2") (v "5.1.5") (d (list (d (n "foreign-types") (r "^0.4.0") (d #t) (k 0)) (d (n "lcms2-sys") (r "^3.1.2") (d #t) (k 0)))) (h "1hssa3h7bkgxgxskhs47k3nvfqv8plljy6962qarsgqnjinpnyxn") (f (quote (("static" "lcms2-sys/static"))))))

(define-public crate-lcms2-5.1.6 (c (n "lcms2") (v "5.1.6") (d (list (d (n "foreign-types") (r "^0.4.0") (d #t) (k 0)) (d (n "lcms2-sys") (r "^3.1.2") (d #t) (k 0)))) (h "0apq70qfr658rsy4pz3drbbvi5a59viy22m5mmk6hmc3qf3rv4i7") (f (quote (("static" "lcms2-sys/static"))))))

(define-public crate-lcms2-5.2.0-alpha (c (n "lcms2") (v "5.2.0-alpha") (d (list (d (n "foreign-types") (r "^0.4.0") (d #t) (k 0)) (d (n "lcms2-sys") (r "^3.1.2") (d #t) (k 0)))) (h "0s0dfm3rjv379y7c6d3mib65k33wvjycqz8wdf4ljmlmmg8dqx5c") (f (quote (("static" "lcms2-sys/static")))) (y #t)))

(define-public crate-lcms2-5.2.0 (c (n "lcms2") (v "5.2.0") (d (list (d (n "foreign-types") (r "^0.4.0") (d #t) (k 0)) (d (n "lcms2-sys") (r "^3.1.2") (d #t) (k 0)))) (h "1i3rn1cp9yin2m3vjrbjf1fqbmw8kjshli6gnp4l1vr4w84y8ds6") (f (quote (("static" "lcms2-sys/static"))))))

(define-public crate-lcms2-5.3.0-alpha (c (n "lcms2") (v "5.3.0-alpha") (d (list (d (n "foreign-types") (r "^0.5") (d #t) (k 0)) (d (n "lcms2-sys") (r "^3.1.2") (d #t) (k 0)))) (h "0knay5nj9x684s0bm3z6xp55ka9dqg0rcr7x762qc600k4qwwqif") (f (quote (("static" "lcms2-sys/static")))) (y #t)))

(define-public crate-lcms2-5.3.1 (c (n "lcms2") (v "5.3.1") (d (list (d (n "foreign-types") (r "^0.5") (d #t) (k 0)) (d (n "lcms2-sys") (r "^3.1.3") (d #t) (k 0)))) (h "1w645zm064ri6mm098wf7i7vyhcl1an5pi02yvai1m8yy5kc33nm") (f (quote (("static" "lcms2-sys/static"))))))

(define-public crate-lcms2-5.4.0 (c (n "lcms2") (v "5.4.0") (d (list (d (n "foreign-types") (r "^0.5") (d #t) (k 0)) (d (n "lcms2-sys") (r "^3.1.3") (d #t) (k 0)))) (h "06igsq8qpsihqmya6lsh09a7aqjbmvb9rvrxm8rfdbng28d20q8h") (f (quote (("static" "lcms2-sys/static"))))))

(define-public crate-lcms2-5.4.1 (c (n "lcms2") (v "5.4.1") (d (list (d (n "foreign-types") (r "^0.5") (d #t) (k 0)) (d (n "lcms2-sys") (r "^3.1.3") (d #t) (k 0)))) (h "1y5gig19n1p7ij67mhmb8y2xfic6x9zacxs214aiyabl3cg67xvl") (f (quote (("static" "lcms2-sys/static"))))))

(define-public crate-lcms2-5.5.0 (c (n "lcms2") (v "5.5.0") (d (list (d (n "foreign-types") (r "^0.5") (d #t) (k 0)) (d (n "lcms2-sys") (r "^4.0") (d #t) (k 0)))) (h "0nx4mbn9q9m3zczf96c71rwblav2bns4mmvzr55m2h7hrzqnqwsy") (f (quote (("static" "lcms2-sys/static"))))))

(define-public crate-lcms2-5.6.0 (c (n "lcms2") (v "5.6.0") (d (list (d (n "foreign-types") (r "^0.5") (d #t) (k 0)) (d (n "lcms2-sys") (r "^4.0") (d #t) (k 0)))) (h "0l7iv8p121d3lc6kwsx3nmwknfxz17m39ar3q07dl2fr016hmmdr") (f (quote (("static" "lcms2-sys/static"))))))

(define-public crate-lcms2-6.0.0-alpha.1 (c (n "lcms2") (v "6.0.0-alpha.1") (d (list (d (n "foreign-types") (r "^0.5") (d #t) (k 0)) (d (n "lcms2-sys") (r "^4.0.2") (d #t) (k 0)))) (h "11icmd5pzllh0ml2rz3rcj2654vz2az5wgpncv7by6s1waqx1si0") (f (quote (("static" "lcms2-sys/static")))) (y #t)))

(define-public crate-lcms2-6.0.0-alpha.2 (c (n "lcms2") (v "6.0.0-alpha.2") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "foreign-types") (r "^0.5") (d #t) (k 0)) (d (n "lcms2-sys") (r "^4.0.2") (d #t) (k 0)))) (h "1qcnm5rk27j4hviy45bhddjdssgws5xic2fcr130q8xi6z872ma2") (f (quote (("static" "lcms2-sys/static")))) (y #t)))

(define-public crate-lcms2-6.0.0 (c (n "lcms2") (v "6.0.0") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "foreign-types") (r "^0.5") (d #t) (k 0)) (d (n "lcms2-sys") (r "^4.0.3") (d #t) (k 0)))) (h "14hhqy93ay47j1wnb2hwrxzzb556vm6sg88pmic5rfskn8b7fyl9") (f (quote (("static" "lcms2-sys/static"))))))

(define-public crate-lcms2-6.0.2 (c (n "lcms2") (v "6.0.2") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "foreign-types") (r "^0.5") (d #t) (k 0)) (d (n "lcms2-sys") (r "^4.0.4") (d #t) (k 0)))) (h "0isc591d2c7nkhh5agr6pqvm9mr5aabq4i5j41gj1xrjafzraqas") (f (quote (("static" "lcms2-sys/static"))))))

(define-public crate-lcms2-6.0.3 (c (n "lcms2") (v "6.0.3") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "foreign-types") (r "^0.5") (d #t) (k 0)) (d (n "lcms2-sys") (r "^4.0.4") (d #t) (k 0)))) (h "1gg1amnna600fwgndgx9dyas2728bl3vc78x5kjcqmrcy75m6m3l") (f (quote (("static" "lcms2-sys/static"))))))

(define-public crate-lcms2-6.0.4 (c (n "lcms2") (v "6.0.4") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "foreign-types") (r "^0.5") (d #t) (k 0)) (d (n "lcms2-sys") (r "^4.0.4") (d #t) (k 0)))) (h "0q2spgn5y3j8x7s71mmbmklfzr9b7lfxcjg0na8pbp9x6rvd4j92") (f (quote (("static" "lcms2-sys/static"))))))

(define-public crate-lcms2-6.1.0 (c (n "lcms2") (v "6.1.0") (d (list (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "foreign-types") (r "^0.5") (d #t) (k 0)) (d (n "lcms2-sys") (r "^4.0.4") (d #t) (k 0)))) (h "0x3gps06mr2d55a6f3pfy73jj8m888vhmwh2rbwhlvn38bxc63k8") (f (quote (("static" "lcms2-sys/static"))))))

