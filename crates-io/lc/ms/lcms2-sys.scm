(define-module (crates-io lc ms lcms2-sys) #:use-module (crates-io))

(define-public crate-lcms2-sys-0.1.0 (c (n "lcms2-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1ldhj50bh3kajvgyw2hg4zk0r8gs7y0igl77grhx596kwzd2bnb1") (y #t)))

(define-public crate-lcms2-sys-0.1.2 (c (n "lcms2-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1awpxvfyla5agq0r246q5ai9p164vg23ggqywr43h6nkj6a6phfr") (y #t)))

(define-public crate-lcms2-sys-0.2.0 (c (n "lcms2-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0vk7idmlmw861039ydbkz5ss4apsiwnjqhp2g6bnfjaxgq2lvmi4") (y #t)))

(define-public crate-lcms2-sys-0.3.0 (c (n "lcms2-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "03sz8r4qijznbrqy3z1s6rg7716352d87fynsb127i4sicrx40yl") (y #t)))

(define-public crate-lcms2-sys-0.4.0 (c (n "lcms2-sys") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "011kmfxznsbsl45dzqx3ch1cr3wig6v39knklkf5pq8gc0dlmyqc") (y #t)))

(define-public crate-lcms2-sys-0.5.0 (c (n "lcms2-sys") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0lril16rbrwxpygdsc0ld0zxfgw8si1b3yy3z9rp3p4q1jfpfk97") (y #t)))

(define-public crate-lcms2-sys-1.0.0 (c (n "lcms2-sys") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0990xyjrc43ljkjg04xj8hbn822c8c0vmld5ixsvpj5nqks5q0rl") (y #t)))

(define-public crate-lcms2-sys-1.0.1 (c (n "lcms2-sys") (v "1.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "08krxi4pl6bh71hk80zfiwkgcll8b8drd7sj6mq6p032xvm7pyik") (y #t)))

(define-public crate-lcms2-sys-1.0.2 (c (n "lcms2-sys") (v "1.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0kz830iwnsc2jydk49wk33klipxilqyrnfbqvpa84pkz4yjsvg09") (y #t)))

(define-public crate-lcms2-sys-2.0.0 (c (n "lcms2-sys") (v "2.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "01azdrly8vc4dvkkrc52ahyjfs3c3a0k6vfzl8wj2lw81vjiw55p") (y #t)))

(define-public crate-lcms2-sys-2.1.0 (c (n "lcms2-sys") (v "2.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0rv2xinzsa88x6kyz9yycdiq0ch077cz8p433139kwa14z7y33dx") (y #t)))

(define-public crate-lcms2-sys-2.1.1 (c (n "lcms2-sys") (v "2.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0wni0fc2z7s0k9qyj4mf0qkb97iw7g2nb0dq535002mkbjkcxrv6") (y #t)))

(define-public crate-lcms2-sys-2.2.0 (c (n "lcms2-sys") (v "2.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0jnichf3kyrr3izlssv2min0y9a1j88gpd37s0w5g6lcc2zj134v") (y #t)))

(define-public crate-lcms2-sys-2.3.0 (c (n "lcms2-sys") (v "2.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "137pvrq9y0z1apfp26qcha5pc7d7q5affz4319vf5bsrzgyrczib") (y #t)))

(define-public crate-lcms2-sys-2.3.1 (c (n "lcms2-sys") (v "2.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "03x493sz1zirn1nlpxh1f7hqvfns288bl7kmgld2z59dy0360lrq") (y #t)))

(define-public crate-lcms2-sys-2.4.0 (c (n "lcms2-sys") (v "2.4.0") (d (list (d (n "gcc") (r "^0.3.51") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (o #t) (d #t) (k 1)))) (h "0l6hwx7pz3gfz8lwn8h15b47am46zk23yj37058j3mq4k0d3qbqa") (f (quote (("static" "gcc") ("dynamic" "pkg-config") ("default" "dynamic" "static")))) (y #t)))

(define-public crate-lcms2-sys-2.4.1 (c (n "lcms2-sys") (v "2.4.1") (d (list (d (n "gcc") (r "^0.3.51") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (o #t) (d #t) (k 1)))) (h "11bldc2pwm45gjh7p1pl4gl8kagy4l2y9ln7w8g3nmcllarkrgbz") (f (quote (("static" "gcc") ("dynamic" "pkg-config") ("default" "dynamic" "static")))) (y #t)))

(define-public crate-lcms2-sys-2.4.2 (c (n "lcms2-sys") (v "2.4.2") (d (list (d (n "gcc") (r "^0.3.51") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (o #t) (d #t) (k 1)))) (h "0l7grjl2wjirph63akv967bbgr56vysnaqnxl7kraywa72ap25v3") (f (quote (("static" "gcc") ("dynamic" "pkg-config") ("default" "dynamic")))) (y #t)))

(define-public crate-lcms2-sys-2.4.3 (c (n "lcms2-sys") (v "2.4.3") (d (list (d (n "gcc") (r "^0.3.51") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (o #t) (d #t) (k 1)))) (h "0i7xizix7zh4fvp41hqkqpl8cdcpp4yyrpq1ww6sgyp2dm5wk7ng") (f (quote (("static-fallback" "gcc") ("static" "gcc") ("dynamic" "pkg-config") ("default" "dynamic" "static-fallback")))) (y #t)))

(define-public crate-lcms2-sys-2.4.4 (c (n "lcms2-sys") (v "2.4.4") (d (list (d (n "gcc") (r "^0.3.51") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (o #t) (d #t) (k 1)))) (h "0j4hm6l5h1lrqslafid1clpv11di7h0mdwkdzvihxpz80kyansy3") (f (quote (("static-fallback" "gcc") ("static" "gcc") ("dynamic" "pkg-config") ("default" "dynamic" "static-fallback")))) (y #t)))

(define-public crate-lcms2-sys-2.4.5 (c (n "lcms2-sys") (v "2.4.5") (d (list (d (n "gcc") (r "^0.3.51") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (o #t) (d #t) (k 1)))) (h "0cjs8jl7zf1v2fy0ylxz5r0b1xagyqz8kz1n6dkvw88ibb31pgm0") (f (quote (("static-fallback" "gcc") ("static" "gcc") ("dynamic" "pkg-config") ("default" "dynamic" "static-fallback")))) (y #t)))

(define-public crate-lcms2-sys-2.4.6 (c (n "lcms2-sys") (v "2.4.6") (d (list (d (n "cc") (r "^1.0.0") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (o #t) (d #t) (k 1)))) (h "1f1gp5qp60viq0m9w22ssy1q2hp7drgfdixpilhxmrc3arjcilb3") (f (quote (("static-fallback" "cc") ("static" "cc") ("dynamic" "pkg-config") ("default" "dynamic" "static-fallback"))))))

(define-public crate-lcms2-sys-2.4.7 (c (n "lcms2-sys") (v "2.4.7") (d (list (d (n "cc") (r "^1.0.3") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (o #t) (d #t) (k 1)))) (h "0nkdnih2y9xzmx5sx8vmfpl06px9lvdlgrvyzjp99wn37qbv8y8g") (f (quote (("static-fallback" "cc") ("static" "cc") ("dynamic" "pkg-config") ("default" "dynamic" "static-fallback"))))))

(define-public crate-lcms2-sys-2.4.8 (c (n "lcms2-sys") (v "2.4.8") (d (list (d (n "cc") (r "^1.0.3") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "dunce") (r "^0.1.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (o #t) (d #t) (k 1)))) (h "09ksl4fqb4bbsv3dgs8iab1qbqnix3zjrc3f9gn8rws42zxy6qb5") (f (quote (("static-fallback" "cc") ("static" "cc") ("dynamic" "pkg-config") ("default" "dynamic" "static-fallback"))))))

(define-public crate-lcms2-sys-3.1.0 (c (n "lcms2-sys") (v "3.1.0") (d (list (d (n "cc") (r "^1.0.30") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "dunce") (r "^1.0.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (o #t) (d #t) (k 1)))) (h "0r4wbp23km1i8njnjjss8mnz51g297gc3yjf8z1ha4830spn7xmx") (f (quote (("static-fallback" "cc") ("static" "cc") ("dynamic" "pkg-config") ("default" "dynamic" "static-fallback")))) (l "lcms2")))

(define-public crate-lcms2-sys-3.1.1 (c (n "lcms2-sys") (v "3.1.1") (d (list (d (n "cc") (r "^1.0.30") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "dunce") (r "^1.0.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (o #t) (d #t) (k 1)))) (h "0bx929gnqgi56dm2pwi1gkp12c9cj325avjmkp89s6v4w1g007ry") (f (quote (("static-fallback" "cc") ("static" "cc") ("dynamic" "pkg-config") ("default" "dynamic" "static-fallback")))) (l "lcms2")))

(define-public crate-lcms2-sys-3.1.2 (c (n "lcms2-sys") (v "3.1.2") (d (list (d (n "cc") (r "^1.0.30") (o #t) (d #t) (k 1)) (d (n "dunce") (r "^1.0.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (o #t) (d #t) (k 1)))) (h "14c1kl7c05961xmi9km2ng7y920a2p487fgy0ppkf6s400bgr5wj") (f (quote (("static-fallback" "cc") ("static" "cc") ("parallel" "cc/parallel") ("dynamic" "pkg-config") ("default" "dynamic" "static-fallback" "parallel")))) (l "lcms2")))

(define-public crate-lcms2-sys-3.1.3 (c (n "lcms2-sys") (v "3.1.3") (d (list (d (n "cc") (r "^1.0.52") (o #t) (d #t) (k 1)) (d (n "dunce") (r "^1.0.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (o #t) (d #t) (k 1)))) (h "1qkfijg9n9kkml2ql38f5wrm8nskxgxdabkjz6s7b0aswyjj1nya") (f (quote (("static-fallback" "cc") ("static" "cc") ("parallel" "cc/parallel") ("dynamic" "pkg-config") ("default" "dynamic" "static-fallback" "parallel")))) (l "lcms2")))

(define-public crate-lcms2-sys-3.1.4 (c (n "lcms2-sys") (v "3.1.4") (d (list (d (n "cc") (r "^1.0.52") (o #t) (d #t) (k 1)) (d (n "dunce") (r "^1.0.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (o #t) (d #t) (k 1)))) (h "193ifxqkkv7qaij8r0dbdhhk3jy6vbk612ps8bk2f34c2mqjyzdv") (f (quote (("static-fallback" "cc") ("static" "cc") ("parallel" "cc/parallel") ("dynamic" "pkg-config") ("default" "dynamic" "static-fallback" "parallel")))) (l "lcms2")))

(define-public crate-lcms2-sys-3.1.5 (c (n "lcms2-sys") (v "3.1.5") (d (list (d (n "cc") (r "^1.0.52") (o #t) (d #t) (k 1)) (d (n "dunce") (r "^1.0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (o #t) (d #t) (k 1)))) (h "0iywyyvcll6q70visw7j5wj4dgraq7svxvm5kqkzmxqv7r0yndw4") (f (quote (("static-fallback" "cc") ("static" "cc") ("parallel" "cc/parallel") ("dynamic" "pkg-config") ("default" "dynamic" "static-fallback" "parallel")))) (l "lcms2")))

(define-public crate-lcms2-sys-3.1.6 (c (n "lcms2-sys") (v "3.1.6") (d (list (d (n "cc") (r "^1.0.66") (o #t) (d #t) (k 1)) (d (n "dunce") (r "^1.0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (o #t) (d #t) (k 1)))) (h "1vyhik19k1lpl297n6336741kwx3dpvqc8yrr7zgvzggg7pxvxq4") (f (quote (("static-fallback" "cc") ("static" "cc") ("parallel" "cc/parallel") ("dynamic" "pkg-config") ("default" "dynamic" "static-fallback" "parallel")))) (l "lcms2")))

(define-public crate-lcms2-sys-3.1.7 (c (n "lcms2-sys") (v "3.1.7") (d (list (d (n "cc") (r "^1.0.66") (o #t) (d #t) (k 1)) (d (n "dunce") (r "^1.0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (o #t) (d #t) (k 1)))) (h "1rwlm414046zkn1awm5bqkgsnchik25l5xzpkxgdjsihrnqjkzyw") (f (quote (("static-fallback" "cc") ("static" "cc") ("parallel" "cc/parallel") ("dynamic" "pkg-config") ("default" "dynamic" "static-fallback" "parallel")))) (l "lcms2")))

(define-public crate-lcms2-sys-3.1.8 (c (n "lcms2-sys") (v "3.1.8") (d (list (d (n "cc") (r "^1.0.66") (o #t) (d #t) (k 1)) (d (n "dunce") (r "^1.0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (o #t) (d #t) (k 1)))) (h "14z8j1argiwh7g65bqmrn17c734wr102n5anrhgp972av43wazi4") (f (quote (("static-fallback" "cc") ("static" "cc") ("parallel" "cc/parallel") ("dynamic" "pkg-config") ("default" "dynamic" "static-fallback" "parallel")))) (l "lcms2")))

(define-public crate-lcms2-sys-3.1.9 (c (n "lcms2-sys") (v "3.1.9") (d (list (d (n "cc") (r "^1.0.66") (o #t) (d #t) (k 1)) (d (n "dunce") (r "^1.0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (o #t) (d #t) (k 1)))) (h "0k3iq12x4m4p5x4wp25llf6h88q1ngmfjvnd6fcpp4ncb4vcsx0v") (f (quote (("static-fallback" "cc") ("static" "cc") ("parallel" "cc/parallel") ("dynamic" "pkg-config") ("default" "dynamic" "static-fallback" "parallel")))) (l "lcms2")))

(define-public crate-lcms2-sys-3.1.10 (c (n "lcms2-sys") (v "3.1.10") (d (list (d (n "cc") (r "^1.0.73") (o #t) (d #t) (k 1)) (d (n "dunce") (r "^1.0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.129") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.25") (o #t) (d #t) (k 1)))) (h "1m62knkzjxr2j6d91xq9kmqrpg8r9ff30361zrx63322xasiy459") (f (quote (("static-fallback" "cc") ("static" "cc") ("parallel" "cc/parallel") ("dynamic" "pkg-config") ("default" "dynamic" "static-fallback" "parallel")))) (l "lcms2")))

(define-public crate-lcms2-sys-4.0.0 (c (n "lcms2-sys") (v "4.0.0") (d (list (d (n "cc") (r "^1.0.73") (o #t) (d #t) (k 1)) (d (n "dunce") (r "^1.0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.129") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.25") (o #t) (d #t) (k 1)))) (h "1a3qj39l8wh3628lfvbryavj5skgpph09v7vmv2i1wm89zxkd0lh") (f (quote (("static-fallback" "cc") ("static" "cc") ("parallel" "cc/parallel") ("dynamic" "pkg-config") ("default" "dynamic" "static-fallback" "parallel")))) (l "lcms2")))

(define-public crate-lcms2-sys-4.0.1 (c (n "lcms2-sys") (v "4.0.1") (d (list (d (n "cc") (r "^1.0.73") (o #t) (d #t) (k 1)) (d (n "dunce") (r "^1.0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.129") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.25") (o #t) (d #t) (k 1)))) (h "0bv8ilsa30x18jfq82yj9hbgfyrshyin2m52zbgqrz873an8mfvs") (f (quote (("static-fallback" "cc") ("static" "cc") ("parallel" "cc/parallel") ("dynamic" "pkg-config") ("default" "dynamic" "static-fallback" "parallel")))) (l "lcms2")))

(define-public crate-lcms2-sys-4.0.2 (c (n "lcms2-sys") (v "4.0.2") (d (list (d (n "cc") (r "^1.0.73") (o #t) (d #t) (k 1)) (d (n "dunce") (r "^1.0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.129") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.25") (o #t) (d #t) (k 1)))) (h "18r2zyda9fzfyjdm2rh6w8pa9k7zw104lyvbhl0hi92z5a87n1c1") (f (quote (("static-fallback" "cc") ("static" "cc") ("parallel" "cc/parallel") ("dynamic" "pkg-config") ("default" "dynamic" "static-fallback" "parallel")))) (l "lcms2")))

(define-public crate-lcms2-sys-4.0.3 (c (n "lcms2-sys") (v "4.0.3") (d (list (d (n "cc") (r "^1.0.73") (o #t) (d #t) (k 1)) (d (n "dunce") (r "^1.0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.129") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.25") (o #t) (d #t) (k 1)))) (h "0n3v0v6kvx9mbhlm6jnivjfnzw965saf4wzz55kydfbs35bawz8k") (f (quote (("lcms2-strict-cgats") ("default" "dynamic" "static-fallback" "parallel")))) (l "lcms2") (s 2) (e (quote (("static-fallback" "dep:cc") ("static" "dep:cc") ("parallel" "cc?/parallel") ("dynamic" "dep:pkg-config"))))))

(define-public crate-lcms2-sys-4.0.4 (c (n "lcms2-sys") (v "4.0.4") (d (list (d (n "cc") (r "^1.0.73") (o #t) (d #t) (k 1)) (d (n "dunce") (r "^1.0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.129") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.25") (o #t) (d #t) (k 1)))) (h "0di5w5n2j6lc71iwa2hglixrsqnlmwr8d5x0fi86i8b8d63zxicr") (f (quote (("lcms2-strict-cgats") ("default" "dynamic" "static-fallback" "parallel")))) (l "lcms2") (s 2) (e (quote (("static-fallback" "dep:cc") ("static" "dep:cc") ("parallel" "cc?/parallel") ("dynamic" "dep:pkg-config"))))))

(define-public crate-lcms2-sys-4.0.5 (c (n "lcms2-sys") (v "4.0.5") (d (list (d (n "cc") (r "^1.0.73") (o #t) (d #t) (k 1)) (d (n "dunce") (r "^1.0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.129") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.25") (o #t) (d #t) (k 1)))) (h "0bixcbxvgsvll5nl26abjaz333rl67p809dn9w18088plgwnacjr") (f (quote (("lcms2-strict-cgats") ("default" "dynamic" "static-fallback" "parallel")))) (l "lcms2") (s 2) (e (quote (("static-fallback" "dep:cc") ("static" "dep:cc") ("parallel" "cc?/parallel") ("dynamic" "dep:pkg-config"))))))

