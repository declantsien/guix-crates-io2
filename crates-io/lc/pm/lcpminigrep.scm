(define-module (crates-io lc pm lcpminigrep) #:use-module (crates-io))

(define-public crate-LCPminigrep-0.1.0 (c (n "LCPminigrep") (v "0.1.0") (h "0sxgcck5ws3jczcixg1cpwmgmlnns408mk7a8ifp6fbpy4dl9x60")))

(define-public crate-LCPminigrep-0.2.0 (c (n "LCPminigrep") (v "0.2.0") (h "01ax2cw3n7yyabisv0ip9rfl7q1wcw3qqysldz8n9w49zfdkgjzi")))

(define-public crate-LCPminigrep-0.1.2 (c (n "LCPminigrep") (v "0.1.2") (h "00vfcb7cni5r2ajsx5apsj79ygkd6x5k5gcp7wl5881xnwn4dz82")))

(define-public crate-LCPminigrep-0.2.1 (c (n "LCPminigrep") (v "0.2.1") (h "1par5ipszlkj8vfzymfl1yqcpgb8zvxwkw6ly4qvss9l2nhml42j")))

