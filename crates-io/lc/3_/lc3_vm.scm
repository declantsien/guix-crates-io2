(define-module (crates-io lc #{3_}# lc3_vm) #:use-module (crates-io))

(define-public crate-lc3_vm-0.1.0 (c (n "lc3_vm") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "libc") (r "^0.2.45") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.6") (d #t) (k 0)) (d (n "termios") (r "^0.3.1") (d #t) (k 0)))) (h "1b411b68hv3fd7g9rcjpm7g23lnyy4aic1ks68k87pgn3qy7nnsr")))

