(define-module (crates-io lc ov lcov-util) #:use-module (crates-io))

(define-public crate-lcov-util-0.1.0 (c (n "lcov-util") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lcov") (r "^0.1") (d #t) (k 0)))) (h "0zl9lwhiv4yn53vd888lv9kp0lvip6lh5mi4fl7f2vn9c5lm8j84")))

(define-public crate-lcov-util-0.1.1 (c (n "lcov-util") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lcov") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0s9kbnrdalv46p01zvx0qq078i73g7qd67i4qahxcbqrp3gzw874")))

(define-public crate-lcov-util-0.1.2 (c (n "lcov-util") (v "0.1.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lcov") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1fdqnd7dvfcz9wqiyjz3rl74z5bzjwh5klvx95249vnawaa22v6y")))

(define-public crate-lcov-util-0.1.3 (c (n "lcov-util") (v "0.1.3") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lcov") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "14fa5jhbr8h2xvfz379pll7xd85h97mh9w45pfaspgy0r4g40nnv")))

(define-public crate-lcov-util-0.1.4 (c (n "lcov-util") (v "0.1.4") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lcov") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0xd59ff5ah34g821sajk1w8ahdy98f1v7i8qjd7r0ask85ndvdw0")))

(define-public crate-lcov-util-0.1.5 (c (n "lcov-util") (v "0.1.5") (d (list (d (n "lcov") (r "^0.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)))) (h "09hvj2prpg551fslvap4f1100wjl4b5p5nan1mqmm9qiqnhlw9ds")))

(define-public crate-lcov-util-0.1.6 (c (n "lcov-util") (v "0.1.6") (d (list (d (n "lcov") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "1hjdwpqlz9k1dy6jf42g9d0fkv9g2ci5kvr6ma2ysm0gbxj0rp3b")))

(define-public crate-lcov-util-0.1.7 (c (n "lcov-util") (v "0.1.7") (d (list (d (n "lcov") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "07jgh3b2a2y1jqld5yal40968y9fr9cdxgpsmvvd4vdxrddipwpq")))

(define-public crate-lcov-util-0.1.8 (c (n "lcov-util") (v "0.1.8") (d (list (d (n "lcov") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "12w496l337p3mxg3dqlp7y3kb52b3iwzdfq13d6zwsf2ycgcpdh3")))

(define-public crate-lcov-util-0.1.9 (c (n "lcov-util") (v "0.1.9") (d (list (d (n "lcov") (r "^0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "0526sb928na1skf9d00hjy1pp93sj463z883hwhbvc616zffs0k0")))

(define-public crate-lcov-util-0.2.0 (c (n "lcov-util") (v "0.2.0") (d (list (d (n "lcov") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "0azzlksy7r0v0jasmcgkx9c2sj81xib8iwa0zjxw7vk7l688q8h4") (r "1.56.1")))

(define-public crate-lcov-util-0.2.1 (c (n "lcov-util") (v "0.2.1") (d (list (d (n "lcov") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "05bzim5awb7wl97q1vxf70z8apvia8i0xhbqiifvfc8wnwg329nz") (r "1.56.1")))

