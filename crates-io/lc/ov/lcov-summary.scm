(define-module (crates-io lc ov lcov-summary) #:use-module (crates-io))

(define-public crate-lcov-summary-0.1.0 (c (n "lcov-summary") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.23") (d #t) (k 0)))) (h "17lm77b7gjqb8x410d78acl5an4smgvblx5lhnj24n0nh4viv1pf")))

(define-public crate-lcov-summary-0.2.0 (c (n "lcov-summary") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.23") (d #t) (k 0)))) (h "0jd1p3x9d5k4x98s0fh3c2vnp9p92hrzphn36h6z09fx6wdc6aid")))

(define-public crate-lcov-summary-0.2.1 (c (n "lcov-summary") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.23") (d #t) (k 0)))) (h "1vyam71z51agjaqlq407kjal9k7f8749qhvw8cvf8kmlgy1v7zsj")))

