(define-module (crates-io lc ov lcov) #:use-module (crates-io))

(define-public crate-lcov-0.1.0 (c (n "lcov") (v "0.1.0") (d (list (d (n "cargo-readme") (r "^2.0") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "1gkpbq857prja3v5dd3maa6g7pyxx2fgn7wx2iqzvqdhs2yg6dy3")))

(define-public crate-lcov-0.1.1 (c (n "lcov") (v "0.1.1") (d (list (d (n "cargo-readme") (r "^2.0") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "14756v8sw9vr1g75nm2v5rq1s4qw1wz8xpg17q456ydkm32qxjay")))

(define-public crate-lcov-0.1.2 (c (n "lcov") (v "0.1.2") (d (list (d (n "cargo-readme") (r "^2.0") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "1v0lfnfnvlxbyhcvbd4fq956mlcdz4p36gmgfdd26qrqk3blrgpp")))

(define-public crate-lcov-0.2.0 (c (n "lcov") (v "0.2.0") (d (list (d (n "cargo-readme") (r "^2.0") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "1kfmfk0g961076kp65zlpx7qi180wsx3czzar05dwjl1y8yhd6k7")))

(define-public crate-lcov-0.3.0 (c (n "lcov") (v "0.3.0") (d (list (d (n "cargo-readme") (r "^2.0") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "19n3gf7a1kq2bvbz6ghp8rr852j23h4ijzjdawbayac9wrbxwj2l")))

(define-public crate-lcov-0.4.0 (c (n "lcov") (v "0.4.0") (d (list (d (n "cargo-readme") (r "^2.0") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "0kpzqv9vq3xm9jiz84n9ygpbddqgaf7cpv6zpiv135c0z5nhmx4d")))

(define-public crate-lcov-0.4.1 (c (n "lcov") (v "0.4.1") (d (list (d (n "cargo-readme") (r "^2.0") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "112655mzv22s3xfbps7vycm5w3y84mbflnn3616888pqwg7qv4q7")))

(define-public crate-lcov-0.4.2 (c (n "lcov") (v "0.4.2") (d (list (d (n "cargo-readme") (r "^2.0") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "1bwdrgpsxvk0i54s7rfl7zgr48ik1zzfqkzcb52x3vw7hxzr17jg")))

(define-public crate-lcov-0.5.0 (c (n "lcov") (v "0.5.0") (d (list (d (n "cargo-readme") (r "^2.0") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 2)) (d (n "matches") (r "^0.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "1nhk83ab8cc1vv9cllddnv2x85xk1hvcs90jl2y5v4723x3ic9s6")))

(define-public crate-lcov-0.6.0 (c (n "lcov") (v "0.6.0") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "matches") (r "^0.1.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "0m0fpgd0qz585jly1qchds9asikknsf5rxnqq7j11bjqngx93cjk")))

(define-public crate-lcov-0.7.0 (c (n "lcov") (v "0.7.0") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "matches") (r "^0.1.9") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.3") (d #t) (k 2)))) (h "00yd7wfs4i7k2wf59v0qn581gaap2l9s4719jzcrf49jrxl67z73")))

(define-public crate-lcov-0.7.2 (c (n "lcov") (v "0.7.2") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "matches") (r "^0.1.9") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.3") (d #t) (k 2)))) (h "0rksiqvhb5dpcdhlha7c0xghqw5v060q2jn3nidwi09jr5w1wwcq")))

(define-public crate-lcov-0.8.0 (c (n "lcov") (v "0.8.0") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "matches") (r "^0.1.9") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "0nx4w5f21zna90kcrgkb5y2bdcfya4w73s2pqp0rskp6y6rlr7dh") (r "1.56.1")))

(define-public crate-lcov-0.8.1 (c (n "lcov") (v "0.8.1") (d (list (d (n "cargo-readme") (r "^3.2.0") (d #t) (k 2)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "matches") (r "^0.1.9") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "08xph2prpzlh19fw6jl89pvylr239qc3hzxkcpdq9a45wpasdkqw") (r "1.56.1")))

