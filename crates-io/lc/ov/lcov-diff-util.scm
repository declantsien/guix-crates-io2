(define-module (crates-io lc ov lcov-diff-util) #:use-module (crates-io))

(define-public crate-lcov-diff-util-0.2.0 (c (n "lcov-diff-util") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "lcov") (r "^0.6") (d #t) (k 0)) (d (n "lcov-diff") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)))) (h "070rkyc4jwv7plzbzd4j4fqcmh0d5nkk2sp4wihrk5whqzwlvp1g")))

(define-public crate-lcov-diff-util-0.2.1 (c (n "lcov-diff-util") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "lcov") (r "^0.8") (d #t) (k 0)) (d (n "lcov-diff") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)))) (h "1wf3ivy3mrb26q3g82f9r9acqvk9w56iybbcgnbl86wf414qky0g")))

