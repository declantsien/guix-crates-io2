(define-module (crates-io lc ov lcoviz) #:use-module (crates-io))

(define-public crate-lcoviz-0.1.0 (c (n "lcoviz") (v "0.1.0") (d (list (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "lcov") (r "^0.8.1") (d #t) (k 0)) (d (n "mockall") (r "^0.12.1") (d #t) (k 2)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "1qy2xf2qiiqygjgywbr5h6cc2hs8fjgh0i0whnwckdjxjif381zq")))

(define-public crate-lcoviz-0.1.1 (c (n "lcoviz") (v "0.1.1") (d (list (d (n "htmlescape") (r "^0.3.1") (d #t) (k 0)) (d (n "lcov") (r "^0.8.1") (d #t) (k 0)) (d (n "mockall") (r "^0.12.1") (d #t) (k 2)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "0g6hw0774dbfzr1p9jimm8f6p3q7nd39sw9xnn6nkah0r94z64b7")))

