(define-module (crates-io lc ov lcov2cobertura) #:use-module (crates-io))

(define-public crate-lcov2cobertura-1.0.0 (c (n "lcov2cobertura") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "quick-xml") (r "^0.26.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.21") (d #t) (k 0)))) (h "0pfqv99w3s7kvnyhapmz7vz6p3ap47gdmld7586yr89r5bq8qjsv")))

(define-public crate-lcov2cobertura-1.0.1 (c (n "lcov2cobertura") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "quick-xml") (r "^0.26.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.21") (d #t) (k 0)))) (h "0b8nvv1b8269sv4lckiazg87p7j8l8zn2ljhkg3sks6anhwyp26g") (r "1.59")))

(define-public crate-lcov2cobertura-1.0.2 (c (n "lcov2cobertura") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "quick-xml") (r "^0.30.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.21") (d #t) (k 0)))) (h "1nx364adpfa6fmaklv20dsnh8mvni87b4wrs8hs2a2bh341yrmh7") (r "1.59")))

(define-public crate-lcov2cobertura-1.0.3 (c (n "lcov2cobertura") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.23") (d #t) (k 0)))) (h "00swx6r497ij61s8yh88d2kxhiijwd2139s0nivw489ld6azknqk") (r "1.59")))

