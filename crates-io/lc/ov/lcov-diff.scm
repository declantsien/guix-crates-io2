(define-module (crates-io lc ov lcov-diff) #:use-module (crates-io))

(define-public crate-lcov-diff-0.1.0 (c (n "lcov-diff") (v "0.1.0") (d (list (d (n "lcov") (r "^0.6") (d #t) (k 0)))) (h "0glnvhx7137jxpp7hk783b6w52ggxly5w1aq4nwgxy12hjr7w93x")))

(define-public crate-lcov-diff-0.1.1 (c (n "lcov-diff") (v "0.1.1") (d (list (d (n "lcov") (r "^0.6") (d #t) (k 0)))) (h "01hvqk1a54nfkrm7fay9bq4dfbxcpy66j2prqzl186z01k9m9frn")))

(define-public crate-lcov-diff-0.1.2 (c (n "lcov-diff") (v "0.1.2") (d (list (d (n "lcov") (r "^0.8") (d #t) (k 0)))) (h "05w0f1pfhbmxvi8vzmfxv3w978inm5apsd7d5c8qzsmn10d4rxlq")))

