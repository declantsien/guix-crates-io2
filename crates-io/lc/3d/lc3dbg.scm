(define-module (crates-io lc #{3d}# lc3dbg) #:use-module (crates-io))

(define-public crate-lc3dbg-1.0.0 (c (n "lc3dbg") (v "1.0.0") (d (list (d (n "arraydeque") (r "^0.4") (d #t) (k 0)) (d (n "console") (r "^0.7") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lc3-rs") (r "^0.2") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2") (d #t) (k 0)))) (h "0d19h5hjv3nm3d8wn5kynfigihkqicrzcf69xdhh7w3g2khz3xa0")))

(define-public crate-lc3dbg-1.0.1 (c (n "lc3dbg") (v "1.0.1") (d (list (d (n "arraydeque") (r "^0.4") (d #t) (k 0)) (d (n "console") (r "^0.7") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lc3-rs") (r "^0.2") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2") (d #t) (k 0)))) (h "0icildjyis1ffz7wrz8kdyb59aycjf4m2507y87lbzn5lkj6n029")))

(define-public crate-lc3dbg-1.1.0 (c (n "lc3dbg") (v "1.1.0") (d (list (d (n "arraydeque") (r "^0.4") (d #t) (k 0)) (d (n "console") (r "^0.7") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "lc3-rs") (r "^0.2") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "1xmxjvkjv0iwxxp0pk8mhfrwxgdaaiamlxll6hpbk1lapnzj4swb")))

(define-public crate-lc3dbg-1.1.1 (c (n "lc3dbg") (v "1.1.1") (d (list (d (n "arraydeque") (r "^0.4") (d #t) (k 0)) (d (n "console") (r "^0.7") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "lc3-rs") (r "^0.2") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "0p3rlpd71a4m28s75hliky8mc0q5v1fqjalcqzla6d9qkyc8g7nl")))

(define-public crate-lc3dbg-1.1.2 (c (n "lc3dbg") (v "1.1.2") (d (list (d (n "arraydeque") (r "^0.4") (d #t) (k 0)) (d (n "console") (r "^0.7") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "lc3-rs") (r "^0.2") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "1gh2q0wssb1qdhxr24748kigidhiva95g5ld7wlbjd5zm218sss6")))

(define-public crate-lc3dbg-1.2.0 (c (n "lc3dbg") (v "1.2.0") (d (list (d (n "arraydeque") (r "^0.4") (d #t) (k 0)) (d (n "console") (r "^0.7") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "lc3-rs") (r "^0.2") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "1cfp4hns7rxp2rzhdslcvhavw3sw94r913y5cj46hbwzamczm3x1")))

(define-public crate-lc3dbg-1.2.1 (c (n "lc3dbg") (v "1.2.1") (d (list (d (n "arraydeque") (r "^0.4") (d #t) (k 0)) (d (n "console") (r "^0.7") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "lc3-rs") (r "^0.3") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "1hibhk2wmw0bhqxnxcamzwrfznlak990xc6zf0722g3b10qvz3bl")))

(define-public crate-lc3dbg-1.2.2 (c (n "lc3dbg") (v "1.2.2") (d (list (d (n "arraydeque") (r "^0.4") (d #t) (k 0)) (d (n "console") (r "^0.7") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "lc3-rs") (r "^0.4") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "06cch3b55qssba3m7213csgmvhjrqvh06mz3v8kv9k215zx9z8gk")))

