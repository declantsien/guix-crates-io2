(define-module (crates-io lc id lcid) #:use-module (crates-io))

(define-public crate-lcid-0.1.0 (c (n "lcid") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1z6h1qykycsbwg1ry53mq9xbzabw0q17rii8l44mg1b6hnmf9il7")))

(define-public crate-lcid-0.2.0 (c (n "lcid") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0igkzislnjc7pa8x6rp256h6b0xshk700q3fqblpdpiwg07pykx7")))

(define-public crate-lcid-0.2.1 (c (n "lcid") (v "0.2.1") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)))) (h "1qxgkqy2fjp7ga1aci3d6hkj22smm7359p9yhgqry2k9zdakicnp") (r "1.56")))

(define-public crate-lcid-0.3.0 (c (n "lcid") (v "0.3.0") (h "1kjj5z5hb55dibrb9rbshvg1vqyfxpz3xa5nvjh60ksf7x4x0r0s") (r "1.56")))

