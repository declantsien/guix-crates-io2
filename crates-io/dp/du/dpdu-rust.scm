(define-module (crates-io dp du dpdu-rust) #:use-module (crates-io))

(define-public crate-dpdu-rust-0.8.0 (c (n "dpdu-rust") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "17s6wa7h2pcfqbkrps56cywsjv3j3hqwfaxw4wc82bpfvv7lr86l")))

(define-public crate-dpdu-rust-0.8.5 (c (n "dpdu-rust") (v "0.8.5") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "17886zzsliambiqg7ywg31lnjy3wwpha460kz3rhqamvmci3injp")))

(define-public crate-dpdu-rust-0.8.6 (c (n "dpdu-rust") (v "0.8.6") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)))) (h "0wshjqhzwdx73psv5mqn0rwxxh4r2fxjy7fg6lbh09gs7chj1x7b")))

