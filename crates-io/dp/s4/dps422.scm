(define-module (crates-io dp s4 dps422) #:use-module (crates-io))

(define-public crate-dps422-0.1.0 (c (n "dps422") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.2") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.12") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "stm32g0xx-hal") (r "^0.0.7") (f (quote ("stm32g07x" "rt"))) (d #t) (k 2)))) (h "195q23qqyifahld9shq82lvz6bf4kywbwawsxgsi0bvsb8wsz9w3")))

(define-public crate-dps422-0.1.1 (c (n "dps422") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.6.2") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.12") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "stm32g0xx-hal") (r "^0.0.7") (f (quote ("stm32g07x" "rt"))) (d #t) (k 2)))) (h "07fxkvsmylrc9ij0446sqlhi5a5ym5b30nskj292z8nvvaqk6k0m")))

