(define-module (crates-io dp tr dptree) #:use-module (crates-io))

(define-public crate-dptree-0.1.0 (c (n "dptree") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros" "sync"))) (d #t) (k 2)))) (h "1bw2vsm08zhzmybgvgx845qk717xy999m26lw8lkh0a8d5rb7f0n")))

(define-public crate-dptree-0.1.1 (c (n "dptree") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros" "sync"))) (d #t) (k 2)))) (h "1rx3k4g6plvqjz3gzjp57pr0xyq5dpjmr5nb36ayckpfhw8ca1wx")))

(define-public crate-dptree-0.1.2 (c (n "dptree") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros" "sync"))) (d #t) (k 2)))) (h "1g8aikqwsfqkqvz688z2g4nbbs564l21f974inil9f0mvwvw9z5p")))

(define-public crate-dptree-0.2.0 (c (n "dptree") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros" "sync"))) (d #t) (k 2)))) (h "1hkm4gag567j6ji77abl8ms3cv3x3z4mksmg3bvhxg1hcz8d93ci")))

(define-public crate-dptree-0.2.1 (c (n "dptree") (v "0.2.1") (d (list (d (n "futures") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros" "sync"))) (d #t) (k 2)))) (h "0gfa1jv6ix969i07jk4k44d4d3ga8wkin9r2l2m1cp5xh0nqs0ch")))

(define-public crate-dptree-0.3.0 (c (n "dptree") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("alloc"))) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros" "sync"))) (d #t) (k 2)))) (h "0p165vl0gc91nc5h00n340bp5qa4qb92xpvn0l7c6ygcnpd7a4fq")))

