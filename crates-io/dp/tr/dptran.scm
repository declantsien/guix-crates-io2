(define-module (crates-io dp tr dptran) #:use-module (crates-io))

(define-public crate-dptran-1.0.0 (c (n "dptran") (v "1.0.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0iqhdqaky6bq2syiy1nlk86qkklf4s3dvhjxmz4g9g53f2msl1bb")))

(define-public crate-dptran-1.0.1 (c (n "dptran") (v "1.0.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "13f65jfl360b9lz23ylb4zrkrcr5r14qb5b70p3p76kx892af2aj")))

(define-public crate-dptran-1.0.2 (c (n "dptran") (v "1.0.2") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0wjj5vk5s923nd9xvpr2y7dhqdflk8fhm6f4ma240cm8s73nia0b")))

(define-public crate-dptran-2.0.0 (c (n "dptran") (v "2.0.0") (d (list (d (n "atty") (r "^0.2.14") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1l41hy5mx31fnyycqpvvyk06lxww92ng6whqjfxb75k7jygyvjj5") (f (quote (("app" "confy" "clap" "atty"))))))

