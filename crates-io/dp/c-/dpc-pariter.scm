(define-module (crates-io dp c- dpc-pariter) #:use-module (crates-io))

(define-public crate-dpc-pariter-0.1.0 (c (n "dpc-pariter") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rusty_pool") (r "^0.6") (k 0)))) (h "19bkj6k1lgglh5ckq0bgv6yj95c3nxd87zyyx47y9qicin41qwhg")))

(define-public crate-dpc-pariter-0.2.0 (c (n "dpc-pariter") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rusty_pool") (r "^0.6") (k 0)))) (h "1d67kmw9nh5rymvkfcjq17lsyssmln1fh180na4897s15z02bdzd")))

(define-public crate-dpc-pariter-0.2.1 (c (n "dpc-pariter") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rusty_pool") (r "^0.6") (k 0)))) (h "0aadlsz465lay36dxphj869pf4rmgvd65kyrbpkh8j450blfgmz8")))

(define-public crate-dpc-pariter-0.3.0 (c (n "dpc-pariter") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (f (quote ("std"))) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "1f5r5cpby2s8nmrksz4q4zwl2y16f7xizlwyzvlx606s7kfd5y6s")))

(define-public crate-dpc-pariter-0.3.1 (c (n "dpc-pariter") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (f (quote ("std"))) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "0plhzdbsfgb0rj0692118wylmrh857kcs7m59dqrdng60zc0c76p")))

(define-public crate-dpc-pariter-0.4.0 (c (n "dpc-pariter") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (f (quote ("std"))) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "0s546d43yyiw55jz3yw4nyxgzmnc4f0gamzkfi6m7kyw2xlf2anl")))

(define-public crate-dpc-pariter-0.5.0 (c (n "dpc-pariter") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (f (quote ("std"))) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)))) (h "19h1zm2m6ia8vglg6akkvi087c8s6prfgy9arrbwx0r2h933bp9g")))

(define-public crate-dpc-pariter-0.5.1 (c (n "dpc-pariter") (v "0.5.1") (d (list (d (n "pariter") (r "^0.5") (d #t) (k 0)))) (h "148wibypq0227skvrgdc2jx673jsgxm4hxwvgrf1608x0vh60vqs")))

