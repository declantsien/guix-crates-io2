(define-module (crates-io dp c- dpc-rdup-du) #:use-module (crates-io))

(define-public crate-dpc-rdup-du-0.1.0 (c (n "dpc-rdup-du") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)))) (h "07d7rp3rcrfsyy4hg6r6k8ddki4zi65nv57gr8gpf4vf8pv2ffyp")))

(define-public crate-dpc-rdup-du-0.1.1 (c (n "dpc-rdup-du") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)))) (h "1pdr3r111yqmh0q0ngkn4wl2yj9i42am1xk97rxdzz8g4a3crwvf")))

(define-public crate-dpc-rdup-du-0.2.0 (c (n "dpc-rdup-du") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)))) (h "1r1smjnkxl0wnn3gr5s13c6snl9wjxsjbqlld96lljrycvd12yl3")))

