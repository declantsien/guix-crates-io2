(define-module (crates-io dp c- dpc-simplemap) #:use-module (crates-io))

(define-public crate-dpc-simplemap-0.0.1 (c (n "dpc-simplemap") (v "0.0.1") (d (list (d (n "rand") (r "*") (d #t) (k 2)))) (h "0cnz0bwpjw234fq5zbsccnx8jg34ysrrgk1l4r04sqky244cprsl") (f (quote (("default") ("bench"))))))

(define-public crate-dpc-simplemap-0.0.2 (c (n "dpc-simplemap") (v "0.0.2") (d (list (d (n "fnv") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "19l9fyhzzvz8jfd4cjxib03i9zrzhdghdbrv829d80hfjchnycw9") (f (quote (("default") ("bench"))))))

(define-public crate-dpc-simplemap-0.0.3 (c (n "dpc-simplemap") (v "0.0.3") (d (list (d (n "fnv") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "0p5mv22d6zlcycpldg4ygrsxfqvkplqdrk2amrnmzs5cz32hv4ry") (f (quote (("default") ("bench"))))))

(define-public crate-dpc-simplemap-0.1.0 (c (n "dpc-simplemap") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "1pxisqqz5plzh4hw9cpg9c0szp41rlp2asaak4y9n0gsgqp0knda") (f (quote (("default") ("bench"))))))

