(define-module (crates-io dp s3 dps310) #:use-module (crates-io))

(define-public crate-dps310-0.1.0 (c (n "dps310") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.6.4") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.13") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "stm32f3xx-hal-v2") (r "^0.5.0") (f (quote ("stm32f303xc"))) (d #t) (k 0)))) (h "0h60w6fr29rnmpfn9rff8q2md3x6klg7ys6ph69y6ccn31cv6wld") (y #t)))

(define-public crate-dps310-0.1.1 (c (n "dps310") (v "0.1.1") (d (list (d (n "cortex-m") (r "^0.6.4") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.13") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "stm32f3xx-hal-v2") (r "^0.5.0") (f (quote ("stm32f303xc"))) (d #t) (k 0)))) (h "053cnb6cir207bqywrrdc91wyqmfijvk55h97hdnhax6v693hf5l")))

(define-public crate-dps310-0.1.2 (c (n "dps310") (v "0.1.2") (d (list (d (n "cortex-m") (r "^0.6.4") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6.13") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "stm32f3") (r "^0.12.1") (f (quote ("stm32f303" "rt"))) (d #t) (k 0)))) (h "186f2z9qmqp1vccvn479n58xvfqhf4gdg39qg7q3n7pvkzf0s9y3")))

(define-public crate-dps310-0.1.3 (c (n "dps310") (v "0.1.3") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2") (d #t) (k 2)) (d (n "stm32f3xx-hal") (r "^0.9") (f (quote ("stm32f303xc" "rt"))) (d #t) (k 2)))) (h "061ma7zgmhmnfwmfikrb13az8bkgqwdizq3fcrd7k4cm31wnb0f5")))

(define-public crate-dps310-0.1.4 (c (n "dps310") (v "0.1.4") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2") (d #t) (k 2)) (d (n "stm32f3xx-hal") (r "^0.9") (f (quote ("stm32f303xc" "rt"))) (d #t) (k 2)))) (h "036s3jc200fd6ji0zn1h15hb3ncg9gnjw8kzyv9m6p9fydr0x66n")))

(define-public crate-dps310-0.1.5 (c (n "dps310") (v "0.1.5") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.7") (d #t) (k 2)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2") (d #t) (k 2)) (d (n "stm32f3xx-hal") (r "^0.9") (f (quote ("stm32f303xc" "rt"))) (d #t) (k 2)))) (h "03q3w372brc54ixhdh36z05w0310vqw37iw0aa4p3djrair83ljw")))

