(define-module (crates-io dp dk dpdk-cyclic-redundancy-check) #:use-module (crates-io))

(define-public crate-dpdk-cyclic-redundancy-check-0.1.0 (c (n "dpdk-cyclic-redundancy-check") (v "0.1.0") (d (list (d (n "dpdk-sys") (r "^0.1") (d #t) (t "cfg(any(all(any(target_os = \"android\", target_os = \"linux\"), any(target_arch = \"aarch64\", target_arch = \"arm\", target_arch = \"powerpc64\", target_arch = \"x86\", target_arch = \"x86_64\")), all(target_os = \"freebsd\", target_arch = \"x86_64\")))") (k 0)))) (h "0x9355c2g22rklvxlhb4afwbfklb4ja02m6r09a7i4galq1qbn0z")))

(define-public crate-dpdk-cyclic-redundancy-check-0.1.1 (c (n "dpdk-cyclic-redundancy-check") (v "0.1.1") (d (list (d (n "dpdk-sys") (r "^0.1") (d #t) (t "cfg(any(all(any(target_os = \"android\", target_os = \"linux\"), any(target_arch = \"aarch64\", target_arch = \"arm\", target_arch = \"powerpc64\", target_arch = \"x86\", target_arch = \"x86_64\")), all(target_os = \"freebsd\", target_arch = \"x86_64\")))") (k 0)))) (h "0x0d1971kmf92xiniafgka7qwrs03jsm6544z39fy0242lqh6zgd")))

