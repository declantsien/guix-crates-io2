(define-module (crates-io dp dk dpdk-sys) #:use-module (crates-io))

(define-public crate-dpdk-sys-0.0.1 (c (n "dpdk-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "rust_c") (r "^0.1.1") (f (quote ("macro"))) (d #t) (k 0)) (d (n "rust_c") (r "^0.1.1") (f (quote ("build"))) (d #t) (k 1)))) (h "0560d8v6zx5nbwd2dg8g0xglxbkpcff6ygfqwgdilvscq62b89jj")))

(define-public crate-dpdk-sys-0.0.3 (c (n "dpdk-sys") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "libnuma-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "rdma-core-sys") (r "^0.1.10") (d #t) (k 0)))) (h "1xgmkfjcm9dn4pp5hnfal1p2fc6qp2rw5mprsp780jmihr2z0sqz") (l "dpdk")))

(define-public crate-dpdk-sys-0.0.4 (c (n "dpdk-sys") (v "0.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "libnuma-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "rdma-core-sys") (r "^0.1.10") (d #t) (k 0)))) (h "1fwqw2rwdy4i5f69ahma5lw540zmv1vr6ca883w7zwqwh85xri1n") (l "dpdk")))

(define-public crate-dpdk-sys-0.0.5 (c (n "dpdk-sys") (v "0.0.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "libnuma-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "rdma-core-sys") (r "^0.1.10") (d #t) (k 0)))) (h "13fax1jq9703gr1a3gk8y1w9qqd2wls8ndf5wmizn4mww9fp8m5m") (f (quote (("drop-packets-with-ipv4-options") ("default" "drop-packets-with-ipv4-options")))) (l "dpdk")))

(define-public crate-dpdk-sys-0.0.6 (c (n "dpdk-sys") (v "0.0.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "libnuma-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "rdma-core-sys") (r "^0.1.10") (d #t) (k 0)))) (h "0m3h8grgn8jx8p7i3y92slqw5y9gpvsc79xixjrlr5ak6id9w0mw") (l "dpdk")))

(define-public crate-dpdk-sys-0.1.0 (c (n "dpdk-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "libnuma-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "rdma-core-sys") (r "^0.1.10") (d #t) (k 0)))) (h "0lcq5db049a3finx7rim2z3x0ha9gv2q79lcj2sz0d4fl6g5s48x") (l "dpdk")))

(define-public crate-dpdk-sys-0.1.2 (c (n "dpdk-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "libnuma-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "rdma-core-sys") (r "^0.1.10") (d #t) (k 0)))) (h "0xdqn05cir0w5vwsvyb1im2309zicdcklccnvs4pcadbkik1ijsc") (l "dpdk")))

(define-public crate-dpdk-sys-0.1.3 (c (n "dpdk-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "libnuma-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "rdma-core-sys") (r "^0.1.10") (d #t) (k 0)))) (h "0qwjfkfryl0kyh6w04g1nl6q4vzzavpslqpqx54cbij2nqa4b9rf") (l "dpdk")))

(define-public crate-dpdk-sys-0.1.4 (c (n "dpdk-sys") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "libnuma-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "rdma-core-sys") (r "^0.1.10") (d #t) (k 0)))) (h "0vgp76h9f7ln2llsymp7m2cqi8f343r3bp2vqa1yzw1j4b6xz0vd") (l "dpdk")))

(define-public crate-dpdk-sys-0.1.5 (c (n "dpdk-sys") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "libnuma-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "rdma-core-sys") (r "^0.1.10") (d #t) (k 0)))) (h "10isdadd4ngl8pg0qb22ak99lbffkak93nlbfiychaqp0m2i3xng") (l "dpdk")))

