(define-module (crates-io dp dk dpdk-ioctl) #:use-module (crates-io))

(define-public crate-dpdk-ioctl-0.0.1 (c (n "dpdk-ioctl") (v "0.0.1") (d (list (d (n "errno") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc-extra") (r "^0.0.14") (d #t) (k 0)) (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "syscall-alt") (r "^0.0.14") (d #t) (k 0)))) (h "1ii4c32gl65xc9yxnd8srm60smdkjmiskzhq38i8fsnmk2zrwapk")))

