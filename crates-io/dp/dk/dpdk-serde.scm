(define-module (crates-io dp dk dpdk-serde) #:use-module (crates-io))

(define-public crate-dpdk-serde-0.0.1 (c (n "dpdk-serde") (v "0.0.1") (d (list (d (n "ordermap") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0lrvhlqkbj7y1s1namzxmb4j6c6pm0slv7v265q2f3swl954aigr")))

