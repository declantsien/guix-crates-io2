(define-module (crates-io dp kg dpkg-query-json) #:use-module (crates-io))

(define-public crate-dpkg-query-json-0.1.0 (c (n "dpkg-query-json") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "06vdf5rp1wnciir5vhj44fgk8jcinnfm2afg8zlk9fn3g0wiznd9")))

(define-public crate-dpkg-query-json-0.1.1 (c (n "dpkg-query-json") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0lb3314pwqshydg9wz8j2wx5fcvxkb5jr0mxjns4l68n79lh0s59")))

(define-public crate-dpkg-query-json-0.1.11 (c (n "dpkg-query-json") (v "0.1.11") (d (list (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0m33wfn2jnrfg0n53pn93ypr0ydql5qrgdjxwzp2dyir58kj6sfp")))

(define-public crate-dpkg-query-json-0.1.12 (c (n "dpkg-query-json") (v "0.1.12") (d (list (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1rr97sp5c81wki11gdrwxfqiyyasy60rrsgbv2x8xzl08aldlbhc")))

