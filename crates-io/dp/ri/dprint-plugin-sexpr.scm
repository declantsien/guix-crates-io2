(define-module (crates-io dp ri dprint-plugin-sexpr) #:use-module (crates-io))

(define-public crate-dprint-plugin-sexpr-0.1.0 (c (n "dprint-plugin-sexpr") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "dprint-core") (r "^0.61.0") (f (quote ("wasm"))) (d #t) (k 0)) (d (n "rsexpr") (r "^0.2.2") (f (quote ("comments"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0gi5psdfsak8hcrp96b0sk6m13lg4m1gb4wsw305c8ljy0sz33ph")))

(define-public crate-dprint-plugin-sexpr-0.1.1 (c (n "dprint-plugin-sexpr") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "dprint-core") (r "^0.62.1") (f (quote ("wasm"))) (d #t) (k 0)) (d (n "rsexpr") (r "^0.2.4") (f (quote ("comments"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.177") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)))) (h "09yp9ghhmy49pqjx2jnmv6sh4mn8zh7call38znx19b9h2zyjvb4")))

