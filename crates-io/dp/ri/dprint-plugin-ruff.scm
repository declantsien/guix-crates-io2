(define-module (crates-io dp ri dprint-plugin-ruff) #:use-module (crates-io))

(define-public crate-dprint-plugin-ruff-0.0.1 (c (n "dprint-plugin-ruff") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "dprint-core") (r "^0.63.3") (f (quote ("formatting"))) (d #t) (k 0)) (d (n "dprint-development") (r "^0.9.5") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.108") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0xvn20c0ydqcmdaax25y0m48igan46igc6abz3vc4qvzf8mmw8rz") (f (quote (("wasm" "serde_json" "dprint-core/wasm"))))))

