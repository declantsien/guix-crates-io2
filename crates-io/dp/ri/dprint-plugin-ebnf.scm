(define-module (crates-io dp ri dprint-plugin-ebnf) #:use-module (crates-io))

(define-public crate-dprint-plugin-ebnf-0.1.1 (c (n "dprint-plugin-ebnf") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "dprint-core") (r "^0.59.0") (f (quote ("wasm"))) (d #t) (k 0)) (d (n "ebnf-fmt") (r "^0.1.0") (f (quote ("serde" "fromstr"))) (d #t) (k 0)) (d (n "heck-but-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1r762pia3drrymcngaaa88h2ipljjfnclqjjz63mjvxanmyyig3k")))

