(define-module (crates-io dp ri dprint-development) #:use-module (crates-io))

(define-public crate-dprint-development-0.1.0 (c (n "dprint-development") (v "0.1.0") (h "179rqaw0m1facyd6w639wq7shavqwmnwas9jwsmvlcrypc26a2ca")))

(define-public crate-dprint-development-0.2.0 (c (n "dprint-development") (v "0.2.0") (d (list (d (n "dprint-core") (r "^0.21.0") (d #t) (k 0)))) (h "0wg6p3fph2vmgy0jzqzn6866hxbpq8hzlbc9l2inmhp1khbscxd6")))

(define-public crate-dprint-development-0.2.1 (c (n "dprint-development") (v "0.2.1") (d (list (d (n "dprint-core") (r "^0.22.0") (d #t) (k 0)))) (h "1j11h4l28jfs59ffz6bnjsfyhz1nsxinjbpfbmy4wwvkp74k49k9")))

(define-public crate-dprint-development-0.2.2 (c (n "dprint-development") (v "0.2.2") (h "03kih6c9rahzqyi1bp4p7ksrsgnijcfhclflky9j3pbd6hbx8zj1")))

(define-public crate-dprint-development-0.2.3 (c (n "dprint-development") (v "0.2.3") (d (list (d (n "console") (r "^0.14.0") (d #t) (k 0)) (d (n "similar") (r "^0.5.0") (d #t) (k 0)))) (h "0l70p3ziaj2f7752xshjpqzv31v86isdcy4iaf8wif5y27nxyshr")))

(define-public crate-dprint-development-0.2.4 (c (n "dprint-development") (v "0.2.4") (d (list (d (n "console") (r "^0.14.0") (d #t) (k 0)) (d (n "similar") (r "^0.5.0") (d #t) (k 0)))) (h "1zdq1chip422q1b3y4jmvrdja9aag702cm8w0ysxw07bakbw8sh5")))

(define-public crate-dprint-development-0.2.5 (c (n "dprint-development") (v "0.2.5") (d (list (d (n "console") (r "^0.14.0") (d #t) (k 0)) (d (n "similar") (r "^0.5.0") (d #t) (k 0)))) (h "1p65wdlnr1bsm85aqxw7j7jm7kjcx9i9gw753nszg648hc1bnqq6")))

(define-public crate-dprint-development-0.2.6 (c (n "dprint-development") (v "0.2.6") (d (list (d (n "console") (r "^0.14.0") (d #t) (k 0)) (d (n "similar") (r "^0.5.0") (d #t) (k 0)))) (h "0dspc9h32pgrcjzyx5sdzqpqbywm2f14l3w2gbj51d41jxwl2xip")))

(define-public crate-dprint-development-0.3.0 (c (n "dprint-development") (v "0.3.0") (d (list (d (n "console") (r "^0.14.0") (d #t) (k 0)) (d (n "similar") (r "^0.5.0") (d #t) (k 0)))) (h "0disyd7yslc3ypdb4p30lip89bzx4z8i3116p0g4rw6zypfdqhc2")))

(define-public crate-dprint-development-0.4.0 (c (n "dprint-development") (v "0.4.0") (d (list (d (n "console") (r "^0.14.0") (d #t) (k 0)) (d (n "similar") (r "^0.5.0") (d #t) (k 0)))) (h "03k3g51772m1xhdy9qs9qzbyhicpbk3ajqp9p9fpfa37ryhps98i")))

(define-public crate-dprint-development-0.4.1 (c (n "dprint-development") (v "0.4.1") (d (list (d (n "console") (r "^0.14.0") (d #t) (k 0)) (d (n "similar") (r "^0.5.0") (d #t) (k 0)))) (h "1z7msf9z89349zfxpsmh3kjd2z93wdjxrlvyjiw4l02lii807afk")))

(define-public crate-dprint-development-0.5.0 (c (n "dprint-development") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "similar") (r "^1.3.0") (d #t) (k 0)))) (h "1ym7w09w1s82likgv3kbibccrnpcws3gayqvjnr5vm6gy1c59s35")))

(define-public crate-dprint-development-0.6.0 (c (n "dprint-development") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "similar") (r "^1.3.0") (d #t) (k 0)))) (h "1x51jaz1v91ma1pyc3zhr9fbdldbc5zagkgfrfppah06vxgwq8hs")))

(define-public crate-dprint-development-0.7.0 (c (n "dprint-development") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "similar") (r "^1.3.0") (d #t) (k 0)))) (h "1szif2788bqb6k45i9dhha0qhk3rwxyfnhiqcd278cimp6pw8bl2")))

(define-public crate-dprint-development-0.8.0 (c (n "dprint-development") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "similar") (r "^1.3.0") (d #t) (k 0)))) (h "0vl9qn5hk1d7mxld2ykajzp6v21m56yb4qs6kw04gzb1x071ak9g")))

(define-public crate-dprint-development-0.9.0 (c (n "dprint-development") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "similar") (r "^1.3.0") (d #t) (k 0)))) (h "0cxixhx1z4pkn88mv6ng75yylmmhlj13mgqqwzd015j1ppkpsqjx")))

(define-public crate-dprint-development-0.9.1 (c (n "dprint-development") (v "0.9.1") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "similar") (r "^1.3.0") (d #t) (k 0)))) (h "1qfn1f8jqi2xzlpp0bff9iwlikxf1fg8rzn3gsd5m2n95r2cshlp")))

(define-public crate-dprint-development-0.9.2 (c (n "dprint-development") (v "0.9.2") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "similar") (r "^1.3.0") (d #t) (k 0)))) (h "15z26bvgcg5y7clsrjd0g8rzp9wh54y8cmnc9ivj8m6x2q5wsv8m")))

(define-public crate-dprint-development-0.9.3 (c (n "dprint-development") (v "0.9.3") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (d #t) (k 0)) (d (n "similar") (r "^2.2.1") (d #t) (k 0)))) (h "09mxi2r02p3l9sadd5sij7pji448kn6vycclymsa8f7j5dy4djls")))

(define-public crate-dprint-development-0.9.4 (c (n "dprint-development") (v "0.9.4") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "similar") (r "^2.2.1") (d #t) (k 0)))) (h "16lll1ijzwb4sxrbc2fzza7ycpg0ahz8k2i4dcb6a95ak8v47c8s")))

(define-public crate-dprint-development-0.9.5 (c (n "dprint-development") (v "0.9.5") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "similar") (r "^2.2.1") (d #t) (k 0)))) (h "0wwcwh65lnaw6sxzpmw5qa6kg7s5xdbk72hsq57k7799lk5h49ay")))

(define-public crate-dprint-development-0.10.0 (c (n "dprint-development") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "file_test_runner") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "similar") (r "^2.2.1") (d #t) (k 0)))) (h "0sn6k6q9sx4052p52fsalxzhr621lk41d8zvpqi9yapmvvd66v8p")))

(define-public crate-dprint-development-0.10.1 (c (n "dprint-development") (v "0.10.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "file_test_runner") (r "^0.5.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "similar") (r "^2.2.1") (d #t) (k 0)))) (h "0s220gzs3khs6yy7bj6gaf92xk1krdnbhwfcikb3pg4mc5f87g0j")))

