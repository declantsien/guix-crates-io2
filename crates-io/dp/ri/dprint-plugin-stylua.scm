(define-module (crates-io dp ri dprint-plugin-stylua) #:use-module (crates-io))

(define-public crate-dprint-plugin-stylua-0.2.1 (c (n "dprint-plugin-stylua") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "dprint-core") (r "^0.59.0") (f (quote ("wasm"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "stylua") (r "^0.15.2") (f (quote ("serialize" "fromstr"))) (k 0)))) (h "1cqlzfbpy9fvh7jxzbk9mdfkwndi0bnhal85a1zma9dbxmfqlmi8") (f (quote (("luau" "stylua/luau") ("lua54" "stylua/lua54") ("lua53" "stylua/lua53") ("lua52" "stylua/lua52") ("default" "lua52" "lua53" "lua54" "luau"))))))

