(define-module (crates-io gg ml ggml) #:use-module (crates-io))

(define-public crate-ggml-0.1.0-rc1 (c (n "ggml") (v "0.1.0-rc1") (d (list (d (n "ggml-sys") (r "^0.1.0-rc1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zgw5dvyglzp37fkk73infl3n3wnkd4pzq32vr1bc60fy88pw2wa")))

(define-public crate-ggml-0.1.0-rc2 (c (n "ggml") (v "0.1.0-rc2") (d (list (d (n "ggml-sys") (r "^0.1.0-rc2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0mkhmlhsi7ngrdpgqfybgh2fparsqlj3qdpdnca0bm2axdal06jc")))

(define-public crate-ggml-0.1.0-rc3 (c (n "ggml") (v "0.1.0-rc3") (d (list (d (n "ggml-sys") (r "^0.1.0-rc3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1h37bwadsa84pbgxxwhynjb97rfxnbdvswxfcksfiglwf2vlgz38")))

(define-public crate-ggml-0.1.0-rc4 (c (n "ggml") (v "0.1.0-rc4") (d (list (d (n "ggml-sys") (r "^0.1.0-rc4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0201kpjipjq6l8rkhmjvga3mc1ycdnmdzi2n0sllba6xh03g3p32")))

(define-public crate-ggml-0.1.1 (c (n "ggml") (v "0.1.1") (d (list (d (n "ggml-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1l71vl3h50q5v19mrf14b6wj8rbx3sm6wl4jr086prafw5capiv7")))

