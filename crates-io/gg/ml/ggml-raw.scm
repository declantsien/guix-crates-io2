(define-module (crates-io gg ml ggml-raw) #:use-module (crates-io))

(define-public crate-ggml-raw-0.1.0 (c (n "ggml-raw") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.62.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1v7mi0mjrjv85cfcdk8k5cibvnhj3rgca55qgv83w7sd61gc62m9")))

