(define-module (crates-io gg ml ggml-sys) #:use-module (crates-io))

(define-public crate-ggml-sys-0.1.0-rc1 (c (n "ggml-sys") (v "0.1.0-rc1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1m1icafzxz9sh7hij81z1i5wr54i7xymp7wavchbkxmfclf0gwsp")))

(define-public crate-ggml-sys-0.1.0-rc2 (c (n "ggml-sys") (v "0.1.0-rc2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "132jr034yay3s15iapa8knr0pdnz01rm5ax9nr95wnx2bpxpygrj")))

(define-public crate-ggml-sys-0.1.0-rc3 (c (n "ggml-sys") (v "0.1.0-rc3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "01h691n0p82p07vqb50n4akfzk8bix4z8skb32ywfqkpz4mz04ig")))

(define-public crate-ggml-sys-0.1.0-rc4 (c (n "ggml-sys") (v "0.1.0-rc4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1bm2844ybzdpxsp4d1bn0fzbckd9gqr74dwhfgg62fxk9lffz9dc")))

(define-public crate-ggml-sys-0.1.1 (c (n "ggml-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "10sxgsar27gll9gb7mdclffiiq2v569ikwrq977qd5357ajvdwbq")))

