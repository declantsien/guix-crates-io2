(define-module (crates-io gg ap ggapi) #:use-module (crates-io))

(define-public crate-ggapi-0.1.0 (c (n "ggapi") (v "0.1.0") (d (list (d (n "gql_client") (r "^1.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (d #t) (k 0)))) (h "0137dkybmfch1272sfpwyx6zkcrpjhbds6hi48kwkd64c2j7y2nk")))

(define-public crate-ggapi-0.2.0 (c (n "ggapi") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "gql_client") (r "^1.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (d #t) (k 0)))) (h "0rpx1qch7q39dzrnj73yxlwk5qahwaawpjhzf8zj870p2qk7hgi5")))

(define-public crate-ggapi-0.2.1 (c (n "ggapi") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "gql_client") (r "^1.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (d #t) (k 0)))) (h "1csna7b3fcrnsdjy243l1nz9mfy3sbj3ikvlvj7jxmr41243n52n")))

(define-public crate-ggapi-0.2.2 (c (n "ggapi") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "gql_client") (r "^1.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (d #t) (k 0)))) (h "0qhx4ghim4n8wy1dfckqh2rzpy9rrp9ljfwp34kc6bbyxgx1z4ww")))

(define-public crate-ggapi-0.2.3 (c (n "ggapi") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "gql_client") (r "^1.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (d #t) (k 0)))) (h "0s9r7pkxfdnkjwvfa0ijd4l94502kc5fx4mh3iwyd3n2f2mvxl7b")))

(define-public crate-ggapi-0.2.4 (c (n "ggapi") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "gql_client") (r "^1.0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (d #t) (k 0)))) (h "1jnq3871wkla69ipw87ppcv5681012sxjisb9nd6mq7111g1vyzq")))

