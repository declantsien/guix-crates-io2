(define-module (crates-io gg ez ggez-goodies) #:use-module (crates-io))

(define-public crate-ggez-goodies-0.1.0 (c (n "ggez-goodies") (v "0.1.0") (d (list (d (n "ggez") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1rxpm7i3g03yr1yf9riqg5fxpz1999k4cda8x0r6fbxb23p0wmlb")))

(define-public crate-ggez-goodies-0.5.0 (c (n "ggez-goodies") (v "0.5.0") (d (list (d (n "euclid") (r "^0.19") (f (quote ("mint"))) (d #t) (k 0)) (d (n "ezing") (r "^0.2") (d #t) (k 2)) (d (n "ggez") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra-glm") (r "^0.4.0") (d #t) (k 0)) (d (n "tiled") (r "^0.8") (d #t) (k 0)))) (h "04vrplp8ddqf9wfhlg17761zpgncc93fkid2zmlic5i6yq3abk2i")))

