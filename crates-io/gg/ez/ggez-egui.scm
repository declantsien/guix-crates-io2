(define-module (crates-io gg ez ggez-egui) #:use-module (crates-io))

(define-public crate-ggez-egui-0.1.0 (c (n "ggez-egui") (v "0.1.0") (d (list (d (n "egui") (r "^0.13.1") (d #t) (k 0)) (d (n "ggez") (r "^0.6.0") (d #t) (k 0)))) (h "0hddmi1y00xc705bjzlaci0q7gh79i704rx6sq6q2s3jm5pdmhd5")))

(define-public crate-ggez-egui-0.1.1 (c (n "ggez-egui") (v "0.1.1") (d (list (d (n "egui") (r "^0.15.0") (d #t) (k 0)) (d (n "ggez") (r "^0.6.1") (d #t) (k 0)))) (h "1fwbr04mrha41shqy6z60vghf21dy6s2lfblx73ni65j7pall6il")))

(define-public crate-ggez-egui-0.1.2 (c (n "ggez-egui") (v "0.1.2") (d (list (d (n "egui") (r "^0.15.0") (d #t) (k 0)) (d (n "ggez") (r "^0.7.0") (d #t) (k 0)))) (h "008l16ylma5plk7mwbw04wlx3bxyvbd4whx7hj3qwbm8dcpi4q3j")))

(define-public crate-ggez-egui-0.1.3 (c (n "ggez-egui") (v "0.1.3") (d (list (d (n "egui") (r "^0.15.0") (d #t) (k 0)) (d (n "ggez") (r "^0.7.0") (d #t) (k 0)))) (h "1dmdfzkzvl01bxby4dib2gvvqcfqalf4nlwhp8njwa5zn79r870h")))

(define-public crate-ggez-egui-0.1.5 (c (n "ggez-egui") (v "0.1.5") (d (list (d (n "egui") (r "^0.16") (d #t) (k 0)) (d (n "ggez") (r "^0.7.0") (d #t) (k 0)))) (h "1lddawq2nm5ah78m0bxnkh1j4z9snnc80arndhp9rnchxdjszm2v")))

(define-public crate-ggez-egui-0.1.6 (c (n "ggez-egui") (v "0.1.6") (d (list (d (n "egui") (r "^0.16") (d #t) (k 0)) (d (n "ggez") (r "^0.7.0") (d #t) (k 0)))) (h "05303plf6xa4d8hzs3ijbm6hgpvxh7l015m294v41w7aw2hpywf7")))

(define-public crate-ggez-egui-0.1.7 (c (n "ggez-egui") (v "0.1.7") (d (list (d (n "egui") (r "^0.16") (d #t) (k 0)) (d (n "ggez") (r "^0.7.0") (d #t) (k 0)))) (h "0rnizsgbwzb8spq2ndi3d90id75szhs6aqh6dv43fvxvxnkvqg17")))

(define-public crate-ggez-egui-0.2.0 (c (n "ggez-egui") (v "0.2.0") (d (list (d (n "egui") (r "^0.18.1") (d #t) (k 0)) (d (n "ggez") (r "^0.7.0") (d #t) (k 0)))) (h "0a4x0aq9haqaqgb9ifij8lsz73xi7b2672irb9z15cahjrxjry7y")))

(define-public crate-ggez-egui-0.2.1 (c (n "ggez-egui") (v "0.2.1") (d (list (d (n "egui") (r "^0.18.1") (d #t) (k 0)) (d (n "ggez") (r "^0.7.0") (d #t) (k 0)))) (h "1474zf33smyps692yiw19hmxfcjx3slw40m8qf1vrxzpk3q76yaa")))

(define-public crate-ggez-egui-0.3.1 (c (n "ggez-egui") (v "0.3.1") (d (list (d (n "egui") (r "^0.20.1") (d #t) (k 0)) (d (n "ggez") (r "^0.8.1") (d #t) (k 0)))) (h "00rx5qjvqpbr6ypkx0vrw4szzg67k9lqjxs9wz8hgzks6k3ra5pi")))

