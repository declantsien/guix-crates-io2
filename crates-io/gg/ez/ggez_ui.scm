(define-module (crates-io gg ez ggez_ui) #:use-module (crates-io))

(define-public crate-ggez_ui-0.1.0 (c (n "ggez_ui") (v "0.1.0") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 0)))) (h "1zd7khs1mrhxibwfpkzzi1nyx2vhqbgy7cfi1kln8lf55wm03k6i")))

(define-public crate-ggez_ui-0.1.1 (c (n "ggez_ui") (v "0.1.1") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 0)))) (h "18lffrvdcjgkji1q5236g7jcypjsxqd3cxs3fhgjnibq4raz3bxj")))

(define-public crate-ggez_ui-0.1.2 (c (n "ggez_ui") (v "0.1.2") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 0)))) (h "0jnlydj079yvsi19r4bng94lnpxf0kq62idfd7xb2rlq5lqzjhd2")))

(define-public crate-ggez_ui-0.1.3 (c (n "ggez_ui") (v "0.1.3") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 0)))) (h "02w1i78md6vphjqcv78ips7ad28cmwgm0qh6xvncr2ibrrng6bbh")))

(define-public crate-ggez_ui-0.2.0 (c (n "ggez_ui") (v "0.2.0") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 0)))) (h "06rgc5irbdsmgyzvkqj8hs6fs0257mkj5h89bivl1wka9ai5biyr")))

(define-public crate-ggez_ui-0.2.1 (c (n "ggez_ui") (v "0.2.1") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 0)))) (h "1f3rxk10qqhdh8gaf2yyh308l15frrrgxpzgkwsxkbr7gh9wabn8")))

(define-public crate-ggez_ui-0.2.2 (c (n "ggez_ui") (v "0.2.2") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 0)))) (h "0zvl3pqgvzhql7y9am022dgd0vy9akmx8i67zb2yhzhlg4k8yjiz")))

