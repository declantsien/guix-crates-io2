(define-module (crates-io gg ez ggez_tools) #:use-module (crates-io))

(define-public crate-ggez_tools-0.1.0 (c (n "ggez_tools") (v "0.1.0") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 0)))) (h "14frcdizh5m69hj0danncvsgls0vids414f6r2nasrfmi080mmp6")))

(define-public crate-ggez_tools-0.1.1 (c (n "ggez_tools") (v "0.1.1") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 0)))) (h "1dbxvgipvknjdxzx376s4xs871n2gzyyzbfkdmr6zzvak283721a")))

(define-public crate-ggez_tools-0.2.0 (c (n "ggez_tools") (v "0.2.0") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 0)))) (h "0ayrwirlgqmmwspplq9smxqqfd9kpfswbsdbsv47wc1l5aqbcay1")))

(define-public crate-ggez_tools-0.2.1 (c (n "ggez_tools") (v "0.2.1") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 0)))) (h "1sp6f79qc3cacmk2f21rzzjcyr5s22hqq3ww044xh43ka1dwi02l")))

(define-public crate-ggez_tools-0.2.2 (c (n "ggez_tools") (v "0.2.2") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 0)))) (h "0wcbdbxim70l55d94bsx7jqvhsqqb71ys00zly3z8c9pf2rp8yji")))

(define-public crate-ggez_tools-0.3.0 (c (n "ggez_tools") (v "0.3.0") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 0)))) (h "1cd1l1bxcnlgf5d9jfjxhvjzv90p6v4jxq5r2pz55x89m19liwca")))

(define-public crate-ggez_tools-0.4.0 (c (n "ggez_tools") (v "0.4.0") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 0)))) (h "0427jqa6h59qi61hdwllm55g4l5d16s4k20zwa43vgmsxypn309j")))

(define-public crate-ggez_tools-0.4.1 (c (n "ggez_tools") (v "0.4.1") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 0)))) (h "1j9n5v6r70kd5rvm2wazazi1lg38f63inch7m7kg6a8ckabxr0wd")))

