(define-module (crates-io gg re ggrep) #:use-module (crates-io))

(define-public crate-ggrep-0.1.0 (c (n "ggrep") (v "0.1.0") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)))) (h "1k7vlag6hy71d0jll6cnjqjpli5maw9ax9vsiq64ni496b5r9ski")))

(define-public crate-ggrep-0.1.1 (c (n "ggrep") (v "0.1.1") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)))) (h "1zy406gmx1rbfh97s1xv5c6823wklwibdll3fyk4vnvrnnk1hk2n")))

(define-public crate-ggrep-0.1.2 (c (n "ggrep") (v "0.1.2") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)))) (h "0fgakr1aqrj3p7vnr049vjyz1kk0z7k7pdngzh32ipjllfpfpm6m")))

(define-public crate-ggrep-0.1.3 (c (n "ggrep") (v "0.1.3") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)))) (h "152y42vs9ai0jmwrgfvf724m8gww194gfybpb5499vssz35msh5f")))

(define-public crate-ggrep-0.1.4 (c (n "ggrep") (v "0.1.4") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)))) (h "0ijw46hdv7xjy6vzbvfmz3zh113dg9v5cqp57fpv6ailsjydwilh")))

(define-public crate-ggrep-0.1.5 (c (n "ggrep") (v "0.1.5") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)))) (h "1rzgcm97f4ln0xxp4v1la5pnxiim9n1n557abd3dqmbnkl4nikfm")))

(define-public crate-ggrep-0.1.6 (c (n "ggrep") (v "0.1.6") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)))) (h "191izw40mk9lz8wc41wkzzdjwz6knd2cskyr9s2gcc503vi17xm9")))

(define-public crate-ggrep-0.1.8 (c (n "ggrep") (v "0.1.8") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "unicode-segmentation") (r "1.*") (d #t) (k 0)))) (h "02p1523jxw3dpcx32whav3qgzrzqb9bgy4lpalzk63j9g9kz11p1")))

(define-public crate-ggrep-0.1.9 (c (n "ggrep") (v "0.1.9") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "unicode-segmentation") (r "1.*") (d #t) (k 0)))) (h "1axsrfgwkpyrjcvmb949mbnw8smrdjdfmxfiicg1yh946x57a1b2")))

(define-public crate-ggrep-0.1.10 (c (n "ggrep") (v "0.1.10") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "encoding_rs") (r "0.8.*") (d #t) (k 0)))) (h "0yky63ppn1jg5p9ldp991y0iy45vjyvk1alk0di9bqxynljbs8vc")))

(define-public crate-ggrep-1.0.0 (c (n "ggrep") (v "1.0.0") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "encoding_rs") (r "0.8.*") (d #t) (k 0)))) (h "0qqagn0js92p6smwqgipkj3s62py2ln48zci96f9g98bvqxm80z1")))

(define-public crate-ggrep-0.1.11 (c (n "ggrep") (v "0.1.11") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "encoding_rs") (r "0.8.*") (d #t) (k 0)))) (h "1f2p6pvwf7v52qzxhi898lq9v1l5b2sl9xnmbfgcg3ssdx7dwkyx")))

(define-public crate-ggrep-0.1.12 (c (n "ggrep") (v "0.1.12") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "encoding_rs") (r "0.8.*") (d #t) (k 0)))) (h "0z4fshm6kgzfj42f94799fxwikv84wvx0p8hb2xlmqv6qf8msl7z")))

(define-public crate-ggrep-0.1.13 (c (n "ggrep") (v "0.1.13") (d (list (d (n "async-std") (r "1.0.*") (d #t) (k 0)) (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "encoding_rs") (r "0.8.*") (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("executor"))) (d #t) (k 0)))) (h "0mk8fzbsfw5lmimpcrlyi8nh5qks724b9z7ndhwin5rrgfl1chg1")))

