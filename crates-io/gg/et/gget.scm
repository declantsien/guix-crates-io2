(define-module (crates-io gg et gget) #:use-module (crates-io))

(define-public crate-gget-0.1.0 (c (n "gget") (v "0.1.0") (d (list (d (n "anyhow") (r ">=1.0.32, <2.0.0") (d #t) (k 0)) (d (n "rustls") (r ">=0.18.0, <0.19.0") (f (quote ("dangerous_configuration"))) (d #t) (k 0)) (d (n "structopt") (r ">=0.3.13, <0.4.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0.20, <2.0.0") (d #t) (k 0)) (d (n "url") (r ">=2.1.0, <3.0.0") (d #t) (k 0)) (d (n "webpki") (r ">=0.21.3, <0.22.0") (d #t) (k 0)) (d (n "webpki-roots") (r ">=0.20.0, <0.21.0") (d #t) (k 0)))) (h "09xji82gsx3dyb0ahwcq42y3phwwmf5nbj5d6w5gzq1d5m1qvxsj")))

