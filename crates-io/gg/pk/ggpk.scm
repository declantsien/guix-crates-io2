(define-module (crates-io gg pk ggpk) #:use-module (crates-io))

(define-public crate-ggpk-1.1.0 (c (n "ggpk") (v "1.1.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "simplelog") (r "^0.8.0") (d #t) (k 0)) (d (n "widestring") (r "^0.4.3") (d #t) (k 0)))) (h "0l64m9a2xrl4k90z3z1qc9isgb5pi2biqlh1nv9zdphk9cjvlm8x")))

(define-public crate-ggpk-1.2.1 (c (n "ggpk") (v "1.2.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 2)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 2)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "11829xh6w0b0jhk1g0q3z1zjb4f55jc32pws3qjff1wq25qdy6m7")))

(define-public crate-ggpk-1.2.2 (c (n "ggpk") (v "1.2.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 2)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 2)) (d (n "widestring") (r "^1.0.2") (d #t) (k 0)))) (h "07xfcmp50birifxrwg6diyad6hhwwbi9sf4sj4f6hhhka26ygfyh")))

