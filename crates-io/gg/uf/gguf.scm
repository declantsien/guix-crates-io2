(define-module (crates-io gg uf gguf) #:use-module (crates-io))

(define-public crate-gguf-0.0.1 (c (n "gguf") (v "0.0.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1qqv8n1dhm3mkz26jjwyyvlm573kxv0rxi4q2ks5x6dhc4lfc3k6")))

(define-public crate-gguf-0.1.0 (c (n "gguf") (v "0.1.0") (d (list (d (n "nom") (r "^7") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (o #t) (d #t) (k 0)))) (h "14yxshajyl9zyvrdmq4ap1j5gzpsspj0m7jkvg02v94byxvwwchc") (f (quote (("bin" "serde_yaml"))))))

(define-public crate-gguf-0.1.1 (c (n "gguf") (v "0.1.1") (d (list (d (n "nom") (r "^7") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0s0fscp2nyfhvv2c21b8gq1slx58hk1dgydc8ap9dfp5cn8qyc71") (f (quote (("bin" "serde_yaml"))))))

(define-public crate-gguf-0.1.2 (c (n "gguf") (v "0.1.2") (d (list (d (n "bytes") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "comfy-table") (r "^7") (o #t) (d #t) (k 0)) (d (n "nom") (r "^7") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0n4y7a45n03k54jav88hfxxfjwf3j85jfq18gx1jswv6w1w04cn3") (f (quote (("bin" "serde_yaml" "serde_json" "comfy-table" "bytes" "clap"))))))

