(define-module (crates-io gg up gguppy) #:use-module (crates-io))

(define-public crate-gguppy-0.1.0 (c (n "gguppy") (v "0.1.0") (d (list (d (n "arwggplot") (r "^0.1.0") (d #t) (k 0)) (d (n "gguppy_core") (r "^0.1.0") (d #t) (k 0)) (d (n "gguppy_data") (r "^0.1.0") (d #t) (k 0)))) (h "1sg16srj58j8q43mryc34cv0bapjiwqg23jrhg9rw9v0kyb2ziwi") (f (quote (("save_file" "arwggplot/save_file"))))))

