(define-module (crates-io gg st ggstd) #:use-module (crates-io))

(define-public crate-ggstd-0.0.1 (c (n "ggstd") (v "0.0.1") (h "1i71hbrqz5s9xczv8zqpk88krwd3dq4vgz26gn0fk02xgbgfw4jb")))

(define-public crate-ggstd-0.0.2 (c (n "ggstd") (v "0.0.2") (h "0gwhmqpa5wg2iwv33mv8g987rbpd0wb7p81gv9284qhz64ycy35d")))

(define-public crate-ggstd-0.0.3 (c (n "ggstd") (v "0.0.3") (h "0mq7axr22pj0im9nsisl1rxsnabk2i5il6cf3hpf9v8lc2nr5xjv")))

(define-public crate-ggstd-0.0.4 (c (n "ggstd") (v "0.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("everything"))) (d #t) (t "cfg(windows)") (k 0)))) (h "06wkzr2mxhds7vczkvny5xzssfipxl323sal53axzgnj7dv2fim6")))

(define-public crate-ggstd-0.0.5 (c (n "ggstd") (v "0.0.5") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("everything"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1yc9zvrcn52amnh8gr7w1qr2b2cm7hvjc0ggfj8dg3k43n5m16qp")))

(define-public crate-ggstd-0.0.6 (c (n "ggstd") (v "0.0.6") (h "1g5drypvjnz9gq3lnwrzspi3ksjdlxgh2nj729902miyn8wr72bw")))

(define-public crate-ggstd-0.0.7 (c (n "ggstd") (v "0.0.7") (h "0bjs3kcrm17r8mqc650kk3vdwzsm9dmngf2rabg1ky98347yk88s")))

(define-public crate-ggstd-0.0.8 (c (n "ggstd") (v "0.0.8") (h "0b1q676iipzlgpqxv83a1bbvkkzwhf7n8j5dgcz50swkpyds0a3x")))

(define-public crate-ggstd-0.0.9 (c (n "ggstd") (v "0.0.9") (h "1d0zc4jspy84aa8izcrrr4ziilcwq6yh93gvfqhj04qkzpamb8bm")))

(define-public crate-ggstd-0.1.0 (c (n "ggstd") (v "0.1.0") (h "11v4zlbfds31rr82x9q9l5f7qii9hjz8lrdfgnnkbawdsdwl94lf")))

