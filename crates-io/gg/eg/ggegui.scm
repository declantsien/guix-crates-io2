(define-module (crates-io gg eg ggegui) #:use-module (crates-io))

(define-public crate-ggegui-0.3.2 (c (n "ggegui") (v "0.3.2") (d (list (d (n "egui") (r "^0.20.1") (d #t) (k 0)) (d (n "ggez") (r "^0.8.1") (d #t) (k 0)))) (h "0w0n52in5s6ys2gzsd0psg64hrqcqqipwqd7xklla03wsllvympf")))

(define-public crate-ggegui-0.3.3 (c (n "ggegui") (v "0.3.3") (d (list (d (n "egui") (r "^0.20.1") (d #t) (k 0)) (d (n "ggez") (r "^0.8.1") (d #t) (k 0)))) (h "0qim8gg8czlwj9c17s233j499whvv208hk8kkplyycvbcvxhlybj")))

(define-public crate-ggegui-0.3.4 (c (n "ggegui") (v "0.3.4") (d (list (d (n "egui") (r "^0.20.1") (d #t) (k 0)) (d (n "ggez") (r "^0.8.1") (d #t) (k 0)))) (h "160xalr3cxgi76hqq0dx0l659lz1ql81gqc6aqj25s2yvqzxgdli")))

(define-public crate-ggegui-0.3.5 (c (n "ggegui") (v "0.3.5") (d (list (d (n "egui") (r "^0.20.1") (d #t) (k 0)) (d (n "ggez") (r "^0.8.1") (d #t) (k 0)))) (h "1v3w71lczzwf9xpjn1lrqi5vz2vd6d28l6wbxvx0n9rir14bqs5z")))

(define-public crate-ggegui-0.3.6 (c (n "ggegui") (v "0.3.6") (d (list (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "egui_demo_lib") (r "^0.22") (d #t) (k 2)) (d (n "ggez") (r "^0.9.0") (d #t) (k 0)))) (h "1v6ff3sq744k5gjxai2c7kf9wgcr94c04y1ixx9djvkzki7qwxjk")))

(define-public crate-ggegui-0.3.7 (c (n "ggegui") (v "0.3.7") (d (list (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "egui_demo_lib") (r "^0.22") (d #t) (k 2)) (d (n "ggez") (r "^0.9.1") (d #t) (k 0)))) (h "0hx0shvlg3a71vdfm5p7q5fjv4d0b7xpm5p54p5wyyy188597cyx")))

(define-public crate-ggegui-0.3.8 (c (n "ggegui") (v "0.3.8") (d (list (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "egui_demo_lib") (r "^0.22") (d #t) (k 2)) (d (n "ggez") (r "^0.9.1") (d #t) (k 0)))) (h "0bvj7k1s8j7vdbcdgxchy9sy4rapfgwa3mqgahm2hb8z7b27yakx")))

(define-public crate-ggegui-0.3.9 (c (n "ggegui") (v "0.3.9") (d (list (d (n "egui") (r "^0.26.2") (d #t) (k 0)) (d (n "egui_demo_lib") (r "^0.26") (d #t) (k 2)) (d (n "ggez") (r "^0.9.1") (d #t) (k 0)))) (h "1gzgikvq0y8dz9fxkz77iw15xnknr394a3f9gkhsf01bnk7zmi4v")))

(define-public crate-ggegui-0.4.0 (c (n "ggegui") (v "0.4.0") (d (list (d (n "egui") (r "^0.26.2") (d #t) (k 0)) (d (n "egui_demo_lib") (r "^0.26") (d #t) (k 2)) (d (n "ggez") (r "^0.9.1") (d #t) (k 0)))) (h "0h8fss62q2h5ghkgj0dx5zzmkpfgqa2a53111lyvmnax3ip665nr")))

