(define-module (crates-io gg po ggpo-sys) #:use-module (crates-io))

(define-public crate-ggpo-sys-0.1.0 (c (n "ggpo-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "winapi") (r "^0.3.0") (f (quote ("processenv" "processthreadsapi" "timeapi" "winsock2" "winuser" "ws2tcpip"))) (d #t) (t "cfg(windows)") (k 0)))) (h "184nvyd65dpby2pyzi1x34gbrnh6xr95labj33dmwlqa9cv98s9z") (l "GGPO")))

