(define-module (crates-io gg pl ggplot-error) #:use-module (crates-io))

(define-public crate-ggplot-error-1.0.0 (c (n "ggplot-error") (v "1.0.0") (d (list (d (n "csscolorparser") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)))) (h "0ayzrpnki9zjzc6nfi0rsfhsh8bqw9mjvmj3wrw53mxzskyzw7hy") (f (quote (("default"))))))

