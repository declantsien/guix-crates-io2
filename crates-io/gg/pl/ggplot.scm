(define-module (crates-io gg pl ggplot) #:use-module (crates-io))

(define-public crate-ggplot-0.1.0 (c (n "ggplot") (v "0.1.0") (d (list (d (n "csscolorparser") (r "^0.5.0") (d #t) (k 0)) (d (n "dyn-clone") (r "^1.0.4") (d #t) (k 0)))) (h "18a3pby916zi7i2d5yggzk92194py15kpjc74w28k37agaymg29b") (f (quote (("default"))))))

(define-public crate-ggplot-0.1.1 (c (n "ggplot") (v "0.1.1") (d (list (d (n "ggrust") (r "0.1.*") (d #t) (k 0)))) (h "1hccjwrrk31lykwckjhpfj4d8raryxh0jgkn4sixzhw3s6qar82f") (f (quote (("default"))))))

