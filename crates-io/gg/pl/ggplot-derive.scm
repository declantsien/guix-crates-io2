(define-module (crates-io gg pl ggplot-derive) #:use-module (crates-io))

(define-public crate-ggplot-derive-0.1.0 (c (n "ggplot-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "1lmg0cymnmi3yid3cy7f37v2k3k7v1scq00ipr4n9kr2rrxyzz22") (f (quote (("default" "add") ("add"))))))

(define-public crate-ggplot-derive-0.1.1 (c (n "ggplot-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (d #t) (k 0)))) (h "097qbspav0l8z66qy7alqxxpricmm2h3jknid38gyhipkrk2rrhv") (f (quote (("default" "add") ("add"))))))

