(define-module (crates-io gg ru ggrust) #:use-module (crates-io))

(define-public crate-ggrust-0.1.0 (c (n "ggrust") (v "0.1.0") (d (list (d (n "csscolorparser") (r "^0.5.0") (d #t) (k 0)) (d (n "ggplot-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "ggplot-error") (r "1.0.*") (f (quote ("csscolorparser"))) (d #t) (k 0)) (d (n "plotters") (r "^0.3.1") (d #t) (k 0)) (d (n "plotters-svg") (r "^0.3.1") (d #t) (k 0)))) (h "1v839wzjdj8w6gdya76hhxqpmd6c1s3bbbiawdk6f7qpf1j5i9pv") (f (quote (("svg") ("default"))))))

