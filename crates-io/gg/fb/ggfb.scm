(define-module (crates-io gg fb ggfb) #:use-module (crates-io))

(define-public crate-ggfb-0.1.0 (c (n "ggfb") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0351vdfx7x0xla9r8qdzd9la8pp7vpmzr4ksqidlqmdi0kdq5qmw")))

(define-public crate-ggfb-0.2.0 (c (n "ggfb") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "08gqp2axzfk3iazksyy57qz9nr39xy3hc782cpcpivaapi3fp317")))

