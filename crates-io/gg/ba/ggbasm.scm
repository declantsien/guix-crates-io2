(define-module (crates-io gg ba ggbasm) #:use-module (crates-io))

(define-public crate-ggbasm-0.1.0 (c (n "ggbasm") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "11v1n214yr5alcfldc2ynqdbvglv53qxl1p52yl4frc46gmajxd9")))

(define-public crate-ggbasm-0.1.1 (c (n "ggbasm") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "1rz2d3il7q6rcy1iqg6f533l1x1gyvcc9ap5h29hhzqqnf1cv1wh")))

(define-public crate-ggbasm-0.1.2 (c (n "ggbasm") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.19") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "0rshcw9qnyncb3zf1a4i3mw36kj6gylmh1gb6qaxd1xng10aj248")))

(define-public crate-ggbasm-0.1.3 (c (n "ggbasm") (v "0.1.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.19") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "1qq9hq9j60pj0lx6zjy50x11kgbxs5ik2gkd548wvicrdrpqirp8")))

(define-public crate-ggbasm-0.2.0 (c (n "ggbasm") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.20") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "04m24kz4dr47y0rad9b5xgmhqpc9hz29cfnvi8p312wmn1gdsv0g")))

(define-public crate-ggbasm-0.2.1 (c (n "ggbasm") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.20") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "18p50qnmnrkjlrd00v38g434rkn8davxkjm446bb2yf0qifblc6r")))

(define-public crate-ggbasm-0.2.2 (c (n "ggbasm") (v "0.2.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.20") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "1jhxqb2m2b1bjnsrxhi98qc7jrl5abxgq1j7yvpbcq4gsmvvvhih")))

(define-public crate-ggbasm-0.2.3 (c (n "ggbasm") (v "0.2.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.20") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "19yjyhmnmi39wkzqfpx8p7viwrf81x6d128p1b7phgcw0vlf3rjb")))

(define-public crate-ggbasm-0.2.4 (c (n "ggbasm") (v "0.2.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "nom") (r "^4") (d #t) (k 0)))) (h "071g4kgp5r28ikxm0j92m9iaa3qh09z52bfj21fy4sfvl612bigv")))

(define-public crate-ggbasm-0.2.5 (c (n "ggbasm") (v "0.2.5") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "09fv6daphz1idql0m85mx8nqi3p98cc0rdjxa52n0aq1w6jqylhq")))

(define-public crate-ggbasm-0.3.0 (c (n "ggbasm") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0iyraa119zisy440ndrfrnm114l2chbprbji4h83p0m09c854l4v")))

