(define-module (crates-io c0 sv c0sv) #:use-module (crates-io))

(define-public crate-c0sv-0.1.0 (c (n "c0sv") (v "0.1.0") (d (list (d (n "nom") (r "^6") (k 0)))) (h "07ykka07jqi4vr03h30rcvpwl3sdzdldqvs1dqcijc7mp2aisi9m")))

(define-public crate-c0sv-0.1.1 (c (n "c0sv") (v "0.1.1") (d (list (d (n "nom") (r "^6") (k 0)))) (h "0x6cvs37si0cmy5d14kbnnhpwf3qi3wljn2j414jmf9spjqa0gw2")))

(define-public crate-c0sv-0.2.0 (c (n "c0sv") (v "0.2.0") (d (list (d (n "nom") (r "^6") (k 0)))) (h "1dxzl383299x0g8sqz5a78j3087znkrj26q6mqwmin6cz2b0yfjm") (f (quote (("std") ("default" "std"))))))

