(define-module (crates-io xg _p xg_proc) #:use-module (crates-io))

(define-public crate-xg_proc-0.0.1 (c (n "xg_proc") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pgw") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "trt") (r "^0.1.5") (d #t) (k 0)))) (h "1irns734ii7477qflhgkfmy6h49f55bjdq118r8ji9ramj9dkjrj")))

(define-public crate-xg_proc-0.0.8 (c (n "xg_proc") (v "0.0.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pgw") (r "^0.2.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "trt") (r "^0.1.5") (d #t) (k 0)))) (h "1qhyh3ggdjff42qb6gg2r2m5c009cdbwwg0ncc60v25wxfaqs4x7")))

(define-public crate-xg_proc-0.0.9 (c (n "xg_proc") (v "0.0.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pgw") (r "^0.2.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "trt") (r "^0.1.5") (d #t) (k 0)))) (h "0jn7wsvnxzirlq1442xf8p5n7nap7jrz52w6ivv1y5isqsxssh8l")))

