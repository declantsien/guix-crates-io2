(define-module (crates-io xg bo xgboost) #:use-module (crates-io))

(define-public crate-xgboost-0.1.1 (c (n "xgboost") (v "0.1.1") (d (list (d (n "derive_builder") (r "^0.5") (d #t) (k 0)) (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "xgboost-sys") (r "^0.1") (d #t) (k 0)))) (h "0c0sjc44g5b232w49mvlyhhsjpriq1rgj1da0drs3hvrqjfpxrpm")))

(define-public crate-xgboost-0.1.2 (c (n "xgboost") (v "0.1.2") (d (list (d (n "derive_builder") (r "^0.5") (d #t) (k 0)) (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "xgboost-sys") (r "^0.1") (d #t) (k 0)))) (h "1an11w2qxbyjamm1p3mfcc62vc5mkgnsy66ndvsjrjrp0ik7p9k5")))

(define-public crate-xgboost-0.1.3 (c (n "xgboost") (v "0.1.3") (d (list (d (n "derive_builder") (r "^0.5") (d #t) (k 0)) (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "xgboost-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1y1xggc2wlws7i2yg28v6bjd2hv3wmrbsm9jlhlg9k6dpnm7bskm")))

(define-public crate-xgboost-0.1.4 (c (n "xgboost") (v "0.1.4") (d (list (d (n "derive_builder") (r "^0.5") (d #t) (k 0)) (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "xgboost-sys") (r "^0.1.2") (d #t) (k 0)))) (h "12973z7hk4m7ia99wwvy0gqlia8jy0qwblsnrnqfyi7aa1qkmrrs")))

