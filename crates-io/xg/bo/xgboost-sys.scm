(define-module (crates-io xg bo xgboost-sys) #:use-module (crates-io))

(define-public crate-xgboost-sys-0.1.1 (c (n "xgboost-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.36") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1476a3jx9giwy4ba6nhfwj82rrvywbrh806wfwfvjs93gqdlm32s") (l "xgboost")))

(define-public crate-xgboost-sys-0.1.2 (c (n "xgboost-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.36") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01lby6ajbmx2snm70xalndq2kblhkpcda6hb4mghvxy87rq2v7qb") (l "xgboost")))

