(define-module (crates-io xg bo xgboost-rs-sys) #:use-module (crates-io))

(define-public crate-xgboost-rs-sys-0.1.0 (c (n "xgboost-rs-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.135") (d #t) (k 0)))) (h "0qzkikhfkyck09a2ffsq5d9qwk64z3096zx68w3ii3wyvbb76g68") (l "xgboost")))

(define-public crate-xgboost-rs-sys-0.1.1 (c (n "xgboost-rs-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.135") (d #t) (k 0)))) (h "1p5clqkk0gih9wn2ww0z3z8nf8wrjgcka94f15gs9d35295d2hxm") (l "xgboost")))

(define-public crate-xgboost-rs-sys-0.1.2 (c (n "xgboost-rs-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.135") (d #t) (k 0)))) (h "187a6l0207xn1085nm7qf1n4m04liabmk7slmm1jzii6z8mw72jv") (l "xgboost")))

(define-public crate-xgboost-rs-sys-0.1.3 (c (n "xgboost-rs-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xw6kr6cx6rhdmpr3pm08wghbrcbyszbkxzdnl4p1j5xsyshavvw") (l "xgboost")))

(define-public crate-xgboost-rs-sys-0.1.31 (c (n "xgboost-rs-sys") (v "0.1.31") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "15110a8lbz54my7pldlclw9f63n4ysz8nwj2kxdqjpb8z04x5hxv") (l "xgboost")))

