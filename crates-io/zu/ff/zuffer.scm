(define-module (crates-io zu ff zuffer) #:use-module (crates-io))

(define-public crate-zuffer-0.1.0 (c (n "zuffer") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fmmap") (r "^0.3") (d #t) (k 0)) (d (n "indexsort") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1x9w8zj32cj07qsydfhvs0n7affb2wij4wv2r7106887akz1aif2") (f (quote (("std") ("default" "std"))))))

(define-public crate-zuffer-0.1.1 (c (n "zuffer") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "fmmap") (r "^0.3") (d #t) (k 0)) (d (n "indexsort") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0jwwxqr3r7ngd7qi2ixr6nyfnphan29rbdybvhxxz3619s2qf12y") (f (quote (("std") ("default" "std"))))))

