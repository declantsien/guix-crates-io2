(define-module (crates-io zu sa zusammen) #:use-module (crates-io))

(define-public crate-zusammen-0.1.0 (c (n "zusammen") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("stream"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "08rf21a810xbp3v8c4sbkmfsr3p355xygq7aca0hvf7bpzm31jv7")))

(define-public crate-zusammen-0.1.1 (c (n "zusammen") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("stream"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "17yprgy2ahnx24bhpzalj4l3kql0krb7cdyppgfpy93vnkpxz9al")))

