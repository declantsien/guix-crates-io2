(define-module (crates-io zu la zula-core) #:use-module (crates-io))

(define-public crate-zula-core-0.0.8 (c (n "zula-core") (v "0.0.8") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "19sjbp5lzs6bj75nnj1m8j4cg2lbl2czj6wa7hm6ml6cwncv9mrv") (y #t)))

(define-public crate-zula-core-0.0.9 (c (n "zula-core") (v "0.0.9") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0zfzw6lkn71g9pgys19wgjb2yz2w10281p3a5lbximwvljh6ryxd") (y #t)))

(define-public crate-zula-core-1.0.0 (c (n "zula-core") (v "1.0.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1f0gqbfib3wsk9xmcpc95vn8lnyc5w6r060ai3ix3kd0qw7l57b1") (y #t)))

(define-public crate-zula-core-2.0.0 (c (n "zula-core") (v "2.0.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1ncwxbgy4g0z6qrmf6wiqxvzcacmslyqgza90a1bgr4knd9n3lcs") (y #t)))

(define-public crate-zula-core-2.0.1 (c (n "zula-core") (v "2.0.1") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0cs7qfcjs0bmsq372dj0z5zbm5dlzq9f0g4mv492a0z2h3d5h8fv") (y #t)))

(define-public crate-zula-core-3.0.0 (c (n "zula-core") (v "3.0.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1d5ng8ixzm8xds6g2sdkdiqwbwq3rm7ahjkpyidigrjwsdrlpghb") (y #t)))

(define-public crate-zula-core-3.0.1 (c (n "zula-core") (v "3.0.1") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "03xqly9ic7n2rjmq5y72ch1bfdmh9l9w9i4yrbbjs3q1nxsa3ga5") (y #t)))

(define-public crate-zula-core-3.0.2 (c (n "zula-core") (v "3.0.2") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1y2i2lca2m4wf4ps8iglaz72c6nq10xzn9ix2sbg3r1v5bynrkp9") (y #t)))

(define-public crate-zula-core-3.0.3 (c (n "zula-core") (v "3.0.3") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "041jzdkqfsxxcb5vhhgiyjm498wdbnvlgwjjilpdzq22wfdb0gxn") (y #t)))

(define-public crate-zula-core-3.0.4 (c (n "zula-core") (v "3.0.4") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "18g3jp9a3qagj51bcvzky8cva6c6f3j7qjrhbbg3c2gdvc7v6yhm") (y #t)))

(define-public crate-zula-core-3.0.5 (c (n "zula-core") (v "3.0.5") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "12knwh5pnarhb9fh7ya6y7qbqsvw8f4946b6nhxzd9h923p7cnrg") (y #t)))

(define-public crate-zula-core-4.0.0 (c (n "zula-core") (v "4.0.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "libloading") (r "^0.8.0") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0scj49wd67nbhbppbigwhflxdp4iqw0y1d5sdxdbqa12095pdvrv")))

