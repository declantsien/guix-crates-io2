(define-module (crates-io zu la zula) #:use-module (crates-io))

(define-public crate-zula-0.0.7 (c (n "zula") (v "0.0.7") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "zula-core") (r "^0.0.8") (d #t) (k 0)))) (h "0hszhy1iz0db7l9a3iz6gqphdrgd8i681xj5fwzyjfmd56nwgz4v")))

(define-public crate-zula-0.0.8 (c (n "zula") (v "0.0.8") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "zula-core") (r "^0.0.9") (d #t) (k 0)))) (h "1xsdnvp6qdm4jw98j5brxamwmlz499b8mpk38d4awj56kpfi611l")))

(define-public crate-zula-0.0.10 (c (n "zula") (v "0.0.10") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "zula-core") (r "^0.0.9") (d #t) (k 0)))) (h "02zhzbyq2gk8k33kzidyrnqbgqdzgsc7p3dz059imxkzyxcwa3j5")))

(define-public crate-zula-0.1.0 (c (n "zula") (v "0.1.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "zula-core") (r "^0.0.9") (d #t) (k 0)))) (h "00854ay78yv6hck2s1yxsk5f47lkixx9cygvapn99agf3ni744hv")))

(define-public crate-zula-0.1.1 (c (n "zula") (v "0.1.1") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "zula-core") (r "^0.0.9") (d #t) (k 0)))) (h "0vw480jjcrp79k5i99fmqr7n4wwpv5jiik6vajgqxfj548jzbs0v")))

(define-public crate-zula-1.0.2 (c (n "zula") (v "1.0.2") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "zula-core") (r "^0.0.9") (d #t) (k 0)))) (h "1cvjir4brcf9jqsacnpzbmqzpz9hlg6gigw62z005hh97wmwpzri")))

(define-public crate-zula-1.0.3 (c (n "zula") (v "1.0.3") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "zula-core") (r "^0.0.9") (d #t) (k 0)))) (h "10q1vdl83jh5mmngpnrwcff5gf7ciqf7vss6b3z06j37l1yh6f94")))

(define-public crate-zula-1.1.0 (c (n "zula") (v "1.1.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "zula-core") (r "^0.0.9") (d #t) (k 0)))) (h "08sfhn71km4vijkiyf5sp6b12j2423wz0azj8f2l9vi49965dg27")))

(define-public crate-zula-2.0.0 (c (n "zula") (v "2.0.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "zula-core") (r "^1.0.0") (d #t) (k 0)))) (h "0s01x7ngm1ywqgzvpgf9p6a7lvd1f09fgx326gjhrpwm9p6rk0fy")))

(define-public crate-zula-2.0.1 (c (n "zula") (v "2.0.1") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "zula-core") (r "^2.0.0") (d #t) (k 0)))) (h "0zqa90y5yhd7gqdbarfqwyqgy3gk8hj89944h64qbz80kga2ymyc")))

(define-public crate-zula-2.0.2 (c (n "zula") (v "2.0.2") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "zula-core") (r "^3.0.0") (d #t) (k 0)))) (h "0y0d5x4r8byrgi2lnlk96ndbqd0rkfi6x2cbhqzd3bxa23zphjgk")))

(define-public crate-zula-2.0.3 (c (n "zula") (v "2.0.3") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "zula-core") (r "^3.0.2") (d #t) (k 0)))) (h "1jzh4279a3c6bmssn59x7aq3dp65qgv5l130as4nf4andln0c42q")))

