(define-module (crates-io zu ne zune-bmp) #:use-module (crates-io))

(define-public crate-zune-bmp-0.4.0 (c (n "zune-bmp") (v "0.4.0") (d (list (d (n "zune-core") (r "^0.4") (d #t) (k 0)))) (h "03wcrvc3a5s2154bdp07mrsxghjpmc2nabghzh1a4jrlhj6ns79v") (f (quote (("log" "zune-core/log"))))))

(define-public crate-zune-bmp-0.5.0-rc0 (c (n "zune-bmp") (v "0.5.0-rc0") (d (list (d (n "zune-core") (r "^0.5.0-rc0") (d #t) (k 0)))) (h "1jhdsng3pskr8hbn15w7a4312l5da120zwylx4djxfmnkjgqy15i") (f (quote (("std" "zune-core/std") ("log" "zune-core/log"))))))

(define-public crate-zune-bmp-0.5.0-rc1 (c (n "zune-bmp") (v "0.5.0-rc1") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "zune-core") (r "^0.5.0-rc0") (d #t) (k 0)))) (h "0w9swqwbgaswlpb53y71fc0wv20p179h6hinvf1w1bw6p2rpkrv6") (f (quote (("std" "zune-core/std") ("rgb_inverse") ("log" "zune-core/log"))))))

(define-public crate-zune-bmp-0.5.0-rc2 (c (n "zune-bmp") (v "0.5.0-rc2") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "zune-core") (r "^0.5.0-rc0") (d #t) (k 0)))) (h "1mx1hdavla67ygxkcjjjx8ns96lw1lkhd8qpa5382gq8952jxz73") (f (quote (("std" "zune-core/std") ("rgb_inverse") ("log" "zune-core/log"))))))

(define-public crate-zune-bmp-0.5.0-rc3 (c (n "zune-bmp") (v "0.5.0-rc3") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "zune-core") (r "^0.5.0-rc0") (d #t) (k 0)))) (h "1xj44hncdc6qpxi5c7w1aagsv9gqw8cs80jjzrklkg1j5br3x3f3") (f (quote (("std" "zune-core/std") ("rgb_inverse") ("log" "zune-core/log"))))))

(define-public crate-zune-bmp-0.5.0-rc4 (c (n "zune-bmp") (v "0.5.0-rc4") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "zune-core") (r "^0.5.0-rc1") (d #t) (k 0)))) (h "13clpni1rg5a3jyxsv0ywbh558pcpb66n0lag9m852fz4lxi3nj2") (f (quote (("std" "zune-core/std") ("rgb_inverse") ("log" "zune-core/log"))))))

