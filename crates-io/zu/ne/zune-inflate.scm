(define-module (crates-io zu ne zune-inflate) #:use-module (crates-io))

(define-public crate-zune-inflate-0.2.0 (c (n "zune-inflate") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (k 2)) (d (n "flate2") (r "^1.0.25") (f (quote ("zlib-ng"))) (k 2)) (d (n "libdeflater") (r "^0.11.0") (d #t) (k 2)) (d (n "simd-adler32") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "09kzkhiww7a9xwdy384vm9la5wf7kcfpanq44rcr7wicmip9gg5h") (f (quote (("zlib" "simd-adler32") ("gzip") ("default" "zlib" "gzip"))))))

(define-public crate-zune-inflate-0.2.1 (c (n "zune-inflate") (v "0.2.1") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (k 2)) (d (n "flate2") (r "^1.0.25") (f (quote ("zlib-ng"))) (k 2)) (d (n "libdeflater") (r "^0.11.0") (d #t) (k 2)) (d (n "simd-adler32") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "00k4yp5688bhzgja0m8vqgpx26mrc8njbsm9scfbwahs7yffvsc3") (f (quote (("zlib" "simd-adler32") ("gzip") ("default" "zlib" "gzip"))))))

(define-public crate-zune-inflate-0.2.2 (c (n "zune-inflate") (v "0.2.2") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (k 2)) (d (n "flate2") (r "^1.0.25") (f (quote ("zlib-ng"))) (k 2)) (d (n "libdeflater") (r "^0.11.0") (d #t) (k 2)) (d (n "simd-adler32") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "11xbmby8814a7br6b1plawkbxfmqjijlyps9l51k29mc5y2pzm4x") (f (quote (("zlib" "simd-adler32") ("gzip") ("default" "zlib" "gzip"))))))

(define-public crate-zune-inflate-0.2.3 (c (n "zune-inflate") (v "0.2.3") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (k 2)) (d (n "flate2") (r "^1.0.25") (f (quote ("zlib-ng"))) (k 2)) (d (n "libdeflater") (r "^0.11.0") (d #t) (k 2)) (d (n "simd-adler32") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "11hgy776jvmsz3inlnnar8ir6jrdzavrv21cy76h4lhf37f74hdy") (f (quote (("zlib" "simd-adler32") ("gzip") ("default" "zlib" "gzip"))))))

(define-public crate-zune-inflate-0.2.4 (c (n "zune-inflate") (v "0.2.4") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (k 2)) (d (n "flate2") (r "^1.0.25") (f (quote ("zlib-ng"))) (k 2)) (d (n "libdeflater") (r "^0.11.0") (d #t) (k 2)) (d (n "simd-adler32") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "12nifglfvckl7f28dgpr7n1wriqs05xdpn7j60ssi8l71dpbcwbl") (f (quote (("zlib" "simd-adler32") ("gzip") ("default" "zlib" "gzip"))))))

(define-public crate-zune-inflate-0.2.41 (c (n "zune-inflate") (v "0.2.41") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (k 2)) (d (n "flate2") (r "^1.0.25") (f (quote ("zlib-ng"))) (k 2)) (d (n "libdeflater") (r "^0.11.0") (d #t) (k 2)) (d (n "simd-adler32") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "18rwzcmvaibg7g6rnmknf96jf1p0dbylwv96hjqyizc3f9gm4csg") (f (quote (("zlib" "simd-adler32") ("gzip") ("default" "zlib" "gzip"))))))

(define-public crate-zune-inflate-0.2.42 (c (n "zune-inflate") (v "0.2.42") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (k 2)) (d (n "flate2") (r "^1.0.25") (f (quote ("zlib-ng"))) (k 2)) (d (n "libdeflater") (r "^0.11.0") (d #t) (k 2)) (d (n "simd-adler32") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "0v9dj1zgf325yraby2mafz4yjy366g6l9yaq4xmar8y425y3fwy4") (f (quote (("zlib" "simd-adler32") ("gzip") ("default" "zlib" "gzip"))))))

(define-public crate-zune-inflate-0.2.50 (c (n "zune-inflate") (v "0.2.50") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (k 2)) (d (n "flate2") (r "^1.0.25") (f (quote ("zlib-ng"))) (k 2)) (d (n "libdeflater") (r "^0.11.0") (d #t) (k 2)) (d (n "simd-adler32") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "0h3d46jfyraxzl7kcgr2zpqjmisw72lc1p44b4q9r0rhcbglb4jq") (f (quote (("zlib" "simd-adler32") ("std") ("gzip") ("default" "zlib" "gzip" "std"))))))

(define-public crate-zune-inflate-0.2.51 (c (n "zune-inflate") (v "0.2.51") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (k 2)) (d (n "flate2") (r "^1.0.25") (f (quote ("zlib-ng"))) (k 2)) (d (n "libdeflater") (r "^0.11.0") (d #t) (k 2)) (d (n "simd-adler32") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "0sxcm2rynp65r9k7s03a86kwvhldrphibxqiij5f5dxrkyvjh5x0") (f (quote (("zlib" "simd-adler32") ("std") ("gzip") ("default" "zlib" "gzip" "std"))))))

(define-public crate-zune-inflate-0.2.52 (c (n "zune-inflate") (v "0.2.52") (d (list (d (n "simd-adler32") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "1q37v8v0hcpb805wxjrkk6yv9fazax11jr269pvkn8byi7iqxjhh") (f (quote (("zlib" "simd-adler32") ("std") ("gzip") ("default" "zlib" "gzip" "std"))))))

(define-public crate-zune-inflate-0.2.53 (c (n "zune-inflate") (v "0.2.53") (d (list (d (n "simd-adler32") (r "^0.3.4") (o #t) (d #t) (k 0)))) (h "1y5aklf5sc1cmghidci8wzm0g0y3hq1v3abfhi5jwi66b7yhh2j4") (f (quote (("zlib" "simd-adler32") ("std") ("gzip") ("default" "zlib" "gzip" "std"))))))

(define-public crate-zune-inflate-0.2.54 (c (n "zune-inflate") (v "0.2.54") (d (list (d (n "simd-adler32") (r "^0.3.4") (o #t) (k 0)))) (h "00kg24jh3zqa3i6rg6yksnb71bch9yi1casqydl00s7nw8pk7avk") (f (quote (("zlib" "simd-adler32") ("std" "simd-adler32/std") ("gzip") ("default" "zlib" "gzip" "std"))))))

