(define-module (crates-io zu ne zune-hdr) #:use-module (crates-io))

(define-public crate-zune-hdr-0.4.0 (c (n "zune-hdr") (v "0.4.0") (d (list (d (n "zune-core") (r "^0.4") (k 0)))) (h "09k4d043n7dj99x5zy3arpkjvv82hyvvminvqspi82zq6534gzsi") (f (quote (("log" "zune-core/log"))))))

(define-public crate-zune-hdr-0.5.0-rc0 (c (n "zune-hdr") (v "0.5.0-rc0") (d (list (d (n "zune-core") (r "^0.5.0-rc0") (f (quote ("std"))) (k 0)))) (h "0rsqac0dzqkz95dpbg055ihdzx95rmy7pg3fnmsbv0y071vmb0d0") (f (quote (("log" "zune-core/log"))))))

