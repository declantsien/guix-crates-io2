(define-module (crates-io zu ne zune-core) #:use-module (crates-io))

(define-public crate-zune-core-0.2.0 (c (n "zune-core") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)))) (h "14y3qhaja12176smzzp28a3nf4glc54sa2x3dz32cfclgjq2m1f4") (f (quote (("std") ("default" "serde"))))))

(define-public crate-zune-core-0.2.1 (c (n "zune-core") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)))) (h "1285shac96a0ph9ka7m5555rbn4qizbgk5n8aaxxyzia76sbszmr") (f (quote (("std") ("default" "serde"))))))

(define-public crate-zune-core-0.2.12 (c (n "zune-core") (v "0.2.12") (d (list (d (n "bitflags") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.52") (o #t) (d #t) (k 0)))) (h "00rdw8cndxcjjv9z45z60p08ss8m5k59g4da8ishq2v0gz67sxy3") (f (quote (("std") ("default" "serde"))))))

(define-public crate-zune-core-0.2.13 (c (n "zune-core") (v "0.2.13") (d (list (d (n "bitflags") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.52") (o #t) (d #t) (k 0)))) (h "0sqh8qw77nzlc1sn2kr0az669v75119x00n1wirh13qxnbhc94j8") (f (quote (("std") ("default" "serde"))))))

(define-public crate-zune-core-0.2.14 (c (n "zune-core") (v "0.2.14") (d (list (d (n "bitflags") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.52") (o #t) (d #t) (k 0)))) (h "1vnfjrpg82j2lkxc3lvkpqbcbl9ywfzl4xcpxvbxiw1aw313dji9") (f (quote (("std"))))))

(define-public crate-zune-core-0.4.0 (c (n "zune-core") (v "0.4.0") (d (list (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.52") (o #t) (d #t) (k 0)))) (h "0wmx9arl4p11qyman0aiz1yz5fqx04wr0gjndlm2ipfbq1kqlb0z") (f (quote (("std"))))))

(define-public crate-zune-core-0.4.1 (c (n "zune-core") (v "0.4.1") (d (list (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.52") (o #t) (d #t) (k 0)))) (h "0i494007f673xza4h8xkz67fgwrlcnmyq46vcyzd9qxn66nl0yqn") (f (quote (("std"))))))

(define-public crate-zune-core-0.4.11 (c (n "zune-core") (v "0.4.11") (d (list (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.52") (o #t) (d #t) (k 0)))) (h "1wn0wml0hrqywm3ar2zs372mcw4ywzsrl323rcx6aalf5brj5qda") (f (quote (("std"))))))

(define-public crate-zune-core-0.4.12 (c (n "zune-core") (v "0.4.12") (d (list (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.52") (o #t) (d #t) (k 0)))) (h "0jj1ra86klzlcj9aha9als9d1dzs7pqv3azs1j3n96822wn3lhiz") (f (quote (("std"))))))

(define-public crate-zune-core-0.5.0-rc0 (c (n "zune-core") (v "0.5.0-rc0") (d (list (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.52") (o #t) (d #t) (k 0)))) (h "0l5rdw2k74wl2684np7llic1bcqai5k28z88lghkzf6illc9vwxi") (f (quote (("std"))))))

(define-public crate-zune-core-0.5.0-rc1 (c (n "zune-core") (v "0.5.0-rc1") (d (list (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.52") (o #t) (d #t) (k 0)))) (h "1l5pbsynfwv0izyjz41p0imapnlha858dc29gi4s4liv6wkv9lg0") (f (quote (("std"))))))

