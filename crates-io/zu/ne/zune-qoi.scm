(define-module (crates-io zu ne zune-qoi) #:use-module (crates-io))

(define-public crate-zune-qoi-0.4.0 (c (n "zune-qoi") (v "0.4.0") (d (list (d (n "zune-core") (r "^0.4") (d #t) (k 0)))) (h "02llh26xmbhljnvah6j6n5r2ny9wx6q4wisj391rg02m6mkc7lgm") (f (quote (("std") ("log" "zune-core/log") ("default" "std" "log"))))))

(define-public crate-zune-qoi-0.4.10 (c (n "zune-qoi") (v "0.4.10") (d (list (d (n "zune-core") (r "^0.4") (d #t) (k 0)))) (h "1jg9hhj15zjwc8ycrmanl5an1d43vf6ps1xm7i0ldrgd55kqnnfw") (f (quote (("std") ("log" "zune-core/log") ("default" "std" "log"))))))

(define-public crate-zune-qoi-0.5.0-rc0 (c (n "zune-qoi") (v "0.5.0-rc0") (d (list (d (n "zune-core") (r "^0.5.0-rc0") (d #t) (k 0)))) (h "007f0mgq1kdx9gz9aml28ipf1vnj9zkqw1sj8gz64si5ky4ahj5q") (f (quote (("std" "zune-core/std") ("log" "zune-core/log") ("default" "std" "log"))))))

