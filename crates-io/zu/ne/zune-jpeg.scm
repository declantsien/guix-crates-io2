(define-module (crates-io zu ne zune-jpeg) #:use-module (crates-io))

(define-public crate-zune-jpeg-0.1.0 (c (n "zune-jpeg") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "jpeg-decoder") (r "^0.2.6") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mozjpeg") (r "^0.9.2") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "0ym0lwbyprmvrsafs56f29psaw2g5m8v3n3y3v8nrbx5pw3x00pp") (f (quote (("x86") ("default" "x86"))))))

(define-public crate-zune-jpeg-0.1.1 (c (n "zune-jpeg") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "jpeg-decoder") (r "^0.2.6") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mozjpeg") (r "^0.9.2") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "14qvadhp0qhb30flq545miday0637mgnnn2ima9a5946lc5z2k1x") (f (quote (("x86") ("default" "x86"))))))

(define-public crate-zune-jpeg-0.1.2 (c (n "zune-jpeg") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "jpeg-decoder") (r "^0.2.6") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mozjpeg") (r "^0.9.2") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "16cqwwixzffg47jv0ncwdykfqj228hzfln2rhq85nzhka3ijc6qx") (f (quote (("x86") ("default" "x86"))))))

(define-public crate-zune-jpeg-0.1.3 (c (n "zune-jpeg") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "jpeg-decoder") (r "^0.2.6") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mozjpeg") (r "^0.9.2") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "1fl213yxiw83bj4wi1k0cqpszsayff2zdw57nghb5fph7wjxdyg8") (f (quote (("x86") ("default" "x86"))))))

(define-public crate-zune-jpeg-0.1.4 (c (n "zune-jpeg") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "jpeg-decoder") (r "^0.2.6") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mozjpeg") (r "^0.9.2") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "03bil92rx2f5xz7r5x1yqbag88vpvzxawwq8ccpk8hz1rf0sxrpc") (f (quote (("x86") ("default" "x86"))))))

(define-public crate-zune-jpeg-0.1.5 (c (n "zune-jpeg") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "jpeg-decoder") (r "^0.2.6") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mozjpeg") (r "^0.9.2") (d #t) (k 2)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "1z4gqva6cwg17is42j4jzpgkrzrx05gjb7npc1znmjq1ap2wnncy") (f (quote (("x86") ("default" "x86"))))))

(define-public crate-zune-jpeg-0.2.0 (c (n "zune-jpeg") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "jpeg-decoder") (r "^0.2.6") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mozjpeg") (r "^0.9.2") (d #t) (k 2)) (d (n "scoped_threadpool") (r "^0.1.9") (d #t) (k 0)))) (h "0g4mnb9411qx5klmf94yaz9p4x8s1ijh4k06d74nkkcf5kg8rblk") (f (quote (("x86") ("default" "x86"))))))

(define-public crate-zune-jpeg-0.3.0 (c (n "zune-jpeg") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (k 2)) (d (n "jpeg-decoder") (r "^0.2.6") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mozjpeg") (r "^0.9.2") (d #t) (k 2)) (d (n "xxhash-rust") (r "^0.8.6") (f (quote ("xxh3"))) (k 2)) (d (n "zune-core") (r "^0.2.1") (d #t) (k 0)))) (h "0hx09v449q24x9mwj5nds0ac6ik8g92f3ms2d7049xj4gl4351n5") (f (quote (("x86") ("std" "zune-core/std") ("default" "x86" "std"))))))

(define-public crate-zune-jpeg-0.3.1 (c (n "zune-jpeg") (v "0.3.1") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (k 2)) (d (n "jpeg-decoder") (r "^0.2.6") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mozjpeg") (r "^0.9.2") (d #t) (k 2)) (d (n "xxhash-rust") (r "^0.8.6") (f (quote ("xxh3"))) (k 2)) (d (n "zune-core") (r "^0.2.1") (d #t) (k 0)))) (h "1yiccmhavdh6x3dxm1s312gx0jh2d9jl31y9mhbz30drcrsr5isj") (f (quote (("x86") ("std" "zune-core/std") ("default" "x86" "std"))))))

(define-public crate-zune-jpeg-0.3.10 (c (n "zune-jpeg") (v "0.3.10") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (k 2)) (d (n "jpeg-decoder") (r "^0.2.6") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mozjpeg") (r "^0.9.2") (d #t) (k 2)) (d (n "xxhash-rust") (r "^0.8.6") (f (quote ("xxh3"))) (k 2)) (d (n "zune-core") (r "^0.2.1") (d #t) (k 0)))) (h "0k8s75ya5x3z14z2vj4mrn6sa9xh6pa3c7d09yhi5is8yw99ks93") (f (quote (("x86") ("std" "zune-core/std") ("default" "x86" "std"))))))

(define-public crate-zune-jpeg-0.3.11 (c (n "zune-jpeg") (v "0.3.11") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (k 2)) (d (n "jpeg-decoder") (r "^0.2.6") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mozjpeg") (r "^0.9.2") (d #t) (k 2)) (d (n "xxhash-rust") (r "^0.8.6") (f (quote ("xxh3"))) (k 2)) (d (n "zune-core") (r "^0.2.1") (d #t) (k 0)))) (h "0znifpjjzwhanpl53sy81qvsmm555g11rf4q0j8588y1kvdpz3x4") (f (quote (("x86") ("std" "zune-core/std") ("default" "x86" "std"))))))

(define-public crate-zune-jpeg-0.3.12 (c (n "zune-jpeg") (v "0.3.12") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (k 2)) (d (n "jpeg-decoder") (r "^0.2.6") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "mozjpeg") (r "^0.9.2") (d #t) (k 2)) (d (n "xxhash-rust") (r "^0.8.6") (f (quote ("xxh3"))) (k 2)) (d (n "zune-core") (r "^0.2.1") (d #t) (k 0)))) (h "1ji3k04a9n2g241p4xz1mnivgay4sahl9dsydl71hd423jm1mga4") (f (quote (("x86") ("std" "zune-core/std") ("default" "x86" "std"))))))

(define-public crate-zune-jpeg-0.3.13 (c (n "zune-jpeg") (v "0.3.13") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "zune-core") (r "^0.2.1") (d #t) (k 0)))) (h "029knc93za1bc3654q2rii5v1j4grzpmlll8009qnpnfq426b9rx") (f (quote (("x86") ("std" "zune-core/std") ("default" "x86" "std"))))))

(define-public crate-zune-jpeg-0.3.14 (c (n "zune-jpeg") (v "0.3.14") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "zune-core") (r "^0.2.1") (d #t) (k 0)))) (h "1kjn520gdzps2l36yry1i7p193b8fgjfkk9baklj7ghz8w1dx8bd") (f (quote (("x86") ("std" "zune-core/std") ("default" "x86" "std"))))))

(define-public crate-zune-jpeg-0.3.15 (c (n "zune-jpeg") (v "0.3.15") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "zune-core") (r "^0.2.12") (d #t) (k 0)))) (h "0yis9m27jkklffwd6ssvh5v7zdsmzmdckdm09nd5va4ikmbha787") (f (quote (("x86") ("std" "zune-core/std") ("default" "x86" "std"))))))

(define-public crate-zune-jpeg-0.3.16 (c (n "zune-jpeg") (v "0.3.16") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "zune-core") (r "^0.2.13") (d #t) (k 0)))) (h "0qaqdh0078br6dwp4vjvvr036qil2lwa7n3bp000dqliqsia7ipz") (f (quote (("x86") ("std" "zune-core/std") ("default" "x86" "std"))))))

(define-public crate-zune-jpeg-0.3.17 (c (n "zune-jpeg") (v "0.3.17") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "zune-core") (r "^0.2") (d #t) (k 0)))) (h "07lrnkc51aaw7fc2lvsmz37bmc1bybgspcqaj5wwrgcxybsfhj18") (f (quote (("x86") ("std" "zune-core/std") ("default" "x86" "std"))))))

(define-public crate-zune-jpeg-0.4.0 (c (n "zune-jpeg") (v "0.4.0") (d (list (d (n "zune-core") (r "^0.4") (d #t) (k 0)))) (h "1b922bhynp7yjwv48yrhgaijicnbgfmia3l5lp9cdx3rrx147cr0") (f (quote (("x86") ("std" "zune-core/std") ("neon") ("log" "zune-core/log") ("default" "x86" "neon" "std"))))))

(define-public crate-zune-jpeg-0.4.10 (c (n "zune-jpeg") (v "0.4.10") (d (list (d (n "zune-core") (r "^0.4") (d #t) (k 0)))) (h "0r27bjrjs25m82ckx46bg636z06r2a5mwnm78d4hcrm3ipkfdjyp") (f (quote (("x86") ("std" "zune-core/std") ("neon") ("log" "zune-core/log") ("default" "x86" "neon" "std"))))))

(define-public crate-zune-jpeg-0.4.11 (c (n "zune-jpeg") (v "0.4.11") (d (list (d (n "zune-core") (r "^0.4") (d #t) (k 0)))) (h "0j74rzx82w9zwfqvzrg7k67l77qp3g577w33scrn3zd1l926p1pc") (f (quote (("x86") ("std" "zune-core/std") ("neon") ("log" "zune-core/log") ("default" "x86" "neon" "std"))))))

(define-public crate-zune-jpeg-0.5.0-rc0 (c (n "zune-jpeg") (v "0.5.0-rc0") (d (list (d (n "zune-core") (r "^0.5.0-rc0") (d #t) (k 0)))) (h "12d63njsa3qxfcpzqj0iw24qg1k08z4z2844pyzl0g06apsxv8nl") (f (quote (("x86") ("std" "zune-core/std") ("neon") ("log" "zune-core/log") ("default" "x86" "neon" "std"))))))

(define-public crate-zune-jpeg-0.5.0-rc1 (c (n "zune-jpeg") (v "0.5.0-rc1") (d (list (d (n "zune-core") (r "^0.5.0-rc1") (d #t) (k 0)))) (h "1q1nhzbisf54bhzmpiav20g2y8fx6i86wwdbza0jvns69qxrl0g4") (f (quote (("x86") ("std" "zune-core/std") ("neon") ("log" "zune-core/log") ("default" "x86" "neon" "std"))))))

