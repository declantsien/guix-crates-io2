(define-module (crates-io zu ne zune-farbfeld) #:use-module (crates-io))

(define-public crate-zune-farbfeld-0.2.0 (c (n "zune-farbfeld") (v "0.2.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "zune-core") (r "^0.2") (d #t) (k 0)))) (h "1rldmj6d1qs204yazan1gnmdka4cccxs653fdpg32g65c5sxmw8l")))

(define-public crate-zune-farbfeld-0.4.0 (c (n "zune-farbfeld") (v "0.4.0") (d (list (d (n "zune-core") (r "^0.4") (d #t) (k 0)))) (h "04gh23s72hz8gz80phk0ljf427ksi4hj8qx88hh5d72lh3569a7f") (f (quote (("log" "zune-core/log"))))))

(define-public crate-zune-farbfeld-0.5.0-rc0 (c (n "zune-farbfeld") (v "0.5.0-rc0") (d (list (d (n "zune-core") (r "^0.5.0-rc0") (d #t) (k 0)))) (h "1n5ipg3v35nw79fv0mmrvmk5grxfv34sa4k8p56b0j9p4sri9vrz") (f (quote (("log" "zune-core/log"))))))

