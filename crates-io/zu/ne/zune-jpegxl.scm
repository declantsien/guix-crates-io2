(define-module (crates-io zu ne zune-jpegxl) #:use-module (crates-io))

(define-public crate-zune-jpegxl-0.4.0 (c (n "zune-jpegxl") (v "0.4.0") (d (list (d (n "zune-core") (r "^0.4") (d #t) (k 0)))) (h "0x1nkrxil53d3nxayflc3wzdd1m17zdvys27xncmzcc48d4fxzvi") (f (quote (("threads") ("std") ("log" "zune-core/log") ("default" "threads" "std"))))))

(define-public crate-zune-jpegxl-0.5.0-rc0 (c (n "zune-jpegxl") (v "0.5.0-rc0") (d (list (d (n "zune-core") (r "^0.5.0-rc0") (d #t) (k 0)))) (h "1whxcyxz3xq0rbibmlxcq9ax3cr570scapz0bfphhi1bbnccrcw1") (f (quote (("threads") ("std") ("log" "zune-core/log") ("default" "threads" "std"))))))

