(define-module (crates-io zu ne zune-psd) #:use-module (crates-io))

(define-public crate-zune-psd-0.4.0 (c (n "zune-psd") (v "0.4.0") (d (list (d (n "zune-core") (r "^0.4") (d #t) (k 0)))) (h "0jadhgyjk0i3s5v347671wy77c7a8z34w3y2limj04qqc9fjf4db") (f (quote (("log" "zune-core/log"))))))

(define-public crate-zune-psd-0.5.0-rc0 (c (n "zune-psd") (v "0.5.0-rc0") (d (list (d (n "zune-core") (r "^0.5.0-rc0") (d #t) (k 0)))) (h "1rmincd6dcqc8m4jrsznlnfgzn09zjmm2hjdzpibp3pyhlmpmw6g") (f (quote (("log" "zune-core/log"))))))

