(define-module (crates-io zu ol zuolar-galaxy) #:use-module (crates-io))

(define-public crate-zuolar-galaxy-0.1.0 (c (n "zuolar-galaxy") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.16") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "037h3xnzx8h65jlncmbyls4d3fv7fcbm1hv8pfdc9nr9730gr02k")))

