(define-module (crates-io zu si zusi-protocol) #:use-module (crates-io))

(define-public crate-zusi-protocol-0.1.0 (c (n "zusi-protocol") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zusi-protocol-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0q0kdsahz04fg461233wj399ak9i7q9k8jyg990jvzrfgzgxd5jk") (f (quote (("parser" "nom") ("default"))))))

(define-public crate-zusi-protocol-0.1.1 (c (n "zusi-protocol") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zusi-protocol-derive") (r "^0.1.1") (d #t) (k 0)))) (h "0cnwb53mlnlxvxlz7n22fvp6mmh40jkhsylw9pjhd3hwvb6lg70d") (f (quote (("parser" "nom") ("default"))))))

(define-public crate-zusi-protocol-0.1.2 (c (n "zusi-protocol") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zusi-protocol-derive") (r "^0.1.1") (d #t) (k 0)))) (h "1wgpjcjxj9l84dli6klklxbd2h2hhpgg6z0l494sdxb82w3zl3sa") (f (quote (("default")))) (s 2) (e (quote (("parser" "dep:nom"))))))

(define-public crate-zusi-protocol-0.2.0 (c (n "zusi-protocol") (v "0.2.0") (d (list (d (n "either") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zusi-protocol-derive") (r "^0.1.1") (d #t) (k 0)))) (h "0mbn1gn2c8azs9rxcqifnrl920ikhrh8fabxypmyia002rc8nfqf") (f (quote (("default")))) (s 2) (e (quote (("parser" "dep:nom" "dep:either"))))))

(define-public crate-zusi-protocol-0.2.1 (c (n "zusi-protocol") (v "0.2.1") (d (list (d (n "either") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zusi-protocol-derive") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "0zky0bpxq222vwzds0bybyrfv8aymhlbfv9p70yr6cr9hjvmws8d") (f (quote (("default" "derive")))) (s 2) (e (quote (("parser" "dep:nom" "dep:either") ("derive" "dep:zusi-protocol-derive"))))))

