(define-module (crates-io zu si zusi-fahrpult) #:use-module (crates-io))

(define-public crate-zusi-fahrpult-0.1.0 (c (n "zusi-fahrpult") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zusi-protocol") (r "^0.1.0") (d #t) (k 0)) (d (n "zusi-protocol-derive") (r "^0.1.0") (d #t) (k 0)))) (h "08crn2m7f59gh0rmsw192qgais6vksqjjlp6yi32pk96nczwawgl")))

(define-public crate-zusi-fahrpult-0.1.1 (c (n "zusi-fahrpult") (v "0.1.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zusi-protocol") (r "^0.1.1") (d #t) (k 0)) (d (n "zusi-protocol-derive") (r "^0.1.1") (d #t) (k 0)))) (h "15cbi5sf0l1x7x1k5ks6nyr1kyfgqhp4kdngn5h7ia8m0ndyy1bc")))

(define-public crate-zusi-fahrpult-0.1.2 (c (n "zusi-fahrpult") (v "0.1.2") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zusi-protocol") (r "^0.1.2") (d #t) (k 0)) (d (n "zusi-protocol-derive") (r "^0.1.1") (d #t) (k 0)))) (h "1hfbq5li20v5d2i416164fx0wpnbjr845rigs62biyxpsc9kpsfq")))

(define-public crate-zusi-fahrpult-0.1.3 (c (n "zusi-fahrpult") (v "0.1.3") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zusi-protocol") (r "^0.2.0") (d #t) (k 0)) (d (n "zusi-protocol-derive") (r "^0.1.1") (d #t) (k 0)))) (h "0wx9hcp5qli4c73xb0pdkgmbm3kmbyc6n5hn77303788dfh2rrag")))

(define-public crate-zusi-fahrpult-0.1.4 (c (n "zusi-fahrpult") (v "0.1.4") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zusi-protocol") (r "^0.2.1") (d #t) (k 0)))) (h "0miy68pxgpg1f4m4gjwcrm5wwplqpan0cm5cacnc0qwfa0nsai45")))

(define-public crate-zusi-fahrpult-0.1.5 (c (n "zusi-fahrpult") (v "0.1.5") (d (list (d (n "zusi-protocol") (r "^0.2.1") (d #t) (k 0)))) (h "125xmlfir856acpckvdv9idc1zh1fhgbasz87zbv48q3alc3wmq6")))

