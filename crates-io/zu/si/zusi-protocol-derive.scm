(define-module (crates-io zu si zusi-protocol-derive) #:use-module (crates-io))

(define-public crate-zusi-protocol-derive-0.1.0 (c (n "zusi-protocol-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("default" "derive"))) (d #t) (k 0)))) (h "1n75lrxps42ppix59qh4ifwj75jz1j9ik2xkinpr01zd9ghibzma")))

(define-public crate-zusi-protocol-derive-0.1.1 (c (n "zusi-protocol-derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("default" "derive"))) (d #t) (k 0)))) (h "1bgybdr4gppsn0cisxw47q9cnsjyvd11485avm0vgx3wj9j47nj6")))

(define-public crate-zusi-protocol-derive-0.1.2 (c (n "zusi-protocol-derive") (v "0.1.2") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0h6ddj7cp5xyx0brgqnrfsrrv5gy1nz5299p7ildb1v1a37v4ddh")))

