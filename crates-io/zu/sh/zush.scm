(define-module (crates-io zu sh zush) #:use-module (crates-io))

(define-public crate-zush-0.1.0 (c (n "zush") (v "0.1.0") (h "1bi3sdbgpf5nv4q8wm5r8h6qlgiw4dimfscp074vv8cpcc575i8w") (y #t)))

(define-public crate-zush-0.0.0 (c (n "zush") (v "0.0.0") (h "14i1zzz4zdkpx1cma9fw04iak64ziaj47z3w9nxl3jyx198c127v") (y #t)))

