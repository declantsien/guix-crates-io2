(define-module (crates-io zu bb zubbers) #:use-module (crates-io))

(define-public crate-zubbers-0.0.1 (c (n "zubbers") (v "0.0.1") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "flame") (r "^0.2.2") (d #t) (k 0)) (d (n "flamer") (r "^0.3") (d #t) (k 0)) (d (n "fnv") (r "^1.0.3") (d #t) (k 0)) (d (n "hashbrown") (r "^0.7.2") (d #t) (k 0)) (d (n "im-rc") (r "^14.3.0") (d #t) (k 0)) (d (n "logos") (r "^0.11.4") (d #t) (k 2)))) (h "0s8v89r52l879xvkp5q4w0xy3g2pqqww5n4lf3fchj1b55p5ysry")))

