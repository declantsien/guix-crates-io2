(define-module (crates-io zu ri zuri_nbt) #:use-module (crates-io))

(define-public crate-zuri_nbt-0.3.0 (c (n "zuri_nbt") (v "0.3.0") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 2)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1gk8r382jpgckiknlnya5xmflv8fq4zwh5al0p5mi6r34lwvm0hr")))

