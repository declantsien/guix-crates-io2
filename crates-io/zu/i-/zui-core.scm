(define-module (crates-io zu i- zui-core) #:use-module (crates-io))

(define-public crate-zui-core-0.0.1 (c (n "zui-core") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hi2651xq6ql2a21w3j3j1phsqpxziyywkr9szp3g1xbdkqqp0w9")))

(define-public crate-zui-core-0.0.2 (c (n "zui-core") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0naaz35qlikwmc23hbsq66ng56l44igysil089gki3jydfm2gxfs")))

(define-public crate-zui-core-0.0.3 (c (n "zui-core") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0kfvhd6x5bh2bbkjam4nw7l5qqjjh2517wfi90hk4xz4jr5gdzkk")))

