(define-module (crates-io zu -u zu-util) #:use-module (crates-io))

(define-public crate-zu-util-0.1.0 (c (n "zu-util") (v "0.1.0") (h "1fgj9nphy36jx1mz5ha6hkjvbsgrpdkkaw8fdw53niw3irbbn59c")))

(define-public crate-zu-util-0.2.1 (c (n "zu-util") (v "0.2.1") (d (list (d (n "roxmltree") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "yew") (r "^0.20.0") (d #t) (k 0)))) (h "1bjh8nxz63vgk0bpfv8wb3b69i8y2lkjlrbb71fmrqkbc1lqhv53") (f (quote (("icon" "roxmltree") ("default"))))))

(define-public crate-zu-util-0.3.1 (c (n "zu-util") (v "0.3.1") (d (list (d (n "roxmltree") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "yew") (r "^0.20.0") (d #t) (k 0)))) (h "1bcwjwc14lg9d2a0yg2fz7dc8r6f4sp5krq6ib3nmwdsnpkmq0v7") (f (quote (("icon" "roxmltree") ("default"))))))

(define-public crate-zu-util-0.3.2 (c (n "zu-util") (v "0.3.2") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "roxmltree") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("HtmlImageElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.20.0") (d #t) (k 0)))) (h "1aryvbxph789byn9a105rd3snzs9iz53qzmpk9xvinyyblzgahhr") (f (quote (("icon" "roxmltree") ("default"))))))

(define-public crate-zu-util-0.3.3 (c (n "zu-util") (v "0.3.3") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "roxmltree") (r "^0.18.0") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("HtmlImageElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.20.0") (d #t) (k 0)))) (h "1d6nbb9wfdzc662w2vkxhylrf7n666w9mzc7fm1zx1dcaz52g8yl") (f (quote (("icon" "roxmltree") ("default"))))))

(define-public crate-zu-util-0.3.4 (c (n "zu-util") (v "0.3.4") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "roxmltree") (r "^0.18.1") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.88") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.65") (f (quote ("HtmlImageElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.21.0") (d #t) (k 0)))) (h "0fdvq9fqyyr8xhnlr2jzgn0dfpf4md9vba4dbipkb6ipyira58s3") (f (quote (("icon" "roxmltree") ("default"))))))

