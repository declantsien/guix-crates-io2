(define-module (crates-io zu ic zuicon-util) #:use-module (crates-io))

(define-public crate-zuicon-util-0.1.0 (c (n "zuicon-util") (v "0.1.0") (h "0nzm0zf32b7gq8yqnnf325d48wl6kvjjvfd9rnb35la5l3jvrz7i")))

(define-public crate-zuicon-util-0.1.1 (c (n "zuicon-util") (v "0.1.1") (h "1vfrg2yl54z3r577winyl16624ciimxqdrb24y8v4whbzc6phhjj")))

(define-public crate-zuicon-util-0.2.1 (c (n "zuicon-util") (v "0.2.1") (d (list (d (n "roxmltree") (r "^0.18.0") (d #t) (k 0)))) (h "03n2hmigk8fw1583579is86dbzpzc1p9zls5z2d9f1zzrpg5i071")))

(define-public crate-zuicon-util-0.2.2 (c (n "zuicon-util") (v "0.2.2") (d (list (d (n "roxmltree") (r "^0.18.0") (d #t) (k 0)))) (h "1f584ra8wwz5js9ckma9y6dsf3wf4x1wmphqfblk47k099fcq8g6")))

