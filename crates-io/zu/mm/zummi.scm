(define-module (crates-io zu mm zummi) #:use-module (crates-io))

(define-public crate-zummi-0.1.0 (c (n "zummi") (v "0.1.0") (h "0ax7fyq9gbgnf364zqsfb1l5y3dxnsc4c5wmmnyxg8plr9ih016x")))

(define-public crate-zummi-0.1.1 (c (n "zummi") (v "0.1.1") (h "1z1y5d51dd62rjgc95a9gfsrllj2laybkxpmrmybgsvv98nzh1rn")))

(define-public crate-zummi-0.1.2 (c (n "zummi") (v "0.1.2") (h "1n7nzkc74gc70vaby43yd621sy97pqy50kpx4dyxbazisr5gdv5c") (f (quote (("nightly"))))))

