(define-module (crates-io zu ni zunivers-rs) #:use-module (crates-io))

(define-public crate-zunivers-rs-0.1.0 (c (n "zunivers-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.33") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "07xd2iblswxg32iayf6p5shnash4zyakxn08pdlvclw9l3ypvnsg")))

(define-public crate-zunivers-rs-1.0.0 (c (n "zunivers-rs") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.33") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "00v84kmhbvcx2isxdgaz4xgh4cx18y7rjyid7pflghnvn622mn9m")))

(define-public crate-zunivers-rs-1.0.1 (c (n "zunivers-rs") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.33") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ln6001cajnd0n52rp483zgrrv1msacbiyfxwlvlrb6jiphy73hs")))

(define-public crate-zunivers-rs-1.0.2 (c (n "zunivers-rs") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.33") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0kmcm42n961xpnzsa70fcyhgs432drp6scyvwzdwrm1hwhf6kcys")))

