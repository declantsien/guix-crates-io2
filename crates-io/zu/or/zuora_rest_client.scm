(define-module (crates-io zu or zuora_rest_client) #:use-module (crates-io))

(define-public crate-zuora_rest_client-0.1.0 (c (n "zuora_rest_client") (v "0.1.0") (d (list (d (n "mockito") (r "^0.30") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1myzkw25jighigjavd9mn8q1pg0fa7ri4pyiksi02hwpr5d9xx4v")))

(define-public crate-zuora_rest_client-0.1.1 (c (n "zuora_rest_client") (v "0.1.1") (d (list (d (n "mockito") (r "^0.30") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xahrrpxakqs5k5j7xb6yyrw0v98hq5ji1jyvxa8mjv236j3yii4")))

(define-public crate-zuora_rest_client-0.1.2 (c (n "zuora_rest_client") (v "0.1.2") (d (list (d (n "mockito") (r "^0.30") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "05yc35c54vm56xwx2gi5a8jqr6n5f658qlplqa433j9n8q8k82ca")))

(define-public crate-zuora_rest_client-0.1.3 (c (n "zuora_rest_client") (v "0.1.3") (d (list (d (n "mockito") (r "^0.30") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1xb3jrnpjixbkgm5f7arjxvwz1p0ssg5x6ilhydwzd42alvkqlv2")))

(define-public crate-zuora_rest_client-0.1.4 (c (n "zuora_rest_client") (v "0.1.4") (d (list (d (n "mockito") (r "^0.30") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1wgs5jg8awzn7923gf072chj62p1hp1w00f5q41v44qvpbwlvdnr")))

(define-public crate-zuora_rest_client-0.1.5 (c (n "zuora_rest_client") (v "0.1.5") (d (list (d (n "mockito") (r "^0.30") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "12cfvwi663aa380w5bwdl4228xrhvq0sqr0z5k7vnm15pd8h80z5")))

