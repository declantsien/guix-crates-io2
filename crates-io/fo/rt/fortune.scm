(define-module (crates-io fo rt fortune) #:use-module (crates-io))

(define-public crate-fortune-0.1.0 (c (n "fortune") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1vvjfc2zv7q4fm7ysqrlvka0jxzkr6mp3y1kn2f7j9qcrpkkawvk")))

(define-public crate-fortune-0.2.0 (c (n "fortune") (v "0.2.0") (h "0w1lrvjhi1fkpwzncbvs2gpbblpinlba0qsz4ywcmbg8k60lpq2c")))

