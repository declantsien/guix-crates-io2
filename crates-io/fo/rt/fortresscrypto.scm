(define-module (crates-io fo rt fortresscrypto) #:use-module (crates-io))

(define-public crate-fortresscrypto-1.0.1 (c (n "fortresscrypto") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1dpcmi5q6m4ra05yzz880nzx2md57ym37wy1vwbfycgx12qnwwkp")))

(define-public crate-fortresscrypto-2.0.0 (c (n "fortresscrypto") (v "2.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chacha20") (r "^0.9.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "password-hash") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("std"))) (d #t) (k 0)) (d (n "scrypt") (r "^0.10.0") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "subtle") (r "^2.4.1") (d #t) (k 0)))) (h "1f3246d6pflyhxdybi7wjqxx5glhgblnnz0k6hglzr6kcz4zfkm1")))

