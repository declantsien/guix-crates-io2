(define-module (crates-io fo rt forth) #:use-module (crates-io))

(define-public crate-forth-0.1.0 (c (n "forth") (v "0.1.0") (h "06q1inpdqhqbf7pdmpv7q7q4bxs59x0i9g5xvr7iqxli094z4vcd")))

(define-public crate-forth-0.1.1 (c (n "forth") (v "0.1.1") (h "07cv8hdsg7yc2dbzjy55qj832b921vi0236i47fribhjhknkmypg")))

