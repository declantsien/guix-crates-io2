(define-module (crates-io fo rt fort) #:use-module (crates-io))

(define-public crate-fort-0.1.0 (c (n "fort") (v "0.1.0") (d (list (d (n "bastion") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.5") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1vgqil3f9298ji8zqlf3nr3bvp8qbp4r3bmmczv7qacmbz9sxzy8")))

(define-public crate-fort-0.1.1 (c (n "fort") (v "0.1.1") (d (list (d (n "bastion") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.5") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "109smk97r74g22k83hnmmkz73jmrvc2ynjsmdz9g9mzpisdmakfv")))

(define-public crate-fort-0.2.0 (c (n "fort") (v "0.2.0") (d (list (d (n "bastion") (r "^0.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.5") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0mf3gazpwzh9wi99pdcf24fwpimk2fzss0hnibn26988jrdajva0")))

(define-public crate-fort-0.2.1 (c (n "fort") (v "0.2.1") (d (list (d (n "bastion") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.5") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0h525x76zvmmk0nr6wak38zwlbhvw5ip8jvnhf65mxbhqqp678fn")))

(define-public crate-fort-0.3.0 (c (n "fort") (v "0.3.0") (d (list (d (n "bastion") (r "^0.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.5") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "01sbc70926x9av0xn13yj80ba1jrigf3g7jmrb96hsdm8r6d89cg")))

(define-public crate-fort-0.4.0 (c (n "fort") (v "0.4.0") (d (list (d (n "bastion") (r "^0.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0b9yrvicz3c0mdzimsk4ym1yxyg77j7liqvz2h0za80x03a1w9sk")))

