(define-module (crates-io fo rt fortunelike) #:use-module (crates-io))

(define-public crate-fortunelike-0.2.0 (c (n "fortunelike") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.3") (d #t) (k 0)))) (h "09l170jyl5q8qc3jmmrza1wp4g5i71cwbmv5bpq3b1szjj8fx61b")))

