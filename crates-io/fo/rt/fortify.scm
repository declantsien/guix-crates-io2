(define-module (crates-io fo rt fortify) #:use-module (crates-io))

(define-public crate-fortify-0.1.0 (c (n "fortify") (v "0.1.0") (d (list (d (n "fortify_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1lfafk1ybn7z2yvil60md2kxnf2905snwiwszk8vsnbwrkf8akkc")))

(define-public crate-fortify-0.2.0 (c (n "fortify") (v "0.2.0") (d (list (d (n "fortify_derive") (r "^0.1.0") (d #t) (k 0)))) (h "028y2g323zfiwy7iah3k2jj92mwzpf51z9y3iq764xg78fysaycv")))

(define-public crate-fortify-0.2.1 (c (n "fortify") (v "0.2.1") (d (list (d (n "fortify_derive") (r "^0.1.0") (d #t) (k 0)))) (h "17vzhm4issg56c588kwb5bzcnrnb2067fmifc4gak8269lpih1dy")))

(define-public crate-fortify-0.3.0 (c (n "fortify") (v "0.3.0") (d (list (d (n "fortify_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1ixzqb9x2ri22f96blcli33q8v2c02vxwlb4lpk7i6w02jy5dkym") (y #t)))

(define-public crate-fortify-0.3.1 (c (n "fortify") (v "0.3.1") (d (list (d (n "fortify_derive") (r "^0.2.0") (d #t) (k 0)))) (h "1b27icsn34ym29i5sdm2m2czhssydidcn6nlplvckgzpjbh9h41h")))

(define-public crate-fortify-0.4.0 (c (n "fortify") (v "0.4.0") (d (list (d (n "fortify_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (t "cfg(not(miri))") (k 2)))) (h "1pq0p3mwkahfhwwn58za5rsld7zs8yjy6j8wms4kk8mamgxpy0q9")))

(define-public crate-fortify-0.4.1 (c (n "fortify") (v "0.4.1") (d (list (d (n "fortify_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (t "cfg(not(miri))") (k 2)))) (h "0pf4prcn77zihf544i4hkh4g48fy6ig3f9qiffqni71dp39q5z89")))

