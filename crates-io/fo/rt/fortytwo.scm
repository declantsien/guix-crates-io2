(define-module (crates-io fo rt fortytwo) #:use-module (crates-io))

(define-public crate-fortytwo-0.0.1 (c (n "fortytwo") (v "0.0.1") (h "0df35sawyhh5xxa46ipm3lrmwkhmzm358xc5iididvmd9k203p39")))

(define-public crate-fortytwo-42.0.0 (c (n "fortytwo") (v "42.0.0") (d (list (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0gz2qkxahmhhgr0kikwirpazfknyvva5qc64a89hqhamsmrld9sr")))

