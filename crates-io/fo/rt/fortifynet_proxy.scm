(define-module (crates-io fo rt fortifynet_proxy) #:use-module (crates-io))

(define-public crate-fortifynet_proxy-0.1.0 (c (n "fortifynet_proxy") (v "0.1.0") (h "0yv3wng34lshm7rxlgx1g4cdkc7hk0dx1473jknh557azh4chvy8") (y #t)))

(define-public crate-fortifynet_proxy-0.1.2 (c (n "fortifynet_proxy") (v "0.1.2") (h "1alqvssh1dcbjrj56ngn29pv8k672j01njai2q699s3fj7zmgarv") (y #t)))

(define-public crate-fortifynet_proxy-0.1.3 (c (n "fortifynet_proxy") (v "0.1.3") (h "1ac3m9b5bkwaqcx0gm9n3kr01bygxfvpcc0xg3n0c63pakb9lm20") (y #t)))

(define-public crate-fortifynet_proxy-0.1.4 (c (n "fortifynet_proxy") (v "0.1.4") (h "0cvw6qja54sq0pmvcwkb1kz3ykkj2f2f9k8mky7g2vkz8nx35nlh")))

(define-public crate-fortifynet_proxy-0.1.5 (c (n "fortifynet_proxy") (v "0.1.5") (h "01cdjsyq0r6c6ls5a9pnq9nrpnwgy0g61sds3vp8lzccgq9hd5wc")))

(define-public crate-fortifynet_proxy-1.1.5 (c (n "fortifynet_proxy") (v "1.1.5") (h "03lqmpmkhmh8705wrad7dgwd1s7x2diaayqbnzdfix10f8ik5njk")))

(define-public crate-fortifynet_proxy-1.1.6 (c (n "fortifynet_proxy") (v "1.1.6") (d (list (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "0yz20cscjsvbsmz3h7w7w2d5n51gx3id7sy4my0psdxfyi7q0wjq")))

(define-public crate-fortifynet_proxy-1.1.7 (c (n "fortifynet_proxy") (v "1.1.7") (d (list (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1kfj0zjl2qnmg683zy9myzh8srz91gx9ffswr8hqns6dki298vyj")))

(define-public crate-fortifynet_proxy-1.1.8 (c (n "fortifynet_proxy") (v "1.1.8") (d (list (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "1bp49bgzn7vrphanknh01pwljjgdqg6bxsb4qmziw4pk3q877wmh")))

(define-public crate-fortifynet_proxy-1.1.9 (c (n "fortifynet_proxy") (v "1.1.9") (h "02cls1hf6jnij6vb7anbzym2mvhf5jwwrj7n95bagajmhafdjzx1")))

