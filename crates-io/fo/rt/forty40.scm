(define-module (crates-io fo rt forty40) #:use-module (crates-io))

(define-public crate-forty40-0.1.0 (c (n "forty40") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0x6ziy4lk87r70dprhicwqc7yj3b457nfg24f9bjhcihiyzw7kgx")))

(define-public crate-forty40-0.1.1 (c (n "forty40") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1dxjdqbnm3q13nfc8zpzwshmsgzacy8h2pmbh04j8khm90qkf3j1")))

(define-public crate-forty40-0.1.2 (c (n "forty40") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "16m6hb06qji97l0sgq3r89q8lk7295vg2j4wpyhf8p321v54q0mh")))

