(define-module (crates-io fo rt fortility) #:use-module (crates-io))

(define-public crate-Fortility-0.0.1 (c (n "Fortility") (v "0.0.1") (d (list (d (n "glfw") (r "^0.29.0") (d #t) (k 0)))) (h "1h7cwqbcc3x1km6mrqz6n957vv12fbg94cg9hcrs3f14vq5cis6w") (y #t)))

(define-public crate-Fortility-0.0.2 (c (n "Fortility") (v "0.0.2") (d (list (d (n "glfw") (r "^0.29.0") (d #t) (k 0)))) (h "088rn9yqi9amlrrhkcmq0j2xwi6ka7b3xhmvavvndfg9mc54g9vy") (y #t)))

(define-public crate-Fortility-0.0.3 (c (n "Fortility") (v "0.0.3") (d (list (d (n "glfw") (r "^0.29.0") (d #t) (k 0)))) (h "0khxw28qmzxc2hz9213df4jfirwmddz0752yp21rrqydqn99wcsg") (y #t)))

