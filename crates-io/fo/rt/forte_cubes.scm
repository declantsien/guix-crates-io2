(define-module (crates-io fo rt forte_cubes) #:use-module (crates-io))

(define-public crate-forte_cubes-0.1.0 (c (n "forte_cubes") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cgmath") (r "^0.18") (d #t) (k 0)) (d (n "forte_engine") (r "^0.1.1") (d #t) (k 0)) (d (n "include-wgsl-oil") (r "^0.2.4") (d #t) (k 0)) (d (n "pollster") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "wgpu") (r "^0.18") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 2)))) (h "170agh74b0vykssn66fxv8q7riyjyk1fdr4802ydh5s90www4w1q")))

