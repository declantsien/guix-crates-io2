(define-module (crates-io fo rt fortify_derive) #:use-module (crates-io))

(define-public crate-fortify_derive-0.1.0 (c (n "fortify_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0b3vm128dzxh56zckvddnf59vi9vrmgrigz4ybydfnvldk2v3r4l")))

(define-public crate-fortify_derive-0.2.0 (c (n "fortify_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f73vxc469sr3a9144mwlha8csi4appq1m7gdphxaz2d29332ci9")))

