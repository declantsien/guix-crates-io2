(define-module (crates-io fo rt fortuples) #:use-module (crates-io))

(define-public crate-fortuples-0.1.0 (c (n "fortuples") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "0ijyzpq1bm2ifk69mswbp5mk499zx9my2c52z581lins0a49f9kh")))

(define-public crate-fortuples-0.1.1 (c (n "fortuples") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "1dkf1wdyvd83ap796nrcavv17ss6nv2kia5ng5h70gjynjd0awp6")))

(define-public crate-fortuples-0.1.2 (c (n "fortuples") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)))) (h "00n9qfma0xqcsd75c8fhxy5vi5ymmk4rbjwx6d3b4l1rs4a1kysn")))

(define-public crate-fortuples-0.2.0 (c (n "fortuples") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.71") (d #t) (k 2)))) (h "0l1ffnjdrhj0mbk45x5hgw104393y4ywnkprdi1pp6xjz2flbjyx") (f (quote (("debug"))))))

(define-public crate-fortuples-0.9.0 (c (n "fortuples") (v "0.9.0") (d (list (d (n "impl-trait-for-tuples") (r "^0.2.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.71") (d #t) (k 2)))) (h "19lwgi1y49g01dc62p26p8v63r0r3ffz0yv6by5mffzl4cm7xm4d") (f (quote (("debug"))))))

(define-public crate-fortuples-0.9.1 (c (n "fortuples") (v "0.9.1") (d (list (d (n "impl-trait-for-tuples") (r "^0.2.2") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.71") (d #t) (k 2)))) (h "1kpam2ipawqhypc1fsvvnl993jnxa3ifavpvxnvw9jp9hy00lqw7") (f (quote (("debug"))))))

