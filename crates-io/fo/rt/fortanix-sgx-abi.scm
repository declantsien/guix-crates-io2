(define-module (crates-io fo rt fortanix-sgx-abi) #:use-module (crates-io))

(define-public crate-fortanix-sgx-abi-0.1.0-rc1 (c (n "fortanix-sgx-abi") (v "0.1.0-rc1") (h "16plrbjs88k05apgfldqn9qv2yip3p5w64gnldrq42554cpjagzq") (f (quote (("docs"))))))

(define-public crate-fortanix-sgx-abi-0.3.0 (c (n "fortanix-sgx-abi") (v "0.3.0") (h "05z7fbxrs6yly540dlav1zf5v2yc0jk4mkxgifp0dyba9fmjb74s") (f (quote (("docs"))))))

(define-public crate-fortanix-sgx-abi-0.3.1 (c (n "fortanix-sgx-abi") (v "0.3.1") (d (list (d (n "compiler_builtins") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "0kgf1idd15ah6x9mah29x5hmfvm4r55baxhkvccs7xy3nhh5w416") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins/rustc-dep-of-std") ("docs"))))))

(define-public crate-fortanix-sgx-abi-0.3.2 (c (n "fortanix-sgx-abi") (v "0.3.2") (d (list (d (n "compiler_builtins") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "0l653cfbpyjk9y8wpx3xpyknqw64kcgh96lr3av7vkvjx3jvx31z") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins/rustc-dep-of-std") ("docs"))))))

(define-public crate-fortanix-sgx-abi-0.3.3 (c (n "fortanix-sgx-abi") (v "0.3.3") (d (list (d (n "compiler_builtins") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "19kczgs9r5s8wak31lcxjqlmkp080qkqgbjp5ndqcqk0z0p44v65") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins/rustc-dep-of-std") ("docs"))))))

(define-public crate-fortanix-sgx-abi-0.4.0 (c (n "fortanix-sgx-abi") (v "0.4.0") (d (list (d (n "compiler_builtins") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "08vm7vg26lw7jj76fc8g78pigyjyr90q2nifn8kj7fpvg20cfkas") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins/rustc-dep-of-std") ("docs"))))))

(define-public crate-fortanix-sgx-abi-0.4.1 (c (n "fortanix-sgx-abi") (v "0.4.1") (d (list (d (n "compiler_builtins") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "0a8k5ann9pkhbnvidrmmlpbrm63bxsi1akyzgvfqfp5xafykhsl1") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins/rustc-dep-of-std") ("docs"))))))

(define-public crate-fortanix-sgx-abi-0.5.0 (c (n "fortanix-sgx-abi") (v "0.5.0") (d (list (d (n "compiler_builtins") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "1ic90dx083l8a06hk5vmr87njzp17j82asqp9wisn3y1fhigrjjp") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins/rustc-dep-of-std") ("docs"))))))

