(define-module (crates-io fo rt fortress) #:use-module (crates-io))

(define-public crate-fortress-1.0.1 (c (n "fortress") (v "1.0.1") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "data-encoding") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "directories") (r "^0.10") (d #t) (k 0)) (d (n "gtk") (r "^0.1.2") (f (quote ("v3_10"))) (d #t) (k 0)) (d (n "libfortress") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gh3x6g4rvb21r00616zsnp09adw3bbg4432dkfzz4h2j5ifcl25")))

