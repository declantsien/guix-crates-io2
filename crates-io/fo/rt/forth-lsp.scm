(define-module (crates-io fo rt forth-lsp) #:use-module (crates-io))

(define-public crate-forth-lsp-0.1.0 (c (n "forth-lsp") (v "0.1.0") (d (list (d (n "lsp-server") (r "^0.7.0") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94.0") (d #t) (k 0)) (d (n "ropey") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "03dq4rcg7wr85q7yjpl5fy1dfqw3y2aysrh3j49xr3qj1ipfhjfa")))

(define-public crate-forth-lsp-0.1.1 (c (n "forth-lsp") (v "0.1.1") (d (list (d (n "lsp-server") (r "^0.7.0") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94.0") (d #t) (k 0)) (d (n "ropey") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "1bzsrv4jf1bdp0vj7gx34b1sc6wi26ay1scsv41dj8zbyyjmxpf4")))

(define-public crate-forth-lsp-0.2.0 (c (n "forth-lsp") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "forth-lexer") (r "^0.1.0") (f (quote ("ropey"))) (d #t) (k 0)) (d (n "lsp-server") (r "^0.7.0") (d #t) (k 0)) (d (n "lsp-types") (r "^0.94.0") (d #t) (k 0)) (d (n "ropey") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)))) (h "1sgrqnqy574sw3qqfd5vgkm4m0rgwxi4n36hig2s6i533kmd21y2")))

