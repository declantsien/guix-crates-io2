(define-module (crates-io fo rt fortnite) #:use-module (crates-io))

(define-public crate-fortnite-0.1.0 (c (n "fortnite") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "14c0s44p6zakx28x71ca5fwwa7fcdardpg7yi3mknaji1qcrz147")))

(define-public crate-fortnite-0.1.1 (c (n "fortnite") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0i7fqdwi27xp0sc5w23g6q9nf507rfrsbp8sm5mskyr1s091pmfp")))

(define-public crate-fortnite-0.1.2 (c (n "fortnite") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "19phpakn8jv1zwp91875lx2sbfisibyx0cxmyx54qsvnlin4hwxb")))

(define-public crate-fortnite-0.1.3 (c (n "fortnite") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0yi1xnx7gclb0yhzw3p9xl1sgrh3pvfgf9f43qjppkpkywn4acwf")))

(define-public crate-fortnite-0.1.4 (c (n "fortnite") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0mgwf249v99b04pqq23qgca5adsnbqxzyx0yxhpmkj65a1q4lnpl")))

(define-public crate-fortnite-0.1.6 (c (n "fortnite") (v "0.1.6") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.10") (d #t) (k 0)))) (h "0513kn7nk9r73pkj6x86v9rnv87v4q8sn6n0c26f7g22i55r7y2m")))

(define-public crate-fortnite-0.1.7 (c (n "fortnite") (v "0.1.7") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.10") (d #t) (k 0)))) (h "07pgx1akaw52cyi6g7d801xffchiys18jr16wldci6v4b8xr3dw2")))

(define-public crate-fortnite-0.1.8 (c (n "fortnite") (v "0.1.8") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.10") (d #t) (k 0)))) (h "14c7ansmmyami9073yclr80022ibi4g2a1yqv27z07mfya7lyi2j")))

