(define-module (crates-io fo rt fortraith) #:use-module (crates-io))

(define-public crate-fortraith-0.1.0 (c (n "fortraith") (v "0.1.0") (d (list (d (n "trait_eval") (r "^0") (d #t) (k 0)))) (h "04cw5vh7gvws2c68600ysl9zf2s3fiifph6b33say17dx0zn42k8")))

(define-public crate-fortraith-0.1.1 (c (n "fortraith") (v "0.1.1") (d (list (d (n "trait_eval") (r "^0") (d #t) (k 0)))) (h "122h4dn3h6a9ld1x4rwznj5jy3500p6qn1a2106gf8ya55a2l4ij")))

(define-public crate-fortraith-0.1.2 (c (n "fortraith") (v "0.1.2") (d (list (d (n "trait_eval") (r "^0") (d #t) (k 0)))) (h "0hqksys5cbv9l0rfc45kgjbjww5v5v92pmp8mg4c8gqyrqkzd9zw")))

(define-public crate-fortraith-0.1.3 (c (n "fortraith") (v "0.1.3") (d (list (d (n "trait_eval") (r "^0") (d #t) (k 0)))) (h "1k8n63npsdnyha7a5ppfag57hy27s3qv7vs5nrgki6vkydgnfnqr")))

