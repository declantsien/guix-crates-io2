(define-module (crates-io fo rt forth-lexer) #:use-module (crates-io))

(define-public crate-forth-lexer-0.1.0 (c (n "forth-lexer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "ropey") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "0h5b114csiam93q104zsk1pq4a9sfkxknr0k73jg2czbx1k7b0w5") (s 2) (e (quote (("ropey" "dep:ropey"))))))

(define-public crate-forth-lexer-0.1.1 (c (n "forth-lexer") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "ropey") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "12nl9pkma93k66gikvgk5v0jck04pxi7saqxqsg8jw8dqaa72fx9") (s 2) (e (quote (("ropey" "dep:ropey"))))))

