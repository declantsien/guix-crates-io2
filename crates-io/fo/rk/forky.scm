(define-module (crates-io fo rk forky) #:use-module (crates-io))

(define-public crate-forky-0.1.0 (c (n "forky") (v "0.1.0") (h "0hli5v27xbjjqxgcc4x76pkfp0fimclkjzf4mqfqgl96kapiqc7s")))

(define-public crate-forky-0.1.1 (c (n "forky") (v "0.1.1") (h "0qy4rwcis6kq5629rwnvgnm9vfd7b0qd96yydxpxc8s10qmk2zsn")))

(define-public crate-forky-0.1.2 (c (n "forky") (v "0.1.2") (h "1qqkpzrq2dv3l31djsapwv6wiqzaxsa9kdz8h8x4svqlngr629c1")))

(define-public crate-forky-0.1.3 (c (n "forky") (v "0.1.3") (h "0anpchdvnp4mv575c7pccg9r7jjwpwhvy50br0f7brbrcacz3kiy")))

(define-public crate-forky-0.1.4 (c (n "forky") (v "0.1.4") (h "08lcr9cn0r4q63365bnr3jmaslzkqjy6c7zcj7a4b7pj80sl5qna")))

(define-public crate-forky-0.1.5 (c (n "forky") (v "0.1.5") (h "10v520ys9xadp7dz9dj130yyhjwyjl6azrbp00fhk9ksdz2p63k9")))

(define-public crate-forky-0.1.6 (c (n "forky") (v "0.1.6") (h "1wxxr0jr6vp42ljxm5lqngplnvfqnrv41r5arsr712zzkc7kyb17")))

(define-public crate-forky-0.1.7 (c (n "forky") (v "0.1.7") (h "05223jvmap97h6a36mz8w4cs1j6h8ll0chzh45sm82n25fa7irsh")))

(define-public crate-forky-0.1.8 (c (n "forky") (v "0.1.8") (h "05xdnrqr8rdv85d6j7fiw9ypm1v5dix1vschpxkvn7m0m0paxsap")))

(define-public crate-forky-0.1.9 (c (n "forky") (v "0.1.9") (h "0cfl7ba0bcnp7ki1mh2wa46ljnz6wxbg3vyhn6v85w1s94jcfwk5")))

(define-public crate-forky-0.1.10 (c (n "forky") (v "0.1.10") (h "0vf4w2nq8qnxz42dnsgwsvqgdz84qc974ywv3l01ffsnyrc23pnh")))

(define-public crate-forky-0.1.12 (c (n "forky") (v "0.1.12") (d (list (d (n "sweet") (r "0.*") (d #t) (k 0)))) (h "1cbd49m0nbw3sjaflcbjq4z1xjm3cgqp5p704g4sfm2iglkvazii")))

(define-public crate-forky-0.1.13 (c (n "forky") (v "0.1.13") (d (list (d (n "sweet") (r "0.*") (d #t) (k 0)))) (h "0pmjh7nsjfp2ihvdyywawi2jqhsp76b7jhkswckz3wawijmjnnjp")))

(define-public crate-forky-0.1.14 (c (n "forky") (v "0.1.14") (d (list (d (n "sweet") (r "0.*") (d #t) (k 0)))) (h "1yvpvcn9yvbfxw22r2723lwynl99azcaw1pdnpxckcprn0366dsk")))

(define-public crate-forky-0.1.15 (c (n "forky") (v "0.1.15") (d (list (d (n "sweet") (r "0.*") (d #t) (k 0)))) (h "03jkx2nxm5w1q7mjfzvzvsgxdjdqy2cp9wwxvc9d7xdgwi0w1r4h")))

(define-public crate-forky-0.1.16 (c (n "forky") (v "0.1.16") (d (list (d (n "sweet") (r "0.*") (d #t) (k 0)))) (h "0r01vsbg7bx7cmx9nznnbb4yn0a6fjvz0nyf6ac9xfjx85w37ia9")))

(define-public crate-forky-0.1.17 (c (n "forky") (v "0.1.17") (d (list (d (n "sweet") (r "0.*") (d #t) (k 0)))) (h "0xs4cazxj25n17ckd7hkx44v4694gqxjqrxfmsr9x51hnfvp2404")))

(define-public crate-forky-0.1.18 (c (n "forky") (v "0.1.18") (d (list (d (n "sweet") (r "0.*") (d #t) (k 0)))) (h "0ngqdwwhzgsasrw67mzf4avr9rcy788ix41vgd6r55sxfy91gmvw")))

(define-public crate-forky-0.1.19 (c (n "forky") (v "0.1.19") (d (list (d (n "sweet") (r "0.*") (d #t) (k 0)))) (h "07bmaq9z8r1z6br2dp02x06czrh6rf8m1v3pr5v3gg9vlbhbm01m")))

(define-public crate-forky-0.1.20 (c (n "forky") (v "0.1.20") (h "16i3is2lwi2jjxf0d0pvjmx6wxiiqhgl99j4ysqxhky3mqdpshid")))

(define-public crate-forky-0.1.21 (c (n "forky") (v "0.1.21") (h "0y0nvhxk5233rdpzf9b0slvhdilr4i2cgb3h0gb9w29m8bv9vq9h")))

(define-public crate-forky-0.1.22 (c (n "forky") (v "0.1.22") (h "1ybhsavkb0xpql9z7hjw6m7agmr5rfqa93bmfrx7s2pq839q5qjk")))

(define-public crate-forky-0.1.23 (c (n "forky") (v "0.1.23") (h "0kxdsqrzia3dvyab0n9gjgpvmqhhpdlf3bkmk0nmpb1s8aib5lf2")))

(define-public crate-forky-0.1.24 (c (n "forky") (v "0.1.24") (h "0dcvbxn9vc1253acb0855yg2vb1yr8q73fgq1lbaaj1p0qwxai3m")))

(define-public crate-forky-0.1.25 (c (n "forky") (v "0.1.25") (h "1rhwbif54mxdm23m7vn7w7fnyh1injmyz07z7ibkqb9fl5lxvabf")))

(define-public crate-forky-0.1.26 (c (n "forky") (v "0.1.26") (h "1lh00g207gv5pci1c452srmgzl0mqvzmzfsy850j8zgip6qp5y59")))

(define-public crate-forky-0.1.27 (c (n "forky") (v "0.1.27") (h "1x1dql6wwi95pha1vvw6ni17hqzlhglr982svr65vlzim9jj8pb8")))

(define-public crate-forky-0.1.28 (c (n "forky") (v "0.1.28") (h "0q252vp7ac5ajzwkrdcy2gv475y94lkwil2qlvjf0rg4bgag6jdr")))

(define-public crate-forky-0.1.29 (c (n "forky") (v "0.1.29") (h "0ipb6rdxi4m0i81d9sjyf4lwkswi9xdzm3jszlk1nzmpsx08wpwg")))

(define-public crate-forky-0.1.30 (c (n "forky") (v "0.1.30") (h "1sn95w4yikk4kagm8cmpgsl2805k1lhlh4ay4n02zvin5arwyy2p")))

(define-public crate-forky-0.1.31 (c (n "forky") (v "0.1.31") (h "0x692acw898cpfq96qb7ldlijmzsjybzm94gc1gw2pfl6qrzgk9c")))

(define-public crate-forky-0.1.32 (c (n "forky") (v "0.1.32") (h "0p4aws16wabsp90yrcysa5b5xqb9rfs2jvz51yri99qb3bw7dl04")))

(define-public crate-forky-0.1.33 (c (n "forky") (v "0.1.33") (h "1qs8qlfhf81qwp83ma2fn0sw8qn7qkkn3jgmw6v6wpbadl30vc9v")))

(define-public crate-forky-0.1.34 (c (n "forky") (v "0.1.34") (h "0b631c57zd82dx7dq2ra9s1gj5mavfrr6giwd5kxqxx86119gk4h")))

(define-public crate-forky-0.1.35 (c (n "forky") (v "0.1.35") (h "11c5s34w4jn7q5y5c7z77czsrwnak87ly9wnkx8a01j6p8xapmbf")))

(define-public crate-forky-0.1.36 (c (n "forky") (v "0.1.36") (h "0mx9ldspkhlpx3ym8cqg4g6nkf1xcmwnv2admqzs9140h2hpx59z")))

(define-public crate-forky-0.1.37 (c (n "forky") (v "0.1.37") (h "0qjbdh4lca40dkga01bhvflhzm6906h16vvql7ydmzwk7z503cdw")))

(define-public crate-forky-0.1.39 (c (n "forky") (v "0.1.39") (h "1gwajpp12gk1139z6c6algbbfvwdhxdw055q91jyss093mx9v04m")))

(define-public crate-forky-0.1.40 (c (n "forky") (v "0.1.40") (h "0fngqq04r4ypsx7biqrw2bypm535ifzws2psm1srh9jqsnpwmh0x")))

(define-public crate-forky-0.1.41 (c (n "forky") (v "0.1.41") (h "165w2y7qcr72lq7j8j5zjxqhqx05nl2sgpgzpml3m1sdd60k5b55")))

(define-public crate-forky-0.1.42 (c (n "forky") (v "0.1.42") (h "1b80niph1ic7qmpj08s581r09zirsvmcr72w3m520i674p8zhzcn")))

(define-public crate-forky-0.1.43 (c (n "forky") (v "0.1.43") (h "1yzsnk1vh1iv7palfkf2spkijl2in6md001gy7sh0qyk8qymhz7v")))

(define-public crate-forky-0.1.44 (c (n "forky") (v "0.1.44") (h "00d8qdpr7q5l7f10fcijc9hi84vxbb4q4ynpdvlm68zdxx4mnh2l")))

(define-public crate-forky-0.1.45 (c (n "forky") (v "0.1.45") (h "1na1f0rv7bmsgq0w96csk5g069sd4pzqwhdayb7myqqf5z9rdmzv")))

(define-public crate-forky-0.1.46 (c (n "forky") (v "0.1.46") (h "0b0srfjbrd8965xfsij278s6fr46rxf2ycrwr89vvd93rq407jcl")))

