(define-module (crates-io fo rk fork) #:use-module (crates-io))

(define-public crate-fork-0.1.0 (c (n "fork") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "10mishrx6g0qa6vp1kjhkd64w6cz2zd4nzg6kjf92d88596mw83w")))

(define-public crate-fork-0.1.1 (c (n "fork") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "14a3hqff4qiprrbvv4ig3hbkldz121mqyn6hl59kmp7570a727rw")))

(define-public crate-fork-0.1.2 (c (n "fork") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "1ibihxiwrybh3d62c34a757hb5vgwbd11y0hgcadnx1sdgigv4m3")))

(define-public crate-fork-0.1.3 (c (n "fork") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "19n7kd8b8f1cm5pd2msjbww28a11lxvnx17m5yvhwapn1mqpqczi")))

(define-public crate-fork-0.1.4 (c (n "fork") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "162wd5grzisxy75y31wygy2sjg91jz2d6b8ygxb0qiqgl7nj3cpx")))

(define-public crate-fork-0.1.5 (c (n "fork") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "0kmzv4dg0khjc2awlgb6lnl055gvy7dxdxa84mc996hr8y7n834g")))

(define-public crate-fork-0.1.6 (c (n "fork") (v "0.1.6") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "0iqr42fak9k6w7qqb1rpp34bgz9m466bg8wvnngy1a45kj3jc7b0")))

(define-public crate-fork-0.1.7 (c (n "fork") (v "0.1.7") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "0im3f342mwhjk0zfpsjjlv77j7vsjr7rsgrndppr7rcd538xjlxl")))

(define-public crate-fork-0.1.8 (c (n "fork") (v "0.1.8") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "07dys6arpk0npqpy5p4sykw5lil4im1ckxhfdiaj9rlsknmy1vxf")))

(define-public crate-fork-0.1.9 (c (n "fork") (v "0.1.9") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "0dpkxcmap33477vfp13dqvqbvibdpp0jly916vgl7yi9fw65fx4a")))

(define-public crate-fork-0.1.10 (c (n "fork") (v "0.1.10") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "0yq9vyl80ymd7xh6nqbvfwyq8m2fj5xgh32mv24262z6v4njzfir")))

(define-public crate-fork-0.1.11 (c (n "fork") (v "0.1.11") (d (list (d (n "libc") (r "^0.2.67") (d #t) (k 0)))) (h "123lkadmj3yj6mw8wj9kkl84vhahw4zqh6rwy3ifcvqkcxw59vhn")))

(define-public crate-fork-0.1.12 (c (n "fork") (v "0.1.12") (d (list (d (n "libc") (r "^0.2.67") (d #t) (k 0)))) (h "13qq0riq5z1jjzm66bdy1rnzzqb6y3yr7fqflgisixv8w2lj5fjg")))

(define-public crate-fork-0.1.13 (c (n "fork") (v "0.1.13") (d (list (d (n "libc") (r "^0.2.68") (d #t) (k 0)))) (h "15c2dwzmx5ig2n8zgkzqx8kfidkcq614xzwq97akwy43z0jywr9i")))

(define-public crate-fork-0.1.14 (c (n "fork") (v "0.1.14") (d (list (d (n "libc") (r "^0.2.69") (d #t) (k 0)))) (h "0lqv36df3qkmch2fylq3qwqvlf3si0yvv9wpff9dk287wyi449wy")))

(define-public crate-fork-0.1.15 (c (n "fork") (v "0.1.15") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)))) (h "149lcdvh9fiy2mk53m2hqw6b31h6f7jvc2b10lhn2m53f09qbrxb")))

(define-public crate-fork-0.1.16 (c (n "fork") (v "0.1.16") (d (list (d (n "libc") (r "^0.2.73") (d #t) (k 0)))) (h "037fmq8dng18zr6j8wc2wc0x5f5681vxvxn7wdbw1m4m7dgzcmar")))

(define-public crate-fork-0.1.17 (c (n "fork") (v "0.1.17") (d (list (d (n "libc") (r "^0.2.73") (d #t) (k 0)))) (h "14ldh35iwi2rcf2aznr7slc5p83vl5nqbl0bza2nys8hzm11pas8")))

(define-public crate-fork-0.1.18 (c (n "fork") (v "0.1.18") (d (list (d (n "libc") (r "^0.2.79") (d #t) (k 0)))) (h "1fr3fgmp8kwsf54ijrfgh6xd5l5aid741i1sz1ba8jg2pjqbkig4")))

(define-public crate-fork-0.1.19 (c (n "fork") (v "0.1.19") (d (list (d (n "libc") (r "^0.2.119") (d #t) (k 0)))) (h "05vpjcdl1va3w9kxlivswv36ljc6imjw2k1445c4jbir82kz3d2p")))

(define-public crate-fork-0.1.20 (c (n "fork") (v "0.1.20") (d (list (d (n "libc") (r "^0.2.132") (d #t) (k 0)))) (h "1jhd3yma7kbghn42vgc7cd2qgiq54g8zgls3ixp8vgzl184wx24p")))

(define-public crate-fork-0.1.21 (c (n "fork") (v "0.1.21") (d (list (d (n "libc") (r "^0.2.140") (d #t) (k 0)))) (h "0mh0gxf944lb8gsmsdm4m5z78hmk12igzv2pa5siziays80v1x7a")))

(define-public crate-fork-0.1.22 (c (n "fork") (v "0.1.22") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "124fbfwqka5xh2jndmlpgbl2ya7a9yf7q6adxvkja510b5xajb5z")))

(define-public crate-fork-0.1.23 (c (n "fork") (v "0.1.23") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vd8fv09zzxpn67iizndsl24msvrzd995r06v7lmg2lr4cs4vrv0")))

