(define-module (crates-io fo rk forktree) #:use-module (crates-io))

(define-public crate-forktree-0.0.0 (c (n "forktree") (v "0.0.0") (h "1w4ys4277dxbd0hs5flgcpnvmmjcv71am4ciqqjp8p47xrr93py6") (y #t)))

(define-public crate-forktree-2.0.1 (c (n "forktree") (v "2.0.1") (d (list (d (n "codec") (r "^2.0.0") (f (quote ("derive"))) (d #t) (k 0) (p "tetsy-scale-codec")))) (h "1ds817qs550cnhv8889c73753fx1a7g301shlf1a71k0icfhhn7n")))

(define-public crate-forktree-2.1.2 (c (n "forktree") (v "2.1.2") (d (list (d (n "codec") (r "^2.0.1") (f (quote ("derive"))) (d #t) (k 0) (p "tetsy-scale-codec")))) (h "16f4rpdw42n1nkbkfarfd4z2rj1fyfp1yzdgcs8rqpr8iic4av7l")))

