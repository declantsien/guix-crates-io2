(define-module (crates-io fo rk forkable) #:use-module (crates-io))

(define-public crate-forkable-0.1.0 (c (n "forkable") (v "0.1.0") (d (list (d (n "circularing") (r "^2.2.1") (d #t) (k 0)) (d (n "likeness") (r "^1.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)) (d (n "snafu") (r "^0.8.2") (k 0)) (d (n "spin") (r "^0.9.8") (f (quote ("mutex"))) (d #t) (k 0)))) (h "0pj1g6yyc0a69lqkp3ydd4d4br70a0yfjqc067l1rnysy351wy5m") (f (quote (("std") ("default" "std"))))))

