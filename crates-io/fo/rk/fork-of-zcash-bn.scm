(define-module (crates-io fo rk fork-of-zcash-bn) #:use-module (crates-io))

(define-public crate-fork-of-zcash-bn-0.1.0 (c (n "fork-of-zcash-bn") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0") (f (quote ("i128"))) (k 0)) (d (n "crunchy") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (k 0)) (d (n "rand") (r "^0.8.3") (f (quote ("std_rng"))) (d #t) (k 2)) (d (n "rustc-hex") (r "^2") (k 0)))) (h "0xrjckkfvc28j0hbrqhl7wh5s2xdx30hp46bbr3y4nr99qlhik5g") (f (quote (("default"))))))

