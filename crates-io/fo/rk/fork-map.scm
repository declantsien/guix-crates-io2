(define-module (crates-io fo rk fork-map) #:use-module (crates-io))

(define-public crate-fork-map-0.1.0 (c (n "fork-map") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-error") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03zz0rdgsihwbwgnkca19ykhrmlw4ilhhmngwfw6mswqy7yxpfql")))

(define-public crate-fork-map-0.1.1 (c (n "fork-map") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-error") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1b5lqs62785zw0av9ssdxxrbs829waa2w5apmspysgni1mq28irg")))

(define-public crate-fork-map-0.1.2 (c (n "fork-map") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-error") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "117z0wkqg3phlmfh9j1gnhldaplhksrf9fcfbspkcr08pfgpxjk0")))

(define-public crate-fork-map-0.1.3 (c (n "fork-map") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-error") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0w40dwypy10z8rcfvan6ggfwg8ck4h5zzq81qb51xnw49r66222w")))

