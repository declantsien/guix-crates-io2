(define-module (crates-io fo rk forky-udpflow) #:use-module (crates-io))

(define-public crate-forky-udpflow-0.1.0 (c (n "forky-udpflow") (v "0.1.0") (d (list (d (n "socket2") (r "^0.5.3") (f (quote ("all"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "time" "sync" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "07xgnhirnyn1mp2f53wknw39mg645ivazk9dcvnysl7ipvsbkz2l")))

(define-public crate-forky-udpflow-0.2.0 (c (n "forky-udpflow") (v "0.2.0") (d (list (d (n "socket2") (r "^0.5.3") (f (quote ("all"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "net" "time" "sync" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1mhsabybfyxqw8nh5k0nz2jxgvzgsmm3j3scgmy0n9vk4wz5rpvf")))

