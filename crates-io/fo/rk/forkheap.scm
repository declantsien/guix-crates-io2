(define-module (crates-io fo rk forkheap) #:use-module (crates-io))

(define-public crate-forkheap-0.1.0 (c (n "forkheap") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.10") (f (quote ("alloc_ref"))) (k 0)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)) (d (n "sysinfo") (r "^0.30") (k 0)))) (h "14xgbmcrl4b9ily160bz1sv6d180axlv85ik2ds6lp3yr82qa3lc")))

