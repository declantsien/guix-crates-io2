(define-module (crates-io fo rk forky_test) #:use-module (crates-io))

(define-public crate-forky_test-0.1.1 (c (n "forky_test") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.3.66") (d #t) (k 0)) (d (n "colorize") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "forky_core") (r "^0.1.1") (d #t) (k 0)) (d (n "gag") (r "^1.0.0") (d #t) (k 0)) (d (n "inventory") (r "^0.3.2") (d #t) (k 0)))) (h "00djkbxk4253806pc7gbvngnxncnmgmdkvbxn2rprlg0nlvck0ac")))

(define-public crate-forky_test-0.1.2 (c (n "forky_test") (v "0.1.2") (d (list (d (n "backtrace") (r "^0.3.66") (d #t) (k 0)) (d (n "colorize") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "forky_core") (r "^0.1.2") (d #t) (k 0)) (d (n "gag") (r "^1.0.0") (d #t) (k 0)) (d (n "inventory") (r "^0.3.2") (d #t) (k 0)))) (h "0y89i05dlhd40h7zn75m4szfgsq3avgsmq5rjvfz6zd83qshj5by")))

(define-public crate-forky_test-0.1.8 (c (n "forky_test") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1vgcb62b9i9mxjqpnn9i5d2hl2a3inylpb0ql2y29rndwbz0w06d")))

(define-public crate-forky_test-0.1.9 (c (n "forky_test") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "02dnyp31ckgmpjrwlvk1rkdmsra6mv10l9qzx74g180v38g8vix7")))

(define-public crate-forky_test-0.1.10 (c (n "forky_test") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1v0zz2ciazs02qjjdh8bax9w1px42zrggd1r1lqxjggkqj0ridy5")))

(define-public crate-forky_test-0.1.12 (c (n "forky_test") (v "0.1.12") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0bm109p3hfpqg98dwzqmzq6d0pd45a8xjib1056r2d6am2a490lq")))

(define-public crate-forky_test-0.1.13 (c (n "forky_test") (v "0.1.13") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "03rvmwhbpwii5j665g2rx8pyigcigp68pc87cj2gj4lmf0z81bww")))

(define-public crate-forky_test-0.1.14 (c (n "forky_test") (v "0.1.14") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1sxyj2yz7zh0v9vq5j2qlcqn5r1h3kgipr0ncz9isxwahvaj9iga")))

(define-public crate-forky_test-0.1.15 (c (n "forky_test") (v "0.1.15") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1jaxc8235c0q5s0vcr1h9lwkxjmld4aiygifvki5q0f3fgrcqik6")))

(define-public crate-forky_test-0.1.16 (c (n "forky_test") (v "0.1.16") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "08si8i0avxbzwfnvigi034xf7jp619bwwyq3brdm246b9zldyanb")))

(define-public crate-forky_test-0.1.17 (c (n "forky_test") (v "0.1.17") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1ybvkff9cgcz1psd3hbyyqg52xlzxlsqlqs4dd2f45nh7dcpr3cp")))

(define-public crate-forky_test-0.1.18 (c (n "forky_test") (v "0.1.18") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0pgr1g04gr1ss8mwqnwv2sfmgd039wgcz8fihb4mg0ixvach4a0b")))

(define-public crate-forky_test-0.1.19 (c (n "forky_test") (v "0.1.19") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0q6qljkhw5d3sg8k2gw23mm1fyj89833gbxgrppq79wzispcx5ms")))

(define-public crate-forky_test-0.1.20 (c (n "forky_test") (v "0.1.20") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0f2y7nr0ccjzz42psw36rrij24sfa9j7fvql6bv5ixrv9336spvh")))

(define-public crate-forky_test-0.1.21 (c (n "forky_test") (v "0.1.21") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0cxsqr2gadjw7f1m25ks6gnydjb043f2b4yh733snfmdkmw4z6fy")))

(define-public crate-forky_test-0.1.22 (c (n "forky_test") (v "0.1.22") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1afjxi5zn9lc6f11l77m04v5wrflm2xd43h0pf3nlsszrnqw03w5")))

(define-public crate-forky_test-0.1.24 (c (n "forky_test") (v "0.1.24") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1ann9v1nyx1q67cc9kslwvmngqgdgikvg3a6160c6wib50in4l36")))

(define-public crate-forky_test-0.1.26 (c (n "forky_test") (v "0.1.26") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1wjrd0b81kqzd9af7ilcc1aycjmcsgallc93c2wjg8r34dj4p7k9")))

(define-public crate-forky_test-0.1.27 (c (n "forky_test") (v "0.1.27") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1yf6vc67nbgb0g36p78bmlf1qj07gqh5slgm14lay9lnmhkfkamz")))

(define-public crate-forky_test-0.1.28 (c (n "forky_test") (v "0.1.28") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1dsh6mjs4zzh8x3kl3qwcicxsjapxiq2hkzj3zbxa8v79p9gkwnr")))

(define-public crate-forky_test-0.1.29 (c (n "forky_test") (v "0.1.29") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0ybxpm33a5vrm4yh1j2rjn20479n8y5f838ymiiplrnvavaffagz")))

(define-public crate-forky_test-0.1.30 (c (n "forky_test") (v "0.1.30") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0xbks9g7l4pgdxyq56i9pzj38vvr761a5icxm78vnfmfykrma4ii")))

(define-public crate-forky_test-0.1.31 (c (n "forky_test") (v "0.1.31") (h "1h5m3d73qyijgg9djymf5x57ca5w4xrwx49s1ppjfifi4l4rz6xz")))

(define-public crate-forky_test-0.1.32 (c (n "forky_test") (v "0.1.32") (h "0hxd1v572argxgc5p5c1hslfh8wqbd8gb0d9623ivkffwwzlva86")))

(define-public crate-forky_test-0.1.33 (c (n "forky_test") (v "0.1.33") (h "1y9h33hwk4ab2pbjmp56hfddn8sirw4d67s9iy8idmynbkmknwic")))

(define-public crate-forky_test-0.1.34 (c (n "forky_test") (v "0.1.34") (h "0gn1gscgvbydljpgphv438nrcw0974fajh7vxgasrp9in86l353r")))

(define-public crate-forky_test-0.1.35 (c (n "forky_test") (v "0.1.35") (h "013yzg84wff6ipxrsr6zyn5v0xgpq17hkzmc1d2fsw0679189s5d")))

(define-public crate-forky_test-0.1.36 (c (n "forky_test") (v "0.1.36") (h "1ljydr92mdjlysayb2in54c6vrm8y539rzawg7p04q6c0djbrs5r")))

(define-public crate-forky_test-0.1.37 (c (n "forky_test") (v "0.1.37") (h "0vn1iz5w1yfqq2kkl1kbh0pcp16ni731lz94wkz3qw75jfrrkabv")))

(define-public crate-forky_test-0.1.38 (c (n "forky_test") (v "0.1.38") (h "07qw3dxbqrqd4ycz6201hpdm5h1yn6dqx3sm4zd055sb1zy4gh20")))

(define-public crate-forky_test-0.1.39 (c (n "forky_test") (v "0.1.39") (h "1q7hlin61fkgnqxv0bd46fbz234g1as16x3piv9x8vwkq866imvf")))

(define-public crate-forky_test-0.1.40 (c (n "forky_test") (v "0.1.40") (h "0n6ymbbp32fhn8x8diqwrzp2h79lijr7h3k8ad0zm46n18dci672")))

(define-public crate-forky_test-0.1.41 (c (n "forky_test") (v "0.1.41") (h "1iax7kw925n3m0d766cpbj32v3vbx6ri0zify61vmwv0fbw3yinp")))

(define-public crate-forky_test-0.1.42 (c (n "forky_test") (v "0.1.42") (h "0jzi9k7341p553jndbkvwndp9manihlqb9z9wxmxj8gg1gih3fik")))

(define-public crate-forky_test-0.1.43 (c (n "forky_test") (v "0.1.43") (h "1x2bv2da6pl6cxb4n0ia6xf2lxfmz97dv4a6nw22h09l6svxw2ay")))

(define-public crate-forky_test-0.1.44 (c (n "forky_test") (v "0.1.44") (h "0x3a2a016mjhlyd8nn75mrnfc6lxv0jpd766b6pqi9bkxcbl0hk5")))

(define-public crate-forky_test-0.1.45 (c (n "forky_test") (v "0.1.45") (h "12jfyfdpgmzi63v45skzayicgl2aik354i8rg8rfvycb9mr1cj9p")))

(define-public crate-forky_test-0.1.46 (c (n "forky_test") (v "0.1.46") (h "0d6fnfjr6139a9b1x5f6jndyj9hzl8wimx370anxj9lmjvmyzx91")))

