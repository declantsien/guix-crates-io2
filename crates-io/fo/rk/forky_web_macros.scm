(define-module (crates-io fo rk forky_web_macros) #:use-module (crates-io))

(define-public crate-forky_web_macros-0.1.42 (c (n "forky_web_macros") (v "0.1.42") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "01vyqrxscvvpai59mkb7kpkp3yfdvv5bvcklcbwgpyx7da63y80d")))

(define-public crate-forky_web_macros-0.1.43 (c (n "forky_web_macros") (v "0.1.43") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1cb9bmmib66ky5mxnlvnfbdg4iznbjwsfh304fblq667nfq825xl")))

(define-public crate-forky_web_macros-0.1.44 (c (n "forky_web_macros") (v "0.1.44") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1y4hg6fbjy6k1z46z082fpwhi9975mcyk1ld57wfqc69i21526zz")))

(define-public crate-forky_web_macros-0.1.45 (c (n "forky_web_macros") (v "0.1.45") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "14w99mind01dym3lvgbz8kg6m541g8gil7sd6in1xp2fwz4khvgi")))

(define-public crate-forky_web_macros-0.1.46 (c (n "forky_web_macros") (v "0.1.46") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "2.0.*") (f (quote ("full"))) (d #t) (k 0)))) (h "0ygi4p57yii4dlx9xnp60nr9ssasgwz6in6riwb0m4pqj834ma80")))

