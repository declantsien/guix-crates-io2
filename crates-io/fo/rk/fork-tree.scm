(define-module (crates-io fo rk fork-tree) #:use-module (crates-io))

(define-public crate-fork-tree-2.0.0-alpha.2 (c (n "fork-tree") (v "2.0.0-alpha.2") (d (list (d (n "codec") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "1hacsq12sqanjqrpwq5shcp73rl9608yrpvsx9hlf419nghqm0jj")))

(define-public crate-fork-tree-2.0.0-alpha.3 (c (n "fork-tree") (v "2.0.0-alpha.3") (d (list (d (n "codec") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "07vxkvsf0ks15zs7ckpc696n8pf23ig6wwg120q67imk67r01k1i")))

(define-public crate-fork-tree-2.0.0-alpha.5 (c (n "fork-tree") (v "2.0.0-alpha.5") (d (list (d (n "codec") (r "^1.2.0") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "1fw26pkrhsqpj98cg6yj424ckrhid4nabc6agmssmd3d074klasb")))

(define-public crate-fork-tree-2.0.0-alpha.6 (c (n "fork-tree") (v "2.0.0-alpha.6") (d (list (d (n "codec") (r "^1.3.0") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "0wlym6f0yy8mdl1kw9iwhp3i4w194l80y7i63lkwgbphg0mlk920")))

(define-public crate-fork-tree-2.0.0-alpha.7 (c (n "fork-tree") (v "2.0.0-alpha.7") (d (list (d (n "codec") (r "^1.3.0") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "19j34kwkg62i3mgyj1bgvwgjhc5y3zmzqbfq3hhrpcpnsfsagjh0")))

(define-public crate-fork-tree-2.0.0-alpha.8 (c (n "fork-tree") (v "2.0.0-alpha.8") (d (list (d (n "codec") (r "^1.3.0") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "0yimbas8lwlj8m5pl1bgabm8jilljzdysnz2ldbwkq3366byr1g0")))

(define-public crate-fork-tree-2.0.0-rc1 (c (n "fork-tree") (v "2.0.0-rc1") (d (list (d (n "codec") (r "^1.3.0") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "12lv6l5lw8x72hzh9c73vd84hdyay988jgzgkpai31sspn82vmwn")))

(define-public crate-fork-tree-2.0.0-rc2 (c (n "fork-tree") (v "2.0.0-rc2") (d (list (d (n "codec") (r "^1.3.0") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "0vrj62k6dwqd1r7qf54fvq9agpf6dpf98wkb8hkk0c6s7kfapzj8")))

(define-public crate-fork-tree-2.0.0-rc3 (c (n "fork-tree") (v "2.0.0-rc3") (d (list (d (n "codec") (r "^1.3.0") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "1bzkijvnzl07d2f9z819yd9fi29ylmpypqicxzvbqdsbdyfk8y2d")))

(define-public crate-fork-tree-2.0.0-rc4 (c (n "fork-tree") (v "2.0.0-rc4") (d (list (d (n "codec") (r "^1.3.1") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "1j7z37kfpi6d6j93mnh1ydsgbmcdgfzcbzdkkypyc0y5w6am2kc2")))

(define-public crate-fork-tree-2.0.0-rc5 (c (n "fork-tree") (v "2.0.0-rc5") (d (list (d (n "codec") (r "^1.3.1") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "1hd1zc3mag380076hnxgfx2hkfnprn32bdgxagrzn0ax1l2gwcnq")))

(define-public crate-fork-tree-2.0.0-rc6 (c (n "fork-tree") (v "2.0.0-rc6") (d (list (d (n "codec") (r "^1.3.1") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "0d0wklx53m635n8z56j8xspbvi77cla2j8d8vfhh02mc1qb66ani")))

(define-public crate-fork-tree-2.0.0 (c (n "fork-tree") (v "2.0.0") (d (list (d (n "codec") (r "^1.3.1") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "15p3imxyqbp96xdpnvrzy1id53cx4lgrgwkr8jpxan6i0v3g3mwq")))

(define-public crate-fork-tree-2.0.1 (c (n "fork-tree") (v "2.0.1") (d (list (d (n "codec") (r "^1.3.1") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "09lrnm7rq3w9nkc5mnk845aw5rj3dxfw5aagagsg111whlmi7xjb")))

(define-public crate-fork-tree-3.0.0 (c (n "fork-tree") (v "3.0.0") (d (list (d (n "codec") (r "^2.0.0") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "1f0grl8lyl26s0zy0c1q6dz0cavp527bqhispksjyarkgzwraav3")))

(define-public crate-fork-tree-4.0.0 (c (n "fork-tree") (v "4.0.0") (d (list (d (n "codec") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "0azs59mxlqcz7m94qblz8hfsp1nckvjxxf5379q1lz98mv4sfdh6")))

(define-public crate-fork-tree-5.0.0 (c (n "fork-tree") (v "5.0.0") (d (list (d (n "codec") (r "^3.2.2") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "0jw22fks72v2ah6y2k97sxy3ipvr7p85w0zaxcbrbxrfyn0s0prw")))

(define-public crate-fork-tree-6.0.0 (c (n "fork-tree") (v "6.0.0") (d (list (d (n "codec") (r "^3.2.2") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "1hkks6142h2a7a2lmccbsmqanrzbk228b0mdf3a1gnxgyicqbw2l")))

(define-public crate-fork-tree-7.0.0 (c (n "fork-tree") (v "7.0.0") (d (list (d (n "codec") (r "^3.2.2") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "1fgp06awwyvdq2k0m98ibl3s62zvgqwk6lbgkdram2blz5gzw5a6")))

(define-public crate-fork-tree-7.1.0-dev1 (c (n "fork-tree") (v "7.1.0-dev1") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "1q33kzd2j0s1garm8ghjkbch64b80j6rawbklyvjwsznsa0b48yk")))

(define-public crate-fork-tree-7.1.0-dev.2 (c (n "fork-tree") (v "7.1.0-dev.2") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "0xlvy9yz915kcq16v60kzjs1jkfmi9kxxbrm6h9a3nsh3c6y8af6")))

(define-public crate-fork-tree-7.1.0-dev.3 (c (n "fork-tree") (v "7.1.0-dev.3") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "19p5wc56x5xc8kb179yb91z3swy7ncg5d61ckkzm5cd05qy0j644")))

(define-public crate-fork-tree-7.1.0-dev.4 (c (n "fork-tree") (v "7.1.0-dev.4") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "0mskn3r5prc1rqyy9i9f6mnph2a165xyavmgwgnw3265japw5q7q")))

(define-public crate-fork-tree-7.1.0-dev.5 (c (n "fork-tree") (v "7.1.0-dev.5") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "12ipz17jxvfh7ddyb3x07nyddkqd0lwgfrz7h89d55fvjp09qa6w")))

(define-public crate-fork-tree-7.1.0-dev.6 (c (n "fork-tree") (v "7.1.0-dev.6") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "19ri0hgcbwdmk0dkv7yql3ryl1lr1032kpl5n13i8yidcxrzs37f")))

(define-public crate-fork-tree-8.0.0 (c (n "fork-tree") (v "8.0.0") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "10z4n3hf1xy6lfbw1aqm5iq2mpafspbffngclzqa235m05x6hiwn")))

(define-public crate-fork-tree-9.0.0 (c (n "fork-tree") (v "9.0.0") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "0ivb04hrdkkabh7km53npp61rwdqmbm67jl091ra259k1lkyk5s7")))

(define-public crate-fork-tree-10.0.0-dev.1 (c (n "fork-tree") (v "10.0.0-dev.1") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "0i176zly2wb82mvyakzmwgaag2iali5mladl1dbsmd69k7qifdy4")))

(define-public crate-fork-tree-10.0.0 (c (n "fork-tree") (v "10.0.0") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "1jjfmw350jl7k82szxbhaqsgd2slwr24lkwj1hhpj0lgccx4vmd6")))

(define-public crate-fork-tree-11.0.0 (c (n "fork-tree") (v "11.0.0") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "1faxrx7w5pw5ws1c73wg3kv0xa7xq86ycdcfwz6fbw6w211hlb8w")))

(define-public crate-fork-tree-12.0.0 (c (n "fork-tree") (v "12.0.0") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "0ay5gy6qr85rh2pvqac1rb8nlz7yjamy029n8liwzv622l1kygg9")))

(define-public crate-fork-tree-13.0.0 (c (n "fork-tree") (v "13.0.0") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")))) (h "1q2j8ivhd9yrzc7nkybfzlzyfnam1xapwsjzanfb9s1v9lqw4k5d")))

