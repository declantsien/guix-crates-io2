(define-module (crates-io fo rk forkjoin) #:use-module (crates-io))

(define-public crate-forkjoin-1.0.0 (c (n "forkjoin") (v "1.0.0") (d (list (d (n "deque") (r "0.2.*") (d #t) (k 0)) (d (n "num_cpus") (r "0.2.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "1hfxz33jk785rrvd6ik3dhgcii10vnmk30gcd83ysar6hfqp353s")))

(define-public crate-forkjoin-1.0.1 (c (n "forkjoin") (v "1.0.1") (d (list (d (n "deque") (r "0.2.*") (d #t) (k 0)) (d (n "num_cpus") (r "0.2.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "0cn5250cjz0wywhf6cc5m3j6ppbnqsya3zisc229iwk2nlyz1n32")))

(define-public crate-forkjoin-2.0.0 (c (n "forkjoin") (v "2.0.0") (d (list (d (n "deque") (r "0.2.*") (d #t) (k 0)) (d (n "num_cpus") (r "0.2.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "0z17zcs592rzh7q72vjdgwyrda28269hlwz0cs5k4qwrvalf5kgl")))

(define-public crate-forkjoin-2.0.1 (c (n "forkjoin") (v "2.0.1") (d (list (d (n "deque") (r "0.2.*") (d #t) (k 0)) (d (n "num_cpus") (r "0.2.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "1hxcbkj4jk660z1wx1m78nap3zdbbjcqadv5cjmilfd331gdyq3c")))

(define-public crate-forkjoin-2.1.0 (c (n "forkjoin") (v "2.1.0") (d (list (d (n "deque") (r "0.2.*") (d #t) (k 0)) (d (n "num_cpus") (r "0.2.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "07a7vsd0svzm4z1qwg6ifp4qjykav4h4s7cv778cad6cff6cnfar") (f (quote (("threadstats"))))))

(define-public crate-forkjoin-2.2.0 (c (n "forkjoin") (v "2.2.0") (d (list (d (n "deque") (r "0.2.*") (d #t) (k 0)) (d (n "num_cpus") (r "0.2.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "0c49qc7ni8znqpwkjpc111gjjpcy1n5vixmvzsqixk04wchfhlz9") (f (quote (("threadstats"))))))

(define-public crate-forkjoin-2.3.0 (c (n "forkjoin") (v "2.3.0") (d (list (d (n "deque") (r "0.2.*") (d #t) (k 0)) (d (n "num_cpus") (r "0.2.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "1qani786icxkafczqhv1agan46gdxakfb3p5lszdw0lziifgadm0") (f (quote (("threadstats"))))))

