(define-module (crates-io fo rk fork_stream) #:use-module (crates-io))

(define-public crate-fork_stream-0.1.0 (c (n "fork_stream") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "insta") (r "^1.31.0") (d #t) (k 2)) (d (n "pin-project") (r "^1.1.3") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "17bzcvbj8yvg93dv9fr5axhbmp5h6xxgq4f9zkzjva55d8lwym7w")))

