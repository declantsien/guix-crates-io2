(define-module (crates-io fo sc fosc-rs) #:use-module (crates-io))

(define-public crate-fosc-rs-0.1.0 (c (n "fosc-rs") (v "0.1.0") (d (list (d (n "ta-common") (r "^0.1.2") (d #t) (k 0)) (d (n "tsf-rs") (r "^0.1.0") (d #t) (k 0)))) (h "08v184fi9bhmhpnfqjaybbmmcj225ps1qpcnahx3gq3cs6wvy5aq")))

