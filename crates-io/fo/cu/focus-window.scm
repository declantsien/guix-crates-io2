(define-module (crates-io fo cu focus-window) #:use-module (crates-io))

(define-public crate-focus-window-0.1.0 (c (n "focus-window") (v "0.1.0") (d (list (d (n "focus-events") (r "^0.1.0") (d #t) (k 0)) (d (n "glium") (r "^0.16.0") (d #t) (k 0)))) (h "0xvskbhpx6bg74w5qj5qbd4imxg5cir5dd8k8v8x3zlcmalh9vvb")))

