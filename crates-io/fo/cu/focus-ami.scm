(define-module (crates-io fo cu focus-ami) #:use-module (crates-io))

(define-public crate-focus-ami-0.0.1 (c (n "focus-ami") (v "0.0.1") (d (list (d (n "directories") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)) (d (n "sysinfo") (r "^0") (d #t) (k 0)))) (h "1c8rnkj15f1sidaiash7dbxc4s3bziywww029k51bs7cznkghhdf") (y #t)))

(define-public crate-focus-ami-0.0.2 (c (n "focus-ami") (v "0.0.2") (d (list (d (n "directories") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)) (d (n "sysinfo") (r "^0") (d #t) (k 0)))) (h "0xvcra9fcya7zxx1p4k4c2jg8v0zqp0h47gvqijy4s2sc655rknf") (y #t)))

(define-public crate-focus-ami-0.0.3 (c (n "focus-ami") (v "0.0.3") (d (list (d (n "directories") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)) (d (n "sysinfo") (r "^0") (d #t) (k 0)))) (h "180fr88dswjf7dpys08qh87irp3qian0ilbwisk1c2szh8fhs8p7")))

(define-public crate-focus-ami-0.0.4 (c (n "focus-ami") (v "0.0.4") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)) (d (n "sysinfo") (r "^0") (d #t) (k 0)))) (h "1hasavbmx17v3qfck1qgxh8y5mkhvip5yq229ar7iyswbrdhn9kh")))

(define-public crate-focus-ami-0.0.5 (c (n "focus-ami") (v "0.0.5") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0") (d #t) (k 0)) (d (n "sysinfo") (r "^0") (d #t) (k 0)))) (h "19mvlpjmaz88jijv389nyykpnn2lz51a26skrhbbx2v35p8zl6iq")))

(define-public crate-focus-ami-0.0.6 (c (n "focus-ami") (v "0.0.6") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23") (d #t) (k 0)))) (h "0hnm8c9pj09kim5ys1w5ikjpmyc8s594kz7lhz1ank3m1ry8cx9z")))

(define-public crate-focus-ami-0.0.7 (c (n "focus-ami") (v "0.0.7") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "sysinfo") (r "^0.24") (d #t) (k 0)))) (h "1ygjk6762vmlwjcs2vr75zmv6f4n3xm219bf9vciiai3jx8wsang")))

(define-public crate-focus-ami-0.0.8 (c (n "focus-ami") (v "0.0.8") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "sysinfo") (r "^0.25") (d #t) (k 0)))) (h "14g43g58bqf75r8l2n3vg068rk51idz40swdlb8zqrrk0da77qzx")))

(define-public crate-focus-ami-0.0.9 (c (n "focus-ami") (v "0.0.9") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26") (d #t) (k 0)))) (h "15nr7p3axmz3669vz7fis5929rcnn3lk9kmnbk2vq2z39f2036zv")))

(define-public crate-focus-ami-0.1.0 (c (n "focus-ami") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "sysinfo") (r "^0.27") (d #t) (k 0)))) (h "1yzfchdca3gpr54n3qm8cznz26prcqbh9ihn1v9c04cimiy476ah")))

(define-public crate-focus-ami-0.1.1 (c (n "focus-ami") (v "0.1.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "sysinfo") (r "^0.27") (d #t) (k 0)))) (h "12bf5gh4z69jdx47qqiyhbz7iai6s7fw8r60rc4r9095vp240hxj")))

(define-public crate-focus-ami-1.0.0 (c (n "focus-ami") (v "1.0.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10") (f (quote ("fuzzy-select" "history" "completion"))) (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "sysinfo") (r "^0.27") (d #t) (k 0)))) (h "1qy20a8jf3klcb0466z412j1flvc6zxljm3yvl43pq2ibfkc2zy5")))

