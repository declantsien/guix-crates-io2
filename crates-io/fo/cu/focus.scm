(define-module (crates-io fo cu focus) #:use-module (crates-io))

(define-public crate-focus-0.1.0 (c (n "focus") (v "0.1.0") (d (list (d (n "focus-events") (r "^0.1.0") (d #t) (k 0)) (d (n "focus-input") (r "^0.1.0") (d #t) (k 0)) (d (n "focus-sound") (r "^0.1.0") (d #t) (k 0)) (d (n "focus-window") (r "^0.1.0") (d #t) (k 0)))) (h "07nnha8f1f0r798j8s9194xhinqr7l6civ8pkdalz75y4y9y86sz")))

(define-public crate-focus-0.1.1 (c (n "focus") (v "0.1.1") (h "1im0kzcbgcg37nc1ba2zmc043z78qbxzjxa0zasg8s1c96mnhrdb")))

