(define-module (crates-io fo xh foxhole-production-calculator) #:use-module (crates-io))

(define-public crate-foxhole-production-calculator-0.1.0 (c (n "foxhole-production-calculator") (v "0.1.0") (d (list (d (n "foxhole-production-calculator-types") (r "^0.1") (d #t) (k 0)) (d (n "foxhole-production-calculator-types") (r "^0.1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 1)))) (h "1li6fp3jfpg4aaxh03hb4xjbf3nc10zj6jyxa7jzr32cf4nlx7ds")))

(define-public crate-foxhole-production-calculator-0.1.1 (c (n "foxhole-production-calculator") (v "0.1.1") (d (list (d (n "foxhole-production-calculator-types") (r "^0.1") (d #t) (k 0)) (d (n "foxhole-production-calculator-types") (r "^0.1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 1)))) (h "10rll3m228l747xrgw8wma3plzzmh3z3qj66xmki07ydn9xbf8dv")))

(define-public crate-foxhole-production-calculator-0.2.0 (c (n "foxhole-production-calculator") (v "0.2.0") (d (list (d (n "foxhole-production-calculator-types") (r "^0.2.0") (d #t) (k 0)) (d (n "foxhole-production-calculator-types") (r "^0.2.0") (d #t) (k 1)) (d (n "genco") (r "^0.17") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 1)))) (h "1kp88n7mj3z3wnvdbdb7l2l19ilymndk5fm9hj8fag3abim0y4mh")))

(define-public crate-foxhole-production-calculator-0.2.2 (c (n "foxhole-production-calculator") (v "0.2.2") (d (list (d (n "foxhole-production-calculator-types") (r "^0.2.0") (d #t) (k 0)) (d (n "foxhole-production-calculator-types") (r "^0.2.0") (d #t) (k 1)) (d (n "genco") (r "^0.17") (d #t) (k 1)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 1)))) (h "1abd9hsk2i9x4nr7is1yh8v1d320dzgkl38prq5fyl6nr1j476dw")))

