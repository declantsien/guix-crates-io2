(define-module (crates-io fo xh foxhole-production-calculator-cli) #:use-module (crates-io))

(define-public crate-foxhole-production-calculator-cli-0.1.0 (c (n "foxhole-production-calculator-cli") (v "0.1.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "foxhole-production-calculator") (r "^0.1") (d #t) (k 0)) (d (n "foxhole-production-calculator-types") (r "^0.1") (d #t) (k 0)))) (h "1sn2bkl54mjvpxk35vvmkjvpgf33p2h6i4p51jaz2hkavs9c2pkl")))

(define-public crate-foxhole-production-calculator-cli-0.1.1 (c (n "foxhole-production-calculator-cli") (v "0.1.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "foxhole-production-calculator") (r "^0.1") (d #t) (k 0)) (d (n "foxhole-production-calculator-types") (r "^0.1") (d #t) (k 0)))) (h "0gmq4gm36wxqwl6jw79d1qcykwckrgi2kkp1a6i85jj6mrg2xiby")))

(define-public crate-foxhole-production-calculator-cli-0.2.0 (c (n "foxhole-production-calculator-cli") (v "0.2.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "foxhole-production-calculator") (r "^0.2.0") (d #t) (k 0)) (d (n "foxhole-production-calculator-types") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "003psqmkgj74p7d7z0dh8vfbn72zd1jdpszqldk6vhj79jicyh1z")))

(define-public crate-foxhole-production-calculator-cli-0.2.1 (c (n "foxhole-production-calculator-cli") (v "0.2.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "foxhole-production-calculator") (r "^0.2.0") (d #t) (k 0)) (d (n "foxhole-production-calculator-types") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "181gkfd57723v1vz2199c3k016hpv0h4llc3q1pb1fkxdf2bk3yw")))

(define-public crate-foxhole-production-calculator-cli-0.2.2 (c (n "foxhole-production-calculator-cli") (v "0.2.2") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "foxhole-production-calculator") (r "^0.2.0") (d #t) (k 0)) (d (n "foxhole-production-calculator-types") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "166zm3yxg9srmhknnfgi1kxhjdymk0jzrw2jxfppndm3j7zhm4dd")))

