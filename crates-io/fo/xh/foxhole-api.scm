(define-module (crates-io fo xh foxhole-api) #:use-module (crates-io))

(define-public crate-foxhole-api-0.1.0 (c (n "foxhole-api") (v "0.1.0") (d (list (d (n "mockito") (r "^0.30") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0c984cn1f4dfxwpw8chdx9fp8d1m5m4kb83s603786dcjgzl0sc6")))

(define-public crate-foxhole-api-0.2.0 (c (n "foxhole-api") (v "0.2.0") (d (list (d (n "mockito") (r "^0.30") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1vaznzbhryd1wvgkg518f3xv46x209v6njyc4mxikzb1asi61sqh")))

(define-public crate-foxhole-api-0.2.1 (c (n "foxhole-api") (v "0.2.1") (d (list (d (n "mockito") (r "^0.30") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "010rf8s9rp89fys0kqq8x8p94jpviykp01nf6wjmcvk3p4iv9dxr")))

(define-public crate-foxhole-api-0.3.0 (c (n "foxhole-api") (v "0.3.0") (d (list (d (n "mockito") (r "^0.30") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "tokio") (r "^1.12") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1cd76kzwv62vlxnzfsl2cz7hgx7nqvwwzm8rjmpfrilc00v1hv4a")))

