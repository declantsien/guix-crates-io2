(define-module (crates-io fo xh foxhole-production-calculator-types) #:use-module (crates-io))

(define-public crate-foxhole-production-calculator-types-0.1.0 (c (n "foxhole-production-calculator-types") (v "0.1.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qpvg5zvm606pq4mbsvcf7gc7a6giqpw169iyhw91qhwqqkzqkrk")))

(define-public crate-foxhole-production-calculator-types-0.1.1 (c (n "foxhole-production-calculator-types") (v "0.1.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sc5w8x27ml27plbv0mnz9v95s0wj3b0kzbcjf997zibhjbq84gk")))

(define-public crate-foxhole-production-calculator-types-0.2.0 (c (n "foxhole-production-calculator-types") (v "0.2.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "genco") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "13pv7qvn0wk4vzwsazisifvfchbrpq6521x59682p1xyjp7ga0ha")))

(define-public crate-foxhole-production-calculator-types-0.2.2 (c (n "foxhole-production-calculator-types") (v "0.2.2") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "genco") (r "^0.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rljbl6a9n9bim7p1yxddm1hhi7w5kki4xhja932z8bl98nawbr8")))

