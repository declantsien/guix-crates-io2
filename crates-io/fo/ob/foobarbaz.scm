(define-module (crates-io fo ob foobarbaz) #:use-module (crates-io))

(define-public crate-foobarbaz-0.1.0 (c (n "foobarbaz") (v "0.1.0") (h "12rxk34scyjjvqnwj71hdavv1ydqgqj8vm2gga71w857jzjycylb") (y #t)))

(define-public crate-foobarbaz-0.1.1 (c (n "foobarbaz") (v "0.1.1") (h "0p2myha4jz5nc8pn6w1nxygf7mzpjbcmry2md6ml21hg4ls1v5mf") (y #t)))

(define-public crate-foobarbaz-0.1.2 (c (n "foobarbaz") (v "0.1.2") (h "0xyl2qln4gzrfs3wqfxwph6sf8d2sxgxhzj8najpfmkg1jxrh7by") (y #t)))

