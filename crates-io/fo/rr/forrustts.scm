(define-module (crates-io fo rr forrustts) #:use-module (crates-io))

(define-public crate-forrustts-0.1.0 (c (n "forrustts") (v "0.1.0") (d (list (d (n "GSL") (r "^2.0.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tskit_rust") (r "^0.1.0") (d #t) (k 0)))) (h "1xf4n1l38wvsp4q93cwa30s3r9cyrl54jmllc85l5z7djv6x74pl")))

(define-public crate-forrustts-0.1.1 (c (n "forrustts") (v "0.1.1") (d (list (d (n "GSL") (r "^2.0.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tskit_rust") (r "^0.1.0") (d #t) (k 0)))) (h "1gkzaq0rp6gxyf4s9v9h7nhx5w03knb2x25nyw9cw24kp8n08g5z")))

(define-public crate-forrustts-0.1.2 (c (n "forrustts") (v "0.1.2") (d (list (d (n "GSL") (r "^2.0.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tskit") (r "^0.1.1") (d #t) (k 0)))) (h "0inr487f3jhx86699711jkzzrgd8zia42nxxxbbckpl70fngdxd8")))

(define-public crate-forrustts-0.1.3 (c (n "forrustts") (v "0.1.3") (d (list (d (n "GSL") (r "^4.0.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tskit") (r "^0.1.1") (d #t) (k 0)))) (h "0hak5w2h3fskx7vj4j7rmgg8gaxl2zd7xj3m3r2c6i0qn48jkdbv")))

(define-public crate-forrustts-0.2.0 (c (n "forrustts") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "clap") (r "~2.27.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tskit") (r "=0.2.0") (d #t) (k 0)))) (h "1x7wd8z6w9y8fl3xqpa3fx4v6nwagij5k0myxwza7jcmxa4752rp")))

