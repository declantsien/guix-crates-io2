(define-module (crates-io fo rr forr) #:use-module (crates-io))

(define-public crate-forr-0.1.0 (c (n "forr") (v "0.1.0") (d (list (d (n "manyhow") (r "^0.4.2") (k 0)) (d (n "proc-macro-utils") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0l33x4dczlgsncx6hiwha28b8zi8rm1madnvpr4dvww6r6vszfnm")))

(define-public crate-forr-0.1.1 (c (n "forr") (v "0.1.1") (d (list (d (n "manyhow") (r "^0.4.2") (k 0)) (d (n "proc-macro-utils") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1v4a8rnaz8ryc53ihyxb9a2z4y8mk8zrp7cdlpq0f7hnbngkf3xg")))

(define-public crate-forr-0.2.0 (c (n "forr") (v "0.2.0") (d (list (d (n "manyhow") (r "^0.6") (k 0)) (d (n "proc-macro-utils") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "1dn7ic01yyfqbn5p2lql68xqh7jxly1ic35wzzv5ip5a0qr2l49j")))

(define-public crate-forr-0.2.1 (c (n "forr") (v "0.2.1") (d (list (d (n "manyhow") (r "^0.6") (k 0)) (d (n "proc-macro-utils") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0dis0n4skyhyd6lbfi9nb3zxjgn5jxi5zcvc26i6hl7cd4srnr4q")))

(define-public crate-forr-0.2.2 (c (n "forr") (v "0.2.2") (d (list (d (n "manyhow") (r "^0.6") (k 0)) (d (n "proc-macro-utils") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0rfmbnz8q83cm65iwxzhwhx9p9hdrrfgjad1zzkz24r7agsmac3v")))

(define-public crate-forr-0.2.3 (c (n "forr") (v "0.2.3") (d (list (d (n "manyhow") (r "^0.6") (k 0)) (d (n "proc-macro-utils") (r "^0.9.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)))) (h "0whk251yi21lc74k6n2y80j2q0kicq365wxaap9ixsgp0iha320y")))

