(define-module (crates-io fo rr forro) #:use-module (crates-io))

(define-public crate-forro-0.1.0 (c (n "forro") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "poly1305") (r "^0.8") (d #t) (k 0)) (d (n "zeroize") (r "^1.6") (f (quote ("derive"))) (o #t) (k 0)))) (h "1ppk5cmxybghv626qindxfgmqvi31xjbjhk75w388yh10xivwv22") (f (quote (("std") ("error_in_core") ("default" "zeroize")))) (s 2) (e (quote (("zeroize" "dep:zeroize"))))))

