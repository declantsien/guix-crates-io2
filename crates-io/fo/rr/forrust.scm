(define-module (crates-io fo rr forrust) #:use-module (crates-io))

(define-public crate-forrust-0.1.0 (c (n "forrust") (v "0.1.0") (d (list (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)))) (h "1j9hjmsjhy7ikn0chc65w331zd5wvmlgwzj9hvqrqm7isy3pk12d")))

(define-public crate-forrust-0.1.1 (c (n "forrust") (v "0.1.1") (d (list (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)))) (h "1szay1pmdkw7l3xqj53g78ckf2b537xs87fw02a9j4cmqkcgpjch")))

(define-public crate-forrust-0.1.2 (c (n "forrust") (v "0.1.2") (d (list (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)))) (h "10v5zms55h0gjcld6rshqs0i4arh6iwmnj11sb6zi01cnj44j3ik")))

(define-public crate-forrust-0.1.3 (c (n "forrust") (v "0.1.3") (d (list (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)))) (h "0jxjdizpz4lnqvf69vv8jjawj8ig70lp4q4i4lbxya4qlvn38647")))

(define-public crate-forrust-0.1.4 (c (n "forrust") (v "0.1.4") (d (list (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)))) (h "1wdnfm11k2slxr4xcn80y9f838sahw7lz1dwr9bnhap5ffx68mnf")))

(define-public crate-forrust-0.1.5 (c (n "forrust") (v "0.1.5") (d (list (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)))) (h "0702bsfl55nqbfdkdq4hbpb284f7vyz6hxardmyacgm1n0prqbqz")))

(define-public crate-forrust-0.1.6 (c (n "forrust") (v "0.1.6") (d (list (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)))) (h "14xkjmjsh1fqw213356sbxn8k1g7x1l5y8mpfs0y68pxb2lkkk99")))

(define-public crate-forrust-0.1.7 (c (n "forrust") (v "0.1.7") (d (list (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)))) (h "18c9c2v0dh5pym1888ivx5947bfhl6m3sssyx3nagb322cnjj6j5")))

(define-public crate-forrust-0.2.0 (c (n "forrust") (v "0.2.0") (d (list (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)))) (h "0q9v9sx8q5nkb72a9d5g49017fmq2dsdb6bx53az6d2wbnz6hlmd")))

(define-public crate-forrust-0.2.1 (c (n "forrust") (v "0.2.1") (d (list (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)))) (h "1lj0lyb50xa9dy2fjl651hl6xr458gwxrka5ybc4bcvz4qz15r54")))

(define-public crate-forrust-0.2.2 (c (n "forrust") (v "0.2.2") (d (list (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)))) (h "0j5nz7vvvag6262pp4wn0xjgybc2wk8s1an93lvrw9z328zsnann")))

(define-public crate-forrust-0.2.3 (c (n "forrust") (v "0.2.3") (d (list (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)))) (h "1vf93b5gbmkm4rr3dlrm9pjqy8r6z5ny9cd4h0yrj0qhhmfhjac7")))

(define-public crate-forrust-0.2.4 (c (n "forrust") (v "0.2.4") (d (list (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)))) (h "0aqij9f5llp5v6fkzggk5d3spw7z5vj9hywa5cv1ad93kmakx6ha")))

