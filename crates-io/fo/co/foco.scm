(define-module (crates-io fo co foco) #:use-module (crates-io))

(define-public crate-foco-0.1.1 (c (n "foco") (v "0.1.1") (d (list (d (n "array-macro") (r "^1.0.5") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.2") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "spms_ring") (r "^0.1.2") (d #t) (k 0)))) (h "1m24l1ak9prywf32drf1scslv41k7g3hg3arclrcbxxjckzgw066")))

(define-public crate-foco-0.1.2 (c (n "foco") (v "0.1.2") (d (list (d (n "array-macro") (r "^1.0.5") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.2") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "spms_ring") (r "^0.1.3") (d #t) (k 0)))) (h "0n4inlndfw816x4lvh65rcz1d6ldr4rdmaqsd37w6b5k79h2slvp")))

(define-public crate-foco-0.1.4 (c (n "foco") (v "0.1.4") (d (list (d (n "array-macro") (r "^1.0.5") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "spms_ring") (r "^0.1.4") (d #t) (k 0)))) (h "1pd7j7w2nl1l16zg1r1yyjc8pby08ss7jg05vdr4y0ayglj6b9a4")))

