(define-module (crates-io fo ot footile) #:use-module (crates-io))

(define-public crate-footile-0.0.1 (c (n "footile") (v "0.0.1") (h "1786mi3dsiwiczc5y06i9zx3i1bghmxin7nlzpf6kd5537hfwwrq")))

(define-public crate-footile-0.0.2 (c (n "footile") (v "0.0.2") (h "0pkl4g50ry3rgkm57shgsdnp10v23gfppzfqsfh81f4yc26fmrjs")))

(define-public crate-footile-0.0.3 (c (n "footile") (v "0.0.3") (h "1bdfa4kdvmd6vppljyaw3lsx1wzrpbg5zsfkq8w33ixcvl3k69yg")))

(define-public crate-footile-0.0.4 (c (n "footile") (v "0.0.4") (h "0n7mwnxjwfqfwgkc6y3zpgjbpd8687ds38k2ypv73blabh71c14m")))

(define-public crate-footile-0.0.5 (c (n "footile") (v "0.0.5") (d (list (d (n "png") (r "^0.10.0") (d #t) (k 0)))) (h "06sz9myy0nvaw6x1zv5wbk9y7pcmwv7k5k5xhgf1w6kks895bmfl")))

(define-public crate-footile-0.0.6 (c (n "footile") (v "0.0.6") (d (list (d (n "palette") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.10") (d #t) (k 0)))) (h "11376245v1z9na67ybg9cdcgry369ybd9fp05jz4zy112vbhj9cw")))

(define-public crate-footile-0.0.7 (c (n "footile") (v "0.0.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "palette") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.10") (d #t) (k 0)))) (h "04m09l0j0ggbazm2jhca85ilrp8kqa74bpzflhixvjryjwl307zg")))

(define-public crate-footile-0.0.8 (c (n "footile") (v "0.0.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "palette") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.10") (d #t) (k 0)))) (h "0hxifqpcrpmc1g54szs4r1a8wj4cwlrlgl6jmjhxrzfgmj5ayiqg")))

(define-public crate-footile-0.0.9 (c (n "footile") (v "0.0.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "palette") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.10") (d #t) (k 0)))) (h "1zckkn0wmsfdb3qfkcwn3sgps7zj1q1c1bhlmj3f4r6hpwasanar")))

(define-public crate-footile-0.0.10 (c (n "footile") (v "0.0.10") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "palette") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.10") (d #t) (k 0)))) (h "10m3mcfjpkaw0f6dv9m87ms4gxrbw0brf45xikrilqrcy2lh8f2c")))

(define-public crate-footile-0.1.0 (c (n "footile") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "png") (r "^0.10") (d #t) (k 0)))) (h "11si8jiddrqgis1bhz2w394rm4kdycbsd8jjp0bmfv8jb6vjjrxf")))

(define-public crate-footile-0.1.1 (c (n "footile") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "png") (r "^0.10") (d #t) (k 0)))) (h "17w36xaxnrppcpp2r2xfsrddss7rk3nglkvnwpjhxhs4q23lnii3")))

(define-public crate-footile-0.2.0 (c (n "footile") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "png") (r "^0.10") (d #t) (k 0)))) (h "0l8l5a1j314id253jcwi8gy8c7sixrd9z6b0lx1m4bpdv0f8ynyk")))

(define-public crate-footile-0.3.0 (c (n "footile") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "png") (r "^0.10") (d #t) (k 0)))) (h "16ps02y2zb33iylwnkd9w307b6ipqfpx3fr0wij5frbnfzann226")))

(define-public crate-footile-0.3.1 (c (n "footile") (v "0.3.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "png") (r "^0.10") (d #t) (k 0)))) (h "1lj703r3x3313narmiziwqj360dgkn1ynjn3l1fxgrmjkz5m3s42") (f (quote (("use-simd") ("default" "use-simd"))))))

(define-public crate-footile-0.4.0 (c (n "footile") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pix") (r "^0.12") (d #t) (k 0)) (d (n "png_pong") (r "^0.4") (d #t) (k 2)))) (h "030i8gs9jijwzgfvfyziacn71khfrm5y1kyf0j0zlqfyhdk8c8x2") (f (quote (("simd") ("default" "simd"))))))

(define-public crate-footile-0.5.0 (c (n "footile") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "png_pong") (r "^0.5") (d #t) (k 2)))) (h "00cq1ah6g3c0v9q5lsh6pbz857idlzwixfljyin4b7ayngzpzzrn") (f (quote (("simd") ("default" "simd"))))))

(define-public crate-footile-0.6.0 (c (n "footile") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "png_pong") (r "^0.5") (d #t) (k 2)))) (h "0849zb5s1klbcdg0fnz98ycbfy5804sbz96m4y2pl1vz91my3514") (f (quote (("simd") ("default" "simd"))))))

(define-public crate-footile-0.7.0 (c (n "footile") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "png_pong") (r "^0.8") (d #t) (k 2)) (d (n "pointy") (r "^0.3") (d #t) (k 0)))) (h "017x3d5w3rkasjgsb4sydhcjs7zqyik2mjiiy3fjb7h54sk166gp") (f (quote (("simd") ("default" "simd"))))))

