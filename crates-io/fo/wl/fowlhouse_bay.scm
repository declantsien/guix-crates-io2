(define-module (crates-io fo wl fowlhouse_bay) #:use-module (crates-io))

(define-public crate-fowlhouse_bay-0.1.0 (c (n "fowlhouse_bay") (v "0.1.0") (h "0skjiaf3y4p6c9znhf38dk6y7hmc3wkdyfh1cldn8hzlmrysmvv4")))

(define-public crate-fowlhouse_bay-0.2.0 (c (n "fowlhouse_bay") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.17.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)))) (h "1fdyn886adbigjvi7v3d7jkh6iffqjkqjxgmzz5gvd50a0akj462")))

(define-public crate-fowlhouse_bay-0.2.1 (c (n "fowlhouse_bay") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.17.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)))) (h "13vrgvw228hp4jn7y6rfryjxbila2c8dgj92glmd3s0ypm4ja4xh")))

(define-public crate-fowlhouse_bay-0.2.2 (c (n "fowlhouse_bay") (v "0.2.2") (d (list (d (n "crossterm") (r "^0.17.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)))) (h "1jbr3912ify4kw69ns9wrzwizp2nn1d4s7zd5dsydmgphnxrbbxg")))

(define-public crate-fowlhouse_bay-0.2.3 (c (n "fowlhouse_bay") (v "0.2.3") (d (list (d (n "crossterm") (r "^0.17.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)))) (h "04blyw7j0k3y292zcwg49mnyx4viarpp4m359l2r8rj8ck33b3j1")))

(define-public crate-fowlhouse_bay-0.3.0 (c (n "fowlhouse_bay") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.17.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)))) (h "1rhijl2940qjpbikzrknh84gblscijywprd34h7ralbrzm63kbyf")))

