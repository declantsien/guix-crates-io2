(define-module (crates-io fo ld folda) #:use-module (crates-io))

(define-public crate-folda-0.0.1 (c (n "folda") (v "0.0.1") (d (list (d (n "display_derive") (r "^0.0.0") (d #t) (k 0)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)) (d (n "uuid") (r "^0.7.2") (d #t) (k 0)))) (h "08hyhb3mfsji5fyih0xdgdl5chazz7x33x9hz5vbwhirxgy65mqd")))

