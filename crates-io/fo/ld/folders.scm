(define-module (crates-io fo ld folders) #:use-module (crates-io))

(define-public crate-folders-0.1.0 (c (n "folders") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.23") (d #t) (k 0)))) (h "0m68dk5syrc5ii3a6wsw8sikck0cwcv5d1dm6mqf0w54aymmnlbq")))

