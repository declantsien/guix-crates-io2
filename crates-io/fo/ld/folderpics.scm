(define-module (crates-io fo ld folderpics) #:use-module (crates-io))

(define-public crate-folderpics-0.4.0 (c (n "folderpics") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "config") (r "^0.10.1") (d #t) (k 0)) (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "magick_rust") (r "^0.14.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r6g3jb0vk0966r6wwnqg9dx9xwvmls3jc6vczsqi67g89i71h9k")))

