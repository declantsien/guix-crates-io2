(define-module (crates-io fo ld fold-dif) #:use-module (crates-io))

(define-public crate-fold-dif-0.1.0 (c (n "fold-dif") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "file-size") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1sn6m38zgldgd56bakgw48j07hlrac6fffgqxb05rxndj7dziax1")))

