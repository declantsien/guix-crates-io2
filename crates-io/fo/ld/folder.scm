(define-module (crates-io fo ld folder) #:use-module (crates-io))

(define-public crate-folder-0.1.0 (c (n "folder") (v "0.1.0") (d (list (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1wbx0ims3zwnzbi67j0njwbkrw8ki2najlbwphnqfljzkvjx8szq")))

(define-public crate-folder-0.1.1 (c (n "folder") (v "0.1.1") (d (list (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "19z22y3h2jh2rbwvrwghjfcjyla7kcyp5f3jzhg0l6klki68r3n7")))

(define-public crate-folder-0.2.0 (c (n "folder") (v "0.2.0") (d (list (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1848s50vnwwcd1vaj1krklfg3cnmbkbg8qlym59142f1hsx23dn7")))

(define-public crate-folder-0.3.0 (c (n "folder") (v "0.3.0") (d (list (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0nzji6nkqmddqfqbwbxal8vgsais6v7lvqcinn4wgqqrzmrf5kjv")))

(define-public crate-folder-0.3.1 (c (n "folder") (v "0.3.1") (d (list (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "19q8nxqsxamdlr7q8c1p137l1nbkvy82q1gvsbgf7fc8vsg2d0hs")))

(define-public crate-folder-0.4.0 (c (n "folder") (v "0.4.0") (d (list (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "13n37j42gkiy7v1r6h8ayckc5zyaab3zidzcdw2qfcg0mrqga3lq")))

(define-public crate-folder-0.5.0 (c (n "folder") (v "0.5.0") (d (list (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1wwf3hbzp7xlh5kcv4z41n7qk1a8a1051l1hddbr2agr7hil8j7m")))

(define-public crate-folder-0.5.1 (c (n "folder") (v "0.5.1") (d (list (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1nf6gvxmrz909syzwy76fr0dgyf51w4nbgxz4ysqj3dsm0q9z24k")))

(define-public crate-folder-0.6.0 (c (n "folder") (v "0.6.0") (d (list (d (n "loop") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0cc7gs28dzg70683fr3p9x6r0yy8zdk727wgr68yy4qpy51xamrr")))

