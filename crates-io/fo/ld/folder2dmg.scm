(define-module (crates-io fo ld folder2dmg) #:use-module (crates-io))

(define-public crate-folder2dmg-0.1.5 (c (n "folder2dmg") (v "0.1.5") (d (list (d (n "assert_cmd") (r "^1.0.2") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (f (quote ("suggestions"))) (k 0)) (d (n "predicates") (r "^1.0.5") (d #t) (k 2)) (d (n "question") (r "^0.2.2") (d #t) (k 0)))) (h "1c9l940rbk1j1nsc3awn42lqawm9l4jczn7sqnvjys51wlh1i2d4")))

