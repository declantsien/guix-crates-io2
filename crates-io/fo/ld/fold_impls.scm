(define-module (crates-io fo ld fold_impls) #:use-module (crates-io))

(define-public crate-fold_impls-0.0.0 (c (n "fold_impls") (v "0.0.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "1mf60s9cda92lyqhwl0r9fpl2dxiqy6cy3n3fzf1nj3j4lwk909y")))

