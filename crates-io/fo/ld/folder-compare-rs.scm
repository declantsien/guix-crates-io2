(define-module (crates-io fo ld folder-compare-rs) #:use-module (crates-io))

(define-public crate-folder-compare-rs-0.1.0 (c (n "folder-compare-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.38") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full" "tracing"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "13ql56njfjsazbr0xfpxr1h35yxqrj9p76j8ys3kr01qzqy08ry8")))

