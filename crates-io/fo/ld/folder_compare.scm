(define-module (crates-io fo ld folder_compare) #:use-module (crates-io))

(define-public crate-folder_compare-0.1.0 (c (n "folder_compare") (v "0.1.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0hg403qllsn1hjrpc0lwxklih31i71qmifr1mxdwqirkw6pq996q")))

(define-public crate-folder_compare-0.1.1 (c (n "folder_compare") (v "0.1.1") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0sziw9m6n3vq0jr9lvad9aflm2npq7604cwj3amhr769wkdkf0id")))

(define-public crate-folder_compare-0.1.2 (c (n "folder_compare") (v "0.1.2") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0avb6fy9l6sqj6084qn1klyw4k5kvq8b3k8dd8vfdzpg5rsrj4a3")))

(define-public crate-folder_compare-0.2.0 (c (n "folder_compare") (v "0.2.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0vmhix9m6w3j2x3a93f450bgmrmz0y8wmckdghpfljcbpshldcy6")))

(define-public crate-folder_compare-0.3.0 (c (n "folder_compare") (v "0.3.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0lyzli3q94ygkg5ddxvn8c61xbp9hlx9hnn5ll1a48fr2l3xkhwp")))

(define-public crate-folder_compare-0.4.0 (c (n "folder_compare") (v "0.4.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "15r37v5ldk44jkm5a4bxy5wbkzczj5ckqn9y4gjwf1z28yaqpk7p")))

