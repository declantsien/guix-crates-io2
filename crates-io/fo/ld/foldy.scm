(define-module (crates-io fo ld foldy) #:use-module (crates-io))

(define-public crate-foldy-0.1.0-alpha.1 (c (n "foldy") (v "0.1.0-alpha.1") (h "071gvi9krangpk7fm2683155l9dv097dj2srxxw74ldg8lk2v63s")))

(define-public crate-foldy-0.1.0-alpha.2 (c (n "foldy") (v "0.1.0-alpha.2") (h "117k1bnnbwi1gm3kbbyzs93nw10rpcl9787a71854zv9j1pgzhnx")))

