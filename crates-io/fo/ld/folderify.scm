(define-module (crates-io fo ld folderify) #:use-module (crates-io))

(define-public crate-folderify-3.0.0-pre1 (c (n "folderify") (v "3.0.0-pre1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 0)))) (h "09gghzxfipd82d3sjzbbnj6gqdnaqfcf5j1c58fkbyb9yfv2608y")))

(define-public crate-folderify-3.0.0-pre2 (c (n "folderify") (v "3.0.0-pre2") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 0)))) (h "1zmn6l1n8dd4scldqvl03bs7dh8m2knh9rxg53fhracscbfh1b7a")))

(define-public crate-folderify-3.0.0 (c (n "folderify") (v "3.0.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 0)))) (h "0h8lzqh0470wmvbxfms6qx31sz3hbal0wfvry9xffr26fmn1x1v3")))

(define-public crate-folderify-3.0.1 (c (n "folderify") (v "3.0.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 0)))) (h "0sgiw5cyqar27g0xsm27ng6sxan9l7fa9lxlvq2wkj5vdz5ac1mq")))

(define-public crate-folderify-3.0.2 (c (n "folderify") (v "3.0.2") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.2.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 0)))) (h "15538yalxyfh4p73mz23b6wgbc02c0c3n985hkg4xffy7ssx99z4")))

(define-public crate-folderify-3.0.3 (c (n "folderify") (v "3.0.3") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.2.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 0)))) (h "1zmv3crvhivigl3yzhpprvzzq1aqq4agra3rgvpaili5784x13rz")))

(define-public crate-folderify-3.0.4 (c (n "folderify") (v "3.0.4") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.2.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 0)))) (h "0if2yg3b2fvyryk781w8v4ifbildfvzrs2k2nnwkfjh51mjg4rn5")))

(define-public crate-folderify-3.0.5 (c (n "folderify") (v "3.0.5") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.2.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 0)))) (h "08jak7ih979lld4gf7mcsa699bkcpz8za5afn4bk0whzv11jh1vm")))

(define-public crate-folderify-3.0.6 (c (n "folderify") (v "3.0.6") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.2.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 0)))) (h "1vavniqvbnv4ryj78i1kap9i7cv8kxj3z38s2cfx5qlsiaz1mrnd")))

(define-public crate-folderify-3.0.7 (c (n "folderify") (v "3.0.7") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.2.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 0)))) (h "0s3s7p3qad23b2l5cya3f7yinn16dxrwqmqfd8j8pqfg2s6pnp5h")))

(define-public crate-folderify-3.0.8 (c (n "folderify") (v "3.0.8") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.2.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 0)))) (h "0j2mb084dx083p9sy0v06702g7g3s09jqyj5w3lkgyd79ff6icpy")))

(define-public crate-folderify-3.0.9 (c (n "folderify") (v "3.0.9") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.2.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 0)))) (h "1hg8az9wjwvrnhs03xn5dh8n619q690framx0imc1qzl2nxnah0w")))

(define-public crate-folderify-3.0.10 (c (n "folderify") (v "3.0.10") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.2.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 0)))) (h "1552i8zly3ala0qllka8pmg1k3gqx8sx49mq00fk1zsxkjai58a0")))

(define-public crate-folderify-3.0.11 (c (n "folderify") (v "3.0.11") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.2.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 0)))) (h "10ca523pwylcbjf2n1ywcx1y97ccbhv3wxgqks6297a3psgg3mg0")))

(define-public crate-folderify-3.0.12 (c (n "folderify") (v "3.0.12") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.2.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.4") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 0)))) (h "1hknh0if3kxifa2cw3mp79dxbrg6zqwhmzsl5pg0wgsncav08i79")))

(define-public crate-folderify-3.0.13 (c (n "folderify") (v "3.0.13") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.2.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 0)))) (h "0dipc7xb9vf9wg6ygxh457grsbk215pqvqd5qmpwlz037xf0mbhb")))

(define-public crate-folderify-3.1.0 (c (n "folderify") (v "3.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.2.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 0)))) (h "1pvqmqljjzdczy0g41naqyd5b3nqbwzqpisgdjgvcma54xpmj6kz")))

(define-public crate-folderify-3.1.1 (c (n "folderify") (v "3.1.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.2.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 0)))) (h "122pw8idj3n6hvzjizpmf97sijhhmqqpk210504q130ar3m7yqxb")))

(define-public crate-folderify-3.2.0 (c (n "folderify") (v "3.2.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.2.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 0)))) (h "0jj1xiza8nidkmyv40904clm1r75k2fmqy9lvdhinn1z0snpa94z")))

(define-public crate-folderify-3.2.1 (c (n "folderify") (v "3.2.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.2.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 0)))) (h "0v2n5mbvvs4n2zn545j4b9ck2svl3sgk2dkc7j9ajdkkccahl0lq")))

(define-public crate-folderify-3.2.2 (c (n "folderify") (v "3.2.2") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.2.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 0)))) (h "1c3w404cb3iifrv9i68ax8szcmg2kb6h6d3770g8j4amwsg96lh8")))

(define-public crate-folderify-3.2.4 (c (n "folderify") (v "3.2.4") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.2.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 0)))) (h "1vj2myp365mmgqlxr7lva7z3b1x5h5gbmsg977acjsfcwv5xdm3h")))

(define-public crate-folderify-4.0.0 (c (n "folderify") (v "4.0.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.2.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "mktemp") (r "^0.5.0") (d #t) (k 0)))) (h "0cppas0p38qdy7rq228jkc56433w5wa35nl18r25lsfnbns81l6l")))

