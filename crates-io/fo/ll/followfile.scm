(define-module (crates-io fo ll followfile) #:use-module (crates-io))

(define-public crate-followfile-0.1.0 (c (n "followfile") (v "0.1.0") (d (list (d (n "tokio") (r "^1.37.0") (f (quote ("time"))) (o #t) (d #t) (k 0)))) (h "1dq06q0bnmpvs9f9bhcg7yc68z9z8462jyqq68icjjihmzgacfva") (f (quote (("tail" "tokio/macros" "tokio/rt" "tokio/fs" "tokio/io-util"))))))

(define-public crate-followfile-0.1.1 (c (n "followfile") (v "0.1.1") (d (list (d (n "tokio") (r "^1.37.0") (f (quote ("time"))) (o #t) (d #t) (k 0)))) (h "05c5zgffzh3c1fzdyzq7zfk9wm3g0rxrg7z47khs8zn44fg28fz3") (f (quote (("tail" "tokio/macros" "tokio/rt" "tokio/fs" "tokio/io-util"))))))

(define-public crate-followfile-0.2.0 (c (n "followfile") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("time"))) (o #t) (d #t) (k 0)))) (h "0cxpvlaqil83bzsyb237rfvh5w2b0krf0n747i2dcjdx1jih6giy") (s 2) (e (quote (("tail" "tokio/macros" "tokio/rt" "tokio/fs" "tokio/io-util" "dep:clap"))))))

