(define-module (crates-io fo ll follow-redirects) #:use-module (crates-io))

(define-public crate-follow-redirects-0.1.0 (c (n "follow-redirects") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4.6") (d #t) (k 0)) (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "http") (r "^0.1.5") (d #t) (k 0)) (d (n "hyper") (r "^0.11.24") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.16") (d #t) (k 2)))) (h "126wsi293r15bkz93f5d23xjgvijg65dlfsg355qzdicz4npcc6p")))

(define-public crate-follow-redirects-0.1.1 (c (n "follow-redirects") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4.6") (d #t) (k 0)) (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "http") (r "^0.1.5") (d #t) (k 0)) (d (n "hyper") (r "^0.11.24") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.16") (d #t) (k 2)))) (h "06nyl1sk7dxvx2gxk3w3zvafjhs2lkb7kk19yzxd7n7khxzx6ahl")))

(define-public crate-follow-redirects-0.1.2 (c (n "follow-redirects") (v "0.1.2") (d (list (d (n "bytes") (r "^0.4.6") (d #t) (k 0)) (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "http") (r "^0.1.5") (d #t) (k 0)) (d (n "hyper") (r "^0.11.24") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.16") (d #t) (k 2)))) (h "1bsqp14azn6rsy7jm6hzppddr47l9ibn1v2sjh41a24jvgpczg8g")))

(define-public crate-follow-redirects-0.1.3 (c (n "follow-redirects") (v "0.1.3") (d (list (d (n "bytes") (r "^0.4.6") (d #t) (k 0)) (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "http") (r "^0.1.5") (d #t) (k 0)) (d (n "hyper") (r "^0.11.24") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.16") (d #t) (k 2)))) (h "1jbddahp05jfl37cfk8irv1snfbjp24yvw1vczh25sgf9m4s7js6")))

