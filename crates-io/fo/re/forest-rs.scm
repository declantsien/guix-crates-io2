(define-module (crates-io fo re forest-rs) #:use-module (crates-io))

(define-public crate-forest-rs-0.1.0 (c (n "forest-rs") (v "0.1.0") (h "0pj905ysp3vmynmvwf2g85vcdrk3x1p6d6l2ig8vkf5f0c85mkml")))

(define-public crate-forest-rs-0.2.0 (c (n "forest-rs") (v "0.2.0") (h "15b448sa2d7qnys6yr7kxs0p9d7c1rc674sxy8i33k2rfsxfxhz5")))

