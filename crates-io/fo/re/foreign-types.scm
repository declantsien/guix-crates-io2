(define-module (crates-io fo re foreign-types) #:use-module (crates-io))

(define-public crate-foreign-types-0.1.0 (c (n "foreign-types") (v "0.1.0") (h "088wfpy40malk71mp5gm9i1b59a8synifhpy5wj04372r3wknc3d")))

(define-public crate-foreign-types-0.2.0 (c (n "foreign-types") (v "0.2.0") (h "17d2p1lqply2p785zbxs3dnpkbhdgbvp3rrbl5dsry27pnwmch1y")))

(define-public crate-foreign-types-0.3.0 (c (n "foreign-types") (v "0.3.0") (h "1l751y2f0ijsaw08kj281kw05mgafhamid97svrg3zqrj3qh9g2y")))

(define-public crate-foreign-types-0.3.1 (c (n "foreign-types") (v "0.3.1") (d (list (d (n "foreign-types-shared") (r "^0.1") (d #t) (k 0)))) (h "0rchvhjpx99aq8f603qjnfv5brr57mnfvic34kia3ch3c11l06x2")))

(define-public crate-foreign-types-0.2.1 (c (n "foreign-types") (v "0.2.1") (d (list (d (n "foreign-types-shared") (r "^0.1") (d #t) (k 0)))) (h "013l3w70wgwlb92xvkl97kjn25260ldjrfbzx7m6q3nij3gpp900")))

(define-public crate-foreign-types-0.3.2 (c (n "foreign-types") (v "0.3.2") (d (list (d (n "foreign-types-shared") (r "^0.1") (d #t) (k 0)))) (h "1cgk0vyd7r45cj769jym4a6s7vwshvd0z4bqrb92q1fwibmkkwzn")))

(define-public crate-foreign-types-0.4.0 (c (n "foreign-types") (v "0.4.0") (d (list (d (n "foreign-types-macros") (r "^0.1") (d #t) (k 0)) (d (n "foreign-types-shared") (r "^0.2") (d #t) (k 0)))) (h "0ca4i38yrf9iy5k47lr1ylb3rvcbn36d81k5pr5kzf6kmj6p111n") (f (quote (("std" "foreign-types-macros/std") ("default" "std"))))))

(define-public crate-foreign-types-0.5.0 (c (n "foreign-types") (v "0.5.0") (d (list (d (n "foreign-types-macros") (r "^0.2") (d #t) (k 0)) (d (n "foreign-types-shared") (r "^0.3") (d #t) (k 0)))) (h "0rfr2zfxnx9rz3292z5nyk8qs2iirznn5ff3rd4vgdwza6mdjdyp") (f (quote (("std" "foreign-types-macros/std") ("default" "std"))))))

