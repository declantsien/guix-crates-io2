(define-module (crates-io fo re foreignc_template) #:use-module (crates-io))

(define-public crate-foreignc_template-0.1.0 (c (n "foreignc_template") (v "0.1.0") (d (list (d (n "foreignc_util") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "tera") (r "^0.11.20") (d #t) (k 0)))) (h "17qvbi03crqmva38byhq3cccrwb3h801v7zlcbzy5m128mgadcs4")))

(define-public crate-foreignc_template-0.1.1 (c (n "foreignc_template") (v "0.1.1") (d (list (d (n "foreignc_util") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "tera") (r "^0.11.20") (d #t) (k 0)))) (h "1zil26n7pnf9xx3iza7df2mn7qqlcgwv8gg5wa199z0g7bps6rgs")))

