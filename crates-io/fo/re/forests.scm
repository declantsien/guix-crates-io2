(define-module (crates-io fo re forests) #:use-module (crates-io))

(define-public crate-forests-0.0.1 (c (n "forests") (v "0.0.1") (h "1k64649pwhpr79nigxi4liynrskg5v55352fbvj27pzdjlmjjic5")))

(define-public crate-forests-0.0.2 (c (n "forests") (v "0.0.2") (h "1hspmhkvlh3ch3228z48p805495j0klc15k5368jlh13gzn173k5")))

