(define-module (crates-io fo re forecastle) #:use-module (crates-io))

(define-public crate-forecastle-0.0.1 (c (n "forecastle") (v "0.0.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "mcom") (r "^0.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "wchar") (r "^0.6") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("d3d9" "d3d9types" "minwindef" "windef" "winerror" "errhandlingapi" "libloaderapi" "winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0y67dp4xnyb5shb3nj89y2v6xnn4x8df0vjc292y177j4cxwql1b") (y #t)))

