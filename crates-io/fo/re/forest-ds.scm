(define-module (crates-io fo re forest-ds) #:use-module (crates-io))

(define-public crate-forest-ds-0.1.0 (c (n "forest-ds") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0a39yj8pvflpkwvrnljzha3x8057lxp03wvazrykyx1203gq5zbw")))

(define-public crate-forest-ds-0.1.1 (c (n "forest-ds") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0g3vhpk400v6lxis78dfpixdkrws0si8bnvi7v79xgw6j8rdphl2")))

(define-public crate-forest-ds-0.1.2 (c (n "forest-ds") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1fyrd6arr5wfhbaxmn8jcali2s5lpd5q7l5fihvv0hvapmx5x0j1") (y #t)))

(define-public crate-forest-ds-0.1.3 (c (n "forest-ds") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1iiaad756nx4gdd8wkg1yf429igmv56y8d683l9p30dd9h0cgvd9")))

(define-public crate-forest-ds-0.1.4 (c (n "forest-ds") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13bs7ywhr6l76qkz63kxbdcdmdgrjiys862acm71jz8dnpr8m4rf")))

(define-public crate-forest-ds-1.0.0 (c (n "forest-ds") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "17cqg9vngz6pf98ab9fm7cain17g3hjxxv89iwh12wppjx0fl90c")))

(define-public crate-forest-ds-1.0.2 (c (n "forest-ds") (v "1.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1x006rpwybdl8hddikwy8wrv5xj4kqxkl7cprksnhjh66826y83y")))

(define-public crate-forest-ds-1.0.3 (c (n "forest-ds") (v "1.0.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "033v7sa9zvc4kfi6q6f775mrhi0gfx7n8q3mgafss5cn67jaxgqv")))

(define-public crate-forest-ds-1.0.4 (c (n "forest-ds") (v "1.0.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jqxzdsb7i1rw7xfywy1nzq4dnhj21rgv5mnb4ilp4v6hhxma14n")))

(define-public crate-forest-ds-1.1.4 (c (n "forest-ds") (v "1.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1a7jxrf98wk16g2ncz1n7w73nl3nb7g6rz7hlnna69rqpmxipy0c")))

(define-public crate-forest-ds-1.1.5 (c (n "forest-ds") (v "1.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04qpp5dh9a7cl5r4xybm7d1agnm9ymd846g5r1y11h0k06vd4kfn")))

(define-public crate-forest-ds-1.1.6 (c (n "forest-ds") (v "1.1.6") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "19427b5akb543jagvv5lifrp5mwarnscplm69p3b7mazr703kwvc")))

(define-public crate-forest-ds-1.1.7 (c (n "forest-ds") (v "1.1.7") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "07lc5m1jxl1m4zzad9zwjj2isbni5ypr4c98zb0wdwizfxii9fwk") (r "1.56.1")))

