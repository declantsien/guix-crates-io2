(define-module (crates-io fo re foreach) #:use-module (crates-io))

(define-public crate-foreach-0.1.0 (c (n "foreach") (v "0.1.0") (h "1jalj1gjn29091lvha5gawnq4hhz5f44d5cql1vw7qyqlm0kmjkc")))

(define-public crate-foreach-0.1.1 (c (n "foreach") (v "0.1.1") (h "0x26pizzx1x6n8kb29dv8mzkpkxh5s3cpmz120ny0rmfjpg6dyyf")))

(define-public crate-foreach-0.2.0 (c (n "foreach") (v "0.2.0") (h "0jvkl1l6dwrknpsq5q0p9hrmrszq9m295flpmhqbbaqxbf8nr7k9")))

(define-public crate-foreach-0.3.0 (c (n "foreach") (v "0.3.0") (h "14kd7ph4vd2xv3h5pq4mxy4n5l5q16411df6f8j8r8yyxd4zjh3f")))

