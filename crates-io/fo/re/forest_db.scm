(define-module (crates-io fo re forest_db) #:use-module (crates-io))

(define-public crate-forest_db-0.1.0 (c (n "forest_db") (v "0.1.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 0) (p "forest_encoding")) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rocksdb") (r "^0.15.0") (o #t) (d #t) (k 0)) (d (n "sled") (r "^0.34") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0imgg71n4k52qp2xhk1jwgpwp9bnimfz1337szk58m9zdcj68x33")))

