(define-module (crates-io fo re forecast_io) #:use-module (crates-io))

(define-public crate-forecast_io-0.1.0 (c (n "forecast_io") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "0qjc5d9y0py7wmg9v38fq3lrxrx0y6mj0vm0vj4w5zv3970yk2fr")))

(define-public crate-forecast_io-0.1.1 (c (n "forecast_io") (v "0.1.1") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "0smp9gfi5wdq12j3rqdbg97g4y4rk1q3d7hwakq00flkcdqahy1y")))

(define-public crate-forecast_io-0.2.0 (c (n "forecast_io") (v "0.2.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "195b29faaglkv0cfg925k54pqncqkfx3ihmb8w467swfmpcjclzi")))

(define-public crate-forecast_io-0.3.0 (c (n "forecast_io") (v "0.3.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1x0jn2w3dnrlpwcg9c80a5sn8lls8szzcnyx4jqkdvflqcps42xv")))

(define-public crate-forecast_io-0.4.0 (c (n "forecast_io") (v "0.4.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1c9n6xhjw3y5l3ldfjfc4zalyfa7kdly3f8pc5ys3zis3qg0fpd9")))

