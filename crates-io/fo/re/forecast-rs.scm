(define-module (crates-io fo re forecast-rs) #:use-module (crates-io))

(define-public crate-forecast-rs-0.0.1 (c (n "forecast-rs") (v "0.0.1") (d (list (d (n "hyper") (r "^0.9.12") (d #t) (k 0)) (d (n "itertools") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^0.8.9") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8.10") (o #t) (d #t) (k 1)) (d (n "serde_derive") (r "^0.8.9") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.8.2") (d #t) (k 0)))) (h "0naf6i8w41629y1fy56j2n8vy1d9wn7hyd89kdpyc3hvaqr89pqa") (f (quote (("unstable" "serde_derive") ("integration") ("default" "serde_codegen")))) (y #t)))

