(define-module (crates-io fo re foreignc_util) #:use-module (crates-io))

(define-public crate-foreignc_util-0.1.0 (c (n "foreignc_util") (v "0.1.0") (h "1xaq8s3n6kk07fk67m1nz16sbdvnbxa0i0q9h3mdbsgw5nin7gp8")))

(define-public crate-foreignc_util-0.1.1 (c (n "foreignc_util") (v "0.1.1") (h "0zrp0gwknqs7hlzz4az7qdniy953kc7xc8qw36gnlq1aqxya0610")))

