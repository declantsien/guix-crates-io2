(define-module (crates-io fo re foreignc) #:use-module (crates-io))

(define-public crate-foreignc-0.1.0 (c (n "foreignc") (v "0.1.0") (d (list (d (n "foreignc_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "foreignc_template") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "1gvrhxjbm6xb8mkgbd2x3cqxhdwhql5wkw698w6mqcx3xjhchyj7") (f (quote (("template" "foreignc_derive/template" "foreignc_template"))))))

(define-public crate-foreignc-0.1.1 (c (n "foreignc") (v "0.1.1") (d (list (d (n "foreignc_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "foreignc_template") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "0jmqb9isgdm9jlv9d66afrkdifpi8fwfk4514rqn0nz7m741ds88") (f (quote (("template" "foreignc_derive/template" "foreignc_template"))))))

(define-public crate-foreignc-0.1.2 (c (n "foreignc") (v "0.1.2") (d (list (d (n "foreignc_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "foreignc_template") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "0vn7ldv0msraq8y9fh556wi40jzjsgj6v1xq01rd6l855x9hb7ij") (f (quote (("template" "foreignc_derive/template" "foreignc_template"))))))

