(define-module (crates-io fo re forest_json_utils) #:use-module (crates-io))

(define-public crate-forest_json_utils-0.1.0 (c (n "forest_json_utils") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0qlxqb1bildwmd931gbxxk9vaclfknpb4ja2abw8rs5dglmrggxj")))

(define-public crate-forest_json_utils-0.1.1 (c (n "forest_json_utils") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1a32x9968mlax8k4bzgbl014ac2gy0wi62jjf0pb414chjk9p8qd")))

