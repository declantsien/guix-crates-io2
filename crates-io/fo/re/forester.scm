(define-module (crates-io fo re forester) #:use-module (crates-io))

(define-public crate-forester-0.0.1 (c (n "forester") (v "0.0.1") (d (list (d (n "cpuprofiler") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.2") (k 2)) (d (n "gnuplot") (r "^0.0.24") (d #t) (k 2)) (d (n "image") (r "^0.18") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0hbvgll6j52vlyn0hajrdfznilcad6rmcc7lxh4lb8s7f4kgd6gg") (f (quote (("profiling" "cpuprofiler"))))))

(define-public crate-forester-0.0.2 (c (n "forester") (v "0.0.2") (d (list (d (n "cpuprofiler") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.2") (k 2)) (d (n "gnuplot") (r "^0.0.24") (d #t) (k 2)) (d (n "image") (r "^0.18") (d #t) (k 2)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0fh9nhqnjav9wnbk1bq15jx95mlv8r2xlhnm7byh3dzwzsxg5n71") (f (quote (("profiling" "cpuprofiler"))))))

(define-public crate-forester-0.0.3 (c (n "forester") (v "0.0.3") (d (list (d (n "cpuprofiler") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1lhjm8bnkvwl12xqy2y8m1qqsklbfvid772lkwph05dqcqyw4bxx")))

