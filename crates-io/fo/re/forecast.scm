(define-module (crates-io fo re forecast) #:use-module (crates-io))

(define-public crate-forecast-0.0.2 (c (n "forecast") (v "0.0.2") (d (list (d (n "hyper") (r "^0.9.12") (d #t) (k 0)) (d (n "itertools") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^0.8.9") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8.10") (o #t) (d #t) (k 1)) (d (n "serde_derive") (r "^0.8.9") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.8.2") (d #t) (k 0)))) (h "0yqf5krwi3qwxxiwh27w5gnr02jz9rj2pcjldqmg7vak5zkjpfsw") (f (quote (("unstable" "serde_derive") ("integration") ("default" "serde_codegen"))))))

(define-public crate-forecast-0.1.0 (c (n "forecast") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9.12") (d #t) (k 0)) (d (n "itertools") (r "^0.5.4") (d #t) (k 0)) (d (n "serde") (r "^0.8.9") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8.10") (o #t) (d #t) (k 1)) (d (n "serde_derive") (r "^0.8.9") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.8.2") (d #t) (k 0)))) (h "01zrg2p2gxdvzhyi47yd4vpnd900pmsi49a1706q5jwsa0vndc42") (f (quote (("unstable" "serde_derive") ("integration") ("default" "serde_codegen"))))))

(define-public crate-forecast-0.2.0 (c (n "forecast") (v "0.2.0") (d (list (d (n "itertools") (r "^0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1v4wzbd5zp7cklzx4x4xb8751dwp3hbyvs2fxl2whx6qqfbkfwr2") (f (quote (("integration"))))))

(define-public crate-forecast-1.0.0 (c (n "forecast") (v "1.0.0") (d (list (d (n "itertools") (r "0.7.*") (d #t) (k 0)) (d (n "reqwest") (r "0.8.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "1jg7yhw5ygkl0hmabx7avzaajf1zpn52m0hsxjqs2yk6mnhskxf5") (f (quote (("integration"))))))

(define-public crate-forecast-1.0.1 (c (n "forecast") (v "1.0.1") (d (list (d (n "itertools") (r "0.7.*") (d #t) (k 0)) (d (n "reqwest") (r "0.8.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "1iira071rc2dlygs0qyga1jh455jmwrdpv8d61r9ijgm95d82pha") (f (quote (("integration"))))))

(define-public crate-forecast-1.1.0 (c (n "forecast") (v "1.1.0") (d (list (d (n "itertools") (r "0.7.*") (d #t) (k 0)) (d (n "reqwest") (r "0.8.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "0l24x52nvhdfxzkd96ns8253i57jiwkyprbvqm9wvxcqixwq04gw") (f (quote (("integration"))))))

(define-public crate-forecast-2.0.0 (c (n "forecast") (v "2.0.0") (d (list (d (n "itertools") (r "0.7.*") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "0a78vdyb22mx22hycpdsisxiign1ghwiq7i4fxq7455hchs2391b") (f (quote (("integration"))))))

(define-public crate-forecast-3.0.0 (c (n "forecast") (v "3.0.0") (d (list (d (n "itertools") (r "0.7.*") (d #t) (k 0)) (d (n "reqwest") (r "0.10.*") (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "1v97y2pglr6hfw7kmci98s5mlplykrhczkbzya1yjyyp1i1jp6wa") (f (quote (("integration"))))))

