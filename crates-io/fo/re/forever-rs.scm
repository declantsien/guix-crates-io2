(define-module (crates-io fo re forever-rs) #:use-module (crates-io))

(define-public crate-forever-rs-0.1.0 (c (n "forever-rs") (v "0.1.0") (d (list (d (n "commander") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full"))) (d #t) (k 0)))) (h "1c89zgl96wz6h0rk5xgskmssl05kwp67id9srzabn06x43as2kqy")))

(define-public crate-forever-rs-0.1.1 (c (n "forever-rs") (v "0.1.1") (d (list (d (n "commander") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full"))) (d #t) (k 0)))) (h "1jlqifr9bc51489kja8azrmfk8rzi7011mxabic1qgrm47695z7z")))

(define-public crate-forever-rs-0.1.2 (c (n "forever-rs") (v "0.1.2") (d (list (d (n "commander") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (f (quote ("full"))) (d #t) (k 0)))) (h "114wwma4ia5nd8c92a54wfvb90lrnqsc5gmkkzhfval65jccac7r")))

