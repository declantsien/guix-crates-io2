(define-module (crates-io fo re foreback) #:use-module (crates-io))

(define-public crate-foreback-1.0.0 (c (n "foreback") (v "1.0.0") (d (list (d (n "async-channel") (r "^1.6.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.14") (f (quote ("executor"))) (k 2)) (d (n "futures-core") (r "^0.3.14") (k 0)) (d (n "pin-project") (r "^1.0.7") (d #t) (k 0)))) (h "06kl0yrvqyzn139b65di7sp8ksnqgx341db5hn3i3n73crfzrslw")))

(define-public crate-foreback-1.1.0 (c (n "foreback") (v "1.1.0") (d (list (d (n "async-channel") (r "^1.6.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.14") (f (quote ("executor"))) (k 2)) (d (n "futures-core") (r "^0.3.14") (k 0)) (d (n "pin-project") (r "^1.0.7") (d #t) (k 0)))) (h "1a09lagkshjqmrcsmw431kznjbddq3h8izmbbxgdmb9rq7nz5fp0")))

(define-public crate-foreback-1.1.1 (c (n "foreback") (v "1.1.1") (d (list (d (n "async-channel") (r "^1.6.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.14") (f (quote ("executor"))) (k 2)) (d (n "futures-core") (r "^0.3.14") (k 0)) (d (n "pin-project") (r "^1.0.7") (d #t) (k 0)))) (h "1j92j63gc3k7j7pr7clg9jcbcz5hrchp692mb5ahnlim8sjm8i21")))

(define-public crate-foreback-1.1.2 (c (n "foreback") (v "1.1.2") (d (list (d (n "futures-core") (r "^0.3.14") (k 0)) (d (n "pin-project") (r "^1.0.7") (d #t) (k 0)) (d (n "async-channel") (r "^1.6.1") (d #t) (k 2)) (d (n "futures") (r "^0.3.14") (f (quote ("executor"))) (k 2)))) (h "0h4mwydhwxgkxz5c1kqk88lpcgqw0nmw2v600233nldnl2laxyz8")))

