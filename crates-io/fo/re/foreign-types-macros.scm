(define-module (crates-io fo re foreign-types-macros) #:use-module (crates-io))

(define-public crate-foreign-types-macros-0.1.0 (c (n "foreign-types-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "16yjigjcsklcwy2ad32l24k1nwm9n3bsnyhxc3z9whjbsrj60qk6") (f (quote (("std"))))))

(define-public crate-foreign-types-macros-0.1.1 (c (n "foreign-types-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0bh6z5rwdhfm987idal8r0ha5svz1li7md5l1g2a5966gya1jns0") (f (quote (("std"))))))

(define-public crate-foreign-types-macros-0.2.0 (c (n "foreign-types-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0x71sdffjfb123l2jn5vhz0ni09b4rdq3h3gpczd1dj1g5qlr2yv") (f (quote (("std"))))))

(define-public crate-foreign-types-macros-0.2.1 (c (n "foreign-types-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1mmga61b4xca4gdfw2mfnvii5vnaaq62j3jvr3z297manbw17xv3") (f (quote (("std"))))))

(define-public crate-foreign-types-macros-0.2.2 (c (n "foreign-types-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1k6c73nq2gap7qxjygwi2ppdxwgki36g2qv8xh4cd6si806rsin8") (f (quote (("std"))))))

(define-public crate-foreign-types-macros-0.2.3 (c (n "foreign-types-macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hjpii8ny6l7h7jpns2cp9589016l8mlrpaigcnayjn9bdc6qp0s") (f (quote (("std"))))))

