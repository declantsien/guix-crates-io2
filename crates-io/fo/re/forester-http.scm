(define-module (crates-io fo re forester-http) #:use-module (crates-io))

(define-public crate-forester-http-0.1.0 (c (n "forester-http") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "173nhz7qrkmphv56lvm3vqhj7l7aaqsisk6ldmf75g8wy24b0mal")))

