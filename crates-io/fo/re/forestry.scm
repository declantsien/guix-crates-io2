(define-module (crates-io fo re forestry) #:use-module (crates-io))

(define-public crate-forestry-0.0.0 (c (n "forestry") (v "0.0.0") (h "0b3mq6f65jvn0y1kq34z3iy28rh7wlf02gr3dzf1fp54xrkm018s") (y #t)))

(define-public crate-forestry-0.0.1 (c (n "forestry") (v "0.0.1") (h "08f2smxjvrcgqx9xrdjvi251rvqg0h523ggxdnsclxdgksj6f6p2")))

