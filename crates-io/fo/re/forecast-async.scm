(define-module (crates-io fo re forecast-async) #:use-module (crates-io))

(define-public crate-forecast-async-0.1.1 (c (n "forecast-async") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.35") (d #t) (k 0)))) (h "1y5ihhllmv96dqrrawj9lcr36jrwqkw84m5c1va0qzrid57jzi35")))

