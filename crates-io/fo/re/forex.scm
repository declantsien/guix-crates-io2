(define-module (crates-io fo re forex) #:use-module (crates-io))

(define-public crate-forex-0.1.0 (c (n "forex") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pn7qr3spb3whaxvlmldapijs99x62psh8msjncgp2m3br5v69r0") (y #t)))

(define-public crate-forex-0.1.1 (c (n "forex") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i3fx1fs60psc6579848bd8gajsfhk7xmzppyz0hbp1rin0znx2g")))

