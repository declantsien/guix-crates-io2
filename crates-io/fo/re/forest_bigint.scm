(define-module (crates-io fo re forest_bigint) #:use-module (crates-io))

(define-public crate-forest_bigint-0.1.0 (c (n "forest_bigint") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.3") (d #t) (k 0)))) (h "17gg46c89xsvkvki7gdmqwx18i14p6m3k2kh7vsqr91hm4agab5m")))

(define-public crate-forest_bigint-0.1.1 (c (n "forest_bigint") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.3") (d #t) (k 0)))) (h "09b344k2v7vllkk42fb1g6w687rdv6c4r0l78gr5xw2qz4xn0gw7")))

(define-public crate-forest_bigint-0.1.2 (c (n "forest_bigint") (v "0.1.2") (d (list (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.3") (d #t) (k 0)))) (h "1p7p22i4ap2waylnax2hxanvglf730r3hlx39cf21z0sj1fb61v0") (f (quote (("json"))))))

(define-public crate-forest_bigint-0.1.3 (c (n "forest_bigint") (v "0.1.3") (d (list (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.12") (d #t) (k 0) (p "cs_serde_bytes")))) (h "051q5n54l1qrapj9hf2ry1ql29g9rrp6jl9rq0r3mfnnnhpnh4rn") (f (quote (("json"))))))

(define-public crate-forest_bigint-0.1.4 (c (n "forest_bigint") (v "0.1.4") (d (list (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.12") (d #t) (k 0) (p "cs_serde_bytes")))) (h "1ryixyp1k6z477i4bb8fjp95a2n564xp099bbfkj69zq7vrn77am") (f (quote (("json"))))))

