(define-module (crates-io fo re foreign-types-shared) #:use-module (crates-io))

(define-public crate-foreign-types-shared-0.1.0 (c (n "foreign-types-shared") (v "0.1.0") (h "17mw5rr4l0dw45ak29vic32fd17xwdsgg94y29g7nj65qfgq78ds")))

(define-public crate-foreign-types-shared-0.1.1 (c (n "foreign-types-shared") (v "0.1.1") (h "0jxgzd04ra4imjv8jgkmdq59kj8fsz6w4zxsbmlai34h26225c00")))

(define-public crate-foreign-types-shared-0.2.0 (c (n "foreign-types-shared") (v "0.2.0") (h "0kanxlif1vp0ffh2r9l610jqbkmb3183yqykxq1z5w1vay2rn7y6")))

(define-public crate-foreign-types-shared-0.3.0 (c (n "foreign-types-shared") (v "0.3.0") (h "0mg85r21bxg2i97zl3q8l3cviqz4wcbwziz875wlja3zpcrwz13n")))

(define-public crate-foreign-types-shared-0.3.1 (c (n "foreign-types-shared") (v "0.3.1") (h "0nykdvv41a3d4py61bylmlwjhhvdm0b3bcj9vxhqgxaxnp5ik6ma")))

