(define-module (crates-io fo re forensic-rs) #:use-module (crates-io))

(define-public crate-forensic-rs-0.1.0 (c (n "forensic-rs") (v "0.1.0") (h "1s78anlgh5qpnh9sdjvv1rz0669pmpn4xffk83193mkrh8h0avjf")))

(define-public crate-forensic-rs-0.1.1 (c (n "forensic-rs") (v "0.1.1") (h "0knpbzp1hm9c9z6czfhlws8bya36shxb0bdmi2r59dmb03yfi63g")))

(define-public crate-forensic-rs-0.1.3 (c (n "forensic-rs") (v "0.1.3") (h "0asvmhgxdji1m919ff8jx954w977g78ja3rdn5m5ychngm9b46nz")))

(define-public crate-forensic-rs-0.1.4 (c (n "forensic-rs") (v "0.1.4") (d (list (d (n "sqlite") (r "^0.28.1") (d #t) (k 2)))) (h "0w91m9bv7japznhpcwjj3820fx6x0ffk0nq1a246nlc26cf0k2fn")))

(define-public crate-forensic-rs-0.1.6 (c (n "forensic-rs") (v "0.1.6") (d (list (d (n "sqlite") (r "^0.28.1") (d #t) (k 2)))) (h "19ck8hfdmlvq29m5f2ii4z4ma7zbh1x11y9406x2nzcaz9f7c0yq")))

(define-public crate-forensic-rs-0.2.1 (c (n "forensic-rs") (v "0.2.1") (d (list (d (n "sqlite") (r "^0.28.1") (d #t) (k 2)))) (h "1lyagnyh1rbimjvmizkyii9vbvnh0aqsgxz8lbxgh5wwas7y98v9")))

(define-public crate-forensic-rs-0.3.1 (c (n "forensic-rs") (v "0.3.1") (d (list (d (n "sqlite") (r "^0.28.1") (d #t) (k 2)))) (h "1q1ac2x47jbaf5k993mzspz0rrk74ydd8ws2v1m3crv8j71nyac4")))

(define-public crate-forensic-rs-0.4.0 (c (n "forensic-rs") (v "0.4.0") (d (list (d (n "sqlite") (r "^0.28.1") (d #t) (k 2)))) (h "1fm21bq823g0jp747l7mpn1xq5j398wxvc2cfgk0lnpz2gmr2fjr")))

(define-public crate-forensic-rs-0.5.0 (c (n "forensic-rs") (v "0.5.0") (d (list (d (n "sqlite") (r "^0.28.1") (d #t) (k 2)))) (h "14byigs8flzmrr85xzrbf9yw1q9vib7g1b6nlf4laxlsh1d1wx3r")))

(define-public crate-forensic-rs-0.5.1 (c (n "forensic-rs") (v "0.5.1") (d (list (d (n "sqlite") (r "^0.28.1") (d #t) (k 2)))) (h "14dvhhqw4lfppq7c8xnfqfyrnysbliy32w0syx231ar3cr8din4j")))

(define-public crate-forensic-rs-0.5.2 (c (n "forensic-rs") (v "0.5.2") (d (list (d (n "sqlite") (r "^0.28.1") (d #t) (k 2)))) (h "110mw0nhqmi4chipmamwdagxwf26i2h7w4ra8g1jx54qy7sxbasd")))

(define-public crate-forensic-rs-0.6.1 (c (n "forensic-rs") (v "0.6.1") (d (list (d (n "sqlite") (r "^0.28.1") (d #t) (k 2)))) (h "0ghqam5320vc0qqc9yblz2farbip6xpvrmi8zqgpmkj0bbhwv6sk")))

(define-public crate-forensic-rs-0.6.2 (c (n "forensic-rs") (v "0.6.2") (d (list (d (n "sqlite") (r "^0.28.1") (d #t) (k 2)))) (h "0092f5xhkjzxqh00ca6qpriggac6cps7ksj0qfw7p7ngbdwqv154")))

(define-public crate-forensic-rs-0.7.0 (c (n "forensic-rs") (v "0.7.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "sqlite") (r "^0.28.1") (d #t) (k 2)))) (h "14fciicns054hc8z1cm4fprn0jv4wb9fcpd942glilkrwxy3nq8v") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-forensic-rs-0.7.1 (c (n "forensic-rs") (v "0.7.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "sqlite") (r "^0.28.1") (d #t) (k 2)))) (h "1iga2znr4xj8vm6wbicszs0qsln86ijdvs1lxr9hbfma1dayvym6") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-forensic-rs-0.7.2 (c (n "forensic-rs") (v "0.7.2") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "sqlite") (r "^0.28.1") (d #t) (k 2)))) (h "0dxk28pk6fvaa6izpb2nkzibshjp0lwmkvx3m319fm3wsa962ifg") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-forensic-rs-0.7.3 (c (n "forensic-rs") (v "0.7.3") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "sqlite") (r "^0.28.1") (d #t) (k 2)))) (h "1n1siww0ayss61gn3k0m78v593gsvn6b6rsafpyiwvdnc7xz2fik") (f (quote (("default" "serde")))) (y #t) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-forensic-rs-0.7.4 (c (n "forensic-rs") (v "0.7.4") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "sqlite") (r "^0.28.1") (d #t) (k 2)))) (h "0ijwlcqvwffcy2z3rvmran36mk9bs676dycdzixwc3g09wjibaxw") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-forensic-rs-0.8.0 (c (n "forensic-rs") (v "0.8.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "sqlite") (r "^0.28.1") (d #t) (k 2)))) (h "0zfggxxasrkn9zc56fwmf7zsgp51p46867j4g1s9ji41shdhmq7i") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-forensic-rs-0.9.0 (c (n "forensic-rs") (v "0.9.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "sqlite") (r "^0.28.1") (d #t) (k 2)))) (h "1w5w0by7cg4bsr1ywjq152as26qqvv3221w8adkd9mgn5ns3vhd7") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-forensic-rs-0.9.1 (c (n "forensic-rs") (v "0.9.1") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "sqlite") (r "^0.28.1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "086g33bsxlxglv0bg9691vxvqr2qag4b264cgljrqczfqpxqx1qy") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-forensic-rs-0.10.0 (c (n "forensic-rs") (v "0.10.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "sqlite") (r "^0.28.1") (d #t) (k 2)))) (h "00wxc1drgv97jchz0cw7pnri59mffw6c68ihmch561x1s4sjgr07") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-forensic-rs-0.11.0 (c (n "forensic-rs") (v "0.11.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "sqlite") (r "^0.28.1") (d #t) (k 2)))) (h "1zs4xr2nlad4lmrgal4appsysibz14vb9b4is5g5qv2sh7lrpcnc") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-forensic-rs-0.12.0 (c (n "forensic-rs") (v "0.12.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "sqlite") (r "^0.28.1") (d #t) (k 2)))) (h "1qwhki9y5n4swan7hmp475aa26718yvi0lzsfy0gq28qb7qsdzr0") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-forensic-rs-0.13.0 (c (n "forensic-rs") (v "0.13.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "sqlite") (r "^0.28.1") (d #t) (k 2)))) (h "19s5xsmqwqkz3dczs235h06livvadzsfp4yarzwnc7n2zw45ql3b") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

