(define-module (crates-io fo r- for-else) #:use-module (crates-io))

(define-public crate-for-else-0.1.0 (c (n "for-else") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "028qgb2hjdk52ss1d1g3cwlq3g6fp5adrc1nnx0b2773i483c3qs")))

(define-public crate-for-else-0.2.0 (c (n "for-else") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kgwmiphqf4g8iw5yvq5id4l36kvx5bpn1nx7a6zhj5cc4aladj4")))

