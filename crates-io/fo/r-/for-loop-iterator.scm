(define-module (crates-io fo r- for-loop-iterator) #:use-module (crates-io))

(define-public crate-for-loop-iterator-0.1.0 (c (n "for-loop-iterator") (v "0.1.0") (h "0azlip7cvl3qh8j8w9fs7ajk4ad16my9mimzyg32wjkzfn20dqlp")))

(define-public crate-for-loop-iterator-0.2.0 (c (n "for-loop-iterator") (v "0.2.0") (h "1vcscb6qcfaiskahnf9g85nbs1yjlhjhlakbqr94x8kwrn0pl6h6")))

(define-public crate-for-loop-iterator-0.2.1 (c (n "for-loop-iterator") (v "0.2.1") (h "17by0zfmq39cq5l66aq7ddq5zixlbq5hdgzvg0axzm309ws3kamn") (y #t)))

(define-public crate-for-loop-iterator-0.2.2 (c (n "for-loop-iterator") (v "0.2.2") (h "0rgflgsx1xdvzgzygaazxznz429kdkd9qipxr9l8aa35kbv463rs")))

