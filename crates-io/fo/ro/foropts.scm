(define-module (crates-io fo ro foropts) #:use-module (crates-io))

(define-public crate-foropts-0.1.0 (c (n "foropts") (v "0.1.0") (h "0cj0avh1cxy4718gqas88sb576ghd63rwky72mn75fzh5y5iv4bk")))

(define-public crate-foropts-0.1.1 (c (n "foropts") (v "0.1.1") (h "06yql2yc7d1mncpww9sj9h6y5s416lw749c537rkmcll8mal77ib")))

(define-public crate-foropts-0.1.2 (c (n "foropts") (v "0.1.2") (h "0c52n03fsmqchfkrxyh7lc03h5ravz1b9p18y0gb16bgplhjy82n")))

(define-public crate-foropts-0.2.0 (c (n "foropts") (v "0.2.0") (h "1qyv7vs0y61zlvnqyyvi7mil6kdw40rk5dzb781y59m29gs5b6vy")))

(define-public crate-foropts-0.2.1 (c (n "foropts") (v "0.2.1") (h "1rj3k9l8wyp76n9hr4585d0l35ki210xx6a4v9f22d35rz63c71g")))

(define-public crate-foropts-0.3.0 (c (n "foropts") (v "0.3.0") (h "085pmn76f1d5003cbjs81wi2myg4zyq23b4sgq36a3fbgwd0lzry")))

(define-public crate-foropts-0.3.1 (c (n "foropts") (v "0.3.1") (h "1rk8kw7ywqr7f65dr8bysxhqlsiqppna5aj4nap8qqh63rcx0pbv")))

(define-public crate-foropts-0.3.2 (c (n "foropts") (v "0.3.2") (h "0d348acmzx70pvgkqzsmfw57cdkzb6fz1bj5ybx9kd3y7pclprcl")))

(define-public crate-foropts-0.3.3 (c (n "foropts") (v "0.3.3") (h "0chjn9y82l7frmkaid4ydgyz138ln4a3lpw5wz74c35w22adjzjc")))

(define-public crate-foropts-0.3.4 (c (n "foropts") (v "0.3.4") (h "0alcwd1cknip44pbbf0mdwa7rxwfkra9pj3j3y0l9qxr3m8h60bd")))

(define-public crate-foropts-0.3.5 (c (n "foropts") (v "0.3.5") (h "05pi01nkcclzfzxk3mjgik7z7cb2dv9fsv3ivbz4lxhrg2r1hy7y")))

(define-public crate-foropts-0.3.6 (c (n "foropts") (v "0.3.6") (h "08jwbqrg9zwqz7xgrhq9cy6wa752ilkddc1mfqr125yw5yzb89qw")))

