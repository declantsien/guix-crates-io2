(define-module (crates-io fo x- fox-default) #:use-module (crates-io))

(define-public crate-fox-default-0.6.0 (c (n "fox-default") (v "0.6.0") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "1rk7z8671x36vay315waf3iah07kf444zx1dxywiwxyw8rg287wl")))

(define-public crate-fox-default-0.6.1 (c (n "fox-default") (v "0.6.1") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)))) (h "11dx2z9hgangvvjmbgw579372vw0d803jgxidm2yw8xnaasmi3ib")))

(define-public crate-fox-default-0.7.0 (c (n "fox-default") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)))) (h "1iwfhc3482fkkp8nipmlsv1n3wnasrap83lhz9349syz9d0if0l3")))

(define-public crate-fox-default-0.8.0 (c (n "fox-default") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)))) (h "1nv4jx040f705mrq27mxsgn3ynnr46z564crqmwgjp1kc95g54f0")))

