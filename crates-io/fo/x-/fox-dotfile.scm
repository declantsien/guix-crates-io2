(define-module (crates-io fo x- fox-dotfile) #:use-module (crates-io))

(define-public crate-fox-dotfile-0.2.0 (c (n "fox-dotfile") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.0.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "notify") (r "^5.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "02wjkc2lb1a9vqr7w2lf657dz22x3hbj8a5q29ckz8xjw84m1jkp")))

