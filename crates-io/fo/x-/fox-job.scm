(define-module (crates-io fo x- fox-job) #:use-module (crates-io))

(define-public crate-fox-job-0.1.0 (c (n "fox-job") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "06wknvrj2kyz5b78gl5yxhxlyyglw9lvp2731j1npmq1fq108w67")))

