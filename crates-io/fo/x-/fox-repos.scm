(define-module (crates-io fo x- fox-repos) #:use-module (crates-io))

(define-public crate-fox-repos-0.1.0 (c (n "fox-repos") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "octocrab") (r "^0.30.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.153") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1589j3j75v664nk917iizjpsg52ci0kg3prmfkckac5axxx4mycj")))

