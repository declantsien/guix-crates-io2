(define-module (crates-io fo x- fox-and-hounds) #:use-module (crates-io))

(define-public crate-fox-and-hounds-0.1.0 (c (n "fox-and-hounds") (v "0.1.0") (h "1rzf6bkhxmzfr3lmymjhqgbjhl9k289qrjg0p3h75r5rbp09dcnd") (y #t)))

(define-public crate-fox-and-hounds-0.1.1 (c (n "fox-and-hounds") (v "0.1.1") (h "0dw2agi4i83ms25layr0hxpyka65bq0i91rnzgmjxnvns6jcgwfi") (y #t)))

(define-public crate-fox-and-hounds-0.1.2 (c (n "fox-and-hounds") (v "0.1.2") (h "128b9vb8kclwpf3r1jwyxr6kxhimam6h1ajg550xm5glblqfzdy6") (y #t)))

(define-public crate-fox-and-hounds-0.1.3 (c (n "fox-and-hounds") (v "0.1.3") (d (list (d (n "fox-and-hounds-model") (r "^0.1.3") (d #t) (k 0)))) (h "1xdxn892irzrs0nx8z22winbcshvv8p6vhdp9ndax2jc5c6mpvfy") (y #t)))

(define-public crate-fox-and-hounds-0.1.4 (c (n "fox-and-hounds") (v "0.1.4") (d (list (d (n "fox-and-hounds-model") (r "^0.1.3") (d #t) (k 0)))) (h "0pmm3pk7hd6pklmz9412n2xz740csha4k1rwiwyzkzaqyqp33xny") (y #t)))

(define-public crate-fox-and-hounds-0.1.5 (c (n "fox-and-hounds") (v "0.1.5") (d (list (d (n "fox-and-hounds-model") (r "^0.1.3") (d #t) (k 0)))) (h "1msxzwd9h9c8l5rqf2cjixyvw29a8yii6jqhffwc95k3spg5sjhk") (y #t)))

(define-public crate-fox-and-hounds-0.2.0 (c (n "fox-and-hounds") (v "0.2.0") (d (list (d (n "fox-and-hounds-model") (r "^0.2.0") (d #t) (k 0)))) (h "1dqw2jivcxf304ngqbkvcb4jspjmdrc0sx80w0z6hsc1bmvqyj8x") (y #t)))

(define-public crate-fox-and-hounds-0.2.1 (c (n "fox-and-hounds") (v "0.2.1") (d (list (d (n "fox-and-hounds-model") (r "^0.2.0") (d #t) (k 0)))) (h "1q8ba4rg771jhhs7y8krl1kpgfl4gkd2s02r529vvvkcqansnf75") (y #t)))

(define-public crate-fox-and-hounds-0.2.2 (c (n "fox-and-hounds") (v "0.2.2") (d (list (d (n "fox-and-hounds-model") (r "^0.2.0") (d #t) (k 0)))) (h "0kric8kg6m3i4w3fbp1dhdcyqf5pc1h1r5cnzp28rjwnf7zm0ik0") (y #t)))

(define-public crate-fox-and-hounds-0.3.0 (c (n "fox-and-hounds") (v "0.3.0") (h "0qbr1fm76xqfvxknngvz13fq4yb34sgk2syv3s0px72n47jimw4v") (y #t)))

(define-public crate-fox-and-hounds-0.3.1 (c (n "fox-and-hounds") (v "0.3.1") (h "0mgk79qbpc9dhs1wf62qxz1jbabpgmv1h0k3kw5xc0rd4sxyrpv7") (y #t)))

(define-public crate-fox-and-hounds-0.4.0 (c (n "fox-and-hounds") (v "0.4.0") (h "1m4gx3lxzmp82ky02q9s5c8y7rv62ssf4rbcrxlyvzanxyay47wb") (y #t)))

(define-public crate-fox-and-hounds-0.4.1 (c (n "fox-and-hounds") (v "0.4.1") (h "1qipky1wqy06dvp09c2cz2zcyx7vjyc32s7bvn9h4ikdv2yrcwgs") (y #t)))

(define-public crate-fox-and-hounds-0.4.2 (c (n "fox-and-hounds") (v "0.4.2") (h "1l1s5chjlha50ss3lsd0g6q555jdbb57n9qgsycfb8hp0zan370d") (y #t)))

(define-public crate-fox-and-hounds-0.5.0 (c (n "fox-and-hounds") (v "0.5.0") (h "16qxi5w9vvh48z3y4h7h18icd85zf77pkvf6wv38rj5x8nd3xc54") (y #t)))

(define-public crate-fox-and-hounds-0.5.1 (c (n "fox-and-hounds") (v "0.5.1") (h "1z29sv20si5v98hd44l3dx8nvm1hwpwbj9im00ad6q2mmlx9jk15") (y #t)))

(define-public crate-fox-and-hounds-0.5.2 (c (n "fox-and-hounds") (v "0.5.2") (h "1jrja98lcd41javj80frfs1fr97x99s9hy47gsx97xxbnm1c9m8i") (y #t)))

(define-public crate-fox-and-hounds-0.6.0 (c (n "fox-and-hounds") (v "0.6.0") (h "1j20yrymwbdivxmcbpnvjdv7sgiiz1v7xdf6x71ki8bz5a0rqxny") (y #t)))

