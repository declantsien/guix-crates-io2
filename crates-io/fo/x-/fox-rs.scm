(define-module (crates-io fo x- fox-rs) #:use-module (crates-io))

(define-public crate-fox-rs-0.1.0 (c (n "fox-rs") (v "0.1.0") (h "16mcvdbim9m25zj6i9q1s6lpksd24vw7jnbv924nd21n1z79imdb")))

(define-public crate-fox-rs-0.1.1 (c (n "fox-rs") (v "0.1.1") (h "0n693nn2abi7qxrgsp3bvr0i25fglz9b3i5y1xakv8nh1xyrzxgx")))

