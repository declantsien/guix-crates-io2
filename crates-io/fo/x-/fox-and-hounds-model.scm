(define-module (crates-io fo x- fox-and-hounds-model) #:use-module (crates-io))

(define-public crate-fox-and-hounds-model-0.1.0 (c (n "fox-and-hounds-model") (v "0.1.0") (h "0a523mqn1s0ml8jf2g7pdg7n6srzvajd114fd1frc5s0ik4da9ly") (y #t)))

(define-public crate-fox-and-hounds-model-0.1.1 (c (n "fox-and-hounds-model") (v "0.1.1") (h "176fb4x99c5r6p8yiiayj4mmk7sancqznv1gkpv44kk0phkv2zzn") (y #t)))

(define-public crate-fox-and-hounds-model-0.1.2 (c (n "fox-and-hounds-model") (v "0.1.2") (h "143bsy2395ga479yra21hxfw5f5iqlpx2fw0frcm0d1jixjil4j1") (y #t)))

(define-public crate-fox-and-hounds-model-0.1.3 (c (n "fox-and-hounds-model") (v "0.1.3") (h "1j9qckqn4p469ky4x3vdmddfb7v3yizx4qafi4pyw4a5ay2cnxs1") (y #t)))

(define-public crate-fox-and-hounds-model-0.1.4 (c (n "fox-and-hounds-model") (v "0.1.4") (h "1rgw9z059lr40rcljr9v1vp0vzvpgfpb4n3y1akvkya9kr0b7sv6") (y #t)))

(define-public crate-fox-and-hounds-model-0.2.0 (c (n "fox-and-hounds-model") (v "0.2.0") (h "1b4ws9qf79jnmkpyigc2aih9vfblzqzpij6n5mxpa90qng515fp9") (y #t)))

(define-public crate-fox-and-hounds-model-0.2.1 (c (n "fox-and-hounds-model") (v "0.2.1") (h "1vaxp2mqily016b47pw9z858xhcz7qc70w6lamnpxzd5y69zr9nc") (y #t)))

