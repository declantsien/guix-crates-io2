(define-module (crates-io fo ca focaccia) #:use-module (crates-io))

(define-public crate-focaccia-1.0.0 (c (n "focaccia") (v "1.0.0") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1n2v8nhisf4qbaxdbzlyp48jp17kjn50n4mi16g0ic9lims9vphy") (f (quote (("std") ("default" "std"))))))

(define-public crate-focaccia-1.0.1 (c (n "focaccia") (v "1.0.1") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "09356yq837pv1ck8j1c3pcqhd3pd7bg562vynp1zws4916bpaava") (f (quote (("std") ("default" "std"))))))

(define-public crate-focaccia-1.0.2 (c (n "focaccia") (v "1.0.2") (d (list (d (n "version-sync") (r "^0.9, >=0.9.2") (d #t) (k 2)))) (h "1h3y66788cj83xf4gvc6v9qggmsprs3q7jjws4zknxwnnvzc5mbd") (f (quote (("std") ("default" "std"))))))

(define-public crate-focaccia-1.1.0 (c (n "focaccia") (v "1.1.0") (d (list (d (n "version-sync") (r "^0.9, >=0.9.2") (d #t) (k 2)))) (h "1phc5wgygpfcnqpnvi9aqnhfv03hq39bcmvca7ykz8y602h8vsmx") (f (quote (("std") ("default" "std"))))))

(define-public crate-focaccia-1.1.1 (c (n "focaccia") (v "1.1.1") (d (list (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "0q4jh0pny4hgk6m6cgv9jvv477pysa8drziqq9zkkx6fjs8b7q2r") (f (quote (("std") ("default" "std"))))))

(define-public crate-focaccia-1.1.2 (c (n "focaccia") (v "1.1.2") (d (list (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "0zllji0257an038z1dsvxpzi972z0xc53h3mplcl0fdqarwri9c1") (f (quote (("std") ("default" "std"))))))

(define-public crate-focaccia-1.2.0 (c (n "focaccia") (v "1.2.0") (d (list (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "13xvs7wx6riy11apqq4y0wyvmlij8i5yzm9ammpv22jaqxlsgw0b") (f (quote (("std") ("default" "std"))))))

(define-public crate-focaccia-1.3.0 (c (n "focaccia") (v "1.3.0") (d (list (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "0dzqfln7xaprf5qw2knanwpkmla3jx8njn4kbvl2gsl27n8n02ky") (f (quote (("std") ("default" "std"))))))

(define-public crate-focaccia-1.3.1 (c (n "focaccia") (v "1.3.1") (d (list (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "1yafqzbma43z707lfak8qx7yq8d00swbgk84wizvs9rhqn85b4nw") (f (quote (("std") ("default" "std"))))))

(define-public crate-focaccia-1.3.2 (c (n "focaccia") (v "1.3.2") (d (list (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "1gmrazkqb2nzj19ifaz3wdxvzigsf70ijdghrr65sxvh31ff05g4") (f (quote (("std") ("default" "std")))) (r "1.52.0")))

(define-public crate-focaccia-1.4.0 (c (n "focaccia") (v "1.4.0") (d (list (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "03n8rgb7378wxsralkfap1d3g9yi5izgn20fs1fi03479jq1mqcy") (f (quote (("std") ("default" "std")))) (r "1.56.0")))

