(define-module (crates-io fo ss fossable) #:use-module (crates-io))

(define-public crate-fossable-0.1.0 (c (n "fossable") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "01k0j7261qlag7n0yx5ra98b7409wq544gghrljk2g248yvgh1k0")))

(define-public crate-fossable-0.1.1 (c (n "fossable") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0csvawaq7jlqwahimq184l0a73r47rivc55vykdlkfj65aazbmyk")))

(define-public crate-fossable-0.1.2 (c (n "fossable") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y79rs87q0q8c4ih3i3zz8b36q3n9yhysnc55rs2c6qv9jnqnw87")))

