(define-module (crates-io fo ss fossair) #:use-module (crates-io))

(define-public crate-fossair-0.1.0 (c (n "fossair") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.20.2") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0s5pmn6cf5m59gbfvn5dqp55xaqj1hvxp8gi5mjp3dp10a61s8zm") (y #t) (r "1.75.0")))

(define-public crate-fossair-0.0.0 (c (n "fossair") (v "0.0.0") (d (list (d (n "pyo3") (r "^0.20.2") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0jnsqjbhszn9hkgh0v4hp7kdrgprgn1yyxsnaqqjbrjkf0gqa3nc") (r "1.75.0")))

