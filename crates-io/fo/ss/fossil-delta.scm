(define-module (crates-io fo ss fossil-delta) #:use-module (crates-io))

(define-public crate-fossil-delta-0.1.0 (c (n "fossil-delta") (v "0.1.0") (h "1dkspq6ihx73smshk6k7v26c18nd078dlysm232wi9lim0b424zy")))

(define-public crate-fossil-delta-0.1.1 (c (n "fossil-delta") (v "0.1.1") (h "019h91x926jxxgpgswv7lm0ypb41ybzk320f8dg09ylr7mlnb773")))

(define-public crate-fossil-delta-0.1.2 (c (n "fossil-delta") (v "0.1.2") (h "1cwrph521m655d05fmbia0a32my7b22a8prg68h3s8ssd8yr0vh2")))

(define-public crate-fossil-delta-0.1.3 (c (n "fossil-delta") (v "0.1.3") (h "0kz2l16ijb0sha7zqczlmskh1404s50dik1fjri11ljy0mms4kr1")))

(define-public crate-fossil-delta-0.1.4 (c (n "fossil-delta") (v "0.1.4") (h "1mp27m8w68wlcik1ry9y68xcsv0nr8s4pb5n3p2m3ws3nysar2kg")))

(define-public crate-fossil-delta-0.1.5 (c (n "fossil-delta") (v "0.1.5") (h "0fnrwij4h55jpl4yhsm8cgd82a860ndpr0dvszcycpp3mvh2x7p9")))

(define-public crate-fossil-delta-0.1.6 (c (n "fossil-delta") (v "0.1.6") (h "13q7540x908xr6dl62yjazzir8m1jkxy2xy6byqw2i017ngaagyn")))

(define-public crate-fossil-delta-0.2.0 (c (n "fossil-delta") (v "0.2.0") (h "0b72mjn5ml8ykry1p83786n6m4r5nka66g67ajpq1x2zqqnzfhjf")))

