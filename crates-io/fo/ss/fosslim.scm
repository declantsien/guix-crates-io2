(define-module (crates-io fo ss fosslim) #:use-module (crates-io))

(define-public crate-fosslim-0.0.1 (c (n "fosslim") (v "0.0.1") (d (list (d (n "rmp-serde") (r "~0.13.6") (d #t) (k 0)) (d (n "serde") (r "~1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0.11") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.0") (d #t) (k 0)))) (h "1gla4bdknw5rqa8nz4k0j2xxb149nncyh9vzimi4ckjpinw3jh6x")))

(define-public crate-fosslim-0.0.2-alpha (c (n "fosslim") (v "0.0.2-alpha") (d (list (d (n "rmp-serde") (r "~0.13.6") (d #t) (k 0)) (d (n "serde") (r "~1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0.11") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.0") (d #t) (k 0)))) (h "02pl12kk8ih7mbz56pxmw2i4q4cb9gsg2r6cqnzaichkrrdmy74w")))

(define-public crate-fosslim-0.0.2 (c (n "fosslim") (v "0.0.2") (d (list (d (n "rmp-serde") (r "~0.13.6") (d #t) (k 0)) (d (n "seahash") (r "~3.0.5") (d #t) (k 0)) (d (n "serde") (r "~1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0.11") (d #t) (k 0)) (d (n "serde_json") (r "~1.0.0") (d #t) (k 0)))) (h "0jdpvqz6cvx8q4pkn50m98xrnkl0z0v5zsi43c8mq4nra3ash91h")))

