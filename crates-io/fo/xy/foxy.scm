(define-module (crates-io fo xy foxy) #:use-module (crates-io))

(define-public crate-foxy-0.1.0 (c (n "foxy") (v "0.1.0") (d (list (d (n "colored") (r "2.0.*") (d #t) (k 0)) (d (n "env_logger") (r "0.9.*") (d #t) (k 0)) (d (n "glium") (r "0.30.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "tracing") (r "0.1.*") (d #t) (k 0)) (d (n "tracing-subscriber") (r "0.2.*") (d #t) (k 0)) (d (n "tracing-unwrap") (r "0.9.*") (d #t) (k 0)))) (h "0g00jravfvjqjnj176vkyz3rgmndisc5qhlg9f2xp1y3v2adkn3z")))

