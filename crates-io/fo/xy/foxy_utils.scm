(define-module (crates-io fo xy foxy_utils) #:use-module (crates-io))

(define-public crate-foxy_utils-0.2.0 (c (n "foxy_utils") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "glam") (r "^0.25") (d #t) (k 0)) (d (n "quanta") (r "^0.12") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "09aqaiwkg99dhkam719yzhgjhybak36xxzlc3j55m3wdnhs3ki47")))

(define-public crate-foxy_utils-0.3.0 (c (n "foxy_utils") (v "0.3.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "foxy_log") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.25") (d #t) (k 0)) (d (n "quanta") (r "^0.12") (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1gwcp17iyqz54lsjd9kpnvgqbdy4xnp6w93zqn8rcbgpiih3avd5")))

