(define-module (crates-io fo xy foxy_util) #:use-module (crates-io))

(define-public crate-foxy_util-0.1.0 (c (n "foxy_util") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.77") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0x6y40jia3vgn0j4z5gdflcw7b3p3ag2prgsf5scfdxg4612m89l")))

(define-public crate-foxy_util-0.2.0 (c (n "foxy_util") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "arraydeque") (r "^0.5") (d #t) (k 0)) (d (n "quanta") (r "^0.12") (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "13yvclp1hv5c6rwz4yxzx7xig22if2cv7rax65dckgv8950vs7mq")))

