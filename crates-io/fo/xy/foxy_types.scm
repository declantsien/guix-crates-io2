(define-module (crates-io fo xy foxy_types) #:use-module (crates-io))

(define-public crate-foxy_types-0.1.0 (c (n "foxy_types") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.77") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "08v36b54g9ia8dr9rdrv3s9qqfwc5h1ad6908927gy39z3gjn5jm")))

(define-public crate-foxy_types-0.2.0 (c (n "foxy_types") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0j9if6fh28xibq2hfj8n0l2y5wfvk2j5bwynl0p1gg0z5id6wgvq")))

