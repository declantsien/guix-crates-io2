(define-module (crates-io fo xy foxy_time) #:use-module (crates-io))

(define-public crate-foxy_time-1.0.0 (c (n "foxy_time") (v "1.0.0") (d (list (d (n "quanta") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "15rr430f1xp0sbwf6ha00wfmnrj9adhyn64xfd11lyhq5qn2xlvy")))

(define-public crate-foxy_time-1.1.0 (c (n "foxy_time") (v "1.1.0") (d (list (d (n "quanta") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "11dw6qzbk2sw1a7lzj3xilxs3i00vw4rfpnvyi1c2r6xj5jz2sxm")))

