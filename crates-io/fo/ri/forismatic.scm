(define-module (crates-io fo ri forismatic) #:use-module (crates-io))

(define-public crate-forismatic-0.1.0 (c (n "forismatic") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0wz74w88h16md9mjx820hsj1l3myv8jvi0174xzl9y6ljgld651a")))

(define-public crate-forismatic-0.2.0 (c (n "forismatic") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vmvmhgi2d2lasw4zyhf9ggw624n2bz4dqx7xcsx19a4h92p1y01")))

(define-public crate-forismatic-0.3.0 (c (n "forismatic") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)) (d (n "tokio") (r "^0.2.19") (d #t) (k 0)))) (h "09cvybdmvn25dfh48nwgy936pj12vb78kvx308rcgsbfrcjx6y1h") (y #t)))

(define-public crate-forismatic-0.3.1 (c (n "forismatic") (v "0.3.1") (d (list (d (n "reqwest") (r "^0.10.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.51") (d #t) (k 0)))) (h "1c3p8jswnsw2sla6bw9daiff6bm0xnv981ivkqsg95lkpppryam1")))

