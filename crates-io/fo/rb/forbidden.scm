(define-module (crates-io fo rb forbidden) #:use-module (crates-io))

(define-public crate-forbidden-0.1.0 (c (n "forbidden") (v "0.1.0") (d (list (d (n "argon2") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "password-hash") (r "^0.3.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "scrypt") (r "^0.8.0") (d #t) (k 0)) (d (n "unicase") (r "^2.6.0") (d #t) (k 0)))) (h "0xk1vcgp48gsy9p9d5mr9iri7aqr0zskh6pg29xqcj036xdzbna6") (f (quote (("use_scrypt") ("use_argon") ("recommend" "use_argon"))))))

(define-public crate-forbidden-0.1.1 (c (n "forbidden") (v "0.1.1") (d (list (d (n "argon2") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "password-hash") (r "^0.3.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "scrypt") (r "^0.8.0") (d #t) (k 0)) (d (n "unicase") (r "^2.6.0") (d #t) (k 0)))) (h "0x4zbchcxiia409mw3z1mgzlyil5gzg3h18nrb0bxcn7ykh3zhqk") (f (quote (("use_scrypt") ("use_argon") ("recommend" "use_argon"))))))

(define-public crate-forbidden-0.1.2 (c (n "forbidden") (v "0.1.2") (d (list (d (n "argon2") (r "^0.3") (f (quote ("std"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "password-hash") (r "^0.3.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "scrypt") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "sqlite3") (r "^0.24.0") (d #t) (k 2)))) (h "1y2sxk56j8ikwwwbjlzd2h34q1fsq0a1llmpvlkkarr75far8i4l") (f (quote (("use_scrypt" "scrypt")))) (y #t)))

(define-public crate-forbidden-0.1.3 (c (n "forbidden") (v "0.1.3") (d (list (d (n "argon2") (r "^0.3.1") (f (quote ("std"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "password-hash") (r "^0.3.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.3") (d #t) (k 0)) (d (n "scrypt") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "sqlite3") (r "^0.24.0") (d #t) (k 2)))) (h "1c5mi30x2pr6y0si8qvmk1ahf2i5pzflk2i7ynhjmmhs3mryrdra") (f (quote (("use_scrypt" "scrypt"))))))

