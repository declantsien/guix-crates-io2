(define-module (crates-io fo rb forbidden-bands) #:use-module (crates-io))

(define-public crate-forbidden-bands-0.1.0 (c (n "forbidden-bands") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rid6hh0c1l8blsad1bvkjj24cdrlslnm59m9gj0wb4q89y9mvh0") (f (quote (("external-json"))))))

(define-public crate-forbidden-bands-0.1.1 (c (n "forbidden-bands") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1aazc37p9i4chp1xknmiv98gy5kymdx88hh0k7adbyl4nd69lb6m") (f (quote (("external-json"))))))

