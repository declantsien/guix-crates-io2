(define-module (crates-io fo li folio) #:use-module (crates-io))

(define-public crate-folio-0.0.0 (c (n "folio") (v "0.0.0") (h "11rcw5sffrc3hyb5siw7pwpvv2k627zjr1vjij9ar5788da2qvgi") (y #t)))

(define-public crate-folio-0.0.1 (c (n "folio") (v "0.0.1") (h "072p351z779irk03vhn0j78yk9s88q7bm5l7p7kmnf4vpbq9w6f8") (y #t)))

