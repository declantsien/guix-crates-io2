(define-module (crates-io fo rv forvo-cli) #:use-module (crates-io))

(define-public crate-forvo-cli-0.1.0 (c (n "forvo-cli") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "requests") (r "^0.0.30") (d #t) (k 0)))) (h "0djbsqncl3c3d1dbbwc7gl2v0nzr1n68b4hs9qcn9afvfcxqsmhb")))

(define-public crate-forvo-cli-0.1.1 (c (n "forvo-cli") (v "0.1.1") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "requests") (r "^0.0.30") (d #t) (k 0)))) (h "0rp39wcjgcd92mzvqa7dx9v67pf6cdwh85sjg0hw8cb5zvd54vxs")))

(define-public crate-forvo-cli-0.1.2 (c (n "forvo-cli") (v "0.1.2") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)))) (h "0124lqgh9pg6p3rfq2vc3xj2ljc8dxa5kczjaikg4nmag8kr9pfn")))

(define-public crate-forvo-cli-0.1.3 (c (n "forvo-cli") (v "0.1.3") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)))) (h "15bx17s4hcd86fggq3392wxg6rl8dyjwn1lcrgi0jgj92q4z20ib")))

