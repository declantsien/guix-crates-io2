(define-module (crates-io fo ra foraget) #:use-module (crates-io))

(define-public crate-foraget-0.1.0 (c (n "foraget") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "1ppvh7ijaad84imlwd7qs0zi26yaha0j423s20wyjfy3vq96f2f2")))

(define-public crate-foraget-0.1.1 (c (n "foraget") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "0p64vmmdvnbnyhx7w1d0pba0hphhs08jshxl7n6srazhpazl066g")))

(define-public crate-foraget-0.1.2 (c (n "foraget") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "0zd6mh1v2wd4dywqmam534xvjjgf1h4d0m0yfpgdsx9c99mv4d3z")))

(define-public crate-foraget-0.1.3 (c (n "foraget") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "107wdpr44y06gnb066xp4r39q3g3a8q84wg607rnhrf6w4nlhw57")))

