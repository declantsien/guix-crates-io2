(define-module (crates-io fo rc force_graph) #:use-module (crates-io))

(define-public crate-force_graph-0.1.0 (c (n "force_graph") (v "0.1.0") (d (list (d (n "petgraph") (r "^0.5.0") (d #t) (k 0)))) (h "0038mzvw1qbyamfk4kazlz1cv1hchkn1nswrwylj706wac2g6705")))

(define-public crate-force_graph-0.1.1 (c (n "force_graph") (v "0.1.1") (d (list (d (n "petgraph") (r "^0.5.0") (d #t) (k 0)))) (h "09ikc2fx1dwffd5rlfywkh6z11p94lmhgijvxf03xyjshcvbrn4d")))

(define-public crate-force_graph-0.2.0 (c (n "force_graph") (v "0.2.0") (d (list (d (n "petgraph") (r "^0.5") (d #t) (k 0)))) (h "0rbh5adkdxzdfrcph6kp58lipg3msr7fjj5v623srlsvazlkd9if")))

(define-public crate-force_graph-0.3.0 (c (n "force_graph") (v "0.3.0") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 2)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)))) (h "0vgr06lq8cc4mc3vqpjfx8d1f5a9rsprrmw6qan5scxbnjxj1bby")))

(define-public crate-force_graph-0.3.1 (c (n "force_graph") (v "0.3.1") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 2)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)))) (h "03is0rb0dgirrx5hh437w5shl2k5d8xbc8gc8lfhyg20infb0a1l")))

(define-public crate-force_graph-0.3.2 (c (n "force_graph") (v "0.3.2") (d (list (d (n "macroquad") (r "^0.3") (d #t) (k 2)) (d (n "petgraph") (r "^0.5.1") (d #t) (k 0)))) (h "088h9g94j19bwc5jdv8bjxfnpl01cz6r5aaa98ll3mblam2zfxm3")))

