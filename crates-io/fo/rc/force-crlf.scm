(define-module (crates-io fo rc force-crlf) #:use-module (crates-io))

(define-public crate-force-crlf-0.1.0 (c (n "force-crlf") (v "0.1.0") (h "0aviwflzmmq1kd1qxlcmhndq22jz4qhci9vv5hy3wjdnn5ykv364")))

(define-public crate-force-crlf-0.1.1 (c (n "force-crlf") (v "0.1.1") (h "1jszjx4asnj87m438i99zhci2bi4xrl49w69265wa0rqg0lpcdmr")))

(define-public crate-force-crlf-0.1.2 (c (n "force-crlf") (v "0.1.2") (h "1xm5k8z4hg87dx1f8wk4bcsm9gk6mbrpcfim2kc8g2zhcc6qvkwf")))

