(define-module (crates-io fo rc forcast-async) #:use-module (crates-io))

(define-public crate-forcast-async-0.1.0 (c (n "forcast-async") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.35") (d #t) (k 0)))) (h "1ig89c9vi5fpd7yxi5b215z2b7pk3x4l1bmwkn47qh5mcx1f5iig")))

