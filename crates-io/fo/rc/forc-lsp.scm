(define-module (crates-io fo rc forc-lsp) #:use-module (crates-io))

(define-public crate-forc-lsp-0.10.0 (c (n "forc-lsp") (v "0.10.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0jyal8q5dv8jvh934w639s10j5zl08q5xfbvirb1944dz19x8m77")))

(define-public crate-forc-lsp-0.10.1 (c (n "forc-lsp") (v "0.10.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.10.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1w74dpki36jy65rsa40i10rwvrii45gk9c58fpq4q3dd22ldd8vx")))

(define-public crate-forc-lsp-0.10.3 (c (n "forc-lsp") (v "0.10.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.10.3") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0fcj1c4gkdrkzpbckcknmbwrqpy1pljpm0799wcra8srnpwhsgk5")))

(define-public crate-forc-lsp-0.11.0 (c (n "forc-lsp") (v "0.11.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.11.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1dbbnjyw5jcxcqaqs9l45yj1nybk0xjic1svfk6c9h9vg7nzwhpw")))

(define-public crate-forc-lsp-0.12.1 (c (n "forc-lsp") (v "0.12.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0d0lxipbv6hb80wfz0gm976l62a91lz31pjggp10sy7b7yjhqc51")))

(define-public crate-forc-lsp-0.12.2 (c (n "forc-lsp") (v "0.12.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.12.2") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1lc5jm0sh8qpzwq190f4b9lzmynwvi7b43mqfqmk0pdc2wy4rknn")))

(define-public crate-forc-lsp-0.13.0 (c (n "forc-lsp") (v "0.13.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.13.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0hq1sf93d313lrkx8gariwbs21h1ng342g5xkzzkhp14l71kq0fj")))

(define-public crate-forc-lsp-0.13.1 (c (n "forc-lsp") (v "0.13.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.13.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1nvfrr3v7vgf3f3xlbqxn0yd10v8k3l7v1ym5m95vbcscl7r5cjl")))

(define-public crate-forc-lsp-0.13.2 (c (n "forc-lsp") (v "0.13.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.13.2") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1rn0qxh8h8pib521mmc1n9agm2sx8pfb0j36izzl7lh81mmch33q")))

(define-public crate-forc-lsp-0.14.0 (c (n "forc-lsp") (v "0.14.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.14.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0wh45xldzld51hg1cisnhhh0ii4sl9hcb9c6qy5hizawp4a4yn1n")))

(define-public crate-forc-lsp-0.14.1 (c (n "forc-lsp") (v "0.14.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.14.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "04yz4f3m0c70335320xwd3rv3f4dl2swd4wq8g4kg99iqs0nlxm1")))

(define-public crate-forc-lsp-0.14.2 (c (n "forc-lsp") (v "0.14.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.14.2") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1mkjl0sasdl44y1q4gnvlwfpzwr8nnyaqxwvjknl8k80kzw830gn")))

(define-public crate-forc-lsp-0.14.3 (c (n "forc-lsp") (v "0.14.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.14.3") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1nwf2c2jlndp28968dkx099yx4xs8dr00yrybp2z7jm24b5svy8i")))

(define-public crate-forc-lsp-0.14.4 (c (n "forc-lsp") (v "0.14.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.14.4") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "10gh2rgv98fdh6kcaknzq88isw0lz9ph8fwq4qgmhnc2m9zdckm3")))

(define-public crate-forc-lsp-0.14.5 (c (n "forc-lsp") (v "0.14.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.14.5") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1yjnmvn0wqj4yimrk3vqgslsbrvfb6akhr56mraafx8zyfcc8sz8")))

(define-public crate-forc-lsp-0.15.1 (c (n "forc-lsp") (v "0.15.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.15.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1amnrdr10ahfgdaihcs95jnv7d8avfmyfipk4j25vqndbmkykh50")))

(define-public crate-forc-lsp-0.15.2 (c (n "forc-lsp") (v "0.15.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.15.2") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1g75c8qz9p41x15scxp5nraykmy04sgbzm3z54lz22lj752qfqg0")))

(define-public crate-forc-lsp-0.16.0 (c (n "forc-lsp") (v "0.16.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.16.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0zixi7zwwzwpp60wxg7z6i771jvbcz9la9g8s3xphws1xbakpbpw")))

(define-public crate-forc-lsp-0.16.1 (c (n "forc-lsp") (v "0.16.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.16.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0alrcbmwadvfwyhxw5abwcdh2jr6yf72md9xljfwmgck7jvgr6xg")))

(define-public crate-forc-lsp-0.16.2 (c (n "forc-lsp") (v "0.16.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.16.2") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "15204nrh7i5xmisg8pfb6qgick3hrrfc6r9zmbbidhzrlr0j7p7q")))

(define-public crate-forc-lsp-0.17.0 (c (n "forc-lsp") (v "0.17.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.17.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0n876d74b2vbvjq7s2h1fgw8cwm21wazgjwbvymxl60gqlwgsn7l")))

(define-public crate-forc-lsp-0.18.0 (c (n "forc-lsp") (v "0.18.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.18.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1x9g7dps5ljxqywbcjmly1i237ii9xm0ssjfphaciqkb7p4didzg")))

(define-public crate-forc-lsp-0.18.1 (c (n "forc-lsp") (v "0.18.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.18.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "04k4rpfq08f6lmkiqsxwy47vj9rcdivbgdhk65b4xj5z6095v4qg")))

(define-public crate-forc-lsp-0.19.0 (c (n "forc-lsp") (v "0.19.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.19.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "14cprfma6jhb1z61zkgglgk5zcjx1m6gs6fdrbal74cd07jn54p2")))

(define-public crate-forc-lsp-0.19.1 (c (n "forc-lsp") (v "0.19.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.19.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1ap83mwz6g7a20cwn7r45ca22pw1rq4qb0mjc2yam80zysvzgvd1")))

(define-public crate-forc-lsp-0.19.2 (c (n "forc-lsp") (v "0.19.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.19.2") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0vcnfb3c0lk3x11gd4982srvrxda9104ab1lf7lakppx46q5mv8y")))

(define-public crate-forc-lsp-0.20.1 (c (n "forc-lsp") (v "0.20.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.20.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0qwwczijp2vl1nb554hrl1ln9br82qhqah1f0xwqyaamx0rxmg3p")))

(define-public crate-forc-lsp-0.20.2 (c (n "forc-lsp") (v "0.20.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.20.2") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1mrfhjcg6hrq89ajwln9c92ih3jlwbm0kh684wpj4ld19zmgyb3g")))

(define-public crate-forc-lsp-0.21.0 (c (n "forc-lsp") (v "0.21.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.21.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "15cpnbgdbmnaycjmfbinpk6xh966lhz4g2yyvh0jbx34sam6333c")))

(define-public crate-forc-lsp-0.22.0 (c (n "forc-lsp") (v "0.22.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.22.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1mamxf7fwjqx95shn0vv9nf8i6i6fqh0knf6fz9i2pay912d8ps3")))

(define-public crate-forc-lsp-0.22.1 (c (n "forc-lsp") (v "0.22.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.22.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1b7a4kb4v2m3yxrw26sy6am4yxyx9hs6xmaycq59jlv2n1f3klhg")))

(define-public crate-forc-lsp-0.23.0 (c (n "forc-lsp") (v "0.23.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.23.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "09bmrx0w2hkzpy2z8jqay9jbvxl1yfknisni23xmdk1kqmswci5s")))

(define-public crate-forc-lsp-0.24.0 (c (n "forc-lsp") (v "0.24.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.24.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "00kcn2kl0431fv8acf6kaw7hhm11l5hmy9l36i0va8ax35l39qb6")))

(define-public crate-forc-lsp-0.24.1 (c (n "forc-lsp") (v "0.24.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.24.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "09nrfd7ifa6jwans2py72yq93ib51hk6rwix7jj1081abjf53jrf")))

(define-public crate-forc-lsp-0.24.2 (c (n "forc-lsp") (v "0.24.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.24.2") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "11w56hx6h4i6ap9iwr8368q7712xxlqylgdda9gxg27agzjm2d3j")))

(define-public crate-forc-lsp-0.24.3 (c (n "forc-lsp") (v "0.24.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.24.3") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0bq5yn3dg96i86jfdj7zc2zws25lvfixz6lh2wlb2ib8ms95jy41")))

(define-public crate-forc-lsp-0.24.4 (c (n "forc-lsp") (v "0.24.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.24.4") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0xk2xs5cq3ar7klsyzx2ba37gjsk5hgbmsbnk6h4vcw8y8z9pim7")))

(define-public crate-forc-lsp-0.24.5 (c (n "forc-lsp") (v "0.24.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.24.5") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "16ibxvxk031l8v3fs0fl42fyziz6v1yi03ni2a4h2vavrfk9zca3")))

(define-public crate-forc-lsp-0.25.0 (c (n "forc-lsp") (v "0.25.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.25.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1axdrim7i723mwjlnlbkb7d223xjzl96agqfa7rrd31z1qqr4fj8")))

(define-public crate-forc-lsp-0.25.1 (c (n "forc-lsp") (v "0.25.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.25.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0cspz5z2jv0akkjcm410q14r5a5zvvqpf6887v7l6mrh1r1ryndv")))

(define-public crate-forc-lsp-0.25.2 (c (n "forc-lsp") (v "0.25.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.25.2") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1726ack4ivdfchlpsm8iad07sg9i4h9q2s5i1ylv6zz09gzac75k")))

(define-public crate-forc-lsp-0.26.0 (c (n "forc-lsp") (v "0.26.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.26.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0h5117i9205qiq9a7v28q6231qjdv08hkp41l0jmlkcy4hsjlinq")))

(define-public crate-forc-lsp-0.27.0 (c (n "forc-lsp") (v "0.27.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.27.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0gfb6b0dc2v216fr27vbwizmf66cdrcqq8dxw0vkim7pxiqb8r5a")))

(define-public crate-forc-lsp-0.28.0 (c (n "forc-lsp") (v "0.28.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.28.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0xiwiqflddslpg9dymrgxvl7hh56b3gazc8rc442r28kc3nl1qsc")))

(define-public crate-forc-lsp-0.28.1 (c (n "forc-lsp") (v "0.28.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.28.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1s5m9psznk9li7k95n0jny5yj1aq1vv791gvs5fj1ifl19vf0bfy")))

(define-public crate-forc-lsp-0.29.0 (c (n "forc-lsp") (v "0.29.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.29.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0hj914kv421zasrqdf3hr2ddn6ymsdfz1yjcmwcrvn69cljrd5fn")))

(define-public crate-forc-lsp-0.30.0 (c (n "forc-lsp") (v "0.30.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.30.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1q9smxl7adpscpwjx5nmnkn5h9ah9brk7vma1wb894j0cm38kas2")))

(define-public crate-forc-lsp-0.30.1 (c (n "forc-lsp") (v "0.30.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.30.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "03nfy0z2ypl183npvp7r77yxbqhs7dypaxwa4vpg941mdxba6k6l")))

(define-public crate-forc-lsp-0.31.0 (c (n "forc-lsp") (v "0.31.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.31.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1x1619kp4pbd7bk24642avkgr2w4xpr8jdsjifx9l9cv337450ib")))

(define-public crate-forc-lsp-0.31.1 (c (n "forc-lsp") (v "0.31.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.31.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1n3gjspdgkdxwvbj65965k7f22glwc8zhvx8nrqvp3jk2pj79gbk")))

(define-public crate-forc-lsp-0.31.2 (c (n "forc-lsp") (v "0.31.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.31.2") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1dalmh9nfkrqz7746bxsi75fxnq35m0a3rmssyzryp0zx0d2q0br")))

(define-public crate-forc-lsp-0.31.3 (c (n "forc-lsp") (v "0.31.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.31.3") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1zjrh1f9bqgw59wfqbzg1j94fl92974ci058lv4zw8gpr6fw8q70")))

(define-public crate-forc-lsp-0.32.1 (c (n "forc-lsp") (v "0.32.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.32.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "09ciw5mi9jk69cjks73qbx1mjgirjszqzp6mvdpyb7mdjvwzpia8")))

(define-public crate-forc-lsp-0.32.2 (c (n "forc-lsp") (v "0.32.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.32.2") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "19miy66ldr0fgzqwqi8gdsw3j4fkvwxj8zsq9qszajlm45b4s07d")))

(define-public crate-forc-lsp-0.33.0 (c (n "forc-lsp") (v "0.33.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.33.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1y23y2sifgpy6g07q88s40i94rz8mkazy7p8q4wcnzx6x5fsa2i7")))

(define-public crate-forc-lsp-0.33.1 (c (n "forc-lsp") (v "0.33.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.33.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0d3s6xaqgyxpvb6ngjv5vz1mvmp5xlbjgdzvfvlnlz9nxqbyd3ka")))

(define-public crate-forc-lsp-0.34.0 (c (n "forc-lsp") (v "0.34.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.34.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1ncsmk6yfcxc8ii5ni66rnzq0wp536bflikr3gcc9vcv9i3lk3j7")))

(define-public crate-forc-lsp-0.35.0 (c (n "forc-lsp") (v "0.35.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.35.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "07zr674616dd7q3zlip18dyrjpbr8rlanjav8xxb16zfcapaa3wz")))

(define-public crate-forc-lsp-0.35.1 (c (n "forc-lsp") (v "0.35.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.35.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1mg1x4glzp4vl8wcfc64l2h50dwypp2jw17yd5gcjgwb04xh5k77")))

(define-public crate-forc-lsp-0.35.2 (c (n "forc-lsp") (v "0.35.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.35.2") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0z7yx6d5abipxyg45m0zpr9ip54fw257rs0grzg7sdmlpw5vg1rd")))

(define-public crate-forc-lsp-0.35.3 (c (n "forc-lsp") (v "0.35.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.35.3") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1whmy9nn835y2khdpk1fs8x6vbn6vyqkw742n0pk1rc23gcc77y2")))

(define-public crate-forc-lsp-0.35.4 (c (n "forc-lsp") (v "0.35.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.35.4") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1svlsgwiynhvcapm50kr60aqswcx1ns4gdpwwx2fbw3g96z9ssdg")))

(define-public crate-forc-lsp-0.35.5 (c (n "forc-lsp") (v "0.35.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.35.5") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0nbjxkpjqf5nwwb47m97n3wfkfiif9y6jmmnnzag0n4zb9pxs0iq")))

(define-public crate-forc-lsp-0.36.0 (c (n "forc-lsp") (v "0.36.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.36.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1f9l2rd84ws0blf1ygyddqcqp3wdycldm610n55n3qz7w4qn3h6l")))

(define-public crate-forc-lsp-0.36.1 (c (n "forc-lsp") (v "0.36.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.36.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0nkc40h2q64krmmc35s7vnp55zgyv2kf7f9r5ff7r7b0crdsl5qs")))

(define-public crate-forc-lsp-0.37.0 (c (n "forc-lsp") (v "0.37.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.37.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1sh5kmwqlz72inak9sxj1hjgch33i4k9ydqygay3nign3isac9a9")))

(define-public crate-forc-lsp-0.37.1 (c (n "forc-lsp") (v "0.37.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.37.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "17hvdp6hwpzcasqygvnjfawj8dri0v580v94c98s35yi4rf357mk")))

(define-public crate-forc-lsp-0.37.2 (c (n "forc-lsp") (v "0.37.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.37.2") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0sk65hxaw8ndn592012558w2rhphijig0qwnyc6jdqnrviab59np")))

(define-public crate-forc-lsp-0.37.3 (c (n "forc-lsp") (v "0.37.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.37.3") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "181f8is9arv1qmd155kwjilyxvcxf16h889724lwnxwq8lkl431j")))

(define-public crate-forc-lsp-0.38.0 (c (n "forc-lsp") (v "0.38.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.38.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1ivb42kww1bfhzh8wz99092ag1znjfdimzbr8v8xwl2bprnj172f")))

(define-public crate-forc-lsp-0.39.0 (c (n "forc-lsp") (v "0.39.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.39.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "15xdin8xlkz4zrvmdvrb2criygrpj756h7gdb2ivix9bkfkwxz4s")))

(define-public crate-forc-lsp-0.39.1 (c (n "forc-lsp") (v "0.39.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.39.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0w387bvpbpvmnwsmcwzkcw964lsv1s914xddqhnfbz295dgvk4ry")))

(define-public crate-forc-lsp-0.40.0 (c (n "forc-lsp") (v "0.40.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.40.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0884zdz2gizmc5nwxh2d43s84yd2ql5a1qsacricisnw46jbwmj8")))

(define-public crate-forc-lsp-0.40.1 (c (n "forc-lsp") (v "0.40.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.40.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "09d1msp5wzay5qcx97fsjw6zid1g0kpylgqq0596sfw9l42bcbq8")))

(define-public crate-forc-lsp-0.41.0 (c (n "forc-lsp") (v "0.41.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.41.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1j3sd4j0p1dvkz5s79cghyyqihwcfa83y2ygj7hn5m40wpg8q194")))

(define-public crate-forc-lsp-0.42.0 (c (n "forc-lsp") (v "0.42.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.42.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0bqv7j0swihr1mz072bbrbr1inyk8ziphi7gq2ab07544012zm7h")))

(define-public crate-forc-lsp-0.42.1 (c (n "forc-lsp") (v "0.42.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.42.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "123910j78dvgrnxpa50br88c7qbb4rmfmqp5lx0anm5q1lqncqgf")))

(define-public crate-forc-lsp-0.43.0 (c (n "forc-lsp") (v "0.43.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.43.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "17d3n1lnpd4fsvhd3syad0f760vc98i7naiad01yss2myfys7bxz")))

(define-public crate-forc-lsp-0.43.2 (c (n "forc-lsp") (v "0.43.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.43.2") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "170zgylvh09m8jwhhc1ns6s5w61a0zczz3adf5q9y93as6q3ijbi")))

(define-public crate-forc-lsp-0.44.0 (c (n "forc-lsp") (v "0.44.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.44.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "161yd3d4p2qp4qgq49kw2jka2fwjn21nxsd9wxizwmxysp98axr6")))

(define-public crate-forc-lsp-0.44.1 (c (n "forc-lsp") (v "0.44.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.44.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1p1yx7xmcik66bvcmw2ah4j5civ537yc17yacln35blhl2dl5vb8")))

(define-public crate-forc-lsp-0.45.0 (c (n "forc-lsp") (v "0.45.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.45.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "06nwp12l5k97v3fq69f5q8lpi3jphvkwqc1f18m3kdjb3w70ka0s")))

(define-public crate-forc-lsp-0.46.0 (c (n "forc-lsp") (v "0.46.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.46.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1561zyc3phcl9jrl3y7xajfrhqy7qd15crkzawnvbvngw0yblcmq")))

(define-public crate-forc-lsp-0.46.1 (c (n "forc-lsp") (v "0.46.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.46.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1gh2nhq30ymzis78fmcdpccrz3fahv0lkl4m6zkqs8nc12435p07")))

(define-public crate-forc-lsp-0.47.0 (c (n "forc-lsp") (v "0.47.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.47.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0v7v85pbvyw016zyy6xkwxy8mv0c5nfjl665ixq4qk0g4hhr4gxz")))

(define-public crate-forc-lsp-0.48.0 (c (n "forc-lsp") (v "0.48.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.48.0") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "08135dni2s40v4x9rcsgm8pglggpgmpi641ykfb5l2hfss98m2c7")))

(define-public crate-forc-lsp-0.48.1 (c (n "forc-lsp") (v "0.48.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.48.1") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0gk8yd8qwbnazrg79mmmcpf417xnr6a1wavgcwk227zd57bfyip6")))

(define-public crate-forc-lsp-0.49.1 (c (n "forc-lsp") (v "0.49.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.49.1") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "12bf0gkjxn1mb4760ib3kq1ax617jw4brm3lqvl32kxy0nr4a9iz")))

(define-public crate-forc-lsp-0.49.2 (c (n "forc-lsp") (v "0.49.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.49.2") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1izsfh6pn1x1vnfyxkva53q1qrf66i4anfh6gbssi042dvb4l6ki")))

(define-public crate-forc-lsp-0.50.0 (c (n "forc-lsp") (v "0.50.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.50.0") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0lbia2v8s6j4hz9da9bj2kwc3v70zkz141rhbjwl1i3b98bd6wbv")))

(define-public crate-forc-lsp-0.51.0 (c (n "forc-lsp") (v "0.51.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.51.0") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "13knx90lhb12xzbdwq1spsz3jrh31ksnfig76svibc5inhv502lk")))

(define-public crate-forc-lsp-0.51.1 (c (n "forc-lsp") (v "0.51.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.51.1") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "009zwh0kdngi5pw5aiwf4z6vwk8q6zfi0s2zwc5lmhzm0v07zg53")))

(define-public crate-forc-lsp-0.49.3 (c (n "forc-lsp") (v "0.49.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.49.3") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0bx9vq01m1bn733njrcg3k9n5q8afyfxjfxd58lgwmxv7cwvmwfh")))

(define-public crate-forc-lsp-0.52.0 (c (n "forc-lsp") (v "0.52.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.52.0") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "079y4wbmq8r1fqx3w4bl78r8pmkjf69zii3svm35cjna5aay7lwk")))

(define-public crate-forc-lsp-0.52.1 (c (n "forc-lsp") (v "0.52.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.52.1") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "134lyq9g52hh0qrs3zzrn4hwyljazpk5wxi84jp1hf9scx00wi38")))

(define-public crate-forc-lsp-0.53.0 (c (n "forc-lsp") (v "0.53.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.53.0") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "18k9qkacpphmdamiaw6a82fwb83xhyqvqz9znd9kv0jx1fj6wazf")))

(define-public crate-forc-lsp-0.54.0 (c (n "forc-lsp") (v "0.54.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.54.0") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0z5xhz6qj888c34bsn3zc8w9npzac7iyv55j01f2acjnp280g4lw")))

(define-public crate-forc-lsp-0.55.0 (c (n "forc-lsp") (v "0.55.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.55.0") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0gjykj0wm9y6pmx89np0hfpk87yimhxg14cngggq8xxr8gmfy60s")))

(define-public crate-forc-lsp-0.56.0 (c (n "forc-lsp") (v "0.56.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.56.0") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0blvbjshb4204ylq0vr804lflxgnk65454ws4mq6x3wlrdvif1xj")))

(define-public crate-forc-lsp-0.56.1 (c (n "forc-lsp") (v "0.56.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.56.1") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "17i0nmizrpqjxb4n4ai01lpg9xi5v8w8n1wsl0rfafzdkhqwcc88")))

(define-public crate-forc-lsp-0.57.0 (c (n "forc-lsp") (v "0.57.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.57.0") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "1v1ik04z3imzbglgklhm10dzjrbsxy4b7synwac8sc9rbi0620kj")))

(define-public crate-forc-lsp-0.58.0 (c (n "forc-lsp") (v "0.58.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.58.0") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0q3p2yzcsfc8hkgx79x8kj0725zg74wiqspvm23xkzmm9fqqrj59")))

(define-public crate-forc-lsp-0.59.0 (c (n "forc-lsp") (v "0.59.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.59.0") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "01hzji27mvasry7ban6yc8ip39gxv1h2qcw3fvyi0ds9biy1c679")))

(define-public crate-forc-lsp-0.60.0 (c (n "forc-lsp") (v "0.60.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sway-lsp") (r "^0.60.0") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.8") (d #t) (k 0)))) (h "0anvp96xz80pn6r5vxdnpdhk8mvph8gpwjhnrm8ckdx399sxvrwf")))

