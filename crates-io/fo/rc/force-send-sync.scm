(define-module (crates-io fo rc force-send-sync) #:use-module (crates-io))

(define-public crate-force-send-sync-1.0.0 (c (n "force-send-sync") (v "1.0.0") (h "1dlmqiwwsjjanxi8a2hg83bi2jy562z50y9cbd2m9h43x3a8i4ax")))

(define-public crate-force-send-sync-1.1.0 (c (n "force-send-sync") (v "1.1.0") (h "1dpy69psypanm8mx3k0mjmvb0mccyyd8yffcdr1899la8k68ss1j")))

