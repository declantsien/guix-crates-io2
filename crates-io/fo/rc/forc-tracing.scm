(define-module (crates-io fo rc forc-tracing) #:use-module (crates-io))

(define-public crate-forc-tracing-0.28.0 (c (n "forc-tracing") (v "0.28.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "1pwnyrinzqxlh3l7f4kpy96gzasvjnxsdc4jar02iwhi0k4vcxzq")))

(define-public crate-forc-tracing-0.28.1 (c (n "forc-tracing") (v "0.28.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0z98p47f5gwf5sq42404l90n4m76d8vigklwf9345gp952rh28fg")))

(define-public crate-forc-tracing-0.29.0 (c (n "forc-tracing") (v "0.29.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "1yjwcrwjsk4ky3yk9j5f2amxzb0rp2sjm2lkqyvnyprgibllq7b7")))

(define-public crate-forc-tracing-0.30.0 (c (n "forc-tracing") (v "0.30.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "1fx40qh5myhc2fb9n1qmb20sp1qbwq2hji7jnx4gyhlvypi3arx1")))

(define-public crate-forc-tracing-0.30.1 (c (n "forc-tracing") (v "0.30.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "008z6rb7ak3qdbnf6176v0rs8qbmyvlz7rqwvca6davp8zd9j11x")))

(define-public crate-forc-tracing-0.31.0 (c (n "forc-tracing") (v "0.31.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0nklg0lzvndl6rxaq5psav9af1c7v4s2wvv9i0pr9qzcj69c297j")))

(define-public crate-forc-tracing-0.31.1 (c (n "forc-tracing") (v "0.31.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "07ahrs5afr2iwz55bwm07z3ypv84c0br4xh7c30zaww338zkz692")))

(define-public crate-forc-tracing-0.31.2 (c (n "forc-tracing") (v "0.31.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "1q8ijwvb9llkfifcmi0jqh4dxd9pp0wv2xhm4bay2d3fv8qij5hg")))

(define-public crate-forc-tracing-0.31.3 (c (n "forc-tracing") (v "0.31.3") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "18fh09p8f1g57skf73hpnchkl88y67y2qa9v22aknz1qg1i8n4ky")))

(define-public crate-forc-tracing-0.32.0 (c (n "forc-tracing") (v "0.32.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "1091lv2fnhji3lh2zgfvd9x4v1xmfka312bdfs9blrchwkkqmg97")))

(define-public crate-forc-tracing-0.32.1 (c (n "forc-tracing") (v "0.32.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "1q30h3pnk5flca6jgvlgp0sqhyq6yi7l9kwvjnrhd5s4dpz66x3v")))

(define-public crate-forc-tracing-0.32.2 (c (n "forc-tracing") (v "0.32.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0j3a444p5skh34092143r0cnhk4q7nwnzpdsk9js1xlfdkjp6lqx")))

(define-public crate-forc-tracing-0.33.0 (c (n "forc-tracing") (v "0.33.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0f7dl7f3n61w6nmy0lflagc1c38lgbk1fchkvf8cancxyfbfl9gv")))

(define-public crate-forc-tracing-0.33.1 (c (n "forc-tracing") (v "0.33.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "1k74cj0c5ncayb2jgy40x07hcqirki5yghzmh60hbs0ilrimnwf5")))

(define-public crate-forc-tracing-0.34.0 (c (n "forc-tracing") (v "0.34.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "147gq14l9dl15fz9x99jkq6fviqrnk2i1z7glfnr6gh9524k4vl0")))

(define-public crate-forc-tracing-0.35.0 (c (n "forc-tracing") (v "0.35.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0c6g3k3d7c7qc244yg7d38m18y6311gah5cjk45zzla7wxd7nrr6")))

(define-public crate-forc-tracing-0.35.1 (c (n "forc-tracing") (v "0.35.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "12rkmkn3xbwcg0lkcww4q6s8q3hak8fs6w11fcf3440hra0inp98")))

(define-public crate-forc-tracing-0.35.2 (c (n "forc-tracing") (v "0.35.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0d9h80725x2cxk2kz3h52kphfny9qykkl329ksxb49sblwz9388s")))

(define-public crate-forc-tracing-0.35.3 (c (n "forc-tracing") (v "0.35.3") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "17w9pc1f0x37xahl5rg6p8anf967dhwjna6z29d20g4v60va8x8m")))

(define-public crate-forc-tracing-0.35.4 (c (n "forc-tracing") (v "0.35.4") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0q8iy3cv0l27330gr5ravbcbwhhjlxnq3kigd9sm3nq1ldc5w8kn")))

(define-public crate-forc-tracing-0.35.5 (c (n "forc-tracing") (v "0.35.5") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "1c93hnqgbfwrx2mypk3dpknn3j484cvj765i2kzxxffl65b5198m")))

(define-public crate-forc-tracing-0.36.0 (c (n "forc-tracing") (v "0.36.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "1dx3mk10cax6kbmg4l9a4w78p5hbmkrslw14ihw8f2l086zdjjgs")))

(define-public crate-forc-tracing-0.36.1 (c (n "forc-tracing") (v "0.36.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "13h8ky0jrnlg7plxfhxivp0299s4d6bvf8r6xyg2pr6amnfz34r4")))

(define-public crate-forc-tracing-0.37.0 (c (n "forc-tracing") (v "0.37.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0g7az2ahbckw5grd1p4hn2xizb6wlcadgxag05k9jaml3m0nglac")))

(define-public crate-forc-tracing-0.37.1 (c (n "forc-tracing") (v "0.37.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "154kbydf5hah5b0gb9wagfvv44sqx9q6dvzali6xf5k6q4msz5w2")))

(define-public crate-forc-tracing-0.37.2 (c (n "forc-tracing") (v "0.37.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0mxq26awq6gcc45ddcvbwcgad1z3ih426cpaqsqrgr78dpa7qcj0")))

(define-public crate-forc-tracing-0.37.3 (c (n "forc-tracing") (v "0.37.3") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0p3rgr64srsbkj0pw88kz5jvbyywmwsmccnv8q4zwllr7v8xbmhg")))

(define-public crate-forc-tracing-0.38.0 (c (n "forc-tracing") (v "0.38.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0jhqmlk8s1f3yc6bj4r0r0x5bzqyvdqm46m2vqzr4mbd7i5f3wq3")))

(define-public crate-forc-tracing-0.39.0 (c (n "forc-tracing") (v "0.39.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "06m3g334rxsdwg3firni6nbqppzb35fm7i65sb2s85a1a5hfl8yd")))

(define-public crate-forc-tracing-0.39.1 (c (n "forc-tracing") (v "0.39.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0ckf2v9b721lc7my31ms88a5br6wckc4q4vky9rdcnn34ss30xiq")))

(define-public crate-forc-tracing-0.40.0 (c (n "forc-tracing") (v "0.40.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "1lgiyb520agfzddlihi3bks66zkrsw8y9nrpvmmhbbac7cqsd35y")))

(define-public crate-forc-tracing-0.40.1 (c (n "forc-tracing") (v "0.40.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0s51a8ri7v3d9c28n2pn42jxl3klpk0w770h3sz2pfa96wk7asn9")))

(define-public crate-forc-tracing-0.41.0 (c (n "forc-tracing") (v "0.41.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0yfk8x60di6xs8gqaa9k0bbjia91hndbg9s8d4i9w9ggkwgzqcpd")))

(define-public crate-forc-tracing-0.42.0 (c (n "forc-tracing") (v "0.42.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "1vpbdxgwkm5lsm45832f786wwanpz23qshhnf9zc7nx1yvjh2jl4")))

(define-public crate-forc-tracing-0.42.1 (c (n "forc-tracing") (v "0.42.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "04nq0920rnj26hvid535wkp102nqkyfgjxnh6i7asfbjg0g53az3")))

(define-public crate-forc-tracing-0.43.0 (c (n "forc-tracing") (v "0.43.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "1qpmh1lygrya2l3355wmw6q9x0wf2kvf815wdxmggjmiv2s2j88v")))

(define-public crate-forc-tracing-0.43.2 (c (n "forc-tracing") (v "0.43.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "17i491a0aicc4ynab6k8w8hsd1zag05xq74ypfbcwxfhpqipqx5j")))

(define-public crate-forc-tracing-0.44.0 (c (n "forc-tracing") (v "0.44.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0j27jgcynpkhi13a6w8i29q1x6w8w662mnc8kgasn9gbfb7yxgwy")))

(define-public crate-forc-tracing-0.44.1 (c (n "forc-tracing") (v "0.44.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "15341jcz4b3wlzlk56mvgc0g3hzv9y3sbvjh4hz2ydpvyn692i4x")))

(define-public crate-forc-tracing-0.45.0 (c (n "forc-tracing") (v "0.45.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "159z2ar1x5jyi9g47xshp2jry30442s9mbwjrjkjv3qkh0l7w47w")))

(define-public crate-forc-tracing-0.46.0 (c (n "forc-tracing") (v "0.46.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0vjzapj2cj2gq26qsq1xyqf51gfnx8z4dvxyih9jnrznmkas34c0")))

(define-public crate-forc-tracing-0.46.1 (c (n "forc-tracing") (v "0.46.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0zbvbk99ndadibp2mf6an6i2738jxhi63vmxmg6b6k4ra2q2afki")))

(define-public crate-forc-tracing-0.47.0 (c (n "forc-tracing") (v "0.47.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "092ds8rv2j6nf9mr8yanmmjalcqpml16kjplyrmvyycibkg4yl5r")))

(define-public crate-forc-tracing-0.48.0 (c (n "forc-tracing") (v "0.48.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "1aq6xgn656wsfpslnfgd30wwisai41gbc5smg98dp8fiwcdh32kv")))

(define-public crate-forc-tracing-0.48.1 (c (n "forc-tracing") (v "0.48.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0ydpimvafqg75s2qaw1xsjz3yjah8difj2r9kys80r0nrd1ly3q1")))

(define-public crate-forc-tracing-0.49.1 (c (n "forc-tracing") (v "0.49.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "19i9r376c5m1c1crvrbmbvhid2p8kk6gn7g9gi7yy9ag03fy73li")))

(define-public crate-forc-tracing-0.49.2 (c (n "forc-tracing") (v "0.49.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0a7v7rzbffm60r7l21r9h5sdjhxyh2m46r6gjyrag9wjralhzzz3")))

(define-public crate-forc-tracing-0.50.0 (c (n "forc-tracing") (v "0.50.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "13vsg9v97zgr3wb241lica2d5qpn9hh28j000hmpbzy0lp4jw2ma")))

(define-public crate-forc-tracing-0.51.0 (c (n "forc-tracing") (v "0.51.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0b1n5nbfzhv650wn1niknr0qba8mgi4hpl4nz09wf16ad3b6p5p5")))

(define-public crate-forc-tracing-0.51.1 (c (n "forc-tracing") (v "0.51.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "1jdmmi67kd3qrixxqks494qj96y4n6qmi84dasn7ssx9l05mcdjv")))

(define-public crate-forc-tracing-0.49.3 (c (n "forc-tracing") (v "0.49.3") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0bwc0jpd9r911ls9j0081676piwrjczic89pj43brziahslyxzba")))

(define-public crate-forc-tracing-0.52.0 (c (n "forc-tracing") (v "0.52.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0z8xhrmaj30gxjdy9dn10gsh7p8xdxwazf363q0qwkd750xrb914")))

(define-public crate-forc-tracing-0.52.1 (c (n "forc-tracing") (v "0.52.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "1j5lb2zsgnci7ar2jxmqw8lrrfk2w0ha3qraazbz7s61b13qwsaq")))

(define-public crate-forc-tracing-0.53.0 (c (n "forc-tracing") (v "0.53.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "12sgb0nc0mdvjhqgxysbjjlypyv6kyhmm751va819fhn8cc7sgwx")))

(define-public crate-forc-tracing-0.54.0 (c (n "forc-tracing") (v "0.54.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0hfbb9ychn3j64skcdrskzbwrdsnqxcrnvgkq9jlx95i2k20q0lb")))

(define-public crate-forc-tracing-0.55.0 (c (n "forc-tracing") (v "0.55.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "05105m1xagk557ggck8qpa07k4k9hgp1m3zvidpmz75kbnk2yz4g")))

(define-public crate-forc-tracing-0.56.0 (c (n "forc-tracing") (v "0.56.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0qc6410gx4vzsv4l7s8pw4a2kgqpxz3pill67vb2n2lmx7rcspmi")))

(define-public crate-forc-tracing-0.56.1 (c (n "forc-tracing") (v "0.56.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0q1f8ph0da2n309wniq2idffc4n3amh2c5xh0n8msybjpr9jvx92")))

(define-public crate-forc-tracing-0.57.0 (c (n "forc-tracing") (v "0.57.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0r8mr9ajik440sn22dlrda9941gyji6n2qk4qbz6sn98rxm4cnyc")))

(define-public crate-forc-tracing-0.58.0 (c (n "forc-tracing") (v "0.58.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "08i5fba8zlcw9mc9c9avap96wy5wvzblic8rf79m5b1mmr4ky7ms")))

(define-public crate-forc-tracing-0.59.0 (c (n "forc-tracing") (v "0.59.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "0k88mdlgqw2a2gr78plyvh8sfcxspwv3q6crvmv59c08401gsbmz")))

(define-public crate-forc-tracing-0.60.0 (c (n "forc-tracing") (v "0.60.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("ansi" "env-filter" "json"))) (d #t) (k 0)))) (h "1jysl22jc64b16hadnysn77z0ghf0d97lckp2a6ysyp1nskw8799")))

