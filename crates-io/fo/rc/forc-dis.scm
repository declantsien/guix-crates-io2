(define-module (crates-io fo rc forc-dis) #:use-module (crates-io))

(define-public crate-forc-dis-0.2.0 (c (n "forc-dis") (v "0.2.0") (d (list (d (n "colorful") (r "^0.2") (d #t) (k 0)) (d (n "fuel-asm") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "08gh56mzgv3da562l2mqcvyn1wd7n1k50hb58fxnmiqp33fb1rw4")))

(define-public crate-forc-dis-0.2.1 (c (n "forc-dis") (v "0.2.1") (d (list (d (n "colorful") (r "^0.2") (d #t) (k 0)) (d (n "fuel-asm") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0k1kb0s7s85klikin85hlqriq3whz5zgfsaykp1mqsjcfin50vx9")))

(define-public crate-forc-dis-0.2.2 (c (n "forc-dis") (v "0.2.2") (d (list (d (n "colorful") (r "^0.2") (d #t) (k 0)) (d (n "fuel-asm") (r "^0.10") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1m9xl7j47g2l1q5gr14dkzwlswm2jlsbng5yykdd7w5z101ykvxb")))

