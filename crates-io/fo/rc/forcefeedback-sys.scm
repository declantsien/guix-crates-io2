(define-module (crates-io fo rc forcefeedback-sys) #:use-module (crates-io))

(define-public crate-ForceFeedback-sys-0.0.0 (c (n "ForceFeedback-sys") (v "0.0.0") (h "096z1k9fxr0l1kd2cjwsdv66g8c7l2905m1mhqz40dys23kvpwwz")))

(define-public crate-ForceFeedback-sys-0.0.1 (c (n "ForceFeedback-sys") (v "0.0.1") (h "06s84g4bd81yv31a37dwfwygwbw119z5g1hqgny9jkm2wxb4pn1m")))

