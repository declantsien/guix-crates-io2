(define-module (crates-io fo xg foxglove_derive) #:use-module (crates-io))

(define-public crate-foxglove_derive-0.1.0 (c (n "foxglove_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)))) (h "08xn89y79v9b64lglxr1xdw7wri3ppj8fibdizh5xbycd4arqp7z")))

