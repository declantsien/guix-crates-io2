(define-module (crates-io fo xg foxglove-protos) #:use-module (crates-io))

(define-public crate-foxglove-protos-0.1.0 (c (n "foxglove-protos") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.2.0") (d #t) (k 1)))) (h "0qh8s4sksgim29l8jasiknpvjghadn6x354vi9kvd6fpzxyv2dyk")))

(define-public crate-foxglove-protos-0.1.1 (c (n "foxglove-protos") (v "0.1.1") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "protobuf") (r "^3.4.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.2.0") (d #t) (k 1)))) (h "0h6k2ikid8yr8l4f50582mwj085yv23656j4v4bx0jka6i07jk7n")))

