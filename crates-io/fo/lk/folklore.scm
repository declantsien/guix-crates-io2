(define-module (crates-io fo lk folklore) #:use-module (crates-io))

(define-public crate-folklore-0.1.0 (c (n "folklore") (v "0.1.0") (d (list (d (n "atomic") (r "^0.6.0") (k 0)) (d (n "bytemuck") (r "^1.14.3") (d #t) (k 0)) (d (n "fixedstr") (r "^0.5.5") (d #t) (k 2)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)))) (h "08vp4ws30mxzklxpsy7lv1v6kyyy0badhi9kssbxx5k9fgv5p9s9")))

(define-public crate-folklore-0.1.1 (c (n "folklore") (v "0.1.1") (d (list (d (n "atomic") (r "^0.6.0") (k 0)) (d (n "bytemuck") (r "^1.14.3") (d #t) (k 0)) (d (n "fixedstr") (r "^0.5.5") (d #t) (k 2)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)))) (h "1cclq4abszlrcxym9j2jzqhal7pqib2xpi4y5rbfca82iqx9wznq")))

(define-public crate-folklore-0.2.1 (c (n "folklore") (v "0.2.1") (d (list (d (n "atomic") (r "^0.6.0") (k 0)) (d (n "bytemuck") (r "^1.14.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fixedstr") (r "^0.5.5") (d #t) (k 2)) (d (n "leapfrog") (r "^0.3.0") (f (quote ("stable_alloc"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)))) (h "0x5f4fpdl116k3aaxrwp1f3y03pgbk3mj44139qf03hryxhkc4p3")))

(define-public crate-folklore-0.3.0 (c (n "folklore") (v "0.3.0") (d (list (d (n "atomic") (r "^0.6.0") (k 0)) (d (n "bytemuck") (r "^1.14.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fixedstr") (r "^0.5.5") (d #t) (k 2)) (d (n "leapfrog") (r "^0.3.0") (f (quote ("stable_alloc"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)))) (h "01gfn2v3pi2lj3wfba9z5hpscnhqll02viqk7ghs7y5j8sfa9r2a")))

(define-public crate-folklore-0.4.0 (c (n "folklore") (v "0.4.0") (d (list (d (n "atomic") (r "^0.6.0") (k 0)) (d (n "bytemuck") (r "^1.14.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "fixedstr") (r "^0.5.5") (d #t) (k 2)) (d (n "hash32") (r "^0.3.1") (d #t) (k 0)) (d (n "leapfrog") (r "^0.3.0") (f (quote ("stable_alloc"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "siphasher") (r "^1.0.0") (d #t) (k 0)))) (h "1nnqh2xqqlaq0vcqv4y9k8h20jybx63r2zrz66b5lxx59gv471d5")))

