(define-module (crates-io fo lk folketinget-api-models) #:use-module (crates-io))

(define-public crate-folketinget-api-models-0.2.0 (c (n "folketinget-api-models") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1p601ddzik1pm2jwqcyz29had4mcc39qg8m99wx4fz5wz4krpvag") (f (quote (("reflection") ("enable-serde" "chrono/serde" "serde") ("default" "enable-serde" "reflection"))))))

