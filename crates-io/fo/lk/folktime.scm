(define-module (crates-io fo lk folktime) #:use-module (crates-io))

(define-public crate-folktime-0.1.0 (c (n "folktime") (v "0.1.0") (d (list (d (n "const-default") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10pf4qq3qifcqvl5macvdgg86r9ipzflfp2sw3ls8wpdgzr4xlsd")))

(define-public crate-folktime-0.2.0 (c (n "folktime") (v "0.2.0") (h "1vz16gig9rflzcpzradc7qh826pq4nqnip4gr8srqb4dxbllgy5n")))

(define-public crate-folktime-0.2.1 (c (n "folktime") (v "0.2.1") (h "1yfvf4fqdavrjyiv9c77078j438hzgpsiv4ccb47fqmbakm4zwb2")))

