(define-module (crates-io fo rg forgejo-sdk) #:use-module (crates-io))

(define-public crate-forgejo-sdk-0.1.0 (c (n "forgejo-sdk") (v "0.1.0") (d (list (d (n "miette") (r "^5.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1v62abr2k1pig8dsyi9grwk1l7cz38llq2fk7kya2nb90j7fawvn")))

