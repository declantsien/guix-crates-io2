(define-module (crates-io fo rg forgery) #:use-module (crates-io))

(define-public crate-forgery-0.1.0 (c (n "forgery") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0kqkh50jd5adncc1ah95ihdsad36j2jbhf4zyg85a3l4m3l0n3f5")))

(define-public crate-forgery-0.1.1 (c (n "forgery") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0fqnmfp5y9w832sfp21mic23a9v5s2bwikrzbcn9h4744b330byh")))

(define-public crate-forgery-0.1.2 (c (n "forgery") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1sdzlccywqk9rnvjc3rsn474kk47yzc2x6nf0mr4vjxgw4bsdiml")))

(define-public crate-forgery-0.1.3 (c (n "forgery") (v "0.1.3") (d (list (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1scf5xlzsbf6f289i0z05xk65a1899bf6d8xgw0nk513mqwn9i7b")))

(define-public crate-forgery-1.0.0 (c (n "forgery") (v "1.0.0") (d (list (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "1gmfpny146l3q7ar3lpjxjgfv41xgb57vicybc39s3irbs1pyjba")))

(define-public crate-forgery-1.0.1 (c (n "forgery") (v "1.0.1") (d (list (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0ib59wb6k2rnm9vbcmqn8bf9n3l0npavpn41lkdwijc23b15vglf")))

(define-public crate-forgery-1.0.2 (c (n "forgery") (v "1.0.2") (d (list (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "12c5l4cp5p69qaffn582q8ds1pjip3qx1w2yj7b4nn6f3xpa2xaj")))

(define-public crate-forgery-1.0.3 (c (n "forgery") (v "1.0.3") (d (list (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)))) (h "0b2xjzr7zdj00cxawrjaqaknwknj7qb96gs366ammbjy1cj76hqy")))

