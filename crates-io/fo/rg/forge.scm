(define-module (crates-io fo rg forge) #:use-module (crates-io))

(define-public crate-forge-0.1.0 (c (n "forge") (v "0.1.0") (d (list (d (n "rustc_version") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.23") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.23") (d #t) (k 0)) (d (n "toml") (r "^0.4.5") (d #t) (k 0)))) (h "17ipz46aclad1pv9cr36dzbil7sd9is4k78myc4xwpkbgmgiyw65")))

