(define-module (crates-io fo rg forge_did) #:use-module (crates-io))

(define-public crate-forge_did-0.1.0 (c (n "forge_did") (v "0.1.0") (d (list (d (n "base58") (r "^0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "forge_hasher") (r "^0.1") (d #t) (k 0)) (d (n "forge_signer") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1y8zq6xm2nsxdi3qd8j3g7npzbs7gnmjyq8j5jx48xj6li9ir3xz")))

(define-public crate-forge_did-0.1.1 (c (n "forge_did") (v "0.1.1") (d (list (d (n "base58") (r "^0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "forge_hasher") (r "^0.1") (d #t) (k 0)) (d (n "forge_signer") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1xwrc95ypzinjkz6krywmpic6fcjq631ia7y4kg73zcm3l2sidc1")))

(define-public crate-forge_did-0.1.2 (c (n "forge_did") (v "0.1.2") (d (list (d (n "base58") (r "^0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "forge_hasher") (r "^0.1") (d #t) (k 0)) (d (n "forge_signer") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0izgw034sbdblc9q8i2p4brlbp68lcr3ricy5sf4g9x6n3f3fqcx")))

(define-public crate-forge_did-0.1.3 (c (n "forge_did") (v "0.1.3") (d (list (d (n "base58") (r "^0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "forge_hasher") (r "^0.1") (d #t) (k 0)) (d (n "forge_signer") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "08080f62pz51kql950j4vrk88rdri14ia7m63dmk2qrv34l9w077")))

