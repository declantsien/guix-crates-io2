(define-module (crates-io fo rg forger) #:use-module (crates-io))

(define-public crate-forger-0.1.0 (c (n "forger") (v "0.1.0") (d (list (d (n "peroxide") (r "^0.34.1") (f (quote ("parquet"))) (d #t) (k 0)))) (h "1ha99zhgxvr1mgnhj6njj04bl4j41hzwl9g1ffnys1i0aji0cxvx")))

(define-public crate-forger-0.1.1 (c (n "forger") (v "0.1.1") (d (list (d (n "peroxide") (r "^0.34.1") (f (quote ("parquet"))) (d #t) (k 0)))) (h "0rz4lkihk878jnj4ym9d00hyj8mnspf55bax6sd5qkyjgpffakhh")))

(define-public crate-forger-0.1.2 (c (n "forger") (v "0.1.2") (d (list (d (n "peroxide") (r "^0.34.1") (f (quote ("parquet"))) (d #t) (k 0)))) (h "1rlhkv1njk9x2m5p0fcjyqvpd6nxawvjbg1c2xvp0g5gsg7hwpxp")))

(define-public crate-forger-0.1.3 (c (n "forger") (v "0.1.3") (d (list (d (n "peroxide") (r "^0.34.1") (f (quote ("parquet"))) (d #t) (k 0)))) (h "03r2w7hvilwfy86j50pd5x2qiqy0d3620fx6zkqd6pg58hi4qg66")))

(define-public crate-forger-0.1.4 (c (n "forger") (v "0.1.4") (d (list (d (n "peroxide") (r "^0.34.1") (f (quote ("parquet"))) (d #t) (k 0)))) (h "1i7aa5xp9hy683r4gq7bkjk6qg384f5jfjww89d76bjmgslwmzn5")))

