(define-module (crates-io fo rg forgotten) #:use-module (crates-io))

(define-public crate-forgotten-1.0.0 (c (n "forgotten") (v "1.0.0") (h "0zrihxdjvfgyc5c16ll4xdqcmbh5jgs64vp8s7v4fr5w2b4zgbdd")))

(define-public crate-forgotten-1.1.0 (c (n "forgotten") (v "1.1.0") (h "0id9fh2w548pb5drg564h4ff9w6p3i08ha85m3pd40pjf3y6s5fh")))

(define-public crate-forgotten-1.1.1 (c (n "forgotten") (v "1.1.1") (h "1ms3q40jkyaha7vsjgyack8z2q0hzr5mfji2fl248z7896cvcsb6")))

(define-public crate-forgotten-1.2.0 (c (n "forgotten") (v "1.2.0") (d (list (d (n "num") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "1phl30nri17qzck94f0mgjyblspr27b9nwmshp4lms7sd1i4grm3") (f (quote (("custom" "num"))))))

