(define-module (crates-io fo rg forge-exec-ipc-client) #:use-module (crates-io))

(define-public crate-forge-exec-ipc-client-0.0.1 (c (n "forge-exec-ipc-client") (v "0.0.1") (d (list (d (n "ethabi") (r "^18.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "interprocess") (r "^1.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "18pjvdh0m18hay3gfwdkd7rkbrvg46nv85ylcsb8mx6q9y8s0v8r")))

(define-public crate-forge-exec-ipc-client-0.0.2 (c (n "forge-exec-ipc-client") (v "0.0.2") (d (list (d (n "ethabi") (r "^18.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "interprocess") (r "^1.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ra61am5hk2lk0yg05my4k5ds0kpwxf1i5l3wm585bzpzkxrx032")))

(define-public crate-forge-exec-ipc-client-0.1.0 (c (n "forge-exec-ipc-client") (v "0.1.0") (d (list (d (n "ethabi") (r "^18.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "interprocess") (r "^1.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1vakma3blc1y1wyjx8piwqqgpj57vg8pdbrpxq3xipxn4v63mckr")))

(define-public crate-forge-exec-ipc-client-0.1.1 (c (n "forge-exec-ipc-client") (v "0.1.1") (d (list (d (n "ethabi") (r "^18.0.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "interprocess") (r "^1.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1dhqb71hlq5qh9lm5fc2m5b10d205gjdfqhwbby9ra6ncc69rlk8")))

