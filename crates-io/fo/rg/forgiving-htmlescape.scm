(define-module (crates-io fo rg forgiving-htmlescape) #:use-module (crates-io))

(define-public crate-forgiving-htmlescape-0.1.0 (c (n "forgiving-htmlescape") (v "0.1.0") (d (list (d (n "num") (r "^0.1.26") (d #t) (k 2)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "1b2hlwxvrz1nwc9mj076sklna6cbc4gry7chzbxbvj2fgm9dli0s")))

