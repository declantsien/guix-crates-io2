(define-module (crates-io fo rg forgiving-semver) #:use-module (crates-io))

(define-public crate-forgiving-semver-0.11.0 (c (n "forgiving-semver") (v "0.11.0") (d (list (d (n "diesel") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "forgiving-semver-parser") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1angscz98gj5fnrmm5m17nv5mv7l630kghwff8xa0sz6gsrs6cdq") (f (quote (("default") ("ci" "serde" "diesel/sqlite"))))))

