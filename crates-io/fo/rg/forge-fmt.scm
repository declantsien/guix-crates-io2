(define-module (crates-io fo rg forge-fmt) #:use-module (crates-io))

(define-public crate-forge-fmt-0.0.0 (c (n "forge-fmt") (v "0.0.0") (h "1qpf0sx20hgvqcvxa7x9n1l6n80f4lmvl59s4w0f2yivrcgqigm9")))

(define-public crate-forge-fmt-0.2.0 (c (n "forge-fmt") (v "0.2.0") (d (list (d (n "alloy-primitives") (r "^0.3") (k 0)) (d (n "ariadne") (r "^0.2") (d #t) (k 0)) (d (n "foundry-config") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "itertools") (r "^0.11") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "solang-parser") (r "=0.3.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 2)))) (h "123w2v968b3z6cnx63b5agilbhv9hybgn7807cmw22zqgxcq42xc") (r "1.72")))

