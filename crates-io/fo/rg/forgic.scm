(define-module (crates-io fo rg forgic) #:use-module (crates-io))

(define-public crate-forgic-0.1.0 (c (n "forgic") (v "0.1.0") (d (list (d (n "libic") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "xmltojson") (r "^0") (d #t) (k 0)))) (h "1f4cwmj2cy46b4znpy460rkakbzbbxi86wdpi6ycfdwllsvz06mf")))

