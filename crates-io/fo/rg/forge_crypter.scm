(define-module (crates-io fo rg forge_crypter) #:use-module (crates-io))

(define-public crate-forge_crypter-0.1.0 (c (n "forge_crypter") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_os") (r "^0.2.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "02ilkwg3spqmqwrphsw2317lbzndgm485jjz2l15sg6lhvbjp45p")))

(define-public crate-forge_crypter-0.1.1 (c (n "forge_crypter") (v "0.1.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_os") (r "^0.2.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1ac08qvj6dz7kd95j59nwy9y1xqjyhi8inzq9yanp13r7pbvk9yx")))

(define-public crate-forge_crypter-0.1.2 (c (n "forge_crypter") (v "0.1.2") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_os") (r "^0.2.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0im6k198l799zb55liz62hw44faknjprk2g72kpw04zl0iksa3ih")))

(define-public crate-forge_crypter-0.1.3 (c (n "forge_crypter") (v "0.1.3") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_os") (r "^0.2.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1wc3h9fxlajjhdsa15xs80aapbag6698g3fw1xm72ngxz4h3i1wk")))

