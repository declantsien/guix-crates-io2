(define-module (crates-io fo rg forge_hasher) #:use-module (crates-io))

(define-public crate-forge_hasher-0.1.0 (c (n "forge_hasher") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.5") (d #t) (k 0)))) (h "1l6rbbllas6qjm25glfk6kf5xbnfxgj890vjh3pf35008j72y5gf")))

(define-public crate-forge_hasher-0.1.1 (c (n "forge_hasher") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.5") (d #t) (k 0)))) (h "1hdyanybw53k3jxw46mclp0y14pxv9bfy4zym5rm1n7kx4gpcmqg")))

(define-public crate-forge_hasher-0.1.2 (c (n "forge_hasher") (v "0.1.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.5") (d #t) (k 0)))) (h "1zdz314nw0h23ijbs9snlf3xrmi07y2vl73kl0h1pr5fdfwx6lm6")))

(define-public crate-forge_hasher-0.1.3 (c (n "forge_hasher") (v "0.1.3") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "sha3") (r "^0.8.2") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.5") (d #t) (k 0)))) (h "052476ixb7p7f6b2hjnk0rsjgsp7jrabdx7j6rr0avaskkr8y1sb")))

