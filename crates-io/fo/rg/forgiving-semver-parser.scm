(define-module (crates-io fo rg forgiving-semver-parser) #:use-module (crates-io))

(define-public crate-forgiving-semver-parser-0.10.1 (c (n "forgiving-semver-parser") (v "0.10.1") (d (list (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_generator") (r "^2.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)))) (h "1ar6ixk3jn3m0ggriz676b0793x8x6p3abs2bhxib8jf1hwdhx47")))

