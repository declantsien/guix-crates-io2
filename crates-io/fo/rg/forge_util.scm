(define-module (crates-io fo rg forge_util) #:use-module (crates-io))

(define-public crate-forge_util-0.1.0 (c (n "forge_util") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1dgd10lj1cls4sdjhisggix37q2cmn7cyq0rqm5dvwhka5x9hg7g")))

(define-public crate-forge_util-0.1.1 (c (n "forge_util") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "12sin4l6niab4b438qb1p5n5rgv0nkrdhnrn4nqfhji7k4zl0wyw")))

(define-public crate-forge_util-0.1.2 (c (n "forge_util") (v "0.1.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0psdgqxvc14hxl3ldabbv41d41yqg5sr67v4g63585hiigwskn2q")))

(define-public crate-forge_util-0.1.3 (c (n "forge_util") (v "0.1.3") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ic043damnip7ampr8p6ad9bv5gpid7q1png7rbyihrqhzqkwacc")))

