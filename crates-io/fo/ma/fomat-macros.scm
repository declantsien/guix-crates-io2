(define-module (crates-io fo ma fomat-macros) #:use-module (crates-io))

(define-public crate-fomat-macros-0.1.0 (c (n "fomat-macros") (v "0.1.0") (h "1djbfwfr4acb51ssmhwicsi9a10wf36bv4lspdspbzywajgw4a3i")))

(define-public crate-fomat-macros-0.1.1 (c (n "fomat-macros") (v "0.1.1") (h "16dw4q7clcsrma29l7d9wqhb398ypvrsf5ngkqn56dg0nf6gjllq")))

(define-public crate-fomat-macros-0.2.0 (c (n "fomat-macros") (v "0.2.0") (d (list (d (n "tar") (r "^0.4.9") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0vx5h8vywkrzmssw0a553cwfkdffw7g243b57nd9bkfjbfa7abwq")))

(define-public crate-fomat-macros-0.2.1 (c (n "fomat-macros") (v "0.2.1") (d (list (d (n "tar") (r "^0.4.9") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1iyhqlrb5blv8cmwjmb1wcbfpmhl2d04sapwcsxgnk9scifv6hdm")))

(define-public crate-fomat-macros-0.3.0 (c (n "fomat-macros") (v "0.3.0") (d (list (d (n "tar") (r "^0.4.9") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "00cpagvk1n18k8wn65mq5sp8x1qvs42si2hv4i2h83pd3igybf5x")))

(define-public crate-fomat-macros-0.3.1 (c (n "fomat-macros") (v "0.3.1") (d (list (d (n "tar") (r "^0.4.9") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0cx8qrr5cskbyf2lbfyxs9gp3cx8q45kjszba1hmb7wziim5ampy")))

(define-public crate-fomat-macros-0.3.2 (c (n "fomat-macros") (v "0.3.2") (d (list (d (n "tar") (r "^0.4.9") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "15m090189g6vzwparj6y6q6q7splysclc05nxfh39399fnl2lwiz")))

