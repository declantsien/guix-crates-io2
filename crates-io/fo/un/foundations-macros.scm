(define-module (crates-io fo un foundations-macros) #:use-module (crates-io))

(define-public crate-foundations-macros-3.0.0 (c (n "foundations-macros") (v "3.0.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0g57nv827bxknh2hnl89yr01ry02f6gw7h6k2szg3yw1y1p8rlal")))

(define-public crate-foundations-macros-3.0.1 (c (n "foundations-macros") (v "3.0.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1n43d4xrvfpazsql58zkc63ryr21b40kycm9b23z9vi4mdkvxh93")))

(define-public crate-foundations-macros-3.1.0 (c (n "foundations-macros") (v "3.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0vxfwy0jgy0r1sy19h3l2h62ssa6w7hmgcfdy0asihshsqbldzg7")))

(define-public crate-foundations-macros-3.1.1 (c (n "foundations-macros") (v "3.1.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "0rhkg9y5bd6z7f4bqvhzmnpf409kr633577zswyll6wwijyzkyj6")))

(define-public crate-foundations-macros-3.2.2 (c (n "foundations-macros") (v "3.2.2") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1iqapdkw33yyp7qvyyxyghyxzv9q1skfnw0yxavxhx8qw733h1za")))

(define-public crate-foundations-macros-3.3.0 (c (n "foundations-macros") (v "3.3.0") (d (list (d (n "darling") (r "^0.14.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "030vl6vnanmni2k7v8sgb3s90p9lc0ycc3p1bqzqi4nrsr5kgjcn")))

