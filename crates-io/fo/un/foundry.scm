(define-module (crates-io fo un foundry) #:use-module (crates-io))

(define-public crate-foundry-0.2.0 (c (n "foundry") (v "0.2.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "vulkano") (r "^0.10") (d #t) (k 0)) (d (n "vulkano-shader-derive") (r "^0.10") (d #t) (k 0)))) (h "0y72p3bqc2cjr0nzzj26h8qsscr5n13ms2ki89kxzcf979yrdmql")))

(define-public crate-foundry-0.2.1 (c (n "foundry") (v "0.2.1") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "vulkano") (r "^0.10") (d #t) (k 0)) (d (n "vulkano-shader-derive") (r "^0.10") (d #t) (k 0)))) (h "00v9a29rhjkb3ca8vpw9xzrmkl0m6b18lf70wg9gbbm84fbcx5n0")))

(define-public crate-foundry-0.3.0 (c (n "foundry") (v "0.3.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "vulkano") (r "^0.10") (d #t) (k 0)) (d (n "vulkano-shader-derive") (r "^0.10") (d #t) (k 0)))) (h "1js9glvf56q9hsi08l53dbyqg5p5p77dlpds8r9524v66zvw9r41")))

