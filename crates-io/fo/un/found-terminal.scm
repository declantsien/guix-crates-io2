(define-module (crates-io fo un found-terminal) #:use-module (crates-io))

(define-public crate-found-terminal-0.1.0 (c (n "found-terminal") (v "0.1.0") (d (list (d (n "inquire") (r "^0.5.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_derive2") (r "^0.1.18") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0lib7na40x4y8lc5mgfvsdrnb1ja2qss7zbrgnjlhipgpbz62yll")))

