(define-module (crates-io fo un founder) #:use-module (crates-io))

(define-public crate-founder-0.0.1 (c (n "founder") (v "0.0.1") (h "0rsbzlbbsxbl734hxcpjdi3y7ain7hs0yj7802lpgyf79phab5r1")))

(define-public crate-founder-0.2.0 (c (n "founder") (v "0.2.0") (d (list (d (n "arguments") (r "^0.7") (d #t) (k 0)) (d (n "font") (r "^0.10") (d #t) (k 0)) (d (n "svg") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "09m6mspirx365ghg6a0nn97hs1byi4bkgdfzv14ggj6iczsf8y09")))

(define-public crate-founder-0.3.0 (c (n "founder") (v "0.3.0") (d (list (d (n "arguments") (r "^0.7") (d #t) (k 0)) (d (n "font") (r "^0.10") (d #t) (k 0)) (d (n "svg") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0d6jnqg1ja45l6mig4k56kfpcgfzmlqm2g5bvs25spnw8w9b5gpj")))

(define-public crate-founder-0.3.1 (c (n "founder") (v "0.3.1") (d (list (d (n "arguments") (r "^0.7") (d #t) (k 0)) (d (n "font") (r "^0.10") (d #t) (k 0)) (d (n "svg") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0w7rmqq06qhpichwbdzskphsm2zc9flqcxdi3i9w2isj70qq00mi")))

(define-public crate-founder-0.4.0 (c (n "founder") (v "0.4.0") (d (list (d (n "arguments") (r "^0.7") (d #t) (k 0)) (d (n "font") (r "^0.10.3") (d #t) (k 0)) (d (n "svg") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "05damn3w92gfl4h88clv4jd3n2yn6igr6nzffy9cyv7fznnwfbls")))

(define-public crate-founder-0.5.0 (c (n "founder") (v "0.5.0") (d (list (d (n "arguments") (r "^0.7") (d #t) (k 0)) (d (n "font") (r "^0.10.3") (d #t) (k 0)) (d (n "resvg") (r "^0.28.0") (o #t) (k 0)) (d (n "svg") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "14ap4rh5kj8dybzilspgfa1p0qyj9zh137b2ddsiv0gpz4c2ymyg") (f (quote (("convert" "resvg"))))))

(define-public crate-founder-0.5.1 (c (n "founder") (v "0.5.1") (d (list (d (n "arguments") (r "^0.7") (d #t) (k 0)) (d (n "font") (r "^0.10.3") (d #t) (k 0)) (d (n "resvg") (r "^0.28.0") (o #t) (k 0)) (d (n "svg") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1fimfy35apz2k68jmzd6xk6s3fwdldg29i5ym619jf97h8wcxypm") (f (quote (("convert" "resvg"))))))

(define-public crate-founder-0.5.2 (c (n "founder") (v "0.5.2") (d (list (d (n "arguments") (r "^0.7") (d #t) (k 0)) (d (n "font") (r "^0.10.4") (d #t) (k 0)) (d (n "resvg") (r "^0.28.0") (o #t) (k 0)) (d (n "svg") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0br453m6bjrmfjml12bfbch7cxzzk9piv9rgjwh1mzkbcxprjh0g") (f (quote (("convert" "resvg"))))))

(define-public crate-founder-0.5.3 (c (n "founder") (v "0.5.3") (d (list (d (n "arguments") (r "^0.7") (d #t) (k 0)) (d (n "font") (r "^0.10.5") (d #t) (k 0)) (d (n "resvg") (r "^0.28.0") (o #t) (k 0)) (d (n "svg") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0ww2spxkzqc6xlqplz7745gp5p4vr2rqrk0gngm1pxflmkhfsnvv") (f (quote (("convert" "resvg"))))))

(define-public crate-founder-0.5.4 (c (n "founder") (v "0.5.4") (d (list (d (n "arguments") (r "^0.7") (d #t) (k 0)) (d (n "font") (r "^0.10.6") (d #t) (k 0)) (d (n "resvg") (r "^0.28.0") (o #t) (k 0)) (d (n "svg") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "146w1jqgfx4yr76l1nipxw0akwbwh6vqj901b7hr1ayaiwm0cswi") (f (quote (("convert" "resvg"))))))

(define-public crate-founder-0.5.5 (c (n "founder") (v "0.5.5") (d (list (d (n "arguments") (r "^0.7") (d #t) (k 0)) (d (n "font") (r "^0.10.7") (d #t) (k 0)) (d (n "resvg") (r "^0.28.0") (o #t) (k 0)) (d (n "svg") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1wlj9wcxq0v4fxd1p92979sh0d9jviaww4yydzw19g7c3278rqp7") (f (quote (("convert" "resvg"))))))

(define-public crate-founder-0.5.6 (c (n "founder") (v "0.5.6") (d (list (d (n "arguments") (r "^0.7") (d #t) (k 0)) (d (n "font") (r "^0.10.8") (d #t) (k 0)) (d (n "resvg") (r "^0.28.0") (o #t) (k 0)) (d (n "svg") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0mk4j2yaw779spx711lccvbl66niqw745y3m74yxw4acrvg1hnqq") (f (quote (("convert" "resvg"))))))

(define-public crate-founder-0.6.0 (c (n "founder") (v "0.6.0") (d (list (d (n "arguments") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "font") (r "^0.10.9") (d #t) (k 0)) (d (n "resvg") (r "^0.28.0") (o #t) (k 0)) (d (n "svg") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0wgc2gvsx6igv9ip5n33vnp09f2wm8lh2z3wz6bw29g3znkjyqy5") (f (quote (("name" "bin") ("draw" "bin" "svg") ("convert" "bin" "resvg") ("bin" "arguments"))))))

(define-public crate-founder-0.7.0 (c (n "founder") (v "0.7.0") (d (list (d (n "arguments") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "font") (r "^0.10.10") (d #t) (k 0)) (d (n "resvg") (r "^0.28.0") (o #t) (k 0)) (d (n "svg") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1kl1lqirniy0j7cmg23wy619qdz8k6h81nji1d1sb15xnx7kmnwx") (f (quote (("name" "bin") ("draw" "bin" "svg") ("convert" "bin" "resvg") ("bin" "arguments"))))))

(define-public crate-founder-0.8.1 (c (n "founder") (v "0.8.1") (d (list (d (n "arguments") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "folder") (r "^0.2") (d #t) (k 0)) (d (n "font") (r "^0.11.1") (d #t) (k 0)) (d (n "resvg") (r "^0.28.0") (o #t) (k 0)) (d (n "svg") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0p23m20xfxrh5xdp17fd0cwbas2ykgqk7qsfnk6lnf9kl3yiwnhc") (f (quote (("name" "bin") ("draw" "bin" "svg") ("convert" "bin" "resvg") ("bin" "arguments"))))))

(define-public crate-founder-0.9.0 (c (n "founder") (v "0.9.0") (d (list (d (n "arguments") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "folder") (r "^0.5") (d #t) (k 0)) (d (n "font") (r "^0.21") (d #t) (k 0)) (d (n "resvg") (r "^0.28") (o #t) (k 0)) (d (n "svg") (r "^0.13") (o #t) (d #t) (k 0)))) (h "05s03vs5xhcgi6afv4m1laxri3a8cdyxmviyiv6akdyhw0sqrbgz") (f (quote (("name" "bin") ("draw" "bin" "svg") ("convert" "bin" "resvg") ("bin" "arguments"))))))

(define-public crate-founder-0.10.0 (c (n "founder") (v "0.10.0") (d (list (d (n "arguments") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "colored") (r "^2") (o #t) (d #t) (k 0)) (d (n "folder") (r "^0.5") (d #t) (k 0)) (d (n "font") (r "^0.24") (d #t) (k 0)) (d (n "resvg") (r "^0.28") (o #t) (k 0)) (d (n "svg") (r "^0.13") (d #t) (k 0)))) (h "13yl9xw8agvrr60qpagilyc4x4miyq54z828yj2yfllyi6c01247") (f (quote (("default" "bin") ("bin" "arguments" "colored" "resvg"))))))

(define-public crate-founder-0.11.0 (c (n "founder") (v "0.11.0") (d (list (d (n "arguments") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "colored") (r "^2") (o #t) (d #t) (k 0)) (d (n "folder") (r "^0.6") (d #t) (k 0)) (d (n "font") (r "^0.31") (d #t) (k 0)) (d (n "resvg") (r "^0.41") (o #t) (k 0)) (d (n "svg") (r "^0.17") (d #t) (k 0)))) (h "0wa7hc8dq0bdmjvqz7bjvz95l3bjm8qpj92jzrhvad58hzargrbg") (f (quote (("default" "binary") ("binary" "arguments" "colored" "resvg"))))))

