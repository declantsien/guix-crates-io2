(define-module (crates-io fo un foundation-firmware) #:use-module (crates-io))

(define-public crate-foundation-firmware-0.1.0 (c (n "foundation-firmware") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.83") (o #t) (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.14") (k 0)) (d (n "heapless") (r "^0.8") (k 0)) (d (n "hex") (r "^0.4.2") (o #t) (k 0)) (d (n "nom") (r "^7") (k 0)) (d (n "secp256k1") (r "^0.29") (k 0)))) (h "0cxm3qjrzzalc85f4dk1cl8hbfygld59za3xvpa5azc3b3zn0brf") (f (quote (("default" "std" "binary") ("binary" "anyhow" "hex" "secp256k1/global-context" "std")))) (s 2) (e (quote (("std" "hex?/std" "nom/std" "secp256k1/std"))))))

