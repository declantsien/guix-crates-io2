(define-module (crates-io fo un foundationdb-gen) #:use-module (crates-io))

(define-public crate-foundationdb-gen-0.1.0 (c (n "foundationdb-gen") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1dbjf22w6f4pnq5wsabbym52rmxbrjy3bh4jsi0vkvmw14nyfy76")))

(define-public crate-foundationdb-gen-0.4.1 (c (n "foundationdb-gen") (v "0.4.1") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "17bbabailg82yr5h571n0yadha161ilm4ca7prdmzm6f7ikdlqkb") (f (quote (("fdb-6_2") ("fdb-6_1") ("fdb-6_0") ("fdb-5_2") ("fdb-5_1") ("embedded-fdb-include") ("default" "fdb-6_1"))))))

(define-public crate-foundationdb-gen-0.4.2 (c (n "foundationdb-gen") (v "0.4.2") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "153psidbkk2ppwdczmgs1m16vbyjkwlj72lvqx1v8ry8i6kksg87") (f (quote (("fdb-6_2") ("fdb-6_1") ("fdb-6_0") ("fdb-5_2") ("fdb-5_1") ("embedded-fdb-include") ("default" "fdb-6_1"))))))

(define-public crate-foundationdb-gen-0.5.0 (c (n "foundationdb-gen") (v "0.5.0") (d (list (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "1bncivi7y9hrzjmir69vw3rvg0qs44hjvjgv3ykylhidhqgn19vr") (f (quote (("fdb-6_2") ("fdb-6_1") ("fdb-6_0") ("fdb-5_2") ("fdb-5_1") ("embedded-fdb-include") ("default" "fdb-6_2"))))))

(define-public crate-foundationdb-gen-0.6.0 (c (n "foundationdb-gen") (v "0.6.0") (d (list (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "13ms8bb471lcpnl9brrwy4iajc7xip404b8di0i2gdqc8lz3yzda") (f (quote (("fdb-6_3") ("fdb-6_2") ("fdb-6_1") ("fdb-6_0") ("fdb-5_2") ("fdb-5_1") ("embedded-fdb-include") ("default" "fdb-6_3")))) (r "1.56")))

(define-public crate-foundationdb-gen-0.7.0 (c (n "foundationdb-gen") (v "0.7.0") (d (list (d (n "xml-rs") (r "^0.8.4") (d #t) (k 0)))) (h "12blqg49avzb06aj4kknp9i43hg8am595m16yh28kdrbdac1qkhk") (f (quote (("fdb-7_1") ("fdb-7_0") ("fdb-6_3") ("fdb-6_2") ("fdb-6_1") ("fdb-6_0") ("fdb-5_2") ("fdb-5_1") ("embedded-fdb-include") ("default" "fdb-7_1")))) (r "1.56")))

(define-public crate-foundationdb-gen-0.8.0 (c (n "foundationdb-gen") (v "0.8.0") (d (list (d (n "xml-rs") (r "^0.8.14") (d #t) (k 0)))) (h "021k1vckvslmr5fh5cfjkykf2q7ra9y89sxaf9iin10vy009f8v2") (f (quote (("fdb-7_1") ("fdb-7_0") ("fdb-6_3") ("fdb-6_2") ("fdb-6_1") ("fdb-6_0") ("fdb-5_2") ("fdb-5_1") ("embedded-fdb-include") ("default" "fdb-7_1")))) (r "1.60")))

(define-public crate-foundationdb-gen-0.9.0 (c (n "foundationdb-gen") (v "0.9.0") (d (list (d (n "xml-rs") (r "^0.8.19") (d #t) (k 0)))) (h "0xzsampq9rygpvvjbv77f6q6hndbjqhbx2gys2affj3alxa8v1rn") (f (quote (("fdb-7_3") ("fdb-7_1") ("fdb-7_0") ("fdb-6_3") ("fdb-6_2") ("fdb-6_1") ("fdb-6_0") ("fdb-5_2") ("fdb-5_1") ("embedded-fdb-include") ("default")))) (r "1.70")))

