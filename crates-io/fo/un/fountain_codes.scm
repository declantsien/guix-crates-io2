(define-module (crates-io fo un fountain_codes) #:use-module (crates-io))

(define-public crate-fountain_codes-0.1.0 (c (n "fountain_codes") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1fiy057zbq93vrdvhk41vrmvxqv2fylgl8a81f9svf388mgn1ncw")))

(define-public crate-fountain_codes-0.2.0 (c (n "fountain_codes") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0hc25xwfjy48a8v0kks4796vm981z27jy6ip2aackxbr5pgck2i0")))

(define-public crate-fountain_codes-0.2.1 (c (n "fountain_codes") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1wm2gn7p84rw83m3ajmxkms2lakf6bzwa87g4a47d5va1s44cn17")))

