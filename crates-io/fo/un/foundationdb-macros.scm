(define-module (crates-io fo un foundationdb-macros) #:use-module (crates-io))

(define-public crate-foundationdb-macros-0.1.1 (c (n "foundationdb-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "1bhcz3hcq2wpm5a0fm0jdh8cyidfqqz3042yc3f8xgh8c8rcfqr1") (r "1.56")))

(define-public crate-foundationdb-macros-0.2.0 (c (n "foundationdb-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.59") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)) (d (n "try_map") (r "^0.3.1") (d #t) (k 0)))) (h "05kmh2blmv2apni2pmghk3yfvbl5dl6wrkfynhibhsmlx0pxbj43") (r "1.60")))

(define-public crate-foundationdb-macros-0.3.0 (c (n "foundationdb-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)) (d (n "try_map") (r "^0.3.1") (d #t) (k 0)))) (h "171z5lh8dycyn971mwkd3lc72gmy8b9mpmcm1n0s68f6rd9ndnzq") (r "1.70")))

