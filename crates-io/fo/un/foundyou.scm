(define-module (crates-io fo un foundyou) #:use-module (crates-io))

(define-public crate-foundyou-0.1.0 (c (n "foundyou") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "prettytable") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1mjv451ajl6xi0gnghb6rzy8lcgm3fbycngbqwx0kq2xi2jk0fv2") (y #t)))

(define-public crate-foundyou-0.1.1 (c (n "foundyou") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "prettytable") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10sbzi7lfpxcp0qgzirjk9jk1k0crxzl3lzaiwc47mrvvr5a1z1h") (y #t)))

(define-public crate-foundyou-0.1.2 (c (n "foundyou") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "prettytable") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0b4znj14n6yfc0bapbjmbr18kcq904hrys5yyljyxkq58kvfb5dl") (y #t)))

(define-public crate-foundyou-0.1.3 (c (n "foundyou") (v "0.1.3") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "prettytable") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1fp29d80zi5p54fdxqnz9l0gb3q0prh0c3q3jlfbja8522jp9z4q") (y #t)))

(define-public crate-foundyou-0.1.4 (c (n "foundyou") (v "0.1.4") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "prettytable") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pv4551ny1wxbam9lmizskfvkdkppgqlr4yihr56msnpadx2kkyk")))

