(define-module (crates-io fo un foundationdb-sys) #:use-module (crates-io))

(define-public crate-foundationdb-sys-0.0.0 (c (n "foundationdb-sys") (v "0.0.0") (d (list (d (n "bindgen") (r "^0.36") (d #t) (k 1)))) (h "1a1j93qghq2jh8zaxxqynmhnld4vqwsliflbwwarvscfx19h7qpc")))

(define-public crate-foundationdb-sys-0.0.1 (c (n "foundationdb-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.36") (d #t) (k 1)))) (h "0lqwh42kd1gcfjnin6acqpv0mfrhm6bgpjv3hdrlcanc07mz72ri")))

(define-public crate-foundationdb-sys-0.0.2 (c (n "foundationdb-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.36") (d #t) (k 1)))) (h "16vf624fkn6qbz8fray9wizbdshpq7hy303aw4yk0f8izr3nzi2y")))

(define-public crate-foundationdb-sys-0.1.0 (c (n "foundationdb-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.36") (d #t) (k 1)))) (h "1dxsz9w5zyg63vmbdmll1iynckwbm1ch92aag7z3v8mhpswbwril")))

(define-public crate-foundationdb-sys-0.1.1 (c (n "foundationdb-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.36") (d #t) (k 1)))) (h "0lzkzf53phva6cvijwyamy42y3fqc3c8yrrldzmchhwvjn9fzx0s")))

(define-public crate-foundationdb-sys-0.2.0 (c (n "foundationdb-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.36") (d #t) (k 1)))) (h "1z3sxssz401lnbp6rgiqwq8gg4brpzz3clp18sqsr3m1w78bjbpg")))

(define-public crate-foundationdb-sys-0.4.0 (c (n "foundationdb-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "1zp7rk113l26ijwn3yy8l0mkyras0pcdgslvzl6zj1iaw4ngqihy") (f (quote (("fdb-6_2") ("fdb-6_1") ("fdb-6_0") ("fdb-5_2") ("fdb-5_1") ("embedded-fdb-include") ("default" "fdb-6_1"))))))

(define-public crate-foundationdb-sys-0.4.1 (c (n "foundationdb-sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "157vr53fcnd12hpp5p5c2rcgjxcbh9w7b5l0wqg6v0c93av5g3r3") (f (quote (("fdb-6_2") ("fdb-6_1") ("fdb-6_0") ("fdb-5_2") ("fdb-5_1") ("embedded-fdb-include") ("default" "fdb-6_1"))))))

(define-public crate-foundationdb-sys-0.4.2 (c (n "foundationdb-sys") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "1rb87d5lpr60kvryzcw7jk7kdlxj996g51a467vgbjxdjriwayjl") (f (quote (("fdb-6_2") ("fdb-6_1") ("fdb-6_0") ("fdb-5_2") ("fdb-5_1") ("embedded-fdb-include") ("default" "fdb-6_1"))))))

(define-public crate-foundationdb-sys-0.5.0 (c (n "foundationdb-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "1kzpr3m3z6zpp689swba37rlpcbd007gsrgj7maph63samg8j50k") (f (quote (("fdb-6_2") ("fdb-6_1") ("fdb-6_0") ("fdb-5_2") ("fdb-5_1") ("embedded-fdb-include") ("default" "fdb-6_2"))))))

(define-public crate-foundationdb-sys-0.6.0 (c (n "foundationdb-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)))) (h "05f6jb799dkaiv1madpyr2sz389cmxalcr1h7q9x63sarm7gfcnf") (f (quote (("fdb-6_3") ("fdb-6_2") ("fdb-6_1") ("fdb-6_0") ("fdb-5_2") ("fdb-5_1") ("embedded-fdb-include") ("default" "fdb-6_3")))) (r "1.56")))

(define-public crate-foundationdb-sys-0.7.0 (c (n "foundationdb-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "0mnk0dk0vaiiccw11a2hq6i46m25zbgak8p1pr6pk5hhfzp6xciy") (f (quote (("fdb-7_1") ("fdb-7_0") ("fdb-6_3") ("fdb-6_2") ("fdb-6_1") ("fdb-6_0") ("fdb-5_2") ("fdb-5_1") ("embedded-fdb-include") ("default")))) (r "1.56")))

(define-public crate-foundationdb-sys-0.8.0 (c (n "foundationdb-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0vr5hhr2rrsb6i284ps34wvx77siz7ipg348gdmjfg9rym2rbr4q") (f (quote (("fdb-7_1") ("fdb-7_0") ("fdb-6_3") ("fdb-6_2") ("fdb-6_1") ("fdb-6_0") ("fdb-5_2") ("fdb-5_1") ("embedded-fdb-include") ("default")))) (r "1.60")))

(define-public crate-foundationdb-sys-0.9.0 (c (n "foundationdb-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1yk9n8yyqjrpjwbhnhd39l5bspwv0ph14sk57dz7sjv1p2fz9qmc") (f (quote (("fdb-7_3") ("fdb-7_1") ("fdb-7_0") ("fdb-6_3") ("fdb-6_2") ("fdb-6_1") ("fdb-6_0") ("fdb-5_2") ("fdb-5_1") ("embedded-fdb-include") ("default")))) (r "1.70")))

