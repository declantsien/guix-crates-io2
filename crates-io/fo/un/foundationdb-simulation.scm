(define-module (crates-io fo un foundationdb-simulation) #:use-module (crates-io))

(define-public crate-foundationdb-simulation-0.2.0 (c (n "foundationdb-simulation") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.90") (d #t) (k 1)) (d (n "foundationdb") (r "^0.9.0") (k 0)) (d (n "foundationdb-macros") (r "^0.3.0") (k 2)) (d (n "foundationdb-sys") (r "^0.9.0") (k 0)))) (h "157wq17wrlivgdnfvgqjh9xpr65c6js8fpy0lv1a3xcw101b67x9") (f (quote (("fdb-docker") ("fdb-7_3" "foundationdb/fdb-7_3" "foundationdb-sys/fdb-7_3") ("fdb-7_1" "foundationdb/fdb-7_1" "foundationdb-sys/fdb-7_1") ("embedded-fdb-include" "foundationdb/embedded-fdb-include" "foundationdb-sys/embedded-fdb-include") ("default" "embedded-fdb-include"))))))

