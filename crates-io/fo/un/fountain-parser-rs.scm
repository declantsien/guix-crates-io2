(define-module (crates-io fo un fountain-parser-rs) #:use-module (crates-io))

(define-public crate-fountain-parser-rs-0.1.0 (c (n "fountain-parser-rs") (v "0.1.0") (d (list (d (n "enum-iterator") (r "^1.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (d #t) (k 0)))) (h "1b8b5yzdnpdxrpcxk5xb6bvln28v6bljvkwhy3szf83ds251bqrz")))

(define-public crate-fountain-parser-rs-0.2.0 (c (n "fountain-parser-rs") (v "0.2.0") (d (list (d (n "enum-iterator") (r "^1.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (d #t) (k 0)))) (h "00fglfyfgnqrz74iqd0ya1l7whajjxd79n3mvs7h7zqgwaxhn1sp")))

(define-public crate-fountain-parser-rs-0.3.0 (c (n "fountain-parser-rs") (v "0.3.0") (d (list (d (n "enum-iterator") (r "^1.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (d #t) (k 0)))) (h "021nwacs3sd6l9300913qdshwc8kbhb5h56mjmyj2z26hx4mrlb8")))

(define-public crate-fountain-parser-rs-0.4.0 (c (n "fountain-parser-rs") (v "0.4.0") (d (list (d (n "enum-iterator") (r "^1.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (d #t) (k 0)))) (h "1smwjjyqk04c81bmlczw5cf36g6bdwiihrgf54pp5zdms6pydm1g")))

