(define-module (crates-io fo un foundation) #:use-module (crates-io))

(define-public crate-foundation-0.0.1 (c (n "foundation") (v "0.0.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "10mhc8j4dhjmpqnbvfancnrlgj3mqw4wrn5lswzxsb8qmkycsm85") (y #t)))

