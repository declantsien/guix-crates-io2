(define-module (crates-io fo un foundry2echidna) #:use-module (crates-io))

(define-public crate-foundry2echidna-0.1.0 (c (n "foundry2echidna") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "140vcy988r9ykaybb5wxdd3qk6886v8mb4ajngvbysj3wyikq6ay")))

(define-public crate-foundry2echidna-0.2.0 (c (n "foundry2echidna") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19b695sgsbv1idy7rxbslg3akdzv125xzbvc1nwwfnr55ssksmc1")))

(define-public crate-foundry2echidna-0.2.1 (c (n "foundry2echidna") (v "0.2.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "130yac4vd815f3cym4njgd9b1s27j05pwbmx053vdjkcfp848c42")))

