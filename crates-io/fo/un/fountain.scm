(define-module (crates-io fo un fountain) #:use-module (crates-io))

(define-public crate-fountain-0.1.0 (c (n "fountain") (v "0.1.0") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "1rs2yv47p0c9gikh2whwpnn4jywb24x3vz3n6wqhvrf7cfpyi57f")))

(define-public crate-fountain-0.1.1 (c (n "fountain") (v "0.1.1") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "1zzfjnbpqb19l03pf9d8kaibbhry69xb3i56r1wfd67b35m21127")))

(define-public crate-fountain-0.1.2 (c (n "fountain") (v "0.1.2") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "02bv9fm0i1m6hagxalqiin77jqxkxmq1bw9nk37cda344cw30zpf")))

(define-public crate-fountain-0.1.3 (c (n "fountain") (v "0.1.3") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "0yffvvwsnddnjq6dfkij3m213dq9nn11w3ya50qk4gl64d74gl07")))

(define-public crate-fountain-0.1.4 (c (n "fountain") (v "0.1.4") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "1grf5ybafkx6c2zqvpkbmjv0nhmajagk68xffssjkdrg7cm5pzrh")))

(define-public crate-fountain-0.1.5 (c (n "fountain") (v "0.1.5") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "0lxgn2cmk01sldi802f6hmcwx2z6zl1lwrmvqbwqsi234mgfp6ii")))

(define-public crate-fountain-0.1.6 (c (n "fountain") (v "0.1.6") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "1gdyk90pqdl5fd0afc1y630jsccqavh5xk1lbz80gvmhjbm9lgq3")))

(define-public crate-fountain-0.1.7 (c (n "fountain") (v "0.1.7") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s6nfahdxcn2901p3fd0iy230snm79i9snva32980bsmdzhbpd4r")))

(define-public crate-fountain-0.1.8 (c (n "fountain") (v "0.1.8") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "17ph1yrdh29f25qvybdsr2m3daqphfvdivzwmxa81gjyh9p7qyag")))

(define-public crate-fountain-0.1.9 (c (n "fountain") (v "0.1.9") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xr3jx41iz1nbllqb2hcnrbyfyklmx77g86jyhjzq7z7h6bk2lma")))

(define-public crate-fountain-0.1.10 (c (n "fountain") (v "0.1.10") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "189baiyy3fgjbhbbynxfk9qykr2nsfx8x3n6l75l8csbdmxgi0pp")))

(define-public crate-fountain-0.1.11 (c (n "fountain") (v "0.1.11") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0c0psnna5r7x7nmgmw2j8g0bz9b2cr1b0pvkcijblam88j7rr1jb") (f (quote (("use_serde" "serde"))))))

(define-public crate-fountain-0.1.12 (c (n "fountain") (v "0.1.12") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xkjggjgwzvsc8x3k8lpldksl0qhbxp51s2wd0x266hawga260qg") (f (quote (("use_serde" "serde"))))))

