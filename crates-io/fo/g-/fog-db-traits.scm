(define-module (crates-io fo g- fog-db-traits) #:use-module (crates-io))

(define-public crate-fog-db-traits-0.0.0 (c (n "fog-db-traits") (v "0.0.0") (h "0794xx72g8p8s7clk5sb4145lpsag9b5cwcm9i8c2hddm0kw5r30")))

(define-public crate-fog-db-traits-0.0.1 (c (n "fog-db-traits") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "fog-crypto") (r "^0.4") (d #t) (k 0)) (d (n "fog-pack") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1f5gg5kcq3knwv0pb9fl96vmva5acipnj85h7j9zdi4a05wsbc5l")))

(define-public crate-fog-db-traits-0.0.2 (c (n "fog-db-traits") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "fog-crypto") (r "^0.4") (d #t) (k 0)) (d (n "fog-pack") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ph2z8x63crvqjss3a663d56y0ly5rb41nksvs9lggv404msw5rv")))

