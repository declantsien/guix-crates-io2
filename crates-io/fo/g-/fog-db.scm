(define-module (crates-io fo g- fog-db) #:use-module (crates-io))

(define-public crate-fog-db-0.0.1 (c (n "fog-db") (v "0.0.1") (d (list (d (n "crossbeam-channel") (r "^0.3.8") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.6.5") (d #t) (k 0)) (d (n "fog-pack") (r "^0.1") (d #t) (k 0)) (d (n "rocksdb") (r "^0.12.2") (d #t) (k 0)))) (h "0639dswlx56qiirrisk4m2d1lazl3lmn676jayxa6rp97gvkxm54")))

