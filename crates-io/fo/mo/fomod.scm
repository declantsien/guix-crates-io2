(define-module (crates-io fo mo fomod) #:use-module (crates-io))

(define-public crate-fomod-0.1.0 (c (n "fomod") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.30.0") (f (quote ("serialize" "serde-types"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bz0h5p0py6jvc1a3vfrr5x9an6mzrvlczmg0zc99j0znq79gji5")))

(define-public crate-fomod-0.2.0 (c (n "fomod") (v "0.2.0") (d (list (d (n "quick-xml") (r "^0.30.0") (f (quote ("serialize" "serde-types"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v9y3vah6vqpk656nrrr5bl7d3107pn8h1yhsrxidmwbahj5vci1")))

(define-public crate-fomod-0.2.1 (c (n "fomod") (v "0.2.1") (d (list (d (n "quick-xml") (r "^0.30.0") (f (quote ("serialize" "serde-types"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sbs7nbv78cbbah1g6wqlh42clj09vy5fxjmhkxfb1g37y2rbk12")))

(define-public crate-fomod-0.2.2 (c (n "fomod") (v "0.2.2") (d (list (d (n "quick-xml") (r "^0.30.0") (f (quote ("serialize" "serde-types"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "0g9gpagn23rzrlzbvdmjsaljvvlb6xbabg62ydkwzqmgaall5mb3")))

(define-public crate-fomod-0.2.3 (c (n "fomod") (v "0.2.3") (d (list (d (n "quick-xml") (r "^0.30.0") (f (quote ("serialize" "serde-types"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lcrh7y3ip1ydhs2gbm2fvsbj5b5wc61w73kxzn8d1hvjrsm0vi8")))

