(define-module (crates-io fo mo fomoscript) #:use-module (crates-io))

(define-public crate-fomoscript-0.1.0 (c (n "fomoscript") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1yg90y8n5h37c63fxnxgrzibrjdzrgkagn0cff8gb57f3wv3dmfr")))

(define-public crate-fomoscript-0.1.1 (c (n "fomoscript") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1b4g8awf20lshbfadbfvi9wwrmis97n28h7bla0fxnq3v5vgfz0s")))

(define-public crate-fomoscript-0.1.2 (c (n "fomoscript") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "180fxj1qb6nd61yy3nk4bac0yn8x1l3d0d4il1b7nymrhapdpd19")))

(define-public crate-fomoscript-0.2.0 (c (n "fomoscript") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "121gfvs6r5cp81rcj7l43ljci85cm1jjahrb31m1rqszb8hjssld")))

(define-public crate-fomoscript-0.2.1 (c (n "fomoscript") (v "0.2.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0l3lnxyb7sj7b9dbdpw4bn6g3z9kdafyrn9sn839amvr000lg9rf")))

(define-public crate-fomoscript-0.2.4 (c (n "fomoscript") (v "0.2.4") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1g9rmdf5rswj4mya5j8dsi9rq6snqy7n71y7mfikk8pyhzs13cbf")))

(define-public crate-fomoscript-0.2.5 (c (n "fomoscript") (v "0.2.5") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1mgmy0dvcw72h8vivsqajydgqq97wlx7qijgzqa5s413s0bi3ya5")))

