(define-module (crates-io fo ur four-char-code) #:use-module (crates-io))

(define-public crate-four-char-code-0.0.1 (c (n "four-char-code") (v "0.0.1") (d (list (d (n "four-char-code-macros-impl") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0d6xb51zgjavivpya00pg042pxlwf9lhm3nmr0m72hfhs70rdib1")))

(define-public crate-four-char-code-0.0.2 (c (n "four-char-code") (v "0.0.2") (d (list (d (n "four-char-code-macros-impl") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0lgsm72ib2xir2s50b36yc38yvnx7fc09ahlci2vzsr8nqi2gxwx")))

(define-public crate-four-char-code-0.0.3 (c (n "four-char-code") (v "0.0.3") (d (list (d (n "four-char-code-macros-impl") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1xv1nxis7njh50a5mzby521i12449sg1hy6qzkq6a6kyyq7fqkvk")))

(define-public crate-four-char-code-0.0.4 (c (n "four-char-code") (v "0.0.4") (d (list (d (n "four-char-code-macros-impl") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0vmxnl8mkxs5ir9skg167yw97126rg3q1hqf8zw5xygszyzcjng0")))

(define-public crate-four-char-code-0.0.5 (c (n "four-char-code") (v "0.0.5") (d (list (d (n "four-char-code-macros-impl") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "14m5sxr0rfkip0cwnm881n58wzx7rws9pdj6vmkqvibp2pjfv3i1")))

(define-public crate-four-char-code-2.0.0 (c (n "four-char-code") (v "2.0.0") (h "1sybj11qwm7bpncdh2kg51s1857hznxhd6441jkh859m3pgx6d2h") (f (quote (("std") ("default" "std"))))))

(define-public crate-four-char-code-1.0.0 (c (n "four-char-code") (v "1.0.0") (d (list (d (n "four-char-code-macros-impl") (r "^1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "103ff108ky7gskw7acpk9nvjgns033risha95m1j81s3408lrp4z") (f (quote (("std") ("default" "std"))))))

(define-public crate-four-char-code-2.0.1 (c (n "four-char-code") (v "2.0.1") (h "176k56lb60rlxibgf59blra5qh2vciimsaa3hyfxkd254gxc4vg7") (f (quote (("std") ("default" "std"))))))

(define-public crate-four-char-code-1.0.1 (c (n "four-char-code") (v "1.0.1") (d (list (d (n "four-char-code-macros-impl") (r "^1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "13s81mka61d9grdvjkhs753mhr4j599i3masb8n45r0kpvns1w3k") (f (quote (("std") ("default" "std"))))))

(define-public crate-four-char-code-2.0.2 (c (n "four-char-code") (v "2.0.2") (h "1sji4ysdc1ka9jiy6m43xygy7z8qaqri6lnfd9pf08l2smi20994") (f (quote (("std") ("default" "std"))))))

(define-public crate-four-char-code-1.0.2 (c (n "four-char-code") (v "1.0.2") (d (list (d (n "four-char-code-macros-impl") (r "^1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0gnrr25sqbi7nig8g4kvrsx5r3zd1p556c4hnvrafazskb9qg4v3") (f (quote (("std") ("default" "std"))))))

(define-public crate-four-char-code-2.1.0 (c (n "four-char-code") (v "2.1.0") (h "0d3d2j300kjq5ic6y05vfxpawbwp7xgrm4ps8c05m2i3w4xyi0l9") (f (quote (("std") ("default" "std"))))))

(define-public crate-four-char-code-1.1.0 (c (n "four-char-code") (v "1.1.0") (d (list (d (n "four-char-code-macros-impl") (r "^1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0jdbk1cky0ax25ivma61b7iib9xh5cw9j4iskqznv1ix1xv3jnn0") (f (quote (("std") ("default" "std"))))))

(define-public crate-four-char-code-2.2.0 (c (n "four-char-code") (v "2.2.0") (h "02zja0qsid0rriyjz1zlkva7h9ldlaqpqyyzf3ws3ck6sdgk2qf6") (f (quote (("std") ("default" "std"))))))

(define-public crate-four-char-code-1.2.0 (c (n "four-char-code") (v "1.2.0") (d (list (d (n "four-char-code-macros-impl") (r "^1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1infm8s0rplhgdr1c92g8y03jfgwfgs2hllflnvl6hlabswrdf5s") (f (quote (("std") ("default" "std"))))))

