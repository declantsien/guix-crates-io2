(define-module (crates-io fo ur fourpaws) #:use-module (crates-io))

(define-public crate-fourpaws-0.1.0 (c (n "fourpaws") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "catppuccin") (r "^2.2.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)))) (h "1pddhzgal82vj94wrykwy05g56kwm9fkvqz2shh3kh1sikkgvbc3")))

