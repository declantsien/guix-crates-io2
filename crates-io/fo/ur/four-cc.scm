(define-module (crates-io fo ur four-cc) #:use-module (crates-io))

(define-public crate-four-cc-0.1.0 (c (n "four-cc") (v "0.1.0") (h "0kqj6x12lwfrvl2s6p2v0q4n0jxi7sinm6zksf21678vldlayn1r")))

(define-public crate-four-cc-0.2.0 (c (n "four-cc") (v "0.2.0") (d (list (d (n "schemars") (r "^0.8.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "zerocopy") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "1dz1lnriwvjr7318299pm1gnajk6j0dvlp84z717igfsdc3kmmwj")))

(define-public crate-four-cc-0.3.0 (c (n "four-cc") (v "0.3.0") (d (list (d (n "schemars") (r "^0.8.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "zerocopy") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "1jqf4ddklx6dazhrv6l2riawmvhry9slkqqd82s55plgfwqlq6j3") (f (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-four-cc-0.4.0 (c (n "four-cc") (v "0.4.0") (d (list (d (n "schemars") (r "^0.8.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "zerocopy") (r "^0.7.0") (o #t) (d #t) (k 0)))) (h "0acnincbs26yjzkmdhw3q22c9dx5v56m1dybgkj7r6j1dp2vyp3r") (f (quote (("std") ("nightly") ("default" "std"))))))

