(define-module (crates-io fo ur foursquare-async) #:use-module (crates-io))

(define-public crate-foursquare-async-0.1.2 (c (n "foursquare-async") (v "0.1.2") (d (list (d (n "arcstr") (r "^1.1.4") (d #t) (k 0)) (d (n "bytes") (r "^1.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1fcj1fzldz795zdf26w7mb2ip9r2ns1laf7c9iqywmd2b1l53020") (f (quote (("rustls" "reqwest/rustls-tls") ("native-tls" "reqwest/native-tls") ("default" "rustls"))))))

