(define-module (crates-io fo ur fourleaf) #:use-module (crates-io))

(define-public crate-fourleaf-0.1.0 (c (n "fourleaf") (v "0.1.0") (d (list (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)))) (h "0vskxns2p7y9wmvw9sj6m8jfwwk0ys677f9vqxdf41irmibjx4wv")))

(define-public crate-fourleaf-0.1.1 (c (n "fourleaf") (v "0.1.1") (d (list (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)))) (h "1xshkn3vaxrqwyna45dczr8ssgwib23y2q5r7synqkv1j9k7laab")))

