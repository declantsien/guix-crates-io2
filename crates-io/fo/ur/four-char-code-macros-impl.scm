(define-module (crates-io fo ur four-char-code-macros-impl) #:use-module (crates-io))

(define-public crate-four-char-code-macros-impl-0.0.1 (c (n "four-char-code-macros-impl") (v "0.0.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.35") (f (quote ("derive"))) (d #t) (k 0)))) (h "00l9rkixdvlncds6zw24lqyimmqdkp68ilvl9r5wyrxjvn2zw04r")))

(define-public crate-four-char-code-macros-impl-0.0.2 (c (n "four-char-code-macros-impl") (v "0.0.2") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.35") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dgi3m47pl82pi9ixxd99xa8nj6130gi0xiqqfmrwgxs08aj8s1v")))

(define-public crate-four-char-code-macros-impl-1.0.0 (c (n "four-char-code-macros-impl") (v "1.0.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j3b59m3ly4pq1dfj0y07ghcxy7himikz8ysqlklral9460binj7")))

