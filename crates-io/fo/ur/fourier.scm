(define-module (crates-io fo ur fourier) #:use-module (crates-io))

(define-public crate-fourier-0.1.0 (c (n "fourier") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.6") (d #t) (k 2)) (d (n "multiversion") (r "^0.3") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (f (quote ("serde"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "13rd7hyfbkqmif3a58ksn8yk2vb9lqgz5pvnkj8zax9wf3kwxbls")))

