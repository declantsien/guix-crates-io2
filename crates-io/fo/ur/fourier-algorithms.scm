(define-module (crates-io fo ur fourier-algorithms) #:use-module (crates-io))

(define-public crate-fourier-algorithms-0.1.1 (c (n "fourier-algorithms") (v "0.1.1") (d (list (d (n "multiversion") (r "^0.6") (k 0)) (d (n "num-complex") (r "^0.2") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "1b0qf377hfw2shmqf0maqk6jndqw0ydx9wbqj5xjsx5nqpjjrp2d") (f (quote (("std" "multiversion/std" "num-traits/std") ("default" "std") ("alloc"))))))

