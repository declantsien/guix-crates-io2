(define-module (crates-io fo ur fourq) #:use-module (crates-io))

(define-public crate-fourq-0.1.0 (c (n "fourq") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0l2dk0z0k54wd4i2agfv5grf03kqcl1hzkl6f0xlbhz19c3s8b8c")))

(define-public crate-fourq-0.1.1 (c (n "fourq") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "08qm9w3frkw7h7q7s2h4mpga0fbi8jiw2qj4y5l83l91z39s7gya")))

(define-public crate-fourq-0.1.2 (c (n "fourq") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "curve25519-dalek") (r "^4.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0wshsjprs92sq3gc17kk1369ng61a5432dxpk9hzhg6wz618sm67")))

(define-public crate-fourq-0.1.3 (c (n "fourq") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0mnmyh4lb53q33wqrwcf6pi900ll997n5bqyavcjv20g6zyhx06i")))

(define-public crate-fourq-0.1.4 (c (n "fourq") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0dsck6bs0nwsmkwgivl81sy22j9pa6dcr9lmwm9h58qcbxskddvz")))

(define-public crate-fourq-0.1.5 (c (n "fourq") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0wnzjgm7bv2b7shs78xrwxn97nb0sla7qra3vwklj0fiwp0wrzym")))

(define-public crate-fourq-0.1.6 (c (n "fourq") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 0)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0h6gqprq6qf56kq7zc22n72x8mfa78qckhdx877gy38k28q6jxrk")))

