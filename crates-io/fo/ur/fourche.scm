(define-module (crates-io fo ur fourche) #:use-module (crates-io))

(define-public crate-fourche-0.1.0 (c (n "fourche") (v "0.1.0") (d (list (d (n "redis") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)))) (h "1ljf7xid7dda1y82pgcivjyxxd0ppa88ladpjggg8pql2kzvnkap")))

(define-public crate-fourche-0.1.1 (c (n "fourche") (v "0.1.1") (d (list (d (n "redis") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)))) (h "1nkqh6i4bkpi55vvbidg7ccwpfs89g73ljqf814ph1v4r1qrbz5h")))

(define-public crate-fourche-0.2.0 (c (n "fourche") (v "0.2.0") (d (list (d (n "redis") (r "^0.20.1") (d #t) (k 0)) (d (n "serde") (r "1.*") (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)))) (h "19xjv1r6jvxln2xcp00qf3l2pmc1ci49pflb94p9b38hw6kiikp0")))

