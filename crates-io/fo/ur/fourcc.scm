(define-module (crates-io fo ur fourcc) #:use-module (crates-io))

(define-public crate-fourcc-0.0.1 (c (n "fourcc") (v "0.0.1") (h "1g0zsk31gfgddr2wq49sq9b1nwsx57zpil630iv9w7ib06li9jxy")))

(define-public crate-fourcc-0.0.2 (c (n "fourcc") (v "0.0.2") (h "1089xm6f2hmrnsss1gzmslswysdgr4bmxmms6jxgyd07nwmvrd6r")))

(define-public crate-fourcc-0.0.3 (c (n "fourcc") (v "0.0.3") (h "1ka9zzm1817155rwqic0bbgd4gdn9hjmzpbz4hkvksfgmnikyyds")))

(define-public crate-fourcc-0.0.4 (c (n "fourcc") (v "0.0.4") (h "1154b22z27msixw8j578dh55k96b4zggpfqf47avhz1brjs2r7zz")))

