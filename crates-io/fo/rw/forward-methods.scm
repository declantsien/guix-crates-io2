(define-module (crates-io fo rw forward-methods) #:use-module (crates-io))

(define-public crate-forward-methods-0.0.0 (c (n "forward-methods") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1762b7aa9s8h1bi3ma3syhpa2597ynld9q3yrmnizllj3xkvqzix")))

(define-public crate-forward-methods-0.0.1 (c (n "forward-methods") (v "0.0.1") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "test-case") (r "^3.0") (d #t) (k 2)))) (h "1shh48v6h915jimpdd74x9zjyiv3z7vxlnwgnwzpnyhn6wy9bj84")))

(define-public crate-forward-methods-0.0.2 (c (n "forward-methods") (v "0.0.2") (d (list (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "test-case") (r "^3.0") (d #t) (k 2)))) (h "09d106zd8dj5k8m2yh321qzm2vppp5rp89q5af94mf1cv25yhwpb")))

