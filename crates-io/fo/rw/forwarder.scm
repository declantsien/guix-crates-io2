(define-module (crates-io fo rw forwarder) #:use-module (crates-io))

(define-public crate-forwarder-0.1.0 (c (n "forwarder") (v "0.1.0") (d (list (d (n "chacha20poly1305") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "0pfwjpjfa6v60vxcw67xbp3idc5p7cpcsmfz915b86hcx30r6ghc")))

(define-public crate-forwarder-0.2.0 (c (n "forwarder") (v "0.2.0") (d (list (d (n "chacha20poly1305") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nacl") (r "^0.5.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rmp") (r "^0.8.11") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "0n6jxw7zh564gy6ksi7jqcj2j0vrwiv50nyhh91l6ksw4kd6b12f")))

(define-public crate-forwarder-0.3.0 (c (n "forwarder") (v "0.3.0") (d (list (d (n "chacha20poly1305") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "core_affinity") (r "^0.5.10") (d #t) (k 0)) (d (n "nacl") (r "^0.5.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rmp") (r "^0.8.11") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "0s9a26qbgdf7rxvn9d7h3j93dxf7rl4jqmmqz87dph153cpv693n")))

