(define-module (crates-io fo rw forwarded-header-value) #:use-module (crates-io))

(define-public crate-forwarded-header-value-0.1.0 (c (n "forwarded-header-value") (v "0.1.0") (d (list (d (n "nonempty") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0r6fmy2pbsm4wv2f4mk2mrwc7nvxgrpz7as9nc5w8pskbwldr4vx")))

(define-public crate-forwarded-header-value-0.1.1 (c (n "forwarded-header-value") (v "0.1.1") (d (list (d (n "nonempty") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1scqf5ar22066i901bx7p5zja23rd5amd00a25pwhk28717zhdc8")))

