(define-module (crates-io fo rw forward_ref_generic) #:use-module (crates-io))

(define-public crate-forward_ref_generic-0.1.0 (c (n "forward_ref_generic") (v "0.1.0") (h "1g727g6mmr77k56mjch8yhf4vi5j5b680vjzqy3gfx4bcjdaqilv")))

(define-public crate-forward_ref_generic-0.1.1 (c (n "forward_ref_generic") (v "0.1.1") (h "0ws836bdidkhz2ncdvqdg4ypbbqfnksc0k8905qmksjkdw02s5h4")))

(define-public crate-forward_ref_generic-0.1.2 (c (n "forward_ref_generic") (v "0.1.2") (h "02y41czcaiaipyzjyzgms47rcxmj56ik1kyzlamdfba3hx35wr0k")))

(define-public crate-forward_ref_generic-0.1.3 (c (n "forward_ref_generic") (v "0.1.3") (h "0la23bdy6isxsm1cm9qx84lfcwx70c9dbry134xhhnb430m9gslr")))

(define-public crate-forward_ref_generic-0.1.4 (c (n "forward_ref_generic") (v "0.1.4") (h "0j8sbns5fm8skhinml2av1bgzzklryy6fq00ghxkjvai1fsl39hf")))

(define-public crate-forward_ref_generic-0.1.5 (c (n "forward_ref_generic") (v "0.1.5") (h "17f868cn248ybw286vw73g1ljh5c6h0vwfqa7l453ilfkc1ka2ni")))

(define-public crate-forward_ref_generic-0.2.0 (c (n "forward_ref_generic") (v "0.2.0") (h "0v8gzvx3pxpd5vci4rlwzpn7jdzq6fjgxzrsxjqxp9n8wgwkydxy")))

(define-public crate-forward_ref_generic-0.2.1 (c (n "forward_ref_generic") (v "0.2.1") (h "0292fmbch8a2pm3fh0yyk0pjsxkq0xgl4w9l3i5ykxz7819536yk")))

