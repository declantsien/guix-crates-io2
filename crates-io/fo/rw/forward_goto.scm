(define-module (crates-io fo rw forward_goto) #:use-module (crates-io))

(define-public crate-forward_goto-0.1.0 (c (n "forward_goto") (v "0.1.0") (d (list (d (n "fix_fn") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ki821zywra6c1ibz4bqslf4jjz40qs66c8rjl11cxhyk67fv7qv")))

(define-public crate-forward_goto-0.1.1 (c (n "forward_goto") (v "0.1.1") (d (list (d (n "fix_fn") (r "^1.0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08lb2knvs202rkdpcmivyi0ibrfd8x5vq4gaza3bvlby92rzj580")))

