(define-module (crates-io fo rw forward-traits) #:use-module (crates-io))

(define-public crate-forward-traits-1.0.0 (c (n "forward-traits") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "syn_derive") (r "^0.1") (d #t) (k 0)))) (h "0jm07b4mn8fly32z67dl42alnf2rs6irwhviqc5g90rz7xycg0w4")))

(define-public crate-forward-traits-2.0.0 (c (n "forward-traits") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "syn_derive") (r "^0.1") (d #t) (k 0)))) (h "1bf0ax22qp3gf32j553v8cdy2gzi9px9dk7zpq5w1pzmrzjjdja3")))

(define-public crate-forward-traits-2.0.1 (c (n "forward-traits") (v "2.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "syn_derive") (r "^0.1") (d #t) (k 0)))) (h "0hggb3qncxnn8nrn29zbbca227llj42vxh1grchd9ym7qfqwdz3f")))

(define-public crate-forward-traits-2.0.2 (c (n "forward-traits") (v "2.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "syn_derive") (r "^0.1") (d #t) (k 0)))) (h "0c7yjkg8fqq7bmwrkcvhaas5lvz635ald9z3znsqdzhm7kw8w16f")))

(define-public crate-forward-traits-2.0.3 (c (n "forward-traits") (v "2.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "syn_derive") (r "^0.1") (d #t) (k 0)))) (h "0lcf8lbqff6xsas64307am0x1l3bn04yffnppjcxf8mlsq1m67ns")))

(define-public crate-forward-traits-3.0.0 (c (n "forward-traits") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "syn_derive") (r "^0.1") (d #t) (k 0)))) (h "1x3kfqnv47pr19j2pydps9dd4drn6qxjcwacfbhqg6s7v7mvb58p")))

(define-public crate-forward-traits-3.0.1 (c (n "forward-traits") (v "3.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "syn_derive") (r "^0.1") (d #t) (k 0)))) (h "1qwgmnjjcznb17hjgbc1xhnindh59gcfp6crfaz5qgjk2v01kknd")))

(define-public crate-forward-traits-3.1.0 (c (n "forward-traits") (v "3.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold" "extra-traits"))) (d #t) (k 0)) (d (n "syn_derive") (r "^0.1") (d #t) (k 0)))) (h "086w010xj6v68zkwnkmni4bs20508js1r0nj4gr9l1mv9mwdp6hx")))

