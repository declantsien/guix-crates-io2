(define-module (crates-io fo rw forward-dll-derive) #:use-module (crates-io))

(define-public crate-forward-dll-derive-0.1.6 (c (n "forward-dll-derive") (v "0.1.6") (d (list (d (n "object") (r "^0.30.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("full"))) (d #t) (k 0)))) (h "0g9s7hx9pnl8ym2rwfj9l9xw0js2vwny65yvchm2gm61d8sb90dx") (y #t)))

(define-public crate-forward-dll-derive-0.1.7 (c (n "forward-dll-derive") (v "0.1.7") (d (list (d (n "object") (r "^0.30.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("full"))) (d #t) (k 0)))) (h "1k3q727v7m4ka3rqh00m363laczip876cbdnvnxbqh45svmay1zx") (y #t)))

(define-public crate-forward-dll-derive-0.1.8 (c (n "forward-dll-derive") (v "0.1.8") (d (list (d (n "object") (r "^0.30.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("full"))) (d #t) (k 0)))) (h "0fr5f2hm0j28cn5nhpb03ri2xb6xqkhz8cfsqjd5cdairlwbyigh")))

(define-public crate-forward-dll-derive-0.1.9 (c (n "forward-dll-derive") (v "0.1.9") (d (list (d (n "object") (r "^0.30.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("full"))) (d #t) (k 0)))) (h "18xwh87ccbm36syk2ph94qaypshrkr7gw3rmrjrbwgbpf86p1rqa")))

(define-public crate-forward-dll-derive-0.1.10 (c (n "forward-dll-derive") (v "0.1.10") (d (list (d (n "object") (r "^0.30.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("full"))) (d #t) (k 0)))) (h "113g2sdap9r4maix8vhrhvx07b3y3lch73qh71cn7k03c0kykdb2")))

(define-public crate-forward-dll-derive-0.1.11 (c (n "forward-dll-derive") (v "0.1.11") (d (list (d (n "object") (r "^0.30.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("full"))) (d #t) (k 0)))) (h "0ynvc1nlxbrfnv8az6c5jzbmdxiz4z1kinl6p4wjz70xv38q55rg")))

(define-public crate-forward-dll-derive-0.1.12 (c (n "forward-dll-derive") (v "0.1.12") (d (list (d (n "object") (r "^0.30.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("full"))) (d #t) (k 0)))) (h "084g3aj1p98k0rd1lk0ysyaq57d9f6cwx9pbidrg3san11m90a6i")))

(define-public crate-forward-dll-derive-0.1.13 (c (n "forward-dll-derive") (v "0.1.13") (d (list (d (n "object") (r "^0.30.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("full"))) (d #t) (k 0)))) (h "1jgmx5sdq27p3ls7wb3zlkf95q79gj8px79an16x1hrnmb81qfcl")))

(define-public crate-forward-dll-derive-0.1.14 (c (n "forward-dll-derive") (v "0.1.14") (d (list (d (n "object") (r "^0.30.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("full"))) (d #t) (k 0)))) (h "0czjbk82j7wxgrhxd9w7f6v6pzxfnwjw9ay3pycpd18p93wjnrrz")))

(define-public crate-forward-dll-derive-0.1.15 (c (n "forward-dll-derive") (v "0.1.15") (d (list (d (n "object") (r "^0.30.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("full"))) (d #t) (k 0)))) (h "04qdmi9dnkj61ipc9mvjzcaqvzqsxx0bkr0b4h82fi9dkxjbspq9")))

(define-public crate-forward-dll-derive-0.1.16 (c (n "forward-dll-derive") (v "0.1.16") (d (list (d (n "object") (r "^0.30.3") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("full"))) (d #t) (k 0)))) (h "0f8821z60rlpwmgljhaghpsx81ckaynz0b58z484mj0b5a67syy4")))

