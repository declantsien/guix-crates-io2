(define-module (crates-io fo rw forwarding) #:use-module (crates-io))

(define-public crate-forwarding-0.1.0 (c (n "forwarding") (v "0.1.0") (d (list (d (n "forwarding_gen") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.54") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (d #t) (k 0)))) (h "00sb0cmvfqpfiza3xzm8mrqhljwksfz7jb3pb3ykygr1k1wd9x94")))

