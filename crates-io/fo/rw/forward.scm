(define-module (crates-io fo rw forward) #:use-module (crates-io))

(define-public crate-forward-0.1.0-alpha.1 (c (n "forward") (v "0.1.0-alpha.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1linmdj9n3ng7pv8cdl0lc2fskws3cbrh4mxvxcchmjjhzl5mwf0")))

