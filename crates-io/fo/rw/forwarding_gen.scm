(define-module (crates-io fo rw forwarding_gen) #:use-module (crates-io))

(define-public crate-forwarding_gen-0.1.0 (c (n "forwarding_gen") (v "0.1.0") (d (list (d (n "base32ct") (r "^0.2.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.54") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)) (d (n "syn") (r "^2.0.10") (f (quote ("full" "fold" "parsing" "extra-traits"))) (d #t) (k 0)) (d (n "trait_info_gen") (r "^0.1.0") (d #t) (k 0)))) (h "1w34c0064v32knfqxys8aq22a2wgyw3d484h10g792kfm9133sn2")))

