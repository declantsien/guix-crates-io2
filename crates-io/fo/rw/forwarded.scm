(define-module (crates-io fo rw forwarded) #:use-module (crates-io))

(define-public crate-forwarded-1.0.0 (c (n "forwarded") (v "1.0.0") (d (list (d (n "http") (r "^0.2.4") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0161yljyv4jznf1zb8ix0536l80w9hadavpf2g3fjlq7ppc9m43y") (y #t)))

(define-public crate-forwarded-1.0.1 (c (n "forwarded") (v "1.0.1") (d (list (d (n "http") (r "^0.2.4") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1ar64156qjlpr98q54npb5zd2l6p42m7609mwghda8avfbk45bis") (y #t)))

(define-public crate-forwarded-1.0.2 (c (n "forwarded") (v "1.0.2") (d (list (d (n "actix-web") (r "^3.3.2") (d #t) (k 0)) (d (n "http") (r "^0.2.4") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "14ymwsrcd6rsycid8m7r4xj27wifiv8908gp7kq2iwarzvplwl60") (y #t)))

(define-public crate-forwarded-1.0.3 (c (n "forwarded") (v "1.0.3") (d (list (d (n "actix-web") (r "^3.3.2") (d #t) (k 0)) (d (n "http") (r "^0.2.4") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "02aam5gkkkvdaaya8y5xj0v2rscxbiwzzki5802n9ccw5hvjl9kj") (y #t)))

(define-public crate-forwarded-1.0.4 (c (n "forwarded") (v "1.0.4") (d (list (d (n "actix-web") (r "^3.3.2") (d #t) (k 0)) (d (n "http") (r "^0.2.4") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0k119z5ma3dv74f89hxxzjpvd7h634gigg0r0b1gds2f7afhwkw5") (y #t)))

(define-public crate-forwarded-1.0.5 (c (n "forwarded") (v "1.0.5") (d (list (d (n "actix-web") (r "^3.3.2") (d #t) (k 0)) (d (n "http") (r "^0.2.4") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0r1kgv0xhgs7j9gkwalzwl6xp4m8rn4ic8c3i2ix6v30y7iajv4s") (y #t)))

(define-public crate-forwarded-1.0.6 (c (n "forwarded") (v "1.0.6") (d (list (d (n "actix-web") (r "^3.3.2") (d #t) (k 0)) (d (n "http") (r "^0.2.4") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0l3b13k0baniyl7ihkn1r14giq45yy2rkzv0h9qwv04061qfsiw5") (y #t)))

(define-public crate-forwarded-1.0.7 (c (n "forwarded") (v "1.0.7") (d (list (d (n "actix-rt") (r "^1.1.1") (d #t) (k 0)) (d (n "actix-web") (r "^3.3.2") (d #t) (k 0)) (d (n "http") (r "^0.2.4") (d #t) (k 0)))) (h "1yix4pz3ckahw4rhg94ykwjw7p1cq8ycfkrpd5y83y6yv8ly5c3m")))

