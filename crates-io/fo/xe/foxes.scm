(define-module (crates-io fo xe foxes) #:use-module (crates-io))

(define-public crate-foxes-1.0.0 (c (n "foxes") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ureq") (r "^2.7.1") (f (quote ("tls"))) (k 0)))) (h "07x7p6ncfih3nhddxpdx3lx7chz7xi2ijdgs3qrmcl4j127ca6rx")))

