(define-module (crates-io fo o- foo-cargo-filename-test) #:use-module (crates-io))

(define-public crate-foo-cargo-filename-test-0.1.0 (c (n "foo-cargo-filename-test") (v "0.1.0") (h "0a6chrl0svfl1frfhbzzd73lkzyz0nplq5p83ra598xjdp5bwn8x")))

(define-public crate-foo-cargo-filename-test-0.2.0 (c (n "foo-cargo-filename-test") (v "0.2.0") (d (list (d (n "flate2") (r "^1.0.14") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)))) (h "19z432bbqk2ivpffcr85j7dr0iwpbvgvn6zcjax9inzysyj57d52")))

(define-public crate-foo-cargo-filename-test-0.3.0 (c (n "foo-cargo-filename-test") (v "0.3.0") (d (list (d (n "flate2") (r "^1.0.14") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)))) (h "1g3m1hzlxlvvb5xz4m1zzahfyg60y5iymxkwcsmn1w7k97mpyx74")))

