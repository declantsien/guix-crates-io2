(define-module (crates-io fo nt font-atlas-freetype) #:use-module (crates-io))

(define-public crate-font-atlas-freetype-0.1.0 (c (n "font-atlas-freetype") (v "0.1.0") (d (list (d (n "font-atlas") (r "^0.1.0") (d #t) (k 0)) (d (n "font-atlas-image") (r "^0.1.0") (d #t) (k 0)) (d (n "freetype-rs") (r "*") (d #t) (k 0)) (d (n "glyph_packer") (r "^0.0.0") (d #t) (k 0)) (d (n "image") (r "*") (d #t) (k 0)))) (h "02yip1h4vckklf1nmg90j9fiy4y8qia858zwfyc6z30w25hhxr13")))

