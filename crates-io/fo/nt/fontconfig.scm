(define-module (crates-io fo nt fontconfig) #:use-module (crates-io))

(define-public crate-fontconfig-0.0.1 (c (n "fontconfig") (v "0.0.1") (d (list (d (n "fontconfig-sys") (r "~2.11.1") (d #t) (k 0)))) (h "0gv78wjmq6hkvk821r61h95n61643jmnrvdj7n3wm3i5z0zs3m02")))

(define-public crate-fontconfig-0.2.0 (c (n "fontconfig") (v "0.2.0") (d (list (d (n "yeslogic-fontconfig-sys") (r "^2.11.1") (d #t) (k 0)))) (h "0ngj5vzsvsy7z6c6whzqigfrzkmqng230ick64dn5yszwdy0wmx5")))

(define-public crate-fontconfig-0.2.1 (c (n "fontconfig") (v "0.2.1") (d (list (d (n "yeslogic-fontconfig-sys") (r "^2.11.2") (d #t) (k 0)))) (h "1k4x4mff7yf9gnsl77dpkyjgyyw14x1w0mfxs0r7564k7smi5i6p")))

(define-public crate-fontconfig-0.3.0 (c (n "fontconfig") (v "0.3.0") (d (list (d (n "yeslogic-fontconfig-sys") (r "^3.0.0") (d #t) (k 0)))) (h "0kj6cfm9zjrnw0vcrmp7vryin9yhhysy6xfdv2xa6idzl0i6pprn") (f (quote (("dlopen" "yeslogic-fontconfig-sys/dlopen")))) (r "1.46")))

(define-public crate-fontconfig-0.4.0 (c (n "fontconfig") (v "0.4.0") (d (list (d (n "yeslogic-fontconfig-sys") (r "^3.0.0") (d #t) (k 0)))) (h "0msicjyjfikliwjsxjw16jpsalin1q4zqwkfvymscz4fh32z07vw") (f (quote (("dlopen" "yeslogic-fontconfig-sys/dlopen")))) (r "1.46")))

(define-public crate-fontconfig-0.5.0 (c (n "fontconfig") (v "0.5.0") (d (list (d (n "yeslogic-fontconfig-sys") (r "^3.0.0") (d #t) (k 0)))) (h "0iwvwjhh2jz64zn43cxv0ja10893rmyxv5dkl2npi5aag2nj0lx2") (f (quote (("dlopen" "yeslogic-fontconfig-sys/dlopen")))) (r "1.46")))

(define-public crate-fontconfig-0.6.0 (c (n "fontconfig") (v "0.6.0") (d (list (d (n "yeslogic-fontconfig-sys") (r "^4.0.0") (d #t) (k 0)))) (h "0isqa9yx3im9my6bwcwng9pi58s9gg40vkdsjmgn90lw5mc561lv") (f (quote (("dlopen" "yeslogic-fontconfig-sys/dlopen")))) (r "1.56")))

(define-public crate-fontconfig-0.7.0 (c (n "fontconfig") (v "0.7.0") (d (list (d (n "yeslogic-fontconfig-sys") (r "^5.0.0") (d #t) (k 0)))) (h "1g7x6hhy1r3iji6h404aj8fkk93j1rr1gkrih7v8l3s0dy46mp2f") (f (quote (("dlopen" "yeslogic-fontconfig-sys/dlopen")))) (r "1.64")))

(define-public crate-fontconfig-0.8.0 (c (n "fontconfig") (v "0.8.0") (d (list (d (n "yeslogic-fontconfig-sys") (r "^5.0.0") (d #t) (k 0)))) (h "1nb2dklqp5gwab0b15d1pmwlja8h3mmkf9z9g7m4iq2ak9hrpdy9") (f (quote (("dlopen" "yeslogic-fontconfig-sys/dlopen")))) (r "1.64")))

