(define-module (crates-io fo nt fontgen) #:use-module (crates-io))

(define-public crate-fontgen-0.1.0 (c (n "fontgen") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fontgen-export") (r "^0.1.0") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.24.0") (d #t) (k 0)) (d (n "png") (r "^0.16.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "125kcp8636wrssvnq95n5lqdlqn7kq2jrhnr9p3xbm8rjgffqmmr")))

(define-public crate-fontgen-0.2.0 (c (n "fontgen") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fontgen-export") (r "^0.2.0") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.24.0") (d #t) (k 0)) (d (n "png") (r "^0.16.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "09wkik6a88rylvybq0s4fz5m1nicirv55qfjwrdpkkaqs97zljp6")))

