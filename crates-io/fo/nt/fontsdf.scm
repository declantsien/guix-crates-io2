(define-module (crates-io fo nt fontsdf) #:use-module (crates-io))

(define-public crate-fontsdf-0.1.0 (c (n "fontsdf") (v "0.1.0") (d (list (d (n "fontdue") (r "^0.7") (d #t) (k 0)) (d (n "glam") (r "^0.20") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "ttf-parser") (r "^0.15") (d #t) (k 0)))) (h "133w86znmgymr8f2iqyr4pag7a1ymrcs9bbfx5y9zjlh4p3ky5aq")))

(define-public crate-fontsdf-0.2.0 (c (n "fontsdf") (v "0.2.0") (d (list (d (n "fontdue") (r "^0.7") (d #t) (k 0)) (d (n "glam") (r "^0.20") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "ttf-parser") (r "^0.15") (d #t) (k 0)))) (h "0k0d5z9pwpmr39qb8lfv9vpd76qjqjr93kvf8xndw4pi44nwx22x")))

(define-public crate-fontsdf-0.3.0 (c (n "fontsdf") (v "0.3.0") (d (list (d (n "fontdue") (r "^0.7") (d #t) (k 0)) (d (n "glam") (r "^0.20") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "ttf-parser") (r "^0.15") (d #t) (k 0)))) (h "130p98mzn39rhkh5p002dd4hlzkqkh2n9m0583gw8vgsjw9p6grd")))

(define-public crate-fontsdf-0.4.0 (c (n "fontsdf") (v "0.4.0") (d (list (d (n "fontdue") (r "^0.7") (d #t) (k 0)) (d (n "glam") (r "^0.20") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "ttf-parser") (r "^0.15") (d #t) (k 0)))) (h "1ha640pwr3pk751qxf7nfyihi6shraypljk4bj61i85hs8py2i2s")))

(define-public crate-fontsdf-0.4.1 (c (n "fontsdf") (v "0.4.1") (d (list (d (n "fontdue") (r "^0.7") (d #t) (k 0)) (d (n "glam") (r "^0.20") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "ttf-parser") (r "^0.15") (d #t) (k 0)))) (h "0kr7br7pn1inrvc2h9fdq29jmm00jww5xh2s4rlyhdr0f8y3wwhb")))

(define-public crate-fontsdf-0.4.2 (c (n "fontsdf") (v "0.4.2") (d (list (d (n "fontdue") (r "^0.7") (d #t) (k 0)) (d (n "glam") (r "^0.20") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "ttf-parser") (r "^0.15") (d #t) (k 0)))) (h "0lfd6g71sg5vzp3p9i8id1j8a170asww0wb1rl9x8dkrlw6g65qd")))

(define-public crate-fontsdf-0.4.3 (c (n "fontsdf") (v "0.4.3") (d (list (d (n "fontdue") (r "^0.7") (d #t) (k 0)) (d (n "glam") (r "^0.20") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "ttf-parser") (r "^0.15") (d #t) (k 0)))) (h "05fzshxbxhax8bhn6b4vgwr82b8ghfkb3cniyn3n79r9z6bddm4z")))

(define-public crate-fontsdf-0.4.4 (c (n "fontsdf") (v "0.4.4") (d (list (d (n "fontdue") (r "^0.7") (d #t) (k 0)) (d (n "glam") (r "^0.20") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "ttf-parser") (r "^0.15") (d #t) (k 0)))) (h "09xshx3c0i3n8yzbix5lfi6k63vvrc84wpbs3qirq0m04nric66p")))

(define-public crate-fontsdf-0.4.5 (c (n "fontsdf") (v "0.4.5") (d (list (d (n "fontdue") (r "^0.7") (d #t) (k 0)) (d (n "glam") (r "^0.20") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "ttf-parser") (r "^0.15") (d #t) (k 0)))) (h "0zjsskmgdfz9abapl8hbjsk05xd08hw15diwfd7hxdwgrj3lnapz")))

(define-public crate-fontsdf-0.4.6 (c (n "fontsdf") (v "0.4.6") (d (list (d (n "fontdue") (r "^0.7") (d #t) (k 0)) (d (n "glam") (r "^0.21") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "ttf-parser") (r "^0.15") (d #t) (k 0)))) (h "0azab1k101hr9py9zrmd9q66cma0vvc285c67i9rds1lwdxvf916")))

(define-public crate-fontsdf-0.4.7 (c (n "fontsdf") (v "0.4.7") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "fontdue") (r "^0.7") (d #t) (k 0)) (d (n "glam") (r "^0.21") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "ttf-parser") (r "^0.17") (d #t) (k 0)))) (h "0l0n59903r8xsvk9z4b71cmm992gwma5yxgzry35mf1ryam1a5gc")))

(define-public crate-fontsdf-0.5.0 (c (n "fontsdf") (v "0.5.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "fontdue") (r "^0.7") (d #t) (k 0)) (d (n "glam") (r "^0.21") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "ordered-float") (r "^3.0") (d #t) (k 0)) (d (n "ttf-parser") (r "^0.17") (d #t) (k 0)))) (h "1p6hbms4xzd1h2yk87iv24c7ipsihjc6lf7qm0sxrn9lmznb7mb4")))

(define-public crate-fontsdf-0.5.1 (c (n "fontsdf") (v "0.5.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "fontdue") (r "^0.7") (d #t) (k 0)) (d (n "glam") (r "^0.23") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "ordered-float") (r "^3.6") (d #t) (k 0)) (d (n "ttf-parser") (r "^0.19") (d #t) (k 0)))) (h "1mxs5ajzpc23bfzlxk7rqjbzmflzg40p4h0y5xzxahdm4y2k37kz")))

