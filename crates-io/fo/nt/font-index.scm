(define-module (crates-io fo nt font-index) #:use-module (crates-io))

(define-public crate-font-index-0.1.0 (c (n "font-index") (v "0.1.0") (d (list (d (n "fontconfig-parser") (r "^0.5") (d #t) (t "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"android\"))))") (k 0)) (d (n "memmap2") (r "^0.2.1") (d #t) (k 0)) (d (n "swash") (r "^0.1") (d #t) (k 0)))) (h "0yxccdx9vdi45w41asnzl639qg8p57ydnjmvd776b18j0m1iw6nm") (y #t)))

(define-public crate-font-index-0.1.1 (c (n "font-index") (v "0.1.1") (d (list (d (n "fontconfig-parser") (r "^0.5") (d #t) (t "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"android\"))))") (k 0)) (d (n "memmap2") (r "^0.2.1") (d #t) (k 0)) (d (n "swash") (r "^0.1") (d #t) (k 0)))) (h "1ym53yf6nvfikfnwpk2ns04gqm0k1x3aj4cnkq8ffj01rh3x9cl4")))

(define-public crate-font-index-0.1.2 (c (n "font-index") (v "0.1.2") (d (list (d (n "fontconfig-parser") (r "^0.5") (d #t) (t "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"android\"))))") (k 0)) (d (n "memmap2") (r "^0.2.1") (d #t) (k 0)) (d (n "swash") (r "^0.1") (d #t) (k 0)))) (h "1nzdf7xvj2z4vkbvlm83ckqv04vjd7vdyw02dljrm5fqygz9vpg9") (y #t)))

(define-public crate-font-index-0.1.3 (c (n "font-index") (v "0.1.3") (d (list (d (n "fontconfig-parser") (r "^0.5") (d #t) (t "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"android\"))))") (k 0)) (d (n "memmap2") (r "^0.2.1") (d #t) (k 0)) (d (n "swash") (r "^0.1") (d #t) (k 0)))) (h "1xl7p22x6d328bb3qavi29mrqcqrhnllxi3hpr7dq19zzdxpkabi")))

(define-public crate-font-index-0.1.4 (c (n "font-index") (v "0.1.4") (d (list (d (n "fontconfig-parser") (r "^0.5") (d #t) (t "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"android\"))))") (k 0)) (d (n "memmap2") (r "^0.2.1") (d #t) (k 0)) (d (n "swash") (r "^0.1") (d #t) (k 0)))) (h "0v1wygydlz1lrlsvzwf5hpinify1fqw9p7ar9756fk8b0aykhirc")))

(define-public crate-font-index-0.1.5 (c (n "font-index") (v "0.1.5") (d (list (d (n "fontconfig-parser") (r "^0.5") (d #t) (t "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"android\"))))") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap2") (r "^0.2.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (o #t) (d #t) (k 0)) (d (n "swash") (r "^0.1") (d #t) (k 0)))) (h "1n4ymd6gzzqn6sl35w8ds2rjpq6mrbvvi2bmgmx0bbjp1icxvcp6") (f (quote (("default" "emacs")))) (s 2) (e (quote (("emacs" "dep:once_cell"))))))

(define-public crate-font-index-0.1.6 (c (n "font-index") (v "0.1.6") (d (list (d (n "fontconfig-parser") (r "^0.5") (d #t) (t "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"android\"))))") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap2") (r "^0.2.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (o #t) (d #t) (k 0)) (d (n "swash") (r "^0.1") (d #t) (k 0)))) (h "0n7qdb4p47lnwa3124vmgf89dl312vb43xr5cpjbnb1nzdgyc1a8") (f (quote (("default" "emacs")))) (s 2) (e (quote (("emacs" "dep:once_cell"))))))

(define-public crate-font-index-0.2.0 (c (n "font-index") (v "0.2.0") (d (list (d (n "fancy-regex") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "fontconfig-parser") (r "^0.5") (d #t) (t "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"android\"))))") (k 0)) (d (n "isolang") (r "^2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap2") (r "^0.2.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (o #t) (d #t) (k 0)) (d (n "swash") (r "^0.1") (d #t) (k 0)))) (h "0xd91dybmrxn5bxl0yi5h5lmvg52xamv7s0qx6029i2qbn1rabjv") (f (quote (("default" "emacs")))) (s 2) (e (quote (("emacs" "dep:once_cell" "dep:isolang" "dep:fancy-regex"))))))

