(define-module (crates-io fo nt fonterator) #:use-module (crates-io))

(define-public crate-fonterator-0.1.0 (c (n "fonterator") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "svg") (r "^0.5") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1axfd6s1zghvzlng62l6akrvwpgq2md2hjcpcs226r29ni77jqbd")))

(define-public crate-fonterator-0.2.0-pre0 (c (n "fonterator") (v "0.2.0-pre0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "footile") (r "^0.2") (d #t) (k 0)) (d (n "svg") (r "^0.5") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "13nqb6n47449dxmgk5vnhhywgqbr9kmcnsqs584gcxip31chshb8")))

(define-public crate-fonterator-0.2.0 (c (n "fonterator") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "footile") (r "^0.2") (d #t) (k 0)) (d (n "svg") (r "^0.5") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0zbq4jf5cjwcchggrx46bvj8r5gjrvk8bddiwa2hkg2rvd6n5d4r")))

(define-public crate-fonterator-0.2.1 (c (n "fonterator") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "footile") (r "^0.2") (d #t) (k 0)) (d (n "svg") (r "^0.5") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0m98fgqyvadfaf7zdy2jczxi481ii8vqppq83b28fbirgvv5kn7h")))

(define-public crate-fonterator-0.3.0 (c (n "fonterator") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "footile") (r "^0.2") (d #t) (k 0)) (d (n "svg") (r "^0.5") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0andp80ma43rnh37z04pqdjfbjp08qh2rqmi07aii6g8svmqi1k6") (f (quote (("default" "builtin-font") ("builtin-font"))))))

(define-public crate-fonterator-0.4.0 (c (n "fonterator") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "footile") (r "^0.3") (d #t) (k 0)) (d (n "svg") (r "^0.5") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0rn1rgrk5rlvzmq9s1p7wqbjmx69jw3c20harr28rncm9y5vg7sa") (f (quote (("default" "builtin-font") ("builtin-font"))))))

(define-public crate-fonterator-0.4.1 (c (n "fonterator") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "footile") (r "^0.3") (d #t) (k 0)) (d (n "svg") (r "^0.5") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1mvsr6c6y9918x4fhzhw2070gcgfs65hh3br5gmdnk5g92r6rrlb") (f (quote (("default" "builtin-font") ("builtin-font"))))))

(define-public crate-fonterator-0.4.2 (c (n "fonterator") (v "0.4.2") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "footile") (r "^0.3") (d #t) (k 0)) (d (n "svg") (r "^0.5") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "11d0b0fb38pvw5vyj0jfc7y555bwwvrk4g1alcp3lhv2mb634g48") (f (quote (("default" "builtin-font") ("builtin-font"))))))

(define-public crate-fonterator-0.5.0 (c (n "fonterator") (v "0.5.0") (d (list (d (n "footile") (r "^0.3") (d #t) (k 0)) (d (n "footile") (r "^0.3") (d #t) (k 2)) (d (n "png_pong") (r "^0.0.2") (d #t) (k 2)) (d (n "svg") (r "^0.5") (d #t) (k 2)) (d (n "ttf-parser") (r "^0.2") (d #t) (k 0)) (d (n "unicode-script") (r "^0.3") (d #t) (k 0)))) (h "06fivh5c1b0z0hpxpndppp4wgj810jw8i8wyh7r2kjkd33ga69rm") (f (quote (("normal-font") ("monospace-font") ("default" "monospace-font"))))))

(define-public crate-fonterator-0.5.1 (c (n "fonterator") (v "0.5.1") (d (list (d (n "footile") (r "^0.3") (d #t) (k 0)) (d (n "footile") (r "^0.3") (d #t) (k 2)) (d (n "png_pong") (r "^0.0.2") (d #t) (k 2)) (d (n "svg") (r "^0.5") (d #t) (k 2)) (d (n "ttf-parser") (r "^0.2") (d #t) (k 0)) (d (n "unicode-script") (r "^0.3") (d #t) (k 0)))) (h "0mld8hpjbxl5zahkjmmmrrwscyya21is6vqj4zsmvbff87wdld68") (f (quote (("normal-font") ("monospace-font") ("default" "monospace-font"))))))

(define-public crate-fonterator-0.6.0 (c (n "fonterator") (v "0.6.0") (d (list (d (n "footile") (r "^0.3") (d #t) (k 0)) (d (n "footile") (r "^0.3") (d #t) (k 2)) (d (n "png_pong") (r "^0.0.2") (d #t) (k 2)) (d (n "svg") (r "^0.6") (d #t) (k 2)) (d (n "ttf-parser") (r "^0.3") (d #t) (k 0)) (d (n "unicode-script") (r "^0.3") (d #t) (k 0)))) (h "1w6mpq6wmz9r1ib055pdxx32h5wqf5kk0lwas10knjqpnsghb4mp") (f (quote (("normal-font") ("monospace-font") ("default" "monospace-font"))))))

(define-public crate-fonterator-0.7.0 (c (n "fonterator") (v "0.7.0") (d (list (d (n "footile") (r "^0.4") (d #t) (k 0)) (d (n "pix") (r "^0.12") (d #t) (k 2)) (d (n "png_pong") (r "^0.4") (d #t) (k 2)) (d (n "svg") (r "^0.8") (d #t) (k 2)) (d (n "ttf-parser") (r "^0.5") (k 0)) (d (n "unicode-script") (r "^0.5") (d #t) (k 0)))) (h "1l0acjk4syrn30w40nxn1s6ymawm97rgkx5n3r01vgjypjpwl8dh") (f (quote (("normal-font") ("monospace-font") ("docs-rs") ("default"))))))

(define-public crate-fonterator-0.8.0 (c (n "fonterator") (v "0.8.0") (d (list (d (n "footile") (r "^0.5") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 2)) (d (n "png_pong") (r "^0.5") (d #t) (k 2)) (d (n "svg") (r "^0.8") (d #t) (k 2)) (d (n "ttf-parser") (r "^0.6") (d #t) (k 0)) (d (n "unicode-script") (r "^0.5") (d #t) (k 0)))) (h "0msdn18yj6iazg9rl6zn3dkgsw4pwkl9nym25shlyx0xqmnr4nrz") (f (quote (("normal-font") ("monospace-font") ("docs-rs") ("default"))))))

(define-public crate-fonterator-0.9.0 (c (n "fonterator") (v "0.9.0") (d (list (d (n "footile") (r "^0.6") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 2)) (d (n "png_pong") (r "^0.7") (d #t) (k 2)) (d (n "svg") (r "^0.8") (d #t) (k 2)) (d (n "ttf-parser") (r "^0.8") (d #t) (k 0)) (d (n "unicode-script") (r "^0.5") (d #t) (k 0)))) (h "14rfhms66qz2js7h9v3d05nirzglfz7x2b8349p1gd6zrlkarmdk") (f (quote (("normal-font") ("monospace-font") ("docs-rs") ("default"))))))

