(define-module (crates-io fo nt fonticulus) #:use-module (crates-io))

(define-public crate-fonticulus-0.1.0 (c (n "fonticulus") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "designspace") (r "^0") (f (quote ("norad"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "fonttools") (r "^0") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "kurbo") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.1") (d #t) (k 0)) (d (n "norad") (r "^0.4") (f (quote ("rayon" "kurbo"))) (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)))) (h "1bsqfnqhqp42v26qfd0fyfv1cpaby18i49rl6075b8hmjp6sc93r")))

