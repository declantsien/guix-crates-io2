(define-module (crates-io fo nt fontconfig-cache-parser) #:use-module (crates-io))

(define-public crate-fontconfig-cache-parser-0.1.0 (c (n "fontconfig-cache-parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "02332iais72v7b6h5dmh4vrgp01rc76031l3pxss46p9q6ry81g8")))

(define-public crate-fontconfig-cache-parser-0.1.1 (c (n "fontconfig-cache-parser") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0q12m6lazawfhf16jymc32sihi3ml4bjkhankwxfmr26f63i616v")))

(define-public crate-fontconfig-cache-parser-0.2.0 (c (n "fontconfig-cache-parser") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 2)) (d (n "bytemuck") (r "^1.12.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "061vdyh4j2x9pa3mmlh5lqcwcz1kk9ai9ci7dmkzssc01jrazy57")))

