(define-module (crates-io fo nt font8x8) #:use-module (crates-io))

(define-public crate-font8x8-0.1.0 (c (n "font8x8") (v "0.1.0") (h "07hkdvwfcjwl5vmqfqisds472mh5w9n0dqnpp1n87rq1dqk4nf14")))

(define-public crate-font8x8-0.1.1 (c (n "font8x8") (v "0.1.1") (h "0fi2l93vyzh9a8hl9xxl8rm6jywnc6a7dfn5d3vk9am7m7c4pkda")))

(define-public crate-font8x8-0.1.2 (c (n "font8x8") (v "0.1.2") (h "1y8i62c3bjnpwfv6p0r176n3cnm53nc2p7gvl02jwa9z1nnvglk7") (f (quote (("unicode") ("default" "unicode"))))))

(define-public crate-font8x8-0.1.3 (c (n "font8x8") (v "0.1.3") (h "0q41d60amz7lqz6a7fsa442v6vc1ff4ln5iywx335vk7gbzcx34q") (f (quote (("unicode") ("default" "unicode"))))))

(define-public crate-font8x8-0.1.4 (c (n "font8x8") (v "0.1.4") (h "0x5pllldlqhls2pkzs4s5h2fjmmhpr0ppmkcr2rai34z54sl5r0s") (f (quote (("unicode") ("default" "unicode"))))))

(define-public crate-font8x8-0.1.5 (c (n "font8x8") (v "0.1.5") (h "1z5jxgqy2z8rf25wbpi3505c802j32sjm322cn0g8mg3056dcqwl") (f (quote (("unicode") ("default" "unicode"))))))

(define-public crate-font8x8-0.1.6 (c (n "font8x8") (v "0.1.6") (h "0kyhdm7nrra34kfydh3n7y1xvihby2dailhyn1b9w26f5mnzxyjx") (f (quote (("unicode") ("std") ("default" "unicode" "std"))))))

(define-public crate-font8x8-0.2.0 (c (n "font8x8") (v "0.2.0") (h "1n5ylgh9nwppgnhw0j7bn1q1cr42wc1bmzpbak78wdf7pyk2qx6d") (f (quote (("unicode") ("std") ("default" "unicode" "std"))))))

(define-public crate-font8x8-0.2.1 (c (n "font8x8") (v "0.2.1") (h "04pibaznmrdi3lnianppxlipm7bq1x7qqhnvzbs205gsxsih43z9") (f (quote (("unicode") ("std") ("default" "unicode" "std"))))))

(define-public crate-font8x8-0.2.2 (c (n "font8x8") (v "0.2.2") (h "1brl9q1q52k3508f6f0szvcdh6fwfd0iry8mbg6lybi0wwxzizix") (f (quote (("unicode") ("std") ("default" "unicode" "std"))))))

(define-public crate-font8x8-0.2.3 (c (n "font8x8") (v "0.2.3") (h "167yf7iipn2fqp3w9v2z7w5kdhl6m8vlif9pyv646dk3d8bb5g9z") (f (quote (("unicode") ("std") ("default" "unicode" "std"))))))

(define-public crate-font8x8-0.2.4 (c (n "font8x8") (v "0.2.4") (h "07ywz0rhw5gwb1nqypfi37c5dacv9fprh88ssc2pvbvqr71q87dq") (f (quote (("unicode") ("std") ("default" "unicode" "std"))))))

(define-public crate-font8x8-0.2.5 (c (n "font8x8") (v "0.2.5") (h "1gpiv08g0pkvv8gdlv9rbr1w6w25ahdqy7aa6h1ddccz9106q8j4") (f (quote (("unicode") ("std") ("default" "unicode" "std"))))))

(define-public crate-font8x8-0.2.6 (c (n "font8x8") (v "0.2.6") (h "0zp1ygpv4b9lm0386db4mg1b8dhfj62m71n0lqkai62546v81y8s") (f (quote (("unicode") ("std") ("default" "unicode" "std"))))))

(define-public crate-font8x8-0.2.7 (c (n "font8x8") (v "0.2.7") (h "1gvbp12ykpg44qmi4pdipyfzmi0ni76ar8di44wqij5q4k302cp6") (f (quote (("unicode") ("std") ("default" "unicode" "std"))))))

(define-public crate-font8x8-0.3.0 (c (n "font8x8") (v "0.3.0") (h "08jsnwyqjwbrs7fr4rr8yj2g9cqssxx8znfl2incyjdgn056l6wa") (f (quote (("unicode") ("std") ("default" "unicode" "std"))))))

(define-public crate-font8x8-0.3.1 (c (n "font8x8") (v "0.3.1") (h "0znh67wfv3pyx12619rm8v59fc3ig2ai6pfgqxl855hsf6w8hm47") (f (quote (("unicode") ("std") ("default" "unicode" "std"))))))

