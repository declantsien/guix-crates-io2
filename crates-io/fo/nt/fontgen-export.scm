(define-module (crates-io fo nt fontgen-export) #:use-module (crates-io))

(define-public crate-fontgen-export-0.1.0 (c (n "fontgen-export") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "1w0xv6aj8cpjb3lmsbyll16dll68yk267hwpc1p1bdaz3l7p0w38")))

(define-public crate-fontgen-export-0.2.0 (c (n "fontgen-export") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)))) (h "15lsgcx6al6rqi54bk1b6d0zay8yym0k7gcillngacwhphqxr8qw")))

