(define-module (crates-io fo nt fontstash) #:use-module (crates-io))

(define-public crate-fontstash-0.1.2 (c (n "fontstash") (v "0.1.2") (d (list (d (n "fontstash-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0sy13z4dbnwcf63vpgydingxwidbafr9r9m1zv7797fsc9vzwnh7")))

(define-public crate-fontstash-0.1.3 (c (n "fontstash") (v "0.1.3") (d (list (d (n "fontstash-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0bqz6yv3r3y57kwfdpwnk843kqi7k0gvjihd9ka7xrr4rw2rrvng")))

(define-public crate-fontstash-0.1.5 (c (n "fontstash") (v "0.1.5") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "fontstash-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0a2smh9d3qn9i037z6nxvx784qwk8bsnzga785m9k40yh4y32iqy")))

(define-public crate-fontstash-0.1.6 (c (n "fontstash") (v "0.1.6") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "fontstash-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0kw05l3srbaqr2c9qgg4b1abh6l3ciqyr14v8md0267ds1y2sx3g")))

