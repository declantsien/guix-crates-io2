(define-module (crates-io fo nt font-rs) #:use-module (crates-io))

(define-public crate-font-rs-0.1.0 (c (n "font-rs") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0sijp6vcrvlyvmvn8za1z5s26ah4znn4vpfv4n6y16wyb0gxdkwn") (f (quote (("sse"))))))

(define-public crate-font-rs-0.1.1 (c (n "font-rs") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "03np4m53w2wn7lkpv3vax6dy3ran6d5q49w30a2xd5gjw1fwdxhb") (f (quote (("sse"))))))

(define-public crate-font-rs-0.1.2 (c (n "font-rs") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1s8h58drdffbsfdhhdv4k5pfixkx9f8rc2ba46b09pidwqzmgf2c") (f (quote (("sse"))))))

(define-public crate-font-rs-0.1.3 (c (n "font-rs") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (t "cfg(target_feature = \"sse\")") (k 1)))) (h "1lkcswgvwq8m53lch70j7qkqbc696njlkk69jb69ixqfgfcrv4rk") (f (quote (("sse"))))))

