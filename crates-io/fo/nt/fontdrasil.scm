(define-module (crates-io fo nt fontdrasil) #:use-module (crates-io))

(define-public crate-fontdrasil-0.0.1 (c (n "fontdrasil") (v "0.0.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 2)) (d (n "diff") (r "^0.1.12") (d #t) (k 2)) (d (n "ordered-float") (r "^4.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "smol_str") (r "^0.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "write-fonts") (r "^0.25.0") (f (quote ("serde" "read"))) (d #t) (k 0)))) (h "11z0ic3qb38mp8bpv5r1b172qk1hdzjflj54y36wf92gw253z2gz")))

