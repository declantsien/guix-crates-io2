(define-module (crates-io fo nt font_metrics) #:use-module (crates-io))

(define-public crate-font_metrics-0.0.2 (c (n "font_metrics") (v "0.0.2") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "lopdf") (r "^0.15") (d #t) (k 0)) (d (n "num-bigint") (r "^0.1") (d #t) (k 0)) (d (n "num-rational") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pdf-extract") (r "^0.4") (d #t) (k 0)) (d (n "rusttype") (r "^0.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "11146n4gcm5xh78gc2gr4rfm1j1qgs59w4rdk0a9ivr1w8lrvkk6")))

(define-public crate-font_metrics-0.0.3 (c (n "font_metrics") (v "0.0.3") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "lopdf") (r "^0.15") (d #t) (k 0)) (d (n "num-rational") (r "^0.1") (d #t) (k 0)) (d (n "pdf-extract") (r "^0.4") (d #t) (k 0)) (d (n "rusttype") (r "^0.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1p932nmp808qpafg83wwd3x8paffd0vjqxl3wl5z35nzp9xlav6y")))

(define-public crate-font_metrics-0.0.4 (c (n "font_metrics") (v "0.0.4") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "lopdf") (r "^0.15") (d #t) (k 0)) (d (n "num-rational") (r "^0.1") (d #t) (k 0)) (d (n "pdf-extract") (r "^0.4") (d #t) (k 0)) (d (n "rusttype") (r "^0.6") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "16cdfaz36ll2nq1qwjf2xs2xhj8wwnl2kfmxrnd5n64bd2wd735l")))

