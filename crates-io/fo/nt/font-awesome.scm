(define-module (crates-io fo nt font-awesome) #:use-module (crates-io))

(define-public crate-font-awesome-0.1.0 (c (n "font-awesome") (v "0.1.0") (h "11ds8x5nji7j47bm52pc8l9vz1jcd9ibz6rif1xqxdif38xknn5s")))

(define-public crate-font-awesome-0.2.0 (c (n "font-awesome") (v "0.2.0") (h "0wj33wpv4phvxc82qf1wjn1806gnwv2j2iscsrd398srv70zxrnk")))

