(define-module (crates-io fo nt fontdue-sdl2) #:use-module (crates-io))

(define-public crate-fontdue-sdl2-0.1.0 (c (n "fontdue-sdl2") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.5.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "fontdue") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "sdl2") (r "^0.34") (d #t) (k 0)))) (h "1sd4d7yb41ad9vh5y7g36w7dyd1q8qs46ynvwbka9660qqi5p7pm")))

(define-public crate-fontdue-sdl2-0.1.1 (c (n "fontdue-sdl2") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "fontdue") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "sdl2") (r "^0.34") (d #t) (k 0)))) (h "01880gw2crahcn48r62rqnq0v4c57b0mwh6wdlpx2im0vba30sk9")))

(define-public crate-fontdue-sdl2-0.2.0 (c (n "fontdue-sdl2") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "fontdue") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (d #t) (k 0)))) (h "0kf8in7kxr501irxi8kwd1d78yvyrw3mypsf7q8d1xkh6mh6lfdn")))

(define-public crate-fontdue-sdl2-0.3.0 (c (n "fontdue-sdl2") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "fontdue") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (d #t) (k 0)))) (h "0xh850js8087i0n421925brpn72g2m9p7hyh4zr4a03nq98gsgqq") (f (quote (("unsafe_textures" "sdl2/unsafe_textures"))))))

(define-public crate-fontdue-sdl2-0.3.1 (c (n "fontdue-sdl2") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "fontdue") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "sdl2") (r "^0.36") (d #t) (k 0)))) (h "0arb53ksnvlkf4xiq1qnbz2i30p058b27vjp6w3fkfxgvq2qa5s6") (f (quote (("unsafe_textures" "sdl2/unsafe_textures"))))))

