(define-module (crates-io fo nt font_7seg) #:use-module (crates-io))

(define-public crate-font_7seg-0.0.1 (c (n "font_7seg") (v "0.0.1") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0k4cwfgwpf615kv126nkbm7nny5v0mz88qx37ik0fa1dznsvx2sb")))

(define-public crate-font_7seg-0.0.2 (c (n "font_7seg") (v "0.0.2") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0lf1f3zwz9slkh007sai2jxhdwbr13aldxcahiwldbr31prncx15")))

(define-public crate-font_7seg-0.0.3 (c (n "font_7seg") (v "0.0.3") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1v7mpy34jc1d6cmx015ap22m7zjzhncqqz74vsrqvv0gpky92yrl")))

(define-public crate-font_7seg-0.0.4 (c (n "font_7seg") (v "0.0.4") (d (list (d (n "embedded-graphics") (r "^0.7.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0hh451xa981br4rw0yf390l1pjyfaxfnn3dvhzfnd081zfszq6hw")))

(define-public crate-font_7seg-0.0.5 (c (n "font_7seg") (v "0.0.5") (d (list (d (n "embedded-graphics") (r "^0.8.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0fa2apr91182j9x13fjqkivwda5a6pdqrl4b4rxkrnmapfnjr9qh")))

