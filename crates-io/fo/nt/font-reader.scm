(define-module (crates-io fo nt font-reader) #:use-module (crates-io))

(define-public crate-font-reader-0.1.0 (c (n "font-reader") (v "0.1.0") (d (list (d (n "binary-reader") (r "^0.3.0") (d #t) (k 0)))) (h "0vxyqlhqm1fxgq6xdxxshlfdzx6n8fdpdsv1vqz1la43m2ybk9kx")))

(define-public crate-font-reader-0.1.1 (c (n "font-reader") (v "0.1.1") (d (list (d (n "binary-reader") (r "^0.3.0") (d #t) (k 0)))) (h "06442nckwhh29xa6lcgxrz7kqyvpjn9yqz9chs5ssna59h2kai2v")))

(define-public crate-font-reader-0.1.3 (c (n "font-reader") (v "0.1.3") (d (list (d (n "binary-reader") (r "^0.4.0") (d #t) (k 0)))) (h "0mvjl3867phia8y07rvlssw3fjywvbqm54hf6l9wjf0kqvwk0ynw")))

(define-public crate-font-reader-0.1.4 (c (n "font-reader") (v "0.1.4") (d (list (d (n "binary-reader") (r "^0.4.0") (d #t) (k 0)))) (h "04pqghlmysrwlyxbhxsbqvsss49dicyvadgcjnq1vp2j2wp0zzjm")))

(define-public crate-font-reader-0.1.5 (c (n "font-reader") (v "0.1.5") (d (list (d (n "binary-reader") (r "^0.4.0") (d #t) (k 0)))) (h "1biq24g0m6nz88chkz50lg3bbhb8y0f0g7z9jv8js9jhb0vqrpvf")))

(define-public crate-font-reader-0.1.6 (c (n "font-reader") (v "0.1.6") (d (list (d (n "binary-reader") (r "^0.4.0") (d #t) (k 0)))) (h "04dja0rh8plbh58x5djsbrn0j9vya73i4mwjl8lkywki8jbmyk53")))

(define-public crate-font-reader-0.1.7 (c (n "font-reader") (v "0.1.7") (d (list (d (n "binary-reader") (r "^0.4.0") (d #t) (k 0)))) (h "0bpwpzj2m20n9xb55vihvhg412h3av1fdfdj4fggaadmf0z1nskd")))

(define-public crate-font-reader-0.1.8 (c (n "font-reader") (v "0.1.8") (d (list (d (n "binary-reader") (r "^0.4.0") (d #t) (k 0)))) (h "1r5d7zhp0aaxhnkqpxy94dby4n9drlmgd3x3qv3k1cwgdjjcg87h")))

(define-public crate-font-reader-0.1.9 (c (n "font-reader") (v "0.1.9") (d (list (d (n "binary-reader") (r "^0.4.0") (d #t) (k 0)))) (h "1p25b8pwvr93qj4v7zawdky2p34cwpsbsp75ik8gfffx00d7ngb2")))

(define-public crate-font-reader-0.1.10 (c (n "font-reader") (v "0.1.10") (d (list (d (n "binary-reader") (r "^0.4.0") (d #t) (k 0)))) (h "1w7b39rmxk3my5apmbwflbj027z8za7s28a38zdjzgjhznsx97p2")))

