(define-module (crates-io fo nt fontstash-sys) #:use-module (crates-io))

(define-public crate-fontstash-sys-0.1.0 (c (n "fontstash-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.60") (d #t) (k 1)))) (h "0572dznmzp81iwfh2243qa76slx4nbqkvl7wdy0wcksdb324qxps") (l "fontstash")))

(define-public crate-fontstash-sys-0.1.1 (c (n "fontstash-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.60") (d #t) (k 1)))) (h "1763jvps3a66rirj4269bhf5nzf9fbk1nbc7zx9n5mxb07rsbxsl") (l "fontstash")))

