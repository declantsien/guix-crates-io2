(define-module (crates-io fo nt font2png) #:use-module (crates-io))

(define-public crate-font2png-0.1.0 (c (n "font2png") (v "0.1.0") (d (list (d (n "colors-transform") (r "^0.2.5") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1vhjdkwpqmylfmxpcf0ccrl37zs37blgimxrff2rhrllcmpvy5na") (y #t)))

(define-public crate-font2png-1.0.0 (c (n "font2png") (v "1.0.0") (d (list (d (n "colors-transform") (r "^0.2.5") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1hkmnmv1pjfgy2m3k29s4xsf7jwbznr55x28accjr4zqsfngb78l")))

