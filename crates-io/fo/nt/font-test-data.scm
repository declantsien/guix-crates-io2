(define-module (crates-io fo nt font-test-data) #:use-module (crates-io))

(define-public crate-font-test-data-0.1.0 (c (n "font-test-data") (v "0.1.0") (h "0y1dxqpgv1i3v22vh1zsnfd7wrhav88653836jlf0lk6hj7q9i1s")))

(define-public crate-font-test-data-0.1.1 (c (n "font-test-data") (v "0.1.1") (h "1xrbjqdvcs3flwl1pbqz528n7cj9mvqzy9ahpyxsvl2h5cykyixs")))

(define-public crate-font-test-data-0.1.2 (c (n "font-test-data") (v "0.1.2") (h "09s6zskzzcz5p1fcrqzdwyhn4ha3sl1ag2l95lbpxyq9rrcp88lr")))

(define-public crate-font-test-data-0.1.3 (c (n "font-test-data") (v "0.1.3") (h "1bqj5m44xm567r2ag0xs4rydn1lzn111r4ss199k8f8cg548pbgh")))

(define-public crate-font-test-data-0.1.4 (c (n "font-test-data") (v "0.1.4") (h "06l8k1q6idhlz4nwnb9z1mzs5xjjaj1n8za089c46rlvhhfix0g3")))

(define-public crate-font-test-data-0.1.5 (c (n "font-test-data") (v "0.1.5") (h "17h628976i5ys2i759rfpa0dnhzqqm4dr67gc6cfilllb4ql01ga")))

