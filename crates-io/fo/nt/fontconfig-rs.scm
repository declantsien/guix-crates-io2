(define-module (crates-io fo nt fontconfig-rs) #:use-module (crates-io))

(define-public crate-fontconfig-rs-0.1.0 (c (n "fontconfig-rs") (v "0.1.0") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 2)) (d (n "const-cstr") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yeslogic-fontconfig-sys") (r "^3.1.0") (d #t) (k 0)))) (h "1zbk7aksxxcqqcyfisl8hhh10a89jcjyzdh7y922avmprqkjpgzl") (f (quote (("dlopen" "yeslogic-fontconfig-sys/dlopen")))) (r "1.56.1")))

(define-public crate-fontconfig-rs-0.1.1 (c (n "fontconfig-rs") (v "0.1.1") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 2)) (d (n "const-cstr") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yeslogic-fontconfig-sys") (r "^3.1.0") (d #t) (k 0)))) (h "1wr95y0y2hjbvcw8bcxmfbrfcn3j0zjvvj3zx4h6h70ispdaljyb") (f (quote (("dlopen" "yeslogic-fontconfig-sys/dlopen")))) (r "1.56.1")))

