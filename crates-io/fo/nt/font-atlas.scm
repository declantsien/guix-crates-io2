(define-module (crates-io fo nt font-atlas) #:use-module (crates-io))

(define-public crate-font-atlas-0.1.0 (c (n "font-atlas") (v "0.1.0") (d (list (d (n "glyph_packer") (r "^0.0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1xlvbsgsrxpkcnnk8nafh43h21vdn25b5kx9xjfjiv25slcmfc13")))

(define-public crate-font-atlas-0.1.1 (c (n "font-atlas") (v "0.1.1") (d (list (d (n "glyph_packer") (r "^0.0.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0qcabn3zhkx6wv6m8vig7pjx67j5279gl94yhxfvdh68c620b3nn")))

(define-public crate-font-atlas-0.1.2 (c (n "font-atlas") (v "0.1.2") (d (list (d (n "glyph_packer") (r "^0.0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0346kfjzhc7zxi51mjjzqra79zxa4srn6b7i7lwp437sx97j3n2s")))

(define-public crate-font-atlas-0.1.3 (c (n "font-atlas") (v "0.1.3") (d (list (d (n "rusttype") (r "^0.2.0") (d #t) (k 0)) (d (n "void") (r "1.0.*") (d #t) (k 0)))) (h "1w3l3v446y6a1xd45f3g4cfcw0mrn7x4rwrj301b5pyzkjmwix52")))

(define-public crate-font-atlas-0.1.4 (c (n "font-atlas") (v "0.1.4") (d (list (d (n "rusttype") (r "^0.2.0") (d #t) (k 0)) (d (n "void") (r "1.0.*") (d #t) (k 0)))) (h "1a3m7cs7psa4mamz1gjynvd4l0nn4bcc5isqpml1yhaiyq74xxkp")))

