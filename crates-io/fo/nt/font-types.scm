(define-module (crates-io fo nt font-types) #:use-module (crates-io))

(define-public crate-font-types-0.0.1 (c (n "font-types") (v "0.0.1") (h "1yn31l800sbhllabnyrw0jyybbfzfbyipryxjfbsva93g122s6yg") (f (quote (("std") ("default" "std"))))))

(define-public crate-font-types-0.0.2 (c (n "font-types") (v "0.0.2") (h "1wigx6i4c1jx4n4x31r52rkf08wlq6r3az6yl32vxpddwdj229lh") (f (quote (("std") ("default" "std"))))))

(define-public crate-font-types-0.0.3 (c (n "font-types") (v "0.0.3") (h "1mvhz7dad1rhfw8x4qr5sy1ipvdfc82jcab2q7yyphsybhw9v8nq") (f (quote (("std") ("default" "std"))))))

(define-public crate-font-types-0.0.5 (c (n "font-types") (v "0.0.5") (h "1z4kpx0y6p4m1jn3q9fnfdkkmgb0nddmnlivw9p7y8zr3nz35ry1") (f (quote (("std") ("default" "std"))))))

(define-public crate-font-types-0.0.6 (c (n "font-types") (v "0.0.6") (h "1ciq6cx1w39lgprb6bg8kmf1h6mlf7mxf037j7k43jknkk8p25q1") (f (quote (("std") ("default" "std"))))))

(define-public crate-font-types-0.1.0 (c (n "font-types") (v "0.1.0") (h "1r10ikq9l1xghpj5p6clgc2dm0139gic105dxpw8m0d0lcy4x541") (f (quote (("std") ("default" "std"))))))

(define-public crate-font-types-0.1.1 (c (n "font-types") (v "0.1.1") (h "1j84p2zrlqhc6vb4xijj7jzfb8j9dc9jrdqd12y5hj233cvrj7k3") (f (quote (("std") ("default" "std"))))))

(define-public crate-font-types-0.1.2 (c (n "font-types") (v "0.1.2") (h "01bd07n9hb1bb3wh74ncw05yxldna81dyjvz4lmd9329cby0yv3g") (f (quote (("std") ("default" "std"))))))

(define-public crate-font-types-0.1.3 (c (n "font-types") (v "0.1.3") (h "0yp8xzi9j422fkrz8d7r18xq2j3852mms2lh9zmjsnnywfvhspcd") (f (quote (("std") ("default" "std"))))))

(define-public crate-font-types-0.1.4 (c (n "font-types") (v "0.1.4") (h "0zsa6cz0drb59lx0zjnh39bh9yipnm81x7qvn8j24mfpff9gclyi") (f (quote (("std") ("default" "std"))))))

(define-public crate-font-types-0.1.5 (c (n "font-types") (v "0.1.5") (h "11l0sjl7klbn5q8ybgi7gb23wgniz93dqrps7h7a29l2l5cqxijg") (f (quote (("std") ("default" "std"))))))

(define-public crate-font-types-0.1.6 (c (n "font-types") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1j9vp9ldhhcxb8csxy0l4bylvzya0z3s26cjzk00lh3n148k2qak") (f (quote (("std") ("default" "std"))))))

(define-public crate-font-types-0.1.7 (c (n "font-types") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0bybjchmiw60p25iicrdwk9lxgbh7xr59ijrb18b106fjzjah74h") (f (quote (("std") ("default" "std"))))))

(define-public crate-font-types-0.1.8 (c (n "font-types") (v "0.1.8") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0gl26sfprhr7hgq79wh44dr0a6hd3300c53js3bymg8dlxscji1d") (f (quote (("std") ("default" "std"))))))

(define-public crate-font-types-0.2.0 (c (n "font-types") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "184i1aram3hmndpbf31a43nm2idsk221wwwq6m9f410pcfnxjzcp") (f (quote (("std") ("default" "std"))))))

(define-public crate-font-types-0.3.0 (c (n "font-types") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "15kqzz22yhk0mnf301z1n3g57hwaddi7jvdvwfn55fw28frl0wa4") (f (quote (("std") ("default" "std"))))))

(define-public crate-font-types-0.3.1 (c (n "font-types") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1mq2hsw3d155x7pdgyisq3q3frm6ldwinzb9hvj38v08h6j3l9qi") (f (quote (("std") ("default" "std"))))))

(define-public crate-font-types-0.3.2 (c (n "font-types") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0d3yyzcfi5jr27as1pgg065mryzrkrb71bxdhbmc92gfrw93rm96") (f (quote (("std") ("default" "std"))))))

(define-public crate-font-types-0.3.3 (c (n "font-types") (v "0.3.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0f9bmdrr6588ha99wkgjkzr8g43wjzgkpjwwpb7lwl1m7yr5agbm") (f (quote (("std") ("default" "std"))))))

(define-public crate-font-types-0.3.4 (c (n "font-types") (v "0.3.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0rv56q9j1d7ndr4pqs8sgxhpyx8mhbf19ngxzr4s4ah2c5fxcy39") (f (quote (("std") ("default" "std"))))))

(define-public crate-font-types-0.4.0 (c (n "font-types") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1fiwifbw5jhxysnfjpm34m2za1ipvd8gp98fcaafb2p9r8366602") (f (quote (("std") ("default" "std"))))))

(define-public crate-font-types-0.4.1 (c (n "font-types") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0x0kkzpd7z4s4k80aksckbcbsazpy9jywb1zqh22lpll0yl4l80y") (f (quote (("std") ("default" "std"))))))

(define-public crate-font-types-0.4.2 (c (n "font-types") (v "0.4.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0gdpj0hnj64rwn02a1jbf01z1kbfzcng8pdk0sv409jp2zmg7mqb") (f (quote (("std") ("default" "std"))))))

(define-public crate-font-types-0.4.3 (c (n "font-types") (v "0.4.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "02vkwz258innv5c3gcl8rv7g5valj58cc7xj991l9g9psd060zsv") (f (quote (("std") ("default" "std"))))))

(define-public crate-font-types-0.5.0 (c (n "font-types") (v "0.5.0") (d (list (d (n "bytemuck") (r "=1.13.1") (f (quote ("derive" "min_const_generics"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1ydin8wdh4lcchpwidh6v31vvvhmh42nb7dwjirlnsfxcy27mjzr")))

(define-public crate-font-types-0.5.1 (c (n "font-types") (v "0.5.1") (d (list (d (n "bytemuck") (r "=1.13.1") (f (quote ("derive" "min_const_generics"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1jh70wmc9am6l4q5477is702kcfx1rz7qlks12a6h58xwd7k3r97")))

(define-public crate-font-types-0.5.2 (c (n "font-types") (v "0.5.2") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive" "min_const_generics"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0cx5qfs269vm9ns6ww7b73lm50ijx68j6imqldp16awwdakq8rxx")))

(define-public crate-font-types-0.5.3 (c (n "font-types") (v "0.5.3") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive" "min_const_generics"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0hafs97z81z8i7ija37glp13r5dm9v89nn04kvixi434x0fsmxmx")))

(define-public crate-font-types-0.5.4 (c (n "font-types") (v "0.5.4") (d (list (d (n "bytemuck") (r "^1.13.1") (f (quote ("derive" "min_const_generics"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0krg6nvw4r6j9nggvk9942xyygqddjxzhqh5kczxids1n1xza6hw")))

