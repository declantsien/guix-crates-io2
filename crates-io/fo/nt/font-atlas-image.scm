(define-module (crates-io fo nt font-atlas-image) #:use-module (crates-io))

(define-public crate-font-atlas-image-0.1.0 (c (n "font-atlas-image") (v "0.1.0") (d (list (d (n "bincode") (r "^0.3.0") (d #t) (k 0)) (d (n "font-atlas") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0bfkz37mmzb05zarix7yfgx1x76nqbihjh9rhwzr33il1wn95hxb")))

(define-public crate-font-atlas-image-0.1.1 (c (n "font-atlas-image") (v "0.1.1") (d (list (d (n "bincode") (r "^0.3.0") (d #t) (k 0)) (d (n "font-atlas") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.3.9") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0aj6raa2bgi107vfj5hhg4w219kblss1ykfvmr1k6ywcqhczy23g")))

(define-public crate-font-atlas-image-0.1.2 (c (n "font-atlas-image") (v "0.1.2") (d (list (d (n "bincode") (r "^0.3.0") (d #t) (k 0)) (d (n "font-atlas") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "= 0.3.9") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "06nib7d2kfvcayw66sb8a0jkql1hzc4zzw7aa2wbg052dy2nzj85")))

