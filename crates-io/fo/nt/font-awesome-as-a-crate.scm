(define-module (crates-io fo nt font-awesome-as-a-crate) #:use-module (crates-io))

(define-public crate-font-awesome-as-a-crate-0.1.0 (c (n "font-awesome-as-a-crate") (v "0.1.0") (h "0p7bjh4f21d29b93z53i85d33cbjdjlpnd1f2599kgpnsz88l118")))

(define-public crate-font-awesome-as-a-crate-0.1.1 (c (n "font-awesome-as-a-crate") (v "0.1.1") (h "0vd5k2h3jc4np5g008941sgjr4l2b14wnac03ba7rzfxxvqywfcl")))

(define-public crate-font-awesome-as-a-crate-0.1.2 (c (n "font-awesome-as-a-crate") (v "0.1.2") (h "1ajqva236578qy9dhzpd281sfgfx4mrwfsfr4ns4rbb240mwab51")))

(define-public crate-font-awesome-as-a-crate-0.2.0 (c (n "font-awesome-as-a-crate") (v "0.2.0") (h "0636iyapnw2r57xvj1ixj1ziark0bl7850mnpkfx1989yf0skbw6")))

(define-public crate-font-awesome-as-a-crate-0.3.0 (c (n "font-awesome-as-a-crate") (v "0.3.0") (h "1p64cir7d3jxlhdj4b3xmhn37v3xaqp2v80vgzrga2ija6ywybck")))

