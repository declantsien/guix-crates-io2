(define-module (crates-io fo nt font-glyph) #:use-module (crates-io))

(define-public crate-font-glyph-0.1.0 (c (n "font-glyph") (v "0.1.0") (d (list (d (n "bezier-interpolation") (r "^0.2") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.7") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 0)))) (h "18qjd4yqkda98qxaxzgdabwrfnd62chrc0jacqkisdc2rga6rdxf") (y #t)))

(define-public crate-font-glyph-0.3.0 (c (n "font-glyph") (v "0.3.0") (d (list (d (n "bezier-interpolation") (r "^0.3") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.7") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 0)))) (h "1bfb114j1isbk0wkn10jry9adq0k84axmlzwyf7kpyyhzy9hafwp") (y #t)))

(define-public crate-font-glyph-0.3.1 (c (n "font-glyph") (v "0.3.1") (d (list (d (n "bezier-interpolation") (r "^0.3") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.7") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0qjh6sbdnz5n2sqzjnv8s8db0npz7pd4jd4ylcc25nhdgcvwqrbl")))

