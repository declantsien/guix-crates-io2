(define-module (crates-io fo nt fontu) #:use-module (crates-io))

(define-public crate-fontu-0.1.0 (c (n "fontu") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "tfon") (r "^0.1") (d #t) (k 0)))) (h "0yimdpy1bwnq7y765xhk77b01jh45qmvyvx9kf54vyrcbmkn3k1p")))

(define-public crate-fontu-0.1.1 (c (n "fontu") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "tfon") (r "^0.1") (d #t) (k 0)))) (h "19mkcyjq5mmhca8ihk5d20d9j6ilm3rbirvll3lkck3h6vg2a1g8")))

