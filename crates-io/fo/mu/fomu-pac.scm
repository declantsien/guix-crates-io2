(define-module (crates-io fo mu fomu-pac) #:use-module (crates-io))

(define-public crate-fomu-pac-0.0.1 (c (n "fomu-pac") (v "0.0.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "fomu-rt") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)) (d (n "vexriscv") (r "^0.0.1") (d #t) (k 0)))) (h "1yn09mjnal044sjslcsb4vyqc04mg0mrklxj6jzh19zdwvky6h11") (f (quote (("rt" "fomu-rt"))))))

(define-public crate-fomu-pac-0.0.2 (c (n "fomu-pac") (v "0.0.2") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "fomu-rt") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)) (d (n "vexriscv") (r "^0.0.1") (d #t) (k 0)))) (h "1m9adkmxfqn6zqxa4amg4bw8glq8iy0j34b6q1xsf8kpkrf4vwzv") (f (quote (("rt" "fomu-rt"))))))

(define-public crate-fomu-pac-0.0.3 (c (n "fomu-pac") (v "0.0.3") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "fomu-rt") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)) (d (n "vexriscv") (r "^0.0.1") (d #t) (k 0)))) (h "1il8581napng427y5kysj1xz2ca372xxszrzkky4rz9cwg615bzj") (f (quote (("rt" "fomu-rt"))))))

