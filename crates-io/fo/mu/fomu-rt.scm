(define-module (crates-io fo mu fomu-rt) #:use-module (crates-io))

(define-public crate-fomu-rt-0.0.1 (c (n "fomu-rt") (v "0.0.1") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "r0") (r "^0.2.2") (d #t) (k 0)) (d (n "riscv-rt-macros") (r "^0.1.6") (d #t) (k 0)) (d (n "vexriscv") (r "^0.0.1") (d #t) (k 0)) (d (n "vexriscv") (r "^0.0.1") (d #t) (k 2)))) (h "1ln2frv59kscaxmghdlakm4pymr5qymadax62gd38m2xi8mfk2nx") (f (quote (("inline-asm" "vexriscv/inline-asm")))) (y #t)))

(define-public crate-fomu-rt-0.0.2 (c (n "fomu-rt") (v "0.0.2") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "r0") (r "^0.2.2") (d #t) (k 0)) (d (n "riscv-rt-macros") (r "^0.1.6") (d #t) (k 0)) (d (n "vexriscv") (r "^0.0") (d #t) (k 0)) (d (n "vexriscv") (r "^0.0.1") (d #t) (k 2)))) (h "0v51wwcl74a569d629j8hpa9hhrpvb3rf4wwvaia4wd4v3867k10") (f (quote (("inline-asm" "vexriscv/inline-asm")))) (y #t)))

(define-public crate-fomu-rt-0.0.3 (c (n "fomu-rt") (v "0.0.3") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "r0") (r "^0.2.2") (d #t) (k 0)) (d (n "riscv-rt-macros") (r "^0.1.6") (d #t) (k 0)) (d (n "vexriscv") (r "^0.0") (d #t) (k 0)) (d (n "vexriscv") (r "^0.0.1") (d #t) (k 2)))) (h "0a9712g4ly4hkp90s61hhfd513rdh07yj2nz41idxfh18akppsgr") (f (quote (("inline-asm" "vexriscv/inline-asm"))))))

(define-public crate-fomu-rt-0.0.4 (c (n "fomu-rt") (v "0.0.4") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "r0") (r "^0.2.2") (d #t) (k 0)) (d (n "riscv-rt-macros") (r "^0.1.6") (d #t) (k 0)) (d (n "vexriscv") (r "^0.0") (d #t) (k 0)) (d (n "vexriscv") (r "^0.0.1") (d #t) (k 2)))) (h "0jyfklabrkmpy2696abnqbk36xp20bnvlfvxri32byyn7a062y3h") (f (quote (("inline-asm" "vexriscv/inline-asm"))))))

(define-public crate-fomu-rt-0.0.5 (c (n "fomu-rt") (v "0.0.5") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "r0") (r "^0.2.2") (d #t) (k 0)) (d (n "riscv-rt-macros") (r "^0.1.6") (d #t) (k 0)) (d (n "vexriscv") (r "^0.0") (d #t) (k 0)) (d (n "vexriscv") (r "^0.0.1") (d #t) (k 2)))) (h "0gsapldzy77i5knf1khbyyjf8xgbp2gj46z096xis51lkkn2rp6j") (f (quote (("inline-asm" "vexriscv/inline-asm"))))))

(define-public crate-fomu-rt-0.0.6 (c (n "fomu-rt") (v "0.0.6") (d (list (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "r0") (r "^0.2.2") (d #t) (k 0)) (d (n "riscv") (r "^0.6") (d #t) (k 0)) (d (n "riscv") (r "^0.6") (d #t) (k 2)) (d (n "riscv-rt-macros") (r "^0.1.6") (d #t) (k 0)) (d (n "vexriscv") (r "^0.0.3") (d #t) (k 0)) (d (n "vexriscv") (r "^0.0.3") (d #t) (k 2)))) (h "0ic3dpdw0b2rpz2aadbcd4jd05xkdwfr7dn3wmpm9f3d20gwhn0m") (f (quote (("inline-asm" "riscv/inline-asm"))))))

