(define-module (crates-io fo rs fors) #:use-module (crates-io))

(define-public crate-fors-0.1.0 (c (n "fors") (v "0.1.0") (d (list (d (n "clap") (r "^2.18.0") (d #t) (k 0)) (d (n "nix") (r "^0.7.0") (d #t) (k 0)) (d (n "nom") (r "^1.2.4") (d #t) (k 0)) (d (n "term") (r "^0.4.4") (d #t) (k 0)))) (h "0abnka7fv1q2pbsfcij3dj0vigk18vwfklv9in7yp3c12v3pcyas")))

