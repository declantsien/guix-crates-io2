(define-module (crates-io fo rs forsyth) #:use-module (crates-io))

(define-public crate-forsyth-0.1.0 (c (n "forsyth") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lz-fear") (r "^0.1") (d #t) (k 2)) (d (n "tobj") (r "^2.0") (d #t) (k 2)))) (h "1k989s1ngd147189m4j0005g9hdgagg5lb3ldshq7lvxfacizq95")))

(define-public crate-forsyth-0.2.0 (c (n "forsyth") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lz-fear") (r "^0.1") (d #t) (k 2)) (d (n "tobj") (r "^3.0") (d #t) (k 2)))) (h "0qfbd92w7h3qqsh13vd85y3kwn3s3gih2rlw2kjf9nxiz4zlc4hn")))

(define-public crate-forsyth-0.3.0 (c (n "forsyth") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lz-fear") (r "^0.1") (d #t) (k 2)) (d (n "tobj") (r "^3.1") (d #t) (k 2)))) (h "07k9q6j7pc7w84bd9zn2ls9bqpqw9fxm9kjbfnn83i0v6kcb5hn1")))

(define-public crate-forsyth-1.0.0 (c (n "forsyth") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lz-fear") (r "^0.1") (d #t) (k 2)) (d (n "tobj") (r "^3.1") (d #t) (k 2)))) (h "1yak3nl58g45xgs7qpk5blq4rdzzn05q9kwf8vl5mbrd8m427j9i")))

(define-public crate-forsyth-1.0.1 (c (n "forsyth") (v "1.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lz-fear") (r "^0.1") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "tobj") (r "^3.1") (d #t) (k 2)))) (h "0vdkb2qj0djchsg510047j0mw48qhg0pmx5jyivmknic2bnzkr50")))

