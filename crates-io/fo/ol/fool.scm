(define-module (crates-io fo ol fool) #:use-module (crates-io))

(define-public crate-fool-0.0.1 (c (n "fool") (v "0.0.1") (h "09f819dia01jv1p3nyhk4cp5b968nffmlp1sglgmnxlkznml7s0m") (f (quote (("std") ("default" "std"))))))

(define-public crate-fool-0.0.2 (c (n "fool") (v "0.0.2") (h "0ybbafk78q6m20s24g70m3d94myd6pdwx7a6l60b9b7bpndqj33l") (f (quote (("std") ("default"))))))

(define-public crate-fool-0.0.3 (c (n "fool") (v "0.0.3") (h "111pgnryl01z0n9cm9w8974w0xmflm8qzimcgnmjwsjrf7ry46dj")))

(define-public crate-fool-0.0.4 (c (n "fool") (v "0.0.4") (d (list (d (n "itertools") (r "^0.9.0") (o #t) (k 0)))) (h "12wrapq5wl3q76z13i26y2nbnd87garh168gssk3hysqs4wigw3y") (f (quote (("zip" "itertools") ("default") ("bare"))))))

