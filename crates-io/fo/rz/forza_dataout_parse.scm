(define-module (crates-io fo rz forza_dataout_parse) #:use-module (crates-io))

(define-public crate-forza_dataout_parse-0.1.0 (c (n "forza_dataout_parse") (v "0.1.0") (h "19l8qdki70dfnlr2w6k7wx5gnjx8ga172jj4nyxl776p00zks9ry")))

(define-public crate-forza_dataout_parse-0.2.0 (c (n "forza_dataout_parse") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1byp4z7n70dwqr419arbk4b0jhkrnznddbmipin1lp04azciqrnm")))

