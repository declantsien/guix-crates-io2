(define-module (crates-io fo rm form-yew) #:use-module (crates-io))

(define-public crate-form-yew-0.1.0 (c (n "form-yew") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.101") (d #t) (k 0)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "18bjxnjfrdl6fwwd2zj19dp0rf78wn3n2z77301ki2h03a8bva2j")))

(define-public crate-form-yew-0.1.1 (c (n "form-yew") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.46") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.101") (d #t) (k 0)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "02fz89w8mcj0a8jdjw7sm55vm0n51c4qk0611pk0imvbhngv6cq6")))

