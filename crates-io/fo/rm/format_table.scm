(define-module (crates-io fo rm format_table) #:use-module (crates-io))

(define-public crate-format_table-0.1.0 (c (n "format_table") (v "0.1.0") (h "02nz6lrvqq6bywy3l4r0048kkiiajan9zfpap3kpliizyfqaxkmj")))

(define-public crate-format_table-0.1.1 (c (n "format_table") (v "0.1.1") (h "0p3wir7vnb69sgda6swx6834kwhwjndknw18i1hlgba4nfhi2k7r")))

