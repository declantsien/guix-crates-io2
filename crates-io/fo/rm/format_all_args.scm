(define-module (crates-io fo rm format_all_args) #:use-module (crates-io))

(define-public crate-format_all_args-0.1.0 (c (n "format_all_args") (v "0.1.0") (h "0g1i05yayiw0iny8ipf2yzrrd5nrwyzl89m7m9qw2k54vns52jpz")))

(define-public crate-format_all_args-0.2.0 (c (n "format_all_args") (v "0.2.0") (h "1rayhhm6sfh74c4ghz5kwim3cb57533afi8rzvk1irrq5cqf8ld0")))

(define-public crate-format_all_args-0.3.0 (c (n "format_all_args") (v "0.3.0") (h "1m6fgg4c4flwjj9my0z0frkdm29fih47vb9bpji09vyifhzmii82")))

(define-public crate-format_all_args-0.4.0 (c (n "format_all_args") (v "0.4.0") (h "0v4mk7kxk0g4jxdy3hli9j9xwg0lg0dad8frb79rf1y9s128n89y")))

(define-public crate-format_all_args-0.5.0 (c (n "format_all_args") (v "0.5.0") (h "0d0gqspz965yd3q2n6rbj2sdm3hxkzmvh8482mjwwijv0v9djrp2")))

