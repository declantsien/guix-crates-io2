(define-module (crates-io fo rm formy_derive) #:use-module (crates-io))

(define-public crate-formy_derive-0.1.0-alpha.0 (c (n "formy_derive") (v "0.1.0-alpha.0") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "1qflbnm6rd7j7vzrg7py2l777m99cmc9p4lkxb6w1ab1k8gq2n2x")))

(define-public crate-formy_derive-0.1.0-alpha.1 (c (n "formy_derive") (v "0.1.0-alpha.1") (d (list (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "0m7kx8plb6p8ny151zs3hrhi7bzfgw1pg7xrwyn7c4fv1xg7lj0f")))

(define-public crate-formy_derive-0.1.0 (c (n "formy_derive") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "0hsy0pmix9br819s80hy5zkmwpc2ds9mls7h0kdlcdblf2l4rzgb")))

(define-public crate-formy_derive-0.2.0 (c (n "formy_derive") (v "0.2.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "05f8n3l73y985xg2lzh1zm9v3gwa212w6gyaxhk8swa7ynjhpfj7")))

(define-public crate-formy_derive-0.2.1 (c (n "formy_derive") (v "0.2.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "0x2djaniy7qnrb7ynz4k2h3q5g8ilwvn38ymq87mj4gphc4pp1nx")))

(define-public crate-formy_derive-0.2.2 (c (n "formy_derive") (v "0.2.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (f (quote ("derive" "parsing"))) (d #t) (k 0)))) (h "1sfzy7yxgsiwh80gmgccblh1lh4932lmnqbc5h3m79ymzk1665pd")))

