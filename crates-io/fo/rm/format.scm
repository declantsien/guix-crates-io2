(define-module (crates-io fo rm format) #:use-module (crates-io))

(define-public crate-format-0.1.0 (c (n "format") (v "0.1.0") (h "0xxlz1b67rbd14c78bb18wd06y0sa2x575rh6j78yr02vazkcrxr")))

(define-public crate-format-0.1.1 (c (n "format") (v "0.1.1") (d (list (d (n "format-core") (r "^0.1.1") (d #t) (k 0)) (d (n "format-macro") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1x42ry3ng7yxhzy0nb4l6zzh8271j9mqfazh5ajd13mkpq4yinkb") (f (quote (("macro" "format-macro") ("default" "macro"))))))

(define-public crate-format-0.1.2 (c (n "format") (v "0.1.2") (d (list (d (n "format-core") (r "^0.1.2") (d #t) (k 0)) (d (n "format-macro") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "0wf4m134m6fx76vdvm0zzajr0cki5jigv755b2sd51hdyijqbnlf") (f (quote (("macro" "format-macro") ("default" "macro"))))))

(define-public crate-format-0.1.3 (c (n "format") (v "0.1.3") (d (list (d (n "format-core") (r "^0.1.3") (d #t) (k 0)) (d (n "format-macro") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "0nnm13r9agblh2j3q6zxhk6zqkda5556s7lchsk10gyz07drb0pn") (f (quote (("macro" "format-macro") ("default" "macro"))))))

(define-public crate-format-0.2.0 (c (n "format") (v "0.2.0") (d (list (d (n "format-core") (r "^0.2.0") (d #t) (k 0)) (d (n "format-macro") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0p023b4r053yka67f36a4z52vdh4vkl1acnpxg93hmwa6w78ncd1") (f (quote (("unstable") ("nightly") ("macro" "format-macro" "nightly" "unstable") ("default" "macro"))))))

(define-public crate-format-0.2.1 (c (n "format") (v "0.2.1") (d (list (d (n "format-core") (r "^0.2.1") (d #t) (k 0)) (d (n "format-macro") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0islp4kkb5f1zk6pzvz0vz50d8nsj4vxfbm5mlxzmw2154f0fx22") (f (quote (("unstable") ("nightly") ("macro" "format-macro" "nightly" "unstable") ("default" "macro"))))))

(define-public crate-format-0.2.2 (c (n "format") (v "0.2.2") (d (list (d (n "format-core") (r "^0.2.2") (d #t) (k 0)) (d (n "format-macro") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0xanpb6jvbn4aa5z4sgd232cgpd2mj991snafa0bbc6dryi6gvgm") (f (quote (("unstable") ("nightly" "macro" "format-macro/nightly" "unstable") ("macro") ("default" "macro" "format-macro" "proc-macro-hack"))))))

(define-public crate-format-0.2.3 (c (n "format") (v "0.2.3") (d (list (d (n "format-core") (r "^0.2.3") (d #t) (k 0)) (d (n "format-macro") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "1ilnn0134p3xv90f2ly8f3yw15164y39x8639d30kkzbakx8hq0i") (f (quote (("macro" "format-macro") ("default" "macro"))))))

(define-public crate-format-0.2.4 (c (n "format") (v "0.2.4") (d (list (d (n "format-core") (r "^0.2.4") (d #t) (k 0)) (d (n "format-macro") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "1j1syang86kaycjdxsbmi4rz5ag58f5sxc9np2gnvy33miiin7lh") (f (quote (("macro" "format-macro") ("default" "macro"))))))

