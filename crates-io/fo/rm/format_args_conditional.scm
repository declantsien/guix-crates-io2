(define-module (crates-io fo rm format_args_conditional) #:use-module (crates-io))

(define-public crate-format_args_conditional-0.1.0 (c (n "format_args_conditional") (v "0.1.0") (h "1shrrv8nwc81q1c2h19fdxakpy4r5pmsi2p403irp1k9zpbljnb4")))

(define-public crate-format_args_conditional-0.1.1 (c (n "format_args_conditional") (v "0.1.1") (h "1kfxixfnwx6f7piawskfbwkq3gdnchq4akvk2ldyibvqgai4j31d")))

