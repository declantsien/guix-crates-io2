(define-module (crates-io fo rm formality) #:use-module (crates-io))

(define-public crate-formality-0.1.0 (c (n "formality") (v "0.1.0") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "symmetric-interaction-calculus") (r "^0.1.3") (d #t) (k 0)))) (h "1mkyyh9jga2jxcrwncam585zl8zy93jxk7ass5jgwcn686ya0wr1")))

(define-public crate-formality-0.1.1 (c (n "formality") (v "0.1.1") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "symmetric-interaction-calculus") (r "^0.1.3") (d #t) (k 0)))) (h "074wri30swjn6szwzkmydi1h7pxm7l6ahn3lng03rw38x7g2mkrj")))

(define-public crate-formality-0.1.3 (c (n "formality") (v "0.1.3") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "symmetric-interaction-calculus") (r "^0.1.8") (d #t) (k 0)))) (h "087l95b5287j1hqyrjfz3b05kirrh3rgjjmj0r02j8gis811cpy1")))

(define-public crate-formality-0.1.4 (c (n "formality") (v "0.1.4") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "symmetric-interaction-calculus") (r "^0.1.8") (d #t) (k 0)))) (h "1kdjhf1870d0f8vg53rm8cx2xh7ms0hr966hflnrb6b8mqr74y1k")))

(define-public crate-formality-0.1.5 (c (n "formality") (v "0.1.5") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "symmetric-interaction-calculus") (r "^0.1.8") (d #t) (k 0)))) (h "009slk1rwxyrvck98xf24w4ihwav3zzg35y88r8gq7cdmmx35jbl")))

(define-public crate-formality-0.1.6 (c (n "formality") (v "0.1.6") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "symmetric-interaction-calculus") (r "^0.1.8") (d #t) (k 0)))) (h "0nd0va8rbiwggvwp3nqx1jf22pfpasf67xv9fq0ji4fra6hmnv2k")))

(define-public crate-formality-0.1.7 (c (n "formality") (v "0.1.7") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "symmetric-interaction-calculus") (r "^0.1.8") (d #t) (k 0)))) (h "19zpcixhlrjzw56q6lp3fd820s8qhyjf8xcp6wkq2c4hgm2l6wnf")))

(define-public crate-formality-0.1.8 (c (n "formality") (v "0.1.8") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "symmetric-interaction-calculus") (r "^0.1.8") (d #t) (k 0)))) (h "04m8dnc9y8gdb2c1pf5wdp0ancnq0pxzxnqv8br7pn9dykih80qh")))

(define-public crate-formality-0.1.9 (c (n "formality") (v "0.1.9") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "symmetric-interaction-calculus") (r "^0.1.8") (d #t) (k 0)))) (h "16z12zwv7abvybrzkv7pw3s56xq24hi4s3np5byh0zlc3ydx46sp")))

(define-public crate-formality-0.1.10 (c (n "formality") (v "0.1.10") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "symmetric-interaction-calculus") (r "^0.1.8") (d #t) (k 0)))) (h "1kdyg932gfifw75kgcwv9a6iml806949hf9sd7nlcam1spp4dmdr")))

(define-public crate-formality-0.1.11 (c (n "formality") (v "0.1.11") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "symmetric-interaction-calculus") (r "^0.1.8") (d #t) (k 0)))) (h "1yb6bav676m07ps9sxix4fzaac9mcb2mknkqys3y39cbnpxn4gwk")))

