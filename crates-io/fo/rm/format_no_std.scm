(define-module (crates-io fo rm format_no_std) #:use-module (crates-io))

(define-public crate-format_no_std-1.0.0 (c (n "format_no_std") (v "1.0.0") (h "1zsymfvmi8m0xnmwf6wg4xg253nsbzmm3rnks8r51z68h6zvhshg")))

(define-public crate-format_no_std-1.0.1 (c (n "format_no_std") (v "1.0.1") (h "08pzrkd0s8jqq009s6kcg2g2b6s4jnvng6s85hdbyr4kdyaigiqc")))

(define-public crate-format_no_std-1.0.2 (c (n "format_no_std") (v "1.0.2") (h "0zgyfi6vai9bd58a0gw22z20hdqxv00r75dnfi3046cgr5vsi31d")))

(define-public crate-format_no_std-1.1.0 (c (n "format_no_std") (v "1.1.0") (h "1lp82yq1wlrjxagff0pbdani13px7ygbddpsm85lw8rpll434ww4")))

(define-public crate-format_no_std-1.2.0 (c (n "format_no_std") (v "1.2.0") (h "19vk53cvc6j2bfyfs9h301i8b82sgc18g3jzh0ncdqmzn0r5xr4s")))

