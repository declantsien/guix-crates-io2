(define-module (crates-io fo rm format_money) #:use-module (crates-io))

(define-public crate-format_money-0.1.0 (c (n "format_money") (v "0.1.0") (h "0ympam03n9snnyglfncrkg5j8kksvnvpm5r26qfwi3p898j9x4iw")))

(define-public crate-format_money-0.1.1 (c (n "format_money") (v "0.1.1") (h "0s5k627kqhrclydxna4x2jhhmz52ccb7vpnf7vpb3rmzvqhqivrz")))

