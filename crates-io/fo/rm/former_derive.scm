(define-module (crates-io fo rm former_derive) #:use-module (crates-io))

(define-public crate-former_derive-0.1.0 (c (n "former_derive") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)) (d (n "unzip-n") (r "^0.1.2") (d #t) (k 0)))) (h "1qc03pabkafa129af2fqlpl475xar2aziv1q278dsxz5ypagg43x")))

(define-public crate-former_derive-0.1.2 (c (n "former_derive") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "~1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0.32") (d #t) (k 0)) (d (n "quote") (r "~1.0.10") (d #t) (k 0)) (d (n "syn") (r "~1.0.81") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "~1.0") (f (quote ("diff"))) (d #t) (k 2)) (d (n "unzip-n") (r "~0.1.2") (d #t) (k 0)))) (h "13b03c2gy8xmick4i9lfghqbsxrfm75cm2yc4h8hbk12h5l6gjjd")))

(define-public crate-former_derive-0.1.3 (c (n "former_derive") (v "0.1.3") (d (list (d (n "proc-macro-error") (r "~1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0.32") (d #t) (k 0)) (d (n "quote") (r "~1.0.10") (d #t) (k 0)) (d (n "syn") (r "~1.0.81") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "~1.0") (f (quote ("diff"))) (d #t) (k 2)) (d (n "unzip-n") (r "~0.1.2") (d #t) (k 0)))) (h "1j3jhlggp1i9fgcxc1d3gdcd4bvq7fm07dqhznc3pmiacasv5l4i")))

(define-public crate-former_derive-0.1.4 (c (n "former_derive") (v "0.1.4") (d (list (d (n "proc-macro-error") (r "~1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0.32") (d #t) (k 0)) (d (n "quote") (r "~1.0.10") (d #t) (k 0)) (d (n "syn") (r "~1.0.81") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "~1.0") (f (quote ("diff"))) (d #t) (k 2)) (d (n "unzip-n") (r "~0.1.2") (d #t) (k 0)))) (h "1yb0g08lwpmqv0rhvjy4pmqrwh672knyzgkd3qv9ng1z22fknpa6")))

(define-public crate-former_derive-0.1.5 (c (n "former_derive") (v "0.1.5") (d (list (d (n "proc-macro-error") (r "~1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0.32") (d #t) (k 0)) (d (n "quote") (r "~1.0.10") (d #t) (k 0)) (d (n "syn") (r "~1.0.81") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "~1.0") (f (quote ("diff"))) (d #t) (k 2)) (d (n "unzip-n") (r "~0.1.2") (d #t) (k 0)))) (h "1saa9fyc0igasryr6ikijkk5spiyf45xcn5f9yigf817vqlgrqqm")))

(define-public crate-former_derive-0.1.6 (c (n "former_derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "~1.0.32") (d #t) (k 0)) (d (n "quote") (r "~1.0.10") (d #t) (k 0)) (d (n "syn") (r "~1.0.81") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "~1.0") (f (quote ("diff"))) (d #t) (k 2)) (d (n "unzip-n") (r "~0.1.2") (d #t) (k 0)) (d (n "werror") (r "~0.0") (d #t) (k 0)) (d (n "wproc_macro") (r "~0.0") (d #t) (k 0)))) (h "07fx1pbmq4rwm2zyd279inkz0rrjy77zy590mbazxsayw8d3rx7m")))

(define-public crate-former_derive-0.1.8 (c (n "former_derive") (v "0.1.8") (d (list (d (n "itertools") (r "~0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0.32") (d #t) (k 0)) (d (n "quote") (r "~1.0.10") (d #t) (k 0)) (d (n "syn") (r "~1.0.81") (f (quote ("full" "extra-traits" "parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "~1.0") (f (quote ("diff"))) (d #t) (k 2)) (d (n "werror") (r "~0.0") (d #t) (k 0)) (d (n "wproc_macro") (r "~0.0") (d #t) (k 0)))) (h "0f53amg2ilgl55n0alsgw5109rc5xi0scz188aryvvv50x7bqjjf")))

(define-public crate-former_derive-0.1.9 (c (n "former_derive") (v "0.1.9") (d (list (d (n "iter_tools") (r "~0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "proc_macro_tools") (r "~0.1") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("full" "extra-traits" "parsing" "printing"))) (d #t) (k 0)) (d (n "werror") (r "~0.1") (d #t) (k 0)))) (h "07hm7nyfznvyh1d5xm4cf4a53k0b2jq3hmyvj3v8bf39n9h712pj") (y #t)))

(define-public crate-former_derive-0.1.10 (c (n "former_derive") (v "0.1.10") (h "14hkfg2xaffxfqm8wx9ahm1dblr0z5ibgsd56qaf0973lbz0295s") (y #t)))

(define-public crate-former_derive-0.2.0 (c (n "former_derive") (v "0.2.0") (h "140lziijhcfl7d322lk1cvpks2v4sw4gy0vmzq9nsmwygwyfh58b")))

