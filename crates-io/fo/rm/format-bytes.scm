(define-module (crates-io fo rm format-bytes) #:use-module (crates-io))

(define-public crate-format-bytes-0.1.0 (c (n "format-bytes") (v "0.1.0") (d (list (d (n "format-bytes-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0j0h2qhdsjv50h1xlf4icdn5ikyrlb811mmj8gmwfj10jm7kqkcm")))

(define-public crate-format-bytes-0.1.1 (c (n "format-bytes") (v "0.1.1") (d (list (d (n "format-bytes-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "04nazlr7y5j24600cpvqyla07f3dfr2fvmd3y7gdvh2nik9s9dx6")))

(define-public crate-format-bytes-0.1.2 (c (n "format-bytes") (v "0.1.2") (d (list (d (n "format-bytes-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "048cqz1507ipxv2xs7ijf6x8w82c7bzsbn5dklzar1w6q88nlwaz")))

(define-public crate-format-bytes-0.1.3 (c (n "format-bytes") (v "0.1.3") (d (list (d (n "format-bytes-macros") (r ">=0.1.2, <0.2.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r ">=0.5.0, <0.6.0") (d #t) (k 0)) (d (n "trybuild") (r ">=1.0.0, <2.0.0") (d #t) (k 2)))) (h "1407mq82fgha8kx3lyx1hqr4qm8w16c44mbqb3j9mljcazmp8wqs")))

(define-public crate-format-bytes-0.1.4 (c (n "format-bytes") (v "0.1.4") (d (list (d (n "format-bytes-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "16dxzms5z2dd9vh5xdi7kq6bz634j3hn8n69pc10xbdj2bgdbjh7")))

(define-public crate-format-bytes-0.1.5 (c (n "format-bytes") (v "0.1.5") (d (list (d (n "format-bytes-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "02566gg39bhd5pqz0x6adxv6qxnd3fl0kzk51kgzk2ckdh7g6i72")))

(define-public crate-format-bytes-0.2.0 (c (n "format-bytes") (v "0.2.0") (d (list (d (n "format-bytes-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0kz0170dj80p0717ljx5g9hshbx5dzy8y1qkx8y0acbbbpjgadfc")))

(define-public crate-format-bytes-0.2.1 (c (n "format-bytes") (v "0.2.1") (d (list (d (n "format-bytes-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "01ivkgc3dz8wniqb6z2907xmdbwcq4przr3g5mhirjph0i5zyc40")))

(define-public crate-format-bytes-0.2.2 (c (n "format-bytes") (v "0.2.2") (d (list (d (n "format-bytes-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0qjp3ppz325ff38piv4qq2hgipj5b9qaq818pbkb9mvz1h28jkhw")))

(define-public crate-format-bytes-0.3.0 (c (n "format-bytes") (v "0.3.0") (d (list (d (n "format-bytes-macros") (r "^0.4.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1zf22idlv3q99w6r71cyr84grik803hwjnhpisimv5wkxxk27528")))

