(define-module (crates-io fo rm format-macro) #:use-module (crates-io))

(define-public crate-format-macro-0.1.0 (c (n "format-macro") (v "0.1.0") (d (list (d (n "format-core") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "01qqr4ipw1ds3vmprhhnkvznrwa58yfzh048kdizyf7ndfiw7zyl")))

(define-public crate-format-macro-0.1.1 (c (n "format-macro") (v "0.1.1") (d (list (d (n "format-core") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "036njm3x0qq04dmm8yaiac0dkspmhfkqz56xvn95bwpww9kyy050")))

(define-public crate-format-macro-0.1.2 (c (n "format-macro") (v "0.1.2") (d (list (d (n "format-core") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "1xl3dj08aid33p1ykw5dam6xiaiv5r1fip6f64n48k1msdfs2a54")))

(define-public crate-format-macro-0.1.3 (c (n "format-macro") (v "0.1.3") (d (list (d (n "format-core") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "1yls2dmaw3pg2jqlkr2xajnalmpy9c9ij2nvm4g233djv3xhcflq")))

(define-public crate-format-macro-0.2.0 (c (n "format-macro") (v "0.2.0") (d (list (d (n "format-core") (r "^0.2.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "0b7l8glj0a1p5n9a8d8vkbcsqpl4pgf9havwby6ka78z9fmkq6al")))

(define-public crate-format-macro-0.2.1 (c (n "format-macro") (v "0.2.1") (d (list (d (n "format-core") (r "^0.2.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "015lnpfh50i88h627ixfy9kb013mhvd2r1xnyvrj110j9gnbzhjy")))

(define-public crate-format-macro-0.2.2 (c (n "format-macro") (v "0.2.2") (d (list (d (n "format-core") (r "^0.2.2") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5.15") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (f (quote ("full"))) (d #t) (k 0)))) (h "00c86wll1yl4qxwi1hswkg4nspl7zzy7s0951vyq52zbiyg5dwia") (f (quote (("unstable") ("nightly" "unstable") ("default"))))))

(define-public crate-format-macro-0.2.3 (c (n "format-macro") (v "0.2.3") (d (list (d (n "format-core") (r "^0.2.2") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5.18") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.22") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (f (quote ("full"))) (d #t) (k 0)))) (h "0zsr5b4iwqm04qfa25p02irrjmn1dlpw6mzb77pxa6m0i5kbcjmk")))

(define-public crate-format-macro-0.2.4 (c (n "format-macro") (v "0.2.4") (d (list (d (n "format-core") (r "^0.2.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.68") (f (quote ("full"))) (d #t) (k 0)))) (h "0k0a6w3lkml0r9varzw1lqvsl9n5l0l8dm8zvmipa8bzwp2am7wg")))

