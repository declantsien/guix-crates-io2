(define-module (crates-io fo rm formatjson) #:use-module (crates-io))

(define-public crate-formatjson-0.1.0 (c (n "formatjson") (v "0.1.0") (d (list (d (n "miette") (r "^7.2.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1rrwy7jlcq3ssbsqkx0d39pgw9d7dn3a82s34gfqnrf0g7mbd24b")))

(define-public crate-formatjson-0.1.1 (c (n "formatjson") (v "0.1.1") (d (list (d (n "miette") (r "^7.2.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0h2vlb9ji453xpazlsxpcn7sa1sg32iiv41q7rm1sx2g5kzdf5ah")))

(define-public crate-formatjson-0.1.2 (c (n "formatjson") (v "0.1.2") (d (list (d (n "miette") (r "^7.2.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1amgr8kmj3nypjihkv4n8r7npywn60vqwdqjk20rkv15550b992p")))

(define-public crate-formatjson-0.1.3 (c (n "formatjson") (v "0.1.3") (d (list (d (n "miette") (r "^7.2.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0h30dv1kmr1s0x5104sb3wbhj907kdd47n98agz7d8nk27rli95l")))

(define-public crate-formatjson-0.1.4 (c (n "formatjson") (v "0.1.4") (d (list (d (n "miette") (r "^7.2.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1z6yn1vpmiq83y1bcmbdw7g7mlfzxr61wmv481skdrwfxj9zs306")))

(define-public crate-formatjson-0.1.5 (c (n "formatjson") (v "0.1.5") (d (list (d (n "miette") (r "^7.2.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1y4ywdmvczi6146gzk5cirq178izvwzvgvy8s9cm37yp7h776nlj")))

(define-public crate-formatjson-0.2.0 (c (n "formatjson") (v "0.2.0") (d (list (d (n "miette") (r "^7.2.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "14kd45kx2ydwk216sw5qm3ra7bdipk80qrb5iba688r7grznfkx0")))

(define-public crate-formatjson-0.2.1 (c (n "formatjson") (v "0.2.1") (d (list (d (n "miette") (r "^7.2.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1gr4k5l3q2kshn4llhw8ga02gqc112s9hjjcbk99f0pm5rnkalbg")))

(define-public crate-formatjson-0.3.0 (c (n "formatjson") (v "0.3.0") (d (list (d (n "miette") (r "^7.2.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "088b5wsx2p3x65sks6ld8lgl9q2a4zq48li30kxmnz3ha252a9m9")))

(define-public crate-formatjson-0.3.1 (c (n "formatjson") (v "0.3.1") (d (list (d (n "miette") (r "^7.2.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1vg2f09zfijlbp6asgm53v2mcixk2gngyasvydlqkzrazrya2fsd")))

