(define-module (crates-io fo rm formal_spec) #:use-module (crates-io))

(define-public crate-formal_spec-0.0.1 (c (n "formal_spec") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1r2q00wih16pisbilvqykqk2pl3z5v5rdkl6kj9z0zfk8y8nihd2")))

