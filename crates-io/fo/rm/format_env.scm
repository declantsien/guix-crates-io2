(define-module (crates-io fo rm format_env) #:use-module (crates-io))

(define-public crate-format_env-1.0.0 (c (n "format_env") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "0sv7ab4j9c3g80f2k6vi03gav3j15fi1nl271kbfvgy2aj8yvyrd")))

(define-public crate-format_env-1.0.1 (c (n "format_env") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "19ns8nqmaj54f120vvfbbf80jyx25pgpn630m1s5w80fmd0dmfgw")))

