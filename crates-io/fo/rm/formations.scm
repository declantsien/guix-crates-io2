(define-module (crates-io fo rm formations) #:use-module (crates-io))

(define-public crate-formations-0.1.0 (c (n "formations") (v "0.1.0") (d (list (d (n "termsize") (r "^0.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5") (d #t) (k 0)))) (h "1i3p321q5czrxbmv5p55d7j4mjrn5y4fmbvn1id6qj8jpz375wrm")))

