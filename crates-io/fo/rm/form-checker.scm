(define-module (crates-io fo rm form-checker) #:use-module (crates-io))

(define-public crate-form-checker-0.2.0 (c (n "form-checker") (v "0.2.0") (d (list (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "0nmx46msg61ii01g4v45h94bqzpkvavxl6v1hh7djrzhgnq5r52d")))

(define-public crate-form-checker-0.2.1 (c (n "form-checker") (v "0.2.1") (d (list (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "15xgy9bmpdf5ddqihh8l7id922yxkparyyx00s5g1lsx44m7xan1")))

(define-public crate-form-checker-0.2.2 (c (n "form-checker") (v "0.2.2") (d (list (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "0bvmndk2x6bhsdv79hpya2dmp0nswl1rcaaxq4lg7vniyasl1002")))

