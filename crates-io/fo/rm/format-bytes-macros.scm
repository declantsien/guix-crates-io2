(define-module (crates-io fo rm format-bytes-macros) #:use-module (crates-io))

(define-public crate-format-bytes-macros-0.1.0 (c (n "format-bytes-macros") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xwm2w4850519vhvx1xk7rysvns4xfsfbq337495f5cz8c14xjjk")))

(define-public crate-format-bytes-macros-0.1.1 (c (n "format-bytes-macros") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0glx7lamgmisclfkq12v8bvlpbq89nhryl6hylp4zwmb6ikakfkh")))

(define-public crate-format-bytes-macros-0.1.2 (c (n "format-bytes-macros") (v "0.1.2") (d (list (d (n "pretty_assertions") (r ">=0.6.0, <0.7.0") (d #t) (k 2)) (d (n "proc-macro-hack") (r ">=0.5.0, <0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.0, <2.0.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1g1c8gl5r8qmqpc6wzjcj84vlgxr8syxwylkdc77m8ff051c1p2f")))

(define-public crate-format-bytes-macros-0.2.0 (c (n "format-bytes-macros") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0i8f0vzm8ws5kjvvspw1vsk7qp40sk2q4ax34p4lz8g635y7x633")))

(define-public crate-format-bytes-macros-0.3.0 (c (n "format-bytes-macros") (v "0.3.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vbb9h5fvnrqv9caxpph0w5qd5qmnvvkn311w94h8im087iqjl5h")))

(define-public crate-format-bytes-macros-0.4.0 (c (n "format-bytes-macros") (v "0.4.0") (d (list (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0kvyrsk9y3pjmlr006wrpafgjc7qmf724v1970hd2wycxzmssfi0")))

