(define-module (crates-io fo rm formatx) #:use-module (crates-io))

(define-public crate-formatx-0.1.0 (c (n "formatx") (v "0.1.0") (h "1qxd1hhbxz9g0p37hbcg0dmbahhrijbhiv3wq1j9v87b5hp8vrq6")))

(define-public crate-formatx-0.1.1 (c (n "formatx") (v "0.1.1") (h "12nbyijvdhgpkkjzxq5cfsnykwq1h7ayxvja1xzmb1f1m29zmgri")))

(define-public crate-formatx-0.1.2 (c (n "formatx") (v "0.1.2") (h "0n5lhlbf86i2sclq01m2a4cixdwp7hyfbhpbsxq7wz70al7x8g9y")))

(define-public crate-formatx-0.1.3 (c (n "formatx") (v "0.1.3") (h "1xihrar2vis7mwkbjlgjnnz1srnhr8v8q0nx19ngk4givvjihk2n")))

(define-public crate-formatx-0.1.4 (c (n "formatx") (v "0.1.4") (h "071rh6bns91fzl7bn96cqhb61ml88w4ni2f7ay1x7c52b0064k79")))

(define-public crate-formatx-0.2.0 (c (n "formatx") (v "0.2.0") (h "0l8l6vnhachr1plrsn1966281zf5rlpjvjw82668z95lvlvpsgrh")))

(define-public crate-formatx-0.2.1 (c (n "formatx") (v "0.2.1") (h "0a55ij9m2bz5vbd80m0cmpj66k0mfrkxns4ksdvs631mk2l6s6r0")))

(define-public crate-formatx-0.2.2 (c (n "formatx") (v "0.2.2") (h "00zydrncsmsm1ihn5zbjyrgqhhnj1fb6cmrig0jkp2m9md4hq3yv")))

