(define-module (crates-io fo rm format-core) #:use-module (crates-io))

(define-public crate-format-core-0.1.0 (c (n "format-core") (v "0.1.0") (h "0j21bcwvh77cal44lzr9v4q4rdsdzi9byp4rzjagxqksz8ikm83m")))

(define-public crate-format-core-0.1.1 (c (n "format-core") (v "0.1.1") (h "08qwa292yn3jrr2swmqr3wp8v3r0jb96abbxqd45wdmls53kkw7s")))

(define-public crate-format-core-0.1.2 (c (n "format-core") (v "0.1.2") (h "13qbrsjwqfhymv8w33yb8fc2sdb2dicdla9g4z9q59qm3y0iiz82")))

(define-public crate-format-core-0.1.3 (c (n "format-core") (v "0.1.3") (h "1za2hxa4db1c7hip72chmhwd1kir1vc93967pxqjxgzfhnksdr00")))

(define-public crate-format-core-0.2.0 (c (n "format-core") (v "0.2.0") (h "1sl3kwazq96r0f8mg6n0yrc4gnzklqyikaflkm56c3h52fpy4v7i")))

(define-public crate-format-core-0.2.1 (c (n "format-core") (v "0.2.1") (h "1f7bs7n0lcy0ypdfnyvfqhbgd0xzl311y6yyizb18rdnl08w109z")))

(define-public crate-format-core-0.2.2 (c (n "format-core") (v "0.2.2") (h "04x520qfj8mbd02z50jrv9lsr12f8bhn1v48gwipyir5a9h0mdy6")))

(define-public crate-format-core-0.2.3 (c (n "format-core") (v "0.2.3") (h "0i654c4ri7j4dglm79flcq90ixbaynjp6pssjmws5hbf2mn4irhy")))

(define-public crate-format-core-0.2.4 (c (n "format-core") (v "0.2.4") (h "1xfkm6md3nswg0irr4c7izqzwsqdkrjd0gg4fd5h8iqcf3b70sz6")))

