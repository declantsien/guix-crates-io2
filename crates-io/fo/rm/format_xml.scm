(define-module (crates-io fo rm format_xml) #:use-module (crates-io))

(define-public crate-format_xml-0.1.0 (c (n "format_xml") (v "0.1.0") (h "1hf21k97yy9fk89z0fqnd63s9y4dhk7i99k1fv79x3lvnd4kjsij")))

(define-public crate-format_xml-0.1.1 (c (n "format_xml") (v "0.1.1") (h "10v2dd9b6vyipl7vhp9dkn3bcgmzm2wyl9hl91yy4h7f28rbcscj")))

(define-public crate-format_xml-0.1.2 (c (n "format_xml") (v "0.1.2") (h "1xhx6pp5xhxggdxkzrxzqvc81pwb2yncdfa918p6sbzhjk7c2jir")))

(define-public crate-format_xml-0.1.3 (c (n "format_xml") (v "0.1.3") (h "0i4lsiwwccijaygcf5fz8cjdh0758195gcg4kigs1qmicg5d4xi3")))

(define-public crate-format_xml-0.1.4 (c (n "format_xml") (v "0.1.4") (h "1pzk2gshyd2c9vz7v91jvvzad6xbcywa1fa4g73q3f48s6azlv6m")))

(define-public crate-format_xml-0.1.5 (c (n "format_xml") (v "0.1.5") (h "01i3ln5m3v3wqsv49mcl8rmk1g6xpddx5n6qfsic1dgywwn9aym9")))

(define-public crate-format_xml-0.1.6 (c (n "format_xml") (v "0.1.6") (h "1m4babrax8mqj72702d3y6f1m0rvs360sm370prcxp5fnmka95sh")))

(define-public crate-format_xml-0.2.0 (c (n "format_xml") (v "0.2.0") (h "1a3wnbiq1g7gk5i0x14pmgffdk51gl2mps5lbmkyrn8av8h3kh3a")))

(define-public crate-format_xml-0.3.0 (c (n "format_xml") (v "0.3.0") (d (list (d (n "fmtools") (r "^0.1") (d #t) (k 0)))) (h "096b4g0ivqzx37m0hl0iqrvb060n4vbv4n69jfmvkxbhdl17z3gk") (f (quote (("std") ("obfstr" "fmtools/obfstr") ("default" "std"))))))

