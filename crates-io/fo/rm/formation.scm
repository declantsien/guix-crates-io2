(define-module (crates-io fo rm formation) #:use-module (crates-io))

(define-public crate-formation-0.0.0 (c (n "formation") (v "0.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty") (r "^0.10.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "sqlparser") (r "^0.5.0") (d #t) (k 0)))) (h "1qqnfrhnawjw0qhwxyilbwcv1jda5ljdrsxsy3a3yl2rnhhw4aj8")))

(define-public crate-formation-0.1.0 (c (n "formation") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty") (r "^0.10.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rstest") (r "^0.6.3") (d #t) (k 2)) (d (n "sqlparser") (r "^0.5.0") (d #t) (k 0)))) (h "15k65fm9nrvw4lvs9hfqm49n62pc2588bw06jf9fd7hildgf6y49")))

(define-public crate-formation-0.1.1 (c (n "formation") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty") (r "^0.10.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rstest") (r "^0.6.3") (d #t) (k 2)) (d (n "sqlparser") (r "^0.5.0") (d #t) (k 0)))) (h "1ydikb7gnl50qfx4v6m5n34d3qpa76lyh55ah83i6hi4v32dv3pi")))

(define-public crate-formation-0.1.2 (c (n "formation") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty") (r "^0.10.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rstest") (r "^0.6.3") (d #t) (k 2)) (d (n "sqlparser") (r "^0.5.0") (d #t) (k 0)))) (h "1ksmv8p5p6m66xaqwdzgs9fsqzfarf04ncimb7d1qkij58k83rdq")))

(define-public crate-formation-0.2.0 (c (n "formation") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty") (r "^0.10.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rstest") (r "^0.6.3") (d #t) (k 2)) (d (n "sqlparser") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "00f4ccsk49s04zvqy189qn15j3mykf966k7b0g3dcarvr4660sk5")))

(define-public crate-formation-0.3.0 (c (n "formation") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty") (r "^0.10.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rstest") (r "^0.6.3") (d #t) (k 2)) (d (n "sqlparser") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "04969wfq31ris5bsyksf7il5s5as2sbylsl0wdwcpvbzbsmhzvxc")))

