(define-module (crates-io fo rm format-sql-query) #:use-module (crates-io))

(define-public crate-format-sql-query-0.1.0 (c (n "format-sql-query") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "0vin6ix58j8lln9izb9m3b040qh9mxjvs0ssb5w211gifbxd0xc3")))

(define-public crate-format-sql-query-0.2.0 (c (n "format-sql-query") (v "0.2.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "0qjan7dhgaq2d5qdkw9bbzmrm0l06w8yilz4h1sqvsa1ivsg5302")))

(define-public crate-format-sql-query-0.3.0 (c (n "format-sql-query") (v "0.3.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "13n7lx6nlyppxc1j8j4wvpgxl6sxlxyc0v02jcf704bnl0jzkjpf")))

(define-public crate-format-sql-query-0.3.1 (c (n "format-sql-query") (v "0.3.1") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "0n5bkydp2yw5zhh3s4mmzqrd768dyz4n14v90dcy2a1gqafw53r8")))

(define-public crate-format-sql-query-0.4.0 (c (n "format-sql-query") (v "0.4.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "1557q6ds534g9nirfp529vg80mfw7xy2lf42myv3ifbg5p6626qb")))

