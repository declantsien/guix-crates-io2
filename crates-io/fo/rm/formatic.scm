(define-module (crates-io fo rm formatic) #:use-module (crates-io))

(define-public crate-formatic-0.1.0 (c (n "formatic") (v "0.1.0") (d (list (d (n "object") (r "^0.34.0") (f (quote ("write"))) (d #t) (k 0)))) (h "02aarxw77hnccjrf6537vkxb8m31fv3yrfxr5p82s5w41r0b25mk")))

(define-public crate-formatic-0.1.1 (c (n "formatic") (v "0.1.1") (d (list (d (n "object") (r "^0.34.0") (f (quote ("write"))) (d #t) (k 0)))) (h "1vjsv7laa7xfkry0lbb9k9g6qz3zpp98mjig81xxhkznjyp5z00d")))

