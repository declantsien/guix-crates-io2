(define-module (crates-io fo rm format-url) #:use-module (crates-io))

(define-public crate-format-url-0.4.0 (c (n "format-url") (v "0.4.0") (d (list (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)))) (h "1q7i9agzrfhnx8bck0jb5mafxw23c4vp75afr4zdjir72sng4j7a")))

(define-public crate-format-url-0.5.0 (c (n "format-url") (v "0.5.0") (d (list (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 0)))) (h "16x9wy0kwwklhzdr8ir3rs1kbmb0yzb0wnkx1hcqx73kw4043cgj")))

(define-public crate-format-url-0.6.1 (c (n "format-url") (v "0.6.1") (d (list (d (n "percent-encoding") (r "^2") (d #t) (k 0)))) (h "1sal4k5l28dacn3w8r3s5pmcgyhfnqf0zj0aakchqs972wlgy4w0")))

(define-public crate-format-url-0.6.2 (c (n "format-url") (v "0.6.2") (d (list (d (n "percent-encoding") (r "^2") (d #t) (k 0)))) (h "162b8bj308w2g4a80g5q7j3vqw81knayzk15wf6g24m9n4wdryz1")))

