(define-module (crates-io fo rm format_escape_default) #:use-module (crates-io))

(define-public crate-format_escape_default-0.1.0 (c (n "format_escape_default") (v "0.1.0") (h "1g9kkzvbadz6126ip9k6zxh7nl4zh56bv43anjc57i1rw7a71v0b")))

(define-public crate-format_escape_default-0.1.1 (c (n "format_escape_default") (v "0.1.1") (h "1s9r8rmki6in6nnmizd7lhwji002smc1nh7736zc3q81q4ps5cnd")))

