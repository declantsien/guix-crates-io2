(define-module (crates-io fo rm formula_3d_shape_lib) #:use-module (crates-io))

(define-public crate-formula_3d_shape_lib-0.1.0 (c (n "formula_3d_shape_lib") (v "0.1.0") (h "1rj16pnkwzhwi532gyp7jv8vyqpfd2fn7x33iz4di2dl34lm7cx5") (r "1.56")))

(define-public crate-formula_3d_shape_lib-0.1.1 (c (n "formula_3d_shape_lib") (v "0.1.1") (h "1mh1zagg6p32vmq005h4fw8jd1sb53pj93ci2k6rbd8rnfadl9qx") (r "1.56")))

