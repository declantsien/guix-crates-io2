(define-module (crates-io fo rm former_runtime) #:use-module (crates-io))

(define-public crate-former_runtime-0.1.0 (c (n "former_runtime") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "15wcl1yz7q8sx5gyz7b293dqbp89h1yyi8p1q7hklxm2b3ngi723")))

(define-public crate-former_runtime-0.1.1 (c (n "former_runtime") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "03ivl9hklagrq58f6rqxahxl6hj65n1amp0sg2h2qmc2b5540h6p")))

(define-public crate-former_runtime-0.1.2 (c (n "former_runtime") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (f (quote ("diff"))) (d #t) (k 2)))) (h "059mgd9rq3f79gl8xbsnz2hpakgw5f9wwlcgyvin2ws0ah4c0lpn")))

(define-public crate-former_runtime-0.1.3 (c (n "former_runtime") (v "0.1.3") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 0)) (d (n "maplit") (r "~1.0.2") (d #t) (k 2)) (d (n "trybuild") (r "~1.0") (f (quote ("diff"))) (d #t) (k 2)) (d (n "unzip-n") (r "~0.1") (d #t) (k 0)) (d (n "wtest") (r "~0") (d #t) (k 2)))) (h "1wb08rc4wf3b0p3fsn4a1cy9bz24g7fvzyfrn1i6p9n7qnsl1q61")))

(define-public crate-former_runtime-0.1.4 (c (n "former_runtime") (v "0.1.4") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 0)) (d (n "maplit") (r "~1.0.2") (d #t) (k 2)) (d (n "unzip-n") (r "~0.1") (d #t) (k 0)) (d (n "wtest") (r "~0") (d #t) (k 2)))) (h "1a02kfc8561jdgrs0w7qpphv6pajv3cab7yniaidm4fgjxczfa7p")))

(define-public crate-former_runtime-0.1.5 (c (n "former_runtime") (v "0.1.5") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 0)) (d (n "maplit") (r "~1.0.2") (d #t) (k 2)) (d (n "unzip-n") (r "~0.1") (d #t) (k 0)) (d (n "wtest") (r "~0") (d #t) (k 2)))) (h "0nvkj34azs70hdh4kq6j7wka0hmcnspjnljxim8vj69x3crv7gxh")))

(define-public crate-former_runtime-0.1.6 (c (n "former_runtime") (v "0.1.6") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 0)) (d (n "maplit") (r "~1.0.2") (d #t) (k 2)) (d (n "unzip-n") (r "~0.1") (d #t) (k 0)) (d (n "wtest_basic") (r "~0") (d #t) (k 2)))) (h "1sa96jjh41hpn6r7xx852cwvnbasm9hzl8hidg06jxxdb3cvcnv6")))

(define-public crate-former_runtime-0.1.7 (c (n "former_runtime") (v "0.1.7") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 2)) (d (n "maplit") (r "~1.0.2") (d #t) (k 2)) (d (n "wtest_basic") (r "~0") (d #t) (k 2)))) (h "0gxzhyjncsp9k2xh1w746vjhjmjd7yir16czha9m8y4avh35jfqa")))

(define-public crate-former_runtime-0.1.8 (c (n "former_runtime") (v "0.1.8") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 2)) (d (n "maplit") (r "~1.0.2") (d #t) (k 2)) (d (n "wtest_basic") (r "~0") (d #t) (k 2)))) (h "1781llb1rsd3xp10l215bk25v3vxas0smyfwiyr2v1vqvhpnccr1")))

(define-public crate-former_runtime-0.1.9 (c (n "former_runtime") (v "0.1.9") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 2)) (d (n "meta_tools") (r "~0.1") (d #t) (k 2)) (d (n "wtest_basic") (r "~0.1") (d #t) (k 2)))) (h "1xryvsbszk6zfwpz4b9imj057hip9dvvb5nrxa0gwams0anvng0h")))

(define-public crate-former_runtime-0.1.10 (c (n "former_runtime") (v "0.1.10") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 2)) (d (n "meta_tools") (r "~0.1") (d #t) (k 2)) (d (n "wtest_basic") (r "~0.1") (d #t) (k 2)))) (h "0llr3hld7ngyw4jsw6gb59qdrygwbyxpr798q5hx2khk8s889nqs")))

(define-public crate-former_runtime-0.1.11 (c (n "former_runtime") (v "0.1.11") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 2)) (d (n "meta_tools") (r "~0.2") (d #t) (k 2)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "15k6fvvjlc8pi453mj4dw4lvy2ga1l2549rbd95qfcfpas9xnqy6")))

