(define-module (crates-io fo rm forma) #:use-module (crates-io))

(define-public crate-forma-0.0.0 (c (n "forma") (v "0.0.0") (d (list (d (n "pretty") (r "^0.10.0") (d #t) (k 0)) (d (n "sqlparser") (r "^0.5.0") (d #t) (k 0)))) (h "0s64n9h8c2igpisybz4wxd8qjc45gmb5dipshj6y4bl4zlj1bzz1")))

(define-public crate-forma-0.1.0 (c (n "forma") (v "0.1.0") (d (list (d (n "formation") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "164w4vkkfqyjli3lm3vfv5bagqqbrhlz9sq4c9nn886zz4z2vq74")))

(define-public crate-forma-0.1.1 (c (n "forma") (v "0.1.1") (d (list (d (n "formation") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1jz8vji261ds1lvwyl5srqy0yn01sns2kdwwpaq53dpkh4w3fdvp")))

(define-public crate-forma-0.1.2 (c (n "forma") (v "0.1.2") (d (list (d (n "formation") (r "^0.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1g1y9b9k6kdmadd0r612yrkmqpiv7r6ayw438mncabx22yim2cic")))

(define-public crate-forma-0.2.0 (c (n "forma") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "formation") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "09lkawj9c63644773pc9d8iw642xrhzacdbawkhglsxwi3fnn3cc")))

(define-public crate-forma-0.3.0 (c (n "forma") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 2)) (d (n "formation") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.15") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "18lzy008gp02jzhmmj76avwyifxcqvpn2w2md1khfxjhhnya506v")))

