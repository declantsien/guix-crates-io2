(define-module (crates-io fo rm format-bom) #:use-module (crates-io))

(define-public crate-format-bom-0.0.1 (c (n "format-bom") (v "0.0.1") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 0)))) (h "15h2zs8xwrsl9ffd3pnb2qcih3471ipi88qmi4n8yh3aq7a4zyvq")))

(define-public crate-format-bom-0.0.2 (c (n "format-bom") (v "0.0.2") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 0)))) (h "1qngjl7i6hq4k456hncbq1cblfaknrqxp3zjcj1lcms7pkf6vx2k")))

(define-public crate-format-bom-0.0.3 (c (n "format-bom") (v "0.0.3") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 0)))) (h "05yqnkc7fd0jpshkn78m8p1z4d4agsgfjppsq994w4i676rvx3xc")))

(define-public crate-format-bom-0.0.4 (c (n "format-bom") (v "0.0.4") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 0)))) (h "163xnyrvxcxmg6mhbmjwh2510c4q2mg28pxkx7f3267pwjypm96i")))

