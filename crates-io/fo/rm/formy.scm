(define-module (crates-io fo rm formy) #:use-module (crates-io))

(define-public crate-formy-0.1.0-alpha.0 (c (n "formy") (v "0.1.0-alpha.0") (d (list (d (n "formy_derive") (r "^0.1.0-alpha.0") (d #t) (k 0)))) (h "027zq0wxsg8l9g14azplawlmikrm8vnpapai95vhfg3drxl8rfli")))

(define-public crate-formy-0.1.0-alpha.1 (c (n "formy") (v "0.1.0-alpha.1") (d (list (d (n "formy_derive") (r "^0.1.0-alpha.1") (d #t) (k 0)))) (h "0bwlmv0jjvf90sihwn4kgpylax2ns8yc92z116digm4i3z9101r1")))

(define-public crate-formy-0.1.0 (c (n "formy") (v "0.1.0") (d (list (d (n "formy_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1p986c1aiscmr3kqhbk35ngjf5a1j7xjm4ij3051pl4sr3p4195g")))

(define-public crate-formy-0.2.0 (c (n "formy") (v "0.2.0") (d (list (d (n "formy_derive") (r "^0.2.0") (d #t) (k 0)))) (h "05ycij58qfqzkyf3dlbjlba039x5fssn5aw0k0g8zjpwa9nzjf0j")))

(define-public crate-formy-0.2.1 (c (n "formy") (v "0.2.1") (d (list (d (n "formy_derive") (r "^0.2.1") (d #t) (k 0)))) (h "003v150x30gysgw6dm5qrwzm8i2vm0pqifrggbss1nrn36i8fpfj")))

(define-public crate-formy-0.2.2 (c (n "formy") (v "0.2.2") (d (list (d (n "formy_derive") (r "^0.2.2") (d #t) (k 0)))) (h "1z3qawdbvz5cshqrd4kzfbbjvr7amz84pjbm8spcmy9wl8hymj65")))

