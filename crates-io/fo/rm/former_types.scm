(define-module (crates-io fo rm former_types) #:use-module (crates-io))

(define-public crate-former_types-2.1.0 (c (n "former_types") (v "2.1.0") (d (list (d (n "collection_tools") (r "~0.8.0") (f (quote ("collection_constructors"))) (k 0)) (d (n "test_tools") (r "~0.9.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1bcgqai206lpy9qw6f969d8sb5hq2ag47g7ciyxbj8n3hraqm77z") (f (quote (("use_alloc" "no_std" "collection_tools/use_alloc") ("types_components") ("types_component_assign" "types_components") ("no_std" "collection_tools/no_std") ("full" "enabled" "derive_former" "types_components" "types_component_assign") ("enabled" "collection_tools/enabled") ("derive_former") ("default" "enabled" "derive_former" "types_components" "types_component_assign"))))))

