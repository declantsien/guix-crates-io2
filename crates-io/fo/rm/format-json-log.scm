(define-module (crates-io fo rm format-json-log) #:use-module (crates-io))

(define-public crate-format-json-log-0.1.0 (c (n "format-json-log") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored_json") (r "^3.2.0") (d #t) (k 0)) (d (n "insta") (r "^1.30.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.97") (d #t) (k 0)) (d (n "yansi") (r "^0.5.1") (d #t) (k 0)))) (h "16gfdpnpvjnlrx9jwlah3wmj9wzy78hddfj4mgpj643px5jgyibh")))

