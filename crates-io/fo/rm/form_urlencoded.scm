(define-module (crates-io fo rm form_urlencoded) #:use-module (crates-io))

(define-public crate-form_urlencoded-1.0.0 (c (n "form_urlencoded") (v "1.0.0") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "005yi1319k5bz8g5ylbdiakq5jp5jh90yy6k357zm11fr4aqvrpc")))

(define-public crate-form_urlencoded-1.0.1 (c (n "form_urlencoded") (v "1.0.1") (d (list (d (n "matches") (r "^0.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1491fmakavcmsjbm3q6iy0bhmn9l422jasdhzx5hkljgza3mmhjz")))

(define-public crate-form_urlencoded-1.1.0 (c (n "form_urlencoded") (v "1.1.0") (d (list (d (n "percent-encoding") (r "^2.2.0") (d #t) (k 0)))) (h "1y3bwavygjzv7b0yqsjqk33yi6wz25b7q2aaq9h54vqmc7qq9hx9") (r "1.51")))

(define-public crate-form_urlencoded-1.2.0 (c (n "form_urlencoded") (v "1.2.0") (d (list (d (n "percent-encoding") (r "^2.3.0") (k 0)))) (h "0ljn0kz23nr9yf3432k656k178nh4jqryfji9b0jw343dz7w2ax6") (f (quote (("std" "alloc" "percent-encoding/std") ("default" "std") ("alloc" "percent-encoding/alloc")))) (r "1.51")))

(define-public crate-form_urlencoded-1.2.1 (c (n "form_urlencoded") (v "1.2.1") (d (list (d (n "percent-encoding") (r "^2.3.0") (k 0)))) (h "0milh8x7nl4f450s3ddhg57a3flcv6yq8hlkyk6fyr3mcb128dp1") (f (quote (("std" "alloc" "percent-encoding/std") ("default" "std") ("alloc" "percent-encoding/alloc")))) (r "1.51")))

