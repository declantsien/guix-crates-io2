(define-module (crates-io fo rm formality-macros) #:use-module (crates-io))

(define-public crate-formality-macros-0.1.0 (c (n "formality-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0d5iiwmvyzg3c2snpprrybwj086b2i2var0q976aj8h3xr17rrfc")))

