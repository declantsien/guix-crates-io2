(define-module (crates-io fo rm form-validation) #:use-module (crates-io))

(define-public crate-form-validation-0.1.0 (c (n "form-validation") (v "0.1.0") (d (list (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0hki91bx1h9sxmnyqx1wwxxim4klrxhl466kx701ff79i3714d2r")))

(define-public crate-form-validation-0.1.1 (c (n "form-validation") (v "0.1.1") (d (list (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1z6089cqfn0vkhz1qs9lhzpc7ghl43m8i0b6l4g28akq4hqmi8cs")))

(define-public crate-form-validation-0.1.2 (c (n "form-validation") (v "0.1.2") (d (list (d (n "stdweb") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1xkzfsa8l8sik24k9rxyk21f248sgqsqmwb9qwdj3hp7i156vi3i") (f (quote (("wasm-bindgen-support" "wasm-bindgen" "uuid/wasm-bindgen") ("stdweb-support" "stdweb" "uuid/stdweb") ("default"))))))

(define-public crate-form-validation-0.2.0 (c (n "form-validation") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "stdweb") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1pi4rvhyxaawvgrcz4xm51dh80fh6qc8qb8ivajp0szkqyqvddww") (f (quote (("wasm-bindgen-support" "wasm-bindgen" "uuid/wasm-bindgen") ("stdweb-support" "stdweb" "uuid/stdweb") ("default") ("async" "futures"))))))

(define-public crate-form-validation-0.3.0 (c (n "form-validation") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "stdweb") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1dpzx7bmdws5caxrvfffwjwhy898757f789cg92dhnkrbj7y3lhl") (f (quote (("wasm-bindgen-support" "wasm-bindgen" "uuid/wasm-bindgen") ("stdweb-support" "stdweb" "uuid/stdweb") ("default") ("async" "futures"))))))

(define-public crate-form-validation-0.3.1 (c (n "form-validation") (v "0.3.1") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "stdweb") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0c2vhw041i4cplx1q8f5dnbqfy1qmvv3bsxldihx4v9giwdmhmcg") (f (quote (("wasm-bindgen-support" "wasm-bindgen" "uuid/wasm-bindgen") ("stdweb-support" "stdweb" "uuid/stdweb") ("default") ("async" "futures"))))))

