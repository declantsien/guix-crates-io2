(define-module (crates-io fo pe fopencookie-sys) #:use-module (crates-io))

(define-public crate-fopencookie-sys-0.1.0 (c (n "fopencookie-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.97") (d #t) (k 1)))) (h "1w6b1sbq9ky7428mp53sbm1j6qzxljyrr6mn9pqhmqwnmfz37l22")))

(define-public crate-fopencookie-sys-0.1.1 (c (n "fopencookie-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cc") (r "^1.0.97") (d #t) (k 1)))) (h "15f1yz0v018gwznxxf15cjlsah9s8mrdbnbgriqrkpah12g7d1aq")))

