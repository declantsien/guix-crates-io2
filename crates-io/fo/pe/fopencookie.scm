(define-module (crates-io fo pe fopencookie) #:use-module (crates-io))

(define-public crate-fopencookie-0.1.0 (c (n "fopencookie") (v "0.1.0") (d (list (d (n "cstream") (r "^0.1.0") (d #t) (k 0)) (d (n "fopencookie-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.154") (d #t) (k 0)))) (h "028glcx51gi8n0037zrylkarynx2fcg9ii1axks4j9mlnl1m6wbd")))

(define-public crate-fopencookie-0.1.1 (c (n "fopencookie") (v "0.1.1") (d (list (d (n "cstream") (r "^0.1.1") (d #t) (k 0)) (d (n "fopencookie-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.154") (d #t) (k 0)))) (h "0a49kg9xn85b0xbklqzi3npq1g4yj5ayh14b1xdkcj7gcqnywck3")))

