(define-module (crates-io fo nd fondant) #:use-module (crates-io))

(define-public crate-fondant-0.1.0 (c (n "fondant") (v "0.1.0") (d (list (d (n "fondant_deps") (r "^0.1.0") (d #t) (k 0)) (d (n "fondant_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0rdbqgfyljd17s35y91hnn7wr4lcl2sqi65k1526fxmm78ggqnmx")))

(define-public crate-fondant-0.1.1 (c (n "fondant") (v "0.1.1") (d (list (d (n "fondant_deps") (r "^0.1.0") (d #t) (k 0)) (d (n "fondant_derive") (r "^0.1.1") (d #t) (k 0)))) (h "1413nlqvqxnz792fh8mng9brdkh1hfvkj05llv93spn70gqzx2vk")))

(define-public crate-fondant-0.1.2 (c (n "fondant") (v "0.1.2") (d (list (d (n "fondant_deps") (r "^0.1.1") (d #t) (k 0)) (d (n "fondant_derive") (r "^0.1.1") (d #t) (k 0)))) (h "0xs1izgqbfvssqmk91fsqlrpp5cb19p71196gm5jq246xjin7kc4")))

