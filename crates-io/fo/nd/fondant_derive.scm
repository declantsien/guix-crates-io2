(define-module (crates-io fo nd fondant_derive) #:use-module (crates-io))

(define-public crate-fondant_derive-0.1.0 (c (n "fondant_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vgdrww3v0j9c4fwlgsmx7138ljwnh4b9sq9vj1dg5hj02p8rls2")))

(define-public crate-fondant_derive-0.1.1 (c (n "fondant_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1z5zm9rl3qh6cv23pbfh3ya3nnawgaxjlnyskk4aaa7dlm9112p2")))

