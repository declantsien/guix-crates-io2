(define-module (crates-io fo ba foba) #:use-module (crates-io))

(define-public crate-foba-0.0.1 (c (n "foba") (v "0.0.1") (h "0nxch65b259lh732708fck3d4vsqbv3ajmjjygpl2davahgbfq53")))

(define-public crate-foba-0.0.2 (c (n "foba") (v "0.0.2") (h "0shjfddk49i9dvhjmngbc2qvv2lv2z9nh9hrfinrn96zrbhlpxqf")))

