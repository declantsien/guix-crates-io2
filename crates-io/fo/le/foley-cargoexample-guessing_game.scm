(define-module (crates-io fo le foley-cargoexample-guessing_game) #:use-module (crates-io))

(define-public crate-foley-cargoexample-guessing_game-0.1.0 (c (n "foley-cargoexample-guessing_game") (v "0.1.0") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "028f5ji37d3wnzyxjymgyalkvkjvij80qgza02p9bfhc1q4fg87n")))

(define-public crate-foley-cargoexample-guessing_game-0.1.1 (c (n "foley-cargoexample-guessing_game") (v "0.1.1") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "09gwy3hbbika5z01wqw0c64h2rq22d0f5n2h6x47j02pk1g4vnf6")))

