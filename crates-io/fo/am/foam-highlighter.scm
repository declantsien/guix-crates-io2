(define-module (crates-io fo am foam-highlighter) #:use-module (crates-io))

(define-public crate-foam-highlighter-0.1.0 (c (n "foam-highlighter") (v "0.1.0") (d (list (d (n "tree-sitter-foam") (r "^0.1.2") (d #t) (k 0)) (d (n "tree-sitter-highlight") (r "^0.20.1") (d #t) (k 0)))) (h "17sv1c4alwg01qam5pdlxwlc2mml2wj61m4sl80803wm32qsiyhi")))

(define-public crate-foam-highlighter-0.1.1 (c (n "foam-highlighter") (v "0.1.1") (d (list (d (n "tree-sitter-foam") (r "^0.1.3") (d #t) (k 0)) (d (n "tree-sitter-highlight") (r "^0.20.1") (d #t) (k 0)))) (h "15nmb2zhsw0a28kjvxrb1i81gwnisp0wl6g05167hzgm8dasg3gw")))

(define-public crate-foam-highlighter-0.1.2 (c (n "foam-highlighter") (v "0.1.2") (d (list (d (n "tree-sitter-foam") (r "^0.1.3") (d #t) (k 0)) (d (n "tree-sitter-highlight") (r "^0.20.1") (d #t) (k 0)))) (h "15mm2mi72zqn7hpfzqidk2wdk004w88h1q8ln3k8hazz787icghk")))

(define-public crate-foam-highlighter-0.1.3 (c (n "foam-highlighter") (v "0.1.3") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tree-sitter-foam") (r "^0.1.3") (d #t) (k 0)) (d (n "tree-sitter-highlight") (r "^0.20.1") (d #t) (k 0)))) (h "0akb9qwx1n0r9azfz2lr6nijsq8y9920h7csd9ys4wnfmwb9mji9")))

(define-public crate-foam-highlighter-0.2.0 (c (n "foam-highlighter") (v "0.2.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tree-sitter-foam") (r "^0.1.3") (d #t) (k 0)) (d (n "tree-sitter-highlight") (r "^0.20.1") (d #t) (k 0)))) (h "193fkfy37ak0bj7ylb2jxjdrafgcjnlq4mxmn4z9xk6j16kyfx5v")))

