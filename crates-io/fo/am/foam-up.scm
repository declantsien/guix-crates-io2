(define-module (crates-io fo am foam-up) #:use-module (crates-io))

(define-public crate-foam-up-1.0.0 (c (n "foam-up") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1l8kg8xpagl4knip0rcl35vgdcdza17ypy10a24wk3q2lg399kv1")))

(define-public crate-foam-up-2.0.0 (c (n "foam-up") (v "2.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ksyjg2dvbr6qjf56hjp9lnd19a25pvzfa3im5xs1xk1ymsamhc3")))

