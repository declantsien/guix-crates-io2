(define-module (crates-io fo r_ for_let) #:use-module (crates-io))

(define-public crate-for_let-0.1.0 (c (n "for_let") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "proc-macro2") (r "~1") (d #t) (k 0)) (d (n "quote") (r "~1") (d #t) (k 0)) (d (n "syn") (r "~1") (f (quote ("parsing" "full" "proc-macro" "printing"))) (k 0)))) (h "0v8d8cm2g18za52384nbsn4yma3p6gfixr8x12py3dxd86k1awix")))

