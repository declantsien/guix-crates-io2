(define-module (crates-io fo r_ for_event_bus) #:use-module (crates-io))

(define-public crate-for_event_bus-0.1.0 (c (n "for_event_bus") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.66") (d #t) (k 0)) (d (n "custom-utils") (r "^0.10.12") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "189lqa54ipkzz798qcdngybqg3ngwfzn2n4f6q3qkh54yx2wa6fl")))

(define-public crate-for_event_bus-0.1.1 (c (n "for_event_bus") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.66") (d #t) (k 0)) (d (n "custom-utils") (r "^0.10.12") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bhcl27wc3h5wnv0wqmyq6gchwpyzazbc7zdykmc22rq4hfm0yay")))

(define-public crate-for_event_bus-0.1.2 (c (n "for_event_bus") (v "0.1.2") (d (list (d (n "custom-utils") (r "^0.10.12") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0ipgygq923va2lqx6wr8hrxx5bvv0p58x4ay4gs0csc0akyxalci")))

(define-public crate-for_event_bus-0.1.3 (c (n "for_event_bus") (v "0.1.3") (d (list (d (n "custom-utils") (r "^0.10.14") (d #t) (k 2)) (d (n "for-event-bus-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 2)))) (h "027xxjhxvjrjw56rkw5aa5jhc0d3fjv8x4i0fx7s9kjl91kb28l0")))

(define-public crate-for_event_bus-0.1.4 (c (n "for_event_bus") (v "0.1.4") (d (list (d (n "custom-utils") (r "^0.10.14") (d #t) (k 2)) (d (n "for-event-bus-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0igfs16gk48y75rf0r1x2cv64b41657yzdxx6mr75xpy5d7v26k2")))

(define-public crate-for_event_bus-0.1.5 (c (n "for_event_bus") (v "0.1.5") (d (list (d (n "custom-utils") (r "^0.10.14") (d #t) (k 2)) (d (n "for-event-bus-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0ky2vg9y1bmaf4fd8wzc0lxh846gaazcdklk9nzg9s8cffz8d0ry")))

(define-public crate-for_event_bus-0.1.6 (c (n "for_event_bus") (v "0.1.6") (d (list (d (n "custom-utils") (r "^0.10.14") (d #t) (k 2)) (d (n "for-event-bus-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 2)))) (h "17x3a387gicy586nglhbmk05l2lryb5frkikj3986wlj9caqwyh1")))

