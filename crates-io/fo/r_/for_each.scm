(define-module (crates-io fo r_ for_each) #:use-module (crates-io))

(define-public crate-for_each-0.1.0 (c (n "for_each") (v "0.1.0") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0x8ihmi3hwp658k5aqd4gj9hmcad0kyzxrzsif8hmsc42g63ywl9")))

(define-public crate-for_each-0.1.1 (c (n "for_each") (v "0.1.1") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1yb7mdp70nz6qmc4hpl9khlf694rzyz7jqlspzc3g6qhza33xr7f")))

(define-public crate-for_each-0.1.2 (c (n "for_each") (v "0.1.2") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0h3hivfy3ha0gbpgqg23zzc1rhhkzk53klhzdn6kz0nf2xf9n347")))

(define-public crate-for_each-0.1.3 (c (n "for_each") (v "0.1.3") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "0vx7c1wi9q5z12xcw7pkmxd3ym11v7cb5bzhk8ylc1sqry2qjh76")))

(define-public crate-for_each-0.2.0 (c (n "for_each") (v "0.2.0") (h "0ix79nbi592hjcxl6irsix1hf9g43x1ccxkqj3l4y086a89vivzz") (f (quote (("use_alloc") ("no_std") ("full") ("default"))))))

(define-public crate-for_each-0.3.0 (c (n "for_each") (v "0.3.0") (d (list (d (n "test_tools") (r "^0.1.5") (d #t) (k 2)))) (h "08s0maiqc1h6a87a6pqzm4zfidlvx8x9sqs2wgyb8w56si6lvv42") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-for_each-0.4.0 (c (n "for_each") (v "0.4.0") (d (list (d (n "test_tools") (r "~0.5.0") (d #t) (k 2)))) (h "1crm9lc4zdn21amcsx9b3jg9vzir8szzai7r8l4wqh7w76wcjbzn") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-for_each-0.5.0 (c (n "for_each") (v "0.5.0") (d (list (d (n "test_tools") (r "~0.6.0") (d #t) (k 2)))) (h "12f5vhq9c7p07i7fk3v2h8a3icivz9nlfvkb9sfsldjm6qdiqsz8") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-for_each-0.6.0 (c (n "for_each") (v "0.6.0") (d (list (d (n "test_tools") (r "~0.7.0") (d #t) (k 2)))) (h "1rwnmdqn9ss7hzf8nhgzqzsbvj4z0vnf40gwd0n2g0kw1vacfnv9") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-for_each-0.7.0 (c (n "for_each") (v "0.7.0") (d (list (d (n "test_tools") (r "~0.7.0") (d #t) (k 2)))) (h "1j36wgpnyq1cxlb0w0kr026367l3whjwby0dzx6dc380sarjcrcn") (f (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-for_each-0.8.0 (c (n "for_each") (v "0.8.0") (d (list (d (n "test_tools") (r "~0.8.0") (d #t) (k 2)))) (h "0m2grx5w8b0fd71cify389lbzxwd4xbl2cl60jw3f7iqsvkcw7hc") (f (quote (("use_alloc" "no_std") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

