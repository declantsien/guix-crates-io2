(define-module (crates-io fo r_ for_ch) #:use-module (crates-io))

(define-public crate-for_ch-0.1.0 (c (n "for_ch") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "18ldnsfhqhkhflpchmrh0b94y8r36i36r05whzb1d0fw3q87qq0c")))

(define-public crate-for_ch-0.1.1 (c (n "for_ch") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0q6d98spqsx7ss46ks2kmanz5jf8zamwln47vfcwk96fcjlkys5c") (y #t)))

(define-public crate-for_ch-0.1.2 (c (n "for_ch") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "0k6grcqkv6zpfkk1avmmggp949ycbyk2i3211s3nx4sfsbspvlyq")))

(define-public crate-for_ch-0.1.3 (c (n "for_ch") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing"))) (d #t) (k 0)))) (h "14hn79diis3p25zg5nh253vrkxf9k3acfj3wb9w4mw6dd33c16bn")))

