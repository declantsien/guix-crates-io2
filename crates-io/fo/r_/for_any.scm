(define-module (crates-io fo r_ for_any) #:use-module (crates-io))

(define-public crate-for_any-0.0.1-beta.0 (c (n "for_any") (v "0.0.1-beta.0") (d (list (d (n "darling") (r "^0.13.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "18wmayh1h2vmf0mj08r69khg78l8nh3b57f9pm8zpc3z07adpc14")))

