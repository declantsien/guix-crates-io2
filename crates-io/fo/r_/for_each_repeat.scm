(define-module (crates-io fo r_ for_each_repeat) #:use-module (crates-io))

(define-public crate-for_each_repeat-0.1.0 (c (n "for_each_repeat") (v "0.1.0") (h "0gl54zz4kabgahnyjqliilq7w2pgd0cdjgdvjzbvryplmibd68z3") (y #t)))

(define-public crate-for_each_repeat-0.1.1 (c (n "for_each_repeat") (v "0.1.1") (h "0rl72k87mkydvc5h27q8by65cawlbwaxiick6j12zdjq5pw9ifzx") (y #t)))

(define-public crate-for_each_repeat-0.1.2 (c (n "for_each_repeat") (v "0.1.2") (h "1zdg1h0y5zsyqs1basxds2yvvl2wqhwjhv6cafx6sg2r773br3dp") (y #t)))

(define-public crate-for_each_repeat-0.1.3 (c (n "for_each_repeat") (v "0.1.3") (h "085ib4xg4lzgxjqbsgf9v764j1fpv6xkvklg00lavand98v75fhw")))

