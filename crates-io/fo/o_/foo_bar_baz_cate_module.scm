(define-module (crates-io fo o_ foo_bar_baz_cate_module) #:use-module (crates-io))

(define-public crate-foo_bar_baz_cate_module-0.6.0 (c (n "foo_bar_baz_cate_module") (v "0.6.0") (h "1rnlmbhcp29n47rys1lw1s8j0v9p86byvk9nrd0l77mckmqqvr4z")))

(define-public crate-foo_bar_baz_cate_module-0.7.0 (c (n "foo_bar_baz_cate_module") (v "0.7.0") (h "08yq17nv14z0v6kpz8pq0kj9pq4y17zxq79f2mqbn84hh2x9kwj6")))

(define-public crate-foo_bar_baz_cate_module-0.8.0 (c (n "foo_bar_baz_cate_module") (v "0.8.0") (h "0pjimw701jlsd3hjl5cyix17bvs93111h9jyjzys29w3zaw6g1a6")))

(define-public crate-foo_bar_baz_cate_module-0.9.0 (c (n "foo_bar_baz_cate_module") (v "0.9.0") (h "06ml0bzq4f4i4m12znif1nl5jm63ppzqd25kqx3c90a0sp1qxpl0")))

(define-public crate-foo_bar_baz_cate_module-0.10.0 (c (n "foo_bar_baz_cate_module") (v "0.10.0") (h "0x0hbjahhhgwylrld3w86zfa1ixjlnfmrddc5j3w7zdm0nkr5mxx")))

(define-public crate-foo_bar_baz_cate_module-0.11.0 (c (n "foo_bar_baz_cate_module") (v "0.11.0") (h "1pyvjy7y9pl0wqk7idkv09g14g1dgd84lkfkf2bkjj5n1n0j7b4a")))

(define-public crate-foo_bar_baz_cate_module-0.12.0 (c (n "foo_bar_baz_cate_module") (v "0.12.0") (h "0w8az7bv7zhzvr3yjqvbd428yw8jpr08982gsm6fars5iaqj44z5")))

