(define-module (crates-io la ng langcraft_api) #:use-module (crates-io))

(define-public crate-langcraft_api-0.1.0 (c (n "langcraft_api") (v "0.1.0") (h "0h0hh8hwnfcknb807jgp9hdf6gbnkj0kmvdn0zrj2sa09lvypqbs")))

(define-public crate-langcraft_api-0.1.1 (c (n "langcraft_api") (v "0.1.1") (h "0gpg96k5gffw3nmpsqadm5b2g904wmnlwwdzv1z39h46hi11r8d1")))

(define-public crate-langcraft_api-0.1.2 (c (n "langcraft_api") (v "0.1.2") (h "129qjxkdwxy7z87hamwykddv371j2h8dddn7hm7r5w26ivgvsa7m")))

