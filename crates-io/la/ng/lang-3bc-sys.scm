(define-module (crates-io la ng lang-3bc-sys) #:use-module (crates-io))

(define-public crate-lang-3bc-sys-0.1.0 (c (n "lang-3bc-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.72") (d #t) (k 1)))) (h "1zm24wmmjws2pdpaz5qsbjmxl0hrzr31q1saqzcgw598c26qrs38")))

(define-public crate-lang-3bc-sys-0.1.1 (c (n "lang-3bc-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.72") (d #t) (k 1)))) (h "18lflxqbxjk2szy6w7ixgcg5l0cvpgncw5zwddsy2lp27d2s45ws")))

(define-public crate-lang-3bc-sys-0.1.2 (c (n "lang-3bc-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.72") (d #t) (k 1)))) (h "1rn9k58sz8fmzzkln609qi6fh32inw19c1zp2182hyp5cpqdfmhk")))

(define-public crate-lang-3bc-sys-0.1.3-alpha (c (n "lang-3bc-sys") (v "0.1.3-alpha") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.72") (d #t) (k 1)))) (h "14gdwr6i9bmkmb38rgymlqi6f1ci0xg90801gff3w3zz7afd9biv") (y #t)))

(define-public crate-lang-3bc-sys-0.1.3 (c (n "lang-3bc-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "02pknhyq6mg5dji9zzpdxlrn5w4z73gbcqsdaywbq89lv5fqa7fg")))

