(define-module (crates-io la ng langid) #:use-module (crates-io))

(define-public crate-langid-0.0.1 (c (n "langid") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 2)) (d (n "itertools") (r "^0.4.11") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)))) (h "0wh95156j0mvxpcrsgckwav4366rzxygkya6lkj2cjdq0955pa4i")))

