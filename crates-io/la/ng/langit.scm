(define-module (crates-io la ng langit) #:use-module (crates-io))

(define-public crate-langit-0.1.0 (c (n "langit") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "lingua") (r "^1.5") (f (quote ("english" "portuguese" "german" "french"))) (k 0)))) (h "1qxjayw7vmcllybz3xiqzshsanbn27ghy004mjv151wksz1rgdfg")))

(define-public crate-langit-0.1.1 (c (n "langit") (v "0.1.1") (d (list (d (n "lingua") (r "^1.5") (f (quote ("english" "portuguese" "german" "french"))) (k 0)))) (h "0wszv0nprx66nsnf47kya4mqbdmfl4x788pmbma25n3328vdnmz4")))

