(define-module (crates-io la ng langbox_procmacro) #:use-module (crates-io))

(define-public crate-langbox_procmacro-0.1.0 (c (n "langbox_procmacro") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1l9z46ym7z58yb5j33my21wxqlcczy7vsm3l7nqmsf2fb2gja63c")))

(define-public crate-langbox_procmacro-0.1.1 (c (n "langbox_procmacro") (v "0.1.1") (d (list (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gz9b0793d1ryxfshg56sidd8n8pdjsm5bg0xgzvxcl4rx4fq9gl")))

(define-public crate-langbox_procmacro-0.2.0 (c (n "langbox_procmacro") (v "0.2.0") (d (list (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10igzb3hl3q6fs5hkivpk59jwg3v7fb89x3ac7yyfsgnb4rplrb0")))

