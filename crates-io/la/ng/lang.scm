(define-module (crates-io la ng lang) #:use-module (crates-io))

(define-public crate-lang-0.1.1 (c (n "lang") (v "0.1.1") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1n9jhq9mygr48nw9586y4w3w2qbs7bhl9jhspc5wl3hlika8lyhj")))

(define-public crate-lang-0.1.2 (c (n "lang") (v "0.1.2") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0nd4sx09zgapidhcf86w1mhq4a7xwwjcvn7g9qssrp2slwiw93n7")))

(define-public crate-lang-0.1.3 (c (n "lang") (v "0.1.3") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1fsq3k2ym811j3qwxxmf9m1jj27xmzn18m5kkvadi31zi5v40hnw")))

(define-public crate-lang-0.1.4 (c (n "lang") (v "0.1.4") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1b9drlkdza3l30qk96a6b187pi9c01dx9q3z8whkzsn9ri4k8a7p")))

(define-public crate-lang-0.1.5 (c (n "lang") (v "0.1.5") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "104kmrjshii8kihjmy0z37fd015gw760jb1lihxcl74xabz9y69p")))

(define-public crate-lang-0.1.6 (c (n "lang") (v "0.1.6") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0yhix7gv2ps5l7ylphs3b6dvmjrzibndpwm7yxmxb3hm08vmsqqi")))

(define-public crate-lang-0.1.7 (c (n "lang") (v "0.1.7") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1rd0kgnyw4c8gx78h77qigrw3b50ngz4ryx36fk9vrwxb534vi4q")))

(define-public crate-lang-0.1.8 (c (n "lang") (v "0.1.8") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0b5v8rsr2w2bc2i6hsz07g66ld0h52wi57pxgr43jxycmzh48iv7")))

(define-public crate-lang-0.1.9 (c (n "lang") (v "0.1.9") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0djh9rl94k952ndm9gbmdnp5fsbvmg7r2xk3krz2nmzsfgcd1my9")))

(define-public crate-lang-0.1.10 (c (n "lang") (v "0.1.10") (d (list (d (n "aok") (r "^0.1.3") (d #t) (k 2)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1pllmp9frqdyz86sfh955lgydfdsmwm5hjqapm8f1avzv2xcgjls")))

(define-public crate-lang-0.1.11 (c (n "lang") (v "0.1.11") (d (list (d (n "aok") (r "^0.1.3") (d #t) (k 2)) (d (n "int-enum") (r "^1.0.1") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1jsk4hdyvvl4d75ssl63fj83nm7s6na8644fc53759crpvmf207y")))

(define-public crate-lang-0.1.12 (c (n "lang") (v "0.1.12") (d (list (d (n "aok") (r "^0.1.3") (d #t) (k 2)) (d (n "int-enum") (r "^1.0.1") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1zdigq74inx5yniizv6ny06bxk38x9zq3grn5v1fdbv1acxk23n0")))

(define-public crate-lang-0.1.13 (c (n "lang") (v "0.1.13") (d (list (d (n "aok") (r "^0.1.3") (d #t) (k 2)) (d (n "int-enum") (r "^1.1.1") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1phq5jlhh49x6hmv34q210wlwjsq9bxli7l5k67x4wgkr06734nv")))

(define-public crate-lang-0.1.14 (c (n "lang") (v "0.1.14") (d (list (d (n "aok") (r "^0.1.3") (d #t) (k 2)) (d (n "ifs") (r "^0.1.25") (f (quote ("hash"))) (k 0)) (d (n "int-enum") (r "^1.1.1") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "strum") (r "^0.26.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0k26wdpbdl5hll09mpq86743raf0ylkx98i3yws9m5f6jar534nq")))

(define-public crate-lang-0.1.15 (c (n "lang") (v "0.1.15") (d (list (d (n "aok") (r "^0.1.3") (d #t) (k 2)) (d (n "ifs") (r "^0.1.27") (f (quote ("hash"))) (k 0)) (d (n "int-enum") (r "^1.1.1") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "strum") (r "^0.26.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1hhz1v1a72jryqkycr48920wc76pdngm3xb302zmzb5n5s54dj9k")))

(define-public crate-lang-0.1.16 (c (n "lang") (v "0.1.16") (d (list (d (n "aok") (r "^0.1.11") (d #t) (k 2)) (d (n "ifs") (r "^0.1.27") (f (quote ("hash"))) (k 0)) (d (n "int-enum") (r "^1.1.1") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "strum") (r "^0.26.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "1s4r38yn8j59jkjqgxi5w7jw5gcqrd4dsxfls4s4avjxxxyrbbqv")))

(define-public crate-lang-0.1.17 (c (n "lang") (v "0.1.17") (d (list (d (n "aok") (r "^0.1.11") (d #t) (k 2)) (d (n "ifs") (r "^0.1.27") (f (quote ("hash"))) (k 0)) (d (n "int-enum") (r "^1.1.1") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "strum") (r "^0.26.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "093v0s4yc6rrpmaxijvjd7b2b8hy4aka4ajkvcdssk7zaz2ls858")))

(define-public crate-lang-0.1.18 (c (n "lang") (v "0.1.18") (d (list (d (n "aok") (r "^0.1.11") (d #t) (k 2)) (d (n "ifs") (r "^0.1.27") (f (quote ("hash"))) (k 0)) (d (n "int-enum") (r "^1.1.1") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "strum") (r "^0.26.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "0n5701qn9mkq3ypf43z2cmfw6xpcyd9f9j1wi3p7jbna50bn5rj1")))

(define-public crate-lang-0.1.19 (c (n "lang") (v "0.1.19") (d (list (d (n "aok") (r "^0.1.11") (d #t) (k 2)) (d (n "ifs") (r "^0.1.27") (f (quote ("hash"))) (k 0)) (d (n "int-enum") (r "^1.1.1") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "strum") (r "^0.26.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "0f0byf4nd6s4rplvx4rmhc5z54yl0if2ms12bryrrhaiflk5qyrv")))

(define-public crate-lang-0.1.20 (c (n "lang") (v "0.1.20") (d (list (d (n "aok") (r "^0.1.11") (d #t) (k 2)) (d (n "ifs") (r "^0.1.27") (f (quote ("hash"))) (k 0)) (d (n "int-enum") (r "^1.1.1") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "strum") (r "^0.26.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "1xpccm667nrvzbkfa6v7gbymlsl36rn342gv78ckqjmdn9hra0dj")))

(define-public crate-lang-0.1.21 (c (n "lang") (v "0.1.21") (d (list (d (n "aok") (r "^0.1.11") (d #t) (k 2)) (d (n "ifs") (r "^0.1.27") (f (quote ("hash"))) (k 0)) (d (n "int-enum") (r "^1.1.1") (d #t) (k 0)) (d (n "strum") (r "^0.26.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "05zyfp8467s65bcdmvzj0x0zsqmxjr343dzkfm82lb2b47x48v33")))

