(define-module (crates-io la ng langley) #:use-module (crates-io))

(define-public crate-langley-0.0.0 (c (n "langley") (v "0.0.0") (d (list (d (n "abscissa") (r "^0") (d #t) (k 0)))) (h "0cm1r3d56m8y0xm26bdkq41sqvyyl7g4bszcap5andfds18iild1") (y #t)))

(define-public crate-langley-0.0.1 (c (n "langley") (v "0.0.1") (d (list (d (n "abscissa") (r "^0") (d #t) (k 0)))) (h "0mivhypiblwck15n8vaj9vi4v6pdn8bwhf6kpfyjgylfwh7fsqkr") (y #t)))

