(define-module (crates-io la ng langlang_lib) #:use-module (crates-io))

(define-public crate-langlang_lib-0.1.0 (c (n "langlang_lib") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "02bn7y4vc64mhm2za8vfpxjp4hvfmnsdvn7vyg10ki1j55v5v9xx")))

(define-public crate-langlang_lib-0.1.1 (c (n "langlang_lib") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "langlang_syntax") (r "^0.1.1") (d #t) (k 0)) (d (n "langlang_value") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "17y9ayk9z0hg69slqx0l3bk9ns6zma3yd3k8pcc1ad0khnz8dh42")))

(define-public crate-langlang_lib-0.1.2 (c (n "langlang_lib") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "langlang_syntax") (r "^0.1.2") (d #t) (k 0)) (d (n "langlang_value") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1x6h8p5vqfakq8jhfkaxfn94a70hjqwljw700lzx77nylj1vaf16")))

