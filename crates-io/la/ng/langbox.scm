(define-module (crates-io la ng langbox) #:use-module (crates-io))

(define-public crate-langbox-0.1.0 (c (n "langbox") (v "0.1.0") (d (list (d (n "langbox_procmacro") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0hh2nzbha4ayjr2bj3gj7jls6j0b579iaydxsp2hy0q9qkpq62cw")))

(define-public crate-langbox-0.1.1 (c (n "langbox") (v "0.1.1") (d (list (d (n "langbox_procmacro") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1cxgp9zgpjr44jvw2n808bwm6wqzks53l2qk1gvc4xsjr8hbhc92")))

(define-public crate-langbox-0.1.2 (c (n "langbox") (v "0.1.2") (d (list (d (n "langbox_procmacro") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1cbrf4vkp2mg09bpfjs2kd28cpddjz515c78x59aj2zzr4fmaj1x")))

(define-public crate-langbox-0.1.3 (c (n "langbox") (v "0.1.3") (d (list (d (n "langbox_procmacro") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0v8wclcksk264zgdp11d4drvc30r3q818x21b020abjjs39pc77w")))

(define-public crate-langbox-0.2.0 (c (n "langbox") (v "0.2.0") (d (list (d (n "langbox_procmacro") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1whzjyqkxrbva9v4qm58f1al78b96c7za3ls0si80jkh4qg1rc93")))

(define-public crate-langbox-0.3.0 (c (n "langbox") (v "0.3.0") (d (list (d (n "langbox_procmacro") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "1z71zz585dnc4ygwadakjqaafg2qb4g5r5c1m8w8ax0p5kl2m1dw")))

(define-public crate-langbox-0.4.0 (c (n "langbox") (v "0.4.0") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "langbox_procmacro") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0h135wf2sgbm0k6kkjigfgx523m13xw63z6hz9717wjl2sf82g5r")))

(define-public crate-langbox-0.5.0 (c (n "langbox") (v "0.5.0") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "langbox_procmacro") (r "^0.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)))) (h "0zapsy26x8gjxgzp5maz3wdj5lm4664a2wayigdf2pi3hmsnwzpw")))

