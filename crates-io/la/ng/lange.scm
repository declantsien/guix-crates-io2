(define-module (crates-io la ng lange) #:use-module (crates-io))

(define-public crate-lange-0.1.0 (c (n "lange") (v "0.1.0") (d (list (d (n "askama") (r "^0.9.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "0bf0md4zcj74nm6cf7bsy0jgnlbzhvvrkd04492q6qhlg62v6v87")))

