(define-module (crates-io la ng langtag) #:use-module (crates-io))

(define-public crate-langtag-0.1.0 (c (n "langtag") (v "0.1.0") (h "15f5yffz8ax8smx1a0flq1p26bi10npsi1rxck8id375sg52gnz7")))

(define-public crate-langtag-0.1.1 (c (n "langtag") (v "0.1.1") (h "0qkcjhccd46dr162i2zk3rs90f8wz36lwzrdqx1ld8v4icl4bpv6")))

(define-public crate-langtag-0.2.0 (c (n "langtag") (v "0.2.0") (h "0fysjyss52gfjlhwpsq6jgbk5zjd9j1v9724353qf5py2kzp1dfz")))

(define-public crate-langtag-0.3.0 (c (n "langtag") (v "0.3.0") (h "17asc10zq18jjarm6x3s557m23zvh2x0pkw67d07mq4kdbr8046q")))

(define-public crate-langtag-0.3.1 (c (n "langtag") (v "0.3.1") (h "1rycpgw7rlgcsmydqrzzsiaijzqqww1y5lcmwpdyp1h0za4glabl")))

(define-public crate-langtag-0.3.2 (c (n "langtag") (v "0.3.2") (h "00ys72y6p89by8pfri2b35q18hb1m518vckdmifh4vicfk1hi2yp")))

(define-public crate-langtag-0.3.3 (c (n "langtag") (v "0.3.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ahvafi5ifbf4w9l7ckk986460vl5vraxfhdcx9z282ac0ahcxq0")))

(define-public crate-langtag-0.3.4 (c (n "langtag") (v "0.3.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "01gqyq1v8n4s5chgdz7npp8w9yqyjbfyw5gc1i2yhsjd4mgwhq7d")))

(define-public crate-langtag-0.4.0 (c (n "langtag") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "static-regular-grammar") (r "^2.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "00367kzi1s3jxjlj3bg6fb80sc3y0cs7y8qlmaz8xr1hk9l4rjwy")))

