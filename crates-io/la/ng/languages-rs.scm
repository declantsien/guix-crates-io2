(define-module (crates-io la ng languages-rs) #:use-module (crates-io))

(define-public crate-languages-rs-0.1.0 (c (n "languages-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1689y60fn60fld25qgcj4jmjaq312dwyd7yvm32zxvwpv1cq4brk")))

(define-public crate-languages-rs-0.2.0 (c (n "languages-rs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0zbsar7ppjqrfqp3j7ap9ivgivqbpr8gr1n86bzh504z6pgnzxlp") (f (quote (("with-toml" "toml") ("with-json" "serde_json") ("default"))))))

