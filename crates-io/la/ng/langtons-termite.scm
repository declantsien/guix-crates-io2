(define-module (crates-io la ng langtons-termite) #:use-module (crates-io))

(define-public crate-langtons-termite-0.1.0 (c (n "langtons-termite") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "082yjaipx53yzxka261628iw62kv55610jiq1ydl532x8jyr3a5x")))

(define-public crate-langtons-termite-0.2.0 (c (n "langtons-termite") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1safslxr5b958jq09cs5i7ysk8j57m0a2yvh8calgvk3hk1nl0yi")))

(define-public crate-langtons-termite-0.3.0 (c (n "langtons-termite") (v "0.3.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0rzwl1p9p415zpd8xk0hn7mciw03dzawgb8g7x2mbjrimvp7q4dy")))

(define-public crate-langtons-termite-0.4.0 (c (n "langtons-termite") (v "0.4.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1gx9rw8n23y4v6r1bq9chj5sj76lxlijmsmx9ii2vsibw7842lrb")))

(define-public crate-langtons-termite-0.5.0 (c (n "langtons-termite") (v "0.5.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1azbqscgin96f8p5lfnccjhd4q6hlfgi4gjln8bp3c96a4ggdg6h")))

(define-public crate-langtons-termite-1.0.0 (c (n "langtons-termite") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "19hcxqhx83fl0bk5v82x9n9iq32vlz9rnzw6mgwc55w95m72gi5v") (f (quote (("debug_updates"))))))

