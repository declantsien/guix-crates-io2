(define-module (crates-io la ng langmeantree-proc) #:use-module (crates-io))

(define-public crate-langmeantree-proc-0.1.0 (c (n "langmeantree-proc") (v "0.1.0") (d (list (d (n "by_address") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "06x22hp7svm78g2x5dj4hyh17s3jhn433bbw19nmql1x38zxzfcl") (y #t)))

