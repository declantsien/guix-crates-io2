(define-module (crates-io la ng lang-util-derive) #:use-module (crates-io))

(define-public crate-lang-util-derive-0.1.0 (c (n "lang-util-derive") (v "0.1.0") (d (list (d (n "darling") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0q8f6h26mfgr0r8phxqgzkwr2ym9cylqbjgwq8y9zdx673zqclwm")))

(define-public crate-lang-util-derive-0.1.1 (c (n "lang-util-derive") (v "0.1.1") (d (list (d (n "darling") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1gjnzq0bq5836ay1gxn4rjz84vnikbj7n7grpyy57mph5cm8i8y5")))

(define-public crate-lang-util-derive-0.1.2 (c (n "lang-util-derive") (v "0.1.2") (d (list (d (n "darling") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0v8n7rna7kv0824ig6s3p8xhavpa71ds0nqbh51ha54v6ii3v34q")))

(define-public crate-lang-util-derive-0.1.3 (c (n "lang-util-derive") (v "0.1.3") (d (list (d (n "darling") (r "^0.12") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "131xvn4lvhpfpgydg54gy506gsldv50rinb15gkca5zvipv38q9q")))

(define-public crate-lang-util-derive-0.2.0 (c (n "lang-util-derive") (v "0.2.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vq77fh5ii72whc1v83npch719xi39sax5ykc2kl482115k1626d")))

(define-public crate-lang-util-derive-0.2.1 (c (n "lang-util-derive") (v "0.2.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06b7yjyx1yin0cndjycfaava4pi73grzg00hhw4y9jf3ra66mi5i")))

(define-public crate-lang-util-derive-0.3.0 (c (n "lang-util-derive") (v "0.3.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qprz5xh6yrjzz3phcb4jcvrh5y9zpd8kprnd4z6h5s5rxw9rqs8")))

(define-public crate-lang-util-derive-0.3.1 (c (n "lang-util-derive") (v "0.3.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "016zzjq1amayk1di42n4fryg3d7bgbpa2i3q5khbnp2bz1vdwlvz")))

(define-public crate-lang-util-derive-0.4.0 (c (n "lang-util-derive") (v "0.4.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1llhish7jj4pw6l2v40s951wnxwn2wzkaddc5r6q9zz760idfami")))

(define-public crate-lang-util-derive-0.4.1 (c (n "lang-util-derive") (v "0.4.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1n6idd8431cwm11c12h2lmys1xxqn9201pzyb0d0akbmva7qh7hh")))

(define-public crate-lang-util-derive-0.5.0 (c (n "lang-util-derive") (v "0.5.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0pswzpdqs98bb44zw5lf45ab5rjqm4imsm093ilimidk319xggjf")))

(define-public crate-lang-util-derive-0.5.1 (c (n "lang-util-derive") (v "0.5.1") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1y8z9z72ln1kpkpmrq5rkmnf72ks9mrjll7zzfcaxy87mggndf95")))

(define-public crate-lang-util-derive-0.5.2 (c (n "lang-util-derive") (v "0.5.2") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1mj4xa8slpaq98idz00wifhdjmgsk7l6xl9sjdlakn86jvwis23r")))

