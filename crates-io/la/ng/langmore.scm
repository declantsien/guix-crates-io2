(define-module (crates-io la ng langmore) #:use-module (crates-io))

(define-public crate-langmore-0.1.0 (c (n "langmore") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "dotenv_codegen") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 0)))) (h "07kggggrlws9f7hj5xi8bjqlx543kwgq1dx17ndcir7gbfkggdfn")))

(define-public crate-langmore-0.3.0 (c (n "langmore") (v "0.3.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "dotenv_codegen") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1g2hfbqk3ngvnjqykchjvrqms8xj42d285bw0mqblf98rcxgnhf2")))

