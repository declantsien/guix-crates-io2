(define-module (crates-io la ng language-code) #:use-module (crates-io))

(define-public crate-language-code-0.0.0 (c (n "language-code") (v "0.0.0") (h "0znr5r901jwk00rbbxdvh8nn9i9x4ndzy8rd49jnaaawjya8iks6")))

(define-public crate-language-code-0.1.0 (c (n "language-code") (v "0.1.0") (d (list (d (n "country-code") (r "^0.1") (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "paste") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (o #t) (k 0)))) (h "05919z1swdpyj21jsrn5x1wakkq38xw7z30nwd734v0gg21yj8cy") (f (quote (("std" "country-code/std") ("default" "std"))))))

(define-public crate-language-code-0.2.0 (c (n "language-code") (v "0.2.0") (d (list (d (n "country-code") (r "^0.2") (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "impl-macros") (r "^0.1.1") (f (quote ("alloc"))) (k 0)) (d (n "paste") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0faxmaz2qc1jmn3i43hmzzsdazw0fxaxydy4bgyfxrhscyq1c7y0") (f (quote (("std" "country-code/std") ("default" "std"))))))

(define-public crate-language-code-0.3.0 (c (n "language-code") (v "0.3.0") (d (list (d (n "country-code") (r "^0.3") (k 0)) (d (n "impl-macros") (r "^0.1.1") (f (quote ("alloc"))) (k 0)) (d (n "paste") (r "^1") (k 0)) (d (n "serde") (r "^1") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0rbgp5cb2jfm1cykpij7x2hp0vjkgps648iks0mgxzg9im2lcpvi") (f (quote (("std" "country-code/std") ("default" "std"))))))

