(define-module (crates-io la ng langis) #:use-module (crates-io))

(define-public crate-langis-0.1.0 (c (n "langis") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (o #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (k 0)))) (h "14df0cb9135ljclmv9qcxx3qjlkhj8qxd1r5qmyrvm20lgk9qymc") (f (quote (("rand-std" "rand/std") ("num-std" "num-traits/std") ("num-libm" "num-traits/libm") ("default")))) (y #t)))

(define-public crate-langis-0.1.1 (c (n "langis") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (o #t) (k 0)) (d (n "rand") (r "^0.7") (o #t) (k 0)))) (h "1r7s6k7g4yrkvn8r761i2zdfz4aky8q2fjghhwik5ri0lgfr899r") (f (quote (("rand-std" "rand/std") ("num-std" "num-traits/std") ("num-libm" "num-traits/libm") ("default" "rand-std" "num-std"))))))

