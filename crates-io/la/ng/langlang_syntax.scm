(define-module (crates-io la ng langlang_syntax) #:use-module (crates-io))

(define-public crate-langlang_syntax-0.1.1 (c (n "langlang_syntax") (v "0.1.1") (d (list (d (n "langlang_value") (r "^0.1.1") (d #t) (k 0)))) (h "0p771a2bfh6jj5adgm6r377df5jb8z6llwxdwhqxa15cyb0c125g")))

(define-public crate-langlang_syntax-0.1.2 (c (n "langlang_syntax") (v "0.1.2") (d (list (d (n "langlang_value") (r "^0.1.2") (d #t) (k 0)))) (h "0zlwqdl2lwn3jklnlcnipgns1gwm0284v142f23gb669rr6iyykz")))

