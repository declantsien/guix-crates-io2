(define-module (crates-io la ng lang-c) #:use-module (crates-io))

(define-public crate-lang-c-0.1.0 (c (n "lang-c") (v "0.1.0") (h "00yx8k8xq0l67l86plzngnpc022rpryd79p6qw1j9b6c1glrjn6m")))

(define-public crate-lang-c-0.2.0 (c (n "lang-c") (v "0.2.0") (h "0g68shiq6fvsq45lqb550g5jxgi06zi9jgr03c922645rfw4s9h4")))

(define-public crate-lang-c-0.3.0 (c (n "lang-c") (v "0.3.0") (h "1gh75xbns2i6byxrd7was9fvqz87fbdzg42v925c58f7chvqcskz")))

(define-public crate-lang-c-0.4.0 (c (n "lang-c") (v "0.4.0") (h "1bz1k63pgg3ba314q84d075qw06zijbfpl626lxrr3j4scjv38sc")))

(define-public crate-lang-c-0.4.1 (c (n "lang-c") (v "0.4.1") (h "0sdr5krpkljkb2qm59n8zjhyhy0fjgqckr0is0l23plkqw6yd7q6")))

(define-public crate-lang-c-0.5.0 (c (n "lang-c") (v "0.5.0") (h "0fy6ixxqc1aiq2hnsx9f73y61cmknbgc8j610c3jbm99gd9mqw62")))

(define-public crate-lang-c-0.5.1 (c (n "lang-c") (v "0.5.1") (h "0a18s8cbajxjv0ryq1cg87dc3jg5ri6fm9hyh4n1ysjvj2vkzxs9")))

(define-public crate-lang-c-0.5.2 (c (n "lang-c") (v "0.5.2") (h "17n249fbvkin3qzmc9vhn1s99j253cmxx7hlvg7v1j2s19acw0pv")))

(define-public crate-lang-c-0.6.0 (c (n "lang-c") (v "0.6.0") (h "078a6kh566waw8b6h9373yz3rfah80h8nby2hmv94dxy5m2by5bf")))

(define-public crate-lang-c-0.7.0 (c (n "lang-c") (v "0.7.0") (h "03ycgk38bvi0zh08hh7rmc1v34hi28galsjf54927qp7057f1n23")))

(define-public crate-lang-c-0.8.0 (c (n "lang-c") (v "0.8.0") (h "0mbda3kc5330yy3ch15jy43k6il1giysgw9gxdapch6pslhc9vw6")))

(define-public crate-lang-c-0.8.1 (c (n "lang-c") (v "0.8.1") (h "1v78a3zqny3jbb244rxk5amln52ckirfcgv0cfnxiqr4lfkv6m15")))

(define-public crate-lang-c-0.9.0 (c (n "lang-c") (v "0.9.0") (h "0w5zs7md2c4vsks1cfzkknh6hhyhnfns5zykjnjv731rkb5v2kaw")))

(define-public crate-lang-c-0.10.0 (c (n "lang-c") (v "0.10.0") (h "06q0hywgcrli7aclyiwzq60ixdnlls18w3f8sfshxnchavrffkzy") (f (quote (("dev-pegviz"))))))

(define-public crate-lang-c-0.10.1 (c (n "lang-c") (v "0.10.1") (h "01q2hkisslqmghjsj2qsias68f0iwcl009v5av4zxbzrnqyk95x5") (f (quote (("dev-pegviz"))))))

(define-public crate-lang-c-0.11.0 (c (n "lang-c") (v "0.11.0") (h "0ak9yklj5xkdzylnnp3b12chhsniwz3ppf6x91iqnnldk854h0m4") (f (quote (("dev-pegviz"))))))

(define-public crate-lang-c-0.12.0 (c (n "lang-c") (v "0.12.0") (h "1g2jy3f2k6krk2jhwdfafj5ykhvx0yjw6qic42mh3ffqixb5593b") (f (quote (("dev-pegviz")))) (y #t)))

(define-public crate-lang-c-0.13.0 (c (n "lang-c") (v "0.13.0") (h "03acycps9mmgr2as6jvwj1cd0v31xfkm9k2rjdkhxlj12ssvqrhx") (f (quote (("dev-pegviz"))))))

(define-public crate-lang-c-0.14.0 (c (n "lang-c") (v "0.14.0") (h "1xk2xm7zy7fn7gwi48cy9y1jzzhra70dk15l3c6i8sdskaypm818") (f (quote (("dev-pegviz"))))))

(define-public crate-lang-c-0.15.0 (c (n "lang-c") (v "0.15.0") (h "1rvljp1lp2v239ybabxcr0w1p6inl12z6a04w942hmsmpx44yk45") (f (quote (("dev-pegviz"))))))

(define-public crate-lang-c-0.15.1 (c (n "lang-c") (v "0.15.1") (h "166riqy0pf21l8r2rr90b9sbwwrhxiwiirdjis1zdlcmny9683kj") (f (quote (("dev-pegviz"))))))

