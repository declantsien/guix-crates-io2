(define-module (crates-io la ng lang-util-dev) #:use-module (crates-io))

(define-public crate-lang-util-dev-0.2.0 (c (n "lang-util-dev") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.1.0") (d #t) (k 0)))) (h "11lbkpa2s33azfnddbxz3p8vwr3x5kg5sds7nibx1ag84an269s1")))

(define-public crate-lang-util-dev-0.2.1 (c (n "lang-util-dev") (v "0.2.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.1.0") (d #t) (k 0)))) (h "1hwsnkkli7r5w97m32pyj2m61cpxk36slv93l502rczfn2qi6nbk")))

(define-public crate-lang-util-dev-0.3.0 (c (n "lang-util-dev") (v "0.3.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.2") (d #t) (k 0)))) (h "1yf6pqyrr6fzca93b9ja1hn74ps3d4m4qjznrdk57gj69ijwy7bz")))

(define-public crate-lang-util-dev-0.3.1 (c (n "lang-util-dev") (v "0.3.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.2") (d #t) (k 0)))) (h "0j6hvadi5y6pz2a04kqdapcda13q9ap4r3bvd1snbq2vd6j8gl3f")))

(define-public crate-lang-util-dev-0.4.0 (c (n "lang-util-dev") (v "0.4.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.4") (d #t) (k 0)))) (h "05lcgpd5rkfylxx1ghgh3v6byz9q6spab9yrjfh7clhsb3vs2kpn")))

(define-public crate-lang-util-dev-0.4.1 (c (n "lang-util-dev") (v "0.4.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.4") (d #t) (k 0)))) (h "0hls18acz8idgh90vh6pg79wbzd6fbrhjmzi56y5iwpi054xkzk8")))

(define-public crate-lang-util-dev-0.5.0 (c (n "lang-util-dev") (v "0.5.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.4") (d #t) (k 0)))) (h "0frr210sx10f3aplmy9yix5mm54mh4w1jiznb3byylkdr32v7qkr")))

(define-public crate-lang-util-dev-0.5.1 (c (n "lang-util-dev") (v "0.5.1") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.4") (d #t) (k 0)))) (h "0n61ka22nk8igw188glg7231sqzvfr7v9spwl554vwp3lc8hznnj")))

(define-public crate-lang-util-dev-0.5.2 (c (n "lang-util-dev") (v "0.5.2") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "similar-asserts") (r "^1.4") (d #t) (k 0)))) (h "115wyqvb423h791gsbnwa6dy6ad83ljwsz09h2fpj3np1hh3yjzw")))

