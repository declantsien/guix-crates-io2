(define-module (crates-io la ng langen_macro) #:use-module (crates-io))

(define-public crate-langen_macro-1.0.0 (c (n "langen_macro") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6.28") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full"))) (d #t) (k 0)))) (h "148zfv89k6hwz5gm9pnz9bv9zc99ajblbqlj7zyz1rdri0gly6sb")))

