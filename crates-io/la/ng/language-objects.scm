(define-module (crates-io la ng language-objects) #:use-module (crates-io))

(define-public crate-language-objects-1.0.0 (c (n "language-objects") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lsnlrwgsr9g8d1zgfangp3c9yq5wjc14a7ydmylbw3an8qmvar0")))

(define-public crate-language-objects-1.0.1 (c (n "language-objects") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1wnsvyaa2ymzipipc9jpfxfmsghdymfx46w3kwz0q2a0fph9n2v6")))

(define-public crate-language-objects-1.0.2 (c (n "language-objects") (v "1.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vfq3z4d6k59jmzspyy84jhdd9zl1x6lp5k427w9kmdhkgx3j9pf")))

