(define-module (crates-io la ng langtime) #:use-module (crates-io))

(define-public crate-langtime-0.1.0 (c (n "langtime") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0q1672yhk43wry5s1643dvn9ls8fq41gl71sngigdzpg5ysfn4n6")))

(define-public crate-langtime-0.1.1 (c (n "langtime") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "14nbs83sq263zx8dz50c6w5a8aa7vcl5dc3df0i6bbngjsv64f09")))

(define-public crate-langtime-0.1.2 (c (n "langtime") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "0fz16ch55hiwy9mgr3kirmfglax3igvg7wgp9dq20rc44ay8plsb")))

(define-public crate-langtime-0.1.3 (c (n "langtime") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)))) (h "09vvg1m7l5krwd2x3fkg2w2kd8m60axhlxwiv7yfzaa1x7fflixq")))

