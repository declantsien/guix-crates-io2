(define-module (crates-io la ng langlang) #:use-module (crates-io))

(define-public crate-langlang-0.1.0 (c (n "langlang") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "langlang_lib") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0r8l0wnk6sp4s6g96yvlaj3j2b0jymqjaqgvk66a5774p1mx9j63")))

(define-public crate-langlang-0.1.1 (c (n "langlang") (v "0.1.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "langlang_lib") (r "^0.1.1") (d #t) (k 0)) (d (n "langlang_value") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "07x5bzghsalzcwc32xsbwnk6z1s1cr5nyj06msv2ms29dgrh7vsm")))

(define-public crate-langlang-0.1.2 (c (n "langlang") (v "0.1.2") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "langlang_lib") (r "^0.1.2") (d #t) (k 0)) (d (n "langlang_value") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0p5k7nlnh5amvf51vwjb4imz0vp637nhpc14mk5p62nf7pm9q9pf")))

