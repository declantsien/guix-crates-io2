(define-module (crates-io la ng languagetool) #:use-module (crates-io))

(define-public crate-languagetool-0.1.0 (c (n "languagetool") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "03jpbnxvrladzl24dc3ni4pq0kr02zmjaxdybnigkx6w4w6gk4r0")))

(define-public crate-languagetool-0.1.1 (c (n "languagetool") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1jwam901b01d2gkd7i3c5jqc5w3zd0hs196dx7gpnn4bwhbgyi1r")))

(define-public crate-languagetool-0.1.2 (c (n "languagetool") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ml0vy34q8spdp8hd7vjcws7qa2wy8x2vpbbwa0xhd9vkkj56kn3")))

