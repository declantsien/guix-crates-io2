(define-module (crates-io la ng lang_pt) #:use-module (crates-io))

(define-public crate-lang_pt-0.1.0 (c (n "lang_pt") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "ptree") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)))) (h "0jqicp8gkbmizgr2vpw6spx4z9bgdfazavk4qy82ssnlyax9d87c")))

(define-public crate-lang_pt-0.1.1 (c (n "lang_pt") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "ptree") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)))) (h "0rlcilqz954nh4xa3hmavjlsfqqpamppgkjs9cj81k2nwf7lx1np")))

(define-public crate-lang_pt-0.1.2 (c (n "lang_pt") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "ptree") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 2)))) (h "01sysgxc3gzq8kag62aifa0wnaaby2grwwvg41r6gs961gw38ssj")))

