(define-module (crates-io la ps laps_regex) #:use-module (crates-io))

(define-public crate-laps_regex-0.0.1 (c (n "laps_regex") (v "0.0.1") (d (list (d (n "regex-syntax") (r "^0.7.2") (d #t) (k 0)))) (h "0d6iyd84fbdx687x2hq314a1ma2wqk5nfvbx50lkkf4ls8fpzm48")))

(define-public crate-laps_regex-0.0.2 (c (n "laps_regex") (v "0.0.2") (d (list (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.7.2") (d #t) (k 0)))) (h "0w9fj082zx8nq9mz187snn9p6xq05qbi7q9mzprq7v1p76chc4zh")))

(define-public crate-laps_regex-0.1.0 (c (n "laps_regex") (v "0.1.0") (d (list (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.7.2") (d #t) (k 0)))) (h "1zq7jpld7c7448p1dwz1dgk8bhc7cyyq7lgnjy0y7ygjcgn35qvj")))

(define-public crate-laps_regex-0.1.1 (c (n "laps_regex") (v "0.1.1") (d (list (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.7.2") (d #t) (k 0)))) (h "152q378aj5d3i12krx203d8z7ay3v4i2q1q1mnbn6qqxrp78kh9s")))

