(define-module (crates-io la ps laps) #:use-module (crates-io))

(define-public crate-laps-0.0.1 (c (n "laps") (v "0.0.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "laps_macros") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.4") (d #t) (k 0)))) (h "1h3b1ipdqk3q3hj0yxfxsj51ixprxaj0ybxfw34vpynp2qs39gz0") (f (quote (("no-logger")))) (s 2) (e (quote (("macros" "dep:laps_macros"))))))

(define-public crate-laps-0.0.2 (c (n "laps") (v "0.0.2") (d (list (d (n "colored") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "laps_macros") (r "^0.0.2") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.4") (d #t) (k 0)))) (h "10ih91qsf37fnfipqcchh8mw6bb3a1hd01g4vjbdq3mkdyfwq2vm") (f (quote (("default" "logger")))) (s 2) (e (quote (("macros" "dep:laps_macros") ("logger" "dep:colored"))))))

(define-public crate-laps-0.1.0 (c (n "laps") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "laps_macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0xzqcbxh17918hhvlkydppq4nv0dv8xd5svlnaszksq4rhcw2d68") (f (quote (("default" "logger")))) (s 2) (e (quote (("macros" "dep:laps_macros") ("logger" "dep:colored"))))))

(define-public crate-laps-0.1.1 (c (n "laps") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "laps_macros") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "12yi0g0kngx6ddkf3hzrvd053nwjdca2l4sf9salzz1jnxx5bd9d") (f (quote (("default" "logger")))) (s 2) (e (quote (("macros" "dep:laps_macros") ("logger" "dep:colored"))))))

(define-public crate-laps-0.1.2 (c (n "laps") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "laps_macros") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0jyknjl2yqqfzan07svcri5fmq5n45cvajc0b9nhfx1wgc7w64b6") (f (quote (("default" "logger")))) (s 2) (e (quote (("macros" "dep:laps_macros") ("logger" "dep:colored"))))))

(define-public crate-laps-0.1.3 (c (n "laps") (v "0.1.3") (d (list (d (n "colored") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "laps_macros") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0jd0nsz8byigs62zmqqzmls2hcxfswwa5fzr239payx1d58n47pc") (f (quote (("default" "logger")))) (s 2) (e (quote (("macros" "dep:laps_macros") ("logger" "dep:colored"))))))

(define-public crate-laps-0.1.4 (c (n "laps") (v "0.1.4") (d (list (d (n "colored") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "laps_macros") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0iwnm4jkkj7q7px1sx925fnszpplcf0v5ha92k76abkvjqwz0h68") (f (quote (("default" "logger")))) (s 2) (e (quote (("macros" "dep:laps_macros") ("logger" "dep:colored"))))))

(define-public crate-laps-0.1.5 (c (n "laps") (v "0.1.5") (d (list (d (n "colored") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "laps_macros") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1srqy5pi5bq5gkgbkml3wfmr8aj8327yl85xccn1wh2zjbkx4m72") (f (quote (("default" "logger")))) (s 2) (e (quote (("macros" "dep:laps_macros") ("logger" "dep:colored"))))))

(define-public crate-laps-0.1.6 (c (n "laps") (v "0.1.6") (d (list (d (n "colored") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "laps_macros") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "00kznw66b3f9fpawm2m3akfl6g2675wasyv836sdrp19a70iyzdg") (f (quote (("default" "logger")))) (s 2) (e (quote (("macros" "dep:laps_macros") ("logger" "dep:colored"))))))

(define-public crate-laps-0.1.7 (c (n "laps") (v "0.1.7") (d (list (d (n "colored") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "laps_macros") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0vksmv1vc7j0xi3w3gnw1xlcd7gg73mcfxqp1hixm0h57g20pdji") (f (quote (("default" "logger")))) (s 2) (e (quote (("macros" "dep:laps_macros") ("logger" "dep:colored"))))))

