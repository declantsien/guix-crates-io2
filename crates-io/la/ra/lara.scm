(define-module (crates-io la ra lara) #:use-module (crates-io))

(define-public crate-lara-0.1.0 (c (n "lara") (v "0.1.0") (d (list (d (n "blas-src") (r "^0.2.1") (f (quote ("intel-mkl"))) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "csv") (r "^1.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "multimap") (r "^0.4.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (f (quote ("blas"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.10") (f (quote ("intel-mkl"))) (d #t) (k 0)) (d (n "ndarray-parallel") (r "^0.9") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "10bj64rw5bqvzj2yy50d4h4kwvlmlk90i28pskjq2kmidcbq7d63")))

