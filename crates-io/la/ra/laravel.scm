(define-module (crates-io la ra laravel) #:use-module (crates-io))

(define-public crate-laravel-0.1.0 (c (n "laravel") (v "0.1.0") (h "1q37fny9pcb4r25qkkmycab81n14k7mzpa45kn0p9k7zhbqngvmd")))

(define-public crate-laravel-0.1.1 (c (n "laravel") (v "0.1.1") (d (list (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0s8wk6cl9v1ymd1mdsfn04lwzikzf4g6dm6jjh53aqzg45dgkdaa")))

(define-public crate-laravel-0.1.2 (c (n "laravel") (v "0.1.2") (d (list (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0fwgib4i330n0dmpzjj6winblyx7997lnjrpv43qwhwni5x3gfj8")))

(define-public crate-laravel-0.1.3 (c (n "laravel") (v "0.1.3") (d (list (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "0jm10x45fasxj562699k5s68s0mb224xpszbfmkwkqg6h8vk478w")))

(define-public crate-laravel-0.1.4 (c (n "laravel") (v "0.1.4") (h "1abgcmj1bkaqixvlkibhrx02b7ifplxgkia3ymf3jq4qdhs8lfps")))

