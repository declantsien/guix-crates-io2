(define-module (crates-io la tl latlon) #:use-module (crates-io))

(define-public crate-latlon-0.1.0 (c (n "latlon") (v "0.1.0") (d (list (d (n "geo-types") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)))) (h "1lxfv4ihz6rl8pj5r8gp9cvr7k9h8hfxznsgi06q3h2g5ni6mssn")))

(define-public crate-latlon-0.1.1 (c (n "latlon") (v "0.1.1") (d (list (d (n "geo-types") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)))) (h "0aaclgvhawn2rj29w016j7dvwklb93a2c61dk9hq30lvbyzx813z")))

(define-public crate-latlon-0.1.2 (c (n "latlon") (v "0.1.2") (d (list (d (n "geo-types") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)))) (h "060jvmz9lzv083f86h1xc780dmjvkqmjcps2pqbjnp9sa3z0xs89")))

(define-public crate-latlon-0.1.3 (c (n "latlon") (v "0.1.3") (d (list (d (n "geo-types") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1w7v2p2jivjf1j7y568hfkpbfkjhl5w5hczsb2zbys1z0h90wayl")))

