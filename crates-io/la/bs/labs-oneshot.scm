(define-module (crates-io la bs labs-oneshot) #:use-module (crates-io))

(define-public crate-labs-oneshot-0.1.1 (c (n "labs-oneshot") (v "0.1.1") (h "1yv0lizgqwi9anykd357q6i8hm69lrk03lhzsc7mdi4hqxg1q57i") (y #t)))

(define-public crate-labs-oneshot-0.1.2 (c (n "labs-oneshot") (v "0.1.2") (h "0yzycrcq9yvabzklpysskks0n6qms1my28wj6kxv4r5i7x9dwy6w") (y #t)))

(define-public crate-labs-oneshot-0.1.3 (c (n "labs-oneshot") (v "0.1.3") (h "13879cj0b88zhpj17kvbdrcda3scccz8i7v961cxawxp8mnsqhsc") (y #t)))

(define-public crate-labs-oneshot-0.1.4 (c (n "labs-oneshot") (v "0.1.4") (h "0wcf45gnjva6sqklagxhbk6xljhzmf6vvx1shqg1iqbzcn4qc2fv") (y #t)))

(define-public crate-labs-oneshot-0.1.5 (c (n "labs-oneshot") (v "0.1.5") (h "1i7yc35qdbzsc1kbjhm3dg0yg7yzvq1ahlbvm6yvir906qnh58pj") (y #t)))

