(define-module (crates-io la nc lancat) #:use-module (crates-io))

(define-public crate-lancat-0.1.0 (c (n "lancat") (v "0.1.0") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "whoami") (r "^0.5") (d #t) (k 0)))) (h "1j9902glj86pazm7xxas8pk0y6h4c6ibwwhv8z78mmzb1n9dqkw8")))

(define-public crate-lancat-0.2.0 (c (n "lancat") (v "0.2.0") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "whoami") (r "^0.5") (d #t) (k 0)))) (h "1jkjqk57h3swjg39jck6h5mn8dbz5jmav94srz30abylyycliv6v")))

(define-public crate-lancat-0.3.0 (c (n "lancat") (v "0.3.0") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "whoami") (r "^0.5") (d #t) (k 0)))) (h "0bv7jwdpmncyp5mg0bvz0i4ncz3a5zl8rvq0viy6w448gp6nm0pp")))

