(define-module (crates-io la sr lasrs) #:use-module (crates-io))

(define-public crate-lasrs-0.1.0 (c (n "lasrs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)))) (h "0lgfjb3spypdp5fmvx5hv8zylvxs6ycvcrplmc1brq48wnv57ka7")))

(define-public crate-lasrs-0.1.1 (c (n "lasrs") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)))) (h "161bksb8pz1v92aky47zqgvglwb8bpi1im574411vj9piq0kvfw6")))

(define-public crate-lasrs-0.1.2 (c (n "lasrs") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)))) (h "1a1m54l7r8hgdm57k1przj3zz2ips83ym60px1pn4vhzd9cim4q5")))

(define-public crate-lasrs-0.1.3 (c (n "lasrs") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)))) (h "10dcyxck1h59m4qdd7a5pvww095835csqfy1m0lp6jk5mn5rgl4z")))

