(define-module (crates-io la tk latke) #:use-module (crates-io))

(define-public crate-latke-0.0.0 (c (n "latke") (v "0.0.0") (d (list (d (n "ahash") (r "^0.7.6") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dashmap") (r "^5.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "sha2") (r "^0.10.0") (d #t) (k 0)) (d (n "topograph") (r "^0.2.0-alpha.1") (d #t) (k 0)))) (h "0qdll380wf3p0cfnf1pf6h66vn39q2g5ybhndxv0a6i1nh73jvpx")))

