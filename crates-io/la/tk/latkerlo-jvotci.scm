(define-module (crates-io la tk latkerlo-jvotci) #:use-module (crates-io))

(define-public crate-latkerlo-jvotci-1.0.0 (c (n "latkerlo-jvotci") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0xa76zab692gn8dpqnaq1bk5qp9hhn270sl80iywkm25sjsdkmqg")))

(define-public crate-latkerlo-jvotci-1.0.1 (c (n "latkerlo-jvotci") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0lly1l59j09qn6ng2y5kzjcx33148s0w0zlivkm3v8ifhpcz4f7s")))

(define-public crate-latkerlo-jvotci-1.0.2 (c (n "latkerlo-jvotci") (v "1.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "00w21lwim8wp6wsqb4xcb1gq3qhcpc0drhlxkw1cpqhnkz11hs0y")))

(define-public crate-latkerlo-jvotci-1.0.3 (c (n "latkerlo-jvotci") (v "1.0.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0yjzw7rsz78340w9nkibl787agx4v7jqlgysf634zn3rq2d6kran")))

(define-public crate-latkerlo-jvotci-1.0.4 (c (n "latkerlo-jvotci") (v "1.0.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0ybxzyk252k9n7hp33bg7schs1gq7sp873kghk7qv876dy9n8fra")))

(define-public crate-latkerlo-jvotci-1.0.5 (c (n "latkerlo-jvotci") (v "1.0.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1xyb6v180s8g11yv0dz9f0m0b61sbrcla4p1gfd1zrdngwx6bgah")))

(define-public crate-latkerlo-jvotci-1.0.6 (c (n "latkerlo-jvotci") (v "1.0.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "0h9d1qkgv7xsjdbf9w5x17y3rza5pvl7lqdkicp16kg2qvfikf3z")))

(define-public crate-latkerlo-jvotci-1.0.7 (c (n "latkerlo-jvotci") (v "1.0.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "19yi69czqigdcbzxqczw3308hrhky1py3dl48mh9bghgpz274cah")))

