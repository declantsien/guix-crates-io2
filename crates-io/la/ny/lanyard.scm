(define-module (crates-io la ny lanyard) #:use-module (crates-io))

(define-public crate-lanyard-0.1.0 (c (n "lanyard") (v "0.1.0") (h "1b0902hsg6r71bl8bvjvis1r98m1xibd2ihyjvafq8mzvcrg9r7f") (f (quote (("std" "alloc") ("default" "alloc") ("alloc"))))))

(define-public crate-lanyard-0.1.1 (c (n "lanyard") (v "0.1.1") (h "1ff9m6pm2gfmwxxnpnzlg8db6knbabvf41nb6dzqf2bv6nhncgrh") (f (quote (("std" "alloc") ("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-lanyard-0.1.2 (c (n "lanyard") (v "0.1.2") (h "0rrp7cc2n8bcyad4r3wl7djfs7hdish8svwvsdr2dw1gqam5ma7a") (f (quote (("std" "alloc") ("default" "alloc") ("alloc"))))))

