(define-module (crates-io la fo lafont) #:use-module (crates-io))

(define-public crate-lafont-0.0.1 (c (n "lafont") (v "0.0.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "kiss3d") (r "^0.12") (d #t) (k 0)) (d (n "nalgebra") (r "^0.13") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1zw24y5qclvvw9i1cg76kp0zzmyjnzb71y8awcbgvyq4x2p7lmmc")))

