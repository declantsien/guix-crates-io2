(define-module (crates-io la sz laszip-sys) #:use-module (crates-io))

(define-public crate-laszip-sys-0.1.0 (c (n "laszip-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.30") (d #t) (k 1)) (d (n "tempfile") (r "^2.2") (d #t) (k 2)))) (h "12d4c7mn4lwy90jx8jhw885arh23zf7skkpmc427v5br2xsgg4y3")))

(define-public crate-laszip-sys-0.1.1 (c (n "laszip-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0rkh619s0ihwb1c6arzcx6axmpfwqbcvaqhz915ymv0jyvii56ah") (l "laszip")))

(define-public crate-laszip-sys-0.1.2 (c (n "laszip-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "17113y14pf21h0aipjkknh1mmxk9kmxdbpbyfpk7w0wfyj5p72si") (l "laszip")))

