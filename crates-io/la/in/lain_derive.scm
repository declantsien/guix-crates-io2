(define-module (crates-io la in lain_derive) #:use-module (crates-io))

(define-public crate-lain_derive-0.1.0 (c (n "lain_derive") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1yxnjs63fhqa7avvnxq32jyf83zn2gw6g4wd6v49kdvkyi4pw4b0")))

(define-public crate-lain_derive-0.1.1 (c (n "lain_derive") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0cz33135ph51bcd3k3ikjriqzi48fn0mlb925lsqgj0gmdvbrsim")))

(define-public crate-lain_derive-0.1.2 (c (n "lain_derive") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1xy57hiz890kqjx6psj91cx8blf046mwayymdrc5967qijxjkx2l")))

(define-public crate-lain_derive-0.2.0 (c (n "lain_derive") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "08gdxms1ykcf4l9cdz7371m2kr8jdw0sqmkh1l2fpn93wpm4z6z0")))

(define-public crate-lain_derive-0.2.1 (c (n "lain_derive") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1rap41g1mdkqfmpiw1w444512hv5cxnhk87kxq7qgpha8w0j92a5")))

(define-public crate-lain_derive-0.2.2 (c (n "lain_derive") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "16lilzr72a0zg2v1s2gv3qf0w62lg9k246340h5234nbizfz8yss")))

(define-public crate-lain_derive-0.2.3 (c (n "lain_derive") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0lkp0sxnyngil67db5avlr978p972f9l4d2qps4wcagf1g0jd63n")))

(define-public crate-lain_derive-0.2.4 (c (n "lain_derive") (v "0.2.4") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0wyspy4mnd5d52mfdw9l711qxa0vmy2v9kfkc53gak3kzml6sjmg")))

(define-public crate-lain_derive-0.2.5 (c (n "lain_derive") (v "0.2.5") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1gysrjcjmgr74d02d0ivqm6hsk5wiyf8cv0hxfq2f7scxamm5c04")))

(define-public crate-lain_derive-0.3.0 (c (n "lain_derive") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0vzd73ww03vxffjzv9jxm6g1k8ikncrxzxkxs1mgfxpm8flr4b2d")))

(define-public crate-lain_derive-0.4.0 (c (n "lain_derive") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1n0qza96gfgpl8r20c69zgqbyli9icqgivai7h45lvycynvnvvgj")))

(define-public crate-lain_derive-0.4.1 (c (n "lain_derive") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0ca2jgp45fr596bq3sqppbjdkq52k879i9l21wiaqgk9jqqk3sdf")))

(define-public crate-lain_derive-0.5.0 (c (n "lain_derive") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1lvnhi3vs9d93pzgnc4b7hryp1bzj1vl3qlb14kfjx1g9z2943j8")))

(define-public crate-lain_derive-0.5.1 (c (n "lain_derive") (v "0.5.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0jisaw9mwcsfdij7j0aqf9gkscc5srlfrqnrqi29cjvij9pbv0sg")))

(define-public crate-lain_derive-0.5.2 (c (n "lain_derive") (v "0.5.2") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0n49bgqlgxvp4wl1zl7yi0ll5p9rzgdw7py3blnlsjnyx4gj7p72")))

(define-public crate-lain_derive-0.5.3 (c (n "lain_derive") (v "0.5.3") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0xsc8hz17j21568mr13mkaml40hyghw2xv0a3icrsc1zihgrad3n")))

(define-public crate-lain_derive-0.5.4 (c (n "lain_derive") (v "0.5.4") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0j7gja4lzbkxayipkqkwapl4pbsjzxs083rj5dvvyfa5lp3ryy15")))

(define-public crate-lain_derive-0.5.5 (c (n "lain_derive") (v "0.5.5") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0ijp4jnja7ic13rzia73wi26ap3v72ncf68jiimhcvn6accx7div")))

