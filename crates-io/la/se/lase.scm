(define-module (crates-io la se lase) #:use-module (crates-io))

(define-public crate-lase-0.0.1 (c (n "lase") (v "0.0.1") (d (list (d (n "etherdream") (r "^0.0.7") (d #t) (k 0)))) (h "0r77i044xmaja3w697avx44mg4v6li8jabv2jxlyl6wq2vl47h6r")))

(define-public crate-lase-0.0.2 (c (n "lase") (v "0.0.2") (d (list (d (n "etherdream") (r "0.1.*") (d #t) (k 0)) (d (n "point") (r "0.3.*") (d #t) (k 0)))) (h "0kb8kix8i9zb75z8hka5dw04z68s1a2ym84xjbvp5hjgrjna0c40")))

