(define-module (crates-io la se laser) #:use-module (crates-io))

(define-public crate-laser-0.0.1 (c (n "laser") (v "0.0.1") (d (list (d (n "etherdream") (r "0.0.*") (d #t) (k 0)) (d (n "ilda") (r "0.0.*") (d #t) (k 0)))) (h "0y6qcsq1zkfhdwcqgbil6nn14090r81fd1lz9831ycfsvy7sadgf")))

(define-public crate-laser-0.0.2 (c (n "laser") (v "0.0.2") (d (list (d (n "camera_capture") (r "^0.3") (d #t) (k 0)) (d (n "image") (r "^0.7") (d #t) (k 0)) (d (n "lase") (r "^0.0.2") (d #t) (k 0)))) (h "1brvhv107m07203agwx7rrinr3xy4yjx3z11yay4rm76hablmqjp")))

