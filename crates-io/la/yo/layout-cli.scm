(define-module (crates-io la yo layout-cli) #:use-module (crates-io))

(define-public crate-layout-cli-0.1.0 (c (n "layout-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.18") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "layout-rs") (r "^0.1.0") (f (quote ("log"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1dlz898z84c4cbs2lj42bqaxb7yx6jr788lvrm5z3l9l9c4kdibs")))

(define-public crate-layout-cli-0.1.1 (c (n "layout-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.18") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "layout-rs") (r "^0.1.1") (f (quote ("log"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "13yhcsymd8g2lrkg1q67kbkw7dq0b8w9drzqnrnj6dpg778zx7wq")))

(define-public crate-layout-cli-0.1.2 (c (n "layout-cli") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.18") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "layout-rs") (r "^0.1.2") (f (quote ("log"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "07cclxgixjpq8qnlr16vfg1gzajp8ax0345sbcpw210pvlma76qz")))

