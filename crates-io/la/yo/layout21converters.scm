(define-module (crates-io la yo layout21converters) #:use-module (crates-io))

(define-public crate-layout21converters-3.0.0-pre.2 (c (n "layout21converters") (v "3.0.0-pre.2") (d (list (d (n "chrono") (r "^0.4.20") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gds21") (r "^3.0.0-pre.2") (d #t) (k 0)) (d (n "layout21protos") (r "^3.0.0-pre.2") (d #t) (k 0)) (d (n "layout21raw") (r "^3.0.0-pre.2") (d #t) (k 0)) (d (n "layout21utils") (r "^3.0.0-pre.2") (d #t) (k 0)) (d (n "lef21") (r "^3.0.0-pre.2") (d #t) (k 0)))) (h "1a7nfjk70jjcy0ikv30x8h0l0bhpb5jnsxd5gy6dnjpwlws59xqb")))

