(define-module (crates-io la yo layout2d) #:use-module (crates-io))

(define-public crate-layout2d-0.1.0 (c (n "layout2d") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "simd") (r "^0.2.0") (d #t) (k 0)))) (h "0r7zdkwm2nnjs62mm9z8maxznhqzjgb334ack9y980p2swixsnx9")))

(define-public crate-layout2d-0.1.1 (c (n "layout2d") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "simd") (r "^0.2.0") (d #t) (k 0)))) (h "0bp6sivpqzhd7qy57zndxyd3dxpl05n9kwia0px0q5x9hashy698")))

(define-public crate-layout2d-0.1.2 (c (n "layout2d") (v "0.1.2") (d (list (d (n "rand") (r "0.3.*") (d #t) (k 2)) (d (n "simd") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1sibh4sgn6df542brmdddhf96yfdczbncqsy7ykyajkfwri699km") (f (quote (("use_simd"))))))

