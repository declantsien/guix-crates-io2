(define-module (crates-io la yo layout-rs) #:use-module (crates-io))

(define-public crate-layout-rs-0.1.0 (c (n "layout-rs") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)))) (h "0ri5gk6di0alk6j6ipdqnk82lq0ibnp0y4y7mflfcfikdmmdknch")))

(define-public crate-layout-rs-0.1.1 (c (n "layout-rs") (v "0.1.1") (d (list (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)))) (h "1q44jn18vlsxh2x9ny71zxd8k4pwy1wwlvi1hzcc41wnrf3yyr0i")))

(define-public crate-layout-rs-0.1.2 (c (n "layout-rs") (v "0.1.2") (d (list (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)))) (h "1a6y7wjc0p7cbawdwa9gkvb1c518697nchbk5aj9r0vc7a5b5pl4") (s 2) (e (quote (("log" "dep:log"))))))

