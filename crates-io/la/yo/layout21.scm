(define-module (crates-io la yo layout21) #:use-module (crates-io))

(define-public crate-layout21-3.0.0-pre.2 (c (n "layout21") (v "3.0.0-pre.2") (d (list (d (n "gds21") (r "^3.0.0-pre.2") (f (quote ("selftest"))) (d #t) (k 0)) (d (n "layout21converters") (r "^3.0.0-pre.2") (d #t) (k 0)) (d (n "layout21protos") (r "^3.0.0-pre.2") (d #t) (k 0)) (d (n "layout21raw") (r "^3.0.0-pre.2") (d #t) (k 0)) (d (n "layout21utils") (r "^3.0.0-pre.2") (d #t) (k 0)) (d (n "lef21") (r "^3.0.0-pre.2") (d #t) (k 0)))) (h "1lrryyirwhp9sgjks8iqq5mib946liiji3sn6gwz80yydv9gxmrl")))

