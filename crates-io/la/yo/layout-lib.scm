(define-module (crates-io la yo layout-lib) #:use-module (crates-io))

(define-public crate-layout-lib-0.1.0 (c (n "layout-lib") (v "0.1.0") (d (list (d (n "layout-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1fnzcsyh6m3grxl2cicb464fikp16chd77rf1x9mh9n94h3cvgay")))

(define-public crate-layout-lib-0.1.1 (c (n "layout-lib") (v "0.1.1") (d (list (d (n "layout-macro") (r "^0.1.0") (d #t) (k 0)))) (h "1m1q8wq61cg2shwwpipyy9i1qc9ypnj7xcnvpdzbdwg1lcnrk9g6")))

