(define-module (crates-io la yo layouts-rs) #:use-module (crates-io))

(define-public crate-layouts-rs-0.1.0 (c (n "layouts-rs") (v "0.1.0") (d (list (d (n "derive-object-merge") (r "^0.1.0-alpha1") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "object-merge") (r "^0.1.0-alpha1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vkyhil8is267nn8sb80fc9srlj76z0cmjybhh7mx2imdkb9dv8h")))

