(define-module (crates-io la yo layout_id) #:use-module (crates-io))

(define-public crate-layout_id-0.0.1 (c (n "layout_id") (v "0.0.1") (h "01ffi62h8r7v5yfwsc4sr3mrw8s27plbd3zy06g3gdvkays5gdna")))

(define-public crate-layout_id-0.1.0 (c (n "layout_id") (v "0.1.0") (d (list (d (n "twox-hash") (r "*") (d #t) (k 0)))) (h "1ra3gbq0h047vy0nq0yh9i70b79dr160lrc6nwmmlqd5b8viqq42")))

(define-public crate-layout_id-0.2.0 (c (n "layout_id") (v "0.2.0") (d (list (d (n "clippy") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)) (d (n "twox-hash") (r "*") (d #t) (k 0)))) (h "1xpr5gyqha6qapnfk3w633p8d1kxal1ygyj8rmpq4y0lcwzsyd74")))

(define-public crate-layout_id-0.2.1 (c (n "layout_id") (v "0.2.1") (d (list (d (n "clippy") (r "*") (o #t) (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "regex_macros") (r "*") (d #t) (k 0)) (d (n "twox-hash") (r "*") (d #t) (k 0)))) (h "1ziah5xpgdwcgfsk1laaa10w5m3j4026cdp20x89bal3m04zbvjh") (f (quote (("use_clippy" "clippy"))))))

(define-public crate-layout_id-0.2.2 (c (n "layout_id") (v "0.2.2") (d (list (d (n "clippy") (r "*") (o #t) (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "twox-hash") (r "*") (d #t) (k 0)))) (h "0kq10lfbh8gjkzg1l02xq7cq2yw3yd52ykw1kwfbn3z0pvzy7ndp") (f (quote (("use_clippy" "clippy"))))))

(define-public crate-layout_id-0.2.3 (c (n "layout_id") (v "0.2.3") (d (list (d (n "clippy") (r "*") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "twox-hash") (r "^0.1") (d #t) (k 0)))) (h "072w2fnh3lpx6jjnjn8hbbmpdq9dpykcwkra3w5qr80ay0dibdmx") (f (quote (("use_clippy" "clippy"))))))

(define-public crate-layout_id-0.2.4 (c (n "layout_id") (v "0.2.4") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "twox-hash") (r "^0.1") (d #t) (k 0)))) (h "02lisynh6bzrp0d6m5qs6gr0wl0xxp7dym7r50d2x23k01ym44zh")))

(define-public crate-layout_id-0.3.0 (c (n "layout_id") (v "0.3.0") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1xa9lv6gllil18vn6mxkq5kbz6jd9v4jgs92vfnn9kz693syw1id")))

