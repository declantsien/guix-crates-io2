(define-module (crates-io la yo layout21utils) #:use-module (crates-io))

(define-public crate-layout21utils-0.2.0 (c (n "layout21utils") (v "0.2.0") (d (list (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "0gnkp9cj3rby1m7fqa0h7kcd82bp5i56qq62l0slab0vs6jyfdqp")))

(define-public crate-layout21utils-3.0.0-pre.2 (c (n "layout21utils") (v "3.0.0-pre.2") (d (list (d (n "by_address") (r "^1.0.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.10") (f (quote ("rust_decimal"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.88") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "textwrap") (r "^0.14.2") (d #t) (k 0)) (d (n "toml") (r "^0.5.10") (d #t) (k 0)))) (h "18bpqhyzn7bx0an4cfr9al2bg70ivwn14filz8n12jwbaaaa5ph7")))

