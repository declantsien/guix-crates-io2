(define-module (crates-io la yo layouts) #:use-module (crates-io))

(define-public crate-layouts-0.1.0 (c (n "layouts") (v "0.1.0") (d (list (d (n "getset") (r "^0.1.1") (d #t) (k 0)) (d (n "pretty") (r "^0.10.0") (d #t) (k 0)) (d (n "tycho") (r "^0.1.2") (d #t) (k 0)))) (h "1k7q4mascswml9xfvwj2rg7zp8j58gdn08b1zqxv7ja0kc02wa0y")))

