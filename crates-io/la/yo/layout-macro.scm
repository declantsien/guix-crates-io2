(define-module (crates-io la yo layout-macro) #:use-module (crates-io))

(define-public crate-layout-macro-0.1.0 (c (n "layout-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (f (quote ("full"))) (d #t) (k 0)))) (h "0rxpwclxz1vwbgcnxmly7s5203fi9q8m0jjqmhyjds99sndx12kz")))

