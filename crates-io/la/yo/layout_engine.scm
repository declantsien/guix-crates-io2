(define-module (crates-io la yo layout_engine) #:use-module (crates-io))

(define-public crate-layout_engine-0.1.0 (c (n "layout_engine") (v "0.1.0") (h "0gsn7vaa4388v2y493bk21r08lk6mslcbw4dcxrzk5a559hhjkg7")))

(define-public crate-layout_engine-0.1.1 (c (n "layout_engine") (v "0.1.1") (h "00b50im59zi01yqsy8hd0mcibqx7mp78rn1pdnkk39445p3m43kg")))

(define-public crate-layout_engine-0.2.0 (c (n "layout_engine") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1871amf1amz54lnap268zn5cchkh0aasaar36zgmp5mi6j2l06h5")))

(define-public crate-layout_engine-0.3.0 (c (n "layout_engine") (v "0.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1pk7fh96xhhwcr85hp9rnp5x7xi0ap2h107wvg2z8dyqhy45j93k")))

(define-public crate-layout_engine-0.4.0 (c (n "layout_engine") (v "0.4.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1s4wawiy62ya6yiyi1cwrf32vyrmijgvl19kkd07aygli419h160")))

(define-public crate-layout_engine-0.5.0 (c (n "layout_engine") (v "0.5.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0q7qppp9ykbndp3g7xazf0a2b3956yknwcv0n0b45n1gcz59n640")))

(define-public crate-layout_engine-0.5.2 (c (n "layout_engine") (v "0.5.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1bqpvg2pvrs01vp6cyk05qjg7fhk5ik7878s415q4b6zq5n1yghh")))

(define-public crate-layout_engine-0.5.3 (c (n "layout_engine") (v "0.5.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0l1npqd2mahcc6hbw679s7y45dnxm8535g2cilcrqhpxwwbwx1lp")))

(define-public crate-layout_engine-0.5.4 (c (n "layout_engine") (v "0.5.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0dg2kk2xx76ffcfly13q03557xwv27p3iwxmbahgskx9qw1n20yq")))

(define-public crate-layout_engine-0.6.0 (c (n "layout_engine") (v "0.6.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1iraa4ajajs24zjld8ywqzr2c8ky89bsw92lnlwgbjnbwxwl34cw")))

