(define-module (crates-io la ye layered-nlp) #:use-module (crates-io))

(define-public crate-layered-nlp-0.1.0 (c (n "layered-nlp") (v "0.1.0") (d (list (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.10.3") (f (quote ("std"))) (k 2)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "05rmn6iy4x8qr5vlfv82vqaim5pcglkll9ks79q164y7j5ij7svn")))

(define-public crate-layered-nlp-0.1.1 (c (n "layered-nlp") (v "0.1.1") (d (list (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.10.3") (f (quote ("std"))) (k 2)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)))) (h "0pr1ias1vw8x2mrxgrw1n2yminrwz96c1x9iqzk61fs73cyv112k")))

