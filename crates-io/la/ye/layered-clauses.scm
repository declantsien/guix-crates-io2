(define-module (crates-io la ye layered-clauses) #:use-module (crates-io))

(define-public crate-layered-clauses-0.1.0 (c (n "layered-clauses") (v "0.1.0") (d (list (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "layered-nlp") (r "^0.1") (k 0)))) (h "1rcjgrxsxw2wbh6avl4g2qw8q7smqf5vp3jlffxk7lcljb3gbxmy")))

(define-public crate-layered-clauses-0.1.1 (c (n "layered-clauses") (v "0.1.1") (d (list (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "layered-nlp") (r "^0.1") (k 0)) (d (n "layered-part-of-speech") (r "^0.1") (d #t) (k 2)))) (h "0kckiqx08wsycq37340d6bd09hjy902c25n74sn4fign50ybcqiv")))

