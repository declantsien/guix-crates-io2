(define-module (crates-io la ye layered-amount) #:use-module (crates-io))

(define-public crate-layered-amount-0.1.0 (c (n "layered-amount") (v "0.1.0") (d (list (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "layered-nlp") (r "^0.1") (k 0)) (d (n "rust_decimal") (r "^1.10") (f (quote ("std"))) (k 0)))) (h "1zxjxm7s88rp5h5mi0c11bjpllibjypii3f3kdqra9rj9djplp7n")))

(define-public crate-layered-amount-0.1.1 (c (n "layered-amount") (v "0.1.1") (d (list (d (n "insta") (r "^1.7.1") (d #t) (k 2)) (d (n "layered-nlp") (r "^0.1") (k 0)) (d (n "rust_decimal") (r "^1.10") (f (quote ("std"))) (k 0)))) (h "03n5vzwa0c1dl0g272ipark3d90p2016x1b307kqrx2wr15cyc8g")))

