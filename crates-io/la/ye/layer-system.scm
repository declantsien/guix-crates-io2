(define-module (crates-io la ye layer-system) #:use-module (crates-io))

(define-public crate-layer-system-0.1.0 (c (n "layer-system") (v "0.1.0") (h "0iyglpjy6iwyls4qar4zrls9ibw54kk5glwdbiip508p6b5v3mjz")))

(define-public crate-layer-system-0.1.1 (c (n "layer-system") (v "0.1.1") (h "19k0y957z0ncrlj80nwc7ya0fx3cd0cva105rj4ysp14lllsdrcz")))

(define-public crate-layer-system-0.1.2 (c (n "layer-system") (v "0.1.2") (h "12bdhyhkgvxx4ix2bijgmbp7l349rfv1c1kgy4xbavz1xnpnw7gn")))

(define-public crate-layer-system-0.2.0 (c (n "layer-system") (v "0.2.0") (h "04dqwzbmdrvlmcns6hyg4ihc5ffrvgbfwlx53lfl257pbps7vrll")))

(define-public crate-layer-system-0.3.0 (c (n "layer-system") (v "0.3.0") (h "15pb69zxl1sgw2xnr6rlc6r6s2pz9li6qlrijsr2ji5klvm77l5b")))

(define-public crate-layer-system-0.3.1 (c (n "layer-system") (v "0.3.1") (h "0jzri33rmb41f8wjrfv5q6yd551sfr6i2lcdif622249awvn59li")))

