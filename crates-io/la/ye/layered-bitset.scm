(define-module (crates-io la ye layered-bitset) #:use-module (crates-io))

(define-public crate-layered-bitset-0.1.0 (c (n "layered-bitset") (v "0.1.0") (h "0i90gikcav51icng1b4xh93rb1iikk7spfa4x0b0jq31h9yq3wjp") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-layered-bitset-0.1.1 (c (n "layered-bitset") (v "0.1.1") (h "0x2cfswm7ic8wmjxsx44vfi7vw3cdwfv0hng7mw792hlzv9svg9y") (f (quote (("default" "alloc") ("alloc")))) (y #t)))

(define-public crate-layered-bitset-0.1.2 (c (n "layered-bitset") (v "0.1.2") (h "14izsgzs756ngq0xvcfgkq8lmks58i5z3vssbg10nwqn8a9jh157") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-layered-bitset-0.1.3 (c (n "layered-bitset") (v "0.1.3") (h "0wv30axz2hidmzwmsrk7lvv8zh44816vzg37fvdgkdkf95ka44m7") (f (quote (("default" "alloc") ("alloc"))))))

