(define-module (crates-io la ye layered-part-of-speech) #:use-module (crates-io))

(define-public crate-layered-part-of-speech-0.1.0 (c (n "layered-part-of-speech") (v "0.1.0") (d (list (d (n "layered-nlp") (r "^0.1") (k 0)) (d (n "wiktionary-part-of-speech-extract") (r "^0.1") (d #t) (k 0)))) (h "0ccqpqf13xbza98376dl0a2brnpxi9ld693w1lhdykd7m48sa556")))

(define-public crate-layered-part-of-speech-0.1.1 (c (n "layered-part-of-speech") (v "0.1.1") (d (list (d (n "layered-nlp") (r "^0.1") (k 0)) (d (n "wiktionary-part-of-speech-extract") (r "^0.1") (d #t) (k 0)))) (h "1mdlzfpyp440bjm19szqwzs5if4ah2s4afzlkk9cx1pdnyfrwkca")))

