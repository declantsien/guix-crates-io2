(define-module (crates-io la ck lacking) #:use-module (crates-io))

(define-public crate-lacking-0.0.1 (c (n "lacking") (v "0.0.1") (h "1jlksbmzh9bax1gfd6b9cw4nf5zbp36wp2vznlyfr43q4ah5sgzz")))

(define-public crate-lacking-0.0.2 (c (n "lacking") (v "0.0.2") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 0)) (d (n "glfw") (r "^0.51.0") (o #t) (d #t) (k 0)))) (h "07xk2m0b11342p4rhp48wxqvfmqjlgxdplqw4gvaskc2xgx5x24d") (f (quote (("desktop" "glfw") ("default") ("browser"))))))

