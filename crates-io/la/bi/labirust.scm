(define-module (crates-io la bi labirust) #:use-module (crates-io))

(define-public crate-labirust-0.1.0 (c (n "labirust") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0n2c9wjzp2ynfrk0xmm558dggh7336a3qsykqyyvwajlfhd46mv9")))

(define-public crate-labirust-0.1.1 (c (n "labirust") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "098791jjh9189k6g495v2frsg586q3v5ba16vg474209ddknqnyx")))

(define-public crate-labirust-0.2.0 (c (n "labirust") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1xvwr0xxb5j1n07z3bbz08zj6b6314s780j58x2l5g1w1pl3344c")))

(define-public crate-labirust-0.2.1 (c (n "labirust") (v "0.2.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "0l6h14bfpz3wqld345za53mgpr8m1vx5kgdnc46f1qg6sr761w45")))

(define-public crate-labirust-0.3.0 (c (n "labirust") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "134z6mcdngfz265v73vqq0sshaiwnjipai6bbbxd37hqikpyd2dy")))

