(define-module (crates-io la bi labirust-cli) #:use-module (crates-io))

(define-public crate-labirust-cli-0.1.0 (c (n "labirust-cli") (v "0.1.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "labirust") (r "^0.3") (d #t) (k 0)))) (h "1v4f4qchk207dcm7a3qsc7xp11fzdl90jm3vzsqv4p64pabwpg3i")))

