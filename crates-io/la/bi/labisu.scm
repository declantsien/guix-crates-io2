(define-module (crates-io la bi labisu) #:use-module (crates-io))

(define-public crate-labisu-0.1.0 (c (n "labisu") (v "0.1.0") (d (list (d (n "normalize_url") (r "^0.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r7mqxszjrzv76s417padjwsq4892bbfp5ms1qkyrmq0bqm7gr8y")))

(define-public crate-labisu-0.1.1 (c (n "labisu") (v "0.1.1") (d (list (d (n "normalize_url") (r "^0.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gbirf6ssikvjdb009rqipd9bcdkvriwxcd1djnacvm3mpayds35")))

