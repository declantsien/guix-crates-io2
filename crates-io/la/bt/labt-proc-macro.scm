(define-module (crates-io la bt labt-proc-macro) #:use-module (crates-io))

(define-public crate-labt-proc-macro-0.1.0 (c (n "labt-proc-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "1l0gshlkivwij3h0daqx8vqh58ich9r1xzd3yfbqq18b9p8wp2m1")))

