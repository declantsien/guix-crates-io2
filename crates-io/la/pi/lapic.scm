(define-module (crates-io la pi lapic) #:use-module (crates-io))

(define-public crate-lapic-0.1.0 (c (n "lapic") (v "0.1.0") (d (list (d (n "memoffset") (r "^0.9.0") (d #t) (k 2)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)))) (h "1w16kih7fmc948h6yz4wql37zm3vfs06hqrd2p9dagqym8xgzahx")))

(define-public crate-lapic-0.1.1 (c (n "lapic") (v "0.1.1") (d (list (d (n "memoffset") (r "^0.9.0") (d #t) (k 2)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)))) (h "02c1vs316p6a8qkd9xcck7kb7agssl8jyr12l8byq8jfq7ibx9zw")))

