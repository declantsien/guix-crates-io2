(define-module (crates-io la pi lapin-actix-rt) #:use-module (crates-io))

(define-public crate-lapin-actix-rt-0.1.0-alpha-1 (c (n "lapin-actix-rt") (v "0.1.0-alpha-1") (d (list (d (n "actix-rt") (r "^2.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "executor-trait") (r "^2.0") (d #t) (k 0)) (d (n "lapin") (r "^1.6") (k 0)))) (h "1s1h0sd49qns5x9zh6aq4cpjsg59lh33fisxy3lcxzzdnm5jfa73") (y #t)))

(define-public crate-lapin-actix-rt-0.1.0-alpha-2 (c (n "lapin-actix-rt") (v "0.1.0-alpha-2") (d (list (d (n "actix-rt") (r "^1.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "executor-trait") (r "^2.0") (d #t) (k 0)) (d (n "lapin") (r "^1.6") (k 0)))) (h "045scn3hjyd55ssi4p51mg3jf7ly6ca64bqzw9k7xcwvnlnhafnr") (y #t)))

(define-public crate-lapin-actix-rt-0.1.0 (c (n "lapin-actix-rt") (v "0.1.0") (d (list (d (n "actix-rt") (r "^1.1") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "executor-trait") (r "^2.0") (d #t) (k 0)) (d (n "lapin") (r "^1.6") (k 0)))) (h "03a1hcfkfwa5apff44cpybzd7xxx5qmsnn4m2hwm8fkj6v3mcm9m")))

