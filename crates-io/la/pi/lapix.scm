(define-module (crates-io la pi lapix) #:use-module (crates-io))

(define-public crate-lapix-0.1.0 (c (n "lapix") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-case") (r "^2.2.2") (d #t) (k 2)))) (h "0igkd3p5fiija5qghrjp3f9adn5mzpzsic5w4yh3j0id0qj6wqbv")))

