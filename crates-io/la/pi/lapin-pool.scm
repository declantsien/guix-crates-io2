(define-module (crates-io la pi lapin-pool) #:use-module (crates-io))

(define-public crate-lapin-pool-0.0.0 (c (n "lapin-pool") (v "0.0.0") (h "0wx2pbwvs528269a4k1c2nprhf9ybby41sx9a0av68gvw5f73dyv")))

(define-public crate-lapin-pool-0.1.0 (c (n "lapin-pool") (v "0.1.0") (d (list (d (n "lapin") (r "^2") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "snafu") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio-executor-trait") (r "^2") (d #t) (k 0)) (d (n "tokio-reactor-trait") (r "^1") (d #t) (k 0)))) (h "0ij0nn02xcgnwq9yzarsd49zzh9j1riwgjy6w2iianv6q59s33ij")))

