(define-module (crates-io la zu lazuli_derive) #:use-module (crates-io))

(define-public crate-lazuli_derive-0.1.1-rc.2 (c (n "lazuli_derive") (v "0.1.1-rc.2") (d (list (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (d #t) (k 0)))) (h "0g5v42mk97jnw5pxc37d6qgi04rzpjqhdzq748imxmfwg1snnkbi")))

