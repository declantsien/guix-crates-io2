(define-module (crates-io la zu lazuli_core) #:use-module (crates-io))

(define-public crate-lazuli_core-0.1.1-rc.2 (c (n "lazuli_core") (v "0.1.1-rc.2") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.2") (d #t) (k 2)))) (h "00g5953x120wbx7a7cn60mxqgapijbwb45k8hz6wl10cjhk2q5yr")))

(define-public crate-lazuli_core-0.1.1-rc.3 (c (n "lazuli_core") (v "0.1.1-rc.3") (d (list (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.2") (d #t) (k 2)))) (h "0gaqy4i2a4sp8xidfaw1ln7hahi55agscz3lm7w7g1rbyzlqs3ny")))

