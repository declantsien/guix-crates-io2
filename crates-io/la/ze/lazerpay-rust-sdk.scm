(define-module (crates-io la ze lazerpay-rust-sdk) #:use-module (crates-io))

(define-public crate-lazerpay-rust-sdk-0.1.0 (c (n "lazerpay-rust-sdk") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "envy") (r "^0.4.2") (d #t) (k 0)) (d (n "erased-serde") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0a1l86ks3bkbky1596k6mhlxfdgbw5bi6rgvigs9a4ccrj8b83j5")))

(define-public crate-lazerpay-rust-sdk-0.1.1 (c (n "lazerpay-rust-sdk") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "envy") (r "^0.4.2") (d #t) (k 0)) (d (n "erased-serde") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03s7vdndf97mlj60mjwy56lnwrhzg5zn8pkzinkds8369xqx9s80")))

