(define-module (crates-io la ze lazer) #:use-module (crates-io))

(define-public crate-lazer-0.1.0 (c (n "lazer") (v "0.1.0") (h "0rxlh7fqv5l89736mcygbgknzzgwd4l5naq0jcfyvgmcfi2x2h46")))

(define-public crate-lazer-0.1.1 (c (n "lazer") (v "0.1.1") (h "1vw3fmq16d6gzgxrnphyr40wxl9wlbh9qmp4sx4czwki2iihdfpl")))

(define-public crate-lazer-0.2.0 (c (n "lazer") (v "0.2.0") (h "0smrvqhbvr2nrxs26apw019pnc17ghvgaa9zy01ysjmq6k4y5n4j")))

(define-public crate-lazer-0.3.0 (c (n "lazer") (v "0.3.0") (h "0yb45b02vikd6bssg01yqwwppps5i8jhy7paibd93wxf5q2v16di")))

(define-public crate-lazer-0.3.1 (c (n "lazer") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "17yzwa5cr90qyw1885nhkacsy66qa8lj9v8j5cpcyv2qpp4z90rk")))

