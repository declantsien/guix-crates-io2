(define-module (crates-io la ve lavellang) #:use-module (crates-io))

(define-public crate-lavellang-0.3.0 (c (n "lavellang") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.17") (d #t) (k 0)) (d (n "evalexpr") (r "^8.1.0") (d #t) (k 0)) (d (n "lexer") (r "^0.1.18") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)))) (h "1rf33czjd6ip5v4f2y0xhx33sq0fyv5v67cfq5q81i1fcpfy5aa3")))

