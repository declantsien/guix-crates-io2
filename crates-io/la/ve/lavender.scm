(define-module (crates-io la ve lavender) #:use-module (crates-io))

(define-public crate-lavender-0.1.0 (c (n "lavender") (v "0.1.0") (d (list (d (n "clap") (r "^2.29") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "gc") (r "^0.3") (d #t) (k 0)) (d (n "gc_derive") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "pest") (r "^1.0.0-beta.16") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0-beta.16") (d #t) (k 0)) (d (n "rustyline") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0c27my7b7dgga2fsmvyy0pzkg2jx8axk5wxby4lcssxbzfkdw4a4")))

