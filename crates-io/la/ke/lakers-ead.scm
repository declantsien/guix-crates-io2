(define-module (crates-io la ke lakers-ead) #:use-module (crates-io))

(define-public crate-lakers-ead-0.5.1 (c (n "lakers-ead") (v "0.5.1") (d (list (d (n "lakers-ead-authz") (r "^0.5.1") (o #t) (d #t) (k 0) (p "lakers-ead-authz")) (d (n "lakers-shared") (r "^0.5.1") (d #t) (k 0) (p "lakers-shared")))) (h "1hm4wcamhzcp1bppsmci609rrzmf9hfzipbl1r86wa5knmrmcl03") (f (quote (("ead-none") ("ead-authz" "lakers-ead-authz") ("default" "ead-none"))))))

