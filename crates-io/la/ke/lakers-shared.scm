(define-module (crates-io la ke lakers-shared) #:use-module (crates-io))

(define-public crate-lakers-shared-0.4.0-alpha.1 (c (n "lakers-shared") (v "0.4.0-alpha.1") (d (list (d (n "hexlit") (r "^0.5.3") (d #t) (k 2)))) (h "0zi1nfgm1ggd8ybzknk9vi3ig731wph105s01amcllfbjpsnjp1z")))

(define-public crate-lakers-shared-0.4.0 (c (n "lakers-shared") (v "0.4.0") (d (list (d (n "hexlit") (r "^0.5.3") (d #t) (k 2)))) (h "0w4bnrb9c4rjfnv0jazm17hi86zyj90cg8kpwz1g0vchylssl4rq")))

(define-public crate-lakers-shared-0.4.1 (c (n "lakers-shared") (v "0.4.1") (d (list (d (n "hexlit") (r "^0.5.3") (d #t) (k 2)))) (h "1y8pbqlc9k6if2cldiscaayjvj72rj7mwd93nb30ajy59vwzvlxg")))

(define-public crate-lakers-shared-0.5.0 (c (n "lakers-shared") (v "0.5.0") (d (list (d (n "hexlit") (r "^0.5.3") (d #t) (k 2)) (d (n "pyo3") (r "^0.20.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "04m2dryy71kz4s6yr00mgdb70l2ryb21hcyvmzxiiyaabfmz9rxy") (f (quote (("python-bindings" "pyo3") ("default"))))))

(define-public crate-lakers-shared-0.5.1 (c (n "lakers-shared") (v "0.5.1") (d (list (d (n "hexlit") (r "^0.5.3") (d #t) (k 2)) (d (n "pyo3") (r "^0.20.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "0irjnhlcqvpsgnsbhjm1j1cnizchzy8c2aky0jm8vj6rdfqz1pd2") (f (quote (("python-bindings" "pyo3") ("default"))))))

(define-public crate-lakers-shared-0.6.0 (c (n "lakers-shared") (v "0.6.0") (d (list (d (n "hex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "hexlit") (r "^0.5.3") (d #t) (k 2)) (d (n "pyo3") (r "^0.20.2") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "0rwai0hhnvrm02j32iybvhz1jgcpn5v19wx14ss6nzq8bslhqrdi") (f (quote (("python-bindings" "pyo3" "hex") ("default"))))))

