(define-module (crates-io la ke lakers-ead-authz) #:use-module (crates-io))

(define-public crate-lakers-ead-authz-0.5.0 (c (n "lakers-ead-authz") (v "0.5.0") (d (list (d (n "hexlit") (r "^0.5.3") (d #t) (k 2)) (d (n "lakers-shared") (r "^0.5.0") (d #t) (k 0) (p "lakers-shared")))) (h "1gyxr7wzh1cg4j2xx3k6lji1y30dkryk20n7j64hlk17adww082n")))

(define-public crate-lakers-ead-authz-0.5.1 (c (n "lakers-ead-authz") (v "0.5.1") (d (list (d (n "hexlit") (r "^0.5.3") (d #t) (k 2)) (d (n "lakers-shared") (r "^0.5.1") (d #t) (k 0) (p "lakers-shared")))) (h "1nkzxmbspm5z1bld9pbmzk585lhy0w1m9w1yir1ya823xa0kmw6a")))

(define-public crate-lakers-ead-authz-0.6.0 (c (n "lakers-ead-authz") (v "0.6.0") (d (list (d (n "hexlit") (r "^0.5.3") (d #t) (k 2)) (d (n "lakers-shared") (r "^0.6.0") (d #t) (k 0) (p "lakers-shared")))) (h "0nhhjbpbi4wxfzlmzqi9mylxy0lxmqyzfmhrc8i06kykdv7zcvhi")))

