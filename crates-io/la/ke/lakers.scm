(define-module (crates-io la ke lakers) #:use-module (crates-io))

(define-public crate-lakers-0.4.0-alpha.1 (c (n "lakers") (v "0.4.0-alpha.1") (d (list (d (n "edhoc-consts") (r "^0.4.0-alpha.1") (d #t) (k 0) (p "lakers-shared")) (d (n "edhoc-ead") (r "^0.4.0-alpha.1") (k 0) (p "lakers-ead-dispatch")) (d (n "hexlit") (r "^0.5.3") (d #t) (k 2)))) (h "0f2m9m1k63nw4z5vcsl4xgj1xc7sqxpad5jynvqsxd8fbksx4nji") (f (quote (("ead-zeroconf" "edhoc-ead/ead-zeroconf") ("ead-none" "edhoc-ead/ead-none") ("default" "edhoc-ead/ead-none"))))))

(define-public crate-lakers-0.4.0 (c (n "lakers") (v "0.4.0") (d (list (d (n "hexlit") (r "^0.5.3") (d #t) (k 2)) (d (n "lakers-ead") (r "^0.4.0") (k 0) (p "lakers-ead-dispatch")) (d (n "lakers-shared") (r "^0.4.0") (d #t) (k 0) (p "lakers-shared")))) (h "1nf4sxrmncs07vnd29a9a8llv8x0wb9s2nng3rdyxyxbz6q3vd5b") (f (quote (("ead-zeroconf" "lakers-ead/ead-zeroconf") ("ead-none" "lakers-ead/ead-none") ("default" "lakers-ead/ead-none"))))))

(define-public crate-lakers-0.4.1 (c (n "lakers") (v "0.4.1") (d (list (d (n "hexlit") (r "^0.5.3") (d #t) (k 2)) (d (n "lakers-ead") (r "^0.4.1") (k 0) (p "lakers-ead-dispatch")) (d (n "lakers-shared") (r "^0.4.1") (d #t) (k 0) (p "lakers-shared")))) (h "001av246yly4m0z2gg3nk7wr0j1bcyxjyc0gbh5lcm8a7c1wygfs") (f (quote (("ead-zeroconf" "lakers-ead/ead-zeroconf") ("ead-none" "lakers-ead/ead-none") ("default" "lakers-ead/ead-none"))))))

(define-public crate-lakers-0.5.0 (c (n "lakers") (v "0.5.0") (d (list (d (n "hexlit") (r "^0.5.3") (d #t) (k 2)) (d (n "lakers-ead") (r "^0.5.0") (k 0) (p "lakers-ead-dispatch")) (d (n "lakers-shared") (r "^0.5.0") (d #t) (k 0) (p "lakers-shared")))) (h "0hg3b1a8ylawnrxsvj1nkihlb0lqx93x3mg84xil9dnv17k8anvh") (f (quote (("ead-none" "lakers-ead/ead-none") ("ead-authz" "lakers-ead/ead-authz") ("default" "lakers-ead/ead-none"))))))

(define-public crate-lakers-0.5.1 (c (n "lakers") (v "0.5.1") (d (list (d (n "hexlit") (r "^0.5.3") (d #t) (k 2)) (d (n "lakers-ead") (r "^0.5.1") (k 2) (p "lakers-ead")) (d (n "lakers-shared") (r "^0.5.1") (d #t) (k 0) (p "lakers-shared")))) (h "0dd5bypwbaxhfyh2pk1n7vg52bmcai2bdq76rx4sf9i6wz118plh") (f (quote (("test-ead-none" "lakers-ead/ead-none") ("test-ead-authz" "lakers-ead/ead-authz") ("default" "test-ead-none"))))))

(define-public crate-lakers-0.6.0 (c (n "lakers") (v "0.6.0") (d (list (d (n "hexlit") (r "^0.5.3") (d #t) (k 2)) (d (n "lakers-ead-authz") (r "^0.6.0") (d #t) (k 2) (p "lakers-ead-authz")) (d (n "lakers-shared") (r "^0.6.0") (d #t) (k 0) (p "lakers-shared")))) (h "19k1ldam1c0jq0mx62vma1i1qrmbj1nfrg2dfm4grryb69k2gmxc") (f (quote (("test-ead-none") ("test-ead-authz") ("default" "test-ead-none"))))))

