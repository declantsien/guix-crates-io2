(define-module (crates-io la ke lakers-ead-none) #:use-module (crates-io))

(define-public crate-lakers-ead-none-0.4.0-alpha.1 (c (n "lakers-ead-none") (v "0.4.0-alpha.1") (d (list (d (n "edhoc-consts") (r "^0.4.0-alpha.1") (d #t) (k 0) (p "lakers-shared")))) (h "01r23vxmia9w9jpdrsxnr380g326awcigxdm94gq6jcfxdnynfcn")))

(define-public crate-lakers-ead-none-0.4.0 (c (n "lakers-ead-none") (v "0.4.0") (d (list (d (n "lakers-shared") (r "^0.4.0") (d #t) (k 0) (p "lakers-shared")))) (h "15s7jjg7bvqslcc39xsxyhdr03fmxh0h2ddndsnsbdgsdrrvpnry")))

(define-public crate-lakers-ead-none-0.4.1 (c (n "lakers-ead-none") (v "0.4.1") (d (list (d (n "lakers-shared") (r "^0.4.1") (d #t) (k 0) (p "lakers-shared")))) (h "0m61w0jq5m8jdjn7na0c54k24v3zyia1dinzxmmqfn5wwyzgm989")))

