(define-module (crates-io la ke lake-parent-transaction-cache) #:use-module (crates-io))

(define-public crate-lake-parent-transaction-cache-0.8.0-beta.1 (c (n "lake-parent-transaction-cache") (v "0.8.0-beta.1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 2)) (d (n "cached") (r "^0.43.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "near-lake-framework") (r "^0.8.0-beta.1") (d #t) (k 0)))) (h "0sbdn9q4p5wgjmmrwvg1hm5icsl09zbqsklhgs27m687nr9852hw")))

