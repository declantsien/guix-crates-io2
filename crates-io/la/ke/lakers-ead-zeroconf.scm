(define-module (crates-io la ke lakers-ead-zeroconf) #:use-module (crates-io))

(define-public crate-lakers-ead-zeroconf-0.4.0-alpha.1 (c (n "lakers-ead-zeroconf") (v "0.4.0-alpha.1") (d (list (d (n "edhoc-consts") (r "^0.4.0-alpha.1") (d #t) (k 0) (p "lakers-shared")) (d (n "hexlit") (r "^0.5.3") (d #t) (k 2)))) (h "0fp1ymmvrk36qckm8d9m861p863h2zbyfcgm6zbcgc2f38nm7cai")))

(define-public crate-lakers-ead-zeroconf-0.4.0 (c (n "lakers-ead-zeroconf") (v "0.4.0") (d (list (d (n "hexlit") (r "^0.5.3") (d #t) (k 2)) (d (n "lakers-shared") (r "^0.4.0") (d #t) (k 0) (p "lakers-shared")))) (h "05h7b3mkparqvc23j49d75sgbr49id83301444fvr1p3q379y0yi")))

(define-public crate-lakers-ead-zeroconf-0.4.1 (c (n "lakers-ead-zeroconf") (v "0.4.1") (d (list (d (n "hexlit") (r "^0.5.3") (d #t) (k 2)) (d (n "lakers-shared") (r "^0.4.1") (d #t) (k 0) (p "lakers-shared")))) (h "11a32xhsi431va0s2klg87rp4v6iryqk04hdw4zqfxksaw3g42x8")))

