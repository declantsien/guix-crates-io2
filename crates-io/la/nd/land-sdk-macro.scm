(define-module (crates-io la nd land-sdk-macro) #:use-module (crates-io))

(define-public crate-land-sdk-macro-0.0.3 (c (n "land-sdk-macro") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "1xjfilaqphqn9ycwncnqlm2svlncy6via44jb53mnch6v0xmqv5l")))

(define-public crate-land-sdk-macro-0.0.4 (c (n "land-sdk-macro") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "183wmxc90c2rmngc68x2abi8pf669lrd8rkdqcgjvvw0wqaml7z8")))

(define-public crate-land-sdk-macro-0.1.0-rc.2 (c (n "land-sdk-macro") (v "0.1.0-rc.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "07zg4r6iqfgprkgl0f2jpml39rlaxc41sjh0jaajqnivgihl740b")))

(define-public crate-land-sdk-macro-0.1.0-rc.3 (c (n "land-sdk-macro") (v "0.1.0-rc.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "1vqr89gj4lvgbpmmr3la5djlnlljb4vlvpxb3fh7ngapml9ypl9i")))

(define-public crate-land-sdk-macro-0.1.0-rc.4 (c (n "land-sdk-macro") (v "0.1.0-rc.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "02kpcm15v1cirvsxc9b9fmrymm4napgvc8jw0wsg6napxcyc9rrg")))

(define-public crate-land-sdk-macro-0.1.0 (c (n "land-sdk-macro") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "0ww2hb1q28sh774qwwyy31syhljcicp1pnx7qlqndf3df5vp9vvv")))

(define-public crate-land-sdk-macro-0.1.1 (c (n "land-sdk-macro") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "1fspnjhgsp3pfaggpxsxqj9mw5zannqxsncy90sz65cwarllyr0h")))

(define-public crate-land-sdk-macro-0.1.4 (c (n "land-sdk-macro") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1k6q37c1h3l1p4xp066kfzkbnm3cyggfyk9py3cb6l4d7f8bsq3x")))

