(define-module (crates-io la nd land-migration) #:use-module (crates-io))

(define-public crate-land-migration-0.0.3 (c (n "land-migration") (v "0.0.3") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "sea-orm-migration") (r "^0.11.0") (d #t) (k 0)))) (h "1ak8f4vq05izzc64fk78i6pmdfmpxd454dg8dnh4kq258zh49bgp")))

(define-public crate-land-migration-0.0.4 (c (n "land-migration") (v "0.0.4") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "sea-orm-migration") (r "^0.11.0") (d #t) (k 0)))) (h "1ypbd4n3p31v1x5bipnf4s6m3ifn9k9j2shhbn8jpwzxwya8yj59")))

