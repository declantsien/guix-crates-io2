(define-module (crates-io la nd landlock) #:use-module (crates-io))

(define-public crate-landlock-0.1.0 (c (n "landlock") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0l4bg4gazpnmg8fiz1zk03ykngcbd4icnshz2sghvcf84q11gmp4")))

(define-public crate-landlock-0.2.0 (c (n "landlock") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "enumflags2") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2.133") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 2)) (d (n "strum_macros") (r "^0.24") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0s004kdjl87wvncz6driqcvndlnqbfy8d8f35xyraklcf0ral2sj") (r "1.63")))

(define-public crate-landlock-0.3.0 (c (n "landlock") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "enumflags2") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2.133") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 2)) (d (n "strum_macros") (r "^0.25") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1k6rcy5nf42pp4giydif4yk4axkkynx28zmg2v14mvgfffwwac0m") (r "1.63")))

(define-public crate-landlock-0.3.1 (c (n "landlock") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "enumflags2") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2.133") (d #t) (k 0)) (d (n "strum") (r "^0.25") (d #t) (k 2)) (d (n "strum_macros") (r "^0.25") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19sfpff04hfn74lk9q0yxybcdzglj2higrlpjd144n9idvmrxalv") (r "1.63")))

