(define-module (crates-io la nd land-sdk) #:use-module (crates-io))

(define-public crate-land-sdk-0.0.4 (c (n "land-sdk") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "land-sdk-macro") (r "^0.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "matchit") (r "^0.7.0") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.7.0") (d #t) (k 0)))) (h "14ahvmgh0345gjzsxw97srkp7irjy76b9bxa08y1wqklzmh2pbg2")))

(define-public crate-land-sdk-0.1.0-rc.2 (c (n "land-sdk") (v "0.1.0-rc.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "land-sdk-macro") (r "^0.1.0-rc.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "matchit") (r "^0.7.2") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.10.0") (d #t) (k 0)))) (h "1pixrf0sgkckc56hn74gd22cra68pxi0lcq1ccfjanlvzm002sca")))

(define-public crate-land-sdk-0.1.0-rc.4 (c (n "land-sdk") (v "0.1.0-rc.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "land-sdk-macro") (r "^0.1.0-rc.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "matchit") (r "^0.7.2") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.10.0") (d #t) (k 0)))) (h "1k9ai5597vqqv2k84qs3vwr7dl5q8wsh1yca4d2zmrvllysx1rci")))

(define-public crate-land-sdk-0.1.0 (c (n "land-sdk") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "land-sdk-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "matchit") (r "^0.7.2") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.10.0") (d #t) (k 0)))) (h "05lm6149dxf0y0ffjqbv35j2rlqdnj7kwm3ghip2ddbxx78sif0s")))

(define-public crate-land-sdk-0.1.1 (c (n "land-sdk") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "land-sdk-macro") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "matchit") (r "^0.7.2") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.11.0") (d #t) (k 0)))) (h "0bylqk5z56gl27hrx92ck7l2lfnnjs7aas65jim6s0i1sifwwn8g")))

(define-public crate-land-sdk-0.1.4 (c (n "land-sdk") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "land-sdk-macro") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "matchit") (r "^0.7.3") (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.12.0") (d #t) (k 0)))) (h "0w7l675vbmaf2fzjmc13k6xd4j6xs3yb42zgkb3lclhfk97g8nq2")))

