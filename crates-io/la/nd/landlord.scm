(define-module (crates-io la nd landlord) #:use-module (crates-io))

(define-public crate-landlord-0.1.1 (c (n "landlord") (v "0.1.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("wasm-bindgen" "small_rng"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1w34d3z427w577np7zxgxa974fwzz78mx2y6b6yinlkf5rzn7fhp")))

