(define-module (crates-io la nd land-calc) #:use-module (crates-io))

(define-public crate-land-calc-0.1.1 (c (n "land-calc") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "slint") (r "^1.3.2") (d #t) (k 0)) (d (n "slint-build") (r "^1.3.0") (d #t) (k 1)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "08c52v30blnrwig8jp9m3rfs3ymzcgk0lzmlx442nndng057r8gk")))

