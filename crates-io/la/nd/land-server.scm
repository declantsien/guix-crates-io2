(define-module (crates-io la nd land-server) #:use-module (crates-io))

(define-public crate-land-server-0.0.4 (c (n "land-server") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "axum") (r "^0.6.18") (d #t) (k 0)) (d (n "clap") (r "^4.3.2") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "land-core") (r "^0.0.4") (d #t) (k 0)) (d (n "land-restful") (r "^0.0.4") (d #t) (k 0)) (d (n "land-runtime") (r "^0.0.4") (d #t) (k 1)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.4") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0s22y4dhxx7pqxkviamcvi2yhi0187mpnz1chcha1d1hfbc7mign")))

