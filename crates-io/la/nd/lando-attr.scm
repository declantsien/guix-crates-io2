(define-module (crates-io la nd lando-attr) #:use-module (crates-io))

(define-public crate-lando-attr-0.2.0 (c (n "lando-attr") (v "0.2.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0dy3di5npwxkn3r5pnnw9cyrcakhas59hksdvsgqv85sk6k3zwam")))

(define-public crate-lando-attr-0.2.1 (c (n "lando-attr") (v "0.2.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1xr5qsj9v6dqihy3g3wg5k62xcxfqh7yjhlwpkbqd3k0awzp16hr")))

