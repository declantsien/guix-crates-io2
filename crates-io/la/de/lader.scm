(define-module (crates-io la de lader) #:use-module (crates-io))

(define-public crate-lader-0.1.0 (c (n "lader") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "regi") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jnl450s8kx46x4spap3q25cfa6jzifv3cp0rlb7d1c9vhp9bwf9")))

(define-public crate-lader-0.1.1 (c (n "lader") (v "0.1.1") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "regi") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hd074f54gh1rl3c32izm7xh4y21jmpv58pf91jnw83i7hizp81z")))

(define-public crate-lader-0.1.2 (c (n "lader") (v "0.1.2") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "regi") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13kgs9cz61yzxg5ck8mka218jhab1lg0mnah7h6g1k3f0b0vb2q5")))

(define-public crate-lader-0.1.3 (c (n "lader") (v "0.1.3") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "regi") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nwalgl81lwf3x5bscjwvf0ahv1k4z8s9q4y1z7haxj30x2byafd")))

