(define-module (crates-io la ir lair-keystore-socket-tester) #:use-module (crates-io))

(define-public crate-lair-keystore-socket-tester-0.1.1 (c (n "lair-keystore-socket-tester") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lair_keystore_api") (r "=0.3.0") (d #t) (k 0)) (d (n "serde") (r "=1.0.166") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0slq4s2lpc6kw6zj7fi5wbrh7id05pn58g7x2sgk5lqscmr9sxq9")))

(define-public crate-lair-keystore-socket-tester-0.1.2 (c (n "lair-keystore-socket-tester") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lair_keystore_api") (r "=0.3.0") (d #t) (k 0)) (d (n "serde") (r "=1.0.166") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "1817y4smmdkyvjbfbm2mmbxnyfdg1zrnlw2j2chqpm2ipxl9bjsg")))

(define-public crate-lair-keystore-socket-tester-0.1.3 (c (n "lair-keystore-socket-tester") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lair_keystore_api") (r "=0.3.0") (d #t) (k 0)) (d (n "serde") (r "=1.0.166") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0d1lzcik663nfwfj7fw3ibvlvb276z0a5abc33ivfs9l74sbhz7w")))

