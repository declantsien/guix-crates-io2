(define-module (crates-io la go lagoinha) #:use-module (crates-io))

(define-public crate-lagoinha-0.1.0 (c (n "lagoinha") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "curl") (r "^0.4") (f (quote ("static-curl"))) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1fd7z8hl74s4yf90140gn44lkvgswh32jwwwdq81jsv3w716mvz7")))

(define-public crate-lagoinha-0.2.0 (c (n "lagoinha") (v "0.2.0") (d (list (d (n "async-std") (r "^1.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "isahc") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1jv5n0n3cg96vy5br26jam6rlk3i96z82p0nzdzylpic9svpd8yy")))

