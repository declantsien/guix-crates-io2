(define-module (crates-io la rc larc) #:use-module (crates-io))

(define-public crate-larc-0.1.0 (c (n "larc") (v "0.1.0") (h "1x2x6191a1qvrcsyk4fyrbqa7zi6kyvgk40vyqkg6ifbk5rx70ir")))

(define-public crate-larc-0.2.0 (c (n "larc") (v "0.2.0") (h "066midswmv236q6367saaqbd58chz8akplr7pj7z4cv15rdrc8z8")))

(define-public crate-larc-0.2.1 (c (n "larc") (v "0.2.1") (h "04101awa34pw5g6fb3nk1hz7ahvsfwjgk27bka96ciam6ld68dc3")))

(define-public crate-larc-0.3.0 (c (n "larc") (v "0.3.0") (h "13k7hp41a2bsf7vdjm5dq1ky1wbwydi3adl7g8rngdbf73qls2ra")))

(define-public crate-larc-0.4.0 (c (n "larc") (v "0.4.0") (h "1irnvfh2db0h3gnbsissszvh7nha0zmpn1gw6z3r719fvlsx42wb")))

(define-public crate-larc-0.5.0 (c (n "larc") (v "0.5.0") (h "0xx5awn9a6bz976sdspxphdz0d0ny4xg4fcg9hjdj9pi9qnkglcn")))

