(define-module (crates-io la by laby) #:use-module (crates-io))

(define-public crate-laby-0.1.0 (c (n "laby") (v "0.1.0") (d (list (d (n "laby_common") (r "^0.1") (d #t) (k 0)) (d (n "laby_macros") (r "^0.1") (d #t) (k 0)))) (h "1hy3nz5p7v9k72qjz2923j3cnr87ymm6lvpv3pam2ahqmrbs4qji")))

(define-public crate-laby-0.2.0 (c (n "laby") (v "0.2.0") (d (list (d (n "laby_common") (r "^0.1") (d #t) (k 0)) (d (n "laby_macros") (r "^0.1") (d #t) (k 0)))) (h "124s1i5b1mp0fn13a8xi6dh5xj62zski62xbd7njs26c3b787lkp")))

(define-public crate-laby-0.2.1 (c (n "laby") (v "0.2.1") (d (list (d (n "laby_common") (r "^0.1.0") (d #t) (k 0)) (d (n "laby_macros") (r "^0.1.1") (d #t) (k 0)))) (h "1032xw71yqizpinkfka0zkg7y10b6hbw4q4y615cz26b5k8dw56x")))

(define-public crate-laby-0.2.2 (c (n "laby") (v "0.2.2") (d (list (d (n "laby_common") (r "^0.1.1") (d #t) (k 0)) (d (n "laby_macros") (r "^0.1.2") (d #t) (k 0)))) (h "17lvwcc5j7akbkk69dnq2y2wp1ndzyw06rl1pqz6j5p3ciia6z0x") (f (quote (("decl_macro" "laby_macros/decl_macro"))))))

(define-public crate-laby-0.2.3 (c (n "laby") (v "0.2.3") (d (list (d (n "laby_common") (r "^0.1.1") (d #t) (k 0)) (d (n "laby_macros") (r "^0.1.2") (d #t) (k 0)))) (h "08vh2cn3cjd8wdjkh9r4ngca9yxss58xq1bkl73560a9b6zvdyb4") (f (quote (("decl_macro" "laby_macros/decl_macro"))))))

(define-public crate-laby-0.2.4 (c (n "laby") (v "0.2.4") (d (list (d (n "laby_common") (r "^0.1.1") (d #t) (k 0)) (d (n "laby_macros") (r "^0.1.3") (d #t) (k 0)))) (h "12jh5yr4ns2z11siam9qnmdhib1nd079nms0b06kl6ah797ls6gl") (f (quote (("decl_macro" "laby_macros/decl_macro"))))))

(define-public crate-laby-0.2.5 (c (n "laby") (v "0.2.5") (d (list (d (n "laby_common") (r "^0.1") (d #t) (k 0)) (d (n "laby_macros") (r "^0.1") (d #t) (k 0)))) (h "0rq7mlc2fv61ajhakzj5654g9j9vm0q148zrrgkjfa6m3hi8sg9f") (f (quote (("decl_macro" "laby_macros/decl_macro"))))))

(define-public crate-laby-0.3.0 (c (n "laby") (v "0.3.0") (d (list (d (n "laby_common") (r "^0.1") (d #t) (k 0)) (d (n "laby_macros") (r "^0.2") (d #t) (k 0)))) (h "13wykf60qh0d3vmijyy90nq9r0r6ffs3ivn5y93wgad8qzwgkc83") (f (quote (("decl_macro" "laby_macros/decl_macro"))))))

(define-public crate-laby-0.3.1 (c (n "laby") (v "0.3.1") (d (list (d (n "laby_common") (r "^0.1") (d #t) (k 0)) (d (n "laby_macros") (r "^0.2") (d #t) (k 0)))) (h "1rnkk6wkd7dgy2mh2l3j0lpf61k54c71f4ch1792hhmy09gjyh8j") (f (quote (("decl_macro" "laby_macros/decl_macro"))))))

(define-public crate-laby-0.4.0 (c (n "laby") (v "0.4.0") (d (list (d (n "laby_common") (r "^0.2") (d #t) (k 0)) (d (n "laby_macros") (r "^0.3") (d #t) (k 0)))) (h "1ms3vr61ndvciarcrs6wd04wxmvlwc3c9zfz5pg4lvyrxrkipavf") (f (quote (("decl_macro" "laby_macros/decl_macro"))))))

(define-public crate-laby-0.4.1 (c (n "laby") (v "0.4.1") (d (list (d (n "laby_common") (r "^0.2") (d #t) (k 0)) (d (n "laby_macros") (r "^0.3") (d #t) (k 0)))) (h "03c1bqhkvxksr6g7hvsg4f6rrz08kjc43v5cwd19agml2qdr5xhm") (f (quote (("decl_macro" "laby_macros/decl_macro"))))))

