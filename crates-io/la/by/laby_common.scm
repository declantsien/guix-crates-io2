(define-module (crates-io la by laby_common) #:use-module (crates-io))

(define-public crate-laby_common-0.1.0 (c (n "laby_common") (v "0.1.0") (d (list (d (n "itoap") (r "^1") (d #t) (k 0)) (d (n "raw-cpuid") (r "^10") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)))) (h "0dghkcacxcw90nfjg6psjys7k73rc32ylbxm4dgb1kr1993qhim3")))

(define-public crate-laby_common-0.1.1 (c (n "laby_common") (v "0.1.1") (d (list (d (n "itoap") (r "^1") (d #t) (k 0)) (d (n "raw-cpuid") (r "^10") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)))) (h "0rfdqbg8p2ynbqiczqk55fjlmcai4lqh5k2csm38dp6bbmm8xjhs")))

(define-public crate-laby_common-0.1.2 (c (n "laby_common") (v "0.1.2") (d (list (d (n "itoap") (r "^1") (d #t) (k 0)) (d (n "raw-cpuid") (r "^10") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)))) (h "0b38bi8ixyqajd724mzq3axnzhp5i6yc50s0f01ggy7b3cd2l75i")))

(define-public crate-laby_common-0.1.3 (c (n "laby_common") (v "0.1.3") (d (list (d (n "itoap") (r "^1") (d #t) (k 0)) (d (n "raw-cpuid") (r "^10") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)))) (h "0nw1sbcqpv81m9n7advcl83i5imfa7qh7g299yl164sj8s355m8f")))

(define-public crate-laby_common-0.1.4 (c (n "laby_common") (v "0.1.4") (d (list (d (n "itoap") (r "^1") (d #t) (k 0)) (d (n "raw-cpuid") (r "^10") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)))) (h "0s95639clfkqvq7dqx51ng0a3hspsknxrhfbgjr7id4zyfl7za3z")))

(define-public crate-laby_common-0.2.0 (c (n "laby_common") (v "0.2.0") (d (list (d (n "itoap") (r "^1") (d #t) (k 0)) (d (n "raw-cpuid") (r "^10") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)))) (h "0y5s65d82n7in3838yilhrr9229biq3hjipw0yi2l2dfd2hyqxaj")))

