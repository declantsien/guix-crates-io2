(define-module (crates-io la by labyrinth_macros) #:use-module (crates-io))

(define-public crate-labyrinth_macros-1.0.0 (c (n "labyrinth_macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "1i1yc000rxzaryk06jpz9bgn71pjd2kf979vb4l4bs9mx1sw67c4")))

(define-public crate-labyrinth_macros-2.0.0 (c (n "labyrinth_macros") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "190zg4lh9gyjrrhbs3n6kxdjx6l8i650ap2ypn2lj8dzhjbl56m4")))

(define-public crate-labyrinth_macros-3.0.0 (c (n "labyrinth_macros") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)))) (h "0xb64cjpsl8x7ai056bif4d67jhnravc8ri9kpr5g2x6fmydxrd1")))

