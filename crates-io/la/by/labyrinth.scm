(define-module (crates-io la by labyrinth) #:use-module (crates-io))

(define-public crate-labyrinth-0.0.0 (c (n "labyrinth") (v "0.0.0") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1m4dpax085kfzvl73j2aqylnlf0d4dw3l96l0rr9mfgianpz6r9b") (f (quote (("default"))))))

