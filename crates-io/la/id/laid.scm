(define-module (crates-io la id laid) #:use-module (crates-io))

(define-public crate-laid-0.0.0 (c (n "laid") (v "0.0.0") (d (list (d (n "glam") (r "^0.8") (d #t) (k 0)) (d (n "miniquad") (r "^0.2") (d #t) (k 0)) (d (n "miniquad_text_rusttype") (r "^0.1.2") (d #t) (k 0)))) (h "0szl6spjcf2imc7r0mcjklfv6h18ql0grv6llkfl27qf3cks31lr")))

