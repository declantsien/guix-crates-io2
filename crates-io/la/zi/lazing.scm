(define-module (crates-io la zi lazing) #:use-module (crates-io))

(define-public crate-lazing-0.1.0 (c (n "lazing") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0rc6k637xi3pzivj22d4rx288d691grjhs2rvzi8r7iyh7k9vvb3")))

(define-public crate-lazing-0.1.1 (c (n "lazing") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17fxj24vsi1hyxzgghk4yjhlgcqcjl1sr9hdm6l8ypjb95sr567l")))

