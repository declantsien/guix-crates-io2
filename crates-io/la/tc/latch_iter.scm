(define-module (crates-io la tc latch_iter) #:use-module (crates-io))

(define-public crate-latch_iter-0.1.0 (c (n "latch_iter") (v "0.1.0") (h "0ma8fqaxkh5bkw6wpdp9ka23rl28a886ignpmdwpnl7nl9i08z78")))

(define-public crate-latch_iter-0.1.1 (c (n "latch_iter") (v "0.1.1") (h "12p2xabfhcq2r0hyqznlwgbqwy8fnpv6kw8x6b1nj5cmjw56bzdz")))

