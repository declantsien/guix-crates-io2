(define-module (crates-io la tc latches) #:use-module (crates-io))

(define-public crate-latches-0.1.0 (c (n "latches") (v "0.1.0") (d (list (d (n "atomic-wait") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0nqnx6l1lfl4pbbhmfmkygigb2cnbz5pszpwxdjw4qiy94cfzaka") (f (quote (("task") ("sync") ("std") ("futex" "atomic-wait") ("default" "sync" "atomic-wait")))) (y #t) (s 2) (e (quote (("atomic-wait" "dep:atomic-wait"))))))

(define-public crate-latches-0.1.1 (c (n "latches") (v "0.1.1") (d (list (d (n "atomic-wait") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0jh99m1wz6w8lxzgr4mghpa891dd05xfdzlybzmh8gg4jyhd893f") (f (quote (("task") ("sync") ("std") ("futex" "atomic-wait") ("default" "sync" "atomic-wait")))) (s 2) (e (quote (("atomic-wait" "dep:atomic-wait"))))))

(define-public crate-latches-0.1.2 (c (n "latches") (v "0.1.2") (d (list (d (n "atomic-wait") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "15b7wcnd60xpi0x4n5hglgjl6z2i643945zadk318isdvdpcj44c") (f (quote (("task") ("sync") ("std") ("futex" "atomic-wait") ("default" "sync" "atomic-wait")))) (s 2) (e (quote (("atomic-wait" "dep:atomic-wait"))))))

(define-public crate-latches-0.1.3 (c (n "latches") (v "0.1.3") (d (list (d (n "atomic-wait") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "137rvds91p7jn11mr25papafhwz1l396hih3fcbccpzivfpf488r") (f (quote (("task") ("sync") ("std") ("futex" "atomic-wait") ("default" "sync" "atomic-wait")))) (s 2) (e (quote (("atomic-wait" "dep:atomic-wait"))))))

(define-public crate-latches-0.2.0 (c (n "latches") (v "0.2.0") (d (list (d (n "atomic-wait") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "013j5dpp4pz6zk69lw4np1xjg5hxk18pl2kfd8k72m79wi647ci8") (f (quote (("task") ("sync") ("std") ("futex" "atomic-wait") ("default" "sync" "atomic-wait")))) (s 2) (e (quote (("atomic-wait" "dep:atomic-wait")))) (r "1.63")))

