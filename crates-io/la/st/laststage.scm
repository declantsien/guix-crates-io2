(define-module (crates-io la st laststage) #:use-module (crates-io))

(define-public crate-laststage-0.1.0 (c (n "laststage") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "hashring") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("sync" "macros" "rt-multi-thread" "time"))) (d #t) (k 0)))) (h "0hjvxxb74k3v0r2gmvai4h8l3z0218xznj6h4iccv9mfva1qp24x")))

(define-public crate-laststage-0.1.1 (c (n "laststage") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "hashring") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("sync" "macros" "rt-multi-thread" "time"))) (d #t) (k 0)))) (h "130pifqv270d6mrlr3dpmmrd5cj3hixmh8z31hkih2mxfpzmawka")))

(define-public crate-laststage-1.0.0 (c (n "laststage") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("sync" "macros" "rt-multi-thread" "time"))) (d #t) (k 0)))) (h "0jc5i0kxm407j7fkx6vpdj0ihjmxcm31qfa33ci43glgih5vaiqi")))

