(define-module (crates-io la st last-git-commit) #:use-module (crates-io))

(define-public crate-last-git-commit-0.1.0 (c (n "last-git-commit") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "git2") (r "^0.9.1") (d #t) (k 0)))) (h "1b6jqmryabqghs7dj0fv7i0j5viplgaa0zw2b2ghl0arbw9gqw50")))

(define-public crate-last-git-commit-0.1.1 (c (n "last-git-commit") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "git2") (r "^0.9.1") (d #t) (k 0)))) (h "0b34j0bhzfyzxa81v2cp4fznl904r3ivxp2y47f1ccpj6wa714ry")))

(define-public crate-last-git-commit-0.2.0 (c (n "last-git-commit") (v "0.2.0") (d (list (d (n "git2") (r "^0.13") (d #t) (k 0)))) (h "1h493k62pb8ykplkk3qyazr0j1bvm5g6zzbr7m2a0bjv711m4blz")))

