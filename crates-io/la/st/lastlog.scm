(define-module (crates-io la st lastlog) #:use-module (crates-io))

(define-public crate-lastlog-0.1.0 (c (n "lastlog") (v "0.1.0") (d (list (d (n "cached") (r "^0.40.0") (d #t) (k 0)))) (h "06wz2az3fnxq4gxr8hdyb15j9zzh8nkxr3nhkmgcyzs6ma0xskwj")))

(define-public crate-lastlog-0.2.0 (c (n "lastlog") (v "0.2.0") (d (list (d (n "cached") (r "^0.40.0") (d #t) (k 0)))) (h "113waywd0j27g775dnnvf850aq3m1viifbd79rxfk9mjraf32030")))

(define-public crate-lastlog-0.2.1 (c (n "lastlog") (v "0.2.1") (d (list (d (n "cached") (r "^0.40.0") (d #t) (k 0)))) (h "1hcinandrviwa5klw2zmwysmv830dir4fw5iazzwmbmc8fr2irc0")))

(define-public crate-lastlog-0.2.2 (c (n "lastlog") (v "0.2.2") (d (list (d (n "cached") (r "^0.40.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (o #t) (d #t) (k 0)))) (h "1azcl2zzfm1bfcdhg59yp0rdn0y76axxfxp8yrrbqi4gz3sfrmn9") (s 2) (e (quote (("libc" "dep:libc"))))))

(define-public crate-lastlog-0.2.3 (c (n "lastlog") (v "0.2.3") (d (list (d (n "cached") (r "^0.40.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (o #t) (d #t) (k 0)))) (h "1lv9j9fr16mppdvdbwspgjhjp008c9wf91ir70vwr6rxidhj1mkc") (s 2) (e (quote (("libc" "dep:libc"))))))

