(define-module (crates-io la st last_order_logic) #:use-module (crates-io))

(define-public crate-last_order_logic-0.1.0 (c (n "last_order_logic") (v "0.1.0") (h "0l0kb1as3h344wfhir00jji64h9g01yjhdh7bv1v261ipz1sp9wc")))

(define-public crate-last_order_logic-0.2.0 (c (n "last_order_logic") (v "0.2.0") (d (list (d (n "piston_meta") (r "^2.0.1") (d #t) (k 0)))) (h "1cqrcivql7vkszn4an4v0fy64ys9hzblj2iphv4qrji1w652rqi0")))

