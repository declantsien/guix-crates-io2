(define-module (crates-io la st last-update-time) #:use-module (crates-io))

(define-public crate-last-update-time-0.1.0 (c (n "last-update-time") (v "0.1.0") (h "139fzzrk8nhqcb4fgkr09kvh7j902mswgfxh59bg53glbby5fa8i")))

(define-public crate-last-update-time-0.1.1 (c (n "last-update-time") (v "0.1.1") (h "0jl14m055vjx80dn85gs4ad54jid02icsmv6hld1cm9fl6pwpb4v")))

