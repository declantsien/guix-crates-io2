(define-module (crates-io la st last-rs) #:use-module (crates-io))

(define-public crate-last-rs-0.2.0 (c (n "last-rs") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "time") (r "^0.3.14") (f (quote ("formatting" "macros" "local-offset"))) (d #t) (k 0)) (d (n "utmp-rs") (r "^0.3.0") (d #t) (k 0)))) (h "0xs2wf5qaxvw5kv6in4z059j55yvc7sa74r9lkjznv0hnyk9ivd2")))

(define-public crate-last-rs-0.2.1 (c (n "last-rs") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "time") (r "^0.3.14") (f (quote ("formatting" "macros" "local-offset"))) (d #t) (k 0)) (d (n "utmp-rs") (r "^0.3.0") (d #t) (k 0)))) (h "0dcfj7acxbi946w04lj8qbpn16bxa8hy5y38b3242dvjlr3m6wyb")))

