(define-module (crates-io la st lasttime) #:use-module (crates-io))

(define-public crate-lasttime-0.1.0 (c (n "lasttime") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha256") (r "^1.1.3") (d #t) (k 0)))) (h "1sabbqxb3yphd1nn57r25mb56208270hj57vh1l6rirc493rszdz")))

