(define-module (crates-io la sp laspa) #:use-module (crates-io))

(define-public crate-laspa-0.1.0 (c (n "laspa") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "regex") (r "^1.9.3") (k 0)))) (h "1a3s0ay5gh0y46rbfdvclzrhlbzld07symyd93vypzc078kviaj1")))

(define-public crate-laspa-0.1.1 (c (n "laspa") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "regex") (r "^1.9.3") (k 0)))) (h "00iq89m7d8f0qwk5xmayvamp30dnq9l2zr39bciwxfkqhq61ji7i")))

(define-public crate-laspa-0.2.0 (c (n "laspa") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "regex") (r "^1.9.3") (k 0)))) (h "1gdcqsa4ci973k1gnd0v10rh42js6796pflaxkk6sbyhjhla11kj")))

(define-public crate-laspa-0.3.0 (c (n "laspa") (v "0.3.0") (d (list (d (n "clap") (r "^4.3.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.6") (d #t) (k 0)) (d (n "inkwell") (r "^0.2.0") (f (quote ("llvm16-0"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (k 0)))) (h "1wcz71a4zkxmm626mbgsmpjfykd73nkk9mq89vrdp6krh75ybz30")))

