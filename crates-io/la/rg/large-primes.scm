(define-module (crates-io la rg large-primes) #:use-module (crates-io))

(define-public crate-large-primes-0.1.0 (c (n "large-primes") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_off"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0bx3xmm30g0lng50g6haqw48j4yc194x77yyi3yk788aks1s0b8b")))

(define-public crate-large-primes-0.2.0 (c (n "large-primes") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_off"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0v72y2iw0vgghayjsvkwp285z1d08v9k4572fdp8czznkm3dg3by")))

(define-public crate-large-primes-0.3.0 (c (n "large-primes") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_off"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "01g2p7c02znb2f47drpyqx16iq94zmms7nzf6pdci63v93z3qpx6")))

(define-public crate-large-primes-0.4.0 (c (n "large-primes") (v "0.4.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_off"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "074x89lbj77b8d5hfyk7qx1z41cna1k5cam438dr3krz20aqqjfb")))

(define-public crate-large-primes-0.5.0 (c (n "large-primes") (v "0.5.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_off"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0vf0c369mx3060jk7d3gwlr7jwlpwr0dscc6dx6xm9hf4xaz1mmx")))

(define-public crate-large-primes-0.5.1 (c (n "large-primes") (v "0.5.1") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("release_max_level_off"))) (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "1snj7fmh0raj99bbbc9s4iaxw6rih1y1yncs1f96q12cl9nd5wfh")))

