(define-module (crates-io la rg largest-remainder-method) #:use-module (crates-io))

(define-public crate-largest-remainder-method-0.1.0 (c (n "largest-remainder-method") (v "0.1.0") (h "0mrcrwvj7bl66d337m4z4xc9k7llal2cvhnzv4zlmlf1j6l83jfq")))

(define-public crate-largest-remainder-method-0.1.1 (c (n "largest-remainder-method") (v "0.1.1") (h "070xc5faq6387yawma2klnwqd2bwn3wvvm0yr8rqnyks6x7vaswj")))

