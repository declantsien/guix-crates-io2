(define-module (crates-io la rg largo) #:use-module (crates-io))

(define-public crate-largo-0.0.1 (c (n "largo") (v "0.0.1") (h "1g3l30caa00n67cnlzcrw83c507wdmynaq28fgpp2nrby3h1sj69")))

(define-public crate-largo-0.0.2 (c (n "largo") (v "0.0.2") (d (list (d (n "chrono") (r "0.2.*") (d #t) (k 0)) (d (n "dotenv") (r "0.8.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "1f3wbfklz0malm0zbi2iha8kkv18x7m4ga6l4zqlpwy2kh8kzrcj")))

