(define-module (crates-io la rg largeint) #:use-module (crates-io))

(define-public crate-largeint-0.1.0 (c (n "largeint") (v "0.1.0") (h "07rk87v5aarm4xj353ydc8nifvknxbl3plw5l4v14zf7qb8pxxn0")))

(define-public crate-largeint-0.2.0 (c (n "largeint") (v "0.2.0") (h "0z03l9piqv1vsg29n1x4k0c8mgjsn9vk3c0hjmiaqhighq6dirlj")))

