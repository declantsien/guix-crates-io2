(define-module (crates-io la rg large_int) #:use-module (crates-io))

(define-public crate-large_int-1.0.0 (c (n "large_int") (v "1.0.0") (h "1zzj3byaf6ixcibp2na9br343avwps1bxnbjbvhhikmbbn6y7lha") (y #t)))

(define-public crate-large_int-0.1.0 (c (n "large_int") (v "0.1.0") (h "0zzsxyq1i39rkkzfnzjrsynb86vbv6y54wmnicyfdkqhlp9jii74")))

(define-public crate-large_int-0.2.0 (c (n "large_int") (v "0.2.0") (h "02il1sl0fqsixpyr5mz3vcq85ypcdgp285wm9xx9aa589mppf5dd")))

(define-public crate-large_int-0.2.1 (c (n "large_int") (v "0.2.1") (h "0z5ypssg1ibgx4l8am713kcy9yz43nhd35npz3vmn71kv8y6dcnd")))

(define-public crate-large_int-0.2.2 (c (n "large_int") (v "0.2.2") (h "1jq2ycb33j9fc23mkkgbiibb550x8463ar5za4xswwvgdk9m79b9")))

