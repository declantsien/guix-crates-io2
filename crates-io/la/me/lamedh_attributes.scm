(define-module (crates-io la me lamedh_attributes) #:use-module (crates-io))

(define-public crate-lamedh_attributes-0.3.0 (c (n "lamedh_attributes") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)))) (h "1wnl4kx7aiq834fawjwnh60rclysbn9w00vhvgva1hchxl848wq9")))

