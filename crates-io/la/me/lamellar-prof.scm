(define-module (crates-io la me lamellar-prof) #:use-module (crates-io))

(define-public crate-lamellar-prof-0.1.0 (c (n "lamellar-prof") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)))) (h "0hhc311bxgzx7rv96svh2qv47gm6lirg3g6ws708mkh18m4wihls") (f (quote (("enable-prof"))))))

