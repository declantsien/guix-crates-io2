(define-module (crates-io la me lame) #:use-module (crates-io))

(define-public crate-lame-0.1.0 (c (n "lame") (v "0.1.0") (h "08xlrk2kcz760yc609gjp93gm3zs34v04ryrqy2xvbbzvgw3ba78")))

(define-public crate-lame-0.1.1 (c (n "lame") (v "0.1.1") (h "1nkx8jdpp5zhsbb4kvk62362cxq29gzgvls5q90wk34s66dgm505")))

(define-public crate-lame-0.1.2 (c (n "lame") (v "0.1.2") (h "1hspaf45rrmbc1nc35mbvkv0kj0w7qpg0lbh8kqjrcdnhmmb19l9")))

(define-public crate-lame-0.1.3 (c (n "lame") (v "0.1.3") (h "0bq0dz3ir7azk8wva5843mjvrfcvl6kl70fz4n1f8lf465vypyiw")))

