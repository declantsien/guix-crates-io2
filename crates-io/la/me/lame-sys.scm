(define-module (crates-io la me lame-sys) #:use-module (crates-io))

(define-public crate-lame-sys-0.1.0 (c (n "lame-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.50") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "target_info") (r "^0.1.0") (o #t) (d #t) (k 1)))) (h "06d649fznk5sghm2scl9hll6fmh1n13mm52nj167lfn8lxsphbs6") (f (quote (("default") ("bundled" "gcc" "target_info"))))))

(define-public crate-lame-sys-0.1.1 (c (n "lame-sys") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3.50") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "target_info") (r "^0.1.0") (o #t) (d #t) (k 1)))) (h "13kahyfmj8vmp9ks98jd5q729xpjspwwhjdnkfrrmiy3p109xzb2") (f (quote (("default") ("bundled" "gcc" "target_info")))) (y #t)))

(define-public crate-lame-sys-0.1.2 (c (n "lame-sys") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3.50") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "target_info") (r "^0.1.0") (o #t) (d #t) (k 1)))) (h "13z3qy240kd1yymk3q9g2wyr7jmdrhkf8djzycyhbfy9kdd8cypg") (f (quote (("default") ("bundled" "gcc" "target_info"))))))

