(define-module (crates-io la gr lagraph) #:use-module (crates-io))

(define-public crate-lagraph-0.1.0 (c (n "lagraph") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.5") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)))) (h "1ybzk4vlrdnzpag9n6shrwhf24m7dabbambcj9ql6ravqbal4gsi")))

(define-public crate-lagraph-0.2.0 (c (n "lagraph") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.5") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)))) (h "0fhqs53xlyqhakdh1ycc1h2vaz61x0bxnc5haignddbzwlmjkapc")))

(define-public crate-lagraph-0.2.1 (c (n "lagraph") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.8") (d #t) (k 0)))) (h "1f6kwmx4lliydf10v6jd6xqs695lhnk2xmv0fcpk25497hj4lbkq")))

