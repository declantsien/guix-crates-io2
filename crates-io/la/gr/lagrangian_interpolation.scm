(define-module (crates-io la gr lagrangian_interpolation) #:use-module (crates-io))

(define-public crate-lagrangian_interpolation-0.1.0 (c (n "lagrangian_interpolation") (v "0.1.0") (h "1fk0liq4wq7x8kd88l86b1xr40y3fg8hkzd7ijkv9ahcs4nlw4ai")))

(define-public crate-lagrangian_interpolation-0.1.1 (c (n "lagrangian_interpolation") (v "0.1.1") (h "1ch5rq1c8gs924rjnapsx3j6b4vkr2x3l095vybph8psh63gl4pq")))

