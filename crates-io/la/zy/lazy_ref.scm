(define-module (crates-io la zy lazy_ref) #:use-module (crates-io))

(define-public crate-lazy_ref-0.1.0 (c (n "lazy_ref") (v "0.1.0") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 2)))) (h "0fyby2238gng6wn0dw5mhgbbkjn7c96d736h5sqm1flxmgf8w0wx") (y #t)))

(define-public crate-lazy_ref-0.2.0 (c (n "lazy_ref") (v "0.2.0") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 2)))) (h "06kx88zhdkaq5jx6zicvcp5ja3n3j0qlcp9q0i64px9wkd6sz0ga")))

(define-public crate-lazy_ref-0.3.0 (c (n "lazy_ref") (v "0.3.0") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 2)))) (h "13jsmvhbpqlywyhpafnys52ss3dhrrlb9ss8954cdfhhrs2q2vvp")))

(define-public crate-lazy_ref-0.4.0 (c (n "lazy_ref") (v "0.4.0") (d (list (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 2)))) (h "0hz9jzmpl6h4r47cx2ajdkzij5vhc8ri6k9h4vsnvadyg6n6lcyx")))

