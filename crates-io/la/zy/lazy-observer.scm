(define-module (crates-io la zy lazy-observer) #:use-module (crates-io))

(define-public crate-lazy-observer-0.1.0 (c (n "lazy-observer") (v "0.1.0") (h "05dhql0lq2l1fklxkh0pb4r5wrxpbbbp6mm224g65gzhyzqadgwj")))

(define-public crate-lazy-observer-0.3.0 (c (n "lazy-observer") (v "0.3.0") (h "066dw4ah8lamkq0sp25bpa96sqv9xl1kayjjhbwpdc92ixbq1ma6")))

