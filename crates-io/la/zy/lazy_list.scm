(define-module (crates-io la zy lazy_list) #:use-module (crates-io))

(define-public crate-lazy_list-0.1.0 (c (n "lazy_list") (v "0.1.0") (d (list (d (n "once_cell") (r "~1.19.0") (d #t) (k 2)))) (h "1lcpwrb77p0i1vqvzf4mj48j9qs5hqh137x5fnk9nakb1qfk4r2n")))

(define-public crate-lazy_list-0.1.1 (c (n "lazy_list") (v "0.1.1") (d (list (d (n "criterion") (r "~0.5.1") (d #t) (k 2)) (d (n "once_cell") (r "~1.19.0") (d #t) (k 2)))) (h "11ihhfwmkhd3rmam3gipaz5z2b84g9hxasikqpkpwahkgr6xp2hi")))

