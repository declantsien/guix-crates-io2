(define-module (crates-io la zy lazyonce) #:use-module (crates-io))

(define-public crate-lazyonce-0.2.0 (c (n "lazyonce") (v "0.2.0") (h "18an8fbfd0sc84fccnlfq1vi47a3dls4zfwfc6lmy1089mdg8777") (y #t)))

(define-public crate-lazyonce-0.3.0 (c (n "lazyonce") (v "0.3.0") (h "0zgz2zxahvnayhyz9g9jvbpigkv17zickcz0ay00jd10vg3f8gdm") (y #t)))

(define-public crate-lazyonce-1.0.0 (c (n "lazyonce") (v "1.0.0") (d (list (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "1w3d60v1cwj8ijbl1w7bnpjlzyar7xcbbyc2aib9rgqny785v1b9") (y #t)))

(define-public crate-lazyonce-2.0.0 (c (n "lazyonce") (v "2.0.0") (d (list (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)))) (h "03pqalk4gj6nmqcpm9dyivr09hqxzn9drdkl6jcqvklazhpmspfa") (y #t)))

(define-public crate-lazyonce-2.0.1 (c (n "lazyonce") (v "2.0.1") (d (list (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)))) (h "1k0q9xjsmnhy9mf8s68qwnwam9sqwjijgf749gfwydh5fzihnib8") (y #t)))

