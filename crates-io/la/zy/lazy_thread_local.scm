(define-module (crates-io la zy lazy_thread_local) #:use-module (crates-io))

(define-public crate-lazy_thread_local-0.1.0 (c (n "lazy_thread_local") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_family=\"unix\")") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("fibersapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "10f7cdnmlarv203wc11cfmhpyv4ydpma3a43y8zzrdik5khkk0n9") (y #t)))

(define-public crate-lazy_thread_local-0.1.1 (c (n "lazy_thread_local") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_family=\"unix\")") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("fibersapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0gjwjs0fv7spsvxhxz2096bc6cf4lfw6d082030s71xs3zgxl7iv")))

(define-public crate-lazy_thread_local-0.1.2 (c (n "lazy_thread_local") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_family=\"unix\")") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("fibersapi"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0vwhl20gapxd5r9y82b143dlh0v9nq6jgkphag6kmf63jgdbcqrr")))

