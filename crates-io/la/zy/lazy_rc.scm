(define-module (crates-io la zy lazy_rc) #:use-module (crates-io))

(define-public crate-lazy_rc-0.1.0 (c (n "lazy_rc") (v "0.1.0") (h "1dfqkzgyv846bix65qd49lz307ry0jfa2nar9nj93p6dhxwhgbla")))

(define-public crate-lazy_rc-0.1.1 (c (n "lazy_rc") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0qn4r3lym8djvcnycc3a4a5bz2f1vz146bvdwydw7xgsia5776fj")))

(define-public crate-lazy_rc-0.1.2 (c (n "lazy_rc") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1hfcsq96i1s9xj0gvl8sy254959ksd1091yzix5n1dwzdxyhifmb")))

(define-public crate-lazy_rc-0.1.3 (c (n "lazy_rc") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "01dkwdlgyhi63kv3r83j7snx2d61ccn7aksyqlq3xx1rk0qb8y2l")))

