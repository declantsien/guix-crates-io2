(define-module (crates-io la zy lazy) #:use-module (crates-io))

(define-public crate-lazy-0.3.0 (c (n "lazy") (v "0.3.0") (d (list (d (n "stainless") (r "*") (d #t) (k 0)))) (h "13fq3d52l61q1nckl4vs42r8njp618lgzswhmdk9qnmb8hbrv11d")))

(define-public crate-lazy-0.3.1 (c (n "lazy") (v "0.3.1") (d (list (d (n "stainless") (r "*") (d #t) (k 0)))) (h "0bpm0bh1n0w983zyn5z74nfad4wi1yzgpmbaqqkzl7iwb5i3dv09")))

(define-public crate-lazy-0.3.2 (c (n "lazy") (v "0.3.2") (d (list (d (n "stainless") (r "*") (d #t) (k 0)))) (h "1yljpq22pgbyz1achdkapk9h9a0qciymnw907p1jhi1i452n1vjg")))

(define-public crate-lazy-0.4.0 (c (n "lazy") (v "0.4.0") (d (list (d (n "debug_unreachable") (r "*") (d #t) (k 0)) (d (n "oncemutex") (r "*") (d #t) (k 0)) (d (n "stainless") (r "*") (d #t) (k 2)))) (h "1rw1xb1rifwmzqsk0a3ncbvqn9rv2sw4hh6fciyhx12lxr06fgja")))

(define-public crate-lazy-0.4.1 (c (n "lazy") (v "0.4.1") (d (list (d (n "debug_unreachable") (r "*") (d #t) (k 0)) (d (n "oncemutex") (r "*") (d #t) (k 0)) (d (n "stainless") (r "*") (d #t) (k 2)))) (h "0czwdsj2qs1y6b77q16zg0ql928288hbcz7iksm55gm4q1xx06xy")))

(define-public crate-lazy-0.5.0 (c (n "lazy") (v "0.5.0") (d (list (d (n "debug_unreachable") (r "*") (d #t) (k 0)) (d (n "oncemutex") (r "*") (d #t) (k 0)) (d (n "stainless") (r "*") (d #t) (k 2)))) (h "1c1qar2p0kpdwbagd59zb5z6b74bcc65xjpxwy9vxjz2kwyhz93h")))

(define-public crate-lazy-0.5.1 (c (n "lazy") (v "0.5.1") (d (list (d (n "debug_unreachable") (r "*") (d #t) (k 0)) (d (n "oncemutex") (r "*") (d #t) (k 0)) (d (n "stainless") (r "*") (d #t) (k 2)))) (h "0m2p760yckvrcin3dfdjzpwb2wac2nkbr2dp17w3kd8jqfm7mf8c")))

(define-public crate-lazy-0.5.2 (c (n "lazy") (v "0.5.2") (d (list (d (n "debug_unreachable") (r "*") (d #t) (k 0)) (d (n "oncemutex") (r "*") (d #t) (k 0)) (d (n "stainless") (r "*") (d #t) (k 2)))) (h "08wv8bd8rr9dg8vckblha87k777qakigqz10ixvvwyb6d3zmsjzn")))

(define-public crate-lazy-0.5.3 (c (n "lazy") (v "0.5.3") (d (list (d (n "debug_unreachable") (r "*") (d #t) (k 0)) (d (n "oncemutex") (r "*") (d #t) (k 0)) (d (n "stainless") (r "*") (d #t) (k 2)))) (h "13w82wl8khsxwkmizdj1h1dd0j30gl5w5zaqqvy4a7sg8nwy68pm")))

