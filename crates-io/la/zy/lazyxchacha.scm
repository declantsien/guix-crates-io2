(define-module (crates-io la zy lazyxchacha) #:use-module (crates-io))

(define-public crate-lazyxchacha-0.1.0 (c (n "lazyxchacha") (v "0.1.0") (d (list (d (n "chacha20poly1305") (r "^0.10.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "faster-hex") (r "^0.9.0") (d #t) (k 0)) (d (n "x25519-dalek") (r "^2.0.1") (f (quote ("getrandom"))) (d #t) (k 0)))) (h "1fjb1kd9jyqpn4m7py1854jxmmy2ql941wq8vqs4cjs4vzsv5g45")))

(define-public crate-lazyxchacha-0.1.1 (c (n "lazyxchacha") (v "0.1.1") (d (list (d (n "chacha20poly1305") (r "^0.10.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "faster-hex") (r "^0.9.0") (d #t) (k 0)) (d (n "x25519-dalek") (r "^2.0.1") (f (quote ("getrandom"))) (d #t) (k 0)))) (h "158w0knjp2qq3xj65ga0nmnv59cva9xfyg256x991pymjzn5ac65")))

