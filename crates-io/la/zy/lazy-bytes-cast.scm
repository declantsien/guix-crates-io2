(define-module (crates-io la zy lazy-bytes-cast) #:use-module (crates-io))

(define-public crate-lazy-bytes-cast-0.1.0 (c (n "lazy-bytes-cast") (v "0.1.0") (h "10x1hfjgqgw1m8hkzs18mi3l6sfjhjyca6zvxypp49zifdzd1r6z")))

(define-public crate-lazy-bytes-cast-0.1.1 (c (n "lazy-bytes-cast") (v "0.1.1") (h "1p8skizwal6lg7qpghly5rinbkacdb4d4xp3hj63j80n90ivisay")))

(define-public crate-lazy-bytes-cast-0.1.2 (c (n "lazy-bytes-cast") (v "0.1.2") (h "1h85kifa22nf9n03njbn246rjckrqjw1lv73lnd3lzkz0vsc0msr")))

(define-public crate-lazy-bytes-cast-0.2.0 (c (n "lazy-bytes-cast") (v "0.2.0") (h "0945rcagsv0fyjci6s4qpjfsdqwh1zjw4vpqdnwdmb5fid4f6kd6")))

(define-public crate-lazy-bytes-cast-0.3.0 (c (n "lazy-bytes-cast") (v "0.3.0") (h "0pzlzann3h128907mch82hypga07wja11s05sgk0mjcykx4r2ycy")))

(define-public crate-lazy-bytes-cast-0.4.0 (c (n "lazy-bytes-cast") (v "0.4.0") (h "0kc8bdh0j113rwbnz8nzrap7kcw0pccas7xz8lnfnjmmq4gzk2fv")))

(define-public crate-lazy-bytes-cast-0.5.0 (c (n "lazy-bytes-cast") (v "0.5.0") (h "1yx0zhkfgaz165646mfsjig4hdg8gr87vz4prj48j22fn07krkm9")))

(define-public crate-lazy-bytes-cast-0.5.1 (c (n "lazy-bytes-cast") (v "0.5.1") (h "09spr9a5349gsyjc9fqj8afqg4q9b3pa3ghnzmvjb8216xk9hcgq")))

(define-public crate-lazy-bytes-cast-1.0.0 (c (n "lazy-bytes-cast") (v "1.0.0") (h "02ryf9hmhb3g2fmvf1xms0pj572n3ha8yrn61lajpwp382vhg9lh") (f (quote (("std") ("default" "std"))))))

(define-public crate-lazy-bytes-cast-2.0.0 (c (n "lazy-bytes-cast") (v "2.0.0") (h "1w2r0vk3s7c9mnygahzc8j7rc3984ni78qhgcgxm0577rrl2xc81") (f (quote (("std") ("default" "std"))))))

(define-public crate-lazy-bytes-cast-2.1.0 (c (n "lazy-bytes-cast") (v "2.1.0") (h "02ywqdyiy8h4v4vax11s3g9lcjxzdypidb0wcyzd2njvclqk7kd9") (f (quote (("std") ("default" "std"))))))

(define-public crate-lazy-bytes-cast-2.2.0 (c (n "lazy-bytes-cast") (v "2.2.0") (h "1ilmnqyrm6wyha86w85d7890z76mmfy4rik167jpc9srf7y90ymz") (f (quote (("std") ("default" "std"))))))

(define-public crate-lazy-bytes-cast-2.3.0 (c (n "lazy-bytes-cast") (v "2.3.0") (h "0vv4pj6igd34n984hp725jva8qfsb2waycg047ky6s1cs3bxyd5x") (f (quote (("std") ("default" "std"))))))

(define-public crate-lazy-bytes-cast-3.0.0 (c (n "lazy-bytes-cast") (v "3.0.0") (h "10lr9gzic8nn8mjg0f1bhzvj93xzz4qxj688mnyc0pdf8a7l2pf2") (y #t)))

(define-public crate-lazy-bytes-cast-4.0.0 (c (n "lazy-bytes-cast") (v "4.0.0") (h "00v4qlxikkl7ib68w4disf8q1hjlc39d1clrfm9irxs9dh0pc0bw")))

(define-public crate-lazy-bytes-cast-5.0.0 (c (n "lazy-bytes-cast") (v "5.0.0") (h "0gvbcrzkjnwsj1s8m742nmi9g0xg75dwb8x01bw9hqbqraa6ncwi")))

(define-public crate-lazy-bytes-cast-5.0.1 (c (n "lazy-bytes-cast") (v "5.0.1") (h "0sr0dy1jfg7bjwm9js4hk0ngl0cmgparq2idv1m1bkc9y2cp898h")))

(define-public crate-lazy-bytes-cast-6.0.0 (c (n "lazy-bytes-cast") (v "6.0.0") (h "1dxj63h6w6bqjxg7h6sjgg4mi68ja4gd4w5jr30c99ajz3z6wg51")))

(define-public crate-lazy-bytes-cast-6.0.1 (c (n "lazy-bytes-cast") (v "6.0.1") (h "0s320205wb25l6i3z6zs7wzsd3cs7609r9lq3hznpjli4q2nxvk2")))

(define-public crate-lazy-bytes-cast-7.0.0 (c (n "lazy-bytes-cast") (v "7.0.0") (h "1wa8jzqw6810cmzkmgs405djk0bi9b985127r28xk9bhkxyd0lvk")))

