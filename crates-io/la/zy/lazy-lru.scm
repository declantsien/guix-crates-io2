(define-module (crates-io la zy lazy-lru) #:use-module (crates-io))

(define-public crate-lazy-lru-0.1.0 (c (n "lazy-lru") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "lru") (r "^0.12.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "01ddvvvxqz0k3r9cx5lhxlg6r52lkr27nkg2picf8hx0qscj67pb")))

(define-public crate-lazy-lru-0.1.1 (c (n "lazy-lru") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "lru") (r "^0.12.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0arl2fzhw20z5dcmi2a62bh8alnq3hv45n0glc3cwm4mqyk3q6q8")))

(define-public crate-lazy-lru-0.1.2 (c (n "lazy-lru") (v "0.1.2") (d (list (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "lru") (r "^0.12.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1wgflfdpk243xjbrj6mab3vyxifmnfxigv9qx4wdzwvn2ay366zq")))

