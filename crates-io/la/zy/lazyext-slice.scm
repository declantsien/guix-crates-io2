(define-module (crates-io la zy lazyext-slice) #:use-module (crates-io))

(define-public crate-lazyext-slice-0.0.1 (c (n "lazyext-slice") (v "0.0.1") (d (list (d (n "bytes") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "17mb141n7z5higmzjqcd53z463z5svhl1yri3cpgq2ldgdyz8lwi") (f (quote (("std" "alloc") ("full" "std" "bytes") ("default" "full") ("alloc"))))))

(define-public crate-lazyext-slice-0.0.2 (c (n "lazyext-slice") (v "0.0.2") (d (list (d (n "bytes") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1zsgbsw9cbylnx0537kg1v1nk9zz0gmq5ds3k75jrmid84xikazc") (f (quote (("std" "alloc") ("full" "std" "bytes") ("default" "full") ("alloc"))))))

