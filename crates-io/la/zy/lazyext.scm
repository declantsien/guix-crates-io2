(define-module (crates-io la zy lazyext) #:use-module (crates-io))

(define-public crate-lazyext-0.0.1 (c (n "lazyext") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1iypdzaxq8l1fjmya33cri2din83a316jsw30a7qvr5iqcfrl8h0") (f (quote (("default"))))))

(define-public crate-lazyext-0.0.3 (c (n "lazyext") (v "0.0.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazyext-slice") (r "^0.0.2") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1zrr3n89k59b6qx0k53xa10mjjgkdggjwkwgnsbqzakgqqnzwx3w") (f (quote (("std" "lazyext-slice/std") ("full" "std") ("default" "full"))))))

