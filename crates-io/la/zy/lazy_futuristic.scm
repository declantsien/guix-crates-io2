(define-module (crates-io la zy lazy_futuristic) #:use-module (crates-io))

(define-public crate-lazy_futuristic-1.0.0 (c (n "lazy_futuristic") (v "1.0.0") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 2)))) (h "0q7b57x0aq5cq4w7ww8b4b4b2r04il56cca5s0k9sk9r9ipc0pj4")))

(define-public crate-lazy_futuristic-1.0.1 (c (n "lazy_futuristic") (v "1.0.1") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 2)))) (h "08q4frqx692krw00mv8jvkyvgfw4qa24q59x89xj3fvhkdhkk0ny")))

(define-public crate-lazy_futuristic-1.0.2 (c (n "lazy_futuristic") (v "1.0.2") (d (list (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 2)))) (h "171rn0n6dlsqznxb4q3xmd8id9dwiximwm90s51ihf1w7i04gjpx")))

