(define-module (crates-io la zy lazy-heap) #:use-module (crates-io))

(define-public crate-lazy-heap-0.1.0-alpha.1 (c (n "lazy-heap") (v "0.1.0-alpha.1") (d (list (d (n "slab_allocator_rs") (r "^1.0.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "10lbyh02nhxvspd3iygn2lfhrzqbjqzk2xc1sc0y85kg1r6l6zqa")))

(define-public crate-lazy-heap-0.1.1-alpha.1 (c (n "lazy-heap") (v "0.1.1-alpha.1") (d (list (d (n "slab_allocator_rs") (r "^1.0.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "09bgy3rlkpl3wsvvkhn53a696x9yznvhjd5xj859m1d97208gva4")))

(define-public crate-lazy-heap-0.1.1-alpha.2 (c (n "lazy-heap") (v "0.1.1-alpha.2") (d (list (d (n "slab_allocator_rs") (r "^1.0.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "07afbnacafxhznpc74js9b57a31842vsrwwn17l11adw9jgbq4wc")))

(define-public crate-lazy-heap-0.1.1-alpha.3 (c (n "lazy-heap") (v "0.1.1-alpha.3") (d (list (d (n "slab_allocator_rs") (r "^1.0.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "1fn5zvb2b91gdsaaxqmhx5xgkvk82yvb72dvv88qkk79j948wlj4")))

(define-public crate-lazy-heap-0.1.1-alpha.4 (c (n "lazy-heap") (v "0.1.1-alpha.4") (d (list (d (n "slab_allocator_rs") (r "^1.0.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "0np31gf6b4y2rf64na97sz3dggjj6p426rkhsbpqv7k842phdx60")))

(define-public crate-lazy-heap-0.1.1-alpha.5 (c (n "lazy-heap") (v "0.1.1-alpha.5") (d (list (d (n "slab_allocator_rs") (r "^1.0.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "1lrzilw1fxs06v9f23lccl2mr0im8gdwqa9b8jda5nsrva2rzmff")))

(define-public crate-lazy-heap-0.1.1-alpha.6 (c (n "lazy-heap") (v "0.1.1-alpha.6") (d (list (d (n "slab_allocator_rs") (r "^1.0.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "1cxnlnrzvgc66hz3jx7wa84ig184zpfbf719wvinnn50axj257pr")))

(define-public crate-lazy-heap-0.1.1-alpha.7 (c (n "lazy-heap") (v "0.1.1-alpha.7") (d (list (d (n "slab_allocator_rs") (r "^1.0.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "1xig4al1f8dyb66q8n8zcxsz1aidpmxdbbywx70dqp22maqnysad")))

(define-public crate-lazy-heap-0.1.1-alpha.8 (c (n "lazy-heap") (v "0.1.1-alpha.8") (d (list (d (n "slab_allocator_rs") (r "^1.0.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "0f9a4gsdxa2n1m6sh6g2c1ln0brpm1b3srrnlsrmppsxsily8wxl")))

(define-public crate-lazy-heap-0.1.1-alpha.9 (c (n "lazy-heap") (v "0.1.1-alpha.9") (d (list (d (n "slab_allocator_rs") (r "^1.0.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.8") (d #t) (k 0)))) (h "1d5pqrbh48hd0prbcd93416c1n9qs2ayrykb6jz6j16syfc7smgm")))

