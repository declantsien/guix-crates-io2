(define-module (crates-io la zy lazy_format) #:use-module (crates-io))

(define-public crate-lazy_format-1.0.0 (c (n "lazy_format") (v "1.0.0") (h "1czdsr5ripgd3v51592v9fanvpv2n3ndmr8kq4yng8b6rgql0ifw")))

(define-public crate-lazy_format-1.1.0 (c (n "lazy_format") (v "1.1.0") (h "1n5nfcnq45zgqjdxn65hbfgs3jkfsbrqpjyf196rqd353js28c2w")))

(define-public crate-lazy_format-1.2.0 (c (n "lazy_format") (v "1.2.0") (h "1splc2x720yp4vjf08qwlr3yd4bpf6hm0ajya7259v6yl2qm90w0")))

(define-public crate-lazy_format-1.2.1 (c (n "lazy_format") (v "1.2.1") (h "0lln8yj1gssbb358937fdjl0k88dpiv3riryjn8ljv0imqfjqayh")))

(define-public crate-lazy_format-1.3.0 (c (n "lazy_format") (v "1.3.0") (h "1fh1x5mry78cf1qri9qbwbhhn9kjk6b6z5yhpxjn691ysk4ky2dx")))

(define-public crate-lazy_format-1.3.1 (c (n "lazy_format") (v "1.3.1") (h "0bj3rw076b7xhldv8cmxf2w29ypkyf8p0pd3g2zvjvsqr8ghs8kz")))

(define-public crate-lazy_format-1.4.0 (c (n "lazy_format") (v "1.4.0") (h "1wbg89wq86paapv7zh5lnnk9lfy1qm0v1fm5ijxwyyc389dmvf60")))

(define-public crate-lazy_format-1.5.0 (c (n "lazy_format") (v "1.5.0") (h "1x8839l7y84dy1i4s478m6w5yax3xf2iql24yd61g9h10kq43b5v")))

(define-public crate-lazy_format-1.6.0 (c (n "lazy_format") (v "1.6.0") (h "19pdzphw2gk22a442pzc0xpmxjrslncrcbnzcb1mi16h937vlb27")))

(define-public crate-lazy_format-1.7.0 (c (n "lazy_format") (v "1.7.0") (h "110c9kmgsm9rgxlgfrg50qs56jqhai0si39h2g13kfv1jkj72z8n")))

(define-public crate-lazy_format-1.7.1 (c (n "lazy_format") (v "1.7.1") (h "0yji2fbf5d6sz4fkln2b89ksbhjm78b3d94j51qh5yzfq07ck2q3")))

(define-public crate-lazy_format-1.7.2 (c (n "lazy_format") (v "1.7.2") (h "0d8v5pib72c9q1lwgwq0shj57740p6bn7r4nswl8m4spq4qyz1b3")))

(define-public crate-lazy_format-1.7.3 (c (n "lazy_format") (v "1.7.3") (h "14386q3wfq7m528x78kk6ywh02rkj8ycqrb6q8ppvb30g441adfc")))

(define-public crate-lazy_format-1.7.4 (c (n "lazy_format") (v "1.7.4") (h "022hk3ay5ps06n4bpap8y9xiz7hbikik08l8a1px9fc80hvzmjqz")))

(define-public crate-lazy_format-1.8.0 (c (n "lazy_format") (v "1.8.0") (d (list (d (n "horrorshow") (r "^0.8.3") (o #t) (d #t) (k 0)))) (h "1g2imwcml6isr0nb9ibiawzby7giz7cpcyjzi1v1cia7dwp1fjyr") (y #t)))

(define-public crate-lazy_format-1.8.1 (c (n "lazy_format") (v "1.8.1") (d (list (d (n "horrorshow") (r "^0.8.3") (o #t) (d #t) (k 0)))) (h "0js8b9i0b62rlcl4fw9piqzy1jjcqm74dx8xxyvh8kj3bzh8ay0w") (y #t)))

(define-public crate-lazy_format-1.8.2 (c (n "lazy_format") (v "1.8.2") (d (list (d (n "horrorshow") (r "^0.8.3") (o #t) (d #t) (k 0)))) (h "0h25b0hm40sbl0z018453x4aalxzr73y4n9687mbihx19blfk8cj") (y #t)))

(define-public crate-lazy_format-1.8.3 (c (n "lazy_format") (v "1.8.3") (d (list (d (n "horrorshow") (r "^0.8.3") (o #t) (d #t) (k 0)))) (h "13ysa9ricahj47fx9x0rp1gzw9v16slzgs40klfhyfqdlwqzv2gk")))

(define-public crate-lazy_format-1.8.4 (c (n "lazy_format") (v "1.8.4") (d (list (d (n "horrorshow") (r "^0.8.3") (o #t) (d #t) (k 0)))) (h "0ip79hmfc3xyjbb1j0l5avrp190csbg1x8s2alhcmayq4lvxz06v")))

(define-public crate-lazy_format-1.9.0 (c (n "lazy_format") (v "1.9.0") (d (list (d (n "horrorshow") (r "^0.8.3") (o #t) (d #t) (k 0)))) (h "0syz7aicq900jgd2l088m0adnjbaj56nfr8phlm071i03p1p08a8")))

(define-public crate-lazy_format-1.10.0 (c (n "lazy_format") (v "1.10.0") (d (list (d (n "horrorshow") (r "^0.8.3") (o #t) (d #t) (k 0)))) (h "1hxxmyarfl700yd7ppkmvc74cdp9b8ckbyb48j9hcc6nkjz64mmh")))

(define-public crate-lazy_format-2.0.0 (c (n "lazy_format") (v "2.0.0") (h "0kcmk470ia7mpi0as8j96vzn2lk9hj76nn7da6x69na7zwksy81c")))

(define-public crate-lazy_format-2.0.1 (c (n "lazy_format") (v "2.0.1") (h "09ff9qaf6j938as6v332d8mibf75giw7c7w143d9r56fdib0nlg3")))

(define-public crate-lazy_format-2.0.2 (c (n "lazy_format") (v "2.0.2") (h "10kkk96fknmknl0ha0lbhi0yh6pcbi3p4i8r726lpcqkxr4whg4q")))

(define-public crate-lazy_format-2.0.3 (c (n "lazy_format") (v "2.0.3") (h "14m7zcfvfmf1ywa5p76vi841kcldyk6r4k6dyvaqwmvx52dyjyg4")))

