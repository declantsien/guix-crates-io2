(define-module (crates-io la zy lazy-regex-proc_macros) #:use-module (crates-io))

(define-public crate-lazy-regex-proc_macros-2.0.0 (c (n "lazy-regex-proc_macros") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ldmrxv9827q7cykd4s9h3mpy3gj3hlq2mgn9nwclckfw19jx1r1")))

(define-public crate-lazy-regex-proc_macros-2.0.1 (c (n "lazy-regex-proc_macros") (v "2.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "10nsnv3h6kkwblsnjjdnppd8j0k0lyg44x5bddjkx7xka97va3lz")))

(define-public crate-lazy-regex-proc_macros-2.0.2 (c (n "lazy-regex-proc_macros") (v "2.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04xb6jzz9rlimg3a1vxc5xkdrwwfiq1qs4z0vzzdxw7px49z4pa9")))

(define-public crate-lazy-regex-proc_macros-2.1.0 (c (n "lazy-regex-proc_macros") (v "2.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13ac6vz5rq1pfg1jv2hbp1qviw7ax3rzshi3w47nyad1pqdk4vyd")))

(define-public crate-lazy-regex-proc_macros-2.2.0 (c (n "lazy-regex-proc_macros") (v "2.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12qqvpz9bji8yf5q95c8qcnwn3bikvw0cwy0l1pg9fr4m8ciqy27")))

(define-public crate-lazy-regex-proc_macros-2.2.1 (c (n "lazy-regex-proc_macros") (v "2.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kpqhhs2p9zn2q4cfjz30vkwgl4znwapwlj057i5pkwj3f5r64kc")))

(define-public crate-lazy-regex-proc_macros-2.2.2 (c (n "lazy-regex-proc_macros") (v "2.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dkf94rpg55kfwfc5vsvjn5rpvbmgq0da9b6jw3irxaal3q6pgjz")))

(define-public crate-lazy-regex-proc_macros-2.3.0 (c (n "lazy-regex-proc_macros") (v "2.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "14rjlb49k7vjfqznr0qd26p0d56qkimvfzpkrhkcg6q6ci96wjgj")))

(define-public crate-lazy-regex-proc_macros-2.3.1 (c (n "lazy-regex-proc_macros") (v "2.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0dnxf93km1qp627b3sny0whirdgjk1big97xj7q40dpplm130g6s")))

(define-public crate-lazy-regex-proc_macros-2.3.2 (c (n "lazy-regex-proc_macros") (v "2.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "08gn2273jpd21rwiy4wjv5pc796bbd3aa1w4k7b31yjkxfpxyalv") (y #t)))

(define-public crate-lazy-regex-proc_macros-2.4.0 (c (n "lazy-regex-proc_macros") (v "2.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "1xs6yg39cvf4s96k9jyk65xqjh3hhkgfng0hy0gx7v9jnl796mp2")))

(define-public crate-lazy-regex-proc_macros-2.4.1 (c (n "lazy-regex-proc_macros") (v "2.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "08hwc7w6y20v32p08swg9ar9p86gawas4bp60zi8bkjniwdw3pwf")))

(define-public crate-lazy-regex-proc_macros-3.0.0 (c (n "lazy-regex-proc_macros") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (f (quote ("full"))) (d #t) (k 0)))) (h "0n6mw94l82vhfkdhh9yra8zazx8ixdjjsm6nkrl7xbcxv3ynsx2g")))

(define-public crate-lazy-regex-proc_macros-3.0.1 (c (n "lazy-regex-proc_macros") (v "3.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pvjdh8q6bsnn6hil7nd46mg4w04pb8cba885s32xvph768is2hg")))

(define-public crate-lazy-regex-proc_macros-3.1.0 (c (n "lazy-regex-proc_macros") (v "3.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02zgaxdq95s0xm4svbdz6f4xkf4kncl5gjfdzyxgr9wpdj7dbg24")))

