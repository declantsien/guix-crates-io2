(define-module (crates-io la zy lazy-wrap) #:use-module (crates-io))

(define-public crate-lazy-wrap-0.1.0 (c (n "lazy-wrap") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1iyqlyh9qbm5jindq87v2ba5n6yiaymda9fvk9jl38310an2y2vp")))

(define-public crate-lazy-wrap-0.2.0 (c (n "lazy-wrap") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "13pkfd7m6mld7l0slcbms3s7hlbwp8sr8hvv0874zhnr062p9rjn")))

(define-public crate-lazy-wrap-0.3.0 (c (n "lazy-wrap") (v "0.3.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "12bd268ah15hhplaazr36n2fhmjgbxj77l3dj1y9zybvfs0s0r98")))

(define-public crate-lazy-wrap-0.3.1 (c (n "lazy-wrap") (v "0.3.1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1wgpzyi090nf3fypj9jjch60nfvcbfvppc058sdjdv9b3yg5z4w3")))

(define-public crate-lazy-wrap-0.4.0 (c (n "lazy-wrap") (v "0.4.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "09hcg1ci5qlb634sj4vvgma011a541imgnm8d3348wf47h0nglwr")))

(define-public crate-lazy-wrap-0.4.1 (c (n "lazy-wrap") (v "0.4.1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "1qir2nf80rn6dm9i3g4sb9nw3mf4i9jda53fbbkaad3dqqzcypxh")))

