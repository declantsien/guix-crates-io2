(define-module (crates-io la zy lazy-pbar) #:use-module (crates-io))

(define-public crate-lazy-pbar-0.1.0 (c (n "lazy-pbar") (v "0.1.0") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)))) (h "0ds0i268hzqxyybdkx3jvs9b4dwcjaam1hss1kkgxka4410zqs3p")))

(define-public crate-lazy-pbar-0.1.1 (c (n "lazy-pbar") (v "0.1.1") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)))) (h "0fcpqv60hv70kvc7c6l4hyyxy37v73ajhi1lh6xw90wmb3nr2icp")))

(define-public crate-lazy-pbar-0.2.0 (c (n "lazy-pbar") (v "0.2.0") (d (list (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)))) (h "023xn8a3znc7h1wcqlfcm5kk0ijs1zmmrwywz9azlidcchgi29ig")))

