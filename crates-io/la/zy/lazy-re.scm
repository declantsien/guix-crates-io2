(define-module (crates-io la zy lazy-re) #:use-module (crates-io))

(define-public crate-lazy-re-0.1.0 (c (n "lazy-re") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "053f17mc4gbbycl7acmpx8xna5afk1yj9pbsxbc4z9fsjr0fxlri")))

(define-public crate-lazy-re-0.1.1 (c (n "lazy-re") (v "0.1.1") (d (list (d (n "memoffset") (r "^0.6.5") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0ax3gdclrwc70icvdr2li7vqwqd4k6nxlpbfafsyy22w0hjg3fwa")))

