(define-module (crates-io la zy lazy-panic) #:use-module (crates-io))

(define-public crate-lazy-panic-0.1.0 (c (n "lazy-panic") (v "0.1.0") (h "06znzlfbj6xdsxhbyq3sdq1lhfsav282b3s310avfblrh1dpji9k")))

(define-public crate-lazy-panic-0.1.1 (c (n "lazy-panic") (v "0.1.1") (h "0qmiyizgdxfb452yn5n5xsgbvpmffnw9833gqx0adbqw7v3l00ik")))

(define-public crate-lazy-panic-0.2.0 (c (n "lazy-panic") (v "0.2.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0fwy3ck8agmdywc87c3r1n89n0pnjbi7vzkwgcq44ql7akzqv6m3") (f (quote (("backtrace-on" "backtrace"))))))

(define-public crate-lazy-panic-0.3.0 (c (n "lazy-panic") (v "0.3.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)))) (h "04javjpzllrhv5ig0q7wmdjghxls37jh3an95b4bw0cvgvyi711l") (f (quote (("backtrace-on" "backtrace"))))))

(define-public crate-lazy-panic-0.3.1 (c (n "lazy-panic") (v "0.3.1") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0m3ckiiv4nkgc0zb406acmxclyvbb703p4rna16r0c3yfxcfc72d") (f (quote (("backtrace-on" "backtrace"))))))

(define-public crate-lazy-panic-0.3.2 (c (n "lazy-panic") (v "0.3.2") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1mc29qv04hnkncqhlwv72z7b599xch7avdk06i6q7w75g58yhm94") (f (quote (("backtrace-on" "backtrace"))))))

(define-public crate-lazy-panic-1.0.0 (c (n "lazy-panic") (v "1.0.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0vgaq2afgxxv5c8x5vp3j91wg281jd66hcz72p3sgvkidfkv7d90") (f (quote (("backtrace-on" "backtrace"))))))

