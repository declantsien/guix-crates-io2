(define-module (crates-io la zy lazyfunctions) #:use-module (crates-io))

(define-public crate-lazyfunctions-0.1.0 (c (n "lazyfunctions") (v "0.1.0") (h "0g5bl1f08qwpxfgr85nlbki2hp6v0s3kxza2kda7a7wwbrz23bh6")))

(define-public crate-lazyfunctions-0.1.1 (c (n "lazyfunctions") (v "0.1.1") (h "0v17hiyw2yl34ssnf0rnsick5h17n2087hhb81hh2lcpgx69zwxg")))

(define-public crate-lazyfunctions-0.1.2 (c (n "lazyfunctions") (v "0.1.2") (h "06jb9rfqilikjrbl5irvmxvysnyanmy9a9vcd62q80v8011i7fld")))

