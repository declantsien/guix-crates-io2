(define-module (crates-io la zy lazyk-rust) #:use-module (crates-io))

(define-public crate-lazyk-rust-0.1.0 (c (n "lazyk-rust") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "predicates") (r "^2.1") (d #t) (k 2)))) (h "1ni0af17138irsnbxpisrhxp1jrcvdgkp02idnyi3j91zfz2wl5a")))

