(define-module (crates-io la zy lazylink) #:use-module (crates-io))

(define-public crate-lazylink-0.1.0 (c (n "lazylink") (v "0.1.0") (d (list (d (n "lazylink-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "libloading") (r "^0.6") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0klgz4qzb92fiyhrr95r8l4ipwbkzvs381a37xp5jh8yfpj611qv")))

(define-public crate-lazylink-0.1.1 (c (n "lazylink") (v "0.1.1") (d (list (d (n "lazylink-macro") (r "^0.1.1") (d #t) (k 0)) (d (n "libloading") (r "^0.6") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "09bp7wc3486q5g95bynqcp7f0ilg9dqqfzv7bl860q1igxykk25z")))

(define-public crate-lazylink-0.1.2 (c (n "lazylink") (v "0.1.2") (d (list (d (n "lazylink-macro") (r "^0.1.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0px3nxw79bwjrxc04wmzvmbqsanj71rsalyl52vcny9568a46y0g")))

