(define-module (crates-io la zy lazy_async_promise) #:use-module (crates-io))

(define-public crate-lazy_async_promise-0.1.0 (c (n "lazy_async_promise") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "sync" "time"))) (d #t) (k 0)))) (h "080rbb1ggp3xihjl4hj82h2ds6nhvv34ck9ypascb4igfzjgwa2y") (y #t)))

(define-public crate-lazy_async_promise-0.1.1 (c (n "lazy_async_promise") (v "0.1.1") (d (list (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "sync" "time"))) (d #t) (k 0)))) (h "145l7jl1pfp81p17s8fp9j9hwdmbk74nwl11s6shsmqxcnjiglwv")))

(define-public crate-lazy_async_promise-0.1.2 (c (n "lazy_async_promise") (v "0.1.2") (d (list (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "sync" "time" "fs"))) (d #t) (k 2)))) (h "1x1hp3nnqq6d4mwl10aqhgr93gfxmlmxgay06kgvvci6c1sbcanh")))

(define-public crate-lazy_async_promise-0.2.0 (c (n "lazy_async_promise") (v "0.2.0") (d (list (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "sync" "time" "fs"))) (d #t) (k 2)))) (h "0y19c0s4lfd393aqsby0anihl44alwm53cq80mapliv5s34gy2ai")))

(define-public crate-lazy_async_promise-0.3.0 (c (n "lazy_async_promise") (v "0.3.0") (d (list (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "sync" "time" "fs"))) (d #t) (k 2)))) (h "0y4dwpmj08g384a0pz2r09l2v9bz1zxs1kyvrqpkm6n3qva3ssib")))

(define-public crate-lazy_async_promise-0.3.1 (c (n "lazy_async_promise") (v "0.3.1") (d (list (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "sync" "time" "fs"))) (d #t) (k 2)))) (h "0wvhai2vyib9bsjcq1fmah4mim3llasbwl7h6hkhpl8sbl7krrx1")))

(define-public crate-lazy_async_promise-0.4.0-RC1 (c (n "lazy_async_promise") (v "0.4.0-RC1") (d (list (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "sync" "time" "fs"))) (d #t) (k 2)))) (h "1crfvgh0gwqdm8w50cp5ri90g8s4k6qmdy9331jr76fgz94pbcss")))

(define-public crate-lazy_async_promise-0.4.0 (c (n "lazy_async_promise") (v "0.4.0") (d (list (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "sync" "time" "fs"))) (d #t) (k 2)))) (h "09zg31l0fj2l0wki4rnbzyxlijbsp3g8y6z9qjnm4dzjhck5nigd")))

(define-public crate-lazy_async_promise-0.5.0 (c (n "lazy_async_promise") (v "0.5.0") (d (list (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "sync" "time" "fs" "macros"))) (d #t) (k 2)))) (h "1s70kh2djjgps9q8375668xkyy7xssx51hiprjzci2jz1nzba8ql")))

