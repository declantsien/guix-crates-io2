(define-module (crates-io la zy lazy-array) #:use-module (crates-io))

(define-public crate-lazy-array-0.1.0 (c (n "lazy-array") (v "0.1.0") (h "0w385j6973l9vwa1a2fzr8hkm6li3qmdciqij0n59np1q2b61cky")))

(define-public crate-lazy-array-0.1.1 (c (n "lazy-array") (v "0.1.1") (h "0ks1ja95pgqydmck4qmjzbc342z5ql895p04g8g40ah85fhrnmj1")))

(define-public crate-lazy-array-0.1.2 (c (n "lazy-array") (v "0.1.2") (h "11k7gqdnhvjwk063185bfw5jlir08xfajxgb8rd16a33w8aypgqa")))

