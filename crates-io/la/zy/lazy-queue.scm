(define-module (crates-io la zy lazy-queue) #:use-module (crates-io))

(define-public crate-lazy-queue-0.1.0 (c (n "lazy-queue") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "tokio") (r "^0.1.17") (d #t) (k 2)) (d (n "tokio-sync") (r "^0.1.4") (d #t) (k 0)))) (h "1mr413bdf2f2lj14nmbmlmxdvkdr9fbd5wsrxrinr3m8gvccdci6")))

(define-public crate-lazy-queue-0.1.1 (c (n "lazy-queue") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.21") (d #t) (k 0)) (d (n "tokio") (r "^0.1.7") (d #t) (k 2)) (d (n "tokio-sync") (r "^0.1.3") (d #t) (k 0)))) (h "10mhc5hg2narnmy0r7vppfc9fz23z0x7j1a6y47d6vicvdqvj7yy")))

(define-public crate-lazy-queue-0.2.0 (c (n "lazy-queue") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2.6") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.6") (f (quote ("full"))) (d #t) (k 2)))) (h "1mmljkn584z4cfmrv3bdf2304y37q2wr93n262i0dk9vxmqmmarg") (y #t)))

(define-public crate-lazy-queue-0.2.1 (c (n "lazy-queue") (v "0.2.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "pin-project") (r "^0.4.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2.6") (f (quote ("sync" "stream"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2.6") (f (quote ("full"))) (d #t) (k 2)))) (h "0k0phjj9a38ipciqrqqp912rahdqj9yya0zgak4lwaysir6c0sh8")))

