(define-module (crates-io la zy lazyrand) #:use-module (crates-io))

(define-public crate-lazyrand-0.1.0 (c (n "lazyrand") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "0600d0jcmdh43rda0p9x9rbv9yc4qj85n1xf22ik00mmfzy1j324")))

(define-public crate-lazyrand-0.1.1 (c (n "lazyrand") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "1fyl7lkmxbvj4ilrwnap1g01gsfxnp7r8dcympk9laknkb97g85g")))

(define-public crate-lazyrand-0.1.2 (c (n "lazyrand") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "194kv5dry4ksdp2zjd2qw0j05qr1ssb530bps86wmp4ix5n4pp03")))

(define-public crate-lazyrand-0.1.3 (c (n "lazyrand") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "0pfavz4mz9s1zybxw68skxvqw0ckxqh80k1lpqr1pinlb7kdk2pa")))

(define-public crate-lazyrand-0.1.4 (c (n "lazyrand") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "11ixyrgbl0b53db20r14wykiai1d55l0d8jwklzi22kgdwz25n6l")))

(define-public crate-lazyrand-0.1.5 (c (n "lazyrand") (v "0.1.5") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "07mpcgwwg1pfigxnvkzw875mcf35h9z71n8zggl0g0jv8p09asx3")))

(define-public crate-lazyrand-0.1.7 (c (n "lazyrand") (v "0.1.7") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "1jrkafvk8nfjgvr8z0zx4imvbiffiw5pqqh2l82kz1vd4wi6ssrl")))

(define-public crate-lazyrand-0.1.8 (c (n "lazyrand") (v "0.1.8") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "13874bx3kj4gyq7hmgwncmyfkchglxarsgpd0hxj636pawphk5m2")))

(define-public crate-lazyrand-0.1.9 (c (n "lazyrand") (v "0.1.9") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)))) (h "1f3fvcj10y4v6l3x9vllvcb9y8q0b0sjb50rgf31rx4mjnbwcwnc")))

(define-public crate-lazyrand-0.1.10 (c (n "lazyrand") (v "0.1.10") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "0cwqkrbc8slz6pdkw5srpx2s0h2s7126c0ijkyc8w5j28lm4m5v0")))

(define-public crate-lazyrand-0.1.11 (c (n "lazyrand") (v "0.1.11") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "0rhzi9gx7w173v47fzjr0lxzarmans3fyhc2nbydkivr9z8sm8nq")))

(define-public crate-lazyrand-0.1.12 (c (n "lazyrand") (v "0.1.12") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)))) (h "1ynldn0xyv90zclv1vmxvfl4i22i9n7mc79y2fw7pm228jrc9fsx")))

