(define-module (crates-io la zy lazy_extern) #:use-module (crates-io))

(define-public crate-lazy_extern-0.1.0 (c (n "lazy_extern") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 2)) (d (n "libloading") (r "^0.5.0") (d #t) (k 0)))) (h "1ifqfh9m5wxw6drlpzxvkfjlcd9kmm9zrs954hqj39l75c6wa5is")))

