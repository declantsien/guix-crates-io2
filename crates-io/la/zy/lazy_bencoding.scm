(define-module (crates-io la zy lazy_bencoding) #:use-module (crates-io))

(define-public crate-lazy_bencoding-0.1.0 (c (n "lazy_bencoding") (v "0.1.0") (h "1wn27204b8d5z4w2za0jbjmrvm3vw1vihl9zm7wbhq8s3h54ag80")))

(define-public crate-lazy_bencoding-0.1.1 (c (n "lazy_bencoding") (v "0.1.1") (h "18800s5masvzyck80a2jznkp977pfrf8q3s1scglg43vq127ah05")))

