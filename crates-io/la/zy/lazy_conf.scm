(define-module (crates-io la zy lazy_conf) #:use-module (crates-io))

(define-public crate-lazy_conf-0.1.0 (c (n "lazy_conf") (v "0.1.0") (h "1rryjc5kx432n7j2c07yp26fc6paalznl03c7smw6y7mhhps7kdc")))

(define-public crate-lazy_conf-0.1.1 (c (n "lazy_conf") (v "0.1.1") (h "13d5nd04g6yr2hfybijximnwp2wh21lgij2b9xdwnbmpqz56w7s4")))

