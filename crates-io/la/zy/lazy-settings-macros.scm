(define-module (crates-io la zy lazy-settings-macros) #:use-module (crates-io))

(define-public crate-lazy-settings-macros-0.2.0 (c (n "lazy-settings-macros") (v "0.2.0") (d (list (d (n "darling") (r "=0.14") (d #t) (k 0)) (d (n "quote") (r "=1.0") (d #t) (k 0)) (d (n "syn") (r "=1.0") (d #t) (k 0)))) (h "0mg8n69dfzxm0pdgqmsz5azqmgw4z2wqpmlfhhv4kffasic6laml")))

(define-public crate-lazy-settings-macros-0.3.0 (c (n "lazy-settings-macros") (v "0.3.0") (d (list (d (n "darling") (r "=0.14") (d #t) (k 0)) (d (n "quote") (r "=1.0") (d #t) (k 0)) (d (n "syn") (r "=1.0") (d #t) (k 0)))) (h "11jwzl6hkm7hd3fymgjhpaqmw87hiazvfsgpls1dhzrp3n2k876s")))

(define-public crate-lazy-settings-macros-0.4.0 (c (n "lazy-settings-macros") (v "0.4.0") (d (list (d (n "darling") (r "=0.14") (d #t) (k 0)) (d (n "quote") (r "=1.0") (d #t) (k 0)) (d (n "syn") (r "=1.0") (d #t) (k 0)))) (h "091kqlbwr04f61fiikpnbcwiwkims67ncdh5q994kxb1cjmndjc8")))

