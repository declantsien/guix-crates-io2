(define-module (crates-io la zy lazy-pool) #:use-module (crates-io))

(define-public crate-lazy-pool-0.1.0 (c (n "lazy-pool") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.18") (d #t) (k 0)) (d (n "uuid") (r "^0.5.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "1qld1wqb3i6rdyv6yrlv0qyd5r3ipjvxxxm1kvxwysk5iaglic23") (y #t)))

(define-public crate-lazy-pool-0.1.1 (c (n "lazy-pool") (v "0.1.1") (d (list (d (n "futures") (r "~0.1") (d #t) (k 0)) (d (n "uuid") (r "~0") (f (quote ("v4"))) (d #t) (k 2)))) (h "1bsm967jmr5am80flvq5r50qqjnv93p7k5fpnx65bcp49w7p1dqp") (y #t)))

(define-public crate-lazy-pool-0.1.2 (c (n "lazy-pool") (v "0.1.2") (d (list (d (n "futures") (r "~0.1") (d #t) (k 0)) (d (n "uuid") (r "~0") (f (quote ("v4"))) (d #t) (k 2)))) (h "02wscqsm8ky5cbhjil3dydm5i12wwm6rwgn2xlcwl0fnxsqi0gch") (y #t)))

(define-public crate-lazy-pool-0.1.3 (c (n "lazy-pool") (v "0.1.3") (d (list (d (n "futures") (r "~0.1") (d #t) (k 0)) (d (n "uuid") (r "~0") (f (quote ("v4"))) (d #t) (k 2)))) (h "1zac169l1479kisg8wjsjvs240chn11jdch0rkrl4y9vdr13ghvn") (y #t)))

(define-public crate-lazy-pool-0.2.0 (c (n "lazy-pool") (v "0.2.0") (d (list (d (n "futures") (r "~0.1") (d #t) (k 0)) (d (n "log") (r "~0") (d #t) (k 0)) (d (n "uuid") (r "~0") (f (quote ("v4"))) (d #t) (k 2)))) (h "1y3hk918wmyvq0a120qmlcxvqcn9dl87jg199iap40wyb5v3frpw")))

(define-public crate-lazy-pool-0.2.1 (c (n "lazy-pool") (v "0.2.1") (d (list (d (n "futures") (r "~0.1") (d #t) (k 0)) (d (n "futures-await") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "~0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.1") (d #t) (k 2)) (d (n "uuid") (r "~0") (f (quote ("v4"))) (d #t) (k 2)))) (h "0vg5y4ak43lkjy0y644slnmxg8w5rbaw9h1x45qzb98bmcrlxyk1")))

(define-public crate-lazy-pool-0.2.2 (c (n "lazy-pool") (v "0.2.2") (d (list (d (n "futures") (r "~0.1") (d #t) (k 0)) (d (n "futures-await") (r "^0.1") (d #t) (k 2)) (d (n "log") (r "~0") (d #t) (k 0)) (d (n "tokio") (r "^0.1.1") (d #t) (k 2)) (d (n "uuid") (r "~0") (f (quote ("v4"))) (d #t) (k 2)))) (h "127zmwfgq16sh0ccm87ap4daidl434s73ld6m8p5jxc2maddwlzj")))

(define-public crate-lazy-pool-0.2.3 (c (n "lazy-pool") (v "0.2.3") (d (list (d (n "futures") (r "~0.1") (d #t) (k 0)) (d (n "log") (r "~0") (d #t) (k 0)) (d (n "uuid") (r "~0") (f (quote ("v4"))) (d #t) (k 2)))) (h "04323vn11swssxij8qafjlgiqs4d483n5d4wl01i1nsy763igs35")))

(define-public crate-lazy-pool-1.0.0 (c (n "lazy-pool") (v "1.0.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "~0") (d #t) (k 0)) (d (n "uuid") (r "~0") (f (quote ("v4"))) (d #t) (k 2)))) (h "0yhizp2nzd4p0crcr7vwsl8f1v55094pq61b1px6pm2rpkj46ssy")))

(define-public crate-lazy-pool-1.1.0 (c (n "lazy-pool") (v "1.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "~0") (d #t) (k 0)) (d (n "uuid") (r "~0") (f (quote ("v4"))) (d #t) (k 2)))) (h "0ljcffw0d7kkimv80iaqmbxz74a88cxk81sys4s7fifxd2syvqim")))

(define-public crate-lazy-pool-1.3.0-beta (c (n "lazy-pool") (v "1.3.0-beta") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 2)) (d (n "log") (r "~0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.12") (d #t) (k 2)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "uuid") (r "~0") (f (quote ("v4"))) (d #t) (k 2)))) (h "1d71sn3a977ji9lpl9jwm1zln8zl0pdbxf1aqkb9i2j93mv3nrcc") (y #t)))

(define-public crate-lazy-pool-1.3.0-beta.2 (c (n "lazy-pool") (v "1.3.0-beta.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 2)) (d (n "log") (r "~0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.12") (d #t) (k 2)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "uuid") (r "~0") (f (quote ("v4"))) (d #t) (k 2)))) (h "0zl1ns1lrf751zdk56fic06n2hhcbyj3kp80cn2ylpplipaar4r1") (y #t)))

(define-public crate-lazy-pool-2.0.0-beta.0 (c (n "lazy-pool") (v "2.0.0-beta.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 2)) (d (n "log") (r "~0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.12") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "uuid") (r "~0") (f (quote ("v4"))) (d #t) (k 2)))) (h "0dg8jvdhci4z88s4iwjp7d3wnp17zxqfs70gp44zx8j9hrr5l26f")))

(define-public crate-lazy-pool-2.0.0 (c (n "lazy-pool") (v "2.0.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 2)) (d (n "log") (r "~0") (d #t) (k 0)) (d (n "test-log") (r "^0.2.12") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros" "rt"))) (d #t) (k 2)) (d (n "uuid") (r "~0") (f (quote ("v4"))) (d #t) (k 2)))) (h "17p24qcn1yrgcrkc7z6qiym0zd3knmix139bvnmsgylj3a2pif5w")))

