(define-module (crates-io la zy lazycell) #:use-module (crates-io))

(define-public crate-lazycell-0.0.1 (c (n "lazycell") (v "0.0.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "096bba8bvdlx66ix3cb1j3mpkh4qpg22lrg2rzirr9qyj7vjji0m") (f (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-0.1.0 (c (n "lazycell") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "0f3yb8jpcf5gw50v3b5fm6wkaqgf7qz1nnh11kbwn0whkbkkghwg") (f (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-0.2.0 (c (n "lazycell") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "1bg3zhi17ffm40ysibbirmag8r8rscc9v3gjlbpiyn8qp2p0aj29") (f (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-0.2.1 (c (n "lazycell") (v "0.2.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "02bcp63giw041hlprlg52bfr9rwpnvyphaxpfg29g6q4js4knnc3") (f (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-0.3.0 (c (n "lazycell") (v "0.3.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "1xnbbphgdmqhwhrssnvhq8fj82k6sydrj143081lvgwh91qkavz5") (f (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-0.4.0 (c (n "lazycell") (v "0.4.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "0szgqfl2aw18kn9cf6qqpxxkiw6x6hx9y4r3gklnxn1r8xn304nf") (f (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-0.5.0 (c (n "lazycell") (v "0.5.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "0cb3h771aqnq753krx6f6vpxw3nn41b8gaj24q6y7wqy5z1aaf7c") (f (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-0.5.1 (c (n "lazycell") (v "0.5.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "16w5c75sb7xjica1ys6w8ndxvy001y52fjz722m07yqid1x5nn1v") (f (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-0.6.0 (c (n "lazycell") (v "0.6.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "1vy0gxy28z8afkqhvgl6x895k72i1wsxarnmw4zlmvvhphwqiw56") (f (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-1.0.0 (c (n "lazycell") (v "1.0.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "0l0yy7djpirza5w37slbilgcnxa9hgpkwrncb0chi5jw6v84hfnk") (f (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-1.1.0 (c (n "lazycell") (v "1.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "1zfg2c5550j72a3bsbcmkhlrnv0fx4mm1zm65g7szw1r3d0lqvg2") (f (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-1.2.0 (c (n "lazycell") (v "1.2.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "1q41614v4nqg1xi2hzc2hpiklfz5f14wjby9rvzbwa43lwq4rfnx") (f (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-1.2.1 (c (n "lazycell") (v "1.2.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "0gvqycmpv7parc98i6y64ai7rvxrn1947z2a6maa02g4kvxdd55j") (f (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

(define-public crate-lazycell-1.3.0 (c (n "lazycell") (v "1.3.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0m8gw7dn30i0zjjpjdyf6pc16c34nl71lpv461mix50x3p70h3c3") (f (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

