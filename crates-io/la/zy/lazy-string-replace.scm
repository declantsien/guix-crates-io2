(define-module (crates-io la zy lazy-string-replace) #:use-module (crates-io))

(define-public crate-lazy-string-replace-0.1.0 (c (n "lazy-string-replace") (v "0.1.0") (d (list (d (n "memchr") (r "^2.2") (d #t) (k 0)))) (h "00s0ghacpagwiqng33jmlxrchz0wk3aw8ji3whpcsaqkqmacbymn") (f (quote (("nightly") ("default"))))))

(define-public crate-lazy-string-replace-0.1.1 (c (n "lazy-string-replace") (v "0.1.1") (d (list (d (n "memchr") (r "^2.2") (d #t) (k 0)))) (h "013iy5x01c0wsjrby4rjsjhlbkdcg4n3y5x9bkdld5p14ywzacb2") (f (quote (("nightly") ("default"))))))

(define-public crate-lazy-string-replace-0.1.2 (c (n "lazy-string-replace") (v "0.1.2") (d (list (d (n "memchr") (r "^2.2") (d #t) (k 0)))) (h "0zp3qb3kc1p42sf0x9l0p11ya1dzsf5bm6i3vlndn3nrcvvpsnwa") (f (quote (("nightly") ("default")))) (y #t)))

(define-public crate-lazy-string-replace-0.1.3 (c (n "lazy-string-replace") (v "0.1.3") (d (list (d (n "memchr") (r "^2.2") (d #t) (k 0)))) (h "1gb0dk0vsfj50vflmmvjna5k6g84i1ckd506r5rzyk20jpcmb7ih") (f (quote (("nightly") ("default"))))))

