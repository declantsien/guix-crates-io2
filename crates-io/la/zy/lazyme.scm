(define-module (crates-io la zy lazyme) #:use-module (crates-io))

(define-public crate-lazyme-0.0.1 (c (n "lazyme") (v "0.0.1") (d (list (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)))) (h "03yh777d30x3ahqaa58p1dx3gm2g5yw7pq8b3cl3damsdmn84j42")))

(define-public crate-lazyme-0.0.2 (c (n "lazyme") (v "0.0.2") (d (list (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)))) (h "16kimyn1qqvm0lkdnzq801zk1qnxjm65f3m1whgyfsd8w30gpz2d")))

(define-public crate-lazyme-0.0.3 (c (n "lazyme") (v "0.0.3") (d (list (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)))) (h "1c3j9fmb81km72r8zq9nbgxxpc9nzlv7m7575dqa80vk9zjrw8nz")))

