(define-module (crates-io la zy lazy_diamond_square) #:use-module (crates-io))

(define-public crate-lazy_diamond_square-0.1.0 (c (n "lazy_diamond_square") (v "0.1.0") (d (list (d (n "seahash") (r "^4.1.0") (d #t) (k 0)) (d (n "tinyrand") (r "^0.5.0") (d #t) (k 0)) (d (n "tinyrand-std") (r "^0.5.0") (d #t) (k 0)))) (h "1vksblg2qzazmlccsfnk40kr170qxs7ardrm2n0qsw94nc4rn8c9")))

(define-public crate-lazy_diamond_square-0.1.0-1 (c (n "lazy_diamond_square") (v "0.1.0-1") (d (list (d (n "lds_simple_view") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "seahash") (r "^4") (d #t) (k 0)) (d (n "tinyrand") (r "^0.5") (d #t) (k 0)) (d (n "tinyrand-std") (r "^0.5") (d #t) (k 0)))) (h "15hl573mycvky31x8rw7fzxrksx98bznklxf7k583a531r5q87h4") (y #t) (s 2) (e (quote (("simple_view" "dep:lds_simple_view"))))))

(define-public crate-lazy_diamond_square-0.1.1 (c (n "lazy_diamond_square") (v "0.1.1") (d (list (d (n "lds_simple_view") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "seahash") (r "^4") (d #t) (k 0)) (d (n "tinyrand") (r "^0.5") (d #t) (k 0)) (d (n "tinyrand-std") (r "^0.5") (d #t) (k 0)))) (h "05471n1c83mya5356fia23kvklymninbcxqsyvrvdw4znp8fpcij") (s 2) (e (quote (("simple_view" "dep:lds_simple_view"))))))

(define-public crate-lazy_diamond_square-0.1.1-2 (c (n "lazy_diamond_square") (v "0.1.1-2") (d (list (d (n "lds_simple_view") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "seahash") (r "^4") (d #t) (k 0)) (d (n "tinyrand") (r "^0.5") (d #t) (k 0)) (d (n "tinyrand-std") (r "^0.5") (d #t) (k 0)))) (h "1ihis7v500knrhikwi4xf0cp5q3pxf1s92b0qb9p1n5xn02jj9mz") (y #t) (s 2) (e (quote (("simple_view" "dep:lds_simple_view"))))))

(define-public crate-lazy_diamond_square-0.2.0 (c (n "lazy_diamond_square") (v "0.2.0") (d (list (d (n "image") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "seahash") (r "^4") (d #t) (k 0)) (d (n "tinyrand") (r "^0.5") (d #t) (k 0)) (d (n "tinyrand-std") (r "^0.5") (d #t) (k 0)))) (h "0809pchm3v4s9a48max5nf5kznmaxyxifxanlrkr8g7p3xdr3ach") (s 2) (e (quote (("simple_viewing" "dep:image"))))))

(define-public crate-lazy_diamond_square-1.0.0 (c (n "lazy_diamond_square") (v "1.0.0") (d (list (d (n "image") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "seahash") (r "^4") (d #t) (k 0)) (d (n "tinyrand") (r "^0.5") (d #t) (k 0)) (d (n "tinyrand-std") (r "^0.5") (d #t) (k 0)))) (h "14gbvkj26ps2rmr1ihf5bln6g5war07hqg2pb20swn1274yav03p") (s 2) (e (quote (("simple_viewing" "dep:image"))))))

(define-public crate-lazy_diamond_square-1.1.0 (c (n "lazy_diamond_square") (v "1.1.0") (d (list (d (n "image") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "seahash") (r "^4") (d #t) (k 0)) (d (n "tinyrand") (r "^0.5") (d #t) (k 0)) (d (n "tinyrand-std") (r "^0.5") (d #t) (k 0)))) (h "0pd0hqz1gq4j625fk2zvwy60nq21m32b8jmk4hbyqqxszksjf96f") (s 2) (e (quote (("simple_viewing" "dep:image"))))))

