(define-module (crates-io la zy lazy_concat) #:use-module (crates-io))

(define-public crate-lazy_concat-0.1.2 (c (n "lazy_concat") (v "0.1.2") (h "1553bzfqrg0lbna1ipim32zz0m8rmx6qzq94vvvywriblxygkw12")))

(define-public crate-lazy_concat-0.1.3 (c (n "lazy_concat") (v "0.1.3") (h "0pavi098jsqrln3369v2k0sy7jrrbnq9vrhdjwxv4bps9hznyf3p")))

