(define-module (crates-io la zy lazyivy) #:use-module (crates-io))

(define-public crate-lazyivy-0.2.0 (c (n "lazyivy") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0kq9kcb41m7n40ndlhk25awa3ssyi4v3qxy94l5ch6bmjhf5447w")))

(define-public crate-lazyivy-0.3.0 (c (n "lazyivy") (v "0.3.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0zrvg3lf6pvds3c9xx00xqab0q4phvifv2pqj4i16kj4i10ywdww")))

(define-public crate-lazyivy-0.3.1 (c (n "lazyivy") (v "0.3.1") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1m7p6kpkjsnk4va0ylm4x83c5h5a0q3l9l0qbrg2djs1xgnx6kw3")))

(define-public crate-lazyivy-0.4.0 (c (n "lazyivy") (v "0.4.0") (d (list (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)))) (h "0dfv8hx75xyzqpvvb9swhzwlishvvh11ccypk9s1plpr99ln3la3")))

