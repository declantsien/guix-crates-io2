(define-module (crates-io la zy lazylink-macro) #:use-module (crates-io))

(define-public crate-lazylink-macro-0.1.0 (c (n "lazylink-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1x65lb9xglhg6865w13gi4bm264dh0hswy0agcz7hnc4w5hd4ysm")))

(define-public crate-lazylink-macro-0.1.1 (c (n "lazylink-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0k5y47804fa32xh4zzsdgfrhcyjgj7iyqlbs8wf29rx010ac1r4s")))

(define-public crate-lazylink-macro-0.1.2 (c (n "lazylink-macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0c9lgf9kr894h24x9w6rk7h057zsr2dfy964m071x1ar4dpc2rq3")))

