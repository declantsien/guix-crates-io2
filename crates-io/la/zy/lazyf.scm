(define-module (crates-io la zy lazyf) #:use-module (crates-io))

(define-public crate-lazyf-0.1.0 (c (n "lazyf") (v "0.1.0") (h "0mnlyc0fgizwgnqdyxqpppxhlh0924ard9rw2gvn46gviyylmk30")))

(define-public crate-lazyf-0.1.1 (c (n "lazyf") (v "0.1.1") (h "0ry4bnzvazm9dpaa6gjvn9m3j9zi5kr3bdxs32gygq4n0bv08dxv")))

(define-public crate-lazyf-0.1.2 (c (n "lazyf") (v "0.1.2") (h "0afrx5i2bmrm6z3gch7sm90j1lfkfi2675h0w0qqagg2fsdy0aix")))

(define-public crate-lazyf-0.1.3 (c (n "lazyf") (v "0.1.3") (h "1b2lgxzx3b2k5jgc47ridrji582bflgjn1cqgyv79xyy22pb3qlh")))

(define-public crate-lazyf-0.1.4 (c (n "lazyf") (v "0.1.4") (h "0wnwszbkma0i12sv2wqalbl9mif1vbi42nwn1n7y0njw04cyf2v8")))

