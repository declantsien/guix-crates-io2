(define-module (crates-io la zy lazycrate) #:use-module (crates-io))

(define-public crate-lazycrate-0.0.1 (c (n "lazycrate") (v "0.0.1") (d (list (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)))) (h "1dq3wjrvd322xxva37ckp8gp907q12s9gwp4hhk6ximpdxyy0bhc")))

