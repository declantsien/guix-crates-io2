(define-module (crates-io la zy lazy_type) #:use-module (crates-io))

(define-public crate-lazy_type-0.1.0 (c (n "lazy_type") (v "0.1.0") (h "1zi088yfr7wbjmxdjr5jdvxd00vyifnhy00sjfvicyj3j680l9m3")))

(define-public crate-lazy_type-0.1.1 (c (n "lazy_type") (v "0.1.1") (h "0gncwgs6qdqjha9winfvnkwc2vsskc97ihqgzjjmim1qampimi97")))

