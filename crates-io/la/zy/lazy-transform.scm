(define-module (crates-io la zy lazy-transform) #:use-module (crates-io))

(define-public crate-lazy-transform-0.1.0 (c (n "lazy-transform") (v "0.1.0") (h "1ahzra9nwp5nabw6gq2w0ll5rpy0bc4ay17wi60kaasv9zjmayrg")))

(define-public crate-lazy-transform-0.1.1 (c (n "lazy-transform") (v "0.1.1") (h "01dlzdk2dji5h70acl2b6aww78h1bp4fk221wg2671vi7izwm66p")))

