(define-module (crates-io la zy lazy-seq) #:use-module (crates-io))

(define-public crate-lazy-seq-0.1.0 (c (n "lazy-seq") (v "0.1.0") (h "1w5qqk8cshqixv2b2adbhr1wiai5a4hji81jwv66x1blr59c8h6i")))

(define-public crate-lazy-seq-0.1.1 (c (n "lazy-seq") (v "0.1.1") (h "10nnjsa8447ka10bfw881zpjpwn5bvsbr0dpqxjm9gw1hraxic94")))

(define-public crate-lazy-seq-0.1.2 (c (n "lazy-seq") (v "0.1.2") (h "1b0lh73wi9h6wc6vrap98dnnz9zi97lzg3b9vl0875lxsnp90qk4")))

