(define-module (crates-io la zy lazychacha) #:use-module (crates-io))

(define-public crate-lazychacha-0.1.1 (c (n "lazychacha") (v "0.1.1") (d (list (d (n "chacha20poly1305") (r "^0.10.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "faster-hex") (r "^0.9.0") (d #t) (k 0)) (d (n "x25519-dalek") (r "^2.0.1") (f (quote ("getrandom"))) (d #t) (k 0)))) (h "06jn28kjpzwndazgwxsqni4w3dh4pw4lf1wj2105b3vyl52mdm6x")))

