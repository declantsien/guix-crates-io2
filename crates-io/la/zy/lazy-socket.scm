(define-module (crates-io la zy lazy-socket) #:use-module (crates-io))

(define-public crate-lazy-socket-0.0.0 (c (n "lazy-socket") (v "0.0.0") (h "035hmsbdbdp5i49j90akx1lid031dksaalmqnl6isl6gn35pcclx")))

(define-public crate-lazy-socket-0.0.1 (c (n "lazy-socket") (v "0.0.1") (h "151ivgmnimdxpzzjd0a2v0z3c49sh65jka4ssyq74a8izf55hmny")))

(define-public crate-lazy-socket-0.0.2 (c (n "lazy-socket") (v "0.0.2") (h "1dhg1b2nbm9k661sx18fkcyz82c7qc91ki4bvx0hyfvcdr80n2cs")))

(define-public crate-lazy-socket-0.0.3 (c (n "lazy-socket") (v "0.0.3") (d (list (d (n "libc") (r "^0.2.20") (d #t) (t "cfg(unix)") (k 0)))) (h "1h9gpxq5kgprs7bzhg6ckcw1biys0xsf1kkb2mb3l33qq11prxsm")))

(define-public crate-lazy-socket-0.0.4 (c (n "lazy-socket") (v "0.0.4") (d (list (d (n "libc") (r "^0.2.20") (d #t) (t "cfg(unix)") (k 0)))) (h "1s3s14iwc0qvrnfqrj0jvfjrcsrxiksvblyy14xnzz56n98whhs5")))

(define-public crate-lazy-socket-0.1.0 (c (n "lazy-socket") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.20") (d #t) (t "cfg(unix)") (k 0)))) (h "1gqmdcb323q9s7pfss66v9y6qj3yll1w0af57p83d4rn3zsz8rg3")))

(define-public crate-lazy-socket-0.1.1 (c (n "lazy-socket") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.20") (d #t) (t "cfg(unix)") (k 0)))) (h "1h75bqmlri3jbaz2m4qk04nragxcj9flj6513lsz5i8318405q77") (y #t)))

(define-public crate-lazy-socket-0.1.2 (c (n "lazy-socket") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.20") (d #t) (t "cfg(unix)") (k 0)))) (h "0dwbcpa65llligihbk8113kj83bny4xsi5yw82i79s2hjfkydhqs")))

(define-public crate-lazy-socket-0.2.0 (c (n "lazy-socket") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.20") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)) (d (n "ws2_32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)))) (h "0xlqqg75vrxh16rww8kkdyrg3il8k1mrdywy49jvx9jb58p924m4")))

(define-public crate-lazy-socket-0.2.1 (c (n "lazy-socket") (v "0.2.1") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.22") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)) (d (n "ws2_32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)))) (h "0i7x5f6ncwadv0kxxx9lrbkp11pjlz333r3wch399vih84g6lfna") (f (quote (("safe_buffer_len") ("default" "safe_buffer_len"))))))

(define-public crate-lazy-socket-0.2.2 (c (n "lazy-socket") (v "0.2.2") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.22") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.2.8") (d #t) (t "cfg(windows)") (k 0)) (d (n "ws2_32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)))) (h "0prbgc19rvrc0lgqs43wbf9yhim0jjs04bs1dgm3900i93fx4q7b") (f (quote (("safe_buffer_len") ("default" "safe_buffer_len"))))))

(define-public crate-lazy-socket-0.3.0 (c (n "lazy-socket") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.22") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("minwindef" "handleapi" "winsock2" "ws2def" "winerror" "ntdef" "inaddr" "in6addr" "ws2ipdef"))) (t "cfg(windows)") (k 0)))) (h "1w15rvimn5q9rmk8akhdnjdda285w0gsqs61shf025vvyqwh1xrf") (f (quote (("safe_buffer_len") ("default" "safe_buffer_len"))))))

