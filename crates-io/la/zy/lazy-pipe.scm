(define-module (crates-io la zy lazy-pipe) #:use-module (crates-io))

(define-public crate-lazy-pipe-0.1.0 (c (n "lazy-pipe") (v "0.1.0") (h "17icpa0rzd0c542pyv2mmxqmyqvahz4jx8ndryqb3cpzharc9jik") (y #t)))

(define-public crate-lazy-pipe-0.1.1 (c (n "lazy-pipe") (v "0.1.1") (h "09mahr93w57zqd3sbsdyp5ar5yrhjcxn8y82dlnxixymn57mgw3n") (y #t)))

(define-public crate-lazy-pipe-0.1.2 (c (n "lazy-pipe") (v "0.1.2") (h "0502ajz7b549yxy439x02nkx8f59ipggw4kngb5n92j2rbsd2x8z") (y #t)))

(define-public crate-lazy-pipe-0.1.3 (c (n "lazy-pipe") (v "0.1.3") (h "0m6ma48i0fmj4ijh6bv410ghacfzd61p3j6amrm4ngp0mznl5pgq") (y #t)))

