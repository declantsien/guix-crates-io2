(define-module (crates-io la zy lazy_errors) #:use-module (crates-io))

(define-public crate-lazy_errors-0.1.0 (c (n "lazy_errors") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 2)))) (h "1nycn3kzmpv9a76c948bbd59vkz1wvyxh86lhas0wk2kcw5d78cw") (f (quote (("std")))) (s 2) (e (quote (("eyre" "std" "dep:color-eyre"))))))

(define-public crate-lazy_errors-0.1.1 (c (n "lazy_errors") (v "0.1.1") (d (list (d (n "color-eyre") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "indoc") (r "^2.0.5") (d #t) (k 2)) (d (n "test-case") (r "^3.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 2)))) (h "1hkg6yr2ya7pwzsd87zc8csmdf62swbli600pb22a09wsrs0s9y1") (f (quote (("std")))) (s 2) (e (quote (("eyre" "std" "dep:color-eyre"))))))

