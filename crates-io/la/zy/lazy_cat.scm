(define-module (crates-io la zy lazy_cat) #:use-module (crates-io))

(define-public crate-lazy_cat-1.0.0 (c (n "lazy_cat") (v "1.0.0") (h "01cral4qlpd8g78zmh3ph8xhrwfg7gaqarw1yy5cc5hr3hh0z5ik")))

(define-public crate-lazy_cat-1.0.1 (c (n "lazy_cat") (v "1.0.1") (h "142c6rrybnmll05bhssr0xs98ci8ah0mqqwq7lx7w6yw69hfrvl1")))

(define-public crate-lazy_cat-1.0.2 (c (n "lazy_cat") (v "1.0.2") (h "01w9sxjrdvk0i9abchsn59r6vr39x9miwbwd2lygdlg4mr45yywg")))

