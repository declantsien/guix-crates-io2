(define-module (crates-io la zy lazy-db) #:use-module (crates-io))

(define-public crate-lazy-db-1.0.0 (c (n "lazy-db") (v "1.0.0") (d (list (d (n "lz4_flex") (r "^0.11.1") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "1cjfg28jw1f38989795q8rq6fxiklp1ij2pq08bjlg8xmrshg1wp") (y #t)))

(define-public crate-lazy-db-1.0.1 (c (n "lazy-db") (v "1.0.1") (d (list (d (n "lz4_flex") (r "^0.11.1") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "0mywar067fpc6652wayahxklia2igmaw1jjl97yyydwqqm08xkb9") (y #t)))

(define-public crate-lazy-db-1.0.2 (c (n "lazy-db") (v "1.0.2") (d (list (d (n "lz4_flex") (r "^0.11.1") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "1bx7r7zv9df29pa2l4rdj1vz6h0aw9bl3akmw1pyx4jv4hvwac8a")))

(define-public crate-lazy-db-1.1.0 (c (n "lazy-db") (v "1.1.0") (d (list (d (n "lz4_flex") (r "^0.11.1") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "016hcfq5c9sanbxazlif2n70b3ccrafvchn6qyvl9h1lzmhzwdm4")))

(define-public crate-lazy-db-1.2.0 (c (n "lazy-db") (v "1.2.0") (d (list (d (n "lz4_flex") (r "^0.11.1") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "17xkfm1a199vcds2xh7qbw7qr0497z7yvizr9nanrhnnzp06nwyz")))

(define-public crate-lazy-db-1.2.1 (c (n "lazy-db") (v "1.2.1") (d (list (d (n "lz4_flex") (r "^0.11.1") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "0raycvkw5ywknsxagvw2m0iblfmik4psfm95a2z8ndi7dr879crj")))

(define-public crate-lazy-db-1.2.2 (c (n "lazy-db") (v "1.2.2") (d (list (d (n "lz4_flex") (r "^0.11.1") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "19x3yjqig1zik1frvyr4ylhqjcf6y7xxvcg3zjzad1cz562sbmp4") (y #t)))

(define-public crate-lazy-db-1.2.3 (c (n "lazy-db") (v "1.2.3") (d (list (d (n "lz4_flex") (r "^0.11.1") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "12wfydh4l2zsmgina0vz78llnrdnyqs53z0q1mzjia3045w68fxs")))

(define-public crate-lazy-db-1.3.0 (c (n "lazy-db") (v "1.3.0") (d (list (d (n "lz4_flex") (r "^0.11.1") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "04wdbzy5lz191s7vcrhp3fr3v5fshzna08gm8bskxbmlf1l7zn1v")))

(define-public crate-lazy-db-1.3.1 (c (n "lazy-db") (v "1.3.1") (d (list (d (n "lz4_flex") (r "^0.11.1") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "0a5lyvphj5gici8y8a4789riz2jibdn0hh92a58nlnql3whng6vq")))

(define-public crate-lazy-db-1.4.0 (c (n "lazy-db") (v "1.4.0") (d (list (d (n "lz4_flex") (r "^0.11.1") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "1202y85vrabmbh7ksclahrra78rn0c1p5cc43r9crr81cfdpjbbj")))

(define-public crate-lazy-db-1.4.1 (c (n "lazy-db") (v "1.4.1") (d (list (d (n "lz4_flex") (r "^0.11.1") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "0wqgvs36k3f1zc0kq2465nk6b2qxmxmsn4fi4y93784ifmm8cwgr")))

(define-public crate-lazy-db-1.5.0 (c (n "lazy-db") (v "1.5.0") (d (list (d (n "lz4_flex") (r "^0.11.1") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "0hag907mfnp96pdd1gmlgrsjv52y07id25106vdz059lbr7lhc1v")))

(define-public crate-lazy-db-1.5.1 (c (n "lazy-db") (v "1.5.1") (d (list (d (n "lz4_flex") (r "^0.11.1") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "1cq1a9j3sf109wkll41a3zsjkb6zr3p7fwsxzpmr0mrhd66kacbf") (y #t)))

(define-public crate-lazy-db-1.5.2 (c (n "lazy-db") (v "1.5.2") (d (list (d (n "lz4_flex") (r "^0.11.1") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "1z398m2d9wgf6vfr00gl60fabyvjn0b68vi40yylxy8a2av78lw3")))

(define-public crate-lazy-db-1.5.3 (c (n "lazy-db") (v "1.5.3") (d (list (d (n "lz4_flex") (r "^0.11.1") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "1hdyd892hv9iik7vmpz73ds0hgjxcgichmbflh7i32i23yqfhwdm")))

(define-public crate-lazy-db-1.5.4 (c (n "lazy-db") (v "1.5.4") (d (list (d (n "lz4_flex") (r "^0.11.1") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)))) (h "0xhcryz2bb6kmqi87ypj1g428rdij7yqbdy702z5zg8y83y70j6f")))

