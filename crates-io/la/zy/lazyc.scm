(define-module (crates-io la zy lazyc) #:use-module (crates-io))

(define-public crate-lazyc-0.5.1 (c (n "lazyc") (v "0.5.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1mik7scgpm1x56f76xrydb590b4vi233wy1c3729vs3ms6pyxaiq")))

(define-public crate-lazyc-0.5.2 (c (n "lazyc") (v "0.5.2") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "12s51a67f8y756gpmpc5i2sxhjzq47zl36sl2fymd8y9hqc068vp")))

(define-public crate-lazyc-0.5.3 (c (n "lazyc") (v "0.5.3") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1qk6sp3lm23hknslfq6ya823sb2pn9fnfsyqqi62asxdn2b2cwvp")))

(define-public crate-lazyc-0.5.4 (c (n "lazyc") (v "0.5.4") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0g4c30a35caa3blqyvh208l68r5n524i7my88x0k5n9w6kcsnsmi")))

(define-public crate-lazyc-0.6.0 (c (n "lazyc") (v "0.6.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0g5xc7hijclcbhw127jqd4490md3jqa91wj3rl79ij4ighi00b8i")))

(define-public crate-lazyc-0.6.1 (c (n "lazyc") (v "0.6.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0s762hnq83fhnv7h8zp705pbv4v303vqydg5cn5fq79s0zyxqdfm")))

