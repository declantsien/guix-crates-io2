(define-module (crates-io la zy lazy-init) #:use-module (crates-io))

(define-public crate-lazy-init-0.1.0 (c (n "lazy-init") (v "0.1.0") (d (list (d (n "scoped-pool") (r "^1.0.0") (d #t) (k 2)))) (h "13lffspxglbk8ib4ajjr881gqj5l9zr3slf633q81z0bi0cnf6hv")))

(define-public crate-lazy-init-0.1.1 (c (n "lazy-init") (v "0.1.1") (d (list (d (n "scoped-pool") (r "^1.0.0") (d #t) (k 2)))) (h "0ljrr0lxdan9ln7n7cfl6ff637k4fmf6xzm13bi8fihb62fb2v2r")))

(define-public crate-lazy-init-0.2.0 (c (n "lazy-init") (v "0.2.0") (d (list (d (n "scoped-pool") (r "^1.0.0") (d #t) (k 2)))) (h "0wrpqjdar2jmraa4lr287bl3wp68kyd9rwcnlm14j7zzmb4a2y89")))

(define-public crate-lazy-init-0.3.0 (c (n "lazy-init") (v "0.3.0") (d (list (d (n "scoped-pool") (r "^1.0.0") (d #t) (k 2)))) (h "04ykc9ccprzca4224y0ci2wq479kawdf53nsfra936i3mwrj47z7")))

(define-public crate-lazy-init-0.4.0 (c (n "lazy-init") (v "0.4.0") (d (list (d (n "scoped-pool") (r "^1.0.0") (d #t) (k 2)))) (h "1arhx5bazfpggfgsbq1873fd2v3y17c96555xj452pcvrkrja437")))

(define-public crate-lazy-init-0.5.0 (c (n "lazy-init") (v "0.5.0") (d (list (d (n "scoped-pool") (r "^1.0.0") (d #t) (k 2)))) (h "058qswwdkrx5x4m8ds1y29ql28d8dclgcssf6831vac7pr07al93")))

(define-public crate-lazy-init-0.5.1 (c (n "lazy-init") (v "0.5.1") (d (list (d (n "rayon") (r "^1") (d #t) (k 2)))) (h "0vvhq8xdmajy2ai8p7zxja68a95n7m65xhdgjapxq4mc4qv9ch4z")))

