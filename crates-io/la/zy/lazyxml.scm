(define-module (crates-io la zy lazyxml) #:use-module (crates-io))

(define-public crate-lazyxml-0.0.1 (c (n "lazyxml") (v "0.0.1") (h "0crv3x0gwa5dzjl0ra698w1h7b03hfq6689r122pl4k946aphlp1") (y #t)))

(define-public crate-lazyxml-0.0.2 (c (n "lazyxml") (v "0.0.2") (d (list (d (n "memchr") (r ">=2.3.0, <3.0.0") (o #t) (d #t) (k 0)))) (h "14ki29r5za1r1yjmc9wdml26qshnkg9rff2l43gh4h867cyz4mm9") (f (quote (("use-memchr" "memchr") ("default" "use-memchr")))) (y #t)))

(define-public crate-lazyxml-0.0.3 (c (n "lazyxml") (v "0.0.3") (d (list (d (n "memchr") (r ">=2.3.0, <3.0.0") (o #t) (d #t) (k 0)))) (h "1dslvhqmhvvxk9gdf44bn9sdc3fjmamkrvc6jjvs64jpqd40zrm4") (f (quote (("use-memchr" "memchr") ("default" "use-memchr")))) (y #t)))

