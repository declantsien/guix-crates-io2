(define-module (crates-io la zy lazy-prime-sieve) #:use-module (crates-io))

(define-public crate-lazy-prime-sieve-0.1.0 (c (n "lazy-prime-sieve") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "0dyir8gpisab9lygfs90m3pn8ngmwddfmxgrc40ajy2by3ws4jnw")))

(define-public crate-lazy-prime-sieve-0.1.1 (c (n "lazy-prime-sieve") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "0a74h36fvq8l8vyarvl3rll117qbnzpfxribhwq45kavhrx0cjpm")))

(define-public crate-lazy-prime-sieve-0.1.2 (c (n "lazy-prime-sieve") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "04n4l62ynk1x5h1nh7vvr9nfgzlgjp37x0vj7frfi4axwadr663s")))

(define-public crate-lazy-prime-sieve-0.1.3 (c (n "lazy-prime-sieve") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "02r9hgpjl4bp0rgi3r0kd034xc771j5xvy077648niqr082v4wmb")))

