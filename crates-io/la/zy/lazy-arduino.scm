(define-module (crates-io la zy lazy-arduino) #:use-module (crates-io))

(define-public crate-lazy-arduino-0.1.0 (c (n "lazy-arduino") (v "0.1.0") (d (list (d (n "termion") (r "^2.0") (d #t) (k 0)))) (h "0jgqbx9wwhg8cbg476mx991r76a1nk0k6slvy6szq1mkdhc0d6k7")))

(define-public crate-lazy-arduino-0.1.2 (c (n "lazy-arduino") (v "0.1.2") (d (list (d (n "termion") (r "^2.0") (d #t) (k 0)))) (h "1sdzc6a0xyw6p9kjhgcyihlbv83fhsh7sxaf3s3vxind48gsha5h")))

(define-public crate-lazy-arduino-0.1.3 (c (n "lazy-arduino") (v "0.1.3") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^2.0") (d #t) (k 0)))) (h "0iq6qlw62gzpv5627gknrv2xrfn9kfvrdni4qsyk5yvnxfckbp1w") (y #t)))

(define-public crate-lazy-arduino-0.1.4 (c (n "lazy-arduino") (v "0.1.4") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^2.0") (d #t) (k 0)))) (h "06iqapyhaz1xkiyd7rgk0q9b25xsyhkw8bd3hkc8xi3dv6np3b3k") (y #t)))

(define-public crate-lazy-arduino-0.1.5 (c (n "lazy-arduino") (v "0.1.5") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^2.0") (d #t) (k 0)))) (h "130w05gchw6icddaw8jy4vp043mwlvvfwv6n4lsa93l1ch28bhky")))

