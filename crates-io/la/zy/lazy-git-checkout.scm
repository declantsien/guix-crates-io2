(define-module (crates-io la zy lazy-git-checkout) #:use-module (crates-io))

(define-public crate-lazy-git-checkout-0.1.0 (c (n "lazy-git-checkout") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.14") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)))) (h "00pqwcp4hlgc64zlgq4q21k0n5x8z8asv4x49c7zxwayxq8w3h31")))

(define-public crate-lazy-git-checkout-0.1.1 (c (n "lazy-git-checkout") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.14") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)))) (h "0qkihjpifyzjmmh83xps1lysfjrqw83g8v5f80nwh50gy4i0jf81")))

(define-public crate-lazy-git-checkout-0.1.2 (c (n "lazy-git-checkout") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.14") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)))) (h "15a98z8vp6403a084cg4293lcs8b05zhpwcx272dzwqhbdr6faqw")))

(define-public crate-lazy-git-checkout-0.1.3 (c (n "lazy-git-checkout") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.14") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.25.0") (d #t) (k 0)))) (h "1qlc84bxm3z16fgab98aym5r1wp9qdjg2d6h1k49s2akzsdc5w80")))

