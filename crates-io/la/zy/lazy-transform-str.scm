(define-module (crates-io la zy lazy-transform-str) #:use-module (crates-io))

(define-public crate-lazy-transform-str-0.0.1 (c (n "lazy-transform-str") (v "0.0.1") (d (list (d (n "cervine") (r "^0.0.2") (d #t) (k 0)) (d (n "gnaw") (r "^0.0.1") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.3") (d #t) (k 0)))) (h "0b85nzf04v9j8ysz20r6gl3g5fppywcghwh6m2gppmlfp4fg39b2")))

(define-public crate-lazy-transform-str-0.0.2 (c (n "lazy-transform-str") (v "0.0.2") (d (list (d (n "cargo-husky") (r "^1.5.0") (d #t) (k 2)) (d (n "cervine") (r "^0.0.2") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "git_info") (r "^0.1.2") (d #t) (k 2)) (d (n "gnaw") (r "^0.0.1") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.3") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "0qmjhsdcc2lmc7zij5yzbbmh17gn7f9ggaca4gv0iwkwk63pgfsg")))

(define-public crate-lazy-transform-str-0.0.3 (c (n "lazy-transform-str") (v "0.0.3") (d (list (d (n "cargo-husky") (r "^1.5.0") (d #t) (k 2)) (d (n "cervine") (r "^0.0.2") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "git_info") (r "^0.1.2") (d #t) (k 2)) (d (n "gnaw") (r "^0.0.1") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.3") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "0qn1akn96df4j8y3p8nmf9kpxq9m3d8djvdrlk00dws2jynwnjxl")))

(define-public crate-lazy-transform-str-0.0.4 (c (n "lazy-transform-str") (v "0.0.4") (d (list (d (n "cargo-husky") (r "^1.5.0") (d #t) (k 2)) (d (n "cervine") (r "^0.0.4") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "git_info") (r "^0.1.2") (d #t) (k 2)) (d (n "gnaw") (r "^0.0.1") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.3") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "0q16yb3kw6fcbi2n2bw8bm1n9c79zg5bw45wlnnfvx9f5gaiyyv8")))

(define-public crate-lazy-transform-str-0.0.5 (c (n "lazy-transform-str") (v "0.0.5") (d (list (d (n "cargo-husky") (r "^1.5.0") (d #t) (k 2)) (d (n "cervine") (r "^0.0.5") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "git_info") (r "^0.1.2") (d #t) (k 2)) (d (n "gnaw") (r "^0.0.2") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.3") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "00kb0n448br6ci0p9dp6r26ggfj58mwr51ihxfvnnc80gsa9xikk")))

(define-public crate-lazy-transform-str-0.0.6 (c (n "lazy-transform-str") (v "0.0.6") (d (list (d (n "cargo-husky") (r "^1.5.0") (d #t) (k 2)) (d (n "cervine") (r "^0.0.6") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "git_info") (r "^0.1.2") (d #t) (k 2)) (d (n "gnaw") (r "^0.0.2") (d #t) (k 0)) (d (n "smartstring") (r "^0.2.3") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "0zimvshm3k5x5lbrplbhdqc60zf08wkk1ik6krq560yswn6z1wnk")))

