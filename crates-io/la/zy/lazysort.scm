(define-module (crates-io la zy lazysort) #:use-module (crates-io))

(define-public crate-lazysort-0.0.1 (c (n "lazysort") (v "0.0.1") (h "15l63vdqsmpwihqnd9jphmfxsd8s9cdwqn852p468k4ka5bk9nlf")))

(define-public crate-lazysort-0.0.2 (c (n "lazysort") (v "0.0.2") (h "0h878gbhn8a8vc1zqrbmkl9d11c6zaxkasxxs84ipjfhi2jxnpzr")))

(define-public crate-lazysort-0.0.3 (c (n "lazysort") (v "0.0.3") (h "0q3dff2fn76fgiq915baadi7rhhcg4glv26prz5qvrw8ck566qs8")))

(define-public crate-lazysort-0.0.4 (c (n "lazysort") (v "0.0.4") (h "16f96cs1z085ywglg5yk54r21d2s513fwkbwx3xk0g52wfqivgfi")))

(define-public crate-lazysort-0.0.5 (c (n "lazysort") (v "0.0.5") (h "12jm5nvs285irkhyxc6zjwjj2s2ym5bij62wimlgls8ww3xbyjr7")))

(define-public crate-lazysort-0.0.6 (c (n "lazysort") (v "0.0.6") (h "0mdhfsinnw0dj8c7p57rnwhc3ia3jm7ry9fpzf3wrjlgaq9isb5q")))

(define-public crate-lazysort-0.0.7 (c (n "lazysort") (v "0.0.7") (h "0zjr9sfkjrz92igy7prrr03nifhrhqj4amn65jk1mp2gbg19hy5w")))

(define-public crate-lazysort-0.0.8 (c (n "lazysort") (v "0.0.8") (h "1ay4qq33zwy5a483pb7fb4fgmzy6wyp6zljfb8wmyv14zr0nrds2")))

(define-public crate-lazysort-0.0.9 (c (n "lazysort") (v "0.0.9") (h "0lw6isfjs2iqwvp7lcz9bjbymi702hn0xmhckwgg1xp9dk89an6d")))

(define-public crate-lazysort-0.0.10 (c (n "lazysort") (v "0.0.10") (h "013bdh0vbap39kzsvx3q9ahv4gr29ijgx1s98m00rhd1v5nnjx8r")))

(define-public crate-lazysort-0.0.11 (c (n "lazysort") (v "0.0.11") (h "10pzkik9xc35r5i7jph7m59csz3mjb1c1b6r5vyndy6g01l4imqj")))

(define-public crate-lazysort-0.0.12 (c (n "lazysort") (v "0.0.12") (d (list (d (n "rand") (r "^0.1.3") (d #t) (k 0)))) (h "1nwmpyrig5rhk8zm8dm8s4haq765mzic23wp0g150mni7xc9hq19")))

(define-public crate-lazysort-0.0.13 (c (n "lazysort") (v "0.0.13") (d (list (d (n "rand") (r "^0.2.1") (d #t) (k 0)))) (h "0gsf60zpgd98paf9fp83gnm71vjbsgnjrnk4w7ds50zw1cjx4g47")))

(define-public crate-lazysort-0.0.14 (c (n "lazysort") (v "0.0.14") (d (list (d (n "rand") (r "^0.3.7") (d #t) (k 0)))) (h "16z1850p718gi06yshq8m6v7yd9fvxl6xhvqa6psw04hdba893y6")))

(define-public crate-lazysort-0.0.15 (c (n "lazysort") (v "0.0.15") (d (list (d (n "rand") (r "^0.3.7") (d #t) (k 0)))) (h "1ilq6mj0ydvk8a83jym533rrlzp9g1i9nyhqbr6pndgc16s5i8cn")))

(define-public crate-lazysort-0.0.16 (c (n "lazysort") (v "0.0.16") (d (list (d (n "rand") (r "^0.3.8") (d #t) (k 0)))) (h "1jc0wmhsxmlb00q39ssdsbcv36lr869z20pmgwrsjnh4z88f7r3p")))

(define-public crate-lazysort-0.1.0 (c (n "lazysort") (v "0.1.0") (d (list (d (n "rand") (r "*") (d #t) (k 0)))) (h "0vz4kdwq37x6g58l6zinhw831a3948qgcbfkh6fyz12j0dvh1is8")))

(define-public crate-lazysort-0.1.1 (c (n "lazysort") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1fq04qmpy140ba3m9isyx667rqj8yfdfg2blibd03f3zpp259q4y")))

(define-public crate-lazysort-0.2.0 (c (n "lazysort") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "02lhvv1w4wjwbv3kqmrrzxnsb4cblc88yjdm6b890h6izv6whghq") (f (quote (("nightly"))))))

(define-public crate-lazysort-0.2.1 (c (n "lazysort") (v "0.2.1") (d (list (d (n "rand") (r ">= 0.3, <= 0.5") (d #t) (k 2)))) (h "13qncky7xc2g450y15rkw45bjwxw7vjq8jfphwphw7i37gs2zqnh") (f (quote (("nightly"))))))

