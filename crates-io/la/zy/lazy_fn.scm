(define-module (crates-io la zy lazy_fn) #:use-module (crates-io))

(define-public crate-lazy_fn-1.0.0 (c (n "lazy_fn") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1y8c7sya5fsib30qwcymc0v4diks07m3wq0jp5mqhps6351fzi8n")))

(define-public crate-lazy_fn-1.0.1 (c (n "lazy_fn") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "09r19vhimdddsy9g7ssnscvgxbaa6d45md3dwj6jarcq1smc40dr")))

(define-public crate-lazy_fn-1.0.2 (c (n "lazy_fn") (v "1.0.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)))) (h "00iswficj57234n1ky5aagxjs411lx4lxjglsk8gzhwiad5mkg6y")))

