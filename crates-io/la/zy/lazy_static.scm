(define-module (crates-io la zy lazy_static) #:use-module (crates-io))

(define-public crate-lazy_static-0.1.0 (c (n "lazy_static") (v "0.1.0") (h "0xr7qlk99kn7vf5p1gcmssfl60d26jy2zdji1v1i5v1pa534mpjh")))

(define-public crate-lazy_static-0.1.1 (c (n "lazy_static") (v "0.1.1") (h "1dfi7m5i3q8wbvyi6akp1g9wcjpy2yi0xf6zr8b714j67xq7kfjj")))

(define-public crate-lazy_static-0.1.2 (c (n "lazy_static") (v "0.1.2") (h "0l3b0xgx9j2kcbpwc483cliz912dlkps3y44bwj29nwg5kgk6v9b")))

(define-public crate-lazy_static-0.1.3 (c (n "lazy_static") (v "0.1.3") (h "1c63hlcplqivcjn1yr6dqvzla4acrn01bgrx1li4y30shh3v4kji")))

(define-public crate-lazy_static-0.1.4 (c (n "lazy_static") (v "0.1.4") (h "0l85qls2sxsz2lpazqnf208hbnyy90dgv48yy1b6h814kmnn37na")))

(define-public crate-lazy_static-0.1.5 (c (n "lazy_static") (v "0.1.5") (h "1fwsk0cd7d0kisvz7snfl9wvrgc8zdgc10gkq9dnhpyyxs59ak60")))

(define-public crate-lazy_static-0.1.6 (c (n "lazy_static") (v "0.1.6") (h "1h4nkp81znxhx2p1sxaj7my110vqfaqrvzagzcghm0ppankz4hww")))

(define-public crate-lazy_static-0.1.7 (c (n "lazy_static") (v "0.1.7") (h "1xj28ajdi4x033ka7gajna1ya3f7m6mk9k0lk3c34j6cdip4jq7c")))

(define-public crate-lazy_static-0.1.8 (c (n "lazy_static") (v "0.1.8") (h "1yhry89qdjjyp8l8vhd9jcw4qm1w866rajdqkrzi9rmd4kfa7qdq")))

(define-public crate-lazy_static-0.1.9 (c (n "lazy_static") (v "0.1.9") (h "0a16lmp3p0ns3xjalm53d853187lnqj76g0f9x2nsyx4mcj8zq3n")))

(define-public crate-lazy_static-0.1.10 (c (n "lazy_static") (v "0.1.10") (h "1xlv9wv1sbnr86xc4h5ljrdfxkgdq2bf6c7bafywdiw2s772c3h7")))

(define-public crate-lazy_static-0.1.11 (c (n "lazy_static") (v "0.1.11") (h "1l15npn688s93281qf16rsvavzs4yz9v5hdjadsx6ay3rd21p372")))

(define-public crate-lazy_static-0.1.12 (c (n "lazy_static") (v "0.1.12") (h "0yyjjax556g8d5dp4bfn6hnv7miqr7kqcl20qvfdyiibvnhf0i45")))

(define-public crate-lazy_static-0.1.13 (c (n "lazy_static") (v "0.1.13") (h "0ppv7hm3dyizw8mv6apbkdn4cya8l3yziqi6xdrw1br02wz1rv38")))

(define-public crate-lazy_static-0.1.14 (c (n "lazy_static") (v "0.1.14") (h "0dj5cvfvs3svm7q2z9ry11l0mb8zb0s87mvjzbkikghc97f0fvj6") (f (quote (("nightly"))))))

(define-public crate-lazy_static-0.1.15 (c (n "lazy_static") (v "0.1.15") (h "0jf4lsjrbmc40yg5s09kfabipc6g93357px6ywwspyq00hdx2zsl") (f (quote (("nightly"))))))

(define-public crate-lazy_static-0.1.16 (c (n "lazy_static") (v "0.1.16") (h "05vl1h4b0iv800grsdyc3fg2bq29p70wjav6zpjvxxd5i8d6s66g") (f (quote (("nightly"))))))

(define-public crate-lazy_static-0.2.0 (c (n "lazy_static") (v "0.2.0") (h "0fwmvdx0fi2b8j1ib15f1alv9xhrqbari6di49j1pdim9d1ix7jg") (f (quote (("nightly"))))))

(define-public crate-lazy_static-0.2.1 (c (n "lazy_static") (v "0.2.1") (d (list (d (n "spin") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0zzyvxi66zm9blfprrzw3cjyf9f0jd8w7nfb4g5kvfw5lb17w929") (f (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

(define-public crate-lazy_static-0.2.2 (c (n "lazy_static") (v "0.2.2") (d (list (d (n "spin") (r "^0.4") (o #t) (d #t) (k 0)))) (h "16z1h7w702sxnscak38jykxlhxq0b5ip4mndlb46pkaqwzi0xgka") (f (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

(define-public crate-lazy_static-0.2.3 (c (n "lazy_static") (v "0.2.3") (d (list (d (n "spin") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0yg6f2zf9ymplrksvl0scxsx2ch6mbr1ny7zdlrhywixq1blrrzp") (f (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

(define-public crate-lazy_static-0.2.4 (c (n "lazy_static") (v "0.2.4") (d (list (d (n "spin") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1p30qiivkkg1d43s10pj3g07zpriqbdxy0hbc99gfcfkjzfv34bj") (f (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

(define-public crate-lazy_static-0.2.5 (c (n "lazy_static") (v "0.2.5") (d (list (d (n "spin") (r "^0.4") (o #t) (d #t) (k 0)))) (h "15qsxv3rxp4hyd9lgbhi5442yx26gfm7sx64cmjl06m2p5iwacj7") (f (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

(define-public crate-lazy_static-0.2.6 (c (n "lazy_static") (v "0.2.6") (d (list (d (n "spin") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1cxb8g3ar37xapk1pbq5v9kpap6xdzaja5k1j71lhiks3i1bhq9g") (f (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

(define-public crate-lazy_static-0.2.7 (c (n "lazy_static") (v "0.2.7") (d (list (d (n "spin") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0kb1bdcxrvih26p50iix91q0zg7030a5wxsdf1klfdlwrai7wgm3") (f (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

(define-public crate-lazy_static-0.2.8 (c (n "lazy_static") (v "0.2.8") (d (list (d (n "spin") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1gsgx41jlpamhparl0mr2h434nqw4flan2j2qqz87p96nxd58drv") (f (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

(define-public crate-lazy_static-0.2.9 (c (n "lazy_static") (v "0.2.9") (d (list (d (n "spin") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1mgqfm1hgjy1id1089fagnwnmb0cxqi8m9v1llavkhx4l67ybrf9") (f (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

(define-public crate-lazy_static-0.2.10 (c (n "lazy_static") (v "0.2.10") (d (list (d (n "compiletest_rs") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0mrdihpmvblbd921aa5bsl1cl1iyvvbn6xwvm10ll7arc9xb6vi3") (f (quote (("spin_no_std" "nightly" "spin") ("nightly") ("compiletest" "compiletest_rs"))))))

(define-public crate-lazy_static-0.2.11 (c (n "lazy_static") (v "0.2.11") (d (list (d (n "compiletest_rs") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.4.6") (o #t) (d #t) (k 0)))) (h "0wxy8vak7jsx6r8gx475pjqpx11p2bfq4wvw6idmqi31mp3k7w3n") (f (quote (("spin_no_std" "nightly" "spin") ("nightly") ("compiletest" "compiletest_rs"))))))

(define-public crate-lazy_static-1.0.0 (c (n "lazy_static") (v "1.0.0") (d (list (d (n "compiletest_rs") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.4.6") (o #t) (d #t) (k 0)))) (h "0z9335v3jxxfwsmxl1r4hf0vbhzpyjfw4isbw6dz2rd3v93i1wy8") (f (quote (("spin_no_std" "nightly" "spin") ("nightly") ("compiletest" "compiletest_rs"))))))

(define-public crate-lazy_static-1.0.1 (c (n "lazy_static") (v "1.0.1") (d (list (d (n "spin") (r "^0.4.6") (o #t) (d #t) (k 0)))) (h "0f9pwzdlssfd1n4kz4qbm39ddk96409974wpiq5lnn6r59g2qhg6") (f (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

(define-public crate-lazy_static-1.0.2 (c (n "lazy_static") (v "1.0.2") (d (list (d (n "spin") (r "^0.4.6") (o #t) (d #t) (k 0)))) (h "1lvdk5mkhw0im12fignd0i38miy2gyh5cjfrrwqs7dk2scspqjgv") (f (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

(define-public crate-lazy_static-1.1.0 (c (n "lazy_static") (v "1.1.0") (d (list (d (n "spin") (r "^0.4.6") (o #t) (d #t) (k 0)) (d (n "version_check") (r "^0.1.4") (d #t) (k 1)))) (h "19vincf2navflh6jlcysalbwyj78nc4mdfa5rlp0lyv5ln4qnj6a") (f (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

(define-public crate-lazy_static-1.2.0 (c (n "lazy_static") (v "1.2.0") (d (list (d (n "spin") (r "^0.4.10") (f (quote ("once"))) (o #t) (k 0)))) (h "18fy414dxmg92qjsh1dl045yrpnrc64f7hbl792ran5mkndwhx53") (f (quote (("spin_no_std" "spin") ("nightly"))))))

(define-public crate-lazy_static-1.3.0 (c (n "lazy_static") (v "1.3.0") (d (list (d (n "spin") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "052ac27w189hrf1j3hz7sga46rp84zl2hqnzyihxv78mgzr2jmxw") (f (quote (("spin_no_std" "spin"))))))

(define-public crate-lazy_static-1.4.0 (c (n "lazy_static") (v "1.4.0") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)) (d (n "spin") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "0in6ikhw8mgl33wjv6q6xfrb5b9jr16q8ygjy803fay4zcisvaz2") (f (quote (("spin_no_std" "spin"))))))

(define-public crate-lazy_static-1.1.1 (c (n "lazy_static") (v "1.1.1") (d (list (d (n "spin") (r "^0.4.6") (o #t) (d #t) (k 0)) (d (n "version_check") (r "^0.1.4") (d #t) (k 1)))) (h "0az813wdll13lksglr7wj1lr5iiyvx4ya127rx4zpbhr69ldm7nq") (f (quote (("spin_no_std" "nightly" "spin") ("nightly"))))))

