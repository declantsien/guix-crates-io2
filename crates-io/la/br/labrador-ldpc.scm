(define-module (crates-io la br labrador-ldpc) #:use-module (crates-io))

(define-public crate-labrador-ldpc-0.1.0 (c (n "labrador-ldpc") (v "0.1.0") (h "0bgqskxvn7zkiss0vqjcc2f81s3rm21bb0g9g5yilgrzw4v87l7b")))

(define-public crate-labrador-ldpc-1.0.0 (c (n "labrador-ldpc") (v "1.0.0") (h "123cqv3yqgmai8568lgby610k48gpvimvm8zqbw67w8h02l04vmm")))

(define-public crate-labrador-ldpc-1.0.1 (c (n "labrador-ldpc") (v "1.0.1") (h "0j86a09ir038sgk058ranqbs1yp1z4jk9g6d2hqv24agkk2d0xbb")))

(define-public crate-labrador-ldpc-1.1.0 (c (n "labrador-ldpc") (v "1.1.0") (h "0ngl2ycyk120lbffkxi5lc56fr1g1s0s19mn0r7bc8k3ixba1x1g")))

(define-public crate-labrador-ldpc-1.1.1 (c (n "labrador-ldpc") (v "1.1.1") (h "1da51wwnsd8libqhmv6dj0a1485awmvs6vmy85sbfrn15yi9xh93")))

(define-public crate-labrador-ldpc-1.2.0 (c (n "labrador-ldpc") (v "1.2.0") (h "02zs8cxvwnbaq5hx6crjs0xsa4jv2fzf5fjf95bqs2qqw1gx6npq")))

(define-public crate-labrador-ldpc-1.2.1 (c (n "labrador-ldpc") (v "1.2.1") (h "19454krghc180qgh6qvxxllvr863xa1mn2fdic3yjjhy6nisvfsj")))

