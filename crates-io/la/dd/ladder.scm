(define-module (crates-io la dd ladder) #:use-module (crates-io))

(define-public crate-ladder-0.1.0 (c (n "ladder") (v "0.1.0") (d (list (d (n "eframe") (r "^0.20.1") (d #t) (k 0)) (d (n "egui-notify") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1k50wb9wk50k6rvbpazjnk12pdzrb9xmzwnfbhxn6sfx5ggy9vs2")))

(define-public crate-ladder-0.1.2 (c (n "ladder") (v "0.1.2") (d (list (d (n "eframe") (r "^0.20.1") (d #t) (k 0)) (d (n "egui-notify") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tts") (r "^0.25.0") (d #t) (k 0)))) (h "096nsii77zsriqbmvgza9a9qi3ivgx758wrnbdb4v2y4k90k6msc")))

