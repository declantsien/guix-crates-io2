(define-module (crates-io la rp larp) #:use-module (crates-io))

(define-public crate-larp-0.0.1 (c (n "larp") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "warp") (r "^0.1.9") (d #t) (k 0)))) (h "0vpm3n7m4vlhsb8fczs2g4jbl07z8s3lmd8sj4813kparyab3yqw")))

