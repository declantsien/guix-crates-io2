(define-module (crates-io la wn lawn-sftp) #:use-module (crates-io))

(define-public crate-lawn-sftp-0.3.0 (c (n "lawn-sftp") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "flurry") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lawn-constants") (r "^0.3.0") (d #t) (k 0)) (d (n "lawn-fs") (r "^0.3.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("io-std" "io-util" "macros" "net" "process" "rt" "rt-multi-thread" "signal" "sync" "time"))) (d #t) (k 0)))) (h "1lc2x8s3g1qqslck0a8h11wq4c897sl1my69kc31r8ldq0j3d035") (r "1.48.0")))

