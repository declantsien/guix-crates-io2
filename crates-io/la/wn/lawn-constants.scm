(define-module (crates-io la wn lawn-constants) #:use-module (crates-io))

(define-public crate-lawn-constants-0.1.0 (c (n "lawn-constants") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0kn566zrcwjbpf9b4bl9k6grjm07anyjcvaj8bxdpf170kgvl4zf") (r "1.48.0")))

(define-public crate-lawn-constants-0.1.1 (c (n "lawn-constants") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "15j1w7j2ii94wgi89f3zx2xrj29qv6ig73nb5ij652qc80ir42kj") (r "1.48.0")))

(define-public crate-lawn-constants-0.2.0 (c (n "lawn-constants") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "14badf5xa0kigln842z2dgdrklk2gsiljk54vgkrgbc79nnvd2m4") (r "1.48.0")))

(define-public crate-lawn-constants-0.3.0 (c (n "lawn-constants") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rustix") (r "^0.37") (o #t) (d #t) (k 0)))) (h "0lnszvp7afpf5hicrw3ac111cfjmmfs3m2l026nqcvm1hjc0a9aj") (r "1.48.0")))

