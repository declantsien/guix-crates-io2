(define-module (crates-io la wn lawn-fs) #:use-module (crates-io))

(define-public crate-lawn-fs-0.3.0 (c (n "lawn-fs") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "flurry") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lawn-constants") (r "^0.3.0") (f (quote ("rustix"))) (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rustix") (r "^0.37") (f (quote ("fs" "process"))) (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "109jqc8mvqir28yyx1a3s8sncarmxmvxkc1c8zg0vfi868g62i3y") (f (quote (("unix" "rustix") ("default" "unix")))) (r "1.48.0")))

