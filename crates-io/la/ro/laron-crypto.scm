(define-module (crates-io la ro laron-crypto) #:use-module (crates-io))

(define-public crate-laron-crypto-0.1.0 (c (n "laron-crypto") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "laron-primitives") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "secp256k1") (r "^0.24.0") (f (quote ("std" "recovery"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "1lra015ws9pyxbj273x00brlpciq9h2kwawmbjljq6dfc0i0kmsn")))

(define-public crate-laron-crypto-0.1.1 (c (n "laron-crypto") (v "0.1.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "laron-primitives") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "secp256k1") (r "^0.24.0") (f (quote ("std" "recovery"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "1xw9dnlfh977br0ifvpmafh31zc05ffi1qcmlzz1lxil6pv65i0i")))

(define-public crate-laron-crypto-0.1.2 (c (n "laron-crypto") (v "0.1.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "k256") (r "^0.11.5") (f (quote ("ecdsa" "keccak256"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "00bm4j07qdnzpckqax7fvd1rfqwsmjsr5h6h92p0r6cygg3k8zn8")))

(define-public crate-laron-crypto-0.1.3 (c (n "laron-crypto") (v "0.1.3") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "k256") (r "^0.11.5") (f (quote ("ecdsa" "keccak256"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "06mk21szmi7lavcvh4apfcmzqd2icimrgbl3wwx4l421f0vm5isp")))

(define-public crate-laron-crypto-0.1.4 (c (n "laron-crypto") (v "0.1.4") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "horror") (r "^0.1") (d #t) (k 0)) (d (n "k256") (r "^0.11.5") (f (quote ("ecdsa" "keccak256"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (d #t) (k 0)))) (h "09aba6ww8faxipdlsjvx128nanyxzmid9a5az2qp39c132g7rmr7")))

(define-public crate-laron-crypto-0.2.0 (c (n "laron-crypto") (v "0.2.0") (d (list (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "k256") (r "^0.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha3") (r "^0.10.7") (d #t) (k 0)))) (h "0c022vr7n8sp07cgqgva6pvsfhlw6a2x5f00x7j86ypsdlybmlf6")))

(define-public crate-laron-crypto-0.2.1 (c (n "laron-crypto") (v "0.2.1") (d (list (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "k256") (r "^0.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha3") (r "^0.10.7") (d #t) (k 0)))) (h "0iifp5nlfylf5ihw8vvmz6l6y2i3vmvggz5dfx7zfh7i1mxwn6ax")))

(define-public crate-laron-crypto-0.2.2 (c (n "laron-crypto") (v "0.2.2") (d (list (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "k256") (r "^0.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha3") (r "^0.10.7") (d #t) (k 0)))) (h "1zk8vaa6vz19mg90mcyk0l756lsv31pvzm3ajn3czyz8z4xbfh6f")))

(define-public crate-laron-crypto-0.2.3 (c (n "laron-crypto") (v "0.2.3") (d (list (d (n "aes-gcm") (r "^0.10.1") (d #t) (k 0)) (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "constant_time_eq") (r "^0.2.5") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "k256") (r "^0.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "sha3") (r "^0.10.7") (d #t) (k 0)))) (h "1hw5iymgvr6xmgaqax3mx7vppgxxz9ynxdpgabivvb5njqn37qxd")))

