(define-module (crates-io la yl layla-log) #:use-module (crates-io))

(define-public crate-layla-log-0.1.0 (c (n "layla-log") (v "0.1.0") (h "0vv39ka82bzb076wczgf2w7bnh29314rl6s9wxg585l9gm0r8394") (y #t)))

(define-public crate-layla-log-0.1.1 (c (n "layla-log") (v "0.1.1") (h "1c0hcy4vr5x7h3spxpl2jfryq23cinhwj1y1r8r6sawph8zj8gky") (y #t)))

(define-public crate-layla-log-0.2.0 (c (n "layla-log") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)))) (h "1hm5prrgbi1z3mi1247z9axp3v4w0z1wlv818ql4x7lnb37xbx46")))

(define-public crate-layla-log-0.2.1 (c (n "layla-log") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)))) (h "1n0fp35134irfgmllzv8sd4il7ph2kppnfvrkpja6y69kcf79v4d") (y #t)))

(define-public crate-layla-log-0.2.2 (c (n "layla-log") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)))) (h "1h53j8czcm07qv7sfmw3zvg1nixirrpc47sy9n83xhysbg63l4yn")))

(define-public crate-layla-log-0.2.3 (c (n "layla-log") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)))) (h "17325qjw90lbbhxwkvdwm1glkh4fmy4251p7aiidk7ayx1bml3q3")))

(define-public crate-layla-log-0.2.4 (c (n "layla-log") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)))) (h "152rhpah75mvxww1wbphdgqnspr7757fijikh94a8jqm20wsp425")))

(define-public crate-layla-log-0.2.5 (c (n "layla-log") (v "0.2.5") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)))) (h "1lqqxa1j6ha798d1i9370d4hks3i8nkkrnibqngks73jafmvvql5")))

(define-public crate-layla-log-0.2.6 (c (n "layla-log") (v "0.2.6") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)))) (h "150771cgvdpc5502jpmg8yi063jj3valhwhdrrfkxkb2hhdwsb9y")))

(define-public crate-layla-log-0.2.7 (c (n "layla-log") (v "0.2.7") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)))) (h "10zlwdai501r6s1cm5s4dn7xgsg8a4fxk713vkfmr2g46svlrlqz")))

(define-public crate-layla-log-0.2.8 (c (n "layla-log") (v "0.2.8") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)))) (h "1b2spcyczcyr4iqar63a6i3lwx5320vzf431ffs2848cjfyzxpcp")))

