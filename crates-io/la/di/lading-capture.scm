(define-module (crates-io la di lading-capture) #:use-module (crates-io))

(define-public crate-lading-capture-0.1.0 (c (n "lading-capture") (v "0.1.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "uuid") (r "^1.2") (f (quote ("v4" "serde" "serde" "v4"))) (k 0)))) (h "10b944r119kndivxsyaa3cf1z4zk15bkvqhf6ssz9glkq1bb9vwm")))

(define-public crate-lading-capture-0.1.1 (c (n "lading-capture") (v "0.1.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "uuid") (r "^1.2") (f (quote ("v4" "serde" "serde" "v4"))) (k 0)))) (h "1fqn6x0697jflvr44a0l03jc788hkdglqf02n8xqh21d8wf82krq")))

