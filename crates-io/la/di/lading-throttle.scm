(define-module (crates-io la di lading-throttle) #:use-module (crates-io))

(define-public crate-lading-throttle-0.1.0 (c (n "lading-throttle") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (k 0)) (d (n "metrics") (r "^0.21") (d #t) (k 0)) (d (n "proptest") (r "^1.2") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1avmnrrn4wxxbl5056fny4dj6iid53jkbjwyzvapmnc3nlycr0cj")))

