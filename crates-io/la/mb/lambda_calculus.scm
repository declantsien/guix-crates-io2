(define-module (crates-io la mb lambda_calculus) #:use-module (crates-io))

(define-public crate-lambda_calculus-0.1.0 (c (n "lambda_calculus") (v "0.1.0") (h "04hf97z1v6sm689hjy5xx97qx96xrjspp5pghybyllr0sy29khyl")))

(define-public crate-lambda_calculus-0.1.1 (c (n "lambda_calculus") (v "0.1.1") (h "1cfybhkgdl6aqfshvr67l0cinkjk1jyb7fp42w7di95k5hr59qkh")))

(define-public crate-lambda_calculus-0.1.2 (c (n "lambda_calculus") (v "0.1.2") (h "1gvdcg0q65dhnsasdm4r9hn41pmdkpmiqsyhff88zzsg05sq92wz")))

(define-public crate-lambda_calculus-0.1.3 (c (n "lambda_calculus") (v "0.1.3") (h "05zrccf8slgd7dk8kq10y2315pf31qi37kjxld5dnb7p6fx65k9r")))

(define-public crate-lambda_calculus-0.1.4 (c (n "lambda_calculus") (v "0.1.4") (h "1q9mi3sb13iz7a679fvc3fly2dinyh97p7f6nmvvfwy5926hm903")))

(define-public crate-lambda_calculus-0.1.5 (c (n "lambda_calculus") (v "0.1.5") (h "0r1289z90qh9kg67v9p6h7y84asa2szy58s8x8kbf85hjmc2j9h1")))

(define-public crate-lambda_calculus-0.1.6 (c (n "lambda_calculus") (v "0.1.6") (h "117qdg045mh9xa6wsjahahhw5hwhyh42cyc3px742dk4bzfds0qi")))

(define-public crate-lambda_calculus-0.2.0 (c (n "lambda_calculus") (v "0.2.0") (h "16rna2k9ksy51shfhjg66d2f7yssfq6dvskc487nnkfqwh49blvn")))

(define-public crate-lambda_calculus-0.2.1 (c (n "lambda_calculus") (v "0.2.1") (h "0z9y1s49p4sa2idhbc31a4w2pi9py62kzpk9iwp35c2g1p8fvzr0")))

(define-public crate-lambda_calculus-0.3.0 (c (n "lambda_calculus") (v "0.3.0") (h "09zpw57248zl9gkcppx6m1mb5q898dh9mab8sr2wz6y1lyqz3g3p")))

(define-public crate-lambda_calculus-0.4.0 (c (n "lambda_calculus") (v "0.4.0") (h "1ybc6dlxnd7p57vb5lv7cxfvis7qyy5wk6gk859vbymagd96vasn")))

(define-public crate-lambda_calculus-0.4.1 (c (n "lambda_calculus") (v "0.4.1") (h "0f6di31fvimjvc9wdq99b4g4y0nfnaq626ahr9sq44bx32hww68k")))

(define-public crate-lambda_calculus-0.5.0 (c (n "lambda_calculus") (v "0.5.0") (h "1981djrapxx6d5h3i0s7qz8njlzl2d645dx6ngq7jwpfiy2xx9x1")))

(define-public crate-lambda_calculus-0.5.1 (c (n "lambda_calculus") (v "0.5.1") (h "1ykn549y6x55ibc35xsfnpriqlp21hkpi159s9dzs95q2qpyjqh2")))

(define-public crate-lambda_calculus-0.6.0 (c (n "lambda_calculus") (v "0.6.0") (h "1k6nk1kjncjljaszsjr9yfyqg92w08181cd83h3wc5phaslr95b4")))

(define-public crate-lambda_calculus-0.6.1 (c (n "lambda_calculus") (v "0.6.1") (h "03gsk0sa7ip4vqi7fqnmmmv7vhpmmf4ghqiycbdp5iy902rwl21c")))

(define-public crate-lambda_calculus-0.6.2 (c (n "lambda_calculus") (v "0.6.2") (h "129ly6djckmkngyabxskyv7lcqvs6kb6jy6fn1pllhw9xqyq4ykc")))

(define-public crate-lambda_calculus-0.7.0 (c (n "lambda_calculus") (v "0.7.0") (h "0yjdyyybzm71ac0mi9yml0qdz2chhhgvx5shvqyc8d9dfphxkkm3")))

(define-public crate-lambda_calculus-0.8.0 (c (n "lambda_calculus") (v "0.8.0") (h "1l2zfgcjxsp3113gkqn4v9sqab06668pzz0xlyry7nz2mj7bgw3i")))

(define-public crate-lambda_calculus-0.9.0 (c (n "lambda_calculus") (v "0.9.0") (h "1bdabbrivw4nl60lpv8xzlk3nvnjjpyvqs4q4p6hyqr8qzl9rh6s")))

(define-public crate-lambda_calculus-0.10.0 (c (n "lambda_calculus") (v "0.10.0") (h "1b4jlswj4a0zpfwb1snkgxnxxljv5dsgml8cw3b3xrn54inxd95r")))

(define-public crate-lambda_calculus-0.10.1 (c (n "lambda_calculus") (v "0.10.1") (h "00ny0ccf0ls6gh4brrkrny04h21f96h1zs1ivzbs3lknysidaz28")))

(define-public crate-lambda_calculus-0.11.0 (c (n "lambda_calculus") (v "0.11.0") (h "1awlfykzvjypd8pwh3zjqaa0ks3aq8id1ba4cs1li4r26scp3ikq")))

(define-public crate-lambda_calculus-0.12.0 (c (n "lambda_calculus") (v "0.12.0") (h "1ki5kwixy62a3hzd1igwvz4vcqidbylvqhbsgh2kmshpsdp5yp1j")))

(define-public crate-lambda_calculus-1.0.0 (c (n "lambda_calculus") (v "1.0.0") (h "1mrj1f49cmqc2v4ddcgrk4acdy24qvmn75h5b7sx5x8qvamn4a7h") (f (quote (("no_church") ("backslash_lambda"))))))

(define-public crate-lambda_calculus-1.1.0 (c (n "lambda_calculus") (v "1.1.0") (h "1rw7gvrd9ghmqspkg4hpyycm4l9w6llw8w3szcdw0gglvx8401id") (f (quote (("no_church") ("backslash_lambda"))))))

(define-public crate-lambda_calculus-1.1.1 (c (n "lambda_calculus") (v "1.1.1") (h "102jf9wswps4zbz4klpxgq0yfvf3sh0y3r5f363rhgfyk72ibc5i") (f (quote (("no_church") ("backslash_lambda"))))))

(define-public crate-lambda_calculus-1.2.0 (c (n "lambda_calculus") (v "1.2.0") (h "17v5nh91qcmfq0wlsw44q1kl6xjzwq5c5aclfvphfqkkgy24s7bd") (f (quote (("no_church") ("backslash_lambda"))))))

(define-public crate-lambda_calculus-1.3.0 (c (n "lambda_calculus") (v "1.3.0") (h "0r48a4m7gypx6lcvsjlnf5g2nnyp3r9m6kbahf9l16qnzz2g7k9z") (f (quote (("no_church") ("backslash_lambda"))))))

(define-public crate-lambda_calculus-1.4.0 (c (n "lambda_calculus") (v "1.4.0") (h "06iq73bj41y3ihv6b8k2vr0f2m573zs2k67jaf3c0kasydp4jqig") (f (quote (("scott") ("parigot") ("no_church") ("backslash_lambda"))))))

(define-public crate-lambda_calculus-2.0.0 (c (n "lambda_calculus") (v "2.0.0") (h "0vf3win0lm54qfw09j0yn8i4ri7xhcb50lmz1azcyhddd9rv85l0") (f (quote (("encoding") ("default" "encoding") ("backslash_lambda"))))))

(define-public crate-lambda_calculus-2.1.0 (c (n "lambda_calculus") (v "2.1.0") (h "07358q0z5grwr6zj94h064x69qkn32ivl2jmk344iw0nhkjypi6l") (f (quote (("encoding") ("default" "encoding") ("backslash_lambda"))))))

(define-public crate-lambda_calculus-2.2.0 (c (n "lambda_calculus") (v "2.2.0") (h "1h77v4wcf6agckwa8fpdwkaayrpcza759lmr4nwy8kcsk558b2yr") (f (quote (("encoding") ("default" "encoding") ("backslash_lambda"))))))

(define-public crate-lambda_calculus-3.0.0 (c (n "lambda_calculus") (v "3.0.0") (h "0sygygvj790yygqqlsvc0hjqgz5vckw8nyhl3pfas2dy44kzj3pb") (f (quote (("encoding") ("default" "encoding") ("backslash_lambda"))))))

(define-public crate-lambda_calculus-3.0.1 (c (n "lambda_calculus") (v "3.0.1") (h "1f0sws68g8d95szpa74qnj5jqfrzqpiq2dbpnjl340fbrl5qv6qz") (f (quote (("encoding") ("default" "encoding") ("backslash_lambda"))))))

(define-public crate-lambda_calculus-3.0.2 (c (n "lambda_calculus") (v "3.0.2") (h "166196fz6aj9r8g5hh4mmyhhyfbfprrym0nvidmgxzj8apvh4idw") (f (quote (("encoding") ("default" "encoding") ("backslash_lambda"))))))

(define-public crate-lambda_calculus-3.1.0 (c (n "lambda_calculus") (v "3.1.0") (h "19ch4ghyqqwr9r1vgmlvs33ibpglvggv4w99486ah6j3q976ih8q") (f (quote (("encoding") ("default" "encoding") ("backslash_lambda"))))))

(define-public crate-lambda_calculus-3.2.0 (c (n "lambda_calculus") (v "3.2.0") (h "1cak1jph3l2fdd5af5bv5kwawfryvidr2n0qfv6w0c4609k07jdi") (f (quote (("encoding") ("default" "encoding") ("backslash_lambda"))))))

(define-public crate-lambda_calculus-3.2.1 (c (n "lambda_calculus") (v "3.2.1") (h "1131rflxg7dyvc06qakn0g0ma51vkr4lvpfyfvkv5fgjxxg6jp8c") (f (quote (("encoding") ("default" "encoding") ("backslash_lambda"))))))

(define-public crate-lambda_calculus-3.2.2 (c (n "lambda_calculus") (v "3.2.2") (h "0f9364268v9dk37wavm5ck7chv5im77kmzbwc1dk3s6ssci7dh4f") (f (quote (("encoding") ("default" "encoding") ("backslash_lambda"))))))

