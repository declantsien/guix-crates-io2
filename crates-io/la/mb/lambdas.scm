(define-module (crates-io la mb lambdas) #:use-module (crates-io))

(define-public crate-lambdas-0.1.0 (c (n "lambdas") (v "0.1.0") (d (list (d (n "egg") (r "^0.7.1") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "sexp") (r "^1.1.4") (d #t) (k 0)))) (h "0zn375awc01pcy8clw9nar4czk6p97xrspznmcsn0rj23yjrj1hk")))

