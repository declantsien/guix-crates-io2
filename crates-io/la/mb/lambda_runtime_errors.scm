(define-module (crates-io la mb lambda_runtime_errors) #:use-module (crates-io))

(define-public crate-lambda_runtime_errors-0.1.0 (c (n "lambda_runtime_errors") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lambda_runtime_errors_derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "01m6q0l1a3sbspbz5c205bhgrw7k4zdw9s4l8d8qd7sywcj55ipd")))

(define-public crate-lambda_runtime_errors-0.1.1 (c (n "lambda_runtime_errors") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lambda_runtime_core") (r "^0.1") (d #t) (k 2)) (d (n "lambda_runtime_errors_derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "02n2bbk5xj2d7kmrswi4srk16j6cj4r5yg34lnm69wcdd2ikxdlk")))

