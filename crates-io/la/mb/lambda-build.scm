(define-module (crates-io la mb lambda-build) #:use-module (crates-io))

(define-public crate-lambda-build-0.9.0 (c (n "lambda-build") (v "0.9.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "users") (r "^0.10") (d #t) (k 0)))) (h "07ynar7dl7blay7sgiibzky4x2ya5g02gza7hnl2q2y3m8mkwikm") (y #t)))

(define-public crate-lambda-build-1.0.0 (c (n "lambda-build") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.11") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "users") (r "^0.10") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "00ywsymwwbn9hnf74kki8pwv6pdyffvv24d4qhxw4yalbkwy1gs8") (y #t)))

(define-public crate-lambda-build-1.1.0 (c (n "lambda-build") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.11") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "users") (r "^0.10") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "0qwaas18iwa5z2fsmibrqal8zilb2lqkxlkf95bclal0ihwi20vm") (y #t)))

(define-public crate-lambda-build-1.2.0 (c (n "lambda-build") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.11") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "users") (r "^0.10") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "0qiy93sij1if08dwkffjy6f2rsy608b0h6782cmy08h72psm260h") (y #t)))

(define-public crate-lambda-build-1.3.0 (c (n "lambda-build") (v "1.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.11") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "users") (r "^0.10") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "1dsr05kg29jss4dcszh7l1fhr8yb9wyyswkbczk4238pfjy8g1hn") (y #t)))

(define-public crate-lambda-build-1.4.0 (c (n "lambda-build") (v "1.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.11") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fehler") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "users") (r "^0.10") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "054x8ii7xbaahxlh6ckh0pmc512y5gmrc8x62cr8n45h77w19xcr") (y #t)))

(define-public crate-lambda-build-0.0.1-alpha (c (n "lambda-build") (v "0.0.1-alpha") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)))) (h "0g3rhqicmgfy7c4kbw3vavps4rpvw055l50m3agf73rv2rjg5dzj")))

