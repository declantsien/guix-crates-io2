(define-module (crates-io la mb lambda_sqs) #:use-module (crates-io))

(define-public crate-lambda_sqs-0.1.0 (c (n "lambda_sqs") (v "0.1.0") (h "1ns7gxz7504p8daxih2p4m16rwqd03b4jw9x72q6swdrwanjrcr2")))

(define-public crate-lambda_sqs-0.2.0 (c (n "lambda_sqs") (v "0.2.0") (d (list (d (n "lambda_runtime") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "1rxn90davr1wyw9g930snmzxs9kiy091v0sqp856s9nbih2mmvfr")))

(define-public crate-lambda_sqs-0.2.1 (c (n "lambda_sqs") (v "0.2.1") (d (list (d (n "lambda_runtime") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.135") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "1m0b7c33h45rrhgxrvl87kh7jj3l2ha3907aqjyfxv3wzicqqbd5")))

(define-public crate-lambda_sqs-0.2.2 (c (n "lambda_sqs") (v "0.2.2") (d (list (d (n "lambda_runtime") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "14sc0nbd4vch32xjplhvm6prgksjh7salv4mbyivrsbnsi8f6152")))

(define-public crate-lambda_sqs-0.2.3 (c (n "lambda_sqs") (v "0.2.3") (d (list (d (n "lambda_runtime") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "086hfrh6k4vw09jxfyywgd1ybryq5y3rhz479an50gkjcx3b4fdc")))

(define-public crate-lambda_sqs-0.2.4 (c (n "lambda_sqs") (v "0.2.4") (d (list (d (n "lambda_runtime") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 2)))) (h "12xavnmwz503ff7cdan3lv6jkhmkwai4hh23hb82hglgln5i08w8")))

