(define-module (crates-io la mb lambda-rs-args) #:use-module (crates-io))

(define-public crate-lambda-rs-args-2023.1.27 (c (n "lambda-rs-args") (v "2023.1.27") (h "081hb7m4ysf47x17mfah9b92vp8l8iif2z26f69vvr2cm355r3li")))

(define-public crate-lambda-rs-args-2023.1.27-1 (c (n "lambda-rs-args") (v "2023.1.27-1") (h "01f6cqljn1sgsr5zcysbw9l2jp98ji2rd8hr752827qb4dwf79jl")))

(define-public crate-lambda-rs-args-2023.1.28 (c (n "lambda-rs-args") (v "2023.1.28") (h "18n34kis5szmds5wyn8abmb2laqy5bxfsv9zynn1bckzlwz9808y")))

