(define-module (crates-io la mb lambda-router-macros) #:use-module (crates-io))

(define-public crate-lambda-router-macros-0.1.0 (c (n "lambda-router-macros") (v "0.1.0") (d (list (d (n "lazy-regex") (r "^3.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0ydhyhsdzs61s7zjmfh4wh904bcmff734nlf38brvhqy3y37lfbj")))

(define-public crate-lambda-router-macros-0.1.1 (c (n "lambda-router-macros") (v "0.1.1") (d (list (d (n "lazy-regex") (r "^3.0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.31") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0ylwlpik899hmzgg3vpyddaa0hyx828s7b5ggv9w5qxh50w66s07")))

(define-public crate-lambda-router-macros-0.1.2 (c (n "lambda-router-macros") (v "0.1.2") (d (list (d (n "lazy-regex") (r "^3.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1qflal461jg9abq8kzmgzhbxxc7b4y04cahsl3wk93a5ic4nbv1i")))

