(define-module (crates-io la mb lambda_runtime_errors_derive) #:use-module (crates-io))

(define-public crate-lambda_runtime_errors_derive-0.1.0 (c (n "lambda_runtime_errors_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "1h1mdy9nklyjzx978iw7k9cj9im28i3inlfg59qb5kna61gdmsvf")))

(define-public crate-lambda_runtime_errors_derive-0.1.1 (c (n "lambda_runtime_errors_derive") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 2)) (d (n "lambda_runtime_errors") (r "^0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "synstructure") (r "^0.10") (d #t) (k 0)))) (h "1bvvda1j3s3dli50zhgzp00kzj1m20jkss09l5qj7vpiqsg63yiq")))

