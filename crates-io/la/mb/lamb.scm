(define-module (crates-io la mb lamb) #:use-module (crates-io))

(define-public crate-lamb-0.1.0 (c (n "lamb") (v "0.1.0") (d (list (d (n "ariadne") (r "^0.1.5") (o #t) (d #t) (k 0)) (d (n "chumsky") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (o #t) (d #t) (k 0)))) (h "0fzz9p62rr3n0ir350z0a47mlgyws429dzn5s62bw41cvmi9mh8y") (f (quote (("prelude")))) (s 2) (e (quote (("repl" "dep:ariadne" "dep:chumsky" "dep:logos"))))))

