(define-module (crates-io la mb lambda-query) #:use-module (crates-io))

(define-public crate-lambda-query-0.1.0 (c (n "lambda-query") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "jwalk") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0nxi6wamhkv05wz54d9c335gp62xsf0qh3nhw3wzfdyv8bc096yq")))

