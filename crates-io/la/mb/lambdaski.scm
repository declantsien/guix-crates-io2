(define-module (crates-io la mb lambdaski) #:use-module (crates-io))

(define-public crate-lambdaski-0.0.1 (c (n "lambdaski") (v "0.0.1") (h "0876wzvr7cvvc6n3mkzgpxmm8v3907r0mjv0gqnbgzk0wrxcvgn2")))

(define-public crate-lambdaski-0.0.2 (c (n "lambdaski") (v "0.0.2") (h "1x841ngk67qy0268h7zc7277arzdzb8pdp90r5swgcxnayl7sndl")))

