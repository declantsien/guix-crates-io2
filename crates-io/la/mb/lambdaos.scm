(define-module (crates-io la mb lambdaos) #:use-module (crates-io))

(define-public crate-lambdaOS-0.1.0 (c (n "lambdaOS") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.7.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.4") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "linked_list_allocator") (r "^0.4.3") (d #t) (k 0)) (d (n "multiboot2") (r "^0.1.0") (d #t) (k 0)) (d (n "once") (r "^0.3.3") (d #t) (k 0)) (d (n "rlibc") (r "^1.0") (d #t) (k 0)) (d (n "spin") (r "^0.4.5") (d #t) (k 0)) (d (n "volatile") (r "^0.1.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.1.2") (d #t) (k 0)))) (h "04lcxs0z35srjxak0knx2hcivaj801kansx0cgmvhk5w9g5hnzhc") (f (quote (("us") ("uk") ("default" "uk")))) (y #t)))

