(define-module (crates-io la mb lambek) #:use-module (crates-io))

(define-public crate-lambek-0.1.0 (c (n "lambek") (v "0.1.0") (h "17wy1fzqvcs178hq3ybv9m9i3k2fv4gmrgpkm8a8apk57bsyyr9f")))

(define-public crate-lambek-0.1.1 (c (n "lambek") (v "0.1.1") (h "0vhc2vipkjad4ymkxwlmanr33xs1vkkfrncr4fv4shhv8wwag0d4")))

