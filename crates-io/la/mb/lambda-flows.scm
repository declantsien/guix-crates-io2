(define-module (crates-io la mb lambda-flows) #:use-module (crates-io))

(define-public crate-lambda-flows-0.1.0 (c (n "lambda-flows") (v "0.1.0") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0cgfki0wprrla46vfnh4lai8m7blzwfrfhbfzadakqlg630irr11")))

(define-public crate-lambda-flows-0.1.1 (c (n "lambda-flows") (v "0.1.1") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1zirlxm6lbd6lfmr03zkxr356m19z8izk6b20v5zj8vk12imag45")))

(define-public crate-lambda-flows-0.1.2 (c (n "lambda-flows") (v "0.1.2") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0bnbvp000rj1i0izb5yvmq9mzf4mrsks3rh5bsyx4xv6pzn8kbly")))

(define-public crate-lambda-flows-0.1.3 (c (n "lambda-flows") (v "0.1.3") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "12m7vshybz9i6sr80awkdbz01xglxb4m9ydj3bx0swhi4h8isrbf")))

(define-public crate-lambda-flows-0.1.4 (c (n "lambda-flows") (v "0.1.4") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1rg6gnlfgnyif4bp6wnkxiw9k8d7g6s2fxzg3i2qnwbqsrhgf6my")))

(define-public crate-lambda-flows-0.1.5 (c (n "lambda-flows") (v "0.1.5") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1svwrgqxnf648qmdx0kb5rqbsydm009chkd4bpzxh46a34b8f4mh")))

(define-public crate-lambda-flows-0.1.6 (c (n "lambda-flows") (v "0.1.6") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "09jswjbclvwic6shpz8dr1d7j1fqzv0jf1qh9my6h12wkx904f1z")))

(define-public crate-lambda-flows-0.1.7 (c (n "lambda-flows") (v "0.1.7") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "1y11qah4prkp09v4wafawwm1ra66w5yk80jjahdwmlal4armiilx")))

(define-public crate-lambda-flows-0.1.8 (c (n "lambda-flows") (v "0.1.8") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0lbc0d69ybjignqc1wk9758kjgd3h5s3g1rv66xf9kkpcpxjdjif")))

(define-public crate-lambda-flows-0.2.0 (c (n "lambda-flows") (v "0.2.0") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0phcz5ylihk7n0zlbs3d8bbq5j43md5jrsyfrw7w2vx6hrp772y6")))

(define-public crate-lambda-flows-0.2.1 (c (n "lambda-flows") (v "0.2.1") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0bwyj9njsjbd0djcscyxafxr08p9a5csqxnrr559768m11gr0rhv")))

(define-public crate-lambda-flows-0.2.2 (c (n "lambda-flows") (v "0.2.2") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0m7b8rnnva41pv8pys9a8kyh222g5zqcj1hw97rcdizdz20dw1hj")))

(define-public crate-lambda-flows-0.3.0 (c (n "lambda-flows") (v "0.3.0") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "05rk0dp06gi1nvxz7xfq5xv38czfkb5flk57f4jk4f7bkzhwkph7")))

(define-public crate-lambda-flows-0.3.1 (c (n "lambda-flows") (v "0.3.1") (d (list (d (n "http_req_wasi") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0p44kvj6qq2lfdg6gf5ags2hxh4dx798vhxcrclnm110qp81379r")))

(define-public crate-lambda-flows-0.4.0 (c (n "lambda-flows") (v "0.4.0") (d (list (d (n "http_req_wasi") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "0pxcs1nvkv2plfy7d5dnhs6hyprs1a64vl3rzqbm75b0mg18xbng")))

(define-public crate-lambda-flows-0.4.1 (c (n "lambda-flows") (v "0.4.1") (d (list (d (n "http_req_wasi") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlencoding") (r "^2") (d #t) (k 0)))) (h "11cz7qs1z1ypwv023fvnxpxbp7s6sc4sr8hkfsc3wfwc4g0zd31l")))

