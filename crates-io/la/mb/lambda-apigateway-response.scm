(define-module (crates-io la mb lambda-apigateway-response) #:use-module (crates-io))

(define-public crate-lambda-apigateway-response-0.1.0 (c (n "lambda-apigateway-response") (v "0.1.0") (d (list (d (n "http") (r "^0.2.7") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0v3qsnckmqpchpdy6a8j009f25qndlxbf9w32y0hn2p918qwdp3c")))

(define-public crate-lambda-apigateway-response-0.1.1 (c (n "lambda-apigateway-response") (v "0.1.1") (d (list (d (n "http") (r "^0.2.7") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wfs873s8zd1kzldj724hy9p8fgmxkgb8ch9rcx8wqm4fp6g3y9x")))

