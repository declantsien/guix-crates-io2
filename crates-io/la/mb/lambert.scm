(define-module (crates-io la mb lambert) #:use-module (crates-io))

(define-public crate-lambert-0.1.0 (c (n "lambert") (v "0.1.0") (h "0vfgx4mi0fsgd33c0lphnkw73qqz47dapr5fhrvs7r1n1swqh2cg")))

(define-public crate-lambert-1.0.0 (c (n "lambert") (v "1.0.0") (h "1rkb98wg1hfm7s0r1qw04bgir223sn316v3p1rq3gwqzq2gsvmgr")))

