(define-module (crates-io la mb lambdascript) #:use-module (crates-io))

(define-public crate-lambdascript-0.1.0 (c (n "lambdascript") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fixedstr") (r "^0.1") (d #t) (k 0)) (d (n "rustlr") (r "^0.2") (d #t) (k 0)))) (h "090x9b2q37drsv4p0pcjhb6sipv8gfmmlqa7js2g8jpkcy9m5gdd")))

(define-public crate-lambdascript-0.1.1 (c (n "lambdascript") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fixedstr") (r "^0.2") (d #t) (k 0)) (d (n "rustlr") (r "^0.2") (d #t) (k 0)))) (h "0l1bnz3qrnsxi5j5n2q414h6m96hw0z6qc6b5hzpbg823f9gv2ss")))

(define-public crate-lambdascript-0.1.2 (c (n "lambdascript") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fixedstr") (r "^0.2.8") (d #t) (k 0)) (d (n "rustlr") (r "^0.2.4") (d #t) (k 0)))) (h "0xp4rmlaw3xqiklb3rrfkiz7lyr1ryxadqirc7nq35ladkl1izkj") (y #t)))

(define-public crate-lambdascript-0.1.3 (c (n "lambdascript") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fixedstr") (r "^0.2.8") (d #t) (k 0)) (d (n "rustlr") (r "^0.2.4") (d #t) (k 0)))) (h "0q5sfxdr896fvnapd6gkdmnrx3l3zsl5m60pqdjrbdgrngaz2c4w")))

(define-public crate-lambdascript-0.1.31 (c (n "lambdascript") (v "0.1.31") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fixedstr") (r "^0.2.8") (d #t) (k 0)) (d (n "rustlr") (r "^0.2.9") (d #t) (k 0)))) (h "0pvfxplv7a01fl120c7r0y4kdxfqlls3fj3yp1zcg0n4b0c8nigw")))

(define-public crate-lambdascript-0.1.4 (c (n "lambdascript") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fixedstr") (r "^0.2.8") (d #t) (k 0)) (d (n "rustlr") (r "^0.3.5") (d #t) (k 0)))) (h "02xs1777jjcm5fyl0in4sc3fyi53c0zry87g2q1jbp9sq6zb5286")))

(define-public crate-lambdascript-0.1.41 (c (n "lambdascript") (v "0.1.41") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fixedstr") (r "^0.2.8") (d #t) (k 0)) (d (n "rustlr") (r "^0.3.5") (d #t) (k 0)))) (h "026fggq79ikblnva3kv2wpjxh6za208a9cl17jxikqlhxb2w23cj")))

(define-public crate-lambdascript-0.1.5 (c (n "lambdascript") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fixedstr") (r "^0.2.9") (d #t) (k 0)) (d (n "rustlr") (r "^0.4") (d #t) (k 0)))) (h "0ila653r9rgpzil3ma0x2czgydqq9z1n0gizr8xb8vmhj8pni5w7")))

(define-public crate-lambdascript-0.1.50 (c (n "lambdascript") (v "0.1.50") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fixedstr") (r "^0.2.9") (d #t) (k 0)) (d (n "rustlr") (r "^0.4") (d #t) (k 0)))) (h "0vkm4a2irv63jfg5z0di62ia8jg3cl5g5pnwbfbvr9m43ig5cw8f")))

(define-public crate-lambdascript-0.1.51 (c (n "lambdascript") (v "0.1.51") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fixedstr") (r "^0.2.9") (d #t) (k 0)) (d (n "rustlr") (r "^0.4") (d #t) (k 0)))) (h "04ggxxg9xsj2fqw2sclh1qyll7ivwsjb8j65yy1nd795a04pnhy8")))

(define-public crate-lambdascript-0.1.52 (c (n "lambdascript") (v "0.1.52") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "fixedstr") (r "^0.2.9") (d #t) (k 0)) (d (n "rustlr") (r "^0.4") (d #t) (k 0)))) (h "1scmd6pahvjg0d9c2dcwilvqxlvakzkfam858nplyccsvhfd0mra")))

(define-public crate-lambdascript-0.1.53 (c (n "lambdascript") (v "0.1.53") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "fixedstr") (r "^0.2.9") (d #t) (k 0)) (d (n "rustlr") (r "^0.4") (d #t) (k 0)))) (h "0jyrn1fbx3sg0fl5gxw0xpvqp3xlkifgsqppib5wbp44fhf2ifbh")))

(define-public crate-lambdascript-0.1.54 (c (n "lambdascript") (v "0.1.54") (d (list (d (n "fixedstr") (r "^0.2.9") (d #t) (k 0)) (d (n "rustlr") (r "^0.4") (d #t) (k 0)))) (h "0nbim6702kxr63s310jvjrr9w18qx7ifzxfy2rfkqlbj62zamhjs")))

(define-public crate-lambdascript-0.1.55 (c (n "lambdascript") (v "0.1.55") (d (list (d (n "fixedstr") (r "^0.5") (d #t) (k 0)) (d (n "rustlr") (r "^0.4") (d #t) (k 0)))) (h "0w4cmzjvhifkkak5lixy91xf5iwy0s3ygyyay88qy8ir0yvkyqb5")))

(define-public crate-lambdascript-0.1.56 (c (n "lambdascript") (v "0.1.56") (d (list (d (n "fixedstr") (r "^0.5") (f (quote ("no-alloc"))) (d #t) (k 0)) (d (n "rustlr") (r "^0.5") (k 0)))) (h "0snby9gq0j50z5k2cwlffpq2xk4mmmgd4gd4ryl9bvpaffarwn5h")))

(define-public crate-lambdascript-0.2.0 (c (n "lambdascript") (v "0.2.0") (d (list (d (n "fixedstr") (r "^0.5") (f (quote ("no-alloc"))) (d #t) (k 0)) (d (n "rustlr") (r "^0.5") (k 0)))) (h "1hsl4wgxgzs62fav6p58r6h80f3f8y8jd4ssylapsirwhyrl6v79")))

(define-public crate-lambdascript-0.2.1 (c (n "lambdascript") (v "0.2.1") (d (list (d (n "fixedstr") (r "^0.5") (f (quote ("no-alloc"))) (d #t) (k 0)) (d (n "rustlr") (r "^0.5") (k 0)))) (h "1pwcg143mgjqs8m63jixvjncwdsxdgv23xy1rgy8dzl1blghqvs3")))

