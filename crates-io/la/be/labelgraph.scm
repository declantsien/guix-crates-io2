(define-module (crates-io la be labelgraph) #:use-module (crates-io))

(define-public crate-labelgraph-0.1.0 (c (n "labelgraph") (v "0.1.0") (h "03ailq6815d9sgb2qqfg7iwvmw4w9nas2ardxc17m3f574pwj417")))

(define-public crate-labelgraph-0.1.1 (c (n "labelgraph") (v "0.1.1") (h "0976zq7ybnbvgi09x5lw4f34d79scdlkb1idzilikrxynqsbqdw5")))

(define-public crate-labelgraph-0.1.2 (c (n "labelgraph") (v "0.1.2") (h "1444mwpbkkffhf17xd6fb2sz1zigbwm1h70wlqjnb2zxcjnx6naf")))

(define-public crate-labelgraph-0.1.3 (c (n "labelgraph") (v "0.1.3") (h "18fyl746lqxfk9w1d86iy235d97yg2f3jhmfc5rzsyp0fqc8fl7h")))

(define-public crate-labelgraph-0.1.4 (c (n "labelgraph") (v "0.1.4") (h "0gxidl3n4dvybifq3aw7b30azg1k90ag8kf9433xdfx93s183nqf")))

(define-public crate-labelgraph-0.1.5 (c (n "labelgraph") (v "0.1.5") (h "0gpz2zha5xhxq12zrl3j4hhzss90rpqlp5r77yb23ap7msywwraj")))

(define-public crate-labelgraph-0.1.6 (c (n "labelgraph") (v "0.1.6") (h "020cm8s4z89wvc78ycsv2f68247n6561ibs9931clwg9043klbkl")))

(define-public crate-labelgraph-1.0.0 (c (n "labelgraph") (v "1.0.0") (h "02qpbwpv0840bnvlqimn87g9ggy2dm604klv04xczpm41jq8lnhq")))

(define-public crate-labelgraph-1.0.2 (c (n "labelgraph") (v "1.0.2") (h "1z7w15kh39l4c8qb7rh9y4zxiy32m1izpmpf9yyqvv9n8v060z4p")))

(define-public crate-labelgraph-2.0.0 (c (n "labelgraph") (v "2.0.0") (h "12q10gapw9cmiifyak3jh3x79fnvhyvv6yn983a0hk40di6vpzh1")))

(define-public crate-labelgraph-2.1.0 (c (n "labelgraph") (v "2.1.0") (h "1w7h1hc8jzrpxlcl4lr905xn94xrq866q58bz5nkwyx63m0mi90h")))

(define-public crate-labelgraph-2.1.1 (c (n "labelgraph") (v "2.1.1") (h "02gbqj9c7ys1xqhbgw384s5b8bcncv1l8554hbbrx7rw86ncrkja")))

(define-public crate-labelgraph-2.2.1 (c (n "labelgraph") (v "2.2.1") (h "1jrznf9hb9ip3kga4b1phxhwmy52qn33g9i04h7ddfgkmahavcbq")))

(define-public crate-labelgraph-2.2.2 (c (n "labelgraph") (v "2.2.2") (h "0ncmlyi6hwvvca23d71pz66gj7kgg2r63y216dxmxqpb9agzn56w")))

(define-public crate-labelgraph-2.2.3 (c (n "labelgraph") (v "2.2.3") (h "0fyhqnskgx6aqmsdig765fv77zcd96ma75dxr48yynfdxpwdih3a")))

