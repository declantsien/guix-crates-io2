(define-module (crates-io la be labeled) #:use-module (crates-io))

(define-public crate-labeled-0.1.0 (c (n "labeled") (v "0.1.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b6w0dbqhwhijy1x3pb17mz35l185f9qfvwl034lrav84qghg9vb") (f (quote (("default" "dclabel" "buckle") ("dclabel") ("buckle"))))))

