(define-module (crates-io la be label-macros) #:use-module (crates-io))

(define-public crate-label-macros-0.1.0 (c (n "label-macros") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "1lwvaxr1gqa5wn198jqlngjpm7wzmss2s48pn7xxk6n36r5ik0lz")))

(define-public crate-label-macros-0.1.1 (c (n "label-macros") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "1ycnq417kmh80i99zjsqn219k5vcp7zhk3q1kbwlc2q16qiv7rai")))

(define-public crate-label-macros-0.1.2 (c (n "label-macros") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "0v9fyyics8hr32kvl0gi17s36sd2cssqza0bsswcj95k0qjxjxw9")))

(define-public crate-label-macros-0.1.3 (c (n "label-macros") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "1vix2p4sx5qn5qxnp1mr3ahqqgl1f60r2pzzc0y1ds4fjx0pvzgm")))

(define-public crate-label-macros-0.1.4 (c (n "label-macros") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "0lm60h5m3mgkhjrrrkzhhnf08h27jnb78h3xlfssj31rnmwa1570")))

(define-public crate-label-macros-0.1.5 (c (n "label-macros") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "01r14famfhl8x9664qmh6xqd5qw9y1bxprb11jm38zy7xrrzxxlz")))

(define-public crate-label-macros-0.1.6 (c (n "label-macros") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "1j9nfs6sl84ds622wfz3wm2n082gf6hrr6z7i1ymxw81nsprgfmp")))

(define-public crate-label-macros-0.1.7 (c (n "label-macros") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "080l07g8j4l94drzcfsw5ag7jb1qmh4wz2h59br70zza3zd0dhag")))

(define-public crate-label-macros-0.1.8 (c (n "label-macros") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "0bkp6qc7gk9g5jrwwfhajz6a2cn6lm9w9jnzmc00fdh846xa12h5")))

(define-public crate-label-macros-0.1.9 (c (n "label-macros") (v "0.1.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "037ax79szf6606mgjvz9z64j9frs5xv85arck00ba76dii19272a")))

(define-public crate-label-macros-0.2.0 (c (n "label-macros") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "0hvvldg79nlq3fi9wzdnrb4zk6wk7snms4p30vyj8svmll59bps1")))

(define-public crate-label-macros-0.2.1 (c (n "label-macros") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "1dfav9fkjsnnaagr34krcnpl4j22mqshrl8h3ini626cryqg1d8s")))

(define-public crate-label-macros-0.3.0 (c (n "label-macros") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "1k0qfffas5dc5rzhg0ciidx9b80q0sjkwlgn69xxs56cqdfzqnia")))

(define-public crate-label-macros-0.4.0 (c (n "label-macros") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1di71ighdkp7mha68kybbjnkgn8ml6ph49lgd32va0y2bsmr2abf")))

(define-public crate-label-macros-0.4.1 (c (n "label-macros") (v "0.4.1") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1lqn4svk5raax93lzpvgzid1aypvixb63vvrnxhkvk1c41m7h1dk")))

(define-public crate-label-macros-0.4.2 (c (n "label-macros") (v "0.4.2") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0g79kg5qyrkjqp5dasxwki6w89hnmwcsv3r2yy4d6jc5rfd6vqzb")))

(define-public crate-label-macros-0.4.3 (c (n "label-macros") (v "0.4.3") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1xh4w464div4h4qi1ipb2xaj74zafyhk31jqh9zydnc4j8pr5fmi")))

(define-public crate-label-macros-0.4.4 (c (n "label-macros") (v "0.4.4") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1cmrznay02p9c8gkqhcjiq91gn3jq64kwdbfbq82kq2sdx140pcm")))

(define-public crate-label-macros-0.5.0 (c (n "label-macros") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "1izyqbjbzwxv28fr8aag984zw84fzd8bd7i4wm5whxwl16245ks1")))

(define-public crate-label-macros-0.5.1 (c (n "label-macros") (v "0.5.1") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "12nay7ra32h8qbcxwhj572s6cli04lascz524ymvwqrysrp55r9p")))

(define-public crate-label-macros-0.6.0 (c (n "label-macros") (v "0.6.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full"))) (d #t) (k 0)))) (h "0zfks8fkn4r1pk02348hql3k57fd2aixhzs8k0wsqpzw4m7v4d8w")))

