(define-module (crates-io la be label-propagation) #:use-module (crates-io))

(define-public crate-label-propagation-0.1.0 (c (n "label-propagation") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.5.0") (d #t) (k 0)))) (h "1z4gl87wh3g7s48lxsrxdjjqgh102hxqqn8abmvab8q6aw33n0q2")))

(define-public crate-label-propagation-0.1.1 (c (n "label-propagation") (v "0.1.1") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.5.0") (d #t) (k 0)))) (h "0xlzh91m24ji26n4bfjr0db45zbmixxvcs2diwbqkzjc4lvdcv95")))

