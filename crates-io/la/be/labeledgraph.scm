(define-module (crates-io la be labeledgraph) #:use-module (crates-io))

(define-public crate-labeledgraph-0.1.0 (c (n "labeledgraph") (v "0.1.0") (d (list (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "12vglz97ayw4fvgqq30gh0m26a5zvrszbxqhfkz6bh3k4xq7c8f2") (y #t)))

(define-public crate-labeledgraph-0.1.1 (c (n "labeledgraph") (v "0.1.1") (d (list (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "09wdm5qzwd5xqcl2whmgqq7zrgifvvph9r9f83k9hm8pc6rld06b")))

(define-public crate-labeledgraph-0.1.2 (c (n "labeledgraph") (v "0.1.2") (d (list (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "081i5r5sxcq905pmvbv9hhlr7f50knjsbr3b4dcfq2ky01jkcc7y")))

