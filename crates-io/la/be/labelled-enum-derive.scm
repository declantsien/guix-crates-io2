(define-module (crates-io la be labelled-enum-derive) #:use-module (crates-io))

(define-public crate-labelled-enum-derive-0.1.0 (c (n "labelled-enum-derive") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0vliqswpld9v0nx9l9gfy23b97bfqp6lh4z73967g1hclz1x1y07")))

