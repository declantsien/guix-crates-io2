(define-module (crates-io la be label_attribute) #:use-module (crates-io))

(define-public crate-label_attribute-0.1.0 (c (n "label_attribute") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (t "cfg(not(feature = \"extra-traits\"))") (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (t "cfg(feature = \"extra-traits\")") (k 0)))) (h "18hxnljcxvw94mxqinh5f4nlzpvw6467xx7yzcwkikq5yg3i2y96") (f (quote (("extra-traits") ("default" "extra-traits"))))))

(define-public crate-label_attribute-0.1.1 (c (n "label_attribute") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (t "cfg(not(feature = \"extra-traits\"))") (k 0)) (d (n "syn") (r "^0.15") (f (quote ("extra-traits"))) (d #t) (t "cfg(feature = \"extra-traits\")") (k 0)))) (h "0b1m63ik3rx9q8qmrj89cc4zrq8fbhakvizwn4cbbipsvnir1xvv") (f (quote (("extra-traits") ("default" "extra-traits"))))))

