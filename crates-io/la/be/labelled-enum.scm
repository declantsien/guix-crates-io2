(define-module (crates-io la be labelled-enum) #:use-module (crates-io))

(define-public crate-labelled-enum-0.1.0 (c (n "labelled-enum") (v "0.1.0") (d (list (d (n "labelled-enum-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "05p3pflibaaxpky1xsydp823hf7d03pzxrj1fm382gaj77ys30af") (f (quote (("serde_plugin" "serde"))))))

(define-public crate-labelled-enum-0.1.1 (c (n "labelled-enum") (v "0.1.1") (d (list (d (n "labelled-enum-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 2)))) (h "19lq0538jaxg45rgls9fnnsz7yy9kjp801lv4w7h37qi7qpfgyqr") (f (quote (("serde_plugin" "serde"))))))

