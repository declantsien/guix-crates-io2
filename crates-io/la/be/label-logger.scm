(define-module (crates-io la be label-logger) #:use-module (crates-io))

(define-public crate-label-logger-0.1.0 (c (n "label-logger") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "00jjsd5nhwpggd39z9zahkxacl4bfkhpinrmvyfggl7lwrkilz48")))

(define-public crate-label-logger-0.1.1 (c (n "label-logger") (v "0.1.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "1hxlv2jn0n6kdn907xxr3vj2kckqmbmmwg7rd93v3r7ii0gkjvp7")))

(define-public crate-label-logger-1.0.0-rc.1 (c (n "label-logger") (v "1.0.0-rc.1") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "17w6k7dg5zdcmz8z3x9q4bv48rn0387r5g6wg6y9vwfs2zw9qaqk") (y #t)))

(define-public crate-label-logger-1.0.0-rc.2 (c (n "label-logger") (v "1.0.0-rc.2") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "1zg7j4iwqf3pdryf229p08mn49brhg27dfd9khjhv0c1mijfvlbq") (y #t)))

(define-public crate-label-logger-1.0.0-alpha.3 (c (n "label-logger") (v "1.0.0-alpha.3") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "0j8szdp5vcynrl04jxxixvb360yxafri2g2wm5pi8637351ja6nj") (y #t)))

(define-public crate-label-logger-1.0.0-alpha.4 (c (n "label-logger") (v "1.0.0-alpha.4") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "0zmryz1ckmm3lvqv4affklxzr3fjh6biqlivyhlymz8nmrsnila7") (y #t)))

(define-public crate-label-logger-1.0.0-alpha.5 (c (n "label-logger") (v "1.0.0-alpha.5") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "13rjfnr0mnnkanfz013rwyxzzahx067niw9kp4hv8kc1pbjcrg0v") (y #t)))

(define-public crate-label-logger-1.0.0-alpha.6 (c (n "label-logger") (v "1.0.0-alpha.6") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "1y6ri8l0lw3z76gjvi9cn6wa61nxcykd7i77ca3rs4q69d9xxi6j") (f (quote (("default")))) (y #t) (s 2) (e (quote (("dialoguer" "dep:dialoguer"))))))

(define-public crate-label-logger-1.0.0-alpha.7 (c (n "label-logger") (v "1.0.0-alpha.7") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "1g7fmqg6p0sm617xwazp5hpkb32qiajhj7yyyk19mf6kkq0sdgv8") (f (quote (("default")))) (y #t) (s 2) (e (quote (("dialoguer" "dep:dialoguer"))))))

(define-public crate-label-logger-1.0.0-alpha.8 (c (n "label-logger") (v "1.0.0-alpha.8") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "1xmnmzpskwk501155qaxqap221h8dcxh4yiwm88cnl80r2s8qrpj") (f (quote (("default")))) (y #t) (s 2) (e (quote (("dialoguer" "dep:dialoguer" "dialoguer/fuzzy-select" "dialoguer/password"))))))

(define-public crate-label-logger-1.0.0-alpha.9 (c (n "label-logger") (v "1.0.0-alpha.9") (d (list (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "1dr1g1rl8drl4qy57pd2akmxqdrlck1vqxyd0wl3m02sd1m5syk2") (f (quote (("default")))) (y #t) (s 2) (e (quote (("dialoguer" "dep:dialoguer" "dialoguer/fuzzy-select" "dialoguer/password"))))))

(define-public crate-label-logger-0.2.0 (c (n "label-logger") (v "0.2.0") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (o #t) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "1h8kxngj12xgdx9npfkwdirkv6pdj4yyqwbx9pwn9i87a3nbn1w0") (f (quote (("default")))) (s 2) (e (quote (("indicatif" "dep:indicatif") ("dialoguer" "dep:dialoguer"))))))

(define-public crate-label-logger-0.2.1 (c (n "label-logger") (v "0.2.1") (d (list (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (o #t) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "15mirvxrvas1066ldlza2afj6zq6gwxs2fb655v5m2ig1d1zdcds") (f (quote (("default")))) (s 2) (e (quote (("indicatif" "dep:indicatif") ("dialoguer" "dep:dialoguer"))))))

