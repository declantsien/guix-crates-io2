(define-module (crates-io la be labels) #:use-module (crates-io))

(define-public crate-labels-0.0.1 (c (n "labels") (v "0.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1r761dm7nk9d1amj752j0024rqvdqjlbrfx95pany5pklp6pz3xr")))

(define-public crate-labels-0.0.2 (c (n "labels") (v "0.0.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1m3zvpksz05ksqq45zzh41brapjlpb92d806cj5mi329c98i23gg")))

