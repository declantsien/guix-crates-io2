(define-module (crates-io la be labello) #:use-module (crates-io))

(define-public crate-labello-0.0.1 (c (n "labello") (v "0.0.1") (h "0y56ngswlg0f68brqrcn8gqi60qhazck6v8xpqxwihy25ahh2vhq")))

(define-public crate-labello-0.0.2 (c (n "labello") (v "0.0.2") (h "1shybd3ihvsvbj3gmq8pab3a6nl5h117f9vxijgfrhm5drgbkn1s")))

(define-public crate-labello-0.0.3 (c (n "labello") (v "0.0.3") (h "0xsqci8x5xyfa0iynfiac47865z0h9jgdasaz0y24fvqjh5wfs0q")))

(define-public crate-labello-0.0.5 (c (n "labello") (v "0.0.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)))) (h "15i7sp6a7l4dr2hbwj1lah490f0jpyfgfwl2sj94jhis2hn5kl8s")))

