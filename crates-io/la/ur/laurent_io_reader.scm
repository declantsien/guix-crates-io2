(define-module (crates-io la ur laurent_io_reader) #:use-module (crates-io))

(define-public crate-laurent_io_reader-0.1.0 (c (n "laurent_io_reader") (v "0.1.0") (h "16n82r0igvbi5v0ylsqx7m4misbg4296msy0axsjn2ffapfgg25a") (y #t)))

(define-public crate-laurent_io_reader-0.1.1 (c (n "laurent_io_reader") (v "0.1.1") (h "10xk4mvc5qq6145h2g954sr9nq8zqm6vy4j4h6i6hriih4f9fgnz") (y #t)))

(define-public crate-laurent_io_reader-0.1.2 (c (n "laurent_io_reader") (v "0.1.2") (h "1plmll2zrzplvcch15aff2jr9n9amhriy8f5n05gya0xvqnpzbia") (y #t)))

