(define-module (crates-io la y- lay-simulator-blueqat) #:use-module (crates-io))

(define-public crate-lay-simulator-blueqat-0.1.0 (c (n "lay-simulator-blueqat") (v "0.1.0") (d (list (d (n "cpython") (r "^0.5.0") (d #t) (k 0)) (d (n "lay") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("rt-core" "rt-threaded" "blocking" "macros"))) (d #t) (k 0)))) (h "13p8h6b5qy7jwfqli4ijvb1bn8nk1is7s1jl7r710nrjg85h54m8")))

