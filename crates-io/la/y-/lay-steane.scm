(define-module (crates-io la y- lay-steane) #:use-module (crates-io))

(define-public crate-lay-steane-0.1.0 (c (n "lay-steane") (v "0.1.0") (d (list (d (n "lay") (r "^0.1.0") (d #t) (k 0)) (d (n "lay-simulator-gk") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0vx1szhlh0pd891hnd0ws3nfsmbvmcnx66g0zizlq7f0rjzdi33n")))

(define-public crate-lay-steane-0.1.1 (c (n "lay-steane") (v "0.1.1") (d (list (d (n "lay") (r "^0.1.0") (d #t) (k 0)) (d (n "lay-simulator-blueqat") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "lay-simulator-gk") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1ifj57dh00mz7am1hd8bi3ndwfwbn8l1hv9my10vgm3ia0jk21hr") (f (quote (("test-blueqat" "lay-simulator-blueqat"))))))

(define-public crate-lay-steane-0.1.2 (c (n "lay-steane") (v "0.1.2") (d (list (d (n "lay") (r "^0.1.0") (d #t) (k 0)) (d (n "lay-simulator-blueqat") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "lay-simulator-gk") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1lz7q69l8zzwd2bqvf7j52c5p6j9n2rry9gfcq48fir0pik9zhm5") (f (quote (("test-blueqat" "lay-simulator-blueqat"))))))

