(define-module (crates-io la y- lay-simulator-gk) #:use-module (crates-io))

(define-public crate-lay-simulator-gk-0.1.0 (c (n "lay-simulator-gk") (v "0.1.0") (d (list (d (n "lay") (r "^0.1.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("rt-core" "rt-threaded" "blocking" "macros"))) (d #t) (k 2)))) (h "0slklicmb7ya4phs87pkx8n755vw409yzhpcnh9p7aaa2sibf9pj")))

