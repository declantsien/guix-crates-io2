(define-module (crates-io la ch lachesis) #:use-module (crates-io))

(define-public crate-lachesis-1.0.0 (c (n "lachesis") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fjfh252ihy9gakgxlj57rknvci3sghdcx4qcp2pbkky21bxzrfy")))

(define-public crate-lachesis-1.0.1 (c (n "lachesis") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fs8w61k7hq46nqikc9q9pj6rxlf4np3sja21w1jrzdyzhgcgvvy")))

(define-public crate-lachesis-1.0.2 (c (n "lachesis") (v "1.0.2") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bnfyh0cz9igfbdm1xxdpg6r69pn868nvqpjaxm5nxsl3md4c4rs")))

