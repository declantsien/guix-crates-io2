(define-module (crates-io la pj lapjv) #:use-module (crates-io))

(define-public crate-lapjv-0.1.0 (c (n "lapjv") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0qfadinjlpzcz2c1y1646z66bcdq5ib33ymcrim0b2m0jgdwsdx7") (f (quote (("nightly"))))))

(define-public crate-lapjv-0.1.1 (c (n "lapjv") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "15lbjp7ipcfmg60h2zwn9d70hy623gk4wmwlhh4kyaqzx506vi6i") (f (quote (("nightly"))))))

(define-public crate-lapjv-0.1.2 (c (n "lapjv") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.12") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 2)))) (h "1s5sbqmrxi7d872kx33jvbpm8v1c13d80wgr1j7kxxg8y3jnc5f5") (f (quote (("nightly"))))))

(define-public crate-lapjv-0.2.0 (c (n "lapjv") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)) (d (n "ndarray") (r "^0.13") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 2)))) (h "1fgg4gcgbpf63y0xmymkv1jjsrdwllfiljmizcqqh2fl9q06rxb0") (f (quote (("nightly"))))))

(define-public crate-lapjv-0.2.1 (c (n "lapjv") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.13") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 2)))) (h "10zcks6wkk4hjiz5k1mb9zx8xgyi53a8xrhlrnqgkcdsl073y2fp") (f (quote (("nightly"))))))

