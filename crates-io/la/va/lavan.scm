(define-module (crates-io la va lavan) #:use-module (crates-io))

(define-public crate-lavan-0.0.1-EXPERIMENTAL-SUBJECT-TO-BREAKING-CHANGES (c (n "lavan") (v "0.0.1-EXPERIMENTAL-SUBJECT-TO-BREAKING-CHANGES") (d (list (d (n "either") (r "^1.9.0") (o #t) (d #t) (k 0)))) (h "07gick2gykr9xjc1w82d8dx7rn4knqzhbw1vms2yrlx1mcmhgpr2") (f (quote (("default" "either")))) (s 2) (e (quote (("either" "dep:either"))))))

(define-public crate-lavan-0.0.2-EXPERIMENTAL-SUBJECT-TO-BREAKING-CHANGES (c (n "lavan") (v "0.0.2-EXPERIMENTAL-SUBJECT-TO-BREAKING-CHANGES") (d (list (d (n "either") (r "^1.9.0") (o #t) (d #t) (k 0)))) (h "183916q2lzkizk569a124l15hflkzld3zzx5ap4f10yqrnipjv22") (f (quote (("default" "either")))) (s 2) (e (quote (("either" "dep:either"))))))

(define-public crate-lavan-0.0.3-UNSTABLE (c (n "lavan") (v "0.0.3-UNSTABLE") (d (list (d (n "either") (r "^1.9.0") (o #t) (d #t) (k 0)))) (h "171vfgjn3padf556249z0yh3prwh9raa1b5m6wcn4x6l901fhpsw") (f (quote (("experimental") ("default" "either")))) (s 2) (e (quote (("either" "dep:either"))))))

