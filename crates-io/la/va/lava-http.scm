(define-module (crates-io la va lava-http) #:use-module (crates-io))

(define-public crate-lava-http-0.1.0 (c (n "lava-http") (v "0.1.0") (h "1p8flawh3fm964w9ffnw4xkffhfcy2g9xn9l3x9559jv9qs6qysm")))

(define-public crate-lava-http-0.1.1 (c (n "lava-http") (v "0.1.1") (h "14cd8wiz4pb4wxdrgiamq6pkin7q9sbik3in27ymq9dzrdn01v6p")))

