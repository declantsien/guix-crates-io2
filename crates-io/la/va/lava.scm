(define-module (crates-io la va lava) #:use-module (crates-io))

(define-public crate-lava-0.1.0 (c (n "lava") (v "0.1.0") (h "13vklg11abym1mylg2mn8n4lmy606dgzx55c113x1jm28qk7ryl7")))

(define-public crate-lava-0.2.0 (c (n "lava") (v "0.2.0") (h "0g6xz1j3fypdrw8wvw6wglxmiwhrllcfclix8q6y3csmfx7djcxf")))

(define-public crate-lava-0.3.0 (c (n "lava") (v "0.3.0") (h "0x803jh8zprmczdmlss3vpxpw4kj39vs0p3drv7vqcrv1cr59qfp")))

(define-public crate-lava-0.3.1 (c (n "lava") (v "0.3.1") (h "1fs45qrd40n7amapx6r78rg6nybq14zcs8xc4dl4xa6h94cmqs34")))

(define-public crate-lava-0.3.2 (c (n "lava") (v "0.3.2") (h "1nvdmcpsvkpsixvidk4m2q44vjcr1zqi50b7ic3r0bdj0mn6kk0d")))

(define-public crate-lava-0.4.0 (c (n "lava") (v "0.4.0") (h "00smyi1vlzxczg5cwlidj119csfp23hnn0y3k4hbvaqm8y96a0ky")))

(define-public crate-lava-0.4.1 (c (n "lava") (v "0.4.1") (h "08spbpsjvqlg4igdd3n98hbvch0qy6bmjs5yhcc7vjdwfaddg9dn")))

(define-public crate-lava-0.4.2 (c (n "lava") (v "0.4.2") (h "1arikvlkbra8h6wcr8p67csf6sajmdl22yyd98dwq1gzdbx9j0df")))

(define-public crate-lava-0.4.3 (c (n "lava") (v "0.4.3") (h "1lvrd1dyf926y82yh53gng1p8izg62y7z31q5mss68l57akpsphf")))

(define-public crate-lava-0.4.4 (c (n "lava") (v "0.4.4") (h "1cxvd80h0aal5sg98s26is3fyyzlb86r041l5a3g6nvkiwhrkah4")))

(define-public crate-lava-0.4.5 (c (n "lava") (v "0.4.5") (h "0nr1jhlcjg6py33hjzn9i763kjl67d4xigbnvpwpk29ymb1m5hjk")))

(define-public crate-lava-0.4.6 (c (n "lava") (v "0.4.6") (h "0ac7ng39q9x20yqzipq8c8iqkm3x3zcp046f9mrzk6ksarlyn3mx")))

(define-public crate-lava-0.4.7 (c (n "lava") (v "0.4.7") (h "1p8gg25xzfpv4h8wpp8g91qgnnvv3zhyizmxlp0sy374x22n9p5d")))

(define-public crate-lava-0.4.8 (c (n "lava") (v "0.4.8") (h "0gzkqpd9jzf66cacv6vabn1kasg5caprrgaix9yw24azf6x9vh3z")))

(define-public crate-lava-0.4.9 (c (n "lava") (v "0.4.9") (h "0xqgy1bhs177abd6y435ygddxqc94dmfm6nyc114xg77fxggj02b")))

