(define-module (crates-io la va lavagna_core) #:use-module (crates-io))

(define-public crate-lavagna_core-1.1.2 (c (n "lavagna_core") (v "1.1.2") (d (list (d (n "line_drawing") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q6wnzjrfrb6rfgasrpappp9jclpfx19dkbxqsjpk6drp2ax31bh")))

