(define-module (crates-io la va lavalink_rs_macros) #:use-module (crates-io))

(define-public crate-lavalink_rs_macros-0.1.0 (c (n "lavalink_rs_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "derive" "extra-traits"))) (d #t) (k 0)))) (h "0hcs7dpyrzmxll495krhrmgn84kkgxsds3x0lrpq19jg9lhg4vj2")))

