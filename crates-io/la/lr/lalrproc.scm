(define-module (crates-io la lr lalrproc) #:use-module (crates-io))

(define-public crate-lalrproc-0.0.1 (c (n "lalrproc") (v "0.0.1") (d (list (d (n "lalrpop") (r "^0.13") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.13") (d #t) (k 0)))) (h "16yxfkinq7ym7zchff8ffwm5k6spiqpsfb89mmphr0hx9dl4n5y0")))

(define-public crate-lalrproc-0.0.2 (c (n "lalrproc") (v "0.0.2") (d (list (d (n "lalrpop") (r "^0.15") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.15") (d #t) (k 0)))) (h "1k72ahjwkbmn7j4b4iln0ycbgww8vximbgwmiazmlg1g233rf2n1")))

(define-public crate-lalrproc-0.0.3 (c (n "lalrproc") (v "0.0.3") (d (list (d (n "lalrpop") (r "^0.15") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.15") (d #t) (k 0)))) (h "113wpv5vncnpj9qgmgyahr2s3x3hijcnyyddf5k06cdw5bb2v6v1")))

(define-public crate-lalrproc-0.0.4 (c (n "lalrproc") (v "0.0.4") (d (list (d (n "lalrpop") (r "^0.15") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.15") (d #t) (k 0)))) (h "12i7clcq0izfm44hbhhmcplm0a285zwqlyxmcxrpwsk5p1blrbw9")))

(define-public crate-lalrproc-0.0.5 (c (n "lalrproc") (v "0.0.5") (d (list (d (n "lalrpop") (r "^0.16") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.16") (d #t) (k 0)))) (h "0xcw6s4dv72yzf7k1hmw2vh3af6c0zi3gbq23x53wgy0y9kryd7m")))

(define-public crate-lalrproc-0.0.6 (c (n "lalrproc") (v "0.0.6") (d (list (d (n "lalrpop") (r "^0.19") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)))) (h "0fnqsy6gpvgl4ds70i2radz1wp2sdxnnf6si1k0igacnlvcnxr3m")))

(define-public crate-lalrproc-0.0.7 (c (n "lalrproc") (v "0.0.7") (d (list (d (n "lalrpop") (r "^0.19") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)))) (h "1gqpiyv5jifvpglas8y1qgflkasi933683axwpywh6nn9pfn269x")))

(define-public crate-lalrproc-0.0.8 (c (n "lalrproc") (v "0.0.8") (d (list (d (n "lalrpop") (r "^0.19") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19") (d #t) (k 0)))) (h "0p5nakj7k6s2cafzapsqmc4xxgwjfkf6czqhcbyn0c5sxgha60wl")))

