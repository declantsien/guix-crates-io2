(define-module (crates-io la lr lalr) #:use-module (crates-io))

(define-public crate-lalr-0.0.1 (c (n "lalr") (v "0.0.1") (h "1v6bcbqjaq3i3qj0qp9ighgblzd1hhqchfmznm4b64gvjglhbw63")))

(define-public crate-lalr-0.0.2 (c (n "lalr") (v "0.0.2") (h "1rwdjml6nxigdlrcxabshqsjbdypai2rbi2gkc0y7nssz547av8h")))

