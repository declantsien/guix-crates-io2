(define-module (crates-io la lr lalrpop-lambda) #:use-module (crates-io))

(define-public crate-lalrpop-lambda-0.1.0 (c (n "lalrpop-lambda") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.17.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.0") (d #t) (k 0)))) (h "176xs2qdr2i9j3bz5gx8zkk0h0y1hr0fmzy9p62khrn3iv222c4x")))

(define-public crate-lalrpop-lambda-0.2.0 (c (n "lalrpop-lambda") (v "0.2.0") (d (list (d (n "lalrpop") (r "^0.17.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.0") (d #t) (k 0)))) (h "03c4x8yy11m9f8v0ar2kl4m24inrw437hh8zd3lkhwk8j65ghy86")))

(define-public crate-lalrpop-lambda-0.3.0 (c (n "lalrpop-lambda") (v "0.3.0") (d (list (d (n "lalrpop") (r "^0.17.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.0") (d #t) (k 0)))) (h "10qxhl73q1fs7qd6k1px995m8s5kzgakch2ahzyq5nb4hh3g0bpx")))

(define-public crate-lalrpop-lambda-0.4.0 (c (n "lalrpop-lambda") (v "0.4.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lalrpop") (r "^0.17.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.0") (d #t) (k 0)))) (h "07xxkq8j6rpk8j7ciflyk9s8nxqgy77hwajpjr855i801s7anqk2")))

(define-public crate-lalrpop-lambda-0.4.1 (c (n "lalrpop-lambda") (v "0.4.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lalrpop") (r "^0.17.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.0") (d #t) (k 0)))) (h "1ajv9whq42ywb8gz6n444jd9adi35bywabq5xnkzk070l6xsxvb9")))

(define-public crate-lalrpop-lambda-0.5.0 (c (n "lalrpop-lambda") (v "0.5.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lalrpop") (r "^0.17.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "regex") (r "^0.2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0r6cwq09kd0iaiwzj38qq4pb5xrm8siw3qzm079y93c3shqkmz5z") (f (quote (("wasm" "wasm-bindgen") ("default" "wasm"))))))

(define-public crate-lalrpop-lambda-0.6.1 (c (n "lalrpop-lambda") (v "0.6.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "lalrpop") (r "^0.17.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.17.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "regex") (r "^0.2.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0yihp1x3xjg4wqi4zcyng4pjcla5mkbssq0g4nahqhkkz8d4faaz") (f (quote (("wasm" "wasm-bindgen") ("default" "wasm"))))))

