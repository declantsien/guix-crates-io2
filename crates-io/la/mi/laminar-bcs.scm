(define-module (crates-io la mi laminar-bcs) #:use-module (crates-io))

(define-public crate-laminar-bcs-0.1.3 (c (n "laminar-bcs") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1zrcpnlqsffr732780krcvmvfgymnw2v7vxxfajz8z1lwphcf30g")))

