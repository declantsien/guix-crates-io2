(define-module (crates-io la mi laminar-aptos-crypto-derive) #:use-module (crates-io))

(define-public crate-laminar-aptos-crypto-derive-0.0.3 (c (n "laminar-aptos-crypto-derive") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.38") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ag2v340b0v2zn2wwl88m4b50r801gmr80mrkziyxf0h4l0y0bqy")))

