(define-module (crates-io la mi laminar-ffi) #:use-module (crates-io))

(define-public crate-laminar-ffi-0.0.1 (c (n "laminar-ffi") (v "0.0.1") (d (list (d (n "laminar") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (k 0)))) (h "17pkrk41vsnyn6h7f87f6jkvydj5v379nq15q9jz2p197zpc908p")))

