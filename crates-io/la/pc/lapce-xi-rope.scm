(define-module (crates-io la pc lapce-xi-rope) #:use-module (crates-io))

(define-public crate-lapce-xi-rope-0.3.1 (c (n "lapce-xi-rope") (v "0.3.1") (d (list (d (n "bytecount") (r "^0.6") (d #t) (k 0)) (d (n "memchr") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "02m1bh91lbxigk2k2yk4imy9gqza4f3ci1wsvlgg14fgp3nj7bh8") (f (quote (("default"))))))

(define-public crate-lapce-xi-rope-0.3.2 (c (n "lapce-xi-rope") (v "0.3.2") (d (list (d (n "bytecount") (r "^0.6") (d #t) (k 0)) (d (n "memchr") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "02pzv7wmk0pddf504c233ff6j975sn1dfbn03cddqnahkjlsl5k5") (f (quote (("default"))))))

