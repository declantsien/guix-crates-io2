(define-module (crates-io la pc lapce-plugin) #:use-module (crates-io))

(define-public crate-lapce-plugin-0.1.0 (c (n "lapce-plugin") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "19s0vxa4caz1dvbpsly20zbbz6nd4dmiypk01567n7jhzn8fnlps")))

(define-public crate-lapce-plugin-0.1.1 (c (n "lapce-plugin") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "jsonrpc-lite") (r "^0.5.0") (d #t) (k 0)) (d (n "lapce-wasi-experimental-http") (r "^0.10.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "psp-types") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "0p2mrfdm4vn4n8r5jh4djdn503yrjyb45c6x1bs0zhf2jfv9q5hw") (y #t)))

(define-public crate-lapce-plugin-0.1.2 (c (n "lapce-plugin") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "jsonrpc-lite") (r "^0.5.0") (d #t) (k 0)) (d (n "lapce-wasi-experimental-http") (r "^0.10.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "psp-types") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "1i077kq4hj6xrf79hk13r502mgmq8wq8qcgn4hsims0nd54dyd3v")))

