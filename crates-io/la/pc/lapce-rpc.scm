(define-module (crates-io la pc lapce-rpc) #:use-module (crates-io))

(define-public crate-lapce-rpc-0.2.1 (c (n "lapce-rpc") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.34") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "lapce-xi-rope") (r "^0.3.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "lsp-types") (r "^0.93") (f (quote ("proposed"))) (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "18xrpgnsd540aj2dihl9nga0mbzcgcxa76jw0bw451lidvni5crw")))

