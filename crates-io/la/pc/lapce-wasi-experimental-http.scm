(define-module (crates-io la pc lapce-wasi-experimental-http) #:use-module (crates-io))

(define-public crate-lapce-wasi-experimental-http-0.10.0 (c (n "lapce-wasi-experimental-http") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)))) (h "03l1rrcyjbq0rmmwsg8bkvqk9kvf9v5y1nqw93i3dd9sq6561xya")))

