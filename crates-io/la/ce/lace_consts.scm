(define-module (crates-io la ce lace_consts) #:use-module (crates-io))

(define-public crate-lace_consts-0.1.0 (c (n "lace_consts") (v "0.1.0") (d (list (d (n "rv") (r "^0.15.0") (f (quote ("serde1" "arraydist"))) (d #t) (k 0)))) (h "0dfrlbf34m1id5lphq0sar11w2x2pn7700z61315csw7lbgqn735")))

(define-public crate-lace_consts-0.1.1 (c (n "lace_consts") (v "0.1.1") (d (list (d (n "rv") (r "^0.15.0") (f (quote ("serde1" "arraydist"))) (d #t) (k 0)))) (h "1q7b4vy03zihr3aagc74s6lr4p4z63ngrk5gnjkw8rhazgfx893j")))

(define-public crate-lace_consts-0.1.2 (c (n "lace_consts") (v "0.1.2") (d (list (d (n "rv") (r "^0.15.0") (f (quote ("serde1" "arraydist"))) (d #t) (k 0)))) (h "0w0rfbr7kppm8gr56gfd6kb4347fwxia3qiv0qgnxs6wl4h3jzkl")))

(define-public crate-lace_consts-0.1.4 (c (n "lace_consts") (v "0.1.4") (d (list (d (n "rv") (r "^0.16.0") (f (quote ("serde1" "arraydist"))) (d #t) (k 0)))) (h "16xvn0nj6r0nl7h5kc05f669fl059310xnz3rdn04blkzs7b4gxl")))

(define-public crate-lace_consts-0.2.1 (c (n "lace_consts") (v "0.2.1") (d (list (d (n "rv") (r "^0.16.2") (f (quote ("serde1" "arraydist"))) (d #t) (k 0)))) (h "1acyrjyln8pfzwnrzil5ws8h8pwvq744m2g3xsxmqc8rxs4iyagb")))

