(define-module (crates-io la ce lace_geweke) #:use-module (crates-io))

(define-public crate-lace_geweke-0.1.0 (c (n "lace_geweke") (v "0.1.0") (d (list (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "lace_stats") (r "^0.1.0") (d #t) (k 0)) (d (n "lace_utils") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.4") (d #t) (k 0)))) (h "017d07czws79r6n44s0mlxjg4cvr3b0418q4v87nm5zddg8049j2")))

(define-public crate-lace_geweke-0.1.1 (c (n "lace_geweke") (v "0.1.1") (d (list (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "lace_stats") (r "^0.1.1") (d #t) (k 0)) (d (n "lace_utils") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.4") (d #t) (k 0)))) (h "116gnkhdz32ych8fyx054ny5d8xdmdvxjzany7s7fbl0xsg7yikl")))

(define-public crate-lace_geweke-0.1.2 (c (n "lace_geweke") (v "0.1.2") (d (list (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "lace_stats") (r "^0.1.2") (d #t) (k 0)) (d (n "lace_utils") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.4") (d #t) (k 0)))) (h "10vn00878352vb1gfvlkbvg8q5686may9shgkql46pl29cxdb5br")))

(define-public crate-lace_geweke-0.1.3 (c (n "lace_geweke") (v "0.1.3") (d (list (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "lace_stats") (r "^0.1.5") (d #t) (k 0)) (d (n "lace_utils") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.4") (d #t) (k 0)))) (h "1dhi65xin317c1vjf1hz0968w27a79llz1wzcdlwnilhd3pw73a6")))

(define-public crate-lace_geweke-0.2.1 (c (n "lace_geweke") (v "0.2.1") (d (list (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "lace_stats") (r "^0.2.1") (d #t) (k 0)) (d (n "lace_utils") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.4") (d #t) (k 0)))) (h "0b71rjrbxi8dsjq4ib03g28d8c75kbz3z5khfi5rq9hjpn29xnyq")))

(define-public crate-lace_geweke-0.3.0 (c (n "lace_geweke") (v "0.3.0") (d (list (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "lace_stats") (r "^0.3.0") (d #t) (k 0)) (d (n "lace_utils") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.4") (d #t) (k 0)))) (h "1fcd61gpai4c9wnbd2mkz1p27fpiw0sdqxyw28k9rmn1ik5cywlp")))

