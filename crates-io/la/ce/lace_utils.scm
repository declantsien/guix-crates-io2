(define-module (crates-io la ce lace_utils) #:use-module (crates-io))

(define-public crate-lace_utils-0.1.0 (c (n "lace_utils") (v "0.1.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("serde1"))) (d #t) (k 0)))) (h "0dmgyxp1bhzm875h2mm6vfgcahddn6xgkbwkkgy52h86g5zdj8rc")))

(define-public crate-lace_utils-0.1.1 (c (n "lace_utils") (v "0.1.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("serde1"))) (d #t) (k 0)))) (h "1l90iddhrc5hb9z92jjkd4ip1jq05gri6kvfh4z2mffkrm3r7sjy")))

(define-public crate-lace_utils-0.1.2 (c (n "lace_utils") (v "0.1.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("serde1"))) (d #t) (k 0)))) (h "15mimwsfjalyq4zwdckn65hwbg4dkx6p1mqw11ffw1nmky0x0448")))

(define-public crate-lace_utils-0.2.0 (c (n "lace_utils") (v "0.2.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("serde1"))) (d #t) (k 0)))) (h "0d0skj36bsmiv5n3jmkjdwhg0h1rd8a6znynm9ch4svlk76ankgk")))

(define-public crate-lace_utils-0.3.0 (c (n "lace_utils") (v "0.3.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "1rcm4s7qr7c539s61dmpzk3gds9j5lsy3l3lamccn2p7r0jyshlz")))

