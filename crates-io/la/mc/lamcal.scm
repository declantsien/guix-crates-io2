(define-module (crates-io la mc lamcal) #:use-module (crates-io))

(define-public crate-lamcal-0.1.0 (c (n "lamcal") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "1y9mqv8c3wqzrfda46s0fl4zq5jln1had309w33212yw0gilylwv")))

(define-public crate-lamcal-0.1.1 (c (n "lamcal") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "0ak7pgila2lysw88z2xs1a4k4nrs8bh3f9dhp4qb7fhjp6m8mk34")))

(define-public crate-lamcal-0.2.0 (c (n "lamcal") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "09209ymkzsqgw6ga5cjk74nx7hr2cqb7qnglk6nrg9qrpa8gndsi")))

(define-public crate-lamcal-0.3.0 (c (n "lamcal") (v "0.3.0") (d (list (d (n "failure") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "04p92hgc3xp1x0w66hyijf2bkpml39zzc7kwr9ks8p1w21zp85jg")))

(define-public crate-lamcal-0.4.0 (c (n "lamcal") (v "0.4.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "failure") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "proptest") (r "^0.8") (d #t) (k 2)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "1ji5jsmqs8h0vnw5852n1w99d583i44myv3l649wmxj1va0cl8n6")))

