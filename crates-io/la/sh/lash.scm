(define-module (crates-io la sh lash) #:use-module (crates-io))

(define-public crate-lash-0.3.0 (c (n "lash") (v "0.3.0") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "1bc3xz3v11lvav6yn0grgp2vp0x569jlagqwbqjl862fif4iqiip")))

(define-public crate-lash-0.3.1 (c (n "lash") (v "0.3.1") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "06765i1px4r58c6sid3kf2f8djc3s39xazcn64y654vkx67dym7l")))

