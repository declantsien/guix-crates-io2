(define-module (crates-io la bo laboratory) #:use-module (crates-io))

(define-public crate-laboratory-0.1.0 (c (n "laboratory") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "console") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "1ij94biakrj65lbmakrryw4jdx381zcvs1pq5bfh0whmcmvpj9gq")))

(define-public crate-laboratory-1.0.0 (c (n "laboratory") (v "1.0.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "console") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "0w1yzxn8jyikgpxp604m5c6qnn5a1imrl8ys17hsq1nafqyalm63")))

(define-public crate-laboratory-1.1.0 (c (n "laboratory") (v "1.1.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "console") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "1z532cqjpw1xlxlxg9icmrlps361f3nk5gxpbgxpckklirsyx95y")))

(define-public crate-laboratory-1.2.0 (c (n "laboratory") (v "1.2.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "console") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "0k537xxvd2fzhd5s6jkmzm1lbqd89nbc5agj53y4l3kyff8v2jg7")))

(define-public crate-laboratory-1.3.0 (c (n "laboratory") (v "1.3.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "console") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "1r1jrqfsmx875q1a7my25gs2f5i9xk6zb75msidj9ijrh7as4ysf")))

(define-public crate-laboratory-2.0.0 (c (n "laboratory") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "console") (r "^0.12.0") (d #t) (k 0)) (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "0cl0gg8sbsq4dzh2l3psys5cbgd8wksy823w69m4l8hk5gfw2z83")))

