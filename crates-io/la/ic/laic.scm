(define-module (crates-io la ic laic) #:use-module (crates-io))

(define-public crate-laic-0.1.0 (c (n "laic") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.25") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cmd_lib") (r "^1.3.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "fern") (r "^0.6.2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "uuid") (r "^1.3.3") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "02w2wpd55w4bjb7c2sn4c94r71pbhjj3vm067a2xvlalyl00h8lw")))

