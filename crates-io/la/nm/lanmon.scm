(define-module (crates-io la nm lanmon) #:use-module (crates-io))

(define-public crate-lanmon-0.1.0 (c (n "lanmon") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "config") (r "^0.7") (d #t) (k 0)) (d (n "fastping-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "fern") (r "^0.5") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.34") (d #t) (k 0)))) (h "08lv6h65a6i5k1cas3m96fvbyy0fdlq5fzi54hbfmrrd7y435x1f")))

(define-public crate-lanmon-0.2.0 (c (n "lanmon") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "fastping-rs") (r "^0.1.1") (d #t) (k 0)) (d (n "fern") (r "^0.5") (f (quote ("colored"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.34") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1vnk2kq9nsicdklpahx74n6yzp2dkldw3kbilddzha9m378p9y8h")))

