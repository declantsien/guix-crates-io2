(define-module (crates-io la pa lapacke-static) #:use-module (crates-io))

(define-public crate-lapacke-static-0.1.0 (c (n "lapacke-static") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.2") (d #t) (k 1)) (d (n "tempdir") (r "^0.3") (d #t) (k 1)))) (h "0liwiknnrpys8hhcgrnj8jv9rx4yvr3qd92zlx0n4fi5mfq3f15z")))

