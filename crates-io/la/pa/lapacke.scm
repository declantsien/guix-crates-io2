(define-module (crates-io la pa lapacke) #:use-module (crates-io))

(define-public crate-lapacke-0.0.1 (c (n "lapacke") (v "0.0.1") (h "1g6z43wgl07axf5b1xj20aqm10pxb7wn6zza9nf6shqgd76k6ksk")))

(define-public crate-lapacke-0.1.0 (c (n "lapacke") (v "0.1.0") (d (list (d (n "lapacke-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "1n9sacywbkwpd4389gvqsc8j4rz4mr58mr4zxr7vmk41419fhgfc")))

(define-public crate-lapacke-0.1.1 (c (n "lapacke") (v "0.1.1") (d (list (d (n "lapacke-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "07hvlcqff581vlzz9kxcr16cawrl1jhpvf3bf29cd90ypfg9qjph")))

(define-public crate-lapacke-0.1.2 (c (n "lapacke") (v "0.1.2") (d (list (d (n "lapacke-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "1p5w5nvrb18nm5k0yrqkmkaqi8wdgvi0b1qkyc8vj2w18zykhmmg")))

(define-public crate-lapacke-0.1.3 (c (n "lapacke") (v "0.1.3") (d (list (d (n "lapacke-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "0q7fcdjw90qpvvc66yakvzv6mywwrx5lckkxa4qa0vhs961561qd")))

(define-public crate-lapacke-0.1.4 (c (n "lapacke") (v "0.1.4") (d (list (d (n "lapacke-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "0xilhfbjhrqc3mlhyhq807h4i1jgijrrmlv1nd4vva9ij9r4xmm4")))

(define-public crate-lapacke-0.1.5 (c (n "lapacke") (v "0.1.5") (d (list (d (n "lapacke-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (k 0)))) (h "0wm2dpng5f8y2c6lir99p6aws21j26mr06clh89shsr0gk8wxkwv") (y #t)))

(define-public crate-lapacke-0.2.0 (c (n "lapacke") (v "0.2.0") (d (list (d (n "lapacke-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (k 0)))) (h "0xw6nndv1iyj2n22xdi8la63dp92vz23vgmwfqiyffzychs9vkjb")))

(define-public crate-lapacke-0.3.0 (c (n "lapacke") (v "0.3.0") (d (list (d (n "lapacke-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.3") (k 0)))) (h "1y35dpyap66vgami3dz7wqgiv5v62j0q9aflck1zdp7g4czn41m1")))

(define-public crate-lapacke-0.4.0 (c (n "lapacke") (v "0.4.0") (d (list (d (n "lapacke-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (k 0)))) (h "1phyg605hnw8vbkvdspi7xs569wa2c7xx1q9rcfqq9bfdvziix72")))

(define-public crate-lapacke-0.5.0 (c (n "lapacke") (v "0.5.0") (d (list (d (n "lapacke-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (k 0)))) (h "0i47chc0dgj3wjs96i6pn86zvsyxs68s51w4gy81x7y6rzisw9c4")))

