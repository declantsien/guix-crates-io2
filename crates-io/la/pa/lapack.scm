(define-module (crates-io la pa lapack) #:use-module (crates-io))

(define-public crate-lapack-0.0.1 (c (n "lapack") (v "0.0.1") (d (list (d (n "assert") (r "^0.0.1") (d #t) (k 0)))) (h "1vl9hrlp4d2b7yj2flf71y2bfpjrc4fgilfqiqckvfibx93jpz28")))

(define-public crate-lapack-0.0.2 (c (n "lapack") (v "0.0.2") (d (list (d (n "assert") (r "^0.0.1") (d #t) (k 0)) (d (n "liblapack-sys") (r "^0.0.2") (d #t) (k 0)))) (h "1582k0yilbkhwwgq5mhjnzjzkvxaknrm5nfr9cms51rdq72wbywy")))

(define-public crate-lapack-0.0.3 (c (n "lapack") (v "0.0.3") (d (list (d (n "assert") (r "^0.0.2") (d #t) (k 2)) (d (n "liblapack-sys") (r "^0.0.3") (d #t) (k 0)))) (h "0hm9nr1h57zr0v6ifp29030i8ykmja52s3c2f1cydhknsx94d98h")))

(define-public crate-lapack-0.0.4 (c (n "lapack") (v "0.0.4") (d (list (d (n "assert") (r "^0.0.2") (d #t) (k 2)) (d (n "liblapack-sys") (r "^0.0.4") (d #t) (k 0)))) (h "07y402hxsxlwavjw220jwwndd9jvsk5vwsqv29m316slr5zas4r2")))

(define-public crate-lapack-0.0.5 (c (n "lapack") (v "0.0.5") (d (list (d (n "assert") (r "^0.0.2") (d #t) (k 2)) (d (n "liblapack-sys") (r "^0.0.4") (d #t) (k 0)))) (h "09185c2d54zzgxdgjckgfp28jlj9byp5bg09m2h9i1cs4x0adhj1")))

(define-public crate-lapack-0.0.6 (c (n "lapack") (v "0.0.6") (d (list (d (n "assert") (r "^0.0.3") (d #t) (k 2)) (d (n "liblapack-sys") (r "^0.0.5") (d #t) (k 0)))) (h "02l7s93lj8kv87mc6ha2s0c8gcq88ryj871gmf6d1xl1lricqjj1")))

(define-public crate-lapack-0.0.7 (c (n "lapack") (v "0.0.7") (d (list (d (n "assert") (r "^0.0.4") (d #t) (k 2)) (d (n "liblapack-sys") (r "^0.0.6") (d #t) (k 0)))) (h "0cdgrxdp4pffg9v9a6g3gzjv8bb0b9mwkj5rs7m1hbfr0ni91vbr")))

(define-public crate-lapack-0.0.8 (c (n "lapack") (v "0.0.8") (d (list (d (n "assert") (r "^0.0.4") (d #t) (k 2)) (d (n "liblapack-sys") (r "^0.0.6") (d #t) (k 0)))) (h "1f67hmli33b45lldcdkamdpq8xdilfacgs02wmbzi9xlp5c4h01y")))

(define-public crate-lapack-0.0.9 (c (n "lapack") (v "0.0.9") (d (list (d (n "assert") (r "^0.0.4") (d #t) (k 2)) (d (n "liblapack-sys") (r "^0.0.7") (d #t) (k 0)))) (h "1xmcb2z5jkw8131ynm8aljpf00a6gdq68cfz54x1npk9jqkx7myh")))

(define-public crate-lapack-0.0.10 (c (n "lapack") (v "0.0.10") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "liblapack-sys") (r "^0.0.7") (d #t) (k 0)))) (h "0vyc2701wxngny4v6cim6m7791ll1kk24w7jwp3mjc70b670bccv")))

(define-public crate-lapack-0.0.11 (c (n "lapack") (v "0.0.11") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "liblapack-sys") (r "^0.0.8") (d #t) (k 0)))) (h "0573pjrxf1c15vhj4lkabwdf0smpzry3fzn4sfnyzdz69mdjldlb")))

(define-public crate-lapack-0.0.13 (c (n "lapack") (v "0.0.13") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "liblapack-sys") (r "^0.0.13") (d #t) (k 0)))) (h "1xf28z3lyr9ziw5akx9f38fyjvhf9ca7khd7hsgjhqsvsvi4v5zl")))

(define-public crate-lapack-0.0.14 (c (n "lapack") (v "0.0.14") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "liblapack-sys") (r "^0.0.14") (d #t) (k 0)))) (h "0iqad2siccz5lqp3m0v5zl9mfjmxr9hmh25x1hiq3825r9g42w6s")))

(define-public crate-lapack-0.0.16 (c (n "lapack") (v "0.0.16") (d (list (d (n "assert") (r "^0.1") (d #t) (k 2)) (d (n "liblapack-sys") (r "*") (d #t) (k 0)))) (h "17f7amq3cnkjkhrjbyvffxh9dj8r1mgafgbh5fmzvapdhpvkgljj")))

(define-public crate-lapack-0.0.17 (c (n "lapack") (v "0.0.17") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "liblapack-sys") (r "*") (d #t) (k 0)))) (h "00qnjh84ghmh7vn19yvn9ww2hh4li5hzhcxx96qjxkz316s406cm")))

(define-public crate-lapack-0.0.18 (c (n "lapack") (v "0.0.18") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "liblapack-sys") (r "^0.0.16") (d #t) (k 0)))) (h "1hc00wl5xbh7gp94n1wavka266cxjg4xirzx6f95678av71y95w3")))

(define-public crate-lapack-0.0.19 (c (n "lapack") (v "0.0.19") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "liblapack-sys") (r "^0.0.16") (d #t) (k 0)))) (h "0n0aqzljfvnby3zwykpjqpn926w0xw4079198kjpsqpi5bf1sb50")))

(define-public crate-lapack-0.1.0 (c (n "lapack") (v "0.1.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "liblapack-sys") (r "^0.0.16") (d #t) (k 0)))) (h "1v2s0lz8vz32qfskfs68c0idqyi55mddgm7lngqkdywh7bsfyh2k")))

(define-public crate-lapack-0.1.1 (c (n "lapack") (v "0.1.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "liblapack-sys") (r "*") (d #t) (k 0)))) (h "0nli3chv63znazjnw3k3vvx84aljml8pbp12jaiadzj0b1ml4llv")))

(define-public crate-lapack-0.2.0 (c (n "lapack") (v "0.2.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "liblapack-sys") (r "*") (d #t) (k 0)))) (h "14qbnjq5nja3rhfiygfj1jrmazqbw0wqbnik58q8fdfpagcjhmfh")))

(define-public crate-lapack-0.2.1 (c (n "lapack") (v "0.2.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "liblapack-sys") (r "*") (d #t) (k 0)))) (h "1f03mrcfgvjpxw69ddh7qlx8i02md5nx4rg2b8l55vj0nq7gdyzd")))

(define-public crate-lapack-0.3.0 (c (n "lapack") (v "0.3.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "liblapack-sys") (r "*") (d #t) (k 0)))) (h "11rfarjrf1x2lkzb8pbmbzr2yrcz96xdpbfqgxfy4a7qvhh3yn9l")))

(define-public crate-lapack-0.3.1 (c (n "lapack") (v "0.3.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "liblapack-sys") (r "*") (d #t) (k 0)))) (h "0palhp72zkv038krlddp4ympvayna00ljp5p3wjp1i1ai1aqp9w4")))

(define-public crate-lapack-0.3.2 (c (n "lapack") (v "0.3.2") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "lapack-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "07p56iwpyddwnhwdnymxvpnizgi6dzlkbk4fjwb659lyjghmscmy")))

(define-public crate-lapack-0.3.3 (c (n "lapack") (v "0.3.3") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "lapack-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "061br97ybsqd7zn3rxzryq8pw4hp9rpfyhrs98qvj34d7lbkb95r")))

(define-public crate-lapack-0.3.4 (c (n "lapack") (v "0.3.4") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "lapack-sys") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0z0z31pa3r1w51r8hv9sh4rpkgblc72apwfbnhk0983w2a73l2r3")))

(define-public crate-lapack-0.4.0 (c (n "lapack") (v "0.4.0") (d (list (d (n "complex") (r "^0.7") (d #t) (k 0)) (d (n "lapack-sys") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "05id74r4f8z857ibn1ligx0jm3j7v5zsq7dj314jzrx11g8w3p42")))

(define-public crate-lapack-0.4.1 (c (n "lapack") (v "0.4.1") (d (list (d (n "complex") (r "^0.7") (d #t) (k 0)) (d (n "lapack-sys") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0ra2l04zavz5mm2gg8b0w0b15aqhfkdaxb3i8y9c3znj6ipd9fcz")))

(define-public crate-lapack-0.5.0 (c (n "lapack") (v "0.5.0") (d (list (d (n "complex") (r "^0.7") (d #t) (k 0)) (d (n "lapack-sys") (r "^0.3") (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0pqfiz0f42nzpnj48fgxv5px8hd7mja3alz6hrdh4dbq7s9zrvdr") (f (quote (("openblas" "lapack-sys/openblas") ("netlib" "lapack-sys/netlib") ("default" "openblas"))))))

(define-public crate-lapack-0.6.0 (c (n "lapack") (v "0.6.0") (d (list (d (n "complex") (r "^0.7") (d #t) (k 0)) (d (n "lapack-sys") (r "^0.3") (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "00m0ind823k48wzkna16cbanqxy8db87fxkz50apgx4kmq6aq86l") (f (quote (("openblas" "lapack-sys/openblas") ("netlib" "lapack-sys/netlib") ("default" "openblas"))))))

(define-public crate-lapack-0.7.0 (c (n "lapack") (v "0.7.0") (d (list (d (n "complex") (r "^0.7") (d #t) (k 0)) (d (n "lapack-sys") (r "^0.4") (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "13pqx5vh3a7pjlh8jpc1gfm3gmkwg24zg3h3359l149v446chq7z") (f (quote (("openblas" "lapack-sys/openblas") ("netlib" "lapack-sys/netlib") ("default" "openblas") ("accelerate" "lapack-sys/accelerate"))))))

(define-public crate-lapack-0.8.0 (c (n "lapack") (v "0.8.0") (d (list (d (n "lapack-sys") (r "^0.4") (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)))) (h "07nhpnngzkqn2y3yyiy9xk4ms2mpskffgb0ya0ih45s70zp6csdm") (f (quote (("openblas" "lapack-sys/openblas") ("netlib" "lapack-sys/netlib") ("default" "openblas") ("accelerate" "lapack-sys/accelerate"))))))

(define-public crate-lapack-0.8.1 (c (n "lapack") (v "0.8.1") (d (list (d (n "lapack-sys") (r "^0.4") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)))) (h "1121f8nhwcjg4k81s9av4yqvyl0smppvbjbzq98wliy75l838m54") (f (quote (("openblas" "lapack-sys/openblas") ("netlib" "lapack-sys/netlib") ("default" "openblas") ("accelerate" "lapack-sys/accelerate"))))))

(define-public crate-lapack-0.9.0 (c (n "lapack") (v "0.9.0") (d (list (d (n "lapack-sys") (r "^0.10") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)))) (h "1zwzqbci2plwlw8zjplzf9sicf3aljpbvwc53shva3a4jrcndv2q") (f (quote (("openblas" "lapack-sys/openblas") ("netlib" "lapack-sys/netlib") ("default" "openblas") ("accelerate" "lapack-sys/accelerate"))))))

(define-public crate-lapack-0.10.0 (c (n "lapack") (v "0.10.0") (d (list (d (n "lapack-sys") (r "^0.11") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)))) (h "1mshd1v8jpnbbwg20519d3h6lbf2rmlv0v3icnqnf6mw9bi6vzvb") (f (quote (("openblas" "lapack-sys/openblas") ("netlib" "lapack-sys/netlib") ("default" "openblas") ("accelerate" "lapack-sys/accelerate"))))))

(define-public crate-lapack-0.10.1 (c (n "lapack") (v "0.10.1") (d (list (d (n "lapack-sys") (r "^0.11") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "1kcaghki5ysxmn9gxrdlah7cx12j2c4svwnhv7rvyp3byjs90c32") (f (quote (("openblas" "lapack-sys/openblas") ("netlib" "lapack-sys/netlib") ("default" "openblas") ("accelerate" "lapack-sys/accelerate"))))))

(define-public crate-lapack-0.11.0 (c (n "lapack") (v "0.11.0") (d (list (d (n "lapack-sys") (r "^0.11") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "1w78w12mrlpr45wc4hicn2zxwiq3mvnfq243i6hzv5yp2rlwyw6z") (f (quote (("openblas" "lapack-sys/openblas") ("netlib" "lapack-sys/netlib") ("default" "openblas") ("accelerate" "lapack-sys/accelerate"))))))

(define-public crate-lapack-0.11.1 (c (n "lapack") (v "0.11.1") (d (list (d (n "lapack-sys") (r "^0.11") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "0b4vxzrj79gybyjp70l6ydq59aj0l30fymdza2b6h21896c89pzg") (f (quote (("openblas" "lapack-sys/openblas") ("netlib" "lapack-sys/netlib") ("default" "openblas") ("accelerate" "lapack-sys/accelerate"))))))

(define-public crate-lapack-0.11.2 (c (n "lapack") (v "0.11.2") (d (list (d (n "lapack-sys") (r "^0.11") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "0mgr8a0jgxb89zs85b9jw9lr8iwclg9b3imh5mssd1y4yqr0cbx1") (f (quote (("openblas" "lapack-sys/openblas") ("netlib" "lapack-sys/netlib") ("default" "openblas") ("accelerate" "lapack-sys/accelerate"))))))

(define-public crate-lapack-0.11.3 (c (n "lapack") (v "0.11.3") (d (list (d (n "lapack-sys") (r "^0.11") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "16w7ql6aqa4crsidn50ky930y2fiy91ynlmibicrsw9lvdphp91s") (f (quote (("openblas" "lapack-sys/openblas") ("netlib" "lapack-sys/netlib") ("default" "openblas") ("accelerate" "lapack-sys/accelerate"))))))

(define-public crate-lapack-0.11.4 (c (n "lapack") (v "0.11.4") (d (list (d (n "lapack-sys") (r "^0.11") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "0vla2h2bify441ws4sdaxhm429rv3ap51rh9g8177nia9khvjwp3") (f (quote (("openblas" "lapack-sys/openblas") ("netlib" "lapack-sys/netlib") ("default" "openblas") ("accelerate" "lapack-sys/accelerate"))))))

(define-public crate-lapack-0.12.0 (c (n "lapack") (v "0.12.0") (d (list (d (n "lapack-sys") (r "^0.11") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "1ln29z3ibvdbi95i4cajllhcs25jf37p3i5m1y4zb0idg9pa6jlf") (f (quote (("openblas" "lapack-sys/openblas") ("netlib" "lapack-sys/netlib") ("default" "openblas") ("accelerate" "lapack-sys/accelerate"))))))

(define-public crate-lapack-0.13.0 (c (n "lapack") (v "0.13.0") (d (list (d (n "lapack-sys") (r "^0.11") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "10znv3wbsm599qxdrfc8by2x6ifivxiq9n1a31p3s9kscidh7941") (f (quote (("openblas" "lapack-sys/openblas") ("netlib" "lapack-sys/netlib") ("default" "openblas") ("accelerate" "lapack-sys/accelerate"))))))

(define-public crate-lapack-0.13.1 (c (n "lapack") (v "0.13.1") (d (list (d (n "lapack-sys") (r "^0.11") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "1nsi726zlvnn1iin2pmqwdil9dsvrniakjwjwlgad8712y24z81s") (f (quote (("openblas" "lapack-sys/openblas") ("netlib" "lapack-sys/netlib") ("default" "openblas") ("accelerate" "lapack-sys/accelerate"))))))

(define-public crate-lapack-0.13.2 (c (n "lapack") (v "0.13.2") (d (list (d (n "lapack-sys") (r "^0.11") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "0kncv32i1lv8wykbff7xpsmj6x0hgqskwmaz39b6zlhlq70b831f") (f (quote (("openblas" "lapack-sys/openblas") ("netlib" "lapack-sys/netlib") ("default" "openblas") ("accelerate" "lapack-sys/accelerate"))))))

(define-public crate-lapack-0.14.0 (c (n "lapack") (v "0.14.0") (d (list (d (n "lapack-sys") (r "^0.11") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "1gnmksar0v6wcq30mwq1cb397wbznr4jyix8g3y2fmqpcaim3faj") (f (quote (("openblas" "lapack-sys/openblas") ("netlib" "lapack-sys/netlib") ("default" "openblas") ("accelerate" "lapack-sys/accelerate"))))))

(define-public crate-lapack-0.14.1 (c (n "lapack") (v "0.14.1") (d (list (d (n "lapack-sys") (r "^0.11") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "13947af98g0jff9h0n21rbjbjl3rkip5kzjcvk6cj7qc1b0m80jf") (f (quote (("openblas" "lapack-sys/openblas") ("netlib" "lapack-sys/netlib") ("default" "openblas") ("accelerate" "lapack-sys/accelerate"))))))

(define-public crate-lapack-0.15.0 (c (n "lapack") (v "0.15.0") (d (list (d (n "lapack-sys") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "144xwjd75hq0ib5cldz0fczpq997wqa8aq3cidfaz2940zmmw0hr")))

(define-public crate-lapack-0.15.1 (c (n "lapack") (v "0.15.1") (d (list (d (n "lapack-sys") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "1bh5j0yqgg3vaz88ppc6515v18qcb62845qczis2sd48hdxz6wgf")))

(define-public crate-lapack-0.15.2 (c (n "lapack") (v "0.15.2") (d (list (d (n "lapack-sys") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "0dlha3q2dc0xla1lhxj6yqxwh8frskrxz6c5qqm989man71h8ls7")))

(define-public crate-lapack-0.15.3 (c (n "lapack") (v "0.15.3") (d (list (d (n "lapack-sys") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (k 0)))) (h "1gnkdvp83r0m03hn84gfdr4bvhgkxfvpy83jvgchwqkr8y437dgg") (y #t)))

(define-public crate-lapack-0.16.0 (c (n "lapack") (v "0.16.0") (d (list (d (n "lapack-sys") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (k 0)))) (h "0ggsxw7msih0mrllqdiqd43pwkqyrscmk3qy903iygviidrskqa6")))

(define-public crate-lapack-0.17.0 (c (n "lapack") (v "0.17.0") (d (list (d (n "lapack-sys") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.3") (k 0)))) (h "1sa0xwv7mwncyfyjdx6jwvd048sxf476s17a2zim6aprh2rkq6q4")))

(define-public crate-lapack-0.18.0 (c (n "lapack") (v "0.18.0") (d (list (d (n "lapack-sys") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (k 0)))) (h "09lvsp8zkby1a7aksk898m75vjvhi3ris2gnwk2kp48hmx800zva")))

(define-public crate-lapack-0.19.0 (c (n "lapack") (v "0.19.0") (d (list (d (n "lapack-sys") (r "^0.14") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (k 0)))) (h "01yja6fdjr7h6nqhlamn0mhqv5533735030av2gnmrzp9mmnlrxd")))

