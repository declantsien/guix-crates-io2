(define-module (crates-io la pa lapacke-sys) #:use-module (crates-io))

(define-public crate-lapacke-sys-0.0.1 (c (n "lapacke-sys") (v "0.0.1") (h "145a4dzgwv2iysw8pm3k2idjh9ssj2hna7y8xwp56rhvap29l61h")))

(define-public crate-lapacke-sys-0.1.0 (c (n "lapacke-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gsj1bvwlbvaqx1flgszwbp803kb3d5c3pm6r1bwapjpyqlsiavn")))

(define-public crate-lapacke-sys-0.1.1 (c (n "lapacke-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19dr97g4wpvcsdkqsxrcni6mrwc5a793x6vg0gj1kpf2vbnw9aki")))

(define-public crate-lapacke-sys-0.1.2 (c (n "lapacke-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wxy2bj67dnn1bay6sw999irlrzv95bf4bcm93csh1b1ipj6xyjw")))

(define-public crate-lapacke-sys-0.1.3 (c (n "lapacke-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ax4jkdxdf8z2s06hsmk930nqvgizmmqjajizjyjlzax253ihbdi")))

(define-public crate-lapacke-sys-0.1.4 (c (n "lapacke-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vmark9mrpyqig2kcxcscpgzkp4qchfy0g8m7fgh59plqqbhhz9g")))

