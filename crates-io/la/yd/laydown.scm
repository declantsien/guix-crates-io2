(define-module (crates-io la yd laydown) #:use-module (crates-io))

(define-public crate-laydown-0.1.0 (c (n "laydown") (v "0.1.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "ron") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w9zlamj5f8g3acz255lg1gh98dzhqwa18qq0z8qqhv7jc9bgb4c") (y #t)))

(define-public crate-laydown-1.0.0 (c (n "laydown") (v "1.0.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "ron") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "19lpf7vfyj13s2zc20pql8j57d8lv260prdp7lzif4m2dmqcc71v")))

(define-public crate-laydown-1.1.0 (c (n "laydown") (v "1.1.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "ron") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vxiq2rlw2hb7j146h9pdnq1qmkp27km8h1bx4sh48rbbpv67c1w")))

(define-public crate-laydown-1.2.0 (c (n "laydown") (v "1.2.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "ron") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vmlqxcrqiqz4166g2raagswlg3jdscmwq4n3yy50q1qpswg2mkw")))

(define-public crate-laydown-1.3.0 (c (n "laydown") (v "1.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "ron") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ixzarvn1n1l98ckm5qzr6bfrbv7rsq8ixpfv1ngzkad48hwgjba")))

(define-public crate-laydown-1.4.0 (c (n "laydown") (v "1.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "ron") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sb9fiayp3r1b63n6fvb2p9lkxjdwrsbv6jnbjwdldnsbs650krg")))

(define-public crate-laydown-1.4.1 (c (n "laydown") (v "1.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "ron") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vrg20qvnszp3fyywrn9gz6larazcabwjczp70rlcccvpfyz7qb7")))

(define-public crate-laydown-1.5.0 (c (n "laydown") (v "1.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "ron") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p4ws436yaz704qzpvcgn23gdd5i7c986zh8a5jl3rl7nb9jyx66")))

(define-public crate-laydown-1.5.1 (c (n "laydown") (v "1.5.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "ron") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pvjklkl25f4ypnv1yk2bs6sddkaix5h5qqxvmnbly7c454l9cqv")))

(define-public crate-laydown-1.6.0 (c (n "laydown") (v "1.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "ron") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f8a5n55i4zrg52gmmzafpzdhwj9fd5wf6hq5p2zlc3cy4mr7wpb")))

(define-public crate-laydown-2.0.0 (c (n "laydown") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "ron") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "03m5vk1fflg9j8jjr2qxb3l7r0byqin14mc0nqrabm1cri5vab48")))

(define-public crate-laydown-2.0.1 (c (n "laydown") (v "2.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "ron") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "12hny7kfqzfhkfc5c7g7vgws35rwvrqvvfw6x3h8xjgb6j6n7hzd")))

(define-public crate-laydown-2.1.0 (c (n "laydown") (v "2.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "ron") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p1dicxvbg89yxv822c5qcs4slaf4f8lq74bkja0zwf845ky8psb")))

(define-public crate-laydown-2.2.0 (c (n "laydown") (v "2.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "ron") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qffv64mm9hqkn2kqdkjz6g4nxmwisccn68fwdnf3l7z0kplzkkh")))

(define-public crate-laydown-2.3.0 (c (n "laydown") (v "2.3.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1d2bm7s93dsmjihd4r9dc47mgskb27qs8bnbfysmbwii5f4wwsyd")))

(define-public crate-laydown-2.4.0 (c (n "laydown") (v "2.4.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xw7x8pq4cb57kx0984bjd0v22z4pmrb9shrp36f493bcalwd41d")))

(define-public crate-laydown-2.4.1 (c (n "laydown") (v "2.4.1") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "03ym0pyxnzfyrya87r1cry1526lm2lvvh4cc6klyg0h0mh92mpv2")))

(define-public crate-laydown-2.5.0 (c (n "laydown") (v "2.5.0") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vari4dm9jlsy597s65yy32cqh05k50405x4scbkzhdfjgnjcbvj")))

(define-public crate-laydown-2.6.2 (c (n "laydown") (v "2.6.2") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j77rg669kwbwg9l3p0cc2dc7msfj4580bxsxwfxbwcaa954ajly")))

(define-public crate-laydown-2.6.3 (c (n "laydown") (v "2.6.3") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c8hqafq9a1620qr96iidzidjjnwz8bn2jl84m12ck0a5gxxqnvq")))

(define-public crate-laydown-2.6.4 (c (n "laydown") (v "2.6.4") (d (list (d (n "assert_cmd") (r "^2") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "16k8x3hfdl3iw3wgi1gbhvh8waqywlbd6kjaclrwy2mnag2fm8qb")))

