(define-module (crates-io la ds ladspa) #:use-module (crates-io))

(define-public crate-ladspa-0.1.0 (c (n "ladspa") (v "0.1.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "1fhqscw4kaflgdn6m088fj27myqk9z3n4q3wgjd03ib4xvyfwibx")))

(define-public crate-ladspa-0.1.1 (c (n "ladspa") (v "0.1.1") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "0101k2scylgxjxyydkn1x1z3y7d77hizl7bhjhy4zn00gb9d3rlz")))

(define-public crate-ladspa-0.1.2 (c (n "ladspa") (v "0.1.2") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "03paw4zc7h0clmvxjnsvpkidkqaw6a93kbf0vi5qi1ycgqyasall")))

(define-public crate-ladspa-0.1.3 (c (n "ladspa") (v "0.1.3") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "05ls02ycv8ay05w394kxdryny33k9kdkn2drahqn6dq8v8fwprga")))

(define-public crate-ladspa-0.1.4 (c (n "ladspa") (v "0.1.4") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "0r6p7yq94q4nzfkrv28gnx70shb9wb1jzzchrik7hm30b5mpy7yc")))

(define-public crate-ladspa-0.2.0 (c (n "ladspa") (v "0.2.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)))) (h "1h4ry1ar8xb59bnd1wf9sf3x7072vhnrkzwb75cr84wj24j8z7ni")))

(define-public crate-ladspa-0.2.1 (c (n "ladspa") (v "0.2.1") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "vec_map") (r "*") (d #t) (k 0)))) (h "0sqvm0w5h5kkjf19il29kpnph9ch0h81lhvbz97cdxq88pmkm48g")))

(define-public crate-ladspa-0.2.2 (c (n "ladspa") (v "0.2.2") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "vec_map") (r "*") (d #t) (k 0)))) (h "0mhrkxskl7b72n3bpa9sqvaxkab46gyqps1gxag2s75bigirgm8n")))

(define-public crate-ladspa-0.2.3 (c (n "ladspa") (v "0.2.3") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "vec_map") (r "^0.4.0") (d #t) (k 0)))) (h "0srpf84pcx9mbbqrr4y60hsfjwyfd8hclvamf051da7pvgbdb9fb")))

(define-public crate-ladspa-0.3.0 (c (n "ladspa") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "vec_map") (r "^0.4.0") (d #t) (k 0)))) (h "1d0nlpwi3zw19hsmp1i6nbq5wk98w23jh0rb5l3nw2f1n9g3f3yk")))

(define-public crate-ladspa-0.3.0-1 (c (n "ladspa") (v "0.3.0-1") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "vec_map") (r "^0.4.0") (d #t) (k 0)))) (h "1bmr2kxdhdmgc01m54zs13figdygzpww2v8wkwr5n9b89wr3054r")))

(define-public crate-ladspa-0.3.1 (c (n "ladspa") (v "0.3.1") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "vec_map") (r "^0.6.0") (d #t) (k 0)))) (h "1y1vyyw7v0rrmz265qig0mjiz3k224a8hgnn45snc1mmphmabmiw")))

(define-public crate-ladspa-0.3.2 (c (n "ladspa") (v "0.3.2") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "vec_map") (r "^0.6.0") (d #t) (k 0)))) (h "1yzkd2rm5hki7kd878n47im5yp470789v2xx9lwb7d6v5xg1p1ws")))

(define-public crate-ladspa-0.3.3 (c (n "ladspa") (v "0.3.3") (d (list (d (n "bitflags") (r "^0.8.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "vec_map") (r "^0.7.0") (d #t) (k 0)))) (h "1x854n15wvr4k6975wjy5cp1mjqyssdv3ns254ia3m3bdi4i12sj")))

(define-public crate-ladspa-0.3.4 (c (n "ladspa") (v "0.3.4") (d (list (d (n "bitflags") (r "^0.8.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)) (d (n "vec_map") (r "^0.7.0") (d #t) (k 0)))) (h "0rx29baws6ps433xv8g117z3rck5fjs8k5kf4759xa9xibxy55v1")))

