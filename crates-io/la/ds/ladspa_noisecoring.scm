(define-module (crates-io la ds ladspa_noisecoring) #:use-module (crates-io))

(define-public crate-ladspa_noisecoring-1.0.0 (c (n "ladspa_noisecoring") (v "1.0.0") (d (list (d (n "ladspa") (r "^0.3.4") (d #t) (k 0)) (d (n "rustfft") (r "^2.0.0") (d #t) (k 0)))) (h "004wdfb4dx4k29xidnnh4s4200m0zmi42w8yh6qj85ar1shg3sv1")))

