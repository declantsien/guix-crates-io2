(define-module (crates-io la pl laplade) #:use-module (crates-io))

(define-public crate-laplade-0.1.0 (c (n "laplade") (v "0.1.0") (h "1q9a01x9cnh03j64awbqg8ib0cjqq8fn536a2zmy33m489da5qhp")))

(define-public crate-laplade-0.1.1 (c (n "laplade") (v "0.1.1") (h "0l7pr9p95d8bq063ziaafi895nychk4f6lbk5dianad9gqc5mdn5")))

(define-public crate-laplade-0.1.2 (c (n "laplade") (v "0.1.2") (h "05l46xypvlqmlb8m9il1m77f34w860hapj3zf6z22xpn6ja328cr")))

