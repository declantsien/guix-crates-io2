(define-module (crates-io la te latexify) #:use-module (crates-io))

(define-public crate-latexify-0.0.0 (c (n "latexify") (v "0.0.0") (h "0aagsyv04pnv5r5b886yi257v604yp4qpwkq1nmskj779biqlrcz") (f (quote (("default"))))))

(define-public crate-latexify-0.0.1 (c (n "latexify") (v "0.0.1") (h "19b7xlzw21z7z19c6wl1pd2pacph0zh5bq2c80v5id1zl7sqhlab") (f (quote (("default"))))))

(define-public crate-latexify-0.1.0 (c (n "latexify") (v "0.1.0") (h "0f34rwvhbbndmw8kbi9jv2awqinhg9pr0214zb89s6sv7pgkqbb3") (f (quote (("default"))))))

