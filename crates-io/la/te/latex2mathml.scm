(define-module (crates-io la te latex2mathml) #:use-module (crates-io))

(define-public crate-latex2mathml-0.1.0 (c (n "latex2mathml") (v "0.1.0") (h "1fbpi5pqq007m5svivkakf3kqmk6pmfhdjc7djv4xi1r6x2d8mbd")))

(define-public crate-latex2mathml-0.1.1 (c (n "latex2mathml") (v "0.1.1") (h "1v3g8x1k1405xxkn9ppbvzb1riwbszxh3d8plijchfkacbqzzw28")))

(define-public crate-latex2mathml-0.2.0 (c (n "latex2mathml") (v "0.2.0") (h "0gljjqdzr7rzypcqvzz34wymlgrfb6kka7w2287bakb5mrcag9c7")))

(define-public crate-latex2mathml-0.2.1 (c (n "latex2mathml") (v "0.2.1") (h "1zcy4fhycxqzh3wvfm4xbp35cxxmyib7pxp4zgbxhmdj60bfss4k")))

(define-public crate-latex2mathml-0.2.2 (c (n "latex2mathml") (v "0.2.2") (h "18mij25s8j497g4dnlr5wlahvr41lbjxipp94yj3mwzf3yaqcwjz")))

(define-public crate-latex2mathml-0.2.3 (c (n "latex2mathml") (v "0.2.3") (h "0r87f6g315hz3j0dv95gl0nivjiqcpiyxjg0wrja4qxsnfyzb337")))

