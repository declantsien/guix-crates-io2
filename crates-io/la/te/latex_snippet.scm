(define-module (crates-io la te latex_snippet) #:use-module (crates-io))

(define-public crate-latex_snippet-0.1.0 (c (n "latex_snippet") (v "0.1.0") (h "14scz55y7gjkgna2f9ix4kp6v4x61l2kcqjz4ck38mfii9irnwsm")))

(define-public crate-latex_snippet-0.1.4 (c (n "latex_snippet") (v "0.1.4") (h "1ckdknrc0h0859m4fpbk122n1ppk097if8wc0j98sixjz7d1fc4w")))

(define-public crate-latex_snippet-0.1.5 (c (n "latex_snippet") (v "0.1.5") (h "1mrf02ksyl5h0j87nrm81hnghzsdaw2qsgcmg8c8pxrcpc4k2qqf")))

(define-public crate-latex_snippet-0.1.6 (c (n "latex_snippet") (v "0.1.6") (h "1h4xa7fwpi08ys6aqjh81pclpm2hmmjjm6wjgp6hzhfsmd6y3kp8")))

(define-public crate-latex_snippet-0.1.7 (c (n "latex_snippet") (v "0.1.7") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "15l9pv2d189p7d0jd1x4yhakkw3l1d9hrdnfxmy9ddqpw8zbrzic")))

(define-public crate-latex_snippet-0.1.8 (c (n "latex_snippet") (v "0.1.8") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "07lygm1v5y601rizrm4byglpm3lyv6svi3q88s7m6hshgfdyh7dd")))

(define-public crate-latex_snippet-0.1.9 (c (n "latex_snippet") (v "0.1.9") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1facn6ipyvg0j047mpw4ylp2506m6749rmb9lj9c05ba7qmsi5zx")))

(define-public crate-latex_snippet-0.1.10 (c (n "latex_snippet") (v "0.1.10") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0347igsdj87k7vjzs4rip80lnpp1115nmh272xn2rs25w6qipd8j")))

(define-public crate-latex_snippet-0.1.11 (c (n "latex_snippet") (v "0.1.11") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1k2x1mm9hb986p81j71srzg513p584b0r6rgcgkx74bvdjj35d2i")))

(define-public crate-latex_snippet-0.1.12 (c (n "latex_snippet") (v "0.1.12") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1vpidzh6nzi49p0hbb5mcgqk63c7hj1kkf5zv3796bdsm3y6p3g2")))

(define-public crate-latex_snippet-0.1.13 (c (n "latex_snippet") (v "0.1.13") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "115wp677c2vx20q5kc68l4bxa23ap18jmvy55a870v03w20j88a3")))

(define-public crate-latex_snippet-0.2.0 (c (n "latex_snippet") (v "0.2.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.2") (o #t) (d #t) (k 0)))) (h "0id4bbij26z1500mfy782r5y8spfr8ml1m2n1j41rk78pzy5p911") (f (quote (("default" "console_error_panic_hook"))))))

(define-public crate-latex_snippet-0.3.0 (c (n "latex_snippet") (v "0.3.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.2") (o #t) (d #t) (k 0)))) (h "0yqk3apa8fakh0yx72cy44lja0wyxc2ahzb0z9i8riizg8qykkdr") (f (quote (("default" "console_error_panic_hook"))))))

(define-public crate-latex_snippet-0.3.2 (c (n "latex_snippet") (v "0.3.2") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.2") (o #t) (d #t) (k 0)))) (h "1id9szs53v60f4265a1ycyf5p9x6nd3w3pswk856aa0vzxzs11f8") (f (quote (("default" "console_error_panic_hook"))))))

(define-public crate-latex_snippet-0.3.3 (c (n "latex_snippet") (v "0.3.3") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "expect-test") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.2") (o #t) (d #t) (k 0)))) (h "1pw0y749b5mi4g6w5baqwrbjg46nk3w3c9zm72ycn3f7wryvbd80") (f (quote (("default" "console_error_panic_hook"))))))

