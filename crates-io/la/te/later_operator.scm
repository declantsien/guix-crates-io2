(define-module (crates-io la te later_operator) #:use-module (crates-io))

(define-public crate-later_operator-0.1.0 (c (n "later_operator") (v "0.1.0") (d (list (d (n "serde") (r "1.0.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "1.0.*") (d #t) (k 2)) (d (n "trimothy") (r "0.1.*") (d #t) (k 0)))) (h "1qq2si8qa41mw4lg8ckv3x95cwgp3iyv9syvjzjq3pshi1i3ib2x") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.60")))

(define-public crate-later_operator-0.1.1 (c (n "later_operator") (v "0.1.1") (d (list (d (n "serde") (r "1.0.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "1.0.*") (d #t) (k 2)) (d (n "trimothy") (r "0.1.*") (d #t) (k 0)))) (h "0ldvx04f1ai6fgxx5r0mb3drqppii9lzngwivhd1hlxv78n291rr") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.60")))

(define-public crate-later_operator-0.1.2 (c (n "later_operator") (v "0.1.2") (d (list (d (n "serde") (r "1.0.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "1.0.*") (d #t) (k 2)) (d (n "trimothy") (r "0.1.*") (d #t) (k 0)))) (h "0bzcalliq7apsykfy1gnfsb8nas4qavq887b4ax99qg3qwy80fad") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.60")))

(define-public crate-later_operator-0.1.3 (c (n "later_operator") (v "0.1.3") (d (list (d (n "serde") (r "1.0.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "1.0.*") (d #t) (k 2)) (d (n "trimothy") (r "0.2.*") (d #t) (k 0)))) (h "1cdbpjpydlf5hhl07j3ypswbsb9102vnhb35kpvd2v0r38r63vyc") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.60")))

(define-public crate-later_operator-0.2.0 (c (n "later_operator") (v "0.2.0") (d (list (d (n "serde") (r "1.0.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "1.0.*") (d #t) (k 2)) (d (n "trimothy") (r "0.2.*") (d #t) (k 0)))) (h "1bygsx0v6rx1spyhdsjd8li9j7zl8hm7slphq96w94j1d13ksql7") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.60")))

