(define-module (crates-io la te lateinit) #:use-module (crates-io))

(define-public crate-lateinit-0.1.0 (c (n "lateinit") (v "0.1.0") (h "0b8zpakiz4cgp5ncd3bsf32hjjb0bc21y24ss8v404yjbcz9wpmg") (f (quote (("unchecked"))))))

(define-public crate-lateinit-0.1.1 (c (n "lateinit") (v "0.1.1") (h "1s0jrgdsx5lv2r6v2z0nv594lvs6xq3bypq64rrdghw9a5scdx4p") (f (quote (("unchecked"))))))

(define-public crate-lateinit-0.2.0 (c (n "lateinit") (v "0.2.0") (h "1q2df8h9lbgpm26926vcl8zj1mafadfqklgbpxxvri5j7g2lrd5a") (f (quote (("debug_unchecked"))))))

