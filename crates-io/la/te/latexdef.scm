(define-module (crates-io la te latexdef) #:use-module (crates-io))

(define-public crate-latexdef-0.1.0 (c (n "latexdef") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "133byi22wqn42ywv8z5m37skf7j6nqj19j86fq3hazyxn1kwa8mq")))

(define-public crate-latexdef-0.2.0 (c (n "latexdef") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1l3gg93lz1ji0677q7vfmcmgfy58jnnh9xqqj9r78j6a4f9mc0a9")))

(define-public crate-latexdef-0.3.0 (c (n "latexdef") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1md8zs1w0zm3b0899rji0z589x4qr0jby8ag7c5qsg7c1r5b5g5q")))

(define-public crate-latexdef-0.4.1 (c (n "latexdef") (v "0.4.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "024wwlwna75nsaz1wrb2hi6zmvj76wml5vvj43j8z81a37j2llkn")))

(define-public crate-latexdef-0.4.2 (c (n "latexdef") (v "0.4.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)))) (h "1n5cpq3k9ccbc5njdx4f094dm5jl6l67qgd4p9v3gsl5r1wy061c")))

