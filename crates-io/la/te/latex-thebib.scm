(define-module (crates-io la te latex-thebib) #:use-module (crates-io))

(define-public crate-latex-thebib-0.1.0 (c (n "latex-thebib") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1dxg5rrxlrmshcfssa1nahlwlnx8bzc8g6lpg26gj79xr56zvfym") (y #t)))

(define-public crate-latex-thebib-0.2.0 (c (n "latex-thebib") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1y4pwh8icnlw07zi32sdz44df2sc6ll5ya9gmangh6pifpb1ph5i") (y #t)))

(define-public crate-latex-thebib-0.3.0 (c (n "latex-thebib") (v "0.3.0") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0jfgwhy8ng648m5cskk3xrck1nv6wxx3rnq2mv6xc25wgclnync3")))

(define-public crate-latex-thebib-0.3.1 (c (n "latex-thebib") (v "0.3.1") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1hjqc9aqllql4vvpsk6ld8kp62xqvmi5snc4hqg9ac3wpqsfabwg")))

(define-public crate-latex-thebib-0.3.2 (c (n "latex-thebib") (v "0.3.2") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1c1naa7sxbvzqsrirpmaw7p990la0n9biyn720vw5pzv0zg1rp69")))

(define-public crate-latex-thebib-0.3.3 (c (n "latex-thebib") (v "0.3.3") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "0i6vpvrhlj7k9czgl8p2z21zcdjw01hlszzynijw73js27dznm0a")))

