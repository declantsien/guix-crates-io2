(define-module (crates-io la te late_format) #:use-module (crates-io))

(define-public crate-late_format-1.0.0 (c (n "late_format") (v "1.0.0") (d (list (d (n "lazy-regex") (r "^3.0.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0vfw64kgbilqj2wkg3fijmhwvfib3gvnsnyx1j0lcfz1hlklq2n2")))

