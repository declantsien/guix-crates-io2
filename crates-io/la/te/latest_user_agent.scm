(define-module (crates-io la te latest_user_agent) #:use-module (crates-io))

(define-public crate-latest_user_agent-0.1.0 (c (n "latest_user_agent") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "simple_logger") (r "^2") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "0yb4x2drb4c06rh2dxsgpr0p8r8gm1lkfmp2qg36bpffbydfpcaw")))

(define-public crate-latest_user_agent-0.1.1 (c (n "latest_user_agent") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "simple_logger") (r "^2") (d #t) (k 2)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "06l99356z5sk3mbh06jyrawhaaq7s64j6mfghn98kgfimzir6q40")))

