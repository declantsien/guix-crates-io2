(define-module (crates-io la te latex_cleaner) #:use-module (crates-io))

(define-public crate-latex_cleaner-1.0.0 (c (n "latex_cleaner") (v "1.0.0") (d (list (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0vhq7614gnmylfpfzxvf9mm268frzfb6wni7gvqr4d1cs33mkbgd")))

(define-public crate-latex_cleaner-1.0.1 (c (n "latex_cleaner") (v "1.0.1") (d (list (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1qclydgxls678pn66hdv331j79qv19bzvi22gc05cd9pm3fdq46w")))

(define-public crate-latex_cleaner-1.1.0 (c (n "latex_cleaner") (v "1.1.0") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1kbdqcffmnifwp4ip9pdm5v7axp5z14mavh6vc5plnc1wnpqqnxc")))

(define-public crate-latex_cleaner-1.1.1 (c (n "latex_cleaner") (v "1.1.1") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0q0qgh2gzwp6k1fvw8a9vc23j6ccm99k5zrlcvmfwfpmn2lb7q0w") (y #t)))

(define-public crate-latex_cleaner-1.2.0 (c (n "latex_cleaner") (v "1.2.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0j9bgdc090akmfz37lcxhcqxzyv7cnn0hd4h8fc5qy21ysxmva2y") (y #t)))

(define-public crate-latex_cleaner-1.2.1 (c (n "latex_cleaner") (v "1.2.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1bbcz3mpx2q0g2xw2cqly6j4i1ayy6985kwsgqjkkqd8bb1sh0q4")))

(define-public crate-latex_cleaner-1.3.0 (c (n "latex_cleaner") (v "1.3.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.5") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1h2bas34r4d9xw5jdk2x2azqks4i7cxynymyxzqcx0pxibqamrxc")))

