(define-module (crates-io la te lateximggen-lib) #:use-module (crates-io))

(define-public crate-LatexImgGen-lib-0.1.0 (c (n "LatexImgGen-lib") (v "0.1.0") (d (list (d (n "mathjax") (r "^0.1.1") (d #t) (k 0)))) (h "1ngv9w66rnnz0c4xs7p3zff0pzrcby6djqbbqbxg6pm2nwd9r7ms")))

(define-public crate-LatexImgGen-lib-0.1.1 (c (n "LatexImgGen-lib") (v "0.1.1") (d (list (d (n "mathjax") (r "^0.1.1") (d #t) (k 0)))) (h "1k7yvn0mr2wdfqfd2k79j874d9zbgpqk9jj4gi82slqkc48x994q")))

(define-public crate-LatexImgGen-lib-0.1.2 (c (n "LatexImgGen-lib") (v "0.1.2") (d (list (d (n "mathjax") (r "^0.1.1") (d #t) (k 0)))) (h "1j0la2m49raqi3frcbmzq67gxg8nlkrwpmsdrd46ppvpf1d8aqkm")))

