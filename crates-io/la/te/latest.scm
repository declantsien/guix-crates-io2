(define-module (crates-io la te latest) #:use-module (crates-io))

(define-public crate-latest-0.0.1 (c (n "latest") (v "0.0.1") (h "1ca5vrkzwnk9xsa75hz5hmfpmxvph4q8frwy52rkiv0ryvkb71si")))

(define-public crate-latest-0.0.2 (c (n "latest") (v "0.0.2") (h "01nx9j8nhhs3qibwwaqpzpwa4hizz48if5a6cmxmd1kafk729wvv")))

(define-public crate-latest-0.1.0 (c (n "latest") (v "0.1.0") (h "0cw4rg92f4k448iv30x7ac1yjyb10978yrv5qbr343x3ch3s89gc")))

(define-public crate-latest-0.1.1 (c (n "latest") (v "0.1.1") (h "02nvkp4iwwk76z37f9d2q8nd3vbzi4gx56mnkq0il4v5r7zwpknv")))

