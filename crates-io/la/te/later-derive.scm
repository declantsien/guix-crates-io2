(define-module (crates-io la te later-derive) #:use-module (crates-io))

(define-public crate-later-derive-0.0.1 (c (n "later-derive") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1z9gcb3vr7d47pq9rjf5x11f0mbipvgm04k6hd19mgfg75aa1fd2")))

(define-public crate-later-derive-0.0.2 (c (n "later-derive") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07sx14viv0fw281i7107xj7m37xq4xbbkaw92azvjqzciv8wqjig")))

(define-public crate-later-derive-0.0.3 (c (n "later-derive") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1254xm85iylw9gp74754fkxada04dzj28ffn4031c1mjsplhycxd")))

(define-public crate-later-derive-0.0.4 (c (n "later-derive") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1rsi9s64pnfx35yc2iyxyfg7zg6n8i2k63fy72fsylxd8cgnpf9v")))

(define-public crate-later-derive-0.0.5 (c (n "later-derive") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11w6wdacx05h0w3pc90jrh1fq4194xkypi2b9b5b9q52g11hhqic")))

(define-public crate-later-derive-0.0.6 (c (n "later-derive") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13na8zb0jlyj22b8fgg51xk4izixv22nr1ypz1v16419w3lbv61y")))

