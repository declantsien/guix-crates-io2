(define-module (crates-io la te latexbuild) #:use-module (crates-io))

(define-public crate-latexbuild-0.1.0 (c (n "latexbuild") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "1lxacvnx91pyh8wmmxz3gwbil3ja61jh1f6hisg27n8q1b86nzmg")))

(define-public crate-latexbuild-0.2.0 (c (n "latexbuild") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "0f0khrn5yjs3ga4ha7lxz2p937xkyvmzm963r3wxxbphdzl4li4h")))

(define-public crate-latexbuild-0.3.0 (c (n "latexbuild") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1skig56809rk3rsw2jqsgyr4djw7r2zjkf2sln7m0z334pcbf4wi")))

(define-public crate-latexbuild-0.3.1 (c (n "latexbuild") (v "0.3.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1d7r2gm0dwa6jkjyy4bg3gyj3d564sdggg2qjnlc2z3sw0f6x1qz")))

(define-public crate-latexbuild-0.3.2 (c (n "latexbuild") (v "0.3.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "12msq4zjlz41xax9l9crfirzy18if7ajnl09adk4ijqv7v91bkna")))

