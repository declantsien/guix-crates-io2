(define-module (crates-io la te late_substitution) #:use-module (crates-io))

(define-public crate-late_substitution-1.0.0 (c (n "late_substitution") (v "1.0.0") (d (list (d (n "lazy-regex") (r "^3.0.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "05rdkvfc9snbzd02g8i4kkjwyw6bmhhn82y5mysw5irvqnyfavc7") (y #t)))

(define-public crate-late_substitution-1.0.1 (c (n "late_substitution") (v "1.0.1") (d (list (d (n "lazy-regex") (r "^3.0.1") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)))) (h "0z2y1zrdmx8jnaxi7vfsaf2py7a5rw1kx1b7pz1pgy28ss0wddgi") (y #t)))

