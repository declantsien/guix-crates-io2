(define-module (crates-io la te latex) #:use-module (crates-io))

(define-public crate-latex-0.1.0 (c (n "latex") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)))) (h "0bmb2blb2s7x25iqgvr8g5wff8px0sgnvgj4filgg6npwabv68q1")))

(define-public crate-latex-0.1.1 (c (n "latex") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)))) (h "0qqlcim7x8kd3qmyrfp5ljqvk3x2cx7vqj51dwbxs6kpbsr4nc0j")))

(define-public crate-latex-0.1.2 (c (n "latex") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)))) (h "1gdrm1qysc5zx7bsjkqp7c55wcrghdqmzxdd0xmkzmx8lc6k3v0z")))

(define-public crate-latex-0.2.0 (c (n "latex") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)))) (h "0rayzcl2y5mm69p0s47m1w0xvfj1nf72ni6k175gprmlhkkiq7cy")))

(define-public crate-latex-0.3.0 (c (n "latex") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1jvl92diasp507vjvgcvmlc1gk8p3p1jmv09r2iasabi1hzzbf4r")))

(define-public crate-latex-0.3.1 (c (n "latex") (v "0.3.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0hfj9ac5r0ryz8p697hvfjsbc4r5yp5xa4vmpm6xyd1ms8rz6764")))

