(define-module (crates-io la te latexcompile) #:use-module (crates-io))

(define-public crate-latexcompile-0.1.0 (c (n "latexcompile") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.3") (d #t) (k 0)))) (h "08afgy2c4v49dbddr1zx84pccb4j24ldfhw8rnj6iplm1mw6r7zd")))

