(define-module (crates-io la un launch-worker) #:use-module (crates-io))

(define-public crate-launch-worker-0.1.0 (c (n "launch-worker") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.61") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.4") (f (quote ("console" "Blob" "BlobPropertyBag" "Document" "Location" "Url" "Window" "Worker"))) (d #t) (k 0)))) (h "0p3lyxs0lsc93q2hli5lkgjcfxw4jypi2dnl2z3qmj1s7jb89jjp")))

