(define-module (crates-io la un launch-services) #:use-module (crates-io))

(define-public crate-launch-services-0.0.1 (c (n "launch-services") (v "0.0.1") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "core-foundation") (r "^0.6.4") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.6.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)))) (h "1fb3bky95kcfmbpziqn06w74d8wfr94fwsfmadrw9pz6fjdrcs6b")))

(define-public crate-launch-services-0.0.2 (c (n "launch-services") (v "0.0.2") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "core-foundation") (r "^0.6.4") (d #t) (k 0)) (d (n "core-foundation-sys") (r "^0.6.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)))) (h "0pf6hably3id2vdkw9fk9hfwpy9i0pqwa47mnwqh5yz3fb2kyraz")))

