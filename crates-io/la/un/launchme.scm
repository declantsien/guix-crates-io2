(define-module (crates-io la un launchme) #:use-module (crates-io))

(define-public crate-launchme-0.1.0 (c (n "launchme") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "font-kit") (r "^0.11.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (f (quote ("default" "ttf"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y9pbk77hfkan8lrfkzcwaj6r183rcfxpjxjyrc5dwnifbr34lav")))

(define-public crate-launchme-0.2.0 (c (n "launchme") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.21") (f (quote ("derive"))) (d #t) (k 0)) (d (n "font-kit") (r "^0.11.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "ron") (r "^0.8.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (f (quote ("default" "ttf"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cblcfy7yg890qsgh0lgj1gg9ikwkzj2s029nzb1skyn7zgs9gpb")))

