(define-module (crates-io la un launchy) #:use-module (crates-io))

(define-public crate-launchy-0.1.0 (c (n "launchy") (v "0.1.0") (d (list (d (n "embedded-graphics") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0311pn4hvs82292vp0apc82rmqkrzg4zcmkyajkzrqv4vfcidq70") (f (quote (("embedded-graphics-support" "embedded-graphics") ("default" "embedded-graphics-support"))))))

(define-public crate-launchy-0.2.0 (c (n "launchy") (v "0.2.0") (d (list (d (n "embedded-graphics") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.7") (d #t) (k 0)))) (h "082bjwilqkfvxfr1wsy14lqpjyg7mc06jyywdibgvpx8rigk5156")))

(define-public crate-launchy-0.3.0 (c (n "launchy") (v "0.3.0") (d (list (d (n "embedded-graphics") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "midir") (r "^0.9.1") (d #t) (k 0)) (d (n "nanorand") (r "^0.7") (d #t) (k 2)) (d (n "rodio") (r "^0.15.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0r4ap07hkqkxgnbrylqn7cz354l6i69javaa4ivrxd9znvir4m9d")))

