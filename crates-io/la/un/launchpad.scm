(define-module (crates-io la un launchpad) #:use-module (crates-io))

(define-public crate-launchpad-0.1.0 (c (n "launchpad") (v "0.1.0") (d (list (d (n "portmidi") (r "^0.2.4") (d #t) (k 0)))) (h "0hdwx7kg64zbqi0wj0ajg3x63mv97x919k3xhlkl9jcwzbg87k3r")))

(define-public crate-launchpad-0.1.1 (c (n "launchpad") (v "0.1.1") (d (list (d (n "portmidi") (r "^0.2.4") (d #t) (k 0)))) (h "0lq5k15ffhhwj0y66bjdp2yng1ahcgbxppzm0d2c6wf7mxlg6p89")))

