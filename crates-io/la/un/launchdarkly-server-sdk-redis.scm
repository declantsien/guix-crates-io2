(define-module (crates-io la un launchdarkly-server-sdk-redis) #:use-module (crates-io))

(define-public crate-launchdarkly-server-sdk-redis-1.0.0-rc.1 (c (n "launchdarkly-server-sdk-redis") (v "1.0.0-rc.1") (d (list (d (n "launchdarkly-server-sdk") (r "^1.0.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 2)) (d (n "redis") (r "^0.22.1") (f (quote ("tls"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (f (quote ("float_roundtrip"))) (d #t) (k 0)) (d (n "test-case") (r "^2.0.0") (d #t) (k 2)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "0immrc3qxlxyywz9j7azmmr8az6wkryqfg2pghk8dy8vq16rp6dc") (r "1.60.0")))

