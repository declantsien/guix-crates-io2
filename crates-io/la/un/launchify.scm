(define-module (crates-io la un launchify) #:use-module (crates-io))

(define-public crate-launchify-0.1.0 (c (n "launchify") (v "0.1.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "handlebars") (r "^3.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1rhnlh9j3cz2i1q8jn8blb73wiz2d195ksha9kxl6675676crgb0")))

(define-public crate-launchify-0.2.0 (c (n "launchify") (v "0.2.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "handlebars") (r "^3.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "18h20dfick1k4qbvqcxm4cbfgb090p8skyhhfj7lgqk58k195q76")))

(define-public crate-launchify-0.3.0 (c (n "launchify") (v "0.3.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "handlebars") (r "^3.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "which") (r "^4.0.2") (d #t) (k 0)))) (h "0jjqfycwqdzkwc9bbdsjx8jxi0dfai9i74c69fy8dj7n4i0017s4")))

(define-public crate-launchify-0.3.1 (c (n "launchify") (v "0.3.1") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "handlebars") (r "^3.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.62") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "which") (r "^4.0.2") (d #t) (k 0)))) (h "0pjk524d8fsdqs0q5d43xkkrdp4gkkj7v25ygqv1vg3r779ph095")))

