(define-module (crates-io la un launchd) #:use-module (crates-io))

(define-public crate-launchd-0.1.0 (c (n "launchd") (v "0.1.0") (d (list (d (n "cron") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "plist") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ai4rm191p8kmy38aqp2rww3jvi7m1kvlzsbnk0hw8cdhxmp5sm8") (f (quote (("io" "serde" "plist") ("default" "io"))))))

(define-public crate-launchd-0.2.0 (c (n "launchd") (v "0.2.0") (d (list (d (n "cron") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "plist") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1y8s9r5fxzvdnqbydbcx088l7qixhw6ldv8qdm67hakpmf9105vn") (f (quote (("io" "serde" "plist") ("default" "io"))))))

(define-public crate-launchd-0.3.0 (c (n "launchd") (v "0.3.0") (d (list (d (n "cron") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "plist") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1j8m3g0vy8gkp4jy3msbdx1jvpcii2np1q37rx7i76z6b458z7dk") (f (quote (("io" "serde" "plist") ("default" "io"))))))

