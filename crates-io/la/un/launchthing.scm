(define-module (crates-io la un launchthing) #:use-module (crates-io))

(define-public crate-launchthing-0.1.0 (c (n "launchthing") (v "0.1.0") (d (list (d (n "gtk") (r "^0.6.6") (f (quote ("v4_10"))) (d #t) (k 0) (p "gtk4")))) (h "05zmq47vh09qx88dc2qal53d8wvs4anzdr15vhy408szbhansf3p")))

(define-public crate-launchthing-0.1.1 (c (n "launchthing") (v "0.1.1") (d (list (d (n "gtk") (r "^0.6.6") (f (quote ("v4_10"))) (d #t) (k 0) (p "gtk4")))) (h "025i7ssyifina9s6hp3jffx07m7rky549nx90wh2zkh83q89yr1s")))

