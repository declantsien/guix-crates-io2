(define-module (crates-io la nb lanb) #:use-module (crates-io))

(define-public crate-lanb-0.1.0 (c (n "lanb") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("std" "backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "filey") (r "^0.3.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1lzfa0h2cmmfhw0icm4zs7kwiz6fhcxij74nwmg46bb27wm5zx0w") (y #t)))

(define-public crate-lanb-0.1.1 (c (n "lanb") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (f (quote ("backtrace" "std"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "filey") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1402llx8jlzgfk9id4q687c16bijnhywbda48djk7ccm22yazl6y") (y #t)))

