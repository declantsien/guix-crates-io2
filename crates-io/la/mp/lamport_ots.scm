(define-module (crates-io la mp lamport_ots) #:use-module (crates-io))

(define-public crate-lamport_ots-0.1.0 (c (n "lamport_ots") (v "0.1.0") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 2)))) (h "1xajlinqiz934sgs1awpyl66g7bv988dpapvxjxvngw30fn7p138")))

(define-public crate-lamport_ots-0.1.1 (c (n "lamport_ots") (v "0.1.1") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 2)))) (h "1y5bm0npvpbjkirbbcwsn2486h5zwsgs4gn15xswkqqbahvjxg1a")))

