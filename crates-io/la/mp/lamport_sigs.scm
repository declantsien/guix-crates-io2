(define-module (crates-io la mp lamport_sigs) #:use-module (crates-io))

(define-public crate-lamport_sigs-0.1.0 (c (n "lamport_sigs") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1w0plf8l1s8arhwqwcfnsdq9mf3sjrzgfix9dir0a1lv11m9myig")))

(define-public crate-lamport_sigs-0.1.1 (c (n "lamport_sigs") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1pjg3ynyiirg7b4bindqq1fmdgvjxgbarfd7x58rzrgrsj6kb57j")))

(define-public crate-lamport_sigs-0.1.2 (c (n "lamport_sigs") (v "0.1.2") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "ring") (r "^0.6.0") (d #t) (k 0)))) (h "1n6pqfij5bw2a8dlpc86bxbxai4il8nfcwccxp04h0j4xls40axc")))

(define-public crate-lamport_sigs-0.2.0 (c (n "lamport_sigs") (v "0.2.0") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "ring") (r "^0.9.3") (d #t) (k 0)))) (h "1j0ghshi8g5jzxhccn1rsfci00np9mhz172n2gx58iryz41cnda8")))

(define-public crate-lamport_sigs-0.2.1-pre (c (n "lamport_sigs") (v "0.2.1-pre") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "ring") (r "^0.9.3") (d #t) (k 0)))) (h "19wpz7z81xm20gv8pzkvw7s37c6s1rlj5n6zmng7a48x3cah48r1")))

(define-public crate-lamport_sigs-0.3.0 (c (n "lamport_sigs") (v "0.3.0") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "ring") (r "^0.11.0") (d #t) (k 0)))) (h "04yry4xiklg0bqgwllpsvgj8cniva3cp87sqv7qjljzv27jyws91")))

(define-public crate-lamport_sigs-0.4.0 (c (n "lamport_sigs") (v "0.4.0") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "ring") (r "^0.11.0") (d #t) (k 0)))) (h "1xbalm5idaif5l14avb2zjhv20cz4s6j1vabqp4kk52j11lz39by")))

(define-public crate-lamport_sigs-0.4.1 (c (n "lamport_sigs") (v "0.4.1") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "ring") (r "^0.11.0") (d #t) (k 0)))) (h "1k8bx2z3fd87q9f68g4m5581xs5l37kwn93pdba4fa91ri0xrmwi")))

(define-public crate-lamport_sigs-0.5.0 (c (n "lamport_sigs") (v "0.5.0") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "ring") (r "^0.12.0") (d #t) (k 0)))) (h "0myqfdcx2fz394p9hff637gfx0bw8i3wh5aqlvynmqcbanp2dzsx")))

(define-public crate-lamport_sigs-0.6.0 (c (n "lamport_sigs") (v "0.6.0") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "ring") (r "^0.13.0") (d #t) (k 0)))) (h "00cxap4kf6m770480lciq11rh9x6lgbylfclmlmlbvjf62sv66vs")))

(define-public crate-lamport_sigs-0.7.0 (c (n "lamport_sigs") (v "0.7.0") (d (list (d (n "ring") (r "^0.16.1") (d #t) (k 0)))) (h "0hsggnjqbivsviswhs5dz3s0yawfd1vdrcc8q9a6n8lsf7jvs114")))

