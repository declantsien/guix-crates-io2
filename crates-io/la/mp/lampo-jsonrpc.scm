(define-module (crates-io la mp lampo-jsonrpc) #:use-module (crates-io))

(define-public crate-lampo-jsonrpc-0.0.1-alpha.1 (c (n "lampo-jsonrpc") (v "0.0.1-alpha.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "ntest") (r "^0.9.0") (d #t) (k 2)) (d (n "popol") (r "^2.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "0apbfvv5k8rylwnqw7fim288c8p1v6rx3w4zijvq5745pb48sq1h")))

