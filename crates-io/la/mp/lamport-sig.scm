(define-module (crates-io la mp lamport-sig) #:use-module (crates-io))

(define-public crate-lamport-sig-0.1.0 (c (n "lamport-sig") (v "0.1.0") (h "11gn8cmgr8v51yzpvhy0lqpp4v0ys6c1kny9kljaxrd7dl6xjqx9")))

(define-public crate-lamport-sig-0.1.1 (c (n "lamport-sig") (v "0.1.1") (d (list (d (n "bigint") (r "^4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)))) (h "0vgyinf0mfnr3fh6m4g431wp1jk6c4pvk3yagjzkdj3x6aa0jmqw")))

