(define-module (crates-io la mp lamp) #:use-module (crates-io))

(define-public crate-lamp-0.0.0 (c (n "lamp") (v "0.0.0") (h "12k45gv0730r6im3mrxhy8kvh0mmqibfb91vf1zbbvrk56kmb2qq") (y #t)))

(define-public crate-lamp-0.2.2 (c (n "lamp") (v "0.2.2") (d (list (d (n "clap") (r "^3.2.12") (d #t) (k 0)) (d (n "clap") (r "^3.2.12") (d #t) (k 1)) (d (n "clap_complete") (r "^3.2.3") (d #t) (k 1)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "174n4j73n4qzli4nxxpa05lnmfsk93fxm7cjn0sqkpqy961z94br")))

(define-public crate-lamp-0.3.0 (c (n "lamp") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.12") (d #t) (k 0)) (d (n "clap") (r "^3.2.12") (d #t) (k 1)) (d (n "clap_complete") (r "^3.2.3") (d #t) (k 1)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "0jgsp9ch7ib5imv6h07ikyxi3b85b1y6fbvssyg51qwi0hp0ggbv")))

(define-public crate-lamp-0.3.1 (c (n "lamp") (v "0.3.1") (d (list (d (n "clap") (r "^3.2.12") (d #t) (k 0)) (d (n "clap") (r "^3.2.12") (d #t) (k 1)) (d (n "clap_complete") (r "^3.2.3") (d #t) (k 1)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "0l7xgsfmf3187rhd5ss2w9yic9iqcqaswyjkczghjvvrl48h7n61")))

