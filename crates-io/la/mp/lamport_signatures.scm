(define-module (crates-io la mp lamport_signatures) #:use-module (crates-io))

(define-public crate-lamport_signatures-0.1.0 (c (n "lamport_signatures") (v "0.1.0") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "ring") (r "^0.13.5") (d #t) (k 0)))) (h "0fcvl804hrd7brzwd9nb9mmq09jqazcfgpx0r100q291j897x10z")))

(define-public crate-lamport_signatures-0.2.0 (c (n "lamport_signatures") (v "0.2.0") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "ring") (r "^0.13.5") (d #t) (k 0)))) (h "01awph13208iv3q5gn1qp6bz1mv0xkr4xz95mllvyvq13rb5qyg5")))

(define-public crate-lamport_signatures-0.2.1 (c (n "lamport_signatures") (v "0.2.1") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "ring") (r "^0.13.5") (d #t) (k 0)))) (h "1vlw1f7lafwnd3wqaid3qg35aqc1hmccnrwc4d4pjrkyw06lvnc3")))

(define-public crate-lamport_signatures-0.2.2 (c (n "lamport_signatures") (v "0.2.2") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "ring") (r "^0.13.5") (d #t) (k 0)))) (h "0zp0zci6l3dv975l5nvalxbzp4zgd36y0nyq6x7ik1yxs1fql5cj")))

(define-public crate-lamport_signatures-0.2.3 (c (n "lamport_signatures") (v "0.2.3") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "ring") (r "^0.13.5") (d #t) (k 0)))) (h "0nz32y6033nrg291hjssn31ibdh5zq3vpp4biprhbfxpwnxj1khx")))

(define-public crate-lamport_signatures-0.2.4 (c (n "lamport_signatures") (v "0.2.4") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "ring") (r "^0.13.5") (d #t) (k 0)))) (h "131pp6xwscy2qb614s8ynpv971wgjjkwbsh05537h0z3w4fb4ayr")))

