(define-module (crates-io la tt lattices) #:use-module (crates-io))

(define-public crate-lattices-0.0.0 (c (n "lattices") (v "0.0.0") (d (list (d (n "sealed") (r "^0.5") (d #t) (k 0)))) (h "1xnrc4fpdrdib269qcbkkwm0ivk84ycl44mf468d274y9ks55h74")))

(define-public crate-lattices-0.1.0 (c (n "lattices") (v "0.1.0") (d (list (d (n "cc-traits") (r "^1.0.0") (d #t) (k 0)) (d (n "sealed") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0j40ng213x2mip5rppqcwwrvaxj4k6bpcxrj3ckq8hiycwq9kwmx") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-lattices-0.1.1 (c (n "lattices") (v "0.1.1") (d (list (d (n "cc-traits") (r "^2.0.0") (d #t) (k 0)) (d (n "sealed") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0kgydif7zxlq9yk5ihhhq00892c186zcnyb0fwg79j2niy9hwkh6") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-lattices-0.1.2 (c (n "lattices") (v "0.1.2") (d (list (d (n "cc-traits") (r "^2.0.0") (d #t) (k 0)) (d (n "sealed") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1haigch61yym085yw6n5wfbw7v5c5bmqx25i3ldzg90v72g20rlv") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-lattices-0.2.0 (c (n "lattices") (v "0.2.0") (d (list (d (n "cc-traits") (r "^2.0.0") (d #t) (k 0)) (d (n "sealed") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1nif02zma2k0x3pmx4x6fscwj1nr2szqizzifwfyj7yh5wzrs87y") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-lattices-0.3.0 (c (n "lattices") (v "0.3.0") (d (list (d (n "cc-traits") (r "^2.0.0") (d #t) (k 0)) (d (n "sealed") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1hqb6kfc4fvajki1fl7izlgrc9kss1pi0zrm1bbl4z8bdrhwg1rz") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-lattices-0.4.0 (c (n "lattices") (v "0.4.0") (d (list (d (n "cc-traits") (r "^2.0.0") (d #t) (k 0)) (d (n "sealed") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0r4y9agcvwzd5r7qkgi5zz16q6p0v0p8q0j7i75lp1aj22nl5qj6") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-lattices-0.5.0 (c (n "lattices") (v "0.5.0") (d (list (d (n "cc-traits") (r "^2.0.0") (d #t) (k 0)) (d (n "sealed") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jfa8cfd5z47ag5sszhhc633kn0vzjf3cjba21hip5rs6n1gyfsg") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-lattices-0.5.1 (c (n "lattices") (v "0.5.1") (d (list (d (n "cc-traits") (r "^2.0.0") (d #t) (k 0)) (d (n "sealed") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17jlnkb178hpvxky17d5c7s5hivhalvwg04a7inhgi14vk8bphjp") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-lattices-0.5.2 (c (n "lattices") (v "0.5.2") (d (list (d (n "cc-traits") (r "^2.0.0") (d #t) (k 0)) (d (n "sealed") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1n2xrzrfrgdcw7rsi8pnz269wk5x6z0wzc5an6y1ra5kx4fj7j3c") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-lattices-0.5.3 (c (n "lattices") (v "0.5.3") (d (list (d (n "cc-traits") (r "^2.0.0") (d #t) (k 0)) (d (n "sealed") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0zcm0ml5xagjdadphwb2ms97brdnr71n14haha9cw70ny5i2vlgi") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-lattices-0.5.4 (c (n "lattices") (v "0.5.4") (d (list (d (n "cc-traits") (r "^2.0.0") (d #t) (k 0)) (d (n "sealed") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1a06ybxlg7mj13wva89zr6cvc6x1ybjjy7rv0mb661m5cqlzfdwi") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-lattices-0.5.5 (c (n "lattices") (v "0.5.5") (d (list (d (n "cc-traits") (r "^2.0.0") (d #t) (k 0)) (d (n "sealed") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1m4jbw98lvyd020lvaqn2vd6s0pradbvqls7s2c8n8c5x495kash") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

