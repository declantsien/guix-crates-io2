(define-module (crates-io la tt latticequeries) #:use-module (crates-io))

(define-public crate-latticequeries-0.1.0 (c (n "latticequeries") (v "0.1.0") (h "0yibd7576g3hn66i4wkfdfb88fhmcwbwi3b6dg8w5mvy2296m7gc") (y #t)))

(define-public crate-latticequeries-0.1.1 (c (n "latticequeries") (v "0.1.1") (h "19fg6hj2qqhgqppbvyl8h6wq1pap4hdc29ipy697002dp9wyf3b9") (y #t)))

(define-public crate-latticequeries-0.1.2 (c (n "latticequeries") (v "0.1.2") (h "0d2kvxxydpb81vk7bq21b10raxqippl7094295vqs06ph09glm0x") (y #t)))

(define-public crate-latticequeries-0.1.3 (c (n "latticequeries") (v "0.1.3") (h "05cslmdyl43a3gl0xnxyznw00m26k7yk4i1ykksyv0rwjwly68qi")))

