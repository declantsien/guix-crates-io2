(define-module (crates-io la tt latte) #:use-module (crates-io))

(define-public crate-latte-0.1.0 (c (n "latte") (v "0.1.0") (h "0pm1wzqhrsfihgnxl3dir9q6w4bhsrz0nl6mm1jwphky8d8mgj2f")))

(define-public crate-latte-0.1.1 (c (n "latte") (v "0.1.1") (h "00ph3br70692ddrcba4a8iph2y5llw5bx3g44and4r9gcv43smc3")))

(define-public crate-latte-0.1.2 (c (n "latte") (v "0.1.2") (h "0a8kipzaxq5x3ybfxfhi88is76icxa81narralgxx7l1dh6yhky7")))

