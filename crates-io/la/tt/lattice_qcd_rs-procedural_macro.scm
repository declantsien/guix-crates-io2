(define-module (crates-io la tt lattice_qcd_rs-procedural_macro) #:use-module (crates-io))

(define-public crate-lattice_qcd_rs-procedural_macro-0.2.0 (c (n "lattice_qcd_rs-procedural_macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.17") (d #t) (k 0)) (d (n "syn") (r "^1.0.90") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "0xx5wn4xf8vrf1i0pqhhvar2rm24lgviyhh5l2hmgwzjwrn0ba7x")))

(define-public crate-lattice_qcd_rs-procedural_macro-0.2.1 (c (n "lattice_qcd_rs-procedural_macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.39") (d #t) (k 0)) (d (n "quote") (r "^1.0.19") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "06fcqnppsj5s1vjx0k3dbph84wc3skmfgvcyhm232a5kv3sfzqrl")))

