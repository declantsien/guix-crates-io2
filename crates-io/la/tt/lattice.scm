(define-module (crates-io la tt lattice) #:use-module (crates-io))

(define-public crate-Lattice-0.0.1 (c (n "Lattice") (v "0.0.1") (h "0ll9rzk65vhamcl94sdniix0956vrb6ffs1xxiaff6msbham4dx6")))

(define-public crate-Lattice-0.1.1 (c (n "Lattice") (v "0.1.1") (d (list (d (n "sdl2") (r "^0.30") (d #t) (k 0)))) (h "1892ca06s13kgywj7r2wg2q5zwd3y4dydcf4ji8cjcy03ay3kl7f")))

(define-public crate-Lattice-0.1.2 (c (n "Lattice") (v "0.1.2") (d (list (d (n "sdl2") (r "^0.30") (d #t) (k 0)))) (h "1m3lxivr2ipvj61gj3fci2fhh4zmmzp8ww0542p7h46d4bibywij")))

(define-public crate-Lattice-0.1.3 (c (n "Lattice") (v "0.1.3") (d (list (d (n "sdl2") (r "^0.30") (d #t) (k 0)))) (h "00n1mh4waysqjcwqsnfmvisw65pa35bkpd5aff5a18xr5z3vbjb3")))

(define-public crate-Lattice-0.1.4 (c (n "Lattice") (v "0.1.4") (d (list (d (n "sdl2") (r "^0.30") (d #t) (k 0)))) (h "0z6wnsr6zpbm3ivkxyp1yf71q9gmh7rrp9m66db25kqggdw1kbid")))

(define-public crate-Lattice-0.1.5 (c (n "Lattice") (v "0.1.5") (d (list (d (n "sdl2") (r "^0.30") (d #t) (k 0)))) (h "05z6i2l1266znyzgd9grav3dw2y533i2rffxg6n88cks8nn63xj0")))

(define-public crate-Lattice-0.1.6 (c (n "Lattice") (v "0.1.6") (d (list (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "tempfile") (r "^2.1") (d #t) (k 0)))) (h "07lf4pi92xyi4q8ybmfymj7wz1q2hym4gl9nw2v8c8h4z6ipcnd3")))

(define-public crate-Lattice-0.1.7 (c (n "Lattice") (v "0.1.7") (d (list (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "tempfile") (r "^2.1") (d #t) (k 0)))) (h "1d26s3pxha3jw8z87rx9rjbg8nkbf9s0qgjff6zdndhmgm1wbrbr")))

(define-public crate-Lattice-0.1.8 (c (n "Lattice") (v "0.1.8") (d (list (d (n "image") (r "^0.13.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)))) (h "16xwj4iisbxc5mmbhxzgf6sqvzqf41v1dcn4bj1qshhhc58qjcda")))

(define-public crate-Lattice-0.1.9 (c (n "Lattice") (v "0.1.9") (d (list (d (n "image") (r "^0.13.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)))) (h "1lgy1vbzcgbcaq5hrh46027yccafqi44wzhdnkd94miz93kzi5zh")))

(define-public crate-Lattice-0.1.11 (c (n "Lattice") (v "0.1.11") (d (list (d (n "image") (r "^0.13.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)))) (h "12aidc5a0b4bqf3lfhk5wg5lnwxg0vjigccxyd9rz2l03b5wy52v")))

(define-public crate-Lattice-0.1.12 (c (n "Lattice") (v "0.1.12") (d (list (d (n "image") (r "^0.13.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)))) (h "0ya1f0mw99lg2b76x30y3mzaa6n698vs5i01g59m4r6pw9lxyibh")))

(define-public crate-Lattice-0.1.13 (c (n "Lattice") (v "0.1.13") (d (list (d (n "image") (r "^0.13.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)))) (h "1xpz80l0bd8qc7ls1db72vwqlq30chd497zx662qkd8x3dnbw7dc")))

(define-public crate-Lattice-0.1.15 (c (n "Lattice") (v "0.1.15") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)))) (h "0vw59bsnlkknifcy6vglcxzgz8ngw8rkszfyi1jn40dk7l41x0fq")))

(define-public crate-Lattice-0.1.16 (c (n "Lattice") (v "0.1.16") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)))) (h "031mpwj9lldm7r8bcr4wcivz7nd389sivfiz886dhnv6z2vkfxif")))

(define-public crate-Lattice-0.1.17 (c (n "Lattice") (v "0.1.17") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)))) (h "0g2714n2iq2810j3l8hj7g77aw5706yc9gwyc4pa129jnhwnpvig")))

(define-public crate-Lattice-0.1.18 (c (n "Lattice") (v "0.1.18") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)))) (h "1xw3vrmvzv9zkq2mgir9f3iig3sklfzc9ni9ysnqav8kcys4xcg7")))

(define-public crate-Lattice-0.1.19 (c (n "Lattice") (v "0.1.19") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)))) (h "0hypxprvkglf2dfk25myxzglh0hb5m0m0rsr53wzgch0b2zh97ch")))

(define-public crate-Lattice-0.1.20 (c (n "Lattice") (v "0.1.20") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)))) (h "12mys16y81pq7x9jy4mzffsrrd5rsph5g43wwg8l5hp3hwkl0lxk")))

(define-public crate-Lattice-0.1.21 (c (n "Lattice") (v "0.1.21") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0pxycyfhlnq86xm9dlmm1b8dyrfhrsb23a0x30i8856gfdywba58")))

(define-public crate-Lattice-0.1.22 (c (n "Lattice") (v "0.1.22") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1x5gy237sxgcypkkqdcqjhm2ff64vzn2ymh1v43rc3vdc0a2xl3j")))

(define-public crate-Lattice-0.1.23 (c (n "Lattice") (v "0.1.23") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0hq7mzycyrmzxdzrzxvbfp4syavy553ib8fi4b1ka6xmww81hjsn")))

(define-public crate-Lattice-0.1.24 (c (n "Lattice") (v "0.1.24") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1rwib15zclwazs49ibcn17d4w099bhv7afh500czwgfmbcfh1sxh")))

(define-public crate-Lattice-0.1.25 (c (n "Lattice") (v "0.1.25") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "01y7ylgfxjw45hvpngsjc0ixm5840fymv8hml3gk6sfxj4wj929k")))

(define-public crate-Lattice-0.1.26 (c (n "Lattice") (v "0.1.26") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "05aaw57ds15cwgbrnwxbwi9r1c94af75dh4wcn4zkp35x3208xfr")))

(define-public crate-Lattice-0.1.27 (c (n "Lattice") (v "0.1.27") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1yp373n4q7hb1bqwi3y1r9kirwm5w7cpfd10yakcj0s2bfsn5jx5")))

(define-public crate-Lattice-0.2.0 (c (n "Lattice") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1l8v3g6lhclfsgf9kjx75sc61ijjk0c85zzcg7kv68vldpwav6rc")))

(define-public crate-Lattice-0.2.1 (c (n "Lattice") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "060c9fqhqza0zhbsjm6bhc56ldz6fd370j8j3ikzabgzn107d3zr")))

(define-public crate-Lattice-0.2.2 (c (n "Lattice") (v "0.2.2") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1dws1xw8nq15nk4wn1zr9csp9y1yzmh9dgswzkn9dib7g6wm1yc1")))

(define-public crate-Lattice-0.2.3 (c (n "Lattice") (v "0.2.3") (d (list (d (n "arrayvec") (r "^0.3") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "10ibzarmj0br0mwrdwjpcz3rc395k7sg45dncjgwi6lh1hj6p7v1")))

(define-public crate-Lattice-0.2.5 (c (n "Lattice") (v "0.2.5") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0a2nb3vjqhf031glwnisb617468n8npcg7qkas7bavbkhnd5ljc0")))

(define-public crate-Lattice-0.2.6 (c (n "Lattice") (v "0.2.6") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "012m31if5wzpmd2qqyzhvdw2l49viqkn7p0kvzvb9blwq05yzr9n")))

(define-public crate-Lattice-0.2.7 (c (n "Lattice") (v "0.2.7") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "15fcbqivwdjg8fp9w6bwqr2xcw37r170fk2hxm4g6hvb6q719ga7")))

(define-public crate-Lattice-0.2.8 (c (n "Lattice") (v "0.2.8") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1srkmavsa7nm033jbg7ashc9hi9gj7np9g7kspbg49102qk26hwi")))

(define-public crate-Lattice-0.2.9 (c (n "Lattice") (v "0.2.9") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1j3f5rjsfqdy368zjf7wsb92sq82zqj5vqz5yp5qs97wp3pfm19d")))

(define-public crate-Lattice-0.2.10 (c (n "Lattice") (v "0.2.10") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "03ahcnxqq9wjsy7v2hzs4f749xv7ih4dyajr0hmfhp7m15ajd868")))

(define-public crate-Lattice-0.2.11 (c (n "Lattice") (v "0.2.11") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0jlliq8w36awjml5csjpkakax2zdwh0bw8pzxygzhj987xi97p1j")))

(define-public crate-Lattice-0.2.12 (c (n "Lattice") (v "0.2.12") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1fkwkq7n2gbv30069zf639knhvfk6n7ai1v5f538gcn066sggm04")))

(define-public crate-Lattice-0.2.13 (c (n "Lattice") (v "0.2.13") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "00xzw3lb9d9clkfli2s1f0ssvwxbwdirrxz3k973rb2i9pw0vw6c")))

(define-public crate-Lattice-0.3.0 (c (n "Lattice") (v "0.3.0") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "14ldybahs33p7jla36zryrh5a21bilnzv8wnj3lhbgjxkq46msfk")))

(define-public crate-Lattice-0.3.1 (c (n "Lattice") (v "0.3.1") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1vf2q4c4ds5d2ychscbjcypv8ia2lz24pr0f3jfrmazgy2x3v1wa")))

(define-public crate-Lattice-0.3.2 (c (n "Lattice") (v "0.3.2") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "02w6xwz44ppl1jf503agpp2mppyw0qxqhmp9af525lilnzdkz4ij")))

(define-public crate-Lattice-0.3.3 (c (n "Lattice") (v "0.3.3") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0vvi7a1nxdrjigswfzv511bv22qwpyv1rl9yaq7ci60y3g823hka")))

(define-public crate-Lattice-0.3.4 (c (n "Lattice") (v "0.3.4") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0gjj4cn00ikwghciy3bn0qg2hlqh4jms1sxm817mnfcg87i3nclf")))

(define-public crate-Lattice-0.3.5 (c (n "Lattice") (v "0.3.5") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0kllrh711xhc52xbqxdsnfgb9qqb4ba239b1r29yy8zdh2fcnk7w")))

(define-public crate-Lattice-0.3.6 (c (n "Lattice") (v "0.3.6") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1msvmslc2zgrg94j3xz5cvznaspw4pmqx8njlh5gy39wds4r7lb0")))

(define-public crate-Lattice-0.3.7 (c (n "Lattice") (v "0.3.7") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1zi3s0i0kap35ll64fakshck6qxl2gslxi6axy8jdqsgarj2ss8f")))

(define-public crate-Lattice-0.3.9 (c (n "Lattice") (v "0.3.9") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0xqxzzr58yf1jinksiqiqrm4fhw2ljahl7h39gqd616xfy8dfif9")))

(define-public crate-Lattice-0.3.10 (c (n "Lattice") (v "0.3.10") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0kmn89ci96v4d9hgyprxryh6w889fi92f8a8jwnqyg95kspfjsdk")))

(define-public crate-Lattice-0.3.11 (c (n "Lattice") (v "0.3.11") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0xajwmi6pxli62wwk0r1y7ilvy5xwj1nqk40ig7ldlnh79zjzxnv")))

(define-public crate-Lattice-0.3.12 (c (n "Lattice") (v "0.3.12") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0hjv6x164ll1gvj6q45jiq4mqbxjg5c61s1kqnpflj2xsnvhvrw5")))

(define-public crate-Lattice-0.3.13 (c (n "Lattice") (v "0.3.13") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "15xh6fk6mapxlyfn2l0hd2l9ycxmlnda5nqj1xi8fhr94kgcd1x9")))

(define-public crate-Lattice-0.3.14 (c (n "Lattice") (v "0.3.14") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "11gbqhi71m6qcfsw1g9kz83ypzy7spqhcnk3qbashppxv7k0plda")))

(define-public crate-Lattice-0.3.15 (c (n "Lattice") (v "0.3.15") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "16x2qiqclh51w8736yyp3njch4zghkdgix56jq5v9ljg3k57zvm8")))

(define-public crate-Lattice-0.3.16 (c (n "Lattice") (v "0.3.16") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1ifn5hk5sa7zx6ndbyd7x8p1nbb76pgzs0h2dkn2a6ikp7qc0cjk")))

(define-public crate-Lattice-0.3.17 (c (n "Lattice") (v "0.3.17") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1mw9nf48yp9avq85ah52rd4gk13v10jhs3n78mlql2nvfaf0an7c")))

(define-public crate-Lattice-0.3.18 (c (n "Lattice") (v "0.3.18") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0rmi215m0p45qm8pp1pv3q62i623pg6ysdwcw3y228yx8wnwd28x")))

(define-public crate-Lattice-0.4.0 (c (n "Lattice") (v "0.4.0") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1vn4ny89h08is19j0x61b2kbj10mxc040h4x544phq39vzjznnam")))

(define-public crate-Lattice-0.4.1 (c (n "Lattice") (v "0.4.1") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "05b580nqcrrcrmphg56mkiw8s73ydwrjsgmc9agzqsisd77h5hc4")))

(define-public crate-Lattice-0.4.2 (c (n "Lattice") (v "0.4.2") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1pvh7g45ap5hignpq1y1wmi6yxdlhgklzbfivmri2az6pmibksg8")))

(define-public crate-Lattice-0.4.3 (c (n "Lattice") (v "0.4.3") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0c9rjwpzz7wjf3z5ljx0pk5jfs7c78n4prxhjarng091gixlfgkp")))

(define-public crate-Lattice-0.4.4 (c (n "Lattice") (v "0.4.4") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.13") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0i0id43z7y3pafdd1v1fzyxpi91vhgvl81fwdl10bjkagjn3zh8f")))

(define-public crate-Lattice-0.4.5 (c (n "Lattice") (v "0.4.5") (d (list (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.14") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rusttype") (r "^0.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.30") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0kalpqqi7jq7wyrz4qb9lap8byghjp33vvmg2pg624iazzd3y6as")))

(define-public crate-Lattice-0.5.0 (c (n "Lattice") (v "0.5.0") (h "1vw074gh9akzzcd04jg3a8aq55i7d7dndja2dd81jvxc8klsls2z")))

