(define-module (crates-io la tt lattescript) #:use-module (crates-io))

(define-public crate-lattescript-0.1.0 (c (n "lattescript") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0iz0ly0i4s4g746j6lyza5j4km0a5vxj7k1xj78pl0b53rb86alc")))

(define-public crate-lattescript-0.1.1 (c (n "lattescript") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1rmhlxhfaph9y0j1sh94sw524pblxlmniyj5fc9pdcgzca00mxvx")))

(define-public crate-lattescript-0.1.2 (c (n "lattescript") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1bs1y8vil7z6yic2zhcxmsvy87qg3z1075p1hkg2ks0z1a7nnqw3")))

