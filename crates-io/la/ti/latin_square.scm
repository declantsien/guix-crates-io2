(define-module (crates-io la ti latin_square) #:use-module (crates-io))

(define-public crate-latin_square-0.1.0 (c (n "latin_square") (v "0.1.0") (d (list (d (n "moon_math") (r "^0.1.0") (d #t) (k 0)) (d (n "moon_stats") (r "^0.1.0") (d #t) (k 0)))) (h "0sczwm31v5sqrldx7l05qfdygzbsxd79hwi6m3wlw06h6iz8kjhv") (y #t)))

