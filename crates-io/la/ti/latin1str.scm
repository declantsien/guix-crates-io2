(define-module (crates-io la ti latin1str) #:use-module (crates-io))

(define-public crate-latin1str-0.1.0 (c (n "latin1str") (v "0.1.0") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)))) (h "02x6cx0f0dzggvbmlfr4sqzqis4rrsrwpkxyhisvmq4hz1hhqxqq")))

(define-public crate-latin1str-0.1.1 (c (n "latin1str") (v "0.1.1") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "011qxp860xnrsmdyblnj4ckisc8857a4b75ihzq2lc3my9m9k9qb") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-latin1str-0.1.2 (c (n "latin1str") (v "0.1.2") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02z1rgp2sa2ygxvjsplslnj2j54yfdfidrcr2vgxa853xi4np4g5") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-latin1str-0.1.3 (c (n "latin1str") (v "0.1.3") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0mzhn5n95v82idxsw8qz8pvmbqmrjz8wnfkz72gsjdrpy76vvn3w") (s 2) (e (quote (("serde" "dep:serde"))))))

