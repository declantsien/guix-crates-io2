(define-module (crates-io la ti latin_squares) #:use-module (crates-io))

(define-public crate-latin_squares-0.1.0 (c (n "latin_squares") (v "0.1.0") (d (list (d (n "moon_math") (r "^0.1.0") (d #t) (k 0)) (d (n "moon_stats") (r "^0.1.0") (d #t) (k 0)))) (h "007h59qv58f93xdrs3ikl5dwx6l5ygqxas31b74i5x5kg5x3lk1f")))

(define-public crate-latin_squares-0.1.1 (c (n "latin_squares") (v "0.1.1") (d (list (d (n "moon_math") (r "^0.1.0") (d #t) (k 0)) (d (n "moon_stats") (r "^0.1.0") (d #t) (k 0)))) (h "08sk80gyvas8hk3854wd1nlfaqakfc9pypdrmzgq2ykm2yl017sy")))

(define-public crate-latin_squares-0.1.2 (c (n "latin_squares") (v "0.1.2") (d (list (d (n "moon_math") (r "^0.1.3") (d #t) (k 0)) (d (n "moon_stats") (r "^0.1.2") (d #t) (k 0)))) (h "0yqsv77d1xjv8y4mvd6pjjz7618csv88qv5dbdkk6hmlq20c0f0i")))

