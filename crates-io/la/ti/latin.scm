(define-module (crates-io la ti latin) #:use-module (crates-io))

(define-public crate-latin-0.1.0 (c (n "latin") (v "0.1.0") (h "035b7c4bsnhxm4kssd5b38hz16iyzws10zzhwnnd7n5d8s16sk3w")))

(define-public crate-latin-0.1.1 (c (n "latin") (v "0.1.1") (h "1w4j3h7b2myr5zz4wwma2z0y1qbn4ln63xdz635wbrgj54v11f09")))

(define-public crate-latin-0.1.2 (c (n "latin") (v "0.1.2") (h "0b2mjq9nrzyzn3hx2sq0z75f2pbl3ga8qcfyxkj2zvf0rn21nsnm")))

(define-public crate-latin-0.1.3 (c (n "latin") (v "0.1.3") (h "1mphginfs0h43lqg319c2bk6717aqpbf95kyngpg856bc5l6za6y")))

(define-public crate-latin-0.1.4 (c (n "latin") (v "0.1.4") (h "0kanwhpw55if7q1bc20pxxx34c5h551p9py22q7bgbvibwgh4k7h")))

(define-public crate-latin-0.1.5 (c (n "latin") (v "0.1.5") (h "0mp737041ndym73arw42pm235afb186q6j1n0j4xlx1gyfc1hsxx")))

(define-public crate-latin-0.1.6 (c (n "latin") (v "0.1.6") (h "0fyja08jhmm5bycbb4di8h4xavl5kmyfgb42rzzd1pw6vryps1x5")))

(define-public crate-latin-0.1.7 (c (n "latin") (v "0.1.7") (h "0psrs4rklhywj4rjsx9pbry4rpphpafx9i5p7sjm18acg6vrv7jx")))

