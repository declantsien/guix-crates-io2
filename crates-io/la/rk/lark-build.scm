(define-module (crates-io la rk lark-build) #:use-module (crates-io))

(define-public crate-lark-build-0.1.0 (c (n "lark-build") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "lark-debug-with") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-entity") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-error") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-hir") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-intern") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-parser") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-query-system") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-string") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-ty") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-type-check") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^2.0") (d #t) (k 0)))) (h "115mj2ga4afxpyxbxhb08ywfdx8fqkyqrijnsa3warbqph98mmil")))

