(define-module (crates-io la rk lark-debug-derive) #:use-module (crates-io))

(define-public crate-lark-debug-derive-0.1.0 (c (n "lark-debug-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "01g2w8g8wjmyibyvdbwnw2j4lslndmmn1cay5hkvjxp534wkh1bf")))

