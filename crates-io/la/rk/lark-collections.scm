(define-module (crates-io la rk lark-collections) #:use-module (crates-io))

(define-public crate-lark-collections-0.1.0 (c (n "lark-collections") (v "0.1.0") (d (list (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "lark-debug-with") (r "^0.1.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.0") (d #t) (k 0)))) (h "1y0al1j469lpr6yvxcdmvhspcq4x41w1jzh3nn0grb2fjv6rv4fk")))

