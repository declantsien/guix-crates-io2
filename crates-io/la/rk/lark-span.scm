(define-module (crates-io la rk lark-span) #:use-module (crates-io))

(define-public crate-lark-span-0.1.0 (c (n "lark-span") (v "0.1.0") (d (list (d (n "derive-new") (r "^0.5.6") (d #t) (k 0)) (d (n "language-reporting") (r "^0.3.0") (d #t) (k 0)) (d (n "languageserver-types") (r "^0.53") (d #t) (k 0)) (d (n "lark-debug-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-debug-with") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-intern") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-string") (r "^0.1.0") (d #t) (k 0)))) (h "06psb3pv7d1bj9hkyldy33vfagfc3bam073lragd5s67gak7w2a5")))

