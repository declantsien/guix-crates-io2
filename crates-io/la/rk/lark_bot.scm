(define-module (crates-io la rk lark_bot) #:use-module (crates-io))

(define-public crate-lark_bot-0.0.1 (c (n "lark_bot") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ijsy9qx4ayvrmqhra40zxm5m9ajcdkxis0ppi27v3j705rm63nf")))

(define-public crate-lark_bot-0.0.2 (c (n "lark_bot") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.11.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0wpnhaqxgvd5skl8k25dpv592nw9rv25m1jbpswnq37fzag0s5xr")))

