(define-module (crates-io la rk lark-language-server) #:use-module (crates-io))

(define-public crate-lark-language-server-0.1.0 (c (n "lark-language-server") (v "0.1.0") (d (list (d (n "languageserver-types") (r "^0.53") (d #t) (k 0)) (d (n "lark-actor") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)))) (h "1x7l2w4v0j0yxpcgjk9x1vgh88bnbh3m0yxs74ndizif0ha00mj1")))

