(define-module (crates-io la rk lark-intern) #:use-module (crates-io))

(define-public crate-lark-intern-0.1.0 (c (n "lark-intern") (v "0.1.0") (d (list (d (n "lark-collections") (r "^0.1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)))) (h "0rsgixs64df4i5dm9dvwms3x5cljjzpyxmywk3wcy9wfjcjbikz6")))

