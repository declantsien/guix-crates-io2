(define-module (crates-io la rk lark-entity) #:use-module (crates-io))

(define-public crate-lark-entity-0.1.0 (c (n "lark-entity") (v "0.1.0") (d (list (d (n "lark-collections") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-debug-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-debug-with") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-error") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-intern") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-span") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-string") (r "^0.1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)))) (h "1gfyzgr7z0ryr86d8iv48wfa7pfd156js8la3chq4i5j2cq6s231")))

