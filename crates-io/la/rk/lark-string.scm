(define-module (crates-io la rk lark-string) #:use-module (crates-io))

(define-public crate-lark-string-0.1.0 (c (n "lark-string") (v "0.1.0") (d (list (d (n "lark-collections") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-debug-with") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-intern") (r "^0.1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)))) (h "0wlalwjcja0d89i9qiz66mbpan5f573jb978g5ff6lghj2p4ag31")))

