(define-module (crates-io la rk lark-ty) #:use-module (crates-io))

(define-public crate-lark-ty-0.1.0 (c (n "lark-ty") (v "0.1.0") (d (list (d (n "derive-new") (r "^0.5.6") (d #t) (k 0)) (d (n "lark-collections") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-debug-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-debug-with") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-entity") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-error") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-intern") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-string") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-unify") (r "^0.1.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)))) (h "0q1djrkrvwrsj0w9x8wx3vl4xlyw431iznc98hrnx2yi5pbd7xp5")))

