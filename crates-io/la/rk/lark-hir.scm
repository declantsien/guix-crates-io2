(define-module (crates-io la rk lark-hir) #:use-module (crates-io))

(define-public crate-lark-hir-0.1.0 (c (n "lark-hir") (v "0.1.0") (d (list (d (n "lark-collections") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-debug-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-debug-with") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-entity") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-error") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-intern") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-span") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-string") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-ty") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "salsa") (r "^0.9") (d #t) (k 0)))) (h "1wfl739wzrp5398cmsijw8mws8w12ig4svxqbs52vvn1xl16a1nc")))

