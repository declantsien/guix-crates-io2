(define-module (crates-io la rk lark-error) #:use-module (crates-io))

(define-public crate-lark-error-0.1.0 (c (n "lark-error") (v "0.1.0") (d (list (d (n "lark-collections") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-debug-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-debug-with") (r "^0.1.0") (d #t) (k 0)) (d (n "lark-span") (r "^0.1.0") (d #t) (k 0)))) (h "0whppjyzqpsz3rml8rghd54xgqwd9xl6x5rwsrcqly6n8rfgj0rz")))

