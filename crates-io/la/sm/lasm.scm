(define-module (crates-io la sm lasm) #:use-module (crates-io))

(define-public crate-lasm-0.1.0 (c (n "lasm") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "comment") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)))) (h "171yqb9xjf49qz4fvzya3ci00nfi71z0zsp225r8hxmd69pwjj2k")))

