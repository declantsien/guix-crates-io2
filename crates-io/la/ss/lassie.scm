(define-module (crates-io la ss lassie) #:use-module (crates-io))

(define-public crate-lassie-0.1.0 (c (n "lassie") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "ureq") (r "^2.6.2") (d #t) (k 2)))) (h "0as03v0zd9z5a9mgrvqljcjczhv9fvd1h4l47xwm756mdznj2gg9")))

(define-public crate-lassie-0.1.1 (c (n "lassie") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "ureq") (r "^2.6.2") (d #t) (k 2)))) (h "0yw0naqjmnhlb15fll4cywsk8k4pxj323gjy1jkgay8n4l6wi4qi")))

(define-public crate-lassie-0.2.0 (c (n "lassie") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "ureq") (r "^2.6.2") (d #t) (k 2)))) (h "16jqbmiivqd3fly1w6vbkr4vaxv8z2h7jbjvzjchzsbbrisan360")))

(define-public crate-lassie-0.3.0 (c (n "lassie") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "ureq") (r "^2.6.2") (d #t) (k 2)))) (h "1zsp4vh87nrn16pkf3vi5plb8d61j88c8af7dh4j96dl25s529jp")))

(define-public crate-lassie-0.3.1 (c (n "lassie") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "ureq") (r "^2.6.2") (d #t) (k 2)))) (h "0wx8nyn55vdwhjyj10n3vjx4r85w4sydv3zhbqv2aqbnsk19825v")))

(define-public crate-lassie-0.3.2 (c (n "lassie") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "ureq") (r "^2.6.2") (d #t) (k 2)))) (h "14wrbkplfpv6y3rj9fm95f2mwdqkmmlhqrf33cdh21pw60rr7b7w")))

(define-public crate-lassie-0.4.0 (c (n "lassie") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "ureq") (r "^2.7.1") (d #t) (k 2)))) (h "14a89dsba15x6015hqz1lq5nrwbf8w2vy81d2p8x105yy79phrwr")))

(define-public crate-lassie-0.5.0 (c (n "lassie") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "ureq") (r "^2.7.1") (d #t) (k 2)))) (h "1vp80zf4rvvxiwmhc8shzjvmfjqi9ldzl7d8x38w820giy3kj205")))

(define-public crate-lassie-0.5.1 (c (n "lassie") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "ureq") (r "^2.7.1") (d #t) (k 2)))) (h "1xi8ki8h4jjj424ql3i9najlf2d6c6wrns5zvp0lhi8580qlnp90")))

(define-public crate-lassie-0.6.0 (c (n "lassie") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "ureq") (r "^2.7.1") (d #t) (k 2)))) (h "0yc1pvxhdb8z9qlr9b2nc7p9g3avcx67xxw2k808clmgrcqyarv2")))

(define-public crate-lassie-0.7.0 (c (n "lassie") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "ureq") (r "^2.7.1") (d #t) (k 2)))) (h "0j0d7qk42m4y102pj3ckn94cbyb452vh1lw5mfq9g31vf8wpjl2f")))

(define-public crate-lassie-0.8.0 (c (n "lassie") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "ureq") (r "^2.9.1") (d #t) (k 2)))) (h "16vs676jv69s6asdczfggf33k0z3bq51sfxp1wpik4b0hbm2cxbg")))

(define-public crate-lassie-0.9.0 (c (n "lassie") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.11.1") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "ureq") (r "^2.9.4") (d #t) (k 2)))) (h "0jcmc1vlr1zis69asaqhn3nzxqqxnhi02nr7zlbg5a5dncagam8h")))

