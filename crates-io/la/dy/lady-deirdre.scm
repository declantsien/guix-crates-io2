(define-module (crates-io la dy lady-deirdre) #:use-module (crates-io))

(define-public crate-lady-deirdre-1.0.0 (c (n "lady-deirdre") (v "1.0.0") (d (list (d (n "lady-deirdre-derive") (r "^1.0") (k 0)))) (h "0ryg2xcxzclkal1jgbdhv5bsg5s7z42jma8qqvnczaxks1c4ga33") (f (quote (("std" "lady-deirdre-derive/std") ("default" "std")))) (r "1.65")))

(define-public crate-lady-deirdre-1.0.1 (c (n "lady-deirdre") (v "1.0.1") (d (list (d (n "lady-deirdre-derive") (r "^1.0") (k 0)))) (h "1hypvc13ca5bhjxprxydv3szjs0piymg5s526x93vvl0n9wr5r4c") (f (quote (("std" "lady-deirdre-derive/std") ("default" "std")))) (r "1.65")))

(define-public crate-lady-deirdre-1.0.2 (c (n "lady-deirdre") (v "1.0.2") (d (list (d (n "lady-deirdre-derive") (r "^1.0") (k 0)))) (h "1axkdnw5v80hdpg89wxqycgamxdp35a428r3cy5xk6ppy89jns1n") (f (quote (("std" "lady-deirdre-derive/std") ("default" "std")))) (r "1.65")))

(define-public crate-lady-deirdre-1.0.3 (c (n "lady-deirdre") (v "1.0.3") (d (list (d (n "lady-deirdre-derive") (r "^1.0") (k 0)))) (h "1b9ffy2vxrf30zc3yjm09gb8wp0agyk9vb9bkhj4y5d8p7zzms3r") (f (quote (("std" "lady-deirdre-derive/std") ("default" "std")))) (r "1.65")))

(define-public crate-lady-deirdre-1.0.4 (c (n "lady-deirdre") (v "1.0.4") (d (list (d (n "lady-deirdre-derive") (r "^1.0") (k 0)))) (h "00ccxlxc0a4mk4yf5hp7mdl3cxw3pxaxzv06i8qssvs874wslq2b") (f (quote (("std" "lady-deirdre-derive/std") ("default" "std")))) (r "1.65")))

(define-public crate-lady-deirdre-1.0.5 (c (n "lady-deirdre") (v "1.0.5") (d (list (d (n "lady-deirdre-derive") (r "^1.0") (k 0)))) (h "0502rkiwg1dihxw5gj6550zysbq90j47azhjdimf6ig0pyldwk4k") (f (quote (("std" "lady-deirdre-derive/std") ("default" "std")))) (r "1.65")))

