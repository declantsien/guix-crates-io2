(define-module (crates-io la dy lady-deirdre-derive) #:use-module (crates-io))

(define-public crate-lady-deirdre-derive-1.0.0 (c (n "lady-deirdre-derive") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("default" "full" "extra-traits"))) (d #t) (k 0)))) (h "1w28fqq4607lfp2gkm0w8lzv5jnx9p9g07hwg5gd2b8xkzzfmdpz") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-lady-deirdre-derive-1.0.1 (c (n "lady-deirdre-derive") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("default" "full" "extra-traits"))) (d #t) (k 0)))) (h "1fh9shrjy28j02jkv17f5sm3i00jqihvagmr2yq59zzqvyqqsz41") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-lady-deirdre-derive-1.0.2 (c (n "lady-deirdre-derive") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("default" "full" "extra-traits"))) (d #t) (k 0)))) (h "1vr7qawyzzb0pgmk02z3p2i3ljy08f2429r6lh7xzw6mvvjhiywr") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-lady-deirdre-derive-1.0.3 (c (n "lady-deirdre-derive") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("default" "full" "extra-traits"))) (d #t) (k 0)))) (h "18fkwlciwjbs9ybvlyvgyrcwhwxrsjgjj9c64j5vrllpsyb8v9f4") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-lady-deirdre-derive-1.0.4 (c (n "lady-deirdre-derive") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("default" "full" "extra-traits"))) (d #t) (k 0)))) (h "0fc5z1qfzc04pvpr1m46vw7lksrp9z6acgrsxblxr2k7i0j4nvsf") (f (quote (("std") ("default" "std")))) (r "1.65")))

(define-public crate-lady-deirdre-derive-1.0.5 (c (n "lady-deirdre-derive") (v "1.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("default" "full" "extra-traits"))) (d #t) (k 0)))) (h "1fpsnbvq1xwjf4dhy1ykajjxc6cz0inhp3cn5xr3766dls2mg2f1") (f (quote (("std") ("default" "std")))) (r "1.65")))

