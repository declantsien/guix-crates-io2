(define-module (crates-io la -r la-rete) #:use-module (crates-io))

(define-public crate-la-rete-0.1.0 (c (n "la-rete") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "json-utils") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "157w976hjb01mqwvsi0khf17ry90chxgaxvkqnk12ac2pqpfmi3q")))

(define-public crate-la-rete-0.1.1 (c (n "la-rete") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "json-utils") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "031xzqyxl6z72fxwp854ln32lbzdrgd5rz1ns8xws4s3z5fk5hk0")))

(define-public crate-la-rete-0.1.2 (c (n "la-rete") (v "0.1.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "json-utils") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04lbzl84r5kljq83fj5xg6h4zzvx2vriymrh6jd6xcjkp0qwdg1q")))

(define-public crate-la-rete-0.1.3 (c (n "la-rete") (v "0.1.3") (d (list (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "json-utils") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0za341ahj8i7zil4dr6913m24vs1w17c1sf7mam2dr3ay2hnrljz")))

