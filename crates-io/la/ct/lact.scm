(define-module (crates-io la ct lact) #:use-module (crates-io))

(define-public crate-lact-0.1.0 (c (n "lact") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lact_method") (r "^0.1.0") (d #t) (k 0)) (d (n "lact_status") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "167zj8f2ysasc1vavqawzy4bf1m7nn52y69rrlxz6lrx4y0kra86")))

(define-public crate-lact-0.1.1 (c (n "lact") (v "0.1.1") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lact_method") (r "^0.1.0") (d #t) (k 0)) (d (n "lact_status") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kni44xklzm66gdisyrfgxr2amx6kcfggjys532zyym793j9n4im") (y #t)))

(define-public crate-lact-0.1.2 (c (n "lact") (v "0.1.2") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.60") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lact_method") (r "^0.1.0") (d #t) (k 0)) (d (n "lact_status") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0fh4wcha93crrhjmjwi0bn3dvdzsq0rciarjydb69jhwi7kzz3lk")))

