(define-module (crates-io la ri lariv) #:use-module (crates-io))

(define-public crate-lariv-0.1.0 (c (n "lariv") (v "0.1.0") (d (list (d (n "aliasable") (r "^0.1.3") (f (quote ("aliasable_deref_trait" "traits" "stable_deref_trait"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)))) (h "1jiyskgyjrhbkyahggwi7k9wnz79m2q094pagc96zj0brmr4jrc3") (y #t)))

(define-public crate-lariv-0.1.1 (c (n "lariv") (v "0.1.1") (d (list (d (n "aliasable") (r "^0.1.3") (f (quote ("aliasable_deref_trait" "traits" "stable_deref_trait"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)))) (h "1ky0jpka2kj4pcwbq739p8jy4p65k51754rdwflzwy1010lghxad") (y #t)))

(define-public crate-lariv-0.1.2 (c (n "lariv") (v "0.1.2") (d (list (d (n "aliasable") (r "^0.1.3") (f (quote ("aliasable_deref_trait" "traits" "stable_deref_trait"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)))) (h "05gph461y3y4lqbn4p996crl0n3jqqskgz8gz424s9j01h2ja735")))

(define-public crate-lariv-0.1.3 (c (n "lariv") (v "0.1.3") (d (list (d (n "aliasable") (r "^0.1.3") (f (quote ("aliasable_deref_trait" "traits" "stable_deref_trait"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)))) (h "1i5ff7v852wv712r6g5dv6x8n6nyjcqqy1s12hdri5638naq4x48")))

(define-public crate-lariv-0.2.0 (c (n "lariv") (v "0.2.0") (d (list (d (n "aliasable") (r "^0.1.3") (f (quote ("aliasable_deref_trait" "traits" "stable_deref_trait"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)))) (h "0c30y3w2f1n4pbl6rmymwpldg6sv901sjgld7njm0gvf08a5i0sy")))

(define-public crate-lariv-0.2.1 (c (n "lariv") (v "0.2.1") (d (list (d (n "aliasable") (r "^0.1.3") (f (quote ("aliasable_deref_trait" "traits" "stable_deref_trait"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)))) (h "0c8ab2xwy7myp2jdkhmsazhfn10vmz4b2bigcqqnc3q06sal0ym2")))

(define-public crate-lariv-0.2.2 (c (n "lariv") (v "0.2.2") (d (list (d (n "aliasable") (r "^0.1.3") (f (quote ("aliasable_deref_trait" "traits" "stable_deref_trait"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)))) (h "0k0fqly7lg67203anbdc9s5fyl9lbdcc0bm0sazckzja21lj6bsb") (y #t)))

(define-public crate-lariv-0.2.3 (c (n "lariv") (v "0.2.3") (d (list (d (n "aliasable") (r "^0.1.3") (f (quote ("aliasable_deref_trait" "traits" "stable_deref_trait"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)))) (h "0b1s6k8xlm0938b5di3hmvcppl4jr6wrvmh21k68y69pslwvqfkb")))

(define-public crate-lariv-0.2.4 (c (n "lariv") (v "0.2.4") (d (list (d (n "aliasable") (r "^0.1.3") (f (quote ("aliasable_deref_trait" "traits" "stable_deref_trait"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)))) (h "06fad5msahg9z3in4zgkws3hdbfxy87c5mca48ky25w2agykd9gp")))

(define-public crate-lariv-0.3.0 (c (n "lariv") (v "0.3.0") (d (list (d (n "aliasable") (r "^0.1.3") (f (quote ("aliasable_deref_trait" "traits" "stable_deref_trait"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 2)))) (h "0w74khdswh2jq6mj4vxlfbk03ks5598sbgd996ndy8x2ydyrk5x2")))

(define-public crate-lariv-0.3.1 (c (n "lariv") (v "0.3.1") (d (list (d (n "aliasable") (r "^0.1.3") (f (quote ("aliasable_deref_trait" "traits" "stable_deref_trait"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 2)))) (h "01f7mcmrnc62h859x7n94lc9dv4mm439zi5n7abf1dhq9wfy4lz9")))

(define-public crate-lariv-0.3.2 (c (n "lariv") (v "0.3.2") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "dashmap") (r "^5.4.0") (d #t) (k 2)))) (h "1sb3gglxqr7127yhjx3l2dv8k77kiacvbanqm3sch0vb3x87ra98")))

