(define-module (crates-io q3 to q3tool) #:use-module (crates-io))

(define-public crate-q3tool-0.1.0 (c (n "q3tool") (v "0.1.0") (h "045d1zvz8gv2jck4q090ysnm9h7dkfrz7agzmi8v9fq804zfkcjx")))

(define-public crate-q3tool-0.1.1 (c (n "q3tool") (v "0.1.1") (h "0b81d9s4p474ygr1akirxf02nlzqm61cl31ycjs20mcc687323cx")))

(define-public crate-q3tool-0.1.2 (c (n "q3tool") (v "0.1.2") (h "14lm1x89q85ybgq4v5n758sksg431bdgfa30d3dwj7x4x8hffnxi")))

(define-public crate-q3tool-0.1.3 (c (n "q3tool") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1l3miv5ml1851bw2jw145f55lrdiq57lzm24h91xyiay23c3qanf")))

(define-public crate-q3tool-0.1.4 (c (n "q3tool") (v "0.1.4") (d (list (d (n "byte-strings") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "110p5dz1byf93wd5sj4vy78svjds1d3zjwdhm2jiy4jc5jcp8m38")))

(define-public crate-q3tool-0.2.0 (c (n "q3tool") (v "0.2.0") (h "1hb35d2kwv6p7qcvvn2p89070crbki0wb74spkxnhlxipwqb249z")))

(define-public crate-q3tool-0.3.0 (c (n "q3tool") (v "0.3.0") (d (list (d (n "format-bytes") (r "^0.3.0") (d #t) (k 0)))) (h "1h7snysfgri4r8kq6innbxhyyyknlgirmapd4k1b1zvf15873qr4")))

(define-public crate-q3tool-0.3.1 (c (n "q3tool") (v "0.3.1") (d (list (d (n "format-bytes") (r "^0.3.0") (d #t) (k 0)))) (h "0y3p5wmrszs977nrx0al7cd0kv40la1awda7siyic1vca1d64yrm")))

(define-public crate-q3tool-0.3.2 (c (n "q3tool") (v "0.3.2") (d (list (d (n "format-bytes") (r "^0.3.0") (d #t) (k 0)))) (h "04qwq9da373dp04ksn6g50csdmfzz5d6xf5f0718pb4z1cdypf8y")))

(define-public crate-q3tool-0.3.3 (c (n "q3tool") (v "0.3.3") (d (list (d (n "format-bytes") (r "^0.3.0") (d #t) (k 0)))) (h "14an9ik326afzyr3pn4am7kc6k91kb094gx5abpx4zbwwhwwcv6c")))

