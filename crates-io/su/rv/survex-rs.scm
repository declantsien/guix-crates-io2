(define-module (crates-io su rv survex-rs) #:use-module (crates-io))

(define-public crate-survex-rs-0.1.2 (c (n "survex-rs") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)))) (h "1xdlq0xg1xrcd4b9fg08d8fz1ni6sdnwyjv2saizqpz8nb0k7n0q")))

(define-public crate-survex-rs-0.1.3 (c (n "survex-rs") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)))) (h "0vd2kgaif6zi3k6nnd1pza7q8nx18g7y75zi1p5qmvn6wmar3f07")))

(define-public crate-survex-rs-0.1.4 (c (n "survex-rs") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)))) (h "1a7kr7xs0vs1r0wfvwrximikqlvrz5d1hc9arpxdcj7g4wj6lxbv")))

(define-public crate-survex-rs-0.1.5 (c (n "survex-rs") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "0nf0l4wbgrrm6ggwl85xzyia9ppc76xhwzg7xs0ybl64gmfhxr07")))

(define-public crate-survex-rs-0.1.6 (c (n "survex-rs") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "153ryz09i9cvqq5k6kfzlzhmd03whih8j803x1c65nvj2gynzcjh")))

(define-public crate-survex-rs-0.1.7 (c (n "survex-rs") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4" "fast-rng"))) (d #t) (k 0)))) (h "107rw3w0823cd93cdswbj3d5vbbx1dr38yil4saxir4dhvr9z436")))

