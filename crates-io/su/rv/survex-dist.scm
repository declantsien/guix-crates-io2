(define-module (crates-io su rv survex-dist) #:use-module (crates-io))

(define-public crate-survex-dist-0.3.0 (c (n "survex-dist") (v "0.3.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "clap") (r "^4.3.19") (f (quote ("derive" "cargo" "wrap_help"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.3") (d #t) (k 0)) (d (n "predicates") (r "^3.0") (d #t) (k 2)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "survex-rs") (r "^0.1.7") (d #t) (k 0)))) (h "07yx6yj9570gv0p4m6xhy8c0q797abqrbp3kbhg0k78yrl277qj8")))

