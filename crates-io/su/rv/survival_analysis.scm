(define-module (crates-io su rv survival_analysis) #:use-module (crates-io))

(define-public crate-survival_analysis-0.1.0 (c (n "survival_analysis") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argmin") (r "^0.3") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "ndarray") (r "^0.13") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0g8s3xwwx57wl6yx82hzzvachlm5mjx7yj90lgqmra7s3960rn6l")))

