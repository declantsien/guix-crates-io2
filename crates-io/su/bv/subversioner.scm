(define-module (crates-io su bv subversioner) #:use-module (crates-io))

(define-public crate-subversioner-0.9.0-rc13 (c (n "subversioner") (v "0.9.0-rc13") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1h5b2x2i9v9ch1n60pjm4ilif3d38qrgna1js2pzxyrxgp570xd9")))

(define-public crate-subversioner-0.9.0-rc14 (c (n "subversioner") (v "0.9.0-rc14") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0kjnjhk1c5m9vwj8sjmbpaxzmdcv6hhm9c9q00cqvkf593h44cx2")))

(define-public crate-subversioner-0.9.0-rc15 (c (n "subversioner") (v "0.9.0-rc15") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "17hdsqpad85yggv2kaaw1diwg3azbwpywlwj84rkal2j5k6qbrvc")))

(define-public crate-subversioner-0.9.0-rc16 (c (n "subversioner") (v "0.9.0-rc16") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "05g2yy81900ycjpyjv27p9l227m9lfmadg86hhygn8vm28p74in5")))

(define-public crate-subversioner-0.9.0-rc17 (c (n "subversioner") (v "0.9.0-rc17") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0jj569livlfl0svdpnyrpivcl9wk9qp5z126r8h17shx3y3wpyqf")))

(define-public crate-subversioner-0.9.0-rc18 (c (n "subversioner") (v "0.9.0-rc18") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0z7qmx11d479qd39klp3am3gzyhclpf5sdfcvrrisi59f0xw97f3")))

(define-public crate-subversioner-0.9.0-rc19 (c (n "subversioner") (v "0.9.0-rc19") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "18fr4scw4fcgir35wzgh71jxpsvk05w9qcbpl8kbqmx0iii3kq19")))

(define-public crate-subversioner-0.9.0-rc20 (c (n "subversioner") (v "0.9.0-rc20") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "064bd18rw1fnjykdmng4ghh1dlsr724h52a330yrfvpnlk9rhz7d")))

(define-public crate-subversioner-0.9.0 (c (n "subversioner") (v "0.9.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1r3xzch6ln33s3mjm9sz2c09bba5qyhj4xlxy1100kl6la4fvw1y")))

(define-public crate-subversioner-0.9.1 (c (n "subversioner") (v "0.9.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0vkrll8cfajawq0qm58f1ag7jsd49nxf4nsaddrgpwkixhgky27p")))

(define-public crate-subversioner-0.9.2 (c (n "subversioner") (v "0.9.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "07dczlmqpnfpalkl396ggq9hqhlajvbfwjl0zjv6f4bd8v5j9a98")))

(define-public crate-subversioner-0.10.0 (c (n "subversioner") (v "0.10.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0m7l60llamvjlv5cdk3cdrqpmdp0jwjnylqiq9a036lk87rh248f")))

(define-public crate-subversioner-0.10.1 (c (n "subversioner") (v "0.10.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16p1x3b6fwjhjp6dy13scbm85i0qyqqifj2xwrl55x7zd030m199")))

(define-public crate-subversioner-0.11.0 (c (n "subversioner") (v "0.11.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0bmr3jcd334hq4qc4vjnm6bc8c5yjvyasvsvr3ill2ih2s20mhsq")))

