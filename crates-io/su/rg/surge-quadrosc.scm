(define-module (crates-io su rg surge-quadrosc) #:use-module (crates-io))

(define-public crate-surge-quadrosc-0.1.17-alpha.0 (c (n "surge-quadrosc") (v "0.1.17-alpha.0") (h "0xvfly3ahbzf6m7mk357kjkpvs0gfrai0lksgxsyvfn6lfws7bhh")))

(define-public crate-surge-quadrosc-0.1.20-alpha.0 (c (n "surge-quadrosc") (v "0.1.20-alpha.0") (h "12qxd8fw4bjzqgry84fclczf5ry2fx9gp2p5kk1qf4s6jbnss77m")))

(define-public crate-surge-quadrosc-0.1.23-alpha.0 (c (n "surge-quadrosc") (v "0.1.23-alpha.0") (h "1pyd9j1xjc0pygliygjdslabcgdjj29lmfglajn3ky46s5shg6d8")))

(define-public crate-surge-quadrosc-0.1.24-alpha.0 (c (n "surge-quadrosc") (v "0.1.24-alpha.0") (h "1x3hxp44wv0xdlwmkr39my27awxy8k5sgs711sc20gljspvsr1vd")))

(define-public crate-surge-quadrosc-0.1.26-alpha.0 (c (n "surge-quadrosc") (v "0.1.26-alpha.0") (h "1520br8wa3ki60ly494cic5imggnd9rgqz5ycwam092xx9dlxpdq")))

(define-public crate-surge-quadrosc-0.1.27-alpha.0 (c (n "surge-quadrosc") (v "0.1.27-alpha.0") (h "00qncjf7699wnx6c64vl2wcr46i7x7z18295fvi1lwirxk7z7xb1")))

(define-public crate-surge-quadrosc-0.1.28-alpha.0 (c (n "surge-quadrosc") (v "0.1.28-alpha.0") (h "1rxg26n9xxjc9cndw452dmqg54h9j606aq4zz6k0d72in3b59a4z")))

(define-public crate-surge-quadrosc-0.1.29-alpha.0 (c (n "surge-quadrosc") (v "0.1.29-alpha.0") (h "0qvxmr61rwvbcmdn3pbfnfpdj3msjp14kmqdw6z9s34v46kl2a65")))

(define-public crate-surge-quadrosc-0.1.30-alpha.0 (c (n "surge-quadrosc") (v "0.1.30-alpha.0") (h "1lp40nx2yvnmp5nv5i4bfhp9bjjcd342dmqqy21wqib1fllby9fm")))

(define-public crate-surge-quadrosc-0.1.31-alpha.0 (c (n "surge-quadrosc") (v "0.1.31-alpha.0") (h "02bj4f2c4c7ymgb0b68vsxkz4g214j51d2xbsy45hmp99jhdp88k")))

(define-public crate-surge-quadrosc-0.1.32-alpha.0 (c (n "surge-quadrosc") (v "0.1.32-alpha.0") (h "15y0099vrh9qvhink70x8lmpglyas07rk2vifxbm72vw6x0lp95x")))

(define-public crate-surge-quadrosc-0.1.33-alpha.0 (c (n "surge-quadrosc") (v "0.1.33-alpha.0") (h "11pxy6c8g1n3d71h06sama3pb01b1nmfd38jijf94g7n61gbqiqb")))

(define-public crate-surge-quadrosc-0.1.36-alpha.0 (c (n "surge-quadrosc") (v "0.1.36-alpha.0") (h "15i6kqyr4g6b69s2k4vx6f77la45f5h1gb453k39k111pypqq7da")))

(define-public crate-surge-quadrosc-0.1.39-alpha.0 (c (n "surge-quadrosc") (v "0.1.39-alpha.0") (h "0qjgn9xkg5g7vj4ycqw7aiawvq23qz7mr4sm8418xnyx35097f7p")))

(define-public crate-surge-quadrosc-0.1.40-alpha.0 (c (n "surge-quadrosc") (v "0.1.40-alpha.0") (h "10mghxrz2ilns5jp7zj66b545pvly27g8zz7qbhp3l5iwyipyw7j")))

(define-public crate-surge-quadrosc-0.1.42-alpha.0 (c (n "surge-quadrosc") (v "0.1.42-alpha.0") (h "15wpyf0ms4i5kgz2ikjh4nfchkg162ckfavc879wlfsqh0jgrq7s")))

(define-public crate-surge-quadrosc-0.2.0-alpha.0 (c (n "surge-quadrosc") (v "0.2.0-alpha.0") (h "01fpgqqkj9w6b9qqk5b081s51mkh5ccdj50ix5xfzslz4babbgdl")))

(define-public crate-surge-quadrosc-0.2.4-alpha.0 (c (n "surge-quadrosc") (v "0.2.4-alpha.0") (h "0fvj1qdqs91gghwmyajxbkrcj67y1bxyy91pv5fr8czqzcnkf5v9")))

(define-public crate-surge-quadrosc-0.2.5-alpha.0 (c (n "surge-quadrosc") (v "0.2.5-alpha.0") (d (list (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)))) (h "19m0ivyr6rgzcb8kjpq3kc0hhzpfx5gz6ywj0cz98mhha4q2ggzr")))

(define-public crate-surge-quadrosc-0.2.6-alpha.0 (c (n "surge-quadrosc") (v "0.2.6-alpha.0") (d (list (d (n "surge-imports") (r "^0.2.6-alpha.0") (d #t) (k 0)))) (h "05lif640y8iv6163dk28kgh9jqh91kb9vhfsf1k8za97j56aqy2z")))

(define-public crate-surge-quadrosc-0.2.9-alpha.0 (c (n "surge-quadrosc") (v "0.2.9-alpha.0") (d (list (d (n "surge-imports") (r "^0.2.9-alpha.0") (d #t) (k 0)))) (h "0idz1qvzpf7h7bp9qjfkhlq61k1ccrdhs0m5l4if3ks9kkbhsmlx")))

(define-public crate-surge-quadrosc-0.2.11-alpha.0 (c (n "surge-quadrosc") (v "0.2.11-alpha.0") (d (list (d (n "surge-imports") (r "^0.2.11-alpha.0") (d #t) (k 0)))) (h "1pqjxkw2vbgb99s8a8hnhk4x7klb9nhapk700c8s8n3ivsavcl06")))

(define-public crate-surge-quadrosc-0.2.12-alpha.0 (c (n "surge-quadrosc") (v "0.2.12-alpha.0") (d (list (d (n "surge-imports") (r "^0.2.12-alpha.0") (d #t) (k 0)))) (h "1xnzdv2ya6mcz603wp6ipbkclb43dgl9i8k28mgwdvyzpqrz3nwd")))

