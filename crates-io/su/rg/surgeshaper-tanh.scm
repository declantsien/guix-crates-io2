(define-module (crates-io su rg surgeshaper-tanh) #:use-module (crates-io))

(define-public crate-surgeshaper-tanh-0.1.40-alpha.0 (c (n "surgeshaper-tanh") (v "0.1.40-alpha.0") (d (list (d (n "surge-filter") (r "^0.1.40-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.40-alpha.0") (d #t) (k 0)))) (h "1lmrl5mrd5la2ngzr9wpn3brfcq9cfhv0y80dgkid2cq7b52fnhh")))

(define-public crate-surgeshaper-tanh-0.1.42-alpha.0 (c (n "surgeshaper-tanh") (v "0.1.42-alpha.0") (d (list (d (n "surge-filter") (r "^0.1.42-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.42-alpha.0") (d #t) (k 0)))) (h "1n9zx4hz0mkf70mhnhl66y4d29id1a824vbbrxadyihanzch9c10")))

(define-public crate-surgeshaper-tanh-0.2.0-alpha.0 (c (n "surgeshaper-tanh") (v "0.2.0-alpha.0") (d (list (d (n "surge-filter") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.0-alpha.0") (d #t) (k 0)))) (h "0q8qflapyj5pbgwdvfybw45aqmsa1nxfx7jz7i0j41kg94c6m7yy")))

(define-public crate-surgeshaper-tanh-0.2.1-alpha.0 (c (n "surgeshaper-tanh") (v "0.2.1-alpha.0") (d (list (d (n "surge-filter") (r "^0.2.1-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.1-alpha.0") (d #t) (k 0)))) (h "1cin1zgh4vvxb3f472cmaaib85lpkl77qwk6r7n2w2h79yv08m7n")))

(define-public crate-surgeshaper-tanh-0.2.4-alpha.0 (c (n "surgeshaper-tanh") (v "0.2.4-alpha.0") (d (list (d (n "surge-filter") (r "^0.2.4-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.4-alpha.0") (d #t) (k 0)))) (h "0x46l5sxmbnvlwfpbm300hcj4lrw7vsg9z59rnnc1x3ki8hv399w")))

(define-public crate-surgeshaper-tanh-0.2.5-alpha.0 (c (n "surgeshaper-tanh") (v "0.2.5-alpha.0") (d (list (d (n "surge-filter") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.5-alpha.0") (d #t) (k 0)))) (h "0wjdxbnh7lxwlb4sfpiyfpxq82bvhdx5zkac274i914bk9dcf8fi")))

(define-public crate-surgeshaper-tanh-0.2.11-alpha.0 (c (n "surgeshaper-tanh") (v "0.2.11-alpha.0") (d (list (d (n "surge-filter") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.11-alpha.0") (d #t) (k 0)))) (h "08zljbva3fdv4854zhfy3rd53rb8wgrsn6xkl57n4jgb75x5md0i")))

(define-public crate-surgeshaper-tanh-0.2.12-alpha.0 (c (n "surgeshaper-tanh") (v "0.2.12-alpha.0") (d (list (d (n "surge-filter") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.12-alpha.0") (d #t) (k 0)))) (h "1qs877s3ilqr3nqgbfh0jclxk4nahsdh5r2rkcvpna6rp9i7l626")))

