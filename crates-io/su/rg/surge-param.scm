(define-module (crates-io su rg surge-param) #:use-module (crates-io))

(define-public crate-surge-param-0.1.11-alpha.0 (c (n "surge-param") (v "0.1.11-alpha.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.11-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.11-alpha.0") (d #t) (k 0)))) (h "1fl22835ass82jn9hg11zikhij3a5zablpvg410naifl83lhj4cs")))

(define-public crate-surge-param-0.1.12-alpha.0 (c (n "surge-param") (v "0.1.12-alpha.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.12-alpha.0") (d #t) (k 0)))) (h "19iy1hp0qlbcgzrqpazpyhgm6gbdm773yi5a6zs04s13pzn1cyfk")))

(define-public crate-surge-param-0.1.13-alpha.0 (c (n "surge-param") (v "0.1.13-alpha.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.13-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.13-alpha.0") (d #t) (k 0)))) (h "0qfincrphybqqa9n068yaap7qsg1ywng0713jnb7ps8sml6qa045")))

(define-public crate-surge-param-0.1.14-alpha.0 (c (n "surge-param") (v "0.1.14-alpha.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.14-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.14-alpha.0") (d #t) (k 0)))) (h "0wmngfl12rniy2nbxmzd5b4kq1hblyd9dmfayl98wpyjimck3z74")))

(define-public crate-surge-param-0.1.15-alpha.0 (c (n "surge-param") (v "0.1.15-alpha.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.15-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.15-alpha.0") (d #t) (k 0)))) (h "1ni8jr872gpj5djfnizdypdlns6vr47xlnnhv87zsaasmxgrl6c5")))

(define-public crate-surge-param-0.1.16-alpha.0 (c (n "surge-param") (v "0.1.16-alpha.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.16-alpha.0") (d #t) (k 0)))) (h "1yazbhlr8kilal5gsakdh573225jw8gx5gp5gcxqxa89xca7nr63")))

(define-public crate-surge-param-0.1.17-alpha.0 (c (n "surge-param") (v "0.1.17-alpha.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.17-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.17-alpha.0") (d #t) (k 0)))) (h "1fdv9d9dxmllc524lq4bsli8nmrwfxi9jqcb8lck0hfn4g2g37ny")))

(define-public crate-surge-param-0.1.20-alpha.0 (c (n "surge-param") (v "0.1.20-alpha.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.20-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.20-alpha.0") (d #t) (k 0)))) (h "1c7ax28lf7dipmx67p06zds6ffwkqa2ss3vbim1vp1xsxiaja0n0")))

(define-public crate-surge-param-0.1.23-alpha.0 (c (n "surge-param") (v "0.1.23-alpha.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.23-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.23-alpha.0") (d #t) (k 0)))) (h "0k1gww63xcybhi7j68yfj0kfi5k6bq5ww7m26cnvx79fgvkmdcwk")))

(define-public crate-surge-param-0.1.24-alpha.0 (c (n "surge-param") (v "0.1.24-alpha.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.24-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.24-alpha.0") (d #t) (k 0)))) (h "1znah0zcq30rzp0xyymmlypayp7plw0qvndjdmbz1cb738dhikwz")))

(define-public crate-surge-param-0.1.26-alpha.0 (c (n "surge-param") (v "0.1.26-alpha.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.26-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.26-alpha.0") (d #t) (k 0)))) (h "1023v0nc67xi2axiqf7hxwl1j6d062hv1x3r6ljvg9yjkx2zrb00")))

(define-public crate-surge-param-0.1.27-alpha.0 (c (n "surge-param") (v "0.1.27-alpha.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.27-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.27-alpha.0") (d #t) (k 0)))) (h "06zlpgymb3qfdikfqd5fj4l6gspxx13nknm7lx4zm8kcyfghgmqb")))

(define-public crate-surge-param-0.1.28-alpha.0 (c (n "surge-param") (v "0.1.28-alpha.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.28-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.28-alpha.0") (d #t) (k 0)))) (h "0zbgrlw2cbf14r4zb22qp6fcm7bc5swkxyv1y6f5wfcf3x6yfbih")))

(define-public crate-surge-param-0.1.29-alpha.0 (c (n "surge-param") (v "0.1.29-alpha.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.29-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.29-alpha.0") (d #t) (k 0)))) (h "05rjqmr96z365gi6l754mli6id3cqfva6hc4zqismzpknljqrvjb")))

(define-public crate-surge-param-0.1.30-alpha.0 (c (n "surge-param") (v "0.1.30-alpha.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.30-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.30-alpha.0") (d #t) (k 0)))) (h "0314q5qal5irfyb2inngp0f4y71zvk7ylzhkd2ylhxdc4b7mgl20")))

(define-public crate-surge-param-0.1.31-alpha.0 (c (n "surge-param") (v "0.1.31-alpha.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.31-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.31-alpha.0") (d #t) (k 0)))) (h "151pi2fbrxy8crc0a73aryv8vxi1qqmkj5a9n9mfrr318mkz8fxf")))

(define-public crate-surge-param-0.1.32-alpha.0 (c (n "surge-param") (v "0.1.32-alpha.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.32-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.32-alpha.0") (d #t) (k 0)))) (h "0nv07isv1mb5pr85cl0jm2bwdxnyrmfid379p7ph02l87h9imxsa")))

(define-public crate-surge-param-0.1.33-alpha.0 (c (n "surge-param") (v "0.1.33-alpha.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.33-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.33-alpha.0") (d #t) (k 0)))) (h "1d92r8ryjfk1asp5q62ygggcii00kwh9cmcbqym3hhbxgmxljsb4")))

(define-public crate-surge-param-0.1.36-alpha.0 (c (n "surge-param") (v "0.1.36-alpha.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.36-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.36-alpha.0") (d #t) (k 0)))) (h "1vhjlm2rgbh7qncqkqbc4hsl10cp8xsg6ryqj9admpjd3x9m3373")))

(define-public crate-surge-param-0.1.39-alpha.0 (c (n "surge-param") (v "0.1.39-alpha.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.39-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.39-alpha.0") (d #t) (k 0)))) (h "1mfqm43zd3hbwlibcqw74dq7fi92wf120dxi7aqzmjh7zsnlssxm")))

(define-public crate-surge-param-0.1.40-alpha.0 (c (n "surge-param") (v "0.1.40-alpha.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.40-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.40-alpha.0") (d #t) (k 0)))) (h "1cin4ahcmk7zvvdmyfapmimrw6v90sqriz0vhgdd7vp43mr1avmg")))

(define-public crate-surge-param-0.1.42-alpha.0 (c (n "surge-param") (v "0.1.42-alpha.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.42-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.42-alpha.0") (d #t) (k 0)))) (h "066pfrasy6wnaalncdl2164dwq5j9jbgl3i2020jk7xzbs5nkh59")))

(define-public crate-surge-param-0.2.0-alpha.0 (c (n "surge-param") (v "0.2.0-alpha.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "surge-constants") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.0-alpha.0") (d #t) (k 0)))) (h "0qanl8gb4l2iz3snp9xfsy2mnq7sl69hpvspw0h8lj64bhhs02mk")))

(define-public crate-surge-param-0.2.3-alpha.0 (c (n "surge-param") (v "0.2.3-alpha.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "surge-constants") (r "^0.2.3-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.3-alpha.0") (d #t) (k 0)))) (h "0m3ci6rxb6qzkmh2zbb3kjyxgy0dn9w4xp7pp5kisx2iwyva8pi9")))

(define-public crate-surge-param-0.2.4-alpha.0 (c (n "surge-param") (v "0.2.4-alpha.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "surge-constants") (r "^0.2.4-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.4-alpha.0") (d #t) (k 0)))) (h "1awlk1bls9wnmlhf3khv7517m15i895gfjyff1kxdndlkybqwkcl")))

(define-public crate-surge-param-0.2.5-alpha.0 (c (n "surge-param") (v "0.2.5-alpha.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "surge-constants") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.5-alpha.0") (d #t) (k 0)))) (h "1kqfmiasvh2wgadbk5ygh6lf9ca9kx5ydlzkkz1cd7j6vbgdn38d")))

(define-public crate-surge-param-0.2.6-alpha.0 (c (n "surge-param") (v "0.2.6-alpha.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "surge-constants") (r "^0.2.6-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.6-alpha.0") (d #t) (k 0)))) (h "1r6sr20g0xdbalnykwxlkx5x0kq8rv0a9vkxivfdaxvi1srnw8wg")))

(define-public crate-surge-param-0.2.8-alpha.0 (c (n "surge-param") (v "0.2.8-alpha.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "surge-constants") (r "^0.2.8-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.8-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.8-alpha.0") (d #t) (k 0)))) (h "0gmar94mah9p0xil23n69nza3f66pn1css5rfzslcc75k21gfaig")))

(define-public crate-surge-param-0.2.9-alpha.0 (c (n "surge-param") (v "0.2.9-alpha.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "surge-constants") (r "^0.2.9-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.9-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.9-alpha.0") (d #t) (k 0)))) (h "0dylspcnpyb33pv6lvsicq821q11i90k7dggahpyzy6iamz1gs6a")))

(define-public crate-surge-param-0.2.11-alpha.0 (c (n "surge-param") (v "0.2.11-alpha.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "surge-constants") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.11-alpha.0") (d #t) (k 0)))) (h "1k56cbfwpf2bs4g3qn1fjqj4razrwifzpm899cx44ifn59n352pm")))

(define-public crate-surge-param-0.2.12-alpha.0 (c (n "surge-param") (v "0.2.12-alpha.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "surge-constants") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.12-alpha.0") (d #t) (k 0)))) (h "15vwb9sq8xncrk1qfman9x4kv4xjzgmfg6mpfjawl23dgaxk105g")))

