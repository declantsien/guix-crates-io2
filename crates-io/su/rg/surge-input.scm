(define-module (crates-io su rg surge-input) #:use-module (crates-io))

(define-public crate-surge-input-0.1.17-alpha.0 (c (n "surge-input") (v "0.1.17-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.17-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.17-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.17-alpha.0") (d #t) (k 0)))) (h "0q8qnsy7r1bxz6snya535bh17iz1djp0g884nds474h4f6i0sl19")))

(define-public crate-surge-input-0.1.20-alpha.0 (c (n "surge-input") (v "0.1.20-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.20-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.20-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.20-alpha.0") (d #t) (k 0)))) (h "197v0kjxnhg3dhwf426qaxjg7bcskg1zv8rzfc263s1hh426wr1y")))

(define-public crate-surge-input-0.1.23-alpha.0 (c (n "surge-input") (v "0.1.23-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.23-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.23-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.23-alpha.0") (d #t) (k 0)))) (h "0lcxh3l2g60j93w3vgvacc68masbryszv8iraw2yds0gyaj264s5")))

(define-public crate-surge-input-0.1.24-alpha.0 (c (n "surge-input") (v "0.1.24-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.24-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.24-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.24-alpha.0") (d #t) (k 0)))) (h "15mlkvhaangih1rbkzaf9ah1mzjs301li3s02lzbv7f96kahqwyg")))

(define-public crate-surge-input-0.1.26-alpha.0 (c (n "surge-input") (v "0.1.26-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.26-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.26-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.26-alpha.0") (d #t) (k 0)))) (h "0hjbpswgf1q2apricb73gi2aaq49bnbhbgj6vb9q1ghhkd7gjzn8")))

(define-public crate-surge-input-0.1.27-alpha.0 (c (n "surge-input") (v "0.1.27-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.27-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.27-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.27-alpha.0") (d #t) (k 0)))) (h "02jp8a92ajiw2zjah3pri6pif2dnpdv93fyqvxi0jrcbwhchbj5v")))

(define-public crate-surge-input-0.1.28-alpha.0 (c (n "surge-input") (v "0.1.28-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.28-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.28-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.28-alpha.0") (d #t) (k 0)))) (h "1lfw7xswvkq805a605r382gc7wf3qj9fac30isafjswjghhqw21h")))

(define-public crate-surge-input-0.1.29-alpha.0 (c (n "surge-input") (v "0.1.29-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.29-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.29-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.29-alpha.0") (d #t) (k 0)))) (h "1ywv02hdk0k41f7b4f502v9wycwpilkl2qgknq2h8lmylils14wf")))

(define-public crate-surge-input-0.1.30-alpha.0 (c (n "surge-input") (v "0.1.30-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.30-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.30-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.30-alpha.0") (d #t) (k 0)))) (h "16l65979nivvrghixcg8106h4ad93ld7fdpvgrz3rx1fnpg4zrvm")))

(define-public crate-surge-input-0.1.31-alpha.0 (c (n "surge-input") (v "0.1.31-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.31-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.31-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.31-alpha.0") (d #t) (k 0)))) (h "1b3l2bx6gfa9515msr3fgp4kzxvbg27frjpy4dh46g0lyb0h1d21")))

(define-public crate-surge-input-0.1.32-alpha.0 (c (n "surge-input") (v "0.1.32-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.32-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.32-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.32-alpha.0") (d #t) (k 0)))) (h "10n0460a0vjvnx4m9p9kvlb4q0xa5q34daajigc7m4d1g2mn74yp")))

(define-public crate-surge-input-0.1.33-alpha.0 (c (n "surge-input") (v "0.1.33-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.33-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.33-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.33-alpha.0") (d #t) (k 0)))) (h "0dwpz3zy773y91fyzszv2b82grkzsgg2pqbv2vidj4514hl6qxik")))

(define-public crate-surge-input-0.1.36-alpha.0 (c (n "surge-input") (v "0.1.36-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.36-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.36-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.36-alpha.0") (d #t) (k 0)))) (h "0894i6lw7nqajj0d9iqycinqvc4fmijwsc8759924xn6wdvgyac5")))

(define-public crate-surge-input-0.1.39-alpha.0 (c (n "surge-input") (v "0.1.39-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.39-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.39-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.39-alpha.0") (d #t) (k 0)))) (h "1lxpwdpsrgmyf66hqr8c55ap2841j9210ip2l83awhfr007qwrkd")))

(define-public crate-surge-input-0.1.40-alpha.0 (c (n "surge-input") (v "0.1.40-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.40-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.40-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.40-alpha.0") (d #t) (k 0)))) (h "1k00b1qs2476ak6ssf0zkwar2n5x7j6rp3bjayqb6nh0f1gjcp3l")))

(define-public crate-surge-input-0.1.42-alpha.0 (c (n "surge-input") (v "0.1.42-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.42-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.42-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.42-alpha.0") (d #t) (k 0)))) (h "168qgis47sk8firzjvijxsix4nyz9pgm3g8s9jb2r9dm990xcaaw")))

(define-public crate-surge-input-0.2.0-alpha.0 (c (n "surge-input") (v "0.2.0-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.0-alpha.0") (d #t) (k 0)))) (h "1a65lzns050sdvl09mbpyl3cbl4ksz9qlx3c8mwxlcicwcgkj1vy")))

(define-public crate-surge-input-0.2.4-alpha.0 (c (n "surge-input") (v "0.2.4-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.2.4-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.4-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.4-alpha.0") (d #t) (k 0)))) (h "0jn51p8ixv0hfsynqgh12i9ih66gjqf5cy8dgicy4wqn9dpjlfag")))

(define-public crate-surge-input-0.2.5-alpha.0 (c (n "surge-input") (v "0.2.5-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.5-alpha.0") (d #t) (k 0)))) (h "0wmaf014yzqcxznszx4qp1fbjninskp9ms035h23m1h8f43insgi")))

(define-public crate-surge-input-0.2.6-alpha.0 (c (n "surge-input") (v "0.2.6-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.6-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.6-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.6-alpha.0") (d #t) (k 0)))) (h "1xf0kd3c2s3ll19lzljp1zz126gm0db2rss3dvf22dkzkfl4l3jq")))

(define-public crate-surge-input-0.2.9-alpha.0 (c (n "surge-input") (v "0.2.9-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.9-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.9-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.9-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.9-alpha.0") (d #t) (k 0)))) (h "1hbkcr78hw9qs6r3m93jqxfphmxzxd094m9g7z31wcnw7pqci797")))

(define-public crate-surge-input-0.2.11-alpha.0 (c (n "surge-input") (v "0.2.11-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.11-alpha.0") (d #t) (k 0)))) (h "018arkyz1qnp00kiwf0nfgpzs0h2f8zw7v6nrnhfznwij9ysnghk")))

(define-public crate-surge-input-0.2.12-alpha.0 (c (n "surge-input") (v "0.2.12-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.12-alpha.0") (d #t) (k 0)))) (h "08202adckn1kkmamsnqji8nmp0l5phnjinls6x4b70rprabwkx12")))

