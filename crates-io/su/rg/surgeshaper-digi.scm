(define-module (crates-io su rg surgeshaper-digi) #:use-module (crates-io))

(define-public crate-surgeshaper-digi-0.1.33-alpha.0 (c (n "surgeshaper-digi") (v "0.1.33-alpha.0") (d (list (d (n "surge-filter") (r "^0.1.33-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.33-alpha.0") (d #t) (k 0)))) (h "16bpy08nf1yzakb4kc1143rk1g7ibgf7nlr5q7hzvq4yw8b2z3x3")))

(define-public crate-surgeshaper-digi-0.1.40-alpha.0 (c (n "surgeshaper-digi") (v "0.1.40-alpha.0") (d (list (d (n "surge-filter") (r "^0.1.40-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.40-alpha.0") (d #t) (k 0)))) (h "1svxm0fr7lhxcsihwdd0xymj40a6y06pab885b1747zwjk1wpwcn")))

(define-public crate-surgeshaper-digi-0.1.42-alpha.0 (c (n "surgeshaper-digi") (v "0.1.42-alpha.0") (d (list (d (n "surge-filter") (r "^0.1.42-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.42-alpha.0") (d #t) (k 0)))) (h "03mdz9v1cbf1a026qazpks0sqhhj32c07fb0c6h9f86l44g5d7ya")))

(define-public crate-surgeshaper-digi-0.2.0-alpha.0 (c (n "surgeshaper-digi") (v "0.2.0-alpha.0") (d (list (d (n "surge-filter") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.0-alpha.0") (d #t) (k 0)))) (h "02cpakqfad7lz9shipm22gjjpnyjcwywjwvx2f5dmvr6641r4q2a")))

(define-public crate-surgeshaper-digi-0.2.4-alpha.0 (c (n "surgeshaper-digi") (v "0.2.4-alpha.0") (d (list (d (n "surge-filter") (r "^0.2.4-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.4-alpha.0") (d #t) (k 0)))) (h "1s26ylyxx9x24555s4qynfn8f12mw98z05n2zcb4pbb9xdk5f0zr")))

(define-public crate-surgeshaper-digi-0.2.5-alpha.0 (c (n "surgeshaper-digi") (v "0.2.5-alpha.0") (d (list (d (n "surge-filter") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.5-alpha.0") (d #t) (k 0)))) (h "10sa9wwac5scmr6rh4r8d2bf85l088fmik0nylqbfmyx58cc70kn")))

(define-public crate-surgeshaper-digi-0.2.11-alpha.0 (c (n "surgeshaper-digi") (v "0.2.11-alpha.0") (d (list (d (n "surge-filter") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.11-alpha.0") (d #t) (k 0)))) (h "1dif4x6wil5dvny7sfa7hgk5flppn37p1a7kc9h9rx3z2xnaqfma")))

(define-public crate-surgeshaper-digi-0.2.12-alpha.0 (c (n "surgeshaper-digi") (v "0.2.12-alpha.0") (d (list (d (n "surge-filter") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.12-alpha.0") (d #t) (k 0)))) (h "1i5vqbi0sbq11w72cw9pxm7fijmli166xkmaadbx3kzwncvqn3bd")))

