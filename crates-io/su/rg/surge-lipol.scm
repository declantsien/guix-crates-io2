(define-module (crates-io su rg surge-lipol) #:use-module (crates-io))

(define-public crate-surge-lipol-0.1.17-alpha.0 (c (n "surge-lipol") (v "0.1.17-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.17-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.17-alpha.0") (d #t) (k 0)))) (h "04z1y2pb12imj5bhiv0jrqqcaq8f9fyqj0jaghbs23lbhdvzazwj")))

(define-public crate-surge-lipol-0.1.20-alpha.0 (c (n "surge-lipol") (v "0.1.20-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.20-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.20-alpha.0") (d #t) (k 0)))) (h "1pgp3c55hlk1fnx6wz5rpnmgjn9bwlk6bd48l8y2ggbr0bq3ljzk")))

(define-public crate-surge-lipol-0.1.23-alpha.0 (c (n "surge-lipol") (v "0.1.23-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.23-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.23-alpha.0") (d #t) (k 0)))) (h "10s9ix3xhqdw5dihv0m5v1jb21jv4qy3vmx35xh0lapm6ra9hw7l")))

(define-public crate-surge-lipol-0.1.24-alpha.0 (c (n "surge-lipol") (v "0.1.24-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.24-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.24-alpha.0") (d #t) (k 0)))) (h "0nydx2ac9m6dgws578ws0h4xpbksh1cbdxlyhzsbpcl74wcy59xd")))

(define-public crate-surge-lipol-0.1.26-alpha.0 (c (n "surge-lipol") (v "0.1.26-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.26-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.26-alpha.0") (d #t) (k 0)))) (h "1v98d9hn444vsxa6gpcp5c0pjkjaj7ixb3r6ffrgsf7ixzvnbvlp")))

(define-public crate-surge-lipol-0.1.27-alpha.0 (c (n "surge-lipol") (v "0.1.27-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.27-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.27-alpha.0") (d #t) (k 0)))) (h "0218j5iiyg2sbvrw6ngn8w9gjzddaf7q1qjanqnglwgdggjip6cm")))

(define-public crate-surge-lipol-0.1.28-alpha.0 (c (n "surge-lipol") (v "0.1.28-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.28-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.28-alpha.0") (d #t) (k 0)))) (h "1ja4kiybpr3c3hqnxmn9l8x3pzwsi6gbhapwf9kiw9r12sd7xpy9")))

(define-public crate-surge-lipol-0.1.29-alpha.0 (c (n "surge-lipol") (v "0.1.29-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.29-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.29-alpha.0") (d #t) (k 0)))) (h "0nh3v85vkiwql25fkna5dpdnd9saphk48fkkfg4yr5xzar864rbw")))

(define-public crate-surge-lipol-0.1.30-alpha.0 (c (n "surge-lipol") (v "0.1.30-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.30-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.30-alpha.0") (d #t) (k 0)))) (h "0k5vmi75ad7ha7kyi2vvmvyfi9jw340y0l4ck0c6y9aqswny3ziq")))

(define-public crate-surge-lipol-0.1.31-alpha.0 (c (n "surge-lipol") (v "0.1.31-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.31-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.31-alpha.0") (d #t) (k 0)))) (h "196hx589sm48g7rlf4vrwzhj1lwxh8cwy3ibq0j31848nid3c76c")))

(define-public crate-surge-lipol-0.1.32-alpha.0 (c (n "surge-lipol") (v "0.1.32-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.32-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.32-alpha.0") (d #t) (k 0)))) (h "17q55pv0279c1r9mxi3f5qh0v2pk9dml1y2l1ghszz51hxy34jq0")))

(define-public crate-surge-lipol-0.1.33-alpha.0 (c (n "surge-lipol") (v "0.1.33-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.33-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.33-alpha.0") (d #t) (k 0)))) (h "0kib6ipbs6vrx1bx5abh7cifxjkhizfa5yxa7h6i30afk5kflf41")))

(define-public crate-surge-lipol-0.1.36-alpha.0 (c (n "surge-lipol") (v "0.1.36-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.36-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.36-alpha.0") (d #t) (k 0)))) (h "01qmb6g3ma5igk8xcx07ai9avp7kfbxj96rmwsr1c157x8svxk7k")))

(define-public crate-surge-lipol-0.1.39-alpha.0 (c (n "surge-lipol") (v "0.1.39-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.39-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.39-alpha.0") (d #t) (k 0)))) (h "01jfb5wjinh9hlbsivcr25wapd3cjkxw9b89fyblardhb7bgzz4f")))

(define-public crate-surge-lipol-0.1.40-alpha.0 (c (n "surge-lipol") (v "0.1.40-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.40-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.40-alpha.0") (d #t) (k 0)))) (h "1fifa6f8iykpw6j46x9iwskkfmdj6sl52i6j94wlnckfd14h5kla")))

(define-public crate-surge-lipol-0.1.42-alpha.0 (c (n "surge-lipol") (v "0.1.42-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.42-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.42-alpha.0") (d #t) (k 0)))) (h "0i44kq4lhyszwwqmaf57pyqq2fw9pkh2bkjs4zzx457j6i618q9q")))

(define-public crate-surge-lipol-0.2.0-alpha.0 (c (n "surge-lipol") (v "0.2.0-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-constants") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.0-alpha.0") (d #t) (k 0)))) (h "0j16shv98av3bcjjg2k6m7w7wqrbrfjs39x562iyz1rq6gpay9a2")))

(define-public crate-surge-lipol-0.2.4-alpha.0 (c (n "surge-lipol") (v "0.2.4-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-constants") (r "^0.2.4-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.4-alpha.0") (d #t) (k 0)))) (h "0a6qi4kj1kpxlv7b3di9pwbhk7ngh40b5rvdwgdkaarrfxqysi44")))

(define-public crate-surge-lipol-0.2.5-alpha.0 (c (n "surge-lipol") (v "0.2.5-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.5-alpha.0") (d #t) (k 0)))) (h "0simfixag87mqh34nvd6mjgy1a9jjqj5sczr4zs3iv16ag5cs0z1")))

(define-public crate-surge-lipol-0.2.6-alpha.0 (c (n "surge-lipol") (v "0.2.6-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.6-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.6-alpha.0") (d #t) (k 0)))) (h "18j2mi12zpkk2qd6nw7wdpvknskdwvv9s0fr2qgny4krmgn88gzl")))

(define-public crate-surge-lipol-0.2.9-alpha.0 (c (n "surge-lipol") (v "0.2.9-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.9-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.9-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.9-alpha.0") (d #t) (k 0)))) (h "1y9ybckska5bz22jfbhlcfl7ibaigjdc7576j9p3bpzqgx25dfjl")))

(define-public crate-surge-lipol-0.2.11-alpha.0 (c (n "surge-lipol") (v "0.2.11-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.11-alpha.0") (d #t) (k 0)))) (h "02n3924a7almbxck9xrd2dyfypszxpgy7jcldki38qk0shhki8mv")))

(define-public crate-surge-lipol-0.2.12-alpha.0 (c (n "surge-lipol") (v "0.2.12-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.12-alpha.0") (d #t) (k 0)))) (h "1bqamhx4a0a1ka2db4d5qinjc7fh4w0cblaygfbwbhj8c1xdqga9")))

