(define-module (crates-io su rg surge-traits) #:use-module (crates-io))

(define-public crate-surge-traits-0.1.11-alpha.0 (c (n "surge-traits") (v "0.1.11-alpha.0") (d (list (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.11-alpha.0") (d #t) (k 0)))) (h "05fxfnwi2fbrg9af0dsnsblya0ffswzhidab5kwnvs8r7w2hlkc8")))

(define-public crate-surge-traits-0.1.12-alpha.0 (c (n "surge-traits") (v "0.1.12-alpha.0") (d (list (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.12-alpha.0") (d #t) (k 0)))) (h "1jqwd2qhacw8ibh77r3g1i5373747vh4bdnhsqiw1rrf09qvl994")))

(define-public crate-surge-traits-0.1.14-alpha.0 (c (n "surge-traits") (v "0.1.14-alpha.0") (d (list (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.14-alpha.0") (d #t) (k 0)))) (h "0rlis619hjpsbdfa2h1qxavr9j7sbpnarrajsf3i0jgdgdxjsckg")))

(define-public crate-surge-traits-0.1.15-alpha.0 (c (n "surge-traits") (v "0.1.15-alpha.0") (d (list (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.15-alpha.0") (d #t) (k 0)))) (h "1nqyn9cfljpl62fvsvb47g3802vkckahf366mapvhqhqxn8n6nx2")))

(define-public crate-surge-traits-0.1.16-alpha.0 (c (n "surge-traits") (v "0.1.16-alpha.0") (d (list (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.16-alpha.0") (d #t) (k 0)))) (h "1kmgr0q9r2npya2qbfakcw6pw4dnrr91xcrq4r3ydz603wk7k2dn")))

(define-public crate-surge-traits-0.1.17-alpha.0 (c (n "surge-traits") (v "0.1.17-alpha.0") (d (list (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.17-alpha.0") (d #t) (k 0)))) (h "1xvi2apqkvsl5ibqk5ppdhsi8q8y646nkjrwsgmiviyfyd72ra8y")))

(define-public crate-surge-traits-0.1.20-alpha.0 (c (n "surge-traits") (v "0.1.20-alpha.0") (d (list (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.20-alpha.0") (d #t) (k 0)))) (h "059jr5fj1jbzy6s3a9na6src0br8758nnpi0p9vd13ypqvksnv8q")))

(define-public crate-surge-traits-0.1.23-alpha.0 (c (n "surge-traits") (v "0.1.23-alpha.0") (d (list (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.23-alpha.0") (d #t) (k 0)))) (h "1lj29fdibqk6ihkw1cjr8j69xc8vsg93kyhc5yz6zbfhfnsjfhii")))

(define-public crate-surge-traits-0.1.24-alpha.0 (c (n "surge-traits") (v "0.1.24-alpha.0") (d (list (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.24-alpha.0") (d #t) (k 0)))) (h "0a9fzyivnkypysidkwgcx88i8wqd1rh5g99i4v07ab330av5vxn3")))

(define-public crate-surge-traits-0.1.26-alpha.0 (c (n "surge-traits") (v "0.1.26-alpha.0") (d (list (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.26-alpha.0") (d #t) (k 0)))) (h "0nmsk88va3s2g4ajsvgx45mllrpax09nf3pmj7wnf558vf2fa2bl")))

(define-public crate-surge-traits-0.1.27-alpha.0 (c (n "surge-traits") (v "0.1.27-alpha.0") (d (list (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.27-alpha.0") (d #t) (k 0)))) (h "10wh4016jhlp4y462kl6jm1wwzm7al5r3p1xl3d1wy3dcv7rcciy")))

(define-public crate-surge-traits-0.1.28-alpha.0 (c (n "surge-traits") (v "0.1.28-alpha.0") (d (list (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.28-alpha.0") (d #t) (k 0)))) (h "17v4qzvk6xwd9wlaqgf7511k9s04yydawbfw5hp55s57db5qnl2g")))

(define-public crate-surge-traits-0.1.29-alpha.0 (c (n "surge-traits") (v "0.1.29-alpha.0") (d (list (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.29-alpha.0") (d #t) (k 0)))) (h "1fja5xkypb7dkm87mpabi6p78s639113dcksdh0k36q8p0scz9sl")))

(define-public crate-surge-traits-0.1.30-alpha.0 (c (n "surge-traits") (v "0.1.30-alpha.0") (d (list (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.30-alpha.0") (d #t) (k 0)))) (h "0vdi1npcxwv4fjx26jx6grn207aff6932v0xdkfcfcli0bjjhhks")))

(define-public crate-surge-traits-0.1.31-alpha.0 (c (n "surge-traits") (v "0.1.31-alpha.0") (d (list (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.31-alpha.0") (d #t) (k 0)))) (h "0b3ykid26msiyfv9qp4sgbr4bkd7www6fyngcv5xdn3lbf2083sa")))

(define-public crate-surge-traits-0.1.32-alpha.0 (c (n "surge-traits") (v "0.1.32-alpha.0") (d (list (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.32-alpha.0") (d #t) (k 0)))) (h "1p7vq7p2l6x82m2f9nkjnwn80a1bicc3nnqar4vbb6q9ih9lqi97")))

(define-public crate-surge-traits-0.1.33-alpha.0 (c (n "surge-traits") (v "0.1.33-alpha.0") (d (list (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.33-alpha.0") (d #t) (k 0)))) (h "1kbh5qck82hhsbql3gd8k4pm803044ncn24fj3ax1a4h87kxjrpk")))

(define-public crate-surge-traits-0.1.36-alpha.0 (c (n "surge-traits") (v "0.1.36-alpha.0") (d (list (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.36-alpha.0") (d #t) (k 0)))) (h "0vxw6bacy0wffkygszbdhhw5xbgbxycsva8b6kmz07zw0ii800if")))

(define-public crate-surge-traits-0.1.39-alpha.0 (c (n "surge-traits") (v "0.1.39-alpha.0") (d (list (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.39-alpha.0") (d #t) (k 0)))) (h "0i32grvh68rps1kphb6c3yrlf1addmjdzlwmhdpi11xnxid7i280")))

(define-public crate-surge-traits-0.1.40-alpha.0 (c (n "surge-traits") (v "0.1.40-alpha.0") (d (list (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.40-alpha.0") (d #t) (k 0)))) (h "0r9csqp1pbg6q3klhcfzia1swcrfs061pb59p06419bfvyz0xf3l")))

(define-public crate-surge-traits-0.1.42-alpha.0 (c (n "surge-traits") (v "0.1.42-alpha.0") (d (list (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.42-alpha.0") (d #t) (k 0)))) (h "1cpamm97cpdys48pm0hm56w4vgq2ds17qza0xvny3f9kjz583wsx")))

(define-public crate-surge-traits-0.2.0-alpha.0 (c (n "surge-traits") (v "0.2.0-alpha.0") (d (list (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-types") (r "^0.2.0-alpha.0") (d #t) (k 0)))) (h "06n68li2qqm4fwjmjcrkbb8z9lmcds2bjfa918r1pz8bqh0i6kms")))

(define-public crate-surge-traits-0.2.3-alpha.0 (c (n "surge-traits") (v "0.2.3-alpha.0") (d (list (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-types") (r "^0.2.3-alpha.0") (d #t) (k 0)))) (h "00xd04jl2nq52s1x7skrlwyz079wp64w4r0crlbfs1q0z2c87sn8")))

(define-public crate-surge-traits-0.2.4-alpha.0 (c (n "surge-traits") (v "0.2.4-alpha.0") (d (list (d (n "enhanced_enum") (r "^0.2.2") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-types") (r "^0.2.4-alpha.0") (d #t) (k 0)))) (h "0nm2wg51s99xvbhwjrnp7x2fcvi6krd9kgphhqhfbhdq2ic3n341")))

(define-public crate-surge-traits-0.2.5-alpha.0 (c (n "surge-traits") (v "0.2.5-alpha.0") (d (list (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.2.5-alpha.0") (d #t) (k 0)))) (h "0lbnndn2x0idf956kcqgln01p6k5is10xy42q02wp1lfvqsfdsvw")))

(define-public crate-surge-traits-0.2.6-alpha.0 (c (n "surge-traits") (v "0.2.6-alpha.0") (d (list (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.2.6-alpha.0") (d #t) (k 0)))) (h "02284k6wwgn05fkbxdpsjf5aar2czcawfpzlb356a5pwqdgv6dpl")))

(define-public crate-surge-traits-0.2.8-alpha.0 (c (n "surge-traits") (v "0.2.8-alpha.0") (d (list (d (n "surge-imports") (r "^0.2.8-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.2.8-alpha.0") (d #t) (k 0)))) (h "04lslqnwk18jzib9rbvr2gn6qr9affwn5q2302r3whs1878lrhyn")))

(define-public crate-surge-traits-0.2.9-alpha.0 (c (n "surge-traits") (v "0.2.9-alpha.0") (d (list (d (n "surge-imports") (r "^0.2.9-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.2.9-alpha.0") (d #t) (k 0)))) (h "0gzigf6clwl7lmp5dr5mbh5j4c8s9zc0kgjs7517ag5jlry2v2ma")))

(define-public crate-surge-traits-0.2.11-alpha.0 (c (n "surge-traits") (v "0.2.11-alpha.0") (d (list (d (n "surge-imports") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.2.11-alpha.0") (d #t) (k 0)))) (h "1bwwyprikklykz11i3npcldishk3ffm5rim8mxpxc23y25c45n4r")))

(define-public crate-surge-traits-0.2.12-alpha.0 (c (n "surge-traits") (v "0.2.12-alpha.0") (d (list (d (n "surge-imports") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.2.12-alpha.0") (d #t) (k 0)))) (h "1qyk1l3l75sna18sl8wgg211pd2mp3mrgcijvcjhjgypmxvf7dc5")))

