(define-module (crates-io su rg surge-hound) #:use-module (crates-io))

(define-public crate-surge-hound-0.1.7-alpha.0 (c (n "surge-hound") (v "0.1.7-alpha.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "0rl7r7bsrqrr22ih3pbvmc7k6hp76c1x40dahk2awirrq74axpii")))

(define-public crate-surge-hound-0.1.8-alpha.0 (c (n "surge-hound") (v "0.1.8-alpha.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "0zl2vpi6ksm4sczqc1h2kwfwypf65cm3v8h5p42pm16zncbms336")))

(define-public crate-surge-hound-0.1.17-alpha.0 (c (n "surge-hound") (v "0.1.17-alpha.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "19dfhpm5pw4dp82cj65ghhiqrx6lrp4dcfjm5b8r8ab3gqpxz3lq")))

(define-public crate-surge-hound-0.1.20-alpha.0 (c (n "surge-hound") (v "0.1.20-alpha.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "17qs2668y2ixrrqd0hpvjbavf5lz2idn2bvgq62kk5i7p0xbysfl")))

(define-public crate-surge-hound-0.1.23-alpha.0 (c (n "surge-hound") (v "0.1.23-alpha.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "0k8z9ji4r94rr3ks8a045srsbkzc9715gzdlkg4hk1aaa0j84kir")))

(define-public crate-surge-hound-0.1.24-alpha.0 (c (n "surge-hound") (v "0.1.24-alpha.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "0mxjb1gq027jh8rmzwjgbiix5n21dd0vdh8lyaq7lwlam7db0dg7")))

(define-public crate-surge-hound-0.1.26-alpha.0 (c (n "surge-hound") (v "0.1.26-alpha.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "0y744bgmz0fc42ayi07m4hwdmz10bx6m00cs10axy6p41zlx7ff6")))

(define-public crate-surge-hound-0.1.27-alpha.0 (c (n "surge-hound") (v "0.1.27-alpha.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "1f984s7vlnk0rnb0kvx8348r6fz9wv4fama06dpcj11akcgsz3zk")))

(define-public crate-surge-hound-0.1.28-alpha.0 (c (n "surge-hound") (v "0.1.28-alpha.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "0ncqfw85piy5vph4g72gny1v04ns3kvzw9iwjmh4xdbdj6gy82ax")))

(define-public crate-surge-hound-0.1.29-alpha.0 (c (n "surge-hound") (v "0.1.29-alpha.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "1p6060r0v55kkc8ixibk7ccmssv93kdic05i4lc75ivcxn7iwsfl")))

(define-public crate-surge-hound-0.1.30-alpha.0 (c (n "surge-hound") (v "0.1.30-alpha.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "14jj0hqbzrl7989nf5cyv9f45ajmwk21743zq6paznjzvpmqpxy8")))

(define-public crate-surge-hound-0.1.31-alpha.0 (c (n "surge-hound") (v "0.1.31-alpha.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "15w8imzdwral4hr516fbjps6yh24czykdb0msdym85aw249iz01h")))

(define-public crate-surge-hound-0.1.32-alpha.0 (c (n "surge-hound") (v "0.1.32-alpha.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "0s6q1aasjaj2y2a2lrc8wfd9i7ss99vbf1y9kdqz7h19j7mhf1sb")))

(define-public crate-surge-hound-0.1.33-alpha.0 (c (n "surge-hound") (v "0.1.33-alpha.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "1k7rzai9bxnvsz206vcyf8m2lcfhb0p5chwalsv9skjfsgfawvxc")))

(define-public crate-surge-hound-0.1.39-alpha.0 (c (n "surge-hound") (v "0.1.39-alpha.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "104kfg2xvpq9kyxmynm9cvgg5s8gils6c56dk7nv86kivxb6fyrz")))

(define-public crate-surge-hound-0.1.40-alpha.0 (c (n "surge-hound") (v "0.1.40-alpha.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "1fb038qlgsn7d08b139nx9a5jxwsgivmb1v82za4ad4m04pxjrwl")))

(define-public crate-surge-hound-0.1.42-alpha.0 (c (n "surge-hound") (v "0.1.42-alpha.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "0qq44csmb4cpcbns4p9ddhvdx4ld24q28fyw8fqxlhic1myx146n")))

(define-public crate-surge-hound-0.2.0-alpha.0 (c (n "surge-hound") (v "0.2.0-alpha.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "0n5p9bkiq2kykiyi0imyyax5icipn3ljs403m5lfqwk9w7z1gzxs")))

(define-public crate-surge-hound-0.2.4-alpha.0 (c (n "surge-hound") (v "0.2.4-alpha.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "015n4mjl7sdsn8sq8qmjw8yji67cdwk7s75jn3z6dpwbh4igal67")))

(define-public crate-surge-hound-0.2.5-alpha.0 (c (n "surge-hound") (v "0.2.5-alpha.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "0qr00jaicncx7rg3s285kri29hpf01za2gl194apmlhv1aa2bs9c")))

(define-public crate-surge-hound-0.2.9-alpha.0 (c (n "surge-hound") (v "0.2.9-alpha.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "0q7y24dh696qsly7azzzj3a88y3h18nvbhvl1p5y1jbzl662xz34")))

(define-public crate-surge-hound-0.2.11-alpha.0 (c (n "surge-hound") (v "0.2.11-alpha.0") (d (list (d (n "cpal") (r "^0.2.12") (d #t) (k 2)))) (h "17rdaknf45zmd18nq5hw752ly66z7r0rm7zj83dgynjl9dn24rni")))

