(define-module (crates-io su rg surge-svf) #:use-module (crates-io))

(define-public crate-surge-svf-0.1.29-alpha.0 (c (n "surge-svf") (v "0.1.29-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.29-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.29-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.29-alpha.0") (d #t) (k 0)))) (h "1bd2ma9almia4fn3brswl48xy1wbx709r85dsk85273nw792qcmb")))

(define-public crate-surge-svf-0.1.30-alpha.0 (c (n "surge-svf") (v "0.1.30-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.30-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.30-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.30-alpha.0") (d #t) (k 0)))) (h "14qra2whrb7l5m2x0q4jjndh5cdfpryzc87mhmdk0q8w6m20qs6v")))

(define-public crate-surge-svf-0.1.31-alpha.0 (c (n "surge-svf") (v "0.1.31-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.31-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.31-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.31-alpha.0") (d #t) (k 0)))) (h "0j2wmlb0d8dbv85la5jq2zpnjvb7qwmx8m5j4cb1d68j1x3adnbx")))

(define-public crate-surge-svf-0.1.32-alpha.0 (c (n "surge-svf") (v "0.1.32-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.32-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.32-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.32-alpha.0") (d #t) (k 0)))) (h "0l582vywsnrqmbvj2rakb21gzixb70y92xx2m1c1r4civj9f5n08")))

(define-public crate-surge-svf-0.1.33-alpha.0 (c (n "surge-svf") (v "0.1.33-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.33-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.33-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.33-alpha.0") (d #t) (k 0)))) (h "18p216p21v3pmxcv3dv4jkzsz35pk71wcjlzi49v6aid9c66nrbm")))

(define-public crate-surge-svf-0.1.40-alpha.0 (c (n "surge-svf") (v "0.1.40-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.40-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.40-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.40-alpha.0") (d #t) (k 0)))) (h "0mi5y6bhxxmqisga7aw1vhs1j83mvcqzxylza96l16s39d06w6lr")))

(define-public crate-surge-svf-0.1.42-alpha.0 (c (n "surge-svf") (v "0.1.42-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.42-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.42-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.42-alpha.0") (d #t) (k 0)))) (h "09mi3d8gixn2q1ykjjbp8jla1n760yplqmdykvdg5074r0lf6ank")))

(define-public crate-surge-svf-0.2.0-alpha.0 (c (n "surge-svf") (v "0.2.0-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.0-alpha.0") (d #t) (k 0)))) (h "0g0h162j6kcpv5agdwjm7wrjljlqzkf94xy9j12sfzkxds7wnx6z")))

(define-public crate-surge-svf-0.2.4-alpha.0 (c (n "surge-svf") (v "0.2.4-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.4-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.4-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.4-alpha.0") (d #t) (k 0)))) (h "1dnllws71x7wp596w91nkl981z28ysb7f5ika7w4l5l9k5hgl13l")))

(define-public crate-surge-svf-0.2.5-alpha.0 (c (n "surge-svf") (v "0.2.5-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.5-alpha.0") (d #t) (k 0)))) (h "1jjpabn3910lvpn6jlkx29m5s3h4bwry0hblzpnimcyids61kiq5")))

(define-public crate-surge-svf-0.2.6-alpha.0 (c (n "surge-svf") (v "0.2.6-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.6-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.6-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.6-alpha.0") (d #t) (k 0)))) (h "0pfyqy3cp7jnf10rgyly7iv82zlzdvbdm99d14l1fx03mzpz9wiq")))

(define-public crate-surge-svf-0.2.11-alpha.0 (c (n "surge-svf") (v "0.2.11-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.11-alpha.0") (d #t) (k 0)))) (h "1wzab3127zcsycgi83sz0z4gn52rxhqfbl9kr6rkfssi1z9jx5jd")))

(define-public crate-surge-svf-0.2.12-alpha.0 (c (n "surge-svf") (v "0.2.12-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.12-alpha.0") (d #t) (k 0)))) (h "16rng5d7b70in783vxlmrjxqjha0jw5yd4gcq057cm06w33g52kh")))

