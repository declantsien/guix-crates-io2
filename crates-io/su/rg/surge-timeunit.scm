(define-module (crates-io su rg surge-timeunit) #:use-module (crates-io))

(define-public crate-surge-timeunit-0.1.12-alpha.0 (c (n "surge-timeunit") (v "0.1.12-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.12-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.12-alpha.0") (d #t) (k 0)))) (h "0jab6kb3b34h29zr83zaahi68zm8xn3wzlrh664gb9ddqn1zpql4")))

(define-public crate-surge-timeunit-0.1.14-alpha.0 (c (n "surge-timeunit") (v "0.1.14-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.14-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.14-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.14-alpha.0") (d #t) (k 0)))) (h "19zv1r5m9n0ais38s2x228a686kn5mr051yfwz3fg2hmy1zqpa22")))

(define-public crate-surge-timeunit-0.1.15-alpha.0 (c (n "surge-timeunit") (v "0.1.15-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.15-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.15-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.15-alpha.0") (d #t) (k 0)))) (h "0rkb1a4bc69ib69qrf9kfqqpyy1w2m9jwpjjs85fhisymw84lzxk")))

(define-public crate-surge-timeunit-0.1.16-alpha.0 (c (n "surge-timeunit") (v "0.1.16-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.16-alpha.0") (d #t) (k 0)))) (h "1baixqa84c8m9kj9vzwcj5i7zp5gm7916z69mk04n4l0kcbf3jcr")))

(define-public crate-surge-timeunit-0.1.17-alpha.0 (c (n "surge-timeunit") (v "0.1.17-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.17-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.17-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.17-alpha.0") (d #t) (k 0)))) (h "04sbmmyy6hwg17mk37b3az41jhvg2sdv7dqklrbxp2zxcw6ij9l1")))

(define-public crate-surge-timeunit-0.1.20-alpha.0 (c (n "surge-timeunit") (v "0.1.20-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.20-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.20-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.20-alpha.0") (d #t) (k 0)))) (h "16hb9h245b088sijhp6898y4r8a9ly5kjfip9bq5i9bf3i5n5gjl")))

(define-public crate-surge-timeunit-0.1.23-alpha.0 (c (n "surge-timeunit") (v "0.1.23-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.23-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.23-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.23-alpha.0") (d #t) (k 0)))) (h "1bk1f35afvzbx2zax5b3822kgjz1dk52iwwzivlf1ayj5sg5yz90")))

(define-public crate-surge-timeunit-0.1.24-alpha.0 (c (n "surge-timeunit") (v "0.1.24-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.24-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.24-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.24-alpha.0") (d #t) (k 0)))) (h "0fx6vlj2c2i1jhiqs2b1lvwviwvadqps27xr184cb2agx5fg2gad")))

(define-public crate-surge-timeunit-0.1.26-alpha.0 (c (n "surge-timeunit") (v "0.1.26-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.26-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.26-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.26-alpha.0") (d #t) (k 0)))) (h "06r6kivphabw2l2msn4p3ckr1adabi9dc9sx9vn1pci3rqiwp2vc")))

(define-public crate-surge-timeunit-0.1.27-alpha.0 (c (n "surge-timeunit") (v "0.1.27-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.27-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.27-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.27-alpha.0") (d #t) (k 0)))) (h "1hx0vzg52pynsipr2qfvq29riyjf38g455q7h6j7xc2l2wfzwmkb")))

(define-public crate-surge-timeunit-0.1.28-alpha.0 (c (n "surge-timeunit") (v "0.1.28-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.28-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.28-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.28-alpha.0") (d #t) (k 0)))) (h "190x5s1nwz0rbrvrj8b20s39041ix6z414lsjxfbx5hakzzzmwn9")))

(define-public crate-surge-timeunit-0.1.29-alpha.0 (c (n "surge-timeunit") (v "0.1.29-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.29-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.29-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.29-alpha.0") (d #t) (k 0)))) (h "0cdzl5jdy6qzhlb8a8caw0clrkj4lhhwc00s7qbfp76mcsr18wd9")))

(define-public crate-surge-timeunit-0.1.30-alpha.0 (c (n "surge-timeunit") (v "0.1.30-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.30-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.30-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.30-alpha.0") (d #t) (k 0)))) (h "1cyvbn8naj6b3mirwq89b9fj9z7m4mfd8z4w9iq1b8sqzw4q03m6")))

(define-public crate-surge-timeunit-0.1.31-alpha.0 (c (n "surge-timeunit") (v "0.1.31-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.31-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.31-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.31-alpha.0") (d #t) (k 0)))) (h "0m01qz28fnrlmqm07x9m6hhq76660ihw1z502k5zdv3v8w2gsxrg")))

(define-public crate-surge-timeunit-0.1.32-alpha.0 (c (n "surge-timeunit") (v "0.1.32-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.32-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.32-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.32-alpha.0") (d #t) (k 0)))) (h "198zjiw3i63k5b3apkyx61hmy3y6bh28c4jay5ngr4w8vag7s701")))

(define-public crate-surge-timeunit-0.1.33-alpha.0 (c (n "surge-timeunit") (v "0.1.33-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.33-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.33-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.33-alpha.0") (d #t) (k 0)))) (h "1kj7cparik2gdn29b5klihqym8lczq08161ccxxq7m5j954byxml")))

(define-public crate-surge-timeunit-0.1.36-alpha.0 (c (n "surge-timeunit") (v "0.1.36-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.36-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.36-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.36-alpha.0") (d #t) (k 0)))) (h "0sqzdkkg5dhb5vr5r0y59q1w61aan76lv77l1g54x8z1z1lh7mmr")))

(define-public crate-surge-timeunit-0.1.39-alpha.0 (c (n "surge-timeunit") (v "0.1.39-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.39-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.39-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.39-alpha.0") (d #t) (k 0)))) (h "0pjibxgmd7b72xzjnvn26plbg9f4zgvb4fja7slm0rifcvqgaaqi")))

(define-public crate-surge-timeunit-0.1.40-alpha.0 (c (n "surge-timeunit") (v "0.1.40-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.40-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.40-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.40-alpha.0") (d #t) (k 0)))) (h "0iab3crrsfsvk2izd5s57nbl7ql6645y4lxs61mg5d0bjjffzl65")))

(define-public crate-surge-timeunit-0.1.42-alpha.0 (c (n "surge-timeunit") (v "0.1.42-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.42-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.42-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.42-alpha.0") (d #t) (k 0)))) (h "1g48pjxssqmb32d65wkg9r947jw76vj4r7shkwf932g6bkhfj7lc")))

(define-public crate-surge-timeunit-0.2.0-alpha.0 (c (n "surge-timeunit") (v "0.2.0-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.0-alpha.0") (d #t) (k 0)))) (h "195qsg2ph94nwh8pa8l0zdg5yxpsxqh1jxin2gk0jp2vrqv7wccp")))

(define-public crate-surge-timeunit-0.2.3-alpha.0 (c (n "surge-timeunit") (v "0.2.3-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.3-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.2.3-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.3-alpha.0") (d #t) (k 0)))) (h "1k0kzmcszfch7qsxlnhxw6c886hsrxq0ki90wki9m8q9chvhyxfb")))

(define-public crate-surge-timeunit-0.2.4-alpha.0 (c (n "surge-timeunit") (v "0.2.4-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.4-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.2.4-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.4-alpha.0") (d #t) (k 0)))) (h "1pvdsvawvwidy0djffr1kc8kbsj55hsg8b1xj6yx78isy5x0m0im")))

(define-public crate-surge-timeunit-0.2.5-alpha.0 (c (n "surge-timeunit") (v "0.2.5-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.5-alpha.0") (d #t) (k 0)))) (h "1pin75s4i3br4m11nzl7wzz52jplhydhl9rlv9mxn1kfwilrfc6k")))

(define-public crate-surge-timeunit-0.2.6-alpha.0 (c (n "surge-timeunit") (v "0.2.6-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.6-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.2.6-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.6-alpha.0") (d #t) (k 0)))) (h "1acqn3isif5kmhlnqwps1q93z0zqv5x7xzzp1q6kd7ah8pn63idm")))

(define-public crate-surge-timeunit-0.2.8-alpha.0 (c (n "surge-timeunit") (v "0.2.8-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.8-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.8-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.2.8-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.8-alpha.0") (d #t) (k 0)))) (h "0ym07bzjdksk70vld920whvfs4h7lsm191i8wbllx5gbnav7fbz3")))

(define-public crate-surge-timeunit-0.2.9-alpha.0 (c (n "surge-timeunit") (v "0.2.9-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.9-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.9-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.2.9-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.9-alpha.0") (d #t) (k 0)))) (h "1qlc18jxpbnzf07683b8kxvd9bgk0x8ghgl703lykm1342bj5hs4")))

(define-public crate-surge-timeunit-0.2.11-alpha.0 (c (n "surge-timeunit") (v "0.2.11-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.11-alpha.0") (d #t) (k 0)))) (h "01ajivyhy8416p6s378n2mi4v2gid5jss73niv7qhn7y136lhgh1")))

(define-public crate-surge-timeunit-0.2.12-alpha.0 (c (n "surge-timeunit") (v "0.2.12-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.12-alpha.0") (d #t) (k 0)))) (h "13ria8l6av3niy4a5ii0ldcpanqvbrx16vzd38y0nhmwmm2ygqb7")))

