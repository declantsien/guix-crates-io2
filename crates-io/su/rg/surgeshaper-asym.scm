(define-module (crates-io su rg surgeshaper-asym) #:use-module (crates-io))

(define-public crate-surgeshaper-asym-0.1.32-alpha.0 (c (n "surgeshaper-asym") (v "0.1.32-alpha.0") (d (list (d (n "surge-filter") (r "^0.1.32-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.32-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.32-alpha.0") (d #t) (k 0)) (d (n "surge-tables") (r "^0.1.32-alpha.0") (d #t) (k 0)))) (h "04av6kiwz7874zqszxd3zqzvywq0ix71bzbnzh448xkflqfjanad")))

(define-public crate-surgeshaper-asym-0.1.33-alpha.0 (c (n "surgeshaper-asym") (v "0.1.33-alpha.0") (d (list (d (n "surge-filter") (r "^0.1.33-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.33-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.33-alpha.0") (d #t) (k 0)) (d (n "surge-tables") (r "^0.1.33-alpha.0") (d #t) (k 0)))) (h "00b1rgknwdvmrxla3w0q1d8wq901kjg8xzgnwzcb3a6k7g0l49bw")))

(define-public crate-surgeshaper-asym-0.1.40-alpha.0 (c (n "surgeshaper-asym") (v "0.1.40-alpha.0") (d (list (d (n "surge-filter") (r "^0.1.40-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.40-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.40-alpha.0") (d #t) (k 0)) (d (n "surge-tables") (r "^0.1.40-alpha.0") (d #t) (k 0)))) (h "042m3ajd8bgj31bchghag2bjbvrnb9z7h7k6zc61mxgizz1dajyp")))

(define-public crate-surgeshaper-asym-0.1.42-alpha.0 (c (n "surgeshaper-asym") (v "0.1.42-alpha.0") (d (list (d (n "surge-filter") (r "^0.1.42-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.42-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.42-alpha.0") (d #t) (k 0)) (d (n "surge-tables") (r "^0.1.42-alpha.0") (d #t) (k 0)))) (h "0jjxbjaplfjggj540qayvn40xq9azfqnzmh99px3mflcb955mwf9")))

(define-public crate-surgeshaper-asym-0.2.0-alpha.0 (c (n "surgeshaper-asym") (v "0.2.0-alpha.0") (d (list (d (n "surge-filter") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "surge-tables") (r "^0.2.0-alpha.0") (d #t) (k 0)))) (h "08z0xc5pqdmac0zhijvh693pnfmfnl9693253rfnqfhhksgj6wwl")))

(define-public crate-surgeshaper-asym-0.2.4-alpha.0 (c (n "surgeshaper-asym") (v "0.2.4-alpha.0") (d (list (d (n "surge-filter") (r "^0.2.4-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.4-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.2.4-alpha.0") (d #t) (k 0)) (d (n "surge-tables") (r "^0.2.4-alpha.0") (d #t) (k 0)))) (h "1l563wl3g4c0agvx8966acvy0hvigd6yg0kdrbwjr3qjv34qmkl2")))

(define-public crate-surgeshaper-asym-0.2.5-alpha.0 (c (n "surgeshaper-asym") (v "0.2.5-alpha.0") (d (list (d (n "surge-filter") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-tables") (r "^0.2.5-alpha.0") (d #t) (k 0)))) (h "1inxsp7asjljh90jg2ns6g2nh263n2kg4hb8slm3bp0nagzvl5lq")))

(define-public crate-surgeshaper-asym-0.2.11-alpha.0 (c (n "surgeshaper-asym") (v "0.2.11-alpha.0") (d (list (d (n "surge-filter") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-tables") (r "^0.2.11-alpha.0") (d #t) (k 0)))) (h "1n7xi8qh1q71rdc81ar6l23y2hyaddrpafxk1lwpyrcxn79x91i5")))

(define-public crate-surgeshaper-asym-0.2.12-alpha.0 (c (n "surgeshaper-asym") (v "0.2.12-alpha.0") (d (list (d (n "surge-filter") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-tables") (r "^0.2.12-alpha.0") (d #t) (k 0)))) (h "1chckg9m0qwhcxnnaxcgwz463nyaj0dps9mskgk8wvcb9zbcd7km")))

