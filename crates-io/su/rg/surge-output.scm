(define-module (crates-io su rg surge-output) #:use-module (crates-io))

(define-public crate-surge-output-0.1.20-alpha.0 (c (n "surge-output") (v "0.1.20-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.20-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.20-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.20-alpha.0") (d #t) (k 0)))) (h "1m5q9qn63m0fpl485y28v75qmg04skvfz6w601n7cg2h0m15h3dr")))

(define-public crate-surge-output-0.1.23-alpha.0 (c (n "surge-output") (v "0.1.23-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.23-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.23-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.23-alpha.0") (d #t) (k 0)))) (h "1g6ikxlk9k5n4mrjra1m0q2771zklc465gqszsxr5s68h7y66z11")))

(define-public crate-surge-output-0.1.24-alpha.0 (c (n "surge-output") (v "0.1.24-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.24-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.24-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.24-alpha.0") (d #t) (k 0)))) (h "0scv585m58qrwy65z7pa13mb71qqlx3fzzh05i6d4x25ccfhl1ib")))

(define-public crate-surge-output-0.1.26-alpha.0 (c (n "surge-output") (v "0.1.26-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.26-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.26-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.26-alpha.0") (d #t) (k 0)))) (h "08zm666rrml4kh2h6bjbsfg3aqn4hj0b838m08frhwadplpwy8vq")))

(define-public crate-surge-output-0.1.27-alpha.0 (c (n "surge-output") (v "0.1.27-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.27-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.27-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.27-alpha.0") (d #t) (k 0)))) (h "09nvdy7cyxhkhdmi4rbwxa9406ihiv1ali1xwc5hh8yni5ms4y8j")))

(define-public crate-surge-output-0.1.28-alpha.0 (c (n "surge-output") (v "0.1.28-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.28-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.28-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.28-alpha.0") (d #t) (k 0)))) (h "1b6i49kxxiz007lvg3xawvqjnwagywc8ws2ynp7ix9nb1dm9768c")))

(define-public crate-surge-output-0.1.29-alpha.0 (c (n "surge-output") (v "0.1.29-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.29-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.29-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.29-alpha.0") (d #t) (k 0)))) (h "155w7ghprggbvikm2y9kara519p6a7jq8dgc6b424ng430zfjzx3")))

(define-public crate-surge-output-0.1.30-alpha.0 (c (n "surge-output") (v "0.1.30-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.30-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.30-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.30-alpha.0") (d #t) (k 0)))) (h "1m868f4nzxviqb1v41wmqhairz2ncgb1n4885z35rlrwalfazh9y")))

(define-public crate-surge-output-0.1.31-alpha.0 (c (n "surge-output") (v "0.1.31-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.31-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.31-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.31-alpha.0") (d #t) (k 0)))) (h "1j7aq14vrjl6zhbncbl4jy4la8r2bp27qwag7l1f02zr6m6kx5rv")))

(define-public crate-surge-output-0.1.32-alpha.0 (c (n "surge-output") (v "0.1.32-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.32-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.32-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.32-alpha.0") (d #t) (k 0)))) (h "06nija4kygrd906601nr4h9vqj71xwbb49n889ji1bc9pp458x5h")))

(define-public crate-surge-output-0.1.33-alpha.0 (c (n "surge-output") (v "0.1.33-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.33-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.33-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.33-alpha.0") (d #t) (k 0)))) (h "0rps6fnw9srjv9azgwicwdynar4ayij2ln1zzsq0hx4rhvvgzaf0")))

(define-public crate-surge-output-0.1.36-alpha.0 (c (n "surge-output") (v "0.1.36-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.36-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.36-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.36-alpha.0") (d #t) (k 0)))) (h "0pyans5aspxad9f6bng90brh4ql5fa3zpwc1i6zrgkc6zgv8d0sy")))

(define-public crate-surge-output-0.1.39-alpha.0 (c (n "surge-output") (v "0.1.39-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.39-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.39-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.39-alpha.0") (d #t) (k 0)))) (h "1gjwmgzlpq9h5f6nq8ssyvaqhc4rn1bm60fc5ls03fc88faq1pp0")))

(define-public crate-surge-output-0.1.40-alpha.0 (c (n "surge-output") (v "0.1.40-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.40-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.40-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.40-alpha.0") (d #t) (k 0)))) (h "0ff3pvvhfz14nyafnj7cc8dkywz8xvxh1mxmahij9szjrvyl0azh")))

(define-public crate-surge-output-0.1.42-alpha.0 (c (n "surge-output") (v "0.1.42-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.42-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.42-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.42-alpha.0") (d #t) (k 0)))) (h "1wpqc1znjvnli2323zijxb20zgcv9x23wh1m0xi85dkx47yxkwzz")))

(define-public crate-surge-output-0.2.0-alpha.0 (c (n "surge-output") (v "0.2.0-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.0-alpha.0") (d #t) (k 0)))) (h "1fm052zzspwkw33vibglpgiv3215lj7ha13ya1ias1gksjnar97f")))

(define-public crate-surge-output-0.2.4-alpha.0 (c (n "surge-output") (v "0.2.4-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.2.4-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.2.4-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.4-alpha.0") (d #t) (k 0)))) (h "18q2616maiqysai74j35bvxf8ifmgm2iwrwklzzr25wzyrdn5zkr")))

(define-public crate-surge-output-0.2.5-alpha.0 (c (n "surge-output") (v "0.2.5-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.5-alpha.0") (d #t) (k 0)))) (h "123c4rx6vxsh49r7cgd8zkbzrmagrkn5h6whhnxwk8m3f6i4fp36")))

(define-public crate-surge-output-0.2.6-alpha.0 (c (n "surge-output") (v "0.2.6-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.6-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.6-alpha.0") (d #t) (k 0)))) (h "14xh8f9w3xddfya59j089s3w09ryw6a2ncdr1g14n466j7043ys7")))

(define-public crate-surge-output-0.2.11-alpha.0 (c (n "surge-output") (v "0.2.11-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.11-alpha.0") (d #t) (k 0)))) (h "0fm3620fx8kj8hs3w7zq59n2yrby0wb0g8qlmp436kqdf61k2kwa")))

(define-public crate-surge-output-0.2.12-alpha.0 (c (n "surge-output") (v "0.2.12-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.12-alpha.0") (d #t) (k 0)))) (h "0gl180vmwcbadfsbd7zv3x4hyysq4wbwr9p31yag6231ywajk95s")))

