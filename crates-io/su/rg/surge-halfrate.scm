(define-module (crates-io su rg surge-halfrate) #:use-module (crates-io))

(define-public crate-surge-halfrate-0.1.17-alpha.0 (c (n "surge-halfrate") (v "0.1.17-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.17-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.17-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.17-alpha.0") (d #t) (k 0)))) (h "0xh09qkzihkmsz8qifkvh3x0nd9r2lvaskzfz6zsdqn1ndmncngq")))

(define-public crate-surge-halfrate-0.1.20-alpha.0 (c (n "surge-halfrate") (v "0.1.20-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.20-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.20-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.20-alpha.0") (d #t) (k 0)))) (h "0p8cxzlbck1fpm7ayhwqxynrc3br51558x6gjsjh9bkwyk8xsjdw")))

(define-public crate-surge-halfrate-0.1.23-alpha.0 (c (n "surge-halfrate") (v "0.1.23-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.23-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.23-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.23-alpha.0") (d #t) (k 0)))) (h "0fg0vffsv7zjz26037a097s23rv74r0d48b8j552l2m0nbz44800")))

(define-public crate-surge-halfrate-0.1.24-alpha.0 (c (n "surge-halfrate") (v "0.1.24-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.24-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.24-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.24-alpha.0") (d #t) (k 0)))) (h "0w9cl4whxbzpz0vq80qky12332ddxzx2z9mvjnxrc3dlz0hz61y2")))

(define-public crate-surge-halfrate-0.1.26-alpha.0 (c (n "surge-halfrate") (v "0.1.26-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.26-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.26-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.26-alpha.0") (d #t) (k 0)))) (h "1nvyp9adpqlbdy6r78za6zi1naqvs2v2g5js1j83fxs0vdr3cp1z")))

(define-public crate-surge-halfrate-0.1.27-alpha.0 (c (n "surge-halfrate") (v "0.1.27-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.27-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.27-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.27-alpha.0") (d #t) (k 0)))) (h "09981n89ax7cai51qqcvyh2z8n6q0l3gid9wg54v7wzf8hc8fgln")))

(define-public crate-surge-halfrate-0.1.28-alpha.0 (c (n "surge-halfrate") (v "0.1.28-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.28-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.28-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.28-alpha.0") (d #t) (k 0)))) (h "0x8k8dscqyvhzgfdb2878yr880jhk5kbkk9qharz6yzm79y82xzw")))

(define-public crate-surge-halfrate-0.1.29-alpha.0 (c (n "surge-halfrate") (v "0.1.29-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.29-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.29-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.29-alpha.0") (d #t) (k 0)))) (h "1krc97w3mik5h9hjd06bc2psc7a7q40sxjh0gfsr4brjpk3h5km9")))

(define-public crate-surge-halfrate-0.1.30-alpha.0 (c (n "surge-halfrate") (v "0.1.30-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.30-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.30-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.30-alpha.0") (d #t) (k 0)))) (h "125qyrnjsiki098linzjli46yw0nqyg34d7ry8j5p05cld3fsc3k")))

(define-public crate-surge-halfrate-0.1.31-alpha.0 (c (n "surge-halfrate") (v "0.1.31-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.31-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.31-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.31-alpha.0") (d #t) (k 0)))) (h "014g56150z065jv18zw0i3vwkwf30fm2j9j0gvaicykwhihax2yb")))

(define-public crate-surge-halfrate-0.1.32-alpha.0 (c (n "surge-halfrate") (v "0.1.32-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.32-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.32-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.32-alpha.0") (d #t) (k 0)))) (h "1hcj1f3xr28zaiwm62r3dqxya1ya9fi3y5kfy72ywzbhb0l3jryi")))

(define-public crate-surge-halfrate-0.1.33-alpha.0 (c (n "surge-halfrate") (v "0.1.33-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.33-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.33-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.33-alpha.0") (d #t) (k 0)))) (h "1dyr0zscwhlg63nd3q9552mc5vrp8bpkwm8najspw0wk1n9z5ggc")))

(define-public crate-surge-halfrate-0.1.36-alpha.0 (c (n "surge-halfrate") (v "0.1.36-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.36-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.36-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.36-alpha.0") (d #t) (k 0)))) (h "1rayl76z305qwjm6hdr6jcjixkyaz3gasb4ly6s44b4yg8k2q5vx")))

(define-public crate-surge-halfrate-0.1.39-alpha.0 (c (n "surge-halfrate") (v "0.1.39-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.39-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.39-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.39-alpha.0") (d #t) (k 0)))) (h "1baq71aagqg6l71lmnqxvxsiijpjh2kxcvb8xsxv4k5wahid3rj5")))

(define-public crate-surge-halfrate-0.1.40-alpha.0 (c (n "surge-halfrate") (v "0.1.40-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.40-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.40-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.40-alpha.0") (d #t) (k 0)))) (h "14flk6d9sldb4wdy1rg1bk5bpxf70sam986scjnlwic5skamjmrs")))

(define-public crate-surge-halfrate-0.1.42-alpha.0 (c (n "surge-halfrate") (v "0.1.42-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.42-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.42-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.42-alpha.0") (d #t) (k 0)))) (h "0q7xndskf51j8dfkgaki70xd66qddccdzn8cyj70cks332gzi66x")))

(define-public crate-surge-halfrate-0.2.0-alpha.0 (c (n "surge-halfrate") (v "0.2.0-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.0-alpha.0") (d #t) (k 0)))) (h "1dnxrh9r4xp92b10cdphcm2cmi00r1iygpp3l0ddaad1h8hk43hl")))

(define-public crate-surge-halfrate-0.2.3-alpha.0 (c (n "surge-halfrate") (v "0.2.3-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.2.3-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.3-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.3-alpha.0") (d #t) (k 0)))) (h "015qb0gy19bj3ryszj4wlg6jda38l0q4464jwivb2vg9rlk93lam")))

(define-public crate-surge-halfrate-0.2.4-alpha.0 (c (n "surge-halfrate") (v "0.2.4-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.2.4-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.4-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.4-alpha.0") (d #t) (k 0)))) (h "0472l6avmgdxw8rxhbqn4y3lvs32nsmi6fizz0j1csh3p68l8c8m")))

(define-public crate-surge-halfrate-0.2.5-alpha.0 (c (n "surge-halfrate") (v "0.2.5-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.5-alpha.0") (d #t) (k 0)))) (h "17k16c0r42q5rb556iwdgc1bckczxwzhzvsrfhfi05zkxd0zycr1")))

(define-public crate-surge-halfrate-0.2.6-alpha.0 (c (n "surge-halfrate") (v "0.2.6-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.6-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.6-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.6-alpha.0") (d #t) (k 0)))) (h "0ybpn2w6qwwr31697cxkzym7ml6sss1q4pj53dw02iy3z5x2ymjr")))

(define-public crate-surge-halfrate-0.2.9-alpha.0 (c (n "surge-halfrate") (v "0.2.9-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.9-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.9-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.9-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.9-alpha.0") (d #t) (k 0)))) (h "1033flpfp3bsnqb8fm2zaj11xf9mqkl7zl7lr9189ihv862valfn")))

(define-public crate-surge-halfrate-0.2.11-alpha.0 (c (n "surge-halfrate") (v "0.2.11-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.11-alpha.0") (d #t) (k 0)))) (h "1bwmx6iwx2yg4hl0pr00i0apik274ywnxb3850xbfhh5snkwgs5g")))

(define-public crate-surge-halfrate-0.2.12-alpha.0 (c (n "surge-halfrate") (v "0.2.12-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.12-alpha.0") (d #t) (k 0)))) (h "12jmvqk5ycf3dv9w2iff9rpdvr974f4prkvlyj38lbc4znyqsjg4")))

