(define-module (crates-io su rg surge-constants) #:use-module (crates-io))

(define-public crate-surge-constants-0.1.10-alpha.0 (c (n "surge-constants") (v "0.1.10-alpha.0") (h "0s28dalvdldc2j4dmlv4pjqn0zm12aq7q246pvrzdkr340ibz012") (y #t)))

(define-public crate-surge-constants-0.1.11-alpha.0 (c (n "surge-constants") (v "0.1.11-alpha.0") (h "0pln6l3hd5n1vpl8799y4dy0g6nipx1lzchbscql4wm9afswrd4r") (y #t)))

(define-public crate-surge-constants-0.1.12-alpha.0 (c (n "surge-constants") (v "0.1.12-alpha.0") (h "11pn1fv0jl0a2ddiyxn70j9j3bhqh8r2y0ndq4ajvwg35s1aqvhx") (y #t)))

(define-public crate-surge-constants-0.1.13-alpha.0 (c (n "surge-constants") (v "0.1.13-alpha.0") (h "147jiwk5w61s8qm008llb30c2hfkfv7qn9d48dq7hkss4yz9whjk") (y #t)))

(define-public crate-surge-constants-0.1.14-alpha.0 (c (n "surge-constants") (v "0.1.14-alpha.0") (h "0js14c23y8h1q0g7db9km83bdgjibi4pll713hnmp6dxq6f8f85p") (y #t)))

(define-public crate-surge-constants-0.1.15-alpha.0 (c (n "surge-constants") (v "0.1.15-alpha.0") (h "1zf8cqk7fy8jli4w5i6kk1m0sq1y5fw967sg50q8sr844p0jidp3") (y #t)))

(define-public crate-surge-constants-0.1.16-alpha.0 (c (n "surge-constants") (v "0.1.16-alpha.0") (h "0ga429nciqhlifw2p7gz9m7x7x3drbk9hn3vjaamnp6lfqk5fm7q") (y #t)))

(define-public crate-surge-constants-0.1.17-alpha.0 (c (n "surge-constants") (v "0.1.17-alpha.0") (h "1chydn9jrirs00vrxpa63hjyzyh2szrlz7zgkiy61ib131dd03sk") (y #t)))

(define-public crate-surge-constants-0.1.19-alpha.0 (c (n "surge-constants") (v "0.1.19-alpha.0") (h "1rwcpji7pdilyn9yjjd4ai3cghgiz0rpwh4s3sqg4nq09kkc2vkx") (y #t)))

(define-public crate-surge-constants-0.1.20-alpha.0 (c (n "surge-constants") (v "0.1.20-alpha.0") (h "106cd1cj4nb9dlvvl4ak62qnp3fg9y076zlwkbbvqvnh4913fjay") (y #t)))

(define-public crate-surge-constants-0.1.21-alpha.0 (c (n "surge-constants") (v "0.1.21-alpha.0") (h "0jjymzjrnq5jm17hvcyfg3vmrrfq781zys3lc73j7b0gi8pzim61") (y #t)))

(define-public crate-surge-constants-0.1.23-alpha.0 (c (n "surge-constants") (v "0.1.23-alpha.0") (h "1qljmhirvygdqbqan53lrbk22xs5446iwq8wpmf52mpaw9x1qrzr") (y #t)))

(define-public crate-surge-constants-0.1.24-alpha.0 (c (n "surge-constants") (v "0.1.24-alpha.0") (h "1s11yvwbs8947a3qij7hdjqnnih4jgbc59m6ldrs9927r0x4l07d") (y #t)))

(define-public crate-surge-constants-0.1.26-alpha.0 (c (n "surge-constants") (v "0.1.26-alpha.0") (h "0i18zlg2yzja5cni5mabfynjbs804vm8h8y3aa26kr62j2af4ljz") (y #t)))

(define-public crate-surge-constants-0.1.27-alpha.0 (c (n "surge-constants") (v "0.1.27-alpha.0") (h "1rvbvp0841hmc3898h30mrm53j7fh84sl9z0wj74z9l8fm6rvia0") (y #t)))

(define-public crate-surge-constants-0.1.28-alpha.0 (c (n "surge-constants") (v "0.1.28-alpha.0") (h "06isw379qmc2p3bqs2hs5g7gqpp8fqprr8ywf0lsjd64d1vhrv2s") (y #t)))

(define-public crate-surge-constants-0.1.29-alpha.0 (c (n "surge-constants") (v "0.1.29-alpha.0") (h "068b8nv1qzknb558rnmc50yb0zm7xjf55nkq8nm6xjai2rljrass") (y #t)))

(define-public crate-surge-constants-0.1.30-alpha.0 (c (n "surge-constants") (v "0.1.30-alpha.0") (h "0qvimznfr28pknamglk4i70pvzrkyr3l4r77yg0fq54d8m7vnr11") (y #t)))

(define-public crate-surge-constants-0.1.31-alpha.0 (c (n "surge-constants") (v "0.1.31-alpha.0") (h "13fhvb323hhnhgx8c9s6ff1fh5xv9cw2zmrhdyrzzgx65nfyyvx3") (y #t)))

(define-public crate-surge-constants-0.1.32-alpha.0 (c (n "surge-constants") (v "0.1.32-alpha.0") (h "1i21sg5j9gm1saa619nsaglr54l6jp4wifdc1l6b5v2085jd6vfh") (y #t)))

(define-public crate-surge-constants-0.1.33-alpha.0 (c (n "surge-constants") (v "0.1.33-alpha.0") (h "1gdnqrxza2lixi28643x22hh90mn8z8rjvfjh0q97zwrhq254a9k") (y #t)))

(define-public crate-surge-constants-0.1.36-alpha.0 (c (n "surge-constants") (v "0.1.36-alpha.0") (h "1il8dklg8ly4x9ip34f3m1yv0nn5vw9pd9hm0kbhl24p0ivdp3b5") (y #t)))

(define-public crate-surge-constants-0.1.37-alpha.0 (c (n "surge-constants") (v "0.1.37-alpha.0") (h "1g5n0h3ddwkkgj7ww71fr5fp0y7934zqis2f165msypbn2fidiz1") (y #t)))

(define-public crate-surge-constants-0.1.39-alpha.0 (c (n "surge-constants") (v "0.1.39-alpha.0") (h "1nw7ny1kj92icpvj72vyzjcmb6a4pli20avvrcanj4wi23csihly") (y #t)))

(define-public crate-surge-constants-0.1.40-alpha.0 (c (n "surge-constants") (v "0.1.40-alpha.0") (h "07bcvvzd62l09pk4insyjsm4fdp6iplcpwwg2gilzvqbiglkhmzc") (y #t)))

(define-public crate-surge-constants-0.1.42-alpha.0 (c (n "surge-constants") (v "0.1.42-alpha.0") (h "0cxd5pvds0jill0fid31lp98kk8b38y8xq48k94gfx373j4rsmdn") (y #t)))

(define-public crate-surge-constants-0.2.0-alpha.0 (c (n "surge-constants") (v "0.2.0-alpha.0") (h "1i188fgfwpjqbzmr856jsbh6hj1iwgad2vady1acqnbvil7crbj8")))

(define-public crate-surge-constants-0.2.2-alpha.0 (c (n "surge-constants") (v "0.2.2-alpha.0") (h "1m4hx3p0x702868396j4zr8h13dqw85h85q5i0d1hv0pmi9gj0fl")))

(define-public crate-surge-constants-0.2.3-alpha.0 (c (n "surge-constants") (v "0.2.3-alpha.0") (h "02pjd5747z9xd0kgwbiv60rnsfn70bkxc442mzl65n46p3344n77")))

(define-public crate-surge-constants-0.2.4-alpha.0 (c (n "surge-constants") (v "0.2.4-alpha.0") (h "19qab87bvccpbl2f43lr5fwx8y4yadqshsdkr254v0b7bsh9ih8n")))

(define-public crate-surge-constants-0.2.5-alpha.0 (c (n "surge-constants") (v "0.2.5-alpha.0") (h "0bl5cqq6g5w1qqpyyh6j8zlvry1k3ziry6yr91x5j9gklkp31pby")))

(define-public crate-surge-constants-0.2.6-alpha.0 (c (n "surge-constants") (v "0.2.6-alpha.0") (h "0if3ah8rshxmxnbyg3pyd3m6f98rpi5rf316znddgz58lk9jjy6g")))

(define-public crate-surge-constants-0.2.8-alpha.0 (c (n "surge-constants") (v "0.2.8-alpha.0") (h "0fl2qng22ybbpcjnlbg6kbvfxcwppzr9sakrrsl05qvb1hhifzax")))

(define-public crate-surge-constants-0.2.9-alpha.0 (c (n "surge-constants") (v "0.2.9-alpha.0") (h "04m07c0r3a4ssl70z4aim4accycjxhsm8vkx4hhamjbdk5h39jz6")))

(define-public crate-surge-constants-0.2.11-alpha.0 (c (n "surge-constants") (v "0.2.11-alpha.0") (h "1zs46gkr0b3705b29wr0yw39cr8glkk7kklb6jxp297znjbl6jmh")))

(define-public crate-surge-constants-0.2.12-alpha.0 (c (n "surge-constants") (v "0.2.12-alpha.0") (h "1rm8bzl0zazdjvvmrpdldf5dd6whx16avgk5yjdrjrv9gji4awpb")))

