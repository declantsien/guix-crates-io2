(define-module (crates-io su rg surge-lag) #:use-module (crates-io))

(define-public crate-surge-lag-0.1.12-alpha.0 (c (n "surge-lag") (v "0.1.12-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.12-alpha.0") (d #t) (k 0)))) (h "09m66lrhi1679q6m3jgbc9mp6iq04dx2d9xxc9mdsyb7ic0y3mqm")))

(define-public crate-surge-lag-0.1.14-alpha.0 (c (n "surge-lag") (v "0.1.14-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.14-alpha.0") (d #t) (k 0)))) (h "175vcvj4df4bs2y880pjvzm5if6g78n9b2vd9296qx492kcdf989")))

(define-public crate-surge-lag-0.1.15-alpha.0 (c (n "surge-lag") (v "0.1.15-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.15-alpha.0") (d #t) (k 0)))) (h "0jxgwjz8l7jbhgilyld5nqp2jgn8wdy95zviqfncgnsw17kwkba7")))

(define-public crate-surge-lag-0.1.16-alpha.0 (c (n "surge-lag") (v "0.1.16-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.16-alpha.0") (d #t) (k 0)))) (h "0gi2qpcf7lpiawvjgsyhlyl6andin8d8cqxi6gp3nyy3a3a1cj3d")))

(define-public crate-surge-lag-0.1.17-alpha.0 (c (n "surge-lag") (v "0.1.17-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.17-alpha.0") (d #t) (k 0)))) (h "1cbz84m7jfd0bcqrgbd2qm89ppprhacbjxl0x6crsik09s6nhm9s")))

(define-public crate-surge-lag-0.1.20-alpha.0 (c (n "surge-lag") (v "0.1.20-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.20-alpha.0") (d #t) (k 0)))) (h "0ab9wlbd7jvs3wcg74mkfyr712vicbpcrj5k6majp3vba513x6p0")))

(define-public crate-surge-lag-0.1.23-alpha.0 (c (n "surge-lag") (v "0.1.23-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.23-alpha.0") (d #t) (k 0)))) (h "0wyxnyx5rnmk55m3fi75jqxkdsac8174zfx31dps6mbwsvhsj4kk")))

(define-public crate-surge-lag-0.1.24-alpha.0 (c (n "surge-lag") (v "0.1.24-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.24-alpha.0") (d #t) (k 0)))) (h "0dqs731xr3ljvz1b0ldda9hg2qw3hfgxv2h18kx71h79rgf4a0k9")))

(define-public crate-surge-lag-0.1.26-alpha.0 (c (n "surge-lag") (v "0.1.26-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.26-alpha.0") (d #t) (k 0)))) (h "02n49x0z8aa3r6n118mj7sb3jrww1pidgh884svdnazxdcfql8i1")))

(define-public crate-surge-lag-0.1.27-alpha.0 (c (n "surge-lag") (v "0.1.27-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.27-alpha.0") (d #t) (k 0)))) (h "1nf1dshv161385nrm9lclqvla4dgiqkgx0pqf6m0jblskh1nhq0k")))

(define-public crate-surge-lag-0.1.28-alpha.0 (c (n "surge-lag") (v "0.1.28-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.28-alpha.0") (d #t) (k 0)))) (h "0s4dhz3k3322j15ahjwld268hjw5jv0d2sp9pds5c9n7f2r877s1")))

(define-public crate-surge-lag-0.1.29-alpha.0 (c (n "surge-lag") (v "0.1.29-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.29-alpha.0") (d #t) (k 0)))) (h "0vcg1qg2gjxc6543bj6r48vbi9lsig3lay7rranqrl5qhkaiby06")))

(define-public crate-surge-lag-0.1.30-alpha.0 (c (n "surge-lag") (v "0.1.30-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.30-alpha.0") (d #t) (k 0)))) (h "0yawwr9wf5szra7ydy83cmclim8lni55g3xyl6vkp2bwmfjc0r4h")))

(define-public crate-surge-lag-0.1.31-alpha.0 (c (n "surge-lag") (v "0.1.31-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.31-alpha.0") (d #t) (k 0)))) (h "0nm47hsks4iizdqban8s5297qrpkyl03jfhvky543m23gp91br92")))

(define-public crate-surge-lag-0.1.32-alpha.0 (c (n "surge-lag") (v "0.1.32-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.32-alpha.0") (d #t) (k 0)))) (h "1gq2mvnaf9s94knbgbkdv6zzjsxr539px21xsjck6dvgcflsg158")))

(define-public crate-surge-lag-0.1.33-alpha.0 (c (n "surge-lag") (v "0.1.33-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.33-alpha.0") (d #t) (k 0)))) (h "0mykrwng7233rz36y3vlpg93vqhg70xywjcf75mcj1m1q7yfqfz0")))

(define-public crate-surge-lag-0.1.36-alpha.0 (c (n "surge-lag") (v "0.1.36-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.36-alpha.0") (d #t) (k 0)))) (h "17s0r2xr9b51psc0py08d43lhx9q6zy1hzgdghqcd6b6icf4v3wj")))

(define-public crate-surge-lag-0.1.39-alpha.0 (c (n "surge-lag") (v "0.1.39-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.39-alpha.0") (d #t) (k 0)))) (h "092vq3yvi18924wkskw4llbskx22fi31nvggg45fy1s2n5ghfh03")))

(define-public crate-surge-lag-0.1.40-alpha.0 (c (n "surge-lag") (v "0.1.40-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.40-alpha.0") (d #t) (k 0)))) (h "1ciw39r1p1rz2156391qpgwk3xc0lfm0fv4mbbz087b2dc3014fv")))

(define-public crate-surge-lag-0.1.42-alpha.0 (c (n "surge-lag") (v "0.1.42-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.42-alpha.0") (d #t) (k 0)))) (h "1v7w5f71nrk53cimpldyh56i8g6vm73df8di8ysi5bgcq73j6yfb")))

(define-public crate-surge-lag-0.2.0-alpha.0 (c (n "surge-lag") (v "0.2.0-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.0-alpha.0") (d #t) (k 0)))) (h "0s4bby65wh765p5x9kk50a8v03pvbk0ahpl2yhznwpbzzycvpf6w")))

(define-public crate-surge-lag-0.2.3-alpha.0 (c (n "surge-lag") (v "0.2.3-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.3-alpha.0") (d #t) (k 0)))) (h "0j1bm3byij2b710dmrp6g92n4fi8wsgikzd0251sd9irqjww46qi")))

(define-public crate-surge-lag-0.2.4-alpha.0 (c (n "surge-lag") (v "0.2.4-alpha.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.4-alpha.0") (d #t) (k 0)))) (h "057qzbfks7fd2xbnn7dzqr0i06n7y1hhs0sgp50c4bxkavfp71z8")))

(define-public crate-surge-lag-0.2.5-alpha.0 (c (n "surge-lag") (v "0.2.5-alpha.0") (d (list (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.5-alpha.0") (d #t) (k 0)))) (h "0n7xnj5fhb0jz48wg9l60idblgc4qhxmzk6p8829h6dgripqpg54")))

(define-public crate-surge-lag-0.2.6-alpha.0 (c (n "surge-lag") (v "0.2.6-alpha.0") (d (list (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.6-alpha.0") (d #t) (k 0)))) (h "00afx5y29gh97zi3rkjpb117pgqrnqf0pqg1b4rjbcykvxcbd8s1")))

(define-public crate-surge-lag-0.2.8-alpha.0 (c (n "surge-lag") (v "0.2.8-alpha.0") (d (list (d (n "surge-imports") (r "^0.2.8-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.8-alpha.0") (d #t) (k 0)))) (h "18mv43d64q4zp00045wk13dfmll8sn1rhmmfwvm6fgasg6dfrd1r")))

(define-public crate-surge-lag-0.2.9-alpha.0 (c (n "surge-lag") (v "0.2.9-alpha.0") (d (list (d (n "surge-imports") (r "^0.2.9-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.9-alpha.0") (d #t) (k 0)))) (h "148y73ml26hlhxg2k07cdis4hq6vqscn4zi1qik8ycf11c2jdir4")))

(define-public crate-surge-lag-0.2.11-alpha.0 (c (n "surge-lag") (v "0.2.11-alpha.0") (d (list (d (n "surge-imports") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.11-alpha.0") (d #t) (k 0)))) (h "03rypcy1cjjnap8057xdr18hmwxqd01wzwhs9435vaq0ykc3id1a")))

(define-public crate-surge-lag-0.2.12-alpha.0 (c (n "surge-lag") (v "0.2.12-alpha.0") (d (list (d (n "surge-imports") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.12-alpha.0") (d #t) (k 0)))) (h "0511rl0m7cfb0hg4w1lkzrnrpng6512m81cpb5nl02ksd6i7dq5q")))

