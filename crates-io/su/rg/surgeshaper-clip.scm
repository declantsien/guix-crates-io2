(define-module (crates-io su rg surgeshaper-clip) #:use-module (crates-io))

(define-public crate-surgeshaper-clip-0.1.33-alpha.0 (c (n "surgeshaper-clip") (v "0.1.33-alpha.0") (d (list (d (n "surge-filter") (r "^0.1.33-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.33-alpha.0") (d #t) (k 0)))) (h "0gw09ps486iizvmlfd6lppicf9c6835zpsdkhxqbvaw4q0r2f39j")))

(define-public crate-surgeshaper-clip-0.1.40-alpha.0 (c (n "surgeshaper-clip") (v "0.1.40-alpha.0") (d (list (d (n "surge-filter") (r "^0.1.40-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.40-alpha.0") (d #t) (k 0)))) (h "0s8ilmdvxim7m3kn9qlxnd9pkmdd610i2r42ximlwbvf268a587d")))

(define-public crate-surgeshaper-clip-0.1.42-alpha.0 (c (n "surgeshaper-clip") (v "0.1.42-alpha.0") (d (list (d (n "surge-filter") (r "^0.1.42-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.42-alpha.0") (d #t) (k 0)))) (h "0rzg93bbqmdhjnpjbkdd0bss2yl36anc1mwj6vh0y071y3l0s7y5")))

(define-public crate-surgeshaper-clip-0.2.0-alpha.0 (c (n "surgeshaper-clip") (v "0.2.0-alpha.0") (d (list (d (n "surge-filter") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.0-alpha.0") (d #t) (k 0)))) (h "0axx62mqbx2cy8pg0l9fnpqh3maql4kaw53cag8sj7dr0y2rsiya")))

(define-public crate-surgeshaper-clip-0.2.4-alpha.0 (c (n "surgeshaper-clip") (v "0.2.4-alpha.0") (d (list (d (n "surge-filter") (r "^0.2.4-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.4-alpha.0") (d #t) (k 0)))) (h "1l57wwskdd1dhf72hqcrzn5rd1xfn19d1i3qshxhy6wivah4d50i")))

(define-public crate-surgeshaper-clip-0.2.5-alpha.0 (c (n "surgeshaper-clip") (v "0.2.5-alpha.0") (d (list (d (n "surge-filter") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.5-alpha.0") (d #t) (k 0)))) (h "0kgfsnskb8x398nkhfm9zzbsrn6mpbi32nxipzsjzx7zf766xmnr")))

(define-public crate-surgeshaper-clip-0.2.11-alpha.0 (c (n "surgeshaper-clip") (v "0.2.11-alpha.0") (d (list (d (n "surge-filter") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.11-alpha.0") (d #t) (k 0)))) (h "0h24b8ygwhb39f19zkyp2p4a7s1a3546vlzkz472pw3xm63x50bb")))

(define-public crate-surgeshaper-clip-0.2.12-alpha.0 (c (n "surgeshaper-clip") (v "0.2.12-alpha.0") (d (list (d (n "surge-filter") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.12-alpha.0") (d #t) (k 0)))) (h "0750zji602i0wfi512iw0xmrwgf4fcs9xnxdsg7xr3nfhip4f4l0")))

