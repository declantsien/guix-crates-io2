(define-module (crates-io su rg surge-blitter) #:use-module (crates-io))

(define-public crate-surge-blitter-0.1.14-alpha.0 (c (n "surge-blitter") (v "0.1.14-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.14-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.14-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.14-alpha.0") (d #t) (k 0)))) (h "0bfxgpw7f9xqfy2zsa23nzind1bckvaqmrg4w6vnc5h2wz383mp7")))

(define-public crate-surge-blitter-0.1.15-alpha.0 (c (n "surge-blitter") (v "0.1.15-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.15-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.15-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.15-alpha.0") (d #t) (k 0)))) (h "16xhnpnyd081ijiwn7mz2dqgz7nb2pljsz8bhdmw38wg1bgx14zf")))

(define-public crate-surge-blitter-0.1.16-alpha.0 (c (n "surge-blitter") (v "0.1.16-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.16-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.16-alpha.0") (d #t) (k 0)))) (h "1q71lbarr28jh0z83i3csmnmrjif02awcgdq01rp8vli65bk7xsx")))

(define-public crate-surge-blitter-0.1.17-alpha.0 (c (n "surge-blitter") (v "0.1.17-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.17-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.17-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.17-alpha.0") (d #t) (k 0)))) (h "1m0sj9hx7pq3bkjdng2r0k3pscwfjvhmsss2a7l5rgmbm51yzm6q")))

(define-public crate-surge-blitter-0.1.20-alpha.0 (c (n "surge-blitter") (v "0.1.20-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.20-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.20-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.20-alpha.0") (d #t) (k 0)))) (h "1fwjj0lv8w09wrqf23px0pp8ki0b2hjnmqggqviwzzh248728319")))

(define-public crate-surge-blitter-0.1.23-alpha.0 (c (n "surge-blitter") (v "0.1.23-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.23-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.23-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.23-alpha.0") (d #t) (k 0)))) (h "1pymk2nn19wflhkcp8b7hm1njigq9zvby4ig31ii1i86jn9xiis2")))

(define-public crate-surge-blitter-0.1.24-alpha.0 (c (n "surge-blitter") (v "0.1.24-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.24-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.24-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.24-alpha.0") (d #t) (k 0)))) (h "08mhnbkpw57brcfa81901nvrapy80yxb5jmyrymnp55z75p7zdw3")))

(define-public crate-surge-blitter-0.1.26-alpha.0 (c (n "surge-blitter") (v "0.1.26-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.26-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.26-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.26-alpha.0") (d #t) (k 0)))) (h "1jsh2y16wqgwphxzwgwh67rz352f012i2s0ibx1iyrfkaaxxppp6")))

(define-public crate-surge-blitter-0.1.27-alpha.0 (c (n "surge-blitter") (v "0.1.27-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.27-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.27-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.27-alpha.0") (d #t) (k 0)))) (h "0qrbv04zgpkvwg83vnprj7d4a0qmw4790fi2mbbmg2vlgkh5qiwn")))

(define-public crate-surge-blitter-0.1.28-alpha.0 (c (n "surge-blitter") (v "0.1.28-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.28-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.28-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.28-alpha.0") (d #t) (k 0)))) (h "126fhiwavl9q9jx8xnj76q35ha3c5066f8dx1i0jfk0mi94dlzjk")))

(define-public crate-surge-blitter-0.1.29-alpha.0 (c (n "surge-blitter") (v "0.1.29-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.29-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.29-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.29-alpha.0") (d #t) (k 0)))) (h "04rrhdmwgn8zc3zzd2x7g1nxxrk88r978ibpnf2hlqmz8hzg2m0l")))

(define-public crate-surge-blitter-0.1.30-alpha.0 (c (n "surge-blitter") (v "0.1.30-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.30-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.30-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.30-alpha.0") (d #t) (k 0)))) (h "1l8qpw9gg0ijc8c27h7vg4cc8r1vc605rn1377mxdz9d44kyvra9")))

(define-public crate-surge-blitter-0.1.31-alpha.0 (c (n "surge-blitter") (v "0.1.31-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.31-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.31-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.31-alpha.0") (d #t) (k 0)))) (h "1lxff737mc2qqigfkn53vkfwsjhjgb3p38x290lad4vi7w4g72dd")))

(define-public crate-surge-blitter-0.1.32-alpha.0 (c (n "surge-blitter") (v "0.1.32-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.32-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.32-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.32-alpha.0") (d #t) (k 0)))) (h "04nq3gh32lmhc2knhxs6lv4cjnf6aqch6gfy299pvcqdhj7b04lp")))

(define-public crate-surge-blitter-0.1.33-alpha.0 (c (n "surge-blitter") (v "0.1.33-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.33-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.33-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.33-alpha.0") (d #t) (k 0)))) (h "11rf6hj094swazacmg9gyc4pwcmd9ar1g6sxq06fmjc2h3y2dpf4")))

(define-public crate-surge-blitter-0.1.36-alpha.0 (c (n "surge-blitter") (v "0.1.36-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.36-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.36-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.36-alpha.0") (d #t) (k 0)))) (h "09zkixznz4qzr9zjf4jz7cnfzwsayav6amy2mdwq3hb1jfsfkyyh")))

(define-public crate-surge-blitter-0.1.39-alpha.0 (c (n "surge-blitter") (v "0.1.39-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.39-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.39-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.39-alpha.0") (d #t) (k 0)))) (h "0096hb0jv5z3f5hfcynv0fpsjzj6kjnz1kcvgdc70244syfp34bj")))

(define-public crate-surge-blitter-0.1.40-alpha.0 (c (n "surge-blitter") (v "0.1.40-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.40-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.40-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.40-alpha.0") (d #t) (k 0)))) (h "1zli7g44ygmmxh9z14g3yql4qyrvjir6nq5chzw11zkply3va369")))

(define-public crate-surge-blitter-0.1.42-alpha.0 (c (n "surge-blitter") (v "0.1.42-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.42-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.1.42-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.1.42-alpha.0") (d #t) (k 0)))) (h "1fgbfrd31wnh3qqn4r6p6bidf980x3m0rqsljvy8iljnfcgl6yi1")))

(define-public crate-surge-blitter-0.2.0-alpha.0 (c (n "surge-blitter") (v "0.2.0-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.2.0-alpha.0") (d #t) (k 0)))) (h "03ab28phbqm53bjsjhx3rc63h22b6byn4khgvraay7m44yjahqg8")))

(define-public crate-surge-blitter-0.2.3-alpha.0 (c (n "surge-blitter") (v "0.2.3-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.2.3-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.3-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.2.3-alpha.0") (d #t) (k 0)))) (h "0g0r577abx5n8ck6qz44h54g6k7q3d38iqbxmzr601hjid64chxx")))

(define-public crate-surge-blitter-0.2.4-alpha.0 (c (n "surge-blitter") (v "0.2.4-alpha.0") (d (list (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "surge-constants") (r "^0.2.4-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.4-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.2.4-alpha.0") (d #t) (k 0)))) (h "1kl095mcr6nlbb1lqrbahg6mgxbl327fg694nxwbiswvrzwp25d3")))

(define-public crate-surge-blitter-0.2.5-alpha.0 (c (n "surge-blitter") (v "0.2.5-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.2.5-alpha.0") (d #t) (k 0)))) (h "0ndcvq95yvwgzczvp4xg1q82i4wadq1wclgb5m936x9116ja8klw")))

(define-public crate-surge-blitter-0.2.6-alpha.0 (c (n "surge-blitter") (v "0.2.6-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.6-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.6-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.2.6-alpha.0") (d #t) (k 0)))) (h "1gwk81idh72bf4d3qq6nza1s1qznpd5jnqpq1r81ig6g0pfch81l")))

(define-public crate-surge-blitter-0.2.9-alpha.0 (c (n "surge-blitter") (v "0.2.9-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.9-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.9-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.9-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.2.9-alpha.0") (d #t) (k 0)))) (h "023pv1nq2f44flgmg32vj031byp6dvgc2269q5piqiiz99w7slkp")))

(define-public crate-surge-blitter-0.2.11-alpha.0 (c (n "surge-blitter") (v "0.2.11-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.2.11-alpha.0") (d #t) (k 0)))) (h "1i25kdsab50m4m6i7gbcp8m6jksmz7aa16gpnasyi742irlvvz9n")))

(define-public crate-surge-blitter-0.2.12-alpha.0 (c (n "surge-blitter") (v "0.2.12-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-math") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-samplerate") (r "^0.2.12-alpha.0") (d #t) (k 0)))) (h "1im301yxjb31r4wcdsy4m9z4xzdaq79r8qqclzmq2nqs0qx5aq95")))

