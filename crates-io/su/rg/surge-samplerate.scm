(define-module (crates-io su rg surge-samplerate) #:use-module (crates-io))

(define-public crate-surge-samplerate-0.1.11-alpha.0 (c (n "surge-samplerate") (v "0.1.11-alpha.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.11-alpha.0") (d #t) (k 0)))) (h "1kys7p267lwyq583c48492nkqijr6n59fb6qw0caaxhjz6zf08hw")))

(define-public crate-surge-samplerate-0.1.12-alpha.0 (c (n "surge-samplerate") (v "0.1.12-alpha.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.12-alpha.0") (d #t) (k 0)))) (h "1j1gmx9rflpqhazk485vflfasc1jjg97q8rxyg2bj64plg90mhyv")))

(define-public crate-surge-samplerate-0.1.13-alpha.0 (c (n "surge-samplerate") (v "0.1.13-alpha.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.13-alpha.0") (d #t) (k 0)))) (h "154aiiv1r2cscazv29w07nvwhfn622xwl06d8x8cg0r1nh5z0db7")))

(define-public crate-surge-samplerate-0.1.14-alpha.0 (c (n "surge-samplerate") (v "0.1.14-alpha.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.14-alpha.0") (d #t) (k 0)))) (h "16l1wqgi7i8hgnrrbkjr7dsra7d990jlp8qgcvyihrpfy2987bml")))

(define-public crate-surge-samplerate-0.1.15-alpha.0 (c (n "surge-samplerate") (v "0.1.15-alpha.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.15-alpha.0") (d #t) (k 0)))) (h "19p96z2f6r3gmz28zw5cwd9ibyqfp7a7r4m2hk8pdayrk5ps770x")))

(define-public crate-surge-samplerate-0.1.16-alpha.0 (c (n "surge-samplerate") (v "0.1.16-alpha.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.16-alpha.0") (d #t) (k 0)))) (h "0r17zkpxyij82nsvac81017q84iycn4qll9hybfxfi7mnza5nicg")))

(define-public crate-surge-samplerate-0.1.17-alpha.0 (c (n "surge-samplerate") (v "0.1.17-alpha.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.17-alpha.0") (d #t) (k 0)))) (h "1qj0z3kwhpibnbg2x5a39ms1xwc8fjdvck1rvv306c56vyma6rfs")))

(define-public crate-surge-samplerate-0.1.20-alpha.0 (c (n "surge-samplerate") (v "0.1.20-alpha.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.20-alpha.0") (d #t) (k 0)))) (h "1adfrl0nb97ysilc033xh9fia5bfp4zidkf0c845083f58zbllf4")))

(define-public crate-surge-samplerate-0.1.23-alpha.0 (c (n "surge-samplerate") (v "0.1.23-alpha.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.23-alpha.0") (d #t) (k 0)))) (h "11fgwkj45kb7s2f35x8hyl7rbg25fa7zasbc5lq2n2wjigbjbr48")))

(define-public crate-surge-samplerate-0.1.24-alpha.0 (c (n "surge-samplerate") (v "0.1.24-alpha.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.24-alpha.0") (d #t) (k 0)))) (h "13k0p313zngdivfc5nqpjbfdb3z534faya5lnif8fas8x9i8mnxj")))

(define-public crate-surge-samplerate-0.1.26-alpha.0 (c (n "surge-samplerate") (v "0.1.26-alpha.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.26-alpha.0") (d #t) (k 0)))) (h "1v1yyj9x4xmgifgq0gl2swnr1x5pahmbzpfab1qw6svlpw1gaim2")))

(define-public crate-surge-samplerate-0.1.27-alpha.0 (c (n "surge-samplerate") (v "0.1.27-alpha.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.27-alpha.0") (d #t) (k 0)))) (h "0g72s8daybmsnd0dwpx613f31v4f30qly322pc6c05hbdz7kkdgd")))

(define-public crate-surge-samplerate-0.1.28-alpha.0 (c (n "surge-samplerate") (v "0.1.28-alpha.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.28-alpha.0") (d #t) (k 0)))) (h "059j9p2fw9mqg26l3p36xc607g5aafv8qvmd10lcynyb9d915s18")))

(define-public crate-surge-samplerate-0.1.29-alpha.0 (c (n "surge-samplerate") (v "0.1.29-alpha.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.29-alpha.0") (d #t) (k 0)))) (h "0j442p6xy0jrwcj23gjgwjl7szii1kwm42n16q2x1pygbk1qrrm5")))

(define-public crate-surge-samplerate-0.1.30-alpha.0 (c (n "surge-samplerate") (v "0.1.30-alpha.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.30-alpha.0") (d #t) (k 0)))) (h "16x5sm78ipaxhqgs6zhjfc6qvrjyapvcrql9njgq2s1rxn6jdpa2")))

(define-public crate-surge-samplerate-0.1.31-alpha.0 (c (n "surge-samplerate") (v "0.1.31-alpha.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.31-alpha.0") (d #t) (k 0)))) (h "08kknxjvghx9yqwi8w559nz1wkknskqx4vjmbjdbw594h0dg3cb3")))

(define-public crate-surge-samplerate-0.1.32-alpha.0 (c (n "surge-samplerate") (v "0.1.32-alpha.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.32-alpha.0") (d #t) (k 0)))) (h "0p358iiw5wcxfm1s0b77hxkynxh2dryyaz2m7j0pvcyy5l607dqd")))

(define-public crate-surge-samplerate-0.1.33-alpha.0 (c (n "surge-samplerate") (v "0.1.33-alpha.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.33-alpha.0") (d #t) (k 0)))) (h "1dhczgczkvf9j1ijy3hmhidx1cg51vp94j0h1sf4rnj5li1ibpv9")))

(define-public crate-surge-samplerate-0.1.36-alpha.0 (c (n "surge-samplerate") (v "0.1.36-alpha.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.36-alpha.0") (d #t) (k 0)))) (h "1z93xy862j511dicackwq62l5js2jrh082m0lh2lqdk52yfn46gr")))

(define-public crate-surge-samplerate-0.1.39-alpha.0 (c (n "surge-samplerate") (v "0.1.39-alpha.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.39-alpha.0") (d #t) (k 0)))) (h "01937wjrjphsiywq6rbf04grr44yk6ykpwcg2382q3vlccgf8i3f")))

(define-public crate-surge-samplerate-0.1.40-alpha.0 (c (n "surge-samplerate") (v "0.1.40-alpha.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.40-alpha.0") (d #t) (k 0)))) (h "0hzkdlc8jbc5vhmm7692g451rmfw8g2m6shv5pm444z0qqkq140a")))

(define-public crate-surge-samplerate-0.1.42-alpha.0 (c (n "surge-samplerate") (v "0.1.42-alpha.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-constants") (r "^0.1.42-alpha.0") (d #t) (k 0)))) (h "1jxw43cdxbd89vgcvm04ydig722k9n78d523lqngaybjysj8zmb4")))

(define-public crate-surge-samplerate-0.2.0-alpha.0 (c (n "surge-samplerate") (v "0.2.0-alpha.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-constants") (r "^0.2.0-alpha.0") (d #t) (k 0)))) (h "1xwyznjjcl4riw0s27dmlcwvlgiklssdl7z9zynaw5ss7ncpylvc")))

(define-public crate-surge-samplerate-0.2.3-alpha.0 (c (n "surge-samplerate") (v "0.2.3-alpha.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-constants") (r "^0.2.3-alpha.0") (d #t) (k 0)))) (h "1fx51l3m864g18qsrk1bwj1pi92134qnkyk2gyr75gn02c2c0n84")))

(define-public crate-surge-samplerate-0.2.4-alpha.0 (c (n "surge-samplerate") (v "0.2.4-alpha.0") (d (list (d (n "atomic_float") (r "^0.1.0") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "surge-constants") (r "^0.2.4-alpha.0") (d #t) (k 0)))) (h "1jxsz67g40i9yi9pqx090m3vxw4yv1vdmlp37l8656y7dhahpik1")))

(define-public crate-surge-samplerate-0.2.5-alpha.0 (c (n "surge-samplerate") (v "0.2.5-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)))) (h "0012g33cspaa721jqxyvg2nnbl1p9hm56qpc5ax09523mb8rabfh")))

(define-public crate-surge-samplerate-0.2.6-alpha.0 (c (n "surge-samplerate") (v "0.2.6-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.6-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)))) (h "11qw4mrc9si1c6w3naxwj9y4dm251qlyl5r2r0qn7n9qx3iirf9b")))

(define-public crate-surge-samplerate-0.2.8-alpha.0 (c (n "surge-samplerate") (v "0.2.8-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.8-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.8-alpha.0") (d #t) (k 0)))) (h "15dz59s1287q2hzd98krkcmrggisgf9ivyzz1hg8rbblf2w7cgd3")))

(define-public crate-surge-samplerate-0.2.9-alpha.0 (c (n "surge-samplerate") (v "0.2.9-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.9-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.9-alpha.0") (d #t) (k 0)))) (h "0b9x7xpxqz8w9zch1xa4rk0jdbpxs0p673nxqlxdhcj80kig9xsn")))

(define-public crate-surge-samplerate-0.2.11-alpha.0 (c (n "surge-samplerate") (v "0.2.11-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.11-alpha.0") (d #t) (k 0)))) (h "0hz88dg1z7la4c81q74a97qy221hm61f1j3zpsscmx2ssy2bxv25")))

(define-public crate-surge-samplerate-0.2.12-alpha.0 (c (n "surge-samplerate") (v "0.2.12-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.12-alpha.0") (d #t) (k 0)))) (h "0dvzp8f1krw9vv33ibr3kk31p97zmq9nzx2ms1lkxfmyr84q0az3")))

