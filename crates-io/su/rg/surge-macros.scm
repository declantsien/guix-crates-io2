(define-module (crates-io su rg surge-macros) #:use-module (crates-io))

(define-public crate-surge-macros-0.1.10-alpha.0 (c (n "surge-macros") (v "0.1.10-alpha.0") (h "16miyy3kc1cpsr5f8qjd13lszjdqdj3r2lbf9qpd5ra2chcln3gl")))

(define-public crate-surge-macros-0.1.11-alpha.0 (c (n "surge-macros") (v "0.1.11-alpha.0") (h "0vfxilz5d0b4xivskwwyibz9ml4yyylm27r27xwl1djgbjmxn1m0")))

(define-public crate-surge-macros-0.1.12-alpha.0 (c (n "surge-macros") (v "0.1.12-alpha.0") (h "08jvrz8s05pmnjfvliyg30kxgfa47403i97lhn85wlnaf5nrk8h7")))

(define-public crate-surge-macros-0.1.13-alpha.0 (c (n "surge-macros") (v "0.1.13-alpha.0") (h "0p8nshyhrgkm10bphb048d9zshc3vwj6fzv0bmsczbkwwh15pnng")))

(define-public crate-surge-macros-0.1.14-alpha.0 (c (n "surge-macros") (v "0.1.14-alpha.0") (h "0g0l0k61fk7j1g023i6ivr1bnf8pqha3b7n662xgvfh3vr36wwz3")))

(define-public crate-surge-macros-0.1.15-alpha.0 (c (n "surge-macros") (v "0.1.15-alpha.0") (h "174x4iyn570rhrqbar3angbbaalp0xhvm0pgh3vn3sb74fklczcs")))

(define-public crate-surge-macros-0.1.16-alpha.0 (c (n "surge-macros") (v "0.1.16-alpha.0") (h "1nq53g2g4gd34wsxkzy1i9vrhyj7yn5yvp2g7wgxmdzbaskzqqm3")))

(define-public crate-surge-macros-0.1.17-alpha.0 (c (n "surge-macros") (v "0.1.17-alpha.0") (h "169fisx1pyqrhwc51fzx51cmsp73gfsamcbl8622cdkvc0zxqkrf")))

(define-public crate-surge-macros-0.1.20-alpha.0 (c (n "surge-macros") (v "0.1.20-alpha.0") (h "0rywsly9msa6bx8kwl2nr1b1wr3rckls16i0nl4sdba8np81qnqx")))

(define-public crate-surge-macros-0.1.23-alpha.0 (c (n "surge-macros") (v "0.1.23-alpha.0") (h "0gnw25psxbgrbqg0inla6hl5qpzxzvrs7apqbzxw4q98rn2gjxa8")))

(define-public crate-surge-macros-0.1.24-alpha.0 (c (n "surge-macros") (v "0.1.24-alpha.0") (h "1q70p73i6w15mrzpqbfqjji5v908nw3mpmc1fjqy4nsl8jab50a4")))

(define-public crate-surge-macros-0.1.26-alpha.0 (c (n "surge-macros") (v "0.1.26-alpha.0") (h "06jwcgc4wimjgxq5kg86ch19j7iz94dbz3695bwj64v26k0kz535")))

(define-public crate-surge-macros-0.1.27-alpha.0 (c (n "surge-macros") (v "0.1.27-alpha.0") (h "05jcsl9j9ws1jawzkqyisd1r94vy9bv86c6n7mvbkk728akdsah2")))

(define-public crate-surge-macros-0.1.28-alpha.0 (c (n "surge-macros") (v "0.1.28-alpha.0") (h "1ni1jhb311jb1i1vc0cl8sczsifx2k52wdq8nib03jabdr88xsnz")))

(define-public crate-surge-macros-0.1.29-alpha.0 (c (n "surge-macros") (v "0.1.29-alpha.0") (h "1fj5qg5l94366vqgprsjypi7lic72jvx2yh61ash4hgik3ispgp5")))

(define-public crate-surge-macros-0.1.30-alpha.0 (c (n "surge-macros") (v "0.1.30-alpha.0") (h "001cqigqxq43h6kjrlk3pkzqvrxy3ix80vgglv15jnl3h6dfrald")))

(define-public crate-surge-macros-0.1.31-alpha.0 (c (n "surge-macros") (v "0.1.31-alpha.0") (h "1qdnabw9azp9qi0gf8pjgqpvf1j39zl3b63y9q6jim73g2xpk9sh")))

(define-public crate-surge-macros-0.1.32-alpha.0 (c (n "surge-macros") (v "0.1.32-alpha.0") (h "1la89zi6s3za4i7iiwq4di39n2ql4cqky4044f9ivrhhqihygd6x")))

(define-public crate-surge-macros-0.1.33-alpha.0 (c (n "surge-macros") (v "0.1.33-alpha.0") (h "142qcvapnrnad2w86814ixp80qgrdfjn14hg61ikqmqascviq6g8")))

(define-public crate-surge-macros-0.1.36-alpha.0 (c (n "surge-macros") (v "0.1.36-alpha.0") (h "1frv6xg7z91vd2kjm69f582m7805pwijg2q34zjsyqrf2sk9p28x")))

(define-public crate-surge-macros-0.1.39-alpha.0 (c (n "surge-macros") (v "0.1.39-alpha.0") (h "0p1306xhfdizk0k4bczb0szidlygxnfxmxffm6q5zbmhb5x63w9p")))

(define-public crate-surge-macros-0.1.40-alpha.0 (c (n "surge-macros") (v "0.1.40-alpha.0") (h "18l2mqyxws26dy5phrzhcysjvj1bg4b9ixlq6sjj92cw96miq3kq")))

(define-public crate-surge-macros-0.1.42-alpha.0 (c (n "surge-macros") (v "0.1.42-alpha.0") (h "14nq7w2y6rnzk8ma5dw96xj4bs81iynzspb6am2p056s97y849m8")))

(define-public crate-surge-macros-0.2.0-alpha.0 (c (n "surge-macros") (v "0.2.0-alpha.0") (h "0m0hp62np1mhzpcnx7r9spaalq1vd8wwv85wz85qcr8m4q1s8crl")))

(define-public crate-surge-macros-0.2.2-alpha.0 (c (n "surge-macros") (v "0.2.2-alpha.0") (h "196v7996zpj9mqj4w2j4v0j1z27wvd6widzw8ywcrzjz0sy2s8bk")))

(define-public crate-surge-macros-0.2.3-alpha.0 (c (n "surge-macros") (v "0.2.3-alpha.0") (h "063vxyl1c8m27ag35b4p5hwvpllivmc8b521hly8qfvhbbggxhq4")))

(define-public crate-surge-macros-0.2.4-alpha.0 (c (n "surge-macros") (v "0.2.4-alpha.0") (h "014gf2b573fxqxpmwrvimkipzv6c10sy3964irhaic74al3z62nk")))

(define-public crate-surge-macros-0.2.5-alpha.0 (c (n "surge-macros") (v "0.2.5-alpha.0") (d (list (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)))) (h "0jm1wxhaqias0c0qh636c7cy9pr1qm2j2wgfg6br3gdlmysl6i8s")))

(define-public crate-surge-macros-0.2.6-alpha.0 (c (n "surge-macros") (v "0.2.6-alpha.0") (d (list (d (n "surge-imports") (r "^0.2.6-alpha.0") (d #t) (k 0)))) (h "145rapwq4vzdl64qp18mmpjc0w6mwqj6gr6rhbdrqckjma2qym36")))

(define-public crate-surge-macros-0.2.8-alpha.0 (c (n "surge-macros") (v "0.2.8-alpha.0") (d (list (d (n "surge-imports") (r "^0.2.8-alpha.0") (d #t) (k 0)))) (h "007ap6i01glzmgpcayab3aabpsc25yz82682f5ajafafh1rcn46a")))

(define-public crate-surge-macros-0.2.9-alpha.0 (c (n "surge-macros") (v "0.2.9-alpha.0") (d (list (d (n "surge-imports") (r "^0.2.9-alpha.0") (d #t) (k 0)))) (h "0xalwjqgabskn3mhly80nzfxk33kh85849ay05mf24s3dpf9mlic")))

(define-public crate-surge-macros-0.2.11-alpha.0 (c (n "surge-macros") (v "0.2.11-alpha.0") (d (list (d (n "surge-imports") (r "^0.2.11-alpha.0") (d #t) (k 0)))) (h "05b17n41069a0b0r6zg58mzj6w85q144fhckd7y9irrd9n2vvkmd")))

(define-public crate-surge-macros-0.2.12-alpha.0 (c (n "surge-macros") (v "0.2.12-alpha.0") (d (list (d (n "surge-imports") (r "^0.2.12-alpha.0") (d #t) (k 0)))) (h "0jlir9xvgfycn0pgxckgr5mw3w1ffpqfb0h11930nkxgg72xki01")))

