(define-module (crates-io su rg surge-midi) #:use-module (crates-io))

(define-public crate-surge-midi-0.1.17-alpha.0 (c (n "surge-midi") (v "0.1.17-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.17-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.17-alpha.0") (d #t) (k 0)) (d (n "surge-quadrosc") (r "^0.1.17-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.17-alpha.0") (d #t) (k 0)))) (h "14db6ks1l6gbn88pxw1xa3rsnlcj2iymzahxvwznz3apl2940dzj")))

(define-public crate-surge-midi-0.1.20-alpha.0 (c (n "surge-midi") (v "0.1.20-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.20-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.20-alpha.0") (d #t) (k 0)) (d (n "surge-quadrosc") (r "^0.1.20-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.20-alpha.0") (d #t) (k 0)))) (h "0ix1nl0wvv071j386dd265jvgv4llzcg514kn5vz42jvjy4pgz28")))

(define-public crate-surge-midi-0.1.23-alpha.0 (c (n "surge-midi") (v "0.1.23-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.23-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.23-alpha.0") (d #t) (k 0)) (d (n "surge-quadrosc") (r "^0.1.23-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.23-alpha.0") (d #t) (k 0)))) (h "1wwgpqk7m3i6cpvw6k26n6db38yr5faqazgg26w6pmk6p19fn9yq")))

(define-public crate-surge-midi-0.1.24-alpha.0 (c (n "surge-midi") (v "0.1.24-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.24-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.24-alpha.0") (d #t) (k 0)) (d (n "surge-quadrosc") (r "^0.1.24-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.24-alpha.0") (d #t) (k 0)))) (h "04l20ivzbbz25pl69j4md70ssn5rbn0m9qd5778wdwyrbcfhljib")))

(define-public crate-surge-midi-0.1.26-alpha.0 (c (n "surge-midi") (v "0.1.26-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.26-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.26-alpha.0") (d #t) (k 0)) (d (n "surge-quadrosc") (r "^0.1.26-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.26-alpha.0") (d #t) (k 0)))) (h "06kz78y025m1hd9hc929pjmqkfmbkv85slhag48nh08acqrbjr2l")))

(define-public crate-surge-midi-0.1.27-alpha.0 (c (n "surge-midi") (v "0.1.27-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.27-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.27-alpha.0") (d #t) (k 0)) (d (n "surge-quadrosc") (r "^0.1.27-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.27-alpha.0") (d #t) (k 0)))) (h "0w5sf9ql1mgk83qka8xgjsqbcxhn4dh8yqaicg77kxlmkskir133")))

(define-public crate-surge-midi-0.1.28-alpha.0 (c (n "surge-midi") (v "0.1.28-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.28-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.28-alpha.0") (d #t) (k 0)) (d (n "surge-quadrosc") (r "^0.1.28-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.28-alpha.0") (d #t) (k 0)))) (h "1g4f9cssi87y0ylp5jffm3q4vq36yh1v4s7iaajk6zxvj9q2x6pp")))

(define-public crate-surge-midi-0.1.29-alpha.0 (c (n "surge-midi") (v "0.1.29-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.29-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.29-alpha.0") (d #t) (k 0)) (d (n "surge-quadrosc") (r "^0.1.29-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.29-alpha.0") (d #t) (k 0)))) (h "1dyycgv1ghb8jc0v53hld3b5lh1sr3yf5ssg6pq6fgl2inqhikzy")))

(define-public crate-surge-midi-0.1.30-alpha.0 (c (n "surge-midi") (v "0.1.30-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.30-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.30-alpha.0") (d #t) (k 0)) (d (n "surge-quadrosc") (r "^0.1.30-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.30-alpha.0") (d #t) (k 0)))) (h "0xylhqal3xrcabh9ylf67h6j74kjg1q8nc35vdvp2yd2r99lg31i")))

(define-public crate-surge-midi-0.1.31-alpha.0 (c (n "surge-midi") (v "0.1.31-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.31-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.31-alpha.0") (d #t) (k 0)) (d (n "surge-quadrosc") (r "^0.1.31-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.31-alpha.0") (d #t) (k 0)))) (h "1iz7w3l22av3phqv4j2mq18y5cl8f6d5llb6cmfmqdjbr8q2bm2d")))

(define-public crate-surge-midi-0.1.32-alpha.0 (c (n "surge-midi") (v "0.1.32-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.32-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.32-alpha.0") (d #t) (k 0)) (d (n "surge-quadrosc") (r "^0.1.32-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.32-alpha.0") (d #t) (k 0)))) (h "0s61s4wgh0nx607nh6hadni4827kdrw90vbrgsrnav43h5slgnp8")))

(define-public crate-surge-midi-0.1.33-alpha.0 (c (n "surge-midi") (v "0.1.33-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.33-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.33-alpha.0") (d #t) (k 0)) (d (n "surge-quadrosc") (r "^0.1.33-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.33-alpha.0") (d #t) (k 0)))) (h "0af6fkpkilmhj9f52dpfbcq2cjbbs0wxapi72s2aplb907bmam04")))

(define-public crate-surge-midi-0.1.36-alpha.0 (c (n "surge-midi") (v "0.1.36-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.36-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.36-alpha.0") (d #t) (k 0)) (d (n "surge-quadrosc") (r "^0.1.36-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.36-alpha.0") (d #t) (k 0)))) (h "1r9x940gkkrns5mm7rjgr6mdl8blhb42l2bipmlcxnswcc80p89k")))

(define-public crate-surge-midi-0.1.39-alpha.0 (c (n "surge-midi") (v "0.1.39-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.39-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.39-alpha.0") (d #t) (k 0)) (d (n "surge-quadrosc") (r "^0.1.39-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.39-alpha.0") (d #t) (k 0)))) (h "03h3l5l1q7948ks2d5nda23jlq4ihklk67ir8v5c4jxnsd14v2qc")))

(define-public crate-surge-midi-0.1.40-alpha.0 (c (n "surge-midi") (v "0.1.40-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.40-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.40-alpha.0") (d #t) (k 0)) (d (n "surge-quadrosc") (r "^0.1.40-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.40-alpha.0") (d #t) (k 0)))) (h "0ayidhb2m8nsac1swh1sz1bnl8fl5i1kxg3y4y7b5m9irnqyzxv6")))

(define-public crate-surge-midi-0.1.42-alpha.0 (c (n "surge-midi") (v "0.1.42-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.42-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.1.42-alpha.0") (d #t) (k 0)) (d (n "surge-quadrosc") (r "^0.1.42-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.42-alpha.0") (d #t) (k 0)))) (h "1bi8pg9f4ixj310y7p0rziqah2cg8i7j6zsyaz6wrbkbkm9jdk0q")))

(define-public crate-surge-midi-0.2.0-alpha.0 (c (n "surge-midi") (v "0.2.0-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "surge-quadrosc") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.2.0-alpha.0") (d #t) (k 0)))) (h "0yxdqpx6j0qs9zlldjy9wp840pc69ifsv6r9z683yq1mm4al7ryl")))

(define-public crate-surge-midi-0.2.4-alpha.0 (c (n "surge-midi") (v "0.2.4-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.4-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.2.4-alpha.0") (d #t) (k 0)) (d (n "surge-quadrosc") (r "^0.2.4-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.2.4-alpha.0") (d #t) (k 0)))) (h "1s6jchi74warkx6gzxhw2sdz58zhj7rx0qrj8nb6fr1vafxvwykf")))

(define-public crate-surge-midi-0.2.5-alpha.0 (c (n "surge-midi") (v "0.2.5-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-quadrosc") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.2.5-alpha.0") (d #t) (k 0)))) (h "1qa19pl2kl2iis2x9365iah8p23fk9ibxf3bc66ykxp7szlsdixs")))

(define-public crate-surge-midi-0.2.6-alpha.0 (c (n "surge-midi") (v "0.2.6-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.6-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-quadrosc") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.2.6-alpha.0") (d #t) (k 0)))) (h "0xzfgnlb3wf7l7nilkgriy8m5dbn4hm9mzfqswpgk0912lap9qyz")))

(define-public crate-surge-midi-0.2.11-alpha.0 (c (n "surge-midi") (v "0.2.11-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-quadrosc") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.2.11-alpha.0") (d #t) (k 0)))) (h "1d5323zlpgq4c19am63c7dvfyy2dbmxv6vw291ig93a1hz14d88c")))

(define-public crate-surge-midi-0.2.12-alpha.0 (c (n "surge-midi") (v "0.2.12-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-macros") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-quadrosc") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.2.12-alpha.0") (d #t) (k 0)))) (h "0swb31qhd35wmkdy400lnm3k267rhwmylvkrc16w3fly6db8f9bc")))

