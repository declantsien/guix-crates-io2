(define-module (crates-io su rg surge) #:use-module (crates-io))

(define-public crate-surge-0.1.0 (c (n "surge") (v "0.1.0") (h "0rpisjb4qn5d3v4jhar7m3y2dg392k94vrznxmlgmzs6yd3b7x05") (y #t)))

(define-public crate-surge-0.1.1 (c (n "surge") (v "0.1.1") (d (list (d (n "hyper") (r "^0.10.11") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.3") (d #t) (k 0)) (d (n "image") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "rodio") (r "^0.5.0") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "termimage") (r "^0.3.1") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.6") (d #t) (k 0)) (d (n "toml") (r "^0.4.1") (d #t) (k 0)))) (h "1a6m74i9pz3rwdr0cp4dxh7al64lzfl79cdsmm51rv5gg18g4nwn") (y #t)))

(define-public crate-surge-0.2.0 (c (n "surge") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "hyper") (r "^0.10.11") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2.3") (d #t) (k 0)) (d (n "mpv") (r "^0.2.3") (d #t) (k 0)) (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "0121mrw8scb4fzcnl5z3piymgl2n3xcvf19cwl96byk8bdmbn3bq")))

