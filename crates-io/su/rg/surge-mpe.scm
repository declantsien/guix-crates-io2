(define-module (crates-io su rg surge-mpe) #:use-module (crates-io))

(define-public crate-surge-mpe-0.1.20-alpha.0 (c (n "surge-mpe") (v "0.1.20-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.20-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.20-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.20-alpha.0") (d #t) (k 0)))) (h "0z3qvdhz3kvl9dc98fkyf2nysg7ska0pss6j8hyiqgmmj9jjc66a")))

(define-public crate-surge-mpe-0.1.23-alpha.0 (c (n "surge-mpe") (v "0.1.23-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.23-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.23-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.23-alpha.0") (d #t) (k 0)))) (h "1ywypf7vjxz7i6faw2qffdxyf7isl110j9vvbrhpy9h9b3yb32x5")))

(define-public crate-surge-mpe-0.1.24-alpha.0 (c (n "surge-mpe") (v "0.1.24-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.24-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.24-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.24-alpha.0") (d #t) (k 0)))) (h "198x31jxvh7qz0j482kpi6hjsp7v569vhb121x3llz35kbx774ql")))

(define-public crate-surge-mpe-0.1.26-alpha.0 (c (n "surge-mpe") (v "0.1.26-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.26-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.26-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.26-alpha.0") (d #t) (k 0)))) (h "1qyn06l313ds36xjsmzpqwcz145wwin3scsxndpn54gddkb5cmbw")))

(define-public crate-surge-mpe-0.1.27-alpha.0 (c (n "surge-mpe") (v "0.1.27-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.27-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.27-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.27-alpha.0") (d #t) (k 0)))) (h "03z0bhssasrqq35iq7xgql4vdvpkifmggbz1rpmwkswwd61hcbjh")))

(define-public crate-surge-mpe-0.1.28-alpha.0 (c (n "surge-mpe") (v "0.1.28-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.28-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.28-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.28-alpha.0") (d #t) (k 0)))) (h "1wcvng1ncq53syxc43cf2mx95qanbp3i3ysc0i01aarcwgd2jzxm")))

(define-public crate-surge-mpe-0.1.29-alpha.0 (c (n "surge-mpe") (v "0.1.29-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.29-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.29-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.29-alpha.0") (d #t) (k 0)))) (h "1hzlqbibj2x5i94lhqfawg0ydbfasxm4v6bv7v67jh90ml6k3bwx")))

(define-public crate-surge-mpe-0.1.30-alpha.0 (c (n "surge-mpe") (v "0.1.30-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.30-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.30-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.30-alpha.0") (d #t) (k 0)))) (h "0hcri1jb037wjzj1372pardy21pwsc9aa32330nyzk36hgw41p4c")))

(define-public crate-surge-mpe-0.1.31-alpha.0 (c (n "surge-mpe") (v "0.1.31-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.31-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.31-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.31-alpha.0") (d #t) (k 0)))) (h "04garw3hcv9dd1gwdwm533fr3ilfiadrlcd6j37lqf5kwz66cc8f")))

(define-public crate-surge-mpe-0.1.32-alpha.0 (c (n "surge-mpe") (v "0.1.32-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.32-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.32-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.32-alpha.0") (d #t) (k 0)))) (h "0c3qjbbb2q8izwbshmvr09naqy4jc9rhackizjk80b7kpjlfyvsj")))

(define-public crate-surge-mpe-0.1.33-alpha.0 (c (n "surge-mpe") (v "0.1.33-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.33-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.33-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.33-alpha.0") (d #t) (k 0)))) (h "0admrxih0y5hr7xzyw99sizvpg36clc6bz42xdsbjssqnnwbql4g")))

(define-public crate-surge-mpe-0.1.36-alpha.0 (c (n "surge-mpe") (v "0.1.36-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.36-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.36-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.36-alpha.0") (d #t) (k 0)))) (h "0974r9xazsap74nxjdlnivm1rra3kx6q1a4p41pdfwnfw14ipaj9")))

(define-public crate-surge-mpe-0.1.39-alpha.0 (c (n "surge-mpe") (v "0.1.39-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.39-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.39-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.39-alpha.0") (d #t) (k 0)))) (h "0jsy7jysp1bwzgkqpm6d1f5fixzzpw9qqrpi4ak54rvfynampi08")))

(define-public crate-surge-mpe-0.1.40-alpha.0 (c (n "surge-mpe") (v "0.1.40-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.40-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.40-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.40-alpha.0") (d #t) (k 0)))) (h "01snm4xmpvvjjqjp0z5x67vzpd5c4mgpj3m4csli8mc2shk5rw9g")))

(define-public crate-surge-mpe-0.1.42-alpha.0 (c (n "surge-mpe") (v "0.1.42-alpha.0") (d (list (d (n "surge-constants") (r "^0.1.42-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.1.42-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.1.42-alpha.0") (d #t) (k 0)))) (h "1w3wfndzzjaymydqr8fyiawq39gp3n05k9f453r1sp9yb37bjp79")))

(define-public crate-surge-mpe-0.2.0-alpha.0 (c (n "surge-mpe") (v "0.2.0-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.2.0-alpha.0") (d #t) (k 0)))) (h "033b7n3z5fkd23cl26swm5b3vsbszz9yvcfr813ccbxbsq6wyf8g")))

(define-public crate-surge-mpe-0.2.4-alpha.0 (c (n "surge-mpe") (v "0.2.4-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.4-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.4-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.2.4-alpha.0") (d #t) (k 0)))) (h "0f24fnc7yjlpxgxf1p9r3rkw3dw3m8yzqzz6imw9dg1shy8fbvij")))

(define-public crate-surge-mpe-0.2.5-alpha.0 (c (n "surge-mpe") (v "0.2.5-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.2.5-alpha.0") (d #t) (k 0)))) (h "1hvf2wxdj868i0y7vwy67rfjvk7c6jmk5rawiamzy0yq9jx76diy")))

(define-public crate-surge-mpe-0.2.6-alpha.0 (c (n "surge-mpe") (v "0.2.6-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.6-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.5-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.6-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.2.6-alpha.0") (d #t) (k 0)))) (h "1xdw65r5462fs8xzg7cf7zsa7i83baawjgc4m4qjkvpnmmagf4jb")))

(define-public crate-surge-mpe-0.2.11-alpha.0 (c (n "surge-mpe") (v "0.2.11-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.11-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.2.11-alpha.0") (d #t) (k 0)))) (h "1757s85lp18nlhsdr28xp9315lczivrq1dsp0ynl516rk80iz3jw")))

(define-public crate-surge-mpe-0.2.12-alpha.0 (c (n "surge-mpe") (v "0.2.12-alpha.0") (d (list (d (n "surge-constants") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-imports") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-traits") (r "^0.2.12-alpha.0") (d #t) (k 0)) (d (n "surge-types") (r "^0.2.12-alpha.0") (d #t) (k 0)))) (h "109pbdj2jvvbqcg3r972fx08qdzn3aal9l4yzyv74indlcczfczh")))

