(define-module (crates-io su sp suspend) #:use-module (crates-io))

(define-public crate-suspend-0.1.0 (c (n "suspend") (v "0.1.0") (d (list (d (n "futures-util") (r "^0.3") (d #t) (k 0)))) (h "1r2g26chmj84x0xm4w3xkg48qf29qh56xgmhf339jh5g0s0dw4vj")))

(define-public crate-suspend-0.1.1 (c (n "suspend") (v "0.1.1") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-task") (r "^0.3") (d #t) (k 0)) (d (n "oneshot_rs") (r "^0.1") (o #t) (d #t) (k 0) (p "oneshot")))) (h "15c55j4ky2kfayh98pyj06flavacwl1a74ca349wnnaci4j7rd4a") (f (quote (("oneshot" "oneshot_rs") ("default"))))))

(define-public crate-suspend-0.1.2 (c (n "suspend") (v "0.1.2") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-task") (r "^0.3") (d #t) (k 0)) (d (n "oneshot_rs") (r "^0.1") (o #t) (d #t) (k 0) (p "oneshot")))) (h "1snsw4gj7rgxr24rnildg0v4z9hdy3y2ij6nmas13x6sf3hg1sbg") (f (quote (("oneshot" "oneshot_rs") ("default" "oneshot"))))))

(define-public crate-suspend-0.1.3 (c (n "suspend") (v "0.1.3") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-task") (r "^0.3") (d #t) (k 0)) (d (n "oneshot_rs") (r "^0.1") (o #t) (d #t) (k 0) (p "oneshot")) (d (n "pin-utils") (r "^0.1") (d #t) (k 0)))) (h "1l6mdd08pilrs7r28sjshrddfqx0k3mgcm97mwq49z1vwcvdifd5") (f (quote (("oneshot" "oneshot_rs") ("default" "oneshot"))))))

