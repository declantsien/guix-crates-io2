(define-module (crates-io su sp suspicious-pods) #:use-module (crates-io))

(define-public crate-suspicious-pods-0.1.0 (c (n "suspicious-pods") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.6.0") (f (quote ("v1_15"))) (k 0)) (d (n "kube") (r "^0.17.0") (f (quote ("openapi"))) (d #t) (k 0)))) (h "1s3rwd4mhapc90n42ip74y6aqla1y2j9wjaaca4qxdv3gy1qxlrq")))

(define-public crate-suspicious-pods-0.2.0 (c (n "suspicious-pods") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.6.0") (f (quote ("v1_15"))) (k 0)) (d (n "kube") (r "^0.17.0") (f (quote ("openapi"))) (d #t) (k 0)))) (h "0mxs9fhc3qh6ybhahds9wns20ky9219iqv50z3v0lqy1qxlzdhh0")))

(define-public crate-suspicious-pods-0.3.0 (c (n "suspicious-pods") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.6.0") (f (quote ("v1_15"))) (k 0)) (d (n "kube") (r "^0.17.0") (f (quote ("openapi"))) (d #t) (k 0)) (d (n "suspicious-pods-lib") (r "^0.1.0") (d #t) (k 0)))) (h "0nwj1pjvsmf49l271lq6lq95vhz7fw9f9jhmabmycba06r0d1zsa")))

(define-public crate-suspicious-pods-0.3.1 (c (n "suspicious-pods") (v "0.3.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.6.0") (f (quote ("v1_15"))) (k 0)) (d (n "kube") (r "^0.17.0") (f (quote ("openapi"))) (d #t) (k 0)) (d (n "suspicious-pods-lib") (r "^0.1.0") (d #t) (k 0)))) (h "0yp7rqyzi1saam7jlnvgrxi80h4xp7798rr6b95gls613l3106vj")))

(define-public crate-suspicious-pods-0.4.0 (c (n "suspicious-pods") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.6.0") (f (quote ("v1_15"))) (k 0)) (d (n "kube") (r "^0.17.0") (f (quote ("openapi"))) (d #t) (k 0)) (d (n "suspicious-pods-lib") (r "^0.2.0") (d #t) (k 0)))) (h "0v9ms9kfv767vq07qf0yspks260ml9862wnbfkbyfk1im2zfg7g9")))

(define-public crate-suspicious-pods-0.5.0 (c (n "suspicious-pods") (v "0.5.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "suspicious-pods-lib") (r "^0.3.0") (d #t) (k 0)))) (h "0qsmxg14dxjjqz3yl901i2gbwhi0nscna1gf8lr4a0yg9bp9n1z8")))

(define-public crate-suspicious-pods-0.5.1 (c (n "suspicious-pods") (v "0.5.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "suspicious-pods-lib") (r "^0.3.1") (d #t) (k 0)))) (h "06h0rc09m0zig2nli4gg8940rw77bghk4caks0lg61w2jbwcj440")))

(define-public crate-suspicious-pods-0.5.2 (c (n "suspicious-pods") (v "0.5.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "suspicious-pods-lib") (r "^0.3.1") (d #t) (k 0)))) (h "0w23p4gw45n425ra40f54cr6cghws6kzhigx915s56c07q9ad40i")))

(define-public crate-suspicious-pods-0.6.0 (c (n "suspicious-pods") (v "0.6.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "suspicious-pods-lib") (r "^0.5.0") (d #t) (k 0)))) (h "0817wf0sls11nssbjsn1vrc7yzy2v4jfwi39246h8ms3pna0n2bs")))

(define-public crate-suspicious-pods-1.0.0 (c (n "suspicious-pods") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "suspicious-pods-lib") (r "^1.0.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0v7n2df8i5fzpabvf78ckqm0i4wrb0220bbsd9rcajfl49b5iabi")))

(define-public crate-suspicious-pods-1.1.0 (c (n "suspicious-pods") (v "1.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "suspicious-pods-lib") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 0)))) (h "0wxax48hbd7p48jqxmpv6wrdzi00hr7fhg22wpi79davs02p1s07")))

(define-public crate-suspicious-pods-1.2.0 (c (n "suspicious-pods") (v "1.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^3.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "suspicious-pods-lib") (r "^1.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt"))) (d #t) (k 0)))) (h "0mzp3fdg2f70hr7k3kg7zn6q00pj6kd30sl87qm9spalmvva21iq")))

