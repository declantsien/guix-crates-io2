(define-module (crates-io su sp suspend_fn_proc_macro) #:use-module (crates-io))

(define-public crate-suspend_fn_proc_macro-0.1.0 (c (n "suspend_fn_proc_macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full"))) (d #t) (k 0)) (d (n "take_mut") (r "^0.2.2") (d #t) (k 0)))) (h "05ilaymxhn0arn1i83rb1245wy2lal3zqhlzkqh53mwia2s69wr8")))

