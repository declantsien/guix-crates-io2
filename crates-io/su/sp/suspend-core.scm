(define-module (crates-io su sp suspend-core) #:use-module (crates-io))

(define-public crate-suspend-core-0.1.0 (c (n "suspend-core") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.11") (d #t) (k 2)) (d (n "libc") (r "^0.2.71") (d #t) (t "cfg(unix)") (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)))) (h "1m3yjv2w32447rdyv0an5aihwighkxfdpflpv4kk7qihpz77l2qg") (f (quote (("default"))))))

