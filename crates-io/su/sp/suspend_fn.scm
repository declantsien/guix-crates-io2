(define-module (crates-io su sp suspend_fn) #:use-module (crates-io))

(define-public crate-suspend_fn-0.1.0 (c (n "suspend_fn") (v "0.1.0") (d (list (d (n "suspend_fn_proc_macro") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "1pmkjld3d8s9kxahq2c7sgs2rjfbb4g4qdsrpvzixzb3q7z6xhcd")))

