(define-module (crates-io su sp suspend-channel) #:use-module (crates-io))

(define-public crate-suspend-channel-0.1.0 (c (n "suspend-channel") (v "0.1.0") (d (list (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-task") (r "^0.3") (d #t) (k 2)) (d (n "maybe-cell") (r "^0.1") (d #t) (k 0)) (d (n "suspend-core") (r "^0.1") (d #t) (k 0)))) (h "14nrb342y6vzj7ijb9chqns3cvybf4x19z5qzq709xy9sxk3ajbv") (f (quote (("default"))))))

