(define-module (crates-io su sp suspicious-pods-lib) #:use-module (crates-io))

(define-public crate-suspicious-pods-lib-0.1.0 (c (n "suspicious-pods-lib") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.6.0") (f (quote ("v1_15"))) (k 0)) (d (n "kube") (r "^0.17.0") (f (quote ("openapi"))) (d #t) (k 0)))) (h "1j50cwc53r9w89msfplhggfafjcyi4g7r3mjmbz2vqvwr4lvx0cc")))

(define-public crate-suspicious-pods-lib-0.2.0 (c (n "suspicious-pods-lib") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.6.0") (f (quote ("v1_15"))) (k 0)) (d (n "kube") (r "^0.17.0") (f (quote ("openapi"))) (d #t) (k 0)))) (h "067nprw9vwckng42hzj3bp4r5x585i0k3g9cmblh89kh22kwx864")))

(define-public crate-suspicious-pods-lib-0.3.0 (c (n "suspicious-pods-lib") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.6.0") (f (quote ("v1_15"))) (k 0)) (d (n "kube") (r "^0.17.0") (f (quote ("openapi"))) (d #t) (k 0)))) (h "0h4dwb1l517c7kg0v6pc2h98bww1cz5ygqpzhwdh6x2n9pjwmm0d")))

(define-public crate-suspicious-pods-lib-0.3.1 (c (n "suspicious-pods-lib") (v "0.3.1") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.6.0") (f (quote ("v1_15"))) (k 0)) (d (n "kube") (r "^0.17.0") (f (quote ("openapi"))) (d #t) (k 0)))) (h "10ww7mfmrsrsiq5ak86a0gzyyrnkfdsz09h0a82dxbhx0dqkqwib")))

(define-public crate-suspicious-pods-lib-0.4.0 (c (n "suspicious-pods-lib") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.6.0") (f (quote ("v1_15"))) (k 0)) (d (n "kube") (r "^0.17.0") (f (quote ("openapi"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)))) (h "1akllj79il154gfkvimsszxfavfg5mdsa57j9a91zyhvb7rkx7d7")))

(define-public crate-suspicious-pods-lib-0.5.0 (c (n "suspicious-pods-lib") (v "0.5.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.6") (f (quote ("v1_15"))) (k 0)) (d (n "kube") (r "^0.17.0") (f (quote ("openapi"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1460i0zlvkh42g0lvbhc3id72lyvzs8hg60r1yv1dk1bg3q8axqc")))

(define-public crate-suspicious-pods-lib-1.0.0 (c (n "suspicious-pods-lib") (v "1.0.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "k8s-openapi") (r "^0.7") (f (quote ("v1_17"))) (k 0)) (d (n "kube") (r "^0.31") (f (quote ("rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h9nzzfv2jp40dhjv14r61sd1bg6k2f7vz63xbfdl2bz6k8gf051")))

(define-public crate-suspicious-pods-lib-1.1.0 (c (n "suspicious-pods-lib") (v "1.1.0") (d (list (d (n "k8s-openapi") (r "^0.10") (f (quote ("v1_19"))) (k 0)) (d (n "kube") (r "^0.47") (f (quote ("rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qwxgcp4x50wmk1a3wbwapsks8b0376gz3zj0df0v240qj4r6wm7")))

(define-public crate-suspicious-pods-lib-1.2.0 (c (n "suspicious-pods-lib") (v "1.2.0") (d (list (d (n "k8s-openapi") (r "^0.14") (f (quote ("v1_22"))) (k 0)) (d (n "kube") (r "^0.71") (f (quote ("client" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hz6c9rk7gp8jrlmjkzbb2brszzya43x5vi2nhnpyw4rdb1pl18i")))

