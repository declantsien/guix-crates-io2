(define-module (crates-io su b_ sub_if) #:use-module (crates-io))

(define-public crate-sub_if-1.0.0 (c (n "sub_if") (v "1.0.0") (h "1l1p7zvwv8f2k3qyd9ndp0fmfdx3vbrb1mgfxcncrliyn5abpgxf")))

(define-public crate-sub_if-1.0.1 (c (n "sub_if") (v "1.0.1") (h "1kvl9rx1x76zzz2zcfnqmfhjn99lq96xy1cc3f5fapi9ykv30380")))

