(define-module (crates-io su mn sumno) #:use-module (crates-io))

(define-public crate-sumno-0.1.0 (c (n "sumno") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.16.0") (d #t) (k 0)))) (h "065kfsml8wdx1p6fc7mcm8szghbq8mim2m5rvbcw7q541q4dknhf")))

