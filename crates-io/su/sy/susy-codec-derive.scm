(define-module (crates-io su sy susy-codec-derive) #:use-module (crates-io))

(define-public crate-susy-codec-derive-3.1.0 (c (n "susy-codec-derive") (v "3.1.0") (d (list (d (n "proc-macro-crate") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1l86giiybmcwb7xr4sd4c4qfbkpm1pgav3fx9hbkx9s3bzsdlgsc") (f (quote (("std") ("default" "std"))))))

(define-public crate-susy-codec-derive-3.2.0 (c (n "susy-codec-derive") (v "3.2.0") (d (list (d (n "proc-macro-crate") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "0ljrh833v1l9l5hng2nk65i927r32k1q0b3m5hikdai85rfv6mkz") (f (quote (("std") ("default" "std"))))))

(define-public crate-susy-codec-derive-3.3.0 (c (n "susy-codec-derive") (v "3.3.0") (d (list (d (n "proc-macro-crate") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1l5yw9kgcnqz2cwdbvchngnbw3babcr84hi502p1hka91iic8szf") (f (quote (("std") ("default" "std"))))))

(define-public crate-susy-codec-derive-2.1.0 (c (n "susy-codec-derive") (v "2.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "susy-codec") (r "~2.1.0") (d #t) (k 2)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "004qydx1a18ixlkv6d98daqr4rpbkjrqr2b1ksxm22hbqcnax4sr") (f (quote (("std") ("default" "std"))))))

(define-public crate-susy-codec-derive-2.2.0 (c (n "susy-codec-derive") (v "2.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0zci2x9yg2vdjinq7mxvpdmyq219vp4fvj9bsz26jixjwcsxcpk0") (f (quote (("std") ("default" "std"))))))

