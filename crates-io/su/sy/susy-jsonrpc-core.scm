(define-module (crates-io su sy susy-jsonrpc-core) #:use-module (crates-io))

(define-public crate-susy-jsonrpc-core-10.1.0 (c (n "susy-jsonrpc-core") (v "10.1.0") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vavj73j53hmbiacsy6ksxvc4nff6x80ffcirrl22rljqaimlvxi") (y #t)))

(define-public crate-susy-jsonrpc-core-10.0.1 (c (n "susy-jsonrpc-core") (v "10.0.1") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1q89dg0asd6dxplwgj5g5fnvh8fgals2sfgxm351qj8b1nn5245i") (y #t)))

(define-public crate-susy-jsonrpc-core-10.0.2 (c (n "susy-jsonrpc-core") (v "10.0.2") (d (list (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1irklbrd0c6s910sf39zrbcr37cg09djag8waal6c8rf605apq7p")))

(define-public crate-susy-jsonrpc-core-8.0.0 (c (n "susy-jsonrpc-core") (v "8.0.0") (d (list (d (n "futures") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01hf87w5d45a482ihids46iamvd1ng3qpr4yj7c8q835c2i715fk")))

