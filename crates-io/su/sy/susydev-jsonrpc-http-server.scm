(define-module (crates-io su sy susydev-jsonrpc-http-server) #:use-module (crates-io))

(define-public crate-susydev-jsonrpc-http-server-10.0.1 (c (n "susydev-jsonrpc-http-server") (v "10.0.1") (d (list (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "susydev-jsonrpc-core") (r "^10.0") (d #t) (k 0)) (d (n "susydev-jsonrpc-server-utils") (r "^10.0") (d #t) (k 0)) (d (n "unicase") (r "^2.0") (d #t) (k 0)))) (h "0x6sszrca7wzm1dn3f6g5s2lz0cvfdf0izmkjm062vrh2rac8kr3")))

(define-public crate-susydev-jsonrpc-http-server-10.1.0 (c (n "susydev-jsonrpc-http-server") (v "10.1.0") (d (list (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "susydev-jsonrpc-core") (r "^10.1") (d #t) (k 0)) (d (n "susydev-jsonrpc-server-utils") (r "^10.1") (d #t) (k 0)) (d (n "unicase") (r "^2.0") (d #t) (k 0)))) (h "163n0nv18mcmdmy1khj75sry2ckrj0968nfk2ya55gkkq2b8glgw")))

(define-public crate-susydev-jsonrpc-http-server-11.0.0 (c (n "susydev-jsonrpc-http-server") (v "11.0.0") (d (list (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "susydev-jsonrpc-core") (r "^11.0") (d #t) (k 0)) (d (n "susydev-jsonrpc-server-utils") (r "^11.0") (d #t) (k 0)) (d (n "unicase") (r "^2.0") (d #t) (k 0)))) (h "1sgq4bkgyixg7zd7fj6myf376zszrh7a6kb98hclvw5bgwggbddm")))

