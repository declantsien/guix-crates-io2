(define-module (crates-io su sy susywasmi-validation) #:use-module (crates-io))

(define-public crate-susywasmi-validation-0.1.0 (c (n "susywasmi-validation") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "susywasm") (r "^0.31") (k 0)))) (h "0jx1zjadngzvvk5ngzga3qn3hi2snp2x1ikkm0sa2l42md1l9j6d") (f (quote (("std" "susywasm/std") ("default" "std") ("core" "hashbrown/nightly"))))))

