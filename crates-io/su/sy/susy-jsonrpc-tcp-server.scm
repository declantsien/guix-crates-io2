(define-module (crates-io su sy susy-jsonrpc-tcp-server) #:use-module (crates-io))

(define-public crate-susy-jsonrpc-tcp-server-10.1.0 (c (n "susy-jsonrpc-tcp-server") (v "10.1.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "susy-jsonrpc-core") (r "^10.1") (d #t) (k 0)) (d (n "susy-jsonrpc-server-utils") (r "^10.1") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1") (d #t) (k 0)))) (h "07z3fziki87xx8pns8h9dr6w72mrxqnssc1z7h5kvlfdis17hsjn")))

(define-public crate-susy-jsonrpc-tcp-server-10.0.1 (c (n "susy-jsonrpc-tcp-server") (v "10.0.1") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "susy-jsonrpc-core") (r "^10.0") (d #t) (k 0)) (d (n "susy-jsonrpc-server-utils") (r "^10.0") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1") (d #t) (k 0)))) (h "09x99khb29n1nvbyyqg79lipgk7ifs4j4igw5yyd5rhc4a2p3ls0")))

