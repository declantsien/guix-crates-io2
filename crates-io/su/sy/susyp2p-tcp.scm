(define-module (crates-io su sy susyp2p-tcp) #:use-module (crates-io))

(define-public crate-susyp2p-tcp-0.6.0 (c (n "susyp2p-tcp") (v "0.6.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "multiaddr") (r "^0.2.0") (d #t) (k 0) (p "susy-multiaddr")) (d (n "susyp2p-core") (r "^0.6.0") (d #t) (k 0)) (d (n "tk-listen") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-tcp") (r "^0.1") (d #t) (k 0)))) (h "1v8v8rqzx0fv0lakbicj5f9wbnpp21jbkfab0w6w2cbvwm6fcvhx")))

(define-public crate-susyp2p-tcp-0.7.0 (c (n "susyp2p-tcp") (v "0.7.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "get_if_addrs") (r "^0.5.3") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "multiaddr") (r "^0.4.0") (d #t) (k 0) (p "susy-multiaddr")) (d (n "susyp2p-core") (r "^0.7.0") (d #t) (k 0)) (d (n "tk-listen") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-tcp") (r "^0.1") (d #t) (k 0)))) (h "0632qc92fgjnpm56g5xhiwd0sm9d7b0w565c16wdnmlc7hn04grk")))

