(define-module (crates-io su sy susy-rocksdb) #:use-module (crates-io))

(define-public crate-susy-rocksdb-0.5.0 (c (n "susy-rocksdb") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "susy-rocksdb-sys") (r "^0.5") (d #t) (k 0)))) (h "12yja5rmnyn14hw12gmbdd779416j8x6h3p3l9cdvy6jp623vchf") (f (quote (("valgrind") ("default"))))))

