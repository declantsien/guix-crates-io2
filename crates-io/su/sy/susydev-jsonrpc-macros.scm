(define-module (crates-io su sy susydev-jsonrpc-macros) #:use-module (crates-io))

(define-public crate-susydev-jsonrpc-macros-10.0.1 (c (n "susydev-jsonrpc-macros") (v "10.0.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "susydev-jsonrpc-core") (r "^10.0") (d #t) (k 0)) (d (n "susydev-jsonrpc-pubsub") (r "^10.0") (d #t) (k 0)) (d (n "susydev-jsonrpc-tcp-server") (r "^10.0") (d #t) (k 2)))) (h "1b2rk0d9v3v5kzilj9n0ky7pz6s46xs1x2ch63bl6s6h708z0f7y")))

