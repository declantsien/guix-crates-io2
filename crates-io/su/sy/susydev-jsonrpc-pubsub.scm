(define-module (crates-io su sy susydev-jsonrpc-pubsub) #:use-module (crates-io))

(define-public crate-susydev-jsonrpc-pubsub-10.0.1 (c (n "susydev-jsonrpc-pubsub") (v "10.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "susydev-jsonrpc-core") (r "^10.0") (d #t) (k 0)) (d (n "susydev-jsonrpc-tcp-server") (r "^10.0") (d #t) (k 2)))) (h "1szdx2jy845b495716077bppd8l7cl351shqqy8i200izwz3cvq9")))

(define-public crate-susydev-jsonrpc-pubsub-10.1.0 (c (n "susydev-jsonrpc-pubsub") (v "10.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "susydev-jsonrpc-core") (r "^10.1") (d #t) (k 0)) (d (n "susydev-jsonrpc-tcp-server") (r "^10.1") (d #t) (k 2)))) (h "0vhb4nbs9csga0l80fa6yy9sbd8cvf9sbgw8cyq7hs9k7ni9qc5j")))

(define-public crate-susydev-jsonrpc-pubsub-11.0.0 (c (n "susydev-jsonrpc-pubsub") (v "11.0.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "susydev-jsonrpc-core") (r "^11.0") (d #t) (k 0)) (d (n "susydev-jsonrpc-tcp-server") (r "^11.0") (d #t) (k 2)))) (h "0r48z86r29ln3kkz4w02s2iivig80fkv3zcbz68bkvbj60x6bxhr")))

