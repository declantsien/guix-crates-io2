(define-module (crates-io su sy susyabi-derive) #:use-module (crates-io))

(define-public crate-susyabi-derive-6.0.2 (c (n "susyabi-derive") (v "6.0.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "susyabi") (r "^6.0") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "07530s1iw4mwk6fsr26dl69w4xy9krkr1vrqyqd827sm04hf6343")))

