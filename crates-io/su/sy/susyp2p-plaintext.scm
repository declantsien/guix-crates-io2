(define-module (crates-io su sy susyp2p-plaintext) #:use-module (crates-io))

(define-public crate-susyp2p-plaintext-0.6.0 (c (n "susyp2p-plaintext") (v "0.6.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "susyp2p-core") (r "^0.6.0") (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "03y1iim8445csw8axmrizdl9n3mbp0bh1lyir3xcaqchdkx3d6b4")))

(define-public crate-susyp2p-plaintext-0.7.0 (c (n "susyp2p-plaintext") (v "0.7.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "susyp2p-core") (r "^0.7.0") (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "06jzhadjfi26ka56773975pfl4692npdjn7bm5gpyn62dfxfwsnk")))

