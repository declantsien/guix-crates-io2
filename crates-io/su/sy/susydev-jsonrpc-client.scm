(define-module (crates-io su sy susydev-jsonrpc-client) #:use-module (crates-io))

(define-public crate-susydev-jsonrpc-client-11.0.0 (c (n "susydev-jsonrpc-client") (v "11.0.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "~0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "susydev-jsonrpc-core") (r "^11.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)))) (h "0mm7qj7pc6axwdg0ikrkqf92fx8ildnx97zixrn2xqs9fp6l0lbc")))

