(define-module (crates-io su sy susydev-rocksdb) #:use-module (crates-io))

(define-public crate-susydev-rocksdb-0.5.0 (c (n "susydev-rocksdb") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)) (d (n "susydev-rocksdb-sys") (r "^0.5") (d #t) (k 0)))) (h "0ls7psq0qmnsy21zcxg4sywkhv37gq53rfm26i7l519id00zxh8v") (f (quote (("valgrind") ("default"))))))

