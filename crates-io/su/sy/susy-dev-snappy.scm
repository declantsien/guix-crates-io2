(define-module (crates-io su sy susy-dev-snappy) #:use-module (crates-io))

(define-public crate-susy-dev-snappy-0.1.0 (c (n "susy-dev-snappy") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "susy-dev-snappy-sys") (r "^0.1") (d #t) (k 0)))) (h "1hb1715cl773qw7x35gvj5p60w7adv2a5q003bg08c30ijdagnd4") (y #t)))

