(define-module (crates-io su sy susy-dev-snappy-sys) #:use-module (crates-io))

(define-public crate-susy-dev-snappy-sys-0.1.2 (c (n "susy-dev-snappy-sys") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0caivmyqnqn0lrk9mjy8vbdlcaxq4gzz7083lcp3jrv0fkjglhbp") (f (quote (("default")))) (y #t) (l "snappy")))

