(define-module (crates-io su sy susy-trie-standardmap) #:use-module (crates-io))

(define-public crate-susy-trie-standardmap-0.1.1 (c (n "susy-trie-standardmap") (v "0.1.1") (d (list (d (n "ethereum-types") (r "^0.4") (d #t) (k 0)) (d (n "keccak-hash") (r "^0.1") (d #t) (k 0)) (d (n "rlp") (r "^0.3.0") (d #t) (k 0)))) (h "1rmzm4274jj9av4kpxfcjd40hhrs7pdm2xz93rl24ndz9iisplmr")))

(define-public crate-susy-trie-standardmap-0.12.2 (c (n "susy-trie-standardmap") (v "0.12.2") (d (list (d (n "criterion") (r "^0.2.8") (d #t) (k 0)) (d (n "hash-db") (r "^0.12.2") (d #t) (k 0)) (d (n "keccak-hasher") (r "^0.12.2") (d #t) (k 0)))) (h "1c0nahx61awlrbx4q0z0cpli287vj0m8b4f0w8blmvpkfwin8h12")))

(define-public crate-susy-trie-standardmap-0.12.0 (c (n "susy-trie-standardmap") (v "0.12.0") (d (list (d (n "criterion") (r "^0.2.8") (d #t) (k 0)) (d (n "hash-db") (r "^0.12.0") (d #t) (k 0)) (d (n "keccak-hasher") (r "^0.12.0") (d #t) (k 0)))) (h "0pj4jhwhmvk01h5bsa7jx159x4jjzbq4vkdlm1jy77cncmacb9nr")))

