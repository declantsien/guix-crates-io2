(define-module (crates-io su sy susyp2p-yamux) #:use-module (crates-io))

(define-public crate-susyp2p-yamux-0.6.0 (c (n "susyp2p-yamux") (v "0.6.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "susyp2p-core") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "yamux") (r "^0.1.9") (d #t) (k 0)))) (h "0xnc0612jadiviqh51fw20i60j3xvlikiywr031w9dfg7lzxh30i")))

(define-public crate-susyp2p-yamux-0.7.0 (c (n "susyp2p-yamux") (v "0.7.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "susyp2p-core") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "yamux") (r "^0.2.0") (d #t) (k 0)))) (h "0c114z45y379as5vbpgachd3zfyzfqs3rzfrnqv42mgjr1ppmia1")))

