(define-module (crates-io su sy susy-jsonrpc-pubsub) #:use-module (crates-io))

(define-public crate-susy-jsonrpc-pubsub-10.1.0 (c (n "susy-jsonrpc-pubsub") (v "10.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "susy-jsonrpc-core") (r "^10.1") (d #t) (k 0)) (d (n "susy-jsonrpc-tcp-server") (r "^10.1") (d #t) (k 2)))) (h "0g1r8n1aifl1p6yxjaw8phvs09bgnsj7csp18ly7c7hmhxjggbwd")))

(define-public crate-susy-jsonrpc-pubsub-10.0.1 (c (n "susy-jsonrpc-pubsub") (v "10.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "susy-jsonrpc-core") (r "^10.0") (d #t) (k 0)) (d (n "susy-jsonrpc-tcp-server") (r "^10.0") (d #t) (k 2)))) (h "04xj8rsjwgj1i5w77srrnh3fxshjz331kzma9y1dy030mys8zs6w")))

