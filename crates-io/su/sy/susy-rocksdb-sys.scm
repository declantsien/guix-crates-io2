(define-module (crates-io su sy susy-rocksdb-sys) #:use-module (crates-io))

(define-public crate-susy-rocksdb-sys-0.5.5 (c (n "susy-rocksdb-sys") (v "0.5.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)))) (h "0lxg6impssgglafn03fr63s8rcf2dyxrn81hansq93biwh430h5q") (f (quote (("default")))) (l "rocksdb")))

