(define-module (crates-io su sy susy-multihash) #:use-module (crates-io))

(define-public crate-susy-multihash-0.1.0 (c (n "susy-multihash") (v "0.1.0") (d (list (d (n "blake2") (r "^0.8") (k 0)) (d (n "rand") (r "^0.6") (f (quote ("std"))) (k 0)) (d (n "sha-1") (r "^0.8") (k 0)) (d (n "sha2") (r "^0.8") (k 0)) (d (n "sha3") (r "^0.8") (k 0)) (d (n "unsigned-varint") (r "^0.2") (d #t) (k 0)))) (h "1qm86w7jpmr1vdc2ms6mnmmg4gg57mbqw924bv7jwl9bxj254zwx")))

(define-public crate-susy-multihash-0.1.1 (c (n "susy-multihash") (v "0.1.1") (d (list (d (n "blake2") (r "^0.8") (k 0)) (d (n "rand") (r "^0.6") (f (quote ("std"))) (k 0)) (d (n "sha-1") (r "^0.8") (k 0)) (d (n "sha2") (r "^0.8") (k 0)) (d (n "sha3") (r "^0.8") (k 0)) (d (n "unsigned-varint") (r "^0.2") (d #t) (k 0)))) (h "18v3srvls5ysn1rnwfsw433701y1rnp2vr3diksslhcgbcliflr3")))

