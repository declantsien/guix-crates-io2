(define-module (crates-io su sy susy-wasmi-validation) #:use-module (crates-io))

(define-public crate-susy-wasmi-validation-0.1.0 (c (n "susy-wasmi-validation") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "susy-wasm") (r "^0.31") (k 0)))) (h "1m635drc6jhv2rjqx1y4i8fcsfyzglivyyz36vv1gp1anbnljm3j") (f (quote (("std" "susy-wasm/std") ("default" "std") ("core" "hashbrown/nightly"))))))

