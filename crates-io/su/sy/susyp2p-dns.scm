(define-module (crates-io su sy susyp2p-dns) #:use-module (crates-io))

(define-public crate-susyp2p-dns-0.6.0 (c (n "susyp2p-dns") (v "0.6.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "multiaddr") (r "^0.2.0") (d #t) (k 0) (p "susy-multiaddr")) (d (n "susyp2p-core") (r "^0.6.0") (d #t) (k 0)) (d (n "susyp2p-tcp") (r "^0.6.0") (d #t) (k 2)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-dns-unofficial") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0gnbj6rzg0bvfpmglwjn67h0y5jbjm3sv664ixamlnmqjqx71zcq")))

(define-public crate-susyp2p-dns-0.7.0 (c (n "susyp2p-dns") (v "0.7.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "multiaddr") (r "^0.4.0") (d #t) (k 0) (p "susy-multiaddr")) (d (n "susyp2p-core") (r "^0.7.0") (d #t) (k 0)) (d (n "susyp2p-tcp") (r "^0.7.0") (d #t) (k 2)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-dns-unofficial") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "18wkh9jj7xlx5payzpdsf37jkx78jh3x2zk54krwyzbb6bbbifgn")))

