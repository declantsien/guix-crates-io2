(define-module (crates-io su sy susync-macros) #:use-module (crates-io))

(define-public crate-susync-macros-0.1.0 (c (n "susync-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1s2p40iakdz3arvyhdn36n0as92ixzy9ndzhp1i0rb1cnqfpg1zl")))

