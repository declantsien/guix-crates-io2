(define-module (crates-io su sy susync) #:use-module (crates-io))

(define-public crate-susync-0.1.0 (c (n "susync") (v "0.1.0") (d (list (d (n "spin") (r "^0.9.8") (f (quote ("spin_mutex"))) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0wc2nxrn7hcwmc50q4k51s1lm8lai8293qxy2ax96b18f949lm9l")))

(define-public crate-susync-0.1.1 (c (n "susync") (v "0.1.1") (d (list (d (n "spin") (r "^0.9.8") (f (quote ("spin_mutex"))) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1iafiyi32izx40mzpnxbiijzsjypvn02x1r8jzj9jp524xz5dr2v")))

(define-public crate-susync-0.1.2 (c (n "susync") (v "0.1.2") (d (list (d (n "spin") (r "^0.9.8") (f (quote ("spin_mutex"))) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.3") (d #t) (k 2)))) (h "03i8pfhsh07sg02pvnrw4avrfyf06cgaxn2gssviwpb518b6imwi")))

(define-public crate-susync-0.2.0 (c (n "susync") (v "0.2.0") (d (list (d (n "spin") (r "^0.9.8") (f (quote ("spin_mutex"))) (k 0)) (d (n "susync-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.51") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4.3") (d #t) (k 2)))) (h "1mn2x9mlfnpi4yww5x3a8dxin2xld04apqdrk71vw6wgr0rv9v2a")))

