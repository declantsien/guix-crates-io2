(define-module (crates-io su sy susyp2p-observed-address) #:use-module (crates-io))

(define-public crate-susyp2p-observed-address-0.6.0 (c (n "susyp2p-observed-address") (v "0.6.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "susyp2p-core") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.2.1") (f (quote ("codec"))) (d #t) (k 0)))) (h "0a4ha1v2lz6icy8hab62zs6ipj7n96aza2mpfc3q1jcqwlc9fddd")))

(define-public crate-susyp2p-observed-address-0.7.0 (c (n "susyp2p-observed-address") (v "0.7.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "susyp2p-core") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 2)) (d (n "tokio-codec") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "unsigned-varint") (r "^0.2.1") (f (quote ("codec"))) (d #t) (k 0)))) (h "1hcb7r2v3vhzxak9hdgwsb9mhzfs93h1wdi2f3j7fl0hzfmil3w9")))

