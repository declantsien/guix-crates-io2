(define-module (crates-io su sy susy-susy-pwasm-utils) #:use-module (crates-io))

(define-public crate-susy-susy-pwasm-utils-0.6.2 (c (n "susy-susy-pwasm-utils") (v "0.6.2") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "diff") (r "^0.1.11") (d #t) (k 2)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (k 0)) (d (n "susy-wasm") (r "^0.31") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "wabt") (r "^0.2") (d #t) (k 2)))) (h "1zyc1jrkz6v74lankhy39w3ivv5addzdgzjz6ldkkdyx2yxsx66k") (f (quote (("std" "susy-wasm/std" "log/std" "byteorder/std") ("default" "std")))) (y #t)))

