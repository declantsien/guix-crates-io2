(define-module (crates-io su sy susy-jsonrpc-http-server) #:use-module (crates-io))

(define-public crate-susy-jsonrpc-http-server-10.1.0 (c (n "susy-jsonrpc-http-server") (v "10.1.0") (d (list (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "susy-jsonrpc-core") (r "^10.1") (d #t) (k 0)) (d (n "susy-jsonrpc-server-utils") (r "^10.1") (d #t) (k 0)) (d (n "unicase") (r "^2.0") (d #t) (k 0)))) (h "1wra4iawh2l7zsmhj283pxp6gmy3w4gsqr4avqr13qhqc0bvajxw")))

(define-public crate-susy-jsonrpc-http-server-10.0.1 (c (n "susy-jsonrpc-http-server") (v "10.0.1") (d (list (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)) (d (n "susy-jsonrpc-core") (r "^10.0") (d #t) (k 0)) (d (n "susy-jsonrpc-server-utils") (r "^10.0") (d #t) (k 0)) (d (n "unicase") (r "^2.0") (d #t) (k 0)))) (h "1yfa80mmz2s187v2kiqbrvnm2kmk23pdg6hp51fdh1h6givqcjic")))

