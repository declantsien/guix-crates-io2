(define-module (crates-io su sy susy-snappy-sys-dev) #:use-module (crates-io))

(define-public crate-susy-snappy-sys-dev-0.1.2 (c (n "susy-snappy-sys-dev") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0an5xjvwpazx8y7ycpprh6z0gdg6xd0aigsx5hmdi6v3zqy9gvi9") (f (quote (("default")))) (y #t) (l "snappy")))

