(define-module (crates-io su sy susy-jsonrpc-test) #:use-module (crates-io))

(define-public crate-susy-jsonrpc-test-10.1.0 (c (n "susy-jsonrpc-test") (v "10.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "susy-jsonrpc-core") (r "^10.0") (d #t) (k 0)) (d (n "susy-jsonrpc-derive") (r "^10.0") (d #t) (k 2)) (d (n "susy-jsonrpc-pubsub") (r "^10.0") (d #t) (k 0)))) (h "178ylwliphxwhqji8als211fbh39291vyhfcn1n3ij4bq3amcalq")))

(define-public crate-susy-jsonrpc-test-10.0.1 (c (n "susy-jsonrpc-test") (v "10.0.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "susy-jsonrpc-core") (r "^10.0") (d #t) (k 0)) (d (n "susy-jsonrpc-derive") (r "^10.0") (d #t) (k 2)) (d (n "susy-jsonrpc-pubsub") (r "^10.0") (d #t) (k 0)))) (h "086jkll8y6sjs8yf3j0y8hq920adbsxs3jq7znyqa7z67h2ks5f5")))

