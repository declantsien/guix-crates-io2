(define-module (crates-io su sy susy-jsonrpc-ws-server) #:use-module (crates-io))

(define-public crate-susy-jsonrpc-ws-server-10.1.0 (c (n "susy-jsonrpc-ws-server") (v "10.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "susy-jsonrpc-core") (r "^10.1") (d #t) (k 0)) (d (n "susy-jsonrpc-server-utils") (r "^10.1") (d #t) (k 0)) (d (n "ws") (r "^0.8") (d #t) (k 0)))) (h "0rllavrx2zrqrf4via0vslv818sw4smjl9h645b7mpvhfgpb33fm")))

(define-public crate-susy-jsonrpc-ws-server-10.0.1 (c (n "susy-jsonrpc-ws-server") (v "10.0.1") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "susy-jsonrpc-core") (r "^10.0") (d #t) (k 0)) (d (n "susy-jsonrpc-server-utils") (r "^10.0") (d #t) (k 0)) (d (n "susy-ws") (r "^0.8") (d #t) (k 0)))) (h "12aq5cchyhksmx9cabzyv8lbv5sggia31lh4aijybay5z63jld79")))

