(define-module (crates-io su sy susyp2p-core-derive) #:use-module (crates-io))

(define-public crate-susyp2p-core-derive-0.6.0 (c (n "susyp2p-core-derive") (v "0.6.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1ys14gkr5r4m7f5kkjv8pncnl12fp3afljwldi4zk4jq00jib90f")))

(define-public crate-susyp2p-core-derive-0.7.0 (c (n "susyp2p-core-derive") (v "0.7.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0hsspa615vx02vx43gy66qmgckbfmkxab2q65hw34gk7x9qz5wvv")))

