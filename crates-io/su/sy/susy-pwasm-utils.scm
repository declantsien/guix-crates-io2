(define-module (crates-io su sy susy-pwasm-utils) #:use-module (crates-io))

(define-public crate-susy-pwasm-utils-0.6.2 (c (n "susy-pwasm-utils") (v "0.6.2") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "diff") (r "^0.1.11") (d #t) (k 2)) (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (k 0)) (d (n "susy-wasm") (r "^0.31") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "wabt") (r "^0.2") (d #t) (k 2)))) (h "1nlifss0zwq90f9fxqsr2kmg7c0pjxrm54vspg6fpvnaa5fd6cpf") (f (quote (("std" "susy-wasm/std" "log/std" "byteorder/std") ("default" "std"))))))

(define-public crate-susy-pwasm-utils-0.6.1 (c (n "susy-pwasm-utils") (v "0.6.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "diff") (r "^0.1.11") (d #t) (k 2)) (d (n "log") (r "^0.4") (k 0)) (d (n "susy-wasm") (r "^0.31") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "wabt") (r "^0.2") (d #t) (k 2)))) (h "086453jbbpab9mk3z2vzshvxjmsrd32vwn142fzmw9ps6wpn5wcg") (f (quote (("std" "susy-wasm/std" "log/std" "byteorder/std") ("default" "std"))))))

