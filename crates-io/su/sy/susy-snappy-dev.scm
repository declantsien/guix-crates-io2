(define-module (crates-io su sy susy-snappy-dev) #:use-module (crates-io))

(define-public crate-susy-snappy-dev-0.1.0 (c (n "susy-snappy-dev") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "susy-snappy-sys-dev") (r "^0.1") (d #t) (k 0)))) (h "0vyiy1k8jxs3720k5p3myx974g7p3ijm8m4mv9jd5c456yy6ckcf") (y #t)))

