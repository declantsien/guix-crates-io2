(define-module (crates-io su sy susy-tokio-ipc) #:use-module (crates-io))

(define-public crate-susy-tokio-ipc-0.1.0 (c (n "susy-tokio-ipc") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mio-named-pipes") (r "^0.1") (d #t) (k 0)) (d (n "miow") (r "~0.3.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-named-pipes") (r "^0.1") (d #t) (k 0)) (d (n "tokio-uds") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "winnt" "accctrl" "aclapi" "securitybaseapi" "minwinbase" "winbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1fxrk704kibhspqmhcl5h4qddjcj7vy38khah23s1whwk1vkjkdy")))

