(define-module (crates-io su sy susydev-rocksdb-sys) #:use-module (crates-io))

(define-public crate-susydev-rocksdb-sys-0.5.5 (c (n "susydev-rocksdb-sys") (v "0.5.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "local-encoding") (r "^0.2.0") (d #t) (k 0)))) (h "1hm380cq45ih7x1h44jc33hzyig8f9b8sn4jq17vciqqkcppwifv") (f (quote (("default")))) (l "rocksdb")))

