(define-module (crates-io su sy susydev-jsonrpc-ws-server) #:use-module (crates-io))

(define-public crate-susydev-jsonrpc-ws-server-10.0.1 (c (n "susydev-jsonrpc-ws-server") (v "10.0.1") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "susy-ws") (r "^0.8") (d #t) (k 0)) (d (n "susydev-jsonrpc-core") (r "^10.0") (d #t) (k 0)) (d (n "susydev-jsonrpc-server-utils") (r "^10.0") (d #t) (k 0)))) (h "0jnhk7y9sa9p8fzjf1x13bxz3drifwz4zxgasfbmvls3nybqxxp0")))

(define-public crate-susydev-jsonrpc-ws-server-10.1.0 (c (n "susydev-jsonrpc-ws-server") (v "10.1.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "susy-ws") (r "^0.8") (d #t) (k 0)) (d (n "susydev-jsonrpc-core") (r "^10.1") (d #t) (k 0)) (d (n "susydev-jsonrpc-server-utils") (r "^10.1") (d #t) (k 0)))) (h "0jy1vh850wnaqzdgf2c2hh906lxcnxbvlp8ghjc2dxh7y9hq2z7l")))

(define-public crate-susydev-jsonrpc-ws-server-11.0.0 (c (n "susydev-jsonrpc-ws-server") (v "11.0.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "susydev-jsonrpc-core") (r "^11.0") (d #t) (k 0)) (d (n "susydev-jsonrpc-server-utils") (r "^11.0") (d #t) (k 0)) (d (n "ws") (r "^0.8") (d #t) (k 0)))) (h "04pv3b2dml79lx664ch28l244s0ix8zdny7ylp638vn6s6xid099")))

