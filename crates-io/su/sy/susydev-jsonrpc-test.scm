(define-module (crates-io su sy susydev-jsonrpc-test) #:use-module (crates-io))

(define-public crate-susydev-jsonrpc-test-10.0.1 (c (n "susydev-jsonrpc-test") (v "10.0.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "susydev-jsonrpc-core") (r "^10.0") (d #t) (k 0)) (d (n "susydev-jsonrpc-derive") (r "^10.0") (d #t) (k 2)) (d (n "susydev-jsonrpc-pubsub") (r "^10.0") (d #t) (k 0)))) (h "1lqdfahdxb2b2di46zffmvxcnm3322r0pfpanhnsh2llw9xwpviq")))

