(define-module (crates-io su sy susy-jsonrpc-macros) #:use-module (crates-io))

(define-public crate-susy-jsonrpc-macros-10.1.0 (c (n "susy-jsonrpc-macros") (v "10.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "susy-jsonrpc-core") (r "^10.1") (d #t) (k 0)) (d (n "susy-jsonrpc-pubsub") (r "^10.1") (d #t) (k 0)) (d (n "susy-jsonrpc-tcp-server") (r "^10.1") (d #t) (k 2)))) (h "11vsnranqw2d21r1qs6h19xwjj96b4dqgk5898l8ypd0z6ra6m2z")))

