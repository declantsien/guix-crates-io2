(define-module (crates-io su sy susyp2p-ratelimit) #:use-module (crates-io))

(define-public crate-susyp2p-ratelimit-0.6.0 (c (n "susyp2p-ratelimit") (v "0.6.0") (d (list (d (n "aio-limited") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "susyp2p-core") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0yqhrixg7qsgidyd64nkm06k4rfs151q986psa44l370f231ccd8")))

(define-public crate-susyp2p-ratelimit-0.7.0 (c (n "susyp2p-ratelimit") (v "0.7.0") (d (list (d (n "aio-limited") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "susyp2p-core") (r "^0.7.0") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "10176b77084kyvaa9bf73ns11bjrmbqyig91rrzxw2j6pcqzch1p")))

