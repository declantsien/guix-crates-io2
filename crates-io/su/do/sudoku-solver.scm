(define-module (crates-io su do sudoku-solver) #:use-module (crates-io))

(define-public crate-sudoku-solver-0.2.1 (c (n "sudoku-solver") (v "0.2.1") (d (list (d (n "bit-iter") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0j43byqj96887hvy019qdngxxx7hniblsmwk422sb40cwsjnvwpx")))

(define-public crate-sudoku-solver-0.2.2 (c (n "sudoku-solver") (v "0.2.2") (d (list (d (n "bit-iter") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0xrzpdjk42lxysdqiixi1703bwmq2jr5qn1mrgw7z63rhpdmpg00")))

(define-public crate-sudoku-solver-0.3.0 (c (n "sudoku-solver") (v "0.3.0") (d (list (d (n "bit-iter") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0fgyzm633z4zr2mg7j8gshc5y5g40f66q7lxcpng1l4glf04d7j7")))

(define-public crate-sudoku-solver-0.4.0 (c (n "sudoku-solver") (v "0.4.0") (d (list (d (n "bit-iter") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1gdwbp0125av942qjl10mfaqgm4297w0jfryyd5l122bbkpp747r")))

(define-public crate-sudoku-solver-0.5.0 (c (n "sudoku-solver") (v "0.5.0") (d (list (d (n "bit-iter") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1a0m32lwnaz0wjdmpxq6aril9dwgijwsfs8qjd5y4q71cy8k30a2")))

(define-public crate-sudoku-solver-0.6.1 (c (n "sudoku-solver") (v "0.6.1") (d (list (d (n "bit-iter") (r "^1.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0mkg74dh3xcfx6dprw4l7r9hmdmgvj5l04rp8y2y44l36hyy2blb")))

