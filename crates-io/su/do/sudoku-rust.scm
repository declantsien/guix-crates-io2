(define-module (crates-io su do sudoku-rust) #:use-module (crates-io))

(define-public crate-sudoku-rust-0.1.0 (c (n "sudoku-rust") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "03166w7zccmg2r92bhpr2xajb8j1qk40m1payydivf78j3bwqh48")))

(define-public crate-sudoku-rust-0.2.0 (c (n "sudoku-rust") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "06hjmzxxdxzizq1q8n3x4gpsi29zwj9z3ma5qyb40pc3zgm5g39d")))

(define-public crate-sudoku-rust-0.3.0 (c (n "sudoku-rust") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "16j4h7sna6w94npydrz6n8p9w2kg1c7lm6g4i630mm9gxs4zd85d")))

