(define-module (crates-io su do sudo_pair) #:use-module (crates-io))

(define-public crate-sudo_pair-0.9.0 (c (n "sudo_pair") (v "0.9.0") (d (list (d (n "error-chain") (r "~0") (d #t) (k 0)) (d (n "libc") (r "~0") (d #t) (k 0)) (d (n "sudo_plugin") (r "~1") (d #t) (k 0)))) (h "0ns9a7m5zw702y1yyvi68m9z3dmg6n0nncywc789h3ly1767s92b")))

(define-public crate-sudo_pair-0.9.1 (c (n "sudo_pair") (v "0.9.1") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "sudo_plugin") (r "^1") (d #t) (k 0)))) (h "0y3blvp5i92y0wxisxq2rsgycjp4bs6a0q570p4hjw9g0acp0li9")))

(define-public crate-sudo_pair-0.9.2 (c (n "sudo_pair") (v "0.9.2") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "sudo_plugin") (r "^1.1") (d #t) (k 0)))) (h "0jy78kkvdj1clvr7vvlkzm73v6l0shhszh323m168066w0px2vsg")))

(define-public crate-sudo_pair-0.11.0 (c (n "sudo_pair") (v "0.11.0") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "sudo_plugin") (r "^1.1") (d #t) (k 0)))) (h "057bsjic0zkyv1nlfq8wsv50xdxaya3zifaiqjyp79q8wqijh1kh")))

(define-public crate-sudo_pair-0.11.1 (c (n "sudo_pair") (v "0.11.1") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "sudo_plugin") (r "^1.1") (d #t) (k 0)))) (h "1hgsq3ccnwxmj40dh21dy0bc0sd960izxyhc7h2rd928djk6if11")))

(define-public crate-sudo_pair-1.0.0 (c (n "sudo_pair") (v "1.0.0") (d (list (d (n "failure") (r "^0") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "sudo_plugin") (r "^1.2") (d #t) (k 0)))) (h "18q4lnxpl7k3kiklg633wdzqhs5hav9g65ar0xibmhf2y9p4jcr7")))

