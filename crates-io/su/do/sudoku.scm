(define-module (crates-io su do sudoku) #:use-module (crates-io))

(define-public crate-sudoku-0.1.0 (c (n "sudoku") (v "0.1.0") (h "01kbc4i0nn51lxvryzq0pa22wnyy2xaad8hkwq152gfga92cmabl")))

(define-public crate-sudoku-0.1.1 (c (n "sudoku") (v "0.1.1") (h "1cgl3556bl8d8cv79dyfh6ri43g5nk53mapp3xhr2afaq2iz0vbb")))

(define-public crate-sudoku-0.1.2 (c (n "sudoku") (v "0.1.2") (h "1p7cabmal5npir4ijnilgry2i468v7w2i7xzlqinqh2cjfn1rj4p")))

(define-public crate-sudoku-0.1.3 (c (n "sudoku") (v "0.1.3") (h "0vv1spz84kl2lsjqgila5d38isgfbwl1rnahx5k9f84hmqik6hql")))

(define-public crate-sudoku-0.2.0 (c (n "sudoku") (v "0.2.0") (h "08f3mwf1zzq3w9s2l7xdqjw1qrpiphfp0haw0x745vfyifs2gg5d") (y #t)))

(define-public crate-sudoku-0.2.1 (c (n "sudoku") (v "0.2.1") (h "1kasbcj69h000fg9862ylkx990ppfqq2b5q3lbc6vmf4czvw67b5")))

(define-public crate-sudoku-0.3.0 (c (n "sudoku") (v "0.3.0") (h "1l6x8bwlhldmhss2kyrjfy0hb1yvqymiicw83wfi8a84f60sq6vh")))

(define-public crate-sudoku-0.3.1 (c (n "sudoku") (v "0.3.1") (h "1m35l1vwg9i858nh06iqpf8vwg9l7ya3bhsij7abha0yy7kkqvmv")))

(define-public crate-sudoku-0.3.2 (c (n "sudoku") (v "0.3.2") (h "0d12gvcdgwl2306f1ayqzdsfpxdxwbzf9rixndyza3acccbgqnld")))

(define-public crate-sudoku-0.4.0 (c (n "sudoku") (v "0.4.0") (h "0p1hr16f4qcj4yfhdsly9168l8j6y2fwl5l93z4ff60p1i11mci3")))

(define-public crate-sudoku-0.4.1 (c (n "sudoku") (v "0.4.1") (d (list (d (n "rand") (r "^0.3.18") (d #t) (k 0)))) (h "1sqrhxz8lnixq2ibzgqp91iinydkmvpdp1h4ix909yj5j60wv0y6")))

(define-public crate-sudoku-0.5.0 (c (n "sudoku") (v "0.5.0") (d (list (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)))) (h "01pv0alvlrkh6kx544aaya9r4306rg5dvg0c09dwhnnh2xskjfvn") (f (quote (("unchecked_indexing"))))))

(define-public crate-sudoku-0.6.0 (c (n "sudoku") (v "0.6.0") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.64") (o #t) (d #t) (k 0)))) (h "0rg39078glsabwc5cfkiwql6ns56jcafbzi78l6qzpjgw59xa4k9") (f (quote (("unchecked_indexing"))))))

(define-public crate-sudoku-0.6.1 (c (n "sudoku") (v "0.6.1") (d (list (d (n "rand") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.64") (o #t) (d #t) (k 0)))) (h "0d8g86rilnmir1xn2xvdmhc9y8k8l4li4wjls46v4v40aprp9mdv") (f (quote (("unchecked_indexing"))))))

(define-public crate-sudoku-0.6.2 (c (n "sudoku") (v "0.6.2") (d (list (d (n "rand") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.66") (o #t) (d #t) (k 0)))) (h "19b03x49sy8rb95kl986kxgqp957cnnrhqrwqgl276mmqlnq1iyj") (f (quote (("unchecked_indexing"))))))

(define-public crate-sudoku-0.7.0 (c (n "sudoku") (v "0.7.0") (d (list (d (n "crunchy") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (o #t) (d #t) (k 0)))) (h "1r60ac60dsnnl0g7bzwrmgq50j8s1abmz37h6ga2qsq8bfrsfr8y") (f (quote (("unchecked_indexing"))))))

(define-public crate-sudoku-0.8.0 (c (n "sudoku") (v "0.8.0") (d (list (d (n "crunchy") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.19.2") (d #t) (k 2)) (d (n "strum_macros") (r "^0.19.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1w31qm8ydld3zv1mn33i94rkc99k3dyzmggqzv1rz5838mgrp207") (f (quote (("unchecked_indexing"))))))

