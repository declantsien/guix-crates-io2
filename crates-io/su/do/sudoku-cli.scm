(define-module (crates-io su do sudoku-cli) #:use-module (crates-io))

(define-public crate-sudoku-cli-0.1.0 (c (n "sudoku-cli") (v "0.1.0") (d (list (d (n "ansi-escapes") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.1") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "12a3nhf19synyl64gbhnz3a06fl2cjp7wpz7nwzcx2sj66nn0m2z")))

(define-public crate-sudoku-cli-0.1.1 (c (n "sudoku-cli") (v "0.1.1") (d (list (d (n "ansi-escapes") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.1") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "07kvs63b0636d1ciwcws4lk246dbkbx07kyv73jjknink4clcp0k")))

(define-public crate-sudoku-cli-0.1.2 (c (n "sudoku-cli") (v "0.1.2") (d (list (d (n "ansi-escapes") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.1") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "1yqzamhzi7p9ym83h6brav5spqzy78ajiwrd5crj63ipmz59g6gf")))

(define-public crate-sudoku-cli-0.1.3 (c (n "sudoku-cli") (v "0.1.3") (d (list (d (n "ansi-escapes") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.1") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "09kpkl9ls78zq3w78jsrzdm4sys3yg4z1bil20d5s2sr717za1dg")))

(define-public crate-sudoku-cli-0.1.4 (c (n "sudoku-cli") (v "0.1.4") (d (list (d (n "ansi-escapes") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.1") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "0w3g0mwrml7khv7yxvbvd0a7jdjpy91x4h6xsk5x26dkm7b91f4q")))

(define-public crate-sudoku-cli-0.1.5 (c (n "sudoku-cli") (v "0.1.5") (d (list (d (n "ansi-escapes") (r "^0.1.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.1") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "03li27nfvckjf2nyh4kgdsmhjx8hl3rb3ddsqiqy7pqvp1apgd8z")))

(define-public crate-sudoku-cli-0.2.0 (c (n "sudoku-cli") (v "0.2.0") (d (list (d (n "ansi-escapes") (r "^0.1.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.1") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (d #t) (k 0)))) (h "001pv0y1r3s581g3pjllvhdwvk4in21sv699mc6xyc19d7932y78")))

