(define-module (crates-io su do sudo-rs) #:use-module (crates-io))

(define-public crate-sudo-rs-0.1.0 (c (n "sudo-rs") (v "0.1.0") (h "10sbib7fbwrlm8dkz7fz10xnmw27hghhjr1zypcwi9hyac8hmf4w")))

(define-public crate-sudo-rs-0.1.0-dev.20230620 (c (n "sudo-rs") (v "0.1.0-dev.20230620") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "signal-hook") (r "^0.3.15") (f (quote ("extended-siginfo"))) (d #t) (k 0)) (d (n "signal-hook-registry") (r "^1.4.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "10flh63y9d98x1pzs4wa4j7gdyf9a3d6vpg14l2i0mlmv99nrac5") (f (quote (("dev") ("default"))))))

(define-public crate-sudo-rs-0.2.0-dev.20230627 (c (n "sudo-rs") (v "0.2.0-dev.20230627") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "signal-hook") (r "^0.3.15") (f (quote ("extended-siginfo"))) (d #t) (k 0)) (d (n "signal-hook-registry") (r "^1.4.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "0h8qml683vcykk8zr9nc8cj2wdz6w82s0knjl5ji0mbi1q4fpisv") (f (quote (("dev") ("default"))))))

(define-public crate-sudo-rs-0.2.0-dev.20230703 (c (n "sudo-rs") (v "0.2.0-dev.20230703") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "signal-hook") (r "^0.3.15") (f (quote ("extended-siginfo"))) (d #t) (k 0)) (d (n "signal-hook-registry") (r "^1.4.1") (d #t) (k 0)))) (h "0wvf4sdlwnnblfgqd1iz3ka37ljs5gp92lxxgvfqf8hzda64sjnf") (f (quote (("dev") ("default"))))))

(define-public crate-sudo-rs-0.2.0-dev.20230711 (c (n "sudo-rs") (v "0.2.0-dev.20230711") (d (list (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0xma2d3k94h04n67jakkb63jq1z0a06n2ls2dv17fk49jsb9cr7y") (f (quote (("dev") ("default"))))))

(define-public crate-sudo-rs-0.2.0 (c (n "sudo-rs") (v "0.2.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.127") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "1qskv07pgsk1hv424rhca1bvwf814i2l2wjx7wsipjf4y312nzxm") (f (quote (("dev") ("default")))) (r "1.70")))

(define-public crate-sudo-rs-0.2.1 (c (n "sudo-rs") (v "0.2.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.127") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "1vj7xc0wrgdncnrfxr6n2qwwlp3680fs6fc57dqni7xa240wdl7l") (f (quote (("dev") ("default")))) (r "1.70")))

(define-public crate-sudo-rs-0.2.2 (c (n "sudo-rs") (v "0.2.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.127") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (f (quote ("std"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)))) (h "0jg3r4620vkk55wa7rl2377rhns3hmzzj1nhl8vbd4pr4ndnipif") (f (quote (("dev") ("default")))) (r "1.70")))

