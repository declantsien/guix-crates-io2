(define-module (crates-io su do sudokugen) #:use-module (crates-io))

(define-public crate-sudokugen-0.1.0 (c (n "sudokugen") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)))) (h "0z0ridlspwpkcfqa3399vfhwybbz1ipz084hf3sym5vbsqg2cp6l")))

(define-public crate-sudokugen-0.2.0 (c (n "sudokugen") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)))) (h "04f7q8x62lp3srqdx39sgp53xjlpfwwkagj9rbx3bjc7mn5zmgmf")))

(define-public crate-sudokugen-0.3.0 (c (n "sudokugen") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "0jvwpc32vj07m8fkrchi7lkxs1kg640395glyls0s70c7r8i6pd8") (r "1.56")))

(define-public crate-sudokugen-0.3.1 (c (n "sudokugen") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)))) (h "19lpn3jbrwqcv8xbjmy8vl4fap2xivhdk15wqbjyjbf1xa92camr") (r "1.56")))

(define-public crate-sudokugen-0.4.0 (c (n "sudokugen") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.10") (d #t) (k 0)))) (h "0l0af6339vl913av0z8yb8ljv5m1d77029kiwvb31fg71cnbglpg") (r "1.56")))

