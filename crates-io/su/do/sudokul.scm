(define-module (crates-io su do sudokul) #:use-module (crates-io))

(define-public crate-sudokul-0.1.0 (c (n "sudokul") (v "0.1.0") (h "0jpfjs31pnn9wyz750w4gnjr6shsg5ixncbz10wxb29ifsw2xl0n")))

(define-public crate-sudokul-0.2.0 (c (n "sudokul") (v "0.2.0") (h "10cnrwf23zcfxqyi0l82hdv1j7zhsa139dwmbia04ym6rkzp23b8")))

