(define-module (crates-io su do sudoku-tui) #:use-module (crates-io))

(define-public crate-sudoku-tui-0.0.1 (c (n "sudoku-tui") (v "0.0.1") (d (list (d (n "cursive") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sudoku") (r "^0.7") (d #t) (k 0)))) (h "0vsvqhrh1xyinxd3wy7va0m2mnva8bhhffib2q0xs8xlfhg92afm")))

(define-public crate-sudoku-tui-0.0.2 (c (n "sudoku-tui") (v "0.0.2") (d (list (d (n "cursive") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sudoku") (r "^0.7") (d #t) (k 0)))) (h "0psqxyrnx2c3shr93hy3v8s58dzri8zy0jkw18fiscrx3m88bgx8")))

(define-public crate-sudoku-tui-0.0.3 (c (n "sudoku-tui") (v "0.0.3") (d (list (d (n "cursive") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sudoku") (r "^0.7") (d #t) (k 0)))) (h "0c7bc08vms3aa8fzvdgnzqb2xyyhw0wbnk7f0898aa8p1kpxq42a")))

(define-public crate-sudoku-tui-0.0.4 (c (n "sudoku-tui") (v "0.0.4") (d (list (d (n "cursive") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sudoku") (r "^0.7") (d #t) (k 0)))) (h "12n56qmii2d2zpgr8krlglna27mb9q0ffd2fd7xqdv84lgyswgmv")))

(define-public crate-sudoku-tui-0.1.0 (c (n "sudoku-tui") (v "0.1.0") (d (list (d (n "cursive") (r "^0.15") (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sudoku") (r "^0.7") (d #t) (k 0)))) (h "14map7862a8z7f7ik54g80jd71v6b80p01h0wm4y02q4s30xz92d") (f (quote (("default" "cursive/crossterm-backend"))))))

(define-public crate-sudoku-tui-0.2.0 (c (n "sudoku-tui") (v "0.2.0") (d (list (d (n "clock-core") (r "^0.0") (d #t) (k 0)) (d (n "cursive") (r "^0.15") (k 0)) (d (n "hhmmss") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sudoku") (r "^0.7") (d #t) (k 0)))) (h "0ypbql72gdzhs5ybqjpq9aiwig7i7yql8x805c0nksz23kpwvbdr") (f (quote (("default" "cursive/crossterm-backend"))))))

