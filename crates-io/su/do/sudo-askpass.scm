(define-module (crates-io su do sudo-askpass) #:use-module (crates-io))

(define-public crate-sudo-askpass-2.2.0 (c (n "sudo-askpass") (v "2.2.0") (d (list (d (n "ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "inquire") (r "^0.6.2") (d #t) (k 0)) (d (n "read_char") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.22") (d #t) (k 0)) (d (n "termios") (r "^0.3.3") (d #t) (k 0)) (d (n "xdg") (r "^2.5.0") (d #t) (k 0)))) (h "1xq5r76mhjym597gdd1b3ip5i9z0ha6is7snprma1c19cn2w1dpx")))

