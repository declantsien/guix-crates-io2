(define-module (crates-io su do sudo_plugin-sys) #:use-module (crates-io))

(define-public crate-sudo_plugin-sys-1.0.0 (c (n "sudo_plugin-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "~0") (d #t) (k 1)) (d (n "libc") (r "~0") (d #t) (k 0)))) (h "1qikz2j8kswy4wx6xpda6f8dzs8rhvn412zy0b4lc077a1kx06vz")))

(define-public crate-sudo_plugin-sys-1.0.1 (c (n "sudo_plugin-sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "libc") (r "^0") (d #t) (k 0)))) (h "1vhz444sr8d0zkrzpifwd80cd40maqraik3ajqlmnfgz3w7zn85l")))

(define-public crate-sudo_plugin-sys-1.1.0 (c (n "sudo_plugin-sys") (v "1.1.0") (d (list (d (n "bindgen") (r "^0.37") (d #t) (k 1)) (d (n "libc") (r "^0") (d #t) (k 0)))) (h "080k7lkzqz8zhgj74iyk9cmlsi8w8b99qd945spnn5722ds2wf0b")))

(define-public crate-sudo_plugin-sys-1.2.0 (c (n "sudo_plugin-sys") (v "1.2.0") (d (list (d (n "libc") (r "^0") (d #t) (k 0)))) (h "1hqlykpbmdw86qcdknsr8k720crf7s8h8c8y63wsklwbh3qif5rg")))

(define-public crate-sudo_plugin-sys-1.2.1 (c (n "sudo_plugin-sys") (v "1.2.1") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "libc") (r "^0") (d #t) (k 0)))) (h "01289ishg9wmmpyskfbjbwzgd42q9azdrma1mlnlnc2jva5cqaaj")))

