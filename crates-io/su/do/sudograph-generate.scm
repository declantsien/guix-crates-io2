(define-module (crates-io su do sudograph-generate) #:use-module (crates-io))

(define-public crate-sudograph-generate-0.0.0 (c (n "sudograph-generate") (v "0.0.0") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "0xrcyzrcvr58k68kppwf2zj6f3p2k1zassf5d51fm7r500qszy40")))

(define-public crate-sudograph-generate-0.1.0 (c (n "sudograph-generate") (v "0.1.0") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "1kzgzwv8nl4wpmcs66vcmh0rdmh2xqbl9hxw6f8g5wp58kg6l212")))

(define-public crate-sudograph-generate-0.2.0-beta.0 (c (n "sudograph-generate") (v "0.2.0-beta.0") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "0gn52zldizbqns8b80d99dp6z81kqg1nal3nv5pkhnj73z3bcwzb")))

(define-public crate-sudograph-generate-0.2.0-beta.1 (c (n "sudograph-generate") (v "0.2.0-beta.1") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "1paa3cnnaqw8qhvgx7893mylravfj5jyhyr30ks6kvldak9j4gxq")))

(define-public crate-sudograph-generate-0.2.0-beta.2 (c (n "sudograph-generate") (v "0.2.0-beta.2") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "0s3b2d4kv02gh0hx4ah4nvh90pzkg2agy3p93h4cpal8330lrjdj")))

(define-public crate-sudograph-generate-0.2.0-beta.4 (c (n "sudograph-generate") (v "0.2.0-beta.4") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "0bbd8gjx4n0y3nryn6hfyfcnn2fz8qgcrhdk3xwpffyf44c853pi")))

(define-public crate-sudograph-generate-0.2.0-beta.11 (c (n "sudograph-generate") (v "0.2.0-beta.11") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "124sxgj4463zxq5h4qvq9nbg5gkbzkkzdr5dbrm50rhvdvsmxvn5")))

(define-public crate-sudograph-generate-0.2.0-beta.12 (c (n "sudograph-generate") (v "0.2.0-beta.12") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "0fzznclijsj3wfmnjsx27m2ss03mjdc9ndms5v2pwagdwh0p5ls8")))

(define-public crate-sudograph-generate-0.2.0-beta.14 (c (n "sudograph-generate") (v "0.2.0-beta.14") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "1gncs5hzbxl83p3mdf78vwszr0cp2yxynabdn81licx2vymn54j9")))

(define-public crate-sudograph-generate-0.2.0-beta.15 (c (n "sudograph-generate") (v "0.2.0-beta.15") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "0j1rbg8xfdd0zdcvrpxifag5hrpkj9znqphx06mrja0q9q2lvhw4")))

(define-public crate-sudograph-generate-0.2.0-beta.17 (c (n "sudograph-generate") (v "0.2.0-beta.17") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "0brn2qcm4f5ji1xx0bl1plmywdqac2vh8m1ycp9yq4bd70hy9z8z")))

(define-public crate-sudograph-generate-0.2.0-beta.18 (c (n "sudograph-generate") (v "0.2.0-beta.18") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "13qzap3h1j797vjxq0bj12cs1lvzx9lc20xfqk4qwvzgl5dnzj0j")))

(define-public crate-sudograph-generate-0.2.0-beta.19 (c (n "sudograph-generate") (v "0.2.0-beta.19") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "1h44czhvdyvw9ww395snjihfrls6fdkdj9nc235908n75kl4rz4a")))

(define-public crate-sudograph-generate-0.2.0-beta.20 (c (n "sudograph-generate") (v "0.2.0-beta.20") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "0rinfvn9c9fd1kf7cc8j9bxsp8y4i9lpwc71kc3b9z3giq4ykfs0")))

(define-public crate-sudograph-generate-0.2.0-beta.21 (c (n "sudograph-generate") (v "0.2.0-beta.21") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "1i5ckc9qvf775dy9fv2s7cw3kzl6jk9k4kpsmd9rms00hm1iiq7i")))

(define-public crate-sudograph-generate-0.2.0-beta.22 (c (n "sudograph-generate") (v "0.2.0-beta.22") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "1kxfa0pfvmd4krswdv9jnc13a59ipxba853j9p1hy56d4w2izb69")))

(define-public crate-sudograph-generate-0.2.0-beta.23 (c (n "sudograph-generate") (v "0.2.0-beta.23") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "0ba91dlsj0px5bz7zsjf92s10kfq53s5qsfcbbvdnjmvk6bs29vm")))

(define-public crate-sudograph-generate-0.2.0-beta.25 (c (n "sudograph-generate") (v "0.2.0-beta.25") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "0kqzc9sqyr17vwash338b6p6lwfznqh2b2l038v34xsp67v6jsx7")))

(define-public crate-sudograph-generate-0.2.0-beta.26 (c (n "sudograph-generate") (v "0.2.0-beta.26") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "0k481nnqdn96jvgar5c67qxcv2faddlcs0sh30asjvxgx95i84qf")))

(define-public crate-sudograph-generate-0.2.0-beta.27 (c (n "sudograph-generate") (v "0.2.0-beta.27") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "1fm8ayglgq2ps0rl5im8naxwhwfmknkl5628iq6c2wh59c183yy0")))

(define-public crate-sudograph-generate-0.2.0 (c (n "sudograph-generate") (v "0.2.0") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "1ivm0hs58h5cfandbmxsh8j16hgwj1nd89ah2nk28ylccd8vg7hx")))

(define-public crate-sudograph-generate-0.2.1 (c (n "sudograph-generate") (v "0.2.1") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "06my5sgp2lh5ls6vjmf1za10g4k392w4w6dyy5xj68jsgqi0fl1p")))

(define-public crate-sudograph-generate-0.2.2-beta.0 (c (n "sudograph-generate") (v "0.2.2-beta.0") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "1ri8gbn9m2igs9fd285yxcs0jb1z3w53m92cjgfk0hs7asfwxfy7")))

(define-public crate-sudograph-generate-0.2.2-beta.1 (c (n "sudograph-generate") (v "0.2.2-beta.1") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "03lprm9i21f1s0yysc3ilfqyp2qjqa9flkf4z8racakm6sxkf6sn")))

(define-public crate-sudograph-generate-0.2.2 (c (n "sudograph-generate") (v "0.2.2") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "1ir8l6jiv24cicl03nbzhwqaxyjv8wagxnjd0vhp84563rn9z7ms")))

(define-public crate-sudograph-generate-0.3.0-beta.0 (c (n "sudograph-generate") (v "0.3.0-beta.0") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "1bk9rmlg84n8nf0wjis7fkw60byjw05hvh37ibz716nhf1ip64kz")))

(define-public crate-sudograph-generate-0.3.0-beta.1 (c (n "sudograph-generate") (v "0.3.0-beta.1") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "1x3accg6yylvja0hflhmj8fnq9dp6mn0i88016ar1cx8m0by6131")))

(define-public crate-sudograph-generate-0.3.0-beta.2 (c (n "sudograph-generate") (v "0.3.0-beta.2") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "0n5jkf4lkx5hm6pkkvjd58w4fxanc24sdybaf88ifazazc005cv6")))

(define-public crate-sudograph-generate-0.3.0-beta.4 (c (n "sudograph-generate") (v "0.3.0-beta.4") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "02zgkbz56sb4m8hszwmy32xk19by87ad1f2ckasmzvsm5m8gfb9k")))

(define-public crate-sudograph-generate-0.3.0-beta.5 (c (n "sudograph-generate") (v "0.3.0-beta.5") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "05g43gw09m066d15m2fxbjqxm1l02s3jsly38887dbsjinab632n")))

(define-public crate-sudograph-generate-0.3.0-beta.6 (c (n "sudograph-generate") (v "0.3.0-beta.6") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "0al3jmsxdniy0d53kcmp3lpayykbcs7fvic1iw4gs0z59dfv5l32")))

(define-public crate-sudograph-generate-0.3.0-beta.7 (c (n "sudograph-generate") (v "0.3.0-beta.7") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "01jaz6xr8klsfgrb9nz5l01cz46qkdiwzzpf4v99q06cx8n2mvqc")))

(define-public crate-sudograph-generate-0.3.0-beta.8 (c (n "sudograph-generate") (v "0.3.0-beta.8") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "1zxzfzf8kyvx5c9g8jrd22y4idnnkgga897n4iwk3m12nar2sz4p")))

(define-public crate-sudograph-generate-0.3.0-beta.9 (c (n "sudograph-generate") (v "0.3.0-beta.9") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "0qxnyr1aqy0cqg49n28gh5gz0pxayvc7w21qaarbhdblvv1p6hsc")))

(define-public crate-sudograph-generate-0.3.0-beta.10 (c (n "sudograph-generate") (v "0.3.0-beta.10") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "1bk518c36ykjy88zd4hbw367phpv27ggkbdknq2p4wahisdkw7v7")))

(define-public crate-sudograph-generate-0.3.0-beta.11 (c (n "sudograph-generate") (v "0.3.0-beta.11") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "13zd2g5qf6gfgp97z0icclrrg49wbmrfbir43bc3lbgscgj65mp4")))

(define-public crate-sudograph-generate-0.3.0-beta.12 (c (n "sudograph-generate") (v "0.3.0-beta.12") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "144cl01a2rqx15n99wpzv7ydgbq9nm63ss3qdsnda2qhzn8w9q57")))

(define-public crate-sudograph-generate-0.3.0-beta.13 (c (n "sudograph-generate") (v "0.3.0-beta.13") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "0hyn7pxxlcm8c4mqlp548v46paybb2kzf7xcpk9ms27y34xiqa9k")))

(define-public crate-sudograph-generate-0.3.0-beta.14 (c (n "sudograph-generate") (v "0.3.0-beta.14") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "02lwfx3amzfw61rqlybrn0jfnsnibvm1yzs2hc6plp5ma8q95hgh")))

(define-public crate-sudograph-generate-0.3.0-beta.16 (c (n "sudograph-generate") (v "0.3.0-beta.16") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "0adpvifqr6ds3drg72ac5j327gyvbsmzphqwzri1di62fk65xb9h")))

(define-public crate-sudograph-generate-0.3.0-beta.17 (c (n "sudograph-generate") (v "0.3.0-beta.17") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "0mrr24067d59fjy33jbrq8ys2qlmx56rkvfbh547rvd346g0ii3m")))

(define-public crate-sudograph-generate-0.3.0-beta.18 (c (n "sudograph-generate") (v "0.3.0-beta.18") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "0qy8pyg73wlwnr8i1ds5zvpcvc7a4lj1lljh49msz06xbax2chq7")))

(define-public crate-sudograph-generate-0.3.0-beta.19 (c (n "sudograph-generate") (v "0.3.0-beta.19") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "0k30a42daqmfvw4slcl9cnh73mm3f5bqgmmrqin86j3fjfgl9lf8")))

(define-public crate-sudograph-generate-0.3.0-beta.20 (c (n "sudograph-generate") (v "0.3.0-beta.20") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "1zsqwgk9dm21isrvnf9gfm99i3g4flix7k6ffchai16q9rjx2z74")))

(define-public crate-sudograph-generate-0.3.0-beta.21 (c (n "sudograph-generate") (v "0.3.0-beta.21") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "0kd6jlsqpxvwv7ab7cx5pjivpnw98q6wi710sj9p319v4n7cp8b3")))

(define-public crate-sudograph-generate-0.3.0-beta.22 (c (n "sudograph-generate") (v "0.3.0-beta.22") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "057bf63dh2wxy7qgp4hmqlb0sykxnpd0c6npg68dbvsf01fi4wzq")))

(define-public crate-sudograph-generate-0.3.0-beta.23 (c (n "sudograph-generate") (v "0.3.0-beta.23") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "0p91x514f3zli5v0srw8270a4aapgkb8n7mnzqchk794g3n18r92")))

(define-public crate-sudograph-generate-0.3.0 (c (n "sudograph-generate") (v "0.3.0") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "0v55x294wyawr1vb3g5hlaf4hrh0nyq2hz58l3xy8ck0a45lkaqv")))

(define-public crate-sudograph-generate-0.4.3 (c (n "sudograph-generate") (v "0.4.3") (d (list (d (n "graphql-parser") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.61") (d #t) (k 0)))) (h "1pvlz91krl7yp38aa4fnxvc340jwribihfbwi7s9vj75ka1lv49b")))

