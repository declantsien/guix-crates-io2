(define-module (crates-io su do sudoku_lib) #:use-module (crates-io))

(define-public crate-sudoku_lib-0.2.0 (c (n "sudoku_lib") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cn59k2vfskkn4mzflxy75rdfndlw20y2gaskhpklpavsymq51cv")))

(define-public crate-sudoku_lib-0.2.1 (c (n "sudoku_lib") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nrgl743schlfym6fpbxkyyx02vlf4j3y89p8h8s5q5lz2jlm105")))

(define-public crate-sudoku_lib-0.2.2 (c (n "sudoku_lib") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)))) (h "13h86wxpzqmm29a24f3p0f6ahrwhim0d02z8sdwx96r881cvm52c")))

(define-public crate-sudoku_lib-0.2.3 (c (n "sudoku_lib") (v "0.2.3") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "0hllxbmx0mclk9isqwl3kdi4dd9ps004cg899gka3v5fqf63czff")))

(define-public crate-sudoku_lib-0.2.4 (c (n "sudoku_lib") (v "0.2.4") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "16cal1qh9cfnwmz1shmh8cdg1hpn9vr777l2g02116qz237g7la7")))

(define-public crate-sudoku_lib-0.2.5 (c (n "sudoku_lib") (v "0.2.5") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "12m68l106c2lhqgksl89s7y51lddgk1x68zg5g0gla2fq6a4lqna")))

(define-public crate-sudoku_lib-0.2.6 (c (n "sudoku_lib") (v "0.2.6") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "1qwwlp0q3m6d1nhq0dw2w7gi9b8fjqm5hcw0adqvk377ca74ls97")))

(define-public crate-sudoku_lib-0.2.7 (c (n "sudoku_lib") (v "0.2.7") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "06nmjbbgicj3m6rr4c8k8hh237wkix1jgznlrxg7l43ni8vi47dp")))

(define-public crate-sudoku_lib-0.2.8 (c (n "sudoku_lib") (v "0.2.8") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "1m4gcw2255rimn7ivnb43s2wndbmfagyplc4ysdzapdm53qlq0z0")))

(define-public crate-sudoku_lib-0.2.9 (c (n "sudoku_lib") (v "0.2.9") (d (list (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)))) (h "09bjmfvz2aq5z7y45apcan9g77drj8cbyf5r4ic3a7inw5cjj9l2")))

