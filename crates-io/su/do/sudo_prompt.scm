(define-module (crates-io su do sudo_prompt) #:use-module (crates-io))

(define-public crate-sudo_prompt-0.1.0 (c (n "sudo_prompt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "01jh7dygc7gqja8i8jrlgp3c8g1g1gjnyw2178vm1rs3p39fbiz5")))

