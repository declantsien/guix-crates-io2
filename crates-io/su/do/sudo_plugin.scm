(define-module (crates-io su do sudo_plugin) #:use-module (crates-io))

(define-public crate-sudo_plugin-1.0.0 (c (n "sudo_plugin") (v "1.0.0") (d (list (d (n "error-chain") (r "~0") (d #t) (k 0)) (d (n "libc") (r "~0") (d #t) (k 0)) (d (n "sudo_plugin-sys") (r "~1") (d #t) (k 0)))) (h "09sl32ag7h32ckbi2mzms17a16np2m39i39m9mjr8j24clcaby7f")))

(define-public crate-sudo_plugin-1.1.0 (c (n "sudo_plugin") (v "1.1.0") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "sudo_plugin-sys") (r "^1.1") (d #t) (k 0)))) (h "0agv4977l22ymnxvkkfiz6chd5y3hz477v7pmxq3gz548x3qv138")))

(define-public crate-sudo_plugin-1.2.0 (c (n "sudo_plugin") (v "1.2.0") (d (list (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)) (d (n "sudo_plugin-sys") (r "^1.2") (d #t) (k 0)))) (h "19pb6n4573h4dd9xvh1vzb6j6z8nhz75y7bpxr998qrdan4w8spp")))

