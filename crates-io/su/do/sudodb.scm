(define-module (crates-io su do sudodb) #:use-module (crates-io))

(define-public crate-sudodb-0.0.0 (c (n "sudodb") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "15p1qmgljkv1rqx5asafp8glsfxpayqc290hhhwnqixad54x2gsk")))

(define-public crate-sudodb-0.1.0 (c (n "sudodb") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "1v56ckkmf0ppjdkbb1hsxchn3qyh4ljp9vr467kvnp5hhb3bs2af")))

(define-public crate-sudodb-0.2.0-beta.0 (c (n "sudodb") (v "0.2.0-beta.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "01z2nl6i552d3046g8qq5sjbgbha883vbak058p9mwwwr8240h1q")))

(define-public crate-sudodb-0.2.0-beta.1 (c (n "sudodb") (v "0.2.0-beta.1") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "07dk4dryw3x075lf13cqwblw2xhqn1ivl261lhphrr2p68qqkz1x")))

(define-public crate-sudodb-0.2.0-beta.2 (c (n "sudodb") (v "0.2.0-beta.2") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "0qjyykmqybipcmv05p4451ic9ivgjq6iza32hnasmcxz136898i7")))

(define-public crate-sudodb-0.2.0-beta.4 (c (n "sudodb") (v "0.2.0-beta.4") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "1y1sv5wzwgy68pkx7ndiy779hhfq05vicv3mvq50lbd2gmrsj0vd")))

(define-public crate-sudodb-0.2.0-beta.11 (c (n "sudodb") (v "0.2.0-beta.11") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "11vgapaqlzxg4inzh37yh1vm7cc27y7147wnx2nss7jz5iq4dffq")))

(define-public crate-sudodb-0.2.0-beta.12 (c (n "sudodb") (v "0.2.0-beta.12") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "0n4iw8yz93zb5bbrbcd1jyisry1cbziwa9ypxsqzjxd6q5pni75h")))

(define-public crate-sudodb-0.2.0-beta.14 (c (n "sudodb") (v "0.2.0-beta.14") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "15bvxs6baq32iz9dgw20pm8nqsii9309n9l97w7pmkyjk90k0gvp")))

(define-public crate-sudodb-0.2.0-beta.15 (c (n "sudodb") (v "0.2.0-beta.15") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "03aycs8k2lmg5jblbk3lnnfihzcyrkjsn2vnrl9nycc81ryi4243")))

(define-public crate-sudodb-0.2.0-beta.17 (c (n "sudodb") (v "0.2.0-beta.17") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "07kzmcj57kzpr0isfbyha98hmi031hnpsz5g0cw4crzz87bgbq27")))

(define-public crate-sudodb-0.2.0-beta.18 (c (n "sudodb") (v "0.2.0-beta.18") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "1r8421dq6hrw4n180vyyfdag8ikn82b97brmgldncbn1876w8bx6")))

(define-public crate-sudodb-0.2.0-beta.19 (c (n "sudodb") (v "0.2.0-beta.19") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "1mdz2ijlpyn3bj1ayf5mwn9vrc1b5s8rlbwprm9bigx09sc3kxab")))

(define-public crate-sudodb-0.2.0-beta.20 (c (n "sudodb") (v "0.2.0-beta.20") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "07g5akiqpk4jc5haj626awn98g8i1a8k564xvgz0n0p6d5f83bgc")))

(define-public crate-sudodb-0.2.0-beta.21 (c (n "sudodb") (v "0.2.0-beta.21") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "17mnsaqr6fs9ngk15wmsy5ijfmimil0yf3d0x27xppcv89r8wz35")))

(define-public crate-sudodb-0.2.0-beta.22 (c (n "sudodb") (v "0.2.0-beta.22") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "0m421ll76rl1hzh10s0w7cg2sy738rzg8lyal19apjd651bbybda")))

(define-public crate-sudodb-0.2.0-beta.23 (c (n "sudodb") (v "0.2.0-beta.23") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "0dc96l50gilsq87icl66dn2dahdiq44x5mf3aagrlks2mhpanm22")))

(define-public crate-sudodb-0.2.0-beta.25 (c (n "sudodb") (v "0.2.0-beta.25") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "0x5kwa4di8xmgzglynhjvijrxcsasfxx59kmi4d056k1hm8mvsrg")))

(define-public crate-sudodb-0.2.0-beta.26 (c (n "sudodb") (v "0.2.0-beta.26") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "088zl70lsrd9zjc06lhvz30v6rjl3fn2k3nn65vr67w27d5gvi8g")))

(define-public crate-sudodb-0.2.0-beta.27 (c (n "sudodb") (v "0.2.0-beta.27") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "1hh6fklg43hakjf2k4r17r2pi0499yvyv2rksy6hp4vp7lprj9w0")))

(define-public crate-sudodb-0.2.0 (c (n "sudodb") (v "0.2.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "1ynmq3dc13lwa4klg2fgnzblxp5a56yzk8smjm3qf733dssc67b3")))

(define-public crate-sudodb-0.2.1 (c (n "sudodb") (v "0.2.1") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "05s78rvcwkj4n23mj6wyqzv14prb41ml69v1p012gnqlx7vfh96l")))

(define-public crate-sudodb-0.2.2-beta.0 (c (n "sudodb") (v "0.2.2-beta.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "1fviqq7m2cykbp65x7wwq3hwhkhacgkhzxm5w8q6j30aai0riifj")))

(define-public crate-sudodb-0.2.2-beta.1 (c (n "sudodb") (v "0.2.2-beta.1") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "1d3blwrfwnxyvq4safh6wrqn2wixz3a682crghgjz009fv230hnj")))

(define-public crate-sudodb-0.2.2 (c (n "sudodb") (v "0.2.2") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "11226y9knbx4l2clvsfi27jmli4d3m7a1jmvbxikhbjq904flpxr")))

(define-public crate-sudodb-0.3.0-beta.0 (c (n "sudodb") (v "0.3.0-beta.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "1sw33kshysz4wwkk932dbcqala8wb46883ixz2wk8185k0wmq3mb")))

(define-public crate-sudodb-0.3.0-beta.1 (c (n "sudodb") (v "0.3.0-beta.1") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "1licyrp0svlk1vlq4d1a9bf7pgmvz45130s1byly4akxbzyh2i47")))

(define-public crate-sudodb-0.3.0-beta.2 (c (n "sudodb") (v "0.3.0-beta.2") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "12x7zzfz2cgap324vnzw8brbl5imf3rsj3balyflx3i7a2qlcw8j")))

(define-public crate-sudodb-0.3.0-beta.4 (c (n "sudodb") (v "0.3.0-beta.4") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "0lbx0w8j53my0sg3gw6lbi6v9dqflfwhs51cilnhcyr2ip79xjrg")))

(define-public crate-sudodb-0.3.0-beta.5 (c (n "sudodb") (v "0.3.0-beta.5") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "1rispvlmqixbbw261d7d1vwljac7vmb2hlb9jlbgnm4h9fr2sm4v")))

(define-public crate-sudodb-0.3.0-beta.6 (c (n "sudodb") (v "0.3.0-beta.6") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "1294q62v9d6jha4ny7pwghy46j1avn915d8fi9q5k3nw8zaiiss8")))

(define-public crate-sudodb-0.3.0-beta.7 (c (n "sudodb") (v "0.3.0-beta.7") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "1acc50iwc5lyhs4n4na6k9x392ly8kwr8bsvl56hxpkzc0av1gnk")))

(define-public crate-sudodb-0.3.0-beta.8 (c (n "sudodb") (v "0.3.0-beta.8") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "0i5p8d0k13zz2z1piarvgqvry143pphy1gvhjbss5545hxmchgy2")))

(define-public crate-sudodb-0.3.0-beta.9 (c (n "sudodb") (v "0.3.0-beta.9") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "0pwld8h3rsij7bjqmw2ikh9hxyp6sqzj1pc5gwc999v24mplppjl")))

(define-public crate-sudodb-0.3.0-beta.10 (c (n "sudodb") (v "0.3.0-beta.10") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "1ns2i19svjzy8hnn0vblw15fmi4rnyb8jqqsmclp4j06ni3r429n")))

(define-public crate-sudodb-0.3.0-beta.11 (c (n "sudodb") (v "0.3.0-beta.11") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "1apx4a04gfgzhhgdk9mds3mys5l2x6fcyiqrd7vw1jqbxvfnjykk")))

(define-public crate-sudodb-0.3.0-beta.12 (c (n "sudodb") (v "0.3.0-beta.12") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "0qhj0ap501py4fcbz9sisjiy0fczqk6dj6spfbw6qnbg0rn7cxab")))

(define-public crate-sudodb-0.3.0-beta.13 (c (n "sudodb") (v "0.3.0-beta.13") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "1x95y5a4gzhyd2y5nb3r4c97k9b5gm4a38ii7ghq8i1hf3yx10k3")))

(define-public crate-sudodb-0.3.0-beta.14 (c (n "sudodb") (v "0.3.0-beta.14") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "0sf13fw3x288aq7wwz9lc1mm6gmhliyv6l7ri4lbqzl935ykjsdh")))

(define-public crate-sudodb-0.3.0-beta.16 (c (n "sudodb") (v "0.3.0-beta.16") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "0wmciqgqyb76y8ik07r21lw20ipac0220fh3k0prqyq1mxfrkr8n")))

(define-public crate-sudodb-0.3.0-beta.17 (c (n "sudodb") (v "0.3.0-beta.17") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "0hmy9rlqkahm9hjxcxy87cbyyiya5qm7pdvyq1k9wn2s2gs6l3ql")))

(define-public crate-sudodb-0.3.0-beta.18 (c (n "sudodb") (v "0.3.0-beta.18") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "0k4fryni0m6larh364zypm5k2isvgf9111v809g9ib4m1z1szdv6")))

(define-public crate-sudodb-0.3.0-beta.19 (c (n "sudodb") (v "0.3.0-beta.19") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "0vparx0lqv4f8h1r9k3zgpphll3aryg1zfk8iv55q3pm8107iskr")))

(define-public crate-sudodb-0.3.0-beta.20 (c (n "sudodb") (v "0.3.0-beta.20") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "0manw7gix2lns7wwnbxhpydpvhhpz2hy2pc1lzpz12hilkbb3ddk")))

(define-public crate-sudodb-0.3.0-beta.21 (c (n "sudodb") (v "0.3.0-beta.21") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "0ikr7b014n9i6w4kzkhc7ajwjqhnmi3vy2qs7ljyxdqyv1w558f9")))

(define-public crate-sudodb-0.3.0-beta.22 (c (n "sudodb") (v "0.3.0-beta.22") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "0103055g6s3flmh3yi9fra5d5gj948izcl9428l0cdd9qaka7cxp")))

(define-public crate-sudodb-0.3.0-beta.23 (c (n "sudodb") (v "0.3.0-beta.23") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "0zlix3msmcf4g78209rc2fhi9ir8k8qp8wfa97ipgzni5pb92prq")))

(define-public crate-sudodb-0.3.0 (c (n "sudodb") (v "0.3.0") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "1vj5602q0fbkppq4migi5ka8g12qs03jx27ca2wyy7qhz5q1whm1")))

(define-public crate-sudodb-0.4.3 (c (n "sudodb") (v "0.4.3") (d (list (d (n "base32") (r "^0.4.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)))) (h "0m4jw69q29fwrvdb3qkw60wwlk1xim356j5gadba4v7z87aw9z8b")))

