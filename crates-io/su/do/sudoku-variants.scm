(define-module (crates-io su do sudoku-variants) #:use-module (crates-io))

(define-public crate-sudoku-variants-0.1.0 (c (n "sudoku-variants") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.2.2") (d #t) (k 2)))) (h "1bzffadkkkh5ls60dgm2ya1pa7h5apsp28ryrc6za3lq3k88zgwg")))

(define-public crate-sudoku-variants-0.1.1 (c (n "sudoku-variants") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.2.2") (d #t) (k 2)))) (h "0mbz716f30wj3hr5lxynycs0hlpzs0vh900fp6zbrm682qfzf81j")))

(define-public crate-sudoku-variants-0.2.0 (c (n "sudoku-variants") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "0fr7ny79z3k6ririf7rwavxrajrgplbi7i38cllrbqai3r6m8kma")))

(define-public crate-sudoku-variants-0.2.1 (c (n "sudoku-variants") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 2)) (d (n "toml") (r "^0.5") (d #t) (k 2)))) (h "0hnmz44hal2rcn044n67xr6l2m2inw02icnvh3c0v1wfsy9lcxf3")))

