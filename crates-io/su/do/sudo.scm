(define-module (crates-io su do sudo) #:use-module (crates-io))

(define-public crate-sudo-0.0.0 (c (n "sudo") (v "0.0.0") (h "12hf8aiilchhjmam6ikhaarwbdy580f7qm07a2d425gf5rgsfch8")))

(define-public crate-sudo-0.1.3 (c (n "sudo") (v "0.1.3") (h "00d562ffxq7lqjiv9pz4mqwx9198kizy7s6ji6yx5ls4p8pn6idv")))

(define-public crate-sudo-0.2.0 (c (n "sudo") (v "0.2.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1icvfnkah2gdq07mnm753ssxc70ks8a0hkwqqq3lz7bds068mlvv")))

(define-public crate-sudo-0.2.1 (c (n "sudo") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "09rivs8q3qdasch4d0qwi8cnyqncsmj97y49hl7kgqrcd2hw8w70")))

(define-public crate-sudo-0.3.0 (c (n "sudo") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6") (d #t) (k 2)))) (h "1gmf5wvnqqaqis5476r0d19xfvh1ylijaq9v10c4sq1iygz4ssxx")))

(define-public crate-sudo-0.3.1 (c (n "sudo") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6") (d #t) (k 2)))) (h "0xmjsd9mik8bgpp1dhcyvkgf5lg6341rafjg1p88jcxwzp7aycdc")))

(define-public crate-sudo-0.4.0 (c (n "sudo") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6") (d #t) (k 2)))) (h "0pkjvvl3h6c22qzar4iqp3gsv60hfic0dmzck40v13k24c8jpgz2")))

(define-public crate-sudo-0.5.0 (c (n "sudo") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6") (d #t) (k 2)))) (h "01af44glj6gyr9nn4w678h8370f7cqhal818zwd2hvr0vx7fg21a")))

(define-public crate-sudo-0.6.0 (c (n "sudo") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.6") (d #t) (k 2)))) (h "10svglxy0pjbzc3sc8wxy77anza0wj400b7mzqvqxqc2q3a89gc8")))

