(define-module (crates-io su do sudo-boo) #:use-module (crates-io))

(define-public crate-sudo-boo-1.0.0 (c (n "sudo-boo") (v "1.0.0") (h "1iwcmi6jzrvy4daqi6h56w7s3db107l7nrpfzx8nf4qhg5qa831i")))

(define-public crate-sudo-boo-1.0.1 (c (n "sudo-boo") (v "1.0.1") (h "0awyl9prjm9bki93s30qrm583w3sm7dx8bw48mhrsv04vc1xpcak")))

(define-public crate-sudo-boo-1.0.2 (c (n "sudo-boo") (v "1.0.2") (h "18492mnfnypr5zdnh765igls2g4alv43l7n8ndylcjb1596xapc1")))

