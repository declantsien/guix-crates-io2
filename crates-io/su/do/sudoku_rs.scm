(define-module (crates-io su do sudoku_rs) #:use-module (crates-io))

(define-public crate-sudoku_rs-0.1.0 (c (n "sudoku_rs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "00laqwxqyxmjfvgrfzkz616jg6hwvcbxd19a35wg67q6lak8hagr")))

(define-public crate-sudoku_rs-0.2.0 (c (n "sudoku_rs") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)))) (h "0g4v40wvmiw89g69zv335bxinanvsnpi1hnd5qi1f8njhyxjcq6n")))

(define-public crate-sudoku_rs-0.2.1 (c (n "sudoku_rs") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)))) (h "02ld57xw1bb0nbyncrdnlavsx4hn219avv32yrs6x2bq99bbwvqf")))

(define-public crate-sudoku_rs-0.2.2 (c (n "sudoku_rs") (v "0.2.2") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)))) (h "12p537cds0106vbg1cfi1kis9kp06rb6mvm2bphbwkbn4vd3zr7m")))

(define-public crate-sudoku_rs-0.2.3 (c (n "sudoku_rs") (v "0.2.3") (d (list (d (n "criterion") (r "^0.2.11") (d #t) (k 2)))) (h "0pqr1kgpw2b1ji8n7nq68rpp4fj8zz5q4cybycr88rkfpy8f1x5b")))

