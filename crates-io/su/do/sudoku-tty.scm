(define-module (crates-io su do sudoku-tty) #:use-module (crates-io))

(define-public crate-sudoku-tty-0.1.0 (c (n "sudoku-tty") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "termion") (r "^1.3.0") (d #t) (k 0)))) (h "0szrbb3wlsacz293xgcw0n3f3v1m2ja19xamd0m9wsfvi7493sak")))

(define-public crate-sudoku-tty-0.1.1 (c (n "sudoku-tty") (v "0.1.1") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "termion") (r "^1.3.0") (d #t) (k 0)))) (h "1zv0wi8apkc9fzjmhqbrvawwbyzxvlxxdi1zfp2gfmizygqb3qw1")))

(define-public crate-sudoku-tty-0.1.2 (c (n "sudoku-tty") (v "0.1.2") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "termion") (r "^1.3.0") (d #t) (k 0)))) (h "0dzhmq386j2icks84fcccpxdbnyiydjgqdld2a8pwjzvqn3s937x")))

(define-public crate-sudoku-tty-0.1.3 (c (n "sudoku-tty") (v "0.1.3") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "termion") (r "^1.3.0") (d #t) (k 0)))) (h "0q4644fn201pii1jmyclcj8l0g5ijin91gmcd08x7awv6wfagczj")))

(define-public crate-sudoku-tty-0.1.4 (c (n "sudoku-tty") (v "0.1.4") (d (list (d (n "fastrand") (r "^2.0.2") (d #t) (k 0)) (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "108sq02jg9ggr6vblww59d0dzihkwnla4l92qpawgyad7nx7z6s2")))

