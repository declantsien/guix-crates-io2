(define-module (crates-io su n_ sun_rpc) #:use-module (crates-io))

(define-public crate-sun_rpc-0.1.0 (c (n "sun_rpc") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xdr") (r "^0.6") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "xdr_extras") (r "^0.1") (d #t) (k 0)))) (h "1r0k4ngarx0mrgaiwybc2w8p9241sbq0j9816bq26gzw2z261c7b")))

