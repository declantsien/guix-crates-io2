(define-module (crates-io su n_ sun_rpc_client) #:use-module (crates-io))

(define-public crate-sun_rpc_client-0.1.0 (c (n "sun_rpc_client") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde-xdr") (r "^0.6") (d #t) (k 0)) (d (n "sun_rpc") (r "^0.1") (d #t) (k 0)) (d (n "vm_test_fixture") (r "^0.1") (d #t) (k 2)))) (h "07hvsdlf9yb98l1979ppyh0pl2wdv65n5nhp3kpsr3ayza2cs5jr")))

