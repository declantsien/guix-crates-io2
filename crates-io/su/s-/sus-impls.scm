(define-module (crates-io su s- sus-impls) #:use-module (crates-io))

(define-public crate-sus-impls-0.1.0 (c (n "sus-impls") (v "0.1.0") (d (list (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "0anzc070ws7kb01g0wl9fdiizxzxrk93i922si3w3cvr0y8an5q3")))

(define-public crate-sus-impls-0.2.0 (c (n "sus-impls") (v "0.2.0") (d (list (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "18kc3wrp6r3xqr3dh1l6iwv7z6p2ywsls6hblq91h0nw6ahqfrp0")))

(define-public crate-sus-impls-0.2.1 (c (n "sus-impls") (v "0.2.1") (d (list (d (n "syn") (r "^2.0.18") (d #t) (k 0)))) (h "03xxjgydhvcjqckc5fipfnpp8j0kc8y19rs13q2wcpzs97x4dyvx")))

