(define-module (crates-io su ns sunshine) #:use-module (crates-io))

(define-public crate-sunshine-0.0.1 (c (n "sunshine") (v "0.0.1") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "macroquad") (r "^0.3.0-alpha.11") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1x99p5nmwf3s2jv5mcy1jfwifa3yh6gjjxis3pq56gwv1q01qd8s")))

(define-public crate-sunshine-0.0.2 (c (n "sunshine") (v "0.0.2") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)))) (h "02cags1p49dcm8bfl1k5fqabxs0gs1n15ws32rzf0d2n5a6ynbcv")))

(define-public crate-sunshine-0.0.3 (c (n "sunshine") (v "0.0.3") (d (list (d (n "image") (r ">=0.23.0, <0.24.0") (d #t) (k 0)))) (h "0plwfvxq5kiijbhnpwky2dwdvk2ls74yv05dz0xv6gcy8ka89nwx")))

(define-public crate-sunshine-0.0.4 (c (n "sunshine") (v "0.0.4") (d (list (d (n "image") (r ">=0.23.0, <0.24.0") (d #t) (k 0)))) (h "0lwfdvs80kr65ydvhgcr6p3nsn7l5m76wjb2a4sw63xi5b2lslcq")))

(define-public crate-sunshine-0.0.5 (c (n "sunshine") (v "0.0.5") (d (list (d (n "image") (r ">=0.23.0, <0.24.0") (d #t) (k 0)))) (h "0llb974hfrdhfyr20bjly9kmdn5qrb7ggdpq444fcc5138y0l8mm")))

(define-public crate-sunshine-0.0.6 (c (n "sunshine") (v "0.0.6") (d (list (d (n "image") (r ">=0.23.0, <0.24.0") (d #t) (k 0)))) (h "11hkrpz47jfyw5c7mbqlypz5wa71096i02bmws1386f1s7rpf3lp")))

