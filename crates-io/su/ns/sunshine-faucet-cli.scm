(define-module (crates-io su ns sunshine-faucet-cli) #:use-module (crates-io))

(define-public crate-sunshine-faucet-cli-0.2.0 (c (n "sunshine-faucet-cli") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.36") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "substrate-subxt") (r "^0.10.1") (d #t) (k 0)) (d (n "sunshine-faucet-client") (r "^0.2.0") (d #t) (k 0)))) (h "0xqjzmysrga64iwip36ppblwiklhiw8hzn3xwz5h2wbkr267pbbg")))

(define-public crate-sunshine-faucet-cli-0.2.1 (c (n "sunshine-faucet-cli") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "substrate-subxt") (r "^0.10.1") (d #t) (k 0)) (d (n "sunshine-faucet-client") (r "^0.2.0") (d #t) (k 0)))) (h "0d553ysdsgcccl2s69xxrj4c43vh5w8zkifslq4apzzhcnay43gs")))

