(define-module (crates-io su ns sunshine-faucet-client) #:use-module (crates-io))

(define-public crate-sunshine-faucet-client-0.1.0 (c (n "sunshine-faucet-client") (v "0.1.0") (d (list (d (n "async-std") (r "=1.5.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "codec") (r "^1.3.0") (d #t) (k 0) (p "parity-scale-codec")) (d (n "identity-client") (r "^0.1.0") (d #t) (k 0) (p "sunshine-identity-client")) (d (n "substrate-subxt") (r "^0.10.0") (d #t) (k 0)))) (h "0i78mh8jmwblzgd68nij2jlh902b612yx2lhpd42ndid08pm8xy5")))

(define-public crate-sunshine-faucet-client-0.2.0 (c (n "sunshine-faucet-client") (v "0.2.0") (d (list (d (n "async-std") (r "=1.5.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.36") (d #t) (k 0)) (d (n "codec") (r "^1.3.0") (d #t) (k 0) (p "parity-scale-codec")) (d (n "substrate-subxt") (r "^0.10.1") (d #t) (k 0)) (d (n "sunshine-core") (r "^0.2.0") (d #t) (k 0)) (d (n "sunshine-identity-client") (r "^0.2.0") (d #t) (k 0)))) (h "0d041jbasx74a2sq29ln1kqw9gb6dk8dhd821kyzcai9bxgm307l")))

