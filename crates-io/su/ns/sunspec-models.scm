(define-module (crates-io su ns sunspec-models) #:use-module (crates-io))

(define-public crate-sunspec-models-0.1.0 (c (n "sunspec-models") (v "0.1.0") (h "0bzfmg1d1c6i72xznqyab6q99a8fykak4cyg4pgasqlj0xfb4dns")))

(define-public crate-sunspec-models-0.2.0 (c (n "sunspec-models") (v "0.2.0") (h "18i2ml3n3hxb5pl5bakcxvf1ryvd5hydh2lhb47cccaywbg6lwia")))

(define-public crate-sunspec-models-0.3.0 (c (n "sunspec-models") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "01dgbv2hybna92rhvms88f5v9zbmwd1243fjlykfqhava0scx5ai")))

(define-public crate-sunspec-models-0.4.0 (c (n "sunspec-models") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0zlkxhw6qszby6vzpzgs7n162fq3r7rl1km9x97h0qj9yp7ynf8g")))

(define-public crate-sunspec-models-0.5.0 (c (n "sunspec-models") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "02w3fwvs8gp06xi2q0983mzk1kabxn3hbhsj1k99w7rklxzf8yj5")))

(define-public crate-sunspec-models-0.5.1 (c (n "sunspec-models") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "17cig7hs8r4i8axvfh48imlk5riqyvizbyawx065ipx0b6208jnj")))

(define-public crate-sunspec-models-0.5.2 (c (n "sunspec-models") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0fiy6a8apdvvdgiwc4j3sjr5ac32l8iazcvpr0rysnjm0sfjn1wy")))

(define-public crate-sunspec-models-0.5.3 (c (n "sunspec-models") (v "0.5.3") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0fqyn93qi60dnks6vrx0vz8zm7fk78d94yi410kjg5dy3yw44zva")))

(define-public crate-sunspec-models-0.5.4 (c (n "sunspec-models") (v "0.5.4") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "181b27y2raw8dlvmfy5i3vgyl2js94z4swyi2vr8fffzaig4rklf")))

(define-public crate-sunspec-models-0.5.6 (c (n "sunspec-models") (v "0.5.6") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0n09ka3145zz572dgq3qv0p51zv6cwbxjpvzk0x6r87vn3fbrs2a")))

(define-public crate-sunspec-models-0.5.7 (c (n "sunspec-models") (v "0.5.7") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1dj6mk4jlgvq8kqbs14dj8jjmhdxc2brmz7chg6s7snqz4y7dykl")))

(define-public crate-sunspec-models-0.5.8 (c (n "sunspec-models") (v "0.5.8") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "06klsn32g0r7syhskvvy045vikk1gc403mj8791klpv0iw4cjvcf")))

(define-public crate-sunspec-models-0.5.9 (c (n "sunspec-models") (v "0.5.9") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0qfnb6vfy3dhdbqh09zks8zw0m75syaz7r9j44q3ry2dcj51jva3")))

(define-public crate-sunspec-models-0.5.10 (c (n "sunspec-models") (v "0.5.10") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1v678n1gh3gkli601lzg30ajg3sj5izhbd6jc02b9r52zkdqib96")))

(define-public crate-sunspec-models-0.5.11 (c (n "sunspec-models") (v "0.5.11") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0xn7y0b10hc452ybm31ah0y5n946gyjhpf4bgmjm4109wy28ip3n")))

(define-public crate-sunspec-models-0.5.12 (c (n "sunspec-models") (v "0.5.12") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0crsi6692y3wh42syw1c8pjkmig7yx4vqz4nkcyvjgqzcpw29z7s")))

(define-public crate-sunspec-models-0.5.13 (c (n "sunspec-models") (v "0.5.13") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 2)) (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "07k2cyfrknj1grm9v3b7afq5a52568rcfkm0g6iz79yg96f86qgg")))

(define-public crate-sunspec-models-0.5.14 (c (n "sunspec-models") (v "0.5.14") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 2)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1xkfdlkndw748p0118d8yqxdbj40b54hinr8x2lrc0124vixdbax")))

(define-public crate-sunspec-models-0.5.15 (c (n "sunspec-models") (v "0.5.15") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 2)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0hgyai1lm94nvllli9vybpvghbn2imwbk1l4l88ax2b07y3zkaaj")))

(define-public crate-sunspec-models-0.5.16 (c (n "sunspec-models") (v "0.5.16") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 2)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-modbus") (r "^0.9.0") (f (quote ("tcp" "tcp-server"))) (d #t) (k 2)))) (h "0p00lajcqww0gjd30rh1w5kmf9abn58aixs7jhb3wm8g5b9q2514")))

(define-public crate-sunspec-models-0.5.17 (c (n "sunspec-models") (v "0.5.17") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 2)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-modbus") (r "^0.9.0") (f (quote ("tcp" "tcp-server"))) (d #t) (k 2)))) (h "1z0mq4bgbb7mf2xbyjqgkadd1gvswdk73gxc682lc02y894fx17g")))

(define-public crate-sunspec-models-0.5.18 (c (n "sunspec-models") (v "0.5.18") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (d #t) (k 2)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 2)) (d (n "tokio") (r "^1.28.2") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-modbus") (r "^0.9.0") (f (quote ("tcp" "tcp-server"))) (d #t) (k 2)))) (h "0lm489s0f5qjrcf0c9257npdr930awkny77v2y1y74fx10vjq2dg")))

