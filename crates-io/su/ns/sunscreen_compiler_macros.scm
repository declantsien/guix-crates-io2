(define-module (crates-io su ns sunscreen_compiler_macros) #:use-module (crates-io))

(define-public crate-sunscreen_compiler_macros-0.5.0 (c (n "sunscreen_compiler_macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 2)) (d (n "syn") (r "^1.0.81") (f (quote ("derive" "full" "fold"))) (d #t) (k 0)))) (h "0bs3gnz4i8x4l5pdwjjh0njkw1avfrwbglbi2v0w1lm1fiacnxyz") (r "1.60.0")))

(define-public crate-sunscreen_compiler_macros-0.6.0 (c (n "sunscreen_compiler_macros") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 2)) (d (n "syn") (r "^1.0.81") (f (quote ("derive" "full" "fold"))) (d #t) (k 0)))) (h "0vpyhbbsq2jf5qaxykjywl9whgil6q614f754kz3a22x7zc1ns7x") (r "1.60.0")))

(define-public crate-sunscreen_compiler_macros-0.6.1 (c (n "sunscreen_compiler_macros") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 2)) (d (n "syn") (r "^1.0.81") (f (quote ("derive" "full" "fold"))) (d #t) (k 0)))) (h "1c7jws4a933vjwm9hc7ycf49yv3krvd20n7xzwis12n27s09ryam") (r "1.60.0")))

(define-public crate-sunscreen_compiler_macros-0.7.0 (c (n "sunscreen_compiler_macros") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 2)) (d (n "syn") (r "^1.0.81") (f (quote ("derive" "full" "fold"))) (d #t) (k 0)))) (h "143kkhymqshx2crymqfbqny5wj3bdn0ds1z2aaqpfxmk45b59ibq") (r "1.56.0")))

(define-public crate-sunscreen_compiler_macros-0.8.0 (c (n "sunscreen_compiler_macros") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 2)) (d (n "sunscreen_compiler_common") (r "^0.8.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1v2pacgp21vv0b7xdzsnfb9vhb5lshj5vr31s55nh4wn8bpj95b4") (r "1.56.0")))

(define-public crate-sunscreen_compiler_macros-0.8.1 (c (n "sunscreen_compiler_macros") (v "0.8.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 2)) (d (n "sunscreen_compiler_common") (r "^0.8.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "1xrgvn5has77189ryra6zvzbnhwflf9jyxh2h914rqyxz13434nk") (r "1.56.0")))

