(define-module (crates-io su ns sunspec) #:use-module (crates-io))

(define-public crate-sunspec-0.1.0 (c (n "sunspec") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "09vx1s0wszm5xv5w42q3vsgzxyyfmbhz45j2jyjgg6wbjqfc8wap") (f (quote (("default" "tokio-modbus"))))))

(define-public crate-sunspec-0.2.0 (c (n "sunspec") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "0yvfdmsamgl734xfmfq6mflwiza8ivn615a5xffcpfrpfcz5viay") (f (quote (("default" "tokio-modbus"))))))

(define-public crate-sunspec-0.3.0 (c (n "sunspec") (v "0.3.0") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "0n02fglpnycwz7pz5sw51phh9j9kbac2y0k6w0bg8gghd5rc91ia") (f (quote (("default" "tokio-modbus"))))))

(define-public crate-sunspec-0.3.1 (c (n "sunspec") (v "0.3.1") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "1wnrqxrslxa16mx26k069kzr4fmdrnz7zc0m56hzz5ap4wv0yj6i") (f (quote (("default" "tokio-modbus"))))))

(define-public crate-sunspec-0.4.0 (c (n "sunspec") (v "0.4.0") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 2)) (d (n "strum") (r "^0.26.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.11.0") (o #t) (d #t) (k 0)))) (h "18i8jfhrg0jls42vsp5346ck0gb17v0xn34flxj1wzr5is44qa94") (f (quote (("default" "tokio-modbus")))) (s 2) (e (quote (("serde" "dep:serde" "bitflags/serde"))))))

