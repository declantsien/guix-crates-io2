(define-module (crates-io su ns sunscreen_math_macros) #:use-module (crates-io))

(define-public crate-sunscreen_math_macros-0.8.0 (c (n "sunscreen_math_macros") (v "0.8.0") (d (list (d (n "bytemuck") (r "^1.13.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "13h5izn3f7i9fahvw0pfl8w2jr0xizclk3i10s7hsl96lcw0r0wh") (r "1.56.0")))

(define-public crate-sunscreen_math_macros-0.8.1 (c (n "sunscreen_math_macros") (v "0.8.1") (d (list (d (n "bytemuck") (r "^1.13.0") (d #t) (k 0)) (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0ma2h9gfamfn740faj1yi0c47rrss8vcnpc6i6bxg3vm2zvcv6c8") (r "1.56.0")))

