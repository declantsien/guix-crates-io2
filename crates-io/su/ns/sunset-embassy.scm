(define-module (crates-io su ns sunset-embassy) #:use-module (crates-io))

(define-public crate-sunset-embassy-0.2.0 (c (n "sunset-embassy") (v "0.2.0") (d (list (d (n "atomic-polyfill") (r "^1.0") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embassy-futures") (r "^0.1") (d #t) (k 0)) (d (n "embassy-sync") (r "^0.5") (d #t) (k 0)) (d (n "embedded-io-async") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1") (d #t) (k 0)) (d (n "sunset") (r "^0.2.0") (f (quote ("embedded-io"))) (d #t) (k 0)))) (h "07ivqx31c95ndaa9782mdc7a9wj11im29mgyfbh8wj9fa0sckxv9") (s 2) (e (quote (("defmt" "dep:defmt"))))))

