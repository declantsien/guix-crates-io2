(define-module (crates-io su ns sunset-sshwire-derive) #:use-module (crates-io))

(define-public crate-sunset-sshwire-derive-0.1.0 (c (n "sunset-sshwire-derive") (v "0.1.0") (d (list (d (n "virtue") (r "^0.0") (d #t) (k 0)))) (h "00nnhks5wz8qyvm64hgdvdm32v3wmhp5d5hfa6vxz33z3vnscl0y")))

(define-public crate-sunset-sshwire-derive-0.2.0 (c (n "sunset-sshwire-derive") (v "0.2.0") (d (list (d (n "virtue") (r "^0.0.16") (d #t) (k 0)))) (h "172445pjiq33gldq4nikpw6nr867rln13wjsywiakpgw4giwwdx3")))

