(define-module (crates-io su ns sunshine-tray) #:use-module (crates-io))

(define-public crate-sunshine-tray-1.0.0 (c (n "sunshine-tray") (v "1.0.0") (d (list (d (n "const-gen") (r "^1.3") (d #t) (k 1)) (d (n "image") (r "^0.24") (d #t) (k 1)) (d (n "ksni") (r "^0.2") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)))) (h "0fj7fsnly1ixdbl9x9i124v4h0k5sdyl5lwlidar09s9r38gybj5")))

(define-public crate-sunshine-tray-1.0.1 (c (n "sunshine-tray") (v "1.0.1") (d (list (d (n "const-gen") (r "^1.3") (d #t) (k 1)) (d (n "image") (r "^0.24") (d #t) (k 1)) (d (n "ksni") (r "^0.2") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)))) (h "0x954d2z96lkknc1wwyn7drlnd62r5cc1bkjaib1z50s0hgr1v3k")))

(define-public crate-sunshine-tray-1.0.2 (c (n "sunshine-tray") (v "1.0.2") (d (list (d (n "const-gen") (r "^1.3") (d #t) (k 1)) (d (n "image") (r "^0.24") (d #t) (k 1)) (d (n "ksni") (r "^0.2") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)))) (h "0gdckfd4033n5fvpmnaybfndblsh0vh9rfidkvncsk9gdy9n3j05")))

