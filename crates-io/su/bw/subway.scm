(define-module (crates-io su bw subway) #:use-module (crates-io))

(define-public crate-subway-0.1.0 (c (n "subway") (v "0.1.0") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "13a31hnjllfwldsxhw7qyk066gxvlps79fxz67dlvnrpkxfam0fp")))

(define-public crate-subway-0.1.1 (c (n "subway") (v "0.1.1") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "1v0mdd6cnyh7g1pwqa3gn0yc64k75mjhk1myhhpaw9cbcvj0nc12")))

(define-public crate-subway-0.1.2 (c (n "subway") (v "0.1.2") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 0)))) (h "0niicc5b9jc9hqw5kkf5wmadfilcq881r0dixdw8zcsq2dq7z0pa")))

