(define-module (crates-io su bw subwasm) #:use-module (crates-io))

(define-public crate-subwasm-0.13.2 (c (n "subwasm") (v "0.13.2") (d (list (d (n "assert_cmd") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "substrate-runtime-proposal-hash") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "subwasmlib") (r "^0.13.2") (d #t) (k 0)) (d (n "wasm-loader") (r "^0.13.0") (d #t) (k 0)) (d (n "wasm-testbed") (r "^0.13.1") (d #t) (k 0)))) (h "0qavh24nj59n00x8vnyq5lc01pni3lqznyswa0fsgmsk5iba7gm5") (y #t)))

