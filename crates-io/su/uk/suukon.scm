(define-module (crates-io su uk suukon) #:use-module (crates-io))

(define-public crate-suukon-0.1.0 (c (n "suukon") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.11") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "08wb09hlgraaqxmyjbx74yylv92dxbjr04na1xm2snfz1w7mvsj0") (f (quote (("default" "build-binary") ("build-binary" "clap"))))))

(define-public crate-suukon-0.2.0 (c (n "suukon") (v "0.2.0") (d (list (d (n "clap") (r ">=4.3, <4.5") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)))) (h "1cbwmbyddxb8w5glf6xwmj742xid9kglzcs420rcvv1gpmabd52l") (f (quote (("default" "build-binary") ("build-binary" "clap"))))))

