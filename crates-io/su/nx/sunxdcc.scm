(define-module (crates-io su nx sunxdcc) #:use-module (crates-io))

(define-public crate-sunxdcc-0.1.1 (c (n "sunxdcc") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1q9292jfaydlzbxcv4bjnh3wiz7ywrwg3c52w26q1n6mrrg6jvj2")))

