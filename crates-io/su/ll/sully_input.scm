(define-module (crates-io su ll sully_input) #:use-module (crates-io))

(define-public crate-sully_input-0.1.0 (c (n "sully_input") (v "0.1.0") (h "094x2449rrvdxc10w1dyd9313rgiqyb537dscxgw8qkj33z7c74n")))

(define-public crate-sully_input-0.1.1 (c (n "sully_input") (v "0.1.1") (h "1pl6sj87zrk8pnksmcmgvpd5a9v067iy5svvyklqip7j42rlw96v")))

(define-public crate-sully_input-0.1.11 (c (n "sully_input") (v "0.1.11") (h "0n7jcblhbcs8mccvnn2dzl5lwmisbf66qs62h0m86dvpdrahimaf")))

(define-public crate-sully_input-0.1.12 (c (n "sully_input") (v "0.1.12") (h "0606hkpqrc577ndq8qrhld49ffrlm0rzscgbi55v5s5said56lqj")))

(define-public crate-sully_input-0.1.13 (c (n "sully_input") (v "0.1.13") (h "14vgjs9yxwfwv5d5km0md13hsr1p4frh4l7sq5kih2kyykl9rmij")))

