(define-module (crates-io su ll sully_peg) #:use-module (crates-io))

(define-public crate-sully_peg-0.1.0 (c (n "sully_peg") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sully_graph") (r "^0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing" "full"))) (d #t) (k 0)))) (h "14xzinxrk4bch6zhl0fivy3jrw8k61pp5j0xkwivlvclhmfsbgi9")))

(define-public crate-sully_peg-0.1.11 (c (n "sully_peg") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "sully_graph") (r "^0.1.0") (d #t) (k 0)) (d (n "sully_input") (r "^0.1.13") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "parsing" "full"))) (d #t) (k 0)))) (h "0glz1abgqcsavffrvf68pjxz2mc024fj9hgxcb2hhs12ib2drh3k")))

