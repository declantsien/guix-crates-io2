(define-module (crates-io su bo suborbital) #:use-module (crates-io))

(define-public crate-suborbital-0.0.1 (c (n "suborbital") (v "0.0.1") (d (list (d (n "cargo_toml") (r ">=0.6.3, <0.7.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simple-error") (r ">=0.1.9, <0.2.0") (d #t) (k 0)) (d (n "toml") (r ">=0.5.6, <0.6.0") (d #t) (k 0)))) (h "18bk9b96njqj0fjmc3jyds87v5skv801p8764mn3fwf814nl3swm")))

(define-public crate-suborbital-0.2.2 (c (n "suborbital") (v "0.2.2") (d (list (d (n "cargo_toml") (r "^0.6.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "simple-error") (r "^0.1.9") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0cn9d3crh4b3zpw8jlcnfn942f60x5qby67b970a1q3kmlr6j9gl")))

(define-public crate-suborbital-0.2.4 (c (n "suborbital") (v "0.2.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "simple-error") (r "^0.1.9") (d #t) (k 0)))) (h "0wgpmxyacgvvhk3c5fr57qywkgyn9pqxn98a9h9spjr5vpk1ccvm")))

(define-public crate-suborbital-0.2.5 (c (n "suborbital") (v "0.2.5") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "simple-error") (r "^0.1.9") (d #t) (k 0)))) (h "1c1cibxpnv3irwk4a2zszp8xfjwcwhbgxanpn95qbc8sqnszcsvk")))

(define-public crate-suborbital-0.3.0 (c (n "suborbital") (v "0.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "simple-error") (r "^0.1.9") (d #t) (k 0)))) (h "0pdax1s7j4g5ai5nac8i6d5army9hsra3krxnasw03j0rw3hbyk3")))

(define-public crate-suborbital-0.4.0 (c (n "suborbital") (v "0.4.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "simple-error") (r "^0.1.9") (d #t) (k 0)))) (h "0v9z58xrzxnar6h1xls0m3dhcrq6i5d6bpbfpmmr5xfjdbs6pw7f")))

(define-public crate-suborbital-0.4.1 (c (n "suborbital") (v "0.4.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "simple-error") (r "^0.1.9") (d #t) (k 0)))) (h "1l66pjqdb358h1n3h852lp6xd3vzwa0pg6c6k685prq0130qjz7k")))

(define-public crate-suborbital-0.4.2 (c (n "suborbital") (v "0.4.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "simple-error") (r "^0.1.9") (d #t) (k 0)))) (h "03n8f47xmrjkqca68fw59y9jp1c682gvar5anffp5jix7gdz2s9b")))

(define-public crate-suborbital-0.4.4 (c (n "suborbital") (v "0.4.4") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (f (quote ("alloc"))) (k 0)) (d (n "simple-error") (r "^0.1.9") (d #t) (k 0)))) (h "0gbrsw5233ap4i0v87pj8sziimaa6z0qlc4qizpgr564n9bcjg0b")))

(define-public crate-suborbital-0.5.0 (c (n "suborbital") (v "0.5.0") (h "1i0prcsp3qn2dfzlnb5rjh8xkwb5ygwqpry8afj67saj5jmr681p")))

(define-public crate-suborbital-0.6.0 (c (n "suborbital") (v "0.6.0") (h "1j7hvl8xa8ympsiyph2k3sz0nph0x0f3s3mr0sp73ziks950b5bh")))

(define-public crate-suborbital-0.6.3 (c (n "suborbital") (v "0.6.3") (h "06dxhg129qib17q7s6rxlpp6pg0lvf86c0ppg56dywws56gvqbf3")))

(define-public crate-suborbital-0.8.0 (c (n "suborbital") (v "0.8.0") (h "00175k5w1wbj7j6iaz1mzvviswpsglm3p3p2z8jxffgqywagvp7f")))

(define-public crate-suborbital-0.8.1 (c (n "suborbital") (v "0.8.1") (h "1a43c2wwf79wyjvcbdvpin2wfnbm1cfzrksd9gkanpaxn1i7s8qb")))

(define-public crate-suborbital-0.8.2 (c (n "suborbital") (v "0.8.2") (h "1wmi76y28cvxdf5m4i2i6vw8ihwq27p2g5sqwnrnmzrrp4455r28")))

(define-public crate-suborbital-0.9.0 (c (n "suborbital") (v "0.9.0") (h "17pygni8lk2q07bl3lgn9vbj8idfdasbzwy1lcvi6l90ar9544l8")))

(define-public crate-suborbital-0.9.1 (c (n "suborbital") (v "0.9.1") (h "12r0i81n7mp47bx89hnm4i34w2ckvgiq6n7zf1qz9s1zw6h0pf83")))

(define-public crate-suborbital-0.9.2 (c (n "suborbital") (v "0.9.2") (h "1rzjbpls3d3skfbf4af6pkhkjbkx9vx5an6rh8rn030nmrgz7w9f")))

(define-public crate-suborbital-0.10.0 (c (n "suborbital") (v "0.10.0") (h "168bzylncnyxpg7qi43dlxdagvm6d9daj6brqzs64k3f8bx0927k")))

(define-public crate-suborbital-0.11.0 (c (n "suborbital") (v "0.11.0") (h "09ldpbdc3vilj21b76dmz8hay0pyimv063ss5jbhjzjml50l609g")))

(define-public crate-suborbital-0.11.1 (c (n "suborbital") (v "0.11.1") (h "1g27ssfq1qi0c108lxacll8rzkw1sxw5lnfs4kvcx9jqln689nb3")))

(define-public crate-suborbital-0.12.0 (c (n "suborbital") (v "0.12.0") (h "1585nvxmnfvglqd6y6qiq6w47k96zk6z9jaynql5dkkj5vjzimgq")))

(define-public crate-suborbital-0.13.0 (c (n "suborbital") (v "0.13.0") (d (list (d (n "suborbital-macro") (r "^0.13.0") (d #t) (k 0)))) (h "1vcpf1cmyxd6pgymvmax8z3kf1d01qhg0bmz75fflnwrfa44mjs5")))

(define-public crate-suborbital-0.13.1 (c (n "suborbital") (v "0.13.1") (d (list (d (n "suborbital-macro") (r "^0.13.1") (d #t) (k 0)))) (h "10031kxsimb0hka2mrj7fl06c12p7k7pb8ri2aqjxwm19vvvq57m")))

(define-public crate-suborbital-0.14.0 (c (n "suborbital") (v "0.14.0") (d (list (d (n "suborbital-macro") (r "^0.14.0") (d #t) (k 0)))) (h "0p93pyb66v7ckpd0zcgm7kbp7mq7rcip1kqa8d8g7dw0znjms19a")))

(define-public crate-suborbital-0.15.0 (c (n "suborbital") (v "0.15.0") (d (list (d (n "suborbital-macro") (r "^0.15.0") (d #t) (k 0)))) (h "02q7h2znw51zk3sja4yzniy47q5dmny25byimbimaq6limi944ib")))

(define-public crate-suborbital-0.15.1 (c (n "suborbital") (v "0.15.1") (d (list (d (n "suborbital-macro") (r "^0.15.1") (d #t) (k 0)))) (h "0kd7mgzpzxk8khbif3g9bidxnw6n5gl8bdf9bg2kz9kp3mwyglsx")))

(define-public crate-suborbital-0.16.0 (c (n "suborbital") (v "0.16.0") (d (list (d (n "suborbital-macro") (r "^0.16.0") (d #t) (k 0)))) (h "1lq2rj0zhkzhwwwc8v4bi1zyhxa5y6msjbbxdxfgmmydc9xmbh0m")))

(define-public crate-suborbital-0.16.1 (c (n "suborbital") (v "0.16.1") (d (list (d (n "suborbital-macro") (r "^0.16.1") (d #t) (k 0)))) (h "1nq324657h50qagv5yip4smqf96rk2lcl07f301wjmz19i3j3fqm")))

(define-public crate-suborbital-0.16.2 (c (n "suborbital") (v "0.16.2") (d (list (d (n "suborbital-macro") (r "^0.16.2") (d #t) (k 0)))) (h "1rb13c2yhbdpjdqgi0rrd6p353dpb0m7kma1zr4as15s083sv1l5")))

(define-public crate-suborbital-0.16.3 (c (n "suborbital") (v "0.16.3") (d (list (d (n "suborbital-macro") (r "^0.16.3") (d #t) (k 0)))) (h "1r6j2ff4qd34nbwdq08rkr36nhhgqcaayn0gqv48bfjipbgd58c6")))

