(define-module (crates-io su bo subotai) #:use-module (crates-io))

(define-public crate-subotai-1.0.0 (c (n "subotai") (v "1.0.0") (d (list (d (n "bincode") (r "0.*") (d #t) (k 0)) (d (n "bus") (r "1.*") (d #t) (k 0)) (d (n "itertools") (r "0.*") (d #t) (k 0)) (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "serde") (r "0.*") (d #t) (k 0)) (d (n "serde_macros") (r "0.*") (d #t) (k 0)) (d (n "sha1") (r "0.*") (d #t) (k 0)) (d (n "time") (r "0.*") (d #t) (k 0)))) (h "09vvqrlhwb64p9mrvr8vrvgfi7wfz815gdz1r4b8d6qrih4kw49c")))

(define-public crate-subotai-1.0.1 (c (n "subotai") (v "1.0.1") (d (list (d (n "bincode") (r "0.*") (d #t) (k 0)) (d (n "bus") (r "1.*") (d #t) (k 0)) (d (n "itertools") (r "0.*") (d #t) (k 0)) (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "serde") (r "0.*") (d #t) (k 0)) (d (n "serde_macros") (r "0.*") (d #t) (k 0)) (d (n "sha1") (r "0.*") (d #t) (k 0)) (d (n "time") (r "0.*") (d #t) (k 0)))) (h "0jdlfhzp8jwcca7mda4y3z1gbwf1b77mpxfhjqr0z5i0jgx3pagq")))

(define-public crate-subotai-1.0.2 (c (n "subotai") (v "1.0.2") (d (list (d (n "bincode") (r "0.*") (d #t) (k 0)) (d (n "bus") (r "1.*") (d #t) (k 0)) (d (n "itertools") (r "0.*") (d #t) (k 0)) (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "serde") (r "0.*") (d #t) (k 0)) (d (n "serde_macros") (r "0.*") (d #t) (k 0)) (d (n "sha1") (r "0.*") (d #t) (k 0)) (d (n "time") (r "0.*") (d #t) (k 0)))) (h "0514zyc207cp5jzbm590wkk7cfxavd3ffl6ri617cjhm1j7kf0yx")))

(define-public crate-subotai-1.0.3 (c (n "subotai") (v "1.0.3") (d (list (d (n "bincode") (r "0.*") (d #t) (k 0)) (d (n "bus") (r "1.*") (d #t) (k 0)) (d (n "itertools") (r "0.*") (d #t) (k 0)) (d (n "rand") (r "0.*") (d #t) (k 0)) (d (n "serde") (r "0.*") (d #t) (k 0)) (d (n "serde_macros") (r "0.*") (d #t) (k 0)) (d (n "sha1") (r "0.*") (d #t) (k 0)) (d (n "time") (r "0.*") (d #t) (k 0)))) (h "09kxsikiirqad5pg6w7rd0ac06xx5shdygqzy71v6hdfsqs2l6ym")))

