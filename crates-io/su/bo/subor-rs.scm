(define-module (crates-io su bo subor-rs) #:use-module (crates-io))

(define-public crate-subor-rs-0.1.0 (c (n "subor-rs") (v "0.1.0") (d (list (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "thrift") (r "^0.0.4") (d #t) (k 0)) (d (n "try_from") (r "^0.2") (d #t) (k 0)))) (h "05y6ghr3pw4wbwgnzv5jxdqy046qaxpb9947s2mfn207g55c1in8")))

(define-public crate-subor-rs-0.9.2 (c (n "subor-rs") (v "0.9.2") (d (list (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "thrift") (r "^0.0.4") (d #t) (k 0)) (d (n "try_from") (r "^0.2") (d #t) (k 0)))) (h "0fdnqv0vymr6lbf56cy90i87in94385ay1lw36jkwd1anzsnzp2i")))

(define-public crate-subor-rs-0.9.3 (c (n "subor-rs") (v "0.9.3") (d (list (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "thrift") (r "^0.12") (d #t) (k 0)) (d (n "try_from") (r "^0.2") (d #t) (k 0)))) (h "0x94703p0s8dlrvi9fv47ldvdg01lkyiix63x9hlfhinx283gi4m")))

