(define-module (crates-io su pr supremo_loan) #:use-module (crates-io))

(define-public crate-supremo_loan-0.1.0 (c (n "supremo_loan") (v "0.1.0") (d (list (d (n "actix-web") (r "^3.3") (d #t) (k 0)) (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0p7vdqjrv28fysn4ldlrzb2zkqiscx229w8anwlqqd0rkqfwqb1h")))

(define-public crate-supremo_loan-0.1.1 (c (n "supremo_loan") (v "0.1.1") (d (list (d (n "actix-web") (r "^3.3") (d #t) (k 0)) (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18rwmxxg5ldm6qgzw1hr109wnixsp8frg0laz42gqy712mc37vp3")))

(define-public crate-supremo_loan-0.1.2 (c (n "supremo_loan") (v "0.1.2") (d (list (d (n "actix-web") (r "^3.3") (d #t) (k 0)) (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1i5vkdm3a6hnd074rh85797rh6nw5gyqpw7zd43z0hbp5f4wwa84")))

(define-public crate-supremo_loan-0.1.3 (c (n "supremo_loan") (v "0.1.3") (d (list (d (n "actix-web") (r "^3.3") (d #t) (k 0)) (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0r8679ah3mm2w5qxmwxdqmms5lil83iikm7svkf4g4cm7psgx6vb")))

(define-public crate-supremo_loan-0.1.4 (c (n "supremo_loan") (v "0.1.4") (d (list (d (n "actix-web") (r "^3.3") (d #t) (k 0)) (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ql7gwjhzkjl201dydbw8dyhs5jmpl780n09m4l25g977nn7c4sh")))

(define-public crate-supremo_loan-0.1.5 (c (n "supremo_loan") (v "0.1.5") (d (list (d (n "actix-web") (r "^3.3") (d #t) (k 0)) (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hdkbc1j12vdp642ch3gnlb2xnfhs6x77s2mp2p0557k44sgi01c")))

(define-public crate-supremo_loan-0.1.6 (c (n "supremo_loan") (v "0.1.6") (d (list (d (n "actix-web") (r "^3.3") (d #t) (k 0)) (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1qi95v3cy8s96p7j443ml81i0piq53wgf272hm4rbv2zd6k4xavr")))

