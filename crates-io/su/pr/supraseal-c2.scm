(define-module (crates-io su pr supraseal-c2) #:use-module (crates-io))

(define-public crate-supraseal-c2-0.1.0 (c (n "supraseal-c2") (v "0.1.0") (d (list (d (n "blst") (r "^0.3.11") (d #t) (k 0)) (d (n "cc") (r "^1.0.70") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "sppark") (r "^0.1.5") (d #t) (k 0)))) (h "0nsx50h1xmgby4pnvwrf45sqvxc87jbzk6f348q5nc1qsbl6yqxx") (f (quote (("quiet") ("portable" "blst/portable") ("force-adx" "blst/force-adx") ("default"))))))

