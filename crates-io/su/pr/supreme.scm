(define-module (crates-io su pr supreme) #:use-module (crates-io))

(define-public crate-supreme-1.1.1 (c (n "supreme") (v "1.1.1") (d (list (d (n "dialoguer") (r "^0.7.1") (d #t) (k 0)) (d (n "handlebars") (r "^3.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "1ql20a49grq7yz754jz1z03b0z8xgzgjbwy9wiwmjpg1z18jyjmk")))

