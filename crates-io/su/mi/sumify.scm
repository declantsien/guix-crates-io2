(define-module (crates-io su mi sumify) #:use-module (crates-io))

(define-public crate-sumify-1.0.0 (c (n "sumify") (v "1.0.0") (d (list (d (n "clap") (r "^4.5.3") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "04jzqfjg6gvawl7dz10y2jl1033yb0k7n1cvmqgc7a3nwhkv6gxw")))

(define-public crate-sumify-1.0.1 (c (n "sumify") (v "1.0.1") (d (list (d (n "clap") (r "^4.5.3") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.5.0") (d #t) (k 0)))) (h "03pzncs8f7h10pmj0wvsrwn5w2l793dsalw2xpi86ymp96ak7pvl")))

