(define-module (crates-io su gg suggestion-cli) #:use-module (crates-io))

(define-public crate-suggestion-cli-0.2.0 (c (n "suggestion-cli") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "suggestion") (r "^0.2.0") (d #t) (k 0)))) (h "1dws4jqn6dq5m9azz2xsmy1wj8i9whq2525030qmcrwkmgk8hszh") (y #t)))

(define-public crate-suggestion-cli-0.3.0 (c (n "suggestion-cli") (v "0.3.0") (d (list (d (n "clap") (r "^3.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "suggestion") (r "^0.3.0") (d #t) (k 0)))) (h "1dmcw4rfx5k8sdk766c4jz20p9n61b89ndh7k4kzw0v2842myfc2") (y #t)))

(define-public crate-suggestion-cli-0.3.1 (c (n "suggestion-cli") (v "0.3.1") (d (list (d (n "clap") (r "^3.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "suggestion") (r "^0.3.0") (d #t) (k 0)))) (h "13fp2vz4d0g4bwk4razgz6dksyzlx4viz94w9k2m0bwkm0jaaci9") (y #t)))

