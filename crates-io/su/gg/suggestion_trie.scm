(define-module (crates-io su gg suggestion_trie) #:use-module (crates-io))

(define-public crate-suggestion_trie-0.1.0 (c (n "suggestion_trie") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "19f4wm8qxl000bzgc44xrmfmks8f3bnv239dlz92gd42n4fi09yz")))

(define-public crate-suggestion_trie-0.1.1 (c (n "suggestion_trie") (v "0.1.1") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1aagjkipj6xixxk1pr2x7fp2zdv36v8dx58zzdd7hfrbmc5sa0f7")))

(define-public crate-suggestion_trie-0.1.2 (c (n "suggestion_trie") (v "0.1.2") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "01yvkg0j37q0f2413dzlnskh8vgv0f0x6bbrmj1cv7s8nbyc9l0w")))

(define-public crate-suggestion_trie-0.1.3 (c (n "suggestion_trie") (v "0.1.3") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1jwqaxi0fhsksmihnxyligas9c02vlsxkyr2kic68y8l3j65xvya")))

(define-public crate-suggestion_trie-0.1.4 (c (n "suggestion_trie") (v "0.1.4") (d (list (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "04xjnarg2p71h2f79xlzk86wpjlndnvff261spg4xk195v92x5ja")))

