(define-module (crates-io su gg suggestion) #:use-module (crates-io))

(define-public crate-suggestion-0.1.0 (c (n "suggestion") (v "0.1.0") (d (list (d (n "lev_distance") (r "^0.1.0") (d #t) (k 0)))) (h "0jz5asd940v1przagki65d23bzxkhkdzq4jh9whbwq3ngch3clq1") (y #t)))

(define-public crate-suggestion-0.1.1 (c (n "suggestion") (v "0.1.1") (d (list (d (n "lev_distance") (r "^0.1.0") (d #t) (k 0)))) (h "177vpg4mvvmg4aacv558iipvwjqhb3xyp2y6l553dv5nvlv0b98z") (y #t)))

(define-public crate-suggestion-0.1.2 (c (n "suggestion") (v "0.1.2") (d (list (d (n "lev_distance") (r "^0.1.1") (d #t) (k 0)))) (h "1zqsabda6ya6aqzd9iw7jjvfrs83758wvkj0i0nc0j9ancjp89np") (y #t)))

(define-public crate-suggestion-0.2.0 (c (n "suggestion") (v "0.2.0") (d (list (d (n "lev_distance") (r "^0.1.1") (d #t) (k 0)))) (h "112pr82qjxwvxy8bzcnfa12gl701sdwnv6s3nmnx37z1ib18srbk") (y #t)))

(define-public crate-suggestion-0.3.0 (c (n "suggestion") (v "0.3.0") (d (list (d (n "lev_distance") (r "^0.1.1") (d #t) (k 0)))) (h "1qrn644ccp2p2yk7k4c2jcfry3k9r2pn3p3zhqgmm9gy269ndmk4") (y #t)))

(define-public crate-suggestion-0.3.2 (c (n "suggestion") (v "0.3.2") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lev_distance") (r "^0.1.1") (d #t) (k 0)))) (h "0ql6954krrz581d5p8dkcvy7p5p8fgjznd3qs1n4ishafjvjk2d1") (y #t)))

(define-public crate-suggestion-0.3.3 (c (n "suggestion") (v "0.3.3") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lev_distance") (r "^0.1.1") (d #t) (k 0)))) (h "09z90kzyfs6wd5j38wr1nz3x323nkvrsbndgz8nhpi410ym8qsny") (y #t)))

(define-public crate-suggestion-0.3.4 (c (n "suggestion") (v "0.3.4") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lev_distance") (r "^0.1.1") (d #t) (k 0)))) (h "0gqdz2llj58bx420i1rlg9fx9qs655w5p7bz9xzpbblr2z27fb23") (y #t)))

