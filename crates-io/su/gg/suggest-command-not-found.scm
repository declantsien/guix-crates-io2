(define-module (crates-io su gg suggest-command-not-found) #:use-module (crates-io))

(define-public crate-suggest-command-not-found-0.1.0 (c (n "suggest-command-not-found") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "suggest") (r "^0.4") (d #t) (k 0)))) (h "17gk3h5kfs264rzw5jvqdp5ww2979bjb58nmrbygm5li9m1rldbv")))

(define-public crate-suggest-command-not-found-0.1.1 (c (n "suggest-command-not-found") (v "0.1.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "suggest") (r "^0.4") (d #t) (k 0)))) (h "1pdcr3g7irh1w6d3qwrax3l4dn390jcp7hsqikgwnv45a7abal16")))

(define-public crate-suggest-command-not-found-0.1.2 (c (n "suggest-command-not-found") (v "0.1.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "suggest") (r "^0.4") (d #t) (k 0)))) (h "0q83ligjy3jq4hbwlyjzssp0isxan4633dxpzqc8rr3lhwmb0j0x")))

(define-public crate-suggest-command-not-found-0.1.3 (c (n "suggest-command-not-found") (v "0.1.3") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "suggest") (r "^0.4") (d #t) (k 0)))) (h "063yyr7qpyjfq8qsg0k6s4vrprf53lx36x8g7507sgzg9akfxras")))

(define-public crate-suggest-command-not-found-0.1.4 (c (n "suggest-command-not-found") (v "0.1.4") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "suggest") (r "^0.4") (d #t) (k 0)))) (h "0srm07hqy2nxnj6rml7a4dl472jvcdc2qbpwix7cfl62ls8ah068")))

(define-public crate-suggest-command-not-found-0.1.5 (c (n "suggest-command-not-found") (v "0.1.5") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "suggest") (r "^0.4") (d #t) (k 0)))) (h "1vr64jigbqxfqh93j9hcrb2mkzbmfz4j8v8c2b5z854k82g7lr6d")))

(define-public crate-suggest-command-not-found-0.1.6 (c (n "suggest-command-not-found") (v "0.1.6") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "suggest") (r "^0.5") (d #t) (k 0)))) (h "0ivwl9vkyikm2y3x5f2cc95hkvdzqsw97fblv6kf8qik4xmb5mdd")))

(define-public crate-suggest-command-not-found-0.1.7 (c (n "suggest-command-not-found") (v "0.1.7") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "suggest") (r "^0.5") (d #t) (k 0)))) (h "0625948015zdb2vmhm9zgza0k7lz01cch3pm1chv7drpw7ifazjc")))

