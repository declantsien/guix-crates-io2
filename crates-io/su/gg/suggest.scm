(define-module (crates-io su gg suggest) #:use-module (crates-io))

(define-public crate-suggest-0.4.0 (c (n "suggest") (v "0.4.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lev_distance") (r "^0.1.1") (d #t) (k 0)))) (h "0nb6axl4i58g7k0q3p3bg6m363aw6qnqdg31y5c8b43x6bbd0n15")))

(define-public crate-suggest-0.5.0 (c (n "suggest") (v "0.5.0") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 2)) (d (n "lev_distance") (r "^0.1.1") (d #t) (k 0)))) (h "0ag8zz53fp54w9jzpc4m7diw1xcw9l74afa6371hf2f3r33bcdyz")))

(define-public crate-suggest-0.5.1 (c (n "suggest") (v "0.5.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "lev_distance") (r "^0.1.1") (d #t) (k 0)))) (h "1f4zjv9n12bcwp94dl76150rcg45kry3i7gsb56bxz1hbzny182f")))

