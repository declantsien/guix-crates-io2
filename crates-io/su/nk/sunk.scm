(define-module (crates-io su nk sunk) #:use-module (crates-io))

(define-public crate-sunk-0.1.0 (c (n "sunk") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "md5") (r "^0.3.7") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "1k24n37pbw1dmq72jl5lfclix9jpf5z4gvaqwx3p13mxqjmx26av")))

(define-public crate-sunk-0.1.1 (c (n "sunk") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "md5") (r "^0.3.7") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.9") (d #t) (k 0)))) (h "13gpwvzyssngxk4fccdw960l49l0f2qb1fr7sx7h1cxdz6wncgwp")))

(define-public crate-sunk-0.1.2 (c (n "sunk") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "md5") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "16pwwyfv24720h6sjhhb5w9lx18dxplf3r3alf4hrpwd7fhw7zi0")))

