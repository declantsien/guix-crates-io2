(define-module (crates-io su md sumdir) #:use-module (crates-io))

(define-public crate-sumdir-0.1.0 (c (n "sumdir") (v "0.1.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "blake2") (r "^0.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0zhxcgc9khxl5kv9s1xd0l7rk0bdwld254vm42lgicbw4fq4c0xx")))

(define-public crate-sumdir-0.2.0 (c (n "sumdir") (v "0.2.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "blake2") (r "^0.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0rsjgbi14ckzkibzsnfflwshxikd1i3pgmqk2frlmyfqn4a3937v")))

