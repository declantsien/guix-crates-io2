(define-module (crates-io su bl sublime_fuzzy) #:use-module (crates-io))

(define-public crate-sublime_fuzzy-0.2.0 (c (n "sublime_fuzzy") (v "0.2.0") (h "0qkqixrfh5jyicsrcbr5addjms3g6i7szxq24xhrh0x7k7ws4b5s")))

(define-public crate-sublime_fuzzy-0.3.0 (c (n "sublime_fuzzy") (v "0.3.0") (h "0pnly8xmpnfjh5fwcl5v98ydq5zc3gr95d5xfvrp4n1x80wa4j9b")))

(define-public crate-sublime_fuzzy-0.4.0 (c (n "sublime_fuzzy") (v "0.4.0") (h "0zv21drx0l6zmzq3k3wqiwfgiy2rb7835ks5a4nfgrlg8chfsvss")))

(define-public crate-sublime_fuzzy-0.4.1 (c (n "sublime_fuzzy") (v "0.4.1") (h "1wf5qqf3snrwqg1zjb84331vdsw778bii883fvs0yaxpc76xmsqr")))

(define-public crate-sublime_fuzzy-0.5.0 (c (n "sublime_fuzzy") (v "0.5.0") (h "1kvs0zgcs47gkk9w534zw10z6d622yhzzhk0gwx3ljgak3b7mgcp")))

(define-public crate-sublime_fuzzy-0.6.0 (c (n "sublime_fuzzy") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.67") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.67") (o #t) (d #t) (k 0)))) (h "1fb6kcwbxznh21qxi5d81bjwdsa3v9pf3xd1gd41jg077nc3vb5x") (f (quote (("serde_support" "serde" "serde_derive"))))))

(define-public crate-sublime_fuzzy-0.7.0 (c (n "sublime_fuzzy") (v "0.7.0") (d (list (d (n "serde") (r "^1.0.67") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.67") (o #t) (d #t) (k 0)))) (h "002lrl3qwfdzi087agqh9z4smmd391q6sn3y81sb62kw7w38cygs") (f (quote (("serde_support" "serde" "serde_derive"))))))

