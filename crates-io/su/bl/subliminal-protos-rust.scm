(define-module (crates-io su bl subliminal-protos-rust) #:use-module (crates-io))

(define-public crate-subliminal-protos-rust-0.0.1 (c (n "subliminal-protos-rust") (v "0.0.1") (d (list (d (n "axum") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "prost") (r "^0.11.6") (d #t) (k 0)) (d (n "redis") (r "^0.22.1") (d #t) (k 0)) (d (n "redis-derive") (r "^0.1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.150") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "tonic") (r "^0.8.3") (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "utoipa") (r "^3") (f (quote ("axum_extras"))) (d #t) (k 0)))) (h "0alwxyvm5xidd87n5kh9l98gkray67sm8gf55d2lsmfpzhx59pag")))

