(define-module (crates-io su bl sublock) #:use-module (crates-io))

(define-public crate-sublock-0.1.0 (c (n "sublock") (v "0.1.0") (h "1xcbhpkljkfjsr38smz6hh9igv4bm5smsp945bhn1xnp7ipbff9g")))

(define-public crate-sublock-0.1.1 (c (n "sublock") (v "0.1.1") (h "0wmmdwqwbrmqzzlgqy2dzw7wlfsqhhkj3hg0wmivjhnn7x9ms3m4")))

(define-public crate-sublock-0.2.0 (c (n "sublock") (v "0.2.0") (h "0r5y20y5lj4zsaxyqwda7cdc5bnqncc3jswaqy43r2xp6gf3jq1d")))

