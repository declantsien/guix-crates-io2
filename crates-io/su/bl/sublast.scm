(define-module (crates-io su bl sublast) #:use-module (crates-io))

(define-public crate-sublast-0.0.0 (c (n "sublast") (v "0.0.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "13v5sf1bl976nnhvhjmrh6frrw7s1qccaddlfr59wa74sh4gczfl")))

