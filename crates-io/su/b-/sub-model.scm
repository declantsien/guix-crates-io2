(define-module (crates-io su b- sub-model) #:use-module (crates-io))

(define-public crate-sub-model-0.1.0 (c (n "sub-model") (v "0.1.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0v12205r7x3li9x60zy5xm39p7vcgvp6vj6cv6gsadd44w7xzv5v")))

(define-public crate-sub-model-0.1.1 (c (n "sub-model") (v "0.1.1") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1wi7bsf1d0pfqqzal631df55gz8946mwvn85m41n11pv8nm5znzi") (f (quote (("default") ("auto_derive_serde") ("auto_derive_builder") ("auto_derive_base") ("auto_derive_all" "auto_derive_serde" "auto_derive_base" "auto_derive_builder"))))))

(define-public crate-sub-model-0.2.0 (c (n "sub-model") (v "0.2.0") (d (list (d (n "darling") (r "^0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1hsm1xfwip3c8hbwk2jfrc46fn2ka8gvs6smlbrbjm46knhxa3bs") (f (quote (("default") ("auto_derive_serde") ("auto_derive_builder") ("auto_derive_base") ("auto_derive_all" "auto_derive_serde" "auto_derive_base" "auto_derive_builder"))))))

