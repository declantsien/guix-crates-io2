(define-module (crates-io su b- sub-array) #:use-module (crates-io))

(define-public crate-sub-array-0.1.0 (c (n "sub-array") (v "0.1.0") (h "0lqi8dyk2869k76y57q79ms30mpiyynzdr6gwad5302yn8nb1rg0")))

(define-public crate-sub-array-0.1.1 (c (n "sub-array") (v "0.1.1") (h "0xy4yv57b2vg2rhp6aknj3smi0i4p3ksk01vrzsxc493a4252f6v")))

