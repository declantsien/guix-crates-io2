(define-module (crates-io su b- sub-image) #:use-module (crates-io))

(define-public crate-sub-image-0.1.0 (c (n "sub-image") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "json5") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0mspjlm8wv0rms9hzjqr82sq67y61rwdyf3wmdxp6rldgi2vw702")))

(define-public crate-sub-image-0.1.1 (c (n "sub-image") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.64") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "json5") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "01blf22vjqmls1ld55khdpamzhcyaldwy51m7kp3nvz8nysmpr56")))

