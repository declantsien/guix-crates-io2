(define-module (crates-io su b- sub-strs) #:use-module (crates-io))

(define-public crate-sub-strs-0.1.0 (c (n "sub-strs") (v "0.1.0") (h "0k8a39s37crfrbkf2bjbm7p1mig02w56p5rqgfvm0sg30hl5hjc1")))

(define-public crate-sub-strs-0.2.0 (c (n "sub-strs") (v "0.2.0") (h "1mfxkrgmi5gvf5bwb3rlbs3bmfy5xj7fxni3qpfgap0gakzi8zbk")))

(define-public crate-sub-strs-0.3.1 (c (n "sub-strs") (v "0.3.1") (h "1ihm8hqrgb9r301y0vcsspv83z1bnns4n4x1hpf7cpc04schrc4h")))

(define-public crate-sub-strs-0.4.0 (c (n "sub-strs") (v "0.4.0") (h "0n9gr88c97gis1dwlxps396gs0vsk9ddw8b04l3kfp91xpp66nmz")))

(define-public crate-sub-strs-0.5.0 (c (n "sub-strs") (v "0.5.0") (h "0p2n3nz6hc2mbjq151b5rfj9rmdc9qwbl8n3l63xd8v9zgxirj8a")))

(define-public crate-sub-strs-0.6.0 (c (n "sub-strs") (v "0.6.0") (h "1xkcniam3p1kgxw56plrb9rhib20mdhs7d9walmwixaw7mhpw6dn")))

(define-public crate-sub-strs-0.6.1 (c (n "sub-strs") (v "0.6.1") (h "0zxia001611fr4q6ppw3q1gpdlh2jhwqxvw93a43wz6xnc4srxwv")))

(define-public crate-sub-strs-0.7.0 (c (n "sub-strs") (v "0.7.0") (h "17v7sbc1vhv3vz6m396av21r9r33439zyn25gaxnhniw1z0zbfm3")))

(define-public crate-sub-strs-0.8.0 (c (n "sub-strs") (v "0.8.0") (h "1y24ip0spjjwzk4sqa52klgnhnmp416dwkb4iwf2lnrlhl1c70px")))

(define-public crate-sub-strs-0.9.0 (c (n "sub-strs") (v "0.9.0") (h "03sy1vpjmdjs96sgvrz3q0jsrp32j0bfgd4lizlj6p790ahk1k17")))

(define-public crate-sub-strs-0.10.0 (c (n "sub-strs") (v "0.10.0") (h "0dqryvnsbdcn9ay0iadsh17kbw0xamhhsvp7vifldw6xy0sw9kbc")))

(define-public crate-sub-strs-0.11.0 (c (n "sub-strs") (v "0.11.0") (h "149v6ymihvirir21gbynlrqjgqg9wj44yqs945545aqh0lh7xjvy")))

(define-public crate-sub-strs-0.12.0 (c (n "sub-strs") (v "0.12.0") (h "011g1k19qzpibmbfckwazkcwfy4mv72w9vx0djs76ypipvaljj1j")))

(define-public crate-sub-strs-0.13.0 (c (n "sub-strs") (v "0.13.0") (h "0ln2zdxlrrdlqiw3jvqfpc4krzq3adrpfkl0pfmz0pbbb9wl64lx")))

(define-public crate-sub-strs-0.14.0 (c (n "sub-strs") (v "0.14.0") (h "16kl8zlh55r51nldq8r35m5a2ca0b5nyy483xwf53g1v4zxsxbkd")))

(define-public crate-sub-strs-0.15.0 (c (n "sub-strs") (v "0.15.0") (h "0nbcs3xh43gp9sz6ibbf6jcx8vvkkm3aadsgfj7gf9bb2hq0r3m7")))

(define-public crate-sub-strs-0.15.1 (c (n "sub-strs") (v "0.15.1") (h "02akcyzq9j2hpbp0na0b14s5g69qx7ahfqr16mnywqr06srf57wz")))

(define-public crate-sub-strs-0.15.2 (c (n "sub-strs") (v "0.15.2") (h "1553w132z6bcdp0vrgjd5mclh0fj2m2wsvysdmc4mimwiq58mkls")))

(define-public crate-sub-strs-0.15.3 (c (n "sub-strs") (v "0.15.3") (h "0nzr0shv32fn91glpmp04h1wizb4l7s7hyphy7w4q8qkwwvi4l5x")))

(define-public crate-sub-strs-0.16.0 (c (n "sub-strs") (v "0.16.0") (h "032p45il9ncmszgpmhw3cvghj5xgzx6lp3cgbmxv4rws807dccg6")))

(define-public crate-sub-strs-0.17.0 (c (n "sub-strs") (v "0.17.0") (h "1c5k3aqymypb7wpxmd2jik9f7558rmvwzfcwy2y2iign6jgnxhzv")))

(define-public crate-sub-strs-0.18.0 (c (n "sub-strs") (v "0.18.0") (h "1adn5d32wgd43gccpr7nl62yl9h7zzmq6zn4bijr32ak8hy2dlxk") (f (quote (("std"))))))

(define-public crate-sub-strs-0.19.0 (c (n "sub-strs") (v "0.19.0") (h "05b3xyhi7zkmrnijbbsz48yiykq2dixbs0cq611sxcfv2dy6kjby") (f (quote (("std"))))))

(define-public crate-sub-strs-0.20.0 (c (n "sub-strs") (v "0.20.0") (h "05nbizd6cq9w9p7l4863czqyn5g33zgicjd0c099d84vlidnbrjg") (f (quote (("std"))))))

(define-public crate-sub-strs-0.21.0 (c (n "sub-strs") (v "0.21.0") (h "1spr09lckjzry9i9r9n8jh3wszns20cxhnh3avmlc6nk7xdc3b5a") (f (quote (("std"))))))

(define-public crate-sub-strs-0.22.0 (c (n "sub-strs") (v "0.22.0") (h "0n177yljzl3iypp69dr1bfz5y919bn2hda43h8hszy2ib66hdyvs") (f (quote (("std"))))))

(define-public crate-sub-strs-0.23.0 (c (n "sub-strs") (v "0.23.0") (h "13f0ql4s709magp4ib2k5mh5vcyh6wja5gl3yk8pk7aymk98f7mj") (f (quote (("std"))))))

(define-public crate-sub-strs-0.23.1 (c (n "sub-strs") (v "0.23.1") (h "1dvnwnb65zpmnqd0bl76w8zyf099xp514ha351szjayg5wnyg4nb") (f (quote (("std"))))))

(define-public crate-sub-strs-0.24.0 (c (n "sub-strs") (v "0.24.0") (h "07bmbv4yq0za75572ls0bjyis44bafqsz3jszhcdyssg214gbvvq") (f (quote (("std"))))))

(define-public crate-sub-strs-0.25.0 (c (n "sub-strs") (v "0.25.0") (h "165ydiivsz049rfkqrdbmipnrmgpjzlhrgqb9vc1avbmbmv3sqpm") (f (quote (("std"))))))

(define-public crate-sub-strs-0.25.1 (c (n "sub-strs") (v "0.25.1") (h "1iidn2fca26yc3011qibnsc12g34n6kyfg8y0lb39knysm92wrp1") (f (quote (("std"))))))

(define-public crate-sub-strs-0.25.2 (c (n "sub-strs") (v "0.25.2") (h "0myyy09plr9f7b5rccpa42vay8n5978ag53fb5dkrjss6zw66x51") (f (quote (("std"))))))

(define-public crate-sub-strs-0.25.3 (c (n "sub-strs") (v "0.25.3") (h "1zi3qm5xv548p07w4cpvvcj15mfmcxjqi6liirnc6zbjmzrqyf1g") (f (quote (("std"))))))

(define-public crate-sub-strs-0.26.0 (c (n "sub-strs") (v "0.26.0") (h "1rg4rpsya8f80j5jqz5vdzvdww0vj71y43h07j6pznnr7xlyn6bj") (f (quote (("std"))))))

(define-public crate-sub-strs-0.26.1 (c (n "sub-strs") (v "0.26.1") (h "0j1n4c01rjplbzk1amfv71s169a86yxk5nr0arnyqxkns9g1pjmm") (f (quote (("std"))))))

(define-public crate-sub-strs-0.26.2 (c (n "sub-strs") (v "0.26.2") (h "077ybqhcvi4xaay6m85ya8p0px3pgvyj351c43ifvkq2018cdq6z") (f (quote (("std"))))))

(define-public crate-sub-strs-0.26.3 (c (n "sub-strs") (v "0.26.3") (h "1c5jm4a6jxrkcyljy3s4ja4fxdinh72xhsmsf7v6mcvvdhqawlfz") (f (quote (("std"))))))

(define-public crate-sub-strs-0.27.0 (c (n "sub-strs") (v "0.27.0") (h "12d4hr2a7im3mb4pwzav6s99f3k5qvfmj6d70xi0i9y1h7qd664s") (f (quote (("std"))))))

(define-public crate-sub-strs-0.28.0 (c (n "sub-strs") (v "0.28.0") (h "10prnj88sqhvwx12iqx0rjqpmhpgk6siig4s2pvybj9ihlviygxa") (f (quote (("std"))))))

(define-public crate-sub-strs-0.29.0 (c (n "sub-strs") (v "0.29.0") (h "13lpa88y1z0lvcg2xwwjacx4qw61rrj1xhc3k92kawbijyqn6w0r") (f (quote (("std"))))))

(define-public crate-sub-strs-0.29.1 (c (n "sub-strs") (v "0.29.1") (h "1dj7f542x907hsbd14swmq3dc2xqwaq8qqa3cs7c6pd2zc00jx19") (f (quote (("std"))))))

(define-public crate-sub-strs-0.29.2 (c (n "sub-strs") (v "0.29.2") (h "0qjr9vlp4q1in9sxq9lgf51za707axg46yavavvda5vwwwwvpcln") (f (quote (("std"))))))

