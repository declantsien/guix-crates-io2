(define-module (crates-io su b- sub-image-gui) #:use-module (crates-io))

(define-public crate-sub-image-gui-0.1.0 (c (n "sub-image-gui") (v "0.1.0") (d (list (d (n "eframe") (r "^0.19.0") (d #t) (k 0)) (d (n "egui") (r "^0.19.0") (d #t) (k 0)) (d (n "egui_file") (r "^0.2.0") (d #t) (k 0)) (d (n "sub-image") (r "^0.1.0") (d #t) (k 0)))) (h "06q2yjvhcciig1p7k6hm3mag3i2b3sw3sfwwcb3zisr2r2zadysc")))

(define-public crate-sub-image-gui-0.1.1 (c (n "sub-image-gui") (v "0.1.1") (d (list (d (n "eframe") (r "^0.19.0") (d #t) (k 0)) (d (n "egui") (r "^0.19.0") (d #t) (k 0)) (d (n "egui_file") (r "^0.2.0") (d #t) (k 0)) (d (n "sub-image") (r "^0.1.0") (d #t) (k 0)))) (h "1ryklcl69ixq07ynnl5s8wsj3lkg068ljiizy7vlba0l068dvh7i")))

