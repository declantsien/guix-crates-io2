(define-module (crates-io su rp surprise-me) #:use-module (crates-io))

(define-public crate-surprise-me-0.1.0 (c (n "surprise-me") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "surprise-me-derive") (r "^0.1") (d #t) (k 0)))) (h "0xb02yjpnd55ll8q718rpybrg54s53pghd1x5hnxsqg1xqk7acx5")))

