(define-module (crates-io su rp surprise-me-derive) #:use-module (crates-io))

(define-public crate-surprise-me-derive-0.1.0 (c (n "surprise-me-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0zfxhsbszdjsqswa61f3ak1xq2nxahpqqvyxsgfn2kz34xa6sghs")))

