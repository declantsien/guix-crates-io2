(define-module (crates-io su rp surplus-compiler) #:use-module (crates-io))

(define-public crate-surplus-compiler-0.1.0 (c (n "surplus-compiler") (v "0.1.0") (d (list (d (n "oxc") (r "^0.4.0") (d #t) (k 0)) (d (n "oxc_codegen") (r "^0.4.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1ffh9xhknfwglz2wn2s3rwi8p0hz1wjj2h7lp1128wp7wprr18wf")))

(define-public crate-surplus-compiler-0.2.0 (c (n "surplus-compiler") (v "0.2.0") (d (list (d (n "oxc") (r "^0.4.0") (d #t) (k 0)) (d (n "oxc_codegen") (r "^0.4.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0hb8gg6ql8npn2v135n0fh7qwyq2cym0283m2hz83m9p3pkf1fx2")))

