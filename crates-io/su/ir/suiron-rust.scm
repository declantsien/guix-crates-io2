(define-module (crates-io su ir suiron-rust) #:use-module (crates-io))

(define-public crate-suiron-rust-0.1.0 (c (n "suiron-rust") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serial_test") (r "^0.4.0") (d #t) (k 2)) (d (n "thread_timer") (r "^0.3.0") (d #t) (k 0)))) (h "0hms79705mc2gjm9x3sva33nf1nk577hxwrzvvja52n71yh0ls2k")))

