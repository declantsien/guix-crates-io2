(define-module (crates-io su nc suncalc) #:use-module (crates-io))

(define-public crate-suncalc-0.3.0 (c (n "suncalc") (v "0.3.0") (h "0sg6755bvjpgm3hmhw4jcqxvs0ad9k8w905611v7pyf1k21ixzzi")))

(define-public crate-suncalc-0.3.1 (c (n "suncalc") (v "0.3.1") (h "1nv4lil98jzqh0vzsc8kdi2jnv5fb6wyvyqbysnb34j67l7la1m4")))

(define-public crate-suncalc-0.4.0 (c (n "suncalc") (v "0.4.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)))) (h "0ic0wvl11mgphx589rnpwmc3wk015ri64i2imn9zlbgxjv2l8343")))

