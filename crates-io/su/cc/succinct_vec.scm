(define-module (crates-io su cc succinct_vec) #:use-module (crates-io))

(define-public crate-succinct_vec-0.1.0 (c (n "succinct_vec") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1d3j13yga78clzs4vrwp8qn210mjmbjlnkhvyxhbx4mxi1kg1v95")))

