(define-module (crates-io su cc succinct) #:use-module (crates-io))

(define-public crate-succinct-0.0.1 (c (n "succinct") (v "0.0.1") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "typenum") (r "^1.3") (d #t) (k 0)))) (h "1h199jns3bjrd2p8lxgflhzxcb7z5wf033w2v4q35s7g7c6g3p7h")))

(define-public crate-succinct-0.1.0 (c (n "succinct") (v "0.1.0") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1yybwa4zyy890ax5dkll9p3jn0gjs3babl2cc8cik2i4q06x3plb")))

(define-public crate-succinct-0.1.1 (c (n "succinct") (v "0.1.1") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "0p90kaczgij35q9lf20d90rrs0kw73mcz2xch7bya8rdr541l6v4")))

(define-public crate-succinct-0.2.0 (c (n "succinct") (v "0.2.0") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "08r9wnqb18v0a1vj32dbay61i92hxf4xh34ai3gv24vbxr36ab5q")))

(define-public crate-succinct-0.2.1 (c (n "succinct") (v "0.2.1") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1ms3h7833qvj16kwb2gz5c6pmgw4nqayqgqiwjs045d5n7a4pwi6")))

(define-public crate-succinct-0.2.2 (c (n "succinct") (v "0.2.2") (d (list (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1c4bbnyj3qlamk53d6ghn9zn1gpmnkxfhkgf3qbwbwrpb54kal5l")))

(define-public crate-succinct-0.3.0 (c (n "succinct") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1kpw4jz1y82gbkrcvr4q7qqm5s1v5nahhr3rq987268cmqmj09ar")))

(define-public crate-succinct-0.3.1 (c (n "succinct") (v "0.3.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "130cssp12zqcc05pccgbyqj3s6vlyzlv0z4rs6plz1d8az4b920g")))

(define-public crate-succinct-0.3.2 (c (n "succinct") (v "0.3.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1mhrsr074i43y8sgb31g323ijj566kjjij72l6644csjah8fasgc")))

(define-public crate-succinct-0.4.0 (c (n "succinct") (v "0.4.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "0ix7rsipx3i1xw5n2c5348s5slysp3pphz26g7aknwgs72qlkdg0")))

(define-public crate-succinct-0.4.1 (c (n "succinct") (v "0.4.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "13ywvvzi8jbf2mbnbk1850glwhwbd58flxmrm2j8sx53a6djm7dp")))

(define-public crate-succinct-0.4.2 (c (n "succinct") (v "0.4.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1fibffidk7ivcvfhapp2f4x92fjvw9dvym071ix91xzc1wf19jsh")))

(define-public crate-succinct-0.4.3 (c (n "succinct") (v "0.4.3") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "1flcjxjv947l739z9jrkya6fhp6g9sj4s8ngc37wq31di45g1q3q")))

(define-public crate-succinct-0.4.4 (c (n "succinct") (v "0.4.4") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6.2") (d #t) (k 2)))) (h "1nk4q58msmbsqknyv0rsjz15axqyxcyknrigap6qcx840k2i976b")))

(define-public crate-succinct-0.5.0 (c (n "succinct") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)))) (h "0xc0viyd5v2gk95k380npssaakr5x8vbjym367488cnc47i2cdws")))

(define-public crate-succinct-0.5.1 (c (n "succinct") (v "0.5.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)))) (h "10zq6rn50b1q84lblk39qbav4p76lr91vjjia5gk4ihc0dvqfjmy")))

(define-public crate-succinct-0.5.2 (c (n "succinct") (v "0.5.2") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)))) (h "0654c9gq50x7djyf25zbzz3d2pc4x3z21wmjj3qbr6d9h4hbd63p")))

