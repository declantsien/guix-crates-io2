(define-module (crates-io su cc succinct_rs) #:use-module (crates-io))

(define-public crate-succinct_rs-0.1.0 (c (n "succinct_rs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "07myrxf0m0hldv6i430l7i7hkknf0sirc2nnvcf9w2k6a9mri0js")))

(define-public crate-succinct_rs-0.1.1 (c (n "succinct_rs") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1dabf4z72jasw2qjn7cx3blz0kz96h1lgsz2csw4vf2bzgfa63xm")))

(define-public crate-succinct_rs-0.2.0 (c (n "succinct_rs") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "17cv0zcr693aaff0ma1hrb1z7mznzkg2xg48cd3i1vsishikpx80")))

(define-public crate-succinct_rs-0.3.0 (c (n "succinct_rs") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "015kfid9g775y0cx20si273485razk9snc0k6iny1150zp4v3q78")))

(define-public crate-succinct_rs-0.4.0 (c (n "succinct_rs") (v "0.4.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "070d7vfs5nxrylwsx7v0n0xqhnbbvph1fwjggpcxqgkfmmrqkc4x")))

(define-public crate-succinct_rs-0.5.0 (c (n "succinct_rs") (v "0.5.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "16j85n8ch443qhibaggkc4lwrz9jscmr36lqgplja9g42fxn8wj5")))

(define-public crate-succinct_rs-0.6.0 (c (n "succinct_rs") (v "0.6.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1aw008qp7gqaql18m96va56mcrbdzvfqyhbwb47x2z0sifdgfxl8")))

(define-public crate-succinct_rs-0.6.1 (c (n "succinct_rs") (v "0.6.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0l2i93lk2jgzzr8hz483sh51sc5j8gy14m9vvavkwjvkdnjrs03p")))

