(define-module (crates-io su mc sumcol) #:use-module (crates-io))

(define-public crate-sumcol-0.1.0 (c (n "sumcol") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "predicates") (r "^3") (d #t) (k 2)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1kzl2y97zjyswpsmqxjl0wdr0xakkg66jzvshfm1n1gx28dqm8zx")))

(define-public crate-sumcol-0.1.1 (c (n "sumcol") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "predicates") (r "^3") (d #t) (k 2)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1qsnzba3nd0fknmv942vgbj26qbmjbjpldwxc05gahyn2fskbg3x")))

(define-public crate-sumcol-0.1.2 (c (n "sumcol") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "predicates") (r "^3") (d #t) (k 2)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "174bkn6gcr7vvnvaq8kdhac82x3fa1bvjm2amzgsb37zyclwi2mw")))

(define-public crate-sumcol-0.1.3 (c (n "sumcol") (v "0.1.3") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "predicates") (r "^3") (d #t) (k 2)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1il55c30srhryhfvmhv3fd6jg74xr9vik17pdm5c2102hsdcpdv8")))

