(define-module (crates-io su pp supports-unicode) #:use-module (crates-io))

(define-public crate-supports-unicode-1.0.0 (c (n "supports-unicode") (v "1.0.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "1vjz6qn64aavj60ndj47h6pa7f03dm1raw200vrg36azj8qyz242")))

(define-public crate-supports-unicode-1.0.1 (c (n "supports-unicode") (v "1.0.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "0bv2nkr1y6fpq1mgjhchg7ij0qrfv8n9dg8k16a5bci0ls1s4pvx")))

(define-public crate-supports-unicode-1.0.2 (c (n "supports-unicode") (v "1.0.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)))) (h "1cpq6mbixlpdibwx203p6qh7kpzqy9yin7y5ird14ys1bgj4bfd8")))

(define-public crate-supports-unicode-2.0.0 (c (n "supports-unicode") (v "2.0.0") (d (list (d (n "is-terminal") (r "^0.4.0") (d #t) (k 0)))) (h "1xxscsdjmdp7i3ikqnnivfn4hnpy4gp9as4hshgd4pdb82r2qv2b")))

(define-public crate-supports-unicode-2.1.0 (c (n "supports-unicode") (v "2.1.0") (d (list (d (n "is-terminal") (r "^0.4.0") (d #t) (k 0)))) (h "0yp703pvpzpmaw9mpncvwf0iqis4xmhs569ii1g20jhqvngc2l7q")))

(define-public crate-supports-unicode-3.0.0 (c (n "supports-unicode") (v "3.0.0") (h "1qpc344453x3ai4k9iygxnbk6lr2nw5jflj8ns5q3dbcmwq1lh5p") (r "1.70.0")))

