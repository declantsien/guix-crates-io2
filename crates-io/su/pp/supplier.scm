(define-module (crates-io su pp supplier) #:use-module (crates-io))

(define-public crate-supplier-0.0.0 (c (n "supplier") (v "0.0.0") (h "0sx30gc68wvxjmdygv9hl8bvd2k4fypdz5bdn1d7zfp4dmxgsfq2")))

(define-public crate-supplier-0.0.1 (c (n "supplier") (v "0.0.1") (h "1rddd0d43ggzky2qzyriisy9lj4nn8y3ch8crsvsarkkrl9jmic8")))

(define-public crate-supplier-0.0.2 (c (n "supplier") (v "0.0.2") (h "1qmb2c9dnhsvvgxpjhdc70rjyrd2rjz7l4g6pcmcr5xb9lgg4s8f")))

