(define-module (crates-io su pp suppaftp-cli) #:use-module (crates-io))

(define-public crate-suppaftp-cli-4.5.0 (c (n "suppaftp-cli") (v "4.5.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpassword") (r "^5") (d #t) (k 0)) (d (n "suppaftp") (r "^4.5") (f (quote ("native-tls"))) (d #t) (k 0)))) (h "1rgnm6i84f4aza5410416nz9jxvd4cvzl0xg5s2c4mx0kdsm42bk")))

(define-public crate-suppaftp-cli-4.7.0 (c (n "suppaftp-cli") (v "4.7.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpassword") (r "^7.2") (d #t) (k 0)) (d (n "suppaftp") (r "^4.7") (f (quote ("native-tls"))) (d #t) (k 0)))) (h "1h2yh20hxa5hd5lgfwkwllqx10wqcvfn1nsvlr3fcbh15md6q86w")))

(define-public crate-suppaftp-cli-5.0.0 (c (n "suppaftp-cli") (v "5.0.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpassword") (r "^7.2") (d #t) (k 0)) (d (n "suppaftp") (r "^5.0") (f (quote ("native-tls"))) (d #t) (k 0)))) (h "1dydbfdqahhcb288iwhsxciy8nvinv93cl1b7803psa0lg6vnnnq")))

(define-public crate-suppaftp-cli-5.2.0 (c (n "suppaftp-cli") (v "5.2.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpassword") (r "^7.2") (d #t) (k 0)) (d (n "suppaftp") (r "^5.2") (f (quote ("native-tls"))) (d #t) (k 0)))) (h "1wplkfahfkwnbn1q5zbwxfz7d4l38xwmw79240vma04hi5hz8cjy")))

(define-public crate-suppaftp-cli-5.4.0 (c (n "suppaftp-cli") (v "5.4.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpassword") (r "^7.2") (d #t) (k 0)) (d (n "suppaftp") (r "^5.4") (f (quote ("native-tls"))) (d #t) (k 0)))) (h "08mn1lvrrbjj1jal21h9idv754xxy3sa48pq6lbccycipfsx6k9l")))

(define-public crate-suppaftp-cli-6.0.0 (c (n "suppaftp-cli") (v "6.0.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpassword") (r "^7.2") (d #t) (k 0)) (d (n "suppaftp") (r "^6.0") (f (quote ("native-tls"))) (d #t) (k 0)))) (h "0haq3njvjmva5gk2bb31cxw33zc87kj1pl55fn1n9jcli454xfh4")))

(define-public crate-suppaftp-cli-6.0.1 (c (n "suppaftp-cli") (v "6.0.1") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpassword") (r "^7.2") (d #t) (k 0)) (d (n "suppaftp") (r "^6.0") (f (quote ("native-tls"))) (d #t) (k 0)))) (h "0wg33xapdp2l0krfiv2dvbmacc7ps2kvzb6n143i3x7m8w1va7b2")))

