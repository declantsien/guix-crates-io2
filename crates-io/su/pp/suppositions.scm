(define-module (crates-io su pp suppositions) #:use-module (crates-io))

(define-public crate-suppositions-0.1.0 (c (n "suppositions") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-slice") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 0)))) (h "0h9y82gaci8xa34iyvx44wkqa8xigp2fx3n6i9xvq6i7vs524f30")))

(define-public crate-suppositions-0.1.1 (c (n "suppositions") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-slice") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 0)))) (h "1ambljzp2yhgdj58vf2iyqsysimcrbb55y1hcb6dcz9n806j6nfs")))

(define-public crate-suppositions-0.1.2 (c (n "suppositions") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-slice") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 0)))) (h "02sm4s5sn19hpjj1dvsf11lwxndjiaz5rzcdmj185d16j1c2qn58")))

(define-public crate-suppositions-0.1.3 (c (n "suppositions") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-slice") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 0)))) (h "1acx83a2fmldb9rizpc8dp92cjl56hhsr9payw9csr9w140vd3m7")))

(define-public crate-suppositions-0.1.4 (c (n "suppositions") (v "0.1.4") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-slice") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 0)))) (h "1n03ipkqgpvdmjcz9xp9d6yyf58rq7c5g4ki76k848a18r59m9qm")))

