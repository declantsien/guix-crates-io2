(define-module (crates-io su pp supports-color) #:use-module (crates-io))

(define-public crate-supports-color-1.0.0 (c (n "supports-color") (v "1.0.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "ci_info") (r "^0.14.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1vg9pq7cdf4ylrr71v13yghksa6kaigiz0jdq8y8qrhvxaf0wcx2")))

(define-public crate-supports-color-1.0.1 (c (n "supports-color") (v "1.0.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "ci_info") (r "^0.14.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0y7hbny7m6zrhswncxall2pp16zjvds2lp5jjd157nrws9qbv4w4")))

(define-public crate-supports-color-1.0.2 (c (n "supports-color") (v "1.0.2") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "ci_info") (r "^0.14.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0g02x4w1my3kqwbr40c09byxkx9282np6qyc4jhpkwwkg4a0dg20")))

(define-public crate-supports-color-1.0.3 (c (n "supports-color") (v "1.0.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "is_ci") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "07bbyfb550r2n3dp970fxlkgxrqab1b4j2d6dpiki01h3pg17i17")))

(define-public crate-supports-color-1.0.4 (c (n "supports-color") (v "1.0.4") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "is_ci") (r "^1.0.1") (d #t) (k 0)))) (h "05xvbg7cznczg4wgqy2pv3irl4y35k4z6wd44haj9aja6kbcx2pd")))

(define-public crate-supports-color-1.1.0 (c (n "supports-color") (v "1.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "is_ci") (r "^1.0.1") (d #t) (k 0)))) (h "1x9kpv5i7q2cjmf44n4q2j0k7v1dnq9d5d2rzc0g45cj91r4lq4x")))

(define-public crate-supports-color-1.1.1 (c (n "supports-color") (v "1.1.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "is_ci") (r "^1.1.1") (d #t) (k 0)))) (h "1vlg0i84qpqa0vlilmxpfl6vc5kligb9sii8fbi55pc9wvwv1x9j")))

(define-public crate-supports-color-1.2.0 (c (n "supports-color") (v "1.2.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "is_ci") (r "^1.1.1") (d #t) (k 0)))) (h "0a406vsvkyqibp72y8kdfw5w43lia9gm2rb7kyxr7rlfhxayyg1z")))

(define-public crate-supports-color-1.3.0 (c (n "supports-color") (v "1.3.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "is_ci") (r "^1.1.1") (d #t) (k 0)))) (h "1yfi8yswjipk3ljzm79rbf3phw2fwlznhji1iap7pm4idg9wwwj8")))

(define-public crate-supports-color-1.3.1 (c (n "supports-color") (v "1.3.1") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "is_ci") (r "^1.1.1") (d #t) (k 0)))) (h "0vqdhwc3yf1bv1xbaz5d8p2brmlv1ap4fhwg8pfjzr3yrbrgm9lb")))

(define-public crate-supports-color-2.0.0 (c (n "supports-color") (v "2.0.0") (d (list (d (n "is-terminal") (r "^0.4.0") (d #t) (k 0)) (d (n "is_ci") (r "^1.1.1") (d #t) (k 0)))) (h "0m5kayz225f23k5jyjin82sfkrqhfdq3j72ianafkazz9cbyfl29")))

(define-public crate-supports-color-2.1.0 (c (n "supports-color") (v "2.1.0") (d (list (d (n "is-terminal") (r "^0.4.0") (d #t) (k 0)) (d (n "is_ci") (r "^1.1.1") (d #t) (k 0)))) (h "12csf7chawxinaapm9rh718nha9hggk6ra86fdaw9hxdagg8qffn")))

(define-public crate-supports-color-3.0.0 (c (n "supports-color") (v "3.0.0") (d (list (d (n "is_ci") (r "^1.2.0") (d #t) (k 0)))) (h "0kw5miaai8sarcikzdvsf2ys6rkakngyf2g4yifmgz0xc8ab6acq") (r "1.70.0")))

