(define-module (crates-io su rj surjective-enum) #:use-module (crates-io))

(define-public crate-surjective-enum-0.1.0 (c (n "surjective-enum") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "01z4s35acpc9p7p54q2560xn7ypanp9jh6clzszfdwqnzn1jrgiv")))

