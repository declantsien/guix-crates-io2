(define-module (crates-io su gi sugiura-hiromichi_mylibrary) #:use-module (crates-io))

(define-public crate-sugiura-hiromichi_mylibrary-1.1.2 (c (n "sugiura-hiromichi_mylibrary") (v "1.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "0yq5j0cxsj5ghlsmjf5fcjmxym42rdm1aq2riyhyggm56lqrv4jd")))

(define-public crate-sugiura-hiromichi_mylibrary-1.1.3 (c (n "sugiura-hiromichi_mylibrary") (v "1.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)))) (h "06ldaq41cvm04cvrj1nzbkhlh1fdiajz4x4dva1z1vz4nf9fnr2i")))

