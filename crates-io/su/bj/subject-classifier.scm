(define-module (crates-io su bj subject-classifier) #:use-module (crates-io))

(define-public crate-subject-classifier-0.2.0 (c (n "subject-classifier") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1xx8qm75njvfm7wn0bx3n5dlji682jzigh4nys95vs3r2w5vnxfb")))

(define-public crate-subject-classifier-0.3.0 (c (n "subject-classifier") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ymy0dnkqc094ajvk1d1c70ip252g97p13f4s59nn8b62byng839")))

(define-public crate-subject-classifier-0.4.0 (c (n "subject-classifier") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "045x7a091gr99p38j552rp79vr5r0xpmhgaymz0mddz8fjpqkpny")))

(define-public crate-subject-classifier-0.4.2 (c (n "subject-classifier") (v "0.4.2") (d (list (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0l4fbqrz1r6gqh9wg9x43bkd3c85j1gcl86nbc3nrhbwhidf5fgy")))

