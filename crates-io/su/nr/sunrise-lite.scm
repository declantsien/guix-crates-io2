(define-module (crates-io su nr sunrise-lite) #:use-module (crates-io))

(define-public crate-sunrise-lite-1.0.0 (c (n "sunrise-lite") (v "1.0.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (k 0)) (d (n "libm") (r "^0.2.2") (d #t) (k 0)))) (h "1xxvvjnjbs46nppc7jnk3jqfwj3iyrl3hn15kakw6l47s13h9vzg")))

