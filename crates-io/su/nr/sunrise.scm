(define-module (crates-io su nr sunrise) #:use-module (crates-io))

(define-public crate-sunrise-1.0.0 (c (n "sunrise") (v "1.0.0") (d (list (d (n "approx") (r "^0.3.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0byzgwm5cjnn03a1zyxw73ky3kx016ysk89gzjs5mwr5am5b5sii")))

(define-public crate-sunrise-1.0.1 (c (n "sunrise") (v "1.0.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (k 0)))) (h "1nial9b86g45g072ygg8xd2c30ha2lkgacf75lgnp58n0j1wa5rj")))

