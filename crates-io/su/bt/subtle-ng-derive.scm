(define-module (crates-io su bt subtle-ng-derive) #:use-module (crates-io))

(define-public crate-subtle-ng-derive-0.0.0 (c (n "subtle-ng-derive") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "subtle-ng") (r "^2.5.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "13f61cc7wlz0aqbfr4byp8zxskygsqynykxmwndsq4b3vyhqvrkd")))

(define-public crate-subtle-ng-derive-0.0.1 (c (n "subtle-ng-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "subtle-ng") (r "^2.5.0") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vz5zx5f6mhh2xcjz4sn8lxizw06rh0pkf9kg1bhy3diifmi2v91")))

(define-public crate-subtle-ng-derive-0.0.2 (c (n "subtle-ng-derive") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "subtle-ng") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1qqqkjsrsrfshv4r47hanhl70saki4qbw4wahm3115kyj7087vkh") (f (quote (("with-ng") ("default"))))))

(define-public crate-subtle-ng-derive-0.0.3 (c (n "subtle-ng-derive") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "subtle") (r "^2") (d #t) (k 2) (p "subtle-ng")) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0n6d4dnsav8z34cl1bz9yxx76ypribm99d8q92d1ja1gp08mfn47") (f (quote (("with-ng") ("default"))))))

(define-public crate-subtle-ng-derive-0.0.4 (c (n "subtle-ng-derive") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0q4m5d6q43miz4dascl1sp2ymcghgj2xjhjfcqwlrs4c5fmnm46g") (f (quote (("with-ng") ("default"))))))

(define-public crate-subtle-ng-derive-0.0.5 (c (n "subtle-ng-derive") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0w0640kvpz0h549wi81m9bc24nslx5wp5b53cscgcmk9k0jcw2nh") (f (quote (("with-ng") ("default"))))))

