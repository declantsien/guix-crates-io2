(define-module (crates-io su bt subtle-ml) #:use-module (crates-io))

(define-public crate-subtle-ml-2.5.0 (c (n "subtle-ml") (v "2.5.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1ba9l594pf8dy50b25yzczzxk41l94zd2pwr7hvh6mz0ag60babq") (f (quote (("std") ("nightly") ("i128") ("default" "std" "i128"))))))

