(define-module (crates-io su bt subtitles) #:use-module (crates-io))

(define-public crate-subtitles-0.1.0 (c (n "subtitles") (v "0.1.0") (d (list (d (n "unicode-segmentation") (r "^1.7.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "1x49wfxnzdcb4mrp3lndrbbm70bwwxaskiivd9d4hxwlraqg9fp8")))

(define-public crate-subtitles-0.1.1 (c (n "subtitles") (v "0.1.1") (d (list (d (n "unicode-segmentation") (r "^1.7.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "1hiwpk8fwf4jifbc0233gr9qgca88dqpdl34ivh8w0w411wqdvvl")))

(define-public crate-subtitles-0.1.2 (c (n "subtitles") (v "0.1.2") (d (list (d (n "unicode-segmentation") (r "^1.7.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "1c2ib2gqxz9kypyxpa1gdv2l7va0710lwwbw1v41ckdfaha3l0x2")))

(define-public crate-subtitles-0.1.3 (c (n "subtitles") (v "0.1.3") (d (list (d (n "unicode-segmentation") (r "^1.7.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "0pladcr33m5rlrjk0hbqcj6zy4a101g1x8rx5nnc330ylir5zsh1")))

(define-public crate-subtitles-0.1.4 (c (n "subtitles") (v "0.1.4") (d (list (d (n "unicode-segmentation") (r "^1.7.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "099gsz84wz1ir8hy0f36iglfaw216lykj72fci7piigyxhv7cn3c")))

(define-public crate-subtitles-0.1.5 (c (n "subtitles") (v "0.1.5") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "0gfjrxy6g6jgbgrzvnasxgg6mn61xbhwscdj7ix0cgcfbfm06ygm")))

(define-public crate-subtitles-0.2.0 (c (n "subtitles") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)))) (h "1il8nlrxsi9fava5j05g9v7c66sljscbic5z9pnp604lspi3rv8q")))

