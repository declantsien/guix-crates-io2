(define-module (crates-io su bt subtle-ng) #:use-module (crates-io))

(define-public crate-subtle-ng-2.4.0 (c (n "subtle-ng") (v "2.4.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1mrq5rz2sas7mh8xp1i3kgrw2ihcrllsmwassii2cx3p1cw210p1") (f (quote (("std") ("nightly") ("i128") ("default" "std" "i128"))))))

(define-public crate-subtle-ng-2.4.1 (c (n "subtle-ng") (v "2.4.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0gqsdqslhgmlz203rxpmp5zzd4fb1k5kkr6x72pxc5g7y22wyjc0") (f (quote (("std") ("nightly") ("i128") ("default" "std" "i128"))))))

(define-public crate-subtle-ng-2.5.0 (c (n "subtle-ng") (v "2.5.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0hj1wp8xl64bjhbvlfffmllqy7wdw2b505f32gn3qqic4vmpcikk") (f (quote (("std") ("nightly") ("i128") ("default" "std" "i128"))))))

