(define-module (crates-io su bt subtoml) #:use-module (crates-io))

(define-public crate-subtoml-0.1.0 (c (n "subtoml") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.16") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1.3") (d #t) (k 0)))) (h "08913gl7si8jmx6r5br4cv05bn33wzn0x61hydyndrj9lvzqvahb")))

