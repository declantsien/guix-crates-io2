(define-module (crates-io su bt subtle) #:use-module (crates-io))

(define-public crate-subtle-0.1.0 (c (n "subtle") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0p609h9yi43x28hw17scwcpc7v9kr1fmhid1dlzzy1i5q5v1b0bv") (f (quote (("std" "num-traits") ("default" "std") ("bench")))) (y #t)))

(define-public crate-subtle-0.2.0 (c (n "subtle") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1wmhsz6qi266las5bac7ldpmzwwja1g0yk8i6gz8p850xr5zrnrj") (f (quote (("std" "num-traits") ("nightly") ("default" "std") ("bench")))) (y #t)))

(define-public crate-subtle-0.3.0 (c (n "subtle") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1i83p2932q219a8af1f53zflrbz098jg8l6psgmh3z9ygjsvm9n7") (f (quote (("std" "num-traits") ("nightly") ("default" "std") ("bench")))) (y #t)))

(define-public crate-subtle-0.4.0 (c (n "subtle") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0jjpz4f4kgqda5jlxgx9xwa4h6zzm06j2y2qkjpf38jsirwy0lls") (f (quote (("std" "num-traits") ("nightly") ("generic-impls") ("default" "std") ("bench")))) (y #t)))

(define-public crate-subtle-0.5.0 (c (n "subtle") (v "0.5.0") (h "1wwiaiy07da39w41wgd6kwm4c8lligbc0hi8kshwb3nrgnyzcym6") (f (quote (("std") ("nightly") ("generic-impls") ("default" "std") ("bench")))) (y #t)))

(define-public crate-subtle-0.5.1 (c (n "subtle") (v "0.5.1") (h "1yw08a9f9728m6sny2h94x2q0c0nrk618fh6ilshfm7fq99n6zyw") (f (quote (("std") ("nightly") ("generic-impls") ("default" "std") ("bench")))) (y #t)))

(define-public crate-subtle-0.6.0 (c (n "subtle") (v "0.6.0") (h "0m0j7dg6jz145vjs8xv9m554l08kk2dsg7awf04jx2yh6di6ymsd") (f (quote (("std") ("nightly") ("generic-impls") ("default" "std" "generic-impls") ("bench")))) (y #t)))

(define-public crate-subtle-0.6.1 (c (n "subtle") (v "0.6.1") (h "0gi7202pldsjhvch80071zfc9043yrk3hp0x3j6zpnzx2hwgk4bs") (f (quote (("std") ("nightly") ("generic-impls") ("default" "std" "generic-impls") ("bench")))) (y #t)))

(define-public crate-subtle-0.6.2 (c (n "subtle") (v "0.6.2") (h "11j3b46a45rvr2m9x9n6n7p68wz9m8wv3v8m6nwcqfbq6x3fydlx") (f (quote (("std") ("nightly") ("generic-impls") ("default" "std" "generic-impls") ("bench")))) (y #t)))

(define-public crate-subtle-0.6.3 (c (n "subtle") (v "0.6.3") (h "0z8i79ni7zrdvplyzr5vd6b2a88jmsack886g3mh7k59x6vdvlmk") (f (quote (("std") ("nightly") ("generic-impls") ("default" "std" "generic-impls") ("bench")))) (y #t)))

(define-public crate-subtle-0.7.0 (c (n "subtle") (v "0.7.0") (h "05c5iqhbawa06gii34m8rlsh8b4b3zn1d5zkspi5mg76vn7jxbwd") (f (quote (("std") ("nightly") ("generic-impls") ("default" "std" "generic-impls") ("bench")))) (y #t)))

(define-public crate-subtle-0.7.1 (c (n "subtle") (v "0.7.1") (h "0sg36y3bgzjmsrw2c749nvhfmph98akyqwgh75ikbmhhkywg2f2r") (f (quote (("std") ("nightly") ("generic-impls") ("default" "std" "generic-impls") ("bench")))) (y #t)))

(define-public crate-subtle-0.8.0 (c (n "subtle") (v "0.8.0") (h "1d6rx5d8f07v9k8rq8i8wniv45csrf7l1dn8330z8llb0zqmplwa") (f (quote (("std") ("nightly") ("default" "std") ("bench")))) (y #t)))

(define-public crate-subtle-0.9.0 (c (n "subtle") (v "0.9.0") (h "0j3w1zgbp86pvkx464f9g0kjk22z9dbd7s5yg2prpgn99mgblayz") (f (quote (("std") ("nightly") ("default" "std") ("bench")))) (y #t)))

(define-public crate-subtle-1.0.0 (c (n "subtle") (v "1.0.0") (h "1vm80mxbwfj334izwm8x8l65v1xl9hr0kwrg36r1rq565fkaarrd") (f (quote (("std") ("nightly") ("i128") ("default" "std" "i128"))))))

(define-public crate-subtle-2.0.0-pre.0 (c (n "subtle") (v "2.0.0-pre.0") (h "1d3ar6nl8xr0z9vdx4q5127d4bddnrl3x3danb03cbcd7h9sm0wr") (f (quote (("std") ("nightly") ("i128") ("default" "std" "i128")))) (y #t)))

(define-public crate-subtle-2.0.0 (c (n "subtle") (v "2.0.0") (h "09nr433vny879q2vna1xcsxvdqbyf3rvnbyfci5bgpix5x8n49kh") (f (quote (("std") ("nightly") ("i128") ("default" "std" "i128"))))))

(define-public crate-subtle-2.1.0 (c (n "subtle") (v "2.1.0") (h "0a88l15n23058ry6nxm76n6n2mz7jkbjnadk9a37kcf3yqya3p01") (f (quote (("std") ("nightly") ("i128") ("default" "std" "i128"))))))

(define-public crate-subtle-2.1.1 (c (n "subtle") (v "2.1.1") (h "10mhgwmr8hdnjan0jgss6x8bchbbdx5fpwsgkrq65izzv43hkx01") (f (quote (("std") ("nightly") ("i128") ("default" "std" "i128"))))))

(define-public crate-subtle-2.2.0 (c (n "subtle") (v "2.2.0") (h "0k07sv5ax6mwyfmyn67pfqc1ddny3h0z1i61m2kpr6x8s20bm721") (f (quote (("std") ("nightly") ("i128") ("default" "std" "i128")))) (y #t)))

(define-public crate-subtle-2.2.1 (c (n "subtle") (v "2.2.1") (h "16cpww4f22vvfzhvcvd905z76hkcalr84m23rw68ybn467mz4fmb") (f (quote (("std") ("nightly") ("i128") ("default" "std" "i128"))))))

(define-public crate-subtle-2.2.2 (c (n "subtle") (v "2.2.2") (h "0hg9p61kyqhahk8assj5zcc5nhvyb6506jgkjk1fmjhcn4qdarbw") (f (quote (("std") ("nightly") ("i128") ("default" "std" "i128"))))))

(define-public crate-subtle-2.2.3 (c (n "subtle") (v "2.2.3") (h "1h9jd7v0imksyl5mvnjk2rw54sa3xrril76z0md61mq2gh056bah") (f (quote (("std") ("nightly") ("i128") ("default" "std" "i128"))))))

(define-public crate-subtle-2.3.0 (c (n "subtle") (v "2.3.0") (h "1zg727iw7wh4h1fjljm6yancz75i41rg352y2n7r05991i8kygrl") (f (quote (("std") ("nightly") ("i128") ("default" "std" "i128"))))))

(define-public crate-subtle-2.4.0 (c (n "subtle") (v "2.4.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1hipji54nipkya6szyk9kl7nz07qli570b1isklz78dda44dm08y") (f (quote (("std") ("nightly") ("i128") ("default" "std" "i128"))))))

(define-public crate-subtle-2.4.1 (c (n "subtle") (v "2.4.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "00b6jzh9gzb0h9n25g06nqr90z3xzqppfhhb260s1hjhh4pg7pkb") (f (quote (("std") ("nightly") ("i128") ("default" "std" "i128"))))))

(define-public crate-subtle-2.5.0 (c (n "subtle") (v "2.5.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1g2yjs7gffgmdvkkq0wrrh0pxds3q0dv6dhkw9cdpbib656xdkc1") (f (quote (("std") ("nightly") ("i128") ("default" "std" "i128") ("core_hint_black_box") ("const-generics"))))))

