(define-module (crates-io su bt subtle-encoding) #:use-module (crates-io))

(define-public crate-subtle-encoding-0.0.0 (c (n "subtle-encoding") (v "0.0.0") (h "06lz75cjlh29h5qxpvy00c3j04fn5bgd3asn0b2bp5a1mxflm91f") (y #t)))

(define-public crate-subtle-encoding-0.1.0 (c (n "subtle-encoding") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1y5cx64vwbplbrgvz6f9n1rjr63916mc46qf768b1w54dqry09qf") (f (quote (("std" "alloc" "zeroize") ("nightly") ("hex") ("default" "base64" "hex" "std") ("base64" "zeroize") ("alloc")))) (y #t)))

(define-public crate-subtle-encoding-0.1.1 (c (n "subtle-encoding") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^0.1") (o #t) (d #t) (k 0)))) (h "113cyljlmafbzgihjcmxf08k48mvp4w63vfixad2k66bgx9a62za") (f (quote (("std" "alloc" "zeroize") ("nightly") ("hex") ("default" "base64" "hex" "std") ("base64" "zeroize") ("alloc")))) (y #t)))

(define-public crate-subtle-encoding-0.2.0 (c (n "subtle-encoding") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^0.1") (o #t) (d #t) (k 0)))) (h "189ldl106g81wqsdm7jx1nhvyggajg5f3bhbs461nm2whqajsiml") (f (quote (("std" "alloc" "zeroize") ("nightly") ("hex") ("default" "base64" "hex" "std") ("base64" "zeroize") ("alloc")))) (y #t)))

(define-public crate-subtle-encoding-0.2.1 (c (n "subtle-encoding") (v "0.2.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1fknjcsjyxw5a1blgk6fan75wkfdcrvi9m7bsr1b9ncbwdn78bgn") (f (quote (("std" "alloc" "zeroize") ("nightly") ("hex") ("default" "base64" "hex" "std") ("base64" "zeroize") ("alloc")))) (y #t)))

(define-public crate-subtle-encoding-0.2.2 (c (n "subtle-encoding") (v "0.2.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1sz070ziv7ibpyp7xalym5ppmkab6xsjypyvklkxh4gnrq4z705a") (f (quote (("std" "alloc" "zeroize") ("nightly") ("hex") ("default" "base64" "hex" "std") ("base64" "zeroize") ("alloc")))) (y #t)))

(define-public crate-subtle-encoding-0.2.3 (c (n "subtle-encoding") (v "0.2.3") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1ib19088dj01k2jwc6zd66q3sp22skjh2kd6889c1sdgbp4sxy85") (f (quote (("std" "alloc" "zeroize") ("nightly") ("hex") ("default" "base64" "hex" "std") ("base64" "zeroize") ("alloc")))) (y #t)))

(define-public crate-subtle-encoding-0.3.0-alpha1 (c (n "subtle-encoding") (v "0.3.0-alpha1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^0.4") (o #t) (d #t) (k 0)))) (h "091xifxyzprysb5afis982i5vwfijdm8i84cryag0xf8wq4rf6bg") (f (quote (("std" "alloc" "zeroize") ("nightly") ("hex") ("default" "base64" "hex" "std") ("bech32-preview" "alloc") ("base64" "zeroize") ("alloc")))) (y #t)))

(define-public crate-subtle-encoding-0.3.0 (c (n "subtle-encoding") (v "0.3.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1adh7nxbclg983lpxzsikkqhbd15lw9mxwbxzxb0yyaybpyxjhw6") (f (quote (("std" "alloc" "zeroize") ("nightly") ("hex") ("default" "base64" "hex" "std") ("bech32-preview" "alloc") ("base64" "zeroize") ("alloc"))))))

(define-public crate-subtle-encoding-0.3.1 (c (n "subtle-encoding") (v "0.3.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0wk4w3p03is22pdh4c9w5h8ygpj84yi5iv29af9pww4an22s8d1l") (f (quote (("std" "alloc" "zeroize") ("nightly") ("hex") ("default" "base64" "hex" "std") ("bech32-preview" "alloc") ("base64" "zeroize") ("alloc"))))))

(define-public crate-subtle-encoding-0.3.2 (c (n "subtle-encoding") (v "0.3.2") (d (list (d (n "failure") (r "^0.1") (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^0.5") (o #t) (k 0)))) (h "0p7psika9fajka713f3h0xcg3s7h78xvjvjn77nw5m6aqxxazram") (f (quote (("std" "alloc" "zeroize") ("nightly" "zeroize/nightly") ("hex") ("default" "base64" "hex" "std") ("bech32-preview" "alloc" "zeroize") ("base64" "zeroize") ("alloc"))))))

(define-public crate-subtle-encoding-0.3.3 (c (n "subtle-encoding") (v "0.3.3") (d (list (d (n "failure") (r "^0.1") (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^0.5") (o #t) (k 0)))) (h "0i28a78c3chli8s44ica4p5x525fvmdfmxdj3dk6qp25vkyx95zg") (f (quote (("std" "alloc" "zeroize") ("nightly" "zeroize/nightly") ("hex") ("default" "base64" "hex" "std") ("bech32-preview" "alloc" "zeroize") ("base64" "zeroize") ("alloc"))))))

(define-public crate-subtle-encoding-0.3.4 (c (n "subtle-encoding") (v "0.3.4") (d (list (d (n "failure") (r "^0.1") (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^0.6") (o #t) (k 0)))) (h "011cp0dbricjmczvbvnkv78n8i4hkzcx6jsdfn5lcp33jbw7qdc3") (f (quote (("std" "alloc" "zeroize") ("nightly" "zeroize/nightly") ("hex") ("default" "base64" "hex" "std") ("bech32-preview" "alloc" "zeroize") ("base64" "zeroize") ("alloc"))))))

(define-public crate-subtle-encoding-0.3.5 (c (n "subtle-encoding") (v "0.3.5") (d (list (d (n "failure") (r "^0.1") (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^0.7") (o #t) (k 0)))) (h "0hf0qabx7dz6jv5drqc1a17rrcnmc6clgpa8vspdqpnjdrp33733") (f (quote (("std" "alloc" "zeroize") ("hex") ("default" "base64" "hex" "std") ("bech32-preview" "alloc" "zeroize") ("base64" "zeroize") ("alloc"))))))

(define-public crate-subtle-encoding-0.3.6 (c (n "subtle-encoding") (v "0.3.6") (d (list (d (n "failure") (r "^0.1") (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^0.8") (o #t) (k 0)))) (h "03g5f06pbw97yfyqrfg4lbvnd2ibf17bmi3z9gvhl4szfbbwklrj") (f (quote (("std" "alloc" "zeroize") ("hex") ("default" "base64" "hex" "std") ("bech32-preview" "alloc" "zeroize") ("base64" "zeroize") ("alloc"))))))

(define-public crate-subtle-encoding-0.3.7 (c (n "subtle-encoding") (v "0.3.7") (d (list (d (n "failure") (r "^0.1") (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^0.9") (o #t) (k 0)))) (h "0yflmsqqiwmlsmqbsn790j8aryq14agjsiav3amnzjjfqzb0q6k6") (f (quote (("std" "alloc" "zeroize") ("hex") ("default" "base64" "hex" "std") ("bech32-preview" "alloc" "zeroize") ("base64" "zeroize") ("alloc"))))))

(define-public crate-subtle-encoding-0.4.0 (c (n "subtle-encoding") (v "0.4.0") (d (list (d (n "zeroize") (r "^0.10") (o #t) (k 0)))) (h "0hgxwigz5dpiwh9mnx862wvinn0g1a60kwsphzsxlh80idaqvgah") (f (quote (("std" "alloc" "zeroize") ("hex") ("default" "base64" "hex" "std") ("bech32-preview" "alloc" "zeroize") ("base64" "zeroize") ("alloc"))))))

(define-public crate-subtle-encoding-0.4.1 (c (n "subtle-encoding") (v "0.4.1") (d (list (d (n "zeroize") (r "^1.0.0-pre") (o #t) (k 0)))) (h "10x7q9bpsdqxrpyyq44gadhi6pxf3iqm3n6jdmyyxplbxicjqj9h") (f (quote (("std" "alloc" "zeroize") ("hex") ("default" "base64" "hex" "std") ("bech32-preview" "alloc" "zeroize") ("base64" "zeroize") ("alloc"))))))

(define-public crate-subtle-encoding-0.5.0 (c (n "subtle-encoding") (v "0.5.0") (d (list (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "0fxnpj5hv6s8i7wvf94xa274p53nlzm0sns9dl5nhagp2s51iifb") (f (quote (("std" "alloc" "zeroize") ("hex") ("default" "base64" "hex" "std") ("bech32-preview" "alloc" "zeroize") ("base64" "zeroize") ("alloc"))))))

(define-public crate-subtle-encoding-0.5.1 (c (n "subtle-encoding") (v "0.5.1") (d (list (d (n "zeroize") (r "^1") (o #t) (k 0)))) (h "0icrwnjs67xf4k02x3yq5rgcf4ksvm8jc1a1aknmw31kp3bixjvx") (f (quote (("std" "alloc" "zeroize") ("hex") ("default" "base64" "hex" "std") ("bech32-preview" "alloc" "zeroize") ("base64" "zeroize") ("alloc"))))))

