(define-module (crates-io su bt subtree) #:use-module (crates-io))

(define-public crate-subtree-0.1.0 (c (n "subtree") (v "0.1.0") (h "0hfllv9ip4var0fnxifhb90j8yqjai9rq8ml5qgc57kg2dgch624")))

(define-public crate-subtree-0.1.1 (c (n "subtree") (v "0.1.1") (h "009krn8n0zx4kmp4igfpk6ysh6ahlgksyp8c2x8ap8rf7458nkr9")))

