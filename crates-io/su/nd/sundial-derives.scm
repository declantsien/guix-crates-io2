(define-module (crates-io su nd sundial-derives) #:use-module (crates-io))

(define-public crate-sundial-derives-0.1.0 (c (n "sundial-derives") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full"))) (d #t) (k 0)))) (h "0s6hzrlbl2dy4xn8m4g3hj745is4wvzk10nij4nsc9vhhk08g2gq")))

