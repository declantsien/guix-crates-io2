(define-module (crates-io su mm summon) #:use-module (crates-io))

(define-public crate-summon-0.1.0 (c (n "summon") (v "0.1.0") (d (list (d (n "paste") (r "^0.1.10") (d #t) (k 0)))) (h "10wvpjyvq7mvn532md008wd9dh5qxqxvha6ak8mm9gcdjh3421vz")))

(define-public crate-summon-0.2.0 (c (n "summon") (v "0.2.0") (d (list (d (n "paste") (r "^0.1.10") (d #t) (k 0)) (d (n "stowaway") (r "^2.0.0") (d #t) (k 0)))) (h "04k784y7fi0dmjlnlq1g2fvlcz415kv0w2b3xzxbh1r9vgc3h9br")))

(define-public crate-summon-0.3.0 (c (n "summon") (v "0.3.0") (d (list (d (n "paste") (r "^0.1.10") (d #t) (k 0)))) (h "1k7ml3pr3y9y9gzg54cdavcic5l1562g5p43wcaxafi1nszv6jby")))

(define-public crate-summon-0.3.1 (c (n "summon") (v "0.3.1") (d (list (d (n "paste") (r "^0.1.10") (d #t) (k 0)))) (h "08rcjv3bp4iphkdck3bws49zbc8w6m83r9qpl2idvxfv94w3mxlm")))

