(define-module (crates-io su mm summavy-bitpacker) #:use-module (crates-io))

(define-public crate-summavy-bitpacker-0.2.0 (c (n "summavy-bitpacker") (v "0.2.0") (h "0244bw0x3zs66pmgaqfhjl8fw9s52jb09bm9wsb8gk6xxsy313hm")))

(define-public crate-summavy-bitpacker-0.2.1 (c (n "summavy-bitpacker") (v "0.2.1") (h "16vzblksq57vcjn08p7r0z09iaj3cm7q0gix153rqbn7qhqrmjx0")))

(define-public crate-summavy-bitpacker-0.2.2 (c (n "summavy-bitpacker") (v "0.2.2") (h "07rrys1qaiqri65yyax5ibn79g4yp84g93c96v97gdknz6bghgy2")))

(define-public crate-summavy-bitpacker-0.2.3 (c (n "summavy-bitpacker") (v "0.2.3") (h "1h1axc8m89s94ab8pskah11gcdldi510wgm6wvfy5wnmwwrr2krf")))

(define-public crate-summavy-bitpacker-0.3.0 (c (n "summavy-bitpacker") (v "0.3.0") (h "0g9gwxnfprjvq8x0s2iyrcp4bjran1i5r55jzn09h9b8mfyx2bpp")))

