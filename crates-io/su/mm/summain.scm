(define-module (crates-io su mm summain) #:use-module (crates-io))

(define-public crate-summain-0.1.0 (c (n "summain") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "unix_mode") (r "^0.1") (d #t) (k 0)))) (h "0sc3r061xs1spgdcx0dcc3bj9804lwlyzgal3wx2mcdpncffb2ww")))

(define-public crate-summain-0.26.0 (c (n "summain") (v "0.26.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "unix_mode") (r "^0.1") (d #t) (k 0)))) (h "0r1p65kh8n1i8vx50rk6kv1iz42fpy1xgn1nrzr6fy10bdkwd6z2")))

