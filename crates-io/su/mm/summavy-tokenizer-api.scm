(define-module (crates-io su mm summavy-tokenizer-api) #:use-module (crates-io))

(define-public crate-summavy-tokenizer-api-0.1.0 (c (n "summavy-tokenizer-api") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lxnvr3irhb43y5lp7yp0aszhhzb7vqqpkwy588qxijpd0hnwb87")))

