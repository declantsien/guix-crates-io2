(define-module (crates-io su mm summer-ipfs-client) #:use-module (crates-io))

(define-public crate-summer-ipfs-client-0.1.0 (c (n "summer-ipfs-client") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1zbnvhssrvphyl4rfyw1mvqk6ryadwhs7q2f01ika1336c62ijf7") (r "1.59.0")))

