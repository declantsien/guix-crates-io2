(define-module (crates-io su mm summum-types) #:use-module (crates-io))

(define-public crate-summum-types-0.1.0 (c (n "summum-types") (v "0.1.0") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "09sx0i692d7v59vdr7791xjlflsxnd81f6n9yjnz0r8y1hrm81mm") (f (quote (("default"))))))

(define-public crate-summum-types-0.1.1 (c (n "summum-types") (v "0.1.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "1n8872rdqgfws6m1izlf1w8s7cczcaz5l5ljjpqxskcxr8s7bpnj") (f (quote (("default"))))))

(define-public crate-summum-types-0.1.2 (c (n "summum-types") (v "0.1.2") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "0hwinjf6pbva3gdb0xli3g8ay0vlzniab96y31d7dwb9prb4x979") (f (quote (("default"))))))

(define-public crate-summum-types-0.1.3 (c (n "summum-types") (v "0.1.3") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "1bwn9c32xxk4y3rzkjhxdd9k887a24qarvdymy24pqvl5pai9mpf") (f (quote (("default"))))))

(define-public crate-summum-types-0.1.4 (c (n "summum-types") (v "0.1.4") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.79") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full"))) (d #t) (k 0)))) (h "0lx95hh6sclmq71lz2djpffcd088c2l58m146jkg4gzjrx6nydcg") (f (quote (("default"))))))

