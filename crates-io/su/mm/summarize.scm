(define-module (crates-io su mm summarize) #:use-module (crates-io))

(define-public crate-summarize-0.1.0 (c (n "summarize") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("blocking" "json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.153") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qapj9g9bcnyf5fcigpbbn5i7b53l4m3wbgxzjqzxhgaf1b241h7")))

(define-public crate-summarize-0.2.0 (c (n "summarize") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "multipart" "stream"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.153") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.7") (d #t) (k 0)))) (h "0id8v4am9q5ayqi3m4wsjn8ylq3wd53krw10q5znn200x14by9aj")))

