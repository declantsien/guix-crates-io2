(define-module (crates-io su mm summavy-sstable) #:use-module (crates-io))

(define-public crate-summavy-sstable-0.1.0 (c (n "summavy-sstable") (v "0.1.0") (d (list (d (n "ciborium") (r "^0.2") (d #t) (k 0)) (d (n "common") (r "^0.5") (d #t) (k 0) (p "summavy-common")) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "names") (r "^0.14") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tantivy-fst") (r "^0.4") (d #t) (k 0)))) (h "1ank4sn3xkkbjh8yz4d5gcf9g3k7a759gy0b3vyi9phxry5ysvsa")))

