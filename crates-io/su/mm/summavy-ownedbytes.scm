(define-module (crates-io su mm summavy-ownedbytes) #:use-module (crates-io))

(define-public crate-summavy-ownedbytes-0.3.1 (c (n "summavy-ownedbytes") (v "0.3.1") (d (list (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "0mgm52jd8sfqjn7kyfgg1v4gg5y1qnnk5y655rxmc9giim33lz5q")))

(define-public crate-summavy-ownedbytes-0.3.2 (c (n "summavy-ownedbytes") (v "0.3.2") (d (list (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "07sxv83dl2zl8pkjhqyrx9in2ciszq98sllicd590gjslvbbh80g")))

(define-public crate-summavy-ownedbytes-0.5.0 (c (n "summavy-ownedbytes") (v "0.5.0") (d (list (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "0pq909nf1dld6r0ndljp9av5gmd728sc2ydp6312q2s2kpb6bdxp")))

