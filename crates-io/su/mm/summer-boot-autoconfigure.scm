(define-module (crates-io su mm summer-boot-autoconfigure) #:use-module (crates-io))

(define-public crate-summer-boot-autoconfigure-0.1.0 (c (n "summer-boot-autoconfigure") (v "0.1.0") (d (list (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0bwjd4mk4cgapcppp3c822finx7hra8bhwy8mk88qzfiqx9y8kn5") (y #t) (r "1.60.0")))

(define-public crate-summer-boot-autoconfigure-0.1.1 (c (n "summer-boot-autoconfigure") (v "0.1.1") (d (list (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0h7fxxzxfm5az519na7gaqyhdkga8hghqws30v6yjy7343gf0k1b") (y #t) (r "1.60.0")))

(define-public crate-summer-boot-autoconfigure-0.1.2 (c (n "summer-boot-autoconfigure") (v "0.1.2") (d (list (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0gpzzh1rxn9yj0whpggbiznc7ppqq61bd81z8dym4w79ppakw6jb") (r "1.60.0")))

(define-public crate-summer-boot-autoconfigure-1.0.0 (c (n "summer-boot-autoconfigure") (v "1.0.0") (d (list (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0xvq5ssm1wm31y690pl8dja56d59xzzqjwwmhzdlzqvfxwdw6b4m") (r "1.60.0")))

(define-public crate-summer-boot-autoconfigure-1.0.1 (c (n "summer-boot-autoconfigure") (v "1.0.1") (d (list (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "1acrcdggag697ynwyw1lwz50rispxyl2v8bg0fs7va07f5mqglc0") (r "1.60.0")))

(define-public crate-summer-boot-autoconfigure-1.3.0 (c (n "summer-boot-autoconfigure") (v "1.3.0") (d (list (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "170m87k33jy3vp77yc4c0jxj87lr8cb6nlcm38xgb3gg03a22y89") (r "1.60.0")))

(define-public crate-summer-boot-autoconfigure-1.4.0 (c (n "summer-boot-autoconfigure") (v "1.4.0") (d (list (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.7") (d #t) (k 0)))) (h "1g46gxqlxa3sbkvsmjy2id02k0fjvdmgsbfp66h4znnpzi3ldhch") (r "1.60.0")))

(define-public crate-summer-boot-autoconfigure-1.4.1 (c (n "summer-boot-autoconfigure") (v "1.4.1") (d (list (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.75") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)))) (h "08mgksa3dfzq8gchlpl0d522a2xp9kgl4y8jzi8sznd2i8l6hik5") (r "1.73.0")))

