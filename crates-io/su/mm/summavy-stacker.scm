(define-module (crates-io su mm summavy-stacker) #:use-module (crates-io))

(define-public crate-summavy-stacker-0.1.0 (c (n "summavy-stacker") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "common") (r "^0.5") (d #t) (k 0) (p "summavy-common")) (d (n "murmurhash32") (r "^0.2") (d #t) (k 0)))) (h "0mrv9wks3ymlyvav41qzjkqickvn9jzprfhn0ill2d41dqsd212n")))

