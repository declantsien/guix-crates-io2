(define-module (crates-io su mm summed-area) #:use-module (crates-io))

(define-public crate-summed-area-1.0.0 (c (n "summed-area") (v "1.0.0") (d (list (d (n "imgref") (r "^1.9.4") (d #t) (k 0)) (d (n "rgb") (r "^0.8.34") (k 2)))) (h "1cplxj4ky4qxfmnm0zfpbsdsc59afqy59cs8s0bq4xjapnkjf7sg")))

