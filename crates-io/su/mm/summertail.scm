(define-module (crates-io su mm summertail) #:use-module (crates-io))

(define-public crate-summertail-0.3.0 (c (n "summertail") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "12mmicdfnxw7phvqj2nwkbif1zjhln67620hbb4y7zmpadp60wxm")))

