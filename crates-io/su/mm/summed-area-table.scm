(define-module (crates-io su mm summed-area-table) #:use-module (crates-io))

(define-public crate-summed-area-table-0.0.1 (c (n "summed-area-table") (v "0.0.1") (d (list (d (n "nalgebra") (r "^0.2.5") (d #t) (k 0)))) (h "1lzswcmv70s356wqa3r2pj13adv2cl4yfk42ljg8gzzn4xsrmrd6")))

(define-public crate-summed-area-table-0.0.2 (c (n "summed-area-table") (v "0.0.2") (d (list (d (n "nalgebra") (r "^0.2.14") (d #t) (k 0)) (d (n "num") (r "^0.1.24") (d #t) (k 0)))) (h "082n6vgn35p8mcah00887s0jmm45bxfmpmaxj72s69i24c5g407s")))

(define-public crate-summed-area-table-0.0.3 (c (n "summed-area-table") (v "0.0.3") (d (list (d (n "nalgebra") (r "^0.2.22") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "0663m955shy294xvrqz0gcs626w7gy1283a32xcy6ddpx6bjvl86")))

(define-public crate-summed-area-table-0.0.4 (c (n "summed-area-table") (v "0.0.4") (d (list (d (n "nalgebra") (r "^0.2.22") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "09ncyr5gwfnra6vlaidgi86y3cgpdkx8whbwpkz7xy1ss70wp134")))

(define-public crate-summed-area-table-0.0.5 (c (n "summed-area-table") (v "0.0.5") (d (list (d (n "nalgebra") (r "^0.2.22") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "1ny0nwvl3c2d1gv2sci7j2hy02s4yh2pgjb21c29ffjzs59r08cn")))

(define-public crate-summed-area-table-0.0.6 (c (n "summed-area-table") (v "0.0.6") (d (list (d (n "nalgebra") (r "^0.2.22") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "0zahdx2ihwj5jp4a5lndg2axf7p8lxhgpl8c07pjnzs1822r4ndp")))

(define-public crate-summed-area-table-0.0.7 (c (n "summed-area-table") (v "0.0.7") (d (list (d (n "nalgebra") (r "^0.2.23") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "1w68y388pn9sm1qv7kg35z1kg0zcgx141785871dyclgkf8b6dww")))

(define-public crate-summed-area-table-0.0.8 (c (n "summed-area-table") (v "0.0.8") (d (list (d (n "nalgebra") (r "^0.3.0") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "1b3bav7s0lsjz9yjkqx1r3wvil66ypk5g0k34mzk1q5q95piz337")))

(define-public crate-summed-area-table-0.0.9 (c (n "summed-area-table") (v "0.0.9") (d (list (d (n "nalgebra") (r "^0.4.0") (d #t) (k 0)) (d (n "num") (r "^0.1.28") (d #t) (k 0)))) (h "0zxmhkpikqav103pmm6ajcjmqknl855l6496dn8xhfrh2jh4w46s")))

(define-public crate-summed-area-table-1.0.0 (c (n "summed-area-table") (v "1.0.0") (d (list (d (n "nalgebra") (r "^0.4.0") (d #t) (k 0)))) (h "03dmar46q32hyfp234fm9arvn3qqbhqlqqzlwwhxa1yn6y3iyrl1")))

(define-public crate-summed-area-table-2.0.0 (c (n "summed-area-table") (v "2.0.0") (d (list (d (n "nalgebra") (r "0.10.*") (d #t) (k 0)))) (h "18ryqbw8s0myg8g5n76y1dycxx221ifl7kqx8z32ps9v9spj63fk")))

