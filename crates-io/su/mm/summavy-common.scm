(define-module (crates-io su mm summavy-common) #:use-module (crates-io))

(define-public crate-summavy-common-0.3.0 (c (n "summavy-common") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "ownedbytes") (r "^0.3") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "015bflldjs4y41kfyy6drf7x03w8azws28kdzyyrhj9v2zqwhl6f")))

(define-public crate-summavy-common-0.3.1 (c (n "summavy-common") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "ownedbytes") (r "^0.3") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1519mksic209ql1v7fbzn50hacawg5fzzsy8i550wbl84x1fx4p9")))

(define-public crate-summavy-common-0.3.2 (c (n "summavy-common") (v "0.3.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "ownedbytes") (r "^0.3") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0v7rk0w5qpx6f81ykrpscf7vjzy3xm8v149m2530ngr5pxf62fii")))

(define-public crate-summavy-common-0.3.3 (c (n "summavy-common") (v "0.3.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "ownedbytes") (r "^0.3.2") (d #t) (k 0) (p "summavy-ownedbytes")) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1bcaph88k2qrg6c0zz7dddhalx8al0abaxhpp77jqdkkyi2wlrjw")))

(define-public crate-summavy-common-0.5.0 (c (n "summavy-common") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "ownedbytes") (r "^0.5") (d #t) (k 0) (p "summavy-ownedbytes")) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0dppdkgdsc2jl6sk54ajmr0q7l0mnxhyw1ykq6n5wlq4spax2lw2") (f (quote (("quickwit"))))))

