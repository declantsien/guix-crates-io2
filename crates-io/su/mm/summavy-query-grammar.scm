(define-module (crates-io su mm summavy-query-grammar) #:use-module (crates-io))

(define-public crate-summavy-query-grammar-0.18.0 (c (n "summavy-query-grammar") (v "0.18.0") (d (list (d (n "combine") (r "^4") (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (f (quote ("std" "unicode"))) (k 0)))) (h "0w7b44dd44ml3w9kzayipdfkmnpyvag6dfc26k7qzcxxphhj8sim")))

(define-public crate-summavy-query-grammar-0.18.1 (c (n "summavy-query-grammar") (v "0.18.1") (d (list (d (n "combine") (r "^4") (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (f (quote ("std" "unicode"))) (k 0)))) (h "1mnp28ar8y8ambcdc5sxj74i5sv7zbjkqyxi5kfaymq0r15ga7yp")))

(define-public crate-summavy-query-grammar-0.18.2 (c (n "summavy-query-grammar") (v "0.18.2") (d (list (d (n "combine") (r "^4") (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (f (quote ("std" "unicode"))) (k 0)))) (h "08allaih5b12wwi74b9szbijy8cy2lvv06ffzlc8b4sms9a37yjn")))

