(define-module (crates-io su mh sumhash) #:use-module (crates-io))

(define-public crate-sumhash-0.1.0 (c (n "sumhash") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.59") (d #t) (k 0)) (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "hex") (r "^0.3.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha3") (r "^0.10.1") (d #t) (k 0)))) (h "1j74sjf2hlcrp6rwxf7359r7xnbc26q7rk1n7p4l53655xn6qq9b")))

(define-public crate-sumhash-0.1.1 (c (n "sumhash") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.59") (d #t) (k 0)) (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha3") (r "^0.10.1") (d #t) (k 0)))) (h "1assn92ljrwcm31nhk24jrgjy2jgb8pppfgpidh94ab6fxwjcj7q")))

