(define-module (crates-io su av suave) #:use-module (crates-io))

(define-public crate-suave-0.1.0 (c (n "suave") (v "0.1.0") (h "0qjnqwkhb0zm9xbpdy85s1g0l4x9yl08mxm43sq51xmxkljm5j7i") (r "1.73.0")))

(define-public crate-suave-0.1.1 (c (n "suave") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)))) (h "1islxh4frzd39xmdwwfxnzpnrrsyd4fp0ri7wkj8bz61k0r9ff2k") (r "1.73.0")))

(define-public crate-suave-0.1.2 (c (n "suave") (v "0.1.2") (d (list (d (n "tokio") (r "^1.34.0") (f (quote ("fs"))) (d #t) (k 0)))) (h "18szalpfcha9zy4kfy4vb6phz77llmjbn35x4ax5fvcg81dckgl3") (r "1.73.0")))

(define-public crate-suave-0.1.3 (c (n "suave") (v "0.1.3") (d (list (d (n "tokio") (r "^1.34.0") (f (quote ("fs" "rt" "macros"))) (d #t) (k 0)))) (h "0zshqi3a7jcykg63mywjfh15i20n5yzd8vg2iv5bawihdvx8yq8k") (r "1.73.0")))

(define-public crate-suave-0.1.4 (c (n "suave") (v "0.1.4") (d (list (d (n "tokio") (r "^1.34.0") (f (quote ("fs" "rt" "macros" "io-util" "time"))) (d #t) (k 0)))) (h "1lmj9a7s4ignh9iq8pznd55k3i17a4cblyxrz6kvkqggclw88mkv") (r "1.73.0")))

(define-public crate-suave-0.1.5 (c (n "suave") (v "0.1.5") (d (list (d (n "tokio") (r "^1.34.0") (f (quote ("fs" "rt" "macros" "io-util" "time"))) (d #t) (k 0)))) (h "172kamvb7ijkqi3aaydlzcvqj3m3vkxz944gjz90wv35cv09w7lp") (r "1.73.0")))

(define-public crate-suave-0.2.0 (c (n "suave") (v "0.2.0") (d (list (d (n "arboard") (r "^3.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("fs" "rt" "macros" "io-util" "time"))) (d #t) (k 0)))) (h "1hswbhf23zld85zdh6flib68aicz78lqjxz90ljf130rn97hkl2c") (r "1.73.0")))

(define-public crate-suave-0.2.1 (c (n "suave") (v "0.2.1") (d (list (d (n "arboard") (r "^3.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("fs" "rt" "macros" "io-util" "time"))) (d #t) (k 0)))) (h "1v3523snc497l4b38cagkcjsjk08s2lh3rh1gpw1rf7irk0bmqlm") (r "1.73.0")))

(define-public crate-suave-0.2.2 (c (n "suave") (v "0.2.2") (d (list (d (n "arboard") (r "^3.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("fs" "rt" "macros" "io-util" "time"))) (d #t) (k 0)))) (h "0w6m3wr4gyw5yd9l7y674kljfgfvf3d6zcmw132b880yf8lin0ca") (r "1.73.0")))

(define-public crate-suave-0.2.3 (c (n "suave") (v "0.2.3") (d (list (d (n "arboard") (r "^3.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.150") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("fs" "rt" "macros" "io-util" "time"))) (d #t) (k 0)))) (h "1gdilny86bxsl4vsaf659dd6asc118y0kwjsydxgbf3mh7f5qzrd") (f (quote (("queue")))) (r "1.73.0")))

