(define-module (crates-io su bp subparse) #:use-module (crates-io))

(define-public crate-subparse-0.1.0 (c (n "subparse") (v "0.1.0") (d (list (d (n "combine") (r "~2.3.1") (d #t) (k 0)) (d (n "error-chain") (r "~0.10.0") (d #t) (k 0)))) (h "0w3jxjb8vbqqpdh4fj2gpnkq4igwz9m0pywzlf51vyr6r1076pgi")))

(define-public crate-subparse-0.2.0 (c (n "subparse") (v "0.2.0") (d (list (d (n "combine") (r "~2.3.1") (d #t) (k 0)) (d (n "error-chain") (r "~0.10.0") (d #t) (k 0)) (d (n "itertools") (r "^0.5.9") (d #t) (k 0)) (d (n "vobsub") (r "^0.2.0") (d #t) (k 0)))) (h "16ii409h6ik6i8adw2qn3iw02af6f2da8ymi5s38ksqdla50qvc9")))

(define-public crate-subparse-0.3.0 (c (n "subparse") (v "0.3.0") (d (list (d (n "combine") (r "^2.5.1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "itertools") (r "^0.6.2") (d #t) (k 0)) (d (n "vobsub") (r "^0.2.0") (d #t) (k 0)))) (h "0i9hsc37nh4j9i94kaq2y2jjgqc0w5dvq4dj361qqp7iczv5sbph")))

(define-public crate-subparse-0.4.0 (c (n "subparse") (v "0.4.0") (d (list (d (n "combine") (r "^2.5.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "itertools") (r "^0.6.2") (d #t) (k 0)) (d (n "vobsub") (r "^0.2.0") (d #t) (k 0)))) (h "0xnhmck57dnz7qgqqdy5jaxkjikza791pzzwbw2yr4jksjhhcxsp")))

(define-public crate-subparse-0.5.0 (c (n "subparse") (v "0.5.0") (d (list (d (n "combine") (r "^2.5.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "itertools") (r "^0.6.2") (d #t) (k 0)) (d (n "vobsub") (r "^0.2.0") (d #t) (k 0)))) (h "1hwwp1vpi571d3dprvb8y19zg4v8acy87jvkv01ygzpd0wmc1n52")))

(define-public crate-subparse-0.6.0 (c (n "subparse") (v "0.6.0") (d (list (d (n "combine") (r "^2.5.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "itertools") (r "^0.6.2") (d #t) (k 0)) (d (n "vobsub") (r "^0.2.0") (d #t) (k 0)))) (h "1lqshjwfahryl4ikgrnwh4jmzs0ja0n2a22ra0g9i1dh3mxmg9r4")))

(define-public crate-subparse-0.6.1 (c (n "subparse") (v "0.6.1") (d (list (d (n "combine") (r "^2.5.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "itertools") (r "^0.6.2") (d #t) (k 0)) (d (n "vobsub") (r "^0.2.0") (d #t) (k 0)))) (h "11qnzw1kqfqh2qaivkj4xc2ghma6f7a4vpwzm3qak4hna96dxbrw")))

(define-public crate-subparse-0.6.2 (c (n "subparse") (v "0.6.2") (d (list (d (n "combine") (r "^2.5.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "vobsub") (r "^0.2.0") (d #t) (k 0)))) (h "0f803n1x989ypma40jklxdlbascmi46smbljx2q0185cdxg9mpw5")))

(define-public crate-subparse-0.7.0 (c (n "subparse") (v "0.7.0") (d (list (d (n "chardet") (r "^0.2.4") (d #t) (k 0)) (d (n "combine") (r "^2.5.1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "vobsub") (r "^0.2.3") (d #t) (k 0)))) (h "1ar9gjgpnw3y2489fl3i6pnafiyad44kg1hw6v6i6wn90ia0x3m0")))

