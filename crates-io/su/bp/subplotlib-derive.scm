(define-module (crates-io su bp subplotlib-derive) #:use-module (crates-io))

(define-public crate-subplotlib-derive-0.1.0 (c (n "subplotlib-derive") (v "0.1.0") (d (list (d (n "fehler") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "08fzls7sn9a8xxph8g7nf90qld0xhcdbk6kdfbh3rh31cn077zn9")))

(define-public crate-subplotlib-derive-0.4.0 (c (n "subplotlib-derive") (v "0.4.0") (d (list (d (n "fehler") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "10yba5v560jhr0a259arqqgw8d6syv4m702ahiybkyj33p568lnv")))

(define-public crate-subplotlib-derive-0.4.2 (c (n "subplotlib-derive") (v "0.4.2") (d (list (d (n "fehler") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0g39jydjf480faa8lfdlwvh7p9wjfwnhxi1k0g7m2r4bkjzd0zka")))

(define-public crate-subplotlib-derive-0.4.3 (c (n "subplotlib-derive") (v "0.4.3") (d (list (d (n "fehler") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1q0v3nvqvl6qlgp4wcwvdjq74hy2663h6amdj2wi7spn13jh7zyx")))

(define-public crate-subplotlib-derive-0.5.0 (c (n "subplotlib-derive") (v "0.5.0") (d (list (d (n "fehler") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "059x5bfmm67mhs3pppsmp155q3f0a9x0bfxcdr9vpm7lgs3p8v7d")))

(define-public crate-subplotlib-derive-0.6.0 (c (n "subplotlib-derive") (v "0.6.0") (d (list (d (n "fehler") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "07chfbi1ic6nmhg7ad7xv09r8c58r12y3189qvr6n0khdznsl4hc")))

(define-public crate-subplotlib-derive-0.7.0 (c (n "subplotlib-derive") (v "0.7.0") (d (list (d (n "fehler") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0hz6n26q23zlbwjvzh4rvhggns4viq20svkbh5rrsalmyqldnrrh") (r "1.63")))

(define-public crate-subplotlib-derive-0.7.1 (c (n "subplotlib-derive") (v "0.7.1") (d (list (d (n "fehler") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "11x93kzj6x5x44irkbiz1vyns9ivr7skixyd318sxa021xas16ci") (r "1.63")))

(define-public crate-subplotlib-derive-0.8.0 (c (n "subplotlib-derive") (v "0.8.0") (d (list (d (n "fehler") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1xa8x1rnvsp5hmb4a9vs3wdr05lx1gbkn2c1cyq1rkhg8ajg57ms") (r "1.63")))

(define-public crate-subplotlib-derive-0.9.0 (c (n "subplotlib-derive") (v "0.9.0") (d (list (d (n "fehler") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0q5wfgjb6fvkmpk56l76jx11qiva9zvbjc0jr0y4x58jm50s0d02") (r "1.63")))

