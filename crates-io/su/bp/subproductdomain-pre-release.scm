(define-module (crates-io su bp subproductdomain-pre-release) #:use-module (crates-io))

(define-public crate-subproductdomain-pre-release-0.1.0-alpha.0 (c (n "subproductdomain-pre-release") (v "0.1.0-alpha.0") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)) (d (n "ark-bls12-381") (r "^0.4") (d #t) (k 2)) (d (n "ark-ec") (r "^0.4") (d #t) (k 0)) (d (n "ark-ff") (r "^0.4") (d #t) (k 0)) (d (n "ark-poly") (r "^0.4") (d #t) (k 0)) (d (n "ark-std") (r "^0.4") (d #t) (k 0)))) (h "1sbd6m55rz1lsj00mg6s843j0h8isknpd05ydc02074dcdml8lwv")))

(define-public crate-subproductdomain-pre-release-0.1.0 (c (n "subproductdomain-pre-release") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.47") (d #t) (k 0)) (d (n "ark-bls12-381") (r "^0.4") (d #t) (k 2)) (d (n "ark-ec") (r "^0.4") (d #t) (k 0)) (d (n "ark-ff") (r "^0.4") (d #t) (k 0)) (d (n "ark-poly") (r "^0.4") (d #t) (k 0)) (d (n "ark-std") (r "^0.4") (d #t) (k 0)))) (h "1avy042j03im7wdry38img3j4rl2c23hx59fmpa9wz4vmgjc8cbk")))

