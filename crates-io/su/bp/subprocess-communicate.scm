(define-module (crates-io su bp subprocess-communicate) #:use-module (crates-io))

(define-public crate-subprocess-communicate-0.1.0 (c (n "subprocess-communicate") (v "0.1.0") (d (list (d (n "bytes") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "nix") (r "^0.5") (d #t) (k 0)) (d (n "slab") (r "^0.1") (d #t) (k 0)))) (h "0di7jgsxgxx8n7zd8y69vxaa8xsmhsg63dragnkn436vybmlqjjg")))

(define-public crate-subprocess-communicate-0.2.1 (c (n "subprocess-communicate") (v "0.2.1") (d (list (d (n "bytes") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "nix") (r "^0.5") (d #t) (k 0)) (d (n "slab") (r "^0.1") (d #t) (k 0)))) (h "0c2p6q0h4lbmavarw8agl4gdpj5dc50lsrzmxv3w2nbbn87f3r0i")))

(define-public crate-subprocess-communicate-0.2.2 (c (n "subprocess-communicate") (v "0.2.2") (d (list (d (n "mio") (r "^0.6") (d #t) (k 0)))) (h "00ylnr4qa0a5m4gjpk9pnhyl82is6p1qlmarvyszmwavdikcjyiz")))

