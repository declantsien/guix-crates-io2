(define-module (crates-io su bp subplot-build) #:use-module (crates-io))

(define-public crate-subplot-build-0.1.0 (c (n "subplot-build") (v "0.1.0") (d (list (d (n "subplot") (r "^0.3.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "10zj472lv6a7k4kajbzx3wbqhif8wyclvh95k8db8624yws0nvxb")))

(define-public crate-subplot-build-0.4.0 (c (n "subplot-build") (v "0.4.0") (d (list (d (n "subplot") (r "^0.4.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "06xm5qm119l3wag8cbybss686qbnzks0cw00kj8srbx65vlaw17f")))

(define-public crate-subplot-build-0.4.2 (c (n "subplot-build") (v "0.4.2") (d (list (d (n "subplot") (r "^0.4.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0iap13d6fzjrhm6rvr7ryhiyv6zspk02r79cycwk3am2pkq8rxcc")))

(define-public crate-subplot-build-0.4.3 (c (n "subplot-build") (v "0.4.3") (d (list (d (n "subplot") (r "^0.4.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "18lcsfk8vj97q83prxrzi26m1gg19blmiplgvglvly5slm4hyyxh")))

(define-public crate-subplot-build-0.5.0 (c (n "subplot-build") (v "0.5.0") (d (list (d (n "subplot") (r "^0.5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1ywf2vxj1yq1w9i69wqsgg758m9g81jp5dihy7xy43ifl22h6pn6")))

(define-public crate-subplot-build-0.6.0 (c (n "subplot-build") (v "0.6.0") (d (list (d (n "subplot") (r "^0.6.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0ji7iipcd6r8ail5la8m56cdwff45s23p3ylqqlr4axp6a8c1hql")))

(define-public crate-subplot-build-0.7.0 (c (n "subplot-build") (v "0.7.0") (d (list (d (n "subplot") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "01lqz5b8jyqapi6274qv4fxkackb2i3nlwm57k9j05b6amcljpxd") (r "1.63")))

(define-public crate-subplot-build-0.7.1 (c (n "subplot-build") (v "0.7.1") (d (list (d (n "subplot") (r "^0.7.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1p38kq7fmg6gpwqmkymn68f2qiaw7k35s7i4k0ifs6g5c0d70xfw") (r "1.63")))

(define-public crate-subplot-build-0.8.0 (c (n "subplot-build") (v "0.8.0") (d (list (d (n "subplot") (r "^0.8.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0fm0sn2a6z47xd9c9kh28mrrn9y23lgyygpbkbv8az5g73qm2wlh") (r "1.63")))

(define-public crate-subplot-build-0.9.0 (c (n "subplot-build") (v "0.9.0") (d (list (d (n "subplot") (r "^0.9.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "1gps315x5mfa57hi1hwbn2jyw9ijybvdkp5wsmg5lgp84dqzyn6d") (r "1.63")))

