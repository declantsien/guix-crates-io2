(define-module (crates-io su cr sucrose-macros) #:use-module (crates-io))

(define-public crate-sucrose-macros-1.0.0 (c (n "sucrose-macros") (v "1.0.0") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "1vaydawhmiqzaqpj3rnw9x715sz7mv8l4f9hksslfa38plm1fcwy")))

(define-public crate-sucrose-macros-1.0.1 (c (n "sucrose-macros") (v "1.0.1") (d (list (d (n "darling") (r "^0.20.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0i7jkwq9f60asm3931qk1zdcp6gmzadp87x9p3i01c3js5pxdf6f")))

