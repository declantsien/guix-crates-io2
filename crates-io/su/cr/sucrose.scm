(define-module (crates-io su cr sucrose) #:use-module (crates-io))

(define-public crate-sucrose-1.0.0 (c (n "sucrose") (v "1.0.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (o #t) (d #t) (k 0)) (d (n "sucrose-macros") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0hjajas8lppvgmmczwbfqlwbk8v0j65i3c255pmi7pdfip2pbfil")))

(define-public crate-sucrose-1.0.1 (c (n "sucrose") (v "1.0.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (o #t) (d #t) (k 0)) (d (n "sucrose-macros") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0w1i00i61w5nlkqmsbb4axkma7fqpm89hbzw07dpm513zcz1qnca")))

(define-public crate-sucrose-1.1.0 (c (n "sucrose") (v "1.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (o #t) (d #t) (k 0)) (d (n "sucrose-macros") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "0a3ikx1cpb1j1xihk4df6cj7gvw3hi5qmlql9s3vz18y6lr4jh89")))

(define-public crate-sucrose-1.1.1 (c (n "sucrose") (v "1.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.31") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (o #t) (d #t) (k 0)) (d (n "sucrose-macros") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)))) (h "11r1kwdwrc1599digp8h965bblmxpv0jam824vdq2i35v5ppz2nc")))

