(define-module (crates-io su m_ sum_error) #:use-module (crates-io))

(define-public crate-sum_error-0.1.0 (c (n "sum_error") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "05gz47qq5vq0nkninzrfg0qbsfnkshmzarjw6143dx140y8j4fn0")))

(define-public crate-sum_error-0.1.1 (c (n "sum_error") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "0i1wprnj69mvmgg1ql6glimbjw25kw0pn001x15pkbmgdih7i5r3") (y #t)))

(define-public crate-sum_error-0.1.2 (c (n "sum_error") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (d #t) (k 0)))) (h "0wiidrxrrdzxcyrxwrpj9mifgnbbrn03yc3gv1zdxlzy93hxi7ih")))

