(define-module (crates-io su m_ sum_type) #:use-module (crates-io))

(define-public crate-sum_type-0.1.0 (c (n "sum_type") (v "0.1.0") (h "08svsa4prf7gl3bshz3xryy22h2jp8iygx2hjx8yfp4gyz4al8ar") (f (quote (("try_from") ("generated_example") ("default"))))))

(define-public crate-sum_type-0.1.1 (c (n "sum_type") (v "0.1.1") (h "1yln7bwv3whk785l9hbw9f56m1dibykblifirjly3rnghsnz1jlz") (f (quote (("try_from") ("generated_example") ("default"))))))

(define-public crate-sum_type-0.2.0 (c (n "sum_type") (v "0.2.0") (h "05ba7qldsrl0g60bcfm2pgl0iqi7crvpqnj43s4qwz1wkw64lnys") (f (quote (("generated_example") ("default"))))))

