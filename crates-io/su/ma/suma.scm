(define-module (crates-io su ma suma) #:use-module (crates-io))

(define-public crate-suma-0.0.1 (c (n "suma") (v "0.0.1") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.0") (d #t) (k 0)) (d (n "subgraph-matching") (r "^0.0.2") (d #t) (k 0)))) (h "1aycdgrr9knpsf56zwg1rdxfgx65831flhwkq2fj9bzjrzpjb0bp")))

(define-public crate-suma-0.0.2 (c (n "suma") (v "0.0.2") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.0") (d #t) (k 0)) (d (n "subgraph-matching") (r "^0.0.3") (d #t) (k 0)))) (h "11zhczij51lf90rwfs1rcs5fmv00rj2a5vlmy3n2ac5342mjkypr")))

(define-public crate-suma-0.0.3 (c (n "suma") (v "0.0.3") (d (list (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.0") (d #t) (k 0)) (d (n "subgraph-matching") (r "^0.0.4") (d #t) (k 0)))) (h "1f2vd62i3pb2mpyghph72k7yzmbwia9n2ssrf7g64s7iki27yd07")))

