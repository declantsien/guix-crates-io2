(define-module (crates-io su si susie) #:use-module (crates-io))

(define-public crate-susie-0.1.0 (c (n "susie") (v "0.1.0") (d (list (d (n "cranelift") (r "^0.50.0") (d #t) (k 0)) (d (n "scryer-prolog") (r "^0.8.115") (d #t) (k 0)))) (h "1k775v7lws2ggbcqc9k3jcx554jslrd7jq0bfxcvsdfjqvq9nirv")))

