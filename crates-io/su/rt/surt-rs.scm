(define-module (crates-io su rt surt-rs) #:use-module (crates-io))

(define-public crate-surt-rs-0.1.0 (c (n "surt-rs") (v "0.1.0") (d (list (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "01n85y2rdc5g4cf6pyyifr46rq6x40yg9bcz8qdaylaz8dhxqbiy")))

(define-public crate-surt-rs-0.1.1 (c (n "surt-rs") (v "0.1.1") (d (list (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 2)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "url-escape") (r "^0.1.1") (d #t) (k 0)))) (h "1xrcg3piqc5z0l1x7315b7i3qdvqfi2hhq205hhlfm9lgawf1fc4")))

