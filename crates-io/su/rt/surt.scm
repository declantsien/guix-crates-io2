(define-module (crates-io su rt surt) #:use-module (crates-io))

(define-public crate-surt-0.1.0 (c (n "surt") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1wn83r10bpk45i4d1184fvmcvfhgqz45gy2vza43v44i1dmlr1ys")))

