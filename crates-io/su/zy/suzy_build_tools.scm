(define-module (crates-io su zy suzy_build_tools) #:use-module (crates-io))

(define-public crate-suzy_build_tools-0.0.1 (c (n "suzy_build_tools") (v "0.0.1") (d (list (d (n "rect_packer") (r "^0.2.1") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.1") (d #t) (k 0)))) (h "1d0prdcz72n48hpj0yagajrkr32kjylbnxbcbq7jmdnkl1ya3yp7")))

