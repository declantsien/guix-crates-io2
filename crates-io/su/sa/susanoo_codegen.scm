(define-module (crates-io su sa susanoo_codegen) #:use-module (crates-io))

(define-public crate-susanoo_codegen-0.0.3 (c (n "susanoo_codegen") (v "0.0.3") (d (list (d (n "quote") (r "~0.3") (d #t) (k 0)) (d (n "susanoo") (r "^0.0.3") (d #t) (k 0)) (d (n "syn") (r "~0.11") (d #t) (k 0)))) (h "1x61avvw9drr14jabwvyp90dz976lpklipgwfr1v335w5gkbpvb2") (y #t)))

(define-public crate-susanoo_codegen-0.0.4 (c (n "susanoo_codegen") (v "0.0.4") (d (list (d (n "quote") (r "~0.3") (d #t) (k 0)) (d (n "susanoo") (r "^0.0.4") (d #t) (k 0)) (d (n "syn") (r "~0.11") (d #t) (k 0)))) (h "0v4v43pcksjkprpvf6kjmcr677bnhflg8ac9nsi0nn7sgg6ck9hs") (y #t)))

(define-public crate-susanoo_codegen-0.0.5 (c (n "susanoo_codegen") (v "0.0.5") (d (list (d (n "quote") (r "~0.3") (d #t) (k 0)) (d (n "susanoo") (r "^0.0.5") (d #t) (k 0)) (d (n "syn") (r "~0.11") (d #t) (k 0)))) (h "0lm0chqkb636jpdf19mr71568gjcafbwzk1rz07ql987xrim4ck1") (y #t)))

(define-public crate-susanoo_codegen-0.0.6 (c (n "susanoo_codegen") (v "0.0.6") (d (list (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "susanoo") (r "^0.0.6") (d #t) (k 2)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "1giavywcgy8idc6y9lfj6yd6cyx0hdn94bn678yi6bq7yd5qn04q") (y #t)))

