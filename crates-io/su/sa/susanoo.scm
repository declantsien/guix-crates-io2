(define-module (crates-io su sa susanoo) #:use-module (crates-io))

(define-public crate-susanoo-0.0.1 (c (n "susanoo") (v "0.0.1") (d (list (d (n "error-chain") (r "~0.10") (d #t) (k 0)) (d (n "futures") (r "~0.1") (d #t) (k 0)) (d (n "hyper") (r "~0.11") (d #t) (k 0)) (d (n "regex") (r "~0.2") (d #t) (k 0)) (d (n "tokio-core") (r "~0.1") (d #t) (k 0)) (d (n "typemap") (r "~0.3") (d #t) (k 0)))) (h "1ybcznkcap71bi97jic2395vp81jqsd1qwfjhln8c3iv9b596hpq") (f (quote (("unstable")))) (y #t)))

(define-public crate-susanoo-0.0.2 (c (n "susanoo") (v "0.0.2") (d (list (d (n "error-chain") (r "~0.10") (d #t) (k 0)) (d (n "futures") (r "~0.1") (d #t) (k 0)) (d (n "hyper") (r "~0.11") (d #t) (k 0)) (d (n "regex") (r "~0.2") (d #t) (k 0)) (d (n "tokio-core") (r "~0.1") (d #t) (k 0)) (d (n "typemap") (r "~0.3") (d #t) (k 0)))) (h "13amw88554kabqzy662ykzxbx889fyx47hw5wyj72z7zpyfrdv1p") (f (quote (("unstable")))) (y #t)))

(define-public crate-susanoo-0.0.3 (c (n "susanoo") (v "0.0.3") (d (list (d (n "error-chain") (r "~0.10") (d #t) (k 0)) (d (n "futures") (r "~0.1") (d #t) (k 0)) (d (n "hyper") (r "~0.11") (d #t) (k 0)) (d (n "regex") (r "~0.2") (d #t) (k 0)) (d (n "tokio-core") (r "~0.1") (d #t) (k 0)) (d (n "tokio-proto") (r "~0.1") (d #t) (k 0)) (d (n "typemap") (r "~0.3") (d #t) (k 0)) (d (n "url") (r "~1.5") (d #t) (k 0)))) (h "16w6326scy9l9i7g3zd1bs0s6h7dyp9mrwdmcj0wnh2prdjjqjzf") (f (quote (("unstable")))) (y #t)))

(define-public crate-susanoo-0.0.4 (c (n "susanoo") (v "0.0.4") (d (list (d (n "futures") (r "~0.1") (d #t) (k 0)) (d (n "hyper") (r "~0.11") (d #t) (k 0)) (d (n "regex") (r "~0.2") (d #t) (k 0)) (d (n "tokio-core") (r "~0.1") (d #t) (k 0)) (d (n "tokio-proto") (r "~0.1") (d #t) (k 0)) (d (n "typemap") (r "~0.3") (d #t) (k 0)) (d (n "url") (r "~1.5") (d #t) (k 0)))) (h "12k42qnq2jq2h50h7cqyy933av15p6cqc3bbqa1sc64r15fjvfi4") (f (quote (("unstable")))) (y #t)))

(define-public crate-susanoo-0.0.5 (c (n "susanoo") (v "0.0.5") (d (list (d (n "futures") (r "~0.1") (d #t) (k 0)) (d (n "hyper") (r "~0.11") (d #t) (k 0)) (d (n "regex") (r "~0.2") (d #t) (k 0)) (d (n "tokio-core") (r "~0.1") (d #t) (k 0)) (d (n "tokio-proto") (r "~0.1") (d #t) (k 0)) (d (n "url") (r "~1.5") (d #t) (k 0)))) (h "02795hd807cnvj7nhn41vhpnfsmfnaxin2h1k5vby15cwpgrwvgk") (f (quote (("unstable")))) (y #t)))

(define-public crate-susanoo-0.0.6 (c (n "susanoo") (v "0.0.6") (d (list (d (n "cookie") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-proto") (r "^0.1") (d #t) (k 0)))) (h "0i3a05y1qh71g6lyxm13n2bkmh0ib7m3z8ma8wy8n0l42kpqdwpq") (f (quote (("unstable")))) (y #t)))

