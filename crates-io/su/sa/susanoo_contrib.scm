(define-module (crates-io su sa susanoo_contrib) #:use-module (crates-io))

(define-public crate-susanoo_contrib-0.0.2 (c (n "susanoo_contrib") (v "0.0.2") (d (list (d (n "r2d2") (r "~0.7") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "susanoo") (r "^0.0.2") (d #t) (k 0)) (d (n "tera") (r "~0.10") (d #t) (k 0)))) (h "0j1xjzws0ar2hh8hkcikj6pfhfl710m6wsmyxc71vj6warsjnx9m") (y #t)))

(define-public crate-susanoo_contrib-0.0.3 (c (n "susanoo_contrib") (v "0.0.3") (d (list (d (n "futures") (r "~0.1") (d #t) (k 0)) (d (n "hyper") (r "~0.11") (d #t) (k 0)) (d (n "r2d2") (r "~0.7") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "susanoo") (r "^0.0.3") (d #t) (k 0)) (d (n "tera") (r "~0.10") (d #t) (k 0)))) (h "03i849b6cbvc49w9ygf5ncsal4069f6c7ld7w361ayc9qzzm8vgp") (y #t)))

(define-public crate-susanoo_contrib-0.0.4 (c (n "susanoo_contrib") (v "0.0.4") (d (list (d (n "futures") (r "~0.1") (d #t) (k 0)) (d (n "hyper") (r "~0.11") (d #t) (k 0)) (d (n "r2d2") (r "~0.7") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "susanoo") (r "^0.0.4") (d #t) (k 0)) (d (n "tera") (r "~0.10") (d #t) (k 0)))) (h "1pw9wc72wf528c8zhmhakrb5aj98890wrabnb0kb60skahkzz6hc") (y #t)))

(define-public crate-susanoo_contrib-0.0.5 (c (n "susanoo_contrib") (v "0.0.5") (d (list (d (n "futures") (r "~0.1") (d #t) (k 0)) (d (n "hyper") (r "~0.11") (d #t) (k 0)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "susanoo") (r "^0.0.5") (d #t) (k 0)) (d (n "tera") (r "~0.10") (d #t) (k 0)))) (h "1rss1ykxdbm9fwqarc6cyg2qdlsyafbbjw6adgnw7h3ci6gwfcdp") (y #t)))

(define-public crate-susanoo_contrib-0.0.6 (c (n "susanoo_contrib") (v "0.0.6") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "susanoo") (r "^0.0.6") (d #t) (k 0)) (d (n "tera") (r "^0.11") (d #t) (k 0)) (d (n "url") (r "^1.6") (d #t) (k 0)))) (h "1l56m848vqap616jims106ivpj92gslin7gr8phc2pd33j406cvy") (y #t)))

