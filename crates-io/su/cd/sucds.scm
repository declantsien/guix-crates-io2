(define-module (crates-io su cd sucds) #:use-module (crates-io))

(define-public crate-sucds-0.1.0 (c (n "sucds") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0g3nlhn6zkkikjcfg4nrly9g9hcqdj17j36zjjhicyx0qzaqg4yj")))

(define-public crate-sucds-0.1.1 (c (n "sucds") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l2km9d2gl3j68ddwkascsc65khgw55rljmlg9q8bc65wmv84nqh")))

(define-public crate-sucds-0.1.2 (c (n "sucds") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l6wj03ba85wihp6zx0j3k6r9f21hv0rjwl910frydzwrlcksjg7")))

(define-public crate-sucds-0.1.3 (c (n "sucds") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1skvwi327pi9v4ybrbkx97b15wqhvnjp41agxkjcwbpbaybmx3wx") (f (quote (("intrinsics") ("default"))))))

(define-public crate-sucds-0.1.4 (c (n "sucds") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "05cypb7yrdm3m2w02bj07rg77r512bjwwrspzbi1vda3xa6jrl66") (f (quote (("intrinsics") ("default"))))))

(define-public crate-sucds-0.1.5 (c (n "sucds") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lb1nz36m6fw83lxkgqmyqpw4jh73f1yqsfa519dsfzcxqv4npm5") (f (quote (("intrinsics") ("default"))))))

(define-public crate-sucds-0.1.6 (c (n "sucds") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "07gfq0r6kn01xdm0vhjwn3wp41kdlz41zy1jyr00imj2l8pfykqx") (f (quote (("intrinsics") ("default"))))))

(define-public crate-sucds-0.1.7 (c (n "sucds") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14b8apqv9xc4x50kcqinviayca4xff8v4rc0as6890jgfjn6s0l7") (f (quote (("intrinsics") ("default"))))))

(define-public crate-sucds-0.1.8 (c (n "sucds") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "1734g4rp9pnzrbwysjncdassrb0f85fzds0nnb77g0vna34lvzj1") (f (quote (("intrinsics") ("default"))))))

(define-public crate-sucds-0.2.0 (c (n "sucds") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "06mhw0lafmlz487858qknwvfh0pcz0wx4654fhmdl27wyjdmwiil") (f (quote (("intrinsics") ("default"))))))

(define-public crate-sucds-0.2.1 (c (n "sucds") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "1413aavxmvzfb6mfyvikc1m423prn22vq3f7qj9qx0nxllmgdq95") (f (quote (("intrinsics") ("default"))))))

(define-public crate-sucds-0.2.2 (c (n "sucds") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "1vcsrpz17mrp8zrncpyzlz17hl7jimykfxckp3fdws054fy5hhb0") (f (quote (("intrinsics") ("default"))))))

(define-public crate-sucds-0.2.3 (c (n "sucds") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "1biqgnm4x6azfwwxdjvi4xbkdlmrjjlss0syj84m3pgk01z0nayv") (f (quote (("intrinsics") ("default"))))))

(define-public crate-sucds-0.2.4 (c (n "sucds") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "055f55y0g1b288r822hh818gnzy719by277m00blgr2sff10f7dy") (f (quote (("intrinsics") ("default"))))))

(define-public crate-sucds-0.2.5 (c (n "sucds") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "1v4lm7nqhwdmhr5c9ynkbvl12xw382ig67n8j04cn163dkw3b9my") (f (quote (("intrinsics") ("default"))))))

(define-public crate-sucds-0.3.0 (c (n "sucds") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "0br3znnh2s4nxr7lz7rdx5j9yx8dmq7x91ilk6bhnij3xax9bn34") (f (quote (("intrinsics") ("default"))))))

(define-public crate-sucds-0.3.1 (c (n "sucds") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "1vp0a175r513kdzcysvvh6hd3fd9rk8scghzi2nchlidrrlixdvp") (f (quote (("std") ("intrinsics") ("default" "std"))))))

(define-public crate-sucds-0.4.0 (c (n "sucds") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "1v43h579x4ab8d5pz6gi4d70ilvv3556mnq76yd4inwarjdac0k4") (f (quote (("std") ("intrinsics") ("default" "std"))))))

(define-public crate-sucds-0.5.0 (c (n "sucds") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "0i2lijckjxqkhcf2nqa3q8d4kv0p6ppjbszralilv8vi8j0py73c") (f (quote (("std") ("intrinsics") ("default" "std"))))))

(define-public crate-sucds-0.6.0 (c (n "sucds") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 2)))) (h "0cwm0mly6nwpbaivvq8n0s0zgks6h5c1vic3mmkyzyqx2hhcvb34") (f (quote (("std") ("intrinsics") ("default" "std"))))))

(define-public crate-sucds-0.7.0 (c (n "sucds") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "=0.2.15") (d #t) (k 0)))) (h "0q1r4qlzrl76c0scvx221c7dabd4irjzpgm5jdbr6qzgv9hn504b") (f (quote (("std") ("intrinsics") ("default" "std")))) (r "1.61.0")))

(define-public crate-sucds-0.8.0 (c (n "sucds") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1n2q97jmfyh4svg6fiq2lwv5da973knjvv50ibi837s3chq47cjs") (f (quote (("std") ("intrinsics") ("default" "std")))) (r "1.61.0")))

(define-public crate-sucds-0.8.1 (c (n "sucds") (v "0.8.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1mndcjl9qlln8pf6n8j9iwd6jn8dvhraam2cm4idhvpy58c4cgfm") (f (quote (("std") ("intrinsics") ("default" "std")))) (r "1.61.0")))

