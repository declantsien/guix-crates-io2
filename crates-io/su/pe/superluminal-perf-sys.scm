(define-module (crates-io su pe superluminal-perf-sys) #:use-module (crates-io))

(define-public crate-superluminal-perf-sys-0.1.0 (c (n "superluminal-perf-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0l5gps9gfsgbvjkwiw6ywrlkrvijhjszijy92ss0326j92cwiy90")))

(define-public crate-superluminal-perf-sys-0.1.1 (c (n "superluminal-perf-sys") (v "0.1.1") (h "1qqfn5gzs9jlczaz7pcn69i37fg1h4gy091s1br4q3gd1hscg1ck")))

(define-public crate-superluminal-perf-sys-0.1.2 (c (n "superluminal-perf-sys") (v "0.1.2") (h "05pz0yybf4y2iw3rvqf2crk04zv7610jjm3glhi8hlv2rhms0hh3")))

(define-public crate-superluminal-perf-sys-0.3.0 (c (n "superluminal-perf-sys") (v "0.3.0") (h "0v3pcs9nb5nhwdvfb5l3akjj50b4yhl0gyycsjs3mn61c31m5sxj")))

