(define-module (crates-io su pe superviseur-provider) #:use-module (crates-io))

(define-public crate-superviseur-provider-0.1.0 (c (n "superviseur-provider") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "hcl-rs") (r "^0.12.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "superviseur-types") (r "^0.1.0") (d #t) (k 0)))) (h "0qxmrv7mwkqlywnlh9c2kyv9y0nllhxcmkqa5zdgc6yxg4b9kcv6")))

