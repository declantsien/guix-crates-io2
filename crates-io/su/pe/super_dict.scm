(define-module (crates-io su pe super_dict) #:use-module (crates-io))

(define-public crate-super_dict-0.1.0 (c (n "super_dict") (v "0.1.0") (h "15sz20wr2vfm3djc5biwxg84w9rb71si00srmiqclvni6v8d1lw4")))

(define-public crate-super_dict-0.1.1 (c (n "super_dict") (v "0.1.1") (h "00j120c4wiqmln2g7kcx6m750wmhb01cvzsa7is8b7c3z6j21a0j")))

