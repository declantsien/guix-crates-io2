(define-module (crates-io su pe supermodel) #:use-module (crates-io))

(define-public crate-supermodel-0.1.0 (c (n "supermodel") (v "0.1.0") (d (list (d (n "dotenvy") (r "^0.15.7") (d #t) (k 2)) (d (n "supermodel-macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1z554fig4dnc9y63gqmlbvcmf429ba4a51qn0vyv5f4jq2752qpl") (f (quote (("macros" "supermodel-macros"))))))

