(define-module (crates-io su pe supermemo2) #:use-module (crates-io))

(define-public crate-supermemo2-0.1.0 (c (n "supermemo2") (v "0.1.0") (h "133zacpjaijbx4z68iysvl8if3z951nkvmklwiiay8ps6njbdv59")))

(define-public crate-supermemo2-0.1.1 (c (n "supermemo2") (v "0.1.1") (h "0kbf5pll3x25kwvm2jk0ikb4iai2qnrs6d7z8nfllddgdmiaylh6")))

(define-public crate-supermemo2-0.2.0 (c (n "supermemo2") (v "0.2.0") (h "1f1n7ccjy45124b3r017hh81iriam02qrzcxlw73cf33d0sy7bgk")))

(define-public crate-supermemo2-1.0.0 (c (n "supermemo2") (v "1.0.0") (h "1s595hf3qc5br34f8j89y4nhxz0b64p6761kq8fakd1yr5z7yyxw")))

(define-public crate-supermemo2-1.0.1 (c (n "supermemo2") (v "1.0.1") (h "0cmcw0wjhqz5ia9dvp0ln1b643zwqli2h7kmnqqcjpmi7nq4xazh")))

(define-public crate-supermemo2-1.0.2 (c (n "supermemo2") (v "1.0.2") (h "0wqwz9q88mjs3j34dz6l4vf2zckvx1cl23g1khzvcnvrqmz3xq9n")))

(define-public crate-supermemo2-1.0.3 (c (n "supermemo2") (v "1.0.3") (h "1df02p8q570c5n0famk4qk5hgxl0cppmvphfh0lkfhk68pm98jrs")))

