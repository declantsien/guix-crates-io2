(define-module (crates-io su pe superbitty-macros) #:use-module (crates-io))

(define-public crate-superbitty-macros-0.1.0 (c (n "superbitty-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "1r42xc1pzpyc3dd305dn000vcx1dq4817r4h3lb3pl988habl984") (y #t)))

(define-public crate-superbitty-macros-0.2.0 (c (n "superbitty-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "1c2j6h2nw3jr6sd06p72s7ns23c1fyj1ix32774a14ai2hh9pavi")))

(define-public crate-superbitty-macros-0.3.0 (c (n "superbitty-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "1ywiz16fmqzq6njm8sdlhbgkhichf53c1ca9nm9g3awrbm9d3azi")))

(define-public crate-superbitty-macros-0.3.1 (c (n "superbitty-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (d #t) (k 0)))) (h "18v8hr78vkpz7vrql6dbia5pbzh88n7v2ifv3mpxcl5pdbfjf8d4")))

