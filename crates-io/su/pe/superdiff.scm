(define-module (crates-io su pe superdiff) #:use-module (crates-io))

(define-public crate-superdiff-2.0.1 (c (n "superdiff") (v "2.0.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08fpblkb569rydz7l21gwwps1mf973ccbg36ndjcci59i2qnagd7")))

(define-public crate-superdiff-2.0.2 (c (n "superdiff") (v "2.0.2") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16bm11gbqxrz64lmjd9lwb31ygzsivhvm1dpkbramg5wplxz0r2p")))

(define-public crate-superdiff-2.0.3 (c (n "superdiff") (v "2.0.3") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18x6s1xm0lis7z2kqz2d400m7adyra1c4wr1hpv5hxcwfpls7w0i")))

(define-public crate-superdiff-2.1.0 (c (n "superdiff") (v "2.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ag8j0gxnafzi3g8am4v2vka1dng12fl9p3kqgzl6j98fbxckjf7")))

(define-public crate-superdiff-2.1.1 (c (n "superdiff") (v "2.1.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11hm5kk4b88zl273ag38z4d4vb16k73ipn3avqmy62bdlx25nyzy")))

(define-public crate-superdiff-2.1.2 (c (n "superdiff") (v "2.1.2") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0q3adai3iikl82in79aaqc5msfhqr5p28yzn75qzr0b6q0g96wcc")))

(define-public crate-superdiff-2.2.0 (c (n "superdiff") (v "2.2.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04yw8pwvd4ynl83kax08zqldi5q698fcfqrnvsbhdfyf4674sh8p")))

