(define-module (crates-io su pe superviseur-code) #:use-module (crates-io))

(define-public crate-superviseur-code-0.1.0 (c (n "superviseur-code") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "tokio") (r "^1.24.2") (f (quote ("tokio-macros" "macros" "rt" "rt-multi-thread" "sync"))) (d #t) (k 0)))) (h "1cib1axlzyf4xmz0rl9ldzghy1xkkyna51cxnj2rq3jck9lli25g")))

