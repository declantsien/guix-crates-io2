(define-module (crates-io su pe supertrait-macros) #:use-module (crates-io))

(define-public crate-supertrait-macros-0.1.0 (c (n "supertrait-macros") (v "0.1.0") (d (list (d (n "macro_magic") (r "^0.4") (f (quote ("proc_support"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "proc-utils") (r "^0") (o #t) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit" "visit-mut"))) (d #t) (k 0)))) (h "00wz9jlg0fqhlvd7pzgcaalz7gf4kk7aam170yzwwy9z2zw6ryrh") (f (quote (("default")))) (s 2) (e (quote (("debug" "dep:proc-utils"))))))

