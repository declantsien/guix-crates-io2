(define-module (crates-io su pe superman) #:use-module (crates-io))

(define-public crate-superman-0.1.0 (c (n "superman") (v "0.1.0") (d (list (d (n "tokio") (r "^1.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)) (d (n "whoami") (r "^1.3") (d #t) (k 0)))) (h "145hql55gf77frqr5ck17pf585qqcjgjgj5k2ghxfnb1j3r1zz61")))

(define-public crate-superman-0.1.1 (c (n "superman") (v "0.1.1") (d (list (d (n "tokio") (r "^1.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)) (d (n "whoami") (r "^1.3") (d #t) (k 0)))) (h "1v1gjjvfk2hwk0gxw7r87pkx8c8h0gfswxlsnr92jc5cm50h76md")))

(define-public crate-superman-0.1.2 (c (n "superman") (v "0.1.2") (d (list (d (n "tokio") (r "^1.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)) (d (n "whoami") (r "^1.3") (d #t) (k 0)))) (h "0jkk6cwx5kprakfbmza5wfmj17q944klkynazjrm9rpm6rsk2gks")))

(define-public crate-superman-0.1.3 (c (n "superman") (v "0.1.3") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)) (d (n "whoami") (r "^1.3") (d #t) (k 0)))) (h "1zjzncx0p18rywh8pfhnv7kq0d581psdy9arf8yal1ywnwgvfpzd")))

(define-public crate-superman-0.1.4 (c (n "superman") (v "0.1.4") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)) (d (n "whoami") (r "^1.3") (d #t) (k 0)))) (h "14bs75g9yswvk2s48f78wvd8hf9vvhmy17ynr6cjsn4ldliic0h6")))

