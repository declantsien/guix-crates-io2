(define-module (crates-io su pe superf4-rust) #:use-module (crates-io))

(define-public crate-superf4-rust-0.1.0 (c (n "superf4-rust") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.13") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi" "tlhelp32" "winnt" "handleapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1zxfsis04h2iwrl8x6sxb5b6bn0ybjrai4rqipaqsxqax9bmjdaq")))

