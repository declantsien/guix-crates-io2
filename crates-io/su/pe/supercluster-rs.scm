(define-module (crates-io su pe supercluster-rs) #:use-module (crates-io))

(define-public crate-supercluster-rs-0.1.0 (c (n "supercluster-rs") (v "0.1.0") (d (list (d (n "geo-index") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1zda2myrzvyrx4lqp245xdiy2lamdxz2cilxv22i4svybr8n5zwn")))

