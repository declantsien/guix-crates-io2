(define-module (crates-io su pe supergitignore) #:use-module (crates-io))

(define-public crate-supergitignore-0.1.0 (c (n "supergitignore") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)))) (h "0an3hingxpa8l2fiblja6nb9qnbsvafmpyj6ic3pjcmxh4b9angl") (y #t)))

(define-public crate-supergitignore-0.2.0 (c (n "supergitignore") (v "0.2.0") (d (list (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)))) (h "0rb7lm1nlw9ppx2c1zkhkwfgcwi9c3b7p6591bwjz7cripmfjvp7") (y #t)))

(define-public crate-supergitignore-1.2.0 (c (n "supergitignore") (v "1.2.0") (d (list (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)))) (h "1zlncbbxvqxllyncbmra0fcw9yahbc8jlalhb51kwqcslf0fcb53") (y #t)))

(define-public crate-supergitignore-1.3.0 (c (n "supergitignore") (v "1.3.0") (d (list (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)))) (h "0hi0x53lhgharxjzjbzx7wps13is5npr8yzbirk6jg2760lsg0fx") (y #t)))

(define-public crate-supergitignore-1.3.1 (c (n "supergitignore") (v "1.3.1") (d (list (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)))) (h "062df7wrdyy8dikvpz80f103cla2xwzi27vwr8qmpz182a21m5d8") (y #t)))

(define-public crate-supergitignore-2.0.0 (c (n "supergitignore") (v "2.0.0") (d (list (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)))) (h "1qb1fmvz93q2z1j6pjmagxcriax7yw59py3q3nwms9kn9sdcz0gh") (y #t)))

(define-public crate-supergitignore-2.1.0 (c (n "supergitignore") (v "2.1.0") (d (list (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)))) (h "1cjlzpqjsxvmm7vkg9cvq20x35mpbwaad8x4fxybpngkykj7mbzb") (y #t)))

(define-public crate-supergitignore-2.1.1 (c (n "supergitignore") (v "2.1.1") (d (list (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)))) (h "09h46jpa2iawwlhi29sf34ci1wgc6gyg4snfpygl3yl1msy9rhaz") (y #t)))

(define-public crate-supergitignore-3.0.0 (c (n "supergitignore") (v "3.0.0") (d (list (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)))) (h "0hdw4ycfc25c76kkac3w136krqah5hm1wcbjvbvh5vm37n3mcamw")))

