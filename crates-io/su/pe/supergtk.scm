(define-module (crates-io su pe supergtk) #:use-module (crates-io))

(define-public crate-supergtk-0.1.0 (c (n "supergtk") (v "0.1.0") (d (list (d (n "glib") (r "^0.7.1") (d #t) (k 0)) (d (n "gtk") (r "^0.6.0") (d #t) (k 0)))) (h "16yx05j3ipkh9cfrlh0pf2pp7zvp4f8p1lyrdwfp4nlsvf1nlmxg")))

(define-public crate-supergtk-0.1.1 (c (n "supergtk") (v "0.1.1") (d (list (d (n "glib") (r "^0.7.1") (d #t) (k 0)) (d (n "gtk") (r "^0.6.0") (d #t) (k 0)))) (h "1x5602krhkqzd6mpk6sfylpb2iz678imj8sh39v23lsbaqsjn4p1")))

(define-public crate-supergtk-0.1.2 (c (n "supergtk") (v "0.1.2") (d (list (d (n "glib") (r "^0.7.1") (d #t) (k 0)) (d (n "gtk") (r "^0.6.0") (d #t) (k 0)))) (h "0kbrqgn55c0fn609nhcdhmfsbx4k2x0nighiadrgnclkwlrixw48")))

(define-public crate-supergtk-0.1.3 (c (n "supergtk") (v "0.1.3") (d (list (d (n "glib") (r "^0.7.1") (d #t) (k 0)) (d (n "gtk") (r "^0.6.0") (d #t) (k 0)))) (h "082cw8fyiggn9pk0mqiribbd11fhyh7cjzwkljdpib32xwcyaapw")))

