(define-module (crates-io su pe superflake) #:use-module (crates-io))

(define-public crate-superflake-0.1.0 (c (n "superflake") (v "0.1.0") (h "1g1hvmq8pgq9samvxwr39663xk6mhkqmc7nxi55z1164vzv54xdq")))

(define-public crate-superflake-0.2.0 (c (n "superflake") (v "0.2.0") (h "1jknqy1jppy8hiwv5qc0jxa45qwnypv9d7qqnbxdznih8nfgmjy6")))

