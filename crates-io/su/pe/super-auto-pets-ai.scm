(define-module (crates-io su pe super-auto-pets-ai) #:use-module (crates-io))

(define-public crate-super-auto-pets-ai-0.1.0 (c (n "super-auto-pets-ai") (v "0.1.0") (h "190s8ijs7j6gzvhjv22fc4hp78cgbmiklmzzjqclhppg0q197z1y")))

(define-public crate-super-auto-pets-ai-0.1.1 (c (n "super-auto-pets-ai") (v "0.1.1") (h "0dhbs0nb8n5cs7m6hhyl185rwvmpfp0wck2z5v134d70hi4ar32d")))

(define-public crate-super-auto-pets-ai-0.1.2 (c (n "super-auto-pets-ai") (v "0.1.2") (h "09d0ama9l78jx7i93d87n37lf8rx02km7ivd8b587w0v7179dk90")))

