(define-module (crates-io su pe superslice) #:use-module (crates-io))

(define-public crate-superslice-0.1.0 (c (n "superslice") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0c3bm7p663p2ydgj3awd8psylr80sb1dbzgacbygbq3h4ga162xm")))

(define-public crate-superslice-1.0.0 (c (n "superslice") (v "1.0.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "07z84bqs2kncgchrv1pafwlsqzbjm3ckw7nq5z44d2mx9pcww5mb")))

