(define-module (crates-io su pe supervisely-format) #:use-module (crates-io))

(define-public crate-supervisely-format-0.1.0 (c (n "supervisely-format") (v "0.1.0") (d (list (d (n "hex_color") (r "^1.0.0") (d #t) (k 0)) (d (n "noisy_float") (r "^0.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "palette") (r "^0.6.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.6") (d #t) (k 0)))) (h "0mii8vvan29sdkbd6095dwasa7w6pdg8kmg08521a8bwva8qf2fr")))

