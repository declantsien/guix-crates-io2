(define-module (crates-io su pe supersize) #:use-module (crates-io))

(define-public crate-supersize-0.1.0 (c (n "supersize") (v "0.1.0") (d (list (d (n "async-compression") (r "^0.3.14") (f (quote ("brotli" "tokio" "gzip"))) (d #t) (k 0)) (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "humansize") (r "^1.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("io-util" "macros" "rt" "fs"))) (d #t) (k 0)) (d (n "wax") (r "^0.5.0") (d #t) (k 0)))) (h "1gnypxvr6ns4xdbmpzrf8b6qdx8kjnc1r1qgrinv7b40f6dyd0dn")))

