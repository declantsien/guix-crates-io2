(define-module (crates-io su pe superviseur-types) #:use-module (crates-io))

(define-public crate-superviseur-types-0.1.0 (c (n "superviseur-types") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "hcl-rs") (r "^0.12.3") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "tabled") (r "^0.10.0") (d #t) (k 0)) (d (n "tantivy") (r "^0.19.2") (d #t) (k 0)))) (h "07ifrwgsf1js9q715d5754s04ahwl198x8wli8y4rbaia3qksn6i")))

