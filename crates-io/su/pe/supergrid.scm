(define-module (crates-io su pe supergrid) #:use-module (crates-io))

(define-public crate-supergrid-0.1.0 (c (n "supergrid") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.3") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "1g4cxlbqjk5k1444s8zzmyyfzfa9dc7za3kgvzxlg9kw0vksp23j")))

(define-public crate-supergrid-0.2.0 (c (n "supergrid") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7.3") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "0kj9q0k1j3rw54an1qsz55jq0n2426s4mk79cfzbjfffd4ahmw48")))

(define-public crate-supergrid-0.2.1 (c (n "supergrid") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.7.3") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "0l299pwvhxs8nrcp3h4yc7rd9hy82f1yhl2haj793whxd1inh618")))

(define-public crate-supergrid-0.2.2 (c (n "supergrid") (v "0.2.2") (d (list (d (n "arrayvec") (r "^0.7.3") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "01zmqqmrzcx04ic9zibdfwc5m5lmzg6h9b69xbirzpxixj4dx6yx")))

(define-public crate-supergrid-0.2.3 (c (n "supergrid") (v "0.2.3") (d (list (d (n "arrayvec") (r "^0.7.3") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "0lfx9xhl585hrvlm8hggr1s6j5gkf8q8vsn74v0c9azfmphh6fr7")))

(define-public crate-supergrid-0.2.4 (c (n "supergrid") (v "0.2.4") (d (list (d (n "arrayvec") (r "^0.7.3") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "14w90ciw80h56g3b4jk0i8lff9lwm3jk4k47gpd76ccps7mh9yxy")))

(define-public crate-supergrid-0.2.5 (c (n "supergrid") (v "0.2.5") (d (list (d (n "arrayvec") (r "^0.7.3") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "0ymvxd1y360hvhkxly9334vwil62k2v77m85spl2a86mik5mclhv")))

(define-public crate-supergrid-0.2.6 (c (n "supergrid") (v "0.2.6") (d (list (d (n "arrayvec") (r "^0.7.3") (d #t) (k 0)) (d (n "num-format") (r "^0.4.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "structopt") (r "^0.3.26") (d #t) (k 2)))) (h "1gll6kgl483g2a3qzgj3i8613b9q9vfdpxwqi4cph7zqjp92348n")))

