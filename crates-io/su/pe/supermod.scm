(define-module (crates-io su pe supermod) #:use-module (crates-io))

(define-public crate-supermod-0.1.0 (c (n "supermod") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "proc-quote") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "091shiz449nrcpsskawgk239q3vn7s2siwnswifi7adpn92nkaw5")))

(define-public crate-supermod-0.2.0 (c (n "supermod") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "proc-quote") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0b4h4zijhb377l36lhdmvpikjp72934j99vzazmmqbnqxnq0522g")))

