(define-module (crates-io su pe supernotes-client) #:use-module (crates-io))

(define-public crate-supernotes-client-0.1.0 (c (n "supernotes-client") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "markdown") (r "^0.3") (d #t) (k 0)) (d (n "mockito") (r "^0.29") (d #t) (k 2)) (d (n "oauth2") (r "^4.0.0-alpha.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "026ddb1pydf45la2sq5jqk8bmkqm1x0vawcrph6ciz12i2b6qxwj")))

