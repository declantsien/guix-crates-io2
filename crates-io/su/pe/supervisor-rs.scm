(define-module (crates-io su pe supervisor-rs) #:use-module (crates-io))

(define-public crate-supervisor-rs-0.5.6 (c (n "supervisor-rs") (v "0.5.6") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1wchx8bjhzcnjirzzyn5wxzd7gbxwwhsjj1rairc8hpr11xa1zqc")))

(define-public crate-supervisor-rs-0.5.7 (c (n "supervisor-rs") (v "0.5.7") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1xzshv7k8ha331y0wg6kisy1pd7lpa9r5filvawzrjbwp6bmy6rn")))

(define-public crate-supervisor-rs-0.5.8 (c (n "supervisor-rs") (v "0.5.8") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1drrdzwiys2i20mpjppx2l12srfrj5dgm0ala78pf7m5f7bq6rf9")))

(define-public crate-supervisor-rs-0.5.9 (c (n "supervisor-rs") (v "0.5.9") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0xcvlm8ivq33p5w60xcdj15c50bs78ab9hni4ll6xim5mj2cmcc0")))

(define-public crate-supervisor-rs-0.5.10 (c (n "supervisor-rs") (v "0.5.10") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1s85h73gakb2h485p6rmzmak4y8w5j4gpgx65knsny8wi4zdvjld")))

(define-public crate-supervisor-rs-0.6.0 (c (n "supervisor-rs") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "openssl") (r "^0.10.26") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0x97s39yb16s68fd0i5kp8wz3b5ryi9d4dbyglgll1bakhmgaic9")))

(define-public crate-supervisor-rs-0.6.1 (c (n "supervisor-rs") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "openssl") (r "^0.10.26") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1npwj1rwpgzvp4vbwsa17wd220kkjnhmxl2g9rvqss5ld30cx220")))

(define-public crate-supervisor-rs-0.6.2 (c (n "supervisor-rs") (v "0.6.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "openssl") (r "^0.10.26") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "137f8akjr4p5ysxv6aphqc0x9j96s59p0bj761pak4wvxhaqi9y3")))

(define-public crate-supervisor-rs-0.6.3 (c (n "supervisor-rs") (v "0.6.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "openssl") (r "^0.10.26") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "039dgqf0vbxgah32aj8rq61v4ky4lpyahszmx3by86fs1xih8pnf")))

(define-public crate-supervisor-rs-0.7.0 (c (n "supervisor-rs") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "openssl") (r "^0.10.26") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "08d0s7ff56xmii077nk3plj76y5546hbk03zrzsxxr12vnx9zbf9")))

(define-public crate-supervisor-rs-0.7.1 (c (n "supervisor-rs") (v "0.7.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "openssl") (r "^0.10.26") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0kx3gxr0gd73dv22y7krl9sbhz2yvgj64p9gsdymx7c46s9pksig")))

(define-public crate-supervisor-rs-0.8.0 (c (n "supervisor-rs") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "openssl") (r "^0.10.26") (d #t) (k 0)) (d (n "ssh2") (r "^0.9") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "19nd946zwpd7wihsm7hby56pxffhvnfjiv9rjvdydnv8pq9504p2")))

(define-public crate-supervisor-rs-0.8.1 (c (n "supervisor-rs") (v "0.8.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "openssl") (r "^0.10.26") (d #t) (k 0)) (d (n "ssh2") (r "^0.9") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0wa6wqiwv297dqhf615rgk62bfhvqja288aa2c0cb9fzll59n769")))

(define-public crate-supervisor-rs-0.8.2 (c (n "supervisor-rs") (v "0.8.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "openssl") (r "^0.10.26") (d #t) (k 0)) (d (n "ssh2") (r "^0.9") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1841qdr7cllp23l55w2jnb6n7nppmjsydrh8ldhlm1gfyrvmzv91")))

(define-public crate-supervisor-rs-0.8.3 (c (n "supervisor-rs") (v "0.8.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "ssh2") (r "^0.9") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1wznymy4zlixkxvn1rmrhlxwcyis2v4dj72h94pqqal35h7d9j4b")))

(define-public crate-supervisor-rs-0.8.4 (c (n "supervisor-rs") (v "0.8.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "ssh2") (r "^0.9") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "00ip61agnplmqjdlr8wj2a8yzx2lvv4ag89xmvrkfrfabpky7akm")))

(define-public crate-supervisor-rs-0.8.5 (c (n "supervisor-rs") (v "0.8.5") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "openssl") (r "^0.10") (f (quote ("vendored"))) (d #t) (k 0)) (d (n "ssh2") (r "^0.9") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0k2j6hxcanca6i16d7nfh88d8923ckdh3nmp31xd04x3dq2b0h8c")))

