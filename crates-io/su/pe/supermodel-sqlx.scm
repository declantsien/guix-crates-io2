(define-module (crates-io su pe supermodel-sqlx) #:use-module (crates-io))

(define-public crate-supermodel-sqlx-0.1.0 (c (n "supermodel-sqlx") (v "0.1.0") (d (list (d (n "sqlx") (r "^0.7.3") (k 0)) (d (n "supermodel") (r "^0.1.0") (d #t) (k 0)))) (h "1kakml4h82rjzinbdd81gvc558xbqgh38jj6h1h0s0dkjr0lrpaw") (f (quote (("tls-rustls" "sqlx/tls-rustls") ("tls-native-tls" "sqlx/tls-native-tls") ("sqlite" "sqlx/sqlite") ("runtime-tokio" "sqlx/runtime-tokio") ("runtime-async-std" "sqlx/runtime-async-std") ("postgres" "sqlx/postgres") ("mysql" "sqlx/mysql") ("any" "sqlx/any"))))))

