(define-module (crates-io su pe superbitty) #:use-module (crates-io))

(define-public crate-superbitty-0.1.0 (c (n "superbitty") (v "0.1.0") (d (list (d (n "rustversion") (r "^1.0.7") (d #t) (k 2)) (d (n "superbitty-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "1n7ylw4ga9xaz33lq3c3cxb7hv49w2m36bnjgj4l9jfhww4xpvly") (y #t)))

(define-public crate-superbitty-0.1.1 (c (n "superbitty") (v "0.1.1") (d (list (d (n "rustversion") (r "^1.0.7") (d #t) (k 2)) (d (n "superbitty-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "0mcqnb0bxw4d9b2qgb76i9iql7vk6r5vzksbhwmmcswyv6rcpddr") (y #t)))

(define-public crate-superbitty-0.1.2 (c (n "superbitty") (v "0.1.2") (d (list (d (n "rustversion") (r "^1.0.7") (d #t) (k 2)) (d (n "superbitty-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "0xcs0rvi42a3s0hz2bxhnw818vmbmm08a857v4s8j4nxn2gffcfg") (y #t)))

(define-public crate-superbitty-0.2.0 (c (n "superbitty") (v "0.2.0") (d (list (d (n "rustversion") (r "^1.0.7") (d #t) (k 2)) (d (n "superbitty-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "12q7pjiscny93w2zxms05vw85h1rjwd4i864vzfmdm7y2r94wvl1")))

(define-public crate-superbitty-0.2.1 (c (n "superbitty") (v "0.2.1") (d (list (d (n "rustversion") (r "^1.0.7") (d #t) (k 2)) (d (n "superbitty-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "0ccnglvp9hlys0ncax97blzapzh0dwfprxda1pas6kxmcdya0lp2")))

(define-public crate-superbitty-0.3.0 (c (n "superbitty") (v "0.3.0") (d (list (d (n "rustversion") (r "^1.0.7") (d #t) (k 2)) (d (n "superbitty-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "00jg1aayjh2rshy55by2f0q0nhw2ap43m6dlh7v00dblj2zil2pr")))

(define-public crate-superbitty-0.3.1 (c (n "superbitty") (v "0.3.1") (d (list (d (n "rustversion") (r "^1.0.7") (d #t) (k 2)) (d (n "superbitty-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "18nn7hadp0n6g2kkqffvsrm5nv317qx23hj7cgr1sr7h9541x8fp")))

(define-public crate-superbitty-0.3.2 (c (n "superbitty") (v "0.3.2") (d (list (d (n "rustversion") (r "^1.0.7") (d #t) (k 2)) (d (n "superbitty-macros") (r "^0.3.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.63") (d #t) (k 2)))) (h "0fzw8wnsscvbhbnp8f4vf4kq5m6gkrd11qc1p37zgkgpcmh0z673")))

