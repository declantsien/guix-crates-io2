(define-module (crates-io su pe super_mass) #:use-module (crates-io))

(define-public crate-super_mass-0.1.0 (c (n "super_mass") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "jemallocator") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "rustfft") (r "^4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "139qmclq5dx42vkyn9sd133if8bnkz66lwjas3vkrdlgsa8cv4ds") (f (quote (("pseudo_distance") ("jemalloc" "jemallocator") ("default" "pseudo_distance" "auto") ("auto"))))))

