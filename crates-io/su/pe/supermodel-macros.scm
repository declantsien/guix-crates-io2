(define-module (crates-io su pe supermodel-macros) #:use-module (crates-io))

(define-public crate-supermodel-macros-0.1.0 (c (n "supermodel-macros") (v "0.1.0") (d (list (d (n "case") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0386v80v7k8m32jfkzqkwqp14r58lz9ngc63iypbf0zxs61yglql")))

