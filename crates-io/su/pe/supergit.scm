(define-module (crates-io su pe supergit) #:use-module (crates-io))

(define-public crate-supergit-0.1.0 (c (n "supergit") (v "0.1.0") (d (list (d (n "async-std") (r "^1.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "atomptr") (r "^1.0") (d #t) (k 0)) (d (n "git2") (r "^0.11") (d #t) (k 0)))) (h "0ljjw0pfrw206vba5b4km4nxnvciwg7zpl9fw01yvacdgrgrrwdm")))

(define-public crate-supergit-0.2.0 (c (n "supergit") (v "0.2.0") (d (list (d (n "async-std") (r "^1.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "atomptr") (r "^1.0") (d #t) (k 0)) (d (n "git2") (r "^0.11") (d #t) (k 0)))) (h "1nnzh098r6wa7vs66dabi80ldb71bb74vywwlaqdj7agnwh0jm0d")))

(define-public crate-supergit-0.2.1 (c (n "supergit") (v "0.2.1") (d (list (d (n "atomptr") (r "^1.0") (d #t) (k 0)) (d (n "git2") (r "^0.11") (d #t) (k 0)))) (h "0gl59cnih0j6qca44dyxmsgkjk1dnvryrh6ndbs1j984klc5a1fl")))

