(define-module (crates-io su pe superluminal-perf) #:use-module (crates-io))

(define-public crate-superluminal-perf-0.1.0 (c (n "superluminal-perf") (v "0.1.0") (d (list (d (n "superluminal-perf-sys") (r "^0.1.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0yyxahn1cqgym70vbj6105g424n5rj6jrhkpcsb1376xw244p49k") (f (quote (("enable") ("default" "enable"))))))

(define-public crate-superluminal-perf-0.1.1 (c (n "superluminal-perf") (v "0.1.1") (d (list (d (n "superluminal-perf-sys") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)))) (h "0q0ykfn04i2qg5zfizp75y4dn2klpvhb6xfwlygq8jiabpgqvvc0") (f (quote (("enable") ("default" "enable"))))))

(define-public crate-superluminal-perf-0.2.0 (c (n "superluminal-perf") (v "0.2.0") (d (list (d (n "superluminal-perf-sys") (r "^0.1.1") (d #t) (t "cfg(windows)") (k 0)))) (h "0jg0xwdl4vm05zp0a0gyh7qx0jbz940qc4rvii105g6wyixd4085") (f (quote (("enable") ("default" "enable"))))))

(define-public crate-superluminal-perf-0.3.0 (c (n "superluminal-perf") (v "0.3.0") (d (list (d (n "superluminal-perf-sys") (r "^0.3.0") (d #t) (t "cfg(windows)") (k 0)))) (h "1i5qqi5p4v74hb75rp0463fqz8ldrz8arzmxp08qrpn4qlk3rkmf") (f (quote (("enable") ("default" "enable"))))))

