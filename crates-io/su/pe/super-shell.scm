(define-module (crates-io su pe super-shell) #:use-module (crates-io))

(define-public crate-super-shell-1.0.1 (c (n "super-shell") (v "1.0.1") (h "16mxwd8gi5js3z8vdrvagldi7fj3ignn4gxc4ndwnnrpgsi3j0q9")))

(define-public crate-super-shell-1.0.2 (c (n "super-shell") (v "1.0.2") (h "184xl567fqqvsgk6vwkfjdbkadljbfqm3d8mr4ggzi9flf9vp2fq")))

(define-public crate-super-shell-1.0.3 (c (n "super-shell") (v "1.0.3") (h "1ii5n3j810snqzdjkzf68q06m6f36gm04q6vzfnf2ib3285s60q3")))

(define-public crate-super-shell-1.1.0 (c (n "super-shell") (v "1.1.0") (h "0kpjwhdkid2s6dd7xjwfal0i8fax45n784jv6g5sshi3kpwmw19s")))

(define-public crate-super-shell-1.1.1 (c (n "super-shell") (v "1.1.1") (h "0x0mqxiy8m5ywsl8yp4hlc61xlwj007qn5dxcirzld0b64bllm5h")))

(define-public crate-super-shell-1.2.0 (c (n "super-shell") (v "1.2.0") (h "14zpl8pv5fr0ihmmn0qidxy0f1dqjhad8s7m2rh239fqci3knaga")))

(define-public crate-super-shell-1.2.1 (c (n "super-shell") (v "1.2.1") (h "1q85gplp3zwcqhns21ndnw3r9qi4lzylrzkxr6qks7ys1l3a591w")))

(define-public crate-super-shell-1.2.2 (c (n "super-shell") (v "1.2.2") (h "0m146ibfhdbvdrpdwpanqnpqk4zvixcjvljmwzq50j4kijkarjys")))

(define-public crate-super-shell-1.2.3 (c (n "super-shell") (v "1.2.3") (h "1lkrg4c4qkasc0s0d0mdydxbi5gk1vsjzq21x82as90hran2s4dz")))

(define-public crate-super-shell-1.2.4 (c (n "super-shell") (v "1.2.4") (h "1g03wqzl95ggrkibf23bckly200bypy1bv7sfgsk1vags0bmpgs2")))

