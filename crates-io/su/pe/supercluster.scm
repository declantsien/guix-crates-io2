(define-module (crates-io su pe supercluster) #:use-module (crates-io))

(define-public crate-supercluster-1.0.0 (c (n "supercluster") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "1brwah9i8jr3sw6cqrrsf3g0bhkj9lqm2fvyz4g8facz4p2qm6m4")))

(define-public crate-supercluster-1.0.1 (c (n "supercluster") (v "1.0.1") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "092qlq60yj77an9jrsww1j8iiybn48xjyg5ianz45baccrwnga3x")))

(define-public crate-supercluster-1.0.2 (c (n "supercluster") (v "1.0.2") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "13hnz8fdsjk9zc1yak9m43pmr41cgaf4g7gv36ncp1dcgspspfjx")))

(define-public crate-supercluster-1.0.3 (c (n "supercluster") (v "1.0.3") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "12f4nn32r4b0mhkn8l738dkxpbd3rmf2m3mr3caii3dz4fqcdw2c")))

(define-public crate-supercluster-1.0.4 (c (n "supercluster") (v "1.0.4") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "1n9wga11ipn6svgw9z99ww2aq2pim3fz9191qz0pvc62z5v1i6pm")))

(define-public crate-supercluster-1.0.5 (c (n "supercluster") (v "1.0.5") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "000611ppm5vifw180ygvd0v2b8llb3gljrg00r0y50yh90sc8g7b")))

(define-public crate-supercluster-1.0.6 (c (n "supercluster") (v "1.0.6") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "0wcjf075c24jcwhzcwvvaslak014p4b65c072xc5pwba4skldqnp")))

(define-public crate-supercluster-1.0.7 (c (n "supercluster") (v "1.0.7") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "016z1zq4dxcjwjkfgmiyjqv8ba1ir1mxnwrqdywx5wahichf0fd3")))

(define-public crate-supercluster-1.0.8 (c (n "supercluster") (v "1.0.8") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "1lck286ni3a8a9s7w4jpf4g5vs01p9j8wx1r467ar2fnbim840g8")))

(define-public crate-supercluster-1.0.10 (c (n "supercluster") (v "1.0.10") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "0m17js95shb3dj1jfhp179qm83zbxrxxfxwf0hg700b7zkg80fx9")))

(define-public crate-supercluster-1.0.11 (c (n "supercluster") (v "1.0.11") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "0cmrljm307fncyvvavzcpzdrq69w3b9xnx6kyxjg5kq4cnmf01kb")))

(define-public crate-supercluster-1.0.12 (c (n "supercluster") (v "1.0.12") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "0dlw8r1a73imq9mz0amg246dzhbhimrqmibaqa5dhq6a1kcf9851")))

(define-public crate-supercluster-1.0.13 (c (n "supercluster") (v "1.0.13") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "1200w047bvqqdzz69022afafx4ny1c01qk2nbddpszvdfbiqnfaz")))

(define-public crate-supercluster-1.0.14 (c (n "supercluster") (v "1.0.14") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "012llqjaqdh9z7lgaijqblmz0lihws1yxlby7mmbgllgixam6mld")))

(define-public crate-supercluster-1.0.15 (c (n "supercluster") (v "1.0.15") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 2)))) (h "0k4x4ygl6a73vz6bi2x43iz241h1w8bxnrfknbs034y3k9qi72ya")))

(define-public crate-supercluster-1.0.16 (c (n "supercluster") (v "1.0.16") (d (list (d (n "geojson") (r "^0.24.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "161ngv6cfijragmf1cbzr5sjkdi6jhl3kg8arjdrbgxiq3yzini7")))

