(define-module (crates-io su pe super-ttt) #:use-module (crates-io))

(define-public crate-super-ttt-0.1.0 (c (n "super-ttt") (v "0.1.0") (h "06vjn6zgfa8gmfnb9gl6z5l3mwpymsxxrhngzxljhg5yi4g6nhgi")))

(define-public crate-super-ttt-0.1.1 (c (n "super-ttt") (v "0.1.1") (h "0jq064h28cg2gzwi84091cky9wmikl025xvflqncx3q24gwvwky1")))

(define-public crate-super-ttt-0.1.2 (c (n "super-ttt") (v "0.1.2") (h "13j8n39nm8mcing9wmip4lq0mr0kc1w5q6z1wgpfaxxm5zhigqmf")))

(define-public crate-super-ttt-0.2.0 (c (n "super-ttt") (v "0.2.0") (h "088sx4k4fha1y28161jn0j8wn74yqkp01nc33r6bgjypjkgvvjww")))

