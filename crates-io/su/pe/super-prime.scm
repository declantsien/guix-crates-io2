(define-module (crates-io su pe super-prime) #:use-module (crates-io))

(define-public crate-super-prime-0.0.0 (c (n "super-prime") (v "0.0.0") (d (list (d (n "latexify") (r "^0.0.1") (d #t) (k 0)) (d (n "num-primes") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "135rcw0y06grb37fdwr7q2sj16krnhax2i4dlifdypi7wvzy9z20") (f (quote (("default"))))))

(define-public crate-super-prime-0.1.0 (c (n "super-prime") (v "0.1.0") (d (list (d (n "num-primes") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0vfidqvljphjmh4ic3mkpq0bap7wpvxiwp2zrlp0gdx6dl5gf2nw") (f (quote (("default"))))))

