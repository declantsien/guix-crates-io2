(define-module (crates-io su pe superset_map) #:use-module (crates-io))

(define-public crate-superset_map-0.1.0 (c (n "superset_map") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.4.4") (k 2)) (d (n "zfc") (r "^0.1.0") (k 0)))) (h "196w69dwb11vni13m2a14gi998js95k9pa46apa0lpy7x6v3ba25") (r "1.69")))

(define-public crate-superset_map-0.1.1 (c (n "superset_map") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.4.4") (k 2)) (d (n "zfc") (r "^0.1.1") (k 0)))) (h "1pf1bpbdaxml5w03c4v35bvkm6r7cbrms9i0k4rllqfrdcjd4047") (r "1.69")))

(define-public crate-superset_map-0.2.0 (c (n "superset_map") (v "0.2.0") (d (list (d (n "num-bigint") (r "^0.4.4") (k 2)) (d (n "zfc") (r "^0.3.0") (k 0)))) (h "125q3mz307myxa50hrzy8jkbqz4v8c40hg961y87146y8b33mqiq") (r "1.69")))

(define-public crate-superset_map-0.2.1 (c (n "superset_map") (v "0.2.1") (d (list (d (n "num-bigint") (r "^0.4.4") (k 2)) (d (n "zfc") (r "^0.3.1") (k 0)))) (h "1cr1zm5njp71v327n5r8m8hmz3bfymvxa3i0yr7ffkh1pnraij9n")))

(define-public crate-superset_map-0.2.2 (c (n "superset_map") (v "0.2.2") (d (list (d (n "num-bigint") (r "^0.4.4") (k 2)) (d (n "zfc") (r "^0.3.1") (k 0)))) (h "1jm5a1xqj7638vfp23x9668ynd17gd35blhc2mql66qil6znb09r")))

(define-public crate-superset_map-0.2.3 (c (n "superset_map") (v "0.2.3") (d (list (d (n "num-bigint") (r "^0.4.4") (k 2)) (d (n "zfc") (r "^0.3.2") (k 0)))) (h "1n8x8fmql5g3a6a8q0zz74an5liqr8fj6bbf43ryqvhdw8ralnlk")))

