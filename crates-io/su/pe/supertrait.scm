(define-module (crates-io su pe supertrait) #:use-module (crates-io))

(define-public crate-supertrait-0.0.1 (c (n "supertrait") (v "0.0.1") (h "09r54cysdbhjxzj1764c1y67ywjfd2gr8h7jalqrzbxsw7yz8n5y")))

(define-public crate-supertrait-0.1.0 (c (n "supertrait") (v "0.1.0") (d (list (d (n "macro_magic") (r "^0.4") (d #t) (k 0)) (d (n "supertrait-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1s9w2wz747wa9f60v3pw218a96im7sk2mwknb7r8h7dgr1m925vv") (f (quote (("default") ("debug" "supertrait-macros/debug"))))))

