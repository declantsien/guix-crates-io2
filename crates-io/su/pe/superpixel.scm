(define-module (crates-io su pe superpixel) #:use-module (crates-io))

(define-public crate-superpixel-0.1.0 (c (n "superpixel") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "18p7vp08sf6xgsggfgc1wdcx4mi1mgxk8l50fc649b494fpvg0vx") (f (quote (("build-binary" "docopt"))))))

(define-public crate-superpixel-0.1.1 (c (n "superpixel") (v "0.1.1") (d (list (d (n "docopt") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "06zqfjbvxjljcl83pkhaa4pqrhrkhwrczmbszh073r487pgaqgff") (f (quote (("build-binary" "docopt"))))))

