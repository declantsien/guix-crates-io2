(define-module (crates-io su pe super-units) #:use-module (crates-io))

(define-public crate-super-units-1.0.0 (c (n "super-units") (v "1.0.0") (h "0n9flqh3rnpl6ilsjs5jlacl9nkwsgmbm0dfb0i3g3ljqb0sxipg")))

(define-public crate-super-units-1.0.1 (c (n "super-units") (v "1.0.1") (h "1pn0gfq9xhl5hr86919d0j62j5q065lags7p0rl805wpvzfscmyi")))

(define-public crate-super-units-1.0.2 (c (n "super-units") (v "1.0.2") (h "13q3whl7z4fhj5w9k0banc1rkkgr3lnygph49wfjggqbn5kghbwd")))

