(define-module (crates-io su pe superchan) #:use-module (crates-io))

(define-public crate-superchan-0.0.1 (c (n "superchan") (v "0.0.1") (h "1f3s226ra10am9jra46gbxqp8chw5fp3kzzfflzd0i4dq5v0azc5")))

(define-public crate-superchan-0.0.2 (c (n "superchan") (v "0.0.2") (h "02kk92y5wzghbb6qkgg0afygl1mgl3firl5x72f66xgm4qfnzlj0")))

(define-public crate-superchan-0.0.3 (c (n "superchan") (v "0.0.3") (h "1q4scxclyx0j1vyfv0labwq0b9p862qym7xv9b296d6k58k3jz03")))

(define-public crate-superchan-0.0.4 (c (n "superchan") (v "0.0.4") (h "1gn2y3vmh7w4mrc9xpa8715lda4qcsmaja4rbyqzsbm6pal1h4zh")))

(define-public crate-superchan-0.0.5 (c (n "superchan") (v "0.0.5") (h "12gpm0i5j1hmi5nwgxb2wi1pgj1apc02rljqnw94jh8r4f5sgffy")))

(define-public crate-superchan-0.0.6 (c (n "superchan") (v "0.0.6") (d (list (d (n "rustc-serialize") (r "^0.2.7") (d #t) (k 0)))) (h "1sliqxzbhg6s1khc099mx0vicas4px3l16xqajr5mscj0jhrvn9r")))

(define-public crate-superchan-0.0.7 (c (n "superchan") (v "0.0.7") (d (list (d (n "bincode") (r "^0.0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.2.7") (d #t) (k 0)))) (h "09xvv05w2d1ffpq98ib2lp6vlbnh65ldjk3lmdqv5xi768hvl8as")))

