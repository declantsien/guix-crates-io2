(define-module (crates-io su pe superconf) #:use-module (crates-io))

(define-public crate-superconf-0.1.0 (c (n "superconf") (v "0.1.0") (h "0wc27880pa0h71lqyv4sz4bnhml40kw56hj9lfs59z94lkb1kqik") (y #t)))

(define-public crate-superconf-0.1.1 (c (n "superconf") (v "0.1.1") (h "0v5zqgc2sipdwmgynkb39fjjlblkwp6d9v18aw2j3x44qyq9vsz9")))

(define-public crate-superconf-0.1.2 (c (n "superconf") (v "0.1.2") (h "0955qydbk8w8f0qm05z7x37s5kzrland5pnzq1dhl7f9ddnc8dqb")))

(define-public crate-superconf-0.3.0 (c (n "superconf") (v "0.3.0") (h "1vrgr783ayk9kc5vv1hwffqn4y71w6f4d79i9kj5x7xcf8hgshgn")))

(define-public crate-superconf-0.3.1 (c (n "superconf") (v "0.3.1") (h "1qvl28n9k6dknzn9s481x5r44wmdlq96a1i12694rrzdygcrg3h7")))

(define-public crate-superconf-0.3.2 (c (n "superconf") (v "0.3.2") (h "0fnawg7120dmkk9d2g9fcw61ap15nhaxhvppar3r6v38p7rzlryv")))

(define-public crate-superconf-0.4.0 (c (n "superconf") (v "0.4.0") (h "12nap24xlk7ilmyvfqf0wm0dx320mzvb4mbcpsx16q5gg4dilzm6") (y #t)))

(define-public crate-superconf-0.4.1 (c (n "superconf") (v "0.4.1") (h "0jvj8sdj8bbnmyhgjhz3ci90krcyr730z3smi94yxk5d1qmjzg3g") (y #t)))

(define-public crate-superconf-0.4.2 (c (n "superconf") (v "0.4.2") (h "0wijk39bwaq0cb021g3pp4vsn3fbki07qpssk5ba85hyh8spqjz5")))

(define-public crate-superconf-0.4.3 (c (n "superconf") (v "0.4.3") (h "05c58by3drxm9zd4rwafmndhmb9a85816lyskqkryibsk3q5vm69")))

