(define-module (crates-io su pe super-spotify-protocol) #:use-module (crates-io))

(define-public crate-super-spotify-protocol-0.0.1 (c (n "super-spotify-protocol") (v "0.0.1") (d (list (d (n "protobuf") (r "^3") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3") (d #t) (k 1)))) (h "0prfgxldz3bm65brpyac414shac1hl7gbqdxsr1438mhcxsa5wx9")))

(define-public crate-super-spotify-protocol-0.0.2 (c (n "super-spotify-protocol") (v "0.0.2") (d (list (d (n "protobuf") (r "^3") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3") (d #t) (k 1)))) (h "14fln6wyll9y8yqich1wx49byp8j6a8xyz6ic77gxp2g889ppgkm")))

