(define-module (crates-io su pe supercilex-tests) #:use-module (crates-io))

(define-public crate-supercilex-tests-0.1.0 (c (n "supercilex-tests") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("std"))) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 0)) (d (n "public-api") (r "^0.25.0") (d #t) (k 0)) (d (n "rustdoc-json") (r "^0.7.4") (d #t) (k 0)))) (h "1bkcm22g29kyf3jbh35fk9inzirgcc3w05k4rc49z2vb510c9gfv")))

(define-public crate-supercilex-tests-0.1.1 (c (n "supercilex-tests") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("std"))) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 0)) (d (n "public-api") (r "^0.25.0") (d #t) (k 0)) (d (n "rustdoc-json") (r "^0.7.4") (d #t) (k 0)))) (h "0ahimkl3an85dckcsrhq6fiynx9c1prcw85ggkfkmfh342sqhxag")))

(define-public crate-supercilex-tests-0.1.2 (c (n "supercilex-tests") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("std"))) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 0)) (d (n "public-api") (r "^0.25.0") (d #t) (k 0)) (d (n "rustdoc-json") (r "^0.7.4") (d #t) (k 0)))) (h "1ra6fqb1lm2jr2g1yqyw0ga9rb8i5kz8sn62f8nm4638vjgwl539")))

(define-public crate-supercilex-tests-0.2.0 (c (n "supercilex-tests") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("std"))) (k 0)) (d (n "clap2") (r "^4.0.32") (f (quote ("std"))) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 0)) (d (n "public-api") (r "^0.25.0") (d #t) (k 0)) (d (n "rustdoc-json") (r "^0.7.4") (d #t) (k 0)))) (h "058d5fwf3vns1r86lprcx79y16j9cw8xqivh0a0al3k39rpl882l")))

(define-public crate-supercilex-tests-0.2.1 (c (n "supercilex-tests") (v "0.2.1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("std"))) (k 0)) (d (n "clap2") (r "^4.0.32") (f (quote ("std"))) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 0)) (d (n "public-api") (r "^0.26.0") (d #t) (k 0)) (d (n "rustdoc-json") (r "^0.7.4") (d #t) (k 0)))) (h "1yndhkx0q5nryz97pqm476d8z85idnbmggsajjmrhfknyc97h5d5")))

(define-public crate-supercilex-tests-0.2.2 (c (n "supercilex-tests") (v "0.2.2") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("std"))) (k 0)) (d (n "clap2") (r "^4.0.32") (f (quote ("std"))) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 0)) (d (n "public-api") (r "^0.26.0") (d #t) (k 0)) (d (n "rustdoc-json") (r "^0.7.4") (d #t) (k 0)))) (h "0dlmiwbqpgsvnlppq8vpwyjwfyr3mq0qcpxf5wjdvdn7cvn0bfa6")))

(define-public crate-supercilex-tests-0.3.0 (c (n "supercilex-tests") (v "0.3.0") (d (list (d (n "clap2") (r "^4.0.32") (f (quote ("std"))) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 0)) (d (n "public-api") (r "^0.27.2") (d #t) (k 0)) (d (n "rustdoc-json") (r "^0.8.1") (d #t) (k 0)))) (h "1gsgghgzimpvpckxlbkbvsx2a06g460072rcj9dc4rfmdw3bsvja")))

(define-public crate-supercilex-tests-0.3.1 (c (n "supercilex-tests") (v "0.3.1") (d (list (d (n "clap2") (r "^4.0.32") (f (quote ("std"))) (k 0)) (d (n "expect-test") (r "^1.4.0") (d #t) (k 0)) (d (n "public-api") (r "^0.27.2") (d #t) (k 0)) (d (n "rustdoc-json") (r "^0.8.1") (d #t) (k 0)))) (h "0qxfacwkhs4hcl3bx08p6wvpg2f88nn538kcdz4df08hfr1556nn")))

(define-public crate-supercilex-tests-0.3.2 (c (n "supercilex-tests") (v "0.3.2") (d (list (d (n "clap2") (r "^4.1.8") (f (quote ("std"))) (k 0)) (d (n "expect-test") (r "^1.4.1") (d #t) (k 0)) (d (n "public-api") (r "^0.28.0") (d #t) (k 0)) (d (n "rustdoc-json") (r "^0.8.3") (d #t) (k 0)))) (h "1hqk33i00jjirs3fhyi4ngqjfikqwxqi4rkbqidaq2jqy5d3gvw4")))

(define-public crate-supercilex-tests-0.3.3 (c (n "supercilex-tests") (v "0.3.3") (d (list (d (n "clap_builder") (r "^4.3.9") (f (quote ("std"))) (k 0)) (d (n "expect-test") (r "^1.4.1") (d #t) (k 0)) (d (n "public-api") (r "^0.31.2") (d #t) (k 0)) (d (n "rustdoc-json") (r "^0.8.6") (d #t) (k 0)))) (h "1a3qdx7r0sqlly9xinniay0kk5wqs11krj9sa48zv5kr9y2qrv4q")))

(define-public crate-supercilex-tests-0.3.4 (c (n "supercilex-tests") (v "0.3.4") (d (list (d (n "clap_builder") (r "^4.4.2") (f (quote ("std"))) (k 0)) (d (n "expect-test") (r "^1.4.1") (d #t) (k 0)) (d (n "public-api") (r "^0.32.0") (d #t) (k 0)) (d (n "rustdoc-json") (r "^0.8.7") (d #t) (k 0)))) (h "1444dx64a6y0882z6j1qqlmflf56mlqnj1s3rk99dcb3jii22z27")))

(define-public crate-supercilex-tests-0.4.0 (c (n "supercilex-tests") (v "0.4.0") (d (list (d (n "clap_builder") (r "^4.4.8") (f (quote ("std"))) (k 0)) (d (n "expect-test") (r "^1.4.1") (d #t) (k 0)) (d (n "public-api") (r "^0.32.0") (d #t) (k 0)) (d (n "rustdoc-json") (r "^0.8.7") (d #t) (k 0)))) (h "0s3xh94lp8jbcw1vjdjlc331xykvjmcrqpwdg8w2qnk6nfyx5cv9")))

(define-public crate-supercilex-tests-0.4.1 (c (n "supercilex-tests") (v "0.4.1") (d (list (d (n "clap_builder") (r "^4.4.8") (f (quote ("std"))) (k 0)) (d (n "expect-test") (r "^1.4.1") (d #t) (k 0)) (d (n "public-api") (r "^0.32.0") (d #t) (k 0)) (d (n "rustdoc-json") (r "^0.8.7") (d #t) (k 0)))) (h "142wxpkb2b8gq8gk6hdk07dyxsi8vwjsyn1qqwzrfvkp04mks4g3")))

(define-public crate-supercilex-tests-0.4.2 (c (n "supercilex-tests") (v "0.4.2") (d (list (d (n "clap_builder") (r "^4.4.11") (f (quote ("std"))) (o #t) (k 0)) (d (n "expect-test") (r "^1.4.1") (o #t) (d #t) (k 0)) (d (n "public-api") (r "^0.33.1") (o #t) (d #t) (k 0)) (d (n "rustdoc-json") (r "^0.8.8") (o #t) (d #t) (k 0)))) (h "096b8xllvw8cq3h2ac017yfww7angzznzhjd6m0lhrwxycsxhpy6") (f (quote (("default" "api" "clap") ("clap" "clap_builder") ("api" "expect-test" "public-api" "rustdoc-json"))))))

(define-public crate-supercilex-tests-0.4.3 (c (n "supercilex-tests") (v "0.4.3") (d (list (d (n "clap_builder") (r "^4.4.11") (f (quote ("std"))) (o #t) (k 0)) (d (n "expect-test") (r "^1.4.1") (o #t) (d #t) (k 0)) (d (n "public-api") (r "^0.33.1") (o #t) (d #t) (k 0)) (d (n "rustdoc-json") (r "^0.8.8") (o #t) (d #t) (k 0)))) (h "1w16hwifdasafcrgiygj1ps8w82f205y2qa8z1w0xw2xh8ghwynd") (f (quote (("default" "api" "clap")))) (s 2) (e (quote (("clap" "dep:clap_builder") ("api" "dep:expect-test" "dep:public-api" "dep:rustdoc-json"))))))

(define-public crate-supercilex-tests-0.4.4 (c (n "supercilex-tests") (v "0.4.4") (d (list (d (n "clap_builder") (r "^4.4.11") (f (quote ("std"))) (o #t) (k 0)) (d (n "expect-test") (r "^1.4.1") (o #t) (d #t) (k 0)) (d (n "public-api") (r "^0.33.1") (o #t) (d #t) (k 0)) (d (n "rustdoc-json") (r "^0.8.8") (o #t) (d #t) (k 0)))) (h "001h19yil97xyhhjih1amvqzdqklw4c3h756pq8ysibdjkwrk1qd") (f (quote (("default" "api" "clap")))) (s 2) (e (quote (("clap" "dep:clap_builder") ("api" "dep:expect-test" "dep:public-api" "dep:rustdoc-json"))))))

