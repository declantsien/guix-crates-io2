(define-module (crates-io su pe super_world) #:use-module (crates-io))

(define-public crate-super_world-0.1.0 (c (n "super_world") (v "0.1.0") (h "1iv0m95dnrmac1kypg9rbvwgxspjgkv30xx3068w1pi5cfc8djgm")))

(define-public crate-super_world-0.1.1 (c (n "super_world") (v "0.1.1") (h "1bpimfsdqmly4vwk6spilq43fl2mv3zz4pn3lrv89k4xm4gip6wg")))

