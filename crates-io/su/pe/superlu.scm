(define-module (crates-io su pe superlu) #:use-module (crates-io))

(define-public crate-superlu-0.1.0 (c (n "superlu") (v "0.1.0") (h "0zkqfqnnwpsbxwjjzgajdhkbl1zax767nz4dpzhibkdv4qfjxha2")))

(define-public crate-superlu-0.2.0 (c (n "superlu") (v "0.2.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "matrix") (r "*") (d #t) (k 0)) (d (n "superlu-sys") (r "*") (d #t) (k 0)))) (h "0qmfnbvsxhqzh5dmc7h1hrjky8m4hqzyfz2pv2dif466r66xvxjk")))

(define-public crate-superlu-0.2.1 (c (n "superlu") (v "0.2.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "matrix") (r "*") (d #t) (k 0)) (d (n "superlu-sys") (r "*") (d #t) (k 0)))) (h "0lrxcrp5m4d2sarvzrbg151zi1jbpvdxrm1c5bc5mw0n42s6ws5r")))

(define-public crate-superlu-0.2.2 (c (n "superlu") (v "0.2.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "matrix") (r "^0.12") (d #t) (k 0)) (d (n "superlu-sys") (r "*") (d #t) (k 0)))) (h "0ylc79qpq851828anhkgayqmjq6gp56z1gk5b29zy69x0p7s4vpy")))

(define-public crate-superlu-0.2.3 (c (n "superlu") (v "0.2.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "matrix") (r "^0.13") (d #t) (k 0)) (d (n "superlu-sys") (r "*") (d #t) (k 0)))) (h "19083xvdzxbyjizyjjr49ywq354xdgzb1cfw8jjy1qmbs1r5fcr0")))

(define-public crate-superlu-0.2.4 (c (n "superlu") (v "0.2.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "matrix") (r "^0.14") (d #t) (k 0)) (d (n "superlu-sys") (r "*") (d #t) (k 0)))) (h "0fc8pc6in688z0qacl67q6djcmk3281xvmf7f003h68226mhmlps")))

(define-public crate-superlu-0.2.5 (c (n "superlu") (v "0.2.5") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "matrix") (r "^0.15") (d #t) (k 0)) (d (n "superlu-sys") (r "*") (d #t) (k 0)))) (h "0fi04nznylrbcyczz0a8hmf0s0i1n4l1fhzhv4jybdkhxn0fkaql")))

(define-public crate-superlu-0.2.6 (c (n "superlu") (v "0.2.6") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "matrix") (r "^0.17") (k 0)) (d (n "superlu-sys") (r "*") (d #t) (k 0)))) (h "1k2n0d0qpac20z9czs6gilb0ijfa0liln3kxvdb6fjn9q8s39cbf")))

(define-public crate-superlu-0.2.7 (c (n "superlu") (v "0.2.7") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "matrix") (r "^0.18") (k 0)) (d (n "superlu-sys") (r "*") (d #t) (k 0)))) (h "124xdbxcipxcwsgmf8392s61cz9n0dhjnmbj1bbpcr85xfbpdm0w")))

(define-public crate-superlu-0.2.8 (c (n "superlu") (v "0.2.8") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "matrix") (r "^0.19") (k 0)) (d (n "superlu-sys") (r "*") (d #t) (k 0)))) (h "1hrsjfj8hb09bna26825vn95jwj99y0g79gc1ajmzwwc7i8anfrq")))

(define-public crate-superlu-0.2.9 (c (n "superlu") (v "0.2.9") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "matrix") (r "^0.19") (k 0)) (d (n "superlu-sys") (r "*") (d #t) (k 0)))) (h "1g9izw4vx93yjlhz9ly8271h0m8hyih02ml06g93srn866k57pz2")))

(define-public crate-superlu-0.2.10 (c (n "superlu") (v "0.2.10") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "matrix") (r "^0.20") (k 0)) (d (n "superlu-sys") (r "*") (d #t) (k 0)))) (h "0xn1kf5bb6mv3g76hzxmsljrlqn5f5bhqp51xzrlj2x4as1jnpmq")))

(define-public crate-superlu-0.2.11 (c (n "superlu") (v "0.2.11") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "matrix") (r "^0.20") (k 0)) (d (n "superlu-sys") (r "^0.2") (d #t) (k 0)))) (h "0hgq4c5gi93hzi6432qa0r1d5vkkrhfzscf7xl0g750s7fprayh1")))

(define-public crate-superlu-0.2.12 (c (n "superlu") (v "0.2.12") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "matrix") (r "^0.21") (k 0)) (d (n "superlu-sys") (r "^0.2") (d #t) (k 0)))) (h "027hphxhrkspyplq4mhabzj1kp3hpm0lq7sbv7pilv9zzsjcgjch")))

(define-public crate-superlu-0.3.0 (c (n "superlu") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "matrix") (r "^0.21") (k 0)) (d (n "superlu-sys") (r "^0.3") (d #t) (k 0)))) (h "1i17aka1p4ia9bzc6188p6846blljszam3z459b8kpw9qawvnjb2")))

(define-public crate-superlu-0.3.1 (c (n "superlu") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "matrix") (r "^0.21") (k 0)) (d (n "superlu-sys") (r "^0.3") (d #t) (k 0)))) (h "10aqj7rli8rjdpc5lzj6dzhmsmzx9yc4s71lv94zlk54dwfkbamv")))

(define-public crate-superlu-0.3.2 (c (n "superlu") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "matrix") (r "^0.21") (k 0)) (d (n "superlu-sys") (r "^0.3") (d #t) (k 0)))) (h "0k3l9bqih351k493nj6b43i4dwfwf96b8gq5mmpldda55mys38vn")))

