(define-module (crates-io su pe superstream) #:use-module (crates-io))

(define-public crate-superstream-0.1.0 (c (n "superstream") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.25.0") (d #t) (k 0)))) (h "0mps1cldzzgrab9svfqwl5gd2pbj6njycifbpc0ssjx6g08p3l4h") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-superstream-0.2.0 (c (n "superstream") (v "0.2.0") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.25.0") (d #t) (k 0)))) (h "1mii2mp94a671rx3bmqyd0dz650mvrsqpvxfgwk653mkz8aq75v8") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-superstream-0.3.0 (c (n "superstream") (v "0.3.0") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.28.0") (d #t) (k 0)))) (h "1fnyjmphmzdmdh75yr01zfcf4hv5nwz7i9v1g69h6hcb55qy9qx2") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-superstream-0.3.1 (c (n "superstream") (v "0.3.1") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.28.0") (d #t) (k 0)))) (h "194ny6vslnx3lvh3z7d68a1nf3kpyxavx8p3hgpzjsjc1b834smm") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-superstream-0.3.2 (c (n "superstream") (v "0.3.2") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.28.0") (d #t) (k 0)))) (h "0bz4q4hp6qzckgyq10kq678csc5j1a5wx9hh03k14nv5spzvksv7") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-superstream-0.3.3 (c (n "superstream") (v "0.3.3") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.28.0") (d #t) (k 0)))) (h "1w8aa7lypvl2bw8w90wn2zjlx4kh1ql5v2ns3fc657dvg44cykvp") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

