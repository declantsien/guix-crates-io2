(define-module (crates-io su pe supermon) #:use-module (crates-io))

(define-public crate-supermon-0.1.0 (c (n "supermon") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.52") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a4h23fq8l2ccwqjdw50bp4d2yg2mzd3a47cx6v7vsc8vfyn8xrc")))

