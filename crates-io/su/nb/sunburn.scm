(define-module (crates-io su nb sunburn) #:use-module (crates-io))

(define-public crate-sunburn-0.0.0-reserved (c (n "sunburn") (v "0.0.0-reserved") (h "10rh6vndcqdmr9c07yz6gf8khga69hxv85jv5hwngam7rm5g8a7k")))

(define-public crate-sunburn-0.1.0 (c (n "sunburn") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "solana-bpf-loader-program") (r "~1.10.10") (d #t) (k 0)) (d (n "solana-logger") (r "~1.10.10") (d #t) (k 0)) (d (n "solana-program-test") (r "~1.10.10") (d #t) (k 0)) (d (n "solana-runtime") (r "~1.10.10") (d #t) (k 0)) (d (n "solana-sdk") (r "~1.10.10") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (d #t) (k 0)) (d (n "spl-token") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15q86dzmqmka212l5xrh4n5a0f8p8qbqb8a3sazm88hrnq2hlprm")))

(define-public crate-sunburn-0.2.0 (c (n "sunburn") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "solana-bpf-loader-program") (r "~1.10.10") (d #t) (k 0)) (d (n "solana-client") (r "~1.10.10") (d #t) (k 0)) (d (n "solana-logger") (r "~1.10.10") (d #t) (k 0)) (d (n "solana-program-test") (r "~1.10.10") (d #t) (k 0)) (d (n "solana-runtime") (r "~1.10.10") (d #t) (k 0)) (d (n "solana-sdk") (r "~1.10.10") (d #t) (k 0)) (d (n "solana-transaction-status") (r "~1.10.10") (d #t) (k 0)) (d (n "spl-associated-token-account") (r "^1.0.3") (d #t) (k 0)) (d (n "spl-token") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ncplxa457zykwywpxf38clbadb0s0yq912mxm3h2naz2dslxash")))

