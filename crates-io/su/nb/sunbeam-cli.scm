(define-module (crates-io su nb sunbeam-cli) #:use-module (crates-io))

(define-public crate-sunbeam-cli-0.0.1-alpha (c (n "sunbeam-cli") (v "0.0.1-alpha") (h "17j9pxjjm2rvxh249ffqmpx4za3bxqvs7fc1js0g2f4wgr9gq1b4")))

(define-public crate-sunbeam-cli-0.0.3-alpha (c (n "sunbeam-cli") (v "0.0.3-alpha") (h "01v5d1nhx8ayjw57qm93rfn4d9a3qf2wnc6lq9ggirlsla2canlr")))

(define-public crate-sunbeam-cli-0.0.4-alpha (c (n "sunbeam-cli") (v "0.0.4-alpha") (h "1462m9anls0k2n73qdhiabdfy7jk0bvz63m25vwkl83h3fnagidy")))

(define-public crate-sunbeam-cli-0.0.5-alpha (c (n "sunbeam-cli") (v "0.0.5-alpha") (h "1bkvlq6p5jipr3skczkmqj4dg1rb8nijjcgc8xpimxvagwic81rp")))

(define-public crate-sunbeam-cli-0.0.6-alpha (c (n "sunbeam-cli") (v "0.0.6-alpha") (h "0n548hhf3i7yi5gcvbn4i0x1z0ii9gvavs602v7q93giydy8mnd7")))

(define-public crate-sunbeam-cli-0.0.7-alpha (c (n "sunbeam-cli") (v "0.0.7-alpha") (h "07pgwimaf9pkn7r2vcsx9kpj0vdky9akvvinh6x4a2npy43gg4ry")))

(define-public crate-sunbeam-cli-0.0.8-alpha (c (n "sunbeam-cli") (v "0.0.8-alpha") (h "0n7dzqazzm3idzwijw980qqn0fa9fk3wnd46353jry5zhbn8v91b")))

(define-public crate-sunbeam-cli-0.0.9-alpha (c (n "sunbeam-cli") (v "0.0.9-alpha") (h "11zr5b496hihzrmw8ygiynp2srbykyn1y70x00zl6vj958c71gkw")))

(define-public crate-sunbeam-cli-0.0.10-alpha (c (n "sunbeam-cli") (v "0.0.10-alpha") (h "1158la24nx7lj85fskj99wsmbib3sdviw455qhjg7wbh9n8f3bgj")))

(define-public crate-sunbeam-cli-0.0.11-alpha (c (n "sunbeam-cli") (v "0.0.11-alpha") (h "1r494gj03p4g93s5av23iv6m7hhz8y77vjb0qwzkzgcxxsyng75g")))

(define-public crate-sunbeam-cli-0.0.12-alpha (c (n "sunbeam-cli") (v "0.0.12-alpha") (h "08hcz7dfbqflv3gav8ynxp47gsqbgppwdany6nd8dkm3hi7iig0r")))

(define-public crate-sunbeam-cli-0.0.13-alpha (c (n "sunbeam-cli") (v "0.0.13-alpha") (h "0d84p7dqy3awwfy862x5k6g52kr7p23yd0ssk0qpnafpdx30p3hw")))

(define-public crate-sunbeam-cli-0.0.14-alpha (c (n "sunbeam-cli") (v "0.0.14-alpha") (h "1nlifcri74fgyb1w6svvim8123vj7nw9sminqzgskzgiqpkw17s4")))

(define-public crate-sunbeam-cli-0.0.15-alpha (c (n "sunbeam-cli") (v "0.0.15-alpha") (h "15hj22q23bjvb9wl91w0y6jp1xhq0a0jjkydshdv463gjqvlmcsa")))

(define-public crate-sunbeam-cli-0.0.16-alpha (c (n "sunbeam-cli") (v "0.0.16-alpha") (h "1pnhglm7qmk1121rm30lixyd4dr2a214i707p63ap6srhl6ckcs1")))

(define-public crate-sunbeam-cli-0.0.17-alpha (c (n "sunbeam-cli") (v "0.0.17-alpha") (h "04mpnqfcpdf1ssx9ck4qb8kzchlfj5hikn8yd1axg27ypcnx6vyz")))

(define-public crate-sunbeam-cli-0.0.18-alpha (c (n "sunbeam-cli") (v "0.0.18-alpha") (h "0lmcpbjccmh14b2mqhjc7jn6mckfzi6a45x8k9w2w0aj2ci9bgzn")))

(define-public crate-sunbeam-cli-0.0.19-alpha (c (n "sunbeam-cli") (v "0.0.19-alpha") (h "0x8zpg4a363vw9ld7vrxbp3w3ll5dk7gf3gh1qfxi6wdiwq5028s")))

(define-public crate-sunbeam-cli-0.0.20-alpha (c (n "sunbeam-cli") (v "0.0.20-alpha") (h "1p7vgdvwzljgw6srx3v4ddvpdbq8gp3xggyvhj5f2bhy0sss15c0")))

(define-public crate-sunbeam-cli-0.0.21-alpha (c (n "sunbeam-cli") (v "0.0.21-alpha") (h "0zc15vgmnqq3hkvrsnqkwivzyylyj2q4mzzgi879kbnwgc3cb77x")))

(define-public crate-sunbeam-cli-0.0.22-alpha (c (n "sunbeam-cli") (v "0.0.22-alpha") (h "0y6zdd5gzqna6hqy9j1pi4gjppnd0kh98yvx8is910kc5j76a9hv")))

