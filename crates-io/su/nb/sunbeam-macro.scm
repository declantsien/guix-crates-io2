(define-module (crates-io su nb sunbeam-macro) #:use-module (crates-io))

(define-public crate-sunbeam-macro-0.0.1-alpha (c (n "sunbeam-macro") (v "0.0.1-alpha") (d (list (d (n "sunbeam-ir") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "05wqbmic0slfj441mr5ni9hq9j5309md76jnqxsa3vbp68i8aqkj")))

(define-public crate-sunbeam-macro-0.0.2-alpha (c (n "sunbeam-macro") (v "0.0.2-alpha") (d (list (d (n "sunbeam-ir") (r "^0.0.2-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "057y1v7frnvyqdrrm8h3hzr7cnr5n4m28827pnsmpabyx5i3wixa")))

(define-public crate-sunbeam-macro-0.0.3-alpha (c (n "sunbeam-macro") (v "0.0.3-alpha") (d (list (d (n "sunbeam-ir") (r "^0.0.3-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0zh7m1xz586y7kgp7q3ag7qs5dq3i1adx1kz42lw5vgrrjrzn801")))

(define-public crate-sunbeam-macro-0.0.4-alpha (c (n "sunbeam-macro") (v "0.0.4-alpha") (d (list (d (n "sunbeam-ir") (r "^0.0.4-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "19k6q41q9vawqba51nhxhdm8b08s8wn8kb128sy29ig4gqy9lxsm")))

(define-public crate-sunbeam-macro-0.0.5-alpha (c (n "sunbeam-macro") (v "0.0.5-alpha") (d (list (d (n "sunbeam-ir") (r "^0.0.5-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0gncv6c6v8i967jn6kq84wclw5wvrqil7lncag34m8nr0d4av4k0")))

(define-public crate-sunbeam-macro-0.0.6-alpha (c (n "sunbeam-macro") (v "0.0.6-alpha") (d (list (d (n "sunbeam-ir") (r "^0.0.6-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1h6dj5ffrf5x89grkh889896wfc1550sbgcnx7m69b7j33bl5w5m")))

(define-public crate-sunbeam-macro-0.0.7-alpha (c (n "sunbeam-macro") (v "0.0.7-alpha") (d (list (d (n "sunbeam-ir") (r "^0.0.7-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "03npr6kyh3rchx5spv8dvffdfdkkfqhlqwj7q04r7gzq3f3w23m4")))

(define-public crate-sunbeam-macro-0.0.8-alpha (c (n "sunbeam-macro") (v "0.0.8-alpha") (d (list (d (n "sunbeam-ir") (r "^0.0.8-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "08lbhv7gzrs96lnkgl4igxif1963d0ql045yhhkzsxsqkzfm564r")))

(define-public crate-sunbeam-macro-0.0.9-alpha (c (n "sunbeam-macro") (v "0.0.9-alpha") (d (list (d (n "sunbeam-ir") (r "^0.0.9-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1j69v227xzszs06x0hly6b5wvmygcqyh9angkz80rga1n8y9431w")))

(define-public crate-sunbeam-macro-0.0.10-alpha (c (n "sunbeam-macro") (v "0.0.10-alpha") (d (list (d (n "sunbeam-ir") (r "^0.0.10-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1sxgdhk4rdplb0gxflb6nf93r8jqc6nqiwa76hhl1r9pdc6pk6rs")))

(define-public crate-sunbeam-macro-0.0.11-alpha (c (n "sunbeam-macro") (v "0.0.11-alpha") (d (list (d (n "sunbeam-ir") (r "^0.0.11-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1bg8yfbi6m0xi68id1d75hyjr4yb3c6gd7h0bhakwic659gvmxnw")))

(define-public crate-sunbeam-macro-0.0.12-alpha (c (n "sunbeam-macro") (v "0.0.12-alpha") (d (list (d (n "sunbeam-ir") (r "^0.0.12-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "12ff98zjd7vdl90g5zp3ki42ywx5lihbwvplbmjm26pnfjlh5a78")))

(define-public crate-sunbeam-macro-0.0.13-alpha (c (n "sunbeam-macro") (v "0.0.13-alpha") (d (list (d (n "sunbeam-ir") (r "^0.0.13-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1xafncz8bcdyakql0kzzl0wiafc2p7akq7dadidnpbpxdb9x1ca5")))

(define-public crate-sunbeam-macro-0.0.14-alpha (c (n "sunbeam-macro") (v "0.0.14-alpha") (d (list (d (n "sunbeam-ir") (r "^0.0.14-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1szc8jqg7z2zkdp2qz0dxh70r3xl6kkgqkk1s2rccp1vfd6kjbaq")))

(define-public crate-sunbeam-macro-0.0.15-alpha (c (n "sunbeam-macro") (v "0.0.15-alpha") (d (list (d (n "sunbeam-ir") (r "^0.0.15-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "13yhq8mjizxl8xjb3gh80g049yb7grv96352jqsr454kv99wzwpz")))

(define-public crate-sunbeam-macro-0.0.16-alpha (c (n "sunbeam-macro") (v "0.0.16-alpha") (d (list (d (n "sunbeam-ir") (r "^0.0.16-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1jsfly1cg9mjahjk6vjh6s8kyny8b57if68plpcvnc56g7ji8r7i")))

(define-public crate-sunbeam-macro-0.0.17-alpha (c (n "sunbeam-macro") (v "0.0.17-alpha") (d (list (d (n "sunbeam-ir") (r "^0.0.17-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "14jlbwl2221n7r38xwlrggllz3qcpk6dgrwky02jfz5i9scrm6aw")))

(define-public crate-sunbeam-macro-0.0.18-alpha (c (n "sunbeam-macro") (v "0.0.18-alpha") (d (list (d (n "sunbeam-ir") (r "^0.0.18-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0akv5hfh0r0frmlv69jsvn554lyx7b5i5ylkp1hj7p1bg1rawmrm")))

(define-public crate-sunbeam-macro-0.0.19-alpha (c (n "sunbeam-macro") (v "0.0.19-alpha") (d (list (d (n "sunbeam-ir") (r "^0.0.19-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "185ixbssppccs8rwza8mfp87n9nhn7fxymmpjkfsh871mh1z71n3")))

(define-public crate-sunbeam-macro-0.0.20-alpha (c (n "sunbeam-macro") (v "0.0.20-alpha") (d (list (d (n "sunbeam-ir") (r "^0.0.20-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1k0kr3z0iabjz3mlkzzm6bk8qm68rrh4g52gysk37n100g18bi47")))

(define-public crate-sunbeam-macro-0.0.21-alpha (c (n "sunbeam-macro") (v "0.0.21-alpha") (d (list (d (n "sunbeam-ir") (r "^0.0.21-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1lhmqmx8n6hlqzkygs3sfzghjdrsh2kzj07j7d2pn9mlag2xs0x5")))

(define-public crate-sunbeam-macro-0.0.22-alpha (c (n "sunbeam-macro") (v "0.0.22-alpha") (d (list (d (n "sunbeam-ir") (r "^0.0.22-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1b604fb4ibz9qfvbrasfdirwa1l3yqgaazaah0jclp4angpnw1ly")))

