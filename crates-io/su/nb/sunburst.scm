(define-module (crates-io su nb sunburst) #:use-module (crates-io))

(define-public crate-sunburst-0.1.0 (c (n "sunburst") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "1lwhrq74bg8w5xr985r21q60daffwl8s26dq71z542x63jd49g58")))

(define-public crate-sunburst-0.2.0 (c (n "sunburst") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "noto-sans-mono-bitmap") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (d #t) (k 0)))) (h "0crsvn9xrzf7c4agy4v5iskpdqws81kzaidsgnx4csgrwy72calr")))

