(define-module (crates-io su nb sunbeam) #:use-module (crates-io))

(define-public crate-sunbeam-0.0.1-alpha (c (n "sunbeam") (v "0.0.1-alpha") (d (list (d (n "sunbeam-macro") (r "^0.0.1-alpha") (d #t) (k 0)))) (h "1p89r7qlpkzkdf9c86qm18774i1hi8pymbp67nsnp44r5f5nywz8")))

(define-public crate-sunbeam-0.0.2-alpha (c (n "sunbeam") (v "0.0.2-alpha") (d (list (d (n "sunbeam-macro") (r "^0.0.2-alpha") (d #t) (k 0)))) (h "0spmnxkx3dm8lz2908nwm57yfdyqg1ldlibrfjfiq6fxnsnhmf3v")))

(define-public crate-sunbeam-0.0.3-alpha (c (n "sunbeam") (v "0.0.3-alpha") (d (list (d (n "sunbeam-macro") (r "^0.0.3-alpha") (d #t) (k 0)))) (h "0l6aqy9arc7zvxpwz6q5bza247bcn0s5s4bmsmj5djz71szjx8kc")))

(define-public crate-sunbeam-0.0.4-alpha (c (n "sunbeam") (v "0.0.4-alpha") (d (list (d (n "sunbeam-macro") (r "^0.0.4-alpha") (d #t) (k 0)))) (h "1ibary8qf43zjwkrilrwxn5p4mivagb888c7z8y1nq01s39ki8q1")))

(define-public crate-sunbeam-0.0.5-alpha (c (n "sunbeam") (v "0.0.5-alpha") (d (list (d (n "sunbeam-macro") (r "^0.0.5-alpha") (d #t) (k 0)))) (h "1rgkzdk2gjdxcghck9wj9kqxx0k3kyaqkagm1apxhqh9hmkydd39")))

(define-public crate-sunbeam-0.0.6-alpha (c (n "sunbeam") (v "0.0.6-alpha") (d (list (d (n "sunbeam-macro") (r "^0.0.6-alpha") (d #t) (k 0)))) (h "02xlazj6qqj8canng5hwpq9ry0gcpbm3kq15gfwa3r2lby1b7c6c")))

(define-public crate-sunbeam-0.0.7-alpha (c (n "sunbeam") (v "0.0.7-alpha") (d (list (d (n "sunbeam-macro") (r "^0.0.7-alpha") (d #t) (k 0)))) (h "0sqv6ywqshgwi7w54iisyidxpzj63bf650nqqdiprfzx9wjsbqj3")))

(define-public crate-sunbeam-0.0.8-alpha (c (n "sunbeam") (v "0.0.8-alpha") (d (list (d (n "sunbeam-macro") (r "^0.0.8-alpha") (d #t) (k 0)))) (h "1s8pgx85ycwz25fg9ks38k8i29cwabl5qlrklji89bzhzkd34a1q")))

(define-public crate-sunbeam-0.0.9-alpha (c (n "sunbeam") (v "0.0.9-alpha") (d (list (d (n "sunbeam-macro") (r "^0.0.9-alpha") (d #t) (k 0)))) (h "11ifla83n16b5pzx45c9y7lmw2fkga1rhfb0nwlkf97s3z9ig197")))

(define-public crate-sunbeam-0.0.10-alpha (c (n "sunbeam") (v "0.0.10-alpha") (d (list (d (n "sunbeam-macro") (r "^0.0.10-alpha") (d #t) (k 0)))) (h "0gjdx0aaigncvpljr4wavl66cv3jzmk3r0xkbgyk5fgbzglxa1bc")))

(define-public crate-sunbeam-0.0.11-alpha (c (n "sunbeam") (v "0.0.11-alpha") (d (list (d (n "sunbeam-macro") (r "^0.0.11-alpha") (d #t) (k 0)))) (h "14cirg304s9hsiz37pbbmkqd9waiwr2cnlk4bmnq1kbc2ba327aw")))

(define-public crate-sunbeam-0.0.12-alpha (c (n "sunbeam") (v "0.0.12-alpha") (d (list (d (n "sunbeam-macro") (r "^0.0.12-alpha") (d #t) (k 0)))) (h "0834az4y70prr1lypcddpqr65wnwypr1m25dw2rc1wa4mpjxwjkk")))

(define-public crate-sunbeam-0.0.13-alpha (c (n "sunbeam") (v "0.0.13-alpha") (d (list (d (n "sunbeam-macro") (r "^0.0.13-alpha") (d #t) (k 0)))) (h "0g7m25vxhnhm8919zdnvjn2rli3wqjc1nwlf9pdl24hq1l2dg6dp")))

(define-public crate-sunbeam-0.0.14-alpha (c (n "sunbeam") (v "0.0.14-alpha") (d (list (d (n "sunbeam-macro") (r "^0.0.14-alpha") (d #t) (k 0)))) (h "14y8av7h25amvd453kf72av1ib292nyvklbr5yq2mbhxdlvb8qc0")))

(define-public crate-sunbeam-0.0.15-alpha (c (n "sunbeam") (v "0.0.15-alpha") (d (list (d (n "sunbeam-macro") (r "^0.0.15-alpha") (d #t) (k 0)))) (h "0kn7yjkgc5w6hgh3xlz2wv0383c75kgkb52s2bc53awa625s2cy5")))

(define-public crate-sunbeam-0.0.16-alpha (c (n "sunbeam") (v "0.0.16-alpha") (d (list (d (n "sunbeam-macro") (r "^0.0.16-alpha") (d #t) (k 0)))) (h "19b0h2r1g6i1idqi826aj5j2acszfdkrnh553vaisaxf6bwpxbd6")))

(define-public crate-sunbeam-0.0.17-alpha (c (n "sunbeam") (v "0.0.17-alpha") (d (list (d (n "sunbeam-macro") (r "^0.0.17-alpha") (d #t) (k 0)))) (h "1knxcsfhdg1fvc4wpni8ir4v687i1fhly0v0z083agbdzgf78bj5")))

(define-public crate-sunbeam-0.0.18-alpha (c (n "sunbeam") (v "0.0.18-alpha") (d (list (d (n "sunbeam-macro") (r "^0.0.18-alpha") (d #t) (k 0)))) (h "1rfnbzx0430s94x61x2niwi586j881r42xx2wxndb6lb0rlpvbqx")))

(define-public crate-sunbeam-0.0.19-alpha (c (n "sunbeam") (v "0.0.19-alpha") (d (list (d (n "sunbeam-macro") (r "^0.0.19-alpha") (d #t) (k 0)))) (h "0g62apd163711m2dh3kw48w0nrzdj8wpi8ilhy4j99zf13y04f30")))

(define-public crate-sunbeam-0.0.20-alpha (c (n "sunbeam") (v "0.0.20-alpha") (d (list (d (n "sunbeam-macro") (r "^0.0.20-alpha") (d #t) (k 0)))) (h "17my2q57b83dy3fwkbj202czp78km8xh7q83aqv3xd409v18gij9")))

(define-public crate-sunbeam-0.0.21-alpha (c (n "sunbeam") (v "0.0.21-alpha") (d (list (d (n "sunbeam-macro") (r "^0.0.21-alpha") (d #t) (k 0)))) (h "03s5scs1jwvchqqf0ardv01ifh671xm258bfx1ds62yjlhxjbprl")))

(define-public crate-sunbeam-0.0.22-alpha (c (n "sunbeam") (v "0.0.22-alpha") (d (list (d (n "sunbeam-macro") (r "^0.0.22-alpha") (d #t) (k 0)))) (h "1w7cfr0kizqwqq8zzs7jk02433x5khqn1zv8746dp0nmavcz1p5c")))

