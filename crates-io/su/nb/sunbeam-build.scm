(define-module (crates-io su nb sunbeam-build) #:use-module (crates-io))

(define-public crate-sunbeam-build-0.0.1-alpha (c (n "sunbeam-build") (v "0.0.1-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "sunbeam-ir") (r "^0.0.1-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0lcbaklhzkkvy7qxfk79aw8rdli3gs4mgwv51zvb89myqaym9xim") (f (quote (("serde" "sunbeam-ir/serde"))))))

(define-public crate-sunbeam-build-0.0.2-alpha (c (n "sunbeam-build") (v "0.0.2-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "sunbeam-ir") (r "^0.0.2-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "162bfs31i9dirpj7jkcpsnbd3x2af29jll522lsjpxq9h1b40ikh")))

(define-public crate-sunbeam-build-0.0.3-alpha (c (n "sunbeam-build") (v "0.0.3-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "sunbeam-ir") (r "^0.0.3-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ibxskdnfpmi06jmlr0s1mz1b5hq2g6ww0rj798zzyfrlfwrzvc2")))

(define-public crate-sunbeam-build-0.0.4-alpha (c (n "sunbeam-build") (v "0.0.4-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "sunbeam-ir") (r "^0.0.4-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0j5h9l4kg82r2klkzn49xpa2agzgz7216220frf5ymwl64fl1zn2")))

(define-public crate-sunbeam-build-0.0.5-alpha (c (n "sunbeam-build") (v "0.0.5-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "sunbeam-ir") (r "^0.0.5-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ky1hgbiy3n7nsx1h54ddqj1k81bqrxp5f3y0dfp9a0hi8198ah9")))

(define-public crate-sunbeam-build-0.0.6-alpha (c (n "sunbeam-build") (v "0.0.6-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "sunbeam-ir") (r "^0.0.6-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1z31pn7ac0canyay5jpswl4smrpgw6f2hfr0d358m4yx1y7asqgh")))

(define-public crate-sunbeam-build-0.0.7-alpha (c (n "sunbeam-build") (v "0.0.7-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "sunbeam-ir") (r "^0.0.7-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "18dwp6yg29gay22f7q7v55v181gw6rbs9graz8jbjrad2kvsm6x8")))

(define-public crate-sunbeam-build-0.0.8-alpha (c (n "sunbeam-build") (v "0.0.8-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "sunbeam-ir") (r "^0.0.8-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1sgxcwdla992yvhpkmxj5bhcq58apflfyb1vpc1a16671w60h84n")))

(define-public crate-sunbeam-build-0.0.9-alpha (c (n "sunbeam-build") (v "0.0.9-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "sunbeam-ir") (r "^0.0.9-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0y9k0szk6f4d3i49ddirahfgj28kmqcq7fcik26ifbvazq01yacc")))

(define-public crate-sunbeam-build-0.0.10-alpha (c (n "sunbeam-build") (v "0.0.10-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "sunbeam-ir") (r "^0.0.10-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03q6nfblnmbrxaq206f2qmaz0v7prp6jwbqwjd5w4chm0h24d8q1")))

(define-public crate-sunbeam-build-0.0.11-alpha (c (n "sunbeam-build") (v "0.0.11-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "sunbeam-ir") (r "^0.0.11-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0lwkhsl6rw1r6p1fh79vgrpvpg64gv5c6jf5fzjd471alhb78m03")))

(define-public crate-sunbeam-build-0.0.12-alpha (c (n "sunbeam-build") (v "0.0.12-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "sunbeam-ir") (r "^0.0.12-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1r3niih5zkbbarjxpmjrkpdk416ghzi1i33zm15kbqzvxxzbc5hw")))

(define-public crate-sunbeam-build-0.0.13-alpha (c (n "sunbeam-build") (v "0.0.13-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "sunbeam-ir") (r "^0.0.13-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1hv3nhsq6l8fvlbdm0ijaw040dh0mri1icqjrq45q79djivkjry3")))

(define-public crate-sunbeam-build-0.0.14-alpha (c (n "sunbeam-build") (v "0.0.14-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "sunbeam-ir") (r "^0.0.14-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1qmlwy0amlpsrrn19xv1wp5wm5i6b9k72g9fvdjiwj6lhkmbjfcm")))

(define-public crate-sunbeam-build-0.0.15-alpha (c (n "sunbeam-build") (v "0.0.15-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "sunbeam-ir") (r "^0.0.15-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "108ik0zag4rzcxjv30fyry26c21bq8lckxd0ampy7fj8g8mwy0d9")))

(define-public crate-sunbeam-build-0.0.16-alpha (c (n "sunbeam-build") (v "0.0.16-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "sunbeam-ir") (r "^0.0.16-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "18wllbwnvc03n91v5rhvb2izy7m8lzrby9g7raz90iazziz9xj6d")))

(define-public crate-sunbeam-build-0.0.17-alpha (c (n "sunbeam-build") (v "0.0.17-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "sunbeam-ir") (r "^0.0.17-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "10bz8vxak324blfybg1plip21xp9ls495yjd138g2bn4a397jwg4")))

(define-public crate-sunbeam-build-0.0.18-alpha (c (n "sunbeam-build") (v "0.0.18-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "sunbeam-ir") (r "^0.0.18-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1fl59bifs0cajgbkn1yyxqzdq213s6amlsxchdk5bwh4apzlhfd1")))

(define-public crate-sunbeam-build-0.0.19-alpha (c (n "sunbeam-build") (v "0.0.19-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "sunbeam-ir") (r "^0.0.19-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0s0xh42hgy1zvhqa37s12yza3lr5nxhxqg3bh8sm08c64h1bk4wv")))

(define-public crate-sunbeam-build-0.0.20-alpha (c (n "sunbeam-build") (v "0.0.20-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "sunbeam-ir") (r "^0.0.20-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0hrm02117zfklckp8psil2f28igpsb6r7dj3a9gbqrz8l6spnb7c")))

(define-public crate-sunbeam-build-0.0.21-alpha (c (n "sunbeam-build") (v "0.0.21-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "sunbeam-ir") (r "^0.0.21-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0m0f4rkshnammm989rpd4iir96qxi8ky4m42s1r4n80c52ip8g1c")))

(define-public crate-sunbeam-build-0.0.22-alpha (c (n "sunbeam-build") (v "0.0.22-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "sunbeam-ir") (r "^0.0.22-alpha") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1cwa7g4g9zkrahrh6spps3j4g7bwibskn201g2i962qa76dxhpim")))

