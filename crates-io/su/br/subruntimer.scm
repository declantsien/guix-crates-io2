(define-module (crates-io su br subruntimer) #:use-module (crates-io))

(define-public crate-subruntimer-0.9.0-rc13 (c (n "subruntimer") (v "0.9.0-rc13") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "08xhglg0f4nyl4hchfhwp7zdj4d75pd9kycvqnl6vr0s3d15gh10")))

(define-public crate-subruntimer-0.9.0-rc14 (c (n "subruntimer") (v "0.9.0-rc14") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1j1di5mi5cbbpsfp7515mngjlhvr7f1ax8hi4n0d1i0fwi0vhhqh")))

(define-public crate-subruntimer-0.9.0-rc15 (c (n "subruntimer") (v "0.9.0-rc15") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1k8baqmbyyyybzb4dgw4h1f5k2jkbxfkdqspaq6dnvdw5h12gr6q")))

(define-public crate-subruntimer-0.9.0-rc16 (c (n "subruntimer") (v "0.9.0-rc16") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0kd1cgm906vngpz5y81rv7j32jnbq2g45k4qikgkqj1hwh5jbgx7")))

(define-public crate-subruntimer-0.9.0-rc17 (c (n "subruntimer") (v "0.9.0-rc17") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1dddwrbjiv8m8icq89hvifd1xyd7h1ba6niqkxw604prasbhxkpy")))

(define-public crate-subruntimer-0.9.0-rc18 (c (n "subruntimer") (v "0.9.0-rc18") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xhwignclg1kvp2z3ppij8qbksqqd124ismi2mwxl5iqgbqjrh3b")))

(define-public crate-subruntimer-0.9.0-rc19 (c (n "subruntimer") (v "0.9.0-rc19") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "053pnr0zb7643n8ncrjwvm35wgjn210yr4gi7ylplwmlnmbgn0cm")))

(define-public crate-subruntimer-0.9.0-rc20 (c (n "subruntimer") (v "0.9.0-rc20") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "055mbncs50wjfhjj69hqq94vyxmgg7qd1br3fafkdd8wa42mww5n")))

(define-public crate-subruntimer-0.9.0 (c (n "subruntimer") (v "0.9.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1c6wn96xi3ssr19q73wbdf8s3r28lfn2ks6iqq8bzw104hprpwpz")))

(define-public crate-subruntimer-0.9.1 (c (n "subruntimer") (v "0.9.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "00z70yycziyl2gvqcbc43aqzr1ss404f19w9gkwi9imxnzq92mcd")))

(define-public crate-subruntimer-0.9.2 (c (n "subruntimer") (v "0.9.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1crqqjmcc305rj09sk6n0cwihs1p47gmpxnmjc44q3dnj29mzsbv")))

(define-public crate-subruntimer-0.10.0 (c (n "subruntimer") (v "0.10.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0r36lfrgp7mx7y30xx75k4nzh0j4fppwxxl3s6bca5dl1zaassyg")))

(define-public crate-subruntimer-0.10.1 (c (n "subruntimer") (v "0.10.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "12yg2h10pyhpl7ly7gs9hk6zna1b0c39y5i3jxc09cklkyp1lz2x")))

(define-public crate-subruntimer-0.11.0 (c (n "subruntimer") (v "0.11.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0zd6m2imvz15p4gph68qparhbjdhzhfkdk7z617g46zcqd0p263r")))

