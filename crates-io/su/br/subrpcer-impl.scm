(define-module (crates-io su br subrpcer-impl) #:use-module (crates-io))

(define-public crate-subrpcer-impl-0.1.0 (c (n "subrpcer-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.70") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0sixyhqlk60q35hbaajxd7nvm5jhjljq119cdi4az3r0zb0vgxxr")))

(define-public crate-subrpcer-impl-0.1.1 (c (n "subrpcer-impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.70") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1jdgvi3vw97s7mhzlrdhack9kfdkkfqw0w7r3pqvfc02zcbqddyf")))

(define-public crate-subrpcer-impl-0.1.2 (c (n "subrpcer-impl") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.70") (f (quote ("full"))) (d #t) (k 0)))) (h "103pbq9jrp5rd3yg2lxwp8zz258w6n9vx8f8iajkfbi0k5cpmm4y")))

(define-public crate-subrpcer-impl-0.1.3 (c (n "subrpcer-impl") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qpa7cg28s5iq445bv112g18d0lvw28slmq1q57aplk8hh1j70ii")))

(define-public crate-subrpcer-impl-0.7.0 (c (n "subrpcer-impl") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "027plhfdmswn7fxc3ar9plwrg2vg4qn5xyhlb0a5df3pgirb6b8s")))

