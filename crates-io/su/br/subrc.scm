(define-module (crates-io su br subrc) #:use-module (crates-io))

(define-public crate-subrc-0.1.0 (c (n "subrc") (v "0.1.0") (h "1jf19kz4z4pph1434i4ary46aam13xn0wkm7c5qgh8w3ynmrr91j")))

(define-public crate-subrc-0.2.0 (c (n "subrc") (v "0.2.0") (h "0sra5q7v2lwzclbkzxr1scsh3rms8lydb3r9rnap1zmk3jkd3fsh")))

