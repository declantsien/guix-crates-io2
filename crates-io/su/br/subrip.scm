(define-module (crates-io su br subrip) #:use-module (crates-io))

(define-public crate-subrip-0.1.0 (c (n "subrip") (v "0.1.0") (d (list (d (n "encoding_rs") (r "^0.8.31") (d #t) (k 2)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "08qk7g84nxm84r419fifbvgylaz6rkpf1cd5a0l0chqispbp9mpr") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-subrip-0.1.1 (c (n "subrip") (v "0.1.1") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 2)))) (h "0laq972qgfa7bkmnxlf2s995swch2fmqsr3bnyy80cyhks72smcb") (s 2) (e (quote (("serde" "dep:serde"))))))

