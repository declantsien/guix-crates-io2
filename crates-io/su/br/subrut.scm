(define-module (crates-io su br subrut) #:use-module (crates-io))

(define-public crate-subrut-0.1.0 (c (n "subrut") (v "0.1.0") (d (list (d (n "anscape") (r "^0.2.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.21.2") (d #t) (k 0)))) (h "1wv83rfk730ly8nhgp264lwmn0axwk0yjp183kzlhybwr2wxz2nk")))

(define-public crate-subrut-0.1.1 (c (n "subrut") (v "0.1.1") (d (list (d (n "anscape") (r "^0.2.2") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.21.2") (d #t) (k 0)))) (h "12yxd52pjpdqisrmaffw2glwr80k0naaf865cf057mdp8ql86ykx")))

