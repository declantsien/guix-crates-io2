(define-module (crates-io su bf subfilter) #:use-module (crates-io))

(define-public crate-subfilter-0.1.0 (c (n "subfilter") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "subparse") (r "^0.6") (d #t) (k 0)) (d (n "time") (r "^0.2") (k 0)))) (h "1k0n1kfdjzmw0w7bmfmdpz3dxh79l9pqf4c8bkqnz8byhx1hq6i1")))

(define-public crate-subfilter-0.2.0 (c (n "subfilter") (v "0.2.0") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "subparse") (r "^0.6") (d #t) (k 0)) (d (n "time") (r "^0.2") (k 0)))) (h "14436vph7rg9sq6zh8k5n9a5csww7ahr2hig1lmkrfi3hqa1ha3s")))

(define-public crate-subfilter-0.3.0 (c (n "subfilter") (v "0.3.0") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "subparse") (r "^0.6") (d #t) (k 0)) (d (n "time") (r "^0.2") (k 0)))) (h "1fk6wsf6qypw7v7yxm2m0r1z27bpfb2dwgrv22nym3w9fmpl8irc")))

