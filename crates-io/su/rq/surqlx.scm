(define-module (crates-io su rq surqlx) #:use-module (crates-io))

(define-public crate-surqlx-0.1.0 (c (n "surqlx") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0-beta.9") (d #t) (k 0)) (d (n "tabled") (r "^0.14.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros"))) (d #t) (k 0)))) (h "1dkqy20cvaqzmjdbmvxlj7zf8zidnj5s5hh2nqvpykcmldmlwpd0")))

