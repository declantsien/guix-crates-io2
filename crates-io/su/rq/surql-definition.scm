(define-module (crates-io su rq surql-definition) #:use-module (crates-io))

(define-public crate-surql-definition-0.2.0 (c (n "surql-definition") (v "0.2.0") (d (list (d (n "surql-definition-core") (r "^0.2.0") (d #t) (k 0)) (d (n "surql-definition-macros") (r "^0.2.0") (d #t) (k 0)))) (h "0qvzj4dvxdw3cz44rq898cpvkiab4kak88v7hfxc9qd0ki42xm4j") (f (quote (("runtime_query_validation" "surql-definition-macros/runtime_query_validation") ("default") ("compile_query_validation" "surql-definition-macros/compile_query_validation"))))))

(define-public crate-surql-definition-0.2.1 (c (n "surql-definition") (v "0.2.1") (d (list (d (n "surql-definition-core") (r "^0.2.1") (d #t) (k 0)) (d (n "surql-definition-macros") (r "^0.2.1") (d #t) (k 0)))) (h "1a70mx2sd2039dfhyw0a28307v8gl50rhrdnlnavpkmqam84sgb7") (f (quote (("runtime_query_validation" "surql-definition-macros/runtime_query_validation") ("default") ("compile_query_validation" "surql-definition-macros/compile_query_validation"))))))

