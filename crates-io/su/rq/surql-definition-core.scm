(define-module (crates-io su rq surql-definition-core) #:use-module (crates-io))

(define-public crate-surql-definition-core-0.2.0 (c (n "surql-definition-core") (v "0.2.0") (h "1qzl684g0rhdf6rrdd8v5gqghsjj118k8kn02qb4dglrdqkwspla")))

(define-public crate-surql-definition-core-0.2.1 (c (n "surql-definition-core") (v "0.2.1") (h "1d9bks59xlk1p6yf2m1rndd0ffyi3ia59slbdzc0rrzm3v0rfabh")))

