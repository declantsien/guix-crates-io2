(define-module (crates-io su m- sum-queue) #:use-module (crates-io))

(define-public crate-sum-queue-0.1.0 (c (n "sum-queue") (v "0.1.0") (h "10873k53kfw8328d37gzr5rg249qy7df1h2w1fxm2fnp9xvglhwp")))

(define-public crate-sum-queue-0.2.0 (c (n "sum-queue") (v "0.2.0") (h "0m1amdx44s5r75im7p6xcrw72firp7jdl7h7sm15h56ljyiqhc7p")))

(define-public crate-sum-queue-1.0.0 (c (n "sum-queue") (v "1.0.0") (h "17yl942a71x789mnprw8f6m74ggcnpi7jj5gm3az5dqricfad1sr")))

