(define-module (crates-io su m- sum-calc) #:use-module (crates-io))

(define-public crate-sum-calc-0.0.0 (c (n "sum-calc") (v "0.0.0") (d (list (d (n "aoko") (r "^0.3.0-alpha.1") (d #t) (k 0)))) (h "1dhniy9kkj5s1swh7q16rfaivy9jfdr9q46gmh0vw1c1rsm63yxc")))

(define-public crate-sum-calc-0.0.1 (c (n "sum-calc") (v "0.0.1") (d (list (d (n "aoko") (r "^0.3.0-alpha.1") (d #t) (k 0)))) (h "1qfakvlfvbyd9fqwqfjd996d045y4l6789r5qgzmsvql1siphrmi")))

(define-public crate-sum-calc-0.0.2 (c (n "sum-calc") (v "0.0.2") (d (list (d (n "aoko") (r "^0.3.0-alpha.1") (d #t) (k 0)))) (h "1q2kmm4sm0qyqbm6x7hcaqg1lg1si8zipwp2ndxk3495ypqxf707")))

(define-public crate-sum-calc-0.0.3 (c (n "sum-calc") (v "0.0.3") (d (list (d (n "aoko") (r "=0.3.0-alpha.19") (d #t) (k 0)))) (h "0why56w94hn9jbrp9aj32ah0kf4daxqh67n0r26gmvij33gj14jc")))

(define-public crate-sum-calc-0.0.4 (c (n "sum-calc") (v "0.0.4") (d (list (d (n "aoko") (r "=0.3.0-alpha.19") (d #t) (k 0)))) (h "1i8sfi6q2g4lnlaz6akaqpjfbfspv1is5wgx3pk6cnixv4in4fv2")))

(define-public crate-sum-calc-0.0.5 (c (n "sum-calc") (v "0.0.5") (d (list (d (n "aoko") (r "=0.3.0-alpha.19") (d #t) (k 0)))) (h "0yhgsr8jq64c1szw0bzjjkr0v43yfd3jv1j7nyc9gkmi4zwbswjm")))

(define-public crate-sum-calc-0.0.6 (c (n "sum-calc") (v "0.0.6") (d (list (d (n "aoko") (r "=0.3.0-alpha.19") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "16mqaycspjqwksi0yf2n20fn3yw68fjbpdpsy088yj04xbxsc86f")))

(define-public crate-sum-calc-0.0.7 (c (n "sum-calc") (v "0.0.7") (d (list (d (n "aoko") (r "=0.3.0-alpha.19") (d #t) (k 0)))) (h "16dj8s249kwwpz67yax07ybhx0a78dlg13lzxwzyvnjqglgw480y")))

(define-public crate-sum-calc-0.0.8 (c (n "sum-calc") (v "0.0.8") (d (list (d (n "aoko") (r "=0.3.0-alpha.19") (d #t) (k 0)))) (h "0c3x8xamcq8g9ci2d3x0l5vx0hl3p6vl58744xln65ysd3pxvq93")))

(define-public crate-sum-calc-0.0.9 (c (n "sum-calc") (v "0.0.9") (d (list (d (n "aoko") (r "=0.3.0-alpha.19") (d #t) (k 0)))) (h "0kj7nxvlnrgxklrpf4frfvida2c7r94miwxi0k554bg5q20s3fpq")))

(define-public crate-sum-calc-0.0.10 (c (n "sum-calc") (v "0.0.10") (d (list (d (n "aoko") (r "=0.3.0-alpha.19") (d #t) (k 0)))) (h "1blg1pmvmgmgri6s6nz23jx5wyc9w50x74ffs6mhyf2bpf1rh2zw")))

(define-public crate-sum-calc-0.0.11 (c (n "sum-calc") (v "0.0.11") (d (list (d (n "aoko") (r "=0.3.0-alpha.19") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "14dfiriblxdv69y9pl13adqxkvx4scm8flgm8jzh74hvhxd27vbd")))

(define-public crate-sum-calc-0.0.12 (c (n "sum-calc") (v "0.0.12") (d (list (d (n "aoko") (r "=0.3.0-alpha.19") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "1ljsyz4i3492ah989dxr8iay5bdihxdpl5azlwdbc554z5vb8zkf")))

(define-public crate-sum-calc-0.0.13 (c (n "sum-calc") (v "0.0.13") (d (list (d (n "aoko") (r "^0.3.0-alpha.25") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)))) (h "14q66j4f4xn37ylg5nl125yjy2wbr4cyh3a9fnmnzr8qcar77m47")))

(define-public crate-sum-calc-0.2.0 (c (n "sum-calc") (v "0.2.0") (h "1z8083z63rqjljb632aj49gid24r60vv1qv5id9p25vs6y82yig3")))

(define-public crate-sum-calc-0.2.1 (c (n "sum-calc") (v "0.2.1") (h "15snm0fc6n98fqnqj2l2njbb55i4xmzyf34p3kmkq4sk2czwip5x")))

(define-public crate-sum-calc-0.2.2 (c (n "sum-calc") (v "0.2.2") (h "15pdbh444rvp1n48mjpjj2v98kfrg3jdcw8b7i8dwlkncjgnnmfb")))

(define-public crate-sum-calc-0.2.3 (c (n "sum-calc") (v "0.2.3") (h "0zrxbgqhhqdjpbf6bsl0ix9sa14y4sjf2lpn7bi0jzllavhn16zj")))

