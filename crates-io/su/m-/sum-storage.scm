(define-module (crates-io su m- sum-storage) #:use-module (crates-io))

(define-public crate-sum-storage-2.0.0-alpha.6 (c (n "sum-storage") (v "2.0.0-alpha.6") (d (list (d (n "codec") (r "^1.3.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "frame-support") (r "^2.0.0-alpha.6") (k 0)) (d (n "frame-system") (r "^2.0.0-alpha.6") (k 0)) (d (n "sp-core") (r "^2.0.0-alpha.6") (k 2)) (d (n "sp-io") (r "^2.0.0-alpha.6") (k 2)) (d (n "sp-runtime") (r "^2.0.0-alpha.6") (k 0)) (d (n "sp-std") (r "^2.0.0-alpha.6") (k 0)))) (h "07lng1l3ks55ia5q8rca8q7nkya45rlzx99mnfv647pf5d77m1pd") (f (quote (("std" "codec/std" "sp-std/std" "sp-runtime/std" "frame-support/std" "frame-system/std") ("default" "std"))))))

