(define-module (crates-io su rr surreal-derive) #:use-module (crates-io))

(define-public crate-surreal-derive-0.1.0 (c (n "surreal-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "0gqrv9x9x7ck7zp3hzvibal4li3hrhnycrcklbimd8qj5s7rk10c")))

(define-public crate-surreal-derive-0.1.1 (c (n "surreal-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.103") (d #t) (k 0)))) (h "1rpkghf495mwc8sp0fdkdbk2dslwlx515sqcm9clp3zbm1y8nkdd")))

