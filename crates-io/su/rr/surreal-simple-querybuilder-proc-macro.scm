(define-module (crates-io su rr surreal-simple-querybuilder-proc-macro) #:use-module (crates-io))

(define-public crate-surreal-simple-querybuilder-proc-macro-0.1.0 (c (n "surreal-simple-querybuilder-proc-macro") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.19.8") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.7") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1k9ai0wg1nyqi95pxgqgbd40lwp1mh6kv4lq602jw0qxl13yi23d")))

(define-public crate-surreal-simple-querybuilder-proc-macro-0.2.0 (c (n "surreal-simple-querybuilder-proc-macro") (v "0.2.0") (d (list (d (n "lalrpop") (r "^0.19.8") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.7") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "18fzx3il32yfgbk5bvnw70z61djb0nmydk85yjqnqrciagq3gswd")))

(define-public crate-surreal-simple-querybuilder-proc-macro-0.5.0 (c (n "surreal-simple-querybuilder-proc-macro") (v "0.5.0") (d (list (d (n "lalrpop") (r "^0.19.8") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.7") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "12kffdjpnsvmmiixlkznn0vrsp28923f3pw9lkblnc6sbjmc8qpy")))

(define-public crate-surreal-simple-querybuilder-proc-macro-0.6.0 (c (n "surreal-simple-querybuilder-proc-macro") (v "0.6.0") (d (list (d (n "lalrpop") (r "^0.19.8") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.7") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "08m2bhldcci7lwyi689b7ppc3k1v2dwpdqhngsd465nbff4zv5h8")))

(define-public crate-surreal-simple-querybuilder-proc-macro-0.7.0 (c (n "surreal-simple-querybuilder-proc-macro") (v "0.7.0") (d (list (d (n "lalrpop") (r "^0.19.8") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.7") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "03x7xfsqxn5gw7rf4p67vkb9hf30ldmb76akq95sjirjkjdk30y6")))

(define-public crate-surreal-simple-querybuilder-proc-macro-0.8.0 (c (n "surreal-simple-querybuilder-proc-macro") (v "0.8.0") (d (list (d (n "lalrpop") (r "^0.20.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20.0") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "12ra297jfmgm11nzww2pz4q8s8sbvfdw8015l4c8nylfjzhdq5vy")))

