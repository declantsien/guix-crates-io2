(define-module (crates-io su rr surrealdb-obj-derive) #:use-module (crates-io))

(define-public crate-surrealdb-obj-derive-0.1.0 (c (n "surrealdb-obj-derive") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.58") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0-beta.8") (f (quote ("kv-rocksdb" "parallel"))) (k 0)) (d (n "surrealdb-obj-derive-impl") (r "=0.1.0") (d #t) (k 0)) (d (n "surrealdb-obj-derive-macro") (r "=0.1.0") (d #t) (k 0)))) (h "0n00f2jcgbqvqnr07ii044088pvqcb9b4klgbg5ak7q5nll0gg62")))

(define-public crate-surrealdb-obj-derive-0.1.1 (c (n "surrealdb-obj-derive") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.58") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0-beta.8") (f (quote ("kv-rocksdb" "parallel"))) (k 0)) (d (n "surrealdb-obj-derive-impl") (r "=0.1.1") (d #t) (k 0)) (d (n "surrealdb-obj-derive-macro") (r "=0.1.1") (d #t) (k 0)))) (h "1skx71wv6ki3l313abyc0icsxysxjlx763rcmyfldyqawk25ymlf")))

