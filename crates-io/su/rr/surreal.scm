(define-module (crates-io su rr surreal) #:use-module (crates-io))

(define-public crate-surreal-0.0.1 (c (n "surreal") (v "0.0.1") (h "1vlxd3maa0x6vmhwzgyli4vdjhv7ifgd8zhrxc8y847yhan4ww2s")))

(define-public crate-surreal-0.1.0 (c (n "surreal") (v "0.1.0") (h "0hmbl68rj7h350sg48s6221nyb3273i5ha5k6gfvf3m9zmv1dw16")))

(define-public crate-surreal-0.1.1 (c (n "surreal") (v "0.1.1") (h "1ls11vgbsxwdnjzy7yx73hjgvk4xm3sl22vl3i7wfn2f02mc6pnk")))

(define-public crate-surreal-0.1.3 (c (n "surreal") (v "0.1.3") (h "043gfliccj0fzjrs3ghyzg6my8fjfzrv27scl63w0zs2rhx990d7")))

(define-public crate-surreal-0.2.0 (c (n "surreal") (v "0.2.0") (h "01pm8a9y4kmdmv4668v3rhagy1vnk5s3i53k7qs446jknpgcnymg")))

(define-public crate-surreal-0.3.0 (c (n "surreal") (v "0.3.0") (h "0w180n7b7dwfdgn9g15jc9j5szyn4bby42r15pz5yw4kxkl9ay0y")))

(define-public crate-surreal-0.3.1 (c (n "surreal") (v "0.3.1") (h "0q5f75x8gwx7dsvlxsbjx2xvpd55xy642d2sj3f5gm7phg2yvwgd")))

(define-public crate-surreal-0.4.0 (c (n "surreal") (v "0.4.0") (h "06kx3q8xp7198gynqdwkw0l8p49d4c8ha38fl9brwnf7l4vhvwqd")))

(define-public crate-surreal-0.4.1 (c (n "surreal") (v "0.4.1") (h "0p880ai2llr3c93r48imf9kcrfnj0i7mrfwpdyfhiys110lylyhr")))

