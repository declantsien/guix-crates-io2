(define-module (crates-io su rr surrealdb_extra_derive) #:use-module (crates-io))

(define-public crate-surrealdb_extra_derive-0.1.0 (c (n "surrealdb_extra_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0-beta.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("derive"))) (d #t) (k 0)))) (h "16kqz0cxlcarwx9v6z8p7vi47l3xrizc3k4hgaypiybha6l8bp9n") (y #t)))

(define-public crate-surrealdb_extra_derive-0.1.1 (c (n "surrealdb_extra_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0-beta.9") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("derive"))) (d #t) (k 0)))) (h "035iz1a9r65ja5nqk5kyxfbia777sijslcwldhsmam8zd7swn52p") (y #t)))

(define-public crate-surrealdb_extra_derive-0.2.0 (c (n "surrealdb_extra_derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0-beta.10") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n5sl04d21hagvalnd4bvfxwvp0j6fs292fxc8ic74v4dnd04bna") (y #t)))

(define-public crate-surrealdb_extra_derive-0.3.0 (c (n "surrealdb_extra_derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0-beta.11") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("derive"))) (d #t) (k 0)))) (h "189qq2lgi60hd9sflci7wkrpimfjr2bszx26aiw428xazz8jy1pf") (y #t)))

(define-public crate-surrealdb_extra_derive-0.4.0 (c (n "surrealdb_extra_derive") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rkzx3i49zpr5ynvp38xl21hjw2l9z8y34gr2m3zsj05v699ywy2")))

(define-public crate-surrealdb_extra_derive-0.5.0 (c (n "surrealdb_extra_derive") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mz933d6q68qzkjahrg928ysk2fg0r1xxrwdcsyqmvm3h3zdyl16")))

(define-public crate-surrealdb_extra_derive-0.6.0 (c (n "surrealdb_extra_derive") (v "0.6.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.42") (f (quote ("derive"))) (d #t) (k 0)))) (h "13fy1gipsav19xmmnjdp234875p7w4irncwicr5klr9nmlw4xaq4")))

(define-public crate-surrealdb_extra_derive-0.7.0 (c (n "surrealdb_extra_derive") (v "0.7.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "surrealdb") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.42") (f (quote ("derive"))) (d #t) (k 0)))) (h "00mi701dzhdd416dva5b7jysa5niv6rj9knjc7lhgmzjn7g3m4s4")))

(define-public crate-surrealdb_extra_derive-0.7.1 (c (n "surrealdb_extra_derive") (v "0.7.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "surrealdb") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.42") (f (quote ("derive"))) (d #t) (k 0)))) (h "02rhhrkysfa90mzb1lv328dh6rbfxawd0yny1zwvjgldfv2w8ryn")))

(define-public crate-surrealdb_extra_derive-0.8.0 (c (n "surrealdb_extra_derive") (v "0.8.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "surrealdb") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.42") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k0fclgrqh04ph24jbxyj0jzwadl93dvlaxkn6wp3csibaiddsiz")))

(define-public crate-surrealdb_extra_derive-0.8.1 (c (n "surrealdb_extra_derive") (v "0.8.1") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "surrealdb") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.42") (f (quote ("derive"))) (d #t) (k 0)))) (h "0adhci0xa4w6mx1w566w2ylvdbfwv0j1851gydfymppiz807cc5i")))

