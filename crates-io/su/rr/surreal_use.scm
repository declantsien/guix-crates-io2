(define-module (crates-io su rr surreal_use) #:use-module (crates-io))

(define-public crate-surreal_use-0.1.0 (c (n "surreal_use") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "surrealdb") (r "^1.1.0") (d #t) (k 0)))) (h "1ygjhkq368pchhzhjl298fknm2c1sr8v38h49rwm42lz044245yi")))

