(define-module (crates-io su rr surreal_devl) #:use-module (crates-io))

(define-public crate-surreal_devl-1.0.0 (c (n "surreal_devl") (v "1.0.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0") (d #t) (k 0)))) (h "16l87x3qxk7dg2fz2596hj9rnw0pvxw5wazdx1xjff7fsyy07r16") (y #t)))

(define-public crate-surreal_devl-1.0.1 (c (n "surreal_devl") (v "1.0.1") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0") (d #t) (k 0)))) (h "0ndwqqvw5gndbkg8lzp6p94h6f7ld26d1w7fq10gfnxl35fz9ckv")))

(define-public crate-surreal_devl-1.0.2 (c (n "surreal_devl") (v "1.0.2") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0") (d #t) (k 0)))) (h "02i5qqmy42scqrkphm3flal03jdikhk4r09yspc542crnsr717gm")))

(define-public crate-surreal_devl-1.0.3 (c (n "surreal_devl") (v "1.0.3") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0") (k 0)))) (h "0f1z4nl2w1hkiib9pjzx2via2q4ns2229cxp58rmgjk4nmz313xx")))

(define-public crate-surreal_devl-1.0.4 (c (n "surreal_devl") (v "1.0.4") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "surrealdb") (r "^1.0.0") (k 0)))) (h "131jiihw1h1v0h9ds1lhnfdg5b5bpkdzsv0sdzvk1spq8f0a2fxr")))

(define-public crate-surreal_devl-1.0.5 (c (n "surreal_devl") (v "1.0.5") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.35") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 2)) (d (n "surrealdb") (r "^1.0.0") (k 0)) (d (n "surrealdb_id") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (d #t) (k 2)))) (h "1f1zmk1sifndy3b833xhs3pyxsirjixvxgdlngsa2aclffl8zyxg")))

