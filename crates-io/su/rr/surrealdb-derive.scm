(define-module (crates-io su rr surrealdb-derive) #:use-module (crates-io))

(define-public crate-surrealdb-derive-0.1.0 (c (n "surrealdb-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0gyb0qrfmdscjsk28y6b6akmfkc4n3q7h9cbhxbyi205dvm7j29n")))

(define-public crate-surrealdb-derive-0.1.1 (c (n "surrealdb-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0y39hw3qga3y4l9ajlxcaq6cj9k6if76ygwn897x4hrg4ab2683i")))

(define-public crate-surrealdb-derive-0.1.2 (c (n "surrealdb-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0j3mfdjmb92678dm8k9xxxy2mdq4ygpkbmrdhvq8a90a0ddswib0")))

(define-public crate-surrealdb-derive-0.2.0 (c (n "surrealdb-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (d #t) (k 0)))) (h "0wzhbzp5dan3gimnhz4jzb5dz6wxkaf0qp7xb49bd7pd76nrd8r2")))

(define-public crate-surrealdb-derive-0.3.0 (c (n "surrealdb-derive") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (d #t) (k 0)))) (h "1n5lc37hdwi3pxyxb12hbmnnakyl6d1f2h8f8a657mpfld1zayg9")))

(define-public crate-surrealdb-derive-0.4.0 (c (n "surrealdb-derive") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (d #t) (k 0)))) (h "1354vfaqzbpzxpi0cw816r83q93cizg85k3knpdzy6v2na0ps3h1")))

(define-public crate-surrealdb-derive-0.5.0 (c (n "surrealdb-derive") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (d #t) (k 0)))) (h "054bnwpsgb4z52g55b8axs6fwlybkvm8knbr9nqr7x29xgwv8asm")))

(define-public crate-surrealdb-derive-0.6.0 (c (n "surrealdb-derive") (v "0.6.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (d #t) (k 0)))) (h "1vz0n5v5ad048v5njh842wz6gsy0kqg8vfvajg84q78b49rr5cyz")))

(define-public crate-surrealdb-derive-0.7.0 (c (n "surrealdb-derive") (v "0.7.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.96") (d #t) (k 0)))) (h "0ayq2mzphppf04d4if2mhqmh1x40yslyz993ry3za5r4w9sjl3qf")))

(define-public crate-surrealdb-derive-0.8.0 (c (n "surrealdb-derive") (v "0.8.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "storekey") (r "^0.5.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "08caidi3ma74lh1nwwr88kkc5cj6br3myn25rdq6cwd91dfdni3z")))

(define-public crate-surrealdb-derive-0.9.0 (c (n "surrealdb-derive") (v "0.9.0") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "storekey") (r "^0.5.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "14l842k5xnjdbm00afdz7pvr3fsnk5zg13d5ql7hyhdc1xaxpyvg")))

(define-public crate-surrealdb-derive-0.10.0 (c (n "surrealdb-derive") (v "0.10.0") (d (list (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "storekey") (r "^0.5.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0ndcgpxzwfrbml0lyyy6ac9vhrs0rjpar99r9bc5qsmg9wyzgicc")))

(define-public crate-surrealdb-derive-0.11.0 (c (n "surrealdb-derive") (v "0.11.0") (d (list (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "storekey") (r "^0.5.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0sqsnlayzd49kjd4sb0qkzgx8xsmfk587wrrsi77736snc3b6a8y")))

(define-public crate-surrealdb-derive-0.12.0 (c (n "surrealdb-derive") (v "0.12.0") (d (list (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "storekey") (r "^0.5.0") (d #t) (k 2)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "1x6x3mpipijjb5wg6qfk21bdh4h0mxiwvbqh2clz1glyig2v9kda")))

