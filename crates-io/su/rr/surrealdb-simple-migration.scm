(define-module (crates-io su rr surrealdb-simple-migration) #:use-module (crates-io))

(define-public crate-surrealdb-simple-migration-0.1.0 (c (n "surrealdb-simple-migration") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.38") (f (quote ("serde"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.203") (d #t) (k 0)) (d (n "surrealdb") (r "^1.5.1") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "193565p5wbsv3q6zzcwj4dq0kwb21dim1z36rdnvx1jvibv3mngw")))

(define-public crate-surrealdb-simple-migration-0.2.0 (c (n "surrealdb-simple-migration") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.38") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.203") (d #t) (k 0)) (d (n "surrealdb") (r "^1.5.1") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06726qczq6ccpfzxmhkk8rj2q74zji1kq01fx5ddgfji5w44ld05")))

(define-public crate-surrealdb-simple-migration-0.2.1 (c (n "surrealdb-simple-migration") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.38") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.203") (d #t) (k 0)) (d (n "surrealdb") (r "^1.5.1") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nf931fsxlmvwzck7hwq337hb60k7q6kpgpnksqvcalc3jl6f3bc")))

