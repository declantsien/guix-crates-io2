(define-module (crates-io su rr surrealdb-extras-proc-macro) #:use-module (crates-io))

(define-public crate-surrealdb-extras-proc-macro-0.1.1 (c (n "surrealdb-extras-proc-macro") (v "0.1.1") (d (list (d (n "deluxe") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1y39nm84dlm9j924jz2wbvnifnjb09fck8991fmpihp1cy5ka2xx")))

(define-public crate-surrealdb-extras-proc-macro-0.1.2 (c (n "surrealdb-extras-proc-macro") (v "0.1.2") (d (list (d (n "deluxe") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0l9r9ygxxa1nr9i59594fbsfyzsl7zj0fzxf7mrzq0yapsi0z50s")))

(define-public crate-surrealdb-extras-proc-macro-0.1.3 (c (n "surrealdb-extras-proc-macro") (v "0.1.3") (d (list (d (n "deluxe") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "05kvgri798m3jsq11vd3613g5j0hm62hhd3clqqz3b4l0mvldyl0")))

