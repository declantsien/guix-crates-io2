(define-module (crates-io su rr surrealdb-http) #:use-module (crates-io))

(define-public crate-surrealdb-http-0.1.0 (c (n "surrealdb-http") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "env") (r "^0.0.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "simplehttp") (r "^0.0.5") (d #t) (k 0)) (d (n "simplehttp") (r "^0.0.5") (f (quote ("reqwest"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1i0bpd6pybsaj6rilr1qa6jwabfp89pz9zkk7f6mhy98xm5m0gmd")))

