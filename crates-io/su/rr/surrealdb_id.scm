(define-module (crates-io su rr surrealdb_id) #:use-module (crates-io))

(define-public crate-surrealdb_id-0.1.0 (c (n "surrealdb_id") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 2)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.196") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "surrealdb") (r "^1.3.0") (f (quote ("kv-mem"))) (k 0)) (d (n "tokio") (r "^1.36.0") (d #t) (k 2)))) (h "0zrdad5mv2fabdp89aq8g2ny7pdpk4ah2sa5dijk5sf48b1dwwmw")))

(define-public crate-surrealdb_id-0.1.1 (c (n "surrealdb_id") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 2)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.196") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "surrealdb") (r "^1.3.0") (f (quote ("kv-mem"))) (k 0)) (d (n "tokio") (r "^1.36.0") (d #t) (k 2)))) (h "0l3v67n8kakdyjj01h4ggh1hjqilwpddqhay62vwrh9ghy4z0w3q")))

(define-public crate-surrealdb_id-0.1.2 (c (n "surrealdb_id") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.35") (d #t) (k 2)) (d (n "serde") (r "^1.0.196") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.196") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "surrealdb") (r "^1.3.0") (k 0)) (d (n "surrealdb") (r "^1.3.0") (f (quote ("kv-mem"))) (k 2)) (d (n "tokio") (r "^1.36.0") (d #t) (k 2)))) (h "0i6l0ky7308cp29gdfw9mxfsx6ijmyq0g4p4zjvw8dy1f993ng73")))

