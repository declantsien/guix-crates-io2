(define-module (crates-io su rf surff) #:use-module (crates-io))

(define-public crate-surff-0.1.0 (c (n "surff") (v "0.1.0") (h "12ik4f6d4z93bp1lvsr6blqzdbazgv7rzvbiq19vmkfk1vakprsl")))

(define-public crate-surff-0.1.1 (c (n "surff") (v "0.1.1") (h "18b2v1srshxsa309i4bzkvl4vacr1wj7c8qqcd9s9g1k98c17klz")))

