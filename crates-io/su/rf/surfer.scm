(define-module (crates-io su rf surfer) #:use-module (crates-io))

(define-public crate-surfer-0.3.0 (c (n "surfer") (v "0.3.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "surfer_macros") (r "^0.1.0") (d #t) (k 0)))) (h "0j15yqqin0ihp877kizs0f5s27q41wagxspr7mk7pfw67dc6zrpd")))

(define-public crate-surfer-0.3.1 (c (n "surfer") (v "0.3.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "surfer_macros") (r "^0.1.0") (d #t) (k 0)))) (h "0sxyrjsbqa13xl8qpaglm2h097iwsvg0j3y4ag0xs1jds2wsaxxv")))

(define-public crate-surfer-0.3.2 (c (n "surfer") (v "0.3.2") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "surfer_macros") (r "^0.1.0") (d #t) (k 0)))) (h "0dv5a1g5s9zzd7j5vqfc4r8skbjp19pf19mknmlpgxq967mcdmjg")))

