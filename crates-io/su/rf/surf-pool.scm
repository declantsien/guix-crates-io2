(define-module (crates-io su rf surf-pool) #:use-module (crates-io))

(define-public crate-surf-pool-0.1.1 (c (n "surf-pool") (v "0.1.1") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "async-weighted-semaphore") (r "^0.2.1") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "surf") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "09zgvfvgn8mwwrccw49ssg0xxig1n9g2a5wal0xx99840znnvgz1")))

(define-public crate-surf-pool-0.1.2 (c (n "surf-pool") (v "0.1.2") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "async-weighted-semaphore") (r "^0.2.1") (d #t) (k 0)) (d (n "futures-lite") (r "^1.12.0") (d #t) (k 2)) (d (n "surf") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "022s6jm45jly8hnhi3lr0rvpmx37rpkd150z8l0449ls0gdm4p04")))

