(define-module (crates-io su rf surf-sse) #:use-module (crates-io))

(define-public crate-surf-sse-1.0.0-alpha.0 (c (n "surf-sse") (v "1.0.0-alpha.0") (d (list (d (n "async-std") (r "^1.1.0") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "sse-codec") (r "^0.3.0") (d #t) (k 0)) (d (n "surf") (r "=2.0.0-alpha.4") (d #t) (k 0)))) (h "1k3sdmkakfv1qzp6zc64v727i0aaq5c72znx4r4ff6nwczh2n7zi")))

(define-public crate-surf-sse-1.0.0 (c (n "surf-sse") (v "1.0.0") (d (list (d (n "async-std") (r "^1.1.0") (d #t) (k 2)) (d (n "futures-core") (r "^0.3.4") (d #t) (k 0)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.4") (k 2)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "sse-codec") (r "^0.3.0") (d #t) (k 0)) (d (n "surf") (r "^2.0.0") (k 0)))) (h "180dpn79kbsppcv58f6c6ci62zqlrsx5g8h5dsz4bdvw2k0cjc0z") (f (quote (("default" "surf/default"))))))

