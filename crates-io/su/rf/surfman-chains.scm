(define-module (crates-io su rf surfman-chains) #:use-module (crates-io))

(define-public crate-surfman-chains-0.1.0 (c (n "surfman-chains") (v "0.1.0") (d (list (d (n "euclid") (r "^0.20") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sparkle") (r "^0.1") (d #t) (k 0)) (d (n "surfman") (r "^0.1.0") (f (quote ("sm-osmesa"))) (d #t) (k 0)))) (h "1i8fwpy5vcr5cd5gdcynw3fnmhzd996gvh0z96k1rk88vbwb4smb")))

(define-public crate-surfman-chains-0.1.1 (c (n "surfman-chains") (v "0.1.1") (d (list (d (n "euclid") (r "^0.20") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sparkle") (r "^0.1") (d #t) (k 0)) (d (n "surfman") (r "^0.1.0") (f (quote ("sm-osmesa"))) (d #t) (k 0)))) (h "0xc1khszpxdqjvkb2y6328wi7wfpq0ck0arhag1d1spsc6hvxdi1")))

(define-public crate-surfman-chains-0.2.0 (c (n "surfman-chains") (v "0.2.0") (d (list (d (n "euclid") (r "^0.20") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sparkle") (r "^0.1") (d #t) (k 0)) (d (n "surfman") (r "^0.1") (f (quote ("sm-osmesa"))) (d #t) (k 0)) (d (n "surfman-chains-api") (r "^0.2") (d #t) (k 0)))) (h "1jwpxj5apb06n3qxmy0wf4xnmjg4b2zyv66db6nlm3sxq6cx7c23")))

(define-public crate-surfman-chains-0.2.1 (c (n "surfman-chains") (v "0.2.1") (d (list (d (n "euclid") (r "^0.20") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sparkle") (r "^0.1") (d #t) (k 0)) (d (n "surfman") (r "^0.1") (f (quote ("sm-osmesa"))) (d #t) (k 0)) (d (n "surfman-chains-api") (r "^0.2") (d #t) (k 0)))) (h "08hfz1igaz72zhm7ziw2k25r8xx5iljbbcvrkrx816i2dfbvbhd2")))

(define-public crate-surfman-chains-0.3.0 (c (n "surfman-chains") (v "0.3.0") (d (list (d (n "euclid") (r "^0.20") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sparkle") (r "^0.1") (d #t) (k 0)) (d (n "surfman") (r "^0.1") (f (quote ("sm-osmesa"))) (d #t) (k 0)) (d (n "surfman-chains-api") (r "^0.2") (d #t) (k 0)))) (h "00bg2x9z9dnqr3axddyl8z0q2syc0jkv3wv26vwvni4npvspk9m2")))

(define-public crate-surfman-chains-0.4.0 (c (n "surfman-chains") (v "0.4.0") (d (list (d (n "euclid") (r "^0.20") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sparkle") (r "^0.1") (d #t) (k 0)) (d (n "surfman") (r "^0.3") (d #t) (k 0)) (d (n "surfman-chains-api") (r "^0.2") (d #t) (k 0)))) (h "1bwpya92ljaviq90z3d5isa9cn3j8r5xrac13fxkvnibfzhrh5fm")))

(define-public crate-surfman-chains-0.5.0 (c (n "surfman-chains") (v "0.5.0") (d (list (d (n "euclid") (r "^0.20") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sparkle") (r "^0.1") (d #t) (k 0)) (d (n "surfman") (r "^0.3") (d #t) (k 0)) (d (n "surfman-chains-api") (r "^0.2") (d #t) (k 0)))) (h "017czkr6a506xr5sz2r1m0gggrq10nnc7najrqk68f19zglhndyp")))

(define-public crate-surfman-chains-0.5.1 (c (n "surfman-chains") (v "0.5.1") (d (list (d (n "euclid") (r "^0.20") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sparkle") (r "^0.1") (d #t) (k 0)) (d (n "surfman") (r "^0.3") (d #t) (k 0)) (d (n "surfman-chains-api") (r "^0.2") (d #t) (k 0)))) (h "1i4fvmd77xyzqmvcnwqad9rsfjv3fibr0ijmi8hj3wpy5fc86p2k")))

(define-public crate-surfman-chains-0.6.0 (c (n "surfman-chains") (v "0.6.0") (d (list (d (n "euclid") (r "^0.20") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sparkle") (r "^0.1") (d #t) (k 0)) (d (n "surfman") (r "^0.4") (d #t) (k 0)) (d (n "surfman-chains-api") (r "^0.2") (d #t) (k 0)))) (h "098adb1ywgmm281ip9gd26kcfp0ha2xalkvgsf03lqli0q4hgrwr")))

(define-public crate-surfman-chains-0.7.0 (c (n "surfman-chains") (v "0.7.0") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sparkle") (r "^0.1") (d #t) (k 0)) (d (n "surfman") (r "^0.6") (d #t) (k 0)) (d (n "surfman-chains-api") (r "^0.2") (d #t) (k 0)))) (h "0yfch63bnnvwirabc00rafh4jb2jr68md6cjxn0wqxgrpxl9s7mm")))

(define-public crate-surfman-chains-0.8.0 (c (n "surfman-chains") (v "0.8.0") (d (list (d (n "euclid") (r "^0.22") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sparkle") (r "^0.1") (d #t) (k 0)) (d (n "surfman") (r "^0.7") (d #t) (k 0)) (d (n "surfman-chains-api") (r "^0.2") (d #t) (k 0)))) (h "1751z5b2fy6zvvkgsg94r714f0g8ns2bmg1lcv6b3dnjd2b2qvw9")))

