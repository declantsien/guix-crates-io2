(define-module (crates-io su rf surf-vcr) #:use-module (crates-io))

(define-public crate-surf-vcr-0.1.0 (c (n "surf-vcr") (v "0.1.0") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("std"))) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("std" "attributes"))) (k 2)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (f (quote ("curl-client"))) (k 0)))) (h "000bd79ncnjqi5qzczk21557qdjkb04x27drigkvp464l5s0ljwc")))

(define-public crate-surf-vcr-0.1.1 (c (n "surf-vcr") (v "0.1.1") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("std"))) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("std" "attributes"))) (k 2)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (f (quote ("curl-client"))) (k 0)))) (h "0z5iv5sb2cv2bxqd67allmv9bngw2jiz33shw93djzv8dbm5lc54")))

(define-public crate-surf-vcr-0.1.2 (c (n "surf-vcr") (v "0.1.2") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("std"))) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("std" "attributes"))) (k 2)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (f (quote ("curl-client"))) (k 0)))) (h "0h3ym16w1xjg5jgqvs9pm7kr6k65azifnpjp243vrm290vawx3i9")))

(define-public crate-surf-vcr-0.2.0 (c (n "surf-vcr") (v "0.2.0") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("std"))) (k 0)) (d (n "async-std") (r "^1.9.0") (f (quote ("std" "attributes"))) (k 2)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.17") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (f (quote ("curl-client"))) (k 0)))) (h "0fmw6rlixvv4q0g8ggbys79skab745bca5fz9a5yz21vhfhqfydv")))

