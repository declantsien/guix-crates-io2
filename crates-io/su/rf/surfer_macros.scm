(define-module (crates-io su rf surfer_macros) #:use-module (crates-io))

(define-public crate-surfer_macros-0.1.0 (c (n "surfer_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jnj4mhvc6cfrba4v7vpji1vligpvjl161x3imr65sx2jdrij2lx")))

