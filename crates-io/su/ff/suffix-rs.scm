(define-module (crates-io su ff suffix-rs) #:use-module (crates-io))

(define-public crate-suffix-rs-0.1.0 (c (n "suffix-rs") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1nl7g3xj27pqsk81fk28025ha5qqjv7gdnlrxc6ggs1nzc4a9nj9")))

