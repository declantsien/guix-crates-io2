(define-module (crates-io su ff suffix) #:use-module (crates-io))

(define-public crate-suffix-0.1.1 (c (n "suffix") (v "0.1.1") (h "028hs7r2wjfsxp4sm4jz1m1dnd69krsvv92jmwc5s56p8wvz3zmi")))

(define-public crate-suffix-0.1.2 (c (n "suffix") (v "0.1.2") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "153l89zpm7ixy19wqyg8ay7vqi79f4p6dl7ifr0jk2hs76pyvzya")))

(define-public crate-suffix-0.1.3 (c (n "suffix") (v "0.1.3") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "05w1f6vmk4gv96zy69ya7cyvd8imqk2kfb6wra9k58d714p9fjbf")))

(define-public crate-suffix-0.1.4 (c (n "suffix") (v "0.1.4") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "06ci51sd9bl6bc2gkbnrhklzs6afj44vm6c97shabyzyhxha56hf")))

(define-public crate-suffix-0.1.5 (c (n "suffix") (v "0.1.5") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0fy1ck468pvby4gafcmgz8da7vwyj5lay51ybhqf02kvr5idh4a6")))

(define-public crate-suffix-0.1.6 (c (n "suffix") (v "0.1.6") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0bxf2r3bq00xhl2xq5886nkjzm520x1jchx71mfw6k55kk358n7x")))

(define-public crate-suffix-0.1.7 (c (n "suffix") (v "0.1.7") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0hd8m870hbqyxh1x8sq5qmbn01zql0iyjj2rj450205mvi1fws64")))

(define-public crate-suffix-0.1.8 (c (n "suffix") (v "0.1.8") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "19fvzmr3qj3cxihlh1rcfz1q78ay6ahy6b96x12wdgzdx5mgd2dq")))

(define-public crate-suffix-0.1.9 (c (n "suffix") (v "0.1.9") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "15y405lzda88wc6bscafajjgidag577xgabdf6wr23i4kkw5jpmf")))

(define-public crate-suffix-0.1.10 (c (n "suffix") (v "0.1.10") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "17ysabvlakjlilpvcragl1c6jn6mkgc3ha894d6kphawycqdibgj")))

(define-public crate-suffix-0.1.11 (c (n "suffix") (v "0.1.11") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1q615rfr1qn1nhg0n25aq9i1y9145pzvdkdc24phv0lz4q1jgscj")))

(define-public crate-suffix-0.1.12 (c (n "suffix") (v "0.1.12") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0h2gv2hmf9vmylx3wnn3nwqf35dc03swxih7hvfsb12jc1phgl2z")))

(define-public crate-suffix-0.1.13 (c (n "suffix") (v "0.1.13") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "03mv71ann2grcs7lpxf2wcdib2mjifas7bpjvzh6hnzsn97j283j")))

(define-public crate-suffix-0.1.14 (c (n "suffix") (v "0.1.14") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0wbbrkd8jsibap8hz76rwlqrn2sjljpk83r8ng7rbqxj96cxm147")))

(define-public crate-suffix-0.1.15 (c (n "suffix") (v "0.1.15") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1izhnf2xw93kyqn4k6q5l4f0na9gnz6a9n9czqvk3mikvq72jjls")))

(define-public crate-suffix-0.1.16 (c (n "suffix") (v "0.1.16") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "12awwj1p37xh2lwfhj16xh4zwani5zascy01bf310v04k3kfj1z2")))

(define-public crate-suffix-0.1.17 (c (n "suffix") (v "0.1.17") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0xab5hnxahkv3xyxdldswk1zf50y9pyf8akjhd9lr0i5wgr6qjch")))

(define-public crate-suffix-0.1.18 (c (n "suffix") (v "0.1.18") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "19xm5lyzasbg8f1zznarpznrmzkpzaj4prk9hmbhjm46zia68rcy")))

(define-public crate-suffix-0.1.19 (c (n "suffix") (v "0.1.19") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1s2ahgl150rm6ph5hl9z4nxysr0ab654p4psy8pkhc3n5081jl38")))

(define-public crate-suffix-0.1.20 (c (n "suffix") (v "0.1.20") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1lq77gz72n8zr0h167r03i62rdj1d77fzn8zpzf54k23iqkr7zk3")))

(define-public crate-suffix-0.1.21 (c (n "suffix") (v "0.1.21") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "12l3i6ydp5iag6cri7rg5h38d0cykkj9jsb3amnqgsbk27r6zqws")))

(define-public crate-suffix-0.1.22 (c (n "suffix") (v "0.1.22") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1lm6fjxnxzdfj2xqpbmyjdb1qn4slzlakqvmzv6ab364r4d8qdsm")))

(define-public crate-suffix-0.1.25 (c (n "suffix") (v "0.1.25") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0rfsq0qlgdhc2x4c08fqyxh4iqq8d8ygm630vhjv97nn8b7gij1q")))

(define-public crate-suffix-0.1.26 (c (n "suffix") (v "0.1.26") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0g4fmbi36xwgryyvs0cx2k14am8inc5vbbxrz0b9n8mqf0lax6qm")))

(define-public crate-suffix-0.1.27 (c (n "suffix") (v "0.1.27") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0hsk7bbmrjn821li15slbwlkppbnnsgmlw5myr09a0y99as2ijm6")))

(define-public crate-suffix-0.1.28 (c (n "suffix") (v "0.1.28") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1zf2ax5sbsipwsjfhisnl6dhrbwqq5lr0837356qbswpjlgsy7zw")))

(define-public crate-suffix-0.1.29 (c (n "suffix") (v "0.1.29") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "19sdsnibb0jxg5k0px85vwvk03lkykjwlsz8915yc3wvq211nckg")))

(define-public crate-suffix-0.1.30 (c (n "suffix") (v "0.1.30") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "11h5wqmj3b7i7xffwcx7pkh1in8jyz3qfaz3l3p31s9gqsh7f0r9")))

(define-public crate-suffix-0.1.31 (c (n "suffix") (v "0.1.31") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0lcwd84mrysnl3krf5qhqw1aflywz87p6kdj06n9g5mg8pg0vyw3")))

(define-public crate-suffix-0.1.32 (c (n "suffix") (v "0.1.32") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0mc1wnyqgziywgagdg43hdhzlg8a3850yh2c87pb5qw7iscnqmld")))

(define-public crate-suffix-0.1.33 (c (n "suffix") (v "0.1.33") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "18fr2j9nrla16p6ydd5v6i7y5kjmpkxvyfm44f8fqlw0pk1j82s5")))

(define-public crate-suffix-0.1.34 (c (n "suffix") (v "0.1.34") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "12vabl5dd33v3ajknrqfdvs5qcshv9i53nrfl1jgi5ddb6fix98g")))

(define-public crate-suffix-0.1.35 (c (n "suffix") (v "0.1.35") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "1vy741l18bcn16abkmd275v84da4dkk6fbx0v5cg2cvhpal93k8l")))

(define-public crate-suffix-0.2.0 (c (n "suffix") (v "0.2.0") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0hbh0bj7m7bjjaz0mg334jv1rafg74l134x41k0s3rdq442bxn2j")))

(define-public crate-suffix-0.2.1 (c (n "suffix") (v "0.2.1") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "1wx3aas5270c6llx0chd7r6iyifx7i13csa67lp0bpjl67wfr27v")))

(define-public crate-suffix-0.2.2 (c (n "suffix") (v "0.2.2") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0idzkwk3709shd2j5zlfs5nbl8r30jy52n70lihszmwgpynhqswq")))

(define-public crate-suffix-0.2.3 (c (n "suffix") (v "0.2.3") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "1zwjppzg20c3f1xa1lg5g5hi8x0dwhdwc03sdwqfxg117alvrf7i")))

(define-public crate-suffix-0.2.4 (c (n "suffix") (v "0.2.4") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "1r83ivbjhk7s7q25hbz1i3f05yyv8x70m42ppr239ys70jmi2svn")))

(define-public crate-suffix-0.2.5 (c (n "suffix") (v "0.2.5") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "1rl2xfqd63law4s2ignbzlmmcpr07zhjj7hkg5xrkkysnyyp2wr4")))

(define-public crate-suffix-0.2.6 (c (n "suffix") (v "0.2.6") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "1ykbdiy4hmc1k14d5y78g9ipqpwrvlvyn9xkmqaw2ma05pdzah6s")))

(define-public crate-suffix-0.2.7 (c (n "suffix") (v "0.2.7") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0m1hnskdjk80vbwpsrjisjd85nnc01n4vf00746w225g4rkbbjs6")))

(define-public crate-suffix-0.2.8 (c (n "suffix") (v "0.2.8") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0j4pxxw79mzf033nflim9gxql8c112a38dh34x907x4x7y2szwlf")))

(define-public crate-suffix-0.2.9 (c (n "suffix") (v "0.2.9") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "0y2hjbn4vq8pvw5pza3ab80xllbjfp6vzjwnb76hjbhnf8pgcy1y")))

(define-public crate-suffix-0.2.10 (c (n "suffix") (v "0.2.10") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "134fb3sl3337ij3v04lbgy1mn865bhgywr2hja1xr40mgaii3ipf")))

(define-public crate-suffix-0.3.0 (c (n "suffix") (v "0.3.0") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "1zy6sdn0my0jbzzab8iiqqaii49hqapv0s7lrnb1bsvqcx231xhs")))

(define-public crate-suffix-0.3.1 (c (n "suffix") (v "0.3.1") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "118ji98bl9ic85ajadfyxq12rxq2rx15m29bd1v3k3xyhfd8cny8")))

(define-public crate-suffix-0.3.2 (c (n "suffix") (v "0.3.2") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "0la4qxaf8hinz4ypg14kjangz381crgfwls4mh7lb5zj051ff83f")))

(define-public crate-suffix-0.4.0 (c (n "suffix") (v "0.4.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "005qvgs4m4b0yyf7kryxc663gcrjkhyiz8y6wr1s8gbi1h1rfndi")))

(define-public crate-suffix-0.4.1 (c (n "suffix") (v "0.4.1") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)))) (h "00c0xch8bx9w6922kwj39agdaqdykrazysll3m1bw16zpdzf0jrh")))

(define-public crate-suffix-1.0.0 (c (n "suffix") (v "1.0.0") (d (list (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "0fgr6i01l86nn1q6zhgc4hvkx84xg80prbmg71j814g184sb8w04")))

(define-public crate-suffix-1.1.0 (c (n "suffix") (v "1.1.0") (d (list (d (n "quickcheck") (r "^0.7") (k 2)))) (h "08fk2x8ykbgib0fb5qxzra7mdrp2lzspbwjdd343nmydxiqq1ksv")))

(define-public crate-suffix-1.2.0 (c (n "suffix") (v "1.2.0") (d (list (d (n "quickcheck") (r "^0.9") (k 2)))) (h "0k91a8ng2sdis638v2kkq3myyjwg13mzzikyxf1k822s0ks4nb45")))

(define-public crate-suffix-1.3.0 (c (n "suffix") (v "1.3.0") (d (list (d (n "quickcheck") (r "^0.9") (k 2)))) (h "16a10mhkcqv1pykwbbjgiyxr56s90390wsf6v454jrjbp2wk91w8")))

