(define-module (crates-io su ff suffix_tree) #:use-module (crates-io))

(define-public crate-suffix_tree-0.1.0 (c (n "suffix_tree") (v "0.1.0") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "suffix") (r "*") (d #t) (k 0)))) (h "1sdddphzaj5ppwph14aqj7h5m85wy2bi12d0cnsg0b2ipx8npka4")))

(define-public crate-suffix_tree-0.2.0 (c (n "suffix_tree") (v "0.2.0") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "suffix") (r "*") (d #t) (k 0)))) (h "06si9rfffybhlisv2yfwh74h63qwg6fra49j2k4jpsbzvz1faymj")))

(define-public crate-suffix_tree-0.2.1 (c (n "suffix_tree") (v "0.2.1") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "suffix") (r "^0.3") (d #t) (k 0)))) (h "1pzd3vw1ihfm5qiblcml0ccpibq2hmh7ja42jbn092ph5f2vmfsf")))

(define-public crate-suffix_tree-0.2.2 (c (n "suffix_tree") (v "0.2.2") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "suffix") (r "^0.4") (d #t) (k 0)))) (h "1x3i56f8xwqplcf7h2pzzcyf6jscxyhwp2mlzq1pk0cggr1dnvl2")))

