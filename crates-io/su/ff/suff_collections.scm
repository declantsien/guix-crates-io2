(define-module (crates-io su ff suff_collections) #:use-module (crates-io))

(define-public crate-suff_collections-0.1.1 (c (n "suff_collections") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "1a2axd72032hncg6hmyzxyk9lyjsp8fb6l072pccnig1jfg89dw0")))

(define-public crate-suff_collections-0.1.2 (c (n "suff_collections") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "12crgy4isp0v1i26z9nj8dgy857p0vbirfkbys01nb7i1y6508w7")))

(define-public crate-suff_collections-0.1.3 (c (n "suff_collections") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "1riwq83hg3fw1bm2y2wj17ksphg6r2airivxx8q8f6rabh7imhaj")))

(define-public crate-suff_collections-1.0.0 (c (n "suff_collections") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "0a0xx853zhx5asm0bq6xabp1xcdmxh53lg11dfyn2fiqir1yp386")))

(define-public crate-suff_collections-1.1.0 (c (n "suff_collections") (v "1.1.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "1gklw3n5w3nmjk95dsw977zdrb7qivjgnmmfksd5mymkg3mnhf11")))

(define-public crate-suff_collections-1.1.1 (c (n "suff_collections") (v "1.1.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "0q5zph7ywm36blkar9iw2gsav3pxaqq2xfm8dcy0mvjl19pf1lbg")))

(define-public crate-suff_collections-1.2.0 (c (n "suff_collections") (v "1.2.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "1awfn3brz80ajdd16dgjpjmlpqzzw1r1p6l9hwkqsp30fb3pxnl5")))

(define-public crate-suff_collections-1.2.1 (c (n "suff_collections") (v "1.2.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "121mr8kiy1xap4g746hi4347qdrxnr61zgn2j1m6a219a0j485wx")))

(define-public crate-suff_collections-2.0.0 (c (n "suff_collections") (v "2.0.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "00h6ligh6873pwd4fq4yhwgkff7f1r611k7rvz4smvsvfk3fxarn")))

