(define-module (crates-io su ff suffix_cmd) #:use-module (crates-io))

(define-public crate-suffix_cmd-0.1.33 (c (n "suffix_cmd") (v "0.1.33") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "= 0.2.13") (d #t) (k 0)) (d (n "suffix") (r "*") (d #t) (k 0)))) (h "09zgkjd9l34c4rkpybbyvkam1ndspfx9xq75x72sbgkr8br0w9z4")))

