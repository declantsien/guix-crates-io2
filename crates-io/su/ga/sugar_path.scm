(define-module (crates-io su ga sugar_path) #:use-module (crates-io))

(define-public crate-sugar_path-0.0.1 (c (n "sugar_path") (v "0.0.1") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "1r98gn52xil7j6nn9wamh13shc1b2xx6hcahnqlks9d63rbj93sg")))

(define-public crate-sugar_path-0.0.3 (c (n "sugar_path") (v "0.0.3") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "1pp521vid2d5b2kp91qg1lqsg2zqg5z8hqlvgr1kf3v7ps667a1k")))

(define-public crate-sugar_path-0.0.4 (c (n "sugar_path") (v "0.0.4") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "1w8rpkd83b29zlzjxyysv1xhma2q1nwvhqd0ig7hd2dw60m3d8gw")))

(define-public crate-sugar_path-0.0.5 (c (n "sugar_path") (v "0.0.5") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "01k6as56sh9wrpkja25kfdhn5lypfxn2zv54vq7nx01jac4xq1fh")))

(define-public crate-sugar_path-0.0.6 (c (n "sugar_path") (v "0.0.6") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "13xj295qzr5rv25z988gdrjvnxa1mpfxgac7224lkpq3zsp4sbp4")))

(define-public crate-sugar_path-0.0.7 (c (n "sugar_path") (v "0.0.7") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "1aljgmq35qxrw4lbi73rjlahj4vam6hkq340ihyfqwvx4qd9zapi")))

(define-public crate-sugar_path-0.0.8 (c (n "sugar_path") (v "0.0.8") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "1vd63j73i6rbpfzd8q71vkhjw6nica687bg8qjpyd144fiz2dlbm")))

(define-public crate-sugar_path-0.0.9 (c (n "sugar_path") (v "0.0.9") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "1lrmim56fikrgababd6dpf7lm983f7gc8iz4ig9g766sw414r17f")))

(define-public crate-sugar_path-0.0.10 (c (n "sugar_path") (v "0.0.10") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "0lk9yv5p18svi91dyyvadcd10mzkvpfgjymwn9h7p0j68rjj9sh1")))

(define-public crate-sugar_path-0.0.11 (c (n "sugar_path") (v "0.0.11") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "0k8svpxsfs20w2zr1sj482msp6vcp2k83frdry9a6hhks4dnfivx")))

(define-public crate-sugar_path-0.0.12 (c (n "sugar_path") (v "0.0.12") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)))) (h "1ffp1d8k8agkc81f8za0np76xb4dkkvga8b72cvcjpqnqnias8rc")))

(define-public crate-sugar_path-1.0.0 (c (n "sugar_path") (v "1.0.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "1j0fwx9prxhjra0p1xhcf6f6x59smxwb136djak6hqpa53f5w6yl")))

(define-public crate-sugar_path-1.1.0 (c (n "sugar_path") (v "1.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "1rh4miiwgdjkcnx4yrm6h0f193hv18fm399zh23l5ri1cgcmj12l") (f (quote (("cached_current_dir"))))))

(define-public crate-sugar_path-1.2.0 (c (n "sugar_path") (v "1.2.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "1kjpkgb4a28a76d6hac3idx49fhjjgfxps2ygr54svaslswdac42") (f (quote (("cached_current_dir"))))))

