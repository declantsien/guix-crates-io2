(define-module (crates-io su ga sugar) #:use-module (crates-io))

(define-public crate-sugar-0.1.0 (c (n "sugar") (v "0.1.0") (h "08sx7cl7qbkmlcncdrcwrjfm32j2fj76ldcrw9h7kq59rb9dkk7z")))

(define-public crate-sugar-0.2.0 (c (n "sugar") (v "0.2.0") (d (list (d (n "cute") (r "^0.3.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "min_max_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "vec_box") (r "^1.0.0") (d #t) (k 0)))) (h "1h927qg1wyf1ih3psqy1b3p3m5bxs7y8m8fr4xf80nl211zkjsk1")))

