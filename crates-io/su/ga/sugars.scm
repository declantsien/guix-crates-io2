(define-module (crates-io su ga sugars) #:use-module (crates-io))

(define-public crate-sugars-0.1.0 (c (n "sugars") (v "0.1.0") (h "15vqhlc3c7p5g2b2qj572yag6ll2v0d1c1xkx9rsgmsgdbid0jfd") (f (quote (("nightly"))))))

(define-public crate-sugars-0.2.0 (c (n "sugars") (v "0.2.0") (h "013cq1px08mxn6ks8mn1mq2zvp88fdpza5ld0xfzslg56pwsw7p3") (f (quote (("nightly"))))))

(define-public crate-sugars-0.3.0 (c (n "sugars") (v "0.3.0") (h "05mhimrxkfi4vd97sp42fx2hcgzbabjd48gcylyv37050xkggpri") (f (quote (("nightly"))))))

(define-public crate-sugars-0.4.0 (c (n "sugars") (v "0.4.0") (h "0jll0b55gr2si7ddsx0dl6jsxghvsd6qm6szi8bhyprnnqlrr27l") (f (quote (("nightly"))))))

(define-public crate-sugars-0.5.0 (c (n "sugars") (v "0.5.0") (h "1cvv5nji9qksrhdl9bazc31mbf389x9nikvyx9m3fh5dfqhh4sps") (f (quote (("nightly"))))))

(define-public crate-sugars-1.0.0 (c (n "sugars") (v "1.0.0") (h "0a8km6jl1dmmxg2m50pdsisgcyk2qvy8rcdap062b5r0bhxg323k") (f (quote (("nightly"))))))

(define-public crate-sugars-1.1.0 (c (n "sugars") (v "1.1.0") (h "092ahyxh18zpsaw4xvhqrw5klrakcpwf58g9hd5qqzw6pnbr837i")))

(define-public crate-sugars-1.2.0 (c (n "sugars") (v "1.2.0") (h "05wkixf0wyqac2nkyq16hkcm1mpg01v25n6hc0ajvycqylrddyiq")))

(define-public crate-sugars-2.0.0 (c (n "sugars") (v "2.0.0") (h "1hss68z1v9y29rczy82vi549qn3i2y3wmr0fjiwwcg8lswr0gv21")))

(define-public crate-sugars-3.0.0 (c (n "sugars") (v "3.0.0") (h "0xhaiz70pfbxg8hd6hvmsnwypj80yzcip9bilnvxyhx6z1rdj19l")))

(define-public crate-sugars-3.0.1 (c (n "sugars") (v "3.0.1") (h "0yp6wf1rbzzf6cyin18ny0i7030ignyn199is0wy01p7kr7vf3fc")))

