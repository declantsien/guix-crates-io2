(define-module (crates-io su bu subunit) #:use-module (crates-io))

(define-public crate-subunit-0.2.1 (c (n "subunit") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3") (d #t) (k 0)))) (h "0sv83d9h0h6f86fpycnx8kjgnm2k7lh6lvrlvvvnk46hr09bhwiq")))

