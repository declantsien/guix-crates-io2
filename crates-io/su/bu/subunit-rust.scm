(define-module (crates-io su bu subunit-rust) #:use-module (crates-io))

(define-public crate-subunit-rust-0.1.0 (c (n "subunit-rust") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)))) (h "1x6g671wvwabxn8j8djg1dfg5xdrigmach0yp5izas7wvcv3p024")))

(define-public crate-subunit-rust-0.1.1 (c (n "subunit-rust") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)))) (h "17b19v4l321jp2nz0rqizqy9c9cbp97clfq1i80fw5sd4wi31g02")))

(define-public crate-subunit-rust-0.1.2 (c (n "subunit-rust") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)))) (h "1p3cyfzgb66acpbwymik3378r482w8v944p89pnn4s0adwm4l8ic")))

(define-public crate-subunit-rust-0.1.3 (c (n "subunit-rust") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "crc") (r "^1.0.0") (d #t) (k 0)))) (h "12ms0g8xyc6vdhnlx4x6agcrnybn2r0wf143n9bmb4014jxlxh8d")))

(define-public crate-subunit-rust-0.2.0 (c (n "subunit-rust") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "crc32fast") (r "^1.3") (d #t) (k 0)))) (h "1rqmrjq1ls4v5hndfcjpyj9y26vqnbn0vb2xz232kz86zr4vz36r")))

