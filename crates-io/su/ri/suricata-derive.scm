(define-module (crates-io su ri suricata-derive) #:use-module (crates-io))

(define-public crate-suricata-derive-7.0.0-alpha.2 (c (n "suricata-derive") (v "7.0.0-alpha.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1m78jg3pcicxdrn030rfnyd19f1hp9jla5mrpagiv6x0h837jkf9") (y #t)))

(define-public crate-suricata-derive-7.0.0-alpha.3 (c (n "suricata-derive") (v "7.0.0-alpha.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1g0xnq79nsgg5dpqzymh6c0nsncxz1y6mmx98rcn6xp2zmbjlng4") (y #t)))

(define-public crate-suricata-derive-7.0.0-alpha.4 (c (n "suricata-derive") (v "7.0.0-alpha.4") (d (list (d (n "proc-macro-crate") (r "=1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0900hvlwj78n6q0c1hii40zw09iyixjzpk5spmyznlg7p0hykfwp") (y #t)))

(define-public crate-suricata-derive-7.0.0-alpha.5 (c (n "suricata-derive") (v "7.0.0-alpha.5") (d (list (d (n "proc-macro-crate") (r "=1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18c6ridw5r967i966w4fywhi3ypnbzy40i2lkqkjs27668byff7i") (y #t)))

(define-public crate-suricata-derive-8.0.0-alpha.1 (c (n "suricata-derive") (v "8.0.0-alpha.1") (d (list (d (n "proc-macro-crate") (r "=1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04y354alzjq7nn0c4xq8na5dlsn7352y9aqsi1z4dj18d5r2rvgd")))

