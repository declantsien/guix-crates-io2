(define-module (crates-io su ri suricatasc) #:use-module (crates-io))

(define-public crate-suricatasc-0.1.0-alpha.1 (c (n "suricatasc") (v "0.1.0-alpha.1") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "suricata_client") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0fgd030hfikaq7ljc2lcyjpx1l2k72kmv02qvwdg6f3c5mrp9vdj") (y #t)))

(define-public crate-suricatasc-0.1.0-alpha.2 (c (n "suricatasc") (v "0.1.0-alpha.2") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "suricata_client") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0ra6vr0m6626sapcqs7379h0q7gxh2w96h852k01v5bh2s1i8fx6")))

