(define-module (crates-io su ri suricata_client) #:use-module (crates-io))

(define-public crate-suricata_client-0.1.0-alpha.1 (c (n "suricata_client") (v "0.1.0-alpha.1") (d (list (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1acplzgw6ndlg37qfd6dggdmm5s4c117bjf63zmbrd7j1lza949h") (y #t)))

(define-public crate-suricata_client-0.1.0-alpha.2 (c (n "suricata_client") (v "0.1.0-alpha.2") (d (list (d (n "serde") (r "^1.0.132") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "05lds7fr2wd2cgylapi47qdnjqv0p31f015n4idz2vj4rimskw7c")))

