(define-module (crates-io su ed sued) #:use-module (crates-io))

(define-public crate-sued-0.2.0 (c (n "sued") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0c6m777yxrbjwzkz5vr8a6z8lc0plrz7zxb3wik0k5phwx44icj1")))

(define-public crate-sued-0.2.1 (c (n "sued") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "152592l1ik903n96s6v8xh0ks01dvsp2dvkl6az6ld09igv5gzmh")))

(define-public crate-sued-0.2.2 (c (n "sued") (v "0.2.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1h09xmf04b8fadgkjdsnlcwszp77a89garixhi4v3qbgckqipvn9")))

(define-public crate-sued-0.3.0 (c (n "sued") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0sy8wd7blx9gmhg8jl3gzlk07grf81xlhkypzp2kry6bmipdg2hh")))

(define-public crate-sued-0.3.1 (c (n "sued") (v "0.3.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "195qjvbpn955479xprafk4mvgvvls85la15cc1jhiwpzqw47qcn2")))

(define-public crate-sued-0.3.2 (c (n "sued") (v "0.3.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "16wni5m590fxm3q4rfm52f4iivijnpsd2v4f3ynyfv6m8bm9y33v")))

(define-public crate-sued-0.4.0 (c (n "sued") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0qfam6x0bz9sp7km6zyxwjxrcw1838bf2b85sqhnhw379kychfgy")))

(define-public crate-sued-0.5.0 (c (n "sued") (v "0.5.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1g7q6p92bj9vkfdykgd43cmaa8rz0fzvqz19rf0kavl80zw7px2n")))

(define-public crate-sued-0.6.1 (c (n "sued") (v "0.6.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0bz2h0ybxwz1fkhmxwcmqaxgz39vgs1jqpjqjhb8dqq0df7kvn7n")))

(define-public crate-sued-0.6.2 (c (n "sued") (v "0.6.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "143j79j6xmc9ic8nw0cv2i04ia6x782vg9ilyi8impgah9w3822s")))

(define-public crate-sued-0.7.0 (c (n "sued") (v "0.7.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1gycgbaisnzb9rqp2205v8876g8n9zyngxdkni6qsl6ax4c72d98")))

(define-public crate-sued-0.8.0 (c (n "sued") (v "0.8.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1d6v3r1s6zsmrk2kwjqdkbchkgjrqah1i8crz03a2ja4lckl9q45")))

(define-public crate-sued-0.9.0 (c (n "sued") (v "0.9.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1bc75zgly87psm8ww9gqcd5bcjihvv5zxlj2m7qsf2arx7qjzchb")))

(define-public crate-sued-0.9.1 (c (n "sued") (v "0.9.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "064qg3hrsavzi9hpx6f79dj3za68vqw175y1zyig4xil7696csa0")))

(define-public crate-sued-0.9.2 (c (n "sued") (v "0.9.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1babz1bgdn5pfbvdn7pnl63klp5h0an8zj184hiw3v32pw3394vn")))

(define-public crate-sued-0.9.3 (c (n "sued") (v "0.9.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1g83s04ifb13y2xq5cvld1n9pb55hnwjwwbz8nlssqnqlkhgzjdk")))

(define-public crate-sued-0.9.4 (c (n "sued") (v "0.9.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1mmbdk28wk7gx442qsibm25gqwk4wmgr89zpvym9djmc8mmgw9yi")))

(define-public crate-sued-0.10.0 (c (n "sued") (v "0.10.0") (d (list (d (n "copypasta") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "18ld4ln3wf2lwknx5v6c3631xp1z22dhmw9qzm6jl0rq81ypg0wd")))

(define-public crate-sued-0.10.1 (c (n "sued") (v "0.10.1") (d (list (d (n "copypasta") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "09ah9dnhv3bn7iif7r919i0xprbf5b53icqyc7i3h345dj65m31l")))

(define-public crate-sued-0.10.2 (c (n "sued") (v "0.10.2") (d (list (d (n "copypasta") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "01c09b0krsq8jd1qk7x9z50cr6nw5hmkj1d5kxab8mhb9kmq3v9j")))

(define-public crate-sued-0.10.4 (c (n "sued") (v "0.10.4") (d (list (d (n "copypasta") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0z74d6h0d83pwwl8b4hr9qgk2bbanfg13hb7kiqpsb4klrr5alc5")))

(define-public crate-sued-0.10.5 (c (n "sued") (v "0.10.5") (d (list (d (n "copypasta") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "00s1i3p4v7ybl11mzqivfff4qilyvigp0fpw7gzl534gcyyrb8a5")))

(define-public crate-sued-0.10.6 (c (n "sued") (v "0.10.6") (d (list (d (n "copypasta") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0rjg7gxvc3kzxc0ivmjd3g3vlzfc7d7dshryhs4wb4jinkjmm730")))

(define-public crate-sued-0.10.7 (c (n "sued") (v "0.10.7") (d (list (d (n "copypasta") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1vr7gc5frpxfd282664vk84gp42dg734jhbsszm3il5m1xdxsrkg")))

(define-public crate-sued-0.11.0 (c (n "sued") (v "0.11.0") (d (list (d (n "copypasta") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1fl5q69xmv76z7q4bx05cwcpfpazlgyl0w5dpaf0r6lqnx2hc0c5")))

(define-public crate-sued-0.11.1 (c (n "sued") (v "0.11.1") (d (list (d (n "copypasta") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1p8crkwivrc98c0p81ip5p0506qkkfbcrhcrn4wfjx0maghiv3ja")))

(define-public crate-sued-0.11.2 (c (n "sued") (v "0.11.2") (d (list (d (n "arboard") (r "^3.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0dnvhdbs1lm7jsgc67cb8l8k50iyxh76h7xpvlq9cagawhbb8x75")))

(define-public crate-sued-0.11.3 (c (n "sued") (v "0.11.3") (d (list (d (n "copypasta") (r "^0.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0visw7mynfkad6x1zgwl6rmq71wdscvs59af3ik2wwxv2cm9p6w5")))

(define-public crate-sued-0.12.0 (c (n "sued") (v "0.12.0") (d (list (d (n "copypasta") (r "^0.8.2") (d #t) (k 0)) (d (n "linefeed") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1rrd1yv0xkmf5v8n52fqbdnmvm8dfapjkhxizmg286vvcw6hd9lh")))

(define-public crate-sued-0.13.0 (c (n "sued") (v "0.13.0") (d (list (d (n "copypasta") (r "^0.8.2") (d #t) (k 0)) (d (n "linefeed") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0hnn5fvfdvnacz8q0a9k26lds6dfbc711x2r1b08qik9awpibh41")))

(define-public crate-sued-0.13.1 (c (n "sued") (v "0.13.1") (d (list (d (n "copypasta") (r "^0.8.2") (d #t) (k 0)) (d (n "linefeed") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0vyj5ldm9vfnpi7187mf3m99w323jqqba2v63qcci8j1nvnapa7i")))

(define-public crate-sued-0.13.2 (c (n "sued") (v "0.13.2") (d (list (d (n "copypasta") (r "^0.8.2") (d #t) (k 0)) (d (n "linefeed") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0kr8l4z3dj9j2pkxbfvgqwg8vcdh6vjr8hqaqg9d98m7lx728nj3")))

(define-public crate-sued-0.13.3 (c (n "sued") (v "0.13.3") (d (list (d (n "copypasta") (r "^0.8.2") (d #t) (k 0)) (d (n "linefeed") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1fwkjqkgw4fbbphvbrh052p845wf9994hplcb3kc4174m4vfb0sh")))

(define-public crate-sued-0.13.4 (c (n "sued") (v "0.13.4") (d (list (d (n "copypasta") (r "^0.8.2") (d #t) (k 0)) (d (n "linefeed") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0x7qgq3cih9i800v7ryvvsmk1dx49xvppbxx34h33snffpyvb3ry")))

(define-public crate-sued-0.13.5 (c (n "sued") (v "0.13.5") (d (list (d (n "copypasta") (r "^0.8.2") (d #t) (k 0)) (d (n "linefeed") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "070mnx4nyjd72xcqmmz55pig6v45l2zl5i128ny65wfy6nc900bx")))

(define-public crate-sued-0.13.6 (c (n "sued") (v "0.13.6") (d (list (d (n "copypasta") (r "^0.8.2") (d #t) (k 0)) (d (n "linefeed") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1hbm4jrslmx375jy8ky8m34k8462d9b96x9jhigpa5xprz49x69d")))

(define-public crate-sued-0.14.0 (c (n "sued") (v "0.14.0") (d (list (d (n "copypasta") (r "^0.8.2") (d #t) (k 0)) (d (n "linefeed") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1qqvfi2znmazbabzsfp3mb7a4m4qf4yan677vms4c9zjc1g9ybys")))

(define-public crate-sued-0.15.0 (c (n "sued") (v "0.15.0") (d (list (d (n "copypasta") (r "^0.8.2") (d #t) (k 0)) (d (n "linefeed") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1nqy7943dahjq483kw4npvs5dh29xxn6zikrfgpcm8grnp9nhj39")))

(define-public crate-sued-0.16.0 (c (n "sued") (v "0.16.0") (d (list (d (n "copypasta") (r "^0.8.2") (d #t) (k 0)) (d (n "linefeed") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0w86r3illqghlm5abf2b34aiilwhp57ai1nybpiqx7bgd9sk14bb")))

(define-public crate-sued-0.16.1 (c (n "sued") (v "0.16.1") (d (list (d (n "copypasta") (r "^0.8.2") (d #t) (k 0)) (d (n "linefeed") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "03j0l0pn9ng8g11n139kf0jgw8qj73l8s28i28d7blh21y2wmc4a")))

