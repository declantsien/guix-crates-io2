(define-module (crates-io su ln suln) #:use-module (crates-io))

(define-public crate-suln-0.1.0 (c (n "suln") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1miz5d4mzrhpv1xjxzx2nc15fz85c4b6z0lryi9i6azycczkdamd")))

(define-public crate-suln-0.1.1 (c (n "suln") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1k462qdqccmh841jgifk652bs113kbsgp7i9yrhbmwr6wiyryxnj")))

(define-public crate-suln-0.1.2 (c (n "suln") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1hpqd571xzynm0vzhsrmrsc316bs0wa3h3wd729398d1m16wfirp")))

