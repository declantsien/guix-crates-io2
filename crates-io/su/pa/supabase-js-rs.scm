(define-module (crates-io su pa supabase-js-rs) #:use-module (crates-io))

(define-public crate-supabase-js-rs-0.1.0 (c (n "supabase-js-rs") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.33") (d #t) (k 0)))) (h "03fmkw6xdhrh8c19ikc3czarbf2ycvl7f7pkgnm0d5x86301vlkf")))

(define-public crate-supabase-js-rs-0.1.1 (c (n "supabase-js-rs") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.33") (d #t) (k 0)))) (h "0q2bx0bh8hb6lg3h4fj8ymfw7kx5hxiigw337nih6h2r6crh1na4")))

(define-public crate-supabase-js-rs-0.1.2 (c (n "supabase-js-rs") (v "0.1.2") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.33") (d #t) (k 0)))) (h "1jdpiqdaq1bz96q17a4l7yfxb9gdfzhklgs8lp87k1hjmwqms67i")))

(define-public crate-supabase-js-rs-0.1.3 (c (n "supabase-js-rs") (v "0.1.3") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.33") (d #t) (k 0)))) (h "09fwja0l5rc8al2vii9lfi0rdyim1iazjlms25pjldg1r9j25lh2")))

