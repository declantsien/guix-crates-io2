(define-module (crates-io su pa supabase-rust) #:use-module (crates-io))

(define-public crate-supabase-rust-0.1.0 (c (n "supabase-rust") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "17yyzd29xpalc2z7d6r47dgh3qfwaxr0nk1a7k0ak8imp60jvfpb")))

(define-public crate-supabase-rust-0.1.1 (c (n "supabase-rust") (v "0.1.1") (d (list (d (n "jsonwebtoken") (r "^8.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0lwmr6c467qk6rygmz2znpjc8miagjzjnjy4hm9990i6xbvzkm09")))

(define-public crate-supabase-rust-0.1.2 (c (n "supabase-rust") (v "0.1.2") (d (list (d (n "jsonwebtoken") (r "^8.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0p8phdxz49cx1i8dxbkpz8j38w4ai49l0mc4jy6l0l1bp7231d2x")))

