(define-module (crates-io su pa supabase-wrappers-macros) #:use-module (crates-io))

(define-public crate-supabase-wrappers-macros-0.1.0 (c (n "supabase-wrappers-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "01c4r46bpjrbkc9aga2cah8jakrnsacw1bb313zqq7xmc4gbyaxs") (y #t)))

(define-public crate-supabase-wrappers-macros-0.1.1 (c (n "supabase-wrappers-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17551pjiw9kbxzy76sq7kzc6vkm85f3ignms5cnvmpnc9lyjjkk4") (y #t)))

(define-public crate-supabase-wrappers-macros-0.1.2 (c (n "supabase-wrappers-macros") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pvmyl0ppp577w9rq7m2imz68v1misv9b3xygxhggcjgyxrm4d5a") (y #t)))

(define-public crate-supabase-wrappers-macros-0.1.3 (c (n "supabase-wrappers-macros") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "09p008m9i8ia30q37989lklh9pq3vr2lci04acmikspbv9v4pfiw") (y #t)))

(define-public crate-supabase-wrappers-macros-0.1.4 (c (n "supabase-wrappers-macros") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1nqb9nky4f6s97618llz5bxg8yaa5li602vfr5y4dd7jay6g9dr3") (y #t)))

(define-public crate-supabase-wrappers-macros-0.1.5 (c (n "supabase-wrappers-macros") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0m5nbhynz7zsndq589mmf8jhdvmz65myaxafazr3fvjjb9lrva35") (y #t)))

(define-public crate-supabase-wrappers-macros-0.1.6 (c (n "supabase-wrappers-macros") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wk4wvkwbv7y6xaiq03iaxd2hm0jx8ncy9ppd4qpgya8kc1p0px5")))

(define-public crate-supabase-wrappers-macros-0.1.7 (c (n "supabase-wrappers-macros") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vhzq6cglkazcbgdgcab5sfib3fmd5afwvcgvxhf1k92h43lw879")))

(define-public crate-supabase-wrappers-macros-0.1.8 (c (n "supabase-wrappers-macros") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00ayfhb36gqlmp4ymbkpwhw4dfj8c7g6i539d8q9b9g56w6is173")))

(define-public crate-supabase-wrappers-macros-0.1.9 (c (n "supabase-wrappers-macros") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0n57q2h5p529g4n95mw4fx7l5x26n8a97b3b2vaz4hakxpn861hp")))

(define-public crate-supabase-wrappers-macros-0.1.10 (c (n "supabase-wrappers-macros") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0dz7gw1v3dby6ccxwkr950i002a9zv50hgcdnhccpjdnz08rvssj")))

(define-public crate-supabase-wrappers-macros-0.1.11 (c (n "supabase-wrappers-macros") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06f13rjnvqa7wlbg0w8n11xx0h7ji5ksw5xk04sa6y58jziabcq9")))

(define-public crate-supabase-wrappers-macros-0.1.12 (c (n "supabase-wrappers-macros") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1z47h2nlsfxwxq1ar18841g2m0n7zijn40ndzar3n8ps1bp33129") (y #t)))

(define-public crate-supabase-wrappers-macros-0.1.13 (c (n "supabase-wrappers-macros") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nlamf96dhkwpfqzcihvwhk4x56gw19wrja9s488h963f0c0wvqr") (y #t)))

(define-public crate-supabase-wrappers-macros-0.1.14 (c (n "supabase-wrappers-macros") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1d8170v3h8w1zy80xkdpm5hv6bz0nhnkg16ilqjkv4gw45h8pvyz")))

(define-public crate-supabase-wrappers-macros-0.1.15 (c (n "supabase-wrappers-macros") (v "0.1.15") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0lfn3gc3fyhz30agib24wyjfwh0vagcn9x9s8lyl0yl1x2rsywdf")))

