(define-module (crates-io su lh sulhyd) #:use-module (crates-io))

(define-public crate-sulhyd-0.1.0 (c (n "sulhyd") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.29") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.188") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.25") (d #t) (k 0)))) (h "091xfx35b8js2ipd0aqyb5fr4gj46bnzgd4c8vb089w3w5c6mvaa")))

