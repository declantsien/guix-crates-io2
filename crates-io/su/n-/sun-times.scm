(define-module (crates-io su n- sun-times) #:use-module (crates-io))

(define-public crate-sun-times-0.1.0 (c (n "sun-times") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2.0") (d #t) (k 0)))) (h "0ii7zyp6haxnzcjxyg8ympi4m2qg2491xcxs9632vvg365q1fi84")))

(define-public crate-sun-times-0.1.1 (c (n "sun-times") (v "0.1.1") (d (list (d (n "chrono") (r "^0.3.1") (d #t) (k 0)))) (h "0fh70ngddbymp80ms51b32dy2rpn85d75a741yxqqjqdbrzg8igf") (y #t)))

(define-public crate-sun-times-0.1.2 (c (n "sun-times") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)))) (h "1cgf8dk9y91ipfkny1zajdv6arf5wqasdym9d81ywck40wjfmfha")))

(define-public crate-sun-times-0.2.0 (c (n "sun-times") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)))) (h "1j45lx0crf54pxx0j6mzaz27ls5hipajz1dlx9wlm2iqwiadcdnw")))

