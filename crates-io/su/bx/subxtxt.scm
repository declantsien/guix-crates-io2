(define-module (crates-io su bx subxtxt) #:use-module (crates-io))

(define-public crate-subxtxt-0.0.0 (c (n "subxtxt") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.71") (d #t) (k 0)) (d (n "codec") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0) (p "parity-scale-codec")) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "subxt") (r "^0.25.0") (d #t) (k 0)))) (h "088p39mdw02r37aw6ba9svinbwrj3cn7qhsqmhgp9rck4za9y660")))

