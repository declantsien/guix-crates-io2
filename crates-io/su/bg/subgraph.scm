(define-module (crates-io su bg subgraph) #:use-module (crates-io))

(define-public crate-subgraph-0.1.0 (c (n "subgraph") (v "0.1.0") (h "0m76ph4jkpibwi38j7g6wh5an9vbavaizj0s17d7c3f8b80hl94l")))

(define-public crate-subgraph-0.2.0 (c (n "subgraph") (v "0.2.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)))) (h "1y3qadjgxnbdz0fwvfz5da7fgdi9akkd2b5w4kqc3dqkyz348ygd")))

