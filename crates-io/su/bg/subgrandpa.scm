(define-module (crates-io su bg subgrandpa) #:use-module (crates-io))

(define-public crate-subgrandpa-0.1.0 (c (n "subgrandpa") (v "0.1.0") (d (list (d (n "parity-scale-codec") (r "^1.3.5") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0m3akmx3j6h2hh00qb9k3pngi5q4xm78snv45wpl50hd7b4zy950") (f (quote (("codec" "parity-scale-codec")))) (y #t)))

(define-public crate-subgrandpa-0.2.0 (c (n "subgrandpa") (v "0.2.0") (d (list (d (n "parity-scale-codec") (r "^1.3.5") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1019znlb3hdfrgvds0p2asvji72vk4sj81cr1r4d3lf60gkzskwm") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-subgrandpa-0.2.1 (c (n "subgrandpa") (v "0.2.1") (d (list (d (n "parity-scale-codec") (r "^1.3.6") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "12lxg59v9pnfnrspij59h2n9cm8j1kalb7br60smxyx02033qwlb") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-subgrandpa-0.2.2 (c (n "subgrandpa") (v "0.2.2") (d (list (d (n "parity-scale-codec") (r "^2.0.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0bg86l0p810lav2wfp7v0qpys4azsf565gr7sk72d83x85w4vjrq") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-subgrandpa-0.2.3 (c (n "subgrandpa") (v "0.2.3") (d (list (d (n "parity-scale-codec") (r "^2.0.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0fij6xgsq644smzx8bd4y449s4rb40ln1r12bam2ngrkpz8b3yzq") (f (quote (("codec" "parity-scale-codec")))) (y #t)))

(define-public crate-subgrandpa-0.2.4 (c (n "subgrandpa") (v "0.2.4") (d (list (d (n "parity-scale-codec") (r "^2.0.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0yd75bbiijg385q7730rd5zz5fz7if5w6dhcn86sw6sw0pmkqpcm") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-subgrandpa-0.2.5 (c (n "subgrandpa") (v "0.2.5") (d (list (d (n "parity-scale-codec") (r "^2.1.1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "15x72y441dv0pp51g0ka1wxygs2gks8023sqi837y0w3gcd6gz01") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-subgrandpa-0.2.6 (c (n "subgrandpa") (v "0.2.6") (d (list (d (n "parity-scale-codec") (r "^2.2.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0jl17hvzqxyhqarwghqc0f4vljqdxj5h3mqzrvsbn9742w3i7lj7") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-subgrandpa-0.2.7 (c (n "subgrandpa") (v "0.2.7") (d (list (d (n "parity-scale-codec") (r "^2.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0qcysa7ykk132q7ifhbkv7wvjfsmqzkxhj0hh39nyxhv13mpm68y") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-subgrandpa-0.7.0 (c (n "subgrandpa") (v "0.7.0") (d (list (d (n "parity-scale-codec") (r "^2.3") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1h3cslfwb4cj5w02g0c5xmwdp12qyp5x13q5d5p155acx20hqrbl") (f (quote (("codec" "parity-scale-codec"))))))

(define-public crate-subgrandpa-0.9.0-rc13 (c (n "subgrandpa") (v "0.9.0-rc13") (d (list (d (n "parity-scale-codec") (r "^3.2") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "00ygmgr9rdbmpnz0pds9zb61aljyj7x7hmwmcrffv95xj7pa48hx") (f (quote (("codec" "parity-scale-codec"))))))

