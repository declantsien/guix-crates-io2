(define-module (crates-io su bg subgraph-matching) #:use-module (crates-io))

(define-public crate-subgraph-matching-0.0.1 (c (n "subgraph-matching") (v "0.0.1") (d (list (d (n "atoi") (r "^0.4.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "gdl") (r "^0.2.4") (d #t) (k 0)) (d (n "linereader") (r "^0.4.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.0") (d #t) (k 0)) (d (n "trim-margin") (r "^0.1.0") (d #t) (k 2)))) (h "0185wn34vlnvgvah8311db4pzw7z4dxbkhzwzxnv1qw3mcp23j1n")))

(define-public crate-subgraph-matching-0.0.2 (c (n "subgraph-matching") (v "0.0.2") (d (list (d (n "atoi") (r "^0.4.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.5") (d #t) (k 0)) (d (n "gdl") (r "^0.2.4") (d #t) (k 0)) (d (n "linereader") (r "^0.4.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "trim-margin") (r "^0.1.0") (d #t) (k 2)))) (h "1p0y7s670pfk2nyb75bah8ph8y91wyqvan6d8i7dc911vgz4rd30")))

(define-public crate-subgraph-matching-0.0.3 (c (n "subgraph-matching") (v "0.0.3") (d (list (d (n "atoi") (r "^0.4.0") (d #t) (k 0)) (d (n "gdl") (r "^0.2.4") (d #t) (k 0)) (d (n "linereader") (r "^0.4.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "trim-margin") (r "^0.1.0") (d #t) (k 2)))) (h "03r57d14wzchxv30saapfvbacdg8wvc9hbs7g33pqjr2nix0dk4z")))

(define-public crate-subgraph-matching-0.0.4 (c (n "subgraph-matching") (v "0.0.4") (d (list (d (n "atoi") (r "^0.4.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "delegate") (r "^0.6.1") (d #t) (k 0)) (d (n "gdl") (r "^0.2.4") (d #t) (k 0)) (d (n "graph") (r "^0.1.4") (d #t) (k 0)) (d (n "linereader") (r "^0.4.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "trim-margin") (r "^0.1.0") (d #t) (k 2)))) (h "0f08r0sg07lns6w8cmnh9w4jvlw8mm991yjf3fa4fp9gs4azl3dx")))

