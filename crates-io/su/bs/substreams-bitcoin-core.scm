(define-module (crates-io su bs substreams-bitcoin-core) #:use-module (crates-io))

(define-public crate-substreams-bitcoin-core-1.0.0 (c (n "substreams-bitcoin-core") (v "1.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.0") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.0") (d #t) (k 0)) (d (n "substreams") (r "^0.5.0") (d #t) (k 0)))) (h "11v1j8dhmsyimr64lba1dcrr69xznh4xym7n03xcxysadp7cb65j") (r "1.60")))

