(define-module (crates-io su bs substring) #:use-module (crates-io))

(define-public crate-substring-1.0.0 (c (n "substring") (v "1.0.0") (h "1drifmpf5jnhdygfgabckmjy6gy5p1my89pzaj8qwr5n2ayn1p3i")))

(define-public crate-substring-1.1.0 (c (n "substring") (v "1.1.0") (h "19c84bh1w3qr7hvaks7ikc653llhj0sa6ivrpc54r0lpx7r1029w")))

(define-public crate-substring-1.1.1 (c (n "substring") (v "1.1.1") (h "0imvl0baga03zby4y6gbr1m6hf094yaby4xk2z5xm1qaxhbw6b9q")))

(define-public crate-substring-1.2.0 (c (n "substring") (v "1.2.0") (h "1xzapd1j4pqcpvd0hlqid26liikp8lz1wmj7r5j4cwvra0cd1fqm")))

(define-public crate-substring-1.2.1 (c (n "substring") (v "1.2.1") (h "1vddzkgpmnqnbb9ap2h97hch3g2yj52x0xm8mpf7b94vb9vjv5wj")))

(define-public crate-substring-1.2.2 (c (n "substring") (v "1.2.2") (h "012ij434b8shpw7bgk25glngagy5np3f9ifa8hdc3350bwgngd64")))

(define-public crate-substring-1.2.3 (c (n "substring") (v "1.2.3") (h "172k3c6k3h8jmffy9ihlv99x4663vkz8n83bswk5w0p3p6z8d5wr")))

(define-public crate-substring-1.2.4 (c (n "substring") (v "1.2.4") (h "0z2f18iw8vzw0jc5pac0vw4s7qkyp042c6sxi9mmmjrl6zq9bwz8")))

(define-public crate-substring-1.2.5 (c (n "substring") (v "1.2.5") (h "1lrsg9bk7f1fdkp71p0wd2v7rjaf574jy5mn54ri7viic6lf8znm")))

(define-public crate-substring-1.3.0 (c (n "substring") (v "1.3.0") (h "1lcsn8qdiqgipskmkcsidfhww1awpsiv7s8z254p9qxf5i0rz7ak")))

(define-public crate-substring-1.3.1 (c (n "substring") (v "1.3.1") (h "0z0i58n8shcwazi4w5b8v67wfa4x5i6bxrfb0g6nh5zphk1zdx52")))

(define-public crate-substring-1.4.0 (c (n "substring") (v "1.4.0") (d (list (d (n "autocfg") (r "^1.0.1") (d #t) (k 1)))) (h "16xn4sixx4b4wcxszfaxp47n0467hzchyndz6pvlqpkg9dsbcjqk")))

(define-public crate-substring-1.4.1 (c (n "substring") (v "1.4.1") (d (list (d (n "autocfg") (r "^1.0.1") (d #t) (k 1)))) (h "0av1maasz8d0xjqg2cbci859320j638gyj5sg7z2kmni7hgs6hy1")))

(define-public crate-substring-1.4.2 (c (n "substring") (v "1.4.2") (d (list (d (n "autocfg") (r "^1.0.1") (d #t) (k 1)))) (h "1836p7ymlpf1n1ycijvdc5hyiz0d7csalb0qjwjd7dv8y54y0ijb")))

(define-public crate-substring-1.4.3 (c (n "substring") (v "1.4.3") (d (list (d (n "autocfg") (r "^1.0.1") (d #t) (k 1)))) (h "071jgfprvgc1h72mf2fnxm5048604k1kzv7fxdi0wq4jjar6ii92")))

(define-public crate-substring-1.4.4 (c (n "substring") (v "1.4.4") (d (list (d (n "autocfg") (r "^1.0.1") (d #t) (k 1)))) (h "1kygdiaykxbwmapksb323f10mw1mprvn27xbkj3fay7vzd32ncj9") (y #t)))

(define-public crate-substring-1.4.5 (c (n "substring") (v "1.4.5") (d (list (d (n "autocfg") (r "^1.0.1") (d #t) (k 1)))) (h "11jcadn4h1xwx3dq5gbgs5y3x57ml9jfz1zmf8p3n8ggxhrn9vj2")))

