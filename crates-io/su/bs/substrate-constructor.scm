(define-module (crates-io su bs substrate-constructor) #:use-module (crates-io))

(define-public crate-substrate-constructor-0.1.0 (c (n "substrate-constructor") (v "0.1.0") (d (list (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc"))) (k 0)) (d (n "external-memory-tools") (r "^0.1.0") (k 0)) (d (n "frame-metadata") (r "^16.0.0") (f (quote ("current" "decode"))) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (k 0)) (d (n "parity-scale-codec") (r "^3.6.9") (f (quote ("derive" "full" "bit-vec"))) (k 0)) (d (n "primitive-types") (r "^0.12.2") (k 0)) (d (n "scale-info") (r "^2.10.0") (k 0)) (d (n "sp-arithmetic") (r "^24.0.0") (k 0)) (d (n "substrate_parser") (r "^0.6.1") (k 0)))) (h "08krq5rv2xw5k1ils1hcs2kk1w2p6sm467fi891y5a6m7w1mv1bk")))

