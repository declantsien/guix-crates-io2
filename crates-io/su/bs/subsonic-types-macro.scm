(define-module (crates-io su bs subsonic-types-macro) #:use-module (crates-io))

(define-public crate-subsonic-types-macro-0.0.0 (c (n "subsonic-types-macro") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "0ppzbwk6420g1hsh4hg1cib98wrb6q13ylrbw8ijfh0myhnm3jab")))

