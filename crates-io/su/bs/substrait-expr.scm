(define-module (crates-io su bs substrait-expr) #:use-module (crates-io))

(define-public crate-substrait-expr-0.1.0 (c (n "substrait-expr") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "substrait") (r "^0.20.2") (d #t) (k 0)) (d (n "substrait-expr-funcgen") (r "^0.1.0") (d #t) (k 1)) (d (n "substrait-expr-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "09i2s103bhg0rrz454vkrs05s9hn3ffqmxf945zzvw9ykbkc7p9i")))

(define-public crate-substrait-expr-0.1.1 (c (n "substrait-expr") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "substrait") (r "^0.20.2") (d #t) (k 0)) (d (n "substrait-expr-funcgen") (r "^0.1.0") (d #t) (k 1)) (d (n "substrait-expr-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1j85644vy932ckmk6dsipkd7aac3sakm9py5b1ifsd4qr90v66b3")))

(define-public crate-substrait-expr-0.2.0 (c (n "substrait-expr") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "substrait") (r "^0.20.2") (d #t) (k 0)) (d (n "substrait-expr-funcgen") (r "^0.2.0") (d #t) (k 1)) (d (n "substrait-expr-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "07agqs7n87yi6w808bf1z3szxvk6dkk7pvxpfi1qfsjc0s6mylh1")))

(define-public crate-substrait-expr-0.2.1 (c (n "substrait-expr") (v "0.2.1") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "substrait") (r "^0.29.3") (d #t) (k 0)) (d (n "substrait-expr-funcgen") (r "^0.2.1") (d #t) (k 1)) (d (n "substrait-expr-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0xs5vmafqxl0s9l584cbny5q6dfysgq9bhg7cwdkjas4hb6bia69")))

