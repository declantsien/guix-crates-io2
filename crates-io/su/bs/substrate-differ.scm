(define-module (crates-io su bs substrate-differ) #:use-module (crates-io))

(define-public crate-substrate-differ-0.13.0 (c (n "substrate-differ") (v "0.13.0") (d (list (d (n "frame-metadata") (r "^14.0.0-rc.1") (f (quote ("v12" "v13" "v14" "std"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-format") (r "^0.4") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "treediff") (r "^4.0") (f (quote ("with-serde-json" "with-rustc-serialize"))) (d #t) (k 0)) (d (n "wasm-loader") (r "^0.13.0") (d #t) (k 2)) (d (n "wasm-testbed") (r "^0.13.1") (d #t) (k 0)))) (h "16niar1wvzqpa2wkxjq5a867jliypiy19b39fmcy5g174qvwj5sf")))

