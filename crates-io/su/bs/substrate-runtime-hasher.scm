(define-module (crates-io su bs substrate-runtime-hasher) #:use-module (crates-io))

(define-public crate-substrate-runtime-hasher-0.2.3 (c (n "substrate-runtime-hasher") (v "0.2.3") (d (list (d (n "blake2") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "codec") (r "^1.3.1") (d #t) (k 0) (p "parity-scale-codec")) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "1ap642gj8vqpagpkz6705qx0lm4912rb2nj6bc83zz0s5155jjmz")))

(define-public crate-substrate-runtime-hasher-0.2.4 (c (n "substrate-runtime-hasher") (v "0.2.4") (d (list (d (n "blake2") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "codec") (r "^1.3.1") (d #t) (k 0) (p "parity-scale-codec")) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "0hir4c99scwcl1wi1g6m8wl0v6xmvvmjzqc59ryrqaayvrpp30cm")))

(define-public crate-substrate-runtime-hasher-0.2.5 (c (n "substrate-runtime-hasher") (v "0.2.5") (d (list (d (n "blake2") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "codec") (r "^1.3.1") (d #t) (k 0) (p "parity-scale-codec")) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "0v5d4lkq47s9v2lvl85hh7msjcxcl9l4l5v5a3gixznlg88z5byc")))

(define-public crate-substrate-runtime-hasher-0.2.6 (c (n "substrate-runtime-hasher") (v "0.2.6") (d (list (d (n "blake2") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "codec") (r "^1.3.1") (d #t) (k 0) (p "parity-scale-codec")) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "0lq0081mgc374afn7fxhxdf85l4x54y341ddxbfi7ab6cs1r91qq")))

(define-public crate-substrate-runtime-hasher-0.2.7 (c (n "substrate-runtime-hasher") (v "0.2.7") (d (list (d (n "blake2") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "codec") (r "^1.3.1") (d #t) (k 0) (p "parity-scale-codec")) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "039zfgn0d5jm7kkyv5msqf8n5n9qbx15pdv2ciynv073jpchzsh9")))

(define-public crate-substrate-runtime-hasher-0.2.8 (c (n "substrate-runtime-hasher") (v "0.2.8") (d (list (d (n "blake2") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "codec") (r "^1.3.1") (d #t) (k 0) (p "parity-scale-codec")) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "0flzzq2yzm52m1b725zv6b29vsbllgjibv1g61g9cvzg2z6lflmn")))

(define-public crate-substrate-runtime-hasher-0.2.9 (c (n "substrate-runtime-hasher") (v "0.2.9") (d (list (d (n "blake2") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "codec") (r "^1.3.1") (d #t) (k 0) (p "parity-scale-codec")) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "1kmhnqy4iyjnc35wv6azdv2zx4frf6mlv6vwz26v7aygz0y0y2x5")))

