(define-module (crates-io su bs substrate-runtime-proposal-hash) #:use-module (crates-io))

(define-public crate-substrate-runtime-proposal-hash-0.13.0 (c (n "substrate-runtime-proposal-hash") (v "0.13.0") (d (list (d (n "blake2") (r "^0.9") (d #t) (k 0)) (d (n "codec") (r "^2.2") (d #t) (k 0) (p "parity-scale-codec")) (d (n "frame-metadata") (r "^14.0.0-rc.1") (f (quote ("v12" "v13" "v14" "std"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "sc-executor") (r "^0.9") (d #t) (k 0)) (d (n "sp-core") (r "^3.0") (d #t) (k 0)) (d (n "sp-io") (r "^3.0") (d #t) (k 0)) (d (n "sp-runtime") (r "^3.0") (d #t) (k 0)) (d (n "sp-wasm-interface") (r "^3.0") (d #t) (k 0)))) (h "0m4v1yzhgw450xcy2qnr86pzzv7fivg1nqpbq5lccblghl2khk32")))

