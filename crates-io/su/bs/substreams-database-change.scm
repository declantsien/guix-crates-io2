(define-module (crates-io su bs substreams-database-change) #:use-module (crates-io))

(define-public crate-substreams-database-change-0.1.0 (c (n "substreams-database-change") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.0") (d #t) (k 0)) (d (n "substreams") (r "^0.4.0") (d #t) (k 0)))) (h "125nbcd5q9zhy336qrnp3wyh3vxy2wb0kkwjblr6p18yy2cmvg21") (r "1.60")))

(define-public crate-substreams-database-change-0.1.1 (c (n "substreams-database-change") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.0") (d #t) (k 0)) (d (n "substreams") (r "^0.4.0") (d #t) (k 0)))) (h "0h37mhk74z5zz1v2v27mcadz51ralk9a7bdp878k7pa7vys2k378") (r "1.60")))

(define-public crate-substreams-database-change-0.1.2 (c (n "substreams-database-change") (v "0.1.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.0") (d #t) (k 0)) (d (n "substreams") (r "^0.4.0") (d #t) (k 0)))) (h "045b7cvic06wp4p9w3yy0rmdib26bmhq21xw1xylszjfwzhdc6fc") (r "1.60")))

(define-public crate-substreams-database-change-0.2.0 (c (n "substreams-database-change") (v "0.2.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.0") (d #t) (k 0)) (d (n "substreams") (r "^0.5.0") (d #t) (k 0)))) (h "049wqzms19dwwb21m43k91bqiz6bkyg3h24p0hljih0rbjxlhmb2") (r "1.60")))

(define-public crate-substreams-database-change-1.0.0 (c (n "substreams-database-change") (v "1.0.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.0") (d #t) (k 0)) (d (n "substreams") (r "^0.5.0") (d #t) (k 0)))) (h "0k1g4k64baz50hjxaw9xscri74wa4xfaiwnxfcffxslrisnydbdw") (r "1.60")))

(define-public crate-substreams-database-change-1.1.0 (c (n "substreams-database-change") (v "1.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.0") (d #t) (k 0)) (d (n "substreams") (r "^0.5.0") (d #t) (k 0)) (d (n "substreams-entity-change") (r "^1.0.0") (d #t) (k 0)))) (h "0fdg17cnil9v8bpmcjsgz8alpbidjkrs66qmzcszxdwd76yivpmr") (r "1.60")))

(define-public crate-substreams-database-change-1.1.1 (c (n "substreams-database-change") (v "1.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.0") (d #t) (k 0)) (d (n "substreams") (r "^0.5.0") (d #t) (k 0)) (d (n "substreams-entity-change") (r "^1.0.0") (d #t) (k 0)))) (h "13m48m2ha3lnqcjzqnbj511yfwsmniyi6bdnfm21wlms5w7b562m") (r "1.60")))

(define-public crate-substreams-database-change-1.1.2 (c (n "substreams-database-change") (v "1.1.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.0") (d #t) (k 0)) (d (n "substreams") (r "^0.5.0") (d #t) (k 0)) (d (n "substreams-entity-change") (r "^1.0.0") (d #t) (k 0)))) (h "1x5q1m3b0pd28sy7r8wwsc2jky8gr7zdxawzcp7r55ayhrl0c1ix") (r "1.60")))

(define-public crate-substreams-database-change-1.1.3 (c (n "substreams-database-change") (v "1.1.3") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "substreams") (r "^0.5") (d #t) (k 0)))) (h "07flhsigrajy56rgzg6hj9aa8b36xknlxgw6kbpz2n6gk27x14vp") (r "1.60")))

(define-public crate-substreams-database-change-1.2.0 (c (n "substreams-database-change") (v "1.2.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "substreams") (r "^0.5") (d #t) (k 0)))) (h "0l0m83w4i1r8xxfalfrs6nnq15948ql1dd96w3qlw1n2m710fd0a") (r "1.60")))

(define-public crate-substreams-database-change-1.2.1 (c (n "substreams-database-change") (v "1.2.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "substreams") (r "^0.5") (d #t) (k 0)))) (h "1wa6r2pnrlzs19fzhi2ik65h04h86dg9rckmq9zyhdvb3z19gcgz") (r "1.60")))

(define-public crate-substreams-database-change-1.3.0 (c (n "substreams-database-change") (v "1.3.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "substreams") (r "^0.5") (d #t) (k 0)))) (h "1k267s934wcqiyk9m2dyi3lgr2vxcfqbbamr1vjs7zlq0iq9r81y") (r "1.60")))

(define-public crate-substreams-database-change-1.3.1 (c (n "substreams-database-change") (v "1.3.1") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)) (d (n "substreams") (r "^0.5") (d #t) (k 0)))) (h "1hshngwbdnrw8hv76bj0hbgx5basajq5pg9s9ml7ljzsqxpwlcpd") (r "1.60")))

