(define-module (crates-io su bs substrate-wasm-builder-runner) #:use-module (crates-io))

(define-public crate-substrate-wasm-builder-runner-1.0.0 (c (n "substrate-wasm-builder-runner") (v "1.0.0") (h "11d4j2wpdyi92f18i82l78vgfwjskn7h62cmgfa5hl3f8wmvccrm")))

(define-public crate-substrate-wasm-builder-runner-1.0.1 (c (n "substrate-wasm-builder-runner") (v "1.0.1") (h "03gbr9dxm9z6dbbwzdr304v2x9ndirqy84b44aglabiakgraa7qk")))

(define-public crate-substrate-wasm-builder-runner-1.0.2 (c (n "substrate-wasm-builder-runner") (v "1.0.2") (h "0wfh82y1brhzy0wf4g40srcz905n6xcy2a0q833fbmn3dkzwnbpm")))

(define-public crate-substrate-wasm-builder-runner-1.0.3 (c (n "substrate-wasm-builder-runner") (v "1.0.3") (h "0h4bxgs22ibxn78y08jdw76qhg7kvsq0qw4zj70i5ciqmmzv48dg")))

(define-public crate-substrate-wasm-builder-runner-1.0.4 (c (n "substrate-wasm-builder-runner") (v "1.0.4") (h "0l455z1ivqb7hg6x4yml4pwni00wn1xm673cglgjryfpx4zjfj5x")))

(define-public crate-substrate-wasm-builder-runner-1.0.5 (c (n "substrate-wasm-builder-runner") (v "1.0.5") (h "1f43qihaj14w4h7lkb5ic0kkwmbb9mx1xnr6zq2d8pvxgvg70373")))

(define-public crate-substrate-wasm-builder-runner-1.0.6 (c (n "substrate-wasm-builder-runner") (v "1.0.6") (h "0sifw2f1lvbf5kyzzm6dh4czsir9bwj614iyi79kbaql8ncnbafj")))

(define-public crate-substrate-wasm-builder-runner-2.0.0 (c (n "substrate-wasm-builder-runner") (v "2.0.0") (h "0bw67ppshnmhsam4cylhbwqxwk47l1d859ay5jk3haz3cwhv3jjl")))

(define-public crate-substrate-wasm-builder-runner-3.0.0 (c (n "substrate-wasm-builder-runner") (v "3.0.0") (h "13vyw9klznplzcjx4aqq2hl4qj12wqc2askf26arq8f0rapjcrii")))

