(define-module (crates-io su bs subs) #:use-module (crates-io))

(define-public crate-subs-0.1.0 (c (n "subs") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1bd3vd3m53qhpflvhylk9hf1krg3qm6p7npzzv38nd1mp1hr3i2s")))

(define-public crate-subs-0.1.1 (c (n "subs") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "inotify") (r "^0.7") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "1d66cns9bv5ybd28c0wyfp1wgrv6y1pcmg62gd15rmcx1x85sbpx")))

(define-public crate-subs-0.1.2 (c (n "subs") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "12p8vya6lqgk50z0nwhhjvdlhk85mb9jdlh9mjy8hmkkjsmahj07")))

(define-public crate-subs-0.1.3 (c (n "subs") (v "0.1.3") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0533gqjp6sdawqmbzypz9ikgqlgyjlkh5g8jh20zxks8ks8r33pv")))

(define-public crate-subs-0.1.4 (c (n "subs") (v "0.1.4") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0j4jn669wl2nnsgpskj8fnb5l17y8jrp0lps66p88d6k3v3ia88n")))

(define-public crate-subs-0.1.5 (c (n "subs") (v "0.1.5") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0435y8c0x7kq1n5q6m86r35pyaywyks2qs5fr43qsya250klwwif")))

(define-public crate-subs-0.1.6 (c (n "subs") (v "0.1.6") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0biqx8i9wz1qyl1iw4f92s13chcpa8wznfrbzang3pzg24bgknds")))

(define-public crate-subs-0.2.0 (c (n "subs") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "112mgkigsjyq71vd41isv4l8clp3l5ia6azcx6c9l0x33n0chaqh")))

