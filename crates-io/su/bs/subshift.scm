(define-module (crates-io su bs subshift) #:use-module (crates-io))

(define-public crate-subshift-0.1.0 (c (n "subshift") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.2") (d #t) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "predicates") (r "^2.0.3") (d #t) (k 2)) (d (n "test-case") (r "^1.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13is99mswy5jzi19nyq3d757l33k96wv5b8br7c8kmvladaygsh8")))

