(define-module (crates-io su bs substance-macro) #:use-module (crates-io))

(define-public crate-substance-macro-0.1.0-alpha (c (n "substance-macro") (v "0.1.0-alpha") (d (list (d (n "proc-macro2") (r "1.*") (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "syn") (r "1.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1l1x5sp7bcjh6iarch0p2s7sqsng574c0f8wzn4fv5nkhypw3w5w")))

(define-public crate-substance-macro-0.1.1-alpha (c (n "substance-macro") (v "0.1.1-alpha") (d (list (d (n "proc-macro2") (r "1.*") (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "syn") (r "1.*") (f (quote ("full"))) (d #t) (k 0)))) (h "1h42zyfw18k0gnzwpshrrvkqm341ngjf2y0swzg9b77z7nh72dnn")))

(define-public crate-substance-macro-0.2.0-alpha (c (n "substance-macro") (v "0.2.0-alpha") (d (list (d (n "proc-macro2") (r "1.*") (d #t) (k 0)) (d (n "quote") (r "1.*") (d #t) (k 0)) (d (n "syn") (r "1.*") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "1.*") (d #t) (k 2)))) (h "1mv671ivwsvnp3b94qwi3sa4ac3y2flph2w32lawis5cmcbrb62z")))

