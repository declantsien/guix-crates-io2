(define-module (crates-io su bs substrate-build-script-utils) #:use-module (crates-io))

(define-public crate-substrate-build-script-utils-2.0.0-alpha.3 (c (n "substrate-build-script-utils") (v "2.0.0-alpha.3") (h "0ygchajmhzi1pj7ivvamdlm34qxssrsdg5p0jxw5kh657s5akhs9")))

(define-public crate-substrate-build-script-utils-2.0.0-alpha.5 (c (n "substrate-build-script-utils") (v "2.0.0-alpha.5") (h "1x10aq89r1d02l8ns2hy7sx24fnh27pr4ran7f02m424dc01dipg")))

(define-public crate-substrate-build-script-utils-2.0.0-alpha.6 (c (n "substrate-build-script-utils") (v "2.0.0-alpha.6") (d (list (d (n "platforms") (r "^0.2.1") (d #t) (k 0)))) (h "0z13087n1aqrp0kb5cc533wmvsvmddn033gxrzax0q6m8smxiax0")))

(define-public crate-substrate-build-script-utils-2.0.0-alpha.7 (c (n "substrate-build-script-utils") (v "2.0.0-alpha.7") (d (list (d (n "platforms") (r "^0.2.1") (d #t) (k 0)))) (h "19b6dnqnvd5s0bgv95ff2as2jvqy6ziifi1rssjzgw6bxbh51cqr")))

(define-public crate-substrate-build-script-utils-2.0.0-alpha.8 (c (n "substrate-build-script-utils") (v "2.0.0-alpha.8") (d (list (d (n "platforms") (r "^0.2.1") (d #t) (k 0)))) (h "0ykqw53iiav60alwrfx0iklrpfg6za7rvhnb4vwmg10j2m8aa3xz")))

(define-public crate-substrate-build-script-utils-2.0.0-rc1 (c (n "substrate-build-script-utils") (v "2.0.0-rc1") (d (list (d (n "platforms") (r "^0.2.1") (d #t) (k 0)))) (h "0qi6kvsg42ihn3y8pbsvzsd5g5giak3d1sj5pklpv0ib33d2ps5q")))

(define-public crate-substrate-build-script-utils-2.0.0-rc2 (c (n "substrate-build-script-utils") (v "2.0.0-rc2") (d (list (d (n "platforms") (r "^0.2.1") (d #t) (k 0)))) (h "13bj3kfbqkl429i2jlg55vvn3j189n0y6abvkz91y0fcwbg8yx2w")))

(define-public crate-substrate-build-script-utils-2.0.0-rc3 (c (n "substrate-build-script-utils") (v "2.0.0-rc3") (d (list (d (n "platforms") (r "^0.2.1") (d #t) (k 0)))) (h "0rzzrczb7d9fm9x91bb0kqk2yisljknqwrrwavvdafz4j7viiqgf")))

(define-public crate-substrate-build-script-utils-2.0.0-rc4 (c (n "substrate-build-script-utils") (v "2.0.0-rc4") (d (list (d (n "platforms") (r "^0.2.1") (d #t) (k 0)))) (h "0ligdhz658hdgxvra1zmim0g8xl19zxs15vmk9d6rm94dgrkc4d1")))

(define-public crate-substrate-build-script-utils-2.0.0-rc5 (c (n "substrate-build-script-utils") (v "2.0.0-rc5") (d (list (d (n "platforms") (r "^0.2.1") (d #t) (k 0)))) (h "1lvvpr7z7mr91lh415wj4jp5byfyxd2hyc2vjrix68zn6zq01sb6")))

(define-public crate-substrate-build-script-utils-2.0.0-rc6 (c (n "substrate-build-script-utils") (v "2.0.0-rc6") (d (list (d (n "platforms") (r "^0.2.1") (d #t) (k 0)))) (h "1qs2laafmlq4phc9i9h11xdvv8vbh6ip8lh15pj5jbzdhmxi1w0b")))

(define-public crate-substrate-build-script-utils-2.0.0 (c (n "substrate-build-script-utils") (v "2.0.0") (d (list (d (n "platforms") (r "^0.2.1") (d #t) (k 0)))) (h "1jrzf4srmr5j0pvy932vmvvjdi1lqv0xflsx9057s7p3dywflkzi")))

(define-public crate-substrate-build-script-utils-2.0.1 (c (n "substrate-build-script-utils") (v "2.0.1") (d (list (d (n "platforms") (r "^0.2.1") (d #t) (k 0)))) (h "1ykak034yrng7gfn8v2215v6kimmrv2gqffrsirry8qqcj9c49lp")))

(define-public crate-substrate-build-script-utils-3.0.0 (c (n "substrate-build-script-utils") (v "3.0.0") (d (list (d (n "platforms") (r "^1.1") (d #t) (k 0)))) (h "0id63w712savaa9bwn0z925cr8zbgr8bz5nffcn4q5r04nkhnm5x")))

(define-public crate-substrate-build-script-utils-4.0.0 (c (n "substrate-build-script-utils") (v "4.0.0") (d (list (d (n "platforms") (r "^2.0") (d #t) (k 0)))) (h "1arz1drpk3flh7z1jkl6qa9adskizr3dbyr3c8p356059fxmkmy2")))

(define-public crate-substrate-build-script-utils-5.0.0 (c (n "substrate-build-script-utils") (v "5.0.0") (d (list (d (n "platforms") (r "^2.0") (d #t) (k 0)))) (h "0ka5plj2n6i34ym9m7g41cpyrvzjr50crhqna4z2izbdhmp6pg1w")))

(define-public crate-substrate-build-script-utils-5.1.0-dev1 (c (n "substrate-build-script-utils") (v "5.1.0-dev1") (h "0h9xqygqb25c5g5lnkdrlfdnnv06slij26rzph3dyagpmhsv09fa")))

(define-public crate-substrate-build-script-utils-5.1.0-dev2 (c (n "substrate-build-script-utils") (v "5.1.0-dev2") (h "11g73qcyy4lwywk91h6m9nvsszxw3w14v3yqxfv01wxgysp25srl")))

(define-public crate-substrate-build-script-utils-5.1.0-dev.2 (c (n "substrate-build-script-utils") (v "5.1.0-dev.2") (h "1gfws0nznyw4axc6a457gdx30xp67s831i2ggvmjxvwcr5fh8ypk")))

(define-public crate-substrate-build-script-utils-5.1.0-dev.3 (c (n "substrate-build-script-utils") (v "5.1.0-dev.3") (h "1ym9rmkbn2vqbbvasfn1wisgcr4a2sz9a58cbm7r7vdrqf6058i6")))

(define-public crate-substrate-build-script-utils-5.1.0-dev.4 (c (n "substrate-build-script-utils") (v "5.1.0-dev.4") (h "0w6ximyp8yhblwkmzlw9i5dnhyz7x1rn2919glqc4rwah9m1kzv4")))

(define-public crate-substrate-build-script-utils-5.1.0-dev.5 (c (n "substrate-build-script-utils") (v "5.1.0-dev.5") (h "001y4q004nnqrmkz9r1kivaz1ws16v3gvdkwlbdc2pfi84d82i6l")))

(define-public crate-substrate-build-script-utils-5.1.0-dev.6 (c (n "substrate-build-script-utils") (v "5.1.0-dev.6") (h "0fzqqg0d20l1sd08j45423a6imkzijglyg3n4ra40gqasmq1gr1n")))

(define-public crate-substrate-build-script-utils-6.0.0 (c (n "substrate-build-script-utils") (v "6.0.0") (h "1hislcjw4cqrc4avgpp7hmkg72nd812jlxxv0myrk1yy8lq31acs")))

(define-public crate-substrate-build-script-utils-7.0.0 (c (n "substrate-build-script-utils") (v "7.0.0") (h "0vrj59qw59a43i5m8zx4mjpabxmn7ysnspl8vijllsikrihbzvfv")))

(define-public crate-substrate-build-script-utils-8.0.0-dev.1 (c (n "substrate-build-script-utils") (v "8.0.0-dev.1") (h "0ygz7h0h08r3yjivw0673ka66cllh2b91c5g68a31xjf8mhsh7h0")))

(define-public crate-substrate-build-script-utils-8.0.0 (c (n "substrate-build-script-utils") (v "8.0.0") (h "1fgy6a22965yb9b6xd7393hs9hjpxbx31izrkwwxgvj9b3dpq4kq")))

(define-public crate-substrate-build-script-utils-9.0.0 (c (n "substrate-build-script-utils") (v "9.0.0") (h "1hbc2li68gp9qkql4wdayb167xgn2xc44kn3mfwbfxrdlrb7afwa")))

(define-public crate-substrate-build-script-utils-10.0.0 (c (n "substrate-build-script-utils") (v "10.0.0") (h "1svwydhlfmn46w1j1p49lqsdnv5y5yksxyhvnrvhblpcl3gzn4fj")))

(define-public crate-substrate-build-script-utils-11.0.0 (c (n "substrate-build-script-utils") (v "11.0.0") (h "0bksqxrp097g173vf0nnza8h2y8mg4dxiwqrq7yk49x3hg8yg1dj")))

