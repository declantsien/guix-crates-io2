(define-module (crates-io su bs substruct) #:use-module (crates-io))

(define-public crate-substruct-0.1.0 (c (n "substruct") (v "0.1.0") (d (list (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0.39") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.96") (d #t) (k 2)))) (h "0llp9l3v08vsx4wrl6kzad35wmh8y0nk1gzyn6rswnchp58ss7yy")))

(define-public crate-substruct-0.1.1 (c (n "substruct") (v "0.1.1") (d (list (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 2)) (d (n "syn") (r "^2.0.39") (f (quote ("parsing"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.96") (d #t) (k 2)))) (h "0mqnhgakk47m39jr31n90yjpw12hda27hr4sk9rkvznz8pi1hcxn")))

