(define-module (crates-io su bs substance-framework) #:use-module (crates-io))

(define-public crate-substance-framework-0.1.0-alpha (c (n "substance-framework") (v "0.1.0-alpha") (d (list (d (n "libc-print") (r "^0.1.17") (d #t) (k 0)))) (h "170x08p9vfzp22c9mmh740vs8zrrh8hkvms3ssjkqwqlpa38bbhr")))

(define-public crate-substance-framework-0.1.1-alpha (c (n "substance-framework") (v "0.1.1-alpha") (d (list (d (n "libc-print") (r "^0.1.17") (d #t) (k 0)) (d (n "substance-macro") (r "^0.1.1-alpha") (d #t) (k 0)))) (h "1iilh5vljwbm24d6amcrmw5hbw9c7hckdpvi946yfh4a2l4iw059")))

(define-public crate-substance-framework-0.2.0-alpha (c (n "substance-framework") (v "0.2.0-alpha") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "1.4.*") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "libc-print") (r "0.1.*") (d #t) (k 0)) (d (n "spin") (r "0.9.*") (d #t) (k 0)) (d (n "substance-macro") (r "^0.1.1-alpha") (d #t) (k 0)))) (h "0p5fqs66av79mal8dv7dljfjagszcm0pl8h7d4aip1443yvmi7dr") (f (quote (("xml") ("pretty") ("default" "combined") ("combined")))) (y #t)))

(define-public crate-substance-framework-0.2.1-alpha (c (n "substance-framework") (v "0.2.1-alpha") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "1.4.*") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "libc-print") (r "0.1.*") (d #t) (k 0)) (d (n "spin") (r "0.9.*") (d #t) (k 0)) (d (n "substance-macro") (r "^0.1.1-alpha") (d #t) (k 0)))) (h "0bf3vzr8732jlazhp4imgm8yx1m3caxhq077877sjfm5rizbhqvn") (f (quote (("xml") ("pretty") ("default" "combined") ("combined"))))))

(define-public crate-substance-framework-0.3.0-alpha (c (n "substance-framework") (v "0.3.0-alpha") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "1.4.*") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "libc-print") (r "0.1.*") (d #t) (k 0)) (d (n "spin") (r "0.9.*") (d #t) (k 0)) (d (n "substance-macro") (r "^0.2.0-alpha") (d #t) (k 0)))) (h "0h5k8ikg6jidq7d7x6ks23rj3cf78jrsaaydinb2n1w2k1xkw60g") (f (quote (("xml") ("pretty") ("default" "combined") ("combined"))))))

