(define-module (crates-io su bs substreams-near) #:use-module (crates-io))

(define-public crate-substreams-near-0.9.2 (c (n "substreams-near") (v "0.9.2") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("custom"))) (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "0m4q4kqw5gx43046cvjd91wq46iambcvr4lp9ixz8bli50p8il2l") (r "1.60")))

(define-public crate-substreams-near-0.9.3 (c (n "substreams-near") (v "0.9.3") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("custom"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.11.0") (d #t) (k 1)) (d (n "prost-types") (r "^0.11.0") (d #t) (k 0)))) (h "0jmh39lz499q70v625gi0c4c9gg7dsa63psinjlswd7pk5khy6qm") (r "1.60")))

(define-public crate-substreams-near-0.9.4 (c (n "substreams-near") (v "0.9.4") (d (list (d (n "substreams") (r "^0.5.0") (d #t) (k 0)) (d (n "substreams-near-core") (r "^0.9.4") (d #t) (k 0)))) (h "1nch5lrxdk1bc9n2lmjzk42ysx8kcy2av7sqi0ds51hwblwxq1x8") (r "1.60")))

(define-public crate-substreams-near-0.9.5 (c (n "substreams-near") (v "0.9.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (f (quote ("custom"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "substreams") (r "^0.5.0") (d #t) (k 0)) (d (n "substreams-near-core") (r "^0.9.5") (d #t) (k 0)))) (h "1iwcq55527g14747ijnx3mbdycfxmrs0x0yqrncv6frf5gi7fb59") (r "1.60")))

(define-public crate-substreams-near-0.9.6 (c (n "substreams-near") (v "0.9.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "getrandom") (r "^0.2") (f (quote ("custom"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "substreams") (r "^0.5.0") (d #t) (k 0)) (d (n "substreams-near-core") (r "^0.9.6") (d #t) (k 0)))) (h "1lym7i3ca28wg4f3mw2k6nl24k0yw43j4h51s192379r3p7kvh7h") (r "1.60")))

