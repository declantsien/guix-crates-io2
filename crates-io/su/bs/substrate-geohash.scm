(define-module (crates-io su bs substrate-geohash) #:use-module (crates-io))

(define-public crate-substrate-geohash-0.13.0 (c (n "substrate-geohash") (v "0.13.0") (d (list (d (n "codec") (r "^3.6.1") (f (quote ("derive" "max-encoded-len"))) (k 0) (p "parity-scale-codec")) (d (n "fixed") (r "^0.5.9") (d #t) (k 0) (p "substrate-fixed")) (d (n "scale-info") (r "^2.5.0") (f (quote ("derive"))) (k 0)))) (h "16if2ikhvnpywql20494bb1pkv6miz8nzarqfgckf6xcsikssaps")))

