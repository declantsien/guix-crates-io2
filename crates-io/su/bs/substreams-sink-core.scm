(define-module (crates-io su bs substreams-sink-core) #:use-module (crates-io))

(define-public crate-substreams-sink-core-0.1.3 (c (n "substreams-sink-core") (v "0.1.3") (d (list (d (n "prost") (r "^0.10.1") (d #t) (k 0)) (d (n "prost-types") (r "^0.10.1") (d #t) (k 0)))) (h "186wdpzkcl4cdg5yqfms08rzi2g003qw28x13np1ll8rysi3kv8x") (r "1.60")))

