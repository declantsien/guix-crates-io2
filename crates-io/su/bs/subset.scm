(define-module (crates-io su bs subset) #:use-module (crates-io))

(define-public crate-subset-0.1.0 (c (n "subset") (v "0.1.0") (h "0krb09xhfawjbzl28v471nrr42fg4cn5pz0a36fv7c23ab4j67zk")))

(define-public crate-subset-0.1.1 (c (n "subset") (v "0.1.1") (h "05sjdrwb5sz8hggzm1s0x8zzw28ybdh1zs4nwcifjh5sf43bgq1x")))

(define-public crate-subset-0.2.1 (c (n "subset") (v "0.2.1") (h "13r2a3r43clv3n2jxhj9jihs4mz6sqdi5k2fqghhqp9rv8hd29ab")))

(define-public crate-subset-0.2.2 (c (n "subset") (v "0.2.2") (h "0bb3n0iq7kxws1wgq2ad3zx4416pjdablqq85592jvp7qhsfzh4k")))

(define-public crate-subset-0.3.0 (c (n "subset") (v "0.3.0") (h "0bcj6nw2ijf535b4pdd5f1rk5zs3372kwfm96aznbbv54cj91p97")))

