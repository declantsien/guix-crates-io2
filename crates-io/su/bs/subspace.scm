(define-module (crates-io su bs subspace) #:use-module (crates-io))

(define-public crate-subspace-0.1.0 (c (n "subspace") (v "0.1.0") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (k 0)) (d (n "serde") (r "^1.0.111") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b157ia7lhfdn4vjk9v0bbcbywjv9ff073dr9ik728cafanc4w2j")))

