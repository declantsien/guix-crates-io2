(define-module (crates-io su bs substrate_macro) #:use-module (crates-io))

(define-public crate-substrate_macro-0.1.0 (c (n "substrate_macro") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0x6mf1nmbigd22pya14kqdvjrl8jaagi3rqfavqw8jbbbig267n1")))

(define-public crate-substrate_macro-0.1.1 (c (n "substrate_macro") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0dnb72785ic40mjh531jmcbsqgn8i0qqaxx19xdmjw6sg12ia6ir")))

(define-public crate-substrate_macro-0.1.2 (c (n "substrate_macro") (v "0.1.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0vcnbp5j7zlyxjdygzgkrvi1518iik6bn6qrdzrdvkf3m9jffn5z")))

(define-public crate-substrate_macro-0.1.3 (c (n "substrate_macro") (v "0.1.3") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.95") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "14xgzqxrvx1xjvfpdi5dv8ajkxlrplass1pzijyf4lmrfg7lz815")))

