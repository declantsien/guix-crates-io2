(define-module (crates-io su bs subset_sum) #:use-module (crates-io))

(define-public crate-subset_sum-0.4.3 (c (n "subset_sum") (v "0.4.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "141cgj5fq3mkxm38yrip686phlvh2nal7may5r7wxwjcvw3cqfhs")))

(define-public crate-subset_sum-0.5.0 (c (n "subset_sum") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "018ddnf1k7wxwwfyrwl2m41fnvnw7z0gi5q54lbj512cw6fz7qfr")))

(define-public crate-subset_sum-0.5.1 (c (n "subset_sum") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "162iv332kg2kbkk3cb4k3ziymnk6967b6j3dj0d898n0xggbbaah")))

(define-public crate-subset_sum-0.6.0 (c (n "subset_sum") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0x41c9yz90rjlv922rnhbj710nmyiyqp6q6m4di428q0c1pivydk")))

(define-public crate-subset_sum-0.7.0 (c (n "subset_sum") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1455cly7jv6ng6pw39440zjp859s07c2p15rlqfrik7rx46nzphc")))

(define-public crate-subset_sum-0.8.1 (c (n "subset_sum") (v "0.8.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1nq0x14j278agdwq2yl4xhmi711aqy9q0b8ym3grqiw6w0ry9cll")))

(define-public crate-subset_sum-0.8.2 (c (n "subset_sum") (v "0.8.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "06645jc3y932hp288iwj117mk8z6n8gs0vs5dzxib029v8fam1sg")))

(define-public crate-subset_sum-0.8.3 (c (n "subset_sum") (v "0.8.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0irjm0d123rglfj3lypiv7vjbmihkaadv593jl3816zwim54fs9h")))

(define-public crate-subset_sum-0.8.4 (c (n "subset_sum") (v "0.8.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0chx22dhwvhiw5q68j3wi4xxk746ddpkyryajdn61zldk6wvyw82")))

(define-public crate-subset_sum-0.9.0 (c (n "subset_sum") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "192fbs397iiay142xlzj7mks14vkln79yf72s1pngkfcsjnhhy0l")))

(define-public crate-subset_sum-0.10.1 (c (n "subset_sum") (v "0.10.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1ik5pfa0vpf64k6qy2y2x81m385l17d8qxr2ib1c8qacb3qr8wxp")))

(define-public crate-subset_sum-0.10.3 (c (n "subset_sum") (v "0.10.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0frkk9zjw8m0gbf9gmp5ppayayn98m22fw2gyxm0i94r47wz25ac")))

(define-public crate-subset_sum-0.10.5 (c (n "subset_sum") (v "0.10.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1yajws4h8jhxnxzrw5a7dbrp5jlbnp4rvgy0d33k4zlicc7c34qp")))

(define-public crate-subset_sum-0.11.0 (c (n "subset_sum") (v "0.11.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1j743lhyahrcm5xz1x96yqyxgn6453gpd4a6i0xj4kwwrv59iz4k")))

(define-public crate-subset_sum-0.11.1 (c (n "subset_sum") (v "0.11.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "179641wy2cc3qd0zcxlkqjma307hz0jjdvmmlhf27q9p8zw2kr5q")))

(define-public crate-subset_sum-0.11.2 (c (n "subset_sum") (v "0.11.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1fw2ybs9k44wz6y24l7p6c8lzcd3rkwmxdr4jgdgg8v5r310lhql")))

(define-public crate-subset_sum-0.11.3 (c (n "subset_sum") (v "0.11.3") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0kd302hanpxysvn4yy4gg5xk6fh9dgq4angrjpn6nhla5y6j2vab")))

(define-public crate-subset_sum-0.12.0 (c (n "subset_sum") (v "0.12.0") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0fz2d1dbdllxl4djwbgj2y24b9vnmkrjd9ir2nzc7siab3krvvr0")))

(define-public crate-subset_sum-0.12.1 (c (n "subset_sum") (v "0.12.1") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "05yxa22x2vjmqdky6g2d06akka7krms5cwr8gzqhg34nzb1h65nr")))

(define-public crate-subset_sum-0.13.0 (c (n "subset_sum") (v "0.13.0") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0qal2grjfaqbprdbwv3zghq8npygqxy43xk217kp6f5mpwcjl3hi")))

(define-public crate-subset_sum-0.13.1 (c (n "subset_sum") (v "0.13.1") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1dg1zniczxp5ajvhvj6z136ylzsjyjgwi8bqfwc8k368svdrfk9k")))

(define-public crate-subset_sum-0.13.2 (c (n "subset_sum") (v "0.13.2") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1g3n7777la0mkcc21zadswzn1y8vldw47xdq9gvvr2rpnzk5gqp2")))

(define-public crate-subset_sum-0.13.3 (c (n "subset_sum") (v "0.13.3") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0sa00lll68njx15pa8cp1k8hqxxzhyqvf51nlhfhv6m4lgpn0q58")))

(define-public crate-subset_sum-0.13.4 (c (n "subset_sum") (v "0.13.4") (d (list (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0azg0p5x31b1rbbfh3mhk15jhpbizdkcp09adq7x3g3kg6llknpw")))

(define-public crate-subset_sum-0.14.0 (c (n "subset_sum") (v "0.14.0") (d (list (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "0nqj9wkmmgn54kl9705yzb32dvlmp1xc2hfg001kb48jq22sr8mr")))

(define-public crate-subset_sum-0.15.0 (c (n "subset_sum") (v "0.15.0") (d (list (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.1") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "1j3dfznr6c8k07dz2pka3mfy0n25hzzr5y53s1x1n5l7g84zacya")))

(define-public crate-subset_sum-0.16.0 (c (n "subset_sum") (v "0.16.0") (d (list (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.1") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "03qk3i6rrpqqmmxw7g3a097hkwvgk6wii53swiviw0mmbxhd0j4q")))

(define-public crate-subset_sum-0.16.1 (c (n "subset_sum") (v "0.16.1") (d (list (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.1") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "0pmj2dsrl0xadbmccvwfrc22a0bpn4cvd8zhjyg4x75lz8565ks2")))

(define-public crate-subset_sum-0.16.2 (c (n "subset_sum") (v "0.16.2") (d (list (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.1") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "0hin1gj6z1zwm0flnzqqa2zxh480i7xz2ixk2xcv3495f9ric0cx")))

(define-public crate-subset_sum-0.17.0 (c (n "subset_sum") (v "0.17.0") (d (list (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "1s39l18dwqq3f23f51i4akq2gx1d3p1msdcs7filzpaf9bm1qgkr")))

(define-public crate-subset_sum-0.17.1 (c (n "subset_sum") (v "0.17.1") (d (list (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "0q6d60xjwv2p7nia9d8dsd3jrr3yqjjgrk5r612s5llxiyrfbmfb")))

(define-public crate-subset_sum-0.17.2 (c (n "subset_sum") (v "0.17.2") (d (list (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "1k0f3j72cx1fx5cbhh1mkcwi3zfn56al6fhmj15kj5z5csmh248h")))

(define-public crate-subset_sum-0.17.3 (c (n "subset_sum") (v "0.17.3") (d (list (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "0fc3fi7vp30k8y18ky69b4aslrhxsm9vfbamamrbnq31ifsirsxd")))

(define-public crate-subset_sum-0.17.4 (c (n "subset_sum") (v "0.17.4") (d (list (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.1") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "0f5296jqfv84aq9pqwny4fdpvw346m8pm4h4imh4qa8m2rlj19vq")))

(define-public crate-subset_sum-0.18.0 (c (n "subset_sum") (v "0.18.0") (d (list (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.1") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (o #t) (d #t) (k 0)))) (h "1w6dsjywwwsrmv3sxknv5v45ql46qznk49brj0k3mnawvbgcxpca") (f (quote (("wasm" "wasm-bindgen") ("python" "pyo3"))))))

(define-public crate-subset_sum-0.18.1 (c (n "subset_sum") (v "0.18.1") (d (list (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.1") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (o #t) (d #t) (k 0)))) (h "15lf70jfyp26mf9xv45c3cifly4aaz5ihlwj1dwn88md3wlxh9az") (f (quote (("wasm" "wasm-bindgen") ("python" "pyo3"))))))

(define-public crate-subset_sum-0.18.2 (c (n "subset_sum") (v "0.18.2") (d (list (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.1") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (o #t) (d #t) (k 0)))) (h "14rjmaglfrvfd374an2byh9zyvv3kq5hhmgzly8n20fj0ncn36il") (f (quote (("wasm" "wasm-bindgen") ("python" "pyo3"))))))

(define-public crate-subset_sum-0.19.0 (c (n "subset_sum") (v "0.19.0") (d (list (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.1") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (o #t) (d #t) (k 0)))) (h "1p3b4whybqxf9ffy87a4k8b701iw8dkaw8xkzki5ps3ailmh28r2") (f (quote (("wasm" "wasm-bindgen") ("python" "pyo3"))))))

(define-public crate-subset_sum-0.20.0 (c (n "subset_sum") (v "0.20.0") (d (list (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.1") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (o #t) (d #t) (k 0)))) (h "03ypz64dl365dyljr6ss366jzpm1afcl15m27zbf2978lzbap8r4") (f (quote (("wasm" "wasm-bindgen") ("python" "pyo3"))))))

(define-public crate-subset_sum-0.21.0 (c (n "subset_sum") (v "0.21.0") (d (list (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.1") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (o #t) (d #t) (k 0)))) (h "18dfazcdj59r683q42jmg5kk29kp8jspazy5jar07ikqwnv6fi5h") (f (quote (("wasm" "wasm-bindgen") ("python" "pyo3"))))))

(define-public crate-subset_sum-0.22.0 (c (n "subset_sum") (v "0.22.0") (d (list (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.1") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (o #t) (d #t) (k 0)))) (h "0s85s619hgj2rx6snawjyq41qri78rqz69vd8l0sa2v3hplq2q6c") (f (quote (("wasm" "wasm-bindgen") ("python" "pyo3"))))))

(define-public crate-subset_sum-0.22.1 (c (n "subset_sum") (v "0.22.1") (d (list (d (n "field_accessor") (r "^0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.1") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (o #t) (d #t) (k 0)))) (h "1m4n9mgdf3dv1paxpmxr86c9i6ibf42fiqdnf9163izbzpsmplqb") (f (quote (("wasm" "wasm-bindgen") ("python" "pyo3"))))))

(define-public crate-subset_sum-0.22.2 (c (n "subset_sum") (v "0.22.2") (d (list (d (n "field_accessor") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.1") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (o #t) (d #t) (k 0)))) (h "062fb5xrvdh4gsaj0vs6awjr4bbi4nshc0mb9d9r467vn1yqpym9") (f (quote (("wasm" "wasm-bindgen") ("python" "pyo3"))))))

(define-public crate-subset_sum-0.22.3 (c (n "subset_sum") (v "0.22.3") (d (list (d (n "field_accessor") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (o #t) (d #t) (k 0)))) (h "0hf03a2jdh90x6x65qmdlk1xz45c65lyv51imzl69pn2y9389vhx") (f (quote (("wasm" "wasm-bindgen") ("python" "pyo3"))))))

