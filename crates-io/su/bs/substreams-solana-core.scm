(define-module (crates-io su bs substreams-solana-core) #:use-module (crates-io))

(define-public crate-substreams-solana-core-0.1.1 (c (n "substreams-solana-core") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "03x7y3805l4565sib8nsvp3k5daxknvf77zzq1q5l9bqz53cmlfm") (r "1.64")))

(define-public crate-substreams-solana-core-0.10.1 (c (n "substreams-solana-core") (v "0.10.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "1ssw8r0bx2k8b2d1ijrjg20x0pnin5k3qi565p6dm27qgz5syd6w") (r "1.64")))

(define-public crate-substreams-solana-core-0.10.2 (c (n "substreams-solana-core") (v "0.10.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "198rb5d7ilikyzh8lz0gzfxw3qvf9wkd7i8g52l2m6lqmwb40k24") (r "1.64")))

(define-public crate-substreams-solana-core-0.11.0 (c (n "substreams-solana-core") (v "0.11.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "0s6xqdhzn7x5jq009ynf7dd0af5g3rl8ckywqndvjaz0rb08s66d") (y #t) (r "1.64")))

(define-public crate-substreams-solana-core-0.11.1 (c (n "substreams-solana-core") (v "0.11.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "1vyhlpzllry8d100c1bg7z4ng4jxpym0qb3jig7fkmfnp6c3mvnm") (r "1.64")))

