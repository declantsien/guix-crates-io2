(define-module (crates-io su bs substreams-sink-kv) #:use-module (crates-io))

(define-public crate-substreams-sink-kv-0.1.0 (c (n "substreams-sink-kv") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.0") (d #t) (k 0)) (d (n "substreams") (r "^0.5.0") (d #t) (k 0)))) (h "1wrypkrfpwb4sdgmj8pap0zcva9w49y36ac5ripfplmvcy26pfhh") (r "1.60")))

(define-public crate-substreams-sink-kv-0.1.1 (c (n "substreams-sink-kv") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.0") (d #t) (k 0)) (d (n "substreams") (r "^0.5.0") (d #t) (k 0)))) (h "0hnz3rcxb0mqg0653ljzaf6lk8j2ihjrf4sg8ddgkh5pci3aifyb") (r "1.60")))

(define-public crate-substreams-sink-kv-0.1.2 (c (n "substreams-sink-kv") (v "0.1.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.0") (d #t) (k 0)) (d (n "substreams") (r "^0.5.0") (d #t) (k 0)))) (h "0bc7d43b4wyyijx0kg1143givdywwvc1pinshjmyb9sqir24s2fb") (r "1.60")))

(define-public crate-substreams-sink-kv-0.1.3 (c (n "substreams-sink-kv") (v "0.1.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.0") (d #t) (k 0)) (d (n "substreams-sink-core") (r "^0.1.3") (d #t) (k 0)))) (h "1rnxr2jkw2bmkz4wqd9bg0nji951zq89ybj43l6gzmx9h14f4bdj") (r "1.60")))

