(define-module (crates-io su bs subset-map) #:use-module (crates-io))

(define-public crate-subset-map-0.1.0 (c (n "subset-map") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "194yq0kqxxf1gj6hf7x9mhyayngifzpaws3v4q2pz8v67sippsrp")))

(define-public crate-subset-map-0.2.0 (c (n "subset-map") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "16lw6qh2v31j5j4y77hvbadh8dc4s0jca85jqm5dswiz2rzihxx3")))

(define-public crate-subset-map-0.2.1 (c (n "subset-map") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0abzvgwya1rzxyv69ywdpzlyvydi2r0vyhc1a9d4l4a9fkq4pizw")))

(define-public crate-subset-map-0.2.2 (c (n "subset-map") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0bp29j5yj8hx8bz0n78fm9yxnlciwyzs9bpjq287wdzyy8i4vls3")))

(define-public crate-subset-map-0.3.0 (c (n "subset-map") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0n394fwwi4jw75522d75gqq99kgymwm9x5jn0d9vip4831pvq4p6")))

(define-public crate-subset-map-0.3.1 (c (n "subset-map") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1wh4qqblwqqs6l469xyzcsnd1cfkvrscbghf9gik8k2niii8jzai")))

(define-public crate-subset-map-0.3.2 (c (n "subset-map") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1x3l1rq5wi2ay7bkn28q5c1jw948ls6h5zw28vdkyzll37dz8rl3")))

(define-public crate-subset-map-0.3.3 (c (n "subset-map") (v "0.3.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1c9syrfm1v1l046glrx4n97xyzlvsm9fvlpz7zyfsirrv4b7idgp")))

(define-public crate-subset-map-0.3.4 (c (n "subset-map") (v "0.3.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1prymzilrqh5y4zzz5gncc6c82w4nhhs79qszq9rfy3m12l6yi0z")))

