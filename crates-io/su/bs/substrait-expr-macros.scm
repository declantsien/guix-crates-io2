(define-module (crates-io su bs substrait-expr-macros) #:use-module (crates-io))

(define-public crate-substrait-expr-macros-0.1.0 (c (n "substrait-expr-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0vrzvwjrrglagfy7x965ymd4j4cafdc65bslkgcp58xb9f7xa57x")))

(define-public crate-substrait-expr-macros-0.1.1 (c (n "substrait-expr-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0mjdx2w196hhqhly2f79cisiykfm2hwr4cxbh49gkx0bn4q9qjgd")))

(define-public crate-substrait-expr-macros-0.2.0 (c (n "substrait-expr-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0hp19dxhphzp4hw00a0nqp8bqklqw5vc3k5bgnpaq53scpglwscz")))

(define-public crate-substrait-expr-macros-0.2.1 (c (n "substrait-expr-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0aqirjdky7xkxiy37d4qzg5r7j23l5s9sdmqn3zqvlcmaif5p7li")))

