(define-module (crates-io su bs subst) #:use-module (crates-io))

(define-public crate-subst-0.0.1-reserved (c (n "subst") (v "0.0.1-reserved") (h "0p686wcc44fc8m9w1fb43wd3s3y8pnlrjzk2dvwy0av93v8niad1")))

(define-public crate-subst-0.1.0 (c (n "subst") (v "0.1.0") (d (list (d (n "assert2") (r "^0.3.6") (d #t) (k 2)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0xcfwk9qsq7lnvj9zhl6wvrmz9x04lav9ylkf50w2rzscw9rzz5g")))

(define-public crate-subst-0.1.1 (c (n "subst") (v "0.1.1") (d (list (d (n "assert2") (r "^0.3.6") (d #t) (k 2)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.23") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0vvmqzgxq75fl1q0kawsba4i132212ylmwwlc1hcdhffxib3cw2h") (f (quote (("yaml" "serde" "serde_yaml"))))))

(define-public crate-subst-0.1.2 (c (n "subst") (v "0.1.2") (d (list (d (n "assert2") (r "^0.3.6") (d #t) (k 2)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8.23") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1ydllqz18fv6bxaq5d2lyymrhsvq4851nwficwhdk1ia4brj3rma") (f (quote (("yaml" "serde" "serde_yaml"))))))

(define-public crate-subst-0.2.0 (c (n "subst") (v "0.2.0") (d (list (d (n "assert2") (r "^0.3.6") (d #t) (k 2)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9.13") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "03l41bjrpscang2c81zlscvfp0jja2k328kmy0jlhbv2jnnm38r8") (f (quote (("yaml" "serde" "serde_yaml"))))))

(define-public crate-subst-0.2.1 (c (n "subst") (v "0.2.1") (d (list (d (n "assert2") (r "^0.3.6") (d #t) (k 2)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9.13") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1f1482zhcmc3bx84kpaqclaqywqg6j0lsmq0c7w54lg1r73hs5v4") (f (quote (("yaml" "serde" "serde_yaml")))) (y #t)))

(define-public crate-subst-0.2.2 (c (n "subst") (v "0.2.2") (d (list (d (n "assert2") (r "^0.3.6") (d #t) (k 2)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9.13") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "15mmwnb54wiwmfh6f8jd79vzxm2h4zaxbq813hrswvd8vgww5xdx") (f (quote (("yaml" "serde" "serde_yaml"))))))

(define-public crate-subst-0.2.3 (c (n "subst") (v "0.2.3") (d (list (d (n "assert2") (r "^0.3.6") (d #t) (k 2)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9.13") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1f0l1pggkv14xhpqmdcif82m7vyx4ziis6427y8ibv83ax16yblj") (f (quote (("yaml" "serde" "serde_yaml"))))))

(define-public crate-subst-0.3.0 (c (n "subst") (v "0.3.0") (d (list (d (n "assert2") (r "^0.3.6") (d #t) (k 2)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9.13") (o #t) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "162vgypx8vslhvhhhsgxdbz8rz4dkf6wh9v7d50navbisvjih4ya") (f (quote (("yaml" "serde" "serde_yaml"))))))

