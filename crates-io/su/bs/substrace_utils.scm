(define-module (crates-io su bs substrace_utils) #:use-module (crates-io))

(define-public crate-substrace_utils-0.2.0 (c (n "substrace_utils") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "if_chain") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rustc-semver") (r "^1.1") (d #t) (k 0)))) (h "094mxqh4h50isnsm2nhk2kbrzr7drrnjx9cr0dia7ajlzn738njw") (f (quote (("internal") ("deny-warnings"))))))

