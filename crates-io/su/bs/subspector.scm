(define-module (crates-io su bs subspector) #:use-module (crates-io))

(define-public crate-subspector-0.9.0-rc17 (c (n "subspector") (v "0.9.0-rc17") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0c4qr338jv1c8jb2mz4kdikjmzjji3f4hb71cvn48sfjxlwl42p7")))

(define-public crate-subspector-0.9.0-rc18 (c (n "subspector") (v "0.9.0-rc18") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "16ckxw4nq9nx3qr3rf54fjj1809kskv5mibckdv205miwfz0c72k")))

(define-public crate-subspector-0.9.0-rc19 (c (n "subspector") (v "0.9.0-rc19") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "088ap2yrzrmdq2mvns47phc01vkvp197imdgr3a36l8vm637mifr")))

(define-public crate-subspector-0.9.0-rc20 (c (n "subspector") (v "0.9.0-rc20") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0amxxk70jrq12k8rrvvzb5hh0p9nzxn2clwc975n1j0y0m5nc2hh")))

(define-public crate-subspector-0.9.0 (c (n "subspector") (v "0.9.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17ldb9xd2qckzkc630z6qa8fqaih65xyfwk8xi9586lfb9gymi2n")))

(define-public crate-subspector-0.9.1 (c (n "subspector") (v "0.9.1") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yf3fr4dqyly1zly6723nm66jr6hph5a7c2gh1xbi4bp0i5mkfm5")))

(define-public crate-subspector-0.9.2 (c (n "subspector") (v "0.9.2") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1s5fbmsk8x6c4r0j0vk5cpsjy80raygp3zkvkgg7w7k3hcybqb8x")))

(define-public crate-subspector-0.10.0 (c (n "subspector") (v "0.10.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rwyf64cbw4jwr8gk7d7qbi0xzr7j9ci8fqfsfcrnay29k7wsbgg")))

(define-public crate-subspector-0.10.1 (c (n "subspector") (v "0.10.1") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0g9f7ql1854mb39h6c90w3qjly6gbn2q53fp29k0xwrz5ryg1lg0")))

(define-public crate-subspector-0.11.0 (c (n "subspector") (v "0.11.0") (d (list (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0il02j98gg580y6bnxa3pjx5ib53fs8p3akx46k4wnj7fx0v1mfc")))

