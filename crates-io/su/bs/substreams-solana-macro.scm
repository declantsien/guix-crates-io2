(define-module (crates-io su bs substreams-solana-macro) #:use-module (crates-io))

(define-public crate-substreams-solana-macro-0.10.1 (c (n "substreams-solana-macro") (v "0.10.1") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)))) (h "0spw55ig9k8m7jfyk45dv5pl7030d8p3rc3akjqvyd4h9wpzczsf") (r "1.60")))

(define-public crate-substreams-solana-macro-0.10.2 (c (n "substreams-solana-macro") (v "0.10.2") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)))) (h "0c6apgq4lcg6l2vl6ak58w5msslhkl0vzf8c5fx2n60mhjbg0m0y") (r "1.60")))

(define-public crate-substreams-solana-macro-0.11.0 (c (n "substreams-solana-macro") (v "0.11.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)))) (h "0b90w0yfzqvdx1is2nl1lqxcydaxlfidm05mgkbjggia10n04vdi") (y #t) (r "1.60")))

(define-public crate-substreams-solana-macro-0.11.1 (c (n "substreams-solana-macro") (v "0.11.1") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)))) (h "1l0mkkxfycbar8dg3z8h83z22p729dysjh7vs41iq3h9c8p6xxxc") (r "1.60")))

