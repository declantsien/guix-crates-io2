(define-module (crates-io su bs substreams-near-core) #:use-module (crates-io))

(define-public crate-substreams-near-core-0.10.1 (c (n "substreams-near-core") (v "0.10.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "1pq6pl5pgf06zzzjaa5c2q373vvxjf9xxplbzlnbd1nbwqvz88lr") (r "1.64")))

(define-public crate-substreams-near-core-0.9.3 (c (n "substreams-near-core") (v "0.9.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "11zkhha5gn161vsimxmp1k4blpi8lynm6x8zp80malzypc9lmrv9") (r "1.60")))

(define-public crate-substreams-near-core-0.9.4 (c (n "substreams-near-core") (v "0.9.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "1i88623jgbih8hxx0cykl599wx3xzvjsw8waax826ad76xs4d5ym") (r "1.60")))

(define-public crate-substreams-near-core-0.9.5 (c (n "substreams-near-core") (v "0.9.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("custom"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "0vsd1kj0zkbwl9wbqm40mychl03hh2109y2g05rg81mnhglhy9rl") (r "1.60")))

(define-public crate-substreams-near-core-0.9.6 (c (n "substreams-near-core") (v "0.9.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("custom"))) (d #t) (t "wasm32-unknown-unknown") (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-types") (r "^0.11") (d #t) (k 0)))) (h "06wqsqbp9w6ic60qxvp3j4332xzndb996zgsmdxaaf1dag64xqbd") (r "1.60")))

